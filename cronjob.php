<?Php
	echo "=======================<br>";
	echo '====='.date('Y-m-d g:i').'====='.'<br>';
	echo "=======================<br>";

	//ini_set('display_errors',1);
	ini_set('max_execution_time',70000);
	include 'dataAccess/DBManager2.php';
	$db =  new DBManager2(1);
	
	require_once "class/cls_commonFunctions_get.php";
	$obj_comm 		= new cls_commonFunctions_get($db);
	
	include "libraries/email_service/mailPoolTable.php";
	
	$db->connect();
			
	$sql	= 	"	SELECT
						sys_cronjob.CRONJOB_ID,
						sys_cronjob.CRONJOB_NAME,
						sys_cronjob.EXCUTE_FILE_PATH,
						sys_cronjob.TO_EMAILS,
						sys_cronjob.CC_EMAILS,
						sys_cronjob.BCC_EMAILS
						
					FROM `sys_cronjob`
					WHERE EXCUTE_TIME_WITH_FROM <=TIME_FORMAT(NOW(),'%H:%i:%s') AND EXCUTE_TIME_TO >TIME_FORMAT(NOW(),'%H:%i:%s')
					AND ( UPPER( DAYNAME(now())) =IF( 		sys_cronjob.SUNDAY,		'SUNDAY','') OR
								UPPER( DAYNAME(now())) =IF( sys_cronjob.MONDAY,		'MONDAY','') OR
								UPPER( DAYNAME(now())) =IF( sys_cronjob.TUESDAY,	'TUESDAY','') OR
								UPPER( DAYNAME(now())) =IF( sys_cronjob.WEDNESDAY,	'WEDNESDAY','') OR
								UPPER( DAYNAME(now())) =IF( sys_cronjob.THURSDAY,	'THURSDAY','') OR
								UPPER( DAYNAME(now())) =IF( sys_cronjob.FRIDAY,		'FRIDAY','') OR
								UPPER( DAYNAME(now())) =IF( sys_cronjob.SATURDAY,	'SATURDAY','') 
						 )  ";
						// echo $sql;
	$result 		= $db->RunQuery($sql);
	$row 			= mysqli_fetch_array($result);
 	$filePath		= $row['EXCUTE_FILE_PATH'];
	if($filePath!='')
	{
		$CRON_ID			= $row['CRONJOB_ID'];
		$CRON_TABLE_TO		= $row['TO_EMAILS'];
		$CRON_TABLE_CC		= $row['CC_EMAILS'];
		$CRON_TABLE_BCC		= $row['BCC_EMAILS'];

		include $filePath;
	}
	$hour 		= date('G');
	$min 		= date('i');
	$date 		= date('d');
	$lastDay	= date('t');
	//include_once "libraries/mail/mailservice.php";

	//if(($hour=>6 && $hour<=23  && $min>=50 && $min<55) || ($hour=>6 && $hour<=23  && $min>=20 && $min<25)){
	if($min>=25 && $min<30)
		include "presentation/backup_email_pool.php";
	//}

	if($date == $lastDay && ($hour==18 && $min>=30 && $min<35))
		include "presentation/dailyMails/invalidUserLogin/rptInvalidLoginAttemptEmail.php";
        
        if($date == $lastDay && ($hour==23 && $min>=20 && $min<25))
		include "presentation/dailyMails/costing_inquires_monthly/costing_inquires_monthly_email.php";
        
        if($date == $lastDay && ($hour==23 && $min>=25 && $min<30))
		include "presentation/dailyMails/costing_inquires_monthly-VS/costing_inquires_monthly_email_vs.php";
		
	if($hour==9 && $min>=00 && $min<05)
		include "presentation/customerAndOperation/bulk/reports/rptPORejectReminderEmail.php";
	
	if(date('i') == '00' && ($hour>=6 && $hour<24))
		include "presentation/customerAndOperation/bulk/poUserInactive.php";
		
	if($date == 01 && ($hour==01 && $min>=05 && $min<10))
		include "presentation/dailyMails/revenue_sales_order_wise_summery/revenue_sales_order_wise_summery.php";


	if($date == 01 && ($hour==03 && $min>=05 && $min<10))
		include "presentation/dailyMails/po_details/department_wise_po_details_report.php";

/*	if($date == 23 && ($hour==14 && $min>=05 && $min<10))
		include "presentation/dailyMails/revenue_sales_order_wise_summery/revenue_sales_order_wise_summery.php";
*/ 
	
	
	$db->disconnect();
	echo "CRON JOB DONE";
?>
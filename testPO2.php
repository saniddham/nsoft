<?php
session_start();
ini_set('max_execution_time',100000);
include_once 'dataAccess/DBManager.php';


require_once $_SESSION['ROOT_PATH']."class/common/azureDBconnection.php";


$objAzure = new cls_azureDBconnection();
$azure_connection = $objAzure->connectAzureDB();
$NBT_taxcodes = array(2,3,22);


$db = new DBManager();
$db->SetConnectionString($_SESSION["Server"], $_SESSION["UserName"], $_SESSION["Password"], $_SESSION["Database"], $_SESSION['userId']);
$db->OpenConnection();
$serialNo = '106012449';
$serialYear = '2019';
$transactionType = 'SO_Shipment';


$sql_header = "SELECT
						TH.intReviseNo,
						CONCAT(TH.intOrderNo,'/',TH.intOrderYear) AS poString,
						TH.intStatus,
						ware_fabricdispatchheader.strRemarks,
						TH.intApproveLevelStart,
						mst_customer_locations_header.strName AS customer_location,
						TH.strCustomerPoNo,
						ware_fabricdispatchheader.intCompanyId AS location,
						TH.intCustomer,
						TH.intPaymentTerm,
						TH.dtmCreateDate AS order_date,
						mst_customer.strName AS customer,
						mst_customer.strCode AS customerCode,
						TH.strCustomerPoNo,
						mst_financecurrency.strCode AS curr_code,
						TH.intMarketer AS marketer,
						(
							SELECT
								CAST(
									MAX(
										ware_stocktransactions_fabric.dtDate
									) AS DATE
								)
							FROM
								ware_stocktransactions_fabric
							WHERE
								ware_stocktransactions_fabric.intOrderNo = TH.intOrderNo
							AND ware_stocktransactions_fabric.intOrderYear = TH.intOrderYear
							AND ware_stocktransactions_fabric.strType LIKE '%Dispatched%' 
							GROUP BY
								ware_stocktransactions_fabric.intOrderNo,
								ware_stocktransactions_fabric.intOrderYear
						) AS lastDispatchDate
					FROM
					ware_fabricdispatchheader 
					INNER JOIN trn_orderheader TH ON TH.intOrderNo = ware_fabricdispatchheader.intOrderNo AND TH.intOrderYear = ware_fabricdispatchheader.intOrderYear
					INNER JOIN mst_customer ON mst_customer.intId = TH.intCustomer
					INNER JOIN mst_financecurrency ON mst_financecurrency.intId = TH.intCurrency
					LEFT JOIN mst_customer_locations_header ON ware_fabricdispatchheader.intCustLocation = mst_customer_locations_header.intId
					WHERE
						ware_fabricdispatchheader.intBulkDispatchNo = '$serialNo'
					AND ware_fabricdispatchheader.intBulkDispatchNoYear = '$serialYear'";

$result_header = $db->RunQuery2($sql_header);
while ($row_header = mysqli_fetch_array($result_header)) {
    $poString = trim($row_header['poString']);
    $customer = trim($row_header['customer']);
    $cus_location = trim($row_header['customer_location']);
    $customerCode = $row_header['customerCode'];
    $strCustomerPoNo = trim($row_header['strCustomerPoNo']);
    $strRemark = str_replace("'","''",$row_header['strRemarks']);
    $strRemark_sql = $db->escapeString($strRemark);
    $orderDate = $row_header['order_date'];
    $curr_code = ($row_header['curr_code'] == 'EURO')?"Eur":($row_header['curr_code'] == 'LKR'?"":$row_header['curr_code']);
    $payment_term = $row_header['intPaymentTerm'];
    $marketer = $row_header['marketer'];
    $location = '90';
    $revise_no = $row_header['intReviseNo'];
    $lastDispatchDate = $row_header['lastDispatchDate'];
    $shipmentNo = $serialNo.'/'.$serialYear;
    $successHeader = 0;
    $i = 0;
    $canSend = true;

    $sql_select = "SELECT
						OD.strStyleNo,
						OD.strGraphicNo,
						OD.strSalesOrderNo,
						OD.intSalesOrderId,
						OD.strPrintName,
						OD.dtPSD AS PSD,
						OD.strCombo,
						OD.TECHNIQUE_GROUP_ID,
						(
							SELECT
								TECHNIQUE_GROUP_NAME
							FROM
								mst_technique_groups
							WHERE
								mst_technique_groups.TECHNIQUE_GROUP_ID = OD.TECHNIQUE_GROUP_ID
						) AS technique,
						CONCAT(
							OD.intSampleNo,
							'/',
							OD.intSampleYear
						) AS sampleString,
						OD.dblPrice AS price,
						ware_fabricdispatchdetails.strCutNo,
						mst_part.strName AS partName,
						ware_fabricdispatchdetails.strLineNo,
						SUM(
							ware_fabricdispatchdetails.dblGoodQty
						) AS dblGoodQty
					FROM
						trn_orderdetails OD
					INNER JOIN ware_fabricdispatchheader ON ware_fabricdispatchheader.intOrderNo = OD.intOrderNo
					AND ware_fabricdispatchheader.intOrderYear = OD.intOrderYear
					INNER JOIN ware_fabricdispatchdetails ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo
					AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear
					AND ware_fabricdispatchdetails.intSalesOrderId = OD.intSalesOrderId
					LEFT JOIN mst_part ON ware_fabricdispatchdetails.intPart = mst_part.intId
					WHERE
						ware_fabricdispatchheader.intBulkDispatchNo = '$serialNo'
					AND ware_fabricdispatchheader.intBulkDispatchNoYear = '$serialYear'
					GROUP BY
						OD.intOrderNo,
						OD.intOrderYear,
						OD.intSalesOrderId";

    $result1 = $db->RunQuery2($sql_select);
    while ($row = mysqli_fetch_array($result1)) {
        $intSalesOrderId = $row['intSalesOrderId'];
        $strSalesOrderNo = trim($row['strSalesOrderNo']);
        $line_no = intval($row['strLineNo']);
        $strGraphicNo = trim($row['strGraphicNo']);
        $strCombo = trim($row['strCombo']);
        $sampleNo = trim($row['sampleString']);
        $strStyleNo = trim($row['strStyleNo']);
        $strPrintName = trim($row['strPrintName']);
        $poqty = $row['dblGoodQty'];
        $partName = trim($row['partName']);
        $brand = trim($row['brand']);
        $amount = round( $row['price'], 5);
        $psd = $row['PSD'];
        $technique = $row['technique'];
        $successDetails = 0;
        if($poqty > 0) {
            $i++;
            $sql_azure_details = "INSERT into SalesLine (Transaction_type, Order_No, Sales_Order_No, Line_No, Shipment_No, Sales_Order_Name, Item_Code, Description, Item_Category, Item_SubCategory, Quantity, Original_Quantity, Amount, VAT_Prod_Posting_Group, Technique_Revenue_Code, Graphic_No, Style_No, [Print], Combo, Part, Brand, Sample_No, Production_Start_Date, Revised_Count) VALUES ('$transactionType','$poString','$intSalesOrderId','$line_no', '$shipmentNo', '$strSalesOrderNo', 'PRINT','PRINT','PRINT','PRINT','$poqty','$poqty','$amount', 'VAT', '$technique', '$strGraphicNo','$strStyleNo','$strPrintName','$strCombo','$partName','$brand','$sampleNo','$psd', '$revise_no')";
            if ($azure_connection) {
                $getResults = $objAzure->runQuery($azure_connection, $sql_azure_details);
                if ($getResults != FALSE) {
                    $successDetails = 1;
                }
            }
            $sql_details = "INSERT into trn_financemodule_salesline (Transaction_type, Order_No, Sales_Order_No, Line_No, Shipment_No, Sales_Order_Name, Item_Code, Description, Item_Category, Item_SubCategory, Quantity, Original_Quantity, Amount, VAT_Prod_Posting_Group, Technique_Revenue_Code, Graphic_No, Style_No, Print, Combo, Part, Brand, Sample_No, Production_Start_Date, Revised_Count, deliveryStatus, deliveryDate) VALUES ('$transactionType','$poString','$intSalesOrderId','$line_no', '$shipmentNo', '$strSalesOrderNo', 'PRINT','PRINT','PRINT','PRINT','$poqty', '$poqty','$amount', 'VAT', '$technique', '$strGraphicNo','$strStyleNo','$strPrintName','$strCombo','$partName','$brand','$sampleNo','$psd', '$revise_no', '$successDetails',NOW())";
            $result_details = $db->RunQuery2($sql_details);
        }
    }


        $sql_azure_header = "INSERT into SalesHeader (Transaction_type, Order_No, Document_Type, Shipment_No, Customer_Code, Customer_Name, Posting_Date, Order_Date, Currency_Code, Narration, Payment_Terms, Customer_PO_Number, Sales_Person, Location_Code, No_of_Lines, Last_Dispatched_Date, Revised_Count, Customer_Location) VALUES ('$transactionType','$poString','Order','$shipmentNo','$customerCode','$customer', GETDATE(), '$orderDate','$curr_code','$strRemark','$payment_term','$strCustomerPoNo', '$marketer', '$location', '$i', '$lastDispatchDate', '$revise_no', '$cus_location')";
        var_dump($sql_azure_header);
        if ($azure_connection) {
            $getResults = $objAzure->runQuery($azure_connection, $sql_azure_header);
            if ($getResults != FALSE) {
                $successHeader = 1;
            }
        }
        $sql = "INSERT into trn_financemodule_salesheader (Transaction_type, Order_No, Document_Type, Shipment_No, Customer_Code, Customer_Name, Posting_Date, Order_Date, Currency_Code, Narration, Payment_Terms, Customer_PO_Number, Sales_Person, Location_Code, No_of_Lines, Last_Dispatched_Date, Revised_Count, Customer_Location,  deliveryStatus, deliveryDate ) VALUES ('$transactionType','$poString','Order', '$shipmentNo', '$customerCode','$customer', NOW(), '$orderDate','$curr_code','$strRemark_sql','$payment_term','$strCustomerPoNo', '$marketer', '$location', '$i', '$lastDispatchDate', '$revise_no', '$cus_location', '$successHeader', NOW())";
        $result_header = $db->RunQuery2($sql);


}

echo "JOB DONE";
<?php
session_start();
ini_set('max_execution_time',100000);
include_once 'dataAccess/DBManager.php';


require_once $_SESSION['ROOT_PATH']."class/common/azureDBconnection.php";


$objAzure = new cls_azureDBconnection();
$azure_connection = $objAzure->connectAzureDB();
$NBT_taxcodes = array(2,3,22);


$db = new DBManager();
$db->SetConnectionString($_SESSION["Server"], $_SESSION["UserName"], $_SESSION["Password"], $_SESSION["Database"], $_SESSION['userId']);
$db->OpenConnection();


$sql_header = "SELECT
	TPD.intPONo,
	TPD.intPOYear,
	TPD.intItem,
	(
		SELECT
			GROUP_CONCAT(
				DISTINCT ware_grnheader.strInvoiceNo
			)
		FROM
			ware_grnheader
		WHERE
			ware_grnheader.intPoNo = TPD.intPONo
		AND ware_grnheader.intPoYear = TPD.intPOYear
		AND ware_grnheader.intStatus = 1
	) AS invoices,
	trn_poheader.intSupplier,
	mst_item.strCode AS itemCode,
	mst_item.strName AS itemName,
	round(TPD.dblUnitPrice, 6) AS dblUnitPrice,
	TPD.dblDiscount,
	TPD.intTaxCode,
	sum(TPD.dblTaxAmmount) AS dblTaxAmmount,
	sum(TPD.dblQty) AS dblQty
FROM
	trn_podetails TPD
INNER JOIN trn_poheader ON trn_poheader.intPONo = TPD.intPONo
AND trn_poheader.intPOYear = TPD.intPOYear
INNER JOIN mst_locations ON mst_locations.intId = trn_poheader.intCompany
INNER JOIN mst_item ON TPD.intItem = mst_item.intId
WHERE
	TPD.intPOYear = '2019'
AND trn_poheader.intStatus NOT IN (- 10)
AND mst_locations.intCompanyId = 1
GROUP BY
	TPD.intPONo,
	TPD.intPOYear,
	TPD.intItem
HAVING
	dblQty > 0";

$result_header = $db->RunQuery($sql_header);
while ($row_header = mysqli_fetch_array($result_header)) {
    $poNo = $row_header['intPONo'];
    $poYear = $row_header['intPOYear'];
    $supplier = $row_header['intSupplier'];
    $item = $row_header['intItem'];
    $item_code = $row_header['itemCode'];
    $reviseNo = $row_header['intReviseNo'];
    $poString = $poNo.'/'.$poYear;
    $invoice_no = $row_header['invoices'];
    $invoice_no_arr = explode(',',$invoice_no);

    $poQty = $row_header['dblQty'];
    $tax_code = $row_header['intTaxCode'];
    $unit_price = $row_header['dblUnitPrice'];
    $discount = $row_header['dblDiscount'];

    $sql_select = "SELECT
                      IFNULL(SUM(SPID.QTY),0) AS invoiced_qty
                    FROM
                        finance_supplier_purchaseinvoice_details SPID
                    INNER JOIN finance_supplier_purchaseinvoice_header SPIH ON SPID.PURCHASE_INVOICE_NO = SPIH.PURCHASE_INVOICE_NO
                    AND SPIH.PURCHASE_INVOICE_YEAR = SPID.PURCHASE_INVOICE_YEAR
                    WHERE
                    SPIH.INVOICE_NO IN ('".implode("','", $invoice_no_arr)."')
                    AND SPIH.SUPPLIER_ID = '$supplier'
                    AND SPID.ITEM_ID = '$item'
                    AND SPIH.`STATUS` = 1";

    $result1 = $db->RunQuery($sql_select);
    while ($row = mysqli_fetch_array($result1)) {
        $invoiced_qty = $row['invoiced_qty'];
        $qty = $poQty - $invoiced_qty;
        if($qty < 0){
            $qty = 0;
        }
        if(in_array($tax_code,$NBT_taxcodes) ||  ($item_code == 'SERFNBT' && $tax_code != 0)){
            $tax_amount = calculateNewTaxAmount($tax_code,$qty,$unit_price,$discount,$db);
            $tax_amount = round($tax_amount,2);
        }
        else{
            $tax_amount = 0;
        }

       $sql_azure_details = "UPDATE PurchaseLine SET Quantity='$qty', NBT_Amount = '$tax_amount' WHERE Transaction_Type IN ('PO_New','PO_EditStart') AND Purchase_Order_No='$poString' AND Line_No='$item'";
        if ($azure_connection) {
            $getResults = $objAzure->runQuery($azure_connection, $sql_azure_details);

        }
        $sql_details = "UPDATE trn_financemodule_purchaseline SET Quantity='$qty', NBT_Amount = '$tax_amount' WHERE Transaction_Type IN ('PO_New','PO_EditStart') AND Purchase_Order_No='$poString' AND Line_No='$item'";
        $result_details = $db->RunQuery($sql_details);


    }


}

function getTotalReturnToSupplyAmount($grnArray,$itemId,$db){
    $ret_sup_amount = 0;
    for($i=0;$i<sizeof($grnArray);$i++){
        $grn = explode('/',$grnArray[$i]);
        $sql = "SELECT
                    (ware_grndetails.dblGrnQty - IFNULL(SUM(RSD.dblQty),0))  AS balance
                    FROM
                        ware_grndetails
                    INNER JOIN ware_returntosupplierheader RSH ON RSH.intGrnNo = ware_grndetails.intGrnNo
                    AND RSH.intGrnYear = ware_grndetails.intGrnYear AND RSH.intStatus = '1'
                    INNER JOIN ware_returntosupplierdetails RSD ON RSD.intReturnNo = RSH.intReturnNo
                    AND RSD.intReturnYear = RSD.intReturnYear
                    AND RSD.intItemId = ware_grndetails.intItemId
                    WHERE
                        ware_grndetails.intGrnNo = '$grn[0]'
                    AND ware_grndetails.intGrnYear = '$grn[1]'
                    AND ware_grndetails.intItemId = '$itemId'";
        $result = $db->RunQuery($sql);
        $row = mysqli_fetch_array($result);
        $ret_sup_amount += $row['balance'];
    }
    return $ret_sup_amount;
}

function calculateNewTaxAmount($taxId, $quantity, $unitPrice, $discount, $db){
    $amount=$quantity*$unitPrice;
    $amount = $amount * (100 - $discount) / 100;
    $sql = "SELECT
				mst_financetaxgroup.intId,
				mst_financetaxgroup.strProcess
				FROM
				mst_financetaxgroup
				WHERE
				mst_financetaxgroup.intId =  '$taxId'";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $taxProcess = $row['strProcess'];
    $arrTax = explode("/", $taxProcess);
    $operation = '';

    //(3)capture calculation method(isolated/inclusive/exclusive) and the array 'jsonTaxCode' contains only tax ids
    $jsonTaxCode = "[ ";
    if (count($arrTax) == 1)//If strProcess contain only one value (Example $row['strProcess'] = 5)
    {
        $operation = 'Isolated';
        $jsonTaxCode .= '{ "taxId":"' . $taxProcess . '"}';
    } else if (count($arrTax) > 1) //IF there are multiple tax ids in the tax group(example strProcess = {4/Inclusive/7})
    {
        $operation = $arrTax[1];//this should be inclusive/exclusive
        for ($i = 0; $i < count($arrTax); $i = $i + 2) {
            $jsonTaxCode .= '{ "taxId":"' . $arrTax[$i] . '"},'; //create a json array geting 0 and 2 value from the array
        }

        $jsonTaxCode = $jsonTaxCode . substr(0, count($jsonTaxCode) - 1);
    }
    $jsonTaxCode .= " ]";
    $taxCodes = json_decode($jsonTaxCode, true);

    //(4)get tax rates for all tax ids in the tax group ( tax array)
    if (count($taxCodes) != 0) {
        foreach ($taxCodes as $taxCode) {
            //get tax rates from the mst_financetaxisolated table sending taxId to the callTaxValue function which got by json array
            $codeValues[] = callTaxValue($taxCode['taxId'],$db);
        }
    }
    if (count($codeValues) > 1) // if there are more than one tax types in the tax group (this can be identified from mst_financetaxgroup.strProcess field)
    {
        if ($operation == 'Inclusive') {
            //step 1: po amount will be multiplied by the first tax rate
            //step 2 : result of the step 1(po amount+tax ammount), will be multiplied by the second tax rate.
            $firstVal = ($amount * $codeValues[0]) / 100;
            $withTaxVal = $firstVal + ((($amount + $firstVal) * $codeValues[1]) / 100);
            $val1 = ($amount * $codeValues[0]) / 100;
            $val2 = ((($amount + $firstVal) * $codeValues[1]) / 100);
        } else if ($operation == 'Exclusive') {
            //get the summation of the two tax rates and multiply it from the amount
            $withTaxVal = ($amount * ($codeValues[0] + $codeValues[1])) / 100;
            $val1 = ($amount * $codeValues[0]) / 100;
            $val2 = ($amount * $codeValues[1]) / 100;
        }
    }
    else if(count($codeValues) == 1 && $operation == 'Isolated')//there is only one tax type for the tax group
    {
        $withTaxVal = ($amount*$codeValues[0])/100;
        $val1 = ($amount*$codeValues[0])/100;
    }

    return $withTaxVal;
}

function callTaxValue($taxId, $db)
{
    $sql = "SELECT
				mst_financetaxisolated.intId,
				mst_financetaxisolated.strCode,
				mst_financetaxisolated.dblRate
				FROM
				mst_financetaxisolated
				WHERE
				mst_financetaxisolated.intId = '$taxId'
				";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $taxVal = $row['dblRate'];
    return $taxVal;
}

echo "JOB DONE";
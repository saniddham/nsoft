ALTER TABLE `nsoft`.`trn_sample_color_recipes`
ADD COLUMN `recipeRevision` INT(11) NOT NULL DEFAULT 0;

ALTER TABLE `trn_sample_color_recipes`
DROP PRIMARY KEY,
ADD PRIMARY KEY (`intSampleNo`, `intSampleYear`, `intRevisionNo`, `strCombo`, `strPrintName`, `intColorId`, `intTechniqueId`, `intInkTypeId`, `intItem`, `recipeRevision`);

ALTER TABLE `nsoft`.`trn_sample_color_recipes_approve`
ADD COLUMN `recipeRevision` INT(11) NOT NULL DEFAULT 0;

CREATE TABLE `bulk_colorroom_inks` (
  `inkId` int(11) NOT NULL AUTO_INCREMENT,
  `inkCode` varchar(45) NOT NULL,
  `createdDate` date NOT NULL,
  `createdBy` int(5) NOT NULL,
  `initiallyCreatedGraphic` varchar(45) NOT NULL,
  `colorId` int(10) NOT NULL,
  `inkTypeId` int(10) NOT NULL,
  `techniqueId` int(10) NOT NULL,
  PRIMARY KEY (`inkId`),
  UNIQUE KEY `inkCode_UNIQUE` (`inkCode`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1

CREATE TABLE `bulk_ink_quantity` (
  `intId` int(10) NOT NULL AUTO_INCREMENT,
  `inkId` int(10) NOT NULL,
  `orderNo` int(11) NOT NULL,
  `orderYear` int(11) NOT NULL,
  `sampleNo` int(11) NOT NULL,
  `sampleYear` int(11) NOT NULL,
  `revNo` int(11) NOT NULL,
  `print` varchar(45) NOT NULL,
  `combo` varchar(45) NOT NULL,
  `recipeRevision` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `createdDate` datetime NOT NULL,
  `salesOrderId` int(11) NOT NULL,
  `locationId` int(11) NOT NULL,
  PRIMARY KEY (`intId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1

CREATE TABLE `nsoft`.`colorroom_ink_issue` (
  `issueNo` INT(11) NOT NULL,
  `issueYear` INT(11) NOT NULL,
  `inkId` INT(11) NOT NULL,
  `issuedBy` INT(11) NULL,
  `issuedLocation` INT(11) NULL,
  `issuedDate` DATETIME NULL,
  PRIMARY KEY (`issueNo`, `issueYear`));

  ALTER TABLE `nsoft`.`bulk_ink_quantity`
ADD COLUMN `strType` VARCHAR(45) NOT NULL AFTER `locationId`;

ALTER TABLE `nsoft`.`bulk_ink_quantity`
ADD COLUMN `createdBy` INT(11) NOT NULL AFTER `strType`;



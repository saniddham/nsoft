/*
Navicat MySQL Data Transfer

Source Server         : mysqli
Source Server Version : 50723
Source Host           : localhost:3306
Source Database       : nsoft

Target Server Type    : MYSQL
Target Server Version : 50723
File Encoding         : 65001

Date: 2018-12-03 10:06:46
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `tbl_token_details`
-- ----------------------------
DROP TABLE IF EXISTS `tbl_token_details`;
CREATE TABLE `tbl_token_details` (
  `USER_ID` varchar(10) NOT NULL,
  `GENERATED_TIME` int(11) DEFAULT NULL,
  `EXPIRED_DATE` int(11) DEFAULT NULL,
  `TOKEN` text,
  `UPDATED_TIME` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of tbl_token_details
-- ----------------------------

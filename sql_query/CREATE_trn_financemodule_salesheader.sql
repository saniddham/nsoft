DROP TABLE IF EXISTS `trn_financemodule_salesheader`;
CREATE TABLE `trn_financemodule_salesheader` (
  `Transaction_Type` varchar(20) NOT NULL,
  `Order_No` varchar(20) NOT NULL,
  `Document_Type` varchar(20) NOT NULL,
  `Shipment_No` varchar(20) NOT NULL,
  `Customer_Code` varchar(20) DEFAULT NULL,
  `Customer_Name` varchar(50) DEFAULT NULL,
  `Posting_Date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `Order_Date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `Currency_Code` varchar(20) DEFAULT NULL,
  `Revised_Date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `Narration` varchar(250) DEFAULT NULL,
  `Payment_Terms` varchar(20) DEFAULT NULL,
  `Customer_PO_Number` varchar(50) DEFAULT NULL,
  `Sales_Person` varchar(20) DEFAULT NULL,
  `Production_Start_Date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `Sample_No` varchar(20) DEFAULT NULL,
  `Location_Code` varchar(20) DEFAULT NULL,
  `Last_Dispatched_Date` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `No_of_Lines` int(11) DEFAULT NULL,
  `Revised_Count` int(11) NOT NULL,
  `deliveryStatus` int(11) DEFAULT NULL,
  `deliveryDate` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `Customer_Location` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`Transaction_Type`,`Order_No`,`Document_Type`,`Shipment_No`,`Revised_Count`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

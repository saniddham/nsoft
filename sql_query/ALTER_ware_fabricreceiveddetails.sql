ALTER TABLE `ware_fabricreceiveddetails`
ADD COLUMN `ncingaDeliveryStatus`  int(1) NULL AFTER `dblQty`;

ALTER TABLE `ware_fabricreceiveddetails`
ADD COLUMN `maxRetryCount`  int(2) NULL AFTER `ncingaDeliveryStatus`;

ALTER TABLE `ware_fabricreceiveddetails`
ADD COLUMN `retryCount`  int(2) NOT NULL DEFAULT 0 AFTER `maxRetryCount`;
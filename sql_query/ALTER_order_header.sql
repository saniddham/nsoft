ALTER TABLE trn_orderheader ADD TENTATIVE INT(1) DEFAULT 0;
ALTER TABLE trn_orderheader ADD strTentitiveCustomerPoNo varchar(50);
ALTER TABLE trn_orderdetails ADD strTentitiveSalesOrderNo varchar(50);
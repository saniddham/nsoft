ALTER TABLE trn_ordersizeqty  ADD nciga_code VARCHAR(50);

CREATE TABLE manual_ncinga_trigger (
        intOrderNo INT NOT NULL,
        intOrderYear INT NOT NULL,
        intSalesOrderId INT NOT NULL,
        strSize VARCHAR (50),
        dblQty DOUBLE,
        ncingaDeliveryStatus INT (1),
        date date,
        USER VARCHAR (50),
        orderType INT (3),
        nciga_code VARCHAR (255),
        PRIMARY KEY (
        intOrderNo,
        intOrderYear,
        intSalesOrderId,
        strSize
)
);
ALTER TABLE `nsoft`.`manual_ncinga_trigger`
ADD COLUMN `intId` INT(14) NOT NULL AUTO_INCREMENT AFTER `nciga_code`,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`intId`);
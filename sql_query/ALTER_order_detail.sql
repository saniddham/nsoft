ALTER TABLE `trn_ordersizeqty`
ADD COLUMN `ncingaDeliveryStatus`  int(1) NULL DEFAULT 0 AFTER `dblQty`,
ADD COLUMN `maxRetryCount`  int(2) NULL DEFAULT NULL AFTER `ncingaDeliveryStatus`,
ADD COLUMN `retryCount`  int(2) NULL DEFAULT NULL AFTER `maxRetryCount`;

//Heat Seal Dispatch

ALTER TABLE `nsoft`.`trn_orderdetails`
ADD COLUMN `SO_TYPE` INT(5) NOT NULL DEFAULT 0 AFTER `strTentitiveSalesOrderNo`;

ALTER TABLE `nsoft`.`trn_orderdetails`
ADD COLUMN `linkedSoId` INT(11) NULL AFTER `SO_TYPE`;

ALTER TABLE `nsoft`.`trn_orderdetails`
ADD COLUMN `splitedPrice` DOUBLE NOT NULL DEFAULT 0 AFTER `linkedSoId`;

###for mails
ALTER TABLE `nsoft`.`daily_mails_revenue_old`
ADD COLUMN `LOCATION_ID` INT(11) NOT NULL DEFAULT '0' AFTER `REVENUE`,
DROP PRIMARY KEY,
ADD PRIMARY KEY (`MARKETER_ID`, `DATE`, `PLANT_ID`, `PO_TECHNIQUE`, `PO_TECH_GROPU_NEW`, `LOCATION_ID`);


CREATE TABLE `nsoft`.`mst_location_monthly_targets` (
  `LOCATION_ID` INT(11) NOT NULL,
  `YEAR` INT(11) NOT NULL,
  `MONTH` INT(11) NOT NULL,
  `TARGET_MONTHLY` DOUBLE NULL,
  `TARGET_DAILY` DOUBLE NULL,
  `STATUS` INT(11) NULL DEFAULT '0',
  PRIMARY KEY (`LOCATION_ID`, `YEAR`, `MONTH`));


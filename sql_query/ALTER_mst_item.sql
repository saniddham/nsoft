UPDATE mst_item
SET strCode = CONCAT(
	IFNULL(
		(
			SELECT
				mst_supplier.strCode
			FROM
				mst_supplier
			WHERE
				mst_supplier.intId = mst_item.intPerfectSupplier
		),
		''
	),
	mst_item.strCode
);
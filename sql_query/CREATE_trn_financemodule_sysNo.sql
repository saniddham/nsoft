DROP TABLE IF EXISTS `trn_financemodule_sysNo`;
CREATE TABLE `trn_financemodule_sysNo` (
  `COMPANY_ID` int(11) NOT NULL,
  `ISSUE_NO` int(11) DEFAULT NULL,
  `GATEPASS_NO` int(11) DEFAULT NULL,
  `TRANSFERIN_NO` int(11) DEFAULT NULL,
  `DISPOSAL_NO` int(11) DEFAULT NULL,
  `STOCK_ADJUSTMENT_NO` int(11) DEFAULT NULL,
  PRIMARY KEY (`COMPANY_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DELIMITER $$
 
CREATE PROCEDURE insert_companies ()
BEGIN
 
 DECLARE v_finished INTEGER DEFAULT 0;
        DECLARE v_company INTEGER DEFAULT 0;
 
 -- declare cursor for employee email
 DEClARE company_cursor CURSOR FOR  SELECT intId FROM `mst_locations` WHERE `intCompanyId` = '1' AND `intStatus` = '1';
 
 -- declare NOT FOUND handler
 DECLARE CONTINUE HANDLER 
        FOR NOT FOUND SET v_finished = 1;
 
 OPEN company_cursor;
 
 get_company: LOOP
 
 FETCH company_cursor INTO v_company;
 
 IF v_finished = 1 THEN 
 LEAVE get_company;
 END IF;
 
 -- build email list
 INSERT INTO `trn_financemodule_sysNo` (`COMPANY_ID`) VALUES (v_company);
 
 END LOOP get_company;
 
 CLOSE company_cursor;
 
END$$
 
DELIMITER ;

CALL insert_companies ();
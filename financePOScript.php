<?php
session_start();
ini_set('max_execution_time',100000);
include_once 'dataAccess/DBManager.php';


require_once $_SESSION['ROOT_PATH']."class/common/azureDBconnection.php";


$objAzure = new cls_azureDBconnection();
$azure_connection = $objAzure->connectAzureDB();
$NBT_taxcodes = array(2,3,22);


$db = new DBManager();
$db->SetConnectionString($_SESSION["Server"], $_SESSION["UserName"], $_SESSION["Password"], $_SESSION["Database"], $_SESSION['userId']);
$db->OpenConnection();


$sql_header = "SELECT DISTINCT
                trn_poheader.intPONo AS Order_No,
                trn_poheader.intPOYear AS Order_Year,
                trn_poheader.dtmCreateDate AS revised_Date,
                MAX(trn_poheader_approvedby.dtApprovedDate) AS posting_date,
                IF (
                    trn_poheader.intStatus = 1,
                    'PO_New',
                    'PO_EditStart'
                ) AS Transaction_Type,
                 mst_supplier.strName AS supplier,
                 mst_supplier.strCode AS supplier_code,
                 mst_financecurrency.strCode AS currency_code,
                 trn_poheader.intPaymentTerm AS payTerm,
                 trn_poheader.intPaymentMode AS payMode,
                 trn_poheader.intShipmentTerm AS shipmentTerm,
                 trn_poheader.intShipmentMode AS shipmentMode,
                 trn_poheader.dtmDeliveryDate AS deliveryDate,
                 trn_poheader.strRemarks AS narration,
                 trn_poheader.dtmPODate AS orderDate,
                 trn_poheader.intStatus,
                 trn_poheader.PRINT_COUNT,
                 trn_poheader.intApproveLevels,
                 trn_poheader.intUser,
                 trn_poheader.intCompany AS poRaisedLocationId,
                 sys_users.strUserName,
                 trn_poheader.intReviseNo,
                 trn_poheader.PO_TYPE
                FROM
                    trn_poheader
                INNER JOIN trn_poheader_approvedby ON trn_poheader.intPONo = trn_poheader_approvedby.intPONo                
                AND trn_poheader.intPOyear = trn_poheader_approvedby.intYear
                AND trn_poheader_approvedby.intApproveLevelNo = 5
                INNER JOIN mst_locations ON mst_locations.intId = trn_poheader.intCompany
                INNER JOIN mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
                INNER JOIN sys_users ON trn_poheader.intUser = sys_users.intUserId
                INNER JOIN mst_financecurrency ON mst_financecurrency.intId = trn_poheader.intCurrency
                WHERE
                    trn_poheader.intPOYear = '2019'
                AND trn_poheader.intStatus NOT IN (- 10)
                AND mst_locations.intCompanyId = 1
                GROUP BY
                    trn_poheader.intPONo,
                    trn_poheader.intPOYear";

$result_header = $db->RunQuery($sql_header);
while ($row_header = mysqli_fetch_array($result_header)) {
    $poNo = $row_header['Order_No'];
    $poYear = $row_header['Order_Year'];
    $supplier = $row_header['supplier'];
    $supplier_code = $row_header['supplier_code'];
    $currency = ($row_header['currency_code'] == 'EURO')?"Eur":($row_header['currency_code'] == 'LKR'?"":$row_header['currency_code']);
    $payTerm = $row_header['payTerm'];
    $payMode = $row_header['payMode'];
    $shipmentMode = $row_header['shipmentMode'];
    $deliveryDate = $row_header['deliveryDate'];
    $locationId = $row_header['poRaisedLocationId'];
    $orderDate = $row_header['orderDate'];
    $reviseNo = $row_header['intReviseNo'];
    $po_type = $row_header['shipmentTerm'];
    $poString = $poNo.'/'.$poYear;
    $narration = trim($row_header['narration']);
    $narration = str_replace("'", '', $narration);
    $posting_date = $row_header['posting_date'];
    $i = 0;
    $transactionType = $row_header['Transaction_Type'];
    $successHeader = 0;
    $revised_date = '';
    if($transactionType == 'PO_EditStart'){
        $revised_date = $row_header['revised_Date'];
    }


     $sql_select = "SELECT
                    TPD.intPONo,
                    TPD.intPOYear,
                    TPD.intItem,
                    (
                        SELECT
                            GROUP_CONCAT(intGrnNo, '/', intGrnYear)
                        FROM
                            ware_grnheader
                        WHERE
                            ware_grnheader.intPoNo = TPD.intPONo
                        AND ware_grnheader.intPoYear = TPD.intPOYear
                        AND ware_grnheader.intStatus = 1
                    ) AS grn_details,
                    mst_item.strCode AS itemCode,
                    mst_item.strName AS itemName,
                    mst_maincategory.strName AS mainCategory,
                    mst_subcategory.strName AS subCategory,
                    round(TPD.dblUnitPrice, 6) AS dblUnitPrice,
                    TPD.dblDiscount,
                    TPD.intTaxCode,
                    sum(TPD.dblTaxAmmount) AS dblTaxAmmount,
                    mst_financetaxgroup.strCode,
                    TPD.SVAT_ITEM,
                    sum(TPD.dblQty) AS dblQty,
                    mst_item.intUOM,
                    mst_units.strName AS uom,
                    TPD.CostCenter
                FROM
                    trn_podetails TPD
                INNER JOIN mst_item ON TPD.intItem = mst_item.intId
                INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
                INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
                LEFT JOIN mst_financetaxgroup ON TPD.intTaxCode = mst_financetaxgroup.intId
                INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
                WHERE
                    TPD.intPONo = '$poNo'
                AND TPD.intPOYear = '$poYear'
                GROUP BY
                    TPD.intItem
                HAVING
                    dblQty > 0";


    $result1 = $db->RunQuery($sql_select);
    while ($row = mysqli_fetch_array($result1)) {
        $line_no = $row['intItem'];
        $item_code = $row['itemCode'];
        $description = $row['itemName'];
        $item_category = $row['mainCategory'];
        $item_subcategory = $row['subCategory'];
        $qty = $row['dblQty'];
        $tax_code = $row['intTaxCode'];
        $unit_price = $row['dblUnitPrice'];
        $cost_center = $row['CostCenter'];
        $discount = $row['dblDiscount'];
        $successDetails = '0';
        $i++;
        $vat_group = ($tax_code != 0)?"VAT":"NO_VAT";
        $tot_grn_details = $row['grn_details'];
        if(in_array($tax_code,$NBT_taxcodes) ||  ($item_code == 'SERFNBT' && $tax_code != 0)){
            $tax_amount = calculateNewTaxAmount($tax_code,$qty,$unit_price,$discount,$db);
            $tax_amount = round($tax_amount,2);
        }
        else{
            $tax_amount = 0;
        }

        $i++;
        $sql_azure_details = "INSERT into PurchaseLine (Transaction_type, Purchase_Order_No, Revised_Count, Line_No, Item_Code, Narration, Item_Category, Item_SubCategory, Quantity, Unit_Cost, VAT_Prod_Posting_Group, NBT_Amount) VALUES ('$transactionType','$poString','$reviseNo','$line_no','$item_code','$description','$item_category','$item_subcategory','$qty','$unit_price','$vat_group','$tax_amount')";
        if ($azure_connection) {
            $getResults = $objAzure->runQuery($azure_connection, $sql_azure_details);
            if($getResults != FALSE){
                $successDetails = 1;
            }
            if($transactionType == 'PO_EditStart'){
                $sql_azure_details_copy = "INSERT into PurchaseLine (Transaction_type, Purchase_Order_No, Revised_Count, Line_No, Item_Code, Narration, Item_Category, Item_SubCategory, Quantity, Unit_Cost, VAT_Prod_Posting_Group, NBT_Amount) VALUES ('PO_New','$poString','$reviseNo','$line_no','$item_code','$description','$item_category','$item_subcategory','$qty','$unit_price','$vat_group','$tax_amount')";
                $getResultsCopy = $objAzure->runQuery($azure_connection, $sql_azure_details_copy);
            }
        }
            $sql_details = "INSERT into trn_financemodule_purchaseline (Transaction_type, Purchase_Order_No, Revised_Count, Line_No, Item_Code, Narration, Item_Category, Item_SubCategory, Quantity, Unit_Cost, VAT_Prod_Posting_Group, NBT_Amount, deliveryStatus, deliveryDate) VALUES ('$transactionType','$poString','$reviseNo','$line_no','$item_code','$description','$item_category','$item_subcategory','$qty','$unit_price','$vat_group', '$tax_amount','$successDetails', NOW())";
            $result_details = $db->RunQuery($sql_details);
            if($transactionType == 'PO_EditStart'){
                $sql_details_copy = "INSERT into trn_financemodule_purchaseline (Transaction_type, Purchase_Order_No, Revised_Count, Line_No, Item_Code, Narration, Item_Category, Item_SubCategory, Quantity, Unit_Cost, VAT_Prod_Posting_Group, NBT_Amount, deliveryStatus, deliveryDate) VALUES ('PO_EditStart','$poString','$reviseNo','$line_no','$item_code','$description','$item_category','$item_subcategory','$qty','$unit_price','$vat_group', '$tax_amount','$successDetails', NOW())";
                $result_details_copy = $db->RunQuery($sql_details_copy);
            }

    }



    $sql_azure_header = "INSERT INTO PurchaseHeader (Transaction_type, Document_Type, Purchase_Order_No, Revised_Count, Vendor_Code, Vendor_Name, Order_Date, Posting_Date, Revised_Date, Currency_Code, Payment_Term, Payment_Method, Shipment_Mode, Delivery_Date, Location_Code, Narration, PO_Type, No_of_Lines) VALUES ('$transactionType', 'Order', '$poString','$reviseNo', '$supplier_code', '$supplier','$orderDate', '$posting_date', '$revised_date','$currency', '$payTerm', '$payMode', '$shipmentMode','$deliveryDate','$locationId',CONVERT(varchar(250),'$narration'), '$po_type', '$i')";
        if($azure_connection) {
        $getResults = $objAzure->runQuery($azure_connection, $sql_azure_header);
        if($getResults != FALSE){
            $successHeader = 1;
        }
        if($transactionType == 'PO_EditStart'){
            $sql_azure_header_copy = "INSERT INTO PurchaseHeader (Transaction_type, Document_Type, Purchase_Order_No, Revised_Count, Vendor_Code, Vendor_Name, Order_Date, Posting_Date, Revised_Date, Currency_Code, Payment_Term, Payment_Method, Shipment_Mode, Delivery_Date, Location_Code, Narration, PO_Type, No_of_Lines) VALUES ('PO_New', 'Order', '$poString','$reviseNo', '$supplier_code', '$supplier','$orderDate', '$posting_date', '$revised_date','$currency', '$payTerm', '$payMode', '$shipmentMode','$deliveryDate','$locationId',CONVERT(varchar(250),'$narration'), '$po_type', '$i')";
            $getResultsHeaderCopy = $objAzure->runQuery($azure_connection, $sql_azure_header_copy);
        }
    }

    $sql = "INSERT INTO trn_financemodule_purchaseheader (Transaction_type, Document_Type, Purchase_Order_No, Revised_Count, Vendor_Code, Vendor_Name, Order_Date, Posting_Date, Revised_Date, Currency_Code, Payment_Term, Payment_Method, Shipment_Mode, Delivery_Date, Location_Code, Narration, PO_Type, No_of_Lines, version_no, deliveryStatus, deliveryDate) VALUES ('$transactionType', 'Order', '$poString', '$reviseNo','$supplier_code', '$supplier', '$orderDate', '$posting_date','$revised_date','$currency', '$payTerm', '$payMode', '$shipmentMode','$deliveryDate','$locationId','$narration_sql', '$po_type', '$i', '$version_no','$successHeader', NOW())";
    $new_result_header = $db->RunQuery($sql);
    if($transactionType == 'PO_EditStart'){
        $sql_copy = "INSERT INTO trn_financemodule_purchaseheader (Transaction_type, Document_Type, Purchase_Order_No, Revised_Count, Vendor_Code, Vendor_Name, Order_Date, Posting_Date, Revised_Date, Currency_Code, Payment_Term, Payment_Method, Shipment_Mode, Delivery_Date, Location_Code, Narration, PO_Type, No_of_Lines, version_no, deliveryStatus, deliveryDate) VALUES ('PO_New', 'Order', '$poString', '$reviseNo','$supplier_code', '$supplier', '$orderDate', '$posting_date','$revised_date','$currency', '$payTerm', '$payMode', '$shipmentMode','$deliveryDate','$locationId','$narration_sql', '$po_type', '$i', '$version_no','$successHeader', NOW())";
        $new_result_header_copy = $db->RunQuery($sql_copy);
    }


    }

function getTotalReturnToSupplyAmount($grnArray,$itemId,$db){
    $ret_sup_amount = 0;
    for($i=0;$i<sizeof($grnArray);$i++){
        $grn = explode('/',$grnArray[$i]);
        $sql = "SELECT
                    (ware_grndetails.dblGrnQty - IFNULL(SUM(RSD.dblQty),0))  AS balance
                    FROM
                        ware_grndetails
                    INNER JOIN ware_returntosupplierheader RSH ON RSH.intGrnNo = ware_grndetails.intGrnNo
                    AND RSH.intGrnYear = ware_grndetails.intGrnYear AND RSH.intStatus = '1'
                    INNER JOIN ware_returntosupplierdetails RSD ON RSD.intReturnNo = RSH.intReturnNo
                    AND RSD.intReturnYear = RSD.intReturnYear
                    AND RSD.intItemId = ware_grndetails.intItemId
                    WHERE
                        ware_grndetails.intGrnNo = '$grn[0]'
                    AND ware_grndetails.intGrnYear = '$grn[1]'
                    AND ware_grndetails.intItemId = '$itemId'";
        $result = $db->RunQuery($sql);
        $row = mysqli_fetch_array($result);
        $ret_sup_amount += $row['balance'];
    }
    return $ret_sup_amount;
}

function calculateNewTaxAmount($taxId, $quantity, $unitPrice, $discount, $db){
    $amount=$quantity*$unitPrice;
    $amount = $amount * (100 - $discount) / 100;
    $sql = "SELECT
				mst_financetaxgroup.intId,
				mst_financetaxgroup.strProcess
				FROM
				mst_financetaxgroup
				WHERE
				mst_financetaxgroup.intId =  '$taxId'";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $taxProcess = $row['strProcess'];
    $arrTax = explode("/", $taxProcess);
    $operation = '';

    //(3)capture calculation method(isolated/inclusive/exclusive) and the array 'jsonTaxCode' contains only tax ids
    $jsonTaxCode = "[ ";
    if (count($arrTax) == 1)//If strProcess contain only one value (Example $row['strProcess'] = 5)
    {
        $operation = 'Isolated';
        $jsonTaxCode .= '{ "taxId":"' . $taxProcess . '"}';
    } else if (count($arrTax) > 1) //IF there are multiple tax ids in the tax group(example strProcess = {4/Inclusive/7})
    {
        $operation = $arrTax[1];//this should be inclusive/exclusive
        for ($i = 0; $i < count($arrTax); $i = $i + 2) {
            $jsonTaxCode .= '{ "taxId":"' . $arrTax[$i] . '"},'; //create a json array geting 0 and 2 value from the array
        }

        $jsonTaxCode = $jsonTaxCode . substr(0, count($jsonTaxCode) - 1);
    }
    $jsonTaxCode .= " ]";
    $taxCodes = json_decode($jsonTaxCode, true);

    //(4)get tax rates for all tax ids in the tax group ( tax array)
    if (count($taxCodes) != 0) {
        foreach ($taxCodes as $taxCode) {
            //get tax rates from the mst_financetaxisolated table sending taxId to the callTaxValue function which got by json array
            $codeValues[] = callTaxValue($taxCode['taxId'],$db);
        }
    }
    if (count($codeValues) > 1) // if there are more than one tax types in the tax group (this can be identified from mst_financetaxgroup.strProcess field)
    {
        if ($operation == 'Inclusive') {
            //step 1: po amount will be multiplied by the first tax rate
            //step 2 : result of the step 1(po amount+tax ammount), will be multiplied by the second tax rate.
            $firstVal = ($amount * $codeValues[0]) / 100;
            $withTaxVal = $firstVal + ((($amount + $firstVal) * $codeValues[1]) / 100);
            $val1 = ($amount * $codeValues[0]) / 100;
            $val2 = ((($amount + $firstVal) * $codeValues[1]) / 100);
        } else if ($operation == 'Exclusive') {
            //get the summation of the two tax rates and multiply it from the amount
            $withTaxVal = ($amount * ($codeValues[0] + $codeValues[1])) / 100;
            $val1 = ($amount * $codeValues[0]) / 100;
            $val2 = ($amount * $codeValues[1]) / 100;
        }
    }
    else if(count($codeValues) == 1 && $operation == 'Isolated')//there is only one tax type for the tax group
    {
        $withTaxVal = ($amount*$codeValues[0])/100;
        $val1 = ($amount*$codeValues[0])/100;
    }

    return $withTaxVal;
}

function callTaxValue($taxId, $db)
{
    $sql = "SELECT
				mst_financetaxisolated.intId,
				mst_financetaxisolated.strCode,
				mst_financetaxisolated.dblRate
				FROM
				mst_financetaxisolated
				WHERE
				mst_financetaxisolated.intId = '$taxId'
				";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $taxVal = $row['dblRate'];
    return $taxVal;
}

echo "JOB DONE";
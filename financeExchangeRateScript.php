<?php
session_start();
ini_set('max_execution_time', 10000000);
$backwardseperator = "../../../../";
$mainPath 	= $_SESSION['mainPath'];
$userId 	= $_SESSION['userId'];
$curr_date = date('Y-m-d');
$savedStatus	= true;
$savedMsg		= '';
include "dataAccess/Connector.php";

//=============================================================
$bsCurr 	= 2;

//print_r($arrCompany);

//---------exchange rate entering time margin--------------

//--------------------------------------------------------

//=============================================================
/////////// exchange rate insert part /////////////////////

    $sql = "SELECT intCurrencyId, dtmDate,dblBuying,dblSellingRate,strCode  FROM mst_financeexchangerate left join mst_financecurrency
on mst_financecurrency.intId = mst_financeexchangerate.intCurrencyId WHERE dtmDate like '%2019%' AND intCompanyId =1"; // intCurrencyId = '$id' AND
    $result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{

    $currency = $row['intCurrencyId'];
    $currencyCode = $row['strCode'];
    $sellRate = $row['dblSellingRate'];
    $buyRate = $row['dblBuying'];
    $avgRate = round((($sellRate + $buyRate) / 2), 2);
    $date = $row['dtmDate'];
    $userId = $row['intCreator'];
//    var_dump($avgRate);
    sendDetailsToFinanceModule($date, $currencyCode, $currency, $avgRate, $userId, $bsCurr);

}



/////////// exchange rate update part /////////////////////



/////////// exchange rate delete part /////////////////////
/*	else if($requestType=='delete')
	{
		// no needed for this process
	}*/
function setOldDateExchange($days_between,$newFrmDate,$maxDate,$userId,$companyId)
{
    global $db;
    global $savedStatus;
    global $savedMsg;

    $savedStatus = true;
    for($t=1;$t<$days_between;$t++)
    {
        $date1 = date_create($newFrmDate);
        date_add($date1, date_interval_create_from_date_string('1 days'));
        $nextDt = date_format($date1, 'Y-m-d');

        $sqlIns = "INSERT INTO mst_financeexchangerate
					(
					intCurrencyId,dtmDate,
					intCompanyId,dblSellingRate,
					dblBuying,dblExcAvgRate,intCreator,
					dtmCreateDate
					)
					(SELECT intCurrencyId, '".$nextDt."', 
					intCompanyId, dblSellingRate, 
					dblBuying,ROUND(((dblSellingRate+dblBuying)/2),2),$userId, 
					NOW()
					FROM 
					mst_financeexchangerate 
					WHERE dtmDate='$maxDate' and
					intCompanyId='$companyId'
					)";

        $result = $db->RunQuery2($sqlIns);
        if($savedStatus && !$result)
        {
            $savedStatus	= false;
            $savedMsg		= $db->errormsg;
        }

        $newFrmDate = $nextDt;
    }

}

function sendDetailsToFinanceModule($date,$currencyCode,$currencyId,$exchangeRate,$userId,$baseCurrency){

    $configs = include('config/zillionConfig.php');
    $url = $configs['URL'].'IntExRate';
    $allowed_types = $configs['ALLOWED_TYPES'];
    if(!in_array($currencyCode,$allowed_types)){
        return;
    }
    $code = $currencyCode;
    if($currencyCode == 'EURO'){
        $code = 'Eur';
    }
    else if($currencyCode == 'LKR'){
        $code = '';
    }
    $data = array('Starting_Date' => $date, 'Currency_Code' => $code, 'Relational_Exch_Rate_Amount' => strval($exchangeRate));
    $data_json = json_encode($data);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    $auth_header = "Authorization: ".$configs["TYPE"].' '.base64_encode($configs['AUTH_USERNAME'].':'.$configs['AUTH_PASSWORD']);
    $header_arr = array("Content-Type: application/json", $auth_header);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header_arr);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $successStatus = 0;
    $response = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    var_dump($httpcode);
    if($httpcode == '201'){
        $successStatus = 1; //delivered
    }
    curl_close($ch);
    insertAPIRecordsToTable($currencyId,$date,$exchangeRate,$successStatus,$userId,$baseCurrency);

}

function insertAPIRecordsToTable($currencyId,$date,$exchangeRate,$status,$userId,$baseCurrency){

    global $db;
    $sql = "SELECT intCurrencyId, date, exchange_rate FROM trn_financemodule_exrate
							WHERE
							`intCurrencyId`='$currencyId' AND `date`='$date' LIMIT 1";

    $resultNew = $db->RunQuery2($sql);
    while($row=mysqli_fetch_array($resultNew))
    {
        $ex_rate = $row['exchange_rate'];
    }

    if (mysqli_num_rows($resultNew) == 0) {
        $sql_insert = " INSERT INTO `trn_financemodule_exrate` (
                    `intCurrencyId`,
                    `date`,
                    `exchange_rate`,
                    `intStatus`,
                    `createdDate`,
                    `createdBy`,
                    `baseCurrency`
                     )
                     VALUES
                    (
                        '$currencyId',
                        '$date',
                        '$exchangeRate',
                        '$status',
                         NOW(),
                        '$userId',
                        '$baseCurrency'
                    )";
    } else if($ex_rate != $exchangeRate && $status){
        $sql_insert = "UPDATE `trn_financemodule_exrate` SET 	exchange_rate	='$exchangeRate',
																intStatus		='$status',
																modifiedDate    = NOW(),
																modifiedBy		= '$userId'																				
							WHERE (`intCurrencyId`='$currencyId' AND `date`='$date')";
    }
    $result = $db->RunQuery2($sql_insert);
}
?>
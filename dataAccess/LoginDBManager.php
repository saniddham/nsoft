<?php
################################
####  Roshan Perera  ###########
####  copyright 2011 ###########
####  Database Handling Module #
################################
include 'db_authentication.php';



class LoginDBManager
{

/*private $server = '192.168.1.3';
private $userName = 'root';
private $password = 'soft';
private $database = 'nsoft';
private $HRdatabase = 'qpay';
private $SRdatabase = 'ipad_sample';
*/
//    private $server             = '107.23.102.162';   //192.168.1.3
//    private $userName           = 'root';
//    private $password           = '4adsGGarrerGGaa';
//    private $database           = 'nsoft';
//    private $HRdatabase         = 'qpay';
//    private $SRdatabase         = 'ipad_sample';
	/*private $server 		= 'masterdb.nimawum.com';//192.168.1.3
	private $userName 		= 'nsoft_user';
	private $password 		= 'nsoft@1234';
	private $database 		= 'nsoft';
	private $HRdatabase 	= 'qpay';
	private $SRdatabase 	= 'ipad_sample';*/

/*private $server = '127.0.0.1';
private $userName = 'root';
private $password = '123';
private $database = 'nsoft';
private $HRdatabase = 'qpay';
private $SRdatabase = 'ipad_sample'; */

private $con = null;

	public function getServer()
	{
		return $this->server;
	}
	
	public function getUser()
	{
		return $this->userName;
	}
	public function getPassword()
	{
		return $this->password;
	}
	public function getDatabase()
	{
		return $this->database;
	}
	public function getHRDatabase()
	{
		return $this->HRdatabase;
	}
	public function getSampleRequisitionDatabase()
	{
		return $this->SRdatabase;
	}
	public function OpenConnection()
	{
		$this->con = mysqli_connect($this->server, $this->userName, $this->password);
		if (!$this->con)
		{
		  die('Could not connect: ' . mysqli_error());
		}
	}
	
	function sql($SQL)
	{
		$this->OpenConnection();
		mysqli_select_db($this->con, $this->database);
		$result = mysqli_query($SQL);
		$this->CloseConnection();	
			
		return $result;
	}
	
	function ExecuteQuery($SQL)
	{
		$this->OpenConnection();
		mysqli_select_db($this->database,  $this->con);
		$result = mysqli_query($SQL);
		$this->CloseConnection();
	}
	
	
	function CheckRecordAvailability($SQL)
	{
		$this->OpenConnection();
		mysqli_select_db($this->database,  $this->con);
		$result = mysqli_query($SQL);
		$this->CloseConnection();
		while($row = mysqli_fetch_array($result))
  		{
  			return true;
  		}
  		return false;		
	}
	
	function CloseConnection()
	{
		mysqli_close($this->con);		
	}

    function RunQuery($SQL)
    {
        $this->OpenConnection();
        mysqli_select_db($this->con, $this->database);
        $result = mysqli_query($this->con, $SQL);
        $this->CloseConnection();

        return $result;
    }
	
	
}



?>

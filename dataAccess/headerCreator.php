<?php 
################################
####  Roshan Perera  ###########
####  copyright 2011 ###########
####  Header Handling Module #
################################
include "HeaderConnector.php";
include "permissionProvider.php";

?>

<script src="<?php echo $backwardseperator;?>../libraries/SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
<link href="<?php echo $backwardseperator;?>../libraries/SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $backwardseperator;?>../css/mainstyle.css" rel="stylesheet" type="text/css" />

<script src="<?php echo $backwardseperator;?>../libraries/jquery/jquery.js"></script>
<script src="<?php echo $backwardseperator;?>../libraries/javascript/script.js"></script>
<script src="<?php echo $backwardseperator;?>../libraries/jquery/jquery-ui.js"></script>
	
<script type="text/javascript">
	$(document).ready(function() {
		$('#butSave').keypress(function(e) {
			if(e.keyCode==13)
				$('#butSave').trigger('click');
		});
		$('#butDelete').keypress(function(e) {
			if(e.keyCode==13)
				$('#butDelete').trigger('click');
		});
		$('#butReport').keypress(function(e) {
			if(e.keyCode==13)
				$('#butReport').trigger('click');
		});
		$('#butNew').keypress(function(e) {
			if(e.keyCode==13)
				$('#butNew').trigger('click');
		});
		
		/////////////////////
		
		$('.cboCountry').change(function() {
			
				//alert(this.form.id);
		});
		
	});
</script>

<style type="text/css">
<!--
.style2 {
	font-size: 10px;
	font-weight: bold;
	font-family: Verdana;
	color: #FFFFFF;
}

.style3 {
	font-family: Verdana;
	font-size: 10px;
	font-weight: bold;
}
-->
</style>
<script type="text/javascript" >
var xmlHttp;
function createXMLHttpRequestHeader() 
{
    if (window.ActiveXObject) 
    {
        xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    else if (window.XMLHttpRequest) 
    {
        xmlHttp = new XMLHttpRequest();
    }
}

function changeFactoryId(id)
{
    createXMLHttpRequestHeader();
    xmlHttp.onreadystatechange = changeCompanyRequest;
    xmlHttp.open("GET", '<?php echo $backwardseperator;?>HeaderDB.php?id=changeCompany&factoryId=' + id, true);
    xmlHttp.send(null); 
	
}
function changeCompanyRequest()
{
	if(xmlHttp.readyState == 4) 
    {
        if(xmlHttp.status == 200) 
        { 
			var text = xmlHttp.responseText;
			window.location.reload()
		}
	}
}

  function additem(e,obj)
  {
   evt = e || window.event;
  
   if(evt && evt.keyCode == 13)
   {
     var url = "headerCreator-db-set.php?type=addNewItem&intParentId="+obj.id+'&strName='+obj.value;
     $.ajax({url:url,async:false});
	 document.location.reload();
    return false; 
   }
   else
    return true; 
  }
  
  
  function removeThisMenu(obj)
  {
	 var url = "headerCreator-db-set.php?type=deleteMenu&menuId="+obj.parentNode.id;
     $.ajax({url:url,async:false});
	 document.location.reload(); 
   }
   
   function setOrderByNo(obj)
   {
		var url = "headerCreator-db-set.php?type=setOrderBy&menuId="+obj.parentNode.id+"&orderNo="+obj.value;
     	$.ajax({url:url,async:false});	   
   }
   
   function loadFolder(value,id)
   {
	    	var path1 = '../'+document.getElementById('cboFolder1').value ;
			var path2 = document.getElementById('cboFolder2').value ;
			var path3 = document.getElementById('cboFolder3').value ;
			var path4 = document.getElementById('cboFolder4').value ;
			var path5 = document.getElementById('cboFolder5').value ;
		    //var path6 = document.getElementById('cboFolder6').value ;
			
		var path = path1;
			if(path2!='')
				path += "/"+path2;
			if(path3!='')
				path += "/"+path3;
			if(path4!='')
				path += "/"+path4;
			if(path5!='')
				path += "/"+path5;
			
		var url = "headerCreator-db-set.php?type=loadFolder&path="+URLEncode(path);
     	document.getElementById('cboFolder'+(id+1)).innerHTML =  $.ajax({url:url,async:false}).responseText;
		
		if(path=='')
			path = "../";
		var url = "headerCreator-db-set.php?type=loadFiles&path="+URLEncode(path);
     	document.getElementById('tblFiles').innerHTML =  $.ajax({url:url,async:false}).responseText; 	   
   }
   
   function setGridHeader(id,name)
   {
		document.getElementById('lblMenuId').childNodes[0].nodeValue = id;
		document.getElementById('lblMenuName').childNodes[0].nodeValue= name;
   }
   function updateUrl(obj)
   {
		var id = (document.getElementById('lblMenuId').childNodes[0].nodeValue)  ;
		 
		 var path1 = document.getElementById('cboFolder1').value ;
			var path2 = document.getElementById('cboFolder2').value ;
			var path3 = document.getElementById('cboFolder3').value ;
			var path4 = document.getElementById('cboFolder4').value ;
			var path5 = document.getElementById('cboFolder5').value ;
		    //var path6 = document.getElementById('cboFolder6').value ;
			
		var path = path1;
			if(path2!='')
				path += "/"+path2;
			if(path3!='')
				path += "/"+path3;
			if(path4!='')
				path += "/"+path4;
			if(path5!='')
				path += "/"+path5;
		
	 	if(path!='')
			path = path + "/";
		var path = path + obj.parentNode.parentNode.cells[1].childNodes[0].nodeValue;
		var url = "headerCreator-db-set.php?type=updateURL&path="+URLEncode(path)+"&id="+id;
		$.ajax({url:url,async:false});
   }
   function updateUrlNull()
   {
	   var id = (document.getElementById('lblMenuId').childNodes[0].nodeValue).trim()  ;
		 var url = "headerCreator-db-set.php?type=updateURL&path="+URLEncode('#')+"&id="+id;
		$.ajax({url:url,async:false});  
   }
</script>
<link href="../css/mainstyle.css" rel="stylesheet" type="text/css">
<body oncontextmenu="return false;">
<table width="100%" border="0" cellpadding="0" cellspacing="0">
<tr bgcolor="#FFFFFF">
		<td width="12"></td>
      <td width="940" height="44">
	  <table width="100%" cellpadding="0" cellspacing="0">
          <tr>
            <td width="244" rowspan="2">&nbsp;</td>
            <td width="80" rowspan="2">&nbsp;</td>
            <td width="657" rowspan="2" class="tophead" id="companyName"></td>
            <td width="94" class="tophead" id="companyName"><span class="normalfnth2B">Welcome <span class="normalfnth2">
            <?php 
		
		//$SQL ="select useraccounts.intUserID, useraccounts.UserName from useraccounts, role, userpermission where useraccounts.intUserID =" . $_SESSION["userId"] . " and role.RoleID = userpermission.RoleID and userpermission.intUserID = useraccounts.intUserID and role.RoleName = 'Administration'";
		$SQL = "select Name from useraccounts where intUserID  =" . $_SESSION["userId"] ;
		$result = $dbheader-RunQuery($SQL);
	
		while($row = mysqli_fetch_array($result))
		{
			echo $row["Name"];
		}
		?>
!</span></span></td>
            <td width="100" class="tophead" id="companyName"><div   align="right"></div></td>
        </tr>
          <tr>
            <td colspan="2"  class="normalfnth2B" id="companyName"><div align="left"></div></td>
          </tr>
      </table>    </td>
  </tr>
    <tr>
	<td align="center" bgcolor="#f6a813" style="width:30px;"><a href="../main.php" title="Home"><img src="<?php echo $backwardseperator;?>../images/house.png" alt="Home" width="16" height="16" border="0" /></a></td>
      <td bgcolor="#f6a813">
	  
	  <ul id="MenuBar1" class="MenuBarHorizontal">
      <?php 
	  
	 // include "Connector.php";
	  $sql1 = "SELECT  * from menus WHERE menus.intParentId =  '0' order by intOrderBy";			////////////////main menus ///////////////////////////////
	  $result1  = $dbheader-RunQuery($sql1);
	  while($row1=mysqli_fetch_array($result1))
	  {
		  $mainid = $row1['intId'];
	  ?>
		<li><a onClick="setGridHeader('<?php echo $mainid; ?>','<?php echo $row1['strName']; ?>');" id="<?php echo $mainid ?>" class="style2" ><input value="<?php echo $row1['intOrderBy'] ?>" onBlur="setOrderByNo(this);" type="text" style="width:20px"/><img  onClick="removeThisMenu(this);" style="border:none" src="../images/del.png"/><?php echo $row1['strName']; ?></a>
			
            <!-- start second part -->
            <ul>
            <?php 
			  $sql2 = "SELECT  * from menus WHERE menus.intParentId =  '$mainid' order by intOrderBy";
			  $result2  = $dbheader-RunQuery($sql2);
			  $rowcount2 = mysqli_num_rows($result2);
			  
			  while($row2=mysqli_fetch_array($result2))
			  {
				  $subid1 =  $row2['intId'];
			?>
            	<li><a onClick="setGridHeader('<?php echo $subid1; ?>','<?php echo $row2['strName']; ?>');"  id="<?php echo $subid1 ?>" class="style3" ><input value="<?php echo $row2['intOrderBy'] ?>" onBlur="setOrderByNo(this);" type="text" style="width:20px"/><img    onClick="removeThisMenu(this);" style="border:none" src="../images/del.png"/><?php echo $row2['strName']; ?></a>
                
                            <!-- start third part -->
                        <ul>
                        <?php 
                          $sql3 = "SELECT  * from menus WHERE menus.intParentId =  '$subid1' order by intOrderBy";
                          $result3  = $dbheader-RunQuery($sql3);
                          $rowcount3 = mysqli_num_rows($result3);
                          
                          while($row3=mysqli_fetch_array($result3))
                          {
                              $subid2 =  $row3['intId'];
                        ?>
                            <li><a onClick="setGridHeader('<?php echo $subid2; ?>','<?php echo $row3['strName']; ?>');" id="<?php echo $subid2 ?>" class="style3" ><input value="<?php echo $row3['intOrderBy'] ?>" onBlur="setOrderByNo(this);" type="text" style="width:20px"/><img  onClick="removeThisMenu(this);" style="border:none" src="../images/del.png"/><?php echo $row3['strName']; ?></a>
                            
                             <!-- start fourth part -->
                        	<ul>
                        <?php 
                          $sql4 = "SELECT  * from menus WHERE menus.intParentId =  '$subid2' order by intOrderBy";
                          $result4  = $dbheader-RunQuery($sql4);
                          $rowcount4 = mysqli_num_rows($result4);
                          
                          while($row4=mysqli_fetch_array($result4))
                          {
                              $subid3 =  $row4['intId'];
                        ?>
                            <li><a onClick="setGridHeader('<?php echo $subid3; ?>','<?php echo $row4['strName']; ?>');" id="<?php echo $subid3 ?>" class="style3" ><input value="<?php echo $row4['intOrderBy'] ?>" onBlur="setOrderByNo(this);" type="text" style="width:20px"/><img  onClick="removeThisMenu(this);" style="border:none" src="../images/del.png"/><?php echo $row4['strName']; ?></a>
                            
                            
                       
                            
                            </li>
                            
                         <?php
                          }
                         ?>
                         <li><input onKeyPress="additem(event,this);" id="<?php echo $subid2; ?>" type="text"/>
                        </ul>
                         <!-- end fourth part --> 
                       
                            
                            </li>
                            
                         <?php
                          }
                         ?>
                          <li><input onKeyPress="additem(event,this);" id="<?php echo $subid1; ?>"  type="text"/>
                        </ul>
                         <!-- end third part -->  
           
                
                </li>
                
             <?php
			  }
			 ?>
              <li><input onKeyPress="additem(event,this);" id="<?php echo $mainid; ?>"  type="text"/>
           </ul>
             <!-- end second part -->  
             
        </li>
        
       <?php
	   	}
	   ?>
        <li><input id="0" onKeyPress="additem(event,this);" type="text"/>
      </ul>
	  </td>
  </tr>
</table>
<table width="100%">
	<tr>
    	<td width="24%" height="97">&nbsp;</td>
        <td width="50%"><table width="100%" border="0" class="tableBorder2">
          <tr >
            <td align="center" valign="middle" class="mainHeading2"><span id="lblMenuId">&nbsp;</span></td>
            <td align="center" valign="middle" class="mainHeading2"><span id="lblMenuName">&nbsp;</span></td>
          </tr>
          <tr>
            <td width="29%">&nbsp;</td>
            <td width="71%"><select onChange="loadFolder(this.value,1);" name="cboFolder1" class="txtbox" style="width:200px" id="cboFolder1" >
              <option></option>
              <?php
            	filesInDir('../');

				function filesInDir($tdir)
				{
					
					$dirs = scandir($tdir);
					foreach($dirs as $file)
					{
						if (($file == '.')||($file == '..'))
						{
						}
						elseif (is_dir($tdir.'/'.$file))
						{
							echo "<option value=\"$file\">$file</option>";
						}
					}
				}
				//echo $_SERVER['PHP_SELF'];
				?>
            </select></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><select onChange="loadFolder(this.value,2);" name="cboFolder2" class="txtbox" style="width:200px" id="cboFolder2" ></select></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><select onChange="loadFolder(this.value,3);" name="cboFolder3" class="txtbox" style="width:200px" id="cboFolder3" >
            </select></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><select onChange="loadFolder(this.value,4);" name="cboFolder4" class="txtbox" style="width:200px" id="cboFolder4" >
            </select></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><select onChange="loadFolder(this.value,5);" name="cboFolder5" class="txtbox" style="width:200px" id="cboFolder5" >
            </select></td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td><select onChange="loadFolder(this.value,5);" name="cboFolder6" class="txtbox" style="width:200px" id="cboFolder6" >
            </select>
            <input type="reset" name="Reset" id="button" value="Set Null" onClick="updateUrlNull();"></td>
          </tr>
          <tr>
            <td colspan="2">
            <div class="tableFooter"  align="center" style="height:350px;overflow:scroll" >
            <table id="tblFiles" name="tblFiles" width="421"  border="0" align="center">
              <tr>
                <td width="47" bgcolor="#9999CC" class="TitleN2white">Select</td>
                <td width="364" bgcolor="#9999CC" class="TitleN2white">File Name</td>
              </tr>
              <tr>
                <td align="center"><input type="radio" class="txtbox"/></td>
                <td class="normalfnt2">asdfadf</td>
              </tr>
            </table>
            </div>
            </td>
          </tr>
</table></td>
        <td width="26%">&nbsp;</td>
    </tr>
</table>
</body>
<script type="text/javascript">
<!--
	var MenuBar1 = new Spry.Widget.MenuBar("MenuBar1", {imgDown:"../libraries/SpryAssets/SpryMenuBarDownHover.gif", imgRight:"../libraries/SpryAssets/SpryMenuBarRightHover.gif"});
	loadFolder('',1);
//-->
</script>

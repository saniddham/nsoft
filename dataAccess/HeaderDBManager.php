<?php
################################
####  Roshan Perera  ###########
####  copyright 2011 ###########
####  Database Handling Module #
################################
class HeaderDBManager
{

private $server = '';
private $userName = '';
private $password = '';
private $database = '';


private $con = null;

	public function SetConnectionString($Server, $UserName, $Password, $Database)
	{
		$this->server = $Server;
		$this->userName = $UserName;
		$this->password = $Password;
		$this->database = $Database;		
	}
	
	public function OpenConnection()
	{
		$this->con = mysqli_connect($this->server, $this->userName, $this->password);
		if (!$this->con)
		{
		  die($password . 'Could not connect: ' . mysqli_error());
		}
	}
	
	public function sql($SQL)
	{
		$this->OpenConnection();
		mysqli_select_db($this->database,  $this->con);
		$result = mysqli_query($SQL);
		$this->CloseConnection();		
		return $result;
	}
	
	public function ExecuteQuery($SQL)
	{
		$this->OpenConnection();
		mysqli_select_db($this->database,  $this->con);
		$result = mysqli_query($SQL);
		$this->CloseConnection();
		return $result;
	}
	
	
	public function CheckRecordAvailability($SQL)
	{
		$this->OpenConnection();
		mysqli_select_db($this->database,  $this->con);
		$result = mysqli_query($SQL);
		$this->CloseConnection();
		while($row = mysqli_fetch_array($result))
  		{
  			return true;
  		}
  		return false;		
	}
	
	public function CloseConnection()
	{
		mysqli_close($this->con);
	}
}


?>
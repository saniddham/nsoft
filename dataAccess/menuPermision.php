<?php
################################
####  Roshan Perera  ###########
####  copyright 2011 ###########
####  Database Handling Module #
################################
session_start();
$backwardseperator = "../";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>User Permision</title>
<link href="../css/mainstyle.css" rel="stylesheet" type="text/css" />
<script src="../commonscript/script.js" type="text/javascript"></script>
<script type="text/javascript">
	function updateUserPermision(obj)
	{
		var id = obj.id;
		var userId = document.getElementById('cboUser').value;
		var value = 0 ;
		if(obj.checked)value = 1;
		
		var url = "menuPermision-db.php?type=updatePermision&id="+id+"&value="+value+"&userId="+userId;
		$.ajax({url:url,async:false});
	}
	function reloadPage ()
	{
		document.fromIssue.submit();
	}
</script>
</head>

<body >
<?php
	include "Connector.php";	
	$invoiceNo = $_GET['invoiceNo'];
	
	$intShopId = $_POST['cboCustomer'];
	
	//get Header Details
	$sql = "SELECT
			issueheader.intCustomer,
			issueheader.dtInvoiceDate,
			issueheader.dtDeliveryDate
			FROM issueheader
			WHERE
			issueheader.intInvoiceNo =  '$invoiceNo'";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	
	$intCustomer = $row['intCustomer'];
	$dtInvoiceDate = $row['dtInvoiceDate'];
	$dtDeliveryDate = $row['dtDeliveryDate'];
	$dblDiscount = $row['dblDiscount'];
	
?>
<form name="fromIssue" id="fromIssue" action="menuPermision.php" method="post" >
  <tr>
    <td><?php include $backwardseperator.'Header.php'; ?></td>
  </tr>
   <div>
	<div align="center">
		<div class="trans_layoutXL" style="width:960px">
			<div class="trans_text">User-Menu Permisions</div>
<table width="950" border="0" align="center" bgcolor="#FFFFFF">

  <tr>
    <td><table width="100%" border="0">
      <tr>
        <td width="14%" height="22" class="normalfnt">User</td>
        <td width="35%" class="normalfnt"><select name="cboUser" onchange="reloadPage();" class="txtbox" id="cboUser" style="width:150px" >
          <?php
		  
				$SQL = 	"SELECT
						sys_users.intUserId,
						sys_users.strUserName
						FROM `sys_users`
						WHERE
						sys_users.intStatus =  '1'
						ORDER BY
						sys_users.strUserName ASC

						";
				
				$intUser = $_POST['cboUser'];
				
				$result = $db->RunQuery($SQL);
				echo "<option value=\"". "" ."\">" . "" ."</option>";
				while($row = mysqli_fetch_array($result))
				{
					if($intUser==$row["intUserId"])
						echo "<option selected value=\"". $row["intUserId"] ."\">" . trim($row["strUserName"]) ."</option>" ;
					else
						echo "<option value=\"". $row["intUserId"] ."\">" . trim($row["strUserName"]) ."</option>" ;
				}
			?>
        </select></td>
        <td width="11%" class="normalfnt">&nbsp;</td>
        <td width="40%" class="normalfnt">&nbsp;</td>
      </tr>
      <tr>
        <td height="22" class="normalfnt">Main Modules</td>
        <td class="normalfnt"><select name="cboMainModules" onchange="reloadPage();" class="txtbox" id="cboMainModules" style="width:150px" >
          <?php
				$SQL = 	"SELECT
							menus.intId,
							menus.strName
							FROM menus
							WHERE
							menus.intParentId =  '0' AND
							menus.intStatus =  '1'
							GROUP BY
							menus.intOrderBy
						";
				$intMainModules = $_POST['cboMainModules'];
				$result = $db->RunQuery($SQL);
				echo "<option ></option>" ;
				while($row = mysqli_fetch_array($result))
				{
					if($intMainModules==$row["intId"])
						echo "<option selected value=\"". $row["intId"] ."\">" . trim($row["strName"]) ."</option>" ;
					else
						echo "<option value=\"". $row["intId"] ."\">" . trim($row["strName"]) ."</option>" ;
				}
			?>
        </select></td>
        <td class="normalfnt">&nbsp;</td>
        <td class="normalfnt">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td  >&nbsp;</td>
        </tr>
      <tr>
        <td class="normalfnt">
          <table width="900" cellpadding="0" cellspacing="1" id="tblMainGrn">
			<?php 
			if($intUser=='') die();
			if($intMainModules!='')
				$xx = " and intId=$intMainModules";
/*				echo $sql = "SELECT
							menus.intId,
							menus.strName,
							ifnull(menupermision.intUserId,0) as permision
							FROM
							menus
							left Join menupermision ON menus.intId = menupermision.intMenuId
							WHERE
							menus.intParentId =  '0' AND
							menus.intStatus =  '1' AND
							(menupermision.intUserId =  '$intUser'  or isnull(menupermision.intUserId )) $xx
							order BY
							menus.intOrderBy
						";*/
				$sql = "SELECT
						m.intId,
						m.strName,
						 ifnull((select p.intUserId from menupermision as p where p.intMenuId = m.intId and p.intUserId='$intUser' ),0) as permision
						FROM
						menus as m
						WHERE
						m.intStatus =  '1' and
						m.intParentId =  '0' 
						$xx
						ORDER BY
						m.intOrderBy ASC
						";
						
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					$mainId = $row["intId"];
			?>
              <tr bgcolor="#E1F5FF" class="normalfnt" >
                  <td width="3%" align="center" height="20" ><input onclick="updateUserPermision(this);" <?php if($row['permision']) echo "checked"; ?> id="<?php echo $row["intId"]; ?>" name="checkbox" type="checkbox" class="txtbox"  /></td>
                  
                  <td width="97%"  class="normalfnBLD1"><?php echo $row['strName']; ?></td>
                  </tr>
              <tr class="normalfnt" >
                <td  colspan="2" align="center" >
                <table width="100%" border="0">
                <?php 
							
/*				$sql2 = "SELECT
							menus.intId,
							menus.strName,
							ifnull(menupermision.intUserId,0) as permision
							FROM
							menus
							left Join menupermision ON menus.intId = menupermision.intMenuId
							WHERE
							menus.intParentId =  '$mainId' AND
							menus.intStatus =  '1' AND
							(menupermision.intUserId =  '$intUser'  or isnull(menupermision.intUserId )) 
							order BY
							menus.intOrderBy
						";*/
				$sql2 = "SELECT
						m.intId,
						m.strName,
						 ifnull((select p.intUserId from menupermision as p where p.intMenuId = m.intId and p.intUserId='$intUser' ),0) as permision
						FROM
						menus as m
						WHERE
						m.intStatus =  '1' and
						m.intParentId =  '$mainId' 
						$xx
						ORDER BY
						m.intOrderBy ASC
						";
				$result2 = $db->RunQuery($sql2);
				while($row2=mysqli_fetch_array($result2))
				{
					$subId1 = $row2["intId"];
				?>
                  <tr>
                    <td width="3%">&nbsp;</td>
                    <td width="3%"><input onclick="updateUserPermision(this);" <?php if($row2['permision']) echo "checked"; ?> id="<?php echo $row2["intId"]; ?>" name="<?php echo $row2["intId"]; ?>" type="checkbox" class="txtbox"  /></td>
                    <td width="94%"><?php echo $row2['strName']; ?></td>
                  </tr>
                  <tr>
                    <td colspan="3"><table width="100%" border="0">
                      <?php 
							
/*				$sql3 = "SELECT
							menus.intId,
							menus.strName,
							ifnull(menupermision.intUserId,0) as permision
							FROM
							menus
							left Join menupermision ON menus.intId = menupermision.intMenuId
							WHERE
							menus.intParentId =  '$subId1' AND
							menus.intStatus =  '1' AND
							(menupermision.intUserId =  '$intUser'  or isnull(menupermision.intUserId )) 
							order BY
							menus.intOrderBy
						";*/
				$sql3 = "SELECT
						m.intId,
						m.strName,
						 ifnull((select p.intUserId from menupermision as p where p.intMenuId = m.intId and p.intUserId='$intUser' ),0) as permision
						FROM
						menus as m
						WHERE
						m.intStatus =  '1' and
						m.intParentId =  '$subId1' 
						$xx
						ORDER BY
						m.intOrderBy ASC
						";
				$result3 = $db->RunQuery($sql3);
				while($row3=mysqli_fetch_array($result3))
				{
					$subId2 = $row3["intId"];
				?>
                      <tr>
                        <td width="6%">&nbsp;</td>
                        <td width="3%"><input onclick="updateUserPermision(this);" <?php if($row3['permision']) echo "checked"; ?> id="<?php echo $row3["intId"]; ?>" name="<?php echo $row3["intId"]; ?>" type="checkbox" class="txtbox"  /></td>
                        <td width="91%"><?php echo $row3['strName']; ?></td>
                      </tr>
                      <tr>
                        <td colspan="3"><table width="100%" border="0">
                          <?php 
							
/*				$sql4 = "SELECT
							menus.intId,
							menus.strName,
							ifnull(menupermision.intUserId,0) as permision
							FROM
							menus
							left Join menupermision ON menus.intId = menupermision.intMenuId
							WHERE
							menus.intParentId =  '$subId2' AND
							menus.intStatus =  '1' AND
							(menupermision.intUserId =  '$intUser'  or isnull(menupermision.intUserId )) 
							order BY
							menus.intOrderBy
						";*/
				$sql4 = "SELECT
						m.intId,
						m.strName,
						 ifnull((select p.intUserId from menupermision as p where p.intMenuId = m.intId and p.intUserId='$intUser' ),0) as permision
						FROM
						menus as m
						WHERE
						m.intStatus =  '1' and
						m.intParentId =  '$subId2' 
						$xx
						ORDER BY
						m.intOrderBy ASC
						";
				$result4 = $db->RunQuery($sql4);
				while($row4=mysqli_fetch_array($result4))
				{
				?>
                          <tr>
                            <td width="9%">&nbsp;</td>
                            <td width="3%"><input onclick="updateUserPermision(this);" <?php if($row4['permision']) echo "checked"; ?> id="<?php echo $row4["intId"]; ?>" name="<?php echo $row4["intId"]; ?>" type="checkbox" class="txtbox"  /></td>
                            <td width="88%"><?php echo $row4['strName']; ?></td>
                          </tr>
                          <?php
				}
			 ?>
                        </table></td>
                        </tr>
                      <?php
				}
			 ?>
                    </table></td>
                    </tr>
              <?php
				}
			 ?>
                </table></td>
                </tr>
                  
             <?php
				}
			 ?>
          </table>
          </td>
        </tr>
        <tr>
        	<td>&nbsp;</td>
        </tr>
    </table></td>
  </tr>
  <tr>
    <td><table width="100%" cellpadding="0" cellspacing="0" >
      <tr>
        <td width="12%" height="29">&nbsp;</td>
        <td align="center">&nbsp;</td>
        <td width="12%">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</div>
</div>
</div>
</form>
</body>
</html>



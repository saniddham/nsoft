<?php
/*
 * @NSOFT TEAM
 * @Version 3.0
 * @Package Database
 */
//ini_set('display_errors',0);
include_once "db_authentication.php"; // include authentication class

$db_authentication	= new db_authentication();// create object

class DBManager2{
	/* 
	 * Create variables for credentials to MySQL database
	 * The variables have been declared as private. This
	 * means that they will only be available with the 
	 * Database class
	 */
	private $db_host 	= "";  // Change as required
	private $db_user 	= "";  // Change as required
	private $db_pass 	= "";  // Change as required
	private $db_name 	= "";	// Change as required
	private $db_hr		= "";
	private $db_ipad	= "";
	private $Auth		= "";
	/*
	 * Extra variables that are required by other function such as boolean con variable
	 */
	private $con = false; // Check to see if the connection is active
	private $result = array(); // Any results from a query will be stored here
    private $myQuery = "";// used for debugging process with SQL return
    private $numResults = "";// used for returning the number of rows
	private $field_array = "";
	private $mysqli_error = "";
	private $errormsg	= "";
	
	public $memcache = '';
    
	
	//INITIATE DETAILS TO VARIABLES
	function __construct($login=0)
	{//$login=false;
		global $db_authentication;
		$this->Auth		= $_SESSION['AUTH']; // user login authentication true or false
		if(!$this->Auth)
		{
			if(!$login){
				include_once 'login.php';
				die();
			}
		}
		$auth_arr		= $db_authentication->getAuthenticationDetails(); // authentication detail array
		foreach($auth_arr as $k => $v)
		{
          $this->$k = $v;
  		}
			$_SESSION["Server"] 		= $this->db_host;
			$_SESSION["UserName"] 		= $this->db_user;
			$_SESSION["Password"] 		= $this->db_pass;
			$_SESSION["Database"] 		= $this->db_name;
			$_SESSION["HRDatabase"] 	= $this->db_hr;	
			$_SESSION["SRDatabese"] 	= $this->db_ipad;	
	}
	
/*	public function SetConnectionString($Server, $UserName, $Password, $Database,$loginUserId)
	{
		$this->db_host = $Server;
		$this->db_user = $UserName;
		$this->db_pass = $Password;
		$this->db_name = $Database;
		$this->loginUserId = $loginUserId;		
	}*/
	
	// Function to make connection to database
	public function connect(){
			if(!$this->con){
				$myconn = mysqli_connect($this->db_host,$this->db_user,$this->db_pass, $this->db_name);  // mysqli_connect() with variables defined at the start of Database class
				if($myconn){
				    // Credentials have been pass through mysqli_connect() now select the database
                    $this->con = $myconn;
                    return true;  // Connection has been made return TRUE
				} else {
					array_push($this->result,mysqli_error($this->con));$this->mysqli_error = mysqli_error($this->con);
					throw new Exception('Server connecting error...');
					//return false; // Problem connecting return FALSE
				}  
			}else{  
				return true; // Connection has already been made return TRUE 
			}	
	}
	
	// Function to disconnect from the database
    public function disconnect(){
    	// If there is a connection to the database
    	if($this->con){
			
			//rollback queries
			@mysqli_query('rollback');
			
    		// We have found a connection, try to close it
    		if(@mysqli_close($this->con)){
				
    			// We have successfully closed the connection, set the connection variable to false
    			$this->con = false;
				// Return true tjat we have closed the connection
				return true;
			}else{
				// We could not close the connection, return false
				return false;
			}
		}
    }
	
	public function begin()
	{
		// If there is a connection to the database
    	if($this->con){
    		@mysqli_query('begin');
		}	
	}
	
	public function commit()
	{
		// If there is a connection to the database
    	if($this->con){
    		@mysqli_query('commit');
		}	
	}
	
	public function rollback()
	{
		// If there is a connection to the database
    	if($this->con){
    		@mysqli_query('rollback');
			$this->disconnect();
		}	
	}
	
	public function sql($sql){
		//add username to query
		$userString = " /*".$_SESSION['systemUserName']."*/ ";
		
		$this->result = array();
		$query = @mysqli_query($this-> con,$sql);
        $this->myQuery = $userString.$sql; // Pass back the SQL
		if($query){
			// If the query returns >= 1 assign the number of rows to numResults
			$this->numResults = mysqli_num_rows($query);
			// Loop through the query results by the number of rows returned
			for($i = 0; $i < $this->numResults; $i++){
				$r = mysqli_fetch_array($query);
               	$key = array_keys($r);
               	for($x = 0; $x < count($key); $x++){
               		// Sanitizes keys so only alphavalues are allowed
                   	if(!is_int($key[$x])){
                   		if(mysqli_num_rows($query) >= 1){
                   			$this->result[$i][$key[$x]] = $r[$key[$x]];
						}else{
							$this->result = null;
						}
					}
				}
			} 
			return $this->result;  // Query was successful
		}else{
			array_push($this->result,mysqli_error($this->con));$this->mysqli_error = mysqli_error($this->con);
			return false; // No rows where returned
		}
	}
	
	// run query for special queries
	// Function to insert into the database
    public function RunQuery($sql){
		//add username to query
		$userString = " /*".$_SESSION['systemUserName']."*/ ";
		
		$this->myQuery = $userString.$sql;
		$query = @mysqli_query($this-> con,$sql);
		if($query)
		{
			$this->numResults = mysqli_num_rows($query);
			return $query;
		}
		else
		{
			array_push($this->result,mysqli_error($this->con),$this->myQuery);
			$this->mysqli_error = mysqli_error($this->con);
			$this->errormsg = mysqli_error($this->con);
			return false;
		}
		
			/*if($query){
				// If the query returns >= 1 assign the number of rows to numResults
				$this->numResults = mysqli_num_rows($query);
				// Loop through the query results by the number of rows returned
				for($i = 0; $i < $this->numResults; $i++){
					$r = mysqli_fetch_array($query);
                	$key = array_keys($r);
                	for($x = 0; $x < count($key); $x++){
                		// Sanitizes keys so only alphavalues are allowed
                    	if(!is_int($key[$x])){
                    		if(mysqli_num_rows($query) >= 1){
                    			$this->result[$i][$key[$x]] = $r[$key[$x]];
							}else{
								$this->result = null;
							}
						}
					}
				}
				return true; // Query was successful
			}else{
				array_push($this->result,mysqli_error(),$this->myQuery);
				return false; // No rows where returned
			}*/
    }
	
	// Function to SELECT from the database
	public function select($table, $rows = '*', $join = null, $where = null, $order = null, $limit = null){
		// Create query from the variables passed to the function
		
		$this->result= array();
		$q = 'SELECT '.$rows.' FROM '.$table;
		if($join != null){
			$q .= ' '.$join;
		}
        if($where != null){
        	$q .= ' WHERE '.$where;
		}
        if($order != null){
            $q .= ' ORDER BY '.$order;
		}
        if($limit != null){
            $q .= ' LIMIT '.$limit;
        }
		
		//add username to query
		$userString = " /*".$_SESSION['systemUserName']."*/ ";
		
		//echo $q;
        $this->myQuery = $userString.$q;// Pass back the SQL
		// Check to see if the table exists
        if($this->tableExists($table)){
        	// The table exists, run the query
        	$query = @mysqli_query($this-> con,$q);
			if($query){
				$this->numResults = mysqli_num_rows($query);
				return $query;
			}
			else{
				array_push($this->result,mysqli_error($this->con),$this->myQuery);
				$this->mysqli_error = mysqli_error($this->con);
				return false;
			}
			
			
			
			
			/*if($query){
				// If the query returns >= 1 assign the number of rows to numResults
				$this->numResults = mysqli_num_rows($query);
				// Loop through the query results by the number of rows returned
				for($i = 0; $i < $this->numResults; $i++){
					$r = mysqli_fetch_array($query);
                	$key = array_keys($r);
                	for($x = 0; $x < count($key); $x++){
                		// Sanitizes keys so only alphavalues are allowed
                    	if(!is_int($key[$x])){
                    		if(mysqli_num_rows($query) >= 1){
                    			$this->result[$i][$key[$x]] = $r[$key[$x]];
							}else{
								$this->result = null;
							}
						}
					}
				}
				return true; // Query was successful
			}else{
				array_push($this->result,mysqli_error(),$this->myQuery);
				return false; // No rows where returned
			}*/
      	}else{
      		return false; // Table does not exist
    	}
    }
	
	// Function to insert into the database
    public function insert($table,$params=array()){
		$this->result= array();
		//echo $table;
    	// Check to see if the table exists
		 $params=$this->replace_fields($params);
    	 if($this->tableExists($table)){
    	 	$sql='INSERT INTO `'.$table.'` (`'.implode('`, `',array_keys($params)).'`) VALUES (' . implode(',', $params) . ')';
            $this->myQuery = $sql; // Pass back the SQL
			$this->saveQueries($table,$sql,1);//insert to queries
            // Make the query to insert to the database
            if($ins = @mysqli_query($this-> con,$sql)){
				
				$msg = (mysqli_affected_rows($this->con)>0?"Save successfully.":'No changes to save.');
				$this->result = array('status'=>1,'type'=>'pass','msg'=>$msg,'effectRows'=>mysqli_affected_rows($this->con),'insertId'=>mysqli_affected_rows($this->con),'mysqlError'=>mysqli_error($this->con),'sql'=>$this->myQuery);
            	
                return true; // The data has been inserted
            }else{
            	
				$this->result = array('status'=>0,'type'=>'fail','msg'=>mysqli_error($this->con),'effectRows'=>mysqli_affected_rows($this->con),'insertId'=>mysqli_affected_rows($this->con),'mysqlError'=>mysqli_error($this->con),'sql'=>$this->myQuery);
				
				$this->mysqli_error = mysqli_error($this->con);
                return false; // The data has not been inserted
            }
        }else{
        	return false; // Table does not exist
        }
    }
	
	//Function to delete table or row(s) from database
    public function delete($table,$where = null){
		$this->result= array();
		
		// Check to see if table exists
    	 if($this->tableExists($table)){
    	 	// The table exists check to see if we are deleting rows or table
    	 	if($where == null){
                $delete = 'DELETE '.$table; // Create query to delete table
            }else{
                $delete = 'DELETE FROM '.$table.' $deleteWHERE '.$where; // Create query to delete rows
            }
			$this->myQuery = $delete; // Pass back the SQL
			$this->saveQueries($table,$delete,3);//insert to queries
            // Submit query to database
            if($del = @mysqli_query()){
            	
				$msg = (mysqli_affected_rows($this->con)>0?"Deleted successfully.":'Already deleted.');
				$this->result = array('status'=>1,'type'=>'pass','msg'=>$msg,'effectRows'=>mysqli_affected_rows($this->con),'insertId'=>mysqli_affected_rows($this->con),'mysqlError'=>mysqli_error($this->con),'sql'=>$this->myQuery);
				
                return true; // The query exectued correctly
            }else{
            	
				$this->result = array('status'=>0,'type'=>'fail','msg'=>mysqli_error($this->con),'effectRows'=>mysqli_affected_rows($this->con),'insertId'=>mysqli_affected_rows($this->con),'mysqlError'=>mysqli_error($this->con),'sql'=>$this->myQuery);
				
				$this->mysqli_error = mysqli_error($this->con);
               	return false; // The query did not execute correctly
            }
        }else{
            return false; // The table does not exist
        }
    }
	
	// Function to update row in database
    public function update($table,$params=array(),$where){
		$this->result= array();
		// Check to see if table exists
		$params=$this->replace_fields($params);
		
    	if($this->tableExists($table)){
    		// Create Array to hold all the columns to update
            $args=array();
			foreach($params as $field=>$value){
				// Seperate each column out with it's corresponding value
				$args[]=$field.'='.$value;
			}
			//print_r($args);
			// Create the query
			$sql='UPDATE '.$table.' SET '.implode(',',$args).' WHERE '.$where;
			// Make query to database
            $this->myQuery = $sql; // Pass back the SQL
			$this->saveQueries($table,$sql,2);//insert to queries
            if($query = @mysqli_query($this-> con, $sql)){
				
            	$msg = (mysqli_affected_rows($this->con)>0?"Updated successfully.":'No changes to update.');
				$this->result = array('status'=>1,'type'=>'pass','msg'=>$msg,'effectRows'=>mysqli_affected_rows($this->con),'insertId'=>mysqli_affected_rows($this->con),'mysqlError'=>mysqli_error($this->con),'sql'=>$this->myQuery);
				
            	return true; // Update has been successful
            }else{
            	
				$this->result = array('status'=>0,'type'=>'fail','msg'=>mysqli_error($this->con),'effectRows'=>mysqli_affected_rows($this->con),'insertId'=>mysqli_affected_rows($this->con),'mysqlError'=>mysqli_error($this->con),'sql'=>$this->myQuery);
				
                return false; // Update has not been successful
            }
        }else{
            return false; // The table does not exist
        }
    }
	
	// Function to update row in database
    public function upgrade($table,$params=array(),$where){
		$this->result= array();
		// Check to see if table exists
		//$params=$this->replace_fields($params); removed  temporty
    	if($this->tableExists($table)){
    		// Create Array to hold all the columns to update
            $args=array();
			foreach($params as $field=>$value){
				// Seperate each column out with it's corresponding value
				$args[]=$field.'='.$field.''.$value.'';
			}
			// Create the query
			$sql='UPDATE '.$table.' SET '.implode(',',$args).' WHERE '.$where;
			// Make query to database
            $this->myQuery = $sql; // Pass back the SQL
			$this->saveQueries($table,$sql,2);//insert to queries
            if($query = @mysqli_query($this-> con, $sql)){
				
            	$msg = (mysqli_affected_rows($this->con)>0?"Upgraded successfully.":'No changes to upgrade.');
				$this->result = array('status'=>1,'type'=>'pass','msg'=>$msg,'effectRows'=>mysqli_affected_rows($this->con),'insertId'=>mysqli_affected_rows($this->con),'mysqlError'=>mysqli_error($this->con),'sql'=>$this->myQuery);
				
            	return true; // Update has been successful
            }else{
            	
				$this->result = array('status'=>0,'type'=>'fail','msg'=>mysqli_error($this->con),'effectRows'=>mysqli_affected_rows($this->con),'insertId'=>mysqli_affected_rows($this->con),'mysqlError'=>mysqli_error($this->con),'sql'=>$this->myQuery);
				
                return false; // Update has not been successful
            }
        }else{
            return false; // The table does not exist
        }
    }
	
	// Private function to check if table exists for use with queries
	private function tableExists($table){
		
		$tablesInDb = @mysqli_query($this->con, 'SHOW TABLES FROM '.$this->db_name.' LIKE "'.$table.'"');
        if($tablesInDb){
			
        	if(mysqli_num_rows($tablesInDb)==1){
                return true; // The table exists
            }else{
				
            	array_push($this->result,$table." does not exist in this database");
				$this->mysqli_error = mysqli_error($this->con);
                throw new Exception($table." does not exist in this database");
            }
        }
    }
	
	// Public function to return the data to the user
    public function getResult(){
        $val = $this->result;
        $this->result = array();
        return $val;
    }

    //Pass the SQL back for debugging
    public function getSql(){
        $val = $this->myQuery;
        $this->myQuery = array();
        return $val;
    }

    //Pass the number of rows back
    public function numRows(){
        $val = $this->numResults;
        $this->numResults = array();
        return $val;
    }

	//get mysql error
	public function getMysqlError(){
		$val = $this->mysqli_error;
        $this->mysqli_error = array();
        return $val;
	}
    // Escape your string
    public function escapeString($data){
		return addslashes($data);
    }
	
	public function field_array_set($field_array){
		$this->field_array = $field_array;	
		//print_r($data_array);
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	public function replace_fields($data){		
		$data2 = $data;
		$field_array = $this->field_array;
		foreach ( $data2 as $k=>&$v )
		{
			if(strtoupper($v) != 'NULL')
				$v = "'".$this->escapeString($v)."'";
			if($k != $field_array[$k] && ($field_array[$k]!=''))
			{
				$data2[$field_array[$k]] = $v;
				unset($data2[$k]);
			}
		}
		return $data2;
	}
	private function saveQueries($table,$query,$operation){
		global $_SESSION;
		global $_SERVER;
		
		$ip = "";
		if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
		{
			$ip=$_SERVER['HTTP_CLIENT_IP'];
		}
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
		{
			$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else
		{
			$ip=$_SERVER['REMOTE_ADDR'];
		}
		if ($_SERVER['HTTP_X_FORWARD_FOR']) 
		{
			$ip = $_SERVER['HTTP_X_FORWARD_FOR'];
		} 
		else 
		{
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		//$ip = $REMOTE_ADDR;
		$userCode =  $_SESSION["userId"];
		
		$sql  = "INSERT INTO queries 
				(
				program, 
				form,
				tableName, 
				operation, 
				sqlStatement, 
				userID, 
				IP
				)
				VALUES
				( 
				'".$_SERVER['PHP_SELF']."', 
				'',
				'".$table."', 
				'".$operation."', 
				'".$query."', 
				'".$userCode."', 
				'".$ip."'
				);";
		
		mysqli_query($this-> con, $sql);
	}
} 

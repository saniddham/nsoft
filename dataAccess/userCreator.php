<?php
session_start();
$backwardseperator = "";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add New User</title>
<link href="css/erpstyle.css" rel="stylesheet" type="text/css" />


<link href="css/erpstyle.css" rel="stylesheet" type="text/css" />

<link href="css/erpstyle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="javascript/calendar/theme.css" />
<script src="javascript/calendar/calendar.js" type="text/javascript"></script>
<script src="javascript/calendar/calendar-en.js" type="text/javascript"></script>
<script src="javascript/script.js" type="text/javascript"></script>

<script type="text/javascript">

var oldLink = null;
// code to change the active stylesheet
function setActiveStyleSheet(link, title) {
  var i, a, main;
  for(i=0; (a = document.getElementsByTagName("link")[i]); i++) {
    if(a.getAttribute("rel").indexOf("style") != -1 && a.getAttribute("title")) {
      a.disabled = true;
      if(a.getAttribute("title") == title) a.disabled = false;
    }
  }
  if (oldLink) oldLink.style.fontWeight = 'normal';
  oldLink = link;
  link.style.fontWeight = 'bold';
  return false;
}

// This function gets called when the end-user clicks on some date.
function selected(cal, date) {
  cal.sel.value = date; // just update the date in the input field.
  if (cal.dateClicked && (cal.sel.id == "sel1" || cal.sel.id == "sel3"))
    // if we add this call we close the calendar on single-click.
    // just to exemplify both cases, we are using this only for the 1st
    // and the 3rd field, while 2nd and 4th will still require double-click.
    cal.callCloseHandler();
}

// And this gets called when the end-user clicks on the _selected_ date,
// or clicks on the "Close" button.  It just hides the calendar without
// destroying it.
function closeHandler(cal) {
  cal.hide();                        // hide the calendar
//  cal.destroy();
  _dynarch_popupCalendar = null;
}

// This function shows the calendar under the element having the given id.
// It takes care of catching "mousedown" signals on document and hiding the
// calendar if the click was outside.
function showCalendar(id, format, showsTime, showsOtherMonths) {
  var el = document.getElementById(id);
  if (_dynarch_popupCalendar != null) {
    // we already have some calendar created
    _dynarch_popupCalendar.hide();                 // so we hide it first.
  } else {
    // first-time call, create the calendar.
    var cal = new Calendar(1, null, selected, closeHandler);
    // uncomment the following line to hide the week numbers
    // cal.weekNumbers = false;
    if (typeof showsTime == "string") {
      cal.showsTime = true;
      cal.time24 = (showsTime == "24");
    }
    if (showsOtherMonths) {
      cal.showsOtherMonths = true;
    }
    _dynarch_popupCalendar = cal;                  // remember it in the global var
    cal.setRange(1900, 2070);        // min/max year allowed.
    cal.create();
  }
  _dynarch_popupCalendar.setDateFormat(format);    // set the specified date format
  _dynarch_popupCalendar.parseDate(el.value);      // try to parse the text in field
  _dynarch_popupCalendar.sel = el;                 // inform it what input field we use

  // the reference element that we pass to showAtElement is the button that
  // triggers the calendar.  In this example we align the calendar bottom-right
  // to the button.
  _dynarch_popupCalendar.showAtElement(el.nextSibling, "Br");        // show the calendar

  return false;
}

var MINUTE = 60 * 1000;
var HOUR = 60 * MINUTE;
var DAY = 24 * HOUR;
var WEEK = 7 * DAY;

// If this handler returns true then the "date" given as
// parameter will be disabled.  In this example we enable
// only days within a range of 10 days from the current
// date.
// You can use the functions date.getFullYear() -- returns the year
// as 4 digit number, date.getMonth() -- returns the month as 0..11,
// and date.getDate() -- returns the date of the month as 1..31, to
// make heavy calculations here.  However, beware that this function
// should be very fast, as it is called for each day in a month when
// the calendar is (re)constructed.
function isDisabled(date) {
  var today = new Date();
  return (Math.abs(date.getTime() - today.getTime()) / DAY) > 10;
}

function flatSelected(cal, date) {
  var el = document.getElementById("preview");
  el.innerHTML = date;
}

function showFlatCalendar() {
  var parent = document.getElementById("display");

  // construct a calendar giving only the "selected" handler.
  var cal = new Calendar(0, null, flatSelected);

  // hide week numbers
  cal.weekNumbers = false;

  // We want some dates to be disabled; see function isDisabled above
  cal.setDisabledHandler(isDisabled);
  cal.setDateFormat("%A, %B %e");

  // this call must be the last as it might use data initialized above; if
  // we specify a parent, as opposite to the "showCalendar" function above,
  // then we create a flat calendar -- not popup.  Hidden, though, but...
  cal.create(parent);

  // ... we can show it here.
  cal.show();
  

}

</script>
<script type="text/javascript">
	function save(obj)
	{
		document.frmUserCreator.submit();
		
		/*var id = obj.id;
		var userId = document.getElementById('cboUser').value;
		var userId = document.getElementById('txtUserName').value;
		var userId = document.getElementById('txtPassword').value;
		var value = 0 ;
		if(obj.checked)value = 1;
		
		var url = "menuPermision-db.php?type=updatePermision&id="+id+"&value="+value+"&userId="+userId;
		$.ajax({url:url,async:false});*/
	}
	function newPage(obj)
	{
		document.location.href = "userCreator.php";
	}
	function reloadPage()
	{
		document.frmUserCreator.submit();	
	}
</script>
</head>

<body >
<?php
	include "Connector.php";	
	
?>
<form name="frmUserCreator" id="frmUserCreator" action="userCreator.php" method="post" >
  <tr>
    <td><?php include $backwardseperator.'Header.php'; ?></td>
  </tr>
  
  <?php
  		$intUser = $_POST['cboUser'];
		$user = $_POST['txtUserName'];
		
		$pass = $_POST['txtPassword'];
		if($pass !='password')
		{
			$pass = md5($pass);
			$pass1 =" Password='$pass'," ;
		}
		else
		{
			$pass = md5($pass);
			$pass1 = '';
		}
			
		$status = 0;
		if($_POST['chkStatus']=='on')
			$status = 1;
		
	if(isset($_POST['butSave']))
	{
		
		if($intUser>0)
		{
			$sql = "UPDATE `useraccounts` SET `UserName`='$user', $pass1 `status`='$status' WHERE (`intUserID`='$intUser')  ";
		}
		else
		{
			$sql = "INSERT INTO `useraccounts` (`UserName`,`Password`,`Name`,`intCompanyID`,`status`) VALUES ('$user','$pass','$user','1','$status')";	
		}
			$result = $db->RunQuery($sql);
	}

  ?>
   <div>
	<div align="center">
		<div class="trans_layoutD" style="width:600px">
		  <div class="trans_text">Add New User</div>
<table width="615" border="0" align="center" bgcolor="#FFFFFF">

  <tr>
    <td><table width="100%" border="0">
      <tr>
        <td width="15%" height="22" class="normalfnt">&nbsp;</td>
        <td width="19%" height="22" class="normalfnt">User</td>
        <td width="47%" class="normalfnt"><select name="cboUser" onchange="reloadPage();" class="txtbox" id="cboUser" style="width:150px" >
          <?php
				$SQL = 	"SELECT
							useraccounts.intUserID,
							useraccounts.UserName,
							status
							FROM useraccounts
						";
				
				
				
				$result = $db->RunQuery($SQL);
				echo "<option value=\"". "" ."\">" . "" ."</option>";
				
				$strUserName 	= '';
				$password 		= '';
				$intActive 		= 0;
						
				while($row = mysqli_fetch_array($result))
				{
					if($intUser==$row["intUserID"])
					{
						$strUserName 	= $row['UserName'];
						$password		= 'password';
						$intActive		= $row['status'];
							if($intActive==1)
								$chk = "checked";

						echo "<option selected value=\"". $row["intUserID"] ."\">" . trim($row["UserName"]) ."</option>" ;
					}
					else
						echo "<option value=\"". $row["intUserID"] ."\">" . trim($row["UserName"]) ."</option>" ;
				}
			?>
        </select></td>
        <td width="19%" class="normalfnt">&nbsp;</td>
      </tr>
      <tr>
        <td height="21" class="normalfnt">&nbsp;</td>
        <td height="21" class="normalfnt">&nbsp;</td>
        <td class="normalfnt">&nbsp;</td>
        <td class="normalfnt">&nbsp;</td>
      </tr>
      <tr>
        <td height="22" class="normalfnt">&nbsp;</td>
        <td class="normalfnt">User Name</td>
        <td class="normalfnt"><input value="<?php echo $strUserName; ?>"  class="txtbox" style="width:200px" type="text" name="txtUserName" id="txtUserName" /></td>
        <td class="normalfnt">&nbsp;</td>
      </tr>
      <tr>
        <td height="22" class="normalfnt">&nbsp;</td>
        <td class="normalfnt">Password</td>
        <td class="normalfnt"><input value="<?php echo $password; ?>" name="txtPassword" type="password" class="txtbox" id="txtPassword" style="width:200px" /></td>
        <td class="normalfnt">&nbsp;</td>
      </tr>
      <tr>
        <td height="22" class="normalfnt">&nbsp;</td>
        <td class="normalfnt">Active</td>
        <td class="normalfnt"><input <?php echo $chk; ?> name="chkStatus" type="checkbox" class="txtbox" id="chkStatus" /></td>
        <td class="normalfnt">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" cellpadding="0" cellspacing="0" >
      <tr>
        <td width="12%" height="29">&nbsp;</td>
        <td align="center">
          <input onclick="newPage();" type="reset" name="butNew" id="butNew" value="New" />
          <input type="submit" name="butSave" id="butSave" value="Save" />
        <a href="main.php"><input type="submit" name="butClose" id="butClose" value="Close" /></a>
       </td>
        <td width="12%">&nbsp;</td>
      </tr>
    </table></td>
  </tr>
</table>
</div>
</div>
</div>
</form>
</body>
</html>



<?php
	################################
	####  Roshan Perera  ###########
	####  copyright 2011 ###########
	####  Database Handling Module #
	################################
	(session_id() == ''?session_start():'');
	date_default_timezone_set('Asia/Colombo');
	$mainPath = "../../../../../";
	$intUser  = "../../../../../";
	
	
	if(!isset($_SESSION["userId"]))
	{
		include $backwardseperator . 'login.php';
		die();
	}
	
	include "DBManager.php";
	$db =  new DBManager();
	$db->SetConnectionString($_SESSION["Server"],$_SESSION["UserName"],$_SESSION["Password"],$_SESSION["Database"],$_SESSION['userId']);
	
	function val($value)
	{
		if($value=='')
			$value = 0;
		else
			$value = (float)$value;	
		return $value;
	}
	function chk($value)
	{
		if($value=='true' || $value==true)
			return 1;
		else
			return 0;
	}
	function null($value)
	{
		return ($value?$value:'NULL');	
	}
	
	function getApproveLevel($name)
	{
		global $db;
		$sql = "SELECT
					sys_approvelevels.intApprovalLevel
				FROM sys_approvelevels
				WHERE
					sys_approvelevels.strName =  '$name'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return val($row['intApprovalLevel']);	
	}
	
	function getApproveLevel2($name)
	{
		global $db;
		$sql = "SELECT
					sys_approvelevels.intApprovalLevel
				FROM sys_approvelevels
				WHERE
					sys_approvelevels.strName =  '$name'
				";
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		return val($row['intApprovalLevel']);	
	}
	
	function quote($value)
	{
		return str_replace("'","\'",$value);	
	}
?>
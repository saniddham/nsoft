<?php

################################
####  Roshan Perera  ###########
####  copyright 2011 ###########
####  Database Handling Module #
################################
class DBManager
{

    private $server = '';
    private $userName = '';
    private $password = '';
    private $database = '';
    private $formName = '';
    private $loginUserId = '';
    public $errormsg = '';
    public $insertId = '';
    public $effectRows = '';
    private $con = null;

    public function ErrorMsg()
    {
        return $this->errormsg;
    }

    public function SetConnectionString($Server, $UserName, $Password, $Database, $loginUserId)
    {
        $this->server = $Server;
        $this->userName = $UserName;
        $this->password = $Password;
        $this->database = $Database;
        $this->loginUserId = $loginUserId;
    }

    public function OpenConnection()
    {
        $this->con = mysqli_connect($this->server, $this->userName, $this->password);

        if (!$this->con) {
            die($password . 'Could not connect: ' . mysqli_error());
        }
    }

    public function RunQuery2($SQL)
    {

        mysqli_select_db($this->con, $this->database);
        createQueryString($SQL, $this->formName, $this->con);
        mysqli_query($this->con,'SET @LOGIN_USER_ID=' . $this->loginUserId);
        $result = mysqli_query($this->con, $SQL);
        $id = mysqli_insert_id($this->con);
        $this->insertId = $id;
        $this->effectRows = mysqli_affected_rows($this->con);
        if (mysqli_error($this->con)) {
            if (mysqli_errno($this->con) == 1451)
                $this->errormsg = "Sorry!\nThis item has already used by one or more processes.";
            else
                $this->errormsg = mysqli_error($this->con);
        }
        //if(!$result)
        //failQuery($SQL,mysqli_errno(),mysqli_error());
        return $result;
    }

    public function RunQuery_S($SQL)
    {
        global $_SESSION;
        $db_ipad_sample = $_SESSION['SRDatabese'];
        mysqli_select_db($this->con, $db_ipad_sample);
        $result = createQueryString_S($SQL, $this->formName, $this->con);
        return $result;
    }

    //BEGIN - THIS FUNCTION WILL EXECUTE QUERY IN IPAD_SAMPLE DB AND SAME QUERIES IN IPAD_SAMPLE TABLE {
    public function RunQuery_C($SQL)
    {

        mysqli_select_db($this->con, $this->database);
        createQueryString_C($SQL, $this->formName);
        mysqli_query($this->con,'SET @LOGIN_USER_ID=' . $this->loginUserId);
        $result = mysqli_query($this->con, $SQL);
        $id = mysqli_insert_id($this->con);
        $this->insertId = $id;
        $this->effectRows = mysqli_affected_rows($this->con);
        if (mysqli_error($this->con)) {
            if (mysqli_errno($this->con) == 1451)
                $this->errormsg = "Sorry!\nThis item has already used by one or more processes.";
            else
                $this->errormsg = mysqli_error($this->con);
        }
        return $result;
    }

    //END }

    public function sql($SQL)
    {

        $this->OpenConnection();
        mysqli_select_db($this->con, $this->database);
        createQueryString($SQL, $this->formName, $this->con);
        mysqli_query($this->con,'SET @LOGIN_USER_ID=' . $this->loginUserId);
        $result = mysqli_query($this->con, $SQL);
        $id = mysqli_insert_id($this->con);
        $this->insertId = $id;
        $this->effectRows = mysqli_affected_rows($this->con);
        if (mysqli_error($this->con)) {
            if (mysqli_errno($this->con) == 1451)
                $this->errormsg = "Sorry!\nThis item has already used by one or more processes.";
            else
                $this->errormsg = mysqli_error($this->con);
        }
        //echo mysqli_error() . "<br>" . $SQL . "<br>";
        $this->CloseConnection();
        return $result;
    }

    public function RunQuery($SQL)
    {

        $this->OpenConnection();
        mysqli_select_db( $this->con, $this->database);
        createQueryString($SQL, $this->formName, $this->con);
        mysqli_query( $this->con,'SET @LOGIN_USER_ID=' . $this->loginUserId);
        $result = mysqli_query($this->con, $SQL);
        $id = mysqli_insert_id($this->con);
        $this->insertId = $id;
        $this->effectRows = mysqli_affected_rows($this->con);
        if (mysqli_error($this->con)) {
            if (mysqli_errno($this->con) == 1451)
                $this->errormsg = "Sorry!\nThis item has already used by one or more processes.";
            else
                $this->errormsg = mysqli_error($this->con);
        }
        //echo mysql_error() . "<br>" . $SQL . "<br>";
        $this->CloseConnection();
        return $result;
    }

    public function begin()
    {
        $this->OpenConnection();
        mysqli_query($this->con,'BEGIN');
    }

    public function commit()
    {
        mysqli_query($this->con,'COMMIT');
        $this->CloseConnection();
    }

    public function rollback()
    {
        mysqli_query($this->con,'ROLLBACK');
        $this->CloseConnection();
    }

    public function autoInsertNo($SQL)
    {

        $this->OpenConnection();
        mysqli_select_db($this->con, $this->database);
        createQueryString($SQL, $this->formName, $this->con);
        $result = mysqli_query($this->con, $SQL);
        $id = mysqli_insert_id($this->con);
        if (mysqli_error($this->con)) {
            if (mysqli_errno($this->con) == 1451)
                $this->errormsg = "Sorry!\nThis item has already used by one or more processes.";
            else
                $this->errormsg = mysqli_error($this->con);
        }
        $this->CloseConnection();
        return $id;
    }

    public function QueryCount($SQL)
    {

        $this->OpenConnection();
        mysqli_select_db($this->con, $this->database);
        createQueryString($SQL, $this->formName, $this->con);
        $result = mysqli_query($this->con, $SQL);
        $no = mysqli_num_rows($result);
        //if (mysqli_error())
        //echo mysqli_error() . "<br>" . $SQL . "<br>";
        $this->CloseConnection();
        return $no;
    }

    public function open($SQL)
    {
        mysqli_select_db($this->con, $this->database);
        createQueryString($SQL, $this->formName, $this->con);
        $result = mysqli_query($this->con, $SQL);
        if (!$result > 0) {
            failQuery($SQL);
        }
        return $result;
    }

    public function ExecuteQuery($SQL)
    {
        $this->OpenConnection();
        mysqli_select_db($this->con, $this->database);
        createQueryString($SQL, $this->formName, $this->con);
        $result = mysqli_query($this->con, $SQL);
        if (!$result > 0) {
            failQuery($SQL);
        }
        //if (mysqli_error())
        //echo mysqli_error() . "<br>" . $SQL . "<br>";
        if (mysqli_error($this->con))
            $result = false;
        $this->CloseConnection();
        return $result;
    }

    public function AutoIncrementExecuteQuery($SQL)
    {
        $id = -1;
        $this->OpenConnection();
        mysqli_select_db($this->con, $this->database);
        createQueryString($SQL, $this->formName, $this->con);
        $result = mysqli_query($this->con, $SQL);
        //echo mysqli_error();
        $id = mysqli_insert_id($this->con);
        $this->CloseConnection();
        return $id;
    }

    public function AffectedExecuteQuery($SQL)
    {
        $id = 0;
        $this->OpenConnection();
        mysqli_select_db($this->con, $this->database);
        createQueryString($SQL, $this->formName, $this->con);
        $result = mysqli_query($this->con, $SQL);
        //echo mysqli_error();
        $id = mysqli_affected_rows($this->con);
        $this->CloseConnection();
        return $id;
    }


    public function CheckRecordAvailability($SQL)
    {
        $this->OpenConnection();
        mysqli_select_db($this->con, $this->database);
        $result = mysqli_query($this->con, $SQL);
        $this->CloseConnection();
        while ($row = mysqli_fetch_array($result)) {
            return true;
        }
        return false;
    }

    public function CloseConnection()
    {
        mysqli_close($this->con);
    }

    public function escapeString($escapeString){
       return addslashes($escapeString);
    }

}

$dbManager = new DBManager();
function createQueryString($SQL, $formName, $link)
{
    $query = strtoupper("#" . $SQL);
    $query2 = "#" . $SQL;
    $intPos = stripos($query, "INSERT INTO");
    if ($intPos > 0) {
        $strTable = trim(substr($query2, $intPos + 11, strpos($query, "(") - ($intPos + 11)));
        saveQueries($strTable, 1, $SQL, $formName, $link);
    } else {
        $intPos = stripos($query, "UPDATE");
        if ($intPos > 0) {
            $strTable = trim(substr($query2, $intPos + 6, strpos($query, "SET") - ($intPos + 6)));
            saveQueries($strTable, 2, $SQL, $formName, $link);
        } else {
            $intPos = stripos($query, "DELETE FROM");
            if ($intPos > 0) {
                $strTable = trim(substr($query2, $intPos + 11, strpos($query, "WHERE") - ($intPos + 11)));
                saveQueries($strTable, 3, $SQL, $formName, $link);
            }
        }
    }
}

function createQueryString_S($SQL, $formName, $link)
{
    $query = strtoupper("#" . $SQL);
    $query2 = "#" . $SQL;
    $intPos = stripos($query, "INSERT INTO");
    if ($intPos > 0) {
        $strTable = trim(substr($query2, $intPos + 11, strpos($query, "(") - ($intPos + 11)));
        $result = saveQueries_S($strTable, 1, $SQL, $formName, $link);
    } else {
        $intPos = stripos($query, "UPDATE");
        if ($intPos > 0) {
            $strTable = trim(substr($query2, $intPos + 6, strpos($query, "SET") - ($intPos + 6)));
            $result = saveQueries_S($strTable, 2, $SQL, $formName, $link);
        } else {
            $intPos = stripos($query, "DELETE FROM");
            if ($intPos > 0) {
                $strTable = trim(substr($query2, $intPos + 11, strpos($query, "WHERE") - ($intPos + 11)));
                $result = saveQueries_S($strTable, 3, $SQL, $formName, $link);
            }
        }
    }
    return $result;
}

function createQueryString_C($SQL, $formName)
{
    $query = strtoupper("#" . $SQL);
    $query2 = "#" . $SQL;
    $intPos = stripos($query, "INSERT INTO");
    if ($intPos > 0) {
        $strTable = trim(substr($query2, $intPos + 11, strpos($query, "(") - ($intPos + 11)));
        saveQueries_C($strTable, 1, $SQL, $formName);
    } else {
        $intPos = stripos($query, "UPDATE");
        if ($intPos > 0) {
            $strTable = trim(substr($query2, $intPos + 6, strpos($query, "SET") - ($intPos + 6)));
            saveQueries_C($strTable, 2, $SQL, $formName);
        } else {
            $intPos = stripos($query, "DELETE FROM");
            if ($intPos > 0) {
                $strTable = trim(substr($query2, $intPos + 11, strpos($query, "WHERE") - ($intPos + 11)));
                saveQueries_C($strTable, 3, $SQL, $formName);
            }
        }
    }
}

function failQuery($SQL, $errorCode, $errDesc)
{
    global $dbManager;
    $dbManager->rollback();
    $query = strtoupper("#" . $SQL);
    $query2 = "#" . $SQL;
    $intPos = stripos($query, "INSERT INTO");
    if ($intPos > 0) {
        $strTable = trim(substr($query2, $intPos + 11, strpos($query, "(") - ($intPos + 11)));
        saveFailQueries($strTable, 1, $SQL, $errorCode, $errDesc);
    } else {
        $intPos = stripos($query, "UPDATE");
        if ($intPos > 0) {
            $strTable = trim(substr($query2, $intPos + 6, strpos($query, "SET") - ($intPos + 6)));
            saveFailQueries($strTable, 2, $SQL, $errorCode, $errDesc);
        } else {
            $intPos = stripos($query, "DELETE FROM");
            if ($intPos > 0) {
                $strTable = trim(substr($query2, $intPos + 11, strpos($query, "WHERE") - ($intPos + 11)));
                saveFailQueries($strTable, 3, $SQL, $errorCode, $errDesc);
            }
        }
    }
}

function saveFailQueries($tableName, $operation, $query, $errorCode, $errorDesc)
{
    global $_SESSION;
    global $_SERVER;

    $ip = "";
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    if ($_SERVER['HTTP_X_FORWARD_FOR']) {
        $ip = $_SERVER['HTTP_X_FORWARD_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    //$ip = $REMOTE_ADDR;
    $userCode = $_SESSION["userId"];


    $sql = "INSERT INTO queries_fail(tableName,operation,sqlStatement,userID, IP,errorCode,errorDescription) VALUES(\"" . $tableName . "\"," . $operation . ",\"" . $query . "\",\"" . $userCode . "\",\"" . $ip . "\",\"" . $errorCode . "\",\"" . $errorDesc . "\");";
    mysqli_query($this->con, $sql);
}

function saveQueries($tableName, $operation, $query, $formName, $link)
{
    global $_SESSION;
    global $_SERVER;

    //echo $formName;

    $ip = "";
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    if (isset($_SERVER['HTTP_X_FORWARD_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARD_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    //$ip = $REMOTE_ADDR;
    $userCode = $_SESSION["userId"];

    //$fname = $this->getFormName();
    $query = str_replace("'", "\'", $query);
    $query = str_replace('"', '\"', $query);

    $sql = "INSERT INTO queries(tableName,program,form,operation,sqlStatement,userID, IP) VALUES(\"" . $tableName . "\",\"" . $_SERVER['PHP_SELF'] . "\",\"" . $formName . "\"," . $operation . ",\"" . $query . "\",\"" . $userCode . "\",\"" . $ip . "\");";
     mysqli_query($link, $sql);
}

function saveQueries_C($tableName, $operation, $query, $formName)
{
    global $_SESSION;
    global $_SERVER;

    //echo $formName;

    $ip = "";
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    if ($_SERVER['HTTP_X_FORWARD_FOR']) {
        $ip = $_SERVER['HTTP_X_FORWARD_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    //$ip = $REMOTE_ADDR;
    $userCode = $_SESSION["userId"];

    //$fname = $this->getFormName();
    $query = str_replace("'", "\'", $query);
    $query = str_replace('"', '\"', $query);

    $sql = "INSERT INTO queries_sample(tableName,program,form,operation,sqlStatement,userID, IP) VALUES(\"" . $tableName . "\",\"" . $_SERVER['PHP_SELF'] . "\",\"" . $formName . "\"," . $operation . ",\"" . $query . "\",\"" . $userCode . "\",\"" . $ip . "\");";
    //echo $sql;
    mysqli_query($this->con, $sql);
}

function saveQueries_S($tableName, $operation, $query, $formName, $link)
{
    global $_SESSION;
    global $_SERVER;
    $db_ipad_sample = $_SESSION['SRDatabese'];

    //echo $formName;

    $ip = "";
    if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
    {
        $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
    {
        $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    if (isset($_SERVER['HTTP_X_FORWARD_FOR'])) {
        $ip = $_SERVER['HTTP_X_FORWARD_FOR'];
    } else {
        $ip = $_SERVER['REMOTE_ADDR'];
    }
    //$ip = $REMOTE_ADDR;
    $userCode = $_SESSION["userId"];

    //$fname = $this->getFormName();
    $query = str_replace("'", "\'", $query);
    $query = str_replace('"', '\"', $query);

    $sql = "INSERT INTO $db_ipad_sample.queries_sample(tableName,program,form,operation,sqlStatement,userID, IP) VALUES(\"" . $tableName . "\",\"" . $_SERVER['PHP_SELF'] . "\",\"" . $formName . "\"," . $operation . ",\"" . $query . "\",\"" . $userCode . "\",\"" . $ip . "\");";
    //echo $sql;
    $result = mysqli_query($link, $sql);
    return $result;
}


function getformname($val)
{
    switch ($val) {
        case 'branch':
            return 'Branch';
        case 'bank';
            return 'Bank';
        default:
            return $val;
    }

}

?>
<?php
session_start();
$mainPath = $_SESSION['mainPath'];
//echo $xprojectPath ;
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Invalid File Name</title>
<style type="text/css">
#apDiv1 {
	position:absolute;
	width:626px;
	height:373px;
	z-index:1;
	font-family: "Courier New", Courier, monospace;
	font-size: 24px;
	font-style: oblique;
	color: #F00;
}
#apDiv1 table tr td {
	font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	color: #333;
}
#apDiv1 table tr td span {
	font-weight: bold;
	font-size: 14px;
}
.tableBorder_allRound tr .compulsoryRed span {
	font-size: 36px;
}
</style>
<link href="<?php echo $mainPath; ?>css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../css/mainstyle.css" rel="stylesheet" type="text/css" />
</head>

<body>

  <table align="center" bgcolor="#FFFFFF"  style="top:40px;margin-top:50px"  width="500" border="0" class="tableBorder_allRound">
    <tr>
      <td width="15%">&nbsp;</td>
      <td width="85%">&nbsp;</td>
    </tr>
    <tr>
      <td align="center"><img src="<?php echo $mainPath;?>images/notPermision.png" /></td>
      <td align="left" class="compulsoryRed"><span >Permission Denied</span></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td class="normalfnt">Please contact Administrator.</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td class="normalfnt">Please click <a href="main.php"><span id="butClose">here</span></a> to close this page.</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  </table>

</body>
</html>

<?php
//BEGIN - INCLUDE FILES {
//include_once ('class.phpmailer.php');
//END 	- INCLUDE FILES }

//BEGIN - 
//$mail = new PHPMailer();
//

class Cls_Send_Mail
{
	private $db;
	
	function __construct($db)
	{
		$this->db 				= $db;
		$this->host				= 'secure.emailsrvr.com';
		$this->smtpDebug1		= '0';
		$this->smtpAuth			= true;
		$this->arrSys			= $this->get_sysytem_email_configurations($db);
		$this->username 		=$this->arrSys['email'];
		$this->password 		=$this->arrSys['pw'];
//		$this->username 		='hasitha@nimawum.com';
//		$this->password 		='1qaz2wsx@A';
	}
	
	public function Send_Special_Mail($emailModuleCode,$body,$location,$mail_TO,$mail_CC,$mail_BCC)
	{
		global $mail;
		$detail_array			= $this->GetMailInformation($emailModuleCode,$location);

		$mail->isSMTP();
		$mail->SMTPDebug 		= $this->smtpDebug1;
		$mail->Debugoutput 		= 'html';
		$mail->Host      		= $this->host;
		$mail->Port      		= 465;
		$mail->SMTPSecure		= 'ssl';
		$mail->SMTPAuth   		= $this->smtpAuth;
		$mail->Username 		= $this->username;
		$mail->Password 		= $this->password; 
		
		$mail->SetFrom('nsoft@screenlineholdings.com', 'NSOFT');
		$mail->AddReplyTo('nsoft@screenlineholdings.com','NSOFT');

		$email_to_array	= explode(',',$mail_TO);
		$i	= 0;
		foreach($email_to_array as $email_to_loop)
		{
			$mail->AddAddress($email_to_array[$i++]);
		}
		
		$email_to_array	= explode(',',$mail_CC);
		$i	= 0;
		foreach($email_to_array as $email_to_loop)
		{
			$mail->AddCC($email_to_array[$i++]);
		}

		$email_to_array	= explode(',',$mail_BCC);
		$i	= 0;
		foreach($email_to_array as $email_to_loop)
		{
			$mail->AddBCC($email_to_array[$i++]);
		}

		$mail->Subject = $emailModuleCode;

		$mail->MsgHTML($body);

		$mail->AltBody = 'This is a plain-text message body';

		if(!$mail->Send())
			echo "Mailer Error: " . $mail->ErrorInfo;
		else 
		  	echo "Message sent!";		
	}
	
	public function Send_Email_Pool_Mail($fronName,$fromEmail,$toEmail,$ccMail,$bccMail,$mailHeader,$mailBody)
	{	
		global $mail;
		$mail->isSMTP();
		$mail->SMTPDebug 		= $this->smtpDebug1;
		$mail->Debugoutput 		= 'html';
		$mail->Host      		= $this->host;
		$mail->Port      		= 465;
		$mail->SMTPSecure		= 'ssl';
		$mail->SMTPAuth   		= $this->smtpAuth;
		$mail->Username 		= $this->username;
		$mail->Password 		= $this->password; 
		
		$mail->SetFrom($fromEmail,$fronName);
		$mail->AddReplyTo($fromEmail,$fronName);
		
		$email_to_array	= explode(';',$toEmail);
		$i	= 0;
		foreach($email_to_array as $email_to_loop)
		{
			$mail->AddAddress($email_to_array[$i++]);
		}
		
		$email_to_array	= explode(';',$ccMail);
		$i	= 0;
		foreach($email_to_array as $email_to_loop)
		{
			$mail->AddCC($email_to_array[$i++]);
		}

		$email_to_array	= explode(';',$bccMail);
		$i	= 0;
		foreach($email_to_array as $email_to_loop)
		{
			$mail->AddBCC($email_to_array[$i++]);
		}

		$mail->Subject = $mailHeader;

		$mail->MsgHTML($mailBody);

		$mail->AltBody = 'This is a plain-text message body';

		if(!$mail->Send()){
			echo "Mailer Error: " . $mail->ErrorInfo;
			return false;
		}else{ 
		  	echo "Message sent!";
			return true;	
		}
	}
	
	private function GetMailInformation($emailModuleCode,$location)
	{
		if($location!="")
			$sql_where = "AND ML.LOCATION_ID = $location ";
			
		$sql = "SELECT
				ME.strEmailHeader 	AS EMAIL_HEADER,
				MAIL_TO				AS MAIL_TO,
				MAIL_CC				AS MAIL_CC,
				MAIL_BCC			AS MAIL_BCC
				FROM sys_special_mail_loop ML
				INNER JOIN sys_mail_events ME ON ME.intMailIventId = ML.MAIL_EVENT_ID
				WHERE ME.intModuleCode = '$emailModuleCode' 
					$sql_where ";
		$result = $this->db->RunQuery($sql);
		return mysqli_fetch_array($result);
	}
	
	private function get_sysytem_email_configurations($db)
	{
		$sql	="SELECT
		system_configuration.SYSTEM_EMAIL,
		system_configuration.SYSTEM_EMAIL_PASSWORD
		FROM
		system_configuration
		/*where SERVER_HOST= '$server_host'*/ limit 1";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$arr['email']	= $row['SYSTEM_EMAIL'];
		$arr['pw']		= $row['SYSTEM_EMAIL_PASSWORD'];
 		return $arr;
	}


/*
 * Hasitha Charaka
 * 
 */
	public function Send_Mail_Attachment($mailHeader,$body,$attachment,$mail_TO,$mail_CC,$FromMail,$FROMName)
	{

		global $mail;
		//$detail_array			= $this->GetMailInformation($emailModuleCode,$location);
		$mail->isSMTP();
		$mail->SMTPDebug 		= $this->smtpDebug1;
		$mail->Debugoutput 		= 'html'; 					// Set mailer to use SMTP
		$mail->Host      		= $this->host;
		$mail->Port      		= 465;				// Specify main and backup SMTP servers
		$mail->SMTPAuth = true;                               // Enable SMTP authentication
		$mail->Username 		= $this->username;   // SMTP username
		$mail->Password 		= $this->password;                           // SMTP password
		$mail->SMTPSecure = 'ssl';


		// Enable encryption, 'ssl' also accepted

		$mail->From = $FromMail;
		$mail->FromName = $FROMName;
		$mail->addAddress($mail_TO);                 // Add a recipient
		$mail->AddCC($mail_CC);
		$mail->WordWrap = 50;                                 // Set word wrap to 50 characters

		$mail->Subject = $mailHeader;
		$mail->AddAttachment($attachment);

		$mail->Body = $body;

		if (!$mail->send()) {
			echo 'Message could not be sent.';
			echo 'Mailer Error: ' . $mail->ErrorInfo;
		} else {
			echo 'Message has been sent';
		}

	}
}
?>
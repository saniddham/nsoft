<?php
class cls_marketerWiseSalesProjection_get
{
	
 	private $db;
 	
	function __construct($db)
	{
		$this->db 	= $db;
 	}

 	public function get_actuals_result($year,$month,$marketer,$toCureency,$clusterType)
	{
		
		$sql		= $this->get_actuals_sql($year,$month,$marketer,$toCureency,$clusterType);
 		$result		= $this->db->RunQuery($sql);
 		return $result;
	}
   
 	
	private function get_actuals_sql($year,$month,$marketer,$toCureency,$clusterType){

	$maxDate		=$year.'-'.sprintf("%02d",$month).'-01';


	    	$sql = "SELECT 
			table1.*,
		
			IFNULL((table1.intQty*table1.dblPrice*mst_financeexchangerate.dblBuying/exc.dblBuying),0) as dispQty 
		

			FROM (SELECT 
			OD.intOrderNo,
			OD.intOrderYear,
			OD.intSalesOrderId,
			OD.strSalesOrderNo,
			OD.strStyleNo,
			OD.dtPSD,
			OD.intRevisionNo,
			OD.intSampleNo,
			OD.intSampleYear,
			OD.intQty,
			OD.dblPrice,
 			OD.strCombo,
			OD.strGraphicNo,
			OD.strLineNo,
			OD.strPrintName,
			OH.strCustomerPoNo,
			mst_part.strName as part, 
			MC.strName AS customer,


		(SELECT DISTINCT mst_colors_ground.strName
							FROM
							trn_sampleinfomations_details 
							INNER JOIN mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
							WHERE
							trn_sampleinfomations_details.intSampleNo =  OD.intSampleNo AND
							trn_sampleinfomations_details.intSampleYear =  OD.intSampleYear  AND
							trn_sampleinfomations_details.strPrintName =  OD.strPrintName AND
							trn_sampleinfomations_details.strComboName =  OD.strCombo AND
							trn_sampleinfomations_details.intRevNo =  OD.intRevisionNo
							LIMIT 1
		) AS color,

			(SELECT
			Min(FDHA.dtApprovedDate)
			FROM
			ware_fabricdispatchheader
			INNER JOIN ware_fabricdispatchdetails ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo 
			AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear 
 			
			INNER JOIN ware_fabricdispatchheader_approvedby as FDHA 
			ON ware_fabricdispatchheader.intBulkDispatchNo = FDHA.intBulkDispatchNo 
			AND ware_fabricdispatchheader.intBulkDispatchNoYear = FDHA.intYear 
			AND ware_fabricdispatchheader.intApproveLevels = FDHA.intApproveLevelNo 
			AND FDHA.intStatus = 0 AND
			MONTH(FDHA.dtApprovedDate) = '$month' AND
			YEAR(FDHA.dtApprovedDate) = '$year'
		
			INNER JOIN trn_orderdetails as OD2 ON ware_fabricdispatchheader.intOrderNo = OD2.intOrderNo AND ware_fabricdispatchheader.intOrderYear = OD2.intOrderYear AND ware_fabricdispatchdetails.intSalesOrderId = OD2.intSalesOrderId
		
			WHERE ware_fabricdispatchheader.intStatus=1 AND ware_fabricdispatchheader.intOrderNo = OD.intOrderNo AND ware_fabricdispatchheader.intOrderYear = OD.intOrderYear AND OD2.strStyleNo = OD.strStyleNo
			AND FDHA.dtApprovedDate IS NOT NULL
			AND MONTH(FDHA.dtApprovedDate) = '$month' AND
			YEAR(FDHA.dtApprovedDate) = '$year'
 			
			ORDER BY
			FDHA.dtApprovedDate ASC) AS dispDate, 
			 
			(SELECT
			Min(FDHA.dtApprovedDate)
			FROM
			ware_fabricdispatchheader
			INNER JOIN ware_fabricdispatchdetails ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo 
			AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear 
 			
			INNER JOIN ware_fabricdispatchheader_approvedby as FDHA 
			ON ware_fabricdispatchheader.intBulkDispatchNo = FDHA.intBulkDispatchNo 
			AND ware_fabricdispatchheader.intBulkDispatchNoYear = FDHA.intYear 
			AND ware_fabricdispatchheader.intApproveLevels = FDHA.intApproveLevelNo 
			AND FDHA.intStatus = 0 AND
			date(FDHA.dtApprovedDate) < '$maxDate'  
		
			INNER JOIN trn_orderdetails as OD2 ON ware_fabricdispatchheader.intOrderNo = OD2.intOrderNo AND ware_fabricdispatchheader.intOrderYear = OD2.intOrderYear AND ware_fabricdispatchdetails.intSalesOrderId = OD2.intSalesOrderId
			
			WHERE ware_fabricdispatchheader.intStatus=1 AND ware_fabricdispatchheader.intOrderNo = OD.intOrderNo AND ware_fabricdispatchheader.intOrderYear = OD.intOrderYear AND OD2.strStyleNo = OD.strStyleNo
			AND FDHA.dtApprovedDate IS NOT NULL
 			
			ORDER BY
			FDHA.dtApprovedDate ASC) AS dispDatePrier, 
			
			OH.dtDate AS date,
			OH.intLocationId,
			OH.intMarketer,
			OH.intCurrency
			FROM
			trn_orderdetails as OD
			INNER JOIN trn_orderheader as OH ON OD.intOrderNo = OH.intOrderNo AND OD.intOrderYear = OH.intOrderYear  AND 
			OH.intMarketer='$marketer' 			  AND
			OH.intStatus!='-2' AND OH.intStatus!='0' 
			INNER JOIN mst_part ON OD.intPart = mst_part.intId
			
			INNER JOIN mst_customer MC ON MC.intId=OH.intCustomer 
			GROUP BY OD.intOrderNo,OD.intOrderYear,OD.intSalesOrderId  
			HAVING dispDatePrier IS NULL
			) as table1
			
			INNER JOIN mst_marketer ON table1.intMarketer = mst_marketer.intUserId 
			LEFT JOIN mst_locations ON table1.intLocationId = mst_locations.intId 
			LEFT JOIN mst_plant ON mst_locations.intPlant = mst_plant.intPlantId 
					
			LEFT JOIN mst_financeexchangerate ON table1.intCurrency = mst_financeexchangerate.intCurrencyId  
			AND DATE(table1.date) = mst_financeexchangerate.dtmDate AND 
			mst_locations.intCompanyId = mst_financeexchangerate.intCompanyId 
			LEFT JOIN mst_financeexchangerate as exc ON $toCureency = exc.intCurrencyId  
			AND DATE(table1.date) = exc.dtmDate AND 
			mst_locations.intCompanyId = exc.intCompanyId
			WHERE 
			mst_plant.MAIN_CLUSTER_ID ='$clusterType' 
			AND
			MONTH(table1.dispDate) = '$month' AND
			YEAR(table1.dispDate) = '$year' ";
			//echo $sql;
			return $sql;
	}
	
  
	
 }


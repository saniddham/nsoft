<?php
/**
 * Created by PhpStorm.
 * User: Hasitha
 * Date: 2/2/2018
 * Time: 3:32 PM
 */
ini_set('display_errors',0);

class cls_grading
{
    private $db;

    function __construct($db)
    {
        $this->db = $db;
    }

    public function saveGradingData($mainData,$tableData){
global $db;
//var_dump($tableData); die;
        if($tableData['itemCode_inktype']){
            $year= $mainData[0]['year'];
            $SampleNo= $mainData[0]['SampleNo'];
            $RevisionNo= $mainData[0]['RevisionNo'];
            $Combo= $mainData[0]['Combo'];
            $Print= $mainData[0]['Print'];
            $Size= $mainData[0]['Size'];

            $sql="DELETE
FROM
	`trn_sampleinfomations_grading_ink_items`
WHERE
	(`SAMPLE_NO` = '$SampleNo')
AND (`SAMPLE_YEAR` = '$year')
AND (`REVISION` = '$RevisionNo')
AND (`COMBO` = '$Combo')
AND (`PRINT` = '$Print')
AND (`SIZE` ='$Size')";
//var_dump($sql); //die;
$where ="(`SAMPLE_NO` = '$SampleNo')
AND (`SAMPLE_YEAR` = '$year')
AND (`REVISION` = '$RevisionNo')
AND (`COMBO` = '$Combo')
AND (`PRINT` = '$Print')
AND (`SIZE` ='$Size')";

            $delete=$db->delete('trn_sampleinfomations_grading_ink_items',$where);

            $inktypeCount = count($tableData['itemCode_inktype'])-1;
            //($x = 0; $x <= 10; $x++)
            for($i=0;$i<=$inktypeCount;$i++){

                $colourName_inktype = $tableData['colourName_inktype'][$i];
                $inktype=$tableData['inktype'][$i];
                $itemCode_inktype=$tableData['itemCode_inktype'][$i];
                $edited_consumpition_inktype=$tableData['edited_consumpition_inktype'][$i];
                $edit_consumption=!empty($edited_consumpition_inktype) ? "'$edited_consumpition_inktype'" : "NULL";
                $defconsumption_inktype=$tableData['defconsumption_inktype'][$i];
                $height_inktype=$tableData['height_inktype'][$i];
                $width_inktype=$tableData['width_inktype'][$i];
                $techniques_inktype=$tableData['techniques_inktype'][$i];
                $autoConsumption_inktype=$tableData['autoConsumption_inktype'][$i];
                $user=$_SESSION['userId'];

                $inktype_sql="INSERT INTO `trn_sampleinfomations_grading_ink_items` (
	`SAMPLE_NO`,
	`SAMPLE_YEAR`,
	`REVISION`,
	`COMBO`,
	`PRINT`,
	`SIZE`,
	`COLOUR`,
	`TECHNIQUE`,
	`INK_TYPE`,
	`ITEM`,
	`WIDTH`,
	`HEIGHT`,
	`AUTOMATED_CONSUMPTION`,
	`EDITED_CONSUMPTION`,
	`LAST_MODIFIED_BY`,
	`MODIFIED_TIME`
)
VALUES
	(
		'$SampleNo',
		'$year',
		'$RevisionNo',
		'$Combo',
		'$Print',
		'$Size',
		'$colourName_inktype',
		'$techniques_inktype',
		'$inktype',
		'$itemCode_inktype',
		'$width_inktype',
		'$height_inktype',
		'$autoConsumption_inktype',
		$edit_consumption,
		'$user',
		NOW()
	) ";

            $result[]= $this->db->RunQuery($inktype_sql);


            }//die;
        }
        else if ($tableData['itemCode_rollForm']){
            $rollFormCoint = count($tableData['itemCode_rollForm'])-1;
            for($i=0;$i<=$rollFormCoint;$i++){

                $year= $mainData[0]['year'];
                $SampleNo= $mainData[0]['SampleNo'];
                $RevisionNo= $mainData[0]['RevisionNo'];
                $Combo= $mainData[0]['Combo'];
                $Print= $mainData[0]['Print'];
                $Size= $mainData[0]['Size'];

                $colourName_rollForm = $tableData['colourName_rollForm'][$i];
                $techniques_rollForm=$tableData['techniques_rollForm'][$i];
                $itemCode_rollForm=$tableData['itemCode_rollForm'][$i];

                $inktype=$tableData['inktype'][$i];

                $edited_consumpition_inktype=$tableData['edited_consumpition_inktype'][$i];
                $edit_consumption=!empty($edited_consumpition_inktype) ? "'$edited_consumpition_inktype'" : "NULL";
                $defconsumption_inktype=$tableData['defconsumption_inktype'][$i];
                $height_inktype=$tableData['height_inktype'][$i];
                $width_inktype=$tableData['width_inktype'][$i];

                $autoConsumption_inktype=$tableData['autoConsumption_inktype'][$i];
                $user=$_SESSION['userId'];
            }
        }

    }




    public function checkRows($year,$sampleNo,$revision,$combo,$print){
        global $db;
        $sql = "SELECT
tsg.SAMPLE_NO,
tsg.SAMPLE_YEAR,
tsg.REVISION_NO,
tsg.COMBO,
tsg.PRINT
FROM
trn_sampleinfomations_gradings tsg
WHERE 
tsg.SAMPLE_NO ='$sampleNo' AND tsg.SAMPLE_YEAR='$year' AND tsg.REVISION_NO='$revision' AND tsg.COMBO='$combo' AND tsg.PRINT='$print'
";
        $result = $db->RunQuery($sql);

        if (mysqli_num_rows($result) > 1) {
            $status = 0;
        }else{
            $status = 1;
        }

        return $status;
}




    public function  saveModalData($ModalData,$MainData) {
		global $db;
        foreach ($ModalData as $Data){

            $size		=$Data['size'];
            $height 	=$Data['height'];
            $width		=$Data['width'];
            if($Data['precentage']=='' || $Data['precentage']=='null'){
                $precentage=0;
            }else{
                $precentage=$Data['precentage'];
            }

            $default 	= $Data['default_size'];
            //$precentage=($Data['precentage'] !='' ||$Data['precentage'] !='null' ? $Data['precentage'] : '0' );
            $sampleNo	=$MainData[0]['sampleNo'];
            $year 		=$MainData[0]['year'];
            $revision 	=$MainData[0]['revision'];
            $combo 		=$MainData[0]['combo'];
            $print 		=$MainData[0]['print'];

            if($Data['size'] !='') {

                $sql = "INSERT INTO `trn_sampleinfomations_gradings` (
				`SAMPLE_NO`,
				`SAMPLE_YEAR`,
				`REVISION_NO`,
				`COMBO`,
				`PRINT`,
				`SIZE`,
				`DEFAULT`,
				`PRESENTAGE`,
				`WIDTH`,
				`HEIGHT`
				)
				VALUES
				(
				'$sampleNo',
				'$year',
				'$revision',
				'$combo',
				'$print',
				'$size',
				'$default',
				'$precentage',
				'$width',
				'$height'
				);";

        $result = $db->RunQuery2($sql);

		}
		}

        return $sql;
    }

    /*
 * Update Grading Data
 */
    public function updateModalData($ModalData,$MainData){
        global $db;


        $sampleNo=$MainData[0]['sampleNo'];
        $year =$MainData[0]['year'];
        $revision =$MainData[0]['revision'];
        $combo =$MainData[0]['combo'];
        $print =$MainData[0]['print'];
        $sql="DELETE
FROM
	`trn_sampleinfomations_gradings`
WHERE
	(`SAMPLE_NO` = '$sampleNo')
AND (`SAMPLE_YEAR` = '$year')
AND (`REVISION_NO` = '$revision')
AND (`COMBO` = '$combo')
AND (`PRINT` = '$print');";


        $result = $db->RunQuery2($sql);

        if($result == true) {

            foreach ($ModalData as $Data) {

                $size = $Data['size'];
                $height = $Data['height'];
                $width = $Data['width'];

                if($Data['precentage']=='' || $Data['precentage']=='null'){
                    $precentage=0;
                }else{
                    $precentage=$Data['precentage'];
                }

                $default =$Data['default_size'];

//                if ($Data['count'] === $Data['default_size']) {
//                    $default = 1;
//                } else {
//                    $default = 0;
//                }



                if ($Data['size'] != '') {
                    $sql_i= "INSERT INTO `trn_sampleinfomations_gradings` (
	`SAMPLE_NO`,
	`SAMPLE_YEAR`,
	`REVISION_NO`,
	`COMBO`,
	`PRINT`,
	`SIZE`,
	`DEFAULT`,
	`PRESENTAGE`,
	`WIDTH`,
	`HEIGHT`
)
VALUES
	(
		'$sampleNo',
		'$year',
		'$revision',
		'$combo',
		'$print',
		'$size',
		'$default',
		'$precentage',
		'$width',
		'$height'
	);";

                $result = $db->RunQuery2($sql_i);
                }

               // $result = $db->RunQuery($sql);

            }
             // $result = $db->RunQuery($sql);
            return $sql_i;
        }else{

            return false;
        }

    }

    /*
     * Check grading Added Already
     */
    public function checkData($MainData)
    {
        global $db;
        $sampleNo 	= $MainData[0]['sampleNo'];
        $year 		= $MainData[0]['year'];
        $revision 	= $MainData[0]['revision'];
        $combo 		= $MainData[0]['combo'];
        $print 		= $MainData[0]['print'];

        $sql 		= "SELECT
						tsg.SAMPLE_NO,
						tsg.SAMPLE_YEAR,
						tsg.REVISION_NO,
						tsg.COMBO,
						tsg.PRINT
						FROM
						trn_sampleinfomations_gradings tsg
						WHERE 
						tsg.SAMPLE_NO ='$sampleNo' AND tsg.SAMPLE_YEAR='$year' AND tsg.REVISION_NO='$revision' AND tsg.COMBO='$combo' AND tsg.PRINT='$print'
						";
        global $db;
        $result 	= $db->RunQuery2($sql);

        if (mysqli_num_rows($result) > 1) {
            return false;
        }

        return true;


    }


    public function DeleteRow($MainData)
    {
        try {
            global $db;
            $sql = "	DELETE
	FROM
		`trn_sampleinfomations_gradings`
	WHERE
		(`SAMPLE_NO` = '" . $MainData['sampleNo'] . "')
	AND (`SAMPLE_YEAR` = '" . $MainData['year'] . "')
	AND (`REVISION_NO` = '" . $MainData['revision'] . "')
	AND (`COMBO` = '" . $MainData['combo'] . "')
	AND (`PRINT` = '" . $MainData['print'] . "')
	AND (`SIZE` = '" . $MainData['size'] . "')";

            $result = $db->RunQuery($sql);
            Throw new Exception($db->errormsg);
        }catch (Exception $e){
            $db->rollback();
            $arr = array(
                "status" => 'fail',
                "type" => $e->getMessage(),
            );
           return  json_encode($arr);



        }
    }

}
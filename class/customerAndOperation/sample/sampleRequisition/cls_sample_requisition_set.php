<?php
class Cls_sample_requisition_set
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	public function saveHeader($requisitionNo,$requisitionYear,$graphic,$customer,$style,$brand,$marketer,$washStand,$printMode,$locationId,$companyId,$userId,$status,$approveLevels)
	{
		return $this->saveHeader_sql($requisitionNo,$requisitionYear,$graphic,$customer,$style,$brand,$marketer,$washStand,$printMode,$locationId,$companyId,$userId,$status,$approveLevels);
	}
	public function saveDetails($requisitionNo,$requisitionYear,$typeId,$qty,$reqDate)
	{
		return $this->saveDetails_sql($requisitionNo,$requisitionYear,$typeId,$qty,$reqDate);
	}
	public function updateHeader($requisitionNo,$requisitionYear,$graphic,$customer,$style,$brand,$marketer,$washStand,$printMode,$locationId,$companyId,$userId,$userType)
	{
		return $this->updateHeader_sql($requisitionNo,$requisitionYear,$graphic,$customer,$style,$brand,$marketer,$washStand,$printMode,$locationId,$companyId,$userId,$userType);
	}
	public function updateApproveByStatus($requisitionNo,$requisitionYear,$maxStatus)
	{
		return $this->updateApproveByStatus_sql($requisitionNo,$requisitionYear,$maxStatus);
	}
	public function deleteDetails($requisitionNo,$requisitionYear)
	{
		return $this->deleteDetails_sql($requisitionNo,$requisitionYear);
	}
	public function updateHeaderStatus($requisitionNo,$requisitionYear,$para)
	{
		return $this->updateHeaderStatus_sql($requisitionNo,$requisitionYear,$para);
	}
	public function approved_by_insert($requisitionNo,$requisitionYear,$userId,$approval)
	{
		return $this->approved_by_insert_sql($requisitionNo,$requisitionYear,$userId,$approval);
	}
	public function updateBRHeader($requisitionNo,$requisitionYear,$style,$userId,$userType)
	{
		return $this->updateBRHeader_sql($requisitionNo,$requisitionYear,$style,$userId,$userType);
	}
	public function updateDetails($requisitionNo,$requisitionYear,$typeId,$qty,$reqDate)
	{
		return $this->updateDetails_sql($requisitionNo,$requisitionYear,$typeId,$qty,$reqDate);
	}
	public function updateHistoryHeader($requisitionNo,$requisitionYear,$userId)
	{
		return $this->updateHistoryHeader_sql($requisitionNo,$requisitionYear,$userId);
	}
	public function updateHistoryDetail($requisitionNo,$requisitionYear,$histryInsId)
	{
		return $this->updateHistoryDetail_sql($requisitionNo,$requisitionYear,$histryInsId);
	}
	public function saveHistoryDetail($requisitionNo,$requisitionYear,$typeId,$histryInsId)
	{
		return $this->saveHistoryDetail_sql($requisitionNo,$requisitionYear,$typeId,$histryInsId);
	}
	
	public function save_error_log($array_errors)
	{	
		foreach($array_errors as $sql){
			//echo $sql;
		  $this->save_error_log_sql($sql);
		}
	}
	
	
	private function saveHeader_sql($requisitionNo,$requisitionYear,$graphic,$customer,$style,$brand,$marketer,$washStand,$printMode,$locationId,$companyId,$userId,$status,$approveLevels)
	{
		$sql = "INSERT INTO trn_sample_requisition_header 
				(
				REQUISITION_NO, 
				REQUISITION_YEAR, 
				GRAPHIC, 
				CUSTOMER, 
				STYLE, 
				BRAND, 
				MARKETER, 
				WASH_STANDERD, 
				PRINT_MODE, 
				LOCATION_ID, 
				COMPANY_ID,
				STATUS,
				APPROVE_LEVELS,
				CREATED_BY, 
				CREATED_DATE
				)
				VALUES
				(
				'$requisitionNo', 
				'$requisitionYear', 
				UPPER('$graphic'), 
				'$customer', 
				'$style', 
				'$brand', 
				'$marketer', 
				$washStand,
				$printMode, 
				'$locationId', 
				'$companyId',
				'$status',
				'$approveLevels', 
				'$userId', 
				NOW()
				)";
		
		$result = $this->db->RunQuery_C($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function saveDetails_sql($requisitionNo,$requisitionYear,$typeId,$qty,$reqDate)
	{
		$sql = "INSERT INTO trn_sample_requisition_detail
				(
				REQUISITION_NO, 
				REQUISITION_YEAR, 
				SAMPLE_TYPE, 
				QTY, 
				REQUIRED_DATE,
				BAL_QTY
				)
				VALUES
				(
				'$requisitionNo', 
				'$requisitionYear', 
				'$typeId', 
				$qty, 
				$reqDate, 
				$qty
				)";
		
		$result = $this->db->RunQuery_C($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function updateHeader_sql($requisitionNo,$requisitionYear,$graphic,$customer,$style,$brand,$marketer,$washStand,$printMode,$locationId,$companyId,$userId,$userType)
	{
		$sql = "UPDATE trn_sample_requisition_header 
				SET
				GRAPHIC = UPPER('$graphic') , 
				CUSTOMER = '$customer' , 
				STYLE = '$style' , 
				BRAND = '$brand' , 
				STATUS = 1,
				MARKETER = '$marketer' , 
				WASH_STANDERD = '$washStand' , 
				PRINT_MODE = '$printMode' , 
				LOCATION_ID = '$locationId' , 
				COMPANY_ID = '$companyId' ";
		
		if($userType=='B')
			$sql.=",MODIFY_BY = '$userId' "; 
				
		$sql.=" WHERE
				REQUISITION_NO = '$requisitionNo' AND 
				REQUISITION_YEAR = '$requisitionYear' ";
		//echo $sql;
		$result = $this->db->RunQuery_C($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function updateApproveByStatus_sql($requisitionNo,$requisitionYear,$maxStatus)
	{
		$sql = "UPDATE trn_sample_requisition_approveby 
				SET
				STATUS = $maxStatus+1
				WHERE
				REQUISITION_NO = '$requisitionNo' AND 
				REQUISITION_YEAR = '$requisitionYear' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function deleteDetails_sql($requisitionNo,$requisitionYear)
	{
		$sql = "DELETE FROM trn_sample_requisition_detail 
				WHERE
				REQUISITION_NO = '$requisitionNo' AND 
				REQUISITION_YEAR = '$requisitionYear' AND 
				SAMPLE_TYPE = '6' ";
		
		$result = $this->db->RunQuery_C($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function updateHeaderStatus_sql($requisitionNo,$requisitionYear,$para)
	{
		$sql = "UPDATE trn_sample_requisition_header 
				SET
				STATUS = $para
				WHERE
				REQUISITION_NO = '$requisitionNo' AND 
				REQUISITION_YEAR = '$requisitionYear' ";
		
		$result = $this->db->RunQuery_C($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function approved_by_insert_sql($requisitionNo,$requisitionYear,$userId,$approval)
	{
		$sql = "INSERT INTO trn_sample_requisition_approveby 
				(
				REQUISITION_NO, 
				REQUISITION_YEAR, 
				APPROVE_LEVEL_NO, 
				APPROVED_BY, 
				APPROVED_DATE, 
				STATUS
				)
				VALUES
				(
				'$requisitionNo', 
				'$requisitionYear', 
				'$approval', 
				'$userId', 
				NOW(), 
				'0'
				) ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function updateBRHeader_sql($requisitionNo,$requisitionYear,$style,$userId,$userType)
	{
		$sql = "UPDATE trn_sample_requisition_header 
				SET
				STYLE = '$style',
				MODIFY_BY = '$userId' ,
				MODIFY_DATE = NOW()
				WHERE
				REQUISITION_NO = '$requisitionNo' AND 
				REQUISITION_YEAR = '$requisitionYear' ";
		
		$result = $this->db->RunQuery_C($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function updateDetails_sql($requisitionNo,$requisitionYear,$typeId,$qty,$reqDate)
	{
		$sql = "UPDATE trn_sample_requisition_detail 
				SET
				QTY = $qty , 
				REQUIRED_DATE = $reqDate , 
				BAL_QTY = $qty
				WHERE
				REQUISITION_NO = '$requisitionNo' AND 
				REQUISITION_YEAR = '$requisitionYear' AND 
				SAMPLE_TYPE = '$typeId' ";
		
		$result = $this->db->RunQuery_C($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function updateHistoryHeader_sql($requisitionNo,$requisitionYear,$userId)
	{
		$sql = "INSERT INTO trn_sample_requisition_header_history 
				( 
				REQUISITION_NO, 
				REQUISITION_YEAR, 
				GRAPHIC, 
				CUSTOMER, 
				STYLE, 
				BRAND, 
				MARKETER, 
				WASH_STANDERD, 
				PRINT_MODE, 
				LOCATION_ID, 
				COMPANY_ID, 
				STATUS, 
				APPROVE_LEVELS,
				CREATED_BY,
				CREATED_DATE,
				MODIFY_BY,
				MODIFY_DATE,
				CREATE_HISTORY_BY 
				)
				(
				SELECT 
				REQUISITION_NO,
				REQUISITION_YEAR,
				GRAPHIC,
				CUSTOMER,
				STYLE,
				BRAND,
				MARKETER,
				WASH_STANDERD,
				PRINT_MODE,
				LOCATION_ID,
				COMPANY_ID,
				STATUS,
				APPROVE_LEVELS,
				CREATED_BY,
				CREATED_DATE,
				MODIFY_BY,
				MODIFY_DATE,
				$userId
				FROM trn_sample_requisition_header
				WHERE REQUISITION_NO = '$requisitionNo' AND
				REQUISITION_YEAR = '$requisitionYear'
				)";
		
		$result = $this->db->RunQuery_C($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
			$data['insertId']		= $this->db->insertId;
		return $data;
	}
	private function updateHistoryDetail_sql($requisitionNo,$requisitionYear,$histryInsId)
	{
		$sql = "INSERT INTO trn_sample_requisition_detail_history 
				(
				ID, 
				REQUISITION_NO, 
				REQUISITION_YEAR, 
				SAMPLE_TYPE, 
				QTY,
				FABRIC_IN_DATE, 
				REQUIRED_DATE, 
				BAL_QTY,
				SAMPLE_ORDER_QTY,
				FABRIC_IN_QTY,
				DISPATCH_QTY
				)
				(
				SELECT
				$histryInsId,
				REQUISITION_NO,
				REQUISITION_YEAR,
				SAMPLE_TYPE,
				QTY,
				FABRIC_IN_DATE,
				REQUIRED_DATE,
				BAL_QTY,
				SAMPLE_ORDER_QTY,
				FABRIC_IN_QTY,
				DISPATCH_QTY
				FROM trn_sample_requisition_detail
				WHERE REQUISITION_NO = '$requisitionNo' AND
				REQUISITION_YEAR = '$requisitionYear'
				)";
		
		$result = $this->db->RunQuery_C($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function saveHistoryDetail_sql($requisitionNo,$requisitionYear,$typeId,$histryInsId)
	{
		$sql = "INSERT INTO trn_sample_requisition_detail_history 
				(
				ID,
				REQUISITION_NO, 
				REQUISITION_YEAR, 
				SAMPLE_TYPE
				)
				VALUES
				(
				'$histryInsId',
				'$requisitionNo', 
				'$requisitionYear', 
				'$typeId'
				)";
		
		$result = $this->db->RunQuery_C($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	
	private function save_error_log_sql($sql){
		
 		$result = $this->db->RunQuery($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
		
	}
}
?>
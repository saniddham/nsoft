<?php
$userType	= $_SESSION['iPadUserType'];
class Cls_sample_requisition_get
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	public function getCustomer($userId,$mainDb)
	{
		return $this->getCustomer_sql($userId,$mainDb);
	}
	public function getMarketer($mainDb)
	{
		return $this->getMarketer_sql($mainDb);
	}
	public function getSampleTypes($fstLimit,$lastLimit,$mainDb,$requisitionNo,$requisitionYear)
	{
		return $this->getSampleTypes_sql($fstLimit,$lastLimit,$mainDb,$requisitionNo,$requisitionYear);
	}
	public function getWashingStand($mainDb)
	{
		return $this->getWashingStand_sql($mainDb);
	}
	public function getBrand($customer,$mainDb)
	{
		return $this->getBrand_sql($customer,$mainDb);
	}
	public function loadHeaderData($requisitionNo,$requisitionYear,$executionType)
	{
		return $this->loadHeaderData_sql($requisitionNo,$requisitionYear,$executionType);
	}
	public function loadDetailData($requisitionNo,$requisitionYear,$executionType)
	{
		return $this->loadDetailData_sql($requisitionNo,$requisitionYear,$executionType);
	}
	public function getMaxStatus($requisitionNo,$requisitionYear)
	{
		return $this->getMaxStatus_sql($requisitionNo,$requisitionYear);
	}
	public function getRptHeaderData($requisitionNo,$requisitionYear)
	{
		return $this->getRptHeaderData_sql($requisitionNo,$requisitionYear);
	}
	public function getRptDetailData($requisitionNo,$requisitionYear)
	{
		return $this->getRptDetailData_sql($requisitionNo,$requisitionYear);
	}
/*	public function getRptApproveDetails($requisitionNo,$requisitionYear)
	{
		return $this->getRptApproveDetails_sql($requisitionNo,$requisitionYear);
	}*/
	public function validateQty($requisitionNo,$requisitionYear)
	{
		return $this->validateQty_sql($requisitionNo,$requisitionYear);
	}
	
	public function validateQty_sample_type_wise($requisitionNo,$requisitionYear,$type,$mainDb,$executionType)
	{
		return $this->validateQty_sample_type_wise_res($requisitionNo,$requisitionYear,$type,$mainDb,$executionType);
	}
	
	public function getGraphic()
	{
		return $this->getGraphic_sql();
	}
	public function getTypeOfPrint($mainDb)
	{
		return $this->getTypeOfPrint_sql($mainDb);
	}
	public function getPermission($userId,$programCode,$field,$mainDb,$executionType)
	{
		return $this->getPermission_sql($userId,$programCode,$field,$mainDb,$executionType);
	}
	public function getSavedDate($requisitionNo,$requisitionYear,$typeId)
	{
		return $this->getSavedDate_sql($requisitionNo,$requisitionYear,$typeId);
	}
	public function getSampleTypesRowCount($mainDb)
	{
		return $this->getSampleTypesRowCount_sql($mainDb);
	}
	public function getSavedHeaderData($reqNo,$reqYear,$mainDb)
	{
		return $this->getSavedHeaderData_sql($reqNo,$reqYear,$mainDb);
	}
	public function getSavedDetailData($reqNo,$reqYear)
	{
		return $this->getSavedDetailData_sql($reqNo,$reqYear);
	}
	public function checkDataExist($requisitionNo,$requisitionYear,$typeId)
	{
		return $this->checkDataExist_sql($requisitionNo,$requisitionYear,$typeId);
	}
	public function validateTypeWiseBeforeEdit($requisitionNo,$requisitionYear,$typeId)
	{
		return $this->validateTypeWiseBeforeEdit_sql($requisitionNo,$requisitionYear,$typeId);
	}
	public function loadEmailHeaderData($requisitionNo,$requisitionYear,$mainDb,$executionType)
	{
		return $this->loadEmailHeaderData_sql($requisitionNo,$requisitionYear,$mainDb,$executionType);
	}
	public function getCreatorByDetail($requisitionNo,$requisitionYear,$executionType)
	{
		return $this->getCreatorByDetail_sql($requisitionNo,$requisitionYear,$executionType);
	}
	public function getReceiverDetail($requisitionNo,$requisitionYear,$companyId,$mainDb,$executionType)
	{
		return $this->getReceiverDetail_sql($requisitionNo,$requisitionYear,$companyId,$mainDb,$executionType);
	}
	public function loadEmailHeaderUpdateData($requisitionNo,$requisitionYear,$mainDb,$executionType)
	{
		return $this->loadEmailHeaderUpdateData_sql($requisitionNo,$requisitionYear,$mainDb,$executionType);
	}
	public function loadEmailDetailUpdateData($requisitionNo,$requisitionYear,$mainDb,$executionType)
	{
		return $this->loadEmailDetailUpdateData_sql($requisitionNo,$requisitionYear,$mainDb,$executionType);
	}
	public function getLastHistoryId($requisitionNo,$requisitionYear)
	{
		return $this->getLastHistoryId_sql($requisitionNo,$requisitionYear);
	}
	public function loadBrndxEmailDetailUpdateData($requisitionNo,$requisitionYear,$mainDb,$historyId,$executionType)
	{
		return $this->loadBrndxEmailDetailUpdateData_sql($requisitionNo,$requisitionYear,$mainDb,$historyId,$executionType);
	}
	public function get_error_log_sql($savedMasseged, $error_sql, $code, $user){
		return $this->load_error_log_sql($savedMasseged, $error_sql, $code, $user);
	}
	
	public function getRequisitionListing($header_array,$mainDb,$limit,$userType)
	{
		return $this->getRequisitionListing_sql($header_array,$mainDb,$limit,$userType);
	}
	public function check_approved_orders_for_requisition($serialNo,$serialYear,$mainDb,$executionType)
	{
		return $this->check_approved_orders_for_requisition_data($serialNo,$serialYear,$mainDb,$executionType);
	}
	public function loadEmailSaveDetailData($requisitionNo,$requisitionYear,$executionType)
	{
		return $this->loadEmailSaveDetailData_sql($requisitionNo,$requisitionYear,$executionType);
	}
	
	private function getRequisitionListing_sql($header_array,$mainDb,$limit,$userType)
	{
		$para = "";
		if($header_array["Req_No"]!="")
		$para .= "AND CONCAT(RH.REQUISITION_NO,'/',RH.REQUISITION_YEAR) like '%".$header_array['Req_No']."%'";
	
		if($header_array["Graphic"]!="")
			$para .= "AND RH.GRAPHIC like '%".$header_array['Graphic']."%'";
			
		if($header_array["Customer"]!="")
			$para .= "AND CU.strName like '%".$header_array['Customer']."%'";
		
		if($header_array["Marketer"]!="")
			$para .= "AND US.strUserName like '%".$header_array['Marketer']."%'";
		
		if($userType=='M')
			$para .= "AND RD.SAMPLE_TYPE IN (6)";
		else
			$para .= "AND RD.SAMPLE_TYPE NOT IN (6)";
			
		$sql = "SELECT
				  RH.REQUISITION_NO									AS REQUISITION_NO,
				  RH.REQUISITION_YEAR								AS REQUISITION_YEAR,
				  CONCAT(RH.REQUISITION_NO,'/',RH.REQUISITION_YEAR) AS CONCAT_REQU_NO,
				  RH.GRAPHIC         								AS GRAPHIC,
				  CU.strName      									AS CUSTOMER_NAME,
				  US.strUserName  									AS MARKETER,
				  ROUND(COALESCE(SUM(RD.QTY),0)) 					AS REQUISITION_QTY,
				  ROUND(COALESCE(SUM(RD.SAMPLE_ORDER_QTY),0)) 		AS SAMPLE_ORDER_QTY,
				  ROUND(COALESCE(SUM(RD.FABRIC_IN_QTY),0)) 			AS FABRIC_IN_QTY,
				  ROUND(COALESCE(SUM(RD.DISPATCH_QTY),0)) 			AS DISPATCH_QTY,
				  US1.strUserName AS REQUEST_BY,
				  RH.CREATED_DATE AS REQUEST_DATE
				FROM trn_sample_requisition_header RH
				  INNER JOIN trn_sample_requisition_detail RD
					ON RD.REQUISITION_NO = RH.REQUISITION_NO
					  AND RD.REQUISITION_YEAR = RH.REQUISITION_YEAR
				  INNER JOIN $mainDb.brndx_mst_customer CU
					ON CU.intId = RH.CUSTOMER
				  INNER JOIN $mainDb.brndx_sys_users US
					ON US.intUserId = RH.MARKETER
				  INNER JOIN $mainDb.brndx_sys_users US1
					ON US1.intUserId = RH.CREATED_BY
				  INNER JOIN sys_users U
				  	ON U.intUserId = RH.CREATED_BY
				WHERE 1 = 1
					$para
				GROUP BY RH.REQUISITION_NO,RH.REQUISITION_YEAR
				ORDER BY RH.CREATED_DATE DESC
				$limit ";
		return $this->db->RunQuery($sql);
	}
	
	private function getCustomer_sql($userId,$mainDb)
	{
		$sql = "SELECT MC.intId,MC.strName 
				FROM mst_customer SRC
				INNER JOIN $mainDb.brndx_mst_customer MC ON MC.intId=SRC.CUSTOMER_ID 
				WHERE SRC.USER_ID = '$userId'
				ORDER BY MC.strName ";
		
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	private function getMarketer_sql($mainDb)
	{
		$sql = "SELECT SRM.USER_ID,SU.strUserName
				FROM mst_marketer SRM
				INNER JOIN $mainDb.brndx_sys_users SU ON SU.intUserId=SRM.USER_ID
				ORDER BY SU.strUserName ";
		
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	private function getSampleTypes_sql($fstLimit,$lastLimit,$mainDb,$requisitionNo,$requisitionYear)
	{
		$sql = "SELECT 
					SRST.SAMPLE_TYPE,
					ST.strName,
					(SELECT QTY
					 FROM trn_sample_requisition_detail TSRD
					 WHERE TSRD.SAMPLE_TYPE = SRST.SAMPLE_TYPE
					 	AND TSRD.REQUISITION_NO = '$requisitionNo'
						AND TSRD.REQUISITION_YEAR = '$requisitionYear') AS QTY,
					(SELECT REQUIRED_DATE
					 FROM trn_sample_requisition_detail TSRD
					 WHERE TSRD.SAMPLE_TYPE = SRST.SAMPLE_TYPE
					 	AND TSRD.REQUISITION_NO = '$requisitionNo'
						AND TSRD.REQUISITION_YEAR = '$requisitionYear') AS REQUIRED_DATE
				FROM mst_sample_types SRST
				INNER JOIN $mainDb.brndx_sample_types ST 
					ON ST.intId = SRST.SAMPLE_TYPE
				WHERE ST.intStatus = 1
				ORDER BY ST.strName
				LIMIT $fstLimit,$lastLimit  ";
		
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	private function getBrand_sql($customer,$mainDb)
	{
		$sql = "SELECT
				MB.intId,
				MB.strName
				FROM
				$mainDb.brndx_customer_brand CB
				INNER JOIN $mainDb.brndx_mst_brand MB ON MB.intId = CB.intBrandId
				INNER JOIN mst_brand SRB ON SRB.BRAND_ID=MB.intId
				WHERE
				CB.intCustomerId =  '$customer' AND
				MB.intStatus =  '1'
				ORDER BY
				MB.strName ASC ";
		
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	private function loadHeaderData_sql($requisitionNo,$requisitionYear,$executionType)
	{
		$sql = "SELECT 	
				GRAPHIC, 
				CUSTOMER, 
				STYLE, 
				BRAND, 
				MARKETER,
				WASH_STANDERD,
				PRINT_MODE,
				LOCATION_ID, 
				COMPANY_ID, 
				STATUS, 
				APPROVE_LEVELS
				FROM 
				trn_sample_requisition_header 
				WHERE 
				REQUISITION_NO = '$requisitionNo' AND
				REQUISITION_YEAR = '$requisitionYear' ";
		$result	= $this->db->$executionType($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row;
	}
	private function loadDetailData_sql($requisitionNo,$requisitionYear,$executionType)
	{
		$para = '';	
		global $userType;
		if($userType=='M')
			$para = "AND SAMPLE_TYPE = 6";
			
		$sql = "SELECT 	
				SAMPLE_TYPE, 
				QTY, 
				BAL_QTY,
				REQUIRED_DATE			 
				FROM 
				trn_sample_requisition_detail 
				WHERE 
				REQUISITION_NO = '$requisitionNo' AND
				REQUISITION_YEAR = '$requisitionYear'
				$para
				";
		$result	= $this->db->$executionType($sql);		
		return $result;
	}
	private function getMaxStatus_sql($requisitionNo,$requisitionYear)
	{
		$sql = "SELECT MAX(tb.STATUS) AS maxStatus  
				FROM trn_sample_requisition_approveby AS tb
				WHERE 
				tb.REQUISITION_NO='$requisitionNo' AND
				tb.REQUISITION_YEAR='$requisitionYear' ";
		
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		return $row['maxStatus'];
		
	}
	private function getRptHeaderData_sql($requisitionNo,$requisitionYear)
	{
		$sql = "SELECT SRH.GRAPHIC,
				MC.strName AS CUSTOMER_NAME,
				SRH.STYLE,
				MB.strName AS BRAND_NAME,
				SU.strUserName AS MARKETER,
				SRH.REMARKS,
				SRH.STATUS,
				SRH.APPROVE_LEVELS,
				APPROVE_LEVELS as LEVELS,
				UA.strUserName AS CREATOR,
				SRH.CREATED_DATE
				FROM trn_sample_requisition_header SRH
				INNER JOIN mst_customer MC ON MC.intId=SRH.CUSTOMER
				INNER JOIN sys_users SU ON SU.intUserId=SRH.MARKETER
				INNER JOIN sys_users UA ON UA.intUserId=SRH.CREATED_BY
				INNER JOIN mst_brand MB ON MB.intId=SRH.BRAND
				WHERE 
				SRH.REQUISITION_NO = '$requisitionNo' AND
				SRH.REQUISITION_YEAR = '$requisitionYear' ";
		
		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row;
	}
	private function getRptDetailData_sql($requisitionNo,$requisitionYear)
	{
		$sql = "SELECT
				MST.strName AS SAMPLE_TYPE_NAME, 
				SRD.QTY,
				SRD.MINIMUM_DAYS,
				SRD.REQUIRED_DATE,
				SRD.BAL_QTY,
				SRD.DISPATCH_QTY,
				SRD.SAMPLE_TYPE,
				SRD.FABRIC_IN_DATE AS fabInDate,
				
				(SELECT SUM(SFID.INQTY)
				FROM trn_sampleorder_fabricin_details SFID
				INNER JOIN trn_sampleorder_fabricin_header SFIH ON SFIH.SAMPLE_FABRICIN_NO=SFID.SAMPLE_FABRICIN_NO AND
				SFIH.SAMPLE_FABRICIN_YEAR=SFID.SAMPLE_FABRICIN_YEAR
				INNER JOIN trn_sampleorderheader SOH ON SOH.intSampleOrderNo=SFIH.SAMPLE_ORDER_NO AND
				SOH.intSampleOrderYear=SFIH.SAMPLE_FABRICIN_YEAR
				WHERE SOH.intSampleReqNo=SRD.REQUISITION_NO AND
				SOH.intSampleReqYear=SRD.REQUISITION_YEAR AND
				SFID.SAMPLETYPE_ID=SRD.SAMPLE_TYPE AND
				SFIH.STATUS = 1) AS totFabInQty
				
				FROM trn_sample_requisition_detail SRD
				INNER JOIN mst_sampletypes MST ON MST.intId=SRD.SAMPLE_TYPE
				LEFT JOIN trn_sampleorderheader SOH ON SOH.intSampleReqNo=SRD.REQUISITION_NO AND
				SOH.intSampleReqYear=SRD.REQUISITION_YEAR
				WHERE 
				SRD.REQUISITION_NO = '$requisitionNo' AND
				SRD.REQUISITION_YEAR = '$requisitionYear'
				GROUP BY SRD.REQUISITION_YEAR,SRD.REQUISITION_NO,SRD.SAMPLE_TYPE ";
		
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	function getRptApproveDetails_sql($requisitionNo,$requisitionYear)
	{
		$sql = "SELECT
				SRAB.APPROVED_BY,
				SRAB.APPROVED_DATE AS dtApprovedDate,
				SU.strUserName AS UserName,
				SRAB.APPROVE_LEVEL_NO AS intApproveLevelNo
				FROM
				trn_sample_requisition_approveby SRAB
				INNER JOIN sys_users SU ON SRAB.APPROVED_BY = SU.intUserId
				WHERE SRAB.REQUISITION_NO ='$requisitionNo' AND
				SRAB.REQUISITION_YEAR ='$requisitionYear'      
				ORDER BY SRAB.APPROVED_DATE ASC ";
		
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	private function validateQty_sql($requisitionNo,$requisitionYear)
	{
		$sql = "SELECT QTY,BAL_QTY,REQUIRED_DATE
				FROM trn_sample_requisition_detail 
				WHERE 
				REQUISITION_NO = '$requisitionNo' AND 
				REQUISITION_YEAR = '$requisitionYear' ";
		
		$result = $this->db->RunQuery2($sql);
		return $result;
	}
	
	private function validateQty_sample_type_wise_res($requisitionNo,$requisitionYear,$type,$mainDb,$executionType)
	{
		$sql = "SELECT QTY,BAL_QTY,REQUIRED_DATE
				FROM trn_sample_requisition_detail 
				WHERE 
				REQUISITION_NO = '$requisitionNo' AND 
				REQUISITION_YEAR = '$requisitionYear' AND 
				SAMPLE_TYPE = '$type' ";
		
		$result = $this->db->$executionType($sql);
		return $result;
	}
	
	private function getGraphic_sql()
	{
		$sql = "SELECT 
					REQUISITION_NO,
					REQUISITION_YEAR,
					CONCAT(REQUISITION_NO,'/',REQUISITION_YEAR) AS concatReqNo,
					GRAPHIC,
					IF(STATUS='-2','red','')	AS BGCOLOR
				FROM trn_sample_requisition_header
				-- WHERE STATUS = 1 ";
		
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	private function getWashingStand_sql($mainDb)
	{
		$sql = "SELECT WS.intId,WS.strName
				FROM mst_washstanderd SRWS
				INNER JOIN $mainDb.brndx_washstanderd WS ON WS.intId=SRWS.WASH_STANDERD_ID
				WHERE WS.intStatus = 1
				ORDER BY WS.strName ";
		
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	private function getTypeOfPrint_sql($mainDb)
	{
		$sql = "SELECT PM.intId,PM.strName
				FROM mst_printmode SRPM
				INNER JOIN $mainDb.brndx_printmode PM ON PM.intId=SRPM.PRINT_MODE_ID
				WHERE PM.intStatus = 1
				ORDER BY PM.strName ";
		
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	private function getPermission_sql($userId,$programCode,$field,$mainDb,$executionType)
	{
		$sql = "SELECT
				IFNULL(MP.$field,0)  AS permision 
				FROM $mainDb.brndx_menupermision MP
				INNER JOIN $mainDb.brndx_menus M ON MP.intMenuId = M.intId
				WHERE
				M.strCode =  '$programCode' AND
				MP.intUserId =  '$userId' ";
		
		$result = $this->db->$executionType($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row['permision'];
	}
	private function getSavedDate_sql($requisitionNo,$requisitionYear,$typeId)
	{
		$sql = "SELECT REQUIRED_DATE
				FROM 
				trn_sample_requisition_detail 
				WHERE REQUISITION_NO = '$requisitionNo' AND
				REQUISITION_YEAR = '$requisitionYear' AND
				SAMPLE_TYPE = '$typeId' ";
		
		$result = $this->db->RunQuery2($sql);		
		$row	= mysqli_fetch_array($result);
		
		return $row['REQUIRED_DATE'];
	}
	private function getSampleTypesRowCount_sql($mainDb)
	{
		$sql = "SELECT COUNT(SAMPLE_TYPE) rowCount
				FROM mst_sample_types SRST
				INNER JOIN $mainDb.brndx_sample_types ST ON ST.intId=SRST.SAMPLE_TYPE
				WHERE ST.intStatus = 1";
		
		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);

		return $row['rowCount'];
	}
	private function getSavedHeaderData_sql($reqNo,$reqYear,$mainDb)
	{
		$sql = "SELECT SRH.STYLE,
				SRH.MARKETER,
				SRH.PRINT_MODE,
				SRH.BRAND,
				SRH.WASH_STANDERD,
				SRH.GRAPHIC,
				SRH.CUSTOMER,
				SU.strUserName AS marketer,
				PM.strName AS typeOfPeint,
				CU.strName AS customer,
				BR.strName AS brand,
				WS.strName AS washStanderd
				FROM trn_sample_requisition_header SRH
				INNER JOIN $mainDb.brndx_sys_users SU ON SU.intUserId=SRH.MARKETER
				LEFT JOIN $mainDb.brndx_printmode PM ON PM.intId=SRH.PRINT_MODE
				INNER JOIN $mainDb.brndx_mst_customer CU ON CU.intId=SRH.CUSTOMER
				INNER JOIN $mainDb.brndx_mst_brand BR ON BR.intId=SRH.BRAND
				LEFT JOIN $mainDb.brndx_washstanderd WS ON WS.intId=SRH.WASH_STANDERD
				WHERE SRH.REQUISITION_NO = '$reqNo' AND
				SRH.REQUISITION_YEAR = '$reqYear' AND
				SRH.STATUS = 1 ";
		
		$result = $this->db->RunQuery($sql);
		return $result;	
	}
	private function getSavedDetailData_sql($reqNo,$reqYear)
	{
		$sql = "SELECT SAMPLE_TYPE,QTY,REQUIRED_DATE
				FROM trn_sample_requisition_detail
				WHERE REQUISITION_NO = '$reqNo' AND
				REQUISITION_YEAR = '$reqYear' ";
		
		$result = $this->db->RunQuery($sql);
		return $result;	
	}
	public function checkDataExist_sql($requisitionNo,$requisitionYear,$typeId)
	{
		$sql = "SELECT * 
				FROM trn_sample_requisition_detail
				WHERE REQUISITION_NO = '$requisitionNo' AND
				REQUISITION_YEAR = '$requisitionYear' AND
				SAMPLE_TYPE = '$typeId' ";
		
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		$count	= mysqli_num_rows($result);
		
		if($count>0)
			return true;
		else 
			return false;
	}
	public function validateTypeWiseBeforeEdit_sql($requisitionNo,$requisitionYear,$typeId)
	{
		$sql = "SELECT IFNULL(QTY,0) AS qty,
				IFNULL(BAL_QTY,0) AS balQty
				FROM trn_sample_requisition_detail
				WHERE REQUISITION_NO = '$requisitionNo' AND
				REQUISITION_YEAR = '$requisitionYear' AND
				SAMPLE_TYPE = '$typeId' AND
				QTY > 0 ";
		
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		$count 	= mysqli_num_rows($result);
		
		$data['qty']	= $row['qty'];
		$data['balQty']	= $row['balQty'];
		$data['count']	= $count;
		
		return $data;
	}
	public function loadEmailHeaderData_sql($requisitionNo,$requisitionYear,$mainDb,$executionType)
	{
		$sql = "SELECT SRH.GRAPHIC,
				MC.strName AS CUSTOMER_NAME,
				SRH.STYLE,
				MB.strName AS BRAND_NAME,
				SRD.QTY,
				SRD.REQUIRED_DATE,
				SRH.STATUS
				FROM trn_sample_requisition_header SRH
				INNER JOIN $mainDb.brndx_mst_customer MC ON MC.intId=SRH.CUSTOMER
				INNER JOIN $mainDb.brndx_mst_brand MB ON MB.intId=SRH.BRAND
				INNER JOIN trn_sample_requisition_detail SRD ON SRD.REQUISITION_NO=SRH.REQUISITION_NO AND
				SRD.REQUISITION_YEAR=SRH.REQUISITION_YEAR AND
				SRD.SAMPLE_TYPE=6
				WHERE 
				SRH.REQUISITION_NO = '$requisitionNo' AND
				SRH.REQUISITION_YEAR = '$requisitionYear' ";
		
		$result = $this->db->$executionType($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row;
	}
	public function getCreatorByDetail_sql($requisitionNo,$requisitionYear,$executionType)
	{
		$sql = "SELECT SU.strUserName,
				SU.strEmail
				FROM trn_sample_requisition_header SRH
				INNER JOIN sys_users SU ON SU.intUserId=SRH.CREATED_BY
				WHERE SRH.REQUISITION_NO = '$requisitionNo' AND
				SRH.REQUISITION_YEAR = '$requisitionYear' ";
		
		$result = $this->db->$executionType($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row;
	}
	private function getReceiverDetail_sql($requisitionNo,$requisitionYear,$companyId,$mainDb,$executionType)
	{
		$sql = "SELECT DISTINCT
				SU.strUserName AS userName,
				SME.intUserId AS userId,
				SU.strEmail AS emailAddress
				FROM
				sys_mail_eventusers SME
				INNER JOIN $mainDb.brndx_sys_users SU ON SU.intUserId = SME.intUserId
				WHERE
				SME.intMailEventId = '1020' AND
				SME.intCompanyId =  '$companyId'
				UNION 
				(
				SELECT SRSU.strUserName AS userName,
				SRH.MARKETER AS userId,
				SRSU.strEmail AS emailAddress
				FROM trn_sample_requisition_header SRH
				INNER JOIN $mainDb.brndx_sys_users SRSU ON SRSU.intUserId = SRH.MARKETER
				WHERE SRH.REQUISITION_NO = '$requisitionNo' AND
				SRH.REQUISITION_YEAR = '$requisitionYear'
				)";
		
		$result = $this->db->$executionType($sql);
		
		return $result;
	}
	private function loadEmailHeaderUpdateData_sql($requisitionNo,$requisitionYear,$mainDb,$executionType)
	{
		$sql = "SELECT
				CASE WHEN SRHH.GRAPHIC = SRH.GRAPHIC
				THEN '0~0' ELSE CONCAT(SRH.GRAPHIC,'~','Graphic Ref No') END AS GRAPHIC,
				CASE WHEN SRHH.STYLE = SRH.STYLE
				THEN '0~0' ELSE CONCAT(SRH.STYLE,'~','Style No') END AS STYLE,
				CASE WHEN SRHH.CUSTOMER = SRH.CUSTOMER
				THEN '0~0' ELSE CONCAT(MC.strName,'~','Customer') END AS CUSTOMER,
				CASE WHEN SRHH.BRAND = SRH.BRAND
				THEN '0~0' ELSE CONCAT(MB.strName,'~','Brand') END AS BRAND,
				CASE WHEN SRHH.MARKETER = SRH.MARKETER
				THEN '0~0' ELSE CONCAT(SU.strUserName,'~','Marketer') END AS MARKETER,
				CASE WHEN SRHH.WASH_STANDERD = SRH.WASH_STANDERD
				THEN '0~0' ELSE CONCAT(WS.strName,'~','Wash standerd') END AS WASH_STANDERD,
				CASE WHEN SRHH.PRINT_MODE = SRH.PRINT_MODE
				THEN '0~0' ELSE CONCAT(PM.strName,'~','Type of print') END AS PRINT_MODE
				FROM
				trn_sample_requisition_header_history SRHH
				LEFT JOIN trn_sample_requisition_header SRH
				ON SRH.REQUISITION_NO = SRHH.REQUISITION_NO AND
				SRH.REQUISITION_YEAR = SRHH.REQUISITION_YEAR
				LEFT JOIN $mainDb.brndx_mst_customer MC ON MC.intId=SRH.CUSTOMER
				LEFT JOIN $mainDb.brndx_mst_brand MB ON MB.intId=SRH.BRAND
				LEFT JOIN $mainDb.brndx_sys_users SU ON SU.intUserId=SRH.MARKETER
				LEFT JOIN $mainDb.brndx_washstanderd WS ON WS.intId=SRH.WASH_STANDERD
				LEFT JOIN $mainDb.brndx_printmode PM ON PM.intId=SRH.PRINT_MODE
				WHERE SRHH.REQUISITION_NO = '$requisitionNo' AND
				SRHH.REQUISITION_YEAR = '$requisitionYear'
				ORDER BY SRHH.ID DESC
				LIMIT 0,1  ";
		
		$result = $this->db->$executionType($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row;
	}
	private function loadEmailDetailUpdateData_sql($requisitionNo,$requisitionYear,$mainDb,$executionType)
	{
		$sql = "SELECT
				CASE WHEN SRHD.QTY = SRD.QTY
				THEN '0~0' ELSE CONCAT(SRD.QTY,'~','Sample Qty') END AS QTY,
				CASE WHEN SRHD.REQUIRED_DATE = SRD.REQUIRED_DATE
				THEN '0~0' ELSE CONCAT(SRD.REQUIRED_DATE,'~','Required Date') END AS REQDATE
				FROM
				trn_sample_requisition_header_history SRHH
				LEFT JOIN trn_sample_requisition_detail_history SRHD ON SRHH.ID=SRHD.ID
				LEFT JOIN trn_sample_requisition_detail SRD
				ON SRD.REQUISITION_NO = SRHD.REQUISITION_NO AND
				SRD.REQUISITION_YEAR = SRHD.REQUISITION_YEAR
				WHERE SRHH.REQUISITION_NO = '$requisitionNo' AND
				SRHH.REQUISITION_YEAR = '$requisitionYear' AND
				SRHD.SAMPLE_TYPE = '6'
				ORDER BY SRHH.ID DESC
				LIMIT 0,1 ";
		
		$result = $this->db->$executionType($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row;
	}
	private function getLastHistoryId_sql($requisitionNo,$requisitionYear)
	{
		$sql = "SELECT MAX(ID) AS maxId
				FROM trn_sample_requisition_header_history
				WHERE REQUISITION_NO = '$requisitionNo' AND
				REQUISITION_YEAR = '$requisitionYear' ";
		
		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row['maxId'];
	}
	private function loadBrndxEmailDetailUpdateData_sql($requisitionNo,$requisitionYear,$mainDb,$historyId,$executionType)
	{
		$sql = "SELECT
				CASE WHEN IFNULL(SRHD.QTY,0) = IFNULL(SRD.QTY,0)
				THEN '0' ELSE CONCAT(SRD.QTY) END AS QTY,
				CASE WHEN IFNULL(SRHD.REQUIRED_DATE,0) = IFNULL(SRD.REQUIRED_DATE,0)
				THEN '0' ELSE CONCAT(SRD.REQUIRED_DATE) END AS REQDATE,
				ST.strName AS sampleType
				FROM
				trn_sample_requisition_header_history SRHH
				LEFT JOIN trn_sample_requisition_detail_history SRHD ON SRHH.ID=SRHD.ID
				LEFT JOIN trn_sample_requisition_detail SRD
				ON SRD.REQUISITION_NO = SRHD.REQUISITION_NO AND
				SRD.REQUISITION_YEAR = SRHD.REQUISITION_YEAR AND
				SRD.SAMPLE_TYPE=SRHD.SAMPLE_TYPE
				LEFT JOIN $mainDb.brndx_sample_types ST ON ST.intId=SRHD.SAMPLE_TYPE
				WHERE SRHH.REQUISITION_NO = '$requisitionNo' AND
				SRHH.REQUISITION_YEAR = '$requisitionYear' AND
				SRHH.ID = '$historyId' ";
		
		$result = $this->db->$executionType($sql);
		
		return $result;
	}
	
	private function load_error_log_sql($savedMasseged, $error_sql,$code,$user){
		
	$error_sql = str_replace("'","\'",$error_sql);
	$error_sql = str_replace('"','\"',$error_sql);
	
	
		$sql = "INSERT INTO sys_error_log 
				(
				MESSAGE_CODE, 
				ACTUAL_ERROR_MESSAGE, 
				PROGRAMME, 
				QUERY, 
				TIME, 
				USER, 
				REMARKS 
				)
				VALUES
				(
				'$code', 
				'$savedMasseged', 
				'SAMPLE REQUESITION', 
				\"".$error_sql."\", 
				now(), 
				'$user' ,
				''
				)";
		
 		return $sql;
		
	}
	
	private function check_approved_orders_for_requisition_data($serialNo,$serialYear,$mainDb,$executionType){
		
		/*$sql = "SELECT
				$mainDb.brndx_sampleorder_header.intStatus,
				$mainDb.brndx_sampleorder_header.APPROVE_LEVELS,
				$mainDb.brndx_sampleorder_header.intSampleOrderNo,
				$mainDb.brndx_sampleorder_header.intSampleOrderYear
				FROM $mainDb.brndx_sampleorder_header 
				WHERE
				$mainDb.brndx_sampleorder_header.intSampleReqNo = '$serialNo' AND
				$mainDb.brndx_sampleorder_header.intSampleReqYear = '$serialYear' AND
				$mainDb.brndx_sampleorder_header.intStatus >= 1 AND 
				$mainDb.brndx_sampleorder_header.APPROVE_LEVELS <= $mainDb.brndx_sampleorder_header.intStatus";*/
		
		$sql = "SELECT MAX(SAMPLE_ORDER_QTY) AS smplOrdQty
				FROM trn_sample_requisition_detail
				WHERE REQUISITION_NO = '$serialNo' AND
				REQUISITION_YEAR = '$serialYear' ";
		
		$result 	= $this->db->$executionType($sql);
		$row 		= mysqli_fetch_array($result);
		if($row['smplOrdQty'] > 0)
		{
			$data['type']			= 'false';
			$data['savedMassege']	= 'Sample order raised.Can not Edit .';
			$data['error_sql']		= $rows[$sql];
		}
		else
		{
			$data['type']			= 'true';
			$data['savedMassege']	= $rows[''];
			$data['error_sql']		= $rows[$sql];
		}
		
 		return $data;
		
	}
	private function loadEmailSaveDetailData_sql($requisitionNo,$requisitionYear,$executionType)
	{
		$sql = "SELECT 	
				ST.strName AS SAMPLE_TYPE,
				SRD.SAMPLE_TYPE AS SAMPLE_TYPE_ID, 
				SRD.QTY, 
				SRD.REQUIRED_DATE			 
				FROM 
				trn_sample_requisition_detail SRD
				INNER JOIN nsoft.brndx_sample_types ST ON ST.intId=SRD.SAMPLE_TYPE
				WHERE 
				REQUISITION_NO = '$requisitionNo' AND
				REQUISITION_YEAR = '$requisitionYear'
				HAVING SRD.QTY > 0 ";
		
		$result = $this->db->$executionType($sql);
		return $result;
	}
}
?>
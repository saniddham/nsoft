<?php
class Cls_sample_fabricin_get
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	public function getGraphicCombo()
	{
		return $this->getGraphicCombo_sql();
	}
	public function getSampleHeaderData($sampleOrderNo,$sampleOrderYear)
	{
		return $this->getSampleHeaderData_sql($sampleOrderNo,$sampleOrderYear);
	}
	public function getSampleOrderDetailData($sampleOrderNo,$sampleOrderYear)
	{
		return $this->getSampleOrderDetailData_sql($sampleOrderNo,$sampleOrderYear);
	}
	public function loadHeaderData($fabricInNo,$fabricInYear,$executionType)
	{
		return $this->loadHeaderData_sql($fabricInNo,$fabricInYear,$executionType);
	}
	public function getMaxStatus($fabricInNo,$fabricInYear)
	{
		return $this->getMaxStatus_sql($fabricInNo,$fabricInYear);
	}
	public function loadDetailData($fabricInNo,$fabricInYear,$executionType)
	{
		return $this->loadDetailData_sql($fabricInNo,$fabricInYear,$executionType);
	}
	public function getRptApproveDetails($fabricInNo,$fabricInYear)
	{
		return $this->getRptApproveDetails_sql($fabricInNo,$fabricInYear);
	}
	public function getSampleOrderStatus($sampleOrderNo,$sampleOrderYear)
	{
		return $this->getSampleOrderStatus_sql($sampleOrderNo,$sampleOrderYear);
	}
	public function getSampleOrderFabInBalQty($sampleOrderNo,$sampleOrderYear,$combo,$part,$grade,$size,$sampleType,$reqDate)
	{
		return $this->getSampleOrderFabInBalQty_sql($sampleOrderNo,$sampleOrderYear,$combo,$part,$grade,$size,$sampleType,$reqDate);
	}
	public function getSampleFabricInQty($fabricInNo,$fabricInYear)
	{
		return $this->getSampleFabricInQty_sql($fabricInNo,$fabricInYear);
	}
	public function getRequsitionNo($sampleNo,$sampleYear)
	{
		return $this->getRequsitionNo_sql($sampleNo,$sampleYear);
	}
	public function getSampleFabricInDetail($fabricInNo,$fabricInYear)
	{
		return $this->getSampleFabricInDetail_sql($fabricInNo,$fabricInYear);
	}
	public function checkDispatchRaised($sampleOrderNo,$sampleOrderYear,$combo,$part,$grade,$size,$sampleTypeId,$requireDate)
	{
		return $this->checkDispatchRaised_sql($sampleOrderNo,$sampleOrderYear,$combo,$part,$grade,$size,$sampleTypeId,$requireDate);
	}
	private function getGraphicCombo_sql()
	{
		$sql = "SELECT SOH.intSampleOrderYear,SOH.intSampleOrderNo,CONCAT(SI.strGraphicRefNo,'-',SOH.intSampleOrderNo,'/',SOH.intSampleOrderYear) AS conGraphicNo,
				(SELECT SUM(SOD.intBalToFabricInQty)
				FROM trn_sampleorderdetails SOD
				WHERE SOD.intSampleOrderNo=SOH.intSampleOrderNo AND
				SOD.intSampleOrderYear=SOH.intSampleOrderYear) AS balToFabricInQty
				FROM trn_sampleorderheader SOH
				INNER JOIN trn_sampleinfomations SI ON SI.intSampleNo=SOH.intSampleNo AND
				SI.intSampleYear=SOH.intSampleYear AND
				SI.intRevisionNo=SOH.intRevNo
				WHERE SOH.intStatus = 1
				/*HAVING balToFabricInQty>0*/ /*temporarily commented on 2016-10-26 sice they need unlimited fabric in facility.*/
				ORDER BY SI.strGraphicRefNo ";
		
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	private function getSampleHeaderData_sql($sampleOrderNo,$sampleOrderYear)
	{
		$sql = "SELECT DISTINCT SOH.intSampleReqNo,
				SOH.intSampleReqYear,
				CONCAT(SOH.intSampleReqNo,'/',SOH.intSampleReqYear) as conCatReqNo,
				CONCAT(SOH.intSampleOrderNo,'/',SOH.intSampleOrderYear) as conSampleOrderNo,
				MC.strName AS customer,
				CLH.strName AS customerLocation,
				SI.strGraphicRefNo,
				SI.strStyleNo,
				SOH.strRemarks AS sampleOrderRemarks
				FROM 
				trn_sampleorderheader SOH
				INNER JOIN trn_sampleinfomations SI ON SI.intSampleNo=SOH.intSampleNo AND
				SI.intSampleYear=SOH.intSampleYear AND
				SI.intRevisionNo=SOH.intRevNo
				INNER JOIN mst_customer MC ON MC.intId=SI.intCustomer
				INNER JOIN mst_customer_locations CL ON CL.intLocationId=SOH.intCustomerLocation
				INNER JOIN mst_customer_locations_header CLH ON CL.intLocationId = CLH.intId
				WHERE SOH.intSampleOrderNo = '$sampleOrderNo' AND
				SOH.intSampleOrderYear = '$sampleOrderYear' ";
		
		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row;
	}
	private function getSampleOrderDetailData_sql($sampleOrderNo,$sampleOrderYear)
	{
		 $sql = "SELECT strCombo,
				SOD.intPart,
				MP.strName AS partName,
				MST.strName AS sampleType,
				SOD.intSampleTypeId,
				SOD.intGrade,
				SOD.intQty,
				SOD.strSize,
				SOD.dtRequiredDate,
				SOD.intBalToFabricInQty,
				(select sum(INQTY) from trn_sampleorder_fabricin_details as SFID1 
				INNER JOIN trn_sampleorder_fabricin_header SFIH1 ON SFIH1.SAMPLE_FABRICIN_NO=SFID1.SAMPLE_FABRICIN_NO AND
				SFIH1.SAMPLE_FABRICIN_YEAR=SFID1.SAMPLE_FABRICIN_YEAR
				WHERE SOD.intSampleOrderNo=SFIH1.SAMPLE_ORDER_NO AND
				SOD.intSampleOrderYear=SFIH1.SAMPLE_ORDER_YEAR AND
				SOD.strCombo = SFID1.COMBO AND
				SOD.intPart = SFID1.PART AND
				SOD.intGrade = SFID1.GRADE AND
				SOD.strSize = SFID1.SIZE AND
				SOD.intSampleTypeId=SFID1.SAMPLETYPE_ID
				AND SFIH1.STATUS =1
				 ) as totInQty_app,
				(select sum(INQTY) from trn_sampleorder_fabricin_details as SFID1 
				INNER JOIN trn_sampleorder_fabricin_header SFIH1 ON SFIH1.SAMPLE_FABRICIN_NO=SFID1.SAMPLE_FABRICIN_NO AND
				SFIH1.SAMPLE_FABRICIN_YEAR=SFID1.SAMPLE_FABRICIN_YEAR
				WHERE SOD.intSampleOrderNo=SFIH1.SAMPLE_ORDER_NO AND
				SOD.intSampleOrderYear=SFIH1.SAMPLE_ORDER_YEAR AND
				SOD.strCombo = SFID1.COMBO AND
				SOD.intPart = SFID1.PART AND
				SOD.intGrade = SFID1.GRADE AND
				SOD.strSize = SFID1.SIZE AND
				SOD.intSampleTypeId=SFID1.SAMPLETYPE_ID
				AND SFIH1.STATUS > 1 AND SFIH1.STATUS <= (SFIH1.APPROVE_LEVELS+1)
				 ) as totInQty_pen 
 				FROM trn_sampleorderdetails SOD
				INNER JOIN mst_part MP ON MP.intId=SOD.intPart
				INNER JOIN mst_sampletypes MST ON MST.intId=SOD.intSampleTypeId
				WHERE SOD.intSampleOrderNo = '$sampleOrderNo' AND
				SOD.intSampleOrderYear = '$sampleOrderYear' /*AND
				SOD.intBalToFabricInQty>0 */ /*temporarily commented on 2016-10-26 sice they need unlimited fabric in facility. */ /* activated the facility again on 2017-10-27 */ ";

		$result = $this->db->RunQuery($sql);
		return $result;
	}
	private function loadHeaderData_sql($fabricInNo,$fabricInYear,$executionType)
	{
		$sql = "SELECT 	SAMPLE_ORDER_NO, 
				SAMPLE_ORDER_YEAR, 
				COMPANY_ID, 
				LOCATION_ID, 
				INDATE, 
				REMARKS,
				UA.strUserName AS CREATOR,
				CREATED_DATE ,
				STATUS, 
				APPROVE_LEVELS ,
				APPROVE_LEVELS AS LEVELS
				FROM 
				trn_sampleorder_fabricin_header 
				INNER JOIN sys_users UA ON UA.intUserId=trn_sampleorder_fabricin_header.CREATED_BY
				WHERE
				SAMPLE_FABRICIN_NO = '$fabricInNo' AND
				SAMPLE_FABRICIN_YEAR = '$fabricInYear' ";
		
		$result = $this->db->$executionType($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row;
	}
	private function getMaxStatus_sql($fabricInNo,$fabricInYear)
	{
		$sql = "SELECT MAX(tb.STATUS) AS maxStatus  
				FROM trn_sampleorder_fabricin_approveby AS tb
				WHERE 
				tb.SAMPLE_FABRICIN_NO='$fabricInNo' AND
				tb.SAMPLE_FABRICIN_YEAR='$fabricInYear' ";
		
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		return $row['maxStatus'];
	}
	private function loadDetailData_sql($fabricInNo,$fabricInYear,$executionType)
	{
		$sql = "SELECT SFID.COMBO, 
				SFID.PART,
				MP.strName AS partName,
				MST.strName AS sampleType, 
				SFID.GRADE, 
				SFID.SIZE,
				SOD.intQty, 
				SFID.INQTY, 
				SFID.SAMPLETYPE_ID, 
				SFID.REQUIRED_DATE,
				SOD.intBalToFabricInQty,
				(select sum(INQTY) from trn_sampleorder_fabricin_details as SFID1 
				INNER JOIN trn_sampleorder_fabricin_header SFIH1 ON SFIH1.SAMPLE_FABRICIN_NO=SFID1.SAMPLE_FABRICIN_NO AND
				SFIH1.SAMPLE_FABRICIN_YEAR=SFID1.SAMPLE_FABRICIN_YEAR
				WHERE SOD.intSampleOrderNo=SFIH1.SAMPLE_ORDER_NO AND
				SOD.intSampleOrderYear=SFIH1.SAMPLE_ORDER_YEAR AND
				SOD.strCombo = SFID1.COMBO AND
				SOD.intPart = SFID1.PART AND
				SOD.intGrade = SFID1.GRADE AND
				SOD.strSize = SFID1.SIZE AND
				SOD.intSampleTypeId=SFID1.SAMPLETYPE_ID
				AND SFIH1.STATUS = 1 
				 ) as totInQty_app,
				(select sum(INQTY) from trn_sampleorder_fabricin_details as SFID1 
				INNER JOIN trn_sampleorder_fabricin_header SFIH1 ON SFIH1.SAMPLE_FABRICIN_NO=SFID1.SAMPLE_FABRICIN_NO AND
				SFIH1.SAMPLE_FABRICIN_YEAR=SFID1.SAMPLE_FABRICIN_YEAR
				WHERE SOD.intSampleOrderNo=SFIH1.SAMPLE_ORDER_NO AND
				SOD.intSampleOrderYear=SFIH1.SAMPLE_ORDER_YEAR AND
				SOD.strCombo = SFID1.COMBO AND
				SOD.intPart = SFID1.PART AND
				SOD.intGrade = SFID1.GRADE AND
				SOD.strSize = SFID1.SIZE AND
				SOD.intSampleTypeId=SFID1.SAMPLETYPE_ID
				AND SFIH1.STATUS > 1 AND SFIH1.STATUS <= (SFIH1.APPROVE_LEVELS+1)
				 ) as totInQty_pen 
				FROM 
				trn_sampleorder_fabricin_details SFID
				INNER JOIN mst_part MP ON MP.intId=SFID.PART
				INNER JOIN mst_sampletypes MST ON MST.intId=SFID.SAMPLETYPE_ID
				INNER JOIN trn_sampleorder_fabricin_header SFIH ON SFIH.SAMPLE_FABRICIN_NO=SFID.SAMPLE_FABRICIN_NO AND
				SFIH.SAMPLE_FABRICIN_YEAR=SFID.SAMPLE_FABRICIN_YEAR
				INNER JOIN trn_sampleorderdetails SOD ON SOD.intSampleOrderNo=SFIH.SAMPLE_ORDER_NO AND
				SOD.intSampleOrderYear=SFIH.SAMPLE_ORDER_YEAR AND
				SOD.strCombo = SFID.COMBO AND
				SOD.intPart = SFID.PART AND
				SOD.intGrade = SFID.GRADE AND
				SOD.strSize = SFID.SIZE AND
				SOD.intSampleTypeId=SFID.SAMPLETYPE_ID
				WHERE SFID.SAMPLE_FABRICIN_NO = '$fabricInNo' AND
				SFID.SAMPLE_FABRICIN_YEAR = '$fabricInYear' ";
		
		$result = $this->db->$executionType($sql);
		return $result;
	}
	private function getRptApproveDetails_sql($fabricInNo,$fabricInYear)
	{
		$sql = "SELECT
				SRAB.APPROVED_BY,
				SRAB.APPROVED_DATE AS dtApprovedDate,
				SU.strUserName AS UserName,
				SRAB.APPROVE_LEVEL_NO AS intApproveLevelNo
				FROM
				trn_sampleorder_fabricin_approveby SRAB
				INNER JOIN sys_users SU ON SRAB.APPROVED_BY = SU.intUserId
				WHERE SRAB.SAMPLE_FABRICIN_NO ='$fabricInNo' AND
				SRAB.SAMPLE_FABRICIN_YEAR ='$fabricInYear'      
				ORDER BY SRAB.APPROVED_DATE ASC ";
		
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	private function getSampleOrderStatus_sql($sampleOrderNo,$sampleOrderYear)
	{
		$sql = "SELECT 	intStatus 
				FROM 
				trn_sampleorderheader 
				WHERE
				intSampleOrderNo = '$sampleOrderNo' AND
				intSampleOrderYear = '$sampleOrderYear' ";
		
		$result = $this->db->RunQuery2($sql);		
		$row	= mysqli_fetch_array($result);
		return $row['intStatus'];
	}
	private function getSampleOrderFabInBalQty_sql($sampleOrderNo,$sampleOrderYear,$combo,$part,$grade,$size,$sampleType,$reqDate)
	{
		$sql = "SELECT intBalToFabricInQty
				FROM trn_sampleorderdetails
				WHERE 
				intSampleOrderNo = '$sampleOrderNo' AND 
				intSampleOrderYear = '$sampleOrderYear' AND 
				strCombo = '$combo' AND 
				intPart = '$part' AND 
				intGrade = '$grade' AND  ";
		
		//if($size!='')
		//$sql .= "strSize = '$size' AND ";
		
		$sql .= "intSampleTypeId = '$sampleType' AND 
				dtRequiredDate = '$reqDate' ";
		
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row;
	}
	private function getSampleFabricInQty_sql($fabricInNo,$fabricInYear)
	{
		$sql = "SELECT COMBO, 
				PART, 
				GRADE, 
				SIZE, 
				INQTY, 
				SAMPLETYPE_ID, 
				REQUIRED_DATE
				FROM trn_sampleorder_fabricin_details
				WHERE SAMPLE_FABRICIN_NO = '$fabricInNo' AND
				SAMPLE_FABRICIN_YEAR = '$fabricInYear' ";
		
		$result = $this->db->RunQuery2($sql);
		return $result;
	}
	public function getRequsitionNo_sql($sampleNo,$sampleYear)
	{
		$sql = "SELECT intSampleReqNo,intSampleReqYear
				FROM trn_sampleorderheader
				WHERE intSampleOrderNo = '$sampleNo' AND
				intSampleOrderYear = '$sampleYear' ";
		
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row;
	}
	public function getSampleFabricInDetail_sql($fabricInNo,$fabricInYear)
	{
		$sql = "SELECT COMBO, 
				PART, 
				GRADE, 
				SIZE, 
				INQTY, 
				SAMPLETYPE_ID, 
				REQUIRED_DATE,
				SFIH.SAMPLE_ORDER_NO,
				SFIH.SAMPLE_ORDER_YEAR 
				FROM trn_sampleorder_fabricin_details SIFD
				INNER JOIN trn_sampleorder_fabricin_header SFIH ON SFIH.SAMPLE_FABRICIN_NO=SIFD.SAMPLE_FABRICIN_NO AND
				SFIH.SAMPLE_FABRICIN_YEAR=SIFD.SAMPLE_FABRICIN_YEAR
				WHERE SIFD.SAMPLE_FABRICIN_NO='$fabricInNo' AND
				SIFD.SAMPLE_FABRICIN_YEAR='$fabricInYear' AND
				SFIH.STATUS = 1 ";
		
		$result = $this->db->RunQuery2($sql);
		return $result;
	}
	public function checkDispatchRaised_sql($sampleOrderNo,$sampleOrderYear,$combo,$part,$grade,$size,$sampleTypeId,$requireDate)
	{
		$sql = "SELECT intDispatchNo,intDispatchYear
				FROM trn_sampledispatchdetails
				WHERE SAMPLE_ORDER_NO = '$sampleOrderNo' AND
				SAMPLE_ORDER_YEAR = '$sampleOrderYear' AND
				strCombo = '$combo' AND
				intPartId = '$part' AND
				intGrade = '$grade' AND
				intSampleTypeId = '$sampleTypeId' AND
				strSize = '$size' AND
				dtDeliveryDate = '$requireDate' ";
		
		$result = $this->db->RunQuery2($sql);
		return $result;
	}	
	public function load_fabricIn_details_for_requesition($fabricInNo,$fabricInYear,$SRdb,$Maindb,$executeType){
		$sql	="
				SELECT 
				SRH.GRAPHIC,
				SRD.SAMPLE_TYPE,
				SFIH.CREATED_DATE AS transacDate,
				SRH.CREATED_BY AS createdBy,
				SRH.MODIFY_BY AS modifyBy,
				SFID.INQTY AS Qty,
				'FABRICIN' AS transacType
				FROM $Maindb.trn_sampleorder_fabricin_header SFIH
				INNER JOIN $Maindb.trn_sampleorder_fabricin_details SFID ON SFIH.SAMPLE_FABRICIN_YEAR=SFID.SAMPLE_FABRICIN_YEAR AND
				SFIH.SAMPLE_FABRICIN_NO=SFID.SAMPLE_FABRICIN_NO
				INNER JOIN $Maindb.trn_sampleorderdetails SOD ON SOD.intSampleOrderNo=SFIH.SAMPLE_ORDER_NO AND
				SOD.intSampleOrderYear=SFIH.SAMPLE_ORDER_YEAR AND
				SOD.strCombo=SFID.COMBO AND
				SOD.intPart=SFID.PART AND
				SOD.intGrade=SFID.GRADE AND
				SOD.strSize=SFID.SIZE AND
				SOD.intSampleTypeId=SFID.SAMPLETYPE_ID AND
				SOD.dtRequiredDate=SFID.REQUIRED_DATE
				INNER JOIN $Maindb.trn_sampleorderheader SOH ON SOH.intSampleOrderNo=SOD.intSampleOrderNo AND
				SOH.intSampleOrderYear=SOD.intSampleOrderYear
				INNER JOIN $SRdb.trn_sample_requisition_header SRH ON SRH.REQUISITION_NO=SOH.intSampleReqNo AND
				SRH.REQUISITION_YEAR=SOH.intSampleReqYear
				INNER JOIN $SRdb.trn_sample_requisition_detail SRD ON SRH.REQUISITION_NO=SRD.REQUISITION_NO AND
				SRH.REQUISITION_YEAR=SRD.REQUISITION_YEAR AND
				SOD.intSampleTypeId=SRD.SAMPLE_TYPE
				WHERE 
				SFIH.STATUS = 1
				AND SFIH.SAMPLE_FABRICIN_NO = '$fabricInNo' 
				AND SFIH.SAMPLE_FABRICIN_YEAR = '$fabricInYear'
				AND SRD.FABRIC_IN_QTY > 0
				AND SRD.QTY > 0
				GROUP BY SRD.SAMPLE_TYPE,SFIH.CREATED_DATE
				ORDER BY SRD.SAMPLE_TYPE
				LIMIT 0,2
		";
		//echo $sql;
		$result = $this->db->$executeType($sql);
		//$row	= mysqli_fetch_array($result);
		//print_r($row);
		return $result;
		
	}
	
}
?>
<?php

class Cls_sample_fabricin_set
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	public function saveHeader($fabricInNo,$fabricInYear,$fabricInDate,$sampleOrderNo,$sampleOrderYear,$remarks,$locationId,$companyId,$userId,$status,$approveLevels)
	{
		return $this->saveHeader_sql($fabricInNo,$fabricInYear,$fabricInDate,$sampleOrderNo,$sampleOrderYear,$remarks,$locationId,$companyId,$userId,$status,$approveLevels);
	}
	public function updateHeader($fabricInNo,$fabricInYear,$fabricInDate,$sampleOrderNo,$sampleOrderYear,$remarks,$locationId,$companyId,$userId,$status,$approveLevels)
	{
		return $this->updateHeader_sql($fabricInNo,$fabricInYear,$fabricInDate,$sampleOrderNo,$sampleOrderYear,$remarks,$locationId,$companyId,$userId,$status,$approveLevels);
	}
	public function updateApproveByStatus($fabricInNo,$fabricInYear,$maxStatus)
	{
		return $this->updateApproveByStatus_sql($fabricInNo,$fabricInYear,$maxStatus);
	}
	public function deleteDetails($fabricInNo,$fabricInYear)
	{
		return $this->deleteDetails_sql($fabricInNo,$fabricInYear);
	}
	public function saveDetails($fabricInNo,$fabricInYear,$fabricInQty,$combo,$partId,$sampleTypeId,$grade,$size,$reqDate)
	{
		return $this->saveDetails_sql($fabricInNo,$fabricInYear,$fabricInQty,$combo,$partId,$sampleTypeId,$grade,$size,$reqDate);
	}
	public function updateHeaderStatus($fabricInNo,$fabricInYear,$para)
	{
		return $this->updateHeaderStatus_sql($fabricInNo,$fabricInYear,$para);
	}
	public function approved_by_insert($fabricInNo,$fabricInYear,$userId,$approval)
	{
		return $this->approved_by_insert_sql($fabricInNo,$fabricInYear,$userId,$approval);
	}
	public function updateFafbricInBalQty($sampleOrderNo,$sampleOrderYear,$combo,$part,$grade,$size,$sampleType,$reqDate,$inQty,$mode)
	{
		return $this->updateFafbricInBalQty_sql($sampleOrderNo,$sampleOrderYear,$combo,$part,$grade,$size,$sampleType,$reqDate,$inQty,$mode);
	}
	public function updateFabInDateinRequsition($sampleReqNo,$sampleReqYear,$sampleTypeId,$fabInDate,$qty,$mode,$SRdb)
	{
		return $this->updateFabInDateinRequsition_sql($sampleReqNo,$sampleReqYear,$sampleTypeId,$fabInDate,$qty,$mode,$SRdb);
	}
	public function saveLogQueries($SRdb,$graphc,$samp_type,$trns_type,$created_by,$modified_by,$log_date,$executeType)
	{
		return $this->saveLogQueries_sql($SRdb,$graphc,$samp_type,$trns_type,$created_by,$modified_by,$log_date,$executeType);
	}
	
	private function saveHeader_sql($fabricInNo,$fabricInYear,$fabricInDate,$sampleOrderNo,$sampleOrderYear,$remarks,$locationId,$companyId,$userId,$status,$approveLevels)
	{
	    $data = array();
		$sql = "INSERT INTO trn_sampleorder_fabricin_header 
				(
				SAMPLE_FABRICIN_NO, 
				SAMPLE_FABRICIN_YEAR, 
				SAMPLE_ORDER_NO, 
				SAMPLE_ORDER_YEAR, 
				COMPANY_ID, 
				LOCATION_ID, 
				INDATE, 
				REMARKS, 
				STATUS, 
				APPROVE_LEVELS, 
				CREATED_BY, 
				CREATED_DATE
				)
				VALUES
				(
				'$fabricInNo', 
				'$fabricInYear', 
				'$sampleOrderNo', 
				'$sampleOrderYear', 
				'$companyId', 
				'$locationId', 
				'$fabricInDate', 
				'$remarks', 
				'$status', 
				'$approveLevels', 
				'$userId', 
				NOW()
				) ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function updateHeader_sql($fabricInNo,$fabricInYear,$fabricInDate,$sampleOrderNo,$sampleOrderYear,$remarks,$locationId,$companyId,$userId,$status,$approveLevels)
	{
		$sql = "UPDATE trn_sampleorder_fabricin_header 
				SET
				SAMPLE_ORDER_NO = '$sampleOrderNo' , 
				SAMPLE_ORDER_YEAR = '$sampleOrderYear' , 
				COMPANY_ID = '$companyId' , 
				LOCATION_ID = '$locationId' , 
				INDATE = '$fabricInDate' , 
				REMARKS = '$remarks' , 
				STATUS = '$status' , 
				APPROVE_LEVELS = '$approveLevels' , 
				MODIFY_BY = '$userId'
				WHERE
				SAMPLE_FABRICIN_NO = '$fabricInNo' AND 
				SAMPLE_FABRICIN_YEAR = '$fabricInYear' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function updateApproveByStatus_sql($fabricInNo,$fabricInYear,$maxStatus)
	{
		$sql = "UPDATE trn_sampleorder_fabricin_approveby 
				SET
				STATUS = $maxStatus+1
				WHERE
				SAMPLE_FABRICIN_NO = '$fabricInNo' AND 
				SAMPLE_FABRICIN_YEAR = '$fabricInYear' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function deleteDetails_sql($fabricInNo,$fabricInYear)
	{
		$sql = "DELETE FROM trn_sampleorder_fabricin_details 
				WHERE
				SAMPLE_FABRICIN_NO = '$fabricInNo' AND 
				SAMPLE_FABRICIN_YEAR = '$fabricInYear' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function saveDetails_sql($fabricInNo,$fabricInYear,$fabricInQty,$combo,$partId,$sampleTypeId,$grade,$size,$reqDate)
	{
		if(!$size)
			$size='';
		if($size=='&nbsp;')
			$size='';

		$data = array();
		
		$sql = "INSERT INTO trn_sampleorder_fabricin_details 
				(
				SAMPLE_FABRICIN_NO, 
				SAMPLE_FABRICIN_YEAR, 
				COMBO, 
				PART, 
				GRADE, 
				SIZE, 
				INQTY, 
				SAMPLETYPE_ID, 
				REQUIRED_DATE
				)
				VALUES
				(
				'$fabricInNo', 
				'$fabricInYear', 
				'$combo', 
				'$partId', 
				'$grade', 
				'$size', 
				'$fabricInQty', 
				'$sampleTypeId', 
				'$reqDate'
				)";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function updateHeaderStatus_sql($fabricInNo,$fabricInYear,$para)
	{
		$sql = "UPDATE trn_sampleorder_fabricin_header 
				SET
				STATUS = $para
				WHERE
				SAMPLE_FABRICIN_NO = '$fabricInNo' AND 
				SAMPLE_FABRICIN_YEAR = '$fabricInYear' ";

		$data = array();
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function approved_by_insert_sql($fabricInNo,$fabricInYear,$userId,$approval)
	{
	    $data = array();
		$sql = "INSERT INTO trn_sampleorder_fabricin_approveby 
				(
				SAMPLE_FABRICIN_NO, 
				SAMPLE_FABRICIN_YEAR, 
				APPROVE_LEVEL_NO, 
				APPROVED_BY, 
				APPROVED_DATE, 
				STATUS
				)
				VALUES
				(
				'$fabricInNo', 
				'$fabricInYear', 
				'$approval', 
				'$userId', 
				NOW(), 
				'0'
				) ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function updateFafbricInBalQty_sql($sampleOrderNo,$sampleOrderYear,$combo,$part,$grade,$size,$sampleType,$reqDate,$inQty,$mode)
	{
	    $data = array();
		if($mode=='approve')
		{
			$updatePart = "intBalToFabricInQty = intBalToFabricInQty - $inQty, ";
			$updatePart.= "intBalToDeliverQty = intBalToDeliverQty + $inQty ";
		}	
		else
		{
			$updatePart = "intBalToFabricInQty = intBalToFabricInQty + $inQty, ";
			$updatePart.= "intBalToDeliverQty = intBalToDeliverQty - $inQty ";
		}
			
		$sql = "UPDATE trn_sampleorderdetails 
				SET 
				$updatePart 
				WHERE
				intSampleOrderNo = '$sampleOrderNo' AND 
				intSampleOrderYear = '$sampleOrderYear' AND 
				strCombo = '$combo' AND 
				intPart = '$part' AND 
				intGrade = '$grade' AND 
				strSize = '$size' AND 
				intSampleTypeId = '$sampleType' AND 
				dtRequiredDate = '$reqDate' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function updateFabInDateinRequsition_sql($sampleReqNo,$sampleReqYear,$sampleTypeId,$fabInDate,$qty,$mode,$SRdb)
	{
		if($mode=='approve')
		{
			$updatePart = "FABRIC_IN_DATE = '$fabInDate', ";
			$updatePart.= "FABRIC_IN_QTY = FABRIC_IN_QTY + $qty ";
		}
		else
		{
			$updatePart = "FABRIC_IN_DATE = NULL, ";
			$updatePart.= "FABRIC_IN_QTY = FABRIC_IN_QTY - $qty ";
		}
		
		$sql = "UPDATE $SRdb.trn_sample_requisition_detail 
				SET 
				$updatePart 
				WHERE
				REQUISITION_NO = '$sampleReqNo' AND 
				REQUISITION_YEAR = '$sampleReqYear' AND 
				SAMPLE_TYPE = '$sampleTypeId' ";
		
		$result 		= $this->db->RunQuery2($sql);
		$result_ipad 	= $this->db->RunQuery_S($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	
	private function saveLogQueries_sql($SRdb,$graphc,$samp_type,$trns_type,$created_by,$modified_by,$log_date,$executeType){
	
			if($modified_by=='') 
			$modified_by 	= 'NULL';
 			else
			$modified_by 	= "'".$modified_by."'";
			
				$sql_log = "INSERT INTO $SRdb.dashboard_event_log
							(
							GRAPHIC_NO, 
							SAMPLE_TYPE_ID, 
							TYPE, 
							CREATED_BY, 
							MODIFY_BY, 
							LOG_DATE_TIME
							)
							VALUES
							(
							'$graphc', 
							'$samp_type', 
							'$trns_type', 
							'$created_by', 
							$modified_by, 
							'$log_date') ";
				
				$result		 	= $this->db->RunQuery2($sql_log);
				if(!$result){
					$data['savedStatus']	= 'fail';
					$data['savedMassege']	= $this->db->errormsg;
					$data['error_sql']		= $sql_log;
				}
				$result_ipad 	= $this->db->RunQuery_S($sql_log);// save in queries_sample
				if(!$result_ipad){
					$data['savedStatus']	= 'fail';
					$data['savedMassege']	= $this->db->errormsg;
					$data['error_sql']		= $sql_log;
				}
		return $data;
	}
	
	
}
?>
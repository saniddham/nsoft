<?php

class cls_sample_approval_db
{
	
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}

 	
	public function trn_sampleinfomations_select($sampleNo, $sampleYear, $revisionNo)
	{
		$sql1	= "SELECT 
					trn_sampleinfomations.strGraphicRefNo
 				FROM 
					trn_sampleinfomations
				WHERE
					trn_sampleinfomations.intSampleNo 	=  '$sampleNo' AND
					trn_sampleinfomations.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations.intRevisionNo =  '$revisionNo'
					";
		return $sql1; 
	}
	
	public function trn_sampleinfomations_graphic_select($sampleYear,$companyId)
	{
 		$sql = "SELECT DISTINCT
					trn_sampleinfomations.strGraphicRefNo
				FROM 
					trn_sampleinfomations
				WHERE
					
					trn_sampleinfomations.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations.intCompanyId =  '$companyId'  
				ORDER BY strGraphicRefNo
				";
		return $sql; 
	}

	public function trn_sampleinfomations_sampleNo_select($sampleYear, $location, $graphic, $style)
	{
		$sql = "SELECT DISTINCT
						trn_sampleinfomations.intSampleNo
				FROM 
						trn_sampleinfomations
				WHERE
						trn_sampleinfomations.intSampleYear =  '$sampleYear' AND intCompanyId = '$location'";
				if($graphic!='')	
				$sql .= " AND strGraphicRefNo = '$graphic' ";	
				if($style!='')
				$sql .= " AND trn_sampleinfomations.strGraphicRefNo =  '$style'";
			
				$sql .= "ORDER BY
						trn_sampleinfomations.intSampleNo ASC
				";
		return $sql; 
	}

	public function trn_sampleinfomations_revision_select($sampleYear, $sampleNo,$companyId)
	{
	   $sql = "SELECT
					trn_sampleinfomations.intRevisionNo
				FROM 
					trn_sampleinfomations
				WHERE
					trn_sampleinfomations.intSampleNo =  '$sampleNo' AND
					trn_sampleinfomations.intSampleYear =  '$sampleYear' AND 
					trn_sampleinfomations.intCompanyId =  '$companyId'
			";
		return $sql; 
	}
	
	public function trn_sampleinfomations_combo_select($sampleNo, $sampleYear, $revisionNo)
	{
		$sql1	= "SELECT DISTINCT
						trn_sampleinfomations_details.strComboName
					FROM `trn_sampleinfomations_details`
					WHERE
						trn_sampleinfomations_details.intSampleNo = '$sampleNo' AND
						trn_sampleinfomations_details.intSampleYear = '$sampleYear' AND
						trn_sampleinfomations_details.intRevNo = '$revisionNo'
					ORDER BY
						trn_sampleinfomations_details.strComboName ASC
					";
		return $sql1; 
	}
	
	public function trn_sampleinfomations_sampleDetails_select($sampleYear, $sampleNo, $location)
	{
		$sql = "SELECT
					trn_sampleinfomations_printsize.strPrintName,
					trn_sampleinfomations_printsize.intRevisionNo
					FROM trn_sampleinfomations_printsize
				WHERE
					trn_sampleinfomations_printsize.intSampleNo =  '$sampleNo' AND
					trn_sampleinfomations_printsize.intSampleYear =  '$sampleYear' AND
					trn_sampleinfomations_printsize.intRevisionNo=( select max(A.intRevisionNo) from trn_sampleinfomations_printsize A where A.intSampleNo = '$sampleNo' and A.intSampleyear='$sampleYear') AND
					trn_sampleinfomations_printsize.intCompanyId =  '$location'
				ORDER BY
					trn_sampleinfomations_printsize.intSampleYear ASC,
					trn_sampleinfomations_printsize.intSampleNo ASC
				";
		return $sql; 
	}
	
	public function getSampleApprovalRecordSavedStatus($sampleNo,$sampleYear,$revNo,$combo,$print)
	{
		$sql="SELECT * 
			FROM 
				trn_sampleinfomations_approval 
			WHERE
				trn_sampleinfomations_approval.intSampleNo =  '$sampleNo' AND
				trn_sampleinfomations_approval.intSampleYear =  '$sampleYear' AND
				trn_sampleinfomations_approval.intRevisionNo =  '$revNo' AND 
				trn_sampleinfomations_approval.strCombo = '$combo' AND 
				`trn_sampleinfomations_approval`.strPrintName = '$print'";
		return $sql; 
	}
	public function trn_sampleinfomations_approval_update($sampleNo,$sampleYear,$revNo,$combo,$print,$approved,$userId)
	{
		$sql = "UPDATE   
					`trn_sampleinfomations_approval` 
				SET 
					`intApproved`='".$approved."',
					`dtModifiedDate`=now() ,
					`intModifiedBy`='$userId' 
				WHERE
					`trn_sampleinfomations_approval`.intSampleNo =  '$sampleNo' AND
					`trn_sampleinfomations_approval`.intSampleYear =  '$sampleYear' AND
					`trn_sampleinfomations_approval`.intRevisionNo =  '$revNo' AND 
					`trn_sampleinfomations_approval`.strCombo = '$combo' AND 
					`trn_sampleinfomations_approval`.strPrintName = '$print'";
 		return $sql; 
	}
	public function trn_sampleinfomations_approval_insert($sampleNo,$sampleYear,$revNo,$combo,$print,$approved,$userId)
	{
		$sql = "INSERT INTO 
					`trn_sampleinfomations_approval` 
					(
						`intSampleNo`,
						`intSampleYear`,
						`intRevisionNo`,
						`strCombo`,
						`strPrintName`,
						`intApproved`,
						`dtEnterDate`,
						`intEnterBy`
					)
			VALUES (
						'$sampleNo',
						'$sampleYear',
						'$revNo',
						'".$combo."',
						'".$print."',
						'".$approved."',
						now(),
						".$userId."
					)";	
		return $sql; 
	}
	
	
	
}
?>
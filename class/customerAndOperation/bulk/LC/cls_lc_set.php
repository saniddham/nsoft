<?php
class Cls_lc_set{
	
	private $db;
		
	function __construct($db)
	{
		$this->db = $db;	
	}
	
	public function insertHeader($serialNo,$serialYear,$lcNo,$bankRefNo,$customerId,$orderNo,$orderYear,$bankId,$currencyId,$lcAmount,$lcDate,$remarks,$status,$approveLevels,$userId,$executeType)
	{
		return $this->insertHeader_sql($serialNo,$serialYear,$lcNo,$bankRefNo,$customerId,$orderNo,$orderYear,$bankId,$currencyId,$lcAmount,$lcDate,$remarks,$status,$approveLevels,$userId,$executeType);
	}
	public function updateHeader($serialNo,$serialYear,$lcNo,$bankRefNo,$customerId,$orderNo,$orderYear,$bankId,$currencyId,$lcAmount,$lcDate,$remarks,$userId,$executeType)
	{
		return $this->updateHeader_sql($serialNo,$serialYear,$lcNo,$bankRefNo,$customerId,$orderNo,$orderYear,$bankId,$currencyId,$lcAmount,$lcDate,$remarks,$userId,$executeType);
	}
	
	public function update_header_status($serialYear,$serialNo,$status,$executeType)
	{ 

   		$sql	= $this->update_header_status_sql($serialYear,$serialNo,$status);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		}
			return $response;
 	}
	
	
	
	public function approved_by_insert($serialYear,$serialNo,$userId,$approval,$executeType)
	{ 

   		$sql	= $this->approved_by_insert_sql($serialYear,$serialNo,$userId,$approval);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		}
			return $response;
 	}
	
	public function approved_by_update($serialYear,$serialNo,$maxAppByStatus,$executeType)
	{ 

   		$sql	= $this->approved_by_update_sql($serialYear,$serialNo,$maxAppByStatus);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		}
			return $response;
 	}
	
	
//BEGIN - PRIVATE FUNTIONS	
	private function insertHeader_sql($serialNo,$serialYear,$lcNo,$bankRefNo,$customerId,$orderNo,$orderYear,$bankId,$currencyId,$lcAmount,$lcDate,$remarks,$status,$approveLevels,$userId,$executeType)
	{
		$sql = "INSERT INTO trn_lc_header
							(LC_SERIAL_NO,
							 LC_SERIAL_YEAR,
							 LC_NO,
							 BANK_REF_NO,
							 CUSTOMER_ID,
							 ORDER_NO,
							 ORDER_YEAR,
							 BANK_ID,
							 CURRENCY,
							 LC_AMOUNT,
							 LC_DATE,
							 REMARKS,
							 STATUS,
							 LEVELS,
							 CREATED_BY,
							 CREATED_DATE)
				VALUES ('$serialNo',
						'$serialYear',
						'$lcNo',
						'$bankRefNo',
						'$customerId',
						'$orderNo',
						'$orderYear',
						'$bankId',
						'$currencyId',
						'$lcAmount',
						'$lcDate',
						'$remarks',
						$approveLevels+1,
						'$approveLevels',
						'$userId',
						 NOW());";
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= false;
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= true;
			$response['msg']	= "Saved successfully!";
			$response['sql']	= $sql;
		}
			return $response;
	}
	
	private function updateHeader_sql($serialNo,$serialYear,$lcNo,$bankRefNo,$customerId,$orderNo,$orderYear,$bankId,$currencyId,$lcAmount,$lcDate,$remarks,$userId,$executeType)
	{
		$sql = "UPDATE trn_lc_header
				SET LC_NO = '$lcNo',
				  BANK_REF_NO = '$bankRefNo',
				  CUSTOMER_ID = '$customerId',
				  ORDER_NO = '$orderNo',
				  ORDER_YEAR = '$orderYear',
				  BANK_ID = '$bankId',
				  CURRENCY = '$currencyId',
				  LC_AMOUNT = '$lcAmount',
				  LC_DATE = '$lcDate',
				  REMARKS = '$remarks',
				  STATUS = LEVELS+1,
				  LEVELS = LEVELS,
				  MODIFIED_BY = '$userId',
				  MODIFIED_DATE = NOW()
				WHERE LC_SERIAL_NO = '$serialNo'
					AND LC_SERIAL_YEAR = '$serialYear';";
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= false;
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= true;
			$response['msg']	= "Update successfully!";
			$response['sql']	= $sql;
		}
			return $response;
		
	}
	
	private function update_header_status_sql($serialYear,$serialNo,$status)
	{
 		
		$sql = "UPDATE trn_lc_header 
				SET 
				STATUS='$status' 
				WHERE
				trn_lc_header.LC_SERIAL_NO = '$serialNo' AND
				trn_lc_header.LC_SERIAL_YEAR = '$serialYear'";
 		return $sql;
	}
	
	private function approved_by_insert_sql($serialYear,$serialNo,$userId,$approval){
 		
		$sql = "INSERT INTO `trn_lc_header_approved_by` (`LC_SERIAL_NO`,`LC_SERIAL_YEAR`,`LEVEL`,USER,DATE,STATUS) 
			VALUES ('$serialNo','$serialYear','$approval','$userId',now(),0)";
 		
		return $sql;
	}
	
	
	private function approved_by_update_sql($serialYear,$serialNo,$maxAppByStatus){
 		
		  	$sql = "UPDATE `trn_lc_header_approved_by` SET STATUS ='$maxAppByStatus' 
					WHERE (`LC_SERIAL_NO`='$serialNo') AND (`LC_SERIAL_YEAR`='$serialYear') AND (`STATUS`='0')";

		return $sql;
	}
 }
//END - PRIVATE FUNCTION
?>
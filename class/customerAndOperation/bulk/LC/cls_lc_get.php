<?php
class Cls_lc_get{
	
	private $db;
		
	function __construct($db)
	{
		$this->db = $db;	
	}
 	public function getOrderYear($company)
	{
		return $this->getOrderYear_sql($company);
	}
	public function getCurrency()
	{
		return $this->getCurrency_sql();
	}
	public function getCompanyBaseCurrency($company)
	{
		return $this->getCompanyBaseCurrency_sql($company);
	}
	public function getOrderNo($orderYear,$customer,$companyId)
	{
		return $this->getOrderNo_sql($orderYear,$customer,$companyId);
	}
	public function get_header($serialNo,$serialYear,$executeType)
	{
		$sql	= $this->get_header_sql($serialNo,$serialYear);
		$row	= $this->get_header_row($sql,$executeType);
		return $row;
	}
	
	public function get_lc_header_for_order($serialNo,$serialYear,$executeType)
	{
		$sql	= $this->get_lc_header_for_order_sql($serialNo,$serialYear);
		$row	= $this->get_lc_header_for_order_row($sql,$executeType);
		return $row;
	}
	
	public function getCustomer()
	{
		return $this->getCustomer_sql();
	}
	public function getBank()
	{
		return $this->getBank_sql();
	}
	public function getCustomerId($orderYear,$orderNo)
	{
		return $this->getCustomerId_sql($orderYear,$orderNo);
	}
	public function get_max_approved_by_status($serialYear,$serialNo,$executeType)
	{
		$sql	= $this->get_max_approved_by_status_sql($serialYear,$serialNo);
		$result	= $this->db->$executeType($sql);
		$row	=mysqli_fetch_array($result);
		return $row['maxStatus'];	
	}

	public function get_listing_sql($programCode,$approveLevel,$intUser,$where_string)
	{
		$sql	= $this->load_listing_sql($programCode,$approveLevel,$intUser,$where_string);
		return $sql;
	}
	public function getCustomerYear($customer,$companyId)
	{
		return $this->getCustomerYear_sql($customer,$companyId);
	}
	private function getOrderYear_sql($company)
	{
		$sql = "SELECT DISTINCT OH.intOrderYear
				FROM trn_orderheader OH
				INNER JOIN mst_locations LO ON LO.intId = OH.intLocationId
				WHERE OH.intStatus = 1 
				AND LC_STATUS = 6
				AND LO.intCompanyId = '$company'
				ORDER BY OH.intOrderYear";
		
		$result	= $this->db->RunQuery($sql);
		return $result;
	}	
	public function get_Report_approval_details_result($serialYear,$serialNo,$executeType){
		$sql = $this->Load_Report_approval_details_sql($serialYear,$serialNo);
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	
	
  
	private function get_header_sql($serialNo,$serialYear){
		
		$sql	= "SELECT
					trn_lc_header.LC_SERIAL_NO,
					trn_lc_header.LC_SERIAL_YEAR,
					trn_lc_header.LC_NO,
					trn_lc_header.BANK_REF_NO,
					mst_bank.BANK_NAME,
					trn_lc_header.CUSTOMER_ID,
					trn_lc_header.ORDER_NO,
					trn_lc_header.ORDER_YEAR,
					trn_lc_header.BANK_ID,
					trn_lc_header.CURRENCY AS CURRENCY_ID,
					trn_lc_header.LC_AMOUNT,
					trn_lc_header.LC_DATE,
					trn_lc_header.REMARKS,
					trn_lc_header.`STATUS`,
					trn_lc_header.LEVELS,
					trn_lc_header.CREATED_BY,
					trn_lc_header.CREATED_DATE,
					trn_lc_header.MODIFIED_BY,
					trn_lc_header.MODIFIED_DATE,
					mst_customer.strName AS CUSTOMER ,
					sys_users.strUserName AS USER,
					mst_bank.BANK_NAME,
					mst_financecurrency.strCode as CURRENCY , 
					trn_orderheader.intStatus as ORDER_STATUS ,
					trn_orderheader.LC_STATUS 
					FROM
					trn_lc_header
					INNER JOIN mst_customer ON trn_lc_header.CUSTOMER_ID = mst_customer.intId
					INNER JOIN sys_users ON trn_lc_header.CREATED_BY = sys_users.intUserId
					INNER JOIN mst_bank ON trn_lc_header.BANK_ID = mst_bank.BANK_ID
					INNER JOIN mst_financecurrency ON trn_lc_header.CURRENCY = mst_financecurrency.intId 
					INNER JOIN trn_orderheader ON  trn_orderheader.intOrderNo= trn_lc_header.ORDER_NO AND trn_orderheader.intOrderYear = trn_lc_header.ORDER_YEAR 
					WHERE
					trn_lc_header.LC_SERIAL_NO = '$serialNo' AND
					trn_lc_header.LC_SERIAL_YEAR = '$serialYear'";
		
		return $sql;
		
	}

	private function get_header_row($sql,$executeType){
	
		$result	= $this->db->$executeType($sql);
		$row=mysqli_fetch_array($result);
		return $row;		
	}
	
	private function getCustomer_sql()
	{
		$sql	=  "SELECT
					mst_customer.intId,
					mst_customer.strName
					FROM
					mst_customer
					WHERE intStatus = 1
					ORDER BY
					mst_customer.strName";	
		
		$result	= $this->db->RunQuery($sql);
		return $result;
	}
	private function getBank_sql()
	{
		$sql	= " SELECT
					BANK_ID,
					BANK_NAME
					FROM
					mst_bank
					WHERE STATUS = 1
					ORDER BY
					BANK_NAME
					";	
		
		$result	= $this->db->RunQuery($sql);
		return $result;
	}
 
	private function get_lc_header_for_order_sql($serialNo,$serialYear){
	
		$sql	="SELECT
				trn_lc_header.LC_SERIAL_NO,
				trn_lc_header.LC_SERIAL_YEAR,
				trn_lc_header.LC_NO,
				trn_lc_header.BANK_REF_NO,
				trn_lc_header.CUSTOMER_ID,
				trn_lc_header.ORDER_NO,
				trn_lc_header.ORDER_YEAR,
				trn_lc_header.BANK_ID,
				trn_lc_header.CURRENCY,
				trn_lc_header.LC_AMOUNT,
				trn_lc_header.LC_DATE,
				trn_lc_header.REMARKS,
				trn_lc_header.`STATUS`,
				trn_lc_header.LEVELS,
				trn_lc_header.CREATED_BY,
				trn_lc_header.CREATED_DATE,
				trn_lc_header.MODIFIED_BY,
				trn_lc_header.MODIFIED_DATE
				FROM `trn_lc_header`
				WHERE
				trn_lc_header.ORDER_NO = '$serialNo' AND
				trn_lc_header.ORDER_YEAR = '$serialYear' AND 
				trn_lc_header.`STATUS` >= (trn_lc_header.LEVELS+1)
				";
						
		return $sql;	
	}
	
	private function get_lc_header_for_order_row($sql,$executeType){
		
		$result	= $this->db->$executeType($sql);
		$row=mysqli_fetch_array($result);
		return $row;	
			
	}
	private function getCurrency_sql()
	{
		$sql = "SELECT intId,strCode
				FROM mst_financecurrency
				WHERE intStatus = 1
				ORDER BY strCode ";
		
		$result	= $this->db->RunQuery($sql);
		return $result;
	}
	private function getCompanyBaseCurrency_sql($company)
	{
		$sql = "SELECT intBaseCurrencyId
				FROM mst_companies
				WHERE intId = '$company' ";
		
		$result	= $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row['intBaseCurrencyId'];
	}
	public function getOrderNo_sql($orderYear,$customer,$companyId)
	{
		$sql = "SELECT OH.intOrderNo
				FROM trn_orderheader OH
				INNER JOIN mst_locations LO ON LO.intId = OH.intLocationId
				WHERE OH.intStatus = 1
				AND LC_STATUS = 6
				AND LO.intCompanyId = '$companyId' ";
		if($customer!='')
			$sql.= "AND OH.intCustomer = '$customer' ";
				
		$sql.= "AND OH.intOrderYear = '$orderYear'
			    ORDER BY OH.intOrderYear,OH.intOrderNo ";
		
		$result	= $this->db->RunQuery($sql);
		return $result;
	}
	
	private function get_max_approved_by_status_sql($serialNo,$serialYear){
		
		$sql	="SELECT
					IFNULL(Max(trn_lc_header_approved_by.`STATUS`),0) as maxStatus
					FROM `trn_lc_header_approved_by`
					WHERE
					trn_lc_header_approved_by.LC_SERIAL_NO = '$serialNo' AND
					trn_lc_header_approved_by.LC_SERIAL_YEAR = '$serialYear'
				";
		
		return $sql;
		
	}
	
	private function load_listing_sql($programCode,$approveLevel,$intUser,$where_string){

		$sql = "select * from(SELECT DISTINCT 
			if(tb1.STATUS=1,'Approved',if(tb1.STATUS=0,'Rejected',if(tb1.STATUS=-2,'Cancelled','Pending'))) as Status,
			tb1.LC_SERIAL_NO,
			tb1.LC_SERIAL_YEAR,
			CONCAT(tb1.ORDER_NO,' / ',tb1.ORDER_YEAR) AS ORDER_NO,
			mst_customer.strName AS CUSTOMER ,
			mst_financecurrency.strCode as CURRENCY , 
			tb1.LC_AMOUNT,
			tb1.LC_DATE,
			sys_users.strUserName AS USER , 
			IFNULL((
											SELECT
				concat(sys_users.strUserName,'(',max(trn_lc_header_approved_by.DATE),')' )
				FROM
				trn_lc_header_approved_by
				Inner Join sys_users ON trn_lc_header_approved_by.USER = sys_users.intUserId
				WHERE
				trn_lc_header_approved_by.LC_SERIAL_NO  = tb1.LC_SERIAL_NO AND
				trn_lc_header_approved_by.LC_SERIAL_YEAR =  tb1.LC_SERIAL_YEAR AND
				trn_lc_header_approved_by.LEVEL = '1' AND 
				trn_lc_header_approved_by.STATUS = '0' 
			),IF(((SELECT
				menupermision.int1Approval 
				FROM menupermision 
				Inner Join menus ON menupermision.intMenuId = menus.intId
				WHERE
				menus.strCode = '$programCode' AND
				menupermision.intUserId =  '$intUser')=1 AND tb1.STATUS>1),'Approve', '')) as `1st_Approval`,  ";
			
		for($i=2; $i<=$approveLevel; $i++){
			
			if($i==2){
			$approval="2nd_Approval";
			}
			else if($i==3){
			$approval="3rd_Approval";
			}
			else {
			$approval=$i."th_Approval";
			}
			
			
		$sql .= "IFNULL(
		/*condition*/
	(
											SELECT
				concat(sys_users.strUserName,'(',max(trn_lc_header_approved_by.DATE),')' )
				FROM
				trn_lc_header_approved_by
				Inner Join sys_users ON trn_lc_header_approved_by.USER = sys_users.intUserId
				WHERE
				trn_lc_header_approved_by.LC_SERIAL_NO  = tb1.LC_SERIAL_NO AND
				trn_lc_header_approved_by.LC_SERIAL_YEAR =  tb1.LC_SERIAL_YEAR AND
				trn_lc_header_approved_by.LEVEL =  '$i' AND 
				trn_lc_header_approved_by.STATUS='0' 
			),
			/*end condition*/
			
			/*false part*/
	
			IF(
			/*if condition */
			((SELECT
				menupermision.int".$i."Approval 
				FROM menupermision 
				Inner Join menus ON menupermision.intMenuId = menus.intId
				WHERE
				menus.strCode = '$programCode' AND
				menupermision.intUserId =  '$intUser')=1 AND (tb1.STATUS>1) AND (tb1.STATUS<=tb1.LEVELS) AND ((SELECT
				max(trn_lc_header_approved_by.DATE) 
				FROM
				trn_lc_header_approved_by
				Inner Join sys_users ON trn_lc_header_approved_by.USER = sys_users.intUserId
				WHERE
				trn_lc_header_approved_by.LC_SERIAL_NO = tb1.LC_SERIAL_NO AND
				trn_lc_header_approved_by.LC_SERIAL_YEAR =  tb1.LC_SERIAL_YEAR AND
				trn_lc_header_approved_by.LEVEL =  ($i-1) AND 
				trn_lc_header_approved_by.STATUS='0' )<>'')),
				
				/*end if condition*/
				/*if true part*/
				'Approve',
				/*fase part*/
				 if($i>tb1.LEVELS,'-----',''))
				
				/*end IFNULL false part*/
				) as `".$approval."`, "; 
					
				}
			
			$sql .=" IFNULL((
											SELECT
				concat(sys_users.strUserName,'(',max(trn_lc_header_approved_by.DATE),')' )
				FROM
				trn_lc_header_approved_by
				Inner Join sys_users ON trn_lc_header_approved_by.USER = sys_users.intUserId
				WHERE
				trn_lc_header_approved_by.LC_SERIAL_NO  = tb1.LC_SERIAL_NO AND
				trn_lc_header_approved_by.LC_SERIAL_YEAR =  tb1.LC_SERIAL_YEAR AND
				trn_lc_header_approved_by.LEVEL = '-2' AND 
				trn_lc_header_approved_by.STATUS = '0' 
			),IF(((SELECT
				menupermision.int1Approval 
				FROM menupermision 
				Inner Join menus ON menupermision.intMenuId = menus.intId
				WHERE
				menus.strCode = '$programCode' AND
				menupermision.intUserId =  '$intUser')=1 AND tb1.STATUS=1),'Cancel', '')) as `CANCEL`,  ";
			
				
			$sql .= "'View' as `View`   
			FROM
			trn_lc_header AS tb1 
			INNER JOIN mst_customer ON tb1.CUSTOMER_ID = mst_customer.intId
			INNER JOIN sys_users ON tb1.CREATED_BY = sys_users.intUserId
			INNER JOIN mst_bank ON tb1.BANK_ID = mst_bank.BANK_ID
			INNER JOIN mst_financecurrency ON tb1.CURRENCY = mst_financecurrency.intId 
			where 1=1
				$where_string
			)  as t where 1=1
		";
		return $sql;
	}
	
	private function Load_Report_approval_details_sql($serialYear,$serialNo){
	
		
	   	$sql = "SELECT
 				trn_lc_header_approved_by.DATE AS dtApprovedDate ,
				sys_users.strUserName as UserName,
				trn_lc_header_approved_by.LEVEL as intApproveLevelNo
				FROM
				trn_lc_header_approved_by
				Inner Join sys_users ON trn_lc_header_approved_by.USER = sys_users.intUserId
				WHERE
				trn_lc_header_approved_by.LC_SERIAL_NO =  '$serialNo' AND
				trn_lc_header_approved_by.LC_SERIAL_YEAR =  '$serialYear'      order by DATE asc";
				
 		return $sql;
	}
 	private function getCustomerId_sql($orderYear,$orderNo)
	{
		$sql = "SELECT intCustomer
				FROM trn_orderheader
				WHERE intOrderNo = '$orderNo' AND
				intOrderYear = '$orderYear' ";
		
		$result	= $this->db->RunQuery($sql);
		$row 	= mysqli_fetch_array($result);
		
		return $row['intCustomer'];
	}
	private function getCustomerYear_sql($customer,$companyId)
	{
		$sql = "SELECT DISTINCT MAX(OH.intOrderYear) AS orderYear
				FROM trn_orderheader OH
				INNER JOIN mst_locations LO ON LO.intId = OH.intLocationId
				WHERE OH.intStatus = 1
				AND LC_STATUS = 6
				AND LO.intCompanyId = '$companyId'
				AND OH.intCustomer = '$customer' ";
		
		$result	= $this->db->RunQuery($sql);
		$row 	= mysqli_fetch_array($result);
		
		return $row['orderYear'];
	}
}
?>
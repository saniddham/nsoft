<?php
include_once  ($_SESSION['ROOT_PATH']."class/cls_commonErrorHandeling_get.php");
//ini_set('display_errors', 1); 
 
$obj_commonErr		= new cls_commonErrorHandeling_get($db);

$progrmCode			='P0804';

$session_userId		= $_SESSION["userId"];
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];

class cls_sub_contractor_po_agrement_get
{
	private $db;
 	
	function __construct($db)
	{
		$this->db = $db;
 	}

//BEGIN - PUBLIC FUNCTIONS {	
	public function get_header_array($serialNo,$year,$executeType)
	{
		
		$sql = $this->Load_header_sql($serialNo,$year);
		$result = $this->db->$executeType($sql);
		$row = mysqli_fetch_array($result);
		return $row;
	}
	public function get_rpt_header_array($serialNo,$year,$executeType)
	{
		
		$sql = $this->Load_header_sql($serialNo,$year);
		$result = $this->db->$executeType($sql);
		$row = mysqli_fetch_array($result);
		return $row;
	}
	
	public function get_max_approved_by_status($serialNo,$year,$executeType)
	{
		
		$sql = $this->Load_max_approve_by_sql($serialNo,$year);
		$result = $this->db->$executeType($sql);
		$row = mysqli_fetch_array($result);
		return $row['status'];
	}
	
	public function getOrderDetails($location,$orderYear,$orderNo,$executeType){
		
		$sql 					= $this->Load_order_details_sql($location,$orderYear,$orderNo);
		$result 				= $this->db->$executeType($sql);
		$row					= mysqli_fetch_array($result);
		$data['customer_po']	= $row['CUSTOMER_PO'];
		$data['currency']		= $row['CURRENCY'];
		$data['customer']		= $row['CUSTOMER'];
		$data['pay_term']		= $row['PAY_TERM'];
		$data['cust_location']	= $row['CUSTOMER_LOCATION'];
		$data['marketer']		= $row['MARKETER'];
		$data['contact_person']	= $row['CONTACT_PERSON'];

		$sql 		= $this->Load_sales_orders_sql($orderNo,$orderYear);
		$result 	= $this->db->$executeType($sql);
		$combo 		= "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$combo .="<option value=\"".$row['intSalesOrderId']."\">".$row['strSalesOrderNo']."</option>";	
		}
		$data['sales_orders_combo']	= $combo;
		
		return $data;
	}

	public function get_data_for_sales_order($orderNo,$orderYear,$saleOrder,$executeType){
		
		$sql 				= $this->Load_data_for_sales_order($orderNo,$orderYear,$saleOrder);
		$result 			= $this->db->$executeType($sql);
		$row				= mysqli_fetch_array($result);
		$data['graphic']	= $row['strGraphicNo'];
		$data['sample']		= $row['intSampleYear'].'/'.$row['intSampleNo'];
		$data['style']		= $row['strStyleNo'];
		$data['placement']	= $row['partName'];
		$data['combo']		= $row['strCombo'];
		$data['groundColor']= $row['groundColor'];
		$data['revision']	= $row['intRevisionNo'];
 		
		return $data;
	}



	public function get_details_result($serialNo,$year,$executeType)
	{
		$sql 	= $this->Load_details_sql($serialNo,$year);
		$result = $this->db->$executeType($sql);
		return $result;
	}
	public function get_Report_approval_details_result($serialNo,$year,$executeType){
		$sql = $this->Load_Report_approval_details_sql($serialNo,$year);
		$result = $this->db->$executeType($sql);
		return $result;
	}
	
	public function get_comany_options($company,$executeType){
		$sql = $this->Load_company_sql($company);
		$result = $this->db->$executeType($sql);
		$combo ="<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$combo .="<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
		}
	 return $combo;
	}
	
	public function get_contract_type_options($executeType){
		$sql = $this->Load_contract_type_sql();
		$result = $this->db->$executeType($sql);
		$combo ="<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$combo .="<option value=\"".$row['ID']."\">".$row['NAME']."</option>";	
		}
	 return $combo;
	}
 
	public function get_sales_order_options($orderNo,$orderYear,$executeType){
		$sql = $this->Load_sales_orders_sql($orderNo,$orderYear);
		$result = $this->db->$executeType($sql);
		$combo ="<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$combo .="<option value=\"".$row['intSalesOrderId']."\">".$row['strSalesOrderNo']."</option>";	
		}
	 return $combo;
	}
 
	public function get_sub_contract_price($orderNo,$orderYear,$saleOrder,$subType,$executeType){
		
		$sql 		= $this->Load_sub_contract_price_sql($orderNo,$orderYear,$saleOrder,$subType);
		$result		= $this->db->$executeType($sql);
		$row		= mysqli_fetch_array($result);
		$price		= $row['dblPrice'];
 		
		return $price;
	}
	
	public function get_size_wise_order($orderNo,$year,$salesOrderNo,$executeType){
		
		$sql 		= $this->Load_size_wise_order_sql($orderNo,$orderYear,$saleOrder);
		$result		= $this->db->$executeType($sql);
  		
		return $result;
		
	}
	
	public function get_fabric_stock_bal_Qty($orderNo,$year,$salesOrderNo,$size,$location,$executeType){
	
		$sql 		= $this->Load_fabric_stock_bal_Qty($orderNo,$year,$salesOrderNo,$size,$location);
		$result		= $this->db->$executeType($sql);
		$row		= mysqli_fetch_array($result);
		$bal		= $row['bal'];
 		
		return $bal;
	
	}
	
	public function get_confirmed_sub_contract_qty($subNo,$subYear,$orderNo,$year,$salesOrderNo,$size,$subType,$location,$executeType){
		
		$sql 		= $this->Load_confirmed_sub_contract_sql($subNo,$subYear,$orderNo,$year,$salesOrderNo,$size,$subType,$location);
		$result		= $this->db->$executeType($sql);
		$row		= mysqli_fetch_array($result);
		$qty		= $row['QTY'];
 		
		return $qty;
		
	}
	
	public function get_saved_sub_contract_qty($subNo,$subYear,$orderNo,$year,$salesOrderNo,$size,$subType,$location,$executeType){
		
		$sql 		= $this->load_saved_sub_contract_sql($subNo,$subYear,$orderNo,$year,$salesOrderNo,$size,$subType,$location);
		$result		= $this->db->$executeType($sql);
		$row		= mysqli_fetch_array($result);
		$qty		= $row['QTY'];
 		
		return $qty;
		
	}

	public function get_order_wise_fabric_stock_bal_Qty($orderNo,$year,$salesOrderNo,$location,$executeType){
	
		$sql 		= $this->Load_order_wise_fabric_stock_bal_Qty($orderNo,$year,$salesOrderNo,$location);
		$result		= $this->db->$executeType($sql);
		$row		= mysqli_fetch_array($result);
		$bal		= $row['bal'];
 		
		return $bal;
	
	}
	
	public function get_order_wise_confirmed_sub_contract_qty($subNo,$subYear,$orderNo,$year,$salesOrderNo,$subType,$location,$executeType){
		
		$sql 		= $this->Load_order_wise_confirmed_sub_contract_sql($subNo,$subYear,$orderNo,$year,$salesOrderNo,$subType,$location);
		$result		= $this->db->$executeType($sql);
		$row		= mysqli_fetch_array($result);
		$qty		= $row['QTY'];
 		
		return $qty;
		
	}
	
	public function get_order_wise_saved_sub_contract_qty($subNo,$subYear,$orderNo,$year,$salesOrderNo,$subType,$location,$executeType){
		
		$sql 		= $this->load_order_wise_saved_sub_contract_sql($subNo,$subYear,$orderNo,$year,$salesOrderNo,$subType,$location);
		$result		= $this->db->$executeType($sql);
		$row		= mysqli_fetch_array($result);
		$qty		= $row['QTY'];
 		
		return $qty;
		
	}

	
	public function ValidateBeforeSave($serialNo,$serialYear,$executeType)
	{ 
		global $session_userId;
		global $obj_commonErr;	
		global $progrmCode;
		$this->db->begin();
   		$header_arr			= $this->get_header_array2($serialYear,$serialNo,$executeType);
 		$permision_arr		= $obj_commonErr->get_permision_withApproval_save($header_arr['STATUS'],$header_arr['LEVELS'],$session_userId,$progrmCode,$executeType);
		if($permision_arr['type'] =='fail'){
			$rollBack			=	1;
			$response['msg'] 	=	$permision_arr['msg'];	
			$response['type'] 	=	'fail';
			return $response;
		}
		$this->db->commit();
	}
	public function getDetails($location,$subCatId,$itemId,$executeType){
		
		$subCat  = $_REQUEST['subCat'];
		$itemId  = $_REQUEST['itemId'];
		
		     $sql="SELECT
				round(Sum(ware_stocktransactions_bulk.dblQty),4) AS stockBal,
				mst_item.intMainCategory,
				mst_maincategory.strName as mainCatName,
				mst_item.intSubCategory,
				mst_subcategory.strName as subCatName,
				ware_stocktransactions_bulk.intGRNNo,
				ware_stocktransactions_bulk.intGRNYear,
				ware_stocktransactions_bulk.dtGRNDate,
				ware_stocktransactions_bulk.dblGRNRate, 
				ware_stocktransactions_bulk.intCurrencyId,
				ware_stocktransactions_bulk.intItemId, 
				mst_item.strName as itemName, 
				mst_item.strCode,
				mst_item.intUOM,
				mst_units.strCode as uom , 
				mst_financecurrency.strCode as currency 
				FROM
				ware_stocktransactions_bulk
				Inner Join mst_item ON ware_stocktransactions_bulk.intItemId = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
				Inner Join mst_financecurrency ON ware_stocktransactions_bulk.intCurrencyId = mst_financecurrency.intId
				WHERE
				ware_stocktransactions_bulk.intLocationId =  '$location' ";
			if($itemId!=''){ 
			$sql .="  AND ware_stocktransactions_bulk.intItemId =  '$itemId' "; 
			}
			$sql .="  AND mst_item.intSubCategory =  '$subCatId'  "; 
			$sql .=" GROUP BY
				ware_stocktransactions_bulk.intItemId, 
				ware_stocktransactions_bulk.intGRNNo,
				ware_stocktransactions_bulk.intGRNYear,
				ware_stocktransactions_bulk.dtGRNDate,
				ware_stocktransactions_bulk.dblGRNRate 
				HAVING stockBal > 0";
				//echo $sql;
			$result = $this->db->$executeType($sql);
		while($row=mysqli_fetch_array($result))
		{
			$arrData[] = $row;
		}
		$response['arrData'] 	= $arrData;
		return $arrData;
	}
	
	
	
private function Load_details_sql($serialNo,$year){
	$sql = "SELECT
			ware_general_item_disposal_details.DISPOSE_NO,
			ware_general_item_disposal_details.`YEAR`,
			ware_general_item_disposal_details.GRN_NO,
			ware_general_item_disposal_details.GRN_YEAR,
			ware_general_item_disposal_details.CURRENCY,
			ware_general_item_disposal_details.RATE,
			ware_general_item_disposal_details.GRN_DATE,
			ware_general_item_disposal_details.ITEM,
			ware_general_item_disposal_details.QTY,
			mst_financecurrency.strCode as curr,
			mst_item.strCode,
			mst_item.strName AS itemNm,
			mst_item.intUOM,
			mst_units.strCode AS unit,
			mst_item.intMainCategory,
			mst_item.intSubCategory,
			mst_subcategory.strName as subCat,
			mst_maincategory.strName as mainCat
			FROM
			ware_general_item_disposal_header
			INNER JOIN ware_general_item_disposal_details ON ware_general_item_disposal_header.DISPOSE_NO = ware_general_item_disposal_details.DISPOSE_NO AND ware_general_item_disposal_header.`YEAR` = ware_general_item_disposal_details.`YEAR`
			INNER JOIN mst_financecurrency ON ware_general_item_disposal_details.CURRENCY = mst_financecurrency.intId
			INNER JOIN mst_item ON ware_general_item_disposal_details.ITEM = mst_item.intId
			INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
			INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
			INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
			WHERE
			ware_general_item_disposal_header.DISPOSE_NO = '$serialNo' AND
			ware_general_item_disposal_header.`YEAR` = '$year'
";
	
	return $sql;

}

  //END	- PUBLIC FUNCTIONS }

//BEGIN - PRIVATE FUNCTIONS {	
	private function Load_header_sql($serialNo,$year)
	{
		global $session_companyId;
		  $sql="SELECT
				trn_order_sub_contract_header.SUB_CONTRACT_NO,
				trn_order_sub_contract_header.SUB_CONTRACT_YEAR,
				trn_order_sub_contract_header.ORDER_NO,
				trn_order_sub_contract_header.ORDER_YEAR,
				trn_order_sub_contract_header.COMPANY_TO_ID,
				trn_order_sub_contract_header.LOCATION_TO_ID,
				trn_order_sub_contract_header.REMARKS,
				trn_order_sub_contract_header.`STATUS`,
				trn_order_sub_contract_header.LEVELS,
				trn_order_sub_contract_header.DATE,
				trn_order_sub_contract_header.CREATED_DATE,
				trn_order_sub_contract_header.CREATED_BY,
				trn_order_sub_contract_header.MODIFIED_DATE,
				trn_order_sub_contract_header.MODIFIED_BY,
				trn_order_sub_contract_header.LOCATION_ID,
				mst_companies.strName AS TO_COMPANY,
				mst_locations.strName AS TO_LOCATION,
				sys_users.strUserName AS `USER`
				FROM
				trn_order_sub_contract_header
				INNER JOIN mst_companies ON trn_order_sub_contract_header.COMPANY_TO_ID = mst_companies.intId
				INNER JOIN mst_locations ON trn_order_sub_contract_header.LOCATION_TO_ID = mst_locations.intId
				INNER JOIN sys_users ON trn_order_sub_contract_header.CREATED_BY = sys_users.intUserId
				WHERE
				trn_order_sub_contract_header.SUB_CONTRACT_NO = '$serialNo' AND
				trn_order_sub_contract_header.SUB_CONTRACT_YEAR = '$year'";
 		
  		return $sql;
 	}
	
	
	private function Load_max_approve_by_sql($serialNo,$year){
		$sql = "SELECT
				Max(trn_order_sub_contract_approved_by.`STATUS`) as status 
				FROM `trn_order_sub_contract_approved_by`
				WHERE
				trn_order_sub_contract_approved_by.SUB_CONTRACT_NO = '$serialNo' AND
				trn_order_sub_contract_approved_by.SUB_CONTRACT_YEAR = '$year'";
		
		return $sql;
	
	}
	
	private function Load_Report_approval_details_sql($serialNo,$year){
	  	$sql = "";
 		return $sql;
	}
	
	
	
	private function Load_company_sql($company){
		$sql="SELECT
				mst_companies.intId,
				mst_companies.strName
				FROM `mst_companies`
				WHERE
				mst_companies.intStatus = 1 AND 
				mst_companies.intId <> '$company'
				ORDER BY
				mst_companies.strName ASC";
		return $sql;
		
	}
	
	private function Load_contract_type_sql(){
		
		$sql="SELECT
				mst_sub_contract_job_types.ID,
				mst_sub_contract_job_types.`NAME`
				FROM `mst_sub_contract_job_types`
				WHERE
				mst_sub_contract_job_types.`STATUS` = 1
				ORDER BY
				mst_sub_contract_job_types.`NAME` ASC
				";
		return $sql;
		
	}
	
	private function Load_order_details_sql($location,$orderYear,$orderNo){
		
		$sql	= "SELECT
					mst_customer_locations_header.strName AS CUSTOMER_LOCATION,
					trn_orderheader.strCustomerPoNo AS CUSTOMER_PO,
					trn_orderheader.strContactPerson  AS CONTACT_PERSON,
					mst_customer.strName AS CUSTOMER,
					mst_financepaymentsterms.strName AS PAY_TERM,
					sys_users.strUserName AS MARKETER, 
					mst_financecurrency.strCode AS CURRENCY
					FROM
					trn_orderheader
					INNER JOIN mst_customer ON trn_orderheader.intCustomer = mst_customer.intId
					INNER JOIN mst_customer_locations_header ON trn_orderheader.intCustomerLocation = mst_customer_locations_header.intId
					INNER JOIN mst_financepaymentsterms ON trn_orderheader.intPaymentTerm = mst_financepaymentsterms.intId
					INNER JOIN sys_users ON trn_orderheader.intMarketer = sys_users.intUserId
					INNER JOIN mst_financecurrency ON trn_orderheader.intCurrency = mst_financecurrency.intId
					WHERE
					trn_orderheader.intOrderNo = '$orderNo' AND
					trn_orderheader.intOrderYear = '$orderYear' AND
					trn_orderheader.intLocationId = '$location' AND 
					trn_orderheader.intStatus = '1' 
					";
		
		return $sql;
		
	}
	
	private function Load_sales_orders_sql($orderNo,$orderYear){
		
		$sql	= "SELECT
				trn_orderdetails.intSalesOrderId,
				trn_orderdetails.strSalesOrderNo
				FROM `trn_orderdetails`
				WHERE
				trn_orderdetails.intOrderNo = '$orderNo' AND
				trn_orderdetails.intOrderYear = '$orderYear'";
		
		return $sql;
		
	}
	
	private function Load_data_for_sales_order($orderNo,$orderYear,$saleOrder){
		
	  	 $sql = "	SELECT
						trn_orderdetails.strSalesOrderNo,
						trn_orderdetails.intSalesOrderId, 
						trn_orderdetails.strGraphicNo,
						trn_orderdetails.strStyleNo,
						trn_orderdetails.intSampleNo,
						trn_orderdetails.intSampleYear,
						trn_orderdetails.strCombo,
						trn_orderdetails.strPrintName,
						trn_orderdetails.intQty,
						trn_orderdetails.dblPrice,
						trn_orderdetails.dblOverCutPercentage,
						trn_orderdetails.dblDamagePercentage,
						trn_orderdetails.intRevisionNo,
						trn_orderdetails.dtPSD,
						trn_orderdetails.dtDeliveryDate, 
						mst_colors_ground.strName as groundColor, 
						mst_part.strName as partName
						FROM
						trn_orderdetails 
						Inner Join trn_sampleinfomations_details ON trn_orderdetails.intSampleNo = trn_sampleinfomations_details.intSampleNo AND trn_orderdetails.intSampleYear = trn_sampleinfomations_details.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations_details.intRevNo AND trn_orderdetails.strPrintName = trn_sampleinfomations_details.strPrintName AND trn_orderdetails.strCombo = trn_sampleinfomations_details.strComboName
						Inner Join mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
						left Join mst_part on mst_part.intId = trn_orderdetails.intPart 
						WHERE
						trn_orderdetails.intOrderNo 		=  '$orderNo' AND
						trn_orderdetails.intOrderYear 		=  '$orderYear' AND 
						trn_orderdetails.intSalesOrderId	=  '$saleOrder' 
					";
					
					return $sql;
		
	}
	
	private function Load_sub_contract_price_sql($orderNo,$orderYear,$saleOrder,$subType){
		$sql	 ="SELECT
					trn_sampleinfomations_prices_sub_contract_job.dblPrice
					FROM
					trn_orderdetails
					INNER JOIN trn_sampleinfomations_prices_sub_contract_job ON trn_orderdetails.intSampleNo = trn_sampleinfomations_prices_sub_contract_job.intSampleNo AND trn_orderdetails.intSampleYear = trn_sampleinfomations_prices_sub_contract_job.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations_prices_sub_contract_job.intRevisionNo AND trn_orderdetails.strCombo = trn_sampleinfomations_prices_sub_contract_job.strCombo AND trn_orderdetails.strPrintName = trn_sampleinfomations_prices_sub_contract_job.strPrintName
					WHERE
					trn_orderdetails.intOrderNo = '$orderNo' AND
					trn_orderdetails.intOrderYear = '$orderYear' AND
					trn_orderdetails.intSalesOrderId = '$saleOrder' AND
					trn_sampleinfomations_prices_sub_contract_job.intSubContractJobID = '$subType'
					";
	
		return $sql;
	}
	
	private function Load_size_wise_order_sql($orderNo,$orderYear,$saleOrder){
		
		$sql = "SELECT
				trn_ordersizeqty.strSize,
				trn_ordersizeqty.dblQty
				FROM trn_orderdetails 
				Inner Join trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear AND trn_orderdetails.intSalesOrderId = trn_ordersizeqty.intSalesOrderId
				WHERE
				trn_ordersizeqty.intOrderNo =  '$orderNo' AND
				trn_ordersizeqty.intOrderYear =  '$orderYear' AND
				trn_ordersizeqty.intSalesOrderId =  '$saleOrder'";
		return $sql;
			
	}
	
	private function Load_fabric_stock_bal_Qty($orderNo,$year,$salesOrderNo,$size,$location){
		
		$sql	=	"SELECT
					Sum(ware_stocktransactions_fabric.dblQty) as bal
					FROM `ware_stocktransactions_fabric`
					WHERE
					ware_stocktransactions_fabric.intOrderNo = '$orderNo' AND
					ware_stocktransactions_fabric.intOrderYear = '$year' AND
					ware_stocktransactions_fabric.intSalesOrderId = '$salesOrderNo' AND
					ware_stocktransactions_fabric.strSize = '$size' AND
					ware_stocktransactions_fabric.intLocationId = '$location'
					";
		return $sql;
 		
	}
	
	private function Load_confirmed_sub_contract_sql($subNo,$subYear,$orderNo,$year,$salesOrderNo,$size,$subType,$location)	{
		
		$sql	=	"SELECT
					trn_order_sub_contract_size_wise_qty.QTY 
					FROM
					trn_order_sub_contract_size_wise_qty
					INNER JOIN trn_order_sub_contract_header ON trn_order_sub_contract_size_wise_qty.SUB_CONTRACT_NO = trn_order_sub_contract_header.SUB_CONTRACT_NO AND trn_order_sub_contract_size_wise_qty.SUB_CONTRACT_YEAR = trn_order_sub_contract_header.SUB_CONTRACT_YEAR AND trn_order_sub_contract_size_wise_qty.ORDER_NO = trn_order_sub_contract_header.ORDER_NO AND trn_order_sub_contract_size_wise_qty.ORDER_YEAR = trn_order_sub_contract_header.ORDER_YEAR
					WHERE 
					trn_order_sub_contract_size_wise_qty.ORDER_NO = '$orderNo' AND
					trn_order_sub_contract_size_wise_qty.ORDER_YEAR = '$year' AND
					trn_order_sub_contract_size_wise_qty.SALES_ORDER_ID = '$salesOrderNo' AND
					trn_order_sub_contract_size_wise_qty.JOB_TYPE = '$subType' AND
					trn_order_sub_contract_size_wise_qty.SIZE = '$size' AND 
					
					trn_order_sub_contract_header.`STATUS` > 0 AND 
					trn_order_sub_contract_header.`STATUS` <=trn_order_sub_contract_header.LEVELS";
		
		return $sql;
		
	}

	private function load_saved_sub_contract_sql($subNo,$subYear,$orderNo,$year,$salesOrderNo,$size,$subType,$location)	{
		
		$sql	=	"SELECT
					trn_order_sub_contract_size_wise_qty.QTY 
					FROM
					trn_order_sub_contract_size_wise_qty
					INNER JOIN trn_order_sub_contract_header ON trn_order_sub_contract_size_wise_qty.SUB_CONTRACT_NO = trn_order_sub_contract_header.SUB_CONTRACT_NO AND trn_order_sub_contract_size_wise_qty.SUB_CONTRACT_YEAR = trn_order_sub_contract_header.SUB_CONTRACT_YEAR AND trn_order_sub_contract_size_wise_qty.ORDER_NO = trn_order_sub_contract_header.ORDER_NO AND trn_order_sub_contract_size_wise_qty.ORDER_YEAR = trn_order_sub_contract_header.ORDER_YEAR
					WHERE  
					trn_order_sub_contract_size_wise_qty.SUB_CONTRACT_NO = '$subNo' AND
					trn_order_sub_contract_size_wise_qty.SUB_CONTRACT_YEAR = '$subYear' AND
					trn_order_sub_contract_size_wise_qty.ORDER_NO = '$orderNo' AND
					trn_order_sub_contract_size_wise_qty.ORDER_YEAR = '$year' AND
					trn_order_sub_contract_size_wise_qty.SALES_ORDER_ID = '$salesOrderNo' AND
					trn_order_sub_contract_size_wise_qty.JOB_TYPE = '$subType' AND
					trn_order_sub_contract_size_wise_qty.SIZE = '$size' ";
		
		return $sql;
		
	}

	private function Load_order_wise_fabric_stock_bal_Qty($orderNo,$year,$salesOrderNo,$location){
		
		$sql	=	"SELECT
					Sum(ware_stocktransactions_fabric.dblQty) as bal
					FROM `ware_stocktransactions_fabric`
					WHERE
					ware_stocktransactions_fabric.intOrderNo = '$orderNo' AND
					ware_stocktransactions_fabric.intOrderYear = '$year' AND
					ware_stocktransactions_fabric.intSalesOrderId = '$salesOrderNo' AND
					ware_stocktransactions_fabric.intLocationId = '$location'
					";
		return $sql;
 		
	}
	
	private function Load_order_wise_confirmed_sub_contract_sql($subNo,$subYear,$orderNo,$year,$salesOrderNo,$subType,$location)	{
		
		$sql	=	"SELECT
					trn_order_sub_contract_size_wise_qty.QTY 
					FROM
					trn_order_sub_contract_size_wise_qty
					INNER JOIN trn_order_sub_contract_header ON trn_order_sub_contract_size_wise_qty.SUB_CONTRACT_NO = trn_order_sub_contract_header.SUB_CONTRACT_NO AND trn_order_sub_contract_size_wise_qty.SUB_CONTRACT_YEAR = trn_order_sub_contract_header.SUB_CONTRACT_YEAR AND trn_order_sub_contract_size_wise_qty.ORDER_NO = trn_order_sub_contract_header.ORDER_NO AND trn_order_sub_contract_size_wise_qty.ORDER_YEAR = trn_order_sub_contract_header.ORDER_YEAR
					WHERE 
					trn_order_sub_contract_size_wise_qty.ORDER_NO = '$orderNo' AND
					trn_order_sub_contract_size_wise_qty.ORDER_YEAR = '$year' AND
					trn_order_sub_contract_size_wise_qty.SALES_ORDER_ID = '$salesOrderNo' AND
					trn_order_sub_contract_size_wise_qty.JOB_TYPE = '$subType' AND 
					
					trn_order_sub_contract_header.`STATUS` > 0 AND 
					trn_order_sub_contract_header.`STATUS` <=trn_order_sub_contract_header.LEVELS";
		
		return $sql;
		
	}

	private function load_order_wise_saved_sub_contract_sql($subNo,$subYear,$orderNo,$year,$salesOrderNo,$subType,$location)	{
		
		$sql	=	"SELECT
					trn_order_sub_contract_size_wise_qty.QTY 
					FROM
					trn_order_sub_contract_size_wise_qty
					INNER JOIN trn_order_sub_contract_header ON trn_order_sub_contract_size_wise_qty.SUB_CONTRACT_NO = trn_order_sub_contract_header.SUB_CONTRACT_NO AND trn_order_sub_contract_size_wise_qty.SUB_CONTRACT_YEAR = trn_order_sub_contract_header.SUB_CONTRACT_YEAR AND trn_order_sub_contract_size_wise_qty.ORDER_NO = trn_order_sub_contract_header.ORDER_NO AND trn_order_sub_contract_size_wise_qty.ORDER_YEAR = trn_order_sub_contract_header.ORDER_YEAR
					WHERE  
					trn_order_sub_contract_size_wise_qty.SUB_CONTRACT_NO = '$subNo' AND
					trn_order_sub_contract_size_wise_qty.SUB_CONTRACT_YEAR = '$subYear' AND
					trn_order_sub_contract_size_wise_qty.ORDER_NO = '$orderNo' AND
					trn_order_sub_contract_size_wise_qty.ORDER_YEAR = '$year' AND
					trn_order_sub_contract_size_wise_qty.SALES_ORDER_ID = '$salesOrderNo' AND
					trn_order_sub_contract_size_wise_qty.JOB_TYPE = '$subType' ";
		
		return $sql;
		
	}



}

?>
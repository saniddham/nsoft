<?php
include_once  ("../../../../../class/cls_commonFunctions_get.php");
include_once  ("../../../../../class/cls_commonErrorHandeling_get.php");
//ini_set('display_errors', 1); 

$obj_commonErr		= new cls_commonErrorHandeling_get($db);
$obj_common			= new cls_commonFunctions_get($db);

$session_userId		= $_SESSION["userId"];
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];
$programCode		='P0790';

class cls_sub_contractor_po_agrement_set
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	
	public function save_header($serialYear,$serialNo,$orderNo,$orderYear,$to_company,$to_location,$date,$deliveryDate,$remarks,$approveLevels,$status,$location,$userId,$executeType)
	{ 

   		$sql	= $this->save_header_sql($serialYear,$serialNo,$orderNo,$orderYear,$to_company,$to_location,$date,$deliveryDate,$remarks,$approveLevels,$status,$location,$userId);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		}
			return $response;
 	}
	
	public function update_header($serialYear,$serialNo,$orderNo,$orderYear,$to_company,$to_location,$date,$deliveryDate,$remarks,$approveLevels,$status,$location,$userId,$executeType)
	{ 

   		$sql	= $this->update_header_sql($serialYear,$serialNo,$orderNo,$orderYear,$to_company,$to_location,$date,$deliveryDate,$remarks,$approveLevels,$status,$location,$userId);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		}
			return $response;
 	}
	
	public function updateHeaderStatus($serialYear,$serialNo,$status,$executeType){
   		$sql	= $this->update_header_status_sql($serialYear,$serialNo,$status);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		}
			return $response;
		
	}
	
	
	public function approved_by_insert($serialYear,$serialNo,$user,$approval,$executeType){
   		$sql	= $this->save_approved_by_header_sql($serialYear,$serialNo,$user,$approval);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		}
			return $response;
	}
	
	public function approved_by_update($serialYear,$serialNo,$maxAppByStatus,$executeType)
	{ 

   		$sql	= $this->load_update_approved_by_sql($serialYear,$serialNo,$maxAppByStatus);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		}
			return $response;
 	}

	public function delete_details($serialYear,$serialNo,$executeType)
	{ 

   		$sql	= $this->delete_details_sql($serialYear,$serialNo);
		$result = $this->db->$executeType($sql);
		/*if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{*/
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		/*}*/
			return $response;
 	}
	
	public function save_details($serialNo,$serialYear,$company,$location,$orderNo,$year,$salesOrderNo,$subType,$qty,$price,$psDate,$delDate,$userId,$executeType)
	{ 

   		$sql	= $this->save_details_sql($serialNo,$serialYear,$company,$location,$orderNo,$year,$salesOrderNo,$subType,$qty,$price,$psDate,$delDate,$userId);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		}
			return $response;
 	}
	
public function sendFinlConfirmedEmailToUser($serialNo,$year,$objMail,$mainPat,$root_path,$executeType){
			global $db;

			$sql = $this->Load_mail_user_details_sql($serialNo,$year);
			$result = $db->$executeType($sql);
			$row=mysqli_fetch_array($result);
			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="CONFIRMED DISPOSAL ('$serialNo'/'$year')"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST = NULL;
			
			$_REQUEST['program']='GENERAL ITEMDISPOSAL';
			$_REQUEST['field1']='Dispose No';
			$_REQUEST['field2']='Dispose Year';
			$_REQUEST['field3']='';
			$_REQUEST['field4']='';
			$_REQUEST['field5']='';
			$_REQUEST['value1']=$serialNo;
			$_REQUEST['value2']=$year;
			$_REQUEST['value3']='';
			$_REQUEST['value4']='';
			$_REQUEST['value5']='';
			
			$_REQUEST['subject']="CONFIRMED DISPOSAL ('$serialNo'/'$year')";
			
			$_REQUEST['statement1']="Approved this";
			$_REQUEST['statement2']="to view this";
			$_REQUEST['link']=base64_encode($mainPath."presentation/warehouse/general_stock_dispose/listing/rpt_general_stock_dispose.php?serialNo=$serialNo&year=$year&mode=0"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}
 
public function sendRejecedEmailToUser($serialNo,$year,$objMail,$mainPat,$root_path,$executeType){
			global $db;

			$sql = $this->Load_mail_user_details_sql($serialNo,$year);
			$result = $db->$executeType($sql);
			$row=mysqli_fetch_array($result);
			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="REJECTED DISPOSAL ('$serialNo'/'$year')"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST = NULL;
			
			$_REQUEST['program']='GENERAL ITEMDISPOSAL';
			$_REQUEST['field1']='Dispose No';
			$_REQUEST['field2']='Dispose Year';
			$_REQUEST['field3']='';
			$_REQUEST['field4']='';
			$_REQUEST['field5']='';
			$_REQUEST['value1']=$serialNo;
			$_REQUEST['value2']=$year;
			$_REQUEST['value3']='';
			$_REQUEST['value4']='';
			$_REQUEST['value5']='';
			
			$_REQUEST['subject']="REJECTED DISPOSAL ('$serialNo'/'$year')";
			
			$_REQUEST['statement1']="Rejected this";
			$_REQUEST['statement2']="to view this";
			$_REQUEST['link']=base64_encode($mainPath."presentation/warehouse/general_stock_dispose/listing/rpt_general_stock_dispose.php?serialNo=$serialNo&year=$year&mode=0"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}
 	
	
	private function delete_details_sql($serialYear,$serialNo)
	{
 		
		$sql	= "DELETE 
					FROM trn_order_sub_contract_details 
					WHERE
					trn_order_sub_contract_details.SUB_CONTRACT_NO = '$serialNo' AND
					trn_order_sub_contract_details.SUB_CONTRACT_YEAR = '$serialYear'";
 		return $sql;
	}
	
	private function save_details_sql($serialNo,$serialYear,$company,$location,$orderNo,$year,$salesOrderNo,$subType,$qty,$price,$psDate,$delDate,$userId)
	{
		
	 	$sql	= "INSERT INTO trn_order_sub_contract_details 
					(SUB_CONTRACT_NO, 
					SUB_CONTRACT_YEAR, 
					SALES_ORDER_ID,
					SUB_CONTR_JOB_ID,
					QTY,
					PRICE,
					PDS_DATE,
					DELIVERY_DATE)
					VALUES
					('$serialNo', 
					'$serialYear', 
					'$salesOrderNo',
					'$subType',
					'$qty',
					'$price',
					'$psDate',
					'$delDate');";
		return $sql;
	}
	
	private function save_header_sql($serialYear,$serialNo,$orderNo,$orderYear,$to_company,$to_location,$date,$deliveryDate,$remarks,$approveLevels,$status,$location,$userId){
	 
	  	$sql	= "INSERT INTO trn_order_sub_contract_header 
					(SUB_CONTRACT_NO, 
					SUB_CONTRACT_YEAR, 
					ORDER_NO,
					ORDER_YEAR,
					COMPANY_TO_ID,
					LOCATION_TO_ID,
					REMARKS,
					STATUS,
					LEVELS,
					DELIVERY_DATE,
					DATE,
					CREATED_DATE,
					CREATED_BY,
					LOCATION_ID 
					)
					VALUES
					('$serialNo', 
					'$serialYear', 
					'$orderNo',
					'$orderYear',
					'$to_company',
					'$to_location',
					'$remarks',
					'$status',
					'$approveLevels',
					'$deliveryDate',
					'$date',
					 now(),
					'$userId',
					'$location');";
		return $sql;
		
	}
	private function update_header_sql($serialYear,$serialNo,$orderNo,$orderYear,$to_company,$to_location,$date,$deliveryDate,$remarks,$approveLevels,$status,$location,$userId){
			$sql = "UPDATE `trn_order_sub_contract_header` 
			SET  
			ORDER_NO 			='$orderNo',
			ORDER_YEAR 			='$orderYear',
			COMPANY_TO_ID 		='$to_company',
			LOCATION_TO_ID 		='$to_location',
			REMARKS 			='$remarks',
			DELIVERY_DATE 		='$deliveryDate',
			DATE 				='$date',
			STATUS 				='$status',
			LEVELS 				='$approveLevels',
			MODIFIED_BY 		='$userId',
			MODIFIED_DATE 		=now()
 			WHERE (`SUB_CONTRACT_NO`='$serialNo') AND (`SUB_CONTRACT_YEAR`='$serialYear') ";
		return $sql;
	}
	
	private function update_header_status_sql($serialYear,$serialNo,$status){
		if($status !=''){
			$sql= "UPDATE `trn_order_sub_contract_header` 
			SET  
			STATUS=$status  
  					WHERE (`SUB_CONTRACT_NO`='$serialNo') AND (`SUB_CONTRACT_YEAR`='$serialYear')";
		}
		else{
			$sql= "UPDATE `trn_order_sub_contract_header` 
			SET  
			STATUS=STATUS-1 
  					WHERE (`SUB_CONTRACT_NO`='$serialNo') AND (`SUB_CONTRACT_YEAR`='$serialYear')";
		}
		return $sql;
	}
	
 	private function load_update_approved_by_sql($serialYear,$serialNo,$maxAppByStatus){
		 	$sql = "UPDATE `trn_order_sub_contract_approved_by` 
			SET  
			STATUS ='$maxAppByStatus' 
 					WHERE (`SUB_CONTRACT_NO`='$serialNo') AND (`SUB_CONTRACT_YEAR`='$serialYear')";
		return $sql;
		
	}
	
	private function save_approved_by_header_sql($serialYear,$serialNo,$userId,$approval){
		
		$sql = "INSERT INTO `trn_order_sub_contract_approved_by` (`SUB_CONTRACT_NO`,`SUB_CONTRACT_YEAR`,`LEVELS`,USER,DATE,STATUS) 
			VALUES ('$serialNo','$serialYear','$approval','$userId',now(),0)";
		return $sql;
		
	}
	
	private function Load_mail_user_details_sql($serialNo,$year){
	
		    $sql = "SELECT
			sys_users.strFullName,
			sys_users.strEmail 
			FROM 
			trn_order_sub_contract_header
			Inner Join sys_users ON trn_order_sub_contract_header.CREATED_BY = sys_users.intUserId
			WHERE
			trn_order_sub_contract_header.SUB_CONTRACT_NO =  '$serialNo' AND
			trn_order_sub_contract_header.SUB_CONTRACT_YEAR =  '$year'";
			
			return $sql;
		
	}
 
 }
?>
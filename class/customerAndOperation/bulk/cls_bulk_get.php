<?php
class Cls_bulk_Get
{
	private $db;
	
	function __construct($db)
	{
		$this->db 	= $db;
	}
	
	public function get_order_lc_flag($orderNo,$orderYear,$execution)
	{		
 		$sql	= $this->get_order_lc_sql($orderNo,$orderYear);
		$row 	= $this->get_order_lc_row($sql,$execution);
		$flag 	= $row['LC_STATUS'];
 		return $flag;
	}
	
	
	public function get_lc_status($orderNo,$orderYear,$execution)
	{		
		$sql	= $this->get_lc_sql($orderNo,$orderYear);
		$row	= $this->get_lc_row($sql,$execution);
 		return $row['STATUS'];
	}
	
  	
	private function get_order_lc_sql($orderNo,$orderYear)
	{
		$sql	="SELECT
					trn_orderheader.LC_STATUS
					FROM `trn_orderheader`
					WHERE
					trn_orderheader.intOrderNo = '$orderNo' AND
					trn_orderheader.intOrderYear = '$orderYear'";
		return $sql;
	}
	
 	private function get_order_lc_row($sql,$execution)
	{
		$result = $this->db->$execution($sql);
 		$row = mysqli_fetch_array($result);
 		
		return $row;
	}
	
	private function get_lc_sql($orderNo,$orderYear){
		$sql	="SELECT 
					trn_lc_header.LC_SERIAL_NO,
					trn_lc_header.LC_SERIAL_YEAR,
					trn_lc_header.LC_NO,
					trn_lc_header.BANK_REF_NO,
					trn_lc_header.BANK_ID,
					trn_lc_header.CUSTOMER_ID,
					trn_lc_header.ORDER_NO,
					trn_lc_header.ORDER_YEAR,
					trn_lc_header.LC_DATE,
					trn_lc_header.LC_AMOUNT,
					trn_lc_header.REMARKS,
					trn_lc_header.`STATUS`,
					trn_lc_header.LEVELS,
					trn_lc_header.CREATED_BY,
					trn_lc_header.CREATED_DATE,
					trn_lc_header.MODIFIED_BY,
					trn_lc_header.MODIFIED_DATE
					FROM `trn_lc_header`
					WHERE
					trn_lc_header.ORDER_NO 		= '$orderNo' AND
					trn_lc_header.ORDER_YEAR 	= '$orderYear' AND 
					trn_lc_header.`STATUS` 		= 1 
					";
					
					return $sql;
 	}
	
	
 	private function get_lc_row($sql,$execution)
	{
		$result = $this->db->$execution($sql);
 		$row = mysqli_fetch_array($result);
 		
		return $row;
	}
 	
}
?>
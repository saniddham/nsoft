<?php
 
  
class cls_delivery_report_get
{
	private $db;
 	
	function __construct($db)
	{
		$this->db 	= $db;
 	}

 	public function get_customers_delivery_rpt_result($location,$dateFrom,$dateTo)
	{
 		$sql		= $this->load_customers_delivery_rpt_sql($location,$dateFrom,$dateTo);
 		$result		= $this->db->RunQuery($sql);
 		return $result;
	}
  
  
//BEGIN - PRIVATE FUNCTIONS {	
  
	private function load_customers_delivery_rpt_sql($location,$dateFrom,$dateTo)
	{

               $stk_press_pro = 14;
	           $sql = "
							select 
							TB2.customer,
							TB2.marketer,
							TB2.style,
							TB2.graphic,
							TB2.color,
							TB2.part,
							TB2.brand,
							TB2.technique,
							TB2.intOrderNo,
							TB2.intOrderYear,
							TB2.customerPO,
							TB2.intSalesOrderId, 
							TB2.strSalesOrderNo,
							sum(TB2.totOrderQty) as totOrderQty ,
							sum(TB2.totInQty) as totInQty ,
							sum(TB2.todayDispQty) as todayDispQty ,
							sum(TB2.totDispQty) as totDispQty ,  
							sum(TB2.todayPG) AS todayPG,
							sum(TB2.todayPD) as todayPD ,   
							(sum(TB2.todayPD)/sum(TB2.todayPG)*100) as todayPD_prec,
							
							sum(TB2.totDispQty_PG) as totPG,
							sum(TB2.totDispQty_PD) as totPD, 
							(sum(TB2.totDispQty_PD)/sum(TB2.totDispQty_PG)*100) as totPD_prec,
							TB2.totalGoodAmount as totalGoodAmount,
							TB2.sticker_price as sticker_price,
							TB2.dbl_price as dbl_price,
							TB2.SO_TYPE as SO_TYPE
							FROM (
							
							select 
							mst_customer.strName as customer,
							sys_users.strFullName AS marketer,
							OD.strStyleNo as style,
							OD.strGraphicNo as graphic,
							mst_colors_ground.strName as color,
							mst_part.strName as part,
							trn_orderheader.intOrderNo,
							trn_orderheader.intOrderYear,
							OD.intSalesOrderId, 
							OD.strSalesOrderNo,
							trn_orderheader.strCustomerPoNo as customerPO,
							TB.todayDispQty,
							TB.todayPD,
							TB.todayPG,
							TECH.TECHNIQUE_GROUP_NAME AS technique,
							(SELECT Sum(trn_orderdetails.intQty) FROM trn_orderheader 
							INNER JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo 
							AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear 
							WHERE trn_orderdetails.intOrderNo = TB.intOrderNo 
							AND trn_orderdetails.intOrderYear = TB.intOrderYear 
							AND trn_orderdetails.intSalesOrderId = TB.intSalesOrderId) as totOrderQty  ,
							
							(
							(IFNULL((SELECT Sum(ware_fabricreceiveddetails.dblQty) 
							FROM ware_fabricreceivedheader 
							INNER JOIN ware_fabricreceiveddetails 
							ON ware_fabricreceivedheader.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo 
							AND ware_fabricreceivedheader.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear 
							WHERE ware_fabricreceivedheader.intStatus = 1 
							AND ware_fabricreceivedheader.intCompanyId = '$location' 
							AND ware_fabricreceivedheader.intOrderNo =TB.intOrderNo 
							AND ware_fabricreceivedheader.intOrderYear = TB.intOrderYear 
							AND ware_fabricreceiveddetails.intSalesOrderId = TB.intSalesOrderId),0))
							-
							(IFNULL((SELECT 
							Sum(ware_fabricgatepassdetails.dblQty) 
							FROM
							ware_fabricgatepassheader
							INNER JOIN ware_fabricgatepassdetails ON ware_fabricgatepassheader.intFabricGatePassNo = ware_fabricgatepassdetails.intFabricGatePassNo 
							AND ware_fabricgatepassheader.intFabricGatePassYear = ware_fabricgatepassdetails.intFabricGatePassYear
							WHERE 
							ware_fabricgatepassheader.intStatus = 1 AND
							ware_fabricgatepassheader.intCompanyId = '$location'  AND
							ware_fabricgatepassheader.intOrderNo = TB.intOrderNo AND
							ware_fabricgatepassheader.intOrderYear = TB.intOrderYear AND
							ware_fabricgatepassdetails.intSalesOrderId = TB.intSalesOrderId
							),0)) 
							 +
							(IFNULL((SELECT 
							Sum(ware_fabricgptransferindetails.dblQty) 
							FROM
							ware_fabricgptransferinheader
							INNER JOIN ware_fabricgptransferindetails ON ware_fabricgptransferinheader.intFabricGPTransfInNo = ware_fabricgptransferindetails.intFabricGPTransfInNo 
							AND ware_fabricgptransferinheader.intFabricGPTransfInYear = ware_fabricgptransferindetails.intFabricGPTransfInYear
							WHERE 
							ware_fabricgptransferinheader.intStatus = 1 AND
							ware_fabricgptransferinheader.intCompanyId = '$location'  AND
							ware_fabricgptransferinheader.intOrderNo =TB.intOrderNo AND
							ware_fabricgptransferinheader.intOrderYear = TB.intOrderYear AND
							ware_fabricgptransferindetails.intSalesOrderId =TB.intSalesOrderId
							),0))
							) as totInQty  ,
							(SELECT
							Sum(FDD.dblGoodQty+FDD.dblSampleQty+FDD.dblEmbroideryQty+FDD.dblPDammageQty+FDD.dblFdammageQty+FDD.dblCutRetQty)
							FROM
							ware_fabricdispatchheader FDH
							INNER JOIN ware_fabricdispatchdetails FDD 
							ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo 
							AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
							inner join ware_fabricdispatchheader_approvedby on ware_fabricdispatchheader_approvedby.intBulkDispatchNo=FDH.intBulkDispatchNo 
							and ware_fabricdispatchheader_approvedby.intYear=FDH.intBulkDispatchNoYear 
							and ware_fabricdispatchheader_approvedby.intApproveLevelNo= FDH.intApproveLevels 
							AND ware_fabricdispatchheader_approvedby.intStatus =0  AND
							ware_fabricdispatchheader_approvedby.dtApprovedDate <= '$dateTo'
							WHERE 
							FDH.intStatus = 1 AND
							FDH.intCompanyId = '$location'  
							AND 
							FDH.intOrderNo = TB.intOrderNo AND
							FDH.intOrderYear = TB.intOrderYear AND
							FDD.intSalesOrderId = TB.intSalesOrderId) as totDispQty , 
						
							(SELECT
							Sum(FDD.dblGoodQty)
							FROM
							ware_fabricdispatchheader FDH
							INNER JOIN ware_fabricdispatchdetails FDD 
							ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo 
							AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
							inner join ware_fabricdispatchheader_approvedby on ware_fabricdispatchheader_approvedby.intBulkDispatchNo=FDH.intBulkDispatchNo 
							and ware_fabricdispatchheader_approvedby.intYear=FDH.intBulkDispatchNoYear 
							and ware_fabricdispatchheader_approvedby.intApproveLevelNo= FDH.intApproveLevels 
							AND ware_fabricdispatchheader_approvedby.intStatus =0  AND
							ware_fabricdispatchheader_approvedby.dtApprovedDate <= '$dateTo'
							WHERE 
							FDH.intStatus = 1 AND
							FDH.intCompanyId = '$location'  
							AND 
							FDH.intOrderNo = TB.intOrderNo AND
							FDH.intOrderYear = TB.intOrderYear AND
							FDD.intSalesOrderId = TB.intSalesOrderId) as totDispQty_PG ,  
							todayPG*OD.dblPrice AS totalGoodAmount,
							CSR.COST AS sticker_price,
							OD.dblPrice AS dbl_price,
							OD.SO_TYPE AS SO_TYPE,
							(SELECT
							Sum(FDD.dblPDammageQty)
							FROM
							ware_fabricdispatchheader FDH
							INNER JOIN ware_fabricdispatchdetails FDD 
							ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo 
							AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
							inner join ware_fabricdispatchheader_approvedby on ware_fabricdispatchheader_approvedby.intBulkDispatchNo=FDH.intBulkDispatchNo 
							and ware_fabricdispatchheader_approvedby.intYear=FDH.intBulkDispatchNoYear 
							and ware_fabricdispatchheader_approvedby.intApproveLevelNo= FDH.intApproveLevels 
							AND ware_fabricdispatchheader_approvedby.intStatus =0  AND
							ware_fabricdispatchheader_approvedby.dtApprovedDate <= '$dateTo'
							WHERE 
							FDH.intStatus = 1 AND
							FDH.intCompanyId = '$location'  
							AND 
							FDH.intOrderNo = TB.intOrderNo AND
							FDH.intOrderYear = TB.intOrderYear AND
							FDD.intSalesOrderId = TB.intSalesOrderId) as totDispQty_PD,  
							mst_brand.strName AS brand
							FROM(
							SELECT 
							ware_stocktransactions_fabric.intOrderNo,
							ware_stocktransactions_fabric.intOrderYear,
							ware_stocktransactions_fabric.intSalesOrderId, 
							Sum(ware_stocktransactions_fabric.dblQty*-1) as todayDispQty,
							Sum(if(ware_stocktransactions_fabric.strType='Dispatched_P',ware_stocktransactions_fabric.dblQty*-1,0)) as todayPD,
							Sum(if(ware_stocktransactions_fabric.strType='Dispatched_G',ware_stocktransactions_fabric.dblQty*-1,0)) as todayPG 
							FROM ware_stocktransactions_fabric  
							WHERE 
							ware_stocktransactions_fabric.strType IN ('Dispatched_G','Dispatched_S','Dispatched_E','Dispatched_P','Dispatched_F','Dispatched_CUT_RET')
							AND ware_stocktransactions_fabric.dtDate >= '$dateFrom' 
							AND ware_stocktransactions_fabric.dtDate <= '$dateTo' 
							AND ware_stocktransactions_fabric.intLocationId = '$location' 
							GROUP BY 
							ware_stocktransactions_fabric.intOrderNo,
							ware_stocktransactions_fabric.intOrderYear,
							ware_stocktransactions_fabric.intSalesOrderId
							)as TB 
							
							INNER JOIN trn_orderdetails as OD 
							ON  TB.intOrderNo = OD.intOrderNo 
							AND TB.intOrderYear = OD.intOrderYear  
							AND TB.intSalesOrderId = OD.intSalesOrderId 
							INNER JOIN trn_orderheader  
							ON trn_orderheader.intOrderNo =  OD.intOrderNo 
							AND trn_orderheader.intOrderYear = OD.intOrderYear 
							LEFT JOIN mst_customer ON trn_orderheader.intCustomer = mst_customer.intId
							LEFT JOIN mst_marketer ON trn_orderheader.intMarketer = mst_marketer.intUserId
							LEFT JOIN sys_users ON mst_marketer.intUserId = sys_users.intUserId
							LEFT JOIN costing_sample_routing_process as CSR ON OD.intSampleNo = CSR.SAMPLE_NO
                            AND CSR.SAMPLE_YEAR = OD.intSampleYear
                            AND CSR.REVISION = OD.intRevisionNo
                            AND CSR.PRINT = OD.strPrintName
                            AND CSR.COMBO = OD.strCombo
                            AND CSR.PROCESS_ID = '$stk_press_pro'
							LEFT JOIN trn_sampleinfomations_details         
							ON  OD.intSampleNo = trn_sampleinfomations_details.intSampleNo 
							AND OD.intSampleYear = trn_sampleinfomations_details.intSampleYear 
							AND OD.intRevisionNo = trn_sampleinfomations_details.intRevNo 
							AND OD.strPrintName = trn_sampleinfomations_details.strPrintName 
							AND OD.strCombo = trn_sampleinfomations_details.strComboName
							INNER JOIN trn_sampleinfomations ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo
							LEFT JOIN mst_technique_groups as TECH ON OD.TECHNIQUE_GROUP_ID = TECH.TECHNIQUE_GROUP_ID
INNER JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
							LEFT JOIN mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
							LEFT JOIN mst_part ON OD.intPart = mst_part.intId
							GROUP BY
							TB.intOrderNo,
							TB.intOrderYear,
							TB.intSalesOrderId
							HAVING todayDispQty > 0 
							) AS TB2 
							GROUP BY 
							TB2.intOrderNo,
							TB2.intOrderYear,
							TB2.intSalesOrderId 
							ORDER BY 
							TB2.customer asc,
							TB2.style asc,
							TB2.graphic asc,
							TB2.color asc,
							TB2.part asc,
							TB2.intOrderNo asc,
							TB2.intOrderYear asc,
							TB2.intSalesOrderId asc
							";
//	           echo $sql;
//	           die();
  		return $sql;
 	}
 	
 }

?>
<?php
class cls_texttile
{

	public function loadAllSearchComboDetails($year,$graphicNo,$styleId,$customerPONo,$orderNo,$customerId,$locationFlag,$location,$companyFlag,$company) 	
	{
		global $db;
		//#########################
		//######### GRAPHIC NO ####
		//#########################
        $hsStickerOnly = 0;
        $STICKER_MAKING      = '-STICKER_MAKING';

        $sql = "SELECT RESTRICTIONS FROM mst_locations WHERE intId = '$location'";
        $result = $db->RunQuery($sql);
        while($row=mysqli_fetch_array($result))
        {
            $hsStickerOnly = $row['RESTRICTIONS'];
        }

		$para='';
		if($year!='')
			$para.=" AND trn_orderheader.intOrderYear 	=  '$year'  ";
	/*	if($graphicNo!='')
			$para.="trn_orderheader.intOrderYear =  'graphicNo' AND";*/
		if($styleId!='')
			$para.=" AND trn_orderdetails.strStyleNo 		=  '$styleId'  ";
		if($customerPONo!='')
			$para.=" AND trn_orderheader.strCustomerPoNo =  '$customerPONo'  ";
		if($orderNo!='')
			$para.=" AND trn_orderheader.intOrderNo 		=  '$orderNo'  ";
		if($customerId!='')
			$para.=" AND trn_orderheader.intCustomer 	=  '$customerId'  ";	
		$sql = "SELECT DISTINCT
					trn_sampleinfomations.strGraphicRefNo
				FROM
				trn_orderdetails
					Inner Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
					Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
				WHERE
					trn_orderheader.intStatus=1
					$para
					
				ORDER BY trn_sampleinfomations.strGraphicRefNo
				";
			//	echo $sql;
		$result = $db->RunQuery($sql);
		$options ='';
		//if($orderNo=='')
		$options = "<option value=\"\"></option>";
		$options1='';
		$k=0;
		while($row=mysqli_fetch_array($result))
		{
			$k++;
			$options1 .= "<option ".($graphicNo==$row['strGraphicRefNo']?'selected':'')." value=\"".$row['strGraphicRefNo']."\">".$row['strGraphicRefNo']."</option>";
			
		}
		if($k>=1){
			$options=$options.$options1;
		}
		$response['graphicNo'] = $options;
		$response['graphicNo_sql'] = $sql;
		
		//#########################
		//######### STYLE NO ######
		//#########################
			$para='';
		if($year!='')
			$para.=" AND trn_orderheader.intOrderYear 	=  '$year'  ";
		if($graphicNo!='')
			$para.=" AND trn_sampleinfomations.strGraphicRefNo =  '$graphicNo'  ";
/*		if($styleId!='')
			$para.=" AND trn_orderdetails.strStyleNo 		=  '$styleId' ";
*/		if($customerPONo!='')
			$para.=" AND trn_orderheader.strCustomerPoNo =  '$customerPONo'  ";
		if($orderNo!='')
			$para.=" AND trn_orderheader.intOrderNo 		=  '$orderNo'  ";
		if($customerId!='')
			$para.=" AND trn_orderheader.intCustomer 	=  '$customerId'  ";	
		$sql = "SELECT DISTINCT
					trn_orderdetails.strStyleNo
				FROM
				trn_orderdetails
					Inner Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
					Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
				WHERE
					trn_orderheader.intStatus=1
					$para
					
				ORDER BY trn_orderdetails.strStyleNo
				";
				
				//echo $sql ;
		$result = $db->RunQuery($sql);
		$options ='';
		//if($orderNo=='')
		$options = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$options .= "<option ".($styleId==$row['strStyleNo']?'selected':'')." value=\"".$row['strStyleNo']."\">".$row['strStyleNo']."</option>";
		}
		$response['styleNo'] = $options;
		$response['styleNo_sql'] = $sql;
		
		//###############################
		//######### CUSTOMER PO NO ######
		//###############################
			$para='';
		if($year!='')
			$para.=" AND trn_orderheader.intOrderYear 	=  '$year'  ";
		if($graphicNo!='')
			$para.=" AND trn_sampleinfomations.strGraphicRefNo =  '$graphicNo'  ";
		if($styleId!='')
			$para.=" AND trn_orderdetails.strStyleNo 		=  '$styleId'  ";
	/*	if($customerPONo!='')
			$para.="trn_orderheader.strCustomerPoNo =  '$customerPONo' AND";*/
		if($orderNo!='')
			$para.=" AND trn_orderheader.intOrderNo 		=  '$orderNo'  ";
		if($customerId!='')
			$para.=" AND trn_orderheader.intCustomer 	=  '$customerId'  ";	
		$sql = "SELECT DISTINCT
					trn_orderheader.strCustomerPoNo
				FROM
				trn_orderdetails
					Inner Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
					Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
				WHERE
					trn_orderheader.intStatus=1
					$para
					
				ORDER BY trn_orderheader.strCustomerPoNo
				";
		$result = $db->RunQuery($sql);
		$options ='';
		//if($orderNo=='')
		$options = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$options .= "<option ".($customerPONo==$row['strCustomerPoNo']?'selected':'')." value=\"".$row['strCustomerPoNo']."\">".$row['strCustomerPoNo']."</option>";
		}
		$response['customerPoNo'] = $options;
		$response['customerPoNo_sql'] = $sql;
		
		//###############################
		//######### ORDER NO ##### ######
		//###############################
			$para='';
		if($year!='')
			$para.=" AND trn_orderheader.intOrderYear 	=  '$year'  ";
		if($graphicNo!='')
			$para.=" AND trn_sampleinfomations.strGraphicRefNo =  '$graphicNo'  ";
		if($styleId!='')
			$para.=" AND trn_orderdetails.strStyleNo 		=  '$styleId'  ";
		if($customerPONo!='')
			$para.=" AND trn_orderheader.strCustomerPoNo =  '$customerPONo' ";
		//if($orderNo!='')
			//$para.=" AND trn_orderheader.intOrderNo 		=  '$orderNo'  ";
		if($customerId!='')
			$para.=" AND trn_orderheader.intCustomer 	=  '$customerId'  ";	
		$sql = "SELECT DISTINCT
					trn_orderheader.intOrderNo
				FROM
				trn_orderdetails
					Inner Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
					Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo 
					left Join mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
					WHERE trn_orderheader.intStatus = 1 ";
					if($locationFlag==1)
					$sql .= " AND mst_locations.intId =  '$location' ";  
					if($companyFlag==1)
					$sql .= " AND mst_locations.intCompanyId =  '$company' ";
					
					$sql .= " AND trn_orderheader.intStatus=1 
					$para
					
				ORDER BY trn_orderheader.intOrderNo DESC
				";
		$result = $db->RunQuery($sql);
		$options ='';
		$options = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$options .= "<option ".($orderNo==$row['intOrderNo']?'selected':'')." value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";
		}
		$response['orderNo'] = $options;
		$response['orderNo_sql'] = $sql;
	
		//###############################
		//######### SALES ORDER NO ##### ######
		//###############################
			$para='';
		if($year!='')
			$para.=" AND trn_orderheader.intOrderYear 	=  '$year'  ";
		if($graphicNo!='')
			$para.=" AND trn_sampleinfomations.strGraphicRefNo =  '$graphicNo'  ";
		if($styleId!='')
			$para.=" AND trn_orderdetails.strStyleNo 		=  '$styleId'  ";
		if($customerPONo!='')
			$para.=" AND trn_orderheader.strCustomerPoNo =  '$customerPONo' ";
		if($orderNo!='')
			$para.=" AND trn_orderheader.intOrderNo 		=  '$orderNo'  ";
		if($customerId!='')
			$para.=" AND trn_orderheader.intCustomer 	=  '$customerId'  ";
		if ($hsStickerOnly ==1){
            $para.=" AND trn_orderdetails.SO_TYPE 	> -1  ";
        } else {
            $para.=" AND (trn_orderdetails.TECHNIQUE_GROUP_ID NOT IN (5,6) OR trn_orderdetails.SO_TYPE = -1  )";
        }
		$sql = "SELECT DISTINCT
					trn_orderdetails.strSalesOrderNo,
					trn_orderdetails.SO_TYPE
				FROM
				trn_orderdetails
					Inner Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
					Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
				WHERE
					trn_orderheader.intStatus=1
					$para
					
				ORDER BY trn_orderdetails.strSalesOrderNo ASC
				";
				
		$result = $db->RunQuery($sql);
		$options ='';
		$options = "<option value=\"\"></option>";
		$soNoToShow='';
		while($row=mysqli_fetch_array($result))
		{
            $soNoToShow = $row['strSalesOrderNo'];
			if ($row['SO_TYPE']==2){
                $soNoToShow.=$STICKER_MAKING;
            }
		    $options .= "<option ".((isset($salesOrderNo) && $salesOrderNo==$row['strSalesOrderNo'])?'selected':'')." value=\"".$row['strSalesOrderNo']."\">".$soNoToShow."</option>";
		}
		$response['salesOrderNo'] = $options;
		$response['salesOrderNo_sql'] = $sql;
		
		//###############################
		//######### SALES ORDER ID ##### ######
		//###############################
			$para='';
		if($year!='')
			$para.=" AND trn_orderheader.intOrderYear 	=  '$year'  ";
		if($graphicNo!='')
			$para.=" AND trn_sampleinfomations.strGraphicRefNo =  '$graphicNo'  ";
		if($styleId!='')
			$para.=" AND trn_orderdetails.strStyleNo 		=  '$styleId'  ";
		if($customerPONo!='')
			$para.=" AND trn_orderheader.strCustomerPoNo =  '$customerPONo' ";
		if($orderNo!='')
			$para.=" AND trn_orderheader.intOrderNo 		=  '$orderNo'  ";
		if($customerId!='')
			$para.=" AND trn_orderheader.intCustomer 	=  '$customerId'  ";	
		$sql = "SELECT DISTINCT 
					trn_orderdetails.strSalesOrderNo, 
					trn_orderdetails.intSalesOrderId, 
					mst_part.strName as part  
				FROM
				trn_orderdetails
					Inner Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
					Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
						Inner Join mst_part ON trn_orderdetails.intPart = mst_part.intId 
				WHERE
					trn_orderheader.intStatus=1
					$para
					
				ORDER BY trn_orderdetails.strSalesOrderNo ASC
				";
				
		$result = $db->RunQuery($sql);
		$options ='';
		$options = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$no = $row['strSalesOrderNo']."/".$row['part'];
 			$options .= "<option ".((isset($salesOrderNo) && $salesOrderNo==$row['intSalesOrderId'])?'selected':'')." value=\"".$row['intSalesOrderId']."\">".$no."</option>";
		}
		$response['salesOrderIds'] = $options;
		$response['salesOrderIds_sql'] = $sql;
		
	
	
		//###############################
		//######### CUSTOMER NAME  ######
		//###############################
			$para='';
		if($year!='')
			$para.=" AND trn_orderheader.intOrderYear 	=  '$year'  ";
		if($graphicNo!='')
			$para.=" AND trn_sampleinfomations.strGraphicRefNo =  '$graphicNo'  ";
		if($styleId!='')
			$para.=" AND trn_orderdetails.strStyleNo 		=  '$styleId'  ";
		if($customerPONo!='')
			$para.=" AND trn_orderheader.strCustomerPoNo =  '$customerPONo' ";
		if($orderNo!='')
			$para.=" AND trn_orderheader.intOrderNo 		=  '$orderNo'  ";
	/*	if($customerId!='')
			$para.=" AND trn_orderheader.intCustomer 	=  '$customerId'  ";*/	
		$sql = "SELECT DISTINCT
					mst_customer.strName AS customerName,mst_customer.intId
				FROM
				trn_orderdetails
					Inner Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
					Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
					Inner Join mst_customer on trn_orderheader.intCustomer = mst_customer.intId
				WHERE
					trn_orderheader.intStatus=1
					$para
					
				ORDER BY mst_customer.strName
				";
		$result = $db->RunQuery($sql);
		$options ='';
		//if($orderNo=='')
		$options = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$options .= "<option ".($customerId==$row['intId']?'selected':'')." value=\"".$row['intId']."\">".$row['customerName']."</option>";
		}
		$response['customer'] = $options;
		$response['customer_sql'] = $sql;
	
		//###############################
		//######### SAMPLE NO  ######
		//###############################
			$para='';
		if($year!='')
			$para.=" AND trn_orderheader.intOrderYear 	=  '$year'  ";
		if($graphicNo!='')
			$para.=" AND trn_sampleinfomations.strGraphicRefNo =  '$graphicNo'  ";
		if($styleId!='')
			$para.=" AND trn_orderdetails.strStyleNo 		=  '$styleId'  ";
		if($customerPONo!='')
			$para.=" AND trn_orderheader.strCustomerPoNo =  '$customerPONo' ";
		if($orderNo!='')
			$para.=" AND trn_orderheader.intOrderNo 		=  '$orderNo'  ";
	/*	if($customerId!='')
			$para.=" AND trn_orderheader.intCustomer 	=  '$customerId'  ";*/	
		$sql = "SELECT DISTINCT
					trn_sampleinfomations.intSampleNo 
				FROM
				trn_orderdetails
					Inner Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
					Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
					Inner Join mst_customer on trn_orderheader.intCustomer = mst_customer.intId
				WHERE
					trn_orderheader.intStatus=1
					$para
					
				ORDER BY trn_sampleinfomations.intSampleNo 
				";
		$result = $db->RunQuery($sql);
		$options = "";
		$options1 = "<option value=\"\"></option>";
		$i=0;
		while($row=mysqli_fetch_array($result))
		{
			$options .= "<option ".((isset($sampleNo) && $sampleNo==$row['intSampleNo'])?'selected':'')." value=\"".$row['intSampleNo']."\">".$row['intSampleNo']."</option>";
			$i++;
		}
		if($i==1){
			$options=$options.$options1;
		}
		$response['sampNo'] = $options;
		$response['sampleNo_sql'] = $sql;
	
		//###############################
		//######### SAMPLE YEAR  ######
		//###############################
			$para='';
		if($year!='')
			$para.=" AND trn_orderheader.intOrderYear 	=  '$year'  ";
		if($graphicNo!='')
			$para.=" AND trn_sampleinfomations.strGraphicRefNo =  '$graphicNo'  ";
		if($styleId!='')
			$para.=" AND trn_orderdetails.strStyleNo 		=  '$styleId'  ";
		if($customerPONo!='')
			$para.=" AND trn_orderheader.strCustomerPoNo =  '$customerPONo' ";
		if($orderNo!='')
			$para.=" AND trn_orderheader.intOrderNo 		=  '$orderNo'  ";
	/*	if($customerId!='')
			$para.=" AND trn_orderheader.intCustomer 	=  '$customerId'  ";*/	
		$sql = "SELECT DISTINCT
					trn_sampleinfomations.intSampleYear 
				FROM
				trn_orderdetails
					Inner Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
					Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
					Inner Join mst_customer on trn_orderheader.intCustomer = mst_customer.intId
				WHERE
					trn_orderheader.intStatus=1
					$para
					
				ORDER BY trn_sampleinfomations.intSampleYear 
				";
		$result = $db->RunQuery($sql);
		$options ='';
		//if($orderNo=='')
		$options = "";
		$options1 = "<option value=\"\"></option>";
		$i=0;
		while($row=mysqli_fetch_array($result))
		{
			$options .= "<option ".((isset($sampleYear) && $sampleYear==$row['intSampleYear'])?'selected':'')." value=\"".$row['intSampleYear']."\">".$row['intSampleYear']."</option>";
			$i++;
		}
		if($i==1){
			$options=$options.$options1;
		}
		$response['sampYear'] = $options;
		$response['sampleYear_sql'] = $sql;
	
		
	 	return json_encode($response);
	}
	///////////////////////////////////////
	
	public function loadAllSearchComboDetails_with_sampNo($sampNo,$sampYear,$year,$graphicNo,$styleId,$customerPONo,$orderNo,$customerId,$locationFlag,$location,$companyFlag,$company) 	
	{
		global $db;
		//#########################
		//######### GRAPHIC NO ####
		//#########################
		$para='';
		if($year!='')
			$para.=" AND trn_orderheader.intOrderYear 	=  '$year'  ";
	/*	if($graphicNo!='')
			$para.="trn_orderheader.intOrderYear =  'graphicNo' AND";*/
		if($styleId!='')
			$para.=" AND trn_orderdetails.strStyleNo 		=  '$styleId'  ";
		if($customerPONo!='')
			$para.=" AND trn_orderheader.strCustomerPoNo =  '$customerPONo'  ";
		if($orderNo!='')
			$para.=" AND trn_orderheader.intOrderNo 		=  '$orderNo'  ";
		if($customerId!='')
			$para.=" AND trn_orderheader.intCustomer 	=  '$customerId'  ";	
		$sql = "SELECT DISTINCT
					trn_sampleinfomations.strGraphicRefNo
				FROM
				trn_orderdetails
					Inner Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
					Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
				WHERE
					(trn_orderheader.intStatus-1) < intApproveLevelStart 
					$para
					
				ORDER BY trn_sampleinfomations.strGraphicRefNo
				";
			//	echo $sql;
		$result = $db->RunQuery($sql);
		$options ='';
		//if($orderNo=='')
		$options = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$options .= "<option ".($graphicNo==$row['strGraphicRefNo']?'selected':'')." value=\"".$row['strGraphicRefNo']."\">".$row['strGraphicRefNo']."</option>";
			
		}
		$response['graphicNo'] = $options;
		$response['graphicNo_sql'] = $sql;
		
		//#########################
		//######### STYLE NO ######
		//#########################
			$para='';
		if($year!='')
			$para.=" AND trn_orderheader.intOrderYear 	=  '$year'  ";
		if($graphicNo!='')
			$para.=" AND trn_sampleinfomations.strGraphicRefNo =  '$graphicNo'  ";
/*		if($styleId!='')
			$para.=" AND trn_orderdetails.strStyleNo 		=  '$styleId' ";
*/		if($customerPONo!='')
			$para.=" AND trn_orderheader.strCustomerPoNo =  '$customerPONo'  ";
		if($orderNo!='')
			$para.=" AND trn_orderheader.intOrderNo 		=  '$orderNo'  ";
		if($customerId!='')
			$para.=" AND trn_orderheader.intCustomer 	=  '$customerId'  ";	
		$sql = "SELECT DISTINCT
					trn_orderdetails.strStyleNo
				FROM
				trn_orderdetails
					Inner Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
					Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
				WHERE
					(trn_orderheader.intStatus-1) < intApproveLevelStart 
					$para
					
				ORDER BY trn_orderdetails.strStyleNo
				";
				
				//echo $sql ;
		$result = $db->RunQuery($sql);
		$options ='';
		//if($orderNo=='')
		$options = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$options .= "<option ".($styleId==$row['strStyleNo']?'selected':'')." value=\"".$row['strStyleNo']."\">".$row['strStyleNo']."</option>";
		}
		$response['styleNo'] = $options;
		$response['styleNo_sql'] = $sql;
		
		//###############################
		//######### CUSTOMER PO NO ######
		//###############################
			$para='';
		if($year!='')
			$para.=" AND trn_orderheader.intOrderYear 	=  '$year'  ";
		if($graphicNo!='')
			$para.=" AND trn_sampleinfomations.strGraphicRefNo =  '$graphicNo'  ";
		if($styleId!='')
			$para.=" AND trn_orderdetails.strStyleNo 		=  '$styleId'  ";
	/*	if($customerPONo!='')
			$para.="trn_orderheader.strCustomerPoNo =  '$customerPONo' AND";*/
		if($orderNo!='')
			$para.=" AND trn_orderheader.intOrderNo 		=  '$orderNo'  ";
		if($customerId!='')
			$para.=" AND trn_orderheader.intCustomer 	=  '$customerId'  ";	
		$sql = "SELECT DISTINCT
					trn_orderheader.strCustomerPoNo
				FROM
				trn_orderdetails
					Inner Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
					Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
				WHERE
					(trn_orderheader.intStatus-1) < intApproveLevelStart 
					$para
					
				ORDER BY trn_orderheader.strCustomerPoNo
				";
		$result = $db->RunQuery($sql);
		$options ='';
		//if($orderNo=='')
		$options = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$options .= "<option ".($customerPONo==$row['strCustomerPoNo']?'selected':'')." value=\"".$row['strCustomerPoNo']."\">".$row['strCustomerPoNo']."</option>";
		}
		$response['customerPoNo'] = $options;
		$response['customerPoNo_sql'] = $sql;
		
		//###############################
		//######### ORDER NO ##### ######
		//###############################
			$para='';
		if($year!='')
			$para.=" AND trn_orderheader.intOrderYear 	=  '$year'  ";
		if($graphicNo!='')
			$para.=" AND trn_sampleinfomations.strGraphicRefNo =  '$graphicNo'  ";
		if($styleId!='')
			$para.=" AND trn_orderdetails.strStyleNo 		=  '$styleId'  ";
		if($customerPONo!='')
			$para.=" AND trn_orderheader.strCustomerPoNo =  '$customerPONo' ";
		//if($orderNo!='')
			//$para.=" AND trn_orderheader.intOrderNo 		=  '$orderNo'  ";
		if($customerId!='')
			$para.=" AND trn_orderheader.intCustomer 	=  '$customerId'  ";	
		$sql = "SELECT DISTINCT
					trn_orderheader.intOrderNo
				FROM
				trn_orderdetails
					Inner Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
					Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo 
					left Join mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
					WHERE (trn_orderheader.intStatus-1) < intApproveLevelStart ";
					if($locationFlag==1)
					$sql .= " AND mst_locations.intId =  '$location' ";  
					if($companyFlag==1)
					$sql .= " AND mst_locations.intCompanyId =  '$company' ";
					
					//$sql .= " AND trn_orderheader.intStatus=1  ";
					$sql .= "$para
					
				ORDER BY trn_orderheader.intOrderNo DESC
				";
		$result = $db->RunQuery($sql);
		$options ='';
		$options = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$options .= "<option ".($orderNo==$row['intOrderNo']?'selected':'')." value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";
		}
		$response['orderNo'] = $options;
		$response['orderNo_sql'] = $sql;
	
		//###############################
		//######### SALES ORDER NO ##### ######
		//###############################
			$para='';
		if($year!='')
			$para.=" AND trn_orderheader.intOrderYear 	=  '$year'  ";
		if($graphicNo!='')
			$para.=" AND trn_sampleinfomations.strGraphicRefNo =  '$graphicNo'  ";
		if($styleId!='')
			$para.=" AND trn_orderdetails.strStyleNo 		=  '$styleId'  ";
		if($customerPONo!='')
			$para.=" AND trn_orderheader.strCustomerPoNo =  '$customerPONo' ";
		if($orderNo!='')
			$para.=" AND trn_orderheader.intOrderNo 		=  '$orderNo'  ";
		if($customerId!='')
			$para.=" AND trn_orderheader.intCustomer 	=  '$customerId'  ";	
		$sql = "SELECT DISTINCT
					trn_orderdetails.strSalesOrderNo,	concat(trn_orderdetails.strSalesOrderNo,'/',trn_orderdetails.strCombo,'/',trn_orderdetails.strPrintName,'/',mst_part.strName) as so
				FROM
				trn_orderdetails
					Inner Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
					Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
					INNER JOIN mst_part ON trn_orderdetails.intPart = mst_part.intId

				WHERE
					(trn_orderheader.intStatus-1) < intApproveLevelStart 
					$para
					
				ORDER BY trn_orderdetails.strSalesOrderNo ASC
				";
				
		$result = $db->RunQuery($sql);
		$options ='';
		$options = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$options .= "<option ".($salesOrderNo==$row['strSalesOrderNo']?'selected':'')." value=\"".$row['strSalesOrderNo']."\">".$row['so']."</option>";
		}
		$response['salesOrderNo'] = $options;
		$response['salesOrderNo_sql'] = $sql;
		

		//###############################
		//######### SALES ORDER NO ##### ######
		//###############################
			$para='';
		if($year!='')
			$para.=" AND trn_orderheader.intOrderYear 	=  '$year'  ";
		if($graphicNo!='')
			$para.=" AND trn_sampleinfomations.strGraphicRefNo =  '$graphicNo'  ";
		if($styleId!='')
			$para.=" AND trn_orderdetails.strStyleNo 		=  '$styleId'  ";
		if($customerPONo!='')
			$para.=" AND trn_orderheader.strCustomerPoNo =  '$customerPONo' ";
		if($orderNo!='')
			$para.=" AND trn_orderheader.intOrderNo 		=  '$orderNo'  ";
		if($customerId!='')
			$para.=" AND trn_orderheader.intCustomer 	=  '$customerId'  ";	
		$sql = "SELECT DISTINCT  
					trn_orderdetails.intSalesOrderId, 
					mst_part.strName as part  ,
 					trn_orderdetails.strSalesOrderNo
				FROM
				trn_orderdetails
					Inner Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
					Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
						Inner Join mst_part ON trn_orderdetails.intPart = mst_part.intId 
				WHERE
					(trn_orderheader.intStatus-1) < intApproveLevelStart 
					AND trn_orderdetails.SO_TYPE > -1
					$para
					
				ORDER BY trn_orderdetails.strSalesOrderNo ASC
				";
				
		$result = $db->RunQuery($sql);
		$options ='';
		$options = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$no = $row['strSalesOrderNo']."/".$row['part'];
			$options .= "<option ".($salesOrderNo==$row['intSalesOrderId']?'selected':'')." value=\"".$row['intSalesOrderId']."\">".$no."</option>";
		}
		$response['salesOrderId'] = $options;
		$response['salesOrderId_sql'] = $sql;
		


		//###############################
		//######### CUSTOMER NAME  ######
		//###############################
			$para='';
		if($year!='')
			$para.=" AND trn_orderheader.intOrderYear 	=  '$year'  ";
		if($graphicNo!='')
			$para.=" AND trn_sampleinfomations.strGraphicRefNo =  '$graphicNo'  ";
		if($styleId!='')
			$para.=" AND trn_orderdetails.strStyleNo 		=  '$styleId'  ";
		if($customerPONo!='')
			$para.=" AND trn_orderheader.strCustomerPoNo =  '$customerPONo' ";
		if($orderNo!='')
			$para.=" AND trn_orderheader.intOrderNo 		=  '$orderNo'  ";
	/*	if($customerId!='')
			$para.=" AND trn_orderheader.intCustomer 	=  '$customerId'  ";*/	
		$sql = "SELECT DISTINCT
					mst_customer.strName AS customerName,mst_customer.intId
				FROM
				trn_orderdetails
					Inner Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
					Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
					Inner Join mst_customer on trn_orderheader.intCustomer = mst_customer.intId
				WHERE
					(trn_orderheader.intStatus-1) < intApproveLevelStart 
					$para
					
				ORDER BY mst_customer.strName
				";
		$result = $db->RunQuery($sql);
		$options ='';
		//if($orderNo=='')
		$options = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$options .= "<option ".($customerId==$row['intId']?'selected':'')." value=\"".$row['intId']."\">".$row['customerName']."</option>";
		}
		$response['customer'] = $options;
		$response['customer_sql'] = $sql;
	
		//###############################
		//######### SAMPLE NO  ######
		//###############################
			$para='';
		if($year!='')
			$para.=" AND trn_orderheader.intOrderYear 	=  '$year'  ";
		if($graphicNo!='')
			$para.=" AND trn_sampleinfomations.strGraphicRefNo =  '$graphicNo'  ";
		if($styleId!='')
			$para.=" AND trn_orderdetails.strStyleNo 		=  '$styleId'  ";
		if($customerPONo!='')
			$para.=" AND trn_orderheader.strCustomerPoNo =  '$customerPONo' ";
		if($orderNo!='')
			$para.=" AND trn_orderheader.intOrderNo 		=  '$orderNo'  ";
	/*	if($customerId!='')
			$para.=" AND trn_orderheader.intCustomer 	=  '$customerId'  ";*/	
		$sql = "SELECT DISTINCT
					trn_sampleinfomations.intSampleNo 
				FROM
				trn_orderdetails
					Inner Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
					Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
					Inner Join mst_customer on trn_orderheader.intCustomer = mst_customer.intId
				WHERE
					(trn_orderheader.intStatus-1) < intApproveLevelStart 
					$para
					
				ORDER BY trn_sampleinfomations.intSampleNo 
				";
		$result = $db->RunQuery($sql);
		$options = "";
		$options = "<option value=\"\"></option>";
		$i=0;
		while($row=mysqli_fetch_array($result))
		{
			$options .= "<option ".($sampNo==$row['intSampleNo']?'selected':'')." value=\"".$row['intSampleNo']."\">".$row['intSampleNo']."</option>";
			$i++;
			$sn=$row['intSampleNo'];
		if($i==1){
			//$options=$options.$options1;
			$sampNo=$sn;
		}
		}
		$response['sampNo'] = $options;
		$response['sampleNo_sql'] = $sql;
	
		//###############################
		//######### SAMPLE YEAR  ######
		//###############################
			$para='';
		if($year!='')
			$para.=" AND trn_orderheader.intOrderYear 	=  '$year'  ";
		if($graphicNo!='')
			$para.=" AND trn_sampleinfomations.strGraphicRefNo =  '$graphicNo'  ";
		if($styleId!='')
			$para.=" AND trn_orderdetails.strStyleNo 		=  '$styleId'  ";
		if($customerPONo!='')
			$para.=" AND trn_orderheader.strCustomerPoNo =  '$customerPONo' ";
		if($orderNo!='')
			$para.=" AND trn_orderheader.intOrderNo 		=  '$orderNo'  ";
	/*	if($customerId!='')
			$para.=" AND trn_orderheader.intCustomer 	=  '$customerId'  ";*/	
		$sql = "SELECT DISTINCT
					trn_sampleinfomations.intSampleYear 
				FROM
				trn_orderdetails
					Inner Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
					Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
					Inner Join mst_customer on trn_orderheader.intCustomer = mst_customer.intId
				WHERE
					(trn_orderheader.intStatus-1) < intApproveLevelStart 
					$para  ";
				if($sampNo!='')
		$sql .= " AND trn_sampleinfomations.intSampleNo ='$sampNo'	";	
					
		$sql .= " ORDER BY trn_sampleinfomations.intSampleYear ";
				 
		$result = $db->RunQuery($sql);
		$options ='';
		//if($orderNo=='')
		$options = "";
		$options = "<option value=\"\"></option>";
		$i=0;
		while($row=mysqli_fetch_array($result))
		{
			$options .= "<option ".($sampYear==$row['intSampleYear']?'selected':'')." value=\"".$row['intSampleYear']."\">".$row['intSampleYear']."</option>";
			$i++;
			$sy = $row['intSampleYear'];
		if($i==1){
			//$options=$options.$options1;
			$syear=$sy;
		}
		}
		$response['sampYear'] = $syear;
		$response['sampleYear_sql'] = $sql;
	
		
	 	return json_encode($response);
	}
	
	/////////////////////////////////////
	public function getLocationValidation($type,$location,$company,$serialNo,$year){
		
		if($type=='FRN'){
		        $sql="SELECT
					mst_locations.intCompanyId
					FROM
					trn_orderheader
					Inner Join mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
					WHERE
					trn_orderheader.intOrderNo =  '$serialNo' AND
					trn_orderheader.intOrderYear =  '$year'";
					
				$msg='Invalid Save Company';
		}

			$result = $this->db->RunQuery2($sql);
		    $row=mysqli_fetch_array($result);
		 	   //  echo $row['location']."==".$location;
				
				if($row['intCompanyId']==$company){
				$data['type'] = 'pass';
				$data['msg'] = '';
				}
				else{
				$data['type'] = 'fail';
				$data['msg'] = $msg;
				}
				return $data;
	}
	
	public function getFRNexcessFactor2($company){
		
			global $db;
	
			$sql="SELECT
				IFNULL(sys_company_config.dblFRNexcessFactor,0) as excessFactor 
				FROM sys_company_config
				WHERE
				sys_company_config.intCompanyId =  '$company'";

			$result = $db->RunQuery2($sql);
		    $row=mysqli_fetch_array($result);
			return $row['excessFactor'];
	}
	
/*	public function getFRNexcessFactor($company){
		
			global $db;
	
			$sql="SELECT
				IFNULL(sys_company_config.dblFRNexcessFactor,0) as excessFactor 
				FROM sys_company_config
				WHERE
				sys_company_config.intCompanyId =  '$company'";


			$result = $db->RunQuery($sql);
		    $row=mysqli_fetch_array($result);
			return $row['excessFactor'];
	}
*/	
	public function getNewTolerencePercentage2($company,$salesOrderQty){
		
		  global $db;
	
		  $sql="(
				select   IFNULL(dblPercentage,0) as dblPercentage , 
				dblQty 
				from     mst_fabric_tolerancelevels
				where    mst_fabric_tolerancelevels.dblQty > '$salesOrderQty' 
				and      mst_fabric_tolerancelevels.intCompany = '$company'
				order by mst_fabric_tolerancelevels.dblQty asc
				limit 1
				)
				union
				(
				select   IFNULL(dblPercentage,0) as dblPercentage , 
				dblQty 
				from     mst_fabric_tolerancelevels
				where   mst_fabric_tolerancelevels. dblQty <= '$salesOrderQty' 
				and      mst_fabric_tolerancelevels.intCompany = '$company'
				order by mst_fabric_tolerancelevels. dblQty desc
				limit 1
				)
				order by abs(dblQty)
				limit 1";

			$result = $db->RunQuery2($sql);
		    $row=mysqli_fetch_array($result);
		
			return $row['dblPercentage'];
	}
	public function getNewTolerencePercentage($company,$salesOrderQty){
		
		  global $db;
	
		  $sql="(
				select   IFNULL(dblPercentage,0) as dblPercentage , 
				dblQty 
				from     mst_fabric_tolerancelevels
				where    mst_fabric_tolerancelevels.dblQty > '$salesOrderQty' 
				and      mst_fabric_tolerancelevels.intCompany = '$company'
				order by mst_fabric_tolerancelevels.dblQty asc
				limit 1
				)
				union
				(
				select   IFNULL(dblPercentage,0) as dblPercentage , 
				dblQty 
				from     mst_fabric_tolerancelevels
				where   mst_fabric_tolerancelevels. dblQty <= '$salesOrderQty' 
				and      mst_fabric_tolerancelevels.intCompany = '$company'
				order by mst_fabric_tolerancelevels. dblQty desc
				limit 1
				)
				order by abs(dblQty)
				limit 1";

			$result = $db->RunQuery($sql);
		    $row=mysqli_fetch_array($result);
			return $row['dblPercentage'];
	}


	public function getSavedTolerencePercentage($orderNo,$orderYear,$salesOrderId){
		
		  global $db;
	
		  $sql="SELECT
				IFNULL(trn_orderdetails.dblToleratePercentage,0) as dblToleratePercentage  
				FROM trn_orderdetails
				WHERE
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear' AND
				trn_orderdetails.intSalesOrderId =  '$salesOrderId'";

			$result = $db->RunQuery($sql);
		    $row=mysqli_fetch_array($result);
			return $row['dblToleratePercentage'];
	}
	
	public function getSavedTolerencePercentage2($orderNo,$orderYear,$salesOrderId){
		
		  global $db;
	
		  $sql="SELECT
				IFNULL(trn_orderdetails.dblToleratePercentage,0) as dblToleratePercentage  
				FROM trn_orderdetails
				WHERE
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear' AND
				trn_orderdetails.intSalesOrderId =  '$salesOrderId'";

			$result = $db->RunQuery2($sql);
		    $row=mysqli_fetch_array($result);
			return $row['dblToleratePercentage'];
	}
	
	public function getOrderSatatusForKPIreport(){
		
		  global $db;
	
		  $sql="SELECT DISTINCT
				trn_orderheader.intStatus
				FROM `trn_orderheader`
				WHERE
				trn_orderheader.intStatus <> 0
			";
		  $result = $db->RunQuery($sql);
			
			$string="";
			while($row=mysqli_fetch_array($result))
			{
				$string .= "'".$row['intStatus']."',";
				$i++;
			}

			$string=substr($string, 0, -1);
			return $string;
	}
}
?>
<?php
class cls_customer_and_operation
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	
	public function validateSubContractQty($serialNo,$serialYear,$locationId,$executionType)
	{
		$sql 	= $this->validateSubContractQty_sql($serialNo,$serialYear,$locationId);
		$result	= $this->db->$executionType($sql);
		
		return $result;
	}
	public function getSubContractReturnDetail($serialNo,$serialYear,$executionType)
	{
		$sql 	= $this->getSubContractReturnDetail_sql($serialNo,$serialYear);
		$result	= $this->db->$executionType($sql);
		
		return $result;
	}
	private function validateSubContractQty_sql($serialNo,$serialYear,$locationId)
	{
		$sql = "SELECT trn_order_sub_contract_header.ORDER_NO,
				trn_order_sub_contract_header.ORDER_YEAR,
				SCSW.SALES_ORDER_ID,
				SCSW.QTY,
				trn_orderdetails.strSalesOrderNo,
				SCSW.SIZE,
				
				(SELECT IFNULL(SUM(QTY),0)
				FROM trn_order_sub_contract_size_wise_qty
				INNER JOIN trn_order_sub_contract_header SCH ON trn_order_sub_contract_size_wise_qty.SUB_CONTRACT_NO=SCH.SUB_CONTRACT_NO AND
				trn_order_sub_contract_size_wise_qty.SUB_CONTRACT_YEAR=SCH.SUB_CONTRACT_YEAR
				WHERE SCH.ORDER_NO = trn_order_sub_contract_header.ORDER_NO AND
				SCH.ORDER_YEAR = trn_order_sub_contract_header.ORDER_YEAR AND
				trn_order_sub_contract_size_wise_qty.SALES_ORDER_ID = SCSW.SALES_ORDER_ID AND
				trn_order_sub_contract_size_wise_qty.SIZE = SCSW.SIZE AND
				SCH.STATUS = 1) AS totSavedQty,
				
				(SELECT IFNULL(SUM(dblQty),0)
				FROM trn_ordersizeqty OSQ
				LEFT JOIN trn_orderheader OH ON OH.intOrderNo=OSQ.intOrderNo AND
				OH.intOrderYear = OSQ.intOrderYear
				WHERE OSQ.intOrderNo = trn_order_sub_contract_header.ORDER_NO AND
				OSQ.intOrderYear = trn_order_sub_contract_header.ORDER_YEAR AND
				OSQ.intSalesOrderId = SCSW.SALES_ORDER_ID AND
				OSQ.strSize = SCSW.SIZE AND
				OH.intStatus = 1) AS totOrderQty
				
				
				FROM trn_order_sub_contract_header
				LEFT JOIN trn_order_sub_contract_size_wise_qty SCSW ON SCSW.SUB_CONTRACT_NO=trn_order_sub_contract_header.SUB_CONTRACT_NO AND
				SCSW.SUB_CONTRACT_YEAR=trn_order_sub_contract_header.SUB_CONTRACT_YEAR
				INNER JOIN trn_ordersizeqty ON trn_ordersizeqty.intOrderNo = trn_order_sub_contract_header.ORDER_NO AND
				trn_ordersizeqty.intOrderYear=trn_order_sub_contract_header.ORDER_YEAR AND
				trn_ordersizeqty.intSalesOrderId=SCSW.SALES_ORDER_ID AND
				trn_ordersizeqty.strSize=SCSW.SIZE
				INNER JOIN trn_orderdetails ON trn_orderdetails.intOrderNo=trn_order_sub_contract_header.ORDER_NO AND
				trn_orderdetails.intOrderYear=trn_order_sub_contract_header.ORDER_YEAR AND
				trn_orderdetails.intSalesOrderId=SCSW.SALES_ORDER_ID
				WHERE 
				trn_order_sub_contract_header.SUB_CONTRACT_NO = '$serialNo' AND
				trn_order_sub_contract_header.SUB_CONTRACT_YEAR = '$serialYear' AND
				trn_order_sub_contract_header.LOCATION_ID = '$locationId' ";
		
		return $sql;
	}
	private function getSubContractReturnDetail_sql($serialNo,$serialYear)
	{
		$sql = "SELECT SALES_ORDER_ID,
				OD.strSalesOrderNo,
				SUB_CONTR_JOB_ID,
				SCJT.NAME AS subConJobType,
				CUT_NO,
				SIZE,
				qty,
				(SELECT SUM(QTY)
				FROM trn_order_sub_contract_gate_pass_details SCGPD
				WHERE
				SCGPD.SUB_CONTRACT_GP_NO=SCGPH.SUB_CONTRACT_GP_NO AND
				SCGPD.SUB_CONTRACT_GP_YEAR=SCGPH.SUB_CONTRACT_GP_YEAR AND
				SCGPD.SALES_ORDER_ID=trn_order_sub_contract_return_details.SALES_ORDER_ID AND
				SCGPD.CUT_NO=trn_order_sub_contract_return_details.CUT_NO AND
				SCGPD.SIZE=trn_order_sub_contract_return_details.SIZE) AS GATEPASS_QTY,
				(SELECT SUM(BAL_TO_RETURN_QTY)
				FROM trn_order_sub_contract_gate_pass_details SCGPD
				WHERE
				SCGPD.SUB_CONTRACT_GP_NO=SCGPH.SUB_CONTRACT_GP_NO AND
				SCGPD.SUB_CONTRACT_GP_YEAR=SCGPH.SUB_CONTRACT_GP_YEAR AND
				SCGPD.SALES_ORDER_ID=trn_order_sub_contract_return_details.SALES_ORDER_ID AND
				SCGPD.CUT_NO=trn_order_sub_contract_return_details.CUT_NO AND
				SCGPD.SIZE=trn_order_sub_contract_return_details.SIZE) AS BAL_TO_RETURN_QTY
				FROM trn_order_sub_contract_return_details
				
				INNER JOIN trn_order_sub_contract_return_header SCRH ON SCRH.SUB_CONTRACT_RETURN_NO=trn_order_sub_contract_return_details.SUB_CONTRACT_RETURN_NO AND
				SCRH.SUB_CONTRACT_RETURN_YEAR=trn_order_sub_contract_return_details.SUB_CONTRACT_RETURN_YEAR
				INNER JOIN trn_order_sub_contract_gate_pass_header SCGPH ON SCGPH.SUB_CONTRACT_GP_NO=SCRH.SUB_CONTRACT_GP_NO AND
				SCGPH.SUB_CONTRACT_GP_YEAR=SCRH.SUB_CONTRACT_GP_YEAR
				INNER JOIN trn_order_sub_contract_header SCH ON SCH.SUB_CONTRACT_NO=SCGPH.SUB_CONTRACT_NO AND
				SCH.SUB_CONTRACT_YEAR=SCGPH.SUB_CONTRACT_YEAR
				INNER JOIN trn_orderdetails OD ON OD.intOrderNo=SCH.ORDER_NO AND
				OD.intOrderYear=SCH.ORDER_YEAR AND
				OD.intSalesOrderId=trn_order_sub_contract_return_details.SALES_ORDER_ID
				INNER JOIN mst_sub_contract_job_types SCJT ON SCJT.ID=trn_order_sub_contract_return_details.SUB_CONTR_JOB_ID
				WHERE
				trn_order_sub_contract_return_details.SUB_CONTRACT_RETURN_NO = '$serialNo' AND
				trn_order_sub_contract_return_details.SUB_CONTRACT_RETURN_YEAR = '$serialYear' ";
		
		return $sql;
	}
}
<?php
class Cls_sampleindex_get
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	public function getRequisitionSummary($monthStart,$monthEnd,$userId,$mainDb,$userType)
	{
		return $this->getRequisitionSummary_sql($monthStart,$monthEnd,$userId,$mainDb,$userType);
	}
	public function getNewsSticker($userId,$userType,$mainDb)
	{
		return $this->getNewsSticker_sql($userId,$userType,$mainDb);	
	}
	private function getRequisitionSummary_sql($monthStart,$monthEnd,$userId,$mainDb,$userType)
	{
		$sql = "SELECT
				IFNULL(SUM(ReqQty),0) AS totReqQty,
				IFNULL(SUM(DispatchQty),0) AS totDispatchQty,
				IFNULL(SUM(fabInQty),0) AS totFabInQty
				FROM
				(
				SELECT SUM(SRD.QTY) AS ReqQty,
				/*(SELECT SUM(SFID.INQTY)
				FROM $mainDb.brndx_sampleorder_fabricin_details SFID
				INNER JOIN $mainDb.brndx_sampleorder_fabricin_header SFIH ON SFIH.SAMPLE_FABRICIN_NO=SFID.SAMPLE_FABRICIN_NO AND
				SFIH.SAMPLE_FABRICIN_YEAR=SFID.SAMPLE_FABRICIN_YEAR
				INNER JOIN $mainDb.brndx_sampleorder_header SOH ON SOH.intSampleOrderNo=SFIH.SAMPLE_ORDER_NO AND
				SOH.intSampleOrderYear=SFIH.SAMPLE_FABRICIN_YEAR
				WHERE SOH.intSampleReqNo=SRH.REQUISITION_NO AND
				SOH.intSampleReqYear=SRH.REQUISITION_YEAR AND
				SFIH.STATUS = 1) AS fabInQty,*/
				
				SUM(SRD.FABRIC_IN_QTY) AS fabInQty , 
				SUM(SRD.DISPATCH_QTY) AS DispatchQty
				
				FROM trn_sample_requisition_header SRH
				INNER JOIN trn_sample_requisition_detail SRD ON SRH.REQUISITION_NO=SRD.REQUISITION_NO AND
				SRH.REQUISITION_YEAR=SRD.REQUISITION_YEAR
				WHERE 
				SRH.STATUS = 1 ";
		
		if($userType=='M')
			$sql.= "AND SRH.CREATED_BY = '$userId' AND SRD.SAMPLE_TYPE = 6 ";
		else
			$sql.= "AND (SRH.MODIFY_BY = '$userId' OR SRH.CREATED_BY = '$userId') ";
		
		$sql.="AND DATE(SRH.CREATED_DATE) BETWEEN ('$monthStart') AND ('$monthEnd')
				GROUP BY SRH.REQUISITION_NO,SRH.REQUISITION_YEAR
				) AS tb_1 ";
		
		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row;
	}
	private function getNewsSticker_sql($userId,$userType,$mainDb)
	{
		$sql = "SELECT DEL.GRAPHIC_NO,
				DEL.LOG_DATE_TIME AS transacDate,
				DEL.TYPE AS transacType
				FROM dashboard_event_log DEL
				WHERE 
				1=1 ";
				
		if($userType=='M')
			$sql.= "AND DEL.CREATED_BY = '$userId' AND DEL.SAMPLE_TYPE_ID = 6 ";
		else
			$sql.= "AND (DEL.MODIFY_BY = '$userId' OR DEL.CREATED_BY = '$userId') ";	
				
		$sql .= "GROUP BY DEL.LOG_DATE_TIME
				 ORDER BY DEL.LOG_DATE_TIME DESC 
				 LIMIT 0,6 ";
	//echo $sql;
	$result = $this->db->RunQuery($sql);
	return $result;
	}
}
?>
<?php
$session_userId		= $_SESSION["userId"];
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];

class cls_gain_loss_set
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
 	public function Save_Gain_Loss_header($serialNo,$serialYear,$catId,$catType,$year,$month,$day,$date)
	{
		return $this->Save_Gain_Loss_header_sql($serialNo,$serialYear,$catId,$catType,$year,$month,$day,$date);
	}
	
	
	private function Save_Gain_Loss_header_sql($serialNo,$serialYear,$catId,$catType,$year,$month,$day,$date)
	{
		global $session_companyId;
		global $session_locationId;
		global $session_userId;
		
		$sql = "INSERT INTO finance_gain_loss_header
				(DOCUMENT_NO,
				DOCUMENT_YEAR,
				CATEGORY_ID,
				CATEGORY_TYPE,
				YEAR,
				MONTH,
				DAY,
				TIME)
				VALUES (
				'$serialNo',
				'$serialYear',
				$catId,
				'$catType',
				'$year',
				$month,
				$day,
 				NOW());";				
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$response["type"]		= 'fail';
			$response["ErrorMsg"]	= $this->db->errormsg;
			$response["ErrorSql"]	= $sql;
		}
		return $response;
	}
	
}
?>
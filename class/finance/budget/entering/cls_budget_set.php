<?php
class cls_budget_set{
	
	private $db;
		
	function __construct($db)
	{
		$this->db = $db;	
	}
	public function updateHeader($budg_location,$budg_department,$budg_year,$status,$approveLevels,$userId,$executeType)
	{
		return $this->updateHeader_sql($budg_location,$budg_department,$budg_year,$status,$approveLevels,$userId,$executeType);
	}
	public function update_header_status($budg_location,$budg_department,$budg_year,$status,$executeType)
	{ 

   		$sql	= $this->update_header_status_sql($budg_location,$budg_department,$budg_year,$status);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		}
			return $response;
 	}
	public function insertHistoryDetails($budg_location,$budg_department,$budg_year,$historyId,$executeType)
	{
		return $this->insertHistoryDetails_sql($budg_location,$budg_department,$budg_year,$historyId,$executeType);
	}
	
	public function approved_by_insert($budg_location,$budg_department,$budg_year,$userId,$approval,$executeType)
	{ 

   		$sql	= $this->approved_by_insert_sql($budg_location,$budg_department,$budg_year,$userId,$approval);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		}
			return $response;
 	}
	
	public function approved_by_update($budg_location,$budg_department,$budg_year,$maxAppByStatus,$executeType)
	{ 

   		$sql	= $this->approved_by_update_sql($budg_location,$budg_department,$budg_year,$maxAppByStatus);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		}
			return $response;
 	}
	public function saveHeader($locationID,$department,$financeYear,$userId,$status,$approveLevels)
	{
		return $this->saveHeader_sql($locationID,$department,$financeYear,$userId,$status,$approveLevels);
	}
	public function updateHeader2($locationID,$department,$financeYear,$userId,$status,$approveLevels)
	{
		return $this->updateHeader2_sql($locationID,$department,$financeYear,$userId,$status,$approveLevels);
	}
	public function updateApproveByStatus($locationID,$department,$financeYear,$maxStatus)
	{
		return $this->updateApproveByStatus_sql($locationID,$department,$financeYear,$maxStatus);
	}
	public function deleteDetails($locationID,$department,$financeYear)
	{
		return $this->deleteDetails_sql($locationID,$department,$financeYear);
	}
	public function saveDetails($locationID,$department,$financeYear,$subCatId,$itemId,$month,$budgetVal,$type)
	{
		return $this->saveDetails_sql($locationID,$department,$financeYear,$subCatId,$itemId,$month,$budgetVal,$type);
	}
	public function updatePlanDetails_units_or_amount($locationID,$department,$financeYear,$subCatId,$itemId,$month,$budgAmtAmount,$budgUntAmount,$transType,$fieldType,$executeType)
	{
		return $this->updatePlanDetails_units_or_amount_sql($locationID,$department,$financeYear,$subCatId,$itemId,$month,$budgAmtAmount,$budgUntAmount,$transType,$fieldType,$executeType);
	}
	
	public function update_array_PlanDetails_units_or_amount($location,$department,$budg_year,$budg_month,$type,$operation_type,$response){
		return $this->update_array_PlanDetails_units_or_amount_sql($location,$department,$budg_year,$budg_month,$type,$operation_type,$response);	
	}
	public function updateDetails($locationID,$department,$financeYear,$subCatId,$itemId,$month,$budgetVal,$type)
	{
		return $this->updateDetails_sql($locationID,$department,$financeYear,$subCatId,$itemId,$month,$budgetVal,$type);
	}
	
//BEGIN - PRIVATE FUNTIONS	
	private function updateHeader_sql($budg_location,$budg_department,$budg_year,$status,$approveLevels,$userId,$executeType)
	{
		$sql = "UPDATE budget_plan_header
				  SET STATUS = '$status',
				  APPROVE_LEVELS = '$approveLevels',
				  MODIFIED_BY = '$userId',
				  MODIFIED_DATE = NOW()
				WHERE LOCATION_ID = '$budg_location'
					AND DEPARTMENT_ID = '$budg_department'
					AND FINANCE_YEAR_ID = '$budg_year'
					;";
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= false;
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= true;
			$response['msg']	= "Update successfully!";
			$response['sql']	= $sql;
		}
			return $response;
		
	}
	
	private function update_header_status_sql($budg_location,$budg_department,$budg_year,$status)
	{
 		
		$sql = "UPDATE budget_plan_header 
				SET 
				STATUS='$status' 
				WHERE LOCATION_ID = '$budg_location'
				AND DEPARTMENT_ID = '$budg_department'
				AND FINANCE_YEAR_ID = '$budg_year'";
 		return $sql;
	}
	
	private function approved_by_insert_sql($budg_location,$budg_department,$budg_year,$userId,$approval){
 		
		$sql = "INSERT INTO `budget_plan_approve_by` (`LOCATION_ID`,`DEPARTMENT_ID`,`FINANCE_YEAR_ID`,`APPROVED_LEVEL_NO`,APPROVED_BY,APPROVED_DATE,STATUS) 
			VALUES ('$budg_location','$budg_department','$budg_year','$approval','$userId',now(),0)";
 		
		return $sql;
	}
	
	
	private function approved_by_update_sql($budg_location,$budg_department,$budg_year,$maxAppByStatus){
 		
		  	$sql = "UPDATE `budget_plan_approve_by` SET STATUS ='$maxAppByStatus' 
					WHERE (`LOCATION_ID`='$budg_location') AND (`DEPARTMENT_ID`='$budg_department')  AND (`FINANCE_YEAR_ID`='$budg_year') AND (`STATUS`='0')";

		return $sql;
	}
	private function insertHistoryDetails_sql($budg_location,$budg_department,$budg_year,$historyId,$executeType){
 		
		
		$sql	=" INSERT INTO budget_plan_details_history
					( 
					  `HISTORY_ID`,	
					 `LOCATION_ID`,
					 `DEPARTMENT_ID`,
					 `FINANCE_YEAR_ID`,
					 `SUB_CATEGORY_ID`,
					 `ITEM_ID`,
					 MONTH,
					 INITIAL_BUDGET,
					 TRANSFER_BUDGET,
					 ADDITIONAL_BUDGET,
					 MRN_BUDGET,
					 ISSUE_BUDGET,
					 INITIAL_BUDGET_UNITS,
					 TRANSFER_BUDGET_UNITS,
					 ADDITIONAL_BUDGET_UNITS,
					 MRN_BUDGET_UNITS,
					 ISSUE_BUDGET_UNITS 
				) 
		  			 SELECT 
					 $historyId,
					`LOCATION_ID`,
					`DEPARTMENT_ID`,
					`FINANCE_YEAR_ID`,
					`SUB_CATEGORY_ID`,
					`ITEM_ID`,
					MONTH,
					INITIAL_BUDGET,
					TRANSFER_BUDGET,
					ADDITIONAL_BUDGET,
					MRN_BUDGET,
					ISSUE_BUDGET,
					INITIAL_BUDGET_UNITS,
					TRANSFER_BUDGET_UNITS,
					ADDITIONAL_BUDGET_UNITS,
					MRN_BUDGET_UNITS,
					ISSUE_BUDGET_UNITS 
			   FROM budget_plan_details 
			   WHERE (`LOCATION_ID`='$budg_location') AND (`DEPARTMENT_ID`='$budg_department')  AND (`FINANCE_YEAR_ID`='$budg_year')
				";		
			$result = $this->db->$executeType($sql);
			if(!$result){
				$response['type']	= 'fail';
				$response['msg']	= $this->db->errormsg;
				$response['sql']	= $sql;
			}
			else{
				$response['type']	= 'pass';
				$response['msg']	= '';
				$response['sql']	= $sql;
			}
				return $response;
	}
	private function saveHeader_sql($locationID,$department,$financeYear,$userId,$status,$approveLevels)
	{
		$sql = "INSERT INTO budget_plan_header 
				(
				LOCATION_ID, 
				DEPARTMENT_ID, 
				FINANCE_YEAR_ID, 
				STATUS, 
				APPROVE_LEVELS, 
				CREATED_BY, 
				CREATED_DATE
				)
				VALUES
				(
				'$locationID', 
				'$department', 
				'$financeYear', 
				'$status', 
				'$approveLevels', 
				'$userId', 
				NOW()
				);";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function updateHeader2_sql($locationID,$department,$financeYear,$userId,$status,$approveLevels)
	{
		$sql = "UPDATE budget_plan_header 
				SET
				STATUS = '$status' , 
				APPROVE_LEVELS = '$approveLevels' , 
				MODIFY_BY = '$userId' , 
				MODIFY_DATE = NOW()
				
				WHERE
				LOCATION_ID = '$locationID' AND 
				DEPARTMENT_ID = '$department' AND 
				FINANCE_YEAR_ID = '$financeYear' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function updateApproveByStatus_sql($locationID,$department,$financeYear,$maxStatus)
	{
		$sql = "UPDATE budget_plan_approve_by 
				SET
				STATUS = $maxStatus+1
				WHERE
				LOCATION_ID = '$locationID' AND 
				DEPARTMENT_ID = '$department' AND 
				FINANCE_YEAR_ID = '$financeYear' AND
				STATUS = 0 ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function deleteDetails_sql($locationID,$department,$financeYear)
	{
		$sql = "DELETE FROM budget_plan_details 
				WHERE
				LOCATION_ID = '$locationID' AND 
				DEPARTMENT_ID = '$department' AND 
				FINANCE_YEAR_ID = '$financeYear' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function saveDetails_sql($locationID,$department,$financeYear,$subCatId,$itemId,$month,$budgetVal,$type)
	{
		$sql = "INSERT INTO budget_plan_details 
				(
				LOCATION_ID, 
				DEPARTMENT_ID, 
				FINANCE_YEAR_ID, 
				SUB_CATEGORY_ID, 
				ITEM_ID, 
				MONTH, 
				INITIAL_BUDGET, 
				INITIAL_BUDGET_UNITS
				)
				VALUES
				(
				'$locationID', 
				'$department', 
				'$financeYear', 
				'$subCatId', 
				'$itemId', 
				'$month', ";
		if($type=='A')
		{
			$sql .="'$budgetVal',
					'0' ";
		}
		if($type=='U')
			$sql .="'0',
					'$budgetVal' ";		
			
			$sql .=")";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	
	private function updatePlanDetails_units_or_amount_sql($locationID,$department,$financeYear,$subCatId,$itemId,$month,$budgAmtAmount,$budgUntAmount,$transType,$fieldType,$executeType){
	
		$sql = " UPDATE budget_plan_details 
				SET ";
		if(($transType=='TRANSFER') && ($fieldType=='A'))		
		$sql .= " TRANSFER_BUDGET = IFNULL(TRANSFER_BUDGET,0) + $budgAmtAmount ";
		else if(($transType=='TRANSFER') && ($fieldType=='U'))		
		$sql .= " TRANSFER_BUDGET_UNITS = IFNULL(TRANSFER_BUDGET_UNITS,0) + $budgUntAmount ";
		else if(($transType=='ADDITIONAL') && ($fieldType=='A'))		
		$sql .= " ADDITIONAL_BUDGET = IFNULL(ADDITIONAL_BUDGET,0) + $budgAmtAmount ";
		else if(($transType=='ADDITIONAL') && ($fieldType=='U'))		
		$sql .= " ADDITIONAL_BUDGET_UNITS = IFNULL(ADDITIONAL_BUDGET_UNITS,0) + $budgUntAmount ";
		else if(($transType=='MRN') && ($fieldType=='A'))		
		$sql .= " MRN_BUDGET = IFNULL(MRN_BUDGET,0) + $budgAmtAmount ";
		else if(($transType=='MRN') && ($fieldType=='U'))		
		$sql .= " MRN_BUDGET_UNITS = IFNULL(MRN_BUDGET_UNITS,0) + $budgUntAmount ";
		else if(($transType=='ISSUE') && ($fieldType=='A'))		
		$sql .= " ISSUE_BUDGET = IFNULL(ISSUE_BUDGET,0) + $budgAmtAmount ";
		else if(($transType=='ISSUE') && ($fieldType=='U'))		
		$sql .= " ISSUE_BUDGET_UNITS = IFNULL(ISSUE_BUDGET_UNITS,0) + $budgUntAmount ";
		
		$sql .= " WHERE
				LOCATION_ID = '$locationID' AND 
				DEPARTMENT_ID = '$department' AND 
				FINANCE_YEAR_ID = '$financeYear' "; 
		if($subCatId !='' &&  $subCatId !=0)		
		$sql .= "  AND SUB_CATEGORY_ID = '$subCatId' ";
		if($itemId !='' &&  $itemId != 0)		
		$sql .= "  AND ITEM_ID = '$itemId' "; 
		$sql .= "  AND MONTH = '$month' "; 
 // echo $sql;
		$result = $this->db->$executeType($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
		
	}
	
	private function update_array_PlanDetails_units_or_amount_sql($location,$department,$budg_year,$budg_month,$type,$operation_type,$response_arr){
		
		$rollBackFlag 	= 0;
		$savedStatus	= 'pass';

		$arr_item_u		=$response_arr['arr_item_u'];
		$arr_item_a		=$response_arr['arr_item_a'];
		$arr_sub_cat_u	=$response_arr['arr_sub_cat_u'];
		$arr_sub_cat_a	=$response_arr['arr_sub_cat_a'];
		
			if($rollBackFlag != 1){
				while (list($key_i, $value_i) = each($arr_item_u)) {
					$response_b		= $this->updatePlanDetails_units_or_amount($location,$department,$budg_year,'',$key_i,$budg_month,0,$value_i*$operation_type,$type,'U','RunQuery2');
					if($response_b['savedStatus']	 == 'fail'){
						$rollBackFlag		=1;
						$rollBackMsg		.=$response_b['savedMassege'];
						$sqlM				.=$response_b['error_sql'];
					}
				}
			}
			if($rollBackFlag != 1){
				while (list($key_i, $value_i) = each($arr_item_a)) {
					$response_b		= $this->updatePlanDetails_units_or_amount($location,$department,$budg_year,'',$key_i,$budg_month,$value_i*$operation_type,0,$type,'A','RunQuery2');
					if($response_b['savedStatus']	 == 'fail'){
						$rollBackFlag		=1;
						$rollBackMsg		.=$response_b['savedMassege'];
						$sqlM				.=$response_b['error_sql'];
					}
				}
			}
			if($rollBackFlag != 1){
				while (list($key, $value) = each($arr_sub_cat_u)) {
					$response_b		= $this->updatePlanDetails_units_or_amount($location,$department,$budg_year,$key,0,$budg_month,0,$value*$operation_type,$type,'U','RunQuery2');
					if($response_b['savedStatus']	 == 'fail'){
						$rollBackFlag		=1;
						$rollBackMsg		.=$response_b['savedMassege'];
						$sqlM				.=$response_b['error_sql'];
					}
				}
			}
			if($rollBackFlag != 1){
				while (list($key, $value) = each($arr_sub_cat_a)) {
					$response_b		= $this->updatePlanDetails_units_or_amount($location,$department,$budg_year,$key,0,$budg_month,$value*$operation_type,0,$type,'A','RunQuery2');
					if($response_b['savedStatus']	 == 'fail'){
						$rollBackFlag		=1;
						$rollBackMsg		.=$response_b['savedMassege'];
						$sqlM				.=$response_b['error_sql'];
					}
				}
			}
			if($rollBackFlag==1){
				$savedStatus	= 'fail';
			}
		
			$response_s['type']	= $savedStatus;
			$response_s['msg']	= $this->db->errormsg;
			$response_s['sql']	= $sqlM;
			
			return $response_s;
	}
	private function updateDetails_sql($locationID,$department,$financeYear,$subCatId,$itemId,$month,$budgetVal,$type)
	{
	 	$sql = "UPDATE budget_plan_details 
				SET ";
		
		if($type=='A')
			$sql .="INITIAL_BUDGET = '$budgetVal' ";
		
		if($type=='U')
			$sql .="INITIAL_BUDGET_UNITS = '$budgetVal' ";	
		
		$sql .="WHERE
				LOCATION_ID = '$locationID' AND 
				DEPARTMENT_ID = '$department' AND 
				FINANCE_YEAR_ID = '$financeYear' AND 
				SUB_CATEGORY_ID = '$subCatId' AND 
				MONTH = '$month' AND 
				ITEM_ID = '$itemId' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
}
 
//END - PRIVATE FUNCTION
?>
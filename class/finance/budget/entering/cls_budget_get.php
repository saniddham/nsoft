<?php
class cls_budget_get
{
	
	private $db;
		
	function __construct($db)
	{
		$this->db = $db;	
	}
//BEGIN - PUBLIC FUNCTIONS {	
	public function get_header($company,$location,$department,$year,$executeType)
	{
		$sql	= $this->get_header_sql($company,$location,$department,$year);
		$row	= $this->get_row($sql,$executeType);
		return $row;
	}
	
	public function getdetails($company,$location,$department,$year,$executeType)
	{
		$sql	= $this->get_details_sql($company,$location,$department,$year);
		$result	= $this->get_results($sql,$executeType);
		return $result;
	}
	
	public function getFinanceMonths($financeYear)
	{
		return $this->getFinanceMonths_sql($financeYear);
	}
	
	public function get_Report_approval_details_result($location,$department,$year,$executeType){
		$sql = $this->Load_Report_approval_details_sql($location,$department,$year);
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	
	public function getSubCategory($companyID,$locationID)
	{
		return $this->getSubCategory_sql($companyID,$locationID);
	}
	public function getAllocatedBudget($locationId,$department,$financeYearId,$month,$subCatId,$itemId)
	{
		return $this->getAllocatedBudget_sql($locationId,$department,$financeYearId,$month,$subCatId,$itemId);
	}
	public function getReportGridDetails($locationId,$departmentId,$finaceYearId)
	{
		return $this->getReportGridDetails_sql($locationId,$departmentId,$finaceYearId);
	}
	
	public function getBudgetMonthlyAmount($locationId,$departmentId,$finaceYearId,$catId,$itemId,$month)
	{
		return $this->getBudgetMonthlyAmount_sql($locationId,$departmentId,$finaceYearId,$catId,$itemId,$month);
	}
	public function getSubCatItem($company,$locationId,$subCatId)
	{
		return $this->getSubCatItem_sql($company,$locationId,$subCatId);
	}
	public function getMaxStatus($locationID,$department,$financeYear)
	{
		return $this->getMaxStatus_sql($locationID,$department,$financeYear);
	}
	public function get_next_history_id($locationID,$department,$financeYear)
	{
		return $this->get_next_history_id_sql($locationID,$department,$financeYear);
	}
	
	public function getBudgetMonthlyAmountHistory($lastRevisionNo,$locationId,$departmentId,$finaceYearId,$catId,$itemId,$month)
	{
		return $this->getBudgetMonthlyAmountHistory_sql($lastRevisionNo,$locationId,$departmentId,$finaceYearId,$catId,$itemId,$month);
	}
	
	public function getLastRevisionNo($locationId,$departmentId,$finaceYearId)
	{
		return $this->getLastRevisionNo_sql($locationId,$departmentId,$finaceYearId);
	}
	public function checkBudgetPlanAvailable($locationID,$department,$financeYear,$subCatId,$itemId,$month)
	{
		return $this->checkBudgetPlanAvailable_sql($locationID,$department,$financeYear,$subCatId,$itemId,$month);
	}
	
	public function getBudgetDetails($company,$locationID,$department)
	{
		return $this->getBudgetDetails_sql($company,$locationID,$department);
	}
//END	- PUBLIC FUNCTIONS }
	
//BEGIN - PRIVATE FUNCTIONS {
	private function get_header_sql($company,$location,$department,$year)
	{		
		$sql	= "SELECT
					BH.LOCATION_ID					AS LOCATION_ID,
					LO.intCompanyId					AS COMPANY_ID,
					LO.strName 						AS LOCATION_NAME,
					BH.DEPARTMENT_ID				AS DEPARTMENT_ID,
					D.strName  						AS DEPARTMENT_NAME,
					BH.FINANCE_YEAR_ID				AS FINANCE_YEAR_ID,
					CONCAT(FP.dtmStartingDate,' | ',FP.dtmClosingDate) AS CONCAT_PERIOD,
					BH.STATUS						AS STATUS,
					BH.APPROVE_LEVELS 				AS LEVELS,
					BH.CREATED_BY,
					BH.CREATED_DATE,
					BH.MODIFY_BY,
					BH.MODIFY_DATE,
					sys_users.strUserName AS USER
					FROM `budget_plan_header` BH
					INNER JOIN sys_users ON BH.CREATED_BY = sys_users.intUserId
					INNER JOIN mst_locations LO ON LO.intId = BH.LOCATION_ID
					INNER JOIN mst_department D ON D.intId = BH.DEPARTMENT_ID
					INNER JOIN mst_financeaccountingperiod FP ON FP.intId = BH.FINANCE_YEAR_ID
					WHERE
					BH.LOCATION_ID = '$location' AND
					BH.DEPARTMENT_ID = '$department' AND
					BH.FINANCE_YEAR_ID = '$year'";
		
		return $sql;		
	}

	private function get_details_sql($company,$location,$department,$year){
		
		$sql	= "SELECT
					budget_plan_details.SUB_CATEGORY_ID,
					budget_plan_details.ITEM_ID,
					mst_subcategory.strName AS sub_cat_name,
					budget_plan_details.`MONTH`,
					budget_plan_details.INITIAL_BUDGET,
					budget_plan_details.TRANSFER_BUDGET,
					budget_plan_details.ADDITIONAL_BUDGET,
					budget_plan_details.MRN_BUDGET,
					budget_plan_details.ISSUE_BUDGET,
					budget_plan_details.INITIAL_BUDGET_UNITS,
					budget_plan_details.`TRANSFER_BUDGET_UNITS`,
					budget_plan_details.ADDITIONAL_BUDGET_UNITS,
					budget_plan_details.MRN_BUDGET_UNITS,
					budget_plan_details.ISSUE_BUDGET_UNITS,
					mst_item.strName as ITEM_NAME
					FROM `budget_plan_details` 
					INNER JOIN mst_subcategory ON budget_plan_details.SUB_CATEGORY_ID = mst_subcategory.intId 
					LEFT JOIN mst_item ON budget_plan_details.ITEM_ID = mst_item.intId 
					WHERE
					budget_plan_details.LOCATION_ID = '$location' AND
					budget_plan_details.DEPARTMENT_ID = '$department' AND
					budget_plan_details.FINANCE_YEAR_ID = '$year'";
		
		return $sql;
		
	}

	private function get_results($sql,$executeType){
	
		$result	= $this->db->$executeType($sql);
		return $result;		
	}
	private function get_row($sql,$executeType){
	
		$result	= $this->db->$executeType($sql);
		$row=mysqli_fetch_array($result);
		return $row;		
	}


	private function getFinanceMonths_sql($financeYearId)
	{
		$sql = "SELECT YEAR(dtmStartingDate) AS startYear,
				MONTH(dtmStartingDate) AS startMonth,
				YEAR(dtmClosingDate) AS endYear,
				MONTH(dtmClosingDate) AS endMonth
				FROM mst_financeaccountingperiod
				WHERE intId = '$financeYearId' ";
		
		$result	= $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row;
	}	
	private function Load_Report_approval_details_sql($location,$department,$year){
	
		
	   	$sql = "SELECT
 				budget_plan_approve_by.APPROVED_DATE AS dtApprovedDate ,
				sys_users.strUserName as UserName,
				budget_plan_approve_by.APPROVED_LEVEL_NO as intApproveLevelNo
				FROM
				budget_plan_approve_by
				Inner Join sys_users ON budget_plan_approve_by.APPROVED_BY = sys_users.intUserId
				WHERE
				budget_plan_approve_by.LOCATION_ID =  '$location' AND
				budget_plan_approve_by.DEPARTMENT_ID =  '$department' AND
				budget_plan_approve_by.FINANCE_YEAR_ID =  '$year'  
				 order by APPROVED_DATE asc";
				
 		return $sql;
	}
	
	
	private function getSubCategory_sql($companyID,$locationID)
	{
		$sql	= "	SELECT BCL.SUB_CATEGORY_ID,MC.strName AS subCatName,BCL.BUDGET_TYPE,
					(SELECT COUNT(SUB_CATEGORY_ID) FROM mst_budget_category_list
					WHERE SUB_CATEGORY_ID=BCL.SUB_CATEGORY_ID AND
					ITEM_ID !=0) AS itemCount
					FROM mst_budget_category_list BCL
					INNER JOIN mst_subcategory MC ON MC.intId=BCL.SUB_CATEGORY_ID
					WHERE BCL.COMPANY_ID = '$companyID' AND
					BCL.LOCATION_ID = '$locationID'
					GROUP BY SUB_CATEGORY_ID
					ORDER BY MC.strName ";
						
		$result	= $this->db->RunQuery($sql);
				
		return $result;	
	}
	private function getAllocatedBudget_sql($locationId,$department,$financeYearId,$month,$subCatId,$itemId)
	{
		$sql = "SELECT INITIAL_BUDGET,INITIAL_BUDGET_UNITS
				FROM budget_plan_details	
				WHERE 
				LOCATION_ID = '$locationId' AND 
				DEPARTMENT_ID = '$department' AND 
				FINANCE_YEAR_ID = '$financeYearId' AND 
				SUB_CATEGORY_ID = '$subCatId' AND
				ITEM_ID	= '$itemId' AND
				MONTH = '$month' ;";//echo $sql;
	
		$result	= $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row;
	}
	
	private function getReportGridDetails_sql($locationId,$departmentId,$finaceYearId)
	{
		$sql = "SELECT DISTINCT
				  SC.intId 			AS SUB_CATEGORY_ID,
				  MC.strName 		AS MAIN_CATEGORY_NAME,
				  SC.strName 		AS SUB_CATEGORY_NAME,
				  I.intId			AS ITEM_ID,
				  I.strName			AS ITEM_NAME,
				  MU.strCode 		AS UNIT
				FROM budget_plan_details BD
				  INNER JOIN mst_subcategory SC
					ON SC.intId = BD.SUB_CATEGORY_ID
				  INNER JOIN mst_maincategory MC
					ON MC.intId = SC.intMainCategory
				  LEFT JOIN mst_item I
				    ON I.intId = BD.ITEM_ID
				  LEFT JOIN mst_units MU ON MU.intId=I.intUOM	
				 WHERE LOCATION_ID = '$locationId'
					AND DEPARTMENT_ID = '$departmentId'
					AND FINANCE_YEAR_ID = '$finaceYearId'";
		return $this->db->RunQuery($sql);
	}
	
	private function getBudgetMonthlyAmount_sql($locationId,$departmentId,$finaceYearId,$catId,$itemId,$month)
	{
		$sql = "SELECT DISTINCT
			  ROUND(COALESCE(INITIAL_BUDGET,0),2)		AS INITIAL_BUDGET,
			  ROUND(COALESCE(INITIAL_BUDGET_UNITS,0),2)	AS INITIAL_BUDGET_UNITS
			FROM budget_plan_details BD
			WHERE LOCATION_ID = '$locationId'
				AND DEPARTMENT_ID = '$departmentId'
				AND FINANCE_YEAR_ID = '$finaceYearId'
				AND SUB_CATEGORY_ID = '$catId'
				AND BD.ITEM_ID = '$itemId'
				AND MONTH = '$month'";
		$result = $this->db->RunQuery($sql);
		return mysqli_fetch_array($result);
	}
	private function getSubCatItem_sql($company,$locationId,$subCatId)
	{
		$sql = "SELECT BCL.ITEM_ID,MI.strName AS itemName,BCL.BUDGET_TYPE,MU.strCode AS unit
				FROM mst_budget_category_list BCL
				INNER JOIN mst_subcategory MC ON MC.intId=BCL.SUB_CATEGORY_ID
				INNER JOIN mst_item MI ON MI.intId=BCL.ITEM_ID
				INNER JOIN mst_units MU ON MU.intId=MI.intUOM
				WHERE BCL.COMPANY_ID = '$company' AND
				BCL.LOCATION_ID = '$locationId' AND
				BCL.SUB_CATEGORY_ID = '$subCatId'
				ORDER BY MI.strName ";
		
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	private function getMaxStatus_sql($locationID,$department,$financeYear)
	{
		$sql = "SELECT MAX(tb.STATUS) AS maxStatus  
				FROM budget_plan_approve_by AS tb
				WHERE 
				tb.LOCATION_ID='$locationID' AND
				tb.DEPARTMENT_ID='$department' AND
				tb.FINANCE_YEAR_ID='$financeYear' ";
		
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		return $row['maxStatus'];
	}
	private function get_next_history_id_sql($locationID,$department,$financeYear){
		$sql 	= "SELECT COALESCE(MAX(HISTORY_ID),0)+1 AS MAX_ID 
				   FROM budget_plan_details_history 
				   WHERE (`LOCATION_ID`='$locationID') AND (`DEPARTMENT_ID`='$department')  AND (`FINANCE_YEAR_ID`='$financeYear')";
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		return $row['MAX_ID'];
	}
	
	private function getBudgetMonthlyAmountHistory_sql($lastRevisionNo,$locationId,$departmentId,$finaceYearId,$catId,$itemId,$month)
	{
		$sql = "SELECT DISTINCT
			  ROUND(COALESCE(INITIAL_BUDGET,0),2)		AS INITIAL_BUDGET,
			  ROUND(COALESCE(INITIAL_BUDGET_UNITS,0),2)	AS INITIAL_BUDGET_UNITS
			FROM budget_plan_details_history BD
			WHERE LOCATION_ID = '$locationId'
				AND DEPARTMENT_ID = '$departmentId'
				AND FINANCE_YEAR_ID = '$finaceYearId'
				AND SUB_CATEGORY_ID = '$catId'
				AND BD.ITEM_ID = '$itemId'
				AND MONTH = '$month'
				AND HISTORY_ID = $lastRevisionNo";
		$result = $this->db->RunQuery($sql);
		return mysqli_fetch_array($result);
	}
	
	private function getLastRevisionNo_sql($locationId,$departmentId,$finaceYearId)
	{
		$sql = "SELECT
				  COALESCE(MAX(HISTORY_ID),0) AS LAST_HISTORY_ID
				FROM budget_plan_details_history DH
				WHERE DH.LOCATION_ID = '$locationId'
					AND DH.DEPARTMENT_ID = '$departmentId'
					AND DH.FINANCE_YEAR_ID = '$finaceYearId'";
		$result = $this->db->RunQuery($sql);
		$row 	= mysqli_fetch_array($result);
		return $row["LAST_HISTORY_ID"];
	}
	private function checkBudgetPlanAvailable_sql($locationID,$department,$financeYear,$subCatId,$itemId,$month)
	{
		$sql = "SELECT * 
				FROM budget_plan_details
				WHERE LOCATION_ID = '$locationID' AND
				DEPARTMENT_ID = '$department' AND
				FINANCE_YEAR_ID = '$financeYear' AND
				SUB_CATEGORY_ID = '$subCatId' AND
				ITEM_ID = '$itemId' AND
				MONTH = '$month' ";
		
		$result = $this->db->RunQuery2($sql);
		if(mysqli_num_rows($result)>0)
			return true;
		else
			return false;
	}
	
	
	private function getBudgetDetails_sql($company,$locationID,$department)
	{
		$sql = "SELECT
				  BCL.SUB_CATEGORY_ID				AS SUB_CATEGORY_ID ,
				  MC.strName          				AS SUB_CATEGORY_NAME,
				  I.intId							AS ITEM_ID,	
				  I.strName							AS ITEM_NAME,											
				  BCL.BUDGET_TYPE					AS BUDGET_TYPE,
				  MU.strCode 						AS UNIT		
				FROM mst_budget_category_list BCL
				  INNER JOIN mst_subcategory MC
					ON MC.intId = BCL.SUB_CATEGORY_ID
				  INNER JOIN mst_budget_category_list_department_wise W
					ON W.SERIAL_ID = BCL.SERIAL_ID
				  LEFT JOIN mst_item I
					ON I.intId = BCL.ITEM_ID
				  LEFT JOIN mst_units MU 
					ON MU.intId = I.intUOM
				WHERE BCL.COMPANY_ID = '$company'
					AND BCL.LOCATION_ID = '$locationID'
					AND W.DEPARTMENT_ID = $department 
				ORDER BY MC.strName";
		return $this->db->RunQuery($sql);
	}
//END 	- PRIVATE FUNCTIONS }
}
?>
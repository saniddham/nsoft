<?php

class cls_common_function_budget_get
{
	
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	public function loadMainCategory($executionType)
	{
		$sql	= " SELECT
					mst_maincategory.strName,
					mst_maincategory.intId
					FROM
					mst_maincategory
					WHERE
					mst_maincategory.intStatus = 1
					ORDER BY
					mst_maincategory.intId";
					
		$result = $this->db->$executionType($sql);
		
		return $result;
		
	}
	public function loadSubCategory($mainCat,$executionType)
	{
		$sql	= "	SELECT
					mst_subcategory.intId,
					mst_subcategory.strName
					FROM
					mst_subcategory
					WHERE
					mst_subcategory.intMainCategory = '$mainCat' AND
					mst_subcategory.intStatus = 1";
					
		$result = $this->db->$executionType($sql);
		
		return $result;
	}
	
	public function loadFinanceYear($company,$executionType)
	{
		$sql = "SELECT AP.intId,
				CONCAT(YEAR(AP.dtmStartingDate),'/',YEAR(AP.dtmClosingDate)) AS financeYear,
				YEAR(dtmStartingDate) AS startYear
				FROM mst_financeaccountingperiod AP
				INNER JOIN mst_financeaccountingperiod_companies APC ON APC.intPeriodId=AP.intId
				WHERE APC.intCompanyId = '$company'
				ORDER BY AP.dtmStartingDate ";
		
		$result = $this->db->$executionType($sql);
		
		return $result;
	}
	
	public function loadFinanceYear_with_selected_date($company,$date,$executionType)
	{
		$sql = "SELECT AP.intId,
				CONCAT(YEAR(AP.dtmStartingDate),'/',YEAR(AP.dtmClosingDate)) AS financeYear,
				YEAR(dtmStartingDate) AS startYear
				FROM mst_financeaccountingperiod AP
				INNER JOIN mst_financeaccountingperiod_companies APC ON APC.intPeriodId=AP.intId
				WHERE APC.intCompanyId = '$company' ";
		if($date!=''){
		$sql .= " AND DATE(dtmStartingDate) <= '$date' AND DATE(dtmClosingDate) >= '$date'";	
		}
		$sql .= " ORDER BY AP.dtmStartingDate ";
		
		$result = $this->db->$executionType($sql);
		
		return $result;
	}
	
	public function load_actual_year_for_financial_year_and_month($location,$budgetYear,$month,$executionType)
	{
		$sql = "SELECT AP.intId,
				CONCAT(YEAR(AP.dtmStartingDate),'/',YEAR(AP.dtmClosingDate)) AS financeYear,
				YEAR(dtmStartingDate) AS startYear, 
				IF(($month >= 4),YEAR(AP.dtmStartingDate),YEAR(AP.dtmClosingDate)) as actualYear
				FROM mst_financeaccountingperiod AP
				INNER JOIN mst_financeaccountingperiod_companies APC ON APC.intPeriodId=AP.intId
				INNER JOIN mst_locations ON mst_locations.intCompanyId = APC.intCompanyId 
				WHERE mst_locations.intId = '$location' ";
		if($budgetYear!='') 
		$sql .= "  AND AP.intId = '$budgetYear'";	
		$sql .= " ORDER BY AP.dtmStartingDate ";
		//echo $sql;
		$result = $this->db->$executionType($sql);
		$row=mysqli_fetch_array($result);
		return $row['actualYear'];		
	}
	
	public function check_for_approved_next_records($budg_location,$budg_department,$budg_year,$executeType)
	{
		$sqlC	= " SELECT 
					fa.dtmClosingDate
					FROM
					mst_financeaccountingperiod fa
					WHERE fa.intId  = '$budg_year' ";

		$resultC 		= $this->db->$executeType($sqlC);
		$rowC	 		= mysqli_fetch_array($resultC);
		 
		$closingDate 	= ($rowC['dtmClosingDate']==''?0:$rowC['dtmClosingDate']);
				
		$sql 	= " SELECT
					budget_plan_header.FINANCE_YEAR_ID 
					FROM
					budget_plan_header
					INNER JOIN mst_financeaccountingperiod ON budget_plan_header.FINANCE_YEAR_ID = mst_financeaccountingperiod.intId 
					WHERE 
					mst_financeaccountingperiod.dtmStartingDate > '".$closingDate."' 
					AND budget_plan_header.STATUS =  '1' 
					AND budget_plan_header.LOCATION_ID =  '$budg_location' 
					AND budget_plan_header.DEPARTMENT_ID = '$budg_department'
					LIMIT 1
				";	
 		
		$result = $this->db->$executeType($sql);
		$row	= mysqli_fetch_array($result);
		return $row['FINANCE_YEAR_ID'];	
		
	}
	
	public function load_budget_balance($budg_location,$budg_department,$budg_year,$sub_category,$item,$month,$executeType)
	{
		$sql	= "SELECT 
					COALESCE(SUM(IFNULL(INITIAL_BUDGET,0)+IFNULL(TRANSFER_BUDGET,0)+IFNULL(ADDITIONAL_BUDGET,0)-IFNULL(ISSUE_BUDGET,0)),0) as balAmount ,
					COALESCE(SUM(IFNULL(INITIAL_BUDGET_UNITS,0)+IFNULL(TRANSFER_BUDGET_UNITS,0)+IFNULL(ADDITIONAL_BUDGET_UNITS,0)-IFNULL(ISSUE_BUDGET_UNITS,0)),0) as balUnits, 
					COALESCE(SUM(IFNULL(INITIAL_BUDGET,0)),0) as initialBudetAmount ,
					COALESCE(SUM(IFNULL(INITIAL_BUDGET_UNITS,0)),0) as initialBudetUnits, 
					COALESCE(SUM(IFNULL(INITIAL_BUDGET,0)+IFNULL(TRANSFER_BUDGET,0)+IFNULL(ADDITIONAL_BUDGET,0)),0) as totBudgetAmount ,
					COALESCE(SUM(IFNULL(INITIAL_BUDGET_UNITS,0)+IFNULL(TRANSFER_BUDGET_UNITS,0)+IFNULL(ADDITIONAL_BUDGET_UNITS,0)),0) as totBudgetUnits  
					FROM `budget_plan_details`
					INNER JOIN budget_plan_header ON budget_plan_header.LOCATION_ID=budget_plan_details.LOCATION_ID AND
					budget_plan_header.DEPARTMENT_ID=budget_plan_details.DEPARTMENT_ID AND
					budget_plan_header.FINANCE_YEAR_ID=budget_plan_details.FINANCE_YEAR_ID
					WHERE
					budget_plan_header.STATUS = 1
					AND budget_plan_details.LOCATION_ID = '$budg_location'  
					AND budget_plan_details.FINANCE_YEAR_ID = '$budg_year' ";
		if($budg_department != '')
		$sql	.=" AND budget_plan_details.DEPARTMENT_ID = '$budg_department' ";
		if($month != '')
		$sql	.=" AND budget_plan_details.MONTH = '$month' ";
		if($sub_category != '')
		$sql	.=" AND budget_plan_details.SUB_CATEGORY_ID = '$sub_category' ";
		/*if($description!='')
		$sql	.=" AND	mst_item.strName LIKE  '%$description%'";*///new
		if($item != '' && $item != 0)
		$sql	.=" AND budget_plan_details.ITEM_ID = '$item' ";
		
 //echo $sql;
		$result	= $this->db->$executeType($sql);
		$row=mysqli_fetch_array($result);
		return $row;		
		//die(print_r($row));
	}
	
	public function load_budget_balance_for_mrn($budg_location,$budg_department,$budg_year,$sub_category,$item,$month,$executeType)
	{
		$sql	= "SELECT 
					SUM(IFNULL(INITIAL_BUDGET,0)+IFNULL(TRANSFER_BUDGET,0)+IFNULL(ADDITIONAL_BUDGET,0)-IFNULL(MRN_BUDGET,0)) as balAmount ,
					SUM(IFNULL(INITIAL_BUDGET_UNITS,0)+IFNULL(TRANSFER_BUDGET_UNITS,0)+IFNULL(ADDITIONAL_BUDGET_UNITS,0)-IFNULL(MRN_BUDGET_UNITS,0)) as balUnits 
					FROM `budget_plan_details`
					WHERE
					budget_plan_details.LOCATION_ID = '$budg_location'  
					AND budget_plan_details.FINANCE_YEAR_ID = '$budg_year' ";
		if($budg_department != '')
		$sql	.=" AND budget_plan_details.DEPARTMENT_ID = '$budg_department' ";
		if($month != '')
		$sql	.=" AND budget_plan_details.MONTH = '$month' ";
		if($sub_category != '')
		$sql	.=" AND budget_plan_details.SUB_CATEGORY_ID = '$sub_category' ";
		if($item != '' && $item != 0)
		$sql	.=" AND budget_plan_details.ITEM_ID = '$item' ";
		//echo $sql;
		$result	= $this->db->$executeType($sql);
		$row=mysqli_fetch_array($result);
		return $row;		
		
	}
	public function load_approved_service_po_budget($locationId,$subCat,$item,$actualYear,$month,$executeType)
	{
		if($month==12) {
			$date_from	= $actualYear."-".str_pad($month, 2, "0", STR_PAD_LEFT)."-01";
			$date_to	= ($actualYear+1)."-".str_pad(1, 2, "0", STR_PAD_LEFT)."-01";
		}
		else{
			$date_from	= $actualYear."-".str_pad($month, 2, "0", STR_PAD_LEFT)."-01";
			$date_to	= $actualYear."-".str_pad($month+1, 2, "0", STR_PAD_LEFT)."-01";
		}
		
	     	$sql	="SELECT
					sum(trn_podetails.dblQty*trn_podetails.dblUnitPrice*fe1.dblBuying/fe2.dblBuying) as poAmount , 
					sum(trn_podetails.dblQty) as poUnits  
					FROM
					trn_poheader  
					INNER JOIN trn_podetails ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear
					INNER JOIN mst_locations ON mst_locations.intId = trn_poheader.intCompany
					INNER JOIN mst_companies ON mst_locations.intCompanyId = mst_companies.intId
					LEFT JOIN mst_financeexchangerate AS fe1 ON trn_poheader.intCurrency = fe1.intCurrencyId AND date(trn_poheader.dtmPODate)= fe1.dtmDate AND mst_locations.intCompanyId = fe1.intCompanyId
					LEFT JOIN mst_financeexchangerate AS fe2 ON mst_companies.intBaseCurrencyId= fe2.intCurrencyId AND date(trn_poheader.dtmPODate)= fe2.dtmDate AND mst_locations.intCompanyId = fe2.intCompanyId
					LEFT JOIN mst_item ON trn_podetails.intItem = mst_item.intId 
					WHERE 
					trn_poheader.intStatus	= 1 AND 
					trn_poheader.intCompany = '$locationId' ";
					if($subCat != '')
					$sql .=" AND mst_item.intSubCategory = '$subCat' ";
					if($item != '')
					$sql .=" AND mst_item.intId = '$item' ";
					$sql .=" AND date(trn_poheader.dtmPODate) >= '$date_from' AND 	date(trn_poheader.dtmPODate) < '$date_to' 
					";
					//echo $sql;
					$result	= $this->db->$executeType($sql);
					$row=mysqli_fetch_array($result);
		return $row;		
		
	}
	
	
	public function load_budget_balance_for_selected_trns_type($location,$department,$budg_year,$sub_cat,$item,$budg_month,$transType,$unitType,$executeType){
		
		if($transType == 'SERVICE_PO'){
			$budg_bal_arr	=$this->load_budget_balance($location,$department,$budg_year,$sub_cat,$item,$budg_month,$executeType);
			$actulYear		=$this->load_actual_year_for_financial_year_and_month($location,$budg_year,$budg_month,$executeType);
			$service_po_arr	=$this->load_approved_service_po_budget($location,$sub_cat,$item,$actulYear,$budg_month,$executeType);
			//echo $unitType;
			if($unitType == 'A')
				$budg_bal['balAmount'] 	=$budg_bal_arr['totBudgetAmount']-$service_po_arr['poAmount'];
			else
				$budg_bal['balUnits'] 	=$budg_bal_arr['totBudgetUnits']-$service_po_arr['poUnits'];
		}
		//print_r($budg_bal_arr);
		return $budg_bal;
		
	}
	
	
	public function load_budget_type($budg_location,$sub_category,$item,$executeType)
	{
		$sql	= "SELECT
					mst_budget_category_list.COMPANY_ID,
					mst_budget_category_list.LOCATION_ID,
					mst_budget_category_list.SUB_CATEGORY_ID,
					mst_budget_category_list.ITEM_ID,
					mst_budget_category_list.BUDGET_TYPE
					FROM `mst_budget_category_list`
					WHERE
					mst_budget_category_list.LOCATION_ID = '$budg_location' AND
					mst_budget_category_list.SUB_CATEGORY_ID = '$sub_category' ";
		if($item != '' && $item != 0)
		$sql	.=" AND mst_budget_category_list.ITEM_ID = '$item' ";

		$result	= $this->db->$executeType($sql);
		$row=mysqli_fetch_array($result);
		return $row['BUDGET_TYPE'];		
		
	}
	
	public function check_sub_category_budgeted_or_not($budg_location,$sub_category,$executeType)
	{
		  $sql	= "SELECT
					mst_budget_category_list.COMPANY_ID,
					mst_budget_category_list.LOCATION_ID,
					mst_budget_category_list.SUB_CATEGORY_ID,
					mst_budget_category_list.ITEM_ID,
					mst_budget_category_list.BUDGET_TYPE
					FROM `mst_budget_category_list`
					WHERE
					mst_budget_category_list.LOCATION_ID = '$budg_location'  
					AND mst_budget_category_list.SUB_CATEGORY_ID = '$sub_category'
					ORDER BY mst_budget_category_list.ITEM_ID DESC LIMIT 1";
		
		$result	= $this->db->$executeType($sql);
		$row=mysqli_fetch_array($result);
		if($row['ITEM_ID'] > 0)
			return 0;
		else if($row['SUB_CATEGORY_ID'] > 0)		
			return 1;
		else
			return '2';
	}
	
	public function get_price_for_mrn_item($locationId,$item,$executeType){
		
		$sql	="SELECT
					(ware_grndetails.dblGrnRate*fe1.dblBuying/fe2.dblBuying) as price 
					FROM
					ware_grnheader
					INNER JOIN trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
					INNER JOIN mst_locations ON mst_locations.intId = ware_grnheader.intCompanyId
					INNER JOIN mst_companies ON mst_locations.intCompanyId = mst_companies.intId
					
					INNER JOIN ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear
					LEFT JOIN mst_financeexchangerate AS fe1 ON trn_poheader.intCurrency = fe1.intCurrencyId AND date(ware_grnheader.datdate)= fe1.dtmDate AND mst_locations.intCompanyId = fe1.intCompanyId
					LEFT JOIN mst_financeexchangerate AS fe2 ON mst_companies.intBaseCurrencyId= fe2.intCurrencyId AND date(ware_grnheader.datdate)= fe2.dtmDate AND mst_locations.intCompanyId = fe2.intCompanyId
					WHERE
					ware_grnheader.intCompanyId = '$locationId' AND
					ware_grndetails.intItemId = '$item' order by ware_grnheader.datdate desc limit 1";
					$result	= $this->db->$executeType($sql);
					$row=mysqli_fetch_array($result);
					$price	= $row['price'];
		if($price <= 0 || $price =='' ){			
		$sql2	="SELECT
					(mst_item.dblLastPrice*fe1.dblBuying/fe2.dblBuying) as price 
					FROM
					mst_item 
					INNER JOIN mst_locations ON mst_locations.intId = $locationId
					INNER JOIN mst_companies ON mst_locations.intCompanyId = mst_companies.intId
					LEFT JOIN mst_financeexchangerate AS fe1 ON mst_item.intCurrency = fe1.intCurrencyId AND date(mst_item.dtmCreateDate)= fe1.dtmDate AND mst_locations.intCompanyId = fe1.intCompanyId
					LEFT JOIN mst_financeexchangerate AS fe2 ON mst_companies.intBaseCurrencyId= fe2.intCurrencyId AND date(mst_item.dtmCreateDate)= fe2.dtmDate AND mst_locations.intCompanyId = fe2.intCompanyId
					 WHERE 
					mst_item.intId = '$item' ";
					$result2	= $this->db->$executeType($sql2);
					$row2=mysqli_fetch_array($result2);
					$price	= $row2['price'];
		}
					
					return $price;
	}

	public function loadBudgetCategoryList($companyId,$locationId,$mainCat,$subCat,$description)
	{
		return $this->loadBudgetCategoryList_sql($companyId,$locationId,$mainCat,$subCat,$description);
	}
	
	public function getFinancePeriod($companyId,$date,$executeType)
	{
		return $this->getFinancePeriod_sql($companyId,$date,$executeType);
	}
	
public function getItemPrice($location,$itemId,$issueQty,$executeType)
{
 
	$result1	= $this->get_price_for_item($location,$itemId,$executeType);
	while($row = mysqli_fetch_array($result1))
	{
		
		if($issueQty<=0)
			continue;
			
		$stockQty	= $row["stockBal"];
		$price		= $row["price"];

		if($issueQty<$stockQty)
		{
			$itemPrice .= $issueQty * $price;
			$issueQty	= 0;
		}
		else
		{
			$itemPrice .= $issueQty * $price;
			$issueQty = $issueQty - $stockQty;
		}
	}
	return $itemPrice;
}
	
	public function get_price_for_item($location,$itemId,$executeType)
	{
		$sql = "SELECT
				SUM(ware_stocktransactions_bulk.dblQty) AS stockBal,
				ROUND((ware_stocktransactions_bulk.dblGRNRate*fe1.dblExcAvgRate/fe2.dblExcAvgRate),2) AS price ,
				ware_stocktransactions_bulk.intGRNNo,
				ware_stocktransactions_bulk.intGRNYear,
				ware_stocktransactions_bulk.dtGRNDate,
				ware_stocktransactions_bulk.dblGRNRate ,
				ware_stocktransactions_bulk.intCurrencyId
				FROM ware_stocktransactions_bulk 
				INNER JOIN mst_locations ON mst_locations.intId = ware_stocktransactions_bulk.intLocationId 
				INNER JOIN mst_companies ON mst_locations.intCompanyId = mst_companies.intId
				LEFT JOIN mst_financeexchangerate AS fe1 ON ware_stocktransactions_bulk.intCurrencyId = fe1.intCurrencyId AND 
				DATE(ware_stocktransactions_bulk.dtGRNDate)= fe1.dtmDate AND mst_locations.intCompanyId = fe1.intCompanyId
				LEFT JOIN mst_financeexchangerate AS fe2 ON mst_companies.intBaseCurrencyId= fe2.intCurrencyId AND 
				DATE(ware_stocktransactions_bulk.dtGRNDate)= fe2.dtmDate AND mst_locations.intCompanyId = fe2.intCompanyId
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$itemId' AND
				ware_stocktransactions_bulk.intLocationId =  '$location'
				GROUP BY
				ware_stocktransactions_bulk.intItemId,
				ware_stocktransactions_bulk.intGRNNo,
				ware_stocktransactions_bulk.intGRNYear, 
				ware_stocktransactions_bulk.dblGRNRate 
				HAVING stockBal>0
				ORDER BY
				ware_stocktransactions_bulk.dtGRNDate ASC ";
		
		$result	= $this->db->$executeType($sql);
		
		return $result;
		
	}
	public function checkBudgetApprovalDepWise($location,$departmentId,$financeYear,$executeType)
	{
		$checkStatus = true;
		
		$sql = "SELECT intId
				FROM mst_department
				WHERE intStatus = 1 ";
		
		if($departmentId!='')
			$sql.="AND intId = '$departmentId' ";	
		
		$result	= $this->db->$executeType($sql);
		while($row = mysqli_fetch_array($result))
		{
			$sql2 = " SELECT *
						FROM budget_plan_header
						WHERE LOCATION_ID = '$location' AND
						DEPARTMENT_ID = '".$row['intId']."' AND
						FINANCE_YEAR_ID = '$financeYear' AND
						STATUS = 1 ";
			
			$result2	= $this->db->$executeType($sql2);
			if(mysqli_num_rows($result2)<=0 && $checkStatus)
				$checkStatus = false;	
		}
		
		return $checkStatus;
	}
	public function load_save_to_array($results_details,$location,$response,$transType,$serialNo,$serialYear,$executeType)
	{
		return $this->save_to_array($results_details,$location,$response,$transType,$serialNo,$serialYear,$executeType);
	}
	
	private function loadBudgetCategoryList_sql($companyId,$locationId,$mainCat,$subCat,$description)
	{
		$sql = "SELECT
				  SC.intId        AS SUB_CATEGORY_ID,
				  SC.strName      AS SUB_CATEGORY_NAME,
				  BCL.ITEM_ID     AS ITEM_ID,
				  BCL.SERIAL_ID   AS SERIAL_ID,
				 IFNULL(I.strName ,'')      AS ITEM_NAME,
				  BCL.BUDGET_TYPE AS BUDGET_TYPE,
				  IFNULL(MU.strCode, '') 	  AS UNIT
				FROM mst_budget_category_list BCL
				  INNER JOIN mst_subcategory SC
					ON SC.intId = BCL.SUB_CATEGORY_ID
				  LEFT JOIN mst_item I
					ON I.intId = BCL.ITEM_ID
				  LEFT JOIN mst_units MU ON MU.intId=I.intUOM
				WHERE BCL.COMPANY_ID = '$companyId'
					AND BCL.LOCATION_ID = '$locationId'";
				if($mainCat != '')
					$sql .=" AND SC.intMainCategory = '$mainCat' ";
				if($subCat != '')
					$sql .=" AND SC.intId = '$subCat' ";
					if($description != '')
					$sql .=" AND I.strName LIKE  '%$description%'";	
				$sql	.= "ORDER BY SC.strName,I.strName";
				//die ($sql);
		return $this->db->RunQuery($sql);
	}
	
	private function getFinancePeriod_sql($companyId,$date,$executeType)
	{
		$sql = "SELECT
				  FAP.intId	AS FINANCE_ID
				FROM mst_financeaccountingperiod FAP
				  INNER JOIN mst_financeaccountingperiod_companies FAPC
					ON FAP.intId = FAPC.intPeriodId
				  INNER JOIN mst_companies C
					ON FAPC.intCompanyId = C.intId
				WHERE dtmStartingDate <= DATE('$date')
					AND dtmClosingDate >= DATE('$date')
					AND FAPC.intCompanyId = '$companyId'";
		$result = $this->db->$executeType($sql);
		return mysqli_fetch_array($result);
	}
	
	public function get_item_price($location,$item,$transType,$serialNo,$serialYear,$Qty,$executeType){
		$amount	=0;
		if($transType == 'RET_STORES'){
			 $sql = "SELECT
					Sum(ware_stocktransactions_bulk.dblQty) AS qty,
					ware_stocktransactions_bulk.intGRNNo,
					ware_stocktransactions_bulk.intGRNYear,
					ware_stocktransactions_bulk.dtGRNDate,
					ware_stocktransactions_bulk.dblGRNRate,
					ROUND((ware_stocktransactions_bulk.dblGRNRate*fe1.dblExcAvgRate/fe2.dblExcAvgRate),2) AS price  
					FROM ware_stocktransactions_bulk
					INNER JOIN mst_locations ON mst_locations.intId = ware_stocktransactions_bulk.intLocationId 
					INNER JOIN mst_companies ON mst_locations.intCompanyId = mst_companies.intId
					LEFT JOIN mst_financeexchangerate AS fe1 ON ware_stocktransactions_bulk.intCurrencyId = fe1.intCurrencyId AND 
					DATE(ware_stocktransactions_bulk.dtGRNDate)= fe1.dtmDate AND mst_locations.intCompanyId = fe1.intCompanyId
					LEFT JOIN mst_financeexchangerate AS fe2 ON mst_companies.intBaseCurrencyId= fe2.intCurrencyId AND 
					DATE(ware_stocktransactions_bulk.dtGRNDate)= fe2.dtmDate AND mst_locations.intCompanyId = fe2.intCompanyId
					WHERE
					ware_stocktransactions_bulk.intItemId =  '$item' AND
					ware_stocktransactions_bulk.strType =  'ISSUE' AND
					ware_stocktransactions_bulk.intDocumentNo =  '$serialNo' AND 
					ware_stocktransactions_bulk.intDocumntYear =  '$serialYear' AND 
					ware_stocktransactions_bulk.intLocationId =  '$location'
					GROUP BY
					ware_stocktransactions_bulk.intItemId,
					ware_stocktransactions_bulk.intGRNNo,
					ware_stocktransactions_bulk.intGRNYear, 
					ware_stocktransactions_bulk.dblGRNRate 
					ORDER BY
					ware_stocktransactions_bulk.dtGRNDate ASC";
		}
 		$resultG 	= $this->db->$executeType($sql);
		while($rowG=mysqli_fetch_array($resultG)){
			if(($Qty>0) && (($rowG['qty']*(-1))>0)){
				if($Qty	<=	$rowG['qty']*(-1)){
				$saveQty	=$Qty;
				$Qty		=0;
				}
				else if($Qty>$rowG['qty']*(-1)){
				$saveQty	=$rowG['qty']*(-1);
				$Qty		=$Qty-$saveQty;
				}
				$grnRate=$rowG['price'];	
				$currency=loadCurrency($grnNo,$grnYear,$item);
				$saveQty=round($saveQty,4);	
				if($saveQty>0){
					$amount +=$saveQty*$grnRate;
				}
			}
		}
		
		if($transType == 'SERVICE_PO'){
			$sql	=" SELECT
						($Qty*trn_podetails.dblUnitPrice*fe1.dblBuying/fe2.dblBuying) as amount  
						FROM
						trn_poheader  
						INNER JOIN trn_podetails ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear
						INNER JOIN mst_locations ON mst_locations.intId = trn_poheader.intCompany
						INNER JOIN mst_companies ON mst_locations.intCompanyId = mst_companies.intId
						LEFT JOIN mst_financeexchangerate AS fe1 ON trn_poheader.intCurrency = fe1.intCurrencyId AND date(trn_poheader.dtmPODate)= fe1.dtmDate AND mst_locations.intCompanyId = fe1.intCompanyId
						LEFT JOIN mst_financeexchangerate AS fe2 ON mst_companies.intBaseCurrencyId= fe2.intCurrencyId AND date(trn_poheader.dtmPODate)= fe2.dtmDate AND mst_locations.intCompanyId = fe2.intCompanyId
						WHERE 			
						trn_podetails.intPONo = '$serialNo' AND
						trn_podetails.intPOYear = '$serialYear' AND 
						trn_podetails.intItem = '$item' ";
			$result	= $this->db->$executeType($sql);
			$row	=mysqli_fetch_array($result);
			$amount = $row['amount'];
		}
		
		return $amount;
	
	}
	
	public function get_validate_budget($location,$department,$budg_year,$budg_month,$response,$transType,$obj_common,$executeType){
		return $this->validate_budget($location,$department,$budg_year,$budg_month,$response,$transType,$obj_common,$executeType);
	}
	
	private function save_to_array($results_details,$location,$response,$transType,$serialNo,$serialYear,$executeType){
		
				$arr_item_u		=$response['arr_item_u'];
				$arr_item_a		=$response['arr_item_a'];
				$arr_sub_cat_u	=$response['arr_sub_cat_u'];
				$arr_sub_cat_a	=$response['arr_sub_cat_a'];
			
				while($row_details	=mysqli_fetch_array($results_details)){
					
				$item_amount			= $this->get_item_price($location,$row_details['ITEM_ID'],$transType,$serialNo,$serialYear,$row_details['QTY'],$executeType);
				//$item_price				=2;
				$sub_cat_budget_flag	= $this->check_sub_category_budgeted_or_not($location,$row_details['SUB_CATEGORY_ID'],$executeType);
				
				 if($sub_cat_budget_flag	==0){	//item wise budget
					$budget_type			= $this->load_budget_type($location,$row_details['SUB_CATEGORY_ID'],$row_details['ITEM_ID'],$executeType);
					if($budget_type == 'U')	//unit budget
					$arr_item_u[$row_details['ITEM_ID']]	+= $row_details['QTY'];
					else if($budget_type == 'A')//amount budget
					$arr_item_a[$row_details['ITEM_ID']]	+= $item_amount;
					
				}
				else if($sub_cat_budget_flag	==1){//sub category wise budget
					$budget_type			= $this->load_budget_type($location,$row_details['SUB_CATEGORY_ID'],0,$executeType);
					if($budget_type == 'U')//unit budget
					$arr_sub_cat_u[$row_details['SUB_CATEGORY_ID']]	+= $row_details['QTY'];
					else if($budget_type == 'A')//amount budget
					$arr_sub_cat_a[$row_details['SUB_CATEGORY_ID']]	+= $item_amount;;
				}
			}
			$response['arr_item_u']		= $arr_item_u;
			$response['arr_item_a']		= $arr_item_a;
			$response['arr_sub_cat_u']	= $arr_sub_cat_u;
			$response['arr_sub_cat_a']	= $arr_sub_cat_a;
			
			return $response;
			
	}
	
	
	private function validate_budget($location,$department,$budg_year,$budg_month,$response,$transType,$obj_common,$executeType){
		
		$arr_item_u		= $response['arr_item_u'];
		$arr_item_a		= $response['arr_item_a'];
		$arr_sub_cat_u	= $response['arr_sub_cat_u'];
		$arr_sub_cat_a	= $response['arr_sub_cat_a'];
		
 		
		while (list($key_i, $value_i) = each($arr_item_u)) {//item ,unit budget
				$budget_bal	= $this->load_budget_balance_for_selected_trns_type($location,$department,$budg_year,'',$key_i,$budg_month,$transType,'U',$executeType);
				$budget_bal = $budget_bal['balUnits'];
				if($value_i > $budget_bal){
					$rollBackFlag	=1;
					$rollBackMsg	="There is no budget balance for this month for item '".$obj_common->get_item_name($key_i,$executeType)."' - Budget balance is :".$budget_bal." Units";
				}
		}
		while (list($key_i, $value_i) = each($arr_item_a)) {//item, amount budget
				$budget_bal	= $this->load_budget_balance_for_selected_trns_type($location,$department,$budg_year,'',$key_i,$budg_month,$transType,'A',$executeType);
				$budget_bal = $budget_bal['balAmount'];
 				if($value_i > $budget_bal){
					$rollBackFlag	=1;
					$rollBackMsg	="There is no budget balance for this month for item '".$obj_common->get_item_name($key_i,$executeType)."' - Budget balance is :".$budget_bal." Amount";
				}
		}
		while (list($key, $value) = each($arr_sub_cat_u)) {//sub category ,unit budget
				$budget_bal	= $this->load_budget_balance_for_selected_trns_type($location,$department,$budg_year,$key,'',$budg_month,$transType,'U',$executeType);
				$budget_bal = $budget_bal['balUnits'];
				if($value > $budget_bal){
					$rollBackFlag	=1;
					$rollBackMsg	="There is no budget balance for this month for sub category '".$obj_common->get_sub_category_name($key,$executeType)."' - Budget balance is :".$budget_bal." Units";
				}
		}
		while (list($key, $value) = each($arr_sub_cat_a)) {//sub category ,amount budget
				$budget_bal	= $this->load_budget_balance_for_selected_trns_type($location,$department,$budg_year,$key,'',$budg_month,$transType,'A',$executeType);
				$budget_bal = $budget_bal['balAmount'];
				if($value > $budget_bal){
					$rollBackFlag	=1;
					$rollBackMsg	="There is no budget balance for this month for sub category '".$obj_common->get_sub_category_name($key,$executeType)."' - Budget balance is :".$budget_bal." Amount";
				}
		}
		
		if($rollBackFlag == 1)
		{
			$data['type']	= 'fail';
			$data['msg']	= $rollBackMsg;
			$data['sql']		= $sql;
		}
		
		//print_r($data);
		return $data;
		
		
	}
	
	

}
?>
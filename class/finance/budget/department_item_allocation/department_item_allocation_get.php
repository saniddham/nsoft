<?php
class cls_department_item_allocation_get
{
	private $db;
		
	function __construct($db)
	{
		$this->db = $db;	
	}
	public function getItem($locationID,$departmentID,$executeType)
	{
		return $this->getItem_sql($locationID,$departmentID,$executeType);
	}
	private function getItem_sql($locationID,$departmentID,$executeType)
	{
		$sql	= "   SELECT
					  SC.intId        AS SUB_CATEGORY_ID,
					  SC.strName      AS SUB_CATEGORY_NAME,
					  BCL.ITEM_ID     AS ITEM_ID,
					  BCL.SERIAL_ID   AS SERIAL_ID,
					  IFNULL(I.strName ,'')      AS ITEM_NAME,
					  IFNULL(MU.strCode, '') 	  AS UNIT
					  FROM
						mst_budget_category_list_department_wise
						INNER JOIN mst_budget_category_list AS BCL ON BCL.SERIAL_ID = mst_budget_category_list_department_wise.SERIAL_ID
						INNER JOIN mst_subcategory AS SC ON SC.intId = BCL.SUB_CATEGORY_ID
						LEFT JOIN mst_item AS I ON I.intId = BCL.ITEM_ID
						LEFT JOIN mst_units AS MU ON MU.intId = I.intUOM
						
						WHERE
						BCL.LOCATION_ID = '$locationID' AND
						mst_budget_category_list_department_wise.DEPARTMENT_ID = '$departmentID'
						ORDER BY SC.strName,I.strName";
		$result = $this->db->$executeType($sql);
		return $result;
	}	
}
?>
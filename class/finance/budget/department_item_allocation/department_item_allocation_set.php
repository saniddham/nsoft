<?php
class cls_department_item_allocation_set
{
	private $db;
		
	function __construct($db)
	{
		$this->db = $db;	
	}
	public function deleteItem($depatmentID,$executeType)
	{
		return $this->deleteItems_result($depatmentID,$executeType);	
	}
	public function saveItem($serialNo,$depatmentID,$executeType)
	{
		return $this->saveItem_result($serialNo,$depatmentID,$executeType);
	}
	private function deleteItems_result($depatmentID,$executeType)
	{
		$sql	= " DELETE FROM mst_budget_category_list_department_wise 
					WHERE
					DEPARTMENT_ID = '$depatmentID'";
		
		$result	= $this->db->$executeType($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
		
	}
	private function saveItem_result($serialNo,$depatmentID,$executeType)
	{
		$sql	= " INSERT INTO mst_budget_category_list_department_wise 
					(DEPARTMENT_ID, 
					SERIAL_ID
					)
					VALUES
					('$depatmentID', 
					'$serialNo'
					)";
		$result	= $this->db->$executeType($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
}
?>
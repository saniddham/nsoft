<?php
class cls_budget_transfer_get
{
	
	private $db;
		
	function __construct($db)
	{
		$this->db = $db;	
	}
	
	public function get_header($serialNo,$serialYear,$executeType)
	{
		$sql	= $this->get_header_sql($serialNo,$serialYear);
		$row	= $this->get_row($sql,$executeType);
		return $row;
	}
	
	public function get_details_results($serialNo,$serialYear,$executeType)
	{
		$sql	= $this->get_details_sql($serialNo,$serialYear);
		$result	= $this->get_results($sql,$executeType);
		return $result;
	}
	
	public function get_Report_approval_details_result($serialNo,$serialYear,$executeType){
		$sql = $this->Load_Report_approval_details_sql($serialNo,$serialYear);
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	
	public function getMaxStatus($transferNo,$transferYear,$executeType)
	{
		$sql	= $this->getMaxStatus_sql($transferNo,$transferYear);
		$result = $this->db->$executeType($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row['maxStatus'];
	}
	
	private function get_header_sql($serialNo,$serialYear)
	{		
		$sql	= "SELECT 
					mst_companies.strName           AS COMPANY_NAME,
					mst_companies.intId             AS COMANY_ID,
					BTH.LOCATION_ID					AS LOCATION_ID,
					LO.strName 						AS LOCATION_NAME,
					BTH.DEPARTMENT_ID				AS DEPARTMENT_ID,
					D.strName  						AS DEPARTMENT_NAME,
					BTH.FINANCE_YEAR				AS FINANCE_YEAR_ID,
					BTH.`FROM_YEAR`					AS FROM_YEAR,
					BTH.FROM_MONTH					AS FROM_MONTH,
					BTH.TO_YEAR						AS TO_YEAR,
					BTH.TO_MONTH					AS TO_MONTH,
					CONCAT(FP.dtmStartingDate,' | ',FP.dtmClosingDate) AS CONCAT_PERIOD,
					BTH.STATUS						AS STATUS,
					BTH.APPROVE_LEVEL 				AS LEVELS,
					BTH.CREATED_BY,
					BTH.CREATED_DATE,
					BTH.MODIFY_BY,
					BTH.MODIFY_DATE,
					sys_users.strUserName          AS USER
					FROM 
					`budget_transfer_header` BTH
					INNER JOIN sys_users ON BTH.CREATED_BY = sys_users.intUserId
					INNER JOIN mst_locations LO ON LO.intId = BTH.LOCATION_ID
					INNER JOIN mst_companies ON LO.intCompanyId=mst_companies.intId
					INNER JOIN mst_department D ON D.intId = BTH.DEPARTMENT_ID
					INNER JOIN mst_financeaccountingperiod FP ON FP.intId = BTH.FINANCE_YEAR
					WHERE
					BTH.TRANSFER_NO = '$serialNo' AND
					BTH.TRANSFER_YEAR = '$serialYear'";
					
		return $sql;		
	}

	private function get_details_sql($serialNo,$serialYear){
		
		$sql	= "SELECT
					mst_maincategory.intId 						AS MAIN_CATEGORY_ID,
					mst_maincategory.strName 					AS MAIN_CATEGORY_NAME,
					budget_transfer_details.SUB_CATEGORY_ID		AS SUB_CATEGORY_ID,
					mst_subcategory.strName 					AS SUB_CATEGORY_NAME,
					budget_transfer_details.ITEM_ID				AS ITEM_ID,
					mst_item.strName 							AS ITEM_NAME,
					budget_transfer_details.TYPE				AS TYPE,
					IF(budget_transfer_details.TYPE=0,'TRANSFER',IF(budget_transfer_details.TYPE=1,'ADDITIONAL','')) AS TYPE_DESC,
					budget_transfer_details.AMOUNT				AS AMOUNT,
					budget_transfer_details.UNITS 				AS UNITS,
					MU.strCode 									AS UNIT,
					BCL.BUDGET_TYPE 							AS BUDGET_TYPE
					FROM `budget_transfer_details`
					INNER JOIN mst_budget_category_list BCL 
						ON budget_transfer_details.SUB_CATEGORY_ID = BCL.SUB_CATEGORY_ID
						AND budget_transfer_details.ITEM_ID = BCL.ITEM_ID
					INNER JOIN mst_subcategory ON budget_transfer_details.SUB_CATEGORY_ID = mst_subcategory.intId 
					INNER JOIN mst_maincategory ON mst_subcategory.intMainCategory = mst_maincategory.intId 
					LEFT JOIN mst_item ON budget_transfer_details.ITEM_ID = mst_item.intId 
					LEFT JOIN mst_units MU ON MU.intId=mst_item.intUOM
					WHERE
					budget_transfer_details.TRANSFER_NO = '$serialNo' AND
					budget_transfer_details.TRANSFER_YEAR = '$serialYear' ";
					
		return $sql;
		
	}
	
	private function Load_Report_approval_details_sql($serialNo,$serialYear){
		
	   	$sql = "SELECT
 				budget_transfer_approve_by.APPROVED_DATE AS dtApprovedDate ,
				sys_users.strUserName as UserName,
				budget_transfer_approve_by.APPROVED_LEVEL_NO as intApproveLevelNo
				FROM
				budget_transfer_approve_by
				Inner Join sys_users ON budget_transfer_approve_by.APPROVED_BY = sys_users.intUserId
				WHERE
				budget_transfer_approve_by.TRANSFER_NO =  '$serialNo' AND
				budget_transfer_approve_by.TRANSFER_YEAR =  '$serialYear' 
				 order by APPROVED_DATE asc";
				
 		return $sql;
	}

	private function get_results($sql,$executeType){
	
		$result	= $this->db->$executeType($sql);
		return $result;		
	}
	private function get_row($sql,$executeType){
	
		$result	= $this->db->$executeType($sql);
		$row=mysqli_fetch_array($result);
		return $row;		
	}
	private function getMaxStatus_sql($transferNo,$transferYear)
	{
		$sql = "SELECT 	MAX(STATUS) AS maxStatus
				FROM budget_transfer_approve_by 
				WHERE
				TRANSFER_NO = '$transferNo' AND
				TRANSFER_YEAR = '$transferYear' ";
		
		return $sql;
	}
	
	
}
?>
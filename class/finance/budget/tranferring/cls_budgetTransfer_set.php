<?php
class cls_budget_transfer_set
{
	private $db;
		
	function __construct($db)
	{
		$this->db = $db;	
	}
	public function insertDetail($executeType,$transferNo,$transferYear,$subCategoryID,$itemID,$amount,$units,$type)
	{
		return $this->insertDetail_sql($executeType,$transferNo,$transferYear,$subCategoryID,$itemID,$amount,$units,$type);	
	}
	
	public function insertHeader($executeType,$transferNo,$transferYear,$locationID,$departmentID,$financeYear,$fromYear,$fromMonth,$toYear,$toMonth,$status,$approveLevels,$userId)
	{
			return $this->insertHeader_sql($executeType,$transferNo,$transferYear,$locationID,$departmentID,$financeYear,$fromYear,$fromMonth,$toYear,$toMonth,$status,$approveLevels,$userId);
	}
	
	public function updateHeader($executeType,$transferNo,$transferYear,$locationID,$departmentID,$financeYear,$userId)
	{
		return $this->updateHeader_sql($executeType,$transferNo,$transferYear,$locationID,$departmentID,$financeYear,$userId);
	}
	
	public function deleteDetail($executeType,$transferNo,$transferYear)
	{
		return $this->deleteDetail_sql($executeType,$transferNo,$transferYear);	
	}
	
	public function insertApprovedBy($executeType,$transferNo,$transferYear,$levelNo,$userID,$status)
	{
		return $this->insertApprovedBy_sql($executeType,$transferNo,$transferYear,$levelNo,$userID,$status);
	}
	
	public function updateApprovedBy($executeType,$transferNo,$transferYear,$maxAppByStatus)
	{
		return $this->updateApprovedBy_sql($executeType,$transferNo,$transferYear,$maxAppByStatus);	
	}
	public function updateDetails($transferNo,$transferYear,$typeId,$subCatId,$itemId)
	{
		return $this->updateDetails_sql($transferNo,$transferYear,$typeId,$subCatId,$itemId);	
	}
	public function updateHeaderStatus($transferNo,$transferYear,$para)
	{
		return $this->updateHeaderStatus_sql($transferNo,$transferYear,$para);
	}
	public function approved_by_insert($transferNo,$transferYear,$userId,$approval)
	{
		return $this->approved_by_insert_sql($transferNo,$transferYear,$userId,$approval);
	}
	public function updateBugetPlanTrnData($location,$department,$financeYear,$subCatId,$itemId,$month,$amount,$unit)
	{
		return $this->updateBugetPlanTrnData_sql($location,$department,$financeYear,$subCatId,$itemId,$month,$amount,$unit);
	}
	public function updateApproveByStatus($transferNo,$transferYear,$maxStatus)
	{
		$sql 	= $this->updateApproveByStatus_sql($transferNo,$transferYear,$maxStatus);
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['msg']			= $this->db->errormsg;
			$data['sql']			= $sql;
		}
		return $data;	
	}
//BEGIN - private functions
	private function insertDetail_sql($executeType,$transferNo,$transferYear,$subCategoryID,$itemID,$amount,$units,$type)
	{
		$sql	=  "INSERT INTO budget_transfer_details 
					(TRANSFER_NO, 
					TRANSFER_YEAR, 
					SUB_CATEGORY_ID, 
					ITEM_ID, 
					AMOUNT, 
					UNITS, 
					TYPE
					)
					VALUES
					('$transferNo', 
					'$transferYear', 
					'$subCategoryID', 
					'$itemID', 
					'$amount', 
					'$units', 
					$type
					)";	
					
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= false;
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= true;
			$response['msg']	= "Saved successfully!";
			$response['sql']	= $sql;
		}
			return $response;
	}	
	
	private function insertHeader_sql($executeType,$transferNo,$transferYear,$locationID,$departmentID,$financeYear,$fromYear,$fromMonth,$toYear,$toMonth,$status,$approveLevels,$userId)
	{
		$sql	= " INSERT INTO budget_transfer_header 
					(TRANSFER_NO, 
					TRANSFER_YEAR, 
					LOCATION_ID, 
					DEPARTMENT_ID, 
					FINANCE_YEAR, 
					FROM_YEAR,
					FROM_MONTH,
					TO_YEAR,
					TO_MONTH,
					STATUS, 
					APPROVE_LEVEL, 
					CREATED_BY, 
					CREATED_DATE
					)
					VALUES
					('$transferNo', 
					'$transferYear', 
					'$locationID', 
					'$departmentID', 
					'$financeYear',
					'$fromYear',
					'$fromMonth',
					'$toYear',
					'$toMonth',
					'$status', 
					'$approveLevels', 
					'$userId', 
					NOW()
					)";	
		
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= false;
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= true;
			$response['msg']	= "Saved successfully!";
			$response['sql']	= $sql;
		}
			return $response;
	}
	
	private function updateHeader_sql($executeType,$transferNo,$transferYear,$locationID,$departmentID,$financeYear,$userId)
	{
		$sql	= " UPDATE budget_transfer_header
					SET LOCATION_ID = '$locationID',
					  DEPARTMENT_ID = '$departmentID',
					  FINANCE_YEAR = '$financeYear',
					  MODIFY_BY = '$userId',
					  MODIFY_DATE = NOW()
					WHERE TRANSFER_YEAR = '$transferYear'
						AND TRANSFER_NO = '$transferNo'";
					
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= false;
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= true;
			$response['msg']	= "Update successfully!";
			$response['sql']	= $sql;
		}
			return $response;
	}
	private function deleteDetail_sql($executeType,$transferNo,$transferYear)
	{
		$sql	= " DELETE
					FROM budget_transfer_details
					WHERE TRANSFER_NO = '$transferNo'
						AND TRANSFER_YEAR = '$transferYear'";			
		$result	= $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= false;
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else
		{
			$response['type']	= true;
			$response['msg']	= "Deleted successfully";
			$response['sql']	= $sql;	
		}
		return $response;
	}
	private function insertApprovedBy_sql($executeType,$transferNo,$transferYear,$levelNo,$userID,$status)
	{
		$sql = " INSERT INTO budget_transfer_approve_by 
				(TRANSFER_NO, 
				TRANSFER_YEAR, 
				APPROVED_LEVEL_NO, 
				APPROVED_BY, 
				APPROVED_DATE, 
				STATUS
				)
				VALUES
				('$transferNo', 
				'$transferYear', 
				'$levelNo', 
				'$userID', 
				NOW(), 
				0
				)"	;
				
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		}
		return $response;
	}
	
	private function updateApprovedBy_sql($executeType,$transferNo,$transferYear,$maxAppByStatus)
	{
 		 $sql = "	UPDATE budget_transfer_approve_by 
					SET
					STATUS = '$maxAppByStatus'
					
					WHERE
					TRANSFER_NO = 'TRANSFER_NO' AND TRANSFER_YEAR = 'TRANSFER_YEAR' AND STATUS='0'";
			
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		}
		return $response;

	}
	private function updateDetails_sql($transferNo,$transferYear,$typeId,$subCatId,$itemId)
	{
		$sql = "UPDATE budget_transfer_details 
				SET
				TYPE = '$typeId'
				WHERE
				TRANSFER_NO = '$transferNo' AND 
				TRANSFER_YEAR = '$transferYear' AND 
				SUB_CATEGORY_ID = '$subCatId' AND 
				ITEM_ID = '$itemId' ";
		
		$result = $this->db->runQuery2($sql);
		if(!$result)
		{
			$response['savedStatus']	= 'fail';
			$response['savedMassege']	= $this->db->errormsg;
			$response['error_sql']		= $sql;
		}
		return $response;
	}
	private function updateHeaderStatus_sql($transferNo,$transferYear,$para)
	{
		$sql = "UPDATE budget_transfer_header 
				SET
				STATUS = $para
				WHERE
				TRANSFER_NO = '$transferNo' AND 
				TRANSFER_YEAR = '$transferYear' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	function approved_by_insert_sql($transferNo,$transferYear,$userId,$approval)
	{
		$sql = "INSERT INTO budget_transfer_approve_by 
				(
				TRANSFER_NO, 
				TRANSFER_YEAR, 
				APPROVED_LEVEL_NO, 
				APPROVED_BY, 
				APPROVED_DATE, 
				STATUS
				)
				VALUES
				(
				'$transferNo', 
				'$transferYear', 
				'$approval', 
				'$userId', 
				NOW(), 
				0
				)";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function updateBugetPlanTrnData_sql($location,$department,$financeYear,$subCatId,$itemId,$month,$amount,$unit)
	{
		$sql = "UPDATE budget_plan_details 
				SET 
				TRANSFER_BUDGET = '$amount' , 
				TRANSFER_BUDGET_UNITS = '$unit'
				WHERE
				LOCATION_ID = '$location' AND 
				DEPARTMENT_ID = '$department' AND 
				FINANCE_YEAR_ID = '$financeYear' AND 
				SUB_CATEGORY_ID = '$subCatId' AND 
				MONTH = '$month' AND 
				ITEM_ID = '$itemId' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function updateApproveByStatus_sql($transferNo,$transferYear,$maxStatus)
	{
		$sql = "UPDATE budget_transfer_approve_by 
				SET
				STATUS = $maxStatus + 1
				WHERE
				TRANSFER_NO = '$transferNo' AND 
				TRANSFER_YEAR = '$transferYear' AND 
				STATUS = '0' ";
		
		return $sql;
	}
}
?>
<?php
class cls_month_end_set
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	
	public function Process($month,$year,$accPeriod,$session_companyId)
	{
		return $this->Process_sql($month,$year,$accPeriod,$session_companyId);
	}
	
	public function Revise($month,$year,$accPeriod)
	{
		return $this->Revise_sql($month,$year,$accPeriod);
	}
	
	public function ApprovedBy($month,$year,$accPeriod,$session_companyId,$session_userId,$approveStatus,$reviseStatus)
	{
		return $this->ApprovedBy_sql($month,$year,$accPeriod,$session_companyId,$session_userId,$approveStatus,$reviseStatus);
	}
	
	private function Process_sql($month,$year,$accPeriod,$session_companyId)
	{
		$sql = "INSERT INTO finance_month_end_process 
				(PERIOD_ID, 
				YEAR, 
				MONTH,
				COMPANY_ID,
				PROCESS_STATUS
				)
				VALUES
				('$accPeriod', 
				'$year', 
				'$month',
				$session_companyId,
				1);";
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$array['BooSaved']	= false;
			$array['ErrorMsg']	= $this->db->errormsg;
			$array['ErrorSql']	= $sql;
		}
		else
		{
			$array['BooSaved']	= true;
		}
		return $array;
	}
	
	private function Revise_sql($month,$year,$accPeriod)
	{
		$sql = "DELETE FROM finance_month_end_process 
				WHERE PERIOD_ID = '$accPeriod' 
				AND YEAR = '$year' 
				AND MONTH = '$month';";
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$array['BooSaved']	= false;
			$array['ErrorMsg']	= $this->db->errormsg;
			$array['ErrorSql']	= $sql;
		}
		else
		{
			$array['BooSaved']	= true;
		}
		return $array;
	}
	
	private function ApprovedBy_sql($month,$year,$accPeriod,$session_companyId,$session_userId,$approveStatus,$reviseStatus)
	{
		$sql = "INSERT INTO finance_month_end_process_approveby 
				(PERIOD_ID, 
				YEAR, 
				MONTH, 
				COMPANY_ID, 
				PROCESS_STATUS, 
				REVISE_STATUS, 
				PROCESS_BY,
				PROCESS_DATE)
				VALUES
				('$accPeriod', 
				'$year', 
				'$month', 
				'$session_companyId', 
				'$approveStatus', 
				'$reviseStatus', 
				$session_userId,
				NOW());";
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$array['BooSaved']	= false;
			$array['ErrorMsg']	= $this->db->errormsg;
			$array['ErrorSql']	= $sql;
		}
		else
		{
			$array['BooSaved']	= true;
		}
		return $array;		
	}
}
?>
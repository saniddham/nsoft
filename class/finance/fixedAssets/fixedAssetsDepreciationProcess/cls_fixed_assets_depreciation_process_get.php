<?php
class Cls_Fixed_Assets_Depreciation_Process_Get
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	public function validateSave($year,$month,$companyId,$executionType)
	{
		return $this->validateSave_sql($year,$month,$companyId,$executionType);
	}
	public function getDetails($lastDate,$executionType)
	{
		return $this->getDetails_sql($lastDate,$executionType);
	}
	public function getFixedAssetDetails($lastDate,$executionType)
	{
		return $this->getFixedAssetDetails_sql($lastDate,$executionType);
	}
	private function validateSave_sql($year,$month,$companyId,$executionType)
	{
		$sql = "SELECT 	*
				FROM finance_fixed_assets_depreciation_process 
				WHERE YEAR='$year' AND
				MONTH='$month' AND
				COMPANY_ID='$companyId' ";
		
		$result = $this->db->$executionType($sql);
		return $result;
	}
	private function getDetails_sql($lastDate,$executionType)
	{
		$sql = "SELECT
				ROUND(SUM(
				IF(FAR.NET_AMOUNT<((ITEM_PRICE*DEPRECIATE_RATE)/1200),
				(FAR.NET_AMOUNT),
				((ITEM_PRICE*DEPRECIATE_RATE)/1200)))
				,2)AS depAmount,
				MI.intSubCategory,
				COASCD.DEPRECIATION_DEBIT_ID AS debitAccount,
				COASCC.DEPRECIATION_CREDIT_ID AS creditAccount
				FROM fixed_assets_registry FAR
				INNER JOIN mst_item MI ON MI.intId=FAR.ITEM_ID
				LEFT JOIN finance_mst_chartofaccount_subcategory COASCD ON COASCD.SUBCATEGORY_ID=MI.intSubCategory
				LEFT JOIN finance_mst_chartofaccount_subcategory COASCC ON COASCC.SUBCATEGORY_ID=MI.intSubCategory
				WHERE FAR.GRN_DATE<='$lastDate' AND
				FAR.NET_AMOUNT > 0 AND
				STATUS = 1
				GROUP BY MI.intSubCategory ";
		
		$result = $this->db->$executionType($sql);
		return $result;
	}
	private function getFixedAssetDetails_sql($lastDate,$executionType)
	{
		$sql = "SELECT ID,
				ROUND(
				IF(NET_AMOUNT<((ITEM_PRICE*DEPRECIATE_RATE)/1200),
				(NET_AMOUNT),
				((ITEM_PRICE*DEPRECIATE_RATE)/1200))
				,2)AS depAmount
				FROM fixed_assets_registry
				WHERE GRN_DATE<='$lastDate' AND
				NET_AMOUNT > 0 AND
				STATUS = 1 ";
		
		$result = $this->db->$executionType($sql);
		return $result;
	}
}
?>
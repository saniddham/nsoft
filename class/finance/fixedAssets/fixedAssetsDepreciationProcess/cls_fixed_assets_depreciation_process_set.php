<?php
class Cls_Fixed_Assets_Depreciation_Process_Set
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	public function saveFinanceTransaction($fixedAssetDepNo,$fixedAssetDepYear,$glAccount,$amount,$docType,$transacType,$transacCat,$companyId,$locationId,$session_userId,$lastDate)
	{
		$sql = "INSERT INTO finance_transaction 
				(
				CHART_OF_ACCOUNT_ID, 
				AMOUNT, 
				DOCUMENT_NO, 
				DOCUMENT_YEAR, 
				DOCUMENT_TYPE, 
				TRANSACTION_TYPE, 
				TRANSACTION_CATEGORY, 
				LOCATION_ID, 
				COMPANY_ID, 
				LAST_MODIFIED_BY, 
				LAST_MODIFIED_DATE
				)
				VALUES
				(
				'$glAccount', 
				'$amount', 
				'$fixedAssetDepNo', 
				'$fixedAssetDepYear', 
				'$docType', 
				'$transacType', 
				'$transacCat', 
				'$locationId', 
				'$companyId', 
				'$session_userId', 
				'$lastDate'
				)";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	public function updateFixedAssetRegistry($serialNo,$depAmount)
	{
		$sql = "UPDATE fixed_assets_registry 
				SET
				ACCUMILATE_DEPRECIATE_AMOUNT = '$depAmount' , 
				NET_AMOUNT = NET_AMOUNT - $depAmount
				WHERE
				ID = '$serialNo' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	public function savedFixedAssetProcess($fixedAssetDepNo,$fixedAssetDepYear,$year,$month,$companyId,$session_userId)
	{
		$sql = "INSERT INTO finance_fixed_assets_depreciation_process 
				(
				DOCUMENT_NO, 
				DOCUMENT_YEAR, 
				YEAR, 
				MONTH, 
				COMPANY_ID, 
				USER_ID
				)
				VALUES
				(
				'$fixedAssetDepNo', 
				'$fixedAssetDepYear', 
				'$year', 
				'$month', 
				'$companyId', 
				'$session_userId'
				)";
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
}
?>
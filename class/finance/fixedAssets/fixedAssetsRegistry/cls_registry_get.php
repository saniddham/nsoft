<?php
class Cls_Registry_Get
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	public function loadDetailData_grn_fixed($grnNo,$grnYear,$supplier,$item,$serialNo,$assetCode,$location,$department,$subDepartment,$status,$executionType)
	{
		return $this->loadDetailData_grn_fixed_sql($grnNo,$grnYear,$supplier,$item,$serialNo,$assetCode,$location,$department,$subDepartment,$status,$executionType);
	}
	public function loadDetailData_saved_in_grn($grnNo,$grnYear,$supplier,$item,$serialNo,$assetCode,$location,$department,$subDepartment,$status,$executionType)
	{
		return $this->loadDetailData_saved_in_grn_sql($grnNo,$grnYear,$supplier,$item,$serialNo,$assetCode,$location,$department,$subDepartment,$status,$executionType);
	}
	
	
	public function loadSupplier()
	{
		return $this->loadSupplier_sql();
	}
	public function loadItem()
	{
		return $this->loadItem_sql();
	}
	public function loadSubDepartment($departmentId)
	{
		return $this->loadSubDepartment_sql($departmentId);
	}
	public function loadGRNNo()
	{
		return $this->loadGRNNo_sql();
	}
	private function loadDetailData_grn_fixed_sql($grnNo,$grnYear,$supplier,$item,$serialNo,$assetCode,$location,$department,$subDepartment,$status,$executionType)
	{
		
		$wSql = '';
		
		if($grnNo!='')
			$wSql.="AND GRN_NO='$grnNo' ";
		if($grnYear!='')
			$wSql.="AND GRN_YEAR='$grnYear' ";
		if($supplier!='')
			$wSql.="AND SUPPLIER_ID='$supplier' ";
		if($item!='')
			$wSql.="AND ITEM_ID='$item' ";
		if($serialNo!='')
			$wSql.="AND SERIAL_NO LIKE '%$serialNo%' ";
		if($assetCode!='')
			$wSql.="AND ASSET_CODE LIKE '%$assetCode%' ";
		if($location!='')
			$wSql.="AND LOCATION_ID='$location' ";
		if($department!='')
			$wSql.="AND DEPARTMENT_ID='$department' ";
		if($subDepartment!='')
			$wSql.="AND SUB_DEPARTMENT_ID='$subDepartment' ";
		if($status!='')
			$wSql.="AND STATUS='$status' ";
		
		$sql = "SELECT ID, 
				GRN_NO, 
				GRN_YEAR, 
				SUPPLIER_ID, 
				ITEM_ID, 
				SERIAL_NO, 
				WARRANTY_MONTHS, 
				WARRANTY_END_DATE, 
				ASSET_CODE, 
				LOCATION_ID, 
				DEPARTMENT_ID, 
				SUB_DEPARTMENT_ID, 
				PURCHASE_DATE, 
				GRN_DATE, 
				CURRENCY, 
				ITEM_PRICE, 
				DEPRECIATE_RATE, 
				ACCUMILATE_DEPRECIATE_AMOUNT, 
				NET_AMOUNT, 
				MANUAL_FLAG,
				STATUS
				FROM 
				ware_grn_fixed_assets_registry 
				WHERE 1=1 
				$wSql
				ORDER BY ID ";
				//echo  $sql;
		
		$result = $this->db->$executionType($sql);
		return $result;
	}
	private function loadSupplier_sql()
	{
		$sql = "SELECT intId,strName
				FROM mst_supplier
				WHERE intTypeId IN (SELECT intId FROM mst_typeofsupplier WHERE booFixedAssets='1' AND intStatus='1') AND
				intStatus='1'
				ORDER BY strName";
		
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	private function loadItem_sql()
	{
		$sql = "SELECT intId,strName
				FROM mst_item
				WHERE intMainCategory='5' AND
				intStatus='1'
				ORDER BY strName";
		
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	private function loadSubDepartment_sql($departmentId)
	{
		$sql = "SELECT SUB_DEPT_ID,NAME
				FROM mst_department_sub
				WHERE DEPT_ID='$departmentId' AND
				STATUS='1' 
				ORDER BY NAME";
		
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	private function loadGRNNo_sql()
	{
		$sql = "SELECT DISTINCT GRN_NO,GRN_YEAR
				FROM ware_grn_fixed_assets_registry
				ORDER BY GRN_YEAR,GRN_NO ";
		
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	
	private function loadDetailData_saved_in_grn_sql($grnNo,$grnYear,$supplier,$item,$serialNo,$assetCode,$location,$department,$subDepartment,$status,$executionType)
	{
		
		$wSql = '';
		
		if($grnNo!='')
			$wSql.="AND GRN_NO='$grnNo' ";
		if($grnYear!='')
			$wSql.="AND GRN_YEAR='$grnYear' ";
		if($supplier!='')
			$wSql.="AND SUPPLIER_ID='$supplier' ";
		if($item!='')
			$wSql.="AND ITEM_ID='$item' ";
		if($serialNo!='')
			$wSql.="AND SERIAL_NO LIKE '%$serialNo%' ";
		if($assetCode!='')
			$wSql.="AND ASSET_CODE LIKE '%$assetCode%' ";
		if($location!='')
			$wSql.="AND LOCATION_ID='$location' ";
		if($department!='')
			$wSql.="AND DEPARTMENT_ID='$department' ";
		if($subDepartment!='')
			$wSql.="AND SUB_DEPARTMENT_ID='$subDepartment' ";
		if($status!='')
			$wSql.="AND STATUS='$status' ";
		
		$sql = "SELECT ID, 
				GRN_NO, 
				GRN_YEAR, 
				SUPPLIER_ID, 
				ITEM_ID, 
				SERIAL_NO, 
				WARRANTY_MONTHS, 
				WARRANTY_END_DATE, 
				ASSET_CODE, 
				LOCATION_ID, 
				DEPARTMENT_ID, 
				SUB_DEPARTMENT_ID, 
				PURCHASE_DATE, 
				GRN_DATE, 
				CURRENCY,
				ITEM_PRICE, 
				DEPRECIATE_RATE, 
				ACCUMILATE_DEPRECIATE_AMOUNT, 
				NET_AMOUNT, 
				MANUAL_FLAG,
				STATUS
				FROM 
				ware_grn_fixed_assets_registry 
				WHERE 1=1 
				$wSql
				ORDER BY ID ";
				//echo  $sql;
		
		$result = $this->db->$executionType($sql);
		return $result;
	}
	
	
	
}
?>
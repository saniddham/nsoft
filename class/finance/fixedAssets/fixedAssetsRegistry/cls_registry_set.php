<?php
  
$session_userId		= $_SESSION["userId"];
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];
 
class Cls_Registry_Set
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	
	public function SaveDetails_grn_fixed($grnNo,$grnYear,$supplierId,$itemId,$serialNo,$purchaseDate,$grnDate,$currency,$price,$manualFlag,$executionType,$assetsCode,$warrantyMonth,$warrantyEndDate,$location,$department,$subDepartment,$depreciateRate,$status)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		  $sql	= "INSERT INTO ware_grn_fixed_assets_registry 
					( 
					GRN_NO, 
					GRN_YEAR, 
					SUPPLIER_ID, 
					ITEM_ID, 
					SERIAL_NO, 
					WARRANTY_MONTHS, 
					WARRANTY_END_DATE, 
					ASSET_CODE, 
					LOCATION_ID, 
					DEPARTMENT_ID, 
					SUB_DEPARTMENT_ID, 
					PURCHASE_DATE, 
					GRN_DATE, 
					CURRENCY,
					ITEM_PRICE, 
					DEPRECIATE_RATE, 
					NET_AMOUNT, 
					MANUAL_FLAG,
					STATUS
					)
					VALUES
					( 
					'$grnNo', 
					'$grnYear', 
					$supplierId, 
					$itemId, 
					'$serialNo', 
					$warrantyMonth, 
					$warrantyEndDate, 
					'$assetsCode', 
					$location, 
					$department, 
					$subDepartment, 
					$purchaseDate, 
					$grnDate, 
					$currency,
					'$price', 
					'$depreciateRate', 
					'$price', 
					'$manualFlag',
					'$status'
					) ";
			$result = $this->db->$executionType($sql);
			if(!$result)
			{
				$data['savedStatus']	= 'fail';
				$data['savedMassege']	= $this->db->errormsg;
				$data['error_sql']		= $sql;
			}
			else{
				$data['savedStatus']	= 'pass';
				$data['savedMassege']	= $this->db->errormsg;
				$data['error_sql']		= $sql;				
				$data['serial_no']		= $this->db->insertId;
}
			return $data;
	}
	
	public function UpdateDetails_grn_fixed($grnNo,$grnYear,$supplierId,$itemId,$serialNo,$purchaseDate,$grnDate,$currency,$price,$manualFlag,$executionType)
	{
		global $session_userId;
		global $session_locationId;
		global $session_companyId;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "UPDATE ware_grn_fixed_assets_registry 
				SET 
  				SUPPLIER_ID='$supplierId',
 				ITEM_ID='$itemId', 
				SERIAL_NO='$serialNo', 
 				PURCHASE_DATE='$purchaseDate', 
 				GRN_DATE='$grnDate', 
 				CURRENCY='$currency', 
				ITEM_PRICE='$price', 
  				MANUAL_FLAG='$manualFlag ,
				NET_AMOUNT='$price'  
				WHERE
				ware_grn_fixed_assets_registry.GRN_NO = '$grnNo' AND
				ware_grn_fixed_assets_registry.GRN_YEAR = '$grnYear'";
		$result = $this->db->$executionType($sql);
			if(!$result){
				$data['savedStatus']	= 'fail';
				$data['savedMassege']	= $this->db->errormsg;
				$data['error_sql']			= $sqlI;
			}
			return $data;
	}
	
	public function DeleteDetails_grn_fixed($grnNo,$grnYear,$itemId,$executionType)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql	= "DELETE 
					FROM ware_grn_fixed_assets_registry 
				WHERE
				ware_grn_fixed_assets_registry.GRN_NO = '$grnNo' AND
				ware_grn_fixed_assets_registry.GRN_YEAR = '$grnYear' AND 
				ITEM_ID = '$itemId'";
		$result = $this->db->$executionType($sql);
			if(!$result){
				$data['savedStatus']	= 'fail';
				$data['savedMassege']	= $this->db->errormsg;
				$data['error_sql']			= $sqlI;
			}
			return $data;
	}
	public function DeleteNotInListDetails_grn_fixed($grnNo,$grnYear,$itemList,$executionType)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
			$sql	= "DELETE 
					FROM ware_grn_fixed_assets_registry 
				WHERE
				ware_grn_fixed_assets_registry.GRN_NO = '$grnNo' AND
				ware_grn_fixed_assets_registry.GRN_YEAR = '$grnYear' AND 
				ITEM_ID NOT IN ($itemList)";
		$result = $this->db->$executionType($sql);
			if(!$result){
				$data['savedStatus']	= 'fail';
				$data['savedMassege']	= $this->db->errormsg;
				$data['error_sql']			= $sqlI;
			}
			return $data;
	}
	
	public function deleteItem_grn_fixed($rowId,$executionType)
	{
		$sql = "UPDATE ware_grn_fixed_assets_registry 
				SET
				STATUS = '-2'
				WHERE
				ID = '$rowId' ";
		
		$result = $this->db->$executionType($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMasseged']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}	
	public function updateRegistryDetails_grn_fixed($rowId,$supplier,$itemId,$serialNo,$grnDate,$assetsCode,$warrantyMonth,$warrantyEndDate,$location,$department,$subDepartment,$currency,$itemPrice,$depreciateRate,$executionType)
	{
		$sql = "UPDATE ware_grn_fixed_assets_registry 
				SET
				SUPPLIER_ID = $supplier , 
				ITEM_ID = $itemId , 
				SERIAL_NO = '$serialNo' , 
				GRN_DATE = $grnDate,
				WARRANTY_MONTHS = $warrantyMonth , 
				WARRANTY_END_DATE = $warrantyEndDate , 
				ASSET_CODE = '$assetsCode' , 
				LOCATION_ID = $location , 
				DEPARTMENT_ID = $department , 
				SUB_DEPARTMENT_ID = $subDepartment ,  
				CURRENCY = $currency ,  
				ITEM_PRICE = '$itemPrice' , 
				DEPRECIATE_RATE = '$depreciateRate' , 
				NET_AMOUNT = '$itemPrice'
				WHERE
				ID = '$rowId' ";
		
		$result = $this->db->$executionType($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	
		public function DeleteDetails_orginal_fixed($grnNo,$grnYear,$itemId,$executionType)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql	= "DELETE 
					FROM fixed_assets_registry 
				WHERE
				fixed_assets_registry.GRN_NO = '$grnNo' AND
				fixed_assets_registry.GRN_YEAR = '$grnYear' AND 
				ITEM_ID = '$itemId'";
		$result = $this->db->$executionType($sql);
			if(!$result){
				$data['savedStatus']	= 'fail';
				$data['savedMassege']	= $this->db->errormsg;
				$data['error_sql']			= $sqlI;
			}
			return $data;
	}
	
	public function SaveDetails_orginal_fixed($grnNo,$grnYear,$supplierId,$itemId,$serialNo,$purchaseDate,$grnDate,$currency,$price,$manualFlag,$executionType,$assetsCode,$warrantyMonth,$warrantyEndDate,$location,$department,$subDepartment,$depreciateRate,$status)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		  $sql	= "INSERT INTO fixed_assets_registry 
					( 
					GRN_NO, 
					GRN_YEAR, 
					SUPPLIER_ID, 
					ITEM_ID, 
					SERIAL_NO, 
					WARRANTY_MONTHS, 
					WARRANTY_END_DATE, 
					ASSET_CODE, 
					LOCATION_ID, 
					DEPARTMENT_ID, 
					SUB_DEPARTMENT_ID, 
					PURCHASE_DATE, 
					GRN_DATE,
					CURRENCY, 
					ITEM_PRICE, 
					DEPRECIATE_RATE, 
					NET_AMOUNT, 
					MANUAL_FLAG,
					STATUS
					)
					VALUES
					( 
					'$grnNo', 
					'$grnYear', 
					$supplierId, 
 					$itemId, 
					'$serialNo', 
					$warrantyMonth, 
					$warrantyEndDate, 
					'$assetsCode', 
					$location, 
					$department, 
					$subDepartment, 
					'$purchaseDate', 
					'$grnDate', 
					$currency, 
					'$price', 
					'$depreciateRate', 
					'$price', 
					'$manualFlag',
					'$status'
					) ";
					//echo $sql;
			$result = $this->db->$executionType($sql);
			if(!$result)
			{
				$data['savedStatus']	= 'fail';
				$data['savedMassege']	= $this->db->errormsg;
				$data['error_sql']		= $sql;
 			}
			else{
				$data['savedStatus']	= 'pass';
				$data['savedMassege']	= $this->db->errormsg;
				$data['error_sql']		= $sql;				
				$data['serial_no']		= $this->db->insertId;
}
			return $data;
	}
	
	public function SaveDetails_depriciation_fixed($fixed_asset_id,$grnNo,$grnYear,$grnDate,$itemId,$serialNo,$currency,$price,$depreciateRate,$amount_before_process,$depriciated_amount,$netAmount,$type,$executionType){
		
		  $sql	= "INSERT INTO `fixed_assets_process_log` 
		 			(
					`ID_FIXED_ASSET`, 
					`GRN_NO`, 
					`GRN_YEAR`, 
					`GRN_DATE`, 
					`ITEM_ID`, 
					`SERIAL_NO`, 
					`CURRENCY`, 
					`ITEM_PRICE`, 
					`DEPRECIATE_RATE`, 
					`NET_AMOUNT_BEFORE_PROCESS`, 
					`DEPRECIATED_AMOUNT`, 
					`NET_AMOUNT`,
					`TYPE`)
					VALUES
					( 
					'$fixed_asset_id', 
					'$grnNo', 
					$grnYear, 
					'$grnDate',
					$itemId, 
					'$serialNo', 
					$currency, 
					$price, 
					'$depreciateRate', 
					$price, 
					$depriciated_amount, 
					$netAmount,
					'$type') ";
				//	echo $sql;
			$result = $this->db->$executionType($sql);
			if(!$result)
			{
				$data['savedStatus']	= 'fail';
				$data['savedMassege']	= $this->db->errormsg;
				$data['error_sql']		= $sql;
 			}
			else{
				$data['savedStatus']	= 'pass';
				$data['savedMassege']	= $this->db->errormsg;
				$data['error_sql']		= $sql;
				$data['serial_no']		= $this->db->insertId;
			}
			return $data;
		
		
	}

}
?>
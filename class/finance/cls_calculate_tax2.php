<?php
class Cls_Calculate_Tax
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	
	public function CalculateTax($taxId,$value)
	{	if($taxId=='NULL')	
			$responce["TaxValue"]	= 0;
		else
			$responce = $this->CalculateTax_process($taxId,$value);
		return json_encode($responce);
	}
	
	private function CalculateTax_process1($taxId,$value)
	{
		$taxValue = 0;
		
		$sql = "SELECT strProcess FROM mst_financetaxgroup WHERE intId = $taxId";
		$result = $this->db->RunQuery($sql);
		while($row = mysqli_fetch_array($result))
		{
			$process = $row["strProcess"];
		}
		
		$process_array	= explode('/',$process);
		
		if(count($process_array)<=1)
		{
			$taxValue	= ($value * $this->GetRate($process_array[0])) / 100;
		}
		else
		{
			if($process_array[1] == 'Inclusive')
			{
				$firstVal = ($value * $this->GetRate($process_array[0]))/100;
				$taxValue = $firstVal + ((($value + $firstVal) * $this->GetRate($process_array[2])) / 100);
			}
			else if($process_array[1] == 'Exclusive')
			{
				$taxValue = ($value*($this->GetRate($process_array[0]) + $this->GetRate($process_array[2])))/100;
			}
		}
		
		$responce["TaxValue"]	= round($taxValue,4);
		return $responce;
	}
	
	private function CalculateTax_process($taxId,$value)
	{
		$sql 		= "SELECT strFormula AS FORMUAL FROM mst_financetaxgroup WHERE intId = $taxId";
		$result 	= $this->db->RunQuery($sql);
		$row 		= mysqli_fetch_array($result);
		$process 	= $row["FORMUAL"];		
		$value		= $value;
		$process1	= str_replace("VALUE",$value,$process);
		
		$sql 		= "SELECT strFormulaCode AS CODE,dblRate AS RATE FROM mst_financetaxisolated";
		$result 	= $this->db->RunQuery($sql);
		while($row  = mysqli_fetch_array($result))
		{
			$process1	= preg_replace("/\b".$row["CODE"]."\b/",$row["RATE"],$process1);
		}
		
		//echo($process1);
		$responce["TaxValue"]	= $this->Calculate_string($process1);
		return $responce;
	}
	
	private function GetRate($taxId)
	{
		$sql = "SELECT dblRate AS RATE FROM mst_financetaxisolated WHERE intId = $taxId";
		$result = $this->db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return $row["RATE"];
	}
	
	private function Calculate_string($mathString)
	{
		$mathString = trim($mathString);     // trim white spaces
		$mathString = preg_match ('[^0-9\+-\*\/\(\) ]', '', $mathString);    // remove any non-numbers chars; exception for math operators
	
		$compute = create_function("", "return (" . $mathString . ");" );
		return 0 + $compute();
	}
}
?>
<?php
require_once $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
require_once $_SESSION['ROOT_PATH']."class/cls_commonErrorHandeling_get.php";
//ini_set('display_errors', 1); 
 
$obj_commonErr		= new cls_commonErrorHandeling_get($db);
$obj_common			= new cls_commonFunctions_get($db);
$session_userId		= $_SESSION["userId"];
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];
$progrmCode			= 'P0748';

class Cls_bank_payment_Get
{
	private $db;
 	
	function __construct($db)
	{
		$this->db = $db;
 	}

//BEGIN - PUBLIC FUNCTIONS {	
	public function get_header_array($serialYear,$serialNo)
	{
		
		$sql = $this->load_header_sql($serialYear,$serialNo);
		$result = $this->db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return $row;
	}
	public function get_header_array2($serialYear,$serialNo)
	{
 		$sql = $this->load_header_sql($serialYear,$serialNo);
		$result = $this->db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		return $row;
	}
	public function get_Report_header_array($serialYear,$serialNo)
	{
 		$sql = $this->LoadReportHeaderDetails_sql($serialYear,$serialNo);
		$result = $this->db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return $row;
	}
	public function get_Report_details_result($serialYear,$serialNo)
	{
		$sql = $this->LoadReportGridDetails_sql($serialYear,$serialNo);
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	public function get_Report_approval_details_result($serialYear,$serialNo)
	{
		$sql = $this->Load_Report_approval_details_sql($serialYear,$serialNo);
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	public function get_details_result($serialYear,$serialNo)
	{
		$sql = $this->LoadGridDetails_sql($serialYear,$serialNo);
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	public function get_details_result_2($serialYear,$serialNo)
	{
		$sql = $this->LoadGridDetails_sql($serialYear,$serialNo);
		$result = $this->db->RunQuery2($sql);
		return $result;
	}
	public function get_tot_amount($serialYear,$serialNo)
	{
		$sql = $this->Load_tot_amount_sql($serialYear,$serialNo);
		$result = $this->db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return $row['amount'];
	}
	public function ValidateBeforeSave($serialYear,$serialNo)
	{ 
		global $session_userId;
		global $obj_commonErr;	
		global $progrmCode;
		$this->db->begin();
   		$header_arr			= $this->get_header_array2($serialYear,$serialNo);
 		$permision_arr		= $obj_commonErr->get_permision_withApproval_save($header_arr['STATUS'],$header_arr['LEVELS'],$session_userId,$progrmCode,'RunQuery2');
		if($permision_arr['type'] =='fail'){
			$rollBack			=	1;
			$response['msg'] 	=	$permision_arr['msg'];	
			$response['type'] 	=	'fail';
 			return $response;
		}
		$this->db->commit();
		
 	}
	
	public function ValidateBeforeApprove($serialYear,$serialNo)
	{ 
		global $session_userId;
		global $obj_commonErr;	
		global $progrmCode;
		$this->db->begin();
   		$header_arr			= $this->get_header_array2($serialYear,$serialNo);
 		$permision_arr		= $obj_commonErr->get_permision_withApproval_confirm($header_arr['STATUS'],$header_arr['LEVELS'],$session_userId,$progrmCode,'RunQuery2');
		if($permision_arr['type'] =='fail'){
			$rollBack			=	1;
			$response['msg'] 	=	$permision_arr['msg'];	
			$response['type'] 	=	'fail';
 			return $response;
		}
		$this->db->commit();
		
	}

	public function ValidateBeforeReject($serialYear,$serialNo)
	{ 
		global $session_userId;
		global $obj_commonErr;	
		global $progrmCode;
		$this->db->begin();
   		$header_arr			= $this->get_header_array2($serialYear,$serialNo);
 		$permision_arr		= $obj_commonErr->get_permision_withApproval_reject($header_arr['STATUS'],$header_arr['LEVELS'],$session_userId,$progrmCode,'RunQuery2');
		if($permision_arr['type'] =='fail'){
			$rollBack			=	1;
			$response['msg'] 	=	$permision_arr['msg'];	
			$response['type'] 	=	'fail';
 			return $response;
		}
		$this->db->commit();
		
	}
	public function ValidateBeforeCancel($serialYear,$serialNo)
	{ 
		global $session_userId;
		global $obj_commonErr;	
		global $progrmCode;
		$this->db->begin();
   		$header_arr			= $this->get_header_array2($serialYear,$serialNo);
 		$permision_arr		= $obj_commonErr->get_permision_withApproval_cancel($header_arr['STATUS'],$header_arr['LEVELS'],$session_userId,$progrmCode,'RunQuery2');
		if($permision_arr['type'] =='fail'){
			$rollBack			=	1;
			$response['msg'] 	=	$permision_arr['msg'];	
			$response['type'] 	=	'fail';
 			return $response;
		}
		$this->db->commit();
		
	}
 	public function getTotAmmount2($serialYear,$serialNo,$type)
	{
	  	$sql = $this->getTotAmmount_sql($serialYear,$serialNo,$type);
 		$result = $this->db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		return $row['amount'];
 	}
	public function getHeaderData_pdf($paymentNo,$paymentYear,$executionType)
	{
		return $this->HeaderData_pdf($paymentNo,$paymentYear,$executionType);
	}
	public function getDetailData_pdf($paymentNo,$paymentYear,$executionType)
	{
		return $this->DetailData_pdf($paymentNo,$paymentYear,$executionType);
	}
	
 //END	- PUBLIC FUNCTIONS }

//BEGIN - PRIVATE FUNCTIONS {	
	private function load_header_sql($serialYear,$serialNo)
	{
		global $session_companyId;
 		
		 $sql = "SELECT
				finance_bank_payment_header.BANK_PAYMENT_NO,
				finance_bank_payment_header.BANK_PAYMENT_YEAR,
				finance_bank_payment_header.BANK_CHART_OF_ACCOUNT_ID,
				finance_bank_payment_header.CURRENCY_ID,
				finance_bank_payment_header.PAYMENT_METHOD,
				finance_bank_payment_header.BANK_REFERENCE_NO,
				finance_bank_payment_header.REMARKS,
				finance_bank_payment_header.PAYMENT_DATE,
				finance_bank_payment_header.`STATUS` AS STATUS,
				finance_bank_payment_header.APPROVE_LEVEL AS LEVELS,
				finance_bank_payment_header.COMPANY_ID,
				finance_bank_payment_header.LOCATION_ID,
				finance_bank_payment_header.CREATED_BY,
				finance_bank_payment_header.CREATED_DATE,
				finance_bank_payment_header.MODIFIED_BY,
				finance_bank_payment_header.MODIFIED_DATE
				FROM `finance_bank_payment_header`
				WHERE
				finance_bank_payment_header.BANK_PAYMENT_NO = '$serialNo' AND
				finance_bank_payment_header.BANK_PAYMENT_YEAR = '$serialYear'";
 		return $sql;
 	}
	
	private function LoadGridDetails_sql($serialYear,$serialNo)
	{
		global $session_companyId;
		
		$sql = "SELECT
				finance_bank_payment_details.BANK_PAYMENT_NO,
				finance_bank_payment_details.BANK_PAYMENT_YEAR,
				finance_bank_payment_details.CHART_OF_ACCOUNT_ID,
				finance_bank_payment_details.TAX_ID,
				finance_bank_payment_details.AMOUNT,
				finance_bank_payment_details.PAY_TO,
				finance_bank_payment_details.COST_CENTER_ID,
				finance_bank_payment_details.REMARKS, 
				finance_bank_payment_details.SETTELEMENT_NO,
				finance_bank_payment_details.SETTELEMENT_YEAR,
				finance_bank_payment_details.SETTELEMENT_ORDER 
				FROM `finance_bank_payment_details`
				WHERE
				finance_bank_payment_details.BANK_PAYMENT_NO = '$serialNo' AND
				finance_bank_payment_details.BANK_PAYMENT_YEAR = '$serialYear' 
				";
 		return $sql;
	}
	private function LoadReportHeaderDetails_sql($serialYear,$serialNo)
	{
		global $session_companyId;
 		
	  	$sql = "SELECT 
				finance_bank_payment_header.BANK_PAYMENT_NO		AS SERIAL_NO,
				finance_bank_payment_header.COMPANY_ID			AS COMPANY_ID,
				finance_bank_payment_header.PAYMENT_DATE		AS SERIAL_DATE,
				mst_financecurrency.strCode AS CURRENCY,
				finance_mst_chartofaccount.CHART_OF_ACCOUNT_NAME,
				finance_bank_payment_header.BANK_PAYMENT_NO,
				finance_bank_payment_header.BANK_PAYMENT_YEAR,
				finance_bank_payment_header.BANK_CHART_OF_ACCOUNT_ID,
				finance_bank_payment_header.CURRENCY_ID,
				finance_bank_payment_header.PAYMENT_METHOD,
				finance_bank_payment_header.BANK_REFERENCE_NO,
				finance_bank_payment_header.REMARKS,
				finance_bank_payment_header.PAYMENT_DATE,
				finance_bank_payment_header.`STATUS`,
				finance_bank_payment_header.APPROVE_LEVEL as LEVELS,
				finance_bank_payment_header.COMPANY_ID,
				finance_bank_payment_header.LOCATION_ID,
				finance_bank_payment_header.CREATED_BY,
				finance_bank_payment_header.CREATED_DATE,
				finance_bank_payment_header.MODIFIED_BY,
				finance_bank_payment_header.MODIFIED_DATE,
				mst_financepaymentsmethods.strName AS PAY_TO ,
				mst_companies.strName AS COMPANY,
				mst_locations.strName AS LOCATION ,
				sys_users.strUserName as CREATOR ,
				finance_bank_payment_header.CREATED_DATE AS CREATED_DATE
				
				FROM
				finance_bank_payment_header
				INNER JOIN mst_financecurrency ON finance_bank_payment_header.CURRENCY_ID = mst_financecurrency.intId
				INNER JOIN finance_mst_chartofaccount ON finance_bank_payment_header.BANK_CHART_OF_ACCOUNT_ID = finance_mst_chartofaccount.CHART_OF_ACCOUNT_ID
				left JOIN mst_financepaymentsmethods ON finance_bank_payment_header.PAYMENT_METHOD = mst_financepaymentsmethods.intId
				INNER JOIN mst_companies ON finance_bank_payment_header.COMPANY_ID = mst_companies.intId
				INNER JOIN mst_locations ON finance_bank_payment_header.LOCATION_ID = mst_locations.intId
				INNER JOIN sys_users ON finance_bank_payment_header.CREATED_BY = sys_users.intUserId
				WHERE
				finance_bank_payment_header.BANK_PAYMENT_NO = '$serialNo' AND
				finance_bank_payment_header.BANK_PAYMENT_YEAR = '$serialYear'";
 		return $sql;
 	}
	
	private function LoadReportGridDetails_sql($serialYear,$serialNo)
	{
		global $session_companyId;
		
		$sql = "SELECT
				finance_bank_payment_details.BANK_PAYMENT_NO,
				finance_bank_payment_details.BANK_PAYMENT_YEAR,
				finance_bank_payment_details.CHART_OF_ACCOUNT_ID,
				finance_bank_payment_details.TAX_ID,
				finance_bank_payment_details.AMOUNT,
				finance_bank_payment_details.PAY_TO,
				finance_bank_payment_details.COST_CENTER_ID,
				finance_bank_payment_details.REMARKS,
				FCOA.CHART_OF_ACCOUNT_NAME,
				CONCAT(FMT.FINANCE_TYPE_CODE,'',FMMT.MAIN_TYPE_CODE,'',FMST.SUB_TYPE_CODE,'',FCOA.CHART_OF_ACCOUNT_CODE) AS accountCode,
				mst_financetaxgroup.strCode AS TAX,
				mst_financedimension.strName AS COST_CENTER, 
				finance_bank_payment_details.SETTELEMENT_NO,
				finance_bank_payment_details.SETTELEMENT_YEAR,
				finance_bank_payment_details.SETTELEMENT_ORDER 
				FROM
				finance_bank_payment_details
				LEFT JOIN finance_mst_chartofaccount as FCOA ON finance_bank_payment_details.CHART_OF_ACCOUNT_ID = FCOA.CHART_OF_ACCOUNT_ID
				
				LEFT JOIN finance_mst_account_sub_type FMST ON FMST.SUB_TYPE_ID=FCOA.SUB_TYPE_ID
				LEFT JOIN finance_mst_account_main_type FMMT ON FMMT.MAIN_TYPE_ID=FMST.MAIN_TYPE_ID
				LEFT JOIN finance_mst_account_type FMT ON FMT.FINANCE_TYPE_ID=FMMT.FINANCE_TYPE_ID
				
				LEFT JOIN mst_financetaxgroup ON finance_bank_payment_details.TAX_ID = mst_financetaxgroup.intId
				LEFT JOIN mst_financedimension ON finance_bank_payment_details.COST_CENTER_ID = mst_financedimension.intId
				WHERE
				finance_bank_payment_details.BANK_PAYMENT_NO = '$serialNo' AND
				finance_bank_payment_details.BANK_PAYMENT_YEAR = '$serialYear'
";
 		return $sql;
	}

 	private function Load_tot_amount_sql($serialYear,$serialNo)
	{
	  	$sql = "SELECT
				Sum(finance_bank_payment_details.AMOUNT) as amount 
				FROM `finance_bank_payment_details`
				WHERE
				finance_bank_payment_details.BANK_PAYMENT_NO = '$serialNo' AND
				finance_bank_payment_details.BANK_PAYMENT_YEAR = '$serialYear'";
				
 		return $sql;
 	}
	
	private function Load_Report_approval_details_sql($serialYear,$serialNo){
	  	$sql = "SELECT
				finance_bank_payment_header_approveby.intApproveUser,
				finance_bank_payment_header_approveby.dtApprovedDate,
				sys_users.strUserName as UserName,
				finance_bank_payment_header_approveby.intApproveLevelNo
				FROM
				finance_bank_payment_header_approveby
				Inner Join sys_users ON finance_bank_payment_header_approveby.intApproveUser = sys_users.intUserId
				WHERE
				finance_bank_payment_header_approveby.BANK_PAYMENT_NO =  '$serialNo' AND
				finance_bank_payment_header_approveby.BANK_PAYMENT_YEAR =  '$serialYear'      order by dtApprovedDate asc";
				
 		return $sql;
	}
 	private function HeaderData_pdf($paymentNo,$paymentYear,$executionType)
	{
		global $session_companyId;
		
		   $sql = "SELECT PH.CURRENCY_ID,
				FC.strCode AS currency,
				PGLH.COST_CENTER_ID,
				MFD.strCostCenterCode,
				COA.CHART_OF_ACCOUNT_NAME,
				COA.CHART_OF_ACCOUNT_NAME as supplier,
				DATE(PH.PAYMENT_DATE) AS paymentDate,
				(SELECT CONCAT(RIGHT(YEAR(DATE(dtmStartingDate)),2),'-',RIGHT(YEAR(DATE(dtmClosingDate)),2))
				FROM mst_financeaccountingperiod
				INNER JOIN mst_financeaccountingperiod_companies ON mst_financeaccountingperiod.intId=mst_financeaccountingperiod_companies.intPeriodId
				WHERE dtmStartingDate<=DATE(PH.PAYMENT_DATE) AND
				dtmClosingDate>=DATE(PH.PAYMENT_DATE) AND
				mst_financeaccountingperiod_companies.intCompanyId='$session_companyId') AS voucherYear,
				LPAD(MONTH(PH.PAYMENT_DATE),2,'0') AS voucherMonth,
				(PH.VOUCHER_NO) as voucherNo,
 				PH.REMARKS,
				PH.STATUS,
				PH.PRINT_STATUS,
				PH.CREATED_BY,
				ROUND((SELECT SUM(AMOUNT) FROM finance_bank_payment_details PD WHERE PD.BANK_PAYMENT_NO = PH.BANK_PAYMENT_NO AND PD.BANK_PAYMENT_YEAR = PH.BANK_PAYMENT_YEAR),2) AS TOTAL_AMOUNT,
				PH.COMPANY_ID AS COMPANY_ID,
				sys_users.strFullName AS CREATED_BY_NAME ,
				PH.BANK_REFERENCE_NO AS CHEQUE_NO
				FROM finance_bank_payment_header AS PH
				INNER JOIN finance_bank_payment_details AS PGLH ON PH.BANK_PAYMENT_NO = PGLH.BANK_PAYMENT_NO AND PH.BANK_PAYMENT_YEAR = PGLH.BANK_PAYMENT_YEAR
				INNER JOIN mst_financecurrency FC ON PH.CURRENCY_ID=FC.intId
				INNER JOIN finance_mst_chartofaccount COA ON PH.BANK_CHART_OF_ACCOUNT_ID=COA.CHART_OF_ACCOUNT_ID
 				INNER JOIN mst_financedimension MFD ON MFD.intId=PGLH.COST_CENTER_ID
				INNER JOIN sys_users ON PH.CREATED_BY = sys_users.intUserId
				WHERE PH.BANK_PAYMENT_NO='$paymentNo' AND
				PH.BANK_PAYMENT_YEAR='$paymentYear' AND
				PH.COMPANY_ID='$session_companyId' ";
		
		$result 	= $this->db->$executionType($sql);
 		$resultArry = mysqli_fetch_array($result);
	
		return $resultArry;
	}
	private function DetailData_pdf($paymentNo,$paymentYear,$executionType)
	{
		global $session_companyId;
		
		$sql = "SELECT CONCAT(COA.CHART_OF_ACCOUNT_NAME,' - ',PD.REMARKS) AS invoiceNo,
				PD.AMOUNT AS amonut,
				'' AS PONo,
				'' AS GRNNo
				FROM finance_bank_payment_header AS PH
				INNER JOIN finance_bank_payment_details AS PD ON PH.BANK_PAYMENT_NO = PD.BANK_PAYMENT_NO AND PH.BANK_PAYMENT_YEAR = PD.BANK_PAYMENT_YEAR
				INNER JOIN finance_mst_chartofaccount COA ON COA.CHART_OF_ACCOUNT_ID = PD.CHART_OF_ACCOUNT_ID
 				WHERE PD.BANK_PAYMENT_NO='$paymentNo' AND
				PD.BANK_PAYMENT_YEAR='$paymentYear' ";
		
		$result	= $this->db->$executionType($sql);
		
		return $result;
	}
	
}

?>
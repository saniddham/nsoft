<?php
$session_companyId 	= $_SESSION["headCompanyId"];
$session_userId		= $_SESSION["userId"];

include_once  $_SESSION['ROOT_PATH']."class/cls_commonErrorHandeling_get.php";

$obj_errorHandeling = new cls_commonErrorHandeling_get($db);

class Cls_Reconciliation_Get
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	public function loadData($accountId,$currencyId,$toDate,$transacType)
	{
		return $this->loadData_sql($accountId,$currencyId,$toDate,$transacType);
	}
	public function loadLedgerBalance($accountId,$currencyId,$toDate)
	{
		return $this->loadLedgerBalance_sql($accountId,$currencyId,$toDate);
	}
	public function loadStatementBalance($accountId,$currencyId,$toDate)
	{
		return $this->loadStatementBalance_sql($accountId,$currencyId,$toDate);
	}
	public function getRptHeaderData($recNo,$recYear)
	{
		return $this->loadRptHeaderData_sql($recNo,$recYear);
	}
	public function getRptApproveDetails($recNo,$recYear)
	{
		return $this->getRptApproveDetails_sql($recNo,$recYear);
	}
	public function checkPendingConfirm($recNo,$recYear)
	{
		return  $this->checkPendingConfirm_sql($recNo,$recYear);
	}
	public function getRptDetailData($recNo,$recYear,$type)
	{
		return  $this->getRptDetailData_sql($recNo,$recYear,$type);
	}
	public function loadHeaderData($recNo,$recYear)
	{
		return  $this->getHeaderData($recNo,$recYear);
	}
	public function getDetails($recNo,$recYear,$transacType)
	{
		return  $this->getDetails_sql($recNo,$recYear,$transacType);
	}
	public function validateBeforeApprove($recNo,$recYear,$programCode)
	{
		global $obj_errorHandeling;
		global $session_userId;
		
		$header_arr	= $this->getHeaderData($recNo,$recYear);
		$response 	= $obj_errorHandeling->get_permision_withApproval_confirm($header_arr['STATUS'],$header_arr['APPROVE_LEVELS'],$session_userId,$programCode,'RunQuery');
		
		return $response;
	}
	public function validateBeforeReject($recNo,$recYear,$programCode)
	{
		global $obj_errorHandeling;
		global $session_userId;
		
		$header_arr	= $this->getHeaderData($recNo,$recYear);
		$response 	= $obj_errorHandeling->get_permision_withApproval_reject($header_arr['STATUS'],$header_arr['APPROVE_LEVELS'],$session_userId,$programCode,'RunQuery');
		
		return $response;
	}
	public function validateBeforeCancel($recNo,$recYear,$programCode)
	{
		global $obj_errorHandeling;
		global $session_userId;
		
		$header_arr	= $this->getHeaderData($recNo,$recYear);
		$response 	= $obj_errorHandeling->get_permision_withApproval_cancel($header_arr['STATUS'],$header_arr['APPROVE_LEVELS'],$session_userId,$programCode,'RunQuery');
		
		return $response;
	}
	
	private function loadData_sql($accountId,$currencyId,$toDate,$transacType)
	{
/*		$sql = "SELECT FT.SERIAL_ID AS serialId, 
				DATE(FT.LAST_MODIFIED_DATE) AS dtDate,
				CONCAT(FT.DOCUMENT_NO,' / ',FT.DOCUMENT_YEAR) AS docNo,
				FT.AMOUNT,
				IFNULL((SELECT TYPE_NAME
				FROM finance_mst_transaction_type FMTT
				WHERE FMTT.DOCUMENT_TYPE=FT.DOCUMENT_TYPE AND
				FMTT.TRANSACTION_CATEGORY=FT.TRANSACTION_CATEGORY),'') AS transacType,
				
				IFNULL((SELECT CHART_OF_ACCOUNT_NAME
				FROM finance_transaction FT1
				INNER JOIN finance_mst_chartofaccount COA ON COA.CHART_OF_ACCOUNT_ID=FT1.CHART_OF_ACCOUNT_ID
				WHERE 
				FT1.INVOICE_NO=FT.INVOICE_NO AND
   				FT1.INVOICE_YEAR=FT.INVOICE_YEAR AND
				FT1.DOCUMENT_NO=FT.DOCUMENT_NO AND
				FT1.DOCUMENT_YEAR=FT.DOCUMENT_YEAR AND
				FT1.TRANSACTION_CATEGORY=FT.TRANSACTION_CATEGORY AND
				FT1.DOCUMENT_TYPE=FT.DOCUMENT_TYPE AND
				FT1.TRANSACTION_TYPE <>FT.TRANSACTION_TYPE AND
				FT1.AMOUNT=FT.AMOUNT ),'') AS chartOfAccName,
				
				IFNULL(FT.BANK_REFERENCE_NO,'') as bankRefNo,
				IFNULL(FT.REMARKS,'') as remarks
				FROM finance_transaction FT
				WHERE FT.CHART_OF_ACCOUNT_ID='$accountId' AND
				FT.CURRENCY_ID='$currencyId' AND
				FT.TRANSACTION_TYPE='$transacType' AND
				REC_STATUS='0' AND
				DATE(FT.LAST_MODIFIED_DATE)<='$toDate'
				ORDER BY FT.LAST_MODIFIED_DATE ";*/
				//removed subquery 2014-09-11 roshan
		$sql = "SELECT
					FT.SERIAL_ID AS serialId,
					DATE(FT.LAST_MODIFIED_DATE) AS dtDate,
					CONCAT(FT.DOCUMENT_NO,' / ',FT.DOCUMENT_YEAR) AS docNo,
					FT.AMOUNT,
					IFNULL(FT.BANK_REFERENCE_NO,'') AS bankRefNo,
					IFNULL(FT.REMARKS,'') AS remarks,
					ifnull(finance_mst_transaction_type.TYPE_NAME,'') AS transacType,
					ifnull(finance_mst_chartofaccount.CHART_OF_ACCOUNT_NAME,'') as CHART_OF_ACCOUNT_NAME
				FROM
					finance_transaction AS FT
					left JOIN finance_mst_transaction_type ON FT.DOCUMENT_TYPE = finance_mst_transaction_type.DOCUMENT_TYPE 
						AND FT.TRANSACTION_CATEGORY = finance_mst_transaction_type.TRANSACTION_CATEGORY
					left JOIN finance_mst_chartofaccount ON finance_mst_chartofaccount.CHART_OF_ACCOUNT_ID = FT.CHART_OF_ACCOUNT_ID
				WHERE FT.CHART_OF_ACCOUNT_ID='$accountId' AND
								FT.CURRENCY_ID='$currencyId' AND
								FT.TRANSACTION_TYPE='$transacType' AND
								REC_STATUS='0' AND
								DATE(FT.LAST_MODIFIED_DATE)<='$toDate' 
				ORDER BY FT.LAST_MODIFIED_DATE
				";	
				
		$result = $this->db->RunQuery($sql);
		while($row = mysqli_fetch_array($result))
		{
			$data['serialId'] 	 	= $row['serialId'];
			$data['dtDate'] 	 	= $row['dtDate'];
			$data['docNo'] 			= $row['docNo'];
			$data['AMOUNT']			= $row['AMOUNT'];
			$data['transacTypeName']= $row['transacType'];
			$data['chartOfAccName'] = $row['chartOfAccName'];
			$data['bankRefNo'] 		= $row['bankRefNo'];
			$data['remarks'] 		= $row['remarks'];
			$data['remarks'] 		= $row['remarks'];
			$data['transacType'] 	= $transacType;
			
			$arrDetailData[] 		= $data;
		}
		return $arrDetailData;
	}
	private function loadLedgerBalance_sql($accountId,$currencyId,$toDate)
	{
		global $session_companyId;
		
		$creditAmount 	= 0;
		$debitAmount  	= 0;
		$openingBal		= 0;
		
		
		$sql = "SELECT ROUND(SUM((amount/targetExchangeRate)*savedExchangeRate),2) AS AMOUNT,
				transactionType
				FROM(
				SELECT SUM(FT.AMOUNT) AS amount,
				FT.TRANSACTION_TYPE AS transactionType,
				(SELECT dblExcAvgRate 
				FROM mst_financeexchangerate
				WHERE mst_financeexchangerate.intCurrencyId=FT.CURRENCY_ID AND
				mst_financeexchangerate.intCompanyId='$session_companyId' AND
				mst_financeexchangerate.dtmDate=DATE(FT.LAST_MODIFIED_DATE)) AS savedExchangeRate,
				(SELECT dblExcAvgRate 
				FROM mst_financeexchangerate
				WHERE mst_financeexchangerate.intCurrencyId='$currencyId' AND
				mst_financeexchangerate.intCompanyId='$session_companyId' AND
				mst_financeexchangerate.dtmDate=DATE(FT.LAST_MODIFIED_DATE)) AS targetExchangeRate
				FROM finance_transaction FT
				INNER JOIN finance_mst_chartofaccount COA ON COA.CHART_OF_ACCOUNT_ID=FT.CHART_OF_ACCOUNT_ID
				INNER JOIN finance_mst_account_sub_type ST ON ST.SUB_TYPE_ID=COA.SUB_TYPE_ID
				INNER JOIN finance_mst_account_main_type MT ON MT.MAIN_TYPE_ID=ST.MAIN_TYPE_ID
				INNER JOIN finance_mst_account_type AT ON AT.FINANCE_TYPE_ID=MT.FINANCE_TYPE_ID
				WHERE FT.COMPANY_ID='$session_companyId' AND 
				DATE(FT.LAST_MODIFIED_DATE)<='$toDate' AND
				COA.CHART_OF_ACCOUNT_ID='$accountId'
				GROUP BY FT.CHART_OF_ACCOUNT_ID,DATE(FT.LAST_MODIFIED_DATE),FT.INVOICE_NO,
				FT.INVOICE_YEAR,FT.DOCUMENT_NO,FT.DOCUMENT_YEAR,FT.TRANSACTION_TYPE,FT.TRANSACTION_CATEGORY
				ORDER BY FT.CHART_OF_ACCOUNT_ID,FT.INVOICE_YEAR,FT.INVOICE_NO ) AS SUB_1
				GROUP BY transactionType";
		
		$result = $this->db->RunQuery($sql);
		while($row = mysqli_fetch_array($result))
		{
			if($row['transactionType']=='C')
				$creditAmount	= $row['AMOUNT'];
			else
				$debitAmount 	= $row['AMOUNT'];
		}
		$openingBal = $debitAmount-$creditAmount;
	
		return $openingBal ;
	}
	private function loadStatementBalance_sql($accountId,$currencyId,$toDate)
	{
		$sql = "SELECT IFNULL(STATEMENT_BALANCE,0) AS statementBal
				FROM finance_bank_reconciliation_header
				WHERE CHART_OF_ACCOUNT_ID='$accountId' AND
				CURRENCY_ID='$currencyId' AND
				TO_DATE<='$toDate' AND
				STATUS='1'
				ORDER BY CREATED_DATE DESC 
				LIMIT 1 ";
		$result = $this->db->RunQuery($sql);
		$row 	= mysqli_fetch_array($result);
		
		return ($row['statementBal']==''?0:$row['statementBal']);
	}
	private function loadRptHeaderData_sql($recNo,$recYear)
	{
		$sql = "SELECT BRH.STATEMENT_DATE,
				COA.CHART_OF_ACCOUNT_NAME,
				FC.strCode AS currency,
				BRH.FROM_DATE,
				BRH.TO_DATE,
				BRH.LEDGER_BALANCE,
				BRH.STATEMENT_BALANCE,
				BRH.STATUS,
				BRH.APPROVE_LEVELS,
				SU.strUserName
				FROM finance_bank_reconciliation_header BRH
				INNER JOIN finance_mst_chartofaccount COA ON COA.CHART_OF_ACCOUNT_ID=BRH.CHART_OF_ACCOUNT_ID
				INNER JOIN mst_financecurrency FC ON FC.intId=BRH.CURRENCY_ID
				INNER JOIN sys_users SU ON SU.intUserId=BRH.CREATED_BY
				WHERE BRH.RECONCILIATION_NO='$recNo' AND
				RECONCILIATION_YEAR='$recYear' ";
		
		$result = $this->db->RunQuery($sql);
		$row 	= mysqli_fetch_array($result);
		
		return $row;
	}
	private function getRptApproveDetails_sql($recNo,$recYear)
	{
		$sql = "SELECT
				BRAB.APPROVED_BY,
				BRAB.APPROVED_DATE,
				SU.strUserName AS UserName,
				BRAB.APPROVE_LEVEL_NO
				FROM
				finance_bank_reconciliation_approveby BRAB
				INNER JOIN sys_users SU ON BRAB.APPROVED_BY = SU.intUserId
				WHERE BRAB.RECONCILIATION_NO ='$recNo' AND
				BRAB.RECONCILIATION_YEAR ='$recYear'      
				ORDER BY BRAB.APPROVED_DATE ASC ";
		
		$result = $this->db->RunQuery($sql);
		
		return $result;
	}
	private function checkPendingConfirm_sql($recNo,$recYear)
	{
		$sql = "SELECT * FROM finance_bank_reconciliation_header
				WHERE STATUS NOT IN (-2,1) AND
				RECONCILIATION_NO <> '$recNo' AND
				RECONCILIATION_YEAR <> '$recYear' ";
		
		$result = $this->db->RunQuery($sql);
		
		return $result;
		
	}
	private function getRptDetailData_sql($recNo,$recYear,$type)
	{
		if($type=='reconcile')
			$sqlW = "AND BRD.SAVE_FLAG=1 ";
		else
			$sqlW = 'AND BRD.SAVE_FLAG=0'; 
		
/*		$sql = "SELECT BRD.SERIAL_ID,
				BRD.PRESENTED_DATE,
				BRD.SAVE_FLAG,
				DATE(FT.LAST_MODIFIED_DATE) AS dtDate,
				CONCAT(FT.DOCUMENT_NO,'/',FT.DOCUMENT_YEAR) AS docNo,
				FT.AMOUNT,
				IFNULL((SELECT TYPE_NAME
				FROM finance_mst_transaction_type FMTT
				WHERE FMTT.DOCUMENT_TYPE=FT.DOCUMENT_TYPE AND
				FMTT.TRANSACTION_CATEGORY=FT.TRANSACTION_CATEGORY),'') AS transacType,
				IFNULL((SELECT CHART_OF_ACCOUNT_NAME
				FROM finance_transaction FT1
				INNER JOIN finance_mst_chartofaccount COA ON COA.CHART_OF_ACCOUNT_ID=FT1.CHART_OF_ACCOUNT_ID
				WHERE FT1.INVOICE_NO=FT.INVOICE_NO AND 
  				FT1.INVOICE_YEAR=FT.INVOICE_YEAR AND 
				FT1.DOCUMENT_NO=FT.DOCUMENT_NO AND
				FT1.DOCUMENT_YEAR=FT.DOCUMENT_YEAR AND
				FT1.TRANSACTION_CATEGORY=FT.TRANSACTION_CATEGORY AND
				FT1.DOCUMENT_TYPE=FT.DOCUMENT_TYPE AND
				FT1.TRANSACTION_TYPE <>FT.TRANSACTION_TYPE AND
				FT1.AMOUNT=FT.AMOUNT ),'') AS chartOfAccName,
				IFNULL(FT.BANK_REFERENCE_NO,'') AS bankRefNo,
				IFNULL(FT.REMARKS,'') AS remarks,
				BRD.TRANSACTION_TYPE
				FROM finance_bank_reconciliation_details BRD
				INNER JOIN finance_transaction FT ON FT.SERIAL_ID=BRD.SERIAL_ID
				WHERE BRD.RECONCILIATION_NO='$recNo' AND
				BRD.RECONCILIATION_YEAR='$recYear' 
				$sqlW 
				ORDER BY BRD.TRANSACTION_TYPE DESC,DATE(FT.LAST_MODIFIED_DATE)";*/
		//Removed subquery 2014-09-11 roshan
		$sql = "SELECT 	BRD.SERIAL_ID,
						BRD.PRESENTED_DATE,
						BRD.SAVE_FLAG,
						DATE(FT.LAST_MODIFIED_DATE) AS dtDate,
						CONCAT(FT.DOCUMENT_NO,'/',FT.DOCUMENT_YEAR) AS docNo,
						FT.AMOUNT,
						ifnull(finance_mst_transaction_type.TYPE_NAME,'') AS transacType,
						ifnull(finance_mst_chartofaccount.CHART_OF_ACCOUNT_NAME,'') AS CHART_OF_ACCOUNT_NAME,

						IFNULL(FT.BANK_REFERENCE_NO,'') AS bankRefNo,
						IFNULL(FT.REMARKS,'') AS remarks,
						BRD.TRANSACTION_TYPE
				FROM finance_bank_reconciliation_details BRD
					INNER JOIN finance_transaction FT ON FT.SERIAL_ID=BRD.SERIAL_ID
					left JOIN finance_mst_transaction_type ON FT.DOCUMENT_TYPE = finance_mst_transaction_type.DOCUMENT_TYPE 
						AND FT.TRANSACTION_CATEGORY = finance_mst_transaction_type.TRANSACTION_CATEGORY
					left JOIN finance_mst_chartofaccount ON finance_mst_chartofaccount.CHART_OF_ACCOUNT_ID = FT.CHART_OF_ACCOUNT_ID
				WHERE BRD.RECONCILIATION_NO='$recNo' AND
					BRD.RECONCILIATION_YEAR='$recYear' 
					$sqlW 
				ORDER BY BRD.TRANSACTION_TYPE DESC,DATE(FT.LAST_MODIFIED_DATE)";
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	private function getHeaderData($recNo,$recYear)
	{
		$sql = "SELECT STATEMENT_DATE,
				CHART_OF_ACCOUNT_ID,
				CURRENCY_ID,
				FROM_DATE,
				TO_DATE,
				LEDGER_BALANCE,
				STATEMENT_BALANCE,
				STATUS,
				APPROVE_LEVELS
				FROM finance_bank_reconciliation_header
				WHERE RECONCILIATION_NO='$recNo' AND
				RECONCILIATION_YEAR='$recYear' ";
		
		$result = $this->db->RunQuery($sql);
		$row 	= mysqli_fetch_array($result);
		
		return $row;
	}
	private function getDetails_sql($recNo,$recYear,$transacType)
	{
		$sql = "SELECT BRD.SERIAL_ID,
				BRD.PRESENTED_DATE,
				BRD.SAVE_FLAG,
				DATE(FT.LAST_MODIFIED_DATE) AS dtDate,
				CONCAT(FT.DOCUMENT_NO,'/',FT.DOCUMENT_YEAR) AS docNo,
				FT.AMOUNT,
				IFNULL((SELECT TYPE_NAME
				FROM finance_mst_transaction_type FMTT
				WHERE FMTT.DOCUMENT_TYPE=FT.DOCUMENT_TYPE AND
				FMTT.TRANSACTION_CATEGORY=FT.TRANSACTION_CATEGORY),'') AS transacType,
				IFNULL((SELECT CHART_OF_ACCOUNT_NAME
				FROM finance_transaction FT1
				INNER JOIN finance_mst_chartofaccount COA ON COA.CHART_OF_ACCOUNT_ID=FT1.CHART_OF_ACCOUNT_ID
				WHERE FT1.INVOICE_NO=FT.INVOICE_NO AND 
  				FT1.INVOICE_YEAR=FT.INVOICE_YEAR AND
				FT1.DOCUMENT_NO=FT.DOCUMENT_NO AND
				FT1.DOCUMENT_YEAR=FT.DOCUMENT_YEAR AND
				FT1.TRANSACTION_CATEGORY=FT.TRANSACTION_CATEGORY AND
				FT1.DOCUMENT_TYPE=FT.DOCUMENT_TYPE AND
				FT1.TRANSACTION_TYPE <>FT.TRANSACTION_TYPE AND
				FT1.AMOUNT=FT.AMOUNT ),'') AS chartOfAccName,
				IFNULL(FT.BANK_REFERENCE_NO,'') AS bankRefNo,
				IFNULL(FT.REMARKS,'') AS remarks,
				BRD.TRANSACTION_TYPE
				FROM finance_bank_reconciliation_details BRD
				INNER JOIN finance_transaction FT ON FT.SERIAL_ID=BRD.SERIAL_ID
				WHERE BRD.RECONCILIATION_NO='$recNo' AND
				BRD.RECONCILIATION_YEAR='$recYear' AND
				BRD.TRANSACTION_TYPE='$transacType' AND
				FT.REC_STATUS=0
				ORDER BY BRD.TRANSACTION_TYPE DESC,DATE(FT.LAST_MODIFIED_DATE)";
		
		$result = $this->db->RunQuery($sql);
		return $result;
	}
}
?>
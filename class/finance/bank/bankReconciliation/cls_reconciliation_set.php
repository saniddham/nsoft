<?php
include_once  "../../../../class/cls_commonFunctions_get.php";
include_once  "../../../../class/cls_commonErrorHandeling_get.php";

$session_userId		= $_SESSION["userId"];
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];
$savedMasseged		= "";
$error_sql			= "";

$obj_common			= new cls_commonFunctions_get($db);
$obj_errorHandeling = new cls_commonErrorHandeling_get($db);

class Cls_Reconciliation_Set
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	public function save($arrHeader,$arrDetails,$programCode,$programName)
	{
		global $obj_common;
		global $obj_errorHandeling;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_locationId;
		global $session_companyId;
		global $session_userId;
		
		
		$recNo			= $arrHeader["recNo"];
		$recYear		= $arrHeader["recYear"];
		$accountId		= $arrHeader["accountId"];
		$currencyId		= $arrHeader["currencyId"];
		$fromDate		= $arrHeader["fromDate"];
		$toDate			= $arrHeader["toDate"];
		$statementDate	= $arrHeader["statementDate"];
		$ledgerBal		= ($arrHeader["ledgerBal"]==''?0:$arrHeader["ledgerBal"]);	
		$statementBal	= ($arrHeader["statementBal"]==''?0:$arrHeader["statementBal"]);	
		
		$savedStatus	= true;
		$editMode		= false;
		$this->db->begin();
		
		$this->validateBeforeSave($recNo,$recYear,$programCode,$session_userId);
		if($recNo=='' && $recYear=='')
		{
			$invoice_array 	= $obj_common->GetSystemMaxNo('intReconciliationNo',$session_locationId);
			if($invoice_array["rollBackFlag"]==1)
			{
				if($savedStatus)
				{
					$savedStatus	= false;
					$savedMasseged 	= $invoice_array["msg"];
					$error_sql		= $invoice_array["q"];
				}
				
			}
			$recNo					= $invoice_array["max_no"];
			$recYear				= date('Y');
			$approveLevels 			= $obj_common->getApproveLevels($programName);
			$status					= $approveLevels+1;
			
			$this->saveHeader($recNo,$recYear,$approveLevels,$status,$accountId,$currencyId,$fromDate,$toDate,$statementDate,$ledgerBal,$statementBal);
		}
		else
		{
			$header_array 			= $this->getHeaderData($recNo,$recYear);
			$status					= $header_array['STATUS'];
			$approveLevels 			= $obj_common->getApproveLevels($programName);
			$status					= $approveLevels+1;
			
			$this->updateHeader($recNo,$recYear,$approveLevels,$status,$accountId,$currencyId,$fromDate,$toDate,$statementDate,$ledgerBal,$statementBal);
			$this->deleteDetails($recNo,$recYear);
			$editMode				= true;
		}
		foreach($arrDetails as $array_loop)
		{
			$serialNo				= $array_loop["serialNo"];
			$trType					= $array_loop["trType"];
			$saveFlag				= $array_loop["saveFlag"];
			$prDate					= (($array_loop["prDate"]=='' || $saveFlag==0)?'null':"'".$array_loop["prDate"]."'");
			
			$this->saveDetails($recNo,$recYear,$serialNo,$trType,$prDate,$saveFlag);
		}
		$sysNoArr					= $obj_common->validateDuplicateSerialNoWithSysNo($recNo,'intReconciliationNo',$session_locationId);
		if($sysNoArr['type'] == 'fail' && $savedStatus)
		{
			$savedStatus			= false;
			$savedMasseged	 		= $sysNoArr['msg'];
		}
		if($savedStatus)
		{
			$this->db->commit();
			$response['type'] 			= "pass";
			if($editMode)
				$response['msg'] 		= "Updated Successfully.";
			else
				$response['msg'] 		= "Saved Successfully.";
			
			$response['recNo']			= $recNo;
			$response['recYear']		= $recYear;
		}
		else
		{
			$this->db->rollback();
			$response['type'] 			= "fail";
			$response['msg'] 			= $savedMasseged;
			$response['sql']			= $error_sql;
		}
		return json_encode($response);		
	}
	public function approve($recNo,$recYear)
	{
		global $obj_common;
		global $obj_errorHandeling;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_locationId;
		global $session_companyId;
		global $session_userId;
		global $finalApproval;
		
		$savedStatus	= true;
		$finalApproval	= false;
		$this->db->begin();
		
		$this->updateHeaderStatus($recNo,$recYear,'');
		$this->approvedData($recNo,$recYear);
		
		if($savedStatus)
		{
			$this->db->commit();
			$response['type'] 			= "pass";
			if($finalApproval)
				$response['msg'] 		= "Final Approval Raised Successfully.";
			else
				$response['msg'] 		= "Approved Successfully.";
		}
		else
		{
			$this->db->rollback();
			$response['type'] 			= "fail";
			$response['msg'] 			= $savedMasseged;
			$response['sql']			= $error_sql;
		}
		return json_encode($response);
	}
	public function reject($recNo,$recYear)
	{
		global $obj_common;
		global $obj_errorHandeling;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_locationId;
		global $session_companyId;
		global $session_userId;
		
		$savedStatus	= true;
		$this->db->begin();
		
		$this->updateHeaderStatus($recNo,$recYear,'0');
		$this->updateMaxStatus($recNo,$recYear);
		$this->approved_by_insert($recNo,$recYear,$session_userId,0);
		
		if($savedStatus)
		{
			$this->db->commit();
			$response['type'] 		= "pass";
			$response['msg'] 		= "Rejected Successfully.";
		}
		else
		{
			$this->db->rollback();
			$response['type'] 			= "fail";
			$response['msg'] 			= $savedMasseged;
			$response['sql']			= $error_sql;
		}
		return json_encode($response);
	}
	public function cancel($recNo,$recYear)
	{
		global $obj_common;
		global $obj_errorHandeling;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_locationId;
		global $session_companyId;
		global $session_userId;
		
		$savedStatus	= true;
		$this->db->begin();
		
		$this->cancelValidation($recNo,$recYear);
		
		$this->updateHeaderStatus($recNo,$recYear,'-2');
		$this->updateMaxStatus($recNo,$recYear);
		$this->approved_by_insert($recNo,$recYear,$session_userId,-2);
		
		$this->updateFinanceTransaction($recNo,$recYear,'cancel');
		
		if($savedStatus)
		{
			$this->db->commit();
			$response['type'] 		= "pass";
			$response['msg'] 		= "Rejected Successfully.";
		}
		else
		{
			$this->db->rollback();
			$response['type'] 			= "fail";
			$response['msg'] 			= $savedMasseged;
			$response['sql']			= $error_sql;
		}
		return json_encode($response);
	}
	private function validateBeforeSave($recNo,$recYear,$programCode,$session_userId)
	{
		global $obj_errorHandeling;
		global $savedStatus;
		global $savedMasseged;
		
		$header_arr		= $this->getHeaderData($recNo,$recYear);
		$response 		= $obj_errorHandeling->get_permision_withApproval_save($header_arr['STATUS'],$header_arr['APPROVE_LEVELS'],$session_userId,$programCode,'RunQuery2');
		
		
		if($response["type"]=='fail' && ($savedStatus))
		{
			$savedStatus	= false;
			$savedMasseged 	= $response["msg"];
		}
	}
	private function getSavePermission($programCode,$userId)
	{
		$sql = "SELECT
				menupermision.intAdd 
				FROM menupermision 
				INNER JOIN menus ON menupermision.intMenuId = menus.intId
				WHERE
				menus.strCode = '$programCode' AND
				menupermision.intUserId = '$userId' ";
		
		$result = $this->db->RunQuery2($sql);
		$row 	= mysqli_fetch_array($result);
		
		if($row['intAdd']!=1)
			return false;
		else
			return true;
	}
	private function getHeaderData($recNo,$recYear)
	{
		$sql = "SELECT STATUS,
				APPROVE_LEVELS
				FROM finance_bank_reconciliation_header
				WHERE RECONCILIATION_NO='$recNo' AND
				RECONCILIATION_YEAR='$recYear' ";
		
		$result = $this->db->RunQuery2($sql);
		$row 	= mysqli_fetch_array($result);
		
		return $row;
	}
	private function saveHeader($recNo,$recYear,$approveLevels,$status,$accountId,$currencyId,$fromDate,$toDate,$statementDate,$ledgerBal,$statementBal)
	{
		global $session_userId;
		global $session_companyId;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "INSERT INTO finance_bank_reconciliation_header 
				(
				RECONCILIATION_NO, 
				RECONCILIATION_YEAR, 
				STATEMENT_DATE, 
				CHART_OF_ACCOUNT_ID, 
				CURRENCY_ID, 
				FROM_DATE, 
				TO_DATE, 
				LEDGER_BALANCE, 
				STATEMENT_BALANCE, 
				COMPANY_ID, 
				STATUS, 
				APPROVE_LEVELS, 
				CREATED_BY, 
				CREATED_DATE
				)
				VALUES
				(
				'$recNo', 
				'$recYear', 
				'$statementDate', 
				'$accountId', 
				'$currencyId', 
				'$fromDate', 
				'$toDate', 
				'$ledgerBal', 
				'$statementBal', 
				'$session_companyId', 
				'$status', 
				'$approveLevels', 
				'$session_userId', 
				NOW()
				);";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function updateHeader($recNo,$recYear,$approveLevels,$status,$accountId,$currencyId,$fromDate,$toDate,$statementDate,$ledgerBal,$statementBal)
	{
		global $session_userId;
		global $session_companyId;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "UPDATE finance_bank_reconciliation_header 
				SET
				STATEMENT_DATE = '$statementDate' , 
				CHART_OF_ACCOUNT_ID = '$accountId' , 
				CURRENCY_ID = '$currencyId' , 
				FROM_DATE = '$fromDate' , 
				TO_DATE = '$toDate' , 
				LEDGER_BALANCE = '$ledgerBal' , 
				STATEMENT_BALANCE = '$statementBal' , 
				COMPANY_ID = '$session_companyId' , 
				STATUS = '$status' , 
				APPROVE_LEVELS = '$approveLevels' , 
				LAST_MODIFY_BY = '$session_userId'
				WHERE
				RECONCILIATION_NO = '$recNo' AND 
				RECONCILIATION_YEAR = '$recYear' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function deleteDetails($recNo,$recYear)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "DELETE FROM finance_bank_reconciliation_details 
				WHERE
				RECONCILIATION_NO = '$recNo' AND 
				RECONCILIATION_YEAR = '$recYear' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function saveDetails($recNo,$recYear,$serialNo,$trType,$prDate,$saveFlag)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "INSERT INTO finance_bank_reconciliation_details 
				(
				RECONCILIATION_NO, 
				RECONCILIATION_YEAR, 
				SERIAL_ID,
				TRANSACTION_TYPE,
				PRESENTED_DATE,
				SAVE_FLAG
				)
				VALUES
				(
				'$recNo', 
				'$recYear', 
				'$serialNo',
				'$trType',
				$prDate,
				'$saveFlag'
				) ";
		//die($sql);
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function updateHeaderStatus($recNo,$recYear,$status)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		if($status=='') 
			$para='STATUS-1';
 		else
			$para=$status;
		
		$sql = "UPDATE finance_bank_reconciliation_header 
				SET
				STATUS = ".$para."
				WHERE
				RECONCILIATION_NO = '$recNo' AND 
				RECONCILIATION_YEAR = '$recYear' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function approvedData($recNo,$recYear)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_userId;
		global $finalApproval;
		
		$header_arr 	= $this->getHeaderData($recNo,$recYear);
		$status			= $header_arr['STATUS'];
		$savedLevels	= $header_arr['APPROVE_LEVELS'];
		
		if($status==1)
		{
			$finalApproval = true;	
			$this->updateFinanceTransaction($recNo,$recYear,'approve');
		}	
		$approval		= $savedLevels+1-$status;
		$this->approved_by_insert($recNo,$recYear,$session_userId,$approval);
	}
	private function approved_by_insert($recNo,$recYear,$session_userId,$approval)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "INSERT INTO finance_bank_reconciliation_approveby 
				(
				RECONCILIATION_NO, 
				RECONCILIATION_YEAR, 
				APPROVE_LEVEL_NO, 
				APPROVED_BY, 
				APPROVED_DATE, 
				STATUS
				)
				VALUES
				(
				'$recNo', 
				'$recYear', 
				'$approval', 
				'$session_userId', 
				NOW(), 
				0
				) ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function updateMaxStatus($recNo,$recYear)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sqlMax = " SELECT (MAX(tb.STATUS)+1) AS maxStatus  
					FROM finance_bank_reconciliation_approveby AS tb
					WHERE tb.RECONCILIATION_NO='$recNo' AND
					tb.RECONCILIATION_YEAR='$recYear' ";
		
		$resultMax 	= $this->db->RunQuery2($sqlMax);
		$rowMax		= mysqli_fetch_array($resultMax);
		$maxStatus 	= $rowMax['maxStatus'];
		
		$sqlUpd = " UPDATE finance_bank_reconciliation_approveby 
					SET
					STATUS = '$maxStatus'
					WHERE
					RECONCILIATION_NO = '$recNo' AND 
					RECONCILIATION_YEAR = '$recYear' ";
		
		$resultUpd = $this->db->RunQuery2($sqlUpd);
		if(!$resultUpd)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sqlUpd;
			}
		}
	}
	private function updateFinanceTransaction($recNo,$recYear,$type)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		if($type=='approve')
			$status = 1;
		else
			$status = 0;
			
		$sql = "UPDATE finance_transaction
				SET REC_STATUS = '$status'
				WHERE SERIAL_ID IN (SELECT SERIAL_ID
									FROM finance_bank_reconciliation_details
									WHERE RECONCILIATION_NO='$recNo' AND
									RECONCILIATION_YEAR='$recYear' AND
									SAVE_FLAG = 1)";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function cancelValidation($recNo,$recYear)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_companyId;
		
		$sql = "SELECT RECONCILIATION_NO,RECONCILIATION_YEAR
				FROM finance_bank_reconciliation_header
				WHERE COMPANY_ID='$session_companyId'
				ORDER BY RECONCILIATION_YEAR,RECONCILIATION_NO DESC
				LIMIT 0,1";
		
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		
		if($recNo!=$row['RECONCILIATION_NO'])
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= "System allow to cancel the last Reconciliation only .";
			}
		}
	}
}
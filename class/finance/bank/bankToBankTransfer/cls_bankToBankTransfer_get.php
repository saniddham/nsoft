<?php
include_once  $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
include_once  $_SESSION['ROOT_PATH']."class/cls_commonErrorHandeling_get.php";
//ini_set('display_errors', 1); 
 
$obj_commonErr		= new cls_commonErrorHandeling_get($db);
$obj_common			= new cls_commonFunctions_get($db);
$progrmCode			='P0764';

$session_userId		= $_SESSION["userId"];
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];

class Cls_BankToBankTransfer_Get
{
	private $db;
 	
	function __construct($db)
	{
		$this->db = $db;
 	}

//BEGIN - PUBLIC FUNCTIONS {	
 	public function get_header_array($serialYear,$serialNo)
	{
		
		$sql = $this->Load_header_sql($serialYear,$serialNo);
		$result = $this->db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return $row;
	}
	public function get_header_array2($serialYear,$serialNo)
	{
		
		$sql = $this->Load_header_sql($serialYear,$serialNo);
		$result = $this->db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		return $row;
	}
	public function get_details_array2($serialYear,$serialNo)
	{
 		$sql = $this->Load_details_sql($serialYear,$serialNo);
		$result = $this->db->RunQuery2($sql);
		return $result;
	}
	public function get_report_header_array($serialYear,$serialNo)
	{
 		$sql = $this->Load_report_header_sql($serialYear,$serialNo);
		$result = $this->db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return $row;
	}
	public function get_report_details_array($serialYear,$serialNo)
	{
		$sql = $this->Load_report_details_sql($serialYear,$serialNo);
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	public function get_Report_approval_details_result($serialYear,$serialNo)
	{
		$sql = $this->Load_Report_approval_details_sql($serialYear,$serialNo);
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	public function get_details_result($serialYear,$serialNo)
	{
		$sql = $this->Load_details_sql($serialYear,$serialNo);
		$result = $this->db->RunQuery($sql);
		return $result;
	}
 	
	public function ValidateBeforeSave($serialYear,$serialNo)
	{ 
		global $session_userId;
		global $obj_commonErr;	
		global $progrmCode;
		$this->db->begin();
   		$header_arr			= $this->get_header_array2($serialYear,$serialNo);
 		$permision_arr		= $obj_commonErr->get_permision_withApproval_save($header_arr['STATUS'],$header_arr['LEVELS'],$session_userId,$progrmCode,'RunQuery2');
		if($permision_arr['type'] =='fail'){
			$rollBack			=	1;
			$response['msg'] 	=	$permision_arr['msg'];	
			$response['type'] 	=	'fail';
			return $response;
		}
		$this->db->commit();
	}
	
	public function ValidateBeforeApprove($serialYear,$serialNo)
	{ 
		global $session_userId;
		global $obj_commonErr;	
		global $progrmCode;
		$this->db->begin();
   		$header_arr			= $this->get_header_array2($serialYear,$serialNo);
 		$permision_arr		= $obj_commonErr->get_permision_withApproval_confirm($header_arr['STATUS'],$header_arr['LEVELS'],$session_userId,$progrmCode,'RunQuery2');
		$this->db->commit();
		if($permision_arr['type'] =='fail'){
			$rollBack			=	1;
			$response['msg'] 	=	$permision_arr['msg'];	
			$response['type'] 	=	'fail';
 			return $response;
		}
		
	}

	public function ValidateBeforeReject($serialYear,$serialNo)
	{ 
		global $session_userId;
		global $obj_commonErr;	
		global $progrmCode;
		$this->db->begin();
   		$header_arr			= $this->get_header_array2($serialYear,$serialNo);
 		$permision_arr		= $obj_commonErr->get_permision_withApproval_reject($header_arr['STATUS'],$header_arr['LEVELS'],$session_userId,$progrmCode,'RunQuery2');
		if($permision_arr['type'] =='fail'){
			$rollBack			=	1;
			$response['msg'] 	=	$permision_arr['msg'];	
			$response['type'] 	=	'fail';
 			return $response;
		}
		$this->db->commit();
		
	}
	public function ValidateBeforeCancel($serialYear,$serialNo)
	{ 
		global $session_userId;
		global $obj_commonErr;	
		global $progrmCode;
		$this->db->begin();
   		$header_arr			= $this->get_header_array2($serialYear,$serialNo);
 		$permision_arr		= $obj_commonErr->get_permision_withApproval_cancel($header_arr['STATUS'],$header_arr['LEVELS'],$session_userId,$progrmCode,'RunQuery2');
		if($permision_arr['type'] =='fail'){
			$rollBack			=	1;
			$response['msg'] 	=	$permision_arr['msg'];	
			$response['type'] 	=	'fail';
			return $response; 
 		}
		$this->db->commit();
		
	}
	
 	public function getTotAmmount2($serialYear,$serialNo,$type)
	{
	  	$sql = $this->getTotAmmount_sql($serialYear,$serialNo,$type);
 		$result = $this->db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		return $row['amount'];
 	}

	
 //END	- PUBLIC FUNCTIONS }

//BEGIN - PRIVATE FUNCTIONS {	
	private function Load_header_sql($serialYear,$serialNo)
	{
		global $session_companyId;
 		
		 $sql = "SELECT
				finance_bank_to_bank_transfer_header.TRANSFER_NO,
				finance_bank_to_bank_transfer_header.TRANSFER_YEAR,
				finance_bank_to_bank_transfer_header.CURRENCY_ID,
				finance_bank_to_bank_transfer_header.REFERENCE_NO,
				finance_bank_to_bank_transfer_header.TRANSFER_DATE,
				finance_bank_to_bank_transfer_header.COMPANY_ID,
				finance_bank_to_bank_transfer_header.LOCATION_ID,
				finance_bank_to_bank_transfer_header.CREATED_BY,
				finance_bank_to_bank_transfer_header.CREATED_DATE,
				finance_bank_to_bank_transfer_header.MODIFIED_BY,
				finance_bank_to_bank_transfer_header.MODIFIED_DATE,
				finance_bank_to_bank_transfer_header.SAVE_LEVELS as LEVELS,
				finance_bank_to_bank_transfer_header.STATUS 
				FROM `finance_bank_to_bank_transfer_header`
				WHERE
				finance_bank_to_bank_transfer_header.TRANSFER_NO = '$serialNo' AND
				finance_bank_to_bank_transfer_header.TRANSFER_YEAR = '$serialYear'";
 		return $sql;
 	}
	
	private function Load_details_sql($serialYear,$serialNo)
	{
		global $session_companyId;
		
		$sql = "SELECT
				finance_bank_to_bank_transfer_details.TRANSFER_YEAR,
				finance_bank_to_bank_transfer_details.CHART_OF_ACCOUNT_ID,
				finance_bank_to_bank_transfer_details.AMOUNT,
				finance_bank_to_bank_transfer_details.TRANSACTION_TYPE,
				finance_bank_to_bank_transfer_details.REMARKS,
				finance_bank_to_bank_transfer_details.COST_CENTER_ID,
				finance_bank_to_bank_transfer_details.TRANSFER_NO, 
				finance_bank_to_bank_transfer_details.ORDER_ID
				FROM `finance_bank_to_bank_transfer_details`
				WHERE
				finance_bank_to_bank_transfer_details.TRANSFER_NO = '$serialNo' AND
				finance_bank_to_bank_transfer_details.TRANSFER_YEAR = '$serialYear' 
				ORDER BY finance_bank_to_bank_transfer_details.`ORDER_ID` ASC";
 		return $sql;
	}
	private function Load_report_header_sql($serialYear,$serialNo)
	{
		global $session_companyId;
 		
		$sql = "SELECT 
				finance_bank_to_bank_transfer_header.TRANSFER_NO				AS SERIAL_NO,
				finance_bank_to_bank_transfer_header.COMPANY_ID					AS COMPANY_ID,
				finance_bank_to_bank_transfer_header.TRANSFER_DATE				AS SERIAL_DATE,
				finance_bank_to_bank_transfer_header.TRANSFER_NO,
				finance_bank_to_bank_transfer_header.TRANSFER_YEAR,
				finance_bank_to_bank_transfer_header.CURRENCY_ID,
				finance_bank_to_bank_transfer_header.REFERENCE_NO,
				finance_bank_to_bank_transfer_header.TRANSFER_DATE,
				finance_bank_to_bank_transfer_header.COMPANY_ID,
				finance_bank_to_bank_transfer_header.LOCATION_ID,
				finance_bank_to_bank_transfer_header.CREATED_BY,
				finance_bank_to_bank_transfer_header.CREATED_DATE,
				finance_bank_to_bank_transfer_header.MODIFIED_BY,
				finance_bank_to_bank_transfer_header.MODIFIED_DATE,
				finance_bank_to_bank_transfer_header.SAVE_LEVELS AS LEVELS,
				finance_bank_to_bank_transfer_header.STATUS ,
				mst_financecurrency.strCode as CURRENCY ,
				mst_locations.strName AS LOCATION,
				mst_companies.strName AS COMPANY,
				sys_users.strUserName AS CREATOR,
				finance_bank_to_bank_transfer_header.CREATED_DATE
				FROM `finance_bank_to_bank_transfer_header`
				INNER JOIN mst_financecurrency ON finance_bank_to_bank_transfer_header.CURRENCY_ID = mst_financecurrency.intId
				INNER JOIN mst_locations ON finance_bank_to_bank_transfer_header.LOCATION_ID = mst_locations.intId
				INNER JOIN mst_companies ON finance_bank_to_bank_transfer_header.COMPANY_ID = mst_companies.intId
				INNER JOIN sys_users ON finance_bank_to_bank_transfer_header.CREATED_BY = sys_users.intUserId
				WHERE
				finance_bank_to_bank_transfer_header.TRANSFER_NO = '$serialNo' AND
				finance_bank_to_bank_transfer_header.TRANSFER_YEAR = '$serialYear'";
 		return $sql;
 	}
	
	private function Load_report_details_sql($serialYear,$serialNo)
	{
		global $session_companyId;
		
	  	$sql = "SELECT
				finance_bank_to_bank_transfer_details.TRANSFER_YEAR,
				finance_bank_to_bank_transfer_details.CHART_OF_ACCOUNT_ID,
				finance_bank_to_bank_transfer_details.AMOUNT,
				finance_bank_to_bank_transfer_details.TRANSACTION_TYPE,
				IF(finance_bank_to_bank_transfer_details.TRANSACTION_TYPE='C','CREDIT','DEBIT') AS TRANSACTION, 
				finance_bank_to_bank_transfer_details.REMARKS,
				finance_bank_to_bank_transfer_details.COST_CENTER_ID,
				finance_bank_to_bank_transfer_details.TRANSFER_NO,
				FCOA.CHART_OF_ACCOUNT_NAME,
				CONCAT(FMT.FINANCE_TYPE_CODE,'',FMMT.MAIN_TYPE_CODE,'',FMST.SUB_TYPE_CODE,'',FCOA.CHART_OF_ACCOUNT_CODE) AS accountCode,
				mst_financedimension.strName as COST_CENTER, 
				finance_bank_to_bank_transfer_details.ORDER_ID
				FROM `finance_bank_to_bank_transfer_details`
				INNER JOIN finance_mst_chartofaccount as FCOA ON finance_bank_to_bank_transfer_details.CHART_OF_ACCOUNT_ID = FCOA.CHART_OF_ACCOUNT_ID
	
				INNER JOIN finance_mst_account_sub_type FMST ON FMST.SUB_TYPE_ID=FCOA.SUB_TYPE_ID
				INNER JOIN finance_mst_account_main_type FMMT ON FMMT.MAIN_TYPE_ID=FMST.MAIN_TYPE_ID
				INNER JOIN finance_mst_account_type FMT ON FMT.FINANCE_TYPE_ID=FMMT.FINANCE_TYPE_ID
				-- INNER JOIN finance_mst_chartofaccount_company FCOAC ON FCOAC.CHART_OF_ACCOUNT_ID=FCOA.CHART_OF_ACCOUNT_ID
			
				INNER JOIN mst_financedimension ON finance_bank_to_bank_transfer_details.COST_CENTER_ID = mst_financedimension.intId
				WHERE
				finance_bank_to_bank_transfer_details.TRANSFER_NO = '$serialNo' AND
				finance_bank_to_bank_transfer_details.TRANSFER_YEAR = '$serialYear' 
				ORDER BY finance_bank_to_bank_transfer_details.`ORDER_ID` ASC";
 		return $sql;
	}

 	private function getTotAmmount_sql($serialYear,$serialNo,$type)
	{
	  	$sql = "SELECT
				Sum(finance_bank_to_bank_transfer_details.AMOUNT) as amount 
				FROM `finance_bank_to_bank_transfer_details`
				WHERE
				finance_bank_to_bank_transfer_details.TRANSFER_NO = '$serialNo' AND
				finance_bank_to_bank_transfer_details.TRANSFER_YEAR = '$serialYear' AND
				finance_bank_to_bank_transfer_details.TRANSACTION_TYPE = '$type'";
				
 		return $sql;
 	}
 	private function Load_Report_approval_details_sql($serialYear,$serialNo){
		
	   	$sql = "SELECT
				finance_bank_to_bank_transfer_header_approvedby.intApproveUser,
				finance_bank_to_bank_transfer_header_approvedby.dtApprovedDate,
				sys_users.strUserName as UserName,
				finance_bank_to_bank_transfer_header_approvedby.intApproveLevelNo
				FROM
				finance_bank_to_bank_transfer_header_approvedby
				Inner Join sys_users ON finance_bank_to_bank_transfer_header_approvedby.intApproveUser = sys_users.intUserId
				WHERE
				finance_bank_to_bank_transfer_header_approvedby.TRANSFER_NO =  '$serialNo' AND
				finance_bank_to_bank_transfer_header_approvedby.TRANSFER_YEAR =  '$serialYear'      order by dtApprovedDate asc";
				
 		return $sql;
	}
	
	
}

?>
<?php
include_once  $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
include_once  $_SESSION['ROOT_PATH']."class/finance/bank/bankToBankTransfer/cls_bankToBankTransfer_get.php";
include_once  $_SESSION['ROOT_PATH']."class/cls_commonErrorHandeling_get.php";
//ini_set('display_errors', 1); 

$obj_commonErr			= new cls_commonErrorHandeling_get($db);
$obj_common				= new cls_commonFunctions_get($db);
$obj_fin_transfer_get	= new Cls_BankToBankTransfer_Get($db);

$session_userId			= $_SESSION["userId"];
$session_locationId 	= $_SESSION["CompanyID"];
$session_companyId 		= $_SESSION["headCompanyId"];
$programCode			='P0764';

class Cls_BankToBankTransfer_Set
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	
	public function Save($serialYear,$serialNo,$date,$currency,$refferenceNo,$grid)
	{
		global $obj_commonErr;	
		global $obj_common;
		global $obj_fin_transfer_get;
		global $session_userId;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_locationId;
		global $progrmCode;
		
		$savedStatus	= true;
		
		$this->db->begin();
		
		if($serialNo==''){
			$editMode=0;
			$journal_array 	= $obj_common->GetSystemMaxNo('BANK_TO_BANK_TRNS',$session_locationId);
			$serialNo		= $journal_array["max_no"];
			$serialYear		= date('Y');
			$approveLevels 	= $obj_common->getApproveLevels('Bank To Bank transfer');
 			$status			=$approveLevels+1;
		}
		else{
			$editMode=1;
			$header_array 	= $obj_fin_transfer_get->get_header_array2($serialYear,$serialNo);
  			$status			=$header_array['STATUS'];
			$approveLevels 	= $obj_common->getApproveLevels('Bank To Bank transfer');
 			$status			=$approveLevels+1;
		}

		if($journal_array['rollBackFlag']==1){
			$messageErr 		= $journal_array['msg']; 
			$sqlErr				= $journal_array['q'];
			$rollBack=1;
 		}

		if($rollBack != 1){
			if($editMode==0){
				$trans_response=$this->SaveHeader($serialYear,$serialNo,$date,$currency,$refferenceNo,$approveLevels,$status);
 			}
			else{
				$trans_response=$this->UpdateHeader($serialYear,$serialNo,$date,$currency,$refferenceNo,$approveLevels,$status);
			}
 			if($trans_response['savedStatus']=='fail'){
				$messageErr	= $trans_response['savedMasseged']; 
				$sqlErr		= $trans_response['error_sql'];
				$rollBack=1;
			}
		}
		
		$maxAppByStatus=$this->getMaxAppByStatus($serialYear,$serialNo);
		$maxAppByStatus=(int)$maxAppByStatus+1;
		$trans_response=$this->approved_by_update($serialYear,$serialNo,$maxAppByStatus);
		if($trans_response['savedStatus']=='fail'){
			$messageErr	= $trans_response['savedMasseged']; 
			$sqlErr		= $trans_response['error_sql'];
			$rollBack=1;
		}
		
		$trans_response	= $this->DeleteDetails($serialYear,$serialNo);
		if($trans_response['savedStatus']=='fail'){
			$messageErr	= $trans_response['savedMasseged']; 
			$sqlErr		= $trans_response['error_sql'];
			$rollBack=1;
		}
		
		if($rollBack != 1){
			$totAmmCredit	=0;
			$totAmmDebit	=0;
			$i=0;
			foreach($grid as $array_loop)
			{
				$i++;
				
				$account			= $array_loop["account"];
				$credit				= $array_loop["credit"];
				$debit				= $array_loop["debit"];
				$remarks			= $obj_common->replace($array_loop["remarks"]);
				$costCenter			= $array_loop["costCenter"];
 				
				if($credit > 0){
					$type = 'C';
					$ammount		 = $credit;
					$totAmmCredit	+= $ammount;
 				}
				else{
					$type = 'D';
					$ammount		= $debit;
					$totAmmDebit	+= $ammount;
				}
				
				
				$trans_response=$this->SaveDetails($serialYear,$serialNo,$account,$type,$ammount,$remarks,$costCenter,$i);
				if($trans_response['savedStatus']=='fail'){
					$messageErr	= $trans_response['savedMasseged']; 
					$sqlErr		= $trans_response['error_sql'];
					$rollBack=1;
				}
			}
			
			
			
			
			if($totAmmDebit != $totAmmCredit){
					$rollBack	=1;
					$messageErr	= "Total credit ammount not tally with total debit ammount"; 
					$sqlErr		= '';
			}
			else if($i==0){
					$rollBack	=1;
					$messageErr	= "Error with Detail saving"; 
					$sqlErr		= '';
			}
		}
		
		$sysNoArr						= $obj_common->validateDuplicateSerialNoWithSysNo($serialNo,'BANK_TO_BANK_TRNS',$session_locationId);
		if($sysNoArr['type'] == 'fail' && $rollBack == 0)
		{
			$rollBack					= 1;
			$messageErr		 			= $sysNoArr['msg'];
		}
		if($rollBack==1){
			$this->db->rollback();
			$response['type'] 				= "fail";
			$response['msg'] 				= $messageErr;
			$response['sql']				= $sqlErr;
		}
		else if($savedStatus){
			
			$response['type'] 				= "pass";
			if($editMode==1)
			$response['msg'] 				= "Bank To Bank Transfer : '$serialNo/$serialYear' updated successfully.";
			else
			$response['msg'] 				= "Bank To Bank Transfer : '$serialNo/$serialYear' saved successfully.";
			$response_arr					= $obj_commonErr->get_permision_withApproval_confirm($status,$approveLevels,$session_userId,$progrmCode,'RunQuery2');
			$response['permision_confirm'] 	= $response_arr['permision'];
			$response['serialNo'] 			= $serialNo;
			$response['serialYear']			= $serialYear;
			$this->db->commit();
		}else{
			$this->db->rollback();
			$response['type'] 				= "fail";
			$response['msg'] 				= $messageErr;
			$response['sql']				= $sqlErr;
		}
		return json_encode($response);
	}

	public function Approve($serialYear,$serialNo)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $obj_common_get;
		global $session_userId;
		global $obj_fin_transfer_get;
		$savedStatus	= true;
		$this->db->begin();
		
 		
		$trans_response = $this->updateHeaderStatus($serialYear,$serialNo,'');
		if($trans_response['savedStatus']=='fail'){
			$messageErr		= $trans_response['savedMasseged']; 
			$sqlErr			= $trans_response['error_sql'];
			$rollBack=1;
		}
		if($rollBack!=1){
			$header_array	= $obj_fin_transfer_get->get_header_array2($serialYear,$serialNo);
			$status			=$header_array['STATUS'];
			$savedLevels	=$header_array['LEVELS'];
			$approval		=$savedLevels+1-$status;
			
			$trans_response = $this->approved_by_insert($serialYear,$serialNo,$session_userId,$approval);
			if($trans_response['savedStatus']=='fail'){
				$messageErr	= $trans_response['savedMasseged']; 
				$sqlErr		= $trans_response['error_sql'];
				$rollBack=1;
			}
		}
		
		$totAmmCredit	= $obj_fin_transfer_get->getTotAmmount2($serialYear,$serialNo,'C');
		$totAmmDebit	= $obj_fin_transfer_get->getTotAmmount2($serialYear,$serialNo,'D');
		if($totAmmDebit != $totAmmCredit){
				$rollBack=1;
				$messageErr	= "Total credit ammount not tally with total debit ammount"; 
				$sqlErr		= '';
		}
 		$result		= $obj_fin_transfer_get->get_details_array2($serialYear,$serialNo);
   			
		if(($rollBack!=1) && ($status==1)){
			$trans_response = $this->finance_transaction_insert($serialYear,$serialNo);
			if($trans_response['savedStatus']=='fail'){
				$messageErr	= $trans_response['savedMasseged']; 
				$sqlErr		= $trans_response['error_sql'];
				$rollBack=1;
			}
		}
    		
		if($rollBack==1){
			$this->db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= $messageErr;
			$response['sql']		= $sqlErr;
		}
		else if($savedStatus){
			$this->db->commit();
			$response['type'] 		= "pass";
			$header_array = $obj_fin_transfer_get->get_header_array2($serialYear,$serialNo);
			if($header_array['STATUS']==1) 
			$response['msg'] 		= "Final Approval Raised successfully.";
 			else 
			$response['msg'] 		= "Approved successfully.";
 		}else{
			$this->db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= $messageErr;
			$response['sql']		= $sqlErr;
		}
		return json_encode($response);
	}

	public function Reject($serialYear,$serialNo)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $obj_common_get;
		global $session_userId;
		global $obj_fin_transfer_get;
		$savedStatus	= true;
		$this->db->begin();
		
 		
		$trans_response = $this->updateHeaderStatus($serialYear,$serialNo,'0');
		if($trans_response['savedStatus']=='fail'){
			$messageErr	= $trans_response['savedMasseged']; 
			$sqlErr		= $trans_response['error_sql'];
			$rollBack=1;
		}
		if($rollBack!=1){
			$trans_response = $this->approved_by_insert($serialYear,$serialNo,$session_userId,0);
			if($trans_response['savedStatus']=='fail'){
				$messageErr	= $trans_response['savedMasseged']; 
				$sqlErr		= $trans_response['error_sql'];
				$rollBack=1;
			}
		}
   		
		if($rollBack==1){
			$this->db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= $messageErr;
			$response['sql']		= $sqlErr;
		}
		else if($savedStatus){
			$this->db->commit();
			$response['type'] 		= "pass";
 			$response['msg'] 		= "Rejected successfully.";
 		}else{
			$this->db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= $messageErr;
			$response['sql']		= $sqlErr;
		}
		return json_encode($response);
	}

	public function Cancel($serialYear,$serialNo)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $obj_common_get;
		global $session_userId;
		global $obj_fin_transfer_get;
		$savedStatus	= true;
		$this->db->begin();
		
 		
		$trans_response = $this->updateHeaderStatus($serialYear,$serialNo,'-2');
		if($trans_response['savedStatus']=='fail'){
			$messageErr	= $trans_response['savedMasseged']; 
			$sqlErr		= $trans_response['error_sql'];
			$rollBack=1;
		}
  		if($rollBack!=1){
			$trans_response = $this->approved_by_insert($serialYear,$serialNo,$session_userId,-2);
			if($trans_response['savedStatus']=='fail'){
				$messageErr	= $trans_response['savedMasseged']; 
				$sqlErr		= $trans_response['error_sql'];
				$rollBack=1;
			}
		}
		if($rollBack!=1){
			$trans_response = $this->finance_transaction_delete($serialYear,$serialNo);
			if($trans_response['savedStatus']=='fail'){
				$messageErr	= $trans_response['savedMasseged']; 
				$sqlErr		= $trans_response['error_sql'];
				$rollBack=1;
			}
		}
		
   		
		if($rollBack==1){
			$this->db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= $messageErr;
			$response['sql']		= $sqlErr;
		}
		else if($savedStatus){
			$this->db->commit();
			$response['type'] 		= "pass";
 			$response['msg'] 		= "Cancelled successfully.";
 		}else{
			$this->db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= $messageErr;
			$response['sql']		= $sqlErr;
		}
		return json_encode($response);
	}
 	private function SaveHeader($serialYear,$serialNo,$date,$currency,$refferenceNo,$approveLevels,$status)
	{
		global $session_userId;
		global $session_locationId;
		global $session_companyId;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "INSERT INTO finance_bank_to_bank_transfer_header 
				(TRANSFER_NO, 
				TRANSFER_YEAR,
				CURRENCY_ID,
 				REFERENCE_NO, 
 				TRANSFER_DATE, 
 				COMPANY_ID, 
 				LOCATION_ID, 
 				STATUS, 
 				SAVE_LEVELS, 
				CREATED_BY,
				CREATED_DATE)
				VALUES
				('$serialNo', 
				'$serialYear',
				'$currency',
 				'$refferenceNo',
 				'$date',
 				'$session_companyId',
 				'$session_locationId',
				'$status',
 				'$approveLevels',
 				'$session_userId', 
				NOW());";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$data['savedStatus']	= 'fail';
			$data['savedMasseged']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	
	private function UpdateHeader($serialYear,$serialNo,$date,$currency,$refferenceNo,$approveLevels,$status)
	{
		global $session_userId;
		global $session_locationId;
		global $session_companyId;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "UPDATE finance_bank_to_bank_transfer_header 
				SET 
 				CURRENCY_ID='$currency',
 				REFERENCE_NO='$refferenceNo', 
 				TRANSFER_DATE='$date', 
 				COMPANY_ID='$session_companyId', 
 				LOCATION_ID='$session_locationId', 
  				SAVE_LEVELS='$approveLevels',
				STATUS='$status',
				MODIFIED_BY='$session_userId',
				MODIFIED_DATE=NOW()
				WHERE
				finance_bank_to_bank_transfer_header.TRANSFER_NO = '$serialNo' AND
				finance_bank_to_bank_transfer_header.TRANSFER_YEAR = '$serialYear'";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$data['savedStatus']	= 'fail';
			$data['savedMasseged']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	
	private function DeleteDetails($serialYear,$serialNo)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql	= "DELETE 
					FROM finance_bank_to_bank_transfer_details 
					WHERE
					finance_bank_to_bank_transfer_details.TRANSFER_NO = '$serialNo' AND
					finance_bank_to_bank_transfer_details.TRANSFER_YEAR = '$serialYear'";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$data['savedStatus']		= 'fail';
			$data['savedMasseged']		= $this->db->errormsg;
			$data['error_sql']			= $sql;
		}
		return $data;
	}
	
	private function SaveDetails($serialYear,$serialNo,$account,$type,$ammount,$remarks,$costCenter,$order)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql	= "INSERT INTO finance_bank_to_bank_transfer_details 
					(TRANSFER_NO, 
					TRANSFER_YEAR, 
					CHART_OF_ACCOUNT_ID,
					AMOUNT,
					BAL_AMOUNT,
					TRANSACTION_TYPE,
					REMARKS,
					COST_CENTER_ID,
					ORDER_ID)
					VALUES
					('$serialNo', 
					'$serialYear', 
					'$account',
					'$ammount',
					'$ammount',
					'$type',
					'$remarks',
					'$costCenter',
					'$order');";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$data['savedStatus']	= 'fail';
			$data['savedMasseged']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	
	function updateHeaderStatus($serialYear,$serialNo,$status){
		global $session_userId;
		global $session_locationId;
		global $session_companyId;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
 		
		if($status=='') 
			$para='STATUS-1';
 		else
			$para=$status;
		
		$sql = "UPDATE finance_bank_to_bank_transfer_header 
				SET 
				STATUS=".$para." 
				WHERE
				finance_bank_to_bank_transfer_header.TRANSFER_NO = '$serialNo' AND
				finance_bank_to_bank_transfer_header.TRANSFER_YEAR = '$serialYear'";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$data['savedStatus']		= 'fail';
			$data['savedMasseged']		= $this->db->errormsg;
			$data['error_sql']			= $sql;
		}
		return $data;
	}

	private function approved_by_insert($serialYear,$serialNo,$userId,$approval){
		global $session_userId;
		global $session_locationId;
		global $session_companyId;
		global $savedStatus;
		global $error_sql;
		global $savedMasseged;
		
		$sql = "INSERT INTO `finance_bank_to_bank_transfer_header_approvedby` (`TRANSFER_NO`,`TRANSFER_YEAR`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
			VALUES ('$serialNo','$serialYear','$approval','$userId',now(),0)";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$data['savedStatus']		= 'fail';
			$data['savedMasseged']		= $this->db->errormsg;
			$data['error_sql']			= $sql;
		}
		return $data;
	}
	
	private function getMaxAppByStatus($serialYear,$serialNo){
 		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
	   $sql = "SELECT
				max(finance_bank_to_bank_transfer_header_approvedby.intStatus) as status 
				FROM
				finance_bank_to_bank_transfer_header_approvedby
				WHERE
				finance_bank_to_bank_transfer_header_approvedby.TRANSFER_NO =  '$serialNo' AND
				finance_bank_to_bank_transfer_header_approvedby.TRANSFER_YEAR =  '$serialYear'";
				
		$resultm	= $this->db->RunQuery2($sql);
		 $rowm		=mysqli_fetch_array($resultm);
		 
 			 return $rowm['status'];
 	}
	
	private function approved_by_update($serialYear,$serialNo,$maxAppByStatus){
 		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
			$sql = "UPDATE `finance_bank_to_bank_transfer_header_approvedby` SET intStatus ='$maxAppByStatus' 
					WHERE (`TRANSFER_NO`='$serialNo') AND (`TRANSFER_YEAR`='$serialYear') AND (`intStatus`='0')";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$data['savedStatus']	= 'fail';
			$data['savedMasseged']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}

	private function update_datail_balance($serialNo,$serialYear,$orderId,$ammount){
 		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "UPDATE `finance_bank_to_bank_transfer_details` SET BAL_AMOUNT =BAL_AMOUNT - '$ammount' 
					WHERE (`TRANSFER_NO`='$serialNo') AND (`TRANSFER_YEAR`='$serialYear') AND (`ORDER_ID`='$orderId')";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$data['savedStatus']	= 'fail';
			$data['savedMasseged']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	
	private function finance_transaction_insert($serialYear,$serialNo){
		global $session_userId;
		global $session_locationId;
		global $session_companyId;
		global $savedStatus;
		global $error_sql;
		global $savedMasseged;
		
		$sql1 = "SELECT
					finance_bank_to_bank_transfer_details.TRANSFER_NO,
					finance_bank_to_bank_transfer_details.TRANSFER_YEAR,
					finance_bank_to_bank_transfer_details.CHART_OF_ACCOUNT_ID,
					finance_bank_to_bank_transfer_details.AMOUNT,
					finance_bank_to_bank_transfer_details.TRANSACTION_TYPE,
					finance_bank_to_bank_transfer_details.REMARKS,
					finance_bank_to_bank_transfer_details.COST_CENTER_ID,
					finance_bank_to_bank_transfer_header.REFERENCE_NO,
					finance_bank_to_bank_transfer_header.CURRENCY_ID,
					finance_bank_to_bank_transfer_header.TRANSFER_DATE
					FROM
					finance_bank_to_bank_transfer_details
					INNER JOIN finance_bank_to_bank_transfer_header ON finance_bank_to_bank_transfer_details.TRANSFER_NO = finance_bank_to_bank_transfer_header.TRANSFER_NO AND finance_bank_to_bank_transfer_details.TRANSFER_YEAR = finance_bank_to_bank_transfer_header.TRANSFER_YEAR
					WHERE
					finance_bank_to_bank_transfer_details.TRANSFER_NO = '$serialNo' AND
					finance_bank_to_bank_transfer_details.TRANSFER_YEAR = '$serialYear'";
		$result1 = $this->db->RunQuery2($sql1);		
		while($row=mysqli_fetch_array($result1))
		{
			$account		= $row['CHART_OF_ACCOUNT_ID'];
			$ammount		= $row['AMOUNT'];
 			$transType		= $row['TRANSACTION_TYPE'];
 			$transCat		= 'BT';
			$docType		= 'BANK_TRANSFER';
 			$refNo			= $row['REFERENCE_NO'];
			$curr			= $row['CURRENCY_ID'];
			$entryDate		= $row["TRANSFER_DATE"];
 			
			$sqlI = "INSERT INTO `finance_transaction` 
						(`CHART_OF_ACCOUNT_ID`,
						`AMOUNT`,
						`DOCUMENT_NO`,
						DOCUMENT_YEAR,
						DOCUMENT_TYPE,
						TRANSACTION_TYPE,
						TRANSACTION_CATEGORY,
						BANK_REFERENCE_NO,
						CURRENCY_ID,
						LOCATION_ID,
						COMPANY_ID,
						LAST_MODIFIED_BY,
						LAST_MODIFIED_DATE) 
					VALUES ('$account',
						'$ammount',
						'$serialNo',
						'$serialYear',
						'$docType',
						'$transType',
						'$transCat',
						'$refNo',
						'$curr',
						'$session_locationId',
						'$session_companyId',
						'$session_userId',
						'$entryDate')";
			$result = $this->db->RunQuery2($sqlI);
			if(!$result){
				$data['savedStatus']		= 'fail';
				$data['savedMasseged']		= $this->db->errormsg;
				$data['error_sql']			= $sqlI;
			}
		}
		return $data;
	}
	private function finance_transaction_delete($serialYear,$serialNo){
		global $savedStatus;
		global $error_sql;
		global $savedMasseged;
	
		 $sql1 = "DELETE 
					FROM
					finance_transaction
 					WHERE
					finance_transaction.DOCUMENT_NO = '$serialNo' AND
					finance_transaction.DOCUMENT_YEAR = '$serialYear' AND 
					finance_transaction.DOCUMENT_TYPE = 'BANK_TRANSFER'";
		$result1 = $this->db->RunQuery2($sql1);
		
			if(!$result1){
				$data['savedStatus']	= 'fail';
				$data['savedMasseged']	= $this->db->errormsg;
				$data['error_sql']		= $sql1;
			}
		return $data;
	}

 }
?>
<?php
class cls_cash_flow_forecast_set
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	} 
	
	public function UpdateFromAmount($fromDate,$id,$cusAmount,$companyId,$tblType)
	{
		return $this->UpdateFromAmount_sql($fromDate,$id,$cusAmount,$companyId,$tblType);
	}
	
	public function UpdateCustomerForcast($toDate,$id,$cusAmount,$companyId,$tblType)
	{
		return $this->UpdateCustomerForcast_sql($toDate,$id,$cusAmount,$companyId,$tblType);
	}
	
	public function UpdateCustomerForcastMinus($toDate,$id,$cusAmount,$companyId,$tblType,$currencyId,$bankId)
	{
		return $this->UpdateCustomerForcastMinus_sql($toDate,$id,$cusAmount,$companyId,$tblType,$currencyId,$bankId);
	}
	
	public function InsertCustomerForcast($toDate,$id,$cusAmount,$companyId,$tblType)
	{
		return $this->InsertCustomerForcast_sql($toDate,$id,$cusAmount,$companyId,$tblType);
	}
	
	private function UpdateFromAmount_sql($fromDate,$id,$cusAmount,$companyId,$tblType)
	{
		
		$sql = "UPDATE $tblType
				SET BALANCE_AMOUNT = BALANCE_AMOUNT - $cusAmount
				WHERE DATE = '$fromDate'
					AND CATEGORY_ID = '$id'
					AND COMPANY_ID = '$companyId';";					
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$responce['type']	= false;
			$responce['msg']	= $this->db->errormsg;
			$responce['sql']	= $sql;
		}
		else
			$responce['type']	= true;
		return $responce;
	}
	
	private function UpdateCustomerForcast_sql($toDate,$id,$cusAmount,$companyId,$tblType)
	{
		$sql = "UPDATE $tblType
				SET AMOUNT = AMOUNT + $cusAmount,
				  BALANCE_AMOUNT = BALANCE_AMOUNT + $cusAmount
				WHERE DATE = '$toDate' 
					AND CATEGORY_ID = '$id'
					AND COMPANY_ID = '$companyId';";
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$responce['type']	= false;
			$responce['msg']	= $this->db->errormsg;
			$responce['sql']	= $sql;
		}
		else
			$responce['type']	= true;
		return $responce;
	}
	
	private function UpdateCustomerForcastMinus_sql($toDate,$id,$cusAmount,$companyId,$tblType,$currencyId,$bankId)
	{
		$sql = "UPDATE $tblType
				SET BALANCE_AMOUNT = ROUND(BALANCE_AMOUNT - $cusAmount,2)
				WHERE DATE = '$toDate' 
					AND CATEGORY_ID = '$id'
					AND CURRENCY_ID = '$currencyId'
					AND BANK_ACCOUNT_ID = '$bankId'
					AND COMPANY_ID = '$companyId';";
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$responce['type']	= false;
			$responce['msg']	= $this->db->errormsg;
			$responce['sql']	= $sql;
		}
		else
			$responce['type']	= true;
		return $responce;
	}
	
	private function InsertCustomerForcast_sql($toDate,$id,$cusAmount,$companyId,$tblType)
	{
		$sql = "INSERT INTO $tblType 
					(DATE, 
					COMPANY_ID,
					CATEGORY_ID,
					CURRENCY_ID,
					AMOUNT, 
					BALANCE_AMOUNT)
					VALUES
					('$toDate', 
					'$companyId',
					'$id',
					'1',
					'$cusAmount', 
					'$cusAmount');";
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$responce['type']	= false;
			$responce['msg']	= $this->db->errormsg;
			$responce['sql']	= $sql;
		}
		else
			$responce['type']	= true;
		return $responce;
	}
}
?>
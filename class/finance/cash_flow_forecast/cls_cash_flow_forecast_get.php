<?php
class cls_cash_flow_forecast_get
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	} 
	
	public function IsRowAvailable($toDate,$id,$companyId,$tblType)
	{
		return $this->IsRowAvailable_sql($toDate,$id,$companyId,$tblType);
	}
	
	private function IsRowAvailable_sql($toDate,$id,$companyId,$tblType)
	{
		$sql = "SELECT 
				COUNT(*) AS COUNT 
			FROM $tblType 
			WHERE 
					DATE = '$toDate'
				AND CATEGORY_ID = $id 
				AND COMPANY_ID = $companyId";
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		return $row["COUNT"];
	}
}
?>
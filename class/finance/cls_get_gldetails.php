<?php
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];
class Cls_Get_GLDetails
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	
	public function getGLCombo($type,$GLId)
	{		
		$html = $this->GLCombo_HTML($type,$GLId);
		return $html;
	}
	public function getAdvanceGLCombo($type,$GLId)
	{		
		$html = $this->Advance_GLCombo_HTML($type,$GLId);
		return $html;
	}
	public function getBankGLCombo($GLId)
	{		
		$html = $this->bank_GLCombo_HTML($GLId);
		return $html;
	}
	public function getSubCatGLArr($type,$subCat)
	{
		global $db;
		global $session_companyId;
		
		$sql = "SELECT FCOA.CHART_OF_ACCOUNT_ID
				FROM finance_mst_chartofaccount FCOA
				LEFT JOIN finance_mst_chartofaccount_company FCOAC ON FCOA.CHART_OF_ACCOUNT_ID=FCOAC.CHART_OF_ACCOUNT_ID
				LEFT JOIN finance_mst_chartofaccount_subcategory FCOAS ON FCOA.CHART_OF_ACCOUNT_ID=FCOAS.CHART_OF_ACCOUNT_ID
				LEFT JOIN finance_mst_chartofaccount_tax FCOAT ON FCOA.CHART_OF_ACCOUNT_ID=FCOAT.CHART_OF_ACCOUNT_ID
				WHERE FCOAC.COMPANY_ID='$session_companyId' AND 
				FCOA.$type='1' AND 
				FCOAS.SUBCATEGORY_ID='$subCat' ";
		//echo($sql);		
		$result = $this->db->RunQuery($sql);
		while($row = mysqli_fetch_array($result))
		{
			$data['GLAccountId'] = $row['CHART_OF_ACCOUNT_ID'];
			$data['subCatId'] 	 = $subCat;
			$arrGLData[] 		 = $data;
		}
		return $arrGLData;
	}
	public function getTaxGLArr($type,$taxId)
	{
		global $db;
		global $session_companyId;
		
		$sql = "SELECT FCOA.CHART_OF_ACCOUNT_ID
				FROM finance_mst_chartofaccount FCOA
				LEFT JOIN finance_mst_chartofaccount_company FCOAC ON FCOA.CHART_OF_ACCOUNT_ID=FCOAC.CHART_OF_ACCOUNT_ID
				LEFT JOIN finance_mst_chartofaccount_subcategory FCOAS ON FCOA.CHART_OF_ACCOUNT_ID=FCOAS.CHART_OF_ACCOUNT_ID
				LEFT JOIN finance_mst_chartofaccount_tax FCOAT ON FCOA.CHART_OF_ACCOUNT_ID=FCOAT.CHART_OF_ACCOUNT_ID
				WHERE FCOAC.COMPANY_ID='$session_companyId' AND 
				FCOA.$type='1' AND 
				FCOAT.TAX_ID='$taxId' ";
		//echo($sql);		
		$result = $this->db->RunQuery($sql);
		while($row = mysqli_fetch_array($result))
		{
			$data['GLAccountId'] = $row['CHART_OF_ACCOUNT_ID'];
			$data['taxId'] 		 = $taxId;
			$arrGLData[] 		 = $data;
		}
		return $arrGLData;

	}
	private function GLCombo_HTML($type,$GLId)
	{
		global $session_companyId;
		
 		$sql = "select * FROM ((
					SELECT FCOA.CHART_OF_ACCOUNT_ID,
					CONCAT(FMT.FINANCE_TYPE_CODE,'',FMMT.MAIN_TYPE_CODE,'',FMST.SUB_TYPE_CODE,'',FCOA.CHART_OF_ACCOUNT_CODE) AS accountCode,
					FCOA.CHART_OF_ACCOUNT_NAME
					FROM finance_mst_chartofaccount FCOA
					INNER JOIN finance_mst_account_sub_type FMST ON FMST.SUB_TYPE_ID=FCOA.SUB_TYPE_ID
					INNER JOIN finance_mst_account_main_type FMMT ON FMMT.MAIN_TYPE_ID=FMST.MAIN_TYPE_ID
					INNER JOIN finance_mst_account_type FMT ON FMT.FINANCE_TYPE_ID=FMMT.FINANCE_TYPE_ID
					INNER JOIN finance_mst_chartofaccount_company FCOAC ON FCOAC.CHART_OF_ACCOUNT_ID=FCOA.CHART_OF_ACCOUNT_ID
					WHERE FCOA.$type='1' AND FCOAC.COMPANY_ID='$session_companyId' AND FCOA.CATEGORY_TYPE='N' AND FCOA.STATUS=1

 				)
				UNION
				(
					SELECT FCOA.CHART_OF_ACCOUNT_ID,
					CONCAT(FMT.FINANCE_TYPE_CODE,'',FMMT.MAIN_TYPE_CODE,'',FMST.SUB_TYPE_CODE,'',FCOA.CHART_OF_ACCOUNT_CODE) AS accountCode,
					FCOA.CHART_OF_ACCOUNT_NAME
					FROM finance_mst_chartofaccount FCOA
					INNER JOIN finance_mst_account_sub_type FMST ON FMST.SUB_TYPE_ID=FCOA.SUB_TYPE_ID
					INNER JOIN finance_mst_account_main_type FMMT ON FMMT.MAIN_TYPE_ID=FMST.MAIN_TYPE_ID
					INNER JOIN finance_mst_account_type FMT ON FMT.FINANCE_TYPE_ID=FMMT.FINANCE_TYPE_ID
					INNER JOIN finance_mst_chartofaccount_company FCOAC ON FCOAC.CHART_OF_ACCOUNT_ID=FCOA.CHART_OF_ACCOUNT_ID
					WHERE FCOA.CHART_OF_ACCOUNT_ID='$GLId'  
 				)) as tb1 ORDER BY CHART_OF_ACCOUNT_NAME ";
		
		
 		
		$result = $this->db->RunQuery($sql);
		$string ="<option value=\"\"></option>";
		while($row = mysqli_fetch_array($result))
		{
			if($row['CHART_OF_ACCOUNT_ID']==$GLId)
				$string.="<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\" selected=\"selected\">".$row['CHART_OF_ACCOUNT_NAME']." - ".$row['accountCode']."</option>";
			else
				$string.="<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\">".$row['CHART_OF_ACCOUNT_NAME']." - ".$row['accountCode']."</option>";
		}
		return $string;
	}
	private function Advance_GLCombo_HTML($type,$GLId)
	{
		global $session_companyId;
		
		$sql = "select * FROM ((
					SELECT FCOA.CHART_OF_ACCOUNT_ID,
					CONCAT(FMT.FINANCE_TYPE_CODE,'',FMMT.MAIN_TYPE_CODE,'',FMST.SUB_TYPE_CODE,'',FCOA.CHART_OF_ACCOUNT_CODE) AS accountCode,
					FCOA.CHART_OF_ACCOUNT_NAME
					FROM finance_mst_chartofaccount FCOA
					INNER JOIN finance_mst_account_sub_type FMST ON FMST.SUB_TYPE_ID=FCOA.SUB_TYPE_ID
					INNER JOIN finance_mst_account_main_type FMMT ON FMMT.MAIN_TYPE_ID=FMST.MAIN_TYPE_ID
					INNER JOIN finance_mst_account_type FMT ON FMT.FINANCE_TYPE_ID=FMMT.FINANCE_TYPE_ID
					INNER JOIN finance_mst_chartofaccount_company FCOAC ON FCOAC.CHART_OF_ACCOUNT_ID=FCOA.CHART_OF_ACCOUNT_ID
					WHERE FCOA.$type='1' AND FCOA.BANK='1' AND FCOAC.COMPANY_ID='$session_companyId' AND FCOA.CATEGORY_TYPE='N' AND FCOA.STATUS=1
  				)
				UNION
				(
					SELECT FCOA.CHART_OF_ACCOUNT_ID,
					CONCAT(FMT.FINANCE_TYPE_CODE,'',FMMT.MAIN_TYPE_CODE,'',FMST.SUB_TYPE_CODE,'',FCOA.CHART_OF_ACCOUNT_CODE) AS accountCode,
					FCOA.CHART_OF_ACCOUNT_NAME
					FROM finance_mst_chartofaccount FCOA
					INNER JOIN finance_mst_account_sub_type FMST ON FMST.SUB_TYPE_ID=FCOA.SUB_TYPE_ID
					INNER JOIN finance_mst_account_main_type FMMT ON FMMT.MAIN_TYPE_ID=FMST.MAIN_TYPE_ID
					INNER JOIN finance_mst_account_type FMT ON FMT.FINANCE_TYPE_ID=FMMT.FINANCE_TYPE_ID
					INNER JOIN finance_mst_chartofaccount_company FCOAC ON FCOAC.CHART_OF_ACCOUNT_ID=FCOA.CHART_OF_ACCOUNT_ID
					WHERE FCOA.CHART_OF_ACCOUNT_ID='$GLId'  
 				)) as tb1 ORDER BY CHART_OF_ACCOUNT_NAME ";
				
 		$result = $this->db->RunQuery($sql);
		$string ="<option value=\"\"></option>";
		while($row = mysqli_fetch_array($result))
		{
			if($row['CHART_OF_ACCOUNT_ID']==$GLId)
				$string.="<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\" selected=\"selected\">".$row['CHART_OF_ACCOUNT_NAME']." - ".$row['accountCode']."</option>";
			else
				$string.="<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\">".$row['CHART_OF_ACCOUNT_NAME']." - ".$row['accountCode']."</option>";
		}
		return $string;
	}
	private function bank_GLCombo_HTML($GLId)
	{
		global $session_companyId;
		
		$sql = "select * FROM ((
					SELECT FCOA.CHART_OF_ACCOUNT_ID,
					CONCAT(FMT.FINANCE_TYPE_CODE,'',FMMT.MAIN_TYPE_CODE,'',FMST.SUB_TYPE_CODE,'',FCOA.CHART_OF_ACCOUNT_CODE) AS accountCode,
					FCOA.CHART_OF_ACCOUNT_NAME
					FROM finance_mst_chartofaccount FCOA
					INNER JOIN finance_mst_account_sub_type FMST ON FMST.SUB_TYPE_ID=FCOA.SUB_TYPE_ID
					INNER JOIN finance_mst_account_main_type FMMT ON FMMT.MAIN_TYPE_ID=FMST.MAIN_TYPE_ID
					INNER JOIN finance_mst_account_type FMT ON FMT.FINANCE_TYPE_ID=FMMT.FINANCE_TYPE_ID
					INNER JOIN finance_mst_chartofaccount_company FCOAC ON FCOAC.CHART_OF_ACCOUNT_ID=FCOA.CHART_OF_ACCOUNT_ID
					WHERE FCOA.BANK='1' AND FCOAC.COMPANY_ID='$session_companyId' AND FCOA.CATEGORY_TYPE='N' AND FCOA.STATUS=1
  				)
				UNION
				(
					SELECT FCOA.CHART_OF_ACCOUNT_ID,
					CONCAT(FMT.FINANCE_TYPE_CODE,'',FMMT.MAIN_TYPE_CODE,'',FMST.SUB_TYPE_CODE,'',FCOA.CHART_OF_ACCOUNT_CODE) AS accountCode,
					FCOA.CHART_OF_ACCOUNT_NAME
					FROM finance_mst_chartofaccount FCOA
					INNER JOIN finance_mst_account_sub_type FMST ON FMST.SUB_TYPE_ID=FCOA.SUB_TYPE_ID
					INNER JOIN finance_mst_account_main_type FMMT ON FMMT.MAIN_TYPE_ID=FMST.MAIN_TYPE_ID
					INNER JOIN finance_mst_account_type FMT ON FMT.FINANCE_TYPE_ID=FMMT.FINANCE_TYPE_ID
					INNER JOIN finance_mst_chartofaccount_company FCOAC ON FCOAC.CHART_OF_ACCOUNT_ID=FCOA.CHART_OF_ACCOUNT_ID
					WHERE FCOA.CHART_OF_ACCOUNT_ID='$GLId'  
 				)) as tb1 ORDER BY CHART_OF_ACCOUNT_NAME ";
				
		$result = $this->db->RunQuery($sql);
		$string ="<option value=\"\"></option>";
		while($row = mysqli_fetch_array($result))
		{
			if($row['CHART_OF_ACCOUNT_ID']==$GLId)
				$string.="<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\" selected=\"selected\">".$row['CHART_OF_ACCOUNT_NAME']." - ".$row['accountCode']."</option>";
			else
				$string.="<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\">".$row['CHART_OF_ACCOUNT_NAME']." - ".$row['accountCode']."</option>";
		}
		return $string;
	}
}
?>
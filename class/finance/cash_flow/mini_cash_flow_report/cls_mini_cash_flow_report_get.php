<?php
class cls_mini_cash_flow_report_get
{
	private $db;
		
	function __construct($db)
	{
		$this->db = $db;	
	}

//BEGIN - PUBLIC FUNCTION {
	public function getPayment($date,$id,$location)
	{
		return $this->getPayment_sql($date,$id,$location);
	}
	
	public function getPaymentDetails($date,$id,$location)
	{
		 return $this->getPaymentDetails_sql($date,$id,$location);
	}
	
	public function getFinanceCashFlow($date,$id,$location)
	{
		return $this->getFinanceCashFlow_sql($date,$id,$location);
	}
	
	public function getFinanceCashFlowLog($date,$id,$date1,$location)
	{
		return $this->getFinanceCashFlowLog_sql($date,$id,$date1,$location);
	}
	
	public function getCashFlowItemName($id)
	{
		return $this->getCashFlowItemName_sql($id);
	}
	
	public function getCpanalData($location)
	{
		return $this->getCpanalData_sql($location);
	}
	
	public function getForcastBalance($receiveDate,$id,$location)
	{
		return $this->getForcastBalance_sql($receiveDate,$id,$location);
	}
	
	public function isDataAvailable($date,$receiveDate,$id,$location,$type)
	{
		return $this->isDataAvailable_sql($date,$receiveDate,$id,$location,$type);
	}
	
	public function getInvoiceDate($date,$receiveDate,$id,$location)
	{
		return $this->getInvoiceDate_sql($date,$receiveDate,$id,$location);
	}
	
	public function getPaymentAmount($invoiceDate,$id,$location)
	{
		return $this->getPaymentAmount_sql($invoiceDate,$id,$location);
	}
	
	public function getForcastBalanceDetails($receiveDate,$id,$date,$location)
	{
		return $this->getForcastBalanceDetails_sql($receiveDate,$id,$date,$location);
	}
	
	public function getInvoicePaymentDetails($invoiceDate,$id,$location)
	{
		return $this->getInvoicePaymentDetails_sql($invoiceDate,$id,$location);
	}
//END 	- PUBLIC FUNCTION }

//BEGIN - PRIVATE FUNCTIONS {
	private function getPayment_sql($date,$id,$location)
	{
		$sql = "SELECT
				  COALESCE(SUM(AMOUNT),0)	AS AMOUNT
				FROM finance_cashflow_log
				WHERE 
				REPORT_FIELD_ID = $id
				AND LOCATION_ID = '$location'
				AND DATE(LOG_DATE) = '$date'
				AND DOCUMENT_TYPE IN ('BILL_PAYMENT','SUPPLIER_PAYMENT','BANK_PAYMENT','ADV_PAY_SETTLE')";//echo $sql;
		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		return $row["AMOUNT"];
	}
	
	private function getPaymentDetails_sql($date,$id,$location)
	{
		$sql = "SELECT
				  DOCUMENT_TYPE			AS DOCUMENT_TYPE,
				  RECEIVE_DATE			AS RECEIVE_DATE,
				  LOG_DATE				AS LOG_DATE,
				  COALESCE(AMOUNT,0)	AS AMOUNT
				FROM finance_cashflow_log
				WHERE 
				REPORT_FIELD_ID = $id
				AND LOCATION_ID = '$location'
				AND DATE(LOG_DATE) = '$date'
				AND DOCUMENT_TYPE IN ('BILL_PAYMENT','SUPPLIER_PAYMENT','BANK_PAYMENT','ADV_PAY_SETTLE')";//echo $sql;
		return $this->db->RunQuery($sql);
	}
	
	private function getFinanceCashFlow_sql($date,$id,$location)
	{
		$sql 	= "SELECT DISTINCT DATE(LOG_DATE),RECEIVE_DATE
					FROM finance_cashflow_log L
					WHERE LOCATION_ID = $location
						AND REPORT_FIELD_ID = $id
						AND DATE(LOG_DATE) = '$date'
					ORDER BY RECEIVE_DATE;";// echo $sql;
		return $this->db->RunQuery($sql);
	}
	
	private function getFinanceCashFlowLog_sql($date,$id,$date1,$location)
	{
		
		$sql 	= "SELECT *
					FROM finance_cashflow_log L
					WHERE LOCATION_ID = $location
						AND REPORT_FIELD_ID = $id
						AND RECEIVE_DATE = '$date'
						-- AND DATE(LOG_DATE) <= '$date1' ";//echo $sql;
		return $this->db->RunQuery($sql);
	}
	
	private function getCashFlowItemName_sql($id)
	{
		$sql = "SELECT 
					REPORT_FIELD_NAME
				FROM finance_cashflow_cpanal 
				WHERE REPORT_FIELD_ID = $id";
		$result = $this->db->RunQuery($sql);
		return mysqli_fetch_array($result);
	}
	
	private function getCpanalData_sql($location)
	{
		if($location != "")
			$para = "AND I.LOCATION_ID = $location ";
		
		$sql = "SELECT
					CP.REPORT_FIELD_ID,
					CP.REPORT_FIELD_CODE, 
					CP.REPORT_FIELD_NAME,
					CP.BOO_PO_RAISED
				FROM 
					finance_cashflow_cpanal CP
				INNER JOIN finance_cashflow_cpanal_location CL
					ON CL.REPORT_FIELD_ID = CP.REPORT_FIELD_ID
				WHERE CL.LOCATION_ID = $location
					AND CL.STATUS = 1
				ORDER BY ORDER_BY";
		return $this->db->RunQuery($sql);
	}
	
	private function getForcastBalance_sql($receiveDate,$id,$location)
	{
		$sql = "SELECT  AMOUNT - REVISE AS AMOUNT
				FROM 
					(SELECT COALESCE(SUM(L.AMOUNT),0) AS AMOUNT
					FROM finance_cashflow_log L
					WHERE L.REPORT_FIELD_ID = $id
						AND L.RECEIVE_DATE = '$receiveDate'
						AND L.LOCATION_ID = $location
						AND L.DOCUMENT_TYPE IN ('OPENING_FIXED','OPENING_PREVIOUS')) AS AMOUNT,
					(SELECT COALESCE(SUM(L.AMOUNT),0) AS REVISE
					FROM finance_cashflow_log L
					WHERE L.REPORT_FIELD_ID = $id
						AND L.RECEIVE_DATE = '$receiveDate'
						AND L.LOCATION_ID = $location
						AND L.DOCUMENT_TYPE IN ('OPENING_FIXED_REVISE','INVOICE','BILLINVOICE','JOURNAL_ENTRY'))AS REVISE;";// echo $sql;
		$result = $this->db->RunQuery($sql);
		$row 	= mysqli_fetch_array($result);
		return $row["AMOUNT"];
	}
	
	private function isDataAvailable_sql($date,$receiveDate,$id,$location,$type)
	{
		$sql = "SELECT
				  COUNT(*)	AS ROW_COUNT
				FROM finance_cashflow_log L
				WHERE L.REPORT_FIELD_ID = $id
					AND L.RECEIVE_DATE = '$receiveDate'
					AND L.LOG_DATE = '$date'					
					AND L.LOCATION_ID = $location
					AND L.DOCUMENT_TYPE IN($type);";//echo $sql;
		$result = $this->db->RunQuery($sql);
		$rowA	= mysqli_fetch_array($result);
		if($rowA["ROW_COUNT"]>0)
			return true;
		else
			return false;
	}
	
	private function getInvoiceDate_sql($date,$receiveDate,$id,$location)
	{
		$sql = "SELECT
				  RECEIVE_DATE,INVOICE_DATE,LOG_DATE,DOCUMENT_TYPE,
				  COALESCE(AMOUNT,0) AS AMOUNT
				FROM finance_cashflow_log L
				WHERE L.REPORT_FIELD_ID = $id
					AND L.RECEIVE_DATE = '$receiveDate'
					AND L.LOG_DATE = '$date'
					AND L.LOCATION_ID = $location
					AND L.DOCUMENT_TYPE IN('BILLINVOICE','INVOICE','JOURNAL_ENTRY');";//echo $sql;
		return $this->db->RunQuery($sql);
	}
	
	private function getPaymentAmount_sql($invoiceDate,$id,$location)
	{
		$sql = "SELECT
				  COALESCE(SUM(AMOUNT),0) AS AMOUNT
				FROM finance_cashflow_log L
				WHERE L.REPORT_FIELD_ID = $id
					AND L.INVOICE_DATE = '$invoiceDate'
					AND L.LOCATION_ID = $location
					AND L.DOCUMENT_TYPE IN('BILL_PAYMENT','BANK_PAYMENT','SUPPLIER_PAYMENT','ADV_PAY_SETTLE')";
		$result = $this->db->RunQuery($sql);
		$row1	= mysqli_fetch_array($result);
		return $row1["AMOUNT"];
	}
	
	private function getForcastBalanceDetails_sql($receiveDate,$id,$date,$location)
	{
		$sql = "(SELECT RECEIVE_DATE,INVOICE_DATE,LOG_DATE,DOCUMENT_TYPE,AMOUNT
				FROM finance_cashflow_log L
				WHERE L.REPORT_FIELD_ID = $id
					AND L.RECEIVE_DATE = '$receiveDate'
					AND L.LOCATION_ID = $location
					AND L.DOCUMENT_TYPE IN('OPENING_FIXED','OPENING_PREVIOUS'))
				UNION 
				(SELECT
				RECEIVE_DATE,INVOICE_DATE,LOG_DATE,DOCUMENT_TYPE,AMOUNT*-1
				FROM finance_cashflow_log L
				WHERE L.REPORT_FIELD_ID = $id
					AND L.RECEIVE_DATE = '$receiveDate'
					AND L.LOCATION_ID = $location
					AND L.DOCUMENT_TYPE IN('OPENING_FIXED_REVISE','INVOICE','BILLINVOICE','JOURNAL_ENTRY'))";
		return $this->db->RunQuery($sql);
	}
	
	private function getInvoicePaymentDetails_sql($invoiceDate,$id,$location)
	{
		$sql = "SELECT
				  RECEIVE_DATE,INVOICE_DATE,LOG_DATE,DOCUMENT_TYPE,AMOUNT
				FROM finance_cashflow_log L
				WHERE L.REPORT_FIELD_ID = $id
					AND L.INVOICE_DATE = '$invoiceDate'
					AND L.LOCATION_ID = $location
					AND L.DOCUMENT_TYPE IN('BILL_PAYMENT','BANK_PAYMENT','SUPPLIER_PAYMENT','ADV_PAY_SETTLE')";
		return $this->db->RunQuery($sql);
	}
//END 	- PRIV
}
?>
<?php
class cls_fixed_amounts_and_dates_get
{
	private $db;
		
	function __construct($db)
	{
		$this->db = $db;	
	}
	
//BEGIN - PUBLIC FUNCTIONS {
	public function get_header($location,$item,$year,$executeType)
	{
		$sql	= $this->get_header_sql($location,$item,$year);
		$row	= $this->get_row($sql,$executeType);
		return $row;
	}
	
	public function get_details($location,$item,$year,$executeType)
	{
		$sql	= $this->get_details_sql($location,$item,$year);
		$result	= $this->get_results($sql,$executeType);
		return $result;
	}
	
	
	public function get_report_approval_details_result($location,$item,$year,$executeType){
		$sql 	= $this->Load_Report_approval_details_sql($location,$item,$year);
		$result = $this->db->$executeType($sql);
		return $result;
	}
	
	public function get_max_approve_levels($executeType)
	{
		$sql	= $this->get_max_approve_levels_sql();
		$row	= $this->get_row($sql,$executeType);
		return $row['LEVELS'];
	}
	
	public function get_listing_sql($programCode,$approveLevel,$intUser,$location,$where_string)
	{
		$sql	= $this->load_listing_sql($programCode,$approveLevel,$intUser,$location,$where_string);
		return $sql;
	}
	public function getYear()
	{
		return $this->getYear_sql();
	}
	public function getFixedForecastItem()
	{
		return $this->getFixedForecastItem_sql();
	}
	public function getMaxStatus($locationID,$year,$itemId)
	{
		return $this->getMaxStatus_sql($locationID,$year,$itemId);
	}
	public function gelBalAmount($locationID,$year,$itemId,$date)
	{
		return $this->gelBalAmount_sql($locationID,$year,$itemId,$date);
	}
	public function checkDetailAvailable($locationID,$year,$itemId,$date)
	{
		return $this->checkDetailAvailable_sql($locationID,$year,$itemId,$date);
	}
	public function get_saved_fixed_amounts_array($location,$item,$year,$executeType)
	{
		$dates_array	=	NULL;
		$sql	= $this->get_details_sql($location,$item,$year);
		$result	= $this->get_results($sql,$executeType);
		while($row=mysqli_fetch_array($result)){
			$dates_array[$row['DATE']] = $row['AMOUNT'];
		}
		
		return $dates_array;
	}
	
 	public function get_paid_flag($location,$item,$year,$executeType){
		$sql	= $this->get_paid_flag_sql($location,$item,$year);
		$row	= $this->get_row($sql,$executeType);
 		if ($row['used'] > 0){
			return 1;
		}
	}

 //END 	- PUBLIC FUNCTIONS }

//BEGIN - PRIVATE FUNCTIONS {
	
	private function get_header_sql($location,$item,$year){
	
		$sql	= "
					SELECT
					FPI.LOCATION_ID			AS LOCATION_ID,
					FPI.REPORT_FIELD_ID		AS PETTY_CASH_ITEM,
					FPI.YEAR				AS YEAR,
					FPI.CREATED_DATE		AS DATE,
					FPI.`STATUS`			AS STATUS,
					FPI.APPROVE_LEVELS 		AS LEVELS,
					FPI.CREATED_DATE		AS CREATED_DATE,
					FPI.CREATED_BY			AS CREATED_BY,
					FPI.MODIFIED_DATE		AS MODIFIED_DATE,
					FPI.MODIFIED_BY			AS MODIFIED_BY,
					SU.strUserName 			AS `USER` ,
					FCC.REPORT_FIELD_NAME	AS ITEM_NAME,
					FCC.REPORT_FIELD_CODE	AS ITEM_CODE,
					FCC.REPORT_FIELD_ID		AS ITEM_ID,
					L.strName				AS LOCATION_NAME 
					FROM
					finance_cashflow_date_amount_header as FPI
					INNER JOIN sys_users as SU ON FPI.CREATED_BY = SU.intUserId
					INNER JOIN finance_cashflow_cpanal as FCC ON FCC.REPORT_FIELD_ID = FPI.REPORT_FIELD_ID
					INNER JOIN mst_locations as L ON FPI.LOCATION_ID = L.intId
					WHERE
					FPI.LOCATION_ID = '$location' AND
					FPI.REPORT_FIELD_ID = '$item' AND
					FPI.YEAR = '$year'
					
					";
		return $sql;		
	}
	
	private function get_details_sql($location,$item,$year){
		$sql	="SELECT
					FCDA.LOCATION_ID		AS LOCATION_ID,
					FCDA.REPORT_FIELD_ID	AS ITEM_ID,
					FCDA.`YEAR`				AS YEAR,
					FCDA.RECEIVE_DATE		AS DATE,
					FCDA.`CREDIT PERIOD`	AS CREDIT_PERIOD,
					FCDA.AMOUNT				AS AMOUNT,
					FCDA.USED				AS USED_AMOUNT 
					FROM `finance_cashflow_date_amount` as FCDA
					WHERE
					FCDA.LOCATION_ID = '$location' AND
					FCDA.REPORT_FIELD_ID = '$item' AND
					FCDA.`YEAR` = '$year' 
					ORDER BY FCDA.RECEIVE_DATE ASC";
		return $sql;
	}
	
	private function get_row($sql,$executeType){
	
		$result	= $this->db->$executeType($sql);
		$row=mysqli_fetch_array($result);
		return $row;		
	}
	
	private function get_results($sql,$executeType){
	
		$result	= $this->db->$executeType($sql);
		return $result;		
	}
	
	private function Load_Report_approval_details_sql($location,$item,$year){
	
		
	   	$sql = "SELECT
 				finance_cashflow_date_amount_approved_by.APPROVE_DATE AS dtApprovedDate ,
				sys_users.strUserName as UserName,
				finance_cashflow_date_amount_approved_by.APPROVE_LEVEL as intApproveLevelNo
				FROM
				finance_cashflow_date_amount_approved_by
				Inner Join sys_users ON finance_cashflow_date_amount_approved_by.APPROVE_BY = sys_users.intUserId
				WHERE
				finance_cashflow_date_amount_approved_by.LOCATION_ID =  '$location' AND
				finance_cashflow_date_amount_approved_by.REPORT_FIELD_ID =  '$item'  AND
				finance_cashflow_date_amount_approved_by.YEAR =  '$year'     order by APPROVE_DATE asc";
				
 		return $sql;
	}
	
	private function get_max_approve_levels_sql(){
		$sql	="	SELECT
					Max(finance_cashflow_date_amount_header.APPROVE_LEVELS) AS LEVELS
					FROM `finance_cashflow_date_amount_header`";
		return $sql;
	}
	private function load_listing_sql($programCode,$approveLevel,$intUser,$location,$where_string){

		$sql = "select * from(SELECT DISTINCT 
			if(tb1.STATUS=1,'Approved',if(tb1.STATUS=0,'Rejected',if(tb1.STATUS=-2,'Cancelled',if(tb1.STATUS=-1,'Revised','Pending')))) as Status,
			tb1.LOCATION_ID,
			L.strName AS LOCATION,
			tb1.REPORT_FIELD_ID,
			tb1.YEAR,
			tb1.CREATED_DATE		AS DATE,
			SU.strUserName 			AS USER ,
			FCC.REPORT_FIELD_NAME	AS ITEM_NAME,
			FCC.REPORT_FIELD_CODE	AS ITEM_CODE,
			FCC.REPORT_FIELD_ID		AS ITEM_ID,
			IFNULL((
											SELECT
				concat(sys_users.strUserName,'(',max(finance_cashflow_date_amount_approved_by.APPROVE_DATE),')' )
				FROM
				finance_cashflow_date_amount_approved_by
				Inner Join sys_users ON finance_cashflow_date_amount_approved_by.APPROVE_BY = sys_users.intUserId
				WHERE
				finance_cashflow_date_amount_approved_by.LOCATION_ID  = tb1.LOCATION_ID AND
				finance_cashflow_date_amount_approved_by.REPORT_FIELD_ID =  tb1.REPORT_FIELD_ID AND 
				finance_cashflow_date_amount_approved_by.YEAR =  tb1.YEAR AND 
				finance_cashflow_date_amount_approved_by.APPROVE_LEVEL = '1' AND 
				finance_cashflow_date_amount_approved_by.STATUS = '0' 
			),IF(((SELECT
				menupermision.int1Approval 
				FROM menupermision 
				Inner Join menus ON menupermision.intMenuId = menus.intId
				WHERE
				menus.strCode = '$programCode' AND
				menupermision.intUserId =  '$intUser')=1 AND tb1.STATUS>1),'Approve', '')) as `1st_Approval`,  ";
			
		for($i=2; $i<=$approveLevel; $i++){
			if($i==2){
			$approval="2nd_Approval";
			}
			else if($i==3){
			$approval="3rd_Approval";
			}
			else {
			$approval=$i."th_Approval";
			}
			
			
		$sql .= "IFNULL(
		/*condition*/
	(
											SELECT
				concat(sys_users.strUserName,'(',max(finance_cashflow_date_amount_approved_by.APPROVE_DATE),')' )
				FROM
				finance_cashflow_date_amount_approved_by
				Inner Join sys_users ON finance_cashflow_date_amount_approved_by.APPROVE_BY = sys_users.intUserId
				WHERE
				finance_cashflow_date_amount_approved_by.LOCATION_ID  = tb1.LOCATION_ID AND
				finance_cashflow_date_amount_approved_by.REPORT_FIELD_ID =  tb1.REPORT_FIELD_ID AND
				finance_cashflow_date_amount_approved_by.YEAR =  tb1.YEAR AND 
				finance_cashflow_date_amount_approved_by.APPROVE_LEVEL =  '$i' AND 
				finance_cashflow_date_amount_approved_by.STATUS='0' 
			),
			/*end condition*/
			
			/*false part*/
	
			IF(
			/*if condition */
			((SELECT
				menupermision.int".$i."Approval 
				FROM menupermision 
				Inner Join menus ON menupermision.intMenuId = menus.intId
				WHERE
				menus.strCode = '$programCode' AND
				menupermision.intUserId =  '$intUser')=1 AND (tb1.STATUS>1) AND (tb1.STATUS<=tb1.APPROVE_LEVELS) AND ((SELECT
				max(finance_cashflow_date_amount_approved_by.APPROVE_DATE) 
				FROM
				finance_cashflow_date_amount_approved_by
				Inner Join sys_users ON finance_cashflow_date_amount_approved_by.APPROVE_BY = sys_users.intUserId
				WHERE
				finance_cashflow_date_amount_approved_by.LOCATION_ID = tb1.LOCATION_ID AND
				finance_cashflow_date_amount_approved_by.REPORT_FIELD_ID =  tb1.REPORT_FIELD_ID AND
				finance_cashflow_date_amount_approved_by.YEAR =  tb1.YEAR AND 
				finance_cashflow_date_amount_approved_by.APPROVE_LEVEL =  ($i-1) AND 
				finance_cashflow_date_amount_approved_by.STATUS='0' )<>'')),
				
				/*end if condition*/
				/*if true part*/
				'Approve',
				/*fase part*/
				 if($i>tb1.APPROVE_LEVELS,'-----',''))
				
				/*end IFNULL false part*/
				) as `".$approval."`, "; 
					
				}
			
		
			$sql .=" IFNULL((
											SELECT
				concat(sys_users.strUserName,'(',max(finance_cashflow_date_amount_approved_by.APPROVE_DATE),')' )
				FROM
				finance_cashflow_date_amount_approved_by
				Inner Join sys_users ON finance_cashflow_date_amount_approved_by.APPROVE_BY = sys_users.intUserId
				WHERE
				finance_cashflow_date_amount_approved_by.LOCATION_ID  = tb1.LOCATION_ID AND
				finance_cashflow_date_amount_approved_by.REPORT_FIELD_ID =  tb1.REPORT_FIELD_ID AND 
				finance_cashflow_date_amount_approved_by.YEAR =  tb1.YEAR AND 
				finance_cashflow_date_amount_approved_by.APPROVE_LEVEL = '-1' AND 
				finance_cashflow_date_amount_approved_by.STATUS = '0' 
			),IF(((SELECT
				menupermision.intRevise  
				FROM menupermision 
				Inner Join menus ON menupermision.intMenuId = menus.intId
				WHERE
				menus.strCode = '$programCode' AND
				menupermision.intUserId =  '$intUser')=1 AND tb1.STATUS=1),'Revise', '')) as `Revise`,  ";
			
				
			$sql .= "'View' as `View`   
			FROM
			finance_cashflow_date_amount_header AS tb1 
			INNER JOIN sys_users as SU ON tb1.CREATED_BY = SU.intUserId
			INNER JOIN mst_locations as L ON tb1.LOCATION_ID = L.intId  
			INNER JOIN finance_cashflow_cpanal as FCC ON FCC.REPORT_FIELD_ID = tb1.REPORT_FIELD_ID
			WHERE tb1.LOCATION_ID ='$location'
			$where_string
			)  as t where 1=1
		";
		
		//echo $sql;
		return $sql;
	}
	private function getYear_sql()
	{
		$sql = "SELECT DISTINCT YEAR  
				FROM finance_cashflow_date_amount
				UNION
				(
				SELECT YEAR(CURDATE()) YEAR 
				)";
		
		$result	= $this->db->RunQuery($sql);
		return $result;
	}
	private function getFixedForecastItem_sql()
	{
		$sql = "SELECT REPORT_FIELD_ID,
				REPORT_FIELD_NAME
				FROM finance_cashflow_cpanal
				ORDER BY REPORT_FIELD_NAME ASC";
		
		$result	= $this->db->RunQuery($sql);
		return $result;
	}
	private function getMaxStatus_sql($locationID,$year,$itemId)
	{
		$sql = "SELECT MAX(tb.STATUS) AS maxStatus  
				FROM finance_cashflow_date_amount_approved_by AS tb
				WHERE 
				tb.LOCATION_ID='$locationID' AND
				tb.REPORT_FIELD_ID='$itemId' AND
				tb.YEAR='$year' ";
		
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		return $row['maxStatus'];
	}
	private function gelBalAmount_sql($locationID,$year,$itemId,$date)
	{
		$sql = "SELECT 	AMOUNT,USED,BALANCE
				FROM finance_cashflow_date_amount
				WHERE LOCATION_ID = '$locationID' AND
				REPORT_FIELD_ID = '$itemId' AND
				YEAR = '$year' AND
				RECEIVE_DATE = '$date' ";
		
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row;
	}
	private function checkDetailAvailable_sql($locationID,$year,$itemId,$date)
	{
		$sql = "SELECT * 
				FROM finance_cashflow_date_amount
				WHERE LOCATION_ID = '$locationID' AND
				REPORT_FIELD_ID = '$itemId' AND
				YEAR = '$year' AND
				RECEIVE_DATE = '$date' ";
		
		$result = $this->db->RunQuery2($sql);
		$count	= mysqli_num_rows($result);
		
		if($count>0)
			return true;
		else
			return false;
	}	
	private function get_paid_flag_sql($location,$item,$year){
		
		$sql	="SELECT
				Sum(finance_cashflow_date_amount.USED) AS used
				FROM `finance_cashflow_date_amount`
				WHERE
				finance_cashflow_date_amount.LOCATION_ID = '$location' AND
				finance_cashflow_date_amount.REPORT_FIELD_ID = '$item' AND
				finance_cashflow_date_amount.`YEAR` = '$year'";
		return $sql;
	}
//END 	- PRIVATE FUNCTIONS }
}
?>
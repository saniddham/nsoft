<?php
class cls_fixed_amounts_and_dates_set
{
	private $db;
		
	function __construct($db)
	{
		$this->db = $db;	
	}
	
	public function update_header_status($location,$item,$year,$status,$executeType)
	{ 

   		$sql	= $this->update_header_status_sql($location,$item,$year,$status);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
			return $response;
 	}

	public function approved_by_insert($location,$item,$year,$userId,$approval,$executeType)
	{ 

   		$sql	= $this->approved_by_insert_sql($location,$item,$year,$userId,$approval);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
			return $response;
 	}
	public function saveHeader($locationID,$year,$itemId,$userId,$status,$approveLevels)
	{
		return $this->saveHeader_sql($locationID,$year,$itemId,$userId,$status,$approveLevels);
	}
	public function updateHeader($locationID,$year,$itemId,$userId,$status,$approveLevels)
	{
		return $this->updateHeader_sql($locationID,$year,$itemId,$userId,$status,$approveLevels);
	}
	public function updateApproveByStatus($locationID,$year,$itemId,$maxStatus)
	{
		return $this->updateApproveByStatus_sql($locationID,$year,$itemId,$maxStatus);
	}
	public function deleteData($locationID,$year,$itemId)
	{
		return $this->deleteData_sql($locationID,$year,$itemId);
	}
	public function saveDetails($locationID,$year,$itemId,$date,$amount)
	{
		return $this->saveDetails_sql($locationID,$year,$itemId,$date,$amount);
	}
	public function updateDetails($locationID,$year,$itemId,$date,$amount)
	{
		return $this->updateDetails_sql($locationID,$year,$itemId,$date,$amount);
	}
	public function insert_to_log($location,$item,$year,$type,$executeType)
	{ 

   		$arr_res	= $this->insert_to_log_sql($location,$item,$year,$type,$executeType);
		if($arr_res['type']=='fail'){
			$response['type']	= 'fail';
			$response['msg']	= $arr_res['msg'];
			$response['sql']	= $arr_res['sql'];
		}
			return $response;
 	}
//END 	- PUBLIC FUNCTIONS }

	private function update_header_status_sql($location,$item,$year,$status)
	{
 		
		$sql = "UPDATE finance_cashflow_date_amount_header 
				SET 
				STATUS='$status' 
				WHERE
				finance_cashflow_date_amount_header.LOCATION_ID = '$location' AND
				finance_cashflow_date_amount_header.REPORT_FIELD_ID = '$item' AND
				finance_cashflow_date_amount_header.YEAR = '$year'
				";
 		return $sql;
	}
	
	private function approved_by_insert_sql($location,$item,$year,$userId,$approval){
 		
		$sql = "INSERT INTO `finance_cashflow_date_amount_approved_by` (`LOCATION_ID`,`REPORT_FIELD_ID`,`YEAR`,`APPROVE_LEVEL`,APPROVE_BY,APPROVE_DATE,STATUS) 
			VALUES ('$location','$item','$year','$approval','$userId',now(),0)";
 		
		return $sql;
	}
	private function saveHeader_sql($locationID,$year,$itemId,$userId,$status,$approveLevels)
	{
		$sql = "INSERT INTO finance_cashflow_date_amount_header 
				(
				LOCATION_ID, 
				REPORT_FIELD_ID, 
				YEAR, 
				STATUS, 
				APPROVE_LEVELS, 
				CREATED_BY, 
				CREATED_DATE
				)
				VALUES
				('$locationID', 
				'$itemId', 
				'$year', 
				'$status', 
				'$approveLevels', 
				'$userId', 
				NOW()
				);";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function updateHeader_sql($locationID,$year,$itemId,$userId,$status,$approveLevels)
	{
		$sql = "UPDATE finance_cashflow_date_amount_header 
				SET
				STATUS = '$status' , 
				APPROVE_LEVELS = '$approveLevels' , 
				MODIFIED_BY = '$userId' , 
				MODIFIED_DATE = NOW()
				
				WHERE
				LOCATION_ID = '$locationID' AND 
				REPORT_FIELD_ID = '$itemId' AND 
				YEAR = '$year' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function updateApproveByStatus_sql($locationID,$year,$itemId,$maxStatus)
	{
		$sql = "UPDATE finance_cashflow_date_amount_approved_by 
				SET
				STATUS = $maxStatus+1
				WHERE
				LOCATION_ID = '$locationID' AND 
				REPORT_FIELD_ID = '$itemId' AND  
				STATUS = 0 AND 
				YEAR = '$year' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function deleteData_sql($locationID,$year,$itemId)
	{
		$sql = "DELETE FROM finance_cashflow_date_amount 
				WHERE
				LOCATION_ID = '$locationID' AND 
				REPORT_FIELD_ID = '$itemId' AND 
				YEAR = '$year' AND
				AMOUNT=BALANCE ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function saveDetails_sql($locationID,$year,$itemId,$date,$amount)
	{
		$sql = "INSERT INTO finance_cashflow_date_amount 
				(
				LOCATION_ID, 
				REPORT_FIELD_ID, 
				YEAR, 
				RECEIVE_DATE, 
				AMOUNT, 
				USED,
				BALANCE
				)
				VALUES
				(
				'$locationID', 
				'$itemId', 
				'$year', 
				'$date', 
				'$amount', 
				0,
				'$amount'
				)";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	function updateDetails_sql($locationID,$year,$itemId,$date,$amount)
	{
		$sql = "UPDATE finance_cashflow_date_amount 
				SET
				AMOUNT = '$amount' , 
				USED = 0 , 
				BALANCE = '$amount'
				WHERE
				LOCATION_ID = '$locationID' AND 
				REPORT_FIELD_ID = '$itemId' AND 
				YEAR = '$year' AND 
				RECEIVE_DATE = '$date' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function insert_to_log_sql($location,$item,$year,$type,$executeType){
		
		$sql	="
					SELECT
					finance_cashflow_date_amount.RECEIVE_DATE,
					finance_cashflow_date_amount.AMOUNT,
					mst_companies.intBaseCurrencyId AS CURRENCY 
					FROM `finance_cashflow_date_amount` 
					INNER JOIN mst_locations ON finance_cashflow_date_amount.LOCATION_ID = mst_locations.intId
					INNER JOIN mst_companies ON mst_locations.intCompanyId = mst_companies.intId
					WHERE
					finance_cashflow_date_amount.LOCATION_ID = '$location' AND
					finance_cashflow_date_amount.REPORT_FIELD_ID = '$item' AND
					finance_cashflow_date_amount.`YEAR` = '$year'
					";
		$result	= $this->db->$executeType($sql);
		while($row=mysqli_fetch_array($result)){
			$rcv_date	= $row['RECEIVE_DATE'];
			$amount		= $row['AMOUNT'];
			$currency	= $row['CURRENCY'];
			
   			$sql_l		= $this->insert_to_log_table_sql($location,$item,$rcv_date,$amount,$currency,$type,$executeType);
			$result_l 	= $this->db->$executeType($sql_l);
			if(!$result_l){
				$response['type']	= 'fail';
				$response['msg']	.= $this->db->errormsg;
				$response['sql']	.= $sql;
			}
		}
			return $response;
	}

	private function insert_to_log_table_sql($location,$item,$rcv_date,$amount,$currency,$type,$executeType){
		
	 	$sql = "INSERT INTO `finance_cashflow_log` (`LOCATION_ID`,`REPORT_FIELD_ID`,`RECEIVE_DATE`,`LOG_DATE`,DOCUMENT_TYPE,AMOUNT,CURRENCY_ID) 
			VALUES ('$location','$item','$rcv_date','$rcv_date','$type','$amount','$currency')";
 		
		return $sql;
	}
}
?>
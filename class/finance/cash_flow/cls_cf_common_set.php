<?php
class cls_cf_common_set
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	
	public function insert_to_log($location,$rpt_field,$rcv_date,$invoice_date,$entryDate,$serialNo,$serialYear,$docType,$saveAmnt,$currency,$executeType)
	{
		return $this->insert_to_log_sql($location,$rpt_field,$rcv_date,$invoice_date,$entryDate,$serialNo,$serialYear,$docType,$saveAmnt,$currency,$executeType);
	}
	public function delete_from_log($docNo,$docYear,$docType,$executeType)
	{
		return $this->delete_from_log_sql($docNo,$docYear,$docType,$executeType);
	}
	private function insert_to_log_sql($location,$rpt_field,$rcv_date,$invoice_date,$entryDate,$serialNo,$serialYear,$docType,$saveAmnt,$currency,$executeType)
	{
		$sql = "INSERT INTO finance_cashflow_log 
				(
				LOCATION_ID,
				REPORT_FIELD_ID,
				RECEIVE_DATE,
				INVOICE_DATE,
				LOG_DATE,
				DOCUMENT_NO,
				DOCUMENT_YEAR,
				DOCUMENT_TYPE,
				AMOUNT,
				CURRENCY_ID
				) 
				VALUES 
				(
				'$location',
				'$rpt_field',
				'$rcv_date',
				'$invoice_date',
				'$entryDate',
				'$serialNo',
				'$serialYear',
				'$docType',
				'$saveAmnt',
				'$currency'
				)";
		
		$result = $this->db->$executeType($sql);
		if(!$result)
		{
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		return $response;
	}
	private function delete_from_log_sql($docNo,$docYear,$docType,$executeType)
	{
		$sql = "DELETE FROM finance_cashflow_log 
				WHERE
				DOCUMENT_NO = '$docNo' AND
				DOCUMENT_YEAR = '$docYear' AND
				DOCUMENT_TYPE = '$docType' ";
		
		$result = $this->db->$executeType($sql);
		if(!$result)
		{
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		return $response;
	}
}
?>
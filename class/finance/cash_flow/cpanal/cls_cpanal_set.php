<?php
class cls_cpanal_set
{
	private $db;
		
	function __construct($db)
	{
		$this->db = $db;	
	}
	
//BEGIN - PUBLIC FUNCTIONS {
	public function saveLocation($fieldID,$loactionID,$status,$executeType)
	{
		return $this->saveLocation_sql($fieldID,$loactionID,$status,$executeType);	
	}
	public function deleteLocation($fieldID,$executeType)
	{
		return $this->deleteLocation_sql($fieldID,$executeType);	
	}
	public function update_cpanel($id,$code,$name,$boo_po,$po_items,$boo_gl,$gl_items,$amnt_category,$order_by,$active,$executeType)
	{ 

   		$sql	= $this->update_cpanel_sql($id,$code,$name,$boo_po,$po_items,$boo_gl,$gl_items,$amnt_category,$order_by,$active);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
			return $response;
 	}


	public function insert_cpanel($id,$code,$name,$boo_po,$po_items,$boo_gl,$gl_items,$amnt_category,$order_by,$active,$executeType)
	{ 

   		$sql	= $this->insert_cpanel_sql($id,$code,$name,$boo_po,$po_items,$boo_gl,$gl_items,$amnt_category,$order_by,$active);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
			return $response;
 	}

//END 	- PUBLIC FUNCTIONS }

//BEGIN - PRIVATE FUNCTIONS {
	private function saveLocation_sql($fieldID,$loactionID,$status,$executeType)
	{
		$sql	= 	" INSERT INTO finance_cashflow_cpanal_location 
					(REPORT_FIELD_ID, 
					LOCATION_ID, 
					STATUS
					)
					VALUES
					('$fieldID', 
					'$loactionID', 
					'$status'
					)";
		$result	= $this->db->$executeType($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function deleteLocation_sql($fieldID,$executeType)
	{
		$sql	= " DELETE FROM finance_cashflow_cpanal_location 
					WHERE
					REPORT_FIELD_ID = '$fieldID'";
		$result	= $this->db->$executeType($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	
	private function insert_cpanel_sql($id,$code,$name,$boo_po,$po_items,$boo_gl,$gl_items,$amnt_category,$order_by,$active){
		$sql	="
		INSERT INTO finance_cashflow_cpanal 
		(REPORT_FIELD_CODE,REPORT_FIELD_NAME,BOO_PO_RAISED,
		PO_ITEM_ID, BOO_GL_ITEM, GL_ID, AMOUNT_FILLING_CATEGORY, ORDER_BY, STATUS) 
		VALUES 
		('$code','$name',$boo_po,'$po_items',$boo_gl,'$gl_items','$amnt_category',$order_by,$active)";
		return $sql;	
	}
	
	private function update_cpanel_sql($id,$code,$name,$boo_po,$po_items,$boo_gl,$gl_items,$amnt_category,$order_by,$active){

		$sql = "UPDATE finance_cashflow_cpanal 
				SET
				REPORT_FIELD_CODE 	= '$code',
				REPORT_FIELD_NAME	= '$name', 
				BOO_PO_RAISED		= '$boo_po', 
				PO_ITEM_ID			= '$po_items', 
				BOO_GL_ITEM			= '$boo_gl', 
				GL_ID				= '$gl_items', 
				AMOUNT_FILLING_CATEGORY	= '$amnt_category', 
				ORDER_BY			= '$order_by', 
				`STATUS`			= '$active' 
								
				WHERE
				REPORT_FIELD_ID = '$id' ";

		return $sql;	
	}
//END 	- PRIVATE FUNCTIONS }
}
?>
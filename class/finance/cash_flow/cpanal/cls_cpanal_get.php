<?php
class cls_cpanal_get
{
	private $db;
		
	function __construct($db)
	{
		$this->db = $db;	
	}
	
//BEGIN - PUBLIC FUNCTIONS {
	public function getData($fieldID,$executeType)
	{
		return $this->getData_sql($fieldID,$executeType);
	}
	public function getLocation($companyId,$executeType)
	{
		return $this->getLocation_sql($companyId,$executeType);	
	}
	
	public function get_details_results($executeType)
	{
		$sql	= $this->get_details_sql();
		$result	= $this->get_results($sql,$executeType);
		return $result;
	}
	public function get_items_result($saved_items,$executeType)
	{
		$sql	= $this->get_items_sql($saved_items);
		$result	= $this->get_results($sql,$executeType);
		return $result;
	}
	public function get_gl_result($saved_items,$executeType)
	{
		$sql	= $this->get_gl_sql($saved_items);
		$result	= $this->get_results($sql,$executeType);
		return $result;
	}
	public function chk_duplicate_codes($id,$code,$executeType){
		$sql	= $this->chk_duplicate_codes_sql($id,$code);
		$row	= $this->get_row($sql,$executeType);
		if($row['REPORT_FIELD_ID'] !='')
			return true;
		else
			return false;
	}
 	
//END 	- PUBLIC FUNCTIONS }

//BEGIN - PRIVATE FUNCTIONS {
	private function getData_sql($fieldID,$executeType)
	{
		$sql	= " SELECT 	
					LOCATION_ID, 
					STATUS
					 
					FROM 
					finance_cashflow_cpanal_location CL
					
					WHERE	
					CL.REPORT_FIELD_ID = $fieldID";	
		$result	= $this->db->$executeType($sql);
		return $result;
	}
	private function getLocation_sql($companyId,$executeType)
	{
		$sql	= " SELECT 	L.intId, 
					L.strName 
					
					FROM 
					mst_locations L
					
					WHERE	
					L.intCompanyId	= $companyId 
					AND L.intStatus = 1
					
					ORDER BY
					L.strName";	
					//echo $sql;
		$result	= $this->db->$executeType($sql);
		return $result;
	}
	
	private function get_details_sql(){
		$sql	="SELECT
				finance_cashflow_cpanal.REPORT_FIELD_ID,
				finance_cashflow_cpanal.REPORT_FIELD_CODE,
				finance_cashflow_cpanal.REPORT_FIELD_NAME,
				finance_cashflow_cpanal.BOO_FOCUS_DATE,
				finance_cashflow_cpanal.BOO_PO_RAISED,
				finance_cashflow_cpanal.PO_ITEM_ID,
				finance_cashflow_cpanal.BOO_GL_ITEM,
				finance_cashflow_cpanal.GL_ID,
				finance_cashflow_cpanal.AMOUNT_FILLING_CATEGORY,
				finance_cashflow_cpanal.FINANCE_CATEGORY,
				finance_cashflow_cpanal.ORDER_BY,
				finance_cashflow_cpanal.`STATUS`,
				mst_item.intId,
				mst_item.strCode,
				mst_item.strName,
				GROUP_CONCAT(mst_item.strName) AS ITEMS, 
				finance_mst_chartofaccount.CHART_OF_ACCOUNT_ID,
				finance_mst_chartofaccount.CHART_OF_ACCOUNT_CODE,
				finance_mst_chartofaccount.CHART_OF_ACCOUNT_NAME,
				GROUP_CONCAT(finance_mst_chartofaccount.CHART_OF_ACCOUNT_NAME) AS COA 
				FROM
				finance_cashflow_cpanal
				LEFT JOIN mst_item ON FIND_IN_SET(mst_item.intId,finance_cashflow_cpanal.PO_ITEM_ID) 
				LEFT JOIN finance_mst_chartofaccount  ON FIND_IN_SET(finance_mst_chartofaccount.CHART_OF_ACCOUNT_ID,finance_cashflow_cpanal.GL_ID)
				GROUP BY
				finance_cashflow_cpanal.REPORT_FIELD_ID";
		return $sql;
	}
	
	private function get_row($sql,$executeType){
	
		$result	= $this->db->$executeType($sql);
		$row=mysqli_fetch_array($result);
		return $row;		
	}
	
	private function get_results($sql,$executeType){
	
		$result	= $this->db->$executeType($sql);
		return $result;		
	}
	
	private function get_items_sql($saved_items){
		
	  	$sql	="
					SELECT ";
			if($saved_items == '')
			$sql	.=" 0 as saved_flag, ";
			else
			$sql	.=" IF((FIND_IN_SET(mst_item.intId,'$saved_items')=1),1,0) as saved_flag, ";
			$sql	.=" mst_item.intId,
					mst_item.strName 
					FROM `mst_item`
					WHERE
					mst_item.intStatus = '1'
					ORDER BY
					mst_item.strName ASC		";
		return $sql;
		
	}

	private function get_gl_sql($saved_items){
		
	  	$sql	="
					SELECT ";
			if($saved_items == '')
			$sql	.=" 0 as saved_flag, ";
			else
			$sql	.=" IF((FIND_IN_SET(finance_mst_chartofaccount.CHART_OF_ACCOUNT_ID,'$saved_items')=1),1,0) as saved_flag, ";
			$sql	.=" finance_mst_chartofaccount.CHART_OF_ACCOUNT_ID,
					finance_mst_chartofaccount.CHART_OF_ACCOUNT_NAME 
					FROM `finance_mst_chartofaccount`
					WHERE
					finance_mst_chartofaccount.STATUS = '1'
					ORDER BY
					finance_mst_chartofaccount.CHART_OF_ACCOUNT_NAME ASC		";
		return $sql;
		
	}
	
	private function chk_duplicate_codes_sql($id,$code){
	
		$sql	="
					SELECT
					finance_cashflow_cpanal.REPORT_FIELD_ID
					FROM `finance_cashflow_cpanal`
					WHERE 
					finance_cashflow_cpanal.REPORT_FIELD_CODE = '$code'
				";
		//if($id != ''){		
			$sql	.="  AND finance_cashflow_cpanal.REPORT_FIELD_ID <> '$id' ";
		//}
	
	return $sql;
		
	}
	
//END 	- PRIVATE FUNCTIONS }
}
?>
<?php

class cls_cf_common
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	
	public function get_cf_log_bal_to_invoice_result($location,$rpt_field,$currency,$entryDate,$company,$executeType){
		$sql 	= $this->get_cf_log_bal_to_invoice_sql($location,$rpt_field,$currency,$entryDate,$company);
		$result	= $this->get_results($sql,$executeType);
		return $result;
	}
	
	public function get_cf_log_bal_to_payment_result($location,$rpt_field,$currency,$entryDate,$company,$executeType){
		$sql 	= $this->get_cf_log_bal_to_payment_sql($location,$rpt_field,$currency,$entryDate,$company);
		$result	= $this->get_results($sql,$executeType);
		return $result;
	}
	
	public function get_je_header($location,$rpt_field,$executeType){
		$sql 	= $this->get_je_header_sql($location,$rpt_field);
		$result	= $this->get_results($sql,$executeType);
		return $result;
	}
	
	public function get_bank_payment_header($location,$rpt_field,$executeType){
		$sql 	= $this->get_bank_payment_header_sql($location,$rpt_field);
		$result	= $this->get_results($sql,$executeType);
		return $result;
	}
	
	public function get_previous_type_cf_ids_result($executeType){
		$sql 	= $this->get_previous_type_cf_ids_sql();
		$result	= $this->get_results($sql,$executeType);
		return $result;
	}
	
	public function get_cf_receive_dates_range_result($location,$date_from,$currunt_date,$rpt_field,$executeType){
		$sql 	= $this->get_cf_receive_dates_range_sql($location,$date_from,$currunt_date,$rpt_field);
		$result	= $this->get_results($sql,$executeType);
		return $result;
	}
	
	public function get_cf_log_invoice_for_dates_range_result($location,$rpt_field,$rcv_date,$executeType){
		$sql 	= $this->get_cf_log_invoice_for_dates_range_sql($location,$rpt_field,$rcv_date);
		$result	= $this->get_results($sql,$executeType);
		return $result;
	}
	
	public function get_cf_used_invoice_amounts($location,$rpt_field,$currency,$entryDate,$company,$executeType){
		$sql 	= $this->get_cf_used_invoice_amounts_sql($location,$rpt_field,$currency,$entryDate,$company);
		$row	= $this->get_row($sql,$executeType);
		return $row['BAL'];
	}
	
	private function get_cf_log_bal_to_invoice_sql($location,$rpt_field,$currency,$entryDate,$company){
		$sql	="
				SELECT
				finance_cashflow_log.AMOUNT, 
				finance_cashflow_log.LOCATION_ID,
				finance_cashflow_log.REPORT_FIELD_ID,
				finance_cashflow_log.RECEIVE_DATE,
				finance_cashflow_log.LOG_DATE AS DATE,
				finance_cashflow_log.CURRENCY_ID,
				/*SUM(IF(DOCUMENT_TYPE='OPENING_FIXED' OR DOCUMENT_TYPE='OPENING_PREVIOUS' ,finance_cashflow_log.AMOUNT,
				IF(DOCUMENT_TYPE='OPENING_FIXED_REVISE' OR DOCUMENT_TYPE='JOURNAL_ENTRY' OR DOCUMENT_TYPE='INVOICE' OR DOCUMENT_TYPE='BILLINVOICE', finance_cashflow_log.AMOUNT*-1,0))) as BAL */

				IFNULL(SUM((IF(DOCUMENT_TYPE='OPENING_FIXED' OR DOCUMENT_TYPE='OPENING_PREVIOUS' ,finance_cashflow_log.AMOUNT,
				IF(DOCUMENT_TYPE='OPENING_FIXED_REVISE' OR DOCUMENT_TYPE='JOURNAL_ENTRY' OR DOCUMENT_TYPE='INVOICE' OR DOCUMENT_TYPE='BILLINVOICE', finance_cashflow_log.AMOUNT*-1,0)))*convert_frm.dblBuying/convert_to.dblBuying),0) as BAL    
				
				FROM `finance_cashflow_log` 
				INNER JOIN mst_locations ON finance_cashflow_log.LOCATION_ID = mst_locations.intId
				LEFT JOIN mst_financeexchangerate as convert_frm ON finance_cashflow_log.CURRENCY_ID = convert_frm.intCurrencyId  
				AND DATE(finance_cashflow_log.CREATE_DATE) = convert_frm.dtmDate AND 
				mst_locations.intCompanyId = convert_frm.intCompanyId 
				LEFT JOIN mst_financeexchangerate as convert_to ON $currency = convert_to.intCurrencyId  
				AND DATE('$entryDate') = convert_to.dtmDate AND 
				$company = convert_to.intCompanyId 
				WHERE
				finance_cashflow_log.LOCATION_ID = '$location' AND
				finance_cashflow_log.REPORT_FIELD_ID = '$rpt_field'
				GROUP BY
				finance_cashflow_log.LOCATION_ID,
				finance_cashflow_log.REPORT_FIELD_ID,
				finance_cashflow_log.RECEIVE_DATE,
				finance_cashflow_log.CURRENCY_ID
				ORDER BY
				finance_cashflow_log.RECEIVE_DATE ASC";
				
				return $sql;
	}
	
	private function get_cf_log_bal_to_payment_sql($location,$rpt_field,$currency,$entryDate,$company){
		$sql	="
				SELECT
				finance_cashflow_log.AMOUNT, 
				finance_cashflow_log.LOCATION_ID,
				finance_cashflow_log.REPORT_FIELD_ID,
				finance_cashflow_log.RECEIVE_DATE,
				finance_cashflow_log.LOG_DATE AS DATE,
				finance_cashflow_log.CURRENCY_ID,
	
				IFNULL(SUM((IF(DOCUMENT_TYPE='JOURNAL_ENTRY' OR DOCUMENT_TYPE='INVOICE' OR DOCUMENT_TYPE='BILLINVOICE' ,finance_cashflow_log.AMOUNT,
				IF(DOCUMENT_TYPE='BANK_PAYMENT' OR DOCUMENT_TYPE='SUPPLIER_PAYMENT' OR DOCUMENT_TYPE='BILL_PAYMENT' OR DOCUMENT_TYPE='ADV_PAY_SETTLE', finance_cashflow_log.AMOUNT*-1,0)))*convert_frm.dblBuying/convert_to.dblBuying),0) as BAL    
				
				FROM `finance_cashflow_log` 
				INNER JOIN mst_locations ON finance_cashflow_log.LOCATION_ID = mst_locations.intId
				LEFT JOIN mst_financeexchangerate as convert_frm ON finance_cashflow_log.CURRENCY_ID = convert_frm.intCurrencyId  
				AND DATE(finance_cashflow_log.CREATE_DATE) = convert_frm.dtmDate AND 
				mst_locations.intCompanyId = convert_frm.intCompanyId 
				LEFT JOIN mst_financeexchangerate as convert_to ON $currency = convert_to.intCurrencyId  
				AND DATE('$entryDate') = convert_to.dtmDate AND 
				$company = convert_to.intCompanyId 
				WHERE
				finance_cashflow_log.LOCATION_ID = '$location' AND
				finance_cashflow_log.REPORT_FIELD_ID = '$rpt_field'
				GROUP BY
				finance_cashflow_log.LOCATION_ID,
				finance_cashflow_log.REPORT_FIELD_ID,
				finance_cashflow_log.RECEIVE_DATE,
				finance_cashflow_log.CURRENCY_ID
				ORDER BY
				finance_cashflow_log.RECEIVE_DATE ASC";
				
				return $sql;
	}

	private function get_row($sql,$executeType){
	
		$result	= $this->db->$executeType($sql);
		$row=mysqli_fetch_array($result);
		return $row;		
	}
	
	private function get_results($sql,$executeType){
	
		$result	= $this->db->$executeType($sql);
		return $result;		
	}
	
	private function get_previous_type_cf_ids_sql(){
		$sql	="
				SELECT
				finance_cashflow_cpanal.REPORT_FIELD_ID,
				finance_cashflow_cpanal.REPORT_FIELD_CODE,
				finance_cashflow_cpanal.REPORT_FIELD_NAME,
				finance_cashflow_cpanal.BOO_FOCUS_DATE,
				finance_cashflow_cpanal.BOO_PO_RAISED,
				finance_cashflow_cpanal.PO_ITEM_ID,
				finance_cashflow_cpanal.BOO_GL_ITEM,
				finance_cashflow_cpanal.GL_ID,
				finance_cashflow_cpanal.AMOUNT_FILLING_CATEGORY,
				finance_cashflow_cpanal.FINANCE_CATEGORY,
				finance_cashflow_cpanal.ORDER_BY
				FROM `finance_cashflow_cpanal` 
				WHERE
				finance_cashflow_cpanal.AMOUNT_FILLING_CATEGORY = 'P'

				";
		
		return $sql;
	}
	
	private function get_cf_receive_dates_range_sql($location,$date_from,$currunt_date,$rpt_field){
		
		$sql	="
				SELECT  DISTINCT 
				finance_cashflow_date_amount.RECEIVE_DATE 
				FROM `finance_cashflow_date_amount`
				WHERE
				finance_cashflow_date_amount.LOCATION_ID = '$location' AND
				finance_cashflow_date_amount.REPORT_FIELD_ID = '$rpt_field' AND
				finance_cashflow_date_amount.RECEIVE_DATE > '$date_from' AND
				finance_cashflow_date_amount.RECEIVE_DATE <= '$currunt_date'		";
		return $sql;
	}
	
	private function get_cf_log_invoice_for_dates_range_sql($location,$rpt_field,$rcv_date){
		$sql	="
				SELECT
				finance_cashflow_log.RECEIVE_DATE,
				finance_cashflow_log.LOG_DATE,
				finance_cashflow_log.AMOUNT,
				finance_cashflow_log.CURRENCY_ID,
				finance_cashflow_log.DOCUMENT_TYPE
				FROM `finance_cashflow_log`
				WHERE
				finance_cashflow_log.LOCATION_ID = '$location' AND
				finance_cashflow_log.REPORT_FIELD_ID = '$rpt_field' AND
				finance_cashflow_log.RECEIVE_DATE = '$rcv_date'  AND 
				finance_cashflow_log.DOCUMENT_TYPE IN ('JOURNAL_ENTRY','BILLINVOICE','INVOICE')";
		return $sql;
	}
	
	private function get_cf_used_invoice_amounts_sql($location,$rpt_field,$currency,$entryDate,$company){
		$sql	="
				SELECT
				IFNULL(SUM(finance_cashflow_log.AMOUNT*convert_frm.dblBuying/convert_to.dblBuying),0) as BAL    
				FROM `finance_cashflow_log` 
				INNER JOIN mst_locations ON finance_cashflow_log.LOCATION_ID = mst_locations.intId
				LEFT JOIN mst_financeexchangerate as convert_frm ON finance_cashflow_log.CURRENCY_ID = convert_frm.intCurrencyId  
				AND DATE(finance_cashflow_log.CREATE_DATE) = convert_frm.dtmDate AND 
				mst_locations.intCompanyId = convert_frm.intCompanyId 
				LEFT JOIN mst_financeexchangerate as convert_to ON $currency = convert_to.intCurrencyId  
				AND DATE(now()) = convert_to.dtmDate AND 
				$company = convert_to.intCompanyId 
				WHERE
				finance_cashflow_log.LOCATION_ID = '$location' AND
				finance_cashflow_log.REPORT_FIELD_ID = '$rpt_field' AND 
				finance_cashflow_log.RECEIVE_DATE = '$entryDate' AND 
				(finance_cashflow_log.DOCUMENT_TYPE='JOURNAL_ENTRY' OR finance_cashflow_log.DOCUMENT_TYPE='INVOICE' OR finance_cashflow_log.DOCUMENT_TYPE='BILLINVOICE' )";
				
				return $sql;
	}
	
	
 }
?>
<?php
include_once $_SESSION['ROOT_PATH']."class/masterData/exchange_rate/cls_exchange_rate_get.php";
include_once $_SESSION['ROOT_PATH']."class/finance/cash_flow_forecast/cls_cash_flow_forecast_get.php";
include_once $_SESSION['ROOT_PATH']."class/finance/cash_flow_forecast/cls_cash_flow_forecast_set.php";

$objExchangeRateGet 		= new cls_exchange_rate_get($db);
$objCashFlowForecastGet		= new cls_cash_flow_forecast_get($db);
$objCashFlowForecastSet		= new cls_cash_flow_forecast_set($db);

$session_companyId 	= $_SESSION["headCompanyId"];

class Cls_Common_Get
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
 
 	public function getCostCenterCombo($costCenter)
	{		
		$sql = $this->costCenter_sql();
		$html = $this->costCenter_combo($sql,$costCenter);
		return $html;
	}
 	public function getJECombo($JEYear,$JENo,$orderId,$account)
	{		
 		$sql = $this->JE_sql($account);
		$html = $this->JE_combo($sql,$JEYear,$JENo,$orderId);
		return $html;
	}
 	public function getCurrencyCombo($currency)
	{		
		$sql = $this->currency_sql();
		$html = $this->currency_combo($sql,$currency);
		return $html;
	}
 	public function get_Base_Currency_arr($company)
	{		
		$sql = $this->get_Base_Currency_sql($company);
 		$result = $this->db->RunQuery($sql);
 		$row = mysqli_fetch_array($result);
		return $row;
	}
 	public function getCustomerCombo($customer)
	{		
		$sql = $this->customer_sql();
		$html = $this->customer_combo($sql,$customer);
		return $html;
	}
	public function GetCustomerGL($customerId)
	{
		return $this->GetCustomerGL_sql($customerId);
	}
	public function GetSupplierGL($supplierId)
	{
		return $this->GetSupplierGL_sql($supplierId);
	}
	public function getTaxCombo($TaxCode)
	{		
		$sql = $this->tax_sql();
		$html = $this->tax_combo($sql,$TaxCode);
		return $html;
	}
	public function getUOMCombo($uom)
	{		
		$sql = $this->uom_sql();
		$html = $this->uom_combo($sql,$uom);
		return $html;
	}
	public function getPayMethodCombo($payMethod)
	{		
		$sql = $this->PayMethod_sql();
		$html = $this->PayMethod_combo($sql,$payMethod);
		return $html;
	}
	public function getDateChangeMode($programCode,$userId)
	{
		return $this->DateChangeMode_sql($programCode,$userId);
	}
	public function Save_Jurnal_Transaction_bkp_date($glId,$amount,$documentNo,$documentYear,$documentType,$invoiceNo,$invoiceYear,$transType,$transCategory,$transCatId,$currencyId,$runDate)
	{
		return $this->Save_Jurnal_Transaction_bkp_date_sql($glId,$amount,$documentNo,$documentYear,$documentType,$invoiceNo,$invoiceYear,$transType,$transCategory,$transCatId,$currencyId,$runDate);
	}
	public function Save_Jurnal_Transaction($glId,$amount,$documentNo,$documentYear,$documentType,$invoiceNo,$invoiceYear,$transType,$transCategory,$transCatId,$currencyId)
	{
		return $this->Save_Jurnal_Transaction_sql($glId,$amount,$documentNo,$documentYear,$documentType,$invoiceNo,$invoiceYear,$transType,$transCategory,$transCatId,$currencyId);
	}
	
	public function GetPublic_values($selectionF,$table,$conditionF,$condition)
	{
		return  $this->GetPublicValues($selectionF,$table,$conditionF,$condition);
 	}
	public function getSerialNo($serialNo,$date,$company,$executeType)
	{
		$result	= $this->getSerialNo_result($date,$company,$executeType);
 		$row	= mysqli_fetch_array($result);
		return str_pad($serialNo,6,0,STR_PAD_LEFT).'/'.$row['startYear'].'-'.$row['endYear'].'/'.$row['compCode'];
	}
	public function getPaymentVoucherNo($payDate,$bankGLId,$costCenter,$executeType)
	{
		return  $this->getPaymentVoucherNo_sql($payDate,$bankGLId,$costCenter,$executeType);
	}
	public function getCompanyCostCenter($locationId,$executeType)
	{
		return  $this->getCompanyCostCenter_sql($locationId,$executeType);
	}
	public function get_exchane_rate($currency,$date,$company,$executionType)
	{
		$sql	=  $this->get_exchane_sql($currency,$date,$company);
 		$result = $this->db->$executionType($sql);
 		$row	= mysqli_fetch_array($result);
		return $row['dblExcAvgRate'];
	}
	public function getSvatValue($grandTotal,$executionType)
	{
		return  $this->getSvatValue_sql($grandTotal,$executionType);
	}
	public function getSvatValue_poSaved($poNo,$poYear,$grandTotal,$executionType)
	{
		return  $this->getSvatValue_poSaved_sql($poNo,$poYear,$grandTotal,$executionType);
	}
	public function GetBankExRate($glId,$transType)
	{
		return $this->GetBankExRate_sql($glId,$transType);
	}
	public function getBankCombo($bank)
	{		
		$sql = $this->bank_sql();
		$html = $this->bank_combo($sql,$bank);
		return $html;
	}
	public function getMarketerCombo($marketer)
	{		
		$sql = $this->marketer_sql();
		$html = $this->marketer_combo($sql,$marketer);
		return $html;
	}
	
	public function SaveSupplierForcast($grnNo,$grnYear)
	{
		return $this->SaveSupplierForcast_sql($grnNo,$grnYear);
	}
	
	public function UpdateSupplierForcast($amount,$categoryId,$currencyId,$date)
	{
		return $this->UpdateSupplierForcast_sql($amount,$categoryId,$currencyId,$date);
	}
	
	private function GetBankExRate_sql($glId,$transType)
	{
		$sql = "SELECT 
					BANK_ID 	AS BANK_ID,
					CURRENCY_ID AS CURRENCY_ID
				FROM finance_mst_chartofaccount_bank
				WHERE CHART_OF_ACCOUNT_ID = $glId";
		$result = $this->db->$transType($sql);
		return mysqli_fetch_array($result);
	}
	
	
	private function costCenter_sql()
	{
		global $session_companyId;
		
		$sql = "SELECT
					intId,
					strName
				FROM mst_financedimension
				WHERE
					intStatus = 1
				order by strName";
 		return $sql;
	}
	private function costCenter_combo($sql,$costCenter)
	{
 		
 		$result = $this->db->RunQuery($sql);
		$string ="<option value=\"\"></option>";
		while($row = mysqli_fetch_array($result))
		{
			if($row['intId']==$costCenter)
				$string.="<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";
			else
				$string.="<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		return $string;
	}
	private function JE_combo($sql,$JEYear,$JENo,$orderId){
	
 		$result = $this->db->RunQuery($sql);
		$string ="<option value=\"\"></option>";
		while($row = mysqli_fetch_array($result))
		{
			if(($row['JOURNAL_ENTRY_NO']==$JENo) && ($row['JOURNAL_ENTRY_YEAR']==$JEYear) && ($row['ORDER_ID']==$orderId))
				$string.="<option value=\"".$row['JOURNAL_ENTRY_YEAR']."/".$row['JOURNAL_ENTRY_NO']."/".$row['ORDER_ID']."\" selected=\"selected\">".$row['REMARKS']."-".$row['JOURNAL_ENTRY_YEAR']."/".$row['JOURNAL_ENTRY_NO']."->".$row['BALANCE_TO_SETTELE']."</option>";
			else
				$string.="<option value=\"".$row['JOURNAL_ENTRY_YEAR']."/".$row['JOURNAL_ENTRY_NO']."/".$row['ORDER_ID']."\">".$row['REMARKS']."-".$row['JOURNAL_ENTRY_YEAR']."/".$row['JOURNAL_ENTRY_NO']."->".$row['BALANCE_TO_SETTELE']."</option>";
		}
		return $string;
		
	}
 	private function currency_sql()
	{
		global $session_companyId;
		
		$sql = "SELECT intId,strCode 
				FROM  mst_financecurrency 
				WHERE intStatus = 1 
				ORDER BY intId";
 		return $sql;
	}
	private function currency_combo($sql,$currency)
	{
 		
 		$result = $this->db->RunQuery($sql);
		$string ="<option value=\"\"></option>";
		while($row = mysqli_fetch_array($result))
		{
			if($row['intId']==$currency)
				$string.="<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strCode']."</option>";
			else
				$string.="<option value=\"".$row['intId']."\">".$row['strCode']."</option>";
		}
		return $string;
	}
	private function customer_sql()
	{
		global $session_companyId;
		
		$sql = "SELECT
					mst_customer.intId,
					mst_customer.strName
				FROM `mst_customer`
				/*WHERE
					mst_customer.intStatus = '1'*/
				ORDER BY
					mst_customer.strName ASC";
 		return $sql;
	}
	private function customer_combo($sql,$customer)
	{
 		
 		$result = $this->db->RunQuery($sql);
		$string ="<option value=\"\"></option>";
		while($row = mysqli_fetch_array($result))
		{
			if($row['intId']==$customer)
				$string.="<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";
			else
				$string.="<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		return $string;
	}
	private function tax_sql()
	{
		global $session_companyId;
		
		 $sql = "SELECT intId,strCode
				FROM mst_financetaxgroup
				WHERE intStatus=1";
 		return $sql;
	}
	private function JE_sql($account)
	{
		global $session_companyId;
		
		    $sql = "SELECT
				finance_journal_entry_details.JOURNAL_ENTRY_NO,
				finance_journal_entry_details.JOURNAL_ENTRY_YEAR,
				finance_journal_entry_details.CHART_OF_ACCOUNT_ID,
				finance_journal_entry_details.REMARKS,
				finance_mst_chartofaccount.CHART_OF_ACCOUNT_NAME,
				finance_journal_entry_details.ORDER_ID,
				finance_journal_entry_details.BAL_AMOUNT as BALANCE_TO_SETTELE
 				FROM
				finance_journal_entry_details
				INNER JOIN finance_journal_entry_header ON finance_journal_entry_details.JOURNAL_ENTRY_NO = finance_journal_entry_header.JOURNAL_ENTRY_NO AND finance_journal_entry_details.JOURNAL_ENTRY_YEAR = finance_journal_entry_header.JOURNAL_ENTRY_YEAR
				INNER JOIN finance_mst_chartofaccount ON finance_journal_entry_details.CHART_OF_ACCOUNT_ID = finance_mst_chartofaccount.CHART_OF_ACCOUNT_ID
				WHERE 
				finance_journal_entry_details.CHART_OF_ACCOUNT_ID = '$account' AND 
				finance_journal_entry_header.`STATUS` = 1  AND 
				finance_journal_entry_details.SETTELEMENT_NO IS NULL  
				HAVING BALANCE_TO_SETTELE > 0
				ORDER BY
				finance_mst_chartofaccount.CHART_OF_ACCOUNT_NAME ASC";
 		return $sql;
	}
	private function tax_combo($sql,$TaxCode)
	{
 		
 		$result = $this->db->RunQuery($sql);
		$string ="<option value=\"NULL\"></option>";
		while($row = mysqli_fetch_array($result))
		{
			if($row['intId']==$TaxCode)
				$string.="<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strCode']."</option>";
			else
				$string.="<option value=\"".$row['intId']."\">".$row['strCode']."</option>";
		}
		return $string;
	}
	private function uom_sql()
	{
		global $session_companyId;
		
		$sql = "SELECT intId,strName FROM mst_units WHERE intStatus=1 order by strName ASC";
 		return $sql;
	}
	private function uom_combo($sql,$uom)
	{
 		
 		$result = $this->db->RunQuery($sql);
		$string ="<option value=\"\"></option>";
		while($row = mysqli_fetch_array($result))
		{
			if($row['intId']==$uom)
				$string.="<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";
			else
				$string.="<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		return $string;
	}
	private function PayMethod_sql()
	{
		global $session_companyId;
		
		$sql = "SELECT
				intId,
				strName
				FROM mst_financepaymentsmethods
				WHERE
				intStatus =  '1'";
 		
		return $sql;
	}
	private function PayMethod_combo($sql,$payMethod)
	{
 		
 		$result = $this->db->RunQuery($sql);
		$string ="<option value=\"\"></option>";
		while($row = mysqli_fetch_array($result))
		{
			if($row['intId']==$payMethod)
				$string.="<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";
			else
				$string.="<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		return $string;
	}
	private function DateChangeMode_sql($programCode,$userId)
	{
		$sql = "SELECT MS.intStatus
				FROM menus M
				INNER JOIN menus_special MS ON M.intId=MS.intMenuId
				INNER JOIN menus_special_permision SP ON MS.intId=SP.intSpMenuId
				WHERE M.strCode='$programCode' AND
				SP.intUser='$userId' ";
		
		$result = $this->db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		
		return $row['intStatus'];
	}
	private function get_Base_Currency_sql($company)
	{
		$sql = "SELECT
				mst_financecurrency.intId,
				mst_financecurrency.strCode
				FROM
				mst_companies
				INNER JOIN mst_financecurrency ON mst_companies.intBaseCurrencyId = mst_financecurrency.intId
				WHERE
				mst_companies.intId = '$company'";
		
 		return $sql;
	}
	private function GetCustomerGL_sql($customerId)
	{
		$sql = "SELECT 
					CHART_OF_ACCOUNT_ID 
				FROM finance_mst_chartofaccount FCOA
				WHERE FCOA.CATEGORY_TYPE = 'C'
					AND CATEGORY_ID = '$customerId'";
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		$row_count	= mysqli_num_rows($result);
		if($row_count>0){
			return $row["CHART_OF_ACCOUNT_ID"];
		}else
		{
			$customerName		= $this->GetPublicValues('strName','mst_customer','intId',$customerId);
			$response['type']		= 'fail';
			$response['ErrorMsg']	= "No GL Account available for Customer : $customerName";
			return $response;
		}
	}
	private function GetSupplierGL_sql($supplierId)
	{
		$sql = "SELECT 
					CHART_OF_ACCOUNT_ID 
				FROM finance_mst_chartofaccount FCOA
				WHERE FCOA.CATEGORY_TYPE = 'S'
					AND CATEGORY_ID = '$supplierId'";
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		$row_count	= mysqli_num_rows($result);
		if($row_count>0){
			return $row["CHART_OF_ACCOUNT_ID"];
		}else
		{
			$supplierName		= $this->GetPublicValues('strName','mst_supplier','intId',$supplierId);
			$response['type']		= 'fail';
			$response['ErrorMsg']	= "No GL Account available for Supplier : $supplierName";
			return $response;
		}
	}
	private function GetPublicValues($select_fieldName,$tableName,$where_fieldId,$where_fieldValue)
	{
		$sql = "SELECT $select_fieldName AS VALUE FROM $tableName WHERE $where_fieldId = '$where_fieldValue'";
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		return $row["VALUE"];
	}
	private function Save_Jurnal_Transaction_sql($glId,$amount,$documentNo,$documentYear,$documentType,$invoiceNo,$invoiceYear,$transType,$transCategory,$transCatId,$currencyId)
	{
		global $session_companyId;
		global $session_locationId;
		global $session_userId;
		
		$sql = "INSERT INTO finance_transaction
				(CHART_OF_ACCOUNT_ID,
				AMOUNT,
				DOCUMENT_NO,
				DOCUMENT_YEAR,
				DOCUMENT_TYPE,
				INVOICE_NO,
				INVOICE_YEAR,
				TRANSACTION_TYPE,
				TRANSACTION_CATEGORY,
				TRANSACTION_CATEGORY_ID,
				CURRENCY_ID,
				LOCATION_ID,
				COMPANY_ID,
				LAST_MODIFIED_BY,
				LAST_MODIFIED_DATE)
				VALUES ('$glId',
				'$amount',
				'$documentNo',
				'$documentYear',
				'$documentType',
				$invoiceNo,
				$invoiceYear,
				'$transType',
				'$transCategory',
				'$transCatId',
				'$currencyId',
				'$session_locationId',
				'$session_companyId',
				'$session_userId',
				NOW());";				
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$response["type"]		= 'fail';
			$response["ErrorMsg"]	= $this->db->errormsg;
			$response["ErrorSql"]	= $sql;
		}
		return $response;
	}
	
	private function Save_Jurnal_Transaction_bkp_date_sql($glId,$amount,$documentNo,$documentYear,$documentType,$invoiceNo,$invoiceYear,$transType,$transCategory,$transCatId,$currencyId,$runDate)
	{
		global $session_companyId;
		global $session_locationId;
		global $session_userId;
		$runDate	= $runDate;
 
		    $sql = "INSERT INTO finance_transaction
				(CHART_OF_ACCOUNT_ID,
				AMOUNT,
				DOCUMENT_NO,
				DOCUMENT_YEAR,
				DOCUMENT_TYPE,
				INVOICE_NO,
				INVOICE_YEAR,
				TRANSACTION_TYPE,
				TRANSACTION_CATEGORY,
				TRANSACTION_CATEGORY_ID,
				CURRENCY_ID,
				LOCATION_ID,
				COMPANY_ID,
				LAST_MODIFIED_BY,
				LAST_MODIFIED_DATE)
				VALUES ('$glId',
				'$amount',
				'$documentNo',
				'$documentYear',
				'$documentType',
				$invoiceNo,
				$invoiceYear,
				'$transType',
				'$transCategory',
				$transCatId,
				'$currencyId',
				'$session_locationId',
				'$session_companyId',
				'$session_userId',
				'$runDate');";				
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$response["type"]		= 'fail';
			$response["ErrorMsg"]	= $this->db->errormsg;
			$response["ErrorSql"]	= $sql;
		}
		return $response;
	}
	
    private function getSerialNo_result($date,$company,$executeType)
	{
 		
		$sql = "SELECT  
				RIGHT(YEAR(DATE(dtmStartingDate)),2) AS startYear,
				RIGHT(YEAR(DATE(dtmClosingDate)),2)	 AS endYear, 	
				C.strCode							 AS compCode 
				FROM mst_financeaccountingperiod FAP
				INNER JOIN mst_financeaccountingperiod_companies FAPC ON FAP.intId=FAPC.intPeriodId 
				INNER JOIN mst_companies C ON FAPC.intCompanyId = C.intId
				WHERE dtmStartingDate<=DATE('$date') AND
				dtmClosingDate>=DATE('$date') AND
				FAPC.intCompanyId='$company' ";
		$result 	= $this->db->$executeType($sql);
		
		return $result;
	}
	
	private function getPaymentVoucherNo_sql($payDate,$bankGLId,$costCenter,$executeType)
	{
		global $session_companyId;
		$savedStatus	= true;
		$errorMsg		= '';
		
		$sql = "SELECT LPAD(MONTH('$payDate'),2,'0') AS voucherMonth,
				CONCAT(RIGHT(YEAR(DATE(dtmStartingDate)),2),'-',RIGHT(YEAR(DATE(dtmClosingDate)),2)) AS voucherYear,
				(SELECT LPAD(SERIAL_NO,5,'0') AS bankNo
				FROM finance_sys_bank_no
				WHERE BANK_ID='$bankGLId') AS bankNo,
				(SELECT strCostCenterCode 
				FROM mst_financedimension
				WHERE intId='$costCenter') AS costcenterCode
				FROM mst_financeaccountingperiod FAP
				INNER JOIN mst_financeaccountingperiod_companies FAPC ON FAP.intId=FAPC.intPeriodId
				WHERE dtmStartingDate<=DATE('$payDate') AND
				dtmClosingDate>=DATE('$payDate') AND
				FAPC.intCompanyId='$session_companyId' ";
		
		$result 	= $this->db->$executeType($sql);
		$row		= mysqli_fetch_array($result);
		if(($row['voucherYear']=='' || $row['voucherMonth']=='' || $row['bankNo']=='' || $row['costcenterCode']=='') && $savedStatus)
		{
			$savedStatus			= false;
			$errorMsg				= 'Voucher No Error. ';
		}

		$voucherNo 	= $row['voucherYear'].'/'.$row['voucherMonth'].'/'.$row['bankNo'].'/'.$row['costcenterCode'];
		
		$sqlUpd 	= "UPDATE finance_sys_bank_no SET SERIAL_NO=SERIAL_NO+1 WHERE BANK_ID='$bankGLId' ";
		$resultUpd  = $this->db->RunQuery2($sqlUpd);
		
		if(!$resultUpd && $savedStatus)
		{
			$savedStatus			= false;
			$errorMsg				= 'sys Bank No update error. ';
		}
		
		if($savedStatus)
		{
			$response['type']		= 'pass';
			$response['voucherNo']	= $voucherNo;
		}
		else
		{
			$response['type']		= 'fail';
			$response['msg']		= $errorMsg;
		}
		
		
		return $response;
	}
	private function getCompanyCostCenter_sql($locationId,$executeType)
	{
		$sql = "SELECT intId
				FROM mst_financedimension
				WHERE intLocation='$locationId' AND
				intStatus='1' ";
		
		$result = $this->db->$executeType($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row['intId'];
	}
	private function get_exchane_sql($currency,$date,$company){
		
	 	 $sql = "SELECT
				mst_financeexchangerate.dblExcAvgRate
				FROM `mst_financeexchangerate`
				WHERE
				mst_financeexchangerate.intCurrencyId = '$currency' AND
				mst_financeexchangerate.dtmDate = '$date' AND
				mst_financeexchangerate.intCompanyId = '$company' 
			";	
 		return $sql; 
 	}
	private function getSvatValue_sql($grandTotal,$executionType)
	{	
		$sql = "SELECT dblRate FROM mst_financetaxisolated WHERE intId='3' ";
		
		$result 	= $this->db->$executionType($sql);
		$row 		= mysqli_fetch_array($result);
		
		$svatValue 	= $grandTotal*($row['dblRate']/100);
		
		return round($svatValue,2);
	}
	
	private function getSvatValue_poSaved_sql($poNo,$poYear,$grandTotal,$executionType)
	{	
		$sql = "SELECT SVAT_RATE FROM trn_poheader WHERE intPONo='$poNo' and intPOYear = '$poYear' ";
		
		$result 	= $this->db->$executionType($sql);
		$row 		= mysqli_fetch_array($result);
		
		$svatValue 	= $grandTotal*($row['SVAT_RATE']/100);
		
		return round($svatValue,2);
	}
	
	private function bank_sql()
	{
		$sql = "SELECT BANK_ID,BANK_NAME
				FROM mst_bank
				WHERE STATUS = 1
				ORDER BY BANK_NAME ";
		return $sql;
	}
	private function bank_combo($sql,$bank)
	{
		$result = $this->db->RunQuery($sql);
		$string ="<option value=\"\"></option>";
		while($row = mysqli_fetch_array($result))
		{
			if($row['BANK_ID']==$bank)
				$string.="<option value=\"".$row['BANK_ID']."\" selected=\"selected\">".$row['BANK_NAME']."</option>";
			else
				$string.="<option value=\"".$row['BANK_ID']."\">".$row['BANK_NAME']."</option>";
		}
		return $string;
	}
	private function marketer_sql()
	{
		global $session_companyId;
		
		$sql = "SELECT DISTINCT M.intUserId,SU.strUserName AS marketer
				FROM mst_marketer M
				INNER JOIN sys_users SU ON SU.intUserId=M.intUserId
				INNER JOIN mst_locations_user LU ON SU.intUserId=LU.intUserId
				INNER JOIN mst_locations ML ON ML.intId=LU.intLocationId 
				WHERE ML.intCompanyId = '$session_companyId'
				ORDER BY SU.strUserName";
		return $sql;
	}
	private function marketer_combo($sql,$marketer)
	{
		$result = $this->db->RunQuery($sql);
		$string ="<option value=\"\"></option>";
		while($row = mysqli_fetch_array($result))
		{
			if($row['intUserId']==$marketer)
				$string.="<option value=\"".$row['intUserId']."\" selected=\"selected\">".$row['marketer']."</option>";
			else
				$string.="<option value=\"".$row['intUserId']."\">".$row['marketer']."</option>";
		}
		return $string;
	}
	
	private function SaveSupplierForcast_sql($grnNo,$grnYear)
	{
		global $session_companyId;
		global $objExchangeRateGet;
		global $objCashFlowForecastGet;
		global $objCashFlowForecastSet;
		
		$response["type"]		= true;
		$tblType				= "finance_forecast_transaction_supplier";		
		$grnArray				= $this->GetGRNDetails($grnNo,$grnYear);
		$sourceExArray			= $objExchangeRateGet->GetAllValues($grnArray["CURRENCY_ID"],2,$grnArray["GRN_APPROVE_DATE"],$session_companyId,'RunQuery2');
		$targetExArray			= $objExchangeRateGet->GetAllValues(1,2,$grnArray["GRN_APPROVE_DATE"],$session_companyId,'RunQuery2');
		$amount					= round(($grnArray["GRN_VALUE"]/$targetExArray['AVERAGE_RATE'])*$sourceExArray['AVERAGE_RATE'],2);

		$count = $objCashFlowForecastGet->IsRowAvailable($grnArray["GRN_CREDIT_DATE"],$grnArray["SUPPLIER_ID"],$session_companyId,$tblType);
		if($count>0)
		{
			if($response["type"])
				$response = $objCashFlowForecastSet->UpdateCustomerForcast($grnArray["GRN_CREDIT_DATE"],$grnArray["SUPPLIER_ID"],round($amount,2),$session_companyId,$tblType);
		}
		else
		{
			if($response["type"])
				$response = $objCashFlowForecastSet->InsertCustomerForcast($grnArray["GRN_CREDIT_DATE"],$grnArray["SUPPLIER_ID"],round($amount,2),$session_companyId,$tblType);			
		}	
		
		return $response;
	}
	
	private function UpdateSupplierForcast_sql($amount,$categoryId,$currencyId,$date)
	{
		global $session_companyId;
		global $objCashFlowForecastSet;
		global $objExchangeRateGet;
		$result['type']			= true;
		$sourceExArray			= $objExchangeRateGet->GetAllValues($currencyId,2,$date,$session_companyId,'RunQuery2');
		$targetExArray			= $objExchangeRateGet->GetAllValues(1,2,$date,$session_companyId,'RunQuery2');
		$amount					= round(($amount/$targetExArray['AVERAGE_RATE'])*$sourceExArray['AVERAGE_RATE'],2);
		$tblType				= "finance_forecast_transaction_supplier";
		$balAmount				= $amount;
		
		$sql = "SELECT *
				FROM finance_forecast_transaction_supplier
				WHERE BALANCE_AMOUNT > 0
					AND CATEGORY_ID = $categoryId
					AND COMPANY_ID = $session_companyId
				ORDER BY DATE";
		$result_1 = $this->db->RunQuery2($sql);
		while($row = mysqli_fetch_array($result_1))
		{
			if($balAmount>0)
			{			
				$tblbalAmount	= $row["BALANCE_AMOUNT"];
				if($balAmount>=$tblbalAmount)
				{
					$balAmount -= $tblbalAmount;
					$result = $objCashFlowForecastSet->UpdateCustomerForcastMinus($row["DATE"],$row["CATEGORY_ID"],round($tblbalAmount,2),$session_companyId,$tblType, $currencyId, $row["BANK_ACCOUNT_ID"]);
				}
				else
				{
					$result = $objCashFlowForecastSet->UpdateCustomerForcastMinus($row["DATE"],$row["CATEGORY_ID"],round($balAmount,2),$session_companyId,$tblType, $currencyId, $row["BANK_ACCOUNT_ID"]);
					$balAmount	-= $balAmount;
				}
			}
		}
		return $result;
	}
	
	private function GetGRNDetails($grnNo,$grnYear)
	{
		$sql = "SELECT
				  ROUND(SUM(GD.dblGrnQty * dblGrnRate),2) 	AS GRN_VALUE,
				  (SELECT
					DATE(dtApprovedDate)
				  FROM ware_grnheader_approvedby GHA
				  WHERE GHA.intGrnNo = GH.intGrnNo
					AND GHA.intYear = GHA.intYear
					AND GHA.intStatus = 0
				  ORDER BY intApproveLevelNo DESC
				   LIMIT 1)									AS GRN_APPROVE_DATE,
				  DATE_ADD(DATE((SELECT
					DATE(dtApprovedDate)
				  FROM ware_grnheader_approvedby GHA
				  WHERE GHA.intGrnNo = GH.intGrnNo
					AND GHA.intYear = GHA.intYear
					AND GHA.intStatus = 0
				  ORDER BY intApproveLevelNo DESC
				   LIMIT 1)),INTERVAL FPT.strName DAY)		AS GRN_CREDIT_DATE,
				  PH.intCurrency 							AS CURRENCY_ID,
				  PH.intSupplier							AS SUPPLIER_ID
				FROM ware_grnheader GH
				INNER JOIN ware_grndetails GD
					ON GD.intGrnNo = GH.intGrnNo
					  AND GD.intGrnYear = GH.intGrnYear
				INNER JOIN trn_poheader PH
					ON PH.intPONo = GH.intPoNo
					  AND PH.intPOYear = GH.intPoYear
				INNER JOIN mst_financepaymentsterms FPT
					ON FPT.intId = PH.intPaymentTerm
				WHERE GH.intGrnNo = '$grnNo'
					AND GH.intGrnYear = '$grnYear'";
		$result = $this->db->RunQuery2($sql);
		return mysqli_fetch_array($result);
	}
}
?>
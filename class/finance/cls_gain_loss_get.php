<?php
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];
class cls_gain_loss_get
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}

	public function get_customers_results($year,$month)
	{		
		$sql 	= $this->customers_sql($year,$month);
		$result = $this->db->RunQuery2($sql);
		return $result;
	}
	public function get_header_report_results($year,$month)
	{		
		$sql 	= $this->header_report_results($year,$month);
		$result = $this->db->RunQuery2($sql);
		return $result;
	}
	public function get_customer_gainLoss_results($customer,$lastMonthEnd,$runDate)
	{		
		$sql 	= $this->customer_gainLoss_sql($customer,$lastMonthEnd,$runDate);
		$result = $this->db->RunQuery2($sql);
		return $result;
	}
	public function get_customer_gainLoss_report_results($customer,$runDate,$lastMonthEnd)
	{		
		$sql 	= $this->customer_gainLoss_report_sql($customer,$runDate,$lastMonthEnd);
		$result = $this->db->RunQuery2($sql);
		return $result;
	}
	public function get_customer_gainLoss_report_records($customer,$runDate,$lastMonthEnd)
	{		
		$sql 	= $this->customer_gainLoss_report_sql($customer,$runDate,$lastMonthEnd);
		$result = $this->db->RunQuery2($sql);
		$records=$this->db->effectRows;
		return $records;
	}
	public function get_suppliers_results($year,$month)
	{		
		$sql 	= $this->suppliers_sql($year,$month);
		$result = $this->db->RunQuery2($sql);
		return $result;
	}
 	public function get_supplier_gainLoss_results($supplier,$lastMonthEnd,$runDate)
	{		
		$sql 	= $this->supplier_gainLoss_sql($supplier,$lastMonthEnd,$runDate);
		$result = $this->db->RunQuery2($sql);
		return $result;
	}
	public function get_supplier_gainLoss_report_results($supplier,$runDate,$lastMonthEnd)
	{		
		$sql 	= $this->supplier_gainLoss_report_sql($supplier,$runDate,$lastMonthEnd);
		$result = $this->db->RunQuery2($sql);
		return $result;
	}
	public function get_supplier_gainLoss_report_records($supplier,$runDate,$lastMonthEnd)
	{		
		$sql 	= $this->supplier_gainLoss_report_sql($supplier,$runDate,$lastMonthEnd);
		$result = $this->db->RunQuery2($sql);
		$records=$this->db->effectRows;
		return $records;
	}
	public function get_other_payble_results()
	{		
		$sql 	= $this->other_payble_sql();
		$result = $this->db->RunQuery2($sql);
		return $result;
	}
 	public function get_other_payble_gainLoss_results($supplier,$lastMonthEnd,$runDate)
	{		
		$sql 	= $this->other_payble_gainLoss_sql($supplier,$lastMonthEnd,$runDate);
		$result = $this->db->RunQuery2($sql);
		return $result;
	}
 	public function get_other_payble_gainLoss_report_results($supplier,$runDate,$lastMonthEnd)
	{		
		$sql 	= $this->other_payble_gainLoss_report_sql($supplier,$runDate,$lastMonthEnd);
		$result = $this->db->RunQuery2($sql);
		return $result;
	}
	public function get_other_payble_gainLoss_report_records($supplier,$runDate,$lastMonthEnd)
	{		
		$sql 	= $this->other_payble_gainLoss_report_sql($supplier,$runDate,$lastMonthEnd);
		$result = $this->db->RunQuery2($sql);
		$records=$this->db->effectRows;
		return $records;
	}
	public function get_process1_gain_loss_results($startDateThisMonth,$endDateThisMonth)
	{
		return $this->get_process1_gain_loss_results_sql($startDateThisMonth,$endDateThisMonth);
	}
	public function get_process2_gain_loss_results($startDateThisMonth,$endDateThisMonth)
	{
		return $this->get_process2_gain_loss_results_sql($startDateThisMonth,$endDateThisMonth);
	}
	public function get_process3_gain_loss_results($startDateThisMonth,$endDateThisMonth)
	{
		return $this->get_process3_gain_loss_results_sql($startDateThisMonth,$endDateThisMonth);
	}
	public function get_process4_gain_loss_results($startDateThisMonth,$endDateThisMonth)
	{
		return $this->get_process4_gain_loss_results_sql($startDateThisMonth,$endDateThisMonth);
	}
	public function getChartOfAccName($glId)
	{
		return $this->getChartOfAccName_sql($glId);
	}
	private function customers_sql($year,$month)
	{
		$sql="SELECT DISTINCT
				mst_customer.intId,
				mst_customer.strName 
				FROM 
				(SELECT DISTINCT
				finance_customer_transaction.CUSTOMER_ID,
				finance_customer_transaction.TRANSACTION_DATE_TIME
				FROM `finance_customer_transaction`
				WHERE
				MONTH(finance_customer_transaction.TRANSACTION_DATE_TIME) = '$month' AND 
				YEAR(finance_customer_transaction.TRANSACTION_DATE_TIME) = '$year' ) as TB1 
				INNER JOIN mst_customer ON mst_customer.intId = TB1.CUSTOMER_ID
 				ORDER BY
				mst_customer.strName ASC
				";
		return $sql;
	}
 	private function header_report_results($year,$month)
	{
		$sql="SELECT
				finance_gain_loss_header.DOCUMENT_NO,
				finance_gain_loss_header.DOCUMENT_YEAR,
				MC.strName,
				finance_gain_loss_header.CATEGORY_ID,
				finance_gain_loss_header.CATEGORY_TYPE,
				finance_gain_loss_header.`YEAR`,
				finance_gain_loss_header.`MONTH`,
				finance_gain_loss_header.`DAY`,
				DATE(finance_gain_loss_header.TIME) as DATE 
				FROM `finance_gain_loss_header`
				LEFT JOIN mst_customer AS MC ON finance_gain_loss_header.CATEGORY_ID=MC.intId
				WHERE
				finance_gain_loss_header.`YEAR` = '$year' AND
				finance_gain_loss_header.`MONTH` = '$month' ";
		return $sql;
	}
 	private function customer_gainLoss_sql($customer,$lastMonthEnd,$runDate)
	{
		global $session_companyId;
		
		  $sql = "SELECT 
		  		TB_GAIN.SERIAL_NO,
				TB_GAIN.SERIAL_YEAR,
				SUM(TB_GAIN.GAINLOSS) as GAINLOSS 
				FROM ((
				SELECT
				CT.COMPANY_ID,
				CT.CUSTOMER_ID,
				MC.strName as CUSTOMER,
				CT.CURRENCY_ID,
				CIH.SERIAL_NO AS SERIAL_NO,
				CIH.SERIAL_YEAR AS SERIAL_YEAR,
				CIH.INVOICE_NO AS INVOICE_NO,
				CIH.ORDER_NO AS ORDER_NO,
				CIH.ORDER_YEAR AS ORDER_YEAR,
				CIH.INVOICED_DATE AS INVOICED_DATE,
				'$lastMonthEnd' AS LAST_GAIN_TRNS_DATE,
				 FEC.dblExcAvgRate AS EXC_RATE_TODAY,
				(if(DATEDIFF(CIH.INVOICED_DATE,'$lastMonthEnd')>0,CIH.INVOICED_DATE,'$lastMonthEnd')) AS DATE_LAST,
				FEL.dblExcAvgRate AS EXC_RATE_LAST,
				ROUND(SUM(CT.VALUE),2) AS `VALUE` ,
				 -- ROUND(ROUND(SUM(CT.VALUE),2)*(FEC.dblExcAvgRate-FEL.dblExcAvgRate),4) AS GAINLOSS,
				 ROUND(SUM(ABS(CT.VALUE)*(FEC.dblExcAvgRate-FEL.dblExcAvgRate)),4) AS GAINLOSS,
				 'INVOICE_BAL_GAIN' AS DOCUMENT_TYPE
				FROM
				finance_customer_transaction AS CT
				INNER JOIN finance_customer_invoice_header AS CIH ON CIH.SERIAL_YEAR = CT.INVOICE_YEAR AND CIH.SERIAL_NO = CT.INVOICE_NO
				INNER JOIN mst_financeexchangerate AS FEC ON CT.CURRENCY_ID = FEC.intCurrencyId AND CT.COMPANY_ID = FEC.intCompanyId AND FEC.dtmDate = DATE('$runDate')
				INNER JOIN mst_financeexchangerate AS FEL ON CT.CURRENCY_ID = FEL.intCurrencyId AND CT.COMPANY_ID = FEL.intCompanyId AND FEL.dtmDate = DATE((if(DATEDIFF(CIH.INVOICED_DATE,'$lastMonthEnd')<0,CIH.INVOICED_DATE,'$lastMonthEnd')))
				LEFT JOIN mst_customer AS MC ON CT.CUSTOMER_ID=MC.intId
				WHERE CT.CUSTOMER_ID='$customer' AND
				DATEDIFF(DATE(CT.TRANSACTION_DATE_TIME),DATE('$runDate'))<=0 AND
				CT.COMPANY_ID = '$session_companyId'
				GROUP BY
				CT.COMPANY_ID,
				CT.CUSTOMER_ID,
				CT.CURRENCY_ID,
				CIH.INVOICE_NO 
				HAVING VALUE > 0
				
				)
				UNION  
				(
				SELECT
				CT.COMPANY_ID,
				CT.CUSTOMER_ID,
				MC.strName as CUSTOMER,
				CT.CURRENCY_ID,
				CIH.SERIAL_NO AS SERIAL_NO,
				CIH.SERIAL_YEAR AS SERIAL_YEAR,
				CIH.INVOICE_NO AS INVOICE_NO,
				CIH.ORDER_NO AS ORDER_NO,
				CIH.ORDER_YEAR AS ORDER_YEAR,
				CIH.INVOICED_DATE AS INVOICED_DATE,
				'$lastMonthEnd' AS LAST_GAIN_TRNS_DATE,
				 FEC.dblExcAvgRate AS EXC_RATE_TODAY,
				(if(DATEDIFF(CIH.INVOICED_DATE,'$lastMonthEnd')>0,CIH.INVOICED_DATE,'$lastMonthEnd')) AS DATE_LAST,
				FEL.dblExcAvgRate AS EXC_RATE_LAST,
				ROUND(SUM(CT.VALUE),2) AS `VALUE`,
				 ROUND(SUM(ABS(CT.VALUE)*(FEC.dblExcAvgRate-FEL.dblExcAvgRate)),4) AS GAINLOSS,
				 'PAYRECEIVE_TOT_GAIN' AS DOCUMENT_TYPE
				FROM
				finance_customer_transaction AS CT
				INNER JOIN finance_customer_invoice_header AS CIH ON CIH.SERIAL_YEAR = CT.INVOICE_YEAR AND CIH.SERIAL_NO = CT.INVOICE_NO
				INNER JOIN mst_financeexchangerate AS FEC ON CT.CURRENCY_ID = FEC.intCurrencyId AND CT.COMPANY_ID = FEC.intCompanyId AND FEC.dtmDate = DATE(CT.TRANSACTION_DATE_TIME) 
				INNER JOIN mst_financeexchangerate AS FEL ON CT.CURRENCY_ID = FEL.intCurrencyId AND CT.COMPANY_ID = FEL.intCompanyId AND FEL.dtmDate = DATE((if(DATEDIFF(CIH.INVOICED_DATE,'$lastMonthEnd')<0,CIH.INVOICED_DATE,'$lastMonthEnd')))
				LEFT JOIN mst_customer AS MC ON CT.CUSTOMER_ID=MC.intId
				WHERE
				(CT.DOCUMENT_TYPE = 'PAYRECEIVE'  OR CT.DOCUMENT_TYPE = 'EXRECEIVE' ) AND 
				DATEDIFF(DATE(CT.TRANSACTION_DATE_TIME),DATE('$runDate'))<=0 AND 
				CT.CUSTOMER_ID='$customer' AND
				CT.COMPANY_ID = '$session_companyId'
				GROUP BY
				CT.COMPANY_ID,
				CT.CUSTOMER_ID,
				CT.CURRENCY_ID,
				CIH.INVOICE_NO  
				
				)
				) as TB_GAIN 
 				";
		return $sql;
	}
 	private function customer_gainLoss_report_sql($customer,$runDate,$lastMonthEnd)
	{
		global $session_companyId;
		
		    $sql = "SELECT 
		  		TB_GAIN.SERIAL_NO,
				TB_GAIN.SERIAL_YEAR, 
				SUM(TB_GAIN.GAINLOSS) as GAINLOSS 
				FROM ((
				SELECT
				CT.COMPANY_ID,
				CT.CUSTOMER_ID,
				MC.strName as CUSTOMER,
				CT.CURRENCY_ID,
				CIH.SERIAL_NO AS SERIAL_NO,
				CIH.SERIAL_YEAR AS SERIAL_YEAR,
				CIH.INVOICE_NO AS INVOICE_NO,
				CIH.ORDER_NO AS ORDER_NO,
				CIH.ORDER_YEAR AS ORDER_YEAR,
				CIH.INVOICED_DATE AS INVOICED_DATE,
				'$lastMonthEnd' AS LAST_GAIN_TRNS_DATE,
				 FEC.dblExcAvgRate AS EXC_RATE_TODAY,
				(if(DATEDIFF(CIH.INVOICED_DATE,'$lastMonthEnd')>0,CIH.INVOICED_DATE,'$lastMonthEnd')) AS DATE_LAST,
				FEL.dblExcAvgRate AS EXC_RATE_LAST,
				ROUND(SUM(CT.VALUE),2) AS `VALUE` ,
				 ROUND(SUM(ABS(CT.VALUE)*(FEC.dblExcAvgRate-FEL.dblExcAvgRate)),4) AS GAINLOSS,
				 'INVOICE_BAL_GAIN' AS DOCUMENT_TYPE
				FROM
				finance_customer_transaction AS CT
				INNER JOIN finance_customer_invoice_header AS CIH ON CIH.SERIAL_YEAR = CT.INVOICE_YEAR AND CIH.SERIAL_NO = CT.INVOICE_NO
				INNER JOIN mst_financeexchangerate AS FEC ON CT.CURRENCY_ID = FEC.intCurrencyId AND CT.COMPANY_ID = FEC.intCompanyId AND FEC.dtmDate = DATE('$runDate')
				INNER JOIN mst_financeexchangerate AS FEL ON CT.CURRENCY_ID = FEL.intCurrencyId AND CT.COMPANY_ID = FEL.intCompanyId AND FEL.dtmDate = DATE((if(DATEDIFF(CIH.INVOICED_DATE,'$lastMonthEnd')<0,CIH.INVOICED_DATE,'$lastMonthEnd')))
				LEFT JOIN mst_customer AS MC ON CT.CUSTOMER_ID=MC.intId
				WHERE CT.CUSTOMER_ID='$customer' AND 
				DATEDIFF(DATE(CT.TRANSACTION_DATE_TIME),DATE('$runDate'))<=0 AND
				CT.COMPANY_ID = '$session_companyId'
				GROUP BY
				CT.COMPANY_ID,
				CT.CUSTOMER_ID,
				CT.CURRENCY_ID,
				CIH.INVOICE_NO 
				HAVING VALUE > 0
				
				)
				UNION  
				(
				SELECT
				CT.COMPANY_ID,
				CT.CUSTOMER_ID,
				MC.strName as CUSTOMER,
				CT.CURRENCY_ID,
				CIH.SERIAL_NO AS SERIAL_NO,
				CIH.SERIAL_YEAR AS SERIAL_YEAR,
				CIH.INVOICE_NO AS INVOICE_NO,
				CIH.ORDER_NO AS ORDER_NO,
				CIH.ORDER_YEAR AS ORDER_YEAR,
				CIH.INVOICED_DATE AS INVOICED_DATE,
				'$lastMonthEnd' AS LAST_GAIN_TRNS_DATE,
				 FEC.dblExcAvgRate AS EXC_RATE_TODAY,
				(if(DATEDIFF(CIH.INVOICED_DATE,'$lastMonthEnd')>0,CIH.INVOICED_DATE,'$lastMonthEnd')) AS DATE_LAST,
				FEL.dblExcAvgRate AS EXC_RATE_LAST,
				ROUND(SUM(CT.VALUE),2) AS `VALUE`,
				 ROUND(SUM(ABS(CT.VALUE)*(FEC.dblExcAvgRate-FEL.dblExcAvgRate)),4) AS GAINLOSS,
				 'PAYRECEIVE_TOT_GAIN' AS DOCUMENT_TYPE
				FROM
				finance_customer_transaction AS CT
				INNER JOIN finance_customer_invoice_header AS CIH ON CIH.SERIAL_YEAR = CT.INVOICE_YEAR AND CIH.SERIAL_NO = CT.INVOICE_NO
				INNER JOIN mst_financeexchangerate AS FEC ON CT.CURRENCY_ID = FEC.intCurrencyId AND CT.COMPANY_ID = FEC.intCompanyId AND FEC.dtmDate = DATE(CT.TRANSACTION_DATE_TIME) 
				INNER JOIN mst_financeexchangerate AS FEL ON CT.CURRENCY_ID = FEL.intCurrencyId AND CT.COMPANY_ID = FEL.intCompanyId AND FEL.dtmDate = DATE((if(DATEDIFF(CIH.INVOICED_DATE,'$lastMonthEnd')<0,CIH.INVOICED_DATE,'$lastMonthEnd')))
				LEFT JOIN mst_customer AS MC ON CT.CUSTOMER_ID=MC.intId
				WHERE
				(CT.DOCUMENT_TYPE = 'PAYRECEIVE' OR CT.DOCUMENT_TYPE = 'EXRECEIVE' ) AND  
				CT.CUSTOMER_ID='$customer'  AND 
				DATEDIFF(DATE(CT.TRANSACTION_DATE_TIME),DATE('$runDate'))<=0 AND
				CT.COMPANY_ID = '$session_companyId'
				GROUP BY
				CT.COMPANY_ID,
				CT.CUSTOMER_ID,
				CT.CURRENCY_ID,
				CIH.INVOICE_NO  
				
				)
				) as TB_GAIN
				HAVING  GAINLOSS <> 0
				";
 		return $sql;
	}
	private function suppliers_sql($year,$month)
	{
		$sql="SELECT DISTINCT
				mst_supplier.intId,
				mst_supplier.strName
				FROM 
				(SELECT DISTINCT
				finance_supplier_transaction.SUPPLIER_ID,
				finance_supplier_transaction.TRANSACTION_DATE_TIME
				FROM `finance_supplier_transaction`
				WHERE
				MONTH(finance_supplier_transaction.TRANSACTION_DATE_TIME) = '$month' AND 
				YEAR(finance_supplier_transaction.TRANSACTION_DATE_TIME) = '$year') as TB1 
				INNER JOIN mst_supplier ON mst_supplier.intId = TB1.SUPPLIER_ID
 				ORDER BY
				mst_supplier.strName ASC
				";
				
		return $sql;
	}
	private function other_payble_sql()
	{
		  $sql="SELECT
				mst_supplier.intId,
				mst_supplier.strName
				FROM
				mst_supplier
				WHERE
				mst_supplier.intStatus = '1' AND
				mst_supplier.intTypeId = '14'
				ORDER BY
				mst_supplier.strName ASC
				";
		return $sql;
	}
  	private function supplier_gainLoss_sql($supplier,$lastMonthEnd,$runDate)
	{
		global $session_companyId;
		
		    $sql = "SELECT 
				TB_GAIN.SERIAL_NO,
				TB_GAIN.SERIAL_YEAR,
				SUM(TB_GAIN.GAINLOSS) as GAINLOSS 
				FROM ((
				SELECT
				ST.COMPANY_ID,
				ST.LEDGER_ID,
				MS.strName as CUSTOMER,
				ST.CURRENCY_ID,
				SIH.PURCHASE_INVOICE_NO AS SERIAL_NO,
				SIH.PURCHASE_INVOICE_YEAR AS SERIAL_YEAR,
				SIH.INVOICE_NO AS INVOICE_NO,
				/*SIH.ORDER_NO AS ORDER_NO,
				SIH.ORDER_YEAR AS ORDER_YEAR,*/
				SIH.PURCHASE_DATE AS INVOICED_DATE,
				'$lastMonthEnd' AS LAST_GAIN_TRNS_DATE,
				 FEC.dblExcAvgRate AS EXC_RATE_TODAY,
				(if(DATEDIFF(SIH.PURCHASE_DATE,'$lastMonthEnd')>0,SIH.PURCHASE_DATE,'$lastMonthEnd')) AS DATE_LAST,
				FEL.dblExcAvgRate AS EXC_RATE_LAST,
				ROUND(SUM(ST.VALUE),2) AS `VALUE` ,
				 ROUND(SUM(ABS(ST.VALUE)*(FEC.dblExcAvgRate-FEL.dblExcAvgRate)),4) AS GAINLOSS,
				 'INVOICE_BAL_GAIN' AS DOCUMENT_TYPE
				FROM
				finance_supplier_transaction AS ST
				INNER JOIN finance_supplier_purchaseinvoice_header AS SIH ON SIH.PURCHASE_INVOICE_YEAR = ST.PURCHASE_INVOICE_YEAR AND SIH.PURCHASE_INVOICE_NO = ST.PURCHASE_INVOICE_NO
				INNER JOIN mst_financeexchangerate AS FEC ON ST.CURRENCY_ID = FEC.intCurrencyId AND ST.COMPANY_ID = FEC.intCompanyId AND FEC.dtmDate = DATE('$runDate')
				INNER JOIN mst_financeexchangerate AS FEL ON ST.CURRENCY_ID = FEL.intCurrencyId AND ST.COMPANY_ID = FEL.intCompanyId AND FEL.dtmDate = DATE((if(DATEDIFF(SIH.PURCHASE_DATE,'$lastMonthEnd')<0,SIH.PURCHASE_DATE,'$lastMonthEnd')))
 				LEFT JOIN mst_supplier AS MS ON SIH.SUPPLIER_ID=MS.intId
				WHERE 
  				SIH.SUPPLIER_ID='$supplier' AND
				DATEDIFF(DATE(ST.TRANSACTION_DATE_TIME),DATE('$runDate'))<=0 AND
				ST.COMPANY_ID = '$session_companyId'
				GROUP BY
				ST.COMPANY_ID,
				ST.LEDGER_ID,
				ST.CURRENCY_ID,
				SIH.INVOICE_NO 
				HAVING VALUE > 0
				
				)
				UNION  
				(
				SELECT
				ST.COMPANY_ID,
				ST.LEDGER_ID,
				MS.strName as CUSTOMER,
				ST.CURRENCY_ID,
				SIH.PURCHASE_INVOICE_NO AS SERIAL_NO,
				SIH.PURCHASE_INVOICE_YEAR AS SERIAL_YEAR,
				SIH.INVOICE_NO AS INVOICE_NO,
				/*SIH.ORDER_NO AS ORDER_NO,
				SIH.ORDER_YEAR AS ORDER_YEAR,*/
				SIH.PURCHASE_DATE AS INVOICED_DATE,
				'$lastMonthEnd' AS LAST_GAIN_TRNS_DATE,
				 FEC.dblExcAvgRate AS EXC_RATE_TODAY,
				(if(DATEDIFF(SIH.PURCHASE_DATE,'$lastMonthEnd')>0,SIH.PURCHASE_DATE,'$lastMonthEnd')) AS DATE_LAST,
				FEL.dblExcAvgRate AS EXC_RATE_LAST,
				ROUND(SUM(ST.VALUE),2) AS `VALUE`,
				 ROUND(SUM(ABS(ST.VALUE)*(FEC.dblExcAvgRate-FEL.dblExcAvgRate)),4) AS GAINLOSS,
				 'PAYMENT_TOT_GAIN' AS DOCUMENT_TYPE
				FROM
				finance_supplier_transaction AS ST
				INNER JOIN finance_supplier_purchaseinvoice_header AS SIH ON SIH.PURCHASE_INVOICE_YEAR = ST.PURCHASE_INVOICE_YEAR AND SIH.PURCHASE_INVOICE_NO = ST.PURCHASE_INVOICE_NO
				INNER JOIN mst_financeexchangerate AS FEC ON ST.CURRENCY_ID = FEC.intCurrencyId AND ST.COMPANY_ID = FEC.intCompanyId AND FEC.dtmDate = DATE(ST.TRANSACTION_DATE_TIME) 
				INNER JOIN mst_financeexchangerate AS FEL ON ST.CURRENCY_ID = FEL.intCurrencyId AND ST.COMPANY_ID = FEL.intCompanyId AND FEL.dtmDate = DATE((if(DATEDIFF(SIH.PURCHASE_DATE,'$lastMonthEnd')<0,SIH.PURCHASE_DATE,'$lastMonthEnd')))
 				LEFT JOIN mst_supplier AS MS ON SIH.SUPPLIER_ID=MS.intId
				WHERE 
  				SIH.SUPPLIER_ID='$supplier' AND 
				DATEDIFF(DATE(ST.TRANSACTION_DATE_TIME),DATE('$runDate'))<=0 AND
				ST.DOCUMENT_TYPE = 'PAYMENT' AND
				ST.COMPANY_ID = '$session_companyId'
 				GROUP BY
				ST.COMPANY_ID,
				ST.LEDGER_ID,
				ST.CURRENCY_ID,
				SIH.INVOICE_NO  
				
				)
				) as TB_GAIN 
 				";
		return $sql;
	}
 	private function supplier_gainLoss_report_sql($supplier,$runDate,$lastMonthEnd)
	{
		global $session_companyId;
		
		      $sql = "SELECT 
		  		TB_GAIN.SERIAL_NO,
				TB_GAIN.SERIAL_YEAR, 
				SUM(TB_GAIN.GAINLOSS) as GAINLOSS 
				FROM ((
				SELECT
				ST.COMPANY_ID,
				ST.LEDGER_ID,
				MS.strName as CUSTOMER,
				ST.CURRENCY_ID,
				SIH.PURCHASE_INVOICE_NO AS SERIAL_NO,
				SIH.PURCHASE_INVOICE_YEAR AS SERIAL_YEAR,
				SIH.INVOICE_NO AS INVOICE_NO,
				/*SIH.ORDER_NO AS ORDER_NO,
				SIH.ORDER_YEAR AS ORDER_YEAR,*/
				SIH.PURCHASE_DATE AS INVOICED_DATE,
				'$lastMonthEnd' AS LAST_GAIN_TRNS_DATE,
				 FEC.dblExcAvgRate AS EXC_RATE_TODAY,
				(if(DATEDIFF(SIH.PURCHASE_DATE,'$lastMonthEnd')>0,SIH.PURCHASE_DATE,'$lastMonthEnd')) AS DATE_LAST,
				FEL.dblExcAvgRate AS EXC_RATE_LAST,
				ROUND(SUM(ST.VALUE),2) AS `VALUE` ,
				 ROUND(SUM(ABS(ST.VALUE)*(FEC.dblExcAvgRate-FEL.dblExcAvgRate)),4) AS GAINLOSS,
				 'INVOICE_BAL_GAIN' AS DOCUMENT_TYPE
				FROM
				finance_supplier_transaction AS ST
				INNER JOIN finance_supplier_purchaseinvoice_header AS SIH ON SIH.PURCHASE_INVOICE_YEAR = ST.PURCHASE_INVOICE_YEAR AND SIH.PURCHASE_INVOICE_NO = ST.PURCHASE_INVOICE_NO
				INNER JOIN mst_financeexchangerate AS FEC ON ST.CURRENCY_ID = FEC.intCurrencyId AND ST.COMPANY_ID = FEC.intCompanyId AND FEC.dtmDate = DATE('$runDate')
				INNER JOIN mst_financeexchangerate AS FEL ON ST.CURRENCY_ID = FEL.intCurrencyId AND ST.COMPANY_ID = FEL.intCompanyId AND FEL.dtmDate = DATE((if(DATEDIFF(SIH.PURCHASE_DATE,'$lastMonthEnd')<0,SIH.PURCHASE_DATE,'$lastMonthEnd')))
 				LEFT JOIN mst_supplier AS MS ON SIH.SUPPLIER_ID=MS.intId
				WHERE 
  				SIH.SUPPLIER_ID='$supplier' AND 
				DATEDIFF(DATE(ST.TRANSACTION_DATE_TIME),DATE('$runDate'))<=0 AND
				ST.COMPANY_ID = '$session_companyId' 
				GROUP BY
				ST.COMPANY_ID,
				ST.LEDGER_ID,
				ST.CURRENCY_ID,
				SIH.INVOICE_NO  
				HAVING VALUE > 0
				
				)
				UNION  
				(
				SELECT
				ST.COMPANY_ID,
				ST.LEDGER_ID,
				MS.strName as CUSTOMER,
				ST.CURRENCY_ID,
				SIH.PURCHASE_INVOICE_NO AS SERIAL_NO,
				SIH.PURCHASE_INVOICE_YEAR AS SERIAL_YEAR,
				SIH.INVOICE_NO AS INVOICE_NO,
				/*SIH.ORDER_NO AS ORDER_NO,
				SIH.ORDER_YEAR AS ORDER_YEAR,*/
				SIH.PURCHASE_DATE AS INVOICED_DATE,
				'$lastMonthEnd' AS LAST_GAIN_TRNS_DATE,
				 FEC.dblExcAvgRate AS EXC_RATE_TODAY,
				(if(DATEDIFF(SIH.PURCHASE_DATE,'$lastMonthEnd')>0,SIH.PURCHASE_DATE,'$lastMonthEnd')) AS DATE_LAST,
				FEL.dblExcAvgRate AS EXC_RATE_LAST,
				ROUND(SUM(ST.VALUE),2) AS `VALUE`,
				 ROUND(SUM(ABS(ST.VALUE)*(FEC.dblExcAvgRate-FEL.dblExcAvgRate)),4) AS GAINLOSS,
				 'PAYMENT_TOT_GAIN' AS DOCUMENT_TYPE
				FROM
				finance_supplier_transaction AS ST
				INNER JOIN finance_supplier_purchaseinvoice_header AS SIH ON SIH.PURCHASE_INVOICE_YEAR = ST.PURCHASE_INVOICE_YEAR AND SIH.PURCHASE_INVOICE_NO = ST.PURCHASE_INVOICE_NO
				INNER JOIN mst_financeexchangerate AS FEC ON ST.CURRENCY_ID = FEC.intCurrencyId AND ST.COMPANY_ID = FEC.intCompanyId AND FEC.dtmDate = DATE(ST.TRANSACTION_DATE_TIME) 
				INNER JOIN mst_financeexchangerate AS FEL ON ST.CURRENCY_ID = FEL.intCurrencyId AND ST.COMPANY_ID = FEL.intCompanyId AND FEL.dtmDate = DATE((if(DATEDIFF(SIH.PURCHASE_DATE,'$lastMonthEnd')<0,SIH.PURCHASE_DATE,'$lastMonthEnd')))
 				LEFT JOIN mst_supplier AS MS ON SIH.SUPPLIER_ID=MS.intId
				WHERE 
  				SIH.SUPPLIER_ID='$supplier' AND  
				ST.DOCUMENT_TYPE = 'PAYMENT' AND  
 				DATEDIFF(DATE(ST.TRANSACTION_DATE_TIME),DATE('$runDate'))<=0 AND
				ST.COMPANY_ID = '$session_companyId' 
				GROUP BY
				ST.COMPANY_ID,
				ST.LEDGER_ID,
				ST.CURRENCY_ID,
				SIH.INVOICE_NO  
				
				)
				) as TB_GAIN
				HAVING  GAINLOSS <> 0
				";
 		return $sql;
	}

  	private function other_payble_gainLoss_sql($supplier,$lastMonthEnd,$runDate)
	{
		global $session_companyId;
		
		      $sql = "SELECT 
			  	TB_GAIN.SERIAL_NO,
				TB_GAIN.SERIAL_YEAR,
				SUM(TB_GAIN.GAINLOSS) as GAINLOSS 
				FROM ((
				SELECT
				ST.COMPANY_ID,
 				MS.strName as CUSTOMER,
				ST.CURRENCY_ID,
				SIH.BILL_INVOICE_NO AS SERIAL_NO,
				SIH.BILL_INVOICE_YEAR AS SERIAL_YEAR,
 				/*SIH.ORDER_NO AS ORDER_NO,
				SIH.ORDER_YEAR AS ORDER_YEAR,*/
				SIH.BILL_DATE AS INVOICED_DATE,
				'$lastMonthEnd' AS LAST_GAIN_TRNS_DATE,
				 FEC.dblExcAvgRate AS EXC_RATE_TODAY,
				(if(DATEDIFF(SIH.BILL_DATE,'$lastMonthEnd')>0,SIH.BILL_DATE,'$lastMonthEnd')) AS DATE_LAST,
				FEL.dblExcAvgRate AS EXC_RATE_LAST,
				ROUND(SUM(ST.VALUE),2) AS `VALUE` ,
				 ROUND(SUM(ABS(ST.VALUE)*(FEC.dblExcAvgRate-FEL.dblExcAvgRate)),4) AS GAINLOSS,
				 'INVOICE_BAL_GAIN' AS DOCUMENT_TYPE
				FROM
				finance_other_payable_transaction AS ST
				INNER JOIN finance_other_payable_bill_header AS SIH ON SIH.BILL_INVOICE_YEAR = ST.BILL_INVOICE_YEAR AND SIH.BILL_INVOICE_NO = ST.BILL_INVOICE_NO
				INNER JOIN mst_financeexchangerate AS FEC ON ST.CURRENCY_ID = FEC.intCurrencyId AND ST.COMPANY_ID = FEC.intCompanyId AND FEC.dtmDate = DATE('$runDate')
				INNER JOIN mst_financeexchangerate AS FEL ON ST.CURRENCY_ID = FEL.intCurrencyId AND ST.COMPANY_ID = FEL.intCompanyId AND FEL.dtmDate = DATE((if(DATEDIFF(SIH.BILL_DATE,'$lastMonthEnd')<0,SIH.BILL_DATE,'$lastMonthEnd')))
 				LEFT JOIN mst_supplier AS MS ON SIH.SUPPLIER_ID=MS.intId
				WHERE 
  				SIH.SUPPLIER_ID='$supplier' AND
				DATEDIFF(DATE(ST.TRANSACTION_DATE_TIME),DATE('$runDate'))<=0 AND
				ST.COMPANY_ID = '$session_companyId'
				GROUP BY
				ST.COMPANY_ID,
				ST.LEDGER_ID,
				ST.CURRENCY_ID,
				SIH.BILL_INVOICE_NO,
				SIH.BILL_INVOICE_YEAR 
				HAVING VALUE > 0
				
				)
				UNION  
				(
				SELECT
				ST.COMPANY_ID,
 				MS.strName as CUSTOMER,
				ST.CURRENCY_ID,
				SIH.BILL_INVOICE_NO AS SERIAL_NO,
				SIH.BILL_INVOICE_YEAR AS SERIAL_YEAR,
 				/*SIH.ORDER_NO AS ORDER_NO,
				SIH.ORDER_YEAR AS ORDER_YEAR,*/
				SIH.BILL_DATE AS INVOICED_DATE,
				'$lastMonthEnd' AS LAST_GAIN_TRNS_DATE,
				 FEC.dblExcAvgRate AS EXC_RATE_TODAY,
				(if(DATEDIFF(SIH.BILL_DATE,'$lastMonthEnd')>0,SIH.BILL_DATE,'$lastMonthEnd')) AS DATE_LAST,
				FEL.dblExcAvgRate AS EXC_RATE_LAST,
				ROUND(SUM(ST.VALUE),2) AS `VALUE`,
				 ROUND(SUM(ABS(ST.VALUE)*(FEC.dblExcAvgRate-FEL.dblExcAvgRate)),4) AS GAINLOSS,
				 'PAYMENT_TOT_GAIN' AS DOCUMENT_TYPE
				FROM
				finance_other_payable_transaction AS ST
				INNER JOIN finance_other_payable_bill_header AS SIH ON SIH.BILL_INVOICE_YEAR = ST.BILL_INVOICE_YEAR AND SIH.BILL_INVOICE_NO = ST.BILL_INVOICE_NO
				INNER JOIN mst_financeexchangerate AS FEC ON ST.CURRENCY_ID = FEC.intCurrencyId AND ST.COMPANY_ID = FEC.intCompanyId AND FEC.dtmDate = DATE(ST.TRANSACTION_DATE_TIME) 
				INNER JOIN mst_financeexchangerate AS FEL ON ST.CURRENCY_ID = FEL.intCurrencyId AND ST.COMPANY_ID = FEL.intCompanyId AND FEL.dtmDate = DATE((if(DATEDIFF(SIH.BILL_DATE,'$lastMonthEnd')<0,SIH.BILL_DATE,'$lastMonthEnd')))
 				LEFT JOIN mst_supplier AS MS ON SIH.SUPPLIER_ID=MS.intId
				WHERE 
  				SIH.SUPPLIER_ID='$supplier' AND 
				DATEDIFF(DATE(ST.TRANSACTION_DATE_TIME),DATE('$runDate'))<=0 AND
				ST.DOCUMENT_TYPE = 'BILLPAYMENT' AND
				ST.COMPANY_ID = '$session_companyId' 
 				GROUP BY
				ST.COMPANY_ID,
				ST.LEDGER_ID,
				ST.CURRENCY_ID,
				SIH.BILL_INVOICE_NO,
				SIH.BILL_INVOICE_YEAR 
				
				)
				) as TB_GAIN 
 				";
		return $sql;
	}
 	private function other_payble_gainLoss_report_sql($supplier,$runDate,$lastMonthEnd)
	{
		global $session_companyId;
		
		      $sql = "SELECT 
		  		TB_GAIN.SERIAL_NO,
				TB_GAIN.SERIAL_YEAR, 
				SUM(TB_GAIN.GAINLOSS) as GAINLOSS 
				FROM ((
				SELECT
				ST.COMPANY_ID,
 				MS.strName as CUSTOMER,
				ST.CURRENCY_ID,
				SIH.BILL_INVOICE_NO AS SERIAL_NO,
				SIH.BILL_INVOICE_YEAR AS SERIAL_YEAR,
 				/*SIH.ORDER_NO AS ORDER_NO,
				SIH.ORDER_YEAR AS ORDER_YEAR,*/
				SIH.BILL_DATE AS INVOICED_DATE,
				'$lastMonthEnd' AS LAST_GAIN_TRNS_DATE,
				 FEC.dblExcAvgRate AS EXC_RATE_TODAY,
				(if(DATEDIFF(SIH.BILL_DATE,'$lastMonthEnd')>0,SIH.BILL_DATE,'$lastMonthEnd')) AS DATE_LAST,
				FEL.dblExcAvgRate AS EXC_RATE_LAST,
				ROUND(SUM(ST.VALUE),2) AS `VALUE` ,
				 ROUND(SUM(ABS(ST.VALUE)*(FEC.dblExcAvgRate-FEL.dblExcAvgRate)),4) AS GAINLOSS,
				 'INVOICE_BAL_GAIN' AS DOCUMENT_TYPE
				FROM
				finance_other_payable_transaction AS ST
				INNER JOIN finance_other_payable_bill_header AS SIH ON SIH.BILL_INVOICE_YEAR = ST.BILL_INVOICE_YEAR AND SIH.BILL_INVOICE_NO = ST.BILL_INVOICE_NO
				INNER JOIN mst_financeexchangerate AS FEC ON ST.CURRENCY_ID = FEC.intCurrencyId AND ST.COMPANY_ID = FEC.intCompanyId AND FEC.dtmDate = DATE('$runDate')
				INNER JOIN mst_financeexchangerate AS FEL ON ST.CURRENCY_ID = FEL.intCurrencyId AND ST.COMPANY_ID = FEL.intCompanyId AND FEL.dtmDate = DATE((if(DATEDIFF(SIH.BILL_DATE,'$lastMonthEnd')<0,SIH.BILL_DATE,'$lastMonthEnd')))
 				LEFT JOIN mst_supplier AS MS ON SIH.SUPPLIER_ID=MS.intId
				WHERE 
  				SIH.SUPPLIER_ID='$supplier' AND 
				DATEDIFF(DATE(ST.TRANSACTION_DATE_TIME),DATE('$runDate'))<=0 AND
				ST.COMPANY_ID = '$session_companyId' 
				GROUP BY
				ST.COMPANY_ID,
				ST.LEDGER_ID,
				ST.CURRENCY_ID,
				SIH.BILL_INVOICE_NO,
				SIH.BILL_INVOICE_YEAR  
				HAVING VALUE > 0
				
				)
				UNION  
				(
				SELECT
				ST.COMPANY_ID,
 				MS.strName as CUSTOMER,
				ST.CURRENCY_ID,
				SIH.BILL_INVOICE_NO AS SERIAL_NO,
				SIH.BILL_INVOICE_YEAR AS SERIAL_YEAR,
 				/*SIH.ORDER_NO AS ORDER_NO,
				SIH.ORDER_YEAR AS ORDER_YEAR,*/
				SIH.BILL_DATE AS INVOICED_DATE,
				'$lastMonthEnd' AS LAST_GAIN_TRNS_DATE,
				 FEC.dblExcAvgRate AS EXC_RATE_TODAY,
				(if(DATEDIFF(SIH.BILL_DATE,'$lastMonthEnd')>0,SIH.BILL_DATE,'$lastMonthEnd')) AS DATE_LAST,
				FEL.dblExcAvgRate AS EXC_RATE_LAST,
				ROUND(SUM(ST.VALUE),2) AS `VALUE`,
				 ROUND(SUM(ABS(ST.VALUE)*(FEC.dblExcAvgRate-FEL.dblExcAvgRate)),4) AS GAINLOSS,
				 'PAYMENT_TOT_GAIN' AS DOCUMENT_TYPE
				FROM
				finance_other_payable_transaction AS ST
				INNER JOIN finance_other_payable_bill_header AS SIH ON SIH.BILL_INVOICE_YEAR = ST.BILL_INVOICE_YEAR AND SIH.BILL_INVOICE_NO = ST.BILL_INVOICE_NO
				INNER JOIN mst_financeexchangerate AS FEC ON ST.CURRENCY_ID = FEC.intCurrencyId AND ST.COMPANY_ID = FEC.intCompanyId AND FEC.dtmDate = DATE(ST.TRANSACTION_DATE_TIME) 
				INNER JOIN mst_financeexchangerate AS FEL ON ST.CURRENCY_ID = FEL.intCurrencyId AND ST.COMPANY_ID = FEL.intCompanyId AND FEL.dtmDate = DATE((if(DATEDIFF(SIH.BILL_DATE,'$lastMonthEnd')<0,SIH.BILL_DATE,'$lastMonthEnd')))
 				LEFT JOIN mst_supplier AS MS ON SIH.SUPPLIER_ID=MS.intId
				WHERE 
  				SIH.SUPPLIER_ID='$supplier' AND  
				ST.DOCUMENT_TYPE = 'BILLPAYMENT' AND  
 				DATEDIFF(DATE(ST.TRANSACTION_DATE_TIME),DATE('$runDate'))<=0 AND
				ST.COMPANY_ID = '$session_companyId'
				GROUP BY
				ST.COMPANY_ID,
				ST.LEDGER_ID,
				ST.CURRENCY_ID,
				SIH.BILL_INVOICE_NO,
				SIH.BILL_INVOICE_YEAR 
				
				)
				) as TB_GAIN
				HAVING  GAINLOSS <> 0
				";
 		return $sql;
	}
	private function get_process1_gain_loss_results_sql($startDateThisMonth,$endDateThisMonth)
	{
		global $session_companyId;
		
		$sql = "SELECT SUB_1.*,(lastAmonut_Dr-lastAmonut_Cr) AS lastAmount,
				(actualAmonut_Dr-actualAmonut_Cr) AS actualAmount
				FROM
				(SELECT FT.CHART_OF_ACCOUNT_ID,
				FT.CURRENCY_ID,
				ROUND(SUM(IF(FT.TRANSACTION_TYPE='C',FT.AMOUNT,0)*FEC.dblExcAvgRate),2) AS lastAmonut_Cr,
				ROUND(SUM(IF(FT.TRANSACTION_TYPE='D',FT.AMOUNT,0)*FEC.dblExcAvgRate),2) AS lastAmonut_Dr,
				ROUND(SUM(IF(FT.TRANSACTION_TYPE='C',FT.AMOUNT,0)*FEA.dblExcAvgRate),2) AS actualAmonut_Cr,
				ROUND(SUM(IF(FT.TRANSACTION_TYPE='D',FT.AMOUNT,0)*FEA.dblExcAvgRate),2) AS actualAmonut_Dr		
				FROM finance_transaction FT
				INNER JOIN finance_mst_chartofaccount COA ON COA.CHART_OF_ACCOUNT_ID=FT.CHART_OF_ACCOUNT_ID
				INNER JOIN mst_financeexchangerate AS FEC ON FT.CURRENCY_ID = FEC.intCurrencyId AND 
				FT.COMPANY_ID = FEC.intCompanyId AND FEC.dtmDate = DATE('$endDateThisMonth')
				INNER JOIN mst_financeexchangerate AS FEA ON FT.CURRENCY_ID = FEA.intCurrencyId AND 
				FT.COMPANY_ID = FEA.intCompanyId AND FEA.dtmDate = FT.LAST_MODIFIED_DATE
				WHERE COA.GAIN_LOSS_PROCESS_ID = '1' AND
				FT.LAST_MODIFIED_DATE BETWEEN ('$startDateThisMonth') AND ('$endDateThisMonth') AND
				FT.COMPANY_ID = '$session_companyId' AND
				FT.DOCUMENT_TYPE <> 'GAIN_LOSS'
				GROUP BY 
				FT.COMPANY_ID,
				FT.CHART_OF_ACCOUNT_ID,
				FT.CURRENCY_ID ) AS SUB_1 ";
		
		$result = $this->db->RunQuery2($sql);
		
		return $result;
	}
	private function get_process2_gain_loss_results_sql($startDateThisMonth,$endDateThisMonth)
	{
		global $session_companyId;
		
		$sql = "SELECT SUB_1.*,(lastAmonut_Cr-lastAmonut_Dr) AS lastAmount,
				(actualAmonut_Cr-actualAmonut_Dr) AS actualAmount
				FROM
				(SELECT FT.CHART_OF_ACCOUNT_ID,
				FT.CURRENCY_ID,
				ROUND(SUM(IF(FT.TRANSACTION_TYPE='C',FT.AMOUNT,0)*FEC.dblExcAvgRate),2) AS lastAmonut_Cr,
				ROUND(SUM(IF(FT.TRANSACTION_TYPE='D',FT.AMOUNT,0)*FEC.dblExcAvgRate),2) AS lastAmonut_Dr,
				ROUND(SUM(IF(FT.TRANSACTION_TYPE='C',FT.AMOUNT,0)*FEA.dblExcAvgRate),2) AS actualAmonut_Cr,
				ROUND(SUM(IF(FT.TRANSACTION_TYPE='D',FT.AMOUNT,0)*FEA.dblExcAvgRate),2) AS actualAmonut_Dr		
				FROM finance_transaction FT
				INNER JOIN finance_mst_chartofaccount COA ON COA.CHART_OF_ACCOUNT_ID=FT.CHART_OF_ACCOUNT_ID
				INNER JOIN mst_financeexchangerate AS FEC ON FT.CURRENCY_ID = FEC.intCurrencyId AND 
				FT.COMPANY_ID = FEC.intCompanyId AND FEC.dtmDate = DATE('$endDateThisMonth')
				INNER JOIN mst_financeexchangerate AS FEA ON FT.CURRENCY_ID = FEA.intCurrencyId AND 
				FT.COMPANY_ID = FEA.intCompanyId AND FEA.dtmDate = FT.LAST_MODIFIED_DATE
				WHERE COA.GAIN_LOSS_PROCESS_ID = '2' AND
				FT.LAST_MODIFIED_DATE BETWEEN ('$startDateThisMonth') AND ('$endDateThisMonth') AND
				FT.COMPANY_ID = '$session_companyId' 
				GROUP BY 
				FT.COMPANY_ID,
				FT.CHART_OF_ACCOUNT_ID,
				FT.CURRENCY_ID ) AS SUB_1 ";
		
		$result = $this->db->RunQuery2($sql);
		
		return $result;
	}
	private function get_process3_gain_loss_results_sql($startDateThisMonth,$endDateThisMonth)
	{
		global $session_companyId;
		
		$sql = "SELECT CHART_OF_ACCOUNT_ID,
				CURRENCY_ID,
				SUM((BAL_AMOUNT_Cr+actualBPAmonut_Cr+actualPSAmonut_Cr)-actualJEAmonut_Cr) AS gainLossAmount
				FROM
				(
				SELECT SUB_1.*,
					IFNULL(ROUND((AMOUNT-(BP_SETTLE_AMOUNT+PS_SETTLE_AMOUNT))*dblExcAvgRate,2),0) AS BAL_AMOUNT_Cr
					FROM
					(
					SELECT JD.CHART_OF_ACCOUNT_ID,
					JH.COMPANY_ID,
					JH.CURRENCY_ID,
					FEC.dblExcAvgRate,
					IFNULL(IF(JD.TRANSACTION_TYPE='C',JD.AMOUNT,0),0) AS AMOUNT,
					IFNULL(ROUND(SUM(IF(JD.TRANSACTION_TYPE='C',JD.AMOUNT,0)*FEA.dblExcAvgRate),2),0) AS actualJEAmonut_Cr,
					(
					IFNULL((SELECT BPD.AMOUNT
					FROM finance_journal_entry_details BPJD
					INNER JOIN finance_mst_chartofaccount COA ON COA.CHART_OF_ACCOUNT_ID=BPJD.CHART_OF_ACCOUNT_ID
					INNER JOIN finance_journal_entry_header BPJH ON BPJH.JOURNAL_ENTRY_NO=BPJD.JOURNAL_ENTRY_NO 
					AND BPJH.JOURNAL_ENTRY_YEAR=BPJD.JOURNAL_ENTRY_YEAR
					INNER JOIN finance_bank_payment_details BPD ON BPD.SETTELEMENT_NO=BPJD.JOURNAL_ENTRY_NO AND
					BPD.SETTELEMENT_YEAR=BPJD.JOURNAL_ENTRY_YEAR AND
					BPD.SETTELEMENT_ORDER=BPJD.ORDER_ID
					INNER JOIN finance_bank_payment_header BPH ON BPH.BANK_PAYMENT_NO=BPD.BANK_PAYMENT_NO AND
					BPH.BANK_PAYMENT_YEAR=BPD.BANK_PAYMENT_YEAR
					WHERE 
					BPH.STATUS = 1 AND
					BPJH.COMPANY_ID = JH.COMPANY_ID AND
					BPJD.CHART_OF_ACCOUNT_ID = JD.CHART_OF_ACCOUNT_ID AND
					BPJH.CURRENCY_ID = JH.CURRENCY_ID AND
					BPJD.JOURNAL_ENTRY_NO = JD.JOURNAL_ENTRY_NO AND
					BPJD.JOURNAL_ENTRY_YEAR = JD.JOURNAL_ENTRY_YEAR AND
					BPJD.ORDER_ID = JD.ORDER_ID),0)
					) AS BP_SETTLE_AMOUNT,
					(
					IFNULL((SELECT ROUND(SUM(IF(BPJD.TRANSACTION_TYPE='C',BPD.AMOUNT,0)*BFEA.dblExcAvgRate),2)
					FROM finance_journal_entry_details BPJD
					INNER JOIN finance_mst_chartofaccount COA ON COA.CHART_OF_ACCOUNT_ID=BPJD.CHART_OF_ACCOUNT_ID
					INNER JOIN finance_journal_entry_header BPJH ON BPJH.JOURNAL_ENTRY_NO=BPJD.JOURNAL_ENTRY_NO 
					AND BPJH.JOURNAL_ENTRY_YEAR=BPJD.JOURNAL_ENTRY_YEAR
					INNER JOIN finance_bank_payment_details BPD ON BPD.SETTELEMENT_NO=BPJD.JOURNAL_ENTRY_NO AND
					BPD.SETTELEMENT_YEAR=BPJD.JOURNAL_ENTRY_YEAR AND
					BPD.SETTELEMENT_ORDER=BPJD.ORDER_ID
					INNER JOIN finance_bank_payment_header BPH ON BPH.BANK_PAYMENT_NO=BPD.BANK_PAYMENT_NO AND
					BPH.BANK_PAYMENT_YEAR=BPD.BANK_PAYMENT_YEAR
					INNER JOIN mst_financeexchangerate AS BFEA ON BPH.CURRENCY_ID = BFEA.intCurrencyId AND 
					BPH.COMPANY_ID = BFEA.intCompanyId AND BFEA.dtmDate = BPH.PAYMENT_DATE
					WHERE 
					BPH.STATUS = 1 AND
					BPJH.COMPANY_ID = JH.COMPANY_ID AND
					BPJD.CHART_OF_ACCOUNT_ID = JD.CHART_OF_ACCOUNT_ID AND
					BPJH.CURRENCY_ID = JH.CURRENCY_ID AND
					BPJD.JOURNAL_ENTRY_NO = JD.JOURNAL_ENTRY_NO AND
					BPJD.JOURNAL_ENTRY_YEAR = JD.JOURNAL_ENTRY_YEAR AND
					BPJD.ORDER_ID = JD.ORDER_ID),0)
					) AS actualBPAmonut_Cr,
					(
					IFNULL((SELECT IF(JD.TRANSACTION_TYPE='C',PSD.AMOUNT,0)
					FROM finance_journal_entry_details BPJD
					INNER JOIN finance_mst_chartofaccount COA ON COA.CHART_OF_ACCOUNT_ID=BPJD.CHART_OF_ACCOUNT_ID
					INNER JOIN finance_journal_entry_header BPJH ON BPJH.JOURNAL_ENTRY_NO=BPJD.JOURNAL_ENTRY_NO 
					AND BPJH.JOURNAL_ENTRY_YEAR=BPJD.JOURNAL_ENTRY_YEAR
					INNER JOIN finance_payment_schedule_header PSH ON PSH.SETTLEMENT_NO=BPJD.JOURNAL_ENTRY_NO AND
					PSH.SETTLEMENT_YEAR=BPJD.JOURNAL_ENTRY_YEAR AND
					PSH.SETTLEMENT_ORDER=BPJD.ORDER_ID
					INNER JOIN finance_payment_schedule_details PSD ON PSH.SCHEDULE_NO=PSD.SCHEDULE_NO AND
					PSH.SCHEDULE_YEAR=PSD.SCHEDULE_YEAR
					WHERE 
					PSD.STATUS = 1 AND
					BPJH.COMPANY_ID = JH.COMPANY_ID AND
					BPJD.CHART_OF_ACCOUNT_ID = JD.CHART_OF_ACCOUNT_ID AND
					BPJH.CURRENCY_ID = JH.CURRENCY_ID AND
					BPJD.JOURNAL_ENTRY_NO = JD.JOURNAL_ENTRY_NO AND
					BPJD.JOURNAL_ENTRY_YEAR = JD.JOURNAL_ENTRY_YEAR AND
					BPJD.ORDER_ID = JD.ORDER_ID),0)
					) AS PS_SETTLE_AMOUNT,
					
					(
					IFNULL((SELECT ROUND(SUM(IF(JD.TRANSACTION_TYPE='C',PSD.AMOUNT,0)*PFEA.dblExcAvgRate),2)
					FROM finance_journal_entry_details BPJD
					INNER JOIN finance_mst_chartofaccount COA ON COA.CHART_OF_ACCOUNT_ID=BPJD.CHART_OF_ACCOUNT_ID
					INNER JOIN finance_journal_entry_header BPJH ON BPJH.JOURNAL_ENTRY_NO=BPJD.JOURNAL_ENTRY_NO 
					AND BPJH.JOURNAL_ENTRY_YEAR=BPJD.JOURNAL_ENTRY_YEAR
					INNER JOIN finance_payment_schedule_header PSH ON PSH.SETTLEMENT_NO=BPJD.JOURNAL_ENTRY_NO AND
					PSH.SETTLEMENT_YEAR=BPJD.JOURNAL_ENTRY_YEAR AND
					PSH.SETTLEMENT_ORDER=BPJD.ORDER_ID
					INNER JOIN finance_payment_schedule_details PSD ON PSH.SCHEDULE_NO=PSD.SCHEDULE_NO AND
					PSH.SCHEDULE_YEAR=PSD.SCHEDULE_YEAR
					INNER JOIN mst_financeexchangerate AS PFEA ON PSH.CURRENCY_ID = PFEA.intCurrencyId AND 
					PSH.COMPANY_ID = PFEA.intCompanyId AND PFEA.dtmDate = PSD.PAY_DATE
					WHERE 
					PSD.STATUS = 1 AND
					BPJH.COMPANY_ID = JH.COMPANY_ID AND
					BPJD.CHART_OF_ACCOUNT_ID = JD.CHART_OF_ACCOUNT_ID AND
					BPJH.CURRENCY_ID = JH.CURRENCY_ID AND
					BPJD.JOURNAL_ENTRY_NO = JD.JOURNAL_ENTRY_NO AND
					BPJD.JOURNAL_ENTRY_YEAR = JD.JOURNAL_ENTRY_YEAR AND
					BPJD.ORDER_ID = JD.ORDER_ID),0)
					) AS actualPSAmonut_Cr
					
					FROM finance_journal_entry_details JD
					INNER JOIN finance_mst_chartofaccount COA ON COA.CHART_OF_ACCOUNT_ID=JD.CHART_OF_ACCOUNT_ID
					INNER JOIN finance_journal_entry_header JH ON JH.JOURNAL_ENTRY_NO=JD.JOURNAL_ENTRY_NO AND 
					JH.JOURNAL_ENTRY_YEAR=JD.JOURNAL_ENTRY_YEAR
					INNER JOIN mst_financeexchangerate AS FEA ON JH.CURRENCY_ID = FEA.intCurrencyId AND 
					JH.COMPANY_ID = FEA.intCompanyId AND FEA.dtmDate = JH.JOURNAL_ENTRY_DATE
					INNER JOIN mst_financeexchangerate AS FEC ON JH.CURRENCY_ID = FEC.intCurrencyId AND 
					JH.COMPANY_ID = FEC.intCompanyId AND FEC.dtmDate = DATE('$endDateThisMonth')
					WHERE COA.GAIN_LOSS_PROCESS_ID = 3 AND
					JH.JOURNAL_ENTRY_DATE BETWEEN ('$startDateThisMonth') AND ('$endDateThisMonth') AND
					JH.COMPANY_ID = '$session_companyId' AND
					JH.STATUS = 1
					GROUP BY 
					JH.COMPANY_ID,
					JD.CHART_OF_ACCOUNT_ID,
					JH.CURRENCY_ID,
					JD.JOURNAL_ENTRY_NO,
					JD.JOURNAL_ENTRY_YEAR,
					JD.ORDER_ID
					) AS SUB_1
				)AS SUB_2
				GROUP BY COMPANY_ID,
				CURRENCY_ID,
				CHART_OF_ACCOUNT_ID ";
		
		$result = $this->db->RunQuery2($sql);
		return $result;
	}
	private function get_process4_gain_loss_results_sql($startDateThisMonth,$endDateThisMonth)
	{
		global $session_companyId;
		
		$sql = "SELECT CHART_OF_ACCOUNT_ID,
				CURRENCY_ID,
				SUM((BAL_AMOUNT_Dr+actualBPAmonut_Dr+actualPSAmonut_Dr)-actualJEAmonut_Dr) AS gainLossAmount
				FROM
				(
				SELECT SUB_1.*,
					IFNULL(ROUND((AMOUNT-(BP_SETTLE_AMOUNT+PS_SETTLE_AMOUNT))*dblExcAvgRate,2),0) AS BAL_AMOUNT_Dr
					FROM
					(
					SELECT JD.CHART_OF_ACCOUNT_ID,
					JH.COMPANY_ID,
					JH.CURRENCY_ID,
					FEC.dblExcAvgRate,
					IFNULL(IF(JD.TRANSACTION_TYPE='D',JD.AMOUNT,0),0) AS AMOUNT,
					IFNULL(ROUND(SUM(IF(JD.TRANSACTION_TYPE='D',JD.AMOUNT,0)*FEA.dblExcAvgRate),2),0) AS actualJEAmonut_Dr,
					(
					IFNULL((SELECT BPD.AMOUNT
					FROM finance_journal_entry_details BPJD
					INNER JOIN finance_mst_chartofaccount COA ON COA.CHART_OF_ACCOUNT_ID=BPJD.CHART_OF_ACCOUNT_ID
					INNER JOIN finance_journal_entry_header BPJH ON BPJH.JOURNAL_ENTRY_NO=BPJD.JOURNAL_ENTRY_NO 
					AND BPJH.JOURNAL_ENTRY_YEAR=BPJD.JOURNAL_ENTRY_YEAR
					INNER JOIN finance_bank_payment_details BPD ON BPD.SETTELEMENT_NO=BPJD.JOURNAL_ENTRY_NO AND
					BPD.SETTELEMENT_YEAR=BPJD.JOURNAL_ENTRY_YEAR AND
					BPD.SETTELEMENT_ORDER=BPJD.ORDER_ID
					INNER JOIN finance_bank_payment_header BPH ON BPH.BANK_PAYMENT_NO=BPD.BANK_PAYMENT_NO AND
					BPH.BANK_PAYMENT_YEAR=BPD.BANK_PAYMENT_YEAR
					WHERE 
					BPH.STATUS = 1 AND
					BPJH.COMPANY_ID = JH.COMPANY_ID AND
					BPJD.CHART_OF_ACCOUNT_ID = JD.CHART_OF_ACCOUNT_ID AND
					BPJH.CURRENCY_ID = JH.CURRENCY_ID AND
					BPJD.JOURNAL_ENTRY_NO = JD.JOURNAL_ENTRY_NO AND
					BPJD.JOURNAL_ENTRY_YEAR = JD.JOURNAL_ENTRY_YEAR AND
					BPJD.ORDER_ID = JD.ORDER_ID),0)
					) AS BP_SETTLE_AMOUNT,
					(
					IFNULL((SELECT ROUND(SUM(IF(BPJD.TRANSACTION_TYPE='D',BPD.AMOUNT,0)*BFEA.dblExcAvgRate),2)
					FROM finance_journal_entry_details BPJD
					INNER JOIN finance_mst_chartofaccount COA ON COA.CHART_OF_ACCOUNT_ID=BPJD.CHART_OF_ACCOUNT_ID
					INNER JOIN finance_journal_entry_header BPJH ON BPJH.JOURNAL_ENTRY_NO=BPJD.JOURNAL_ENTRY_NO 
					AND BPJH.JOURNAL_ENTRY_YEAR=BPJD.JOURNAL_ENTRY_YEAR
					INNER JOIN finance_bank_payment_details BPD ON BPD.SETTELEMENT_NO=BPJD.JOURNAL_ENTRY_NO AND
					BPD.SETTELEMENT_YEAR=BPJD.JOURNAL_ENTRY_YEAR AND
					BPD.SETTELEMENT_ORDER=BPJD.ORDER_ID
					INNER JOIN finance_bank_payment_header BPH ON BPH.BANK_PAYMENT_NO=BPD.BANK_PAYMENT_NO AND
					BPH.BANK_PAYMENT_YEAR=BPD.BANK_PAYMENT_YEAR
					INNER JOIN mst_financeexchangerate AS BFEA ON BPH.CURRENCY_ID = BFEA.intCurrencyId AND 
					BPH.COMPANY_ID = BFEA.intCompanyId AND BFEA.dtmDate = BPH.PAYMENT_DATE
					WHERE 
					BPH.STATUS = 1 AND
					BPJH.COMPANY_ID = JH.COMPANY_ID AND
					BPJD.CHART_OF_ACCOUNT_ID = JD.CHART_OF_ACCOUNT_ID AND
					BPJH.CURRENCY_ID = JH.CURRENCY_ID AND
					BPJD.JOURNAL_ENTRY_NO = JD.JOURNAL_ENTRY_NO AND
					BPJD.JOURNAL_ENTRY_YEAR = JD.JOURNAL_ENTRY_YEAR AND
					BPJD.ORDER_ID = JD.ORDER_ID),0)
					) AS actualBPAmonut_Dr,
					(
					IFNULL((SELECT IF(JD.TRANSACTION_TYPE='D',PSD.AMOUNT,0)
					FROM finance_journal_entry_details BPJD
					INNER JOIN finance_mst_chartofaccount COA ON COA.CHART_OF_ACCOUNT_ID=BPJD.CHART_OF_ACCOUNT_ID
					INNER JOIN finance_journal_entry_header BPJH ON BPJH.JOURNAL_ENTRY_NO=BPJD.JOURNAL_ENTRY_NO 
					AND BPJH.JOURNAL_ENTRY_YEAR=BPJD.JOURNAL_ENTRY_YEAR
					INNER JOIN finance_payment_schedule_header PSH ON PSH.SETTLEMENT_NO=BPJD.JOURNAL_ENTRY_NO AND
					PSH.SETTLEMENT_YEAR=BPJD.JOURNAL_ENTRY_YEAR AND
					PSH.SETTLEMENT_ORDER=BPJD.ORDER_ID
					INNER JOIN finance_payment_schedule_details PSD ON PSH.SCHEDULE_NO=PSD.SCHEDULE_NO AND
					PSH.SCHEDULE_YEAR=PSD.SCHEDULE_YEAR
					WHERE 
					PSD.STATUS = 1 AND
					BPJH.COMPANY_ID = JH.COMPANY_ID AND
					BPJD.CHART_OF_ACCOUNT_ID = JD.CHART_OF_ACCOUNT_ID AND
					BPJH.CURRENCY_ID = JH.CURRENCY_ID AND
					BPJD.JOURNAL_ENTRY_NO = JD.JOURNAL_ENTRY_NO AND
					BPJD.JOURNAL_ENTRY_YEAR = JD.JOURNAL_ENTRY_YEAR AND
					BPJD.ORDER_ID = JD.ORDER_ID),0)
					) AS PS_SETTLE_AMOUNT,
					
					(
					IFNULL((SELECT ROUND(SUM(IF(JD.TRANSACTION_TYPE='D',PSD.AMOUNT,0)*PFEA.dblExcAvgRate),2)
					FROM finance_journal_entry_details BPJD
					INNER JOIN finance_mst_chartofaccount COA ON COA.CHART_OF_ACCOUNT_ID=BPJD.CHART_OF_ACCOUNT_ID
					INNER JOIN finance_journal_entry_header BPJH ON BPJH.JOURNAL_ENTRY_NO=BPJD.JOURNAL_ENTRY_NO 
					AND BPJH.JOURNAL_ENTRY_YEAR=BPJD.JOURNAL_ENTRY_YEAR
					INNER JOIN finance_payment_schedule_header PSH ON PSH.SETTLEMENT_NO=BPJD.JOURNAL_ENTRY_NO AND
					PSH.SETTLEMENT_YEAR=BPJD.JOURNAL_ENTRY_YEAR AND
					PSH.SETTLEMENT_ORDER=BPJD.ORDER_ID
					INNER JOIN finance_payment_schedule_details PSD ON PSH.SCHEDULE_NO=PSD.SCHEDULE_NO AND
					PSH.SCHEDULE_YEAR=PSD.SCHEDULE_YEAR
					INNER JOIN mst_financeexchangerate AS PFEA ON PSH.CURRENCY_ID = PFEA.intCurrencyId AND 
					PSH.COMPANY_ID = PFEA.intCompanyId AND PFEA.dtmDate = PSD.PAY_DATE
					WHERE 
					PSD.STATUS = 1 AND
					BPJH.COMPANY_ID = JH.COMPANY_ID AND
					BPJD.CHART_OF_ACCOUNT_ID = JD.CHART_OF_ACCOUNT_ID AND
					BPJH.CURRENCY_ID = JH.CURRENCY_ID AND
					BPJD.JOURNAL_ENTRY_NO = JD.JOURNAL_ENTRY_NO AND
					BPJD.JOURNAL_ENTRY_YEAR = JD.JOURNAL_ENTRY_YEAR AND
					BPJD.ORDER_ID = JD.ORDER_ID),0)
					) AS actualPSAmonut_Dr
					
					FROM finance_journal_entry_details JD
					INNER JOIN finance_mst_chartofaccount COA ON COA.CHART_OF_ACCOUNT_ID=JD.CHART_OF_ACCOUNT_ID
					INNER JOIN finance_journal_entry_header JH ON JH.JOURNAL_ENTRY_NO=JD.JOURNAL_ENTRY_NO AND 
					JH.JOURNAL_ENTRY_YEAR=JD.JOURNAL_ENTRY_YEAR
					INNER JOIN mst_financeexchangerate AS FEA ON JH.CURRENCY_ID = FEA.intCurrencyId AND 
					JH.COMPANY_ID = FEA.intCompanyId AND FEA.dtmDate = JH.JOURNAL_ENTRY_DATE
					INNER JOIN mst_financeexchangerate AS FEC ON JH.CURRENCY_ID = FEC.intCurrencyId AND 
					JH.COMPANY_ID = FEC.intCompanyId AND FEC.dtmDate = DATE('$endDateThisMonth')
					WHERE COA.GAIN_LOSS_PROCESS_ID = 4 AND
					JH.JOURNAL_ENTRY_DATE BETWEEN ('$startDateThisMonth') AND ('$endDateThisMonth') AND
					JH.COMPANY_ID = '$session_companyId' AND
					JH.STATUS = 1
					GROUP BY 
					JH.COMPANY_ID,
					JD.CHART_OF_ACCOUNT_ID,
					JH.CURRENCY_ID,
					JD.JOURNAL_ENTRY_NO,
					JD.JOURNAL_ENTRY_YEAR,
					JD.ORDER_ID
					) AS SUB_1
				)AS SUB_2
				GROUP BY COMPANY_ID,
				CURRENCY_ID,
				CHART_OF_ACCOUNT_ID ";
		
		$result = $this->db->RunQuery2($sql);
		return $result;
	}
	private function getChartOfAccName_sql($glId)
	{
		$sql = "SELECT CONCAT(CONCAT(FMT.FINANCE_TYPE_CODE,'',FMMT.MAIN_TYPE_CODE,'',FMST.SUB_TYPE_CODE,'',FCOA.CHART_OF_ACCOUNT_CODE),' - ',
				FCOA.CHART_OF_ACCOUNT_NAME) AS chartOfAccName
				FROM finance_mst_chartofaccount FCOA
				INNER JOIN finance_mst_account_sub_type FMST ON FMST.SUB_TYPE_ID=FCOA.SUB_TYPE_ID
				INNER JOIN finance_mst_account_main_type FMMT ON FMMT.MAIN_TYPE_ID=FMST.MAIN_TYPE_ID
				INNER JOIN finance_mst_account_type FMT ON FMT.FINANCE_TYPE_ID=FMMT.FINANCE_TYPE_ID
				WHERE FCOA.CHART_OF_ACCOUNT_ID='$glId' ";
		
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row['chartOfAccName'];
	}
}
?>
<?php
$companyId	= $_SESSION["headCompanyId"];

class cls_chartOfAccount_bank_get
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	
	public function GetDetails($coaId,$transType)
	{
		return $this->GetDetails_sql($coaId,$transType);
	}


	private function GetDetails_sql($coaId,$transType)
	{
		$sql = "SELECT 
					BANK_ID 	AS BANK_ID,
					CURRENCY_ID AS CURRENCY_ID
				FROM finance_mst_chartofaccount_bank
				WHERE CHART_OF_ACCOUNT_ID = $coaId";		
		$result = $this->db->$transType($sql);
		return mysqli_fetch_array($result);
		
	}	
}
?>
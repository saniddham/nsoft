<?php
$companyId	= $_SESSION["headCompanyId"];

class Cls_ChartOfAccount_Get
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	public function loadFinanceTypeCombo()
	{
		return $this->FinanceTypeCombo_sql();
	}
	public function loadMainTypeCombo($financeType)
	{
		return $this->MainTypeCombo_sql($financeType);
	}
	public function loadSubTypeCombo($mainType)
	{
		return $this->SubTypeCombo_sql($mainType);
	}
	public function getMaxChartOfAccountCode($subType)
	{
		return $this->MaxChartOfAccountCode_sql($subType);
	}
	public function getTransacTypeCount()
	{
		return $this->TransacTypeCount_sql();
	}
	public function getTransacTypeDetail($fstLimit,$lastLimit)
	{
		return $this->TransacTypeDetail_sql($fstLimit,$lastLimit);
	}
	public function getCompanyDetails()
	{
		return $this->CompanyDetails_sql();
	}
	public function loadSearchCombo($subType)
	{
		return $this->searchCombo_sql($subType);
	}
	public function loadData($searchId)
	{
		return $this->loadData_sql($searchId);
	}
	private function FinanceTypeCombo_sql()
	{
		$sql = "SELECT FINANCE_TYPE_ID,FINANCE_TYPE_NAME
				FROM finance_mst_account_type
				WHERE STATUS=1
				ORDER BY FINANCE_TYPE_NAME ";
		
		$result = $this->db->RunQuery($sql);
		$combo  = "<option value=\"NULL\"></option>";
        var_dump($result);
		while($row=mysqli_fetch_array($result))
		{
				$combo .="<option value=\"".$row['FINANCE_TYPE_ID']."\">".$row['FINANCE_TYPE_NAME']."</option>";	
		}
		
		return $combo;
	}
	private function MainTypeCombo_sql($financeType)
	{
		$sql = "SELECT MAIN_TYPE_ID,MAIN_TYPE_NAME
				FROM finance_mst_account_main_type
				WHERE FINANCE_TYPE_ID='$financeType' AND STATUS=1
				ORDER BY MAIN_TYPE_NAME ";
		
		$result = $this->db->RunQuery($sql);
		$combo  = "<option value=\"NULL\"></option>";
		
		while($row=mysqli_fetch_array($result))
		{
			if($row['MAIN_TYPE_ID']=='')
				$combo .="<option value=\"".$row['MAIN_TYPE_ID']."\" selected=\"selected\" >".$row['MAIN_TYPE_NAME']."</option>";	
			else
				$combo .="<option value=\"".$row['MAIN_TYPE_ID']."\">".$row['MAIN_TYPE_NAME']."</option>";	
		}
		
		return $combo;
	}
	private function SubTypeCombo_sql($mainType)
	{
		$sql = "SELECT SUB_TYPE_ID,SUB_TYPE_NAME
				FROM finance_mst_account_sub_type
				WHERE MAIN_TYPE_ID='$mainType' AND STATUS=1
				ORDER BY SUB_TYPE_NAME ";
		
		$result = $this->db->RunQuery($sql);
		$combo  = "<option value=\"NULL\"></option>";
		
		while($row=mysqli_fetch_array($result))
		{
			if($row['SUB_TYPE_ID']=='')
				$combo .="<option value=\"".$row['SUB_TYPE_ID']."\" selected=\"selected\" >".$row['SUB_TYPE_NAME']."</option>";	
			else
				$combo .="<option value=\"".$row['SUB_TYPE_ID']."\">".$row['SUB_TYPE_NAME']."</option>";	
		}
		
		return $combo;
	}
	private function MaxChartOfAccountCode_sql($subType)
	{
		$sql = "SELECT MAX(ROUND(CHART_OF_ACCOUNT_CODE,0)) AS maxCode
				FROM finance_mst_chartofaccount
				WHERE SUB_TYPE_ID='$subType' ";
		
		$result 	= $this->db->RunQuery($sql);
		$row		= mysqli_fetch_array($result);
		
		$newCode 	= $row['maxCode']+1;
		
		if($newCode<10)
			$newCode = '00'.$newCode;
		
		else if($newCode<100)
			$newCode = '0'.$newCode;	
		
		return $newCode;
	}
	private function TransacTypeCount_sql()
	{
		$sql = "SELECT COUNT(TYPE_ID) rowCount
				FROM finance_mst_transaction_type
				WHERE STATUS=1";
		
		$result 	= $this->db->RunQuery($sql);
		$row		= mysqli_fetch_array($result);
		
		return $row['rowCount'];
	}
	private function TransacTypeDetail_sql($fstLimit,$lastLimit)
	{
		$sql = "SELECT TYPE_ID,TYPE_NAME,CHART_OF_ACCOUNT_FIELD
				FROM finance_mst_transaction_type
				WHERE STATUS=1 
				LIMIT $fstLimit,$lastLimit ";
		
		$result = $this->db->RunQuery($sql);
		
		return $result;
	}
	private function CompanyDetails_sql()
	{
		$sql = "SELECT intId,strCode,strName
				FROM mst_companies
				WHERE intStatus=1
				order by strCode";
		
		$result = $this->db->RunQuery($sql);
		
		return $result;
	}
	private function searchCombo_sql($subType)
	{
		global $companyId;
		
		$sql = "SELECT FCOA.CHART_OF_ACCOUNT_ID,
				CONCAT(FMT.FINANCE_TYPE_CODE,'',FMMT.MAIN_TYPE_CODE,'',FMST.SUB_TYPE_CODE,'',FCOA.CHART_OF_ACCOUNT_CODE) AS accountCode,
				FCOA.CHART_OF_ACCOUNT_NAME
				FROM finance_mst_chartofaccount FCOA
				INNER JOIN finance_mst_account_sub_type FMST ON FMST.SUB_TYPE_ID=FCOA.SUB_TYPE_ID
				INNER JOIN finance_mst_account_main_type FMMT ON FMMT.MAIN_TYPE_ID=FMST.MAIN_TYPE_ID
				INNER JOIN finance_mst_account_type FMT ON FMT.FINANCE_TYPE_ID=FMMT.FINANCE_TYPE_ID
				INNER JOIN finance_mst_chartofaccount_company FCOAC ON FCOAC.CHART_OF_ACCOUNT_ID=FCOA.CHART_OF_ACCOUNT_ID
				WHERE FCOAC.COMPANY_ID='$companyId' AND 
				FCOA.CATEGORY_TYPE='N' ";
		if($subType!='')
			$sql.="AND FCOA.SUB_TYPE_ID='$subType' ";
				
		$sql .="ORDER BY CHART_OF_ACCOUNT_NAME ";
		
		$result = $this->db->RunQuery($sql);
		$combo  = "<option value=\"\"></option>";
		
		while($row=mysqli_fetch_array($result))
		{
			$combo .="<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\">".$row['CHART_OF_ACCOUNT_NAME']." - ".$row['accountCode']."</option>";	
		}
		
		return $combo;
	}
	private function loadData_sql($searchId)
	{
		 $response["HEADER"] 		= $this->loadHeaderData($searchId);
		 $response["DETAILS"] 		= $this->loadDetailsData($searchId);
		 $response["BANK_DETAILS"] 	= $this->loadBankDetailsData($searchId);
		 $response["COMPANY"] 		= $this->loadCompanyDetailsData($searchId);
		 
		 return $response;
	}
	private function loadHeaderData($searchId)
	{
		$sql = "SELECT COA.CHART_OF_ACCOUNT_CODE,
				COA.CHART_OF_ACCOUNT_NAME,
				COA.SUB_TYPE_ID,
				MT.MAIN_TYPE_ID,
				FT.FINANCE_TYPE_ID,
				COA.BANK,
				COA.STATUS
				FROM finance_mst_chartofaccount COA
				INNER JOIN finance_mst_account_sub_type ST ON ST.SUB_TYPE_ID=COA.SUB_TYPE_ID
				INNER JOIN finance_mst_account_main_type MT ON MT.MAIN_TYPE_ID=ST.MAIN_TYPE_ID
				INNER JOIN finance_mst_account_main_type FT ON FT.FINANCE_TYPE_ID=MT.FINANCE_TYPE_ID
				WHERE COA.CHART_OF_ACCOUNT_ID='$searchId'
				GROUP BY COA.CHART_OF_ACCOUNT_ID";
		
		$result = $this->db->RunQuery($sql);
		while($row	= mysqli_fetch_array($result))
		{
			$arrayH["accountCode"] 	= $row["CHART_OF_ACCOUNT_CODE"];
			$arrayH["accountName"] 	= $row["CHART_OF_ACCOUNT_NAME"];
			$arrayH["subType"] 		= $row["SUB_TYPE_ID"];
			$arrayH["mainType"] 	= $row["MAIN_TYPE_ID"];
			$arrayH["financeType"] 	= $row["FINANCE_TYPE_ID"];
			$arrayH["bank"] 		= $row["BANK"];
			$arrayH["active"] 		= $row["STATUS"];
		}
		return $details["HEADER"]  = $arrayH;
	}
	
	private function loadDetailsData($searchId)
	{
		$sql = "SELECT 
				CHART_OF_ACCOUNT_FIELD
				FROM
				finance_mst_transaction_type
				WHERE STATUS  = 1";
		$result = $this->db->RunQuery($sql);
		while($row	= mysqli_fetch_array($result))
		{
			$arrayD["FIELD_NAME"] 	= $row["CHART_OF_ACCOUNT_FIELD"];
			$arrayD["STATUS"]	   	= $this->GetStatus($row["CHART_OF_ACCOUNT_FIELD"],$searchId);
			$arrayDetail[]			= $arrayD;
		}
		return $details["DETAILS"]  = $arrayDetail;
	}
	
	private function GetStatus($fieldName,$searchId)
	{
		$sql = "SELECT $fieldName AS STATUS FROM finance_mst_chartofaccount WHERE CHART_OF_ACCOUNT_ID = $searchId";
		$result = $this->db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return $row["STATUS"];
	}
	private function loadCompanyDetailsData($searchId)
	{
		$sql = "SELECT COMPANY_ID
				FROM finance_mst_chartofaccount_company
				WHERE CHART_OF_ACCOUNT_ID='$searchId' ";
		
		$result = $this->db->RunQuery($sql);
		while($row	= mysqli_fetch_array($result))
		{
			$arrayC["companyId"] 	= $row["COMPANY_ID"];
			
			$arrayCompany[]			= $arrayC;
		}
		return $details["COMPANY"]  = $arrayCompany;
	}
	private function loadBankDetailsData($searchId)
	{
		$sql = "SELECT BANK_ID,
				CURRENCY_ID
				FROM finance_mst_chartofaccount_bank
				WHERE CHART_OF_ACCOUNT_ID='$searchId' ";
		
		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		
		$arrayB["bank_id"] 		= $row["BANK_ID"];
		$arrayB["currency_id"] 	= $row["CURRENCY_ID"];
		
		return $details["BANK"]  = $arrayB;
		
	}
	
}
?>
<?php
include  "../../../../class/cls_commonFunctions_get.php";

$session_userId		= $_SESSION["userId"];
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];
$savedMasseged		= "";
$error_sql			= "";
$insertId			= "";

$obj_common			= new cls_commonFunctions_get($db);

class Cls_ChartOfAccount_Set
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	public function save($arrHeader,$arrTransacDetails,$arrCompanyDetails,$programCode)
	{
		global $obj_common;
		global $savedStatus;
		global $savedMasseged;
		global $session_locationId;
		global $session_userId;
		global $error_sql;
		global $insertId;
		
		$searchId		= $arrHeader["searchId"];
		$financeType	= $arrHeader["financeType"];
		$mainType		= $arrHeader["mainType"];
		$subType		= $arrHeader["subType"];
		$accountCode	= $arrHeader["accountCode"];
		$accountName	= $obj_common->replace($arrHeader["accountName"]);
		$bank			= $arrHeader["bank"];
		$bankId			= $arrHeader["bankId"];
		$currency		= $arrHeader["currency"];
		$active			= $arrHeader["active"];
		
		$savedStatus	= true;
		$this->db->begin();
		$this->checkPermission($programCode,$session_userId,'intAdd');
		$this->save_sql($searchId,$financeType,$mainType,$subType,$accountCode,$accountName,$bank,$active);	
		if($bank==1)
			$this->save_bankDetails($searchId,$insertId,$bankId,$currency);	
		else
			$this->deleteBank($searchId,$insertId);
		
		foreach($arrTransacDetails as $array_loop)
		{
			$chartOfAccField	= $array_loop['chartOfAccField'];
			$chkStatus			= $array_loop['chkStatus'];
			
			$this->updateTransactionType($insertId,$chartOfAccField,$chkStatus);
		}
		
		$this->deleteCompany($insertId);
		
		foreach($arrCompanyDetails as $array_loop)
		{
			$companyId			= $array_loop['companyId'];
			
			$this->saveCompany($insertId,$companyId);
		}
		if($savedStatus)
		{
			$this->db->commit();
			$response['type'] 			= "pass";
			if($searchId=='')
				$response['msg'] 		= "Saved Successfully.";
			else
				$response['msg'] 		= "Update Successfully.";
			
		}
		else
		{
			$this->db->rollback();
			$response['type'] 			= "fail";
			$response['msg'] 			= $savedMasseged;
			$response['sql']			= $error_sql;
		}
		return json_encode($response);
	}
	private function checkPermission($programCode,$session_userId,$feild)
	{
		global $obj_common;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$chkPermission 	= $obj_common->Load_menupermision2($programCode,$session_userId,$feild);
		if($chkPermission!=1)
		{
			if($savedStatus)
			{
				$savedMasseged	= "You do not have permission to Save Chart Of Account.";
				$savedStatus 	= false;
			}			
		}
	}
	private function save_sql($searchId,$financeType,$mainType,$subType,$accountCode,$accountName,$bank,$active)
	{
		if($searchId=='')
			$this->saveData($financeType,$mainType,$subType,$accountCode,$accountName,$bank,$active);
		else
			$this->updateData($searchId,$financeType,$mainType,$subType,$accountCode,$accountName,$bank,$active);
		
	}
	private function save_bankDetails($searchId,$insertId,$bankId,$currency)
	{
		$this->deleteBank($searchId,$insertId);
		$this->saveBank($searchId,$insertId,$bankId,$currency);
	}
	private function saveBank($searchId,$insertId,$bankId,$currency)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		if($searchId=='')
			$chartOfAccId	= $insertId;
		else
			$chartOfAccId	= $searchId;
			
		$sql = "INSERT INTO finance_mst_chartofaccount_bank 
				(
				BANK_ID, 
				CURRENCY_ID, 
				CHART_OF_ACCOUNT_ID
				)
				VALUES
				(
				'$bankId', 
				'$currency', 
				'$chartOfAccId'
				)";
		
		$result 	= $this->db->RunQuery2($sql);
		if(!$result && $savedStatus)
		{
			$savedMasseged	= $this->db->errormsg;
			$error_sql		= $sql;
			$savedStatus 	= false;
		}
	}
	private function deleteBank($searchId,$insertId)
	{
		if($searchId=='')
			$chartOfAccId	= $insertId;
		else
			$chartOfAccId	= $searchId;
			
		$sql = "DELETE FROM finance_mst_chartofaccount_bank 
				WHERE
				CHART_OF_ACCOUNT_ID = '$chartOfAccId'  ";
		
		$result 	= $this->db->RunQuery2($sql);
	}
	private function saveData($financeType,$mainType,$subType,$accountCode,$accountName,$bank,$active)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_userId;
		global $insertId;
		
		$sql = "INSERT INTO finance_mst_chartofaccount 
				(
				CHART_OF_ACCOUNT_CODE, 
				CHART_OF_ACCOUNT_NAME, 
				SUB_TYPE_ID, 
				BANK, 
				STATUS, 
				CREATED_BY, 
				CREATED_DATE
				)
				VALUES
				(
				'$accountCode', 
				'$accountName', 
				'$subType', 
				'$bank', 
				'$active', 
				'$session_userId', 
				 NOW()
				);";
		
		$result 	= $this->db->RunQuery2($sql);
		$insertId	= $this->db->insertId;
		
		if(!$result && $savedStatus)
		{
			$savedMasseged	= $this->db->errormsg;
			$error_sql		= $sql;
			$savedStatus 	= false;
		}
	}
	private function updateData($searchId,$financeType,$mainType,$subType,$accountCode,$accountName,$bank,$active)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_userId;
		global $insertId;
		
		$sql = "UPDATE finance_mst_chartofaccount 
				SET
				CHART_OF_ACCOUNT_CODE = '$accountCode' , 
				CHART_OF_ACCOUNT_NAME = '$accountName' , 
				SUB_TYPE_ID = '$subType' , 
				BANK = '$bank' , 
				STATUS = '$active' , 
				LAST_MODIFY_BY = '$session_userId' , 
				LAST_MODIFY_DATE = NOW()
				WHERE
				CHART_OF_ACCOUNT_ID = '$searchId' ";
		
		$result 	= $this->db->RunQuery2($sql);
		$insertId	= $searchId;
		
		if(!$result && $savedStatus)
		{
			$savedMasseged	= $this->db->errormsg;
			$error_sql		= $sql;
			$savedStatus 	= false;
		}
	}
	private function updateTransactionType($insertId,$chartOfAccField,$chkStatus)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "UPDATE finance_mst_chartofaccount 
				SET
				$chartOfAccField = '$chkStatus'
				WHERE
				CHART_OF_ACCOUNT_ID = '$insertId' ";
		
		$result 	= $this->db->RunQuery2($sql);
		if(!$result && $savedStatus)
		{
			$savedMasseged	= $this->db->errormsg;
			$error_sql		= $sql;
			$savedStatus 	= false;
		}
	}
	private function saveCompany($insertId,$companyId)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "INSERT INTO finance_mst_chartofaccount_company 
				(
				COMPANY_ID, 
				CHART_OF_ACCOUNT_ID
				)
				VALUES
				(
				'$companyId', 
				'$insertId'
				) ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result && $savedStatus)
		{
			$savedMasseged	= $this->db->errormsg;
			$error_sql		= $sql;
			$savedStatus 	= false;
		}
	}
	private function deleteCompany($insertId)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "DELETE FROM finance_mst_chartofaccount_company 
				WHERE CHART_OF_ACCOUNT_ID = '$insertId' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result && $savedStatus)
		{
			$savedMasseged	= $this->db->errormsg;
			$error_sql		= $sql;
			$savedStatus 	= false;
		}
	}
}
?>
<?php
class Cls_Bank_Set
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	public function save($bankCode,$bankName,$country,$status,$userId)
	{
		return $this->save_sql($bankCode,$bankName,$country,$status,$userId);
	}
	public function update($searchId,$bankCode,$bankName,$country,$status,$userId)
	{
		return $this->update_sql($searchId,$bankCode,$bankName,$country,$status,$userId);
	}
	public function delete($bankId)
	{
		return $this->delete_sql($bankId);
	}
	private function save_sql($bankCode,$bankName,$country,$status,$userId)
	{
		$sql = "INSERT INTO mst_bank 
				(
				BANK_CODE, 
				BANK_NAME, 
				STATUS, 
				COUNTRY_ID, 
				CREATED_BY, 
				CREATED_DATE
				)
				VALUES
				(
				'$bankCode', 
				'$bankName', 
				'$status', 
				'$country', 
				'$userId', 
				NOW()
				)";
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function update_sql($searchId,$bankCode,$bankName,$country,$status,$userId)
	{
		$sql = "UPDATE mst_bank 
				SET
				BANK_CODE = '$bankCode' , 
				BANK_NAME = '$bankName' , 
				STATUS = '$status' , 
				COUNTRY_ID = '$country' , 
				CREATED_BY = '$userId' , 
				CREATED_DATE = NOW()
				WHERE
				BANK_ID = '$searchId' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function delete_sql($bankId)
	{
		$sql = "DELETE FROM mst_bank 
				WHERE
				BANK_ID = '$bankId' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
}
?>
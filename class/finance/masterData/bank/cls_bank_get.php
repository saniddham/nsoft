<?php
class Cls_Bank_Get
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	public function getSaveBanks()
	{
		return $this->getSaveBanks_sql();
	}
	public function getSearchData($bankId)
	{
		return $this->getSearchData_sql($bankId);
	}
	private function getSaveBanks_sql()
	{
		$sql = "SELECT BANK_ID,BANK_NAME
				FROM mst_bank
				ORDER BY BANK_NAME ";
		
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	private function getSearchData_sql($bankId)
	{
		$sql = "SELECT 	BANK_CODE, 
				BANK_NAME, 
				STATUS, 
				COUNTRY_ID, 
				CREATED_BY
				FROM 
				mst_bank 
				WHERE BANK_ID = '$bankId' ";
		
		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		return $row;
	}
}	
?>
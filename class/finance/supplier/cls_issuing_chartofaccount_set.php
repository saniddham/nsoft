<?php
$session_userId		= $_SESSION["userId"];
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];

class Cls_Issuing_ChartofAccount_Set
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	
	public function saveFinanceTransaction($docNo,$docYear,$docType)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;

		$this->subCatRelatedGLSave($docNo,$docYear,$docType);
		$this->issueRelatedGLSave($docNo,$docYear,$docType);
	}
	private function subCatRelatedGLSave($docNo,$docYear,$docType)
	{
		$detail_result	= $this->getDetailData($docNo,$docYear,$docType);
		while($row = mysqli_fetch_array($detail_result))
		{
			$subCategory	= $row['intSubCategory'];
			$amount			= $row['amount'];
			$currencyId		= $row['intCurrencyId'];
			$supplierId		= $row['intSupplier'];
			if($docType=='ISSUE')
				$transacType	= 'C';
			else
				$transacType	= 'D';
			
			$chartofaccId 	= $this->getSubCatRLCOAId($subCategory);
			$chkStatus	  	= $this->chkIssueNoSaved($docNo,$docYear,$chartofaccId,$supplierId,$currencyId,$transacType,$docType);
			
			if($chkStatus==0)
				$this->saveQuery($chartofaccId,$docNo,$docYear,$subCategory,$amount,$currencyId,$supplierId,$transacType,$docType);	
		}		
	}
	private function issueRelatedGLSave($docNo,$docYear,$docType)
	{
		$detail_result	= $this->getDetailData($docNo,$docYear,$docType);
		while($row = mysqli_fetch_array($detail_result))
		{
			$subCategory	= $row['intSubCategory'];
			$amount			= $row['amount'];
			$currencyId		= $row['intCurrencyId'];
			$supplierId		= $row['intSupplier'];
			if($docType=='ISSUE')
				$transacType	= 'D';
			else
				$transacType	= 'C';
			
			$chartofaccId = $this->getIssueRLCOAId($subCategory);
			$chkStatus	  = $this->chkIssueNoSaved($docNo,$docYear,$chartofaccId,$supplierId,$currencyId,$transacType,$docType);
			
			if($chkStatus==0)
				$this->saveQuery($chartofaccId,$docNo,$docYear,$subCategory,$amount,$currencyId,$supplierId,$transacType,$docType);	
		}
	}
	private function getSubCatRLCOAId($subCat)
	{
		$sql = "SELECT CHART_OF_ACCOUNT_ID
				FROM finance_mst_chartofaccount_subcategory
				WHERE SUBCATEGORY_ID='$subCat' ";
		
		$result = $this->db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		
		return $row['CHART_OF_ACCOUNT_ID'];
	}
	private function getIssueRLCOAId($subCat)
	{
		$sql = "SELECT IRCHA.RL_CHART_OF_ACCOUNT_ID
				FROM finance_mst_issuing_related_chartofaccount IRCHA
				INNER JOIN finance_mst_chartofaccount_subcategory CHAS ON CHAS.CHART_OF_ACCOUNT_ID=IRCHA.CHART_OF_ACCOUNT_ID
				WHERE CHAS.SUBCATEGORY_ID='$subCat' ";
		
		$result = $this->db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		
		return $row['RL_CHART_OF_ACCOUNT_ID'];
	}
	private function chkIssueNoSaved($docNo,$docYear,$chartofaccId,$supplierId,$currencyId,$transacType,$docType)
	{
		global $session_companyId;
		global $session_locationId;
		
		$sql = "SELECT * 
				FROM finance_transaction
				WHERE DOCUMENT_TYPE='$docType' AND
				CHART_OF_ACCOUNT_ID='$chartofaccId' AND
				DOCUMENT_NO='$docNo' AND
				DOCUMENT_YEAR='$docYear' AND
				TRANSACTION_TYPE='$transacType' AND
				TRANSACTION_CATEGORY_ID='$supplierId' AND
				CURRENCY_ID='$currencyId' AND
				LOCATION_ID='$session_locationId' AND
				COMPANY_ID='$session_companyId' ";
		$result   = $this->db->RunQuery($sql);
		$rowCount = mysqli_num_rows($result);
	
		if($rowCount>0)
			return 1;
		else
			return 0;
	}
	private function getDetailData($docNo,$docYear,$docType)
	{

		$sql = "SELECT ROUND(SUM(STB.dblGRNRate*ABS(STB.dblQty)),2) AS amount,MI.intSubCategory,STB.intCurrencyId,PH.intSupplier
				FROM ware_stocktransactions_bulk STB
				INNER JOIN mst_item MI ON MI.intId=STB.intItemId
				INNER JOIN ware_grnheader GH ON GH.intGrnNo=STB.intGRNNo AND GH.intGrnYear=STB.intGRNYear
				INNER JOIN trn_poheader PH ON GH.intPoNo=PH.intPONo AND GH.intPoYear=PH.intPOYear
				WHERE STB.strType='$docType' AND
				STB.intDocumentNo='$docNo' AND
				STB.intDocumntYear='$docYear'
				GROUP BY MI.intSubCategory,STB.intCurrencyId";

		$result   = $this->db->RunQuery($sql);
		
		return $result;
	}
	private function saveQuery($chartofaccId,$docNo,$docYear,$subCategory,$amount,$currencyId,$supplierId,$transacType,$docType)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_companyId;
		global $session_locationId;
		global $session_userId;
		
		$sql = "INSERT INTO finance_transaction 
				( 
				CHART_OF_ACCOUNT_ID, 
				AMOUNT, 
				DOCUMENT_NO, 
				DOCUMENT_YEAR, 
				DOCUMENT_TYPE, 
				TRANSACTION_TYPE, 
				TRANSACTION_CATEGORY,
				TRANSACTION_CATEGORY_ID, 
				CURRENCY_ID, 
				LOCATION_ID, 
				COMPANY_ID, 
				LAST_MODIFIED_BY, 
				LAST_MODIFIED_DATE
				)
				VALUES
				(
				'$chartofaccId', 
				'$amount', 
				'$docNo', 
				'$docYear', 
				'$docType', 
				'$transacType', 
				'SU',
				'$supplierId',
				'$currencyId', 
				'$session_locationId', 
				'$session_companyId', 
				'$session_userId', 
				NOW()
				);";

			$result = $this->db->RunQuery($sql);
	}
}
?>
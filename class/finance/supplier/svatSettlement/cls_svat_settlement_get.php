<?php
//ini_set('display_errors',1);

$session_userId		= $_SESSION["userId"];
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];

class Cls_svat_settlement_Get 
{
	private $db;
	private $obj_errorHandeling;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	
	public function getSupplierResult($executionType)
	{
		$sql	= $this->loadSupplierSql();
		$result = $this->db->$executionType($sql);
 		return $result;
	}
	public function getInvoiceResult($supplier,$dtFrom,$dtTo,$invoiceNo,$invoiceYear,$executionType)
	{
		return $this->loadInvoiceResult($supplier,$dtFrom,$dtTo,$invoiceNo,$invoiceYear,$executionType);
	}

	private function loadSupplierSql()
	{
		
		$sql = "SELECT distinct MS.intId,MS.strName
				FROM  mst_supplier MS 
				WHERE MS.intStatus=1  
				ORDER BY MS.strName ";
 		return $sql;
	}
	
 	private function loadInvoiceResult($supplier,$dtFrom,$dtTo,$invoiceNo,$invoiceYear,$executionType)
	{
		$sql = "SELECT
				finance_transaction.TRANSACTION_CATEGORY_ID AS SUP_ID,
				mst_supplier.strName AS SUPPLIER,
				finance_supplier_purchaseinvoice_header.INVOICE_NO as SUP_INV_NO,
				finance_transaction.INVOICE_NO,
				finance_transaction.INVOICE_YEAR,
				finance_transaction.CURRENCY_ID,
				SUM(IF((DOCUMENT_TYPE='INVOICE'),finance_transaction.AMOUNT,finance_transaction.AMOUNT*-1)) AS BAL_AMOUNT
				FROM
				finance_transaction
				INNER JOIN mst_supplier ON finance_transaction.TRANSACTION_CATEGORY_ID = mst_supplier.intId
				INNER JOIN finance_supplier_purchaseinvoice_header ON finance_transaction.INVOICE_NO = finance_supplier_purchaseinvoice_header.PURCHASE_INVOICE_NO AND finance_transaction.INVOICE_YEAR = finance_supplier_purchaseinvoice_header.PURCHASE_INVOICE_YEAR
				WHERE
				finance_transaction.DOCUMENT_TYPE IN ('INVOICE', 'INVOICE_SETTLEMENT') AND
				finance_transaction.TRANSACTION_CATEGORY = 'SU' AND
				finance_transaction.CHART_OF_ACCOUNT_ID = '97' "; 
		
		if($supplier !='')
		$sql .= " AND finance_transaction.TRANSACTION_CATEGORY_ID = '$supplier' ";
		if($dtTo !='')
		$sql .= " AND finance_transaction.LAST_MODIFIED_DATE <= '$dtTo' ";
		if($dtFrom !='')
		$sql .= " AND finance_transaction.LAST_MODIFIED_DATE >= '$dtFrom'";
		if($invoiceNo !='')
		$sql .= " AND finance_transaction.INVOICE_NO = '$invoiceNo'";
		if($invoiceYear !='')
		$sql .= " AND finance_transaction.INVOICE_YEAR = '$invoiceYear'";
		
		
		$sql .= " GROUP BY
				finance_transaction.TRANSACTION_CATEGORY_ID,
				finance_transaction.INVOICE_NO,
				finance_transaction.INVOICE_YEAR,
				finance_transaction.CHART_OF_ACCOUNT_ID
				HAVING  BAL_AMOUNT > 0
				ORDER BY
				SUPPLIER ASC,
				finance_transaction.INVOICE_YEAR ASC,
				finance_transaction.INVOICE_NO ASC ";

		$result = $this->db->$executionType($sql);
 		return $result;
	}
 }
?>
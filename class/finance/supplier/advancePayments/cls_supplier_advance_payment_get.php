<?php
ini_set('display_errors',0);
$session_userId		= $_SESSION["userId"];
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];

class Cls_Supplier_Advance_Payment_Get
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	public function getHeaderData_pdf($paymentNo,$paymentYear,$executionType)
	{
		return $this->HeaderData_pdf($paymentNo,$paymentYear,$executionType);
	}
	public function getDetailData_pdf($paymentNo,$paymentYear,$executionType)
	{
		return $this->DetailData_pdf($paymentNo,$paymentYear,$executionType);
	}
 	private function HeaderData_pdf($paymentNo,$paymentYear,$executionType)
	{
		global $companyId;
		
		$sql = "SELECT PH.CURRENCY_ID,
				FC.strCode AS currency,
				PGLH.COST_CENTER,
				COA.CHART_OF_ACCOUNT_NAME,
				MFD.strCostCenterCode,
				DATE(PH.PAY_DATE) AS paymentDate,
				(SELECT CONCAT(RIGHT(YEAR(DATE(dtmStartingDate)),2),'-',RIGHT(YEAR(DATE(dtmClosingDate)),2))
				FROM mst_financeaccountingperiod
				INNER JOIN mst_financeaccountingperiod_companies ON mst_financeaccountingperiod.intId=mst_financeaccountingperiod_companies.intPeriodId
				WHERE dtmStartingDate<=DATE(PH.PAY_DATE) AND
				dtmClosingDate>=DATE(PH.PAY_DATE) AND
				mst_financeaccountingperiod_companies.intCompanyId='$companyId') AS voucherYear,
				LPAD(MONTH(PH.PAY_DATE),2,'0') AS voucherMonth,
				(PH.VOUCHER_NO) as voucherNo,
				MS.strName AS supplier,
				PH.REMARKS,
				PH.STATUS,
				PH.PRINT_STATUS,
				PH.CREATED_BY,
				ROUND((SELECT SUM(AMOUNT) FROM finance_supplier_advancepayment_details PD WHERE PD.ADVANCE_PAYMENT_NO = PH.ADVANCE_PAYMENT_NO AND PD.ADVANCE_PAYMENT_YEAR = PH.ADVANCE_PAYMENT_YEAR),2) AS TOTAL_AMOUNT,
				PH.COMPANY_ID AS COMPANY_ID,
				PH.BANK_REFERENCE_NO	AS CHEQUE_NO
				FROM finance_supplier_advancepayment_header PH
				INNER JOIN finance_supplier_advancepayment_details PGLH ON PH.ADVANCE_PAYMENT_NO=PGLH.ADVANCE_PAYMENT_NO AND 
				PH.ADVANCE_PAYMENT_YEAR=PGLH.ADVANCE_PAYMENT_YEAR
				INNER JOIN mst_financecurrency FC ON PH.CURRENCY_ID=FC.intId
				INNER JOIN finance_mst_chartofaccount COA ON PGLH.ACCOUNT_ID=COA.CHART_OF_ACCOUNT_ID
				INNER JOIN mst_supplier MS ON MS.intId=PH.SUPPLIER_ID
				INNER JOIN mst_financedimension MFD ON MFD.intId=PGLH.COST_CENTER
				WHERE PH.ADVANCE_PAYMENT_NO='$paymentNo' AND
				PH.ADVANCE_PAYMENT_YEAR='$paymentYear' AND
				PH.COMPANY_ID='$companyId' ";
		
		$result 	= $this->db->$executionType($sql);
 		$resultArry = mysqli_fetch_array($result);
	
		return $resultArry;
	}
	private function DetailData_pdf($paymentNo,$paymentYear,$executionType)
	{
		global $companyId;
		
		$sql = "SELECT NULL as invoiceNo,
				PD.AMOUNT as amonut,
				CONCAT(PH.PO_NO,'-',PH.PO_YEAR) AS PONo,
				NULL AS GRNNo
				FROM finance_supplier_advancepayment_details PD
				INNER JOIN finance_supplier_advancepayment_header PH ON PH.ADVANCE_PAYMENT_NO=PD.ADVANCE_PAYMENT_NO AND
				PH.ADVANCE_PAYMENT_YEAR=PD.ADVANCE_PAYMENT_YEAR
				WHERE PD.ADVANCE_PAYMENT_NO='$paymentNo' AND
				PD.ADVANCE_PAYMENT_YEAR='$paymentYear' ";
		
		$result	= $this->db->$executionType($sql);
		
		return $result;
	}
 }
?>
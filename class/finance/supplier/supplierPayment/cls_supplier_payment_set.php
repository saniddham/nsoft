<?php
$session_userId			= $_SESSION["userId"];
$session_locationId 	= $_SESSION["CompanyID"];
$session_companyId 		= $_SESSION["headCompanyId"];
$savedMasseged			= "";
$error_sql				= "";

class Cls_Supplier_Payment_Set
{
	private $db;
	private $finance_common_get;
	private $obj_exchange_rate_get;
	private $obj_cf_common;
	private $obj_cf_common_set;
	
	function __construct($db,$finance_common_get,$obj_exchange_rate_get,$obj_cf_common,$obj_cf_common_set)
	{
		$this->db = $db;
		$this->finance_common_get 		= $finance_common_get;
		$this->obj_exchange_rate_get 	= $obj_exchange_rate_get;
		$this->obj_cf_common 			= $obj_cf_common;
		$this->obj_cf_common_set 		= $obj_cf_common_set;
	}
	public function approve($paymentNo,$paymentYear)
	{	
		
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_locationId;
		global $session_companyId;
		global $session_userId;
		global $finalApproval;
		
		$savedStatus	= true;
		$finalApproval	= false;
		$this->db->begin();
		
		$this->updateHeaderStatus($paymentNo,$paymentYear,'','');
		$this->approvedData($paymentNo,$paymentYear);	
		
		if($savedStatus)
		{
			$this->db->commit();
			$response['type'] 			= "pass";
			if($finalApproval)
				$response['msg'] 		= "Final Approval Raised Successfully.";
			else
				$response['msg'] 		= "Approved Successfully.";
		}
		else
		{
			$this->db->rollback();
			$response['type'] 			= "fail";
			$response['msg'] 			= $savedMasseged;
			$response['sql']			= $error_sql;
		}
		return json_encode($response);
	}
	public function reject($paymentNo,$paymentYear)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_locationId;
		global $session_companyId;
		global $session_userId;
		
		$savedStatus	= true;
		$this->db->begin();
		
		$this->updateHeaderStatus($paymentNo,$paymentYear,'0','');
		$this->approved_by_insert($paymentNo,$paymentYear,$session_userId,0);
		
		if($savedStatus)
		{
			$this->db->commit();
			$response['type'] 		= "pass";
			$response['msg'] 		= "Rejected Successfully.";
		}
		else
		{
			$this->db->rollback();
			$response['type'] 			= "fail";
			$response['msg'] 			= $savedMasseged;
			$response['sql']			= $error_sql;
		}
		return json_encode($response);
	}
	
	public function cancel($paymentNo,$paymentYear,$cancel_reason)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;		
		global $session_userId;
		
		$savedStatus	= true;
		$this->db->begin();
		
		$this->updateHeaderStatus($paymentNo,$paymentYear,'-2',$cancel_reason);
		$this->approved_by_insert($paymentNo,$paymentYear,$session_userId,'-2');
		$this->deleteSupplierTransaction($paymentNo,$paymentYear);
		$this->deleteFinanceTransaction($paymentNo,$paymentYear);
		
		if($savedStatus)
		{
			$this->db->commit();
			$response['type'] 		= "pass";
			$response['msg'] 		= "Cancel successfully.";
		}
		else
		{
			$this->db->rollback();
			$response['type'] 			= "fail";
			$response['msg'] 			= $savedMasseged;
			$response['sql']			= $error_sql;
		}
		return json_encode($response);
	}
	
	public function saveHeader($paymentNo,$paymentYear,$approveLevels,$status,$supplierId,$payDate,$currency,$payMethod,$Remarks,$bankRefNo,$userId,$companyId,$locationId)
	{
		$sql = "INSERT INTO finance_supplier_payment_header 
				(
				PAYMENT_NO, 
				PAYMENT_YEAR, 
				SUPPLIER_ID, 
				PAY_DATE, 
				COMPANY_ID, 
				LOCATION_ID,
				CURRENCY_ID, 
				PAY_METHOD, 
				REMARKS, 
				BANK_REFERENCE_NO, 
				STATUS, 
				APPROVE_LEVELS, 
				CREATED_BY, 
				CREATED_DATE
				)
				VALUES
				(
				'$paymentNo', 
				'$paymentYear', 
				'$supplierId', 
				'$payDate', 
				'$companyId',
				'$locationId', 
				'$currency', 
				'$payMethod', 
				'$Remarks', 
				'$bankRefNo', 
				'$status', 
				'$approveLevels', 
				'$userId', 
				NOW()
				) ";
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	public function updateHeader($paymentNo,$paymentYear,$approveLevels,$status,$supplierId,$payDate,$currency,$payMethod,$Remarks,$bankRefNo,$userId,$companyId,$locationId)
	{
		$sql = "UPDATE finance_supplier_payment_header 
				SET
				SUPPLIER_ID = '$supplierId' , 
				PAY_DATE = '$payDate' , 
				COMPANY_ID = '$companyId' , 
				LOCATION_ID = '$locationId' ,
				CURRENCY_ID = '$currency' , 
				PAY_METHOD = '$payMethod' , 
				REMARKS = '$Remarks' , 
				BANK_REFERENCE_NO = '$bankRefNo' , 
				STATUS = '$status' , 
				APPROVE_LEVELS = '$approveLevels' , 
				LAST_MODIFY_BY = '$userId' , 
				LAST_MODIFY_DATE = NOW()
				WHERE
				PAYMENT_NO = '$paymentNo' AND 
				PAYMENT_YEAR = '$paymentYear'";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	public function updateMaxStatus($paymentNo,$paymentYear)
	{
		$sqlMax = " SELECT (MAX(tb.STATUS)+1) AS maxStatus  
					FROM finance_supplier_payment_approveby AS tb
					WHERE tb.PAYMENT_NO='$paymentNo' AND
					tb.PAYMENT_YEAR='$paymentYear' ";
		
		$resultMax 	= $this->db->RunQuery2($sqlMax);
		$rowMax		= mysqli_fetch_array($resultMax);
		$maxStatus 	= $rowMax['maxStatus'];
		
		$sqlUpd = " UPDATE finance_supplier_payment_approveby 
					SET
					STATUS = '$maxStatus'
					WHERE
					PAYMENT_NO = '$paymentNo' AND 
					PAYMENT_YEAR = '$paymentYear' ";
		
		$resultUpd = $this->db->RunQuery2($sqlUpd);
		if(!$resultUpd)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sqlUpd;
		}
		return $data;
	}
	public function deleteDetails($paymentNo,$paymentYear)
	{
		$sql = "DELETE FROM finance_supplier_payment_details 
				WHERE
				PAYMENT_NO = '$paymentNo' AND 
				PAYMENT_YEAR = '$paymentYear' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	public function deleteGLDetails($paymentNo,$paymentYear)
	{
		$sql = "DELETE FROM finance_supplier_payment_gl 
				WHERE
				PAYMENT_NO = '$paymentNo' AND 
				PAYMENT_YEAR = '$paymentYear' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	public function saveDetails($paymentNo,$paymentYear,$invoiceNo,$invoiceYear,$payAmount)
	{
		$sql = "INSERT INTO finance_supplier_payment_details 
				(
				PAYMENT_NO, 
				PAYMENT_YEAR, 
				PURCHASE_INVOICE_NO, 
				PURCHASE_INVOICE_YEAR, 
				AMOUNT
				)
				VALUES
				(
				'$paymentNo', 
				'$paymentYear', 
				'$invoiceNo', 
				'$invoiceYear', 
				'$payAmount'
				)";
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	public function updateVoucherNo($paymentNo,$paymentYear,$voucherNo)
	{
		$sql = "UPDATE finance_supplier_payment_header 
				SET
				VOUCHER_NO = '$voucherNo'
				WHERE
				PAYMENT_NO = '$paymentNo' AND 
				PAYMENT_YEAR = '$paymentYear' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	public function saveGLDetails($paymentNo,$paymentYear,$accountId,$amount,$memo,$costCenter)
	{
		$sql = "INSERT INTO finance_supplier_payment_gl 
				(
				PAYMENT_NO, PAYMENT_YEAR, 
				ACCOUNT_ID, AMOUNT, 
				MEMO, COST_CENTER
				)
				VALUES
				(
				'$paymentNo', '$paymentYear', 
				'$accountId', '$amount', 
				'$memo', '$costCenter'
				)";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	public function confirmPaymnet($paymentNo,$paymentYear)
	{
		return $this->confirmPaymnet_sql($paymentNo,$paymentYear);
	}
	private function getHeaderData($paymentNo,$paymentYear)
	{
		$sql = "SELECT STATUS,
				APPROVE_LEVELS,
				SUPPLIER_ID,
				CURRENCY_ID,
				PAY_DATE,
				COMPANY_ID,
				LOCATION_ID,
				BANK_REFERENCE_NO,
				REMARKS,
				SUM(finance_supplier_payment_details.AMOUNT) AS totAmount,
				finance_supplier_payment_gl.ACCOUNT_ID
				FROM finance_supplier_payment_header 
				INNER JOIN finance_supplier_payment_gl ON finance_supplier_payment_gl.PAYMENT_NO=finance_supplier_payment_header.PAYMENT_NO AND
				finance_supplier_payment_gl.PAYMENT_YEAR=finance_supplier_payment_header.PAYMENT_YEAR
				INNER JOIN finance_supplier_payment_details ON finance_supplier_payment_details.PAYMENT_NO=finance_supplier_payment_header.PAYMENT_NO AND
				finance_supplier_payment_details.PAYMENT_YEAR=finance_supplier_payment_header.PAYMENT_YEAR
				WHERE finance_supplier_payment_header.PAYMENT_NO='$paymentNo' AND
				finance_supplier_payment_header.PAYMENT_YEAR='$paymentYear'
				GROUP BY finance_supplier_payment_header.PAYMENT_YEAR,finance_supplier_payment_header.PAYMENT_NO ";
		
		$result = $this->db->RunQuery2($sql);
		$row 	= mysqli_fetch_array($result);
		
		return $row;
	}
	private function validatePayAmount($paymentNo,$paymentYear,$supplierId,$currency)
	{
		global $savedStatus;
		global $savedMasseged;
		
		$sql = "SELECT PURCHASE_INVOICE_NO,PURCHASE_INVOICE_YEAR,AMOUNT
				FROM finance_supplier_payment_details
				WHERE PAYMENT_NO='$paymentNo' AND
				PAYMENT_YEAR='$paymentYear' ";
		
		$result = $this->db->RunQuery2($sql);
		while($row = mysqli_fetch_array($result))
		{
			$tobePaid = $this->validateBalAmount($supplierId,$currency,$row['PURCHASE_INVOICE_NO'],$row['PURCHASE_INVOICE_YEAR']);
			
			if(($row['AMOUNT']>$tobePaid) && ($savedStatus))
			{
				$savedStatus 	= false;
				$savedMasseged 	= "Some Paying amount exceed Invoice amount.";
			}
		}
	}
	private function validateBalAmount($supplierId,$currency,$invoiceNo,$invoiceYear)
	{
		global $session_companyId;
		
		$sql = "SELECT ROUND(SUM(ST.VALUE),4) AS toBePaid
				FROM finance_supplier_transaction ST
				WHERE ST.PURCHASE_INVOICE_NO='$invoiceNo' AND
				ST.PURCHASE_INVOICE_YEAR='$invoiceYear' AND
				ST.CURRENCY_ID='$currency' AND
				ST.COMPANY_ID='$session_companyId' AND
				ST.SUPPLIER_ID='$supplierId' ";
		
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row['toBePaid'];
	}
	private function updateHeaderStatus($paymentNo,$paymentYear,$status,$reason_cancel)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		if($status=='') 
			$para='STATUS-1';
 		else
			$para=$status;
		
		$sql = "UPDATE finance_supplier_payment_header 
				SET
				CANCEL_REASON 	= '".$reason_cancel."' ,  
				STATUS 			= ".$para." 
				WHERE
				PAYMENT_NO 		= '$paymentNo' AND 
				PAYMENT_YEAR 	= '$paymentYear' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function approved_by_insert($paymentNo,$paymentYear,$session_userId,$approval)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "INSERT INTO finance_supplier_payment_approveby 
				(
				PAYMENT_NO, 
				PAYMENT_YEAR, 
				APPROVE_LEVEL_NO, 
				APPROVED_BY, 
				APPROVED_DATE, 
				STATUS
				)
				VALUES
				(
				'$paymentNo', 
				'$paymentYear', 
				'$approval', 
				'$session_userId', 
				NOW(), 
				0
				) ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function approvedData($paymentNo,$paymentYear)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_userId;
		global $finalApproval;
		global $finance_common_get;
		
		$header_arr 	= $this->getHeaderData($paymentNo,$paymentYear);
		$status			= $header_arr['STATUS'];
		$savedLevels	= $header_arr['APPROVE_LEVELS'];
	
		if($status==1)
		{
			$finalApproval = true;
			$this->validatePayAmount($paymentNo,$paymentYear,$header_arr['SUPPLIER_ID'],$header_arr['CURRENCY_ID']);
			$this->SaveTransaction($paymentNo,$paymentYear,'PAYMENT',$session_userId);
			$this->SaveSupplierTransaction($paymentNo,$paymentYear,$session_userId,$header_arr['totAmount']);
			$this->SaveBankTransaction($paymentNo,$paymentYear,$session_userId,$header_arr['totAmount']);
			$result = $finance_common_get->UpdateSupplierForcast($header_arr['totAmount'],$header_arr['SUPPLIER_ID'],$header_arr['CURRENCY_ID'],$header_arr['PAY_DATE']);
			if(!$result['type'])
			{
				$savedStatus	= false;
				$savedMasseged	= $result['msg'];
				$error_sql		= $result['sql'];				
			}
			
			//-------------------------- cash flow update 28/05/2014 by lahiru --------------------------------
			
				$CFUpdResult	= $this->getDetailData($paymentNo,$paymentYear);
				while($row = mysqli_fetch_array($CFUpdResult))
				{
					$amount	   		= $row['AMOUNT'];	
					$currencyId		= $row['CURRENCY_ID'];
					$entryDate		= $row['PAY_DATE'];
					$company		= $row['COMPANY_ID'];
					//$rpt_field		= $row['REPORT_FIELD_ID'];
					$location		= $row['LOCATION_ID'];
					$purInvNo		= $row['PURCHASE_INVOICE_NO'];
					$purInvYear		= $row['PURCHASE_INVOICE_YEAR'];
					$tot_to_save	= $amount;
					
					$resultInvWise	= $this->getInvoicedDetails($purInvNo,$purInvYear);
					while($rowInv = mysqli_fetch_array($resultInvWise))
					{
						$rpt_field		= $rowInv['REPORT_FIELD_ID'];
						$invAmount	   	= $rowInv['invoiceAmt'];
						$invoiceDate	= $rowInv['INVOICE_DATE'];
						
						if($tot_to_save>0)
						{
							if($invAmount<=$tot_to_save)
							{
								$saveInvAmount 	 	= $invAmount;
								$tot_to_save 		= $tot_to_save-$invAmount;
							}
							else if($invAmount>$tot_to_save)
							{
								$saveInvAmount 		= $tot_to_save;
								$tot_to_save		= 0;
							}
							$cfAmountResult	= $this->obj_cf_common->get_cf_log_bal_to_payment_result($location,$rpt_field,$currencyId,$entryDate,$company,'RunQuery2');
							while($rowAmt = mysqli_fetch_array($cfAmountResult))
							{
								$balAmount	= round($rowAmt['BAL'],2);
								if(($saveInvAmount>0) && ($balAmount>0))
								{
									if($saveInvAmount<=$balAmount)
									{
										$saveAmnt 	 	= $saveInvAmount;
										$saveInvAmount 	= 0;
									}
									else if($saveInvAmount>$balAmount)
									{
										$saveAmnt 		= $balAmount;
										$saveInvAmount	= $saveInvAmount-$saveAmnt;
									}
									
									$rcv_date	= $rowAmt['RECEIVE_DATE'];
									$currency	= $rowAmt['CURRENCY_ID'];
									
									$resultArr	= $this->obj_cf_common_set->insert_to_log($location,$rpt_field,$rcv_date,$invoiceDate,$entryDate,$paymentNo,$paymentYear,'SUPPLIER_PAYMENT',$saveAmnt,$currency,'RunQuery2');
									if($resultArr['type']=='fail' && $savedStatus)
									{
										$savedStatus	= false;
										$savedMasseged	= $resultArr['msg'];
										$error_sql		= $resultArr['sql'];
									}
								}
							}
						}
					}
					if($saveInvAmount>0)
					{
						if($rcv_date == '')
							$rcv_date = $entryDate;
						
						if($currency == '')
							$currency = $currencyId;
								
						$resultInvWise	= $this->getInvoicedDetails($purInvNo,$purInvYear);
						while($rowInv = mysqli_fetch_array($resultInvWise))
						{
							$rpt_field		= $rowInv['REPORT_FIELD_ID'];
							$invAmount	   	= $rowInv['invoiceAmt'];
							$invoiceDate	= $rowInv['INVOICE_DATE'];
							
							if($saveInvAmount>0)
							{
								if($invAmount<=$saveInvAmount)
								{
									$saveAmnt 	 	= $invAmount;
									$saveInvAmount 	= $saveInvAmount-$invAmount;
								}
								else if($invAmount>$saveInvAmount)
								{
									$saveAmnt 		= $saveInvAmount;
									$saveInvAmount	= 0;
								}
								
								$resultArr	= $this->obj_cf_common_set->insert_to_log($location,$rpt_field,$rcv_date,$invoiceDate,$entryDate,$paymentNo,$paymentYear,'SUPPLIER_PAYMENT',$saveAmnt,$currency,'RunQuery2');
								if($resultArr['type']=='fail' && $savedStatus)
								{
									$savedStatus	= false;
									$savedMasseged	= $resultArr['msg'];
									$error_sql		= $resultArr['sql'];
								}
							}		
						}	
					}
					if($tot_to_save>0)
					{
						if($rcv_date == '')
							$rcv_date = $entryDate;
					
						$resultInvWise	= $this->getInvoicedDetails($purInvNo,$purInvYear);
						while($rowInv = mysqli_fetch_array($resultInvWise))
						{
							$rpt_field		= $rowInv['REPORT_FIELD_ID'];
							$invAmount	   	= $rowInv['invoiceAmt'];
							$invoiceDate	= $rowInv['INVOICE_DATE'];
							
							if($tot_to_save>0)
							{
								if($invAmount<=$tot_to_save)
								{
									$saveAmnt 	 	= $invAmount;
									$tot_to_save 	= $tot_to_save-$invAmount;
								}
								else if($invAmount>$tot_to_save)
								{
									$saveAmnt 		= $tot_to_save;
									$tot_to_save	= 0;
								}
								
								$resultArr	= $this->obj_cf_common_set->insert_to_log($location,$rpt_field,$rcv_date,$invoiceDate,$entryDate,$paymentNo,$paymentYear,'SUPPLIER_PAYMENT',$saveAmnt,$currencyId,'RunQuery2');
								if($resultArr['type']=='fail' && $savedStatus)
								{
									$savedStatus	= false;
									$savedMasseged	= $resultArr['msg'];
									$error_sql		= $resultArr['sql'];
								}
							}		
						}	
					}
				}
			//-------------------------------------------------------------------------------------------------
		}
	
		$approval		= $savedLevels+1-$status;
		$this->approved_by_insert($paymentNo,$paymentYear,$session_userId,$approval);
	}
	private function SaveTransaction($paymentNo,$paymentYear,$docType,$session_userId)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "INSERT INTO finance_supplier_transaction 
				(
				PO_YEAR, 
				PO_NO, 
				SUPPLIER_ID, 
				CURRENCY_ID, 
				DOCUMENT_YEAR, 
				DOCUMENT_NO, 
				DOCUMENT_TYPE, 
				PURCHASE_INVOICE_YEAR, 
				PURCHASE_INVOICE_NO, 
				LEDGER_ID, 
				VALUE, 
				COMPANY_ID, 
				LOCATION_ID, 
				USER_ID, 
				TRANSACTION_DATE_TIME
				)
				(
				SELECT
				GH.intPoYear, 
				GH.intPoNo,				
				SPIH.SUPPLIER_ID,
				SPIH.CURRENCY_ID,
				SPD.PAYMENT_YEAR,
				SPD.PAYMENT_NO,
				'$docType',
				SPD.PURCHASE_INVOICE_YEAR,
				SPD.PURCHASE_INVOICE_NO,				
				SPGL.ACCOUNT_ID,
				(SPD.AMOUNT*-1),
				SPH.COMPANY_ID,
				SPH.LOCATION_ID,
				$session_userId,
				SPH.PAY_DATE
				FROM finance_supplier_payment_details AS SPD
				INNER JOIN finance_supplier_payment_header SPH ON SPH.PAYMENT_NO=SPD.PAYMENT_NO AND
				SPH.PAYMENT_YEAR=SPD.PAYMENT_YEAR
				INNER JOIN finance_supplier_purchaseinvoice_header SPIH ON SPIH.PURCHASE_INVOICE_NO=SPD.PURCHASE_INVOICE_NO AND
				SPIH.PURCHASE_INVOICE_YEAR=SPD.PURCHASE_INVOICE_YEAR
				INNER JOIN ware_grnheader GH 
				  ON GH.strInvoiceNo = SPIH.INVOICE_NO
				INNER JOIN trn_poheader PH
				  ON PH.intPONo = GH.intPoNo
				    AND PH.intPOYear = GH.intPoYear
				    AND PH.intSupplier = SPIH.SUPPLIER_ID
				INNER JOIN finance_supplier_payment_gl SPGL 
				  ON SPGL.PAYMENT_NO = SPD.PAYMENT_NO AND
				SPGL.PAYMENT_YEAR=SPD.PAYMENT_YEAR
				WHERE SPD.PAYMENT_NO = '$paymentNo' AND
				SPD.PAYMENT_YEAR = '$paymentYear'
				) ";
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function SaveSupplierTransaction($paymentNo,$paymentYear,$session_userId,$totPayAmount)
	{
		global $savedStatus;
		global $savedMasseged;
	
		$headerArr 			= $this->getHeaderData($paymentNo,$paymentYear);	
		$supChrtOfAccount 	= $this->getSupChartOfAccount($headerArr['SUPPLIER_ID']);
		if($supChrtOfAccount['type']=='fail' && ($savedStatus))
		{
			$savedStatus = false;
			$savedMasseged	= $supChrtOfAccount['ErrorMsg'];
		}
		else
		{
			$this->SaveFinanceTransaction($headerArr['SUPPLIER_ID'],$supChrtOfAccount,$totPayAmount,$paymentNo,$paymentYear,'PAYMENT','D','SU',$headerArr['CURRENCY_ID'],$headerArr['BANK_REFERENCE_NO'],addslashes($headerArr['REMARKS']),$headerArr['PAY_DATE'],$headerArr['LOCATION_ID'],$headerArr['COMPANY_ID'],$session_userId);
		}
	}
	private function getSupChartOfAccount($supplierId)
	{		
		$sql = "SELECT CHART_OF_ACCOUNT_ID
				FROM finance_mst_chartofaccount
				WHERE CATEGORY_TYPE='S' AND
				CATEGORY_ID='$supplierId' AND
				STATUS='1' ";
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		
		if(mysqli_num_rows($result)>0)
		{
			return $row["CHART_OF_ACCOUNT_ID"];
		}
		else
		{
			$supplierName			= $this->GetPublicValues('strName','mst_supplier','intId',$supplierId);
			$response['type']		= 'fail';
			$response['ErrorMsg']	= "No GL Account available for Supplier : $supplierName";
			return $response;
		}
	}
	private function GetPublicValues($select_fieldName,$tableName,$where_fieldId,$where_fieldValue)
	{
		$sql = "SELECT $select_fieldName AS VALUE FROM $tableName WHERE $where_fieldId = '$where_fieldValue'";
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		return $row["VALUE"];
	}
	private function SaveFinanceTransaction($supplierId,$ChrtOfAccount,$totPayAmount,$paymentNo,$paymentYear,$docType,$transacType,$transacCat,$currency,$bankRefNo,$remarks,$date,$location,$company,$session_userId)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "INSERT INTO finance_transaction 
				(
				CHART_OF_ACCOUNT_ID, 
				AMOUNT, 
				DOCUMENT_NO, 
				DOCUMENT_YEAR, 
				DOCUMENT_TYPE, 
				TRANSACTION_TYPE, 
				TRANSACTION_CATEGORY, 
				TRANSACTION_CATEGORY_ID, 
				CURRENCY_ID, 
				BANK_REFERENCE_NO, 
				REMARKS, 
				LOCATION_ID, 
				COMPANY_ID, 
				LAST_MODIFIED_BY, 
				LAST_MODIFIED_DATE
				)
				VALUES
				( 
				'$ChrtOfAccount', 
				'$totPayAmount', 
				'$paymentNo', 
				'$paymentYear', 
				'$docType', 
				'$transacType', 
				'$transacCat', 
				'$supplierId', 
				'$currency', 
				'$bankRefNo', 
				'$remarks', 
				'$location', 
				'$company', 
				'$session_userId', 
				'$date'
				)";
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function SaveBankTransaction($paymentNo,$paymentYear,$session_userId,$totPayAmount)
	{
		global $session_companyId;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$headerArr 		= $this->getHeaderData($paymentNo,$paymentYear);
		$bankArray		= $this->finance_common_get->GetBankExRate($headerArr['ACCOUNT_ID'],'RunQuery2');
		if($headerArr['CURRENCY_ID'] != $bankArray["CURRENCY_ID"])
		{
			$rateTarget		= $this->obj_exchange_rate_get->GetAllBankValues($bankArray["CURRENCY_ID"],$bankArray["BANK_ID"],2,$headerArr['PAY_DATE'],$session_companyId,'RunQuery2');
			$rateSource		= $this->obj_exchange_rate_get->GetAllBankValues($headerArr['CURRENCY_ID'],$bankArray["BANK_ID"],2,$headerArr['PAY_DATE'],$session_companyId,'RunQuery2');
			$currency		= $bankArray["CURRENCY_ID"];
			$totPayAmount	= round((($totPayAmount/$rateTarget["AVERAGE_RATE"])*$rateSource["AVERAGE_RATE"]),2);
			if($totPayAmount=='0')
			{
				$savedStatus	= false;
				$savedMasseged	= "Unable to confirm.Bank exchange rate not available in the system.";
				$error_sql		= $sql;
			}
		}
		else
		{
			$currency		= $headerArr['CURRENCY_ID'];
		}
		
		$this->SaveFinanceTransaction($headerArr['SUPPLIER_ID'],$headerArr['ACCOUNT_ID'],$totPayAmount,$paymentNo,$paymentYear,'PAYMENT','C','SU',$currency,$headerArr['BANK_REFERENCE_NO'],addslashes($headerArr['REMARKS']),$headerArr['PAY_DATE'],$headerArr['LOCATION_ID'],$headerArr['COMPANY_ID'],$session_userId);
	}
	private function confirmPaymnet_sql($paymentNo,$paymentYear)
	{
		$sql	= " UPDATE finance_supplier_payment_header 
					SET
					VOUCHER_CONFIRMATION = '1'
					WHERE
					PAYMENT_NO = '$paymentNo' AND 
					PAYMENT_YEAR = '$paymentYear' ";
					
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function getDetailData($paymentNo,$paymentYear)
	{
		$sql = "SELECT
				SPH.COMPANY_ID,
				CFCP.REPORT_FIELD_ID, 
				SPH.CURRENCY_ID, 
				SPH.LOCATION_ID, 
				SPID.ITEM_ID ,  
				SPD.AMOUNT, 
				SPH.PAY_DATE,
				SPD.PURCHASE_INVOICE_NO,
				SPD.PURCHASE_INVOICE_YEAR 
				
				FROM
				finance_supplier_payment_header SPH
				INNER JOIN finance_supplier_payment_details SPD ON SPH.PAYMENT_NO = SPD.PAYMENT_NO AND 
				SPH.PAYMENT_YEAR = SPD.PAYMENT_YEAR
				INNER JOIN finance_supplier_purchaseinvoice_details SPID ON SPID.PURCHASE_INVOICE_NO=SPD.PURCHASE_INVOICE_NO
				AND SPID.PURCHASE_INVOICE_YEAR=SPD.PURCHASE_INVOICE_YEAR
				INNER JOIN finance_cashflow_cpanal CFCP ON FIND_IN_SET(SPID.ITEM_ID,CFCP.PO_ITEM_ID)
				WHERE
				CFCP.BOO_PO_RAISED = 1 AND 
				SPH.STATUS = 1 AND
				SPH.PAYMENT_NO = '$paymentNo' AND 
				SPH.PAYMENT_YEAR = '$paymentYear'
				GROUP BY SPD.PURCHASE_INVOICE_YEAR,SPD.PURCHASE_INVOICE_NO ";
		
		$result = $this->db->RunQuery2($sql);
		return $result;
	}
	private function getInvoicedDetails($purInvNo,$purInvYear)
	{
		$sql = "SELECT
				CFCP.REPORT_FIELD_ID, 
				ROUND((((SPID.QTY*SPID.UNIT_PRICE*(100-IFNULL(SPID.DISCOUNT,0)))/100)+SPID.TAX_AMOUNT),2) AS invoiceAmt	,
				SPIH.PURCHASE_DATE as INVOICE_DATE		
				FROM
				finance_supplier_purchaseinvoice_header SPIH
				INNER JOIN finance_supplier_purchaseinvoice_details SPID ON SPIH.PURCHASE_INVOICE_NO = SPID.PURCHASE_INVOICE_NO AND 
				SPIH.PURCHASE_INVOICE_YEAR = SPID.PURCHASE_INVOICE_YEAR
				INNER JOIN finance_cashflow_cpanal CFCP ON FIND_IN_SET(SPID.ITEM_ID,CFCP.PO_ITEM_ID)
				INNER JOIN ware_grnheader GH ON GH.strInvoiceNo=SPIH.INVOICE_NO
				INNER JOIN trn_poheader PO ON PO.intPONo=GH.intPoNo AND PO.intPOYear=GH.intPoYear
				INNER JOIN mst_financepaymentsterms FPT ON FPT.intId=PO.intPaymentTerm
				WHERE
				CFCP.BOO_PO_RAISED = 1 AND 
				SPID.PURCHASE_INVOICE_NO = '$purInvNo' AND
				SPID.PURCHASE_INVOICE_YEAR = '$purInvYear' ";
		
		$result = $this->db->RunQuery2($sql);
		return $result;
	}
	
	private function deleteSupplierTransaction($paymentNo,$paymentYear)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql	= " DELETE
					FROM finance_supplier_transaction
					WHERE DOCUMENT_YEAR = '$paymentYear'
						AND DOCUMENT_NO = '$paymentNo'
						AND DOCUMENT_TYPE = 'PAYMENT'";		
		$result = $this->db->RunQuery2($sql);
		if(!$result && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged	= $this->db->errormsg;
			$error_sql		= $sql;
		}
	}

	private function deleteFinanceTransaction($paymentNo,$paymentYear)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql	= " DELETE
					FROM finance_transaction
					WHERE DOCUMENT_YEAR = '$paymentYear'
						AND DOCUMENT_NO = '$paymentNo'
						AND DOCUMENT_TYPE = 'PAYMENT'
						AND TRANSACTION_CATEGORY = 'SU'";					
		$result = $this->db->RunQuery2($sql);
		if(!$result && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged	= $this->db->errormsg;
			$error_sql		= $sql;
		}
	}
}
?>
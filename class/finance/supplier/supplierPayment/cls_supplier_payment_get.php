<?php
//ini_set('display_errors',1);

$session_userId		= $_SESSION["userId"];
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];

class Cls_Supplier_Payment_Get 
{
	private $db;
	private $obj_errorHandeling;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	public function getHeaderData_pdf($paymentNo,$paymentYear,$executionType)
	{
		return $this->HeaderData_pdf($paymentNo,$paymentYear,$executionType);
	}
	public function getDetailData_pdf($paymentNo,$paymentYear,$executionType)
	{
		return $this->DetailData_pdf($paymentNo,$paymentYear,$executionType);
	}
	public function validateBeforeSave($paymentNo,$paymentYear,$userId,$programCode)
	{
		global $obj_errorHandeling;
		
		$header_arr	= $this->getHeaderDataNew($paymentNo,$paymentYear);
		$response 	= $obj_errorHandeling->get_permision_withApproval_save($header_arr['STATUS'],$header_arr['APPROVE_LEVELS'],$userId,$programCode,'RunQuery2');
		
		return $response;
	}
	public function validateBeforeApprove($paymentNo,$paymentYear,$programCode)
	{
		global $obj_errorHandeling;
		global $session_userId;
		
		$header_arr	= $this->getHeaderData($paymentNo,$paymentYear);
		$response 	= $obj_errorHandeling->get_permision_withApproval_confirm($header_arr['STATUS'],$header_arr['APPROVE_LEVELS'],$session_userId,$programCode,'RunQuery');
		
		return $response;
	}
	public function validateBeforeReject($paymentNo,$paymentYear,$programCode)
	{
		global $obj_errorHandeling;
		global $session_userId;
		
		$header_arr	= $this->getHeaderData($paymentNo,$paymentYear);
		$response 	= $obj_errorHandeling->get_permision_withApproval_reject($header_arr['STATUS'],$header_arr['APPROVE_LEVELS'],$session_userId,$programCode,'RunQuery');
		
		return $response;
	}
	
	public function validateBeforeCancel($paymentNo,$paymentYear,$programCode)
	{
		global $obj_errorHandeling;
		global $session_userId;
		
		$header_arr	= $this->getHeaderData($paymentNo,$paymentYear);
		$response 	= $obj_errorHandeling->get_permision_withApproval_cancel($header_arr['STATUS'],$header_arr['APPROVE_LEVELS'],$session_userId,$programCode,'RunQuery');
		
		if($response["type"]=='pass')
			$response	= $this->validateMonthEndStatus($header_arr['PAY_DATE'],$header_arr['COMPANY_ID']);
		return $response;
	}
	
 	private function HeaderData_pdf($paymentNo,$paymentYear,$executionType)
	{
		global $companyId;
		
		$sql = "SELECT PH.CURRENCY_ID,
				FC.strCode AS currency,
				PGLH.COST_CENTER,
				MFD.strCostCenterCode,
				COA.CHART_OF_ACCOUNT_NAME,
				DATE(PH.PAY_DATE) AS paymentDate,
				(SELECT CONCAT(RIGHT(YEAR(DATE(dtmStartingDate)),2),'-',RIGHT(YEAR(DATE(dtmClosingDate)),2))
				FROM mst_financeaccountingperiod
				INNER JOIN mst_financeaccountingperiod_companies ON mst_financeaccountingperiod.intId=mst_financeaccountingperiod_companies.intPeriodId
				WHERE dtmStartingDate<=DATE(PH.PAY_DATE) AND
				dtmClosingDate>=DATE(PH.PAY_DATE) AND
				mst_financeaccountingperiod_companies.intCompanyId='$companyId') AS voucherYear,
				LPAD(MONTH(PH.PAY_DATE),2,'0') AS voucherMonth,
				(PH.VOUCHER_NO) as voucherNo,
				MS.strName AS supplier,
				PH.REMARKS,
				PH.STATUS,
				PH.PRINT_STATUS,
				PH.CREATED_BY,
				U.strFullName 																				AS CREATED_BY_NAME,
				ROUND((SELECT SUM(AMOUNT) 
						FROM finance_supplier_payment_details PD 
						WHERE PD.PAYMENT_NO = PH.PAYMENT_NO 
							AND PD.PAYMENT_YEAR = PH.PAYMENT_YEAR),2) 										AS TOTAL_AMOUNT,
				PH.COMPANY_ID AS COMPANY_ID,
				PH.BANK_REFERENCE_NO									AS CHEQUE_NO
				FROM finance_supplier_payment_header PH
				INNER JOIN finance_supplier_payment_gl PGLH ON PH.PAYMENT_NO=PGLH.PAYMENT_NO AND PH.PAYMENT_YEAR=PGLH.PAYMENT_YEAR
				INNER JOIN mst_financecurrency FC ON PH.CURRENCY_ID=FC.intId
				INNER JOIN finance_mst_chartofaccount COA ON PGLH.ACCOUNT_ID=COA.CHART_OF_ACCOUNT_ID
				INNER JOIN mst_supplier MS ON MS.intId=PH.SUPPLIER_ID
				INNER JOIN mst_financedimension MFD ON MFD.intId=PGLH.COST_CENTER
				INNER JOIN sys_users U ON U.intUserId = PH.CREATED_BY
				WHERE PH.PAYMENT_NO='$paymentNo' AND
				PH.PAYMENT_YEAR='$paymentYear' AND
				PH.COMPANY_ID='$companyId' ";
		
		$result 	= $this->db->$executionType($sql);
 		$resultArry = mysqli_fetch_array($result);
	
		return $resultArry;
	}
	private function DetailData_pdf($paymentNo,$paymentYear,$executionType)
	{
		global $companyId;
		
		$sql = "SELECT 
		           PIH.INVOICE_NO as invoiceNo,
				   PD.AMOUNT as amonut,
				   CONCAT(GH.intPoNo,'-',GH.intPoYear) AS PONo,
				   CONCAT(GH.intGrnNo,'-',GH.intGrnYear) AS GRNNo,
				(SELECT
				   ABS(ROUND(COALESCE(sum(VALUE),0),2))
				FROM finance_supplier_transaction T
				WHERE T.PURCHASE_INVOICE_NO = PIH.PURCHASE_INVOICE_NO
				   AND T.PURCHASE_INVOICE_YEAR = PIH.PURCHASE_INVOICE_YEAR
				   AND T.DOCUMENT_TYPE = 'CREDIT') AS CREDIT_AMOUNT,
				(SELECT
				 ABS(ROUND(COALESCE(sum(VALUE),0),2))
				FROM finance_supplier_transaction T
				WHERE T.PURCHASE_INVOICE_NO = PIH.PURCHASE_INVOICE_NO
				   AND T.PURCHASE_INVOICE_YEAR = PIH.PURCHASE_INVOICE_YEAR
				   AND T.DOCUMENT_TYPE = 'DEBIT') AS DEBIT_AMOUNT
	   
				FROM finance_supplier_payment_details PD
				INNER JOIN finance_supplier_purchaseinvoice_header PIH 
				  ON PIH.PURCHASE_INVOICE_NO = PD.PURCHASE_INVOICE_NO 
				    AND PIH.PURCHASE_INVOICE_YEAR = PD.PURCHASE_INVOICE_YEAR
				INNER JOIN ware_grnheader GH 
				  ON GH.strInvoiceNo=PIH.INVOICE_NO
				INNER JOIN trn_poheader PH
				  ON PH.intPONo = GH.intPoNo
				    AND PH.intPOYear = GH.intPoYear
				    AND PH.intSupplier = PIH.SUPPLIER_ID
				WHERE PD.PAYMENT_NO='$paymentNo' AND
					PD.PAYMENT_YEAR='$paymentYear'
					AND GH.intStatus    = 1  ";
		
		$result	= $this->db->$executionType($sql);
		
		return $result;
	}
	private function getHeaderData($paymentNo,$paymentYear)
	{
		$sql = "SELECT STATUS,
				APPROVE_LEVELS,
				SUPPLIER_ID,
				CURRENCY_ID,
				PAY_DATE,
				COMPANY_ID
				FROM finance_supplier_payment_header
				WHERE PAYMENT_NO='$paymentNo' AND
				PAYMENT_YEAR='$paymentYear' ";
		
		$result = $this->db->RunQuery($sql);
		$row 	= mysqli_fetch_array($result);
		
		return $row;
	}
	private function getHeaderDataNew($paymentNo,$paymentYear)
	{
		$sql = "SELECT STATUS,
				APPROVE_LEVELS,
				SUPPLIER_ID,
				CURRENCY_ID,
				PAY_DATE,
				COMPANY_ID
				FROM finance_supplier_payment_header
				WHERE PAYMENT_NO='$paymentNo' AND
				PAYMENT_YEAR='$paymentYear' ";
		
		$result = $this->db->RunQuery2($sql);
		$row 	= mysqli_fetch_array($result);
		
		return $row;
	}
	
	private function validateMonthEndStatus($payDate,$companyId)
	{
		$response['type'] 	=	'pass';
		
		$sql 	= "SELECT
					  PROCESS_STATUS
					FROM finance_month_end_process
					WHERE YEAR = YEAR('$payDate')
					AND MONTH = MONTH('$payDate')
					AND COMPANY_ID = $companyId";
		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		if($row['PROCESS_STATUS']=='1')
		{
			$response['type'] 	=	'fail';
			$response['msg'] 	=	"Sorry! You cannot cancel. Finance month end lock for the payment date : $payDate";				
		}
		return $response;
	}
 }
?>
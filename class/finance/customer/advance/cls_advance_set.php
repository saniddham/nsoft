<?php
include_once("../../../../class/cls_commonFunctions_get.php");
include_once("../../../../class/finance/customer/cls_common_get.php");

$obj_common			= new cls_commonFunctions_get($db);
$obj_common_get		= new Cls_Common_Get($db);

$session_userId		= $_SESSION["userId"];
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];

class Cls_Advance_Set
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	
	public function Save($customerId,$orderNo,$orderYear,$currencyId,$payMethod,$ledgerAccount,$remarks,$invoiceDate,$detail_array,$bankReferenceNo,$invoiceType,$advanceAmount)
	{
		global $booSavedStatus;
		global $savedMasseged;
		global $pub_ErrorSql;
		global $obj_common;
		global $session_locationId;
		global $obj_common_get;
		
		$booSavedStatus	= true;
		$booReadArray	= false;

		$this->db->begin();
		$no_array 		= $obj_common->GetSystemMaxNo('intFin_Customer_AdvanceNo',$session_locationId);
		$advanceNo		= $no_array["max_no"];
		$advanceYear	= date('Y');
		
		$this->SaveHeader($advanceNo,$advanceYear,$customerId,$orderNo,$orderYear,$currencyId,$payMethod,$ledgerAccount,$remarks,$invoiceDate,$bankReferenceNo);
		
		foreach($detail_array as $detail_loop)
		{
			$booReadArray	= true;
			$this->SaveDetails($advanceNo,$advanceYear,$detail_loop["GLAccountNo"],$detail_loop["Amount"],$detail_loop["Remarks"],$detail_loop["CostCenter"]);
			$this->SaveTransaction($advanceNo,$advanceYear,'null','null',$detail_loop["Amount"],$orderNo,$orderYear,$customerId,$currencyId,$detail_loop["GLAccountNo"],$invoiceDate);
			
			if($this->CheckIsBankGL($detail_loop["GLAccountNo"]))
			{
				//BEGIN - UPDATE CUSTOMER RECEIVE AMOUNT IN TO FORECAST TABLE {						
				$response = $obj_common_get->UpdateForecast($detail_loop["Amount"],$currencyId,$invoiceDate,$customerId,$detail_loop["GLAccountNo"]);
				if(!$response["type"])
				{
					$booSavedStatus	= false;
					$savedMasseged 	= $response['msg'];
					$pub_ErrorSql 	= $response['sql'];				
				}
				//END	- UPDATE CUSTOMER RECEIVE AMOUNT IN TO FORECAST TABLE }
			}
		}
		if(!$booReadArray){
			$booSavedStatus	= false;
			$savedMasseged	= "No details available to proceed.";
		}
		
		//
		$this->GLAutomateTransaction($advanceNo,$advanceYear,$customerId,$advanceAmount,$detail_array,$currencyId,$invoiceDate);
		//
		$resultArr	= $obj_common->validateDuplicateSerialNoWithSysNo($advanceNo,'intFin_Customer_AdvanceNo',$session_locationId);
		if($resultArr['type']=='fail' && $booSavedStatus)
		{
			$booSavedStatus	= false;
			$savedMasseged	= $resultArr['msg'];
		}
			
		if($booSavedStatus){
			$this->db->commit();
			$response['type'] 			= "pass";
			$response['msg'] 			= "Advance No : '$advanceNo/$advanceYear' saved successfully.";
			$response['Advance_No'] 	= $advanceNo;
			$response['Advance_Year'] 	= $advanceYear;
		}else{
			$this->db->rollback();
			$response['type'] 			= "fail";
			$response['msg'] 			= $savedMasseged;
			$response['ErrorSql'] 		= $pub_ErrorSql;
		}
		return json_encode($response);
	}
	
	public function Cancel($advanceNo,$advanceYear)
	{
		global $booSavedStatus;
		global $savedMasseged;
		global $pub_ErrorSql;
		global $obj_common_get;
		
		$booSavedStatus	= true;
		
		$this->db->begin();
		
		$this->UpdateCancelStatus($advanceNo,$advanceYear);
		$trans_response = $obj_common_get->RemoveTransaction($advanceNo,$advanceYear,'ADVANCE');
		if($trans_response["type"])
		{
			$savedStatus	= false;
			$savedMasseged	= $trans_response["msg"];
			$pub_ErrorSql	= $trans_response["errorSql"];
		}
		
		$trans_response = $obj_common_get->RemoveFinanceMainTransaction($advanceNo,$advanceYear,'ADVANCE');
		if($trans_response["type"])
		{
			$savedStatus	= false;
			$savedMasseged	= $trans_response["msg"];
			$pub_ErrorSql	= $trans_response["errorSql"];
		}
		
		if($booSavedStatus){
			$this->db->commit();
			$response['type'] 			= "pass";
			$response['msg'] 			= "Canceled successfully.";
		}else{
			$this->db->rollback();
			$response['type'] 			= "fail";
			$response['msg'] 			= $savedMasseged;
			$response['ErrorSql'] 		= $pub_ErrorSql;
		}
		return json_encode($response);
		
	}
	
	public function Settle($advanceNo,$advanceYear,$invoiceNo,$invoiceYear,$orderNo,$orderYear,$gl_id,$currencyId,$customerId,$settleAmount)
	{
		global $booSavedStatus;
		global $savedMasseged;
		global $pub_ErrorSql;
		global $session_locationId;
		global $obj_common;
		
		$transDate 		= date('Y-m-d');
		$booSavedStatus	= true;
		$booReadArray	= false;
		
		$this->db->begin();
		
		$sysNo_arry 	= $obj_common->GetSystemMaxNo('intCustomerAdvSettleNo',$session_locationId);
		if($sysNo_arry["rollBackFlag"]==1)
		{
			if($savedStatus)
			{
				$booSavedStatus	= false;
				$savedMasseged 	= $sysNo_arry["msg"];
				$pub_ErrorSql	= $sysNo_arry["q"];
			}
			
		}
		$settleNo			= $sysNo_arry["max_no"];
		$settleYear			= date('Y');
		
		$this->saveSettleData($settleNo,$settleYear,$advanceNo,$advanceYear,$invoiceNo,$invoiceYear,$orderNo,$orderYear,$currencyId,$gl_id,$customerId,$transDate,$settleAmount);
		$this->UpdateAdvanceBalance($advanceNo,$advanceYear,$gl_id,$settleAmount);
		$this->DecustCustomerTransaction($advanceNo,$advanceYear,$gl_id,$settleAmount);
		$this->SaveTransaction($advanceNo,$advanceYear,$invoiceYear,$invoiceNo,$settleAmount,$orderNo,$orderYear,$customerId,$currencyId,$gl_id,$transDate);
		
		//BEGIN	-
		$this->GLAutomateTransaction_settle($advanceNo,$advanceYear,$invoiceNo,$invoiceYear,$customerId,$settleAmount,$currencyId,$transDate);
		//END	-
		$resultArr	= $obj_common->validateDuplicateSerialNoWithSysNo($settleNo,'intCustomerAdvSettleNo',$session_locationId);
		if($resultArr['type']=='fail' && $booSavedStatus)
		{
			$booSavedStatus	= false;
			$savedMasseged	= $resultArr['msg'];
		}
		
		if($booSavedStatus){
			$this->db->commit();
			$response['type'] 			= "pass";
			$response['msg'] 			= "Updated successfully.";
		}else{
			$this->db->rollback();
			$response['type'] 			= "fail";
			$response['msg'] 			= $savedMasseged;
			$response['ErrorSql'] 		= $pub_ErrorSql;
		}
		return json_encode($response);
	}
	
	public function SaveHeader($advanceNo,$advanceYear,$customerId,$orderNo,$orderYear,$currencyId,$payMethod,$ledgerAccount,$remarks,$invoiceDate,$bankReferenceNo)
	{
		global $booSavedStatus;
		global $savedMasseged;
		global $pub_ErrorSql;
		global $session_userId;
		global $session_companyId;
		global $session_locationId;
		
		$sql = "INSERT INTO finance_customer_advance_header 
				(ADVANCE_NO, 
				ADVANCE_YEAR, 
				ORDER_NO, 
				ORDER_YEAR, 
				CUSTOMER_ID, 
				CURRENCY_ID, 
				PAYMENT_METHOD, 
				LEDGER_ID,
				BANK_REFERENCE_NO,
				REMARKS, 
				ADVANCE_DATE,
				COMPANY_ID,
				LOCATION_ID, 
				CREATED_BY, 
				CREATED_DATE)
				VALUES
				('$advanceNo', 
				'$advanceYear', 
				$orderNo, 
				$orderYear, 
				'$customerId', 
				'$currencyId', 
				'$payMethod', 
				$ledgerAccount,
				'$bankReferenceNo',
				'$remarks', 
				'$invoiceDate', 
				'$session_companyId',
				'$session_locationId',
				'$session_userId', 
				NOW());";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$booSavedStatus	= false;
			$savedMasseged	= $this->db->errormsg;
			$pub_ErrorSql	= $sql;
		}
	}
	
	public function SaveDetails($advanceNo,$advanceYear,$gl,$amount,$remarks,$costCenter)
	{
		global $booSavedStatus;
		global $savedMasseged;
		global $pub_ErrorSql;
		
		$sql = "INSERT INTO finance_customer_advance_details 
				(ADVANCE_NO, 
				ADVANCE_YEAR, 
				ACCOOUNT_NO, 
				AMOUNT,
				SETTLE_BALANCE, 
				REMARKS, 
				COST_CENTER_ID)
				VALUES
				('$advanceNo', 
				'$advanceYear', 
				'$gl', 
				'$amount',
				'$amount',
				'$remarks', 
				'$costCenter');";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$booSavedStatus	= false;
			$savedMasseged	= $this->db->errormsg;
			$pub_ErrorSql	= $sql;
		}
	}
	
	public function SaveTransaction($receiptNo,$receiptYear,$invoiceYear,$invoiceNo,$payAmount,$orderNo,$orderYear,$customerId,$currencyId,$ledgerId,$transDate)
	{
		global $savedMasseged;
		global $booSavedStatus;
		global $session_companyId;
		global $session_locationId;
		global $session_userId;
		global $pub_ErrorSql;
		
		$sql = "INSERT INTO finance_customer_transaction 
				(ORDER_YEAR, 
				ORDER_NO, 
				CUSTOMER_ID, 
				CURRENCY_ID, 
				DOCUMENT_YEAR, 
				DOCUMENT_NO, 
				DOCUMENT_TYPE, 
				INVOICE_YEAR,
				INVOICE_NO,
				LEDGER_ID, 
				VALUE, 
				COMPANY_ID, 
				LOCATION_ID, 
				USER_ID, 
				TRANSACTION_DATE_TIME,
				SYSTEM_MODIFIED_DATE)
				VALUES
				($orderYear, 
				$orderNo, 
				'$customerId', 
				'$currencyId', 
				'$receiptYear', 
				'$receiptNo', 
				'ADVANCE',
				$invoiceYear,
				$invoiceNo,
				'$ledgerId', 
				'-$payAmount', 
				'$session_companyId', 
				'$session_locationId', 
				'$session_userId',
				'$transDate',
				NOW());";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$booSavedStatus	= false;
			$savedMasseged	= $this->db->errormsg;
			$pub_ErrorSql	= $sql;
		}
	}
	
	private function UpdateAdvanceBalance($advanceNo,$advanceYear,$gl_id,$settleAmount)
	{
		global $booSavedStatus;
		global $savedMasseged;
		global $pub_ErrorSql;
		
		$sql = "UPDATE finance_customer_advance_details
					SET SETTLE_BALANCE = SETTLE_BALANCE - $settleAmount
				WHERE ADVANCE_NO = '$advanceNo'
				AND ADVANCE_YEAR = '$advanceYear'
				AND ACCOOUNT_NO = '$gl_id'";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$booSavedStatus	= false;
			$savedMasseged	= $this->db->errormsg;
			$pub_ErrorSql	= $sql;
		}
	}
	
	private function DecustCustomerTransaction($advanceNo,$advanceYear,$gl_id,$settleAmount)
	{
		global $booSavedStatus;
		global $savedMasseged;
		global $pub_ErrorSql;
		
		$sql = "UPDATE finance_customer_transaction SET VALUE = VALUE + $settleAmount
				WHERE DOCUMENT_NO = '$advanceNo'
				AND DOCUMENT_YEAR = '$advanceYear'
				AND DOCUMENT_TYPE = 'ADVANCE'
				AND LEDGER_ID = '$gl_id'
				AND INVOICE_NO IS NULL
				AND INVOICE_YEAR IS NULL";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$booSavedStatus	= false;
			$savedMasseged	= $this->db->errormsg;
			$pub_ErrorSql	= $sql;
		}
	}

	private function UpdateCancelStatus($advanceNo,$advanceYear)
	{
		global $booSavedStatus;
		global $savedMasseged;
		global $pub_ErrorSql;
		
		$sql = "UPDATE 
					finance_customer_advance_header 
				SET STATUS = 10 
				WHERE ADVANCE_NO = '$advanceNo' AND ADVANCE_YEAR = '$advanceYear'";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$booSavedStatus	= false;
			$savedMasseged	= $this->db->errormsg;
			$pub_ErrorSql	= $sql;
		}
	}
	
	private function UpdateCancelTransaction($advanceNo,$advanceYear)
	{
		global $session_userId;
		global $booSavedStatus;
		global $savedMasseged;
		global $pub_ErrorSql;
		
		$sql = "INSERT INTO finance_customer_transaction 
					(ORDER_YEAR, 
					ORDER_NO, 
					CUSTOMER_ID, 
					CURRENCY_ID, 
					DOCUMENT_YEAR, 
					DOCUMENT_NO, 
					DOCUMENT_TYPE, 
					INVOICE_YEAR, 
					INVOICE_NO, 
					LEDGER_ID, 
					VALUE, 
					COMPANY_ID, 
					LOCATION_ID, 
					USER_ID, 
					TRANSACTION_DATE_TIME
					)
				SELECT 	ORDER_YEAR, 
					ORDER_NO, 
					CUSTOMER_ID, 
					CURRENCY_ID, 
					DOCUMENT_YEAR, 
					DOCUMENT_NO, 
					DOCUMENT_TYPE, 
					INVOICE_YEAR, 
					INVOICE_NO, 
					LEDGER_ID, 
					VALUE * -1, 
					COMPANY_ID, 
					LOCATION_ID, 
					'$session_userId', 
					NOW()	 
					FROM 
					finance_customer_transaction 
					WHERE DOCUMENT_YEAR = '$advanceYear' AND DOCUMENT_NO = '$advanceNo' AND DOCUMENT_TYPE = 'ADVANCE'";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$booSavedStatus	= false;
			$savedMasseged	= $this->db->errormsg;
			$pub_ErrorSql	= $sql;
		}
	}
	
	public function GLAutomateTransaction($advanceNo,$advanceYear,$customerId,$advanceAmount,$detail_array,$currencyId,$transDate)
	{
		global $booSavedStatus;
		global $savedMasseged;
		global $pub_ErrorSql;
		global $obj_common_get;
		
		//BEGIN - SAVING BANKING DETAILS {
		foreach($detail_array as $loop)
		{
			$response 	= $obj_common_get->Save_Jurnal_Transaction($loop["GLAccountNo"],$loop["Amount"],$documentNo=$advanceNo,$documentYear=$advanceYear,$documentType='ADVANCE',$serialNo='null',$serialYear='null',$transType='D',$transCategory='CU',$customerId,$currencyId,$loop["Remarks"],$transDate);
			if($response['type']=='fail')
			{
				$booSavedStatus	= false;
				$savedMasseged	= $response['ErrorMsg'];
				$pub_ErrorSql	= $response['ErrorSql'];
				return;
			}
		}
		//END 	- SAVING BANKING DETAILS }
				
		//BEGIN - SAVING CUSTOMER WISE GL {
		$glId		= $obj_common_get->GetCustomerGL($customerId);	
		if($glId['type']=='fail')
		{
			$booSavedStatus	= false;
			$savedMasseged	= $glId['ErrorMsg'];
			return;
		}		
		$response 	= $obj_common_get->Save_Jurnal_Transaction($glId,$amount=$advanceAmount,$documentNo=$advanceNo,$documentYear=$advanceYear,$documentType='ADVANCE',$serialNo='null',$serialYear='null',$transType='C',$transCategory='CU',$customerId,$currencyId,'',$transDate);
		if($response['type']=='fail')
		{
			$booSavedStatus	= false;
			$savedMasseged	= $response['ErrorMsg'];
			$pub_ErrorSql	= $response['ErrorSql'];
			return;
		}
		//END 	- SAVING CUSTOMER WISE GL }
	}
	
	private function GLAutomateTransaction_settle($advanceNo,$advanceYear,$invoiceNo,$invoiceYear,$customerId,$settleAmount,$currencyId,$transDate)
	{
		global $booSavedStatus;
		global $savedMasseged;
		global $pub_ErrorSql;
		global $obj_common_get;
		
		//BEGIN - CUSTOMER WISE CREDIT TRANSACTION {
		$invoiceType 	= $this->GetInvoiceType($customerId);

		if($invoiceType=='2')
			$settleAmount = round(($settleAmount * 112)/100,2);
		else
			$settleAmount	= $settleAmount;
			
		$glId		= $obj_common_get->GetCustomerGL($customerId);		
		if($glId['type']=='fail')
		{
			$savedStatus	= false;
			$savedMasseged	= $glId['ErrorMsg'];
			return;
		}
		$response 	= $obj_common_get->Save_Jurnal_Transaction($glId,$settleAmount,$documentNo=$advanceNo,$documentYear=$advanceYear,$documentType='ADVANCE',$serialNo=$invoiceNo,$serialYear=$invoiceYear,$transType='C',$transCategory='CU',$customerId,$currencyId,'',$transDate);
		if($response['type']=='fail')
		{
			$booSavedStatus	= false;
			$savedMasseged	= $response['ErrorMsg'];
			$pub_ErrorSql	= $response['ErrorSql'];
			return;
		}
		//END 	- CUSTOMER WISE CREDIT TRANSACTION }
		
		//BEGIN - CUSTOMER WISE DEBIT TRANSACTION  {
		$glId		= $obj_common_get->GetCustomerGL($customerId);		
		if($glId['type']=='fail')
		{
			$savedStatus	= false;
			$savedMasseged	= $glId['ErrorMsg'];
			return;
		}
		$response 	= $obj_common_get->Save_Jurnal_Transaction($glId,$settleAmount,$documentNo=$advanceNo,$documentYear=$advanceYear,$documentType='ADVANCE',$serialNo=$invoiceNo,$serialYear=$invoiceYear,$transType='D',$transCategory='CU',$customerId,$currencyId,'',$transDate);
		if($response['type']=='fail')
		{
			$booSavedStatus	= false;
			$savedMasseged	= $response['ErrorMsg'];
			$pub_ErrorSql	= $response['ErrorSql'];
			return;
		}
		//END 	- CUSTOMER WISE DEBIT TRANSACTION  }
	}
	
	private function GetInvoiceType($customerId)
	{
		$sql = "SELECT strInvoiceType FROM mst_customer WHERE intId = $customerId";
		$result = $this->db->RunQuery2($sql);
		$row 	= mysqli_fetch_array($result);
		return $row["strInvoiceType"];
	}
	private function saveSettleData($settleNo,$settleYear,$advanceNo,$advanceYear,$invoiceNo,$invoiceYear,$orderNo,$orderYear,$currencyId,$gl_id,$customerId,$transDate,$settleAmount)
	{
		global $booSavedStatus;
		global $savedMasseged;
		global $pub_ErrorSql;
		global $session_companyId;
		global $session_locationId;
		global $session_userId;
		
		$sql = "INSERT INTO finance_customer_advance_settle_header 
				(
				SETTLE_NO, 
				SETTLE_YEAR, 
				CUSTOMER_ID, 
				CURRENCY_ID, 
				INVOICE_NO, 
				INVOICE_YEAR, 
				ORDER_NO, 
				ORDER_YEAR, 
				ADVANCE_NO, 
				ADVANCE_YEAR, 
				CHAT_OF_ACCOUNT_ID, 
				SETTLE_AMOUNT, 
				SETTLE_DATE, 
				COMPANY_ID, 
				LOCATION_ID, 
				CREATED_BY, 
				CREATED_DATE
				)
				VALUES
				(
				'$settleNo', 
				'$settleYear', 
				'$customerId', 
				'$currencyId', 
				'$invoiceNo', 
				'$invoiceYear', 
				'$orderNo', 
				'$orderYear', 
				'$advanceNo', 
				'$advanceYear', 
				'$gl_id', 
				'$settleAmount', 
				'$transDate', 
				'$session_companyId', 
				'$session_locationId', 
				'$session_userId', 
				 NOW()
				) ";
		$result = $this->db->RunQuery2($sql);
		if(!$result && ($booSavedStatus))
		{
			$booSavedStatus	= false;
			$savedMasseged	= $this->db->errormsg;
			$pub_ErrorSql	= $sql;
		}
	}
	private function CheckIsBankGL($glAccount)
	{
		$sql = "SELECT CHART_OF_ACCOUNT_ID FROM finance_mst_chartofaccount WHERE CHART_OF_ACCOUNT_ID = '$glAccount' AND BANK = 1";
		$result = $this->db->RunQuery2($sql);
		if(mysqli_fetch_row($result)>0)
			return true;
		else
			return false;
	}				
}
?>
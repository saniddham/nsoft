<?php
$session_companyId	= $_SESSION["headCompanyId"];

class Cls_Advance_Get
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
//BEGIN - PUBLIC FUNCTIONS {	
	public function LoadCustomer($ledgerId)
	{
		return $this->LoadCustomer_sql($ledgerId);
	}
	
	public function LoadWhenChangeCustomer($customerId)
	{
		return $this->LoadWhenChangeCustomer_sql($customerId);
	}
	
	public function LoadWhenChangeOrderNo($customerId,$currencyId)
	{
		return $this->LoadWhenChangeOrderNo_sql($customerId,$currencyId);
	}
	
	public function ValidateBeforeSave($orderNo,$orderYear,$receivingAmount)
	{
		return $this->ValidateBeforeSave_sql($orderNo,$orderYear,$receivingAmount);
	}
	
	public function ValidateBeforeCancel($advanceNo,$advanceYear)
	{
		return $this->ValidateBeforeCancel_sql($advanceNo,$advanceYear);
	}
	public function getAdvanceReportHeader($serialNo,$serialYear)
	{
		return $this->getAdvanceReportHeader_sql($serialNo,$serialYear);
	}
	public function getAdvanceReportDetails($serialNo,$serialYear)
	{
		return $this->getAdvanceReportDetails_sql($serialNo,$serialYear);
	}
	public function UpdateStatus($serialNo,$serialYear)
	{
		$this->UpdateStatus_sql($serialNo,$serialYear);
	}
//END	- PUBLIC FUNCTIONS }

//BEGIN - PRIVATE FUNCTIONS {
	private function LoadCustomer_sql($ledgerId)
	{
		global $session_companyId;
		
		if($ledgerId != "")
			$wherSql .= "AND FCA.intChartOfAccountId = '$ledgerId' ";
			
		$sql = "SELECT DISTINCT 
				C.intId AS ID, C.strName AS NAME
				FROM trn_orderheader OH
				INNER JOIN mst_customer C ON C.intId = OH.intCustomer
				INNER JOIN mst_financecustomeractivate FCA ON C.intId = FCA.intCustomerId
				WHERE OH.intStatus = 1
					AND FCA.intCompanyId = $session_companyId
					$wherSql
				ORDER BY C.strName";
		$result = $this->db->RunQuery($sql);
			$string = "<option value=\"".""."\">&nbsp;</option>";
		while($row = mysqli_fetch_array($result))
		{
			$string .= "<option value=\"".$row["ID"]."\">".$row["NAME"]."</option>";
		}
		$response['Details'] = $string;
		return json_encode($response);
	}
	
	private function LoadWhenChangeOrderNo_sql($customerId,$currencyId)
	{
		global $session_companyId;
		
		$sql = "SELECT
					OH.intCurrency         										AS CURRENCY_ID,
					C.intPaymentsMethodsId 										AS PAY_METHOD,
				  	ROUND(SUM(intQty * dblPrice),2)								AS ORDER_AMOUNT,
				  
					ROUND(COALESCE((SELECT
					SUM(SUB_CT.VALUE)
					FROM finance_customer_transaction SUB_CT 
					WHERE SUB_CT.CUSTOMER_ID = OH.intCustomer
					AND SUB_CT.CURRENCY_ID = OH.intCurrency
					AND SUB_CT.DOCUMENT_TYPE = 'ADVANCE'
					AND SUB_CT.COMPANY_ID = $session_companyId),0),2) 				AS ADVANCED_AMOUNT,
					
					COALESCE((SELECT
					SUM(SUB_CT.VALUE)
					FROM finance_customer_transaction SUB_CT 
					WHERE SUB_CT.CUSTOMER_ID = OH.intCustomer
					AND SUB_CT.CURRENCY_ID = OH.intCurrency
					AND SUB_CT.DOCUMENT_TYPE = 'CREDIT'
					AND SUB_CT.COMPANY_ID = $session_companyId),0) 					AS CREDIT_AMOUNT,
					
					COALESCE((SELECT
					SUM(SUB_CT.VALUE)
					FROM finance_customer_transaction SUB_CT 
					WHERE SUB_CT.CUSTOMER_ID = OH.intCustomer
					AND SUB_CT.CURRENCY_ID = OH.intCurrency
					AND SUB_CT.DOCUMENT_TYPE = 'DEBIT'
					AND SUB_CT.COMPANY_ID = $session_companyId),0) 						AS DEBIT_AMOUNT,
					
					ROUND(COALESCE((SELECT
					SUM(SUB_CT.VALUE)
					FROM finance_customer_transaction SUB_CT 
					WHERE SUB_CT.CUSTOMER_ID = OH.intCustomer
					AND SUB_CT.CURRENCY_ID = OH.intCurrency
					AND SUB_CT.DOCUMENT_TYPE = 'PAYRECEIVE'
					AND SUB_CT.COMPANY_ID = $session_companyId),0),2) 				AS PAID_AMOUNT
				  
				FROM trn_orderheader OH
				  INNER JOIN trn_orderdetails OD ON OD.intOrderNo = OH.intOrderNo AND OD.intOrderYear = OH.intOrderYear
				  INNER JOIN mst_customer C ON C.intId = OH.intCustomer
				  INNER JOIN mst_locations LO ON LO.intId = OH.intLocationId
				WHERE OH.intCustomer = '$customerId'
					AND OH.intCurrency = '$currencyId'
					AND LO.intCompanyId = $session_companyId
				GROUP BY OH.intCustomer,OH.intCurrency";
		$result = $this->db->RunQuery($sql);
		while($row = mysqli_fetch_array($result))
		{
			$response["CURRENCY_ID"]		= $row["CURRENCY_ID"];
			$response["PAY_METHOD"]			= $row["PAY_METHOD"];
			$response["LEDGER_HTML"]		= "<option value=\"".$row["LEDGER_ID"]."\">".$row["LEDGER_NAME"]."</option>";
			$response["ORDER_AMOUNT"]		= $row["ORDER_AMOUNT"];
			$response["ADVANCED_AMOUNT"]	= abs($row["ADVANCED_AMOUNT"]);
			$response["CREDIT_AMOUNT"]		= abs($row["CREDIT_AMOUNT"]);
			$response["DEBIT_AMOUNT"]		= abs($row["DEBIT_AMOUNT"]);
			$response["PAID_AMOUNT"]		= abs($row["PAID_AMOUNT"]);
			
			$response["BALANCE_TO_RECEIVE"]	= $response["ORDER_AMOUNT"] - (abs($row["ADVANCED_AMOUNT"]) + abs($row["CREDIT_AMOUNT"]) + abs($row["PAID_AMOUNT"]));
		}
		return json_encode($response);
	}
	
	private function ValidateBeforeSave_sql($orderNo,$orderYear,$receivingAmount)
	{
		$sql = "SELECT
					ROUND(COALESCE((SELECT SUM((intQty * dblPrice))
					FROM trn_orderdetails SUB_OD
					WHERE SUB_OD.intOrderNo = OH.intOrderNo
					AND SUB_OD.intOrderYear = OH.intOrderYear),0),2)			AS ORDER_AMOUNT,
				  
					ROUND(COALESCE((SELECT
					SUM(SUB_CT.VALUE)
					FROM finance_customer_transaction SUB_CT 
					WHERE SUB_CT.ORDER_YEAR = OH.intOrderYear
					AND SUB_CT.ORDER_NO = OH.intOrderNo
					AND SUB_CT.DOCUMENT_TYPE = 'ADVANCE'),0),2) 				AS ADVANCED_AMOUNT,
					
					COALESCE((SELECT
					SUM(SUB_CT.VALUE)
					FROM finance_customer_transaction SUB_CT 
					WHERE SUB_CT.ORDER_YEAR = OH.intOrderYear
					AND SUB_CT.ORDER_NO = OH.intOrderNo
					AND SUB_CT.DOCUMENT_TYPE = 'CREDIT'),0) 					AS CREDIT_AMOUNT,
					
					COALESCE((SELECT
					SUM(SUB_CT.VALUE)
					FROM finance_customer_transaction SUB_CT 
					WHERE SUB_CT.ORDER_YEAR = OH.intOrderYear
					AND SUB_CT.ORDER_NO = OH.intOrderNo
					AND SUB_CT.DOCUMENT_TYPE = 'DEBIT'),0) 						AS DEBIT_AMOUNT,
					
					ROUND(COALESCE((SELECT
					SUM(SUB_CT.VALUE)
					FROM finance_customer_transaction SUB_CT 
					WHERE SUB_CT.ORDER_YEAR = OH.intOrderYear
					AND SUB_CT.ORDER_NO = OH.intOrderNo
					AND SUB_CT.DOCUMENT_TYPE = 'PAYRECEIVE'),0),2) 				AS PAID_AMOUNT
				  
				FROM trn_orderheader OH
				  INNER JOIN mst_customer C ON C.intId = OH.intCustomer
				WHERE OH.intOrderNo = '$orderNo'
					AND intOrderYear = '$orderYear'
				GROUP BY OH.intOrderNo,OH.intOrderYear";
		$result = $this->db->RunQuery($sql);
		while($row = mysqli_fetch_array($result))
		{
			$balance	= $row["ORDER_AMOUNT"] - (abs($row["ADVANCED_AMOUNT"]) + abs($row["CREDIT_AMOUNT"]) + abs($row["PAID_AMOUNT"]));
			
			if($balance < $receivingAmount)
			{
				$response["type"]		= 'false';
				$response["msg"] 		= "Unable to proceed. You are exceeding the 'Receiving Balance Amount'"; 
			}
		}
		return $response;
	}
	
	private function ValidateBeforeCancel_sql($advanceNo,$advanceYear)
	{
		$status	= $this->CheckStatus($advanceNo,$advanceYear);
		if($status=='10')
		{
			$response["type"]		= 'false';
			$response["msg"] 		= "Advance No : $advanceNo/$advanceYear already canceled."; 
			return $response;
		}
	}
	
	private function CheckStatus($advanceNo,$advanceYear)
	{
		$sql = "SELECT STATUS FROM finance_customer_advance_header WHERE ADVANCE_NO = '$advanceNo' AND ADVANCE_YEAR = '$advanceYear'";
		$result = $this->db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return $row["STATUS"];
	}
	
	private function LoadWhenChangeCustomer_sql($customerId)
	{
		global $session_companyId;

		$sql = "SELECT DISTINCT 
					C.intId 			AS ID, 
					C.strCode 			AS NAME,
					CU.strInvoiceType	AS INVOICE_TYPE
				FROM trn_orderheader OH
				INNER JOIN mst_financecurrency C ON C.intId = OH.intCurrency
				INNER JOIN mst_locations L ON L.intId = OH.intLocationId
				INNER JOIN mst_customer CU ON CU.intId = OH.intCustomer
				WHERE OH.intStatus = 1
					AND OH.intCustomer = '$customerId'
					AND L.intCompanyId = $session_companyId
				ORDER BY C.strCode";
		$result = $this->db->RunQuery($sql);
		$row_count = mysqli_num_rows($result);
		if($row_count>1)
			$string = "<option value=\"".""."\">&nbsp;</option>";
		while($row = mysqli_fetch_array($result))
		{
			$string 	   .= "<option value=\"".$row["ID"]."\">".$row["NAME"]."</option>";
			$invoiceType	= $row["INVOICE_TYPE"];
		}
		$response['INVOICE_TYPE'] 	= $invoiceType;
		$response['Details'] 		= $string;
		return json_encode($response);
		
	}
	private function getAdvanceReportHeader_sql($serialNo,$serialYear)
	{
		$sql = "SELECT
				CAH.ADVANCE_NO        AS ADVANCE_NO,
				CAH.ADVANCE_YEAR 	AS ADVANCE_YEAR,
				CAH.CUSTOMER_ID       AS CUSTOMER_ID,
				CU.strName            AS CUSTOMER_NAME,
				CU.strAddress         AS ADDRESS,
				CU.strCity            AS CITY,
				CO.strCountryName     AS COUNTRY_NAME,
				CAH.ADVANCE_DATE      AS ADVANCE_DATE,
				CAH.CURRENCY_ID       AS CURRENCY_ID,	
				C.strDescription      AS CURRENCY_NAME,
				PM.strName            AS PAYMENT_METHOD,
				CAH.BANK_REFERENCE_NO AS BANK_REFERENCE_NO,
				CAH.REMARKS           AS REMARKS,
				CAH.COMPANY_ID        AS COMPANY_ID,
				CAH.LOCATION_ID       AS LOCATION_ID,
				CAH.STATUS            AS STATUS,
				CAH.PRINT_STATUS	  AS PRINT_STATUS,
				U.strUserName         AS CREATED_BY_NAME
				FROM finance_customer_advance_header CAH
				INNER JOIN mst_customer CU ON CU.intId = CAH.CUSTOMER_ID
				INNER JOIN mst_financecurrency C ON C.intId = CAH.CURRENCY_ID
				LEFT JOIN finance_mst_chartofaccount COA ON COA.CHART_OF_ACCOUNT_ID = CAH.LEDGER_ID
				INNER JOIN mst_financepaymentsmethods PM ON PM.intId = CAH.PAYMENT_METHOD
				INNER JOIN sys_users U ON U.intUserId = CAH.CREATED_BY
				LEFT JOIN mst_country CO ON CO.intCountryID = CU.intCountryId
				WHERE CAH.ADVANCE_NO = '$serialNo'
				AND CAH.ADVANCE_YEAR = '$serialYear' ";
		
		$result = $this->db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return $row;
	}
	private function getAdvanceReportDetails_sql($serialNo,$serialYear)
	{
		$sql = "SELECT
				FCOA.CHART_OF_ACCOUNT_NAME AS ACCOUNT_NAME,
				CAD.AMOUNT  AS AMOUNT
				FROM finance_customer_advance_header CAH
				INNER JOIN finance_customer_advance_details CAD ON CAD.ADVANCE_NO = CAH.ADVANCE_NO
				AND CAD.ADVANCE_YEAR = CAH.ADVANCE_YEAR
				INNER JOIN finance_mst_chartofaccount FCOA ON FCOA.CHART_OF_ACCOUNT_ID = CAD.ACCOOUNT_NO
				WHERE CAH.ADVANCE_NO = $serialNo
				AND CAH.ADVANCE_YEAR = $serialYear ";
		
		$result = $this->db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return $row;
	}
	private function UpdateStatus_sql($serialNo,$serialYear)
	{
		$sql = "UPDATE finance_customer_advance_header 
				SET
				PRINT_STATUS = '1'
				WHERE
				ADVANCE_NO = '$serialNo' AND 
				ADVANCE_YEAR = '$serialYear' ";
		$this->db->RunQuery($sql);
	}
//END 	- PRIVATE FUNCTIONS }
}
?>
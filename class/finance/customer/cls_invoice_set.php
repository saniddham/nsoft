<?php
session_start();
require_once $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
require_once $_SESSION['ROOT_PATH']."class/finance/customer/cls_common_get.php";

$obj_common			= new cls_commonFunctions_get($db);
$obj_common_get		= new Cls_Common_Get($db);
$session_userId		= $_SESSION["userId"];
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];

class Cls_Invoice_Set
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	
	public function Save($customerPoNo,$orderNo,$orderYear,$customerId,$customerLocation,$invoicedDate,$remarks,$complete,$grid,$currencyId,$GLGrid,$invoiceType,$subTotal,$taxTotal,$grandTotal,$bankId)
	{
		global $obj_common;
		global $obj_common_get;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_locationId;
		
		$savedStatus	= true;
		
		$this->db->begin();
	
		if(!$this->ValidateWithSessionCompany($orderNo,$orderYear) && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged	= 'Please logging with correct company location.';
		}

		$invoice_array 	= $obj_common->GetSystemMaxNo('intFin_SalesInvoiceNo',$session_locationId);
		$serialNo		= $invoice_array["max_no"];
		$serialYear		= date('Y');
		$invoiceNo		= $this->CreateinvoiceFormat($serialNo,$serialYear);
		
		$this->SaveHeader($serialNo,$serialYear,$orderNo,$orderYear,$invoiceNo,$customerPoNo,$customerId,$invoicedDate,$remarks,$currencyId,$customerLocation,$bankId,$invoiceType);
		
		foreach($grid as $array_loop)
		{
			
			$salesOrderId		= $array_loop["SalesOrderId"];
			$dispatchQty		= $array_loop["Dispatch"];
			$PDQty				= $array_loop["PDQty"];
			$FDQty				= $array_loop["FDQty"];
			$sampleQty			= $array_loop["Sample"];
			$missingPanalQty	= $array_loop["MissingPanal"];
			$otherQty			= $array_loop["Other"];
			$invoiceQty			= $array_loop["Invoice"];
			$Percentage			= $array_loop["Percentage"];
			$price				= $array_loop["Price"];
			$costCenter			= $array_loop["CostCenter"];
			$tax				= $array_loop["Tax"];
			$taxValue			= $array_loop["TaxValue"];
			$gl_id				= $array_loop["GL_ID"];
			$value				= $invoiceQty * $price;
			
			$this->SaveDetails($serialNo,$serialYear,$salesOrderId,$invoiceQty,$price,$costCenter,$tax,$taxValue,$PDQty,$FDQty,$sampleQty,$missingPanalQty,$otherQty,$gl_id,$Percentage);
			$this->UpdateBalance($orderNo,$orderYear,$salesOrderId,$dispatchQty,$PDQty,$FDQty,$sampleQty,$missingPanalQty,$otherQty,$invoiceQty,$Percentage);			
		}
/*		$loop				= 0;
		foreach($GLGrid as $array_loop)
		{
			$gl_id				= $array_loop["GL_ID"];
			$gl_amount			= $array_loop["GL_Amount"];
			$loop++;
			$this->SaveGL($serialNo,$serialYear,$gl_id,$gl_amount,$loop);
			$this->SaveTransaction($serialNo,$serialYear,$serialYear,$serialNo,$gl_amount,$orderNo,$orderYear,$customerId,$currencyId,$gl_id);
		}*/
		
		//
		$this->GLAutomateTransaction($serialNo,$serialYear,$invoiceType,$customerId,$subTotal,$taxTotal,$grandTotal,$grid,$orderNo,$orderYear,$currencyId,$invoicedDate);
		//
		
		//BEGIN - INSERT CUSTOMER AMOUNT IN TO FORECAST TABLE {
		$obj_common_get->SaveForecast($grandTotal,$orderNo,$orderYear,$currencyId,$invoicedDate,$bankId);
		//END	- INSERT CUSTOMER AMOUNT IN TO FORECAST TABLE }
		
		if($complete==1)
		{
			$this->orderComplete($orderNo,$orderYear);
		}
		$resultArr	= $obj_common->validateDuplicateSerialNoWithSysNo($serialNo,'intFin_SalesInvoiceNo',$session_locationId);
		if($resultArr['type']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged	= $resultArr['msg'];
		}
		
		if($savedStatus){
			$this->db->commit();
			$response['type'] 		= "pass";
			$response['msg'] 		= "Invoiced No : '$invoiceNo' saved successfully.";
			$response['InvoiceNo'] 	= $invoiceNo;
		}else{
			$this->db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= $savedMasseged;
			$response['sql']		= $error_sql;
		}
		return json_encode($response);
	}
	
	public function Cancel($serialYear,$serialNo)
	{
		global $savedStatus;
		global $savedMasseged;
		global $obj_common_get;
		$savedStatus	= true;
		$this->db->begin();
		
		$this->Cancel_sql($serialYear,$serialNo);
		$trans_response = $obj_common_get->RemoveTransaction($serialNo,$serialYear,'INVOICE');
		if($trans_response["type"])
		{
			$savedStatus	= false;
			$savedMasseged	= $trans_response["msg"];
		}
		
		$trans_response = $obj_common_get->RemoveFinanceMainTransaction($serialNo,$serialYear,'INVOICE');
		if($trans_response["type"])
		{
			$savedStatus	= false;
			$savedMasseged	= $trans_response["msg"];
		}
		
		
		if($savedStatus){
			$this->db->commit();
			$response['type'] 		= "pass";
			$response['msg'] 		= "Canceled successfully.";
		}else{
			$this->db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= $savedMasseged;
		}
		return json_encode($response);
	}
	
	public function UpdateStatus($serialNo,$serialYear)
	{
		$this->UpdateStatus_sql($serialNo,$serialYear);
	}
	
	private function SaveHeader($serialNo,$serialYear,$orderNo,$orderYear,$invoiceNo,$customerPoNo,$customerId,$invoicedDate,$remarks,$currencyId,$customerLocation,$bankId,$invoiceType)
	{
		global $session_userId;
		global $session_locationId;
		global $session_companyId;
		global $savedStatus;
		global $savedMasseged;
		
		$sql = "INSERT INTO finance_customer_invoice_header 
				(SERIAL_NO, 
				SERIAL_YEAR,
				ORDER_NO,
				ORDER_YEAR, 
				INVOICE_NO, 
				CUSTOMER_ID,
				CUSTOMER_LOCATION,
				CURRENCY_ID,
				BANK_ACCOUNT_ID,
				REMARKS, 
				INVOICED_DATE,
				COMPANY_ID,
				LOCATION_ID,
				CREATED_BY, 
				CREATED_DATE,
				INVOICE_TYPE)
				VALUES
				('$serialNo', 
				'$serialYear',
				'$orderNo',
				'$orderYear',
				'$invoiceNo', 
				'$customerId',
				'$customerLocation',
				'$currencyId',
				'$bankId',
				'$remarks', 
				'$invoicedDate',
				'$session_companyId',
				'$session_locationId', 
				'$session_userId', 
				NOW(),
				'$invoiceType');";
		$result = $this->db->RunQuery2($sql);
		if(!$result && $savedStatus){
			$savedStatus	= false;
			$savedMasseged	= $this->db->errormsg;
		}
	}
	
	private function CreateinvoiceFormat($serialNo,$serialYear)
	{
		return $serialNo.'-'.$serialYear;
	}
	
	private function SaveDetails($serialNo,$serialYear,$salesOrderId,$invoiceQty,$price,$costCenter,$tax,$taxValue,$PDQty,$FDQty,$sampleQty,$missingPanalQty,$otherQty,$gl_id,$Percentage)
	{
		global $savedStatus;
		global $savedMasseged;
		
		$sql	= "INSERT INTO finance_customer_invoice_details 
					(SERIAL_NO, 
					SERIAL_YEAR, 
					SALES_ORDER_ID,
					GL_ACCOUNT,
					TAX_CODE,
					COST_CENTER,
					PRODUCTION_DAMAGE_QTY,
					FABRIC_DAMAGE_QTY,
					SAMPLE_QTY,
					MISSING_PANAL,
					OTHER,
					QTY, 
					PRICE, 
					VALUE,
					TAX_VALUE,
					DISCOUNT_PERCENTAGE)
					VALUES
					('$serialNo', 
					'$serialYear', 
					'$salesOrderId',
					'$gl_id',
					$tax,
					'$costCenter',
					'$PDQty',
					'$FDQty',
					'$sampleQty',
					'$missingPanalQty',
					'$otherQty',
					'$invoiceQty', 
					'$price'," ;
					if($Percentage==3)
					$sql .= " ($invoiceQty * $price) -  ($invoiceQty * $price *3/100),";
					else
					$sql .= "$invoiceQty * $price,";
					$sql .= "$taxValue,
					$Percentage);";
					//echo $sql;
		$result = $this->db->RunQuery2($sql);
		if(!$result && $savedStatus){
			$savedStatus	= false;
			$savedMasseged	= $this->db->errormsg;
		}
	}
	
	private function UpdateBalance($orderNo,$orderYear,$salesOrderId,$dispatchQty,$PDQty,$FDQty,$sampleQty,$missingPanalQty,$otherQty,$invoiceQty)
	{
		global $savedStatus;
		global $savedMasseged;
		
		$sql = "UPDATE finance_customer_invoice_balance
				SET 
				  PRODUCTION_DAMAGE_QTY = PRODUCTION_DAMAGE_QTY + $PDQty,
				  FABRIC_DAMAGE_QTY		= FABRIC_DAMAGE_QTY + $FDQty,
				  SAMPLE_QTY 			= SAMPLE_QTY + $sampleQty,
				  MISSING_PANAL 		= MISSING_PANAL + $missingPanalQty,
				  OTHER 				= OTHER + $otherQty,
				  TOTAL_DAMAGE			= PRODUCTION_DAMAGE_QTY + FABRIC_DAMAGE_QTY + SAMPLE_QTY + MISSING_PANAL + OTHER,
				  INVOICED_QTY 			= INVOICED_QTY + $invoiceQty,
				  INVOICE_BALANCE 		= DISPATCHED_GOOD_QTY - (INVOICED_QTY + TOTAL_DAMAGE)
				WHERE ORDER_YEAR 		= '$orderYear'
					AND ORDER_NO 		= '$orderNo'
					AND SALES_ORDER_ID 	= '$salesOrderId';";
		$result = $this->db->RunQuery2($sql);
		if(!$result && $savedStatus){
			$savedStatus	= false;
			$savedMasseged	= $this->db->errormsg;
		}
	}
	
	private function Cancel_sql($serialYear,$serialNo)
	{
		global $savedStatus;
		global $savedMasseged;
		
		$sql = "UPDATE finance_customer_invoice_header
				SET STATUS = '10'
				WHERE SERIAL_NO = '$serialNo'
					AND SERIAL_YEAR = '$serialYear';";
		$result = $this->db->RunQuery2($sql);
		if(!$result && $savedStatus){
			$savedStatus	= false;
			$savedMasseged	= $this->db->errormsg;
		}
		
		$sql = "SELECT
				  CIH.ORDER_YEAR,
				  CIH.ORDER_NO,
				  CID.SALES_ORDER_ID,
				  CID.QTY,
				  CID.PRODUCTION_DAMAGE_QTY,
				  CID.FABRIC_DAMAGE_QTY,
				  CID.SAMPLE_QTY,
				  CID.MISSING_PANAL,
				  CID.OTHER
				FROM finance_customer_invoice_details CID
				INNER JOIN finance_customer_invoice_header CIH ON CIH.SERIAL_NO = CID.SERIAL_NO AND CIH.SERIAL_YEAR = CID.SERIAL_YEAR
				WHERE CID.SERIAL_NO = '$serialNo'
				AND CID.SERIAL_YEAR = '$serialYear'";
		$result = $this->db->RunQuery2($sql);
		while($row = mysqli_fetch_array($result))
		{
			$this->Update($row["ORDER_YEAR"],$row["ORDER_NO"],$row["SALES_ORDER_ID"],$row["QTY"],$row["PRODUCTION_DAMAGE_QTY"],$row["FABRIC_DAMAGE_QTY"],$row["SAMPLE_QTY"],$row["MISSING_PANAL"],$row["OTHER"]);
		}		
	}
	
	private function Update($orderYear,$orderNo,$sales_order_id,$qty,$production_damage_qty,$fabric_damage_qty,$sampleQty,$missing_panal,$other)
	{
		global $savedStatus;
		global $savedMasseged;
		$totalDamage	= $production_damage_qty+$fabric_damage_qty+$sampleQty+$missing_panal+$other;
		
		$sql = "UPDATE finance_customer_invoice_balance
				SET 
				  PRODUCTION_DAMAGE_QTY = PRODUCTION_DAMAGE_QTY - $production_damage_qty,
				  FABRIC_DAMAGE_QTY		= FABRIC_DAMAGE_QTY - $fabric_damage_qty,
				  SAMPLE_QTY			= SAMPLE_QTY - $sampleQty,
				  MISSING_PANAL			= MISSING_PANAL - $missing_panal,
				  OTHER					= OTHER - $other,
				  TOTAL_DAMAGE			= TOTAL_DAMAGE - $totalDamage,
				  INVOICED_QTY 			= INVOICED_QTY - $qty,
				  INVOICE_BALANCE 		= DISPATCHED_GOOD_QTY - (INVOICED_QTY + TOTAL_DAMAGE)
				WHERE ORDER_YEAR = '$orderYear'
					AND ORDER_NO = '$orderNo'
					AND SALES_ORDER_ID = '$sales_order_id';";
		$result = $this->db->RunQuery2($sql);
		if(!$result && $savedStatus){
			$savedStatus	= false;
			$savedMasseged	= $this->db->errormsg;
		}
	}
	
	private function SaveTransaction($receiptNo,$receiptYear,$invoiceYear,$invoiceNo,$value,$orderNo,$orderYear,$customerId,$currencyId,$ledgerId,$invoicedDate)
	{
		global $session_userId;
		global $session_locationId;
		global $session_companyId;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "INSERT INTO finance_customer_transaction 
				(ORDER_YEAR, 
				ORDER_NO, 
				CUSTOMER_ID, 
				CURRENCY_ID, 
				DOCUMENT_YEAR, 
				DOCUMENT_NO, 
				DOCUMENT_TYPE, 
				INVOICE_YEAR, 
				INVOICE_NO, 
				LEDGER_ID, 
				VALUE, 
				COMPANY_ID, 
				LOCATION_ID, 
				USER_ID, 
				TRANSACTION_DATE_TIME,
				SYSTEM_MODIFIED_DATE)
				VALUES
				('$orderYear', 
				'$orderNo', 
				'$customerId', 
				'$currencyId', 
				'$receiptYear', 
				'$receiptNo', 
				'INVOICE', 
				'$invoiceYear', 
				'$invoiceNo', 
				'$ledgerId', 
				'$value', 
				'$session_companyId', 
				'$session_locationId', 
				'$session_userId',
				'$invoicedDate',
				NOW());";
		$result = $this->db->RunQuery2($sql);
		if(!$result && $savedStatus){
			$savedStatus	= false;
			$savedMasseged	= $this->db->errormsg;
			$error_sql		= $sql;
		}
	}
	
	private function SaveGL($serialNo,$serialYear,$gl_id,$gl_amount,$loop)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;

		$sql = "INSERT INTO finance_customer_invoice_gl 
				(SERIAL_NO, 
				SERIAL_YEAR, 
				GL_ACCOUNT, 
				GL_AMOUNT,
				ORDER_BY_ID)
				VALUES
				('$serialNo', 
				'$serialYear', 
				'$gl_id', 
				'$gl_amount',
				'$loop');";
		$result = $this->db->RunQuery2($sql);
		if(!$result && $savedStatus){
			$savedStatus	= false;
			$savedMasseged	= $this->db->errormsg;
			$error_sql		= $sql;
		}
	}

    /**
     * @param $serialNo
     * @param $serialYear
     * @param $invoiceType
     * @param $customerId
     * @param $subTotal
     * @param $taxTotal
     * @param $grandTotal
     * @param $grid
     * @param $orderNo
     * @param $orderYear
     * @param $currencyId
     * @param $invoicedDate
     */
    private function GLAutomateTransaction($serialNo, $serialYear, $invoiceType, $customerId, $subTotal, $taxTotal, $grandTotal, $grid, $orderNo, $orderYear, $currencyId, $invoicedDate)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $obj_common_get;
		
		$invTypeStatus	= false;
		
		switch($invoiceType){
			case 1:
				//BEGIN - SAVING CUSTOMER WISE GL {
				$invTypeStatus	= true;
				$glId		= $this->GetCustomerGL($customerId);		
				if($glId['type']=='fail')
				{
					$savedStatus	= false;
					$savedMasseged	= $glId['ErrorMsg'];
					return;
				}		
				$response 	= $obj_common_get->Save_Jurnal_Transaction($glId,$amount=$grandTotal,$documentNo=$serialNo,$documentYear=$serialYear,$documentType='INVOICE',$serialNo,$serialYear,$transType='D',$transCategory='CU',$customerId,$currencyId,'',$invoicedDate);
				if($response['type']=='fail' && $savedStatus)
				{
					$savedStatus	= false;
					$savedMasseged	= $response['ErrorMsg'];
					$error_sql		= $response['ErrorSql'];
					return;
				}
				//END 	- SAVING CUSTOMER WISE GL }
				
				//BEGIN - SAVING INVOICE TYPE GL {
				$glId		= $this->GetInvoiceTypeGL($invoiceType);
				if($glId['type']=='fail' && $savedStatus)
				{
					$savedStatus	= false;
					$savedMasseged	= $glId['ErrorMsg'];
					return;
				}				
				$response 	= $obj_common_get->Save_Jurnal_Transaction($glId,$amount=$grandTotal,$documentNo=$serialNo,$documentYear=$serialYear,$documentType='INVOICE',$serialNo,$serialYear,$transType='C',$transCategory='CU',$customerId,$currencyId,'',$invoicedDate);
				$this->SaveTransaction($serialNo,$serialYear,$serialYear,$serialNo,$grandTotal,$orderNo,$orderYear,$customerId,$currencyId,$glId,$invoicedDate);
				if($response['type']=='fail' && $savedStatus)
				{
					$savedStatus	= false;
					$savedMasseged	= $response['ErrorMsg'];
					$error_sql		= $response['ErrorSql'];
					return;
				}
				//END - SAVING INVOICE TYPE GL }
				break;

            case 5:
                //BEGIN - SAVING CUSTOMER WISE GL {
                $invTypeStatus	= true;
                $glId		= $this->GetCustomerGL($customerId);
                if($glId['type']=='fail')
                {
                    $savedStatus	= false;
                    $savedMasseged	= $glId['ErrorMsg'];
                    return;
                }
                $response 	= $obj_common_get->Save_Jurnal_Transaction($glId,$amount=$grandTotal,$documentNo=$serialNo,$documentYear=$serialYear,$documentType='INVOICE',$serialNo,$serialYear,$transType='D',$transCategory='CU',$customerId,$currencyId,'',$invoicedDate);
                if($response['type']=='fail' && $savedStatus)
                {
                    $savedStatus	= false;
                    $savedMasseged	= $response['ErrorMsg'];
                    $error_sql		= $response['ErrorSql'];
                    return;
                }
                //END 	- SAVING CUSTOMER WISE GL }

                //BEGIN - SAVING INVOICE TYPE GL {
                $glId		= $this->GetInvoiceTypeGL($invoiceType);
                if($glId['type']=='fail' && $savedStatus)
                {
                    $savedStatus	= false;
                    $savedMasseged	= $glId['ErrorMsg'];
                    return;
                }
                $response 	= $obj_common_get->Save_Jurnal_Transaction($glId,$amount=$grandTotal,$documentNo=$serialNo,$documentYear=$serialYear,$documentType='INVOICE',$serialNo,$serialYear,$transType='C',$transCategory='CU',$customerId,$currencyId,'',$invoicedDate);
                $this->SaveTransaction($serialNo,$serialYear,$serialYear,$serialNo,$grandTotal,$orderNo,$orderYear,$customerId,$currencyId,$glId,$invoicedDate);
                if($response['type']=='fail' && $savedStatus)
                {
                    $savedStatus	= false;
                    $savedMasseged	= $response['ErrorMsg'];
                    $error_sql		= $response['ErrorSql'];
                    return;
                }
                //END - SAVING INVOICE TYPE GL }
                break;

			case 2:
				//BEGIN - SAVING CUSTOMER WISE GL {
				$invTypeStatus	= true;
				$glId		= $this->GetCustomerGL($customerId);	
				if($glId['type']=='fail' && $savedStatus)
				{
					$savedStatus	= false;
					$savedMasseged	= $glId['ErrorMsg'];
					return;
				}			
				$response 	= $obj_common_get->Save_Jurnal_Transaction($glId,$amount=$grandTotal,$documentNo=$serialNo,$documentYear=$serialYear,$documentType='INVOICE',$serialNo,$serialYear,$transType='D',$transCategory='CU',$customerId,$currencyId,'',$invoicedDate);
				if($response['type']=='fail' && $savedStatus)
				{
					$savedStatus	= false;
					$savedMasseged	= $response['ErrorMsg'];
					$error_sql		= $response['ErrorSql'];
				}
				//END  - SAVING CUSTOMER WISE GL }
				
				//BEGIN - SAVING TAX GL {
				foreach($grid as $loop)
				{
					$glId		= $this->GetTaxGL($loop["Tax"]);
					if($glId['type']=='fail' && $savedStatus)
					{
						$savedStatus	= false;
						$savedMasseged	= $glId['ErrorMsg'];
						return;
					}
					$response 	= $obj_common_get->Save_Jurnal_Transaction($glId,$amount=$loop["TaxValue"],$documentNo=$serialNo,$documentYear=$serialYear,$documentType='INVOICE',$serialNo,$serialYear,$transType='C',$transCategory='CU',$customerId,$currencyId,'',$invoicedDate);
					$this->SaveTransaction($serialNo,$serialYear,$serialYear,$serialNo,$loop["TaxValue"],$orderNo,$orderYear,$customerId,$currencyId,$glId,$invoicedDate);
					if($response['type']=='fail' && $savedStatus)
					{
						$savedStatus	= false;
						$savedMasseged	= $response['ErrorMsg'];
						$error_sql		= $response['ErrorSql'];
						return;
					}
				}
				//END 	- SAVING TAX GL	}
				
				//BEGIN - SAVING INVOICE TYPE GL {
				$glId		= $this->GetInvoiceTypeGL($invoiceType);
				if($glId['type']=='fail' && $savedStatus)
				{
					$savedStatus	= false;
					$savedMasseged	= $glId['ErrorMsg'];
					return;
				}
				$response 	= $obj_common_get->Save_Jurnal_Transaction($glId,$amount=$subTotal,$documentNo=$serialNo,$documentYear=$serialYear,$documentType='INVOICE',$serialNo,$serialYear,$transType='C',$transCategory='CU',$customerId,$currencyId,'',$invoicedDate);
				$this->SaveTransaction($serialNo,$serialYear,$serialYear,$serialNo,$subTotal,$orderNo,$orderYear,$customerId,$currencyId,$glId,$invoicedDate);
				if($response['type']=='fail' && $savedStatus)
				{
					$savedStatus	= false;
					$savedMasseged	= $response['ErrorMsg'];
					$error_sql		= $response['ErrorSql'];
					return;
				}
				//END 	- SAVING INVOICE TYPE GL }
				break;
				
			case 3:
				//BEGIN - SAVING CUSTOMER WISE GL {
				$invTypeStatus	= true;
				$glId		= $this->GetCustomerGL($customerId);
				if($glId['type']=='fail' && $savedStatus)
				{
					$savedStatus	= false;
					$savedMasseged	= $glId['ErrorMsg'];
					return;
				}				
				$response 	= $obj_common_get->Save_Jurnal_Transaction($glId,$amount=$grandTotal,$documentNo=$serialNo,$documentYear=$serialYear,$documentType='INVOICE',$serialNo,$serialYear,$transType='D',$transCategory='CU',$customerId,$currencyId,'',$invoicedDate);
				if($response['type']=='fail' && $savedStatus)
				{
					$savedStatus	= false;
					$savedMasseged	= $response['ErrorMsg'];
					$error_sql		= $response['ErrorSql'];
					return;
				}
				//END 	- SAVING CUSTOMER WISE GL }
				
				//BEGIN - SAVING INVOICE TYPE GL {
				$glId		= $this->GetInvoiceTypeGL($invoiceType);
				if($glId['type']=='fail' && $savedStatus)
				{
					$savedStatus	= false;
					$savedMasseged	= $glId['ErrorMsg'];
					return;
				}
				
				$response 	= $obj_common_get->Save_Jurnal_Transaction($glId,$amount=$subTotal,$documentNo=$serialNo,$documentYear=$serialYear,$documentType='INVOICE',$serialNo,$serialYear,$transType='C',$transCategory='CU',$customerId,$currencyId,'',$invoicedDate);
				$this->SaveTransaction($serialNo,$serialYear,$serialYear,$serialNo,$subTotal,$orderNo,$orderYear,$customerId,$currencyId,$glId,$invoicedDate);
				if($response['type']=='fail' && $savedStatus)
				{
					$savedStatus	= false;
					$savedMasseged	= $response['ErrorMsg'];
					$error_sql		= $response['ErrorSql'];
					return;
				}
				//END 	- SAVING INVOICE TYPE GL }
				break;
		}
		
		if(!$invTypeStatus && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged	= "Invalid customer Invoice Type.";
		}

	}
	
	private function GetCustomerGL($customerId)
	{
		$sql = "SELECT 
					CHART_OF_ACCOUNT_ID 
				FROM finance_mst_chartofaccount FCOA
				WHERE FCOA.CATEGORY_TYPE = 'C'
					AND CATEGORY_ID = '$customerId'";
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		$row_count	= mysqli_num_rows($result);
		if($row_count>0){
			return $row["CHART_OF_ACCOUNT_ID"];
		}else
		{
			$supplierName		= $this->GetPublicValues('strName','mst_customer','intId',$customerId);
			$response['type']		= 'fail';
			$response['ErrorMsg']	= "No GL Account available for Customer : $supplierName";
			return $response;
		}
	}
	
	private function GetInvoiceTypeGL($invoiceType)
	{
		$sql 	= " SELECT CHART_OF_ACCOUNT_ID 
					FROM finance_mst_chartofaccount_invoice_type 
					WHERE INVOICE_TYPE_ID = $invoiceType 
						AND TRANSACTION_CATEGORY = 'CU'";
		$result = $this->db->RunQuery2($sql);		
		$row_count	= mysqli_num_rows($result);
		$row	= mysqli_fetch_array($result);
		if($row_count>0){
			return $row["CHART_OF_ACCOUNT_ID"];
		}else
		{
			$invoiceTypeName		= $this->GetPublicValues('strInvoiceType','mst_invoicetype','intId',$invoiceType);
			$response['type']		= 'fail';
			$response['ErrorMsg']	= "No GL Account available for invoice type : $invoiceTypeName";
			return $response;
		}		
	}
	
	private function GetTaxGL($taxId)
	{
		$sql 	= " SELECT 
						FCOAT.CHART_OF_ACCOUNT_ID
					FROM finance_mst_chartofaccount_tax FCOAT
					WHERE TAX_ID = $taxId";
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		$row_count	= mysqli_num_rows($result);
		if($row_count>0)
			return $row["CHART_OF_ACCOUNT_ID"];
		else
		{
			$taxCode				= $this->GetPublicValues('strCode','mst_financetaxgroup','intId',$taxId);
			$response['type']		= 'fail';
			$response['ErrorMsg']	= "No GL Account available for tax code : $taxCode";
			return $response;
		}
	}
	
	private function GetPublicValues($select_fieldName,$tableName,$where_fieldId,$where_fieldValue)
	{
		$sql = "SELECT $select_fieldName AS VALUE FROM $tableName WHERE $where_fieldId = '$where_fieldValue'";
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		return $row["VALUE"];
	}
	
	private function UpdateStatus_sql($serialNo,$serialYear)
	{
		$sql = "UPDATE finance_customer_invoice_header
				SET PRINT_STATUS = 1
				WHERE SERIAL_NO = '$serialNo'
					AND SERIAL_YEAR = '$serialYear' ";
		$this->db->RunQuery($sql);
	}
	private function orderComplete($orderNo,$orderYear)
	{
		global $db;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_userId;
		
		$sql = "UPDATE trn_orderheader 
				SET
				intStatus = '-10' 
				WHERE
				intOrderNo = '$orderNo' AND 
				intOrderYear = '$orderYear' ";
				
		// checking pending MRNs for system close
		
		$sqlMrnPending = "SELECT ware_mrnheader.intStatus, ware_mrndetails.intMrnYear,ware_mrnheader.intMrnNo FROM `trn_orderheader` 
		JOIN 
		ware_mrndetails ON trn_orderheader.intOrderNo = ware_mrndetails.intOrderNo AND
		 trn_orderheader.intOrderYear = ware_mrndetails.intOrderYear 
		JOIN 
		ware_mrnheader ON ware_mrndetails.intMrnNo = ware_mrnheader.intMrnNo AND
		 ware_mrndetails.intMrnYear = ware_mrnheader.intMrnYear
		WHERE ware_mrndetails.intOrderYear = '$orderYear' AND ware_mrndetails.intOrderNo = '$orderNo' AND ware_mrnheader.intStatus != 1 AND ware_mrnheader.intStatus >= 0 ";
		
		$resultData = $this->db->RunQuery2($sqlMrnPending);
		
		//updating MRN details
		while($row = mysqli_fetch_array($resultData)){
			
			$mrnYear = $row['intMrnYear'];
			$mrnNo = $row['intMrnNo'];
			
		// if pending MRN Available need to update as -2 (system closed)
		
			$sqlUpdate = "UPDATE ware_mrnheader 
				SET
				intStatus = '-2' 
				WHERE
				intMrnNo = '$mrnNo' AND 
				intMrnYear = '$mrnYear'";

				$resultsOfupdate = $this->db->RunQuery2($sqlUpdate);
			
		}

		
		
		$result = $this->db->RunQuery2($sql);
			if($result==1){
				 $sql = "INSERT INTO `trn_orderheader_approvedby` (`intOrderNo`,`intYear`,`intApproveLevelNo`,`intApproveUser`,`dtApprovedDate`,intStatus) VALUES ('$orderNo','$orderYear','-10','$session_userId',now(),0)";
				 $resultOfapproved = $db->RunQuery2($sql);
				}
		if(!$result && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged	= $this->db->errormsg;
			$error_sql		= $sql;
		}
	}	
	
	function ValidateWithSessionCompany($orderNo,$orderYear)
	{
		global $db;
		global $session_companyId;
		
		$sql = "SELECT
				  LO.intCompanyId	AS COMPANY_ID
				FROM trn_orderheader OH
				  INNER JOIN mst_locations LO
					ON LO.intId = OH.intLocationId
				WHERE OH.intOrderNo = '$orderNo'
					AND OH.intOrderYear = '$orderYear'";
		$result = $db->RunQuery2($sql);
		$row 	= mysqli_fetch_array($result);
		if($session_companyId != $row["COMPANY_ID"])
			return false;
		else
			return true;
	}
}
?>
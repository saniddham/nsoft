<?php
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];
class Cls_Invoice_Get
{
	private $db;
	private $obj_common_get;
	
	function __construct($db)
	{
		$this->db = $db;
	}

//BEGIN - PUBLIC FUNCTIONS {	
	public function LoadData($customerPoNo)
	{		
		$responce = $this->LoadData_sql($customerPoNo);
		return json_encode($responce);
	}
	
	public function LoadMultiData($customerPoNo,$customerId)
	{
		$responce = $this->LoadOtherDetails($customerId,$customerPoNo);
		return json_encode($responce);
	}
	
	public function ValidateBeforeCancel($invoiceNo,$invoiceYear)
	{
		return $this->ValidateBeforeCancel_sql($invoiceNo,$invoiceYear);
	}
	
	public function GetPaymentVoucherReportHeader($serialNo,$serialYear)
	{
		return $this->GetPaymentVoucherReportHeader_sql($serialNo,$serialYear);
	}
	
	public function GetPaymentVoucherReportDetails($serialNo,$serialYear)
	{
		return $this->GetPaymentVoucherReportDetails_sql($serialNo,$serialYear);
	}


	public function GetDispatchNote($orderNo,$orderYear,$SerialNo,$SaleorderId){

        return $this->GetDispatchNote_sql($orderNo,$orderYear,$SerialNo,$SaleorderId);
    }

    public function  AddModalData($arr,$Maindetails){
            return $this->AddModalData_sql($arr,$Maindetails);
    }
    public function  CheckDuplicates($orderNo,$orderYear,$SerialNo,$SaleorderId){
        return $this->CheckDuplicates_sql($orderNo,$orderYear,$SerialNo,$SaleorderId);
    }

    public function  DeleteUnchecked($Serial,$fabricdetails,$Sid){
        return $this->DeleteUnchecked_sql($Serial,$fabricdetails,$Sid);

}//END	- PUBLIC FUNCTIONS }

//BEGIN - PRIVATE FUNCTIONS {	
	private function LoadData_sql($customerPoNo)
	{
		global $session_companyId;
		$customerCount 	= 0;
		$booAvailable	= false;
		
		$sql = "SELECT DISTINCT
				  	CU.intId   									AS CUSTOMER_ID,
				  	CU.strName 									AS CUSTOMER_NAME,
			  		OH.intCustomerLocation						AS CUST_INV_LOCATION_ID,
			  		OH.PAYMENT_RECEIVE_COMPLETED_FLAG			AS PAYMENT_RECEIVE_COMPLETED_FLAG				  
				FROM trn_orderheader OH 
				  INNER JOIN mst_customer CU
					ON CU.intId = OH.intCustomer
				  INNER JOIN finance_customer_invoice_balance CIB
					ON CIB.ORDER_YEAR = OH.intOrderYear
					  AND CIB.ORDER_NO = OH.intOrderNo
				  INNER JOIN mst_locations L 
				    ON L.intId = OH.intLocationId
				WHERE OH.strCustomerPoNo = '$customerPoNo'
					AND CIB.INVOICE_BALANCE > 0
					AND L.intCompanyId = '$session_companyId'
				GROUP BY CU.intId
				ORDER BY CU.strName";
				
				//echo $sql;
		
		$result = $this->db->RunQuery($sql);
			$data["CustomerHTML"] = "<option value=\"".""."\">".""."</option>";
		while($row = mysqli_fetch_array($result))
		{
			$customerCount++;
			$booAvailable		   = true;			
			$data["CustomerHTML"] .= "<option value=\"".$row["CUSTOMER_ID"]."\">".$row["CUSTOMER_NAME"]."</option>";
			$customerId			   = $row["CUSTOMER_ID"];
			$cust 			       = $data;
			$CustomerHTML	       = "<option value=\"".$row["CUSTOMER_ID"]."\">".$row["CUSTOMER_NAME"]."</option>"; 
			$flag				   = $row["PAYMENT_RECEIVE_COMPLETED_FLAG"];
		}		

		if($customerCount<=1 && $flag=='1')
		{
			$responce["Type"]	= "Fail";
			$responce["MSG"]	= "Customer PO : '$customerPoNo' mark as payment completed.";
			return $responce;
		}
		
		if(!$booAvailable)
		{
			$responce["Type"]	= "Fail";
			$responce["MSG"]	= "No pending details available for Customer PO : '$customerPoNo'";
			return $responce;
		}
		
		if($customerCount>1)
		{
			$responce			= $cust;
			$responce["Type"]	= "Fail";
			$responce["MSG"]	= "Multiple customer available for Customer PO : '$customerPoNo'";
			return $responce;
		}
		else
		{
			$responce["CustomerHTML"] 				= $CustomerHTML;
			$responce_details 						= $this->LoadOtherDetails($customerId,$customerPoNo);
			$responce						       += $responce_details;
		}
		return $responce;
	}
	
	private function LoadOtherDetails($customerId,$customerPoNo)
	{
		global $session_companyId;
		
		$sql = "SELECT DISTINCT
				  	CU.intId   									AS CUSTOMER_ID,
				  	CU.strName 									AS CUSTOMER_NAME,
			  		OH.intCustomerLocation						AS CUST_INV_LOCATION_ID,
			  		OH.PAYMENT_RECEIVE_COMPLETED_FLAG			AS PAYMENT_RECEIVE_COMPLETED_FLAG				  
				FROM trn_orderheader OH 
				  INNER JOIN mst_customer CU
					ON CU.intId = OH.intCustomer
				  INNER JOIN finance_customer_invoice_balance CIB
					ON CIB.ORDER_YEAR = OH.intOrderYear
					  AND CIB.ORDER_NO = OH.intOrderNo
				  INNER JOIN mst_locations L 
				    ON L.intId = OH.intLocationId
				WHERE OH.strCustomerPoNo = '$customerPoNo'
					AND CIB.INVOICE_BALANCE > 0
					AND L.intCompanyId = '$session_companyId'
					AND CU.intId = '$customerId'
				GROUP BY CU.intId
				ORDER BY CU.strName";
				//echo $sql;
		
		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		
		if($row['PAYMENT_RECEIVE_COMPLETED_FLAG']==1)
		{
			$responce["Type"]	= "Fail";
			$responce["MSG"]	= "Customer PO : '$customerPoNo' mark as payment completed.";
			return $responce;
		}
		else
		{
			$sql = "SELECT
					  CU.strInvoiceType								AS INVOICE_TYPE,
					  CIB.ORDER_NO							 		AS ORDER_NO,
					  CIB.ORDER_YEAR							 	AS ORDER_YEAR,
					  OH.intCustomerLocation						AS CUST_INV_LOCATION_ID,
					  CONCAT(CIB.ORDER_YEAR,'/',CIB.ORDER_NO) 		AS CONCAT_ORDER_NO,
					  (SELECT SUB_U.strUserName 
					   FROM sys_users SUB_U 
					   WHERE SUB_U.intUserId = OH.intMarketer) 		AS MARKETER,
					  OH.intCurrency								AS CURRENCY_ID
					FROM trn_orderheader OH 
					  INNER JOIN mst_customer CU
						ON CU.intId = OH.intCustomer
					  INNER JOIN finance_customer_invoice_balance CIB
						ON CIB.ORDER_YEAR = OH.intOrderYear
						  AND CIB.ORDER_NO = OH.intOrderNo
					  INNER JOIN mst_locations LO 
						ON LO.intId = OH.intLocationId
					WHERE OH.strCustomerPoNo = '$customerPoNo'
						AND OH.intCustomer = $customerId
						AND LO.intCompanyId = '$session_companyId'
						AND CIB.INVOICE_BALANCE > 0";
						
						
			$result = $this->db->RunQuery($sql);
			while($row = mysqli_fetch_array($result))
			{
				$data["InvoiceType"]			= $row["INVOICE_TYPE"];
				$data["OrderYear"]				= $row["ORDER_YEAR"];
				$data["OrderNo_HTML"]			= "<a href=\"?q=896&orderNo=".$row["ORDER_NO"]."&orderYear=".$row["ORDER_YEAR"]."\" id=\"txtOrderNo\" target=\"rptBulkOrder.php\">".$row["CONCAT_ORDER_NO"]."</a>";
				$data["Marketer"]				= $row["MARKETER"];
				$data["CurrencyId"]				= $row["CURRENCY_ID"];
				$data["Ledger"]					= "<option value=\"".$row["LEDGER_ID"]."\">".$row["LEDGER_NAME"]."</option>";
				//$data["ChartOfAccountID"]		= $this->obj_common_get->Get_InvoiceType_GLID();
				$data["CUSTOMER_INV_LOC_HTML"] 	= $this->CreateCustomerLocationHTML($customerId,$row["CUST_INV_LOCATION_ID"]);
				$data["ORDER_NO"]  = $row["ORDER_NO"];
				$dataArray 						= $data;
				
			}
			
			$loop1	= 0;
			$loop2	= 0;
			$loop3	= 0;
			
			$sql = "SELECT
						OH.intLocationId								AS LOCATION_ID, 
						OD.intSalesOrderId								AS SALES_ORDER_ID,
						OD.strSalesOrderNo								AS SALES_ORDER_NO,
						OD.strGraphicNo									AS GRAPHIC_NO,
						OD.strStyleNo 									AS STYLE_NO,
						P.strName										AS PART,
						SUM(CIB.INVOICE_BALANCE)						AS QTY,
						OD.dblPrice										AS PRICE,
						ROUND(SUM(CIB.INVOICE_BALANCE) * OD.dblPrice,4) AS VALUE	
					FROM trn_orderheader OH
					INNER JOIN trn_orderdetails OD ON OD.intOrderYear = OH.intOrderYear AND  OD.intOrderNo = OH.intOrderNo
					INNER JOIN finance_customer_invoice_balance CIB ON CIB.ORDER_YEAR = OH.intOrderYear AND CIB.ORDER_NO = OH.intOrderNo AND CIB.SALES_ORDER_ID = OD.intSalesOrderId
					INNER JOIN mst_part P ON P.intId = OD.intPart
					WHERE strCustomerPoNo = '$customerPoNo'
						AND OH.intCustomer = $customerId
					AND CIB.INVOICE_BALANCE > 0
					GROUP BY OH.intOrderYear,OH.intOrderNo,OD.intSalesOrderId";
					//echo $sql; 
			$result = $this->db->RunQuery($sql);
			while($row = mysqli_fetch_array($result))
			{
				$data["SALES_ORDER_ID"]	= $row["SALES_ORDER_ID"];
				$data["SALES_ORDER_NO"]	= $row["SALES_ORDER_NO"];
				$data["GRAPHIC_NO"]		= $row["GRAPHIC_NO"];
				$data["STYLE_NO"]		= $row["STYLE_NO"];
				$data["PART"]			= $row["PART"];
				$data["QTY"]			= $row["QTY"];
				$data["PRICE"]			= $row["PRICE"];
				$data["VALUE"]			= $row["VALUE"];
				$data["GL_HTML"]		= $this->GetGL(++$loop1);
				$data["TAX_HTML"]		= $this->GetTax(++$loop2);
				$data["PERCENTAGE_HTML"]= $this->GetPercentage();
				$data["COSTCENTER"]		= $this->GetCostCenter(++$loop3,$row["LOCATION_ID"]);
				$details[]				= $data;
			}
			$dataArray['GRID'] 		= $details;
			return $dataArray;
		}
	}
	
	private function GetCostCenter($id,$locationId)
	{
		$string 	= "";
		$sql = "SELECT
					FD.intId,
					FD.strName,
					FD.intLocation
				FROM mst_financedimension FD
				WHERE
					FD.intStatus = 1
				ORDER BY FD.strName";
		$result = $this->db->RunQuery($sql);
			$string .= "<select style=\"width:90px\" class=\"validate[required]\" id=\"cboCostCenter$id\">";
			$string .= "<option value=\"".""."\">".""."</option>";
		while($row = mysqli_fetch_array($result))
		{
			if($locationId == $row["intLocation"])
				$string .= "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";
			else
				$string .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
			$string .= "</select>";
		return $string;
	}
	
	private function GetTax($id)
	{
		$string 	= "";
		$sql = "SELECT
					mst_financetaxgroup.intId,
					mst_financetaxgroup.strCode
				FROM
				mst_financetaxgroup
				WHERE
					intStatus = 1
				ORDER BY strCode";
		$result = $this->db->RunQuery($sql);
			$string .= "<select style=\"width:90px\" id=\"cboTax$id\" class=\"cls_cbo_tax\">";
			$string .= "<option value=\""."NULL"."\">".""."</option>";
		while($row = mysqli_fetch_array($result))
		{
			$string .= "<option value=\"".$row['intId']."\">".$row['strCode']."</option>";
		}
			$string .= "</select>";
		return $string;
	}
	
	private function GetPercentage()
	{
			$string .= "<select style=\"width:90px\"  class=\"cls_cbo_percentage\">";
			$string .= "<option value=\"0\">"."0%"."</option>";
			$string .= "<option value=\"3\">"."3%"."</option>";
			$string .= "</select>";
		return $string;
	}
	
	private function GetGL($id)
	{
		$sql = "SELECT intId , strName,intDefaultFocus FROM mst_financecustomeritem WHERE intStatus = 1 ORDER BY strName";
		$result = $this->db->RunQuery($sql);
			$string = "<select style=\"width:90px\" id=\"cboGL$id\" class=\"validate[required]\">";
			$string .= "<option value=\"".""."\">".""."</option>";
		while($row = mysqli_fetch_array($result))
		{
			if($row['intDefaultFocus']=='1')
				$string .= "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";
			else
				$string .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
				
		}
			$string .= "</select>";
		return $string;
	}
	
	private function ValidateBeforeCancel_sql($invoiceNo,$invoiceYear)
	{
		$response	= $this->CheckStatus($invoiceNo,$invoiceYear);
		if($response["STATUS"]=='10')
		{
			$response["type"]		= 'false';
			$response["msg"] 		= "Invoice No : ".$response["INVOICE_NO"]." already canceled."; 
			return $response;
		}
		
		if($this->CheckPayment($invoiceNo,$invoiceYear))
		{
			$response["type"]		= 'false';
			$response["msg"] 		= "Unable to cancel. Payment receipt available for this invoice."; 
			return $response;
		}
	}
	
	private function CheckStatus($invoiceNo,$invoiceYear)
	{
		$sql = "SELECT STATUS,INVOICE_NO FROM finance_customer_invoice_header WHERE SERIAL_NO = '$invoiceNo' AND SERIAL_YEAR = '$invoiceYear'";
		$result = $this->db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return $row;
	}
	
	private function GetPaymentVoucherReportHeader_sql($serialNo,$serialYear)
	{
		$sql = "SELECT 
				CU.strName										AS CUSTOMER_NAME,
				CU.strAddress									AS ADDRESS,
				CU.strCity										AS CITY,
				CO.strCountryName								AS COUNTRY_NAME,
				CU.strContactPerson								AS CONTACT_PERSON,
				CU.strVatNo										AS CUSTOMER_VAT,
				CU.strSVatNo									AS CUSTOMER_SVAT,
				FCIH.INVOICE_NO									AS INVOICE_NO,
				CONCAT(FCIH.ORDER_YEAR,'/',FCIH.ORDER_NO)		AS ORDER_NO,
				FCIH.CURRENCY_ID								AS CURRENCY_ID,
				FCIH.INVOICED_DATE								AS INVOICED_DATE,
				FCIH.LOCATION_ID								AS LOCATION_ID,
				FCIH.COMPANY_ID									AS COMPANY_ID,
				FC.strCode 										AS CURRENCY_CODE,
				FC.strSymbol									AS CURRENCY_SYMBOL,
				U.strFullName 									AS CREATED_BY_NAME,
				FCIH.STATUS										AS STATUS,
				LO.strName										AS LOCATION_NAME,
				FCIH.PRINT_STATUS								AS PRINT_STATUS,
				FCIH.INVOICE_TYPE                               AS INVOICE_TYPE,
				(SELECT 
					SUM(FCID.VALUE)
				 FROM finance_customer_invoice_details FCID 
				 WHERE 
				 		FCID.SERIAL_NO = FCIH.SERIAL_NO
				 	AND FCID.SERIAL_YEAR = FCIH.SERIAL_YEAR)	AS VALUE,
				FCIH.REMARKS									AS REMARKS
				FROM 	
				finance_customer_invoice_header FCIH
				INNER JOIN mst_customer CU ON CU.intId = FCIH.CUSTOMER_ID
				INNER JOIN mst_financecurrency FC ON FCIH.CURRENCY_ID = FC.intId
				INNER JOIN sys_users U ON U.intUserId = FCIH.CREATED_BY
				LEFT JOIN mst_country CO ON CO.intCountryID = CU.intCountryId
				INNER JOIN trn_orderheader OH ON OH.intOrderNo = FCIH.ORDER_NO AND OH.intOrderYear = FCIH.ORDER_YEAR 
				INNER JOIN mst_locations LO ON LO.intId = OH.intLocationId
				WHERE 
						FCIH.SERIAL_NO 	= '$serialNo'
					AND SERIAL_YEAR 	= '$serialYear'";
		$result = $this->db->RunQuery($sql);
		return mysqli_fetch_array($result);	
	}
	
	private function GetPaymentVoucherReportDetails_sql($serialNo,$serialYear)
	{
		$sql = "SELECT 
				OH.strCustomerPoNo								AS CUSTOMER_PO_NO,
				OD.strSalesOrderNo								AS SALES_ORDER_NO,
				OD.strGraphicNo									AS GRAPHIC_NO,
				OD.strStyleNo 									AS STYLE_NO,
				P.strName										AS PART,
				CIB.DISPATCHED_GOOD_QTY							AS DISPATCHED_GOOD_QTY,
				CID.QTY											AS INVOICE_QTY,	
				CID.PRICE 										AS INVOICE_PRICE,
				CID.VALUE 										AS INVOICE_VALUE,
				CID.TAX_CODE 									AS TAX_CODE_ID,
				CID.COST_CENTER 								AS COST_CENTER_ID,
				CID.TAX_VALUE 									AS TAX_VALUE,
				FUI.strName										AS ITEM_NAME,
				'PCS'											AS UNIT		
				FROM finance_customer_invoice_details CID
				INNER JOIN finance_customer_invoice_header CIH ON CIH.SERIAL_YEAR = CID.SERIAL_YEAR AND CIH.SERIAL_NO = CID.SERIAL_NO
				INNER JOIN trn_orderdetails OD ON OD.intOrderYear = CIH.ORDER_YEAR AND OD.intOrderNo = CIH.ORDER_NO AND OD.intSalesOrderId = CID.SALES_ORDER_ID
				INNER JOIN trn_orderheader OH ON OH.intOrderNo = OD.intOrderNo AND OH.intOrderYear = OD.intOrderYear
				INNER JOIN mst_part P ON P.intId = OD.intPart
				INNER JOIN finance_customer_invoice_balance CIB ON CIB.ORDER_YEAR = CIH.ORDER_YEAR AND CIB.ORDER_NO = CIH.ORDER_NO AND OD.intSalesOrderId = CIB.SALES_ORDER_ID
				INNER JOIN mst_financecustomeritem FUI ON FUI.intId = CID.GL_ACCOUNT
				WHERE CID.SERIAL_YEAR = '$serialYear' AND CID.SERIAL_NO = '$serialNo'";
		return $this->db->RunQuery($sql);
	}
	
	private function CreateCustomerLocationHTML($customerId,$cusLocationId)
	{
		$html	= "";
		$sql = "SELECT
				  CLH.intId	AS LOCATION_ID,
				  CLH.strName	AS LOCATION_NAME
				FROM mst_customer_locations CL
				  INNER JOIN mst_customer_locations_header CLH
					ON CLH.intId = CL.intLocationId
				WHERE intCustomerId = $customerId
				ORDER BY CLH.strName";				
		$result = $this->db->RunQuery($sql);
		while($row = mysqli_fetch_array($result))
		{
			if($cusLocationId==$row["LOCATION_ID"])
				$html 	.= "<option value=\"".$row["LOCATION_ID"]."\" selected=\"selected\">".$row["LOCATION_NAME"]."</option>";
			else
				$html 	.= "<option value=\"".$row["LOCATION_ID"]."\">".$row["LOCATION_NAME"]."</option>";
				
		}
		return $html;
	}
	
	private function CheckPayment($invoiceNo,$invoiceYear)
	{
		$sql = "SELECT
				  COUNT(*)	AS COUNT
				FROM finance_customer_pay_receive_details D
				WHERE INVOICE_NO = '$invoiceNo'
				AND INVOICE_YEAR = '$invoiceYear'";
		$result = $this->db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		if($row["COUNT"]>0)
			return true;
		else
			return false;
	}


	private function GetDispatchNote_sql($orderNo,$orderYear,$SerialNo,$SaleorderId){

        $serial = explode('-',$SerialNo);
        $SerialNo = $serial[0];
      /*  $sql2= "SELECT
                *
                FROM
(SELECT
	TB1.SERIAL_NO,
	TB1.SERIAL_YEAR,
	TB1.SALES_ORDER_ID,
	TB1.GL_ACCOUNT,
	TB1.TAX_CODE_ID,
	TB1.COST_CENTER_ID,
	TB1.PRODUCTION_DAMAGE,
	TB1.FABRIC_DAMAGE,
	TB1.SAMPLE_QTY,
	TB1.MISSING_PANAL,
	TB1.OTHER,
	TB1.INVOICE_QTY AS INVOICE_QTY,
	TB1.FabricDispatchNo,
	TB1.FabricDispatchYear,
	TB1.FabricDispatchDetails,
	TB1.ORDER_NO,
	TB1.ORDER_YEAR,
	TB1.SALES_ORDER_NO,
	TB1.PART,
	TB1.GRAPHIC_NO,
	TB1.INVOICE_PRICE,
	TB1.INVOICE_VALUE,
	TB1.TAX_VALUE,
	TB1.DISPATCHED_GOOD_QTY AS DISPATCHED_GOOD_QTY,
	TB1.SampleNo,
	TB1.SampleYear,
	TB1.SampleDetails,
	TB1.STYLE_NO,
	TB1.Combo,
	TB1.PrintName,
	TB1.RevisionNo,
	TB1.Size,
	TB1.LineNo,
	TB1.CUT_NO,
	TB1.flag,

IF (TB1.flag != '', 1, 0) AS flag_radio
FROM
	(
	
		SELECT DISTINCT
			finance_customer_invoice_details.SERIAL_NO,
			finance_customer_invoice_details.SERIAL_YEAR,
			finance_customer_invoice_details.SALES_ORDER_ID,
			finance_customer_invoice_details.GL_ACCOUNT AS GL_ACCOUNT,
			finance_customer_invoice_details.TAX_CODE AS TAX_CODE_ID,
			finance_customer_invoice_details.COST_CENTER AS COST_CENTER_ID,
			finance_customer_invoice_details.PRODUCTION_DAMAGE_QTY AS PRODUCTION_DAMAGE,
			finance_customer_invoice_details.FABRIC_DAMAGE_QTY AS FABRIC_DAMAGE,
			finance_customer_invoice_details.SAMPLE_QTY AS SAMPLE_QTY,
			finance_customer_invoice_details.MISSING_PANAL AS MISSING_PANAL,
			finance_customer_invoice_details.OTHER AS OTHER,
			finance_customer_invoice_details.QTY AS INVOICE_QTY,
			ware_fabricdispatchheader.intBulkDispatchNo AS FabricDispatchNo,
			ware_fabricdispatchheader.intBulkDispatchNoYear AS FabricDispatchYear,
			CONCAT(
				ware_fabricdispatchheader.intBulkDispatchNo,
				'/',
				ware_fabricdispatchheader.intBulkDispatchNoYear
			) AS FabricDispatchDetails,
			finance_customer_invoice_header.ORDER_NO,
			finance_customer_invoice_header.ORDER_YEAR,
			trn_orderdetails.strSalesOrderNo AS SALES_ORDER_NO,
			mst_part.strName AS PART,
			trn_orderdetails.strGraphicNo AS GRAPHIC_NO,
			finance_customer_invoice_details.PRICE AS INVOICE_PRICE,
			finance_customer_invoice_details.`VALUE` AS INVOICE_VALUE,
			finance_customer_invoice_details.TAX_VALUE AS TAX_VALUE,
			-- finance_customer_invoice_balance.DISPATCHED_GOOD_QTY AS DISPATCHED_GOOD_QTY, 
			(
				SELECT
				sum(ware_fabricdispatchdetails.dblGoodQty)

				FROM
				ware_fabricdispatchdetails
				WHERE
				ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo
				AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
				AND ware_fabricdispatchdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
			) AS DISPATCHED_GOOD_QTY,
			trn_orderdetails.intSampleNo AS SampleNo,
			trn_orderdetails.intSampleYear AS SampleYear,
			CONCAT(
				trn_orderdetails.intSampleNo,
				'/',
				trn_orderdetails.intSampleYear
			) AS SampleDetails,
			trn_orderdetails.strStyleNo AS STYLE_NO,
			trn_orderdetails.strCombo AS Combo,
			trn_orderdetails.strPrintName AS PrintName,
			trn_orderdetails.intRevisionNo AS RevisionNo,
			ware_fabricreceiveddetails.strSize AS Size,
			ware_fabricreceiveddetails.strLineNo AS LineNo,
			ware_fabricreceiveddetails.strCutNo AS CUT_NO,
			(
				SELECT DISTINCT
					finance_customer_invoice_details_dispatch_wise.DISPATCH_NO
				FROM
					finance_customer_invoice_details_dispatch_wise
				WHERE
					finance_customer_invoice_details_dispatch_wise.DISPATCH_NO = ware_fabricdispatchheader.intBulkDispatchNo
				AND finance_customer_invoice_details_dispatch_wise.DISPATCH_YEAR = ware_fabricdispatchheader.intBulkDispatchNoYear
				AND finance_customer_invoice_details_dispatch_wise.SALES_ORDER_ID = $SaleorderId
				AND finance_customer_invoice_details_dispatch_wise.SERIAL_NO = finance_customer_invoice_details.SERIAL_NO
				AND finance_customer_invoice_details_dispatch_wise.SERIAL_YEAR = finance_customer_invoice_details.SERIAL_YEAR
			) AS flag
		FROM
			finance_customer_invoice_details
		INNER JOIN finance_customer_invoice_header ON finance_customer_invoice_details.SERIAL_NO = finance_customer_invoice_header.SERIAL_NO
		AND finance_customer_invoice_details.SERIAL_YEAR = finance_customer_invoice_header.SERIAL_YEAR
		INNER JOIN ware_fabricdispatchheader ON ware_fabricdispatchheader.intOrderNo = finance_customer_invoice_header.ORDER_NO
		AND ware_fabricdispatchheader.intOrderYear = finance_customer_invoice_header.ORDER_YEAR
		INNER JOIN ware_fabricdispatchdetails ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo
		AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
		AND ware_fabricdispatchdetails.intSalesOrderId = finance_customer_invoice_details.SALES_ORDER_ID
		INNER JOIN trn_orderheader ON finance_customer_invoice_header.ORDER_NO = trn_orderheader.intOrderNo
		AND finance_customer_invoice_header.ORDER_YEAR = trn_orderheader.intOrderYear
		AND ware_fabricdispatchheader.intOrderNo = trn_orderheader.intOrderNo
		AND ware_fabricdispatchheader.intOrderYear = trn_orderheader.intOrderYear
		INNER JOIN trn_orderdetails ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
		AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
		INNER JOIN mst_part ON ware_fabricdispatchdetails.intPart = mst_part.intId
		AND trn_orderdetails.intPart = mst_part.intId
		INNER JOIN finance_customer_invoice_balance ON finance_customer_invoice_header.ORDER_NO = finance_customer_invoice_balance.ORDER_NO
		AND finance_customer_invoice_header.ORDER_YEAR = finance_customer_invoice_balance.ORDER_YEAR
		AND finance_customer_invoice_details.SALES_ORDER_ID = finance_customer_invoice_balance.SALES_ORDER_ID
		INNER JOIN ware_fabricreceivedheader ON ware_fabricreceivedheader.intOrderNo = trn_orderheader.intOrderNo
		AND ware_fabricreceivedheader.intOrderYear = trn_orderheader.intOrderYear
		INNER JOIN ware_fabricreceiveddetails ON ware_fabricreceiveddetails.intFabricReceivedNo = ware_fabricreceivedheader.intFabricReceivedNo
		AND ware_fabricreceiveddetails.intFabricReceivedYear = ware_fabricreceivedheader.intFabricReceivedYear
		AND ware_fabricreceiveddetails.intPart = mst_part.intId
		AND ware_fabricreceiveddetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
		AND ware_fabricreceiveddetails.strCutNo = ware_fabricdispatchdetails.strCutNo
		AND ware_fabricreceiveddetails.strSize = ware_fabricdispatchdetails.strSize
		WHERE

		finance_customer_invoice_header.ORDER_NO ='$orderNo' AND
finance_customer_invoice_header.ORDER_YEAR = '$orderYear' AND
finance_customer_invoice_header.SERIAL_NO = '$SerialNo' AND
finance_customer_invoice_header.SERIAL_YEAR = '$serial[1]'
AND trn_orderdetails.intSalesOrderId= '$SaleorderId'

 group by finance_customer_invoice_header.INVOICE_NO,
finance_customer_invoice_header.SERIAL_NO,
finance_customer_invoice_header.SERIAL_YEAR,
ware_fabricdispatchdetails.intBulkDispatchNo,
ware_fabricdispatchdetails.intBulkDispatchNoYear,
ware_fabricdispatchdetails.intSalesOrderId
	) AS TB1
GROUP BY
	TB1.FabricDispatchNo,
 	TB1.SALES_ORDER_NO,
TB1.FabricDispatchYear
ORDER BY 
TB1.SALES_ORDER_NO,
TB1.FabricDispatchNo
) AS TB2
GROUP BY TB2.SALES_ORDER_ID"; */
      $sql2 ="SELECT
	finance_customer_invoice_details.SERIAL_NO,
	finance_customer_invoice_details.SERIAL_YEAR,
	finance_customer_invoice_details.SALES_ORDER_ID,
	finance_customer_invoice_details.GL_ACCOUNT AS GL_ACCOUNT,
	finance_customer_invoice_details.TAX_CODE AS TAX_CODE_ID,
	finance_customer_invoice_details.COST_CENTER AS COST_CENTER_ID,
	finance_customer_invoice_details.PRODUCTION_DAMAGE_QTY AS PRODUCTION_DAMAGE,
	finance_customer_invoice_details.FABRIC_DAMAGE_QTY AS FABRIC_DAMAGE,
	finance_customer_invoice_details.SAMPLE_QTY AS SAMPLE_QTY,
	finance_customer_invoice_details.MISSING_PANAL AS MISSING_PANAL,
	finance_customer_invoice_details.OTHER AS OTHER,
	finance_customer_invoice_details.QTY AS INVOICE_QTY,
	ware_fabricdispatchheader.intBulkDispatchNo AS FabricDispatchNo,
	ware_fabricdispatchheader.intBulkDispatchNoYear AS FabricDispatchYear,
	CONCAT(
		ware_fabricdispatchheader.intBulkDispatchNo,
		'/',
		ware_fabricdispatchheader.intBulkDispatchNoYear
	) AS FabricDispatchDetails,
	finance_customer_invoice_header.ORDER_NO,
	finance_customer_invoice_header.ORDER_YEAR,

		finance_customer_invoice_details.PRICE AS INVOICE_PRICE,
			finance_customer_invoice_details.`VALUE` AS INVOICE_VALUE,
			finance_customer_invoice_details.TAX_VALUE AS TAX_VALUE,

	(
		SELECT DISTINCT
			sum(
				ware_fabricdispatchdetails.dblGoodQty
			)
		FROM
			ware_fabricdispatchdetails
		WHERE
			ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo
		AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
		AND ware_fabricdispatchdetails.intSalesOrderId = '$SaleorderId'
	) AS DISPATCHED_GOOD_QTY, 
	(
		SELECT DISTINCT
			trn_orderdetails.intSampleNo
		FROM
			trn_orderdetails
		WHERE
			trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
		AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
	) AS SampleNo,

	(
		SELECT DISTINCT
			trn_orderdetails.intSampleYear
		FROM
			trn_orderdetails
		WHERE
			trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
		AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
	) AS SampleYear,
	(
		SELECT DISTINCT
			trn_orderdetails.strGraphicNo
		FROM
			trn_orderdetails
		WHERE
			trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
		AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
	) AS GRAPHIC_NO,
	(
		SELECT DISTINCT
			trn_orderdetails.strStyleNo
		FROM
			trn_orderdetails
		WHERE
			trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
		AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
	) AS STYLE_NO,
	(
		SELECT DISTINCT
			trn_orderdetails.strCombo
		FROM
			trn_orderdetails
		WHERE
			trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
		AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
	) AS Combo,
	(
		SELECT DISTINCT
			trn_orderdetails.strPrintName
		FROM
			trn_orderdetails
		WHERE
			trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
		AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
	) AS PrintName,
	(
		SELECT DISTINCT
			trn_orderdetails.intRevisionNo
		FROM
			trn_orderdetails
		WHERE
			trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
		AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
	) AS RevisionNo,

	(
		SELECT DISTINCT
			mst_part.strName
		FROM
			trn_orderdetails
		INNER JOIN mst_part on  mst_part.intId =trn_orderdetails.intPart 
		WHERE
			trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
		AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
	) AS PART,


	(
		SELECT DISTINCT
			finance_customer_invoice_details_dispatch_wise.DISPATCH_NO
		FROM
			finance_customer_invoice_details_dispatch_wise
		WHERE
			finance_customer_invoice_details_dispatch_wise.DISPATCH_NO = ware_fabricdispatchheader.intBulkDispatchNo
		AND finance_customer_invoice_details_dispatch_wise.DISPATCH_YEAR = ware_fabricdispatchheader.intBulkDispatchNoYear
		AND finance_customer_invoice_details_dispatch_wise.SALES_ORDER_ID = '$SaleorderId'
		AND finance_customer_invoice_details_dispatch_wise.SERIAL_NO = finance_customer_invoice_details.SERIAL_NO
		AND finance_customer_invoice_details_dispatch_wise.SERIAL_YEAR = finance_customer_invoice_details.SERIAL_YEAR
	) AS flag,
	
	(
		SELECT DISTINCT
			trn_orderdetails.strSalesOrderNo
		FROM
			trn_orderdetails
		WHERE
			trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
		AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
AND	trn_orderdetails.intSalesOrderId ='2'
	) AS SALES_ORDER_NO
FROM
finance_customer_invoice_header
INNER JOIN finance_customer_invoice_details ON finance_customer_invoice_details.SERIAL_NO = finance_customer_invoice_header.SERIAL_NO AND finance_customer_invoice_details.SERIAL_YEAR = finance_customer_invoice_header.SERIAL_YEAR
INNER JOIN ware_fabricdispatchheader ON ware_fabricdispatchheader.intOrderNo = finance_customer_invoice_header.ORDER_NO AND ware_fabricdispatchheader.intOrderYear = finance_customer_invoice_header.ORDER_YEAR
INNER JOIN trn_orderheader ON finance_customer_invoice_header.ORDER_NO = trn_orderheader.intOrderNo AND finance_customer_invoice_header.ORDER_YEAR = trn_orderheader.intOrderYear AND ware_fabricdispatchheader.intOrderNo = trn_orderheader.intOrderNo AND ware_fabricdispatchheader.intOrderYear = trn_orderheader.intOrderYear
INNER JOIN ware_fabricreceivedheader ON ware_fabricreceivedheader.intOrderNo = trn_orderheader.intOrderNo AND ware_fabricreceivedheader.intOrderYear = trn_orderheader.intOrderYear
WHERE
		finance_customer_invoice_header.ORDER_NO ='$orderNo' AND
finance_customer_invoice_header.ORDER_YEAR = '$orderYear' AND
finance_customer_invoice_header.SERIAL_NO = '$SerialNo' AND
finance_customer_invoice_header.SERIAL_YEAR = '$serial[1]'
AND finance_customer_invoice_details.SALES_ORDER_ID = '$SaleorderId'
 group by finance_customer_invoice_header.INVOICE_NO,
finance_customer_invoice_header.SERIAL_NO,
finance_customer_invoice_header.SERIAL_YEAR,
ware_fabricdispatchheader.intBulkDispatchNo,
ware_fabricdispatchheader.intBulkDispatchNoYear,
finance_customer_invoice_details.SALES_ORDER_ID";

return  $result = $this->db->RunQuery($sql2);
    }



    private  function  AddModalData_sql ($arr,$Maindetails){


        foreach ($arr as $array) {

            if ($array['fabricDetails'] != '') {
                $fabricDetails = explode("/", $array['fabricDetails']);
                $fabricDetailsNo1 = "'$fabricDetails[0]'";
                $fabricDetailsNo = str_replace('"', '', $fabricDetailsNo1);

                $fabricDetailYear1 = "'$fabricDetails[1]'";
                $fabricDetailYear = str_replace('"', '', $fabricDetailYear1);

            } else {
                $fabricDetailsNo = "NULL";
                $fabricDetailYear = "NULL";
                $responce["type"] = 'fail';
                $responce["msg"] = 'Fabric Details Empty';
                $responce["ErrorSql"] = 'Update Failed';

                //return json_encode($responce);
            }


            $SerialYear = $array['SerialYear'];
            $SerialNo = $array['SerialNo'];
            $salesOrderId = $array['salesOrderId'];
            if ($array['CutNo'] == '') {
                $responce["type"] = 'fail';
                $responce["msg"] = 'CutNO Empty';
                $responce["ErrorSql"] = 'Update Failed';
                $CutNo = "NULL";
                //return json_encode($responce);

            }
            $CutNo = $array['CutNo'];


            if ($array['Size'] == '') {
                $responce["type"] = 'fail';
                $responce["msg"] = 'SIZE Empty';
                $responce["ErrorSql"] = 'Update Failed';

                // return json_encode($responce);

            }
            $Size =$array['Size'];
            $GLAccount = $array['GLAccount'];
            if ($array['TaxCode'] == '') {
                $TaxCode = "NULL";
            } else {
                $TaxCode = $array['TaxCode'];
            }

            $CostCenter = $array['CostCenter'];
            $ProductionDamage = $array['ProductionDamage'];
            $FabricDamge = $array['FabricDamge'];
            $SampleQty = $array['SampleQty'];
            $missingPanel = $array['missingPanel'];
            $other = $array['other'];
            $invoiceQty = $array['DispatchGoodQty'];
            $invoicePrice = $array['invoicePrice'];
            $invoiceValue = $array['invoiceValue'];
            $TaxValue = $array['TaxValue'];

            $SalesorderId   = $Maindetails['SalesorderId'];
            $Orderno    =$Maindetails['Orderno'];
            $Orderyear  = $Maindetails['Orderyear'];
            $SerialNo  = $Maindetails['SerialNo'];
            $Serialyear = $Maindetails['Serialyear'];


            $sql_del = "DELETE
                        FROM
	`finance_customer_invoice_details_dispatch_wise`
    WHERE
	(`SERIAL_NO` = '$SerialNo')
    AND (`SERIAL_YEAR` = '$Serialyear')
    AND (`SALES_ORDER_ID` = '$SalesorderId')
    AND (`DISPATCH_NO` = $fabricDetailsNo)
    AND (`DISPATCH_YEAR` = $fabricDetailYear)";

    $result_del = $this->db->RunQuery($sql_del);

            if ($result_del == true) {

            $sql1 = "INSERT INTO `finance_customer_invoice_details_dispatch_wise` (
	          `SERIAL_NO`,
	          `SERIAL_YEAR`,
	          `SALES_ORDER_ID`,
	          `DISPATCH_NO`,
	          `DISPATCH_YEAR`,
	          `CUT_NO`,
	          `SIZE`,
	          `GL_ACCOUNT`,
	          `TAX_CODE`,
	          `COST_CENTER`,
	          `PRODUCTION_DAMAGE_QTY`,
	          `FABRIC_DAMAGE_QTY`,
	          `SAMPLE_QTY`,
	          `MISSING_PANAL`,
	          `OTHER`,
	          `QTY`,
	          `PRICE`,
	          `VALUE`,
	          `TAX_VALUE`
              )
            VALUES
	        (
		      '$SerialNo',
		      '$Serialyear',
		      '$SalesorderId',
		      $fabricDetailsNo,
		      $fabricDetailYear,
		      '$CutNo',
		      '$Size',
		      '$GLAccount',
		      $TaxCode,
		      '$CostCenter',
		      '$ProductionDamage',
		      '$FabricDamge',
		      '$SampleQty',
		      '$missingPanel',
		      '$other',
		      '$invoiceQty',
		      '$invoicePrice',
		      '$invoiceValue',
		      '$TaxValue'
	          )";

            $result = $this->db->RunQuery($sql1);
        }else{
                $result= false;
            }
        }

if($result == true){
    $responce["type"] = 'pass';
    $responce["msg"] = 'Saved Sucsessfully';
    $responce["Sql"] = 'InsertDone';
}else{
    $responce["type"] = 'fail';
    $responce["msg"] = 'Insert Fail';
    $responce["Sql"] = 'my_sql error';
}

        return json_encode($responce);



        //return  $result = $this->db->RunQuery($sql1);

 }

    private function  CheckDuplicates_sql($orderNo,$orderYear,$SerialNo,$SaleorderId){
        $serial = explode('-',$SerialNo);
        $SerialNo = $serial[0];
	    $sql ="SELECT DISTINCT
ware_fabricdispatchdetails.intBulkDispatchNo,
ware_fabricdispatchdetails.intBulkDispatchNoYear

FROM
finance_customer_invoice_header
INNER JOIN finance_customer_invoice_details ON finance_customer_invoice_details.SERIAL_NO = finance_customer_invoice_header.SERIAL_NO AND finance_customer_invoice_details.SERIAL_YEAR = finance_customer_invoice_header.SERIAL_YEAR
INNER JOIN ware_fabricdispatchheader ON ware_fabricdispatchheader.intOrderNo = finance_customer_invoice_header.ORDER_NO AND ware_fabricdispatchheader.intOrderYear = finance_customer_invoice_header.ORDER_YEAR
INNER JOIN ware_fabricdispatchdetails ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
WHERE  finance_customer_invoice_header.ORDER_NO ='$orderNo' AND
finance_customer_invoice_header.ORDER_YEAR = '$orderYear' AND
finance_customer_invoice_header.SERIAL_NO = '$SerialNo' AND
finance_customer_invoice_header.SERIAL_YEAR = '$serial[1]'
	AND ware_fabricdispatchdetails.intSalesOrderId= '$SaleorderId'
	AND finance_customer_invoice_header.`STATUS`  = '1'";


       $result = $this->db->RunQuery($sql);
        $row = mysqli_fetch_array($result);

       $dispatchNo= $row['intBulkDispatchNo'];
        $dispatchYear= $row['intBulkDispatchNoYear'];

        $sqlcheck = "SELECT DISTINCT
ware_fabricdispatchdetails.intBulkDispatchNo,
ware_fabricdispatchdetails.intBulkDispatchNoYear,
finance_customer_invoice_header.SERIAL_NO,
finance_customer_invoice_header.SERIAL_YEAR
FROM
ware_fabricdispatchheader
INNER JOIN ware_fabricdispatchdetails ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
INNER JOIN finance_customer_invoice_header ON finance_customer_invoice_header.ORDER_NO = ware_fabricdispatchheader.intOrderNo AND finance_customer_invoice_header.ORDER_YEAR = ware_fabricdispatchheader.intOrderYear
WHERE
ware_fabricdispatchheader.intBulkDispatchNo ='$dispatchNo' AND
ware_fabricdispatchheader.intBulkDispatchNoYear = '$dispatchYear'
AND finance_customer_invoice_header.`STATUS`  = '1'" ;

        $result = $this->db->RunQuery($sqlcheck);
        while($row = mysqli_fetch_array($result)){

           if($row['SERIAL_NO'] != $SerialNo){
               $responce["type"] = "fail";
               $responce["msg"] = "Duplicate Dispatch No ".$row['intBulkDispatchNo'];
               $responce["Sql"] = "duplicate";

               return json_encode($responce);
               die;
           }
        }
        $responce["type"] = 'pass';
        return json_encode($responce);
}

private  function  DeleteUnchecked_sql($Serial,$fabricdetails,$Sid){

    $SerialNo = $Serial[0];
    $Serialyear= $Serial[1];
    $fabricDetailsNo=$fabricdetails['0'];
    $fabricDetailYear=$fabricdetails['1'];

    $sql_del = "DELETE
                        FROM
	`finance_customer_invoice_details_dispatch_wise`
    WHERE
	(`SERIAL_NO` = '$SerialNo')
    AND (`SERIAL_YEAR` = '$Serialyear')
    AND (`SALES_ORDER_ID` = '$Sid')
    AND (`DISPATCH_NO` = $fabricDetailsNo)
    AND (`DISPATCH_YEAR` = $fabricDetailYear)";
    $result_del = $this->db->RunQuery($sql_del);

    if($result_del == true){
        $responce["type"] = 'pass';
        $responce["msg"] = 'Removed';
        $responce["Sql"] = 'Removed Done';
    }else{
        $responce["type"] = 'fail';
        $responce["msg"] = 'Removed Fail';
        $responce["Sql"] = 'my_sql error';
    }

    return json_encode($responce);
}
//END 	- PRIVATE FUNCTIONS }
}
?>
<?php
$session_companyId	= $_SESSION["headCompanyId"];

class Cls_Credit_Note_Get
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	
	public function LoadInvoiceNo($customerId)
	{
		return $this->LoadInvoiceNo_sql($customerId);
	}
	
	public function LoadDetails($invoiceNo,$invoiceYear)
	{
		return $this->LoadDetails_sql($invoiceNo,$invoiceYear);
	}
	
	public function SaveValidation($invoiceNo,$currencyId,$customerId,$grandTotal)
	{
		$invoiceNo_Array	= explode('/',$invoiceNo);
		$balanceAmount = $this->GetInvoiceBalace($invoiceNo_Array[0],$invoiceNo_Array[1],$currencyId,$customerId);

		if($balanceAmount < $grandTotal)
		{
			$response['type'] 		 = "fail";
			$response['msg'] 		 = "No enough invoice balance amount available to proceed.";
		}
		return $response;
	}
	
	public function GetReportHeader($serialNo,$serialYear)
	{
		return $this->GetReportHeader_sql($serialNo,$serialYear);
	}
	
	public function GetReportDetails($serialNo,$serialYear)
	{
		return $this->GetReportDetails_sql($serialNo,$serialYear);
	}
	
	private function LoadInvoiceNo_sql($customerId)
	{
		global $session_companyId;

		$sql = "SELECT
				 CONCAT(ST.INVOICE_NO,'/',ST.INVOICE_YEAR)	AS INVOICE_SERIAL_NO,
				 IH.INVOICE_NO								AS INVOICE_NO,
				 IH.REMARKS									AS INVOICE_REMARKS
				FROM finance_customer_transaction ST
				  INNER JOIN finance_customer_invoice_header IH ON IH.SERIAL_NO = ST.INVOICE_NO AND IH.SERIAL_YEAR = ST.INVOICE_YEAR
				WHERE ST.COMPANY_ID = '$session_companyId'
				 AND ST.CUSTOMER_ID = $customerId
				 GROUP BY ST.INVOICE_YEAR,ST.INVOICE_NO
					HAVING ROUND(SUM(ST.VALUE),2)  > 0";
			$string = "<option value=\"".""."\">&nbsp;</option>";
		$result = $this->db->RunQuery($sql);
		while($row = mysqli_fetch_array($result))
		{
			$string .= "<option value=\"".$row["INVOICE_SERIAL_NO"]."\">".$row["INVOICE_REMARKS"].' / '.$row['INVOICE_NO']."</option>";
		}
			$response["HTML"] 			= $string;
			$response["INVOICE_TYPE"]	= $this->GetInvoiceType($customerId);			
		return json_encode($response);
	}
	
	private function LoadDetails_sql($invoiceNo,$invoiceYear)
	{
		$ordReportId	= 896;
		$sql = "SELECT 
				 	CIH.CURRENCY_ID									AS CURRENCY_ID,
					CIH.LEDGER_ID									AS LEDGER_ID,
					CIH.ORDER_NO									AS ORDER_NO,
					CIH.ORDER_YEAR									AS ORDER_YEAR,
					CONCAT(CIH.ORDER_YEAR,'/',CIH.ORDER_NO) 		AS CONCAT_ORDER_NO,
					CIH.CUSTOMER_ID									AS CUSTOMER_ID
				FROM finance_customer_invoice_header CIH
				WHERE SERIAL_NO = '$invoiceNo' AND SERIAL_YEAR = '$invoiceYear'";
		$result = $this->db->RunQuery($sql);
		while($row = mysqli_fetch_array($result))
		{
			$data["CURRENCY_ID"]	= $row["CURRENCY_ID"];
			$data["LEDGER_ID"]		= $row["LEDGER_ID"];
			$data["ORDER_HTML"]		= "<a href=\"?q=".$ordReportId."&orderNo=".$row["ORDER_NO"]."&orderYear=".$row["ORDER_YEAR"]."\" id=\"txtOrderNo\" target=\"rptBulkOrder.php\">".$row["CONCAT_ORDER_NO"]."</a>";
			$data["INVOICE_BALANCE"]= $this->GetInvoiceBalace($invoiceNo,$invoiceYear,$row["CURRENCY_ID"],$row["CUSTOMER_ID"]);
		}
		$response	= $data;
		
		$loop1	= 0;
		$loop2	= 0;
		$loop3	= 0;
		
		$sql = "SELECT 
					OD.intSalesOrderId	AS SALES_ORDER_ID,
					OD.strSalesOrderNo	AS SALES_ORDER_NO,
					OD.strGraphicNo		AS GRAPHIC_NO,
					OD.strStyleNo		AS STYLE_NO,
					P.strName			AS PART,
					CID.COST_CENTER		AS COST_CENTER,
					QTY 				AS QTY,
					PRICE				AS PRICE,
					VALUE 				AS VALUE
				FROM finance_customer_invoice_header CIH
				INNER JOIN finance_customer_invoice_details CID ON CID.SERIAL_NO = CIH.SERIAL_NO AND CID.SERIAL_YEAR = CIH.SERIAL_YEAR
				INNER JOIN trn_orderdetails OD ON OD.intOrderNo = CIH.ORDER_NO AND OD.intOrderYear = CIH.ORDER_YEAR AND OD.intSalesOrderId = CID.SALES_ORDER_ID
				INNER JOIN mst_part P ON P.intId = OD.intPart
				WHERE CIH.SERIAL_NO = '$invoiceNo' AND CIH.SERIAL_YEAR = '$invoiceYear'";
		$result = $this->db->RunQuery($sql);
		while($row = mysqli_fetch_array($result))
		{
			$data["SALES_ORDER_ID"]	= $row["SALES_ORDER_ID"];
			$data["SALES_ORDER_NO"]	= $row["SALES_ORDER_NO"];
			$data["GRAPHIC_NO"]		= $row["GRAPHIC_NO"];
			$data["STYLE_NO"]		= $row["STYLE_NO"];
			$data["PART"]			= $row["PART"];
			$data["QTY"]			= $row["QTY"];
			$data["PRICE"]			= $row["PRICE"];
			$data["VALUE"]			= $row["VALUE"];
			$data["GL_HTML"]		= $this->GetGL(++$loop1);
			$data["TAX_HTML"]		= $this->GetTax(++$loop2);
			$data["COSTCENTER"]		= $this->GetCostCenter(++$loop3,$row["COST_CENTER"]);
			$details[]				= $data;
		}
		$response['GRID'] 		= $details;
		return json_encode($response);
	}
	
	private function GetCostCenter($id,$coseCenter)
	{
		$string 	= "";
		$sql = "SELECT
					intId,
					strName
				FROM mst_financedimension
				WHERE
					intStatus = 1
				ORDER BY strName";
		$result = $this->db->RunQuery($sql);
			$string .= "<select style=\"width:90px\" class=\"validate[required]\" id=\"cboCostCenter$id\">";
			$string .= "<option value=\"".""."\">".""."</option>";
		while($row = mysqli_fetch_array($result))
		{
			if($coseCenter == $row["intId"])
				$string .= "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";
			else
				$string .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
			$string .= "</select>";
		return $string;
	}
	
	private function GetTax($id)
	{
		$string 	= "";
		$sql = "SELECT
					mst_financetaxgroup.intId,
					mst_financetaxgroup.strCode
				FROM
				mst_financetaxgroup
				WHERE
					intStatus = 1
				ORDER BY strCode";
		$result = $this->db->RunQuery($sql);
			$string .= "<select style=\"width:90px\" id=\"cboTax$id\" class=\"cls_cbo_tax\">";
			$string .= "<option value=\""."NULL"."\">".""."</option>";
		while($row = mysqli_fetch_array($result))
		{
			$string .= "<option value=\"".$row['intId']."\">".$row['strCode']."</option>";
		}
			$string .= "</select>";
		return $string;
	}
	
	private function GetInvoiceBalace($invoiceNo,$invoiceYear,$currencyId,$customerId)
	{
		global $session_companyId;
		$amount	= 0;
		$sql = "SELECT
				  ROUND(SUM(ST.VALUE),4) AS BALANCE_AMOUNT
				FROM finance_customer_transaction ST
				WHERE ST.INVOICE_NO = '$invoiceNo'
					AND ST.INVOICE_YEAR = '$invoiceYear'
					AND ST.CURRENCY_ID = '$currencyId'
					AND ST.COMPANY_ID = '$session_companyId'
					AND ST.CUSTOMER_ID = '$customerId'";
		$result = $this->db->RunQuery($sql);
		while($row 	= mysqli_fetch_array($result))
		{
			$amount = $row["BALANCE_AMOUNT"];
		}
		return $amount;
	}
	
	private function GetGL($id)
	{
		global $session_companyId;
		
		$string 	= "";
		$sql = "SELECT intId , strName,intDefaultFocus FROM mst_financecustomeritem WHERE intStatus = 1 ORDER BY strName";
		$result = $this->db->RunQuery($sql);
			$string .= "<select style=\"width:90px\" id=\"cboGL$id\" class=\"validate[required]\">";
			$string .= "<option value=\"".""."\">".""."</option>";
		while($row = mysqli_fetch_array($result))
		{
			if($row['intDefaultFocus']=='1')
				$string .= "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";
			else
				$string .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
			$string .= "</select>";
		return $string;
	}
	
	private function GetInvoiceType($customerId)
	{
		$sql = "SELECT strInvoiceType AS INVOICE_TYPE 
			    FROM mst_customer 
				WHERE intId = $customerId";
		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		return $row["INVOICE_TYPE"];
	}
	
	private function GetReportHeader_sql($serialNo,$serialYear)
	{
		$sql = "SELECT 
				CU.strName										AS CUSTOMER_NAME,
				CU.strAddress									AS ADDRESS,
				CU.strCity										AS CITY,
				CO.strCountryName								AS COUNTRY_NAME,
				CU.strContactPerson								AS CONTACT_PERSON,
				CU.strVatNo										AS CUSTOMER_VAT,
				CU.strSVatNo									AS CUSTOMER_SVAT,
				FCIH.INVOICE_NO									AS INVOICE_NO,
				CONCAT(FCNH.ORDER_YEAR,'/',FCNH.ORDER_NO)		AS ORDER_NO,
				FCNH.CURRENCY_ID								AS CURRENCY_ID,
				FCNH.CREDIT_DATE								AS INVOICED_DATE,
				FCNH.LOCATION_ID								AS LOCATION_ID,
				FCNH.COMPANY_ID									AS COMPANY_ID,
				FC.strCode 										AS CURRENCY_CODE,
				FC.strSymbol									AS CURRENCY_SYMBOL,
				U.strFullName 									AS CREATED_BY_NAME,
				FCNH.STATUS										AS STATUS,
				(SELECT 
					SUM(FCND.VALUE)
				 FROM finance_customer_credit_note_detail FCND 
				 WHERE 
				 		FCND.CREDIT_NO = FCNH.CREDIT_NO
				 	AND FCND.CREDIT_YEAR = FCNH.CREDIT_YEAR)	AS VALUE,
				FCNH.REMARKS									AS REMARKS
				FROM 	
				finance_customer_credit_note_header FCNH
				INNER JOIN finance_customer_invoice_header FCIH ON FCIH.SERIAL_NO = FCNH.INVOICE_NO AND FCIH.SERIAL_YEAR = FCNH.INVOICE_YEAR
				INNER JOIN mst_customer CU ON FCNH.CUSTOMER_ID = CU.intId
				INNER JOIN mst_financecurrency FC ON FCNH.CURRENCY_ID = FC.intId
				INNER JOIN sys_users U ON FCNH.CREATED_BY = U.intUserId
				LEFT JOIN mst_country CO ON CO.intCountryID = CU.intCountryId
				WHERE 
						FCNH.CREDIT_NO 	= '$serialNo'
					AND FCNH.CREDIT_YEAR 	= '$serialYear'";
					//die($sql);
		$result = $this->db->RunQuery($sql);
		return mysqli_fetch_array($result);	
	}
	
	private function GetReportDetails_sql($serialNo,$serialYear)
	{
		$sql = "SELECT 
				OD.strSalesOrderNo								AS SALES_ORDER_NO,
				OD.strGraphicNo									AS GRAPHIC_NO,
				OD.strStyleNo 									AS STYLE_NO,
				P.strName										AS PART,
				FCND.QTY										AS INVOICE_QTY,	
				FCND.PRICE 										AS INVOICE_PRICE,
				FCND.VALUE 										AS INVOICE_VALUE,
				FCND.TAX_ID 									AS TAX_CODE_ID,
				FCND.COST_CENTER_ID								AS COST_CENTER_ID,
				FCND.TAX_VALUE 									AS TAX_VALUE,
				FUI.strName										AS ITEM_NAME,
				'PCS'											AS UNIT	
				FROM finance_customer_credit_note_detail FCND
				INNER JOIN finance_customer_credit_note_header FCNH ON FCNH.CREDIT_NO = FCND.CREDIT_NO AND FCNH.CREDIT_YEAR = FCND.CREDIT_YEAR
				INNER JOIN trn_orderdetails OD ON OD.intOrderNo = FCNH.ORDER_NO AND OD.intOrderYear = FCNH.ORDER_YEAR AND OD.intSalesOrderId = FCND.SALES_ORDER_ID
				INNER JOIN mst_part P ON P.intId = OD.intPart
				INNER JOIN mst_financecustomeritem FUI ON FUI.intId = FCND.GL_ACCOUNT
				WHERE FCND.CREDIT_NO = '$serialNo' AND FCND.CREDIT_YEAR = '$serialYear'";
				//die($sql);
		return $this->db->RunQuery($sql);
	}
}
?>
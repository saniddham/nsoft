<?php
include  "../../../../class/cls_commonFunctions_get.php";
include  "../../../../class/finance/customer/cls_common_get.php";
$obj_common			= new cls_commonFunctions_get($db);
$obj_common_get		= new Cls_Common_Get($db);
$session_userId		= $_SESSION["userId"];
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];

class Cls_Credit_Note_Set
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}	
	
	public function Save($customerId,$invoiceNo,$orderNo,$currencyId,$ledgerId,$date,$remarks,$details,$GLGrid,$invoiceType,$subTotal,$grandTotal)
	{
		global $obj_common;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_locationId;
		$savedStatus = true;
		
		$this->db->begin();
		$credit_array 	= $obj_common->GetSystemMaxNo('intFin_CreditNoteNo',$session_locationId);
		$creditNo		= $credit_array["max_no"];
		$creditYear		= date('Y');
		$invoice_array	= explode('/',$invoiceNo);
		$order_array	= explode('/',$orderNo);
		
		$this->SaveHeader($creditNo,$creditYear,$customerId,$invoiceNo,$orderNo,$currencyId,$ledgerId,$date,$remarks);
		
		foreach($details as $array_loop)
		{
			$salesOrderId		= $array_loop["SalesOrderId"];
			$qty				= $array_loop["Qty"];
			$price				= $array_loop["Price"];
			$tax_Id				= $array_loop["Tax"];
			$costCenter_Id		= $array_loop["CostCenter"];
			$gl_id				= $array_loop["GL_ID"];
			$taxValue			= $array_loop["TaxValue"];

			$value				= $qty * $price;
			
			$this->SaveDetails($creditNo,$creditYear,$salesOrderId,$qty,$price,$tax_Id,$costCenter_Id,$gl_id,$taxValue);
		}
		
/*		$loop				= 0;
		foreach($GLGrid as $array_loop)
		{
			$gl_id				= $array_loop["GL_ID"];
			$gl_amount			= $array_loop["GL_Amount"];
			$loop++;
			$this->SaveGL($creditNo,$creditYear,$gl_id,$gl_amount,$loop);
			$this->SaveTransaction($creditNo,$creditYear,$invoice_array[1],$invoice_array[0],$gl_amount,$order_array[1],$order_array[0],$customerId,$currencyId,$gl_id);
		}*/
		
		//
		$this->GLAutomateTransaction($creditNo,$creditYear,$invoiceType,$customerId,$subTotal,$grandTotal,$details,$order_array[1],$order_array[0],$currencyId,$invoice_array[1],$invoice_array[0],$date);
		//
		
		$resultArr	= $obj_common->validateDuplicateSerialNoWithSysNo($creditNo,'intFin_CreditNoteNo',$session_locationId);
		if($resultArr['type']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged	= $resultArr['msg'];
		}
		
		if($savedStatus){
			$this->db->commit();
			$response['type'] 		 = "pass";
			$response['msg'] 		 = "Invoiced No : '$invoiceNo' saved successfully.";
			$response['credit_no'] 	 = $creditNo;
			$response['credit_year'] = $creditYear;
		}else{
			$this->db->rollback();
			$response['type'] 		 = "fail";
			$response['msg'] 		 = $savedMasseged;
			$response['sql'] 		 = $error_sql;
		}
		return json_encode($response);	
	}
	
	private function SaveHeader($creditNo,$creditYear,$customerId,$invoiceNo,$orderNo,$currencyId,$ledgerId,$date,$remarks)
	{
		global $session_userId;
		global $session_locationId;
		global $session_companyId;
		global $savedStatus;
		global $savedMasseged;
		
		$invoice_array	= explode('/',$invoiceNo);
		$order_array	= explode('/',$orderNo);
		
		$sql = "INSERT INTO finance_customer_credit_note_header 
				(CREDIT_NO, 
				CREDIT_YEAR, 
				CUSTOMER_ID, 
				INVOICE_NO, 
				INVOICE_YEAR, 
				ORDER_NO, 
				ORDER_YEAR, 
				CURRENCY_ID, 
				LEDGER_ID, 
				REMARKS, 
				CREDIT_DATE,
				STATUS,
				COMPANY_ID, 
				LOCATION_ID, 
				CREATED_BY, 
				CREATED_DATE)
				VALUES
				('$creditNo', 
				'$creditYear', 
				'$customerId', 
				'$invoice_array[0]', 
				'$invoice_array[1]', 
				'$order_array[1]', 
				'$order_array[0]', 
				'$currencyId', 
				'$ledgerId', 
				'$remarks', 
				'$date',
				'1',
				'$session_companyId', 
				'$session_locationId', 
				'$session_userId', 
				NOW());";
		$result = $this->db->RunQuery2($sql);
		if(!$result && $savedStatus){
			$savedStatus	= false;
			$savedMasseged	= $this->db->errormsg;
		} 	 	
	}
	
	private function SaveDetails($creditNo,$creditYear,$salesOrderId,$qty,$price,$tax_Id,$costCenter_Id,$gl_id,$taxValue)
	{
		global $savedStatus;
		global $savedMasseged;

		$sql = "INSERT INTO finance_customer_credit_note_detail 
				(CREDIT_NO, 
				CREDIT_YEAR, 
				SALES_ORDER_ID, 
				GL_ACCOUNT,
				QTY, 
				PRICE, 
				VALUE,
				TAX_VALUE, 
				TAX_ID, 
				COST_CENTER_ID)
				VALUES
				('$creditNo', 
				'$creditYear', 
				'$salesOrderId',
				'$gl_id',
				'$qty', 
				'$price', 
				$qty * $price,
				$taxValue,
				$tax_Id, 
				'$costCenter_Id');";
		$result = $this->db->RunQuery2($sql);
		if(!$result && $savedStatus){
			$savedStatus	= false;
			$savedMasseged	= $this->db->errormsg;
		} 	 	
	}
	
	private function SaveTransaction($receiptNo,$receiptYear,$invoiceYear,$invoiceNo,$value,$orderNo,$orderYear,$customerId,$currencyId,$ledgerId,$date)
	{
		global $session_userId;
		global $session_locationId;
		global $session_companyId;
		global $savedStatus;
		global $savedMasseged;
		
		$sql = "INSERT INTO finance_customer_transaction 
				(ORDER_YEAR, 
				ORDER_NO, 
				CUSTOMER_ID, 
				CURRENCY_ID, 
				DOCUMENT_YEAR, 
				DOCUMENT_NO, 
				DOCUMENT_TYPE, 
				INVOICE_YEAR, 
				INVOICE_NO, 
				LEDGER_ID, 
				VALUE, 
				COMPANY_ID, 
				LOCATION_ID, 
				USER_ID, 
				TRANSACTION_DATE_TIME,
				SYSTEM_MODIFIED_DATE)
				VALUES
				('$orderYear', 
				'$orderNo', 
				'$customerId', 
				'$currencyId', 
				'$receiptYear', 
				'$receiptNo', 
				'CREDIT', 
				'$invoiceYear', 
				'$invoiceNo', 
				'$ledgerId', 
				'-$value', 
				'$session_companyId', 
				'$session_locationId', 
				'$session_userId',
				'$date',
				NOW());";
		$result = $this->db->RunQuery2($sql);
		if(!$result && $savedStatus){
			$savedStatus	= false;
			$savedMasseged	= $this->db->errormsg;
		}
	}
	
	private function SaveGL($creditNo,$creditYear,$gl_id,$gl_amount,$loop)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;

		$sql = "INSERT INTO finance_customer_credit_note_gl 
				(CREDIT_NO, 
				CREDIT_YEAR, 
				GL_ACCOUNT, 
				GL_AMOUNT,
				ORDER_BY_ID)
				VALUES
				('$creditNo', 
				'$creditYear', 
				'$gl_id', 
				'$gl_amount',
				'$loop');";
		$result = $this->db->RunQuery2($sql);
		if(!$result && $savedStatus){
			$savedStatus	= false;
			$savedMasseged	= $this->db->errormsg;
			$error_sql		= $sql;
		}
	}
	
	private function GLAutomateTransaction($serialNo,$serialYear,$invoiceType,$customerId,$subTotal,$grandTotal,$grid,$orderNo,$orderYear,$currencyId,$invoiceYear,$invoiceNo,$date)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $obj_common_get;
		
		switch($invoiceType){
			case 1:
				//BEGIN - SAVING CUSTOMER WISE GL {
				$glId		= $obj_common_get->GetCustomerGL($customerId);					
				if($glId['type']=='fail' && $savedStatus)
				{
					$savedStatus	= false;
					$savedMasseged	= $glId['ErrorMsg'];
					return;
				}		
				$response 	= $obj_common_get->Save_Jurnal_Transaction($glId,$amount=$grandTotal,$documentNo=$serialNo,$documentYear=$serialYear,$documentType='CREDIT',$invoiceNo,$invoiceYear,$transType='C',$transCategory='CU',$customerId,$currencyId,'',$date);
				if($response['type']=='fail' && $savedStatus)
				{
					$savedStatus	= false;
					$savedMasseged	= $response['ErrorMsg'];
					$error_sql		= $response['ErrorSql'];
					return;
				}
				//END 	- SAVING CUSTOMER WISE GL }
				
				//BEGIN - SAVING INVOICE TYPE GL {
				$glId		= $obj_common_get->GetInvoiceTypeGL($invoiceType);
				if($glId['type']=='fail' && $savedStatus)
				{
					$savedStatus	= false;
					$savedMasseged	= $glId['ErrorMsg'];
					return;
				}				
				$response 	= $obj_common_get->Save_Jurnal_Transaction($glId,$amount=$grandTotal,$documentNo=$serialNo,$documentYear=$serialYear,$documentType='CREDIT',$invoiceNo,$invoiceYear,$transType='D',$transCategory='CU',$customerId,$currencyId,'',$date);
				$this->SaveTransaction($serialNo,$serialYear,$invoiceYear,$invoiceNo,$grandTotal,$orderNo,$orderYear,$customerId,$currencyId,$glId,$date);
				if($response['type']=='fail' && $savedStatus)
				{
					$savedStatus	= false;
					$savedMasseged	= $response['ErrorMsg'];
					$error_sql		= $response['ErrorSql'];
					return;
				}
				//END - SAVING INVOICE TYPE GL }
				break;
				
			case 2:
				//BEGIN - SAVING CUSTOMER WISE GL {
				$glId		= $obj_common_get->GetCustomerGL($customerId);	
				if($glId['type']=='fail' && $savedStatus)
				{
					$savedStatus	= false;
					$savedMasseged	= $glId['ErrorMsg'];
					return;
				}			
				$response 	= $obj_common_get->Save_Jurnal_Transaction($glId,$amount=$grandTotal,$documentNo=$serialNo,$documentYear=$serialYear,$documentType='CREDIT',$invoiceNo,$invoiceYear,$transType='C',$transCategory='CU',$customerId,$currencyId,'',$date);
				if($response['type']=='fail' && $savedStatus)
				{
					$savedStatus	= false;
					$savedMasseged	= $response['ErrorMsg'];
					$error_sql		= $response['ErrorSql'];
				}
				//END  - SAVING CUSTOMER WISE GL }
				
				//BEGIN - SAVING TAX GL {
				foreach($grid as $loop)
				{
					$glId		= $obj_common_get->GetTaxGL($loop["Tax"]);
					if($glId['type']=='fail' && $savedStatus)
					{
						$savedStatus	= false;
						$savedMasseged	= $glId['ErrorMsg'];
						return; 
					}
					$response 	= $obj_common_get->Save_Jurnal_Transaction($glId,$amount=$loop["TaxValue"],$documentNo=$serialNo,$documentYear=$serialYear,$documentType='CREDIT',$invoiceNo,$invoiceYear,$transType='D',$transCategory='CU',$customerId,$currencyId,'',$date);
					$this->SaveTransaction($serialNo,$serialYear,$invoiceYear,$invoiceNo,$loop["TaxValue"],$orderNo,$orderYear,$customerId,$currencyId,$glId,$date);
					if($response['type']=='fail' && $savedStatus)
					{
						$savedStatus	= false;
						$savedMasseged	= $response['ErrorMsg'];
						$error_sql		= $response['ErrorSql'];
						return;
					}
				}
				//END 	- SAVING TAX GL	}
				
				//BEGIN - SAVING INVOICE TYPE GL {
				$glId		= $obj_common_get->GetInvoiceTypeGL($invoiceType);
				if($glId['type']=='fail' && $savedStatus)
				{
					$savedStatus	= false;
					$savedMasseged	= $glId['ErrorMsg'];
					return;
				}
				$response 	= $obj_common_get->Save_Jurnal_Transaction($glId,$amount=$subTotal,$documentNo=$serialNo,$documentYear=$serialYear,$documentType='CREDIT',$invoiceNo,$invoiceYear,$transType='D',$transCategory='CU',$customerId,$currencyId,'',$date);
				$this->SaveTransaction($serialNo,$serialYear,$invoiceYear,$invoiceNo,$subTotal,$orderNo,$orderYear,$customerId,$currencyId,$glId,$date);
				if($response['type']=='fail' && $savedStatus)
				{
					$savedStatus	= false;
					$savedMasseged	= $response['ErrorMsg'];
					$error_sql		= $response['ErrorSql'];
					return;
				}
				//END 	- SAVING INVOICE TYPE GL }
				break;
				
			case 3:
				//BEGIN - SAVING CUSTOMER WISE GL {
				$glId		= $obj_common_get->GetCustomerGL($customerId);
				if($glId['type']=='fail' && $savedStatus)
				{
					$savedStatus	= false;
					$savedMasseged	= $glId['ErrorMsg'];
					return;
				}				
				$response 	= $obj_common_get->Save_Jurnal_Transaction($glId,$amount=$grandTotal,$documentNo=$serialNo,$documentYear=$serialYear,$documentType='CREDIT',$invoiceNo,$invoiceYear,$transType='C',$transCategory='CU',$customerId,$currencyId,'',$date);
				if($response['type']=='fail' && $savedStatus)
				{
					$savedStatus	= false;
					$savedMasseged	= $response['ErrorMsg'];
					$error_sql		= $response['ErrorSql'];
					return;
				}
				//END 	- SAVING CUSTOMER WISE GL }
				
				//BEGIN - SAVING INVOICE TYPE GL {
				$glId		= $obj_common_get->GetInvoiceTypeGL($invoiceType);
				if($glId['type']=='fail' && $savedStatus)
				{
					$savedStatus	= false;
					$savedMasseged	= $glId['ErrorMsg'];
					return;
				}
				
				$response 	= $obj_common_get->Save_Jurnal_Transaction($glId,$amount=$subTotal,$documentNo=$serialNo,$documentYear=$serialYear,$documentType='CREDIT',$invoiceNo,$invoiceYear,$transType='D',$transCategory='CU',$customerId,$currencyId,'',$date);
				$this->SaveTransaction($serialNo,$serialYear,$invoiceYear,$invoiceNo,$subTotal,$orderNo,$orderYear,$customerId,$currencyId,$glId,$date);
				if($response['type']=='fail' && $savedStatus)
				{
					$savedStatus	= false;
					$savedMasseged	= $response['ErrorMsg'];
					$error_sql		= $response['ErrorSql'];
					return;
				}
				//END 	- SAVING INVOICE TYPE GL }
				break;
		}

	}
}
?>
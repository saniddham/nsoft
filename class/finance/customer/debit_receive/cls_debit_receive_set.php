<?php
include_once  $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
include_once  $_SESSION['ROOT_PATH']."class/finance/customer/cls_common_get.php";
include_once  $_SESSION['ROOT_PATH']."class/finance/customer/advance/cls_advance_set.php";

$obj_common			= new cls_commonFunctions_get($db);
$obj_common_get		= new Cls_Common_Get($db);
$obj_advance_set	= new Cls_Advance_Set($db);

$session_userId		= $_SESSION["userId"];
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];

class Cls_Debit_Receive_Set
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	
	public function Save($customerId,$currencyId,$remarks,$detailArray,$glArray,$receiptDate,$paymentMode)
	{
		global $obj_common;
		global $booSavedStatus;
		global $savedMasseged;
		global $session_locationId;
		global $pub_ErrorSql;
		global $obj_common_get;
		global $obj_advance_set;
		
		$booSavedStatus	= true;
		
		$this->db->begin();
		$serial_array 	= $obj_common->GetSystemMaxNo('DEBIT_RECEIVE_NO',$session_locationId);
		
		if($serial_array['rollBackFlag']==1)
		{
			$booSavedStatus		= false;
			$savedMasseged		= $serial_array["msg"];
			$pub_ErrorSql		= $serial_array["q"];
		}
		
		$receiptNo		= $serial_array["max_no"];
		$receiptYear	= date('Y');
		
		$this->checkExchangeRateAvailable($currencyId,$receiptDate);
		$this->SaveHeader($receiptNo,$receiptYear,$customerId,$currencyId,$remarks,$receiptDate,$paymentMode);
		
		foreach($detailArray as $detailLoop)
		{
			$debitNo_array		= explode('/',$detailLoop["debitNo"]);
			$debitYear			= $debitNo_array[0];
			$debitNo			= $debitNo_array[1];
			$payAmount			= $detailLoop["PayAmount"];
			$toBePayAmount		= $detailLoop["ToBeReceive"];
			
			$this->ValidateDetailSaving($customerId,$currencyId,$debitYear,$debitNo,$payAmount);
			$this->SaveDetails($receiptNo,$receiptYear,$debitYear,$debitNo,$payAmount);
			
			
			//BEGIN - INSERT INTO FINANCE_CUSTOMER_TRANSACTION TABLE {						
			foreach($glArray as $arrValGL)
			{
				if($this->CheckIsCustomerGL($arrValGL['GLAccount'],$customerId))
					$customerGLId	= $arrValGL['GLAccount'];
			}
			//$invoiceValue 		= ($detailLoop["ToBeReceive"]<$detailLoop['PayAmount']?$detailLoop["ToBeReceive"]:$detailLoop['PayAmount']);
			$debitValue				= $detailLoop['PayAmount'];
			$this->SaveTransaction($receiptNo,$receiptYear,$debitNo,$debitYear,$debitValue,$customerId,$currencyId,$customerGLId,'DEBIT_RECEIVE',$receiptDate);			
			//END	- }
			
		}
		
		//BEGIN - INSET RECORDS TO FINANCE_CUSTOMER_PAY_RECEIVE_GL TABLE {
		foreach($glArray as $glLoop)
		{
			$value				= ($glLoop["GLValue_DR"]=='0'?$glLoop["GLValue_CR"]:$glLoop["GLValue_DR"]);
			
			$this->SaveGLDetails($receiptNo,$receiptYear,$glLoop["GLAccount"],$glLoop["GLCostCenter"],$glLoop["GLRemarks"],$value,$glLoop["TransType"]);
			
			//BEGIN - UPDATE CUSTOMER RECEIVE AMOUNT INTO FORECAST TABLE {					
			$response 			= $obj_common_get->UpdateForecast($value,$currencyId,$receiptDate,$customerId,$glLoop["GLAccount"]);
			if(!$response["type"] && $booSavedStatus)
			{
				$booSavedStatus	= false;
				$savedMasseged 	= $response['msg'];
				$pub_ErrorSql 	= $response['sql'];				
			}
			//END	- UPDATE CUSTOMER RECEIVE AMOUNT INTO FORECAST TABLE }
		}
		//END - }
		
		//BEGIN - INSERT RECORDS TO FINANCE_TRANSACTION TABLE {
		foreach($glArray as $glLoop)
		{
			$value				= ($glLoop["GLValue_DR"]=='0'?$glLoop["GLValue_CR"]:$glLoop["GLValue_DR"]);
			$response 			= $obj_common_get->Save_Jurnal_Transaction($glLoop["GLAccount"],$value,$receiptNo,$receiptYear,'DEBIT_RECEIVE','null','null',$glLoop["TransType"],'CU',$customerId,$currencyId,$glLoop["GLRemarks"],$receiptDate);
			if($response['type']=='fail' && $booSavedStatus)
			{
				$booSavedStatus	= false;
				$savedMasseged	= $response['ErrorMsg'];
				$pub_ErrorSql	= $response['ErrorSql'];
			}
		}
		//END - }

		//BEGIN - Cr {
		/*	
		$i			  = 0;
		$finalInvBal  = 0;
		$glBal		  = 0;
		foreach($glArray as $arrValGL)
		{
			if(!$this->CheckIsCustomerGL($arrValGL['GLAccount'],$customerId))
				continue;
				
			$accountId 		= $arrValGL['GLAccount'];
			$glValue 		= ($arrValGL["GLValue_DR"]=='0'?$arrValGL["GLValue_CR"]:$arrValGL["GLValue_DR"]);
			$glBal   		= $glValue;
			$n				= 0;
			
			foreach($detailArray as $arrVal)
			{
				$invoiceNoArr 	= explode('/',trim($arrVal['InvoiceNo']));
				$PONoArr 		= explode('/',trim($arrVal['OrderNo']));
				$PONo			= $PONoArr[1];
				$POYear			= $PONoArr[0];
				$invoiceNo		= $invoiceNoArr[1];
				$invoiceYear	= $invoiceNoArr[0];
				$invValue 		= ($detailLoop["ToBeReceive"]<$detailLoop['PayAmount']?$detailLoop["ToBeReceive"]:$detailLoop['PayAmount']);
				
				if(++$n<$i)
				{
						
				}
				else
				{
					if($finalInvBal>0)
					{
						$invValue = $finalInvBal;
					}
					else
					{
						$i++;
					}
					if($glBal>$invValue)
					{
						$this->SaveTransaction($receiptNo,$receiptYear,$invoiceYear,$invoiceNo,$invValue,$PONo,$POYear,$customerId,$currencyId,$accountId,$arrVal['Type'],$receiptDate);
						$glBal			-= $invValue;
						$finalInvBal = 0;
					}
					else
					{
						$this->SaveTransaction($receiptNo,$receiptYear,$invoiceYear,$invoiceNo,$glBal,$PONo,$POYear,$customerId,$currencyId,$accountId,$arrVal['Type'],$receiptDate);
						$finalInvBal 	= $invValue - $glBal;
						$glBal 			= 0;
					}
				}
			}
		}
		*/
		//END	- }
		
		//BEGIN - SAVE PAYMENT ADVANCE TRANSACTION AUTOMATICALLY WHEN ADVANE AMOUNT AVAILABEL {
			//========================= comment by lahiru due to exceed received settlement (2015-07-03) =======================================
			/*if($advanceAmount>0)
			{				
				$no_array 		= $obj_common->GetSystemMaxNo('intFin_Customer_AdvanceNo',$session_locationId);
				$advanceNo		= $no_array["max_no"];
				$advanceYear	= date('Y');
				
				$obj_advance_set->SaveHeader($advanceNo,$advanceYear,$customerId,'null','null',$currencyId,$paymentMode,'null',$remarks,$receiptDate,'null');
				
				foreach($glArray as $glLoop)
				{
					if($this->CheckIsBankGL($glLoop["GLAccount"]))
					{
						$obj_advance_set->SaveDetails($advanceNo,$advanceYear,$glLoop["GLAccount"],$advanceAmount,$glLoop["GLRemarks"],$glLoop["GLCostCenter"]);
						$obj_advance_set->SaveTransaction($advanceNo,$advanceYear,'null','null',$advanceAmount,'null','null',$customerId,$currencyId,$glLoop["GLAccount"],$receiptDate);						
					}						
				}				
			}*/
			//=======================================================================================================================
		//END	- }
		
		$resultArr	= $obj_common->validateDuplicateSerialNoWithSysNo($receiptNo,'DEBIT_RECEIVE_NO',$session_locationId);
		if($resultArr['type']=='fail' && $booSavedStatus)
		{
			$booSavedStatus	= false;
			$savedMasseged	= $resultArr['msg'];
		}

		if($booSavedStatus)
		{
			$this->db->commit();
			$response['type'] 			= "pass";
			$response['msg'] 			= "Debit Receipt No : '$receiptYear/$receiptNo' saved successfully.";
			$response['ReceiptNo'] 		= $receiptNo;
			$response['ReceiptYear']	= $receiptYear;
		}
		else
		{
			$this->db->rollback();
			$response['type'] 			= "fail";
			$response['msg'] 			= $savedMasseged;
			$response['errorSql']		= $pub_ErrorSql;
		}
		return json_encode($response);
	}
	
	public function Cancel($receiptNo,$receiptYear)
	{
		global $booSavedStatus;
		global $savedMasseged;
		global $pub_ErrorSql;		
		global $obj_common_get;
		
		$booSavedStatus	= true;
		$this->db->begin();
		
		$this->Cancel_sql($receiptNo,$receiptYear);
		
		$trans_response = $obj_common_get->RemoveTransaction($receiptNo,$receiptYear,'DEBIT_RECEIVE');
		if($trans_response["type"])
		{
			$booSavedStatus			= false;
			$savedMasseged			= $trans_response["msg"];
			$pub_ErrorSql			= $trans_response["errorSql"];
		}
		
		$trans_response = $obj_common_get->RemoveFinanceMainTransaction($receiptNo,$receiptYear,'DEBIT_RECEIVE');
		if($trans_response["type"])
		{
			$booSavedStatus			= false;
			$savedMasseged			= $trans_response["msg"];
			$pub_ErrorSql			= $trans_response["errorSql"];
		}		
		
		if($booSavedStatus){
			$this->db->commit();
			$response['type'] 		= "pass";
			$response['msg'] 		= "Canceled successfully.";
		}else{
			$this->db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= $savedMasseged;
			$response['errorSql']	= $pub_ErrorSql;
		}
		return json_encode($response);
	}
	public function UpdateStatus($serialNo,$serialYear)
	{
		$this->UpdateStatus_sql($serialNo,$serialYear);
	}
	public function confirmPaymnet($receiptNo,$receiptYear)
	{
		return $this->confirmPaymnet_sql($receiptNo,$receiptYear);
	}
	private function SaveHeader($receiptNo,$receiptYear,$customerId,$currencyId,$remarks,$receiptDate,$paymentMode)
	{
		global $session_companyId;
		global $session_locationId;
		global $session_userId;
		global $savedMasseged;
		global $booSavedStatus;
		global $pub_ErrorSql;
		
		$sql = "INSERT INTO finance_customer_debit_receive_header 
				(RECEIPT_NO, 
				RECEIPT_YEAR, 
				CUSTOMER_ID, 
				CURRENCY_ID, 
				PAYMENT_MODE, 
				REMARKS, 
				RECEIPT_DATE, 
				STATUS, 
				COMPANY_ID, 
				LOCATION_ID, 
				CREATED_BY, 
				CREATED_DATE
				)
				VALUES
				('$receiptNo', 
				'$receiptYear', 
				'$customerId', 
				'$currencyId', 
				'$paymentMode', 
				'$remarks', 
				'$receiptDate', 
				'1', 
				'$session_companyId', 
				'$session_locationId', 
				'$session_userId', 
				NOW()
				);";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result && $booSavedStatus)
		{
			$booSavedStatus		= false;
			$savedMasseged		= $this->db->errormsg;
			$pub_ErrorSql		= $sql;
		}
	}
	
	private function SaveDetails($receiptNo,$receiptYear,$debitYear,$debitNo,$payAmount)
	{
		global $savedMasseged;
		global $booSavedStatus;
		global $pub_ErrorSql;
		
		$sql = "INSERT INTO finance_customer_debit_receive_details 
				(
				RECEIPT_NO, 
				RECEIPT_YEAR, 
				DEBIT_NO, 
				DEBIT_YEAR, 
				PAY_AMOUNT
				)
				VALUES
				(
				'$receiptNo', 
				'$receiptYear', 
				'$debitNo', 
				'$debitYear', 
				'$payAmount'
				) ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result && $booSavedStatus)
		{
			$booSavedStatus		= false;
			$savedMasseged		= $this->db->errormsg;
			$pub_ErrorSql		= $sql;
		}
	}
	
	private function SaveGLDetails($receiptNo,$receiptYear,$ledgerId,$costCenter,$remarks,$value,$transType)
	{
		global $savedMasseged;
		global $booSavedStatus;
		global $pub_ErrorSql;
		
		$sql = "INSERT INTO finance_customer_debit_receive_gl
				(
				RECEIPT_NO, 
				RECEIPT_YEAR, 
				LEDGER_ID, 
				COST_CENTER_ID, 
				REMARKS, 
				PAY_AMOUNT, 
				PAY_AMOUNT_BALANCE, 
				TRANSACTION_TYPE
				)
				VALUES 
				(
				'$receiptNo',
				'$receiptYear',
				'$ledgerId',
				'$costCenter',
				'$remarks',
				'$value',
				'$value',
				'$transType'
				);";
		$result = $this->db->RunQuery2($sql);
		if(!$result && $booSavedStatus)
		{
			$booSavedStatus	= false;
			$savedMasseged	= $this->db->errormsg;
			$pub_ErrorSql		= $sql;
		}
	}
	
	private function SaveTransaction($receiptNo,$receiptYear,$debitNo,$debitYear,$payAmount,$customerId,$currencyId,$ledgerId,$type,$receiptDate)
	{
		global $savedMasseged;
		global $booSavedStatus;
		global $session_companyId;
		global $session_locationId;
		global $session_userId;
		global $pub_ErrorSql;
		
		if($payAmount<=0)
			return;
		
		$sql = "INSERT INTO finance_customer_transaction 
				(
				CUSTOMER_ID, 
				CURRENCY_ID, 
				DOCUMENT_YEAR, 
				DOCUMENT_NO, 
				DOCUMENT_TYPE, 
				DEBIT_NO,
				DEBIT_YEAR,
				LEDGER_ID, 
				VALUE, 
				COMPANY_ID, 
				LOCATION_ID, 
				USER_ID, 
				TRANSACTION_DATE_TIME,
				SYSTEM_MODIFIED_DATE)
				VALUES
				(
				'$customerId', 
				'$currencyId', 
				'$receiptYear', 
				'$receiptNo', 
				'$type', 
				'$debitNo', 
				'$debitYear', 
				'$ledgerId', 
				'-$payAmount', 
				'$session_companyId', 
				'$session_locationId', 
				'$session_userId',
				'$receiptDate',
				NOW()
				);";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result && $booSavedStatus)
		{
			$booSavedStatus		= false;
			$savedMasseged		= $this->db->errormsg;
			$pub_ErrorSql		= $sql;
		}
	}
	
	private function ValidateDetailSaving($customerId,$currencyId,$debitYear,$debitNo,$payAmount)
	{
		global $savedMasseged;
		global $booSavedStatus;
		global $pub_ErrorSql;
		global $session_companyId;
		
		$sql = "SELECT ROUND(SUM(VALUE),2) AS balAmount
				FROM finance_customer_transaction
				WHERE
				CUSTOMER_ID = '$customerId' AND
				CURRENCY_ID = '$currencyId' AND
				DEBIT_NO = '$debitNo' AND
				DEBIT_YEAR = '$debitYear' ";
				
		$result 	= $this->db->RunQuery2($sql);
		$row		= mysqli_fetch_array($result);
		$toBePaid 	= $row['balAmount'];
		//die($payAmount .'|'. $toBePaid);

		if($toBePaid < $payAmount && $booSavedStatus)
		{
			$booSavedStatus		= false;
			$savedMasseged		= "No balance amount available for Debit No : $debitNo/$debitYear";
			$pub_ErrorSql		= $sql;
		}
	}
	
	private function Cancel_sql($receiptNo,$receiptYear)
	{
		global $booSavedStatus;
		global $savedMasseged;
		global $pub_ErrorSql;
		
		$sql = "UPDATE finance_customer_debit_receive_header
				SET STATUS = '10'
				WHERE RECEIPT_NO = '$receiptNo'
					AND RECEIPT_YEAR = '$receiptYear';";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$booSavedStatus		= false;
			$savedMasseged		= $this->db->errormsg;
			$pub_ErrorSql		= $sql;
		}
		
	}
	
	private function CheckIsBankGL($glAccount)
	{
		$sql = "SELECT CHART_OF_ACCOUNT_ID FROM finance_mst_chartofaccount WHERE CHART_OF_ACCOUNT_ID = '$glAccount' AND BANK = 1";
		$result = $this->db->RunQuery2($sql);
		if(mysqli_fetch_row($result)>0)
			return true;
		else
			return false;
	}
	
	private function CheckIsCustomerGL($glId,$customerId)
	{
		$sql = "SELECT CHART_OF_ACCOUNT_ID FROM finance_mst_chartofaccount WHERE CATEGORY_TYPE = 'C' AND CATEGORY_ID = '$customerId'";
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		if($row["CHART_OF_ACCOUNT_ID"] == $glId)
			return true;
		else
			return false;
	}
	private function UpdateStatus_sql($serialNo,$serialYear)
	{
		$sql = "UPDATE finance_customer_pay_receive_header 
				SET
				PRINT_STATUS = '1'
				WHERE
				RECEIPT_NO = '$serialNo' AND 
				RECEIPT_YEAR = '$serialYear' ";
		$this->db->RunQuery($sql);
	}
	private function confirmPaymnet_sql($receiptNo,$receiptYear)
	{
		$sql	= "UPDATE finance_customer_pay_receive_header 
					SET
					RECEIPT_CONFIRMATION = '1'
					WHERE
					RECEIPT_NO = '$receiptNo' AND 
					RECEIPT_YEAR = '$receiptYear' ";
					
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function checkExchangeRateAvailable($currencyId,$receiptDate)
	{
		global $savedMasseged;
		global $booSavedStatus;
		global $pub_ErrorSql;
		global $session_companyId;
		
		$sql = "SELECT * FROM mst_financeexchangerate 
				WHERE dtmDate = '$receiptDate' AND 
				intCompanyId = '$session_companyId' AND
				intCurrencyId = '$currencyId' ";
		
		$result = $this->db->RunQuery2($sql);
		if(mysqli_num_rows($result)<=0 && $booSavedStatus)
		{
			$booSavedStatus 		= false;
			$savedMasseged			= "Please enter exchange rates for '".$receiptDate."' before save.";
		}
	}
}
?>
<?php
include_once ($_SESSION['ROOT_PATH']."class/masterData/exchange_rate/cls_exchange_rate_get.php");
include_once ($_SESSION['ROOT_PATH']."class/finance/cash_flow_forecast/cls_cash_flow_forecast_get.php");
include_once ($_SESSION['ROOT_PATH']."class/finance/cash_flow_forecast/cls_cash_flow_forecast_set.php");

$objExchangeRateGet 		= new cls_exchange_rate_get($db);
$objCashFlowForecastGet		= new cls_cash_flow_forecast_get($db);
$objCashFlowForecastSet		= new cls_cash_flow_forecast_set($db);

$session_userId		= $_SESSION["userId"];
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];

class Cls_Common_Get
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
//BEGIN - PUBLIC FUNCTIONS {	
	public function Create_Ledger_HTML($id,$emptyRow)
	{
		$string = "";
		if($emptyRow)
			$string .= "<option value=\""."null"."\" selected=\"selected\">&nbsp;</option>";
		$result = $this->Get_Ledger();
		while($row = mysqli_fetch_array($result))
		{
			if($row["VALUE"]==$id)
				$string .= "<option value=\"".$row["VALUE"]."\" selected=\"selected\">".$row["TEXT"]."</option>";
			else
				$string .= "<option value=\"".$row["VALUE"]."\">".$row["TEXT"]."</option>";
		}
		return  $string;		
	}
	
	public function RemoveTransaction($documentNo,$documentYear,$documentType)
	{
		$this->RemoveTransaction_sql($documentNo,$documentYear,$documentType);
	}
	
	public function RemoveFinanceMainTransaction($documentNo,$documentYear,$documentType)
	{
		$this->RemoveFinanceMainTransaction_sql($documentNo,$documentYear,$documentType);
	}
	
	public function Get_InvoiceType_GLID()
	{
		return $this->Get_InvoiceType_GLID_sql();
	}
	
	public function Save_Jurnal_Transaction($glId,$amount,$documentNo,$documentYear,$documentType,$invoiceNo,$invoiceYear,$transType,$transCategory,$transCatId,$currencyId,$remarks,$invoicedDate)
	{
		return $this->Save_Jurnal_Transaction_sql($glId,$amount,$documentNo,$documentYear,$documentType,$invoiceNo,$invoiceYear,$transType,$transCategory,$transCatId,$currencyId,$remarks,$invoicedDate);
	}
	
	public function GetCustomerGL($customerId)
	{
		return $this->GetCustomerGL_sql($customerId);
	}
	
	public function GetInvoiceTypeGL($invoiceType)
	{
		return $this->GetInvoiceTypeGL_sql($invoiceType);
	}
	
	public function GetTaxGL($taxId)
	{
		return $this->GetTaxGL_sql($taxId);
	}
	
	public function SaveForecast($amount,$orderNo,$orderYear,$currencyId,$invoicedDate,$bankId)
	{
		return $this->SaveForecast_sql($amount,$orderNo,$orderYear,$currencyId,$invoicedDate,$bankId);
	}
	
	public function UpdateForecast($amount,$currencyId,$invoicedDate,$customerId,$bankId)
	{
		return $this->UpdateForecast_sql($amount,$currencyId,$invoicedDate,$customerId,$bankId);
	}	
//END 	- PUBLIC FUNCTIONS }

//BEGIN	- PRIVATE FUNCTIONS {	
	private function Get_Ledger()
	{
		global $session_companyId;
		
		$sql = "SELECT DISTINCT
				  FCOA.intId							AS VALUE,
				  CONCAT(FCOA.strCode,' - ',FCOA.strName)	AS TEXT
				FROM mst_financechartofaccounts FCOA
				  INNER JOIN mst_financecustomeractivate FCA
					ON FCOA.intId = FCA.intChartOfAccountId
				WHERE FCA.intCompanyId = '$session_companyId'
				GROUP BY FCOA.intId
				ORDER BY FCOA.strCode ASC";
		return $this->db->RunQuery($sql);
		
	}
	
	private function RemoveTransaction_sql($documentNo,$documentYear,$documentType)
	{
		$sql = "DELETE
				FROM finance_customer_transaction
				WHERE DOCUMENT_NO = '$documentNo'
					AND DOCUMENT_YEAR = '$documentYear'
					AND DOCUMENT_TYPE = '$documentType';";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$response["type"]		= 'fail';
			$response["msg"]		= $this->db->errormsg;
			$response["errorSql"]	= $sql;
		}
		else
			$response["type"]		= 'pass';
			
		return $response;	
	}
	
	private function RemoveFinanceMainTransaction_sql($documentNo,$documentYear,$documentType)
	{
		$sql = "DELETE
				FROM finance_transaction
				WHERE DOCUMENT_NO = '$documentNo'
					AND DOCUMENT_YEAR = '$documentYear'
					AND DOCUMENT_TYPE = '$documentType'
					AND TRANSACTION_CATEGORY = 'CU';";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$response["type"]		= 'fail';
			$response["msg"]		= $this->db->errormsg;
			$response["errorSql"]	= $sql;
		}
		else
			$response["type"]		= 'pass';
			
		return $response;	
	}
	
	private function Get_InvoiceType_GLID_sql()
	{
		$sql = "SELECT CHART_OF_ACCOUNT_ID FROM finance_mst_chartofaccount_invoice_type 
				WHERE INVOICE_TYPE_ID = 1";
		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		return $row["CHART_OF_ACCOUNT_ID"];
	}
	
	private function Save_Jurnal_Transaction_sql($glId,$amount,$documentNo,$documentYear,$documentType,$invoiceNo,$invoiceYear,$transType,$transCategory,$transCatId,$currencyId,$remarks,$invoicedDate)
	{
		global $session_companyId;
		global $session_locationId;
		global $session_userId;
		
		$sql = "INSERT INTO finance_transaction
				(CHART_OF_ACCOUNT_ID,
				AMOUNT,
				DOCUMENT_NO,
				DOCUMENT_YEAR,
				DOCUMENT_TYPE,
				INVOICE_NO,
				INVOICE_YEAR,
				TRANSACTION_TYPE,
				TRANSACTION_CATEGORY,
				TRANSACTION_CATEGORY_ID,
				CURRENCY_ID,
				REMARKS,
				LOCATION_ID,
				COMPANY_ID,
				LAST_MODIFIED_BY,
				LAST_MODIFIED_DATE)
				VALUES ('$glId',
				'$amount',
				'$documentNo',
				'$documentYear',
				'$documentType',
				$invoiceNo,
				$invoiceYear,
				'$transType',
				'CU',
				'$transCatId',
				'$currencyId',
				'$remarks',
				'$session_locationId',
				'$session_companyId',
				'$session_userId',
				'$invoicedDate');";				
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$response["type"]		= 'fail';
			$response["ErrorMsg"]	= $this->db->errormsg;
			$response["ErrorSql"]	= $sql;
		}
		return $response;
	}
	
	private function GetCustomerGL_sql($customerId)
	{
		$sql = "SELECT 
					CHART_OF_ACCOUNT_ID 
				FROM finance_mst_chartofaccount FCOA
				WHERE FCOA.CATEGORY_TYPE = 'C'
					AND CATEGORY_ID = '$customerId'";
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		$row_count	= mysqli_num_rows($result);
		if($row_count>0){
			return $row["CHART_OF_ACCOUNT_ID"];
		}else
		{
			$supplierName		= $this->GetPublicValues('strName','mst_customer','intId',$customerId);
			$response['type']		= 'fail';
			$response['ErrorMsg']	= "No GL Account available for Supplier : $supplierName";
			return $response;
		}
	}
	
	private function GetInvoiceTypeGL_sql($invoiceType)
	{
		$sql 	= " SELECT CHART_OF_ACCOUNT_ID 
					FROM finance_mst_chartofaccount_invoice_type 
					WHERE INVOICE_TYPE_ID = $invoiceType 
						AND TRANSACTION_CATEGORY = 'CU'";
		$result = $this->db->RunQuery2($sql);		
		$row_count	= mysqli_num_rows($result);
		$row	= mysqli_fetch_array($result);
		if($row_count>0){
			return $row["CHART_OF_ACCOUNT_ID"];
		}else
		{
			$invoiceTypeName		= $this->GetPublicValues('strInvoiceType','mst_invoicetype','intId',$invoiceType);
			$response['type']		= 'fail';
			$response['ErrorMsg']	= "No GL Account available for invoice type : $invoiceTypeName";
			return $response;
		}		
	}
	
	private function GetTaxGL_sql($taxId)
	{
		$sql 	= " SELECT 
						FCOAT.CHART_OF_ACCOUNT_ID
					FROM finance_mst_chartofaccount_tax FCOAT
					WHERE TAX_ID = $taxId";
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		$row_count	= mysqli_num_rows($result);
		if($row_count>0)
			return $row["CHART_OF_ACCOUNT_ID"];
		else
		{
			$taxCode				= $this->GetPublicValues('strCode','mst_financetaxgroup','intId',$taxId);
			$response['type']		= 'fail';
			$response['ErrorMsg']	= "No GL Account available for tax code : $taxCode";
			return $response;
		}
	}
	
	private function GetPublicValues($select_fieldName,$tableName,$where_fieldId,$where_fieldValue)
	{
		$sql = "SELECT $select_fieldName AS VALUE FROM $tableName WHERE $where_fieldId = '$where_fieldValue'";
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		return $row["VALUE"];
	}
	
	private function SaveForecast_sql($amount,$orderNo,$orderYear,$currencyId,$invoicedDate,$bankId)
	{
		global $session_companyId;
		global $objExchangeRateGet;
		
		$sourceExArray			= $objExchangeRateGet->GetAllValues($currencyId,2,$invoicedDate,$session_companyId,'RunQuery2');
		$targetExArray			= $objExchangeRateGet->GetAllValues(1,2,$invoicedDate,$session_companyId,'RunQuery2');
		$amount					= round(($amount/$targetExArray['AVERAGE_RATE'])*$sourceExArray['AVERAGE_RATE'],2);
		$result['type']			= true;	
		$array					= $this->GetLastDispatchDate($orderNo,$orderYear);
		$lastDispatchDate		= $array["LAST_DISPATCH_DATE"];
		$customerId				= $array["CUSTOMER_ID"];
		$currencyId				= 1;
		
		$sql = "SELECT 
					COUNT(*) AS COUNT 
				FROM finance_forecast_transaction_customer 
				WHERE 
					    DATE = '$lastDispatchDate'
					AND CATEGORY_ID = $customerId
					AND CURRENCY_ID = '$currencyId'
					AND BANK_ACCOUNT_ID = '$bankId'
					AND COMPANY_ID = $session_companyId";
		$result = $this->db->RunQuery2($sql);
		$row 	= mysqli_fetch_array($result);
		if($row["COUNT"]>0)
		{
			$sql = "UPDATE finance_forecast_transaction_customer
					SET AMOUNT = AMOUNT + $amount,
					  BALANCE_AMOUNT = BALANCE_AMOUNT + $amount
					WHERE DATE = '$lastDispatchDate' 
						AND CATEGORY_ID = $customerId
						AND CURRENCY_ID = $currencyId
						AND BANK_ACCOUNT_ID = '$bankId'
						AND COMPANY_ID = $session_companyId;";
			$result = $this->db->RunQuery2($sql);
			if(!$result)
			{
				$response["type"]	= false;
				$response["msg"]	= $this->db->errormsg;
				$response["sql"]	= $sql;
			}
			else
				$response["type"]	= true;
		}
		else
		{
			$sql = "INSERT INTO finance_forecast_transaction_customer 
					(DATE, 
					COMPANY_ID,
					BANK_ACCOUNT_ID,
					CATEGORY_ID,
					CURRENCY_ID,
					AMOUNT, 
					BALANCE_AMOUNT)
					VALUES
					('$lastDispatchDate', 
					'$session_companyId',
					'$bankId',
					'$customerId',
					'$currencyId',
					'$amount', 
					'$amount');";
			$result = $this->db->RunQuery2($sql);
			if(!$result)
			{
				$response["type"]	= false;
				$response["msg"]	= $this->db->errormsg;
				$response["sql"]	= $sql;
			}
			else
				$response["type"]	= true;
		}
		return $response;		
	}
	
	private function UpdateForecast_sql($amount,$currencyId,$date,$customerId,$bankId)
	{
		global $session_companyId;
		global $objCashFlowForecastSet;
		global $objExchangeRateGet;
		
		$sourceExArray			= $objExchangeRateGet->GetAllValues($currencyId,2,$date,$session_companyId,'RunQuery2');
		$targetExArray			= $objExchangeRateGet->GetAllValues(1,2,$date,$session_companyId,'RunQuery2');
		$amount					= round(($amount/$targetExArray['AVERAGE_RATE'])*$sourceExArray['AVERAGE_RATE'],2);
		$tblType				= "finance_forecast_transaction_customer";
		$balAmount				= $amount;
		$result['type']			= true;	
		$sql = "SELECT * 
				FROM finance_forecast_transaction_customer 
				WHERE BALANCE_AMOUNT >0
					AND CATEGORY_ID = $customerId
					AND CURRENCY_ID = '$currencyId'
					AND BANK_ACCOUNT_ID = '$bankId'
					AND COMPANY_ID = $session_companyId 
				ORDER BY DATE";
		$result_1 = $this->db->RunQuery2($sql);
		while($row = mysqli_fetch_array($result_1))
		{
			if($balAmount>0)
			{			
				$tblbalAmount	 = $row["BALANCE_AMOUNT"];
				if($balAmount>=$tblbalAmount)
				{
					$balAmount  -= $tblbalAmount;
					$result 	 = $objCashFlowForecastSet->UpdateCustomerForcastMinus($row["DATE"],$row["CATEGORY_ID"],$tblbalAmount,$session_companyId,$tblType,$currencyId,$bankId);				
				}
				else
				{
					$result 	 = $objCashFlowForecastSet->UpdateCustomerForcastMinus($row["DATE"],$row["CATEGORY_ID"],$balAmount,$session_companyId,$tblType,$currencyId,$bankId);
					$balAmount	-= $balAmount;
				}
			}
		}
		return $result;
	}
	
	private function GetLastDispatchDate($orderNo,$orderYear)
	{
		$sql = "SELECT
				  OH.intCustomer AS CUSTOMER_ID,
				  OH.intCurrency AS CURRENCY_ID,
				  (SELECT
					 DATE_ADD(DATE(FDH.dtmdate),INTERVAL FPT.strName DAY) AS LAST_DISPATCH_DATE
				   FROM ware_fabricdispatchdetails FDD
					 INNER JOIN ware_fabricdispatchheader FDH
					   ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo
						 AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
				   WHERE FDH.intOrderNo = OH.intOrderNo
					   AND FDH.intOrderYear = OH.intOrderYear
					   AND FDH.intStatus = 1
				   ORDER BY DATE(FDH.dtmdate)DESC
				   LIMIT 1) AS LAST_DISPATCH_DATE
				FROM trn_orderheader OH
				  INNER JOIN mst_financepaymentsterms FPT
					ON FPT.intId = OH.intPaymentTerm
				WHERE OH.intOrderNo = '$orderNo'
					AND OH.intOrderYear = '$orderYear'";
		$result = $this->db->RunQuery2($sql);
		return mysqli_fetch_array($result);
	}
	
	private function UpdateTable($amount,$date,$customerId)
	{
		$sql = "UPDATE finance_forecast_transaction_customer
					SET BALANCE_AMOUNT = ROUND(BALANCE_AMOUNT - $amount,2)
					WHERE DATE = '$date'
						AND CATEGORY_ID = '$customerId';";
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$array["BooSaved"]	= false;
			$array["ErrorMsg"]	= $this->db->errormsg;
			$array["ErrorSql"]	= $sql;
		}
		else
			$array["BooSaved"]	= true;
		return $array;
	}	
//END	- PRIVATE FUNCTIONS }
}
?>
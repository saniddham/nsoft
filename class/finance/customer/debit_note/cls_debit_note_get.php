<?php
class Cls_Debit_Note_Get
{
	private $db;
	private $companyId;
	
	function __construct($db,$companyId)
	{
		$this->db 		 = $db;
		$this->companyId = $companyId;
	}
	public function loadCustomer()
	{
		return $this->loadCustomer_sql();
	}
	public function loadInvoiceType()
	{
		return $this->loadInvoiceType_sql();
	}
	public function loadItem()
	{
		return $this->loadItem_sql();
	}
	public function getInvoiceId($customerId)
	{
		return $this->getInvoiceId_sql($customerId);
	}
	public function getRptHeaderData($debitNoteNo,$debitNoteYear)
	{
		return $this->getRptHeaderData_sql($debitNoteNo,$debitNoteYear);
	}
	public function getRptDetaiData($debitNoteNo,$debitNoteYear)
	{
		return $this->rptDetaiData_sql($debitNoteNo,$debitNoteYear);
	}
	private function loadCustomer_sql()
	{
		$sql = "SELECT intId,strName
				FROM mst_customer
				WHERE intStatus='1'
				ORDER BY strName ";
		
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	private function loadInvoiceType_sql()
	{
		$sql = "SELECT intId, strInvoiceType FROM mst_invoicetype ORDER BY strInvoiceType ";
		
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	private function loadItem_sql()
	{
		$sql = "SELECT intId , 
				strName,
				intDefaultFocus 
				FROM mst_financecustomeritem 
				WHERE intStatus=1
				ORDER BY strName ";
		
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	private function getInvoiceId_sql($customerId)
	{
		$sql = "SELECT strInvoiceType FROM mst_customer WHERE intId='$customerId' ";
		
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	private function getRptHeaderData_sql($debitNoteNo,$debitNoteYear)
	{
		$sql = "SELECT 
				CDNH.DEBIT_NO								AS SERIAL_NO,
				CDNH.COMPANY_ID								AS COMPANY_ID,
				CDNH.DEBIT_DATE								AS SERIAL_DATE,
				MC.strName AS customer,
				CDNH.CUSTOMER_ID,
				CDNH.DEBIT_DATE,
				CDNH.CURRENCY_ID,
				CDNH.INVOICE_TYPE,
				CDNH.REMARKS, 
				FC.strDescription AS currency,
				IT.strInvoiceType,
				CDNH.STATUS
				FROM finance_customer_debit_note_header CDNH
				INNER JOIN mst_customer MC ON MC.intId=CDNH.CUSTOMER_ID
				LEFT JOIN mst_financecurrency FC ON FC.intId=CDNH.CURRENCY_ID
				LEFT JOIN mst_invoicetype IT ON IT.intId=CDNH.INVOICE_TYPE
				WHERE CDNH.COMPANY_ID='".$this->companyId."' AND
				CDNH.DEBIT_NO='$debitNoteNo' AND
				CDNH.DEBIT_YEAR='$debitNoteYear' ";
		
		$result = $this->db->RunQuery($sql);
		
		return mysqli_fetch_array($result);
	}
	private function rptDetaiData_sql($debitNoteNo,$debitNoteYear)
	{
		$sql = "SELECT FCI.strName AS item,
				CDND.ITEM_ID,
				CDND.ITEM_DESCRIPTION,
				CDND.UNIT_OF_MEASURE,
				MU.strCode AS unit,
				ROUND(CDND.UNIT_PRICE,2) AS UNIT_PRICE,
				ROUND(CDND.QTY,2) AS QTY,
				CDND.TAX_ID,
				ROUND(CDND.TAX_VALUE,2) AS taxAmount,
				CDND.COST_CENTER_ID
				FROM finance_customer_debit_note_detail CDND
				INNER JOIN mst_units MU ON MU.intId = CDND.UNIT_OF_MEASURE
				INNER JOIN mst_financecustomeritem FCI ON FCI.intId=CDND.ITEM_ID
				WHERE CDND.DEBIT_NO='$debitNoteNo' AND
				CDND.DEBIT_YEAR='$debitNoteYear' ";
		
		$result = $this->db->RunQuery($sql);
		return $result;
	}
}
?>
<?php
include_once  "../../../../class/cls_commonFunctions_get.php";

class Cls_Debit_Note_Set extends cls_commonFunctions_get
{
	private $db;
	private $companyId;
	private $locationId;
	private $userId;
	private $obj_common;
	
	private $savedStatus	= true;
	private $savedMasseged 	= '';
	private $error_sql		= '';
	
	function __construct($db,$companyId,$locationId,$userId)
	{
		$this->db 			= $db;
		$this->companyId 	= $companyId;
		$this->locationId 	= $locationId;
		$this->userId 		= $userId;
		$this->obj_common	= new cls_commonFunctions_get($db);
	}
	public function save($arrHeader,$arrDetails,$programCode)
	{
		$debitNoteNo		= $arrHeader["debitNoteNo"];
		$debitNoteYear		= $arrHeader["debitNoteYear"];
		$dtDate				= $arrHeader["dtDate"];
		$customerId			= $arrHeader["customerId"];
		$invoiceType		= $arrHeader["invoiceType"];
		$currency			= $arrHeader["currency"];
		$remarks			= $this->obj_common->replace($arrHeader["remarks"]);
		$subTotal			= $arrHeader["subTotal"];
		$grandTotal			= $arrHeader["grandTotal"];
		
		$this->db->begin();
		$this->checkExchangeRateAvailable($currency,$dtDate);
		$this->checkSaveMode($programCode,$this->userId);
		$this->checkUpdateMode($debitNoteNo,$debitNoteYear);
		
		$invoice_array 		= $this->obj_common->GetSystemMaxNo('intFin_DebitNoteNo',$this->locationId);
		
		if($invoice_array["rollBackFlag"]==1)
		{
			if($this->savedStatus)
			{
				$this->savedStatus 		= false;
				$this->savedMasseged 	= $invoice_array["msg"];
				$this->error_sql		= $invoice_array["q"];
			}	
		}
		$debitNoteNo		= $invoice_array["max_no"];
		$debitNoteYear		= date('Y');
		
		$this->saveHeader($debitNoteNo,$debitNoteYear,$dtDate,$customerId,$invoiceType,$currency,$remarks,$grandTotal);	
		$order_by = 1;
		foreach($arrDetails as $array_loop)
		{
			$itemId			= $array_loop["itemId"];
			$itemDisc		= $this->obj_common->replace($array_loop["itemDisc"]);
			$uom			= $array_loop["uom"];
			$qty			= $array_loop["qty"];
			$unitPrice		= $array_loop["unitPrice"];
			$taxId			= $array_loop["taxId"];
			$costCenter		= $array_loop["costCenter"];
			$amount			= $qty*$unitPrice;
			$taxAmount		= $array_loop["taxAmount"];
			
			
			$this->saveDetails($debitNoteNo,$debitNoteYear,$itemId,$itemDisc,$uom,$qty,$unitPrice,$taxId,$costCenter,$taxAmount,$order_by);
			$this->transactionSaveSql($customerId,$currency,'DEBIT',$debitNoteNo,$debitNoteYear,'null',$amount,$dtDate);
			$this->saveTaxTransaction($customerId,$currency,$debitNoteNo,$debitNoteYear,$taxId,$taxAmount,$dtDate);
			$order_by++;			
		}
		
		$this->saveFinanceTransaction($customerId,402,$subTotal,$debitNoteNo,$debitNoteYear,'DEBIT','C','CU',$currency,$dtDate);
		$this->saveCustomerTransaction($customerId,$currency,$debitNoteNo,$debitNoteYear,$grandTotal,$dtDate);
		
		$resultArr	= $this->obj_common->validateDuplicateSerialNoWithSysNo($debitNoteNo,'intFin_DebitNoteNo',$this->locationId);
		if($resultArr['type']=='fail' && $this->savedStatus)
		{
			$this->savedStatus		= false;
			$this->savedMasseged	= $resultArr['msg'];
		}
		
		if($this->savedStatus)
		{
			$this->db->commit();
			$response['type'] 			= "pass";
			$response['msg'] 			= "Saved Successfully.";
			
			$response['debiNoteNo']		= $debitNoteNo;
			$response['debitNoteYear']	= $debitNoteYear;
		}
		else
		{
			$this->db->rollback();
			$response['type'] 			= "fail";
			$response['msg'] 			= $this->savedMasseged;
			$response['sql']			= $this->error_sql;
		}
		return json_encode($response);	
	}
	private function checkSaveMode($programCode,$userId)
	{
		$sql = "SELECT
				menupermision.intAdd 
				FROM menupermision 
				INNER JOIN menus ON menupermision.intMenuId = menus.intId
				WHERE
				menus.strCode = '$programCode' AND
				menupermision.intUserId = '$userId' ";
		
		$result = $this->db->RunQuery2($sql);
		$row 	= mysqli_fetch_array($result);
		
		if($row['intAdd']!=1)
		{
			
			if($this->savedStatus)
			{
				$this->savedStatus		= false;
				$this->savedMasseged	= "You do not have permission to Save Customer Debit Note.";
			}		
		}
	}
	private function checkUpdateMode($debitNoteNo,$debitNoteYear)
	{
		if($debitNoteNo!='' && $debitNoteYear!='')
		{
			if($this->savedStatus)
			{
				$this->savedStatus		= false;
				$this->savedMasseged	= "You can not update Customer Debit Note.";
			}		
		}
	}
	private function saveHeader($debitNoteNo,$debitNoteYear,$dtDate,$customerId,$invoiceType,$currency,$remarks,$grandTotal)
	{		
		$sql = "INSERT INTO finance_customer_debit_note_header 
				(
				DEBIT_NO, 
				DEBIT_YEAR, 
				CUSTOMER_ID, 
				CURRENCY_ID, 
				INVOICE_TYPE, 
				REMARKS, 
				DEBIT_DATE,
				STATUS, 
				COMPANY_ID, 
				LOCATION_ID, 
				CREATED_BY, 
				CREATED_DATE
				)
				VALUES
				(
				'$debitNoteNo', 
				'$debitNoteYear', 
				'$customerId', 
				'$currency', 
				'$invoiceType', 
				'$remarks', 
				'$dtDate',
				'1', 
				'".$this->companyId."', 
				'".$this->locationId."', 
				'".$this->userId."', 
				NOW()
				) ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			if($this->savedStatus)
			{
				$this->savedStatus		= false;
				$this->savedMasseged	= $this->db->errormsg;
				$this->error_sql		= $sql;
			}
		}
	}
	private function saveDetails($debitNoteNo,$debitNoteYear,$itemId,$itemDisc,$uom,$qty,$unitPrice,$taxId,$costCenter,$taxAmount,$order_by)
	{
		$sql = "INSERT INTO finance_customer_debit_note_detail 
				(
				DEBIT_NO, 
				DEBIT_YEAR, 
				ITEM_ID, 
				ITEM_DESCRIPTION, 
				UNIT_OF_MEASURE, 
				QTY, 
				UNIT_PRICE, 
				TAX_VALUE, 
				TAX_ID, 
				COST_CENTER_ID,
				ORDER_BY
				)
				VALUES
				(
				'$debitNoteNo', 
				'$debitNoteYear', 
				'$itemId', 
				'$itemDisc', 
				'$uom', 
				'$qty', 
				'$unitPrice', 
				'$taxAmount', 
				$taxId, 
				'$costCenter',
				'$order_by'
				) ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			if($this->savedStatus)
			{
				$this->savedStatus		= false;
				$this->savedMasseged	= $this->db->errormsg;
				$this->error_sql		= $sql;
			}
		}
	}
	private function transactionSaveSql($customerId,$currency,$docType,$debitNoteNo,$debitNoteYear,$ledgerId,$amount,$dtDate)
	{
		$sql = "INSERT INTO finance_customer_transaction 
				(
				CUSTOMER_ID, 
				CURRENCY_ID, 
				DOCUMENT_YEAR, 
				DOCUMENT_NO, 
				DOCUMENT_TYPE,
				INVOICE_YEAR,
				INVOICE_NO,
				DEBIT_NO,
				DEBIT_YEAR,
				LEDGER_ID, 
				VALUE, 
				COMPANY_ID, 
				LOCATION_ID, 
				USER_ID,
				TRANSACTION_DATE_TIME,
				SYSTEM_MODIFIED_DATE
				)
				VALUES
				(
				'$customerId', 
				'$currency', 
				'$debitNoteYear', 
				'$debitNoteNo', 
				'$docType',
				 NULL,
				 NULL,
				 '$debitNoteNo',
				 '$debitNoteYear',
				 $ledgerId, 
				'$amount', 
				'".$this->companyId."', 
				'".$this->locationId."', 
				'".$this->userId."',
				'$dtDate',
				NOW()) ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			if($this->savedStatus)
			{
				$this->savedStatus		= false;
				$this->savedMasseged	= $this->db->errormsg;
				$this->error_sql		= $sql;
			}
		}
	}
	private function saveTaxTransaction($customerId,$currency,$debitNoteNo,$debitNoteYear,$taxId,$taxAmount,$dtDate)
	{
		if($taxId!='NULL')
		{
			$taxChrtOfAccount 				= $this->getTaxChartOfAccount($taxId);
			if($taxChrtOfAccount['type']=='fail')
			{
				if($this->savedStatus)
				{
					$this->savedStatus 		= false;
					$this->savedMasseged	= $taxChrtOfAccount['ErrorMsg'];
				}
			}
			else
			{
				$this->transactionSaveSql($customerId,$currency,'DEBIT',$debitNoteNo,$debitNoteYear,$taxChrtOfAccount,$taxAmount,$dtDate);
				$this->saveFinanceTransaction($customerId,$taxChrtOfAccount,$taxAmount,$debitNoteNo,$debitNoteYear,'DEBIT','C','CU',$currency,$dtDate);
			}
		}
	}
	private function saveCustomerTransaction($customerId,$currency,$debitNoteNo,$debitNoteYear,$grandTotal,$dtDate)
	{
		$cusChrtOfAccount 				= $this->getCusChartOfAccount($customerId);
		if($cusChrtOfAccount['type']=='fail')
		{
			if($this->savedStatus)
			{
				$this->savedStatus 		= false;
				$this->savedMasseged	= $cusChrtOfAccount['ErrorMsg'];
			}			
		}
		else
		{
			$this->saveFinanceTransaction($customerId,$cusChrtOfAccount,$grandTotal,$debitNoteNo,$debitNoteYear,'DEBIT','D','CU',$currency,$dtDate);
		}
	}
	private function getCusChartOfAccount($customerId)
	{
		$sql = "SELECT 
				CHART_OF_ACCOUNT_ID 
				FROM finance_mst_chartofaccount FCOA
				WHERE FCOA.CATEGORY_TYPE = 'C'
				AND CATEGORY_ID = '$customerId'";
				
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		
		if(mysqli_num_rows($result)>0)
		{
			return $row["CHART_OF_ACCOUNT_ID"];
		}
		else
		{
			$customerName			= $this->getPublicValues('strName','mst_customer','intId',$customerId);
			$response['type']		= 'fail';
			$response['ErrorMsg']	= "No GL Account available for Customer : $customerName";
			return $response;
		}
	}
	private function getTaxChartOfAccount($taxId)
	{
		$sql = "SELECT CHART_OF_ACCOUNT_ID
				FROM finance_mst_chartofaccount_tax
				WHERE TAX_ID='$taxId' ";
		
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		
		if(mysqli_num_rows($result)>0)
		{
			return $row["CHART_OF_ACCOUNT_ID"];
		}
		else
		{
			$taxCode				= $this->getPublicValues('strCode','mst_financetaxgroup','intId',$taxId);
			$response['type']		= 'fail';
			$response['ErrorMsg']	= "No GL Account available for tax code : $taxCode";
			return $response;
		}
	}
	private function saveFinanceTransaction($customerId,$ChrtOfAccount,$value,$docNo,$docYear,$docType,$transcType,$transcCat,$currency,$dtDate)
	{
		$sql = "INSERT INTO finance_transaction 
				( 
				CHART_OF_ACCOUNT_ID, 
				AMOUNT, 
				DOCUMENT_NO, 
				DOCUMENT_YEAR, 
				DOCUMENT_TYPE, 
				TRANSACTION_TYPE, 
				TRANSACTION_CATEGORY,
				TRANSACTION_CATEGORY_ID, 
				CURRENCY_ID, 
				LOCATION_ID, 
				COMPANY_ID, 
				LAST_MODIFIED_BY,
				LAST_MODIFIED_DATE
				)
				VALUES
				(
				'$ChrtOfAccount', 
				'$value', 
				'$docNo', 
				'$docYear', 
				'$docType', 
				'$transcType', 
				'$transcCat',
				'$customerId',
				'$currency', 
				'".$this->locationId."',
				'".$this->companyId."', 
				'".$this->userId."',
				'$dtDate'
				)";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			if($this->savedStatus)
			{
				$this->savedStatus 		= false;
				$this->savedMasseged	= $this->db->errormsg;
				$this->error_sql		= $sql;
			}
		}
	}
	private function getPublicValues($select_fieldName,$tableName,$where_fieldId,$where_fieldValue)
	{
		$sql = "SELECT $select_fieldName AS VALUE FROM $tableName WHERE $where_fieldId = '$where_fieldValue'";
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		return $row["VALUE"];
	}
	private function checkExchangeRateAvailable($currency,$dtDate)
	{
		
		$sql = "SELECT * FROM mst_financeexchangerate 
				WHERE dtmDate = '$dtDate' AND 
				intCompanyId = '".$this->companyId."' AND
				intCurrencyId = '$currency' ";
		
		$result = $this->db->RunQuery2($sql);
		if(mysqli_num_rows($result)<=0 && $this->savedStatus)
		{
			$this->savedStatus 		= false;
			$this->savedMasseged 	= "Please enter exchange rates for '".$dtDate."' before save.";
		}
		
	}
}
?>
<?php
include_once  $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
include_once  $_SESSION['ROOT_PATH']."class/finance/customer/cls_common_get.php";
include_once  $_SESSION['ROOT_PATH']."class/finance/customer/payment_received_settlement/cls_PaymentReceivedSettlement_get.php";
//ini_set('display_errors', 1); 
 
$obj_common				= new cls_commonFunctions_get($db);
$obj_fin_receivd_get	= new Cls_PaymentReceivedSettlement_Get($db);
$obj_common_get			=  new Cls_Common_Get($db);

$session_userId			= $_SESSION["userId"];
$session_locationId 	= $_SESSION["CompanyID"];
$session_companyId 		= $_SESSION["headCompanyId"];

class Cls_PaymentReceivedSettlement_Set
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	
	public function Save($customerId,$currencyId,$detailArray,$glArray,$receiptDate)
	{
		global $obj_common;
		global $savedStatus;
		global $savedMasseged;
		global $session_locationId;
		global $errorSql;
		global $obj_common_get;
		$savedStatus	= true;
		
		$this->db->begin();
		$serial_array 	= $obj_common->GetSystemMaxNo('PAYMENT_SETTLEMENT_NO',$session_locationId);
		$receiptNo		= $serial_array["max_no"];
		$receiptYear	= date('Y');
		
		$this->SaveHeader($receiptNo,$receiptYear,$customerId,$currencyId,$receiptDate);
		
		foreach($detailArray as $detailLoop)
		{
			$invoiceNo_array	= explode('/',$detailLoop["InvoiceNo"]);
			$invoiceYear		= $invoiceNo_array[0];
			$invoiceNo			= $invoiceNo_array[1];
			$payAmount			= $detailLoop["PayAmount"];
			$orderNo_arrary		= explode('/',$detailLoop["OrderNo"]);
			$orderNo			= $orderNo_arrary[1];
			$orderYear			= $orderNo_arrary[0];
			
			$this->ValidateDetailSaving($customerId,$currencyId,$invoiceYear,$invoiceNo,$payAmount);
			$this->SaveDetails($receiptNo,$receiptYear,$invoiceYear,$invoiceNo,$payAmount);
 		}
		
		foreach($glArray as $glLoop)
		{
			$ledgerId			= $glLoop["GLAccount"];
			$costCenter			= $glLoop["GLCostCenter"];
 			$value				= $glLoop["GLValue"];
			$this->ValidateGLUpdating($customerId,$ledgerId,$costCenter,$value);
			$this->SaveGLDetails($receiptNo,$receiptYear,$ledgerId,$costCenter,$value);
 			$this->UpdateGLDetails($customerId,$receiptNo,$receiptYear,$ledgerId,$costCenter,$value);
		}		
		
 			$i			 = 0;
			$finalInvBal = 0;
			foreach($glArray as $arrValGL)
			{
 				
				$accountId 		= $arrValGL['GLAccount'];
				$glValue 		= $arrValGL['GLValue'];
				$glBal   		= $glValue;
				$n				= 0;
				$savedDetails	=0;
				foreach($detailArray as $arrVal)
				{
					$savedDetails++;
 					$invoiceNoArr 	= explode('/',trim($arrVal['InvoiceNo']));
					$PONoArr 		= explode('/',trim($arrVal['OrderNo']));
					$PONo			= $PONoArr[1];
					$POYear			= $PONoArr[0];
					$invoiceNo		= $invoiceNoArr[1];
					$invoiceYear	= $invoiceNoArr[0];
					$invValue 		= $arrVal['PayAmount'];
					if(++$n<$i)
					{
   					}
					else
					{
						if($finalInvBal>0)
						{
 							$invValue = $finalInvBal;
						}
						else
						{
							$i++;
						}
						if(($glBal	>=	$invValue) && ($invValue > 0))
						{
							$this->SaveTransaction($receiptNo,$receiptYear,$invoiceYear,$invoiceNo,$invValue,$PONo,$POYear,$customerId,$currencyId,$accountId);
							$response 		= $obj_common_get->Save_Jurnal_Transaction($accountId,$invValue,$receiptNo,$receiptYear,'INV_SETTLEMENT',$invoiceNo,$invoiceYear,'D',$transCategory='CU',$customerId,$currencyId,'',$receiptDate);
							if($response['type']=='fail')
							{
								$savedStatus	= false;
								$savedMasseged	= $response['ErrorMsg'];
								$errorSql		= $response['ErrorSql'];
							}
							
							$response 	= $obj_common_get->GetCustomerGL($customerId);
							if($response['type']=='fail')
							{
								$savedStatus	= false;
								$savedMasseged	= $response['ErrorMsg'];
								$errorSql		= $response['ErrorSql'];
							}
							else{
								$customerGL=$response;
							}
							$response 	= $obj_common_get->Save_Jurnal_Transaction($customerGL,$invValue,$receiptNo,$receiptYear,'INV_SETTLEMENT',$invoiceNo,$invoiceYear,'C',$transCategory='CU',$customerId,$currencyId,'',$receiptDate);
 							if($response['type']=='fail')
							{
								$savedStatus	= false;
								$savedMasseged	= $response['ErrorMsg'];
								$errorSql		= $response['ErrorSql'];
							}
							
							$glBal			-= $invValue;
							$finalInvBal = 0;
						}
 						else if($glBal>0)
						{
							$this->SaveTransaction($receiptNo,$receiptYear,$invoiceYear,$invoiceNo,$glBal,$PONo,$POYear,$customerId,$currencyId,$accountId);
							$response 	= $obj_common_get->Save_Jurnal_Transaction($accountId,$glBal,$receiptNo,$receiptYear,'INV_SETTLEMENT',$invoiceNo,$invoiceYear,'D',$transCategory='CU',$customerId,$currencyId,'',$receiptDate);
							if($response['type']=='fail')
							{
								$savedStatus	= false;
								$savedMasseged	= $response['ErrorMsg'];
								$errorSql		= $response['ErrorSql'];
							}
							
							$response 	= $obj_common_get->GetCustomerGL($customerId);
							if($response['type']=='fail')
							{
								$savedStatus	= false;
								$savedMasseged	= $response['ErrorMsg'];
								$errorSql		= $response['ErrorSql'];
							}
							else{
								$customerGL=$response;
							}
							$response 	= $obj_common_get->Save_Jurnal_Transaction($customerGL,$glBal,$receiptNo,$receiptYear,'INV_SETTLEMENT',$invoiceNo,$invoiceYear,'C',$transCategory='CU',$customerId,$currencyId,'',$receiptDate);
 							if($response['type']=='fail')
							{
								$savedStatus	= false;
								$savedMasseged	= $response['ErrorMsg'];
								$errorSql		= $response['ErrorSql'];
							}
 							
							$finalInvBal 	= $invValue - $glBal;
							$glBal 			= 0;
						}
 					}
				}
			}
		
		$resultArr	= $obj_common->validateDuplicateSerialNoWithSysNo($receiptNo,'PAYMENT_SETTLEMENT_NO',$session_locationId);
		if($resultArr['type']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged	= $resultArr['msg'];
		} 
 
 		if($savedDetails==0){
			$this->db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= "Details saving error";
			$response['errorSql']	= $errorSql;
		}
		else if($savedStatus){
			$this->db->commit();
			$response['type'] 		= "pass";
			$response['msg'] 		= "Payment Settelement No : '$receiptYear/$receiptNo' saved successfully.";
			$response['ReceiptNo'] 	= $receiptNo;
			$response['ReceiptYear']= $receiptYear;
		}else{
			$this->db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= $savedMasseged;
			$response['errorSql']	= $errorSql;
		}
		return json_encode($response);
	}
	
	public function Cancel($receiptNo,$receiptYear)
	{
		global $savedStatus;
		global $savedMasseged;
		global $errorSql;		
		global $obj_common_get;
		
		$savedStatus	= true;
		$this->db->begin();
		
		$this->Cancel_sql($receiptNo,$receiptYear);
		
		$trans_response = $obj_common_get->RemoveTransaction($receiptNo,$receiptYear,'INV_SETTLEMENT');
		if($trans_response["type"])
		{
			$savedStatus	= false;
			$savedMasseged	= $trans_response["msg"];
			$errorSql		= $trans_response["errorSql"];
		}
		
		$trans_response = $obj_common_get->RemoveFinanceMainTransaction($receiptNo,$receiptYear,'INV_SETTLEMENT');
		if($trans_response["type"])
		{
			$savedStatus	= false;
			$savedMasseged	= $trans_response["msg"];
			$errorSql		= $trans_response["errorSql"];
		}		
		
		if($savedStatus){
			$this->db->commit();
			$response['type'] 		= "pass";
			$response['msg'] 		= "Canceled successfully.";
		}else{
			$this->db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= $savedMasseged;
			$response['errorSql']	= $errorSql;
		}
		return json_encode($response);
	}
	
	private function SaveHeader($receiptNo,$receiptYear,$customerId,$currencyId,$receiptDate)
	{
		global $session_companyId;
		global $session_locationId;
		global $session_userId;
		global $savedMasseged;
		global $savedStatus;
		global $errorSql;
		
		$sql = "INSERT INTO finance_customer_settle_header 
				(SETTLE_NO, 
				SETTLE_YEAR, 
				CUTOMER_ID, 
				CURRENCY_ID, 
				SETTLE_DATE, 
				STATUS,
				COMPANY_ID, 
				LOCATION_ID, 
				CREATED_BY, 
				CREATED_DATE)
				VALUES
				('$receiptNo', 
				'$receiptYear', 
				'$customerId', 
				$currencyId, 
				'$receiptDate',
				'1', 
				'$session_companyId', 
				'$session_locationId', 
				'$session_userId', 
				NOW());";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$savedStatus	= false;
			$savedMasseged	= $this->db->errormsg;
			$errorSql		= $sql;
		}
	}
	
	private function SaveDetails($receiptNo,$receiptYear,$invoiceYear,$invoiceNo,$payAmount)
	{
		global $savedMasseged;
		global $savedStatus;
		global $errorSql;
		
		$sql = "INSERT INTO finance_customer_settle_invoice_details
				(SETTLE_NO,
				STLLET_YEAR,
				INVOICE_NO,
				INVOICE_YEAR,
				INVOICE_AMOUNT)
				VALUES 
				('$receiptNo',
				'$receiptYear',
				'$invoiceNo',
				'$invoiceYear',
				'$payAmount');";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$savedStatus	= false;
			$savedMasseged	= $this->db->errormsg;
			$errorSql		= $sql;
		}
	}
	
	private function SaveGLDetails($receiptNo,$receiptYear,$ledgerId,$costCenter,$value)
	{
		global $savedMasseged;
		global $savedStatus;
		global $errorSql;
		
		$sql = "INSERT INTO finance_customer_settle_details
				(SETTLE_NO,
				SETTLE_YEAR,
				CHART_OF_ACCOUNT_NO,
				COST_CENTER_ID,
				AMOUNT)
				VALUES 
				('$receiptNo',
				'$receiptYear',
				'$ledgerId',
				'$costCenter',
				'$value');";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$savedStatus	= false;
			$savedMasseged	= $this->db->errormsg;
			$errorSql		= $sql;
		}
	}
	private function UpdateGLDetails($customerId,$settleNo,$settleYear,$companyLedgerId,$costCenter,$value)
	{
		global $savedMasseged;
		global $savedStatus;
		global $errorSql;
		
		    $sql="SELECT 
			finance_customer_pay_receive_gl.RECEIPT_NO,
			finance_customer_pay_receive_gl.RECEIPT_YEAR,
			finance_customer_pay_receive_gl.LEDGER_ID,
			finance_mst_chartofaccount.CHART_OF_ACCOUNT_ID,
			finance_customer_pay_receive_gl.COST_CENTER_ID,
			finance_customer_pay_receive_gl.PAY_AMOUNT_BALANCE
			FROM `finance_customer_pay_receive_gl`
			INNER JOIN finance_customer_pay_receive_header ON finance_customer_pay_receive_gl.RECEIPT_NO = finance_customer_pay_receive_header.RECEIPT_NO AND finance_customer_pay_receive_gl.RECEIPT_YEAR = finance_customer_pay_receive_header.RECEIPT_YEAR
		 	INNER JOIN finance_mst_chartofaccount  
					ON finance_mst_chartofaccount.CATEGORY_ID = finance_customer_pay_receive_header.COMPANY_ID
			WHERE 
			finance_customer_pay_receive_header.CUSTOMER_ID = '$customerId' AND
			finance_mst_chartofaccount.CHART_OF_ACCOUNT_ID = '$companyLedgerId' AND
			finance_customer_pay_receive_gl.COST_CENTER_ID = '$costCenter' 
			AND finance_customer_pay_receive_gl.PAY_AMOUNT_BALANCE>0";
			
 		$result 	= $this->db->RunQuery2($sql);
 		while($row	= mysqli_fetch_array($result))
		{
			$receiptNo		=$row['RECEIPT_NO'];
			$receiptYear	=$row['RECEIPT_YEAR'];
			$ledgerId		=$row['LEDGER_ID'];
			
			$balAmmount	=$row['PAY_AMOUNT_BALANCE'];
			if($value	>=	$balAmmount){
				$saveQty	=$balAmmount;
				$value		=$value-$saveQty;
			}
			else{
				$saveQty	=$value;
			}
			$sqlU = "UPDATE finance_customer_pay_receive_gl 
					SET 
					PAY_AMOUNT_BALANCE = PAY_AMOUNT_BALANCE-'$saveQty'
					 WHERE
			finance_customer_pay_receive_gl.RECEIPT_NO = '$receiptNo' AND
			finance_customer_pay_receive_gl.RECEIPT_YEAR = '$receiptYear' AND 
			finance_customer_pay_receive_gl.LEDGER_ID = '$ledgerId' AND
			finance_customer_pay_receive_gl.COST_CENTER_ID = '$costCenter'";
			$resultU = $this->db->RunQuery2($sqlU);
			if(!$resultU){
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$errorSql		= $sql;
			}
		}
	}
	
	private function SaveTransaction($receiptNo,$receiptYear,$invoiceYear,$invoiceNo,$payAmount,$orderNo,$orderYear,$customerId,$currencyId,$ledgerId)
	{
		global $savedMasseged;
		global $savedStatus;
		global $session_companyId;
		global $session_locationId;
		global $session_userId;
		global $errorSql;
		
		if($payAmount<=0)
			return;
		
		$sql = "INSERT INTO finance_customer_transaction 
				(ORDER_YEAR, 
				ORDER_NO, 
				CUSTOMER_ID, 
				CURRENCY_ID, 
				DOCUMENT_YEAR, 
				DOCUMENT_NO, 
				DOCUMENT_TYPE, 
				INVOICE_YEAR, 
				INVOICE_NO, 
				LEDGER_ID, 
				VALUE, 
				COMPANY_ID, 
				LOCATION_ID, 
				USER_ID, 
				TRANSACTION_DATE_TIME)
				VALUES
				('$orderYear', 
				'$orderNo', 
				'$customerId', 
				'$currencyId', 
				'$receiptYear', 
				'$receiptNo', 
				'INV_SETTLEMENT', 
				'$invoiceYear', 
				'$invoiceNo', 
				'$ledgerId', 
				'-$payAmount', 
				'$session_companyId', 
				'$session_locationId', 
				'$session_userId', 
				NOW());";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$savedStatus	= false;
			$savedMasseged	= $this->db->errormsg;
			$errorSql		= $sql;
		}
	}
	
	private function ValidateDetailSaving($customerId,$currencyId,$invoiceYear,$invoiceNo,$payAmount)
	{
		global $savedMasseged;
		global $savedStatus;
		global $errorSql;
		global $session_companyId;
		$sql = "SELECT
				  CIH.INVOICE_NO										AS INVOICE_NO,
				  ROUND(SUM(CT.VALUE),2)								AS VALUE,
				  
				  COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'INVOICE'),0) 			AS INVOICED_VALUE,
				
				  COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'DEBIT'),0) 				AS DEBIT_VALUE,
					
				  COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'CREDIT'),0) 			AS CREDIT_VALUE,
					
				  COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'ADVANCE'),0) 			AS ADVANCE_VALUE,
					
				COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'PAYRECEIVE'),0) 		AS PAID_VALUE ,
					
				ROUND(COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
					 AND SUB_CT.LEDGER_ID = CT.LEDGER_ID
					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'INV_SETTLEMENT'),0),2) 		AS INV_SETTLE_VALUE
					
				FROM finance_customer_transaction CT
				INNER JOIN finance_customer_invoice_header CIH ON CIH.SERIAL_YEAR = CT.INVOICE_YEAR AND CIH.SERIAL_NO = CT.INVOICE_NO
				WHERE 
					CT.CUSTOMER_ID 		= $customerId
					AND CT.CURRENCY_ID 	= $currencyId
					AND CIH.SERIAL_NO 	= '$invoiceNo'
					AND CIH.SERIAL_YEAR = '$invoiceYear'
					AND CT.COMPANY_ID 	= $session_companyId
				GROUP BY CIH.SERIAL_NO,CIH.SERIAL_YEAR";
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		$toBePaid = round(abs($row["INVOICED_VALUE"]) - (abs($row["DEBIT_VALUE"]) + abs($row["CREDIT_VALUE"]) + abs($row["ADVANCE_VALUE"]) + abs($row["PAID_VALUE"]) - abs($row["INV_SETTLE_VALUE"])),2);
		//die($payAmount .'|'. $toBePaid);
		$invoiceNo	= $row["INVOICE_NO"];
		if($toBePaid < $payAmount){
			$savedStatus	= false;
			$savedMasseged	= "No balance amount available for Invoice No : $invoiceNo";
			$errorSql		= $sql;
		}
	}
	
	private function ValidateGLUpdating($customerId,$ledgerId,$costCenter,$value)
	{
		global $savedMasseged;
		global $savedStatus;
		global $errorSql;
		global $session_companyId;
	 	/*$sqlV="SELECT 
 			finance_customer_pay_receive_gl.LEDGER_ID,
			finance_customer_pay_receive_gl.COST_CENTER_ID,
			SUM(IFNULL(finance_customer_pay_receive_gl.PAY_AMOUNT_BALANCE,0)) as bal,
 			mst_financedimension.strName as COST_CENTER,
 			finance_mst_chartofaccount.CHART_OF_ACCOUNT_NAME
			FROM `finance_customer_pay_receive_gl`
			INNER JOIN finance_customer_pay_receive_header ON finance_customer_pay_receive_gl.RECEIPT_NO = finance_customer_pay_receive_header.RECEIPT_NO AND finance_customer_pay_receive_gl.RECEIPT_YEAR = finance_customer_pay_receive_header.RECEIPT_YEAR
			LEFT JOIN mst_financedimension ON finance_customer_pay_receive_gl.COST_CENTER_ID = mst_financedimension.intId
			INNER JOIN finance_mst_chartofaccount ON finance_customer_pay_receive_gl.LEDGER_ID = finance_mst_chartofaccount.CHART_OF_ACCOUNT_ID
			WHERE 
			finance_customer_pay_receive_header.CUSTOMER_ID = '$customerId' AND
			finance_customer_pay_receive_gl.LEDGER_ID = '$ledgerId' AND
			finance_customer_pay_receive_gl.COST_CENTER_ID = '$costCenter' ";*/
 			
	  	$sqlV = "SELECT
				  GL.LEDGER_ID,
				  GL.COST_CENTER_ID,
				  D.strName                 AS COST_CENTER,
				  SUM(GL.PAY_AMOUNT_BALANCE) AS balAmmount,
				  PH.CUSTOMER_ID,
				  COA.CHART_OF_ACCOUNT_NAME,
				  COA1.CHART_OF_ACCOUNT_ID             AS COA1_CHART_OF_ACCOUNT_ID,
				  COA1.CHART_OF_ACCOUNT_NAME    AS COA1_CHART_OF_ACCOUNT_NAME
				FROM finance_customer_pay_receive_gl GL
				  INNER JOIN finance_customer_pay_receive_header PH
					ON GL.RECEIPT_NO = PH.RECEIPT_NO
					  AND GL.RECEIPT_YEAR = PH.RECEIPT_YEAR
				  LEFT JOIN mst_financedimension D
					ON GL.COST_CENTER_ID = D.intId
				  INNER JOIN finance_mst_chartofaccount COA
					ON GL.LEDGER_ID = COA.CHART_OF_ACCOUNT_ID
				  INNER JOIN finance_mst_chartofaccount COA1
					ON COA1.CATEGORY_ID = PH.COMPANY_ID
				WHERE COA.SUB_TYPE_ID = 14
					AND PH.CUSTOMER_ID = '$customerId'
					AND COA1.CATEGORY_TYPE = 'CO' 
				AND COA1.CHART_OF_ACCOUNT_ID = '$ledgerId' 
				AND GL.COST_CENTER_ID = '$costCenter'  
				GROUP BY PH.COMPANY_ID, GL.COST_CENTER_ID
				HAVING balAmmount > 0
				ORDER BY COA.CHART_OF_ACCOUNT_NAME ASC, D.strName ASC
";
	
	
		$resultV	= $this->db->RunQuery2($sqlV);
		$row1		= mysqli_fetch_array($resultV);
 		$acc		=$row1["COA1_CHART_OF_ACCOUNT_NAME"];
		$costCenter	=$row1["COST_CENTER"];
 		$bal		= $row1["balAmmount"];
 		if($bal < $value){
			$savedStatus	= false;
			$savedMasseged	= "No balance amount available for Ledger Account : $acc and Cost center : $costCenter  .Available balance is $bal";
			$errorSql		= $sql;
		}
	}
	
	private function Cancel_sql($receiptNo,$receiptYear)
	{
		global $savedStatus;
		global $savedMasseged;
		global $errorSql;
		
		$sql = "UPDATE finance_customer_settle_header
				SET STATUS = '10'
				WHERE SETTLE_NO = '$receiptNo'
				AND SETTLE_YEAR = '$receiptYear';";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$savedStatus	= false;
			$savedMasseged	= $this->db->errormsg;
			$errorSql		= $sql;
		}
		
	}
}
?>
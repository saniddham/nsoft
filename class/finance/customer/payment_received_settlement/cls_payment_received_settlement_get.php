<?php
include_once  $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
$obj_common			= new cls_commonFunctions_get($db);
 
$session_userId		= $_SESSION["userId"];
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];
$programCode		= 'P0733'; 
 
class Cls_PaymentReceivedSettlement_Get
{
	private $db;
 	
	function __construct($db)
	{
		$this->db = $db;
 	}

 	public function getHeaderDetails_array($serialYear,$serialNo)
	{
 		$sql 	= $this->LoadHeaderDetails_sql($serialYear,$serialNo);
		$result = $this->db->RunQuery($sql);
		$row 	= mysqli_fetch_array($result);
		return $row;
	}
	public function getHeaderDetails_array2($serialYear,$serialNo)
	{
 		$sql 	= $this->LoadHeaderDetails_sql($serialYear,$serialNo);
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		return $row;
	}
	public function get_ReportHeaderDetails_array($serialYear,$serialNo)
	{
 		$sql 	= $this->LoadReportHeaderDetails_sql($serialYear,$serialNo);
		$result = $this->db->RunQuery($sql);
		$row 	= mysqli_fetch_array($result);
		return $row;
	}
	public function get_ReportGridDetails_invoice_result($serialYear,$serialNo)
	{
 		$sql 	= $this->LoadReportGridDetails_invoice_sql($serialYear,$serialNo);
		$result = $this->db->RunQuery($sql);
 		return $result;
	}
	public function get_ReportGridDetails_gl_result($serialYear,$serialNo)
	{
 		$sql 	= $this->LoadReportGridDetails_gl_sql($serialYear,$serialNo);
		$result = $this->db->RunQuery($sql);
 		return $result;
	}
	public function getSavedGridDetails_invoice_result($serialYear,$serialNo,$customerId,$currencyId)
	{
		$data = $this->LoadSavedGridDetails_invoice_data($serialYear,$serialNo,$customerId,$currencyId);
 		return $data;
	}
	public function getSavedGridDetails_accounts_result($serialYear,$serialNo,$customer,$ledgerId,$costCenter)
	{
		$sql 	= $this->LoadSavedGridDetails_accounts_sql($serialYear,$serialNo,$customer,$ledgerId,$costCenter);
		$result = $this->db->RunQuery($sql);
		return $result;
	}
 	public function getGridDetails_accounts_details($customer)
	{
		$sql 	= $this->LoadGridDetails_accounts_sql($customer);
		$result = $this->db->RunQuery($sql);
 		while($row = mysqli_fetch_array($result))
		{
  			$data["LEDGER_ID"] 				= abs($row["COA1_CHART_OF_ACCOUNT_ID"]);
 			$data["CHART_OF_ACCOUNT_NAME"]	= ($row["COA1_CHART_OF_ACCOUNT_NAME"]);
			$data["RECEIVED_VALUE"] 		= abs($row["RECEIVED_VALUE"]);
			$data["COST_CENTER_ID"] 		= abs($row["COST_CENTER_ID"]);
			$data["COST_CENTER"] 			= ($row["COST_CENTER"]);
			$data["balAmmount"] 			= abs($row["balAmmount"]);
 			$arr_data[] = $data;
		}
		$response['GRID'] 	= $arr_data;
		echo json_encode($response);
	}
 	public function get_permision_save($serialYear,$serialNo,$session_userId)
	{
		global $obj_common;
		global $programCode; 
		
		$permition	=0;
		$row 		= $this->getHeaderDetails_array($serialYear,$serialNo);
		$perm		= $obj_common->Load_menupermision($programCode,$session_userId,'intEdit');
 		if(($serialNo == '') && ($perm ==1)){
			$permition=1;
		}
  		
		return $permition;
	}
	public function get_permision_confirm($serialYear,$serialNo,$session_userId)
	{
		global $obj_common;
		global $programCode; 
		
		$permition=0;
		$row	= $this->getHeaderDetails_array($serialYear,$serialNo);
		$k		=$row['SAVE_LEVELS']-$row['STATUS']+2;
		$field	='int'.$k.'Approval';
 		$perm	= $obj_common->Load_menupermision($programCode,$session_userId,$field);
		if(($row['STATUS']>1) && ($perm ==1)){
			$permition=1;
		}
		
		return $permition;
	}
	public function get_permision_reject($serialYear,$serialNo,$session_userId)
	{
		global $obj_common;
		global $programCode; 
		
		$permition=0;
		$row = $this->getHeaderDetails_array($serialYear,$serialNo);
		$perm= $obj_common->Load_menupermision($programCode,$session_userId,'intReject');
		if(($row['STATUS']>1) && ($row['STATUS']<=$row['SAVE_LEVELS']) && ($perm ==1)){
			$permition=1;
		}
 		return $permition;
	}
	public function get_permision_cancel($serialYear,$serialNo,$session_userId)
	{
		global $obj_common;
		global $programCode; 
		
		$permition=0;
		$row = $this->getHeaderDetails_array($serialYear,$serialNo);
		$perm= $obj_common->Load_menupermision($programCode,$session_userId,'intCancel');
		if(($row['STATUS']==1) && ($perm ==1) && ($row['STATUS']!=10)){
			$permition=1;
		}
 		return $permition;
	}
	public function get_permision_confirm2($serialYear,$serialNo,$session_userId)
	{
		global $obj_common;
		global $programCode; 
		
		$permition=0;
		$row	= $this->getHeaderDetails_array2($serialYear,$serialNo);
 		$k		=$row['SAVE_LEVELS']-$row['STATUS']+2;
		$field	='int'.$k.'Approval';
 		$perm	= $obj_common->Load_menupermision2($programCode,$session_userId,$field);
		if(($row['STATUS']>1) && ($perm ==1)){
			$permition=1;
		}
 		return $permition;
	}
	public function get_permision_reject2($serialYear,$serialNo,$session_userId)
	{
		global $obj_common;
		global $programCode; 
		
		$permition=0;
		$row = $this->getHeaderDetails_array2($serialYear,$serialNo);
		$perm= $obj_common->Load_menupermision2($programCode,$session_userId,'intReject');
		if(($row['STATUS']>1) && ($row['STATUS']<=$row['SAVE_LEVELS']) && ($perm ==1)){
			$permition=1;
		}
 		return $permition;
	}
	public function get_permision_cancel2($serialYear,$serialNo,$session_userId)
	{
		global $obj_common;
		global $programCode; 
		
		$permition=0;
		$row = $this->getHeaderDetails_array2($serialYear,$serialNo);
		$perm= $obj_common->Load_menupermision2($programCode,$session_userId,'intCancel');
		if(($perm ==1)){
			$permition=1;
		}
 		return $permition;
	}
	
	public function ValidateBeforeSave($serialYear,$serialNo)
	{ 
		global $session_userId;
		
		$permision_save		 = $this->get_permision_save($serialYear,$serialNo,$session_userId);
		
		if($serialNo!='')
		{
			$response["type"]		= 'false';
			$response["msg"] 		= "Already Saved.Cant Edit"; 
			return $response;
		}
		if($permision_save!=1)
		{
			$response["type"]		= 'false';
			$response["msg"] 		= "No Permission to save"; 
			return $response;
		}
		
		
 	}
 	public function ValidateBeforeCancel($serialYear,$serialNo)
	{ 
		global $session_userId;
		
		$this->db->begin();
		$permision_cancel	= $this->get_permision_cancel2($serialYear,$serialNo,$session_userId);
		$response			= $this->getHeaderDetails_array2($serialYear,$serialNo);
		$this->db->commit();
		
 		if($response["STATUS"]=='10')
		{
			$response["type"]		= 'false';
			$response["msg"] 		= "This is already cancelled."; 
			return $response;
		}
 		else if($permision_cancel!=1)
		{
			$response["type"]		= 'false';
			$response["msg"] 		= "No Permission to cancel"; 
			return $response;
		}
		
	}

	private function LoadHeaderDetails_sql($serialYear,$serialNo)
	{
		global $session_companyId;
 		
		$sql = "SELECT
				finance_customer_settle_header.SETTLE_NO,
				finance_customer_settle_header.SETTLE_YEAR,
				finance_customer_settle_header.CUTOMER_ID,
				finance_customer_settle_header.CURRENCY_ID,
				finance_customer_settle_header.SETTLE_DATE,
				finance_customer_settle_header.`STATUS`,
				finance_customer_settle_header.CREATED_BY,
				finance_customer_settle_header.CREATED_DATE,
				finance_customer_settle_header.MODIFIED_BY,
				finance_customer_settle_header.MODIFIED_DATE,
				mst_customer.strName as CUSTOMER,
				mst_financecurrency.strCode AS CURRENCY
				FROM
				finance_customer_settle_header
				INNER JOIN mst_customer ON finance_customer_settle_header.CUTOMER_ID = mst_customer.intId
				INNER JOIN mst_financecurrency ON finance_customer_settle_header.CURRENCY_ID = mst_financecurrency.intId
				WHERE
				finance_customer_settle_header.SETTLE_NO = '$serialNo' AND
				finance_customer_settle_header.SETTLE_YEAR = '$serialYear'";
 		return $sql;
 	}
	
	private function LoadGridDetails_accounts_sql($customer)
	{
		global $session_companyId;
		
		$sql = "SELECT
				  GL.LEDGER_ID,
				  GL.COST_CENTER_ID,
				  D.strName                 AS COST_CENTER,
				  SUM(GL.PAY_AMOUNT_BALANCE) AS balAmmount,
				  PH.CUSTOMER_ID,
				  COA.CHART_OF_ACCOUNT_NAME,
				  COA1.CHART_OF_ACCOUNT_ID             AS COA1_CHART_OF_ACCOUNT_ID,
				  COA1.CHART_OF_ACCOUNT_NAME    AS COA1_CHART_OF_ACCOUNT_NAME
				FROM finance_customer_pay_receive_gl GL
				  INNER JOIN finance_customer_pay_receive_header PH
					ON GL.RECEIPT_NO = PH.RECEIPT_NO
					  AND GL.RECEIPT_YEAR = PH.RECEIPT_YEAR
				  LEFT JOIN mst_financedimension D
					ON GL.COST_CENTER_ID = D.intId
				  INNER JOIN finance_mst_chartofaccount COA
					ON GL.LEDGER_ID = COA.CHART_OF_ACCOUNT_ID
				  INNER JOIN finance_mst_chartofaccount COA1
					ON COA1.CATEGORY_ID = PH.COMPANY_ID
				WHERE COA.SUB_TYPE_ID = 14
					AND PH.CUSTOMER_ID = '$customer'
					AND COA1.CATEGORY_TYPE = 'CO'
				GROUP BY PH.COMPANY_ID, GL.COST_CENTER_ID
				HAVING balAmmount > 0
				ORDER BY COA.CHART_OF_ACCOUNT_NAME ASC, D.strName ASC
";
 		return $sql;
	}
	private function LoadSavedGridDetails_accounts_sql($serialYear,$serialNo,$customer,$ledgerId,$costCenter)
	{
		global $session_companyId;
		
		$sql = "SELECT
				finance_customer_pay_receive_gl.LEDGER_ID,
				finance_customer_pay_receive_gl.COST_CENTER_ID,
				mst_financedimension.strName AS COST_CENTER,
				Sum(finance_customer_pay_receive_gl.PAY_AMOUNT_BALANCE) AS balAmmount,
				finance_customer_pay_receive_header.CUSTOMER_ID,
				finance_mst_chartofaccount.CHART_OF_ACCOUNT_NAME,
				finance_customer_settle_details.AMOUNT
				FROM
				finance_customer_pay_receive_gl
				INNER JOIN finance_customer_pay_receive_header ON finance_customer_pay_receive_gl.RECEIPT_NO = finance_customer_pay_receive_header.RECEIPT_NO AND finance_customer_pay_receive_gl.RECEIPT_YEAR = finance_customer_pay_receive_header.RECEIPT_YEAR
				LEFT JOIN mst_financedimension ON finance_customer_pay_receive_gl.COST_CENTER_ID = mst_financedimension.intId
				INNER JOIN finance_mst_chartofaccount ON finance_customer_pay_receive_gl.LEDGER_ID = finance_mst_chartofaccount.CHART_OF_ACCOUNT_ID
				INNER JOIN finance_customer_settle_details ON 
				finance_customer_settle_details.SETTLE_NO = '$serialNo' AND
				finance_customer_settle_details.SETTLE_YEAR = '$serialYear' AND 
				finance_customer_pay_receive_gl.LEDGER_ID = finance_customer_settle_details.CHART_OF_ACCOUNT_NO AND 
				finance_customer_pay_receive_gl.COST_CENTER_ID = finance_customer_settle_details.COST_CENTER_ID
				WHERE 
				finance_mst_chartofaccount.SUB_TYPE_ID = 14 AND
				finance_customer_pay_receive_header.CUSTOMER_ID = '$customer' ";
		if($ledgerId!=''){		
		$sql .= "AND
				finance_customer_pay_receive_gl.CUSTOMER_ID = '$ledgerId' ";
		}
		if($costCenter!=''){		
		$sql .= "AND
				finance_customer_pay_receive_gl.COST_CENTER_ID = '$costCenter' ";
		}
				
		$sql .= "GROUP BY
				finance_customer_pay_receive_gl.LEDGER_ID,
				finance_customer_pay_receive_gl.COST_CENTER_ID ,
				finance_customer_pay_receive_header.CUSTOMER_ID
				/*HAVING balAmmount > 0 */
				ORDER BY
				finance_mst_chartofaccount.CHART_OF_ACCOUNT_NAME ASC,
				mst_financedimension.strName ASC";
 		return $sql;
	}
	
	private function LoadGridDetails_invoice_sql($customerId,$currencyId)
	{
		global $session_companyId;
		
		$sql = "SELECT
				  CIH.SERIAL_NO										AS SERIAL_NO,
				  CIH.SERIAL_YEAR									AS SERIAL_YEAR,
				  CIH.INVOICE_NO									AS INVOICE_NO,
				  CIH.ORDER_NO										AS ORDER_NO,
				  CIH.ORDER_YEAR									AS ORDER_YEAR,
				  CIH.INVOICED_DATE									AS INVOICED_DATE,
				  ROUND(SUM(CT.VALUE),2)							AS VALUE,
				  
				  ROUND(COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
 					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'INVOICE'),0),2) 		AS INVOICED_VALUE,

				  ROUND(COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
 					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'CREDIT'),0),2) 			AS CREDIT_VALUE,
					
					ROUND(COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
 					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'DEBIT'),0),2) 			AS DEBIT_VALUE,
					
				  ROUND(COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
 					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'ADVANCE'),0),2) 		AS ADVANCE_VALUE,
					
				ROUND(COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
					AND SUB_CT.INVOICE_YEAR IS NULL 
					AND SUB_CT.INVOICE_NO IS NULL 
					AND SUB_CT.DOCUMENT_TYPE = 'ADVANCE'),0),2) 		AS UNSETTLE_ADVANCE_VALUE,
					
				ROUND(COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
 					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'PAYRECEIVE'),0),2) 		AS RECEIVED_VALUE ,
					
				ROUND(COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
 					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'INV_SETTLEMENT'),0),2) 		AS INV_SETTLE_VALUE
	
				FROM finance_customer_transaction CT
				INNER JOIN finance_customer_invoice_header CIH ON CIH.SERIAL_YEAR = CT.INVOICE_YEAR AND CIH.SERIAL_NO = CT.INVOICE_NO
				WHERE CT.CUSTOMER_ID 	= $customerId
					AND CT.CURRENCY_ID 	= $currencyId
					AND CT.COMPANY_ID 	= $session_companyId
				GROUP BY CT.CUSTOMER_ID,CT.CURRENCY_ID,CIH.INVOICE_NO
				HAVING VALUE > 0";
		$result = $this->db->RunQuery($sql);
		while($row = mysqli_fetch_array($result))
		{
			$toBePaid					= round((abs($row["INVOICED_VALUE"]) + abs($row["DEBIT_VALUE"])) - (abs($row["CREDIT_VALUE"]) + abs($row["ADVANCE_VALUE"]) + abs($row["RECEIVED_VALUE"]) + abs($row["INV_SETTLE_VALUE"])),2);
			if($toBePaid<=0)
				continue;
			$data["TOBEPAID"]			= $toBePaid;
			$data["INVOICE_HTML"] 		= "<a id=\"".$row["SERIAL_YEAR"].'/'.$row["SERIAL_NO"]."\" target=\"rptinvoice.php\" href=\"../invoice/rptinvoice.php?SerialNo=".$row["SERIAL_NO"]."&SerialYear=".$row["SERIAL_YEAR"]."\">".$row["INVOICE_NO"]."</a>";
			$data["INVOICED_DATE"] 		= $row["INVOICED_DATE"];
			$data["ORDER_NO"] 			= $row["ORDER_YEAR"].'/'.$row["ORDER_NO"];
			$data["INVOICED_VALUE"] 	= abs($row["INVOICED_VALUE"]);			
			$data["CREDIT_VALUE"] 		= abs($row["CREDIT_VALUE"]);
			$data["ADVANCE_VALUE"] 		= abs($row["ADVANCE_VALUE"]);
			$data["RECEIVED_VALUE"] 	= abs($row["RECEIVED_VALUE"]);
			$data["DEBIT_VALUE"] 		= abs($row["DEBIT_VALUE"]);
			$data["UNSETTLE_ADVANCE"] 	= abs($row["UNSETTLE_ADVANCE_VALUE"]);
			
 			$data_array[]				= $data;
		}
		 $response["GRID"] 				= $data_array;
 		 return $response;
	}

	private function LoadSavedGridDetails_invoice_data($serialYear,$serialNo,$customerId,$currencyId)
	{
		global $session_companyId;
		
		$sql = "SELECT
				  CIH.SERIAL_NO										AS SERIAL_NO,
				  CIH.SERIAL_YEAR									AS SERIAL_YEAR,
				  CIH.INVOICE_NO									AS INVOICE_NO,
				  CIH.ORDER_NO										AS ORDER_NO,
				  CIH.ORDER_YEAR									AS ORDER_YEAR,
				  CIH.INVOICED_DATE									AS INVOICED_DATE,
				  CIH.REMARKS										AS INVOICE_REMARKS,
				  ROUND(SUM(CT.VALUE),2)							AS VALUE,
				  
				  ROUND(COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
 					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'INVOICE'),0),2) 		AS INVOICED_VALUE,

				  ROUND(COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
 					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'CREDIT'),0),2) 			AS CREDIT_VALUE,
					
					ROUND(COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
 					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'DEBIT'),0),2) 			AS DEBIT_VALUE,
					
				  ROUND(COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
 					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'ADVANCE'),0),2) 		AS ADVANCE_VALUE,
					
				ROUND(COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
					AND SUB_CT.INVOICE_YEAR IS NULL 
					AND SUB_CT.INVOICE_NO IS NULL 
					AND SUB_CT.DOCUMENT_TYPE = 'ADVANCE'),0),2) 		AS UNSETTLE_ADVANCE_VALUE,
					
				ROUND(COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
 					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'PAYRECEIVE'),0),2) 		AS RECEIVED_VALUE,
					
				ROUND(COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
 					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'INV_SETTLEMENT'),0),2) 		AS INV_SETTLEMENT , 
					
					finance_customer_settle_invoice_details.INVOICE_AMOUNT

	
				FROM finance_customer_transaction CT
				INNER JOIN finance_customer_invoice_header CIH ON CIH.SERIAL_YEAR = CT.INVOICE_YEAR AND CIH.SERIAL_NO = CT.INVOICE_NO
				LEFT JOIN finance_customer_settle_invoice_details ON CIH.SERIAL_NO = finance_customer_settle_invoice_details.INVOICE_NO 
				AND CIH.SERIAL_YEAR = finance_customer_settle_invoice_details.INVOICE_YEAR 
				AND finance_customer_settle_invoice_details.SETTLE_NO = '$serialNo' AND
finance_customer_settle_invoice_details.STLLET_YEAR = '$serialYear'
				WHERE CT.CUSTOMER_ID 	= $customerId
					AND CT.CURRENCY_ID 	= $currencyId
					AND CT.COMPANY_ID 	= $session_companyId
				GROUP BY CT.CUSTOMER_ID,CT.CURRENCY_ID,CIH.INVOICE_NO
				HAVING VALUE > 0";
				// echo $sql;
				if($serialNo==''){
					$sql='';
				}
		$result = $this->db->RunQuery($sql);
 		 return $result;
	}
	
	private function LoadReportHeaderDetails_sql($serialYear,$serialNo)
	{
		global $session_companyId;
 		
	$sql = "SELECT  
			PRH.SETTLE_NO		AS SERIAL_NO,
			PRH.COMPANY_ID		AS COMPANY_ID,
			PRH.SETTLE_DATE		AS SERIAL_DATE,
			PRH.SETTLE_DATE		AS RECEIPT_DATE,
			C.strCode 			AS CURRENCY_CODE,
			CU.strName			AS CUSTOMER_NAME,
			PRH.STATUS			AS STATUS
			FROM finance_customer_settle_header PRH 
 			INNER JOIN mst_financecurrency C ON C.intId = PRH.CURRENCY_ID
			INNER JOIN mst_customer CU ON CU.intId = PRH.CUTOMER_ID
			WHERE PRH.SETTLE_YEAR = '$serialYear' 
			AND SETTLE_NO = '$serialNo'";
 		return $sql;
 	}
	
	private function LoadReportGridDetails_invoice_sql($serialYear,$serialNo)
	{
		global $session_companyId;
		
	$sql = "SELECT CONCAT(CPRD.INVOICE_NO,'-',CPRD.INVOICE_YEAR) AS INVOICE_NO,
				  CPRD.INVOICE_AMOUNT	AS PAY_AMOUNT
			FROM finance_customer_settle_invoice_details CPRD
 			WHERE CPRD.STLLET_YEAR = $serialYear
			AND CPRD.SETTLE_NO = $serialNo";
 		return $sql;
	}

	private function LoadReportGridDetails_gl_sql($serialYear,$serialNo)
	{
		global $session_companyId;
		
	 $sql = "SELECT
			  CONCAT(FMT.FINANCE_TYPE_CODE,'',FMMT.MAIN_TYPE_CODE,'',FMST.SUB_TYPE_CODE,'',FCOA.CHART_OF_ACCOUNT_CODE,' - ',FCOA.CHART_OF_ACCOUNT_NAME) AS GL_NAME,
 			  D.strName								AS COST_CENTER,
			  GL.AMOUNT 						AS AMOUNT
			FROM finance_customer_settle_details GL
			INNER JOIN finance_mst_chartofaccount FCOA
			  ON FCOA.CHART_OF_ACCOUNT_ID = GL.CHART_OF_ACCOUNT_NO
			INNER JOIN finance_mst_account_sub_type FMST 
			  ON FMST.SUB_TYPE_ID = FCOA.SUB_TYPE_ID
			INNER JOIN finance_mst_account_main_type FMMT 
			  ON FMMT.MAIN_TYPE_ID = FMST.MAIN_TYPE_ID
			INNER JOIN finance_mst_account_type FMT 
			  ON FMT.FINANCE_TYPE_ID = FMMT.FINANCE_TYPE_ID
			INNER JOIN mst_financedimension D
			  ON D.intId = GL.COST_CENTER_ID 
 			WHERE SETTLE_NO = '$serialNo'
				AND SETTLE_YEAR = '$serialYear'";
 		return $sql;
	}
	
}

?>
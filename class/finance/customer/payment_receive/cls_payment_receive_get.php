<?php
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];

class Cls_Payment_Receive_Get
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
//BEGIN - PUBLIC FUNCTION {	
	public function LoadCustomerChange($customerId)
	{
		$responce = $this->LoadCustomerChange_sql($customerId);
		return json_encode($responce);
	}
	
	public function LoadMainDetails($customerId,$currencyId)
	{
		$responce = $this->LoadMainDetails_sql($customerId,$currencyId);
		return json_encode($responce);
	}
	
	public function GetCustomer($ledgerId)
	{
		$responce = $this->GetCustomer_sql($ledgerId);
		return json_encode($responce);
	}
	
	public function WhenChangeGL($GLID)
	{
		$responce = $this->WhenChangeGL_sql($GLID);
		return json_encode($responce);
	}
	
	public function ValidateBeforeCancel($receiptNo,$receiptYear)
	{
		return $this->ValidateBeforeCancel_sql($receiptNo,$receiptYear);
	}
	public function getPaymentReceiveReportHeader($serialNo,$serialYear)
	{
		return $this->getPaymentReceiveReportHeader_sql($serialNo,$serialYear);
	}
	public function getPaymentReceiveReportDetails($serialNo,$serialYear)
	{
		return $this->getPaymentReceiveReportDetails_sql($serialNo,$serialYear);
	}
	public function getPaymentReceiveReportDamage($serialNo,$serialYear)
	{
		return $this->getPaymentReceiveReportDamage_sql($serialNo,$serialYear);
	}
	public function UpdateStatus($serialNo,$serialYear)
	{
		$this->UpdateStatus_sql($serialNo,$serialYear);
	}
	
//END 	- PUBLIC FUNCTION }	

//BEGIN - PRIVATE FUNCTION {
	private function LoadCustomerChange_sql($customerId)
	{
		$sql = "SELECT DISTINCT
				  COA.intId									AS ID,
				  CONCAT(COA.strCode,' - ',COA.strName)		AS NAME
				  
				FROM mst_financechartofaccounts COA
				  INNER JOIN mst_financecustomeractivate CA
					ON COA.intId = CA.intChartOfAccountId
				WHERE CA.intCompanyId = '1'
					AND CA.intCustomerId = '$customerId'
				GROUP BY COA.intId
				ORDER BY COA.strCode ASC";
		$result = $this->db->RunQuery($sql);
			$data["GL_HTML"] = "<option value=\"".""."\">"."&nbsp;"."</option>";
		while($row = mysqli_fetch_array($result))
		{
			//$data["GL_HTML"] .= "<option value=\"".$row["ID"]."\">".$row["NAME"]."</option>";
		}
			$data["PayMethod"]	= $this->GetPayMethod($customerId);
		return $data;
	}
	
	private function LoadMainDetails_sql($customerId,$currencyId)
	{
		global $session_companyId;
		$invReportId	= 894;
		$debReportId	= 928;
		
		 $sql = "(SELECT
		 		  'PAYRECEIVE'											AS TYPE,
				  CIH.SERIAL_NO											AS SERIAL_NO,
				  CIH.SERIAL_YEAR										AS SERIAL_YEAR,
				  CIH.INVOICE_NO										AS INVOICE_NO,
				  CIH.ORDER_NO											AS ORDER_NO,
				  CIH.ORDER_YEAR										AS ORDER_YEAR,
				  CIH.INVOICED_DATE										AS INVOICED_DATE,
				  CIH.REMARKS											AS INVOICE_REMARKS,
				  ROUND(SUM(CT.VALUE),2)								AS VALUE,
				  
				  ROUND(COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
 					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'INVOICE'),0),2) 		AS INVOICED_VALUE,

				  ROUND(COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
 					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'CREDIT'),0),2) 			AS CREDIT_VALUE,
					
					ROUND(COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
 					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'DEBIT'),0),2) 			AS DEBIT_VALUE,
					
				  ROUND(COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
 					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'ADVANCE'),0),2) 		AS ADVANCE_VALUE,
					
				ROUND(COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
					AND SUB_CT.INVOICE_YEAR IS NULL 
					AND SUB_CT.INVOICE_NO IS NULL 
					AND SUB_CT.DOCUMENT_TYPE = 'ADVANCE'),0),2) 		AS UNSETTLE_ADVANCE_VALUE,
					
				ROUND(COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
 					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'PAYRECEIVE'),0),2) 		AS RECEIVED_VALUE,
					
				ROUND(COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
 					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'INV_SETTLEMENT'),0),2) 	AS INV_SETTLEMENT
					
				FROM finance_customer_transaction CT
				INNER JOIN finance_customer_invoice_header CIH 
					ON CIH.SERIAL_YEAR = CT.INVOICE_YEAR 
					AND CIH.SERIAL_NO = CT.INVOICE_NO
				WHERE CT.CUSTOMER_ID 	= $customerId
					AND CT.CURRENCY_ID 	= $currencyId
					AND CT.COMPANY_ID 	= $session_companyId
				GROUP BY
				 	CT.CUSTOMER_ID,
					CT.CURRENCY_ID,
					CIH.INVOICE_NO
				HAVING VALUE > 0)
				/*UNION
				(SELECT 
				   'DEBIT_RECEIVE'										AS TYPE,
				  CT.DOCUMENT_NO     									AS SERIAL_NO,
				  CT.DOCUMENT_YEAR   									AS SERIAL_YEAR,
				  CONCAT(CT.DOCUMENT_NO,'/',CT.DOCUMENT_YEAR,' - DEBIT NOTE')    AS INVOICE_NO,
				  'null'      											AS ORDER_NO,
				  'null'    											AS ORDER_YEAR,
				  DN.DEBIT_DATE 										AS INVOICED_DATE,
				  ''													AS INVOICE_REMARKS,
				  ROUND(SUM(CT.VALUE),2) 								AS VALUE,
				  ROUND(SUM(CT.VALUE),2) 								AS INVOICED_VALUE,
				  '0' 													AS CREDIT_VALUE,
				  '0' 													AS DEBIT_VALUE,
				  '0' 													AS ADVANCE_VALUE,
				  '0' 													AS UNSETTLE_ADVANCE_VALUE,
				  ROUND(COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
 					AND SUB_CT.INVOICE_YEAR = CT.DOCUMENT_YEAR
					AND SUB_CT.INVOICE_NO = CT.DOCUMENT_NO
					AND SUB_CT.DOCUMENT_TYPE = 'DEBIT_RECEIVE'),0),2) 	AS RECEIVED_VALUE,
				  '0'													AS INV_SETTLEMENT
				FROM finance_customer_transaction CT
				INNER JOIN finance_customer_debit_note_header DN 
					ON DN.DEBIT_NO = CT.DOCUMENT_NO 
					AND DN.DEBIT_YEAR = CT.DOCUMENT_YEAR
				WHERE CT.DOCUMENT_TYPE = 'DEBIT'
					AND CT.CUSTOMER_ID 	= $customerId
					AND CT.CURRENCY_ID 	= $currencyId
					AND CT.COMPANY_ID 	= $session_companyId
				GROUP BY CT.DOCUMENT_YEAR,CT.DOCUMENT_NO)*/";
				//echo $sql;
		$result = $this->db->RunQuery($sql);
		while($row = mysqli_fetch_array($result))
		{
			$toBePaid					= round(abs($row["INVOICED_VALUE"]) + abs($row["DEBIT_VALUE"]) - abs($row["CREDIT_VALUE"]) - abs($row["ADVANCE_VALUE"]) - abs($row["RECEIVED_VALUE"]) - abs($row["INV_SETTLEMENT"]),2);
			if($toBePaid<=0)
				continue;
			
			$data["TYPE"]				= $row["TYPE"];	
			$data["TOBEPAID"]			= $toBePaid;
			if($row["TYPE"]=="PAYRECEIVE")
				$data["INVOICE_HTML"] 		= "<a id=\"".$row["SERIAL_YEAR"].'/'.$row["SERIAL_NO"]."\" target=\"rptinvoice.php\" href=\"?q=".$invReportId."&SerialNo=".$row["SERIAL_NO"]."&SerialYear=".$row["SERIAL_YEAR"]."\">".$row["INVOICE_NO"]."</a>";
			else
				$data["INVOICE_HTML"] 		= "<a id=\"".$row["SERIAL_YEAR"].'/'.$row["SERIAL_NO"]."\" target=\"rptdebit_note.php\" href=\"?q=".$debReportId."&debitNoteNo=".$row["SERIAL_NO"]."&debitNoteYear=".$row["SERIAL_YEAR"]."\">".$row["INVOICE_NO"]."</a>";
				
			$data["INVOICED_DATE"] 		= $row["INVOICED_DATE"];
			$data["INVOICE_REMARKS"] 	= $row["INVOICE_REMARKS"];
			$data["ORDER_NO"] 			= $row["ORDER_YEAR"].'/'.$row["ORDER_NO"];
			$data["INVOICED_VALUE"] 	= abs($row["INVOICED_VALUE"]);			
			$data["CREDIT_VALUE"] 		= abs($row["CREDIT_VALUE"]);
			$data["ADVANCE_VALUE"] 		= abs($row["ADVANCE_VALUE"]);
			$data["RECEIVED_VALUE"] 	= abs($row["RECEIVED_VALUE"]);
			$data["DEBIT_VALUE"] 		= abs($row["DEBIT_VALUE"]);
			$data["UNSETTLE_ADVANCE"] 	= abs($row["UNSETTLE_ADVANCE_VALUE"]);
			$data["INV_SETTLEMENT"] 	= abs($row["INV_SETTLEMENT"]);
			
			$data["UN_SETTLE"]			= $this->CheckIsUnsettleAdvance($row["ORDER_YEAR"],$row["ORDER_NO"]);
			$data_array[]				= $data;
		}
		 $response["GRID"] 				= $data_array;
		 $response["GL_GRID"][]			= $this->CreateGLRow($customerId);
		 return $response;
	}
	
	private function GetPayMethod($customerId)
	{
		$sql 	= "SELECT intPaymentsMethodsId AS PayTerm FROM mst_customer WHERE intId = '$customerId'";
		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row["PayTerm"];
	}
	
	private function CheckIsUnsettleAdvance($orderYear,$orderNo)
	{
		$bgColor = '#FFFFF';
		
		$sql = "SELECT
				  	INVOICE_YEAR
				FROM finance_customer_transaction 
				WHERE ORDER_YEAR = '$orderYear'
					AND ORDER_NO = '$orderNo'
					AND INVOICE_YEAR IS NULL 
					AND INVOICE_NO IS NULL 
					AND VALUE <> 0";
		$result = $this->db->RunQuery($sql);
		while($row = mysqli_fetch_array($result))
		{
			$bgColor = '#B8F9BD';
		}
		return $bgColor;
	}
	
	private function GetCustomer_sql($ledgerId)
	{
		global $session_companyId;
		
		if($ledgerId != "")
			$wherSql .= "AND FCA.intChartOfAccountId = '$ledgerId' ";			
			
		$sql = "SELECT DISTINCT
				  CU.intId   AS CUSTOMER_ID,
				  CU.strName AS CUSTOMER_NAME
				FROM mst_customer CU
				INNER JOIN finance_customer_transaction FCT ON FCT.CUSTOMER_ID = CU.intId
				INNER JOIN mst_financecustomeractivate FCA ON CU.intId = FCA.intCustomerId AND FCT.COMPANY_ID = FCA.intCompanyId
				WHERE FCT.COMPANY_ID = '$session_companyId'
				$wherSql
				GROUP BY FCT.INVOICE_NO,FCT.INVOICE_YEAR
				HAVING ROUND(SUM(FCT.VALUE),2)>0
				ORDER BY strName";
		$result = $this->db->RunQuery($sql);
			$string = "<option value=\"".""."\">&nbsp;</option>";
		while($row = mysqli_fetch_array($result))
		{
			$string .= "<option value=\"".$row["CUSTOMER_ID"]."\">".$row["CUSTOMER_NAME"]."</option>";
		}
		return $string;
	}
	
	private function CreateGLRow($customerId)
	{		
		$array["GL_HTML"]	= $this->GetInvoiceTypeGL($customerId);
		return $array;
	}
	
	private function GetInvoiceTypeGL($customerId)
	{
		$sql 	= "SELECT
					  FCOA.CHART_OF_ACCOUNT_ID,
					  CONCAT(FMT.FINANCE_TYPE_CODE,'',FMMT.MAIN_TYPE_CODE,'',FMST.SUB_TYPE_CODE,'',FCOA.CHART_OF_ACCOUNT_CODE) AS ACCOUNT_CODE,
					  FCOA.CHART_OF_ACCOUNT_NAME
					FROM finance_mst_chartofaccount FCOA
					INNER JOIN finance_mst_account_sub_type FMST ON FMST.SUB_TYPE_ID=FCOA.SUB_TYPE_ID
					INNER JOIN finance_mst_account_main_type FMMT ON FMMT.MAIN_TYPE_ID=FMST.MAIN_TYPE_ID
					INNER JOIN finance_mst_account_type FMT ON FMT.FINANCE_TYPE_ID=FMMT.FINANCE_TYPE_ID
					WHERE FCOA.CATEGORY_TYPE = 'C'
						AND FCOA.CATEGORY_ID = '$customerId'";
		$result = $this->db->RunQuery($sql);		
		$row_count	= mysqli_num_rows($result);
		while($row	= mysqli_fetch_array($result))
		{
			$string .= "<option value=\"".$row["CHART_OF_ACCOUNT_ID"]."\">".$row["ACCOUNT_CODE"].'-'.$row["CHART_OF_ACCOUNT_NAME"]."</option>";
		}
		return $string;
	}
	
	private function WhenChangeGL_sql($GLID)
	{		
		$sql = "SELECT FAST.SUB_TYPE_NAME
				FROM finance_mst_chartofaccount FCOA
				INNER JOIN finance_mst_account_sub_type FAST ON FAST.SUB_TYPE_ID = FCOA.SUB_TYPE_ID
				WHERE FCOA.CHART_OF_ACCOUNT_ID = $GLID
				  AND FAST.SUB_TYPE_NAME in ('RELATED PARTIES')";
		$result = $this->db->RunQuery($sql);
		$row_count	= mysqli_num_rows($result);
		if($row_count > 0){
			$respocse['ADD']	= 'C';
		}else{
			$respocse['ADD']	= 'D';
		}
		return $respocse;
	}
	
	private function ValidateBeforeCancel_sql($receiptNo,$receiptYear)
	{
		$response	= $this->CheckStatus($receiptNo,$receiptYear);
		if($response["STATUS"]=='10')
		{
			$response["type"]		= 'false';
			$response["msg"] 		= "Payment Received No : ".$response["RECEIPT_NO"]." already canceled."; 
			return $response;
		}
	}
	
	private function CheckStatus($receiptNo,$receiptYear)
	{
		$sql = "SELECT STATUS,CONCAT(RECEIPT_NO,'/',RECEIPT_YEAR)AS RECEIPT_NO FROM finance_customer_pay_receive_header 
				WHERE RECEIPT_NO = '$receiptNo' 
					AND RECEIPT_YEAR = '$receiptYear'";
		$result = $this->db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return $row;
	}
	private function getPaymentReceiveReportHeader_sql($serialNo,$serialYear)
	{
		$sql = "SELECT
				CU.strName          AS CUSTOMER_NAME,
				CU.strAddress       AS ADDRESS,
				CU.strCity          AS CITY,
				CO.strCountryName   AS COUNTRY_NAME,
				PRH.RECEIPT_NO	    AS SERIAL_NO,
				PRH.COMPANY_ID	    AS COMPANY_ID,
				PRH.LOCATION_ID	    AS LOCATION_ID,
				PRH.RECEIPT_DATE    AS SERIAL_DATE,
				PRH.RECEIPT_DATE    AS RECEIPT_DATE,
				PRH.CURRENCY_ID    	AS CURRENCY_ID,
				PM.strName	    	AS PAYMENT_MODE,
				C.strCode 	    	AS CURRENCY_CODE,
				PRH.REMARKS	   	 	AS REMARKS,
				PRH.STATUS	    	AS STATUS,
				PRH.PRINT_STATUS	AS PRINT_STATUS,
				U.strUserName		AS CREATED_BY_NAME,
				(SELECT SUM(FCPRD.PAY_AMOUNT)
				FROM finance_customer_pay_receive_details FCPRD 
				WHERE 
				FCPRD.RECEIPT_NO = PRH.RECEIPT_NO
				AND FCPRD.RECEIPT_YEAR = PRH.RECEIPT_YEAR) AS VALUE,
				(SELECT COA.CHART_OF_ACCOUNT_NAME
				FROM finance_mst_chartofaccount COA
				INNER JOIN finance_customer_pay_receive_gl PRGL ON PRGL.LEDGER_ID=COA.CHART_OF_ACCOUNT_ID
				WHERE PRGL.RECEIPT_NO=PRH.RECEIPT_NO AND
				PRGL.RECEIPT_YEAR=PRH.RECEIPT_YEAR AND
				COA.BANK = 1
				) AS BANK_GL_NAME
				FROM finance_customer_pay_receive_header PRH 
				INNER JOIN mst_customer CU ON CU.intId = PRH.CUSTOMER_ID
				INNER JOIN mst_financecurrency C ON C.intId = PRH.CURRENCY_ID
				INNER JOIN sys_users U ON U.intUserId = PRH.CREATED_BY
				INNER JOIN mst_financepaymentsmethods PM ON PM.intId = PRH.PAYMENT_MODE
				LEFT JOIN mst_country CO ON CO.intCountryID = CU.intCountryId
				WHERE PRH.RECEIPT_NO = '$serialNo'
				AND PRH.RECEIPT_YEAR = '$serialYear' ";
		
		$result = $this->db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return $row;
	}
	private function getPaymentReceiveReportDetails_sql($serialNo,$serialYear)
	{
		$sql = "SELECT
				OH.strCustomerPoNo      AS CUSTOMER_PO_NO,
				OD.strSalesOrderNo      AS SALES_ORDER_NO,
				OD.strGraphicNo         AS GRAPHIC_NO,
				OD.strStyleNo           AS STYLE_NO,
				FUI.strName             AS ITEM_NAME,
				SUM(PID.PAY_AMOUNT)     AS PAY_AMOUNT,
				CIH.INVOICE_NO			AS INVOICE_NO
				FROM finance_customer_pay_receive_details PID 
				INNER JOIN finance_customer_invoice_details CID ON PID.INVOICE_NO=CID.SERIAL_NO AND 
				PID.INVOICE_YEAR=CID.SERIAL_YEAR
				INNER JOIN finance_customer_invoice_header CIH ON CIH.SERIAL_YEAR = CID.SERIAL_YEAR AND 
				CIH.SERIAL_NO = CID.SERIAL_NO
				INNER JOIN trn_orderdetails OD ON OD.intOrderYear = CIH.ORDER_YEAR AND 
				OD.intOrderNo = CIH.ORDER_NO AND 
				OD.intSalesOrderId = CID.SALES_ORDER_ID
				INNER JOIN trn_orderheader OH ON OH.intOrderNo = OD.intOrderNo AND 
				OH.intOrderYear = OD.intOrderYear
				INNER JOIN mst_financecustomeritem FUI ON FUI.intId = CID.GL_ACCOUNT
				WHERE PID.RECEIPT_NO = '$serialNo'
				AND PID.RECEIPT_YEAR = '$serialYear'
				GROUP BY PID.INVOICE_NO ,PID.INVOICE_YEAR ";
		
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	private function getPaymentReceiveReportDamage_sql($serialNo,$serialYear)
	{
		$sql = "SELECT COA.CHART_OF_ACCOUNT_NAME,
				PRGL.PAY_AMOUNT
				FROM finance_customer_pay_receive_gl PRGL
				INNER JOIN finance_mst_chartofaccount COA ON COA.CHART_OF_ACCOUNT_ID=PRGL.LEDGER_ID
				WHERE COA.CATEGORY_TYPE = 'N' AND
				COA.BANK = 0 AND
				PRGL.RECEIPT_NO = '$serialNo' AND
				PRGL.RECEIPT_YEAR = '$serialYear' ";
		
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	private function UpdateStatus_sql($serialNo,$serialYear)
	{
		$sql = "UPDATE finance_customer_pay_receive_header 
				SET
				PRINT_STATUS = '1'
				WHERE
				RECEIPT_NO = '$serialNo' AND 
				RECEIPT_YEAR = '$serialYear' ";
		$this->db->RunQuery($sql);
	}
//END 	- PRIVATE FUNCTION }
}
?>
<?php
include_once  "../../../../class/cls_commonFunctions_get.php";
include_once ("../../../../class/finance/customer/cls_common_get.php");
include_once ("../../../../class/finance/customer/advance/cls_advance_set.php");
$obj_common			= new cls_commonFunctions_get($db);
$obj_common_get		= new Cls_Common_Get($db);
$obj_advance_set	= new Cls_Advance_Set($db);
$session_userId		= $_SESSION["userId"];
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];

class Cls_Payment_Receive_Set
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	
	public function Save($customerId,$currencyId,$remarks,$detailArray,$glArray,$receiptDate,$paymentMode)
	{
		global $obj_common;
		global $booSavedStatus;
		global $savedMasseged;
		global $session_locationId;
		global $pub_ErrorSql;
		global $obj_common_get;
		global $obj_advance_set;
		
		$booSavedStatus	= true;
		
		$this->db->begin();
		$serial_array 	= $obj_common->GetSystemMaxNo('intFin_PaymentReceiveNo',$session_locationId);
		$receiptNo		= $serial_array["max_no"];
		$receiptYear	= date('Y');
		
		$this->SaveHeader($receiptNo,$receiptYear,$customerId,$currencyId,$remarks,$receiptDate,$paymentMode);
		
		foreach($detailArray as $detailLoop)
		{
			$invoiceNo_array	= explode('/',$detailLoop["InvoiceNo"]);
			$invoiceYear		= $invoiceNo_array[0];
			$invoiceNo			= $invoiceNo_array[1];
			$payAmount			= $detailLoop["PayAmount"];
			$toBePayAmount		= $detailLoop["ToBeReceive"];
			$orderNo_arrary		= explode('/',$detailLoop["OrderNo"]);
			$orderNo			= $orderNo_arrary[1];
			$orderYear			= $orderNo_arrary[0];
			//============================comment by lahiru due to exceed received settlement (2015-07-03)=========================
			//if($toBePayAmount<$payAmount)
				//$advanceAmount	+= $payAmount - $toBePayAmount;
			//========================================================================================================
			
			$this->SaveDetails($receiptNo,$receiptYear,$invoiceYear,$invoiceNo,$payAmount);
			
			/*
			BEGIN - COMMENTER THIS LINE BECUASE USER MUST ABLE TO ENTER MORE THAN INVOICE VALUE & SYSTEM WILL RAISE PAYMENT ADVANCE FOR EXCEEDED VALUE AUTOMATICALLY {
				=========== This has stop because now exceed receivable settlement program developed (2015-07-03)================================
				
			$this->ValidateDetailSaving($customerId,$currencyId,$invoiceYear,$invoiceNo,$payAmount);
			END 	- } 
			*/
			
			//BEGIN - INSERT INTO FINANCE_CUSTOMER_TRANSACTION TABLE {						
			foreach($glArray as $arrValGL)
			{
				if($this->CheckIsCustomerGL($arrValGL['GLAccount'],$customerId))
					$customerGLId	= $arrValGL['GLAccount'];
			}
			//$invoiceValue 		= ($detailLoop["ToBeReceive"]<$detailLoop['PayAmount']?$detailLoop["ToBeReceive"]:$detailLoop['PayAmount']);
			$invoiceValue			= $detailLoop['PayAmount'];
			$this->SaveTransaction($receiptNo,$receiptYear,$invoiceYear,$invoiceNo,$invoiceValue,$orderNo,$orderYear,$customerId,$currencyId,$customerGLId,$detailLoop['Type'],$receiptDate);			
			//END	- }
			
		}
		
		//BEGIN - INSET RECORDS TO FINANCE_CUSTOMER_PAY_RECEIVE_GL TABLE {
		foreach($glArray as $glLoop)
		{
			$value				= ($glLoop["GLValue_DR"]=='0'?$glLoop["GLValue_CR"]:$glLoop["GLValue_DR"]);
			
			$this->SaveGLDetails($receiptNo,$receiptYear,$glLoop["GLAccount"],$glLoop["GLCostCenter"],$glLoop["GLRemarks"],$value,$glLoop["TransType"]);
			
			//BEGIN - UPDATE CUSTOMER RECEIVE AMOUNT INTO FORECAST TABLE {					
			$response = $obj_common_get->UpdateForecast($value,$currencyId,$receiptDate,$customerId,$glLoop["GLAccount"]);
			if(!$response["type"])
			{
				$booSavedStatus	= false;
				$savedMasseged 	= $response['msg'];
				$pub_ErrorSql 	= $response['sql'];				
			}
			//END	- UPDATE CUSTOMER RECEIVE AMOUNT INTO FORECAST TABLE }
		}
		//END - }
		
		//BEGIN - INSERT RECORDS TO FINANCE_TRANSACTION TABLE {
		foreach($glArray as $glLoop)
		{
			$value				= ($glLoop["GLValue_DR"]=='0'?$glLoop["GLValue_CR"]:$glLoop["GLValue_DR"]);
			$response 	= $obj_common_get->Save_Jurnal_Transaction($glLoop["GLAccount"],$value,$receiptNo,$receiptYear,'PAYRECEIVE','null','null',$glLoop["TransType"],'CU',$customerId,$currencyId,$glLoop["GLRemarks"],$receiptDate);
			if($response['type']=='fail')
			{
				$booSavedStatus	= false;
				$savedMasseged	= $response['ErrorMsg'];
				$pub_ErrorSql	= $response['ErrorSql'];
			}
		}
		//END - }

		//BEGIN - Cr {
		/*	
		$i			  = 0;
		$finalInvBal  = 0;
		$glBal		  = 0;
		foreach($glArray as $arrValGL)
		{
			if(!$this->CheckIsCustomerGL($arrValGL['GLAccount'],$customerId))
				continue;
				
			$accountId 		= $arrValGL['GLAccount'];
			$glValue 		= ($arrValGL["GLValue_DR"]=='0'?$arrValGL["GLValue_CR"]:$arrValGL["GLValue_DR"]);
			$glBal   		= $glValue;
			$n				= 0;
			
			foreach($detailArray as $arrVal)
			{
				$invoiceNoArr 	= explode('/',trim($arrVal['InvoiceNo']));
				$PONoArr 		= explode('/',trim($arrVal['OrderNo']));
				$PONo			= $PONoArr[1];
				$POYear			= $PONoArr[0];
				$invoiceNo		= $invoiceNoArr[1];
				$invoiceYear	= $invoiceNoArr[0];
				$invValue 		= ($detailLoop["ToBeReceive"]<$detailLoop['PayAmount']?$detailLoop["ToBeReceive"]:$detailLoop['PayAmount']);
				
				if(++$n<$i)
				{
						
				}
				else
				{
					if($finalInvBal>0)
					{
						$invValue = $finalInvBal;
					}
					else
					{
						$i++;
					}
					if($glBal>$invValue)
					{
						$this->SaveTransaction($receiptNo,$receiptYear,$invoiceYear,$invoiceNo,$invValue,$PONo,$POYear,$customerId,$currencyId,$accountId,$arrVal['Type'],$receiptDate);
						$glBal			-= $invValue;
						$finalInvBal = 0;
					}
					else
					{
						$this->SaveTransaction($receiptNo,$receiptYear,$invoiceYear,$invoiceNo,$glBal,$PONo,$POYear,$customerId,$currencyId,$accountId,$arrVal['Type'],$receiptDate);
						$finalInvBal 	= $invValue - $glBal;
						$glBal 			= 0;
					}
				}
			}
		}
		*/
		//END	- }
		
		//BEGIN - SAVE PAYMENT ADVANCE TRANSACTION AUTOMATICALLY WHEN ADVANE AMOUNT AVAILABEL {
			//========================= comment by lahiru due to exceed received settlement (2015-07-03) =======================================
			/*if($advanceAmount>0)
			{				
				$no_array 		= $obj_common->GetSystemMaxNo('intFin_Customer_AdvanceNo',$session_locationId);
				$advanceNo		= $no_array["max_no"];
				$advanceYear	= date('Y');
				
				$obj_advance_set->SaveHeader($advanceNo,$advanceYear,$customerId,'null','null',$currencyId,$paymentMode,'null',$remarks,$receiptDate,'null');
				
				foreach($glArray as $glLoop)
				{
					if($this->CheckIsBankGL($glLoop["GLAccount"]))
					{
						$obj_advance_set->SaveDetails($advanceNo,$advanceYear,$glLoop["GLAccount"],$advanceAmount,$glLoop["GLRemarks"],$glLoop["GLCostCenter"]);
						$obj_advance_set->SaveTransaction($advanceNo,$advanceYear,'null','null',$advanceAmount,'null','null',$customerId,$currencyId,$glLoop["GLAccount"],$receiptDate);						
					}						
				}				
			}*/
			//=======================================================================================================================
		//END	- }
		
		$resultArr	= $obj_common->validateDuplicateSerialNoWithSysNo($receiptNo,'intFin_PaymentReceiveNo',$session_locationId);
		if($resultArr['type']=='fail' && $booSavedStatus)
		{
			$booSavedStatus	= false;
			$savedMasseged	= $resultArr['msg'];
		}

		if($booSavedStatus){
			$this->db->commit();
			$response['type'] 		= "pass";
			$response['msg'] 		= "Payment Receipt No : '$receiptYear/$receiptNo' saved successfully.";
			$response['ReceiptNo'] 	= $receiptNo;
			$response['ReceiptYear']= $receiptYear;
		}else{
			$this->db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= $savedMasseged;
			$response['errorSql']	= $pub_ErrorSql;
		}
		return json_encode($response);
	}
	
	public function Cancel($receiptNo,$receiptYear)
	{
		global $booSavedStatus;
		global $savedMasseged;
		global $pub_ErrorSql;		
		global $obj_common_get;
		
		$booSavedStatus	= true;
		$this->db->begin();
		
		$this->Cancel_sql($receiptNo,$receiptYear);
		
		$trans_response = $obj_common_get->RemoveTransaction($receiptNo,$receiptYear,'PAYRECEIVE');
		if($trans_response["type"])
		{
			$booSavedStatus	= false;
			$savedMasseged	= $trans_response["msg"];
			$pub_ErrorSql		= $trans_response["errorSql"];
		}
		
		$trans_response = $obj_common_get->RemoveFinanceMainTransaction($receiptNo,$receiptYear,'PAYRECEIVE');
		if($trans_response["type"])
		{
			$booSavedStatus	= false;
			$savedMasseged	= $trans_response["msg"];
			$pub_ErrorSql		= $trans_response["errorSql"];
		}		
		
		if($booSavedStatus){
			$this->db->commit();
			$response['type'] 		= "pass";
			$response['msg'] 		= "Canceled successfully.";
		}else{
			$this->db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= $savedMasseged;
			$response['errorSql']	= $pub_ErrorSql;
		}
		return json_encode($response);
	}
	public function UpdateStatus($serialNo,$serialYear)
	{
		$this->UpdateStatus_sql($serialNo,$serialYear);
	}
	public function confirmPaymnet($receiptNo,$receiptYear)
	{
		return $this->confirmPaymnet_sql($receiptNo,$receiptYear);
	}
	private function SaveHeader($receiptNo,$receiptYear,$customerId,$currencyId,$remarks,$receiptDate,$paymentMode)
	{
		global $session_companyId;
		global $session_locationId;
		global $session_userId;
		global $savedMasseged;
		global $booSavedStatus;
		global $pub_ErrorSql;
		
		$sql = "INSERT INTO finance_customer_pay_receive_header 
				(RECEIPT_NO, 
				RECEIPT_YEAR, 
				CUSTOMER_ID, 
				CURRENCY_ID, 
				PAYMENT_MODE,
				REMARKS, 
				RECEIPT_DATE,
				STATUS, 
				COMPANY_ID, 
				LOCATION_ID, 
				CREATED_BY, 
				CREATED_DATE)
				VALUES
				('$receiptNo', 
				'$receiptYear', 
				'$customerId', 
				'$currencyId',
				'$paymentMode', 
				'$remarks', 
				'$receiptDate',
				'1', 
				'$session_companyId', 
				'$session_locationId', 
				'$session_userId', 
				NOW());";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$booSavedStatus	= false;
			$savedMasseged	= $this->db->errormsg;
			$pub_ErrorSql		= $sql;
		}
	}
	
	private function SaveDetails($receiptNo,$receiptYear,$invoiceYear,$invoiceNo,$payAmount)
	{
		global $savedMasseged;
		global $booSavedStatus;
		global $pub_ErrorSql;
		
		$sql = "INSERT INTO finance_customer_pay_receive_details
				(RECEIPT_NO,
				RECEIPT_YEAR,
				INVOICE_NO,
				INVOICE_YEAR,
				PAY_AMOUNT)
				VALUES 
				('$receiptNo',
				'$receiptYear',
				'$invoiceNo',
				'$invoiceYear',
				'$payAmount');";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$booSavedStatus	= false;
			$savedMasseged	= $this->db->errormsg;
			$pub_ErrorSql		= $sql;
		}
	}
	
	private function SaveGLDetails($receiptNo,$receiptYear,$ledgerId,$costCenter,$remarks,$value,$transType)
	{
		global $savedMasseged;
		global $booSavedStatus;
		global $pub_ErrorSql;
		
		$sql = "INSERT INTO finance_customer_pay_receive_gl
				(RECEIPT_NO,
				RECEIPT_YEAR,
				LEDGER_ID,
				COST_CENTER_ID,
				REMARKS,
				PAY_AMOUNT,
				PAY_AMOUNT_BALANCE,
				TRANSACTION_TYPE)
				VALUES 
				('$receiptNo',
				'$receiptYear',
				'$ledgerId',
				'$costCenter',
				'$remarks',
				'$value',
				'$value',
				'$transType');";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$booSavedStatus	= false;
			$savedMasseged	= $this->db->errormsg;
			$pub_ErrorSql		= $sql;
		}
	}
	
	private function SaveTransaction($receiptNo,$receiptYear,$invoiceYear,$invoiceNo,$payAmount,$orderNo,$orderYear,$customerId,$currencyId,$ledgerId,$type,$receiptDate)
	{
		global $savedMasseged;
		global $booSavedStatus;
		global $session_companyId;
		global $session_locationId;
		global $session_userId;
		global $pub_ErrorSql;
		
		if($payAmount<=0)
			return;
		
		$sql = "INSERT INTO finance_customer_transaction 
				(ORDER_YEAR, 
				ORDER_NO, 
				CUSTOMER_ID, 
				CURRENCY_ID, 
				DOCUMENT_YEAR, 
				DOCUMENT_NO, 
				DOCUMENT_TYPE, 
				INVOICE_YEAR, 
				INVOICE_NO, 
				LEDGER_ID, 
				VALUE, 
				COMPANY_ID, 
				LOCATION_ID, 
				USER_ID, 
				TRANSACTION_DATE_TIME,
				SYSTEM_MODIFIED_DATE)
				VALUES
				($orderYear, 
				$orderNo, 
				'$customerId', 
				'$currencyId', 
				'$receiptYear', 
				'$receiptNo', 
				'$type', 
				'$invoiceYear', 
				'$invoiceNo', 
				'$ledgerId', 
				'-$payAmount', 
				'$session_companyId', 
				'$session_locationId', 
				'$session_userId',
				'$receiptDate',
				NOW());";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$booSavedStatus	= false;
			$savedMasseged	= $this->db->errormsg;
			$pub_ErrorSql		= $sql;
		}
	}
	
	private function ValidateDetailSaving($customerId,$currencyId,$invoiceYear,$invoiceNo,$payAmount)
	{
		global $savedMasseged;
		global $booSavedStatus;
		global $pub_ErrorSql;
		global $session_companyId;
		$sql = "SELECT
				  CIH.INVOICE_NO										AS INVOICE_NO,
				  ROUND(SUM(CT.VALUE),2)								AS VALUE,
				  
				  COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'INVOICE'),0) 			AS INVOICED_VALUE,
				
				  COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'DEBIT'),0) 				AS DEBIT_VALUE,
					
				  COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'CREDIT'),0) 			AS CREDIT_VALUE,
					
				  COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'ADVANCE'),0) 			AS ADVANCE_VALUE,
					
				COALESCE((SELECT
					 SUM(SUB_CT.VALUE)
				   FROM finance_customer_transaction SUB_CT 
				   WHERE SUB_CT.CUSTOMER_ID = CT.CUSTOMER_ID
					AND SUB_CT.CURRENCY_ID = CT.CURRENCY_ID
					AND SUB_CT.INVOICE_YEAR = CT.INVOICE_YEAR
					AND SUB_CT.INVOICE_NO = CT.INVOICE_NO
					AND SUB_CT.DOCUMENT_TYPE = 'PAYRECEIVE'),0) 		AS PAID_VALUE
					
				FROM finance_customer_transaction CT
				INNER JOIN finance_customer_invoice_header CIH ON CIH.SERIAL_YEAR = CT.INVOICE_YEAR AND CIH.SERIAL_NO = CT.INVOICE_NO
				WHERE 
					CT.CUSTOMER_ID 		= $customerId
					AND CT.CURRENCY_ID 	= $currencyId
					AND CIH.SERIAL_NO 	= '$invoiceNo'
					AND CIH.SERIAL_YEAR = '$invoiceYear'
					AND CT.COMPANY_ID 	= $session_companyId
				GROUP BY CIH.SERIAL_NO,CIH.SERIAL_YEAR";
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		$toBePaid = round(abs($row["INVOICED_VALUE"]) - (abs($row["DEBIT_VALUE"]) + abs($row["CREDIT_VALUE"]) + abs($row["ADVANCE_VALUE"]) + abs($row["PAID_VALUE"])),2);
		//die($payAmount .'|'. $toBePaid);
		$invoiceNo	= $row["INVOICE_NO"];
		if($toBePaid < $payAmount){
			$booSavedStatus	= false;
			$savedMasseged	= "No balance amount available for Invoice No : $invoiceNo";
			$pub_ErrorSql		= $sql;
		}
	}
	
	private function Cancel_sql($receiptNo,$receiptYear)
	{
		global $booSavedStatus;
		global $savedMasseged;
		global $pub_ErrorSql;
		
		$sql = "UPDATE finance_customer_pay_receive_header
				SET STATUS = '10'
				WHERE RECEIPT_NO = '$receiptNo'
					AND RECEIPT_YEAR = '$receiptYear';";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$booSavedStatus	= false;
			$savedMasseged	= $this->db->errormsg;
			$pub_ErrorSql		= $sql;
		}
		
	}
	
	private function CheckIsBankGL($glAccount)
	{
		$sql = "SELECT CHART_OF_ACCOUNT_ID FROM finance_mst_chartofaccount WHERE CHART_OF_ACCOUNT_ID = '$glAccount' AND BANK = 1";
		$result = $this->db->RunQuery2($sql);
		if(mysqli_fetch_row($result)>0)
			return true;
		else
			return false;
	}
	
	private function CheckIsCustomerGL($glId,$customerId)
	{
		$sql = "SELECT CHART_OF_ACCOUNT_ID FROM finance_mst_chartofaccount WHERE CATEGORY_TYPE = 'C' AND CATEGORY_ID = '$customerId'";
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		if($row["CHART_OF_ACCOUNT_ID"] == $glId)
			return true;
		else
			return false;
	}
	private function UpdateStatus_sql($serialNo,$serialYear)
	{
		$sql = "UPDATE finance_customer_pay_receive_header 
				SET
				PRINT_STATUS = '1'
				WHERE
				RECEIPT_NO = '$serialNo' AND 
				RECEIPT_YEAR = '$serialYear' ";
		$this->db->RunQuery($sql);
	}
	private function confirmPaymnet_sql($receiptNo,$receiptYear)
	{
		$sql	= "UPDATE finance_customer_pay_receive_header 
					SET
					RECEIPT_CONFIRMATION = '1'
					WHERE
					RECEIPT_NO = '$receiptNo' AND 
					RECEIPT_YEAR = '$receiptYear' ";
					
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
}
?>
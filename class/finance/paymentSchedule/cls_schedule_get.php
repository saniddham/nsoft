<?php
$session_companyId 	= $_SESSION["headCompanyId"];
$session_userId		= $_SESSION["userId"];

include_once  $_SESSION['ROOT_PATH']."class/cls_commonErrorHandeling_get.php";

$obj_errorHandeling = new cls_commonErrorHandeling_get($db);

class Cls_Schedule_Get
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	public function getRptHeaderData($scheduleNo,$scheduleYear)
	{
		return $this->loadRptHeaderData_sql($scheduleNo,$scheduleYear);
	}
	public function getRptDetailData($scheduleNo,$scheduleYear)
	{
		return $this->loadDetailData_sql($scheduleNo,$scheduleYear);
	}
	public function getRptApproveDetails($scheduleNo,$scheduleYear)
	{
		return $this->getRptApproveDetails_sql($scheduleNo,$scheduleYear);
	}
	public function loadHeaderData($scheduleNo,$scheduleYear)
	{
		return $this->getHeaderData($scheduleNo,$scheduleYear);
	}
	public function loadDetailData($scheduleNo,$scheduleYear)
	{
		return $this->loadDetailData_sql($scheduleNo,$scheduleYear);
	}
	public function validateBeforeApprove($scheduleNo,$scheduleYear,$programCode)
	{
		global $obj_errorHandeling;
		global $session_userId;
		
		$header_arr	= $this->getHeaderData($scheduleNo,$scheduleYear);
		$response 	= $obj_errorHandeling->get_permision_withApproval_confirm($header_arr['STATUS'],$header_arr['APPROVE_LEVELS'],$session_userId,$programCode,'RunQuery');
		
		return $response;
	}
	public function validateBeforeReject($scheduleNo,$scheduleYear,$programCode)
	{
		global $obj_errorHandeling;
		global $session_userId;
		
		$header_arr	= $this->getHeaderData($scheduleNo,$scheduleYear);
		$response 	= $obj_errorHandeling->get_permision_withApproval_reject($header_arr['STATUS'],$header_arr['APPROVE_LEVELS'],$session_userId,$programCode,'RunQuery');
		
		return $response;
	}
	public function validateBeforeCancel($scheduleNo,$scheduleYear,$programCode)
	{
		global $obj_errorHandeling;
		global $session_userId;
		
		$header_arr	= $this->getHeaderData($scheduleNo,$scheduleYear);
		$response 	= $obj_errorHandeling->get_permision_withApproval_cancel($header_arr['STATUS'],$header_arr['APPROVE_LEVELS'],$session_userId,$programCode,'RunQuery');
		
		return $response;
	}
	public function validateBeforeRevise($scheduleNo,$scheduleYear,$programCode)
	{
		global $obj_errorHandeling;
		global $session_userId;
		
		$header_arr	= $this->getHeaderData($scheduleNo,$scheduleYear);
		$response 	= $obj_errorHandeling->get_permision_withApproval_revise($header_arr['STATUS'],$header_arr['APPROVE_LEVELS'],$session_userId,$programCode,'RunQuery');
		
		return $response;
	}
	public function getJECombo($JEYear,$JENo,$orderId,$account)
	{
		$sql = $this->JE_sql($account);
		$html = $this->JE_combo($sql,$JEYear,$JENo,$orderId);
		return $html;
	}
	public function getCreditJECombo($account)
	{
		$sql = $this->creditJECombo_sql();
		$html = $this->creditJECombo($sql,$account);
		return $html;
	}
	public function getJESettlementBalance($settelementYear,$settelementNo,$settelementOrder)
	{
		return $this->getJESettlementBalance_sql($settelementYear,$settelementNo,$settelementOrder);
	}
	private function loadRptHeaderData_sql($scheduleNo,$scheduleYear)
	{
		$sql = "SELECT PSH.SCHEDULE_DATE,
				(SELECT CHART_OF_ACCOUNT_NAME
				FROM finance_mst_chartofaccount
				WHERE CHART_OF_ACCOUNT_ID=PSH.CREDIT_ACCOUNT_ID) AS creditAccount,
				(SELECT CHART_OF_ACCOUNT_NAME
				FROM finance_mst_chartofaccount
				WHERE CHART_OF_ACCOUNT_ID=PSH.DEBIT_ACCOUNT_ID) AS debitAccount,
				STATUS,
				APPROVE_LEVELS,
				APPROVE_LEVELS as LEVELS,
				CURRENCY_ID,
				REMARKS,
				SETTLEMENT_NO,
				SETTLEMENT_YEAR,
				FC.strCode,
				SU.strUserName AS CREATOR,
				CREATED_DATE,
				COMPANY_ID	AS COMPANY_ID
				FROM finance_payment_schedule_header PSH
				INNER JOIN sys_users SU ON PSH.CREATED_BY = SU.intUserId
				INNER JOIN mst_financecurrency FC ON FC.intId = PSH.CURRENCY_ID
				WHERE PSH.SCHEDULE_NO='$scheduleNo' AND
				PSH.SCHEDULE_YEAR='$scheduleYear' ";
		
		$result = $this->db->RunQuery($sql);
		$row 	= mysqli_fetch_array($result);
		
		return $row;
	}
	private function loadDetailData_sql($scheduleNo,$scheduleYear)
	{
		$sql = "SELECT PAY_DATE,
				AMOUNT,
				STATUS,
				PROCESS_DATE
				FROM finance_payment_schedule_details
				WHERE SCHEDULE_NO='$scheduleNo' AND
				SCHEDULE_YEAR='$scheduleYear' ";
		
		$result = $this->db->RunQuery($sql);
		
		return $result;
	}
	private function getRptApproveDetails_sql($scheduleNo,$scheduleYear)
	{
		$sql = "SELECT
				PSAB.APPROVED_BY,
				PSAB.APPROVED_DATE as dtApprovedDate,
				SU.strUserName AS UserName,
				PSAB.APPROVE_LEVEL_NO as intApproveLevelNo
				FROM
				finance_payment_schedule_approveby PSAB
				INNER JOIN sys_users SU ON PSAB.APPROVED_BY = SU.intUserId
				WHERE PSAB.SCHEDULE_NO ='$scheduleNo' AND
				PSAB.SCHEDULE_YEAR ='$scheduleYear'      
				ORDER BY PSAB.APPROVED_DATE ASC ";
		
		$result = $this->db->RunQuery($sql);
		
		return $result;
	}
	private function getHeaderData($scheduleNo,$scheduleYear)
	{
		$sql = "SELECT 
				SCHEDULE_DATE,
				CURRENCY_ID, 
				CREDIT_ACCOUNT_ID, 
				DEBIT_ACCOUNT_ID, 
				STATUS, 
				APPROVE_LEVELS,
				REMARKS,
				SETTLEMENT_NO,
				SETTLEMENT_YEAR,
				SETTLEMENT_ORDER
				FROM 
				finance_payment_schedule_header 
				WHERE SCHEDULE_NO='$scheduleNo' AND
				SCHEDULE_YEAR='$scheduleYear' ";
		
		$result = $this->db->RunQuery($sql);
		$row 	= mysqli_fetch_array($result);
		
		return $row;
	}
	private function JE_sql($account)
	{
		$sql = "SELECT
		finance_journal_entry_details.JOURNAL_ENTRY_NO,
		finance_journal_entry_details.JOURNAL_ENTRY_YEAR,
		finance_journal_entry_details.CHART_OF_ACCOUNT_ID,
		finance_journal_entry_details.REMARKS,
		finance_mst_chartofaccount.CHART_OF_ACCOUNT_NAME,
		finance_journal_entry_details.ORDER_ID,
		finance_journal_entry_details.BAL_AMOUNT as BALANCE_TO_SETTELE
		FROM
		finance_journal_entry_details
		INNER JOIN finance_journal_entry_header ON finance_journal_entry_details.JOURNAL_ENTRY_NO = finance_journal_entry_header.JOURNAL_ENTRY_NO AND 
		finance_journal_entry_details.JOURNAL_ENTRY_YEAR = finance_journal_entry_header.JOURNAL_ENTRY_YEAR
		INNER JOIN finance_mst_chartofaccount ON finance_journal_entry_details.CHART_OF_ACCOUNT_ID = finance_mst_chartofaccount.CHART_OF_ACCOUNT_ID
		WHERE 
		finance_journal_entry_details.CHART_OF_ACCOUNT_ID = '$account' AND 
		finance_journal_entry_details.TRANSACTION_TYPE='D' AND
		finance_journal_entry_header.`STATUS` = 1  AND 
		finance_journal_entry_details.SETTELEMENT_NO IS NULL  
		HAVING BALANCE_TO_SETTELE > 0
		ORDER BY
		finance_mst_chartofaccount.CHART_OF_ACCOUNT_NAME ASC";
 		return $sql;
	}
	private function JE_combo($sql,$JEYear,$JENo,$orderId){
	
 		$result = $this->db->RunQuery($sql);
		$string ="<option value=\"\"></option>";
		while($row = mysqli_fetch_array($result))
		{
			if(($row['JOURNAL_ENTRY_NO']==$JENo) && ($row['JOURNAL_ENTRY_YEAR']==$JEYear) && ($row['ORDER_ID']==$orderId))
				$string.="<option value=\"".$row['JOURNAL_ENTRY_YEAR']."/".$row['JOURNAL_ENTRY_NO']."/".$row['ORDER_ID']."\" selected=\"selected\">".$row['REMARKS']."-".$row['JOURNAL_ENTRY_YEAR']."-".$row['JOURNAL_ENTRY_NO']."-".$row['CHART_OF_ACCOUNT_NAME']."-".$row['BALANCE_TO_SETTELE']."</option>";
			else
				$string.="<option value=\"".$row['JOURNAL_ENTRY_YEAR']."/".$row['JOURNAL_ENTRY_NO']."/".$row['ORDER_ID']."\">".$row['REMARKS']."-".$row['JOURNAL_ENTRY_YEAR']."-".$row['JOURNAL_ENTRY_NO']."-".$row['CHART_OF_ACCOUNT_NAME']."-".$row['BALANCE_TO_SETTELE']."</option>";
		}
		return $string;	
	}
	private function getJESettlementBalance_sql($settelementYear,$settelementNo,$settelementOrder)
	{
		$sql = "SELECT
				JED.BAL_AMOUNT AS BALANCE_TO_SETTELE
				FROM
				finance_journal_entry_details JED
				INNER JOIN finance_journal_entry_header JEH ON JED.JOURNAL_ENTRY_NO = JEH.JOURNAL_ENTRY_NO AND 
				JED.JOURNAL_ENTRY_YEAR = JEH.JOURNAL_ENTRY_YEAR
				INNER JOIN finance_mst_chartofaccount COA ON JED.CHART_OF_ACCOUNT_ID = COA.CHART_OF_ACCOUNT_ID
				WHERE
				JEH.STATUS = 1  AND 
				JED.JOURNAL_ENTRY_NO = '$settelementNo' AND
				JED.JOURNAL_ENTRY_YEAR = '$settelementYear' AND
				JED.ORDER_ID = '$settelementOrder' AND 
				JED.SETTELEMENT_NO IS NULL ";
		
		$result = $this->db->RunQuery($sql);
		$row 	= mysqli_fetch_array($result);
		
		return $row['BALANCE_TO_SETTELE'];
	}
	private function creditJECombo_sql()
	{
		$sql = "SELECT finance_mst_chartofaccount.CHART_OF_ACCOUNT_ID,
				CONCAT(finance_mst_account_type.FINANCE_TYPE_CODE,'',finance_mst_account_main_type.MAIN_TYPE_CODE,'',finance_mst_account_sub_type.SUB_TYPE_CODE,'',finance_mst_chartofaccount.CHART_OF_ACCOUNT_CODE) AS accountCode,
				SUM(finance_journal_entry_details.BAL_AMOUNT) AS BALANCE_TO_SETTELE,
				finance_mst_chartofaccount.CHART_OF_ACCOUNT_NAME
				FROM
				finance_journal_entry_details
				INNER JOIN finance_journal_entry_header ON finance_journal_entry_details.JOURNAL_ENTRY_NO = finance_journal_entry_header.JOURNAL_ENTRY_NO AND 
				finance_journal_entry_details.JOURNAL_ENTRY_YEAR = finance_journal_entry_header.JOURNAL_ENTRY_YEAR
				INNER JOIN finance_mst_chartofaccount ON finance_journal_entry_details.CHART_OF_ACCOUNT_ID = finance_mst_chartofaccount.CHART_OF_ACCOUNT_ID
				INNER JOIN finance_mst_account_sub_type ON finance_mst_account_sub_type.SUB_TYPE_ID=finance_mst_chartofaccount.SUB_TYPE_ID
				INNER JOIN finance_mst_account_main_type ON finance_mst_account_main_type.MAIN_TYPE_ID=finance_mst_account_sub_type.MAIN_TYPE_ID
				INNER JOIN finance_mst_account_type ON finance_mst_account_type.FINANCE_TYPE_ID=finance_mst_account_main_type.FINANCE_TYPE_ID
				WHERE 
				finance_journal_entry_details.TRANSACTION_TYPE='D' AND
				finance_journal_entry_header.STATUS = 1  AND 
				finance_journal_entry_details.SETTELEMENT_NO IS NULL AND
				finance_mst_chartofaccount.PAYMENT_SCHEDULE = 1
				GROUP BY finance_mst_chartofaccount.CHART_OF_ACCOUNT_ID
				HAVING BALANCE_TO_SETTELE > 0
				ORDER BY finance_mst_chartofaccount.CHART_OF_ACCOUNT_NAME ASC ";
		
		return $sql;		
	}
	private function creditJECombo($sql,$account)
	{
		$result = $this->db->RunQuery($sql);
		$string ="<option value=\"\"></option>";
		while($row = mysqli_fetch_array($result))
		{
			if($row['CHART_OF_ACCOUNT_ID']==$account)
				$string.="<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\" selected=\"selected\">".$row['CHART_OF_ACCOUNT_NAME']." - ".$row['accountCode']."</option>";
			else
				$string.="<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\">".$row['CHART_OF_ACCOUNT_NAME']." - ".$row['accountCode']."</option>";
		}
		return $string;	
	}
}
?>
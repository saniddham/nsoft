<?php
include_once  $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
include_once  $_SESSION['ROOT_PATH']."class/cls_commonErrorHandeling_get.php";
include_once  $_SESSION['ROOT_PATH']."class/masterData/exchange_rate/cls_exchange_rate_get.php";

$session_userId			= $_SESSION["userId"];
$session_locationId 	= $_SESSION["CompanyID"];
$session_companyId 		= $_SESSION["headCompanyId"];
$savedMasseged			= "";
$error_sql				= "";

$obj_common				= new cls_commonFunctions_get($db);
$obj_errorHandeling 	= new cls_commonErrorHandeling_get($db);
$obj_exchgRate_get		= new cls_exchange_rate_get($db);

class Cls_Schedule_Set
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	public function save($arrHeader,$arrDetails,$programCode,$programName)
	{
		global $obj_common;
		global $obj_errorHandeling;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_locationId;
		global $session_companyId;
		global $session_userId;
		global $obj_exchgRate_get;
		
		$scheduleNo		= $arrHeader["scheduleNo"];
		$scheduleYear	= $arrHeader["scheduleYear"];
		$date			= $arrHeader["date"];
		$currency		= $arrHeader["currency"];
		$remarks		= $obj_common->replace($arrHeader["remarks"]);
		$creditGL		= $arrHeader["creditGL"];
		$debitGL		= $arrHeader["debitGL"];
		$totalAmonut	= $arrHeader["totalAmonut"];
		
		$settleGLArr	= explode("/",$arrHeader["settleGL"]);
		$settleYear		= $settleGLArr[0];  
		$settleNo		= $settleGLArr[1];  
		$settleOrder	= $settleGLArr[2];
		
		$savedStatus	= true;
		$editMode		= false;
		$this->db->begin();
		
		$exchngRateArr	 = $obj_exchgRate_get->GetAllValues($currency,2,$date,$session_companyId,'RunQuery2');
		if($exchngRateArr['AVERAGE_RATE']=='')
		{
			$savedStatus 	= false;
			$savedMasseged	= 'Please enter exchange rates for '.$date.' before save.';
		}
		
		$this->validateBeforeSave($scheduleNo,$scheduleYear,$programCode,$session_userId);
		if($scheduleNo=='' && $scheduleYear=='')
		{
			$sysNo_arry 	= $obj_common->GetSystemMaxNo('intPaymentScheduleNo',$session_locationId);
			if($sysNo_arry["rollBackFlag"]==1)
			{
				if($savedStatus)
				{
					$savedStatus	= false;
					$savedMasseged 	= $sysNo_arry["msg"];
					$error_sql		= $sysNo_arry["q"];
				}
				
			}
			$scheduleNo				= $sysNo_arry["max_no"];
			$scheduleYear			= date('Y');
			$approveLevels 			= $obj_common->getApproveLevels($programName);
			$status					= $approveLevels+1;
			
			$this->saveHeader($scheduleNo,$scheduleYear,$approveLevels,$status,$date,$currency,$creditGL,$debitGL,$remarks,$totalAmonut,$settleNo,$settleYear,$settleOrder);
		}
		else
		{
			$header_array 			= $this->getHeaderData($scheduleNo,$scheduleYear);
			$status					= $header_array['STATUS'];
			$approveLevels 			= $obj_common->getApproveLevels($programName);
			$status					= $approveLevels+1;
			
			$this->updateHeader($scheduleNo,$scheduleYear,$approveLevels,$status,$date,$currency,$creditGL,$debitGL,$remarks,$totalAmonut,$settleNo,$settleYear,$settleOrder);
			$this->updateMaxStatus($scheduleNo,$scheduleYear);
			$this->deleteDetails($scheduleNo,$scheduleYear);
			$editMode				= true;
		}
		foreach($arrDetails as $array_loop)
		{
			$payDate				= $array_loop["payDate"];
			$amount					= $array_loop["amount"];
			
			$checkDetail			= $this->checkProcessed($scheduleNo,$scheduleYear,$payDate);
			
			if(!$checkDetail)
				$this->saveDetails($scheduleNo,$scheduleYear,$payDate,$amount);
		}
		$resultArr	= $obj_common->validateDuplicateSerialNoWithSysNo($scheduleNo,'intPaymentScheduleNo',$session_locationId);
		if($resultArr['type']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged	= $resultArr['msg'];
		}
		
		if($savedStatus)
		{
			$this->db->commit();
			$response['type'] 			= "pass";
			if($editMode)
				$response['msg'] 		= "Updated Successfully.";
			else
				$response['msg'] 		= "Saved Successfully.";
			
			$response['scheduleNo']		= $scheduleNo;
			$response['scheduleYear']	= $scheduleYear;
		}
		else
		{
			$this->db->rollback();
			$response['type'] 			= "fail";
			$response['msg'] 			= $savedMasseged;
			$response['sql']			= $error_sql;
		}
		return json_encode($response);
	}
	public function approve($scheduleNo,$scheduleYear)
	{	
		global $obj_common;
		global $obj_errorHandeling;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_locationId;
		global $session_companyId;
		global $session_userId;
		global $finalApproval;
		
		$savedStatus	= true;
		$finalApproval	= false;
		
		$header_arr 	= $this->getHeaderDataNew($scheduleNo,$scheduleYear);
		$status			= $header_arr['STATUS'];

		if($status==2)
			$this->validateJEbalAmount($scheduleNo,$scheduleYear);	
		
		$this->db->begin();
		
		$this->updateHeaderStatus($scheduleNo,$scheduleYear,'');
		$this->approvedData($scheduleNo,$scheduleYear);
		
		if($savedStatus)
		{
			$this->db->commit();
			$response['type'] 			= "pass";
			if($finalApproval)
				$response['msg'] 		= "Final Approval Raised Successfully.";
			else
				$response['msg'] 		= "Approved Successfully.";
		}
		else
		{
			$this->db->rollback();
			$response['type'] 			= "fail";
			$response['msg'] 			= $savedMasseged;
			$response['sql']			= $error_sql;
		}
		return json_encode($response);
	}
	public function reject($scheduleNo,$scheduleYear)
	{
		global $obj_common;
		global $obj_errorHandeling;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_locationId;
		global $session_companyId;
		global $session_userId;
		
		$savedStatus	= true;
		$this->db->begin();
		
		$this->updateHeaderStatus($scheduleNo,$scheduleYear,'0');
		//$this->updateMaxStatus($scheduleNo,$scheduleYear);
		$this->approved_by_insert($scheduleNo,$scheduleYear,$session_userId,0);
		
		if($savedStatus)
		{
			$this->db->commit();
			$response['type'] 		= "pass";
			$response['msg'] 		= "Rejected Successfully.";
		}
		else
		{
			$this->db->rollback();
			$response['type'] 			= "fail";
			$response['msg'] 			= $savedMasseged;
			$response['sql']			= $error_sql;
		}
		return json_encode($response);
	}
	public function cancel($scheduleNo,$scheduleYear)
	{
		global $obj_common;
		global $obj_errorHandeling;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_locationId;
		global $session_companyId;
		global $session_userId;
		
		$savedStatus	= true;
		$this->db->begin();
		
		$this->updateHeaderStatus($scheduleNo,$scheduleYear,'-2');
		//$this->updateMaxStatus($scheduleNo,$scheduleYear);
		$this->approved_by_insert($scheduleNo,$scheduleYear,$session_userId,-2);
		//$this->setJEBalance($scheduleNo,$scheduleYear);
		
		if($savedStatus)
		{
			$this->db->commit();
			$response['type'] 		= "pass";
			$response['msg'] 		= "Cancelled Successfully.";
		}
		else
		{
			$this->db->rollback();
			$response['type'] 			= "fail";
			$response['msg'] 			= $savedMasseged;
			$response['sql']			= $error_sql;
		}
		return json_encode($response);
	}
	public function revise($scheduleNo,$scheduleYear)
	{
		global $obj_common;
		global $obj_errorHandeling;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_locationId;
		global $session_companyId;
		global $session_userId;
		
		$savedStatus	= true;
		$this->db->begin();
		
		$this->updateHeaderStatus($scheduleNo,$scheduleYear,'-1');
		//$this->updateMaxStatus($scheduleNo,$scheduleYear);
		$this->approved_by_insert($scheduleNo,$scheduleYear,$session_userId,-1);
		//$this->setJEBalance($scheduleNo,$scheduleYear);
		
		if($savedStatus)
		{
			$this->db->commit();
			$response['type'] 		= "pass";
			$response['msg'] 		= "Revised Successfully.";
		}
		else
		{
			$this->db->rollback();
			$response['type'] 			= "fail";
			$response['msg'] 			= $savedMasseged;
			$response['sql']			= $error_sql;
		}
		return json_encode($response);
	}
	private function validateBeforeSave($scheduleNo,$scheduleYear,$programCode,$session_userId)
	{
		global $obj_errorHandeling;
		global $savedStatus;
		global $savedMasseged;
		
		$header_arr		= $this->getHeaderData($scheduleNo,$scheduleYear);
		$response 		= $obj_errorHandeling->get_permision_withApproval_save($header_arr['STATUS'],$header_arr['APPROVE_LEVELS'],$session_userId,$programCode,'RunQuery2');
		
		
		if($response["type"]=='fail' && ($savedStatus))
		{
			$savedStatus	= false;
			$savedMasseged 	= $response["msg"];
		}
	}
	private function getHeaderData($scheduleNo,$scheduleYear)
	{
		$sql = "SELECT STATUS,
				APPROVE_LEVELS
				FROM finance_payment_schedule_header
				WHERE SCHEDULE_NO='$scheduleNo' AND
				SCHEDULE_YEAR='$scheduleYear' ";
		
		$result = $this->db->RunQuery2($sql);
		$row 	= mysqli_fetch_array($result);
		
		return $row;
	}
	private function getHeaderDataNew($scheduleNo,$scheduleYear)
	{
		$sql = "SELECT STATUS,
				APPROVE_LEVELS
				FROM finance_payment_schedule_header
				WHERE SCHEDULE_NO='$scheduleNo' AND
				SCHEDULE_YEAR='$scheduleYear' ";
		
		$result = $this->db->RunQuery($sql);
		$row 	= mysqli_fetch_array($result);
		
		return $row;
	}
	private function saveHeader($scheduleNo,$scheduleYear,$approveLevels,$status,$date,$currency,$creditGL,$debitGL,$remarks,$totalAmonut,$settleNo,$settleYear,$settleOrder)
	{
		global $session_userId;
		global $session_companyId;
		global $session_locationId;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "INSERT INTO finance_payment_schedule_header 
				(
				SCHEDULE_NO, 
				SCHEDULE_YEAR, 
				SCHEDULE_DATE,
				CURRENCY_ID,
				REMARKS, 
				AMOUNT, 
				CREDIT_ACCOUNT_ID,
				DEBIT_ACCOUNT_ID, 
				STATUS, 
				APPROVE_LEVELS,
				SETTLEMENT_NO, 
				SETTLEMENT_YEAR, 
				SETTLEMENT_ORDER,  
				COMPANY_ID, 
				LOCATION_ID, 
				CREATED_BY, 
				CREATED_DATE
				)
				VALUES
				(
				'$scheduleNo', 
				'$scheduleYear', 
				'$date',
				'$currency',
				'$remarks',
				'$totalAmonut',
				'$creditGL',
				'$debitGL', 
				'$status', 
				'$approveLevels',
				'$settleNo',
				'$settleYear',
				'$settleOrder', 
				'$session_companyId', 
				'$session_locationId', 
				'$session_userId', 
				NOW()
				)";
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function updateHeader($scheduleNo,$scheduleYear,$approveLevels,$status,$date,$currency,$creditGL,$debitGL,$remarks,$totalAmonut,$settleNo,$settleYear,$settleOrder)
	{
		global $session_userId;
		global $session_companyId;
		global $session_locationId;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "UPDATE finance_payment_schedule_header 
				SET 
				SCHEDULE_DATE = '$date' ,
				CURRENCY_ID = '$currency',
				CREDIT_ACCOUNT_ID = '$creditGL',
				REMARKS = '$remarks' , 
				AMOUNT = '$totalAmonut' , 
				DEBIT_ACCOUNT_ID = '$debitGL', 
				STATUS = '$status' , 
				APPROVE_LEVELS = '$approveLevels' ,
				SETTLEMENT_NO = '$settleNo' , 
				SETTLEMENT_YEAR = '$settleYear' , 
				SETTLEMENT_ORDER = '$settleOrder' ,  
				COMPANY_ID = '$session_companyId' , 
				LOCATION_ID = '$session_locationId' , 
				LAST_MODIFY_BY = '$session_userId'
				WHERE
				SCHEDULE_NO = '$scheduleNo' AND 
				SCHEDULE_YEAR = '$scheduleYear' ";
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function deleteDetails($scheduleNo,$scheduleYear)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "DELETE FROM finance_payment_schedule_details 
				WHERE
				SCHEDULE_NO = '$scheduleNo' AND 
				SCHEDULE_YEAR = '$scheduleYear' AND
				STATUS = '0' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function saveDetails($scheduleNo,$scheduleYear,$payDate,$amount)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "INSERT INTO finance_payment_schedule_details 
				(
				SCHEDULE_NO, 
				SCHEDULE_YEAR, 
				PAY_DATE, 
				AMOUNT
				)
				VALUES
				(
				'$scheduleNo', 
				'$scheduleYear', 
				'$payDate', 
				'$amount'
				)";
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function updateHeaderStatus($scheduleNo,$scheduleYear,$status)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		if($status=='') 
			$para='STATUS-1';
 		else
			$para=$status;
		
		$sql = "UPDATE finance_payment_schedule_header 
				SET
				STATUS = ".$para."
				WHERE
				SCHEDULE_NO = '$scheduleNo' AND 
				SCHEDULE_YEAR = '$scheduleYear' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function approvedData($scheduleNo,$scheduleYear)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_userId;
		global $finalApproval;
		
		$header_arr 	= $this->getHeaderData($scheduleNo,$scheduleYear);
		$status			= $header_arr['STATUS'];
		$savedLevels	= $header_arr['APPROVE_LEVELS'];
		
		//if($savedLevels>$status)
			//$this->updateMaxStatus($scheduleNo,$scheduleYear);
		if($status==1)
			$finalApproval = true;	
			
		$approval		= $savedLevels+1-$status;
		$this->approved_by_insert($scheduleNo,$scheduleYear,$session_userId,$approval);
	}
	private function approved_by_insert($scheduleNo,$scheduleYear,$session_userId,$approval)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "INSERT INTO finance_payment_schedule_approveby 
				(
				SCHEDULE_NO, 
				SCHEDULE_YEAR, 
				APPROVE_LEVEL_NO, 
				APPROVED_BY, 
				APPROVED_DATE, 
				STATUS
				)
				VALUES
				(
				'$scheduleNo', 
				'$scheduleYear', 
				'$approval', 
				'$session_userId', 
				NOW(), 
				0
				) ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function updateMaxStatus($scheduleNo,$scheduleYear)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sqlMax = " SELECT (MAX(tb.STATUS)+1) AS maxStatus  
					FROM finance_payment_schedule_approveby AS tb
					WHERE tb.SCHEDULE_NO='$scheduleNo' AND
					tb.SCHEDULE_YEAR='$scheduleYear' ";
		
		$resultMax 	= $this->db->RunQuery2($sqlMax);
		$rowMax		= mysqli_fetch_array($resultMax);
		$maxStatus 	= $rowMax['maxStatus'];
		
		$sqlUpd = " UPDATE finance_payment_schedule_approveby 
					SET
					STATUS = '$maxStatus'
					WHERE
					SCHEDULE_NO = '$scheduleNo' AND 
					SCHEDULE_YEAR = '$scheduleYear' ";
		
		$resultUpd = $this->db->RunQuery2($sqlUpd);
		if(!$resultUpd)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sqlUpd;
			}
		}
	}
	private function checkProcessed($scheduleNo,$scheduleYear,$payDate)
	{
		$checkStatus = false;
		
		$sql = "SELECT STATUS
				FROM finance_payment_schedule_details
				WHERE SCHEDULE_NO='$scheduleNo' AND
				SCHEDULE_YEAR='$scheduleYear' AND
				PAY_DATE='$payDate' ";
		
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		
		if($row['STATUS']==1)
			$checkStatus = true;
		
		return $checkStatus;
	}
	private function updateJEBalanceAmount($scheduleNo,$scheduleYear)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "SELECT JED.BAL_AMOUNT AS settleBlance,
				PSH.AMOUNT AS scheduleAmount,
				PSH.SETTLEMENT_NO,
				PSH.SETTLEMENT_YEAR,
				PSH.SETTLEMENT_ORDER
				FROM finance_payment_schedule_header PSH
				INNER JOIN finance_journal_entry_details JED ON JED.JOURNAL_ENTRY_NO=PSH.SETTLEMENT_NO AND
				JED.JOURNAL_ENTRY_YEAR=PSH.SETTLEMENT_YEAR AND
				JED.ORDER_ID=PSH.SETTLEMENT_ORDER
				INNER JOIN finance_journal_entry_header JEH ON JED.JOURNAL_ENTRY_NO = JEH.JOURNAL_ENTRY_NO AND 
				JED.JOURNAL_ENTRY_YEAR = JEH.JOURNAL_ENTRY_YEAR
				WHERE PSH.SCHEDULE_NO='$scheduleNo' AND
				PSH.SCHEDULE_YEAR='$scheduleYear' ";
		
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		
		if($row['scheduleAmount']>$row['settleBlance'])
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= "Total Schedule amount must less than settle balance amount.";
			}
		}
		else
		{
			$sqlUpd = "UPDATE finance_journal_entry_details 
						SET
						BAL_AMOUNT = BAL_AMOUNT-(SELECT SUM(AMOUNT) AS balAmt FROM finance_payment_schedule_details 
										WHERE SCHEDULE_NO='$scheduleNo' AND SCHEDULE_YEAR='$scheduleYear')	
						WHERE
						JOURNAL_ENTRY_NO = '".$row['SETTLEMENT_NO']."' AND 
						JOURNAL_ENTRY_YEAR = '".$row['SETTLEMENT_YEAR']."' AND 
						ORDER_ID = '".$row['SETTLEMENT_ORDER']."' ";
			
			$resultUpd = $this->db->RunQuery2($sqlUpd);
			if(!$resultUpd)
			{
				if($savedStatus)
				{
					$savedStatus	= false;
					$savedMasseged	= $this->db->errormsg;
					$error_sql		= $sqlUpd;
				}
			}
		}
	}
	private function setJEBalance($scheduleNo,$scheduleYear)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "SELECT (SELECT SUM(AMOUNT) 
				FROM finance_payment_schedule_details 
				WHERE SCHEDULE_NO='$scheduleNo' AND 
				SCHEDULE_YEAR='$scheduleYear') AS scheduleAmount,
				PSH.SETTLEMENT_NO,
				PSH.SETTLEMENT_YEAR,
				PSH.SETTLEMENT_ORDER
				FROM finance_payment_schedule_header PSH
				WHERE PSH.SCHEDULE_NO='$scheduleNo' AND
				PSH.SCHEDULE_YEAR='$scheduleYear' ";
		
		$result 		= $this->db->RunQuery2($sql);
		$row			= mysqli_fetch_array($result);
		
		$scheduleAmt 	= $row['scheduleAmount'];
		$settleNo 		= $row['SETTLEMENT_NO'];
		$settleYear 	= $row['SETTLEMENT_YEAR'];
		$settleOrder 	= $row['SETTLEMENT_ORDER'];
		
		$sqlUpd = "UPDATE finance_journal_entry_details 
					SET
					BAL_AMOUNT = BAL_AMOUNT + $scheduleAmt
					WHERE
					JOURNAL_ENTRY_NO = '$settleNo' AND 
					JOURNAL_ENTRY_YEAR = '$settleYear' AND 
					ORDER_ID = '$settleOrder' ";
		
		$resultUpd = $this->db->RunQuery2($sqlUpd);
		if(!$resultUpd)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sqlUpd;
			}
		}
	}
	private function validateJEbalAmount($scheduleNo,$scheduleYear)
	{
		global $savedStatus;
		global $savedMasseged;
		
		$resultArr	= $this->validateJEbalAmount_sql($scheduleNo,$scheduleYear);
		
		if($resultArr['totAmount']>($resultArr['BAL_AMOUNT']-$resultArr['sheduledAmt']) && ($savedStatus))
		{
			$savedStatus	= false;
			$savedMasseged	= "No balance amount to proceed.";
		}
	}
	private function validateJEbalAmount_sql($scheduleNo,$scheduleYear)
	{
		$sql = "SELECT
				JD.BAL_AMOUNT,
				SUM(SD.AMOUNT) AS totAmount,
				IFNULL((SELECT SUM(FSD.AMOUNT)
				FROM finance_payment_schedule_header FSH
				INNER JOIN finance_payment_schedule_details FSD
				ON FSD.SCHEDULE_NO = FSH.SCHEDULE_NO AND FSD.SCHEDULE_YEAR = FSH.SCHEDULE_YEAR
				WHERE FSH.SETTLEMENT_NO=JD.JOURNAL_ENTRY_NO AND
				FSH.SETTLEMENT_YEAR=JD.JOURNAL_ENTRY_YEAR AND
				FSH.SETTLEMENT_ORDER=JD.ORDER_ID AND
				FSH.STATUS = 1 AND
				FSD.STATUS = 0 ),0) AS sheduledAmt 
				FROM finance_payment_schedule_header SH
				INNER JOIN finance_payment_schedule_details SD
				ON SD.SCHEDULE_NO = SH.SCHEDULE_NO AND SD.SCHEDULE_YEAR = SH.SCHEDULE_YEAR
				INNER JOIN finance_journal_entry_details JD ON JD.JOURNAL_ENTRY_NO=SH.SETTLEMENT_NO AND 
				JD.JOURNAL_ENTRY_YEAR=SH.SETTLEMENT_YEAR AND JD.ORDER_ID=SH.SETTLEMENT_ORDER
				WHERE SH.SCHEDULE_NO = '$scheduleNo' AND
				SH.SCHEDULE_YEAR = '$scheduleYear'
				GROUP BY SH.SCHEDULE_YEAR,SH.SCHEDULE_NO ";
		
		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row;
	}
}
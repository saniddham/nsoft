<?php
$session_companyId 	= $_SESSION["headCompanyId"];

class Cls_Invoice_Get
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	public function loadSupplier($supplierId)
	{
		return $this->loadSupplier_sql($supplierId);
	}
	public function loadCurrency($supplierId)
	{
		return $this->getCurrency_sql($supplierId);
	}
	public function getHeaderData($billInvoiceNo,$billInvoiceYear)
	{
		return $this->headerData_sql($billInvoiceNo,$billInvoiceYear);
	}
	public function getDetailData($billInvoiceNo,$billInvoiceYear)
	{
		return $this->detailData_sql($billInvoiceNo,$billInvoiceYear);
	}
	public function getRptHeaderData($billInvoiceNo,$billInvoiceYear)
	{
		return $this->rptHeaderData_sql($billInvoiceNo,$billInvoiceYear);
	}
	public function getRptDetaiData($billInvoiceNo,$billInvoiceYear)
	{
		return $this->rptDetaiData_sql($billInvoiceNo,$billInvoiceYear);
	}
	private function loadSupplier_sql($supplierId)
	{
		$sql = "SELECT intId,strName
				FROM mst_supplier
				WHERE intTypeId='14' AND
				intStatus='1'
				ORDER BY strName ";
		
		$result = $this->db->RunQuery($sql);
		$string ="<option value=\"\"></option>";
		while($row = mysqli_fetch_array($result))
		{
			if($row['intId']==$supplierId)
				$string.="<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";
			else
				$string.="<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		return $string;
	}
	private function getCurrency_sql($supplierId)
	{
		$sql = "SELECT intCurrencyId
				FROM mst_supplier
				WHERE intId='$supplierId' ";
		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
	
		return $row['intCurrencyId'];
	}
	private function headerData_sql($billInvoiceNo,$billInvoiceYear)
	{
		$sql = "SELECT BILL_DATE,
				SUPPLIER_ID,
				CURRENCY_ID,
				REFERENCE_NO,
				REMARKS,
				STATUS
				FROM finance_other_payable_bill_header
				WHERE BILL_INVOICE_NO='$billInvoiceNo' AND
				BILL_INVOICE_YEAR='$billInvoiceYear' ";
		
		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row;
	}
	private function detailData_sql($billInvoiceNo,$billInvoiceYear)
	{
		$sql = "SELECT CHART_OF_ACCOUNT_ID,
				ITEM_DESCRIPTION,
				UNIT_OF_MEASURE,
				QTY,
				UNIT_PRICE,
				DISCOUNT,
				TAX_AMOUNT,
				TAX_CODE,
				COST_CENTER
				FROM finance_other_payable_bill_details
				WHERE BILL_INVOICE_NO='$billInvoiceNo' AND
				BILL_INVOICE_YEAR='$billInvoiceYear' ";
				
		$result = $this->db->RunQuery($sql);
		
		return $result;
	}
	private function rptHeaderData_sql($billInvoiceNo,$billInvoiceYear)
	{
		global $session_companyId;
		
		$sql = "SELECT 
				OPH.BILL_INVOICE_NO						AS SERIAL_NO,
				OPH.COMPANY_ID							AS COMPANY_ID,
				OPH.BILL_DATE							AS SERIAL_DATE,
				MS.strName AS supplier,
				OPH.BILL_DATE,
				OPH.REMARKS, 
				OPH.REFERENCE_NO,
				FC.strDescription AS currency,
				OPH.STATUS
				FROM finance_other_payable_bill_header OPH
				INNER JOIN mst_supplier MS ON MS.intId=OPH.SUPPLIER_ID
				LEFT JOIN mst_financecurrency FC ON FC.intId=OPH.CURRENCY_ID
				WHERE OPH.COMPANY_ID='$session_companyId' AND
				OPH.BILL_INVOICE_NO='$billInvoiceNo' AND
				OPH.BILL_INVOICE_YEAR='$billInvoiceYear' ";
		
		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row;
	}
	private function rptDetaiData_sql($billInvoiceNo,$billInvoiceYear)
	{
		$sql = "SELECT COA.CHART_OF_ACCOUNT_NAME,
				OPD.ITEM_DESCRIPTION,
				MU.strCode AS unit,
				ROUND(OPD.UNIT_PRICE,2) AS UNIT_PRICE,
				ROUND(OPD.QTY,2) AS QTY,
				OPD.DISCOUNT,
				OPD.TAX_AMOUNT
				FROM finance_other_payable_bill_details OPD
				INNER JOIN mst_units MU ON MU.intId = OPD.UNIT_OF_MEASURE
				INNER JOIN finance_mst_chartofaccount COA ON COA.CHART_OF_ACCOUNT_ID=OPD.CHART_OF_ACCOUNT_ID
				WHERE OPD.BILL_INVOICE_NO='$billInvoiceNo' AND
				OPD.BILL_INVOICE_YEAR='$billInvoiceYear' ";
		
		$result = $this->db->RunQuery($sql);
		
		return $result;
	}
}
?>
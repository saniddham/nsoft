<?php
include  $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
include  $_SESSION['ROOT_PATH']."class/finance/cash_flow/cls_cf_common.php";
include  $_SESSION['ROOT_PATH']."class/finance/cash_flow/cls_cf_common_set.php";

$session_userId		= $_SESSION["userId"];
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];
$savedMasseged		= "";
$error_sql			= "";

$obj_common			= new cls_commonFunctions_get($db);
$obj_cf_common		= new cls_cf_common($db);
$obj_cf_common_set	= new cls_cf_common_set($db);

class Cls_Invoice_Set
{
	private $db;
	private $obj_exchgRate_get;
	
	function __construct($db,$obj_exchgRate_get)
	{
		$this->db 				 = $db;
		$this->obj_exchgRate_get = $obj_exchgRate_get;
	}
	
	public function save($arrHeader,$arrDetails,$programCode)
	{
		global $obj_common;
		global $savedStatus;
		global $savedMasseged;
		global $session_locationId;
		global $session_companyId;
		global $session_userId;
		global $error_sql;
		global $obj_cf_common;
		global $obj_cf_common_set;
		
		$billInvNo		= $arrHeader["billInvNo"];
		$billInvYear	= $arrHeader["billInvYear"];
		$dtDate			= $arrHeader["dtDate"];
		$supplierId		= $arrHeader["supplierId"];
		$currency		= $arrHeader["currency"];
		$remarks		= $obj_common->replace($arrHeader["remarks"]);
		$referenceNo	= $obj_common->replace($arrHeader["referenceNo"]);
		$grandTotal		= $arrHeader["grandTotal"];

		$savedStatus	= true;
		$this->db->begin();
		
		$svatValue		= $this->getSvatValue($grandTotal);
		
		$this->checkSaveMode($programCode,$session_userId);
		$this->checkUpdateMode($billInvNo,$billInvYear);
		
		$exchngRateArr	 = $this->obj_exchgRate_get->GetAllValues($currency,2,$dtDate,$session_companyId,'RunQuery2');
		if($exchngRateArr['AVERAGE_RATE']=='')
		{
			$savedStatus 	= false;
			$savedMasseged	= 'Please enter exchange rates for '.$dtDate.' before save.';
		}
		
		$invoice_array 	= $obj_common->GetSystemMaxNo('intOtherBillInvNo',$session_locationId);
		if($invoice_array["rollBackFlag"]==1)
		{
			$savedStatus 	= false;
			if($savedMasseged=='')
			{
				$savedMasseged 	= $invoice_array["msg"];
				$error_sql		= $invoice_array["q"];
			}
			
		}
		
		$billInvNo		= $invoice_array["max_no"];
		$billInvYear	= date('Y');
		
		$this->saveHeader($billInvNo,$billInvYear,$dtDate,$supplierId,$currency,$remarks,$referenceNo,$grandTotal);
		
		foreach($arrDetails as $array_loop)
		{
			$chartOfAccId	= $array_loop["chartOfAccId"];
			$itemDisc		= $obj_common->replace($array_loop["itemDisc"]);
			$uom			= $array_loop["uom"];
			$qty			= $array_loop["qty"];
			$unitPrice		= $array_loop["unitPrice"];
			$discount		= ($array_loop["discount"]==''?0:$array_loop["discount"]);
			$taxId			= $array_loop["taxId"];
			$costCenter		= $array_loop["costCenter"];
			$taxAmount		= $array_loop["taxAmount"];
			$amount			= $qty*$unitPrice;
			$disamonut		= round((($amount*(100-$discount))/100),2);
			
			$this->saveDetails($billInvNo,$billInvYear,$chartOfAccId,$itemDisc,$uom,$qty,$unitPrice,$discount,$taxId,$costCenter,$taxAmount);
			$this->transactionSaveSql($supplierId,$currency,'BILLINVOICE',$billInvNo,$billInvYear,$chartOfAccId,$disamonut,$dtDate);
			$this->saveTaxTransaction($supplierId,$currency,$billInvNo,$billInvYear,$taxId,$taxAmount,$dtDate);	
			$this->saveFinanceTransaction($supplierId,$chartOfAccId,$disamonut,$billInvNo,$billInvYear,'BILLINVOICE','D','OP',$currency,$dtDate);
		}
		
		$this->saveSupplierTransaction($supplierId,$currency,$billInvNo,$billInvYear,$grandTotal,$dtDate);	
		$this->saveSvatTransaction($supplierId,$currency,$billInvNo,$billInvYear,$taxId,$svatValue,$dtDate);
		
		//-------------------------- cash flow update 27/05/2014 by lahiru --------------------------------
			
		$CFUpdResult	= $this->getDetailData($billInvNo,$billInvYear);
		while($row = mysqli_fetch_array($CFUpdResult))
		{
			$amount	   		= $row['QTY']*$row['UNIT_PRICE'];	
			$taxAmount		= round($row['TAX_AMOUNT'],2);
            $disamonut 		= round((($amount*(100-$row['DISCOUNT']))/100),2)+$taxAmount;
			$currencyId		= $row['CURRENCY_ID'];
			$currency		= $row['CURRENCY_ID'];
			$entryDate		= $row['BILL_DATE'];
			$company		= $row['COMPANY_ID'];
			$rpt_field		= $row['REPORT_FIELD_ID'];
			
			$tot_to_save	= $disamonut;	
			
			$cfAmountResult	= $obj_cf_common->get_cf_log_bal_to_invoice_result($session_locationId,$rpt_field,$currencyId,$entryDate,$company,'RunQuery2');
			while($rowAmt = mysqli_fetch_array($cfAmountResult))
			{
				$balAmount	= round($rowAmt['BAL'],2);
				if(($tot_to_save>0) && ($balAmount>0))
				{
					if($tot_to_save<=$balAmount)
					{
						$saveAmnt 	 	= $tot_to_save;
						$tot_to_save 	= 0;
					}
					else if($tot_to_save>$balAmount)
					{
						$saveAmnt 		= $balAmount;
						$tot_to_save	= $tot_to_save-$saveAmnt;
					}
					
					$rcv_date	= $rowAmt['RECEIVE_DATE'];
					//$currency	= $rowAmt['CURRENCY_ID'];
					
					$resultArr	= $obj_cf_common_set->insert_to_log($session_locationId,$rpt_field,$rcv_date,$entryDate,$entryDate,$billInvNo,$billInvYear,'BILLINVOICE',$saveAmnt,$currency,'RunQuery2');
					if($resultArr['type']=='fail' && $savedStatus)
					{
						$savedStatus	= false;
						$savedMasseged	= $resultArr['msg'];
						$error_sql		= $resultArr['sql'];
					}	
				}
			}
			if($tot_to_save>0)
			{
				if($rcv_date == '')
					$rcv_date = $entryDate;	
				
				$resultArr	= $obj_cf_common_set->insert_to_log($session_locationId,$rpt_field,$rcv_date,$entryDate,$entryDate,$billInvNo,$billInvYear,'BILLINVOICE',$tot_to_save,$currencyId,'RunQuery2');
				if($resultArr['type']=='fail' && $savedStatus)
				{
					$savedStatus	= false;
					$savedMasseged	= $resultArr['msg'];
					$error_sql		= $resultArr['sql'];
				}	
			}
		}
		//-------------------------------------------------------------------------------------------------	
		$sysNoArr					= $obj_common->validateDuplicateSerialNoWithSysNo($billInvNo,'intOtherBillInvNo',$session_locationId);
		if($sysNoArr['type'] == 'fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged	= $sysNoArr['msg'];
		}
		if($savedStatus)
		{
			$this->db->commit();
			$response['type'] 			= "pass";
			$response['msg'] 			= "Saved Successfully.";
			
			$response['InvoiceNo']		= $billInvNo;
			$response['InvoiceYear']	= $billInvYear;
		}
		else
		{
			$this->db->rollback();
			$response['type'] 			= "fail";
			$response['msg'] 			= $savedMasseged;
			$response['sql']			= $error_sql;
		}
		return json_encode($response);	
		
	}
	public function cancle($billInvNo,$billInvYear,$programCode)
	{
		global $savedStatus;
		global $savedMasseged;
		global $session_locationId;
		global $session_userId;
		global $error_sql;
		global $obj_cf_common_set;
		
		$savedStatus	= true;
		$this->db->begin();
		
		$this->checkCancleMode($programCode,$session_userId);
		$this->cancleValidation($billInvNo,$billInvYear);
		
		$this->cancleHeader($billInvNo,$billInvYear);
		$this->cancleOtherPayableTransaction($billInvNo,$billInvYear,'BILLINVOICE');
		$this->cancleFinanceTransaction($billInvNo,$billInvYear,'BILLINVOICE');
		
		$resultDCFArr	= $obj_cf_common_set->delete_from_log($billInvNo,$billInvYear,'BILLINVOICE','RunQuery2');
		if($resultDCFArr['type'] == 'fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged	= $resultDCFArr['msg']; 
			$error_sql		= $resultDCFArr['sql']; 
		}
		
		if($savedStatus)
		{
			$this->db->commit();
			$response['type'] 			= "pass";
			$response['msg'] 			= "Cancelled Successfully.";
		}
		else
		{
			$this->db->rollback();
			$response['type'] 			= "fail";
			$response['msg'] 			= $savedMasseged;
			$response['sql']			= $error_sql;
		}
		return json_encode($response);
		
	}
	private function checkCancleMode($programCode,$userId)
	{
		global $savedStatus;
		global $savedMasseged;
		
		$sql = "SELECT
				menupermision.intCancel 
				FROM menupermision 
				INNER JOIN menus ON menupermision.intMenuId = menus.intId
				WHERE
				menus.strCode = '$programCode' AND
				menupermision.intUserId = '$userId' ";
		
		$result = $this->db->RunQuery2($sql);
		$row 	= mysqli_fetch_array($result);
		
		if($row['intCancel']!=1)
		{
			$savedStatus	= false;
			if($savedMasseged=='')
				$savedMasseged	= "You do not have permission to Cancle Other Payable Invoice.";
		}
	}
	private function cancleValidation($billInvNo,$billInvYear)
	{
		global $savedStatus;
		global $savedMasseged;
		
		$sql = "SELECT STATUS
				FROM finance_other_payable_bill_header 
				WHERE BILL_INVOICE_NO='$billInvNo' AND
				BILL_INVOICE_YEAR='$billInvYear' ";
		
		$result = $this->db->RunQuery2($sql);
		$row 	= mysqli_fetch_array($result);
		
		if($row['STATUS']==-2)
		{
			$savedStatus	= false;
			if($savedMasseged=='')
				$savedMasseged	= "Already Cancelled.";
		}
		
		$sql = "SELECT IFNULL((SUM(ABS(OPT.VALUE))),0) totQty
				FROM finance_other_payable_transaction OPT
				WHERE OPT.BILL_INVOICE_NO='$billInvNo' AND
				OPT.BILL_INVOICE_YEAR='$billInvYear' AND
				OPT.DOCUMENT_TYPE<>'BILLINVOICE' ";
		
		$result = $this->db->RunQuery2($sql);
		$row 	= mysqli_fetch_array($result);
		
		if($row['totQty']>0)
		{
			$savedStatus	= false;
			if($savedMasseged=='')
				$savedMasseged	= "Cannot cancel this Invoice.Some payment raised.";
		}
	}
	private function cancleHeader($billInvNo,$billInvYear)
	{
		global $savedStatus;
		global $savedMasseged;
		global $session_userId;
		global $error_sql;
		
		$sql = "UPDATE finance_other_payable_bill_header 
				SET
				STATUS = '-2' , 
				LAST_MODIFY_BY = '$session_userId' , 
				LAST_MODIFY_DATE = NOW()
				WHERE
				BILL_INVOICE_NO = '$billInvNo' AND 
				BILL_INVOICE_YEAR = '$billInvYear' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$savedStatus	= false;
			if($savedMasseged=='')
			{
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function cancleOtherPayableTransaction($billInvNo,$billInvYear,$docType)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "DELETE FROM finance_other_payable_transaction 
				WHERE DOCUMENT_NO='$billInvNo' AND
				DOCUMENT_YEAR='$billInvYear' AND
				DOCUMENT_TYPE='$docType' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$savedStatus	= false;
			if($savedMasseged=='')
			{
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function cancleFinanceTransaction($billInvNo,$billInvYear,$docType)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "DELETE FROM finance_transaction 
				WHERE DOCUMENT_NO='$billInvNo' AND
				DOCUMENT_YEAR='$billInvYear' AND
				TRANSACTION_CATEGORY = 'OP' AND
				DOCUMENT_TYPE='$docType' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$savedStatus	= false;
			if($savedMasseged=='')
			{
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function checkSaveMode($programCode,$userId)
	{
		global $savedStatus;
		global $savedMasseged;
		
		$sql = "SELECT
				menupermision.intAdd 
				FROM menupermision 
				INNER JOIN menus ON menupermision.intMenuId = menus.intId
				WHERE
				menus.strCode = '$programCode' AND
				menupermision.intUserId = '$userId' ";
		
		$result = $this->db->RunQuery2($sql);
		$row 	= mysqli_fetch_array($result);
		
		if($row['intAdd']!=1)
		{
			$savedStatus	= false;
			if($savedMasseged=='')
				$savedMasseged	= "You do not have permission to Save Other Payable Invoice.";
		}
	}
	private function getSvatValue($grandTotal)
	{
		$sql 		= "SELECT dblRate FROM mst_financetaxisolated WHERE intId='3' ";
		$result 	= $this->db->RunQuery2($sql);
		$row		= mysqli_fetch_array($result);
		
		$svatValue 	= $grandTotal*($row['dblRate']/100);
		return round($svatValue,2);
	}
	private function checkUpdateMode($billInvNo,$billInvYear)
	{
		global $savedStatus;
		global $savedMasseged;
		
		if($billInvNo!='' && $billInvYear!='')
		{
			$savedStatus	= false;
			if($savedMasseged=='')
				$savedMasseged	= "You can not update Other Payable Invoice.";
		}
	}
	private function saveHeader($billInvNo,$billInvYear,$dtDate,$supplierId,$currency,$remarks,$referenceNo,$grandTotal)
	{
		global $session_userId;
		global $session_locationId;
		global $session_companyId;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "INSERT INTO finance_other_payable_bill_header 
				(
				BILL_INVOICE_NO, 
				BILL_INVOICE_YEAR, 
				BILL_DATE, 
				SUPPLIER_ID, 
				COMPANY_ID, 
				REFERENCE_NO,
				REMARKS, 
				CURRENCY_ID, 
				STATUS,
				CREATED_BY, 
				CREATED_DATE
				)
				VALUES
				(
				'$billInvNo', 
				'$billInvYear', 
				'$dtDate', 
				'$supplierId', 
				'$session_companyId', 
				'$referenceNo',
				'$remarks', 
				'$currency', 
				'1',
				'$session_userId', 
				NOW()
				) ";
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$savedStatus	= false;
			if($savedMasseged=='')
			{
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function saveDetails($billInvNo,$billInvYear,$chartOfAccId,$itemDisc,$uom,$qty,$unitPrice,$discount,$taxId,$costCenter,$taxAmount)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "INSERT INTO finance_other_payable_bill_details 
				(
				BILL_INVOICE_NO, 
				BILL_INVOICE_YEAR, 
				CHART_OF_ACCOUNT_ID, 
				ITEM_DESCRIPTION, 
				UNIT_OF_MEASURE, 
				QTY, 
				UNIT_PRICE, 
				DISCOUNT, 
				TAX_AMOUNT, 
				TAX_CODE, 
				COST_CENTER
				)
				VALUES
				(
				'$billInvNo', 
				'$billInvYear', 
				'$chartOfAccId', 
				'$itemDisc', 
				'$uom', 
				'$qty', 
				'$unitPrice', 
				'$discount', 
				'$taxAmount', 
				 $taxId, 
				'$costCenter'
				);";
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$savedStatus = false;
			if($savedMasseged=='')
			{
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;	
			}
		}
	}
	private function saveTaxTransaction($supplierId,$currency,$billInvNo,$billInvYear,$taxId,$taxAmount,$dtDate)
	{
		global $savedStatus;
		global $savedMasseged;
		
		if($taxId!='NULL')
		{
			$taxChrtOfAccount = $this->getTaxChartOfAccount($taxId);
			if($taxChrtOfAccount['type']=='fail')
			{
				$savedStatus = false;
				if($savedMasseged=='')
					$savedMasseged	= $taxChrtOfAccount['ErrorMsg'];
			}
			else
			{
				$this->transactionSaveSql($supplierId,$currency,'BILLINVOICE',$billInvNo,$billInvYear,$taxChrtOfAccount,$taxAmount,$dtDate);
				$this->saveFinanceTransaction($supplierId,$taxChrtOfAccount,$taxAmount,$billInvNo,$billInvYear,'BILLINVOICE','D','OP',$currency,$dtDate);
			}
		}
	}
	private function getTaxChartOfAccount($taxId)
	{
		$sql = "SELECT CHART_OF_ACCOUNT_ID
				FROM finance_mst_chartofaccount_tax
				WHERE TAX_ID='$taxId' ";
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		
		if(mysqli_num_rows($result)>0)
		{
			return $row["CHART_OF_ACCOUNT_ID"];
		}
		else
		{
			$taxCode				= $this->getPublicValues('strCode','mst_financetaxgroup','intId',$taxId);
			$response['type']		= 'fail';
			$response['ErrorMsg']	= "No GL Account available for tax code : $taxCode";
			return $response;
		}
	}
	private function transactionSaveSql($supplierId,$currency,$docType,$billInvNo,$billInvYear,$taxChrtOfAccount,$taxAmount,$dtDate)
	{
		global $session_userId;
		global $session_locationId;
		global $session_companyId;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "INSERT INTO finance_other_payable_transaction 
				(
				SUPPLIER_ID, 
				CURRENCY_ID, 
				DOCUMENT_YEAR, 
				DOCUMENT_NO, 
				DOCUMENT_TYPE, 
				BILL_INVOICE_YEAR, 
				BILL_INVOICE_NO, 
				LEDGER_ID, 
				VALUE, 
				COMPANY_ID, 
				LOCATION_ID, 
				USER_ID,
				TRANSACTION_DATE_TIME
				)
				VALUES
				(
				'$supplierId', 
				'$currency', 
				'$billInvYear', 
				'$billInvNo', 
				'$docType', 
				'$billInvYear', 
				'$billInvNo', 
				'$taxChrtOfAccount', 
				'$taxAmount', 
				'$session_companyId', 
				'$session_locationId', 
				'$session_userId',
				'$dtDate'
				) ";
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$savedStatus = false;
			if($savedMasseged=='')
			{
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function saveFinanceTransaction($supplierId,$chartOfAccId,$disamonut,$billInvNo,$billInvYear,$docType,$transcType,$transcCat,$currency,$dtDate)
	{
		global $session_userId;
		global $session_locationId;
		global $session_companyId;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "INSERT INTO finance_transaction 
				( 
				CHART_OF_ACCOUNT_ID, 
				AMOUNT, 
				DOCUMENT_NO, 
				DOCUMENT_YEAR, 
				DOCUMENT_TYPE, 
				INVOICE_NO, 
				INVOICE_YEAR, 
				TRANSACTION_TYPE, 
				TRANSACTION_CATEGORY,
				TRANSACTION_CATEGORY_ID, 
				CURRENCY_ID, 
				LOCATION_ID, 
				COMPANY_ID, 
				LAST_MODIFIED_BY,
				LAST_MODIFIED_DATE				
				)
				VALUES
				(
				'$chartOfAccId', 
				'$disamonut', 
				'$billInvNo', 
				'$billInvYear', 
				'$docType', 
				'$billInvNo', 
				'$billInvYear', 
				'$transcType', 
				'$transcCat',
				'$supplierId',
				'$currency', 
				'$session_locationId', 
				'$session_companyId', 
				'$session_userId',
				'$dtDate'
				)";
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$savedStatus = false;
			if($savedMasseged=='')
			{
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function saveSupplierTransaction($supplierId,$currency,$billInvNo,$billInvYear,$grandTotal,$dtDate)
	{
		global $savedStatus;
		global $savedMasseged;
		
		$supChrtOfAccount = $this->getSupChartOfAccount($supplierId);
		if($supChrtOfAccount['type']=='fail')
		{
			$savedStatus = false;
			if($savedMasseged=='')
				$savedMasseged	= $supChrtOfAccount['ErrorMsg'];
		}
		else
		{
			$this->saveFinanceTransaction($supplierId,$supChrtOfAccount,$grandTotal,$billInvNo,$billInvYear,'BILLINVOICE','C','OP',$currency,$dtDate);
		}
	}
	private function getSupChartOfAccount($supplierId)
	{
		$sql = "SELECT CHART_OF_ACCOUNT_ID
				FROM finance_mst_chartofaccount
				WHERE CATEGORY_TYPE='S' AND
				CATEGORY_ID='$supplierId' AND
				STATUS='1' ";
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		
		if(mysqli_num_rows($result)>0)
		{
			return $row["CHART_OF_ACCOUNT_ID"];
		}
		else
		{
			$supplierName			= $this->getPublicValues('strName','mst_supplier','intId',$supplierId);
			$response['type']		= 'fail';
			$response['ErrorMsg']	= "No GL Account available for Supplier : $supplierName";
			return $response;
		}
	}
	private function saveSvatTransaction($supplierId,$currency,$billInvNo,$billInvYear,$taxId,$svatValue,$dtDate)
	{
		global $savedStatus;
		global $savedMasseged;
		
		$chkSupSvat = $this->chkSupplierIsSvat($supplierId);
		if(!$chkSupSvat)
		{
			return;
		}
		
		$svatCrChartOfAccount = $this->getSvatChartOfAccount('C');
		if($svatCrChartOfAccount['type']=='fail')
		{
			$savedStatus = false;
			if($savedMasseged=='')
				$savedMasseged	= $svatCrChartOfAccount['ErrorMsg'];
		}
		else
		{
			$this->saveFinanceTransaction($supplierId,$svatCrChartOfAccount,$svatValue,$billInvNo,$billInvYear,'BILLINVOICE','C','OP',$currency,$dtDate);
		}
		
		$svatDrChartOfAccount = $this->getSvatChartOfAccount('D');
		if($svatDrChartOfAccount['type']=='fail')
		{
			$savedStatus = false;
			if($savedMasseged=='')
				$savedMasseged	= $svatDrChartOfAccount['ErrorMsg'];
		}
		else
		{
			$this->saveFinanceTransaction($supplierId,$svatDrChartOfAccount,$svatValue,$billInvNo,$billInvYear,'BILLINVOICE','D','OP',$currency,$dtDate);
		}
		
	}
	private function getSvatChartOfAccount($transcType)
	{
		$sql = "SELECT CHART_OF_ACCOUNT_ID 
				FROM finance_mst_chartofaccount_invoice_type 
				WHERE INVOICE_TYPE_ID='3' AND 
				TRANSACTION_CATEGORY='SU' AND 
				TRANSACTION_TYPE='$transcType' ";
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		
		if(mysqli_num_rows($result)>0)
		{
			return $row["CHART_OF_ACCOUNT_ID"];
		}
		else
		{
			$invoiceTypeName		= $this->getPublicValues('strInvoiceType','mst_invoicetype','intId','3');
			$response['type']		= 'fail';
			$response['ErrorMsg']	= "No GL Account available for invoice type : $invoiceTypeName";
			return $response;
		}
	}
	private function chkSupplierIsSvat($supplierId)
	{
		$sql = "SELECT intInvoiceType
				FROM mst_supplier
				WHERE intId='$supplierId' ";
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		
		if($row['intInvoiceType']==3)
			return true;
		else
			return false;
	}
	private function getPublicValues($select_fieldName,$tableName,$where_fieldId,$where_fieldValue)
	{
		$sql = "SELECT $select_fieldName AS VALUE FROM $tableName WHERE $where_fieldId = '$where_fieldValue'";
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		return $row["VALUE"];
	}
	private function getDetailData($billInvNo,$billInvYear)
	{
		$sql = "SELECT
				OPBH.COMPANY_ID,
				CFCP.REPORT_FIELD_ID, 
				OPBH.CURRENCY_ID, 
				OPBD.CHART_OF_ACCOUNT_ID ,  
				OPBD.QTY,
				OPBD.UNIT_PRICE,
				OPBD.TAX_AMOUNT,
				IFNULL(OPBD.DISCOUNT,0) AS DISCOUNT,
				OPBH.COMPANY_ID, 
				OPBH.BILL_DATE 
				
				FROM
				finance_other_payable_bill_header OPBH
				INNER JOIN finance_other_payable_bill_details OPBD ON OPBH.BILL_INVOICE_NO = OPBD.BILL_INVOICE_NO AND 
				OPBH.BILL_INVOICE_YEAR = OPBD.BILL_INVOICE_YEAR
				INNER JOIN finance_cashflow_cpanal CFCP ON FIND_IN_SET(OPBD.CHART_OF_ACCOUNT_ID,CFCP.GL_ID)
				WHERE
				CFCP.BOO_GL_ITEM = 1 AND 
				OPBD.BILL_INVOICE_NO = '$billInvNo' AND
				OPBD.BILL_INVOICE_YEAR = '$billInvYear' ";
		
		$result = $this->db->RunQuery2($sql);
		return $result ;
	}
}

?>
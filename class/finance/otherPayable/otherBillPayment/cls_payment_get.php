<?php
class Cls_Payment_Get
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	public function getDetailData($supplier,$currency)
	{
		return $this->detailData_sql($supplier,$currency);
	}
	public function getPaymentMethod($supplier)
	{
		return $this->paymentMethod_sql($supplier);
	}
	public function getSavedHeaderData($paymentNo,$paymentYear)
	{
		return $this->SavedHeaderData_sql($paymentNo,$paymentYear);
	}
	public function getSavedDetailData($paymentNo,$paymentYear)
	{
		return $this->SavedDetailData_sql($paymentNo,$paymentYear);
	}
	public function getChartOfAccountData($paymentNo,$paymentYear)
	{
		return $this->ChartOfAccountData_sql($paymentNo,$paymentYear);
	}
	public function getHeaderData_pdf($paymentNo,$paymentYear,$executionType)
	{
		return $this->HeaderData_pdf($paymentNo,$paymentYear,$executionType);
	}
	public function getDetailData_pdf($paymentNo,$paymentYear,$executionType)
	{
		return $this->DetailData_pdf($paymentNo,$paymentYear,$executionType);
	}
	public function getRptHeaderData($paymentNo,$paymentYear,$company)
	{
		return $this->getRptHeaderData_sql($paymentNo,$paymentYear,$company);
	}
	public function getRptGLDetailData($paymentNo,$paymentYear)
	{
		return $this->getRptGLDetailData_sql($paymentNo,$paymentYear);
	}
	private function detailData_sql($supplier,$currency)
	{
		$sql = "SELECT CONCAT(OPH.BILL_INVOICE_NO,'/',OPH.BILL_INVOICE_YEAR) AS invoiceNo,
				DATE(OPH.BILL_DATE) AS invoiceDate,
				OPH.BILL_INVOICE_NO,
				OPH.BILL_INVOICE_YEAR,
				IFNULL(OPH.REFERENCE_NO,'') as referenceNo,
				
				IFNULL((SELECT ROUND(SUM(OPT.VALUE),2)
				FROM finance_other_payable_transaction OPT
				WHERE OPT.BILL_INVOICE_NO=OPH.BILL_INVOICE_NO AND
				OPT.BILL_INVOICE_YEAR=OPH.BILL_INVOICE_YEAR AND
				OPT.DOCUMENT_TYPE='BILLINVOICE'),0) AS invoiceAmount,
				
				IFNULL((SELECT ROUND((SUM(ABS(OPT.VALUE))),2)
				FROM finance_other_payable_transaction OPT
				WHERE OPT.BILL_INVOICE_NO=OPH.BILL_INVOICE_NO AND
				OPT.BILL_INVOICE_YEAR=OPH.BILL_INVOICE_YEAR AND
				OPT.DOCUMENT_TYPE='BILLPAYMENT'),0) AS paidAmount,
				
				IFNULL((SELECT ROUND((SUM(OPT.VALUE)),2)
				FROM finance_other_payable_transaction OPT
				WHERE OPT.BILL_INVOICE_NO=OPH.BILL_INVOICE_NO AND
				OPT.BILL_INVOICE_YEAR=OPH.BILL_INVOICE_YEAR),0) AS toBePaidAmount
				
				FROM finance_other_payable_bill_header OPH
				WHERE SUPPLIER_ID='$supplier' AND
				CURRENCY_ID='$currency' AND
				OPH.STATUS<>-2
				HAVING toBePaidAmount>0
				ORDER BY OPH.BILL_DATE ";
		
		$result = $this->db->RunQuery($sql);
		while($row = mysqli_fetch_array($result))
		{
			$data['invoiceNo'] 	 	= $row['invoiceNo'];
			$data['billInvoiceNo'] 	= $row['BILL_INVOICE_NO'];
			$data['billInvoiceYear']= $row['BILL_INVOICE_YEAR'];
			$data['invoiceDate'] 	= $row['invoiceDate'];
			$data['invoiceAmount'] 	= $row['invoiceAmount'];
			$data['paidAmount'] 	= $row['paidAmount'];
			$data['toBePaidAmount'] = $row['toBePaidAmount'];
			$data['referenceNo'] 	= $row['referenceNo'];
			
			$arrDetailData[] 		= $data;
		}
		return $arrDetailData;
	}
	private function paymentMethod_sql($supplier)
	{
		$sql = "SELECT IFNULL(intPaymentsMethodsId,'') as PaymentsMethodsId
				FROM mst_supplier
				WHERE intStatus=1 AND
				intId='$supplier' ";
		
		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row['PaymentsMethodsId'];
	}
	private function SavedHeaderData_sql($paymentNo,$paymentYear)
	{
		$sql = "SELECT SUPPLIER_ID,
				PAY_DATE,
				CURRENCY_ID,
				PAY_METHOD,
				REMARKS,
				BANK_REFERENCE_NO,
				STATUS
				FROM finance_other_payable_payment_header
				WHERE 
				PAYMENT_NO='$paymentNo' AND
				PAYMENT_YEAR='$paymentYear' ";
		
		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row;
	}
	private function SavedDetailData_sql($paymentNo,$paymentYear)
	{
		$sql = "SELECT CONCAT(OPH.BILL_INVOICE_NO,'/',OPH.BILL_INVOICE_YEAR) AS invoiceNo,
				DATE(OPH.BILL_DATE) AS invoiceDate,
				OPH.BILL_INVOICE_NO,
				OPH.BILL_INVOICE_YEAR,
				OPH.REFERENCE_NO,
				
				IFNULL((SELECT ROUND(SUM(OPT.VALUE),2)
				FROM finance_other_payable_transaction OPT
				WHERE OPT.BILL_INVOICE_NO=OPH.BILL_INVOICE_NO AND
				OPT.BILL_INVOICE_YEAR=OPH.BILL_INVOICE_YEAR AND
				OPT.DOCUMENT_TYPE='BILLINVOICE'),0) AS invoiceAmount,
				
				IFNULL((SELECT ROUND((SUM(ABS(OPT.VALUE))),2)
				FROM finance_other_payable_transaction OPT
				WHERE OPT.BILL_INVOICE_NO=OPH.BILL_INVOICE_NO AND
				OPT.BILL_INVOICE_YEAR=OPH.BILL_INVOICE_YEAR AND
				OPT.DOCUMENT_TYPE='BILLPAYMENT'),0) AS paidAmount,
				
				IFNULL((SELECT ROUND((SUM(OPT.VALUE)),2)
				FROM finance_other_payable_transaction OPT
				WHERE OPT.BILL_INVOICE_NO=OPH.BILL_INVOICE_NO AND
				OPT.BILL_INVOICE_YEAR=OPH.BILL_INVOICE_YEAR),0) AS toBePaidAmount,
				OPPD.AMOUNT AS currPayAmount
				FROM finance_other_payable_payment_details OPPD
				INNER JOIN finance_other_payable_bill_header OPH ON OPH.BILL_INVOICE_NO=OPPD.BILL_INVOICE_NO AND
				OPH.BILL_INVOICE_YEAR=OPPD.BILL_INVOICE_YEAR
				WHERE OPPD.PAYMENT_NO='$paymentNo' AND
				OPPD.BILL_INVOICE_YEAR='$paymentYear'
				ORDER BY OPH.BILL_DATE";
		
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	private function ChartOfAccountData_sql($paymentNo,$paymentYear)
	{
		$sql = "SELECT 	ACCOUNT_ID, 
				AMOUNT, 
				MEMO, 
				COST_CENTER
				FROM 
				finance_other_payable_payment_gl 
				WHERE
				PAYMENT_NO='$paymentNo' AND
				PAYMENT_YEAR='$paymentYear' ";
		
		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row;
	}
 	private function HeaderData_pdf($paymentNo,$paymentYear,$executionType)
	{
		global $companyId;
		
		$sql = "SELECT 
					PH.CURRENCY_ID 																				AS CURRENCY_ID,
					PH.COMPANY_ID																				AS COMPANY_ID,
					FC.strCode 																					AS currency,
					PGLH.COST_CENTER 																			AS COST_CENTER,
					MFD.strCostCenterCode 																		AS strCostCenterCode,
					COA.CHART_OF_ACCOUNT_NAME 																	AS CHART_OF_ACCOUNT_NAME,
					DATE(PH.PAY_DATE) 																			AS paymentDate,
					(SELECT CONCAT(RIGHT(YEAR(DATE(dtmStartingDate)),2),'-',RIGHT(YEAR(DATE(dtmClosingDate)),2))
					FROM mst_financeaccountingperiod
					INNER JOIN mst_financeaccountingperiod_companies 
						ON mst_financeaccountingperiod.intId = mst_financeaccountingperiod_companies.intPeriodId
					WHERE dtmStartingDate<=DATE(PH.PAY_DATE) AND
					dtmClosingDate>=DATE(PH.PAY_DATE) AND
					mst_financeaccountingperiod_companies.intCompanyId='$companyId') 							AS voucherYear,
					LPAD(MONTH(PH.PAY_DATE),2,'0') 																AS voucherMonth,
					(PH.VOUCHER_NO) 																			AS voucherNo,
					MS.strName 																					AS supplier,
					PH.REMARKS 																					AS REMARKS,
					PH.STATUS 																					AS STATUS,
					PH.PRINT_STATUS,
					PH.CREATED_BY 																				AS CREATED_BY,
					U.strFullName 																				AS CREATED_BY_NAME,
					ROUND((SELECT SUM(AMOUNT) 
							FROM finance_other_payable_payment_details PD 
							WHERE PD.PAYMENT_NO = PH.PAYMENT_NO 
							AND PD.PAYMENT_YEAR = PH.PAYMENT_YEAR),2) 											AS TOTAL_AMOUNT,
					PH.BANK_REFERENCE_NO AS CHEQUE_NO
				
				FROM finance_other_payable_payment_header PH
				INNER JOIN finance_other_payable_payment_gl PGLH ON PH.PAYMENT_NO=PGLH.PAYMENT_NO AND PH.PAYMENT_YEAR=PGLH.PAYMENT_YEAR
				INNER JOIN mst_financecurrency FC ON PH.CURRENCY_ID=FC.intId
				INNER JOIN finance_mst_chartofaccount COA ON PGLH.ACCOUNT_ID=COA.CHART_OF_ACCOUNT_ID
				INNER JOIN mst_supplier MS ON MS.intId=PH.SUPPLIER_ID
				INNER JOIN mst_financedimension MFD ON MFD.intId=PGLH.COST_CENTER
				INNER JOIN sys_users U ON U.intUserId = PH.CREATED_BY
				WHERE PH.PAYMENT_NO='$paymentNo' AND
				PH.PAYMENT_YEAR='$paymentYear' AND
				PH.COMPANY_ID='$companyId' ";
		$result 	= $this->db->$executionType($sql);
 		$resultArry = mysqli_fetch_array($result);
	
		return $resultArry;
	}
	private function DetailData_pdf($paymentNo,$paymentYear,$executionType)
	{
		global $companyId;
		
		$sql = "SELECT 
					BH.REFERENCE_NO AS invoiceNo,
					PD.AMOUNT AS amonut,
					'' AS PONo,
					'' AS GRNNo
				FROM finance_other_payable_payment_details PD
				INNER JOIN finance_other_payable_payment_header PIH 
					ON PIH.PAYMENT_NO = PD.PAYMENT_NO 
					AND PIH.PAYMENT_YEAR = PD.PAYMENT_YEAR
				INNER JOIN finance_other_payable_bill_header BH 
					ON BH.BILL_INVOICE_NO = PD.BILL_INVOICE_NO 
					AND BH.BILL_INVOICE_YEAR = PD.BILL_INVOICE_YEAR
				WHERE PD.PAYMENT_NO='$paymentNo' AND
				PD.PAYMENT_YEAR='$paymentYear' ";
		$result	= $this->db->$executionType($sql);
		
		return $result;
	}
	private function getRptHeaderData_sql($paymentNo,$paymentYear,$company)
	{
		$sql = "SELECT MS.strName AS supplier,
				SUPPLIER_ID,
				CURRENCY_ID, 
				PAY_DATE,  
				REMARKS, 
				BANK_REFERENCE_NO, 
				STATUS,
				FC.strDescription AS currency,
				FPM.strName AS paymentMethod,
				(SELECT SUM(AMOUNT)
				FROM finance_other_payable_payment_details
				WHERE PAYMENT_NO=SPH.PAYMENT_NO AND
				PAYMENT_YEAR=SPH.PAYMENT_YEAR) totPaidAmount
				FROM finance_other_payable_payment_header SPH
				INNER JOIN mst_supplier MS ON MS.intId=SPH.SUPPLIER_ID
				LEFT JOIN mst_financecurrency FC ON FC.intId=SPH.CURRENCY_ID
				LEFT JOIN mst_financepaymentsmethods FPM ON FPM.intId=SPH.PAY_METHOD
				WHERE PAYMENT_NO='$paymentNo' AND
				PAYMENT_YEAR='$paymentYear' AND
				COMPANY_ID='$company' ";
				
		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row;
	}
	private function getRptGLDetailData_sql($paymentNo,$paymentYear)
	{
		$sql = "SELECT 	
				(SELECT CONCAT(CONCAT(FMT.FINANCE_TYPE_CODE,'',FMMT.MAIN_TYPE_CODE,'',FMST.SUB_TYPE_CODE,'',FCOA.CHART_OF_ACCOUNT_CODE),' - ',
				FCOA.CHART_OF_ACCOUNT_NAME)
				FROM finance_mst_chartofaccount FCOA
				INNER JOIN finance_mst_account_sub_type FMST ON FMST.SUB_TYPE_ID=FCOA.SUB_TYPE_ID
				INNER JOIN finance_mst_account_main_type FMMT ON FMMT.MAIN_TYPE_ID=FMST.MAIN_TYPE_ID
				INNER JOIN finance_mst_account_type FMT ON FMT.FINANCE_TYPE_ID=FMMT.FINANCE_TYPE_ID
				INNER JOIN finance_mst_chartofaccount_company FCOAC ON FCOAC.CHART_OF_ACCOUNT_ID=FCOA.CHART_OF_ACCOUNT_ID
				WHERE FCOA.CHART_OF_ACCOUNT_ID=SPGL.ACCOUNT_ID) AS account,
				FD.strName AS costCenter, 
				AMOUNT, 
				MEMO
				FROM finance_other_payable_payment_gl SPGL
				INNER JOIN mst_financedimension FD ON FD.intId=SPGL.COST_CENTER
				WHERE PAYMENT_NO='$paymentNo' AND PAYMENT_YEAR='$paymentYear' ";
		
		$result = $this->db->RunQuery($sql);
		
		return $result;
	}
	
}
?>
<?php
include  	  $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
require_once  $_SESSION['ROOT_PATH']."class/finance/cls_common_get.php";
require_once  $_SESSION['ROOT_PATH']."class/finance/cash_flow/cls_cf_common.php";
require_once  $_SESSION['ROOT_PATH']."class/finance/cash_flow/cls_cf_common_set.php";

$session_userId		= $_SESSION["userId"];
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];
$savedMasseged		= "";
$error_sql			= "";

$obj_common			= new cls_commonFunctions_get($db);
$finance_common_get = new cls_common_get($db);
$obj_cf_common		= new cls_cf_common($db);
$obj_cf_common_set	= new cls_cf_common_set($db);

class Cls_Payment_Set
{
	private $db;
	private $obj_exchgRate_get;
	
	function __construct($db,$obj_exchgRate_get)
	{
		$this->db 				 = $db;
		$this->obj_exchgRate_get = $obj_exchgRate_get;
	}
	public function Save($arrHeader,$arrDetails,$programCode)
	{
		global $obj_common;
		global $finance_common_get;
		global $savedStatus;
		global $savedMasseged;
		global $session_locationId;
		global $session_companyId;
		global $session_userId;
		global $error_sql;
		global $obj_cf_common;
		global $obj_cf_common_set;
		
		$paymentNo		= $arrHeader["paymentNo"];
		$paymentYear	= $arrHeader["paymentYear"];
		$payDate		= $arrHeader["payDate"];
		$supplierId		= $arrHeader["supplierId"];
		$currency		= $arrHeader["currency"];
		$Remarks		= $obj_common->replace($arrHeader["Remarks"]);
		$payMethod		= $arrHeader["payMethod"];
		$bankRefNo		= $arrHeader["bankRefNo"];
		$totPayAmount	= $arrHeader["totPayAmount"];
		$totGLAlcAmount = $arrHeader["totGLAlocAmount"];
		$accountId 		= $arrHeader["accountId"];
		$glAmount 		= $arrHeader["glAmount"];
		$memo 			= $obj_common->replace($arrHeader["memo"]);
		$costCenter 	= $arrHeader["costCenter"];
		
		$savedStatus	= true;
		$this->db->begin();
		
		$this->checkPermission($programCode,$session_userId,'intAdd');
		$this->checkSaveValidation($paymentNo,$paymentYear,$totPayAmount,$totGLAlcAmount);
		
		$exchngRateArr	 = $this->obj_exchgRate_get->GetAllValues($currency,2,$payDate,$session_companyId,'RunQuery2');
		if($exchngRateArr['AVERAGE_RATE']=='')
		{
			$savedStatus 	= false;
			$savedMasseged	= 'Please enter exchange rates for '.$payDate.' before save.';
		}
		
		$invoice_array 	= $obj_common->GetSystemMaxNo('intOtherBillPaymentNo',$session_locationId);
		if($invoice_array["rollBackFlag"]==1)
		{
			$savedStatus 	= false;
			if($savedMasseged=='')
			{
				$savedMasseged 	= $invoice_array["msg"];
				$error_sql		= $invoice_array["q"];
			}	
		}
		$paymentNo		= $invoice_array["max_no"];
		$paymentYear	= date('Y');
		
		$this->SaveHeader($paymentNo,$paymentYear,$payDate,$supplierId,$currency,$Remarks,$payMethod,$bankRefNo,$accountId,$costCenter);
		$this->SaveGLHeader($paymentNo,$paymentYear,$accountId,$glAmount,$memo,$costCenter);
		
		foreach($arrDetails as $array_loop)
		{
			$invoiceNoArr 		= explode('/',trim($array_loop['invoiceNoArr']));
			$payAmount			= $array_loop["payAmount"];
			$billInvoiceNo		= $invoiceNoArr[0];
			$billInvoiceYear	= $invoiceNoArr[1];
			
			$this->checkPayAmount($billInvoiceNo,$billInvoiceYear,$payAmount);
			$this->SaveDetails($paymentNo,$paymentYear,$billInvoiceNo,$billInvoiceYear,$payAmount);
			$this->SaveTransaction($supplierId,$currency,'BILLPAYMENT',$paymentNo,$paymentYear,$billInvoiceNo,$billInvoiceYear,$chartOfAccId,$payAmount,$payDate);
			$this->SaveSupplierTransaction($supplierId,$payAmount,$paymentNo,$paymentYear,$billInvoiceNo,$billInvoiceYear,$currency,$bankRefNo,'',$payDate);
			$this->SaveBankTransaction($supplierId,$accountId,$payAmount,$paymentNo,$paymentYear,$billInvoiceNo,$billInvoiceYear,$currency,$bankRefNo,$memo,$payDate);	
		}
		
		//-------------------------- cash flow update 27/05/2014 by lahiru --------------------------------
		
		$CFUpdResult	= $this->getDetailData($paymentNo,$paymentYear);
		while($row = mysqli_fetch_array($CFUpdResult))
		{
			$amount	   		= $row['AMOUNT'];	
			$currencyId		= $row['CURRENCY_ID'];
			$entryDate		= $row['PAY_DATE'];
			$company		= $row['COMPANY_ID'];
			//$rpt_field		= $row['REPORT_FIELD_ID'];
			$billInvNo		= $row['BILL_INVOICE_NO'];
			$billInvYear	= $row['BILL_INVOICE_YEAR'];
			
			$tot_to_save	= $amount;
			
			$resultInvWise	= $this->getInvoicedDetails($billInvNo,$billInvYear);
			while($rowInv = mysqli_fetch_array($resultInvWise))
			{
				$rpt_field		= $rowInv['REPORT_FIELD_ID'];
				$invAmount	   	= $rowInv['invoiceAmt'];
				$invoiceDate	= $rowInv['INVOICE_DATE'];
				
				if($tot_to_save>0)
				{
					if($invAmount<=$tot_to_save)
					{
						$saveInvAmount 	 	= $invAmount;
						$tot_to_save 		= $tot_to_save-$invAmount;
					}
					else if($invAmount>$tot_to_save)
					{
						$saveInvAmount 		= $tot_to_save;
						$tot_to_save		= 0;
					}
					$cfAmountResult	= $obj_cf_common->get_cf_log_bal_to_payment_result($session_locationId,$rpt_field,$currencyId,$entryDate,$company,'RunQuery2');	
					while($rowAmt = mysqli_fetch_array($cfAmountResult))
					{
						$balAmount	= round($rowAmt['BAL'],2);
						if(($saveInvAmount>0) && ($balAmount>0))
						{
							if($saveInvAmount<=$balAmount)
							{
								$saveAmnt 	 	= $saveInvAmount;
								$saveInvAmount 	= 0;
							}
							else if($saveInvAmount>$balAmount)
							{
								$saveAmnt 		= $balAmount;
								$saveInvAmount	= $saveInvAmount-$saveAmnt;
							}
							
							$rcv_date	= $rowAmt['RECEIVE_DATE'];
							$currency	= $rowAmt['CURRENCY_ID'];
							
							$resultArr	= $obj_cf_common_set->insert_to_log($session_locationId,$rpt_field,$rcv_date,$invoiceDate,$entryDate,$paymentNo,$paymentYear,'BILL_PAYMENT',$saveAmnt,$currency,'RunQuery2');
							if($resultArr['type']=='fail' && $savedStatus)
							{
								$savedStatus	= false;
								$savedMasseged	= $resultArr['msg'];
								$error_sql		= $resultArr['sql'];
							}
						}
					}
				}
			}
			if($saveInvAmount>0)
			{
				if($rcv_date == '')
					$rcv_date = $entryDate;
				
				if($currency == '')
					$currency = $currencyId;
					
				$resultInvWise	= $this->getInvoicedDetails($billInvNo,$billInvYear);
				while($rowInv = mysqli_fetch_array($resultInvWise))
				{
					$rpt_field		= $rowInv['REPORT_FIELD_ID'];
					$invAmount	   	= $rowInv['invoiceAmt'];
					$invoiceDate	= $rowInv['INVOICE_DATE'];
					
					if($saveInvAmount>0)
					{
						if($invAmount<=$saveInvAmount)
						{
							$saveAmnt 	 	= $invAmount;
							$saveInvAmount 	= $saveInvAmount-$invAmount;
						}
						else if($invAmount>$saveInvAmount)
						{
							$saveAmnt 		= $saveInvAmount;
							$saveInvAmount	= 0;
						}
						
						$resultArr	= $obj_cf_common_set->insert_to_log($session_locationId,$rpt_field,$rcv_date,$invoiceDate,$entryDate,$paymentNo,$paymentYear,'BILL_PAYMENT',$saveAmnt,$currency,'RunQuery2');
						if($resultArr['type']=='fail' && $savedStatus)
						{
							$savedStatus	= false;
							$savedMasseged	= $resultArr['msg'];
							$error_sql		= $resultArr['sql'];
						}
					}	
				}
			}
			if($tot_to_save>0)
			{
				if($rcv_date == '')
					$rcv_date = $entryDate;
					
				$resultInvWise	= $this->getInvoicedDetails($billInvNo,$billInvYear);
				while($rowInv = mysqli_fetch_array($resultInvWise))
				{
					$rpt_field		= $rowInv['REPORT_FIELD_ID'];
					$invAmount	   	= $rowInv['invoiceAmt'];
					$invoiceDate	= $rowInv['INVOICE_DATE'];
					
					if($tot_to_save>0)
					{
						if($invAmount<=$tot_to_save)
						{
							$saveAmnt 	 	= $invAmount;
							$tot_to_save 	= $tot_to_save-$invAmount;
						}
						else if($invAmount>$tot_to_save)
						{
							$saveAmnt 		= $tot_to_save;
							$tot_to_save	= 0;
						}
						$resultArr	= $obj_cf_common_set->insert_to_log($session_locationId,$rpt_field,$rcv_date,$invoiceDate,$entryDate,$paymentNo,$paymentYear,'BILL_PAYMENT',$saveAmnt,$currencyId,'RunQuery2');
						if($resultArr['type']=='fail' && $savedStatus)
						{
							$savedStatus	= false;
							$savedMasseged	= $resultArr['msg'];
							$error_sql		= $resultArr['sql'];
						}	
					}
				}
			}
		}
		//-------------------------------------------------------------------------------------------------
		
		$sysNoArr					= $obj_common->validateDuplicateSerialNoWithSysNo($paymentNo,'intOtherBillPaymentNo',$session_locationId);
		if($sysNoArr['type'] == 'fail' && $savedStatus)
		{
			$savedStatus			= false;
			$savedMasseged	 		= $sysNoArr['msg'];
		}
		if($savedStatus)
		{
			$this->db->commit();
			$response['type'] 			= "pass";
			$response['msg'] 			= "Saved Successfully.";
			
			$response['paymentNo']		= $paymentNo;
			$response['paymentYear']	= $paymentYear;
		}
		else
		{
			$this->db->rollback();
			$response['type'] 			= "fail";
			$response['msg'] 			= $savedMasseged;
			$response['sql']			= $error_sql;
		}
		return json_encode($response);
	}
	public function Cancel($paymentNo,$paymentYear,$programCode)
	{
		global $obj_common;
		global $savedStatus;
		global $savedMasseged;
		global $session_locationId;
		global $session_userId;
		global $error_sql;
		global $obj_cf_common_set;
		
		$savedStatus	= true;
		$this->db->begin();
		
		$this->checkPermission($programCode,$session_userId,'intCancel');
		$this->checkCancelValidation($paymentNo,$paymentYear);
		
		$this->cancleHeader($paymentNo,$paymentYear);
		$this->cancleOtherPayableTransaction($paymentNo,$paymentYear,'BILLPAYMENT');
		$this->cancleFinanceTransaction($paymentNo,$paymentYear,'BILLPAYMENT');
		
		$resultDCFArr	= $obj_cf_common_set->delete_from_log($paymentNo,$paymentYear,'BILL_PAYMENT','RunQuery2');
		if($resultDCFArr['type'] == 'fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged	= $resultDCFArr['msg']; 
			$error_sql		= $resultDCFArr['sql']; 
		}
		
		if($savedStatus)
		{
			$this->db->commit();
			$response['type'] 			= "pass";
			$response['msg'] 			= "Cancelled Successfully.";
		}
		else
		{
			$this->db->rollback();
			$response['type'] 			= "fail";
			$response['msg'] 			= $savedMasseged;
			$response['sql']			= $error_sql;
		}
		return json_encode($response);
	}
	private function checkPermission($programCode,$session_userId,$feild)
	{
		global $obj_common;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$chkPermission 	= $obj_common->Load_menupermision2($programCode,$session_userId,$feild);
		if($chkPermission!=1)
		{
			$savedStatus	= false;
			if($savedMasseged=='')
				$savedMasseged	= "You do not have permission to Save Other Payable Payment.";
		}
	}
	private function checkSaveValidation($paymentNo,$paymentYear,$totPayAmount,$totGLAlcAmount)
	{
		global $savedStatus;
		global $savedMasseged;
		
		if($paymentNo!='' && $paymentYear!='')
		{
			$savedStatus	= false;
			if($savedMasseged=='')
				$savedMasseged	= "You can not update Other Payable Payment.";
		}
		if($totPayAmount!=$totGLAlcAmount)
		{
			$savedStatus	= false;
			if($savedMasseged=='')
				$savedMasseged	= "Total Pay amount must equal to Total GL Alocated amonut.";
		}
		if($totPayAmount<=0 || $totGLAlcAmount<=0)
		{
			$savedStatus	= false;
			if($savedMasseged=='')
				$savedMasseged	= "Totals must greater than 0.";
		}
	}
	private function SaveHeader($paymentNo,$paymentYear,$payDate,$supplierId,$currency,$Remarks,$payMethod,$bankRefNo,$accountId,$costCenter)
	{
		global $finance_common_get;
		global $session_userId;
		global $session_locationId;
		global $session_companyId;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$response = $finance_common_get->getPaymentVoucherNo($payDate,$accountId,$costCenter,'RunQuery2');
		if($response['type']=='fail' && ($savedStatus))
		{
			$savedStatus	= false;
			$savedMasseged	= $response['msg'];
		}
		else
		{
			$voucherNo 		= $response['voucherNo'];
		}
		
		$sql = "INSERT INTO finance_other_payable_payment_header 
				(
				PAYMENT_NO, 
				PAYMENT_YEAR,
				VOUCHER_NO, 
				SUPPLIER_ID, 
				PAY_DATE, 
				COMPANY_ID, 
				CURRENCY_ID, 
				PAY_METHOD, 
				REMARKS, 
				BANK_REFERENCE_NO, 
				STATUS,
				CREATED_BY, 
				CREATED_DATE
				)
				VALUES
				(
				'$paymentNo', 
				'$paymentYear',
				'$voucherNo', 
				'$supplierId', 
				'$payDate', 
				'$session_companyId', 
				'$currency', 
				$payMethod, 
				'$Remarks', 
				'$bankRefNo', 
				'1', 
				'$session_userId', 
				NOW()
				); ";
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$savedStatus	= false;
			if($savedMasseged=='')
			{
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function SaveGLHeader($paymentNo,$paymentYear,$accountId,$glAmount,$memo,$costCenter)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "INSERT INTO finance_other_payable_payment_gl 
				(
				PAYMENT_NO, 
				PAYMENT_YEAR, 
				ACCOUNT_ID, 
				AMOUNT, 
				MEMO, 
				COST_CENTER
				)
				VALUES
				(
				'$paymentNo', 
				'$paymentYear', 
				'$accountId', 
				'$glAmount', 
				'$memo', 
				'$costCenter'
				)";
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$savedStatus	= false;
			if($savedMasseged=='')
			{
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function SaveDetails($paymentNo,$paymentYear,$billInvoiceNo,$billInvoiceYear,$payAmount)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "INSERT INTO finance_other_payable_payment_details 
				(
				PAYMENT_NO, 
				PAYMENT_YEAR, 
				BILL_INVOICE_NO, 
				BILL_INVOICE_YEAR, 
				AMOUNT
				)
				VALUES
				(
				'$paymentNo', 
				'$paymentYear', 
				'$billInvoiceNo', 
				'$billInvoiceYear', 
				'$payAmount'
				);";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$savedStatus = false;
			if($savedMasseged=='')
			{
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;	
			}
		}
	}
	private function SaveTransaction($supplierId,$currency,$docType,$paymentNo,$paymentYear,$billInvoiceNo,$billInvoiceYear,$chartOfAccId,$payAmount,$payDate)
	{
		global $session_userId;
		global $session_locationId;
		global $session_companyId;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$value = $payAmount*-1;
		$sql = "INSERT INTO finance_other_payable_transaction 
				(
				SUPPLIER_ID, 
				CURRENCY_ID, 
				DOCUMENT_YEAR, 
				DOCUMENT_NO, 
				DOCUMENT_TYPE, 
				BILL_INVOICE_YEAR, 
				BILL_INVOICE_NO, 
				VALUE, 
				COMPANY_ID, 
				LOCATION_ID, 
				USER_ID,
				TRANSACTION_DATE_TIME
				)
				VALUES
				(
				'$supplierId', 
				'$currency', 
				'$paymentYear', 
				'$paymentNo', 
				'$docType', 
				'$billInvoiceYear', 
				'$billInvoiceNo', 
				'$value', 
				'$session_companyId', 
				'$session_locationId', 
				'$session_userId',
				'$payDate'
				) ";
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$savedStatus = false;
			if($savedMasseged=='')
			{
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function SaveFinanceTransaction($supplierId,$chartOfAcc,$payAmount,$paymentNo,$paymentYear,$billInvoiceNo,$billInvoiceYear,$docType,$transacType,$transacCat,$currency,$bankRefNo,$remarks,$payDate)
	{
		global $session_userId;
		global $session_locationId;
		global $session_companyId;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "INSERT INTO finance_transaction 
				( 
				CHART_OF_ACCOUNT_ID, 
				AMOUNT, 
				DOCUMENT_NO, 
				DOCUMENT_YEAR, 
				DOCUMENT_TYPE, 
				INVOICE_NO, 
				INVOICE_YEAR, 
				TRANSACTION_TYPE, 
				TRANSACTION_CATEGORY,
				TRANSACTION_CATEGORY_ID, 
				CURRENCY_ID, 
				BANK_REFERENCE_NO,
				REMARKS,
				LOCATION_ID, 
				COMPANY_ID, 
				LAST_MODIFIED_BY,
				LAST_MODIFIED_DATE
				)
				VALUES
				(
				'$chartOfAcc', 
				'$payAmount', 
				'$paymentNo', 
				'$paymentYear', 
				'$docType', 
				'$billInvoiceNo', 
				'$billInvoiceYear', 
				'$transacType', 
				'$transacCat',
				'$supplierId',
				'$currency',
				'$bankRefNo',
				'$remarks', 
				'$session_locationId', 
				'$session_companyId', 
				'$session_userId',
				'$payDate'
				)";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$savedStatus = false;
			if($savedMasseged=='')
			{
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function getSupChartOfAccount($supplierId)
	{
		$sql = "SELECT CHART_OF_ACCOUNT_ID
				FROM finance_mst_chartofaccount
				WHERE CATEGORY_TYPE='S' AND
				CATEGORY_ID='$supplierId' AND
				STATUS='1' ";
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		
		if(mysqli_num_rows($result)>0)
		{
			return $row["CHART_OF_ACCOUNT_ID"];
		}
		else
		{
			$supplierName			= $this->GetPublicValues('strName','mst_supplier','intId',$supplierId);
			$response['type']		= 'fail';
			$response['ErrorMsg']	= "No GL Account available for Supplier : $supplierName";
			return $response;
		}
	}
	private function checkPayAmount($billInvoiceNo,$billInvoiceYear,$payAmount)
	{
		global $savedStatus;
		global $savedMasseged;
		
		$sql = "SELECT SUM(OPT.VALUE) AS toBePaidAmunt
				FROM finance_other_payable_transaction OPT
				WHERE OPT.BILL_INVOICE_NO='$billInvoiceNo' AND
				OPT.BILL_INVOICE_YEAR='$billInvoiceYear' ";
		
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		
		if($payAmount>$row['toBePaidAmunt'])
		{
			$savedStatus = false;
			if($savedMasseged=='')
			{
				$savedMasseged	= "Paying amount exceed to be paid Amount in $billInvoiceNo-$billInvoiceYear .";
			}
		}
	}
	private function SaveSupplierTransaction($supplierId,$payAmount,$paymentNo,$paymentYear,$billInvoiceNo,$billInvoiceYear,$currency,$bankRefNo,$remarks,$payDate)
	{
		global $savedStatus;
		global $savedMasseged;
		
		$supChrtOfAccount = $this->getSupChartOfAccount($supplierId);
		if($supChrtOfAccount['type']=='fail')
		{
			$savedStatus = false;
			if($savedMasseged=='')
				$savedMasseged	= $supChrtOfAccount['ErrorMsg'];
		}
		else
		{
			$this->SaveFinanceTransaction($supplierId,$supChrtOfAccount,$payAmount,$paymentNo,$paymentYear,$billInvoiceNo,$billInvoiceYear,'BILLPAYMENT','D','OP',$currency,$bankRefNo,$remarks,$payDate);
		}
	}
	private function SaveBankTransaction($supplierId,$accountId,$payAmount,$paymentNo,$paymentYear,$billInvoiceNo,$billInvoiceYear,$currency,$bankRefNo,$remarks,$payDate)
	{
		$this->SaveFinanceTransaction($supplierId,$accountId,$payAmount,$paymentNo,$paymentYear,$billInvoiceNo,$billInvoiceYear,'BILLPAYMENT','C','OP',$currency,$bankRefNo,$remarks,$payDate);
	}
	private function GetPublicValues($select_fieldName,$tableName,$where_fieldId,$where_fieldValue)
	{
		$sql = "SELECT $select_fieldName AS VALUE FROM $tableName WHERE $where_fieldId = '$where_fieldValue'";
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		return $row["VALUE"];
	}
	private function checkCancelValidation($paymentNo,$paymentYear)
	{
		global $savedStatus;
		global $savedMasseged;
		
		$sql = "SELECT STATUS
				FROM finance_other_payable_payment_header 
				WHERE PAYMENT_NO='$paymentNo' AND
				PAYMENT_YEAR='$paymentYear' ";
		
		$result = $this->db->RunQuery2($sql);
		$row 	= mysqli_fetch_array($result);
		
		if($row['STATUS']==-2)
		{
			$savedStatus	= false;
			if($savedMasseged=='')
				$savedMasseged	= "Already Cancelled.";
		}
	}
	private function cancleHeader($paymentNo,$paymentYear)
	{
		global $savedStatus;
		global $savedMasseged;
		global $session_userId;
		global $error_sql;
		
		$sql = "UPDATE finance_other_payable_payment_header 
				SET
				STATUS = '-2' , 
				LAST_MODIFY_BY = '$session_userId' , 
				LAST_MODIFY_DATE = NOW()
				WHERE
				PAYMENT_NO = '$paymentNo' AND 
				PAYMENT_YEAR = '$paymentYear' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$savedStatus	= false;
			if($savedMasseged=='')
			{
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function cancleOtherPayableTransaction($paymentNo,$paymentYear,$docType)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "DELETE FROM finance_other_payable_transaction 
				WHERE DOCUMENT_NO='$paymentNo' AND
				DOCUMENT_YEAR='$paymentYear' AND
				DOCUMENT_TYPE='$docType' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$savedStatus	= false;
			if($savedMasseged=='')
			{
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function cancleFinanceTransaction($paymentNo,$paymentYear,$docType)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "DELETE FROM finance_transaction 
				WHERE DOCUMENT_NO='$paymentNo' AND
				DOCUMENT_YEAR='$paymentYear' AND
				DOCUMENT_TYPE='$docType' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$savedStatus	= false;
			if($savedMasseged=='')
			{
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function getDetailData($paymentNo,$paymentYear)
	{
		$sql = "SELECT
				OPPH.COMPANY_ID,
				CFCP.REPORT_FIELD_ID, 
				OPPH.CURRENCY_ID, 
				OPBD.CHART_OF_ACCOUNT_ID ,  
				OPPD.AMOUNT, 
				OPPH.PAY_DATE,
				OPBD.BILL_INVOICE_NO,
				OPBD.BILL_INVOICE_YEAR 
				
				FROM
				finance_other_payable_payment_header OPPH
				INNER JOIN finance_other_payable_payment_details OPPD ON OPPH.PAYMENT_NO = OPPD.PAYMENT_NO AND 
				OPPH.PAYMENT_YEAR = OPPD.PAYMENT_YEAR
				INNER JOIN finance_other_payable_bill_details OPBD ON OPBD.BILL_INVOICE_NO=OPPD.BILL_INVOICE_NO
				AND OPBD.BILL_INVOICE_YEAR=OPPD.BILL_INVOICE_YEAR
				INNER JOIN finance_cashflow_cpanal CFCP ON FIND_IN_SET(OPBD.CHART_OF_ACCOUNT_ID,CFCP.GL_ID)
				WHERE
				CFCP.BOO_GL_ITEM = 1 AND 
				OPPD.PAYMENT_NO = '$paymentNo' AND
				OPPD.PAYMENT_YEAR = '$paymentYear'
				GROUP BY OPBD.BILL_INVOICE_YEAR,OPBD.BILL_INVOICE_NO ";
		
		$result = $this->db->RunQuery2($sql);
		return $result ;
	}
	private function getInvoicedDetails($billInvNo,$billInvYear)
	{
		$sql = "SELECT				
				CFCP.REPORT_FIELD_ID,
				ROUND((((OPBD.QTY*OPBD.UNIT_PRICE*(100-IFNULL(OPBD.DISCOUNT,0)))/100)+OPBD.TAX_AMOUNT),2) AS invoiceAmt	 ,
				OPBH.BILL_DATE AS INVOICE_DATE
				FROM
				finance_other_payable_bill_header OPBH
				INNER JOIN finance_other_payable_bill_details OPBD ON OPBH.BILL_INVOICE_NO = OPBD.BILL_INVOICE_NO AND 
				OPBH.BILL_INVOICE_YEAR = OPBD.BILL_INVOICE_YEAR
				INNER JOIN finance_cashflow_cpanal CFCP ON FIND_IN_SET(OPBD.CHART_OF_ACCOUNT_ID,CFCP.GL_ID)
				WHERE
				CFCP.BOO_GL_ITEM = 1 AND 
				OPBD.BILL_INVOICE_NO = '$billInvNo' AND
				OPBD.BILL_INVOICE_YEAR = '$billInvYear' ";
		
		$result = $this->db->RunQuery2($sql);
		return $result ;
	}
}
?>
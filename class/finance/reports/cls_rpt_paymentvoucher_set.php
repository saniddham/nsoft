<?php
class Cls_Rpt_Payment_Voucher_Set
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	public function UpdateStatus($serialNo,$serialYear,$type)
	{
		switch($type)
		{
			case 'PAYMENT':
				$tableName 	= 'finance_supplier_payment_header';
				$feildNo	= 'PAYMENT_NO';
				$feildYear	= 'PAYMENT_YEAR';
			break;
			case 'ADVANCE':
				$tableName 	= 'finance_supplier_advancepayment_header';
				$feildNo	= 'ADVANCE_PAYMENT_NO';
				$feildYear	= 'ADVANCE_PAYMENT_YEAR';
			break;
			case 'BILLPAYMENT':
				$tableName 	= 'finance_other_payable_payment_header';
				$feildNo	= 'PAYMENT_NO';
				$feildYear	= 'PAYMENT_YEAR';
			break;
			case 'BANK_PAYMENT':
				$tableName 	= 'finance_bank_payment_header';
				$feildNo	= 'BANK_PAYMENT_NO';
				$feildYear	= 'BANK_PAYMENT_YEAR';
			break;
		}
		$this->UpdateStatus_sql($serialNo,$serialYear,$tableName,$feildNo,$feildYear);
	}
	private function UpdateStatus_sql($serialNo,$serialYear,$tableName,$feildNo,$feildYear)
	{
		$sql = "UPDATE $tableName
				SET PRINT_STATUS = 1
				WHERE $feildNo = '$serialNo'
				AND $feildYear = '$serialYear' ";
		
		$this->db->RunQuery($sql);
	}
}
?>
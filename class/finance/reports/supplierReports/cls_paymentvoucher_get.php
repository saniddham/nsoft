<?php
$companyId	= $_SESSION["headCompanyId"];

class Cls_PaymentVoucher_Get
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	public function getHeaderData($paymentNo,$paymentYear,$payType)
	{
		return $this->HeaderData_sql($paymentNo,$paymentYear,$payType);
	}
	public function getDetailData($paymentNo,$paymentYear,$payType)
	{
		return $this->DetailData_sql($paymentNo,$paymentYear,$payType);
	}
	public function getExchangeRate($currency,$date)
	{
		return $this->ExchangeRate_sql($currency,$date);
	}
	public function getPreparedBy($preparedBy)
	{
		return $this->PreparedBy_sql($preparedBy);
	}
	private function HeaderData_sql($paymentNo,$paymentYear,$payType)
	{
		switch($payType)
		{
			case 'PAYMENT':
				$resultArry = $this->getPayHeaderData($paymentNo,$paymentYear);
			break;
			case 'ADVANCE':
				$resultArry = $this->getAdvHeaderData($paymentNo,$paymentYear);
			break;
			case 'BILLPAYMENT':
				$resultArry = $this->getBillPayHeaderData($paymentNo,$paymentYear);
			break;
		}
		return $resultArry;
	}
	private function DetailData_sql($paymentNo,$paymentYear,$payType)
	{
		switch($payType)
		{
			case 'PAYMENT':
				$result = $this->getPayDetailData($paymentNo,$paymentYear);
			break;
			case 'ADVANCE':
				$result = $this->getAdvDetailData($paymentNo,$paymentYear);
			break;
			case 'BILLPAYMENT':
				$result = $this->getBillPayDetailData($paymentNo,$paymentYear);
			break;
		}
		return $result;
	}
	private function getPayHeaderData($paymentNo,$paymentYear)
	{
		global $companyId;
		
		$sql = "SELECT PH.CURRENCY_ID,
				FC.strCode AS currency,
				PGLH.COST_CENTER,
				MFD.strCostCenterCode,
				COA.CHART_OF_ACCOUNT_NAME,
				DATE(PH.PAY_DATE) AS paymentDate,
				(SELECT CONCAT(RIGHT(YEAR(DATE(dtmStartingDate)),2),'-',RIGHT(YEAR(DATE(dtmClosingDate)),2))
				FROM mst_financeaccountingperiod
				INNER JOIN mst_financeaccountingperiod_companies ON mst_financeaccountingperiod.intId=mst_financeaccountingperiod_companies.intPeriodId
				WHERE dtmStartingDate<=DATE(PH.PAY_DATE) AND
				dtmClosingDate>=DATE(PH.PAY_DATE) AND
				mst_financeaccountingperiod_companies.intCompanyId='$companyId') AS voucherYear,
				LPAD(MONTH(PH.PAY_DATE),2,'0') AS voucherMonth,
				MS.strName AS supplier,
				PH.REMARKS,
				PH.STATUS,
				PH.CREATED_BY,
				ROUND((SELECT SUM(AMOUNT) FROM finance_supplier_payment_details PD WHERE PD.PAYMENT_NO = PH.PAYMENT_NO AND PD.PAYMENT_YEAR = PH.PAYMENT_YEAR),2) AS TOTAL_AMOUNT,
				PH.COMPANY_ID AS COMPANY_ID
				FROM finance_supplier_payment_header PH
				INNER JOIN finance_supplier_payment_gl PGLH ON PH.PAYMENT_NO=PGLH.PAYMENT_NO AND PH.PAYMENT_YEAR=PGLH.PAYMENT_YEAR
				INNER JOIN mst_financecurrency FC ON PH.CURRENCY_ID=FC.intId
				INNER JOIN finance_mst_chartofaccount COA ON PGLH.ACCOUNT_ID=COA.CHART_OF_ACCOUNT_ID
				INNER JOIN mst_supplier MS ON MS.intId=PH.SUPPLIER_ID
				INNER JOIN mst_financedimension MFD ON MFD.intId=PGLH.COST_CENTER
				WHERE PH.PAYMENT_NO='$paymentNo' AND
				PH.PAYMENT_YEAR='$paymentYear' AND
				PH.COMPANY_ID='$companyId' ";
		
		$result 	= $this->db->RunQuery($sql);
		$resultArry = mysqli_fetch_array($result);
	
		return $resultArry;
	}
	private function getAdvHeaderData($paymentNo,$paymentYear)
	{
		global $companyId;
		
		$sql = "SELECT PH.CURRENCY_ID,
				FC.strCode AS currency,
				PGLH.COST_CENTER,
				COA.CHART_OF_ACCOUNT_NAME,
				MFD.strCostCenterCode,
				DATE(PH.PAY_DATE) AS paymentDate,
				(SELECT CONCAT(RIGHT(YEAR(DATE(dtmStartingDate)),2),'-',RIGHT(YEAR(DATE(dtmClosingDate)),2))
				FROM mst_financeaccountingperiod
				INNER JOIN mst_financeaccountingperiod_companies ON mst_financeaccountingperiod.intId=mst_financeaccountingperiod_companies.intPeriodId
				WHERE dtmStartingDate<=DATE(PH.PAY_DATE) AND
				dtmClosingDate>=DATE(PH.PAY_DATE) AND
				mst_financeaccountingperiod_companies.intCompanyId='$companyId') AS voucherYear,
				LPAD(MONTH(PH.PAY_DATE),2,'0') AS voucherMonth,
				MS.strName AS supplier,
				PH.REMARKS,
				PH.STATUS,
				PH.CREATED_BY
				FROM finance_supplier_advancepayment_header PH
				INNER JOIN finance_supplier_advancepayment_details PGLH ON PH.ADVANCE_PAYMENT_NO=PGLH.ADVANCE_PAYMENT_NO AND 
				PH.ADVANCE_PAYMENT_YEAR=PGLH.ADVANCE_PAYMENT_YEAR
				INNER JOIN mst_financecurrency FC ON PH.CURRENCY_ID=FC.intId
				INNER JOIN finance_mst_chartofaccount COA ON PGLH.ACCOUNT_ID=COA.CHART_OF_ACCOUNT_ID
				INNER JOIN mst_supplier MS ON MS.intId=PH.SUPPLIER_ID
				INNER JOIN mst_financedimension MFD ON MFD.intId=PGLH.COST_CENTER
				WHERE PH.ADVANCE_PAYMENT_NO='$paymentNo' AND
				PH.ADVANCE_PAYMENT_YEAR='$paymentYear' AND
				PH.COMPANY_ID='$companyId' ";
		
		$result 	= $this->db->RunQuery($sql);
		$resultArry = mysqli_fetch_array($result);
		
		return $resultArry;
	}
	private function getBillPayHeaderData($paymentNo,$paymentYear)
	{
		global $companyId;
		
		$sql = "SELECT PH.CURRENCY_ID,
				FC.strCode AS currency,
				PGLH.COST_CENTER,
				MFD.strCostCenterCode,
				COA.CHART_OF_ACCOUNT_NAME,
				DATE(PH.PAY_DATE) AS paymentDate,
				(SELECT CONCAT(RIGHT(YEAR(DATE(dtmStartingDate)),2),'-',RIGHT(YEAR(DATE(dtmClosingDate)),2))
				FROM mst_financeaccountingperiod
				INNER JOIN mst_financeaccountingperiod_companies ON mst_financeaccountingperiod.intId=mst_financeaccountingperiod_companies.intPeriodId
				WHERE dtmStartingDate<=DATE(PH.PAY_DATE) AND
				dtmClosingDate>=DATE(PH.PAY_DATE) AND
				mst_financeaccountingperiod_companies.intCompanyId='$companyId') AS voucherYear,
				LPAD(MONTH(PH.PAY_DATE),2,'0') AS voucherMonth,
				MS.strName AS supplier,
				PH.REMARKS,
				PH.STATUS,
				PH.CREATED_BY
				FROM finance_other_payable_payment_header PH
				INNER JOIN finance_other_payable_payment_gl PGLH ON PH.PAYMENT_NO=PGLH.PAYMENT_NO AND PH.PAYMENT_YEAR=PGLH.PAYMENT_YEAR
				INNER JOIN mst_financecurrency FC ON PH.CURRENCY_ID=FC.intId
				INNER JOIN finance_mst_chartofaccount COA ON PGLH.ACCOUNT_ID=COA.CHART_OF_ACCOUNT_ID
				INNER JOIN mst_supplier MS ON MS.intId=PH.SUPPLIER_ID
				INNER JOIN mst_financedimension MFD ON MFD.intId=PGLH.COST_CENTER
				WHERE PH.PAYMENT_NO='$paymentNo' AND
				PH.PAYMENT_YEAR='$paymentYear' AND
				PH.COMPANY_ID='$companyId' ";
		
		$result 	= $this->db->RunQuery($sql);
		$resultArry = mysqli_fetch_array($result);
	
		return $resultArry;
	}
	private function getPayDetailData($paymentNo,$paymentYear)
	{
		global $companyId;
		
		$sql = "SELECT PIH.INVOICE_NO as invoiceNo,
				PD.AMOUNT as amonut,
				CONCAT(GH.intPoNo,'-',GH.intPoYear) AS PONo,
				CONCAT(GH.intGrnNo,'-',GH.intGrnYear) AS GRNNo
				FROM finance_supplier_payment_details PD
				INNER JOIN finance_supplier_purchaseinvoice_header PIH ON PIH.PURCHASE_INVOICE_NO=PD.PURCHASE_INVOICE_NO AND
				PIH.PURCHASE_INVOICE_YEAR=PD.PURCHASE_INVOICE_YEAR
				INNER JOIN ware_grnheader GH ON GH.strInvoiceNo=PIH.INVOICE_NO
				WHERE PD.PAYMENT_NO='$paymentNo' AND
				PD.PAYMENT_YEAR='$paymentYear' ";
		
		$result	= $this->db->RunQuery($sql);
		
		return $result;
	}
	private function getAdvDetailData($paymentNo,$paymentYear)
	{
		global $companyId;
		
		$sql = "SELECT NULL as invoiceNo,
				PD.AMOUNT as amonut,
				CONCAT(PH.PO_NO,'-',PH.PO_YEAR) AS PONo,
				NULL AS GRNNo
				FROM finance_supplier_advancepayment_details PD
				INNER JOIN finance_supplier_advancepayment_header PH ON PH.ADVANCE_PAYMENT_NO=PD.ADVANCE_PAYMENT_NO AND
				PH.ADVANCE_PAYMENT_YEAR=PD.ADVANCE_PAYMENT_YEAR
				WHERE PD.ADVANCE_PAYMENT_NO='$paymentNo' AND
				PD.ADVANCE_PAYMENT_YEAR='$paymentYear' ";
		
		$result	= $this->db->RunQuery($sql);
		
		return $result;
	}
	private function getBillPayDetailData($paymentNo,$paymentYear)
	{
		global $companyId;
		
		$sql = "SELECT CONCAT(PIH.BILL_INVOICE_NO,'-',PIH.BILL_INVOICE_YEAR) AS invoiceNo,
				PD.AMOUNT AS amonut,
				'' AS PONo,
				'' AS GRNNo
				FROM finance_other_payable_payment_details PD
				INNER JOIN finance_other_payable_bill_header PIH ON PIH.BILL_INVOICE_NO=PD.BILL_INVOICE_NO AND
				PIH.BILL_INVOICE_YEAR=PD.PAYMENT_YEAR
				WHERE PD.PAYMENT_NO='$paymentNo' AND
				PD.PAYMENT_YEAR='$paymentYear' ";
		
		$result	= $this->db->RunQuery($sql);
		
		return $result;
	}
	private function ExchangeRate_sql($currency,$date)
	{
		global $companyId;
		
		$sql = "SELECT dblExcAvgRate
				FROM mst_financeexchangerate
				WHERE intCurrencyId='$currency' AND
				dtmDate='$date' AND
				intCompanyId='$companyId' ";
		
		$result	= $this->db->RunQuery($sql);
		$row 	= mysqli_fetch_array($result);
		
		return $row['dblExcAvgRate'];
	}
	private function PreparedBy_sql($preparedBy)
	{
		$sql = "SELECT strUserName
				FROM sys_users
				WHERE intUserId='$preparedBy' ";
		
		$result	= $this->db->RunQuery($sql);
		$row 	= mysqli_fetch_array($result);
		
		return $row['strUserName'];
	}
}
?>
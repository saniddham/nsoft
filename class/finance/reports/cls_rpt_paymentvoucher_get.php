<?php
$companyId	= $_SESSION["headCompanyId"];

include_once $_SESSION['ROOT_PATH']."class/finance/supplier/supplierPayment/cls_supplier_payment_get.php";
include_once $_SESSION['ROOT_PATH']."class/finance/supplier/advancePayments/cls_supplier_advance_payment_get.php";
include_once $_SESSION['ROOT_PATH']."class/finance/otherPayable/otherBillPayment/cls_payment_get.php";
include_once $_SESSION['ROOT_PATH']."class/finance/bank/bank_payment/cls_bank_payment_get.php";

$obj_supplierPay	= new Cls_Supplier_Payment_Get($db); 
$obj_supplierAdvPay	= new Cls_Supplier_Advance_Payment_Get($db); 
$obj_otherPay		= new Cls_Payment_Get($db); 
$obj_bankPay		= new Cls_bank_payment_Get($db);
//$obj_bankPay		= new Cls_bank_payment_Get($db); 
//ini_set('display_errors',1);

class Cls_Rpt_Payment_Voucher_Get
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
 	public function HeaderData_pdf($paymentNo,$paymentYear,$payType,$executionType)
	{
		
		global $obj_supplierPay;
		global $obj_supplierAdvPay;
		global $obj_otherPay;
		global $obj_bankPay;
		
		switch($payType)
		{
			case 'PAYMENT':
				$resultArry = $obj_supplierPay->getHeaderData_pdf($paymentNo,$paymentYear,$executionType);
			break;
			case 'ADVANCE':
				$resultArry = $obj_supplierAdvPay->getHeaderData_pdf($paymentNo,$paymentYear,$executionType);
			break;
			case 'BILLPAYMENT':
				$resultArry = $obj_otherPay->getHeaderData_pdf($paymentNo,$paymentYear,$executionType);
			break;
			case 'BANK_PAYMENT':
				$resultArry = $obj_bankPay->getHeaderData_pdf($paymentNo,$paymentYear,$executionType);
			break;
		}
		return $resultArry;
	}
	public function DetailData_pdf($paymentNo,$paymentYear,$payType,$executionType)
	{
		global $obj_supplierPay;
		global $obj_supplierAdvPay;
		global $obj_otherPay;
		global $obj_bankPay;
		
		switch($payType)
		{
			case 'PAYMENT':
				$result = $obj_supplierPay->getDetailData_pdf($paymentNo,$paymentYear,$executionType);
			break;
			case 'ADVANCE':
				$result = $obj_supplierAdvPay->getDetailData_pdf($paymentNo,$paymentYear,$executionType);
			break;
			case 'BILLPAYMENT':
				$result = $obj_otherPay->getDetailData_pdf($paymentNo,$paymentYear,$executionType);
			break;
			case 'BANK_PAYMENT':
				$result = $obj_bankPay->getDetailData_pdf($paymentNo,$paymentYear,$executionType);
			break;
		}
		return $result;
	}
 }
?>
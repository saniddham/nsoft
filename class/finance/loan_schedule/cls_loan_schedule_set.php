<?php
include_once  $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
include_once  $_SESSION['ROOT_PATH']."class/cls_commonErrorHandeling_get.php";

$session_userId			= $_SESSION["userId"];
$session_locationId 	= $_SESSION["CompanyID"];
$session_companyId 		= $_SESSION["headCompanyId"];
$savedMasseged			= "";
$error_sql				= "";

$obj_common				= new cls_commonFunctions_get($db);
$obj_errorHandeling 	= new cls_commonErrorHandeling_get($db);

class Cls_Schedule_Set
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	public function save($arrHeader,$arrDetails,$programCode,$programName)
	{
		global $obj_common;
		global $obj_errorHandeling;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_locationId;
		global $session_companyId;
		global $session_userId;
		
		$scheduleNo		= $arrHeader["scheduleNo"];
		$scheduleYear	= $arrHeader["scheduleYear"];
		$date			= $arrHeader["date"];
		$currency		= $arrHeader["currency"];
		$remarks		= $obj_common->replace($arrHeader["remarks"]);
		
		$savedStatus	= true;
		$editMode		= false;
		$this->db->begin();
		
		$this->validateBeforeSave($scheduleNo,$scheduleYear,$programCode,$session_userId);
		if($scheduleNo=='' && $scheduleYear=='')
		{
			$sysNo_arry 	= $obj_common->GetSystemMaxNo('intLoanScheduleNo',$session_locationId);
			if($sysNo_arry["rollBackFlag"]==1)
			{
				if($savedStatus)
				{
					$savedStatus	= false;
					$savedMasseged 	= $sysNo_arry["msg"];
					$error_sql		= $sysNo_arry["q"];
				}
				
			}
			$scheduleNo				= $sysNo_arry["max_no"];
			$scheduleYear			= date('Y');
			$approveLevels 			= $obj_common->getApproveLevels($programName);
			$status					= $approveLevels+1;
			
			$this->saveHeader($scheduleNo,$scheduleYear,$approveLevels,$status,$date,$currency,$remarks);
		}
		else
		{
			$header_array 			= $this->getHeaderData($scheduleNo,$scheduleYear);
			$status					= $header_array['STATUS'];
			$approveLevels 			= $obj_common->getApproveLevels($programName);
			$status					= $approveLevels+1;
			
			$this->updateHeader($scheduleNo,$scheduleYear,$approveLevels,$status,$date,$currency,$remarks);
			$this->updateMaxStatus($scheduleNo,$scheduleYear);
			$this->deleteDetails($scheduleNo,$scheduleYear);
			$editMode				= true;
		}
		$count						= $this->getMaxOrderBy($scheduleNo,$scheduleYear);
		foreach($arrDetails as $array_loop)
		{
			$count++;
			$payDate				= $array_loop["payDate"];
			$amount					= $array_loop["amount"];
			$rowId					= $array_loop["rowId"];
			
			if($rowId=='')
				$rowId				= $count;
				
			$checkDetail			= $this->checkProcessed($scheduleNo,$scheduleYear,$payDate);
			
			if(!$checkDetail)
				$this->saveDetails($scheduleNo,$scheduleYear,$payDate,$amount,$rowId);
		}
		$sysNoArr					= $obj_common->validateDuplicateSerialNoWithSysNo($scheduleNo,'intLoanScheduleNo',$session_locationId);
		if($sysNoArr['type'] == 'fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged	= $sysNoArr['msg'];
		}
		if($savedStatus)
		{
			$this->db->commit();
			$response['type'] 			= "pass";
			if($editMode)
				$response['msg'] 		= "Updated Successfully.";
			else
				$response['msg'] 		= "Saved Successfully.";
			
			$response['scheduleNo']		= $scheduleNo;
			$response['scheduleYear']	= $scheduleYear;
		}
		else
		{
			$this->db->rollback();
			$response['type'] 			= "fail";
			$response['msg'] 			= $savedMasseged;
			$response['sql']			= $error_sql;
		}
		return json_encode($response);
	}
	public function approve($scheduleNo,$scheduleYear)
	{	
		global $obj_common;
		global $obj_errorHandeling;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_locationId;
		global $session_companyId;
		global $session_userId;
		global $finalApproval;
		
		$savedStatus	= true;
		$finalApproval	= false;
		$this->db->begin();
		
		$this->updateHeaderStatus($scheduleNo,$scheduleYear,'');
		$this->approvedData($scheduleNo,$scheduleYear);
		
		if($savedStatus)
		{
			$this->db->commit();
			$response['type'] 			= "pass";
			if($finalApproval)
				$response['msg'] 		= "Final Approval Raised Successfully.";
			else
				$response['msg'] 		= "Approved Successfully.";
		}
		else
		{
			$this->db->rollback();
			$response['type'] 			= "fail";
			$response['msg'] 			= $savedMasseged;
			$response['sql']			= $error_sql;
		}
		return json_encode($response);
	}
	public function reject($scheduleNo,$scheduleYear)
	{
		global $obj_common;
		global $obj_errorHandeling;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_locationId;
		global $session_companyId;
		global $session_userId;
		
		$savedStatus	= true;
		$this->db->begin();
		
		$this->updateHeaderStatus($scheduleNo,$scheduleYear,'0');
		//$this->updateMaxStatus($scheduleNo,$scheduleYear);
		$this->approved_by_insert($scheduleNo,$scheduleYear,$session_userId,0);
		
		if($savedStatus)
		{
			$this->db->commit();
			$response['type'] 		= "pass";
			$response['msg'] 		= "Rejected Successfully.";
		}
		else
		{
			$this->db->rollback();
			$response['type'] 			= "fail";
			$response['msg'] 			= $savedMasseged;
			$response['sql']			= $error_sql;
		}
		return json_encode($response);
	}
	public function cancel($scheduleNo,$scheduleYear)
	{
		global $obj_common;
		global $obj_errorHandeling;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_locationId;
		global $session_companyId;
		global $session_userId;
		
		$savedStatus	= true;
		$this->db->begin();
		
		$this->updateHeaderStatus($scheduleNo,$scheduleYear,'-2');
		//$this->updateMaxStatus($scheduleNo,$scheduleYear);
		$this->approved_by_insert($scheduleNo,$scheduleYear,$session_userId,-2);
		
		if($savedStatus)
		{
			$this->db->commit();
			$response['type'] 		= "pass";
			$response['msg'] 		= "Cancelled Successfully.";
		}
		else
		{
			$this->db->rollback();
			$response['type'] 			= "fail";
			$response['msg'] 			= $savedMasseged;
			$response['sql']			= $error_sql;
		}
		return json_encode($response);
	}
	public function revise($scheduleNo,$scheduleYear)
	{
		global $obj_common;
		global $obj_errorHandeling;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_locationId;
		global $session_companyId;
		global $session_userId;
		
		$savedStatus	= true;
		$this->db->begin();
		
		$this->updateHeaderStatus($scheduleNo,$scheduleYear,'-1');
		//$this->updateMaxStatus($scheduleNo,$scheduleYear);
		$this->approved_by_insert($scheduleNo,$scheduleYear,$session_userId,-1);
		
		if($savedStatus)
		{
			$this->db->commit();
			$response['type'] 		= "pass";
			$response['msg'] 		= "Revised Successfully.";
		}
		else
		{
			$this->db->rollback();
			$response['type'] 			= "fail";
			$response['msg'] 			= $savedMasseged;
			$response['sql']			= $error_sql;
		}
		return json_encode($response);
	}
	public function process($scheduleNo,$scheduleYear,$rowId)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_userId;
		
		$savedStatus	= true;
		$this->db->begin();
		
		$this->validateBeforeProcess($scheduleNo,$scheduleYear,$rowId);
		
		$headerData	= $this->getProcessHeaderData($scheduleNo,$scheduleYear,$rowId);
		
		/*
		BEGIN - NO NEED TO ADD TO TRANSACTION TABLE. ALREADY THEY SEND A BANK PAYMENT JOURNAL ENTRY SETTLEMENT .
		$this->ProcessTransaction($headerData['CREDIT_ACCOUNT_ID'],$headerData['AMOUNT'],$scheduleNo,$scheduleYear,'LOAN_SCHEDULE','C','LS',$headerData['CURRENCY_ID'],$headerData['COMPANY_ID'],$headerData['LOCATION_ID'],$headerData['PAY_DATE'],$session_userId);		
		$this->ProcessTransaction($headerData['DEBIT_ACCOUNT_ID'],$headerData['AMOUNT'],$scheduleNo,$scheduleYear,'LOAN_SCHEDULE','D','LS',$headerData['CURRENCY_ID'],$headerData['COMPANY_ID'],$headerData['LOCATION_ID'],$headerData['PAY_DATE'],$session_userId);
		END   - NO NEED TO ADD TO TRANSACTION TABLE. ALREADY THEY SEND A BANK PAYMENT JOURNAL ENTRY SETTLEMENT .
		*/
		
		$this->updatePrcessDetails($scheduleNo,$scheduleYear,$rowId);
		
		$processedData = $this->getProcessedData($scheduleNo,$scheduleYear,$rowId);
		
		if($savedStatus)
		{
			$this->db->commit();
			$response['type'] 			= "pass";
			$response['msg'] 			= "Processed Successfully.";
			$response['status']			= $processedData['STATUS'];
			$response['processDate']	= $processedData['PROCESS_DATE'];
		}
		else
		{
			$this->db->rollback();
			$response['type'] 			= "fail";
			$response['msg'] 			= $savedMasseged;
			$response['sql']			= $error_sql;
		}
		return json_encode($response);
		
	}
	private function validateBeforeSave($scheduleNo,$scheduleYear,$programCode,$session_userId)
	{
		global $obj_errorHandeling;
		global $savedStatus;
		global $savedMasseged;
		
		$header_arr		= $this->getHeaderData($scheduleNo,$scheduleYear);
		$response 		= $obj_errorHandeling->get_permision_withApproval_save($header_arr['STATUS'],$header_arr['APPROVE_LEVELS'],$session_userId,$programCode,'RunQuery2');
		
		
		if($response["type"]=='fail' && ($savedStatus))
		{
			$savedStatus	= false;
			$savedMasseged 	= $response["msg"];
		}
	}
	private function getHeaderData($scheduleNo,$scheduleYear)
	{
		$sql = "SELECT STATUS,
				APPROVE_LEVELS
				FROM finance_loan_schedule_header
				WHERE SCHEDULE_NO='$scheduleNo' AND
				SCHEDULE_YEAR='$scheduleYear' ";
		
		$result = $this->db->RunQuery2($sql);
		$row 	= mysqli_fetch_array($result);
		
		return $row;
	}
	private function saveHeader($scheduleNo,$scheduleYear,$approveLevels,$status,$date,$currency,$remarks)
	{
		global $session_userId;
		global $session_companyId;
		global $session_locationId;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "INSERT INTO finance_loan_schedule_header 
				(
				SCHEDULE_NO, 
				SCHEDULE_YEAR, 
				SCHEDULE_DATE, 
				CREDIT_ACCOUNT_ID, 
				DEBIT_ACCOUNT_ID, 
				CURRENCY_ID, 
				REMARKS, 
				STATUS, 
				APPROVE_LEVELS, 
				COMPANY_ID, 
				LOCATION_ID, 
				CREATED_BY, 
				CREATED_DATE
				)
				VALUES
				('$scheduleNo', 
				'$scheduleYear', 
				'$date', 
				NULL, 
				NULL, 
				'$currency', 
				'$remarks', 
				'$status', 
				'$approveLevels', 
				'$session_companyId', 
				'$session_locationId', 
				'$session_userId', 
				NOW()
				)";
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function updateHeader($scheduleNo,$scheduleYear,$approveLevels,$status,$date,$currency,$remarks)
	{
		global $session_userId;
		global $session_companyId;
		global $session_locationId;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "UPDATE finance_loan_schedule_header 
				SET
				SCHEDULE_DATE = '$date' , 
				CREDIT_ACCOUNT_ID = NULL , 
				DEBIT_ACCOUNT_ID = NULL , 
				CURRENCY_ID = '$currency' , 
				REMARKS = '$remarks' , 
				STATUS = '$status' , 
				APPROVE_LEVELS = '$approveLevels' , 
				COMPANY_ID = '$session_companyId' , 
				LOCATION_ID = '$session_locationId' , 
				LAST_MODIFY_BY = '$session_userId'	
				WHERE
				SCHEDULE_NO = '$scheduleNo' AND 
				SCHEDULE_YEAR = '$scheduleYear' ";
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function deleteDetails($scheduleNo,$scheduleYear)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "DELETE FROM finance_loan_schedule_details 
				WHERE
				SCHEDULE_NO = '$scheduleNo' AND 
				SCHEDULE_YEAR = '$scheduleYear' AND
				STATUS = '0' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function saveDetails($scheduleNo,$scheduleYear,$payDate,$amount,$rowId)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "INSERT INTO finance_loan_schedule_details 
				(
				SCHEDULE_NO, 
				SCHEDULE_YEAR, 
				PAY_DATE, 
				AMOUNT,
				ORDER_BY
				)
				VALUES
				(
				'$scheduleNo', 
				'$scheduleYear', 
				'$payDate', 
				'$amount',
				'$rowId'
				)";
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function updateHeaderStatus($scheduleNo,$scheduleYear,$status)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		if($status=='') 
			$para='STATUS-1';
 		else
			$para=$status;
		
		$sql = "UPDATE finance_loan_schedule_header 
				SET
				STATUS = ".$para."
				WHERE
				SCHEDULE_NO = '$scheduleNo' AND 
				SCHEDULE_YEAR = '$scheduleYear' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function approvedData($scheduleNo,$scheduleYear)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_userId;
		global $finalApproval;
		
		$header_arr 	= $this->getHeaderData($scheduleNo,$scheduleYear);
		$status			= $header_arr['STATUS'];
		$savedLevels	= $header_arr['APPROVE_LEVELS'];
		
		//if($savedLevels>$status)
			//$this->updateMaxStatus($scheduleNo,$scheduleYear);
		if($status==1)
			$finalApproval = true;	
			
		$approval		= $savedLevels+1-$status;
		$this->approved_by_insert($scheduleNo,$scheduleYear,$session_userId,$approval);
	}
	private function approved_by_insert($scheduleNo,$scheduleYear,$session_userId,$approval)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "INSERT INTO finance_loan_schedule_approveby 
				(
				SCHEDULE_NO, 
				SCHEDULE_YEAR, 
				APPROVE_LEVEL_NO, 
				APPROVED_BY, 
				APPROVED_DATE, 
				STATUS
				)
				VALUES
				(
				'$scheduleNo', 
				'$scheduleYear', 
				'$approval', 
				'$session_userId', 
				NOW(), 
				0
				) ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function updateMaxStatus($scheduleNo,$scheduleYear)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sqlMax = " SELECT (MAX(tb.STATUS)+1) AS maxStatus  
					FROM finance_loan_schedule_approveby AS tb
					WHERE tb.SCHEDULE_NO='$scheduleNo' AND
					tb.SCHEDULE_YEAR='$scheduleYear' ";
		
		$resultMax 	= $this->db->RunQuery2($sqlMax);
		$rowMax		= mysqli_fetch_array($resultMax);
		$maxStatus 	= $rowMax['maxStatus'];
		
		$sqlUpd = " UPDATE finance_loan_schedule_approveby 
					SET
					STATUS = '$maxStatus'
					WHERE
					SCHEDULE_NO = '$scheduleNo' AND 
					SCHEDULE_YEAR = '$scheduleYear' ";
		
		$resultUpd = $this->db->RunQuery2($sqlUpd);
		if(!$resultUpd)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sqlUpd;
			}
		}
	}
	private function checkProcessed($scheduleNo,$scheduleYear,$payDate)
	{
		$checkStatus = false;
		
		$sql = "SELECT STATUS
				FROM finance_loan_schedule_details
				WHERE SCHEDULE_NO='$scheduleNo' AND
				SCHEDULE_YEAR='$scheduleYear' AND
				PAY_DATE='$payDate' ";
		
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		
		if($row['STATUS']==1)
			$checkStatus = true;
		
		return $checkStatus;
	}
	private function getMaxOrderBy($scheduleNo,$scheduleYear)
	{
		$sql = "SELECT 	IFNULL(MAX(ORDER_BY),0) AS orderBy
				FROM 
				finance_loan_schedule_details 
				WHERE
				SCHEDULE_NO = '$scheduleNo' AND
				SCHEDULE_YEAR = '$scheduleYear' ";
		
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row['orderBy'];
	}
	private function validateBeforeProcess($scheduleNo,$scheduleYear,$rowId)
	{
		global $savedStatus;
		global $savedMasseged;
		
		$status	= $this->checkApproved($scheduleNo,$scheduleYear);
		if($status!=1 && ($savedStatus))
		{
			$savedStatus	= false;
			$savedMasseged	= "Fail to process.Schedule not confim.";
		}
		
		$checkProcess	= $this->checkProcess($scheduleNo,$scheduleYear,$rowId);
		if($checkProcess==1 && ($savedStatus))
		{
			$savedStatus	= false;
			$savedMasseged	= "Already processed.";
		}	
	}
	private function checkApproved($scheduleNo,$scheduleYear)
	{
		$sql = "SELECT STATUS 
				FROM finance_loan_schedule_header 
				WHERE SCHEDULE_NO = '$scheduleNo' AND
				SCHEDULE_YEAR = '$scheduleYear' ";
		
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row['STATUS'];
	}
	private function checkProcess($scheduleNo,$scheduleYear,$rowId)
	{
		$sql = "SELECT STATUS
				FROM finance_loan_schedule_details
				WHERE SCHEDULE_NO = '$scheduleNo' AND
				SCHEDULE_YEAR = '$scheduleYear' AND
				ORDER_BY = '$rowId' ";
		
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row['STATUS'];
	}
	private function getProcessHeaderData($scheduleNo,$scheduleYear,$rowId)
	{
		$sql = "SELECT LSH.COMPANY_ID,
				LSH.LOCATION_ID,
				LSH.CREDIT_ACCOUNT_ID,
				LSH.DEBIT_ACCOUNT_ID,
				LSH.CURRENCY_ID,
				LSD.PAY_DATE,
				LSD.AMOUNT
				FROM finance_loan_schedule_details LSD
				INNER JOIN finance_loan_schedule_header LSH ON LSH.SCHEDULE_NO=LSD.SCHEDULE_NO AND 
				LSH.SCHEDULE_YEAR=LSD.SCHEDULE_YEAR
				WHERE LSD.SCHEDULE_NO='$scheduleNo' AND
				LSD.SCHEDULE_YEAR='$scheduleYear' AND
				LSD.ORDER_BY='$rowId' ";
		
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row;
	}
	private function ProcessTransaction($chartOfAccount,$amount,$scheduleNo,$scheduleYear,$docType,$transacType,$transacCat,$currencyID,$companyId,$locationId,$payDate,$session_userId)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "INSERT INTO finance_transaction 
				(
				CHART_OF_ACCOUNT_ID, 
				AMOUNT, 
				DOCUMENT_NO, 
				DOCUMENT_YEAR, 
				DOCUMENT_TYPE, 
				TRANSACTION_TYPE, 
				TRANSACTION_CATEGORY, 
				CURRENCY_ID, 
				LOCATION_ID, 
				COMPANY_ID, 
				LAST_MODIFIED_BY, 
				LAST_MODIFIED_DATE
				)
				VALUES
				(
				'$chartOfAccount', 
				'$amount', 
				'$scheduleNo', 
				'$scheduleYear', 
				'$docType', 
				'$transacType', 
				'$transacCat', 
				'$currencyID', 
				'$locationId', 
				'$companyId', 
				'$session_userId', 
				'$payDate'
				)";
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function updatePrcessDetails($scheduleNo,$scheduleYear,$rowId)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_userId;
		
		$sql = "UPDATE finance_loan_schedule_details 
				SET
				STATUS = '1' , 
				PROCESS_BY = '$session_userId' , 
				PROCESS_DATE = NOW()
				WHERE
				SCHEDULE_NO = '$scheduleNo' AND 
				SCHEDULE_YEAR = '$scheduleYear' AND 
				ORDER_BY = '$rowId' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function getProcessedData($scheduleNo,$scheduleYear,$rowId)
	{
		$sql = "SELECT STATUS, 
				PROCESS_DATE 
				FROM 
				finance_loan_schedule_details 
				WHERE 
				SCHEDULE_NO = '$scheduleNo' AND
				SCHEDULE_YEAR = '$scheduleYear' AND
				ORDER_BY = '$rowId' ";
		
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row;
	}
}
<?php
$session_companyId 	= $_SESSION["headCompanyId"];
$session_userId		= $_SESSION["userId"];

include_once  $_SESSION['ROOT_PATH']."class/cls_commonErrorHandeling_get.php";

$obj_errorHandeling = new cls_commonErrorHandeling_get($db);

class Cls_Schedule_Get
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	public function getRptHeaderData($scheduleNo,$scheduleYear)
	{
		return $this->loadRptHeaderData_sql($scheduleNo,$scheduleYear);
	}
	public function getRptDetailData($scheduleNo,$scheduleYear)
	{
		return $this->loadDetailData_sql($scheduleNo,$scheduleYear);
	}
	public function getRptApproveDetails($scheduleNo,$scheduleYear)
	{
		return $this->getRptApproveDetails_sql($scheduleNo,$scheduleYear);
	}
	public function loadHeaderData($scheduleNo,$scheduleYear)
	{
		return $this->getHeaderData($scheduleNo,$scheduleYear);
	}
	public function loadDetailData($scheduleNo,$scheduleYear)
	{
		return $this->loadDetailData_sql($scheduleNo,$scheduleYear);
	}
	public function validateBeforeApprove($scheduleNo,$scheduleYear,$programCode)
	{
		global $obj_errorHandeling;
		global $session_userId;
		
		$header_arr	= $this->getHeaderData($scheduleNo,$scheduleYear);
		$response 	= $obj_errorHandeling->get_permision_withApproval_confirm($header_arr['STATUS'],$header_arr['APPROVE_LEVELS'],$session_userId,$programCode,'RunQuery');
		
		return $response;
	}
	public function validateBeforeReject($scheduleNo,$scheduleYear,$programCode)
	{
		global $obj_errorHandeling;
		global $session_userId;
		
		$header_arr	= $this->getHeaderData($scheduleNo,$scheduleYear);
		$response 	= $obj_errorHandeling->get_permision_withApproval_reject($header_arr['STATUS'],$header_arr['APPROVE_LEVELS'],$session_userId,$programCode,'RunQuery');
		
		return $response;
	}
	public function validateBeforeCancel($scheduleNo,$scheduleYear,$programCode)
	{
		global $obj_errorHandeling;
		global $session_userId;
		
		$header_arr	= $this->getHeaderData($scheduleNo,$scheduleYear);
		$response 	= $obj_errorHandeling->get_permision_withApproval_cancel($header_arr['STATUS'],$header_arr['APPROVE_LEVELS'],$session_userId,$programCode,'RunQuery');
		
		return $response;
	}
	public function validateBeforeRevise($scheduleNo,$scheduleYear,$programCode)
	{
		global $obj_errorHandeling;
		global $session_userId;
		
		$header_arr	= $this->getHeaderData($scheduleNo,$scheduleYear);
		$response 	= $obj_errorHandeling->get_permision_withApproval_revise($header_arr['STATUS'],$header_arr['APPROVE_LEVELS'],$session_userId,$programCode,'RunQuery');
		
		return $response;
	}
	private function loadRptHeaderData_sql($scheduleNo,$scheduleYear)
	{
		$sql = "SELECT PSH.SCHEDULE_DATE,
				(SELECT CHART_OF_ACCOUNT_NAME
				FROM finance_mst_chartofaccount
				WHERE CHART_OF_ACCOUNT_ID=PSH.CREDIT_ACCOUNT_ID) AS creditAccount,
				(SELECT CHART_OF_ACCOUNT_NAME
				FROM finance_mst_chartofaccount
				WHERE CHART_OF_ACCOUNT_ID=PSH.DEBIT_ACCOUNT_ID) AS debitAccount,
				STATUS,
				APPROVE_LEVELS,
				APPROVE_LEVELS as LEVELS,
				CURRENCY_ID,
				REMARKS,
				FC.strCode,
				SU.strUserName AS CREATOR,
				CREATED_DATE
				FROM finance_loan_schedule_header PSH
				INNER JOIN sys_users SU ON PSH.CREATED_BY = SU.intUserId
				INNER JOIN mst_financecurrency FC ON FC.intId = PSH.CURRENCY_ID
				WHERE PSH.SCHEDULE_NO='$scheduleNo' AND
				PSH.SCHEDULE_YEAR='$scheduleYear' ";
		
		$result = $this->db->RunQuery($sql);
		$row 	= mysqli_fetch_array($result);
		
		return $row;
	}
	private function loadDetailData_sql($scheduleNo,$scheduleYear)
	{
		$sql = "SELECT PAY_DATE,
				AMOUNT,
				STATUS,
				PROCESS_DATE,
				ORDER_BY
				FROM finance_loan_schedule_details
				WHERE SCHEDULE_NO='$scheduleNo' AND
				SCHEDULE_YEAR='$scheduleYear' ";
		
		$result = $this->db->RunQuery($sql);
		
		return $result;
	}
	private function getRptApproveDetails_sql($scheduleNo,$scheduleYear)
	{
		$sql = "SELECT
				PSAB.APPROVED_BY,
				PSAB.APPROVED_DATE as dtApprovedDate,
				SU.strUserName AS UserName,
				PSAB.APPROVE_LEVEL_NO as intApproveLevelNo
				FROM
				finance_loan_schedule_approveby PSAB
				INNER JOIN sys_users SU ON PSAB.APPROVED_BY = SU.intUserId
				WHERE PSAB.SCHEDULE_NO ='$scheduleNo' AND
				PSAB.SCHEDULE_YEAR ='$scheduleYear'      
				ORDER BY PSAB.APPROVED_DATE ASC ";
		
		$result = $this->db->RunQuery($sql);
		
		return $result;
	}
	private function getHeaderData($scheduleNo,$scheduleYear)
	{
		global $session_companyId;
		
		$sql = "SELECT 
				SCHEDULE_DATE,
				CURRENCY_ID, 
				CREDIT_ACCOUNT_ID, 
				DEBIT_ACCOUNT_ID, 
				STATUS, 
				APPROVE_LEVELS,
				REMARKS
				FROM 
				finance_loan_schedule_header 
				WHERE SCHEDULE_NO='$scheduleNo' AND
				SCHEDULE_YEAR='$scheduleYear' AND
				COMPANY_ID = '$session_companyId' ";
		
		$result = $this->db->RunQuery($sql);
		$row 	= mysqli_fetch_array($result);
		
		return $row;
	}
}
?>
<?php
class Cls_Convert_Amount_To_Word
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	
	public function Convert_Amount($number,$currencyCode)
	{ 
		$currencyFraction	= "CENT";
		$value1 = $this->Convert_number($number);
		$convrt = explode(".",round($number,2));
		$cents =  (!isset($convrt[1])?0:$convrt[1]);
		if(strlen($cents)<=1)
			$cents = (!isset($convrt[1])?0:$convrt[1]) . "0";
		$value2	= $this->ConvertDecimals($cents);
		
		return strtoupper($value1." $currencyCode and ".$value2 ." $currencyFraction only.");
	}
	
	private function Convert_number($number) 
	{
		if (($number < 0) || ($number > 999999999)) 
		{ 
			return "$number"; 
		} 
	
		$Gn = floor($number / 1000000);  /* Millions (giga) */ 
		$number -= $Gn * 1000000; 
		$kn = floor($number / 1000);     /* Thousands (kilo) */ 
		$number -= $kn * 1000; 
		$Hn = floor($number / 100);      /* Hundreds (hecto) */ 
		$number -= $Hn * 100; 
		$Dn = floor($number / 10);       /* Tens (deca) */ 
		$n = $number % 10;               /* Ones */ 
		$res = ""; 
	
		if ($Gn) 
		{ 
			$res .= $this->Convert_number($Gn) . " Million"; 
		} 
	
		if ($kn) 
		{ 
			$res .= (empty($res) ? "" : " ") . 
				$this->Convert_number($kn) . " Thousand"; 
		} 
	
		if ($Hn) 
		{ 
			$res .= (empty($res) ? "" : " ") . 
				$this->Convert_number($Hn) . " Hundred"; 
		} 

		$ones = array("", "One", "Two", "Three", "Four", "Five", "Six", 
			"Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", 
			"Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen", 
			"Nineteen"); 
		$tens = array("", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty", 
			"Seventy", "Eighty", "Ninety"); 
	
		if ($Dn || $n) 
		{ 
			if (!empty($res)) 
			{ 
				$res .= " and "; 
			} 
	
			if ($Dn < 2) 
			{ 
				$res .= $ones[$Dn * 10 + $n]; 
			} 
			else 
			{ 
				$res .= $tens[$Dn]; 
	
				if ($n) 
				{ 
					$res .= "-" . $ones[$n]; 
				} 
			} 
		} 
	
		if (empty($res)) 
		{ 
			$res = "zero"; 
		} 
	
		return $res;	
	} 
	
	private function ConvertDecimals($number)
	{		
		  $Dn = floor($number / 10);       /* -10 (deci) */ 
		  $n = $number % 10;               /* .0 */ 
		  
		   $ones = array("", "One", "Two", "Three", "Four", "Five", "Six", 
			"Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", 
			"Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen", 
			"Nineteen"); 
		$tens = array("", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty", 
			"Seventy", "Eighty", "Ninety"); 
			
	 if ($Dn || $n) 
		{ 
			if (!empty($res)) 
			{ 
				$res .= " and "; 
			} 
	
			if ($Dn < 2) 
			{ 
				$res .= $ones[$Dn * 10 + $n]; 
			} 
			else 
			{ 
				$res .= $tens[$Dn]; 
	
				if ($n) 
				{ 
					$res .= "-" . $ones[$n]; 
				} 
			} 
		} 
		
		if (empty($res)) 
		{ 
			$res = "zero"; 
		} 
	
		return $res; 	
	}

}


?>
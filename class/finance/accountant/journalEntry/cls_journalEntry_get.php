<?php
include_once $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
include_once $_SESSION['ROOT_PATH']."class/cls_commonErrorHandeling_get.php";
include_once $_SESSION['ROOT_PATH']."class/finance/cls_get_gldetails.php";
include_once $_SESSION['ROOT_PATH']."class/finance/cls_common_get.php";
//ini_set('display_errors', 1); 
 
$obj_commonErr		= new cls_commonErrorHandeling_get($db);
$obj_common			= new cls_commonFunctions_get($db);
$obj_GLData_get		= new Cls_Get_GLDetails($db);
$obj_common_get		= new Cls_Common_Get($db);
$progrmCode			='P0726';

$session_userId		= $_SESSION["userId"];
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];

class Cls_JournalEntry_Get
{
	private $db;
 	
	function __construct($db)
	{
		$this->db = $db;
 	}

//BEGIN - PUBLIC FUNCTIONS {	
	public function get_balanceToSetele_jernal_amount($serialYear,$serialNo,$orderId,$exectionType)
	{
		
		$sql = $this->Load_balanceToSetele_jernal_sql($serialYear,$serialNo,$orderId);
		$result = $this->db->$exectionType($sql);
		$row = mysqli_fetch_array($result);
		return $row['BALANCE_TO_SETTELE'];
	}
	public function get_settle_jernal_type($serialYear,$serialNo,$orderId,$exectionType)
	{
		
		$sql = $this->Load_balanceToSetele_jernal_sql($serialYear,$serialNo,$orderId);
		$result = $this->db->$exectionType($sql);
		$row = mysqli_fetch_array($result);
		return $row['TRANSACTION_TYPE'];
	}
	public function get_header_array($serialYear,$serialNo)
	{
		
		$sql = $this->Load_header_sql($serialYear,$serialNo);
		$result = $this->db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return $row;
	}
	public function get_header_array2($serialYear,$serialNo)
	{
		
		$sql = $this->Load_header_sql($serialYear,$serialNo);
		$result = $this->db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		return $row;
	}
	public function get_details_array2($serialYear,$serialNo)
	{
 		$sql = $this->Load_details_sql($serialYear,$serialNo);
		$result = $this->db->RunQuery2($sql);
		return $result;
	}
	public function get_report_header_array($serialYear,$serialNo)
	{
 		$sql = $this->Load_report_header_sql($serialYear,$serialNo);
		$result = $this->db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return $row;
	}
	public function get_report_details_array($serialYear,$serialNo)
	{
		$sql = $this->Load_report_details_sql($serialYear,$serialNo);
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	public function get_Report_approval_details_result($serialYear,$serialNo)
	{
		$sql = $this->Load_Report_approval_details_sql($serialYear,$serialNo);
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	public function get_details_result($serialYear,$serialNo)
	{
		$sql = $this->Load_details_sql($serialYear,$serialNo);
		$result = $this->db->RunQuery($sql);
		return $result;
	}
 	
	public function ValidateBeforeSave($serialYear,$serialNo)
	{ 
		global $session_userId;
		global $obj_commonErr;	
		global $progrmCode;
		$this->db->begin();
   		$header_arr			= $this->get_header_array2($serialYear,$serialNo);
 		$permision_arr		= $obj_commonErr->get_permision_withApproval_save($header_arr['STATUS'],$header_arr['LEVELS'],$session_userId,$progrmCode,'RunQuery2');
		if($permision_arr['type'] =='fail'){
			$rollBack			=	1;
			$response['msg'] 	=	$permision_arr['msg'];	
			$response['type'] 	=	'fail';
			return $response;
		}
		$this->db->commit();
	}
	
	public function ValidateBeforeApprove($serialYear,$serialNo)
	{ 
		global $session_userId;
		global $obj_commonErr;	
		global $progrmCode;
		$this->db->begin();
   		$header_arr			= $this->get_header_array2($serialYear,$serialNo);
 		$permision_arr		= $obj_commonErr->get_permision_withApproval_confirm($header_arr['STATUS'],$header_arr['LEVELS'],$session_userId,$progrmCode,'RunQuery2');
		$this->db->commit();
		if($permision_arr['type'] =='fail'){
			$rollBack			=	1;
			$response['msg'] 	=	$permision_arr['msg'];	
			$response['type'] 	=	'fail';
 			return $response;
		}
		
	}

	public function ValidateBeforeReject($serialYear,$serialNo)
	{ 
		global $session_userId;
		global $obj_commonErr;	
		global $progrmCode;
		$this->db->begin();
   		$header_arr			= $this->get_header_array2($serialYear,$serialNo);
 		$permision_arr		= $obj_commonErr->get_permision_withApproval_reject($header_arr['STATUS'],$header_arr['LEVELS'],$session_userId,$progrmCode,'RunQuery2');
		if($permision_arr['type'] =='fail'){
			$rollBack			=	1;
			$response['msg'] 	=	$permision_arr['msg'];	
			$response['type'] 	=	'fail';
 			return $response;
		}
		$this->db->commit();
		
	}
	public function ValidateBeforeCancel($serialYear,$serialNo)
	{ 
		global $session_userId;
		global $obj_commonErr;	
		global $progrmCode;
		$this->db->begin();
   		$header_arr			= $this->get_header_array2($serialYear,$serialNo);
 		$permision_arr		= $obj_commonErr->get_permision_withApproval_cancel($header_arr['STATUS'],$header_arr['LEVELS'],$session_userId,$progrmCode,'RunQuery2');
		if($permision_arr['type'] =='fail'){
			$rollBack			=	1;
			$response['msg'] 	=	$permision_arr['msg'];	
			$response['type'] 	=	'fail';
			return $response; 
 		}
		$this->db->commit();
		
	}
	
 	public function getTotAmmount2($serialYear,$serialNo,$type)
	{
	  	$sql = $this->getTotAmmount_sql($serialYear,$serialNo,$type);
 		$result = $this->db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		return $row['amount'];
 	}
	public function getPopupDetails($currDate,$monthBeforDate)
	{
		return $this->getPopupDetails_sql($currDate,$monthBeforDate);
	}
	public function loadJournalEntry($entryNo,$entryYear)
	{
		return $this->loadJournalEntry_sql($entryNo,$entryYear);
	}
	public function loadSearchData($entryNo,$referenceNo,$currDate,$monthBeforDate)
	{
		return $this->loadSearchData_sql($entryNo,$referenceNo,$currDate,$monthBeforDate);
	}
	
 //END	- PUBLIC FUNCTIONS }

//BEGIN - PRIVATE FUNCTIONS {	
	private function Load_header_sql($serialYear,$serialNo)
	{
		global $session_companyId;
 		
		 $sql = "SELECT
				finance_journal_entry_header.JOURNAL_ENTRY_NO,
				finance_journal_entry_header.JOURNAL_ENTRY_YEAR,
				finance_journal_entry_header.CURRENCY_ID,
				finance_journal_entry_header.REFERENCE_NO,
				finance_journal_entry_header.JOURNAL_ENTRY_DATE,
				finance_journal_entry_header.COMPANY_ID,
				finance_journal_entry_header.LOCATION_ID,
				finance_journal_entry_header.CREATED_BY,
				finance_journal_entry_header.CREATED_DATE,
				finance_journal_entry_header.MODIFIED_BY,
				finance_journal_entry_header.MODIFIED_DATE,
				finance_journal_entry_header.SAVE_LEVELS as LEVELS,
				finance_journal_entry_header.STATUS 
				FROM `finance_journal_entry_header`
				WHERE
				finance_journal_entry_header.JOURNAL_ENTRY_NO = '$serialNo' AND
				finance_journal_entry_header.JOURNAL_ENTRY_YEAR = '$serialYear'";
 		return $sql;
 	}
	
	private function Load_details_sql($serialYear,$serialNo)
	{
		global $session_companyId;
		
		$sql = "SELECT
				finance_journal_entry_details.JOURNAL_ENTRY_YEAR,
				finance_journal_entry_details.CHART_OF_ACCOUNT_ID,
				finance_journal_entry_details.AMOUNT,
				finance_journal_entry_details.TRANSACTION_TYPE,
				finance_journal_entry_details.REMARKS,
				finance_journal_entry_details.COST_CENTER_ID,
				finance_journal_entry_details.JOURNAL_ENTRY_NO,
				finance_journal_entry_details.SETTELEMENT_NO,
				finance_journal_entry_details.SETTELEMENT_YEAR,
				finance_journal_entry_details.SETTELEMENT_ORDER,
				finance_journal_entry_details.ORDER_ID
				FROM `finance_journal_entry_details`
				WHERE
				finance_journal_entry_details.JOURNAL_ENTRY_NO = '$serialNo' AND
				finance_journal_entry_details.JOURNAL_ENTRY_YEAR = '$serialYear' 
				ORDER BY finance_journal_entry_details.`ORDER_ID` ASC";
 		return $sql;
	}
	private function Load_report_header_sql($serialYear,$serialNo)
	{
		global $session_companyId;
 		
		$sql = "SELECT 
				finance_journal_entry_header.JOURNAL_ENTRY_NO				AS SERIAL_NO,
				finance_journal_entry_header.COMPANY_ID						AS COMPANY_ID,
				finance_journal_entry_header.JOURNAL_ENTRY_DATE				AS SERIAL_DATE,
				finance_journal_entry_header.JOURNAL_ENTRY_NO,
				finance_journal_entry_header.JOURNAL_ENTRY_YEAR,
				finance_journal_entry_header.CURRENCY_ID,
				finance_journal_entry_header.REFERENCE_NO,
				finance_journal_entry_header.JOURNAL_ENTRY_DATE,
				finance_journal_entry_header.COMPANY_ID,
				finance_journal_entry_header.LOCATION_ID,
				finance_journal_entry_header.CREATED_BY,
				finance_journal_entry_header.CREATED_DATE,
				finance_journal_entry_header.MODIFIED_BY,
				finance_journal_entry_header.MODIFIED_DATE,
				finance_journal_entry_header.SAVE_LEVELS AS LEVELS,
				finance_journal_entry_header.STATUS ,
				mst_financecurrency.strCode as CURRENCY ,
				mst_locations.strName AS LOCATION,
				mst_companies.strName AS COMPANY,
				sys_users.strUserName AS CREATOR,
				finance_journal_entry_header.CREATED_DATE
				FROM `finance_journal_entry_header`
				INNER JOIN mst_financecurrency ON finance_journal_entry_header.CURRENCY_ID = mst_financecurrency.intId
				INNER JOIN mst_locations ON finance_journal_entry_header.LOCATION_ID = mst_locations.intId
				INNER JOIN mst_companies ON finance_journal_entry_header.COMPANY_ID = mst_companies.intId
				INNER JOIN sys_users ON finance_journal_entry_header.CREATED_BY = sys_users.intUserId
				WHERE
				finance_journal_entry_header.JOURNAL_ENTRY_NO = '$serialNo' AND
				finance_journal_entry_header.JOURNAL_ENTRY_YEAR = '$serialYear'";
 		return $sql;
 	}
	
	private function Load_report_details_sql($serialYear,$serialNo)
	{
		global $session_companyId;
		
	  	$sql = "SELECT
				finance_journal_entry_details.JOURNAL_ENTRY_YEAR,
				finance_journal_entry_details.CHART_OF_ACCOUNT_ID,
				finance_journal_entry_details.AMOUNT,
				finance_journal_entry_details.TRANSACTION_TYPE,
				IF(finance_journal_entry_details.TRANSACTION_TYPE='C','CREDIT','DEBIT') AS TRANSACTION, 
				finance_journal_entry_details.REMARKS,
				finance_journal_entry_details.COST_CENTER_ID,
				finance_journal_entry_details.JOURNAL_ENTRY_NO,
				FCOA.CHART_OF_ACCOUNT_NAME,
				CONCAT(FMT.FINANCE_TYPE_CODE,'',FMMT.MAIN_TYPE_CODE,'',FMST.SUB_TYPE_CODE,'',FCOA.CHART_OF_ACCOUNT_CODE) AS accountCode,
				mst_financedimension.strName as COST_CENTER,
				finance_journal_entry_details.SETTELEMENT_NO,
				finance_journal_entry_details.SETTELEMENT_YEAR,
				finance_journal_entry_details.SETTELEMENT_ORDER,
				finance_journal_entry_details.ORDER_ID
				FROM `finance_journal_entry_details`
				INNER JOIN finance_journal_entry_header ON finance_journal_entry_header.JOURNAL_ENTRY_NO = finance_journal_entry_details.JOURNAL_ENTRY_NO AND finance_journal_entry_header.JOURNAL_ENTRY_YEAR = finance_journal_entry_details.JOURNAL_ENTRY_YEAR
				INNER JOIN finance_mst_chartofaccount as FCOA ON finance_journal_entry_details.CHART_OF_ACCOUNT_ID = FCOA.CHART_OF_ACCOUNT_ID	
				INNER JOIN finance_mst_account_sub_type FMST ON FMST.SUB_TYPE_ID=FCOA.SUB_TYPE_ID
				INNER JOIN finance_mst_account_main_type FMMT ON FMMT.MAIN_TYPE_ID=FMST.MAIN_TYPE_ID
				INNER JOIN finance_mst_account_type FMT ON FMT.FINANCE_TYPE_ID=FMMT.FINANCE_TYPE_ID
				INNER JOIN finance_mst_chartofaccount_company FCOAC ON FCOAC.CHART_OF_ACCOUNT_ID=FCOA.CHART_OF_ACCOUNT_ID AND finance_journal_entry_header.COMPANY_ID = FCOAC.COMPANY_ID
			
				INNER JOIN mst_financedimension ON finance_journal_entry_details.COST_CENTER_ID = mst_financedimension.intId
				WHERE
				finance_journal_entry_details.JOURNAL_ENTRY_NO = '$serialNo' AND
				finance_journal_entry_details.JOURNAL_ENTRY_YEAR = '$serialYear' 
				ORDER BY finance_journal_entry_details.`ORDER_ID` ASC";
 		return $sql;
	}

 	private function getTotAmmount_sql($serialYear,$serialNo,$type)
	{
	  	$sql = "SELECT
				Sum(finance_journal_entry_details.AMOUNT) as amount 
				FROM `finance_journal_entry_details`
				WHERE
				finance_journal_entry_details.JOURNAL_ENTRY_NO = '$serialNo' AND
				finance_journal_entry_details.JOURNAL_ENTRY_YEAR = '$serialYear' AND
				finance_journal_entry_details.TRANSACTION_TYPE = '$type'";
				
 		return $sql;
 	}
	private function Load_balanceToSetele_jernal_sql($serialYear,$serialNo,$orderId)
	{
		global $session_companyId;
		
		  $sql = "SELECT
				finance_journal_entry_details.JOURNAL_ENTRY_NO,
				finance_journal_entry_details.JOURNAL_ENTRY_YEAR,
				finance_journal_entry_details.CHART_OF_ACCOUNT_ID,
				finance_journal_entry_details.TRANSACTION_TYPE,
				finance_journal_entry_details.REMARKS,
				finance_mst_chartofaccount.CHART_OF_ACCOUNT_NAME,
				finance_journal_entry_details.ORDER_ID,
				finance_journal_entry_details.BAL_AMOUNT as BALANCE_TO_SETTELE
 				FROM
				finance_journal_entry_details
				INNER JOIN finance_journal_entry_header ON finance_journal_entry_details.JOURNAL_ENTRY_NO = finance_journal_entry_header.JOURNAL_ENTRY_NO AND finance_journal_entry_details.JOURNAL_ENTRY_YEAR = finance_journal_entry_header.JOURNAL_ENTRY_YEAR
				INNER JOIN finance_mst_chartofaccount ON finance_journal_entry_details.CHART_OF_ACCOUNT_ID = finance_mst_chartofaccount.CHART_OF_ACCOUNT_ID
				WHERE
				finance_journal_entry_header.`STATUS` = 1  AND 
				finance_journal_entry_details.JOURNAL_ENTRY_NO = '$serialNo' AND
				finance_journal_entry_details.JOURNAL_ENTRY_YEAR = '$serialYear' AND
				finance_journal_entry_details.ORDER_ID = '$orderId' AND 
				finance_journal_entry_details.SETTELEMENT_NO IS NULL 
 				ORDER BY
				finance_mst_chartofaccount.CHART_OF_ACCOUNT_NAME ASC";
 		return $sql;
	}
	private function Load_Report_approval_details_sql($serialYear,$serialNo){
		
	   	$sql = "SELECT
				finance_journal_entry_header_approvedby.intApproveUser,
				finance_journal_entry_header_approvedby.dtApprovedDate,
				sys_users.strUserName as UserName,
				finance_journal_entry_header_approvedby.intApproveLevelNo
				FROM
				finance_journal_entry_header_approvedby
				Inner Join sys_users ON finance_journal_entry_header_approvedby.intApproveUser = sys_users.intUserId
				WHERE
				finance_journal_entry_header_approvedby.JOURNAL_ENTRY_NO =  '$serialNo' AND
				finance_journal_entry_header_approvedby.JOURNAL_ENTRY_YEAR =  '$serialYear'      order by dtApprovedDate asc";
				
 		return $sql;
	}
	private function getPopupDetails_sql($currDate,$monthBeforDate)
	{
		global $session_companyId;
		
		$sql = "SELECT JOURNAL_ENTRY_NO,
				JOURNAL_ENTRY_YEAR,
				REFERENCE_NO,
				JOURNAL_ENTRY_DATE
				FROM finance_journal_entry_header
				WHERE STATUS = 1 AND
				COMPANY_ID = '$session_companyId' AND
				JOURNAL_ENTRY_DATE BETWEEN('$monthBeforDate') AND('$currDate')
				ORDER BY JOURNAL_ENTRY_YEAR,JOURNAL_ENTRY_NO ";
		
		$result	= $this->db->RunQuery($sql);
		
		return $result;
	}
	private function loadJournalEntry_sql($entryNo,$entryYear)
	{
		global $obj_common_get;
		global $obj_GLData_get;
		
		$sql = "SELECT CHART_OF_ACCOUNT_ID
				FROM finance_journal_entry_details
				WHERE JOURNAL_ENTRY_NO = '$entryNo' AND
				JOURNAL_ENTRY_YEAR = '$entryYear' ";
		
		$result = $this->db->RunQuery($sql);
		while($row = mysqli_fetch_array($result))
		{
			$data['GLCombo'] 	 		= $obj_GLData_get->getGLCombo('JOURNAL_ENTRY',$row['CHART_OF_ACCOUNT_ID']);
			$data['costCenterCombo'] 	= $obj_common_get->getCostCenterCombo('');
			
			$arrDetailData[] 			= $data;
		}
		return $arrDetailData;
	}
	private function loadSearchData_sql($entryNo,$referenceNo,$currDate,$monthBeforDate)
	{
		global $session_companyId;
		$Wsql = '';
		
		if($entryNo!='')
			$Wsql = "AND JOURNAL_ENTRY_NO LIKE '%$entryNo%' ";
			
		if($referenceNo!='')
			$Wsql = "AND REFERENCE_NO LIKE '%$referenceNo%' ";
			
		$sql = "SELECT JOURNAL_ENTRY_NO,
				JOURNAL_ENTRY_YEAR,
				REFERENCE_NO,
				JOURNAL_ENTRY_DATE
				FROM finance_journal_entry_header
				WHERE STATUS = 1 AND
				COMPANY_ID = '$session_companyId' AND
				JOURNAL_ENTRY_DATE BETWEEN('$monthBeforDate') AND('$currDate')
				$Wsql
				ORDER BY JOURNAL_ENTRY_YEAR,JOURNAL_ENTRY_NO ";
		
		$result = $this->db->RunQuery($sql);
		while($row = mysqli_fetch_array($result))
		{
			$data['entryNo'] 	 		= $row['JOURNAL_ENTRY_NO'];
			$data['entryYear'] 	 		= $row['JOURNAL_ENTRY_YEAR'];
			$data['referenceNo'] 	 	= $row['REFERENCE_NO'];
			$data['entryDate'] 	 		= $row['JOURNAL_ENTRY_DATE'];
			
			$arrDetailData[] 			= $data;
		}
		return $arrDetailData;
	}
	
}

?>
<?php
include_once  ("../../../../class/cls_commonFunctions_get.php");
include_once ("../../../../class/finance/accountant/journalEntry/cls_journalEntry_get.php");
include_once  ("../../../../class/cls_commonErrorHandeling_get.php");
include_once  ("../../../../class/finance/cash_flow/cls_cf_common_set.php");
//ini_set('display_errors', 1); 

$obj_commonErr			= new cls_commonErrorHandeling_get($db);
$obj_common				= new cls_commonFunctions_get($db);
$obj_fin_journal_get	= new Cls_JournalEntry_Get($db);
$obj_cf_common_set		= new cls_cf_common_set($db);

$session_userId			= $_SESSION["userId"];
$session_locationId 	= $_SESSION["CompanyID"];
$session_companyId 		= $_SESSION["headCompanyId"];
$programCode			='P0726';

class Cls_JournalEntry_Set
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	
	public function Save($serialYear,$serialNo,$date,$currency,$refferenceNo,$grid)
	{
		global $obj_commonErr;	
		global $obj_common;
		global $obj_fin_journal_get;
		global $session_userId;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_locationId;
		global $progrmCode;
		
		$savedStatus	= true;
		
		$this->db->begin();
		
		if($serialNo==''){
			$editMode=0;
			$journal_array 	= $obj_common->GetSystemMaxNo('JOURNAL_ENTRY',$session_locationId);
			$serialNo		= $journal_array["max_no"];
			$serialYear		= date('Y');
			$approveLevels 	= $obj_common->getApproveLevels('Journal Entry');
 			$status			=$approveLevels+1;
		}
		else{
			$editMode=1;
			$header_array = $obj_fin_journal_get->get_header_array2($serialYear,$serialNo);
			
 			$status=$header_array['STATUS'];
			$approveLevels 	= $obj_common->getApproveLevels('Journal Entry');
 			$status			=$approveLevels+1;
		}

		if($journal_array['rollBackFlag']==1){
			$messageErr 		= $journal_array['msg']; 
			$sqlErr	= $journal_array['q'];
			$rollBack=1;
 		}

		if($rollBack != 1){
			if($editMode==0){
				$trans_response=$this->SaveHeader($serialYear,$serialNo,$date,$currency,$refferenceNo,$approveLevels,$status);
 			}
			else{
				$trans_response=$this->UpdateHeader($serialYear,$serialNo,$date,$currency,$refferenceNo,$approveLevels,$status);
			}
 			if($trans_response['savedStatus']=='fail'){
				$messageErr	= $trans_response['savedMasseged']; 
				$sqlErr		= $trans_response['error_sql'];
				$rollBack=1;
			}
		}
		
		$maxAppByStatus=$this->getMaxAppByStatus($serialYear,$serialNo);
		$maxAppByStatus=(int)$maxAppByStatus+1;
		$trans_response=$this->approved_by_update($serialYear,$serialNo,$maxAppByStatus);
		if($trans_response['savedStatus']=='fail'){
			$messageErr	= $trans_response['savedMasseged']; 
			$sqlErr		= $trans_response['error_sql'];
			$rollBack=1;
		}
		
		$trans_response	= $this->DeleteDetails($serialYear,$serialNo);
		if($trans_response['savedStatus']=='fail'){
			$messageErr	= $trans_response['savedMasseged']; 
			$sqlErr		= $trans_response['error_sql'];
			$rollBack=1;
		}
		
		if($rollBack != 1){
			$totAmmCredit=0;
			$totAmmDebit=0;
			$i=0;
			foreach($grid as $array_loop)
			{
				$i++;
				
				$account			= $array_loop["account"];
				$credit				= $array_loop["credit"];
				$debit				= $array_loop["debit"];
				$remarks			= $obj_common->replace($array_loop["remarks"]);
				$costCenter			= $array_loop["costCenter"];
				$settelement		= $array_loop["settelement"];
				$settelement		= $obj_common->replace($settelement);
				$settelement 		= explode("/", $settelement);
				$settelementYear	= $settelement[0];  
				$settelementNo		= $settelement[1];  
				$settelementOrder	= $settelement[2]; 
				if($settelementNo==''){
					$settelementNo		="NULL";
					$settelementYear	="NULL";
					$settelementOrder	="NULL";
				}
				
				if($credit > 0){
					$type = 'C';
					$ammount = $credit;
					$totAmmCredit += $ammount;
 				}
				else{
					$type = 'D';
					$ammount = $debit;
					$totAmmDebit += $ammount;
				}
				
				if($settelementNo!="NULL"){
					$totSetteAmount[$settelementNo][$settelementYear][$settelementOrder]+=$ammount;
					$balAmount	= $obj_fin_journal_get->get_balanceToSetele_jernal_amount($settelementYear,$settelementNo,$settelementOrder,'RunQuery2');
 					if(round($totSetteAmount[$settelementNo][$settelementYear][$settelementOrder],4) > round($balAmount,4)){
 						$rollBack=1;
						$messageErr	= "Can't exceed 'Balance to settele amount'"; 
						$sqlErr		= '';
					}
				}
				
				$trans_response=$this->SaveDetails($serialYear,$serialNo,$account,$type,$ammount,$remarks,$costCenter,$i,$settelementNo,$settelementYear,$settelementOrder);
				if($trans_response['savedStatus']=='fail'){
					$messageErr	= $trans_response['savedMasseged']; 
					$sqlErr		= $trans_response['error_sql'];
					$rollBack=1;
				}
			}
			
			
			
			//die($totAmmDebit.'|'.$totAmmCredit);
			if(round($totAmmDebit,2) != round($totAmmCredit,2)){
					$rollBack=1;
					$messageErr	= "Total credit amount not tally with total debit amount"; 
					$sqlErr		= '';
			}
			else if($i==0){
					$rollBack=1;
					$messageErr	= "Error with Detail saving"; 
					$sqlErr		= '';
			}
		}
		$resultArr	= $obj_common->validateDuplicateSerialNoWithSysNo($serialNo,'JOURNAL_ENTRY',$session_locationId);
		if($resultArr['type']=='fail' && $rollBack==0)
		{
			$rollBack		= 1;
			$messageErr		= $resultArr['msg'];
		}

		if($rollBack==1){
			$this->db->rollback();
			$response['type'] 				= "fail";
			$response['msg'] 				= $messageErr;
			$response['sql']				= $sqlErr;
		}
		else if($savedStatus){
			$response['type'] 				= "pass";
			if($editMode==1)
			$response['msg'] 				= "Journal Entry : '$serialNo/$serialYear' updated successfully.";
			else
			$response['msg'] 				= "Journal Entry : '$serialNo/$serialYear' saved successfully.";
			$response_arr					= $obj_commonErr->get_permision_withApproval_confirm($status,$approveLevels,$session_userId,$progrmCode,'RunQuery2');
			$response['permision_confirm'] 	= $response_arr['permision'];
			$response['serialNo'] 			= $serialNo;
			$response['serialYear']			= $serialYear;
			$this->db->commit();
		}else{
			$this->db->rollback();
			$response['type'] 				= "fail";
			$response['msg'] 				= $messageErr;
			$response['sql']				= $sqlErr;
		}
		return json_encode($response);
	}

	public function Approve($serialYear,$serialNo)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $obj_common_get;
		global $session_userId;
		global $obj_fin_journal_get;
		$savedStatus	= true;
		$this->db->begin();
		
 		
		$trans_response = $this->updateHeaderStatus($serialYear,$serialNo,'');
		if($trans_response['savedStatus']=='fail'){
			$messageErr	= $trans_response['savedMasseged']; 
			$sqlErr		= $trans_response['error_sql'];
			$rollBack=1;
		}
		if($rollBack!=1){
			$header_array = $obj_fin_journal_get->get_header_array2($serialYear,$serialNo);
			$status=$header_array['STATUS'];
			$savedLevels=$header_array['LEVELS'];
			$approval=$savedLevels+1-$status;
			
			$trans_response = $this->approved_by_insert($serialYear,$serialNo,$session_userId,$approval);
			if($trans_response['savedStatus']=='fail'){
				$messageErr	= $trans_response['savedMasseged']; 
				$sqlErr		= $trans_response['error_sql'];
				$rollBack=1;
			}
		}
		
		$totAmmCredit	= $obj_fin_journal_get->getTotAmmount2($serialYear,$serialNo,'C');
		$totAmmDebit	= $obj_fin_journal_get->getTotAmmount2($serialYear,$serialNo,'D');
		if($totAmmDebit != $totAmmCredit){
				$rollBack=1;
				$messageErr	= "Total credit amount not tally with total debit amount"; 
				$sqlErr		= '';
		}
		
		$result		= $obj_fin_journal_get->get_details_array2($serialYear,$serialNo);
 		
		while($row = mysqli_fetch_array($result)){
 			$settelementNo		= $row['SETTELEMENT_NO'];
			$settelementYear	= $row['SETTELEMENT_YEAR'];
			$settelementOrder	= $row['SETTELEMENT_ORDER'];
			$ammount			= $row['AMOUNT'];
			if($settelementNo!=NULL){//
  				$totSetteAmount[$settelementNo][$settelementYear][$settelementOrder]=$ammount;
				$balAmount	= $obj_fin_journal_get->get_balanceToSetele_jernal_amount($settelementYear,$settelementNo,$settelementOrder,'RunQuery2');
				if(round($totSetteAmount[$settelementNo][$settelementYear][$settelementOrder],4) > round($balAmount,4)){
					$rollBack=1;
					$messageErr	= "Can't exceed 'Balance to settele amount'"; 
					$sqlErr		= '';
				}
				if($rollBack!=1){
					if($header_array['STATUS']==1){
 						$trans_response = $this->update_datail_balance($settelementNo,$settelementYear,$settelementOrder,$ammount);
						if($trans_response['savedStatus']=='fail'){
							$messageErr	= $trans_response['savedMasseged']; 
							$sqlErr		= $trans_response['error_sql'];
							$rollBack=1;
						}
					}
				}
			} 
		}
 			
		if(($rollBack!=1) && ($status==1)){
			$trans_response = $this->finance_transaction_insert($serialYear,$serialNo);
			if($trans_response['savedStatus']=='fail'){
				$messageErr	= $trans_response['savedMasseged']; 
				$sqlErr		= $trans_response['error_sql'];
				$rollBack=1;
			}
		}
	//2014-05-26
		if(($rollBack!=1) && ($status==1)){
			$trans_response = $this->finance_cash_flow_log_insert($serialYear,$serialNo);
			if($trans_response['savedStatus']=='fail'){
				$messageErr	= $trans_response['savedMasseged']; 
				$sqlErr		= $trans_response['error_sql'];
				$rollBack	= 1;
			}
		}
			
		//	$rollBack=1;
		if($rollBack==1){
			$this->db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= $messageErr;
			$response['sql']		= $sqlErr;
		}
		else if($savedStatus){
			$this->db->commit();
			$response['type'] 		= "pass";
			$header_array = $obj_fin_journal_get->get_header_array2($serialYear,$serialNo);
			if($header_array['STATUS']==1) 
			$response['msg'] 		= "Final Approval Raised successfully.";
 			else 
			$response['msg'] 		= "Approved successfully.";
 		}else{
			$this->db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= $messageErr;
			$response['sql']		= $sqlErr;
		}
		return json_encode($response);
	}

	public function Reject($serialYear,$serialNo)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $obj_common_get;
		global $session_userId;
		global $obj_fin_journal_get;
		$savedStatus	= true;
		$this->db->begin();
		
 		
		$trans_response = $this->updateHeaderStatus($serialYear,$serialNo,'0');
		if($trans_response['savedStatus']=='fail'){
			$messageErr	= $trans_response['savedMasseged']; 
			$sqlErr		= $trans_response['error_sql'];
			$rollBack=1;
		}
		if($rollBack!=1){
			$trans_response = $this->approved_by_insert($serialYear,$serialNo,$session_userId,0);
			if($trans_response['savedStatus']=='fail'){
				$messageErr	= $trans_response['savedMasseged']; 
				$sqlErr		= $trans_response['error_sql'];
				$rollBack=1;
			}
		}
   		
		if($rollBack==1){
			$this->db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= $messageErr;
			$response['sql']		= $sqlErr;
		}
		else if($savedStatus){
			$this->db->commit();
			$response['type'] 		= "pass";
 			$response['msg'] 		= "Rejected successfully.";
 		}else{
			$this->db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= $messageErr;
			$response['sql']		= $sqlErr;
		}
		return json_encode($response);
	}

	public function Cancel($serialYear,$serialNo)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $obj_common_get;
		global $session_userId;
		global $obj_fin_journal_get;
		global $obj_cf_common_set;
		
		$savedStatus	= true;
		$this->db->begin();
		
 		
		$trans_response = $this->updateHeaderStatus($serialYear,$serialNo,'-2');
		if($trans_response['savedStatus']=='fail'){
			$messageErr	= $trans_response['savedMasseged']; 
			$sqlErr		= $trans_response['error_sql'];
			$rollBack=1;
		}
		if($rollBack!=1){
			
			$result		= $obj_fin_journal_get->get_details_array2($serialYear,$serialNo);
 			while($row	= mysqli_fetch_array($result)){
				$settelementNo		= $row['SETTELEMENT_NO'];
				$settelementYear	= $row['SETTELEMENT_YEAR'];
				$settelementOrder	= $row['SETTELEMENT_ORDER'];
				$ammount			= $row['AMOUNT'];
				$trans_response = $this->update_datail_balance($settelementNo,$settelementYear,$settelementOrder,-$ammount);
				if($trans_response['savedStatus']=='fail'){
					$messageErr	= $trans_response['savedMasseged']; 
					$sqlErr		= $trans_response['error_sql'];
					$rollBack=1;
				}
			}
 		}
 		if($rollBack!=1){
			$trans_response = $this->approved_by_insert($serialYear,$serialNo,$session_userId,-2);
			if($trans_response['savedStatus']=='fail'){
				$messageErr	= $trans_response['savedMasseged']; 
				$sqlErr		= $trans_response['error_sql'];
				$rollBack=1;
			}
		}
		if($rollBack!=1){
			$trans_response = $this->finance_transaction_delete($serialYear,$serialNo);
			if($trans_response['savedStatus']=='fail'){
				$messageErr	= $trans_response['savedMasseged']; 
				$sqlErr		= $trans_response['error_sql'];
				$rollBack=1;
			}
		}
		if($rollBack!=1){
			$log_response = $obj_cf_common_set->delete_from_log($serialNo,$serialYear,'JOURNAL_ENTRY','RunQuery2');
			if($log_response['type']=='fail'){
				$messageErr	= $log_response['msg']; 
				$sqlErr		= $log_response['sql'];
				$rollBack=1;
			}
		}
   		
		if($rollBack==1){
			$this->db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= $messageErr;
			$response['sql']		= $sqlErr;
		}
		else if($savedStatus){
			$this->db->commit();
			$response['type'] 		= "pass";
 			$response['msg'] 		= "Cancelled successfully.";
 		}else{
			$this->db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= $messageErr;
			$response['sql']		= $sqlErr;
		}
		return json_encode($response);
	}
 	private function SaveHeader($serialYear,$serialNo,$date,$currency,$refferenceNo,$approveLevels,$status)
	{
		global $session_userId;
		global $session_locationId;
		global $session_companyId;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "INSERT INTO finance_journal_entry_header 
				(JOURNAL_ENTRY_NO, 
				JOURNAL_ENTRY_YEAR,
				CURRENCY_ID,
 				REFERENCE_NO, 
 				JOURNAL_ENTRY_DATE, 
 				COMPANY_ID, 
 				LOCATION_ID, 
 				STATUS, 
 				SAVE_LEVELS, 
				CREATED_BY,
				CREATED_DATE)
				VALUES
				('$serialNo', 
				'$serialYear',
				'$currency',
 				'$refferenceNo',
 				'$date',
 				'$session_companyId',
 				'$session_locationId',
				'$status',
 				'$approveLevels',
 				'$session_userId', 
				NOW());";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$data['savedStatus']	= 'fail';
			$data['savedMasseged']	= $this->db->errormsg;
			$data['error_sql']			= $sql;
		}
		return $data;
	}
	
	private function UpdateHeader($serialYear,$serialNo,$date,$currency,$refferenceNo,$approveLevels,$status)
	{
		global $session_userId;
		global $session_locationId;
		global $session_companyId;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "UPDATE finance_journal_entry_header 
				SET 
 				CURRENCY_ID='$currency',
 				REFERENCE_NO='$refferenceNo', 
 				JOURNAL_ENTRY_DATE='$date', 
 				COMPANY_ID='$session_companyId', 
 				LOCATION_ID='$session_locationId', 
  				SAVE_LEVELS='$approveLevels',
				STATUS='$status',
				MODIFIED_BY='$session_userId',
				MODIFIED_DATE=NOW()
				WHERE
				finance_journal_entry_header.JOURNAL_ENTRY_NO = '$serialNo' AND
				finance_journal_entry_header.JOURNAL_ENTRY_YEAR = '$serialYear'";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$data['savedStatus']	= 'fail';
			$data['savedMasseged']	= $this->db->errormsg;
			$data['error_sql']			= $sql;
		}
		return $data;
	}
	
	private function DeleteDetails($serialYear,$serialNo)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql	= "DELETE 
					FROM finance_journal_entry_details 
					WHERE
					finance_journal_entry_details.JOURNAL_ENTRY_NO = '$serialNo' AND
					finance_journal_entry_details.JOURNAL_ENTRY_YEAR = '$serialYear'";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$data['savedStatus']	= 'fail';
			$data['savedMasseged']	= $this->db->errormsg;
			$data['error_sql']			= $sql;
		}
		return $data;
	}
	
	private function SaveDetails($serialYear,$serialNo,$account,$type,$ammount,$remarks,$costCenter,$order,$settelementNo,$settelementYear,$settelementOrder)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql	= "INSERT INTO finance_journal_entry_details 
					(JOURNAL_ENTRY_NO, 
					JOURNAL_ENTRY_YEAR, 
					CHART_OF_ACCOUNT_ID,
					AMOUNT,
					BAL_AMOUNT,
					TRANSACTION_TYPE,
					REMARKS,
					COST_CENTER_ID,
					ORDER_ID,
					SETTELEMENT_NO,
					SETTELEMENT_YEAR,
					SETTELEMENT_ORDER)
					VALUES
					('$serialNo', 
					'$serialYear', 
					'$account',
					'$ammount',
					'$ammount',
					'$type',
					'$remarks',
					'$costCenter',
					'$order',
					$settelementNo,
					$settelementYear,
					$settelementOrder);";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$data['savedStatus']	= 'fail';
			$data['savedMasseged']	= $this->db->errormsg;
			$data['error_sql']			= $sql;
		}
		return $data;
	}
	
	function updateHeaderStatus($serialYear,$serialNo,$status){
		global $session_userId;
		global $session_locationId;
		global $session_companyId;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
 		
		if($status=='') 
			$para='STATUS-1';
 		else
			$para=$status;
		
		$sql = "UPDATE finance_journal_entry_header 
				SET 
				STATUS=".$para." 
				WHERE
				finance_journal_entry_header.JOURNAL_ENTRY_NO = '$serialNo' AND
				finance_journal_entry_header.JOURNAL_ENTRY_YEAR = '$serialYear'";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$data['savedStatus']	= 'fail';
			$data['savedMasseged']	= $this->db->errormsg;
			$data['error_sql']			= $sql;
		}
		return $data;
	}

	private function approved_by_insert($serialYear,$serialNo,$userId,$approval){
		global $session_userId;
		global $session_locationId;
		global $session_companyId;
		global $savedStatus;
		global $error_sql;
		global $savedMasseged;
		
		$sql = "INSERT INTO `finance_journal_entry_header_approvedby` (`JOURNAL_ENTRY_NO`,`JOURNAL_ENTRY_YEAR`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
			VALUES ('$serialNo','$serialYear','$approval','$userId',now(),0)";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$data['savedStatus']	= 'fail';
			$data['savedMasseged']	= $this->db->errormsg;
			$data['error_sql']			= $sql;
		}
		return $data;
	}
	
	private function getMaxAppByStatus($serialYear,$serialNo){
 		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
	   $sql = "SELECT
				max(finance_journal_entry_header_approvedby.intStatus) as status 
				FROM
				finance_journal_entry_header_approvedby
				WHERE
				finance_journal_entry_header_approvedby.JOURNAL_ENTRY_NO =  '$serialNo' AND
				finance_journal_entry_header_approvedby.JOURNAL_ENTRY_YEAR =  '$serialYear'";
				
		$resultm = $this->db->RunQuery2($sql);
		 $rowm=mysqli_fetch_array($resultm);
		 
 			 return $rowm['status'];
 	}
	
	private function approved_by_update($serialYear,$serialNo,$maxAppByStatus){
 		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
			$sql = "UPDATE `finance_journal_entry_header_approvedby` SET intStatus ='$maxAppByStatus' 
					WHERE (`JOURNAL_ENTRY_NO`='$serialNo') AND (`JOURNAL_ENTRY_YEAR`='$serialYear') AND (`intStatus`='0')";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$data['savedStatus']	= 'fail';
			$data['savedMasseged']	= $this->db->errormsg;
			$data['error_sql']			= $sql;
		}
		return $data;
	}

	private function update_datail_balance($serialNo,$serialYear,$orderId,$ammount){
 		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "UPDATE `finance_journal_entry_details` SET BAL_AMOUNT =BAL_AMOUNT - '$ammount' 
					WHERE (`JOURNAL_ENTRY_NO`='$serialNo') AND (`JOURNAL_ENTRY_YEAR`='$serialYear') AND (`ORDER_ID`='$orderId')";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$data['savedStatus']	= 'fail';
			$data['savedMasseged']	= $this->db->errormsg;
			$data['error_sql']			= $sql;
		}
		return $data;
	}
	
	private function finance_transaction_insert($serialYear,$serialNo){
		global $session_userId;
		global $session_locationId;
		global $session_companyId;
		global $savedStatus;
		global $error_sql;
		global $savedMasseged;
		global $obj_fin_comfunc_get;
		
		$sql1 = "SELECT
					finance_journal_entry_details.JOURNAL_ENTRY_NO,
					finance_journal_entry_details.JOURNAL_ENTRY_YEAR,
					finance_journal_entry_details.CHART_OF_ACCOUNT_ID,
					finance_journal_entry_details.AMOUNT,
					finance_journal_entry_details.TRANSACTION_TYPE,
					finance_journal_entry_details.REMARKS,
					finance_journal_entry_details.COST_CENTER_ID,
					finance_journal_entry_header.REFERENCE_NO,
					finance_journal_entry_header.CURRENCY_ID,
					finance_journal_entry_header.JOURNAL_ENTRY_DATE
					FROM
					finance_journal_entry_details
					INNER JOIN finance_journal_entry_header ON finance_journal_entry_details.JOURNAL_ENTRY_NO = finance_journal_entry_header.JOURNAL_ENTRY_NO AND finance_journal_entry_details.JOURNAL_ENTRY_YEAR = finance_journal_entry_header.JOURNAL_ENTRY_YEAR
					WHERE
					finance_journal_entry_details.JOURNAL_ENTRY_NO = '$serialNo' AND
					finance_journal_entry_details.JOURNAL_ENTRY_YEAR = '$serialYear'";
		$result1 = $this->db->RunQuery2($sql1);		
		while($row=mysqli_fetch_array($result1))
		{
			$account		= $row['CHART_OF_ACCOUNT_ID'];
			$ammount		= $row['AMOUNT'];
 			$transType		= $row['TRANSACTION_TYPE'];
 			$transCat		= 'JE';
			$docType		= 'JOURNAL_ENTRY';
 			$refNo			= $obj_fin_comfunc_get->replace($row["REFERENCE_NO"]);
			$curr			= $row['CURRENCY_ID'];
			$entryDate		= $row["JOURNAL_ENTRY_DATE"];
			$REMARKS		= $obj_fin_comfunc_get->replace($row["REMARKS"]);
 			
			$sqlI = "INSERT INTO `finance_transaction` 
						(`CHART_OF_ACCOUNT_ID`,
						`AMOUNT`,
						`DOCUMENT_NO`,
						DOCUMENT_YEAR,
						DOCUMENT_TYPE,
						TRANSACTION_TYPE,
						TRANSACTION_CATEGORY,
						BANK_REFERENCE_NO,
						CURRENCY_ID,
						LOCATION_ID,
						COMPANY_ID,
						LAST_MODIFIED_BY,
						LAST_MODIFIED_DATE,
						REMARKS) 
					VALUES ('$account',
						'$ammount',
						'$serialNo',
						'$serialYear',
						'$docType',
						'$transType',
						'$transCat',
						'$refNo',
						'$curr',
						'$session_locationId',
						'$session_companyId',
						'$session_userId',
						'$entryDate',
						'$REMARKS'
						)";
			$result = $this->db->RunQuery2($sqlI);
			if(!$result){
				$data['savedStatus']		= 'fail';
				$data['savedMasseged']		= $this->db->errormsg;
				$data['error_sql']			= $sqlI;
			}
		}
		return $data;
	}
	private function finance_transaction_delete($serialYear,$serialNo){
		global $savedStatus;
		global $error_sql;
		global $savedMasseged;
	
		 $sql1 = "DELETE 
					FROM
					finance_transaction
 					WHERE
					finance_transaction.DOCUMENT_NO = '$serialNo' AND
					finance_transaction.DOCUMENT_YEAR = '$serialYear' AND 
					finance_transaction.DOCUMENT_TYPE = 'JOURNAL_ENTRY'";
		$result1 = $this->db->RunQuery2($sql1);
		
			if(!$result1){
				$data['savedStatus']	= 'fail';
				$data['savedMasseged']	= $this->db->errormsg;
				$data['error_sql']			= $sql1;
			}
		return $data;
	}
	
	private function finance_cash_flow_log_insert($serialYear,$serialNo){
		global $session_userId;
		global $session_locationId;
		global $session_companyId;
		global $savedStatus;
		global $error_sql;
		global $savedMasseged;
		$i		= 0;
		
		$sql1 = "SELECT 
					finance_journal_entry_header.COMPANY_ID,
					finance_journal_entry_header.LOCATION_ID, 
					finance_cashflow_cpanal.REPORT_FIELD_ID, 
					finance_journal_entry_header.CURRENCY_ID, 
					finance_journal_entry_details.CHART_OF_ACCOUNT_ID ,  
					finance_journal_entry_details.AMOUNT, 
					finance_journal_entry_header.COMPANY_ID, 
					finance_journal_entry_header.JOURNAL_ENTRY_DATE /*  ,  
					IFNULL(sum(finance_journal_entry_details.AMOUNT*convert_frm.dblBuying/convert_to.dblBuying),0) as AMOUNT1    */
					
					FROM
					finance_journal_entry_header
					INNER JOIN finance_journal_entry_details ON finance_journal_entry_header.JOURNAL_ENTRY_NO = finance_journal_entry_details.JOURNAL_ENTRY_NO AND finance_journal_entry_header.JOURNAL_ENTRY_YEAR = finance_journal_entry_details.JOURNAL_ENTRY_YEAR
					INNER JOIN finance_cashflow_cpanal ON FIND_IN_SET(finance_journal_entry_details.CHART_OF_ACCOUNT_ID,finance_cashflow_cpanal.GL_ID)
					INNER JOIN mst_companies ON finance_journal_entry_header.COMPANY_ID = mst_companies.intId
					/*LEFT JOIN mst_financeexchangerate as convert_frm ON mst_companies.intBaseCurrencyId = convert_frm.intCurrencyId  
					AND DATE(finance_journal_entry_header.JOURNAL_ENTRY_DATE) = convert_frm.dtmDate AND 
					finance_journal_entry_header.COMPANY_ID = convert_frm.intCompanyId 
					LEFT JOIN mst_financeexchangerate as convert_to ON mst_companies.intBaseCurrencyId = convert_to.intCurrencyId  
					AND DATE(finance_journal_entry_header.JOURNAL_ENTRY_DATE) = convert_to.dtmDate AND 
					finance_journal_entry_header.COMPANY_ID = convert_to.intCompanyId */
					WHERE
					finance_cashflow_cpanal.BOO_GL_ITEM = 1 AND 
					finance_journal_entry_details.JOURNAL_ENTRY_NO = '$serialNo' AND
					finance_journal_entry_details.JOURNAL_ENTRY_YEAR = '$serialYear'";
		$result1 = $this->db->RunQuery2($sql1);		
		while($row=mysqli_fetch_array($result1))
		{
			$i++;
			$account		= $row['CHART_OF_ACCOUNT_ID'];
			$ammount		= $row['AMOUNT'];
			$docType		= 'JOURNAL_ENTRY';
 			$company		= $row['COMPANY_ID'];
 			$location		= $row['LOCATION_ID'];
			$rpt_field		= $row["REPORT_FIELD_ID"];
			$entryDate		= $row["JOURNAL_ENTRY_DATE"];
			$gl_id			= $row["CHART_OF_ACCOUNT_ID"];
			$currency		= $row["CURRENCY_ID"];
 		
			//******************
				 	$sqlL = "
							SELECT
							finance_cashflow_log.AMOUNT, 
							finance_cashflow_log.LOCATION_ID,
							finance_cashflow_log.REPORT_FIELD_ID,
							finance_cashflow_log.RECEIVE_DATE,
							finance_cashflow_log.LOG_DATE AS DATE,
							finance_cashflow_log.CURRENCY_ID,
							/*SUM(IF(DOCUMENT_TYPE='OPENING_FIXED' OR DOCUMENT_TYPE='OPENING_PREVIOUS' ,finance_cashflow_log.AMOUNT,
							IF(DOCUMENT_TYPE='OPENING_FIXED_REVISE' OR DOCUMENT_TYPE='JOURNAL_ENTRY' OR DOCUMENT_TYPE='INVOICE' OR DOCUMENT_TYPE='BILLINVOICE', finance_cashflow_log.AMOUNT*-1,0))) as BAL */

							IFNULL(SUM((IF(DOCUMENT_TYPE='OPENING_FIXED' OR DOCUMENT_TYPE='OPENING_PREVIOUS' ,finance_cashflow_log.AMOUNT,
							IF(DOCUMENT_TYPE='OPENING_FIXED_REVISE' OR DOCUMENT_TYPE='JOURNAL_ENTRY' OR DOCUMENT_TYPE='INVOICE' OR DOCUMENT_TYPE='BILLINVOICE', finance_cashflow_log.AMOUNT*-1,0)))*convert_frm.dblBuying/convert_to.dblBuying),0) as BAL    
							
							FROM `finance_cashflow_log` 
							INNER JOIN mst_locations ON finance_cashflow_log.LOCATION_ID = mst_locations.intId
							LEFT JOIN mst_financeexchangerate as convert_frm ON finance_cashflow_log.CURRENCY_ID = convert_frm.intCurrencyId  
							AND DATE(finance_cashflow_log.CREATE_DATE) = convert_frm.dtmDate AND 
							mst_locations.intCompanyId = convert_frm.intCompanyId 
							LEFT JOIN mst_financeexchangerate as convert_to ON $currency = convert_to.intCurrencyId  
							AND DATE('$entryDate') = convert_to.dtmDate AND 
							$company = convert_to.intCompanyId 
							WHERE
							finance_cashflow_log.LOCATION_ID = '$location' AND
							finance_cashflow_log.REPORT_FIELD_ID = '$rpt_field'
							GROUP BY
							finance_cashflow_log.LOCATION_ID,
							finance_cashflow_log.REPORT_FIELD_ID,
							finance_cashflow_log.RECEIVE_DATE,
							finance_cashflow_log.CURRENCY_ID
							ORDER BY
							finance_cashflow_log.RECEIVE_DATE ASC";
			$resultL	= $this->db->RunQuery2($sqlL);
			$tot_to_save=$ammount;		
			while($rowL=mysqli_fetch_array($resultL))
			{
				$stockB = round($rowL['BAL'],2);
				if(($tot_to_save>0) && ($stockB>0))
				{
					if($tot_to_save<=$stockB)
					{
						$saveAmnt 	 = $tot_to_save;
						$tot_to_save = 0;
					}
					else if($tot_to_save>$stockB)
					{
						$saveAmnt 		= $stockB;
						$tot_to_save	= $tot_to_save-$saveAmnt;
					}
					$rcv_date	 = $rowL['RECEIVE_DATE'];
					//$currency	 = $rowL['CURRENCY_ID'];
					//******************
					$sqlI = "INSERT INTO `finance_cashflow_log` 
								(`LOCATION_ID`,
								`REPORT_FIELD_ID`,
								`RECEIVE_DATE`,
								INVOICE_DATE, 
								LOG_DATE,
								DOCUMENT_NO,
								DOCUMENT_YEAR,
								DOCUMENT_TYPE,
								AMOUNT,
								CURRENCY_ID,
								CREATE_DATE) 
							VALUES ('$location',
								'$rpt_field',
								'$rcv_date',
								'$entryDate',
								'$entryDate',
								'$serialNo',
								'$serialYear',
								'$docType',
								'$saveAmnt',
								'$currency',
								now())";
					$result = $this->db->RunQuery2($sqlI);
					if(!$result){
						$data['savedStatus']		= 'fail';
						$data['savedMasseged']		= $this->db->errormsg;
						$data['error_sql']			= $sqlI;
					}
					//******************8
				}
			}//end of while
			if($tot_to_save > 0){
				if($rcv_date == ''){
					$rcv_date =$entryDate;
				}
				 	$sqlI = "INSERT INTO `finance_cashflow_log` 
								(`LOCATION_ID`,
								`REPORT_FIELD_ID`,
								`RECEIVE_DATE`,
								LOG_DATE,
								DOCUMENT_NO,
								DOCUMENT_YEAR,
								DOCUMENT_TYPE,
								AMOUNT,
								CURRENCY_ID) 
							VALUES ('$location',
								'$rpt_field',
								'$rcv_date',
								'$entryDate',
								'$serialNo',
								'$serialYear',
								'$docType',
								'$tot_to_save',
								'$currency')";
					$result = $this->db->RunQuery2($sqlI);
					if(!$result){
						$data['savedStatus']		= 'fail';
						$data['savedMasseged']		= $this->db->errormsg;
						$data['error_sql']			= $sqlI;
					}
			}
			
			//******************8
		}//end of while
		if($i==0){
			$data	= NULL;
		}
		return $data;
	}


 }
?>
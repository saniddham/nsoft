<?php
class cls_petty_cash_requisition_set{
 
	private $db;
 	
	function __construct($db)
	{
		$this->db = $db;	
	}
	
//BEGIN - PUBLIC FUNCTIONS {
	public function saveHeader($requisitionNo,$requisitionYear,$total,$remark,$date,$status,$approveLevels,$companyID,$locationID,$userID,$executeType)
	{
		return $this->saveHeader_sql($requisitionNo,$requisitionYear,$total,$remark,$date,$status,$approveLevels,$companyID,$locationID,$userID,$executeType);	
	}
	public function saveDetails($requisitionNo,$requisitionYear,$invoiceNo,$invoiceYear,$executeType)
	{
		return $this->saveDetails_sql($requisitionNo,$requisitionYear,$invoiceNo,$invoiceYear,$executeType);
	}
	public function deleteDetails($requisitionNo,$requisitionYear,$executeType)
	{
		return $this->deleteDetails_sql($requisitionNo,$requisitionYear,$executeType);	
	}
	public function updateHeader($requisitionNo,$requisitionYear,$total,$remark,$userID,$status,$approveLevels,$executeType)
	{
		return $this->updateHeader_result($requisitionNo,$requisitionYear,$total,$remark,$userID,$status,$approveLevels,$executeType);	
	}
	public function update_header_status($requisitionNo,$requisitionYear,$status,$executeType)
	{ 

   		$sql	= $this->update_header_status_sql($requisitionNo,$requisitionYear,$status);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
			return $response;
 	}

	public function approved_by_insert($requisitionNo,$requisitionYear,$userId,$approval,$executeType)
	{ 

   		$sql	= $this->approved_by_insert_sql($requisitionNo,$requisitionYear,$userId,$approval);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
			return $response;
 	}
	public function updateInvoice($invoiceNo,$invoiceYear,$location,$status,$executeType)
	{
		return $this->updateInvoice_result($invoiceNo,$invoiceYear,$location,$status,$executeType);
	}
	public function updateApproveByStatus($requisitionNo,$requisitionYear,$maxStatus)
	{
		return $this->updateApproveByStatus_sql($requisitionNo,$requisitionYear,$maxStatus);
	}
	
//END 	- PUBLIC FUNCTIONS }

//BEGIN - PRIVATE FUNCTIONS {
	private function saveHeader_sql($requisitionNo,$requisitionYear,$total,$remark,$date,$status,$approveLevels,$companyID,$locationID,$userID,$executeType)
	{
		$sql	= " INSERT INTO finance_pettycash_requisition_header 
					(REQUISITION_NO, 
					REQUISITION_YEAR, 
					TOTAL_AMOUNT, 
					REMARKS,
					REQUEST_DATE, 
					STATUS, 
					APPROVE_LEVELS, 
					COMPANY_ID, 
					LOCATION_ID, 
					CREATED_BY, 
					CREATED_DATE
					)
					VALUES
					('$requisitionNo', 
					'$requisitionYear', 
					'$total', 
					'$remark',
					'$date', 
					'$status', 
					'$approveLevels', 
					'$companyID', 
					'$locationID', 
					'$userID', 
					NOW()
					)";
					//die($sql);
		$result	= $this->db->$executeType($sql);	
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql; 	
		}
		return $data;
	}
	private function updateInvoice_result($invoiceNo,$invoiceYear,$location,$status,$executeType)
	{
		$sql	= " UPDATE finance_pettycash_invoice_header 
					SET
					REQUEST_STATUS = '$status'
					
					WHERE
					PETTY_INVOICE_NO = '$invoiceNo' AND PETTY_INVOICE_YEAR = '$invoiceYear' AND LOCATION_ID = '$location'";	
					
		$result	= $this->db->$executeType($sql);	
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql; 	
		}
		return $data;
	}
	private function saveDetails_sql($requisitionNo,$requisitionYear,$invoiceNo,$invoiceYear,$executeType)
	{
		$sql	= " INSERT INTO finance_pettycash_requisition_detail 
					(REQUISITION_NO, 
					REQUISITION_YEAR, 
					INVOICE_NO, 
					INVOICE_YEAR
					)
					VALUES
					('$requisitionNo', 
					'$requisitionYear', 
					'$invoiceNo', 
					'$invoiceYear'
					)";
					//die($sql);
		$result	= $this->db->$executeType($sql);	
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql; 	
		}
		return $data;
	}
	private function deleteDetails_sql($requisitionNo,$requisitionYear,$executeType)
	{
		$sql	= " DELETE FROM finance_pettycash_requisition_detail 
					WHERE
					REQUISITION_NO = '$requisitionNo' AND REQUISITION_YEAR = '$requisitionYear'";
		$result	= $this->db->$executeType($sql);	
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql; 	
		}
		return $data;
	}
	private function updateApproveByStatus_sql($requisitionNo,$requisitionYear,$maxStatus)
	{
		$sql = "UPDATE finance_pettycash_requisition_approvedby 
				SET
				STATUS = $maxStatus+1
				WHERE
				REQUISITION_NO = '$requisitionNo' AND 
				REQUISITION_YEAR = '$requisitionYear' AND 
				STATUS = 0 ";
		//die($sql);
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function updateHeader_result($requisitionNo,$requisitionYear,$total,$remark,$userID,$status,$approveLevels,$executeType)
	{
		$sql	= "	UPDATE finance_pettycash_requisition_header 
					SET
					TOTAL_AMOUNT = '$total' , 
					REMARKS = '$remark' , 
					STATUS = '$status',
					APPROVE_LEVELS = '$approveLevels',
					MODIFIED_BY = '$userID' , 
					MODIFIED_DATE = NOW()
					
					WHERE
					REQUISITION_NO = '$requisitionNo' AND REQUISITION_YEAR = '$requisitionYear'";
		$result	= $this->db->$executeType($sql);	
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql; 	
		}
		return $data;
					
	}
	private function update_header_status_sql($requisitionNo,$requisitionYear,$status)
	{
 		
		$sql = "UPDATE finance_pettycash_requisition_header 
				SET
				STATUS = '$status'
				
				WHERE
				REQUISITION_NO = '$requisitionNo' AND REQUISITION_YEAR = '$requisitionYear'";
				
 		return $sql;
	}
	
	private function approved_by_insert_sql($requisitionNo,$requisitionYear,$userId,$approval){
 		
		$sql = "INSERT INTO finance_pettycash_requisition_approvedby 
				(REQUISITION_NO, 
				REQUISITION_YEAR, 
				APPROVE_LEVEL, 
				APPROVED_BY, 
				APPROVED_DATE, 
				STATUS
				)
				VALUES
				('$requisitionNo', 
				'$requisitionYear', 
				'$approval', 
				'$userId', 
				NOW(), 
				0)";
 		
		return $sql;
	}
//END  - PRIVATE FUNCTIONS}
	
}
?>
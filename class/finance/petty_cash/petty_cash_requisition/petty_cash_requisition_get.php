<?php
class cls_petty_cash_requisition_get{
 
	private $db;
		
	function __construct($db)
	{
		$this->db = $db;	
	}
	
//BEGIN - PUBLIC FUNCTIONS {
	public function getInvoice($toDate,$fromDate,$locationID,$executeType)
	{
		return $this->getInvoice_sql($toDate,$fromDate,$locationID,$executeType);	
	}
	public function get_header($requestNo,$requestYear,$executeType)
	{
		$sql	= $this->get_header_sql($requestNo,$requestYear);
		$row	= $this->get_row($sql,$executeType);
		return $row;
	}
	public function getDetails($requestNo,$requestYear,$main_DB,$hr_DB,$executeType)
	{
		return $this->getDetails_sql($requestNo,$requestYear,$main_DB,$hr_DB,$executeType);
	}
	public function get_report_approval_details_result($requestNo,$requestYear,$executeType){
		$sql 	= $this->Load_Report_approval_details_sql($requestNo,$requestYear);
		$result = $this->db->$executeType($sql);
		return $result;
	}
	public function getItemStatus($invoiceNo,$invoiceYear,$location,$executeType)
	{
		return $this->getItemStatus_sql($invoiceNo,$invoiceYear,$location,$executeType);	
	}
	public function getInvoiceItems($requestNo,$requestYear,$executeType)
	{
		return $this->getInvoiceItems_sql($requestNo,$requestYear,$executeType);
	}
	public function getItemName($itemID,$executeType)
	{
		return $this->getItemName_sql($itemID,$executeType);	
	}
	public function getMaxStatus($requestNo,$requestYear)
	{
		return $this->getMaxStatus_sql($requestNo,$requestYear);
	}
	public function get_max_approve_levels($executeType)
	{
		$sql	= $this->get_max_approve_levels_sql();
		$row	= $this->get_row($sql,$executeType);
		return $row['LEVELS'];
	}
	//END 	- PUBLIC FUNCTIONS }

//BEGIN - PRIVATE FUNCTIONS {
	private function getInvoice_sql($toDate,$fromDate,$locationID,$executeType)
	{
		$sql	= "	SELECT
				CONCAT(finance_pettycash_invoice_header.PETTY_INVOICE_NO,'/',finance_pettycash_invoice_header.PETTY_INVOICE_YEAR) AS CON_SERIAL_NO,
				CONCAT(finance_pettycash_invoice_header.PETTY_INVOICE_NO,'-',finance_pettycash_invoice_header.PETTY_INVOICE_YEAR) AS FILE_NAME,
				finance_pettycash_invoice_header.PETTY_INVOICE_NO AS SERIAL_NO,
				finance_pettycash_invoice_header.PETTY_INVOICE_YEAR AS SERIAL_YEAR,
				finance_pettycash_invoice_detail.PETTY_CASH_ITEM_ID AS PETTY_CASH_ITEM,
				Sum(finance_pettycash_invoice_detail.INVOICE_AMOUNT) AS AMOUNT,
				finance_mst_pettycash_item.`NAME` AS ITEM
				FROM
				finance_pettycash_invoice_header
				INNER JOIN finance_pettycash_invoice_detail ON finance_pettycash_invoice_header.PETTY_INVOICE_NO = finance_pettycash_invoice_detail.PETTY_INVOICE_NO AND finance_pettycash_invoice_header.PETTY_INVOICE_YEAR = finance_pettycash_invoice_detail.PETTY_INVOICE_YEAR
				INNER JOIN finance_mst_pettycash_item ON finance_pettycash_invoice_detail.PETTY_CASH_ITEM_ID = finance_mst_pettycash_item.ID
				WHERE
				finance_pettycash_invoice_header.`STATUS` = 1 AND
				finance_pettycash_invoice_header.INVOICE_TYPE <>2 AND 
				finance_pettycash_invoice_header.REQUEST_STATUS = 0
				";
		if($toDate != '' && $fromDate != '')
			$sql	.= " AND 
					finance_pettycash_invoice_header.DATE BETWEEN '$fromDate' AND '$toDate'";
		else
			$sql	.= " AND
					 MONTH(finance_pettycash_invoice_header.DATE)= MONTH(NOW())";
					 
		$sql	.=	" AND finance_pettycash_invoice_header.LOCATION_ID = $locationID 
					 GROUP BY
					finance_pettycash_invoice_header.PETTY_INVOICE_NO,
					finance_pettycash_invoice_header.PETTY_INVOICE_YEAR ";
		$result	= $this->db->$executeType($sql);
		return $result;
	}
	private function get_header_sql($requestNo,$requestYear)
	{
		$sql	= " SELECT 	REQUISITION_NO, 
					REQUISITION_YEAR, 
					CONCAT(REQUISITION_NO,'/',REQUISITION_YEAR) AS CON_REQUISITION_NO,
					TOTAL_AMOUNT,
					REMARKS, 
					REQUEST_DATE, 
					STATUS, 
					APPROVE_LEVELS,
					APPROVE_LEVELS AS LEVELS, 
					COMPANY_ID, 
					LOCATION_ID, 
					CREATED_BY, 
					CREATED_DATE, 
					MODIFIED_BY, 
					MODIFIED_DATE,
					L.strName AS LOCATION,
					U.strUserName AS USER

					FROM
					finance_pettycash_requisition_header
					INNER JOIN mst_locations L ON finance_pettycash_requisition_header.LOCATION_ID = L.intId
					INNER JOIN sys_users U ON finance_pettycash_requisition_header.CREATED_BY = U.intUserId
					WHERE 
					finance_pettycash_requisition_header.REQUISITION_NO = '$requestNo' AND 
					finance_pettycash_requisition_header.REQUISITION_YEAR = '$requestYear'";
					//die($sql);
		return $sql;		
	}
	
	private function get_row($sql,$executeType)
	{
		$result	= $this->db->$executeType($sql);
		$row=mysqli_fetch_array($result);
		return $row;		
	}
	private function getDetails_sql($requestNo,$requestYear,$main_DB,$hr_DB,$executeType)
	{
		$sql	= " SELECT 	
					CONCAT (PRD.INVOICE_NO,'/',PRD.INVOICE_YEAR) AS CON_INVOICE_NO,
					CONCAT (PRD.INVOICE_NO,'-',PRD.INVOICE_YEAR) AS FILE_PATH,
					PRD.INVOICE_NO,
					PRD.INVOICE_YEAR,	
					ABS(PCI.INVOICE_AMOUNT) AS AMOUNT,
					PCI.PETTY_CASH_ITEM_ID,
					PIT.NAME AS ITEM,
					PCIH.DATE AS DATE,
					$hr_DB.E.strInitialName AS USEDBY			 
					FROM 
					$main_DB.finance_pettycash_requisition_detail PRD 
					INNER JOIN $main_DB.finance_pettycash_invoice_header PCIH
					ON PRD.INVOICE_NO = PCIH.PETTY_INVOICE_NO
					  AND PRD.INVOICE_YEAR = PCIH.PETTY_INVOICE_YEAR
				  INNER JOIN $main_DB.finance_pettycash_invoice_detail PCI
					ON PCIH.PETTY_INVOICE_NO = PCI.PETTY_INVOICE_NO
					  AND PCIH.PETTY_INVOICE_YEAR = PCI.PETTY_INVOICE_YEAR
					INNER JOIN $main_DB.finance_mst_pettycash_item PIT ON PCI.PETTY_CASH_ITEM_ID = PIT.ID
					INNER JOIN $hr_DB.mst_employee E ON PCI.REQUEST_USER_ID = $hr_DB.E.intEmployeeId
					WHERE	
					PRD.REQUISITION_NO	= '$requestNo' AND
					PRD.REQUISITION_YEAR = '$requestYear'";
		$result	= $this->db->$executeType($sql);
		return $result;		
	}
	private function Load_Report_approval_details_sql($requestNo,$requestYear){
	
		
	   	$sql = "SELECT
				finance_pettycash_requisition_approvedby.REQUISITION_NO,
				finance_pettycash_requisition_approvedby.REQUISITION_YEAR,
				finance_pettycash_requisition_approvedby.APPROVE_LEVEL AS intApproveLevelNo,
				finance_pettycash_requisition_approvedby.APPROVED_DATE AS dtApprovedDate,
				finance_pettycash_requisition_approvedby.`STATUS`,
				sys_users.strUserName AS UserName
				FROM
				finance_pettycash_requisition_approvedby
				INNER JOIN sys_users ON finance_pettycash_requisition_approvedby.APPROVED_BY = sys_users.intUserId
				WHERE 
				finance_pettycash_requisition_approvedby.REQUISITION_NO = '$requestNo' AND
				finance_pettycash_requisition_approvedby.REQUISITION_YEAR = '$requestYear' ORDER BY APPROVED_DATE asc";
				//die($sql);
		return $sql;
	}
	private function getMaxStatus_sql($requestNo,$requestYear)
	{
		$sql = "SELECT MAX(tb.STATUS) AS maxStatus  
				FROM finance_pettycash_requisition_approvedby AS tb
				WHERE 
				tb.REQUISITION_NO='$requestNo' AND
				tb.REQUISITION_YEAR='$requestYear' ";
		
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		return $row['maxStatus'];
	}
	
	private function get_max_approve_levels_sql(){
		$sql	="	SELECT
					MAX(finance_pettycash_requisition_header.APPROVE_LEVELS) AS LEVELS
					FROM `finance_pettycash_requisition_header`
					";
					
		return $sql;
	}
	private function getInvoiceItems_sql($requestNo,$requestYear,$executeType)
	{
		$sql	= " SELECT
					finance_pettycash_requisition_detail.INVOICE_NO,
					finance_pettycash_requisition_detail.INVOICE_YEAR,
					finance_pettycash_invoice_detail.PETTY_CASH_ITEM_ID AS PETTY_CASH_ITEM,
					finance_mst_pettycash_item.`NAME`
					FROM
					finance_pettycash_requisition_detail
					INNER JOIN finance_pettycash_invoice_detail ON finance_pettycash_requisition_detail.INVOICE_NO = finance_pettycash_invoice_detail.PETTY_INVOICE_NO AND finance_pettycash_requisition_detail.INVOICE_YEAR = finance_pettycash_invoice_detail.PETTY_INVOICE_YEAR
					INNER JOIN finance_mst_pettycash_item ON finance_pettycash_invoice_detail.PETTY_CASH_ITEM_ID = finance_mst_pettycash_item.ID
					WHERE
					finance_pettycash_requisition_detail.REQUISITION_NO = '$requestNo' AND
					finance_pettycash_requisition_detail.REQUISITION_YEAR = $requestYear ";	
		$result	= $this->db->$executeType($sql);
		return $result;
	}
	private function getItemStatus_sql($invoiceNo,$invoiceYear,$location,$executeType)
	{
		$sql	= " SELECT	
					PCI.REQUEST_STATUS 
					FROM 
					finance_pettycash_invoice_header PCI
					
					WHERE	
					PCI.PETTY_INVOICE_NO = '$invoiceNo' AND
					PCI.PETTY_INVOICE_YEAR = '$invoiceYear' AND 
					PCI.LOCATION_ID = '$location'";
								
		$result	= $this->db->$executeType($sql);
		return $result;
	}
	private function getItemName_sql($itemID,$executeType)
	{
		$sql	= " SELECT 	
					NAME
					 
					FROM 
					finance_mst_pettycash_item
					
					WHERE
					ID = $itemID"	;
					
		$result	= $this->db->$executeType($sql);
		$row	= mysqli_fetch_array($result);
		return $row;
	}
//END  - PRIVATE FUNCTIONS}
	
}
?>
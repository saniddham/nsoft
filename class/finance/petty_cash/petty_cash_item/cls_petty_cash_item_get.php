<?php
class cls_petty_cash_item_get
{
	private $db;
		
	function __construct($db)
	{
		$this->db = $db;	
	}
	
//BEGIN - PUBLIC FUNCTIONS {
	public	function loadCategory($executeType)
	{
		return $this->loadCategory_sql($executeType);
	}
	public function loadItems($executeType)
	{
		return $this->loadItem_sql($executeType);	
	}
	public function searchItem($id,$executeType)
	{
		return $this->searchItem_sql($id,$executeType);	
	}
	public function pettyCash_item($executeType)
	{
		return $this->pettyCash_item_sql($executeType);	
	}
	
	public function getCategoryAndItemName($itemId)
	{
		return $this->getCategoryAndItemName_sql($itemId);
	}
	public function getIOUID()
	{
		return $this->getIOUID_row();	
	}
	public function getCategoryWiseItem($category)
	{
		return $this->getCategoryWiseItem_result($category);
	}
//END 	- PUBLIC FUNCTIONS }

//BEGIN - PRIVATE FUNCTIONS {
	private function loadCategory_sql($executeType)
	{
		$sql	= " SELECT 	ID, 
					NAME 
						 
					FROM 
					finance_mst_pettycash_category
					
					WHERE
					STATUS = 1
					
					ORDER BY	
					NAME"	;
		$result	= $this->db->$executeType($sql);
		return $result;
	}
	private function loadItem_sql($executeType)
	{
		$sql	= " SELECT 	ID, 
					NAME
					 
					FROM 
					finance_mst_pettycash_item 
										
					ORDER BY
					NAME";
					
		$result	= $this->db->$executeType($sql);
		return $result;
	}
	private function searchItem_sql($id,$executeType)
	{
		$sql	= " SELECT 
					CATEGORY_ID, 
					CODE, 
					NAME, 
					STATUS
					
					FROM
					finance_mst_pettycash_item
					
					WHERE 
					ID = '$id'"		;
		$result	= $this->db->$executeType($sql);
		$row	= mysqli_fetch_array($result);
		return $row;	
	}
	private function pettyCash_item_sql($executeType)
	{
		$sql	= " SELECT 	
					PCC.NAME as category, 
					PCI.CODE, 
					PCI.NAME,
					PCI.STATUS 
						 
					FROM 
					finance_mst_pettycash_item PCI
					INNER JOIN
					finance_mst_pettycash_category	PCC ON PCI.CATEGORY_ID	= PCC.ID";
					//die($sql);
		$result		= $this->db->$executeType($sql);
		$rowCount	= mysqli_num_rows($result);
		return $result;			
		
	}
	
	private function getCategoryAndItemName_sql($itemId)
	{
		$sql = "SELECT
				  C.NAME AS CATEGORY_NAME,
				  I.NAME AS ITEM_NAME
				FROM finance_mst_pettycash_item I
				  INNER JOIN finance_mst_pettycash_category C
					ON C.ID = I.CATEGORY_ID
				WHERE I.ID = $itemId";
		$result = $this->db->RunQuery($sql);
		return mysqli_fetch_array($result);
	}
	private function getCategoryWiseItem_result($category)
	{
		$sql		= " SELECT
						finance_mst_pettycash_item.`CODE`,
						finance_mst_pettycash_item.ID,
						finance_mst_pettycash_item.ITEM_ID,
						finance_mst_pettycash_item.CATEGORY_ID,
						finance_mst_pettycash_item.`NAME`,
						finance_mst_pettycash_item.`STATUS`
						FROM
						finance_mst_pettycash_item
						WHERE
						finance_mst_pettycash_item.CATEGORY_ID = '$category'";
						
		$result 	= $this->db->RunQuery2($sql);
		$rowCount	= mysqli_num_rows($result);
		$rowCount	= ($rowCount=NULL?0:$rowCount);
		return $rowCount;
		
	}
	
	private function getIOUID_row()
	{
		$sql	= " select finance_mst_pettycash_category.ID 
					from finance_mst_pettycash_category
					where 
					finance_mst_pettycash_category.IOU_STATUS = 1 AND finance_mst_pettycash_category.STATUS = 1";

		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		return	$row['ID'];
	}
//END 	- PRIVATE FUNCTIONS }
}
?>
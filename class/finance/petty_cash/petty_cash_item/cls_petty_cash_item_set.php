<?php
class cls_petty_cash_item_set
{
	private $db;
		
	function __construct($db)
	{
		$this->db = $db;	
	}
	
//BEGIN - PUBLIC FUNCTIONS {
	public function saveItem($itemCode,$itemName,$category,$status,$executeType)
	{
		return $this->saveItem_sql($itemCode,$itemName,$category,$status,$executeType);
	}
	public function updateItem($searchID,$itemCode,$itemName,$category,$status,$executeType)
	{
		return $this->updateItem_sql($searchID,$itemCode,$itemName,$category,$status,$executeType);	
	}
	public function deleteItem($searchID,$executeType)
	{
		return $this->deleteItem_sql($searchID,$executeType);	
	}
//END 	- PUBLIC FUNCTIONS }

//BEGIN - PRIVATE FUNCTIONS {
	private function saveItem_sql($itemCode,$itemName,$category,$status,$executeType)
	{
		$sql	= " INSERT INTO finance_mst_pettycash_item 
					( 
					CATEGORY_ID, 
					CODE, 
					NAME, 
					STATUS
					)
					VALUES
					('$category', 
					'$itemCode',
					'$itemName', 
					'$status'
					)";
		//die($sql);
		$result	= $this->db->$executeType($sql);
		
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
				
		}
		return $data;
		
	}
	private function updateItem_sql($searchID,$itemCode,$itemName,$category,$status,$executeType)
	{
		$sql	= " UPDATE finance_mst_pettycash_item 
					SET
					CODE = '$itemCode' , 
					NAME = '$itemName' , 
					CATEGORY_ID = '$category' ,
					STATUS = '$status'
					
					WHERE
					ID = '$searchID'";
					
		$result	= $this->db->$executeType($sql);
		
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
				
		}
		return $data;
			
	}
	private function deleteItem_sql($searchID,$executeType)
	{
		$sql	= " DELETE FROM finance_mst_pettycash_item 
					WHERE
					ID = '$searchID' ";
		
		$result	= $this->db->$executeType($sql);
		
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
				
		}
		return $data;
		
	}
//END 	- PRIVATE FUNCTIONS }
}
?>
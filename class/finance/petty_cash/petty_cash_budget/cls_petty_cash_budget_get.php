<?php
class cls_petty_cash_budget_get
{
 
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;	
	}
	//BEGIN - PUBLIC FUNCTIONS
	public function getYear($executionType)
	{
		return $this->getYear_sql($executionType);
	}
	public function getLocation($companyId,$executionType)
	{
		return $this->getLocation_sql($companyId,$executionType);
	}
	public function getAllocateBudget($year,$month,$locationId,$executionType)
	{
		return $this->getAllocateBudget_sql($year,$month,$locationId,$executionType);
	}
	public function checkBudgetPlanAvailable($year,$location,$month,$executionType)
	{
		return $this->checkBudgetPlanAvailable_sql($year,$location,$month,$executionType);
	}
	public function get_header($year,$executionType)
	{
		return $this->get_header_sql($year,$executionType);
	}
	//END 	- PUBLIC FUNCTIONS
	
	//BEGIN - PRIVATE FUNCTIONS
	private function getYear_sql($executionType)
	{
		$sql = "SELECT YEAR
				FROM finance_pettycash_budget_header
				WHERE STATUS = 1
				UNION
				(
				SELECT YEAR(CURDATE())
				)
				UNION
				(
				SELECT YEAR(CURDATE())+1 
				)";
		
		$result	= $this->db->$executionType($sql);
		return $result;
	}
	private function getLocation_sql($companyId,$executionType)
	{
		$sql = "SELECT intId,strName
				FROM mst_locations
				WHERE intStatus = 1 AND
				intCompanyId = '$companyId'
				ORDER BY strName ";
		
		$result	= $this->db->$executionType($sql);
		return $result;
	}
	private function getAllocateBudget_sql($year,$month,$locationId,$executionType)
	{
		$sql = "SELECT AMOUNT
				FROM finance_pettycash_budget_details
				WHERE YEAR = '$year' AND
				MONTH = '$month' AND
				LOCATION_ID = '$locationId' ";
		
		$result	= $this->db->$executionType($sql);
		$row	= mysqli_fetch_array($result);
		return $row['AMOUNT'];
	}
	private function checkBudgetPlanAvailable_sql($year,$location,$month,$executionType)
	{
		$sql = "SELECT *
				FROM finance_pettycash_budget_details
				WHERE YEAR = '$year' AND
				MONTH = '$month' AND
				LOCATION_ID = '$location' ";
		
		$result = $this->db->$executionType($sql);
		if(mysqli_num_rows($result)>0)
			return true;
		else
			return false;
	}
	private function get_header_sql($year,$executionType)
	{
		$sql = "SELECT 	YEAR, 
				STATUS, 
				COMPANY_ID, 
				LOCATION_ID, 
				CREATED_BY, 
				CREATED_DATE, 
				MODIFIED_BY, 
				MODIFIED_DATE
				 
				FROM 
				finance_pettycash_budget_header 
				WHERE YEAR = '$year' ";
		
		$result = $this->db->$executionType($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row;
	}
	//END  - PRIVATE FUNCTIONS
	
}
?>
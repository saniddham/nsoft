<?php
class cls_petty_cash_budget_set
{
 
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;	
	}
	
	//BEGIN - PUBLIC FUNCTIONS
	public function saveHeader($year,$userId,$companyId,$locationId)
	{
		return $this->saveHeader_sql($year,$userId,$companyId,$locationId);
	}
	public function updateHeader($year,$userId,$companyId,$locationId)
	{
		return $this->updateHeader_sql($year,$userId,$companyId,$locationId);
	}
	public function updateDetails($year,$location,$month,$amount)
	{
		return $this->updateDetails_sql($year,$location,$month,$amount);
	}
	public function saveDetails($year,$location,$month,$amount)
	{
		return $this->saveDetails_sql($year,$location,$month,$amount);
	}
	public function updateLog($year,$location,$month)
	{
		return $this->updateLog_sql($year,$location,$month);
	}
	//END 	- PUBLIC FUNCTIONS
	
	//BEGIN - PRIVATE FUNCTIONS
	private function saveHeader_sql($year,$userId,$companyId,$locationId)
	{
		$sql = "INSERT INTO finance_pettycash_budget_header 
				(
				YEAR, 
				COMPANY_ID, 
				LOCATION_ID, 
				CREATED_BY, 
				CREATED_DATE
				)
				VALUES
				(
				'$year',  
				'$companyId', 
				'$locationId', 
				'$userId', 
				NOW()
				)";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function updateHeader_sql($year,$userId,$companyId,$locationId)
	{
		$sql = "UPDATE finance_pettycash_budget_header 
				SET
				COMPANY_ID = '$companyId' , 
				LOCATION_ID = '$locationId' , 
				MODIFIED_BY = '$userId'
				WHERE
				YEAR = '$year' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function updateDetails_sql($year,$location,$month,$amount)
	{
		$sql = "UPDATE finance_pettycash_budget_details 
				SET
				AMOUNT = '$amount'
				WHERE
				YEAR = '$year' AND 
				MONTH = '$month' AND 
				LOCATION_ID = '$location' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function saveDetails_sql($year,$location,$month,$amount)
	{
		$sql = "INSERT INTO finance_pettycash_budget_details 
				(
				YEAR, 
				MONTH, 
				LOCATION_ID, 
				AMOUNT
				)
				VALUES
				(
				'$year', 
				'$month', 
				'$location', 
				'$amount'
				)";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function updateLog_sql($year,$location,$month)
	{
		$sql = "INSERT INTO finance_pettycash_budget_log 
				( 
				YEAR, 
				MONTH, 
				LOCATION_ID, 
				AMOUNT
				)
				(
				SELECT YEAR,
				MONTH,
				LOCATION_ID,
				AMOUNT
				FROM finance_pettycash_budget_details
				WHERE 
				YEAR = '$year' AND
				MONTH = '$month' AND
				LOCATION_ID = '$location'
				)";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	//END  - PRIVATE FUNCTIONS
}
?>
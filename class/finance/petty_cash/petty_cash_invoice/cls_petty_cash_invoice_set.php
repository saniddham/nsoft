<?php

$session_userId			= $_SESSION["userId"];
$session_locationId 	= $_SESSION["CompanyID"];
$session_companyId 		= $_SESSION["headCompanyId"];

class cls_petty_cash_invoice_set
{
	private $db;
		
	function __construct($db)
	{
		$this->db = $db;	
	}
	
//BEGIN - PUBLIC FUNCTIONS {
	public function update_header_status($serialYear,$serialNo,$status,$executeType)
	{ 

   		$sql	= $this->update_header_status_sql($serialYear,$serialNo,$status);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
			return $response;
 	}
	public function update_department_header_status($serialYear,$serialNo,$deparmtent,$status,$executeType)
	{ 

   		$sql	= $this->update_department_header_status_sql($serialYear,$serialNo,$deparmtent,$status);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
			return $response;
 	}
	

	public function approved_by_insert($serialYear,$serialNo,$userId,$approval,$executeType)
	{ 

   		$sql	= $this->approved_by_insert_sql($serialYear,$serialNo,$userId,$approval);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
			return $response;
 	}
	
	public function approved_by_department_insert($serialYear,$serialNo,$department,$userId,$approval,$executeType)
	{ 

   		$sql	= $this->approved_by_department_insert_sql($serialYear,$serialNo,$department,$userId,$approval);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
			return $response;
 	}
	
	public function saveHeader($invoiceNo,$invoiceYear,$settle_no,$settle_year,$reqBy,$date,$amount,$rcvAmount,$balAmnt,$itemId,$remarks,$location,$userId,$status,$approveLevels)
	{
		return $this->saveHeader_sql($invoiceNo,$invoiceYear,$settle_no,$settle_year,$reqBy,$date,$amount,$rcvAmount,$balAmnt,$itemId,$remarks,$location,$userId,$status,$approveLevels);
	}
	public function updateHeader($invoiceNo,$invoiceYear,$settle_no,$settle_year,$reqBy,$date,$amount,$rcvAmount,$balAmnt,$itemId,$remarks,$location,$userId,$status,$approveLevels)
	{
		return $this->updateHeader_sql($invoiceNo,$invoiceYear,$settle_no,$settle_year,$reqBy,$date,$amount,$rcvAmount,$balAmnt,$itemId,$remarks,$location,$userId,$status,$approveLevels);
	}
	
	public function updateBalField($invoiceNo,$invoiceYear,$amount)
	{
		return $this->updateBalField_sql($invoiceNo,$invoiceYear,$amount);
	}
	public function updateBalField_zero($invoiceNo,$invoiceYear,$amount)
	{
		return $this->updateBalField_zero_sql($invoiceNo,$invoiceYear,$amount);
	}
	
	public function updateApproveByStatus($invoiceNo,$invoiceYear,$maxStatus)
	{
		return $this->updateApproveByStatus_sql($invoiceNo,$invoiceYear,$maxStatus);
	}
	
	public function updateApproveByStatus_dep($invoiceNo,$invoiceYear,$maxStatus)
	{
		return $this->updateApproveByStatus_dep_sql($invoiceNo,$invoiceYear,$maxStatus);
	}
	
	public function finance_transaction_insert($serialYear,$serialNo,$executeType){
		return $this->finance_transaction_insert_data($serialYear,$serialNo,$executeType);
	}
	public function deleteDetail($invoiceNo,$invoiceYear)
	{
		return $this->deleteDetail_sql($invoiceNo,$invoiceYear);
	}
	public function saveDetail($invoiceNo,$invoiceYear,$departmentId,$amount,$order)
	{
		return $this->saveDetail_sql($invoiceNo,$invoiceYear,$departmentId,$amount,$order);
	}
//END 	- PUBLIC FUNCTIONS }

	private function update_header_status_sql($serialYear,$serialNo,$status)
	{
 		
		$sql = "UPDATE finance_pettycash_invoice 
				SET 
				STATUS='$status' 
				WHERE
				finance_pettycash_invoice.SERIAL_NO = '$serialNo' AND
				finance_pettycash_invoice.SERIAL_YEAR = '$serialYear'";
 		return $sql;
	}
	
	private function update_department_header_status_sql($serialYear,$serialNo,$department,$status)
	{
 		
		$sql = "UPDATE finance_pettycash_invoice_department 
				SET 
				DEP_APPROVAL_STATUS='$status' 
				WHERE
				finance_pettycash_invoice_department.SERIAL_NO = '$serialNo' AND
				finance_pettycash_invoice_department.SERIAL_YEAR = '$serialYear' AND
				finance_pettycash_invoice_department.DEPARTMENT_ID = '$department'";
 		return $sql;
	}
	
	
	private function approved_by_insert_sql($serialYear,$serialNo,$userId,$approval){
 		
		$sql = "INSERT INTO `finance_pettycash_invoice_approve_by` (`SERIAL_NO`,`SERIAL_YEAR`,`APPROVE_LEVEL`,APPROVE_BY,APPROVE_DATE,STATUS) 
			VALUES ('$serialNo','$serialYear','$approval','$userId',now(),0)";
 		
		return $sql;
	}

	private function approved_by_department_insert_sql($serialYear,$serialNo,$department,$userId,$approval){
 		
		$sql = "INSERT INTO `finance_pettycash_invoice_department_approved_by` (`SERIAL_NO`,`SERIAL_YEAR`,DEPARTMENT_ID,`APPROVE_LEVEL`,APPROVE_BY,APPROVE_DATE,STATUS) 
			VALUES ('$serialNo','$serialYear','$department','$approval','$userId',now(),0)";
 		
		return $sql;
	}
	
	private function saveHeader_sql($invoiceNo,$invoiceYear,$settle_no,$settle_year,$reqBy,$date,$amount,$rcvAmount,$balAmnt,$itemId,$remarks,$location,$userId,$status,$approveLevels)
	{
		$sql = "INSERT INTO finance_pettycash_invoice 
				(
				SERIAL_NO, 
				SERIAL_YEAR, 
				SETTLEMENT_NO, 
				SETTLEMENT_YEAR, 
				PETTY_CASH_ITEM, 
				DATE, 
				AMOUNT,  
				RECEIVE_AMOUNT, 
				BALANCE_AMOUNT, 
				REQUEST_EMPLYEE, 
				REMARKS, 
				STATUS, 
				APPROVE_LEVELS, 
				CREATED_DATE, 
				CREATED_BY, 
				LOCATION_ID
				)
				VALUES
				(
				'$invoiceNo', 
				'$invoiceYear', 
				'$settle_no', 
				'$settle_year', 
				'$itemId', 
				'$date', 
				'$amount',
				'$rcvAmount',
				'$balAmnt',
				'$reqBy', 
				'$remarks', 
				'$status', 
				'$approveLevels', 
				NOW(),
				'$userId',
				'$location'
				);";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function updateHeader_sql($invoiceNo,$invoiceYear,$settle_no,$settle_year,$reqBy,$date,$amount,$rcvAmount,$balAmnt,$itemId,$remarks,$location,$userId,$status,$approveLevels)
	{
		$sql = "UPDATE finance_pettycash_invoice 
				SET
				SETTLEMENT_NO = '$settle_no' , 
				SETTLEMENT_YEAR = '$settle_year' , 
				PETTY_CASH_ITEM = '$itemId' , 
				DATE = '$date' , 
				AMOUNT = '$amount' , 
				RECEIVE_AMOUNT = '$rcvAmount' , 
				BALANCE_AMOUNT = '$balAmnt' , 
				REQUEST_EMPLYEE = '$reqBy' , 
				REMARKS = '$remarks' , 
				STATUS = '$status' , 
				APPROVE_LEVELS = '$approveLevels' , 
				MODIFIED_DATE = NOW() , 
				MODIFIED_BY = '$userId' , 
				LOCATION_ID = '$location'
				WHERE
				SERIAL_NO = '$invoiceNo' AND  
				SERIAL_YEAR = '$invoiceYear' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function updateApproveByStatus_sql($invoiceNo,$invoiceYear,$maxStatus)
	{
		$sql = "UPDATE finance_pettycash_invoice_approve_by 
				SET
				STATUS = $maxStatus+1
				WHERE
				SERIAL_NO = '$invoiceNo' AND 
				SERIAL_YEAR = '$invoiceYear' AND 
				STATUS = 0 ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function updateApproveByStatus_dep_sql($invoiceNo,$invoiceYear,$maxStatus)
	{
		$sql = "UPDATE finance_pettycash_invoice_department_approved_by 
				SET
				STATUS = $maxStatus+1
				WHERE
				SERIAL_NO = '$invoiceNo' AND 
				SERIAL_YEAR = '$invoiceYear'  AND 
				STATUS = 0 ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	
	private function finance_transaction_insert_data($serialYear,$serialNo,$executeType){
		global $session_userId;
		global $session_locationId;
		global $session_companyId;
		global $savedStatus;
		global $error_sql;
		global $savedMasseged;
		
		$sql1 = "SELECT
				finance_pettycash_invoice.SERIAL_NO,
				finance_pettycash_invoice.SERIAL_YEAR,
				finance_mst_pettycash_category.GL_ID,
				finance_pettycash_invoice.AMOUNT,
				finance_pettycash_invoice.DATE,
				mst_companies.intBaseCurrencyId
				FROM
				finance_pettycash_invoice
				INNER JOIN finance_mst_pettycash_item ON finance_pettycash_invoice.PETTY_CASH_ITEM = finance_mst_pettycash_item.ID
				INNER JOIN finance_mst_pettycash_category ON finance_mst_pettycash_item.CATEGORY_ID = finance_mst_pettycash_category.ID
				INNER JOIN mst_locations ON finance_pettycash_invoice.LOCATION_ID = mst_locations.intId
				INNER JOIN mst_companies ON mst_locations.intCompanyId = mst_companies.intId
				WHERE
				finance_pettycash_invoice.SERIAL_NO = '$serialNo' AND
				finance_pettycash_invoice.SERIAL_YEAR = '$serialYear'";
		$result1 = $this->db->$executeType($sql1);		
		while($row=mysqli_fetch_array($result1))
		{
				$account_pc		= '1717';
 				$account_exp	= $row['GL_ID'];
				$ammount		= $row['AMOUNT'];
				if($ammount < 0){
					$transType_pc		= 'C';
					$transType_pc_exp	= 'D';
				}
				else{
					$transType_pc		= 'D';
					$transType_pc_exp	= 'C';
				}
				$ammount		= abs($ammount);
				//$transType		= $row['TRANSACTION_TYPE'];
				$transCat		= 'JE';
				$docType		= 'JOURNAL_ENTRY';
				$refNo			= '';
				$curr			= $row['intBaseCurrencyId'];
				$entryDate		= $row["DATE"];
				$remarks		= 'Petty Cash';
				
				if($account_exp > 0){
					
					$sqlI = "INSERT INTO `finance_transaction` 
								(`CHART_OF_ACCOUNT_ID`,
								`AMOUNT`,
								`DOCUMENT_NO`,
								DOCUMENT_YEAR,
								DOCUMENT_TYPE,
								TRANSACTION_TYPE,
								TRANSACTION_CATEGORY,
								BANK_REFERENCE_NO,
								CURRENCY_ID,
								LOCATION_ID,
								COMPANY_ID,
								LAST_MODIFIED_BY,
								LAST_MODIFIED_DATE,
								REMARKS) 
							VALUES ('$account_pc',
								'$ammount',
								'$serialNo',
								'$serialYear',
								'$docType',
								'$transType_pc',
								'$transCat',
								'$refNo',
								'$curr',
								'$session_locationId',
								'$session_companyId',
								'$session_userId',
								'$entryDate',
								'$remarks')";
					$result = $this->db->$executeType($sqlI);
					
					if(!$result){
						$data['savedStatus']		= 'fail';
						$data['savedMasseged']		= $this->db->errormsg;
						$data['error_sql']			= $sqlI;
					}
					else{
					$sqlI = "INSERT INTO `finance_transaction` 
								(`CHART_OF_ACCOUNT_ID`,
								`AMOUNT`,
								`DOCUMENT_NO`,
								DOCUMENT_YEAR,
								DOCUMENT_TYPE,
								TRANSACTION_TYPE,
								TRANSACTION_CATEGORY,
								BANK_REFERENCE_NO,
								CURRENCY_ID,
								LOCATION_ID,
								COMPANY_ID,
								LAST_MODIFIED_BY,
								LAST_MODIFIED_DATE,
								REMARKS) 
							VALUES ('$account_exp',
								'$ammount',
								'$serialNo',
								'$serialYear',
								'$docType',
								'$transType_pc_exp',
								'$transCat',
								'$refNo',
								'$curr',
								'$session_locationId',
								'$session_companyId',
								'$session_userId',
								'$entryDate',
								'$remarks')";
					$result = $this->db->$executeType($sqlI);
					
					if(!$result){
						$data['savedStatus']		= 'fail';
						$data['savedMasseged']		= $this->db->errormsg;
						$data['error_sql']			= $sqlI;
					}
				}
				}
			
			
		}
		return $data;
	}
	private function deleteDetail_sql($invoiceNo,$invoiceYear)
	{
		$sql = "DELETE FROM finance_pettycash_invoice_department 
				WHERE
				SERIAL_NO = '$invoiceNo' AND 
				SERIAL_YEAR = '$invoiceYear' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function saveDetail_sql($invoiceNo,$invoiceYear,$departmentId,$amount,$order)
	{
		$sql = "INSERT INTO finance_pettycash_invoice_department 
				(
				SERIAL_NO, 
				SERIAL_YEAR, 
				DEPARTMENT_ID, 
				AMOUNT, 
				ORDER_BY
				)
				VALUES
				(
				'$invoiceNo', 
				'$invoiceYear', 
				'$departmentId', 
				'$amount', 
				'$order'
				)";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	
	private function updateBalField_sql($invoiceNo,$invoiceYear,$amount){
		$sql = "UPDATE finance_pettycash_invoice 
				SET
				BALANCE_AMOUNT = (BALANCE_AMOUNT) +abs($amount)
				WHERE
				SERIAL_NO = '$invoiceNo' AND 
				SERIAL_YEAR = '$invoiceYear' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	
	private function updateBalField_zero_sql($invoiceNo,$invoiceYear,$amount){
		$sql = "UPDATE finance_pettycash_invoice 
				SET
				BALANCE_AMOUNT = $amount  
				WHERE
				SERIAL_NO = '$invoiceNo' AND 
				SERIAL_YEAR = '$invoiceYear' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	
//END 	- PRIVATE FUNCTIONS }
}
?>
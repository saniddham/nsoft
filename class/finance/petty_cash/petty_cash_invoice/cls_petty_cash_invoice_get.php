<?php

class cls_petty_cash_invoice_get
{
	private $db;
		
	function __construct($db)
	{
		$this->db = $db;	
	}
	
//BEGIN - PUBLIC FUNCTIONS {
	public function get_header($serialNo,$serialYear,$main_DB,$hr_DB,$executeType)
	{
		$sql	= $this->get_header_sql($serialNo,$serialYear,$main_DB,$hr_DB);
		$row	= $this->get_row($sql,$executeType);
		return $row;
	}
	public function get_header_department_wise($serialNo,$serialYear,$department,$main_DB,$hr_DB,$executeType)
	{
		$sql	= $this->get_header_department_wise_sql($serialNo,$serialYear,$department,$main_DB,$hr_DB);
		$row	= $this->get_row($sql,$executeType);
		return $row;
	}
	
	public function get_file_uploaded_status($serialNo,$serialYear,$path,$executeType)
	{
		$count	= $this->get_uploaded_invoice_count($serialNo,$serialYear,$path,$executeType);
		return $count;
	}
	public function get_department_approvl_status($serialNo,$serialYear,$path,$executeType)
	{
		$res	= $this->get_department_approvl_response($serialNo,$serialYear,$path,$executeType);
		return $res;
	}
	public function get_report_approval_details_result($serialYear,$serialNo,$executeType){
		$sql 	= $this->Load_Report_approval_details_sql($serialYear,$serialNo);
		$result = $this->db->$executeType($sql);
		return $result;
	}
	
	public function get_dep_wise_report_approval_details_result($serialYear,$serialNo,$department,$executeType){
		$sql 	= $this->get_dep_wise_report_approval_details_sql($serialYear,$serialNo,$department);
		$result = $this->db->$executeType($sql);
		return $result;
	}
 	
	public function getPettyItem($category,$executeType)
	{
		return $this->getPettyItem_sql($category,$executeType);
	}
	public function getPettyRequestedBy($hrDB,$locationId,$executeType)
	{
		return $this->getPettyRequestedBy_sql($hrDB,$locationId,$executeType);
	}
	public function getMaxStatus($invoiceNo,$invoiceYear)
	{
		return $this->getMaxStatus_sql($invoiceNo,$invoiceYear);
	}
	public function getMaxStatus_dep($invoiceNo,$invoiceYear)
	{
		return $this->getMaxStatus_dep_sql($invoiceNo,$invoiceYear);
	}
	public function get_max_approve_levels($executeType)
	{
		$sql	= $this->get_max_approve_levels_sql();
		$row	= $this->get_row($sql,$executeType);
		return $row['LEVELS'];
	}
	public function getBudgetCategoryDetail($itemId,$location,$departmentId,$executionType)
	{
		return $this->getBudgetCategoryDetail_sql($itemId,$location,$departmentId,$executionType);
	}
	public function getFinanceYearId($year,$month,$executionType)
	{
		return $this->getFinanceYearId_sql($year,$month,$executionType);
	}
	public function get_listing_sql($programCode,$approveLevel,$intUser,$main_DB,$hr_DB,$location,$where_string)
	{
		$sql	= $this->load_listing_sql($programCode,$approveLevel,$intUser,$main_DB,$hr_DB,$location,$where_string);
		return $sql;
	}
	public function get_department_listing_sql($programCode,$approveLevel,$intUser,$main_DB,$hr_DB,$location)
	{
		$sql	= $this->load_department_listing_sql($programCode,$approveLevel,$intUser,$main_DB,$hr_DB,$location);
		return $sql;
	}
	
	
	public function getStatus($serialNo,$serialYear)
	{
		return $this->getStatus_sql($serialNo,$serialYear);
	}
	public function get_IOU_invoices($settleNo,$settleYear,$executeType){
		$sql = $this->get_IOU_invoices_sql($settleNo,$settleYear);
		$result = $this->db->$executeType($sql);
		return $result;
	}
	public function getItemAndCategory($serialNo,$serialYear,$executeType){
		$sql = $this->getItemAndCategory_sql($serialNo,$serialYear);
		$result = $this->db->$executeType($sql);
		return $result;
	}
	public function get_settled_amounts($invoiceNo,$invoiceYear,$executeType){
		
		$sql	= $this->get_settled_amounts_sql($invoiceNo,$invoiceYear);
		$row	= $this->get_row($sql,$executeType);
		return $row;

	}
	public function getBudgetAmount($month,$year,$location,$executeType)
	{
		return $this->getBudgetAmount_sql($month,$year,$location,$executeType);
	}
	public function getPettyCashMonthExpences($month,$year,$location,$executionType)
	{
		return $this->getPettyCashMonthExpences_sql($month,$year,$location,$executionType);
	}
	public function get_permision_approval_for_department_heads($serialNo,$serialYear,$department,$intUser,$executionType){
		$sql	= $this->get_department_head_for_invoice_sql($serialNo,$serialYear,$department,$intUser);
		$row	= $this->get_row($sql,$executionType);
		if($row['DEP_HEAD_ID'] != $intUser)
			$permision_confirm['permision']	= 0;
		else
			$permision_confirm['permision']	=1;
			
			return $permision_confirm;
		
	}
	public function getDepartmentAmounts($serialNo,$serialYear,$executionType)
	{
		return $this->getDepartmentAmounts_sql($serialNo,$serialYear,$executionType);
	}
	
	public function get_selectedDepartmentAmounts($serialNo,$serialYear,$department,$executionType)
	{
		return $this->get_selectedDepartmentAmounts_result($serialNo,$serialYear,$department,$executionType);
	}
	
	public function get_Departments($executionType)
	{
		$sql	= $this->get_Departments_sql();
		$row	= $this->get_row($sql,$executionType);
		$str	= $row['dep_string'];
		$str	= str_replace(",","",$str);
		$str	= $str.":All";
		return $str;
	}
	public function getEmployeeDepartment($requstBy,$HR_DB,$executionType)
	{
		return $this->getEmployeeDepartment_result($requstBy,$HR_DB,$executionType);
	}
//END 	- PUBLIC FUNCTIONS }

//BEGIN - PRIVATE FUNCTIONS {
	private function get_header_sql($serialNo,$serialYear,$main_DB,$hr_DB){
	
		$sql	= "
					SELECT
					FPI.SERIAL_NO			AS SERIAL_NO,
					FPI.SERIAL_YEAR			AS SERIAL_YEAR,
					FPI.SETTLEMENT_NO		AS SETTLEMENT_NO,
					FPI.SETTLEMENT_YEAR		AS SETTLEMENT_YEAR,
					FPI.PETTY_CASH_ITEM		AS PETTY_CASH_ITEM,
					DATE(FPI.DATE)			AS DATE,
					FPI.AMOUNT				AS AMOUNT,
					FPI.BALANCE_AMOUNT		AS BALANCE_AMOUNT,
					FPI.RECEIVE_AMOUNT		AS RECEIVE_AMOUNT,
					FPI.REQUEST_EMPLYEE		AS EMPLOYEE_ID,
					me.strEmployeeName		AS EMPLOYEE_NAME,
					FPI.`STATUS`			AS STATUS,
					FPI.APPROVE_LEVELS 		AS LEVELS,
					FPI.CREATED_DATE		AS CREATED_DATE,
					FPI.CREATED_BY			AS CREATED_BY,
					FPI.MODIFIED_DATE		AS MODIFIED_DATE,
					FPI.MODIFIED_BY			AS MODIFIED_BY,
					FPI.LOCATION_ID			AS LOCATION_ID,
					SU.strUserName 			AS `USER`,
					FMPI.`CODE` 			AS ITEM_CODE,
					FMPI.`NAME` 			AS ITEM_NAME,
					FMPC.`CODE` 			AS CATEGORY_CODE,
					FMPC.`NAME` 			AS CATEGORY,
					FMPI.CATEGORY_ID		AS ITEM_CATEGORY,
					FPI.REMARKS				AS REMARKS 
					FROM
					$main_DB.finance_pettycash_invoice as FPI
					INNER JOIN $main_DB.sys_users as SU ON FPI.CREATED_BY = SU.intUserId
					INNER JOIN $main_DB.finance_mst_pettycash_item as FMPI ON FPI.PETTY_CASH_ITEM = FMPI.ID
					INNER JOIN $main_DB.finance_mst_pettycash_category as FMPC ON FMPI.CATEGORY_ID = FMPC.ID
					INNER JOIN $hr_DB.mst_employee as me ON me.intEmployeeId = FPI.REQUEST_EMPLYEE 
					INNER JOIN $main_DB.mst_locations as L ON FPI.LOCATION_ID = L.intId
					WHERE
					FPI.SERIAL_NO = '$serialNo' AND
					FPI.SERIAL_YEAR = '$serialYear'";
		return $sql;		
	}
	
	private function get_header_department_wise_sql($serialNo,$serialYear,$department,$main_DB,$hr_DB){
	
		$sql	= "
					SELECT
					FPI.SERIAL_NO			AS SERIAL_NO,
					FPI.SERIAL_YEAR			AS SERIAL_YEAR,
					FPI.SETTLEMENT_NO		AS SETTLEMENT_NO,
					FPI.SETTLEMENT_YEAR		AS SETTLEMENT_YEAR,
					FPI.PETTY_CASH_ITEM		AS PETTY_CASH_ITEM,
					DATE(FPI.DATE)			AS DATE,
					FPD.AMOUNT				AS AMOUNT,
					FPI.REQUEST_EMPLYEE		AS EMPLOYEE_ID,
					me.strEmployeeName		AS EMPLOYEE_NAME,
					FPD.`DEP_APPROVAL_STATUS`			AS STATUS,
					1				 		AS LEVELS,
					FPI.CREATED_DATE		AS CREATED_DATE,
					FPI.CREATED_BY			AS CREATED_BY,
					FPI.MODIFIED_DATE		AS MODIFIED_DATE,
					FPI.MODIFIED_BY			AS MODIFIED_BY,
					FPI.LOCATION_ID			AS LOCATION_ID,
					SU.strUserName 			AS `USER`,
					FMPI.`CODE` 			AS ITEM_CODE,
					FMPI.`NAME` 			AS ITEM_NAME,
					FMPC.`CODE` 			AS CATEGORY_CODE,
					FMPC.`NAME` 			AS CATEGORY,
					FMPI.CATEGORY_ID		AS ITEM_CATEGORY,
					FPI.REMARKS				AS REMARKS 
					FROM 
					$main_DB.finance_pettycash_invoice_department as FPD
					INNER JOIN $main_DB.finance_pettycash_invoice as FPI ON FPI.SERIAL_NO = FPD.SERIAL_NO AND FPI.SERIAL_YEAR = FPD.SERIAL_YEAR
					INNER JOIN $main_DB.sys_users as SU ON FPI.CREATED_BY = SU.intUserId
					INNER JOIN $main_DB.finance_mst_pettycash_item as FMPI ON FPI.PETTY_CASH_ITEM = FMPI.ID
					INNER JOIN $main_DB.finance_mst_pettycash_category as FMPC ON FMPI.CATEGORY_ID = FMPC.ID
					INNER JOIN $hr_DB.mst_employee as me ON me.intEmployeeId = FPI.REQUEST_EMPLYEE 
					INNER JOIN $main_DB.mst_locations as L ON FPI.LOCATION_ID = L.intId
					WHERE
					FPI.SERIAL_NO = '$serialNo' AND
					FPI.SERIAL_YEAR = '$serialYear' AND 
					FPD.DEPARTMENT_ID = '$department'
					";
		return $sql;		
	}
	
	
	private function get_row($sql,$executeType){
	
		$result	= $this->db->$executeType($sql);
		$row=mysqli_fetch_array($result);
		return $row;		
	}
	
	private function Load_Report_approval_details_sql($serialYear,$serialNo){
	
		
	   	$sql = "SELECT
 				finance_pettycash_invoice_approve_by.APPROVE_DATE AS dtApprovedDate ,
				sys_users.strUserName as UserName,
				finance_pettycash_invoice_approve_by.APPROVE_LEVEL as intApproveLevelNo
				FROM
				finance_pettycash_invoice_approve_by
				Inner Join sys_users ON finance_pettycash_invoice_approve_by.APPROVE_BY = sys_users.intUserId
				WHERE
				finance_pettycash_invoice_approve_by.SERIAL_NO =  '$serialNo' AND
				finance_pettycash_invoice_approve_by.SERIAL_YEAR =  '$serialYear'      order by APPROVE_DATE asc";
				
 		return $sql;
	}

	private function get_dep_wise_report_approval_details_sql($serialYear,$serialNo,$department){
	
		
	   	$sql = "SELECT
 				finance_pettycash_invoice_department_approved_by.APPROVE_DATE AS dtApprovedDate ,
				sys_users.strUserName as UserName,
				finance_pettycash_invoice_department_approved_by.APPROVE_LEVEL as intApproveLevelNo
				FROM
				finance_pettycash_invoice_department_approved_by
				Inner Join sys_users ON finance_pettycash_invoice_department_approved_by.APPROVE_BY = sys_users.intUserId
				WHERE
				finance_pettycash_invoice_department_approved_by.SERIAL_NO =  '$serialNo' AND
				finance_pettycash_invoice_department_approved_by.SERIAL_YEAR =  '$serialYear'  AND
				finance_pettycash_invoice_department_approved_by.DEPARTMENT_ID =  '$department' AND 
				finance_pettycash_invoice_department_approved_by.STATUS = 0     
				order by APPROVE_DATE asc";
				
 		return $sql;
	}
	
	
	private function get_uploaded_invoice_count($serialNo,$serialYear,$path,$executeType){
		
	$flag	=0;
	$tdir	=$path."documents/finance/petty_cash/".$serialNo."-".$serialYear;
	
		$m=0;
		$dirs = scandir($tdir);
		foreach($dirs as $file)
		{
			
			if (($file == '.')||($file == '..'))
			{
			}
			else
			{
				
				if (! is_dir($tdir.'/'.$file))
				{
					$flag	= 1;
				}
			}
		}
	
	return $flag;
		
	}
	private function getPettyItem_sql($category,$executeType)
	{
		$sql = "SELECT ID AS itemId,
				NAME AS itemName
				FROM finance_mst_pettycash_item
				WHERE STATUS = 1 AND
				CATEGORY_ID	= '$category'
				ORDER BY NAME ";
		
		$result	= $this->db->$executeType($sql);
		return $result;
	}
	private function getPettyRequestedBy_sql($hrDB,$locationId,$executeType)
	{
		$sql = "SELECT QE.intEmployeeId,QE.strInitialName
				FROM $hrDB.mst_employee QE
				INNER JOIN $hrDB.mst_locations QML ON QML.intId=QE.intLocationId
				INNER JOIN mst_locations NML ON NML.PAYROLL_LOCATION=QML.intId
				WHERE QE.Active = 1 AND
				NML.intId = '$locationId'
				ORDER BY QE.strInitialName ";
		
		$result	= $this->db->$executeType($sql);
		return $result;
	}
	private function getMaxStatus_sql($invoiceNo,$invoiceYear)
	{
		$sql = "SELECT MAX(tb.STATUS) AS maxStatus  
				FROM finance_pettycash_invoice_approve_by AS tb
				WHERE 
				tb.SERIAL_NO='$invoiceNo' AND
				tb.SERIAL_YEAR='$invoiceYear' ";
		
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		return $row['maxStatus'];
	}
	private function getMaxStatus_dep_sql($invoiceNo,$invoiceYear)
	{
		$sql = "SELECT MAX(tb.STATUS) AS maxStatus  
				FROM finance_pettycash_invoice_department_approved_by AS tb
				WHERE 
				tb.SERIAL_NO='$invoiceNo' AND
				tb.SERIAL_YEAR='$invoiceYear' ";
		
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		return $row['maxStatus'];
	}
	
	
	private function get_max_approve_levels_sql(){
		$sql	="	SELECT
					Max(finance_pettycash_invoice.APPROVE_LEVELS) AS LEVELS
					FROM `finance_pettycash_invoice`";
		return $sql;
	}
	private function load_listing_sql($programCode,$approveLevel,$intUser,$main_DB,$hr_DB,$location,$where_string){

		$sql = "select * from(SELECT DISTINCT 
			if(tb1.STATUS=1,'Approved',if(tb1.STATUS=0,'Rejected',if(tb1.STATUS=-2,'Cancelled','Pending'))) as Status,
			CONCAT(tb1.SERIAL_NO,'/',tb1.SERIAL_YEAR )	AS CONCAT_SERIAL_NO,
			tb1.SERIAL_NO,
			tb1.SERIAL_YEAR,
			tb1.REQUEST_EMPLYEE							AS EMPLOYEE_ID,
			me.strInitialName							AS EMPLOYEE_NAME,
			tb1.DATE									AS DATE,
			ABS(tb1.AMOUNT) 									AS AMOUNT,
			ABS(tb1.RECEIVE_AMOUNT) 									AS RECEIVE_AMOUNT,
			
			SU.strUserName 			AS USER , 
			FMPI.NAME				AS ITEM_NAME,
			
			IFNULL((
											SELECT
				concat(sys_users.strUserName,'(',max(finance_pettycash_invoice_approve_by.APPROVE_DATE),')' )
				FROM
				finance_pettycash_invoice_approve_by
				Inner Join sys_users ON finance_pettycash_invoice_approve_by.APPROVE_BY = sys_users.intUserId
				WHERE
				finance_pettycash_invoice_approve_by.SERIAL_NO  = tb1.SERIAL_NO AND
				finance_pettycash_invoice_approve_by.SERIAL_YEAR =  tb1.SERIAL_YEAR AND
				finance_pettycash_invoice_approve_by.APPROVE_LEVEL = '1' AND 
				finance_pettycash_invoice_approve_by.STATUS = '0' AND  tb1.STATUS >0
			),IF(((SELECT
				menupermision.int1Approval 
				FROM menupermision 
				Inner Join menus ON menupermision.intMenuId = menus.intId
				WHERE
				menus.strCode = '$programCode' AND
				menupermision.intUserId =  '$intUser')=1 AND tb1.STATUS>1),'Approve', '')) as `1st_Approval`,  ";
			
		for($i=2; $i<=$approveLevel; $i++){
			if($i==2){
			$approval="2nd_Approval";
			}
			else if($i==3){
			$approval="3rd_Approval";
			}
			else {
			$approval=$i."th_Approval";
			}
			
			
		$sql .= "IFNULL(
		/*condition*/
	(
											SELECT
				concat(sys_users.strUserName,'(',max(finance_pettycash_invoice_approve_by.APPROVE_DATE),')' )
				FROM
				finance_pettycash_invoice_approve_by
				Inner Join sys_users ON finance_pettycash_invoice_approve_by.APPROVE_BY = sys_users.intUserId
				WHERE
				finance_pettycash_invoice_approve_by.SERIAL_NO  = tb1.SERIAL_NO AND
				finance_pettycash_invoice_approve_by.SERIAL_YEAR =  tb1.SERIAL_YEAR AND
				finance_pettycash_invoice_approve_by.APPROVE_LEVEL =  '$i' AND 
				finance_pettycash_invoice_approve_by.STATUS='0' 
			),
			/*end condition*/
			
			/*false part*/
	
			IF(
			/*if condition */
			((SELECT
				menupermision.int".$i."Approval 
				FROM menupermision 
				Inner Join menus ON menupermision.intMenuId = menus.intId
				WHERE
				menus.strCode = '$programCode' AND
				menupermision.intUserId =  '$intUser')=1 AND (tb1.STATUS>1) AND (tb1.STATUS<=tb1.APPROVE_LEVELS) AND ((SELECT
				max(finance_pettycash_invoice_approve_by.APPROVE_DATE) 
				FROM
				finance_pettycash_invoice_approve_by
				Inner Join sys_users ON finance_pettycash_invoice_approve_by.APPROVE_BY = sys_users.intUserId
				WHERE
				finance_pettycash_invoice_approve_by.SERIAL_NO = tb1.SERIAL_NO AND
				finance_pettycash_invoice_approve_by.SERIAL_YEAR =  tb1.SERIAL_YEAR AND
				finance_pettycash_invoice_approve_by.APPROVE_LEVEL =  ($i-1) AND 
				finance_pettycash_invoice_approve_by.STATUS='0' )<>'')),
				
				/*end if condition*/
				/*if true part*/
				'Approve',
				/*fase part*/
				 if($i>tb1.APPROVE_LEVELS,'-----',''))
				
				/*end IFNULL false part*/
				) as `".$approval."`, "; 
					
				}
			
			
				
			$sql .= "'View' as `View`   
			FROM
			finance_pettycash_invoice AS tb1 
			INNER JOIN $main_DB.sys_users as SU ON tb1.CREATED_BY = SU.intUserId
			INNER JOIN $main_DB.finance_mst_pettycash_item as FMPI ON tb1.PETTY_CASH_ITEM = FMPI.ID
			INNER JOIN $main_DB.finance_mst_pettycash_category as FMPC ON FMPI.CATEGORY_ID = FMPC.ID
			INNER JOIN $hr_DB.mst_employee as me ON me.intEmployeeId = tb1.REQUEST_EMPLYEE 
			INNER JOIN $main_DB.mst_locations as L ON tb1.LOCATION_ID = L.intId  
			WHERE tb1.LOCATION_ID ='$location'
			$where_string
			)  as t where 1=1
		";
		return $sql;
	}
	
	private function load_department_listing_sql($programCode,$approveLevel,$intUser,$main_DB,$hr_DB,$location){

		$sql = "select * from(SELECT DISTINCT 
			if(tb1.STATUS=1,'Approved',if(tb1.STATUS=0,'Rejected',if(tb1.STATUS=-2,'Cancelled','Pending'))) as Status,
			CONCAT(tb1.SERIAL_NO,'/',tb1.SERIAL_YEAR )	AS CONCAT_SERIAL_NO,
			tb1.SERIAL_NO,
			tb1.SERIAL_YEAR,
			tb1.REQUEST_EMPLYEE							AS EMPLOYEE_ID,
			me.strInitialName							AS EMPLOYEE_NAME,
			tb1.DATE									AS DATE,
			ABS(tb1.AMOUNT) 							AS AMOUNT,
			ABS(tb1.RECEIVE_AMOUNT) 							AS RECEIVE_AMOUNT,
			
			SU.strUserName 								AS USER , 
			FMPI.NAME									AS ITEM_NAME,
			mst_department.strName						AS DEPARTMENT ,
			PID.DEPARTMENT_ID 							AS DEPARTMENT_ID, 
			IFNULL((
											SELECT
				concat(sys_users.strUserName,'(',max(finance_pettycash_invoice_department_approved_by.APPROVE_DATE),')' )
				FROM
				finance_pettycash_invoice_department_approved_by
				Inner Join sys_users ON finance_pettycash_invoice_department_approved_by.APPROVE_BY = sys_users.intUserId
				WHERE
				finance_pettycash_invoice_department_approved_by.SERIAL_NO  = PID.SERIAL_NO AND
				finance_pettycash_invoice_department_approved_by.SERIAL_YEAR =  PID.SERIAL_YEAR AND
				finance_pettycash_invoice_department_approved_by.DEPARTMENT_ID =  PID.DEPARTMENT_ID AND
				finance_pettycash_invoice_department_approved_by.APPROVE_LEVEL = '1' AND 
				finance_pettycash_invoice_department_approved_by.STATUS = '0' AND  tb1.STATUS >0
			),IF(((SELECT
				menupermision.int1Approval 
				FROM menupermision 
				Inner Join menus ON menupermision.intMenuId = menus.intId
				WHERE
				menus.strCode = '$programCode' AND
				menupermision.intUserId =  '$intUser')=1 AND tb1.STATUS>1),'Approve', '')) as `1st_Approval`,  ";
			
		for($i=2; $i<=$approveLevel; $i++){
			if($i==2){
			$approval="2nd_Approval";
			}
			else if($i==3){
			$approval="3rd_Approval";
			}
			else {
			$approval=$i."th_Approval";
			}
			
			
		$sql .= "IFNULL(
		/*condition*/
	(
											SELECT
				concat(sys_users.strUserName,'(',max(finance_pettycash_invoice_department_approved_by.APPROVE_DATE),')' )
				FROM
				finance_pettycash_invoice_department_approved_by
				Inner Join sys_users ON finance_pettycash_invoice_department_approved_by.APPROVE_BY = sys_users.intUserId
				WHERE
				finance_pettycash_invoice_department_approved_by.SERIAL_NO  = PID.SERIAL_NO AND
				finance_pettycash_invoice_department_approved_by.SERIAL_YEAR =  PID.SERIAL_YEAR AND
				finance_pettycash_invoice_department_approved_by.DEPARTMENT_ID =  PID.DEPARTMENT_ID AND
				finance_pettycash_invoice_department_approved_by.APPROVE_LEVEL =  '$i' AND 
				finance_pettycash_invoice_department_approved_by.STATUS='0' 
			),
			/*end condition*/
			
			/*false part*/
	
			IF(
			/*if condition */
			((SELECT
				menupermision.int".$i."Approval 
				FROM menupermision 
				Inner Join menus ON menupermision.intMenuId = menus.intId
				WHERE
				menus.strCode = '$programCode' AND
				menupermision.intUserId =  '$intUser')=1 AND (tb1.STATUS>1) AND (tb1.STATUS<=tb1.APPROVE_LEVELS) AND ((SELECT
				max(finance_pettycash_invoice_department_approved_by.APPROVE_DATE) 
				FROM
				finance_pettycash_invoice_department_approved_by
				Inner Join sys_users ON finance_pettycash_invoice_department_approved_by.APPROVE_BY = sys_users.intUserId
				WHERE
				finance_pettycash_invoice_department_approved_by.SERIAL_NO = PID.SERIAL_NO AND
				finance_pettycash_invoice_department_approved_by.SERIAL_YEAR =  PID.SERIAL_YEAR AND
				finance_pettycash_invoice_department_approved_by.DEPARTMENT_ID =  PID.DEPARTMENT_ID AND
				finance_pettycash_invoice_department_approved_by.APPROVE_LEVEL =  ($i-1) AND 
				finance_pettycash_invoice_department_approved_by.STATUS='0' )<>'')),
				
				/*end if condition*/
				/*if true part*/
				'Approve',
				/*fase part*/
				 if($i>tb1.APPROVE_LEVELS,'-----',''))
				
				/*end IFNULL false part*/
				) as `".$approval."`, "; 
					
				}
			
			
				
			$sql .= "'View' as `View`   
			FROM
			finance_pettycash_invoice AS tb1  
			INNER JOIN $main_DB.finance_pettycash_invoice_department AS PID  ON tb1.SERIAL_NO = PID.SERIAL_NO AND  tb1.SERIAL_YEAR = PID.SERIAL_YEAR 
			INNER JOIN $main_DB.sys_users as SU ON tb1.CREATED_BY = SU.intUserId
			INNER JOIN $main_DB.finance_mst_pettycash_item as FMPI ON tb1.PETTY_CASH_ITEM = FMPI.ID
			INNER JOIN $main_DB.finance_mst_pettycash_category as FMPC ON FMPI.CATEGORY_ID = FMPC.ID
			INNER JOIN $hr_DB.mst_employee as me ON me.intEmployeeId = tb1.REQUEST_EMPLYEE 
			INNER JOIN $main_DB.mst_locations as L ON tb1.LOCATION_ID = L.intId  
			INNER JOIN mst_department ON PID.DEPARTMENT_ID = mst_department.intId
			
			WHERE tb1.LOCATION_ID ='$location'
			)  as t where 1=1
		";
		return $sql;
	}
	
	
	
	private function getStatus_sql($serialNo,$serialYear)
	{
		$sql = "SELECT STATUS,APPROVE_LEVELS
				FROM finance_pettycash_invoice
				WHERE SERIAL_NO = '$serialNo' AND
				SERIAL_YEAR = '$serialYear' ";
		
		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		return $row;
		
	}
	
	private function get_IOU_invoices_sql($settleNo,$settleYear){
		$sql	="
				SELECT
				CONCAT(FPI.SERIAL_NO,'/',FPI.SERIAL_YEAR) as ID,
				CONCAT('(',FPI.SERIAL_NO,'/',FPI.SERIAL_YEAR,')',FMPC.`NAME`,'/',FMPI.`NAME`,'=>',ABS(FPI.BALANCE_AMOUNT)) as NAME, 
				FPI.SERIAL_NO,
				FPI.SERIAL_YEAR,
				FMPI.`NAME` 			AS ITEM_NAME,
				FMPC.`NAME` 			AS CATEGORY,
				FPI.AMOUNT, 
				FPI.RECEIVE_AMOUNT, 
				(
					SELECT
					IFNULL(Sum(finance_pettycash_invoice.AMOUNT),0)
					FROM `finance_pettycash_invoice`
					WHERE
					finance_pettycash_invoice.`STATUS` = 1 AND
					finance_pettycash_invoice.SETTLEMENT_NO = FPI.SERIAL_NO AND
					finance_pettycash_invoice.SETTLEMENT_YEAR = FPI.SERIAL_YEAR 
				) as SETTLED_AMOUNT 
				FROM `finance_pettycash_invoice` as FPI 
				INNER JOIN finance_mst_pettycash_item as FMPI ON FPI.PETTY_CASH_ITEM = FMPI.ID
				INNER JOIN finance_mst_pettycash_category as FMPC ON FMPI.CATEGORY_ID = FMPC.ID
				WHERE
				FPI.STATUS = 1 AND 
				FPI.PETTY_CASH_ITEM = '11' AND 
				(FPI.SETTLEMENT_NO = 0 OR FPI.SETTLEMENT_NO IS NULL) 
				AND  
				(BALANCE_AMOUNT) < 0 
				";
				
				if($settleNo > 0){
				$sql_1	="
						SELECT
						CONCAT(FPI.SERIAL_NO,'/',FPI.SERIAL_YEAR) as ID,
						CONCAT('(',FPI.SERIAL_NO,'/',FPI.SERIAL_YEAR,')',FMPC.`NAME`,'/',FMPI.`NAME`,'=>',ABS(FPI.AMOUNT)) as NAME, 
						FPI.SERIAL_NO,
						FPI.SERIAL_YEAR,
						FMPI.`NAME` 			AS ITEM_NAME,
						FMPC.`NAME` 			AS CATEGORY,
						FPI.AMOUNT, 
						FPI.RECEIVE_AMOUNT, 
						(
							SELECT
							IFNULL(Sum(finance_pettycash_invoice.AMOUNT),0)
							FROM `finance_pettycash_invoice`
							WHERE
							finance_pettycash_invoice.`STATUS` = 1 AND
							finance_pettycash_invoice.SETTLEMENT_NO = FPI.SERIAL_NO AND
							finance_pettycash_invoice.SETTLEMENT_YEAR = FPI.SERIAL_YEAR 
						) as SETTLED_AMOUNT 
						FROM `finance_pettycash_invoice` as FPI 
						INNER JOIN finance_mst_pettycash_item as FMPI ON FPI.PETTY_CASH_ITEM = FMPI.ID
						INNER JOIN finance_mst_pettycash_category as FMPC ON FMPI.CATEGORY_ID = FMPC.ID
						WHERE
						FPI.STATUS = 1 AND 
						FPI.PETTY_CASH_ITEM = '11' AND 
						(FPI.SERIAL_NO = '$settleNo' AND FPI.SERIAL_YEAR ='$settleYear') 
						
						";
					$sql_2=" Select * from ((".$sql.") UNION (".$sql_1.")) as TB ";
					$sql	= $sql_2;
					
					
				}
		return $sql;
		
	}
	
	private function getItemAndCategory_sql($serialNo,$serialYear){
		$sql	= "
					SELECT 
					finance_pettycash_invoice.REQUEST_EMPLYEE,
					finance_mst_pettycash_item.CATEGORY_ID as CATEGORY_ID,
					finance_pettycash_invoice.PETTY_CASH_ITEM AS ITEM_ID 
					FROM
					finance_pettycash_invoice
					INNER JOIN finance_mst_pettycash_item ON finance_pettycash_invoice.PETTY_CASH_ITEM = finance_mst_pettycash_item.ID
					WHERE
					finance_pettycash_invoice.SERIAL_NO = '$serialNo' AND
					finance_pettycash_invoice.SERIAL_YEAR = '$serialYear'";
		
		return $sql;
	}
	private function get_settled_amounts_sql($serialNo,$serialYear){
		$sql	=" 
				SELECT
				CONCAT(FPI.SERIAL_NO,'/',FPI.SERIAL_YEAR) as ID,
				CONCAT('(',FPI.SERIAL_NO,'/',FPI.SERIAL_YEAR,')',FMPC.`NAME`,'/',FMPI.`NAME`,'=>',FPI.AMOUNT) as NAME, 
				FPI.SERIAL_NO,
				FPI.SERIAL_YEAR,
				FMPI.`NAME` 			AS ITEM_NAME,
				FMPC.`NAME` 			AS CATEGORY,
				FPI.AMOUNT, 
				FPI.RECEIVE_AMOUNT,
				FPI.BALANCE_AMOUNT, 
				(
					SELECT
					IFNULL(Sum(finance_pettycash_invoice.AMOUNT),0)
					FROM `finance_pettycash_invoice`
					WHERE
					finance_pettycash_invoice.`STATUS` = 1 AND
					finance_pettycash_invoice.SETTLEMENT_NO = FPI.SERIAL_NO AND
					finance_pettycash_invoice.SETTLEMENT_YEAR = FPI.SERIAL_YEAR 
				) as SETTLED_AMOUNT 
				FROM `finance_pettycash_invoice` as FPI 
				INNER JOIN finance_mst_pettycash_item as FMPI ON FPI.PETTY_CASH_ITEM = FMPI.ID
				INNER JOIN finance_mst_pettycash_category as FMPC ON FMPI.CATEGORY_ID = FMPC.ID
				WHERE 
				 FPI.SERIAL_NO = '$serialNo' AND 
				 FPI.SERIAL_YEAR = '$serialYear' /*AND 

				FPI.STATUS = 1 	*/	
				HAVING 
				SETTLED_AMOUNT = 0		";
		
		return $sql;		
	}
	private function getBudgetAmount_sql($month,$year,$location,$executeType)
	{
		$sql = "SELECT IFNULL(AMOUNT,0) AS budgetAmount
				FROM finance_pettycash_budget_details
				WHERE
				YEAR = '$year' AND
				MONTH = '$month' AND
				LOCATION_ID = '$location' ";
		
		$result = $this->db->$executeType($sql);
		$row	= mysqli_fetch_array($result);
		return $row['budgetAmount'];
	}
	private function getPettyCashMonthExpences_sql($month,$year,$location,$executionType)
	{
		$sql = "SELECT
				COALESCE(SUM(AMOUNT),0) AS EXPENCES
				FROM finance_pettycash_invoice I
				INNER JOIN finance_mst_pettycash_item PI ON PI.ID = I.PETTY_CASH_ITEM
				WHERE I.STATUS = 1 
				AND PI.CODE <> 'PCR'
				AND MONTH(I.DATE) = '$month'
				AND YEAR(I.DATE) = '$year'
				AND I.LOCATION_ID = '$location' ";
		
		$result = $this->db->$executionType($sql);
		$row	= mysqli_fetch_array($result);
		return $row['EXPENCES'];
	}
 	
	private function get_department_head_for_invoice_sql($serialNo,$serialYear,$department,$intUser){
			$sql	="
						SELECT
						mst_department.strName,
						finance_pettycash_invoice_department.AMOUNT,
						mst_department_heads.DEP_HEAD_ID
						FROM
						finance_pettycash_invoice
						INNER JOIN finance_pettycash_invoice_department ON finance_pettycash_invoice.SERIAL_NO = finance_pettycash_invoice_department.SERIAL_NO AND finance_pettycash_invoice.SERIAL_YEAR = finance_pettycash_invoice_department.SERIAL_YEAR
						INNER JOIN mst_department_heads ON finance_pettycash_invoice_department.DEPARTMENT_ID = mst_department_heads.DEPARTMENT_ID
						INNER JOIN mst_department ON mst_department_heads.DEPARTMENT_ID = mst_department.intId
						WHERE
						mst_department_heads.DEP_HEAD_ID = '$intUser' AND
						finance_pettycash_invoice.SERIAL_NO = '$serialNo' AND
						finance_pettycash_invoice.SERIAL_YEAR = '$serialYear' AND
						finance_pettycash_invoice_department.DEPARTMENT_ID = '$department'  AND
						finance_pettycash_invoice_department.DEP_APPROVAL_STATUS <> '1'
						GROUP BY
						mst_department_heads.DEPARTMENT_ID,
						mst_department_heads.DEP_HEAD_ID			";
			return $sql;
	}
 	private function getDepartmentAmounts_sql($serialNo,$serialYear,$executionType)
	{
		$sql = "SELECT DEPARTMENT_ID,AMOUNT,mst_department.strName AS department
				FROM finance_pettycash_invoice_department
				LEFT JOIN mst_department ON mst_department.intId=finance_pettycash_invoice_department.DEPARTMENT_ID
				WHERE 
				SERIAL_NO = '$serialNo' AND
				SERIAL_YEAR = '$serialYear'
				ORDER BY ORDER_BY ";
		
		$result	= $this->db->$executionType($sql);
		return $result;
	}
 	private function get_selectedDepartmentAmounts_result($serialNo,$serialYear,$department,$executionType)
	{
		$sql = "SELECT
				mst_department.strName,
				finance_pettycash_invoice_department.AMOUNT,
				mst_department_heads.DEP_HEAD_ID
				FROM
				finance_pettycash_invoice
				INNER JOIN finance_pettycash_invoice_department ON finance_pettycash_invoice.SERIAL_NO = finance_pettycash_invoice_department.SERIAL_NO AND finance_pettycash_invoice.SERIAL_YEAR = finance_pettycash_invoice_department.SERIAL_YEAR
				INNER JOIN mst_department_heads ON finance_pettycash_invoice_department.DEPARTMENT_ID = mst_department_heads.DEPARTMENT_ID
				INNER JOIN mst_department ON mst_department_heads.DEPARTMENT_ID = mst_department.intId
				WHERE
				 
				finance_pettycash_invoice.SERIAL_NO = '$serialNo' AND
				finance_pettycash_invoice.SERIAL_YEAR = '$serialYear' AND
				finance_pettycash_invoice_department.DEPARTMENT_ID = '$department'
				GROUP BY
				mst_department_heads.DEPARTMENT_ID";
		
		$result	= $this->db->$executionType($sql);
		return $result;
	}
	private function getBudgetCategoryDetail_sql($itemId,$location,$departmentId,$executionType)
	{
		$sql = "SELECT BC.BUDGET_TYPE,
				BC.ITEM_ID,
				MI.strName AS item
				FROM finance_mst_pettycash_item PI
				INNER JOIN mst_budget_category_list BC ON BC.ITEM_ID=PI.ITEM_ID
				INNER JOIN mst_item MI ON MI.intId = BC.ITEM_ID
				INNER JOIN mst_budget_category_list_department_wise BCD ON BC.SERIAL_ID = BCD.SERIAL_ID
				WHERE 
				PI.ID = '$itemId' AND
				BC.LOCATION_ID = '$location' AND 
				BCD.DEPARTMENT_ID = '$departmentId'";
		
		$result	= $this->db->$executionType($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row;
	}
	private function getFinanceYearId_sql($year,$month,$executionType)
	{
		$sql = "SELECT intId
				FROM mst_financeaccountingperiod
				WHERE DATE('$year-$month-01') BETWEEN (dtmStartingDate) AND (dtmClosingDate)
				AND intStatus = 1 ";
	
		$result	= $this->db->$executionType($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row['intId'];
	}
	private function get_department_approvl_response($serialNo,$serialYear,$path,$executeType){
		
		$flag	=0;
		$str	='';
		$sql 	= "SELECT DEPARTMENT_ID,
				AMOUNT, 
				DEP_APPROVAL_STATUS ,
				mst_department.strName 
				FROM finance_pettycash_invoice_department
				INNER JOIN mst_department ON finance_pettycash_invoice_department.DEPARTMENT_ID = mst_department.intId
				WHERE 
				SERIAL_NO = '$serialNo' AND
				SERIAL_YEAR = '$serialYear'
				ORDER BY ORDER_BY ";
		
		$result	= $this->db->$executeType($sql);
		while($row	= mysqli_fetch_array($result)){
			if($row['DEP_APPROVAL_STATUS'] != 1){
				$flag	=1;
				$str	.=$row['strName'].' ,';
			}
		}
		
		$str	=substr($str,0,-1);
		if($flag	== 1){
			$msg	= "Approval not raised by Departments $str";
			$res['msg']		=$msg;
			$res['type']	='fail';
		}
		
		return $res;
		
	}
	
	private function get_Departments_sql(){
		
		$sql	="SELECT
			GROUP_CONCAT(mst_department.strName,':',mst_department.strName,';') as dep_string
			FROM `mst_department`
			WHERE
			mst_department.intStatus = 1
			ORDER BY
			mst_department.strName ASC";
		return $sql;
	}
	private function getEmployeeDepartment_result($requstBy,$HR_DB,$executionType)
	{
		$sql = "SELECT MD.intId AS deptId
				FROM $HR_DB.mst_employee QME
				LEFT JOIN mst_department MD ON MD.PAYROLL_DEPARTMENT=QME.intDivisionId
				WHERE QME.intEmployeeId = '$requstBy' ";
		
		$result	= $this->db->$executionType($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row['deptId'];
	}
  //END 	- PRIVATE FUNCTIONS }
}
?>
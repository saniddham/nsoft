<?php
class cls_petty_cash_get
{
	private $db;
		
	function __construct($db)
	{
		$this->db = $db;	
	}
	
//BEGIN - PUBLIC FUNCTIONS {
	public function getHeaderPopupDetails($itemId,$date,$hrDB)
	{
		return $this->getHeaderPopupDetails_sql($itemId,$date,$hrDB);
	}
	
	public function getOpenBalance($date,$location,$executionType)
	{
		return $this->getOpenBalance_sql($date,$location,$executionType);
	}
	
	public function getPettyCashBalance($month,$year,$location,$executionType)
	{
		return $this->getPettyCashBalance_sql($month,$year,$location,$executionType);
	}
	
	public function getPettyCashExpences($date,$location,$executionType)
	{
		return $this->getPettyCashExpences_sql($date,$location,$executionType);
	}
	
	public function getDataPeriodOpenBalance($dateFrom,$dateTo,$location,$id,$executionType)
	{
		return $this->getDataPeriodOpenBalance_sql($dateFrom,$dateTo,$location,$id,$executionType);
	}
	
	public function getMonthlyIncome($year,$month,$location)
	{
		return $this->getMonthlyIncome_sql($year,$month,$location);
	}
//END 	- PUBLIC FUNCTIONS }

//BEGIN - PRIVATE FUNCTIONS {
	private function getHeaderPopupDetails_sql($itemId,$date,$hrDB)
	{
		$sql = "SELECT
				finance_pettycash_invoice_detail.PETTY_INVOICE_NO AS SERIAL_NO,
				finance_pettycash_invoice_detail.PETTY_INVOICE_YEAR AS SERIAL_YEAR,
				CONCAT(finance_pettycash_invoice_detail.PETTY_INVOICE_NO,'/',finance_pettycash_invoice_detail.PETTY_INVOICE_YEAR) AS CONCAT_SERIAL,
				ROUND(abs(finance_pettycash_invoice_detail.INVOICE_AMOUNT)) AS AMOUNT,
				finance_pettycash_invoice_header.REMARKS							AS REMARKS,
     			CONCAT(E.strInitialName,' [',IFNULL(E.strCallingName,' '),']') AS EMPLOYEE_NAME,
  				E.Photo          					AS PHOTO_URL
				FROM finance_pettycash_invoice_detail
				INNER JOIN $hrDB.mst_employee E
   					ON E.intEmployeeId = finance_pettycash_invoice_detail.REQUEST_USER_ID
				INNER JOIN finance_pettycash_invoice_header ON finance_pettycash_invoice_detail.PETTY_INVOICE_NO = finance_pettycash_invoice_header.PETTY_INVOICE_NO AND finance_pettycash_invoice_detail.PETTY_INVOICE_YEAR = finance_pettycash_invoice_header.PETTY_INVOICE_YEAR
				WHERE finance_pettycash_invoice_detail.PETTY_CASH_ITEM_ID = '$itemId' AND 
				finance_pettycash_invoice_header.DATE = '$date' AND
				finance_pettycash_invoice_header.`STATUS` = 1";
		return $this->db->RunQuery($sql);
	}
	
	private function getOpenBalance_sql($date,$location,$executionType)
	{
		if($location != "")
			$para = "AND I.LOCATION_ID = '$location' ";
			
		$sql = "SELECT (INCOME + EXPENCES) AS AMOUNT
				FROM
				(SELECT
				  COALESCE(SUM(AMOUNT),0) AS INCOME
				FROM finance_pettycash_invoice I
				  INNER JOIN finance_mst_pettycash_item PI
					ON PI.ID = I.PETTY_CASH_ITEM
				WHERE I.STATUS = 1 
					AND PI.CODE = 'PCR'
					AND DATE(I.DATE) < '$date'
					$para) AS INCOME,
					(SELECT
				  COALESCE(SUM(BALANCE_AMOUNT),0) AS EXPENCES
				FROM finance_pettycash_invoice I
				  INNER JOIN finance_mst_pettycash_item PI
					ON PI.ID = I.PETTY_CASH_ITEM
				WHERE I.STATUS = 1 
					AND PI.CODE <> 'PCR'
					AND DATE(I.DATE) < '$date'
					$para) AS EXPENCES;";//echo $sql;
		$result = $this->db->$executionType($sql);
		$row	= mysqli_fetch_array($result);
		return ($row["AMOUNT"]==''?0:$row["AMOUNT"]);
	}
	
	private function getPettyCashBalance_sql($month,$year,$location,$executionType)
	{
		$sql = "SELECT (INCOME + EXPENCES) AS AMOUNT
				FROM
				(SELECT
				  COALESCE(SUM(BALANCE_AMOUNT),0) AS INCOME
				FROM finance_pettycash_invoice I
				  INNER JOIN finance_mst_pettycash_item PI
					ON PI.ID = I.PETTY_CASH_ITEM
				WHERE I.STATUS = 1 
					AND PI.CODE = 'PCR' 
					AND MONTH(I.DATE) = '$month'
					AND YEAR(I.DATE) = '$year'
					AND I.LOCATION_ID = '$location') AS INCOME,
					(SELECT
				  COALESCE(SUM(BALANCE_AMOUNT),0) AS EXPENCES
				FROM finance_pettycash_invoice I
				  INNER JOIN finance_mst_pettycash_item PI
					ON PI.ID = I.PETTY_CASH_ITEM
				WHERE I.STATUS = 1 
					AND PI.CODE <> 'PCR'
					AND MONTH(I.DATE) = '$month'
					AND YEAR(I.DATE) = '$year'
					AND I.LOCATION_ID = '$location') AS EXPENCES";
		//echo $sql;
		$result = $this->db->$executionType($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row["AMOUNT"];
	}
	
	private function getPettyCashExpences_sql($date,$location,$executionType)
	{
		$sql = "SELECT
				  COALESCE(ABS(SUM(BALANCE_AMOUNT)),0) AS EXPENCES
				FROM finance_pettycash_invoice I
				  INNER JOIN finance_mst_pettycash_item PI
					ON PI.ID = I.PETTY_CASH_ITEM
				WHERE I.STATUS = 1 
					AND PI.CODE <> 'PCR'
					AND DATE(I.DATE) = '$date'
					AND I.LOCATION_ID = '$location'";
		$result = $this->db->$executionType($sql);
		$row	= mysqli_fetch_array($result);		
		return $row["EXPENCES"];
	}
	
	private function getDataPeriodOpenBalance_sql($dateFrom,$dateTo,$location,$id,$executionType)
	{
		if($location != "")
			$para = "AND I.LOCATION_ID = '$location' ";
			
		$sql = "SELECT
				  COALESCE(SUM(BALANCE_AMOUNT),0) AS AMOUNT
				FROM finance_pettycash_invoice I
				  INNER JOIN finance_mst_pettycash_item PI
					ON PI.ID = I.PETTY_CASH_ITEM
				WHERE I.STATUS = 1 
					AND PI.CODE <> 'PCR'
					AND DATE(I.DATE) > '$dateFrom'
					AND DATE(I.DATE) < '$dateTo'
					AND I.PETTY_CASH_ITEM = $id
					$para;";//echo $sql;
		$result = $this->db->$executionType($sql);
		$row	= mysqli_fetch_array($result);
		return ($row["AMOUNT"]==''?0:$row["AMOUNT"]);
	}
	
	private function getMonthlyIncome_sql($year,$month,$location)
	{
		if($location != "")
				$para = "AND I.LOCATION_ID = '$location' ";
				
		$sql = "SELECT
				  COALESCE(SUM(AMOUNT),0)	AS AMOUNT
				FROM finance_pettycash_invoice I
				  INNER JOIN finance_mst_pettycash_item PI
					ON PI.ID = I.PETTY_CASH_ITEM
				WHERE I.STATUS = 1 
					AND PI.CODE = 'PCR'
					AND YEAR(I.DATE) = '$year'
					AND MONTH(I.DATE) = '$month'
					$para ";
		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		return $row["AMOUNT"];
	}
//END 	- PRIVATE FUNCTIONS }
}
?>
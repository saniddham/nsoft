<?php
class Cls_Plan_Common_Get
{
	private $db;
	
	function __construct($db)
	{
		$this->db 	= $db;
	}
	public function getPrinterMethodCombo($type)
	{		
		$sql = $this->printMethod_sql();
		$html = $this->printMethod_combo($sql,$type);
		return $html;
	}
	public function getPrinterMethodGridCombo()
	{		
		$sql = $this->printMethod_sql();
		$html = $this->printMethodGrid_combo($sql);
		return $html;
	}
	public function getLocation($userId,$location)
	{
		$sql = $this->getLocation_sql($userId);
		$html = $this->getLocation_combo($sql,$location);
		return $html;
	}
	public function getPlateSizeCombo()
	{
		$sql = $this->getPlateSize_sql();
		$html = $this->getPlateSize_combo($sql);
		return $html;
	}
	public function getPlateSizeGridCombo()
	{
		$sql = $this->getPlateSize_sql();
		$html = $this->getPlateSizeGrid_combo($sql);
		return $html;
	}
	private function getLocation_sql($userId)
	{
		$sql = "SELECT
				mst_locations_user.intLocationId,
				CONCAT(mst_companies.strName ,\" - \",mst_locations.strName) AS companyName
				FROM
				mst_locations_user
				INNER JOIN mst_locations ON mst_locations.intId = mst_locations_user.intLocationId
				INNER JOIN mst_companies ON mst_locations.intCompanyId = mst_companies.intId
				WHERE
				mst_locations_user.intUserId = '$userId' AND 
				mst_locations.intStatus = '1'
				ORDER BY mst_companies.strName";
		return $sql;
	}
	private function getLocation_combo($sql,$location)
	{
		$result = $this->db->RunQuery($sql);
		$string ="";
		while($row = mysqli_fetch_array($result))
		{
			if($row['intLocationId']==$location)
				$string.="<option value=\"".$row['intLocationId']."\" selected=\"selected\">".$row['companyName']."</option>";
			else
				$string.="<option value=\"".$row['intLocationId']."\">".$row['companyName']."</option>";
		}
		return $string;
	}
	private function printMethod_sql()
	{
		$sql = "SELECT 	intId, strName
				FROM mst_printertypes 
				WHERE intStatus = 1
				ORDER BY strName ";
 		return $sql;
	}
	private function printMethod_combo($sql,$type)
	{
		$result = $this->db->RunQuery($sql);
		if($type=='search')
			$string ="";
		else
			$string ="<option value=\"\"></option>";
			
		while($row = mysqli_fetch_array($result))
		{
			if($row['intId']==$printMethod)
				$string.="<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";
			else
				$string.="<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		return $string;
	}
	private function printMethodGrid_combo($sql)
	{
		$result = $this->db->RunQuery($sql);
		$string = "<select name=\"cboManuPrintMethod\" id=\"cboManuPrintMethod\" style=\"width:100%\" class=\"clsPrintMethod\">";
		$string .= "<option value=\"\"></option>";
		while($row = mysqli_fetch_array($result))
		{
			$string.="<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		$string .="</select>";
		
		return $string;
	}
	private function getPlateSize_sql()
	{
		$sql = "SELECT intId,strName
				FROM mst_printsizes
				WHERE intStatus = 1 
				ORDER BY strName";
		return $sql;
	}
	private function getPlateSize_combo($sql)
	{
		$result = $this->db->RunQuery($sql);
		$string ="<option value=\"\"></option>";
		while($row = mysqli_fetch_array($result))
		{
			$string.="<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		return $string;
	}
	private function getPlateSizeGrid_combo($sql)
	{
		$result = $this->db->RunQuery($sql);
		$string = "<select name=\"cboPlateSize\" id=\"cboPlateSize\" style=\"width:100%\" class=\"clsPlateSize\" disabled=\"disabled\">";
		$string .="<option value=\"\"></option>";
		while($row = mysqli_fetch_array($result))
		{
			$string.="<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		$string .="</select>";
		
		return $string;
	}
}
?>
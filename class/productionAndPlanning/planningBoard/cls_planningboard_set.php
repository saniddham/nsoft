<?php
class Cls_PlanningBoard_Set
{
	private $db;
	private $companyId;
	private $locationId;
	private $userId;

	function __construct($db,$locationId,$companyId,$userId)
	{
		$this->db 				= $db;
		$this->locationId 		= $locationId;
		$this->companyId 		= $companyId;
		$this->userId 			= $userId;
		
	}
	public function saveOrderBookHeaderData($customerId,$orderNo,$orderYear,$salesOrderId,$printMethod,$plateSize,$qty,$colors,$strocks,$panels,$marketerId,$planocation)
	{
		$sql = "INSERT INTO planning_plan_orders_header 
				(
				MARKETER_ID,
				CUSTOMER_ID, 
				ORDER_NO, 
				ORDER_YEAR, 
				SALES_ORDER_ID, 
				QTY, 
				COLORS, 
				STROCKS, 
				PANELS, 
				LOCATION_ID, 
				PRINTING_METHOD_ID,
				PLATE_SIZE_ID,
				CREATED_BY,
				CREATED_DATE
				)
				VALUES
				(
				$marketerId,
				$customerId, 
				$orderNo, 
				$orderYear, 
				$salesOrderId, 
				$qty, 
				$colors, 
				$strocks, 
				$panels, 
				'$planocation', 
				$printMethod,
				$plateSize,
				'".$this->userId."',
				NOW()
				)";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		$data['planId']				= $this->db->insertId;

		return $data;	
	}
	public function saveOrderBookManualData($planId,$PONo,$salesOrderNo,$style,$brand,$graphic,$combo,$psdDate)
	{
		$sql = "INSERT INTO planning_manual_orders 
				(
				PLAN_ID, 
				PO_NO, 
				SALES_ORDER_NO, 
				STYLE_NO, 
				GRAPHIC_NO,
				COMBO, 
				BRAND,
				PSD_DATE
				)
				VALUES
				(
				'$planId', 
				'$PONo', 
				'$salesOrderNo', 
				'$style', 
				'$graphic',
				'$combo', 
				'$brand',
				'$psdDate'
				) ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	public function updateOrderBookManualStatus($planId)
	{
		$sql = "UPDATE planning_plan_orders_header 
				SET
				MANUAL_ORDER = '1'
				WHERE
				PLAN_ID = '$planId' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	public function delete($planId)
	{
		$chkStatus = true;
		
		$sqlM 	= " DELETE FROM planning_manual_orders 
					WHERE
					PLAN_ID = '$planId' ";
		
		$resultM = $this->db->RunQuery2($sqlM);
		if(!$resultM && ($chkStatus))
		{
			$chkStatus				= false;
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sqlM;
		}
		
		$sqlD	= " DELETE FROM planning_plan_orders_details 
					WHERE
					PLAN_ID = '$planId' ";
		
		$resultD = $this->db->RunQuery2($sqlD);
		if(!$resultD && ($chkStatus))
		{
			$chkStatus				= false;
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sqlD;
		}
		
		$sqlH 	= " DELETE FROM planning_plan_orders_header 
					WHERE
					PLAN_ID = '$planId' ";
		
		$resultH = $this->db->RunQuery2($sqlH);
		if(!$resultH && ($chkStatus))
		{
			$chkStatus				= false;
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sqlH;
		}
		return $data;
	}
	public function deletePlanDetail($planId,$date,$typeId)
	{
		$sql = "DELETE FROM planning_plan_orders_details 
				WHERE
				PLAN_ID = '$planId' AND 
				DATE = '$date' AND 
				QTY_TYPE_ID = '$typeId' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	public function savePlan($planId,$date,$typeId,$qty)
	{
		$sql = "INSERT INTO planning_plan_orders_details 
				(
				PLAN_ID, 
				DATE, 
				QTY_TYPE_ID, 
				QTY
				)
				VALUES
				(
				'$planId', 
				'$date', 
				'$typeId', 
				'$qty'
				)";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	public function saveManualOrderAllocation($planId,$orderYear,$orderNo,$salesOrderId)
	{
		$sql = "UPDATE planning_plan_orders_header 
				SET
				ORDER_NO = '$orderNo' , 
				ORDER_YEAR = '$orderYear' , 
				SALES_ORDER_ID = '$salesOrderId' , 
				MANUAL_ORDER = '0',
				LAST_MODIFY_BY = '".$this->userId."'
				WHERE
				PLAN_ID = '$planId' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
}
?>
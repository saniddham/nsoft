<?php
include_once  "../../../../class/productionAndPlanning/cls_plan_common_get.php";
include_once  "../../../../class/cls_commonFunctions_get.php";

$obj_commonFunctions_get  = new cls_commonFunctions_get($db);

class Cls_PlanningBoard_Get extends Cls_Plan_Common_Get
{
	private $db;
	private $locationId ;
	private $companyId ;
	private $userId ;
	private $obj_plan_common_get ;
	
	function __construct($db,$locationId,$companyId,$userId)
	{
		$this->db 						= $db;
		$this->locationId 				= $locationId;
		$this->companyId 				= $companyId;
		$this->userId 					= $userId;
		$this->obj_plan_common_get  	= new Cls_Plan_Common_Get($db);
	}
	public function getCompanyCombo()
	{
		return $this->getCompanyCombo_sql();
	}
	public function loadOrderData($customer,$salesOrderNo,$brand,$style,$graphic,$planLocation)
	{
		global $obj_commonFunctions_get ;
		return $this->loadOrderData_sql($customer,$salesOrderNo,$brand,$style,$graphic,$planLocation);
	}
	public function getMOAOrderYear($customer)
	{		
		$sql = $this->getMOAOrderYear_sql($customer);
		$html = $this->getMOAOrderYear_combo($sql);
		return $html;
	}
	public function loadAllComboes($slected,$orderYear,$orderNo,$customerId)
	{
		$sql = $this->loadAllComboes_sql($slected,$orderYear,$orderNo,$customerId);
		$html = $this->loadAllComboes_combo($sql);
		return $html;
	}
	private function getCompanyCombo_sql()
	{
		$sql = "SELECT
				mst_locations_user.intLocationId,
				CONCAT(mst_companies.strName ,' - ',mst_locations.strName) AS companyName
				FROM
				mst_locations_user
				INNER JOIN mst_locations ON mst_locations.intId = mst_locations_user.intLocationId
				INNER JOIN mst_companies ON mst_locations.intCompanyId = mst_companies.intId
				WHERE
				mst_locations_user.intUserId = '".$this->userId."' ";
		
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	private function loadOrderData_sql($customer,$salesOrderNo,$brand,$style,$graphic,$planLocation)
	{
		global $obj_commonFunctions_get ;
		
		$whSql		= "";
		
		if($customer!='')
			$whSql = "AND intCustomer='$customer' ";
		if($salesOrderNo!='')
			$whSql .= "AND strSalesOrderNo LIKE '%$salesOrderNo%' ";
		if($brand!='')
			$whSql .= "AND brand LIKE '%$brand%' ";
		if($style!='')
			$whSql .= "AND strStyleNo LIKE '%$style%' ";
		if($graphic!='')
			$whSql .= "AND strGraphicNo LIKE '%$graphic%' ";
			
		$sql = "SELECT TB_1.*
				FROM(SELECT OH.intOrderNo,
				OH.intOrderYear,
				OH.intCustomer,
				MC.strName as customer,
				OH.strCustomerPoNo,
				OD.strSalesOrderNo,
				OD.intSalesOrderId,
				OD.strStyleNo,
				OD.strGraphicNo,
				OD.strCombo,
				OH.intStatus,
				OH.intLocationId,
				(SELECT MB.strName 
				FROM trn_sampleinfomations TSI 
				INNER JOIN mst_brand MB ON MB.intId=TSI.intBrand 
				WHERE TSI.intSampleNo = OD.intSampleNo AND 
				TSI.intSampleYear = OD.intSampleYear AND 
				TSI.intRevisionNo = OD.intRevisionNo ) AS brand,
				
				(SELECT COUNT(intColorId)
				FROM trn_sampleinfomations_details SID
				WHERE SID.intSampleNo=OD.intSampleNo AND
				SID.intSampleYear=OD.intSampleYear AND
				SID.intRevNo=OD.intRevisionNo AND
				SID.strPrintName=OD.strPrintName AND
				SID.strComboName=OD.strCombo) noOfColors,
				
				(SELECT SUM(SIDT.intNoOfShots)
				FROM trn_sampleinfomations_details_technical SIDT
				WHERE SIDT.intSampleNo=OD.intSampleNo AND
				SIDT.intSampleYear=OD.intSampleYear AND
				SIDT.intRevNo=OD.intRevisionNo AND
				SIDT.strPrintName=OD.strPrintName AND
				SIDT.strComboName=OD.strCombo
				LIMIT 0,1) noOfStocks,
				
				OH.intMarketer,
				SU.strUserName AS marketer,
				OD.dtPSD,
				
				IFNULL(SUM(OD.intQty),0) AS orderQty
				FROM trn_orderheader OH
				INNER JOIN trn_orderdetails OD ON OD.intOrderNo=OH.intOrderNo AND OD.intOrderYear=OH.intOrderYear
				INNER JOIN mst_customer MC ON MC.intId=OH.intCustomer
				INNER JOIN sys_users SU ON SU.intUserId=OH.intMarketer
				WHERE NOT EXISTS (SELECT DISTINCT ORDER_NO,ORDER_YEAR,SALES_ORDER_ID 
				FROM planning_plan_orders_header POH
				WHERE POH.ORDER_NO=OH.intOrderNo AND
				POH.ORDER_YEAR=OH.intOrderYear AND
				POH.SALES_ORDER_ID=OD.intSalesOrderId)
				GROUP BY OD.intOrderYear,OD.intOrderNo,OD.intSalesOrderId) AS TB_1
				WHERE intStatus=1  
				$whSql ";
		
		$result = $this->db->RunQuery($sql);
		while($row = mysqli_fetch_array($result))
		{
			$data['orderNo'] 	 		= $row['intOrderNo'];
			$data['orderYear'] 	 		= $row['intOrderYear'];
			$data['customerId'] 		= $row['intCustomer'];
			$data['customerName']		= $row['customer'];
			$data['PONo']				= $row['strCustomerPoNo'];
			$data['salesOrderId'] 		= $row['intSalesOrderId'];
			$data['salesOrderNo'] 		= $row['strSalesOrderNo'];
			$data['style'] 				= $row['strStyleNo'];
			$data['graphic'] 			= $row['strGraphicNo'];	
			$data['combo'] 				= $row['strCombo'];	
			$data['brand'] 				= $row['brand'];
			$data['colors'] 			= $row['noOfColors'];
			$data['strocks'] 			= $row['noOfStocks'];
			$data['qty'] 				= $row['orderQty'];
			
			$data['marketerId'] 		= $row['intMarketer'];
			$data['marketerName'] 		= $row['marketer'];
			$data['PSD'] 				= $row['dtPSD'];
			
			$data['printMethodHTML'] 	= $this->obj_plan_common_get->getPrinterMethodGridCombo();
			$data['plateSizeHTML'] 		= $this->obj_plan_common_get->getPlateSizeGridCombo();
			$data['panelAddMode']		= $obj_commonFunctions_get->ValidateSpecialPermission('30',$this->userId,'RunQuery');
			
			$arrDetailData[] 			= $data;
		}
		return $arrDetailData;
	}
	public function getHeaderData($customer,$planLocation,$salesOrder,$style,$graphic,$printType,$planId,$executionType)
	{
		$Wsql 	= "";
		$MWsql 	= "";
		
		if($salesOrder!='')
		{
			$Wsql	.="AND OD.strSalesOrderNo LIKE '%$salesOrder%' ";
			$MWsql	.="AND PMO.SALES_ORDER_NO LIKE '%$salesOrder%' ";
		}
		if($style!='')
		{
			$Wsql	.="AND OD.strStyleNo LIKE '%$style%' ";
			$MWsql	.="AND PMO.STYLE_NO LIKE '%$style%' ";
		}
		if($graphic!='')
		{
			$Wsql	.="AND OD.strGraphicNo LIKE '%$graphic%' ";
			$MWsql	.="AND PMO.GRAPHIC_NO LIKE '%$graphic%' ";
		}
		if($printType!='')
		{
			$Wsql	.="AND POH.PRINTING_METHOD_ID='$printType' ";
			$MWsql	.="AND POH.PRINTING_METHOD_ID='$printType' ";
		}
		if($customer!='')
		{
			$Wsql	.="AND POH.CUSTOMER_ID='$customer' ";
			$MWsql	.="AND POH.CUSTOMER_ID='$customer' ";
		}
		if($planId!='')
		{
			$Wsql	.="AND POH.PLAN_ID='$planId' ";
			$MWsql	.="AND POH.PLAN_ID='$planId' ";
		}
	
		$sql = "(
				SELECT PLAN_ID 		AS planId,
				POH.CUSTOMER_ID		AS customerId,
				OD.strSalesOrderNo	AS salesOrderNo,
				MC.strName			AS customerName,
				POH.SALES_ORDER_ID	AS salesOrderId,
				POH.MANUAL_ORDER	AS manualOrderStatus,
				POH.LOCATION_ID		AS locationId,
				POH.PLATE_SIZE_ID	AS plateSizeId,
				PS.strName			AS plateSizeName,
				PST.STROCK_TARGET	AS targetStrocks,
				SU.strUserName		AS marketer,
				(SELECT MB.strName 
				FROM trn_sampleinfomations TSI 
				INNER JOIN mst_brand MB ON MB.intId=TSI.intBrand
				WHERE TSI.intSampleNo = OD.intSampleNo AND 
				TSI.intSampleYear = OD.intSampleYear AND 
				TSI.intRevisionNo = OD.intRevisionNo ) AS brand,
				OD.strStyleNo		AS styleNo,
				OD.strGraphicNo		AS graphicNo,
				POH.COLORS		AS noOfColors,
				POH.STROCKS		AS noOfStrocks,
				POH.PANELS		AS noOfPanels,
				POH.QTY			AS POQty
				FROM planning_plan_orders_header POH
				INNER JOIN trn_orderdetails OD ON OD.intOrderNo=POH.ORDER_NO AND 
				OD.intOrderYear=POH.ORDER_YEAR AND 
				POH.SALES_ORDER_ID=OD.intSalesOrderId
				INNER JOIN sys_users SU ON SU.intUserId=POH.MARKETER_ID
				INNER JOIN mst_customer MC ON MC.intId=POH.CUSTOMER_ID
				LEFT JOIN mst_printsizes PS ON PS.intId=POH.PLATE_SIZE_ID  
				LEFT JOIN planning_strock_target PST on PST.PRINT_SIZE_ID=POH.PLATE_SIZE_ID AND
				PST.LOCATION_ID=POH.LOCATION_ID
				WHERE POH.LOCATION_ID = '$planLocation' AND
				POH.MANUAL_ORDER = '0' 
				$Wsql 
				)
				UNION 
				(
				SELECT 
				POH.PLAN_ID			AS planId,
				POH.CUSTOMER_ID		AS customerId,
				PMO.SALES_ORDER_NO	AS salesOrderNo,
				MC.strName			AS customerName,
				POH.SALES_ORDER_ID	AS salesOrderId,
				POH.MANUAL_ORDER	AS manualOrderStatus,
				POH.LOCATION_ID		AS locationId,
				POH.PLATE_SIZE_ID	AS plateSizeId,
				PS.strName			AS plateSizeName,
				PST.STROCK_TARGET	AS targetStrocks,
				SU.strUserName		AS marketer,
				PMO.BRAND			AS brand,
				PMO.STYLE_NO		AS styleNo,
				PMO.GRAPHIC_NO		AS graphicNo,
				POH.COLORS			AS noOfColors,
				POH.STROCKS			AS noOfStrocks,
				POH.PANELS			AS noOfPanels,
				POH.QTY				AS POQty
				FROM planning_plan_orders_header POH
				INNER JOIN planning_manual_orders PMO ON PMO.PLAN_ID=POH.PLAN_ID
				INNER JOIN sys_users SU ON SU.intUserId=POH.MARKETER_ID
				INNER JOIN mst_customer MC ON MC.intId=POH.CUSTOMER_ID
				LEFT JOIN mst_printsizes PS ON PS.intId=POH.PLATE_SIZE_ID  
				LEFT JOIN planning_strock_target PST on PST.PRINT_SIZE_ID=POH.PLATE_SIZE_ID AND
				PST.LOCATION_ID=POH.LOCATION_ID
				WHERE POH.LOCATION_ID = '$planLocation' AND
				POH.MANUAL_ORDER = '1' 
				$MWsql
				)
				ORDER BY planId";
		
		$result = $this->db->$executionType($sql);
		return $result;
	}
	public function getTypeData()
	{
		$sql = "SELECT QTY_TYPE_ID,TYPE,HEADER_TYPE
				FROM planning_qty_type
				WHERE STATUS=1";
		
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	public function getPlanQty($newFrmDate,$typeId,$planId,$executionType)
	{
		$sql = "SELECT QTY
				FROM planning_plan_orders_details
				WHERE PLAN_ID = '$planId' AND
				DATE = '$newFrmDate' AND
				QTY_TYPE_ID = '$typeId' ";
		
		$result = $this->db->$executionType($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row['QTY'];
	}
	public function getTotalStrocks($date,$customer,$planLocation,$salesOrder,$style,$graphic,$printType)
	{		
		$Wsql 	= "";
		$MWsql 	= "";
		
		if($salesOrder!='')
		{
			$Wsql	.="AND OD.strSalesOrderNo LIKE '%$salesOrder%' ";
			$MWsql	.="AND PMO.SALES_ORDER_NO LIKE '%$salesOrder%' ";
		}
		if($style!='')
		{
			$Wsql	.="AND OD.strStyleNo LIKE '%$style%' ";
			$MWsql	.="AND PMO.STYLE_NO LIKE '%$style%' ";
		}
		if($graphic!='')
		{
			$Wsql	.="AND OD.strGraphicNo LIKE '%$graphic%' ";
			$MWsql	.="AND PMO.GRAPHIC_NO LIKE '%$graphic%' ";
		}
		if($printType!='')
		{
			$Wsql	.="AND POH.PRINTING_METHOD_ID='$printType' ";
			$MWsql	.="AND POH.PRINTING_METHOD_ID='$printType' ";
		}
		if($customer!='')
		{
			$Wsql	.="AND POH.CUSTOMER_ID='$customer' ";
			$MWsql	.="AND POH.CUSTOMER_ID='$customer' ";
		}
			
		$sql = "SELECT IFNULL(SUM(tbl_1.totStrocks),0) AS totStrockAmount
				FROM
				(
					(SELECT POD.QTY AS totStrocks
									FROM planning_plan_orders_details POD
									INNER JOIN planning_plan_orders_header POH ON POH.PLAN_ID=POD.PLAN_ID
									INNER JOIN trn_orderdetails OD ON OD.intOrderNo=POH.ORDER_NO AND
									OD.intOrderYear=POH.ORDER_YEAR AND
									OD.intSalesOrderId=POH.SALES_ORDER_ID
									WHERE POH.LOCATION_ID='$planLocation' AND
									POD.DATE='$date' AND
									POD.QTY_TYPE_ID='4' 
									$Wsql
					)
					UNION
					(
					SELECT POD.QTY AS totStrocks
									FROM planning_plan_orders_details POD
									INNER JOIN planning_plan_orders_header POH ON POH.PLAN_ID=POD.PLAN_ID
									INNER JOIN planning_manual_orders PMO ON PMO.PLAN_ID=POH.PLAN_ID 
									WHERE POH.LOCATION_ID='$planLocation' AND
									POD.DATE='$date' AND
									POD.QTY_TYPE_ID='4' 
									$MWsql
					)
				) AS tbl_1";
		
		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row['totStrockAmount'];	
	}
	public function getTotalGroups($date,$customer,$planLocation,$salesOrder,$style,$graphic,$printType)
	{
		$Wsql 	= "";
		$MWsql 	= "";
		
		if($salesOrder!='')
		{
			$Wsql	.="AND OD.strSalesOrderNo LIKE '%$salesOrder%' ";
			$MWsql	.="AND PMO.SALES_ORDER_NO LIKE '%$salesOrder%' ";
		}
		if($style!='')
		{
			$Wsql	.="AND OD.strStyleNo LIKE '%$style%' ";
			$MWsql	.="AND PMO.STYLE_NO LIKE '%$style%' ";
		}
		if($graphic!='')
		{
			$Wsql	.="AND OD.strGraphicNo LIKE '%$graphic%' ";
			$MWsql	.="AND PMO.GRAPHIC_NO LIKE '%$graphic%' ";
		}
		if($printType!='')
		{
			$Wsql	.="AND POH.PRINTING_METHOD_ID='$printType' ";
			$MWsql	.="AND POH.PRINTING_METHOD_ID='$printType' ";
		}
		if($customer!='')
		{
			$Wsql	.="AND POH.CUSTOMER_ID='$customer' ";
			$MWsql	.="AND POH.CUSTOMER_ID='$customer' ";
		}
			
		$sql = "SELECT IFNULL(SUM(tbl_1.totGroups),0) AS totGroupAmount
				FROM
				(
					(SELECT POD.QTY AS totGroups
									FROM planning_plan_orders_details POD
									INNER JOIN planning_plan_orders_header POH ON POH.PLAN_ID=POD.PLAN_ID
									INNER JOIN trn_orderdetails OD ON OD.intOrderNo=POH.ORDER_NO AND
									OD.intOrderYear=POH.ORDER_YEAR AND
									OD.intSalesOrderId=POH.SALES_ORDER_ID
									WHERE POH.LOCATION_ID='$planLocation' AND
									POD.DATE='$date' AND
									POD.QTY_TYPE_ID='5' 
									$Wsql
					)
					UNION
					(
					SELECT POD.QTY AS totGroups
									FROM planning_plan_orders_details POD
									INNER JOIN planning_plan_orders_header POH ON POH.PLAN_ID=POD.PLAN_ID
									INNER JOIN planning_manual_orders PMO ON PMO.PLAN_ID=POH.PLAN_ID 
									WHERE POH.LOCATION_ID='$planLocation' AND
									POD.DATE='$date' AND
									POD.QTY_TYPE_ID='5' 
									$MWsql
					)
				) AS tbl_1";
		
		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row['totGroupAmount'];
	}
	public function getAvilableStrocks($year,$month,$planLocation,$printType)
	{
		$sql = "SELECT NO_OF_STROCKS
				FROM planning_avilable_strocks
				WHERE PLAN_YEAR = '$year' AND
				PRINT_TYPE = '$printType' AND
				LOCATION_ID = '$planLocation' ";
		
		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row['NO_OF_STROCKS'];
	}
	public function getAvilableGroups($year,$month,$planLocation,$printType)
	{
		$sql = "SELECT NO_OF_GROUPS
				FROM planning_avilable_strocks
				WHERE PLAN_YEAR = '$year' AND
				PRINT_TYPE = '$printType' AND
				LOCATION_ID = '$planLocation' ";
		
		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row['NO_OF_GROUPS'];
	}
	public function getManualOrderDetails()
	{
		$sql = "SELECT POH.PLAN_ID,
				SU.strUserName AS marketer,
				MC.strName AS customer,
				PMO.PO_NO,
				PMO.SALES_ORDER_NO,
				PMO.STYLE_NO,
				PMO.GRAPHIC_NO
				FROM planning_plan_orders_header POH
				INNER JOIN planning_manual_orders PMO ON POH.PLAN_ID=PMO.PLAN_ID
				INNER JOIN mst_marketer MAR ON MAR.intUserId=POH.MARKETER_ID
				INNER JOIN sys_users SU ON SU.intUserId=MAR.intUserId
				INNER JOIN mst_customer MC ON MC.intId=POH.CUSTOMER_ID
				WHERE POH.LOCATION_ID = '".$this->locationId."' AND
				POH.MANUAL_ORDER = 1 ";
		
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	public function getRptDateRange($planId)
	{
		$sql = "SELECT MIN(DATE) AS startDate,
				MAX(DATE) AS endDate
				FROM planning_plan_orders_details
				WHERE PLAN_ID = '$planId' ";
		
		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row;
	}
	public function getRptHeaderData($planId)
	{
		$sql = "(SELECT POH.CUSTOMER_ID,
				MC.strName AS customer,
				OD.strSalesOrderNo AS salesOrderNo,
				OD.strStyleNo AS styleNo,
				OD.strGraphicNo AS graphicNo,
				PS.strName AS plateSize,
				POH.COLORS AS noOfColors,
				POH.STROCKS AS noOfStrocks,
				POH.PANELS AS noOfPanels,
				POH.QTY	AS POQty,
				OD.dtPSD,
				OD.intSampleNo,
				OD.intSampleYear,
				OD.intRevisionNo
				FROM planning_plan_orders_header POH
				INNER JOIN mst_customer MC ON MC.intId=POH.CUSTOMER_ID
				INNER JOIN trn_orderdetails OD ON OD.intOrderNo=POH.ORDER_NO AND 
				OD.intOrderYear=POH.ORDER_YEAR AND 
				POH.SALES_ORDER_ID=OD.intSalesOrderId
				INNER JOIN mst_printsizes PS ON PS.intId=POH.PLATE_SIZE_ID
				WHERE POH.PLAN_ID = '$planId' 
				)
				UNION
				(
				SELECT POH.CUSTOMER_ID,
				MC.strName AS customer,
				PMO.SALES_ORDER_NO AS salesOrderNo,
				PMO.STYLE_NO AS styleNo,
				PMO.GRAPHIC_NO AS graphicNo,
				PS.strName AS plateSize,
				POH.COLORS AS noOfColors,
				POH.STROCKS AS noOfStrocks,
				POH.PANELS AS noOfPanels,
				POH.QTY	AS POQty,
				PMO.PSD_DATE AS dtPSD,
				0 AS intSampleNo,
				0 AS intSampleYear,
				0 AS intRevisionNo
				FROM planning_plan_orders_header POH
				INNER JOIN mst_customer MC ON MC.intId=POH.CUSTOMER_ID
				INNER JOIN planning_manual_orders PMO ON PMO.PLAN_ID=POH.PLAN_ID
				INNER JOIN mst_printsizes PS ON PS.intId=POH.PLATE_SIZE_ID
				WHERE POH.PLAN_ID = '$planId'
				)";
		
		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row;
	}
	public function getRptPlanQty($newFrmDate,$planId,$typeId)
	{
		$sql = "SELECT QTY
				FROM planning_plan_orders_details
				WHERE PLAN_ID = '$planId' AND
				DATE = '$newFrmDate' AND
				QTY_TYPE_ID = '$typeId' ";
		
		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row['QTY'];
	}
	public function getActualReceivingBC($newFrmDate,$planId)
	{
		$sql = "SELECT SUM(dblQty) totRecievedQty
				FROM ware_stocktransactions_fabric STF
				INNER JOIN planning_plan_orders_header POH ON POH.ORDER_NO=STF.intOrderNo AND
				POH.ORDER_YEAR=STF.intOrderYear AND
				POH.SALES_ORDER_ID=STF.intSalesOrderId
				WHERE POH.PLAN_ID = '$planId' AND
				STF.strType = 'Received' AND
				DATE(STF.dtDate) = '$newFrmDate'
				GROUP BY DATE(STF.dtDate) ";
		
		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row['totRecievedQty'];
	}
	public function allocateOrderDetails($orderYear,$orderNo,$salesOrderId)
	{
		$sql = "SELECT strSalesOrderNo,
				strStyleNo,
				strGraphicNo
				FROM trn_orderdetails
				WHERE intOrderNo = '$orderNo' AND
				intOrderYear = '$orderYear' AND
				intSalesOrderId = '$salesOrderId' ";
		
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row;
	}
	public function getCompanyHoliday($newFrmDate,$executionType)
	{
		$sql = "SELECT PC.intDayType
				FROM mst_plant_calender PC
				INNER JOIN mst_locations ML ON ML.intPlant=PC.intPlantId
				WHERE PC.dtDate = '$newFrmDate' AND
				ML.intId = '".$this->locationId."' ";
		
		$result = $this->db->$executionType($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row;
	}
	private function getMOAOrderYear_sql($customer)
	{
		$sql = "SELECT DISTINCT intOrderYear
				FROM trn_orderheader
				WHERE intStatus = 1 AND
				intCustomer = '$customer'
				ORDER BY intOrderYear ";
		
		return $sql;
	}
	private function getMOAOrderYear_combo($sql)
	{
		$result = $this->db->RunQuery($sql);
		$string ="<option value=\"\"></option>";
		while($row = mysqli_fetch_array($result))
		{
			$string.="<option value=\"".$row['intOrderYear']."\">".$row['intOrderYear']."</option>";
		}
		return $string;
	}
	private function loadAllComboes_sql($slected,$orderYear,$orderNo,$customerId)
	{
		$sql = "SELECT DISTINCT ";
		if($slected=='orderYear')
		{
			$sql .= "OD.intOrderNo as value,
					 OD.intOrderNo as id ";
		}
		if($slected=='orderNo')
		{
			$sql .= "OD.strSalesOrderNo as value, 
					 OD.intSalesOrderId as id ";
		}
		$sql .= "FROM trn_orderdetails OD 
				 INNER JOIN trn_orderheader OH ON OH.intOrderNo=OD.intOrderNo AND OH.intOrderYear=OD.intOrderYear
				 WHERE OH.intStatus = 1 and
				 intCustomer = '$customerId' ";
		
		if($orderYear!='')
			$sql .= "AND OD.intOrderYear = '$orderYear' ";
		if($orderNo!='')
			$sql .= "AND OD.intOrderNo = '$orderNo' ";
		
		if($slected=='orderYear')
			$sql .= "ORDER BY OD.intOrderYear DESC";	
		if($slected=='orderNo')
			$sql .= "ORDER BY OD.intOrderNo DESC";
		
		return $sql ;
	}
	private function loadAllComboes_combo($sql)
	{
		$result = $this->db->RunQuery($sql);
		$string ="<option value=\"\"></option>";
		while($row = mysqli_fetch_array($result))
		{
			$string.="<option value=\"".$row['id']."\">".$row['value']."</option>";
		}
		return $string;
	}
}

?>
<?php
class team_get{

private $db;

function __construct($db)
{
	$this->db = $db;
}

public function loadPrintTypes()
{
	return $this->getPrintType();	
}	

public function loadTeam()
{
	return $this->loadTeam_sql();	
}

public function searchTeam($searchId)
{
	return $this->getSavedTeams_sql($searchId);	
}

private function loadTeam_sql()
{
$sql	= " SELECT 	TEAM_ID, 
			TEAM_NAME 
				 
			FROM 
			planning_plan_teams 
			
			ORDER BY
			TEAM_NAME";
			
$result	= $this->db->RunQuery($sql);
return $result;
}

private function getSavedTeams_sql($searchId)
{
$sql	= " SELECT 	 
			TEAM_NAME, 
			PRINT_TYPE_ID, 
			REMARKS, 
			STATUS
			 
			FROM 
			planning_plan_teams 
			
			WHERE 
			TEAM_ID = '$searchId'";
			
$result	= $this->db->RunQuery($sql);
$row	= mysqli_fetch_array($result);
return $row;	
}

private function getPrintType()
{
$sql	= "	SELECT 	
			intId, 
			strName 
				 
			FROM 
			mst_printertypes 
			
			ORDER BY 
			strName";	
			
$result	= $this->db->RunQuery($sql);
return $result;
}
}

?>

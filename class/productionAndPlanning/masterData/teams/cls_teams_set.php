<?php
class teams_set{
private $db;

function __construct($db)
{
	$this->db = $db;
}

public function saveTeam($teamName,$printType,$description,$status,$companyId,$locationId,$userId)
{
	return $this->saveTeam_sql($teamName,$printType,$description,$status,$companyId,$locationId,$userId);
}

public function updateTeam($searchId,$teamName,$printType,$description,$status,$companyId,$locationId,$userId)
{
	return $this->updateTeam_sql($searchId,$teamName,$printType,$description,$status,$companyId,$locationId,$userId);	
}

public function deleteTeam($searchId)
{
	return $this->deleteTeam_sql($searchId);	
}

private function saveTeam_sql($teamName,$printType,$description,$status,$companyId,$locationId,$userId)
{
	$sql = "INSERT INTO planning_plan_teams 
			( 
			TEAM_NAME, 
			PRINT_TYPE_ID, 
			REMARKS, 
			STATUS, 
			COMPANY_ID, 
			LOCATION_ID, 
			CREATED_BY, 
			CREATED_DATE 
			)
			VALUES
			( 
			'$teamName', 
			'$printType', 
			'$description', 
			'$status', 
			'$companyId', 
			'$locationId', 
			'$userId', 
	     	NOW()
			)";

$result	= $this->db->RunQuery2($sql);
if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data; 
			
	
}

private function updateTeam_sql($searchId,$teamName,$printType,$description,$status,$companyId,$locationId,$userId)
{
	$sql	= "UPDATE planning_plan_teams 
				SET
				TEAM_NAME = '$teamName' , 
				PRINT_TYPE_ID = '$printType' , 
				REMARKS = '$description' , 
				STATUS = '$status' , 
				COMPANY_ID = '$companyId' , 
				LOCATION_ID = '$locationId' , 
				MODIFY_BY = '$userId' , 
				MODIFY_DATE = NOW()
				
				WHERE
				TEAM_ID = '$searchId' ;"	;
				
	$result	= $this->db->RunQuery2($sql);
	if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
	return $data; 
			
}

private function deleteTeam_sql($searchId)
{
	$sql	= "DELETE FROM planning_plan_teams 
				WHERE
				TEAM_ID = '$searchId' ;";	

	$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
}

}
?>
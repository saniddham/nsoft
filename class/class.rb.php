<?php
//ini_set('display_errors',1);
include_once('../../libraries/pdf-php/class.ezpdf.php');

class RBpdf extends Cezpdf {
private $db;
	function __construct($db){
		$this->db = $db;
		Cezpdf::__construct();
		$this->selectFont('libraries/pdf-php/fonts/Helvetica.afm');
	}
	public function view_material_report($sql=''){
		
		if($sql=='')
		{
			$sql='SELECT
				mst_maincategory.strName AS `Main`,
				mst_subcategory.strName AS `Sub`,
				mst_item.strCode AS `Code`,
				mst_item.strName AS `Name`
			FROM
			mst_item
				Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
				Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
			ORDER BY
				mst_maincategory.intId ASC,
				`Sub` ASC,
				`Name` ASC limit 100';	
		}
		
		$result = $this->db->RunQuery($sql);
		while($data[] = mysqli_fetch_array($result, MYSQLI_ASSOC)) {}
		//$this->ezStartPageNumbers(580,10,10,'','',1);
		
		
		//$cols = array('Main'=>"Main",'Sub'=>'Sub','Code'=>'Code','Name'=>'Name');
		$this->ezTable($data,'','ROW MATERIAL LIST',
array('xPos'=>0,'xOrientation'=>'right','width'=>550
,'cols'=>array('Main'=>array('justification'=>'center'))));

		
		$this->ezStream();
	}
}

?>
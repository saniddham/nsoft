<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

$userId = $_SESSION['userId'];
$x_locationId = $_SESSION['CompanyID'];
$root_path = $_SESSION['ROOT_PATH'];
$config_path = 'MAIN_ROOT';

class cls_create_mail
{

    private $db;

    function __construct($db)
    {
        $this->db = $db;
    }

    public function send_Response_Mail($filePath, $dataArray, $fromName, $fromEmail, $subject, $toEmail, $toName)
    {

        $body = $this->getBodyText($filePath, $dataArray);
        $this->sendMessage($fromEmail, $fromName, $toName, $toEmail, $subject, $body);
        //$this->sendCopyMail($fromEmail,$fromName,$toName,$toEmail,$subject,$body);
    }

    public function send_Response_Mail2($filePath, $dataArray, $fromName, $fromEmail, $subject, $toEmail, $toName)
    {

        $body = $this->getBodyText($filePath, $dataArray);
        $this->sendMessage2($fromEmail, $fromName, $toName, $toEmail, $subject, $body);
        //$this->sendCopyMail($fromEmail,$fromName,$toName,$toEmail,$subject,$body);
    }

    public function send_latest_Mail($filePath, $req_para, $fromName, $fromEmail, $subject, $toEmail, $toName)
    {
        $body = $this->getBodyText_latest($filePath, $req_para);
        $this->sendMessage($fromEmail, $fromName, $toName, $toEmail, $subject, $body);
        //$this->sendCopyMail($fromEmail,$fromName,$toName,$toEmail,$subject,$body);
    }

    private function sendCopyMail($fromEmail, $fromName, $toName, $toEmail, $subject, $body)
    {
        $sql1 = "SELECT
						sys_mail_copy_users.intMailCopyUserId,
						u2.strEmail AS copyUserEmail,
						u2.strFullName as copyUserName
					FROM
						sys_mail_copy_users
						Inner Join sys_users AS u1 ON u1.intUserId = sys_mail_copy_users.intMailMasterUserId
						Inner Join sys_users AS u2 ON u2.intUserId = sys_mail_copy_users.intMailCopyUserId
					WHERE
						u1.strEmail =  '$toEmail' AND
						u2.strEmail <> ''  
					";
        $result1 = $this->db->RunQuery($sql1);
        while ($row1 = mysqli_fetch_array($result1)) {
            $this->sendMessage($fromEmail, $fromName, $row1['copyUserName'], $row1['copyUserEmail'], $subject, $body);
        }
    }

    public function sendMail($filePath, $dataArray, $mailEventId, $fromName, $fromEmail, $subject)
    {
        $body = $this->getBodyText($filePath, $dataArray);
        $sql = "SELECT
					sys_users.strEmail,sys_users.strFullName,sys_users.intUserId
				FROM
					sys_mail_eventusers
				Inner Join sys_users ON sys_users.intUserId = sys_mail_eventusers.intUserId
				WHERE
					sys_mail_eventusers.intMailEventId =  '$mailEventId' AND
					sys_users.strEmail <>  ''
				";
        $result = $this->db->RunQuery($sql);
        while ($row = mysqli_fetch_array($result)) {
            $masterUserId = $row['intUserId'];
            $this->sendMessage($fromEmail, $fromName, $row['strFullName'], $row['strEmail'], $subject, $body);
            /*$sql1 	= "SELECT
                            sys_mail_copy_users.intMailCopyUserId,
                            sys_users.strEmail,sys_users.strFullName
                        FROM
                        sys_mail_copy_users
                            Inner Join sys_users ON sys_users.intUserId = sys_mail_copy_users.intMailCopyUserId
                        WHERE
                            sys_mail_copy_users.intMailMasterUserId =  '$masterUserId' AND
                            sys_users.strEmail <>  ''
                    ";
            $result1 = $this->db->RunQuery($sql1);
            while($row1=mysqli_fetch_array($result1))
            {
                $this->sendMessage($fromEmail,$fromName,$row1['strFullName'],$row1['strEmail'],$subject,$body);
            }*/
        }

    }

    private function getBodyText($path, $dataArray)
    {

        global $root_path;
        $db = $this->db;
        include($root_path . 'commonScript/mail/html.php');
        return $body;
    }

    private function getBodyText_latest($filePath, $req_para)
    {
        global $config_path;
        $db = $this->db;

        ob_start();
        include $config_path . "commonScript/mail/css_mail_header.php";
        include $config_path . $filePath;
        $body = ob_get_clean();

        return $body;
    }

    private function sendMessage($senderEmail, $senderName, $recieverName, $recieverEmail, $subject, $body)
    {
        global $userId;
        global $x_locationId;

        $body = $this->replace($body);
        $subject = $this->replace($subject);
        $body = str_replace('MAILRECIVER_NAME', $recieverName, $body);

        $body = str_replace('MAILFROM_NAME', $senderName, $body);

        $sql_final = "INSERT INTO  sys_emailpool 		(`intLocationId`,strFromName,`strFromEmail`,`strToEmail`,`strMailHeader`,`strEmailBody`,`intEnterUserId`,`dtmEnterDate`) 
		VALUES ('$x_locationId','$senderName','$senderEmail','$recieverEmail','$subject','$body','$userId',now())";
        $this->db->RunQuery($sql_final);
    }

    private function sendMessage2($senderEmail, $senderName, $recieverName, $recieverEmail, $subject, $body)
    {
        global $userId;
        global $x_locationId;

        $body = $this->replace($body);
        $subject = $this->replace($subject);
        $body = str_replace('MAILRECIVER_NAME', $recieverName, $body);
        $body = str_replace('MAILFROM_NAME', $senderName, $body);

        $sql_final = "INSERT INTO  sys_emailpool 		(`intLocationId`,strFromName,`strFromEmail`,`strToEmail`,`strMailHeader`,`strEmailBody`,`intEnterUserId`,`dtmEnterDate`) 
		VALUES ('$x_locationId','$senderName','$senderEmail','$recieverEmail','$subject','$body','$userId',now())";
        $this->db->RunQuery2($sql_final);
    }

    private function replace($value)
    {
        $value = str_replace("'", "\'", $value);
        $value = str_replace('"', '\"', $value);
        return $value;
    }
}

?>
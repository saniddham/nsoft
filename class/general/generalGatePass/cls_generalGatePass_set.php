<?php
include_once  "../../../class/cls_commonFunctions_get.php";
include_once  "../../../class/cls_commonErrorHandeling_get.php";

$session_userId			= $_SESSION["userId"];
$session_locationId 	= $_SESSION["CompanyID"];
$session_companyId 		= $_SESSION["headCompanyId"];
$savedMasseged			= "";
$error_sql				= "";

$obj_common				= new cls_commonFunctions_get($db);
$obj_errorHandeling 	= new cls_commonErrorHandeling_get($db);

class Cls_GeneralGatePass_Set
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	public function save($arrHeader,$arrDetails,$programCode,$programName)
	{
		global $obj_common;
		global $obj_errorHandeling;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_locationId;
		global $session_companyId;
		global $session_userId;
		
		$gatePassNo		= $arrHeader["gatePassNo"];
		$gatePassYear	= $arrHeader["gatePassYear"];
		$date			= $arrHeader["date"];
		$gatePassTo		= $obj_common->replace($arrHeader["gatePassTo"]);
		$attentionTo	= $obj_common->replace($arrHeader["attentionTo"]);
		$remarks		= $obj_common->replace($arrHeader["remarks"]);
		$through		= ($arrHeader["through"]==''?'null':$arrHeader["through"]);
		
		$savedStatus	= true;
		$editMode		= false;
		$this->db->begin();
		
		$this->validateBeforeSave($gatePassNo,$gatePassYear,$programCode,$session_userId);
		
		if($gatePassNo=='' && $gatePassYear=='')
		{
			$sysNo_arry 	= $obj_common->GetSystemMaxNo('intGeneralGatePassNo',$session_locationId);
			if($sysNo_arry["rollBackFlag"]==1)
			{
				if($savedStatus)
				{
					$savedStatus	= false;
					$savedMasseged 	= $sysNo_arry["msg"];
					$error_sql		= $sysNo_arry["q"];
				}
				
			}
			$gatePassNo				= $sysNo_arry["max_no"];
			$gatePassYear			= date('Y');
			$approveLevels 			= $obj_common->getApproveLevels($programName);
			$status					= $approveLevels+1;
			
			$this->saveHeader($gatePassNo,$gatePassYear,$approveLevels,$status,$date,$gatePassTo,$attentionTo,$remarks,$through);
		}
		else
		{
			$approveLevels 			= $obj_common->getApproveLevels($programName);
			$status					= $approveLevels+1;
			
			$this->updateHeader($gatePassNo,$gatePassYear,$approveLevels,$status,$date,$gatePassTo,$attentionTo,$remarks,$through);
			$this->updateMaxStatus($gatePassNo,$gatePassYear);
			$this->deleteDetails($gatePassNo,$gatePassYear);
			$editMode				= true;
		}
		foreach($arrDetails as $array_loop)
		{
			$itemId					= $array_loop["itemId"];
			$description			= $obj_common->replace($array_loop["description"]);
			$size					= $obj_common->replace($array_loop["size"]);
			$serialNo				= $obj_common->replace($array_loop["serialNo"]);
			$unit					= $array_loop["unit"];
			$qty					= ($array_loop["qty"]==''?'null':$array_loop["qty"]);
			$returnable				= ($array_loop["returnable"]==true?1:0);
			$returnableDate			= ($array_loop["returnableDate"]==''?'null':"'".$array_loop["returnableDate"]."'");
			
			$this->saveDetails($gatePassNo,$gatePassYear,$itemId,$description,$size,$serialNo,$unit,$qty,$qty,$returnable,$returnableDate);
		}
		if($savedStatus)
		{
			$this->db->commit();
			$response['type'] 			= "pass";
			if($editMode)
				$response['msg'] 		= "Updated Successfully.";
			else
				$response['msg'] 		= "Saved Successfully.";
			
			$response['gatePassNo']		= $gatePassNo;
			$response['gatePassYear']	= $gatePassYear;
		}
		else
		{
			$this->db->rollback();
			$response['type'] 			= "fail";
			$response['msg'] 			= $savedMasseged;
			$response['sql']			= $error_sql;
		}
		return json_encode($response);
	}
	public function approve($gatePassNo,$gatePassYear)
	{
		global $obj_common;
		global $obj_errorHandeling;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_locationId;
		global $session_companyId;
		global $session_userId;
		global $finalApproval;
		
		$savedStatus	= true;
		$finalApproval	= false;
		$this->db->begin();
		
		//if($status>$savedLevels)
			//$this->updateMaxStatus($gatePassNo,$gatePassYear);
			
		$this->updateHeaderStatus($gatePassNo,$gatePassYear,'');
		$this->approvedData($gatePassNo,$gatePassYear);
		
		if($savedStatus)
		{
			$this->db->commit();
			$response['type'] 			= "pass";
			if($finalApproval)
				$response['msg'] 		= "Final Approval Raised Successfully.";
			else
				$response['msg'] 		= "Approved Successfully.";
		}
		else
		{
			$this->db->rollback();
			$response['type'] 			= "fail";
			$response['msg'] 			= $savedMasseged;
			$response['sql']			= $error_sql;
		}
		return json_encode($response);
	}
	public function reject($gatePassNo,$gatePassYear)
	{
		global $obj_common;
		global $obj_errorHandeling;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_locationId;
		global $session_companyId;
		global $session_userId;
		
		$savedStatus	= true;
		$this->db->begin();
		
		$this->updateHeaderStatus($gatePassNo,$gatePassYear,'0');
		//$this->updateMaxStatus($gatePassNo,$gatePassYear);
		$this->approved_by_insert($gatePassNo,$gatePassYear,$session_userId,0);
		
		if($savedStatus)
		{
			$this->db->commit();
			$response['type'] 		= "pass";
			$response['msg'] 		= "Rejected Successfully.";
		}
		else
		{
			$this->db->rollback();
			$response['type'] 			= "fail";
			$response['msg'] 			= $savedMasseged;
			$response['sql']			= $error_sql;
		}
		return json_encode($response);
	}
	public function cancel($gatePassNo,$gatePassYear)
	{
		global $obj_common;
		global $obj_errorHandeling;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_locationId;
		global $session_companyId;
		global $session_userId;
		
		$savedStatus	= true;
		$this->db->begin();
		
		$this->updateHeaderStatus($gatePassNo,$gatePassYear,'-2');
		//$this->updateMaxStatus($gatePassNo,$gatePassYear);
		$this->approved_by_insert($gatePassNo,$gatePassYear,$session_userId,-2);
		
		if($savedStatus)
		{
			$this->db->commit();
			$response['type'] 		= "pass";
			$response['msg'] 		= "Cancelled Successfully.";
		}
		else
		{
			$this->db->rollback();
			$response['type'] 			= "fail";
			$response['msg'] 			= $savedMasseged;
			$response['sql']			= $error_sql;
		}
		return json_encode($response);
	}
	public function revise($gatePassNo,$gatePassYear)
	{
		global $obj_common;
		global $obj_errorHandeling;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_locationId;
		global $session_companyId;
		global $session_userId;
		
		$savedStatus	= true;
		$this->db->begin();
		
		$this->updateHeaderStatus($gatePassNo,$gatePassYear,'-1');
		//$this->updateMaxStatus($gatePassNo,$gatePassYear);
		$this->approved_by_insert($gatePassNo,$gatePassYear,$session_userId,-1);
		
		if($savedStatus)
		{
			$this->db->commit();
			$response['type'] 		= "pass";
			$response['msg'] 		= "Revised Successfully.";
		}
		else
		{
			$this->db->rollback();
			$response['type'] 			= "fail";
			$response['msg'] 			= $savedMasseged;
			$response['sql']			= $error_sql;
		}
		return json_encode($response);
	}
	private function validateBeforeSave($gatePassNo,$gatePassYear,$programCode,$session_userId)
	{
		global $obj_errorHandeling;
		global $savedStatus;
		global $savedMasseged;
		global $session_locationId;
		
		$header_arr		= $this->getHeaderData($gatePassNo,$gatePassYear);
		$response 		= $obj_errorHandeling->get_permision_withApproval_save($header_arr['STATUS'],$header_arr['APPROVE_LEVEL'],$session_userId,$programCode,'RunQuery2');
		if( ($gatePassNo != '' && $gatePassYear != "") && ($header_arr['LOCATION_ID'] != $session_locationId)){
			$savedStatus	= false;
			$savedMasseged 	= 'Invalid save location';
		}
		else if($response["type"]=='fail' && ($savedStatus))
		{
			$savedStatus	= false;
			$savedMasseged 	= $response["msg"];
		}
	}
	private function getHeaderData($gatePassNo,$gatePassYear)
	{
		$sql = "SELECT STATUS,
				APPROVE_LEVEL,
				LOCATION_ID 
				FROM general_gate_pass_header
				WHERE GATEPASS_NO='$gatePassNo' AND
				GATEPASS_YEAR='$gatePassYear' ";

		$result = $this->db->RunQuery2($sql);
		$row 	= mysqli_fetch_array($result);
		
		return $row;
	}
	private function saveHeader($gatePassNo,$gatePassYear,$approveLevels,$status,$date,$gatePassTo,$attentionTo,$remarks,$through)
	{
		global $session_userId;
		global $session_companyId;
		global $session_locationId;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "INSERT INTO general_gate_pass_header 
				(
				GATEPASS_NO, 
				GATEPASS_YEAR, 
				DATE, 
				GATEPASS_TO, 
				ATTENSION_TO, 
				THROUGH_ID, 
				REMARK, 
				STATUS, 
				APPROVE_LEVEL, 
				COMPANY_ID,
				LOCATION_ID,
				CREATED_BY, 
				CREATED_DATE
				)
				VALUES
				(
				'$gatePassNo', 
				'$gatePassYear', 
				'$date', 
				'$gatePassTo', 
				'$attentionTo', 
				$through, 
				'$remarks', 
				'$status', 
				'$approveLevels', 
				'$session_companyId',
				'$session_locationId',
				'$session_userId', 
				NOW()
				);";
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function updateHeader($gatePassNo,$gatePassYear,$approveLevels,$status,$date,$gatePassTo,$attentionTo,$remarks,$through)
	{
		global $session_userId;
		global $session_companyId;
		global $session_locationId;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "UPDATE general_gate_pass_header 
				SET
				DATE = '$date' , 
				GATEPASS_TO = '$gatePassTo' , 
				ATTENSION_TO = '$attentionTo' , 
				THROUGH_ID = $through , 
				REMARK = '$remarks' , 
				STATUS = '$status' , 
				APPROVE_LEVEL = '$approveLevels' , 
				COMPANY_ID = '$session_companyId' , 
				LOCATION_ID = '$session_locationId' , 
				MODIFIED_BY = '$session_userId'
				WHERE
				GATEPASS_NO = '$gatePassNo' AND GATEPASS_YEAR = '$gatePassYear' ";
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function deleteDetails($gatePassNo,$gatePassYear)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "DELETE FROM general_gate_pass_details 
				WHERE
				GATEPASS_NO = '$gatePassNo' AND 
				GATEPASS_YEAR = '$gatePassYear' ";
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function saveDetails($gatePassNo,$gatePassYear,$itemId,$description,$size,$serialNo,$unit,$qty,$balQty,$returnable,$returnableDate)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "INSERT INTO general_gate_pass_details 
				(
				GATEPASS_NO, 
				GATEPASS_YEAR, 
				ITEM_ID, 
				DESCRIPTION, 
				SIZE, 
				SERIAL_NO, 
				UNIT_ID, 
				QTY, 
				BAL_RETURN_QTY,
				RETURNABLE_FLAG, 
				RETURNABLE_DATE
				)
				VALUES
				(
				'$gatePassNo', 
				'$gatePassYear', 
				'$itemId', 
				'$description', 
				'$size', 
				'$serialNo', 
				'$unit', 
				$qty,
				$balQty, 
				'$returnable', 
				$returnableDate
				) ";
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function updateHeaderStatus($gatePassNo,$gatePassYear,$status)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		if($status=='') 
			$para='STATUS-1';
 		else
			$para=$status;
		
		$sql = "UPDATE general_gate_pass_header 
				SET
				STATUS = ".$para."
				WHERE
				GATEPASS_NO = '$gatePassNo' AND 
				GATEPASS_YEAR = '$gatePassYear' ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
	private function approvedData($gatePassNo,$gatePassYear)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_userId;
		global $finalApproval;
		
		$header_arr 	= $this->getHeaderData($gatePassNo,$gatePassYear);
		$status			= $header_arr['STATUS'];
		$savedLevels	= $header_arr['APPROVE_LEVEL'];
		
		if($status==1)
			$finalApproval = true;	
			
		$approval		= $savedLevels+1-$status;
		$this->approved_by_insert($gatePassNo,$gatePassYear,$session_userId,$approval);
	}
	private function updateMaxStatus($gatePassNo,$gatePassYear)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sqlMax = " SELECT (MAX(tb.STATUS)+1) AS maxStatus  
					FROM general_gate_pass_approvedby AS tb
					WHERE tb.GATEPASS_NO='$gatePassNo' AND
					tb.GATEPASS_YEAR='$gatePassYear' ";
		
		$resultMax 	= $this->db->RunQuery2($sqlMax);
		$rowMax		= mysqli_fetch_array($resultMax);
		$maxStatus 	= $rowMax['maxStatus'];
		
		$sqlUpd = " UPDATE general_gate_pass_approvedby 
					SET
					STATUS = '$maxStatus'
					WHERE
					GATEPASS_NO = '$gatePassNo' AND 
					GATEPASS_YEAR = '$gatePassYear' AND
					STATUS = 0 ";
		
		$resultUpd = $this->db->RunQuery2($sqlUpd);
		if(!$resultUpd)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sqlUpd;
			}
		}
	}
	private function approved_by_insert($gatePassNo,$gatePassYear,$session_userId,$approval)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "INSERT INTO general_gate_pass_approvedby 
				(
				GATEPASS_NO, 
				GATEPASS_YEAR, 
				APPROVE_LEVEL_NO, 
				APPROVED_BY, 
				APPROVED_DATE, 
				STATUS
				)
				VALUES
				(
				'$gatePassNo', 
				'$gatePassYear', 
				'$approval', 
				'$session_userId', 
				NOW(), 
				0
				) ";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			if($savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged	= $this->db->errormsg;
				$error_sql		= $sql;
			}
		}
	}
}
?>
<?php
$session_companyId 	= $_SESSION["headCompanyId"];
$session_userId		= $_SESSION["userId"];

include_once  "../../../class/cls_commonErrorHandeling_get.php";

$obj_errorHandeling = new cls_commonErrorHandeling_get($db);

class Cls_GeneralGatePass_Get
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	public function validateBeforeApprove($gatePassNo,$gatePassYear,$programCode)
	{
		global $obj_errorHandeling;
		global $session_userId;
		
		$header_arr	= $this->getHeaderData($gatePassNo,$gatePassYear);
		$response 	= $obj_errorHandeling->get_permision_withApproval_confirm($header_arr['STATUS'],$header_arr['APPROVE_LEVEL'],$session_userId,$programCode,'RunQuery');
		
		return $response;
	}
	public function validateBeforeReject($gatePassNo,$gatePassYear,$programCode)
	{
		global $obj_errorHandeling;
		global $session_userId;
		
		$header_arr	= $this->getHeaderData($gatePassNo,$gatePassYear);
		$response 	= $obj_errorHandeling->get_permision_withApproval_reject($header_arr['STATUS'],$header_arr['APPROVE_LEVEL'],$session_userId,$programCode,'RunQuery');
		
		return $response;
	}
	public function validateBeforeCancel($gatePassNo,$gatePassYear,$programCode)
	{
		global $obj_errorHandeling;
		global $session_userId;
		
		$header_arr	= $this->getHeaderData($gatePassNo,$gatePassYear);
		$response 	= $obj_errorHandeling->get_permision_withApproval_cancel($header_arr['STATUS'],$header_arr['APPROVE_LEVEL'],$session_userId,$programCode,'RunQuery');
		
		return $response;
	}
	public function validateBeforeRevise($gatePassNo,$gatePassYear,$programCode)
	{
		global $obj_errorHandeling;
		global $session_userId;
		
		$header_arr	= $this->getHeaderData($gatePassNo,$gatePassYear);
		$response 	= $obj_errorHandeling->get_permision_withApproval_revise($header_arr['STATUS'],$header_arr['APPROVE_LEVEL'],$session_userId,$programCode,'RunQuery');
		
		return $response;
	}
	public function loadHeaderData($gatePassNo,$gatePassYear)
	{
		return $this->getHeaderData($gatePassNo,$gatePassYear);
	}
	public function loadDetailData($gatePassNo,$gatePassYear)
	{
		return $this->loadDetailData_sql($gatePassNo,$gatePassYear);
	}
	public function getThroughCombo($throughId)
	{
		$sql = $this->through_sql();
		$html = $this->through_combo($sql,$throughId);
		return $html;
	}
	public function getRptHeaderData($gatePassNo,$gatePassYear)
	{
		return $this->loadRptHeaderData_sql($gatePassNo,$gatePassYear);
	}
	public function getRptDetailData($gatePassNo,$gatePassYear)
	{
		return $this->loadDetailData_sql($gatePassNo,$gatePassYear);
	}
	public function getRptApproveDetails($gatePassNo,$gatePassYear)
	{
		return $this->getRptApproveDetails_sql($gatePassNo,$gatePassYear);
	}
	public function getMaxItemId($gatePassNo,$gatePassYear)
	{
		return $this->getMaxItemId_sql($gatePassNo,$gatePassYear);
	}
	private function getHeaderData($gatePassNo,$gatePassYear)
	{
		$sql = "SELECT DATE, 
				GATEPASS_TO, 
				ATTENSION_TO, 
				THROUGH_ID, 
				REMARK, 
				STATUS, 
				APPROVE_LEVEL, 
				COMPANY_ID, 
				LOCATION_ID 
				FROM 
				general_gate_pass_header
				WHERE GATEPASS_NO='$gatePassNo' AND
				GATEPASS_YEAR='$gatePassYear' ";
		
		$result = $this->db->RunQuery($sql);
		$row 	= mysqli_fetch_array($result);
		
		return $row;
	}
	private function loadDetailData_sql($gatePassNo,$gatePassYear)
	{
		$sql = "SELECT ITEM_ID, 
				DESCRIPTION, 
				SIZE, 
				SERIAL_NO, 
				UNIT_ID, 
				MU.strCode,
				QTY, 
				RETURNABLE_FLAG, 
				RETURNABLE_DATE
				FROM 
				general_gate_pass_details
				inner join mst_units MU on MU.intId=general_gate_pass_details.UNIT_ID
				WHERE GATEPASS_NO='$gatePassNo' AND
				GATEPASS_YEAR='$gatePassYear' ";
		
		$result = $this->db->RunQuery($sql);
		
		return $result;
	}
	private function through_sql()
	{
		$sql = "SELECT ID,NAME
				FROM mst_gatepass_through_list
				WHERE STATUS = 1
				ORDER BY NAME ";
		return $sql;
	}
	private function through_combo($sql,$throughId)
	{
		$result = $this->db->RunQuery($sql);
		$string ="<option value=\"\"></option>";
		while($row = mysqli_fetch_array($result))
		{
			if($row['ID']==$throughId)
				$string.="<option value=\"".$row['ID']."\" selected=\"selected\">".$row['NAME']."</option>";
			else
				$string.="<option value=\"".$row['ID']."\" >".$row['NAME']."</option>";
		}
		return $string;	
	}
	private function loadRptHeaderData_sql($gatePassNo,$gatePassYear)
	{
		$sql = "SELECT GPH.GATEPASS_NO,
				GPH.GATEPASS_YEAR,
				GPTL.NAME AS throughName,
				GPH.DATE,
				GPH.GATEPASS_TO,
				GPH.ATTENSION_TO,
				GPH.REMARK,
				GPH.STATUS,
				GPH.APPROVE_LEVEL,
				GPH.APPROVE_LEVEL as LEVELS,
				GPH.CREATED_BY,
				SU.strUserName AS CREATOR,
				GPH.CREATED_DATE
				FROM general_gate_pass_header GPH
				INNER JOIN sys_users SU ON GPH.CREATED_BY = SU.intUserId
				INNER JOIN mst_gatepass_through_list GPTL ON GPTL.ID = GPH.THROUGH_ID
				WHERE GATEPASS_NO='$gatePassNo' AND
				GATEPASS_YEAR='$gatePassYear' ";
		
		$result = $this->db->RunQuery($sql);
		$row 	= mysqli_fetch_array($result);
		
		return $row;
	}
	private function getRptApproveDetails_sql($gatePassNo,$gatePassYear)
	{
		$sql = "SELECT
				GPAB.APPROVED_BY,
				GPAB.APPROVED_DATE AS dtApprovedDate,
				SU.strUserName AS UserName,
				GPAB.APPROVE_LEVEL_NO AS intApproveLevelNo
				FROM
				general_gate_pass_approvedby GPAB
				INNER JOIN sys_users SU ON GPAB.APPROVED_BY = SU.intUserId
				WHERE GPAB.GATEPASS_NO ='$gatePassNo' AND
				GPAB.GATEPASS_YEAR ='$gatePassYear'      
				ORDER BY GPAB.APPROVED_DATE ASC ";
		
		$result = $this->db->RunQuery($sql);
		
		return $result;
	}
	private function getMaxItemId_sql($gatePassNo,$gatePassYear)
	{
		$sql = "SELECT MAX(ITEM_ID) as maxItemId
				FROM general_gate_pass_details
				WHERE GATEPASS_NO='$gatePassNo' AND
				GATEPASS_YEAR='$gatePassYear' ";
		$result = $this->db->RunQuery($sql);
		$row 	= mysqli_fetch_array($result);
		
		return $row['maxItemId'];
	}
}
?>
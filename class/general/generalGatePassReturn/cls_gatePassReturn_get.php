<?php
ini_set('display_errors',0);

include_once  $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
include_once  $_SESSION['ROOT_PATH']."class/cls_commonErrorHandeling_get.php";

$session_userId			= $_SESSION["userId"];
$session_locationId 	= $_SESSION["CompanyID"];
$session_companyId 		= $_SESSION["headCompanyId"];
$savedMasseged			= "";
$error_sql				= "";

$obj_common				= new cls_commonFunctions_get($db);
$obj_errorHandeling 	= new cls_commonErrorHandeling_get($db);

class gatePassReturn_get
{
	private $db;
	
	function __construct($db)
	{
		$this->db	= $db;	
	}
	public function loadTable($gatePassNo,$gatePassYear)
	{
		return $this->loadTable_sql($gatePassNo,$gatePassYear);
	} 
	public function loadHeader($gatePassNo,$gatePassYear)
	{
		return $this->loadHeader_sql($gatePassNo,$gatePassYear);
	}
	public function validateSave($gatePassReturnNo,$gatePassReturnYear,$programCode,$userId)
	{
		return $this->validateBeforeSave($gatePassReturnNo,$gatePassReturnYear,$programCode,$userId);
	}
	public function get_rpt_header_array($gatePassReturnNo,$gatePassReturnYear,$executeType)
	{
		$sql = $this->loadReturnHeader_sql($gatePassReturnNo,$gatePassReturnYear);
		$result = $this->db->$executeType($sql);
		$row = mysqli_fetch_array($result);
		
		return $row;
	}
	public function load_reTable($gatePassReNo,$gatePassReYear)
	{
		return $this->load_ReTable_sql($gatePassReNo,$gatePassReYear);
	}
	public function get_details_result($gatePassReturnNo,$gatePassReturnYear,$executeType)
	{
		$sql = $this->loadReturnDetail_sql($gatePassReturnNo,$gatePassReturnYear);
		$result = $this->db->$executeType($sql);
		return $result;
	}
	
	public function get_Report_approval_details_result($serialNo,$year,$executeType){
		$sql = $this->Load_Report_approval_details_sql($serialNo,$year);
		$result = $this->db->$executeType($sql);
		return $result;
	}
	
	public function getMaxAppByStatus($gatePassReturnNo,$gatePassReturnYear,$executeType)
	{
		$sql = $this->Load_max_approve_by_sql($gatePassReturnNo,$gatePassReturnYear);
		$result = $this->db->$executeType($sql);
		return $result;
	}
	public function loadReturnHeader($gatePassReNo,$gatePassReYear)
	{
		return $this->loadReturnMHeader_sql($gatePassReNo,$gatePassReYear);
	}
	public function BalQty($gatepassNo,$gatepassYear,$itemID)
	{
		return $this->BalQty_sql($gatepassNo,$gatepassYear,$itemID);
	}
	//private functions
	private function validateBeforeSave($gatePassReturnNo,$gatePassReturnYear,$programCode,$userId)
	{
		global $obj_errorHandeling;
		global $savedStatus;
		global $savedMasseged;
		
		$header_arr		= $this->get_rpt_header_array($gatePassReturnNo,$gatePassReturnYear,'RunQuery2');
		$response 		= $obj_errorHandeling->get_permision_withApproval_save($header_arr['STATUS'],$header_arr['LEVELS'],$userId,$programCode,'RunQuery2');
	
		if($response["type"]=='fail' && ($savedStatus))
		{
			$savedStatus	= false;
			$savedMasseged 	= $response["msg"];
		}
		return $response;
	}
	private function loadReturnHeader_sql($gatePassReturnNo,$gatePassReturnYear)
	{
		$sql	="  SELECT
					general_gate_pass_return_header.DATE,
					general_gate_pass_return_header.GATEPASS_RETURN_YEAR,
					general_gate_pass_return_header.GATEPASS_RETURN_NO,
					general_gate_pass_return_header.REMARK,
					general_gate_pass_return_header.STATUS,
					general_gate_pass_return_header.APPROVE_LEVEL AS LEVELS,
					general_gate_pass_return_header.CREATED_BY,
					general_gate_pass_return_header.CREATED_DATE,
					sys_users.strUserName AS CREATOR,
					general_gate_pass_return_header.LOCATION_ID,
					general_gate_pass_return_header.COMPANY_ID,
					general_gate_pass_return_header.GATEPASS_NO,
					general_gate_pass_return_header.GATEPASS_YEAR,
					general_gate_pass_header.DATE AS GATEPASS_DATE,
					general_gate_pass_header.ATTENSION_TO,
					general_gate_pass_header.GATEPASS_TO
					FROM
					general_gate_pass_return_header
					INNER JOIN general_gate_pass_header ON general_gate_pass_return_header.GATEPASS_NO = general_gate_pass_header.GATEPASS_NO AND general_gate_pass_return_header.GATEPASS_YEAR = general_gate_pass_header.GATEPASS_YEAR
					INNER JOIN sys_users ON sys_users.intUserId=general_gate_pass_return_header.CREATED_BY
					WHERE
					general_gate_pass_return_header.GATEPASS_RETURN_NO = '$gatePassReturnNo' AND
					general_gate_pass_return_header.GATEPASS_RETURN_YEAR = '$gatePassReturnYear'";	
		
		return $sql;
	}
	
	private function loadReturnDetail_sql($gatePassReturnNo,$gatePassReturnYear)
	{
		$sql = "SELECT
				general_gate_pass_return_details.RETURN_DATE,
				general_gate_pass_return_details.QTY,
				general_gate_pass_return_details.ITEM_ID,
				general_gate_pass_details.DESCRIPTION,
				general_gate_pass_details.SIZE,
				general_gate_pass_details.ITEM_ID,
				general_gate_pass_details.UNIT_ID,
				mst_units.intId,
				mst_units.strCode,
				general_gate_pass_details.SERIAL_NO
				FROM
				general_gate_pass_return_details
				INNER JOIN general_gate_pass_return_header ON general_gate_pass_return_details.GATEPASS_RETURN_NO = general_gate_pass_return_header.GATEPASS_RETURN_NO AND general_gate_pass_return_details.GATEPASS_RETURN_YEAR = general_gate_pass_return_header.GATEPASS_RETURN_YEAR
				INNER JOIN general_gate_pass_details ON general_gate_pass_return_header.GATEPASS_NO = general_gate_pass_details.GATEPASS_NO AND general_gate_pass_return_header.GATEPASS_YEAR = general_gate_pass_details.GATEPASS_YEAR AND general_gate_pass_return_details.ITEM_ID = general_gate_pass_details.ITEM_ID
				INNER JOIN mst_units ON general_gate_pass_details.UNIT_ID = mst_units.intId
				WHERE
				general_gate_pass_return_details.GATEPASS_RETURN_NO = '$gatePassReturnNo' AND
				general_gate_pass_return_details.GATEPASS_RETURN_YEAR = '$gatePassReturnYear'";
				
		return $sql;	
	}	
	private function loadHeader_sql($gatePassNo,$gatePassYear)
	{
	$sql	= " SELECT 	
				gh.GATEPASS_NO, 
				gh.GATEPASS_YEAR, 
				gh.DATE, 
				gh.GATEPASS_TO, 
				gh.ATTENSION_TO, 
				gt.NAME, 
				gh.REMARK
				 
				FROM 
				general_gate_pass_header gh,mst_gatepass_through_list gt
				
				WHERE	
				gh.GATEPASS_NO = $gatePassNo AND gh.GATEPASS_YEAR= $gatePassYear AND gh.THROUGH_ID	= gt.ID ";
	
	$result	= $this->db->RunQuery($sql);
	$row	= mysqli_fetch_array($result);
	return $row;
	}
	private function loadTable_sql($gatePassNo,$gatePassYear)
	{
		$sql	= " SELECT 	
					gd.DESCRIPTION, 
					gd.SIZE, 
					gd.SERIAL_NO,
					gd.ITEM_ID,
					u.strName, 
					gd.BAL_RETURN_QTY,
					gd.RETURNABLE_DATE
					 
					FROM 
					general_gate_pass_details gd,mst_units u
					
					WHERE 
					gd.GATEPASS_NO= $gatePassNo AND
					gd.GATEPASS_YEAR = $gatePassYear AND
					u.intId = gd.UNIT_ID AND
				    gd.RETURNABLE_FLAG = 1";
		//	die($sql);
		$result	= $this->db->RunQuery($sql);
		return $result;	
	}
	private function load_ReTable_sql($gatePassReNo,$gatePassReYear)
	{
		$sql	= " SELECT
				  	GRD.QTY,
					GRD.ITEM_ID,
				  	GGD.BAL_RETURN_QTY,
				  	GGD.DESCRIPTION,  
				  	GGD.SERIAL_NO,
				  	GGD.SIZE ,
					GGD.RETURNABLE_DATE,
					GRD.RETURN_DATE, 
				  	MU.strCode   
					FROM general_gate_pass_return_header GRH
				  	INNER JOIN general_gate_pass_return_details GRD
					ON GRH.GATEPASS_RETURN_NO = GRD.GATEPASS_RETURN_NO
					AND GRH.GATEPASS_RETURN_YEAR = GRD.GATEPASS_RETURN_YEAR
				  	INNER JOIN general_gate_pass_details GGD
					ON GGD.GATEPASS_NO = GRH.GATEPASS_NO
					AND GGD.GATEPASS_YEAR = GRH.GATEPASS_YEAR
					AND GRD.ITEM_ID = GGD.ITEM_ID
				  	INNER JOIN mst_units    MU
				  	ON   MU.intId  = GGD.UNIT_ID  
					WHERE GRH.GATEPASS_RETURN_NO = '$gatePassReNo'
					AND GRH.GATEPASS_RETURN_YEAR = '$gatePassReYear'";
					//die($sql);
		$result	= $this->db->RunQuery($sql);
		return $result;	
	}
	
	private function Load_Report_approval_details_sql($serialNo,$year){
		$sql = "SELECT
				general_gate_pass_return_approvedby.APPROVED_BY,
				general_gate_pass_return_approvedby.APPROVED_DATE,
				general_gate_pass_return_approvedby.APPROVED_DATE as dtApprovedDate,
				sys_users.strUserName as UserName,
				general_gate_pass_return_approvedby.APPROVE_LEVEL_NO ,
				general_gate_pass_return_approvedby.APPROVE_LEVEL_NO as intApproveLevelNo
				FROM
				general_gate_pass_return_approvedby
				Inner Join sys_users ON general_gate_pass_return_approvedby.APPROVED_BY = sys_users.intUserId
				WHERE
				general_gate_pass_return_approvedby.GATEPASS_RETURN_NO =  '$serialNo' AND
				general_gate_pass_return_approvedby.GATEPASS_RETURN_YEAR =  '$year'      order by APPROVED_DATE asc";
		return $sql;
	}
	private function loadReturnMHeader_sql($gatePassReNo,$gatePassReYear)
	{
		$sql = "SELECT REMARK,DATE,GATEPASS_NO,GATEPASS_YEAR
				FROM general_gate_pass_return_header
				WHERE GATEPASS_RETURN_NO = '$gatePassReNo' AND
				GATEPASS_RETURN_YEAR = '$gatePassReYear' ";
		
		$result	= $this->db->RunQuery($sql);
		return mysqli_fetch_array($result);	
	}
	private function BalQty_sql($gatepassNo,$gatepassYear,$itemID)
	{
		$sqlBal		= " SELECT 	 
						BAL_RETURN_QTY 
						FROM 
						general_gate_pass_details 
						WHERE	
						GATEPASS_NO = '$gatepassNo' AND
						GATEPASS_YEAR = '$gatepassYear' AND ITEM_ID='$itemID'";	
		$resultBal	= $this->db->RunQuery2($sqlBal);
		$row		= mysqli_fetch_array($resultBal);
		return $row;
	}


}
?>
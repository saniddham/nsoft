<?php
class GatePassReturn_Set
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}	
	//public functions
	public function updateQTY($gatePassReturnNo,$gatePassReturnYear)
	{
		return $this->updateQTY_sql($gatePassReturnNo,$gatePassReturnYear);
	}
	public function saveReturnDetail($returnNo,$returnYear,$itemID,$qty,$returnDate)
	{
		return $this->saveReturnDetail_sql($returnNo,$returnYear,$itemID,$qty,$returnDate);
	}
	public function saveReturnHeader($gatePassNo,$returnNo,$gatePassYear,$returnYear,$date,$returnRemarks,$approveLevel,$status,$companyId,$locationId,$userId)
	{
		return $this->saveReturnHeader_sql($gatePassNo,$returnNo,$gatePassYear,$returnYear,$date,$returnRemarks,$approveLevel,$status,$companyId,$locationId,$userId);	
	}
	public function updateReturnHeader($gatepassreturnNo,$returnYear,$date,$returnRemarks,$status,$userId)
	{
		return $this->updateReturnHeader_sql($gatepassreturnNo,$returnYear,$date,$returnRemarks,$status,$userId);
	}
	public function deleteDetail($gatepassreturnNo,$returnYear)
	{
		return $this->deleteReturnDetail_sql($gatepassreturnNo,$returnYear);	
	}
	public function insertApprovedByReturn($gatePassReturnNo,$gatePassReturnYear,$userId,$approval)
	{
		return $this->insertApprovedByReturn_sql($gatePassReturnNo,$gatePassReturnYear,$userId,$approval);	
	}
	public function updateStatus($gatePassReturnNo,$gatePassReturnYear,$status)
	{
		return $this->updateStatus_sql($gatePassReturnNo,$gatePassReturnYear,$status);	
	}
	public function updateMaxStatus($gatepassreturnNo,$returnYear)
	{
		return $this->updateMaxStatus_sql($gatepassreturnNo,$returnYear);	
	}
	//private functions
	
	private function updateQTY_sql($gatePassReturnNo,$gatePassReturnYear)
	{
		$sql1	= 	"SELECT
					general_gate_pass_return_details.ITEM_ID,
					general_gate_pass_return_details.QTY,
					general_gate_pass_return_header.GATEPASS_NO,
					general_gate_pass_return_header.GATEPASS_YEAR
					FROM
					general_gate_pass_return_details
					INNER JOIN general_gate_pass_return_header ON general_gate_pass_return_details.GATEPASS_RETURN_NO = general_gate_pass_return_header.GATEPASS_RETURN_NO AND general_gate_pass_return_details.GATEPASS_RETURN_YEAR = general_gate_pass_return_header.GATEPASS_RETURN_YEAR
					WHERE
					general_gate_pass_return_details.GATEPASS_RETURN_NO = '$gatePassReturnNo' AND
					general_gate_pass_return_details.GATEPASS_RETURN_YEAR = '$gatePassReturnYear'";
					
		$result	= 	$this->db->RunQuery2($sql1); 
		while($row	= mysqli_fetch_array($result))
		{
			$qty			= $row['QTY'];
			$gatePassNo		= $row['GATEPASS_NO'];
			$gatePassYear 	= $row['GATEPASS_YEAR'];
			$itemID			= $row['ITEM_ID'];
			
			$balQTY_sql	= " SELECT
							general_gate_pass_details.BAL_RETURN_QTY
							FROM
							general_gate_pass_details
							WHERE
							general_gate_pass_details.GATEPASS_NO = '$gatePassNo' AND
							general_gate_pass_details.GATEPASS_YEAR = '$gatePassYear' AND
							general_gate_pass_details.ITEM_ID = '$itemID'";
			$result1 = $this->db->RunQuery2($balQTY_sql);	
			$row1	= mysqli_fetch_array($result1);
			$balQTY = $row1['BAL_RETURN_QTY'];
				
				if($balQTY >= $qty)
				{
					$sql	=  "UPDATE general_gate_pass_details 
					
								SET
								BAL_RETURN_QTY = BAL_RETURN_QTY - $qty  
									
								WHERE
								GATEPASS_NO = $gatePassNo AND GATEPASS_YEAR = $gatePassYear AND ITEM_ID = $itemID";	
					
					$result	=  $this->db->RunQuery2($sql);
					if(!$result)
					{
						$data['savedStatus']	= 'fail';
						$data['savedMassege']	= $this->db->errormsg;
						$data['error_sql']		= $sql;	
					}
					//return $data;
				}
				else
				{
					$data['savedStatus']	= 'fail';
					$data['savedMassege']	= 'Balance quantity lower than returned quantity';
					$data['error_sql']		= $sql;
					
					//return $data;		
				}	
		}
		
		return $data;
	}
	private function saveReturnDetail_sql($returnNo,$returnYear,$itemID,$qty,$returnDate)
	{
		$sql		= " INSERT INTO general_gate_pass_return_details 
						(
						GATEPASS_RETURN_NO, 
						GATEPASS_RETURN_YEAR, 
						ITEM_ID, 
						QTY,
						RETURN_DATE
						)
						VALUES
						(
						'$returnNo', 
						$returnYear, 
						$itemID, 
						$qty,
						'$returnDate'
						)";
		$result	=  $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;	
		}	
		return $data;
	}
	private function saveReturnHeader_sql($gatePassNo,$returnNo,$gatePassYear,$returnYear,$date,$returnRemarks,$approveLevel,$status,$companyId,$locationId,$userId)
	{
		$sql	= " INSERT INTO general_gate_pass_return_header 
					(
					GATEPASS_RETURN_NO, 
					GATEPASS_RETURN_YEAR, 
					GATEPASS_NO, 
					GATEPASS_YEAR, 
					DATE, 
					REMARK, 
					STATUS, 
					APPROVE_LEVEL, 
					COMPANY_ID, 
					LOCATION_ID, 
					CREATED_BY, 
					CREATED_DATE 
					)
					VALUES
					(
					$returnNo, 
					$returnYear,
					$gatePassNo,
					$gatePassYear, 
					'$date', 
					'$remarks',
					$status,
					$approveLevel, 
					$companyId, 
					$locationId, 
					$userId, 
					NOW()
					)";
		$result	=  $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;	
		}
		return $data;
	}
	private function deleteDetailRecord_sql($gatePassNo,$gatePassYear,$itemID)
	{
		$sql	= " DELETE FROM general_gate_pass_details 
					WHERE
					GATEPASS_NO = $gatePassNo 
					AND 
					GATEPASS_YEAR = $gatePassYear 
					AND 
					ITEM_ID = $itemID ";
					
		$result	=  $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;

	}
	private function deleteHeaderRecord_sql($gatePassNo,$gatePassYear)
	{
		$sql	= " DELETE FROM general_gate_pass_header 
					WHERE
					GATEPASS_NO = $gatePassNo 
					AND 
					GATEPASS_YEAR = $gatePassYear";
		
		$result	=  $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
			
	}
	private function updateReturnHeader_sql($gatepassreturnNo,$returnYear,$date,$returnRemarks,$status,$userId)
	{
		$sql	= " UPDATE general_gate_pass_return_header 
					SET
					DATE = '$date' , 
					REMARK = '$returnRemarks' , 
					STATUS = APPROVE_LEVEL + 1,
					MODIFIED_BY = '$userId' , 
					MODIFIED_DATE = NOW()
					
					WHERE
					GATEPASS_RETURN_NO = '$gatepassreturnNo' AND GATEPASS_RETURN_YEAR = '$returnYear' ;";
		
		$result	=  $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;	
			
	}
	private function deleteReturnDetail_sql($gatepassreturnNo,$returnYear)
	{
		$sql	= " DELETE
		
					FROM general_gate_pass_return_details
					
					WHERE GATEPASS_RETURN_NO = '$gatepassreturnNo'
					AND GATEPASS_RETURN_YEAR = '$returnYear'";
					
		$result	=  $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
		
	}
	private function insertApprovedByReturn_sql($gatePassReturnNo,$gatePassReturnYear,$userId,$approval)
	{
		$sql 	= " INSERT INTO general_gate_pass_return_approvedby 
					(GATEPASS_RETURN_NO, 
					GATEPASS_RETURN_YEAR, 
					APPROVE_LEVEL_NO, 
					APPROVED_BY, 
					APPROVED_DATE, 
					STATUS
					)
					VALUES
					('$gatePassReturnNo', 
					'$gatePassReturnYear', 
					'$approval', 
					'$userId', 
					 NOW(), 
					'0'
					);";
		$result	=  $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
			
	}
	private function updateStatus_sql($gatePassReturnNo,$gatePassReturnYear,$status)
	{
		if($status=='') 
			$para = 'STATUS-1';
 		else
			$para = $status;
			
		$sql = "UPDATE general_gate_pass_return_header 
				SET
				STATUS = $para 	
				WHERE
				GATEPASS_RETURN_NO = '$gatePassReturnNo' AND GATEPASS_RETURN_YEAR = '$gatePassReturnYear' ";
				
		$result	=  $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function updateMaxStatus_sql($gatepassreturnNo,$returnYear)
	{
		$sqlMax = " SELECT (MAX(tb.STATUS)+1) AS maxStatus  
					FROM general_gate_pass_return_approvedby AS tb
					WHERE tb.GATEPASS_RETURN_NO='$gatepassreturnNo' AND
					tb.GATEPASS_RETURN_YEAR='$returnYear' ";
		
		$resultMax 	= $this->db->RunQuery2($sqlMax);
		$rowMax		= mysqli_fetch_array($resultMax);
		$maxStatus 	= $rowMax['maxStatus'];
		
		$sqlUpd = " UPDATE general_gate_pass_return_approvedby 
					SET
					STATUS = '$maxStatus'
					WHERE
					GATEPASS_RETURN_NO = '$gatepassreturnNo' AND 
					GATEPASS_RETURN_YEAR = '$returnYear' ";
		
		$resultUpd = $this->db->RunQuery2($sqlUpd);
		if(!$resultUpd)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
}
?>
<?php
 //ini_set('display_errors', 1); 
 
$session_userId		= $_SESSION["userId"];
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];
$progrmCode			= 'P0220';

class Cls_purchase_order_Get
{
	private $db;
 	
	function __construct($db)
	{
		$this->db = $db;
 	}

//BEGIN - PUBLIC FUNCTIONS {	
	public function get_maxAmount_array($serialYear,$serialNo)
	{
		
		$sql = $this->load_maxAmount_sql($serialYear,$serialNo);
		$result = $this->db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return $row;
	}
	public function get_maxAmount_array2($serialYear,$serialNo,$executeType)
	{
		
		$sql = $this->load_maxAmount_sql($serialYear,$serialNo);
		$result = $this->db->$executeType($sql);
		$row = mysqli_fetch_array($result);
		return $row;
	}
	
 	public function get_finalApprovalPermision($serialYear,$serialNo,$shipmentTerm,$intUser)
	{
		$row = $this->get_maxAmount_array($serialYear,$serialNo);
 		$maxAmount 	= $row['amount'];
		$category 	= $row['intMainCategory'];
		
		$sql = $this->get_permision_sql($category,$shipmentTerm,$maxAmount,$intUser);
		$result = $this->db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$val 	= $row['userList'];
		if($val!=0)
		$val=1;
		
		return $val;
	}
	
 	public function get_finalApprovalPermision_2($serialYear,$serialNo,$shipmentTerm,$intUser,$executeType)
	{
		$row = $this->get_maxAmount_array2($serialYear,$serialNo,$executeType);
 		$maxAmount 	= $row['amount'];
		$category 	= $row['intMainCategory'];
		
		$sql = $this->get_permision_sql($category,$shipmentTerm,$maxAmount,$intUser,$executeType);
		$result = $this->db->$executeType($sql);
		$row = mysqli_fetch_array($result);
		$val 	= $row['userList'];
		if($val!=0)
		$val=1;
		
		return $val;
	}
	
 	public function get_permited_user_list($serialYear,$serialNo,$shipmentTerm)
	{
		$row = $this->get_maxAmount_array($serialYear,$serialNo);
 		$maxAmount 	= $row['amount'];
		$category 	= $row['intMainCategory'];
		
		$sql = $this->get_permision_group_sql($category,$shipmentTerm,$maxAmount);
		$result = $this->db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$list 	= $row['userList'];
 		
		return $list;
	}
	
	public function get_header($serialNo,$serialYear,$executeType)
	{
		$sql	= $this->get_header_sql($serialNo,$serialYear);
		$row	= $this->get_row($sql,$executeType);
		return $row;
	}
	
	public function get_details_results($serialNo,$serialYear,$executeType)
	{
		$sql	= $this->get_details_sql($serialNo,$serialYear);
		$result	= $this->get_results($sql,$executeType);
		return $result;
	}
	public function get_details_service_po_results($serialNo,$serialYear,$executeType)
	{
		$sql	= $this->get_details_service_po_results_sql($serialNo,$serialYear);
		$result	= $this->get_results($sql,$executeType);
		return $result;
	}
	public function checkBudgetValidatedLocation($deliveryToLoc,$executeType)
	{
		$sql	= $this->checkBudgetValidatedLocation_sql($deliveryToLoc);
		$result	= $this->db->$executeType($sql);
		$row	= mysqli_fetch_array($result);
		
		if($row['BUDGET_PO_VALIDATION_FLAG']==1)
			return true;
		else
			return false;
	}
	public function getSubCategory($poItem,$executeType)
	{
		return $this->getSubCategory_sql($poItem,$executeType);
	}
	
 	private function load_maxAmount_sql($serialYear,$serialNo)
	{
		global $session_companyId;
 		
 	$sql="
		select 
		max(amount) as amount,
		intMainCategory
		from (
		SELECT mst_item.intMainCategory,
		Sum(trn_podetails.dblQty) as amount FROM trn_podetails 
		INNER JOIN mst_item ON mst_item.intId = trn_podetails.intItem WHERE
		trn_podetails.intPONo = '$serialNo' AND
		trn_podetails.intPOYear = '$serialYear'
		GROUP BY
		mst_item.intMainCategory 
		) as tb1";
 		return $sql;
 	}
	
	private function get_permision_sql($category,$shipmentTerm,$maxAmount,$intUser)
	{
		global $session_companyId;
		
  	$sql="
		SELECT
		IFNULL(GROUP_CONCAT(trn_poheader_approve_level_chart.USER_ID),0) as userList 
		FROM `trn_poheader_approve_level_chart`
		WHERE
		trn_poheader_approve_level_chart.MAIN_CATEGORY_ID = '$category' AND
		trn_poheader_approve_level_chart.SHIPMENT_TERM_ID = '$shipmentTerm' AND
		trn_poheader_approve_level_chart.MAX_AMOUNT >= '$maxAmount' AND 
		trn_poheader_approve_level_chart.USER_ID = '$intUser' 
		";
 		return $sql;
	}
	
	private function get_permision_group_sql($category,$shipmentTerm,$maxAmount)
	{
		global $session_companyId;
		
  		$sql="
		SELECT
		GROUP_CONCAT(trn_poheader_approve_level_chart.USER_ID) as userList 
		FROM `trn_poheader_approve_level_chart`
		WHERE
		trn_poheader_approve_level_chart.MAIN_CATEGORY_ID = '$category' AND
		trn_poheader_approve_level_chart.SHIPMENT_TERM_ID = '$shipmentTerm' AND
		trn_poheader_approve_level_chart.MAX_AMOUNT >= '$maxAmount' 
		";
 		return $sql;
	}
	
	private function get_header_sql($serialNo,$serialYear)
	{		
		$sql	= "SELECT 
					LO.intCompanyId					AS COMPANY_ID,
					PH.intCompany					AS LOCATION_ID,
					LO.strName 						AS LOCATION_NAME,
					PH.intStatus					AS STATUS,
					PH.intApproveLevels 			AS LEVELS,
					PH.strRemarks					AS REMARKS,
					PH.intUser,
					PH.dtmCreateDate,
					PH.intModifiedBy,
					PH.dtmModifiedDate,
					PH.dtmPODate					AS DATE, 
					sys_users.strUserName AS USER
					FROM `trn_poheader` PH
					INNER JOIN sys_users ON PH.intUser = sys_users.intUserId
					INNER JOIN mst_locations LO ON LO.intId = PH.intCompany
					WHERE
					PH.intPONo = '$serialNo' AND
					PH.intPOYear = '$serialYear'";
		
		return $sql;		
	}

	private function get_details_sql($serialNo,$serialYear){
		
		 $sql	= "SELECT 
					mst_maincategory.intId 						AS MAIN_CATEGORY_ID,
					mst_maincategory.strName 					AS MAIN_CATEGORY_NAME,
					mst_item.intSubCategory						AS SUB_CATEGORY_ID,
					mst_subcategory.strName 					AS SUB_CATEGORY_NAME,
					trn_podetails.intItem						AS ITEM_ID,
					mst_item.strName 							AS ITEM_NAME,
					trn_podetails.dblQty						AS QTY 
					FROM `trn_podetails`  
					INNER JOIN trn_poheader ON trn_poheader.intPONo= trn_podetails.intPONo AND trn_poheader.intPOYear=trn_podetails.intPOYear
					LEFT JOIN mst_item ON trn_podetails.intItem = mst_item.intId 
					INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
					INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId 
					WHERE 
					trn_podetails.intPONo = '$serialNo' AND
					trn_podetails.intPOYear = '$serialYear' ";
		
		return $sql;
		
	}
	
	private function get_details_service_po_results_sql($serialNo,$serialYear){
		
		$service_main_cat	=3;
		
		 $sql	= "SELECT 
					mst_maincategory.intId 						AS MAIN_CATEGORY_ID,
					mst_maincategory.strName 					AS MAIN_CATEGORY_NAME,
					mst_item.intSubCategory						AS SUB_CATEGORY_ID,
					mst_subcategory.strName 					AS SUB_CATEGORY_NAME,
					trn_podetails.intItem						AS ITEM_ID,
					mst_item.strName 							AS ITEM_NAME,
					trn_podetails.dblQty						AS QTY 
					FROM `trn_podetails` 
					INNER JOIN trn_poheader ON trn_poheader.intPONo= trn_podetails.intPONo AND trn_poheader.intPOYear=trn_podetails.intPOYear
					LEFT JOIN mst_item ON trn_podetails.intItem = mst_item.intId 
					INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
					INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId 
					WHERE 
					trn_podetails.intPONo = '$serialNo' AND
					trn_podetails.intPOYear = '$serialYear' AND 
					mst_maincategory.intId = '$service_main_cat'";
		
		return $sql;
		
	}
	

	private function get_results($sql,$executeType){
	
		 $result	= $this->db->$executeType($sql);
		return $result;		
	}
	private function get_row($sql,$executeType){
		$result	= $this->db->$executeType($sql);
		$row=mysqli_fetch_array($result);
		return $row;		
	}
	private function checkBudgetValidatedLocation_sql($deliveryToLoc)
	{
		$sql = "SELECT BUDGET_PO_VALIDATION_FLAG
				FROM mst_locations
				WHERE intId = '$deliveryToLoc' ";
		
		return $sql;
	}
	private function getSubCategory_sql($itemId,$executionType)
	{
		$sql = "SELECT intSubCategory
				FROM mst_item
				WHERE intId = '$itemId' ";
		
		$result	= $this->db->$executionType($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row['intSubCategory'];
	}
 }

?>
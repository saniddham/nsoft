<?php
require_once $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
require_once $_SESSION['ROOT_PATH']."class/finance/bank/bank_payment/cls_bank_payment_get.php";
require_once $_SESSION['ROOT_PATH']."class/cls_commonErrorHandeling_get.php";
require_once "../../../../class/finance/cls_common_get.php";
include_once ("../../../../class/finance/accountant/journalEntry/cls_journalEntry_get.php");
//ini_set('display_errors', 1); 

$obj_commonErr			= new cls_commonErrorHandeling_get($db);
$obj_common				= new cls_commonFunctions_get($db);
$obj_bank_payment_get	= new Cls_bank_payment_Get($db);
$obj_common_finance		= new Cls_Common_Get($db);
$obj_fin_journal_get	= new Cls_JournalEntry_Get($db);

$session_userId		= $_SESSION["userId"];
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];
$progrmCode			= 'P0748';

class Cls_bank_payment_Set
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	
	public function Save($serialYear,$serialNo,$date,$currency,$bank,$paymentMethod,$referenceNo,$remarks,$grid)
	{
		global $obj_common;
		global $obj_common_finance;
		global $obj_bank_payment_get;
		global $obj_commonErr;
		global $obj_fin_journal_get;
		global $session_userId;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $session_companyId;
		global $session_locationId;
		global $progrmCode;
		
		$savedStatus	= true;
		
		$this->db->begin();
		
		$approveLevels 	= $obj_common->getApproveLevels('Bank Payment');
		$status			=$approveLevels+1;
		
		if($serialNo==''){
			$editMode=0;
			$journal_array 	= $obj_common->GetSystemMaxNo('intFin_BankPaymentNo',$session_locationId);
			$serialNo		= $journal_array["max_no"];
			$serialYear		= date('Y');
			$costCenter		= $obj_common_finance->getCompanyCostCenter($session_locationId,'RunQuery2');
 			$response		= $obj_common_finance->getPaymentVoucherNo($date,$bank,$costCenter,'RunQuery2');
			if($response['type']=='pass'){
				$voucherNo	=$response['voucherNo'];
			}
			else{
				$messageErr	= $response['msg']; 
 				$rollBack=1;
			}
		}
		else{
			$editMode=1;
 		}

		if($journal_array['rollBackFlag']==1){
			$messageErr 		= $journal_array['msg']; 
			$sqlErr	= $journal_array['q'];
			$rollBack=1;
 		}
 		
		if($rollBack != 1){
			if($editMode==0){
			$trans_response=$this->SaveHeader($serialYear,$serialNo,$date,$currency,$bank,$paymentMethod,$referenceNo,$remarks,$voucherNo,$approveLevels,$status);
 			}
			else{
			$trans_response=$this->UpdateHeader($serialYear,$serialNo,$date,$currency,$bank,$paymentMethod,$referenceNo,$remarks,$approveLevels,$status);
			}
			if($trans_response['savedStatus']=='fail'){
				$messageErr	= $trans_response['savedMasseged']; 
				$sqlErr		= $trans_response['error_sql'];
				$rollBack=1;
			}
		}
		
		$maxAppByStatus=$this->getMaxAppByStatus($serialYear,$serialNo);
		$maxAppByStatus=(int)$maxAppByStatus+1;
		$trans_response['savedStatus']=$this->approved_by_update($serialYear,$serialNo,$maxAppByStatus);
		if($trans_response['savedStatus']=='fail'){
			$messageErr	= $trans_response['savedMasseged']; 
			$sqlErr		= $trans_response['error_sql'];
			$rollBack=1;
		}
		
		$trans_response=$this->DeleteDetails($serialYear,$serialNo);
		if($trans_response['savedStatus']=='fail'){
			$messageErr	= $trans_response['savedMasseged']; 
			$sqlErr		= $trans_response['error_sql'];
			$rollBack=1;
		}
		
		if($rollBack != 1){
			$totAmm=0;
 			$i=0;
			foreach($grid as $array_loop)
			{
				$i++;
				
				$account			= $array_loop["account"];
				$taxCode			= $array_loop["taxCode"];
				$amount				= $array_loop["amount"];
				$remarks			= $obj_common->replace($array_loop["remarks"]);
				$payTo				= $array_loop["payTo"];
				$costCenter 		= $array_loop["costCenter"];
				$settelement		= $array_loop["settelement"];
				$settelement		= $obj_common->replace($settelement);
				$settelement 		= explode("/", $settelement);
				$settelementYear	= $settelement[0];  
				$settelementNo		= $settelement[1];  
				$settelementOrder	= $settelement[2]; 
				if($settelementNo==''){
					$settelementNo		="NULL";
					$settelementYear	="NULL";
					$settelementOrder	="NULL";
				}
				if($payTo==''){
 					$payTo=0;
				}
				$totAmm		+= $amount;
    				
				if($settelementNo!="NULL"){
					$totSetteAmount[$settelementNo][$settelementYear][$settelementOrder]+=$ammount;
					$balAmount	= $obj_fin_journal_get->get_balanceToSetele_jernal_amount($settelementYear,$settelementNo,$settelementOrder,'RunQuery2');
 					if(round($totSetteAmount[$settelementNo][$settelementYear][$settelementOrder],4) > round($balAmount,4)){
 						$rollBack=1;
						$messageErr	= "Can't exceed 'Balance to settele amount'"; 
						$sqlErr		= '';
					}
				}
			
			
				$trans_response=$this->SaveDetails($serialYear,$serialNo,$account,$taxCode,$amount,$remarks,$payTo,$costCenter,$settelementNo,$settelementYear,$settelementOrder,$i);
				if($trans_response['savedStatus']=='fail'){
					$messageErr	= $trans_response['savedMasseged']; 
					$sqlErr		= $trans_response['error_sql'];
					$rollBack=1;
				}
			}
			
			if($i==0){
					$rollBack=1;
					$messageErr	= "Error with Detail saving"; 
					$sqlErr		= '';
			}
		}
		
	
		if($rollBack==1){
			$this->db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= $messageErr;
			$response['sql']		= $sqlErr;
		}
		else if($i==0){
			$this->db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= "Details not saved";
			$response['sql']		= $sqlErr;
		}
		else if($savedStatus){
			$response['type'] 				= "pass";
			if($editMode==1)
			$response['msg'] 				= "Payment No : '$serialNo/$serialYear' updated successfully.";
			else
			$response['msg'] 				= "Payment No : '$serialNo/$serialYear' saved successfully.";
			$response_arr					= $obj_commonErr->get_permision_withApproval_confirm($status,$approveLevels,$session_userId,$progrmCode,'RunQuery2');
			$response['permision_confirm'] 	= $response_arr['permision'];
			
			$response['serialNo'] 	= $serialNo;
			$response['serialYear'] = $serialYear;
			$this->db->commit();
		}else{
			$this->db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= $messageErr;
			$response['sql']		= $sqlErr;
		}
		return json_encode($response);
	}

	public function Approve($serialYear,$serialNo)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $obj_common_get;
		global $session_userId;
		global $obj_bank_payment_get;
		global $obj_fin_journal_get;
		$savedStatus	= true;
		$this->db->begin();
		
 		
		$trans_response = $this->updateHeaderStatus($serialYear,$serialNo,'');
		if(!$savedStatus){
			$messageErr	= $savedMasseged; 
			$sqlErr		= $error_sql;
			$rollBack=1;
		}
		if($rollBack!=1){
   			$header_array	= $obj_bank_payment_get->get_header_array2($serialYear,$serialNo);
			$status		=$header_array['STATUS'];
			$savedLevels=$header_array['LEVELS'];
			$approval	=$savedLevels+1-$status;
			
			$trans_response = $this->approved_by_insert($serialYear,$serialNo,$session_userId,$approval);
			if(!$savedStatus){
				$messageErr	= $savedMasseged; 
				$sqlErr		= $error_sql;
				$rollBack=1;
			}
		}
		
		$result		= $obj_bank_payment_get->get_details_result_2($serialYear,$serialNo);
		while($row = mysqli_fetch_array($result)){
 			$settelementNo		= $row['SETTELEMENT_NO'];
			$settelementYear	= $row['SETTELEMENT_YEAR'];
			$settelementOrder	= $row['SETTELEMENT_ORDER'];
			$ammount			= $row['AMOUNT'];
			if($settelementNo!=NULL){//
  				$totSetteAmount[$settelementNo][$settelementYear][$settelementOrder]+=$ammount;
				$balAmount	= $obj_fin_journal_get->get_balanceToSetele_jernal_amount($settelementYear,$settelementNo,$settelementOrder,'RunQuery2');
				if(round($totSetteAmount[$settelementNo][$settelementYear][$settelementOrder],4) > round($balAmount,4)){
					$rollBack=1;
					$messageErr	= "Can't exceed 'Balance to settele amount'"; 
					$sqlErr		= '';
				}
				if($rollBack!=1){
					if($header_array['STATUS']==1){
 						$trans_response = $this->update_datail_balance($settelementNo,$settelementYear,$settelementOrder,$ammount);
						if($trans_response['savedStatus']=='fail'){
							$messageErr	= $trans_response['savedMasseged']; 
							$sqlErr		= $trans_response['error_sql'];
							$rollBack=1;
						}
					}
				}
			} 
		}
 			
		if(($rollBack!=1) && ($status==1)){
			$trans_response = $this->finance_transaction_insert($serialYear,$serialNo);
			if($trans_response['savedStatus']=='fail'){
				$messageErr	= $trans_response['savedMasseged']; 
				$sqlErr		= $trans_response['error_sql'];
				$rollBack=1;
			}
		}
			
			
		//$rollBack=1;
		if($rollBack==1){
			$this->db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= $messageErr;
			$response['sql']		= $sqlErr;
		}
		else if($savedStatus){
			$this->db->commit();
			$response['type'] 		= "pass";
   			$header_array	= $obj_bank_payment_get->get_header_array2($serialYear,$serialNo);
			if($header_array['STATUS']==1) 
			$response['msg'] 		= "Final Approval Raised successfully.";
 			else 
			$response['msg'] 		= "Approved successfully.";
 		}else{
			$this->db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= $messageErr;
			$response['sql']		= $sqlErr;
		}
		return json_encode($response);
	}

	public function Reject($serialYear,$serialNo)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $obj_common_get;
		global $session_userId;
		global $obj_bank_payment_get;
		global $obj_fin_journal_get;
		$savedStatus	= true;
		$this->db->begin();
		
 		
		$trans_response = $this->updateHeaderStatus($serialYear,$serialNo,'0');
		if(!$savedStatus){
			$messageErr	= $savedMasseged; 
			$sqlErr		= $error_sql;
			$rollBack=1;
		}
		if($rollBack!=1){
			$trans_response = $this->approved_by_insert($serialYear,$serialNo,$session_userId,0);
			if($trans_response['savedStatus']=='fail'){
				$messageErr	= $trans_response['savedMasseged']; 
				$sqlErr		= $trans_response['error_sql'];
				$rollBack=1;
			}
		}
   		
		if($rollBack==1){
			$this->db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= $messageErr;
			$response['sql']		= $sqlErr;
		}
		else if($savedStatus){
			$this->db->commit();
			$response['type'] 		= "pass";
 			$response['msg'] 		= "Rejected successfully.";
 		}else{
			$this->db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= $messageErr;
			$response['sql']		= $sqlErr;
		}
		return json_encode($response);
	}

	public function Cancel($serialYear,$serialNo)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		global $obj_common_get;
		global $session_userId;
		global $obj_bank_payment_get;
		$savedStatus	= true;
		$this->db->begin();
		
 		
		$trans_response = $this->updateHeaderStatus($serialYear,$serialNo,'-2');
		if($trans_response['savedStatus']=='fail'){
			$messageErr	= $trans_response['savedMasseged']; 
			$sqlErr		= $trans_response['error_sql'];
			$rollBack=1;
		}
		if($rollBack!=1){
			
			$result		= $obj_bank_payment_get->get_details_result_2($serialYear,$serialNo);
 			while($row	= mysqli_fetch_array($result)){
				$settelementNo		= $row['SETTELEMENT_NO'];
				$settelementYear	= $row['SETTELEMENT_YEAR'];
				$settelementOrder	= $row['SETTELEMENT_ORDER'];
				$ammount			= $row['AMOUNT'];
				$trans_response = $this->update_datail_balance($settelementNo,$settelementYear,$settelementOrder,-$ammount);
				if($trans_response['savedStatus']=='fail'){
					$messageErr	= $trans_response['savedMasseged']; 
					$sqlErr		= $trans_response['error_sql'];
					$rollBack=1;
				}
			}
 		}
		if($rollBack!=1){
			$trans_response = $this->approved_by_insert($serialYear,$serialNo,$session_userId,-2);
			if($trans_response['savedStatus']=='fail'){
				$messageErr	= $trans_response['savedMasseged']; 
				$sqlErr		= $trans_response['error_sql'];
				$rollBack=1;
			}
		}
		if($rollBack!=1){
			$trans_response = $this->finance_transaction_delete($serialYear,$serialNo);
			if($trans_response['savedStatus']=='fail'){
				$messageErr	= $trans_response['savedMasseged']; 
				$sqlErr		= $trans_response['error_sql'];
				$rollBack=1;
			}
		}
		
   		
		if($rollBack==1){
			$this->db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= $messageErr;
			$response['sql']		= $sqlErr;
		}
		else if($savedStatus){
			$this->db->commit();
			$response['type'] 		= "pass";
 			$response['msg'] 		= "Cancelled successfully.";
 		}else{
			$this->db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= $messageErr;
			$response['sql']		= $sqlErr;
		}
		return json_encode($response);
	}

	private function SaveHeader($serialYear,$serialNo,$date,$currency,$bank,$paymentMethod,$referenceNo,$remarks,$voucherNo,$approveLevels,$status)
	{
		global $session_userId;
		global $session_locationId;
		global $session_companyId;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "INSERT INTO finance_bank_payment_header
				(BANK_PAYMENT_NO, 
				BANK_PAYMENT_YEAR,
				VOUCHER_NO,
				BANK_CHART_OF_ACCOUNT_ID,
 				CURRENCY_ID, 
 				PAYMENT_METHOD, 
 				BANK_REFERENCE_NO, 
 				REMARKS, 
 				PAYMENT_DATE, 
 				STATUS, 
				APPROVE_LEVEL,
				COMPANY_ID,
				LOCATION_ID,
				CREATED_BY,
				CREATED_DATE 
				)
				VALUES
				('$serialNo', 
				'$serialYear',
				'$voucherNo',
				'$bank',
 				'$currency',
 				'$paymentMethod',
 				'$referenceNo',
 				'$remarks',
				'$date',
 				'$status',
 				'$approveLevels', 
				'$session_companyId',
				'$session_locationId',
				'$session_userId',
 				NOW());";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$data['savedStatus']	= 'fail';
			$data['savedMasseged']	= $this->db->errormsg;
			$data['error_sql']			= $sqlI;
		}
		return $data;
	}
	
	private function UpdateHeader($serialYear,$serialNo,$date,$currency,$bank,$paymentMethod,$referenceNo,$remarks,$approveLevels,$status)
	{
		global $session_userId;
		global $session_locationId;
		global $session_companyId;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "UPDATE finance_bank_payment_header 
				SET 
  				BANK_CHART_OF_ACCOUNT_ID='$bank',
 				CURRENCY_ID='$currency', 
 				PAYMENT_METHOD='$paymentMethod', 
 				BANK_REFERENCE_NO='$referenceNo', 
 				REMARKS='$remarks', 
  				PAYMENT_DATE='$date',
				STATUS='$status',
 				APPROVE_LEVEL='$approveLevels', 
 				COMPANY_ID='$session_companyId', 
 				LOCATION_ID='$session_locationId', 
				MODIFIED_BY='$session_userId',
				MODIFIED_DATE=NOW()
				WHERE
				finance_bank_payment_header.BANK_PAYMENT_NO = '$serialNo' AND
				finance_bank_payment_header.BANK_PAYMENT_YEAR = '$serialYear'";
		$result = $this->db->RunQuery2($sql);
			if(!$result){
				$data['savedStatus']	= 'fail';
				$data['savedMasseged']	= $this->db->errormsg;
				$data['error_sql']			= $sqlI;
			}
			return $data;
	}
	
	private function DeleteDetails($serialYear,$serialNo)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql	= "DELETE 
					FROM finance_bank_payment_details 
					WHERE
					finance_bank_payment_details.BANK_PAYMENT_NO = '$serialNo' AND
					finance_bank_payment_details.BANK_PAYMENT_YEAR = '$serialYear'";
		$result = $this->db->RunQuery2($sql);
			if(!$result){
				$data['savedStatus']	= 'fail';
				$data['savedMasseged']	= $this->db->errormsg;
				$data['error_sql']			= $sqlI;
			}
			return $data;
	}
	
	private function SaveDetails($serialYear,$serialNo,$account,$taxCode,$amount,$remarks,$payTo,$costCenter,$settelementNo,$settelementYear,$settelementOrder,$i)
	{
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql	= "INSERT INTO finance_bank_payment_details 
					(BANK_PAYMENT_NO, 
					BANK_PAYMENT_YEAR, 
					CHART_OF_ACCOUNT_ID,
					ORDER_BY_ID,
					TAX_ID,
					AMOUNT,
					PAY_TO,
					COST_CENTER_ID,
 					REMARKS,
					SETTELEMENT_NO,
					SETTELEMENT_YEAR,
					SETTELEMENT_ORDER)
					VALUES
					('$serialNo', 
					'$serialYear', 
					'$account',
					'$i',
					$taxCode,
					'$amount',
					'$payTo',
					'$costCenter',
					'$remarks',
					$settelementNo,
					$settelementYear,
					$settelementOrder);";
		$result = $this->db->RunQuery2($sql);
			if(!$result){
				$data['savedStatus']	= 'fail';
				$data['savedMasseged']	= $this->db->errormsg;
				$data['error_sql']			= $sqlI;
			}
			return $data;
	}
	
	function updateHeaderStatus($serialYear,$serialNo,$status){
		global $session_userId;
		global $session_locationId;
		global $session_companyId;
		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
 		
		if($status=='') 
			$para='STATUS-1';
 		else
			$para=$status;
		
		$sql = "UPDATE finance_bank_payment_header 
				SET 
				STATUS=".$para." 
				WHERE
				finance_bank_payment_header.BANK_PAYMENT_NO = '$serialNo' AND
				finance_bank_payment_header.BANK_PAYMENT_YEAR = '$serialYear'";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$data['savedStatus']	= 'fail';
			$data['savedMasseged']	= $this->db->errormsg;
			$data['error_sql']			= $sqlI;
		}
		return $data;
	}

	private function approved_by_insert($serialYear,$serialNo,$userId,$approval){
		global $session_userId;
		global $session_locationId;
		global $session_companyId;
		global $savedStatus;
		global $error_sql;
		global $savedMasseged;
		
		$sql = "INSERT INTO `finance_bank_payment_header_approveby` (`BANK_PAYMENT_NO`,`BANK_PAYMENT_YEAR`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
			VALUES ('$serialNo','$serialYear','$approval','$userId',now(),0)";
		$result = $this->db->RunQuery2($sql);
			if(!$result){
				$data['savedStatus']	= 'fail';
				$data['savedMasseged']	= $this->db->errormsg;
				$data['error_sql']			= $sqlI;
			}
			return $data;
	}
	
	private function getMaxAppByStatus($serialYear,$serialNo){
 		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
	   $sql = "SELECT
				max(finance_bank_payment_header_approveby.intStatus) as status 
				FROM
				finance_bank_payment_header_approveby
				WHERE
				finance_bank_payment_header_approveby.BANK_PAYMENT_NO =  '$serialNo' AND
				finance_bank_payment_header_approveby.BANK_PAYMENT_YEAR =  '$serialYear'";
				
		$resultm = $this->db->RunQuery2($sql);
		 $rowm=mysqli_fetch_array($resultm);
		 
 			 return $rowm['status'];
 	}
	
	private function approved_by_update($serialYear,$serialNo,$maxAppByStatus){
 		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
			$sql = "UPDATE `finance_bank_payment_header_approveby` SET intStatus ='$maxAppByStatus' 
					WHERE (`BANK_PAYMENT_NO`='$serialNo') AND (`BANK_PAYMENT_YEAR`='$serialYear') AND (`intStatus`='0')";
			$result = $this->db->RunQuery2($sql);
			if(!$result){
				$data['savedStatus']	= 'fail';
				$data['savedMasseged']	= $this->db->errormsg;
				$data['error_sql']			= $sqlI;
			}
			return $data;
	}
	
	private function finance_transaction_insert($serialYear,$serialNo){
		global $session_userId;
		global $session_locationId;
		global $session_companyId;
		global $savedStatus;
		global $error_sql;
		global $savedMasseged;
		
		$sql1 = "SELECT
				finance_bank_payment_details.BANK_PAYMENT_NO,
				finance_bank_payment_details.BANK_PAYMENT_YEAR,
				finance_bank_payment_details.CHART_OF_ACCOUNT_ID,
				finance_bank_payment_details.TAX_ID,
				finance_bank_payment_details.AMOUNT,
				finance_bank_payment_details.PAY_TO,
				finance_bank_payment_details.COST_CENTER_ID,
				finance_bank_payment_details.REMARKS as REMARKS_H,
				finance_bank_payment_header.BANK_PAYMENT_NO,
				finance_bank_payment_header.BANK_PAYMENT_YEAR,
				finance_bank_payment_header.BANK_CHART_OF_ACCOUNT_ID,
				finance_bank_payment_header.CURRENCY_ID,
				finance_bank_payment_header.PAYMENT_METHOD,
				finance_bank_payment_header.BANK_REFERENCE_NO,
				finance_bank_payment_header.REMARKS,
				finance_bank_payment_header.PAYMENT_DATE,
				finance_bank_payment_header.`STATUS`,
				finance_bank_payment_header.APPROVE_LEVEL,
				finance_bank_payment_header.COMPANY_ID,
				finance_bank_payment_header.LOCATION_ID,
				finance_bank_payment_header.CREATED_BY,
				finance_bank_payment_header.CREATED_DATE,
				finance_bank_payment_header.MODIFIED_BY,
				finance_bank_payment_header.MODIFIED_DATE
				FROM
				finance_bank_payment_details
				INNER JOIN finance_bank_payment_header ON finance_bank_payment_details.BANK_PAYMENT_NO = finance_bank_payment_header.BANK_PAYMENT_NO AND finance_bank_payment_details.BANK_PAYMENT_YEAR = finance_bank_payment_header.BANK_PAYMENT_YEAR

				WHERE
				finance_bank_payment_details.BANK_PAYMENT_NO = '$serialNo' AND
				finance_bank_payment_details.BANK_PAYMENT_YEAR = '$serialYear' 
				";
		$result1 = $this->db->RunQuery2($sql1);
		$i=0;
		$bankAmount	= 0;
		while($row=mysqli_fetch_array($result1))
		{
			$i++;
			$account		= $row['CHART_OF_ACCOUNT_ID'];
			$bank_acc		= $row['BANK_CHART_OF_ACCOUNT_ID'];
			$ammount		= $row['AMOUNT'];
 			$transType		= 'D';
 			$transCat		= 'BP';
			$docType 		= 'BANK_PAYMENT';
 			$refNo			= $row['BANK_REFERENCE_NO'];
			$curr			= $row['CURRENCY_ID'];
			$remarks		= $row['REMARKS'];
			$paymentDate	= $row["PAYMENT_DATE"];
			$bankAmount    += $row['AMOUNT'];
			
			$sqlI = "INSERT INTO `finance_transaction` (`CHART_OF_ACCOUNT_ID`,`AMOUNT`,`DOCUMENT_NO`,
			DOCUMENT_YEAR,DOCUMENT_TYPE,TRANSACTION_TYPE,TRANSACTION_CATEGORY,
			BANK_REFERENCE_NO,CURRENCY_ID,LOCATION_ID,
			COMPANY_ID,	LAST_MODIFIED_BY,LAST_MODIFIED_DATE) 
				VALUES ('$account','$ammount','$serialNo',
				'$serialYear','$docType','$transType','$transCat',
				'$refNo','$curr','$session_locationId',
				'$session_companyId','$session_userId','$paymentDate')";
			$result = $this->db->RunQuery2($sqlI);
			if(!$result){
				$data['savedStatus']	= 'fail';
				$data['savedMasseged']	= $this->db->errormsg;
				$data['error_sql']		= $sqlI;
			}
		}
		
		$sqlI = "INSERT INTO `finance_transaction` (`CHART_OF_ACCOUNT_ID`,`AMOUNT`,`DOCUMENT_NO`,
					DOCUMENT_YEAR,DOCUMENT_TYPE,TRANSACTION_TYPE,TRANSACTION_CATEGORY,
					BANK_REFERENCE_NO,CURRENCY_ID,LOCATION_ID,
					COMPANY_ID,	LAST_MODIFIED_BY,LAST_MODIFIED_DATE) 
						VALUES ('$bank_acc','$bankAmount','$serialNo',
						'$serialYear','$docType','C','$transCat',
						'$refNo','$curr','$session_locationId',
						'$session_companyId','$session_userId','$paymentDate')";
			$result = $this->db->RunQuery2($sqlI);
			if(!$result){
				$data['savedStatus']	= 'fail';
				$data['savedMasseged']	= $this->db->errormsg;
				$data['error_sql']		= $sqlI;
			}
			return $data;
	}
	private function finance_transaction_delete($serialYear,$serialNo){
		global $savedStatus;
		global $error_sql;
		global $savedMasseged;
	
		 $sql1 = "DELETE 
					FROM
					finance_transaction
 					WHERE
					finance_transaction.DOCUMENT_NO = '$serialNo' AND
					finance_transaction.DOCUMENT_YEAR = '$serialYear' AND 
					finance_transaction.DOCUMENT_TYPE = 'BANK_PAYMENT'";
		$result1 = $this->db->RunQuery2($sql1);
		
		if(!$result1){
			$data['savedStatus']	= 'fail';
			$data['savedMasseged']	= $this->db->errormsg;
			$data['error_sql']			= $sql1;
		}
		return $data;
	}
	private function update_datail_balance($serialNo,$serialYear,$orderId,$ammount){
 		global $savedStatus;
		global $savedMasseged;
		global $error_sql;
		
		$sql = "UPDATE `finance_journal_entry_details` SET BAL_AMOUNT =BAL_AMOUNT - '$ammount' 
					WHERE (`JOURNAL_ENTRY_NO`='$serialNo') AND (`JOURNAL_ENTRY_YEAR`='$serialYear') AND (`ORDER_ID`='$orderId')";
		$result = $this->db->RunQuery2($sql);
		if(!$result){
			$data['savedStatus']	= 'fail';
			$data['savedMasseged']	= $this->db->errormsg;
			$data['error_sql']			= $sql;
		}
		return $data;
	}
 }
?>
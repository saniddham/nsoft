<?php
class cls_mwUserPermission_get
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;	
	}
	public function getMainModule($mainModule)
	{
		return $this->sql_getMainModule($mainModule);
	}
	public function getGridMainModules($mainModules)
	{
		return $this->sql_getGridMainModules($mainModules);
	}
	public function getPermissionTypes($database)
	{
		return $this->sql_getPermissionTypes($database);
	}
	public function getSubModules($mainId)
	{
		return $this->sql_getSubModules($mainId);
	}
	public function getSpecialPermissionCount($subId)
	{
		return $this->sql_getSpecialPermissionCount($subId);
	}
	public function checkPermission($permision_fields,$subId)
	{
		return $this->sql_checkPermission($permision_fields,$subId);
	}
	public function loadUsers($companyId,$menuId,$menuPermType)
	{
		return $this->sql_loadUsers($companyId,$menuId,$menuPermType);
	}
	public function loadPermissionDetails($menuPermType,$permissionType,$menuId,$companyId)
	{
		return $this->sql_loadPermissionDetails($menuPermType,$permissionType,$menuId,$companyId);
	}
	public function checkPermissionAdd($userId,$menuId,$menuPermType)
	{
		return $this->sql_checkPermissionAdd($userId,$menuId,$menuPermType);
	}
	public function loadSpecialPermissionDetails($SMenuId,$companyId)
	{
		return $this->sql_loadSpecialPermissionDetails($SMenuId,$companyId);
	}
	private function sql_getMainModule($mainModule)
	{
		$sql = "SELECT
				menus.intId,
				menus.strName
				FROM menus
				WHERE
				menus.intParentId =  '0' AND
				menus.intStatus =  '1'
				GROUP BY
				menus.intOrderBy ";
		
		return $this->db->RunQuery($sql);
	}
	private function sql_getGridMainModules($mainModules)
	{
		if($mainModules!='')
			$wsql = " and m.intId=$mainModules";

		$sql = "SELECT
				m.intId,
				m.strName
				FROM
				menus as m
				WHERE
				m.intStatus =  '1' and
				m.intParentId =  '0' 
				$wsql
				ORDER BY
				m.intOrderBy ASC ";
	
		return $this->db->RunQuery($sql);
	}
	private function sql_getPermissionTypes($database)
	{
		$sql = "select column_name,column_comment as name 
				from information_schema.columns 
				where TABLE_SCHEMA='$database' and  
				table_name = 'menus' 
				and column_comment<>'' ";
		
		return $this->db->RunQuery($sql);
	}
	private function sql_getSubModules($mainId)
	{
		$sql = "SELECT
				m.intId,
				m.strName,
				m.strURL
				FROM
				menus as m
				WHERE
				m.intStatus =  '1' and
				m.intParentId =  '$mainId' 
				
				ORDER BY
				m.intOrderBy ASC";
		
		return $this->db->RunQuery($sql);
	}
	private function sql_getSpecialPermissionCount($subId)
	{
		$sql = "SELECT
				menus_special.strPermisionType
				FROM
				menus_special
				WHERE
				menus_special.intMenuId =  '$subId' AND
				menus_special.intStatus =  '1'";
		
		return $this->db->RunQuery($sql);
	}
	private function sql_checkPermission($permision_fields,$subId)
	{
		$sql = "select $permision_fields FROM menus WHERE menus.intId =  '$subId'
				union all
				select $permision_fields FROM menupermision WHERE menupermision.intMenuId =  '$subId'";
		
		return $this->db->RunQuery($sql);
	}
	private function sql_loadUsers($companyId,$menuId,$menuPermType)
	{
		 $sql = "SELECT DISTINCT sys_users.intUserId,sys_users.strUserName
				FROM sys_users
				INNER JOIN mst_locations_user ON mst_locations_user.intUserId=sys_users.intUserId
				INNER JOIN mst_locations ON mst_locations.intId=mst_locations_user.intLocationId 
				INNER JOIN (SELECT * FROM `menupermision` WHERE
				menupermision.intMenuId = $menuId ) AS M ON  sys_users.intUserId= M.intUserId
				WHERE sys_users.intStatus = 1 AND
				mst_locations.intCompanyId = '$companyId'
				ORDER BY sys_users.strUserName
				";
		
		return $this->db->RunQuery($sql);
	}
	private function sql_loadPermissionDetails($menuPermType,$permissionType,$menuId,$companyId)
	{
		if($menuPermType=='normal')
		{
			$sql = "SELECT DISTINCT menupermision.intUserId,
					sys_users.strUserName
					FROM menupermision
					INNER JOIN sys_users ON sys_users.intUserId=menupermision.intUserId
					INNER JOIN mst_locations_user ON mst_locations_user.intUserId=sys_users.intUserId
					INNER JOIN mst_locations ON mst_locations.intId=mst_locations_user.intLocationId
					WHERE intMenuId='$menuId' AND
					$permissionType = 1 AND
					mst_locations.intCompanyId ='$companyId' 
					ORDER BY sys_users.strUserName";
		}
		else
		{
			$sql = "SELECT DISTINCT menus_special_permision.intUser,
					sys_users.strUserName
					FROM menus_special_permision
					INNER JOIN sys_users ON sys_users.intUserId=menus_special_permision.intUser
					INNER JOIN mst_locations_user ON mst_locations_user.intUserId=sys_users.intUserId
					INNER JOIN mst_locations ON mst_locations.intId=mst_locations_user.intLocationId
					WHERE intSpMenuId='$menuId' AND
					mst_locations.intCompanyId ='$companyId'
					ORDER BY sys_users.strUserName";
		}
		
		return $this->db->RunQuery($sql);
	}
	private function sql_checkPermissionAdd($userId,$menuId,$menuPermType)
	{
		if($menuPermType=='normal')
		{
			$sql = "SELECT * FROM menupermision
					WHERE intMenuId='$menuId' AND
					intUserId = '$userId' ";
		}
		else
		{
			$sql = "SELECT * FROM menus_special_permision
					WHERE intSpMenuId='$menuId' AND
					intUser = '$userId' ";
		}
		
		$result = $this->db->RunQuery($sql);
		if(mysqli_num_rows($result)>0)
			return true;
		else
			return false;
		
	}
	private function sql_loadSpecialPermissionDetails($SMenuId,$companyId)
	{
		$sql = "SELECT
				menus_special.intId,
				menus_special.strPermisionType
				FROM
				menus_special
				WHERE
				menus_special.intMenuId =  '$SMenuId'";
		
		return $this->db->RunQuery($sql);
	}
}
?>
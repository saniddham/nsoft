<?php
class cls_mwUserPermission_set
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;	
	}
	public function insertUserPermission($userId,$menuId,$menuPermType)
	{
		return $this->sql_insertUserPermission($userId,$menuId,$menuPermType);
	}
	public function updateUserPermission($userId,$menuId,$permissionType,$permission,$menuPermType)
	{
		return $this->sql_updateUserPermission($userId,$menuId,$permissionType,$permission,$menuPermType);
	}
	private function sql_insertUserPermission($userId,$menuId,$menuPermType)
	{
		if($menuPermType=='normal')
		{
			$sql = "INSERT INTO menupermision 
					(
					intMenuId, 
					intUserId
					)
					VALUES
					(
					'$menuId', 
					'$userId'
					);";
		}
		else
		{
			$sql = "INSERT INTO menus_special_permision 
					(
					intSpMenuId, 
					intUser
					)
					VALUES
					(
					'$menuId', 
					'$userId'
					)";
		}
		
		$result = $this->db->RunQuery($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	private function sql_updateUserPermission($userId,$menuId,$permissionType,$permission,$menuPermType)
	{
		if($menuPermType=='normal')
		{
			$sql = "UPDATE menupermision 
					SET
					$permissionType = '$permission' 
					WHERE
					intMenuId = '$menuId' AND 
					intUserId = '$userId' ";
		}
		else
		{
			$sql = "DELETE FROM menus_special_permision 
					WHERE
					intSpMenuId = '$menuId' AND 
					intUser = '$userId'";
		}
		
		$result = $this->db->RunQuery($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
}
?>
<?php
class sysNoUpdate_get{

	private $db;
	
	function __construct($db)
	{
		$this->db = $db;	
	}
	
	public function getProgram()
	{
		return $this->getProgram_sql();	
	}
	public function loadGridData($searchID)
	{
		return $this->loadGrid_sql($searchID);
	}
	
	private function getProgram_sql()
	{
		$sql 	=  "SELECT 	
					PROGRAM_ID,PROGRAM_NAME
					FROM 
					other_sysno_autofix 
					ORDER BY
					PROGRAM_NAME";
					
		$result	=  $this->db->RunQuery($sql);
		return $result;
	}
	private function loadGrid_sql($searchID)
	{
		$sql1 	= " SELECT 	
					PROGRAM_ID, 
					PROGRAM_NAME, 
					TABLE_NAME, 
					TABLE_FIELD, 
					SYS_FIELD, 
					LOCATION_FIELD
					 
					FROM 
					other_sysno_autofix
					
					WHERE
					PROGRAM_ID='$searchID'";
				
		$result	 		= $this->db->RunQuery($sql1);
		$row	 		= mysqli_fetch_array($result);
		
		$table_name		= $row['TABLE_NAME'];
		$table_field	= $row['TABLE_FIELD'];
		$sys_field		= $row['SYS_FIELD'];
		$location_field = $row['LOCATION_FIELD'];
		
		$sql2	= " SELECT 	
					s.intCompanyId, 
					s.$sys_field as sys_no,
					l.strName
									 
					FROM 
					sys_no s
					
					INNER JOIN mst_locations l ON l.intId = s.intCompanyId 
					
					WHERE 
					l.intStatus='1'";
							 
		$result2	= $this->db->RunQuery($sql2);
		while($row1 = mysqli_fetch_array($result2))
		{
				$sql3	= "	SELECT 
							IFNULL(MAX($table_field),0) as last_no
							
							FROM
							$table_name tn
							
							WHERE
							tn.$location_field = '".$row1['intCompanyId']."' " ;
							
				$result3 = $this->db->RunQuery($sql3);
				$row2	 = mysqli_fetch_array($result3);
		
				$data['sys_no']	     = $row1['sys_no'];		
				$data['location_id'] = $row1['intCompanyId'];	
				$data['location']    = $row1['strName'];
				$data['last_no']     = $row2['last_no'];
				
				$dataArr[]	= $data;
							
		}
		return $dataArr;
	}

}
?>
<?php
date_default_timezone_set('Asia/Kolkata');
//ini_set('display_errors',1);//$_SESSION['ROOT_PATH'].
//include $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";


class cls_mailCopyUsers_get
{
	
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	
	public function getDetails($user,$location){
		
		     $sql="SELECT
					sys_users.intUserId,
					sys_users.strUserName,
					sys_users.strEmail
					FROM
					sys_mail_copy_users
					Inner Join sys_users ON sys_mail_copy_users.intMailCopyUserId = sys_users.intUserId
					Inner Join mst_locations_user ON sys_users.intUserId = mst_locations_user.intUserId
					WHERE
					mst_locations_user.intLocationId =  '$location' AND 
					sys_users.intStatus =  '1' and 
					sys_mail_copy_users.intMailMasterUserId =  '$user'
					ORDER BY strUserName ASC";
			 // 	echo $sql;
			$result = $this->db->RunQuery2($sql);
		while($row=mysqli_fetch_array($result))
		{
			$arrData[] = $row;
		}
		$response['arrData'] 	= $arrData;
		return $arrData;
	}
	
	public function getPopupDetails($department,$mainUser,$location){
		
		     $sql="SELECT
					sys_users.intUserId,
					sys_users.strUserName,
					sys_users.strEmail
					FROM 
					sys_users
					Inner Join mst_locations_user ON sys_users.intUserId = mst_locations_user.intUserId
					WHERE
					mst_locations_user.intLocationId =  '$location' AND 
					sys_users.intStatus =  '1' and sys_users.intUserId <> '$mainUser' ";
			if($department!=''){		 
			$sql.=" and	sys_users.intDepartmentId =  '$department' "; 
			}
			$sql.=" ORDER BY strUserName ASC";
			 // 	echo $sql;
			$result = $this->db->RunQuery2($sql);
		while($row=mysqli_fetch_array($result))
		{
			$arrData[] = $row;
		}
		$response['arrData'] 	= $arrData;
		return $arrData;
	}
}
?>
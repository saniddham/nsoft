<?php
class sessions{

	private $locationId;
	private $companyId;
	private $userId;
	private $Auth;
	private $rootPath;
	private $db_hr;
	
	function __construct()
	{
		$this->locationId 		= $_SESSION["CompanyID"];
		$this->companyId 		= $_SESSION["headCompanyId"];
		$this->userId	 		= $_SESSION["userId"];
		$this->rootPath			= $_SERVER['DOCUMENT_ROOT'].'/nsoft/';
		$this->Auth				= $_SESSION['AUTH'];
		$this->db_hr			= $_SESSION['HRDatabase'];
	}
	
	public function getLocationId()
	{
		return $this->locationId;
	}
	
	public function getCompanyId()
	{
		return $this->companyId;
	}
	
	public function getUserId()
	{
		return $this->userId;
	}
	
	public function getAuth()
	{
		return $this->Auth;
	}
	
	public function setAuth($auth)
	{
		$this->Auth	= $auth;
		$_SESSION['AUTH'] = $auth;
	}
	
	public function getRootPath()
	{
		return $this->rootPath;
	}
	
	public function getHrDatabase()
	{
		return $this->db_hr;
	}
}
?>
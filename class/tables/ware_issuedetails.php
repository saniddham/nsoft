<?php
class ware_issuedetails{
 
	private $db;
	private $table= "ware_issuedetails";
	
	//private property
	private $intIssueNo;
	private $intIssueYear;
	private $strOrderNo;
	private $intOrderYear;
	private $strStyleNo;
	private $intMrnNo;
	private $intMrnYear;
	private $intItemId;
	private $dblQty;
	private $dblReturnQty;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('intIssueNo'=>'intIssueNo',
										'intIssueYear'=>'intIssueYear',
										'strOrderNo'=>'strOrderNo',
										'intOrderYear'=>'intOrderYear',
										'strStyleNo'=>'strStyleNo',
										'intMrnNo'=>'intMrnNo',
										'intMrnYear'=>'intMrnYear',
										'intItemId'=>'intItemId',
										'dblQty'=>'dblQty',
										'dblReturnQty'=>'dblReturnQty',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "intIssueNo = ".$this->intIssueNo." and intIssueYear = ".$this->intIssueYear." and strOrderNo = ".$this->strOrderNo." and intOrderYear = ".$this->intOrderYear." and strStyleNo = ".$this->strStyleNo." and intMrnNo = ".$this->intMrnNo." and intMrnYear = ".$this->intMrnYear." and intItemId = ".$this->intItemId."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun intIssueNo
	function getintIssueNo()
	{
		$this->validate();
		return $this->intIssueNo;
	}
	
	//retun intIssueYear
	function getintIssueYear()
	{
		$this->validate();
		return $this->intIssueYear;
	}
	
	//retun strOrderNo
	function getstrOrderNo()
	{
		$this->validate();
		return $this->strOrderNo;
	}
	
	//retun intOrderYear
	function getintOrderYear()
	{
		$this->validate();
		return $this->intOrderYear;
	}
	
	//retun strStyleNo
	function getstrStyleNo()
	{
		$this->validate();
		return $this->strStyleNo;
	}
	
	//retun intMrnNo
	function getintMrnNo()
	{
		$this->validate();
		return $this->intMrnNo;
	}
	
	//retun intMrnYear
	function getintMrnYear()
	{
		$this->validate();
		return $this->intMrnYear;
	}
	
	//retun intItemId
	function getintItemId()
	{
		$this->validate();
		return $this->intItemId;
	}
	
	//retun dblQty
	function getdblQty()
	{
		$this->validate();
		return $this->dblQty;
	}
	
	//retun dblReturnQty
	function getdblReturnQty()
	{
		$this->validate();
		return $this->dblReturnQty;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set intIssueNo
	function setintIssueNo($intIssueNo)
	{
		array_push($this->commitArray,'intIssueNo');
		$this->intIssueNo = $intIssueNo;
	}
	
	//set intIssueYear
	function setintIssueYear($intIssueYear)
	{
		array_push($this->commitArray,'intIssueYear');
		$this->intIssueYear = $intIssueYear;
	}
	
	//set strOrderNo
	function setstrOrderNo($strOrderNo)
	{
		array_push($this->commitArray,'strOrderNo');
		$this->strOrderNo = $strOrderNo;
	}
	
	//set intOrderYear
	function setintOrderYear($intOrderYear)
	{
		array_push($this->commitArray,'intOrderYear');
		$this->intOrderYear = $intOrderYear;
	}
	
	//set strStyleNo
	function setstrStyleNo($strStyleNo)
	{
		array_push($this->commitArray,'strStyleNo');
		$this->strStyleNo = $strStyleNo;
	}
	
	//set intMrnNo
	function setintMrnNo($intMrnNo)
	{
		array_push($this->commitArray,'intMrnNo');
		$this->intMrnNo = $intMrnNo;
	}
	
	//set intMrnYear
	function setintMrnYear($intMrnYear)
	{
		array_push($this->commitArray,'intMrnYear');
		$this->intMrnYear = $intMrnYear;
	}
	
	//set intItemId
	function setintItemId($intItemId)
	{
		array_push($this->commitArray,'intItemId');
		$this->intItemId = $intItemId;
	}
	
	//set dblQty
	function setdblQty($dblQty)
	{
		array_push($this->commitArray,'dblQty');
		$this->dblQty = $dblQty;
	}
	
	//set dblReturnQty
	function setdblReturnQty($dblReturnQty)
	{
		array_push($this->commitArray,'dblReturnQty');
		$this->dblReturnQty = $dblReturnQty;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->intIssueNo=='' || $this->intIssueYear=='' || $this->strOrderNo=='' || $this->intOrderYear=='' || $this->strStyleNo=='' || $this->intMrnNo=='' || $this->intMrnYear=='' || $this->intItemId=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($intIssueNo , $intIssueYear , $strOrderNo , $intOrderYear , $strStyleNo , $intMrnNo , $intMrnYear , $intItemId)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "intIssueNo='$intIssueNo' and intIssueYear='$intIssueYear' and strOrderNo='$strOrderNo' and intOrderYear='$intOrderYear' and strStyleNo='$strStyleNo' and intMrnNo='$intMrnNo' and intMrnYear='$intMrnYear' and intItemId='$intItemId'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($intIssueNo,$intIssueYear,$strOrderNo,$intOrderYear,$strStyleNo,$intMrnNo,$intMrnYear,$intItemId,$dblQty,$dblReturnQty){
		$data = array('intIssueNo'=>$intIssueNo 
				,'intIssueYear'=>$intIssueYear 
				,'strOrderNo'=>$strOrderNo 
				,'intOrderYear'=>$intOrderYear 
				,'strStyleNo'=>$strStyleNo 
				,'intMrnNo'=>$intMrnNo 
				,'intMrnYear'=>$intMrnYear 
				,'intItemId'=>$intItemId 
				,'dblQty'=>$dblQty 
				,'dblReturnQty'=>$dblReturnQty 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('intIssueNo,intIssueYear',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intIssueNo'])
				$html .= '<option selected="selected" value="'.$row['intIssueNo'].'">'.$row['intIssueYear'].'</option>';	
			else
				$html .= '<option value="'.$row['intIssueNo'].'">'.$row['intIssueYear'].'</option>';	
		}
		return $html;
	}
	
	public function getMaterialIssuedItemQty($location,$concatOrderNo,$itemId,$mrnNo,$mrnYear,$deci)
	{
				//die("$location,$concatOrderNo,$itemId,$crnNo,$crnYear,$deci");

		$cols		= "ROUND(COALESCE(SUM(ware_issuedetails.dblQty),0),$deci+2)	AS ISSUE_QTY";
		
		$join 		= " INNER JOIN ware_issueheader IH
					  	ON ware_issuedetails.intIssueNo = IH.intIssueNo
					 	 AND ware_issuedetails.intIssueYear = IH.intIssueYear";
					  
		$where		= " IH.intStatus = 1 
						AND ware_issuedetails.intItemId = $itemId 
						AND IH.intCompanyId = $location 
						AND CONCAT(ware_issuedetails.strOrderNo,'/',ware_issuedetails.intOrderYear,'/',ware_issuedetails.strStyleNo) IN ($concatOrderNo)";
		if($mrnNo != '')
		{			 
		$where	   .= " AND ware_issuedetails.intMrnNo = '$mrnNo' 
					 	AND ware_issuedetails.intMrnYear = '$mrnYear' ";
		}
		
		$result 	= $this->select($cols,$join,$where, $order = null, $limit = null);	
		$row		= mysqli_fetch_array($result);
		return $row['ISSUE_QTY'];
	}	
	
	//END }
}
?>
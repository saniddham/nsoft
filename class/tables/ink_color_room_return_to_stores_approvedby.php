<?php
class ink_color_room_return_to_stores_approvedby{
 
	private $db;
	private $table= "ink_color_room_return_to_stores_approvedby";
	
	//private property
	private $SERIAL_NO;
	private $SERIAL_YEAR;
	private $APPROVE_LEVELS;
	private $APPROVE_BY;
	private $APPROVE_DATE;
	private $STATUS;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('SERIAL_NO'=>'SERIAL_NO',
										'SERIAL_YEAR'=>'SERIAL_YEAR',
										'APPROVE_LEVELS'=>'APPROVE_LEVELS',
										'APPROVE_BY'=>'APPROVE_BY',
										'APPROVE_DATE'=>'APPROVE_DATE',
										'STATUS'=>'STATUS',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "SERIAL_NO = ".$this->SERIAL_NO." and SERIAL_YEAR = ".$this->SERIAL_YEAR." and APPROVE_LEVELS = ".$this->APPROVE_LEVELS." and STATUS = ".$this->STATUS."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun SERIAL_NO
	function getSERIAL_NO()
	{
		$this->validate();
		return $this->SERIAL_NO;
	}
	
	//retun SERIAL_YEAR
	function getSERIAL_YEAR()
	{
		$this->validate();
		return $this->SERIAL_YEAR;
	}
	
	//retun APPROVE_LEVELS
	function getAPPROVE_LEVELS()
	{
		$this->validate();
		return $this->APPROVE_LEVELS;
	}
	
	//retun APPROVE_BY
	function getAPPROVE_BY()
	{
		$this->validate();
		return $this->APPROVE_BY;
	}
	
	//retun APPROVE_DATE
	function getAPPROVE_DATE()
	{
		$this->validate();
		return $this->APPROVE_DATE;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set SERIAL_NO
	function setSERIAL_NO($SERIAL_NO)
	{
		array_push($this->commitArray,'SERIAL_NO');
		$this->SERIAL_NO = $SERIAL_NO;
	}
	
	//set SERIAL_YEAR
	function setSERIAL_YEAR($SERIAL_YEAR)
	{
		array_push($this->commitArray,'SERIAL_YEAR');
		$this->SERIAL_YEAR = $SERIAL_YEAR;
	}
	
	//set APPROVE_LEVELS
	function setAPPROVE_LEVELS($APPROVE_LEVELS)
	{
		array_push($this->commitArray,'APPROVE_LEVELS');
		$this->APPROVE_LEVELS = $APPROVE_LEVELS;
	}
	
	//set APPROVE_BY
	function setAPPROVE_BY($APPROVE_BY)
	{
		array_push($this->commitArray,'APPROVE_BY');
		$this->APPROVE_BY = $APPROVE_BY;
	}
	
	//set APPROVE_DATE
	function setAPPROVE_DATE($APPROVE_DATE)
	{
		array_push($this->commitArray,'APPROVE_DATE');
		$this->APPROVE_DATE = $APPROVE_DATE;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->SERIAL_NO=='' || $this->SERIAL_YEAR=='' || $this->APPROVE_LEVELS=='' || $this->STATUS=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($SERIAL_NO , $SERIAL_YEAR , $APPROVE_LEVELS , $STATUS)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "SERIAL_NO='$SERIAL_NO' and SERIAL_YEAR='$SERIAL_YEAR' and APPROVE_LEVELS='$APPROVE_LEVELS' and STATUS='$STATUS'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($SERIAL_NO,$SERIAL_YEAR,$APPROVE_LEVELS,$APPROVE_BY,$APPROVE_DATE,$STATUS){
		$data = array('SERIAL_NO'=>$SERIAL_NO 
				,'SERIAL_YEAR'=>$SERIAL_YEAR 
				,'APPROVE_LEVELS'=>$APPROVE_LEVELS 
				,'APPROVE_BY'=>$APPROVE_BY 
				,'APPROVE_DATE'=>$APPROVE_DATE 
				,'STATUS'=>$STATUS 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('SERIAL_NO,SERIAL_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['SERIAL_NO'])
				$html .= '<option selected="selected" value="'.$row['SERIAL_NO'].'">'.$row['SERIAL_YEAR'].'</option>';	
			else
				$html .= '<option value="'.$row['SERIAL_NO'].'">'.$row['SERIAL_YEAR'].'</option>';	
		}
		return $html;
	}
	
	public function updateMaxStatus($serialNo,$serialYear)
	{
		$select		= " MAX(STATUS) AS MAX_STATUS ";
		$where		= " SERIAL_NO = '".$serialNo."' AND
						SERIAL_YEAR = '".$serialYear."' ";
		
		$result 	= $this->select($select,NULL,$where, NULL,NULL);	
		$row		= mysqli_fetch_array($result);
		
		$maxStatus	= $row['MAX_STATUS']+1;
		
		$data  		= array('STATUS' => $maxStatus);
		
		$where		= ' SERIAL_NO = "'.$serialNo.'" AND 
						SERIAL_YEAR = "'.$serialYear.'" AND
						STATUS = 0 ' ;
		$result_arr	= $this->update($data,$where);
		if(!$result_arr['status'])
			throw new Exception($result_arr['msg']);	
	}
	
	//END }
}
?>
<?php
class ink_day_end_process_color_room_header{
 
	private $db;
	private $table= "ink_day_end_process_color_room_header";
	
	//private property
	private $PROCESS_ID;
	private $PROCESS_DATE;
	private $PROCESS_BY;
	private $LOCATION_ID;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('PROCESS_ID'=>'PROCESS_ID',
										'PROCESS_DATE'=>'PROCESS_DATE',
										'PROCESS_BY'=>'PROCESS_BY',
										'LOCATION_ID'=>'LOCATION_ID',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "PROCESS_ID = ".$this->PROCESS_ID."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun PROCESS_ID
	function getPROCESS_ID()
	{
		$this->validate();
		return $this->PROCESS_ID;
	}
	
	//retun PROCESS_DATE
	function getPROCESS_DATE()
	{
		$this->validate();
		return $this->PROCESS_DATE;
	}
	
	//retun PROCESS_BY
	function getPROCESS_BY()
	{
		$this->validate();
		return $this->PROCESS_BY;
	}
	
	//retun LOCATION_ID
	function getLOCATION_ID()
	{
		$this->validate();
		return $this->LOCATION_ID;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set PROCESS_ID
	function setPROCESS_ID($PROCESS_ID)
	{
		array_push($this->commitArray,'PROCESS_ID');
		$this->PROCESS_ID = $PROCESS_ID;
	}
	
	//set PROCESS_DATE
	function setPROCESS_DATE($PROCESS_DATE)
	{
		array_push($this->commitArray,'PROCESS_DATE');
		$this->PROCESS_DATE = $PROCESS_DATE;
	}
	
	//set PROCESS_BY
	function setPROCESS_BY($PROCESS_BY)
	{
		array_push($this->commitArray,'PROCESS_BY');
		$this->PROCESS_BY = $PROCESS_BY;
	}
	
	//set LOCATION_ID
	function setLOCATION_ID($LOCATION_ID)
	{
		array_push($this->commitArray,'LOCATION_ID');
		$this->LOCATION_ID = $LOCATION_ID;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->PROCESS_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($PROCESS_ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "PROCESS_ID='$PROCESS_ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($PROCESS_DATE,$PROCESS_BY,$LOCATION_ID){
		$data = array('PROCESS_DATE'=>$PROCESS_DATE 
				,'PROCESS_BY'=>$PROCESS_BY 
				,'LOCATION_ID'=>$LOCATION_ID 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('PROCESS_ID,PROCESS_DATE',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['PROCESS_ID'])
				$html .= '<option selected="selected" value="'.$row['PROCESS_ID'].'">'.$row['PROCESS_DATE'].'</option>';	
			else
				$html .= '<option value="'.$row['PROCESS_ID'].'">'.$row['PROCESS_DATE'].'</option>';	
		}
		return $html;
	}
	
public function getDateArray($orderNo,$orderYear,$salesOrderId)
{
 	$array	= array();
	$cols = " DISTINCT
			 (DATE(PROCESS_DATE))	AS PROCESS_DATE ";
	
	$join =" INNER JOIN ink_day_end_process_color_room_pcs
				ON ink_day_end_process_color_room_pcs.PROCESS_ID = ink_day_end_process_color_room_header.PROCESS_ID ";
	$where ="	ORDER_NO = '$orderNo'
				AND ORDER_YEAR = '$orderYear'
				AND SALES_ORDER_ID = '$salesOrderId'
			ORDER BY DATE(ink_day_end_process_color_room_header.PROCESS_DATE)";
	
	$result	= $this->select($cols,$join,$where,$order=null,$limit=null);
 	while($row=mysqli_fetch_array($result))
	{
		$array[]	= $row['PROCESS_DATE'];
	}
	return $array;
}
	
	//END }
}
?>
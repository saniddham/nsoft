<?php
class finance_check_print_config{

	private $db;
	private $table= "finance_check_print_config";

	//private property
	private $BANK_ID;
	private $DAY1_X;
	private $DAY1_Y;
	private $DAY2_X;
	private $DAY2_Y;
	private $MONTH1_X;
	private $MONTH1_Y;
	private $MONTH2_X;
	private $MONTH2_Y;
	private $YEAR1_X;
	private $YEAR1_Y;
	private $YEAR2_X;
	private $YEAR2_Y;
	private $PAY_NAME_X;
	private $PAY_NAME_Y;
	private $AMOUNT_X;
	private $AMOUNT_Y;
	private $AMOUNT_IN_WORDS_X;
	private $AMOUNT_IN_WORDS_Y;
	private $commitArray = array();
	private $field_array = array();
	

	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('BANK_ID'=>'BANK_ID',
										'DAY1_X'=>'DAY1_X',
										'DAY1_Y'=>'DAY1_Y',
										'DAY2_X'=>'DAY2_X',
										'DAY2_Y'=>'DAY2_Y',
										'MONTH1_X'=>'MONTH1_X',
										'MONTH1_Y'=>'MONTH1_Y',
										'MONTH2_X'=>'MONTH2_X',
										'MONTH2_Y'=>'MONTH2_Y',
										'YEAR1_X'=>'YEAR1_X',
										'YEAR1_Y'=>'YEAR1_Y',
										'YEAR2_X'=>'YEAR2_X',
										'YEAR2_Y'=>'YEAR2_Y',
										'PAY_NAME_X'=>'PAY_NAME_X',
										'PAY_NAME_Y'=>'PAY_NAME_Y',
										'AMOUNT_X'=>'AMOUNT_X',
										'AMOUNT_Y'=>'AMOUNT_Y',
										'AMOUNT_IN_WORDS_X'=>'AMOUNT_IN_WORDS_X',
										'AMOUNT_IN_WORDS_Y'=>'AMOUNT_IN_WORDS_Y',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit );
		return $result;
	}

	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data);
		return $this->db->getResult();


	}

	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}

	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();
	}

	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}

	public function get_field_array(){
		return $this->field_array;
	}

	function commit($type='update')
	{
		//commit update
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{
			$data[$v] = $this->$v;
		}
		$where		= "BANK_ID = ".$this->BANK_ID."" ;
		$this->commitArray = array();

		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}

	//BEGIN - public functions for private properties {
	
	//retun BANK_ID
	function getBANK_ID()
	{
		$this->validate();
		return $this->BANK_ID;
	}
	
	//retun DAY1_X
	function getDAY1_X()
	{
		$this->validate();
		return $this->DAY1_X;
	}
	
	//retun DAY1_Y
	function getDAY1_Y()
	{
		$this->validate();
		return $this->DAY1_Y;
	}
	
	//retun DAY2_X
	function getDAY2_X()
	{
		$this->validate();
		return $this->DAY2_X;
	}
	
	//retun DAY2_Y
	function getDAY2_Y()
	{
		$this->validate();
		return $this->DAY2_Y;
	}
	
	//retun MONTH1_X
	function getMONTH1_X()
	{
		$this->validate();
		return $this->MONTH1_X;
	}
	
	//retun MONTH1_Y
	function getMONTH1_Y()
	{
		$this->validate();
		return $this->MONTH1_Y;
	}
	
	//retun MONTH2_X
	function getMONTH2_X()
	{
		$this->validate();
		return $this->MONTH2_X;
	}
	
	//retun MONTH2_Y
	function getMONTH2_Y()
	{
		$this->validate();
		return $this->MONTH2_Y;
	}
	
	//retun YEAR1_X
	function getYEAR1_X()
	{
		$this->validate();
		return $this->YEAR1_X;
	}
	
	//retun YEAR1_Y
	function getYEAR1_Y()
	{
		$this->validate();
		return $this->YEAR1_Y;
	}
	
	//retun YEAR2_X
	function getYEAR2_X()
	{
		$this->validate();
		return $this->YEAR2_X;
	}
	
	//retun YEAR2_Y
	function getYEAR2_Y()
	{
		$this->validate();
		return $this->YEAR2_Y;
	}
	
	//retun PAY_NAME_X
	function getPAY_NAME_X()
	{
		$this->validate();
		return $this->PAY_NAME_X;
	}
	
	//retun PAY_NAME_Y
	function getPAY_NAME_Y()
	{
		$this->validate();
		return $this->PAY_NAME_Y;
	}
	
	//retun AMOUNT_X
	function getAMOUNT_X()
	{
		$this->validate();
		return $this->AMOUNT_X;
	}
	
	//retun AMOUNT_Y
	function getAMOUNT_Y()
	{
		$this->validate();
		return $this->AMOUNT_Y;
	}
	
	//retun AMOUNT_IN_WORDS_X
	function getAMOUNT_IN_WORDS_X()
	{
		$this->validate();
		return $this->AMOUNT_IN_WORDS_X;
	}
	
	//retun AMOUNT_IN_WORDS_Y
	function getAMOUNT_IN_WORDS_Y()
	{
		$this->validate();
		return $this->AMOUNT_IN_WORDS_Y;
	}
	
	//END }

	//BEGIN - public function set {
	
	//set BANK_ID
	function setBANK_ID($BANK_ID)
	{
		array_push($this->commitArray,'BANK_ID');
		$this->BANK_ID = $BANK_ID;
	}
	
	//set DAY1_X
	function setDAY1_X($DAY1_X)
	{
		array_push($this->commitArray,'DAY1_X');
		$this->DAY1_X = $DAY1_X;
	}
	
	//set DAY1_Y
	function setDAY1_Y($DAY1_Y)
	{
		array_push($this->commitArray,'DAY1_Y');
		$this->DAY1_Y = $DAY1_Y;
	}
	
	//set DAY2_X
	function setDAY2_X($DAY2_X)
	{
		array_push($this->commitArray,'DAY2_X');
		$this->DAY2_X = $DAY2_X;
	}
	
	//set DAY2_Y
	function setDAY2_Y($DAY2_Y)
	{
		array_push($this->commitArray,'DAY2_Y');
		$this->DAY2_Y = $DAY2_Y;
	}
	
	//set MONTH1_X
	function setMONTH1_X($MONTH1_X)
	{
		array_push($this->commitArray,'MONTH1_X');
		$this->MONTH1_X = $MONTH1_X;
	}
	
	//set MONTH1_Y
	function setMONTH1_Y($MONTH1_Y)
	{
		array_push($this->commitArray,'MONTH1_Y');
		$this->MONTH1_Y = $MONTH1_Y;
	}
	
	//set MONTH2_X
	function setMONTH2_X($MONTH2_X)
	{
		array_push($this->commitArray,'MONTH2_X');
		$this->MONTH2_X = $MONTH2_X;
	}
	
	//set MONTH2_Y
	function setMONTH2_Y($MONTH2_Y)
	{
		array_push($this->commitArray,'MONTH2_Y');
		$this->MONTH2_Y = $MONTH2_Y;
	}
	
	//set YEAR1_X
	function setYEAR1_X($YEAR1_X)
	{
		array_push($this->commitArray,'YEAR1_X');
		$this->YEAR1_X = $YEAR1_X;
	}
	
	//set YEAR1_Y
	function setYEAR1_Y($YEAR1_Y)
	{
		array_push($this->commitArray,'YEAR1_Y');
		$this->YEAR1_Y = $YEAR1_Y;
	}
	
	//set YEAR2_X
	function setYEAR2_X($YEAR2_X)
	{
		array_push($this->commitArray,'YEAR2_X');
		$this->YEAR2_X = $YEAR2_X;
	}
	
	//set YEAR2_Y
	function setYEAR2_Y($YEAR2_Y)
	{
		array_push($this->commitArray,'YEAR2_Y');
		$this->YEAR2_Y = $YEAR2_Y;
	}
	
	//set PAY_NAME_X
	function setPAY_NAME_X($PAY_NAME_X)
	{
		array_push($this->commitArray,'PAY_NAME_X');
		$this->PAY_NAME_X = $PAY_NAME_X;
	}
	
	//set PAY_NAME_Y
	function setPAY_NAME_Y($PAY_NAME_Y)
	{
		array_push($this->commitArray,'PAY_NAME_Y');
		$this->PAY_NAME_Y = $PAY_NAME_Y;
	}
	
	//set AMOUNT_X
	function setAMOUNT_X($AMOUNT_X)
	{
		array_push($this->commitArray,'AMOUNT_X');
		$this->AMOUNT_X = $AMOUNT_X;
	}
	
	//set AMOUNT_Y
	function setAMOUNT_Y($AMOUNT_Y)
	{
		array_push($this->commitArray,'AMOUNT_Y');
		$this->AMOUNT_Y = $AMOUNT_Y;
	}
	
	//set AMOUNT_IN_WORDS_X
	function setAMOUNT_IN_WORDS_X($AMOUNT_IN_WORDS_X)
	{
		array_push($this->commitArray,'AMOUNT_IN_WORDS_X');
		$this->AMOUNT_IN_WORDS_X = $AMOUNT_IN_WORDS_X;
	}
	
	//set AMOUNT_IN_WORDS_Y
	function setAMOUNT_IN_WORDS_Y($AMOUNT_IN_WORDS_Y)
	{
		array_push($this->commitArray,'AMOUNT_IN_WORDS_Y');
		$this->AMOUNT_IN_WORDS_Y = $AMOUNT_IN_WORDS_Y;
	}
	
	//END }

	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->BANK_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}

	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}

	public function set($BANK_ID)
	{
		$cols	= "*";

		$join	= NULL;

		$where	= "BANK_ID='$BANK_ID'";

		$result = $this->select($cols,$join,$where);
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}

	//insert as parameters
	public function insertRec($BANK_ID,$DAY1_X,$DAY1_Y,$DAY2_X,$DAY2_Y,$MONTH1_X,$MONTH1_Y,$MONTH2_X,$MONTH2_Y,$YEAR1_X,$YEAR1_Y,$YEAR2_X,$YEAR2_Y,$PAY_NAME_X,$PAY_NAME_Y,$AMOUNT_X,$AMOUNT_Y,$AMOUNT_IN_WORDS_X,$AMOUNT_IN_WORDS_Y){
		$data = array('BANK_ID'=>$BANK_ID 
				,'DAY1_X'=>$DAY1_X 
				,'DAY1_Y'=>$DAY1_Y 
				,'DAY2_X'=>$DAY2_X 
				,'DAY2_Y'=>$DAY2_Y 
				,'MONTH1_X'=>$MONTH1_X 
				,'MONTH1_Y'=>$MONTH1_Y 
				,'MONTH2_X'=>$MONTH2_X 
				,'MONTH2_Y'=>$MONTH2_Y 
				,'YEAR1_X'=>$YEAR1_X 
				,'YEAR1_Y'=>$YEAR1_Y 
				,'YEAR2_X'=>$YEAR2_X 
				,'YEAR2_Y'=>$YEAR2_Y 
				,'PAY_NAME_X'=>$PAY_NAME_X 
				,'PAY_NAME_Y'=>$PAY_NAME_Y 
				,'AMOUNT_X'=>$AMOUNT_X 
				,'AMOUNT_Y'=>$AMOUNT_Y 
				,'AMOUNT_IN_WORDS_X'=>$AMOUNT_IN_WORDS_X 
				,'AMOUNT_IN_WORDS_Y'=>$AMOUNT_IN_WORDS_Y 
				);
		return $this->insert($data);
	}

	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('BANK_ID,DAY1_X',  null, $where = $where);
		$html = '<option value=""></option>';
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['BANK_ID'])
				$html .= '<option selected="selected" value="'.$row['BANK_ID'].'">'.$row['DAY1_X'].'</option>';
			else
				$html .= '<option value="'.$row['BANK_ID'].'">'.$row['DAY1_X'].'</option>';
		}
		return $html;
	}
	
	function getBankChequeConfigaration($bankId)
	{
		$cols	= "*";
		
		$where	= "BANK_ID='$bankId' ";
		
		$result = $this->select($cols,$join=null,$where);
		$row	= mysqli_fetch_array($result);
		$count	= mysqli_num_rows($result);
		
		if($count>0)
			return $row;
		else
			return false;	
	}
	
	//END }
}
?>
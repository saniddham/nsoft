<?php
//don't change add a manually column withou editing insertRec by lahiru
class finance_transaction{
 
	private $db;
	private $table= "finance_transaction";
	
	//private property
	private $SERIAL_ID;
	private $CHART_OF_ACCOUNT_ID;
	private $AMOUNT;
	private $DOCUMENT_NO;
	private $DOCUMENT_YEAR;
	private $DOCUMENT_TYPE;
	private $INVOICE_NO;
	private $INVOICE_YEAR;
	private $TRANSACTION_TYPE;
	private $TRANSACTION_CATEGORY;
	private $TRANSACTION_CATEGORY_ID;
	private $CURRENCY_ID;
	private $BANK_REFERENCE_NO;
	private $REMARKS;
	private $REC_STATUS;
	private $ADVANCE_BALANCE_DEPOSIT_STATUS;
	private $LOCATION_ID;
	private $COMPANY_ID;
	private $LAST_MODIFIED_BY;
	private $LAST_MODIFIED_DATE;
	private $SYSTEM_MODIFIED_DATE;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('SERIAL_ID'=>'SERIAL_ID',
										'CHART_OF_ACCOUNT_ID'=>'CHART_OF_ACCOUNT_ID',
										'AMOUNT'=>'AMOUNT',
										'DOCUMENT_NO'=>'DOCUMENT_NO',
										'DOCUMENT_YEAR'=>'DOCUMENT_YEAR',
										'DOCUMENT_TYPE'=>'DOCUMENT_TYPE',
										'INVOICE_NO'=>'INVOICE_NO',
										'INVOICE_YEAR'=>'INVOICE_YEAR',
										'TRANSACTION_TYPE'=>'TRANSACTION_TYPE',
										'TRANSACTION_CATEGORY'=>'TRANSACTION_CATEGORY',
										'TRANSACTION_CATEGORY_ID'=>'TRANSACTION_CATEGORY_ID',
										'CURRENCY_ID'=>'CURRENCY_ID',
										'BANK_REFERENCE_NO'=>'BANK_REFERENCE_NO',
										'REMARKS'=>'REMARKS',
										'REC_STATUS'=>'REC_STATUS',
										'ADVANCE_BALANCE_DEPOSIT_STATUS'=>'ADVANCE_BALANCE_DEPOSIT_STATUS',
										'LOCATION_ID'=>'LOCATION_ID',
										'COMPANY_ID'=>'COMPANY_ID',
										'LAST_MODIFIED_BY'=>'LAST_MODIFIED_BY',
										'LAST_MODIFIED_DATE'=>'LAST_MODIFIED_DATE',
										'SYSTEM_MODIFIED_DATE'=>'SYSTEM_MODIFIED_DATE',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "SERIAL_ID = ".$this->SERIAL_ID."" ;
		$this->commitArray = array();
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun SERIAL_ID
	function getSERIAL_ID()
	{
		$this->validate();
		return $this->SERIAL_ID;
	}
	
	//retun CHART_OF_ACCOUNT_ID
	function getCHART_OF_ACCOUNT_ID()
	{
		$this->validate();
		return $this->CHART_OF_ACCOUNT_ID;
	}
	
	//retun AMOUNT
	function getAMOUNT()
	{
		$this->validate();
		return $this->AMOUNT;
	}
	
	//retun DOCUMENT_NO
	function getDOCUMENT_NO()
	{
		$this->validate();
		return $this->DOCUMENT_NO;
	}
	
	//retun DOCUMENT_YEAR
	function getDOCUMENT_YEAR()
	{
		$this->validate();
		return $this->DOCUMENT_YEAR;
	}
	
	//retun DOCUMENT_TYPE
	function getDOCUMENT_TYPE()
	{
		$this->validate();
		return $this->DOCUMENT_TYPE;
	}
	
	//retun INVOICE_NO
	function getINVOICE_NO()
	{
		$this->validate();
		return $this->INVOICE_NO;
	}
	
	//retun INVOICE_YEAR
	function getINVOICE_YEAR()
	{
		$this->validate();
		return $this->INVOICE_YEAR;
	}
	
	//retun TRANSACTION_TYPE
	function getTRANSACTION_TYPE()
	{
		$this->validate();
		return $this->TRANSACTION_TYPE;
	}
	
	//retun TRANSACTION_CATEGORY
	function getTRANSACTION_CATEGORY()
	{
		$this->validate();
		return $this->TRANSACTION_CATEGORY;
	}
	
	//retun TRANSACTION_CATEGORY_ID
	function getTRANSACTION_CATEGORY_ID()
	{
		$this->validate();
		return $this->TRANSACTION_CATEGORY_ID;
	}
	
	//retun CURRENCY_ID
	function getCURRENCY_ID()
	{
		$this->validate();
		return $this->CURRENCY_ID;
	}
	
	//retun BANK_REFERENCE_NO
	function getBANK_REFERENCE_NO()
	{
		$this->validate();
		return $this->BANK_REFERENCE_NO;
	}
	
	//retun REMARKS
	function getREMARKS()
	{
		$this->validate();
		return $this->REMARKS;
	}
	
	//retun REC_STATUS
	function getREC_STATUS()
	{
		$this->validate();
		return $this->REC_STATUS;
	}
	
	//retun ADVANCE_BALANCE_DEPOSIT_STATUS
	function getADVANCE_BALANCE_DEPOSIT_STATUS()
	{
		$this->validate();
		return $this->ADVANCE_BALANCE_DEPOSIT_STATUS;
	}
	
	//retun LOCATION_ID
	function getLOCATION_ID()
	{
		$this->validate();
		return $this->LOCATION_ID;
	}
	
	//retun COMPANY_ID
	function getCOMPANY_ID()
	{
		$this->validate();
		return $this->COMPANY_ID;
	}
	
	//retun LAST_MODIFIED_BY
	function getLAST_MODIFIED_BY()
	{
		$this->validate();
		return $this->LAST_MODIFIED_BY;
	}
	
	//retun LAST_MODIFIED_DATE
	function getLAST_MODIFIED_DATE()
	{
		$this->validate();
		return $this->LAST_MODIFIED_DATE;
	}
	
	//retun SYSTEM_MODIFIED_DATE
	function getSYSTEM_MODIFIED_DATE()
	{
		$this->validate();
		return $this->SYSTEM_MODIFIED_DATE;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set SERIAL_ID
	function setSERIAL_ID($SERIAL_ID)
	{
		array_push($this->commitArray,'SERIAL_ID');
		$this->SERIAL_ID = $SERIAL_ID;
	}
	
	//set CHART_OF_ACCOUNT_ID
	function setCHART_OF_ACCOUNT_ID($CHART_OF_ACCOUNT_ID)
	{
		array_push($this->commitArray,'CHART_OF_ACCOUNT_ID');
		$this->CHART_OF_ACCOUNT_ID = $CHART_OF_ACCOUNT_ID;
	}
	
	//set AMOUNT
	function setAMOUNT($AMOUNT)
	{
		array_push($this->commitArray,'AMOUNT');
		$this->AMOUNT = $AMOUNT;
	}
	
	//set DOCUMENT_NO
	function setDOCUMENT_NO($DOCUMENT_NO)
	{
		array_push($this->commitArray,'DOCUMENT_NO');
		$this->DOCUMENT_NO = $DOCUMENT_NO;
	}
	
	//set DOCUMENT_YEAR
	function setDOCUMENT_YEAR($DOCUMENT_YEAR)
	{
		array_push($this->commitArray,'DOCUMENT_YEAR');
		$this->DOCUMENT_YEAR = $DOCUMENT_YEAR;
	}
	
	//set DOCUMENT_TYPE
	function setDOCUMENT_TYPE($DOCUMENT_TYPE)
	{
		array_push($this->commitArray,'DOCUMENT_TYPE');
		$this->DOCUMENT_TYPE = $DOCUMENT_TYPE;
	}
	
	//set INVOICE_NO
	function setINVOICE_NO($INVOICE_NO)
	{
		array_push($this->commitArray,'INVOICE_NO');
		$this->INVOICE_NO = $INVOICE_NO;
	}
	
	//set INVOICE_YEAR
	function setINVOICE_YEAR($INVOICE_YEAR)
	{
		array_push($this->commitArray,'INVOICE_YEAR');
		$this->INVOICE_YEAR = $INVOICE_YEAR;
	}
	
	//set TRANSACTION_TYPE
	function setTRANSACTION_TYPE($TRANSACTION_TYPE)
	{
		array_push($this->commitArray,'TRANSACTION_TYPE');
		$this->TRANSACTION_TYPE = $TRANSACTION_TYPE;
	}
	
	//set TRANSACTION_CATEGORY
	function setTRANSACTION_CATEGORY($TRANSACTION_CATEGORY)
	{
		array_push($this->commitArray,'TRANSACTION_CATEGORY');
		$this->TRANSACTION_CATEGORY = $TRANSACTION_CATEGORY;
	}
	
	//set TRANSACTION_CATEGORY_ID
	function setTRANSACTION_CATEGORY_ID($TRANSACTION_CATEGORY_ID)
	{
		array_push($this->commitArray,'TRANSACTION_CATEGORY_ID');
		$this->TRANSACTION_CATEGORY_ID = $TRANSACTION_CATEGORY_ID;
	}
	
	//set CURRENCY_ID
	function setCURRENCY_ID($CURRENCY_ID)
	{
		array_push($this->commitArray,'CURRENCY_ID');
		$this->CURRENCY_ID = $CURRENCY_ID;
	}
	
	//set BANK_REFERENCE_NO
	function setBANK_REFERENCE_NO($BANK_REFERENCE_NO)
	{
		array_push($this->commitArray,'BANK_REFERENCE_NO');
		$this->BANK_REFERENCE_NO = $BANK_REFERENCE_NO;
	}
	
	//set REMARKS
	function setREMARKS($REMARKS)
	{
		array_push($this->commitArray,'REMARKS');
		$this->REMARKS = $REMARKS;
	}
	
	//set REC_STATUS
	function setREC_STATUS($REC_STATUS)
	{
		array_push($this->commitArray,'REC_STATUS');
		$this->REC_STATUS = $REC_STATUS;
	}
	
	//set ADVANCE_BALANCE_DEPOSIT_STATUS
	function setADVANCE_BALANCE_DEPOSIT_STATUS($ADVANCE_BALANCE_DEPOSIT_STATUS)
	{
		array_push($this->commitArray,'ADVANCE_BALANCE_DEPOSIT_STATUS');
		$this->ADVANCE_BALANCE_DEPOSIT_STATUS = $ADVANCE_BALANCE_DEPOSIT_STATUS;
	}
	
	//set LOCATION_ID
	function setLOCATION_ID($LOCATION_ID)
	{
		array_push($this->commitArray,'LOCATION_ID');
		$this->LOCATION_ID = $LOCATION_ID;
	}
	
	//set COMPANY_ID
	function setCOMPANY_ID($COMPANY_ID)
	{
		array_push($this->commitArray,'COMPANY_ID');
		$this->COMPANY_ID = $COMPANY_ID;
	}
	
	//set LAST_MODIFIED_BY
	function setLAST_MODIFIED_BY($LAST_MODIFIED_BY)
	{
		array_push($this->commitArray,'LAST_MODIFIED_BY');
		$this->LAST_MODIFIED_BY = $LAST_MODIFIED_BY;
	}
	
	//set LAST_MODIFIED_DATE
	function setLAST_MODIFIED_DATE($LAST_MODIFIED_DATE)
	{
		array_push($this->commitArray,'LAST_MODIFIED_DATE');
		$this->LAST_MODIFIED_DATE = $LAST_MODIFIED_DATE;
	}
	
	//set SYSTEM_MODIFIED_DATE
	function setSYSTEM_MODIFIED_DATE($SYSTEM_MODIFIED_DATE)
	{
		array_push($this->commitArray,'SYSTEM_MODIFIED_DATE');
		$this->SYSTEM_MODIFIED_DATE = $SYSTEM_MODIFIED_DATE;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->SERIAL_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($SERIAL_ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "SERIAL_ID='$SERIAL_ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($CHART_OF_ACCOUNT_ID,$AMOUNT,$DOCUMENT_NO,$DOCUMENT_YEAR,$DOCUMENT_TYPE,$INVOICE_NO,$INVOICE_YEAR,$TRANSACTION_TYPE,$TRANSACTION_CATEGORY,$TRANSACTION_CATEGORY_ID,$CURRENCY_ID,$BANK_REFERENCE_NO,$REMARKS,$REC_STATUS,$LOCATION_ID,$COMPANY_ID,$LAST_MODIFIED_BY,$LAST_MODIFIED_DATE,$SYSTEM_MODIFIED_DATE){
		$data = array('CHART_OF_ACCOUNT_ID'=>$CHART_OF_ACCOUNT_ID 
				,'AMOUNT'=>$AMOUNT 
				,'DOCUMENT_NO'=>$DOCUMENT_NO 
				,'DOCUMENT_YEAR'=>$DOCUMENT_YEAR 
				,'DOCUMENT_TYPE'=>$DOCUMENT_TYPE 
				,'INVOICE_NO'=>$INVOICE_NO 
				,'INVOICE_YEAR'=>$INVOICE_YEAR 
				,'TRANSACTION_TYPE'=>$TRANSACTION_TYPE 
				,'TRANSACTION_CATEGORY'=>$TRANSACTION_CATEGORY 
				,'TRANSACTION_CATEGORY_ID'=>$TRANSACTION_CATEGORY_ID 
				,'CURRENCY_ID'=>$CURRENCY_ID 
				,'BANK_REFERENCE_NO'=>$BANK_REFERENCE_NO 
				,'REMARKS'=>$REMARKS 
				,'REC_STATUS'=>$REC_STATUS 
				,'LOCATION_ID'=>$LOCATION_ID 
				,'COMPANY_ID'=>$COMPANY_ID 
				,'LAST_MODIFIED_BY'=>$LAST_MODIFIED_BY 
				,'LAST_MODIFIED_DATE'=>$LAST_MODIFIED_DATE 
				,'SYSTEM_MODIFIED_DATE'=>$SYSTEM_MODIFIED_DATE 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('SERIAL_ID,CHART_OF_ACCOUNT_ID',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['SERIAL_ID'])
				$html .= '<option selected="selected" value="'.$row['SERIAL_ID'].'">'.$row['CHART_OF_ACCOUNT_ID'].'</option>';	
			else
				$html .= '<option value="'.$row['SERIAL_ID'].'">'.$row['CHART_OF_ACCOUNT_ID'].'</option>';	
		}
		return $html;
	}
	
	//END }
	
#BEGIN  - USER DEFINED FUNCTIONS {
	public function getTotAmountForSelectedCriteria($companyId,$chartOfAccountId,$documentType,$dateFrom,$dateTo,$convertCurrencyId)
	{
		global $db;
		
		if($documentType!='')
			$where .= "AND DOCUMENT_TYPE = '$documentType'";
		if($dateFrom!='')
			$where .= "AND LAST_MODIFIED_DATE >= 'dateFrom' ";			
		if($dateTo!='')
			$where .= "AND LAST_MODIFIED_DATE <= '$dateTo' ";
			
		$sql	= "SELECT
					  SUM(T1.CONVERTED_AMOUNT) AS CONVERTED_AMOUNT
					FROM (SELECT
							CURRENCY_ID AS ORIGINAL_CURRENCY_ID,
							AMOUNT      AS ORIGINAL_AMOUNT,
							ROUND(COALESCE(FT.AMOUNT / (SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = $convertCurrencyId AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId) * (SELECT SUB_ER.dblExcAvgRate FROM mst_financeexchangerate SUB_ER WHERE SUB_ER.intCurrencyId = FT.CURRENCY_ID AND DATE(FT.LAST_MODIFIED_DATE) = DATE(SUB_ER.dtmDate) AND SUB_ER.intCompanyId = $companyId),0),2) AS CONVERTED_AMOUNT
						  FROM finance_transaction FT
						  WHERE CHART_OF_ACCOUNT_ID IN($chartOfAccountId)							  
							  AND COMPANY_ID = '$companyId'							  
							  $where
						  GROUP BY CURRENCY_ID) AS T1";
		$result = $db->RunQuery($sql);
		return $result[0]['CONVERTED_AMOUNT'];
	}
	public function getExceedTransactionDetails($docNo,$docYear,$docType,$trnType,$trnCategory,$trnCatId,$currency,$company)
	{
		$cols	= " * ";
		
		$where	= " DOCUMENT_NO = $docNo AND
					DOCUMENT_YEAR  = $docYear AND
					DOCUMENT_TYPE = '".$docType."' AND
					TRANSACTION_TYPE = '".$trnType."' AND
					TRANSACTION_CATEGORY = '".$trnCategory."' AND
					TRANSACTION_CATEGORY_ID = $trnCatId AND
					CURRENCY_ID = $currency AND
					COMPANY_ID = $company ";
		
		$result		= $this->select($cols,$join=null,$where,$order=null,$limit=null);
		return mysqli_fetch_array($result);
	}
#END	- USER DEFINED FUNCTIONS }
}
?>
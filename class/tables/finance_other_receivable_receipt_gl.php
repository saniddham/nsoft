<?php
class finance_other_receivable_receipt_gl{
 
	private $db;
	private $table= "finance_other_receivable_receipt_gl";
	
	//private property
	private $RECEIPT_NO;
	private $RECEIPT_YEAR;
	private $CHART_OF_ACCOUNT_ID;
	private $AMOUNT;
	private $REMARKS;
	private $COST_CENTER;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('RECEIPT_NO'=>'RECEIPT_NO',
										'RECEIPT_YEAR'=>'RECEIPT_YEAR',
										'CHART_OF_ACCOUNT_ID'=>'CHART_OF_ACCOUNT_ID',
										'AMOUNT'=>'AMOUNT',
										'REMARKS'=>'REMARKS',
										'COST_CENTER'=>'COST_CENTER',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "RECEIPT_NO = ".$this->RECEIPT_NO." and RECEIPT_YEAR = ".$this->RECEIPT_YEAR." and CHART_OF_ACCOUNT_ID = ".$this->CHART_OF_ACCOUNT_ID." and COST_CENTER = ".$this->COST_CENTER."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun RECEIPT_NO
	function getRECEIPT_NO()
	{
		$this->validate();
		return $this->RECEIPT_NO;
	}
	
	//retun RECEIPT_YEAR
	function getRECEIPT_YEAR()
	{
		$this->validate();
		return $this->RECEIPT_YEAR;
	}
	
	//retun CHART_OF_ACCOUNT_ID
	function getCHART_OF_ACCOUNT_ID()
	{
		$this->validate();
		return $this->CHART_OF_ACCOUNT_ID;
	}
	
	//retun AMOUNT
	function getAMOUNT()
	{
		$this->validate();
		return $this->AMOUNT;
	}
	
	//retun REMARKS
	function getREMARKS()
	{
		$this->validate();
		return $this->REMARKS;
	}
	
	//retun COST_CENTER
	function getCOST_CENTER()
	{
		$this->validate();
		return $this->COST_CENTER;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set RECEIPT_NO
	function setRECEIPT_NO($RECEIPT_NO)
	{
		array_push($this->commitArray,'RECEIPT_NO');
		$this->RECEIPT_NO = $RECEIPT_NO;
	}
	
	//set RECEIPT_YEAR
	function setRECEIPT_YEAR($RECEIPT_YEAR)
	{
		array_push($this->commitArray,'RECEIPT_YEAR');
		$this->RECEIPT_YEAR = $RECEIPT_YEAR;
	}
	
	//set CHART_OF_ACCOUNT_ID
	function setCHART_OF_ACCOUNT_ID($CHART_OF_ACCOUNT_ID)
	{
		array_push($this->commitArray,'CHART_OF_ACCOUNT_ID');
		$this->CHART_OF_ACCOUNT_ID = $CHART_OF_ACCOUNT_ID;
	}
	
	//set AMOUNT
	function setAMOUNT($AMOUNT)
	{
		array_push($this->commitArray,'AMOUNT');
		$this->AMOUNT = $AMOUNT;
	}
	
	//set REMARKS
	function setREMARKS($REMARKS)
	{
		array_push($this->commitArray,'REMARKS');
		$this->REMARKS = $REMARKS;
	}
	
	//set COST_CENTER
	function setCOST_CENTER($COST_CENTER)
	{
		array_push($this->commitArray,'COST_CENTER');
		$this->COST_CENTER = $COST_CENTER;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->RECEIPT_NO=='' || $this->RECEIPT_YEAR=='' || $this->CHART_OF_ACCOUNT_ID=='' || $this->COST_CENTER=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($RECEIPT_NO , $RECEIPT_YEAR , $CHART_OF_ACCOUNT_ID , $COST_CENTER)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "RECEIPT_NO='$RECEIPT_NO' and RECEIPT_YEAR='$RECEIPT_YEAR' and CHART_OF_ACCOUNT_ID='$CHART_OF_ACCOUNT_ID' and COST_CENTER='$COST_CENTER'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($RECEIPT_NO,$RECEIPT_YEAR,$CHART_OF_ACCOUNT_ID,$AMOUNT,$REMARKS,$COST_CENTER){
		$data = array('RECEIPT_NO'=>$RECEIPT_NO 
				,'RECEIPT_YEAR'=>$RECEIPT_YEAR 
				,'CHART_OF_ACCOUNT_ID'=>$CHART_OF_ACCOUNT_ID 
				,'AMOUNT'=>$AMOUNT 
				,'REMARKS'=>$REMARKS 
				,'COST_CENTER'=>$COST_CENTER 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('RECEIPT_NO,RECEIPT_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['RECEIPT_NO'])
				$html .= '<option selected="selected" value="'.$row['RECEIPT_NO'].'">'.$row['RECEIPT_YEAR'].'</option>';	
			else
				$html .= '<option value="'.$row['RECEIPT_NO'].'">'.$row['RECEIPT_YEAR'].'</option>';	
		}
		return $html;
	}
	public function getGLDetails_result($receiveNo,$receiveYear)
	{
		$glResult		= $this->select('*',NULL,"RECEIPT_NO = '$receiveNo' AND RECEIPT_YEAR = '$receiveYear'",NULL,NULL);
		$rowGL			= mysqli_fetch_array($glResult);	
		return $rowGL;
	}
	
	//END }
}
?>
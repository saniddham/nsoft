
<?php

class mst_marketer_manager{
 
	private $db;
	private $table= "mst_marketer_manager";
	
	//private property
	private $intMarketingManagerId;
	private $strName;
	private $dblAnualaTarget;
	private $dblMonthlyTarget;
	private $dblDayTarget;
	private $intStatus;
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->db->field_array_set(array('intMarketingManagerId'=>'intMarketingManagerId',
										'strName'=>'strName',
										'dblAnualaTarget'=>'dblAnualaTarget',
										'dblMonthlyTarget'=>'dblMonthlyTarget',
										'dblDayTarget'=>'dblDayTarget',
										'intStatus'=>'intStatus',
										));
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  
		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	//public functions for private properties
	
	//retun intMarketingManagerId
	function getintMarketingManagerId()
	{
		$this->validate();
		return $this->intMarketingManagerId;
	}
	
	//retun strName
	function getstrName()
	{
		$this->validate();
		return $this->strName;
	}
	
	//retun dblAnualaTarget
	function getdblAnualaTarget()
	{
		$this->validate();
		return $this->dblAnualaTarget;
	}
	
	//retun dblMonthlyTarget
	function getdblMonthlyTarget()
	{
		$this->validate();
		return $this->dblMonthlyTarget;
	}
	
	//retun dblDayTarget
	function getdblDayTarget()
	{
		$this->validate();
		return $this->dblDayTarget;
	}
	
	//retun intStatus
	function getintStatus()
	{
		$this->validate();
		return $this->intStatus;
	}
	
	//validate primary values
	private function validate()
	{
		if($this->intMarketingManagerId=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
		}
	}
	
	public function set($intMarketingManagerId)
	{
		$result = $this->select('*',null,"intMarketingManagerId='$intMarketingManagerId'");	
		if($this->db->numRows()>0)
			$this->setVariables($result);
		else
			throw new Exception("Record not found.");
			
	}
	
	
}
?>

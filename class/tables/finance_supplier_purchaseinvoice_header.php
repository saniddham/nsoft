<?php
class finance_supplier_purchaseinvoice_header{
 
	private $db;
	private $table= "finance_supplier_purchaseinvoice_header";
	
	//private property
	private $PURCHASE_INVOICE_NO;
	private $PURCHASE_INVOICE_YEAR;
	private $INVOICE_NO;
	private $PURCHASE_DATE;
	private $SUPPLIER_ID;
	private $COMPANY_ID;
	private $INVOICE_TYPE;
	private $REMARKS;
	private $CURRENCY_ID;
	private $STATUS;
	private $CREATED_BY;
	private $CREATED_DATE;
	private $LAST_MODIFY_BY;
	private $LAST_MODIFY_DATE;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('PURCHASE_INVOICE_NO'=>'PURCHASE_INVOICE_NO',
										'PURCHASE_INVOICE_YEAR'=>'PURCHASE_INVOICE_YEAR',
										'INVOICE_NO'=>'INVOICE_NO',
										'PURCHASE_DATE'=>'PURCHASE_DATE',
										'SUPPLIER_ID'=>'SUPPLIER_ID',
										'COMPANY_ID'=>'COMPANY_ID',
										'INVOICE_TYPE'=>'INVOICE_TYPE',
										'REMARKS'=>'REMARKS',
										'CURRENCY_ID'=>'CURRENCY_ID',
										'STATUS'=>'STATUS',
										'CREATED_BY'=>'CREATED_BY',
										'CREATED_DATE'=>'CREATED_DATE',
										'LAST_MODIFY_BY'=>'LAST_MODIFY_BY',
										'LAST_MODIFY_DATE'=>'LAST_MODIFY_DATE',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "PURCHASE_INVOICE_NO = ".$this->PURCHASE_INVOICE_NO." and PURCHASE_INVOICE_YEAR = ".$this->PURCHASE_INVOICE_YEAR."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun PURCHASE_INVOICE_NO
	function getPURCHASE_INVOICE_NO()
	{
		$this->validate();
		return $this->PURCHASE_INVOICE_NO;
	}
	
	//retun PURCHASE_INVOICE_YEAR
	function getPURCHASE_INVOICE_YEAR()
	{
		$this->validate();
		return $this->PURCHASE_INVOICE_YEAR;
	}
	
	//retun INVOICE_NO
	function getINVOICE_NO()
	{
		$this->validate();
		return $this->INVOICE_NO;
	}
	
	//retun PURCHASE_DATE
	function getPURCHASE_DATE()
	{
		$this->validate();
		return $this->PURCHASE_DATE;
	}
	
	//retun SUPPLIER_ID
	function getSUPPLIER_ID()
	{
		$this->validate();
		return $this->SUPPLIER_ID;
	}
	
	//retun COMPANY_ID
	function getCOMPANY_ID()
	{
		$this->validate();
		return $this->COMPANY_ID;
	}
	
	//retun INVOICE_TYPE
	function getINVOICE_TYPE()
	{
		$this->validate();
		return $this->INVOICE_TYPE;
	}
	
	//retun REMARKS
	function getREMARKS()
	{
		$this->validate();
		return $this->REMARKS;
	}
	
	//retun CURRENCY_ID
	function getCURRENCY_ID()
	{
		$this->validate();
		return $this->CURRENCY_ID;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//retun CREATED_BY
	function getCREATED_BY()
	{
		$this->validate();
		return $this->CREATED_BY;
	}
	
	//retun CREATED_DATE
	function getCREATED_DATE()
	{
		$this->validate();
		return $this->CREATED_DATE;
	}
	
	//retun LAST_MODIFY_BY
	function getLAST_MODIFY_BY()
	{
		$this->validate();
		return $this->LAST_MODIFY_BY;
	}
	
	//retun LAST_MODIFY_DATE
	function getLAST_MODIFY_DATE()
	{
		$this->validate();
		return $this->LAST_MODIFY_DATE;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set PURCHASE_INVOICE_NO
	function setPURCHASE_INVOICE_NO($PURCHASE_INVOICE_NO)
	{
		array_push($this->commitArray,'PURCHASE_INVOICE_NO');
		$this->PURCHASE_INVOICE_NO = $PURCHASE_INVOICE_NO;
	}
	
	//set PURCHASE_INVOICE_YEAR
	function setPURCHASE_INVOICE_YEAR($PURCHASE_INVOICE_YEAR)
	{
		array_push($this->commitArray,'PURCHASE_INVOICE_YEAR');
		$this->PURCHASE_INVOICE_YEAR = $PURCHASE_INVOICE_YEAR;
	}
	
	//set INVOICE_NO
	function setINVOICE_NO($INVOICE_NO)
	{
		array_push($this->commitArray,'INVOICE_NO');
		$this->INVOICE_NO = $INVOICE_NO;
	}
	
	//set PURCHASE_DATE
	function setPURCHASE_DATE($PURCHASE_DATE)
	{
		array_push($this->commitArray,'PURCHASE_DATE');
		$this->PURCHASE_DATE = $PURCHASE_DATE;
	}
	
	//set SUPPLIER_ID
	function setSUPPLIER_ID($SUPPLIER_ID)
	{
		array_push($this->commitArray,'SUPPLIER_ID');
		$this->SUPPLIER_ID = $SUPPLIER_ID;
	}
	
	//set COMPANY_ID
	function setCOMPANY_ID($COMPANY_ID)
	{
		array_push($this->commitArray,'COMPANY_ID');
		$this->COMPANY_ID = $COMPANY_ID;
	}
	
	//set INVOICE_TYPE
	function setINVOICE_TYPE($INVOICE_TYPE)
	{
		array_push($this->commitArray,'INVOICE_TYPE');
		$this->INVOICE_TYPE = $INVOICE_TYPE;
	}
	
	//set REMARKS
	function setREMARKS($REMARKS)
	{
		array_push($this->commitArray,'REMARKS');
		$this->REMARKS = $REMARKS;
	}
	
	//set CURRENCY_ID
	function setCURRENCY_ID($CURRENCY_ID)
	{
		array_push($this->commitArray,'CURRENCY_ID');
		$this->CURRENCY_ID = $CURRENCY_ID;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//set CREATED_BY
	function setCREATED_BY($CREATED_BY)
	{
		array_push($this->commitArray,'CREATED_BY');
		$this->CREATED_BY = $CREATED_BY;
	}
	
	//set CREATED_DATE
	function setCREATED_DATE($CREATED_DATE)
	{
		array_push($this->commitArray,'CREATED_DATE');
		$this->CREATED_DATE = $CREATED_DATE;
	}
	
	//set LAST_MODIFY_BY
	function setLAST_MODIFY_BY($LAST_MODIFY_BY)
	{
		array_push($this->commitArray,'LAST_MODIFY_BY');
		$this->LAST_MODIFY_BY = $LAST_MODIFY_BY;
	}
	
	//set LAST_MODIFY_DATE
	function setLAST_MODIFY_DATE($LAST_MODIFY_DATE)
	{
		array_push($this->commitArray,'LAST_MODIFY_DATE');
		$this->LAST_MODIFY_DATE = $LAST_MODIFY_DATE;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->PURCHASE_INVOICE_NO=='' || $this->PURCHASE_INVOICE_YEAR=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($PURCHASE_INVOICE_NO , $PURCHASE_INVOICE_YEAR)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "PURCHASE_INVOICE_NO='$PURCHASE_INVOICE_NO' and PURCHASE_INVOICE_YEAR='$PURCHASE_INVOICE_YEAR'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($PURCHASE_INVOICE_NO,$PURCHASE_INVOICE_YEAR,$INVOICE_NO,$PURCHASE_DATE,$SUPPLIER_ID,$COMPANY_ID,$INVOICE_TYPE,$REMARKS,$CURRENCY_ID,$STATUS,$CREATED_BY,$CREATED_DATE,$LAST_MODIFY_BY,$LAST_MODIFY_DATE){
		$data = array('PURCHASE_INVOICE_NO'=>$PURCHASE_INVOICE_NO 
				,'PURCHASE_INVOICE_YEAR'=>$PURCHASE_INVOICE_YEAR 
				,'INVOICE_NO'=>$INVOICE_NO 
				,'PURCHASE_DATE'=>$PURCHASE_DATE 
				,'SUPPLIER_ID'=>$SUPPLIER_ID 
				,'COMPANY_ID'=>$COMPANY_ID 
				,'INVOICE_TYPE'=>$INVOICE_TYPE 
				,'REMARKS'=>$REMARKS 
				,'CURRENCY_ID'=>$CURRENCY_ID 
				,'STATUS'=>$STATUS 
				,'CREATED_BY'=>$CREATED_BY 
				,'CREATED_DATE'=>$CREATED_DATE 
				,'LAST_MODIFY_BY'=>$LAST_MODIFY_BY 
				,'LAST_MODIFY_DATE'=>$LAST_MODIFY_DATE 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('PURCHASE_INVOICE_NO,PURCHASE_INVOICE_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['PURCHASE_INVOICE_NO'])
				$html .= '<option selected="selected" value="'.$row['PURCHASE_INVOICE_NO'].'">'.$row['PURCHASE_INVOICE_YEAR'].'</option>';	
			else
				$html .= '<option value="'.$row['PURCHASE_INVOICE_NO'].'">'.$row['PURCHASE_INVOICE_YEAR'].'</option>';	
		}
		return $html;
	}
	
	//END }
	
#BEGIN 	- USER DEFINE FUNCTIONS {
	public function getConfirmSupplier($companyId)
	{
		$cols 	= "DISTINCT
				   mst_supplier.intId   	AS SUPPLIER_ID,
				   mst_supplier.strName 	AS SUPPLIER_NAME ";
		
		$join 	= "INNER JOIN  mst_supplier
    				ON mst_supplier.intId = finance_supplier_purchaseinvoice_header.SUPPLIER_ID ";
					
		$where 	= "finance_supplier_purchaseinvoice_header.STATUS = 1
    				AND finance_supplier_purchaseinvoice_header.COMPANY_ID = $companyId ";
		
		$order	= "mst_supplier.strName";
		return $this->select($cols,$join,$where,$order,$limit=null);
	}
	
	public function getSupplierWiseCurrency($supplierId,$companyId)
	{
		$cols 	= "DISTINCT
				   mst_financecurrency.intId   	AS CURRENCY_ID,
				   mst_financecurrency.strCode 	AS CURRENCY_CODE ";
		
		$join 	= "INNER JOIN  mst_financecurrency
    				ON mst_financecurrency.intId = finance_supplier_purchaseinvoice_header.CURRENCY_ID ";
					
		$where 	= "finance_supplier_purchaseinvoice_header.STATUS = 1
					AND finance_supplier_purchaseinvoice_header.SUPPLIER_ID = $supplierId
    				AND finance_supplier_purchaseinvoice_header.COMPANY_ID = $companyId ";
		
		$order	= "mst_financecurrency.strCode";
		return $this->select($cols,$join,$where,$order,$limit=null);
	}
	public function getSupplierCurrencyWiseInvoiceNo($supplierId,$currencyId,$companyId)
	{
		$cols 	= "	DISTINCT
					PURCHASE_INVOICE_NO,
					PURCHASE_INVOICE_YEAR,
					INVOICE_NO,
					CONCAT(PURCHASE_INVOICE_NO,'/',PURCHASE_INVOICE_YEAR) AS CONCAT_INVOICE_NO ";
		
		$where	= " SUPPLIER_ID = '".$supplierId."' AND
					CURRENCY_ID = '".$currencyId."' AND
					COMPANY_ID = '".$companyId."' AND
					STATUS = 1 ";
					
		$order	= " PURCHASE_INVOICE_YEAR DESC, PURCHASE_INVOICE_NO DESC ";	
		
		return $this->select($cols,$join=null,$where,$order,$limit=null);
	}
#END 	- USER DEFINE FUNCTIONS }
}
?>

<?php

class trn_order_sub_contract_return_details{
 
	private $db;
	private $table= "trn_order_sub_contract_return_details";
	
	//private property
	private $SUB_CONTRACT_RETURN_NO;
	private $SUB_CONTRACT_RETURN_YEAR;
	private $SALES_ORDER_ID;
	private $SUB_CONTR_JOB_ID;
	private $CUT_NO;
	private $SIZE;
	private $QTY;
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->db->field_array_set(array('SUB_CONTRACT_RETURN_NO'=>'SUB_CONTRACT_RETURN_NO',
										'SUB_CONTRACT_RETURN_YEAR'=>'SUB_CONTRACT_RETURN_YEAR',
										'SALES_ORDER_ID'=>'SALES_ORDER_ID',
										'SUB_CONTR_JOB_ID'=>'SUB_CONTR_JOB_ID',
										'CUT_NO'=>'CUT_NO',
										'SIZE'=>'SIZE',
										'QTY'=>'QTY',
										));
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  
		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	//public functions for private properties
	
	//retun SUB_CONTRACT_RETURN_NO
	function getSUB_CONTRACT_RETURN_NO()
	{
		$this->validate();
		return $this->SUB_CONTRACT_RETURN_NO;
	}
	
	//retun SUB_CONTRACT_RETURN_YEAR
	function getSUB_CONTRACT_RETURN_YEAR()
	{
		$this->validate();
		return $this->SUB_CONTRACT_RETURN_YEAR;
	}
	
	//retun SALES_ORDER_ID
	function getSALES_ORDER_ID()
	{
		$this->validate();
		return $this->SALES_ORDER_ID;
	}
	
	//retun SUB_CONTR_JOB_ID
	function getSUB_CONTR_JOB_ID()
	{
		$this->validate();
		return $this->SUB_CONTR_JOB_ID;
	}
	
	//retun CUT_NO
	function getCUT_NO()
	{
		$this->validate();
		return $this->CUT_NO;
	}
	
	//retun SIZE
	function getSIZE()
	{
		$this->validate();
		return $this->SIZE;
	}
	
	//retun QTY
	function getQTY()
	{
		$this->validate();
		return $this->QTY;
	}
	
	//validate primary values
	private function validate()
	{
		if($this->SUB_CONTRACT_RETURN_NO=='' || $this->SUB_CONTRACT_RETURN_YEAR=='' || $this->SALES_ORDER_ID=='' || $this->CUT_NO=='' || $this->SIZE=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
		}
	}
	
	public function set($SUB_CONTRACT_RETURN_NO , $SUB_CONTRACT_RETURN_YEAR , $SALES_ORDER_ID , $CUT_NO , $SIZE)
	{
		$result = $this->select('*',null,"SUB_CONTRACT_RETURN_NO='$SUB_CONTRACT_RETURN_NO' and SUB_CONTRACT_RETURN_YEAR='$SUB_CONTRACT_RETURN_YEAR' and SALES_ORDER_ID='$SALES_ORDER_ID' and CUT_NO='$CUT_NO' and SIZE='$SIZE'");	
		if($this->db->numRows()>0)
			$this->setVariables($result);
		else
			throw new Exception("Record not found.");
			
	}
	
	
}
?>

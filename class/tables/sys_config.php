<?php
class sys_config{

	private $db;
	private $table= "sys_config";

	//private property
	private $intCompanyId;
	private $intActiveCRform;
	private $PAYMENT_NOTIFICATION_DAYS;
	private $CUSTOMER_INVOICE_DELAY_DAYS;
	private $PAYMENT_OUTSTANDING_PENDING_DAYS;
	private $PAYMENT_OUTSTANDING_DUE_DAYS;
	private $PASSWORD_EXPIRE_DAYS;
	private $PASSWORD_ERROR_ATTEMPTS;
	private $LOCAL_IP;
	private $LIVE_IP;
	private $commitArray = array();
	private $field_array = array();
	

	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('intCompanyId'=>'intCompanyId',
										'intActiveCRform'=>'intActiveCRform',
										'PAYMENT_NOTIFICATION_DAYS'=>'PAYMENT_NOTIFICATION_DAYS',
										'CUSTOMER_INVOICE_DELAY_DAYS'=>'CUSTOMER_INVOICE_DELAY_DAYS',
										'PAYMENT_OUTSTANDING_PENDING_DAYS'=>'PAYMENT_OUTSTANDING_PENDING_DAYS',
										'PAYMENT_OUTSTANDING_DUE_DAYS'=>'PAYMENT_OUTSTANDING_DUE_DAYS',
										'PASSWORD_EXPIRE_DAYS'=>'PASSWORD_EXPIRE_DAYS',
										'PASSWORD_ERROR_ATTEMPTS'=>'PASSWORD_ERROR_ATTEMPTS',
										'LOCAL_IP'=>'LOCAL_IP',
										'LIVE_IP'=>'LIVE_IP',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit );
		return $result;
	}

	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data);
		return $this->db->getResult();


	}

	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}

	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();
	}

	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}

	public function get_field_array(){
		return $this->field_array;
	}

	function commit($type='update')
	{
		//commit update
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{
			$data[$v] = $this->$v;
		}
		$where		= "intCompanyId = ".$this->intCompanyId."" ;
		$this->commitArray = array();

		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}

	//BEGIN - public functions for private properties {
	
	//retun intCompanyId
	function getintCompanyId()
	{
		$this->validate();
		return $this->intCompanyId;
	}
	
	//retun intActiveCRform
	function getintActiveCRform()
	{
		$this->validate();
		return $this->intActiveCRform;
	}
	
	//retun PAYMENT_NOTIFICATION_DAYS
	function getPAYMENT_NOTIFICATION_DAYS()
	{
		$this->validate();
		return $this->PAYMENT_NOTIFICATION_DAYS;
	}
	
	//retun CUSTOMER_INVOICE_DELAY_DAYS
	function getCUSTOMER_INVOICE_DELAY_DAYS()
	{
		$this->validate();
		return $this->CUSTOMER_INVOICE_DELAY_DAYS;
	}
	
	//retun PAYMENT_OUTSTANDING_PENDING_DAYS
	function getPAYMENT_OUTSTANDING_PENDING_DAYS()
	{
		$this->validate();
		return $this->PAYMENT_OUTSTANDING_PENDING_DAYS;
	}
	
	//retun PAYMENT_OUTSTANDING_DUE_DAYS
	function getPAYMENT_OUTSTANDING_DUE_DAYS()
	{
		$this->validate();
		return $this->PAYMENT_OUTSTANDING_DUE_DAYS;
	}
	
	//retun PASSWORD_EXPIRE_DAYS
	function getPASSWORD_EXPIRE_DAYS()
	{
		$this->validate();
		return $this->PASSWORD_EXPIRE_DAYS;
	}
	
	//retun PASSWORD_ERROR_ATTEMPTS
	function getPASSWORD_ERROR_ATTEMPTS()
	{
		$this->validate();
		return $this->PASSWORD_ERROR_ATTEMPTS;
	}
	
	//retun LOCAL_IP
	function getLOCAL_IP()
	{
		$this->validate();
		return $this->LOCAL_IP;
	}
	
	//retun LIVE_IP
	function getLIVE_IP()
	{
		$this->validate();
		return $this->LIVE_IP;
	}
	
	//END }

	//BEGIN - public function set {
	
	//set intCompanyId
	function setintCompanyId($intCompanyId)
	{
		array_push($this->commitArray,'intCompanyId');
		$this->intCompanyId = $intCompanyId;
	}
	
	//set intActiveCRform
	function setintActiveCRform($intActiveCRform)
	{
		array_push($this->commitArray,'intActiveCRform');
		$this->intActiveCRform = $intActiveCRform;
	}
	
	//set PAYMENT_NOTIFICATION_DAYS
	function setPAYMENT_NOTIFICATION_DAYS($PAYMENT_NOTIFICATION_DAYS)
	{
		array_push($this->commitArray,'PAYMENT_NOTIFICATION_DAYS');
		$this->PAYMENT_NOTIFICATION_DAYS = $PAYMENT_NOTIFICATION_DAYS;
	}
	
	//set CUSTOMER_INVOICE_DELAY_DAYS
	function setCUSTOMER_INVOICE_DELAY_DAYS($CUSTOMER_INVOICE_DELAY_DAYS)
	{
		array_push($this->commitArray,'CUSTOMER_INVOICE_DELAY_DAYS');
		$this->CUSTOMER_INVOICE_DELAY_DAYS = $CUSTOMER_INVOICE_DELAY_DAYS;
	}
	
	//set PAYMENT_OUTSTANDING_PENDING_DAYS
	function setPAYMENT_OUTSTANDING_PENDING_DAYS($PAYMENT_OUTSTANDING_PENDING_DAYS)
	{
		array_push($this->commitArray,'PAYMENT_OUTSTANDING_PENDING_DAYS');
		$this->PAYMENT_OUTSTANDING_PENDING_DAYS = $PAYMENT_OUTSTANDING_PENDING_DAYS;
	}
	
	//set PAYMENT_OUTSTANDING_DUE_DAYS
	function setPAYMENT_OUTSTANDING_DUE_DAYS($PAYMENT_OUTSTANDING_DUE_DAYS)
	{
		array_push($this->commitArray,'PAYMENT_OUTSTANDING_DUE_DAYS');
		$this->PAYMENT_OUTSTANDING_DUE_DAYS = $PAYMENT_OUTSTANDING_DUE_DAYS;
	}
	
	//set PASSWORD_EXPIRE_DAYS
	function setPASSWORD_EXPIRE_DAYS($PASSWORD_EXPIRE_DAYS)
	{
		array_push($this->commitArray,'PASSWORD_EXPIRE_DAYS');
		$this->PASSWORD_EXPIRE_DAYS = $PASSWORD_EXPIRE_DAYS;
	}
	
	//set PASSWORD_ERROR_ATTEMPTS
	function setPASSWORD_ERROR_ATTEMPTS($PASSWORD_ERROR_ATTEMPTS)
	{
		array_push($this->commitArray,'PASSWORD_ERROR_ATTEMPTS');
		$this->PASSWORD_ERROR_ATTEMPTS = $PASSWORD_ERROR_ATTEMPTS;
	}
	
	//set LOCAL_IP
	function setLOCAL_IP($LOCAL_IP)
	{
		array_push($this->commitArray,'LOCAL_IP');
		$this->LOCAL_IP = $LOCAL_IP;
	}
	
	//set LIVE_IP
	function setLIVE_IP($LIVE_IP)
	{
		array_push($this->commitArray,'LIVE_IP');
		$this->LIVE_IP = $LIVE_IP;
	}
	
	//END }

	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->intCompanyId=='')
			throw new exception("Set primary values first");
		else
			return true;
	}

	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}

	public function set($intCompanyId)
	{
		$cols	= "*";

		$join	= NULL;

		$where	= "intCompanyId='$intCompanyId'";

		$result = $this->select($cols,$join,$where);
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}

	//insert as parameters
	public function insertRec($intCompanyId,$intActiveCRform,$PAYMENT_NOTIFICATION_DAYS,$CUSTOMER_INVOICE_DELAY_DAYS,$PAYMENT_OUTSTANDING_PENDING_DAYS,$PAYMENT_OUTSTANDING_DUE_DAYS,$PASSWORD_EXPIRE_DAYS,$PASSWORD_ERROR_ATTEMPTS,$LOCAL_IP,$LIVE_IP){
		$data = array('intCompanyId'=>$intCompanyId 
				,'intActiveCRform'=>$intActiveCRform 
				,'PAYMENT_NOTIFICATION_DAYS'=>$PAYMENT_NOTIFICATION_DAYS 
				,'CUSTOMER_INVOICE_DELAY_DAYS'=>$CUSTOMER_INVOICE_DELAY_DAYS 
				,'PAYMENT_OUTSTANDING_PENDING_DAYS'=>$PAYMENT_OUTSTANDING_PENDING_DAYS 
				,'PAYMENT_OUTSTANDING_DUE_DAYS'=>$PAYMENT_OUTSTANDING_DUE_DAYS 
				,'PASSWORD_EXPIRE_DAYS'=>$PASSWORD_EXPIRE_DAYS 
				,'PASSWORD_ERROR_ATTEMPTS'=>$PASSWORD_ERROR_ATTEMPTS 
				,'LOCAL_IP'=>$LOCAL_IP 
				,'LIVE_IP'=>$LIVE_IP 
				);
		return $this->insert($data);
	}

	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('intCompanyId,intActiveCRform',  null, $where = $where);
		$html = '<option value=""></option>';
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intCompanyId'])
				$html .= '<option selected="selected" value="'.$row['intCompanyId'].'">'.$row['intActiveCRform'].'</option>';
			else
				$html .= '<option value="'.$row['intCompanyId'].'">'.$row['intActiveCRform'].'</option>';
		}
		return $html;
	}
	
	//END }
}
?>
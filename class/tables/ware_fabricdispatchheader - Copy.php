
<?php

class ware_fabricdispatchheader{
 
	private $db;
	private $table= "ware_fabricdispatchheader";
	
	//private property
	private $intBulkDispatchNo;
	private $intBulkDispatchNoYear;
	private $intOrderNo;
	private $intOrderYear;
	private $strAODNo;
	private $intCustLocation;
	private $strRemarks;
	private $intStatus;
	private $intApproveLevels;
	private $dtmdate;
	private $dtmCreateDate;
	private $intCteatedBy;
	private $dtmModifiedDate;
	private $intModifiedBy;
	private $intCompanyId;
	private $invoiced;
	private $pubArray = array();
	
	function __construct($db)
	{
		$this->db = $db;
		$this->db->field_array_set(array('intBulkDispatchNo'=>'intBulkDispatchNo',
										'intBulkDispatchNoYear'=>'intBulkDispatchNoYear',
										'intOrderNo'=>'intOrderNo',
										'intOrderYear'=>'intOrderYear',
										'strAODNo'=>'strAODNo',
										'intCustLocation'=>'intCustLocation',
										'strRemarks'=>'strRemarks',
										'intStatus'=>'intStatus',
										'intApproveLevels'=>'intApproveLevels',
										'dtmdate'=>'dtmdate',
										'dtmCreateDate'=>'dtmCreateDate',
										'intCteatedBy'=>'intCteatedBy',
										'dtmModifiedDate'=>'dtmModifiedDate',
										'intModifiedBy'=>'intModifiedBy',
										'intCompanyId'=>'intCompanyId',
										'invoiced'=>'invoiced',
										));
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  
		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	//public functions for private properties
	
	//retun intBulkDispatchNo
	function getintBulkDispatchNo()
	{
		$this->validate();
		return $this->intBulkDispatchNo;
	}
	
	//retun intBulkDispatchNoYear
	function getintBulkDispatchNoYear()
	{
		$this->validate();
		return $this->intBulkDispatchNoYear;
	}
	
	//retun intOrderNo
	function getintOrderNo()
	{
		$this->validate();
		return $this->intOrderNo;
	}
	
	//retun intOrderYear
	function getintOrderYear()
	{
		$this->validate();
		return $this->intOrderYear;
	}
	
	//retun strAODNo
	function getstrAODNo()
	{
		$this->validate();
		return $this->strAODNo;
	}
	
	//retun intCustLocation
	function getintCustLocation()
	{
		$this->validate();
		return $this->intCustLocation;
	}
	
	//retun strRemarks
	function getstrRemarks()
	{
		$this->validate();
		return $this->strRemarks;
	}
	
	//retun intStatus
	function getintStatus()
	{
		$this->validate();
		return $this->intStatus;
	}
	
	function setStatus($intStatus)
	{		
		array_push($this->pubArray,'intStatus');
		$this->intStatus = $intStatus;
	}
	
		//retun intApproveLevels
	function getintApproveLevels()
	{
		$this->validate();
		return $this->intApproveLevels;
	}
	
	function setintApproveLevels($intApproveLevels)
	{
		array_push($this->pubArray,'intApproveLevels');
		$this->intApproveLevels = $intApproveLevels;		
	}
	
	function commit()
	{		
		$data	= array();
		foreach($this->pubArray as $k=>$v)
		{			
			$data["$v"] = ''.$this->$v.'';
		}
		$where		= "intBulkDispatchNo = '".$this->intBulkDispatchNo."' AND intBulkDispatchNoYear = '".$this->intBulkDispatchNoYear."' " ;
		unset($this->pubArray);
		return $this->update($data,$where);
	}
	
	//retun dtmdate
	function getdtmdate()
	{
		$this->validate();
		return $this->dtmdate;
	}
	
	//retun dtmCreateDate
	function getdtmCreateDate()
	{
		$this->validate();
		return $this->dtmCreateDate;
	}
	
	//retun intCteatedBy
	function getintCteatedBy()
	{
		$this->validate();
		return $this->intCteatedBy;
	}
	
	//retun dtmModifiedDate
	function getdtmModifiedDate()
	{
		$this->validate();
		return $this->dtmModifiedDate;
	}
	
	//retun intModifiedBy
	function getintModifiedBy()
	{
		$this->validate();
		return $this->intModifiedBy;
	}
	
	//retun intCompanyId
	function getintCompanyId()
	{
		$this->validate();
		return $this->intCompanyId;
	}
	
	//retun invoiced
	function getinvoiced()
	{
		$this->validate();
		return $this->invoiced;
	}
	
	//validate primary values
	private function validate()
	{
		if($this->intBulkDispatchNo=='' || $this->intBulkDispatchNoYear=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
		}
	}
	
	public function set($intBulkDispatchNo , $intBulkDispatchNoYear)
	{
		$result = $this->select('*',null,"intBulkDispatchNo='$intBulkDispatchNo' and intBulkDispatchNoYear='$intBulkDispatchNoYear'");	
		if($this->db->numRows()>0)
			$this->setVariables($result);
		else
			throw new Exception("Record not found.");
			
	}
	
	
}
?>

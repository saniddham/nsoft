<?php
	include_once MAIN_ROOT."class/tables/mst_colors.php";							$mst_colors 						= new mst_colors($db);
	include_once MAIN_ROOT."class/tables/mst_techniques.php";						$mst_techniques 					= new mst_techniques($db);
	include_once MAIN_ROOT."class/tables/mst_inktypes.php";						$mst_inktypes 						= new mst_inktypes($db);
	

class ink_color_room_issue_to_production_details{
 
	private $db;
	private $table= "ink_color_room_issue_to_production_details";
	
	//private property
	private $SERIAL_NO;
	private $SERIAL_YEAR;
	private $ORDER_NO;
	private $ORDER_YEAR;
	private $SALES_ORDER_ID;
	private $INK_COLOR;
	private $INK_TECHNIQUE;
	private $INK_TYPE;
	private $INK_WEIGHT;
	private $INK_WEIGHT_BALANCE;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('SERIAL_NO'=>'SERIAL_NO',
										'SERIAL_YEAR'=>'SERIAL_YEAR',
										'ORDER_NO'=>'ORDER_NO',
										'ORDER_YEAR'=>'ORDER_YEAR',
										'SALES_ORDER_ID'=>'SALES_ORDER_ID',
										'INK_COLOR'=>'INK_COLOR',
										'INK_TECHNIQUE'=>'INK_TECHNIQUE',
										'INK_TYPE'=>'INK_TYPE',
										'INK_WEIGHT'=>'INK_WEIGHT',
										'INK_WEIGHT_BALANCE'=>'INK_WEIGHT_BALANCE',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "SERIAL_NO = ".$this->SERIAL_NO." and SERIAL_YEAR = ".$this->SERIAL_YEAR." and ORDER_NO = ".$this->ORDER_NO." and ORDER_YEAR = ".$this->ORDER_YEAR." and SALES_ORDER_ID = ".$this->SALES_ORDER_ID." and INK_COLOR = ".$this->INK_COLOR." and INK_TECHNIQUE = ".$this->INK_TECHNIQUE." and INK_TYPE = ".$this->INK_TYPE."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun SERIAL_NO
	function getSERIAL_NO()
	{
		$this->validate();
		return $this->SERIAL_NO;
	}
	
	//retun SERIAL_YEAR
	function getSERIAL_YEAR()
	{
		$this->validate();
		return $this->SERIAL_YEAR;
	}
	
	//retun ORDER_NO
	function getORDER_NO()
	{
		$this->validate();
		return $this->ORDER_NO;
	}
	
	//retun ORDER_YEAR
	function getORDER_YEAR()
	{
		$this->validate();
		return $this->ORDER_YEAR;
	}
	
	//retun SALES_ORDER_ID
	function getSALES_ORDER_ID()
	{
		$this->validate();
		return $this->SALES_ORDER_ID;
	}
	
	//retun INK_COLOR
	function getINK_COLOR()
	{
		$this->validate();
		return $this->INK_COLOR;
	}
	
	//retun INK_TECHNIQUE
	function getINK_TECHNIQUE()
	{
		$this->validate();
		return $this->INK_TECHNIQUE;
	}
	
	//retun INK_TYPE
	function getINK_TYPE()
	{
		$this->validate();
		return $this->INK_TYPE;
	}
	
	//retun INK_WEIGHT
	function getINK_WEIGHT()
	{
		$this->validate();
		return $this->INK_WEIGHT;
	}
	
	//retun INK_WEIGHT_BALANCE
	function getINK_WEIGHT_BALANCE()
	{
		$this->validate();
		return $this->INK_WEIGHT_BALANCE;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set SERIAL_NO
	function setSERIAL_NO($SERIAL_NO)
	{
		array_push($this->commitArray,'SERIAL_NO');
		$this->SERIAL_NO = $SERIAL_NO;
	}
	
	//set SERIAL_YEAR
	function setSERIAL_YEAR($SERIAL_YEAR)
	{
		array_push($this->commitArray,'SERIAL_YEAR');
		$this->SERIAL_YEAR = $SERIAL_YEAR;
	}
	
	//set ORDER_NO
	function setORDER_NO($ORDER_NO)
	{
		array_push($this->commitArray,'ORDER_NO');
		$this->ORDER_NO = $ORDER_NO;
	}
	
	//set ORDER_YEAR
	function setORDER_YEAR($ORDER_YEAR)
	{
		array_push($this->commitArray,'ORDER_YEAR');
		$this->ORDER_YEAR = $ORDER_YEAR;
	}
	
	//set SALES_ORDER_ID
	function setSALES_ORDER_ID($SALES_ORDER_ID)
	{
		array_push($this->commitArray,'SALES_ORDER_ID');
		$this->SALES_ORDER_ID = $SALES_ORDER_ID;
	}
	
	//set INK_COLOR
	function setINK_COLOR($INK_COLOR)
	{
		array_push($this->commitArray,'INK_COLOR');
		$this->INK_COLOR = $INK_COLOR;
	}
	
	//set INK_TECHNIQUE
	function setINK_TECHNIQUE($INK_TECHNIQUE)
	{
		array_push($this->commitArray,'INK_TECHNIQUE');
		$this->INK_TECHNIQUE = $INK_TECHNIQUE;
	}
	
	//set INK_TYPE
	function setINK_TYPE($INK_TYPE)
	{
		array_push($this->commitArray,'INK_TYPE');
		$this->INK_TYPE = $INK_TYPE;
	}
	
	//set INK_WEIGHT
	function setINK_WEIGHT($INK_WEIGHT)
	{
		array_push($this->commitArray,'INK_WEIGHT');
		$this->INK_WEIGHT = $INK_WEIGHT;
	}
	
	//set INK_WEIGHT_BALANCE
	function setINK_WEIGHT_BALANCE($INK_WEIGHT_BALANCE)
	{
		array_push($this->commitArray,'INK_WEIGHT_BALANCE');
		$this->INK_WEIGHT_BALANCE = $INK_WEIGHT_BALANCE;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->SERIAL_NO=='' || $this->SERIAL_YEAR=='' || $this->ORDER_NO=='' || $this->ORDER_YEAR=='' || $this->SALES_ORDER_ID=='' || $this->INK_COLOR=='' || $this->INK_TECHNIQUE=='' || $this->INK_TYPE=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($SERIAL_NO , $SERIAL_YEAR , $ORDER_NO , $ORDER_YEAR , $SALES_ORDER_ID , $INK_COLOR , $INK_TECHNIQUE , $INK_TYPE)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "SERIAL_NO='$SERIAL_NO' and SERIAL_YEAR='$SERIAL_YEAR' and ORDER_NO='$ORDER_NO' and ORDER_YEAR='$ORDER_YEAR' and SALES_ORDER_ID='$SALES_ORDER_ID' and INK_COLOR='$INK_COLOR' and INK_TECHNIQUE='$INK_TECHNIQUE' and INK_TYPE='$INK_TYPE'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($SERIAL_NO,$SERIAL_YEAR,$ORDER_NO,$ORDER_YEAR,$SALES_ORDER_ID,$INK_COLOR,$INK_TECHNIQUE,$INK_TYPE,$INK_WEIGHT,$INK_WEIGHT_BALANCE){
		$data = array('SERIAL_NO'=>$SERIAL_NO 
				,'SERIAL_YEAR'=>$SERIAL_YEAR 
				,'ORDER_NO'=>$ORDER_NO 
				,'ORDER_YEAR'=>$ORDER_YEAR 
				,'SALES_ORDER_ID'=>$SALES_ORDER_ID 
				,'INK_COLOR'=>$INK_COLOR 
				,'INK_TECHNIQUE'=>$INK_TECHNIQUE 
				,'INK_TYPE'=>$INK_TYPE 
				,'INK_WEIGHT'=>$INK_WEIGHT 
				,'INK_WEIGHT_BALANCE'=>$INK_WEIGHT_BALANCE 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('SERIAL_NO,SERIAL_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['SERIAL_NO'])
				$html .= '<option selected="selected" value="'.$row['SERIAL_NO'].'">'.$row['SERIAL_YEAR'].'</option>';	
			else
				$html .= '<option value="'.$row['SERIAL_NO'].'">'.$row['SERIAL_YEAR'].'</option>';	
		}
		return $html;
	}
	public function getDetailData($serialNo,$serialYear)
	{
		$cols	= " ink_color_room_issue_to_production_details.ORDER_NO,
					ink_color_room_issue_to_production_details.ORDER_YEAR,
					ink_color_room_issue_to_production_details.SALES_ORDER_ID,
					ink_color_room_issue_to_production_details.INK_COLOR,
					ink_color_room_issue_to_production_details.INK_TECHNIQUE,
					ink_color_room_issue_to_production_details.INK_TYPE,
					ink_color_room_issue_to_production_details.INK_WEIGHT ";
		
		$where	= " ink_color_room_issue_to_production_details.SERIAL_NO = '".$serialNo."' AND
					ink_color_room_issue_to_production_details.SERIAL_YEAR = '".$serialYear."' ";
		
		$result = $this->select($cols,$join=null,$where,$order = null, $limit = null);	
	
		return $result;	
	}
	public function getIssuedWeight($location,$serialNo,$serialYear,$orderNo,$orderYear,$salesOrderId,$color,$tehnique,$inkType,$deci)
	{
		$cols	= " COALESCE(ROUND(SUM(ink_color_room_issue_to_production_details.INK_WEIGHT),$deci+2),0) AS ISSUED_WEIGHT ";
		
		$join	= " INNER JOIN ink_color_room_issue_to_production_header CIPH ON CIPH.SERIAL_NO=ink_color_room_issue_to_production_details.SERIAL_NO AND
					CIPH.SERIAL_YEAR=ink_color_room_issue_to_production_details.SERIAL_YEAR ";
		
		$where	= "	CIPH.LOCATION_ID = '".$location."' AND CIPH.STATUS = 1 ";
		
		/*if($serialNo!='')
			$where	.= " AND ink_color_room_issue_to_production_details.SERIAL_NO = '".$serialNo."' ";
		
		if($serialYear!='')
			$where	.= " AND ink_color_room_issue_to_production_details.SERIAL_YEAR = '".$serialYear."' ";*/
		
		if($orderNo!='')
			$where	.= " AND ink_color_room_issue_to_production_details.ORDER_NO = '".$orderNo."' ";
		
		if($orderYear!='')
			$where	.= " AND ink_color_room_issue_to_production_details.ORDER_YEAR = '".$orderYear."' ";
			
		if($salesOrderId!='')
			$where	.= " AND ink_color_room_issue_to_production_details.SALES_ORDER_ID = '".$salesOrderId."' ";
			
		if($color!='')
			$where	.= " AND ink_color_room_issue_to_production_details.INK_COLOR = '".$color."' ";
	
		if($tehnique!='')
			$where	.= " AND ink_color_room_issue_to_production_details.INK_TECHNIQUE = '".$tehnique."' ";
			
		if($inkType!='')
			$where	.= " AND ink_color_room_issue_to_production_details.INK_TYPE = '".$inkType."' ";
				
	
		//echo "select ".$cols." from trn_orderdetails ".$join." where ".$where." order by ".$order;
		$result = $this->select($cols,$join,$where,$order = null, $limit = null);	
		
		$row	= mysqli_fetch_array($result);
		return 	$row['ISSUED_WEIGHT'];

	}
	public function getSavedGraphicNos($serialNo,$serialYear)
	{
		$cols	= " GROUP_CONCAT(DISTINCT OD.strGraphicNo) AS graphicNo ";
	
		$join	= " INNER JOIN trn_orderdetails OD ON OD.intOrderNo=ink_color_room_issue_to_production_details.ORDER_NO AND
					OD.intOrderYear=ink_color_room_issue_to_production_details.ORDER_YEAR AND
					OD.intSalesOrderId=ink_color_room_issue_to_production_details.SALES_ORDER_ID ";
		
		$where	= " ink_color_room_issue_to_production_details.SERIAL_NO = '".$serialNo."' AND 
					ink_color_room_issue_to_production_details.SERIAL_YEAR = '".$serialYear."' ";
		
		$result = $this->select($cols,$join,$where,$order = null, $limit = null);	
		$row	= mysqli_fetch_array($result);
		
		return $row['graphicNo'];
	}
	//END }
	
##################################
##     COMMON FUNCTIONS      	##
##################################

	public function loadIssuedGraphicNos($locationId,$orderYear,$orderNo,$graphicNo)
	{
		$cols	= "DISTINCT
  						OD.strGraphicNo	AS GRAPHIC_NO";
	
		$join	= "INNER JOIN ink_color_room_issue_to_production_header IH
						ON IH.SERIAL_NO = ink_color_room_issue_to_production_details.SERIAL_NO
					  	AND IH.SERIAL_YEAR = ink_color_room_issue_to_production_details.SERIAL_YEAR
				   INNER JOIN trn_orderdetails OD
						ON OD.intOrderNo = ink_color_room_issue_to_production_details.ORDER_NO
					  	AND OD.intOrderYear = ink_color_room_issue_to_production_details.ORDER_YEAR
					  	AND OD.intSalesOrderId = ink_color_room_issue_to_production_details.SALES_ORDER_ID";
		
		$where	= "IH.STATUS = 1
					AND OD.intOrderYear = $orderYear
					AND OD.intOrderNo LIKE '%$orderNo%'
					AND OD.strGraphicNo LIKE '%$graphicNo%'
   					AND IH.LOCATION_ID = $locationId";
		
		return $this->select($cols,$join,$where,$order = null, $limit = null);	
	}
	
	public function getIssuedOrderNos($orderYear,$graphicNo,$locationId)
	{
		$cols	= "DISTINCT
					  ORDER_NO        AS ORDER_NO,
					  ORDER_YEAR      AS ORDER_YEAR,
					  strSalesOrderNo AS SALES_ORDER_ID,
					  OD.intPart 	  AS PART_ID,
					  CONCAT(OD.intOrderNo,'/',OD.intOrderYear,'/',OD.intSalesOrderId) AS VALUE,
					  CONCAT(OD.intOrderNo,'/',OD.intOrderYear,' - ',OD.strSalesOrderNo) AS TEXT";
	
		$join	= "INNER JOIN ink_color_room_issue_to_production_header IH
						ON IH.SERIAL_NO = ink_color_room_issue_to_production_details.SERIAL_NO
						  AND IH.SERIAL_YEAR = ink_color_room_issue_to_production_details.SERIAL_YEAR
					  INNER JOIN trn_orderdetails OD
						ON OD.intOrderNo = ink_color_room_issue_to_production_details.ORDER_NO
						  AND OD.intOrderYear = ink_color_room_issue_to_production_details.ORDER_YEAR
						  AND OD.intSalesOrderId = ink_color_room_issue_to_production_details.SALES_ORDER_ID
					  INNER JOIN trn_orderheader OH 
					  	ON OD.intOrderNo = OH.intOrderNo 
						AND OD.intOrderYear = OH.intOrderYear";
	
						
		$where	= " IH.STATUS = 1
					AND OH.intStatus = 1
					AND OD.intOrderYear = $orderYear
					AND OD.strGraphicNo = '$graphicNo'
					AND IH.LOCATION_ID = $locationId";
	
		return $this->select($cols,$join,$where,$order = null, $limit = null);	
	}

	public function getIssueNo($concatOrderNo,$locationId)
	{
		$cols	= "DISTINCT
					  CONCAT(IH.SERIAL_NO,'/',IH.SERIAL_YEAR) 						AS VALUE,
					  CONCAT(IH.SERIAL_NO,'/',IH.SERIAL_YEAR) 						AS TEXT";
	
		$join	= "INNER JOIN ink_color_room_issue_to_production_header IH
						ON IH.SERIAL_NO = ink_color_room_issue_to_production_details.SERIAL_NO
						  AND IH.SERIAL_YEAR = ink_color_room_issue_to_production_details.SERIAL_YEAR";
		
		$where	= "IH.STATUS = 1
						AND CONCAT(ORDER_NO,'/',ORDER_YEAR,'/',SALES_ORDER_ID) IN ($concatOrderNo)
						AND IH.LOCATION_ID = $locationId";
		
		return $this->select($cols,$join,$where,$order = null, $limit = null);	
	}
	
	public function getDetails($issueNo,$issueYear,$concatOrderNo,$colorId,$techId,$inkId)
	{
		if($concatOrderNo!='')
			$para	= "AND CONCAT(ORDER_NO,'/',ORDER_YEAR,'/',SALES_ORDER_ID) IN ($concatOrderNo)";
		if($colorId!='')
			$para  .= "AND INK_COLOR = $colorId ";
		if($techId!='')
			$para  .= "AND INK_TECHNIQUE = $techId ";
		if($inkId!='')
			$para  .= "AND INK_TYPE = $inkId ";
			
		$cols	= "INK_COLOR          	AS INK_COLOR,
				   INK_TECHNIQUE      	AS INK_TECHNIQUE,
				   INK_TYPE           	AS INK_TYPE,
				   INK_WEIGHT         	AS INK_WEIGHT,
				   INK_WEIGHT_BALANCE 	AS INK_WEIGHT_BALANCE";

		$where	= "SERIAL_NO = '$issueNo'
					AND SERIAL_YEAR = '$issueYear'
					$para";
		return $this->select($cols,$join=null,$where,$order=null,$limit=null);	
	}

	public function checkForAllColorsIssuedToProduction($location,$orderNo,$orderYear,$salesOrderId,$deci){
		global $trn_orderdetails;
		global $mst_colors;
		global $mst_inktypes;
		global $mst_techniques;
		
		$flag	=1;
		$colorMsg_arr =NULL;
		
		$trn_orderdetails->set($orderNo,$orderYear,$salesOrderId);
		$result	= $trn_orderdetails->getColorDetailsForSelectedOrder($orderYear,$orderNo,$salesOrderId);
		$resultc	= $trn_orderdetails->getColorDetailsForSelectedOrder($orderYear,$orderNo,$salesOrderId);
		$recs		= mysqli_num_rows($resultc);
		
		
		while($row = mysqli_fetch_assoc($result)){
			if($recs > 0){
				$color		=$row['intColorId'];
				$tehnique	=$row['intTechniqueId'];
				$inkType	=$row['intInkTypeId'];	
				$mst_colors->set($color);
				$mst_techniques->set($tehnique);
				$mst_inktypes->set($inkType);
				$issued	= $this->getIssuedWeight($location,'','',$orderNo,$orderYear,$salesOrderId,$color,$tehnique,$inkType,$deci);
				
				if($issued==0 || $issued==''){
					$flag	=0;
					if($colorMsg_arr[$orderYear][$orderNo][$salesOrderId] !=1)
					$msg	.=	$orderNo.'/'.$orderYear.'/'.$trn_orderdetails->getstrSalesOrderNo().'</br>'.'/'.$mst_colors->getstrName().'/'.$mst_inktypes->getstrName().'/'.$mst_techniques->getstrName().'</br>';
					$colorMsg_arr[$orderYear][$orderNo][$salesOrderId]= 1;
				}
			}
		}
		$data['flag']	=$flag;
		$data['msg']	=$msg;
		
		return $data;//0 - there exists not issued colors
	}
	
	public function getIssuedQty($issueNo,$issueYear,$concatOrderNo,$colorId,$techId,$inkId,$location,$deci)
	{
		$cols	= "ROUND(SUM(INK_WEIGHT),$deci+2) 	AS INK_WEIGHT";
		
		$join 	= "INNER JOIN ink_color_room_issue_to_production_header H
		           		ON H.SERIAL_NO = ink_color_room_issue_to_production_details.SERIAL_NO
						AND H.SERIAL_YEAR = ink_color_room_issue_to_production_details.SERIAL_YEAR";
		
		$where	= "H.SERIAL_NO = $issueNo
		           AND H.SERIAL_YEAR = $issueYear
				   AND CONCAT(ORDER_NO,'/',ORDER_YEAR,'/',SALES_ORDER_ID) IN ($concatOrderNo)
		           AND INK_COLOR = $colorId
				   AND INK_TECHNIQUE = $techId
				   AND INK_TYPE = $inkId
				   AND H.STATUS = 1
				   AND H.LOCATION_ID = $location";
		$return = $this->select($cols,$join,$where,$order=null,$limit=null);	
		$row	= mysqli_fetch_array($return);
		return $row['INK_WEIGHT'];
	}
	
	//END }
}
?>
<?php
class ware_fabricgptransferinheader{
 
	private $db;
	private $table= "ware_fabricgptransferinheader";
	
	//private property
	private $intFabricGPTransfInNo;
	private $intFabricGPTransfInYear;
	private $intGPNo;
	private $intGPYear;
	private $intOrderNo;
	private $intOrderYear;
	private $strRemarks;
	private $intStatus;
	private $intApproveLevels;
	private $dtmdate;
	private $dtmCreateDate;
	private $intCteatedBy;
	private $dtmModifiedDate;
	private $intModifiedBy;
	private $intCompanyId;
	private $commitArray = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->db->field_array_set(array('intFabricGPTransfInNo'=>'intFabricGPTransfInNo',
										'intFabricGPTransfInYear'=>'intFabricGPTransfInYear',
										'intGPNo'=>'intGPNo',
										'intGPYear'=>'intGPYear',
										'intOrderNo'=>'intOrderNo',
										'intOrderYear'=>'intOrderYear',
										'strRemarks'=>'strRemarks',
										'intStatus'=>'intStatus',
										'intApproveLevels'=>'intApproveLevels',
										'dtmdate'=>'dtmdate',
										'dtmCreateDate'=>'dtmCreateDate',
										'intCteatedBy'=>'intCteatedBy',
										'dtmModifiedDate'=>'dtmModifiedDate',
										'intModifiedBy'=>'intModifiedBy',
										'intCompanyId'=>'intCompanyId',
										));
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function commit()
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = ''.$this->$v.'';
		}
		$where		= "intFabricGPTransfInNo = '".$this->intFabricGPTransfInNo."' and intFabricGPTransfInYear = '".$this->intFabricGPTransfInYear."'" ;
		unset($this->commitArray);
		return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun intFabricGPTransfInNo
	function getintFabricGPTransfInNo()
	{
		$this->validate();
		return $this->intFabricGPTransfInNo;
	}
	
	//retun intFabricGPTransfInYear
	function getintFabricGPTransfInYear()
	{
		$this->validate();
		return $this->intFabricGPTransfInYear;
	}
	
	//retun intGPNo
	function getintGPNo()
	{
		$this->validate();
		return $this->intGPNo;
	}
	
	//retun intGPYear
	function getintGPYear()
	{
		$this->validate();
		return $this->intGPYear;
	}
	
	//retun intOrderNo
	function getintOrderNo()
	{
		$this->validate();
		return $this->intOrderNo;
	}
	
	//retun intOrderYear
	function getintOrderYear()
	{
		$this->validate();
		return $this->intOrderYear;
	}
	
	//retun strRemarks
	function getstrRemarks()
	{
		$this->validate();
		return $this->strRemarks;
	}
	
	//retun intStatus
	function getintStatus()
	{
		$this->validate();
		return $this->intStatus;
	}
	
	//retun intApproveLevels
	function getintApproveLevels()
	{
		$this->validate();
		return $this->intApproveLevels;
	}
	
	//retun dtmdate
	function getdtmdate()
	{
		$this->validate();
		return $this->dtmdate;
	}
	
	//retun dtmCreateDate
	function getdtmCreateDate()
	{
		$this->validate();
		return $this->dtmCreateDate;
	}
	
	//retun intCteatedBy
	function getintCteatedBy()
	{
		$this->validate();
		return $this->intCteatedBy;
	}
	
	//retun dtmModifiedDate
	function getdtmModifiedDate()
	{
		$this->validate();
		return $this->dtmModifiedDate;
	}
	
	//retun intModifiedBy
	function getintModifiedBy()
	{
		$this->validate();
		return $this->intModifiedBy;
	}
	
	//retun intCompanyId
	function getintCompanyId()
	{
		$this->validate();
		return $this->intCompanyId;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set intFabricGPTransfInNo
	function setintFabricGPTransfInNo($intFabricGPTransfInNo)
	{
		array_push($this->commitArray,'intFabricGPTransfInNo');
		$this->intFabricGPTransfInNo = $intFabricGPTransfInNo;
	}
	
	//set intFabricGPTransfInYear
	function setintFabricGPTransfInYear($intFabricGPTransfInYear)
	{
		array_push($this->commitArray,'intFabricGPTransfInYear');
		$this->intFabricGPTransfInYear = $intFabricGPTransfInYear;
	}
	
	//set intGPNo
	function setintGPNo($intGPNo)
	{
		array_push($this->commitArray,'intGPNo');
		$this->intGPNo = $intGPNo;
	}
	
	//set intGPYear
	function setintGPYear($intGPYear)
	{
		array_push($this->commitArray,'intGPYear');
		$this->intGPYear = $intGPYear;
	}
	
	//set intOrderNo
	function setintOrderNo($intOrderNo)
	{
		array_push($this->commitArray,'intOrderNo');
		$this->intOrderNo = $intOrderNo;
	}
	
	//set intOrderYear
	function setintOrderYear($intOrderYear)
	{
		array_push($this->commitArray,'intOrderYear');
		$this->intOrderYear = $intOrderYear;
	}
	
	//set strRemarks
	function setstrRemarks($strRemarks)
	{
		array_push($this->commitArray,'strRemarks');
		$this->strRemarks = $strRemarks;
	}
	
	//set intStatus
	function setintStatus($intStatus)
	{
		array_push($this->commitArray,'intStatus');
		$this->intStatus = $intStatus;
	}
	
	//set intApproveLevels
	function setintApproveLevels($intApproveLevels)
	{
		array_push($this->commitArray,'intApproveLevels');
		$this->intApproveLevels = $intApproveLevels;
	}
	
	//set dtmdate
	function setdtmdate($dtmdate)
	{
		array_push($this->commitArray,'dtmdate');
		$this->dtmdate = $dtmdate;
	}
	
	//set dtmCreateDate
	function setdtmCreateDate($dtmCreateDate)
	{
		array_push($this->commitArray,'dtmCreateDate');
		$this->dtmCreateDate = $dtmCreateDate;
	}
	
	//set intCteatedBy
	function setintCteatedBy($intCteatedBy)
	{
		array_push($this->commitArray,'intCteatedBy');
		$this->intCteatedBy = $intCteatedBy;
	}
	
	//set dtmModifiedDate
	function setdtmModifiedDate($dtmModifiedDate)
	{
		array_push($this->commitArray,'dtmModifiedDate');
		$this->dtmModifiedDate = $dtmModifiedDate;
	}
	
	//set intModifiedBy
	function setintModifiedBy($intModifiedBy)
	{
		array_push($this->commitArray,'intModifiedBy');
		$this->intModifiedBy = $intModifiedBy;
	}
	
	//set intCompanyId
	function setintCompanyId($intCompanyId)
	{
		array_push($this->commitArray,'intCompanyId');
		$this->intCompanyId = $intCompanyId;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->intFabricGPTransfInNo=='' || $this->intFabricGPTransfInYear=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
		}
	}
	
	public function set($intFabricGPTransfInNo , $intFabricGPTransfInYear)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "intFabricGPTransfInNo='$intFabricGPTransfInNo' and intFabricGPTransfInYear='$intFabricGPTransfInYear'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			$this->setVariables($result);
		else
			throw new Exception("Record not found.");
	}	
	
	
	//END }
}
?>
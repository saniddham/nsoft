<?php
class menus{
 
	private $db;
	private $table= "menus";
	
	//private property
	private $intId;
	private $strCode;
	private $intParentId;
	private $strName;
	private $strURL;
	private $JAVASCRIPT_URL;
	private $DB_URL;
	private $intStatus;
	private $intOrderBy;
	private $intView;
	private $intAdd;
	private $intEdit;
	private $intDelete;
	private $int1Approval;
	private $int2Approval;
	private $int3Approval;
	private $int4Approval;
	private $int5Approval;
	private $intSendToApproval;
	private $intCancel;
	private $intReject;
	private $intRevise;
	private $intExportToExcel;
	private $intExportToPDF;
	private $intWithoutPermision;
	private $CASH;
	private $NO_JS;
	private $HIDE_HEADER;
	private $MENU_TYPE;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('intId'=>'intId',
										'strCode'=>'strCode',
										'intParentId'=>'intParentId',
										'strName'=>'strName',
										'strURL'=>'strURL',
										'JAVASCRIPT_URL'=>'JAVASCRIPT_URL',
										'DB_URL'=>'DB_URL',
										'intStatus'=>'intStatus',
										'intOrderBy'=>'intOrderBy',
										'intView'=>'intView',
										'intAdd'=>'intAdd',
										'intEdit'=>'intEdit',
										'intDelete'=>'intDelete',
										'int1Approval'=>'int1Approval',
										'int2Approval'=>'int2Approval',
										'int3Approval'=>'int3Approval',
										'int4Approval'=>'int4Approval',
										'int5Approval'=>'int5Approval',
										'intSendToApproval'=>'intSendToApproval',
										'intCancel'=>'intCancel',
										'intReject'=>'intReject',
										'intRevise'=>'intRevise',
										'intExportToExcel'=>'intExportToExcel',
										'intExportToPDF'=>'intExportToPDF',
										'intWithoutPermision'=>'intWithoutPermision',
										'CASH'=>'CASH',
										'NO_JS'=>'NO_JS',
										'HIDE_HEADER'=>'HIDE_HEADER',
										'MENU_TYPE'=>'MENU_TYPE',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	function commit()
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "intId = ".$this->intId."" ;
		unset($this->commitArray);
		
		if($this->intId==NULL || $this->intId=='' || $this->intId=='NULL')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun intId
	function getintId()
	{
		$this->validate();
		return $this->intId;
	}
	
	//retun strCode
	function getstrCode()
	{
		$this->validate();
		return $this->strCode;
	}
	
	//retun intParentId
	function getintParentId()
	{
		$this->validate();
		return $this->intParentId;
	}
	
	//retun strName
	function getstrName()
	{
		$this->validate();
		return $this->strName;
	}
	
	//retun strURL
	function getstrURL()
	{
		$this->validate();
		return $this->strURL;
	}
	
	//retun JAVASCRIPT_URL
	function getJAVASCRIPT_URL()
	{
		$this->validate();
		return $this->JAVASCRIPT_URL;
	}
	
	//retun DB_URL
	function getDB_URL()
	{
		$this->validate();
		return $this->DB_URL;
	}
	
	//retun intStatus
	function getintStatus()
	{
		$this->validate();
		return $this->intStatus;
	}
	
	//retun intOrderBy
	function getintOrderBy()
	{
		$this->validate();
		return $this->intOrderBy;
	}
	
	//retun intView
	function getintView()
	{
		$this->validate();
		return $this->intView;
	}
	
	//retun intAdd
	function getintAdd()
	{
		$this->validate();
		return $this->intAdd;
	}
	
	//retun intEdit
	function getintEdit()
	{
		$this->validate();
		return $this->intEdit;
	}
	
	//retun intDelete
	function getintDelete()
	{
		$this->validate();
		return $this->intDelete;
	}
	
	//retun int1Approval
	function getint1Approval()
	{
		$this->validate();
		return $this->int1Approval;
	}
	
	//retun int2Approval
	function getint2Approval()
	{
		$this->validate();
		return $this->int2Approval;
	}
	
	//retun int3Approval
	function getint3Approval()
	{
		$this->validate();
		return $this->int3Approval;
	}
	
	//retun int4Approval
	function getint4Approval()
	{
		$this->validate();
		return $this->int4Approval;
	}
	
	//retun int5Approval
	function getint5Approval()
	{
		$this->validate();
		return $this->int5Approval;
	}
	
	//retun intSendToApproval
	function getintSendToApproval()
	{
		$this->validate();
		return $this->intSendToApproval;
	}
	
	//retun intCancel
	function getintCancel()
	{
		$this->validate();
		return $this->intCancel;
	}
	
	//retun intReject
	function getintReject()
	{
		$this->validate();
		return $this->intReject;
	}
	
	//retun intRevise
	function getintRevise()
	{
		$this->validate();
		return $this->intRevise;
	}
	
	//retun intExportToExcel
	function getintExportToExcel()
	{
		$this->validate();
		return $this->intExportToExcel;
	}
	
	//retun intExportToPDF
	function getintExportToPDF()
	{
		$this->validate();
		return $this->intExportToPDF;
	}
	
	//retun intWithoutPermision
	function getintWithoutPermision()
	{
		$this->validate();
		return $this->intWithoutPermision;
	}
	
	//retun CASH
	function getCASH()
	{
		$this->validate();
		return $this->CASH;
	}
	
	//retun NO_JS
	function getNO_JS()
	{
		$this->validate();
		return $this->NO_JS;
	}
	
	//retun HIDE_HEADER
	function getHIDE_HEADER()
	{
		$this->validate();
		return $this->HIDE_HEADER;
	}
	
	//retun MENU_TYPE
	function getMENU_TYPE()
	{
		$this->validate();
		return $this->MENU_TYPE;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set intId
	function setintId($intId)
	{
		array_push($this->commitArray,'intId');
		$this->intId = $intId;
	}
	
	//set strCode
	function setstrCode($strCode)
	{
		array_push($this->commitArray,'strCode');
		$this->strCode = $strCode;
	}
	
	//set intParentId
	function setintParentId($intParentId)
	{
		array_push($this->commitArray,'intParentId');
		$this->intParentId = $intParentId;
	}
	
	//set strName
	function setstrName($strName)
	{
		array_push($this->commitArray,'strName');
		$this->strName = $strName;
	}
	
	//set strURL
	function setstrURL($strURL)
	{
		array_push($this->commitArray,'strURL');
		$this->strURL = $strURL;
	}
	
	//set JAVASCRIPT_URL
	function setJAVASCRIPT_URL($JAVASCRIPT_URL)
	{
		array_push($this->commitArray,'JAVASCRIPT_URL');
		$this->JAVASCRIPT_URL = $JAVASCRIPT_URL;
	}
	
	//set DB_URL
	function setDB_URL($DB_URL)
	{
		array_push($this->commitArray,'DB_URL');
		$this->DB_URL = $DB_URL;
	}
	
	//set intStatus
	function setintStatus($intStatus)
	{
		array_push($this->commitArray,'intStatus');
		$this->intStatus = $intStatus;
	}
	
	//set intOrderBy
	function setintOrderBy($intOrderBy)
	{
		array_push($this->commitArray,'intOrderBy');
		$this->intOrderBy = $intOrderBy;
	}
	
	//set intView
	function setintView($intView)
	{
		array_push($this->commitArray,'intView');
		$this->intView = $intView;
	}
	
	//set intAdd
	function setintAdd($intAdd)
	{
		array_push($this->commitArray,'intAdd');
		$this->intAdd = $intAdd;
	}
	
	//set intEdit
	function setintEdit($intEdit)
	{
		array_push($this->commitArray,'intEdit');
		$this->intEdit = $intEdit;
	}
	
	//set intDelete
	function setintDelete($intDelete)
	{
		array_push($this->commitArray,'intDelete');
		$this->intDelete = $intDelete;
	}
	
	//set int1Approval
	function setint1Approval($int1Approval)
	{
		array_push($this->commitArray,'int1Approval');
		$this->int1Approval = $int1Approval;
	}
	
	//set int2Approval
	function setint2Approval($int2Approval)
	{
		array_push($this->commitArray,'int2Approval');
		$this->int2Approval = $int2Approval;
	}
	
	//set int3Approval
	function setint3Approval($int3Approval)
	{
		array_push($this->commitArray,'int3Approval');
		$this->int3Approval = $int3Approval;
	}
	
	//set int4Approval
	function setint4Approval($int4Approval)
	{
		array_push($this->commitArray,'int4Approval');
		$this->int4Approval = $int4Approval;
	}
	
	//set int5Approval
	function setint5Approval($int5Approval)
	{
		array_push($this->commitArray,'int5Approval');
		$this->int5Approval = $int5Approval;
	}
	
	//set intSendToApproval
	function setintSendToApproval($intSendToApproval)
	{
		array_push($this->commitArray,'intSendToApproval');
		$this->intSendToApproval = $intSendToApproval;
	}
	
	//set intCancel
	function setintCancel($intCancel)
	{
		array_push($this->commitArray,'intCancel');
		$this->intCancel = $intCancel;
	}
	
	//set intReject
	function setintReject($intReject)
	{
		array_push($this->commitArray,'intReject');
		$this->intReject = $intReject;
	}
	
	//set intRevise
	function setintRevise($intRevise)
	{
		array_push($this->commitArray,'intRevise');
		$this->intRevise = $intRevise;
	}
	
	//set intExportToExcel
	function setintExportToExcel($intExportToExcel)
	{
		array_push($this->commitArray,'intExportToExcel');
		$this->intExportToExcel = $intExportToExcel;
	}
	
	//set intExportToPDF
	function setintExportToPDF($intExportToPDF)
	{
		array_push($this->commitArray,'intExportToPDF');
		$this->intExportToPDF = $intExportToPDF;
	}
	
	//set intWithoutPermision
	function setintWithoutPermision($intWithoutPermision)
	{
		array_push($this->commitArray,'intWithoutPermision');
		$this->intWithoutPermision = $intWithoutPermision;
	}
	
	//set CASH
	function setCASH($CASH)
	{
		array_push($this->commitArray,'CASH');
		$this->CASH = $CASH;
	}
	
	//set NO_JS
	function setNO_JS($NO_JS)
	{
		array_push($this->commitArray,'NO_JS');
		$this->NO_JS = $NO_JS;
	}
	
	//set HIDE_HEADER
	function setHIDE_HEADER($HIDE_HEADER)
	{
		array_push($this->commitArray,'HIDE_HEADER');
		$this->HIDE_HEADER = $HIDE_HEADER;
	}
	
	//set MENU_TYPE
	function setMENU_TYPE($MENU_TYPE)
	{
		array_push($this->commitArray,'MENU_TYPE');
		$this->MENU_TYPE = $MENU_TYPE;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->intId=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($intId)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "intId='$intId'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
		//else
			//throw new Exception("Record not found.");
	}	
	public function getMainMenuIdList()
	{
		$cols	= " intId,strName ";
		
		$where	= " intParentId = 0 AND
					intStatus = 1 ";
		
		$result = $this->select($cols,$join=null,$where);
		
		return $result;
	}
	
	//END }
}
?>
<?php
class finance_customer_invoice_balance{
 
	private $db;
	private $table= "finance_customer_invoice_balance";
	
	//private property
	private $ORDER_YEAR;
	private $ORDER_NO;
	private $SALES_ORDER_ID;
	private $DISPATCHED_GOOD_QTY;
	private $CUSTOMER_AGREED_QTY;
	private $PRODUCTION_DAMAGE_QTY;
	private $FABRIC_DAMAGE_QTY;
	private $FABRIC_CUT_RET_QTY;
	private $SAMPLE_QTY;
	private $MISSING_PANAL;
	private $OTHER;
	private $TOTAL_DAMAGE;
	private $INVOICED_QTY;
	private $INVOICE_BALANCE;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('ORDER_YEAR'=>'ORDER_YEAR',
										'ORDER_NO'=>'ORDER_NO',
										'SALES_ORDER_ID'=>'SALES_ORDER_ID',
										'DISPATCHED_GOOD_QTY'=>'DISPATCHED_GOOD_QTY',
										'CUSTOMER_AGREED_QTY'=>'CUSTOMER_AGREED_QTY',
										'PRODUCTION_DAMAGE_QTY'=>'PRODUCTION_DAMAGE_QTY',
										'FABRIC_DAMAGE_QTY'=>'FABRIC_DAMAGE_QTY',
										'FABRIC_CUT_RET_QTY'=>'FABRIC_CUT_RET_QTY',
										'SAMPLE_QTY'=>'SAMPLE_QTY',
										'MISSING_PANAL'=>'MISSING_PANAL',
										'OTHER'=>'OTHER',
										'TOTAL_DAMAGE'=>'TOTAL_DAMAGE',
										'INVOICED_QTY'=>'INVOICED_QTY',
										'INVOICE_BALANCE'=>'INVOICE_BALANCE',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "ORDER_YEAR = ".$this->ORDER_YEAR." and ORDER_NO = ".$this->ORDER_NO." and SALES_ORDER_ID = ".$this->SALES_ORDER_ID."" ;
		//unset($this->commitArray);
		$commitArray   = array();
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun ORDER_YEAR
	function getORDER_YEAR()
	{
		$this->validate();
		return $this->ORDER_YEAR;
	}
	
	//retun ORDER_NO
	function getORDER_NO()
	{
		$this->validate();
		return $this->ORDER_NO;
	}
	
	//retun SALES_ORDER_ID
	function getSALES_ORDER_ID()
	{
		$this->validate();
		return $this->SALES_ORDER_ID;
	}
	
	//retun DISPATCHED_GOOD_QTY
	function getDISPATCHED_GOOD_QTY()
	{
		$this->validate();
		return $this->DISPATCHED_GOOD_QTY;
	}
	
	//retun CUSTOMER_AGREED_QTY
	function getCUSTOMER_AGREED_QTY()
	{
		$this->validate();
		return $this->CUSTOMER_AGREED_QTY;
	}
	
	//retun PRODUCTION_DAMAGE_QTY
	function getPRODUCTION_DAMAGE_QTY()
	{
		$this->validate();
		return $this->PRODUCTION_DAMAGE_QTY;
	}
	
	//retun FABRIC_DAMAGE_QTY
	function getFABRIC_DAMAGE_QTY()
	{
		$this->validate();
		return $this->FABRIC_DAMAGE_QTY;
	}
	
	//retun FABRIC_CUT_RET_QTY
	function getFABRIC_CUT_RET_QTY()
	{
		$this->validate();
		return $this->FABRIC_CUT_RET_QTY;
	}
	
	//retun SAMPLE_QTY
	function getSAMPLE_QTY()
	{
		$this->validate();
		return $this->SAMPLE_QTY;
	}
	
	//retun MISSING_PANAL
	function getMISSING_PANAL()
	{
		$this->validate();
		return $this->MISSING_PANAL;
	}
	
	//retun OTHER
	function getOTHER()
	{
		$this->validate();
		return $this->OTHER;
	}
	
	//retun TOTAL_DAMAGE
	function getTOTAL_DAMAGE()
	{
		$this->validate();
		return $this->TOTAL_DAMAGE;
	}
	
	//retun INVOICED_QTY
	function getINVOICED_QTY()
	{
		$this->validate();
		return $this->INVOICED_QTY;
	}
	
	//retun INVOICE_BALANCE
	function getINVOICE_BALANCE()
	{
		$this->validate();
		return $this->INVOICE_BALANCE;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set ORDER_YEAR
	function setORDER_YEAR($ORDER_YEAR)
	{
		array_push($this->commitArray,'ORDER_YEAR');
		$this->ORDER_YEAR = $ORDER_YEAR;
	}
	
	//set ORDER_NO
	function setORDER_NO($ORDER_NO)
	{
		array_push($this->commitArray,'ORDER_NO');
		$this->ORDER_NO = $ORDER_NO;
	}
	
	//set SALES_ORDER_ID
	function setSALES_ORDER_ID($SALES_ORDER_ID)
	{
		array_push($this->commitArray,'SALES_ORDER_ID');
		$this->SALES_ORDER_ID = $SALES_ORDER_ID;
	}
	
	//set DISPATCHED_GOOD_QTY
	function setDISPATCHED_GOOD_QTY($DISPATCHED_GOOD_QTY)
	{
		array_push($this->commitArray,'DISPATCHED_GOOD_QTY');
		$this->DISPATCHED_GOOD_QTY = $DISPATCHED_GOOD_QTY;
	}
	
	//set CUSTOMER_AGREED_QTY
	function setCUSTOMER_AGREED_QTY($CUSTOMER_AGREED_QTY)
	{
		array_push($this->commitArray,'CUSTOMER_AGREED_QTY');
		$this->CUSTOMER_AGREED_QTY = $CUSTOMER_AGREED_QTY;
	}
	
	//set PRODUCTION_DAMAGE_QTY
	function setPRODUCTION_DAMAGE_QTY($PRODUCTION_DAMAGE_QTY)
	{
		array_push($this->commitArray,'PRODUCTION_DAMAGE_QTY');
		$this->PRODUCTION_DAMAGE_QTY = $PRODUCTION_DAMAGE_QTY;
	}
	
	//set FABRIC_DAMAGE_QTY
	function setFABRIC_DAMAGE_QTY($FABRIC_DAMAGE_QTY)
	{
		array_push($this->commitArray,'FABRIC_DAMAGE_QTY');
		$this->FABRIC_DAMAGE_QTY = $FABRIC_DAMAGE_QTY;
	}
	
	//set FABRIC_CUT_RET_QTY
	function setFABRIC_CUT_RET_QTY($FABRIC_CUT_RET_QTY)
	{
		array_push($this->commitArray,'FABRIC_CUT_RET_QTY');
		$this->FABRIC_CUT_RET_QTY = $FABRIC_CUT_RET_QTY;
	}
	
	//set SAMPLE_QTY
	function setSAMPLE_QTY($SAMPLE_QTY)
	{
		array_push($this->commitArray,'SAMPLE_QTY');
		$this->SAMPLE_QTY = $SAMPLE_QTY;
	}
	
	//set MISSING_PANAL
	function setMISSING_PANAL($MISSING_PANAL)
	{
		array_push($this->commitArray,'MISSING_PANAL');
		$this->MISSING_PANAL = $MISSING_PANAL;
	}
	
	//set OTHER
	function setOTHER($OTHER)
	{
		array_push($this->commitArray,'OTHER');
		$this->OTHER = $OTHER;
	}
	
	//set TOTAL_DAMAGE
	function setTOTAL_DAMAGE($TOTAL_DAMAGE)
	{
		array_push($this->commitArray,'TOTAL_DAMAGE');
		$this->TOTAL_DAMAGE = $TOTAL_DAMAGE;
	}
	
	//set INVOICED_QTY
	function setINVOICED_QTY($INVOICED_QTY)
	{
		array_push($this->commitArray,'INVOICED_QTY');
		$this->INVOICED_QTY = $INVOICED_QTY;
	}
	
	//set INVOICE_BALANCE
	function setINVOICE_BALANCE($INVOICE_BALANCE)
	{
		array_push($this->commitArray,'INVOICE_BALANCE');
		$this->INVOICE_BALANCE = $INVOICE_BALANCE;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->ORDER_YEAR=='' || $this->ORDER_NO=='' || $this->SALES_ORDER_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($ORDER_YEAR , $ORDER_NO , $SALES_ORDER_ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "ORDER_YEAR='$ORDER_YEAR' and ORDER_NO='$ORDER_NO' and SALES_ORDER_ID='$SALES_ORDER_ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($ORDER_YEAR,$ORDER_NO,$SALES_ORDER_ID,$DISPATCHED_GOOD_QTY,$CUSTOMER_AGREED_QTY,$PRODUCTION_DAMAGE_QTY,$FABRIC_DAMAGE_QTY,$FABRIC_CUT_RET_QTY,$SAMPLE_QTY,$MISSING_PANAL,$OTHER,$TOTAL_DAMAGE,$INVOICED_QTY,$INVOICE_BALANCE){
		$data = array('ORDER_YEAR'=>$ORDER_YEAR 
				,'ORDER_NO'=>$ORDER_NO 
				,'SALES_ORDER_ID'=>$SALES_ORDER_ID 
				,'DISPATCHED_GOOD_QTY'=>$DISPATCHED_GOOD_QTY 
				,'CUSTOMER_AGREED_QTY'=>$CUSTOMER_AGREED_QTY 
				,'PRODUCTION_DAMAGE_QTY'=>$PRODUCTION_DAMAGE_QTY 
				,'FABRIC_DAMAGE_QTY'=>$FABRIC_DAMAGE_QTY 
				,'FABRIC_CUT_RET_QTY'=>$FABRIC_CUT_RET_QTY 
				,'SAMPLE_QTY'=>$SAMPLE_QTY 
				,'MISSING_PANAL'=>$MISSING_PANAL 
				,'OTHER'=>$OTHER 
				,'TOTAL_DAMAGE'=>$TOTAL_DAMAGE 
				,'INVOICED_QTY'=>$INVOICED_QTY 
				,'INVOICE_BALANCE'=>$INVOICE_BALANCE 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('ORDER_YEAR,ORDER_NO',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['ORDER_YEAR'])
				$html .= '<option selected="selected" value="'.$row['ORDER_YEAR'].'">'.$row['ORDER_NO'].'</option>';	
			else
				$html .= '<option value="'.$row['ORDER_YEAR'].'">'.$row['ORDER_NO'].'</option>';	
		}
		return $html;
	}
	//FUNCTION
	public function getPDamageQty($orderNo,$orderYear,$salesOrderId)
	{
		$cols		= " IFNULL(finance_customer_invoice_balance.PRODUCTION_DAMAGE_QTY,0) AS FIN_P_DAMAGE,
						IFNULL(finance_customer_invoice_balance.FABRIC_DAMAGE_QTY,0) AS FIN_F_DAMAGE,
						IFNULL(finance_customer_invoice_balance.FABRIC_CUT_RET_QTY,0) AS FIN_CUT_RETURN,
						IFNULL(finance_customer_invoice_balance.DISPATCHED_GOOD_QTY,0) AS FIN_GOOD_QTY,
						IFNULL(finance_customer_invoice_balance.INVOICE_BALANCE,0) AS INVOICE_BALANCE,
						IFNULL(finance_customer_invoice_balance.INVOICED_QTY,0) AS INVOICED_QTY,
						IFNULL(SUM(ware_fabricdispatchconfirmation_details.P_DAMMAGE_QTY),0) AS TOT_PROD_DAMAGE,
						IFNULL(SUM(ware_fabricdispatchconfirmation_details.F_DAMMAGE_QTY),0) AS TOT_FABRIC_DAMAGE,
						IFNULL(SUM(ware_fabricdispatchconfirmation_details.CUT_RETURN_QTY),0) AS TOT_CUT_RETURN,
						IFNULL(SUM(ware_fabricdispatchconfirmation_details.GOOD_QTY),0) AS TOT_GOOD_QTY";
		
		$join		= " INNER JOIN ware_fabricdispatchconfirmation_header ON 
						finance_customer_invoice_balance.ORDER_YEAR = ware_fabricdispatchconfirmation_header.ORDER_YEAR 
						AND finance_customer_invoice_balance.ORDER_NO = ware_fabricdispatchconfirmation_header.ORDER_NO
						INNER JOIN ware_fabricdispatchconfirmation_details ON 
						ware_fabricdispatchconfirmation_header.DISP_CONFIRM_NO = ware_fabricdispatchconfirmation_details.DISP_CONFIRM_NO 
						AND ware_fabricdispatchconfirmation_header.DISP_CONFIRM_YEAR = ware_fabricdispatchconfirmation_details.DISP_CONFIRM_YEAR 
						AND finance_customer_invoice_balance.SALES_ORDER_ID = ware_fabricdispatchconfirmation_details.SALES_ORDER_ID";
		
		$where		= " finance_customer_invoice_balance.ORDER_YEAR = '$orderYear' AND
						finance_customer_invoice_balance.ORDER_NO = '$orderNo' AND
						finance_customer_invoice_balance.SALES_ORDER_ID = '$salesOrderId'";
					
		$result		= $this->select($cols,$join,$where,$order=null,$limit=null);
		return $result;
	
	}
	
	//END }
}
?>
<?php
class sys_password_history{

	private $db;
	private $table= "sys_password_history";

	//private property
	private $ID;
	private $USER_ID;
	private $PASSWORD;
	private $CREATED_DATE;
	private $commitArray = array();
	private $field_array = array();
	

	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('ID'=>'ID',
										'USER_ID'=>'USER_ID',
										'PASSWORD'=>'PASSWORD',
										'CREATED_DATE'=>'CREATED_DATE',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit );
		return $result;
	}

	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data);
		return $this->db->getResult();


	}

	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}

	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();
	}

	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}

	public function get_field_array(){
		return $this->field_array;
	}

	function commit($type='update')
	{
		//commit update
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{
			$data[$v] = $this->$v;
		}
		$where		= "ID = ".$this->ID."" ;
		$this->commitArray = array();

		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}

	//BEGIN - public functions for private properties {
	
	//retun ID
	function getID()
	{
		$this->validate();
		return $this->ID;
	}
	
	//retun USER_ID
	function getUSER_ID()
	{
		$this->validate();
		return $this->USER_ID;
	}
	
	//retun PASSWORD
	function getPASSWORD()
	{
		$this->validate();
		return $this->PASSWORD;
	}
	
	//retun CREATED_DATE
	function getCREATED_DATE()
	{
		$this->validate();
		return $this->CREATED_DATE;
	}
	
	//END }

	//BEGIN - public function set {
	
	//set ID
	function setID($ID)
	{
		array_push($this->commitArray,'ID');
		$this->ID = $ID;
	}
	
	//set USER_ID
	function setUSER_ID($USER_ID)
	{
		array_push($this->commitArray,'USER_ID');
		$this->USER_ID = $USER_ID;
	}
	
	//set PASSWORD
	function setPASSWORD($PASSWORD)
	{
		array_push($this->commitArray,'PASSWORD');
		$this->PASSWORD = $PASSWORD;
	}
	
	//set CREATED_DATE
	function setCREATED_DATE($CREATED_DATE)
	{
		array_push($this->commitArray,'CREATED_DATE');
		$this->CREATED_DATE = $CREATED_DATE;
	}
	
	//END }

	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}

	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}

	public function set($ID)
	{
		$cols	= "*";

		$join	= NULL;

		$where	= "ID='$ID'";

		$result = $this->select($cols,$join,$where);
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}

	//insert as parameters
	public function insertRec($USER_ID,$PASSWORD,$CREATED_DATE){
		$data = array('USER_ID'=>$USER_ID 
				,'PASSWORD'=>$PASSWORD 
				,'CREATED_DATE'=>$CREATED_DATE 
				);
		return $this->insert($data);
	}

	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('ID,USER_ID',  null, $where = $where);
		$html = '<option value=""></option>';
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['ID'])
				$html .= '<option selected="selected" value="'.$row['ID'].'">'.$row['USER_ID'].'</option>';
			else
				$html .= '<option value="'.$row['ID'].'">'.$row['USER_ID'].'</option>';
		}
		return $html;
	}
	
	//END }
}
?>
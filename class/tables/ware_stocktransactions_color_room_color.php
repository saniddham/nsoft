<?php
//Manually Functions Added 

class ware_stocktransactions_color_room_color{
 
	private $db;
	private $table= "ware_stocktransactions_color_room_color";
	
	//private property
	private $id;
	private $intSavedOrder;
	private $intCompanyId;
	private $intLocationId;
	private $intSubStores;
	private $intDocumentNo;
	private $intDocumntYear;
	private $intColorId;
	private $intTechnique;
	private $intInkType;
	private $dblQty;
	private $strType;
	private $intOrderNo;
	private $intOrderYear;
	private $intSalesOrderId;
	private $intUser;
	private $dtDate;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('id'=>'id',
										'intSavedOrder'=>'intSavedOrder',
										'intCompanyId'=>'intCompanyId',
										'intLocationId'=>'intLocationId',
										'intSubStores'=>'intSubStores',
										'intDocumentNo'=>'intDocumentNo',
										'intDocumntYear'=>'intDocumntYear',
										'intColorId'=>'intColorId',
										'intTechnique'=>'intTechnique',
										'intInkType'=>'intInkType',
										'dblQty'=>'dblQty',
										'strType'=>'strType',
										'intOrderNo'=>'intOrderNo',
										'intOrderYear'=>'intOrderYear',
										'intSalesOrderId'=>'intSalesOrderId',
										'intUser'=>'intUser',
										'dtDate'=>'dtDate',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	function commit()
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "id = ".$this->id."" ;
		unset($this->commitArray);
		
		if($this->intId==NULL || $this->intId=='' || $this->intId=='NULL')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun id
	function getid()
	{
		$this->validate();
		return $this->id;
	}
	
	//retun intSavedOrder
	function getintSavedOrder()
	{
		$this->validate();
		return $this->intSavedOrder;
	}
	
	//retun intCompanyId
	function getintCompanyId()
	{
		$this->validate();
		return $this->intCompanyId;
	}
	
	//retun intLocationId
	function getintLocationId()
	{
		$this->validate();
		return $this->intLocationId;
	}
	
	//retun intSubStores
	function getintSubStores()
	{
		$this->validate();
		return $this->intSubStores;
	}
	
	//retun intDocumentNo
	function getintDocumentNo()
	{
		$this->validate();
		return $this->intDocumentNo;
	}
	
	//retun intDocumntYear
	function getintDocumntYear()
	{
		$this->validate();
		return $this->intDocumntYear;
	}
	
	//retun intColorId
	function getintColorId()
	{
		$this->validate();
		return $this->intColorId;
	}
	
	//retun intTechnique
	function getintTechnique()
	{
		$this->validate();
		return $this->intTechnique;
	}
	
	//retun intInkType
	function getintInkType()
	{
		$this->validate();
		return $this->intInkType;
	}
	
	//retun dblQty
	function getdblQty()
	{
		$this->validate();
		return $this->dblQty;
	}
	
	//retun strType
	function getstrType()
	{
		$this->validate();
		return $this->strType;
	}
	
	//retun intOrderNo
	function getintOrderNo()
	{
		$this->validate();
		return $this->intOrderNo;
	}
	
	//retun intOrderYear
	function getintOrderYear()
	{
		$this->validate();
		return $this->intOrderYear;
	}
	
	//retun intSalesOrderId
	function getintSalesOrderId()
	{
		$this->validate();
		return $this->intSalesOrderId;
	}
	
	//retun intUser
	function getintUser()
	{
		$this->validate();
		return $this->intUser;
	}
	
	//retun dtDate
	function getdtDate()
	{
		$this->validate();
		return $this->dtDate;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set id
	function setid($id)
	{
		array_push($this->commitArray,'id');
		$this->id = $id;
	}
	
	//set intSavedOrder
	function setintSavedOrder($intSavedOrder)
	{
		array_push($this->commitArray,'intSavedOrder');
		$this->intSavedOrder = $intSavedOrder;
	}
	
	//set intCompanyId
	function setintCompanyId($intCompanyId)
	{
		array_push($this->commitArray,'intCompanyId');
		$this->intCompanyId = $intCompanyId;
	}
	
	//set intLocationId
	function setintLocationId($intLocationId)
	{
		array_push($this->commitArray,'intLocationId');
		$this->intLocationId = $intLocationId;
	}
	
	//set intSubStores
	function setintSubStores($intSubStores)
	{
		array_push($this->commitArray,'intSubStores');
		$this->intSubStores = $intSubStores;
	}
	
	//set intDocumentNo
	function setintDocumentNo($intDocumentNo)
	{
		array_push($this->commitArray,'intDocumentNo');
		$this->intDocumentNo = $intDocumentNo;
	}
	
	//set intDocumntYear
	function setintDocumntYear($intDocumntYear)
	{
		array_push($this->commitArray,'intDocumntYear');
		$this->intDocumntYear = $intDocumntYear;
	}
	
	//set intColorId
	function setintColorId($intColorId)
	{
		array_push($this->commitArray,'intColorId');
		$this->intColorId = $intColorId;
	}
	
	//set intTechnique
	function setintTechnique($intTechnique)
	{
		array_push($this->commitArray,'intTechnique');
		$this->intTechnique = $intTechnique;
	}
	
	//set intInkType
	function setintInkType($intInkType)
	{
		array_push($this->commitArray,'intInkType');
		$this->intInkType = $intInkType;
	}
	
	//set dblQty
	function setdblQty($dblQty)
	{
		array_push($this->commitArray,'dblQty');
		$this->dblQty = $dblQty;
	}
	
	//set strType
	function setstrType($strType)
	{
		array_push($this->commitArray,'strType');
		$this->strType = $strType;
	}
	
	//set intOrderNo
	function setintOrderNo($intOrderNo)
	{
		array_push($this->commitArray,'intOrderNo');
		$this->intOrderNo = $intOrderNo;
	}
	
	//set intOrderYear
	function setintOrderYear($intOrderYear)
	{
		array_push($this->commitArray,'intOrderYear');
		$this->intOrderYear = $intOrderYear;
	}
	
	//set intSalesOrderId
	function setintSalesOrderId($intSalesOrderId)
	{
		array_push($this->commitArray,'intSalesOrderId');
		$this->intSalesOrderId = $intSalesOrderId;
	}
	
	//set intUser
	function setintUser($intUser)
	{
		array_push($this->commitArray,'intUser');
		$this->intUser = $intUser;
	}
	
	//set dtDate
	function setdtDate($dtDate)
	{
		array_push($this->commitArray,'dtDate');
		$this->dtDate = $dtDate;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->id=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($id)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "id='$id'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($intSavedOrder,$intCompanyId,$intLocationId,$intSubStores,$intDocumentNo,$intDocumntYear,$intColorId,$intTechnique,$intInkType,$dblQty,$strType,$intOrderNo,$intOrderYear,$intSalesOrderId,$intUser,$dtDate){
		$data = array('intSavedOrder'=>$intSavedOrder 
				,'intCompanyId'=>$intCompanyId 
				,'intLocationId'=>$intLocationId 
				,'intSubStores'=>$intSubStores 
				,'intDocumentNo'=>$intDocumentNo 
				,'intDocumntYear'=>$intDocumntYear 
				,'intColorId'=>$intColorId 
				,'intTechnique'=>$intTechnique 
				,'intInkType'=>$intInkType 
				,'dblQty'=>$dblQty 
				,'strType'=>$strType 
				,'intOrderNo'=>$intOrderNo 
				,'intOrderYear'=>$intOrderYear 
				,'intSalesOrderId'=>$intSalesOrderId 
				,'intUser'=>$intUser 
				,'dtDate'=>$dtDate 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('id,intDocumentNo',  null, $where = $where);
		while($row=mysqli_fetch_array($result)){
			$html .= '<option value="'.$row['id'].'">'.$row['intDocumentNo'].'</option>';	
		}
		return $html;
	}
	
	
	public function getColorRoomColorBalance($location,$orderNo,$orderYear,$salesOrderId,$color,$tehnique,$inkType,$deci){
		
		$cols		= " COALESCE(ROUND(SUM(dblQty),$deci+5),0) AS color_bal ";
		
		$where		= " 1=1 ";
		
		if($location!='')
			$where	.= " AND intLocationId = '".$location."' ";
	
		if($orderNo!='')
			$where	.= " AND intOrderNo = '".$orderNo."' ";
		
		if($orderYear!='')
			$where	.= " AND intOrderYear = '".$orderYear."' ";
		
		if($salesOrderId!='')
			$where	.= " AND intSalesOrderId = '".$salesOrderId."' ";
			
		if($color!='')
			$where	.= " AND intColorId = '".$color."' ";
			
		if($tehnique!='')
			$where	.= " AND intTechnique = '".$tehnique."' ";
			
		if($inkType!='')
			$where	.= " AND intInkType = '".$inkType."' ";
		
		//echo "select ".$cols." from ware_stocktransactions_color_room_color ".$join." where ".$where;
		$result 	= $this->select($cols,$join = null,$where, $order = null, $limit = null);	
		$row		= mysqli_fetch_array($result);
		return 		$row['color_bal'];
	}
	
	// NEW FUNCTION
	public function getStockAvailableOrderWiseColors($location,$color,$technique,$inkType,$orderNo,$orderYear,$salesOrderID)
	{
		$select		= " ware_stocktransactions_color_room_color.intOrderNo,
						ware_stocktransactions_color_room_color.intOrderYear,
						Sum(ware_stocktransactions_color_room_color.dblQty) AS stockBalance,
						ware_stocktransactions_color_room_color.intSalesOrderId,
						CONCAT(ware_stocktransactions_color_room_color.intOrderNo,'/',ware_stocktransactions_color_room_color.intOrderYear) AS concat_order";
		
		$where		= " ware_stocktransactions_color_room_color.intColorId = '$color' AND
						ware_stocktransactions_color_room_color.intTechnique = '$technique' AND
						ware_stocktransactions_color_room_color.intInkType = '$inkType' AND
						ware_stocktransactions_color_room_color.intLocationId = '$location'";
						if($orderNo != '')
		$where		.=	"AND ware_stocktransactions_color_room_color.intOrderNo = '$orderNo'";
						if($orderYear != '')
		$where		.=	"AND ware_stocktransactions_color_room_color.intOrderYear = '$orderYear'";
						if($salesOrderID != '')
		$where		.=	"AND ware_stocktransactions_color_room_color.intSalesOrderId = '$salesOrderID'";
		$where		.=	"GROUP BY
						ware_stocktransactions_color_room_color.intOrderNo,
						ware_stocktransactions_color_room_color.intOrderYear,
						ware_stocktransactions_color_room_color.intSalesOrderId
						HAVING 
						stockBalance>0";
				
		$result		= $this->select($select,$join = null,$where, $order = null, $limit = null);
		return $result;
			
	}
	//NEW FUNCTION
	public function loadComboWastage($type,$location)
	{
		
		$select		=  "Sum(ware_stocktransactions_color_room_color.dblQty) AS totQty";
		if($type == 'color')
		$select		.=	", ware_stocktransactions_color_room_color.intColorId";
		if($type == 'technique')
		$select		.=	", ware_stocktransactions_color_room_color.intTechnique";
		if($type == 'inkType')
		$select		.=	", ware_stocktransactions_color_room_color.intInkType";
	
		$where		= 	" ware_stocktransactions_color_room_color.intLocationId = '$location' ";
		if($type == 'color')				
		$where		.=	" GROUP BY ware_stocktransactions_color_room_color.intColorId";
		if($type == 'technique')
		$where		.= " GROUP BY ware_stocktransactions_color_room_color.intTechnique";
		if($type == 'inkType')
		$where		.= " GROUP BY ware_stocktransactions_color_room_color.intInkType";
		$where		.= " HAVING
						totQty>0";
		$resulrArr	= $this->select($select,$join = null,$where, $order = null, $limit = null);
		return $resulrArr;
	}
	//END }
	public function getOrderWiseColorWastage($orderNo,$orderYear,$salesOrderId,$colorId,$techId,$inkTypeId)
	{
		$cols		= " ROUND(COALESCE(SUM(dblQty),0),9)	AS WASTAGE";
		
		$where 		= "intOrderNo = '$orderNo'
						AND intOrderYear = '$orderYear'
						AND intSalesOrderId = '$salesOrderId'
						AND intColorId = '$colorId'
						AND intTechnique = '$techId'
						AND intInkType = '$inkTypeId'
						AND strType = 'COLOR_WASTAGE'";
		$result2 	= $this->select($cols,$join=null,$where,$order=null,$limit=null);
		$row2		= mysqli_fetch_array($result2);
		return abs($row2['WASTAGE']);
	}
	
	public function getOrderWiseColorUsed($orderNo,$orderYear,$salesOrderId,$colorId,$techId,$inkTypeId)
	{
		$cols		= " ROUND(COALESCE(SUM(dblQty*-1),0),9)	AS USED";
		
		$where 		= "intOrderNo = '$orderNo'
						AND intOrderYear = '$orderYear'
						AND intSalesOrderId = '$salesOrderId'
						AND intColorId = '$colorId'
						AND intTechnique = '$techId'
						AND intInkType = '$inkTypeId'
						AND strType IN ('ISSUE_COLOR','COLOR_RETURN')";
		$result2 	= $this->select($cols,$join=null,$where,$order=null,$limit=null);
		$row2		= mysqli_fetch_array($result2);
		return abs($row2['USED']);
	}
	public function getOrderWiseColorCreated($orderNo,$orderYear,$salesOrderId,$colorId,$techId,$inkTypeId)
	{
		$cols		= " ROUND(COALESCE(SUM(dblQty*-1),0),9)	AS USED";
		
		$where 		= "intOrderNo = '$orderNo'
						AND intOrderYear = '$orderYear'
						AND intSalesOrderId = '$salesOrderId'
						AND intColorId = '$colorId'
						AND intTechnique = '$techId'
						AND intInkType = '$inkTypeId'
						AND strType IN ('COLOR_CREATE')";
		$result2 	= $this->select($cols,$join=null,$where,$order=null,$limit=null);
		$row2		= mysqli_fetch_array($result2);
		return abs($row2['USED']);
	}
}
?>
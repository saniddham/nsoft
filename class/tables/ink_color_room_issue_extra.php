<?php
//echo MAIN_ROOT;
include_once (MAIN_ROOT."class/tables/ware_stocktransactions_color_room_item.php");		$ware_stocktransactions_color_room_item = new ware_stocktransactions_color_room_item($db);
include_once (MAIN_ROOT."class/tables/ink_color_room_requisition_extra.php");			$ink_color_room_requisition_extra		= new ink_color_room_requisition_extra($db);		
include_once (MAIN_ROOT."class/common/common.php");   									$common										= new cls_common($db);
class ink_color_room_issue_extra{
 
	private $db;
	private $table= "ink_color_room_issue_extra";
	
	//private property
	private $SERIAL_NO;
	private $SERIAL_YEAR;
	private $ITEM_ID;
	private $QTY;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('SERIAL_NO'=>'SERIAL_NO',
										'SERIAL_YEAR'=>'SERIAL_YEAR',
										'ITEM_ID'=>'ITEM_ID',
										'QTY'=>'QTY',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	function commit()
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "SERIAL_NO = ".$this->SERIAL_NO." and SERIAL_YEAR = ".$this->SERIAL_YEAR." and ITEM_ID = ".$this->ITEM_ID."" ;
		unset($this->commitArray);
		
		if($this->intId==NULL || $this->intId=='' || $this->intId=='NULL')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun SERIAL_NO
	function getSERIAL_NO()
	{
		$this->validate();
		return $this->SERIAL_NO;
	}
	
	//retun SERIAL_YEAR
	function getSERIAL_YEAR()
	{
		$this->validate();
		return $this->SERIAL_YEAR;
	}
	
	//retun ITEM_ID
	function getITEM_ID()
	{
		$this->validate();
		return $this->ITEM_ID;
	}
	
	//retun QTY
	function getQTY()
	{
		$this->validate();
		return $this->QTY;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set SERIAL_NO
	function setSERIAL_NO($SERIAL_NO)
	{
		array_push($this->commitArray,'SERIAL_NO');
		$this->SERIAL_NO = $SERIAL_NO;
	}
	
	//set SERIAL_YEAR
	function setSERIAL_YEAR($SERIAL_YEAR)
	{
		array_push($this->commitArray,'SERIAL_YEAR');
		$this->SERIAL_YEAR = $SERIAL_YEAR;
	}
	
	//set ITEM_ID
	function setITEM_ID($ITEM_ID)
	{
		array_push($this->commitArray,'ITEM_ID');
		$this->ITEM_ID = $ITEM_ID;
	}
	
	//set QTY
	function setQTY($QTY)
	{
		array_push($this->commitArray,'QTY');
		$this->QTY = $QTY;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->SERIAL_NO=='' || $this->SERIAL_YEAR=='' || $this->ITEM_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($SERIAL_NO , $SERIAL_YEAR , $ITEM_ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "SERIAL_NO='$SERIAL_NO' and SERIAL_YEAR='$SERIAL_YEAR' and ITEM_ID='$ITEM_ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($SERIAL_NO,$SERIAL_YEAR,$ITEM_ID,$QTY){
		$data = array('SERIAL_NO'=>$SERIAL_NO 
				,'SERIAL_YEAR'=>$SERIAL_YEAR 
				,'ITEM_ID'=>$ITEM_ID 
				,'QTY'=>$QTY 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('SERIAL_NO,SERIAL_YEAR',  null, $where = $where);
		while($row=mysqli_fetch_array($result)){
			$html .= '<option value="'.$row['SERIAL_NO'].'">'.$row['SERIAL_YEAR'].'</option>';	
		}
		return $html;
	}
	
	public function getColorRoomIssuedItemQty($location,$itemId,$crnNo,$crnYear,$deci)
	{
 		$cols		= "ROUND(COALESCE(SUM(ink_color_room_issue_extra.QTY),0),$deci+2)	AS ISSUE_QTY";
		
		$join 		= "INNER JOIN ink_color_room_issue_header ICRIH
						  ON ink_color_room_issue_extra.SERIAL_NO = ICRIH.SERIAL_NO
						  AND ink_color_room_issue_extra.SERIAL_YEAR = ICRIH.SERIAL_YEAR";
						  
		$where		= "ICRIH.STATUS = 1 
					   AND ink_color_room_issue_extra.ITEM_ID = $itemId
					   AND ICRIH.LOCATION_ID = $location";
		if($crnNo != '')
		{			 
			$where	.= " AND ICRIH.REQUISITION_NO = '$crnNo' 
					   AND ICRIH.REQUISITION_YEAR = '$crnYear'";
		}
		
		$result 	= $this->select($cols,$join,$where, $order = null, $limit = null);	
		$row		= mysqli_fetch_array($result);
		return $row['ISSUE_QTY'];
	}
	public function getExtraIssueDetail($itemId,$serialNo,$serialYear)
	{
		$cols		= "	ink_color_room_issue_extra.QTY,
						ink_color_room_issue_extra.ITEM_ID";
		$where		= " ink_color_room_issue_extra.SERIAL_NO = '$serialNo' AND
						ink_color_room_issue_extra.SERIAL_YEAR = '$serialYear' AND
						ink_color_room_issue_extra.ITEM_ID = '$itemId'";

		$result		= $this->select($cols,$join = null,$where, $order = null, $limit = null);
		return $result;
	}
	public function getMaxIssueableExcessQty($crnNo,$crnYear,$itemID,$location,$deci)
	{
		global $ware_stocktransactions_color_room_item;
		global $ink_color_room_requisition_extra; 
		global $common;
	
	//get issued extra qty	
		$issuedExtraQty		= $this->getColorRoomIssuedItemQty($location,$itemID,$crnNo,$crnYear,$deci);
		//$issuedExtraQty		= $common->ceil_deci($issuedExtraQty,$deci);
		//echo $issuedExtraQty;
	//get color room stock excess qty
		$colorRoomExcess	= $ware_stocktransactions_color_room_item->getColorRoomExtraItemBalance($location,$itemID,$deci);
		//$colorRoomExcess	= $common->ceil_deci($colorRoomExcess,$deci);
	//get requested excess qty
		$ink_color_room_requisition_extra->set($crnNo,$crnYear,$itemID);
		$requestedQty		= $ink_color_room_requisition_extra->getQTY();
		//$requestedQty		= $common->ceil_deci($requestedQty,$deci);
	//get max value from issued extra and color room extra	
		$maxVal				= max($issuedExtraQty,$colorRoomExcess); 
	//max issuable extra qty is (requested qty - above max value)	
	if($requestedQty>0)
		$maxIssuableQty		= (float)$requestedQty - (float)$maxVal;
	else
		$maxIssuableQty		= 0;
	//	echo $maxIssuableQty;
	return $maxIssuableQty;
	}
	//END }
}
?>
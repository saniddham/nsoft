<?php
class finance_pettycash_reimbursement_approve_by{

	private $db;
	private $table= "finance_pettycash_reimbursement_approve_by";

	//private property
	private $REIMBURSEMENT_NO;
	private $REIMBURSEMENT_YEAR;
	private $APPROVE_LEVELS;
	private $APPROVED_BY;
	private $APPROVED_DATE;
	private $STATUS;
	private $commitArray = array();
	private $field_array = array();
	

	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('REIMBURSEMENT_NO'=>'REIMBURSEMENT_NO',
										'REIMBURSEMENT_YEAR'=>'REIMBURSEMENT_YEAR',
										'APPROVE_LEVELS'=>'APPROVE_LEVELS',
										'APPROVED_BY'=>'APPROVED_BY',
										'APPROVED_DATE'=>'APPROVED_DATE',
										'STATUS'=>'STATUS',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit );
		return $result;
	}

	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data);
		return $this->db->getResult();


	}

	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}

	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();
	}

	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}

	public function get_field_array(){
		return $this->field_array;
	}

	function commit($type='update')
	{
		//commit update
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{
			$data[$v] = $this->$v;
		}
		$where		= "REIMBURSEMENT_NO = ".$this->REIMBURSEMENT_NO." and REIMBURSEMENT_YEAR = ".$this->REIMBURSEMENT_YEAR." and APPROVE_LEVELS = ".$this->APPROVE_LEVELS." and STATUS = ".$this->STATUS."" ;
		$this->commitArray = array();

		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}

	//BEGIN - public functions for private properties {
	
	//retun REIMBURSEMENT_NO
	function getREIMBURSEMENT_NO()
	{
		$this->validate();
		return $this->REIMBURSEMENT_NO;
	}
	
	//retun REIMBURSEMENT_YEAR
	function getREIMBURSEMENT_YEAR()
	{
		$this->validate();
		return $this->REIMBURSEMENT_YEAR;
	}
	
	//retun APPROVE_LEVELS
	function getAPPROVE_LEVELS()
	{
		$this->validate();
		return $this->APPROVE_LEVELS;
	}
	
	//retun APPROVED_BY
	function getAPPROVED_BY()
	{
		$this->validate();
		return $this->APPROVED_BY;
	}
	
	//retun APPROVED_DATE
	function getAPPROVED_DATE()
	{
		$this->validate();
		return $this->APPROVED_DATE;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//END }

	//BEGIN - public function set {
	
	//set REIMBURSEMENT_NO
	function setREIMBURSEMENT_NO($REIMBURSEMENT_NO)
	{
		array_push($this->commitArray,'REIMBURSEMENT_NO');
		$this->REIMBURSEMENT_NO = $REIMBURSEMENT_NO;
	}
	
	//set REIMBURSEMENT_YEAR
	function setREIMBURSEMENT_YEAR($REIMBURSEMENT_YEAR)
	{
		array_push($this->commitArray,'REIMBURSEMENT_YEAR');
		$this->REIMBURSEMENT_YEAR = $REIMBURSEMENT_YEAR;
	}
	
	//set APPROVE_LEVELS
	function setAPPROVE_LEVELS($APPROVE_LEVELS)
	{
		array_push($this->commitArray,'APPROVE_LEVELS');
		$this->APPROVE_LEVELS = $APPROVE_LEVELS;
	}
	
	//set APPROVED_BY
	function setAPPROVED_BY($APPROVED_BY)
	{
		array_push($this->commitArray,'APPROVED_BY');
		$this->APPROVED_BY = $APPROVED_BY;
	}
	
	//set APPROVED_DATE
	function setAPPROVED_DATE($APPROVED_DATE)
	{
		array_push($this->commitArray,'APPROVED_DATE');
		$this->APPROVED_DATE = $APPROVED_DATE;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//END }

	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->REIMBURSEMENT_NO=='' || $this->REIMBURSEMENT_YEAR=='' || $this->APPROVE_LEVELS=='' || $this->STATUS=='')
			throw new exception("Set primary values first");
		else
			return true;
	}

	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}

	public function set($REIMBURSEMENT_NO , $REIMBURSEMENT_YEAR , $APPROVE_LEVELS , $STATUS)
	{
		$cols	= "*";

		$join	= NULL;

		$where	= "REIMBURSEMENT_NO='$REIMBURSEMENT_NO' and REIMBURSEMENT_YEAR='$REIMBURSEMENT_YEAR' and APPROVE_LEVELS='$APPROVE_LEVELS' and STATUS='$STATUS'";

		$result = $this->select($cols,$join,$where);
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}

	//insert as parameters
	public function insertRec($REIMBURSEMENT_NO,$REIMBURSEMENT_YEAR,$APPROVE_LEVELS,$APPROVED_BY,$APPROVED_DATE,$STATUS){
		$data = array('REIMBURSEMENT_NO'=>$REIMBURSEMENT_NO 
				,'REIMBURSEMENT_YEAR'=>$REIMBURSEMENT_YEAR 
				,'APPROVE_LEVELS'=>$APPROVE_LEVELS 
				,'APPROVED_BY'=>$APPROVED_BY 
				,'APPROVED_DATE'=>$APPROVED_DATE 
				,'STATUS'=>$STATUS 
				);
		return $this->insert($data);
	}

	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('REIMBURSEMENT_NO,REIMBURSEMENT_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['REIMBURSEMENT_NO'])
				$html .= '<option selected="selected" value="'.$row['REIMBURSEMENT_NO'].'">'.$row['REIMBURSEMENT_YEAR'].'</option>';
			else
				$html .= '<option value="'.$row['REIMBURSEMENT_NO'].'">'.$row['REIMBURSEMENT_YEAR'].'</option>';
		}
		return $html;
	}
	
	public function updateMaxStatus($serialNo,$serialYear)
	{
		$select		= " MAX(STATUS) AS MAX_STATUS ";
		$where		= " REIMBURSEMENT_NO = '".$serialNo."' AND
						REIMBURSEMENT_YEAR = '".$serialYear."' ";
		
		$result 	= $this->select($select,NULL,$where, NULL,NULL);	
		$row		= mysqli_fetch_array($result);

		$maxStatus	= $row['MAX_STATUS']+1;
		$data  		= array('STATUS' => $maxStatus);
		
		$where		= ' REIMBURSEMENT_NO = "'.$serialNo.'" AND 
						REIMBURSEMENT_YEAR = "'.$serialYear.'" AND
						STATUS = 0 ' ;
		$result_arr	= $this->update($data,$where);
		if(!$result_arr['status'])
			throw new Exception($result_arr['msg']);	
	}
	public function getApprovedByData($reimbursementNo,$reimbursementYear)
	{
		$cols			= " APPROVED_DATE AS dtApprovedDate,
							SU.strUserName AS UserName,
							APPROVE_LEVELS AS intApproveLevelNo ";
						
		$join 			= " INNER JOIN sys_users SU ON finance_pettycash_reimbursement_approve_by.APPROVED_BY = SU.intUserId ";
		
		$where			= " finance_pettycash_reimbursement_approve_by.REIMBURSEMENT_NO = '$reimbursementNo' AND
							finance_pettycash_reimbursement_approve_by.REIMBURSEMENT_YEAR = '$reimbursementYear' ";	
		
		$order			= " finance_pettycash_reimbursement_approve_by.APPROVED_DATE ASC ";
											
		$resultA		= $this->select($cols,$join,$where,$order,NULL);
		return $resultA;	
	}
	//END }
}
?>
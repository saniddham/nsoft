<?php
class ware_mrnheader{
 
	private $db;
	private $table= "ware_mrnheader";
	
	//private property
	private $intMrnNo;
	private $intMrnYear;
	private $intDepartment;
	private $intStatus;
	private $intApproveLevels;
	private $datdate;
	private $strRemarks;
	private $dtmCreateDate;
	private $intUser;
	private $intCompanyId;
	private $intModifiedBy;
	private $dtmModifiedDate;
	private $int25ExceedingApproved;
	private $int25ExceedingApproveLevels;
	private $int100ExceedingApproved;
	private $int100ExceedingApproveLevels;
	private $mrnType;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('intMrnNo'=>'intMrnNo',
										'intMrnYear'=>'intMrnYear',
										'intDepartment'=>'intDepartment',
										'intStatus'=>'intStatus',
										'intApproveLevels'=>'intApproveLevels',
										'datdate'=>'datdate',
										'strRemarks'=>'strRemarks',
										'dtmCreateDate'=>'dtmCreateDate',
										'intUser'=>'intUser',
										'intCompanyId'=>'intCompanyId',
										'intModifiedBy'=>'intModifiedBy',
										'dtmModifiedDate'=>'dtmModifiedDate',
										'int25ExceedingApproved'=>'int25ExceedingApproved',
										'int25ExceedingApproveLevels'=>'int25ExceedingApproveLevels',
										'int100ExceedingApproved'=>'int100ExceedingApproved',
										'int100ExceedingApproveLevels'=>'int100ExceedingApproveLevels',
										'mrnType'=>'mrnType',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "intMrnNo = ".$this->intMrnNo." and intMrnYear = ".$this->intMrnYear."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun intMrnNo
	function getintMrnNo()
	{
		$this->validate();
		return $this->intMrnNo;
	}
	
	//retun intMrnYear
	function getintMrnYear()
	{
		$this->validate();
		return $this->intMrnYear;
	}
	
	//retun intDepartment
	function getintDepartment()
	{
		$this->validate();
		return $this->intDepartment;
	}
	
	//retun intStatus
	function getintStatus()
	{
		$this->validate();
		return $this->intStatus;
	}
	
	//retun intApproveLevels
	function getintApproveLevels()
	{
		$this->validate();
		return $this->intApproveLevels;
	}
	
	//retun datdate
	function getdatdate()
	{
		$this->validate();
		return $this->datdate;
	}
	
	//retun strRemarks
	function getstrRemarks()
	{
		$this->validate();
		return $this->strRemarks;
	}
	
	//retun dtmCreateDate
	function getdtmCreateDate()
	{
		$this->validate();
		return $this->dtmCreateDate;
	}
	
	//retun intUser
	function getintUser()
	{
		$this->validate();
		return $this->intUser;
	}
	
	//retun intCompanyId
	function getintCompanyId()
	{
		$this->validate();
		return $this->intCompanyId;
	}
	
	//retun intModifiedBy
	function getintModifiedBy()
	{
		$this->validate();
		return $this->intModifiedBy;
	}
	
	//retun dtmModifiedDate
	function getdtmModifiedDate()
	{
		$this->validate();
		return $this->dtmModifiedDate;
	}
	
	//retun int25ExceedingApproved
	function getint25ExceedingApproved()
	{
		$this->validate();
		return $this->int25ExceedingApproved;
	}
	
	//retun int25ExceedingApproveLevels
	function getint25ExceedingApproveLevels()
	{
		$this->validate();
		return $this->int25ExceedingApproveLevels;
	}
	
	//retun int100ExceedingApproved
	function getint100ExceedingApproved()
	{
		$this->validate();
		return $this->int100ExceedingApproved;
	}
	
	//retun int100ExceedingApproveLevels
	function getint100ExceedingApproveLevels()
	{
		$this->validate();
		return $this->int100ExceedingApproveLevels;
	}
	
	//retun mrnType
	function getmrnType()
	{
		$this->validate();
		return $this->mrnType;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set intMrnNo
	function setintMrnNo($intMrnNo)
	{
		array_push($this->commitArray,'intMrnNo');
		$this->intMrnNo = $intMrnNo;
	}
	
	//set intMrnYear
	function setintMrnYear($intMrnYear)
	{
		array_push($this->commitArray,'intMrnYear');
		$this->intMrnYear = $intMrnYear;
	}
	
	//set intDepartment
	function setintDepartment($intDepartment)
	{
		array_push($this->commitArray,'intDepartment');
		$this->intDepartment = $intDepartment;
	}
	
	//set intStatus
	function setintStatus($intStatus)
	{
		array_push($this->commitArray,'intStatus');
		$this->intStatus = $intStatus;
	}
	
	//set intApproveLevels
	function setintApproveLevels($intApproveLevels)
	{
		array_push($this->commitArray,'intApproveLevels');
		$this->intApproveLevels = $intApproveLevels;
	}
	
	//set datdate
	function setdatdate($datdate)
	{
		array_push($this->commitArray,'datdate');
		$this->datdate = $datdate;
	}
	
	//set strRemarks
	function setstrRemarks($strRemarks)
	{
		array_push($this->commitArray,'strRemarks');
		$this->strRemarks = $strRemarks;
	}
	
	//set dtmCreateDate
	function setdtmCreateDate($dtmCreateDate)
	{
		array_push($this->commitArray,'dtmCreateDate');
		$this->dtmCreateDate = $dtmCreateDate;
	}
	
	//set intUser
	function setintUser($intUser)
	{
		array_push($this->commitArray,'intUser');
		$this->intUser = $intUser;
	}
	
	//set intCompanyId
	function setintCompanyId($intCompanyId)
	{
		array_push($this->commitArray,'intCompanyId');
		$this->intCompanyId = $intCompanyId;
	}
	
	//set intModifiedBy
	function setintModifiedBy($intModifiedBy)
	{
		array_push($this->commitArray,'intModifiedBy');
		$this->intModifiedBy = $intModifiedBy;
	}
	
	//set dtmModifiedDate
	function setdtmModifiedDate($dtmModifiedDate)
	{
		array_push($this->commitArray,'dtmModifiedDate');
		$this->dtmModifiedDate = $dtmModifiedDate;
	}
	
	//set int25ExceedingApproved
	function setint25ExceedingApproved($int25ExceedingApproved)
	{
		array_push($this->commitArray,'int25ExceedingApproved');
		$this->int25ExceedingApproved = $int25ExceedingApproved;
	}
	
	//set int25ExceedingApproveLevels
	function setint25ExceedingApproveLevels($int25ExceedingApproveLevels)
	{
		array_push($this->commitArray,'int25ExceedingApproveLevels');
		$this->int25ExceedingApproveLevels = $int25ExceedingApproveLevels;
	}
	
	//set int100ExceedingApproved
	function setint100ExceedingApproved($int100ExceedingApproved)
	{
		array_push($this->commitArray,'int100ExceedingApproved');
		$this->int100ExceedingApproved = $int100ExceedingApproved;
	}
	
	//set int100ExceedingApproveLevels
	function setint100ExceedingApproveLevels($int100ExceedingApproveLevels)
	{
		array_push($this->commitArray,'int100ExceedingApproveLevels');
		$this->int100ExceedingApproveLevels = $int100ExceedingApproveLevels;
	}
	
	//set mrnType
	function setmrnType($mrnType)
	{
		array_push($this->commitArray,'mrnType');
		$this->mrnType = $mrnType;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->intMrnNo=='' || $this->intMrnYear=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($intMrnNo , $intMrnYear)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "intMrnNo='$intMrnNo' and intMrnYear='$intMrnYear'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($intMrnNo,$intMrnYear,$intDepartment,$intStatus,$intApproveLevels,$datdate,$strRemarks,$dtmCreateDate,$intUser,$intCompanyId,$intModifiedBy,$dtmModifiedDate,$int25ExceedingApproved,$int25ExceedingApproveLevels,$int100ExceedingApproved,$int100ExceedingApproveLevels,$mrnType){
		$data = array('intMrnNo'=>$intMrnNo 
				,'intMrnYear'=>$intMrnYear 
				,'intDepartment'=>$intDepartment 
				,'intStatus'=>$intStatus 
				,'intApproveLevels'=>$intApproveLevels 
				,'datdate'=>$datdate 
				,'strRemarks'=>$strRemarks 
				,'dtmCreateDate'=>$dtmCreateDate 
				,'intUser'=>$intUser 
				,'intCompanyId'=>$intCompanyId 
				,'intModifiedBy'=>$intModifiedBy 
				,'dtmModifiedDate'=>$dtmModifiedDate 
				,'int25ExceedingApproved'=>$int25ExceedingApproved 
				,'int25ExceedingApproveLevels'=>$int25ExceedingApproveLevels 
				,'int100ExceedingApproved'=>$int100ExceedingApproved 
				,'int100ExceedingApproveLevels'=>$int100ExceedingApproveLevels 
				,'mrnType'=>$mrnType 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('intMrnNo,intMrnYear',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intMrnNo'])
				$html .= '<option selected="selected" value="'.$row['intMrnNo'].'">'.$row['intMrnYear'].'</option>';	
			else
				$html .= '<option value="'.$row['intMrnNo'].'">'.$row['intMrnYear'].'</option>';	
		}
		return $html;
	}
	
	//END }
}
?>
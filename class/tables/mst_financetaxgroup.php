
<?php
//include "../../libraries/maths/eos.class.php";
include "mst_financetaxisolated.php";
class mst_financetaxgroup{
 
	private $db;
	private $table= "mst_financetaxgroup";
	
	//private property
	private $intId;
	private $strCode;
	private $strDescription;
	private $strProcess;
	private $strFormula;
	private $intStatus;
	private $intCreator;
	private $dtmCreateDate;
	private $intModifyer;
	private $dtmModifyDate;
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->db->field_array_set(array('intId'=>'intId',
										'strCode'=>'strCode',
										'strDescription'=>'strDescription',
										'strProcess'=>'strProcess',
										'strFormula'=>'strFormula',
										'intStatus'=>'intStatus',
										'intCreator'=>'intCreator',
										'dtmCreateDate'=>'dtmCreateDate',
										'intModifyer'=>'intModifyer',
										'dtmModifyDate'=>'dtmModifyDate',
										));
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  
		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	//public functions for private properties
	
	//retun intId
	function getintId()
	{
		$this->validate();
		return $this->intId;
	}
	
	//retun strCode
	function getstrCode()
	{
		$this->validate();
		return $this->strCode;
	}
	
	//retun strDescription
	function getstrDescription()
	{
		$this->validate();
		return $this->strDescription;
	}
	
	//retun strProcess
	function getstrProcess()
	{
		$this->validate();
		return $this->strProcess;
	}
	
	//retun strFormula
	function getstrFormula()
	{
		$this->validate();
		return $this->strFormula;
	}
	
	//retun intStatus
	function getintStatus()
	{
		$this->validate();
		return $this->intStatus;
	}
	
	//retun intCreator
	function getintCreator()
	{
		$this->validate();
		return $this->intCreator;
	}
	
	//retun dtmCreateDate
	function getdtmCreateDate()
	{
		$this->validate();
		return $this->dtmCreateDate;
	}
	
	//retun intModifyer
	function getintModifyer()
	{
		$this->validate();
		return $this->intModifyer;
	}
	
	//retun dtmModifyDate
	function getdtmModifyDate()
	{
		$this->validate();
		return $this->dtmModifyDate;
	}
	
	//validate primary values
	private function validate()
	{
		if($this->intId=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
		}
	}
	
	public function set($intId)
	{
		$result = $this->select('*',null,"intId='$intId'");	
		if($this->db->numRows()>0)
			$this->setVariables($result);
		else
			throw new Exception("Record not found.");
			
	}
	
	public function getTaxAmount($taxGroupId,$amount)
	{
		//$eq = new eqEOS();
		$mst_financetaxisolated  = new mst_financetaxisolated();
		$this->set($taxGroupId);
		$taxFormula = $this->getstrFormula();
		//replace value
		$taxFormula	= str_replace("VALUE",$value,$taxFormula);
		
		//replace tax values 
		//$sql 		= "SELECT strFormulaCode AS CODE,dblRate AS RATE FROM mst_financetaxisolated";
		//$result 	= $this->db->RunQuery($sql);
		$result 	= $mst_financetaxisolated->select("strFormulaCode");
		while($row  = mysqli_fetch_array($result))
		{
			$taxFormula	= preg_replace("/\b".$row["CODE"]."\b/",$row["RATE"],$taxFormula);
		}
		
		// trim white spaces
		$taxFormula = trim($taxFormula);     
		
		// remove any non-numbers chars; exception for math operators
		$taxFormula = preg_match ('[^0-9\+-\*\/\(\) ]', '', $taxFormula);
	
		$compute = create_function("", "return (" . $taxFormula . ");" );
		return 0 + $compute();
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('intId,strCode',  null, $where = $where,'strCode');
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intId'])
				$html .= '<option selected="selected" value="'.$row['intId'].'">'.$row['strCode'].'</option>';	
			else
				$html .= '<option value="'.$row['intId'].'">'.$row['strCode'].'</option>';	
		}
		return $html;
	}
}
?>

<?php
class trn_sales_order_open_details{
 
	private $db;
	private $table= "trn_sales_order_open_details";
	
	//private property
	private $OPEN_NO;
	private $OPEN_YEAR;
	private $ORDER_NO;
	private $ORDER_YEAR;
	private $SALE_ORDER_ID;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('OPEN_NO'=>'OPEN_NO',
										'OPEN_YEAR'=>'OPEN_YEAR',
										'ORDER_NO'=>'ORDER_NO',
										'ORDER_YEAR'=>'ORDER_YEAR',
										'SALE_ORDER_ID'=>'SALE_ORDER_ID',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "OPEN_NO = ".$this->OPEN_NO." and OPEN_YEAR = ".$this->OPEN_YEAR." and ORDER_NO = ".$this->ORDER_NO." and ORDER_YEAR = ".$this->ORDER_YEAR." and SALE_ORDER_ID = ".$this->SALE_ORDER_ID."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun OPEN_NO
	function getOPEN_NO()
	{
		$this->validate();
		return $this->OPEN_NO;
	}
	
	//retun OPEN_YEAR
	function getOPEN_YEAR()
	{
		$this->validate();
		return $this->OPEN_YEAR;
	}
	
	//retun ORDER_NO
	function getORDER_NO()
	{
		$this->validate();
		return $this->ORDER_NO;
	}
	
	//retun ORDER_YEAR
	function getORDER_YEAR()
	{
		$this->validate();
		return $this->ORDER_YEAR;
	}
	
	//retun SALE_ORDER_ID
	function getSALE_ORDER_ID()
	{
		$this->validate();
		return $this->SALE_ORDER_ID;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set OPEN_NO
	function setOPEN_NO($OPEN_NO)
	{
		array_push($this->commitArray,'OPEN_NO');
		$this->OPEN_NO = $OPEN_NO;
	}
	
	//set OPEN_YEAR
	function setOPEN_YEAR($OPEN_YEAR)
	{
		array_push($this->commitArray,'OPEN_YEAR');
		$this->OPEN_YEAR = $OPEN_YEAR;
	}
	
	//set ORDER_NO
	function setORDER_NO($ORDER_NO)
	{
		array_push($this->commitArray,'ORDER_NO');
		$this->ORDER_NO = $ORDER_NO;
	}
	
	//set ORDER_YEAR
	function setORDER_YEAR($ORDER_YEAR)
	{
		array_push($this->commitArray,'ORDER_YEAR');
		$this->ORDER_YEAR = $ORDER_YEAR;
	}
	
	//set SALE_ORDER_ID
	function setSALE_ORDER_ID($SALE_ORDER_ID)
	{
		array_push($this->commitArray,'SALE_ORDER_ID');
		$this->SALE_ORDER_ID = $SALE_ORDER_ID;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->OPEN_NO=='' || $this->OPEN_YEAR=='' || $this->ORDER_NO=='' || $this->ORDER_YEAR=='' || $this->SALE_ORDER_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($OPEN_NO , $OPEN_YEAR , $ORDER_NO , $ORDER_YEAR , $SALE_ORDER_ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "OPEN_NO='$OPEN_NO' and OPEN_YEAR='$OPEN_YEAR' and ORDER_NO='$ORDER_NO' and ORDER_YEAR='$ORDER_YEAR' and SALE_ORDER_ID='$SALE_ORDER_ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($OPEN_NO,$OPEN_YEAR,$ORDER_NO,$ORDER_YEAR,$SALE_ORDER_ID){
		$data = array('OPEN_NO'=>$OPEN_NO 
				,'OPEN_YEAR'=>$OPEN_YEAR 
				,'ORDER_NO'=>$ORDER_NO 
				,'ORDER_YEAR'=>$ORDER_YEAR 
				,'SALE_ORDER_ID'=>$SALE_ORDER_ID 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('OPEN_NO,OPEN_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['OPEN_NO'])
				$html .= '<option selected="selected" value="'.$row['OPEN_NO'].'">'.$row['OPEN_YEAR'].'</option>';	
			else
				$html .= '<option value="'.$row['OPEN_NO'].'">'.$row['OPEN_YEAR'].'</option>';	
		}
		return $html;
	}
	
	public function getSavedDetails($serialNo , $serialYear)
	{
		$cols	= "
					trn_orderdetails.intOrderNo,
					trn_orderdetails.intOrderYear,
					trn_orderdetails.strSalesOrderNo,
					trn_orderdetails.intSalesOrderId,
					trn_orderdetails.strGraphicNo,
					trn_orderdetails.intSampleNo,
					trn_orderdetails.intSampleYear,
					trn_orderdetails.strCombo,
					trn_orderdetails.strLineNo,
					trn_orderdetails.strStyleNo,
					trn_orderdetails.strPrintName,
					trn_orderdetails.intRevisionNo,
					trn_orderdetails.intPart,
					trn_orderdetails.intQty,
					trn_orderdetails.dblPrice,
					trn_orderdetails.dblOverCutPercentage,
					trn_orderdetails.dblDamagePercentage,
					trn_orderdetails.dtPSD,
					trn_orderdetails.dtDeliveryDate,
					trn_orderdetails.TECHNIQUE_GROUP_ID,
					if(trn_orderdetails.`STATUS`=-10,'Closed','Open') as STATUS,
					if(trn_sales_order_open_details.`SALE_ORDER_ID` IS NOT NULL,1,0) as SAVED_STATUS 
				 ";		

 		$join	.=" INNER JOIN trn_sales_order_open_header ON trn_sales_order_open_header.OPEN_NO = trn_sales_order_open_details.OPEN_NO AND trn_sales_order_open_header.OPEN_YEAR = trn_sales_order_open_details.OPEN_YEAR 
					INNER JOIN trn_orderdetails ON trn_orderdetails.intOrderNo = trn_sales_order_open_details.ORDER_NO AND trn_orderdetails.intOrderYear = trn_sales_order_open_details.ORDER_YEAR AND trn_orderdetails.intSalesOrderId = trn_sales_order_open_details.SALE_ORDER_ID
			
					";
		
		$where	= "trn_sales_order_open_header.OPEN_NO = '$serialNo' AND 
					trn_sales_order_open_header.OPEN_YEAR = '$serialYear' ";
					
		$where	.= "GROUP BY
					trn_sales_order_open_details.ORDER_YEAR, 
					trn_sales_order_open_details.ORDER_NO, 
					trn_sales_order_open_details.SALE_ORDER_ID 
					ORDER BY 
					trn_sales_order_open_details.ORDER_YEAR ASC, 
					trn_sales_order_open_details.ORDER_NO ASC, 
					trn_sales_order_open_details.SALE_ORDER_ID ASC ";
		//echo $sql	="select ".$cols." from trn_sales_order_open_header ".$join." where ".$where;
//$cols = 1; $join='' ; $where = '';
		
		return $result = $this->select($cols,$join,$where);	
	}	
	


	//END }
}
?>
<?php
class mst_companies{
 
	private $db;
	private $table= "mst_companies";
	
	//private property
	private $intId;
	private $strCode;
	private $strName;
	private $intCountryId;
	private $strWebSite;
	private $strRemarks;
	private $strAccountNo;
	private $strRegistrationNo;
	private $strVatNo;
	private $strSVatNo;
	private $strWHCode;
	private $intBaseCurrencyId;
	private $intStatus;
	private $intCreator;
	private $dtmCreateDate;
	private $intModifyer;
	private $dtmModifyDate;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('intId'=>'intId',
										'strCode'=>'strCode',
										'strName'=>'strName',
										'intCountryId'=>'intCountryId',
										'strWebSite'=>'strWebSite',
										'strRemarks'=>'strRemarks',
										'strAccountNo'=>'strAccountNo',
										'strRegistrationNo'=>'strRegistrationNo',
										'strVatNo'=>'strVatNo',
										'strSVatNo'=>'strSVatNo',
										'strWHCode'=>'strWHCode',
										'intBaseCurrencyId'=>'intBaseCurrencyId',
										'intStatus'=>'intStatus',
										'intCreator'=>'intCreator',
										'dtmCreateDate'=>'dtmCreateDate',
										'intModifyer'=>'intModifyer',
										'dtmModifyDate'=>'dtmModifyDate',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "intId = ".$this->intId."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun intId
	function getintId()
	{
		$this->validate();
		return $this->intId;
	}
	
	//retun strCode
	function getstrCode()
	{
		$this->validate();
		return $this->strCode;
	}
	
	//retun strName
	function getstrName()
	{
		$this->validate();
		return $this->strName;
	}
	
	//retun intCountryId
	function getintCountryId()
	{
		$this->validate();
		return $this->intCountryId;
	}
	
	//retun strWebSite
	function getstrWebSite()
	{
		$this->validate();
		return $this->strWebSite;
	}
	
	//retun strRemarks
	function getstrRemarks()
	{
		$this->validate();
		return $this->strRemarks;
	}
	
	//retun strAccountNo
	function getstrAccountNo()
	{
		$this->validate();
		return $this->strAccountNo;
	}
	
	//retun strRegistrationNo
	function getstrRegistrationNo()
	{
		$this->validate();
		return $this->strRegistrationNo;
	}
	
	//retun strVatNo
	function getstrVatNo()
	{
		$this->validate();
		return $this->strVatNo;
	}
	
	//retun strSVatNo
	function getstrSVatNo()
	{
		$this->validate();
		return $this->strSVatNo;
	}
	
	//retun strWHCode
	function getstrWHCode()
	{
		$this->validate();
		return $this->strWHCode;
	}
	
	//retun intBaseCurrencyId
	function getintBaseCurrencyId()
	{
		$this->validate();
		return $this->intBaseCurrencyId;
	}
	
	//retun intStatus
	function getintStatus()
	{
		$this->validate();
		return $this->intStatus;
	}
	
	//retun intCreator
	function getintCreator()
	{
		$this->validate();
		return $this->intCreator;
	}
	
	//retun dtmCreateDate
	function getdtmCreateDate()
	{
		$this->validate();
		return $this->dtmCreateDate;
	}
	
	//retun intModifyer
	function getintModifyer()
	{
		$this->validate();
		return $this->intModifyer;
	}
	
	//retun dtmModifyDate
	function getdtmModifyDate()
	{
		$this->validate();
		return $this->dtmModifyDate;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set intId
	function setintId($intId)
	{
		array_push($this->commitArray,'intId');
		$this->intId = $intId;
	}
	
	//set strCode
	function setstrCode($strCode)
	{
		array_push($this->commitArray,'strCode');
		$this->strCode = $strCode;
	}
	
	//set strName
	function setstrName($strName)
	{
		array_push($this->commitArray,'strName');
		$this->strName = $strName;
	}
	
	//set intCountryId
	function setintCountryId($intCountryId)
	{
		array_push($this->commitArray,'intCountryId');
		$this->intCountryId = $intCountryId;
	}
	
	//set strWebSite
	function setstrWebSite($strWebSite)
	{
		array_push($this->commitArray,'strWebSite');
		$this->strWebSite = $strWebSite;
	}
	
	//set strRemarks
	function setstrRemarks($strRemarks)
	{
		array_push($this->commitArray,'strRemarks');
		$this->strRemarks = $strRemarks;
	}
	
	//set strAccountNo
	function setstrAccountNo($strAccountNo)
	{
		array_push($this->commitArray,'strAccountNo');
		$this->strAccountNo = $strAccountNo;
	}
	
	//set strRegistrationNo
	function setstrRegistrationNo($strRegistrationNo)
	{
		array_push($this->commitArray,'strRegistrationNo');
		$this->strRegistrationNo = $strRegistrationNo;
	}
	
	//set strVatNo
	function setstrVatNo($strVatNo)
	{
		array_push($this->commitArray,'strVatNo');
		$this->strVatNo = $strVatNo;
	}
	
	//set strSVatNo
	function setstrSVatNo($strSVatNo)
	{
		array_push($this->commitArray,'strSVatNo');
		$this->strSVatNo = $strSVatNo;
	}
	
	//set strWHCode
	function setstrWHCode($strWHCode)
	{
		array_push($this->commitArray,'strWHCode');
		$this->strWHCode = $strWHCode;
	}
	
	//set intBaseCurrencyId
	function setintBaseCurrencyId($intBaseCurrencyId)
	{
		array_push($this->commitArray,'intBaseCurrencyId');
		$this->intBaseCurrencyId = $intBaseCurrencyId;
	}
	
	//set intStatus
	function setintStatus($intStatus)
	{
		array_push($this->commitArray,'intStatus');
		$this->intStatus = $intStatus;
	}
	
	//set intCreator
	function setintCreator($intCreator)
	{
		array_push($this->commitArray,'intCreator');
		$this->intCreator = $intCreator;
	}
	
	//set dtmCreateDate
	function setdtmCreateDate($dtmCreateDate)
	{
		array_push($this->commitArray,'dtmCreateDate');
		$this->dtmCreateDate = $dtmCreateDate;
	}
	
	//set intModifyer
	function setintModifyer($intModifyer)
	{
		array_push($this->commitArray,'intModifyer');
		$this->intModifyer = $intModifyer;
	}
	
	//set dtmModifyDate
	function setdtmModifyDate($dtmModifyDate)
	{
		array_push($this->commitArray,'dtmModifyDate');
		$this->dtmModifyDate = $dtmModifyDate;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->intId=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($intId)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "intId='$intId'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($intId,$strCode,$strName,$intCountryId,$strWebSite,$strRemarks,$strAccountNo,$strRegistrationNo,$strVatNo,$strSVatNo,$strWHCode,$intBaseCurrencyId,$intStatus,$intCreator,$dtmCreateDate,$intModifyer,$dtmModifyDate){
		$data = array('intId'=>$intId 
				,'strCode'=>$strCode 
				,'strName'=>$strName 
				,'intCountryId'=>$intCountryId 
				,'strWebSite'=>$strWebSite 
				,'strRemarks'=>$strRemarks 
				,'strAccountNo'=>$strAccountNo 
				,'strRegistrationNo'=>$strRegistrationNo 
				,'strVatNo'=>$strVatNo 
				,'strSVatNo'=>$strSVatNo 
				,'strWHCode'=>$strWHCode 
				,'intBaseCurrencyId'=>$intBaseCurrencyId 
				,'intStatus'=>$intStatus 
				,'intCreator'=>$intCreator 
				,'dtmCreateDate'=>$dtmCreateDate 
				,'intModifyer'=>$intModifyer 
				,'dtmModifyDate'=>$dtmModifyDate 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('intId,strName',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intId'])
				$html .= '<option selected="selected" value="'.$row['intId'].'">'.$row['strName'].'</option>';	
			else
				$html .= '<option value="'.$row['intId'].'">'.$row['strName'].'</option>';	
		}
		return $html;
	}
	
	public function getcompanywiseLocations($userId=null)
	{
		$cols	= " mst_locations.strName AS locationName,
					mst_companies.strName AS companyName,
					mst_locations.intId AS locationId,
					mst_companies.intId AS companyId ,
					IF((
					SELECT intUserId
					FROM mst_locations_user
					WHERE intLocationId = mst_locations.intId AND
					mst_locations_user.intUserId = '$userId'
					) IS NULL,0,1) AS checkUser ";
		
		$join	= " INNER JOIN mst_locations ON mst_companies.intId = mst_locations.intCompanyId ";
	
		$where	= " mst_locations.intStatus = 1 ";
		
		$order	= " companyName ASC,locationName ASC ";
		
		return $this->select($cols,$join,$where,$order);
	}
	//END }
}
?>
<?php
class finance_other_receivable_invoice_header{
 
	private $db;
	private $table= "finance_other_receivable_invoice_header";
	
	//private property
	private $BILL_INVOICE_NO;
	private $BILL_INVOICE_YEAR;
	private $CUSTOMER_ID;
	private $CURRENCY_ID;
	private $REFERENCE_NO;
	private $REMARKS;
	private $BILL_DATE;
	private $STATUS;
	private $APPROVE_LEVELS;
	private $PRINT_STATUS;
	private $COMPANY_ID;
	private $LOCATION_ID;
	private $CREATED_BY;
	private $CREATED_DATE;
	private $LAST_MODIFY_BY;
	private $LAST_MODIFY_DATE;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('BILL_INVOICE_NO'=>'BILL_INVOICE_NO',
										'BILL_INVOICE_YEAR'=>'BILL_INVOICE_YEAR',
										'CUSTOMER_ID'=>'CUSTOMER_ID',
										'CURRENCY_ID'=>'CURRENCY_ID',
										'REFERENCE_NO'=>'REFERENCE_NO',
										'REMARKS'=>'REMARKS',
										'BILL_DATE'=>'BILL_DATE',
										'STATUS'=>'STATUS',
										'APPROVE_LEVELS'=>'APPROVE_LEVELS',
										'PRINT_STATUS'=>'PRINT_STATUS',
										'COMPANY_ID'=>'COMPANY_ID',
										'LOCATION_ID'=>'LOCATION_ID',
										'CREATED_BY'=>'CREATED_BY',
										'CREATED_DATE'=>'CREATED_DATE',
										'LAST_MODIFY_BY'=>'LAST_MODIFY_BY',
										'LAST_MODIFY_DATE'=>'LAST_MODIFY_DATE',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "BILL_INVOICE_NO = ".$this->BILL_INVOICE_NO." and BILL_INVOICE_YEAR = ".$this->BILL_INVOICE_YEAR."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun BILL_INVOICE_NO
	function getBILL_INVOICE_NO()
	{
		$this->validate();
		return $this->BILL_INVOICE_NO;
	}
	
	//retun BILL_INVOICE_YEAR
	function getBILL_INVOICE_YEAR()
	{
		$this->validate();
		return $this->BILL_INVOICE_YEAR;
	}
	
	//retun CUSTOMER_ID
	function getCUSTOMER_ID()
	{
		$this->validate();
		return $this->CUSTOMER_ID;
	}
	
	//retun CURRENCY_ID
	function getCURRENCY_ID()
	{
		$this->validate();
		return $this->CURRENCY_ID;
	}
	
	//retun REFERENCE_NO
	function getREFERENCE_NO()
	{
		$this->validate();
		return $this->REFERENCE_NO;
	}
	
	//retun REMARKS
	function getREMARKS()
	{
		$this->validate();
		return $this->REMARKS;
	}
	
	//retun BILL_DATE
	function getBILL_DATE()
	{
		$this->validate();
		return $this->BILL_DATE;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//retun APPROVE_LEVELS
	function getAPPROVE_LEVELS()
	{
		$this->validate();
		return $this->APPROVE_LEVELS;
	}
	
	//retun PRINT_STATUS
	function getPRINT_STATUS()
	{
		$this->validate();
		return $this->PRINT_STATUS;
	}
	
	//retun COMPANY_ID
	function getCOMPANY_ID()
	{
		$this->validate();
		return $this->COMPANY_ID;
	}
	
	//retun LOCATION_ID
	function getLOCATION_ID()
	{
		$this->validate();
		return $this->LOCATION_ID;
	}
	
	//retun CREATED_BY
	function getCREATED_BY()
	{
		$this->validate();
		return $this->CREATED_BY;
	}
	
	//retun CREATED_DATE
	function getCREATED_DATE()
	{
		$this->validate();
		return $this->CREATED_DATE;
	}
	
	//retun LAST_MODIFY_BY
	function getLAST_MODIFY_BY()
	{
		$this->validate();
		return $this->LAST_MODIFY_BY;
	}
	
	//retun LAST_MODIFY_DATE
	function getLAST_MODIFY_DATE()
	{
		$this->validate();
		return $this->LAST_MODIFY_DATE;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set BILL_INVOICE_NO
	function setBILL_INVOICE_NO($BILL_INVOICE_NO)
	{
		array_push($this->commitArray,'BILL_INVOICE_NO');
		$this->BILL_INVOICE_NO = $BILL_INVOICE_NO;
	}
	
	//set BILL_INVOICE_YEAR
	function setBILL_INVOICE_YEAR($BILL_INVOICE_YEAR)
	{
		array_push($this->commitArray,'BILL_INVOICE_YEAR');
		$this->BILL_INVOICE_YEAR = $BILL_INVOICE_YEAR;
	}
	
	//set CUSTOMER_ID
	function setCUSTOMER_ID($CUSTOMER_ID)
	{
		array_push($this->commitArray,'CUSTOMER_ID');
		$this->CUSTOMER_ID = $CUSTOMER_ID;
	}
	
	//set CURRENCY_ID
	function setCURRENCY_ID($CURRENCY_ID)
	{
		array_push($this->commitArray,'CURRENCY_ID');
		$this->CURRENCY_ID = $CURRENCY_ID;
	}
	
	//set REFERENCE_NO
	function setREFERENCE_NO($REFERENCE_NO)
	{
		array_push($this->commitArray,'REFERENCE_NO');
		$this->REFERENCE_NO = $REFERENCE_NO;
	}
	
	//set REMARKS
	function setREMARKS($REMARKS)
	{
		array_push($this->commitArray,'REMARKS');
		$this->REMARKS = $REMARKS;
	}
	
	//set BILL_DATE
	function setBILL_DATE($BILL_DATE)
	{
		array_push($this->commitArray,'BILL_DATE');
		$this->BILL_DATE = $BILL_DATE;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//set APPROVE_LEVELS
	function setAPPROVE_LEVELS($APPROVE_LEVELS)
	{
		array_push($this->commitArray,'APPROVE_LEVELS');
		$this->APPROVE_LEVELS = $APPROVE_LEVELS;
	}
	
	//set PRINT_STATUS
	function setPRINT_STATUS($PRINT_STATUS)
	{
		array_push($this->commitArray,'PRINT_STATUS');
		$this->PRINT_STATUS = $PRINT_STATUS;
	}
	
	//set COMPANY_ID
	function setCOMPANY_ID($COMPANY_ID)
	{
		array_push($this->commitArray,'COMPANY_ID');
		$this->COMPANY_ID = $COMPANY_ID;
	}
	
	//set LOCATION_ID
	function setLOCATION_ID($LOCATION_ID)
	{
		array_push($this->commitArray,'LOCATION_ID');
		$this->LOCATION_ID = $LOCATION_ID;
	}
	
	//set CREATED_BY
	function setCREATED_BY($CREATED_BY)
	{
		array_push($this->commitArray,'CREATED_BY');
		$this->CREATED_BY = $CREATED_BY;
	}
	
	//set CREATED_DATE
	function setCREATED_DATE($CREATED_DATE)
	{
		array_push($this->commitArray,'CREATED_DATE');
		$this->CREATED_DATE = $CREATED_DATE;
	}
	
	//set LAST_MODIFY_BY
	function setLAST_MODIFY_BY($LAST_MODIFY_BY)
	{
		array_push($this->commitArray,'LAST_MODIFY_BY');
		$this->LAST_MODIFY_BY = $LAST_MODIFY_BY;
	}
	
	//set LAST_MODIFY_DATE
	function setLAST_MODIFY_DATE($LAST_MODIFY_DATE)
	{
		array_push($this->commitArray,'LAST_MODIFY_DATE');
		$this->LAST_MODIFY_DATE = $LAST_MODIFY_DATE;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->BILL_INVOICE_NO=='' || $this->BILL_INVOICE_YEAR=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($BILL_INVOICE_NO , $BILL_INVOICE_YEAR)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "BILL_INVOICE_NO='$BILL_INVOICE_NO' and BILL_INVOICE_YEAR='$BILL_INVOICE_YEAR'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($BILL_INVOICE_NO,$BILL_INVOICE_YEAR,$CUSTOMER_ID,$CURRENCY_ID,$REFERENCE_NO,$REMARKS,$BILL_DATE,$STATUS,$APPROVE_LEVELS,$COMPANY_ID,$LOCATION_ID,$CREATED_BY,$CREATED_DATE,$LAST_MODIFY_BY,$LAST_MODIFY_DATE){
		$data = array('BILL_INVOICE_NO'=>$BILL_INVOICE_NO 
				,'BILL_INVOICE_YEAR'=>$BILL_INVOICE_YEAR 
				,'CUSTOMER_ID'=>$CUSTOMER_ID 
				,'CURRENCY_ID'=>$CURRENCY_ID 
				,'REFERENCE_NO'=>$REFERENCE_NO 
				,'REMARKS'=>$REMARKS 
				,'BILL_DATE'=>$BILL_DATE 
				,'STATUS'=>$STATUS 
				,'APPROVE_LEVELS'=>$APPROVE_LEVELS 
				,'COMPANY_ID'=>$COMPANY_ID 
				,'LOCATION_ID'=>$LOCATION_ID 
				,'CREATED_BY'=>$CREATED_BY 
				,'CREATED_DATE'=>$CREATED_DATE 
				,'LAST_MODIFY_BY'=>$LAST_MODIFY_BY 
				,'LAST_MODIFY_DATE'=>$LAST_MODIFY_DATE 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('BILL_INVOICE_YEAR,BILL_INVOICE_NO',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['BILL_INVOICE_YEAR'])
				$html .= '<option selected="selected" value="'.$row['BILL_INVOICE_YEAR'].'">'.$row['BILL_INVOICE_NO'].'</option>';	
			else
				$html .= '<option value="'.$row['BILL_INVOICE_YEAR'].'">'.$row['BILL_INVOICE_NO'].'</option>';	
		}
		return $html;
	}
	public function getCustomerWiseHeaderDetail($customerID,$currency,$locationId)
	{
		$cols			= "BILL_INVOICE_NO,BILL_INVOICE_YEAR,REFERENCE_NO,BILL_DATE";
		$where			= "CUSTOMER_ID = '$customerID' AND CURRENCY_ID = '$currency' AND LOCATION_ID = '$locationId' AND STATUS = 1";	
		
		$result			= $this->select($cols,NULL,$where,NULL,NULL);	
		return $result;
	}
	
	//END }
}
?>
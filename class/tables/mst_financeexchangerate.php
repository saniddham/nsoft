<?php
class mst_financeexchangerate{
 
	private $db;
	private $table= "mst_financeexchangerate";
	
	//private property
	private $intCurrencyId;
	private $dtmDate;
	private $intCompanyId;
	private $dblSellingRate;
	private $dblBuying;
	private $dblExcAvgRate;
	private $intCreator;
	private $dtmCreateDate;
	private $intModifyer;
	private $dtmModifyDate;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('intCurrencyId'=>'intCurrencyId',
										'dtmDate'=>'dtmDate',
										'intCompanyId'=>'intCompanyId',
										'dblSellingRate'=>'dblSellingRate',
										'dblBuying'=>'dblBuying',
										'dblExcAvgRate'=>'dblExcAvgRate',
										'intCreator'=>'intCreator',
										'dtmCreateDate'=>'dtmCreateDate',
										'intModifyer'=>'intModifyer',
										'dtmModifyDate'=>'dtmModifyDate',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "intCurrencyId = ".$this->intCurrencyId." and dtmDate = ".$this->dtmDate." and intCompanyId = ".$this->intCompanyId."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun intCurrencyId
	function getintCurrencyId()
	{
		$this->validate();
		return $this->intCurrencyId;
	}
	
	//retun dtmDate
	function getdtmDate()
	{
		$this->validate();
		return $this->dtmDate;
	}
	
	//retun intCompanyId
	function getintCompanyId()
	{
		$this->validate();
		return $this->intCompanyId;
	}
	
	//retun dblSellingRate
	function getdblSellingRate()
	{
		$this->validate();
		return $this->dblSellingRate;
	}
	
	//retun dblBuying
	function getdblBuying()
	{
		$this->validate();
		return $this->dblBuying;
	}
	
	//retun dblExcAvgRate
	function getdblExcAvgRate()
	{
		$this->validate();
		return $this->dblExcAvgRate;
	}
	
	//retun intCreator
	function getintCreator()
	{
		$this->validate();
		return $this->intCreator;
	}
	
	//retun dtmCreateDate
	function getdtmCreateDate()
	{
		$this->validate();
		return $this->dtmCreateDate;
	}
	
	//retun intModifyer
	function getintModifyer()
	{
		$this->validate();
		return $this->intModifyer;
	}
	
	//retun dtmModifyDate
	function getdtmModifyDate()
	{
		$this->validate();
		return $this->dtmModifyDate;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set intCurrencyId
	function setintCurrencyId($intCurrencyId)
	{
		array_push($this->commitArray,'intCurrencyId');
		$this->intCurrencyId = $intCurrencyId;
	}
	
	//set dtmDate
	function setdtmDate($dtmDate)
	{
		array_push($this->commitArray,'dtmDate');
		$this->dtmDate = $dtmDate;
	}
	
	//set intCompanyId
	function setintCompanyId($intCompanyId)
	{
		array_push($this->commitArray,'intCompanyId');
		$this->intCompanyId = $intCompanyId;
	}
	
	//set dblSellingRate
	function setdblSellingRate($dblSellingRate)
	{
		array_push($this->commitArray,'dblSellingRate');
		$this->dblSellingRate = $dblSellingRate;
	}
	
	//set dblBuying
	function setdblBuying($dblBuying)
	{
		array_push($this->commitArray,'dblBuying');
		$this->dblBuying = $dblBuying;
	}
	
	//set dblExcAvgRate
	function setdblExcAvgRate($dblExcAvgRate)
	{
		array_push($this->commitArray,'dblExcAvgRate');
		$this->dblExcAvgRate = $dblExcAvgRate;
	}
	
	//set intCreator
	function setintCreator($intCreator)
	{
		array_push($this->commitArray,'intCreator');
		$this->intCreator = $intCreator;
	}
	
	//set dtmCreateDate
	function setdtmCreateDate($dtmCreateDate)
	{
		array_push($this->commitArray,'dtmCreateDate');
		$this->dtmCreateDate = $dtmCreateDate;
	}
	
	//set intModifyer
	function setintModifyer($intModifyer)
	{
		array_push($this->commitArray,'intModifyer');
		$this->intModifyer = $intModifyer;
	}
	
	//set dtmModifyDate
	function setdtmModifyDate($dtmModifyDate)
	{
		array_push($this->commitArray,'dtmModifyDate');
		$this->dtmModifyDate = $dtmModifyDate;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->intCurrencyId=='' || $this->dtmDate=='' || $this->intCompanyId=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($intCurrencyId , $dtmDate , $intCompanyId)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "intCurrencyId='$intCurrencyId' and dtmDate='$dtmDate' and intCompanyId='$intCompanyId'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($intCurrencyId,$dtmDate,$intCompanyId,$dblSellingRate,$dblBuying,$dblExcAvgRate,$intCreator,$dtmCreateDate,$intModifyer,$dtmModifyDate){
		$data = array('intCurrencyId'=>$intCurrencyId 
				,'dtmDate'=>$dtmDate 
				,'intCompanyId'=>$intCompanyId 
				,'dblSellingRate'=>$dblSellingRate 
				,'dblBuying'=>$dblBuying 
				,'dblExcAvgRate'=>$dblExcAvgRate 
				,'intCreator'=>$intCreator 
				,'dtmCreateDate'=>$dtmCreateDate 
				,'intModifyer'=>$intModifyer 
				,'dtmModifyDate'=>$dtmModifyDate 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('intCurrencyId,dtmDate',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intCurrencyId'])
				$html .= '<option selected="selected" value="'.$row['intCurrencyId'].'">'.$row['dtmDate'].'</option>';	
			else
				$html .= '<option value="'.$row['intCurrencyId'].'">'.$row['dtmDate'].'</option>';	
		}
		return $html;
	}
	
	//END }
#BEGIN  - USER DEFINED FUNCTIONS ADDED
	public function checkCurrencyAvailableForSelectedDate($dtmDate,$intCompanyId)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "dtmDate='$dtmDate' AND intCompanyId='$intCompanyId'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return true;
		else
			return false;
	}
	
	public function convertAmountTogivenDate($company,$fromCurrency,$toCurrency,$date,$amount)
	{

		$cols	="	dblExcAvgRate as fromRate ";
		$where  ="
					mst_financeexchangerate.dtmDate = '$date' AND
					mst_financeexchangerate.intCompanyId = $company AND
					(mst_financeexchangerate.intCurrencyId = $fromCurrency) 
		
					";
				//	echo "select ".$cols." from mst_financeexchangerate ". $where;
		$result = $this->select($cols,$join=null,$where,$order=null,$limit=null);
		$row	= mysqli_fetch_array($result);
		$fromRate	= $row['fromRate'];
					
		$cols	="	dblExcAvgRate as toRate ";
		$where  ="
					mst_financeexchangerate.dtmDate = '$date' AND
					mst_financeexchangerate.intCompanyId = $company AND
					(mst_financeexchangerate.intCurrencyId = $toCurrency) 
					";
		$result = $this->select($cols,$join=null,$where,$order=null,$limit=null);
		$row	= mysqli_fetch_array($result);
		$toRate	= $row['toRate'];
		
		return $amount*$fromRate/$toRate;
	
	
	}
#END	- USER DEFINED FUNCTIONS ADDED
}
?>
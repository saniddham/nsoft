
<?php

class mst_financetaxisolated{
 
	private $db;
	private $table= "mst_financetaxisolated";
	
	//private property
	private $intId;
	private $strCode;
	private $strFormulaCode;
	private $strDescription;
	private $dblRate;
	private $intChartOfAccountId;
	private $strTaxReturn;
	private $intStatus;
	private $intCreator;
	private $dtmCreateDate;
	private $intModifyer;
	private $dtmModifyDate;
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->db->field_array_set(array('intId'=>'intId',
										'strCode'=>'strCode',
										'strFormulaCode'=>'strFormulaCode',
										'strDescription'=>'strDescription',
										'dblRate'=>'dblRate',
										'intChartOfAccountId'=>'intChartOfAccountId',
										'strTaxReturn'=>'strTaxReturn',
										'intStatus'=>'intStatus',
										'intCreator'=>'intCreator',
										'dtmCreateDate'=>'dtmCreateDate',
										'intModifyer'=>'intModifyer',
										'dtmModifyDate'=>'dtmModifyDate',
										));
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  
		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	//public functions for private properties
	
	//retun intId
	function getintId()
	{
		$this->validate();
		return $this->intId;
	}
	
	//retun strCode
	function getstrCode()
	{
		$this->validate();
		return $this->strCode;
	}
	
	//retun strFormulaCode
	function getstrFormulaCode()
	{
		$this->validate();
		return $this->strFormulaCode;
	}
	
	//retun strDescription
	function getstrDescription()
	{
		$this->validate();
		return $this->strDescription;
	}
	
	//retun dblRate
	function getdblRate()
	{
		$this->validate();
		return $this->dblRate;
	}
	
	//retun intChartOfAccountId
	function getintChartOfAccountId()
	{
		$this->validate();
		return $this->intChartOfAccountId;
	}
	
	//retun strTaxReturn
	function getstrTaxReturn()
	{
		$this->validate();
		return $this->strTaxReturn;
	}
	
	//retun intStatus
	function getintStatus()
	{
		$this->validate();
		return $this->intStatus;
	}
	
	//retun intCreator
	function getintCreator()
	{
		$this->validate();
		return $this->intCreator;
	}
	
	//retun dtmCreateDate
	function getdtmCreateDate()
	{
		$this->validate();
		return $this->dtmCreateDate;
	}
	
	//retun intModifyer
	function getintModifyer()
	{
		$this->validate();
		return $this->intModifyer;
	}
	
	//retun dtmModifyDate
	function getdtmModifyDate()
	{
		$this->validate();
		return $this->dtmModifyDate;
	}
	
	//validate primary values
	private function validate()
	{
		if($this->intId=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
		}
	}
	
	public function set($intId)
	{
		$result = $this->select('*',null,"intId='$intId'");	
		if($this->db->numRows()>0)
			$this->setVariables($result);
		else
			throw new Exception("Record not found.");
			
	}
	
	
}
?>

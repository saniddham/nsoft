
<?php

class mst_item{
 
	private $db;
	private $table= "mst_item";
	
	//private property
	private $intId;
	private $intMainCategory;
	private $intSubCategory;
	private $strCode;
	private $strName;
	private $intUOM;
	private $intBomItem;
	private $intSpecialRm;
	private $intStoresTrnsItem;
	private $intStockValuationType;
	private $intPerfectSupplier;
	private $intAlternativeSupplier;
	private $intDefaultCostItem;
	private $dblConsumptionPerInch;
	private $dblCostPerInch;
	private $dblLastPrice;
	private $intReorderPolicy;
	private $intReservePolicy;
	private $dblReorderCycle;
	private $dblSaftyLeadTime;
	private $dblSaftyStockQty;
	private $dblReorderPoints;
	private $dblReorderQty;
	private $dblMaximumInventory;
	private $dblMaximumOrderQty;
	private $dblMinimumOrderQty;
	private $intBlockPurchase;
	private $intBlockGrn;
	private $foil_width;
	private $foil_height;
	private $intStatus;
	private $intCreator;
	private $dtmCreateDate;
	private $intModifyer;
	private $dtmModifyDate;
	private $intCurrency;
	private $intInspectionItem;
	private $intTechniqueId;
	private $EXPIRERY_ITEM;
	private $DEPRECIATION_RATE;
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->db->field_array_set(array('intId'=>'intId',
										'intMainCategory'=>'intMainCategory',
										'intSubCategory'=>'intSubCategory',
										'strCode'=>'strCode',
										'strName'=>'strName',
										'intUOM'=>'intUOM',
										'intBomItem'=>'intBomItem',
										'intSpecialRm'=>'intSpecialRm',
										'intStoresTrnsItem'=>'intStoresTrnsItem',
										'intStockValuationType'=>'intStockValuationType',
										'intPerfectSupplier'=>'intPerfectSupplier',
										'intAlternativeSupplier'=>'intAlternativeSupplier',
										'intDefaultCostItem'=>'intDefaultCostItem',
										'dblConsumptionPerInch'=>'dblConsumptionPerInch',
										'dblCostPerInch'=>'dblCostPerInch',
										'dblLastPrice'=>'dblLastPrice',
										'intReorderPolicy'=>'intReorderPolicy',
										'intReservePolicy'=>'intReservePolicy',
										'dblReorderCycle'=>'dblReorderCycle',
										'dblSaftyLeadTime'=>'dblSaftyLeadTime',
										'dblSaftyStockQty'=>'dblSaftyStockQty',
										'dblReorderPoints'=>'dblReorderPoints',
										'dblReorderQty'=>'dblReorderQty',
										'dblMaximumInventory'=>'dblMaximumInventory',
										'dblMaximumOrderQty'=>'dblMaximumOrderQty',
										'dblMinimumOrderQty'=>'dblMinimumOrderQty',
										'intBlockPurchase'=>'intBlockPurchase',
										'intBlockGrn'=>'intBlockGrn',
										'foil_width'=>'foil_width',
										'foil_height'=>'foil_height',
										'intStatus'=>'intStatus',
										'intCreator'=>'intCreator',
										'dtmCreateDate'=>'dtmCreateDate',
										'intModifyer'=>'intModifyer',
										'dtmModifyDate'=>'dtmModifyDate',
										'intCurrency'=>'intCurrency',
										'intInspectionItem'=>'intInspectionItem',
										'intTechniqueId'=>'intTechniqueId',
										'EXPIRERY_ITEM'=>'EXPIRERY_ITEM', 
										'DEPRECIATION_RATE'=>'DEPRECIATION_RATE' 
										));
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  
		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	//public functions for private properties
	
	//retun intId
	function getintId()
	{
		$this->validate();
		return $this->intId;
	}
	
	//retun intMainCategory
	function getintMainCategory()
	{
		$this->validate();
		return $this->intMainCategory;
	}
	
	//retun intSubCategory
	function getintSubCategory()
	{
		$this->validate();
		return $this->intSubCategory;
	}
	
	//retun strCode
	function getstrCode()
	{
		$this->validate();
		return $this->strCode;
	}
	
	//retun strName
	function getstrName()
	{
		$this->validate();
		return $this->strName;
	}
	
	//retun intUOM
	function getintUOM()
	{
		$this->validate();
		return $this->intUOM;
	}
	
	//retun intBomItem
	function getintBomItem()
	{
		$this->validate();
		return $this->intBomItem;
	}
	
	//retun intSpecialRm
	function getintSpecialRm()
	{
		$this->validate();
		return $this->intSpecialRm;
	}
	
	//retun intStoresTrnsItem
	function getintStoresTrnsItem()
	{
		$this->validate();
		return $this->intStoresTrnsItem;
	}
	
	//retun intStockValuationType
	function getintStockValuationType()
	{
		$this->validate();
		return $this->intStockValuationType;
	}
	
	//retun intPerfectSupplier
	function getintPerfectSupplier()
	{
		$this->validate();
		return $this->intPerfectSupplier;
	}
	
	//retun intAlternativeSupplier
	function getintAlternativeSupplier()
	{
		$this->validate();
		return $this->intAlternativeSupplier;
	}
	
	//retun intDefaultCostItem
	function getintDefaultCostItem()
	{
		$this->validate();
		return $this->intDefaultCostItem;
	}
	
	//retun dblConsumptionPerInch
	function getdblConsumptionPerInch()
	{
		$this->validate();
		return $this->dblConsumptionPerInch;
	}
	
	//retun dblCostPerInch
	function getdblCostPerInch()
	{
		$this->validate();
		return $this->dblCostPerInch;
	}
	
	//retun dblLastPrice
	function getdblLastPrice()
	{
		$this->validate();
		return $this->dblLastPrice;
	}
	
	//retun intReorderPolicy
	function getintReorderPolicy()
	{
		$this->validate();
		return $this->intReorderPolicy;
	}
	
	//retun intReservePolicy
	function getintReservePolicy()
	{
		$this->validate();
		return $this->intReservePolicy;
	}
	
	//retun dblReorderCycle
	function getdblReorderCycle()
	{
		$this->validate();
		return $this->dblReorderCycle;
	}
	
	//retun dblSaftyLeadTime
	function getdblSaftyLeadTime()
	{
		$this->validate();
		return $this->dblSaftyLeadTime;
	}
	
	//retun dblSaftyStockQty
	function getdblSaftyStockQty()
	{
		$this->validate();
		return $this->dblSaftyStockQty;
	}
	
	//retun dblReorderPoints
	function getdblReorderPoints()
	{
		$this->validate();
		return $this->dblReorderPoints;
	}
	
	//retun dblReorderQty
	function getdblReorderQty()
	{
		$this->validate();
		return $this->dblReorderQty;
	}
	
	//retun dblMaximumInventory
	function getdblMaximumInventory()
	{
		$this->validate();
		return $this->dblMaximumInventory;
	}
	
	//retun dblMaximumOrderQty
	function getdblMaximumOrderQty()
	{
		$this->validate();
		return $this->dblMaximumOrderQty;
	}
	
	//retun dblMinimumOrderQty
	function getdblMinimumOrderQty()
	{
		$this->validate();
		return $this->dblMinimumOrderQty;
	}
	
	//retun intBlockPurchase
	function getintBlockPurchase()
	{
		$this->validate();
		return $this->intBlockPurchase;
	}
	
	//retun intBlockGrn
	function getintBlockGrn()
	{
		$this->validate();
		return $this->intBlockGrn;
	}
	
	//retun foil_width
	function getfoil_width()
	{
		$this->validate();
		return $this->foil_width;
	}
	
	//retun foil_height
	function getfoil_height()
	{
		$this->validate();
		return $this->foil_height;
	}
	
	//retun intStatus
	function getintStatus()
	{
		$this->validate();
		return $this->intStatus;
	}
	
	//retun intCreator
	function getintCreator()
	{
		$this->validate();
		return $this->intCreator;
	}
	
	//retun dtmCreateDate
	function getdtmCreateDate()
	{
		$this->validate();
		return $this->dtmCreateDate;
	}
	
	//retun intModifyer
	function getintModifyer()
	{
		$this->validate();
		return $this->intModifyer;
	}
	
	//retun dtmModifyDate
	function getdtmModifyDate()
	{
		$this->validate();
		return $this->dtmModifyDate;
	}
	
	//retun intCurrency
	function getintCurrency()
	{
		$this->validate();
		return $this->intCurrency;
	}
	
	//retun intInspectionItem
	function getintInspectionItem()
	{
		$this->validate();
		return $this->intInspectionItem;
	}
	
	//retun intTechniqueId
	function getintTechniqueId()
	{
		$this->validate();
		return $this->intTechniqueId;
	}
	
	//retun EXPIRERY_ITEM
	function getEXPIRERY_ITEM()
	{
		$this->validate();
		return $this->EXPIRERY_ITEM;
	}
	
	//retun DEPRECIATION_RATE
	function getDEPRECIATION_RATE()
	{
		$this->validate();
		return $this->DEPRECIATION_RATE;
	}
	
	//validate primary values
	private function validate()
	{
		if($this->intId=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
		}
	}
	
	public function set($intId)
	{
		$result = $this->select('*',null,"intId='$intId'");	
		if($this->db->numRows()>0)
			$this->setVariables($result);
		else
			throw new Exception("Record not found.");
			
	}
	

	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('intId,strName',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intId'])
				$html .= '<option selected="selected" value="'.$row['intId'].'">'.$row['strName'].'</option>';	
			else
				$html .= '<option value="'.$row['intId'].'">'.$row['strName'].'</option>';	
		}
		return $html;
	}
	

	public function setCodeToGetData($code){
	
		$result = $this->select('*',null,"strCode='$code'");	
		if($this->db->numRows()>0)
			$this->setVariables($result);
		else
			throw new Exception("Record not found.");
		
	}
	
	
}
?>

<?php
ini_set('display_errors',1);
class trn_sales_order_open_requisition_details{
 
	private $db;
	private $table= "trn_sales_order_open_requisition_details";
	
	//private property
	private $REQUISITION_YEAR;
	private $REQUISITION_NO;
	private $ORDER_YEAR;
	private $ORDER_NO;
	private $SALES_ORDER_ID;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('REQUISITION_YEAR'=>'REQUISITION_YEAR',
										'REQUISITION_NO'=>'REQUISITION_NO',
										'ORDER_YEAR'=>'ORDER_YEAR',
										'ORDER_NO'=>'ORDER_NO',
										'SALES_ORDER_ID'=>'SALES_ORDER_ID',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "REQUISITION_YEAR = ".$this->REQUISITION_YEAR." and REQUISITION_NO = ".$this->REQUISITION_NO." and ORDER_YEAR = ".$this->ORDER_YEAR." and ORDER_NO = ".$this->ORDER_NO." and SALES_ORDER_ID = ".$this->SALES_ORDER_ID."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun REQUISITION_YEAR
	function getREQUISITION_YEAR()
	{
		$this->validate();
		return $this->REQUISITION_YEAR;
	}
	
	//retun REQUISITION_NO
	function getREQUISITION_NO()
	{
		$this->validate();
		return $this->REQUISITION_NO;
	}
	
	//retun ORDER_YEAR
	function getORDER_YEAR()
	{
		$this->validate();
		return $this->ORDER_YEAR;
	}
	
	//retun ORDER_NO
	function getORDER_NO()
	{
		$this->validate();
		return $this->ORDER_NO;
	}
	
	//retun SALES_ORDER_ID
	function getSALES_ORDER_ID()
	{
		$this->validate();
		return $this->SALES_ORDER_ID;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set REQUISITION_YEAR
	function setREQUISITION_YEAR($REQUISITION_YEAR)
	{
		array_push($this->commitArray,'REQUISITION_YEAR');
		$this->REQUISITION_YEAR = $REQUISITION_YEAR;
	}
	
	//set REQUISITION_NO
	function setREQUISITION_NO($REQUISITION_NO)
	{
		array_push($this->commitArray,'REQUISITION_NO');
		$this->REQUISITION_NO = $REQUISITION_NO;
	}
	
	//set ORDER_YEAR
	function setORDER_YEAR($ORDER_YEAR)
	{
		array_push($this->commitArray,'ORDER_YEAR');
		$this->ORDER_YEAR = $ORDER_YEAR;
	}
	
	//set ORDER_NO
	function setORDER_NO($ORDER_NO)
	{
		array_push($this->commitArray,'ORDER_NO');
		$this->ORDER_NO = $ORDER_NO;
	}
	
	//set SALES_ORDER_ID
	function setSALES_ORDER_ID($SALES_ORDER_ID)
	{
		array_push($this->commitArray,'SALES_ORDER_ID');
		$this->SALES_ORDER_ID = $SALES_ORDER_ID;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->REQUISITION_YEAR=='' || $this->REQUISITION_NO=='' || $this->ORDER_YEAR=='' || $this->ORDER_NO=='' || $this->SALES_ORDER_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($REQUISITION_YEAR , $REQUISITION_NO , $ORDER_YEAR , $ORDER_NO , $SALES_ORDER_ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "REQUISITION_YEAR='$REQUISITION_YEAR' and REQUISITION_NO='$REQUISITION_NO' and ORDER_YEAR='$ORDER_YEAR' and ORDER_NO='$ORDER_NO' and SALES_ORDER_ID='$SALES_ORDER_ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($REQUISITION_YEAR,$REQUISITION_NO,$ORDER_YEAR,$ORDER_NO,$SALES_ORDER_ID){
		$data = array('REQUISITION_YEAR'=>$REQUISITION_YEAR 
				,'REQUISITION_NO'=>$REQUISITION_NO 
				,'ORDER_YEAR'=>$ORDER_YEAR 
				,'ORDER_NO'=>$ORDER_NO 
				,'SALES_ORDER_ID'=>$SALES_ORDER_ID 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('REQUISITION_YEAR,REQUISITION_NO',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['REQUISITION_YEAR'])
				$html .= '<option selected="selected" value="'.$row['REQUISITION_YEAR'].'">'.$row['REQUISITION_NO'].'</option>';	
			else
				$html .= '<option value="'.$row['REQUISITION_YEAR'].'">'.$row['REQUISITION_NO'].'</option>';	
		}
		return $html;
	}
	
	public function getPendingRequisitionsNos($year,$defaultValue){

		$cols	= "
 					trn_sales_order_open_requisition_details.REQUISITION_NO ";		
		$join	= "
					INNER JOIN trn_sales_order_open_requisition_header ON trn_sales_order_open_requisition_details.REQUISITION_YEAR = trn_sales_order_open_requisition_header.REQUISITION_YEAR AND trn_sales_order_open_requisition_details.REQUISITION_NO = trn_sales_order_open_requisition_header.REQUISITION_NO
					left JOIN trn_sales_order_open_header ON trn_sales_order_open_requisition_details.REQUISITION_YEAR = trn_sales_order_open_header.OPEN_YEAR AND trn_sales_order_open_requisition_details.REQUISITION_NO = trn_sales_order_open_header.REQUISITION_NO AND trn_sales_order_open_header.`STATUS` >0 
					left JOIN trn_sales_order_open_details ON trn_sales_order_open_header.REQUISITION_NO = trn_sales_order_open_details.OPEN_NO AND trn_sales_order_open_header.OPEN_YEAR = trn_sales_order_open_details.OPEN_YEAR AND trn_sales_order_open_requisition_details.ORDER_YEAR = trn_sales_order_open_details.ORDER_YEAR AND trn_sales_order_open_requisition_details.ORDER_NO = trn_sales_order_open_details.ORDER_NO AND trn_sales_order_open_requisition_details.SALES_ORDER_ID = trn_sales_order_open_details.SALE_ORDER_ID ";
		
		$where	= " trn_sales_order_open_header.REQUISITION_NO IS NULL AND 
					trn_sales_order_open_requisition_header.STATUS = 1 AND 
					trn_sales_order_open_requisition_header.REQUISITION_YEAR = '$year'
					GROUP BY
					trn_sales_order_open_requisition_details.REQUISITION_NO 
					ORDER BY 
 					trn_sales_order_open_requisition_details.REQUISITION_NO ASC
					";
			echo $sql	="select ".$cols." from trn_sales_order_open_requisition_details ".$join." where".$where;
	
		$result = $this->select($cols,$join,$where);	

		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['REQUISITION_NO'])
				$html .= '<option selected="selected" value="'.$row['REQUISITION_NO'].'">'.$row['REQUISITION_YEAR']."/".$row['REQUISITION_NO'].'</option>';	
			else
				$html .= '<option value="'.$row['REQUISITION_NO'].'">'.$row['REQUISITION_NO'].'</option>';	
		}
		return $html;
	}
	
	public function getPendingRequisitionsYears($defaultValue){

		$cols	= "
					trn_sales_order_open_requisition_details.REQUISITION_YEAR ";		
		$join	= "
					INNER JOIN trn_sales_order_open_requisition_header ON trn_sales_order_open_requisition_details.REQUISITION_YEAR = trn_sales_order_open_requisition_header.REQUISITION_YEAR AND trn_sales_order_open_requisition_details.REQUISITION_NO = trn_sales_order_open_requisition_header.REQUISITION_NO
					left JOIN trn_sales_order_open_header ON trn_sales_order_open_requisition_details.REQUISITION_YEAR = trn_sales_order_open_header.REQUISITION_YEAR AND trn_sales_order_open_requisition_details.REQUISITION_NO = trn_sales_order_open_header.REQUISITION_NO AND trn_sales_order_open_header.`STATUS` >0 
					left JOIN trn_sales_order_open_details ON trn_sales_order_open_header.OPEN_NO = trn_sales_order_open_details.OPEN_NO AND trn_sales_order_open_header.OPEN_YEAR = trn_sales_order_open_details.OPEN_YEAR AND trn_sales_order_open_requisition_details.ORDER_YEAR = trn_sales_order_open_details.ORDER_YEAR AND trn_sales_order_open_requisition_details.ORDER_NO = trn_sales_order_open_details.ORDER_NO AND trn_sales_order_open_requisition_details.SALES_ORDER_ID = trn_sales_order_open_details.SALE_ORDER_ID ";
		
		$where	= "trn_sales_order_open_header.REQUISITION_NO IS NULL AND 
					trn_sales_order_open_requisition_header.STATUS = 1  
					GROUP BY
					trn_sales_order_open_requisition_details.REQUISITION_YEAR 
					ORDER BY 
					trn_sales_order_open_requisition_details.REQUISITION_YEAR DESC 
					";
		
	//	echo $sql	="select ".$cols." from trn_sales_order_open_requisition_details ".$join." where".$where;
		$result = $this->select($cols,$join,$where);	

		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['REQUISITION_YEAR'])
				$html .= '<option selected="selected" value="'.$row['REQUISITION_YEAR'].'">'.$row['REQUISITION_YEAR'].'</option>';	
			else
				$html .= '<option value="'.$row['REQUISITION_YEAR'].'">'.$row['REQUISITION_YEAR'].'</option>';	
		}
		return $html;
	}


	public function getRequestedOrderYear($reqYear,$reqNo,$defaultValue){

		$cols	= "
					trn_sales_order_open_requisition_details.ORDER_YEAR ";		
		$join	= "
					INNER JOIN trn_sales_order_open_requisition_header ON trn_sales_order_open_requisition_details.REQUISITION_YEAR = trn_sales_order_open_requisition_header.REQUISITION_YEAR AND trn_sales_order_open_requisition_details.REQUISITION_NO = trn_sales_order_open_requisition_header.REQUISITION_NO
 ";
		
		$where	= "trn_sales_order_open_requisition_header.REQUISITION_YEAR ='$reqYear' AND 
					trn_sales_order_open_requisition_header.REQUISITION_NO = '$reqNo'  
					GROUP BY
					trn_sales_order_open_requisition_details.ORDER_YEAR 
					ORDER BY 
					trn_sales_order_open_requisition_details.ORDER_YEAR DESC 
					";
		
		$result = $this->select($cols,$join,$where);	

		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['ORDER_YEAR'])
				$html .= '<option selected="selected" value="'.$row['ORDER_YEAR'].'">'.$row['ORDER_YEAR'].'</option>';	
			else
				$html .= '<option value="'.$row['ORDER_YEAR'].'">'.$row['ORDER_YEAR'].'</option>';	
		}
		return $html;
	}
	
	public function getRequestedOrderNo($reqYear,$reqNo,$defaultValue){

		$cols	= "
					trn_sales_order_open_requisition_details.ORDER_NO ";		
		$join	= "
					INNER JOIN trn_sales_order_open_requisition_header ON trn_sales_order_open_requisition_details.REQUISITION_YEAR = trn_sales_order_open_requisition_header.REQUISITION_YEAR AND trn_sales_order_open_requisition_details.REQUISITION_NO = trn_sales_order_open_requisition_header.REQUISITION_NO
 ";
		
		$where	= "trn_sales_order_open_requisition_header.REQUISITION_YEAR ='$reqYear' AND 
					trn_sales_order_open_requisition_header.REQUISITION_NO = '$reqNo'  
					GROUP BY
					trn_sales_order_open_requisition_details.ORDER_NO 
					ORDER BY 
					trn_sales_order_open_requisition_details.ORDER_NO DESC 
					";
		
		$result = $this->select($cols,$join,$where);	

		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['ORDER_NO'])
				$html .= '<option selected="selected" value="'.$row['ORDER_NO'].'">'.$row['ORDER_NO'].'</option>';	
			else
				$html .= '<option value="'.$row['ORDER_NO'].'">'.$row['ORDER_NO'].'</option>';	
		}
		return $html;
	}
	
	public function getRequestedOrderDetails($reqYear,$reqNo,$serialNo,$serialYear){

		
		
		$cols	= "
					trn_orderdetails.strSalesOrderNo,intSalesOrderId,
					trn_orderdetails.strGraphicNo,
					trn_orderdetails.intSampleNo,
					trn_orderdetails.intSampleYear,
					trn_orderdetails.strCombo,
					trn_orderdetails.strLineNo,
					trn_orderdetails.strStyleNo,
					trn_orderdetails.strPrintName,
					trn_orderdetails.intRevisionNo,
					trn_orderdetails.intPart,
					trn_orderdetails.intQty,
					trn_orderdetails.dblPrice,
					trn_orderdetails.dblOverCutPercentage,
					trn_orderdetails.dblDamagePercentage,
					trn_orderdetails.dtPSD,
					trn_orderdetails.dtDeliveryDate,
					trn_orderdetails.TECHNIQUE_GROUP_ID,
					if(trn_orderdetails.`STATUS`=-10,'Closed','Open') as STATUS,
					if(trn_sales_order_open_requisition_details.`SALES_ORDER_ID` IS NOT NULL,1,0) as SAVED_STATUS 
				 ";		
		$join	= " left JOIN trn_sales_order_open_requisition_header ON trn_sales_order_open_requisition_details.REQUISITION_YEAR = trn_sales_order_open_requisition_header.REQUISITION_YEAR AND trn_sales_order_open_requisition_details.REQUISITION_NO = trn_sales_order_open_requisition_header.REQUISITION_NO ";
		if($serialNo=='' || $serialYear==''){
		$join	.=" AND trn_sales_order_open_requisition_header.`STATUS` >0  ";
	}
 		$join	.=" INNER JOIN trn_orderdetails ON trn_orderdetails.intOrderNo = trn_sales_order_open_requisition_details.ORDER_NO AND trn_orderdetails.intOrderYear = trn_sales_order_open_requisition_details.ORDER_YEAR AND trn_orderdetails.intSalesOrderId = trn_sales_order_open_requisition_details.SALES_ORDER_ID
			
					";
		
		if($serialNo=='' || $serialYear==''){
		$where	.= "trn_orderdetails.STATUS =-10 AND 
					trn_sales_order_open_requisition_header.STATUS = 1 AND 
					trn_sales_order_open_requisition_header.REQUISITION_YEAR = '$reqYear' AND 
					trn_sales_order_open_requisition_header.REQUISITION_NO = '$reqNo' ";
		}
		else{			
		$where	.= "trn_sales_order_open_requisition_header.REQUISITION_NO = '$serialNo' AND 
					trn_sales_order_open_requisition_header.REQUISITION_YEAR = '$serialYear' ";
		}
					
		$where	.= "GROUP BY
					trn_sales_order_open_requisition_details.REQUISITION_YEAR, 
					trn_sales_order_open_requisition_details.REQUISITION_NO, 
					trn_sales_order_open_requisition_details.ORDER_YEAR, 
					trn_sales_order_open_requisition_details.ORDER_NO, 
					trn_sales_order_open_requisition_details.SALES_ORDER_ID 
					ORDER BY 
					trn_sales_order_open_requisition_details.REQUISITION_YEAR ASC, 
					trn_sales_order_open_requisition_details.REQUISITION_NO ASC, 
					trn_sales_order_open_requisition_details.ORDER_YEAR ASC, 
					trn_sales_order_open_requisition_details.ORDER_NO ASC, 
					trn_sales_order_open_requisition_details.SALES_ORDER_ID ASC ";
	 	//echo $sql	="select ".$cols." from trn_sales_order_open_requisition_details ".$join." where ".$where;
		return $result = $this->select($cols,$join,$where);	
	}

	public function getSavedDetails($serialNo , $serialYear)
	{
		$cols	= "
					trn_orderdetails.intOrderNo,
					trn_orderdetails.intOrderYear,
					trn_orderdetails.strSalesOrderNo,
					trn_orderdetails.intSalesOrderId,
					trn_orderdetails.strGraphicNo,
					trn_orderdetails.intSampleNo,
					trn_orderdetails.intSampleYear,
					trn_orderdetails.strCombo,
					trn_orderdetails.strLineNo,
					trn_orderdetails.strStyleNo,
					trn_orderdetails.strPrintName,
					trn_orderdetails.intRevisionNo,
					trn_orderdetails.intPart,
					trn_orderdetails.intQty,
					trn_orderdetails.dblPrice,
					trn_orderdetails.dblOverCutPercentage,
					trn_orderdetails.dblDamagePercentage,
					trn_orderdetails.dtPSD,
					trn_orderdetails.dtDeliveryDate,
					trn_orderdetails.TECHNIQUE_GROUP_ID,
					if(trn_orderdetails.`STATUS`=-10,'Closed','Open') as STATUS,
					if(trn_sales_order_open_requisition_details.`SALES_ORDER_ID` IS NOT NULL,1,0) as SAVED_STATUS 
				 ";		

 		$join	.=" INNER JOIN trn_sales_order_open_requisition_header ON trn_sales_order_open_requisition_header.REQUISITION_NO = trn_sales_order_open_requisition_details.REQUISITION_NO AND trn_sales_order_open_requisition_header.REQUISITION_YEAR = trn_sales_order_open_requisition_details.REQUISITION_YEAR 
					INNER JOIN trn_orderdetails ON trn_orderdetails.intOrderNo = trn_sales_order_open_requisition_details.ORDER_NO AND trn_orderdetails.intOrderYear = trn_sales_order_open_requisition_details.ORDER_YEAR AND trn_orderdetails.intSalesOrderId = trn_sales_order_open_requisition_details.SALES_ORDER_ID
			
					";
		
		$where	= "trn_sales_order_open_requisition_header.REQUISITION_NO = '$serialNo' AND 
					trn_sales_order_open_requisition_header.REQUISITION_YEAR = '$serialYear' ";
					
		$where	.= "GROUP BY
					trn_sales_order_open_requisition_details.ORDER_YEAR, 
					trn_sales_order_open_requisition_details.ORDER_NO, 
					trn_sales_order_open_requisition_details.SALES_ORDER_ID 
					ORDER BY 
					trn_sales_order_open_requisition_details.ORDER_YEAR ASC, 
					trn_sales_order_open_requisition_details.ORDER_NO ASC, 
					trn_sales_order_open_requisition_details.SALES_ORDER_ID ASC ";
		//echo $sql	="select ".$cols." from trn_sales_order_open_requisition_details ".$join." where ".$where;
//$cols = 1; $join='' ; $where = '';
		
		return $result = $this->select($cols,$join,$where);	
	}	

	//END }
}
?>
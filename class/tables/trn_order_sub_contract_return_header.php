
<?php

class trn_order_sub_contract_return_header{
 
	private $db;
	private $table= "trn_order_sub_contract_return_header";
	
	//private property
	private $SUB_CONTRACT_RETURN_NO;
	private $SUB_CONTRACT_RETURN_YEAR;
	private $SUB_CONTRACT_GP_NO;
	private $SUB_CONTRACT_GP_YEAR;
	private $COMPANY_ID;
	private $LOCATION_ID;
	private $REMARKS;
	private $STATUS;
	private $LEVELS;
	private $DATE;
	private $CREATED_DATE;
	private $CREATED_BY;
	private $MODIFIED_DATE;
	private $MODIFIED_BY;
	private $SUB_CONTRACT_NO;
	private $SUB_CONTRACT_YEAR;
	private $ORDER_NO;
	private $ORDER_YEAR;
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->db->field_array_set(array('SUB_CONTRACT_RETURN_NO'=>'SUB_CONTRACT_RETURN_NO',
										'SUB_CONTRACT_RETURN_YEAR'=>'SUB_CONTRACT_RETURN_YEAR',
										'SUB_CONTRACT_GP_NO'=>'SUB_CONTRACT_GP_NO',
										'SUB_CONTRACT_GP_YEAR'=>'SUB_CONTRACT_GP_YEAR',
										'COMPANY_ID'=>'COMPANY_ID',
										'LOCATION_ID'=>'LOCATION_ID',
										'REMARKS'=>'REMARKS',
										'STATUS'=>'STATUS',
										'LEVELS'=>'LEVELS',
										'DATE'=>'DATE',
										'CREATED_DATE'=>'CREATED_DATE',
										'CREATED_BY'=>'CREATED_BY',
										'MODIFIED_DATE'=>'MODIFIED_DATE',
										'MODIFIED_BY'=>'MODIFIED_BY',
										));
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  
		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	//public functions for private properties
	
	//retun SUB_CONTRACT_RETURN_NO
	function getSUB_CONTRACT_RETURN_NO()
	{
		$this->validate();
		return $this->SUB_CONTRACT_RETURN_NO;
	}
	
	//retun SUB_CONTRACT_RETURN_YEAR
	function getSUB_CONTRACT_RETURN_YEAR()
	{
		$this->validate();
		return $this->SUB_CONTRACT_RETURN_YEAR;
	}
	
	//retun SUB_CONTRACT_GP_NO
	function getSUB_CONTRACT_GP_NO()
	{
		$this->validate();
		return $this->SUB_CONTRACT_GP_NO;
	}
	
	//retun SUB_CONTRACT_GP_YEAR
	function getSUB_CONTRACT_GP_YEAR()
	{
		$this->validate();
		return $this->SUB_CONTRACT_GP_YEAR;
	}
	
	//retun COMPANY_ID
	function getCOMPANY_ID()
	{
		$this->validate();
		return $this->COMPANY_ID;
	}
	
	//retun LOCATION_ID
	function getLOCATION_ID()
	{
		$this->validate();
		return $this->LOCATION_ID;
	}
	
	//retun REMARKS
	function getREMARKS()
	{
		$this->validate();
		return $this->REMARKS;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//retun LEVELS
	function getLEVELS()
	{
		$this->validate();
		return $this->LEVELS;
	}
	
	//retun DATE
	function getDATE()
	{
		$this->validate();
		return $this->DATE;
	}
	
	//retun CREATED_DATE
	function getCREATED_DATE()
	{
		$this->validate();
		return $this->CREATED_DATE;
	}
	
	//retun CREATED_BY
	function getCREATED_BY()
	{
		$this->validate();
		return $this->CREATED_BY;
	}
	
	//retun MODIFIED_DATE
	function getMODIFIED_DATE()
	{
		$this->validate();
		return $this->MODIFIED_DATE;
	}
	
	//retun MODIFIED_BY
	function getMODIFIED_BY()
	{
		$this->validate();
		return $this->MODIFIED_BY;
	}
	
	//retun SUB_CONTRACT_NO
	function getSUB_CONTRACT_NO()
	{
		$this->validate();
		return $this->SUB_CONTRACT_NO;
	}
	
	//retun SUB_CONTRACT_YEAR
	function getSUB_CONTRACT_YEAR()
	{
		$this->validate();
		return $this->SUB_CONTRACT_YEAR;
	}
	
	//retun ORDER_NO
	function getORDER_NO()
	{
		$this->validate();
		return $this->ORDER_NO;
	}
	
	//retun ORDER_YEAR
	function getORDER_YEAR()
	{
		$this->validate();
		return $this->ORDER_YEAR;
	}
	
	//validate primary values
	private function validate()
	{
		if($this->SUB_CONTRACT_RETURN_NO=='' || $this->SUB_CONTRACT_RETURN_YEAR=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
		}
	}
	
	public function set($SUB_CONTRACT_RETURN_NO,$SUB_CONTRACT_RETURN_YEAR)
	{
		$cols	= " trn_order_sub_contract_return_header.SUB_CONTRACT_RETURN_NO,
					trn_order_sub_contract_return_header.SUB_CONTRACT_RETURN_YEAR,
					trn_order_sub_contract_return_header.SUB_CONTRACT_GP_NO,
					trn_order_sub_contract_return_header.SUB_CONTRACT_GP_YEAR,
					trn_order_sub_contract_return_header.REMARKS,
					trn_order_sub_contract_return_header.STATUS,
					trn_order_sub_contract_return_header.LEVELS,
					SCGPH.SUB_CONTRACT_NO,
					SCGPH.SUB_CONTRACT_YEAR,
					SCH.ORDER_NO,
					SCH.ORDER_YEAR ";
		
		$join	= " INNER JOIN trn_order_sub_contract_gate_pass_header SCGPH ON SCGPH.SUB_CONTRACT_GP_NO=trn_order_sub_contract_return_header.SUB_CONTRACT_GP_NO AND
					SCGPH.SUB_CONTRACT_GP_YEAR=trn_order_sub_contract_return_header.SUB_CONTRACT_GP_YEAR
					INNER JOIN trn_order_sub_contract_header SCH ON SCH.SUB_CONTRACT_NO=SCGPH.SUB_CONTRACT_NO AND
					SCH.SUB_CONTRACT_YEAR=SCGPH.SUB_CONTRACT_YEAR ";
		
		$where	= "	trn_order_sub_contract_return_header.SUB_CONTRACT_RETURN_NO = '".$SUB_CONTRACT_RETURN_NO."' AND
					trn_order_sub_contract_return_header.SUB_CONTRACT_RETURN_YEAR = '".$SUB_CONTRACT_RETURN_YEAR."' ";
		
		$result = $this->select($cols,$join,$where,$order = null, $limit = null);
		if($this->db->numRows()>0)
			$this->setVariables($result);
		else
			throw new Exception("Record not found.");
			
	}
	
	
}
?>

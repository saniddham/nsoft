<?php
class finance_cashflow_statement_cpanal{

	private $db;
	private $table= "finance_cashflow_statement_cpanal";

	//private property
	private $REPORT_ID;
	private $REPORT_CATEGORY_ID;
	private $REPORT_FIELD_NAME;
	private $CHART_OF_ACCOUNT_ID;
	private $TRANSACTION_PROGRAM_ID;
	private $ORDER_BY_ID;
	private $STATUS;
	private $CREATED_BY;
	private $CREATED_DATE;
	private $MODIFIED_BY;
	private $MODIFIED_DATE;
	private $commitArray = array();
	private $field_array = array();
	

	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('REPORT_ID'=>'REPORT_ID',
										'REPORT_CATEGORY_ID'=>'REPORT_CATEGORY_ID',
										'REPORT_FIELD_NAME'=>'REPORT_FIELD_NAME',
										'CHART_OF_ACCOUNT_ID'=>'CHART_OF_ACCOUNT_ID',
										'TRANSACTION_PROGRAM_ID'=>'TRANSACTION_PROGRAM_ID',
										'ORDER_BY_ID'=>'ORDER_BY_ID',
										'STATUS'=>'STATUS',
										'CREATED_BY'=>'CREATED_BY',
										'CREATED_DATE'=>'CREATED_DATE',
										'MODIFIED_BY'=>'MODIFIED_BY',
										'MODIFIED_DATE'=>'MODIFIED_DATE',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit );
		return $result;
	}

	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data);
		return $this->db->getResult();


	}

	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}

	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();
	}

	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}

	public function get_field_array(){
		return $this->field_array;
	}

	function commit($type='update')
	{
		//commit update
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{
			$data[$v] = $this->$v;
		}
		$where		= "REPORT_ID = ".$this->REPORT_ID."" ;
		$this->commitArray = array();

		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}

	//BEGIN - public functions for private properties {
	
	//retun REPORT_ID
	function getREPORT_ID()
	{
		$this->validate();
		return $this->REPORT_ID;
	}
	
	//retun REPORT_CATEGORY_ID
	function getREPORT_CATEGORY_ID()
	{
		$this->validate();
		return $this->REPORT_CATEGORY_ID;
	}
	
	//retun REPORT_FIELD_NAME
	function getREPORT_FIELD_NAME()
	{
		$this->validate();
		return $this->REPORT_FIELD_NAME;
	}
	
	//retun CHART_OF_ACCOUNT_ID
	function getCHART_OF_ACCOUNT_ID()
	{
		$this->validate();
		return $this->CHART_OF_ACCOUNT_ID;
	}
	
	//retun TRANSACTION_PROGRAM_ID
	function getTRANSACTION_PROGRAM_ID()
	{
		$this->validate();
		return $this->TRANSACTION_PROGRAM_ID;
	}
	
	//retun ORDER_BY_ID
	function getORDER_BY_ID()
	{
		$this->validate();
		return $this->ORDER_BY_ID;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//retun CREATED_BY
	function getCREATED_BY()
	{
		$this->validate();
		return $this->CREATED_BY;
	}
	
	//retun CREATED_DATE
	function getCREATED_DATE()
	{
		$this->validate();
		return $this->CREATED_DATE;
	}
	
	//retun MODIFIED_BY
	function getMODIFIED_BY()
	{
		$this->validate();
		return $this->MODIFIED_BY;
	}
	
	//retun MODIFIED_DATE
	function getMODIFIED_DATE()
	{
		$this->validate();
		return $this->MODIFIED_DATE;
	}
	
	//END }

	//BEGIN - public function set {
	
	//set REPORT_ID
	function setREPORT_ID($REPORT_ID)
	{
		array_push($this->commitArray,'REPORT_ID');
		$this->REPORT_ID = $REPORT_ID;
	}
	
	//set REPORT_CATEGORY_ID
	function setREPORT_CATEGORY_ID($REPORT_CATEGORY_ID)
	{
		array_push($this->commitArray,'REPORT_CATEGORY_ID');
		$this->REPORT_CATEGORY_ID = $REPORT_CATEGORY_ID;
	}
	
	//set REPORT_FIELD_NAME
	function setREPORT_FIELD_NAME($REPORT_FIELD_NAME)
	{
		array_push($this->commitArray,'REPORT_FIELD_NAME');
		$this->REPORT_FIELD_NAME = $REPORT_FIELD_NAME;
	}
	
	//set CHART_OF_ACCOUNT_ID
	function setCHART_OF_ACCOUNT_ID($CHART_OF_ACCOUNT_ID)
	{
		array_push($this->commitArray,'CHART_OF_ACCOUNT_ID');
		$this->CHART_OF_ACCOUNT_ID = $CHART_OF_ACCOUNT_ID;
	}
	
	//set TRANSACTION_PROGRAM_ID
	function setTRANSACTION_PROGRAM_ID($TRANSACTION_PROGRAM_ID)
	{
		array_push($this->commitArray,'TRANSACTION_PROGRAM_ID');
		$this->TRANSACTION_PROGRAM_ID = $TRANSACTION_PROGRAM_ID;
	}
	
	//set ORDER_BY_ID
	function setORDER_BY_ID($ORDER_BY_ID)
	{
		array_push($this->commitArray,'ORDER_BY_ID');
		$this->ORDER_BY_ID = $ORDER_BY_ID;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//set CREATED_BY
	function setCREATED_BY($CREATED_BY)
	{
		array_push($this->commitArray,'CREATED_BY');
		$this->CREATED_BY = $CREATED_BY;
	}
	
	//set CREATED_DATE
	function setCREATED_DATE($CREATED_DATE)
	{
		array_push($this->commitArray,'CREATED_DATE');
		$this->CREATED_DATE = $CREATED_DATE;
	}
	
	//set MODIFIED_BY
	function setMODIFIED_BY($MODIFIED_BY)
	{
		array_push($this->commitArray,'MODIFIED_BY');
		$this->MODIFIED_BY = $MODIFIED_BY;
	}
	
	//set MODIFIED_DATE
	function setMODIFIED_DATE($MODIFIED_DATE)
	{
		array_push($this->commitArray,'MODIFIED_DATE');
		$this->MODIFIED_DATE = $MODIFIED_DATE;
	}
	
	//END }

	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->REPORT_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}

	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}

	public function set($REPORT_ID)
	{
		$cols	= "*";

		$join	= NULL;

		$where	= "REPORT_ID='$REPORT_ID'";

		$result = $this->select($cols,$join,$where);
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}

	//insert as parameters
	public function insertRec($REPORT_CATEGORY_ID,$REPORT_FIELD_NAME,$CHART_OF_ACCOUNT_ID,$TRANSACTION_PROGRAM_ID,$ORDER_BY_ID,$STATUS,$CREATED_BY,$CREATED_DATE,$MODIFIED_BY,$MODIFIED_DATE){
		$data = array('REPORT_CATEGORY_ID'=>$REPORT_CATEGORY_ID 
				,'REPORT_FIELD_NAME'=>$REPORT_FIELD_NAME 
				,'CHART_OF_ACCOUNT_ID'=>$CHART_OF_ACCOUNT_ID 
				,'TRANSACTION_PROGRAM_ID'=>$TRANSACTION_PROGRAM_ID 
				,'ORDER_BY_ID'=>$ORDER_BY_ID 
				,'STATUS'=>$STATUS 
				,'CREATED_BY'=>$CREATED_BY 
				,'CREATED_DATE'=>$CREATED_DATE 
				,'MODIFIED_BY'=>$MODIFIED_BY 
				,'MODIFIED_DATE'=>$MODIFIED_DATE 
				);
		return $this->insert($data);
	}

	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('REPORT_ID,REPORT_FIELD_NAME',  null, $where = $where);
		$html = '<option value=""></option>';
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['REPORT_ID'])
				$html .= '<option selected="selected" value="'.$row['REPORT_ID'].'">'.$row['REPORT_FIELD_NAME'].'</option>';
			else
				$html .= '<option value="'.$row['REPORT_ID'].'">'.$row['REPORT_FIELD_NAME'].'</option>';
		}
		return $html;
	}
	
	//END }
	
#BEGIN  - USER DEFINED FUNCTIONS {
	public function getDetails($reportCategoryId)
	{
		$cols	= "REPORT_ID,
				   REPORT_CATEGORY_ID,
				   REPORT_FIELD_NAME,
				   CHART_OF_ACCOUNT_ID,
				   TRANSACTION_PROGRAM_ID,
				   ORDER_BY_ID";

		$where 	= "STATUS = 1
				   AND REPORT_CATEGORY_ID = $reportCategoryId";
		
		$order 	= "ORDER_BY_ID";
		return $this->select($cols,$join=null,$where,$order,$limit=null);		
	}
	
	public function getMaxRowCount($reportCategoryId)
	{
		$cols	= "COUNT(ORDER_BY_ID) AS MAX_COUNT";

		$where 	= "REPORT_CATEGORY_ID = $reportCategoryId";
		$result = $this->select($cols,$join=null,$where,$order=null,$limit=null);		
		$row	= mysqli_fetch_array($result);
		return $row['MAX_COUNT'];
	}
#END 	- USER DEFINED FUNCTIONS }
}
?>
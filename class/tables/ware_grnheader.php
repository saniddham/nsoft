<?php
class ware_grnheader{
 
	private $db;
	private $table= "ware_grnheader";
	
	//private property
	private $intGrnNo;
	private $intGrnYear;
	private $intPoNo;
	private $intPoYear;
	private $dblExchangeRate;
	private $strInvoiceNo;
	private $strDeliveryNo;
	private $intStatus;
	private $intApproveLevels;
	private $datdate;
	private $dtmCreateDate;
	private $intUser;
	private $intCompanyId;
	private $intModifiedBy;
	private $dtmModifiedDate;
	private $intGrnRateApproval;
	private $intGrnRateApprovedBy;
	private $intGrnRateApprovedDate;
	private $intPayStatus;
	private $strReferenceNo;
	private $PRINT_COUNT;
	private $SUPPLIER_INVOICE_DATE;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('intGrnNo'=>'intGrnNo',
										'intGrnYear'=>'intGrnYear',
										'intPoNo'=>'intPoNo',
										'intPoYear'=>'intPoYear',
										'dblExchangeRate'=>'dblExchangeRate',
										'strInvoiceNo'=>'strInvoiceNo',
										'strDeliveryNo'=>'strDeliveryNo',
										'intStatus'=>'intStatus',
										'intApproveLevels'=>'intApproveLevels',
										'datdate'=>'datdate',
										'dtmCreateDate'=>'dtmCreateDate',
										'intUser'=>'intUser',
										'intCompanyId'=>'intCompanyId',
										'intModifiedBy'=>'intModifiedBy',
										'dtmModifiedDate'=>'dtmModifiedDate',
										'intGrnRateApproval'=>'intGrnRateApproval',
										'intGrnRateApprovedBy'=>'intGrnRateApprovedBy',
										'intGrnRateApprovedDate'=>'intGrnRateApprovedDate',
										'intPayStatus'=>'intPayStatus',
										'strReferenceNo'=>'strReferenceNo',
										'PRINT_COUNT'=>'PRINT_COUNT',
										'SUPPLIER_INVOICE_DATE'=>'SUPPLIER_INVOICE_DATE',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	function commit()
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "intGrnNo = ".$this->intGrnNo." and intGrnYear = ".$this->intGrnYear."" ;
		unset($this->commitArray);
		
		if($this->intId==NULL || $this->intId=='' || $this->intId=='NULL')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun intGrnNo
	function getintGrnNo()
	{
		$this->validate();
		return $this->intGrnNo;
	}
	
	//retun intGrnYear
	function getintGrnYear()
	{
		$this->validate();
		return $this->intGrnYear;
	}
	
	//retun intPoNo
	function getintPoNo()
	{
		$this->validate();
		return $this->intPoNo;
	}
	
	//retun intPoYear
	function getintPoYear()
	{
		$this->validate();
		return $this->intPoYear;
	}
	
	//retun dblExchangeRate
	function getdblExchangeRate()
	{
		$this->validate();
		return $this->dblExchangeRate;
	}
	
	//retun strInvoiceNo
	function getstrInvoiceNo()
	{
		$this->validate();
		return $this->strInvoiceNo;
	}
	
	//retun strDeliveryNo
	function getstrDeliveryNo()
	{
		$this->validate();
		return $this->strDeliveryNo;
	}
	
	//retun intStatus
	function getintStatus()
	{
		$this->validate();
		return $this->intStatus;
	}
	
	//retun intApproveLevels
	function getintApproveLevels()
	{
		$this->validate();
		return $this->intApproveLevels;
	}
	
	//retun datdate
	function getdatdate()
	{
		$this->validate();
		return $this->datdate;
	}
	
	//retun dtmCreateDate
	function getdtmCreateDate()
	{
		$this->validate();
		return $this->dtmCreateDate;
	}
	
	//retun intUser
	function getintUser()
	{
		$this->validate();
		return $this->intUser;
	}
	
	//retun intCompanyId
	function getintCompanyId()
	{
		$this->validate();
		return $this->intCompanyId;
	}
	
	//retun intModifiedBy
	function getintModifiedBy()
	{
		$this->validate();
		return $this->intModifiedBy;
	}
	
	//retun dtmModifiedDate
	function getdtmModifiedDate()
	{
		$this->validate();
		return $this->dtmModifiedDate;
	}
	
	//retun intGrnRateApproval
	function getintGrnRateApproval()
	{
		$this->validate();
		return $this->intGrnRateApproval;
	}
	
	//retun intGrnRateApprovedBy
	function getintGrnRateApprovedBy()
	{
		$this->validate();
		return $this->intGrnRateApprovedBy;
	}
	
	//retun intGrnRateApprovedDate
	function getintGrnRateApprovedDate()
	{
		$this->validate();
		return $this->intGrnRateApprovedDate;
	}
	
	//retun intPayStatus
	function getintPayStatus()
	{
		$this->validate();
		return $this->intPayStatus;
	}
	
	//retun strReferenceNo
	function getstrReferenceNo()
	{
		$this->validate();
		return $this->strReferenceNo;
	}
	
	//retun PRINT_COUNT
	function getPRINT_COUNT()
	{
		$this->validate();
		return $this->PRINT_COUNT;
	}
	
	//retun SUPPLIER_INVOICE_DATE
	function getSUPPLIER_INVOICE_DATE()
	{
		$this->validate();
		return $this->SUPPLIER_INVOICE_DATE;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set intGrnNo
	function setintGrnNo($intGrnNo)
	{
		array_push($this->commitArray,'intGrnNo');
		$this->intGrnNo = $intGrnNo;
	}
	
	//set intGrnYear
	function setintGrnYear($intGrnYear)
	{
		array_push($this->commitArray,'intGrnYear');
		$this->intGrnYear = $intGrnYear;
	}
	
	//set intPoNo
	function setintPoNo($intPoNo)
	{
		array_push($this->commitArray,'intPoNo');
		$this->intPoNo = $intPoNo;
	}
	
	//set intPoYear
	function setintPoYear($intPoYear)
	{
		array_push($this->commitArray,'intPoYear');
		$this->intPoYear = $intPoYear;
	}
	
	//set dblExchangeRate
	function setdblExchangeRate($dblExchangeRate)
	{
		array_push($this->commitArray,'dblExchangeRate');
		$this->dblExchangeRate = $dblExchangeRate;
	}
	
	//set strInvoiceNo
	function setstrInvoiceNo($strInvoiceNo)
	{
		array_push($this->commitArray,'strInvoiceNo');
		$this->strInvoiceNo = $strInvoiceNo;
	}
	
	//set strDeliveryNo
	function setstrDeliveryNo($strDeliveryNo)
	{
		array_push($this->commitArray,'strDeliveryNo');
		$this->strDeliveryNo = $strDeliveryNo;
	}
	
	//set intStatus
	function setintStatus($intStatus)
	{
		array_push($this->commitArray,'intStatus');
		$this->intStatus = $intStatus;
	}
	
	//set intApproveLevels
	function setintApproveLevels($intApproveLevels)
	{
		array_push($this->commitArray,'intApproveLevels');
		$this->intApproveLevels = $intApproveLevels;
	}
	
	//set datdate
	function setdatdate($datdate)
	{
		array_push($this->commitArray,'datdate');
		$this->datdate = $datdate;
	}
	
	//set dtmCreateDate
	function setdtmCreateDate($dtmCreateDate)
	{
		array_push($this->commitArray,'dtmCreateDate');
		$this->dtmCreateDate = $dtmCreateDate;
	}
	
	//set intUser
	function setintUser($intUser)
	{
		array_push($this->commitArray,'intUser');
		$this->intUser = $intUser;
	}
	
	//set intCompanyId
	function setintCompanyId($intCompanyId)
	{
		array_push($this->commitArray,'intCompanyId');
		$this->intCompanyId = $intCompanyId;
	}
	
	//set intModifiedBy
	function setintModifiedBy($intModifiedBy)
	{
		array_push($this->commitArray,'intModifiedBy');
		$this->intModifiedBy = $intModifiedBy;
	}
	
	//set dtmModifiedDate
	function setdtmModifiedDate($dtmModifiedDate)
	{
		array_push($this->commitArray,'dtmModifiedDate');
		$this->dtmModifiedDate = $dtmModifiedDate;
	}
	
	//set intGrnRateApproval
	function setintGrnRateApproval($intGrnRateApproval)
	{
		array_push($this->commitArray,'intGrnRateApproval');
		$this->intGrnRateApproval = $intGrnRateApproval;
	}
	
	//set intGrnRateApprovedBy
	function setintGrnRateApprovedBy($intGrnRateApprovedBy)
	{
		array_push($this->commitArray,'intGrnRateApprovedBy');
		$this->intGrnRateApprovedBy = $intGrnRateApprovedBy;
	}
	
	//set intGrnRateApprovedDate
	function setintGrnRateApprovedDate($intGrnRateApprovedDate)
	{
		array_push($this->commitArray,'intGrnRateApprovedDate');
		$this->intGrnRateApprovedDate = $intGrnRateApprovedDate;
	}
	
	//set intPayStatus
	function setintPayStatus($intPayStatus)
	{
		array_push($this->commitArray,'intPayStatus');
		$this->intPayStatus = $intPayStatus;
	}
	
	//set strReferenceNo
	function setstrReferenceNo($strReferenceNo)
	{
		array_push($this->commitArray,'strReferenceNo');
		$this->strReferenceNo = $strReferenceNo;
	}
	
	//set PRINT_COUNT
	function setPRINT_COUNT($PRINT_COUNT)
	{
		array_push($this->commitArray,'PRINT_COUNT');
		$this->PRINT_COUNT = $PRINT_COUNT;
	}
	
	//set SUPPLIER_INVOICE_DATE
	function setSUPPLIER_INVOICE_DATE($SUPPLIER_INVOICE_DATE)
	{
		array_push($this->commitArray,'SUPPLIER_INVOICE_DATE');
		$this->SUPPLIER_INVOICE_DATE = $SUPPLIER_INVOICE_DATE;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->intGrnNo=='' || $this->intGrnYear=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($intGrnNo , $intGrnYear)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "intGrnNo='$intGrnNo' and intGrnYear='$intGrnYear'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($intGrnNo,$intGrnYear,$intPoNo,$intPoYear,$dblExchangeRate,$strInvoiceNo,$strDeliveryNo,$intStatus,$intApproveLevels,$datdate,$dtmCreateDate,$intUser,$intCompanyId,$intModifiedBy,$dtmModifiedDate,$intGrnRateApproval,$intGrnRateApprovedBy,$intGrnRateApprovedDate,$intPayStatus,$strReferenceNo,$PRINT_COUNT,$SUPPLIER_INVOICE_DATE){
		$data = array('intGrnNo'=>$intGrnNo 
				,'intGrnYear'=>$intGrnYear 
				,'intPoNo'=>$intPoNo 
				,'intPoYear'=>$intPoYear 
				,'dblExchangeRate'=>$dblExchangeRate 
				,'strInvoiceNo'=>$strInvoiceNo 
				,'strDeliveryNo'=>$strDeliveryNo 
				,'intStatus'=>$intStatus 
				,'intApproveLevels'=>$intApproveLevels 
				,'datdate'=>$datdate 
				,'dtmCreateDate'=>$dtmCreateDate 
				,'intUser'=>$intUser 
				,'intCompanyId'=>$intCompanyId 
				,'intModifiedBy'=>$intModifiedBy 
				,'dtmModifiedDate'=>$dtmModifiedDate 
				,'intGrnRateApproval'=>$intGrnRateApproval 
				,'intGrnRateApprovedBy'=>$intGrnRateApprovedBy 
				,'intGrnRateApprovedDate'=>$intGrnRateApprovedDate 
				,'intPayStatus'=>$intPayStatus 
				,'strReferenceNo'=>$strReferenceNo 
				,'PRINT_COUNT'=>$PRINT_COUNT 
				,'SUPPLIER_INVOICE_DATE'=>$SUPPLIER_INVOICE_DATE 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('intGrnNo,intGrnYear',  null, $where = $where);
		while($row=mysqli_fetch_array($result)){
			$html .= '<option value="'.$row['intGrnNo'].'">'.$row['intGrnYear'].'</option>';	
		}
		return $html;
	}
	
	//END }
}
?>
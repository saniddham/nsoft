<?php
class ink_foil_special_rm_actual_conpc_details{
 
	private $db;
	private $table= "ink_foil_special_rm_actual_conpc_details";
	
	//private property
	private $SERIAL_NO;
	private $SERIAL_YEAR;
	private $ITEM_ID;
	private $SIZE;
	private $CONSUMPTION;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('SERIAL_NO'=>'SERIAL_NO',
										'SERIAL_YEAR'=>'SERIAL_YEAR',
										'ITEM_ID'=>'ITEM_ID',
										'SIZE'=>'SIZE',
										'CONSUMPTION'=>'CONSUMPTION',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "SERIAL_NO = ".$this->SERIAL_NO." and SERIAL_YEAR = ".$this->SERIAL_YEAR." and ITEM_ID = ".$this->ITEM_ID." and SIZE = ".$this->SIZE."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun SERIAL_NO
	function getSERIAL_NO()
	{
		$this->validate();
		return $this->SERIAL_NO;
	}
	
	//retun SERIAL_YEAR
	function getSERIAL_YEAR()
	{
		$this->validate();
		return $this->SERIAL_YEAR;
	}
	
	//retun ITEM_ID
	function getITEM_ID()
	{
		$this->validate();
		return $this->ITEM_ID;
	}
	
	//retun SIZE
	function getSIZE()
	{
		$this->validate();
		return $this->SIZE;
	}
	
	//retun CONSUMPTION
	function getCONSUMPTION()
	{
		$this->validate();
		return $this->CONSUMPTION;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set SERIAL_NO
	function setSERIAL_NO($SERIAL_NO)
	{
		array_push($this->commitArray,'SERIAL_NO');
		$this->SERIAL_NO = $SERIAL_NO;
	}
	
	//set SERIAL_YEAR
	function setSERIAL_YEAR($SERIAL_YEAR)
	{
		array_push($this->commitArray,'SERIAL_YEAR');
		$this->SERIAL_YEAR = $SERIAL_YEAR;
	}
	
	//set ITEM_ID
	function setITEM_ID($ITEM_ID)
	{
		array_push($this->commitArray,'ITEM_ID');
		$this->ITEM_ID = $ITEM_ID;
	}
	
	//set SIZE
	function setSIZE($SIZE)
	{
		array_push($this->commitArray,'SIZE');
		$this->SIZE = $SIZE;
	}
	
	//set CONSUMPTION
	function setCONSUMPTION($CONSUMPTION)
	{
		array_push($this->commitArray,'CONSUMPTION');
		$this->CONSUMPTION = $CONSUMPTION;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->SERIAL_NO=='' || $this->SERIAL_YEAR=='' || $this->ITEM_ID=='' || $this->SIZE=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($SERIAL_NO , $SERIAL_YEAR , $ITEM_ID , $SIZE)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "SERIAL_NO='$SERIAL_NO' and SERIAL_YEAR='$SERIAL_YEAR' and ITEM_ID='$ITEM_ID' and SIZE='$SIZE'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($SERIAL_NO,$SERIAL_YEAR,$ITEM_ID,$SIZE,$CONSUMPTION){
		$data = array('SERIAL_NO'=>$SERIAL_NO 
				,'SERIAL_YEAR'=>$SERIAL_YEAR 
				,'ITEM_ID'=>$ITEM_ID 
				,'SIZE'=>$SIZE 
				,'CONSUMPTION'=>$CONSUMPTION 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('SERIAL_NO,ITEM_ID',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['SERIAL_NO'])
				$html .= '<option selected="selected" value="'.$row['SERIAL_NO'].'">'.$row['ITEM_ID'].'</option>';	
			else
				$html .= '<option value="'.$row['SERIAL_NO'].'">'.$row['ITEM_ID'].'</option>';	
		}
		return $html;
	}
	
	public function getDetailData($serialNo,$serialYear)
	{
		$cols	= " ITEM_ID,
					MI.strName AS ITEM_NAME,
					MI.intUOM,
					MU.strName AS UNIT ";
		
		$join	= " INNER JOIN mst_item MI ON MI.intId=ink_foil_special_rm_actual_conpc_details.ITEM_ID
					INNER JOIN mst_units MU ON MU.intId=MI.intUOM ";
		
		$where	= " ink_foil_special_rm_actual_conpc_details.SERIAL_NO = '".$serialNo."' AND ink_foil_special_rm_actual_conpc_details.SERIAL_YEAR = '".$serialYear."' GROUP BY ITEM_ID ";
		
		$order	= " MI.strName ";
		
		$result = $this->select($cols,$join,$where,$order, $limit = null);	
		
		return $result;
	}
	
	public function getSizeConsumption($serialNo,$serialYear,$item)
	{
		$cols	= " SIZE,
					CONSUMPTION ";
		
		$where	= " ink_foil_special_rm_actual_conpc_details.SERIAL_NO = '".$serialNo."' AND 
					ink_foil_special_rm_actual_conpc_details.SERIAL_YEAR = '".$serialYear."' AND
					ink_foil_special_rm_actual_conpc_details.ITEM_ID = '".$item."' ";
		
		$result = $this->select($cols,$join=null,$where,$order=null, $limit = null);	
		
		return $result;
	}
	//END }
}
?>
<?php
class finance_mst_account_main_type{
 
	private $db;
	private $table= "finance_mst_account_main_type";
	
	//private property
	private $MAIN_TYPE_ID;
	private $MAIN_TYPE_CODE;
	private $MAIN_TYPE_NAME;
	private $FINANCE_TYPE_ID;
	private $STATUS;
	private $CREATED_BY;
	private $CREATED_DATE;
	private $LAST_MODIFYED_BY;
	private $LAST_MODIFYED_DATE;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('MAIN_TYPE_ID'=>'MAIN_TYPE_ID',
										'MAIN_TYPE_CODE'=>'MAIN_TYPE_CODE',
										'MAIN_TYPE_NAME'=>'MAIN_TYPE_NAME',
										'FINANCE_TYPE_ID'=>'FINANCE_TYPE_ID',
										'STATUS'=>'STATUS',
										'CREATED_BY'=>'CREATED_BY',
										'CREATED_DATE'=>'CREATED_DATE',
										'LAST_MODIFYED_BY'=>'LAST_MODIFYED_BY',
										'LAST_MODIFYED_DATE'=>'LAST_MODIFYED_DATE',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "MAIN_TYPE_ID = ".$this->MAIN_TYPE_ID."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun MAIN_TYPE_ID
	function getMAIN_TYPE_ID()
	{
		$this->validate();
		return $this->MAIN_TYPE_ID;
	}
	
	//retun MAIN_TYPE_CODE
	function getMAIN_TYPE_CODE()
	{
		$this->validate();
		return $this->MAIN_TYPE_CODE;
	}
	
	//retun MAIN_TYPE_NAME
	function getMAIN_TYPE_NAME()
	{
		$this->validate();
		return $this->MAIN_TYPE_NAME;
	}
	
	//retun FINANCE_TYPE_ID
	function getFINANCE_TYPE_ID()
	{
		$this->validate();
		return $this->FINANCE_TYPE_ID;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//retun CREATED_BY
	function getCREATED_BY()
	{
		$this->validate();
		return $this->CREATED_BY;
	}
	
	//retun CREATED_DATE
	function getCREATED_DATE()
	{
		$this->validate();
		return $this->CREATED_DATE;
	}
	
	//retun LAST_MODIFYED_BY
	function getLAST_MODIFYED_BY()
	{
		$this->validate();
		return $this->LAST_MODIFYED_BY;
	}
	
	//retun LAST_MODIFYED_DATE
	function getLAST_MODIFYED_DATE()
	{
		$this->validate();
		return $this->LAST_MODIFYED_DATE;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set MAIN_TYPE_ID
	function setMAIN_TYPE_ID($MAIN_TYPE_ID)
	{
		array_push($this->commitArray,'MAIN_TYPE_ID');
		$this->MAIN_TYPE_ID = $MAIN_TYPE_ID;
	}
	
	//set MAIN_TYPE_CODE
	function setMAIN_TYPE_CODE($MAIN_TYPE_CODE)
	{
		array_push($this->commitArray,'MAIN_TYPE_CODE');
		$this->MAIN_TYPE_CODE = $MAIN_TYPE_CODE;
	}
	
	//set MAIN_TYPE_NAME
	function setMAIN_TYPE_NAME($MAIN_TYPE_NAME)
	{
		array_push($this->commitArray,'MAIN_TYPE_NAME');
		$this->MAIN_TYPE_NAME = $MAIN_TYPE_NAME;
	}
	
	//set FINANCE_TYPE_ID
	function setFINANCE_TYPE_ID($FINANCE_TYPE_ID)
	{
		array_push($this->commitArray,'FINANCE_TYPE_ID');
		$this->FINANCE_TYPE_ID = $FINANCE_TYPE_ID;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//set CREATED_BY
	function setCREATED_BY($CREATED_BY)
	{
		array_push($this->commitArray,'CREATED_BY');
		$this->CREATED_BY = $CREATED_BY;
	}
	
	//set CREATED_DATE
	function setCREATED_DATE($CREATED_DATE)
	{
		array_push($this->commitArray,'CREATED_DATE');
		$this->CREATED_DATE = $CREATED_DATE;
	}
	
	//set LAST_MODIFYED_BY
	function setLAST_MODIFYED_BY($LAST_MODIFYED_BY)
	{
		array_push($this->commitArray,'LAST_MODIFYED_BY');
		$this->LAST_MODIFYED_BY = $LAST_MODIFYED_BY;
	}
	
	//set LAST_MODIFYED_DATE
	function setLAST_MODIFYED_DATE($LAST_MODIFYED_DATE)
	{
		array_push($this->commitArray,'LAST_MODIFYED_DATE');
		$this->LAST_MODIFYED_DATE = $LAST_MODIFYED_DATE;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->MAIN_TYPE_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($MAIN_TYPE_ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "MAIN_TYPE_ID='$MAIN_TYPE_ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($MAIN_TYPE_ID,$MAIN_TYPE_CODE,$MAIN_TYPE_NAME,$FINANCE_TYPE_ID,$STATUS,$CREATED_BY,$CREATED_DATE,$LAST_MODIFYED_BY,$LAST_MODIFYED_DATE){
		$data = array('MAIN_TYPE_ID'=>$MAIN_TYPE_ID 
				,'MAIN_TYPE_CODE'=>$MAIN_TYPE_CODE 
				,'MAIN_TYPE_NAME'=>$MAIN_TYPE_NAME 
				,'FINANCE_TYPE_ID'=>$FINANCE_TYPE_ID 
				,'STATUS'=>$STATUS 
				,'CREATED_BY'=>$CREATED_BY 
				,'CREATED_DATE'=>$CREATED_DATE 
				,'LAST_MODIFYED_BY'=>$LAST_MODIFYED_BY 
				,'LAST_MODIFYED_DATE'=>$LAST_MODIFYED_DATE 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('MAIN_TYPE_ID,MAIN_TYPE_NAME',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['MAIN_TYPE_ID'])
				$html .= '<option selected="selected" value="'.$row['MAIN_TYPE_ID'].'">'.$row['MAIN_TYPE_NAME'].'</option>';	
			else
				$html .= '<option value="'.$row['MAIN_TYPE_ID'].'">'.$row['MAIN_TYPE_NAME'].'</option>';	
		}
		return $html;
	}
	
	//END }
}
?>
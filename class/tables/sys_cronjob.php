<?php
class sys_cronjob{

	private $db;
	private $table= "sys_cronjob";

	//private property
	private $CRONJOB_ID;
	private $CRONJOB_NAME;
	private $TO_EMAILS;
	private $CC_EMAILS;
	private $BCC_EMAILS;
	private $EXCUTE_FILE_PATH;
	private $SUNDAY;
	private $MONDAY;
	private $TUESDAY;
	private $WEDNESDAY;
	private $THURSDAY;
	private $FRIDAY;
	private $SATURDAY;
	private $EXCUTE_TIME_WITH_FROM;
	private $EXCUTE_TIME_TO;
	private $STATUS;
	private $TESTING;
	private $commitArray = array();
	private $field_array = array();
	

	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('CRONJOB_ID'=>'CRONJOB_ID',
										'CRONJOB_NAME'=>'CRONJOB_NAME',
										'TO_EMAILS'=>'TO_EMAILS',
										'CC_EMAILS'=>'CC_EMAILS',
										'BCC_EMAILS'=>'BCC_EMAILS',
										'EXCUTE_FILE_PATH'=>'EXCUTE_FILE_PATH',
										'SUNDAY'=>'SUNDAY',
										'MONDAY'=>'MONDAY',
										'TUESDAY'=>'TUESDAY',
										'WEDNESDAY'=>'WEDNESDAY',
										'THURSDAY'=>'THURSDAY',
										'FRIDAY'=>'FRIDAY',
										'SATURDAY'=>'SATURDAY',
										'EXCUTE_TIME_WITH_FROM'=>'EXCUTE_TIME_WITH_FROM',
										'EXCUTE_TIME_TO'=>'EXCUTE_TIME_TO',
										'STATUS'=>'STATUS',
										'TESTING'=>'TESTING',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit );
		return $result;
	}

	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data);
		return $this->db->getResult();


	}

	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}

	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();
	}

	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}

	public function get_field_array(){
		return $this->field_array;
	}

	function commit($type='update')
	{
		//commit update
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{
			$data[$v] = $this->$v;
		}
		$where		= "CRONJOB_ID = ".$this->CRONJOB_ID."" ;
		$this->commitArray = array();

		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}

	//BEGIN - public functions for private properties {
	
	//retun CRONJOB_ID
	function getCRONJOB_ID()
	{
		$this->validate();
		return $this->CRONJOB_ID;
	}
	
	//retun CRONJOB_NAME
	function getCRONJOB_NAME()
	{
		$this->validate();
		return $this->CRONJOB_NAME;
	}
	
	//retun TO_EMAILS
	function getTO_EMAILS()
	{
		$this->validate();
		return $this->TO_EMAILS;
	}
	
	//retun CC_EMAILS
	function getCC_EMAILS()
	{
		$this->validate();
		return $this->CC_EMAILS;
	}
	
	//retun BCC_EMAILS
	function getBCC_EMAILS()
	{
		$this->validate();
		return $this->BCC_EMAILS;
	}
	
	//retun EXCUTE_FILE_PATH
	function getEXCUTE_FILE_PATH()
	{
		$this->validate();
		return $this->EXCUTE_FILE_PATH;
	}
	
	//retun SUNDAY
	function getSUNDAY()
	{
		$this->validate();
		return $this->SUNDAY;
	}
	
	//retun MONDAY
	function getMONDAY()
	{
		$this->validate();
		return $this->MONDAY;
	}
	
	//retun TUESDAY
	function getTUESDAY()
	{
		$this->validate();
		return $this->TUESDAY;
	}
	
	//retun WEDNESDAY
	function getWEDNESDAY()
	{
		$this->validate();
		return $this->WEDNESDAY;
	}
	
	//retun THURSDAY
	function getTHURSDAY()
	{
		$this->validate();
		return $this->THURSDAY;
	}
	
	//retun FRIDAY
	function getFRIDAY()
	{
		$this->validate();
		return $this->FRIDAY;
	}
	
	//retun SATURDAY
	function getSATURDAY()
	{
		$this->validate();
		return $this->SATURDAY;
	}
	
	//retun EXCUTE_TIME_WITH_FROM
	function getEXCUTE_TIME_WITH_FROM()
	{
		$this->validate();
		return $this->EXCUTE_TIME_WITH_FROM;
	}
	
	//retun EXCUTE_TIME_TO
	function getEXCUTE_TIME_TO()
	{
		$this->validate();
		return $this->EXCUTE_TIME_TO;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//retun TESTING
	function getTESTING()
	{
		$this->validate();
		return $this->TESTING;
	}
	
	//END }

	//BEGIN - public function set {
	
	//set CRONJOB_ID
	function setCRONJOB_ID($CRONJOB_ID)
	{
		array_push($this->commitArray,'CRONJOB_ID');
		$this->CRONJOB_ID = $CRONJOB_ID;
	}
	
	//set CRONJOB_NAME
	function setCRONJOB_NAME($CRONJOB_NAME)
	{
		array_push($this->commitArray,'CRONJOB_NAME');
		$this->CRONJOB_NAME = $CRONJOB_NAME;
	}
	
	//set TO_EMAILS
	function setTO_EMAILS($TO_EMAILS)
	{
		array_push($this->commitArray,'TO_EMAILS');
		$this->TO_EMAILS = $TO_EMAILS;
	}
	
	//set CC_EMAILS
	function setCC_EMAILS($CC_EMAILS)
	{
		array_push($this->commitArray,'CC_EMAILS');
		$this->CC_EMAILS = $CC_EMAILS;
	}
	
	//set BCC_EMAILS
	function setBCC_EMAILS($BCC_EMAILS)
	{
		array_push($this->commitArray,'BCC_EMAILS');
		$this->BCC_EMAILS = $BCC_EMAILS;
	}
	
	//set EXCUTE_FILE_PATH
	function setEXCUTE_FILE_PATH($EXCUTE_FILE_PATH)
	{
		array_push($this->commitArray,'EXCUTE_FILE_PATH');
		$this->EXCUTE_FILE_PATH = $EXCUTE_FILE_PATH;
	}
	
	//set SUNDAY
	function setSUNDAY($SUNDAY)
	{
		array_push($this->commitArray,'SUNDAY');
		$this->SUNDAY = $SUNDAY;
	}
	
	//set MONDAY
	function setMONDAY($MONDAY)
	{
		array_push($this->commitArray,'MONDAY');
		$this->MONDAY = $MONDAY;
	}
	
	//set TUESDAY
	function setTUESDAY($TUESDAY)
	{
		array_push($this->commitArray,'TUESDAY');
		$this->TUESDAY = $TUESDAY;
	}
	
	//set WEDNESDAY
	function setWEDNESDAY($WEDNESDAY)
	{
		array_push($this->commitArray,'WEDNESDAY');
		$this->WEDNESDAY = $WEDNESDAY;
	}
	
	//set THURSDAY
	function setTHURSDAY($THURSDAY)
	{
		array_push($this->commitArray,'THURSDAY');
		$this->THURSDAY = $THURSDAY;
	}
	
	//set FRIDAY
	function setFRIDAY($FRIDAY)
	{
		array_push($this->commitArray,'FRIDAY');
		$this->FRIDAY = $FRIDAY;
	}
	
	//set SATURDAY
	function setSATURDAY($SATURDAY)
	{
		array_push($this->commitArray,'SATURDAY');
		$this->SATURDAY = $SATURDAY;
	}
	
	//set EXCUTE_TIME_WITH_FROM
	function setEXCUTE_TIME_WITH_FROM($EXCUTE_TIME_WITH_FROM)
	{
		array_push($this->commitArray,'EXCUTE_TIME_WITH_FROM');
		$this->EXCUTE_TIME_WITH_FROM = $EXCUTE_TIME_WITH_FROM;
	}
	
	//set EXCUTE_TIME_TO
	function setEXCUTE_TIME_TO($EXCUTE_TIME_TO)
	{
		array_push($this->commitArray,'EXCUTE_TIME_TO');
		$this->EXCUTE_TIME_TO = $EXCUTE_TIME_TO;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//set TESTING
	function setTESTING($TESTING)
	{
		array_push($this->commitArray,'TESTING');
		$this->TESTING = $TESTING;
	}
	
	//END }

	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->CRONJOB_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}

	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}

	public function set($CRONJOB_ID)
	{
		$cols	= "*";

		$join	= NULL;

		$where	= "CRONJOB_ID='$CRONJOB_ID'";

		$result = $this->select($cols,$join,$where);
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}

	//insert as parameters
	public function insertRec($CRONJOB_ID,$CRONJOB_NAME,$TO_EMAILS,$CC_EMAILS,$BCC_EMAILS,$EXCUTE_FILE_PATH,$SUNDAY,$MONDAY,$TUESDAY,$WEDNESDAY,$THURSDAY,$FRIDAY,$SATURDAY,$EXCUTE_TIME_WITH_FROM,$EXCUTE_TIME_TO,$STATUS,$TESTING){
		$data = array('CRONJOB_ID'=>$CRONJOB_ID 
				,'CRONJOB_NAME'=>$CRONJOB_NAME 
				,'TO_EMAILS'=>$TO_EMAILS 
				,'CC_EMAILS'=>$CC_EMAILS 
				,'BCC_EMAILS'=>$BCC_EMAILS 
				,'EXCUTE_FILE_PATH'=>$EXCUTE_FILE_PATH 
				,'SUNDAY'=>$SUNDAY 
				,'MONDAY'=>$MONDAY 
				,'TUESDAY'=>$TUESDAY 
				,'WEDNESDAY'=>$WEDNESDAY 
				,'THURSDAY'=>$THURSDAY 
				,'FRIDAY'=>$FRIDAY 
				,'SATURDAY'=>$SATURDAY 
				,'EXCUTE_TIME_WITH_FROM'=>$EXCUTE_TIME_WITH_FROM 
				,'EXCUTE_TIME_TO'=>$EXCUTE_TIME_TO 
				,'STATUS'=>$STATUS 
				,'TESTING'=>$TESTING 
				);
		return $this->insert($data);
	}

	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('CRONJOB_ID,CRONJOB_NAME',  null, $where = $where);
		$html = '<option value=""></option>';
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['CRONJOB_ID'])
				$html .= '<option selected="selected" value="'.$row['CRONJOB_ID'].'">'.$row['CRONJOB_NAME'].'</option>';
			else
				$html .= '<option value="'.$row['CRONJOB_ID'].'">'.$row['CRONJOB_NAME'].'</option>';
		}
		return $html;
	}
	
	//END }
}
?>
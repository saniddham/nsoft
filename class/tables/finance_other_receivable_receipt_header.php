<?php
class finance_other_receivable_receipt_header{
 
	private $db;
	private $table= "finance_other_receivable_receipt_header";
	
	//private property
	private $RECEIPT_NO;
	private $RECEIPT_YEAR;
	private $CUSTOMER_ID;
	private $CURRENCY_ID;
	private $RECEIPT_DATE;
	private $PAYMENT_MODE_ID;
	private $BANK_REFERENCE_NO;
	private $REMARKS;
	private $STATUS;
	private $APPROVE_LEVELS;
	private $PRINT_STATUS;
	private $COMPANY_ID;
	private $LOCATION_ID;
	private $CREATED_BY;
	private $CREATED_DATE;
	private $LAST_MODIFY_BY;
	private $LAST_MODIFY_DATE;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('RECEIPT_NO'=>'RECEIPT_NO',
										'RECEIPT_YEAR'=>'RECEIPT_YEAR',
										'CUSTOMER_ID'=>'CUSTOMER_ID',
										'CURRENCY_ID'=>'CURRENCY_ID',
										'RECEIPT_DATE'=>'RECEIPT_DATE',
										'PAYMENT_MODE_ID'=>'PAYMENT_MODE_ID',
										'BANK_REFERENCE_NO'=>'BANK_REFERENCE_NO',
										'REMARKS'=>'REMARKS',
										'STATUS'=>'STATUS',
										'APPROVE_LEVELS'=>'APPROVE_LEVELS',
										'PRINT_STATUS'=>'PRINT_STATUS',
										'COMPANY_ID'=>'COMPANY_ID',
										'LOCATION_ID'=>'LOCATION_ID',
										'CREATED_BY'=>'CREATED_BY',
										'CREATED_DATE'=>'CREATED_DATE',
										'LAST_MODIFY_BY'=>'LAST_MODIFY_BY',
										'LAST_MODIFY_DATE'=>'LAST_MODIFY_DATE',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "RECEIPT_NO = ".$this->RECEIPT_NO." and RECEIPT_YEAR = ".$this->RECEIPT_YEAR."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun RECEIPT_NO
	function getRECEIPT_NO()
	{
		$this->validate();
		return $this->RECEIPT_NO;
	}
	
	//retun RECEIPT_YEAR
	function getRECEIPT_YEAR()
	{
		$this->validate();
		return $this->RECEIPT_YEAR;
	}
	
	//retun CUSTOMER_ID
	function getCUSTOMER_ID()
	{
		$this->validate();
		return $this->CUSTOMER_ID;
	}
	
	//retun CURRENCY_ID
	function getCURRENCY_ID()
	{
		$this->validate();
		return $this->CURRENCY_ID;
	}
	
	//retun RECEIPT_DATE
	function getRECEIPT_DATE()
	{
		$this->validate();
		return $this->RECEIPT_DATE;
	}
	
	//retun PAYMENT_MODE_ID
	function getPAYMENT_MODE_ID()
	{
		$this->validate();
		return $this->PAYMENT_MODE_ID;
	}
	
	//retun BANK_REFERENCE_NO
	function getBANK_REFERENCE_NO()
	{
		$this->validate();
		return $this->BANK_REFERENCE_NO;
	}
	
	//retun REMARKS
	function getREMARKS()
	{
		$this->validate();
		return $this->REMARKS;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//retun APPROVE_LEVELS
	function getAPPROVE_LEVELS()
	{
		$this->validate();
		return $this->APPROVE_LEVELS;
	}
	
	//retun PRINT_STATUS
	function getPRINT_STATUS()
	{
		$this->validate();
		return $this->PRINT_STATUS;
	}
	
	//retun COMPANY_ID
	function getCOMPANY_ID()
	{
		$this->validate();
		return $this->COMPANY_ID;
	}
	
	//retun LOCATION_ID
	function getLOCATION_ID()
	{
		$this->validate();
		return $this->LOCATION_ID;
	}
	
	//retun CREATED_BY
	function getCREATED_BY()
	{
		$this->validate();
		return $this->CREATED_BY;
	}
	
	//retun CREATED_DATE
	function getCREATED_DATE()
	{
		$this->validate();
		return $this->CREATED_DATE;
	}
	
	//retun LAST_MODIFY_BY
	function getLAST_MODIFY_BY()
	{
		$this->validate();
		return $this->LAST_MODIFY_BY;
	}
	
	//retun LAST_MODIFY_DATE
	function getLAST_MODIFY_DATE()
	{
		$this->validate();
		return $this->LAST_MODIFY_DATE;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set RECEIPT_NO
	function setRECEIPT_NO($RECEIPT_NO)
	{
		array_push($this->commitArray,'RECEIPT_NO');
		$this->RECEIPT_NO = $RECEIPT_NO;
	}
	
	//set RECEIPT_YEAR
	function setRECEIPT_YEAR($RECEIPT_YEAR)
	{
		array_push($this->commitArray,'RECEIPT_YEAR');
		$this->RECEIPT_YEAR = $RECEIPT_YEAR;
	}
	
	//set CUSTOMER_ID
	function setCUSTOMER_ID($CUSTOMER_ID)
	{
		array_push($this->commitArray,'CUSTOMER_ID');
		$this->CUSTOMER_ID = $CUSTOMER_ID;
	}
	
	//set CURRENCY_ID
	function setCURRENCY_ID($CURRENCY_ID)
	{
		array_push($this->commitArray,'CURRENCY_ID');
		$this->CURRENCY_ID = $CURRENCY_ID;
	}
	
	//set RECEIPT_DATE
	function setRECEIPT_DATE($RECEIPT_DATE)
	{
		array_push($this->commitArray,'RECEIPT_DATE');
		$this->RECEIPT_DATE = $RECEIPT_DATE;
	}
	
	//set PAYMENT_MODE_ID
	function setPAYMENT_MODE_ID($PAYMENT_MODE_ID)
	{
		array_push($this->commitArray,'PAYMENT_MODE_ID');
		$this->PAYMENT_MODE_ID = $PAYMENT_MODE_ID;
	}
	
	//set BANK_REFERENCE_NO
	function setBANK_REFERENCE_NO($BANK_REFERENCE_NO)
	{
		array_push($this->commitArray,'BANK_REFERENCE_NO');
		$this->BANK_REFERENCE_NO = $BANK_REFERENCE_NO;
	}
	
	//set REMARKS
	function setREMARKS($REMARKS)
	{
		array_push($this->commitArray,'REMARKS');
		$this->REMARKS = $REMARKS;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//set APPROVE_LEVELS
	function setAPPROVE_LEVELS($APPROVE_LEVELS)
	{
		array_push($this->commitArray,'APPROVE_LEVELS');
		$this->APPROVE_LEVELS = $APPROVE_LEVELS;
	}
	
	//set PRINT_STATUS
	function setPRINT_STATUS($PRINT_STATUS)
	{
		array_push($this->commitArray,'PRINT_STATUS');
		$this->PRINT_STATUS = $PRINT_STATUS;
	}
	
	//set COMPANY_ID
	function setCOMPANY_ID($COMPANY_ID)
	{
		array_push($this->commitArray,'COMPANY_ID');
		$this->COMPANY_ID = $COMPANY_ID;
	}
	
	//set LOCATION_ID
	function setLOCATION_ID($LOCATION_ID)
	{
		array_push($this->commitArray,'LOCATION_ID');
		$this->LOCATION_ID = $LOCATION_ID;
	}
	
	//set CREATED_BY
	function setCREATED_BY($CREATED_BY)
	{
		array_push($this->commitArray,'CREATED_BY');
		$this->CREATED_BY = $CREATED_BY;
	}
	
	//set CREATED_DATE
	function setCREATED_DATE($CREATED_DATE)
	{
		array_push($this->commitArray,'CREATED_DATE');
		$this->CREATED_DATE = $CREATED_DATE;
	}
	
	//set LAST_MODIFY_BY
	function setLAST_MODIFY_BY($LAST_MODIFY_BY)
	{
		array_push($this->commitArray,'LAST_MODIFY_BY');
		$this->LAST_MODIFY_BY = $LAST_MODIFY_BY;
	}
	
	//set LAST_MODIFY_DATE
	function setLAST_MODIFY_DATE($LAST_MODIFY_DATE)
	{
		array_push($this->commitArray,'LAST_MODIFY_DATE');
		$this->LAST_MODIFY_DATE = $LAST_MODIFY_DATE;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->RECEIPT_NO=='' || $this->RECEIPT_YEAR=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($RECEIPT_NO , $RECEIPT_YEAR)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "RECEIPT_NO='$RECEIPT_NO' and RECEIPT_YEAR='$RECEIPT_YEAR'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($RECEIPT_NO,$RECEIPT_YEAR,$CUSTOMER_ID,$CURRENCY_ID,$RECEIPT_DATE,$PAYMENT_MODE_ID,$BANK_REFERENCE_NO,$REMARKS,$STATUS,$APPROVE_LEVELS,$PRINT_STATUS,$COMPANY_ID,$LOCATION_ID,$CREATED_BY,$CREATED_DATE,$LAST_MODIFY_BY,$LAST_MODIFY_DATE){
		$data = array('RECEIPT_NO'=>$RECEIPT_NO 
				,'RECEIPT_YEAR'=>$RECEIPT_YEAR 
				,'CUSTOMER_ID'=>$CUSTOMER_ID 
				,'CURRENCY_ID'=>$CURRENCY_ID 
				,'RECEIPT_DATE'=>$RECEIPT_DATE 
				,'PAYMENT_MODE_ID'=>$PAYMENT_MODE_ID 
				,'BANK_REFERENCE_NO'=>$BANK_REFERENCE_NO 
				,'REMARKS'=>$REMARKS 
				,'STATUS'=>$STATUS 
				,'APPROVE_LEVELS'=>$APPROVE_LEVELS 
				,'PRINT_STATUS'=>$PRINT_STATUS 
				,'COMPANY_ID'=>$COMPANY_ID 
				,'LOCATION_ID'=>$LOCATION_ID 
				,'CREATED_BY'=>$CREATED_BY 
				,'CREATED_DATE'=>$CREATED_DATE 
				,'LAST_MODIFY_BY'=>$LAST_MODIFY_BY 
				,'LAST_MODIFY_DATE'=>$LAST_MODIFY_DATE 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('RECEIPT_NO,RECEIPT_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['RECEIPT_NO'])
				$html .= '<option selected="selected" value="'.$row['RECEIPT_NO'].'">'.$row['RECEIPT_YEAR'].'</option>';	
			else
				$html .= '<option value="'.$row['RECEIPT_NO'].'">'.$row['RECEIPT_YEAR'].'</option>';	
		}
		return $html;
	}
	function getPDFHeader_result($receiptNo,$receiptYear)
	{
		$sql	= "SELECT
				CU.strName          AS CUSTOMER_NAME,
				CU.strAddress       AS ADDRESS,
				CU.strCity          AS CITY,
				CO.strCountryName   AS COUNTRY_NAME,
				FORH.RECEIPT_NO	    AS SERIAL_NO,
				FORH.COMPANY_ID	    AS COMPANY_ID,
				FORH.LOCATION_ID	    AS LOCATION_ID,
				FORH.RECEIPT_DATE    AS SERIAL_DATE,
				FORH.RECEIPT_DATE    AS RECEIPT_DATE,
				FORH.CURRENCY_ID    	AS CURRENCY_ID,
				PM.strName	    	AS PAYMENT_MODE,
				C.strCode 	    	AS CURRENCY_CODE,
				FORH.REMARKS	   	 	AS REMARKS,
				FORH.STATUS	    	AS STATUS,
				FORH.PRINT_STATUS	AS PRINT_STATUS,
				U.strUserName		AS CREATED_BY_NAME,
				(SELECT SUM(FORD.AMOUNT)
				FROM finance_other_receivable_receipt_details FORD 
				WHERE 
				FORD.RECEIPT_NO = FORH.RECEIPT_NO
				AND FORD.RECEIPT_YEAR = FORH.RECEIPT_YEAR) AS VALUE,
				(SELECT COA.CHART_OF_ACCOUNT_NAME
				FROM finance_mst_chartofaccount COA
				INNER JOIN finance_other_receivable_receipt_gl FORGL ON FORGL.CHART_OF_ACCOUNT_ID=COA.CHART_OF_ACCOUNT_ID
				WHERE FORGL.RECEIPT_NO=FORH.RECEIPT_NO AND
				FORGL.RECEIPT_YEAR=FORH.RECEIPT_YEAR AND
				COA.BANK = 1
				) AS BANK_GL_NAME,
				ORGL.CHART_OF_ACCOUNT_ID,
				ORGL.AMOUNT,
				ORGL.REMARKS AS MEMO
				FROM finance_other_receivable_receipt_header FORH 
				INNER JOIN finance_other_receivable_receipt_gl AS ORGL ON FORH.RECEIPT_NO = ORGL.RECEIPT_NO AND FORH.RECEIPT_YEAR = ORGL.RECEIPT_YEAR
				INNER JOIN mst_customer CU ON CU.intId = FORH.CUSTOMER_ID
				INNER JOIN mst_financecurrency C ON C.intId = FORH.CURRENCY_ID
				INNER JOIN sys_users U ON U.intUserId = FORH.CREATED_BY
				INNER JOIN mst_financepaymentsmethods PM ON PM.intId = FORH.PAYMENT_MODE_ID
				LEFT JOIN mst_country CO ON CO.intCountryID = CU.intCountryId
				WHERE FORH.RECEIPT_NO = '$receiptNo'
				AND FORH.RECEIPT_YEAR = '$receiptYear' "	;
			//	echo $sql;
		$result = $this->db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		
		return $row;
	}
	//END }
}
?>
<?php
class costing_sample_header_approved_by{

	private $db;
	private $table= "costing_sample_header_approved_by";

	//private property
	private $SAMPLE_NO;
	private $SAMPLE_YEAR;
	private $REVISION_NO;
	private $COMBO;
	private $PRINT;
	private $APPROVED_LEVEL_NO;
	private $APPROVED_BY;
	private $APPROVED_DATE;
	private $STATUS;
	private $commitArray = array();
	private $field_array = array();
	

	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('SAMPLE_NO'=>'SAMPLE_NO',
										'SAMPLE_YEAR'=>'SAMPLE_YEAR',
										'REVISION_NO'=>'REVISION_NO',
										'COMBO'=>'COMBO',
										'PRINT'=>'PRINT',
										'APPROVED_LEVEL_NO'=>'APPROVED_LEVEL_NO',
										'APPROVED_BY'=>'APPROVED_BY',
										'APPROVED_DATE'=>'APPROVED_DATE',
										'STATUS'=>'STATUS',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit );
		return $result;
	}

	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data);
		return $this->db->getResult();


	}

	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}

	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();
	}

	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}

	public function get_field_array(){
		return $this->field_array;
	}

	function commit($type='update')
	{
		//commit update
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{
			$data[$v] = $this->$v;
		}
		$where		= "SAMPLE_NO = ".$this->SAMPLE_NO." and SAMPLE_YEAR = ".$this->SAMPLE_YEAR." and REVISION_NO = ".$this->REVISION_NO." and COMBO = ".$this->COMBO." and PRINT = ".$this->PRINT." and APPROVED_LEVEL_NO = ".$this->APPROVED_LEVEL_NO." and STATUS = ".$this->STATUS."" ;
		$this->commitArray = array();

		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}

	//BEGIN - public functions for private properties {
	
	//retun SAMPLE_NO
	function getSAMPLE_NO()
	{
		$this->validate();
		return $this->SAMPLE_NO;
	}
	
	//retun SAMPLE_YEAR
	function getSAMPLE_YEAR()
	{
		$this->validate();
		return $this->SAMPLE_YEAR;
	}
	
	//retun REVISION_NO
	function getREVISION_NO()
	{
		$this->validate();
		return $this->REVISION_NO;
	}
	
	//retun COMBO
	function getCOMBO()
	{
		$this->validate();
		return $this->COMBO;
	}
	
	//retun PRINT
	function getPRINT()
	{
		$this->validate();
		return $this->PRINT;
	}
	
	//retun APPROVED_LEVEL_NO
	function getAPPROVED_LEVEL_NO()
	{
		$this->validate();
		return $this->APPROVED_LEVEL_NO;
	}
	
	//retun APPROVED_BY
	function getAPPROVED_BY()
	{
		$this->validate();
		return $this->APPROVED_BY;
	}
	
	//retun APPROVED_DATE
	function getAPPROVED_DATE()
	{
		$this->validate();
		return $this->APPROVED_DATE;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//END }

	//BEGIN - public function set {
	
	//set SAMPLE_NO
	function setSAMPLE_NO($SAMPLE_NO)
	{
		array_push($this->commitArray,'SAMPLE_NO');
		$this->SAMPLE_NO = $SAMPLE_NO;
	}
	
	//set SAMPLE_YEAR
	function setSAMPLE_YEAR($SAMPLE_YEAR)
	{
		array_push($this->commitArray,'SAMPLE_YEAR');
		$this->SAMPLE_YEAR = $SAMPLE_YEAR;
	}
	
	//set REVISION_NO
	function setREVISION_NO($REVISION_NO)
	{
		array_push($this->commitArray,'REVISION_NO');
		$this->REVISION_NO = $REVISION_NO;
	}
	
	//set COMBO
	function setCOMBO($COMBO)
	{
		array_push($this->commitArray,'COMBO');
		$this->COMBO = $COMBO;
	}
	
	//set PRINT
	function setPRINT($PRINT)
	{
		array_push($this->commitArray,'PRINT');
		$this->PRINT = $PRINT;
	}
	
	//set APPROVED_LEVEL_NO
	function setAPPROVED_LEVEL_NO($APPROVED_LEVEL_NO)
	{
		array_push($this->commitArray,'APPROVED_LEVEL_NO');
		$this->APPROVED_LEVEL_NO = $APPROVED_LEVEL_NO;
	}
	
	//set APPROVED_BY
	function setAPPROVED_BY($APPROVED_BY)
	{
		array_push($this->commitArray,'APPROVED_BY');
		$this->APPROVED_BY = $APPROVED_BY;
	}
	
	//set APPROVED_DATE
	function setAPPROVED_DATE($APPROVED_DATE)
	{
		array_push($this->commitArray,'APPROVED_DATE');
		$this->APPROVED_DATE = $APPROVED_DATE;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//END }

	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->SAMPLE_NO=='' || $this->SAMPLE_YEAR=='' || $this->REVISION_NO=='' || $this->COMBO=='' || $this->PRINT=='' || $this->APPROVED_LEVEL_NO=='' || $this->STATUS=='')
			throw new exception("Set primary values first");
		else
			return true;
	}

	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}

	public function set($SAMPLE_NO , $SAMPLE_YEAR , $REVISION_NO , $COMBO , $PRINT , $APPROVED_LEVEL_NO , $STATUS)
	{
		$cols	= "*";

		$join	= NULL;

		$where	= "SAMPLE_NO='$SAMPLE_NO' and SAMPLE_YEAR='$SAMPLE_YEAR' and REVISION_NO='$REVISION_NO' and COMBO='$COMBO' and PRINT='$PRINT' and APPROVED_LEVEL_NO='$APPROVED_LEVEL_NO' and STATUS='$STATUS'";

		$result = $this->select($cols,$join,$where);
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}

	//insert as parameters
	public function insertRec($SAMPLE_NO,$SAMPLE_YEAR,$REVISION_NO,$COMBO,$PRINT,$APPROVED_LEVEL_NO,$APPROVED_BY,$APPROVED_DATE,$STATUS){
		$data = array('SAMPLE_NO'=>$SAMPLE_NO 
				,'SAMPLE_YEAR'=>$SAMPLE_YEAR 
				,'REVISION_NO'=>$REVISION_NO 
				,'COMBO'=>$COMBO 
				,'PRINT'=>$PRINT 
				,'APPROVED_LEVEL_NO'=>$APPROVED_LEVEL_NO 
				,'APPROVED_BY'=>$APPROVED_BY 
				,'APPROVED_DATE'=>$APPROVED_DATE 
				,'STATUS'=>$STATUS 
				);
		return $this->insert($data);
	}

	public function getCombo1($defaultValue=null,$where=null){
		$result = $this->select('SAMPLE_NO,SAMPLE_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['SAMPLE_NO'])
				$html .= '<option selected="selected" value="'.$row['SAMPLE_NO'].'">'.$row['SAMPLE_YEAR'].'</option>';
			else
				$html .= '<option value="'.$row['SAMPLE_NO'].'">'.$row['SAMPLE_YEAR'].'</option>';
		}
		return $html;
	}
	
	//END }
}
?>
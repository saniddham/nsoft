<?php
class mst_invoicetype{
 
	private $db;
	private $table= "mst_invoicetype";
	
	//private property
	private $intId;
	private $strInvoiceType;
	private $SALES_INVOICE_REPORT_NAME;
	private $CREDIT_NOTE_REPORT_NAME;
	private $OTHER_RECEIVE_INVOICE_REPORT_NAME;
	private $intStatus;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('intId'=>'intId',
										'strInvoiceType'=>'strInvoiceType',
										'SALES_INVOICE_REPORT_NAME'=>'SALES_INVOICE_REPORT_NAME',
										'CREDIT_NOTE_REPORT_NAME'=>'CREDIT_NOTE_REPORT_NAME',
										'OTHER_RECEIVE_INVOICE_REPORT_NAME'=>'OTHER_RECEIVE_INVOICE_REPORT_NAME',
										'intStatus'=>'intStatus',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "intId = ".$this->intId."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun intId
	function getintId()
	{
		$this->validate();
		return $this->intId;
	}
	
	//retun strInvoiceType
	function getstrInvoiceType()
	{
		$this->validate();
		return $this->strInvoiceType;
	}
	
	//retun SALES_INVOICE_REPORT_NAME
	function getSALES_INVOICE_REPORT_NAME()
	{
		$this->validate();
		return $this->SALES_INVOICE_REPORT_NAME;
	}
	
	//retun CREDIT_NOTE_REPORT_NAME
	function getCREDIT_NOTE_REPORT_NAME()
	{
		$this->validate();
		return $this->CREDIT_NOTE_REPORT_NAME;
	}
	
	//retun OTHER_RECEIVE_INVOICE_REPORT_NAME
	function getOTHER_RECEIVE_INVOICE_REPORT_NAME()
	{
		$this->validate();
		return $this->OTHER_RECEIVE_INVOICE_REPORT_NAME;
	}
	
	//retun intStatus
	function getintStatus()
	{
		$this->validate();
		return $this->intStatus;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set intId
	function setintId($intId)
	{
		array_push($this->commitArray,'intId');
		$this->intId = $intId;
	}
	
	//set strInvoiceType
	function setstrInvoiceType($strInvoiceType)
	{
		array_push($this->commitArray,'strInvoiceType');
		$this->strInvoiceType = $strInvoiceType;
	}
	
	//set SALES_INVOICE_REPORT_NAME
	function setSALES_INVOICE_REPORT_NAME($SALES_INVOICE_REPORT_NAME)
	{
		array_push($this->commitArray,'SALES_INVOICE_REPORT_NAME');
		$this->SALES_INVOICE_REPORT_NAME = $SALES_INVOICE_REPORT_NAME;
	}
	
	//set CREDIT_NOTE_REPORT_NAME
	function setCREDIT_NOTE_REPORT_NAME($CREDIT_NOTE_REPORT_NAME)
	{
		array_push($this->commitArray,'CREDIT_NOTE_REPORT_NAME');
		$this->CREDIT_NOTE_REPORT_NAME = $CREDIT_NOTE_REPORT_NAME;
	}
	
	//set OTHER_RECEIVE_INVOICE_REPORT_NAME
	function setOTHER_RECEIVE_INVOICE_REPORT_NAME($OTHER_RECEIVE_INVOICE_REPORT_NAME)
	{
		array_push($this->commitArray,'OTHER_RECEIVE_INVOICE_REPORT_NAME');
		$this->OTHER_RECEIVE_INVOICE_REPORT_NAME = $OTHER_RECEIVE_INVOICE_REPORT_NAME;
	}
	
	//set intStatus
	function setintStatus($intStatus)
	{
		array_push($this->commitArray,'intStatus');
		$this->intStatus = $intStatus;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->intId=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($intId)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "intId='$intId'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($intId,$strInvoiceType,$SALES_INVOICE_REPORT_NAME,$CREDIT_NOTE_REPORT_NAME,$OTHER_RECEIVE_INVOICE_REPORT_NAME,$intStatus){
		$data = array('intId'=>$intId 
				,'strInvoiceType'=>$strInvoiceType 
				,'SALES_INVOICE_REPORT_NAME'=>$SALES_INVOICE_REPORT_NAME 
				,'CREDIT_NOTE_REPORT_NAME'=>$CREDIT_NOTE_REPORT_NAME 
				,'OTHER_RECEIVE_INVOICE_REPORT_NAME'=>$OTHER_RECEIVE_INVOICE_REPORT_NAME 
				,'intStatus'=>$intStatus 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('intId,strInvoiceType',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intId'])
				$html .= '<option selected="selected" value="'.$row['intId'].'">'.$row['strInvoiceType'].'</option>';	
			else
				$html .= '<option value="'.$row['intId'].'">'.$row['strInvoiceType'].'</option>';	
		}
		return $html;
	}
	
	//END }
}
?>
<?php
//THIS CLASS MANUALLY EDITED BY LAHIRU
class menus_special{
 
	private $db;
	private $table= "menus_special";
	
	//private property
	private $intId;
	private $intMenuId;
	private $strPermisionType;
	private $intStatus;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('intId'=>'intId',
										'intMenuId'=>'intMenuId',
										'strPermisionType'=>'strPermisionType',
										'intStatus'=>'intStatus',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "intId = ".$this->intId."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun intId
	function getintId()
	{
		$this->validate();
		return $this->intId;
	}
	
	//retun intMenuId
	function getintMenuId()
	{
		$this->validate();
		return $this->intMenuId;
	}
	
	//retun strPermisionType
	function getstrPermisionType()
	{
		$this->validate();
		return $this->strPermisionType;
	}
	
	//retun intStatus
	function getintStatus()
	{
		$this->validate();
		return $this->intStatus;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set intId
	function setintId($intId)
	{
		array_push($this->commitArray,'intId');
		$this->intId = $intId;
	}
	
	//set intMenuId
	function setintMenuId($intMenuId)
	{
		array_push($this->commitArray,'intMenuId');
		$this->intMenuId = $intMenuId;
	}
	
	//set strPermisionType
	function setstrPermisionType($strPermisionType)
	{
		array_push($this->commitArray,'strPermisionType');
		$this->strPermisionType = $strPermisionType;
	}
	
	//set intStatus
	function setintStatus($intStatus)
	{
		array_push($this->commitArray,'intStatus');
		$this->intStatus = $intStatus;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->intId=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($intId)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "intId='$intId'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($intId,$intMenuId,$strPermisionType,$intStatus){
		$data = array('intId'=>$intId 
				,'intMenuId'=>$intMenuId 
				,'strPermisionType'=>$strPermisionType 
				,'intStatus'=>$intStatus 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('intId,strPermisionType',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intId'])
				$html .= '<option selected="selected" value="'.$row['intId'].'">'.$row['strPermisionType'].'</option>';	
			else
				$html .= '<option value="'.$row['intId'].'">'.$row['strPermisionType'].'</option>';	
		}
		return $html;
	}
	public function loadSpecialMenuPermission($spMenuId,$userId)
	{
		$cols	= " menus_special_permision.intUser ";
		
		$join	= " INNER JOIN menus_special_permision ON menus_special.intId=menus_special_permision.intSpMenuId ";
		
		$where	= " menus_special.intStatus = 1 AND
					menus_special.intId = '".$spMenuId."' AND
					menus_special_permision.intUser = '".$userId."' ";
		
		$result = $this->select($cols,$join,$where,$order=null,$limit=null);
		
		if(mysqli_num_rows($result)>0)
			return 1;
		else
			return 0;
	}
	
	//END }
}
?>
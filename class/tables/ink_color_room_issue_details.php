<?php
class ink_color_room_issue_details{
 
	private $db;
	private $table= "ink_color_room_issue_details";
	
	//private property
	private $SERIAL_NO;
	private $SERIAL_YEAR;
	private $ORDER_NO;
	private $ORDER_YEAR;
	private $SALES_ORDER_ID;
	private $ITEM_ID;
	private $QTY;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('SERIAL_NO'=>'SERIAL_NO',
										'SERIAL_YEAR'=>'SERIAL_YEAR',
										'ORDER_NO'=>'ORDER_NO',
										'ORDER_YEAR'=>'ORDER_YEAR',
										'SALES_ORDER_ID'=>'SALES_ORDER_ID',
										'ITEM_ID'=>'ITEM_ID',
										'QTY'=>'QTY',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	function commit()
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "SERIAL_NO = ".$this->SERIAL_NO." and SERIAL_YEAR = ".$this->SERIAL_YEAR." and ORDER_NO = ".$this->ORDER_NO." and ORDER_YEAR = ".$this->ORDER_YEAR." and SALES_ORDER_ID = ".$this->SALES_ORDER_ID." and ITEM_ID = ".$this->ITEM_ID."" ;
		unset($this->commitArray);
		
		if($this->intId==NULL || $this->intId=='' || $this->intId=='NULL')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun SERIAL_NO
	function getSERIAL_NO()
	{
		$this->validate();
		return $this->SERIAL_NO;
	}
	
	//retun SERIAL_YEAR
	function getSERIAL_YEAR()
	{
		$this->validate();
		return $this->SERIAL_YEAR;
	}
	
	//retun ORDER_NO
	function getORDER_NO()
	{
		$this->validate();
		return $this->ORDER_NO;
	}
	
	//retun ORDER_YEAR
	function getORDER_YEAR()
	{
		$this->validate();
		return $this->ORDER_YEAR;
	}
	
	//retun SALES_ORDER_ID
	function getSALES_ORDER_ID()
	{
		$this->validate();
		return $this->SALES_ORDER_ID;
	}
	
	//retun ITEM_ID
	function getITEM_ID()
	{
		$this->validate();
		return $this->ITEM_ID;
	}
	
	//retun QTY
	function getQTY()
	{
		$this->validate();
		return $this->QTY;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set SERIAL_NO
	function setSERIAL_NO($SERIAL_NO)
	{
		array_push($this->commitArray,'SERIAL_NO');
		$this->SERIAL_NO = $SERIAL_NO;
	}
	
	//set SERIAL_YEAR
	function setSERIAL_YEAR($SERIAL_YEAR)
	{
		array_push($this->commitArray,'SERIAL_YEAR');
		$this->SERIAL_YEAR = $SERIAL_YEAR;
	}
	
	//set ORDER_NO
	function setORDER_NO($ORDER_NO)
	{
		array_push($this->commitArray,'ORDER_NO');
		$this->ORDER_NO = $ORDER_NO;
	}
	
	//set ORDER_YEAR
	function setORDER_YEAR($ORDER_YEAR)
	{
		array_push($this->commitArray,'ORDER_YEAR');
		$this->ORDER_YEAR = $ORDER_YEAR;
	}
	
	//set SALES_ORDER_ID
	function setSALES_ORDER_ID($SALES_ORDER_ID)
	{
		array_push($this->commitArray,'SALES_ORDER_ID');
		$this->SALES_ORDER_ID = $SALES_ORDER_ID;
	}
	
	//set ITEM_ID
	function setITEM_ID($ITEM_ID)
	{
		array_push($this->commitArray,'ITEM_ID');
		$this->ITEM_ID = $ITEM_ID;
	}
	
	//set QTY
	function setQTY($QTY)
	{
		array_push($this->commitArray,'QTY');
		$this->QTY = $QTY;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->SERIAL_NO=='' || $this->SERIAL_YEAR=='' || $this->ORDER_NO=='' || $this->ORDER_YEAR=='' || $this->SALES_ORDER_ID=='' || $this->ITEM_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($SERIAL_NO , $SERIAL_YEAR , $ORDER_NO , $ORDER_YEAR , $SALES_ORDER_ID , $ITEM_ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "SERIAL_NO='$SERIAL_NO' and SERIAL_YEAR='$SERIAL_YEAR' and ORDER_NO='$ORDER_NO' and ORDER_YEAR='$ORDER_YEAR' and SALES_ORDER_ID='$SALES_ORDER_ID' and ITEM_ID='$ITEM_ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($SERIAL_NO,$SERIAL_YEAR,$ORDER_NO,$ORDER_YEAR,$SALES_ORDER_ID,$ITEM_ID,$QTY){
		$data = array('SERIAL_NO'=>$SERIAL_NO 
				,'SERIAL_YEAR'=>$SERIAL_YEAR 
				,'ORDER_NO'=>$ORDER_NO 
				,'ORDER_YEAR'=>$ORDER_YEAR 
				,'SALES_ORDER_ID'=>$SALES_ORDER_ID 
				,'ITEM_ID'=>$ITEM_ID 
				,'QTY'=>$QTY 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('SERIAL_NO,SERIAL_YEAR',  null, $where = $where);
		while($row=mysqli_fetch_array($result)){
			$html .= '<option value="'.$row['SERIAL_NO'].'">'.$row['SERIAL_YEAR'].'</option>';	
		}
		return $html;
	}
	
	public function getColorRoomIssuedItemQty($location,$concatOrderNo,$itemId,$crnNo,$crnYear,$deci)
	{
				//die("$location,$concatOrderNo,$itemId,$crnNo,$crnYear,$deci");

		$cols		= "ROUND(COALESCE(SUM(ink_color_room_issue_details.QTY),0),$deci+2)	AS ISSUE_QTY";
		
		$join 		= " INNER JOIN ink_color_room_issue_header ICRIH
						  ON ink_color_room_issue_details.SERIAL_NO = ICRIH.SERIAL_NO
						  AND ink_color_room_issue_details.SERIAL_YEAR = ICRIH.SERIAL_YEAR";
						  
		$where		= " ICRIH.STATUS = 1 
					    AND ink_color_room_issue_details.ITEM_ID = $itemId ";
		if($location!='')				
		$where		.= " AND ICRIH.LOCATION_ID = $location  ";
						
		$where		.= " AND CONCAT(ink_color_room_issue_details.ORDER_NO,'/',ink_color_room_issue_details.ORDER_YEAR,'/',ink_color_room_issue_details.SALES_ORDER_ID) IN ($concatOrderNo)";
		if($crnNo != '')
		{			 
			$where	.= " AND ICRIH.REQUISITION_NO = '$crnNo' 
					   	 AND ICRIH.REQUISITION_YEAR = '$crnYear'";
		}
 		//if($itemId==723)
		//echo "select ".$cols." from ink_color_room_issue_details ".$join." where ".$where;
		
		$result 	= $this->select($cols,$join,$where, $order = null, $limit = null);	
		$row		= mysqli_fetch_array($result);
		return $row['ISSUE_QTY'];
	}	
	
	public function getIssueDetails_result($serialNo,$serialYear,$concatOrderNo,$itemId)
	{
		$cols	= "	ink_color_room_issue_details.ITEM_ID,
					ink_color_room_issue_details.QTY";

		$where	= " ink_color_room_issue_details.SERIAL_NO = '$serialNo' AND
					ink_color_room_issue_details.SERIAL_YEAR = '$serialYear' AND
					ink_color_room_issue_details.ITEM_ID = '$itemId' AND
					CONCAT(ink_color_room_issue_details.ORDER_NO,'/',ink_color_room_issue_details.ORDER_YEAR,'/',ink_color_room_issue_details.SALES_ORDER_ID) IN ($concatOrderNo)";
		
		$result	= $this->select($cols, $join=null,$where, $order = null, $limit = null);
		return $result;
							
	}
}
?>
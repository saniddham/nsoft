<?php
//THIS CLASS MANUALLY EDITED BY LAHIRU
class ink_actual_production_details{
 
	private $db;
	private $table= "ink_actual_production_details";
	
	//private property
	private $SERIAL_NO;
	private $SERIAL_YEAR;
	private $ORDER_NO;
	private $ORDER_YEAR;
	private $SALES_ORDER_ID;
	private $QTY;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('SERIAL_NO'=>'SERIAL_NO',
										'SERIAL_YEAR'=>'SERIAL_YEAR',
										'ORDER_NO'=>'ORDER_NO',
										'ORDER_YEAR'=>'ORDER_YEAR',
										'SALES_ORDER_ID'=>'SALES_ORDER_ID',
										'QTY'=>'QTY',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "SERIAL_NO = ".$this->SERIAL_NO." and SERIAL_YEAR = ".$this->SERIAL_YEAR." and ORDER_NO = ".$this->ORDER_NO." and ORDER_YEAR = ".$this->ORDER_YEAR." and SALES_ORDER_ID = ".$this->SALES_ORDER_ID."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun SERIAL_NO
	function getSERIAL_NO()
	{
		$this->validate();
		return $this->SERIAL_NO;
	}
	
	//retun SERIAL_YEAR
	function getSERIAL_YEAR()
	{
		$this->validate();
		return $this->SERIAL_YEAR;
	}
	
	//retun ORDER_NO
	function getORDER_NO()
	{
		$this->validate();
		return $this->ORDER_NO;
	}
	
	//retun ORDER_YEAR
	function getORDER_YEAR()
	{
		$this->validate();
		return $this->ORDER_YEAR;
	}
	
	//retun SALES_ORDER_ID
	function getSALES_ORDER_ID()
	{
		$this->validate();
		return $this->SALES_ORDER_ID;
	}
	
	//retun QTY
	function getQTY()
	{
		$this->validate();
		return $this->QTY;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set SERIAL_NO
	function setSERIAL_NO($SERIAL_NO)
	{
		array_push($this->commitArray,'SERIAL_NO');
		$this->SERIAL_NO = $SERIAL_NO;
	}
	
	//set SERIAL_YEAR
	function setSERIAL_YEAR($SERIAL_YEAR)
	{
		array_push($this->commitArray,'SERIAL_YEAR');
		$this->SERIAL_YEAR = $SERIAL_YEAR;
	}
	
	//set ORDER_NO
	function setORDER_NO($ORDER_NO)
	{
		array_push($this->commitArray,'ORDER_NO');
		$this->ORDER_NO = $ORDER_NO;
	}
	
	//set ORDER_YEAR
	function setORDER_YEAR($ORDER_YEAR)
	{
		array_push($this->commitArray,'ORDER_YEAR');
		$this->ORDER_YEAR = $ORDER_YEAR;
	}
	
	//set SALES_ORDER_ID
	function setSALES_ORDER_ID($SALES_ORDER_ID)
	{
		array_push($this->commitArray,'SALES_ORDER_ID');
		$this->SALES_ORDER_ID = $SALES_ORDER_ID;
	}
	
	//set QTY
	function setQTY($QTY)
	{
		array_push($this->commitArray,'QTY');
		$this->QTY = $QTY;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->SERIAL_NO=='' || $this->SERIAL_YEAR=='' || $this->ORDER_NO=='' || $this->ORDER_YEAR=='' || $this->SALES_ORDER_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($SERIAL_NO , $SERIAL_YEAR , $ORDER_NO , $ORDER_YEAR , $SALES_ORDER_ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "SERIAL_NO='$SERIAL_NO' and SERIAL_YEAR='$SERIAL_YEAR' and ORDER_NO='$ORDER_NO' and ORDER_YEAR='$ORDER_YEAR' and SALES_ORDER_ID='$SALES_ORDER_ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($SERIAL_NO,$SERIAL_YEAR,$ORDER_NO,$ORDER_YEAR,$SALES_ORDER_ID,$QTY){
		$data = array('SERIAL_NO'=>$SERIAL_NO 
				,'SERIAL_YEAR'=>$SERIAL_YEAR 
				,'ORDER_NO'=>$ORDER_NO 
				,'ORDER_YEAR'=>$ORDER_YEAR 
				,'SALES_ORDER_ID'=>$SALES_ORDER_ID 
				,'QTY'=>$QTY 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('SERIAL_NO,SERIAL_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['SERIAL_NO'])
				$html .= '<option selected="selected" value="'.$row['SERIAL_NO'].'">'.$row['SERIAL_YEAR'].'</option>';	
			else
				$html .= '<option value="'.$row['SERIAL_NO'].'">'.$row['SERIAL_YEAR'].'</option>';	
		}
		return $html;
	}
	
	public function getConfirmedTotProductionQty($location,$orderNo,$orderYear,$salesOrderId)
	{
		$cols	= " COALESCE(SUM(QTY),0) as ACTUAL_QTY ";
		
		$join	= " INNER JOIN ink_actual_production_header APH ON APH.SERIAL_NO=ink_actual_production_details.SERIAL_NO AND
					APH.SERIAL_YEAR = ink_actual_production_details.SERIAL_YEAR ";
		
		$where	= " APH.LOCATION_ID = '".$location."' AND 
					APH.STATUS = 1 AND 
					ink_actual_production_details.ORDER_NO = '".$orderNo."' AND
					ink_actual_production_details.ORDER_YEAR = '".$orderYear."' AND
					ink_actual_production_details.SALES_ORDER_ID = '".$salesOrderId."' ";
		
		$result 	= $this->select($cols,$join,$where, $order = null, $limit = null);	
		$row		= mysqli_fetch_array($result);
		
		return $row['ACTUAL_QTY'];
	}
	
	public function getConfirmedNormalTotProductionQty($location,$orderNo,$orderYear,$salesOrderId)
	{
		$cols	= " COALESCE(SUM(QTY),0) as ACTUAL_QTY ";
		
		$join	= " INNER JOIN ink_actual_production_header APH ON APH.SERIAL_NO=ink_actual_production_details.SERIAL_NO AND
					APH.SERIAL_YEAR = ink_actual_production_details.SERIAL_YEAR ";
		
		$where	= " APH.RE_PRINT = 0 AND 
					APH.STATUS = 1  "; 
		if($location!='')
		$where	.= " AND APH.LOCATION_ID = '".$location."' ";
					
		$where	.= " AND ink_actual_production_details.ORDER_NO = '".$orderNo."' AND
					ink_actual_production_details.ORDER_YEAR = '".$orderYear."' ";
		if($salesOrderId != ''){
		$where	.= " AND ink_actual_production_details.SALES_ORDER_ID = '".$salesOrderId."' ";
		}
		
		//echo "select ".$cols." from ink_actual_production_details ".$join." where ".$where;
		$result 	= $this->select($cols,$join,$where, $order = null, $limit = null);	
		$row		= mysqli_fetch_array($result);
		
		return $row['ACTUAL_QTY'];
	}
	
 	
	
	
	public function getDetailData($serialNo,$serialYear)
	{
		$cols	= " ORDER_NO,
					ORDER_YEAR,
					SALES_ORDER_ID,
					QTY,
					OD.intPart,
					MP.strName AS PART,
					OD.strSalesOrderNo ";
		
		$join	= " INNER JOIN trn_orderdetails OD ON OD.intOrderNo=ink_actual_production_details.ORDER_NO AND
					OD.intOrderYear=ink_actual_production_details.ORDER_YEAR AND
					OD.intSalesOrderId=ink_actual_production_details.SALES_ORDER_ID
					INNER JOIN mst_part MP ON MP.intId=OD.intPart ";
		
		$where	= " ink_actual_production_details.SERIAL_NO = '".$serialNo."' AND 
					ink_actual_production_details.SERIAL_YEAR = '".$serialYear."' ";
		
		$result = $this->select($cols,$join,$where,$order=null,$limit=null);
	
		return $result;	
	}
	
	public function getProcessProductionPcs($concatProcessId)
	{
		$cols	= " ORDER_NO,
					  ORDER_YEAR,
					  SALES_ORDER_ID,
					  SUM(QTY)       AS PCS";
		
		$join	= "INNER JOIN ink_actual_production_header H
					ON ink_actual_production_details.SERIAL_NO = H.SERIAL_NO
					  AND ink_actual_production_details.SERIAL_YEAR = H.SERIAL_YEAR";
		
		$where	= "DAY_END_PROCESS_ID IN($concatProcessId)
					GROUP BY ORDER_NO,ORDER_YEAR,SALES_ORDER_ID";
		//echo "select ".$cols." from ink_actual_production_details ".$join." where ".$where;
		return $this->select($cols,$join,$where,$order=null,$limit=null);
	}
	//END }
}
?>
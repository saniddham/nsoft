
<?php

class mst_customer_locations{
 
	private $db;
	private $table= "mst_customer_locations";
	
	//private property
	private $intLocationId;
	private $intCustomerId;
	private $strEmailAddress;
	private $strCCEmailAddress;
	private $intCreator;
	private $dtmCreateDate;
	private $intModifyer;
	private $dtmModifyDate;
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->db->field_array_set(array('intLocationId'=>'intLocationId',
										'intCustomerId'=>'intCustomerId',
										'strEmailAddress'=>'strEmailAddress',
										'strCCEmailAddress'=>'strCCEmailAddress',
										'intCreator'=>'intCreator',
										'dtmCreateDate'=>'dtmCreateDate',
										'intModifyer'=>'intModifyer',
										'dtmModifyDate'=>'dtmModifyDate',
										));
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  
		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	//public functions for private properties
	
	//retun intLocationId
	function getintLocationId()
	{
		$this->validate();
		return $this->intLocationId;
	}
	
	//retun intCustomerId
	function getintCustomerId()
	{
		$this->validate();
		return $this->intCustomerId;
	}
	
	//retun strEmailAddress
	function getstrEmailAddress()
	{
		$this->validate();
		return $this->strEmailAddress;
	}
	
	//retun strCCEmailAddress
	function getstrCCEmailAddress()
	{
		$this->validate();
		return $this->strCCEmailAddress;
	}
	
	//retun intCreator
	function getintCreator()
	{
		$this->validate();
		return $this->intCreator;
	}
	
	//retun dtmCreateDate
	function getdtmCreateDate()
	{
		$this->validate();
		return $this->dtmCreateDate;
	}
	
	//retun intModifyer
	function getintModifyer()
	{
		$this->validate();
		return $this->intModifyer;
	}
	
	//retun dtmModifyDate
	function getdtmModifyDate()
	{
		$this->validate();
		return $this->dtmModifyDate;
	}
	
	//validate primary values
	private function validate()
	{
		if($this->intLocationId=='' || $this->intCustomerId=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
		}
	}
	
	public function set($intLocationId , $intCustomerId)
	{
		$result = $this->select('*',null,"intLocationId='$intLocationId' and intCustomerId='$intCustomerId'");	
		if($this->db->numRows()>0)
			$this->setVariables($result);
		else
			throw new Exception("Record not found.");
			
	}
	
	
}
?>


<?php

class ware_fabricdispatchdetails{
 
	private $db;
	private $table= "ware_fabricdispatchdetails";
	
	//private property
	private $intBulkDispatchNo;
	private $intBulkDispatchNoYear;
	private $strCutNo;
	private $intSalesOrderId;
	private $intPart;
	private $intGroundColor;
	private $strLineNo;
	private $strSize;
	private $dblSampleQty;
	private $dblGoodQty;
	private $dblEmbroideryQty;
	private $dblPDammageQty;
	private $dblFdammageQty;
	private $dblCutRetQty;
	private $strRemarks;
	private $invoiced;
	private $strReferenceNo;
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->db->field_array_set(array('intBulkDispatchNo'=>'intBulkDispatchNo',
										'intBulkDispatchNoYear'=>'intBulkDispatchNoYear',
										'strCutNo'=>'strCutNo',
										'intSalesOrderId'=>'intSalesOrderId',
										'intPart'=>'intPart',
										'intGroundColor'=>'intGroundColor',
										'strLineNo'=>'strLineNo',
										'strSize'=>'strSize',
										'dblSampleQty'=>'dblSampleQty',
										'dblGoodQty'=>'dblGoodQty',
										'dblEmbroideryQty'=>'dblEmbroideryQty',
										'dblPDammageQty'=>'dblPDammageQty',
										'dblFdammageQty'=>'dblFdammageQty',
										'dblCutRetQty'=>'dblCutRetQty',
										'strRemarks'=>'strRemarks',
										'invoiced'=>'invoiced',
										'strReferenceNo'=>'strReferenceNo',
										));
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  
		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	//public functions for private properties
	
	//retun intBulkDispatchNo
	function getintBulkDispatchNo()
	{
		$this->validate();
		return $this->intBulkDispatchNo;
	}
	
	//retun intBulkDispatchNoYear
	function getintBulkDispatchNoYear()
	{
		$this->validate();
		return $this->intBulkDispatchNoYear;
	}
	
	//retun strCutNo
	function getstrCutNo()
	{
		$this->validate();
		return $this->strCutNo;
	}
	
	//retun intSalesOrderId
	function getintSalesOrderId()
	{
		$this->validate();
		return $this->intSalesOrderId;
	}
	
	//retun intPart
	function getintPart()
	{
		$this->validate();
		return $this->intPart;
	}
	
	//retun intGroundColor
	function getintGroundColor()
	{
		$this->validate();
		return $this->intGroundColor;
	}
	
	//retun strLineNo
	function getstrLineNo()
	{
		$this->validate();
		return $this->strLineNo;
	}
	
	//retun strSize
	function getstrSize()
	{
		$this->validate();
		return $this->strSize;
	}
	
	//retun dblSampleQty
	function getdblSampleQty()
	{
		$this->validate();
		return $this->dblSampleQty;
	}
	
	//retun dblGoodQty
	function getdblGoodQty()
	{
		$this->validate();
		return $this->dblGoodQty;
	}
	
	//retun dblEmbroideryQty
	function getdblEmbroideryQty()
	{
		$this->validate();
		return $this->dblEmbroideryQty;
	}
	
	//retun dblPDammageQty
	function getdblPDammageQty()
	{
		$this->validate();
		return $this->dblPDammageQty;
	}
	
	//retun dblFdammageQty
	function getdblFdammageQty()
	{
		$this->validate();
		return $this->dblFdammageQty;
	}
	
	//retun dblCutRetQty
	function getdblCutRetQty()
	{
		$this->validate();
		return $this->dblCutRetQty;
	}
	
	//retun strRemarks
	function getstrRemarks()
	{
		$this->validate();
		return $this->strRemarks;
	}
	
	//retun invoiced
	function getinvoiced()
	{
		$this->validate();
		return $this->invoiced;
	}
	
	//retun strReferenceNo
	function getstrReferenceNo()
	{
		$this->validate();
		return $this->strReferenceNo;
	}
	
	//validate primary values
	private function validate()
	{
		if($this->intBulkDispatchNo=='' || $this->intBulkDispatchNoYear=='' || $this->strCutNo=='' || $this->intSalesOrderId=='' || $this->strSize=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
		}
	}
	
	public function set($intBulkDispatchNo , $intBulkDispatchNoYear , $strCutNo , $intSalesOrderId , $strSize)
	{
		$result = $this->select('*',null,"intBulkDispatchNo='$intBulkDispatchNo' and intBulkDispatchNoYear='$intBulkDispatchNoYear' and strCutNo='$strCutNo' and intSalesOrderId='$intSalesOrderId' and strSize='$strSize'");	
		if($this->db->numRows()>0)
			$this->setVariables($result);
		else
			throw new Exception("Record not found.");
			
	}
	
#BEGIN - USER DEFINED FUNCTION {
	public function getOrderWiseDispatchDetails($orderNo,$orderYear,$location,$salesOrder=null,$size = null)
	{
		$sql 	=   "SELECT
					IFNULL(SUM(ware_fabricdispatchdetails.dblSampleQty + ware_fabricdispatchdetails.dblGoodQty + ware_fabricdispatchdetails.dblEmbroideryQty + ware_fabricdispatchdetails.dblPDammageQty + ware_fabricdispatchdetails.dblFdammageQty+ware_fabricdispatchdetails.dblCutRetQty),0) AS dispatchQty,
					(SELECT
					IFNULL(SUM(ware_fabricdispatchconfirmation_details.SAMPLE_QTY+ware_fabricdispatchconfirmation_details.GOOD_QTY+ware_fabricdispatchconfirmation_details.EMBROIDERY_QTY+ware_fabricdispatchconfirmation_details.P_DAMMAGE_QTY+ware_fabricdispatchconfirmation_details.F_DAMMAGE_QTY+ware_fabricdispatchconfirmation_details.CUT_RETURN_QTY),0)
					FROM
					ware_fabricdispatchconfirmation_details
					INNER JOIN ware_fabricdispatchconfirmation_header ON ware_fabricdispatchconfirmation_details.DISP_CONFIRM_NO = ware_fabricdispatchconfirmation_header.DISP_CONFIRM_NO AND ware_fabricdispatchconfirmation_details.DISP_CONFIRM_YEAR = ware_fabricdispatchconfirmation_header.DISP_CONFIRM_YEAR
					WHERE
					ware_fabricdispatchconfirmation_header.ORDER_NO = ware_fabricdispatchheader.intOrderNo AND
					ware_fabricdispatchconfirmation_header.ORDER_YEAR = ware_fabricdispatchheader.intOrderYear AND
					ware_fabricdispatchconfirmation_details.SALES_ORDER_ID = ware_fabricdispatchdetails.intSalesOrderId AND
					ware_fabricdispatchconfirmation_header.`STATUS` = 1 AND 
					ware_fabricdispatchconfirmation_details.SIZE = ware_fabricdispatchdetails.strSize AND 
					ware_fabricdispatchconfirmation_header.LOCATION_ID = ware_fabricdispatchheader.intCompanyId
					) AS confirmQty,
					ware_fabricdispatchdetails.strSize,
					ware_fabricdispatchdetails.intPart,
					ware_fabricdispatchdetails.intSalesOrderId,
					IFNULL(SUM(ware_fabricdispatchdetails.dblSampleQty),0) AS sample_qty,
					IFNULL(SUM(ware_fabricdispatchdetails.dblGoodQty),0) AS good_qty,
					IFNULL(SUM(ware_fabricdispatchdetails.dblPDammageQty),0) AS p_damage_qty,
					IFNULL(SUM(ware_fabricdispatchdetails.dblEmbroideryQty),0) AS embroidery_qty,
					IFNULL(SUM(ware_fabricdispatchdetails.dblFdammageQty),0) AS f_damage_qty,
					IFNULL(SUM(ware_fabricdispatchdetails.dblCutRetQty),0) AS cut_return_qty
					FROM
					ware_fabricdispatchdetails
					INNER JOIN ware_fabricdispatchheader ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
					WHERE
					ware_fabricdispatchheader.intOrderNo = '$orderNo' AND
					ware_fabricdispatchheader.intOrderYear = '$orderYear' AND
					ware_fabricdispatchheader.intCompanyId = '$location'";
			if($salesOrder != NULL || $salesOrder !='')
				$sql  .= " AND ware_fabricdispatchdetails.intSalesOrderId = '$salesOrder'";
			if($size != NULL)	
				$sql  .= " AND ware_fabricdispatchdetails.strSize = '$size'";
			$sql  .= "GROUP BY
					ware_fabricdispatchheader.intOrderYear,
					ware_fabricdispatchheader.intOrderNo,
					ware_fabricdispatchdetails.intSalesOrderId,
					ware_fabricdispatchdetails.strSize
					HAVING
					(dispatchQty - confirmQty)>0 ";	

		$result	= $this->db->RunQuery($sql);
		return $result;
	}
	public function getDispatchOrderWiseQty($orderNo,$orderYear)
	{
	 
		 $cols	= " IFNULL(SUM(ware_fabricdispatchdetails.dblGoodQty),0) AS TOT_DISP_GOOD,
					IFNULL(SUM(ware_fabricdispatchdetails.dblPDammageQty),0) AS TOT_DISP_DAMAGE";
		 $join	= "INNER JOIN ware_fabricdispatchheader 
					 ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo 
					 AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear";
		 $where = " ware_fabricdispatchheader.intOrderNo = '$orderNo' AND
					ware_fabricdispatchheader.intOrderYear = '$orderYear'";
	

		 $result	= $this->select($cols,$join,$where,$order=null,$limit=null);
		 $row		= mysqli_fetch_array($result);
		 return $row;	
	}
	
	public function getDispatchSalesOrderWiseQty($orderNo,$orderYear,$salesOrder)
	{
	 
		 $cols	= " IFNULL(SUM(ware_fabricdispatchdetails.dblGoodQty),0) AS TOT_DISP_GOOD,
					IFNULL(SUM(ware_fabricdispatchdetails.dblPDammageQty),0) AS TOT_DISP_DAMAGE";
		 $join	= "INNER JOIN ware_fabricdispatchheader 
					 ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo 
					 AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear";
		 $where = " ware_fabricdispatchheader.intOrderNo = '$orderNo' AND
					ware_fabricdispatchheader.intOrderYear = '$orderYear'";
		if($salesOrder!='')
		$where  .= " AND ware_fabricdispatchdetails.intSalesOrderId = '$salesOrder' ";

		 $result	= $this->select($cols,$join,$where,$order=null,$limit=null);
		 $row		= mysqli_fetch_array($result);
		 return $row;	
	}
	
	public function getDispatchQty($orderNo,$orderYear,$salesOrderId,$deci)
	{
		if($salesOrderId!='')
			$para	= "AND ware_fabricdispatchdetails.intSalesOrderId = $salesOrderId";
			
		$cols		= "ROUND(COALESCE(SUM(dblGoodQty),0),$deci) AS DISPATCHED_GOOD_QTY,
					   IFNULL(SUM(ware_fabricdispatchdetails.dblSampleQty + ware_fabricdispatchdetails.dblGoodQty + ware_fabricdispatchdetails.dblEmbroideryQty + ware_fabricdispatchdetails.dblPDammageQty + ware_fabricdispatchdetails.dblFdammageQty+ware_fabricdispatchdetails.dblCutRetQty),0) AS dispatchQty";
		
		$join		= "INNER JOIN ware_fabricdispatchheader H
						ON H.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo
						  AND H.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear";
		
		$where		= "H.intStatus = 1
						AND H.intOrderNo = $orderNo
						AND H.intOrderYear = $orderYear
						$para";
		
		$result		= $this->select($cols,$join,$where,$order=null,$limit=null);
		return mysqli_fetch_array($result);
	}
	
	public function getLastAndFirstDate($orderNo,$orderYear,$salesOrderId,$deci)
	{
		if($salesOrderId!='')
			$para	= "AND ware_fabricdispatchdetails.intSalesOrderId = $salesOrderId";
			
		$cols		= " DATE(MAX(dtmCreateDate)) AS MAX_DATE,
  						DATE(MIN(dtmCreateDate)) AS MIN_DATE";
		
		$join		= "INNER JOIN ware_fabricdispatchheader H
						ON H.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo
						  AND H.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear";
		
		$where		= "H.intStatus = 1
						AND H.intOrderNo = $orderNo
						AND H.intOrderYear = $orderYear
						$para";
		
		$result		= $this->select($cols,$join,$where,$order=null,$limit=null);
		return mysqli_fetch_array($result);
	}
	
	public function getSavedOrderWiseDispatchDetails($orderNo,$orderYear,$location,$salesOrder=null,$size = null)
	{
		$sql 	=   "SELECT
					IFNULL(SUM(ware_fabricdispatchdetails.dblSampleQty + ware_fabricdispatchdetails.dblGoodQty + ware_fabricdispatchdetails.dblEmbroideryQty + ware_fabricdispatchdetails.dblPDammageQty + ware_fabricdispatchdetails.dblFdammageQty+ware_fabricdispatchdetails.dblCutRetQty),0) AS dispatchQty,
					(SELECT
					IFNULL(SUM(ware_fabricdispatchconfirmation_details.SAMPLE_QTY+ware_fabricdispatchconfirmation_details.GOOD_QTY+ware_fabricdispatchconfirmation_details.EMBROIDERY_QTY+ware_fabricdispatchconfirmation_details.P_DAMMAGE_QTY+ware_fabricdispatchconfirmation_details.F_DAMMAGE_QTY+ware_fabricdispatchconfirmation_details.CUT_RETURN_QTY),0)
					FROM
					ware_fabricdispatchconfirmation_details
					INNER JOIN ware_fabricdispatchconfirmation_header ON ware_fabricdispatchconfirmation_details.DISP_CONFIRM_NO = ware_fabricdispatchconfirmation_header.DISP_CONFIRM_NO AND ware_fabricdispatchconfirmation_details.DISP_CONFIRM_YEAR = ware_fabricdispatchconfirmation_header.DISP_CONFIRM_YEAR
					WHERE
					ware_fabricdispatchconfirmation_header.ORDER_NO = ware_fabricdispatchheader.intOrderNo AND
					ware_fabricdispatchconfirmation_header.ORDER_YEAR = ware_fabricdispatchheader.intOrderYear AND
					ware_fabricdispatchconfirmation_details.SALES_ORDER_ID = ware_fabricdispatchdetails.intSalesOrderId AND
					ware_fabricdispatchconfirmation_header.`STATUS` = 1 AND 
					ware_fabricdispatchconfirmation_details.SIZE = ware_fabricdispatchdetails.strSize AND 
					ware_fabricdispatchconfirmation_header.LOCATION_ID = ware_fabricdispatchheader.intCompanyId
					) AS confirmQty
					FROM
					ware_fabricdispatchdetails
					INNER JOIN ware_fabricdispatchheader ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
					WHERE
					ware_fabricdispatchheader.intOrderNo = '$orderNo' AND
					ware_fabricdispatchheader.intOrderYear = '$orderYear' AND
					ware_fabricdispatchheader.intCompanyId = '$location'";
			if($salesOrder != NULL || $salesOrder !='')
				$sql  .= " AND ware_fabricdispatchdetails.intSalesOrderId = '$salesOrder'";
			if($size != NULL)	
				$sql  .= " AND ware_fabricdispatchdetails.strSize = '$size'";
			$sql  .= "GROUP BY
					ware_fabricdispatchheader.intOrderYear,
					ware_fabricdispatchheader.intOrderNo,
					ware_fabricdispatchdetails.intSalesOrderId,
					ware_fabricdispatchdetails.strSize
					";	
		$result	= $this->db->RunQuery($sql);
		return $result;
	}
#END 	- USER DEFINED FUNCTION }	
}
?>

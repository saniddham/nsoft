<?php
class finance_supplier_creditnote_gl{
 
	private $db;
	private $table= "finance_supplier_creditnote_gl";
	
	//private property
	private $CREDIT_NO;
	private $CREDIT_YEAR;
	private $CHART_OF_ACCOUNT_ID;
	private $AMOUNT;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('CREDIT_NO'=>'CREDIT_NO',
										'CREDIT_YEAR'=>'CREDIT_YEAR',
										'CHART_OF_ACCOUNT_ID'=>'CHART_OF_ACCOUNT_ID',
										'AMOUNT'=>'AMOUNT',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "CREDIT_NO = ".$this->CREDIT_NO." and CREDIT_YEAR = ".$this->CREDIT_YEAR." and CHART_OF_ACCOUNT_ID = ".$this->CHART_OF_ACCOUNT_ID."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun CREDIT_NO
	function getCREDIT_NO()
	{
		$this->validate();
		return $this->CREDIT_NO;
	}
	
	//retun CREDIT_YEAR
	function getCREDIT_YEAR()
	{
		$this->validate();
		return $this->CREDIT_YEAR;
	}
	
	//retun CHART_OF_ACCOUNT_ID
	function getCHART_OF_ACCOUNT_ID()
	{
		$this->validate();
		return $this->CHART_OF_ACCOUNT_ID;
	}
	
	//retun AMOUNT
	function getAMOUNT()
	{
		$this->validate();
		return $this->AMOUNT;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set CREDIT_NO
	function setCREDIT_NO($CREDIT_NO)
	{
		array_push($this->commitArray,'CREDIT_NO');
		$this->CREDIT_NO = $CREDIT_NO;
	}
	
	//set CREDIT_YEAR
	function setCREDIT_YEAR($CREDIT_YEAR)
	{
		array_push($this->commitArray,'CREDIT_YEAR');
		$this->CREDIT_YEAR = $CREDIT_YEAR;
	}
	
	//set CHART_OF_ACCOUNT_ID
	function setCHART_OF_ACCOUNT_ID($CHART_OF_ACCOUNT_ID)
	{
		array_push($this->commitArray,'CHART_OF_ACCOUNT_ID');
		$this->CHART_OF_ACCOUNT_ID = $CHART_OF_ACCOUNT_ID;
	}
	
	//set AMOUNT
	function setAMOUNT($AMOUNT)
	{
		array_push($this->commitArray,'AMOUNT');
		$this->AMOUNT = $AMOUNT;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->CREDIT_NO=='' || $this->CREDIT_YEAR=='' || $this->CHART_OF_ACCOUNT_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($CREDIT_NO , $CREDIT_YEAR , $CHART_OF_ACCOUNT_ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "CREDIT_NO='$CREDIT_NO' and CREDIT_YEAR='$CREDIT_YEAR' and CHART_OF_ACCOUNT_ID='$CHART_OF_ACCOUNT_ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($CREDIT_NO,$CREDIT_YEAR,$CHART_OF_ACCOUNT_ID,$AMOUNT){
		$data = array('CREDIT_NO'=>$CREDIT_NO 
				,'CREDIT_YEAR'=>$CREDIT_YEAR 
				,'CHART_OF_ACCOUNT_ID'=>$CHART_OF_ACCOUNT_ID 
				,'AMOUNT'=>$AMOUNT 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('CREDIT_NO,CREDIT_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['CREDIT_NO'])
				$html .= '<option selected="selected" value="'.$row['CREDIT_NO'].'">'.$row['CREDIT_YEAR'].'</option>';	
			else
				$html .= '<option value="'.$row['CREDIT_NO'].'">'.$row['CREDIT_YEAR'].'</option>';	
		}
		return $html;
	}
	
#BEGIN 	- USER DEFINED FUNCTIONS {
	public function getSavedDetails($creditNo,$creditYear)
	{
		$cols	= " CHART_OF_ACCOUNT_ID,AMOUNT,
					(
					SELECT GROUP_CONCAT(COAT.TAX_ID)
					FROM finance_mst_chartofaccount_tax AS COAT
					WHERE COAT.CHART_OF_ACCOUNT_ID = finance_supplier_creditnote_gl.CHART_OF_ACCOUNT_ID
					) AS TAX_ID ";
		
		$where	= " CREDIT_NO = '".$creditNo."' AND 
					CREDIT_YEAR = '".$creditYear."' ";
		
		return $this->select($cols,$join=null,$where);
	}
#END 	- USER DEFINED FUNCTIONS }
}
?>
<?php
class ware_fabricgatepassheader{
 
	private $db;
	private $table= "ware_fabricgatepassheader";
	
	//private property
	private $intFabricGatePassNo;
	private $intFabricGatePassYear;
	private $intGatepassTo;
	private $intOrderNo;
	private $intOrderYear;
	private $strAODNo;
	private $strRemarks;
	private $intStatus;
	private $intApproveLevels;
	private $dtmdate;
	private $dtmCreateDate;
	private $intCteatedBy;
	private $dtmModifiedDate;
	private $intModifiedBy;
	private $intCompanyId;
	private $commitArray = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->db->field_array_set(array('intFabricGatePassNo'=>'intFabricGatePassNo',
										'intFabricGatePassYear'=>'intFabricGatePassYear',
										'intGatepassTo'=>'intGatepassTo',
										'intOrderNo'=>'intOrderNo',
										'intOrderYear'=>'intOrderYear',
										'strAODNo'=>'strAODNo',
										'strRemarks'=>'strRemarks',
										'intStatus'=>'intStatus',
										'intApproveLevels'=>'intApproveLevels',
										'dtmdate'=>'dtmdate',
										'dtmCreateDate'=>'dtmCreateDate',
										'intCteatedBy'=>'intCteatedBy',
										'dtmModifiedDate'=>'dtmModifiedDate',
										'intModifiedBy'=>'intModifiedBy',
										'intCompanyId'=>'intCompanyId',
										));
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function commit()
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = ''.$this->$v.'';
		}
		$where		= "intFabricGatePassNo = '".$this->intFabricGatePassNo."' and intFabricGatePassYear = '".$this->intFabricGatePassYear."'" ;
		unset($this->commitArray);
		return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun intFabricGatePassNo
	function getintFabricGatePassNo()
	{
		$this->validate();
		return $this->intFabricGatePassNo;
	}
	
	//retun intFabricGatePassYear
	function getintFabricGatePassYear()
	{
		$this->validate();
		return $this->intFabricGatePassYear;
	}
	
	//retun intGatepassTo
	function getintGatepassTo()
	{
		$this->validate();
		return $this->intGatepassTo;
	}
	
	//retun intOrderNo
	function getintOrderNo()
	{
		$this->validate();
		return $this->intOrderNo;
	}
	
	//retun intOrderYear
	function getintOrderYear()
	{
		$this->validate();
		return $this->intOrderYear;
	}
	
	//retun strAODNo
	function getstrAODNo()
	{
		$this->validate();
		return $this->strAODNo;
	}
	
	//retun strRemarks
	function getstrRemarks()
	{
		$this->validate();
		return $this->strRemarks;
	}
	
	//retun intStatus
	function getintStatus()
	{
		$this->validate();
		return $this->intStatus;
	}
	
	//retun intApproveLevels
	function getintApproveLevels()
	{
		$this->validate();
		return $this->intApproveLevels;
	}
	
	//retun dtmdate
	function getdtmdate()
	{
		$this->validate();
		return $this->dtmdate;
	}
	
	//retun dtmCreateDate
	function getdtmCreateDate()
	{
		$this->validate();
		return $this->dtmCreateDate;
	}
	
	//retun intCteatedBy
	function getintCteatedBy()
	{
		$this->validate();
		return $this->intCteatedBy;
	}
	
	//retun dtmModifiedDate
	function getdtmModifiedDate()
	{
		$this->validate();
		return $this->dtmModifiedDate;
	}
	
	//retun intModifiedBy
	function getintModifiedBy()
	{
		$this->validate();
		return $this->intModifiedBy;
	}
	
	//retun intCompanyId
	function getintCompanyId()
	{
		$this->validate();
		return $this->intCompanyId;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set intFabricGatePassNo
	function setintFabricGatePassNo($intFabricGatePassNo)
	{
		array_push($this->commitArray,'intFabricGatePassNo');
		$this->intFabricGatePassNo = $intFabricGatePassNo;
	}
	
	//set intFabricGatePassYear
	function setintFabricGatePassYear($intFabricGatePassYear)
	{
		array_push($this->commitArray,'intFabricGatePassYear');
		$this->intFabricGatePassYear = $intFabricGatePassYear;
	}
	
	//set intGatepassTo
	function setintGatepassTo($intGatepassTo)
	{
		array_push($this->commitArray,'intGatepassTo');
		$this->intGatepassTo = $intGatepassTo;
	}
	
	//set intOrderNo
	function setintOrderNo($intOrderNo)
	{
		array_push($this->commitArray,'intOrderNo');
		$this->intOrderNo = $intOrderNo;
	}
	
	//set intOrderYear
	function setintOrderYear($intOrderYear)
	{
		array_push($this->commitArray,'intOrderYear');
		$this->intOrderYear = $intOrderYear;
	}
	
	//set strAODNo
	function setstrAODNo($strAODNo)
	{
		array_push($this->commitArray,'strAODNo');
		$this->strAODNo = $strAODNo;
	}
	
	//set strRemarks
	function setstrRemarks($strRemarks)
	{
		array_push($this->commitArray,'strRemarks');
		$this->strRemarks = $strRemarks;
	}
	
	//set intStatus
	function setintStatus($intStatus)
	{
		array_push($this->commitArray,'intStatus');
		$this->intStatus = $intStatus;
	}
	
	//set intApproveLevels
	function setintApproveLevels($intApproveLevels)
	{
		array_push($this->commitArray,'intApproveLevels');
		$this->intApproveLevels = $intApproveLevels;
	}
	
	//set dtmdate
	function setdtmdate($dtmdate)
	{
		array_push($this->commitArray,'dtmdate');
		$this->dtmdate = $dtmdate;
	}
	
	//set dtmCreateDate
	function setdtmCreateDate($dtmCreateDate)
	{
		array_push($this->commitArray,'dtmCreateDate');
		$this->dtmCreateDate = $dtmCreateDate;
	}
	
	//set intCteatedBy
	function setintCteatedBy($intCteatedBy)
	{
		array_push($this->commitArray,'intCteatedBy');
		$this->intCteatedBy = $intCteatedBy;
	}
	
	//set dtmModifiedDate
	function setdtmModifiedDate($dtmModifiedDate)
	{
		array_push($this->commitArray,'dtmModifiedDate');
		$this->dtmModifiedDate = $dtmModifiedDate;
	}
	
	//set intModifiedBy
	function setintModifiedBy($intModifiedBy)
	{
		array_push($this->commitArray,'intModifiedBy');
		$this->intModifiedBy = $intModifiedBy;
	}
	
	//set intCompanyId
	function setintCompanyId($intCompanyId)
	{
		array_push($this->commitArray,'intCompanyId');
		$this->intCompanyId = $intCompanyId;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->intFabricGatePassNo=='' || $this->intFabricGatePassYear=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
		}
	}
	
	public function set($intFabricGatePassNo , $intFabricGatePassYear)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "intFabricGatePassNo='$intFabricGatePassNo' and intFabricGatePassYear='$intFabricGatePassYear'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			$this->setVariables($result);
		else
			throw new Exception("Record not found.");
	}	
	
	
	//END }
}
?>
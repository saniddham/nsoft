
<?php

class ware_stocktransactions_fabric{
 
	private $db;
	private $table= "ware_stocktransactions_fabric";
	
	//private property
	private $intId;
	private $intLocationId;
	private $strPlace;
	private $intToLocationId;
	private $intParentDocumentNo;
	private $intParentDocumentYear;
	private $intDocumentNo;
	private $intDocumentYear;
	private $intOrderNo;
	private $intOrderYear;
	private $strCutNo;
	private $intSalesOrderId;
	private $intPart;
	private $strSize;
	private $intGrade;
	private $dblQty;
	private $strType;
	private $intUser;
	private $dtDate;
	private $invoiced;
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->db->field_array_set(array('intId'=>'intId',
										'intLocationId'=>'intLocationId',
										'strPlace'=>'strPlace',
										'intToLocationId'=>'intToLocationId',
										'intParentDocumentNo'=>'intParentDocumentNo',
										'intParentDocumentYear'=>'intParentDocumentYear',
										'intDocumentNo'=>'intDocumentNo',
										'intDocumentYear'=>'intDocumentYear',
										'intOrderNo'=>'intOrderNo',
										'intOrderYear'=>'intOrderYear',
										'strCutNo'=>'strCutNo',
										'intSalesOrderId'=>'intSalesOrderId',
										'intPart'=>'intPart',
										'strSize'=>'strSize',
										'intGrade'=>'intGrade',
										'dblQty'=>'dblQty',
										'strType'=>'strType',
										'intUser'=>'intUser',
										'dtDate'=>'dtDate',
										'invoiced'=>'invoiced'
										));
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	public function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  
		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	//public functions for private properties
	
	//retun intId
	function getintId()
	{
		$this->validate();
		return $this->intId;
	}
	
	//retun intLocationId
	function getintLocationId()
	{
		$this->validate();
		return $this->intLocationId;
	}
	
	//retun strPlace
	function getstrPlace()
	{
		$this->validate();
		return $this->strPlace;
	}
	
	//retun intToLocationId
	function getintToLocationId()
	{
		$this->validate();
		return $this->intToLocationId;
	}
	
	//retun intParentDocumentNo
	function getintParentDocumentNo()
	{
		$this->validate();
		return $this->intParentDocumentNo;
	}
	
	//retun intParentDocumentYear
	function getintParentDocumentYear()
	{
		$this->validate();
		return $this->intParentDocumentYear;
	}
	
	//retun intDocumentNo
	function getintDocumentNo()
	{
		$this->validate();
		return $this->intDocumentNo;
	}
	
	//retun intDocumentYear
	function getintDocumentYear()
	{
		$this->validate();
		return $this->intDocumentYear;
	}
	
	//retun intOrderNo
	function getintOrderNo()
	{
		$this->validate();
		return $this->intOrderNo;
	}
	
	//retun intOrderYear
	function getintOrderYear()
	{
		$this->validate();
		return $this->intOrderYear;
	}
	
	//retun strCutNo
	function getstrCutNo()
	{
		$this->validate();
		return $this->strCutNo;
	}
	
	//retun intSalesOrderId
	function getintSalesOrderId()
	{
		$this->validate();
		return $this->intSalesOrderId;
	}
	
	//retun intPart
	function getintPart()
	{
		$this->validate();
		return $this->intPart;
	}
	
	//retun strSize
	function getstrSize()
	{
		$this->validate();
		return $this->strSize;
	}
	
	//retun intGrade
	function getintGrade()
	{
		$this->validate();
		return $this->intGrade;
	}
	
	//retun dblQty
	function getdblQty()
	{
		$this->validate();
		return $this->dblQty;
	}
	
	//retun strType
	function getstrType()
	{
		$this->validate();
		return $this->strType;
	}
	
	//retun intUser
	function getintUser()
	{
		$this->validate();
		return $this->intUser;
	}
	
	//retun dtDate
	function getdtDate()
	{
		$this->validate();
		return $this->dtDate;
	}
	
	//retun invoiced
	function getinvoiced()
	{
		$this->validate();
		return $this->invoiced;
	}
	
	//validate primary values
	private function validate()
	{
		if($this->intId=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result){
		$row = mysqli_fetch_assoc($result);
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
		}
	}
	
	public function set($intId){
		$result = $this->select('*',null,"intId='$intId'");	
		if($this->db->numRows()>0)
			$this->setVariables($result);
		else
			throw new Exception("Record not found.");
			
	}
	
	function insert_rec($docNo,$docYear,$orderNo,$orderYear,$cutNo,$salesOrderId,$partId,$size,$qty,$type){
		$data = array(	'intId'=>'NULL',
						'intLocationId'=>$_SESSION['CompanyID'],
						'strPlace'=>"'Stores'",
						'intToLocationId'=>'0',
						'intParentDocumentNo'=>'NULL',
						'intParentDocumentYear'=>'NULL',
						'intDocumentNo'=>"'$docNo'",
						'intDocumentYear'=>"'$docYear'",
						'intOrderNo'=>"'$orderNo'",
						'intOrderYear'=>"'$orderYear'",
						'strCutNo'=>"'$cutNo'",
						'intSalesOrderId'=>"'$salesOrderId'",
						'intPart'=>"'$partId'",
						'strSize'=>"'$size'",
						'intGrade'=>'NULL',
						'dblQty'=>"'$qty'",
						'strType'=>"'$type'",
						'intUser'=>$_SESSION['userId'],
						'dtDate'=>"'".date('Y-m-d')."'",
						'invoiced'=>'0'
						);
		$result = $this->insert($data);
		return $result;  
	}
	
	//get stock balance qty
	public function getFabricBalanceSumQty($orderNo,$orderYear,$salesOrderId,$cutNo,$partId,$size){
		$result = $this->select("sum(dblQty) as balanceQty",null,"ware_stocktransactions_fabric.intOrderNo = '$orderNo' AND
						ware_stocktransactions_fabric.intOrderYear = '$orderYear' AND
						ware_stocktransactions_fabric.strCutNo = '$cutNo' AND
						ware_stocktransactions_fabric.intSalesOrderId = '$salesOrderId' AND
						ware_stocktransactions_fabric.intPart = '$partId' AND
						ware_stocktransactions_fabric.strSize = '$size'
						GROUP BY
						ware_stocktransactions_fabric.intOrderNo,
						ware_stocktransactions_fabric.intOrderYear,
						ware_stocktransactions_fabric.strCutNo,
						ware_stocktransactions_fabric.intSalesOrderId,
						ware_stocktransactions_fabric.intPart,
						ware_stocktransactions_fabric.strSize");
		$row = mysqli_fetch_array($result); 
		if($row['balanceQty']=='')
			throw new Exception('Invalid fabric balance record set.');
		else
			return round($row['balanceQty']);
	}
}
?>

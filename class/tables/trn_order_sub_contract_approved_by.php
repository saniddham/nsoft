
<?php

class trn_order_sub_contract_approved_by{
 
	private $db;
	private $table= "trn_order_sub_contract_approved_by";
	
	function __construct($db)
	{
		$this->db = $db;
		$this->db->field_array_set(array('SUB_CONTRACT_NO'=>'SUB_CONTRACT_NO',
										'SUB_CONTRACT_YEAR'=>'SUB_CONTRACT_YEAR',
										'APPROVE_LEVEL'=>'APPROVE_LEVEL',
										'APPROVE_BY'=>'APPROVE_BY',
										'APPROVE_DATE'=>'APPROVE_DATE',
										'STATUS'=>'STATUS',
										));
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		return $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  
		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
}
?>

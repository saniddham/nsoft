<?php
class mst_bank{

	private $db;
	private $table= "mst_bank";

	//private property
	private $BANK_ID;
	private $BANK_CODE;
	private $BANK_NAME;
	private $STATUS;
	private $COUNTRY_ID;
	private $CREATED_BY;
	private $CREATED_DATE;
	private $commitArray = array();
	private $field_array = array();
	

	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('BANK_ID'=>'BANK_ID',
										'BANK_CODE'=>'BANK_CODE',
										'BANK_NAME'=>'BANK_NAME',
										'STATUS'=>'STATUS',
										'COUNTRY_ID'=>'COUNTRY_ID',
										'CREATED_BY'=>'CREATED_BY',
										'CREATED_DATE'=>'CREATED_DATE',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit );
		return $result;
	}

	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data);
		return $this->db->getResult();


	}

	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}

	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();
	}

	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}

	public function get_field_array(){
		return $this->field_array;
	}

	function commit($type='update')
	{
		//commit update
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{
			$data[$v] = $this->$v;
		}
		$where		= "BANK_ID = ".$this->BANK_ID."" ;
		$this->commitArray = array();

		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}

	//BEGIN - public functions for private properties {
	
	//retun BANK_ID
	function getBANK_ID()
	{
		$this->validate();
		return $this->BANK_ID;
	}
	
	//retun BANK_CODE
	function getBANK_CODE()
	{
		$this->validate();
		return $this->BANK_CODE;
	}
	
	//retun BANK_NAME
	function getBANK_NAME()
	{
		$this->validate();
		return $this->BANK_NAME;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//retun COUNTRY_ID
	function getCOUNTRY_ID()
	{
		$this->validate();
		return $this->COUNTRY_ID;
	}
	
	//retun CREATED_BY
	function getCREATED_BY()
	{
		$this->validate();
		return $this->CREATED_BY;
	}
	
	//retun CREATED_DATE
	function getCREATED_DATE()
	{
		$this->validate();
		return $this->CREATED_DATE;
	}
	
	//END }

	//BEGIN - public function set {
	
	//set BANK_ID
	function setBANK_ID($BANK_ID)
	{
		array_push($this->commitArray,'BANK_ID');
		$this->BANK_ID = $BANK_ID;
	}
	
	//set BANK_CODE
	function setBANK_CODE($BANK_CODE)
	{
		array_push($this->commitArray,'BANK_CODE');
		$this->BANK_CODE = $BANK_CODE;
	}
	
	//set BANK_NAME
	function setBANK_NAME($BANK_NAME)
	{
		array_push($this->commitArray,'BANK_NAME');
		$this->BANK_NAME = $BANK_NAME;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//set COUNTRY_ID
	function setCOUNTRY_ID($COUNTRY_ID)
	{
		array_push($this->commitArray,'COUNTRY_ID');
		$this->COUNTRY_ID = $COUNTRY_ID;
	}
	
	//set CREATED_BY
	function setCREATED_BY($CREATED_BY)
	{
		array_push($this->commitArray,'CREATED_BY');
		$this->CREATED_BY = $CREATED_BY;
	}
	
	//set CREATED_DATE
	function setCREATED_DATE($CREATED_DATE)
	{
		array_push($this->commitArray,'CREATED_DATE');
		$this->CREATED_DATE = $CREATED_DATE;
	}
	
	//END }

	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->BANK_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}

	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}

	public function set($BANK_ID)
	{
		$cols	= "*";

		$join	= NULL;

		$where	= "BANK_ID='$BANK_ID'";

		$result = $this->select($cols,$join,$where);
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}

	//insert as parameters
	public function insertRec($BANK_ID,$BANK_CODE,$BANK_NAME,$STATUS,$COUNTRY_ID,$CREATED_BY,$CREATED_DATE){
		$data = array('BANK_ID'=>$BANK_ID 
				,'BANK_CODE'=>$BANK_CODE 
				,'BANK_NAME'=>$BANK_NAME 
				,'STATUS'=>$STATUS 
				,'COUNTRY_ID'=>$COUNTRY_ID 
				,'CREATED_BY'=>$CREATED_BY 
				,'CREATED_DATE'=>$CREATED_DATE 
				);
		return $this->insert($data);
	}

	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('BANK_ID,BANK_NAME',  null, $where = $where);
		$html = '<option value=""></option>';
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['BANK_ID'])
				$html .= '<option selected="selected" value="'.$row['BANK_ID'].'">'.$row['BANK_NAME'].'</option>';
			else
				$html .= '<option value="'.$row['BANK_ID'].'">'.$row['BANK_NAME'].'</option>';
		}
		return $html;
	}
	
	//END }
}
?>

<?php

class ware_fabricdispatchheader_approvedby{
 
	private $db;
	private $table= "ware_fabricdispatchheader_approvedby";
	
	//private property
	private $intBulkDispatchNo;
	private $intYear;
	private $intApproveLevelNo;
	private $intApproveUser;
	private $dtApprovedDate;
	private $intStatus;
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->db->field_array_set(array('intBulkDispatchNo'=>'intBulkDispatchNo',
										'intYear'=>'intYear',
										'intApproveLevelNo'=>'intApproveLevelNo',
										'intApproveUser'=>'intApproveUser',
										'dtApprovedDate'=>'dtApprovedDate',
										'intStatus'=>'intStatus',
										));
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  
		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	//public functions for private properties
	
	//retun intBulkDispatchNo
	function getintBulkDispatchNo()
	{
		$this->validate();
		return $this->intBulkDispatchNo;
	}
	
	//retun intYear
	function getintYear()
	{
		$this->validate();
		return $this->intYear;
	}
	
	//retun intApproveLevelNo
	function getintApproveLevelNo()
	{
		$this->validate();
		return $this->intApproveLevelNo;
	}
	
	//retun intApproveUser
	function getintApproveUser()
	{
		$this->validate();
		return $this->intApproveUser;
	}
	
	//retun dtApprovedDate
	function getdtApprovedDate()
	{
		$this->validate();
		return $this->dtApprovedDate;
	}
	
	//retun intStatus
	function getintStatus()
	{
		$this->validate();
		return $this->intStatus;
	}
	
	//validate primary values
	private function validate()
	{
		if($this->intBulkDispatchNo=='' || $this->intYear=='' || $this->intApproveLevelNo=='' || $this->intStatus=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
		}
	}
	
	public function set($intBulkDispatchNo , $intYear , $intApproveLevelNo , $intStatus)
	{
		$result = $this->select('*',null,"intBulkDispatchNo='$intBulkDispatchNo' and intYear='$intYear' and intApproveLevelNo='$intApproveLevelNo' and intStatus='$intStatus'");	
		if($this->db->numRows()>0)
			$this->setVariables($result);
		else
			throw new Exception("Record not found.");
			
	}
	
	
}
?>

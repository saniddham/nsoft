
<?php

class mst_marketer{
 
	private $db;
	private $table= "mst_marketer";
	
	//private property
	private $intUserId;
	private $intManager;
	private $SALES_PROJECTION_TYPE;
	private $REVENUE_TYPE;
	private $REVENUE_TYPES;
	private $OTHER_CATEGORY_FLAG;
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->db->field_array_set(array('intUserId'=>'intUserId',
										'intManager'=>'intManager',
										'SALES_PROJECTION_TYPE'=>'SALES_PROJECTION_TYPE',
										'REVENUE_TYPE'=>'REVENUE_TYPE',
										'REVENUE_TYPES'=>'REVENUE_TYPES',
										'OTHER_CATEGORY_FLAG'=>'OTHER_CATEGORY_FLAG',
										));
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  
		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	//public functions for private properties
	
	//retun intUserId
	function getintUserId()
	{
		$this->validate();
		return $this->intUserId;
	}
	
	//retun intManager
	function getintManager()
	{
		$this->validate();
		return $this->intManager;
	}
	
	//retun SALES_PROJECTION_TYPE
	function getSALES_PROJECTION_TYPE()
	{
		$this->validate();
		return $this->SALES_PROJECTION_TYPE;
	}
	
	//retun REVENUE_TYPE
	function getREVENUE_TYPE()
	{
		$this->validate();
		return $this->REVENUE_TYPE;
	}
	
	//retun REVENUE_TYPES
	function getREVENUE_TYPES()
	{
		$this->validate();
		return $this->REVENUE_TYPES;
	}
	
	//retun OTHER_CATEGORY_FLAG
	function getOTHER_CATEGORY_FLAG()
	{
		$this->validate();
		return $this->OTHER_CATEGORY_FLAG;
	}
	
	//validate primary values
	private function validate()
	{
		if($this->intUserId=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
		}
	}
	
	public function set($intUserId)
	{
		$result = $this->select('*',null,"intUserId='$intUserId'");	
		if($this->db->numRows()>0)
			$this->setVariables($result);
		else
			throw new Exception("Record not found.");
			
	}
	
	
}
?>

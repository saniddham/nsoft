<?php
class fixed_assets_process_log{
 
	private $db;
	private $table= "fixed_assets_process_log";
	
	//private property
	private $ID;
	private $ID_FIXED_ASSET;
	private $GRN_NO;
	private $GRN_YEAR;
	private $GRN_DATE;
	private $ITEM_ID;
	private $SERIAL_NO;
	private $CURRENCY;
	private $ITEM_PRICE;
	private $DEPRECIATE_RATE;
	private $NET_AMOUNT_BEFORE_PROCESS;
	private $DEPRECIATED_AMOUNT;
	private $NET_AMOUNT;
	private $PROCESSED_DATE;
	private $TYPE;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('ID'=>'ID',
										'ID_FIXED_ASSET'=>'ID_FIXED_ASSET',
										'GRN_NO'=>'GRN_NO',
										'GRN_YEAR'=>'GRN_YEAR',
										'GRN_DATE'=>'GRN_DATE',
										'ITEM_ID'=>'ITEM_ID',
										'SERIAL_NO'=>'SERIAL_NO',
										'CURRENCY'=>'CURRENCY',
										'ITEM_PRICE'=>'ITEM_PRICE',
										'DEPRECIATE_RATE'=>'DEPRECIATE_RATE',
										'NET_AMOUNT_BEFORE_PROCESS'=>'NET_AMOUNT_BEFORE_PROCESS',
										'DEPRECIATED_AMOUNT'=>'DEPRECIATED_AMOUNT',
										'NET_AMOUNT'=>'NET_AMOUNT',
										'PROCESSED_DATE'=>'PROCESSED_DATE',
										'TYPE'=>'TYPE',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "ID = ".$this->ID."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun ID
	function getID()
	{
		$this->validate();
		return $this->ID;
	}
	
	//retun ID_FIXED_ASSET
	function getID_FIXED_ASSET()
	{
		$this->validate();
		return $this->ID_FIXED_ASSET;
	}
	
	//retun GRN_NO
	function getGRN_NO()
	{
		$this->validate();
		return $this->GRN_NO;
	}
	
	//retun GRN_YEAR
	function getGRN_YEAR()
	{
		$this->validate();
		return $this->GRN_YEAR;
	}
	
	//retun GRN_DATE
	function getGRN_DATE()
	{
		$this->validate();
		return $this->GRN_DATE;
	}
	
	//retun ITEM_ID
	function getITEM_ID()
	{
		$this->validate();
		return $this->ITEM_ID;
	}
	
	//retun SERIAL_NO
	function getSERIAL_NO()
	{
		$this->validate();
		return $this->SERIAL_NO;
	}
	
	//retun CURRENCY
	function getCURRENCY()
	{
		$this->validate();
		return $this->CURRENCY;
	}
	
	//retun ITEM_PRICE
	function getITEM_PRICE()
	{
		$this->validate();
		return $this->ITEM_PRICE;
	}
	
	//retun DEPRECIATE_RATE
	function getDEPRECIATE_RATE()
	{
		$this->validate();
		return $this->DEPRECIATE_RATE;
	}
	
	//retun NET_AMOUNT_BEFORE_PROCESS
	function getNET_AMOUNT_BEFORE_PROCESS()
	{
		$this->validate();
		return $this->NET_AMOUNT_BEFORE_PROCESS;
	}
	
	//retun DEPRECIATED_AMOUNT
	function getDEPRECIATED_AMOUNT()
	{
		$this->validate();
		return $this->DEPRECIATED_AMOUNT;
	}
	
	//retun NET_AMOUNT
	function getNET_AMOUNT()
	{
		$this->validate();
		return $this->NET_AMOUNT;
	}
	
	//retun PROCESSED_DATE
	function getPROCESSED_DATE()
	{
		$this->validate();
		return $this->PROCESSED_DATE;
	}
	
	//retun PROCESSED_DATE
	function getTYPE()
	{
		$this->validate();
		return $this->TYPE;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set ID
	function setID($ID)
	{
		array_push($this->commitArray,'ID');
		$this->ID = $ID;
	}
	
	//set ID_FIXED_ASSET
	function setID_FIXED_ASSET($ID_FIXED_ASSET)
	{
		array_push($this->commitArray,'ID_FIXED_ASSET');
		$this->ID_FIXED_ASSET = $ID_FIXED_ASSET;
	}
	
	//set GRN_NO
	function setGRN_NO($GRN_NO)
	{
		array_push($this->commitArray,'GRN_NO');
		$this->GRN_NO = $GRN_NO;
	}
	
	//set GRN_YEAR
	function setGRN_YEAR($GRN_YEAR)
	{
		array_push($this->commitArray,'GRN_YEAR');
		$this->GRN_YEAR = $GRN_YEAR;
	}
	
	//set GRN_DATE
	function setGRN_DATE($GRN_DATE)
	{
		array_push($this->commitArray,'GRN_DATE');
		$this->GRN_DATE = $GRN_DATE;
	}
	
	//set ITEM_ID
	function setITEM_ID($ITEM_ID)
	{
		array_push($this->commitArray,'ITEM_ID');
		$this->ITEM_ID = $ITEM_ID;
	}
	
	//set SERIAL_NO
	function setSERIAL_NO($SERIAL_NO)
	{
		array_push($this->commitArray,'SERIAL_NO');
		$this->SERIAL_NO = $SERIAL_NO;
	}
	
	//set CURRENCY
	function setCURRENCY($CURRENCY)
	{
		array_push($this->commitArray,'CURRENCY');
		$this->CURRENCY = $CURRENCY;
	}
	
	//set ITEM_PRICE
	function setITEM_PRICE($ITEM_PRICE)
	{
		array_push($this->commitArray,'ITEM_PRICE');
		$this->ITEM_PRICE = $ITEM_PRICE;
	}
	
	//set DEPRECIATE_RATE
	function setDEPRECIATE_RATE($DEPRECIATE_RATE)
	{
		array_push($this->commitArray,'DEPRECIATE_RATE');
		$this->DEPRECIATE_RATE = $DEPRECIATE_RATE;
	}
	
	//set NET_AMOUNT_BEFORE_PROCESS
	function setNET_AMOUNT_BEFORE_PROCESS($NET_AMOUNT_BEFORE_PROCESS)
	{
		array_push($this->commitArray,'NET_AMOUNT_BEFORE_PROCESS');
		$this->NET_AMOUNT_BEFORE_PROCESS = $NET_AMOUNT_BEFORE_PROCESS;
	}
	
	//set DEPRECIATED_AMOUNT
	function setDEPRECIATED_AMOUNT($DEPRECIATED_AMOUNT)
	{
		array_push($this->commitArray,'DEPRECIATED_AMOUNT');
		$this->DEPRECIATED_AMOUNT = $DEPRECIATED_AMOUNT;
	}
	
	//set NET_AMOUNT
	function setNET_AMOUNT($NET_AMOUNT)
	{
		array_push($this->commitArray,'NET_AMOUNT');
		$this->NET_AMOUNT = $NET_AMOUNT;
	}
	
	//set PROCESSED_DATE
	function setPROCESSED_DATE($PROCESSED_DATE)
	{
		array_push($this->commitArray,'PROCESSED_DATE');
		$this->PROCESSED_DATE = $PROCESSED_DATE;
	}
	
	//set PROCESSED_DATE
	function setTYPE($PROCESSED_DATE)
	{
		array_push($this->commitArray,'TYPE');
		$this->TYPE = $TYPE;
	}
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "ID='$ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($ID_FIXED_ASSET,$GRN_NO,$GRN_YEAR,$GRN_DATE,$ITEM_ID,$SERIAL_NO,$CURRENCY,$ITEM_PRICE,$DEPRECIATE_RATE,$NET_AMOUNT_BEFORE_PROCESS,$DEPRECIATED_AMOUNT,$NET_AMOUNT,$PROCESSED_DATE,$TYPE){
		$data = array('ID_FIXED_ASSET'=>$ID_FIXED_ASSET 
				,'GRN_NO'=>$GRN_NO 
				,'GRN_YEAR'=>$GRN_YEAR 
				,'GRN_DATE'=>$GRN_DATE 
				,'ITEM_ID'=>$ITEM_ID 
				,'SERIAL_NO'=>$SERIAL_NO 
				,'CURRENCY'=>$CURRENCY 
				,'ITEM_PRICE'=>$ITEM_PRICE 
				,'DEPRECIATE_RATE'=>$DEPRECIATE_RATE 
				,'NET_AMOUNT_BEFORE_PROCESS'=>$NET_AMOUNT_BEFORE_PROCESS 
				,'DEPRECIATED_AMOUNT'=>$DEPRECIATED_AMOUNT 
				,'NET_AMOUNT'=>$NET_AMOUNT 
				,'PROCESSED_DATE'=>$PROCESSED_DATE 
				,'TYPE'=>$TYPE 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('ID,ID_FIXED_ASSET',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['ID'])
				$html .= '<option selected="selected" value="'.$row['ID'].'">'.$row['ID_FIXED_ASSET'].'</option>';	
			else
				$html .= '<option value="'.$row['ID'].'">'.$row['ID_FIXED_ASSET'].'</option>';	
		}
		return $html;
	}
	
	//END }
}
?>
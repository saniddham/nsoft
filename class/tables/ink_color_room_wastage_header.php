<?php
class ink_color_room_wastage_header{
 
	private $db;
	private $table= "ink_color_room_wastage_header";
	
	//private property
	private $SERIAL_NO;
	private $SERIAL_YEAR;
	private $INK_COLOR;
	private $INK_TYPE;
	private $INK_TECHNIQUE;
	private $LOCATION_ID;
	private $COMPANY_ID;
	private $STATUS;
	private $APPROVE_LEVELS;
	private $CREATED_BY;
	private $CREATED_DATE;
	private $MODIFIED_BY;
	private $MODIFIED_DATE;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('SERIAL_NO'=>'SERIAL_NO',
										'SERIAL_YEAR'=>'SERIAL_YEAR',
										'RETURN_NO'=>'RETURN_NO',
										'RETURN_YEAR'=>'RETURN_YEAR',
										'INK_COLOR'=>'INK_COLOR',
										'INK_TYPE'=>'INK_TYPE',
										'INK_TECHNIQUE'=>'INK_TECHNIQUE',
										'LOCATION_ID'=>'LOCATION_ID',
										'COMPANY_ID'=>'COMPANY_ID',
										'STATUS'=>'STATUS',
										'APPROVE_LEVELS'=>'APPROVE_LEVELS',
										'CREATED_BY'=>'CREATED_BY',
										'CREATED_DATE'=>'CREATED_DATE',
										'MODIFIED_BY'=>'MODIFIED_BY',
										'MODIFIED_DATE'=>'MODIFIED_DATE',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "SERIAL_NO = ".$this->SERIAL_NO." and SERIAL_YEAR = ".$this->SERIAL_YEAR."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun SERIAL_NO
	function getSERIAL_NO()
	{
		$this->validate();
		return $this->SERIAL_NO;
	}
	
	//retun SERIAL_YEAR
	function getSERIAL_YEAR()
	{
		$this->validate();
		return $this->SERIAL_YEAR;
	}
	
	//retun INK_COLOR
	function getINK_COLOR()
	{
		$this->validate();
		return $this->INK_COLOR;
	}
	
	//retun INK_TYPE
	function getINK_TYPE()
	{
		$this->validate();
		return $this->INK_TYPE;
	}
	
	//retun INK_TECHNIQUE
	function getINK_TECHNIQUE()
	{
		$this->validate();
		return $this->INK_TECHNIQUE;
	}
	
	//retun LOCATION_ID
	function getLOCATION_ID()
	{
		$this->validate();
		return $this->LOCATION_ID;
	}
	
	//retun COMPANY_ID
	function getCOMPANY_ID()
	{
		$this->validate();
		return $this->COMPANY_ID;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//retun APPROVE_LEVELS
	function getAPPROVE_LEVELS()
	{
		$this->validate();
		return $this->APPROVE_LEVELS;
	}
	
	//retun CREATED_BY
	function getCREATED_BY()
	{
		$this->validate();
		return $this->CREATED_BY;
	}
	
	//retun CREATED_DATE
	function getCREATED_DATE()
	{
		$this->validate();
		return $this->CREATED_DATE;
	}
	
	//retun MODIFIED_BY
	function getMODIFIED_BY()
	{
		$this->validate();
		return $this->MODIFIED_BY;
	}
	
	//retun RETURN_NO
	function getRETURN_NO()
	{
		$this->validate();
		return $this->RETURN_NO;
	}
	
	//retun RETURN_YEAR
	function getRETURN_YEAR()
	{
		$this->validate();
		return $this->RETURN_YEAR;
	}
	
	//retun MODIFIED_DATE
	function getMODIFIED_DATE()
	{
		$this->validate();
		return $this->MODIFIED_DATE;
	}

	
	//END }
	
	//BEGIN - public function set {
	
	//set SERIAL_NO
	function setSERIAL_NO($SERIAL_NO)
	{
		array_push($this->commitArray,'SERIAL_NO');
		$this->SERIAL_NO = $SERIAL_NO;
	}
	
	//set SERIAL_YEAR
	function setSERIAL_YEAR($SERIAL_YEAR)
	{
		array_push($this->commitArray,'SERIAL_YEAR');
		$this->SERIAL_YEAR = $SERIAL_YEAR;
	}
	
	//set INK_COLOR
	function setINK_COLOR($INK_COLOR)
	{
		array_push($this->commitArray,'INK_COLOR');
		$this->INK_COLOR = $INK_COLOR;
	}
	
	//set INK_TYPE
	function setINK_TYPE($INK_TYPE)
	{
		array_push($this->commitArray,'INK_TYPE');
		$this->INK_TYPE = $INK_TYPE;
	}
	
	//set INK_TECHNIQUE
	function setINK_TECHNIQUE($INK_TECHNIQUE)
	{
		array_push($this->commitArray,'INK_TECHNIQUE');
		$this->INK_TECHNIQUE = $INK_TECHNIQUE;
	}
	
	//set LOCATION_ID
	function setLOCATION_ID($LOCATION_ID)
	{
		array_push($this->commitArray,'LOCATION_ID');
		$this->LOCATION_ID = $LOCATION_ID;
	}
	
	//set COMPANY_ID
	function setCOMPANY_ID($COMPANY_ID)
	{
		array_push($this->commitArray,'COMPANY_ID');
		$this->COMPANY_ID = $COMPANY_ID;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//set APPROVE_LEVELS
	function setAPPROVE_LEVELS($APPROVE_LEVELS)
	{
		array_push($this->commitArray,'APPROVE_LEVELS');
		$this->APPROVE_LEVELS = $APPROVE_LEVELS;
	}
	
	//set CREATED_BY
	function setCREATED_BY($CREATED_BY)
	{
		array_push($this->commitArray,'CREATED_BY');
		$this->CREATED_BY = $CREATED_BY;
	}
	
	//set CREATED_DATE
	function setCREATED_DATE($CREATED_DATE)
	{
		array_push($this->commitArray,'CREATED_DATE');
		$this->CREATED_DATE = $CREATED_DATE;
	}
	
	//set MODIFIED_BY
	function setMODIFIED_BY($MODIFIED_BY)
	{
		array_push($this->commitArray,'MODIFIED_BY');
		$this->MODIFIED_BY = $MODIFIED_BY;
	}
	
	//set MODIFIED_DATE
	function setMODIFIED_DATE($MODIFIED_DATE)
	{
		array_push($this->commitArray,'MODIFIED_DATE');
		$this->MODIFIED_DATE = $MODIFIED_DATE;
	}
	
	//set MODIFIED_DATE
	function setRETURN_NO($RETURN_NO)
	{
		array_push($this->commitArray,'RETURN_NO');
		$this->RETURN_NO = $RETURN_NO;
	}
	
	//set MODIFIED_DATE
	function setRETURN_YEAR($RETURN_YEAR)
	{
		array_push($this->commitArray,'RETURN_YEAR');
		$this->RETURN_YEAR = $RETURN_YEAR;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->SERIAL_NO=='' || $this->SERIAL_YEAR=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($SERIAL_NO , $SERIAL_YEAR)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "SERIAL_NO='$SERIAL_NO' and SERIAL_YEAR='$SERIAL_YEAR'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($SERIAL_NO,$SERIAL_YEAR,$R_SERIAL_NO,$R_SERIAL_YEAR,$INK_COLOR,$INK_TYPE,$INK_TECHNIQUE,$LOCATION_ID,$COMPANY_ID,$STATUS,$APPROVE_LEVELS,$CREATED_BY,$CREATED_DATE,$MODIFIED_BY,$MODIFIED_DATE){
		$data = array('SERIAL_NO'=>$SERIAL_NO 
				,'SERIAL_YEAR'=>$SERIAL_YEAR 
				,'RETURN_NO'=>$R_SERIAL_NO 
				,'RETURN_YEAR'=>$R_SERIAL_YEAR 
				,'INK_COLOR'=>$INK_COLOR 
				,'INK_TYPE'=>$INK_TYPE 
				,'INK_TECHNIQUE'=>$INK_TECHNIQUE 
				,'LOCATION_ID'=>$LOCATION_ID 
				,'COMPANY_ID'=>$COMPANY_ID 
				,'STATUS'=>$STATUS 
				,'APPROVE_LEVELS'=>$APPROVE_LEVELS 
				,'CREATED_BY'=>$CREATED_BY 
				,'CREATED_DATE'=>$CREATED_DATE 
				,'MODIFIED_BY'=>$MODIFIED_BY 
				,'MODIFIED_DATE'=>$MODIFIED_DATE 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('SERIAL_NO,SERIAL_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['SERIAL_NO'])
				$html .= '<option selected="selected" value="'.$row['SERIAL_NO'].'">'.$row['SERIAL_YEAR'].'</option>';	
			else
				$html .= '<option value="'.$row['SERIAL_NO'].'">'.$row['SERIAL_YEAR'].'</option>';	
		}
		return $html;
	}
	
function checkWastagesINConfirmedReturned($serialNo,$serialYear)
{
		global $db;
				$sql = "SELECT
						ink_color_room_wastage_header.SERIAL_NO,
						ink_color_room_wastage_header.SERIAL_YEAR
						FROM `ink_color_room_wastage_header`
						WHERE
						ink_color_room_wastage_header.RETURN_NO = '$serialNo' AND
						ink_color_room_wastage_header.RETURN_YEAR = '$serialYear'
						 ";
	
		$result = $this->db->RunQuery($sql);
		return mysqli_num_rows($result);
 }
	
function checkWastagesNoINConfirmedReturned($serialNo,$serialYear)
{
		global $db;
				$sql = "SELECT
						ink_color_room_wastage_header.SERIAL_NO,
						ink_color_room_wastage_header.SERIAL_YEAR
						FROM `ink_color_room_wastage_header`
						WHERE
						ink_color_room_wastage_header.RETURN_NO = '$serialNo' AND
						ink_color_room_wastage_header.RETURN_YEAR = '$serialYear'
						 ";
	
		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		return $row;
 }

	public function getPendingWastage($date,$locationId)
	{
		$cols	= " ink_color_room_wastage_details.SERIAL_NO,ink_color_room_wastage_details.SERIAL_YEAR,ink_color_room_wastage_details.ORDER_NO,ink_color_room_wastage_details.ORDER_YEAR,ink_color_room_wastage_details.SALES_ORDER_ID " ;

		$join	= "
					INNER JOIN ink_color_room_wastage_details ON ink_color_room_wastage_header.SERIAL_NO = ink_color_room_wastage_details.SERIAL_NO AND ink_color_room_wastage_header.SERIAL_YEAR = ink_color_room_wastage_details.SERIAL_YEAR
					INNER JOIN ink_color_room_wastage_approvedby ON ink_color_room_wastage_details.SERIAL_NO = ink_color_room_wastage_approvedby.SERIAL_NO AND ink_color_room_wastage_details.SERIAL_YEAR = ink_color_room_wastage_approvedby.SERIAL_YEAR

		";
		$where	= "DATE(ink_color_room_wastage_approvedby.APPROVE_DATE)<= '$date'
					AND ink_color_room_wastage_header.STATUS = 1 
					AND LOCATION_ID = $locationId
					AND COLOR_ROOM_PROCESS_ID IS NULL ";
					//echo ' SELECT '.$cols.' FROM ink_color_room_wastage_header  '. $join. 'WHERE '.$where;
		return $this->select($cols,$join,$where,$order = null, $limit = null);
	}

	public function getPendingWastage_weight_noneProcessed($orderNo,$orderYear,$salesOrderId,$color,$technique,$inkType)
	{
		$cols	= "SUM(ink_color_room_wastage_details.INK_WEIGHT) AS WEIGHT" ;

		$join	= "
					INNER JOIN ink_color_room_wastage_details ON ink_color_room_wastage_header.SERIAL_NO = ink_color_room_wastage_details.SERIAL_NO AND ink_color_room_wastage_header.SERIAL_YEAR = ink_color_room_wastage_details.SERIAL_YEAR
				/*	INNER JOIN ink_color_room_wastage_approvedby ON ink_color_room_wastage_details.SERIAL_NO = ink_color_room_wastage_approvedby.SERIAL_NO AND ink_color_room_wastage_details.SERIAL_YEAR = ink_color_room_wastage_approvedby.SERIAL_YEAR*/

		";
		$where	= "
		ink_color_room_wastage_details.ORDER_NO='$orderNo' AND ink_color_room_wastage_details.ORDER_YEAR  ='$orderYear' AND ink_color_room_wastage_details.SALES_ORDER_ID='$salesOrderId' AND  
		INK_COLOR='$color' AND
INK_TYPE='$inkType' AND
INK_TECHNIQUE='$technique'  

		 
					AND ink_color_room_wastage_header.STATUS = 1 
 					AND COLOR_ROOM_PROCESS_ID IS NULL ";
				//	echo ' SELECT '.$cols.' FROM ink_color_room_wastage_header  '. $join. 'WHERE '.$where;
		$result	= $this->select($cols,$join,$where,$order = null, $limit = null);
		$row	=	mysqli_fetch_array($result);
		return  	$row[0];
	}

	//END }
}
?>
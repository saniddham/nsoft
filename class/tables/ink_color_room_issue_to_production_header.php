<?php
//THIS CLASS MANUALLY EDITED BY LAHIRU
class ink_color_room_issue_to_production_header{
 
	private $db;
	private $table= "ink_color_room_issue_to_production_header";
	
	//private property
	private $SERIAL_NO;
	private $SERIAL_YEAR;
	private $LOCATION_ID;
	private $COMPANY_ID;
	private $STATUS;
	private $APPROVE_LEVELS;
	private $CREATED_BY;
	private $CREATED_DATE;
	private $MODIFIED_BY;
	private $MODIFIED_DATE;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('SERIAL_NO'=>'SERIAL_NO',
										'SERIAL_YEAR'=>'SERIAL_YEAR',
										'LOCATION_ID'=>'LOCATION_ID',
										'COMPANY_ID'=>'COMPANY_ID',
										'STATUS'=>'STATUS',
										'APPROVE_LEVELS'=>'APPROVE_LEVELS',
										'CREATED_BY'=>'CREATED_BY',
										'CREATED_DATE'=>'CREATED_DATE',
										'MODIFIED_BY'=>'MODIFIED_BY',
										'MODIFIED_DATE'=>'MODIFIED_DATE',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	function commit()
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "SERIAL_NO = ".$this->SERIAL_NO." and SERIAL_YEAR = ".$this->SERIAL_YEAR."" ;
		//unset($this->commitArray);
		$this->commitArray = array();
		
		if($this->SERIAL_NO==NULL || $this->SERIAL_NO=='' || $this->SERIAL_NO=='NULL')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun SERIAL_NO
	function getSERIAL_NO()
	{
		$this->validate();
		return $this->SERIAL_NO;
	}
	
	//retun SERIAL_YEAR
	function getSERIAL_YEAR()
	{
		$this->validate();
		return $this->SERIAL_YEAR;
	}
	
	//retun LOCATION_ID
	function getLOCATION_ID()
	{
		$this->validate();
		return $this->LOCATION_ID;
	}
	
	//retun COMPANY_ID
	function getCOMPANY_ID()
	{
		$this->validate();
		return $this->COMPANY_ID;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//retun APPROVE_LEVELS
	function getAPPROVE_LEVELS()
	{
		$this->validate();
		return $this->APPROVE_LEVELS;
	}
	
	//retun CREATED_BY
	function getCREATED_BY()
	{
		$this->validate();
		return $this->CREATED_BY;
	}
	
	//retun CREATED_DATE
	function getCREATED_DATE()
	{
		$this->validate();
		return $this->CREATED_DATE;
	}
	
	//retun MODIFIED_BY
	function getMODIFIED_BY()
	{
		$this->validate();
		return $this->MODIFIED_BY;
	}
	
	//retun MODIFIED_DATE
	function getMODIFIED_DATE()
	{
		$this->validate();
		return $this->MODIFIED_DATE;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set SERIAL_NO
	function setSERIAL_NO($SERIAL_NO)
	{
		array_push($this->commitArray,'SERIAL_NO');
		$this->SERIAL_NO = $SERIAL_NO;
	}
	
	//set SERIAL_YEAR
	function setSERIAL_YEAR($SERIAL_YEAR)
	{
		array_push($this->commitArray,'SERIAL_YEAR');
		$this->SERIAL_YEAR = $SERIAL_YEAR;
	}
	
	//set LOCATION_ID
	function setLOCATION_ID($LOCATION_ID)
	{
		array_push($this->commitArray,'LOCATION_ID');
		$this->LOCATION_ID = $LOCATION_ID;
	}
	
	//set COMPANY_ID
	function setCOMPANY_ID($COMPANY_ID)
	{
		array_push($this->commitArray,'COMPANY_ID');
		$this->COMPANY_ID = $COMPANY_ID;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//set APPROVE_LEVELS
	function setAPPROVE_LEVELS($APPROVE_LEVELS)
	{
		array_push($this->commitArray,'APPROVE_LEVELS');
		$this->APPROVE_LEVELS = $APPROVE_LEVELS;
	}
	
	//set CREATED_BY
	function setCREATED_BY($CREATED_BY)
	{
		array_push($this->commitArray,'CREATED_BY');
		$this->CREATED_BY = $CREATED_BY;
	}
	
	//set CREATED_DATE
	function setCREATED_DATE($CREATED_DATE)
	{
		array_push($this->commitArray,'CREATED_DATE');
		$this->CREATED_DATE = $CREATED_DATE;
	}
	
	//set MODIFIED_BY
	function setMODIFIED_BY($MODIFIED_BY)
	{
		array_push($this->commitArray,'MODIFIED_BY');
		$this->MODIFIED_BY = $MODIFIED_BY;
	}
	
	//set MODIFIED_DATE
	function setMODIFIED_DATE($MODIFIED_DATE)
	{
		array_push($this->commitArray,'MODIFIED_DATE');
		$this->MODIFIED_DATE = $MODIFIED_DATE;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->SERIAL_NO=='' || $this->SERIAL_YEAR=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($SERIAL_NO , $SERIAL_YEAR)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "SERIAL_NO='$SERIAL_NO' and SERIAL_YEAR='$SERIAL_YEAR'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($SERIAL_NO,$SERIAL_YEAR,$LOCATION_ID,$COMPANY_ID,$STATUS,$APPROVE_LEVELS){
	
	global $sessions;
	global $dateTimes;
	
		$data = array('SERIAL_NO'=>$SERIAL_NO 
				,'SERIAL_YEAR'=>$SERIAL_YEAR 
				,'LOCATION_ID'=>$LOCATION_ID 
				,'COMPANY_ID'=>$COMPANY_ID 
				,'STATUS'=>$STATUS 
				,'APPROVE_LEVELS'=>$APPROVE_LEVELS 
				,'CREATED_BY'=>$sessions->getUserId() 
				,'CREATED_DATE'=>$dateTimes->getCurruntDateTime()
				,'MODIFIED_BY'=>$sessions->getUserId() 
				,'MODIFIED_DATE'=>$dateTimes->getCurruntDateTime()
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('SERIAL_NO,SERIAL_YEAR',  null, $where = $where);
		while($row=mysqli_fetch_array($result)){
			$html .= '<option value="'.$row['SERIAL_NO'].'">'.$row['SERIAL_YEAR'].'</option>';	
		}
		return $html;
	}
	
	public function checkInkReturnForProduction($locationId,$orderNo,$orderYear,$salesOrderId)
	{
		 $sql	= " SELECT COUNT(tb1.SERIAL_NO) AS ROW_COUNT ,
					GROUP_CONCAT(ISSUE_NOS) AS ISSUE_NOS   
					FROM
					( 
					SELECT ink_color_room_issue_to_production_header.SERIAL_NO,
					GROUP_CONCAT(DISTINCT CONCAT(ink_color_room_issue_to_production_header.SERIAL_NO,'/',ink_color_room_issue_to_production_header.SERIAL_YEAR)) AS ISSUE_NOS 
					FROM ink_color_room_issue_to_production_header
					INNER JOIN ink_color_room_issue_to_production_details CRIPD ON CRIPD.SERIAL_NO=ink_color_room_issue_to_production_header.SERIAL_NO AND
					CRIPD.SERIAL_YEAR=ink_color_room_issue_to_production_header.SERIAL_YEAR
					LEFT JOIN ink_production_return_header PRH ON PRH.ISSUE_NO=ink_color_room_issue_to_production_header.SERIAL_NO AND
					PRH.ISSUE_YEAR=ink_color_room_issue_to_production_header.SERIAL_YEAR AND PRH.STATUS=1					
					LEFT JOIN ink_production_return_details PRD ON PRD.SERIAL_NO = PRH.SERIAL_NO AND
					PRD.SERIAL_YEAR = PRH.SERIAL_YEAR
					LEFT JOIN ink_production_return_details PRD1 ON PRD1.ORDER_NO = CRIPD.ORDER_NO AND
					PRD1.ORDER_YEAR = CRIPD.ORDER_YEAR AND
					PRD1.SALES_ORDER_ID = CRIPD.SALES_ORDER_ID AND
					PRD1.INK_COLOR = CRIPD.INK_COLOR AND
					PRD1.INK_TECHNIQUE = CRIPD.INK_TECHNIQUE AND
					PRD1.INK_TYPE = CRIPD.INK_TYPE
					
					WHERE ink_color_room_issue_to_production_header.STATUS = 1 AND
					CRIPD.ORDER_NO = '".$orderNo."' AND
					CRIPD.ORDER_YEAR = '".$orderYear."' AND
					CRIPD.SALES_ORDER_ID = '".$salesOrderId."' AND
					ink_color_room_issue_to_production_header.LOCATION_ID = '".$locationId."' AND
					ink_color_room_issue_to_production_header.SERIAL_NO IS NOT NULL AND
					PRD.SERIAL_NO IS NULL 
					
					GROUP BY ink_color_room_issue_to_production_header.SERIAL_NO,
					ink_color_room_issue_to_production_header.SERIAL_YEAR,
					PRH.SERIAL_NO,
					PRH.ISSUE_YEAR
					) AS tb1 ";
		
		$result	= $this->db->RunQuery($sql);
		
		return $result;
		
	}
	
	public function getNoneReturnedColor($locationId,$orderNo,$orderYear,$salesOrderId,$color,$technique, $inkType,$deci)
	{
		 $sql	= "   
					SELECT ROUND(sum(CRIPD.INK_WEIGHT),$deci+2) AS WEIGHT 
 					FROM ink_color_room_issue_to_production_header
					INNER JOIN ink_color_room_issue_to_production_details AS CRIPD ON CRIPD.SERIAL_NO = ink_color_room_issue_to_production_header.SERIAL_NO AND CRIPD.SERIAL_YEAR = ink_color_room_issue_to_production_header.SERIAL_YEAR
					LEFT JOIN ink_production_return_header AS PRH ON PRH.ISSUE_NO = ink_color_room_issue_to_production_header.SERIAL_NO AND PRH.ISSUE_YEAR = ink_color_room_issue_to_production_header.SERIAL_YEAR AND PRH.`STATUS` = 1
					LEFT JOIN ink_production_return_details AS PRD ON PRD.SERIAL_NO = PRH.SERIAL_NO AND PRD.SERIAL_YEAR = PRH.SERIAL_YEAR AND CRIPD.ORDER_NO = PRD.ORDER_NO AND CRIPD.ORDER_YEAR = PRD.ORDER_YEAR AND CRIPD.SALES_ORDER_ID = PRD.SALES_ORDER_ID AND CRIPD.INK_COLOR = PRD.INK_COLOR AND CRIPD.INK_TECHNIQUE = PRD.INK_TECHNIQUE AND CRIPD.INK_TYPE = PRD.INK_TYPE
					
					WHERE ink_color_room_issue_to_production_header.STATUS = 1 AND
					CRIPD.ORDER_NO = '".$orderNo."' AND
					CRIPD.ORDER_YEAR = '".$orderYear."' AND
					CRIPD.SALES_ORDER_ID = '".$salesOrderId."' ";
		if($color!='')
		$sql		.=" AND CRIPD.INK_COLOR = '".$color."' ";
		if($technique!='')
		$sql		.=" AND CRIPD.INK_TECHNIQUE = '".$technique."' ";
		if($inkType!='')
		$sql		.=" AND CRIPD.INK_TYPE = '".$inkType."' ";
		if($locationId!='')
		$sql		.=" AND ink_color_room_issue_to_production_header.LOCATION_ID = '".$locationId."' ";
		
		$sql		.=" ink_color_room_issue_to_production_header.SERIAL_NO IS NOT NULL AND
					PRD.SERIAL_NO IS NULL  ";
		
		$result	= $this->db->RunQuery($sql);
 		return  $result[0]['WEIGHT'];	
 		
	}
	public function getNoneProcessedUsageOfColor($locationId,$orderNo,$orderYear,$salesOrderId,$color,$technique, $inkType,$deci)
	{
		 $sql	= "   
 					SELECT 
					ROUND(sum(CRIPD.INK_WEIGHT),$deci+2) AS ISSUED_WEIGHT, 
					ROUND(sum(IFNULL(PRD.INK_WEIGHT,0)),$deci+2) AS RETURNED_WEIGHT 
 					FROM ink_color_room_issue_to_production_header
					INNER JOIN ink_color_room_issue_to_production_details AS CRIPD ON CRIPD.SERIAL_NO = ink_color_room_issue_to_production_header.SERIAL_NO AND CRIPD.SERIAL_YEAR = ink_color_room_issue_to_production_header.SERIAL_YEAR
					LEFT JOIN ink_production_return_header AS PRH ON PRH.ISSUE_NO = ink_color_room_issue_to_production_header.SERIAL_NO AND PRH.ISSUE_YEAR = ink_color_room_issue_to_production_header.SERIAL_YEAR AND PRH.`STATUS` = 1
					LEFT JOIN ink_production_return_details AS PRD ON PRD.SERIAL_NO = PRH.SERIAL_NO AND PRD.SERIAL_YEAR = PRH.SERIAL_YEAR AND CRIPD.ORDER_NO = PRD.ORDER_NO AND CRIPD.ORDER_YEAR = PRD.ORDER_YEAR AND CRIPD.SALES_ORDER_ID = PRD.SALES_ORDER_ID AND CRIPD.INK_COLOR = PRD.INK_COLOR AND CRIPD.INK_TECHNIQUE = PRD.INK_TECHNIQUE AND CRIPD.INK_TYPE = PRD.INK_TYPE

					WHERE ink_color_room_issue_to_production_header.STATUS = 1 AND
					CRIPD.ORDER_NO = '".$orderNo."' AND
					CRIPD.ORDER_YEAR = '".$orderYear."' AND
					CRIPD.SALES_ORDER_ID = '".$salesOrderId."' ";
		if($color!='')
		$sql		.=" AND CRIPD.INK_COLOR = '".$color."' ";
		if($technique!='')
		$sql		.=" AND CRIPD.INK_TECHNIQUE = '".$technique."' ";
		if($inkType!='')
		$sql		.=" AND CRIPD.INK_TYPE = '".$inkType."' ";
		if($locationId!='')
		$sql		.=" AND ink_color_room_issue_to_production_header.LOCATION_ID = '".$locationId."' ";
		
		$sql		.=" AND PRD.DAY_END_PROCESS_ID IS NULL ";
		//echo $sql;
		$result	= $this->db->RunQuery($sql);
 		return  $result[0]['ISSUED_WEIGHT']-$result[0]['RETURNED_WEIGHT'];	
 		
	}
	
	public function getProcessedUsageOfColor($locationId,$orderNo,$orderYear,$salesOrderId,$color,$technique, $inkType,$deci)
	{
		 $sql	= "   
 					SELECT 
					ROUND(sum(CRIPD.INK_WEIGHT),$deci+2) AS ISSUED_WEIGHT, 
					ROUND(sum(IFNULL(PRD.INK_WEIGHT,0)),$deci+2) AS RETURNED_WEIGHT 
 					FROM ink_color_room_issue_to_production_header
					INNER JOIN ink_color_room_issue_to_production_details AS CRIPD ON CRIPD.SERIAL_NO = ink_color_room_issue_to_production_header.SERIAL_NO AND CRIPD.SERIAL_YEAR = ink_color_room_issue_to_production_header.SERIAL_YEAR
					LEFT JOIN ink_production_return_header AS PRH ON PRH.ISSUE_NO = ink_color_room_issue_to_production_header.SERIAL_NO AND PRH.ISSUE_YEAR = ink_color_room_issue_to_production_header.SERIAL_YEAR AND PRH.`STATUS` = 1
					LEFT JOIN ink_production_return_details AS PRD ON PRD.SERIAL_NO = PRH.SERIAL_NO AND PRD.SERIAL_YEAR = PRH.SERIAL_YEAR AND CRIPD.ORDER_NO = PRD.ORDER_NO AND CRIPD.ORDER_YEAR = PRD.ORDER_YEAR AND CRIPD.SALES_ORDER_ID = PRD.SALES_ORDER_ID AND CRIPD.INK_COLOR = PRD.INK_COLOR AND CRIPD.INK_TECHNIQUE = PRD.INK_TECHNIQUE AND CRIPD.INK_TYPE = PRD.INK_TYPE

					WHERE ink_color_room_issue_to_production_header.STATUS = 1 AND
					CRIPD.ORDER_NO = '".$orderNo."' AND
					CRIPD.ORDER_YEAR = '".$orderYear."' AND
					CRIPD.SALES_ORDER_ID = '".$salesOrderId."' ";
		if($color!='')
		$sql		.=" AND CRIPD.INK_COLOR = '".$color."' ";
		if($technique!='')
		$sql		.=" AND CRIPD.INK_TECHNIQUE = '".$technique."' ";
		if($inkType!='')
		$sql		.=" AND CRIPD.INK_TYPE = '".$inkType."' ";
		if($locationId!='')
		$sql		.=" AND ink_color_room_issue_to_production_header.LOCATION_ID = '".$locationId."' ";
		
		$sql		.=" AND PRD.DAY_END_PROCESS_ID >=0  ";
		//echo $sql;
		$result	= $this->db->RunQuery($sql);
 		return  $result[0]['ISSUED_WEIGHT']-$result[0]['RETURNED_WEIGHT'];	
 		
	}
	
	
	//END }
}
?>
<?php
class ware_mrndetails{
 
	private $db;
	private $table= "ware_mrndetails";
	
	//private property
	private $intMrnNo;
	private $intMrnYear;
	private $intOrderNo;
	private $intOrderYear;
	private $strStyleNo;
	private $intItemId;
	private $dblQty;
	private $dblIssudQty;
	private $dblMRNClearQty;
	private $EXCEED_TYPE;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('intMrnNo'=>'intMrnNo',
										'intMrnYear'=>'intMrnYear',
										'intOrderNo'=>'intOrderNo',
										'intOrderYear'=>'intOrderYear',
										'strStyleNo'=>'strStyleNo',
										'intItemId'=>'intItemId',
										'dblQty'=>'dblQty',
										'dblIssudQty'=>'dblIssudQty',
										'dblMRNClearQty'=>'dblMRNClearQty',
										'EXCEED_TYPE'=>'EXCEED_TYPE',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "intMrnNo = ".$this->intMrnNo." and intMrnYear = ".$this->intMrnYear." and intOrderNo = ".$this->intOrderNo." and intOrderYear = ".$this->intOrderYear." and strStyleNo = ".$this->strStyleNo." and intItemId = ".$this->intItemId."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun intMrnNo
	function getintMrnNo()
	{
		$this->validate();
		return $this->intMrnNo;
	}
	
	//retun intMrnYear
	function getintMrnYear()
	{
		$this->validate();
		return $this->intMrnYear;
	}
	
	//retun intOrderNo
	function getintOrderNo()
	{
		$this->validate();
		return $this->intOrderNo;
	}
	
	//retun intOrderYear
	function getintOrderYear()
	{
		$this->validate();
		return $this->intOrderYear;
	}
	
	//retun strStyleNo
	function getstrStyleNo()
	{
		$this->validate();
		return $this->strStyleNo;
	}
	
	//retun intItemId
	function getintItemId()
	{
		$this->validate();
		return $this->intItemId;
	}
	
	//retun dblQty
	function getdblQty()
	{
		$this->validate();
		return $this->dblQty;
	}
	
	//retun dblIssudQty
	function getdblIssudQty()
	{
		$this->validate();
		return $this->dblIssudQty;
	}
	
	//retun dblMRNClearQty
	function getdblMRNClearQty()
	{
		$this->validate();
		return $this->dblMRNClearQty;
	}
	
	//retun EXCEED_TYPE
	function getEXCEED_TYPE()
	{
		$this->validate();
		return $this->EXCEED_TYPE;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set intMrnNo
	function setintMrnNo($intMrnNo)
	{
		array_push($this->commitArray,'intMrnNo');
		$this->intMrnNo = $intMrnNo;
	}
	
	//set intMrnYear
	function setintMrnYear($intMrnYear)
	{
		array_push($this->commitArray,'intMrnYear');
		$this->intMrnYear = $intMrnYear;
	}
	
	//set intOrderNo
	function setintOrderNo($intOrderNo)
	{
		array_push($this->commitArray,'intOrderNo');
		$this->intOrderNo = $intOrderNo;
	}
	
	//set intOrderYear
	function setintOrderYear($intOrderYear)
	{
		array_push($this->commitArray,'intOrderYear');
		$this->intOrderYear = $intOrderYear;
	}
	
	//set strStyleNo
	function setstrStyleNo($strStyleNo)
	{
		array_push($this->commitArray,'strStyleNo');
		$this->strStyleNo = $strStyleNo;
	}
	
	//set intItemId
	function setintItemId($intItemId)
	{
		array_push($this->commitArray,'intItemId');
		$this->intItemId = $intItemId;
	}
	
	//set dblQty
	function setdblQty($dblQty)
	{
		array_push($this->commitArray,'dblQty');
		$this->dblQty = $dblQty;
	}
	
	//set dblIssudQty
	function setdblIssudQty($dblIssudQty)
	{
		array_push($this->commitArray,'dblIssudQty');
		$this->dblIssudQty = $dblIssudQty;
	}
	
	//set dblMRNClearQty
	function setdblMRNClearQty($dblMRNClearQty)
	{
		array_push($this->commitArray,'dblMRNClearQty');
		$this->dblMRNClearQty = $dblMRNClearQty;
	}
	
	//set EXCEED_TYPE
	function setEXCEED_TYPE($EXCEED_TYPE)
	{
		array_push($this->commitArray,'EXCEED_TYPE');
		$this->EXCEED_TYPE = $EXCEED_TYPE;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->intMrnNo=='' || $this->intMrnYear=='' || $this->intOrderNo=='' || $this->intOrderYear=='' || $this->strStyleNo=='' || $this->intItemId=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($intMrnNo , $intMrnYear , $intOrderNo , $intOrderYear , $strStyleNo , $intItemId)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "intMrnNo='$intMrnNo' and intMrnYear='$intMrnYear' and intOrderNo='$intOrderNo' and intOrderYear='$intOrderYear' and strStyleNo='$strStyleNo' and intItemId='$intItemId'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($intMrnNo,$intMrnYear,$intOrderNo,$intOrderYear,$strStyleNo,$intItemId,$dblQty,$dblIssudQty,$dblMRNClearQty,$EXCEED_TYPE){
		$data = array('intMrnNo'=>$intMrnNo 
				,'intMrnYear'=>$intMrnYear 
				,'intOrderNo'=>$intOrderNo 
				,'intOrderYear'=>$intOrderYear 
				,'strStyleNo'=>$strStyleNo 
				,'intItemId'=>$intItemId 
				,'dblQty'=>$dblQty 
				,'dblIssudQty'=>$dblIssudQty 
				,'dblMRNClearQty'=>$dblMRNClearQty 
				,'EXCEED_TYPE'=>$EXCEED_TYPE 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('intMrnNo,intMrnYear',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intMrnNo'])
				$html .= '<option selected="selected" value="'.$row['intMrnNo'].'">'.$row['intMrnYear'].'</option>';	
			else
				$html .= '<option value="'.$row['intMrnNo'].'">'.$row['intMrnYear'].'</option>';	
		}
		return $html;
	}
	
	//END }
}
?>
<?php
class ware_fabricdispatchconfirmation_header{
 
	private $db;
	private $table= "ware_fabricdispatchconfirmation_header";
	
	//private property
	private $DISP_CONFIRM_NO;
	private $DISP_CONFIRM_YEAR;
	private $ORDER_NO;
	private $ORDER_YEAR;
	private $STATUS;
	private $APPROVE_LEVELS;
	private $DATE;
	private $CREATED_DATE;
	private $CREATED_BY;
	private $MODIFIED_DATE;
	private $MODIFIED_BY;
	private $LOCATION_ID;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('DISP_CONFIRM_NO'=>'DISP_CONFIRM_NO',
										'DISP_CONFIRM_YEAR'=>'DISP_CONFIRM_YEAR',
										'ORDER_NO'=>'ORDER_NO',
										'ORDER_YEAR'=>'ORDER_YEAR',
										'STATUS'=>'STATUS',
										'APPROVE_LEVELS'=>'APPROVE_LEVELS',
										'DATE'=>'DATE',
										'CREATED_DATE'=>'CREATED_DATE',
										'CREATED_BY'=>'CREATED_BY',
										'MODIFIED_DATE'=>'MODIFIED_DATE',
										'MODIFIED_BY'=>'MODIFIED_BY',
										'LOCATION_ID'=>'LOCATION_ID',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "DISP_CONFIRM_NO = ".$this->DISP_CONFIRM_NO." and DISP_CONFIRM_YEAR = ".$this->DISP_CONFIRM_YEAR."" ;
		//unset($this->commitArray);
		$commitArray   = array();                             
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun DISP_CONFIRM_NO
	function getDISP_CONFIRM_NO()
	{
		$this->validate();
		return $this->DISP_CONFIRM_NO;
	}
	
	//retun DISP_CONFIRM_YEAR
	function getDISP_CONFIRM_YEAR()
	{
		$this->validate();
		return $this->DISP_CONFIRM_YEAR;
	}
	
	//retun ORDER_NO
	function getORDER_NO()
	{
		$this->validate();
		return $this->ORDER_NO;
	}
	
	//retun ORDER_YEAR
	function getORDER_YEAR()
	{
		$this->validate();
		return $this->ORDER_YEAR;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//retun APPROVE_LEVELS
	function getAPPROVE_LEVELS()
	{
		$this->validate();
		return $this->APPROVE_LEVELS;
	}
	
	//retun DATE
	function getDATE()
	{
		$this->validate();
		return $this->DATE;
	}
	
	//retun CREATED_DATE
	function getCREATED_DATE()
	{
		$this->validate();
		return $this->CREATED_DATE;
	}
	
	//retun CREATED_BY
	function getCREATED_BY()
	{
		$this->validate();
		return $this->CREATED_BY;
	}
	
	//retun MODIFIED_DATE
	function getMODIFIED_DATE()
	{
		$this->validate();
		return $this->MODIFIED_DATE;
	}
	
	//retun MODIFIED_BY
	function getMODIFIED_BY()
	{
		$this->validate();
		return $this->MODIFIED_BY;
	}
	
	//retun LOCATION_ID
	function getLOCATION_ID()
	{
		$this->validate();
		return $this->LOCATION_ID;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set DISP_CONFIRM_NO
	function setDISP_CONFIRM_NO($DISP_CONFIRM_NO)
	{
		array_push($this->commitArray,'DISP_CONFIRM_NO');
		$this->DISP_CONFIRM_NO = $DISP_CONFIRM_NO;
	}
	
	//set DISP_CONFIRM_YEAR
	function setDISP_CONFIRM_YEAR($DISP_CONFIRM_YEAR)
	{
		array_push($this->commitArray,'DISP_CONFIRM_YEAR');
		$this->DISP_CONFIRM_YEAR = $DISP_CONFIRM_YEAR;
	}
	
	//set ORDER_NO
	function setORDER_NO($ORDER_NO)
	{
		array_push($this->commitArray,'ORDER_NO');
		$this->ORDER_NO = $ORDER_NO;
	}
	
	//set ORDER_YEAR
	function setORDER_YEAR($ORDER_YEAR)
	{
		array_push($this->commitArray,'ORDER_YEAR');
		$this->ORDER_YEAR = $ORDER_YEAR;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//set APPROVE_LEVELS
	function setAPPROVE_LEVELS($APPROVE_LEVELS)
	{
		array_push($this->commitArray,'APPROVE_LEVELS');
		$this->APPROVE_LEVELS = $APPROVE_LEVELS;
	}
	
	//set DATE
	function setDATE($DATE)
	{
		array_push($this->commitArray,'DATE');
		$this->DATE = $DATE;
	}
	
	//set CREATED_DATE
	function setCREATED_DATE($CREATED_DATE)
	{
		array_push($this->commitArray,'CREATED_DATE');
		$this->CREATED_DATE = $CREATED_DATE;
	}
	
	//set CREATED_BY
	function setCREATED_BY($CREATED_BY)
	{
		array_push($this->commitArray,'CREATED_BY');
		$this->CREATED_BY = $CREATED_BY;
	}
	
	//set MODIFIED_DATE
	function setMODIFIED_DATE($MODIFIED_DATE)
	{
		array_push($this->commitArray,'MODIFIED_DATE');
		$this->MODIFIED_DATE = $MODIFIED_DATE;
	}
	
	//set MODIFIED_BY
	function setMODIFIED_BY($MODIFIED_BY)
	{
		array_push($this->commitArray,'MODIFIED_BY');
		$this->MODIFIED_BY = $MODIFIED_BY;
	}
	
	//set LOCATION_ID
	function setLOCATION_ID($LOCATION_ID)
	{
		array_push($this->commitArray,'LOCATION_ID');
		$this->LOCATION_ID = $LOCATION_ID;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->DISP_CONFIRM_NO=='' || $this->DISP_CONFIRM_YEAR=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($DISP_CONFIRM_NO , $DISP_CONFIRM_YEAR)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "DISP_CONFIRM_NO='$DISP_CONFIRM_NO' and DISP_CONFIRM_YEAR='$DISP_CONFIRM_YEAR'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($DISP_CONFIRM_NO,$DISP_CONFIRM_YEAR,$ORDER_NO,$ORDER_YEAR,$STATUS,$APPROVE_LEVELS,$DATE,$CREATED_DATE,$CREATED_BY,$MODIFIED_DATE,$MODIFIED_BY,$LOCATION_ID){
		$data = array('DISP_CONFIRM_NO'=>$DISP_CONFIRM_NO 
				,'DISP_CONFIRM_YEAR'=>$DISP_CONFIRM_YEAR 
				,'ORDER_NO'=>$ORDER_NO 
				,'ORDER_YEAR'=>$ORDER_YEAR 
				,'STATUS'=>$STATUS 
				,'APPROVE_LEVELS'=>$APPROVE_LEVELS 
				,'DATE'=>$DATE 
				,'CREATED_DATE'=>$CREATED_DATE 
				,'CREATED_BY'=>$CREATED_BY 
				,'MODIFIED_DATE'=>$MODIFIED_DATE 
				,'MODIFIED_BY'=>$MODIFIED_BY 
				,'LOCATION_ID'=>$LOCATION_ID 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('DISP_CONFIRM_NO,DISP_CONFIRM_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['DISP_CONFIRM_NO'])
				$html .= '<option selected="selected" value="'.$row['DISP_CONFIRM_NO'].'">'.$row['DISP_CONFIRM_YEAR'].'</option>';	
			else
				$html .= '<option value="'.$row['DISP_CONFIRM_NO'].'">'.$row['DISP_CONFIRM_YEAR'].'</option>';	
		}
		return $html;
	}
	
	//END }
}
?>
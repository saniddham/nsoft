<?php
class mst_inksubcategory{
 
	private $db;
	private $table= "mst_inksubcategory";
	
	//private property
	private $intInkSubCategoryId;
	private $intInkMainCategoryId;
	private $strInkSubCategory;
	private $intStatus;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('intInkSubCategoryId'=>'intInkSubCategoryId',
										'intInkMainCategoryId'=>'intInkMainCategoryId',
										'strInkSubCategory'=>'strInkSubCategory',
										'intStatus'=>'intStatus',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	function commit()
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "intInkSubCategoryId = ".$this->intInkSubCategoryId."" ;
		unset($this->commitArray);
		
		if($this->intId==NULL || $this->intId=='' || $this->intId=='NULL')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun intInkSubCategoryId
	function getintInkSubCategoryId()
	{
		$this->validate();
		return $this->intInkSubCategoryId;
	}
	
	//retun intInkMainCategoryId
	function getintInkMainCategoryId()
	{
		$this->validate();
		return $this->intInkMainCategoryId;
	}
	
	//retun strInkSubCategory
	function getstrInkSubCategory()
	{
		$this->validate();
		return $this->strInkSubCategory;
	}
	
	//retun intStatus
	function getintStatus()
	{
		$this->validate();
		return $this->intStatus;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set intInkSubCategoryId
	function setintInkSubCategoryId($intInkSubCategoryId)
	{
		array_push($this->commitArray,'intInkSubCategoryId');
		$this->intInkSubCategoryId = $intInkSubCategoryId;
	}
	
	//set intInkMainCategoryId
	function setintInkMainCategoryId($intInkMainCategoryId)
	{
		array_push($this->commitArray,'intInkMainCategoryId');
		$this->intInkMainCategoryId = $intInkMainCategoryId;
	}
	
	//set strInkSubCategory
	function setstrInkSubCategory($strInkSubCategory)
	{
		array_push($this->commitArray,'strInkSubCategory');
		$this->strInkSubCategory = $strInkSubCategory;
	}
	
	//set intStatus
	function setintStatus($intStatus)
	{
		array_push($this->commitArray,'intStatus');
		$this->intStatus = $intStatus;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->intInkSubCategoryId=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($intInkSubCategoryId)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "intInkSubCategoryId='$intInkSubCategoryId'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
		//else
			//throw new Exception("Record not found.");
	}	
	
	
	//END }
}
?>
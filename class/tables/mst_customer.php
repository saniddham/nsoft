<?php
class mst_customer{
 
	private $db;
	private $table= "mst_customer";
	
	//private property
	private $intId;
	private $strCode;
	private $strName;
        private $ACTIVE_SAMPLE;
        private $ACTIVE_BULK;
	private $intTypeId;
	private $strAddress;
	private $strContactPerson;
	private $strCity;
	private $intCountryId;
	private $intCurrencyId;
	private $strPhoneNo;
	private $strMobileNo;
	private $strFaxNo;
	private $strEmail;
	private $strWebSite;
	private $intShipmentId;
	private $strVatNo;
	private $strSVatNo;
	private $strRegistrationNo;
	private $strInvoiceType;
	private $intPaymentsTermsId;
	private $intPaymentsMethodsId;
	private $intCreditLimit;
	private $intChartOfAccountId;
	private $strBlocked;
	private $intRank;
	private $intStatus;
	private $intApproveLevel;
	private $intCreator;
	private $dtmCreateDate;
	private $intModifyer;
	private $dtmModifyDate;
	private $intMailAlertDispatchSummaryReport;
	private $intSizeWiseSummary;
	private $intBrandWiseEmails;
	private $SUB_TYPE;
	private $CHART_OF_ACCOUNT;
	private $PAYMENT_COLLECT_DATE;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('intId'=>'intId',
										'strCode'=>'strCode',
										'strName'=>'strName',
										'intTypeId'=>'intTypeId',
                                                                                'ACTIVE_SAMPLE'=>'ACTIVE_SAMPLE',
                                                                                'ACTIVE_BULK'=>'ACTIVE_BULK',
										'strAddress'=>'strAddress',
										'strContactPerson'=>'strContactPerson',
										'strCity'=>'strCity',
										'intCountryId'=>'intCountryId',
										'intCurrencyId'=>'intCurrencyId',
										'strPhoneNo'=>'strPhoneNo',
										'strMobileNo'=>'strMobileNo',
										'strFaxNo'=>'strFaxNo',
										'strEmail'=>'strEmail',
										'strWebSite'=>'strWebSite',
										'intShipmentId'=>'intShipmentId',
										'strVatNo'=>'strVatNo',
										'strSVatNo'=>'strSVatNo',
										'strRegistrationNo'=>'strRegistrationNo',
										'strInvoiceType'=>'strInvoiceType',
										'intPaymentsTermsId'=>'intPaymentsTermsId',
										'intPaymentsMethodsId'=>'intPaymentsMethodsId',
										'intCreditLimit'=>'intCreditLimit',
										'intChartOfAccountId'=>'intChartOfAccountId',
										'strBlocked'=>'strBlocked',
										'intRank'=>'intRank',
										'intStatus'=>'intStatus',
										'intApproveLevel'=>'intApproveLevel',
										'intCreator'=>'intCreator',
										'dtmCreateDate'=>'dtmCreateDate',
										'intModifyer'=>'intModifyer',
										'dtmModifyDate'=>'dtmModifyDate',
										'intMailAlertDispatchSummaryReport'=>'intMailAlertDispatchSummaryReport',
										'intSizeWiseSummary'=>'intSizeWiseSummary',
										'intBrandWiseEmails'=>'intBrandWiseEmails',
										'SUB_TYPE'=>'SUB_TYPE',
										'CHART_OF_ACCOUNT'=>'CHART_OF_ACCOUNT',
										'PAYMENT_COLLECT_DATE'=>'PAYMENT_COLLECT_DATE',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "intId = ".$this->intId."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun intId
	function getintId()
	{
		$this->validate();
		return $this->intId;
	}
	
	//retun strCode
	function getstrCode()
	{
		$this->validate();
		return $this->strCode;
	}
	
	//retun strName
	function getstrName()
	{
		$this->validate();
		return $this->strName;
	}
        //return sample block
	function getactiveSample()
	{
            //echo "sample";
		$this->validate();
		return $this->ACTIVE_SAMPLE;
                
	}
        //return bulk block
        function getactiveBulk()
	{
		$this->validate();
		return $this->ACTIVE_BULK;
	}
	//retun intTypeId
	function getintTypeId()
	{
		$this->validate();
		return $this->intTypeId;
	}
	
	//retun strAddress
	function getstrAddress()
	{
		$this->validate();
		return $this->strAddress;
	}
	
	//retun strContactPerson
	function getstrContactPerson()
	{
		$this->validate();
		return $this->strContactPerson;
	}
	
	//retun strCity
	function getstrCity()
	{
		$this->validate();
		return $this->strCity;
	}
	
	//retun intCountryId
	function getintCountryId()
	{
		$this->validate();
		return $this->intCountryId;
	}
	
	//retun intCurrencyId
	function getintCurrencyId()
	{
		$this->validate();
		return $this->intCurrencyId;
	}
	
	//retun strPhoneNo
	function getstrPhoneNo()
	{
		$this->validate();
		return $this->strPhoneNo;
	}
	
	//retun strMobileNo
	function getstrMobileNo()
	{
		$this->validate();
		return $this->strMobileNo;
	}
	
	//retun strFaxNo
	function getstrFaxNo()
	{
		$this->validate();
		return $this->strFaxNo;
	}
	
	//retun strEmail
	function getstrEmail()
	{
		$this->validate();
		return $this->strEmail;
	}
	
	//retun strWebSite
	function getstrWebSite()
	{
		$this->validate();
		return $this->strWebSite;
	}
	
	//retun intShipmentId
	function getintShipmentId()
	{
		$this->validate();
		return $this->intShipmentId;
	}
	
	//retun strVatNo
	function getstrVatNo()
	{
		$this->validate();
		return $this->strVatNo;
	}
	
	//retun strSVatNo
	function getstrSVatNo()
	{
		$this->validate();
		return $this->strSVatNo;
	}
	
	//retun strRegistrationNo
	function getstrRegistrationNo()
	{
		$this->validate();
		return $this->strRegistrationNo;
	}
	
	//retun strInvoiceType
	function getstrInvoiceType()
	{
		$this->validate();
		return $this->strInvoiceType;
	}
	
	//retun intPaymentsTermsId
	function getintPaymentsTermsId()
	{
		$this->validate();
		return $this->intPaymentsTermsId;
	}
	
	//retun intPaymentsMethodsId
	function getintPaymentsMethodsId()
	{
		$this->validate();
		return $this->intPaymentsMethodsId;
	}
	
	//retun intCreditLimit
	function getintCreditLimit()
	{
		$this->validate();
		return $this->intCreditLimit;
	}
	
	//retun intChartOfAccountId
	function getintChartOfAccountId()
	{
		$this->validate();
		return $this->intChartOfAccountId;
	}
	
	//retun strBlocked
	function getstrBlocked()
	{
		$this->validate();
		return $this->strBlocked;
	}
	
	//retun intRank
	function getintRank()
	{
		$this->validate();
		return $this->intRank;
	}
	
	//retun intStatus
	function getintStatus()
	{
		$this->validate();
		return $this->intStatus;
	}
	
	//retun intApproveLevel
	function getintApproveLevel()
	{
		$this->validate();
		return $this->intApproveLevel;
	}
	
	//retun intCreator
	function getintCreator()
	{
		$this->validate();
		return $this->intCreator;
	}
	
	//retun dtmCreateDate
	function getdtmCreateDate()
	{
		$this->validate();
		return $this->dtmCreateDate;
	}
	
	//retun intModifyer
	function getintModifyer()
	{
		$this->validate();
		return $this->intModifyer;
	}
	
	//retun dtmModifyDate
	function getdtmModifyDate()
	{
		$this->validate();
		return $this->dtmModifyDate;
	}
	
	//retun intMailAlertDispatchSummaryReport
	function getintMailAlertDispatchSummaryReport()
	{
		$this->validate();
		return $this->intMailAlertDispatchSummaryReport;
	}
	
	//retun intSizeWiseSummary
	function getintSizeWiseSummary()
	{
		$this->validate();
		return $this->intSizeWiseSummary;
	}
	
	//retun intBrandWiseEmails
	function getintBrandWiseEmails()
	{
		$this->validate();
		return $this->intBrandWiseEmails;
	}
	
	//retun SUB_TYPE
	function getSUB_TYPE()
	{
		$this->validate();
		return $this->SUB_TYPE;
	}
	
	//retun CHART_OF_ACCOUNT
	function getCHART_OF_ACCOUNT()
	{
		$this->validate();
		return $this->CHART_OF_ACCOUNT;
	}
	
	//retun PAYMENT_COLLECT_DATE
	function getPAYMENT_COLLECT_DATE()
	{
		$this->validate();
		return $this->PAYMENT_COLLECT_DATE;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set intId
	function setintId($intId)
	{
		array_push($this->commitArray,'intId');
		$this->intId = $intId;
	}
	
	//set strCode
	function setstrCode($strCode)
	{
		array_push($this->commitArray,'strCode');
		$this->strCode = $strCode;
	}
	
	//set strName
	
	function setstrName($strName)
	{
		array_push($this->commitArray,'strName');
		$this->strName = $strName;
	}
        //set active sample
        function setactiveSample($ACTIVE_SAMPLE)
	{
		array_push($this->commitArray,'ACTIVE_SAMPLE');
		$this->ACTIVE_SAMPLE = $ACTIVE_SAMPLE;
	}
        //set active bulk
        function setactiveBulk($ACTIVE_BULK)
	{
		array_push($this->commitArray,'ACTIVE_BULK');
		$this->ACTIVE_BULK = $ACTIVE_BULK;
	}
	//set intTypeId
	function setintTypeId($intTypeId)
	{
		array_push($this->commitArray,'intTypeId');
		$this->intTypeId = $intTypeId;
	}
	
	//set strAddress
	function setstrAddress($strAddress)
	{
		array_push($this->commitArray,'strAddress');
		$this->strAddress = $strAddress;
	}
	
	//set strContactPerson
	function setstrContactPerson($strContactPerson)
	{
		array_push($this->commitArray,'strContactPerson');
		$this->strContactPerson = $strContactPerson;
	}
	
	//set strCity
	function setstrCity($strCity)
	{
		array_push($this->commitArray,'strCity');
		$this->strCity = $strCity;
	}
	
	//set intCountryId
	function setintCountryId($intCountryId)
	{
		array_push($this->commitArray,'intCountryId');
		$this->intCountryId = $intCountryId;
	}
	
	//set intCurrencyId
	function setintCurrencyId($intCurrencyId)
	{
		array_push($this->commitArray,'intCurrencyId');
		$this->intCurrencyId = $intCurrencyId;
	}
	
	//set strPhoneNo
	function setstrPhoneNo($strPhoneNo)
	{
		array_push($this->commitArray,'strPhoneNo');
		$this->strPhoneNo = $strPhoneNo;
	}
	
	//set strMobileNo
	function setstrMobileNo($strMobileNo)
	{
		array_push($this->commitArray,'strMobileNo');
		$this->strMobileNo = $strMobileNo;
	}
	
	//set strFaxNo
	function setstrFaxNo($strFaxNo)
	{
		array_push($this->commitArray,'strFaxNo');
		$this->strFaxNo = $strFaxNo;
	}
	
	//set strEmail
	function setstrEmail($strEmail)
	{
		array_push($this->commitArray,'strEmail');
		$this->strEmail = $strEmail;
	}
	
	//set strWebSite
	function setstrWebSite($strWebSite)
	{
		array_push($this->commitArray,'strWebSite');
		$this->strWebSite = $strWebSite;
	}
	
	//set intShipmentId
	function setintShipmentId($intShipmentId)
	{
		array_push($this->commitArray,'intShipmentId');
		$this->intShipmentId = $intShipmentId;
	}
	
	//set strVatNo
	function setstrVatNo($strVatNo)
	{
		array_push($this->commitArray,'strVatNo');
		$this->strVatNo = $strVatNo;
	}
	
	//set strSVatNo
	function setstrSVatNo($strSVatNo)
	{
		array_push($this->commitArray,'strSVatNo');
		$this->strSVatNo = $strSVatNo;
	}
	
	//set strRegistrationNo
	function setstrRegistrationNo($strRegistrationNo)
	{
		array_push($this->commitArray,'strRegistrationNo');
		$this->strRegistrationNo = $strRegistrationNo;
	}
	
	//set strInvoiceType
	function setstrInvoiceType($strInvoiceType)
	{
		array_push($this->commitArray,'strInvoiceType');
		$this->strInvoiceType = $strInvoiceType;
	}
	
	//set intPaymentsTermsId
	function setintPaymentsTermsId($intPaymentsTermsId)
	{
		array_push($this->commitArray,'intPaymentsTermsId');
		$this->intPaymentsTermsId = $intPaymentsTermsId;
	}
	
	//set intPaymentsMethodsId
	function setintPaymentsMethodsId($intPaymentsMethodsId)
	{
		array_push($this->commitArray,'intPaymentsMethodsId');
		$this->intPaymentsMethodsId = $intPaymentsMethodsId;
	}
	
	//set intCreditLimit
	function setintCreditLimit($intCreditLimit)
	{
		array_push($this->commitArray,'intCreditLimit');
		$this->intCreditLimit = $intCreditLimit;
	}
	
	//set intChartOfAccountId
	function setintChartOfAccountId($intChartOfAccountId)
	{
		array_push($this->commitArray,'intChartOfAccountId');
		$this->intChartOfAccountId = $intChartOfAccountId;
	}
	
	//set strBlocked
	function setstrBlocked($strBlocked)
	{
		array_push($this->commitArray,'strBlocked');
		$this->strBlocked = $strBlocked;
	}
	
	//set intRank
	function setintRank($intRank)
	{
		array_push($this->commitArray,'intRank');
		$this->intRank = $intRank;
	}
	
	//set intStatus
	function setintStatus($intStatus)
	{
		array_push($this->commitArray,'intStatus');
		$this->intStatus = $intStatus;
	}
	
	//set intApproveLevel
	function setintApproveLevel($intApproveLevel)
	{
		array_push($this->commitArray,'intApproveLevel');
		$this->intApproveLevel = $intApproveLevel;
	}
	
	//set intCreator
	function setintCreator($intCreator)
	{
		array_push($this->commitArray,'intCreator');
		$this->intCreator = $intCreator;
	}
	
	//set dtmCreateDate
	function setdtmCreateDate($dtmCreateDate)
	{
		array_push($this->commitArray,'dtmCreateDate');
		$this->dtmCreateDate = $dtmCreateDate;
	}
	
	//set intModifyer
	function setintModifyer($intModifyer)
	{
		array_push($this->commitArray,'intModifyer');
		$this->intModifyer = $intModifyer;
	}
	
	//set dtmModifyDate
	function setdtmModifyDate($dtmModifyDate)
	{
		array_push($this->commitArray,'dtmModifyDate');
		$this->dtmModifyDate = $dtmModifyDate;
	}
	
	//set intMailAlertDispatchSummaryReport
	function setintMailAlertDispatchSummaryReport($intMailAlertDispatchSummaryReport)
	{
		array_push($this->commitArray,'intMailAlertDispatchSummaryReport');
		$this->intMailAlertDispatchSummaryReport = $intMailAlertDispatchSummaryReport;
	}
	
	//set intSizeWiseSummary
	function setintSizeWiseSummary($intSizeWiseSummary)
	{
		array_push($this->commitArray,'intSizeWiseSummary');
		$this->intSizeWiseSummary = $intSizeWiseSummary;
	}
	
	//set intBrandWiseEmails
	function setintBrandWiseEmails($intBrandWiseEmails)
	{
		array_push($this->commitArray,'intBrandWiseEmails');
		$this->intBrandWiseEmails = $intBrandWiseEmails;
	}
	
	//set SUB_TYPE
	function setSUB_TYPE($SUB_TYPE)
	{
		array_push($this->commitArray,'SUB_TYPE');
		$this->SUB_TYPE = $SUB_TYPE;
	}
	
	//set CHART_OF_ACCOUNT
	function setCHART_OF_ACCOUNT($CHART_OF_ACCOUNT)
	{
		array_push($this->commitArray,'CHART_OF_ACCOUNT');
		$this->CHART_OF_ACCOUNT = $CHART_OF_ACCOUNT;
	}
	
	//set PAYMENT_COLLECT_DATE
	function setPAYMENT_COLLECT_DATE($PAYMENT_COLLECT_DATE)
	{
		array_push($this->commitArray,'PAYMENT_COLLECT_DATE');
		$this->PAYMENT_COLLECT_DATE = $PAYMENT_COLLECT_DATE;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->intId=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($intId)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "intId='$intId'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($intId,$strCode,$strName,$intTypeId,$strAddress,$strContactPerson,$strCity,$intCountryId,$intCurrencyId,$strPhoneNo,$strMobileNo,$strFaxNo,$strEmail,$strWebSite,$intShipmentId,$strVatNo,$strSVatNo,$strRegistrationNo,$strInvoiceType,$intPaymentsTermsId,$intPaymentsMethodsId,$intCreditLimit,$intChartOfAccountId,$strBlocked,$intRank,$intStatus,$intApproveLevel,$intCreator,$dtmCreateDate,$intModifyer,$dtmModifyDate,$intMailAlertDispatchSummaryReport,$intSizeWiseSummary,$intBrandWiseEmails,$SUB_TYPE,$CHART_OF_ACCOUNT,$PAYMENT_COLLECT_DATE){
		$data = array('intId'=>$intId 
				,'strCode'=>$strCode 
				,'strName'=>$strName 
				,'intTypeId'=>$intTypeId 
				,'strAddress'=>$strAddress 
				,'strContactPerson'=>$strContactPerson 
				,'strCity'=>$strCity 
				,'intCountryId'=>$intCountryId 
				,'intCurrencyId'=>$intCurrencyId 
				,'strPhoneNo'=>$strPhoneNo 
				,'strMobileNo'=>$strMobileNo 
				,'strFaxNo'=>$strFaxNo 
				,'strEmail'=>$strEmail 
				,'strWebSite'=>$strWebSite 
				,'intShipmentId'=>$intShipmentId 
				,'strVatNo'=>$strVatNo 
				,'strSVatNo'=>$strSVatNo 
				,'strRegistrationNo'=>$strRegistrationNo 
				,'strInvoiceType'=>$strInvoiceType 
				,'intPaymentsTermsId'=>$intPaymentsTermsId 
				,'intPaymentsMethodsId'=>$intPaymentsMethodsId 
				,'intCreditLimit'=>$intCreditLimit 
				,'intChartOfAccountId'=>$intChartOfAccountId 
				,'strBlocked'=>$strBlocked 
				,'intRank'=>$intRank 
				,'intStatus'=>$intStatus 
				,'intApproveLevel'=>$intApproveLevel 
				,'intCreator'=>$intCreator 
				,'dtmCreateDate'=>$dtmCreateDate 
				,'intModifyer'=>$intModifyer 
				,'dtmModifyDate'=>$dtmModifyDate 
				,'intMailAlertDispatchSummaryReport'=>$intMailAlertDispatchSummaryReport 
				,'intSizeWiseSummary'=>$intSizeWiseSummary 
				,'intBrandWiseEmails'=>$intBrandWiseEmails 
				,'SUB_TYPE'=>$SUB_TYPE 
				,'CHART_OF_ACCOUNT'=>$CHART_OF_ACCOUNT 
				,'PAYMENT_COLLECT_DATE'=>$PAYMENT_COLLECT_DATE 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
           
		$result = $this->select('intId,strName',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intId'])
				$html .= '<option selected="selected" value="'.$row['intId'].'">'.$row['strName'].'</option>';	
			else
				$html .= '<option value="'.$row['intId'].'">'.$row['strName'].'</option>';	
		}
		return $html;
	}
	
	function getCustomers_to_send_dispatch_summery(){
		
		return $result = $this->select('mst_customer.intId AS customerId',  null, $where = 'mst_customer.SEND_DAILY_DISPATCH_EMAIL = 1');
	}
	
	//END }
}
?>
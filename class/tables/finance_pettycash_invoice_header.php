<?php
class finance_pettycash_invoice_header{

	private $db;
	private $table= "finance_pettycash_invoice_header";

	//private property
	private $PETTY_INVOICE_NO;
	private $PETTY_INVOICE_YEAR;
	private $IOU_PARENT_NO;
	private $IOU_PARENT_YEAR;
	private $DATE;
	private $INVOICE_TYPE;
	private $REMARKS;
	private $LOCATION_ID;
	private $STATUS;
	private $APPROVE_LEVEL;
	private $CREATED_BY;
	private $CREATED_DATE;
	private $MODIFIED_BY;
	private $MODIFIED_DATE;
	private $REQUEST_STATUS;
	private $commitArray = array();
	private $field_array = array();
	

	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('PETTY_INVOICE_NO'=>'PETTY_INVOICE_NO',
										'PETTY_INVOICE_YEAR'=>'PETTY_INVOICE_YEAR',
										'IOU_PARENT_NO'=>'IOU_PARENT_NO',
										'IOU_PARENT_YEAR'=>'IOU_PARENT_YEAR',
										'DATE'=>'DATE',
										'INVOICE_TYPE'=>'INVOICE_TYPE',
										'REMARKS'=>'REMARKS',
										'LOCATION_ID'=>'LOCATION_ID',
										'STATUS'=>'STATUS',
										'APPROVE_LEVEL'=>'APPROVE_LEVEL',
										'CREATED_BY'=>'CREATED_BY',
										'CREATED_DATE'=>'CREATED_DATE',
										'MODIFIED_BY'=>'MODIFIED_BY',
										'MODIFIED_DATE'=>'MODIFIED_DATE',
										'REQUEST_STATUS'=>'REQUEST_STATUS',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit );
		return $result;
	}

	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data);
		return $this->db->getResult();


	}

	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}

	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();
	}

	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}

	public function get_field_array(){
		return $this->field_array;
	}

	function commit($type='update')
	{
		//commit update
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{
			$data[$v] = $this->$v;
		}
		$where		= "PETTY_INVOICE_NO = ".$this->PETTY_INVOICE_NO." and PETTY_INVOICE_YEAR = ".$this->PETTY_INVOICE_YEAR."" ;
		$this->commitArray = array();

		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}

	//BEGIN - public functions for private properties {
	
	//retun PETTY_INVOICE_NO
	function getPETTY_INVOICE_NO()
	{
		$this->validate();
		return $this->PETTY_INVOICE_NO;
	}
	
	//retun PETTY_INVOICE_YEAR
	function getPETTY_INVOICE_YEAR()
	{
		$this->validate();
		return $this->PETTY_INVOICE_YEAR;
	}
	
	//retun IOU_PARENT_NO
	function getIOU_PARENT_NO()
	{
		$this->validate();
		return $this->IOU_PARENT_NO;
	}
	
	//retun IOU_PARENT_YEAR
	function getIOU_PARENT_YEAR()
	{
		$this->validate();
		return $this->IOU_PARENT_YEAR;
	}
	
	//retun DATE
	function getDATE()
	{
		$this->validate();
		return $this->DATE;
	}
	
	//retun INVOICE_TYPE
	function getINVOICE_TYPE()
	{
		$this->validate();
		return $this->INVOICE_TYPE;
	}
	
	//retun REMARKS
	function getREMARKS()
	{
		$this->validate();
		return $this->REMARKS;
	}
	
	//retun LOCATION_ID
	function getLOCATION_ID()
	{
		$this->validate();
		return $this->LOCATION_ID;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//retun APPROVE_LEVEL
	function getAPPROVE_LEVEL()
	{
		$this->validate();
		return $this->APPROVE_LEVEL;
	}
	
	//retun CREATED_BY
	function getCREATED_BY()
	{
		$this->validate();
		return $this->CREATED_BY;
	}
	
	//retun CREATED_DATE
	function getCREATED_DATE()
	{
		$this->validate();
		return $this->CREATED_DATE;
	}
	
	//retun MODIFIED_BY
	function getMODIFIED_BY()
	{
		$this->validate();
		return $this->MODIFIED_BY;
	}
	
	//retun MODIFIED_DATE
	function getMODIFIED_DATE()
	{
		$this->validate();
		return $this->MODIFIED_DATE;
	}
	
	//retun REQUEST_STATUS
	function getREQUEST_STATUS()
	{
		$this->validate();
		return $this->REQUEST_STATUS;
	}
	
	//END }

	//BEGIN - public function set {
	
	//set PETTY_INVOICE_NO
	function setPETTY_INVOICE_NO($PETTY_INVOICE_NO)
	{
		array_push($this->commitArray,'PETTY_INVOICE_NO');
		$this->PETTY_INVOICE_NO = $PETTY_INVOICE_NO;
	}
	
	//set PETTY_INVOICE_YEAR
	function setPETTY_INVOICE_YEAR($PETTY_INVOICE_YEAR)
	{
		array_push($this->commitArray,'PETTY_INVOICE_YEAR');
		$this->PETTY_INVOICE_YEAR = $PETTY_INVOICE_YEAR;
	}
	
	//set IOU_PARENT_NO
	function setIOU_PARENT_NO($IOU_PARENT_NO)
	{
		array_push($this->commitArray,'IOU_PARENT_NO');
		$this->IOU_PARENT_NO = $IOU_PARENT_NO;
	}
	
	//set IOU_PARENT_YEAR
	function setIOU_PARENT_YEAR($IOU_PARENT_YEAR)
	{
		array_push($this->commitArray,'IOU_PARENT_YEAR');
		$this->IOU_PARENT_YEAR = $IOU_PARENT_YEAR;
	}
	
	//set DATE
	function setDATE($DATE)
	{
		array_push($this->commitArray,'DATE');
		$this->DATE = $DATE;
	}
	
	//set INVOICE_TYPE
	function setINVOICE_TYPE($INVOICE_TYPE)
	{
		array_push($this->commitArray,'INVOICE_TYPE');
		$this->INVOICE_TYPE = $INVOICE_TYPE;
	}
	
	//set REMARKS
	function setREMARKS($REMARKS)
	{
		array_push($this->commitArray,'REMARKS');
		$this->REMARKS = $REMARKS;
	}
	
	//set LOCATION_ID
	function setLOCATION_ID($LOCATION_ID)
	{
		array_push($this->commitArray,'LOCATION_ID');
		$this->LOCATION_ID = $LOCATION_ID;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//set APPROVE_LEVEL
	function setAPPROVE_LEVEL($APPROVE_LEVEL)
	{
		array_push($this->commitArray,'APPROVE_LEVEL');
		$this->APPROVE_LEVEL = $APPROVE_LEVEL;
	}
	
	//set CREATED_BY
	function setCREATED_BY($CREATED_BY)
	{
		array_push($this->commitArray,'CREATED_BY');
		$this->CREATED_BY = $CREATED_BY;
	}
	
	//set CREATED_DATE
	function setCREATED_DATE($CREATED_DATE)
	{
		array_push($this->commitArray,'CREATED_DATE');
		$this->CREATED_DATE = $CREATED_DATE;
	}
	
	//set MODIFIED_BY
	function setMODIFIED_BY($MODIFIED_BY)
	{
		array_push($this->commitArray,'MODIFIED_BY');
		$this->MODIFIED_BY = $MODIFIED_BY;
	}
	
	//set MODIFIED_DATE
	function setMODIFIED_DATE($MODIFIED_DATE)
	{
		array_push($this->commitArray,'MODIFIED_DATE');
		$this->MODIFIED_DATE = $MODIFIED_DATE;
	}
	
	//set REQUEST_STATUS
	function setREQUEST_STATUS($REQUEST_STATUS)
	{
		array_push($this->commitArray,'REQUEST_STATUS');
		$this->REQUEST_STATUS = $REQUEST_STATUS;
	}
	
	//END }

	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->PETTY_INVOICE_NO=='' || $this->PETTY_INVOICE_YEAR=='')
			throw new exception("Set primary values first");
		else
			return true;
	}

	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}

	public function set($PETTY_INVOICE_NO , $PETTY_INVOICE_YEAR)
	{
		$cols	= "*";

		$join	= NULL;

		$where	= "PETTY_INVOICE_NO='$PETTY_INVOICE_NO' and PETTY_INVOICE_YEAR='$PETTY_INVOICE_YEAR'";

		$result = $this->select($cols,$join,$where);
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}

	//insert as parameters
	public function insertRec($PETTY_INVOICE_NO,$PETTY_INVOICE_YEAR,$IOU_PARENT_NO,$IOU_PARENT_YEAR,$DATE,$INVOICE_TYPE,$REMARKS,$LOCATION_ID,$STATUS,$APPROVE_LEVEL,$CREATED_BY,$CREATED_DATE,$MODIFIED_BY,$MODIFIED_DATE){
		$data = array('PETTY_INVOICE_NO'=>$PETTY_INVOICE_NO 
				,'PETTY_INVOICE_YEAR'=>$PETTY_INVOICE_YEAR 
				,'IOU_PARENT_NO'=>$IOU_PARENT_NO 
				,'IOU_PARENT_YEAR'=>$IOU_PARENT_YEAR 
				,'DATE'=>$DATE 
				,'INVOICE_TYPE'=>$INVOICE_TYPE 
				,'REMARKS'=>$REMARKS 
				,'LOCATION_ID'=>$LOCATION_ID 
				,'STATUS'=>$STATUS 
				,'APPROVE_LEVEL'=>$APPROVE_LEVEL 
				,'CREATED_BY'=>$CREATED_BY 
				,'CREATED_DATE'=>$CREATED_DATE 
				,'MODIFIED_BY'=>$MODIFIED_BY 
				,'MODIFIED_DATE'=>$MODIFIED_DATE 
				);
		return $this->insert($data);
	}

	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('PETTY_INVOICE_NO,PETTY_INVOICE_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['PETTY_INVOICE_NO'])
				$html .= '<option selected="selected" value="'.$row['PETTY_INVOICE_NO'].'">'.$row['PETTY_INVOICE_YEAR'].'</option>';
			else
				$html .= '<option value="'.$row['PETTY_INVOICE_NO'].'">'.$row['PETTY_INVOICE_YEAR'].'</option>';
		}
		return $html;
	}
	
	//END }
}
?>
<?php
class mst_inkcategory_item{
 
	private $db;
	private $table= "mst_inkcategory_item";
	
	//private property
	private $intSubCategoryId;
	private $intMainCategoryId;
	private $intItemId;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('intSubCategoryId'=>'intSubCategoryId',
										'intMainCategoryId'=>'intMainCategoryId',
										'intItemId'=>'intItemId',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	function commit()
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "intItemId = ".$this->intItemId."" ;
		unset($this->commitArray);
		
		if($this->intId==NULL || $this->intId=='' || $this->intId=='NULL')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun intSubCategoryId
	function getintSubCategoryId()
	{
		$this->validate();
		return $this->intSubCategoryId;
	}
	
	//retun intMainCategoryId
	function getintMainCategoryId()
	{
		$this->validate();
		return $this->intMainCategoryId;
	}
	
	//retun intItemId
	function getintItemId()
	{
		$this->validate();
		return $this->intItemId;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set intSubCategoryId
	function setintSubCategoryId($intSubCategoryId)
	{
		array_push($this->commitArray,'intSubCategoryId');
		$this->intSubCategoryId = $intSubCategoryId;
	}
	
	//set intMainCategoryId
	function setintMainCategoryId($intMainCategoryId)
	{
		array_push($this->commitArray,'intMainCategoryId');
		$this->intMainCategoryId = $intMainCategoryId;
	}
	
	//set intItemId
	function setintItemId($intItemId)
	{
		array_push($this->commitArray,'intItemId');
		$this->intItemId = $intItemId;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->intItemId=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($intItemId)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "intItemId='$intItemId'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
		//else
			//throw new Exception("Record not found.");
	}	
	
	
	//END }
}
?>
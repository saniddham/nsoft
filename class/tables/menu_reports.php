<?php
class menu_reports{
 
	private $db;
	private $table= "menu_reports";
	
	//private property
	private $REPORT_ID;
	private $REPORT_URL;
	private $commitArray = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->db->field_array_set(array('REPORT_ID'=>'REPORT_ID',
										'REPORT_URL'=>'REPORT_URL',
										));
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function commit()
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = ''.$this->$v.'';
		}
		$where		= "REPORT_ID = '".$this->REPORT_ID."'" ;
		unset($this->commitArray);
		return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun REPORT_ID
	function getREPORT_ID()
	{
		$this->validate();
		return $this->REPORT_ID;
	}
	
	//retun REPORT_URL
	function getREPORT_URL()
	{
		$this->validate();
		return $this->REPORT_URL;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set REPORT_ID
	function setREPORT_ID($REPORT_ID)
	{
		array_push($this->commitArray,'REPORT_ID');
		$this->REPORT_ID = $REPORT_ID;
	}
	
	//set REPORT_URL
	function setREPORT_URL($REPORT_URL)
	{
		array_push($this->commitArray,'REPORT_URL');
		$this->REPORT_URL = $REPORT_URL;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->REPORT_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
		}
	}
	
	public function set($REPORT_ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "REPORT_ID='$REPORT_ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			$this->setVariables($result);
		else
			throw new Exception("Record not found.");
	}	
	
	
	//END }
}
?>
<?php
class finance_supplier_payment_header{

	private $db;
	private $table= "finance_supplier_payment_header";

	//private property
	private $PAYMENT_NO;
	private $PAYMENT_YEAR;
	private $VOUCHER_NO;
	private $SUPPLIER_ID;
	private $PAY_DATE;
	private $COMPANY_ID;
	private $LOCATION_ID;
	private $CURRENCY_ID;
	private $PAY_METHOD;
	private $REMARKS;
	private $BANK_REFERENCE_NO;
	private $STATUS;
	private $PRINT_STATUS;
	private $VOUCHER_CONFIRMATION;
	private $CHEQUE_PRINT_STATUS;
	private $CHEQUE_ISSUE_DATE;
	private $CHEQUE_PRINT_DATE;
	private $CHEQUE_PRINT_BY;
	private $CHEQUE_CANCEL_DATE;
	private $CHEQUE_CANCEL_BY;
	private $APPROVE_LEVELS;
	private $CREATED_BY;
	private $CREATED_DATE;
	private $LAST_MODIFY_BY;
	private $LAST_MODIFY_DATE;
	private $commitArray = array();
	private $field_array = array();
	

	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('PAYMENT_NO'=>'PAYMENT_NO',
										'PAYMENT_YEAR'=>'PAYMENT_YEAR',
										'VOUCHER_NO'=>'VOUCHER_NO',
										'SUPPLIER_ID'=>'SUPPLIER_ID',
										'PAY_DATE'=>'PAY_DATE',
										'COMPANY_ID'=>'COMPANY_ID',
										'LOCATION_ID'=>'LOCATION_ID',
										'CURRENCY_ID'=>'CURRENCY_ID',
										'PAY_METHOD'=>'PAY_METHOD',
										'REMARKS'=>'REMARKS',
										'BANK_REFERENCE_NO'=>'BANK_REFERENCE_NO',
										'STATUS'=>'STATUS',
										'PRINT_STATUS'=>'PRINT_STATUS',
										'VOUCHER_CONFIRMATION'=>'VOUCHER_CONFIRMATION',
										'CHEQUE_PRINT_STATUS'=>'CHEQUE_PRINT_STATUS',
										'CHEQUE_ISSUE_DATE'=>'CHEQUE_ISSUE_DATE',
										'CHEQUE_PRINT_DATE'=>'CHEQUE_PRINT_DATE',
										'CHEQUE_PRINT_BY'=>'CHEQUE_PRINT_BY',
										'CHEQUE_CANCEL_DATE'=>'CHEQUE_CANCEL_DATE',
										'CHEQUE_CANCEL_BY'=>'CHEQUE_CANCEL_BY',
										'APPROVE_LEVELS'=>'APPROVE_LEVELS',
										'CREATED_BY'=>'CREATED_BY',
										'CREATED_DATE'=>'CREATED_DATE',
										'LAST_MODIFY_BY'=>'LAST_MODIFY_BY',
										'LAST_MODIFY_DATE'=>'LAST_MODIFY_DATE',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit );
		return $result;
	}

	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data);
		return $this->db->getResult();


	}

	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}

	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();
	}

	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}

	public function get_field_array(){
		return $this->field_array;
	}

	function commit($type='update')
	{
		//commit update
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{
			$data[$v] = $this->$v;
		}
		$where		= " PAYMENT_NO = ".$this->PAYMENT_NO." and PAYMENT_YEAR = ".$this->PAYMENT_YEAR." " ;
		$this->commitArray = array();

		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}

	//BEGIN - public functions for private properties {
	
	//retun PAYMENT_NO
	function getPAYMENT_NO()
	{
		$this->validate();
		return $this->PAYMENT_NO;
	}
	
	//retun PAYMENT_YEAR
	function getPAYMENT_YEAR()
	{
		$this->validate();
		return $this->PAYMENT_YEAR;
	}
	
	//retun VOUCHER_NO
	function getVOUCHER_NO()
	{
		$this->validate();
		return $this->VOUCHER_NO;
	}
	
	//retun SUPPLIER_ID
	function getSUPPLIER_ID()
	{
		$this->validate();
		return $this->SUPPLIER_ID;
	}
	
	//retun PAY_DATE
	function getPAY_DATE()
	{
		$this->validate();
		return $this->PAY_DATE;
	}
	
	//retun COMPANY_ID
	function getCOMPANY_ID()
	{
		$this->validate();
		return $this->COMPANY_ID;
	}
	
	//retun LOCATION_ID
	function getLOCATION_ID()
	{
		$this->validate();
		return $this->LOCATION_ID;
	}
	
	//retun CURRENCY_ID
	function getCURRENCY_ID()
	{
		$this->validate();
		return $this->CURRENCY_ID;
	}
	
	//retun PAY_METHOD
	function getPAY_METHOD()
	{
		$this->validate();
		return $this->PAY_METHOD;
	}
	
	//retun REMARKS
	function getREMARKS()
	{
		$this->validate();
		return $this->REMARKS;
	}
	
	//retun BANK_REFERENCE_NO
	function getBANK_REFERENCE_NO()
	{
		$this->validate();
		return $this->BANK_REFERENCE_NO;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//retun PRINT_STATUS
	function getPRINT_STATUS()
	{
		$this->validate();
		return $this->PRINT_STATUS;
	}
	
	//retun VOUCHER_CONFIRMATION
	function getVOUCHER_CONFIRMATION()
	{
		$this->validate();
		return $this->VOUCHER_CONFIRMATION;
	}
	
	//retun CHEQUE_PRINT_STATUS
	function getCHEQUE_PRINT_STATUS()
	{
		$this->validate();
		return $this->CHEQUE_PRINT_STATUS;
	}
	
	//retun CHEQUE_ISSUE_DATE
	function getCHEQUE_ISSUE_DATE()
	{
		$this->validate();
		return $this->CHEQUE_ISSUE_DATE;
	}
	
	//retun CHEQUE_PRINT_DATE
	function getCHEQUE_PRINT_DATE()
	{
		$this->validate();
		return $this->CHEQUE_PRINT_DATE;
	}
	
	//retun CHEQUE_PRINT_BY
	function getCHEQUE_PRINT_BY()
	{
		$this->validate();
		return $this->CHEQUE_PRINT_BY;
	}
	
	//retun CHEQUE_CANCEL_DATE
	function getCHEQUE_CANCEL_DATE()
	{
		$this->validate();
		return $this->CHEQUE_CANCEL_DATE;
	}
	
	//retun CHEQUE_CANCEL_BY
	function getCHEQUE_CANCEL_BY()
	{
		$this->validate();
		return $this->CHEQUE_CANCEL_BY;
	}
	
	//retun APPROVE_LEVELS
	function getAPPROVE_LEVELS()
	{
		$this->validate();
		return $this->APPROVE_LEVELS;
	}
	
	//retun CREATED_BY
	function getCREATED_BY()
	{
		$this->validate();
		return $this->CREATED_BY;
	}
	
	//retun CREATED_DATE
	function getCREATED_DATE()
	{
		$this->validate();
		return $this->CREATED_DATE;
	}
	
	//retun LAST_MODIFY_BY
	function getLAST_MODIFY_BY()
	{
		$this->validate();
		return $this->LAST_MODIFY_BY;
	}
	
	//retun LAST_MODIFY_DATE
	function getLAST_MODIFY_DATE()
	{
		$this->validate();
		return $this->LAST_MODIFY_DATE;
	}
	
	//END }

	//BEGIN - public function set {
	
	//set PAYMENT_NO
	function setPAYMENT_NO($PAYMENT_NO)
	{
		array_push($this->commitArray,'PAYMENT_NO');
		$this->PAYMENT_NO = $PAYMENT_NO;
	}
	
	//set PAYMENT_YEAR
	function setPAYMENT_YEAR($PAYMENT_YEAR)
	{
		array_push($this->commitArray,'PAYMENT_YEAR');
		$this->PAYMENT_YEAR = $PAYMENT_YEAR;
	}
	
	//set VOUCHER_NO
	function setVOUCHER_NO($VOUCHER_NO)
	{
		array_push($this->commitArray,'VOUCHER_NO');
		$this->VOUCHER_NO = $VOUCHER_NO;
	}
	
	//set SUPPLIER_ID
	function setSUPPLIER_ID($SUPPLIER_ID)
	{
		array_push($this->commitArray,'SUPPLIER_ID');
		$this->SUPPLIER_ID = $SUPPLIER_ID;
	}
	
	//set PAY_DATE
	function setPAY_DATE($PAY_DATE)
	{
		array_push($this->commitArray,'PAY_DATE');
		$this->PAY_DATE = $PAY_DATE;
	}
	
	//set COMPANY_ID
	function setCOMPANY_ID($COMPANY_ID)
	{
		array_push($this->commitArray,'COMPANY_ID');
		$this->COMPANY_ID = $COMPANY_ID;
	}
	
	//set LOCATION_ID
	function setLOCATION_ID($LOCATION_ID)
	{
		array_push($this->commitArray,'LOCATION_ID');
		$this->LOCATION_ID = $LOCATION_ID;
	}
	
	//set CURRENCY_ID
	function setCURRENCY_ID($CURRENCY_ID)
	{
		array_push($this->commitArray,'CURRENCY_ID');
		$this->CURRENCY_ID = $CURRENCY_ID;
	}
	
	//set PAY_METHOD
	function setPAY_METHOD($PAY_METHOD)
	{
		array_push($this->commitArray,'PAY_METHOD');
		$this->PAY_METHOD = $PAY_METHOD;
	}
	
	//set REMARKS
	function setREMARKS($REMARKS)
	{
		array_push($this->commitArray,'REMARKS');
		$this->REMARKS = $REMARKS;
	}
	
	//set BANK_REFERENCE_NO
	function setBANK_REFERENCE_NO($BANK_REFERENCE_NO)
	{
		array_push($this->commitArray,'BANK_REFERENCE_NO');
		$this->BANK_REFERENCE_NO = $BANK_REFERENCE_NO;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//set PRINT_STATUS
	function setPRINT_STATUS($PRINT_STATUS)
	{
		array_push($this->commitArray,'PRINT_STATUS');
		$this->PRINT_STATUS = $PRINT_STATUS;
	}
	
	//set VOUCHER_CONFIRMATION
	function setVOUCHER_CONFIRMATION($VOUCHER_CONFIRMATION)
	{
		array_push($this->commitArray,'VOUCHER_CONFIRMATION');
		$this->VOUCHER_CONFIRMATION = $VOUCHER_CONFIRMATION;
	}
	
	//set CHEQUE_PRINT_STATUS
	function setCHEQUE_PRINT_STATUS($CHEQUE_PRINT_STATUS)
	{
		array_push($this->commitArray,'CHEQUE_PRINT_STATUS');
		$this->CHEQUE_PRINT_STATUS = $CHEQUE_PRINT_STATUS;
	}
	
	//set CHEQUE_ISSUE_DATE
	function setCHEQUE_ISSUE_DATE($CHEQUE_ISSUE_DATE)
	{
		array_push($this->commitArray,'CHEQUE_ISSUE_DATE');
		$this->CHEQUE_ISSUE_DATE = $CHEQUE_ISSUE_DATE;
	}
	
	//set CHEQUE_PRINT_DATE
	function setCHEQUE_PRINT_DATE($CHEQUE_PRINT_DATE)
	{
		array_push($this->commitArray,'CHEQUE_PRINT_DATE');
		$this->CHEQUE_PRINT_DATE = $CHEQUE_PRINT_DATE;
	}
	
	//set CHEQUE_PRINT_BY
	function setCHEQUE_PRINT_BY($CHEQUE_PRINT_BY)
	{
		array_push($this->commitArray,'CHEQUE_PRINT_BY');
		$this->CHEQUE_PRINT_BY = $CHEQUE_PRINT_BY;
	}
	
	//set CHEQUE_CANCEL_DATE
	function setCHEQUE_CANCEL_DATE($CHEQUE_CANCEL_DATE)
	{
		array_push($this->commitArray,'CHEQUE_CANCEL_DATE');
		$this->CHEQUE_CANCEL_DATE = $CHEQUE_CANCEL_DATE;
	}
	
	//set CHEQUE_CANCEL_BY
	function setCHEQUE_CANCEL_BY($CHEQUE_CANCEL_BY)
	{
		array_push($this->commitArray,'CHEQUE_CANCEL_BY');
		$this->CHEQUE_CANCEL_BY = $CHEQUE_CANCEL_BY;
	}
	
	//set APPROVE_LEVELS
	function setAPPROVE_LEVELS($APPROVE_LEVELS)
	{
		array_push($this->commitArray,'APPROVE_LEVELS');
		$this->APPROVE_LEVELS = $APPROVE_LEVELS;
	}
	
	//set CREATED_BY
	function setCREATED_BY($CREATED_BY)
	{
		array_push($this->commitArray,'CREATED_BY');
		$this->CREATED_BY = $CREATED_BY;
	}
	
	//set CREATED_DATE
	function setCREATED_DATE($CREATED_DATE)
	{
		array_push($this->commitArray,'CREATED_DATE');
		$this->CREATED_DATE = $CREATED_DATE;
	}
	
	//set LAST_MODIFY_BY
	function setLAST_MODIFY_BY($LAST_MODIFY_BY)
	{
		array_push($this->commitArray,'LAST_MODIFY_BY');
		$this->LAST_MODIFY_BY = $LAST_MODIFY_BY;
	}
	
	//set LAST_MODIFY_DATE
	function setLAST_MODIFY_DATE($LAST_MODIFY_DATE)
	{
		array_push($this->commitArray,'LAST_MODIFY_DATE');
		$this->LAST_MODIFY_DATE = $LAST_MODIFY_DATE;
	}
	
	//END }

	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->PAYMENT_NO=='' || $this->PAYMENT_YEAR=='')
			throw new exception("Set primary values first");
		else
			return true;
	}

	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}

	public function set($PAYMENT_NO , $PAYMENT_YEAR)
	{
		$cols	= "*";

		$join	= NULL;

		$where	= "PAYMENT_NO='$PAYMENT_NO' and PAYMENT_YEAR='$PAYMENT_YEAR' ";

		$result = $this->select($cols,$join,$where);
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}

	//insert as parameters
	public function insertRec($PAYMENT_NO,$PAYMENT_YEAR,$VOUCHER_NO,$SUPPLIER_ID,$PAY_DATE,$COMPANY_ID,$LOCATION_ID,$CURRENCY_ID,$PAY_METHOD,$REMARKS,$BANK_REFERENCE_NO,$STATUS,$PRINT_STATUS,$VOUCHER_CONFIRMATION,$CHEQUE_PRINT_STATUS,$CHEQUE_ISSUE_DATE,$CHEQUE_PRINT_DATE,$CHEQUE_PRINT_BY,$CHEQUE_CANCEL_DATE,$CHEQUE_CANCEL_BY,$APPROVE_LEVELS,$CREATED_BY,$CREATED_DATE,$LAST_MODIFY_BY,$LAST_MODIFY_DATE){
		$data = array('PAYMENT_NO'=>$PAYMENT_NO 
				,'PAYMENT_YEAR'=>$PAYMENT_YEAR 
				,'VOUCHER_NO'=>$VOUCHER_NO 
				,'SUPPLIER_ID'=>$SUPPLIER_ID 
				,'PAY_DATE'=>$PAY_DATE 
				,'COMPANY_ID'=>$COMPANY_ID 
				,'LOCATION_ID'=>$LOCATION_ID 
				,'CURRENCY_ID'=>$CURRENCY_ID 
				,'PAY_METHOD'=>$PAY_METHOD 
				,'REMARKS'=>$REMARKS 
				,'BANK_REFERENCE_NO'=>$BANK_REFERENCE_NO 
				,'STATUS'=>$STATUS 
				,'PRINT_STATUS'=>$PRINT_STATUS 
				,'VOUCHER_CONFIRMATION'=>$VOUCHER_CONFIRMATION 
				,'CHEQUE_PRINT_STATUS'=>$CHEQUE_PRINT_STATUS 
				,'CHEQUE_ISSUE_DATE'=>$CHEQUE_ISSUE_DATE 
				,'CHEQUE_PRINT_DATE'=>$CHEQUE_PRINT_DATE 
				,'CHEQUE_PRINT_BY'=>$CHEQUE_PRINT_BY 
				,'CHEQUE_CANCEL_DATE'=>$CHEQUE_CANCEL_DATE 
				,'CHEQUE_CANCEL_BY'=>$CHEQUE_CANCEL_BY 
				,'APPROVE_LEVELS'=>$APPROVE_LEVELS 
				,'CREATED_BY'=>$CREATED_BY 
				,'CREATED_DATE'=>$CREATED_DATE 
				,'LAST_MODIFY_BY'=>$LAST_MODIFY_BY 
				,'LAST_MODIFY_DATE'=>$LAST_MODIFY_DATE 
				);
		return $this->insert($data);
	}

	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('PAYMENT_NO,PAYMENT_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['PAYMENT_NO'])
				$html .= '<option selected="selected" value="'.$row['PAYMENT_NO'].'">'.$row['PAYMENT_YEAR'].'</option>';
			else
				$html .= '<option value="'.$row['PAYMENT_NO'].'">'.$row['PAYMENT_YEAR'].'</option>';
		}
		return $html;
	}
	
	public function getChequeDetails($paymentNo,$paymentYear)
	{
		$cols	= " S.strName  AS SUPPLIER_NAME,
					CU.strCode AS CURRENCY_CODE,
					finance_supplier_payment_header.CHEQUE_PRINT_STATUS	AS CHEQUE_PRINT_STATUS,
					SUM(FSPD.AMOUNT) AS AMOUNT,
					COAB.BANK_ID ";
		
		$join	= " INNER JOIN finance_supplier_payment_details FSPD ON FSPD.PAYMENT_NO = finance_supplier_payment_header.PAYMENT_NO AND 
					FSPD.PAYMENT_YEAR = finance_supplier_payment_header.PAYMENT_YEAR
					INNER JOIN mst_supplier S ON S.intId = finance_supplier_payment_header.SUPPLIER_ID
					INNER JOIN mst_financecurrency CU ON CU.intId = finance_supplier_payment_header.CURRENCY_ID
					INNER JOIN finance_supplier_payment_gl SPGL ON SPGL.PAYMENT_NO = finance_supplier_payment_header.PAYMENT_NO AND 
					SPGL.PAYMENT_YEAR = finance_supplier_payment_header.PAYMENT_YEAR
					LEFT JOIN finance_mst_chartofaccount_bank COAB ON COAB.CHART_OF_ACCOUNT_ID = SPGL.ACCOUNT_ID ";
		
		$where	= " finance_supplier_payment_header.PAYMENT_NO = '".$paymentNo."' 
					AND finance_supplier_payment_header.PAYMENT_YEAR = '".$paymentYear."' ";
		
		$result = $this->select($cols,$join,$where,$order=null,$limit=null);
		$row	= mysqli_fetch_array($result);
		
		return $row;
	}
	public function getChequePrintingSuppliers($companyId)
	{
		$cols	= " DISTINCT finance_supplier_payment_header.SUPPLIER_ID,
					mst_supplier.strName ";
		
		$join	= " INNER JOIN mst_supplier ON mst_supplier.intId = finance_supplier_payment_header.SUPPLIER_ID ";
		
		$where	= " finance_supplier_payment_header.CHEQUE_PRINT_STATUS = 0 
					AND finance_supplier_payment_header.STATUS = 1 ";
		
		$order	= " mst_supplier.strName ";
		
		$result = $this->select($cols,$join,$where,$order,$limit=null);
		return $result;
	}
	public function getChequePrintListingData($supplierId,$companyId)
	{
		$cols	= " finance_supplier_payment_header.PAYMENT_NO,
					finance_supplier_payment_header.PAYMENT_YEAR,
					finance_supplier_payment_header.VOUCHER_NO,
					S.intId				AS SUPPLIER_ID,
					S.strName			AS SUPPLIER_NAME,
					finance_supplier_payment_header.PAY_DATE,
					finance_supplier_payment_header.CHEQUE_PRINT_STATUS,
					finance_supplier_payment_header.CHEQUE_ISSUE_DATE,
					FCOA.CHART_OF_ACCOUNT_ID	AS BANK_ID,
					FCOA.CHART_OF_ACCOUNT_NAME	AS BANK_NAME,
					(SELECT ROUND(SUM(FSPD.AMOUNT),2) 
					FROM finance_supplier_payment_details FSPD
					WHERE FSPD.PAYMENT_NO = finance_supplier_payment_header.PAYMENT_NO 
					AND FSPD.PAYMENT_YEAR = finance_supplier_payment_header.PAYMENT_YEAR) AS AMOUNT ";
		
		$join	= " INNER JOIN finance_supplier_payment_gl FSPGL ON FSPGL.PAYMENT_NO = finance_supplier_payment_header.PAYMENT_NO
					AND FSPGL.PAYMENT_YEAR = finance_supplier_payment_header.PAYMENT_YEAR
					INNER JOIN finance_mst_chartofaccount FCOA ON FCOA.CHART_OF_ACCOUNT_ID = FSPGL.ACCOUNT_ID
					INNER JOIN mst_supplier S ON S.intId = finance_supplier_payment_header.SUPPLIER_ID ";
		
		$where	= " finance_supplier_payment_header.COMPANY_ID = '".$companyId."' AND
					finance_supplier_payment_header.STATUS = 1 ";
		
		if($supplierId!='')
			$where .= " AND finance_supplier_payment_header.SUPPLIER_ID = '".$supplierId."' ";
		
		$result = $this->select($cols,$join,$where);
		return $result;
	}
	//END }
}
?>
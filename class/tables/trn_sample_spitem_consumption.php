<?php
class trn_sample_spitem_consumption{
 
	private $db;
	private $table= "trn_sample_spitem_consumption";
	
	//private property
	private $intSampleNo;
	private $intSampleYear;
	private $intRevisionNo;
	private $strCombo;
	private $strPrintName;
	private $intTechniqueId;
	private $intItem;
	private $dblQty;
	private $intUser;
	private $dtDate;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('intSampleNo'=>'intSampleNo',
										'intSampleYear'=>'intSampleYear',
										'intRevisionNo'=>'intRevisionNo',
										'strCombo'=>'strCombo',
										'strPrintName'=>'strPrintName',
										'intTechniqueId'=>'intTechniqueId',
										'intItem'=>'intItem',
										'dblQty'=>'dblQty',
										'intUser'=>'intUser',
										'dtDate'=>'dtDate',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "intSampleNo = ".$this->intSampleNo." and intSampleYear = ".$this->intSampleYear." and intRevisionNo = ".$this->intRevisionNo." and strCombo = ".$this->strCombo." and strPrintName = ".$this->strPrintName." and intTechniqueId = ".$this->intTechniqueId." and intItem = ".$this->intItem."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun intSampleNo
	function getintSampleNo()
	{
		$this->validate();
		return $this->intSampleNo;
	}
	
	//retun intSampleYear
	function getintSampleYear()
	{
		$this->validate();
		return $this->intSampleYear;
	}
	
	//retun intRevisionNo
	function getintRevisionNo()
	{
		$this->validate();
		return $this->intRevisionNo;
	}
	
	//retun strCombo
	function getstrCombo()
	{
		$this->validate();
		return $this->strCombo;
	}
	
	//retun strPrintName
	function getstrPrintName()
	{
		$this->validate();
		return $this->strPrintName;
	}
	
	//retun intTechniqueId
	function getintTechniqueId()
	{
		$this->validate();
		return $this->intTechniqueId;
	}
	
	//retun intItem
	function getintItem()
	{
		$this->validate();
		return $this->intItem;
	}
	
	//retun dblQty
	function getdblQty()
	{
		$this->validate();
		return $this->dblQty;
	}
	
	//retun intUser
	function getintUser()
	{
		$this->validate();
		return $this->intUser;
	}
	
	//retun dtDate
	function getdtDate()
	{
		$this->validate();
		return $this->dtDate;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set intSampleNo
	function setintSampleNo($intSampleNo)
	{
		array_push($this->commitArray,'intSampleNo');
		$this->intSampleNo = $intSampleNo;
	}
	
	//set intSampleYear
	function setintSampleYear($intSampleYear)
	{
		array_push($this->commitArray,'intSampleYear');
		$this->intSampleYear = $intSampleYear;
	}
	
	//set intRevisionNo
	function setintRevisionNo($intRevisionNo)
	{
		array_push($this->commitArray,'intRevisionNo');
		$this->intRevisionNo = $intRevisionNo;
	}
	
	//set strCombo
	function setstrCombo($strCombo)
	{
		array_push($this->commitArray,'strCombo');
		$this->strCombo = $strCombo;
	}
	
	//set strPrintName
	function setstrPrintName($strPrintName)
	{
		array_push($this->commitArray,'strPrintName');
		$this->strPrintName = $strPrintName;
	}
	
	//set intTechniqueId
	function setintTechniqueId($intTechniqueId)
	{
		array_push($this->commitArray,'intTechniqueId');
		$this->intTechniqueId = $intTechniqueId;
	}
	
	//set intItem
	function setintItem($intItem)
	{
		array_push($this->commitArray,'intItem');
		$this->intItem = $intItem;
	}
	
	//set dblQty
	function setdblQty($dblQty)
	{
		array_push($this->commitArray,'dblQty');
		$this->dblQty = $dblQty;
	}
	
	//set intUser
	function setintUser($intUser)
	{
		array_push($this->commitArray,'intUser');
		$this->intUser = $intUser;
	}
	
	//set dtDate
	function setdtDate($dtDate)
	{
		array_push($this->commitArray,'dtDate');
		$this->dtDate = $dtDate;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->intSampleNo=='' || $this->intSampleYear=='' || $this->intRevisionNo=='' || $this->strCombo=='' || $this->strPrintName=='' || $this->intTechniqueId=='' || $this->intItem=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($intSampleNo , $intSampleYear , $intRevisionNo , $strCombo , $strPrintName , $intTechniqueId , $intItem)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "intSampleNo='$intSampleNo' and intSampleYear='$intSampleYear' and intRevisionNo='$intRevisionNo' and strCombo='$strCombo' and strPrintName='$strPrintName' and intTechniqueId='$intTechniqueId' and intItem='$intItem'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($intSampleNo,$intSampleYear,$intRevisionNo,$strCombo,$strPrintName,$intTechniqueId,$intItem,$dblQty,$intUser,$dtDate){
		$data = array('intSampleNo'=>$intSampleNo 
				,'intSampleYear'=>$intSampleYear 
				,'intRevisionNo'=>$intRevisionNo 
				,'strCombo'=>$strCombo 
				,'strPrintName'=>$strPrintName 
				,'intTechniqueId'=>$intTechniqueId 
				,'intItem'=>$intItem 
				,'dblQty'=>$dblQty 
				,'intUser'=>$intUser 
				,'dtDate'=>$dtDate 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('intSampleNo,intSampleYear',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intSampleNo'])
				$html .= '<option selected="selected" value="'.$row['intSampleNo'].'">'.$row['intSampleYear'].'</option>';	
			else
				$html .= '<option value="'.$row['intSampleNo'].'">'.$row['intSampleYear'].'</option>';	
		}
		return $html;
	}
	
	public function getSpecialItemsForSelectedOrders($orderArray,$itemArray)
	{
		if($orderArray!='')
			$para	.= "AND CONCAT(OD.intOrderNo,'/',OD.intOrderYear,'/',OD.intSalesOrderId) IN ($orderArray)";
			
		if($itemArray!='')
			$para	.= "AND I.intId IN ($itemArray)"; 
//-----------------------------------------------------------------------------------------------------
		$cols		= "DISTINCT
						  I.intId  		AS ITEM_ID,
						  I.intUOM   	AS UNIT_ID,
						  I.strName		AS ITEM_NAME ";
						  							
		$join		= "INNER JOIN trn_orderdetails OD
						 ON OD.intSampleNo = trn_sample_spitem_consumption.intSampleNo
						 AND OD.intSampleYear = trn_sample_spitem_consumption.intSampleYear
						 AND OD.intRevisionNo = trn_sample_spitem_consumption.intRevisionNo
						 AND OD.strCombo = trn_sample_spitem_consumption.strCombo
						 AND OD.strPrintName = trn_sample_spitem_consumption.strPrintName
					   	 INNER JOIN mst_item I
						 ON I.intId = trn_sample_spitem_consumption.intItem";
					  
		$where		= "1=1
						$para";
					
		$order		= "I.strName";
		$result 	= $this->select($cols,$join,$where,$order,$limit=null);
 		return 		$result;
	}
	
	public function getSPItemsForSelectedOrdersAndSalesOrders($orderNo,$orderYear,$salesOrderId)
	{
		$cols		= "DISTINCT
 						intItem  AS ITEM_ID ";
						  							
		$join		= " INNER JOIN trn_orderdetails OD
						 ON OD.intSampleNo = trn_sample_spitem_consumption.intSampleNo
						 AND OD.intSampleYear = trn_sample_spitem_consumption.intSampleYear
						 AND OD.intRevisionNo = trn_sample_spitem_consumption.intRevisionNo
						 AND OD.strCombo = trn_sample_spitem_consumption.strCombo
						 AND OD.strPrintName = trn_sample_spitem_consumption.strPrintName ";
					  
		$where		= " OD.intOrderNo = '$orderNo'
						AND OD.intOrderYear = '$orderYear' ";
		
		if($salesOrderId!='')
			$where	.= " AND OD.intSalesOrderId = '$salesOrderId' ";
		
		$where		.= " GROUP BY OD.intSalesOrderId,intItem ";
			
		$result 	= $this->select($cols,$join,$where,$order=null,$limit=null);
 		return 		$result;
	}
	
	public function getSPItemWiseSalesOrders($orderNo,$orderYear,$salesOrderId,$item)
	{
		$cols		= " DISTINCT
 						OD.strSalesOrderNo,
						OD.intSalesOrderId,
						OD.intQty ";
						  							
		$join		= " INNER JOIN trn_orderdetails OD
						 ON OD.intSampleNo = trn_sample_spitem_consumption.intSampleNo
						 AND OD.intSampleYear = trn_sample_spitem_consumption.intSampleYear
						 AND OD.intRevisionNo = trn_sample_spitem_consumption.intRevisionNo
						 AND OD.strCombo = trn_sample_spitem_consumption.strCombo
						 AND OD.strPrintName = trn_sample_spitem_consumption.strPrintName ";
					  
		$where		= " trn_sample_spitem_consumption.intItem = '".$item."' AND
						OD.intOrderNo = '".$orderNo."' AND
						OD.intOrderYear = '".$orderYear."' ";
		
		if($salesOrderId!='')
			$where	.= " AND OD.intSalesOrderId = '".$salesOrderId."' ";

		$where		.= " GROUP BY OD.intSalesOrderId ";
			
		$result 	= $this->select($cols,$join,$where,$order=null,$limit=null);
 		return 		$result;
	}
	
		public function getSPItemsForSelectedSampleNos($sampleNo,$sampleYear,$revNo,$combo,$printName)
	{
		$cols		= "DISTINCT
 						intItem  AS ITEM_ID ";
						  							
 					  
		$where		= " intSampleNo = '$sampleNo'
						AND intSampleYear = '$sampleYear'
						AND intRevisionNo = '$revNo'
						AND strCombo = '$combo'
						AND strPrintName = '$printName' ";
		
 		
		$where		.= " GROUP BY intItem ";
			
		$result 	= $this->select($cols,$join,$where,$order=null,$limit=null);
 		return 		$result;
	}

	
	//END }
}
?>
<?php
class sys_no{

	private $db;
	private $table= "sys_no";

	//private property
	private $intCompanyId;
	private $START_SEQUENCE_NO;
	private $intSampleNo;
	private $intSampleDispatchNo;
	private $intPRNno;
	private $intOrderNo;
	private $intSampleOrderNo;
	private $intPONo;
	private $intGRNNo;
	private $intSupReturnNo;
	private $intMrnNo;
	private $intIssueNo;
	private $intReturnToStores;
	private $intGatePassNo;
	private $intGpTransfNo;
	private $intWareInvoiceNo;
	private $intWareItmAllocationNo;
	private $intFabricReceivedNo;
	private $intFabricTransferdNo;
	private $intFabricGatePassNo;
	private $intFabricGPTransfInNo;
	private $intProductionComplete;
	private $intFabricTransfeinNo;
	private $intFabricReturnNo;
	private $intFabricDamage;
	private $intFinishGoodsGatePass;
	private $intFinishGoodsGatePassIn;
	private $intBulkDispatchNo;
	private $intFabricCustReturnNo;
	private $intFabricCustReturnDispNo;
	private $intFin_SalesInvoiceNo;
	private $intFin_Customer_AdvanceNo;
	private $intFin_PaymentReceiveNo;
	private $intFin_CreditNoteNo;
	private $intFin_DebitNoteNo;
	private $JOURNAL_ENTRY;
	private $PAYMENT_SETTLEMENT_NO;
	private $BANK_PAYMENT_NO;
	private $intSRNNo;
	private $intSTNNo;
	private $intPurchaseInvoiceNo;
	private $intAdvancePaymentNo;
	private $intSupplierPaymentNo;
	private $intSupplierDebitNo;
	private $SUPPLIER_CREDIT_NOTE_NO;
	private $intOtherBillInvNo;
	private $intOtherBillPaymentNo;
	private $GAIN_LOSS_NO;
	private $intFin_BankPaymentNo;
	private $intReconciliationNo;
	private $intPaymentScheduleNo;
	private $BANK_TO_BANK_TRNS;
	private $intGeneralGatePassNo;
	private $GENERAL_GATEPASS_RETURN_NO;
	private $intFixedAssetDepNo;
	private $intLoanScheduleNo;
	private $intAdvanceSettleNo;
	private $intCustomerAdvSettleNo;
	private $SUP_INVOICE_SETTLEMENT;
	private $MRN_COLOR_ROOM;
	private $GENERAL_ITEM_DISPOSE_NO;
	private $SAMPLE_REQUISITION_NO;
	private $SUB_CONTRACT_PO_NO;
	private $SAMPLE_FABRICIN_NO;
	private $CUSTMER_LC;
	private $BUDGET_TRANSFER_NO;
	private $PETTY_CASH_INVOICE_NO;
	private $PETTY_CASH_REQUISITION;
	private $SUB_CONTRACT_GATE_PASS_NO;
	private $SUB_CONTRACT_RETURN_NO;
	private $OTHER_RECEIVABLE_INVOICE_NO;
	private $OTHER_RECEIVABLE_RECEIPT_NO;
	private $SUPPLIER_ADVANCE_BANK_SETTLE_NO;
	private $PETTY_CASH_REIMBURSEMENT_NO;
	private $EXCEED_SETTLE_NO;
	private $ISSUE_TO_COLOR_ROOM;
	private $COLOR_ISSUE_TO_PRODUCTION;
	private $COLOR_ROOM_REQUISITION_NO;
	private $COLOR_ROOM_WASTAGE_NO;
	private $ACTUAL_PRODUCTION;
	private $C_ROOM_RETURN_TO_STORES_NO;
	private $PRODUCTION_RETURN_TO_COLOR_ROOM;
	private $COLOR_CREATION_NO;
	private $ACTUAL_CONSUMPTION_OTHER_NO;
	private $CUSTOMER_DISPATCH_CONFIRM_NO;
	private $ORDER_ALLOCATION_NO;
	private $OPEN_ORDER_NO;
	private $CLOSE_ORDER_NO;
	private $OPEN_ORDER_REQ_NO;
	private $DEBIT_RECEIVE_NO;
	private $TEMP_DISPATCH_NO;
	private $commitArray = array();
	private $field_array = array();
	

	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('intCompanyId'=>'intCompanyId',
										'START_SEQUENCE_NO'=>'START_SEQUENCE_NO',
										'intSampleNo'=>'intSampleNo',
										'intSampleDispatchNo'=>'intSampleDispatchNo',
										'intPRNno'=>'intPRNno',
										'intOrderNo'=>'intOrderNo',
										'intSampleOrderNo'=>'intSampleOrderNo',
										'intPONo'=>'intPONo',
										'intGRNNo'=>'intGRNNo',
										'intSupReturnNo'=>'intSupReturnNo',
										'intMrnNo'=>'intMrnNo',
										'intIssueNo'=>'intIssueNo',
										'intReturnToStores'=>'intReturnToStores',
										'intGatePassNo'=>'intGatePassNo',
										'intGpTransfNo'=>'intGpTransfNo',
										'intWareInvoiceNo'=>'intWareInvoiceNo',
										'intWareItmAllocationNo'=>'intWareItmAllocationNo',
										'intFabricReceivedNo'=>'intFabricReceivedNo',
										'intFabricTransferdNo'=>'intFabricTransferdNo',
										'intFabricGatePassNo'=>'intFabricGatePassNo',
										'intFabricGPTransfInNo'=>'intFabricGPTransfInNo',
										'intProductionComplete'=>'intProductionComplete',
										'intFabricTransfeinNo'=>'intFabricTransfeinNo',
										'intFabricReturnNo'=>'intFabricReturnNo',
										'intFabricDamage'=>'intFabricDamage',
										'intFinishGoodsGatePass'=>'intFinishGoodsGatePass',
										'intFinishGoodsGatePassIn'=>'intFinishGoodsGatePassIn',
										'intBulkDispatchNo'=>'intBulkDispatchNo',
										'intFabricCustReturnNo'=>'intFabricCustReturnNo',
										'intFabricCustReturnDispNo'=>'intFabricCustReturnDispNo',
										'intFin_SalesInvoiceNo'=>'intFin_SalesInvoiceNo',
										'intFin_Customer_AdvanceNo'=>'intFin_Customer_AdvanceNo',
										'intFin_PaymentReceiveNo'=>'intFin_PaymentReceiveNo',
										'intFin_CreditNoteNo'=>'intFin_CreditNoteNo',
										'intFin_DebitNoteNo'=>'intFin_DebitNoteNo',
										'JOURNAL_ENTRY'=>'JOURNAL_ENTRY',
										'PAYMENT_SETTLEMENT_NO'=>'PAYMENT_SETTLEMENT_NO',
										'BANK_PAYMENT_NO'=>'BANK_PAYMENT_NO',
										'intSRNNo'=>'intSRNNo',
										'intSTNNo'=>'intSTNNo',
										'intPurchaseInvoiceNo'=>'intPurchaseInvoiceNo',
										'intAdvancePaymentNo'=>'intAdvancePaymentNo',
										'intSupplierPaymentNo'=>'intSupplierPaymentNo',
										'intSupplierDebitNo'=>'intSupplierDebitNo',
										'SUPPLIER_CREDIT_NOTE_NO'=>'SUPPLIER_CREDIT_NOTE_NO',
										'intOtherBillInvNo'=>'intOtherBillInvNo',
										'intOtherBillPaymentNo'=>'intOtherBillPaymentNo',
										'GAIN_LOSS_NO'=>'GAIN_LOSS_NO',
										'intFin_BankPaymentNo'=>'intFin_BankPaymentNo',
										'intReconciliationNo'=>'intReconciliationNo',
										'intPaymentScheduleNo'=>'intPaymentScheduleNo',
										'BANK_TO_BANK_TRNS'=>'BANK_TO_BANK_TRNS',
										'intGeneralGatePassNo'=>'intGeneralGatePassNo',
										'GENERAL_GATEPASS_RETURN_NO'=>'GENERAL_GATEPASS_RETURN_NO',
										'intFixedAssetDepNo'=>'intFixedAssetDepNo',
										'intLoanScheduleNo'=>'intLoanScheduleNo',
										'intAdvanceSettleNo'=>'intAdvanceSettleNo',
										'intCustomerAdvSettleNo'=>'intCustomerAdvSettleNo',
										'SUP_INVOICE_SETTLEMENT'=>'SUP_INVOICE_SETTLEMENT',
										'MRN_COLOR_ROOM'=>'MRN_COLOR_ROOM',
										'GENERAL_ITEM_DISPOSE_NO'=>'GENERAL_ITEM_DISPOSE_NO',
										'SAMPLE_REQUISITION_NO'=>'SAMPLE_REQUISITION_NO',
										'SUB_CONTRACT_PO_NO'=>'SUB_CONTRACT_PO_NO',
										'SAMPLE_FABRICIN_NO'=>'SAMPLE_FABRICIN_NO',
										'CUSTMER_LC'=>'CUSTMER_LC',
										'BUDGET_TRANSFER_NO'=>'BUDGET_TRANSFER_NO',
										'PETTY_CASH_INVOICE_NO'=>'PETTY_CASH_INVOICE_NO',
										'PETTY_CASH_REQUISITION'=>'PETTY_CASH_REQUISITION',
										'SUB_CONTRACT_GATE_PASS_NO'=>'SUB_CONTRACT_GATE_PASS_NO',
										'SUB_CONTRACT_RETURN_NO'=>'SUB_CONTRACT_RETURN_NO',
										'OTHER_RECEIVABLE_INVOICE_NO'=>'OTHER_RECEIVABLE_INVOICE_NO',
										'OTHER_RECEIVABLE_RECEIPT_NO'=>'OTHER_RECEIVABLE_RECEIPT_NO',
										'SUPPLIER_ADVANCE_BANK_SETTLE_NO'=>'SUPPLIER_ADVANCE_BANK_SETTLE_NO',
										'PETTY_CASH_REIMBURSEMENT_NO'=>'PETTY_CASH_REIMBURSEMENT_NO',
										'EXCEED_SETTLE_NO'=>'EXCEED_SETTLE_NO',
										'ISSUE_TO_COLOR_ROOM'=>'ISSUE_TO_COLOR_ROOM',
										'COLOR_ISSUE_TO_PRODUCTION'=>'COLOR_ISSUE_TO_PRODUCTION',
										'COLOR_ROOM_REQUISITION_NO'=>'COLOR_ROOM_REQUISITION_NO',
										'COLOR_ROOM_WASTAGE_NO'=>'COLOR_ROOM_WASTAGE_NO',
										'ACTUAL_PRODUCTION'=>'ACTUAL_PRODUCTION',
										'C_ROOM_RETURN_TO_STORES_NO'=>'C_ROOM_RETURN_TO_STORES_NO',
										'PRODUCTION_RETURN_TO_COLOR_ROOM'=>'PRODUCTION_RETURN_TO_COLOR_ROOM',
										'COLOR_CREATION_NO'=>'COLOR_CREATION_NO',
										'ACTUAL_CONSUMPTION_OTHER_NO'=>'ACTUAL_CONSUMPTION_OTHER_NO',
										'CUSTOMER_DISPATCH_CONFIRM_NO'=>'CUSTOMER_DISPATCH_CONFIRM_NO',
										'ORDER_ALLOCATION_NO'=>'ORDER_ALLOCATION_NO',
										'OPEN_ORDER_NO'=>'OPEN_ORDER_NO',
										'CLOSE_ORDER_NO'=>'CLOSE_ORDER_NO',
										'OPEN_ORDER_REQ_NO'=>'OPEN_ORDER_REQ_NO',
										'DEBIT_RECEIVE_NO'=>'DEBIT_RECEIVE_NO',
										'TEMP_DISPATCH_NO'=>'TEMP_DISPATCH_NO',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit );
		return $result;
	}

	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data);
		return $this->db->getResult();


	}

	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}

	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();
	}

	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}

	public function get_field_array(){
		return $this->field_array;
	}

	function commit($type='update')
	{
		//commit update
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{
			$data[$v] = $this->$v;
		}
		$where		= "intCompanyId = ".$this->intCompanyId."" ;
		$this->commitArray = array();

		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}

	//BEGIN - public functions for private properties {
	
	//retun intCompanyId
	function getintCompanyId()
	{
		$this->validate();
		return $this->intCompanyId;
	}
	
	//retun START_SEQUENCE_NO
	function getSTART_SEQUENCE_NO()
	{
		$this->validate();
		return $this->START_SEQUENCE_NO;
	}
	
	//retun intSampleNo
	function getintSampleNo()
	{
		$this->validate();
		return $this->intSampleNo;
	}
	
	//retun intSampleDispatchNo
	function getintSampleDispatchNo()
	{
		$this->validate();
		return $this->intSampleDispatchNo;
	}
	
	//retun intPRNno
	function getintPRNno()
	{
		$this->validate();
		return $this->intPRNno;
	}
	
	//retun intOrderNo
	function getintOrderNo()
	{
		$this->validate();
		return $this->intOrderNo;
	}
	
	//retun intSampleOrderNo
	function getintSampleOrderNo()
	{
		$this->validate();
		return $this->intSampleOrderNo;
	}
	
	//retun intPONo
	function getintPONo()
	{
		$this->validate();
		return $this->intPONo;
	}
	
	//retun intGRNNo
	function getintGRNNo()
	{
		$this->validate();
		return $this->intGRNNo;
	}
	
	//retun intSupReturnNo
	function getintSupReturnNo()
	{
		$this->validate();
		return $this->intSupReturnNo;
	}
	
	//retun intMrnNo
	function getintMrnNo()
	{
		$this->validate();
		return $this->intMrnNo;
	}
	
	//retun intIssueNo
	function getintIssueNo()
	{
		$this->validate();
		return $this->intIssueNo;
	}
	
	//retun intReturnToStores
	function getintReturnToStores()
	{
		$this->validate();
		return $this->intReturnToStores;
	}
	
	//retun intGatePassNo
	function getintGatePassNo()
	{
		$this->validate();
		return $this->intGatePassNo;
	}
	
	//retun intGpTransfNo
	function getintGpTransfNo()
	{
		$this->validate();
		return $this->intGpTransfNo;
	}
	
	//retun intWareInvoiceNo
	function getintWareInvoiceNo()
	{
		$this->validate();
		return $this->intWareInvoiceNo;
	}
	
	//retun intWareItmAllocationNo
	function getintWareItmAllocationNo()
	{
		$this->validate();
		return $this->intWareItmAllocationNo;
	}
	
	//retun intFabricReceivedNo
	function getintFabricReceivedNo()
	{
		$this->validate();
		return $this->intFabricReceivedNo;
	}
	
	//retun intFabricTransferdNo
	function getintFabricTransferdNo()
	{
		$this->validate();
		return $this->intFabricTransferdNo;
	}
	
	//retun intFabricGatePassNo
	function getintFabricGatePassNo()
	{
		$this->validate();
		return $this->intFabricGatePassNo;
	}
	
	//retun intFabricGPTransfInNo
	function getintFabricGPTransfInNo()
	{
		$this->validate();
		return $this->intFabricGPTransfInNo;
	}
	
	//retun intProductionComplete
	function getintProductionComplete()
	{
		$this->validate();
		return $this->intProductionComplete;
	}
	
	//retun intFabricTransfeinNo
	function getintFabricTransfeinNo()
	{
		$this->validate();
		return $this->intFabricTransfeinNo;
	}
	
	//retun intFabricReturnNo
	function getintFabricReturnNo()
	{
		$this->validate();
		return $this->intFabricReturnNo;
	}
	
	//retun intFabricDamage
	function getintFabricDamage()
	{
		$this->validate();
		return $this->intFabricDamage;
	}
	
	//retun intFinishGoodsGatePass
	function getintFinishGoodsGatePass()
	{
		$this->validate();
		return $this->intFinishGoodsGatePass;
	}
	
	//retun intFinishGoodsGatePassIn
	function getintFinishGoodsGatePassIn()
	{
		$this->validate();
		return $this->intFinishGoodsGatePassIn;
	}
	
	//retun intBulkDispatchNo
	function getintBulkDispatchNo()
	{
		$this->validate();
		return $this->intBulkDispatchNo;
	}
	
	//retun intFabricCustReturnNo
	function getintFabricCustReturnNo()
	{
		$this->validate();
		return $this->intFabricCustReturnNo;
	}
	
	//retun intFabricCustReturnDispNo
	function getintFabricCustReturnDispNo()
	{
		$this->validate();
		return $this->intFabricCustReturnDispNo;
	}
	
	//retun intFin_SalesInvoiceNo
	function getintFin_SalesInvoiceNo()
	{
		$this->validate();
		return $this->intFin_SalesInvoiceNo;
	}
	
	//retun intFin_Customer_AdvanceNo
	function getintFin_Customer_AdvanceNo()
	{
		$this->validate();
		return $this->intFin_Customer_AdvanceNo;
	}
	
	//retun intFin_PaymentReceiveNo
	function getintFin_PaymentReceiveNo()
	{
		$this->validate();
		return $this->intFin_PaymentReceiveNo;
	}
	
	//retun intFin_CreditNoteNo
	function getintFin_CreditNoteNo()
	{
		$this->validate();
		return $this->intFin_CreditNoteNo;
	}
	
	//retun intFin_DebitNoteNo
	function getintFin_DebitNoteNo()
	{
		$this->validate();
		return $this->intFin_DebitNoteNo;
	}
	
	//retun JOURNAL_ENTRY
	function getJOURNAL_ENTRY()
	{
		$this->validate();
		return $this->JOURNAL_ENTRY;
	}
	
	//retun PAYMENT_SETTLEMENT_NO
	function getPAYMENT_SETTLEMENT_NO()
	{
		$this->validate();
		return $this->PAYMENT_SETTLEMENT_NO;
	}
	
	//retun BANK_PAYMENT_NO
	function getBANK_PAYMENT_NO()
	{
		$this->validate();
		return $this->BANK_PAYMENT_NO;
	}
	
	//retun intSRNNo
	function getintSRNNo()
	{
		$this->validate();
		return $this->intSRNNo;
	}
	
	//retun intSTNNo
	function getintSTNNo()
	{
		$this->validate();
		return $this->intSTNNo;
	}
	
	//retun intPurchaseInvoiceNo
	function getintPurchaseInvoiceNo()
	{
		$this->validate();
		return $this->intPurchaseInvoiceNo;
	}
	
	//retun intAdvancePaymentNo
	function getintAdvancePaymentNo()
	{
		$this->validate();
		return $this->intAdvancePaymentNo;
	}
	
	//retun intSupplierPaymentNo
	function getintSupplierPaymentNo()
	{
		$this->validate();
		return $this->intSupplierPaymentNo;
	}
	
	//retun intSupplierDebitNo
	function getintSupplierDebitNo()
	{
		$this->validate();
		return $this->intSupplierDebitNo;
	}
	
	//retun SUPPLIER_CREDIT_NOTE_NO
	function getSUPPLIER_CREDIT_NOTE_NO()
	{
		$this->validate();
		return $this->SUPPLIER_CREDIT_NOTE_NO;
	}
	
	//retun intOtherBillInvNo
	function getintOtherBillInvNo()
	{
		$this->validate();
		return $this->intOtherBillInvNo;
	}
	
	//retun intOtherBillPaymentNo
	function getintOtherBillPaymentNo()
	{
		$this->validate();
		return $this->intOtherBillPaymentNo;
	}
	
	//retun GAIN_LOSS_NO
	function getGAIN_LOSS_NO()
	{
		$this->validate();
		return $this->GAIN_LOSS_NO;
	}
	
	//retun intFin_BankPaymentNo
	function getintFin_BankPaymentNo()
	{
		$this->validate();
		return $this->intFin_BankPaymentNo;
	}
	
	//retun intReconciliationNo
	function getintReconciliationNo()
	{
		$this->validate();
		return $this->intReconciliationNo;
	}
	
	//retun intPaymentScheduleNo
	function getintPaymentScheduleNo()
	{
		$this->validate();
		return $this->intPaymentScheduleNo;
	}
	
	//retun BANK_TO_BANK_TRNS
	function getBANK_TO_BANK_TRNS()
	{
		$this->validate();
		return $this->BANK_TO_BANK_TRNS;
	}
	
	//retun intGeneralGatePassNo
	function getintGeneralGatePassNo()
	{
		$this->validate();
		return $this->intGeneralGatePassNo;
	}
	
	//retun GENERAL_GATEPASS_RETURN_NO
	function getGENERAL_GATEPASS_RETURN_NO()
	{
		$this->validate();
		return $this->GENERAL_GATEPASS_RETURN_NO;
	}
	
	//retun intFixedAssetDepNo
	function getintFixedAssetDepNo()
	{
		$this->validate();
		return $this->intFixedAssetDepNo;
	}
	
	//retun intLoanScheduleNo
	function getintLoanScheduleNo()
	{
		$this->validate();
		return $this->intLoanScheduleNo;
	}
	
	//retun intAdvanceSettleNo
	function getintAdvanceSettleNo()
	{
		$this->validate();
		return $this->intAdvanceSettleNo;
	}
	
	//retun intCustomerAdvSettleNo
	function getintCustomerAdvSettleNo()
	{
		$this->validate();
		return $this->intCustomerAdvSettleNo;
	}
	
	//retun SUP_INVOICE_SETTLEMENT
	function getSUP_INVOICE_SETTLEMENT()
	{
		$this->validate();
		return $this->SUP_INVOICE_SETTLEMENT;
	}
	
	//retun MRN_COLOR_ROOM
	function getMRN_COLOR_ROOM()
	{
		$this->validate();
		return $this->MRN_COLOR_ROOM;
	}
	
	//retun GENERAL_ITEM_DISPOSE_NO
	function getGENERAL_ITEM_DISPOSE_NO()
	{
		$this->validate();
		return $this->GENERAL_ITEM_DISPOSE_NO;
	}
	
	//retun SAMPLE_REQUISITION_NO
	function getSAMPLE_REQUISITION_NO()
	{
		$this->validate();
		return $this->SAMPLE_REQUISITION_NO;
	}
	
	//retun SUB_CONTRACT_PO_NO
	function getSUB_CONTRACT_PO_NO()
	{
		$this->validate();
		return $this->SUB_CONTRACT_PO_NO;
	}
	
	//retun SAMPLE_FABRICIN_NO
	function getSAMPLE_FABRICIN_NO()
	{
		$this->validate();
		return $this->SAMPLE_FABRICIN_NO;
	}
	
	//retun CUSTMER_LC
	function getCUSTMER_LC()
	{
		$this->validate();
		return $this->CUSTMER_LC;
	}
	
	//retun BUDGET_TRANSFER_NO
	function getBUDGET_TRANSFER_NO()
	{
		$this->validate();
		return $this->BUDGET_TRANSFER_NO;
	}
	
	//retun PETTY_CASH_INVOICE_NO
	function getPETTY_CASH_INVOICE_NO()
	{
		$this->validate();
		return $this->PETTY_CASH_INVOICE_NO;
	}
	
	//retun PETTY_CASH_REQUISITION
	function getPETTY_CASH_REQUISITION()
	{
		$this->validate();
		return $this->PETTY_CASH_REQUISITION;
	}
	
	//retun SUB_CONTRACT_GATE_PASS_NO
	function getSUB_CONTRACT_GATE_PASS_NO()
	{
		$this->validate();
		return $this->SUB_CONTRACT_GATE_PASS_NO;
	}
	
	//retun SUB_CONTRACT_RETURN_NO
	function getSUB_CONTRACT_RETURN_NO()
	{
		$this->validate();
		return $this->SUB_CONTRACT_RETURN_NO;
	}
	
	//retun OTHER_RECEIVABLE_INVOICE_NO
	function getOTHER_RECEIVABLE_INVOICE_NO()
	{
		$this->validate();
		return $this->OTHER_RECEIVABLE_INVOICE_NO;
	}
	
	//retun OTHER_RECEIVABLE_RECEIPT_NO
	function getOTHER_RECEIVABLE_RECEIPT_NO()
	{
		$this->validate();
		return $this->OTHER_RECEIVABLE_RECEIPT_NO;
	}
	
	//retun SUPPLIER_ADVANCE_BANK_SETTLE_NO
	function getSUPPLIER_ADVANCE_BANK_SETTLE_NO()
	{
		$this->validate();
		return $this->SUPPLIER_ADVANCE_BANK_SETTLE_NO;
	}
	
	//retun PETTY_CASH_REIMBURSEMENT_NO
	function getPETTY_CASH_REIMBURSEMENT_NO()
	{
		$this->validate();
		return $this->PETTY_CASH_REIMBURSEMENT_NO;
	}
	
	//retun EXCEED_SETTLE_NO
	function getEXCEED_SETTLE_NO()
	{
		$this->validate();
		return $this->EXCEED_SETTLE_NO;
	}
	
	//retun ISSUE_TO_COLOR_ROOM
	function getISSUE_TO_COLOR_ROOM()
	{
		$this->validate();
		return $this->ISSUE_TO_COLOR_ROOM;
	}
	
	//retun COLOR_ISSUE_TO_PRODUCTION
	function getCOLOR_ISSUE_TO_PRODUCTION()
	{
		$this->validate();
		return $this->COLOR_ISSUE_TO_PRODUCTION;
	}
	
	//retun COLOR_ROOM_REQUISITION_NO
	function getCOLOR_ROOM_REQUISITION_NO()
	{
		$this->validate();
		return $this->COLOR_ROOM_REQUISITION_NO;
	}
	
	//retun COLOR_ROOM_WASTAGE_NO
	function getCOLOR_ROOM_WASTAGE_NO()
	{
		$this->validate();
		return $this->COLOR_ROOM_WASTAGE_NO;
	}
	
	//retun ACTUAL_PRODUCTION
	function getACTUAL_PRODUCTION()
	{
		$this->validate();
		return $this->ACTUAL_PRODUCTION;
	}
	
	//retun C_ROOM_RETURN_TO_STORES_NO
	function getC_ROOM_RETURN_TO_STORES_NO()
	{
		$this->validate();
		return $this->C_ROOM_RETURN_TO_STORES_NO;
	}
	
	//retun PRODUCTION_RETURN_TO_COLOR_ROOM
	function getPRODUCTION_RETURN_TO_COLOR_ROOM()
	{
		$this->validate();
		return $this->PRODUCTION_RETURN_TO_COLOR_ROOM;
	}
	
	//retun COLOR_CREATION_NO
	function getCOLOR_CREATION_NO()
	{
		$this->validate();
		return $this->COLOR_CREATION_NO;
	}
	
	//retun ACTUAL_CONSUMPTION_OTHER_NO
	function getACTUAL_CONSUMPTION_OTHER_NO()
	{
		$this->validate();
		return $this->ACTUAL_CONSUMPTION_OTHER_NO;
	}
	
	//retun CUSTOMER_DISPATCH_CONFIRM_NO
	function getCUSTOMER_DISPATCH_CONFIRM_NO()
	{
		$this->validate();
		return $this->CUSTOMER_DISPATCH_CONFIRM_NO;
	}
	
	//retun ORDER_ALLOCATION_NO
	function getORDER_ALLOCATION_NO()
	{
		$this->validate();
		return $this->ORDER_ALLOCATION_NO;
	}
	
	//retun OPEN_ORDER_NO
	function getOPEN_ORDER_NO()
	{
		$this->validate();
		return $this->OPEN_ORDER_NO;
	}
	
	//retun CLOSE_ORDER_NO
	function getCLOSE_ORDER_NO()
	{
		$this->validate();
		return $this->CLOSE_ORDER_NO;
	}
	
	//retun OPEN_ORDER_REQ_NO
	function getOPEN_ORDER_REQ_NO()
	{
		$this->validate();
		return $this->OPEN_ORDER_REQ_NO;
	}
	
	//retun DEBIT_RECEIVE_NO
	function getDEBIT_RECEIVE_NO()
	{
		$this->validate();
		return $this->DEBIT_RECEIVE_NO;
	}
	
	function getTEMP_DISPATCH_NO()
	{
		$this->validate();
		return $this->TEMP_DISPATCH_NO;
	}
	
	//END }

	//BEGIN - public function set {
	
	//set intCompanyId
	function setintCompanyId($intCompanyId)
	{
		array_push($this->commitArray,'intCompanyId');
		$this->intCompanyId = $intCompanyId;
	}
	
	//set START_SEQUENCE_NO
	function setSTART_SEQUENCE_NO($START_SEQUENCE_NO)
	{
		array_push($this->commitArray,'START_SEQUENCE_NO');
		$this->START_SEQUENCE_NO = $START_SEQUENCE_NO;
	}
	
	//set intSampleNo
	function setintSampleNo($intSampleNo)
	{
		array_push($this->commitArray,'intSampleNo');
		$this->intSampleNo = $intSampleNo;
	}
	
	//set intSampleDispatchNo
	function setintSampleDispatchNo($intSampleDispatchNo)
	{
		array_push($this->commitArray,'intSampleDispatchNo');
		$this->intSampleDispatchNo = $intSampleDispatchNo;
	}
	
	//set intPRNno
	function setintPRNno($intPRNno)
	{
		array_push($this->commitArray,'intPRNno');
		$this->intPRNno = $intPRNno;
	}
	
	//set intOrderNo
	function setintOrderNo($intOrderNo)
	{
		array_push($this->commitArray,'intOrderNo');
		$this->intOrderNo = $intOrderNo;
	}
	
	//set intSampleOrderNo
	function setintSampleOrderNo($intSampleOrderNo)
	{
		array_push($this->commitArray,'intSampleOrderNo');
		$this->intSampleOrderNo = $intSampleOrderNo;
	}
	
	//set intPONo
	function setintPONo($intPONo)
	{
		array_push($this->commitArray,'intPONo');
		$this->intPONo = $intPONo;
	}
	
	//set intGRNNo
	function setintGRNNo($intGRNNo)
	{
		array_push($this->commitArray,'intGRNNo');
		$this->intGRNNo = $intGRNNo;
	}
	
	//set intSupReturnNo
	function setintSupReturnNo($intSupReturnNo)
	{
		array_push($this->commitArray,'intSupReturnNo');
		$this->intSupReturnNo = $intSupReturnNo;
	}
	
	//set intMrnNo
	function setintMrnNo($intMrnNo)
	{
		array_push($this->commitArray,'intMrnNo');
		$this->intMrnNo = $intMrnNo;
	}
	
	//set intIssueNo
	function setintIssueNo($intIssueNo)
	{
		array_push($this->commitArray,'intIssueNo');
		$this->intIssueNo = $intIssueNo;
	}
	
	//set intReturnToStores
	function setintReturnToStores($intReturnToStores)
	{
		array_push($this->commitArray,'intReturnToStores');
		$this->intReturnToStores = $intReturnToStores;
	}
	
	//set intGatePassNo
	function setintGatePassNo($intGatePassNo)
	{
		array_push($this->commitArray,'intGatePassNo');
		$this->intGatePassNo = $intGatePassNo;
	}
	
	//set intGpTransfNo
	function setintGpTransfNo($intGpTransfNo)
	{
		array_push($this->commitArray,'intGpTransfNo');
		$this->intGpTransfNo = $intGpTransfNo;
	}
	
	//set intWareInvoiceNo
	function setintWareInvoiceNo($intWareInvoiceNo)
	{
		array_push($this->commitArray,'intWareInvoiceNo');
		$this->intWareInvoiceNo = $intWareInvoiceNo;
	}
	
	//set intWareItmAllocationNo
	function setintWareItmAllocationNo($intWareItmAllocationNo)
	{
		array_push($this->commitArray,'intWareItmAllocationNo');
		$this->intWareItmAllocationNo = $intWareItmAllocationNo;
	}
	
	//set intFabricReceivedNo
	function setintFabricReceivedNo($intFabricReceivedNo)
	{
		array_push($this->commitArray,'intFabricReceivedNo');
		$this->intFabricReceivedNo = $intFabricReceivedNo;
	}
	
	//set intFabricTransferdNo
	function setintFabricTransferdNo($intFabricTransferdNo)
	{
		array_push($this->commitArray,'intFabricTransferdNo');
		$this->intFabricTransferdNo = $intFabricTransferdNo;
	}
	
	//set intFabricGatePassNo
	function setintFabricGatePassNo($intFabricGatePassNo)
	{
		array_push($this->commitArray,'intFabricGatePassNo');
		$this->intFabricGatePassNo = $intFabricGatePassNo;
	}
	
	//set intFabricGPTransfInNo
	function setintFabricGPTransfInNo($intFabricGPTransfInNo)
	{
		array_push($this->commitArray,'intFabricGPTransfInNo');
		$this->intFabricGPTransfInNo = $intFabricGPTransfInNo;
	}
	
	//set intProductionComplete
	function setintProductionComplete($intProductionComplete)
	{
		array_push($this->commitArray,'intProductionComplete');
		$this->intProductionComplete = $intProductionComplete;
	}
	
	//set intFabricTransfeinNo
	function setintFabricTransfeinNo($intFabricTransfeinNo)
	{
		array_push($this->commitArray,'intFabricTransfeinNo');
		$this->intFabricTransfeinNo = $intFabricTransfeinNo;
	}
	
	//set intFabricReturnNo
	function setintFabricReturnNo($intFabricReturnNo)
	{
		array_push($this->commitArray,'intFabricReturnNo');
		$this->intFabricReturnNo = $intFabricReturnNo;
	}
	
	//set intFabricDamage
	function setintFabricDamage($intFabricDamage)
	{
		array_push($this->commitArray,'intFabricDamage');
		$this->intFabricDamage = $intFabricDamage;
	}
	
	//set intFinishGoodsGatePass
	function setintFinishGoodsGatePass($intFinishGoodsGatePass)
	{
		array_push($this->commitArray,'intFinishGoodsGatePass');
		$this->intFinishGoodsGatePass = $intFinishGoodsGatePass;
	}
	
	//set intFinishGoodsGatePassIn
	function setintFinishGoodsGatePassIn($intFinishGoodsGatePassIn)
	{
		array_push($this->commitArray,'intFinishGoodsGatePassIn');
		$this->intFinishGoodsGatePassIn = $intFinishGoodsGatePassIn;
	}
	
	//set intBulkDispatchNo
	function setintBulkDispatchNo($intBulkDispatchNo)
	{
		array_push($this->commitArray,'intBulkDispatchNo');
		$this->intBulkDispatchNo = $intBulkDispatchNo;
	}
	
	//set intFabricCustReturnNo
	function setintFabricCustReturnNo($intFabricCustReturnNo)
	{
		array_push($this->commitArray,'intFabricCustReturnNo');
		$this->intFabricCustReturnNo = $intFabricCustReturnNo;
	}
	
	//set intFabricCustReturnDispNo
	function setintFabricCustReturnDispNo($intFabricCustReturnDispNo)
	{
		array_push($this->commitArray,'intFabricCustReturnDispNo');
		$this->intFabricCustReturnDispNo = $intFabricCustReturnDispNo;
	}
	
	//set intFin_SalesInvoiceNo
	function setintFin_SalesInvoiceNo($intFin_SalesInvoiceNo)
	{
		array_push($this->commitArray,'intFin_SalesInvoiceNo');
		$this->intFin_SalesInvoiceNo = $intFin_SalesInvoiceNo;
	}
	
	//set intFin_Customer_AdvanceNo
	function setintFin_Customer_AdvanceNo($intFin_Customer_AdvanceNo)
	{
		array_push($this->commitArray,'intFin_Customer_AdvanceNo');
		$this->intFin_Customer_AdvanceNo = $intFin_Customer_AdvanceNo;
	}
	
	//set intFin_PaymentReceiveNo
	function setintFin_PaymentReceiveNo($intFin_PaymentReceiveNo)
	{
		array_push($this->commitArray,'intFin_PaymentReceiveNo');
		$this->intFin_PaymentReceiveNo = $intFin_PaymentReceiveNo;
	}
	
	//set intFin_CreditNoteNo
	function setintFin_CreditNoteNo($intFin_CreditNoteNo)
	{
		array_push($this->commitArray,'intFin_CreditNoteNo');
		$this->intFin_CreditNoteNo = $intFin_CreditNoteNo;
	}
	
	//set intFin_DebitNoteNo
	function setintFin_DebitNoteNo($intFin_DebitNoteNo)
	{
		array_push($this->commitArray,'intFin_DebitNoteNo');
		$this->intFin_DebitNoteNo = $intFin_DebitNoteNo;
	}
	
	//set JOURNAL_ENTRY
	function setJOURNAL_ENTRY($JOURNAL_ENTRY)
	{
		array_push($this->commitArray,'JOURNAL_ENTRY');
		$this->JOURNAL_ENTRY = $JOURNAL_ENTRY;
	}
	
	//set PAYMENT_SETTLEMENT_NO
	function setPAYMENT_SETTLEMENT_NO($PAYMENT_SETTLEMENT_NO)
	{
		array_push($this->commitArray,'PAYMENT_SETTLEMENT_NO');
		$this->PAYMENT_SETTLEMENT_NO = $PAYMENT_SETTLEMENT_NO;
	}
	
	//set BANK_PAYMENT_NO
	function setBANK_PAYMENT_NO($BANK_PAYMENT_NO)
	{
		array_push($this->commitArray,'BANK_PAYMENT_NO');
		$this->BANK_PAYMENT_NO = $BANK_PAYMENT_NO;
	}
	
	//set intSRNNo
	function setintSRNNo($intSRNNo)
	{
		array_push($this->commitArray,'intSRNNo');
		$this->intSRNNo = $intSRNNo;
	}
	
	//set intSTNNo
	function setintSTNNo($intSTNNo)
	{
		array_push($this->commitArray,'intSTNNo');
		$this->intSTNNo = $intSTNNo;
	}
	
	//set intPurchaseInvoiceNo
	function setintPurchaseInvoiceNo($intPurchaseInvoiceNo)
	{
		array_push($this->commitArray,'intPurchaseInvoiceNo');
		$this->intPurchaseInvoiceNo = $intPurchaseInvoiceNo;
	}
	
	//set intAdvancePaymentNo
	function setintAdvancePaymentNo($intAdvancePaymentNo)
	{
		array_push($this->commitArray,'intAdvancePaymentNo');
		$this->intAdvancePaymentNo = $intAdvancePaymentNo;
	}
	
	//set intSupplierPaymentNo
	function setintSupplierPaymentNo($intSupplierPaymentNo)
	{
		array_push($this->commitArray,'intSupplierPaymentNo');
		$this->intSupplierPaymentNo = $intSupplierPaymentNo;
	}
	
	//set intSupplierDebitNo
	function setintSupplierDebitNo($intSupplierDebitNo)
	{
		array_push($this->commitArray,'intSupplierDebitNo');
		$this->intSupplierDebitNo = $intSupplierDebitNo;
	}
	
	//set SUPPLIER_CREDIT_NOTE_NO
	function setSUPPLIER_CREDIT_NOTE_NO($SUPPLIER_CREDIT_NOTE_NO)
	{
		array_push($this->commitArray,'SUPPLIER_CREDIT_NOTE_NO');
		$this->SUPPLIER_CREDIT_NOTE_NO = $SUPPLIER_CREDIT_NOTE_NO;
	}
	
	//set intOtherBillInvNo
	function setintOtherBillInvNo($intOtherBillInvNo)
	{
		array_push($this->commitArray,'intOtherBillInvNo');
		$this->intOtherBillInvNo = $intOtherBillInvNo;
	}
	
	//set intOtherBillPaymentNo
	function setintOtherBillPaymentNo($intOtherBillPaymentNo)
	{
		array_push($this->commitArray,'intOtherBillPaymentNo');
		$this->intOtherBillPaymentNo = $intOtherBillPaymentNo;
	}
	
	//set GAIN_LOSS_NO
	function setGAIN_LOSS_NO($GAIN_LOSS_NO)
	{
		array_push($this->commitArray,'GAIN_LOSS_NO');
		$this->GAIN_LOSS_NO = $GAIN_LOSS_NO;
	}
	
	//set intFin_BankPaymentNo
	function setintFin_BankPaymentNo($intFin_BankPaymentNo)
	{
		array_push($this->commitArray,'intFin_BankPaymentNo');
		$this->intFin_BankPaymentNo = $intFin_BankPaymentNo;
	}
	
	//set intReconciliationNo
	function setintReconciliationNo($intReconciliationNo)
	{
		array_push($this->commitArray,'intReconciliationNo');
		$this->intReconciliationNo = $intReconciliationNo;
	}
	
	//set intPaymentScheduleNo
	function setintPaymentScheduleNo($intPaymentScheduleNo)
	{
		array_push($this->commitArray,'intPaymentScheduleNo');
		$this->intPaymentScheduleNo = $intPaymentScheduleNo;
	}
	
	//set BANK_TO_BANK_TRNS
	function setBANK_TO_BANK_TRNS($BANK_TO_BANK_TRNS)
	{
		array_push($this->commitArray,'BANK_TO_BANK_TRNS');
		$this->BANK_TO_BANK_TRNS = $BANK_TO_BANK_TRNS;
	}
	
	//set intGeneralGatePassNo
	function setintGeneralGatePassNo($intGeneralGatePassNo)
	{
		array_push($this->commitArray,'intGeneralGatePassNo');
		$this->intGeneralGatePassNo = $intGeneralGatePassNo;
	}
	
	//set GENERAL_GATEPASS_RETURN_NO
	function setGENERAL_GATEPASS_RETURN_NO($GENERAL_GATEPASS_RETURN_NO)
	{
		array_push($this->commitArray,'GENERAL_GATEPASS_RETURN_NO');
		$this->GENERAL_GATEPASS_RETURN_NO = $GENERAL_GATEPASS_RETURN_NO;
	}
	
	//set intFixedAssetDepNo
	function setintFixedAssetDepNo($intFixedAssetDepNo)
	{
		array_push($this->commitArray,'intFixedAssetDepNo');
		$this->intFixedAssetDepNo = $intFixedAssetDepNo;
	}
	
	//set intLoanScheduleNo
	function setintLoanScheduleNo($intLoanScheduleNo)
	{
		array_push($this->commitArray,'intLoanScheduleNo');
		$this->intLoanScheduleNo = $intLoanScheduleNo;
	}
	
	//set intAdvanceSettleNo
	function setintAdvanceSettleNo($intAdvanceSettleNo)
	{
		array_push($this->commitArray,'intAdvanceSettleNo');
		$this->intAdvanceSettleNo = $intAdvanceSettleNo;
	}
	
	//set intCustomerAdvSettleNo
	function setintCustomerAdvSettleNo($intCustomerAdvSettleNo)
	{
		array_push($this->commitArray,'intCustomerAdvSettleNo');
		$this->intCustomerAdvSettleNo = $intCustomerAdvSettleNo;
	}
	
	//set SUP_INVOICE_SETTLEMENT
	function setSUP_INVOICE_SETTLEMENT($SUP_INVOICE_SETTLEMENT)
	{
		array_push($this->commitArray,'SUP_INVOICE_SETTLEMENT');
		$this->SUP_INVOICE_SETTLEMENT = $SUP_INVOICE_SETTLEMENT;
	}
	
	//set MRN_COLOR_ROOM
	function setMRN_COLOR_ROOM($MRN_COLOR_ROOM)
	{
		array_push($this->commitArray,'MRN_COLOR_ROOM');
		$this->MRN_COLOR_ROOM = $MRN_COLOR_ROOM;
	}
	
	//set GENERAL_ITEM_DISPOSE_NO
	function setGENERAL_ITEM_DISPOSE_NO($GENERAL_ITEM_DISPOSE_NO)
	{
		array_push($this->commitArray,'GENERAL_ITEM_DISPOSE_NO');
		$this->GENERAL_ITEM_DISPOSE_NO = $GENERAL_ITEM_DISPOSE_NO;
	}
	
	//set SAMPLE_REQUISITION_NO
	function setSAMPLE_REQUISITION_NO($SAMPLE_REQUISITION_NO)
	{
		array_push($this->commitArray,'SAMPLE_REQUISITION_NO');
		$this->SAMPLE_REQUISITION_NO = $SAMPLE_REQUISITION_NO;
	}
	
	//set SUB_CONTRACT_PO_NO
	function setSUB_CONTRACT_PO_NO($SUB_CONTRACT_PO_NO)
	{
		array_push($this->commitArray,'SUB_CONTRACT_PO_NO');
		$this->SUB_CONTRACT_PO_NO = $SUB_CONTRACT_PO_NO;
	}
	
	//set SAMPLE_FABRICIN_NO
	function setSAMPLE_FABRICIN_NO($SAMPLE_FABRICIN_NO)
	{
		array_push($this->commitArray,'SAMPLE_FABRICIN_NO');
		$this->SAMPLE_FABRICIN_NO = $SAMPLE_FABRICIN_NO;
	}
	
	//set CUSTMER_LC
	function setCUSTMER_LC($CUSTMER_LC)
	{
		array_push($this->commitArray,'CUSTMER_LC');
		$this->CUSTMER_LC = $CUSTMER_LC;
	}
	
	//set BUDGET_TRANSFER_NO
	function setBUDGET_TRANSFER_NO($BUDGET_TRANSFER_NO)
	{
		array_push($this->commitArray,'BUDGET_TRANSFER_NO');
		$this->BUDGET_TRANSFER_NO = $BUDGET_TRANSFER_NO;
	}
	
	//set PETTY_CASH_INVOICE_NO
	function setPETTY_CASH_INVOICE_NO($PETTY_CASH_INVOICE_NO)
	{
		array_push($this->commitArray,'PETTY_CASH_INVOICE_NO');
		$this->PETTY_CASH_INVOICE_NO = $PETTY_CASH_INVOICE_NO;
	}
	
	//set PETTY_CASH_REQUISITION
	function setPETTY_CASH_REQUISITION($PETTY_CASH_REQUISITION)
	{
		array_push($this->commitArray,'PETTY_CASH_REQUISITION');
		$this->PETTY_CASH_REQUISITION = $PETTY_CASH_REQUISITION;
	}
	
	//set SUB_CONTRACT_GATE_PASS_NO
	function setSUB_CONTRACT_GATE_PASS_NO($SUB_CONTRACT_GATE_PASS_NO)
	{
		array_push($this->commitArray,'SUB_CONTRACT_GATE_PASS_NO');
		$this->SUB_CONTRACT_GATE_PASS_NO = $SUB_CONTRACT_GATE_PASS_NO;
	}
	
	//set SUB_CONTRACT_RETURN_NO
	function setSUB_CONTRACT_RETURN_NO($SUB_CONTRACT_RETURN_NO)
	{
		array_push($this->commitArray,'SUB_CONTRACT_RETURN_NO');
		$this->SUB_CONTRACT_RETURN_NO = $SUB_CONTRACT_RETURN_NO;
	}
	
	//set OTHER_RECEIVABLE_INVOICE_NO
	function setOTHER_RECEIVABLE_INVOICE_NO($OTHER_RECEIVABLE_INVOICE_NO)
	{
		array_push($this->commitArray,'OTHER_RECEIVABLE_INVOICE_NO');
		$this->OTHER_RECEIVABLE_INVOICE_NO = $OTHER_RECEIVABLE_INVOICE_NO;
	}
	
	//set OTHER_RECEIVABLE_RECEIPT_NO
	function setOTHER_RECEIVABLE_RECEIPT_NO($OTHER_RECEIVABLE_RECEIPT_NO)
	{
		array_push($this->commitArray,'OTHER_RECEIVABLE_RECEIPT_NO');
		$this->OTHER_RECEIVABLE_RECEIPT_NO = $OTHER_RECEIVABLE_RECEIPT_NO;
	}
	
	//set SUPPLIER_ADVANCE_BANK_SETTLE_NO
	function setSUPPLIER_ADVANCE_BANK_SETTLE_NO($SUPPLIER_ADVANCE_BANK_SETTLE_NO)
	{
		array_push($this->commitArray,'SUPPLIER_ADVANCE_BANK_SETTLE_NO');
		$this->SUPPLIER_ADVANCE_BANK_SETTLE_NO = $SUPPLIER_ADVANCE_BANK_SETTLE_NO;
	}
	
	//set PETTY_CASH_REIMBURSEMENT_NO
	function setPETTY_CASH_REIMBURSEMENT_NO($PETTY_CASH_REIMBURSEMENT_NO)
	{
		array_push($this->commitArray,'PETTY_CASH_REIMBURSEMENT_NO');
		$this->PETTY_CASH_REIMBURSEMENT_NO = $PETTY_CASH_REIMBURSEMENT_NO;
	}
	
	//set EXCEED_SETTLE_NO
	function setEXCEED_SETTLE_NO($EXCEED_SETTLE_NO)
	{
		array_push($this->commitArray,'EXCEED_SETTLE_NO');
		$this->EXCEED_SETTLE_NO = $EXCEED_SETTLE_NO;
	}
	
	//set ISSUE_TO_COLOR_ROOM
	function setISSUE_TO_COLOR_ROOM($ISSUE_TO_COLOR_ROOM)
	{
		array_push($this->commitArray,'ISSUE_TO_COLOR_ROOM');
		$this->ISSUE_TO_COLOR_ROOM = $ISSUE_TO_COLOR_ROOM;
	}
	
	//set COLOR_ISSUE_TO_PRODUCTION
	function setCOLOR_ISSUE_TO_PRODUCTION($COLOR_ISSUE_TO_PRODUCTION)
	{
		array_push($this->commitArray,'COLOR_ISSUE_TO_PRODUCTION');
		$this->COLOR_ISSUE_TO_PRODUCTION = $COLOR_ISSUE_TO_PRODUCTION;
	}
	
	//set COLOR_ROOM_REQUISITION_NO
	function setCOLOR_ROOM_REQUISITION_NO($COLOR_ROOM_REQUISITION_NO)
	{
		array_push($this->commitArray,'COLOR_ROOM_REQUISITION_NO');
		$this->COLOR_ROOM_REQUISITION_NO = $COLOR_ROOM_REQUISITION_NO;
	}
	
	//set COLOR_ROOM_WASTAGE_NO
	function setCOLOR_ROOM_WASTAGE_NO($COLOR_ROOM_WASTAGE_NO)
	{
		array_push($this->commitArray,'COLOR_ROOM_WASTAGE_NO');
		$this->COLOR_ROOM_WASTAGE_NO = $COLOR_ROOM_WASTAGE_NO;
	}
	
	//set ACTUAL_PRODUCTION
	function setACTUAL_PRODUCTION($ACTUAL_PRODUCTION)
	{
		array_push($this->commitArray,'ACTUAL_PRODUCTION');
		$this->ACTUAL_PRODUCTION = $ACTUAL_PRODUCTION;
	}
	
	//set C_ROOM_RETURN_TO_STORES_NO
	function setC_ROOM_RETURN_TO_STORES_NO($C_ROOM_RETURN_TO_STORES_NO)
	{
		array_push($this->commitArray,'C_ROOM_RETURN_TO_STORES_NO');
		$this->C_ROOM_RETURN_TO_STORES_NO = $C_ROOM_RETURN_TO_STORES_NO;
	}
	
	//set PRODUCTION_RETURN_TO_COLOR_ROOM
	function setPRODUCTION_RETURN_TO_COLOR_ROOM($PRODUCTION_RETURN_TO_COLOR_ROOM)
	{
		array_push($this->commitArray,'PRODUCTION_RETURN_TO_COLOR_ROOM');
		$this->PRODUCTION_RETURN_TO_COLOR_ROOM = $PRODUCTION_RETURN_TO_COLOR_ROOM;
	}
	
	//set COLOR_CREATION_NO
	function setCOLOR_CREATION_NO($COLOR_CREATION_NO)
	{
		array_push($this->commitArray,'COLOR_CREATION_NO');
		$this->COLOR_CREATION_NO = $COLOR_CREATION_NO;
	}
	
	//set ACTUAL_CONSUMPTION_OTHER_NO
	function setACTUAL_CONSUMPTION_OTHER_NO($ACTUAL_CONSUMPTION_OTHER_NO)
	{
		array_push($this->commitArray,'ACTUAL_CONSUMPTION_OTHER_NO');
		$this->ACTUAL_CONSUMPTION_OTHER_NO = $ACTUAL_CONSUMPTION_OTHER_NO;
	}
	
	//set CUSTOMER_DISPATCH_CONFIRM_NO
	function setCUSTOMER_DISPATCH_CONFIRM_NO($CUSTOMER_DISPATCH_CONFIRM_NO)
	{
		array_push($this->commitArray,'CUSTOMER_DISPATCH_CONFIRM_NO');
		$this->CUSTOMER_DISPATCH_CONFIRM_NO = $CUSTOMER_DISPATCH_CONFIRM_NO;
	}
	
	//set ORDER_ALLOCATION_NO
	function setORDER_ALLOCATION_NO($ORDER_ALLOCATION_NO)
	{
		array_push($this->commitArray,'ORDER_ALLOCATION_NO');
		$this->ORDER_ALLOCATION_NO = $ORDER_ALLOCATION_NO;
	}
	
	//set OPEN_ORDER_NO
	function setOPEN_ORDER_NO($OPEN_ORDER_NO)
	{
		array_push($this->commitArray,'OPEN_ORDER_NO');
		$this->OPEN_ORDER_NO = $OPEN_ORDER_NO;
	}
	
	//set CLOSE_ORDER_NO
	function setCLOSE_ORDER_NO($CLOSE_ORDER_NO)
	{
		array_push($this->commitArray,'CLOSE_ORDER_NO');
		$this->CLOSE_ORDER_NO = $CLOSE_ORDER_NO;
	}
	
	//set OPEN_ORDER_REQ_NO
	function setOPEN_ORDER_REQ_NO($OPEN_ORDER_REQ_NO)
	{
		array_push($this->commitArray,'OPEN_ORDER_REQ_NO');
		$this->OPEN_ORDER_REQ_NO = $OPEN_ORDER_REQ_NO;
	}
	
	//set DEBIT_RECEIVE_NO
	function setDEBIT_RECEIVE_NO($DEBIT_RECEIVE_NO)
	{
		array_push($this->commitArray,'DEBIT_RECEIVE_NO');
		$this->DEBIT_RECEIVE_NO = $DEBIT_RECEIVE_NO;
	}
	
	function setTEMP_DISPATCH_NO($TEMP_DISPATCH_NO)
	{
		array_push($this->commitArray,'TEMP_DISPATCH_NO');
		$this->TEMP_DISPATCH_NO = $TEMP_DISPATCH_NO;
	}
	
	//END }

	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->intCompanyId=='')
			throw new exception("Set primary values first");
		else
			return true;
	}

	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}

	public function set($intCompanyId)
	{
		$cols	= "*";

		$join	= NULL;

		$where	= "intCompanyId='$intCompanyId'";

		$result = $this->select($cols,$join,$where);
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}

	//insert as parameters
	public function insertRec($intCompanyId,$START_SEQUENCE_NO,$intSampleNo,$intSampleDispatchNo,$intPRNno,$intOrderNo,$intSampleOrderNo,$intPONo,$intGRNNo,$intSupReturnNo,$intMrnNo,$intIssueNo,$intReturnToStores,$intGatePassNo,$intGpTransfNo,$intWareInvoiceNo,$intWareItmAllocationNo,$intFabricReceivedNo,$intFabricTransferdNo,$intFabricGatePassNo,$intFabricGPTransfInNo,$intProductionComplete,$intFabricTransfeinNo,$intFabricReturnNo,$intFabricDamage,$intFinishGoodsGatePass,$intFinishGoodsGatePassIn,$intBulkDispatchNo,$intFabricCustReturnNo,$intFabricCustReturnDispNo,$intFin_SalesInvoiceNo,$intFin_Customer_AdvanceNo,$intFin_PaymentReceiveNo,$intFin_CreditNoteNo,$intFin_DebitNoteNo,$JOURNAL_ENTRY,$PAYMENT_SETTLEMENT_NO,$BANK_PAYMENT_NO,$intSRNNo,$intSTNNo,$intPurchaseInvoiceNo,$intAdvancePaymentNo,$intSupplierPaymentNo,$intSupplierDebitNo,$SUPPLIER_CREDIT_NOTE_NO,$intOtherBillInvNo,$intOtherBillPaymentNo,$GAIN_LOSS_NO,$intFin_BankPaymentNo,$intReconciliationNo,$intPaymentScheduleNo,$BANK_TO_BANK_TRNS,$intGeneralGatePassNo,$GENERAL_GATEPASS_RETURN_NO,$intFixedAssetDepNo,$intLoanScheduleNo,$intAdvanceSettleNo,$intCustomerAdvSettleNo,$SUP_INVOICE_SETTLEMENT,$MRN_COLOR_ROOM,$GENERAL_ITEM_DISPOSE_NO,$SAMPLE_REQUISITION_NO,$SUB_CONTRACT_PO_NO,$SAMPLE_FABRICIN_NO,$CUSTMER_LC,$BUDGET_TRANSFER_NO,$PETTY_CASH_INVOICE_NO,$PETTY_CASH_REQUISITION,$SUB_CONTRACT_GATE_PASS_NO,$SUB_CONTRACT_RETURN_NO,$OTHER_RECEIVABLE_INVOICE_NO,$OTHER_RECEIVABLE_RECEIPT_NO,$SUPPLIER_ADVANCE_BANK_SETTLE_NO,$PETTY_CASH_REIMBURSEMENT_NO,$EXCEED_SETTLE_NO,$ISSUE_TO_COLOR_ROOM,$COLOR_ISSUE_TO_PRODUCTION,$COLOR_ROOM_REQUISITION_NO,$COLOR_ROOM_WASTAGE_NO,$ACTUAL_PRODUCTION,$C_ROOM_RETURN_TO_STORES_NO,$PRODUCTION_RETURN_TO_COLOR_ROOM,$COLOR_CREATION_NO,$ACTUAL_CONSUMPTION_OTHER_NO,$CUSTOMER_DISPATCH_CONFIRM_NO,$ORDER_ALLOCATION_NO,$OPEN_ORDER_NO,$CLOSE_ORDER_NO,$OPEN_ORDER_REQ_NO,$DEBIT_RECEIVE_NO,$TEMP_DISPATCH_NO){
		$data = array('intCompanyId'=>$intCompanyId 
				,'START_SEQUENCE_NO'=>$START_SEQUENCE_NO 
				,'intSampleNo'=>$intSampleNo 
				,'intSampleDispatchNo'=>$intSampleDispatchNo 
				,'intPRNno'=>$intPRNno 
				,'intOrderNo'=>$intOrderNo 
				,'intSampleOrderNo'=>$intSampleOrderNo 
				,'intPONo'=>$intPONo 
				,'intGRNNo'=>$intGRNNo 
				,'intSupReturnNo'=>$intSupReturnNo 
				,'intMrnNo'=>$intMrnNo 
				,'intIssueNo'=>$intIssueNo 
				,'intReturnToStores'=>$intReturnToStores 
				,'intGatePassNo'=>$intGatePassNo 
				,'intGpTransfNo'=>$intGpTransfNo 
				,'intWareInvoiceNo'=>$intWareInvoiceNo 
				,'intWareItmAllocationNo'=>$intWareItmAllocationNo 
				,'intFabricReceivedNo'=>$intFabricReceivedNo 
				,'intFabricTransferdNo'=>$intFabricTransferdNo 
				,'intFabricGatePassNo'=>$intFabricGatePassNo 
				,'intFabricGPTransfInNo'=>$intFabricGPTransfInNo 
				,'intProductionComplete'=>$intProductionComplete 
				,'intFabricTransfeinNo'=>$intFabricTransfeinNo 
				,'intFabricReturnNo'=>$intFabricReturnNo 
				,'intFabricDamage'=>$intFabricDamage 
				,'intFinishGoodsGatePass'=>$intFinishGoodsGatePass 
				,'intFinishGoodsGatePassIn'=>$intFinishGoodsGatePassIn 
				,'intBulkDispatchNo'=>$intBulkDispatchNo 
				,'intFabricCustReturnNo'=>$intFabricCustReturnNo 
				,'intFabricCustReturnDispNo'=>$intFabricCustReturnDispNo 
				,'intFin_SalesInvoiceNo'=>$intFin_SalesInvoiceNo 
				,'intFin_Customer_AdvanceNo'=>$intFin_Customer_AdvanceNo 
				,'intFin_PaymentReceiveNo'=>$intFin_PaymentReceiveNo 
				,'intFin_CreditNoteNo'=>$intFin_CreditNoteNo 
				,'intFin_DebitNoteNo'=>$intFin_DebitNoteNo 
				,'JOURNAL_ENTRY'=>$JOURNAL_ENTRY 
				,'PAYMENT_SETTLEMENT_NO'=>$PAYMENT_SETTLEMENT_NO 
				,'BANK_PAYMENT_NO'=>$BANK_PAYMENT_NO 
				,'intSRNNo'=>$intSRNNo 
				,'intSTNNo'=>$intSTNNo 
				,'intPurchaseInvoiceNo'=>$intPurchaseInvoiceNo 
				,'intAdvancePaymentNo'=>$intAdvancePaymentNo 
				,'intSupplierPaymentNo'=>$intSupplierPaymentNo 
				,'intSupplierDebitNo'=>$intSupplierDebitNo 
				,'SUPPLIER_CREDIT_NOTE_NO'=>$SUPPLIER_CREDIT_NOTE_NO 
				,'intOtherBillInvNo'=>$intOtherBillInvNo 
				,'intOtherBillPaymentNo'=>$intOtherBillPaymentNo 
				,'GAIN_LOSS_NO'=>$GAIN_LOSS_NO 
				,'intFin_BankPaymentNo'=>$intFin_BankPaymentNo 
				,'intReconciliationNo'=>$intReconciliationNo 
				,'intPaymentScheduleNo'=>$intPaymentScheduleNo 
				,'BANK_TO_BANK_TRNS'=>$BANK_TO_BANK_TRNS 
				,'intGeneralGatePassNo'=>$intGeneralGatePassNo 
				,'GENERAL_GATEPASS_RETURN_NO'=>$GENERAL_GATEPASS_RETURN_NO 
				,'intFixedAssetDepNo'=>$intFixedAssetDepNo 
				,'intLoanScheduleNo'=>$intLoanScheduleNo 
				,'intAdvanceSettleNo'=>$intAdvanceSettleNo 
				,'intCustomerAdvSettleNo'=>$intCustomerAdvSettleNo 
				,'SUP_INVOICE_SETTLEMENT'=>$SUP_INVOICE_SETTLEMENT 
				,'MRN_COLOR_ROOM'=>$MRN_COLOR_ROOM 
				,'GENERAL_ITEM_DISPOSE_NO'=>$GENERAL_ITEM_DISPOSE_NO 
				,'SAMPLE_REQUISITION_NO'=>$SAMPLE_REQUISITION_NO 
				,'SUB_CONTRACT_PO_NO'=>$SUB_CONTRACT_PO_NO 
				,'SAMPLE_FABRICIN_NO'=>$SAMPLE_FABRICIN_NO 
				,'CUSTMER_LC'=>$CUSTMER_LC 
				,'BUDGET_TRANSFER_NO'=>$BUDGET_TRANSFER_NO 
				,'PETTY_CASH_INVOICE_NO'=>$PETTY_CASH_INVOICE_NO 
				,'PETTY_CASH_REQUISITION'=>$PETTY_CASH_REQUISITION 
				,'SUB_CONTRACT_GATE_PASS_NO'=>$SUB_CONTRACT_GATE_PASS_NO 
				,'SUB_CONTRACT_RETURN_NO'=>$SUB_CONTRACT_RETURN_NO 
				,'OTHER_RECEIVABLE_INVOICE_NO'=>$OTHER_RECEIVABLE_INVOICE_NO 
				,'OTHER_RECEIVABLE_RECEIPT_NO'=>$OTHER_RECEIVABLE_RECEIPT_NO 
				,'SUPPLIER_ADVANCE_BANK_SETTLE_NO'=>$SUPPLIER_ADVANCE_BANK_SETTLE_NO 
				,'PETTY_CASH_REIMBURSEMENT_NO'=>$PETTY_CASH_REIMBURSEMENT_NO 
				,'EXCEED_SETTLE_NO'=>$EXCEED_SETTLE_NO 
				,'ISSUE_TO_COLOR_ROOM'=>$ISSUE_TO_COLOR_ROOM 
				,'COLOR_ISSUE_TO_PRODUCTION'=>$COLOR_ISSUE_TO_PRODUCTION 
				,'COLOR_ROOM_REQUISITION_NO'=>$COLOR_ROOM_REQUISITION_NO 
				,'COLOR_ROOM_WASTAGE_NO'=>$COLOR_ROOM_WASTAGE_NO 
				,'ACTUAL_PRODUCTION'=>$ACTUAL_PRODUCTION 
				,'C_ROOM_RETURN_TO_STORES_NO'=>$C_ROOM_RETURN_TO_STORES_NO 
				,'PRODUCTION_RETURN_TO_COLOR_ROOM'=>$PRODUCTION_RETURN_TO_COLOR_ROOM 
				,'COLOR_CREATION_NO'=>$COLOR_CREATION_NO 
				,'ACTUAL_CONSUMPTION_OTHER_NO'=>$ACTUAL_CONSUMPTION_OTHER_NO 
				,'CUSTOMER_DISPATCH_CONFIRM_NO'=>$CUSTOMER_DISPATCH_CONFIRM_NO 
				,'ORDER_ALLOCATION_NO'=>$ORDER_ALLOCATION_NO 
				,'OPEN_ORDER_NO'=>$OPEN_ORDER_NO 
				,'CLOSE_ORDER_NO'=>$CLOSE_ORDER_NO 
				,'OPEN_ORDER_REQ_NO'=>$OPEN_ORDER_REQ_NO 
				,'DEBIT_RECEIVE_NO'=>$DEBIT_RECEIVE_NO 
				,'TEMP_DISPATCH_NO'=>$TEMP_DISPATCH_NO 
				);
		return $this->insert($data);
	}

	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('intCompanyId,START_SEQUENCE_NO',  null, $where = $where);
		$html = '<option value=""></option>';
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intCompanyId'])
				$html .= '<option selected="selected" value="'.$row['intCompanyId'].'">'.$row['START_SEQUENCE_NO'].'</option>';
			else
				$html .= '<option value="'.$row['intCompanyId'].'">'.$row['START_SEQUENCE_NO'].'</option>';
		}
		return $html;
	}
	
	public function getSerialNoAndUpdateSysNo($fieldName,$locationId)
	{
 	
		$this->set($locationId);
		
		$fName_get	= 'get'.$fieldName;
		$maxNo		= $this->$fName_get();
		
		$data  		= array(''.$fieldName.'' => '+1');
		$where		= " intCompanyId='".$locationId."' ";
		
		$resultArr	= $this->upgrade($data,$where);
		
		if($maxNo=='' || $maxNo==0)
			throw new Exception("System No not initialized.");
		else if(!$resultArr['status'])
			throw new Exception("System No update error.");
		else
			return $maxNo;
	}
	
	public function validateDuplicateSerialNoWithSysNo($serialNo,$fieldName,$location)
	{
		$this->set($location);
		
		$fName_get	= 'get'.$fieldName;
		$fName_set	= 'set'.$fieldName;
		$maxNo		= $this->$fName_get();
		
		if($serialNo>=$maxNo)
		{
			$this->$fName_set($serialNo+1);
			$result_arr	= $this->commit();
			
			if(!$result_arr['status'])
				throw new Exception($result_arr['msg']);
			else
				return true;
		}
	}
	public function insertLocationwiseSystemNos($locationId)
	{
		$cols	= "*";
		$where	= "intCompanyId='$locationId' ";
		
		$result = $this->select($cols,$join=null,$where);
		$count	= mysqli_num_rows($result);	
		if($count==0)
		{
			$sql  	= "SHOW COLUMNS FROM sys_no ";
			$result	= $this->db->RunQuery($sql);
			
			$this->set($locationId);
			foreach($result as $row)
			{
				$setVar	= 'set'.$row['Field'];
				
				if($row['Field']=='intCompanyId')
					$this->$setVar($locationId);
				else
					$this->$setVar($locationId.'000000');
				
			}
			$result_arr = $this->commit('insert');
			if(!$result_arr['status'])
				throw new Exception($result_arr['msg']);
			else
				return true;
		}	
	}
	//END }
}
?>
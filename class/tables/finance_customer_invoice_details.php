<?php
class finance_customer_invoice_details{
 
	private $db;
	private $table= "finance_customer_invoice_details";
	
	//private property
	private $SERIAL_NO;
	private $SERIAL_YEAR;
	private $SALES_ORDER_ID;
	private $GL_ACCOUNT;
	private $TAX_CODE;
	private $COST_CENTER;
	private $PRODUCTION_DAMAGE_QTY;
	private $FABRIC_DAMAGE_QTY;
	private $SAMPLE_QTY;
	private $MISSING_PANAL;
	private $OTHER;
	private $QTY;
	private $PRICE;
	private $VALUE;
	private $TAX_VALUE;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('SERIAL_NO'=>'SERIAL_NO',
										'SERIAL_YEAR'=>'SERIAL_YEAR',
										'SALES_ORDER_ID'=>'SALES_ORDER_ID',
										'GL_ACCOUNT'=>'GL_ACCOUNT',
										'TAX_CODE'=>'TAX_CODE',
										'COST_CENTER'=>'COST_CENTER',
										'PRODUCTION_DAMAGE_QTY'=>'PRODUCTION_DAMAGE_QTY',
										'FABRIC_DAMAGE_QTY'=>'FABRIC_DAMAGE_QTY',
										'SAMPLE_QTY'=>'SAMPLE_QTY',
										'MISSING_PANAL'=>'MISSING_PANAL',
										'OTHER'=>'OTHER',
										'QTY'=>'QTY',
										'PRICE'=>'PRICE',
										'VALUE'=>'VALUE',
										'TAX_VALUE'=>'TAX_VALUE',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "SERIAL_NO = ".$this->SERIAL_NO." and SERIAL_YEAR = ".$this->SERIAL_YEAR." and SALES_ORDER_ID = ".$this->SALES_ORDER_ID."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun SERIAL_NO
	function getSERIAL_NO()
	{
		$this->validate();
		return $this->SERIAL_NO;
	}
	
	//retun SERIAL_YEAR
	function getSERIAL_YEAR()
	{
		$this->validate();
		return $this->SERIAL_YEAR;
	}
	
	//retun SALES_ORDER_ID
	function getSALES_ORDER_ID()
	{
		$this->validate();
		return $this->SALES_ORDER_ID;
	}
	
	//retun GL_ACCOUNT
	function getGL_ACCOUNT()
	{
		$this->validate();
		return $this->GL_ACCOUNT;
	}
	
	//retun TAX_CODE
	function getTAX_CODE()
	{
		$this->validate();
		return $this->TAX_CODE;
	}
	
	//retun COST_CENTER
	function getCOST_CENTER()
	{
		$this->validate();
		return $this->COST_CENTER;
	}
	
	//retun PRODUCTION_DAMAGE_QTY
	function getPRODUCTION_DAMAGE_QTY()
	{
		$this->validate();
		return $this->PRODUCTION_DAMAGE_QTY;
	}
	
	//retun FABRIC_DAMAGE_QTY
	function getFABRIC_DAMAGE_QTY()
	{
		$this->validate();
		return $this->FABRIC_DAMAGE_QTY;
	}
	
	//retun SAMPLE_QTY
	function getSAMPLE_QTY()
	{
		$this->validate();
		return $this->SAMPLE_QTY;
	}
	
	//retun MISSING_PANAL
	function getMISSING_PANAL()
	{
		$this->validate();
		return $this->MISSING_PANAL;
	}
	
	//retun OTHER
	function getOTHER()
	{
		$this->validate();
		return $this->OTHER;
	}
	
	//retun QTY
	function getQTY()
	{
		$this->validate();
		return $this->QTY;
	}
	
	//retun PRICE
	function getPRICE()
	{
		$this->validate();
		return $this->PRICE;
	}
	
	//retun VALUE
	function getVALUE()
	{
		$this->validate();
		return $this->VALUE;
	}
	
	//retun TAX_VALUE
	function getTAX_VALUE()
	{
		$this->validate();
		return $this->TAX_VALUE;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set SERIAL_NO
	function setSERIAL_NO($SERIAL_NO)
	{
		array_push($this->commitArray,'SERIAL_NO');
		$this->SERIAL_NO = $SERIAL_NO;
	}
	
	//set SERIAL_YEAR
	function setSERIAL_YEAR($SERIAL_YEAR)
	{
		array_push($this->commitArray,'SERIAL_YEAR');
		$this->SERIAL_YEAR = $SERIAL_YEAR;
	}
	
	//set SALES_ORDER_ID
	function setSALES_ORDER_ID($SALES_ORDER_ID)
	{
		array_push($this->commitArray,'SALES_ORDER_ID');
		$this->SALES_ORDER_ID = $SALES_ORDER_ID;
	}
	
	//set GL_ACCOUNT
	function setGL_ACCOUNT($GL_ACCOUNT)
	{
		array_push($this->commitArray,'GL_ACCOUNT');
		$this->GL_ACCOUNT = $GL_ACCOUNT;
	}
	
	//set TAX_CODE
	function setTAX_CODE($TAX_CODE)
	{
		array_push($this->commitArray,'TAX_CODE');
		$this->TAX_CODE = $TAX_CODE;
	}
	
	//set COST_CENTER
	function setCOST_CENTER($COST_CENTER)
	{
		array_push($this->commitArray,'COST_CENTER');
		$this->COST_CENTER = $COST_CENTER;
	}
	
	//set PRODUCTION_DAMAGE_QTY
	function setPRODUCTION_DAMAGE_QTY($PRODUCTION_DAMAGE_QTY)
	{
		array_push($this->commitArray,'PRODUCTION_DAMAGE_QTY');
		$this->PRODUCTION_DAMAGE_QTY = $PRODUCTION_DAMAGE_QTY;
	}
	
	//set FABRIC_DAMAGE_QTY
	function setFABRIC_DAMAGE_QTY($FABRIC_DAMAGE_QTY)
	{
		array_push($this->commitArray,'FABRIC_DAMAGE_QTY');
		$this->FABRIC_DAMAGE_QTY = $FABRIC_DAMAGE_QTY;
	}
	
	//set SAMPLE_QTY
	function setSAMPLE_QTY($SAMPLE_QTY)
	{
		array_push($this->commitArray,'SAMPLE_QTY');
		$this->SAMPLE_QTY = $SAMPLE_QTY;
	}
	
	//set MISSING_PANAL
	function setMISSING_PANAL($MISSING_PANAL)
	{
		array_push($this->commitArray,'MISSING_PANAL');
		$this->MISSING_PANAL = $MISSING_PANAL;
	}
	
	//set OTHER
	function setOTHER($OTHER)
	{
		array_push($this->commitArray,'OTHER');
		$this->OTHER = $OTHER;
	}
	
	//set QTY
	function setQTY($QTY)
	{
		array_push($this->commitArray,'QTY');
		$this->QTY = $QTY;
	}
	
	//set PRICE
	function setPRICE($PRICE)
	{
		array_push($this->commitArray,'PRICE');
		$this->PRICE = $PRICE;
	}
	
	//set VALUE
	function setVALUE($VALUE)
	{
		array_push($this->commitArray,'VALUE');
		$this->VALUE = $VALUE;
	}
	
	//set TAX_VALUE
	function setTAX_VALUE($TAX_VALUE)
	{
		array_push($this->commitArray,'TAX_VALUE');
		$this->TAX_VALUE = $TAX_VALUE;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->SERIAL_NO=='' || $this->SERIAL_YEAR=='' || $this->SALES_ORDER_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($SERIAL_NO , $SERIAL_YEAR , $SALES_ORDER_ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "SERIAL_NO='$SERIAL_NO' and SERIAL_YEAR='$SERIAL_YEAR' and SALES_ORDER_ID='$SALES_ORDER_ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($SERIAL_NO,$SERIAL_YEAR,$SALES_ORDER_ID,$GL_ACCOUNT,$TAX_CODE,$COST_CENTER,$PRODUCTION_DAMAGE_QTY,$FABRIC_DAMAGE_QTY,$SAMPLE_QTY,$MISSING_PANAL,$OTHER,$QTY,$PRICE,$VALUE,$TAX_VALUE){
		$data = array('SERIAL_NO'=>$SERIAL_NO 
				,'SERIAL_YEAR'=>$SERIAL_YEAR 
				,'SALES_ORDER_ID'=>$SALES_ORDER_ID 
				,'GL_ACCOUNT'=>$GL_ACCOUNT 
				,'TAX_CODE'=>$TAX_CODE 
				,'COST_CENTER'=>$COST_CENTER 
				,'PRODUCTION_DAMAGE_QTY'=>$PRODUCTION_DAMAGE_QTY 
				,'FABRIC_DAMAGE_QTY'=>$FABRIC_DAMAGE_QTY 
				,'SAMPLE_QTY'=>$SAMPLE_QTY 
				,'MISSING_PANAL'=>$MISSING_PANAL 
				,'OTHER'=>$OTHER 
				,'QTY'=>$QTY 
				,'PRICE'=>$PRICE 
				,'VALUE'=>$VALUE 
				,'TAX_VALUE'=>$TAX_VALUE 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('SERIAL_NO,SERIAL_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['SERIAL_NO'])
				$html .= '<option selected="selected" value="'.$row['SERIAL_NO'].'">'.$row['SERIAL_YEAR'].'</option>';	
			else
				$html .= '<option value="'.$row['SERIAL_NO'].'">'.$row['SERIAL_YEAR'].'</option>';	
		}
		return $html;
	}
	
	//END }
	
	public function getInvoiceAmount($orderNo,$orderYear,$salesOrderId,$deci)
	{
		if($salesOrderId!='')
			$para	= "AND finance_customer_invoice_details.SALES_ORDER_ID = $salesOrderId";
			
		$cols		= " ROUND(COALESCE(SUM(finance_customer_invoice_details.QTY),0),$deci) AS INVOICE_QTY,
  						ROUND(COALESCE(SUM(finance_customer_invoice_details.VALUE),0),$deci) AS INVOICE_AMOUNT";
		
		$join		= " INNER JOIN finance_customer_invoice_header FCIH
						ON finance_customer_invoice_details.SERIAL_NO = FCIH.SERIAL_NO
						  AND finance_customer_invoice_details.SERIAL_YEAR = FCIH.SERIAL_YEAR";
		
		$where		= " FCIH.STATUS = 1
						AND FCIH.ORDER_NO = $orderNo
						AND FCIH.ORDER_YEAR = $orderYear";
					
		$result		= $this->select($cols,$join,$where,$order=null,$limit=null);
		return mysqli_fetch_array($result);
	}
}
?>
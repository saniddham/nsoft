<?php
class finance_supplier_advance_balance_settlement_details{
 
	private $db;
	private $table= "finance_supplier_advance_balance_settlement_details";
	
	//private property
	private $SETTLE_NO;
	private $SETTLE_YEAR;
	private $ADVANCE_NO;
	private $ADVANCE_YEAR;
	private $PO_NO;
	private $PO_YEAR;
	private $SUPPLIER_TRANSACTION_ID;
	private $BANK_CHART_OF_ACCOUNT_ID;
	private $AMOUNT;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('SETTLE_NO'=>'SETTLE_NO',
										'SETTLE_YEAR'=>'SETTLE_YEAR',
										'ADVANCE_NO'=>'ADVANCE_NO',
										'ADVANCE_YEAR'=>'ADVANCE_YEAR',
										'PO_NO'=>'PO_NO',
										'PO_YEAR'=>'PO_YEAR',
										'SUPPLIER_TRANSACTION_ID'=>'SUPPLIER_TRANSACTION_ID',
										'BANK_CHART_OF_ACCOUNT_ID'=>'BANK_CHART_OF_ACCOUNT_ID',
										'AMOUNT'=>'AMOUNT',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "SETTLE_NO = ".$this->SETTLE_NO." and SETTLE_YEAR = ".$this->SETTLE_YEAR." and ADVANCE_NO = ".$this->ADVANCE_NO." and ADVANCE_YEAR = ".$this->ADVANCE_YEAR."" ;
		$this->commitArray = array();
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun SETTLE_NO
	function getSETTLE_NO()
	{
		$this->validate();
		return $this->SETTLE_NO;
	}
	
	//retun SETTLE_YEAR
	function getSETTLE_YEAR()
	{
		$this->validate();
		return $this->SETTLE_YEAR;
	}
	
	//retun ADVANCE_NO
	function getADVANCE_NO()
	{
		$this->validate();
		return $this->ADVANCE_NO;
	}
	
	//retun ADVANCE_YEAR
	function getADVANCE_YEAR()
	{
		$this->validate();
		return $this->ADVANCE_YEAR;
	}
	
	//retun PO_NO
	function getPO_NO()
	{
		$this->validate();
		return $this->PO_NO;
	}
	
	//retun PO_YEAR
	function getPO_YEAR()
	{
		$this->validate();
		return $this->PO_YEAR;
	}
	
	//retun SUPPLIER_TRANSACTION_ID
	function getSUPPLIER_TRANSACTION_ID()
	{
		$this->validate();
		return $this->SUPPLIER_TRANSACTION_ID;
	}
	
	//retun BANK_CHART_OF_ACCOUNT_ID
	function getBANK_CHART_OF_ACCOUNT_ID()
	{
		$this->validate();
		return $this->BANK_CHART_OF_ACCOUNT_ID;
	}
	
	//retun AMOUNT
	function getAMOUNT()
	{
		$this->validate();
		return $this->AMOUNT;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set SETTLE_NO
	function setSETTLE_NO($SETTLE_NO)
	{
		array_push($this->commitArray,'SETTLE_NO');
		$this->SETTLE_NO = $SETTLE_NO;
	}
	
	//set SETTLE_YEAR
	function setSETTLE_YEAR($SETTLE_YEAR)
	{
		array_push($this->commitArray,'SETTLE_YEAR');
		$this->SETTLE_YEAR = $SETTLE_YEAR;
	}
	
	//set ADVANCE_NO
	function setADVANCE_NO($ADVANCE_NO)
	{
		array_push($this->commitArray,'ADVANCE_NO');
		$this->ADVANCE_NO = $ADVANCE_NO;
	}
	
	//set ADVANCE_YEAR
	function setADVANCE_YEAR($ADVANCE_YEAR)
	{
		array_push($this->commitArray,'ADVANCE_YEAR');
		$this->ADVANCE_YEAR = $ADVANCE_YEAR;
	}
	
	//set PO_NO
	function setPO_NO($PO_NO)
	{
		array_push($this->commitArray,'PO_NO');
		$this->PO_NO = $PO_NO;
	}
	
	//set PO_YEAR
	function setPO_YEAR($PO_YEAR)
	{
		array_push($this->commitArray,'PO_YEAR');
		$this->PO_YEAR = $PO_YEAR;
	}
	
	//set SUPPLIER_TRANSACTION_ID
	function setSUPPLIER_TRANSACTION_ID($SUPPLIER_TRANSACTION_ID)
	{
		array_push($this->commitArray,'SUPPLIER_TRANSACTION_ID');
		$this->SUPPLIER_TRANSACTION_ID = $SUPPLIER_TRANSACTION_ID;
	}
	
	//set BANK_CHART_OF_ACCOUNT_ID
	function setBANK_CHART_OF_ACCOUNT_ID($BANK_CHART_OF_ACCOUNT_ID)
	{
		array_push($this->commitArray,'BANK_CHART_OF_ACCOUNT_ID');
		$this->BANK_CHART_OF_ACCOUNT_ID = $BANK_CHART_OF_ACCOUNT_ID;
	}
	
	//set AMOUNT
	function setAMOUNT($AMOUNT)
	{
		array_push($this->commitArray,'AMOUNT');
		$this->AMOUNT = $AMOUNT;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->SETTLE_NO=='' || $this->SETTLE_YEAR=='' || $this->ADVANCE_NO=='' || $this->ADVANCE_YEAR=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($SETTLE_NO , $SETTLE_YEAR , $ADVANCE_NO , $ADVANCE_YEAR)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "SETTLE_NO='$SETTLE_NO' and SETTLE_YEAR='$SETTLE_YEAR' and ADVANCE_NO='$ADVANCE_NO' and ADVANCE_YEAR='$ADVANCE_YEAR'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($SETTLE_NO,$SETTLE_YEAR,$ADVANCE_NO,$ADVANCE_YEAR,$PO_NO,$PO_YEAR,$SUPPLIER_TRANSACTION_ID,$BANK_CHART_OF_ACCOUNT_ID,$AMOUNT){
		$data = array('SETTLE_NO'=>$SETTLE_NO 
				,'SETTLE_YEAR'=>$SETTLE_YEAR 
				,'ADVANCE_NO'=>$ADVANCE_NO 
				,'ADVANCE_YEAR'=>$ADVANCE_YEAR 
				,'PO_NO'=>$PO_NO 
				,'PO_YEAR'=>$PO_YEAR 
				,'SUPPLIER_TRANSACTION_ID'=>$SUPPLIER_TRANSACTION_ID 
				,'BANK_CHART_OF_ACCOUNT_ID'=>$BANK_CHART_OF_ACCOUNT_ID 
				,'AMOUNT'=>$AMOUNT 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('SETTLE_NO,SETTLE_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['SETTLE_NO'])
				$html .= '<option selected="selected" value="'.$row['SETTLE_NO'].'">'.$row['SETTLE_YEAR'].'</option>';	
			else
				$html .= '<option value="'.$row['SETTLE_NO'].'">'.$row['SETTLE_YEAR'].'</option>';	
		}
		return $html;
	}
	
	public function getDetailData($settleNo,$settleYear)
	{
		$cols	= " ADVANCE_NO,
					ADVANCE_YEAR,
					PO_NO,
					PO_YEAR,
					BANK_CHART_OF_ACCOUNT_ID,
					AMOUNT ";
		
		$where	= " SETTLE_NO = '$settleNo' AND
					SETTLE_YEAR = '$settleYear' ";
		
		return $this->select($cols,$join=null,$where);
	}
	//END }
}
?>
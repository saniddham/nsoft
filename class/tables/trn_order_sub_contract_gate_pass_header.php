
<?php

class trn_order_sub_contract_gate_pass_header{
 
	private $db;
	private $table= "trn_order_sub_contract_gate_pass_header";
	
	function __construct($db)
	{
		$this->db = $db;
		$this->db->field_array_set(array('SUB_CONTRACT_GP_NO'=>'SUB_CONTRACT_GP_NO',
										'SUB_CONTRACT_GP_YEAR'=>'SUB_CONTRACT_GP_YEAR',
										'SUB_CONTRACT_NO'=>'SUB_CONTRACT_NO',
										'SUB_CONTRACT_YEAR'=>'SUB_CONTRACT_YEAR',
										'COMPANY_TO_TYPE'=>'COMPANY_TO_TYPE',
										'COMPANY_TO_ID'=>'COMPANY_TO_ID',
										'LOCATION_TO_ID'=>'LOCATION_TO_ID',
										'SUB_CONTRACTOR_ID'=>'SUB_CONTRACTOR_ID',
										'REMARKS'=>'REMARKS',
										'STATUS'=>'STATUS',
										'LEVELS'=>'LEVELS',
										'DATE'=>'DATE',
										'CREATED_DATE'=>'CREATED_DATE',
										'CREATED_BY'=>'CREATED_BY',
										'MODIFIED_DATE'=>'MODIFIED_DATE',
										'MODIFIED_BY'=>'MODIFIED_BY',
										'LOCATION_ID'=>'LOCATION_ID',
										));
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		return $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		//return $this->db->getResult();
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  
		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	//get dispatch header status
	public function getStatus($serial_no,$serial_year)
	{
		$result = 	$this->select('STATUS',null,"SUB_CONTRACT_GP_NO=$serial_no 
																			and SUB_CONTRACT_GP_YEAR=$serial_year");
		$row	=	mysqli_fetch_array($result);
		$status =  	$row[0];
		if($status =='')
			throw new Exception("Gate Pass note not found.");	
		else
			return $status;
	}
	
	
	//get approve levels 
	function getApproveLevels($serial_no,$serial_year)
	{
		$result = 	$this->select('LEVELS',null,"SUB_CONTRACT_GP_NO=$serial_no 
																			and SUB_CONTRACT_GP_YEAR=$serial_year");
		$row	=	mysqli_fetch_array($result);
		$status =  	$row[0];
		if($status =='')
			throw new Exception("Gate Pass note not found.");	
		else
			return $status;	
	}
	
}
?>

<?php
class mst_budget_category_list{
 
	private $db;
	private $table= "mst_budget_category_list";
	
	//private property
	private $SERIAL_ID;
	private $COMPANY_ID;
	private $LOCATION_ID;
	private $SUB_CATEGORY_ID;
	private $ITEM_ID;
	private $BUDGET_TYPE;
	private $commitArray = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->db->field_array_set(array('SERIAL_ID'=>'SERIAL_ID',
										'COMPANY_ID'=>'COMPANY_ID',
										'LOCATION_ID'=>'LOCATION_ID',
										'SUB_CATEGORY_ID'=>'SUB_CATEGORY_ID',
										'ITEM_ID'=>'ITEM_ID',
										'BUDGET_TYPE'=>'BUDGET_TYPE',
										));
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function commit()
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = ''.$this->$v.'';
		}
		$where		= "SERIAL_ID = '".$this->SERIAL_ID."'" ;
		unset($this->commitArray);
		return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun SERIAL_ID
	function getSERIAL_ID()
	{
		$this->validate();
		return $this->SERIAL_ID;
	}
	
	//retun COMPANY_ID
	function getCOMPANY_ID()
	{
		$this->validate();
		return $this->COMPANY_ID;
	}
	
	//retun LOCATION_ID
	function getLOCATION_ID()
	{
		$this->validate();
		return $this->LOCATION_ID;
	}
	
	//retun SUB_CATEGORY_ID
	function getSUB_CATEGORY_ID()
	{
		$this->validate();
		return $this->SUB_CATEGORY_ID;
	}
	
	//retun ITEM_ID
	function getITEM_ID()
	{
		$this->validate();
		return $this->ITEM_ID;
	}
	
	//retun BUDGET_TYPE
	function getBUDGET_TYPE()
	{
		$this->validate();
		return $this->BUDGET_TYPE;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set SERIAL_ID
	function setSERIAL_ID($SERIAL_ID)
	{
		array_push($this->commitArray,'SERIAL_ID');
		$this->SERIAL_ID = SERIAL_ID;
	}
	
	//set COMPANY_ID
	function setCOMPANY_ID($COMPANY_ID)
	{
		array_push($this->commitArray,'COMPANY_ID');
		$this->COMPANY_ID = COMPANY_ID;
	}
	
	//set LOCATION_ID
	function setLOCATION_ID($LOCATION_ID)
	{
		array_push($this->commitArray,'LOCATION_ID');
		$this->LOCATION_ID = LOCATION_ID;
	}
	
	//set SUB_CATEGORY_ID
	function setSUB_CATEGORY_ID($SUB_CATEGORY_ID)
	{
		array_push($this->commitArray,'SUB_CATEGORY_ID');
		$this->SUB_CATEGORY_ID = SUB_CATEGORY_ID;
	}
	
	//set ITEM_ID
	function setITEM_ID($ITEM_ID)
	{
		array_push($this->commitArray,'ITEM_ID');
		$this->ITEM_ID = ITEM_ID;
	}
	
	//set BUDGET_TYPE
	function setBUDGET_TYPE($BUDGET_TYPE)
	{
		array_push($this->commitArray,'BUDGET_TYPE');
		$this->BUDGET_TYPE = $BUDGET_TYPE;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->SERIAL_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
		}
	}
	
	public function set($COMPANY_ID,$LOCATION_ID,$SUB_CATEGORY_ID,$ITEM_ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "COMPANY_ID='$COMPANY_ID' and LOCATION_ID='$LOCATION_ID' and SUB_CATEGORY_ID='$SUB_CATEGORY_ID' and ITEM_ID='$ITEM_ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			$this->setVariables($result);
		else
			throw new Exception("Record not found.");
	}	
	
	
	//END }
}
?>
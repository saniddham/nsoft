
<?php

class sys_auto_email_report_types{
 
	private $db;
	private $table= "sys_auto_email_report_types";
	
	//private property
	private $ID;
	private $NAME;
	private $STATUS;
	private $ACTIVE_COMPANIES;
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->db->field_array_set(array('ID'=>'ID',
										'NAME'=>'NAME',
										'STATUS'=>'STATUS',
										'ACTIVE_COMPANIES'=>'ACTIVE_COMPANIES',
										));
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  
		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	//public functions for private properties
	
	//retun ID
	function getID()
	{
		$this->validate();
		return $this->ID;
	}
	
	//retun NAME
	function getNAME()
	{
		$this->validate();
		return $this->NAME;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//retun ACTIVE_COMPANIES
	function getACTIVE_COMPANIES()
	{
		$this->validate();
		return $this->ACTIVE_COMPANIES;
	}
	
	//validate primary values
	private function validate()
	{
		if($this->ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
		}
	}
	
	public function set($ID)
	{
		$result = $this->select('*',null,"ID='$ID'");	
		if($this->db->numRows()>0)
			$this->setVariables($result);
		else
			throw new Exception("Record not found.");
			
	}
	
	
}
?>

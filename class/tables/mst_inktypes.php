<?php
class mst_inktypes{
 
	private $db;
	private $table= "mst_inktypes";
	
	//private property
	private $intId;
	private $intTechniqueId;
	private $intType;
	private $strCode;
	private $strName;
	private $strRemark;
	private $intStatus;
	private $intCreator;
	private $dtmCreateDate;
	private $intModifyer;
	private $dtmModifyDate;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('intId'=>'intId',
										'intTechniqueId'=>'intTechniqueId',
										'intType'=>'intType',
										'strCode'=>'strCode',
										'strName'=>'strName',
										'strRemark'=>'strRemark',
										'intStatus'=>'intStatus',
										'intCreator'=>'intCreator',
										'dtmCreateDate'=>'dtmCreateDate',
										'intModifyer'=>'intModifyer',
										'dtmModifyDate'=>'dtmModifyDate',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "intId = ".$this->intId."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun intId
	function getintId()
	{
		$this->validate();
		return $this->intId;
	}
	
	//retun intTechniqueId
	function getintTechniqueId()
	{
		$this->validate();
		return $this->intTechniqueId;
	}
	
	//retun intType
	function getintType()
	{
		$this->validate();
		return $this->intType;
	}
	
	//retun strCode
	function getstrCode()
	{
		$this->validate();
		return $this->strCode;
	}
	
	//retun strName
	function getstrName()
	{
		$this->validate();
		return $this->strName;
	}
	
	//retun strRemark
	function getstrRemark()
	{
		$this->validate();
		return $this->strRemark;
	}
	
	//retun intStatus
	function getintStatus()
	{
		$this->validate();
		return $this->intStatus;
	}
	
	//retun intCreator
	function getintCreator()
	{
		$this->validate();
		return $this->intCreator;
	}
	
	//retun dtmCreateDate
	function getdtmCreateDate()
	{
		$this->validate();
		return $this->dtmCreateDate;
	}
	
	//retun intModifyer
	function getintModifyer()
	{
		$this->validate();
		return $this->intModifyer;
	}
	
	//retun dtmModifyDate
	function getdtmModifyDate()
	{
		$this->validate();
		return $this->dtmModifyDate;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set intId
	function setintId($intId)
	{
		array_push($this->commitArray,'intId');
		$this->intId = $intId;
	}
	
	//set intTechniqueId
	function setintTechniqueId($intTechniqueId)
	{
		array_push($this->commitArray,'intTechniqueId');
		$this->intTechniqueId = $intTechniqueId;
	}
	
	//set intType
	function setintType($intType)
	{
		array_push($this->commitArray,'intType');
		$this->intType = $intType;
	}
	
	//set strCode
	function setstrCode($strCode)
	{
		array_push($this->commitArray,'strCode');
		$this->strCode = $strCode;
	}
	
	//set strName
	function setstrName($strName)
	{
		array_push($this->commitArray,'strName');
		$this->strName = $strName;
	}
	
	//set strRemark
	function setstrRemark($strRemark)
	{
		array_push($this->commitArray,'strRemark');
		$this->strRemark = $strRemark;
	}
	
	//set intStatus
	function setintStatus($intStatus)
	{
		array_push($this->commitArray,'intStatus');
		$this->intStatus = $intStatus;
	}
	
	//set intCreator
	function setintCreator($intCreator)
	{
		array_push($this->commitArray,'intCreator');
		$this->intCreator = $intCreator;
	}
	
	//set dtmCreateDate
	function setdtmCreateDate($dtmCreateDate)
	{
		array_push($this->commitArray,'dtmCreateDate');
		$this->dtmCreateDate = $dtmCreateDate;
	}
	
	//set intModifyer
	function setintModifyer($intModifyer)
	{
		array_push($this->commitArray,'intModifyer');
		$this->intModifyer = $intModifyer;
	}
	
	//set dtmModifyDate
	function setdtmModifyDate($dtmModifyDate)
	{
		array_push($this->commitArray,'dtmModifyDate');
		$this->dtmModifyDate = $dtmModifyDate;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->intId=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($intId)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "intId='$intId'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($intId,$intTechniqueId,$intType,$strCode,$strName,$strRemark,$intStatus,$intCreator,$dtmCreateDate,$intModifyer,$dtmModifyDate){
		$data = array('intId'=>$intId 
				,'intTechniqueId'=>$intTechniqueId 
				,'intType'=>$intType 
				,'strCode'=>$strCode 
				,'strName'=>$strName 
				,'strRemark'=>$strRemark 
				,'intStatus'=>$intStatus 
				,'intCreator'=>$intCreator 
				,'dtmCreateDate'=>$dtmCreateDate 
				,'intModifyer'=>$intModifyer 
				,'dtmModifyDate'=>$dtmModifyDate 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('intId,intTechniqueId',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intId'])
				$html .= '<option selected="selected" value="'.$row['intId'].'">'.$row['intTechniqueId'].'</option>';	
			else
				$html .= '<option value="'.$row['intId'].'">'.$row['intTechniqueId'].'</option>';	
		}
		return $html;
	}
	
	//END }
}
?>
<?php
//Manually Functions Added 

#BEGIN 	- INCLUDE CLASSES {
include_once (MAIN_ROOT."class/tables/trn_sample_color_recipes.php"); 						$trn_sample_color_recipes						= new trn_sample_color_recipes($db);
include_once (MAIN_ROOT."class/tables/ware_stocktransactions_color_room_color.php"); 		$ware_stocktransactions_color_room_color		= new ware_stocktransactions_color_room_color($db);
include_once (MAIN_ROOT."class/tables/ink_color_room_wastage_header.php"); 		$ink_color_room_wastage_header		= new ink_color_room_wastage_header($db);
include_once (MAIN_ROOT."class/tables/mst_units.php"); 										$mst_units										= new mst_units($db);
include_once MAIN_ROOT."class/sessions.php";					$sessions				= new sessions($db);


#END	- }

class ware_stocktransactions_color_room_item{
 
	private $db;
	private $table= "ware_stocktransactions_color_room_item";
	
	//private property
	private $id;
	private $intSavedOrder;
	private $intCompanyId;
	private $intLocationId;
	private $intSubStores;
	private $intDocumentNo;
	private $intDocumntYear;
	private $intGRNNo;
	private $intGRNYear;
	private $dtGRNDate;
	private $intCurrencyId;
	private $dblGRNRate;
	private $intItemId;
	private $dblQty;
	private $strType;
	private $STOCK_CATEGORY;
	private $intUser;
	private $dtDate;
	private $intOrderNo;
	private $intOrderYear;
	private $intSalesOrderId;
	private $intColorId;
	private $intTechnique;
	private $intInkType;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('id'=>'id',
										'intSavedOrder'=>'intSavedOrder',
										'intCompanyId'=>'intCompanyId',
										'intLocationId'=>'intLocationId',
										'intSubStores'=>'intSubStores',
										'intDocumentNo'=>'intDocumentNo',
										'intDocumntYear'=>'intDocumntYear',
										'intGRNNo'=>'intGRNNo',
										'intGRNYear'=>'intGRNYear',
										'dtGRNDate'=>'dtGRNDate',
										'intCurrencyId'=>'intCurrencyId',
										'dblGRNRate'=>'dblGRNRate',
										'intItemId'=>'intItemId',
										'dblQty'=>'dblQty',
										'strType'=>'strType',
										'STOCK_CATEGORY'=>'STOCK_CATEGORY',
										'intUser'=>'intUser',
										'dtDate'=>'dtDate',
										'intOrderNo'=>'intOrderNo',
										'intOrderYear'=>'intOrderYear',
										'intSalesOrderId'=>'intSalesOrderId',
										'intColorId'=>'intColorId',
										'intTechnique'=>'intTechnique',
										'intInkType'=>'intInkType',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	function commit()
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "id = ".$this->id."" ;
		unset($this->commitArray);
		
		if($this->intId==NULL || $this->intId=='' || $this->intId=='NULL')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun id
	function getid()
	{
		$this->validate();
		return $this->id;
	}
	
	//retun intSavedOrder
	function getintSavedOrder()
	{
		$this->validate();
		return $this->intSavedOrder;
	}
	
	//retun intCompanyId
	function getintCompanyId()
	{
		$this->validate();
		return $this->intCompanyId;
	}
	
	//retun intLocationId
	function getintLocationId()
	{
		$this->validate();
		return $this->intLocationId;
	}
	
	//retun intSubStores
	function getintSubStores()
	{
		$this->validate();
		return $this->intSubStores;
	}
	
	//retun intDocumentNo
	function getintDocumentNo()
	{
		$this->validate();
		return $this->intDocumentNo;
	}
	
	//retun intDocumntYear
	function getintDocumntYear()
	{
		$this->validate();
		return $this->intDocumntYear;
	}
	
	//retun intGRNNo
	function getintGRNNo()
	{
		$this->validate();
		return $this->intGRNNo;
	}
	
	//retun intGRNYear
	function getintGRNYear()
	{
		$this->validate();
		return $this->intGRNYear;
	}
	
	//retun dtGRNDate
	function getdtGRNDate()
	{
		$this->validate();
		return $this->dtGRNDate;
	}
	
	//retun intCurrencyId
	function getintCurrencyId()
	{
		$this->validate();
		return $this->intCurrencyId;
	}
	
	//retun dblGRNRate
	function getdblGRNRate()
	{
		$this->validate();
		return $this->dblGRNRate;
	}
	
	//retun intItemId
	function getintItemId()
	{
		$this->validate();
		return $this->intItemId;
	}
	
	//retun dblQty
	function getdblQty()
	{
		$this->validate();
		return $this->dblQty;
	}
	
	//retun strType
	function getstrType()
	{
		$this->validate();
		return $this->strType;
	}
	
	//retun STOCK_CATEGORY
	function getSTOCK_CATEGORY()
	{
		$this->validate();
		return $this->STOCK_CATEGORY;
	}
	
	//retun intUser
	function getintUser()
	{
		$this->validate();
		return $this->intUser;
	}
	
	//retun dtDate
	function getdtDate()
	{
		$this->validate();
		return $this->dtDate;
	}
	
	//retun intOrderNo
	function getintOrderNo()
	{
		$this->validate();
		return $this->intOrderNo;
	}
	
	//retun intOrderYear
	function getintOrderYear()
	{
		$this->validate();
		return $this->intOrderYear;
	}
	
	//retun intSalesOrderId
	function getintSalesOrderId()
	{
		$this->validate();
		return $this->intSalesOrderId;
	}
	
	//retun intColorId
	function getintColorId()
	{
		$this->validate();
		return $this->intColorId;
	}
	
	//retun intTechnique
	function getintTechnique()
	{
		$this->validate();
		return $this->intTechnique;
	}
	
	//retun intInkType
	function getintInkType()
	{
		$this->validate();
		return $this->intInkType;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set id
	function setid($id)
	{
		array_push($this->commitArray,'id');
		$this->id = $id;
	}
	
	//set intSavedOrder
	function setintSavedOrder($intSavedOrder)
	{
		array_push($this->commitArray,'intSavedOrder');
		$this->intSavedOrder = $intSavedOrder;
	}
	
	//set intCompanyId
	function setintCompanyId($intCompanyId)
	{
		array_push($this->commitArray,'intCompanyId');
		$this->intCompanyId = $intCompanyId;
	}
	
	//set intLocationId
	function setintLocationId($intLocationId)
	{
		array_push($this->commitArray,'intLocationId');
		$this->intLocationId = $intLocationId;
	}
	
	//set intSubStores
	function setintSubStores($intSubStores)
	{
		array_push($this->commitArray,'intSubStores');
		$this->intSubStores = $intSubStores;
	}
	
	//set intDocumentNo
	function setintDocumentNo($intDocumentNo)
	{
		array_push($this->commitArray,'intDocumentNo');
		$this->intDocumentNo = $intDocumentNo;
	}
	
	//set intDocumntYear
	function setintDocumntYear($intDocumntYear)
	{
		array_push($this->commitArray,'intDocumntYear');
		$this->intDocumntYear = $intDocumntYear;
	}
	
	//set intGRNNo
	function setintGRNNo($intGRNNo)
	{
		array_push($this->commitArray,'intGRNNo');
		$this->intGRNNo = $intGRNNo;
	}
	
	//set intGRNYear
	function setintGRNYear($intGRNYear)
	{
		array_push($this->commitArray,'intGRNYear');
		$this->intGRNYear = $intGRNYear;
	}
	
	//set dtGRNDate
	function setdtGRNDate($dtGRNDate)
	{
		array_push($this->commitArray,'dtGRNDate');
		$this->dtGRNDate = $dtGRNDate;
	}
	
	//set intCurrencyId
	function setintCurrencyId($intCurrencyId)
	{
		array_push($this->commitArray,'intCurrencyId');
		$this->intCurrencyId = $intCurrencyId;
	}
	
	//set dblGRNRate
	function setdblGRNRate($dblGRNRate)
	{
		array_push($this->commitArray,'dblGRNRate');
		$this->dblGRNRate = $dblGRNRate;
	}
	
	//set intItemId
	function setintItemId($intItemId)
	{
		array_push($this->commitArray,'intItemId');
		$this->intItemId = $intItemId;
	}
	
	//set dblQty
	function setdblQty($dblQty)
	{
		array_push($this->commitArray,'dblQty');
		$this->dblQty = $dblQty;
	}
	
	//set strType
	function setstrType($strType)
	{
		array_push($this->commitArray,'strType');
		$this->strType = $strType;
	}
	
	//set STOCK_CATEGORY
	function setSTOCK_CATEGORY($STOCK_CATEGORY)
	{
		array_push($this->commitArray,'STOCK_CATEGORY');
		$this->STOCK_CATEGORY = $STOCK_CATEGORY;
	}
	
	//set intUser
	function setintUser($intUser)
	{
		array_push($this->commitArray,'intUser');
		$this->intUser = $intUser;
	}
	
	//set dtDate
	function setdtDate($dtDate)
	{
		array_push($this->commitArray,'dtDate');
		$this->dtDate = $dtDate;
	}
	
	//set intOrderNo
	function setintOrderNo($intOrderNo)
	{
		array_push($this->commitArray,'intOrderNo');
		$this->intOrderNo = $intOrderNo;
	}
	
	//set intOrderYear
	function setintOrderYear($intOrderYear)
	{
		array_push($this->commitArray,'intOrderYear');
		$this->intOrderYear = $intOrderYear;
	}
	
	//set intSalesOrderId
	function setintSalesOrderId($intSalesOrderId)
	{
		array_push($this->commitArray,'intSalesOrderId');
		$this->intSalesOrderId = $intSalesOrderId;
	}
	
	//set intColorId
	function setintColorId($intColorId)
	{
		array_push($this->commitArray,'intColorId');
		$this->intColorId = $intColorId;
	}
	
	//set intTechnique
	function setintTechnique($intTechnique)
	{
		array_push($this->commitArray,'intTechnique');
		$this->intTechnique = $intTechnique;
	}
	
	//set intInkType
	function setintInkType($intInkType)
	{
		array_push($this->commitArray,'intInkType');
		$this->intInkType = $intInkType;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->id=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($id)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "id='$id'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	//END }
	
	//insert as parameters
	public function insertRec($intSavedOrder,$intCompanyId,$intLocationId,$intSubStores,$intDocumentNo,$intDocumntYear,$intGRNNo,$intGRNYear,$dtGRNDate,$intCurrencyId,$dblGRNRate,$intItemId,$dblQty,$strType,$STOCK_CATEGORY,$intUser,$dtDate,$intOrderNo,$intOrderYear,$intSalesOrderId,$intColorId,$intTechnique,$intInkType){
		$data = array(
				'intSavedOrder'=>$intSavedOrder 
				,'intCompanyId'=>$intCompanyId 
				,'intLocationId'=>$intLocationId 
				,'intSubStores'=>$intSubStores 
				,'intDocumentNo'=>$intDocumentNo 
				,'intDocumntYear'=>$intDocumntYear 
				,'intGRNNo'=>$intGRNNo 
				,'intGRNYear'=>$intGRNYear 
				,'dtGRNDate'=>$dtGRNDate 
				,'intCurrencyId'=>$intCurrencyId 
				,'dblGRNRate'=>$dblGRNRate 
				,'intItemId'=>$intItemId 
				,'dblQty'=>$dblQty 
				,'strType'=>$strType 
				,'STOCK_CATEGORY'=>$STOCK_CATEGORY 
				,'intUser'=>$intUser 
				,'dtDate'=>$dtDate 
				,'intOrderNo'=>$intOrderNo 
				,'intOrderYear'=>$intOrderYear 
				,'intSalesOrderId'=>$intSalesOrderId 
				,'intColorId'=>$intColorId 
				,'intTechnique'=>$intTechnique 
				,'intInkType'=>$intInkType 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('id,intSavedOrder',  null, $where = $where);
		while($row=mysqli_fetch_array($result)){
			$html .= '<option value="'.$row['id'].'">'.$row['intSavedOrder'].'</option>';	
		}
		return $html;
	}
	
	public function getColorRoomOrderWiseItemBalance($location,$orderNo,$orderYear,$salesOrderId,$item,$deci){
		
		$cols		= " COALESCE(ROUND(SUM(dblQty),$deci+5),0) AS stock_bal ";
		
		$where		= " intItemId = '".$item."' AND
						STOCK_CATEGORY = 'ORDER' AND
						intOrderNo = '".$orderNo."' AND
						intOrderYear = '".$orderYear."' AND
						intSalesOrderId = '".$salesOrderId."' ";
		if($location !=''){			
		$where		.= " AND
						intLocationId = '".$location."' ";
		}
		
		$result 	= $this->select($cols,$join = null,$where, $order = null, $limit = null);	
		$row		= mysqli_fetch_array($result);
		return 		$row['stock_bal'];
	}
	
	public function getColorRoomOrderAndGrnWiseItemBalance_result($location,$orderNo,$orderYear,$salesOrderId,$item,$deci){
		
		$cols		= " 
						COALESCE(ROUND(Sum(ware_stocktransactions_color_room_item.dblQty),$deci+5),0) AS stockBal,
						ware_stocktransactions_color_room_item.intGRNNo,
						ware_stocktransactions_color_room_item.intGRNYear,
						ware_stocktransactions_color_room_item.dtGRNDate,
						ware_stocktransactions_color_room_item.dblGRNRate
					 ";
		
		$where		= " 
						ware_stocktransactions_color_room_item.intOrderNo 		=  '$orderNo' AND
						ware_stocktransactions_color_room_item.intOrderYear 	=  '$orderYear' AND
						ware_stocktransactions_color_room_item.intSalesOrderId =  '$salesOrderId' AND
						ware_stocktransactions_color_room_item.intItemId 		=  '$item' AND
						ware_stocktransactions_color_room_item.intLocationId 	=  '$location' AND
						ware_stocktransactions_color_room_item.STOCK_CATEGORY 	= 'ORDER'
						GROUP BY
						ware_stocktransactions_color_room_item.intItemId,
						ware_stocktransactions_color_room_item.intGRNNo,
						ware_stocktransactions_color_room_item.intGRNYear, 
						ware_stocktransactions_color_room_item.dblGRNRate	
					 ";
		$order	= 
					 "
						ware_stocktransactions_color_room_item.dtGRNDate ASC
					 ";
		
		$result 	= $this->select($cols,$join = null,$where, $order = null, $limit = null);	
 		return 		$result;
	}
	
	
	public function getColorRoomExtraItemBalance($location,$item,$deci){
	
	$cols		= " COALESCE(ROUND(SUM(dblQty),$deci+5),0) AS stock_bal ";
	
	$where		= " intItemId = '".$item."' AND
					STOCK_CATEGORY <> 'ORDER' AND
					intLocationId = '".$location."' ";
	
	$result 	= $this->select($cols,$join = null,$where, $order = null, $limit = null);	
	$row		= mysqli_fetch_array($result);
	return 		$row['stock_bal'];
	}

	public function getColorRoomGrnWiseExtraItemBalance_result($location,$item,$deci){
		
		$cols		= " 
						COALESCE(ROUND(SUM(ware_stocktransactions_color_room_item.dblQty),".($deci+5)."),0) AS stockBal,
						ware_stocktransactions_color_room_item.intGRNNo,
						ware_stocktransactions_color_room_item.intGRNYear,
						ware_stocktransactions_color_room_item.dtGRNDate,
						ware_stocktransactions_color_room_item.dblGRNRate,
						ware_stocktransactions_color_room_item.intCurrencyId
					 ";
		
		$where		= " 
						intItemId = '".$item."' AND
						STOCK_CATEGORY <> 'ORDER' AND
						intLocationId = '".$location."' 
						
						GROUP BY
						ware_stocktransactions_color_room_item.intItemId,
						ware_stocktransactions_color_room_item.intGRNNo,
						ware_stocktransactions_color_room_item.intGRNYear, 
						ware_stocktransactions_color_room_item.dblGRNRate 
						HAVING stockBal>0 	
					 ";
		$order	= 
					 "
						ware_stocktransactions_color_room_item.dtGRNDate ASC
					 ";
		
		$result 	= $this->select($cols,$join = null,$where, $order = null, $limit = null);	
 		return 		$result;
	}
	
	public function getColorRoomRemainAllExtraItem_result($location,$itemName = null){
		
		$cols		= " 
						Sum(ware_stocktransactions_color_room_item.dblQty) AS stockBal,
						ware_stocktransactions_color_room_item.intItemId,
						mst_item.strCode AS itemCode,
						mst_item.strName AS itemName,
						mst_item.intUOM AS uomID
					 ";
			
		$join		= "
						INNER JOIN mst_item on ware_stocktransactions_color_room_item.intItemId= mst_item.intId
					  "	;		 
		
		$where		= " 
						STOCK_CATEGORY <> 'ORDER' AND
						intLocationId = '".$location."' ";
		if($itemName != NULL || $itemName != 0)
		{
			$where		.= " AND mst_item.strName LIKE '%$itemName%'" ;
		}
		
		$where			.=	" GROUP BY
						ware_stocktransactions_color_room_item.intItemId 
						HAVING 	stockBal > 0
					 ";
		$order		= "
						 mst_item.strName ASC 
					  ";
 		
		$result 	= $this->select($cols,$join,$where, $order, $limit = null);	
 		return 		$result;
	}

	public function getItemWiseExtraStockBalance($itemId,$locationId,$deci)
	{
		//global $deci;
		
		$cols		= "ROUND(COALESCE(SUM(dblQty),0),$deci+5) 	AS STOCK_BAL";
				
		$join		= "INNER JOIN mst_item on ware_stocktransactions_color_room_item.intItemId= mst_item.intId"	;		 
		
		$where		= "intItemId = '$itemId' AND STOCK_CATEGORY = 'EXTRA'";
		
		if($locationId!=''){	
		$where		.="  AND intLocationId = $locationId 	   ";
		}
		$result = $this->select($cols,$join = null,$where, $order = null, $limit = null);
		$row	= mysqli_fetch_array($result);
		return $row['STOCK_BAL'];
	}
	
	public function getOrderWiseColorRoomItemStockBalance($orderNo,$orderYear,$salesOrderId,$itemId)
	{
		global $deci;
		global $sessions;
		
		$cols		= "ROUND(COALESCE(SUM(dblQty),0),$deci+5) AS STOCK_BAL";

		$where		= "intOrderNo = $orderNo
					   AND intOrderYear = $orderYear
					   AND intSalesOrderId = $salesOrderId
					   AND intItemId = $itemId
					   AND intLocationId = ".$sessions->getLocationId()."
					   AND STOCK_CATEGORY = 'ORDER'";
		$result = $this->select($cols,$join = null,$where, $order = null, $limit = null);
		$row	= mysqli_fetch_array($result);
		return $row['STOCK_BAL'];
	}
	
	public function getOrderWiseColorRoomItemStockBalance_multipleSO($concatOrderNo,$itemId)
	{
		global $deci;
		global $sessions;
		
		$cols		= "ROUND(COALESCE(SUM(dblQty),0),$deci+5) AS STOCK_BAL";

		$where		= "CONCAT(ware_stocktransactions_color_room_item.intOrderNo,'/',ware_stocktransactions_color_room_item.intOrderYear,'/',ware_stocktransactions_color_room_item.intSalesOrderId)IN($concatOrderNo) 
					   AND intItemId = $itemId
					   AND intLocationId = ".$sessions->getLocationId()."
					   AND STOCK_CATEGORY = 'ORDER'";
		
 		//if($itemId==723)
		//echo "select ".$cols." from ware_stocktransactions_color_room_item where ".$where;
		$result = $this->select($cols,$join = null,$where, $order = null, $limit = null);
		$row	= mysqli_fetch_array($result);
		return $row['STOCK_BAL'];
	}

	
	public function getOrderWiseColorRoomItemStockBalance_without_location($orderNo,$orderYear,$salesOrderId,$itemId)
	{
		global $deci;
		global $sessions;
		
		$cols		= "ROUND(COALESCE(SUM(dblQty),0),$deci+5) AS STOCK_BAL";

		$where		= "intOrderNo = $orderNo
					   AND intOrderYear = $orderYear
					   AND intSalesOrderId = $salesOrderId
					   AND intItemId = $itemId
 					   AND STOCK_CATEGORY = 'ORDER'";
 		//if($itemId==723)
		//echo "select ".$cols." from ware_stocktransactions_color_room_item ".$join." where ".$where;
 		$result = $this->select($cols,$join = null,$where, $order = null, $limit = null);
		$row	= mysqli_fetch_array($result);
		return $row['STOCK_BAL'];
	}
	
	public function getExtraItemDetails()
	{
		
	}
	
	public function getItemWiseStockBalance($itemId,$locationId,$deci)
	{
		$cols		= "ROUND(COALESCE(SUM(dblQty),0),$deci+5) 	AS STOCK_BAL";
				
		$join		= "INNER JOIN mst_item on ware_stocktransactions_color_room_item.intItemId= mst_item.intId"	;		 
		
		$where		= "intItemId = '$itemId'
					   AND intLocationId = $locationId";
		$result = $this->select($cols,$join = null,$where, $order = null, $limit = null);
		$row	= mysqli_fetch_array($result);
		return $row['STOCK_BAL'];
	}
	
	
		public function getActualItemPropotion_so($orderNo,$orderYear,$salesOrderId,$itemId,$deci)
	{
		global $ware_stocktransactions_color_room_item;
		
		$sql = "SELECT COALESCE(ROUND(actPrpItemWise/actPrpOrderWise,$deci+5),0) AS actual_item_propotion
				FROM
				(
				SELECT
				(SELECT COALESCE(SUM(dblQty*-1),0)
				FROM ware_stocktransactions_color_room_item
				WHERE
				intOrderNo = '".$orderNo."' AND
				intOrderYear = '".$orderYear."' AND
				intSalesOrderId = '".$salesOrderId."' AND
				intItemId = '".$itemId."' AND
				strType = 'COLOR_CREATE') AS actPrpItemWise,
				
				(SELECT COALESCE(SUM(dblQty*-1),0)
				FROM ware_stocktransactions_color_room_item
				WHERE
				intOrderNo = '".$orderNo."' AND
				intOrderYear = '".$orderYear."' AND
				intSalesOrderId = '".$salesOrderId."' AND
				strType = 'COLOR_CREATE') AS actPrpOrderWise
				) AS tb1 ";	
				
		$result_1	= $this->db->RunQuery($sql);
		
		if($result_1[0]['actual_item_propotion']<= 0 || $result_1[0]['actual_item_propotion']==''){
			
		
		$sql = "SELECT COALESCE(ROUND(actPrpItemWise/actPrpOrderWise,$deci+5),0) AS actual_item_propotion
				FROM
				(
				SELECT
				(SELECT COALESCE(SUM(dblWeight),0)
				FROM trn_sample_color_recipes
				INNER JOIN trn_orderdetails OD
				ON OD.intSampleNo = trn_sample_color_recipes.intSampleNo
				AND OD.intSampleYear = trn_sample_color_recipes.intSampleYear
				AND OD.intRevisionNo = trn_sample_color_recipes.intRevisionNo
				AND OD.strCombo = trn_sample_color_recipes.strCombo
				AND OD.strPrintName = trn_sample_color_recipes.strPrintName
				INNER JOIN trn_orderheader OH ON OH.intOrderNo = OD.intOrderNo AND
				OH.intOrderYear = OD.intOrderYear
											WHERE
				OD.intOrderNo = '".$orderNo."' AND
				OD.intOrderYear = '".$orderYear."' AND
				OD.intSalesOrderId = '".$salesOrderId."' AND
				trn_sample_color_recipes.intItem = '".$itemId."'  ) AS actPrpItemWise,
				
				(SELECT COALESCE(SUM(dblWeight),0)
				FROM trn_sample_color_recipes
				INNER JOIN trn_orderdetails OD
				ON OD.intSampleNo = trn_sample_color_recipes.intSampleNo
				AND OD.intSampleYear = trn_sample_color_recipes.intSampleYear
				AND OD.intRevisionNo = trn_sample_color_recipes.intRevisionNo
				AND OD.strCombo = trn_sample_color_recipes.strCombo
				AND OD.strPrintName = trn_sample_color_recipes.strPrintName
				INNER JOIN trn_orderheader OH ON OH.intOrderNo = OD.intOrderNo AND
				OH.intOrderYear = OD.intOrderYear
				WHERE
				OD.intOrderNo = '".$orderNo."' AND
				OD.intOrderYear = '".$orderYear."' AND
				OD.intSalesOrderId = '".$salesOrderId."' ) AS actPrpOrderWise
				) AS tb1 ";	
			$result_1	= $this->db->RunQuery($sql);
			
		}
		
		return $result_1[0]['actual_item_propotion'];
		
	}
	
	
		public function getActualItemPropotion($orderNo,$orderYear,$salesOrderId,$itemId,$colorId,$techniqueId,$inkType,$deci)
	{
		global $ware_stocktransactions_color_room_item;
		
		$actial_flag=$this->getBulkRecipesFlag($orderNo,$orderYear,$salesOrderId,$colorId,$techniqueId,$inkType,$deci);
		
		if($actial_flag==1){
		
		$sql = "SELECT COALESCE(ROUND(actPrpItemWise/actPrpOrderWise,$deci+5),0) AS actual_item_propotion,
		1 as type 
				FROM
				(
				SELECT
				(SELECT COALESCE(SUM(dblQty*-1),0)
				FROM ware_stocktransactions_color_room_item
				WHERE
				intOrderNo = '".$orderNo."' AND
				intOrderYear = '".$orderYear."' AND
				intSalesOrderId = '".$salesOrderId."' AND
				intItemId = '".$itemId."' AND
				intColorId = '".$colorId."' AND
				intTechnique = '".$techniqueId."' AND
				intInkType = '".$inkType."' AND 
				strType = 'COLOR_CREATE') AS actPrpItemWise,
				
				(SELECT COALESCE(SUM(dblQty*-1),0)
				FROM ware_stocktransactions_color_room_item
				WHERE
				intOrderNo = '".$orderNo."' AND
				intOrderYear = '".$orderYear."' AND
				intSalesOrderId = '".$salesOrderId."' AND
				intColorId = '".$colorId."' AND
				intTechnique = '".$techniqueId."' AND
				intInkType = '".$inkType."' AND 
				strType = 'COLOR_CREATE') AS actPrpOrderWise
				) AS tb1 ";	
				
		$result_1	= $this->db->RunQuery($sql);
		}
		else{
	//	if($result_1[0]['actual_item_propotion']<= 0 || $result_1[0]['actual_item_propotion']==''){
			
		
		$sql = "SELECT COALESCE(ROUND(actPrpItemWise/actPrpOrderWise,$deci+5),0) AS actual_item_propotion ,
		0 as type 
				FROM
				(
				SELECT
				(SELECT COALESCE(SUM(dblWeight),0)
				FROM trn_sample_color_recipes
				INNER JOIN trn_orderdetails OD
				ON OD.intSampleNo = trn_sample_color_recipes.intSampleNo
				AND OD.intSampleYear = trn_sample_color_recipes.intSampleYear
				AND OD.intRevisionNo = trn_sample_color_recipes.intRevisionNo
				AND OD.strCombo = trn_sample_color_recipes.strCombo
				AND OD.strPrintName = trn_sample_color_recipes.strPrintName
				INNER JOIN trn_orderheader OH ON OH.intOrderNo = OD.intOrderNo AND
				OH.intOrderYear = OD.intOrderYear
											WHERE
				OD.intOrderNo = '".$orderNo."' AND
				OD.intOrderYear = '".$orderYear."' AND
				OD.intSalesOrderId = '".$salesOrderId."' AND
				trn_sample_color_recipes.intItem = '".$itemId."' AND
				trn_sample_color_recipes.intColorId = '".$colorId."' AND
				trn_sample_color_recipes.intTechniqueId = '".$techniqueId."' AND
				trn_sample_color_recipes.intInkTypeId = '".$inkType."' ) AS actPrpItemWise,
				
				(SELECT COALESCE(SUM(dblWeight),0)
				FROM trn_sample_color_recipes
				INNER JOIN trn_orderdetails OD
				ON OD.intSampleNo = trn_sample_color_recipes.intSampleNo
				AND OD.intSampleYear = trn_sample_color_recipes.intSampleYear
				AND OD.intRevisionNo = trn_sample_color_recipes.intRevisionNo
				AND OD.strCombo = trn_sample_color_recipes.strCombo
				AND OD.strPrintName = trn_sample_color_recipes.strPrintName
				INNER JOIN trn_orderheader OH ON OH.intOrderNo = OD.intOrderNo AND
				OH.intOrderYear = OD.intOrderYear
				WHERE
				OD.intOrderNo = '".$orderNo."' AND
				OD.intOrderYear = '".$orderYear."' AND
				OD.intSalesOrderId = '".$salesOrderId."' AND
				trn_sample_color_recipes.intColorId = '".$colorId."' AND
				trn_sample_color_recipes.intTechniqueId = '".$techniqueId."' AND
				trn_sample_color_recipes.intInkTypeId = '".$inkType."' ) AS actPrpOrderWise
				) AS tb1 ";	
			$result_1	= $this->db->RunQuery($sql);
			
		}
		
		
		
		return $result_1[0]['actual_item_propotion'];
		
	}
	
	public function getItemWiseAvaragePrice($orderNo,$orderYear,$salesOrderId,$itemId,$convertCurrency,$convertDate,$deci)
	{
		$cols		= "ROUND(SUM((dblGRNRate * FE.dblBuying)/(SELECT FE1.dblBuying 
					FROM mst_financeexchangerate FE1 
					INNER JOIN mst_financecurrency 
						ON FE1.intCurrencyId = mst_financecurrency.intId 
					WHERE FE1.intCurrencyId = '$convertCurrency' 
						AND FE1.intCompanyId = ware_stocktransactions_color_room_item.intCompanyId
						AND FE1.dtmDate = '$convertDate') * dblQty)/SUM(dblQty),$deci+5) 						AS AVG_ITEM_PRICE";
						
		$join		= " LEFT JOIN mst_financeexchangerate FE
						ON ware_stocktransactions_color_room_item.intCurrencyId = FE.intCurrencyId
						  AND ware_stocktransactions_color_room_item.dtGRNDate = FE.dtmDate
						  AND ware_stocktransactions_color_room_item.intCompanyId = FE.intCompanyId";
	  
		$where		= "intOrderNo = '$orderNo'
						AND intOrderYear = '$orderYear'
						AND intSalesOrderId = $salesOrderId
						AND intItemId = '$itemId'
						AND strType = 'COLOR_CREATE'";

		$result = $this->select($cols,$join,$where,$order=null,$limit=null);
		$row	= mysqli_fetch_array($result);
		return $row['AVG_ITEM_PRICE'];
	}
	
	public function getItemWiseActualCost($orderNo,$orderYear,$salesOrderId,$itemId,$convertCurrency,$convertDate,$deci)
	{
		$cols		= "ROUND(SUM((dblGRNRate * FE.dblBuying)/(SELECT FE1.dblBuying 
					FROM mst_financeexchangerate FE1 
					INNER JOIN mst_financecurrency 
						ON FE1.intCurrencyId = mst_financecurrency.intId 
					WHERE FE1.intCurrencyId = '$convertCurrency' 
						AND FE1.intCompanyId = ware_stocktransactions_color_room_item.intCompanyId
						AND FE1.dtmDate = '$convertDate') * dblQty),$deci+4) 						AS ACTUAL_COST";
		
		$join		= " LEFT JOIN mst_financeexchangerate FE
						ON ware_stocktransactions_color_room_item.intCurrencyId = FE.intCurrencyId
						  AND ware_stocktransactions_color_room_item.dtGRNDate = FE.dtmDate
						  AND ware_stocktransactions_color_room_item.intCompanyId = FE.intCompanyId";
						  
		$where		= "intOrderNo = '$orderNo'
						AND intOrderYear = '$orderYear'
						AND intSalesOrderId = $salesOrderId
						AND intItemId = '$itemId'
						AND strType = 'COLOR_CREATE'";
		$result = $this->select($cols,$join,$where,$order=null,$limit=null);
		$row	= mysqli_fetch_array($result);
		return abs($row['ACTUAL_COST']);
	}
	
	public function getInkItemWastage($orderNo,$orderYear,$salesOrderId,$itemId,$deci)
	{
		global $trn_sample_color_recipes;
		global $ware_stocktransactions_color_room_color;
		global $mst_units;
		
		$itemWeight		= 0;
		$resultw = $trn_sample_color_recipes->getColorsForSelectedOrdersAndItem($orderNo,$orderYear,$salesOrderId,$itemId);
		while($roww = mysqli_fetch_array($resultw))
		{
			$wastage			= $ware_stocktransactions_color_room_color->getOrderWiseColorWastage($orderNo,$orderYear,$salesOrderId,$roww['COLOR_ID'],$roww['TECHNIQUE_ID'],$roww['INK_TYPE']);
			$itemPropotion 		= $this->getActualItemPropotion($orderNo,$orderYear,$salesOrderId,$itemId,$roww['COLOR_ID'],$roww['TECHNIQUE_ID'],$roww['INK_TYPE'],$deci);
			$itemWeight		   += round($wastage * $itemPropotion,$deci);
		}
		return $mst_units->conversion('G',$itemWeight,$itemId,$deci);
	}

	public function getInkItemWastage_noneProcessed($orderNo,$orderYear,$salesOrderId,$itemId,$deci)
	{
		global $trn_sample_color_recipes;
		global $ware_stocktransactions_color_room_color;
		global $ink_color_room_wastage_header;
		global $mst_units;
		
		$itemWeight		= 0;
 		$resultw = $trn_sample_color_recipes->getColorsForSelectedOrdersAndItem($orderNo,$orderYear,$salesOrderId,$itemId);
		while($roww = mysqli_fetch_array($resultw))
		{
			$wastage			= $ink_color_room_wastage_header->getPendingWastage_weight_noneProcessed($orderNo,$orderYear,$salesOrderId,$roww['COLOR_ID'],$roww['TECHNIQUE_ID'],$roww['INK_TYPE']);
			$itemPropotion 		= $this->getActualItemPropotion($orderNo,$orderYear,$salesOrderId,$itemId,$roww['COLOR_ID'],$roww['TECHNIQUE_ID'],$roww['INK_TYPE'],$deci);
			$itemWeight		   += round($wastage * $itemPropotion,$deci);
		}
		return $mst_units->conversion('G',$itemWeight,$itemId,$deci);
	}

	
	public function getColorRoomData_docNoWise($docNo,$docYear,$allocStockType,$unallocStockType,$deci)
	{
		$cols	= " intOrderNo,
					intOrderYear,
					intSalesOrderId,
					intItemId,
					(
					SELECT COALESCE(ROUND(SUM(dblQty),$deci+2),0)
					FROM ware_stocktransactions_color_room_item ACR
					WHERE 
					ACR.intOrderNo = ware_stocktransactions_color_room_item.intOrderNo AND
					ACR.intOrderYear = ware_stocktransactions_color_room_item.intOrderYear AND
					ACR.intLocationId = ware_stocktransactions_color_room_item.intLocationId AND
					ACR.intDocumentNo = ware_stocktransactions_color_room_item.intDocumentNo AND
					ACR.intDocumntYear = ware_stocktransactions_color_room_item.intDocumntYear AND
					ACR.intItemId = ware_stocktransactions_color_room_item.intItemId AND
					ACR.intSalesOrderId = ware_stocktransactions_color_room_item.intSalesOrderId AND
					ACR.strType = '$allocStockType' AND
					ACR.STOCK_CATEGORY = 'ORDER'
					) allocate_qty,
					(
					SELECT COALESCE(ROUND(SUM(dblQty),$deci+2),0)
					FROM ware_stocktransactions_color_room_item UCR
					WHERE 
					UCR.intOrderNo = ware_stocktransactions_color_room_item.intOrderNo AND
					UCR.intOrderYear = ware_stocktransactions_color_room_item.intOrderYear AND
					UCR.intLocationId = ware_stocktransactions_color_room_item.intLocationId AND
					UCR.intDocumentNo = ware_stocktransactions_color_room_item.intDocumentNo AND
					UCR.intDocumntYear = ware_stocktransactions_color_room_item.intDocumntYear AND
					UCR.intItemId = ware_stocktransactions_color_room_item.intItemId AND
					UCR.intSalesOrderId = ware_stocktransactions_color_room_item.intSalesOrderId AND
					UCR.strType = '$unallocStockType' AND
					UCR.STOCK_CATEGORY = 'EXTRA'
					) unallocate_qty ";
		
		$where	= " strType IN ('".$allocStockType."','".$unallocStockType."') AND					
					intDocumentNo = '".$docNo."' AND
					intDocumntYear = '".$docYear."'
					GROUP BY intOrderNo,intOrderYear,intDocumentNo,intDocumntYear,intSalesOrderId,intItemId ";
		
		return $this->select($cols,$join=null,$where,$order=null,$limit=null);
	}
	
	
public function getColorRoomData($orderNo,$orderYear,$allocStockType,$unallocStockType,$docNo,$docYear,$deci)
{
 	global $sessions;
	
	$cols	= " intOrderNo,
				intOrderYear,
				intSalesOrderId,
				intItemId,
				(
				SELECT COALESCE(ROUND(SUM(dblQty),$deci+2),0)
				FROM ware_stocktransactions_color_room_item ACR
				WHERE 
				ACR.intOrderNo = ware_stocktransactions_color_room_item.intOrderNo AND
				ACR.intOrderYear = ware_stocktransactions_color_room_item.intOrderYear AND
				ACR.intLocationId = ware_stocktransactions_color_room_item.intLocationId AND
				ACR.intDocumentNo = ware_stocktransactions_color_room_item.intDocumentNo AND
				ACR.intDocumntYear = ware_stocktransactions_color_room_item.intDocumntYear AND
				ACR.intItemId = ware_stocktransactions_color_room_item.intItemId AND
				ACR.intSalesOrderId = ware_stocktransactions_color_room_item.intSalesOrderId AND
				ACR.strType = '$allocStockType' AND
				ACR.STOCK_CATEGORY = 'ORDER'
				) allocate_qty,
				(
				SELECT COALESCE(ROUND(SUM(dblQty),$deci+2),0)
				FROM ware_stocktransactions_color_room_item UCR
				WHERE 
				UCR.intOrderNo = ware_stocktransactions_color_room_item.intOrderNo AND
				UCR.intOrderYear = ware_stocktransactions_color_room_item.intOrderYear AND
				UCR.intLocationId = ware_stocktransactions_color_room_item.intLocationId AND
				UCR.intDocumentNo = ware_stocktransactions_color_room_item.intDocumentNo AND
				UCR.intDocumntYear = ware_stocktransactions_color_room_item.intDocumntYear AND
				UCR.intItemId = ware_stocktransactions_color_room_item.intItemId AND
				UCR.intSalesOrderId = ware_stocktransactions_color_room_item.intSalesOrderId AND
				UCR.strType = '$unallocStockType' AND
				UCR.STOCK_CATEGORY = 'EXTRA'
				) unallocate_qty ";
	
	$where	= " intOrderNo = '".$orderNo."' AND
				intOrderYear = '".$orderYear."' AND
				strType IN ('".$allocStockType."','".$unallocStockType."') AND
				/*intLocationId = '".$sessions->getLocationId()."'  AND */
				intDocumentNo = '".$docNo."' AND
				intDocumntYear = '".$docYear."'
				GROUP BY intOrderNo,intOrderYear,intDocumentNo,intDocumntYear,intSalesOrderId,intItemId ";
	
	$result	= $this->select($cols,$join=null,$where,$order=null,$limit=null);
	
	return $result;
}

public function getBulkRecipesFlag($orderNo,$orderYear,$salesOrderId,$colorId,$techniqueId,$inkType,$deci){
		$sql = "SELECT COALESCE(ROUND(actPrpItemWise/actPrpOrderWise,$deci+5),0) AS actual_item_propotion,
		1 as type 
				FROM
				(
				SELECT
				(SELECT COALESCE(SUM(dblQty*-1),0)
				FROM ware_stocktransactions_color_room_item
				WHERE
				intOrderNo = '".$orderNo."' AND
				intOrderYear = '".$orderYear."' AND
				intSalesOrderId = '".$salesOrderId."' AND
 				intColorId = '".$colorId."' AND
				intTechnique = '".$techniqueId."' AND
				intInkType = '".$inkType."' AND 
				strType = 'COLOR_CREATE') AS actPrpItemWise,
				
				(SELECT COALESCE(SUM(dblQty*-1),0)
				FROM ware_stocktransactions_color_room_item
				WHERE
				intOrderNo = '".$orderNo."' AND
				intOrderYear = '".$orderYear."' AND
				intSalesOrderId = '".$salesOrderId."' AND
				intColorId = '".$colorId."' AND
				intTechnique = '".$techniqueId."' AND
				intInkType = '".$inkType."' AND 
				strType = 'COLOR_CREATE') AS actPrpOrderWise
				) AS tb1 ";	
				
		$result_1	= $this->db->RunQuery($sql);
 		if($result_1[0]['actual_item_propotion'] > 0)
		{
			return 1;
		}
		else
			return 0;
		
	}



	
}
?>
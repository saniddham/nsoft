<?php
//THIS CLASS MANUALLY EDITED BY LAHIRU
class ink_production_return_details{
 
	private $db;
	private $table= "ink_production_return_details";
	
	//private property
	private $SERIAL_NO;
	private $SERIAL_YEAR;
	private $ORDER_NO;
	private $ORDER_YEAR;
	private $SALES_ORDER_ID;
	private $INK_COLOR;
	private $INK_TECHNIQUE;
	private $INK_TYPE;
	private $INK_WEIGHT;
	private $INK_WASTAGE;
	private $WASTAGE_REASON_ID;
	private $WASTAGE_REMARKS;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('SERIAL_NO'=>'SERIAL_NO',
										'SERIAL_YEAR'=>'SERIAL_YEAR',
										'ORDER_NO'=>'ORDER_NO',
										'ORDER_YEAR'=>'ORDER_YEAR',
										'SALES_ORDER_ID'=>'SALES_ORDER_ID',
										'INK_COLOR'=>'INK_COLOR',
										'INK_TECHNIQUE'=>'INK_TECHNIQUE',
										'INK_TYPE'=>'INK_TYPE',
										'INK_WEIGHT'=>'INK_WEIGHT',
										'INK_WASTAGE'=>'INK_WASTAGE',
										'WASTAGE_REASON_ID'=>'WASTAGE_REASON_ID',
										'WASTAGE_REMARKS'=>'WASTAGE_REMARKS',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "SERIAL_NO = ".$this->SERIAL_NO." and SERIAL_YEAR = ".$this->SERIAL_YEAR." and ORDER_NO = ".$this->ORDER_NO." and ORDER_YEAR = ".$this->ORDER_YEAR." and SALES_ORDER_ID = ".$this->SALES_ORDER_ID." and INK_COLOR = ".$this->INK_COLOR." and INK_TECHNIQUE = ".$this->INK_TECHNIQUE." and INK_TYPE = ".$this->INK_TYPE."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun SERIAL_NO
	function getSERIAL_NO()
	{
		$this->validate();
		return $this->SERIAL_NO;
	}
	
	//retun SERIAL_YEAR
	function getSERIAL_YEAR()
	{
		$this->validate();
		return $this->SERIAL_YEAR;
	}
	
	//retun ORDER_NO
	function getORDER_NO()
	{
		$this->validate();
		return $this->ORDER_NO;
	}
	
	//retun ORDER_YEAR
	function getORDER_YEAR()
	{
		$this->validate();
		return $this->ORDER_YEAR;
	}
	
	//retun SALES_ORDER_ID
	function getSALES_ORDER_ID()
	{
		$this->validate();
		return $this->SALES_ORDER_ID;
	}
	
	//retun INK_COLOR
	function getINK_COLOR()
	{
		$this->validate();
		return $this->INK_COLOR;
	}
	
	//retun INK_TECHNIQUE
	function getINK_TECHNIQUE()
	{
		$this->validate();
		return $this->INK_TECHNIQUE;
	}
	
	//retun INK_TYPE
	function getINK_TYPE()
	{
		$this->validate();
		return $this->INK_TYPE;
	}
	
	//retun INK_WEIGHT
	function getINK_WEIGHT()
	{
		$this->validate();
		return $this->INK_WEIGHT;
	}
	
	//retun INK_WASTAGE
	function getINK_WASTAGE()
	{
		$this->validate();
		return $this->INK_WASTAGE;
	}
	
	//retun WASTAGE_REASON_ID
	function getWASTAGE_REASON_ID()
	{
		$this->validate();
		return $this->WASTAGE_REASON_ID;
	}
	
	//retun WASTAGE_REMARKS
	function getWASTAGE_REMARKS()
	{
		$this->validate();
		return $this->WASTAGE_REMARKS;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set SERIAL_NO
	function setSERIAL_NO($SERIAL_NO)
	{
		array_push($this->commitArray,'SERIAL_NO');
		$this->SERIAL_NO = $SERIAL_NO;
	}
	
	//set SERIAL_YEAR
	function setSERIAL_YEAR($SERIAL_YEAR)
	{
		array_push($this->commitArray,'SERIAL_YEAR');
		$this->SERIAL_YEAR = $SERIAL_YEAR;
	}
	
	//set ORDER_NO
	function setORDER_NO($ORDER_NO)
	{
		array_push($this->commitArray,'ORDER_NO');
		$this->ORDER_NO = $ORDER_NO;
	}
	
	//set ORDER_YEAR
	function setORDER_YEAR($ORDER_YEAR)
	{
		array_push($this->commitArray,'ORDER_YEAR');
		$this->ORDER_YEAR = $ORDER_YEAR;
	}
	
	//set SALES_ORDER_ID
	function setSALES_ORDER_ID($SALES_ORDER_ID)
	{
		array_push($this->commitArray,'SALES_ORDER_ID');
		$this->SALES_ORDER_ID = $SALES_ORDER_ID;
	}
	
	//set INK_COLOR
	function setINK_COLOR($INK_COLOR)
	{
		array_push($this->commitArray,'INK_COLOR');
		$this->INK_COLOR = $INK_COLOR;
	}
	
	//set INK_TECHNIQUE
	function setINK_TECHNIQUE($INK_TECHNIQUE)
	{
		array_push($this->commitArray,'INK_TECHNIQUE');
		$this->INK_TECHNIQUE = $INK_TECHNIQUE;
	}
	
	//set INK_TYPE
	function setINK_TYPE($INK_TYPE)
	{
		array_push($this->commitArray,'INK_TYPE');
		$this->INK_TYPE = $INK_TYPE;
	}
	
	//set INK_WEIGHT
	function setINK_WEIGHT($INK_WEIGHT)
	{
		array_push($this->commitArray,'INK_WEIGHT');
		$this->INK_WEIGHT = $INK_WEIGHT;
	}
	
	//set INK_WASTAGE
	function setINK_WASTAGE($INK_WASTAGE)
	{
		array_push($this->commitArray,'INK_WASTAGE');
		$this->INK_WASTAGE = $INK_WASTAGE;
	}
	
	//set WASTAGE_REASON_ID
	function setWASTAGE_REASON_ID($WASTAGE_REASON_ID)
	{
		array_push($this->commitArray,'WASTAGE_REASON_ID');
		$this->WASTAGE_REASON_ID = $WASTAGE_REASON_ID;
	}
	
	//set WASTAGE_REMARKS
	function setWASTAGE_REMARKS($WASTAGE_REMARKS)
	{
		array_push($this->commitArray,'WASTAGE_REMARKS');
		$this->WASTAGE_REMARKS = $WASTAGE_REMARKS;
	}
	
	//END }

	//validate primary values
	private function validate()
	{
		if($this->SERIAL_NO=='' || $this->SERIAL_YEAR=='' || $this->ORDER_NO=='' || $this->ORDER_YEAR=='' || $this->SALES_ORDER_ID=='' || $this->INK_COLOR=='' || $this->INK_TECHNIQUE=='' || $this->INK_TYPE=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($SERIAL_NO , $SERIAL_YEAR , $ORDER_NO , $ORDER_YEAR , $SALES_ORDER_ID , $INK_COLOR , $INK_TECHNIQUE , $INK_TYPE)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "SERIAL_NO='$SERIAL_NO' and SERIAL_YEAR='$SERIAL_YEAR' and ORDER_NO='$ORDER_NO' and ORDER_YEAR='$ORDER_YEAR' and SALES_ORDER_ID='$SALES_ORDER_ID' and INK_COLOR='$INK_COLOR' and INK_TECHNIQUE='$INK_TECHNIQUE' and INK_TYPE='$INK_TYPE'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($SERIAL_NO,$SERIAL_YEAR,$ORDER_NO,$ORDER_YEAR,$SALES_ORDER_ID,$INK_COLOR,$INK_TECHNIQUE,$INK_TYPE,$INK_WEIGHT,$INK_WASTAGE,$WASTAGE_REASON_ID,$WASTAGE_REMARKS){
		$data = array('SERIAL_NO'=>$SERIAL_NO 
				,'SERIAL_YEAR'=>$SERIAL_YEAR 
				,'ORDER_NO'=>$ORDER_NO 
				,'ORDER_YEAR'=>$ORDER_YEAR 
				,'SALES_ORDER_ID'=>$SALES_ORDER_ID 
				,'INK_COLOR'=>$INK_COLOR 
				,'INK_TECHNIQUE'=>$INK_TECHNIQUE 
				,'INK_TYPE'=>$INK_TYPE 
				,'INK_WEIGHT'=>$INK_WEIGHT 
				,'INK_WASTAGE'=>$INK_WASTAGE 
				,'WASTAGE_REASON_ID'=>$WASTAGE_REASON_ID 
				,'WASTAGE_REMARKS'=>$WASTAGE_REMARKS 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('SERIAL_NO,SERIAL_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['SERIAL_NO'])
				$html .= '<option selected="selected" value="'.$row['SERIAL_NO'].'">'.$row['SERIAL_YEAR'].'</option>';	
			else
				$html .= '<option value="'.$row['SERIAL_NO'].'">'.$row['SERIAL_YEAR'].'</option>';	
		}
		return $html;
	}
	
//-------------------------------------- MANUALLY EDIT -----------------------------------------------------------------
	public function getReturnWastagetWeight($orderNo,$orderYear,$salesOrderId,$color,$tehnique,$inkType,$location,$deci)
	{
		$cols	= " COALESCE(ROUND(SUM(ink_production_return_details.INK_WEIGHT-ink_production_return_details.INK_WASTAGE),$deci+2),0) AS RETURN_WASTAGE ";
		
		$join	= " INNER JOIN ink_production_return_header PRH ON PRH.SERIAL_NO=ink_production_return_details.SERIAL_NO AND
					PRH.SERIAL_YEAR=ink_production_return_details.SERIAL_YEAR ";
		
		$where	= " PRH.STATUS = 1 AND 
					ink_production_return_details.ORDER_NO = '".$orderNo."' AND
					ink_production_return_details.ORDER_YEAR = '".$orderYear."' AND
					ink_production_return_details.SALES_ORDER_ID = '".$salesOrderId."' AND
					ink_production_return_details.INK_COLOR = '".$color."' AND
					ink_production_return_details.INK_TECHNIQUE = '".$tehnique."' AND
					ink_production_return_details.INK_TYPE = '".$inkType."' AND
					PRH.LOCATION_ID = '".$location."' ";
		
		$result = $this->select($cols,$join,$where,$order = null, $limit = null);	
		
		$row	= mysqli_fetch_array($result);
		return 	$row['RETURN_WASTAGE'];
		
	}
	
	public function getReturnWeight($orderNo,$orderYear,$salesOrderId,$color,$tehnique,$inkType,$location,$deci)
	{
		$cols	= " COALESCE(ROUND(SUM(ink_production_return_details.INK_WEIGHT),$deci+2),0) AS RETURN_WASTAGE ";
		
		$join	= " INNER JOIN ink_production_return_header PRH ON PRH.SERIAL_NO=ink_production_return_details.SERIAL_NO AND
					PRH.SERIAL_YEAR=ink_production_return_details.SERIAL_YEAR ";
		
		$where	= " PRH.STATUS = 1 AND 
					ink_production_return_details.ORDER_NO = '".$orderNo."' AND
					ink_production_return_details.ORDER_YEAR = '".$orderYear."' AND
					ink_production_return_details.SALES_ORDER_ID = '".$salesOrderId."' AND
					ink_production_return_details.INK_COLOR = '".$color."' AND
					ink_production_return_details.INK_TECHNIQUE = '".$tehnique."' AND
					ink_production_return_details.INK_TYPE = '".$inkType."' AND
					PRH.LOCATION_ID = '".$location."' ";
		
		$result = $this->select($cols,$join,$where,$order = null, $limit = null);	
		
		$row	= mysqli_fetch_array($result);
		return 	$row['RETURN_WASTAGE'];
		
	}

	public function getReturnedQty($issueNo,$issueYear,$concatOrderNo,$colorId,$techId,$inkId,$location,$deci)
	{
		$cols	= "ROUND(COALESCE(SUM(INK_WEIGHT),0),$deci+2) 	AS INK_WEIGHT,
				   ROUND(COALESCE(SUM(INK_WASTAGE),0),$deci+2) 	AS INK_WASTAGE";
		
		$join 	= "INNER JOIN ink_production_return_header H
		           		ON H.SERIAL_NO = ink_production_return_details.SERIAL_NO
						AND H.SERIAL_YEAR = ink_production_return_details.SERIAL_YEAR";
		
		$where	= "ISSUE_NO = $issueNo
			       AND ISSUE_YEAR = $issueYear
				   AND CONCAT(ORDER_NO,'/',ORDER_YEAR,'/',SALES_ORDER_ID) IN ($concatOrderNo)
		           AND INK_COLOR = $colorId
				   AND INK_TECHNIQUE = $techId
				   AND INK_TYPE = $inkId
				   AND H.STATUS = 1
				   AND H.LOCATION_ID = $location";
		$return = $this->select($cols,$join,$where,$order=null,$limit=null);	
		return mysqli_fetch_array($return);
	}
	
	public function getSavedDetails($returnNo,$returnYear)
	{
		$cols	= "ISSUE_NO,
				  ISSUE_YEAR,
				  ORDER_NO,
				  ORDER_YEAR,
				  SALES_ORDER_ID,
				  INK_COLOR,
				  INK_TECHNIQUE,
				  INK_TYPE,
				  INK_WEIGHT,
				  INK_WASTAGE,
				  H.COMPANY_ID,
				  H.LOCATION_ID";
		
		$join 	= "INNER JOIN ink_production_return_header H
				  		ON H.SERIAL_NO = ink_production_return_details.SERIAL_NO
      					AND H.SERIAL_YEAR = ink_production_return_details.SERIAL_YEAR";
						
		$where 	= "H.SERIAL_NO = $returnNo
    				AND H.SERIAL_YEAR = $returnYear";	
		return $this->select($cols,$join,$where,$order = null, $limit = null);	
	}
}
?>
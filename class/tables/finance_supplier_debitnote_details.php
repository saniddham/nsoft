<?php
class finance_supplier_debitnote_details{
 
	private $db;
	private $table= "finance_supplier_debitnote_details";
	
	//private property
	private $DEBIT_NO;
	private $DEBIT_YEAR;
	private $ITEM_ID;
	private $AMOUNT;
	private $TAX_AMOUNT;
	private $TAX_GROUP_ID;
	private $COST_CENTER_ID;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('DEBIT_NO'=>'DEBIT_NO',
										'DEBIT_YEAR'=>'DEBIT_YEAR',
										'ITEM_ID'=>'ITEM_ID',
										'AMOUNT'=>'AMOUNT',
										'TAX_AMOUNT'=>'TAX_AMOUNT',
										'TAX_GROUP_ID'=>'TAX_GROUP_ID',
										'COST_CENTER_ID'=>'COST_CENTER_ID',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "DEBIT_NO = ".$this->DEBIT_NO." and DEBIT_YEAR = ".$this->DEBIT_YEAR." and ITEM_ID = ".$this->ITEM_ID."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun DEBIT_NO
	function getDEBIT_NO()
	{
		$this->validate();
		return $this->DEBIT_NO;
	}
	
	//retun DEBIT_YEAR
	function getDEBIT_YEAR()
	{
		$this->validate();
		return $this->DEBIT_YEAR;
	}
	
	//retun ITEM_ID
	function getITEM_ID()
	{
		$this->validate();
		return $this->ITEM_ID;
	}
	
	//retun AMOUNT
	function getAMOUNT()
	{
		$this->validate();
		return $this->AMOUNT;
	}
	
	//retun TAX_AMOUNT
	function getTAX_AMOUNT()
	{
		$this->validate();
		return $this->TAX_AMOUNT;
	}
	
	//retun TAX_GROUP_ID
	function getTAX_GROUP_ID()
	{
		$this->validate();
		return $this->TAX_GROUP_ID;
	}
	
	//retun COST_CENTER_ID
	function getCOST_CENTER_ID()
	{
		$this->validate();
		return $this->COST_CENTER_ID;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set DEBIT_NO
	function setDEBIT_NO($DEBIT_NO)
	{
		array_push($this->commitArray,'DEBIT_NO');
		$this->DEBIT_NO = $DEBIT_NO;
	}
	
	//set DEBIT_YEAR
	function setDEBIT_YEAR($DEBIT_YEAR)
	{
		array_push($this->commitArray,'DEBIT_YEAR');
		$this->DEBIT_YEAR = $DEBIT_YEAR;
	}
	
	//set ITEM_ID
	function setITEM_ID($ITEM_ID)
	{
		array_push($this->commitArray,'ITEM_ID');
		$this->ITEM_ID = $ITEM_ID;
	}
	
	//set AMOUNT
	function setAMOUNT($AMOUNT)
	{
		array_push($this->commitArray,'AMOUNT');
		$this->AMOUNT = $AMOUNT;
	}
	
	//set TAX_AMOUNT
	function setTAX_AMOUNT($TAX_AMOUNT)
	{
		array_push($this->commitArray,'TAX_AMOUNT');
		$this->TAX_AMOUNT = $TAX_AMOUNT;
	}
	
	//set TAX_GROUP_ID
	function setTAX_GROUP_ID($TAX_GROUP_ID)
	{
		array_push($this->commitArray,'TAX_GROUP_ID');
		$this->TAX_GROUP_ID = $TAX_GROUP_ID;
	}
	
	//set COST_CENTER_ID
	function setCOST_CENTER_ID($COST_CENTER_ID)
	{
		array_push($this->commitArray,'COST_CENTER_ID');
		$this->COST_CENTER_ID = $COST_CENTER_ID;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->DEBIT_NO=='' || $this->DEBIT_YEAR=='' || $this->ITEM_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($DEBIT_NO , $DEBIT_YEAR , $ITEM_ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "DEBIT_NO='$DEBIT_NO' and DEBIT_YEAR='$DEBIT_YEAR' and ITEM_ID='$ITEM_ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($DEBIT_NO,$DEBIT_YEAR,$ITEM_ID,$AMOUNT,$TAX_AMOUNT,$TAX_GROUP_ID,$COST_CENTER_ID){
		$data = array('DEBIT_NO'=>$DEBIT_NO 
				,'DEBIT_YEAR'=>$DEBIT_YEAR 
				,'ITEM_ID'=>$ITEM_ID 
				,'AMOUNT'=>$AMOUNT 
				,'TAX_AMOUNT'=>$TAX_AMOUNT 
				,'TAX_GROUP_ID'=>$TAX_GROUP_ID 
				,'COST_CENTER_ID'=>$COST_CENTER_ID 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('DEBIT_NO,DEBIT_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['DEBIT_NO'])
				$html .= '<option selected="selected" value="'.$row['DEBIT_NO'].'">'.$row['DEBIT_YEAR'].'</option>';	
			else
				$html .= '<option value="'.$row['DEBIT_NO'].'">'.$row['DEBIT_YEAR'].'</option>';	
		}
		return $html;
	}
	public function getDebitNoteDetailData($debitNo,$debitYear)
	{
		$cols	= " mst_item.intMainCategory AS MAIN_CAT_ID,
					mst_item.intSubCategory  AS SUB_CAT_ID,
					mst_item.strName         AS ITEM_NAME,
					mst_item.intId           AS ITEM_ID,
					mst_item.intUOM          AS UNIT_ID,
					ROUND(PID.UNIT_PRICE,2)	 AS UNIT_PRICE,
					ROUND((PID.QTY*PID.UNIT_PRICE),2)+ROUND(PID.TAX_AMOUNT,2) 	AS INVOICE_AMOUNT,
					ROUND(finance_supplier_debitnote_details.AMOUNT,2) 			AS AMOUNT,
					finance_supplier_debitnote_details.TAX_GROUP_ID             AS TAX_CODE,
					finance_supplier_debitnote_details.TAX_AMOUNT		    	AS TAX_AMOUNT,
					finance_supplier_debitnote_details.COST_CENTER_ID           AS COST_CENTER,
					(SELECT
						SPGL.GLACCOUNT_ID
						FROM finance_supplier_purchaseinvoice_gl SPGL
						INNER JOIN finance_mst_chartofaccount_subcategory COAS
						ON COAS.CHART_OF_ACCOUNT_ID = SPGL.GLACCOUNT_ID
						WHERE SPGL.PURCHASE_INVOICE_NO = SDNH.PURCHASE_INVOICE_NO
						AND SPGL.PURCHASE_INVOICE_YEAR = SDNH.PURCHASE_INVOICE_YEAR
						AND COAS.SUBCATEGORY_ID = mst_item.intSubCategory
					) AS GLACCOUNT_ID ";
		
		$join	= " INNER JOIN finance_supplier_debitnote_header SDNH ON SDNH.DEBIT_NO=finance_supplier_debitnote_details.DEBIT_NO AND
					SDNH.DEBIT_YEAR = finance_supplier_debitnote_details.DEBIT_YEAR
					INNER JOIN mst_item ON mst_item.intId = finance_supplier_debitnote_details.ITEM_ID
					INNER JOIN finance_supplier_purchaseinvoice_details PID ON PID.PURCHASE_INVOICE_NO = SDNH.PURCHASE_INVOICE_NO AND
					PID.PURCHASE_INVOICE_YEAR = SDNH.PURCHASE_INVOICE_YEAR AND
					PID.ITEM_ID = finance_supplier_debitnote_details.ITEM_ID ";
		
		$where	= " finance_supplier_debitnote_details.DEBIT_NO = '".$debitNo."' AND
					finance_supplier_debitnote_details.DEBIT_YEAR = '".$debitYear."' ";
		
		$order	= " mst_item.strName";
		
		return $this->select($cols,$join,$where,$order,$limit=null);
	}
	public function getItemWiseDebitedAmount($invoiceNo,$invoiceYear,$itemId)
	{
		$cols	= " COALESCE(ROUND(SUM(AMOUNT+TAX_AMOUNT),2),0) AS debitedAmount ";
		
		$join	= " INNER JOIN finance_supplier_debitnote_header SDH ON SDH.DEBIT_NO = finance_supplier_debitnote_details.DEBIT_NO AND
					SDH.DEBIT_YEAR = finance_supplier_debitnote_details.DEBIT_YEAR ";
		
		$where	= " SDH.PURCHASE_INVOICE_NO = '".$invoiceNo."' AND
					SDH.PURCHASE_INVOICE_YEAR = '".$invoiceYear."' AND
					finance_supplier_debitnote_details.ITEM_ID = '".$itemId."' AND
					SDH.STATUS = 1 ";
		
		$result	= $this->select($cols,$join,$where);
		$row	= mysqli_fetch_array($result);
		
		return $row['debitedAmount'];
	}
	//END }
}
?>
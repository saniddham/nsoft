<?php
class mst_country{
 
	private $db;
	private $table= "mst_country";
	
	//private property
	private $intCountryID;
	private $strCountryCode;
	private $strCountryName;
	private $intStatus;
	private $intCreater;
	private $dtmCreateDate;
	private $intModifyer;
	private $dtmModifyDate;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('intCountryID'=>'intCountryID',
										'strCountryCode'=>'strCountryCode',
										'strCountryName'=>'strCountryName',
										'intStatus'=>'intStatus',
										'intCreater'=>'intCreater',
										'dtmCreateDate'=>'dtmCreateDate',
										'intModifyer'=>'intModifyer',
										'dtmModifyDate'=>'dtmModifyDate',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "intCountryID = ".$this->intCountryID."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun intCountryID
	function getintCountryID()
	{
		$this->validate();
		return $this->intCountryID;
	}
	
	//retun strCountryCode
	function getstrCountryCode()
	{
		$this->validate();
		return $this->strCountryCode;
	}
	
	//retun strCountryName
	function getstrCountryName()
	{
		$this->validate();
		return $this->strCountryName;
	}
	
	//retun intStatus
	function getintStatus()
	{
		$this->validate();
		return $this->intStatus;
	}
	
	//retun intCreater
	function getintCreater()
	{
		$this->validate();
		return $this->intCreater;
	}
	
	//retun dtmCreateDate
	function getdtmCreateDate()
	{
		$this->validate();
		return $this->dtmCreateDate;
	}
	
	//retun intModifyer
	function getintModifyer()
	{
		$this->validate();
		return $this->intModifyer;
	}
	
	//retun dtmModifyDate
	function getdtmModifyDate()
	{
		$this->validate();
		return $this->dtmModifyDate;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set intCountryID
	function setintCountryID($intCountryID)
	{
		array_push($this->commitArray,'intCountryID');
		$this->intCountryID = $intCountryID;
	}
	
	//set strCountryCode
	function setstrCountryCode($strCountryCode)
	{
		array_push($this->commitArray,'strCountryCode');
		$this->strCountryCode = $strCountryCode;
	}
	
	//set strCountryName
	function setstrCountryName($strCountryName)
	{
		array_push($this->commitArray,'strCountryName');
		$this->strCountryName = $strCountryName;
	}
	
	//set intStatus
	function setintStatus($intStatus)
	{
		array_push($this->commitArray,'intStatus');
		$this->intStatus = $intStatus;
	}
	
	//set intCreater
	function setintCreater($intCreater)
	{
		array_push($this->commitArray,'intCreater');
		$this->intCreater = $intCreater;
	}
	
	//set dtmCreateDate
	function setdtmCreateDate($dtmCreateDate)
	{
		array_push($this->commitArray,'dtmCreateDate');
		$this->dtmCreateDate = $dtmCreateDate;
	}
	
	//set intModifyer
	function setintModifyer($intModifyer)
	{
		array_push($this->commitArray,'intModifyer');
		$this->intModifyer = $intModifyer;
	}
	
	//set dtmModifyDate
	function setdtmModifyDate($dtmModifyDate)
	{
		array_push($this->commitArray,'dtmModifyDate');
		$this->dtmModifyDate = $dtmModifyDate;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->intCountryID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($intCountryID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "intCountryID='$intCountryID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($intCountryID,$strCountryCode,$strCountryName,$intStatus,$intCreater,$dtmCreateDate,$intModifyer,$dtmModifyDate){
		$data = array('intCountryID'=>$intCountryID 
				,'strCountryCode'=>$strCountryCode 
				,'strCountryName'=>$strCountryName 
				,'intStatus'=>$intStatus 
				,'intCreater'=>$intCreater 
				,'dtmCreateDate'=>$dtmCreateDate 
				,'intModifyer'=>$intModifyer 
				,'dtmModifyDate'=>$dtmModifyDate 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('intCountryID,strCountryName',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intCountryID'])
				$html .= '<option selected="selected" value="'.$row['intCountryID'].'">'.$row['strCountryName'].'</option>';	
			else
				$html .= '<option value="'.$row['intCountryID'].'">'.$row['strCountryName'].'</option>';	
		}
		return $html;
	}
	
	//END }
}
?>
<?php
class ink_color_room_requisition_header{
 
	private $db;
	private $table= "ink_color_room_requisition_header";
	
	//private property
	private $SERIAL_NO;
	private $SERIAL_YEAR;
	private $COMPANY_ID;
	private $LOCATION_ID;
	private $STATUS;
	private $APPROVE_LEVELS;
	private $CREATED_BY;
	private $CREATED_DATE;
	private $MODIFIED_BY;
	private $MODIFIED_DATE;
	private $STATUS_25;
	private $STATUS_100;
	private $APPROVE_LEVELS_25;
	private $APPROVE_LEVELS_100;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('SERIAL_NO'=>'SERIAL_NO',
										'SERIAL_YEAR'=>'SERIAL_YEAR',
										'COMPANY_ID'=>'COMPANY_ID',
										'LOCATION_ID'=>'LOCATION_ID',
										'STATUS'=>'STATUS',
										'APPROVE_LEVELS'=>'APPROVE_LEVELS',
										'CREATED_BY'=>'CREATED_BY',
										'CREATED_DATE'=>'CREATED_DATE',
										'MODIFIED_BY'=>'MODIFIED_BY',
										'MODIFIED_DATE'=>'MODIFIED_DATE',
										'STATUS_25'=>'STATUS_25',
										'STATUS_100'=>'STATUS_100',
										'APPROVE_LEVELS_25'=>'APPROVE_LEVELS_25',
										'APPROVE_LEVELS_100'=>'APPROVE_LEVELS_100',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	function commit()
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "SERIAL_NO = ".$this->SERIAL_NO." and SERIAL_YEAR = ".$this->SERIAL_YEAR."" ;
		unset($this->commitArray);
		
		if($this->SERIAL_NO==NULL || $this->SERIAL_NO=='' || $this->SERIAL_NO=='NULL')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun SERIAL_NO
	function getSERIAL_NO()
	{
		$this->validate();
		return $this->SERIAL_NO;
	}
	
	//retun SERIAL_YEAR
	function getSERIAL_YEAR()
	{
		$this->validate();
		return $this->SERIAL_YEAR;
	}
	
	//retun COMPANY_ID
	function getCOMPANY_ID()
	{
		$this->validate();
		return $this->COMPANY_ID;
	}
	
	//retun LOCATION_ID
	function getLOCATION_ID()
	{
		$this->validate();
		return $this->LOCATION_ID;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//retun APPROVE_LEVELS
	function getAPPROVE_LEVELS()
	{
		$this->validate();
		return $this->APPROVE_LEVELS;
	}
	
	//retun CREATED_BY
	function getCREATED_BY()
	{
		$this->validate();
		return $this->CREATED_BY;
	}
	
	//retun CREATED_DATE
	function getCREATED_DATE()
	{
		$this->validate();
		return $this->CREATED_DATE;
	}
	
	//retun MODIFIED_BY
	function getMODIFIED_BY()
	{
		$this->validate();
		return $this->MODIFIED_BY;
	}
	
	//retun MODIFIED_DATE
	function getMODIFIED_DATE()
	{
		$this->validate();
		return $this->MODIFIED_DATE;
	}
	
	//retun STATUS_25
	function getSTATUS_25()
	{
		$this->validate();
		return $this->STATUS_25;
	}
	
	//retun STATUS_100
	function getSTATUS_100()
	{
		$this->validate();
		return $this->STATUS_100;
	}
	
	//retun APPROVE_LEVELS_25
	function getAPPROVE_LEVELS_25()
	{
		$this->validate();
		return $this->APPROVE_LEVELS_25;
	}
	
	//retun APPROVE_LEVELS_100
	function getAPPROVE_LEVELS_100()
	{
		$this->validate();
		return $this->APPROVE_LEVELS_100;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set SERIAL_NO
	function setSERIAL_NO($SERIAL_NO)
	{
		array_push($this->commitArray,'SERIAL_NO');
		$this->SERIAL_NO = $SERIAL_NO;
	}
	
	//set SERIAL_YEAR
	function setSERIAL_YEAR($SERIAL_YEAR)
	{
		array_push($this->commitArray,'SERIAL_YEAR');
		$this->SERIAL_YEAR = $SERIAL_YEAR;
	}
	
	//set COMPANY_ID
	function setCOMPANY_ID($COMPANY_ID)
	{
		array_push($this->commitArray,'COMPANY_ID');
		$this->COMPANY_ID = $COMPANY_ID;
	}
	
	//set LOCATION_ID
	function setLOCATION_ID($LOCATION_ID)
	{
		array_push($this->commitArray,'LOCATION_ID');
		$this->LOCATION_ID = $LOCATION_ID;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//set APPROVE_LEVELS
	function setAPPROVE_LEVELS($APPROVE_LEVELS)
	{
		array_push($this->commitArray,'APPROVE_LEVELS');
		$this->APPROVE_LEVELS = $APPROVE_LEVELS;
	}
	
	//set CREATED_BY
	function setCREATED_BY($CREATED_BY)
	{
		array_push($this->commitArray,'CREATED_BY');
		$this->CREATED_BY = $CREATED_BY;
	}
	
	//set CREATED_DATE
	function setCREATED_DATE($CREATED_DATE)
	{
		array_push($this->commitArray,'CREATED_DATE');
		$this->CREATED_DATE = $CREATED_DATE;
	}
	
	//set MODIFIED_BY
	function setMODIFIED_BY($MODIFIED_BY)
	{
		array_push($this->commitArray,'MODIFIED_BY');
		$this->MODIFIED_BY = $MODIFIED_BY;
	}
	
	//set MODIFIED_DATE
	function setMODIFIED_DATE($MODIFIED_DATE)
	{
		array_push($this->commitArray,'MODIFIED_DATE');
		$this->MODIFIED_DATE = $MODIFIED_DATE;
	}
	
	//set STATUS_25
	function setSTATUS_25($STATUS_25)
	{
		array_push($this->commitArray,'STATUS_25');
		$this->STATUS_25 = $STATUS_25;
	}
	
	//set STATUS_100
	function setSTATUS_100($STATUS_100)
	{
		array_push($this->commitArray,'STATUS_100');
		$this->STATUS_100 = $STATUS_100;
	}
	
	//set APPROVE_LEVELS_25
	function setAPPROVE_LEVELS_25($APPROVE_LEVELS_25)
	{
		array_push($this->commitArray,'APPROVE_LEVELS_25');
		$this->APPROVE_LEVELS_25 = $APPROVE_LEVELS_25;
	}
	
	//set APPROVE_LEVELS_100
	function setAPPROVE_LEVELS_100($APPROVE_LEVELS_100)
	{
		array_push($this->commitArray,'APPROVE_LEVELS_100');
		$this->APPROVE_LEVELS_100 = $APPROVE_LEVELS_100;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->SERIAL_NO=='' || $this->SERIAL_YEAR=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($SERIAL_NO , $SERIAL_YEAR)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "SERIAL_NO='$SERIAL_NO' and SERIAL_YEAR='$SERIAL_YEAR'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($SERIAL_NO,$SERIAL_YEAR,$COMPANY_ID,$LOCATION_ID,$STATUS,$APPROVE_LEVELS,$CREATED_BY,$CREATED_DATE,$MODIFIED_BY,$MODIFIED_DATE,$STATUS_25,$STATUS_100,$APPROVE_LEVELS_25,$APPROVE_LEVELS_100){
		$data = array('SERIAL_NO'=>$SERIAL_NO 
				,'SERIAL_YEAR'=>$SERIAL_YEAR 
				,'COMPANY_ID'=>$COMPANY_ID 
				,'LOCATION_ID'=>$LOCATION_ID 
				,'STATUS'=>$STATUS 
				,'APPROVE_LEVELS'=>$APPROVE_LEVELS 
				,'CREATED_BY'=>$CREATED_BY 
				,'CREATED_DATE'=>$CREATED_DATE 
				,'MODIFIED_BY'=>$MODIFIED_BY 
				,'MODIFIED_DATE'=>$MODIFIED_DATE 
				,'STATUS_25'=>$STATUS_25 
				,'STATUS_100'=>$STATUS_100 
				,'APPROVE_LEVELS_25'=>$APPROVE_LEVELS_25 
				,'APPROVE_LEVELS_100'=>$APPROVE_LEVELS_100 
				);
		return $this->insert($data);
	}
	
	/*public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('SERIAL_NO,SERIAL_YEAR',  null, $where = $where);
		while($row=mysqli_fetch_array($result)){
			$html .= '<option value="'.$row['SERIAL_NO'].'">'.$row['SERIAL_YEAR'].'</option>';	
		}
		return $html;
	}*/
	public function getCombo($valField,$textField,$defaultValue=null,$where=null){
		
		//if($valField == $textField)
		$result = $this->select("$valField as val,$textField as text",NULL, $where = $where,NULL,NULL);
		$html = '<option value=""></option>';
		while($row = mysqli_fetch_array($result)){
			if($defaultValue != NULL && $defaultValue==$row['val']){
				$html .= '<option value="'.$row['val'].'" selected="selected">'.$row['text'].'</option>';	
			}
			else
				$html .= '<option value="'.$row['val'].'">'.$row['text'].'</option>';

		}
		return $html;
	}
	
	public function loadCRNCombo($location,$reqYear)
	{	
		$sql	= " SELECT
					ink_color_room_requisition_header.SERIAL_NO,
					ink_color_room_requisition_header.SERIAL_YEAR,
					IFNULL(SUM(ink_color_room_requisition_details.QTY),0) AS REQ_QTY,
					IFNULL(SUM(ink_color_room_requisition_extra.QTY),0) AS REQ_EXTRA_QTY,
					(SELECT
					IFNULL(SUM(ink_color_room_issue_details.QTY),0)
					FROM
					ink_color_room_issue_details
					INNER JOIN ink_color_room_issue_header ON ink_color_room_issue_header.SERIAL_NO = ink_color_room_issue_details.SERIAL_NO AND ink_color_room_issue_header.SERIAL_YEAR = ink_color_room_issue_details.SERIAL_YEAR
					WHERE
					ink_color_room_issue_header.REQUISITION_NO = ink_color_room_requisition_header.SERIAL_NO AND
					ink_color_room_issue_header.REQUISITION_YEAR = ink_color_room_requisition_header.SERIAL_YEAR AND
					ink_color_room_issue_details.ITEM_ID = ink_color_room_requisition_details.ITEM_ID AND 
					ink_color_room_issue_header.STATUS = 1
					) AS ISSUE_QTY,
					(SELECT
					IFNULL(SUM(ink_color_room_issue_extra.QTY),0)
					FROM
					ink_color_room_issue_extra
					INNER JOIN ink_color_room_issue_header ON ink_color_room_issue_extra.SERIAL_NO = ink_color_room_issue_header.SERIAL_NO AND ink_color_room_issue_extra.SERIAL_YEAR = ink_color_room_issue_header.SERIAL_YEAR
					WHERE
					ink_color_room_issue_header.REQUISITION_NO = ink_color_room_requisition_header.SERIAL_NO AND
					ink_color_room_issue_header.REQUISITION_YEAR = ink_color_room_requisition_header.SERIAL_YEAR AND
					ink_color_room_issue_extra.ITEM_ID = ink_color_room_requisition_extra.ITEM_ID AND
					ink_color_room_issue_header.STATUS = 1
					) AS EXTRA_ISSUE_QTY 
					FROM
					ink_color_room_requisition_header
					INNER JOIN ink_color_room_requisition_details ON ink_color_room_requisition_header.SERIAL_NO = ink_color_room_requisition_details.SERIAL_NO AND ink_color_room_requisition_header.SERIAL_YEAR = ink_color_room_requisition_details.SERIAL_YEAR
					LEFT JOIN ink_color_room_requisition_extra ON ink_color_room_requisition_header.SERIAL_NO = ink_color_room_requisition_extra.SERIAL_NO AND ink_color_room_requisition_header.SERIAL_YEAR = ink_color_room_requisition_extra.SERIAL_YEAR
					WHERE
					ink_color_room_requisition_header.`STATUS` = 1 AND 
					ink_color_room_requisition_header.LOCATION_ID = '$location'";
				if($reqYear != '')
				$sql	.=" AND ink_color_room_requisition_header.SERIAL_YEAR = '$reqYear'";
					
				$sql	.=	" GROUP BY
					ink_color_room_requisition_header.SERIAL_NO,
					ink_color_room_requisition_header.SERIAL_YEAR
					HAVING
					((REQ_QTY+REQ_EXTRA_QTY)-(ISSUE_QTY+EXTRA_ISSUE_QTY))>0
					ORDER BY ink_color_room_requisition_header.SERIAL_NO DESC";	
					
		$result	= $this->db->RunQuery($sql);
		return $result;
	}
	
	//END }
}
?>
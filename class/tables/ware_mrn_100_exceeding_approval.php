<?php
class ware_mrn_100_exceeding_approval{
 
	private $db;
	private $table= "ware_mrn_100_exceeding_approval";
	
	//private property
	private $intMrnNo;
	private $intYear;
	private $intApproveLevelNo;
	private $intApproveUser;
	private $dtApprovedDate;
	private $intStatus;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('intMrnNo'=>'intMrnNo',
										'intYear'=>'intYear',
										'intApproveLevelNo'=>'intApproveLevelNo',
										'intApproveUser'=>'intApproveUser',
										'dtApprovedDate'=>'dtApprovedDate',
										'intStatus'=>'intStatus',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "intMrnNo = ".$this->intMrnNo." and intYear = ".$this->intYear." and intApproveLevelNo = ".$this->intApproveLevelNo." and intStatus = ".$this->intStatus."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun intMrnNo
	function getintMrnNo()
	{
		$this->validate();
		return $this->intMrnNo;
	}
	
	//retun intYear
	function getintYear()
	{
		$this->validate();
		return $this->intYear;
	}
	
	//retun intApproveLevelNo
	function getintApproveLevelNo()
	{
		$this->validate();
		return $this->intApproveLevelNo;
	}
	
	//retun intApproveUser
	function getintApproveUser()
	{
		$this->validate();
		return $this->intApproveUser;
	}
	
	//retun dtApprovedDate
	function getdtApprovedDate()
	{
		$this->validate();
		return $this->dtApprovedDate;
	}
	
	//retun intStatus
	function getintStatus()
	{
		$this->validate();
		return $this->intStatus;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set intMrnNo
	function setintMrnNo($intMrnNo)
	{
		array_push($this->commitArray,'intMrnNo');
		$this->intMrnNo = $intMrnNo;
	}
	
	//set intYear
	function setintYear($intYear)
	{
		array_push($this->commitArray,'intYear');
		$this->intYear = $intYear;
	}
	
	//set intApproveLevelNo
	function setintApproveLevelNo($intApproveLevelNo)
	{
		array_push($this->commitArray,'intApproveLevelNo');
		$this->intApproveLevelNo = $intApproveLevelNo;
	}
	
	//set intApproveUser
	function setintApproveUser($intApproveUser)
	{
		array_push($this->commitArray,'intApproveUser');
		$this->intApproveUser = $intApproveUser;
	}
	
	//set dtApprovedDate
	function setdtApprovedDate($dtApprovedDate)
	{
		array_push($this->commitArray,'dtApprovedDate');
		$this->dtApprovedDate = $dtApprovedDate;
	}
	
	//set intStatus
	function setintStatus($intStatus)
	{
		array_push($this->commitArray,'intStatus');
		$this->intStatus = $intStatus;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->intMrnNo=='' || $this->intYear=='' || $this->intApproveLevelNo=='' || $this->intStatus=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($intMrnNo , $intYear , $intApproveLevelNo , $intStatus)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "intMrnNo='$intMrnNo' and intYear='$intYear' and intApproveLevelNo='$intApproveLevelNo' and intStatus='$intStatus'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($intMrnNo,$intYear,$intApproveLevelNo,$intApproveUser,$dtApprovedDate,$intStatus){
		$data = array('intMrnNo'=>$intMrnNo 
				,'intYear'=>$intYear 
				,'intApproveLevelNo'=>$intApproveLevelNo 
				,'intApproveUser'=>$intApproveUser 
				,'dtApprovedDate'=>$dtApprovedDate 
				,'intStatus'=>$intStatus 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('intMrnNo,intYear',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intMrnNo'])
				$html .= '<option selected="selected" value="'.$row['intMrnNo'].'">'.$row['intYear'].'</option>';	
			else
				$html .= '<option value="'.$row['intMrnNo'].'">'.$row['intYear'].'</option>';	
		}
		return $html;
	}
	
	public function updateMaxStatus($serialNo,$serialYear)
	{
		$cols		= " MAX(intStatus) AS MAX_STATUS ";
		$where		= " intMrnNo = '".$serialNo."' AND
						intYear = '".$serialYear."' ";
	
		$result 	= $this->select($cols,$join = null,$where, $order = null, $limit = null);
		$row		= mysqli_fetch_array($result);
		
		$maxStatus	= $row['MAX_STATUS']+1;
	
		$data  		= array('STATUS' => $maxStatus);
		
		$where		= ' intMrnNo = "'.$serialNo.'" AND 
						intYear = "'.$serialYear.'" AND
						intStatus = 0 ' ;
						
		$result_arr	= $this->update($data,$where);
		if(!$result_arr['status'])
			throw new Exception($result_arr['msg']);
	}
	
	public function getApproveByData($serialNo,$serialYear)
	{
		$cols			= " dtApprovedDate AS dtApprovedDate,
							SU.strUserName AS UserName,
							intApproveLevelNo AS intApproveLevelNo ";
						
		$join 			= " INNER JOIN sys_users SU ON intApproveUser = SU.intUserId ";
		
		$where			= " intMrnNo = '$serialNo' AND
							intYear = '$serialYear' ";	
		
		$order			= " dtApprovedDate ASC ";
											
		$result			= $this->select($cols,$join,$where,$order,NULL);
		
		return $result;
	}
	//END }
}
?>
<?php
class trn_orderheader{

	private $db;
	private $table= "trn_orderheader";

	//private property
	private $intOrderNo;
	private $intOrderYear;
	private $strCustomerPoNo;
	private $dtDate;
	private $dtDeliveryDate;
	private $intCustomer;
	private $intCustomerLocation;
	private $intCurrency;
	private $intPaymentTerm;
	private $strContactPerson;
	private $strRemark;
	private $REJECT_REASON;
	private $intMarketer;
	private $intStatus;
	private $intCreator;
	private $dtmCreateDate;
	private $intModifyer;
	private $dtmModifyDate;
	private $intApproveLevelStart;
	private $intLocationId;
	private $intAllocatedStatus;
	private $intReviseNo;
	private $intStatusOld;
	private $PAYMENT_RECEIVE_COMPLETED_FLAG;
	private $LC_STATUS;
	private $TECHNIQUE_TYPE;
	private $commitArray = array();
	private $field_array = array();
	

	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('intOrderNo'=>'intOrderNo',
										'intOrderYear'=>'intOrderYear',
										'strCustomerPoNo'=>'strCustomerPoNo',
										'dtDate'=>'dtDate',
										'dtDeliveryDate'=>'dtDeliveryDate',
										'intCustomer'=>'intCustomer',
										'intCustomerLocation'=>'intCustomerLocation',
										'intCurrency'=>'intCurrency',
										'intPaymentTerm'=>'intPaymentTerm',
										'strContactPerson'=>'strContactPerson',
										'strRemark'=>'strRemark',
										'REJECT_REASON'=>'REJECT_REASON',
										'intMarketer'=>'intMarketer',
										'intStatus'=>'intStatus',
										'intCreator'=>'intCreator',
										'dtmCreateDate'=>'dtmCreateDate',
										'intModifyer'=>'intModifyer',
										'dtmModifyDate'=>'dtmModifyDate',
										'intApproveLevelStart'=>'intApproveLevelStart',
										'intLocationId'=>'intLocationId',
										'intAllocatedStatus'=>'intAllocatedStatus',
										'intReviseNo'=>'intReviseNo',
										'intStatusOld'=>'intStatusOld',
										'PAYMENT_RECEIVE_COMPLETED_FLAG'=>'PAYMENT_RECEIVE_COMPLETED_FLAG',
										'LC_STATUS'=>'LC_STATUS',
										'TECHNIQUE_TYPE'=>'TECHNIQUE_TYPE',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit );
		return $result;
	}

	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data);
		return $this->db->getResult();


	}

	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}

	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();
	}

	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}

	public function get_field_array(){
		return $this->field_array;
	}

	function commit($type='update')
	{
		//commit update
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{
			$data[$v] = $this->$v;
		}
		$where		= "intOrderNo = ".$this->intOrderNo." and intOrderYear = ".$this->intOrderYear."" ;
		$this->commitArray = array();

		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}

	//BEGIN - public functions for private properties {
	
	//retun intOrderNo
	function getintOrderNo()
	{
		$this->validate();
		return $this->intOrderNo;
	}
	
	//retun intOrderYear
	function getintOrderYear()
	{
		$this->validate();
		return $this->intOrderYear;
	}
	
	//retun strCustomerPoNo
	function getstrCustomerPoNo()
	{
		$this->validate();
		return $this->strCustomerPoNo;
	}
	
	//retun dtDate
	function getdtDate()
	{
		$this->validate();
		return $this->dtDate;
	}
	
	//retun dtDeliveryDate
	function getdtDeliveryDate()
	{
		$this->validate();
		return $this->dtDeliveryDate;
	}
	
	//retun intCustomer
	function getintCustomer()
	{
		$this->validate();
		return $this->intCustomer;
	}
	
	//retun intCustomerLocation
	function getintCustomerLocation()
	{
		$this->validate();
		return $this->intCustomerLocation;
	}
	
	//retun intCurrency
	function getintCurrency()
	{
		$this->validate();
		return $this->intCurrency;
	}
	
	//retun intPaymentTerm
	function getintPaymentTerm()
	{
		$this->validate();
		return $this->intPaymentTerm;
	}
	
	//retun strContactPerson
	function getstrContactPerson()
	{
		$this->validate();
		return $this->strContactPerson;
	}
	
	//retun strRemark
	function getstrRemark()
	{
		$this->validate();
		return $this->strRemark;
	}
	
	//retun REJECT_REASON
	function getREJECT_REASON()
	{
		$this->validate();
		return $this->REJECT_REASON;
	}
	
	//retun intMarketer
	function getintMarketer()
	{
		$this->validate();
		return $this->intMarketer;
	}
	
	//retun intStatus
	function getintStatus()
	{
		$this->validate();
		return $this->intStatus;
	}
	
	//retun intCreator
	function getintCreator()
	{
		$this->validate();
		return $this->intCreator;
	}
	
	//retun dtmCreateDate
	function getdtmCreateDate()
	{
		$this->validate();
		return $this->dtmCreateDate;
	}
	
	//retun intModifyer
	function getintModifyer()
	{
		$this->validate();
		return $this->intModifyer;
	}
	
	//retun dtmModifyDate
	function getdtmModifyDate()
	{
		$this->validate();
		return $this->dtmModifyDate;
	}
	
	//retun intApproveLevelStart
	function getintApproveLevelStart()
	{
		$this->validate();
		return $this->intApproveLevelStart;
	}
	
	//retun intLocationId
	function getintLocationId()
	{
		$this->validate();
		return $this->intLocationId;
	}
	
	//retun intAllocatedStatus
	function getintAllocatedStatus()
	{
		$this->validate();
		return $this->intAllocatedStatus;
	}
	
	//retun intReviseNo
	function getintReviseNo()
	{
		$this->validate();
		return $this->intReviseNo;
	}
	
	//retun intStatusOld
	function getintStatusOld()
	{
		$this->validate();
		return $this->intStatusOld;
	}
	
	//retun PAYMENT_RECEIVE_COMPLETED_FLAG
	function getPAYMENT_RECEIVE_COMPLETED_FLAG()
	{
		$this->validate();
		return $this->PAYMENT_RECEIVE_COMPLETED_FLAG;
	}
	
	//retun LC_STATUS
	function getLC_STATUS()
	{
		$this->validate();
		return $this->LC_STATUS;
	}
	
	//retun TECHNIQUE_TYPE
	function getTECHNIQUE_TYPE()
	{
		$this->validate();
		return $this->TECHNIQUE_TYPE;
	}
	
	//END }

	//BEGIN - public function set {
	
	//set intOrderNo
	function setintOrderNo($intOrderNo)
	{
		array_push($this->commitArray,'intOrderNo');
		$this->intOrderNo = $intOrderNo;
	}
	
	//set intOrderYear
	function setintOrderYear($intOrderYear)
	{
		array_push($this->commitArray,'intOrderYear');
		$this->intOrderYear = $intOrderYear;
	}
	
	//set strCustomerPoNo
	function setstrCustomerPoNo($strCustomerPoNo)
	{
		array_push($this->commitArray,'strCustomerPoNo');
		$this->strCustomerPoNo = $strCustomerPoNo;
	}
	
	//set dtDate
	function setdtDate($dtDate)
	{
		array_push($this->commitArray,'dtDate');
		$this->dtDate = $dtDate;
	}
	
	//set dtDeliveryDate
	function setdtDeliveryDate($dtDeliveryDate)
	{
		array_push($this->commitArray,'dtDeliveryDate');
		$this->dtDeliveryDate = $dtDeliveryDate;
	}
	
	//set intCustomer
	function setintCustomer($intCustomer)
	{
		array_push($this->commitArray,'intCustomer');
		$this->intCustomer = $intCustomer;
	}
	
	//set intCustomerLocation
	function setintCustomerLocation($intCustomerLocation)
	{
		array_push($this->commitArray,'intCustomerLocation');
		$this->intCustomerLocation = $intCustomerLocation;
	}
	
	//set intCurrency
	function setintCurrency($intCurrency)
	{
		array_push($this->commitArray,'intCurrency');
		$this->intCurrency = $intCurrency;
	}
	
	//set intPaymentTerm
	function setintPaymentTerm($intPaymentTerm)
	{
		array_push($this->commitArray,'intPaymentTerm');
		$this->intPaymentTerm = $intPaymentTerm;
	}
	
	//set strContactPerson
	function setstrContactPerson($strContactPerson)
	{
		array_push($this->commitArray,'strContactPerson');
		$this->strContactPerson = $strContactPerson;
	}
	
	//set strRemark
	function setstrRemark($strRemark)
	{
		array_push($this->commitArray,'strRemark');
		$this->strRemark = $strRemark;
	}
	
	//set REJECT_REASON
	function setREJECT_REASON($REJECT_REASON)
	{
		array_push($this->commitArray,'REJECT_REASON');
		$this->REJECT_REASON = $REJECT_REASON;
	}
	
	//set intMarketer
	function setintMarketer($intMarketer)
	{
		array_push($this->commitArray,'intMarketer');
		$this->intMarketer = $intMarketer;
	}
	
	//set intStatus
	function setintStatus($intStatus)
	{
		array_push($this->commitArray,'intStatus');
		$this->intStatus = $intStatus;
	}
	
	//set intCreator
	function setintCreator($intCreator)
	{
		array_push($this->commitArray,'intCreator');
		$this->intCreator = $intCreator;
	}
	
	//set dtmCreateDate
	function setdtmCreateDate($dtmCreateDate)
	{
		array_push($this->commitArray,'dtmCreateDate');
		$this->dtmCreateDate = $dtmCreateDate;
	}
	
	//set intModifyer
	function setintModifyer($intModifyer)
	{
		array_push($this->commitArray,'intModifyer');
		$this->intModifyer = $intModifyer;
	}
	
	//set dtmModifyDate
	function setdtmModifyDate($dtmModifyDate)
	{
		array_push($this->commitArray,'dtmModifyDate');
		$this->dtmModifyDate = $dtmModifyDate;
	}
	
	//set intApproveLevelStart
	function setintApproveLevelStart($intApproveLevelStart)
	{
		array_push($this->commitArray,'intApproveLevelStart');
		$this->intApproveLevelStart = $intApproveLevelStart;
	}
	
	//set intLocationId
	function setintLocationId($intLocationId)
	{
		array_push($this->commitArray,'intLocationId');
		$this->intLocationId = $intLocationId;
	}
	
	//set intAllocatedStatus
	function setintAllocatedStatus($intAllocatedStatus)
	{
		array_push($this->commitArray,'intAllocatedStatus');
		$this->intAllocatedStatus = $intAllocatedStatus;
	}
	
	//set intReviseNo
	function setintReviseNo($intReviseNo)
	{
		array_push($this->commitArray,'intReviseNo');
		$this->intReviseNo = $intReviseNo;
	}
	
	//set intStatusOld
	function setintStatusOld($intStatusOld)
	{
		array_push($this->commitArray,'intStatusOld');
		$this->intStatusOld = $intStatusOld;
	}
	
	//set PAYMENT_RECEIVE_COMPLETED_FLAG
	function setPAYMENT_RECEIVE_COMPLETED_FLAG($PAYMENT_RECEIVE_COMPLETED_FLAG)
	{
		array_push($this->commitArray,'PAYMENT_RECEIVE_COMPLETED_FLAG');
		$this->PAYMENT_RECEIVE_COMPLETED_FLAG = $PAYMENT_RECEIVE_COMPLETED_FLAG;
	}
	
	//set LC_STATUS
	function setLC_STATUS($LC_STATUS)
	{
		array_push($this->commitArray,'LC_STATUS');
		$this->LC_STATUS = $LC_STATUS;
	}
	
	//set TECHNIQUE_TYPE
	function setTECHNIQUE_TYPE($TECHNIQUE_TYPE)
	{
		array_push($this->commitArray,'TECHNIQUE_TYPE');
		$this->TECHNIQUE_TYPE = $TECHNIQUE_TYPE;
	}
	
	//END }

	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->intOrderNo=='' || $this->intOrderYear=='')
			throw new exception("Set primary values first");
		else
			return true;
	}

	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}

	public function set($intOrderNo , $intOrderYear)
	{
		$cols	= "*";

		$join	= NULL;

		$where	= "intOrderNo='$intOrderNo' and intOrderYear='$intOrderYear'";

		$result = $this->select($cols,$join,$where);
		if($this->db->numRows()>0)
			return $this->setVariables($result);
		else
			throw new Exception("Record not found.");
	}

	//insert as parameters
	public function insertRec($intOrderNo,$intOrderYear,$strCustomerPoNo,$dtDate,$dtDeliveryDate,$intCustomer,$intCustomerLocation,$intCurrency,$intPaymentTerm,$strContactPerson,$strRemark,$REJECT_REASON,$intMarketer,$intStatus,$intCreator,$dtmCreateDate,$intModifyer,$dtmModifyDate,$intApproveLevelStart,$intLocationId,$intAllocatedStatus,$intReviseNo,$intStatusOld,$PAYMENT_RECEIVE_COMPLETED_FLAG,$LC_STATUS,$TECHNIQUE_TYPE){
		$data = array('intOrderNo'=>$intOrderNo 
				,'intOrderYear'=>$intOrderYear 
				,'strCustomerPoNo'=>$strCustomerPoNo 
				,'dtDate'=>$dtDate 
				,'dtDeliveryDate'=>$dtDeliveryDate 
				,'intCustomer'=>$intCustomer 
				,'intCustomerLocation'=>$intCustomerLocation 
				,'intCurrency'=>$intCurrency 
				,'intPaymentTerm'=>$intPaymentTerm 
				,'strContactPerson'=>$strContactPerson 
				,'strRemark'=>$strRemark 
				,'REJECT_REASON'=>$REJECT_REASON 
				,'intMarketer'=>$intMarketer 
				,'intStatus'=>$intStatus 
				,'intCreator'=>$intCreator 
				,'dtmCreateDate'=>$dtmCreateDate 
				,'intModifyer'=>$intModifyer 
				,'dtmModifyDate'=>$dtmModifyDate 
				,'intApproveLevelStart'=>$intApproveLevelStart 
				,'intLocationId'=>$intLocationId 
				,'intAllocatedStatus'=>$intAllocatedStatus 
				,'intReviseNo'=>$intReviseNo 
				,'intStatusOld'=>$intStatusOld 
				,'PAYMENT_RECEIVE_COMPLETED_FLAG'=>$PAYMENT_RECEIVE_COMPLETED_FLAG 
				,'LC_STATUS'=>$LC_STATUS 
				,'TECHNIQUE_TYPE'=>$TECHNIQUE_TYPE 
				);
		return $this->insert($data);
	}

	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('intOrderNo,intOrderYear',  null, $where = $where);
		$html = '<option value=""></option>';
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intOrderNo'])
				$html .= '<option selected="selected" value="'.$row['intOrderNo'].'">'.$row['intOrderYear'].'</option>';
			else
				$html .= '<option value="'.$row['intOrderNo'].'">'.$row['intOrderYear'].'</option>';
		}
		return $html;
	}
	public function getCustomerCombo($defaultValue=null,$where=null)
	{
		$cols	= " DISTINCT CU.intId 		AS CUSTOMER_ID,
					CU.strName 				AS CUSTOMER_NAME";
		
		$joins	= " INNER JOIN mst_customer CU ON trn_orderheader.intCustomer = CU.intId ";
		
		$order	= " CU.strName ";
		
		$result = $this->select($cols, $joins, $where = $where, $order);
		$html 	= '<option value=""></option>';
		while($row=mysqli_fetch_array($result))
		{
			if($defaultValue==$row['CUSTOMER_ID'])
				$html .= '<option selected="selected" value="'.$row['CUSTOMER_ID'].'">'.$row['CUSTOMER_NAME'].'</option>';
			else
				$html .= '<option value="'.$row['CUSTOMER_ID'].'">'.$row['CUSTOMER_NAME'].'</option>';
		}
		return $html;
	}
	
	public function getOrderYear_for_sales_order_completion($defaultValue=null)
	{
		$result_order_year	= $this->select($cols = 'DISTINCT intOrderYear ', NULL, 'intStatus =1', 'intOrderYear DESC', NULL);
		$combo_order	='<option value=""></option>';
		
		while($row=mysqli_fetch_array($result_order_year)){
			if($defaultValue==$row['intOrderYear'])
				$combo_order .= '<option selected="selected" value="'.$row['intOrderYear'].'">'.$row['intOrderYear'].'</option>';
			else
				$combo_order .= '<option value="'.$row['intOrderYear'].'">'.$row['intOrderYear'].'</option>';
		}
		
		return $combo_order;
	}
	public function getOrderNo_for_sales_order_completion($defaultValue=null)
	{
		$result_order_no	= $this->select($cols = 'DISTINCT intOrderNo ', NULL, 'intStatus =1', 'intOrderNo ASC', NULL);
		$combo_order	='<option value=""></option>';
		
		while($row=mysqli_fetch_array($result_order_no)){
			if($defaultValue==$row['intOrderNo'])
				$combo_order .= '<option selected="selected" value="'.$row['intOrderNo'].'">'.$row['intOrderNo'].'</option>';
			else
				$combo_order .= '<option value="'.$row['intOrderNo'].'">'.$row['intOrderNo'].'</option>';
		}
		
		return $combo_order;
	}

	
	//END }
}
?>
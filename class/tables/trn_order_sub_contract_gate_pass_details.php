
<?php

class trn_order_sub_contract_gate_pass_details{
 
	private $db;
	private $table= "trn_order_sub_contract_gate_pass_details";
	
	//private property
	private $SUB_CONTRACT_GP_NO;
	private $SUB_CONTRACT_GP_YEAR;
	private $SALES_ORDER_ID;
	private $SUB_CONTR_JOB_ID;
	private $CUT_NO;
	private $SIZE;
	private $QTY;
	private $BAL_TO_RETURN_QTY;
	private $strSalesOrderNo;
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->db->field_array_set(array('SUB_CONTRACT_GP_NO'=>'SUB_CONTRACT_GP_NO',
										'SUB_CONTRACT_GP_YEAR'=>'SUB_CONTRACT_GP_YEAR',
										'SALES_ORDER_ID'=>'SALES_ORDER_ID',
										'SUB_CONTR_JOB_ID'=>'SUB_CONTR_JOB_ID',
										'CUT_NO'=>'CUT_NO',
										'SIZE'=>'SIZE',
										'QTY'=>'QTY',
										'BAL_TO_RETURN_QTY'=>'BAL_TO_RETURN_QTY',
										));
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  
		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	//public functions for private properties
	
	//retun SUB_CONTRACT_GP_NO
	function getSUB_CONTRACT_GP_NO()
	{
		$this->validate();
		return $this->SUB_CONTRACT_GP_NO;
	}
	
	//retun SUB_CONTRACT_GP_YEAR
	function getSUB_CONTRACT_GP_YEAR()
	{
		$this->validate();
		return $this->SUB_CONTRACT_GP_YEAR;
	}
	
	//retun SALES_ORDER_ID
	function getSALES_ORDER_ID()
	{
		$this->validate();
		return $this->SALES_ORDER_ID;
	}
	
	//retun SUB_CONTR_JOB_ID
	function getSUB_CONTR_JOB_ID()
	{
		$this->validate();
		return $this->SUB_CONTR_JOB_ID;
	}
	
	//retun CUT_NO
	function getCUT_NO()
	{
		$this->validate();
		return $this->CUT_NO;
	}
	
	//retun SIZE
	function getSIZE()
	{
		$this->validate();
		return $this->SIZE;
	}
	
	//retun QTY
	function getQTY()
	{
		$this->validate();
		return $this->QTY;
	}
	
	//retun BAL_TO_RETURN_QTY
	function getBAL_TO_RETURN_QTY()
	{
		$this->validate();
		return $this->BAL_TO_RETURN_QTY;
	}
	
	//retun strSalesOrderNo
	function getstrSalesOrderNo()
	{
		$this->validate();
		return $this->strSalesOrderNo;
	}
	
	//validate primary values
	private function validate()
	{
		if($this->SUB_CONTRACT_GP_NO=='' || $this->SUB_CONTRACT_GP_YEAR=='' || $this->SALES_ORDER_ID=='' || $this->CUT_NO=='' || $this->SIZE=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
		}
	}
	
	public function set($SUB_CONTRACT_GP_NO , $SUB_CONTRACT_GP_YEAR , $SALES_ORDER_ID , $CUT_NO , $SIZE)
	{
		$cols	= " trn_order_sub_contract_gate_pass_details.SUB_CONTRACT_GP_NO, 
					trn_order_sub_contract_gate_pass_details.SUB_CONTRACT_GP_YEAR, 
					trn_order_sub_contract_gate_pass_details.SALES_ORDER_ID, 
					trn_order_sub_contract_gate_pass_details.SUB_CONTR_JOB_ID, 
					trn_order_sub_contract_gate_pass_details.CUT_NO, 
					trn_order_sub_contract_gate_pass_details.SIZE, 
					trn_order_sub_contract_gate_pass_details.QTY, 
					trn_order_sub_contract_gate_pass_details.BAL_TO_RETURN_QTY,
					OD.strSalesOrderNo ";
		
		$join	= " INNER JOIN trn_order_sub_contract_gate_pass_header SCGPH ON SCGPH.SUB_CONTRACT_GP_NO=trn_order_sub_contract_gate_pass_details.SUB_CONTRACT_GP_NO AND
					SCGPH.SUB_CONTRACT_GP_YEAR=trn_order_sub_contract_gate_pass_details.SUB_CONTRACT_GP_YEAR
					INNER JOIN trn_order_sub_contract_header SCH ON SCH.SUB_CONTRACT_NO=SCGPH.SUB_CONTRACT_NO AND
					SCH.SUB_CONTRACT_YEAR=SCGPH.SUB_CONTRACT_YEAR
					INNER JOIN trn_orderdetails OD ON OD.intOrderNo=SCH.ORDER_NO AND
					OD.intOrderYear=SCH.ORDER_YEAR AND
					OD.intSalesOrderId=trn_order_sub_contract_gate_pass_details.SALES_ORDER_ID ";
		
		$where	= " trn_order_sub_contract_gate_pass_details.SUB_CONTRACT_GP_NO = '".$SUB_CONTRACT_GP_NO."' AND
					trn_order_sub_contract_gate_pass_details.SUB_CONTRACT_GP_YEAR = '".$SUB_CONTRACT_GP_YEAR."' AND
					trn_order_sub_contract_gate_pass_details.SALES_ORDER_ID = '".$SALES_ORDER_ID."' AND
					trn_order_sub_contract_gate_pass_details.CUT_NO = '".$CUT_NO."' AND
					trn_order_sub_contract_gate_pass_details.SIZE = '".$SIZE."' ";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			$this->setVariables($result);
		else
			throw new Exception("Record not found.");
			
	}
	
	
}
?>

<?php
class ware_fabricdispatchconfirmation_details{
 
	private $db;
	private $table= "ware_fabricdispatchconfirmation_details";
	
	//private property
	private $DISP_CONFIRM_NO;
	private $DISP_CONFIRM_YEAR;
	private $SALES_ORDER_ID;
	private $SIZE;
	private $SAMPLE_QTY;
	private $GOOD_QTY;
	private $EMBROIDERY_QTY;
	private $P_DAMMAGE_QTY;
	private $F_DAMMAGE_QTY;
	private $CUT_RETURN_QTY;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('DISP_CONFIRM_NO'=>'DISP_CONFIRM_NO',
										'DISP_CONFIRM_YEAR'=>'DISP_CONFIRM_YEAR',
										'SALES_ORDER_ID'=>'SALES_ORDER_ID',
										'SIZE'=>'SIZE',
										'SAMPLE_QTY'=>'SAMPLE_QTY',
										'GOOD_QTY'=>'GOOD_QTY',
										'EMBROIDERY_QTY'=>'EMBROIDERY_QTY',
										'P_DAMMAGE_QTY'=>'P_DAMMAGE_QTY',
										'F_DAMMAGE_QTY'=>'F_DAMMAGE_QTY',
										'CUT_RETURN_QTY'=>'CUT_RETURN_QTY',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "DISP_CONFIRM_NO = ".$this->DISP_CONFIRM_NO." and DISP_CONFIRM_YEAR = ".$this->DISP_CONFIRM_YEAR." and SALES_ORDER_ID = ".$this->SALES_ORDER_ID." and SIZE = ".$this->SIZE."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun DISP_CONFIRM_NO
	function getDISP_CONFIRM_NO()
	{
		$this->validate();
		return $this->DISP_CONFIRM_NO;
	}
	
	//retun DISP_CONFIRM_YEAR
	function getDISP_CONFIRM_YEAR()
	{
		$this->validate();
		return $this->DISP_CONFIRM_YEAR;
	}
	
	//retun SALES_ORDER_ID
	function getSALES_ORDER_ID()
	{
		$this->validate();
		return $this->SALES_ORDER_ID;
	}
	
	//retun SIZE
	function getSIZE()
	{
		$this->validate();
		return $this->SIZE;
	}
	
	//retun SAMPLE_QTY
	function getSAMPLE_QTY()
	{
		$this->validate();
		return $this->SAMPLE_QTY;
	}
	
	//retun GOOD_QTY
	function getGOOD_QTY()
	{
		$this->validate();
		return $this->GOOD_QTY;
	}
	
	//retun EMBROIDERY_QTY
	function getEMBROIDERY_QTY()
	{
		$this->validate();
		return $this->EMBROIDERY_QTY;
	}
	
	//retun P_DAMMAGE_QTY
	function getP_DAMMAGE_QTY()
	{
		$this->validate();
		return $this->P_DAMMAGE_QTY;
	}
	
	//retun F_DAMMAGE_QTY
	function getF_DAMMAGE_QTY()
	{
		$this->validate();
		return $this->F_DAMMAGE_QTY;
	}
	
	//retun CUT_RETURN_QTY
	function getCUT_RETURN_QTY()
	{
		$this->validate();
		return $this->CUT_RETURN_QTY;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set DISP_CONFIRM_NO
	function setDISP_CONFIRM_NO($DISP_CONFIRM_NO)
	{
		array_push($this->commitArray,'DISP_CONFIRM_NO');
		$this->DISP_CONFIRM_NO = $DISP_CONFIRM_NO;
	}
	
	//set DISP_CONFIRM_YEAR
	function setDISP_CONFIRM_YEAR($DISP_CONFIRM_YEAR)
	{
		array_push($this->commitArray,'DISP_CONFIRM_YEAR');
		$this->DISP_CONFIRM_YEAR = $DISP_CONFIRM_YEAR;
	}
	
	//set SALES_ORDER_ID
	function setSALES_ORDER_ID($SALES_ORDER_ID)
	{
		array_push($this->commitArray,'SALES_ORDER_ID');
		$this->SALES_ORDER_ID = $SALES_ORDER_ID;
	}
	
	//set SIZE
	function setSIZE($SIZE)
	{
		array_push($this->commitArray,'SIZE');
		$this->SIZE = $SIZE;
	}
	
	//set SAMPLE_QTY
	function setSAMPLE_QTY($SAMPLE_QTY)
	{
		array_push($this->commitArray,'SAMPLE_QTY');
		$this->SAMPLE_QTY = $SAMPLE_QTY;
	}
	
	//set GOOD_QTY
	function setGOOD_QTY($GOOD_QTY)
	{
		array_push($this->commitArray,'GOOD_QTY');
		$this->GOOD_QTY = $GOOD_QTY;
	}
	
	//set EMBROIDERY_QTY
	function setEMBROIDERY_QTY($EMBROIDERY_QTY)
	{
		array_push($this->commitArray,'EMBROIDERY_QTY');
		$this->EMBROIDERY_QTY = $EMBROIDERY_QTY;
	}
	
	//set P_DAMMAGE_QTY
	function setP_DAMMAGE_QTY($P_DAMMAGE_QTY)
	{
		array_push($this->commitArray,'P_DAMMAGE_QTY');
		$this->P_DAMMAGE_QTY = $P_DAMMAGE_QTY;
	}
	
	//set F_DAMMAGE_QTY
	function setF_DAMMAGE_QTY($F_DAMMAGE_QTY)
	{
		array_push($this->commitArray,'F_DAMMAGE_QTY');
		$this->F_DAMMAGE_QTY = $F_DAMMAGE_QTY;
	}
	
	//set CUT_RETURN_QTY
	function setCUT_RETURN_QTY($CUT_RETURN_QTY)
	{
		array_push($this->commitArray,'CUT_RETURN_QTY');
		$this->CUT_RETURN_QTY = $CUT_RETURN_QTY;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->DISP_CONFIRM_NO=='' || $this->DISP_CONFIRM_YEAR=='' || $this->SALES_ORDER_ID=='' || $this->SIZE=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($DISP_CONFIRM_NO , $DISP_CONFIRM_YEAR , $SALES_ORDER_ID , $SIZE)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "DISP_CONFIRM_NO='$DISP_CONFIRM_NO' and DISP_CONFIRM_YEAR='$DISP_CONFIRM_YEAR' and SALES_ORDER_ID='$SALES_ORDER_ID' and SIZE='$SIZE'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($DISP_CONFIRM_NO,$DISP_CONFIRM_YEAR,$SALES_ORDER_ID,$SIZE,$SAMPLE_QTY,$GOOD_QTY,$EMBROIDERY_QTY,$P_DAMMAGE_QTY,$F_DAMMAGE_QTY,$CUT_RETURN_QTY){
		$data = array('DISP_CONFIRM_NO'=>$DISP_CONFIRM_NO 
				,'DISP_CONFIRM_YEAR'=>$DISP_CONFIRM_YEAR 
				,'SALES_ORDER_ID'=>$SALES_ORDER_ID 
				,'SIZE'=>$SIZE 
				,'SAMPLE_QTY'=>$SAMPLE_QTY 
				,'GOOD_QTY'=>$GOOD_QTY 
				,'EMBROIDERY_QTY'=>$EMBROIDERY_QTY 
				,'P_DAMMAGE_QTY'=>$P_DAMMAGE_QTY 
				,'F_DAMMAGE_QTY'=>$F_DAMMAGE_QTY 
				,'CUT_RETURN_QTY'=>$CUT_RETURN_QTY 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('DISP_CONFIRM_NO,DISP_CONFIRM_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['DISP_CONFIRM_NO'])
				$html .= '<option selected="selected" value="'.$row['DISP_CONFIRM_NO'].'">'.$row['DISP_CONFIRM_YEAR'].'</option>';	
			else
				$html .= '<option value="'.$row['DISP_CONFIRM_NO'].'">'.$row['DISP_CONFIRM_YEAR'].'</option>';	
		}
		return $html;
	}
	
	function getTotalConfirmDetails($orderNo,$orderYear,$serialNo,$serialYear)
	{
		 $cols	= " IFNULL(SUM(ware_fabricdispatchconfirmation_details.GOOD_QTY),0) AS TOT_CONF_GOOD,
		 		    IFNULL(SUM(ware_fabricdispatchconfirmation_details.P_DAMMAGE_QTY),0) AS TOT_CONF_DAMAGE,
			 IFNULL(SUM(ware_fabricdispatchconfirmation_details.GOOD_QTY*trn_orderdetails.dblPrice),0) AS TOT_CONF_GOOD_AMOUNT,
		 		    IFNULL(SUM(ware_fabricdispatchconfirmation_details.P_DAMMAGE_QTY*trn_orderdetails.dblPrice),0) AS TOT_CONF_DAMAGE_AMOUNT 		
					";
		 $join	= " INNER JOIN ware_fabricdispatchconfirmation_header 
		 			ON ware_fabricdispatchconfirmation_details.DISP_CONFIRM_NO = ware_fabricdispatchconfirmation_header.DISP_CONFIRM_NO 
					AND ware_fabricdispatchconfirmation_details.DISP_CONFIRM_YEAR = ware_fabricdispatchconfirmation_header.DISP_CONFIRM_YEAR
INNER JOIN trn_orderdetails ON ware_fabricdispatchconfirmation_header.ORDER_NO = trn_orderdetails.intOrderNo 
AND ware_fabricdispatchconfirmation_header.ORDER_YEAR = trn_orderdetails.intOrderYear 
AND ware_fabricdispatchconfirmation_details.SALES_ORDER_ID = trn_orderdetails.intSalesOrderId 
					";
		 $where = " ware_fabricdispatchconfirmation_header.ORDER_NO = '$orderNo' AND
				    ware_fabricdispatchconfirmation_header.ORDER_YEAR = '$orderYear'"; 
		if($serialNo != NULL)
		  $where .= " AND ware_fabricdispatchconfirmation_header.DISP_CONFIRM_NO = '$serialNo'";
		if($serialYear != NULL)  
     	  $where .= " AND ware_fabricdispatchconfirmation_header.DISP_CONFIRM_YEAR = '$serialYear'";
		//echo "select ".$cols." from ware_fabricdispatchconfirmation_details ".$join." where ".$where;
		$result	= $this->select($cols,$join,$where,$order=null,$limit=null);
		$row	= mysqli_fetch_array($result);
		return $row;	
	}
	
function loadDamageReturnedQty_confirmed($orderNo,$orderYear,$salesOrderId)
{
		global $db;
		$cols	= "				SUM(IFNULL(ware_fabricdispatchconfirmation_details.P_DAMMAGE_QTY,0)+IFNULL(ware_fabricdispatchconfirmation_details.F_DAMMAGE_QTY,0)+IFNULL(ware_fabricdispatchconfirmation_details.CUT_RETURN_QTY,0)) AS TOT_DAMAGE ";
				$join	= "	INNER JOIN ware_fabricdispatchconfirmation_header ON ware_fabricdispatchconfirmation_details.DISP_CONFIRM_NO = ware_fabricdispatchconfirmation_header.DISP_CONFIRM_NO AND ware_fabricdispatchconfirmation_details.DISP_CONFIRM_YEAR = ware_fabricdispatchconfirmation_header.DISP_CONFIRM_YEAR ";
				
				$where = " 
				ware_fabricdispatchconfirmation_header.ORDER_NO = '$orderNo' AND
				ware_fabricdispatchconfirmation_header.ORDER_YEAR = '$orderYear' AND
				ware_fabricdispatchconfirmation_details.SALES_ORDER_ID = '$salesOrderId'  
				GROUP BY
				ware_fabricdispatchconfirmation_header.ORDER_NO,
				ware_fabricdispatchconfirmation_header.ORDER_YEAR,
				ware_fabricdispatchconfirmation_details.SALES_ORDER_ID ";
				
				//echo "select ".$cols." from ware_fabricdispatchconfirmation_details " .$join." where ".$where;
	
		$result	= $this->select($cols,$join,$where,$order=null,$limit=null);
		$row	= mysqli_fetch_array($result);
 		return $row['TOT_DAMAGE'];
}
	
function checkCustomerConfirmation($orderNo,$orderYear,$salesOrderId)
{
		global $db;
				$sql = "SELECT 
				ware_fabricdispatchconfirmation_header.STATUS 
				FROM 
				ware_fabricdispatchconfirmation_details
				INNER JOIN ware_fabricdispatchconfirmation_header ON ware_fabricdispatchconfirmation_details.DISP_CONFIRM_NO = ware_fabricdispatchconfirmation_header.DISP_CONFIRM_NO AND ware_fabricdispatchconfirmation_details.DISP_CONFIRM_YEAR = ware_fabricdispatchconfirmation_header.DISP_CONFIRM_YEAR 
				WHERE 
				ware_fabricdispatchconfirmation_header.STATUS = '1' AND
				ware_fabricdispatchconfirmation_header.ORDER_NO = '$orderNo' AND
				ware_fabricdispatchconfirmation_header.ORDER_YEAR = '$orderYear' AND
				ware_fabricdispatchconfirmation_details.SALES_ORDER_ID = '$salesOrderId'  
				GROUP BY
				ware_fabricdispatchconfirmation_header.ORDER_NO,
				ware_fabricdispatchconfirmation_header.ORDER_YEAR,
				ware_fabricdispatchconfirmation_details.SALES_ORDER_ID ";
	
		$result = $this->db->RunQuery($sql);
		return mysqli_num_rows($result);
 }
 
function getConfirmedSalesOrderDetails($serialNo,$serialYear)
{
		global $db;
				$sql = "SELECT
				trn_orderdetails.intOrderNo,
				trn_orderdetails.intOrderYear,
				trn_orderdetails.intSalesOrderId,
				trn_orderdetails.strSalesOrderNo,
				mst_part.strName,
				GROUP_CONCAT(DISTINCT strSalesOrderNo,'/',strName) AS salesOrders 				FROM
				ware_fabricdispatchconfirmation_details
				INNER JOIN ware_fabricdispatchconfirmation_header ON ware_fabricdispatchconfirmation_details.DISP_CONFIRM_NO = ware_fabricdispatchconfirmation_header.DISP_CONFIRM_NO AND ware_fabricdispatchconfirmation_details.DISP_CONFIRM_YEAR = ware_fabricdispatchconfirmation_header.DISP_CONFIRM_YEAR
				INNER JOIN trn_orderdetails ON ware_fabricdispatchconfirmation_header.ORDER_NO = trn_orderdetails.intOrderNo AND ware_fabricdispatchconfirmation_header.ORDER_YEAR = trn_orderdetails.intOrderYear AND ware_fabricdispatchconfirmation_details.SALES_ORDER_ID = trn_orderdetails.intSalesOrderId
				INNER JOIN mst_part ON trn_orderdetails.intPart = mst_part.intId
				WHERE
 
				ware_fabricdispatchconfirmation_header.STATUS = '1' AND
				ware_fabricdispatchconfirmation_header.DISP_CONFIRM_NO = '$serialNo' AND
				ware_fabricdispatchconfirmation_header.DISP_CONFIRM_YEAR = '$serialYear'   
				GROUP BY
				ware_fabricdispatchconfirmation_header.ORDER_NO,
				ware_fabricdispatchconfirmation_header.ORDER_YEAR,
				ware_fabricdispatchconfirmation_details.SALES_ORDER_ID ";
	
		$result = $db->RunQuery($sql);
		$rcds			= mysqli_num_rows($result);
		
		$row			= mysqli_fetch_array($result);
 		$salesOrders	= $row['salesOrders'];
 		$salesOrderId	= $row['intSalesOrderId'];
		$arrSales[0]=$rcds;
		$arrSales[1]=$salesOrders;
		$arrSales[2]=$salesOrderId;
		
		return $arrSales;
 }
 
function getTotalDispatchesBeforeConfirmDetails($serialNo,$serialYear,$salesOrder)
{
		global $db;
				$sql = "SELECT
						sum(ware_fabricdispatchdetails.dblGoodQty) as GOOD,
						sum(ware_fabricdispatchdetails.dblSampleQty+
						ware_fabricdispatchdetails.dblEmbroideryQty+
						ware_fabricdispatchdetails.dblPDammageQty+
						ware_fabricdispatchdetails.dblFdammageQty+ 
						ware_fabricdispatchdetails.dblCutRetQty) AS DAMMAGE ,
						sum(ware_fabricdispatchdetails.dblGoodQty*trn_orderdetails.dblPrice) as GOOD_AMOUNT,
						sum((ware_fabricdispatchdetails.dblSampleQty+
						ware_fabricdispatchdetails.dblEmbroideryQty+
						ware_fabricdispatchdetails.dblPDammageQty+
						ware_fabricdispatchdetails.dblFdammageQty+
						ware_fabricdispatchdetails.dblCutRetQty)*trn_orderdetails.dblPrice) AS DAMMAGE_AMOUNT  
						
						FROM
						ware_fabricdispatchconfirmation_header
						INNER JOIN ware_fabricdispatchconfirmation_details ON ware_fabricdispatchconfirmation_header.DISP_CONFIRM_NO = ware_fabricdispatchconfirmation_details.DISP_CONFIRM_NO AND ware_fabricdispatchconfirmation_header.DISP_CONFIRM_YEAR = ware_fabricdispatchconfirmation_details.DISP_CONFIRM_YEAR
						INNER JOIN ware_fabricdispatchheader ON ware_fabricdispatchconfirmation_header.ORDER_NO = ware_fabricdispatchheader.intOrderNo AND ware_fabricdispatchconfirmation_header.ORDER_YEAR = ware_fabricdispatchheader.intOrderYear
						INNER JOIN ware_fabricdispatchdetails ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear AND ware_fabricdispatchconfirmation_details.SALES_ORDER_ID = ware_fabricdispatchdetails.intSalesOrderId AND ware_fabricdispatchconfirmation_details.SIZE = ware_fabricdispatchdetails.strSize
INNER JOIN trn_orderdetails ON ware_fabricdispatchheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabricdispatchheader.intOrderYear = trn_orderdetails.intOrderYear AND ware_fabricdispatchdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
						WHERE
						ware_fabricdispatchconfirmation_header.ORDER_NO = '$serialNo' AND
						ware_fabricdispatchconfirmation_header.ORDER_YEAR = '$serialYear' AND
						ware_fabricdispatchconfirmation_header.`STATUS` = 1
						 ";
		if($salesOrder!='')
		$sql  .= " AND ware_fabricdispatchdetails.intSalesOrderId = '$salesOrder' ";
	//echo $sql;
		$result = $db->RunQuery($sql);
 		$row			= mysqli_fetch_array($result);
 		
		return $row;
 }
 
	public function getConfirmedsalesOrderWisePrice_givenSO($orderNo,$orderYear,$salesOrder)
	{
		$cols	= "trn_orderdetails.dblPrice,
				   trn_orderdetails.intSalesOrderId";
		
		$join	= "INNER JOIN ware_fabricdispatchheader ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
		INNER JOIN trn_orderdetails ON ware_fabricdispatchheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabricdispatchheader.intOrderYear = trn_orderdetails.intOrderYear AND ware_fabricdispatchdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
		";
		$where  = "
		ware_fabricdispatchheader.intStatus = 1
		 AND trn_orderheader.intOrderNo = '$orderNo' 
		 AND trn_orderheader.intOrderYear = '$orderYear'";
		if($salesOrder!='')
		$where  .= " AND trn_orderdetails.intSalesOrderId = '$salesOrder' ";
				
		$result	= $this->select($cols,$join,$where, $order = null, $limit = null);	
 		return 	$result;
	}
 
	//END }
}
?>
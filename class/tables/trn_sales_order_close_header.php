<?php
class trn_sales_order_close_header{
 
	private $db;
	private $table= "trn_sales_order_close_header";
	
	//private property
	private $CLOSE_NO;
	private $CLOSE_YEAR;
	private $ORDER_NO;
	private $ORDER_YEAR;
	private $CREATED_DATE;
	private $CREATED_BY;
	private $MODIFIED_DATE;
	private $MODIFIED_BY;
	private $APPROVE_LEVELS;
	private $STATUS;
	private $LOCATION_ID;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('CLOSE_NO'=>'CLOSE_NO',
										'CLOSE_YEAR'=>'CLOSE_YEAR',
										'ORDER_NO'=>'ORDER_NO',
										'ORDER_YEAR'=>'ORDER_YEAR',
										'CREATED_DATE'=>'CREATED_DATE',
										'CREATED_BY'=>'CREATED_BY',
										'MODIFIED_DATE'=>'MODIFIED_DATE',
										'MODIFIED_BY'=>'MODIFIED_BY',
										'APPROVE_LEVELS'=>'APPROVE_LEVELS',
										'STATUS'=>'STATUS',
										'LOCATION_ID'=>'LOCATION_ID',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "CLOSE_NO = ".$this->CLOSE_NO." and CLOSE_YEAR = ".$this->CLOSE_YEAR."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun CLOSE_NO
	function getCLOSE_NO()
	{
		$this->validate();
		return $this->CLOSE_NO;
	}
	
	//retun CLOSE_YEAR
	function getCLOSE_YEAR()
	{
		$this->validate();
		return $this->CLOSE_YEAR;
	}
	
	//retun ORDER_NO
	function getORDER_NO()
	{
		$this->validate();
		return $this->ORDER_NO;
	}
	
	//retun ORDER_YEAR
	function getORDER_YEAR()
	{
		$this->validate();
		return $this->ORDER_YEAR;
	}
	
	//retun CREATED_DATE
	function getCREATED_DATE()
	{
		$this->validate();
		return $this->CREATED_DATE;
	}
	
	//retun CREATED_BY
	function getCREATED_BY()
	{
		$this->validate();
		return $this->CREATED_BY;
	}
	
	//retun MODIFIED_DATE
	function getMODIFIED_DATE()
	{
		$this->validate();
		return $this->MODIFIED_DATE;
	}
	
	//retun MODIFIED_BY
	function getMODIFIED_BY()
	{
		$this->validate();
		return $this->MODIFIED_BY;
	}
	
	//retun APPROVE_LEVELS
	function getAPPROVE_LEVELS()
	{
		$this->validate();
		return $this->APPROVE_LEVELS;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}

	//retun STATUS
	function getLOCATION_ID()
	{
		$this->validate();
		return $this->LOCATION_ID;
	}
	//END }
	
	//BEGIN - public function set {
	
	//set CLOSE_NO
	function setCLOSE_NO($CLOSE_NO)
	{
		array_push($this->commitArray,'CLOSE_NO');
		$this->CLOSE_NO = $CLOSE_NO;
	}
	
	//set CLOSE_YEAR
	function setCLOSE_YEAR($CLOSE_YEAR)
	{
		array_push($this->commitArray,'CLOSE_YEAR');
		$this->CLOSE_YEAR = $CLOSE_YEAR;
	}
	
	//set ORDER_NO
	function setORDER_NO($ORDER_NO)
	{
		array_push($this->commitArray,'ORDER_NO');
		$this->ORDER_NO = $ORDER_NO;
	}
	
	//set ORDER_YEAR
	function setORDER_YEAR($ORDER_YEAR)
	{
		array_push($this->commitArray,'ORDER_YEAR');
		$this->ORDER_YEAR = $ORDER_YEAR;
	}
	
	//set CREATED_DATE
	function setCREATED_DATE($CREATED_DATE)
	{
		array_push($this->commitArray,'CREATED_DATE');
		$this->CREATED_DATE = $CREATED_DATE;
	}
	
	//set CREATED_BY
	function setCREATED_BY($CREATED_BY)
	{
		array_push($this->commitArray,'CREATED_BY');
		$this->CREATED_BY = $CREATED_BY;
	}
	
	//set MODIFIED_DATE
	function setMODIFIED_DATE($MODIFIED_DATE)
	{
		array_push($this->commitArray,'MODIFIED_DATE');
		$this->MODIFIED_DATE = $MODIFIED_DATE;
	}
	
	//set MODIFIED_BY
	function setMODIFIED_BY($MODIFIED_BY)
	{
		array_push($this->commitArray,'MODIFIED_BY');
		$this->MODIFIED_BY = $MODIFIED_BY;
	}
	
	//set APPROVE_LEVELS
	function setAPPROVE_LEVELS($APPROVE_LEVELS)
	{
		array_push($this->commitArray,'APPROVE_LEVELS');
		$this->APPROVE_LEVELS = $APPROVE_LEVELS;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//set STATUS
	function setLOCATION_ID($LOCATION_ID)
	{
		array_push($this->commitArray,'LOCATION_ID');
		$this->LOCATION_ID = $LOCATION_ID;
	}
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->CLOSE_NO=='' || $this->CLOSE_YEAR=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($CLOSE_NO , $CLOSE_YEAR)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "CLOSE_NO='$CLOSE_NO' and CLOSE_YEAR='$CLOSE_YEAR'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($CLOSE_NO,$CLOSE_YEAR,$ORDER_NO,$ORDER_YEAR,$CREATED_DATE,$CREATED_BY,$MODIFIED_DATE,$MODIFIED_BY,$APPROVE_LEVELS,$STATUS,$LOCATION_ID){
		$data = array('CLOSE_NO'=>$CLOSE_NO 
				,'CLOSE_YEAR'=>$CLOSE_YEAR 
				,'ORDER_NO'=>$ORDER_NO 
				,'ORDER_YEAR'=>$ORDER_YEAR 
				,'CREATED_DATE'=>$CREATED_DATE 
				,'CREATED_BY'=>$CREATED_BY 
				,'MODIFIED_DATE'=>$MODIFIED_DATE 
				,'MODIFIED_BY'=>$MODIFIED_BY 
				,'APPROVE_LEVELS'=>$APPROVE_LEVELS 
				,'STATUS'=>$STATUS 
				,'LOCATION_ID'=>$LOCATION_ID 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('CLOSE_NO,CLOSE_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['CLOSE_NO'])
				$html .= '<option selected="selected" value="'.$row['CLOSE_NO'].'">'.$row['CLOSE_YEAR'].'</option>';	
			else
				$html .= '<option value="'.$row['CLOSE_NO'].'">'.$row['CLOSE_YEAR'].'</option>';	
		}
		return $html;
	}
	
	public function getOpenedOrderNos($year,$defaultValue){

		$cols	= "
 					trn_sales_order_open_requisition_details.REQUISITION_NO ";		
		$join	= "
					INNER JOIN trn_sales_order_open_requisition_header ON trn_sales_order_open_requisition_details.REQUISITION_YEAR = trn_sales_order_open_requisition_header.REQUISITION_YEAR AND trn_sales_order_open_requisition_details.REQUISITION_NO = trn_sales_order_open_requisition_header.REQUISITION_NO
					left JOIN trn_sales_order_open_header ON trn_sales_order_open_requisition_details.REQUISITION_YEAR = trn_sales_order_open_header.REQUISITION_YEAR AND trn_sales_order_open_requisition_details.REQUISITION_NO = trn_sales_order_open_header.REQUISITION_NO AND trn_sales_order_open_header.`STATUS` >0 
					left JOIN trn_sales_order_open_details ON trn_sales_order_open_header.OPEN_NO = trn_sales_order_open_details.OPEN_NO AND trn_sales_order_open_header.OPEN_YEAR = trn_sales_order_open_details.OPEN_YEAR AND trn_sales_order_open_requisition_details.ORDER_YEAR = trn_sales_order_open_details.ORDER_YEAR AND trn_sales_order_open_requisition_details.ORDER_NO = trn_sales_order_open_details.ORDER_NO AND trn_sales_order_open_requisition_details.SALES_ORDER_ID = trn_sales_order_open_details.SALE_ORDER_ID ";
		
		$where	= " trn_sales_order_open_header.OPEN_NO IS NULL AND 
					trn_sales_order_open_requisition_header.STATUS = 1 AND 
					trn_sales_order_open_requisition_header.REQUISITION_YEAR = '$year'
					GROUP BY
					trn_sales_order_open_requisition_details.REQUISITION_NO 
					ORDER BY 
 					trn_sales_order_open_requisition_details.REQUISITION_NO ASC
					";
		//	echo $sql	="select ".$cols." from trn_sales_order_open_requisition_details ".$join." where".$where;
	
		$result = $this->select($cols,$join,$where);	

		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['REQUISITION_NO'])
				$html .= '<option selected="selected" value="'.$row['REQUISITION_NO'].'">'.$row['REQUISITION_YEAR']."/".$row['REQUISITION_NO'].'</option>';	
			else
				$html .= '<option value="'.$row['REQUISITION_NO'].'">'.$row['REQUISITION_NO'].'</option>';	
		}
		return $html;
	}
	
	
	//END }
}
?>
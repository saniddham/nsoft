<?php
class finance_pettycash_invoice_detail{
 
	private $db;
	private $table= "finance_pettycash_invoice_detail";
	
	//private property
	private $PETTY_INVOICE_NO;
	private $PETTY_INVOICE_YEAR;
	private $DEPARTMENT_ID;
	private $PETTY_CASH_ITEM_ID;
	private $REQUEST_USER_ID;
	private $INVOICE_AMOUNT;
	private $RECEIVE_AMOUNT;
	private $BALANCE_AMOUNT;
	private $DEPT_APPROVE_STATUS;
	private $DEPT_APPROVE_LEVELS;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('PETTY_INVOICE_NO'=>'PETTY_INVOICE_NO',
										'PETTY_INVOICE_YEAR'=>'PETTY_INVOICE_YEAR',
										'DEPARTMENT_ID'=>'DEPARTMENT_ID',
										'PETTY_CASH_ITEM_ID'=>'PETTY_CASH_ITEM_ID',
										'REQUEST_USER_ID'=>'REQUEST_USER_ID',
										'INVOICE_AMOUNT'=>'INVOICE_AMOUNT',
										'RECEIVE_AMOUNT'=>'RECEIVE_AMOUNT',
										'BALANCE_AMOUNT'=>'BALANCE_AMOUNT',
										'DEPT_APPROVE_STATUS'=>'DEPT_APPROVE_STATUS',
										'DEPT_APPROVE_LEVELS'=>'DEPT_APPROVE_LEVELS',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "PETTY_INVOICE_NO = ".$this->PETTY_INVOICE_NO." and PETTY_INVOICE_YEAR = ".$this->PETTY_INVOICE_YEAR." and PETTY_CASH_ITEM_ID = ".$this->PETTY_CASH_ITEM_ID." and DEPARTMENT_ID = ".$this->DEPARTMENT_ID."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun PETTY_INVOICE_NO
	function getPETTY_INVOICE_NO()
	{
		$this->validate();
		return $this->PETTY_INVOICE_NO;
	}
	
	//retun PETTY_INVOICE_YEAR
	function getPETTY_INVOICE_YEAR()
	{
		$this->validate();
		return $this->PETTY_INVOICE_YEAR;
	}
	
	//retun DEPARTMENT_ID
	function getDEPARTMENT_ID()
	{
		$this->validate();
		return $this->DEPARTMENT_ID;
	}
	
	//retun PETTY_CASH_ITEM_ID
	function getPETTY_CASH_ITEM_ID()
	{
		$this->validate();
		return $this->PETTY_CASH_ITEM_ID;
	}
	
	//retun REQUEST_USER_ID
	function getREQUEST_USER_ID()
	{
		$this->validate();
		return $this->REQUEST_USER_ID;
	}
	
	//retun INVOICE_AMOUNT
	function getINVOICE_AMOUNT()
	{
		$this->validate();
		return $this->INVOICE_AMOUNT;
	}
	
	//retun RECEIVE_AMOUNT
	function getRECEIVE_AMOUNT()
	{
		$this->validate();
		return $this->RECEIVE_AMOUNT;
	}
	
	//retun BALANCE_AMOUNT
	function getBALANCE_AMOUNT()
	{
		$this->validate();
		return $this->BALANCE_AMOUNT;
	}
	
	//retun DEPT_APPROVE_STATUS
	function getDEPT_APPROVE_STATUS()
	{
		$this->validate();
		return $this->DEPT_APPROVE_STATUS;
	}
	
	//retun DEPT_APPROVE_LEVELS
	function getDEPT_APPROVE_LEVELS()
	{
		$this->validate();
		return $this->DEPT_APPROVE_LEVELS;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set PETTY_INVOICE_NO
	function setPETTY_INVOICE_NO($PETTY_INVOICE_NO)
	{
		array_push($this->commitArray,'PETTY_INVOICE_NO');
		$this->PETTY_INVOICE_NO = $PETTY_INVOICE_NO;
	}
	
	//set PETTY_INVOICE_YEAR
	function setPETTY_INVOICE_YEAR($PETTY_INVOICE_YEAR)
	{
		array_push($this->commitArray,'PETTY_INVOICE_YEAR');
		$this->PETTY_INVOICE_YEAR = $PETTY_INVOICE_YEAR;
	}
	
	//set DEPARTMENT_ID
	function setDEPARTMENT_ID($DEPARTMENT_ID)
	{
		array_push($this->commitArray,'DEPARTMENT_ID');
		$this->DEPARTMENT_ID = $DEPARTMENT_ID;
	}
	
	//set PETTY_CASH_ITEM_ID
	function setPETTY_CASH_ITEM_ID($PETTY_CASH_ITEM_ID)
	{
		array_push($this->commitArray,'PETTY_CASH_ITEM_ID');
		$this->PETTY_CASH_ITEM_ID = $PETTY_CASH_ITEM_ID;
	}
	
	//set REQUEST_USER_ID
	function setREQUEST_USER_ID($REQUEST_USER_ID)
	{
		array_push($this->commitArray,'REQUEST_USER_ID');
		$this->REQUEST_USER_ID = $REQUEST_USER_ID;
	}
	
	//set INVOICE_AMOUNT
	function setINVOICE_AMOUNT($INVOICE_AMOUNT)
	{
		array_push($this->commitArray,'INVOICE_AMOUNT');
		$this->INVOICE_AMOUNT = $INVOICE_AMOUNT;
	}
	
	//set RECEIVE_AMOUNT
	function setRECEIVE_AMOUNT($RECEIVE_AMOUNT)
	{
		array_push($this->commitArray,'RECEIVE_AMOUNT');
		$this->RECEIVE_AMOUNT = $RECEIVE_AMOUNT;
	}
	
	//set BALANCE_AMOUNT
	function setBALANCE_AMOUNT($BALANCE_AMOUNT)
	{
		array_push($this->commitArray,'BALANCE_AMOUNT');
		$this->BALANCE_AMOUNT = $BALANCE_AMOUNT;
	}
	
	//set DEPT_APPROVE_STATUS
	function setDEPT_APPROVE_STATUS($DEPT_APPROVE_STATUS)
	{
		array_push($this->commitArray,'DEPT_APPROVE_STATUS');
		$this->DEPT_APPROVE_STATUS = $DEPT_APPROVE_STATUS;
	}
	
	//set DEPT_APPROVE_LEVELS
	function setDEPT_APPROVE_LEVELS($DEPT_APPROVE_LEVELS)
	{
		array_push($this->commitArray,'DEPT_APPROVE_LEVELS');
		$this->DEPT_APPROVE_LEVELS = $DEPT_APPROVE_LEVELS;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->PETTY_INVOICE_NO=='' || $this->PETTY_INVOICE_YEAR=='' || $this->PETTY_CASH_ITEM_ID=='' || $this->DEPARTMENT_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($PETTY_INVOICE_NO , $PETTY_INVOICE_YEAR , $PETTY_CASH_ITEM_ID , $DEPARTMENT_ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "PETTY_INVOICE_NO='$PETTY_INVOICE_NO' and PETTY_INVOICE_YEAR='$PETTY_INVOICE_YEAR' and PETTY_CASH_ITEM_ID='$PETTY_CASH_ITEM_ID' and DEPARTMENT_ID='$DEPARTMENT_ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($PETTY_INVOICE_NO,$PETTY_INVOICE_YEAR,$DEPARTMENT_ID,$PETTY_CASH_ITEM_ID,$REQUEST_USER_ID,$INVOICE_AMOUNT,$RECEIVE_AMOUNT,$BALANCE_AMOUNT,$DEPT_APPROVE_STATUS,$DEPT_APPROVE_LEVELS){
		$data = array('PETTY_INVOICE_NO'=>$PETTY_INVOICE_NO 
				,'PETTY_INVOICE_YEAR'=>$PETTY_INVOICE_YEAR 
				,'DEPARTMENT_ID'=>$DEPARTMENT_ID 
				,'PETTY_CASH_ITEM_ID'=>$PETTY_CASH_ITEM_ID 
				,'REQUEST_USER_ID'=>$REQUEST_USER_ID 
				,'INVOICE_AMOUNT'=>$INVOICE_AMOUNT 
				,'RECEIVE_AMOUNT'=>$RECEIVE_AMOUNT 
				,'BALANCE_AMOUNT'=>$BALANCE_AMOUNT 
				,'DEPT_APPROVE_STATUS'=>$DEPT_APPROVE_STATUS 
				,'DEPT_APPROVE_LEVELS'=>$DEPT_APPROVE_LEVELS 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('PETTY_INVOICE_NO,PETTY_INVOICE_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['PETTY_INVOICE_NO'])
				$html .= '<option selected="selected" value="'.$row['PETTY_INVOICE_NO'].'">'.$row['PETTY_INVOICE_YEAR'].'</option>';	
			else
				$html .= '<option value="'.$row['PETTY_INVOICE_NO'].'">'.$row['PETTY_INVOICE_YEAR'].'</option>';	
		}
		return $html;
	}
	
	public function loadIOUDetails($invoiceNo,$invoiceYear,$hrDB)
	{
		
		$cols	= ' finance_pettycash_invoice_detail.DEPARTMENT_ID,
					finance_pettycash_invoice_detail.PETTY_CASH_ITEM_ID,
					finance_pettycash_invoice_detail.REQUEST_USER_ID,
					IFNULL(finance_pettycash_invoice_detail.INVOICE_AMOUNT,0) AS INVOICE_AMOUNT,
					IFNULL(finance_pettycash_invoice_detail.RECEIVE_AMOUNT,0) AS RECEIVE_AMOUNT,
					IFNULL(finance_pettycash_invoice_detail.BALANCE_AMOUNT,0) AS BALANCE_AMOUNT,
					finance_pettycash_invoice_detail.DEPT_APPROVE_STATUS,
					finance_pettycash_invoice_detail.DEPT_APPROVE_LEVELS,
					mst_department.strName AS DEPT_NAME,
					finance_mst_pettycash_item.`NAME` AS ITEM,
					finance_mst_pettycash_item.CATEGORY_ID,
					finance_mst_pettycash_category.`NAME` AS CATEGORY,
					finance_pettycash_invoice_detail.PETTY_INVOICE_YEAR,
					finance_pettycash_invoice_detail.PETTY_INVOICE_NO,
					QE.strInitialName AS REQ_USER';
		$join	= ' INNER JOIN mst_department ON finance_pettycash_invoice_detail.DEPARTMENT_ID = mst_department.intId
					INNER JOIN finance_mst_pettycash_item ON finance_pettycash_invoice_detail.PETTY_CASH_ITEM_ID = finance_mst_pettycash_item.ID
					INNER JOIN finance_mst_pettycash_category ON finance_mst_pettycash_item.CATEGORY_ID = finance_mst_pettycash_category.ID
					INNER JOIN finance_pettycash_employees ON finance_pettycash_invoice_detail.REQUEST_USER_ID = finance_pettycash_employees.EMPLOYEE_ID
					INNER JOIN '.$hrDB.'.mst_employee QE ON finance_pettycash_employees.EMPLOYEE_ID = QE.intEmployeeId';
		$where	= "finance_pettycash_invoice_detail.PETTY_INVOICE_NO = '$invoiceNo' AND finance_pettycash_invoice_detail.PETTY_INVOICE_YEAR = '$invoiceYear'";

		$result	= $this->select($cols,$join,$where,NULL,NULL);
		return $result;
	}
	
	public function getDepartmentWiseApprovePermission($invoiceNo,$invoiceYear,$departmentId,$itemId,$userId)
	{
						
		$col	= 'mst_department_heads.DEP_HEAD_ID';
		$join	= " INNER JOIN mst_department_heads ON finance_pettycash_invoice_detail.DEPARTMENT_ID = mst_department_heads.DEPARTMENT_ID
					INNER JOIN mst_department ON mst_department_heads.DEPARTMENT_ID = mst_department.intId"	;
		$where	= " finance_pettycash_invoice_detail.PETTY_INVOICE_NO = '$invoiceNo' AND
						finance_pettycash_invoice_detail.PETTY_INVOICE_YEAR = '$invoiceYear' AND
						finance_pettycash_invoice_detail.DEPARTMENT_ID = '$departmentId'  AND
						finance_pettycash_invoice_detail.PETTY_CASH_ITEM_ID = '$itemId' AND
						finance_pettycash_invoice_detail.DEPT_APPROVE_STATUS > 1
						GROUP BY
						mst_department_heads.DEPARTMENT_ID,
						finance_pettycash_invoice_detail.PETTY_CASH_ITEM_ID,
						mst_department_heads.DEP_HEAD_ID";
		$result	= $this->select($col,$join,$where,NULL,NULL);
		while($row = mysqli_fetch_array($result))
		{
			if($row['DEP_HEAD_ID'] != $intUser)
			$permision_confirm['permision']	= 0;
			else
			$permision_confirm['permision']	=1;
			
			return $permision_confirm;	
		}
	}
	
	public function getIOUDetailsCombo($defaultValue=null)
	{
		$cols  = " finance_pettycash_invoice_detail.PETTY_INVOICE_NO,
					finance_pettycash_invoice_detail.PETTY_INVOICE_YEAR,
					SUM(ABS(finance_pettycash_invoice_detail.BALANCE_AMOUNT)) AS CASH_BALANCE,
					finance_pettycash_invoice_header.INVOICE_TYPE";
					
		$join	= " INNER JOIN finance_pettycash_invoice_header ON finance_pettycash_invoice_detail.PETTY_INVOICE_NO = finance_pettycash_invoice_header.PETTY_INVOICE_NO AND finance_pettycash_invoice_detail.PETTY_INVOICE_YEAR = finance_pettycash_invoice_header.PETTY_INVOICE_YEAR";
				  
		$where	= " finance_pettycash_invoice_header.INVOICE_TYPE = 2 AND
					finance_pettycash_invoice_header.`STATUS` = 1 AND
					finance_pettycash_invoice_header.LOCATION_ID = 2 AND
					ABS(finance_pettycash_invoice_detail.BALANCE_AMOUNT) > 0
					GROUP BY
					finance_pettycash_invoice_detail.PETTY_INVOICE_NO,
					finance_pettycash_invoice_detail.PETTY_INVOICE_YEAR";



		$result = $this->select($cols,  $join, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			$concat_option	= $row['PETTY_INVOICE_NO'].'/'.$row['PETTY_INVOICE_YEAR'].'-'.$row['INVOICE_TYPE'].'=>'.number_format(abs($row['CASH_BALANCE']));
			$concat_val		= $row['PETTY_INVOICE_NO'].'/'.$row['PETTY_INVOICE_YEAR'];
			if($defaultValue==$concat_val)
				$html .= '<option selected="selected" value="'.$concat_val.'">'.$concat_option.'</option>';	
			else
				$html .= '<option value="'.$concat_val.'">'.$concat_option.'</option>';	
		}
		
		return $html;
		
	}
	
	public function getIOUDetails($iouNo,$iouYear,$departmentId=null)
	{
		$cols  = "  finance_pettycash_invoice_detail.PETTY_INVOICE_NO,
					finance_pettycash_invoice_detail.PETTY_INVOICE_YEAR,
					ABS(finance_pettycash_invoice_detail.BALANCE_AMOUNT) AS CASH_BALANCE,
					ABS(finance_pettycash_invoice_detail.INVOICE_AMOUNT) AS INVOICE_AMOUNT,
					finance_pettycash_invoice_header.INVOICE_TYPE,
					finance_pettycash_invoice_detail.REQUEST_USER_ID,
					finance_pettycash_invoice_detail.DEPARTMENT_ID";
				  
		$join	= " INNER JOIN finance_pettycash_invoice_header ON finance_pettycash_invoice_detail.PETTY_INVOICE_NO = finance_pettycash_invoice_header.PETTY_INVOICE_NO AND finance_pettycash_invoice_detail.PETTY_INVOICE_YEAR = finance_pettycash_invoice_header.PETTY_INVOICE_YEAR";
				  
		$where	= " finance_pettycash_invoice_header.INVOICE_TYPE = 2 AND
					finance_pettycash_invoice_header.`STATUS` = 1 AND
					finance_pettycash_invoice_header.LOCATION_ID = 2 AND
					ABS(finance_pettycash_invoice_detail.BALANCE_AMOUNT) > 0 AND 
					finance_pettycash_invoice_detail.PETTY_INVOICE_NO = '$iouNo' AND 
					finance_pettycash_invoice_detail.PETTY_INVOICE_YEAR = '$iouYear'";
		if($departmentId != NULL)
			$where	.= " AND  finance_pettycash_invoice_detail.DEPARTMENT_ID = '$departmentId'";
		
		$result = $this->select($cols,  $join, $where = $where);	
		return $result;	
	}
	
	public function getDetailAmountsWhenCancel($iouNo,$iouYear,$invoiceNo,$invoiceYear)
	{
		$con_invoice	= $invoiceNo.'/'.$invoiceYear;
		$cols	= " IFNULL(SUM(ABS(finance_pettycash_invoice_detail.INVOICE_AMOUNT)+ABS(finance_pettycash_invoice_detail.RECEIVE_AMOUNT)),0) AS TOT";
		$join	= " finance_pettycash_invoice_header
      				INNER JOIN finance_pettycash_invoice_detail ON finance_pettycash_invoice_header.PETTY_INVOICE_NO = 		finance_pettycash_invoice_detail.PETTY_INVOICE_NO AND finance_pettycash_invoice_header.PETTY_INVOICE_YEAR = finance_pettycash_invoice_detail.PETTY_INVOICE_YEAR";
		$where	= " finance_pettycash_invoice_header.IOU_PARENT_NO = '$iouNo' AND
					finance_pettycash_invoice_header.IOU_PARENT_YEAR = '$iouYear' AND 
					CONCAT(finance_pettycash_invoice_header.PETTY_INVOICE_NO,'/',finance_pettycash_invoice_header.PETTY_INVOICE_YEAR) <> '$con_invoice' 
";

		$result = $this->select($cols,  $join, $where = $where);	
		$row	= mysqli_fetch_array($result);
		return $row['TOT'];	
	
	}
	public function getInvoiceAmount($invoiceNo,$invoiceYear)
	{
		$cols	= " ABS(finance_pettycash_invoice_detail.INVOICE_AMOUNT) AS INVOICE_AMOUNT";
		$where	= " finance_pettycash_invoice_detail.PETTY_INVOICE_NO = '$invoiceNo' AND
					finance_pettycash_invoice_detail.PETTY_INVOICE_YEAR = '$invoiceYear' ";
		$result = $this->select($cols,NULL, $where = $where);	
		$row	= mysqli_fetch_array($result);
		return $row['INVOICE_AMOUNT'];	
	}
	
	public function checkDepartmentWiseApprove($invoiceNo,$invoiceYear,$hrDB)
	{
		$resultDtl	= $this->loadIOUDetails($invoiceNo,$invoiceYear,$hrDB);
		$str		='';
		$flag		= 0;
		while($row	= mysqli_fetch_array($resultDtl))		
		{
			if($row['DEPT_APPROVE_STATUS'] != 1)
			{
				$flag	= 1;	
				$str	.=$row['DEPT_NAME'].' ,';
			}
		}
		if($flag == 1)
			throw new Exception("Approval not raised by ". $str);
		else
			return true;
	}
	//END }
}
?>
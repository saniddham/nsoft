<?php
class other_meeting_minutes_details{
 
	private $db;
	private $table= "other_meeting_minutes_details";
	
	//private property
	private $MINUTE_ID;
	private $TASK_ID;
	private $CONCERN;
	private $RECOMMENDATION;
	private $CONCERN_RAISED_BY;
	private $ACTION_PLAN;
	private $RESPONSIBLE_LIST;
	private $DUE_DATE;
	private $INITIAL_DUE_DATE;
	private $RESPONSE_FROM_SENIOR_MANAGEMENT;
	private $COMPLETED_FLAG;
	private $COMPLETED_BY;
	private $COMPETED_DATE;
	private $completedFlag;
	private $completedFlag_level_1;
	private $permision;
	private $permision_final_completion;
	private $CREATED_BY;
	private $COMPLETED_BY_NAME_LEVEL_1;
	private $COMPLETED_BY_NAME;
	private $COMPLETED_FLAG_LEVEL_1;
	private $COMPLETED_BY_LEVEL_1;
	private $COMPETED_DATE_LEVEL_1;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('MINUTE_ID'=>'MINUTE_ID',
										'TASK_ID'=>'TASK_ID',
										'CONCERN'=>'CONCERN',
										'RECOMMENDATION'=>'RECOMMENDATION',
										'CONCERN_RAISED_BY'=>'CONCERN_RAISED_BY',
										'ACTION_PLAN'=>'ACTION_PLAN',
										'RESPONSIBLE_LIST'=>'RESPONSIBLE_LIST',
										'DUE_DATE'=>'DUE_DATE',
										'INITIAL_DUE_DATE'=>'INITIAL_DUE_DATE' ,
										'RESPONSE_FROM_SENIOR_MANAGEMENT'=>'RESPONSE_FROM_SENIOR_MANAGEMENT',
										'COMPLETED_FLAG'=>'COMPLETED_FLAG',
										'COMPLETED_BY'=>'COMPLETED_BY',
										'COMPETED_DATE'=>'COMPETED_DATE',
										'COMPLETED_FLAG_LEVEL_1'=>'COMPLETED_FLAG_LEVEL_1',
										'COMPLETED_BY_LEVEL_1'=>'COMPLETED_BY_LEVEL_1',
										'COMPETED_DATE_LEVEL_1'=>'COMPETED_DATE_LEVEL_1',
								 
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	function commit()
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "MINUTE_ID = ".$this->MINUTE_ID." and TASK_ID = ".$this->TASK_ID."" ;
		unset($this->commitArray);
		
		if($this->intId==NULL || $this->intId=='' || $this->intId=='NULL')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun MINUTE_ID
	function getMINUTE_ID()
	{
		$this->validate();
		return $this->MINUTE_ID;
	}
	
	//retun TASK_ID
	function getTASK_ID()
	{
		$this->validate();
		return $this->TASK_ID;
	}
	
	//retun CONCERN
	function getCONCERN()
	{
		$this->validate();
		return $this->CONCERN;
	}
	
	//retun RECOMMENDATION
	function getRECOMMENDATION()
	{
		$this->validate();
		return $this->RECOMMENDATION;
	}
	
	//retun CONCERN_RAISED_BY
	function getCONCERN_RAISED_BY()
	{
		$this->validate();
		return $this->CONCERN_RAISED_BY;
	}
	
	//retun ACTION_PLAN
	function getACTION_PLAN()
	{
		$this->validate();
		return $this->ACTION_PLAN;
	}
	
	//retun RESPONSIBLE_LIST
	function getRESPONSIBLE_LIST()
	{
		$this->validate();
		return $this->RESPONSIBLE_LIST;
	}
	
	//retun DUE_DATE
	function getDUE_DATE()
	{
		$this->validate();
		return $this->DUE_DATE;
	}
	
	//retun DUE_DATE
	function getINITIAL_DUE_DATE()
	{
		$this->validate();
		return $this->INITIAL_DUE_DATE;
	}

	//retun RESPONSE_FROM_SENIOR_MANAGEMENT
	function getRESPONSE_FROM_SENIOR_MANAGEMENT()
	{
		$this->validate();
		return $this->RESPONSE_FROM_SENIOR_MANAGEMENT;
	}
	
	//retun COMPLETED_FLAG
	function getCOMPLETED_FLAG()
	{
		$this->validate();
		return $this->COMPLETED_FLAG;
	}
	
	//retun COMPLETED_BY
	function getCOMPLETED_BY()
	{
		$this->validate();
		return $this->COMPLETED_BY;
	}
	
	//retun COMPETED_DATE
	function getCOMPETED_DATE()
	{
		$this->validate();
		return $this->COMPETED_DATE;
	}
	
	function getcompletedFlag()
	{
		$this->validate();
		return $this->completedFlag;
	}

	function getcompletedFlag_level_1()
	{
		$this->validate();
		return $this->completedFlag_level_1;
	}
	
	function getpermision()
	{
		$this->validate();
		return $this->permision;
	}
	
	function getpermision_final_completion()
	{
		$this->validate();
		return $this->permision_final_completion;
	}
	
	function getCREATED_BY()
	{
		$this->validate();
		return $this->CREATED_BY;
	}

	function getCREATED_BY_LEVEL_1()
	{
		$this->validate();
		return $this->CREATED_BY;
	}
	
	function getCOMPLETED_BY_NAME()
	{
		$this->validate();
		return $this->COMPLETED_BY_NAME;
	}

	function getCOMPLETED_BY_NAME_LEVEL_1()
	{
		$this->validate();
		return $this->COMPLETED_BY_NAME_LEVEL_1;
	}
	
	function getCOMPLETED_FLAG_LEVEL_1()
	{
		$this->validate();
		return $this->COMPLETED_FLAG_LEVEL_1;
	}
	
	function getCOMPLETED_BY_LEVEL_1()
	{
		$this->validate();
		return $this->COMPLETED_BY_LEVEL_1;
	}
	
	function getCOMPETED_DATE_LEVEL_1()
	{
		$this->validate();
		return $this->COMPETED_DATE_LEVEL_1;
	}

	//END }
	
	//BEGIN - public function set {
	
	//set MINUTE_ID
	function setMINUTE_ID($MINUTE_ID)
	{
		array_push($this->commitArray,'MINUTE_ID');
		$this->MINUTE_ID = $MINUTE_ID;
	}
	
	//set TASK_ID
	function setTASK_ID($TASK_ID)
	{
		array_push($this->commitArray,'TASK_ID');
		$this->TASK_ID = $TASK_ID;
	}
	
	//set CONCERN
	function setCONCERN($CONCERN)
	{
		array_push($this->commitArray,'CONCERN');
		$this->CONCERN = $CONCERN;
	}
	
	//set RECOMMENDATION
	function setRECOMMENDATION($RECOMMENDATION)
	{
		array_push($this->commitArray,'RECOMMENDATION');
		$this->RECOMMENDATION = $RECOMMENDATION;
	}
	
	//set CONCERN_RAISED_BY
	function setCONCERN_RAISED_BY($CONCERN_RAISED_BY)
	{
		array_push($this->commitArray,'CONCERN_RAISED_BY');
		$this->CONCERN_RAISED_BY = $CONCERN_RAISED_BY;
	}
	
	//set ACTION_PLAN
	function setACTION_PLAN($ACTION_PLAN)
	{
		array_push($this->commitArray,'ACTION_PLAN');
		$this->ACTION_PLAN = $ACTION_PLAN;
	}
	
	//set RESPONSIBLE_LIST
	function setRESPONSIBLE_LIST($RESPONSIBLE_LIST)
	{
		array_push($this->commitArray,'RESPONSIBLE_LIST');
		$this->RESPONSIBLE_LIST = $RESPONSIBLE_LIST;
	}
	
	//set DUE_DATE
	function setDUE_DATE($DUE_DATE)
	{
		array_push($this->commitArray,'DUE_DATE');
		$this->DUE_DATE = $DUE_DATE;
	}
	
	//set DUE_DATE
	function setINITIAL_DUE_DATE($INITIAL_DUE_DATE)
	{
		array_push($this->commitArray,'INITIAL_DUE_DATE');
		$this->INITIAL_DUE_DATE = $INITIAL_DUE_DATE;
	}
 
 	//set RESPONSE_FROM_SENIOR_MANAGEMENT
	function setRESPONSE_FROM_SENIOR_MANAGEMENT($RESPONSE_FROM_SENIOR_MANAGEMENT)
	{
		array_push($this->commitArray,'RESPONSE_FROM_SENIOR_MANAGEMENT');
		$this->RESPONSE_FROM_SENIOR_MANAGEMENT = $RESPONSE_FROM_SENIOR_MANAGEMENT;
	}
	
	//set COMPLETED_FLAG
	function setCOMPLETED_FLAG($COMPLETED_FLAG)
	{
		array_push($this->commitArray,'COMPLETED_FLAG');
		$this->COMPLETED_FLAG = $COMPLETED_FLAG;
	}
	
	//set COMPLETED_BY
	function setCOMPLETED_BY($COMPLETED_BY)
	{
		array_push($this->commitArray,'COMPLETED_BY');
		$this->COMPLETED_BY = $COMPLETED_BY;
	}
	
	//set COMPETED_DATE
	function setCOMPETED_DATE($COMPETED_DATE)
	{
		array_push($this->commitArray,'COMPETED_DATE');
		$this->COMPETED_DATE = $COMPETED_DATE;
	}
	
	//set COMPETED_DATE
	function setCOMPLETED_FLAG_LEVEL_1($COMPLETED_FLAG_LEVEL_1)
	{
		array_push($this->commitArray,'COMPLETED_FLAG_LEVEL_1');
		$this->COMPLETED_FLAG_LEVEL_1 = $COMPLETED_FLAG_LEVEL_1;
	}
	
	//set COMPETED_DATE
	function setCOMPLETED_BY_LEVEL_1($COMPLETED_BY_LEVEL_1)
	{
		array_push($this->commitArray,'COMPLETED_BY_LEVEL_1');
		$this->COMPLETED_BY_LEVEL_1 = $COMPLETED_BY_LEVEL_1;
	}

	//set COMPETED_DATE
	function setCOMPETED_DATE_LEVEL_1($COMPETED_DATE_LEVEL_1)
	{
		array_push($this->commitArray,'COMPETED_DATE_LEVEL_1');
		$this->COMPETED_DATE_LEVEL_1 = $COMPETED_DATE_LEVEL_1;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->MINUTE_ID=='' || $this->TASK_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($MINUTE_ID ,$TASK_ID ,$USET_ID)
	{
		$cols	= " other_meeting_minutes_details.MINUTE_ID,
					other_meeting_minutes_details.TASK_ID,
					other_meeting_minutes_details.COMPLETED_FLAG,
					IFNULL(other_meeting_minutes_details.COMPLETED_FLAG,0) as completedFlag,
					other_meeting_minutes_details.COMPLETED_FLAG_LEVEL_1,
					IFNULL(other_meeting_minutes_details.COMPLETED_FLAG_LEVEL_1,0) as completedFlag_level_1,
					other_meeting_minutes_details.CONCERN,
					other_meeting_minutes_details.RESPONSIBLE_LIST,
					other_meeting_minutes_header.CREATED_BY, 
					
					IF((SELECT
					concat(momd.RESPONSIBLE_LIST,',',momh.CREATED_BY) as permitedList  
					FROM
					other_meeting_minutes_details as momd
					INNER JOIN other_meeting_minutes_header as momh 
					ON momd.MINUTE_ID = momh.MINUTE_ID
					WHERE 
					other_meeting_minutes_details.MINUTE_ID = momd.MINUTE_ID AND
					other_meeting_minutes_details.TASK_ID = momd.TASK_ID 
					AND  
					FIND_IN_SET('$USET_ID',concat(momd.RESPONSIBLE_LIST,',',momh.CREATED_BY))
					) <> '',1,0) as permision ,
			
					(SELECT
					menupermision.int2Approval
					FROM `menupermision`
					WHERE
					menupermision.intMenuId = '718' AND
					menupermision.intUserId = '$USET_ID') as permision_final_completion ,
			
					other_meeting_minutes_details.INITIAL_DUE_DATE,
					(SELECT 
						sys_users.strFullName 
					FROM
					other_meeting_minutes_details as momd
					INNER JOIN sys_users ON momd.COMPLETED_BY = sys_users.intUserId
					WHERE
					momd.MINUTE_ID = other_meeting_minutes_details.MINUTE_ID AND
					momd.TASK_ID = other_meeting_minutes_details.TASK_ID) as COMPLETED_BY_NAME,
					(other_meeting_minutes_details.COMPETED_DATE) as COMPETED_DATE,
					(SELECT 
						sys_users.strFullName 
					FROM
					other_meeting_minutes_details as momd
					INNER JOIN sys_users ON momd.COMPLETED_BY_LEVEL_1 = sys_users.intUserId
					WHERE
					momd.MINUTE_ID = other_meeting_minutes_details.MINUTE_ID AND
					momd.TASK_ID = other_meeting_minutes_details.TASK_ID) as COMPLETED_BY_NAME_LEVEL_1,
					(other_meeting_minutes_details.COMPETED_DATE_LEVEL_1) as COMPETED_DATE_LEVEL_1 ";
		
		$join	= " INNER JOIN other_meeting_minutes_header ON other_meeting_minutes_details.MINUTE_ID = other_meeting_minutes_header.MINUTE_ID ";
		
		$where	= "other_meeting_minutes_details.MINUTE_ID = '$MINUTE_ID' AND other_meeting_minutes_details.TASK_ID = '$TASK_ID' ";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
		//else
			//throw new Exception("Record not found.");
	}	
	
	
	//END }
}
?>
<?php
class finance_supplier_advancepayment_header{
 
	private $db;
	private $table= "finance_supplier_advancepayment_header";
	
	//private property
	private $ADVANCE_PAYMENT_NO;
	private $ADVANCE_PAYMENT_YEAR;
	private $VOUCHER_NO;
	private $SUPPLIER_ID;
	private $PO_NO;
	private $PO_YEAR;
	private $PAY_DATE;
	private $COMPANY_ID;
	private $CURRENCY_ID;
	private $PAY_AMOUNT;
	private $PAY_METHOD;
	private $REMARKS;
	private $BANK_REFERENCE_NO;
	private $STATUS;
	private $PRINT_STATUS;
	private $CREATED_BY;
	private $CREATED_DATE;
	private $LAST_MODIFY_BY;
	private $LAST_MODIFY_DATE;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('ADVANCE_PAYMENT_NO'=>'ADVANCE_PAYMENT_NO',
										'ADVANCE_PAYMENT_YEAR'=>'ADVANCE_PAYMENT_YEAR',
										'VOUCHER_NO'=>'VOUCHER_NO',
										'SUPPLIER_ID'=>'SUPPLIER_ID',
										'PO_NO'=>'PO_NO',
										'PO_YEAR'=>'PO_YEAR',
										'PAY_DATE'=>'PAY_DATE',
										'COMPANY_ID'=>'COMPANY_ID',
										'CURRENCY_ID'=>'CURRENCY_ID',
										'PAY_AMOUNT'=>'PAY_AMOUNT',
										'PAY_METHOD'=>'PAY_METHOD',
										'REMARKS'=>'REMARKS',
										'BANK_REFERENCE_NO'=>'BANK_REFERENCE_NO',
										'STATUS'=>'STATUS',
										'PRINT_STATUS'=>'PRINT_STATUS',
										'CREATED_BY'=>'CREATED_BY',
										'CREATED_DATE'=>'CREATED_DATE',
										'LAST_MODIFY_BY'=>'LAST_MODIFY_BY',
										'LAST_MODIFY_DATE'=>'LAST_MODIFY_DATE',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "ADVANCE_PAYMENT_NO = ".$this->ADVANCE_PAYMENT_NO." and ADVANCE_PAYMENT_YEAR = ".$this->ADVANCE_PAYMENT_YEAR." and SUPPLIER_ID = ".$this->SUPPLIER_ID." and PO_NO = ".$this->PO_NO." and PO_YEAR = ".$this->PO_YEAR."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun ADVANCE_PAYMENT_NO
	function getADVANCE_PAYMENT_NO()
	{
		$this->validate();
		return $this->ADVANCE_PAYMENT_NO;
	}
	
	//retun ADVANCE_PAYMENT_YEAR
	function getADVANCE_PAYMENT_YEAR()
	{
		$this->validate();
		return $this->ADVANCE_PAYMENT_YEAR;
	}
	
	//retun VOUCHER_NO
	function getVOUCHER_NO()
	{
		$this->validate();
		return $this->VOUCHER_NO;
	}
	
	//retun SUPPLIER_ID
	function getSUPPLIER_ID()
	{
		$this->validate();
		return $this->SUPPLIER_ID;
	}
	
	//retun PO_NO
	function getPO_NO()
	{
		$this->validate();
		return $this->PO_NO;
	}
	
	//retun PO_YEAR
	function getPO_YEAR()
	{
		$this->validate();
		return $this->PO_YEAR;
	}
	
	//retun PAY_DATE
	function getPAY_DATE()
	{
		$this->validate();
		return $this->PAY_DATE;
	}
	
	//retun COMPANY_ID
	function getCOMPANY_ID()
	{
		$this->validate();
		return $this->COMPANY_ID;
	}
	
	//retun CURRENCY_ID
	function getCURRENCY_ID()
	{
		$this->validate();
		return $this->CURRENCY_ID;
	}
	
	//retun PAY_AMOUNT
	function getPAY_AMOUNT()
	{
		$this->validate();
		return $this->PAY_AMOUNT;
	}
	
	//retun PAY_METHOD
	function getPAY_METHOD()
	{
		$this->validate();
		return $this->PAY_METHOD;
	}
	
	//retun REMARKS
	function getREMARKS()
	{
		$this->validate();
		return $this->REMARKS;
	}
	
	//retun BANK_REFERENCE_NO
	function getBANK_REFERENCE_NO()
	{
		$this->validate();
		return $this->BANK_REFERENCE_NO;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//retun PRINT_STATUS
	function getPRINT_STATUS()
	{
		$this->validate();
		return $this->PRINT_STATUS;
	}
	
	//retun CREATED_BY
	function getCREATED_BY()
	{
		$this->validate();
		return $this->CREATED_BY;
	}
	
	//retun CREATED_DATE
	function getCREATED_DATE()
	{
		$this->validate();
		return $this->CREATED_DATE;
	}
	
	//retun LAST_MODIFY_BY
	function getLAST_MODIFY_BY()
	{
		$this->validate();
		return $this->LAST_MODIFY_BY;
	}
	
	//retun LAST_MODIFY_DATE
	function getLAST_MODIFY_DATE()
	{
		$this->validate();
		return $this->LAST_MODIFY_DATE;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set ADVANCE_PAYMENT_NO
	function setADVANCE_PAYMENT_NO($ADVANCE_PAYMENT_NO)
	{
		array_push($this->commitArray,'ADVANCE_PAYMENT_NO');
		$this->ADVANCE_PAYMENT_NO = $ADVANCE_PAYMENT_NO;
	}
	
	//set ADVANCE_PAYMENT_YEAR
	function setADVANCE_PAYMENT_YEAR($ADVANCE_PAYMENT_YEAR)
	{
		array_push($this->commitArray,'ADVANCE_PAYMENT_YEAR');
		$this->ADVANCE_PAYMENT_YEAR = $ADVANCE_PAYMENT_YEAR;
	}
	
	//set VOUCHER_NO
	function setVOUCHER_NO($VOUCHER_NO)
	{
		array_push($this->commitArray,'VOUCHER_NO');
		$this->VOUCHER_NO = $VOUCHER_NO;
	}
	
	//set SUPPLIER_ID
	function setSUPPLIER_ID($SUPPLIER_ID)
	{
		array_push($this->commitArray,'SUPPLIER_ID');
		$this->SUPPLIER_ID = $SUPPLIER_ID;
	}
	
	//set PO_NO
	function setPO_NO($PO_NO)
	{
		array_push($this->commitArray,'PO_NO');
		$this->PO_NO = $PO_NO;
	}
	
	//set PO_YEAR
	function setPO_YEAR($PO_YEAR)
	{
		array_push($this->commitArray,'PO_YEAR');
		$this->PO_YEAR = $PO_YEAR;
	}
	
	//set PAY_DATE
	function setPAY_DATE($PAY_DATE)
	{
		array_push($this->commitArray,'PAY_DATE');
		$this->PAY_DATE = $PAY_DATE;
	}
	
	//set COMPANY_ID
	function setCOMPANY_ID($COMPANY_ID)
	{
		array_push($this->commitArray,'COMPANY_ID');
		$this->COMPANY_ID = $COMPANY_ID;
	}
	
	//set CURRENCY_ID
	function setCURRENCY_ID($CURRENCY_ID)
	{
		array_push($this->commitArray,'CURRENCY_ID');
		$this->CURRENCY_ID = $CURRENCY_ID;
	}
	
	//set PAY_AMOUNT
	function setPAY_AMOUNT($PAY_AMOUNT)
	{
		array_push($this->commitArray,'PAY_AMOUNT');
		$this->PAY_AMOUNT = $PAY_AMOUNT;
	}
	
	//set PAY_METHOD
	function setPAY_METHOD($PAY_METHOD)
	{
		array_push($this->commitArray,'PAY_METHOD');
		$this->PAY_METHOD = $PAY_METHOD;
	}
	
	//set REMARKS
	function setREMARKS($REMARKS)
	{
		array_push($this->commitArray,'REMARKS');
		$this->REMARKS = $REMARKS;
	}
	
	//set BANK_REFERENCE_NO
	function setBANK_REFERENCE_NO($BANK_REFERENCE_NO)
	{
		array_push($this->commitArray,'BANK_REFERENCE_NO');
		$this->BANK_REFERENCE_NO = $BANK_REFERENCE_NO;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//set PRINT_STATUS
	function setPRINT_STATUS($PRINT_STATUS)
	{
		array_push($this->commitArray,'PRINT_STATUS');
		$this->PRINT_STATUS = $PRINT_STATUS;
	}
	
	//set CREATED_BY
	function setCREATED_BY($CREATED_BY)
	{
		array_push($this->commitArray,'CREATED_BY');
		$this->CREATED_BY = $CREATED_BY;
	}
	
	//set CREATED_DATE
	function setCREATED_DATE($CREATED_DATE)
	{
		array_push($this->commitArray,'CREATED_DATE');
		$this->CREATED_DATE = $CREATED_DATE;
	}
	
	//set LAST_MODIFY_BY
	function setLAST_MODIFY_BY($LAST_MODIFY_BY)
	{
		array_push($this->commitArray,'LAST_MODIFY_BY');
		$this->LAST_MODIFY_BY = $LAST_MODIFY_BY;
	}
	
	//set LAST_MODIFY_DATE
	function setLAST_MODIFY_DATE($LAST_MODIFY_DATE)
	{
		array_push($this->commitArray,'LAST_MODIFY_DATE');
		$this->LAST_MODIFY_DATE = $LAST_MODIFY_DATE;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->ADVANCE_PAYMENT_NO=='' || $this->ADVANCE_PAYMENT_YEAR=='' || $this->SUPPLIER_ID=='' || $this->PO_NO=='' || $this->PO_YEAR=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($ADVANCE_PAYMENT_NO , $ADVANCE_PAYMENT_YEAR)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "ADVANCE_PAYMENT_NO='$ADVANCE_PAYMENT_NO' and ADVANCE_PAYMENT_YEAR='$ADVANCE_PAYMENT_YEAR' ";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($ADVANCE_PAYMENT_NO,$ADVANCE_PAYMENT_YEAR,$VOUCHER_NO,$SUPPLIER_ID,$PO_NO,$PO_YEAR,$PAY_DATE,$COMPANY_ID,$CURRENCY_ID,$PAY_AMOUNT,$PAY_METHOD,$REMARKS,$BANK_REFERENCE_NO,$STATUS,$PRINT_STATUS,$CREATED_BY,$CREATED_DATE,$LAST_MODIFY_BY,$LAST_MODIFY_DATE){
		$data = array('ADVANCE_PAYMENT_NO'=>$ADVANCE_PAYMENT_NO 
				,'ADVANCE_PAYMENT_YEAR'=>$ADVANCE_PAYMENT_YEAR 
				,'VOUCHER_NO'=>$VOUCHER_NO 
				,'SUPPLIER_ID'=>$SUPPLIER_ID 
				,'PO_NO'=>$PO_NO 
				,'PO_YEAR'=>$PO_YEAR 
				,'PAY_DATE'=>$PAY_DATE 
				,'COMPANY_ID'=>$COMPANY_ID 
				,'CURRENCY_ID'=>$CURRENCY_ID 
				,'PAY_AMOUNT'=>$PAY_AMOUNT 
				,'PAY_METHOD'=>$PAY_METHOD 
				,'REMARKS'=>$REMARKS 
				,'BANK_REFERENCE_NO'=>$BANK_REFERENCE_NO 
				,'STATUS'=>$STATUS 
				,'PRINT_STATUS'=>$PRINT_STATUS 
				,'CREATED_BY'=>$CREATED_BY 
				,'CREATED_DATE'=>$CREATED_DATE 
				,'LAST_MODIFY_BY'=>$LAST_MODIFY_BY 
				,'LAST_MODIFY_DATE'=>$LAST_MODIFY_DATE 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('ADVANCE_PAYMENT_NO,ADVANCE_PAYMENT_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['ADVANCE_PAYMENT_NO'])
				$html .= '<option selected="selected" value="'.$row['ADVANCE_PAYMENT_NO'].'">'.$row['ADVANCE_PAYMENT_YEAR'].'</option>';	
			else
				$html .= '<option value="'.$row['ADVANCE_PAYMENT_NO'].'">'.$row['ADVANCE_PAYMENT_YEAR'].'</option>';	
		}
		return $html;
	}
	
	public function getAdvancedSupplier($companyId)
	{
		$cols 	= " DISTINCT
				    mst_supplier.intId   	AS SUPPLIER_ID,
				    mst_supplier.strName 	AS SUPPLIER_NAME ";
		
		$join 	= " INNER JOIN  mst_supplier
    				ON mst_supplier.intId = finance_supplier_advancepayment_header.SUPPLIER_ID ";
					
		$where 	= " finance_supplier_advancepayment_header.STATUS = 1
    				AND finance_supplier_advancepayment_header.COMPANY_ID = '$companyId' ";
		
		$order	= " mst_supplier.strName";
		return $this->select($cols,$join,$where,$order,$limit=null);
	}
	public function getSupplierWiseCurrency($supplierId,$companyId)
	{
		$cols 	= " DISTINCT
				    mst_financecurrency.intId   	AS CURRENCY_ID,
				    mst_financecurrency.strCode 	AS CURRENCY_CODE ";
		
		$join 	= " INNER JOIN  mst_financecurrency
    				ON mst_financecurrency.intId = finance_supplier_advancepayment_header.CURRENCY_ID  ";
					
		$where 	= " finance_supplier_advancepayment_header.STATUS = 1
					AND finance_supplier_advancepayment_header.SUPPLIER_ID = '$supplierId'
    				AND finance_supplier_advancepayment_header.COMPANY_ID = '$companyId' ";
		
		$order	= " mst_financecurrency.strCode";
		return $this->select($cols,$join,$where,$order,$limit=null);
	}
	//END }
}
?>
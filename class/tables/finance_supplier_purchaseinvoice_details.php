<?php
class finance_supplier_purchaseinvoice_details{
 
	private $db;
	private $table= "finance_supplier_purchaseinvoice_details";
	
	//private property
	private $PURCHASE_INVOICE_NO;
	private $PURCHASE_INVOICE_YEAR;
	private $ITEM_ID;
	private $QTY;
	private $UNIT_PRICE;
	private $DISCOUNT;
	private $TAX_AMOUNT;
	private $TAX_CODE;
	private $COST_CENTER;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('PURCHASE_INVOICE_NO'=>'PURCHASE_INVOICE_NO',
										'PURCHASE_INVOICE_YEAR'=>'PURCHASE_INVOICE_YEAR',
										'ITEM_ID'=>'ITEM_ID',
										'QTY'=>'QTY',
										'UNIT_PRICE'=>'UNIT_PRICE',
										'DISCOUNT'=>'DISCOUNT',
										'TAX_AMOUNT'=>'TAX_AMOUNT',
										'TAX_CODE'=>'TAX_CODE',
										'COST_CENTER'=>'COST_CENTER',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "PURCHASE_INVOICE_NO = ".$this->PURCHASE_INVOICE_NO." and PURCHASE_INVOICE_YEAR = ".$this->PURCHASE_INVOICE_YEAR." and ITEM_ID = ".$this->ITEM_ID."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun PURCHASE_INVOICE_NO
	function getPURCHASE_INVOICE_NO()
	{
		$this->validate();
		return $this->PURCHASE_INVOICE_NO;
	}
	
	//retun PURCHASE_INVOICE_YEAR
	function getPURCHASE_INVOICE_YEAR()
	{
		$this->validate();
		return $this->PURCHASE_INVOICE_YEAR;
	}
	
	//retun ITEM_ID
	function getITEM_ID()
	{
		$this->validate();
		return $this->ITEM_ID;
	}
	
	//retun QTY
	function getQTY()
	{
		$this->validate();
		return $this->QTY;
	}
	
	//retun UNIT_PRICE
	function getUNIT_PRICE()
	{
		$this->validate();
		return $this->UNIT_PRICE;
	}
	
	//retun DISCOUNT
	function getDISCOUNT()
	{
		$this->validate();
		return $this->DISCOUNT;
	}
	
	//retun TAX_AMOUNT
	function getTAX_AMOUNT()
	{
		$this->validate();
		return $this->TAX_AMOUNT;
	}
	
	//retun TAX_CODE
	function getTAX_CODE()
	{
		$this->validate();
		return $this->TAX_CODE;
	}
	
	//retun COST_CENTER
	function getCOST_CENTER()
	{
		$this->validate();
		return $this->COST_CENTER;
	}
	
	//END }

	//set PURCHASE_INVOICE_NO
	function setPURCHASE_INVOICE_NO($PURCHASE_INVOICE_NO)
	{
		array_push($this->commitArray,'PURCHASE_INVOICE_NO');
		$this->PURCHASE_INVOICE_NO = $PURCHASE_INVOICE_NO;
	}
	
	//set PURCHASE_INVOICE_YEAR
	function setPURCHASE_INVOICE_YEAR($PURCHASE_INVOICE_YEAR)
	{
		array_push($this->commitArray,'PURCHASE_INVOICE_YEAR');
		$this->PURCHASE_INVOICE_YEAR = $PURCHASE_INVOICE_YEAR;
	}
	
	//set ITEM_ID
	function setITEM_ID($ITEM_ID)
	{
		array_push($this->commitArray,'ITEM_ID');
		$this->ITEM_ID = $ITEM_ID;
	}
	
	//set QTY
	function setQTY($QTY)
	{
		array_push($this->commitArray,'QTY');
		$this->QTY = $QTY;
	}
	
	//set UNIT_PRICE
	function setUNIT_PRICE($UNIT_PRICE)
	{
		array_push($this->commitArray,'UNIT_PRICE');
		$this->UNIT_PRICE = $UNIT_PRICE;
	}
	
	//set DISCOUNT
	function setDISCOUNT($DISCOUNT)
	{
		array_push($this->commitArray,'DISCOUNT');
		$this->DISCOUNT = $DISCOUNT;
	}
	
	//set TAX_AMOUNT
	function setTAX_AMOUNT($TAX_AMOUNT)
	{
		array_push($this->commitArray,'TAX_AMOUNT');
		$this->TAX_AMOUNT = $TAX_AMOUNT;
	}
	
	//set TAX_CODE
	function setTAX_CODE($TAX_CODE)
	{
		array_push($this->commitArray,'TAX_CODE');
		$this->TAX_CODE = $TAX_CODE;
	}
	
	//set COST_CENTER
	function setCOST_CENTER($COST_CENTER)
	{
		array_push($this->commitArray,'COST_CENTER');
		$this->COST_CENTER = $COST_CENTER;
	}
	
	//validate primary values
	private function validate()
	{
		if($this->PURCHASE_INVOICE_NO=='' || $this->PURCHASE_INVOICE_YEAR=='' || $this->ITEM_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($PURCHASE_INVOICE_NO , $PURCHASE_INVOICE_YEAR , $ITEM_ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "PURCHASE_INVOICE_NO='$PURCHASE_INVOICE_NO' and PURCHASE_INVOICE_YEAR='$PURCHASE_INVOICE_YEAR' and ITEM_ID='$ITEM_ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	public function insertRec($PURCHASE_INVOICE_NO,$PURCHASE_INVOICE_YEAR,$ITEM_ID,$QTY,$UNIT_PRICE,$DISCOUNT,$TAX_AMOUNT,$TAX_CODE,$COST_CENTER){
		$data = array('PURCHASE_INVOICE_NO'=>$PURCHASE_INVOICE_NO 
				,'PURCHASE_INVOICE_YEAR'=>$PURCHASE_INVOICE_YEAR 
				,'ITEM_ID'=>$ITEM_ID 
				,'QTY'=>$QTY 
				,'UNIT_PRICE'=>$UNIT_PRICE 
				,'DISCOUNT'=>$DISCOUNT 
				,'TAX_AMOUNT'=>$TAX_AMOUNT 
				,'TAX_CODE'=>$TAX_CODE 
				,'COST_CENTER'=>$COST_CENTER 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('PURCHASE_INVOICE_NO,PURCHASE_INVOICE_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['PURCHASE_INVOICE_NO'])
				$html .= '<option selected="selected" value="'.$row['PURCHASE_INVOICE_NO'].'">'.$row['PURCHASE_INVOICE_YEAR'].'</option>';	
			else
				$html .= '<option value="'.$row['PURCHASE_INVOICE_NO'].'">'.$row['PURCHASE_INVOICE_YEAR'].'</option>';	
		}
		return $html;
	}
	
#BEGIN 	- USER DEFINE FUNCTIONS {
	public function getInvoiceDetails($invoiceNo,$invoiceYear)
	{
		$cols 	= "mst_item.intMainCategory		 	AS MAIN_CAT_ID,
				   mst_item.intSubCategory  		AS SUB_CAT_ID,
				   mst_item.strName         		AS ITEM_NAME,
				   mst_item.intId         		    AS ITEM_ID,
				   mst_item.intUOM          		AS UNIT_ID,
				   UNIT_PRICE       				AS UNIT_PRICE,
				   QTY              				AS QTY,
				   ROUND(UNIT_PRICE * QTY,2)		AS AMOUNT,
				   DISCOUNT         				AS DISCOUNT,
				   TAX_CODE         				AS TAX_CODE,
				   TAX_AMOUNT						AS TAX_AMOUNT,
				   COST_CENTER      				AS COST_CENTER,
				   (
					SELECT SPGL.GLACCOUNT_ID
					FROM finance_supplier_purchaseinvoice_gl SPGL
					INNER JOIN finance_mst_chartofaccount_subcategory COAS ON COAS.CHART_OF_ACCOUNT_ID=SPGL.GLACCOUNT_ID
					WHERE SPGL.PURCHASE_INVOICE_NO = finance_supplier_purchaseinvoice_details.PURCHASE_INVOICE_NO AND
					SPGL.PURCHASE_INVOICE_YEAR = finance_supplier_purchaseinvoice_details.PURCHASE_INVOICE_YEAR AND
					COAS.SUBCATEGORY_ID = mst_item.intSubCategory  
					) AS GLACCOUNT_ID,					
				  (SELECT
					 T.CHART_OF_ACCOUNT_ID
				   FROM finance_mst_chartofaccount_tax T
				   WHERE T.TAX_ID = finance_supplier_purchaseinvoice_details.TAX_CODE) AS TAX_GL_ID ";
		
		$join 	= "INNER JOIN mst_item
    				ON mst_item.intId = finance_supplier_purchaseinvoice_details.ITEM_ID ";
					
		$where 	= "finance_supplier_purchaseinvoice_details.PURCHASE_INVOICE_NO = $invoiceNo
    				AND finance_supplier_purchaseinvoice_details.PURCHASE_INVOICE_YEAR = $invoiceYear";
		
		$order	= "mst_item.strName";
		return $this->select($cols,$join,$where,$order,$limit=null);
	}
	
	public function getItemWiseApprovedInvoiceValur($invoiceNo,$invoiceYear,$itemId)
	{
		$cols	= "ROUND(COALESCE(SUM(UNIT_PRICE * QTY),0),2) AS TOTAL_AMOUNT ";
	
		$join 	= "INNER JOIN finance_supplier_purchaseinvoice_header H
				       ON H.PURCHASE_INVOICE_NO = finance_supplier_purchaseinvoice_details.PURCHASE_INVOICE_NO
					   AND H.PURCHASE_INVOICE_YEAR = finance_supplier_purchaseinvoice_details.PURCHASE_INVOICE_YEAR";
		
		$where	= "H.STATUS = 1
				   AND H.PURCHASE_INVOICE_NO = $invoiceNo 
				   AND H.PURCHASE_INVOICE_YEAR = $invoiceYear
				   AND finance_supplier_purchaseinvoice_details.ITEM_ID = $itemId";
		
		$result = $this->select($cols,$join,$where);
		$row	= mysqli_fetch_array($result);
		return $row['TOTAL_AMOUNT'];
	}
#END 	- USER DEFINE FUNCTIONS }
}
?>
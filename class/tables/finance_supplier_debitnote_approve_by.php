<?php
class finance_supplier_debitnote_approve_by{
 
	private $db;
	private $table= "finance_supplier_debitnote_approve_by";
	
	//private property
	private $DEBIT_NO;
	private $DEBIT_YEAR;
	private $APPROVE_LEVEL;
	private $APPROVE_BY;
	private $APPROVE_DATE;
	private $STATUS;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('DEBIT_NO'=>'DEBIT_NO',
										'DEBIT_YEAR'=>'DEBIT_YEAR',
										'APPROVE_LEVEL'=>'APPROVE_LEVEL',
										'APPROVE_BY'=>'APPROVE_BY',
										'APPROVE_DATE'=>'APPROVE_DATE',
										'STATUS'=>'STATUS',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "DEBIT_NO = ".$this->DEBIT_NO." and DEBIT_YEAR = ".$this->DEBIT_YEAR." and APPROVE_LEVEL = ".$this->APPROVE_LEVEL." and STATUS = ".$this->STATUS."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun DEBIT_NO
	function getDEBIT_NO()
	{
		$this->validate();
		return $this->DEBIT_NO;
	}
	
	//retun DEBIT_YEAR
	function getDEBIT_YEAR()
	{
		$this->validate();
		return $this->DEBIT_YEAR;
	}
	
	//retun APPROVE_LEVEL
	function getAPPROVE_LEVEL()
	{
		$this->validate();
		return $this->APPROVE_LEVEL;
	}
	
	//retun APPROVE_BY
	function getAPPROVE_BY()
	{
		$this->validate();
		return $this->APPROVE_BY;
	}
	
	//retun APPROVE_DATE
	function getAPPROVE_DATE()
	{
		$this->validate();
		return $this->APPROVE_DATE;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set DEBIT_NO
	function setDEBIT_NO($DEBIT_NO)
	{
		array_push($this->commitArray,'DEBIT_NO');
		$this->DEBIT_NO = $DEBIT_NO;
	}
	
	//set DEBIT_YEAR
	function setDEBIT_YEAR($DEBIT_YEAR)
	{
		array_push($this->commitArray,'DEBIT_YEAR');
		$this->DEBIT_YEAR = $DEBIT_YEAR;
	}
	
	//set APPROVE_LEVEL
	function setAPPROVE_LEVEL($APPROVE_LEVEL)
	{
		array_push($this->commitArray,'APPROVE_LEVEL');
		$this->APPROVE_LEVEL = $APPROVE_LEVEL;
	}
	
	//set APPROVE_BY
	function setAPPROVE_BY($APPROVE_BY)
	{
		array_push($this->commitArray,'APPROVE_BY');
		$this->APPROVE_BY = $APPROVE_BY;
	}
	
	//set APPROVE_DATE
	function setAPPROVE_DATE($APPROVE_DATE)
	{
		array_push($this->commitArray,'APPROVE_DATE');
		$this->APPROVE_DATE = $APPROVE_DATE;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->DEBIT_NO=='' || $this->DEBIT_YEAR=='' || $this->APPROVE_LEVEL=='' || $this->STATUS=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($DEBIT_NO , $DEBIT_YEAR , $APPROVE_LEVEL , $STATUS)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "DEBIT_NO='$DEBIT_NO' and DEBIT_YEAR='$DEBIT_YEAR' and APPROVE_LEVEL='$APPROVE_LEVEL' and STATUS='$STATUS'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($DEBIT_NO,$DEBIT_YEAR,$APPROVE_LEVEL,$APPROVE_BY,$APPROVE_DATE,$STATUS){
		$data = array('DEBIT_NO'=>$DEBIT_NO 
				,'DEBIT_YEAR'=>$DEBIT_YEAR 
				,'APPROVE_LEVEL'=>$APPROVE_LEVEL 
				,'APPROVE_BY'=>$APPROVE_BY 
				,'APPROVE_DATE'=>$APPROVE_DATE 
				,'STATUS'=>$STATUS 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('DEBIT_NO,DEBIT_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['DEBIT_NO'])
				$html .= '<option selected="selected" value="'.$row['DEBIT_NO'].'">'.$row['DEBIT_YEAR'].'</option>';	
			else
				$html .= '<option value="'.$row['DEBIT_NO'].'">'.$row['DEBIT_YEAR'].'</option>';	
		}
		return $html;
	}
	
	public function updateMaxStatus($serialNo,$serialYear)
	{
		$cols		= " MAX(STATUS) AS MAX_STATUS ";
		$where		= " DEBIT_NO = '".$serialNo."' AND
						DEBIT_YEAR = '".$serialYear."' ";
	
		$result 	= $this->select($cols,$join = null,$where, $order = null, $limit = null);
		$row		= mysqli_fetch_array($result);
		
		$maxStatus	= $row['MAX_STATUS']+1;
	
		$data  		= array('STATUS' => $maxStatus);
		
		$where		= ' DEBIT_NO = "'.$serialNo.'" AND 
						DEBIT_YEAR = "'.$serialYear.'" AND
						STATUS = 0 ' ;
						
		$result_arr	= $this->update($data,$where);
		if(!$result_arr['status'])
			throw new Exception($result_arr['msg']);
	}
	public function getApproveByData($serialNo,$serialYear)
	{
		$cols			= " APPROVE_DATE AS dtApprovedDate,
							SU.strUserName AS UserName,
							APPROVE_LEVEL AS intApproveLevelNo ";
							
		$join 			= " INNER JOIN sys_users SU ON finance_supplier_debitnote_approve_by.APPROVE_BY = SU.intUserId ";
		
		$where			= " finance_supplier_debitnote_approve_by.DEBIT_NO = '$serialNo' AND
							finance_supplier_debitnote_approve_by.DEBIT_YEAR = '$serialYear' ";	
		
		$order			= " finance_supplier_debitnote_approve_by.APPROVE_DATE ASC ";
											
		$result			= $this->select($cols,$join,$where,$order,NULL);
		return $result;
	}
	//END }
	
}
?>
<?php
class trn_sampleinfomations_prices{

	private $db;
	private $table= "trn_sampleinfomations_prices";

	//private property
	private $intSampleNo;
	private $intSampleYear;
	private $intRevisionNo;
	private $strCombo;
	private $strPrintName;
	private $intCurrency;
	private $dblPrice;
	private $dblRMCost;
	private $dtEnterDate;
	private $intEnterBy;
	private $commitArray = array();
	private $field_array = array();
	

	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('intSampleNo'=>'intSampleNo',
										'intSampleYear'=>'intSampleYear',
										'intRevisionNo'=>'intRevisionNo',
										'strCombo'=>'strCombo',
										'strPrintName'=>'strPrintName',
										'intCurrency'=>'intCurrency',
										'dblPrice'=>'dblPrice',
										'dblRMCost'=>'dblRMCost',
										'dtEnterDate'=>'dtEnterDate',
										'intEnterBy'=>'intEnterBy',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit );
		return $result;
	}

	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data);
		return $this->db->getResult();


	}

	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}

	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();
	}

	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}

	public function get_field_array(){
		return $this->field_array;
	}

	function commit($type='update')
	{
		//commit update
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{
			$data[$v] = $this->$v;
		}
		$where		= "intSampleNo = ".$this->intSampleNo." and intSampleYear = ".$this->intSampleYear." and intRevisionNo = ".$this->intRevisionNo." and strCombo = ".$this->strCombo." and strPrintName = ".$this->strPrintName."" ;
		$this->commitArray = array();

		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}

	//BEGIN - public functions for private properties {
	
	//retun intSampleNo
	function getintSampleNo()
	{
		$this->validate();
		return $this->intSampleNo;
	}
	
	//retun intSampleYear
	function getintSampleYear()
	{
		$this->validate();
		return $this->intSampleYear;
	}
	
	//retun intRevisionNo
	function getintRevisionNo()
	{
		$this->validate();
		return $this->intRevisionNo;
	}
	
	//retun strCombo
	function getstrCombo()
	{
		$this->validate();
		return $this->strCombo;
	}
	
	//retun strPrintName
	function getstrPrintName()
	{
		$this->validate();
		return $this->strPrintName;
	}
	
	//retun intCurrency
	function getintCurrency()
	{
		$this->validate();
		return $this->intCurrency;
	}
	
	//retun dblPrice
	function getdblPrice()
	{
		$this->validate();
		return $this->dblPrice;
	}
	
	//retun dblRMCost
	function getdblRMCost()
	{
		$this->validate();
		return $this->dblRMCost;
	}
	
	//retun dtEnterDate
	function getdtEnterDate()
	{
		$this->validate();
		return $this->dtEnterDate;
	}
	
	//retun intEnterBy
	function getintEnterBy()
	{
		$this->validate();
		return $this->intEnterBy;
	}
	
	//END }

	//BEGIN - public function set {
	
	//set intSampleNo
	function setintSampleNo($intSampleNo)
	{
		array_push($this->commitArray,'intSampleNo');
		$this->intSampleNo = $intSampleNo;
	}
	
	//set intSampleYear
	function setintSampleYear($intSampleYear)
	{
		array_push($this->commitArray,'intSampleYear');
		$this->intSampleYear = $intSampleYear;
	}
	
	//set intRevisionNo
	function setintRevisionNo($intRevisionNo)
	{
		array_push($this->commitArray,'intRevisionNo');
		$this->intRevisionNo = $intRevisionNo;
	}
	
	//set strCombo
	function setstrCombo($strCombo)
	{
		array_push($this->commitArray,'strCombo');
		$this->strCombo = $strCombo;
	}
	
	//set strPrintName
	function setstrPrintName($strPrintName)
	{
		array_push($this->commitArray,'strPrintName');
		$this->strPrintName = $strPrintName;
	}
	
	//set intCurrency
	function setintCurrency($intCurrency)
	{
		array_push($this->commitArray,'intCurrency');
		$this->intCurrency = $intCurrency;
	}
	
	//set dblPrice
	function setdblPrice($dblPrice)
	{
		array_push($this->commitArray,'dblPrice');
		$this->dblPrice = $dblPrice;
	}
	
	//set dblRMCost
	function setdblRMCost($dblRMCost)
	{
		array_push($this->commitArray,'dblRMCost');
		$this->dblRMCost = $dblRMCost;
	}
	
	//set dtEnterDate
	function setdtEnterDate($dtEnterDate)
	{
		array_push($this->commitArray,'dtEnterDate');
		$this->dtEnterDate = $dtEnterDate;
	}
	
	//set intEnterBy
	function setintEnterBy($intEnterBy)
	{
		array_push($this->commitArray,'intEnterBy');
		$this->intEnterBy = $intEnterBy;
	}
	
	//END }

	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->intSampleNo=='' || $this->intSampleYear=='' || $this->intRevisionNo=='' || $this->strCombo=='' || $this->strPrintName=='')
			throw new exception("Set primary values first");
		else
			return true;
	}

	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}

	public function set($intSampleNo , $intSampleYear , $intRevisionNo , $strCombo , $strPrintName)
	{
		$cols	= "*";

		$join	= NULL;

		$where	= "intSampleNo='$intSampleNo' and intSampleYear='$intSampleYear' and intRevisionNo='$intRevisionNo' and strCombo='$strCombo' and strPrintName='$strPrintName'";

		$result = $this->select($cols,$join,$where);
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}

	//insert as parameters
	public function insertRec($intSampleNo,$intSampleYear,$intRevisionNo,$strCombo,$strPrintName,$intCurrency,$dblPrice,$dblRMCost,$dtEnterDate,$intEnterBy){
		$data = array('intSampleNo'=>$intSampleNo 
				,'intSampleYear'=>$intSampleYear 
				,'intRevisionNo'=>$intRevisionNo 
				,'strCombo'=>$strCombo 
				,'strPrintName'=>$strPrintName 
				,'intCurrency'=>$intCurrency 
				,'dblPrice'=>$dblPrice 
				,'dblRMCost'=>$dblRMCost 
				,'dtEnterDate'=>$dtEnterDate 
				,'intEnterBy'=>$intEnterBy 
				);
		return $this->insert($data);
	}

	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('intSampleNo,intSampleYear',  null, $where = $where);
		$html = '<option value=""></option>';
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intSampleNo'])
				$html .= '<option selected="selected" value="'.$row['intSampleNo'].'">'.$row['intSampleYear'].'</option>';
			else
				$html .= '<option value="'.$row['intSampleNo'].'">'.$row['intSampleYear'].'</option>';
		}
		return $html;
	}
	
	//END }
}
?>
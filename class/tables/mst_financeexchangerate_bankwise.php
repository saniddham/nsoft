<?php
class mst_financeexchangerate_bankwise{
 
	private $db;
	private $table= "mst_financeexchangerate_bankwise";
	
	//private property
	private $BANK_ID;
	private $CURRENCY_ID;
	private $DATE;
	private $COMPANY_ID;
	private $SELLING_RATE;
	private $BUYING_RATE;
	private $AVERAGE_RATE;
	private $CREATED_BY;
	private $CREATED_DATE;
	private $MODIFIED_BY;
	private $MODIFIED_DATE;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('BANK_ID'=>'BANK_ID',
										'CURRENCY_ID'=>'CURRENCY_ID',
										'DATE'=>'DATE',
										'COMPANY_ID'=>'COMPANY_ID',
										'SELLING_RATE'=>'SELLING_RATE',
										'BUYING_RATE'=>'BUYING_RATE',
										'AVERAGE_RATE'=>'AVERAGE_RATE',
										'CREATED_BY'=>'CREATED_BY',
										'CREATED_DATE'=>'CREATED_DATE',
										'MODIFIED_BY'=>'MODIFIED_BY',
										'MODIFIED_DATE'=>'MODIFIED_DATE',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "BANK_ID = ".$this->BANK_ID." and CURRENCY_ID = ".$this->CURRENCY_ID." and DATE = ".$this->DATE." and COMPANY_ID = ".$this->COMPANY_ID."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun BANK_ID
	function getBANK_ID()
	{
		$this->validate();
		return $this->BANK_ID;
	}
	
	//retun CURRENCY_ID
	function getCURRENCY_ID()
	{
		$this->validate();
		return $this->CURRENCY_ID;
	}
	
	//retun DATE
	function getDATE()
	{
		$this->validate();
		return $this->DATE;
	}
	
	//retun COMPANY_ID
	function getCOMPANY_ID()
	{
		$this->validate();
		return $this->COMPANY_ID;
	}
	
	//retun SELLING_RATE
	function getSELLING_RATE()
	{
		$this->validate();
		return $this->SELLING_RATE;
	}
	
	//retun BUYING_RATE
	function getBUYING_RATE()
	{
		$this->validate();
		return $this->BUYING_RATE;
	}
	
	//retun AVERAGE_RATE
	function getAVERAGE_RATE()
	{
		$this->validate();
		return $this->AVERAGE_RATE;
	}
	
	//retun CREATED_BY
	function getCREATED_BY()
	{
		$this->validate();
		return $this->CREATED_BY;
	}
	
	//retun CREATED_DATE
	function getCREATED_DATE()
	{
		$this->validate();
		return $this->CREATED_DATE;
	}
	
	//retun MODIFIED_BY
	function getMODIFIED_BY()
	{
		$this->validate();
		return $this->MODIFIED_BY;
	}
	
	//retun MODIFIED_DATE
	function getMODIFIED_DATE()
	{
		$this->validate();
		return $this->MODIFIED_DATE;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set BANK_ID
	function setBANK_ID($BANK_ID)
	{
		array_push($this->commitArray,'BANK_ID');
		$this->BANK_ID = $BANK_ID;
	}
	
	//set CURRENCY_ID
	function setCURRENCY_ID($CURRENCY_ID)
	{
		array_push($this->commitArray,'CURRENCY_ID');
		$this->CURRENCY_ID = $CURRENCY_ID;
	}
	
	//set DATE
	function setDATE($DATE)
	{
		array_push($this->commitArray,'DATE');
		$this->DATE = $DATE;
	}
	
	//set COMPANY_ID
	function setCOMPANY_ID($COMPANY_ID)
	{
		array_push($this->commitArray,'COMPANY_ID');
		$this->COMPANY_ID = $COMPANY_ID;
	}
	
	//set SELLING_RATE
	function setSELLING_RATE($SELLING_RATE)
	{
		array_push($this->commitArray,'SELLING_RATE');
		$this->SELLING_RATE = $SELLING_RATE;
	}
	
	//set BUYING_RATE
	function setBUYING_RATE($BUYING_RATE)
	{
		array_push($this->commitArray,'BUYING_RATE');
		$this->BUYING_RATE = $BUYING_RATE;
	}
	
	//set AVERAGE_RATE
	function setAVERAGE_RATE($AVERAGE_RATE)
	{
		array_push($this->commitArray,'AVERAGE_RATE');
		$this->AVERAGE_RATE = $AVERAGE_RATE;
	}
	
	//set CREATED_BY
	function setCREATED_BY($CREATED_BY)
	{
		array_push($this->commitArray,'CREATED_BY');
		$this->CREATED_BY = $CREATED_BY;
	}
	
	//set CREATED_DATE
	function setCREATED_DATE($CREATED_DATE)
	{
		array_push($this->commitArray,'CREATED_DATE');
		$this->CREATED_DATE = $CREATED_DATE;
	}
	
	//set MODIFIED_BY
	function setMODIFIED_BY($MODIFIED_BY)
	{
		array_push($this->commitArray,'MODIFIED_BY');
		$this->MODIFIED_BY = $MODIFIED_BY;
	}
	
	//set MODIFIED_DATE
	function setMODIFIED_DATE($MODIFIED_DATE)
	{
		array_push($this->commitArray,'MODIFIED_DATE');
		$this->MODIFIED_DATE = $MODIFIED_DATE;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->BANK_ID=='' || $this->CURRENCY_ID=='' || $this->DATE=='' || $this->COMPANY_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($BANK_ID , $CURRENCY_ID , $DATE , $COMPANY_ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "BANK_ID='$BANK_ID' and CURRENCY_ID='$CURRENCY_ID' and DATE='$DATE' and COMPANY_ID='$COMPANY_ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($BANK_ID,$CURRENCY_ID,$DATE,$COMPANY_ID,$SELLING_RATE,$BUYING_RATE,$AVERAGE_RATE,$CREATED_BY,$CREATED_DATE,$MODIFIED_BY,$MODIFIED_DATE){
		$data = array('BANK_ID'=>$BANK_ID 
				,'CURRENCY_ID'=>$CURRENCY_ID 
				,'DATE'=>$DATE 
				,'COMPANY_ID'=>$COMPANY_ID 
				,'SELLING_RATE'=>$SELLING_RATE 
				,'BUYING_RATE'=>$BUYING_RATE 
				,'AVERAGE_RATE'=>$AVERAGE_RATE 
				,'CREATED_BY'=>$CREATED_BY 
				,'CREATED_DATE'=>$CREATED_DATE 
				,'MODIFIED_BY'=>$MODIFIED_BY 
				,'MODIFIED_DATE'=>$MODIFIED_DATE 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('BANK_ID,CURRENCY_ID',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['BANK_ID'])
				$html .= '<option selected="selected" value="'.$row['BANK_ID'].'">'.$row['CURRENCY_ID'].'</option>';	
			else
				$html .= '<option value="'.$row['BANK_ID'].'">'.$row['CURRENCY_ID'].'</option>';	
		}
		return $html;
	}
	public function checkBankCurrencyAvailableForSelectedDate($dtmDate,$chartOfAcc,$intCompanyId)
	{
		$cols	= " mst_financeexchangerate_bankwise.BANK_ID,
					mst_financeexchangerate_bankwise.CURRENCY_ID,
					mst_financeexchangerate_bankwise.DATE,
					mst_financeexchangerate_bankwise.COMPANY_ID,
					mst_financeexchangerate_bankwise.SELLING_RATE,
					mst_financeexchangerate_bankwise.BUYING_RATE,
					mst_financeexchangerate_bankwise.AVERAGE_RATE,
					mst_financeexchangerate_bankwise.CREATED_BY,
					mst_financeexchangerate_bankwise.CREATED_DATE,
					mst_financeexchangerate_bankwise.MODIFIED_BY,
					mst_financeexchangerate_bankwise.MODIFIED_DATE";
		
		$join	= " INNER JOIN finance_mst_chartofaccount_bank ON mst_financeexchangerate_bankwise.BANK_ID = finance_mst_chartofaccount_bank.BANK_ID";

		$where	= " finance_mst_chartofaccount_bank.CHART_OF_ACCOUNT_ID = '$chartOfAcc' AND
					mst_financeexchangerate_bankwise.DATE = '$dtmDate' AND
					mst_financeexchangerate_bankwise.COMPANY_ID = '$intCompanyId'";	
		$result	= $this->select($cols,$join,$where,NULL,NULL);
		if($this->db->numRows()>0)
			return true;
		else
			return false;
	}
	//END }
}
?>
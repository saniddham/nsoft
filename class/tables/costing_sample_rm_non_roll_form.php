<?php
class costing_sample_rm_non_roll_form{

	private $db;
	private $table= "costing_sample_rm_non_roll_form";

	//private property
	private $SAMPLE_NO;
	private $SAMPLE_YEAR;
	private $REVISION;
	private $COMBO;
	private $PRINT;
	private $COLOR;
	private $TECHNIQUE;
	private $ITEM;
	private $GRAPHIC_W;
	private $GRAPHIC_H;
	private $ITEM_PRICE;
	private $CONSUMPTION;
	private $COST;
	private $commitArray = array();
	private $field_array = array();
	

	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('SAMPLE_NO'=>'SAMPLE_NO',
										'SAMPLE_YEAR'=>'SAMPLE_YEAR',
										'REVISION'=>'REVISION',
										'COMBO'=>'COMBO',
										'PRINT'=>'PRINT',
										'COLOR'=>'COLOR',
										'TECHNIQUE'=>'TECHNIQUE',
										'ITEM'=>'ITEM',
										'GRAPHIC_W'=>'GRAPHIC_W',
										'GRAPHIC_H'=>'GRAPHIC_H',
										'ITEM_PRICE'=>'ITEM_PRICE',
										'CONSUMPTION'=>'CONSUMPTION',
										'COST'=>'COST',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit );
		return $result;
	}

	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data);
		return $this->db->getResult();


	}

	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}

	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();
	}

	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}

	public function get_field_array(){
		return $this->field_array;
	}

	function commit($type='update')
	{
		//commit update
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{
			$data[$v] = $this->$v;
		}
		$where		= "SAMPLE_NO = ".$this->SAMPLE_NO." and SAMPLE_YEAR = ".$this->SAMPLE_YEAR." and REVISION = ".$this->REVISION." and COMBO = ".$this->COMBO." and PRINT = ".$this->PRINT." and COLOR = ".$this->COLOR." and TECHNIQUE = ".$this->TECHNIQUE." and ITEM = ".$this->ITEM."" ;
		$this->commitArray = array();

		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}

	//BEGIN - public functions for private properties {
	
	//retun SAMPLE_NO
	function getSAMPLE_NO()
	{
		$this->validate();
		return $this->SAMPLE_NO;
	}
	
	//retun SAMPLE_YEAR
	function getSAMPLE_YEAR()
	{
		$this->validate();
		return $this->SAMPLE_YEAR;
	}
	
	//retun REVISION
	function getREVISION()
	{
		$this->validate();
		return $this->REVISION;
	}
	
	//retun COMBO
	function getCOMBO()
	{
		$this->validate();
		return $this->COMBO;
	}
	
	//retun PRINT
	function getPRINT()
	{
		$this->validate();
		return $this->PRINT;
	}
	
	//retun COLOR
	function getCOLOR()
	{
		$this->validate();
		return $this->COLOR;
	}
	//retun TECHNIQUE
	function getTECHNIQUE()
	{
		$this->validate();
		return $this->TECHNIQUE;
	}
	
	//retun ITEM
	function getITEM()
	{
		$this->validate();
		return $this->ITEM;
	}
	
	//retun GRAPHIC_W
	function getGRAPHIC_W()
	{
		$this->validate();
		return $this->GRAPHIC_W;
	}
	
	//retun GRAPHIC_H
	function getGRAPHIC_H()
	{
		$this->validate();
		return $this->GRAPHIC_H;
	}
	
	//retun ITEM_PRICE
	function getITEM_PRICE()
	{
		$this->validate();
		return $this->ITEM_PRICE;
	}
	
	//retun CONSUMPTION
	function getCONSUMPTION()
	{
		$this->validate();
		return $this->CONSUMPTION;
	}
	
	//retun COST
	function getCOST()
	{
		$this->validate();
		return $this->COST;
	}
	
	//END }

	//BEGIN - public function set {
	
	//set SAMPLE_NO
	function setSAMPLE_NO($SAMPLE_NO)
	{
		array_push($this->commitArray,'SAMPLE_NO');
		$this->SAMPLE_NO = $SAMPLE_NO;
	}
	
	//set SAMPLE_YEAR
	function setSAMPLE_YEAR($SAMPLE_YEAR)
	{
		array_push($this->commitArray,'SAMPLE_YEAR');
		$this->SAMPLE_YEAR = $SAMPLE_YEAR;
	}
	
	//set REVISION
	function setREVISION($REVISION)
	{
		array_push($this->commitArray,'REVISION');
		$this->REVISION = $REVISION;
	}
	
	//set COMBO
	function setCOMBO($COMBO)
	{
		array_push($this->commitArray,'COMBO');
		$this->COMBO = $COMBO;
	}
	
	//set PRINT
	function setPRINT($PRINT)
	{
		array_push($this->commitArray,'PRINT');
		$this->PRINT = $PRINT;
	}
	
	//set COLOR
	function setCOLOR($COLOR)
	{
		array_push($this->commitArray,'COLOR');
		$this->COLOR = $COLOR;
	}
	//set TECHNIQUE
	function setTECHNIQUE($TECHNIQUE)
	{
		array_push($this->commitArray,'TECHNIQUE');
		$this->TECHNIQUE = $TECHNIQUE;
	}
	
	//set ITEM
	function setITEM($ITEM)
	{
		array_push($this->commitArray,'ITEM');
		$this->ITEM = $ITEM;
	}
	
	//set GRAPHIC_W
	function setGRAPHIC_W($GRAPHIC_W)
	{
		array_push($this->commitArray,'GRAPHIC_W');
		$this->GRAPHIC_W = $GRAPHIC_W;
	}
	
	//set GRAPHIC_H
	function setGRAPHIC_H($GRAPHIC_H)
	{
		array_push($this->commitArray,'GRAPHIC_H');
		$this->GRAPHIC_H = $GRAPHIC_H;
	}
	
	//set ITEM_PRICE
	function setITEM_PRICE($ITEM_PRICE)
	{
		array_push($this->commitArray,'ITEM_PRICE');
		$this->ITEM_PRICE = $ITEM_PRICE;
	}
	
	//set CONSUMPTION
	function setCONSUMPTION($CONSUMPTION)
	{
		array_push($this->commitArray,'CONSUMPTION');
		$this->CONSUMPTION = $CONSUMPTION;
	}
	
	//set COST
	function setCOST($COST)
	{
		array_push($this->commitArray,'COST');
		$this->COST = $COST;
	}
	
	//END }

	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->SAMPLE_NO=='' || $this->SAMPLE_YEAR=='' || $this->REVISION=='' || $this->COMBO=='' || $this->PRINT=='' || $this->COLOR==''  || $this->TECHNIQUE=='' || $this->ITEM=='')
			throw new exception("Set primary values first");
		else
			return true;
	}

	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}

	public function set($SAMPLE_NO , $SAMPLE_YEAR , $REVISION , $COMBO , $PRINT , $COLOR, $TECHNIQUE , $ITEM)
	{
		$cols	= "*";

		$join	= NULL;

		$where	= "SAMPLE_NO='$SAMPLE_NO' and SAMPLE_YEAR='$SAMPLE_YEAR' and REVISION='$REVISION' and COMBO='$COMBO' and PRINT='$PRINT' and COLOR='$COLOR' and TECHNIQUE='$TECHNIQUE' and ITEM='$ITEM'";

		$result = $this->select($cols,$join,$where);
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}

	//insert as parameters
	public function insertRec($SAMPLE_NO,$SAMPLE_YEAR,$REVISION,$COMBO,$PRINT,$COLOR,$TECHNIQUE,$ITEM,$GRAPHIC_W,$GRAPHIC_H,$ITEM_PRICE,$CONSUMPTION,$COST){
		$data = array('SAMPLE_NO'=>$SAMPLE_NO 
				,'SAMPLE_YEAR'=>$SAMPLE_YEAR 
				,'REVISION'=>$REVISION 
				,'COMBO'=>$COMBO 
				,'PRINT'=>$PRINT 
				,'COLOR'=>$COLOR 
				,'TECHNIQUE'=>$TECHNIQUE 
				,'ITEM'=>$ITEM 
				,'GRAPHIC_W'=>$GRAPHIC_W 
				,'GRAPHIC_H'=>$GRAPHIC_H 
				,'ITEM_PRICE'=>$ITEM_PRICE 
				,'CONSUMPTION'=>$CONSUMPTION 
				,'COST'=>$COST 
				);
		return $this->insert($data);
	}

	public function getCombo_samp($defaultValue=null,$where=null){
		$result = $this->select('SAMPLE_NO,SAMPLE_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['SAMPLE_NO'])
				$html .= '<option selected="selected" value="'.$row['SAMPLE_NO'].'">'.$row['SAMPLE_YEAR'].'</option>';
			else
				$html .= '<option value="'.$row['SAMPLE_NO'].'">'.$row['SAMPLE_YEAR'].'</option>';
		}
		return $html;
	}
	
	//END }
}
?>
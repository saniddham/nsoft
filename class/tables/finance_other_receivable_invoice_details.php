<?php
class finance_other_receivable_invoice_details{
 
	private $db;
	private $table= "finance_other_receivable_invoice_details";
	
	//private property
	private $BILL_INVOICE_NO;
	private $BILL_INVOICE_YEAR;
	private $CHART_OF_ACCOUNT_ID;
	private $ITEM_DESCRIPTION;
	private $UNIT_ID;
	private $QTY;
	private $UNIT_PRICE;
	private $DISCOUNT;
	private $TAX_AMOUNT;
	private $TAX_GROUP_ID;
	private $COST_CENTER;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('BILL_INVOICE_NO'=>'BILL_INVOICE_NO',
										'BILL_INVOICE_YEAR'=>'BILL_INVOICE_YEAR',
										'CHART_OF_ACCOUNT_ID'=>'CHART_OF_ACCOUNT_ID',
										'ITEM_DESCRIPTION'=>'ITEM_DESCRIPTION',
										'UNIT_ID'=>'UNIT_ID',
										'QTY'=>'QTY',
										'UNIT_PRICE'=>'UNIT_PRICE',
										'DISCOUNT'=>'DISCOUNT',
										'TAX_AMOUNT'=>'TAX_AMOUNT',
										'TAX_GROUP_ID'=>'TAX_GROUP_ID',
										'COST_CENTER'=>'COST_CENTER',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "BILL_INVOICE_NO = ".$this->BILL_INVOICE_NO." and BILL_INVOICE_YEAR = ".$this->BILL_INVOICE_YEAR." and CHART_OF_ACCOUNT_ID = ".$this->CHART_OF_ACCOUNT_ID."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun BILL_INVOICE_NO
	function getBILL_INVOICE_NO()
	{
		$this->validate();
		return $this->BILL_INVOICE_NO;
	}
	
	//retun BILL_INVOICE_YEAR
	function getBILL_INVOICE_YEAR()
	{
		$this->validate();
		return $this->BILL_INVOICE_YEAR;
	}
	
	//retun CHART_OF_ACCOUNT_ID
	function getCHART_OF_ACCOUNT_ID()
	{
		$this->validate();
		return $this->CHART_OF_ACCOUNT_ID;
	}
	
	//retun ITEM_DESCRIPTION
	function getITEM_DESCRIPTION()
	{
		$this->validate();
		return $this->ITEM_DESCRIPTION;
	}
	
	//retun UNIT_ID
	function getUNIT_ID()
	{
		$this->validate();
		return $this->UNIT_ID;
	}
	
	//retun QTY
	function getQTY()
	{
		$this->validate();
		return $this->QTY;
	}
	
	//retun UNIT_PRICE
	function getUNIT_PRICE()
	{
		$this->validate();
		return $this->UNIT_PRICE;
	}
	
	//retun DISCOUNT
	function getDISCOUNT()
	{
		$this->validate();
		return $this->DISCOUNT;
	}
	
	//retun TAX_AMOUNT
	function getTAX_AMOUNT()
	{
		$this->validate();
		return $this->TAX_AMOUNT;
	}
	
	//retun TAX_GROUP_ID
	function getTAX_GROUP_ID()
	{
		$this->validate();
		return $this->TAX_GROUP_ID;
	}
	
	//retun COST_CENTER
	function getCOST_CENTER()
	{
		$this->validate();
		return $this->COST_CENTER;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set BILL_INVOICE_NO
	function setBILL_INVOICE_NO($BILL_INVOICE_NO)
	{
		array_push($this->commitArray,'BILL_INVOICE_NO');
		$this->BILL_INVOICE_NO = $BILL_INVOICE_NO;
	}
	
	//set BILL_INVOICE_YEAR
	function setBILL_INVOICE_YEAR($BILL_INVOICE_YEAR)
	{
		array_push($this->commitArray,'BILL_INVOICE_YEAR');
		$this->BILL_INVOICE_YEAR = $BILL_INVOICE_YEAR;
	}
	
	//set CHART_OF_ACCOUNT_ID
	function setCHART_OF_ACCOUNT_ID($CHART_OF_ACCOUNT_ID)
	{
		array_push($this->commitArray,'CHART_OF_ACCOUNT_ID');
		$this->CHART_OF_ACCOUNT_ID = $CHART_OF_ACCOUNT_ID;
	}
	
	//set ITEM_DESCRIPTION
	function setITEM_DESCRIPTION($ITEM_DESCRIPTION)
	{
		array_push($this->commitArray,'ITEM_DESCRIPTION');
		$this->ITEM_DESCRIPTION = $ITEM_DESCRIPTION;
	}
	
	//set UNIT_ID
	function setUNIT_ID($UNIT_ID)
	{
		array_push($this->commitArray,'UNIT_ID');
		$this->UNIT_ID = $UNIT_ID;
	}
	
	//set QTY
	function setQTY($QTY)
	{
		array_push($this->commitArray,'QTY');
		$this->QTY = $QTY;
	}
	
	//set UNIT_PRICE
	function setUNIT_PRICE($UNIT_PRICE)
	{
		array_push($this->commitArray,'UNIT_PRICE');
		$this->UNIT_PRICE = $UNIT_PRICE;
	}
	
	//set DISCOUNT
	function setDISCOUNT($DISCOUNT)
	{
		array_push($this->commitArray,'DISCOUNT');
		$this->DISCOUNT = $DISCOUNT;
	}
	
	//set TAX_AMOUNT
	function setTAX_AMOUNT($TAX_AMOUNT)
	{
		array_push($this->commitArray,'TAX_AMOUNT');
		$this->TAX_AMOUNT = $TAX_AMOUNT;
	}
	
	//set TAX_GROUP_ID
	function setTAX_GROUP_ID($TAX_GROUP_ID)
	{
		array_push($this->commitArray,'TAX_GROUP_ID');
		$this->TAX_GROUP_ID = $TAX_GROUP_ID;
	}
	
	//set COST_CENTER
	function setCOST_CENTER($COST_CENTER)
	{
		array_push($this->commitArray,'COST_CENTER');
		$this->COST_CENTER = $COST_CENTER;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->BILL_INVOICE_NO=='' || $this->BILL_INVOICE_YEAR=='' || $this->CHART_OF_ACCOUNT_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($BILL_INVOICE_NO , $BILL_INVOICE_YEAR , $CHART_OF_ACCOUNT_ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "BILL_INVOICE_NO='$BILL_INVOICE_NO' and BILL_INVOICE_YEAR='$BILL_INVOICE_YEAR' and CHART_OF_ACCOUNT_ID='$CHART_OF_ACCOUNT_ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($BILL_INVOICE_NO,$BILL_INVOICE_YEAR,$CHART_OF_ACCOUNT_ID,$ITEM_DESCRIPTION,$UNIT_ID,$QTY,$UNIT_PRICE,$DISCOUNT,$TAX_AMOUNT,$TAX_GROUP_ID,$COST_CENTER){
		$data = array('BILL_INVOICE_NO'=>$BILL_INVOICE_NO 
				,'BILL_INVOICE_YEAR'=>$BILL_INVOICE_YEAR 
				,'CHART_OF_ACCOUNT_ID'=>$CHART_OF_ACCOUNT_ID 
				,'ITEM_DESCRIPTION'=>$ITEM_DESCRIPTION 
				,'UNIT_ID'=>$UNIT_ID 
				,'QTY'=>$QTY 
				,'UNIT_PRICE'=>$UNIT_PRICE 
				,'DISCOUNT'=>$DISCOUNT 
				,'TAX_AMOUNT'=>$TAX_AMOUNT 
				,'TAX_GROUP_ID'=>$TAX_GROUP_ID 
				,'COST_CENTER'=>$COST_CENTER 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('BILL_INVOICE_NO,BILL_INVOICE_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['BILL_INVOICE_NO'])
				$html .= '<option selected="selected" value="'.$row['BILL_INVOICE_NO'].'">'.$row['BILL_INVOICE_YEAR'].'</option>';	
			else
				$html .= '<option value="'.$row['BILL_INVOICE_NO'].'">'.$row['BILL_INVOICE_YEAR'].'</option>';	
		}
		return $html;
	}
	public function getDetailData_result($invoiceNo,$invoiceYear)
	{
		$cols	= " finance_other_receivable_invoice_details.CHART_OF_ACCOUNT_ID,
					finance_other_receivable_invoice_details.ITEM_DESCRIPTION,
					finance_other_receivable_invoice_details.UNIT_ID,
					finance_other_receivable_invoice_details.QTY,
					finance_other_receivable_invoice_details.UNIT_PRICE,
					finance_other_receivable_invoice_details.DISCOUNT,
					finance_other_receivable_invoice_details.TAX_AMOUNT,
					finance_other_receivable_invoice_details.TAX_GROUP_ID,
					finance_other_receivable_invoice_details.COST_CENTER,
					finance_mst_chartofaccount.CHART_OF_ACCOUNT_NAME,
					mst_financedimension.strName AS COST_CENTER_NAME,
					mst_units.strName AS UNIT,
					mst_financetaxgroup.strCode AS TAX_CODE";
					
		$join	= " INNER JOIN finance_mst_chartofaccount ON finance_other_receivable_invoice_details.CHART_OF_ACCOUNT_ID = finance_mst_chartofaccount.CHART_OF_ACCOUNT_ID
					INNER JOIN mst_financedimension ON finance_other_receivable_invoice_details.COST_CENTER = mst_financedimension.intId
					INNER JOIN mst_units ON finance_other_receivable_invoice_details.UNIT_ID = mst_units.intId
					LEFT JOIN mst_financetaxgroup ON finance_other_receivable_invoice_details.TAX_GROUP_ID = mst_financetaxgroup.intId";
		
		$where	= " finance_other_receivable_invoice_details.BILL_INVOICE_NO = '$invoiceNo' AND
					finance_other_receivable_invoice_details.BILL_INVOICE_YEAR = '$invoiceYear'";
		
		$result	= $this->select($cols,$join,$where,NULL,NULL);
		return $result;
	}
	//END }
}
?>
<?php
class finance_customer_exceed_settlement_header{

	private $db;
	private $table= "finance_customer_exceed_settlement_header";

	//private property
	private $SETTLE_NO;
	private $SETTLE_YEAR;
	private $CUSTOMER_ID;
	private $CURRENCY_ID;
	private $INVOICE_NO;
	private $INVOICE_YEAR;
	private $SETTLE_AMOUNT;
	private $STATUS;
	private $SETTLE_DATE;
	private $COMPANY_ID;
	private $LOCATION_ID;
	private $CREATED_BY;
	private $CREATED_DATE;
	private $commitArray = array();
	private $field_array = array();
	

	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('SETTLE_NO'=>'SETTLE_NO',
										'SETTLE_YEAR'=>'SETTLE_YEAR',
										'CUSTOMER_ID'=>'CUSTOMER_ID',
										'CURRENCY_ID'=>'CURRENCY_ID',
										'INVOICE_NO'=>'INVOICE_NO',
										'INVOICE_YEAR'=>'INVOICE_YEAR',
										'SETTLE_AMOUNT'=>'SETTLE_AMOUNT',
										'STATUS'=>'STATUS',
										'SETTLE_DATE'=>'SETTLE_DATE',
										'COMPANY_ID'=>'COMPANY_ID',
										'LOCATION_ID'=>'LOCATION_ID',
										'CREATED_BY'=>'CREATED_BY',
										'CREATED_DATE'=>'CREATED_DATE',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit );
		return $result;
	}

	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data);
		return $this->db->getResult();


	}

	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}

	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();
	}

	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}

	public function get_field_array(){
		return $this->field_array;
	}

	function commit($type='update')
	{
		//commit update
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{
			$data[$v] = $this->$v;
		}
		$where		= "SETTLE_NO = ".$this->SETTLE_NO." and SETTLE_YEAR = ".$this->SETTLE_YEAR."" ;
		$this->commitArray = array();

		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}

	//BEGIN - public functions for private properties {
	
	//retun SETTLE_NO
	function getSETTLE_NO()
	{
		$this->validate();
		return $this->SETTLE_NO;
	}
	
	//retun SETTLE_YEAR
	function getSETTLE_YEAR()
	{
		$this->validate();
		return $this->SETTLE_YEAR;
	}
	
	//retun CUSTOMER_ID
	function getCUSTOMER_ID()
	{
		$this->validate();
		return $this->CUSTOMER_ID;
	}
	
	//retun CURRENCY_ID
	function getCURRENCY_ID()
	{
		$this->validate();
		return $this->CURRENCY_ID;
	}
	
	//retun INVOICE_NO
	function getINVOICE_NO()
	{
		$this->validate();
		return $this->INVOICE_NO;
	}
	
	//retun INVOICE_YEAR
	function getINVOICE_YEAR()
	{
		$this->validate();
		return $this->INVOICE_YEAR;
	}
	
	//retun SETTLE_AMOUNT
	function getSETTLE_AMOUNT()
	{
		$this->validate();
		return $this->SETTLE_AMOUNT;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//retun SETTLE_DATE
	function getSETTLE_DATE()
	{
		$this->validate();
		return $this->SETTLE_DATE;
	}
	
	//retun COMPANY_ID
	function getCOMPANY_ID()
	{
		$this->validate();
		return $this->COMPANY_ID;
	}
	
	//retun LOCATION_ID
	function getLOCATION_ID()
	{
		$this->validate();
		return $this->LOCATION_ID;
	}
	
	//retun CREATED_BY
	function getCREATED_BY()
	{
		$this->validate();
		return $this->CREATED_BY;
	}
	
	//retun CREATED_DATE
	function getCREATED_DATE()
	{
		$this->validate();
		return $this->CREATED_DATE;
	}
	
	//END }

	//BEGIN - public function set {
	
	//set SETTLE_NO
	function setSETTLE_NO($SETTLE_NO)
	{
		array_push($this->commitArray,'SETTLE_NO');
		$this->SETTLE_NO = $SETTLE_NO;
	}
	
	//set SETTLE_YEAR
	function setSETTLE_YEAR($SETTLE_YEAR)
	{
		array_push($this->commitArray,'SETTLE_YEAR');
		$this->SETTLE_YEAR = $SETTLE_YEAR;
	}
	
	//set CUSTOMER_ID
	function setCUSTOMER_ID($CUSTOMER_ID)
	{
		array_push($this->commitArray,'CUSTOMER_ID');
		$this->CUSTOMER_ID = $CUSTOMER_ID;
	}
	
	//set CURRENCY_ID
	function setCURRENCY_ID($CURRENCY_ID)
	{
		array_push($this->commitArray,'CURRENCY_ID');
		$this->CURRENCY_ID = $CURRENCY_ID;
	}
	
	//set INVOICE_NO
	function setINVOICE_NO($INVOICE_NO)
	{
		array_push($this->commitArray,'INVOICE_NO');
		$this->INVOICE_NO = $INVOICE_NO;
	}
	
	//set INVOICE_YEAR
	function setINVOICE_YEAR($INVOICE_YEAR)
	{
		array_push($this->commitArray,'INVOICE_YEAR');
		$this->INVOICE_YEAR = $INVOICE_YEAR;
	}
	
	//set SETTLE_AMOUNT
	function setSETTLE_AMOUNT($SETTLE_AMOUNT)
	{
		array_push($this->commitArray,'SETTLE_AMOUNT');
		$this->SETTLE_AMOUNT = $SETTLE_AMOUNT;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//set SETTLE_DATE
	function setSETTLE_DATE($SETTLE_DATE)
	{
		array_push($this->commitArray,'SETTLE_DATE');
		$this->SETTLE_DATE = $SETTLE_DATE;
	}
	
	//set COMPANY_ID
	function setCOMPANY_ID($COMPANY_ID)
	{
		array_push($this->commitArray,'COMPANY_ID');
		$this->COMPANY_ID = $COMPANY_ID;
	}
	
	//set LOCATION_ID
	function setLOCATION_ID($LOCATION_ID)
	{
		array_push($this->commitArray,'LOCATION_ID');
		$this->LOCATION_ID = $LOCATION_ID;
	}
	
	//set CREATED_BY
	function setCREATED_BY($CREATED_BY)
	{
		array_push($this->commitArray,'CREATED_BY');
		$this->CREATED_BY = $CREATED_BY;
	}
	
	//set CREATED_DATE
	function setCREATED_DATE($CREATED_DATE)
	{
		array_push($this->commitArray,'CREATED_DATE');
		$this->CREATED_DATE = $CREATED_DATE;
	}
	
	//END }

	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->SETTLE_NO=='' || $this->SETTLE_YEAR=='')
			throw new exception("Set primary values first");
		else
			return true;
	}

	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}

	public function set($SETTLE_NO , $SETTLE_YEAR)
	{
		$cols	= "*";

		$join	= NULL;

		$where	= "SETTLE_NO='$SETTLE_NO' and SETTLE_YEAR='$SETTLE_YEAR'";

		$result = $this->select($cols,$join,$where);
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}

	//insert as parameters
	public function insertRec($SETTLE_NO,$SETTLE_YEAR,$CUSTOMER_ID,$CURRENCY_ID,$INVOICE_NO,$INVOICE_YEAR,$SETTLE_AMOUNT,$STATUS,$SETTLE_DATE,$COMPANY_ID,$LOCATION_ID,$CREATED_BY,$CREATED_DATE){
		$data = array('SETTLE_NO'=>$SETTLE_NO 
				,'SETTLE_YEAR'=>$SETTLE_YEAR 
				,'CUSTOMER_ID'=>$CUSTOMER_ID 
				,'CURRENCY_ID'=>$CURRENCY_ID 
				,'INVOICE_NO'=>$INVOICE_NO 
				,'INVOICE_YEAR'=>$INVOICE_YEAR 
				,'SETTLE_AMOUNT'=>$SETTLE_AMOUNT 
				,'STATUS'=>$STATUS 
				,'SETTLE_DATE'=>$SETTLE_DATE 
				,'COMPANY_ID'=>$COMPANY_ID 
				,'LOCATION_ID'=>$LOCATION_ID 
				,'CREATED_BY'=>$CREATED_BY 
				,'CREATED_DATE'=>$CREATED_DATE 
				);
		return $this->insert($data);
	}

	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('SETTLE_NO,SETTLE_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['SETTLE_NO'])
				$html .= '<option selected="selected" value="'.$row['SETTLE_NO'].'">'.$row['SETTLE_YEAR'].'</option>';
			else
				$html .= '<option value="'.$row['SETTLE_NO'].'">'.$row['SETTLE_YEAR'].'</option>';
		}
		return $html;
	}
	
	//END }
}
?>
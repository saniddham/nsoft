
<?php

class sys_auto_email_report_type_address{
 
	private $db;
	private $table= "sys_auto_email_report_type_address";
	
	//private property
	private $ID;
	private $MAIN_CLUSTER;
	private $SUB_CLUSTER;
	private $TO_EMAIL_USERS;
	private $CC_EMAIL_USERS;
	private $BCC_EMAIL_USERS;
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->db->field_array_set(array('ID'=>'ID',
										'MAIN_CLUSTER'=>'MAIN_CLUSTER',
										'SUB_CLUSTER'=>'SUB_CLUSTER',
										'TO_EMAIL_USERS'=>'TO_EMAIL_USERS',
										'CC_EMAIL_USERS'=>'CC_EMAIL_USERS',
										'BCC_EMAIL_USERS'=>'BCC_EMAIL_USERS',
										));
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  
		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	//public functions for private properties
	
	//retun ID
	function getID()
	{
		$this->validate();
		return $this->ID;
	}
	
	//retun MAIN_CLUSTER
	function getMAIN_CLUSTER()
	{
		$this->validate();
		return $this->MAIN_CLUSTER;
	}
	
	//retun SUB_CLUSTER
	function getSUB_CLUSTER()
	{
		$this->validate();
		return $this->SUB_CLUSTER;
	}
	
	//retun TO_EMAIL_USERS
	function getTO_EMAIL_USERS()
	{
		$this->validate();
		return $this->TO_EMAIL_USERS;
	}
	
	//retun CC_EMAIL_USERS
	function getCC_EMAIL_USERS()
	{
		$this->validate();
		return $this->CC_EMAIL_USERS;
	}
	
	//retun BCC_EMAIL_USERS
	function getBCC_EMAIL_USERS()
	{
		$this->validate();
		return $this->BCC_EMAIL_USERS;
	}
	
	//validate primary values
	private function validate()
	{
		if($this->ID=='' || $this->MAIN_CLUSTER=='' || $this->SUB_CLUSTER=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
		}
	}
	
	public function set($ID , $MAIN_CLUSTER , $SUB_CLUSTER)
	{
		$result = $this->select('*',null,"ID='$ID' and MAIN_CLUSTER='$MAIN_CLUSTER' and SUB_CLUSTER='$SUB_CLUSTER'");	
		if($this->db->numRows()>0)
			$this->setVariables($result);
		else
			throw new Exception("Record not found.");
			
	}
	
	
}
?>

<?php
class sys_company_item_allocation_percentages{
 
	private $db;
	private $table= "sys_company_item_allocation_percentages";
	
	//private property
	private $COMPANY_ID;
	private $SAMPLE_ALLOCATION;
	private $FULL_ALLOCATION;
	private $FOIL_EXTRA_ALLOCATION;
	private $SP_RM_EXTRA_ALLOCATION;
	private $INK_ITEM_EXTRA_ALLOCATION;
	private $INK_ITEM_EXTRA_SRN;
	private $FOIL_ITEM_EXTRA_MRN;
	private $SPECIAL_ITEM_EXTRA_MRN;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('COMPANY_ID'=>'COMPANY_ID',
										'SAMPLE_ALLOCATION'=>'SAMPLE_ALLOCATION',
										'FULL_ALLOCATION'=>'FULL_ALLOCATION',
										'FOIL_EXTRA_ALLOCATION'=>'FOIL_EXTRA_ALLOCATION',
										'SP_RM_EXTRA_ALLOCATION'=>'SP_RM_EXTRA_ALLOCATION',
										'INK_ITEM_EXTRA_ALLOCATION'=>'INK_ITEM_EXTRA_ALLOCATION',
										'INK_ITEM_EXTRA_SRN'=>'INK_ITEM_EXTRA_SRN',
										'FOIL_ITEM_EXTRA_MRN'=>'FOIL_ITEM_EXTRA_MRN',
										'SPECIAL_ITEM_EXTRA_MRN'=>'SPECIAL_ITEM_EXTRA_MRN',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "COMPANY_ID = ".$this->COMPANY_ID."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun COMPANY_ID
	function getCOMPANY_ID()
	{
		$this->validate();
		return $this->COMPANY_ID;
	}
	
	//retun SAMPLE_ALLOCATION
	function getSAMPLE_ALLOCATION()
	{
		$this->validate();
		return $this->SAMPLE_ALLOCATION;
	}
	
	//retun FULL_ALLOCATION
	function getFULL_ALLOCATION()
	{
		$this->validate();
		return $this->FULL_ALLOCATION;
	}
	
	//retun FOIL_EXTRA_ALLOCATION
	function getFOIL_EXTRA_ALLOCATION()
	{
		$this->validate();
		return $this->FOIL_EXTRA_ALLOCATION;
	}
	
	//retun SP_RM_EXTRA_ALLOCATION
	function getSP_RM_EXTRA_ALLOCATION()
	{
		$this->validate();
		return $this->SP_RM_EXTRA_ALLOCATION;
	}
	
	//retun INK_ITEM_EXTRA_ALLOCATION
	function getINK_ITEM_EXTRA_ALLOCATION()
	{
		$this->validate();
		return $this->INK_ITEM_EXTRA_ALLOCATION;
	}
	
	//retun INK_ITEM_EXTRA_SRN
	function getINK_ITEM_EXTRA_SRN()
	{
		$this->validate();
		return $this->INK_ITEM_EXTRA_SRN;
	}
	
	//retun FOIL_ITEM_EXTRA_MRN
	function getFOIL_ITEM_EXTRA_MRN()
	{
		$this->validate();
		return $this->FOIL_ITEM_EXTRA_MRN;
	}
	
	//retun SPECIAL_ITEM_EXTRA_MRN
	function getSPECIAL_ITEM_EXTRA_MRN()
	{
		$this->validate();
		return $this->SPECIAL_ITEM_EXTRA_MRN;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set COMPANY_ID
	function setCOMPANY_ID($COMPANY_ID)
	{
		array_push($this->commitArray,'COMPANY_ID');
		$this->COMPANY_ID = $COMPANY_ID;
	}
	
	//set SAMPLE_ALLOCATION
	function setSAMPLE_ALLOCATION($SAMPLE_ALLOCATION)
	{
		array_push($this->commitArray,'SAMPLE_ALLOCATION');
		$this->SAMPLE_ALLOCATION = $SAMPLE_ALLOCATION;
	}
	
	//set FULL_ALLOCATION
	function setFULL_ALLOCATION($FULL_ALLOCATION)
	{
		array_push($this->commitArray,'FULL_ALLOCATION');
		$this->FULL_ALLOCATION = $FULL_ALLOCATION;
	}
	
	//set FOIL_EXTRA_ALLOCATION
	function setFOIL_EXTRA_ALLOCATION($FOIL_EXTRA_ALLOCATION)
	{
		array_push($this->commitArray,'FOIL_EXTRA_ALLOCATION');
		$this->FOIL_EXTRA_ALLOCATION = $FOIL_EXTRA_ALLOCATION;
	}
	
	//set SP_RM_EXTRA_ALLOCATION
	function setSP_RM_EXTRA_ALLOCATION($SP_RM_EXTRA_ALLOCATION)
	{
		array_push($this->commitArray,'SP_RM_EXTRA_ALLOCATION');
		$this->SP_RM_EXTRA_ALLOCATION = $SP_RM_EXTRA_ALLOCATION;
	}
	
	//set INK_ITEM_EXTRA_ALLOCATION
	function setINK_ITEM_EXTRA_ALLOCATION($INK_ITEM_EXTRA_ALLOCATION)
	{
		array_push($this->commitArray,'INK_ITEM_EXTRA_ALLOCATION');
		$this->INK_ITEM_EXTRA_ALLOCATION = $INK_ITEM_EXTRA_ALLOCATION;
	}
	
	//set INK_ITEM_EXTRA_SRN
	function setINK_ITEM_EXTRA_SRN($INK_ITEM_EXTRA_SRN)
	{
		array_push($this->commitArray,'INK_ITEM_EXTRA_SRN');
		$this->INK_ITEM_EXTRA_SRN = $INK_ITEM_EXTRA_SRN;
	}
	
	//set FOIL_ITEM_EXTRA_MRN
	function setFOIL_ITEM_EXTRA_MRN($FOIL_ITEM_EXTRA_MRN)
	{
		array_push($this->commitArray,'FOIL_ITEM_EXTRA_MRN');
		$this->FOIL_ITEM_EXTRA_MRN = $FOIL_ITEM_EXTRA_MRN;
	}
	
	//set SPECIAL_ITEM_EXTRA_MRN
	function setSPECIAL_ITEM_EXTRA_MRN($SPECIAL_ITEM_EXTRA_MRN)
	{
		array_push($this->commitArray,'SPECIAL_ITEM_EXTRA_MRN');
		$this->SPECIAL_ITEM_EXTRA_MRN = $SPECIAL_ITEM_EXTRA_MRN;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->COMPANY_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($COMPANY_ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "COMPANY_ID='$COMPANY_ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($COMPANY_ID,$SAMPLE_ALLOCATION,$FULL_ALLOCATION,$FOIL_EXTRA_ALLOCATION,$SP_RM_EXTRA_ALLOCATION,$INK_ITEM_EXTRA_ALLOCATION,$INK_ITEM_EXTRA_SRN,$FOIL_ITEM_EXTRA_MRN,$SPECIAL_ITEM_EXTRA_MRN){
		$data = array('COMPANY_ID'=>$COMPANY_ID 
				,'SAMPLE_ALLOCATION'=>$SAMPLE_ALLOCATION 
				,'FULL_ALLOCATION'=>$FULL_ALLOCATION 
				,'FOIL_EXTRA_ALLOCATION'=>$FOIL_EXTRA_ALLOCATION 
				,'SP_RM_EXTRA_ALLOCATION'=>$SP_RM_EXTRA_ALLOCATION 
				,'INK_ITEM_EXTRA_ALLOCATION'=>$INK_ITEM_EXTRA_ALLOCATION 
				,'INK_ITEM_EXTRA_SRN'=>$INK_ITEM_EXTRA_SRN 
				,'FOIL_ITEM_EXTRA_MRN'=>$FOIL_ITEM_EXTRA_MRN 
				,'SPECIAL_ITEM_EXTRA_MRN'=>$SPECIAL_ITEM_EXTRA_MRN 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('COMPANY_ID,SAMPLE_ALLOCATION',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['COMPANY_ID'])
				$html .= '<option selected="selected" value="'.$row['COMPANY_ID'].'">'.$row['SAMPLE_ALLOCATION'].'</option>';	
			else
				$html .= '<option value="'.$row['COMPANY_ID'].'">'.$row['SAMPLE_ALLOCATION'].'</option>';	
		}
		return $html;
	}
	
	//END }
}
?>
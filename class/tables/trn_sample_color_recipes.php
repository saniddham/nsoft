<?php
class trn_sample_color_recipes{
 
	private $db;
	private $table= "trn_sample_color_recipes";
	
	//private property
	private $intSampleNo;
	private $intSampleYear;
	private $intRevisionNo;
	private $strCombo;
	private $strPrintName;
	private $intColorId;
	private $intTechniqueId;
	private $intInkTypeId;
	private $intItem;
	private $intAddedByColorRoom;
	private $intBulkItem;
	private $dblWeight;
	private $intUser;
	private $dtDate;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('intSampleNo'=>'intSampleNo',
										'intSampleYear'=>'intSampleYear',
										'intRevisionNo'=>'intRevisionNo',
										'strCombo'=>'strCombo',
										'strPrintName'=>'strPrintName',
										'intColorId'=>'intColorId',
										'intTechniqueId'=>'intTechniqueId',
										'intInkTypeId'=>'intInkTypeId',
										'intItem'=>'intItem',
										'intAddedByColorRoom'=>'intAddedByColorRoom',
										'intBulkItem'=>'intBulkItem',
										'dblWeight'=>'dblWeight',
										'intUser'=>'intUser',
										'dtDate'=>'dtDate',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	function commit()
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "intSampleNo = ".$this->intSampleNo." and intSampleYear = ".$this->intSampleYear." and intRevisionNo = ".$this->intRevisionNo." and strCombo = ".$this->strCombo." and strPrintName = ".$this->strPrintName." and intColorId = ".$this->intColorId." and intTechniqueId = ".$this->intTechniqueId." and intInkTypeId = ".$this->intInkTypeId." and intItem = ".$this->intItem."" ;
		unset($this->commitArray);
		
		if($this->intId==NULL || $this->intId=='' || $this->intId=='NULL')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun intSampleNo
	function getintSampleNo()
	{
		$this->validate();
		return $this->intSampleNo;
	}
	
	//retun intSampleYear
	function getintSampleYear()
	{
		$this->validate();
		return $this->intSampleYear;
	}
	
	//retun intRevisionNo
	function getintRevisionNo()
	{
		$this->validate();
		return $this->intRevisionNo;
	}
	
	//retun strCombo
	function getstrCombo()
	{
		$this->validate();
		return $this->strCombo;
	}
	
	//retun strPrintName
	function getstrPrintName()
	{
		$this->validate();
		return $this->strPrintName;
	}
	
	//retun intColorId
	function getintColorId()
	{
		$this->validate();
		return $this->intColorId;
	}
	
	//retun intTechniqueId
	function getintTechniqueId()
	{
		$this->validate();
		return $this->intTechniqueId;
	}
	
	//retun intInkTypeId
	function getintInkTypeId()
	{
		$this->validate();
		return $this->intInkTypeId;
	}
	
	//retun intItem
	function getintItem()
	{
		$this->validate();
		return $this->intItem;
	}
	
	//retun intAddedByColorRoom
	function getintAddedByColorRoom()
	{
		$this->validate();
		return $this->intAddedByColorRoom;
	}
	
	//retun intBulkItem
	function getintBulkItem()
	{
		$this->validate();
		return $this->intBulkItem;
	}
	
	//retun dblWeight
	function getdblWeight()
	{
		$this->validate();
		return $this->dblWeight;
	}
	
	//retun intUser
	function getintUser()
	{
		$this->validate();
		return $this->intUser;
	}
	
	//retun dtDate
	function getdtDate()
	{
		$this->validate();
		return $this->dtDate;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set intSampleNo
	function setintSampleNo($intSampleNo)
	{
		array_push($this->commitArray,'intSampleNo');
		$this->intSampleNo = $intSampleNo;
	}
	
	//set intSampleYear
	function setintSampleYear($intSampleYear)
	{
		array_push($this->commitArray,'intSampleYear');
		$this->intSampleYear = $intSampleYear;
	}
	
	//set intRevisionNo
	function setintRevisionNo($intRevisionNo)
	{
		array_push($this->commitArray,'intRevisionNo');
		$this->intRevisionNo = $intRevisionNo;
	}
	
	//set strCombo
	function setstrCombo($strCombo)
	{
		array_push($this->commitArray,'strCombo');
		$this->strCombo = $strCombo;
	}
	
	//set strPrintName
	function setstrPrintName($strPrintName)
	{
		array_push($this->commitArray,'strPrintName');
		$this->strPrintName = $strPrintName;
	}
	
	//set intColorId
	function setintColorId($intColorId)
	{
		array_push($this->commitArray,'intColorId');
		$this->intColorId = $intColorId;
	}
	
	//set intTechniqueId
	function setintTechniqueId($intTechniqueId)
	{
		array_push($this->commitArray,'intTechniqueId');
		$this->intTechniqueId = $intTechniqueId;
	}
	
	//set intInkTypeId
	function setintInkTypeId($intInkTypeId)
	{
		array_push($this->commitArray,'intInkTypeId');
		$this->intInkTypeId = $intInkTypeId;
	}
	
	//set intItem
	function setintItem($intItem)
	{
		array_push($this->commitArray,'intItem');
		$this->intItem = $intItem;
	}
	
	//set intAddedByColorRoom
	function setintAddedByColorRoom($intAddedByColorRoom)
	{
		array_push($this->commitArray,'intAddedByColorRoom');
		$this->intAddedByColorRoom = $intAddedByColorRoom;
	}
	
	//set intBulkItem
	function setintBulkItem($intBulkItem)
	{
		array_push($this->commitArray,'intBulkItem');
		$this->intBulkItem = $intBulkItem;
	}
	
	//set dblWeight
	function setdblWeight($dblWeight)
	{
		array_push($this->commitArray,'dblWeight');
		$this->dblWeight = $dblWeight;
	}
	
	//set intUser
	function setintUser($intUser)
	{
		array_push($this->commitArray,'intUser');
		$this->intUser = $intUser;
	}
	
	//set dtDate
	function setdtDate($dtDate)
	{
		array_push($this->commitArray,'dtDate');
		$this->dtDate = $dtDate;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->intSampleNo=='' || $this->intSampleYear=='' || $this->intRevisionNo=='' || $this->strCombo=='' || $this->strPrintName=='' || $this->intColorId=='' || $this->intTechniqueId=='' || $this->intInkTypeId=='' || $this->intItem=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($intSampleNo , $intSampleYear , $intRevisionNo , $strCombo , $strPrintName , $intColorId , $intTechniqueId , $intInkTypeId , $intItem)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "intSampleNo='$intSampleNo' and intSampleYear='$intSampleYear' and intRevisionNo='$intRevisionNo' and strCombo='$strCombo' and strPrintName='$strPrintName' and intColorId='$intColorId' and intTechniqueId='$intTechniqueId' and intInkTypeId='$intInkTypeId' and intItem='$intItem'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($intSampleNo,$intSampleYear,$intRevisionNo,$strCombo,$strPrintName,$intColorId,$intTechniqueId,$intInkTypeId,$intItem,$intAddedByColorRoom,$intBulkItem,$dblWeight,$intUser,$dtDate){
		$data = array('intSampleNo'=>$intSampleNo 
				,'intSampleYear'=>$intSampleYear 
				,'intRevisionNo'=>$intRevisionNo 
				,'strCombo'=>$strCombo 
				,'strPrintName'=>$strPrintName 
				,'intColorId'=>$intColorId 
				,'intTechniqueId'=>$intTechniqueId 
				,'intInkTypeId'=>$intInkTypeId 
				,'intItem'=>$intItem 
				,'intAddedByColorRoom'=>$intAddedByColorRoom 
				,'intBulkItem'=>$intBulkItem 
				,'dblWeight'=>$dblWeight 
				,'intUser'=>$intUser 
				,'dtDate'=>$dtDate 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('intSampleNo,intSampleYear',  null, $where = $where);
		while($row=mysqli_fetch_array($result)){
			$html .= '<option value="'.$row['intSampleNo'].'">'.$row['intSampleYear'].'</option>';	
		}
		return $html;
	}
	
	
	public function getColorsForSelectedGraphic($orderYear,$graphic){
		
	
		$cols			= " trn_sample_color_recipes.intColorId,
 							trn_sample_color_recipes.intTechniqueId,
 							trn_sample_color_recipes.intInkTypeId ";
		
		$join			= " INNER JOIN trn_orderdetails OD
							ON OD.intSampleNo = trn_sample_color_recipes.intSampleNo
							AND OD.intSampleYear = trn_sample_color_recipes.intSampleYear
							AND OD.intRevisionNo = trn_sample_color_recipes.intRevisionNo
							AND OD.strCombo = trn_sample_color_recipes.strCombo
							AND OD.strPrintName = trn_sample_color_recipes.strPrintName
							INNER JOIN mst_colors MC ON MC.intId=trn_sample_color_recipes.intColorId
							INNER JOIN mst_techniques MT ON MT.intId=trn_sample_color_recipes.intTechniqueId
							INNER JOIN mst_inktypes MI ON MI.intId=trn_sample_color_recipes.intInkTypeId ";
		
		$where			= " OD.intOrderYear = '".$orderYear."' AND OD.strGraphicNo = '".$graphic."' 
							GROUP BY trn_sample_color_recipes.intColorId,
							trn_sample_color_recipes.intTechniqueId,
							trn_sample_color_recipes.intInkTypeId ";
		
		$order			= " MC.strName,MT.strName,MI.strName ";
		
		$result 	= $this->select($cols,$join,$where, $order = null, $limit = null);	
 		
		return 		$result;
	}

	public function getColorsForSelectedOrder($orderNo,$orderYear,$salesOrderId){
		
	
		$cols			= " OD.intSalesOrderId, 
							trn_sample_color_recipes.intColorId,
 							trn_sample_color_recipes.intTechniqueId,
 							trn_sample_color_recipes.intInkTypeId ";
		
		$join			= " INNER JOIN trn_orderdetails OD
							ON OD.intSampleNo = trn_sample_color_recipes.intSampleNo
							AND OD.intSampleYear = trn_sample_color_recipes.intSampleYear
							AND OD.intRevisionNo = trn_sample_color_recipes.intRevisionNo
							AND OD.strCombo = trn_sample_color_recipes.strCombo
							AND OD.strPrintName = trn_sample_color_recipes.strPrintName ";
		
		$where			= " OD.intOrderNo = '".$orderNo."' AND  OD.intOrderYear = '".$orderYear."'  ";
		if($salesOrderId!='')
		$where			.= "  AND   OD.intSalesOrderId = '".$salesOrderId."' ";
		$where			.= "GROUP BY 
							OD.intSalesOrderId,
							trn_sample_color_recipes.intColorId,
							trn_sample_color_recipes.intTechniqueId,
							trn_sample_color_recipes.intInkTypeId ";
		
		$order			= " MC.strName,MT.strName,MI.strName ";
		//echo "select ".$cols." from trn_sample_color_recipes ".$join." where ".$where;
		$result 	= $this->select($cols,$join,$where, $order = null, $limit = null);	
 		
		return 		$result;
	}
	
	
	public function getInkItemsForSelectedOrders($orderArray,$itemArray)
	{
		if($orderArray!='')
			$para	.= "AND CONCAT(OD.intOrderNo,'/',OD.intOrderYear,'/',OD.intSalesOrderId) IN ($orderArray)";
			
		if($itemArray!='')
			$para	.= "AND I.intId IN ($itemArray)"; 
//-----------------------------------------------------------------------------------------------------
		$cols		= "DISTINCT
		                  I.intMainCategory  			AS MAIN_ID,
						  I.intSubCategory  			AS SUB_ID,
						  I.intId  						AS ITEM_ID,
						  I.intUOM   					AS UNIT_ID ";
						  							
		$join		= "INNER JOIN trn_orderdetails OD
						 ON OD.intSampleNo = trn_sample_color_recipes.intSampleNo
						 AND OD.intSampleYear = trn_sample_color_recipes.intSampleYear
						 AND OD.intRevisionNo = trn_sample_color_recipes.intRevisionNo
						 AND OD.strCombo = trn_sample_color_recipes.strCombo
						 AND OD.strPrintName = trn_sample_color_recipes.strPrintName
					   	 INNER JOIN mst_item I
						 ON I.intId = trn_sample_color_recipes.intItem";
					  
		$where		= "1=1
						$para";
					
		$order		= "I.strName";
		$result 	= $this->select($cols,$join,$where,$order,$limit=null);
 		return 		$result;
	}
	
	public function getItemsForSelectedOrdersAndColors($orderNo,$orderYear,$salesOrderId,$colorId,$techniqueId,$inkType)
	{
		$cols		= "DISTINCT
 						 intItem  		AS ITEM_ID ";
						  							
		$join		= "INNER JOIN trn_orderdetails OD
						 ON OD.intSampleNo = trn_sample_color_recipes.intSampleNo
						 AND OD.intSampleYear = trn_sample_color_recipes.intSampleYear
						 AND OD.intRevisionNo = trn_sample_color_recipes.intRevisionNo
						 AND OD.strCombo = trn_sample_color_recipes.strCombo
						 AND OD.strPrintName = trn_sample_color_recipes.strPrintName";
					  
		$where		= "OD.intOrderNo = '$orderNo'
						AND OD.intOrderYear = '$orderYear'
						AND OD.intSalesOrderId = '$salesOrderId'
						AND intColorId = $colorId
						AND intTechniqueId = $techniqueId
						AND intInkTypeId = $inkType";
	
		//echo "select ".$cols." from trn_sample_color_recipes ".$join." where ".$where;
		$result 	= $this->select($cols,$join,$where,$order=null,$limit=null);
 		return 		$result;
	}
	
	public function getColorsForSelectedOrdersAndItem($orderNo,$orderYear,$salesOrderId,$itemId)
	{
		$cols		= "DISTINCT
 						 intColorId  		AS COLOR_ID,
						 intTechniqueId		AS TECHNIQUE_ID,
						 intInkTypeId		AS INK_TYPE";
						  							
		$join		= "INNER JOIN trn_orderdetails OD
						 ON OD.intSampleNo = trn_sample_color_recipes.intSampleNo
						 AND OD.intSampleYear = trn_sample_color_recipes.intSampleYear
						 AND OD.intRevisionNo = trn_sample_color_recipes.intRevisionNo
						 AND OD.strCombo = trn_sample_color_recipes.strCombo
						 AND OD.strPrintName = trn_sample_color_recipes.strPrintName";
					  
		$where		= "OD.intOrderNo = '$orderNo'
						AND OD.intOrderYear = '$orderYear'
						AND OD.intSalesOrderId = '$salesOrderId'
						AND intItem = $itemId";
		//echo "select ".$cols." from trn_sample_color_recipes ".$join." where ".$where;
		$result 	= $this->select($cols,$join,$where,$order=null,$limit=null);
 		return 		$result;
	}
	
	public function getOrderDetailsForSelectedColor($colorId,$techniqueId,$inkTypeId){
		
		$cols			= " OD.intOrderNo,
							OD.intOrderYear,
							OD.intSalesOrderId  ";
		
		$join			= " INNER JOIN trn_orderdetails OD
							ON OD.intSampleNo = trn_sample_color_recipes.intSampleNo
							AND OD.intSampleYear = trn_sample_color_recipes.intSampleYear
							AND OD.intRevisionNo = trn_sample_color_recipes.intRevisionNo
							AND OD.strCombo = trn_sample_color_recipes.strCombo
							AND OD.strPrintName = trn_sample_color_recipes.strPrintName
							INNER JOIN trn_orderheader OH ON OH.intOrderNo = OD.intOrderNo AND
							OH.intOrderYear = OD.intOrderYear
							INNER JOIN mst_colors MC ON MC.intId=trn_sample_color_recipes.intColorId
							INNER JOIN mst_techniques MT ON MT.intId=trn_sample_color_recipes.intTechniqueId
							INNER JOIN mst_inktypes MI ON MI.intId=trn_sample_color_recipes.intInkTypeId ";
		
		$where			= " trn_sample_color_recipes.intColorId = '".$colorId."' AND
							trn_sample_color_recipes.intTechniqueId = '".$techniqueId."' AND
							trn_sample_color_recipes.intInkTypeId = '".$inkTypeId."' AND
							OH.intStatus = 1 
							GROUP BY trn_sample_color_recipes.intColorId,
							trn_sample_color_recipes.intTechniqueId,
							trn_sample_color_recipes.intInkTypeId,
							OD.intOrderYear,
							OD.intOrderNo,
							OD.intSalesOrderId ";
		
		$order			= " MC.strName,MT.strName,MI.strName ";
		
		$result = $this->select($cols,$join,$where,$order, $limit = null);	
		
		return $result;

	}

	public function getStockOrderWiseDetailsForSelectedColor($location,$colorId,$techniqueId,$inkTypeId){
		
		$cols			= " OD.intOrderNo,
							OD.intOrderYear,
							OD.intSalesOrderId,
							(
							SELECT COALESCE((SUM(dblQty))
							FROM ware_stocktransactions_color_room_color SCRC
							WHERE SCRC.intColorId = trn_sample_color_recipes.intColorId AND
							SCRC.intTechnique = trn_sample_color_recipes.intTechniqueId AND
							SCRC.intInkType = trn_sample_color_recipes.intInkTypeId AND
							SCRC.intOrderNo = OD.intOrderNo AND
							SCRC.intOrderYear = OD.intOrderYear AND
							SCRC.intSalesOrderId = OD.intSalesOrderId AND
							SCRC.intLocationId = '".$location."'
							) AS colorBal ";
		
		$join			= " INNER JOIN trn_orderdetails OD
							ON OD.intSampleNo = trn_sample_color_recipes.intSampleNo
							AND OD.intSampleYear = trn_sample_color_recipes.intSampleYear
							AND OD.intRevisionNo = trn_sample_color_recipes.intRevisionNo
							AND OD.strCombo = trn_sample_color_recipes.strCombo
							AND OD.strPrintName = trn_sample_color_recipes.strPrintName
							INNER JOIN mst_colors MC ON MC.intId=trn_sample_color_recipes.intColorId
							INNER JOIN mst_techniques MT ON MT.intId=trn_sample_color_recipes.intTechniqueId
							INNER JOIN mst_inktypes MI ON MI.intId=trn_sample_color_recipes.intInkTypeId ";
		
		$where			= " trn_sample_color_recipes.intColorId = '".$colorId."' AND
							trn_sample_color_recipes.intTechniqueId = '".$techniqueId."' AND
							trn_sample_color_recipes.intInkTypeId = '".$inkTypeId."'
							GROUP BY trn_sample_color_recipes.intColorId,
							trn_sample_color_recipes.intTechniqueId,
							trn_sample_color_recipes.intInkTypeId,
							OD.intOrderYear,
							OD.intOrderNo,
							OD.intSalesOrderId 
							HAVING colorBal > 0 ";
		
		$order			= " MC.strName,MT.strName,MI.strName ";
		
		$result = $this->select($cols,$join,$where,$order, $limit = null);	
		
		return $result;

	}
	
	public function getOrderWiseItems($sampleNo,$sampleYear,$revNo,$combo,$printName)
	{
		$cols	= " DISTINCT trn_sample_color_recipes.intItem,
					MI.strName AS item_name,
					MI.intUOM,
					MU.strName AS unit ";
		
		$join	= " INNER JOIN mst_item MI ON MI.intId=trn_sample_color_recipes.intItem
					INNER JOIN mst_units MU ON MU.intId=MI.intUOM ";
		
		$where	= " intSampleNo = '".$sampleNo."' AND
					intSampleYear = '".$sampleYear."' AND
					intRevisionNo = '".$revNo."' AND
					strCombo = '".$combo."' AND
					strPrintName = '".$printName."' ";
		
		$order	= " MI.strName ";
		
		$result = $this->select($cols,$join,$where,$order, $limit = null);	
		
		return $result;
	}
	
	public function getItemsForSelectedOrdersAndSalesOrders($orderNo,$orderYear,$salesOrderId)
	{
		$cols		= "DISTINCT
 						intItem  AS ITEM_ID ";
						  							
		$join		= "INNER JOIN trn_orderdetails OD
						 ON OD.intSampleNo = trn_sample_color_recipes.intSampleNo
						 AND OD.intSampleYear = trn_sample_color_recipes.intSampleYear
						 AND OD.intRevisionNo = trn_sample_color_recipes.intRevisionNo
						 AND OD.strCombo = trn_sample_color_recipes.strCombo
						 AND OD.strPrintName = trn_sample_color_recipes.strPrintName";
					  
		$where		= " OD.intOrderNo = '$orderNo'
						AND OD.intOrderYear = '$orderYear' ";
		
		if($salesOrderId!='')
			$where	.= " AND OD.intSalesOrderId = '$salesOrderId' ";
		
		$where		.= " GROUP BY OD.intSalesOrderId,intItem ";
			
		$result 	= $this->select($cols,$join,$where,$order=null,$limit=null);
 		return 		$result;
	}
	
	public function getItemWiseSalesOrders($orderNo,$orderYear,$salesOrderId,$item)
	{
		$cols		= " DISTINCT
 						OD.strSalesOrderNo,
						OD.intSalesOrderId,
						OD.intQty ";
						  							
		$join		= " INNER JOIN trn_orderdetails OD ON OD.intSampleNo = trn_sample_color_recipes.intSampleNo AND
						OD.intSampleYear = trn_sample_color_recipes.intSampleYear AND
						OD.intRevisionNo = trn_sample_color_recipes.intRevisionNo AND
						OD.strCombo = trn_sample_color_recipes.strCombo AND
						OD.strPrintName = trn_sample_color_recipes.strPrintName ";
					  
		$where		= " trn_sample_color_recipes.intItem = '".$item."' AND
						OD.intOrderNo = '".$orderNo."' AND
						OD.intOrderYear = '".$orderYear."' ";
		
		if($salesOrderId!='')
			$where	.= " AND OD.intSalesOrderId = '".$salesOrderId."' ";

		$where		.= " GROUP BY OD.intSalesOrderId ";
			
		$result 	= $this->select($cols,$join,$where,$order=null,$limit=null);
 		return 		$result;
	}
	
	//END }
}
?>
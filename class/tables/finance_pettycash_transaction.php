<?php
class finance_pettycash_transaction{

	private $db;
	private $table= "finance_pettycash_transaction";

	//private property
	private $SERIAL_NO;
	private $LOCATION_ID;
	private $DOCUMENT_NO;
	private $DOCUMENT_YEAR;
	private $DOCUMENT_TYPE;
	private $IOU_PARENT_NO;
	private $IOU_PARENT_YEAR;
	private $DEPARTMENT_ID;
	private $REQEST_USER_ID;
	private $PETTY_CASH_ITEM_ID;
	private $INVOICE_AMOUNT;
	private $CASH_BALANCE;
	private $DOCUMENT_DATE;
	private $CREATED_BY;
	private $CREATED_DATE;
	private $commitArray = array();
	private $field_array = array();
	

	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('SERIAL_NO'=>'SERIAL_NO',
										'LOCATION_ID'=>'LOCATION_ID',
										'DOCUMENT_NO'=>'DOCUMENT_NO',
										'DOCUMENT_YEAR'=>'DOCUMENT_YEAR',
										'DOCUMENT_TYPE'=>'DOCUMENT_TYPE',
										'IOU_PARENT_NO'=>'IOU_PARENT_NO',
										'IOU_PARENT_YEAR'=>'IOU_PARENT_YEAR',
										'DEPARTMENT_ID'=>'DEPARTMENT_ID',
										'REQEST_USER_ID'=>'REQEST_USER_ID',
										'PETTY_CASH_ITEM_ID'=>'PETTY_CASH_ITEM_ID',
										'INVOICE_AMOUNT'=>'INVOICE_AMOUNT',
										'CASH_BALANCE'=>'CASH_BALANCE',
										'DOCUMENT_DATE'=>'DOCUMENT_DATE',
										'CREATED_BY'=>'CREATED_BY',
										'CREATED_DATE'=>'CREATED_DATE',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit );
		return $result;
	}

	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data);
		return $this->db->getResult();


	}

	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}

	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();
	}

	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}

	public function get_field_array(){
		return $this->field_array;
	}

	function commit($type='update')
	{
		//commit update
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{
			$data[$v] = $this->$v;
		}
		$where		= "SERIAL_NO = ".$this->SERIAL_NO." and LOCATION_ID = ".$this->LOCATION_ID."" ;
		$this->commitArray = array();

		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}

	//BEGIN - public functions for private properties {
	
	//retun SERIAL_NO
	function getSERIAL_NO()
	{
		$this->validate();
		return $this->SERIAL_NO;
	}
	
	//retun LOCATION_ID
	function getLOCATION_ID()
	{
		$this->validate();
		return $this->LOCATION_ID;
	}
	
	//retun DOCUMENT_NO
	function getDOCUMENT_NO()
	{
		$this->validate();
		return $this->DOCUMENT_NO;
	}
	
	//retun DOCUMENT_YEAR
	function getDOCUMENT_YEAR()
	{
		$this->validate();
		return $this->DOCUMENT_YEAR;
	}
	
	//retun DOCUMENT_TYPE
	function getDOCUMENT_TYPE()
	{
		$this->validate();
		return $this->DOCUMENT_TYPE;
	}
	
	//retun IOU_PARENT_NO
	function getIOU_PARENT_NO()
	{
		$this->validate();
		return $this->IOU_PARENT_NO;
	}
	
	//retun IOU_PARENT_YEAR
	function getIOU_PARENT_YEAR()
	{
		$this->validate();
		return $this->IOU_PARENT_YEAR;
	}
	
	//retun DEPARTMENT_ID
	function getDEPARTMENT_ID()
	{
		$this->validate();
		return $this->DEPARTMENT_ID;
	}
	
	//retun REQEST_USER_ID
	function getREQEST_USER_ID()
	{
		$this->validate();
		return $this->REQEST_USER_ID;
	}
	
	//retun PETTY_CASH_ITEM_ID
	function getPETTY_CASH_ITEM_ID()
	{
		$this->validate();
		return $this->PETTY_CASH_ITEM_ID;
	}
	
	//retun INVOICE_AMOUNT
	function getINVOICE_AMOUNT()
	{
		$this->validate();
		return $this->INVOICE_AMOUNT;
	}
	
	//retun CASH_BALANCE
	function getCASH_BALANCE()
	{
		$this->validate();
		return $this->CASH_BALANCE;
	}
	
	//retun DOCUMENT_DATE
	function getDOCUMENT_DATE()
	{
		$this->validate();
		return $this->DOCUMENT_DATE;
	}
	
	//retun CREATED_BY
	function getCREATED_BY()
	{
		$this->validate();
		return $this->CREATED_BY;
	}
	
	//retun CREATED_DATE
	function getCREATED_DATE()
	{
		$this->validate();
		return $this->CREATED_DATE;
	}
	
	//END }

	//BEGIN - public function set {
	
	//set SERIAL_NO
	function setSERIAL_NO($SERIAL_NO)
	{
		array_push($this->commitArray,'SERIAL_NO');
		$this->SERIAL_NO = $SERIAL_NO;
	}
	
	//set LOCATION_ID
	function setLOCATION_ID($LOCATION_ID)
	{
		array_push($this->commitArray,'LOCATION_ID');
		$this->LOCATION_ID = $LOCATION_ID;
	}
	
	//set DOCUMENT_NO
	function setDOCUMENT_NO($DOCUMENT_NO)
	{
		array_push($this->commitArray,'DOCUMENT_NO');
		$this->DOCUMENT_NO = $DOCUMENT_NO;
	}
	
	//set DOCUMENT_YEAR
	function setDOCUMENT_YEAR($DOCUMENT_YEAR)
	{
		array_push($this->commitArray,'DOCUMENT_YEAR');
		$this->DOCUMENT_YEAR = $DOCUMENT_YEAR;
	}
	
	//set DOCUMENT_TYPE
	function setDOCUMENT_TYPE($DOCUMENT_TYPE)
	{
		array_push($this->commitArray,'DOCUMENT_TYPE');
		$this->DOCUMENT_TYPE = $DOCUMENT_TYPE;
	}
	
	//set IOU_PARENT_NO
	function setIOU_PARENT_NO($IOU_PARENT_NO)
	{
		array_push($this->commitArray,'IOU_PARENT_NO');
		$this->IOU_PARENT_NO = $IOU_PARENT_NO;
	}
	
	//set IOU_PARENT_YEAR
	function setIOU_PARENT_YEAR($IOU_PARENT_YEAR)
	{
		array_push($this->commitArray,'IOU_PARENT_YEAR');
		$this->IOU_PARENT_YEAR = $IOU_PARENT_YEAR;
	}
	
	//set DEPARTMENT_ID
	function setDEPARTMENT_ID($DEPARTMENT_ID)
	{
		array_push($this->commitArray,'DEPARTMENT_ID');
		$this->DEPARTMENT_ID = $DEPARTMENT_ID;
	}
	
	//set REQEST_USER_ID
	function setREQEST_USER_ID($REQEST_USER_ID)
	{
		array_push($this->commitArray,'REQEST_USER_ID');
		$this->REQEST_USER_ID = $REQEST_USER_ID;
	}
	
	//set PETTY_CASH_ITEM_ID
	function setPETTY_CASH_ITEM_ID($PETTY_CASH_ITEM_ID)
	{
		array_push($this->commitArray,'PETTY_CASH_ITEM_ID');
		$this->PETTY_CASH_ITEM_ID = $PETTY_CASH_ITEM_ID;
	}
	
	//set INVOICE_AMOUNT
	function setINVOICE_AMOUNT($INVOICE_AMOUNT)
	{
		array_push($this->commitArray,'INVOICE_AMOUNT');
		$this->INVOICE_AMOUNT = $INVOICE_AMOUNT;
	}
	
	//set CASH_BALANCE
	function setCASH_BALANCE($CASH_BALANCE)
	{
		array_push($this->commitArray,'CASH_BALANCE');
		$this->CASH_BALANCE = $CASH_BALANCE;
	}
	
	//set DOCUMENT_DATE
	function setDOCUMENT_DATE($DOCUMENT_DATE)
	{
		array_push($this->commitArray,'DOCUMENT_DATE');
		$this->DOCUMENT_DATE = $DOCUMENT_DATE;
	}
	
	//set CREATED_BY
	function setCREATED_BY($CREATED_BY)
	{
		array_push($this->commitArray,'CREATED_BY');
		$this->CREATED_BY = $CREATED_BY;
	}
	
	//set CREATED_DATE
	function setCREATED_DATE($CREATED_DATE)
	{
		array_push($this->commitArray,'CREATED_DATE');
		$this->CREATED_DATE = $CREATED_DATE;
	}
	
	//END }

	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->SERIAL_NO=='' || $this->LOCATION_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}

	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}

	public function set($SERIAL_NO , $LOCATION_ID)
	{
		$cols	= "*";

		$join	= NULL;

		$where	= "SERIAL_NO='$SERIAL_NO' and LOCATION_ID='$LOCATION_ID'";

		$result = $this->select($cols,$join,$where);
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}

	//insert as parameters
	public function insertRec($LOCATION_ID,$DOCUMENT_NO,$DOCUMENT_YEAR,$DOCUMENT_TYPE,$IOU_PARENT_NO,$IOU_PARENT_YEAR,$DEPARTMENT_ID,$REQEST_USER_ID,$PETTY_CASH_ITEM_ID,$INVOICE_AMOUNT,$CASH_BALANCE,$DOCUMENT_DATE,$CREATED_BY,$CREATED_DATE){
		$data = array('LOCATION_ID'=>$LOCATION_ID 
				,'DOCUMENT_NO'=>$DOCUMENT_NO 
				,'DOCUMENT_YEAR'=>$DOCUMENT_YEAR 
				,'DOCUMENT_TYPE'=>$DOCUMENT_TYPE 
				,'IOU_PARENT_NO'=>$IOU_PARENT_NO 
				,'IOU_PARENT_YEAR'=>$IOU_PARENT_YEAR 
				,'DEPARTMENT_ID'=>$DEPARTMENT_ID 
				,'REQEST_USER_ID'=>$REQEST_USER_ID 
				,'PETTY_CASH_ITEM_ID'=>$PETTY_CASH_ITEM_ID 
				,'INVOICE_AMOUNT'=>$INVOICE_AMOUNT 
				,'CASH_BALANCE'=>$CASH_BALANCE 
				,'DOCUMENT_DATE'=>$DOCUMENT_DATE 
				,'CREATED_BY'=>$CREATED_BY 
				,'CREATED_DATE'=>$CREATED_DATE 
				);
		return $this->insert($data);
	}

	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('SERIAL_NO,LOCATION_ID',  null, $where = $where);
		$html = '<option value=""></option>';
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['SERIAL_NO'])
				$html .= '<option selected="selected" value="'.$row['SERIAL_NO'].'">'.$row['LOCATION_ID'].'</option>';
			else
				$html .= '<option value="'.$row['SERIAL_NO'].'">'.$row['LOCATION_ID'].'</option>';
		}
		return $html;
	}
	public function getTotalPettyBal($location)
	{
		$cols	= " IFNULL(SUM(finance_pettycash_transaction.CASH_BALANCE),0) AS CASH_BALANCE";
		$where	= " finance_pettycash_transaction.LOCATION_ID = '$location'";	
		
		$result = $this->select($cols,NULL,$where,NULL,NULL);
		$row	= mysqli_fetch_array($result);
		return $row['CASH_BALANCE'];
			
	}
	public function getPettyCashBalance($month,$year,$location)
	{
		$cols	= " IFNULL(SUM(finance_pettycash_transaction.CASH_BALANCE),0) AS CASH_BALANCE";
		$where	= " YEAR(finance_pettycash_transaction.DOCUMENT_DATE) = '$year' AND
					MONTH(finance_pettycash_transaction.DOCUMENT_DATE) = '$month' AND
					finance_pettycash_transaction.LOCATION_ID = '$location'";	
		
		$result = $this->select($cols,NULL,$where,NULL,NULL);
		$row	= mysqli_fetch_array($result);
		return $row['CASH_BALANCE'];
	}
	public function getIOUDetailsCombo($defaultValue=null)
	{
		$cols  = " finance_pettycash_transaction.DOCUMENT_NO,
				  finance_pettycash_transaction.DOCUMENT_YEAR,
				  SUM(finance_pettycash_transaction.CASH_BALANCE) AS CASH_BALANCE,
				  finance_pettycash_transaction.DOCUMENT_TYPE";
				  
		$where	= " finance_pettycash_transaction.DOCUMENT_TYPE = 'IOU' AND
					ABS(finance_pettycash_transaction.CASH_BALANCE) > 0
					GROUP BY
					finance_pettycash_transaction.DOCUMENT_NO,
				   finance_pettycash_transaction.DOCUMENT_YEAR";
		
		$result = $this->select($cols,  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			$concat_option	= $row['DOCUMENT_NO'].'/'.$row['DOCUMENT_YEAR'].'-'.$row['DOCUMENT_TYPE'].'=>'.number_format(abs($row['CASH_BALANCE']));
			$concat_val		= $row['DOCUMENT_NO'].'/'.$row['DOCUMENT_YEAR'];
			if($defaultValue==$concat_val)
				$html .= '<option selected="selected" value="'.$concat_val.'">'.$concat_option.'</option>';	
			else
				$html .= '<option value="'.$concat_val.'">'.$concat_option.'</option>';	
		}
		
		return $html;
		
	}
	public function getIOUDetails($iouNo,$iouYear,$departmentId=null,$itemId=null)
	{
		$cols  = " finance_pettycash_transaction.DOCUMENT_NO,
				   finance_pettycash_transaction.DOCUMENT_YEAR,
				   finance_pettycash_transaction.REQEST_USER_ID,
				   finance_pettycash_transaction.DEPARTMENT_ID,
				   finance_pettycash_transaction.PETTY_CASH_ITEM_ID,
				   ABS(finance_pettycash_transaction.CASH_BALANCE) AS CASH_BAL,
				   finance_pettycash_transaction.INVOICE_AMOUNT,
				   finance_pettycash_transaction.DOCUMENT_TYPE";
				  
		$where	= " finance_pettycash_transaction.DOCUMENT_TYPE = 'IOU' AND
					-- ABS(finance_pettycash_transaction.CASH_BALANCE) > 0 AND
					finance_pettycash_transaction.DOCUMENT_NO = '$iouNo' AND
				    finance_pettycash_transaction.DOCUMENT_YEAR	= '$iouYear'";
					
		if($departmentId != NULL)
			$where	.= " AND  finance_pettycash_transaction.DEPARTMENT_ID = '$departmentId'";
		if($itemId != NULL)
			$where	.= " AND  finance_pettycash_transaction.PETTY_CASH_ITEM_ID = '$itemId'";
		
		$result = $this->select($cols,  null, $where = $where);	
		return $result;	
	}
	public function getIOUCashBal($iouNo,$iouYear)
	{
		$cols	= " IFNULL(SUM(finance_pettycash_transaction.CASH_BALANCE),0) AS CASH_BAL";
		$where	= " finance_pettycash_transaction.DOCUMENT_NO = '$iouNo' AND
					finance_pettycash_transaction.DOCUMENT_YEAR = '$iouYear' ";
		
		$result = $this->select($cols,NULL,$where,NULL,NULL);
		$row	= mysqli_fetch_array($result);
		return $row['CASH_BAL'];
			
	}
	public function getSettleAmount($iouNo,$iouYear,$departmentId)
	{
		$cols	= "IFNULL(SUM(finance_pettycash_transaction.CASH_BALANCE),0) AS SETTLE_AMOUNT";
		$where	= " finance_pettycash_transaction.IOU_PARENT_NO = '$iouNo' AND
					finance_pettycash_transaction.IOU_PARENT_YEAR = '$iouYear' AND
					finance_pettycash_transaction.DEPARTMENT_ID = '$departmentId'";
		
		$result = $this->select($cols,NULL,$where,NULL,NULL);
		$row	= mysqli_fetch_array($result);
		return $row['SETTLE_AMOUNT'];

	}
	
	public function getMonthlyPreviousMonthOpenBalance($fromDate,$location)
	{
				$cols	= "IFNULL(SUM(finance_pettycash_transaction.CASH_BALANCE),0) AS SETTLE_AMOUNT";
		$where	= " finance_pettycash_transaction.IOU_PARENT_NO = '$iouNo' AND
					finance_pettycash_transaction.IOU_PARENT_YEAR = '$iouYear' AND
					finance_pettycash_transaction.DEPARTMENT_ID = '$departmentId'";
		
		$result = $this->select($cols,NULL,$where,NULL,NULL);
		$row	= mysqli_fetch_array($result);
		return $row['SETTLE_AMOUNT'];
	}

	public function getPreviousMonthOpenBalance($fromDate,$location)
	{
		$cols	= "ABS(SUM(CASH_BALANCE)) AS AMOUNT ";
		
		$where	= " DOCUMENT_DATE < '$fromDate'
					AND LOCATION_ID = $location";

		$result = $this->select($cols,NULL,$where,NULL,NULL);
		$row	= mysqli_fetch_array($result);
		return $row['AMOUNT'];
	}
	
	public function getMonthlyIncome($year,$month,$location)
	{
		$cols	= "ABS(SUM(CASH_BALANCE)) AS AMOUNT ";
		
		$where	= " YEAR(DOCUMENT_DATE) = '$year'
					AND MONTH(DOCUMENT_DATE) = '$month'
					AND LOCATION_ID = $location
					AND DOCUMENT_TYPE = 'REIMBURSEMENT'";

		$result = $this->select($cols,NULL,$where,NULL,NULL);
		$row	= mysqli_fetch_array($result);
		return $row['AMOUNT'];
	}
}
?>

<?php

class mst_subcontractor{
 
	private $db;
	private $table= "mst_subcontractor";
	
	function __construct($db)
	{
		$this->db = $db;
		$this->db->field_array_set(array('SUB_CONTRACTOR_ID'=>'SUB_CONTRACTOR_ID',
										'SUB_CONTRACTOR_CODE'=>'SUB_CONTRACTOR_CODE',
										'SUB_CONTRACTOR_NAME'=>'SUB_CONTRACTOR_NAME',
										'STATUS'=>'STATUS',
										'SUB_CONTRACTOR_ADDRESS'=>'SUB_CONTRACTOR_ADDRESS',
										'SUB_CONTRACTOR_EMAIL'=>'SUB_CONTRACTOR_EMAIL',
										'SUB_CONTRACTOR_PHONE'=>'SUB_CONTRACTOR_PHONE',
										'INTER_COMPANY'=>'INTER_COMPANY',
										'INTER_COMPANY_ID'=>'INTER_COMPANY_ID',
										'INTER_LOCATION_ID'=>'INTER_LOCATION_ID',
										));
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		return $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  
		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
}
?>

<?php
include_once (MAIN_ROOT."class/tables/mst_supplier.php");				$mst_supplier			= new mst_supplier($db);
include_once (MAIN_ROOT."class/tables/mst_customer.php");				$mst_customer			= new mst_customer($db);

class finance_mst_chartofaccount{
 
	private $db;
	private $table= "finance_mst_chartofaccount";
	
	//private property
	private $CHART_OF_ACCOUNT_ID;
	private $CHART_OF_ACCOUNT_CODE;
	private $CHART_OF_ACCOUNT_NAME;
	private $SUB_TYPE_ID;
	private $BANK;
	private $SUPPLIER_INVOICE;
	private $SUPPLIER_ADVANCE;
	private $SUPPLIER_PAYMENT;
	private $SUPPLIER_DEBITNOTE;
	private $CUSTOMER_INVOICE;
	private $CUSTOMER_ADVANCE;
	private $CUSTOMER_DEBITNOTE;
	private $CUSTOMER_CREDITNOTE;
	private $CUSTOMER_PAYMENT_RECEIVE;
	private $JOURNAL_ENTRY;
	private $OTHER_PAYABLE_INVOICE;
	private $BANK_PAYMENT;
	private $OTHER_PAYABLE_PAYMENT;
	private $PAYMENT_SCHEDULE;
	private $LOAN_SCHEDULE;
	private $GAIN_LOSS_PROCESS_ID;
	private $CATEGORY_TYPE;
	private $CATEGORY_ID;
	private $STATUS;
	private $CREATED_BY;
	private $CREATED_DATE;
	private $LAST_MODIFY_BY;
	private $LAST_MODIFY_DATE;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('CHART_OF_ACCOUNT_ID'=>'CHART_OF_ACCOUNT_ID',
										'CHART_OF_ACCOUNT_CODE'=>'CHART_OF_ACCOUNT_CODE',
										'CHART_OF_ACCOUNT_NAME'=>'CHART_OF_ACCOUNT_NAME',
										'SUB_TYPE_ID'=>'SUB_TYPE_ID',
										'BANK'=>'BANK',
										'SUPPLIER_INVOICE'=>'SUPPLIER_INVOICE',
										'SUPPLIER_ADVANCE'=>'SUPPLIER_ADVANCE',
										'SUPPLIER_PAYMENT'=>'SUPPLIER_PAYMENT',
										'SUPPLIER_DEBITNOTE'=>'SUPPLIER_DEBITNOTE',
										'CUSTOMER_INVOICE'=>'CUSTOMER_INVOICE',
										'CUSTOMER_ADVANCE'=>'CUSTOMER_ADVANCE',
										'CUSTOMER_DEBITNOTE'=>'CUSTOMER_DEBITNOTE',
										'CUSTOMER_CREDITNOTE'=>'CUSTOMER_CREDITNOTE',
										'CUSTOMER_PAYMENT_RECEIVE'=>'CUSTOMER_PAYMENT_RECEIVE',
										'JOURNAL_ENTRY'=>'JOURNAL_ENTRY',
										'OTHER_PAYABLE_INVOICE'=>'OTHER_PAYABLE_INVOICE',
										'BANK_PAYMENT'=>'BANK_PAYMENT',
										'OTHER_PAYABLE_PAYMENT'=>'OTHER_PAYABLE_PAYMENT',
										'PAYMENT_SCHEDULE'=>'PAYMENT_SCHEDULE',
										'LOAN_SCHEDULE'=>'LOAN_SCHEDULE',
										'GAIN_LOSS_PROCESS_ID'=>'GAIN_LOSS_PROCESS_ID',
										'CATEGORY_TYPE'=>'CATEGORY_TYPE',
										'CATEGORY_ID'=>'CATEGORY_ID',
										'STATUS'=>'STATUS',
										'CREATED_BY'=>'CREATED_BY',
										'CREATED_DATE'=>'CREATED_DATE',
										'LAST_MODIFY_BY'=>'LAST_MODIFY_BY',
										'LAST_MODIFY_DATE'=>'LAST_MODIFY_DATE',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "CHART_OF_ACCOUNT_ID = ".$this->CHART_OF_ACCOUNT_ID."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun CHART_OF_ACCOUNT_ID
	function getCHART_OF_ACCOUNT_ID()
	{
		$this->validate();
		return $this->CHART_OF_ACCOUNT_ID;
	}
	
	//retun CHART_OF_ACCOUNT_CODE
	function getCHART_OF_ACCOUNT_CODE()
	{
		$this->validate();
		return $this->CHART_OF_ACCOUNT_CODE;
	}
	
	//retun CHART_OF_ACCOUNT_NAME
	function getCHART_OF_ACCOUNT_NAME()
	{
		$this->validate();
		return $this->CHART_OF_ACCOUNT_NAME;
	}
	
	//retun SUB_TYPE_ID
	function getSUB_TYPE_ID()
	{
		$this->validate();
		return $this->SUB_TYPE_ID;
	}
	
	//retun BANK
	function getBANK()
	{
		$this->validate();
		return $this->BANK;
	}
	
	//retun SUPPLIER_INVOICE
	function getSUPPLIER_INVOICE()
	{
		$this->validate();
		return $this->SUPPLIER_INVOICE;
	}
	
	//retun SUPPLIER_ADVANCE
	function getSUPPLIER_ADVANCE()
	{
		$this->validate();
		return $this->SUPPLIER_ADVANCE;
	}
	
	//retun SUPPLIER_PAYMENT
	function getSUPPLIER_PAYMENT()
	{
		$this->validate();
		return $this->SUPPLIER_PAYMENT;
	}
	
	//retun SUPPLIER_DEBITNOTE
	function getSUPPLIER_DEBITNOTE()
	{
		$this->validate();
		return $this->SUPPLIER_DEBITNOTE;
	}
	
	//retun CUSTOMER_INVOICE
	function getCUSTOMER_INVOICE()
	{
		$this->validate();
		return $this->CUSTOMER_INVOICE;
	}
	
	//retun CUSTOMER_ADVANCE
	function getCUSTOMER_ADVANCE()
	{
		$this->validate();
		return $this->CUSTOMER_ADVANCE;
	}
	
	//retun CUSTOMER_DEBITNOTE
	function getCUSTOMER_DEBITNOTE()
	{
		$this->validate();
		return $this->CUSTOMER_DEBITNOTE;
	}
	
	//retun CUSTOMER_CREDITNOTE
	function getCUSTOMER_CREDITNOTE()
	{
		$this->validate();
		return $this->CUSTOMER_CREDITNOTE;
	}
	
	//retun CUSTOMER_PAYMENT_RECEIVE
	function getCUSTOMER_PAYMENT_RECEIVE()
	{
		$this->validate();
		return $this->CUSTOMER_PAYMENT_RECEIVE;
	}
	
	//retun JOURNAL_ENTRY
	function getJOURNAL_ENTRY()
	{
		$this->validate();
		return $this->JOURNAL_ENTRY;
	}
	
	//retun OTHER_PAYABLE_INVOICE
	function getOTHER_PAYABLE_INVOICE()
	{
		$this->validate();
		return $this->OTHER_PAYABLE_INVOICE;
	}
	
	//retun BANK_PAYMENT
	function getBANK_PAYMENT()
	{
		$this->validate();
		return $this->BANK_PAYMENT;
	}
	
	//retun OTHER_PAYABLE_PAYMENT
	function getOTHER_PAYABLE_PAYMENT()
	{
		$this->validate();
		return $this->OTHER_PAYABLE_PAYMENT;
	}
	
	//retun PAYMENT_SCHEDULE
	function getPAYMENT_SCHEDULE()
	{
		$this->validate();
		return $this->PAYMENT_SCHEDULE;
	}
	
	//retun LOAN_SCHEDULE
	function getLOAN_SCHEDULE()
	{
		$this->validate();
		return $this->LOAN_SCHEDULE;
	}
	
	//retun GAIN_LOSS_PROCESS_ID
	function getGAIN_LOSS_PROCESS_ID()
	{
		$this->validate();
		return $this->GAIN_LOSS_PROCESS_ID;
	}
	
	//retun CATEGORY_TYPE
	function getCATEGORY_TYPE()
	{
		$this->validate();
		return $this->CATEGORY_TYPE;
	}
	
	//retun CATEGORY_ID
	function getCATEGORY_ID()
	{
		$this->validate();
		return $this->CATEGORY_ID;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//retun CREATED_BY
	function getCREATED_BY()
	{
		$this->validate();
		return $this->CREATED_BY;
	}
	
	//retun CREATED_DATE
	function getCREATED_DATE()
	{
		$this->validate();
		return $this->CREATED_DATE;
	}
	
	//retun LAST_MODIFY_BY
	function getLAST_MODIFY_BY()
	{
		$this->validate();
		return $this->LAST_MODIFY_BY;
	}
	
	//retun LAST_MODIFY_DATE
	function getLAST_MODIFY_DATE()
	{
		$this->validate();
		return $this->LAST_MODIFY_DATE;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set CHART_OF_ACCOUNT_ID
	function setCHART_OF_ACCOUNT_ID($CHART_OF_ACCOUNT_ID)
	{
		array_push($this->commitArray,'CHART_OF_ACCOUNT_ID');
		$this->CHART_OF_ACCOUNT_ID = $CHART_OF_ACCOUNT_ID;
	}
	
	//set CHART_OF_ACCOUNT_CODE
	function setCHART_OF_ACCOUNT_CODE($CHART_OF_ACCOUNT_CODE)
	{
		array_push($this->commitArray,'CHART_OF_ACCOUNT_CODE');
		$this->CHART_OF_ACCOUNT_CODE = $CHART_OF_ACCOUNT_CODE;
	}
	
	//set CHART_OF_ACCOUNT_NAME
	function setCHART_OF_ACCOUNT_NAME($CHART_OF_ACCOUNT_NAME)
	{
		array_push($this->commitArray,'CHART_OF_ACCOUNT_NAME');
		$this->CHART_OF_ACCOUNT_NAME = $CHART_OF_ACCOUNT_NAME;
	}
	
	//set SUB_TYPE_ID
	function setSUB_TYPE_ID($SUB_TYPE_ID)
	{
		array_push($this->commitArray,'SUB_TYPE_ID');
		$this->SUB_TYPE_ID = $SUB_TYPE_ID;
	}
	
	//set BANK
	function setBANK($BANK)
	{
		array_push($this->commitArray,'BANK');
		$this->BANK = $BANK;
	}
	
	//set SUPPLIER_INVOICE
	function setSUPPLIER_INVOICE($SUPPLIER_INVOICE)
	{
		array_push($this->commitArray,'SUPPLIER_INVOICE');
		$this->SUPPLIER_INVOICE = $SUPPLIER_INVOICE;
	}
	
	//set SUPPLIER_ADVANCE
	function setSUPPLIER_ADVANCE($SUPPLIER_ADVANCE)
	{
		array_push($this->commitArray,'SUPPLIER_ADVANCE');
		$this->SUPPLIER_ADVANCE = $SUPPLIER_ADVANCE;
	}
	
	//set SUPPLIER_PAYMENT
	function setSUPPLIER_PAYMENT($SUPPLIER_PAYMENT)
	{
		array_push($this->commitArray,'SUPPLIER_PAYMENT');
		$this->SUPPLIER_PAYMENT = $SUPPLIER_PAYMENT;
	}
	
	//set SUPPLIER_DEBITNOTE
	function setSUPPLIER_DEBITNOTE($SUPPLIER_DEBITNOTE)
	{
		array_push($this->commitArray,'SUPPLIER_DEBITNOTE');
		$this->SUPPLIER_DEBITNOTE = $SUPPLIER_DEBITNOTE;
	}
	
	//set CUSTOMER_INVOICE
	function setCUSTOMER_INVOICE($CUSTOMER_INVOICE)
	{
		array_push($this->commitArray,'CUSTOMER_INVOICE');
		$this->CUSTOMER_INVOICE = $CUSTOMER_INVOICE;
	}
	
	//set CUSTOMER_ADVANCE
	function setCUSTOMER_ADVANCE($CUSTOMER_ADVANCE)
	{
		array_push($this->commitArray,'CUSTOMER_ADVANCE');
		$this->CUSTOMER_ADVANCE = $CUSTOMER_ADVANCE;
	}
	
	//set CUSTOMER_DEBITNOTE
	function setCUSTOMER_DEBITNOTE($CUSTOMER_DEBITNOTE)
	{
		array_push($this->commitArray,'CUSTOMER_DEBITNOTE');
		$this->CUSTOMER_DEBITNOTE = $CUSTOMER_DEBITNOTE;
	}
	
	//set CUSTOMER_CREDITNOTE
	function setCUSTOMER_CREDITNOTE($CUSTOMER_CREDITNOTE)
	{
		array_push($this->commitArray,'CUSTOMER_CREDITNOTE');
		$this->CUSTOMER_CREDITNOTE = $CUSTOMER_CREDITNOTE;
	}
	
	//set CUSTOMER_PAYMENT_RECEIVE
	function setCUSTOMER_PAYMENT_RECEIVE($CUSTOMER_PAYMENT_RECEIVE)
	{
		array_push($this->commitArray,'CUSTOMER_PAYMENT_RECEIVE');
		$this->CUSTOMER_PAYMENT_RECEIVE = $CUSTOMER_PAYMENT_RECEIVE;
	}
	
	//set JOURNAL_ENTRY
	function setJOURNAL_ENTRY($JOURNAL_ENTRY)
	{
		array_push($this->commitArray,'JOURNAL_ENTRY');
		$this->JOURNAL_ENTRY = $JOURNAL_ENTRY;
	}
	
	//set OTHER_PAYABLE_INVOICE
	function setOTHER_PAYABLE_INVOICE($OTHER_PAYABLE_INVOICE)
	{
		array_push($this->commitArray,'OTHER_PAYABLE_INVOICE');
		$this->OTHER_PAYABLE_INVOICE = $OTHER_PAYABLE_INVOICE;
	}
	
	//set BANK_PAYMENT
	function setBANK_PAYMENT($BANK_PAYMENT)
	{
		array_push($this->commitArray,'BANK_PAYMENT');
		$this->BANK_PAYMENT = $BANK_PAYMENT;
	}
	
	//set OTHER_PAYABLE_PAYMENT
	function setOTHER_PAYABLE_PAYMENT($OTHER_PAYABLE_PAYMENT)
	{
		array_push($this->commitArray,'OTHER_PAYABLE_PAYMENT');
		$this->OTHER_PAYABLE_PAYMENT = $OTHER_PAYABLE_PAYMENT;
	}
	
	//set PAYMENT_SCHEDULE
	function setPAYMENT_SCHEDULE($PAYMENT_SCHEDULE)
	{
		array_push($this->commitArray,'PAYMENT_SCHEDULE');
		$this->PAYMENT_SCHEDULE = $PAYMENT_SCHEDULE;
	}
	
	//set LOAN_SCHEDULE
	function setLOAN_SCHEDULE($LOAN_SCHEDULE)
	{
		array_push($this->commitArray,'LOAN_SCHEDULE');
		$this->LOAN_SCHEDULE = $LOAN_SCHEDULE;
	}
	
	//set GAIN_LOSS_PROCESS_ID
	function setGAIN_LOSS_PROCESS_ID($GAIN_LOSS_PROCESS_ID)
	{
		array_push($this->commitArray,'GAIN_LOSS_PROCESS_ID');
		$this->GAIN_LOSS_PROCESS_ID = $GAIN_LOSS_PROCESS_ID;
	}
	
	//set CATEGORY_TYPE
	function setCATEGORY_TYPE($CATEGORY_TYPE)
	{
		array_push($this->commitArray,'CATEGORY_TYPE');
		$this->CATEGORY_TYPE = $CATEGORY_TYPE;
	}
	
	//set CATEGORY_ID
	function setCATEGORY_ID($CATEGORY_ID)
	{
		array_push($this->commitArray,'CATEGORY_ID');
		$this->CATEGORY_ID = $CATEGORY_ID;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//set CREATED_BY
	function setCREATED_BY($CREATED_BY)
	{
		array_push($this->commitArray,'CREATED_BY');
		$this->CREATED_BY = $CREATED_BY;
	}
	
	//set CREATED_DATE
	function setCREATED_DATE($CREATED_DATE)
	{
		array_push($this->commitArray,'CREATED_DATE');
		$this->CREATED_DATE = $CREATED_DATE;
	}
	
	//set LAST_MODIFY_BY
	function setLAST_MODIFY_BY($LAST_MODIFY_BY)
	{
		array_push($this->commitArray,'LAST_MODIFY_BY');
		$this->LAST_MODIFY_BY = $LAST_MODIFY_BY;
	}
	
	//set LAST_MODIFY_DATE
	function setLAST_MODIFY_DATE($LAST_MODIFY_DATE)
	{
		array_push($this->commitArray,'LAST_MODIFY_DATE');
		$this->LAST_MODIFY_DATE = $LAST_MODIFY_DATE;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->CHART_OF_ACCOUNT_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($CHART_OF_ACCOUNT_ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "CHART_OF_ACCOUNT_ID='$CHART_OF_ACCOUNT_ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($CHART_OF_ACCOUNT_ID,$CHART_OF_ACCOUNT_CODE,$CHART_OF_ACCOUNT_NAME,$SUB_TYPE_ID,$BANK,$SUPPLIER_INVOICE,$SUPPLIER_ADVANCE,$SUPPLIER_PAYMENT,$SUPPLIER_DEBITNOTE,$CUSTOMER_INVOICE,$CUSTOMER_ADVANCE,$CUSTOMER_DEBITNOTE,$CUSTOMER_CREDITNOTE,$CUSTOMER_PAYMENT_RECEIVE,$JOURNAL_ENTRY,$OTHER_PAYABLE_INVOICE,$BANK_PAYMENT,$OTHER_PAYABLE_PAYMENT,$PAYMENT_SCHEDULE,$LOAN_SCHEDULE,$GAIN_LOSS_PROCESS_ID,$CATEGORY_TYPE,$CATEGORY_ID,$STATUS,$CREATED_BY,$CREATED_DATE,$LAST_MODIFY_BY,$LAST_MODIFY_DATE){
		$data = array('CHART_OF_ACCOUNT_ID'=>$CHART_OF_ACCOUNT_ID 
				,'CHART_OF_ACCOUNT_CODE'=>$CHART_OF_ACCOUNT_CODE 
				,'CHART_OF_ACCOUNT_NAME'=>$CHART_OF_ACCOUNT_NAME 
				,'SUB_TYPE_ID'=>$SUB_TYPE_ID 
				,'BANK'=>$BANK 
				,'SUPPLIER_INVOICE'=>$SUPPLIER_INVOICE 
				,'SUPPLIER_ADVANCE'=>$SUPPLIER_ADVANCE 
				,'SUPPLIER_PAYMENT'=>$SUPPLIER_PAYMENT 
				,'SUPPLIER_DEBITNOTE'=>$SUPPLIER_DEBITNOTE 
				,'CUSTOMER_INVOICE'=>$CUSTOMER_INVOICE 
				,'CUSTOMER_ADVANCE'=>$CUSTOMER_ADVANCE 
				,'CUSTOMER_DEBITNOTE'=>$CUSTOMER_DEBITNOTE 
				,'CUSTOMER_CREDITNOTE'=>$CUSTOMER_CREDITNOTE 
				,'CUSTOMER_PAYMENT_RECEIVE'=>$CUSTOMER_PAYMENT_RECEIVE 
				,'JOURNAL_ENTRY'=>$JOURNAL_ENTRY 
				,'OTHER_PAYABLE_INVOICE'=>$OTHER_PAYABLE_INVOICE 
				,'BANK_PAYMENT'=>$BANK_PAYMENT 
				,'OTHER_PAYABLE_PAYMENT'=>$OTHER_PAYABLE_PAYMENT 
				,'PAYMENT_SCHEDULE'=>$PAYMENT_SCHEDULE 
				,'LOAN_SCHEDULE'=>$LOAN_SCHEDULE 
				,'GAIN_LOSS_PROCESS_ID'=>$GAIN_LOSS_PROCESS_ID 
				,'CATEGORY_TYPE'=>$CATEGORY_TYPE 
				,'CATEGORY_ID'=>$CATEGORY_ID 
				,'STATUS'=>$STATUS 
				,'CREATED_BY'=>$CREATED_BY 
				,'CREATED_DATE'=>$CREATED_DATE 
				,'LAST_MODIFY_BY'=>$LAST_MODIFY_BY 
				,'LAST_MODIFY_DATE'=>$LAST_MODIFY_DATE 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('CHART_OF_ACCOUNT_ID,CHART_OF_ACCOUNT_NAME',  null, $where = $where,'CHART_OF_ACCOUNT_NAME');
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['CHART_OF_ACCOUNT_ID'])
				$html .= '<option selected="selected" value="'.$row['CHART_OF_ACCOUNT_ID'].'">'.$row['CHART_OF_ACCOUNT_NAME'].'</option>';	
			else
				$html .= '<option value="'.$row['CHART_OF_ACCOUNT_ID'].'">'.$row['CHART_OF_ACCOUNT_NAME'].'</option>';	
		}
		return $html;
	}
	
	public function getFilterdAccountComboReturnResult($companyId,$financeType)
	{
		$cols	= "finance_mst_chartofaccount.CHART_OF_ACCOUNT_ID,
				CONCAT(FMT.FINANCE_TYPE_CODE,'',FMMT.MAIN_TYPE_CODE,'',FMST.SUB_TYPE_CODE,'',finance_mst_chartofaccount.CHART_OF_ACCOUNT_CODE) AS ACCOUNT_CODE,
				finance_mst_chartofaccount.CHART_OF_ACCOUNT_NAME";
		
		$join	= "INNER JOIN finance_mst_account_sub_type FMST ON FMST.SUB_TYPE_ID=finance_mst_chartofaccount.SUB_TYPE_ID
				INNER JOIN finance_mst_account_main_type FMMT ON FMMT.MAIN_TYPE_ID=FMST.MAIN_TYPE_ID
				INNER JOIN finance_mst_account_type FMT ON FMT.FINANCE_TYPE_ID=FMMT.FINANCE_TYPE_ID
				INNER JOIN finance_mst_chartofaccount_company FCOAC ON FCOAC.CHART_OF_ACCOUNT_ID=finance_mst_chartofaccount.CHART_OF_ACCOUNT_ID";
		
		$where	= "FCOAC.COMPANY_ID='$companyId'
					AND finance_mst_chartofaccount.CATEGORY_TYPE='$financeType'
					AND finance_mst_chartofaccount.STATUS = 1";
		
		$order  = "finance_mst_chartofaccount.CHART_OF_ACCOUNT_NAME";
		
		return $this->select($cols,$join,$where,$order,$limit=null);
	}
	
	public function getFilterdAccountCombo($companyId,$financeType)
	{
		$cols	= "finance_mst_chartofaccount.CHART_OF_ACCOUNT_ID,
				CONCAT(FMT.FINANCE_TYPE_CODE,'',FMMT.MAIN_TYPE_CODE,'',FMST.SUB_TYPE_CODE,'',finance_mst_chartofaccount.CHART_OF_ACCOUNT_CODE) AS ACCOUNT_CODE,
				finance_mst_chartofaccount.CHART_OF_ACCOUNT_NAME";
		
		$join	= "INNER JOIN finance_mst_account_sub_type FMST ON FMST.SUB_TYPE_ID=finance_mst_chartofaccount.SUB_TYPE_ID
				INNER JOIN finance_mst_account_main_type FMMT ON FMMT.MAIN_TYPE_ID=FMST.MAIN_TYPE_ID
				INNER JOIN finance_mst_account_type FMT ON FMT.FINANCE_TYPE_ID=FMMT.FINANCE_TYPE_ID
				INNER JOIN finance_mst_chartofaccount_company FCOAC ON FCOAC.CHART_OF_ACCOUNT_ID=finance_mst_chartofaccount.CHART_OF_ACCOUNT_ID";
		
		$where	= "FCOAC.COMPANY_ID='$companyId'
					AND finance_mst_chartofaccount.CATEGORY_TYPE='$financeType'
					AND finance_mst_chartofaccount.STATUS = 1";
		
		$order  = "finance_mst_chartofaccount.CHART_OF_ACCOUNT_NAME";
		
		$result = $this->select($cols,$join,$where,$order,$limit=null);
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['CHART_OF_ACCOUNT_ID'])
				$html .= "<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\">".$row['CHART_OF_ACCOUNT_NAME']." - ".$row['ACCOUNT_CODE']."</option>";	
			else
				$html .= "<option value=\"".$row['CHART_OF_ACCOUNT_ID']."\">".$row['CHART_OF_ACCOUNT_NAME']." - ".$row['ACCOUNT_CODE']."</option>";	
		}
		return $html;		
	}
	
	public function getFullGLAccount($glId)
	{
		$cols	= "CONCAT(FMT.FINANCE_TYPE_CODE,'',FMMT.MAIN_TYPE_CODE,'',FMST.SUB_TYPE_CODE,'',finance_mst_chartofaccount.CHART_OF_ACCOUNT_CODE,' - ',finance_mst_chartofaccount.CHART_OF_ACCOUNT_NAME) AS GL_NAME ";
		
		$join 	= "INNER JOIN finance_mst_account_sub_type FMST
					 ON FMST.SUB_TYPE_ID = finance_mst_chartofaccount.SUB_TYPE_ID
				   INNER JOIN finance_mst_account_main_type FMMT
					 ON FMMT.MAIN_TYPE_ID = FMST.MAIN_TYPE_ID
				   INNER JOIN finance_mst_account_type FMT
					 ON FMT.FINANCE_TYPE_ID = FMMT.FINANCE_TYPE_ID ";
		
		$where 	= "CHART_OF_ACCOUNT_ID = '$glId' ";
		
		$result = $this->select($cols,$join,$where);
		$row	= mysqli_fetch_array($result);
		return 	$row['GL_NAME'];
	}
	
	public function getCatogoryTypeWiseGL($categoryType,$categoryId)
	{
		global $mst_supplier;
		global $mst_customer;
		
		$cols	= "CHART_OF_ACCOUNT_ID";
		
		$where	= "CATEGORY_TYPE='$categoryType'
				    	AND CATEGORY_ID = '$categoryId' ";
		
		$result = $this->select($cols,$join=null,$where,$order=null,$limit=null);
		$row	= mysqli_fetch_array($result);
		$row_count	= mysqli_num_rows($result);
		
		if($row_count>0)
			return $row["CHART_OF_ACCOUNT_ID"];
		else
		{
			switch($categoryType)
			{
				case 'C':
					$mst_customer->set($categoryId);
					$name	= $mst_customer->getstrName();
					$catName = 'Customer';
					break;
				case 'S':
					$mst_supplier->set($categoryId);
					$name	= $mst_supplier->getstrName();
					$catName = 'Supplier';
					break;
				default :
					break;
			}
			
			throw new Exception("No GL Account available for $catName : $name");
		}
	}
}
?>
<?php
class finance_supplier_creditnote_details{
 
	private $db;
	private $table= "finance_supplier_creditnote_details";
	
	//private property
	private $CREDIT_NO;
	private $CREDIT_YEAR;
	private $ITEM_ID;
	private $AMOUNT;
	private $TAX_AMOUNT;
	private $TAX_GROUP_ID;
	private $COST_CENTER_ID;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('CREDIT_NO'=>'CREDIT_NO',
										'CREDIT_YEAR'=>'CREDIT_YEAR',
										'ITEM_ID'=>'ITEM_ID',
										'AMOUNT'=>'AMOUNT',
										'TAX_AMOUNT'=>'TAX_AMOUNT',
										'TAX_GROUP_ID'=>'TAX_GROUP_ID',
										'COST_CENTER_ID'=>'COST_CENTER_ID',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "CREDIT_NO = ".$this->CREDIT_NO." and CREDIT_YEAR = ".$this->CREDIT_YEAR." and ITEM_ID = ".$this->ITEM_ID."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun CREDIT_NO
	function getCREDIT_NO()
	{
		$this->validate();
		return $this->CREDIT_NO;
	}
	
	//retun CREDIT_YEAR
	function getCREDIT_YEAR()
	{
		$this->validate();
		return $this->CREDIT_YEAR;
	}
	
	//retun ITEM_ID
	function getITEM_ID()
	{
		$this->validate();
		return $this->ITEM_ID;
	}
	
	//retun AMOUNT
	function getAMOUNT()
	{
		$this->validate();
		return $this->AMOUNT;
	}
	
	//retun TAX_AMOUNT
	function getTAX_AMOUNT()
	{
		$this->validate();
		return $this->TAX_AMOUNT;
	}
	
	//retun TAX_GROUP_ID
	function getTAX_GROUP_ID()
	{
		$this->validate();
		return $this->TAX_GROUP_ID;
	}
	
	//retun COST_CENTER_ID
	function getCOST_CENTER_ID()
	{
		$this->validate();
		return $this->COST_CENTER_ID;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set CREDIT_NO
	function setCREDIT_NO($CREDIT_NO)
	{
		array_push($this->commitArray,'CREDIT_NO');
		$this->CREDIT_NO = $CREDIT_NO;
	}
	
	//set CREDIT_YEAR
	function setCREDIT_YEAR($CREDIT_YEAR)
	{
		array_push($this->commitArray,'CREDIT_YEAR');
		$this->CREDIT_YEAR = $CREDIT_YEAR;
	}
	
	//set ITEM_ID
	function setITEM_ID($ITEM_ID)
	{
		array_push($this->commitArray,'ITEM_ID');
		$this->ITEM_ID = $ITEM_ID;
	}
	
	//set AMOUNT
	function setAMOUNT($AMOUNT)
	{
		array_push($this->commitArray,'AMOUNT');
		$this->AMOUNT = $AMOUNT;
	}
	
	//set TAX_AMOUNT
	function setTAX_AMOUNT($TAX_AMOUNT)
	{
		array_push($this->commitArray,'TAX_AMOUNT');
		$this->TAX_AMOUNT = $TAX_AMOUNT;
	}
	
	//set TAX_GROUP_ID
	function setTAX_GROUP_ID($TAX_GROUP_ID)
	{
		array_push($this->commitArray,'TAX_GROUP_ID');
		$this->TAX_GROUP_ID = $TAX_GROUP_ID;
	}
	
	//set COST_CENTER_ID
	function setCOST_CENTER_ID($COST_CENTER_ID)
	{
		array_push($this->commitArray,'COST_CENTER_ID');
		$this->COST_CENTER_ID = $COST_CENTER_ID;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->CREDIT_NO=='' || $this->CREDIT_YEAR=='' || $this->ITEM_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($CREDIT_NO , $CREDIT_YEAR , $ITEM_ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "CREDIT_NO='$CREDIT_NO' and CREDIT_YEAR='$CREDIT_YEAR' and ITEM_ID='$ITEM_ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($CREDIT_NO,$CREDIT_YEAR,$ITEM_ID,$AMOUNT,$TAX_AMOUNT,$TAX_GROUP_ID,$COST_CENTER_ID){
		$data = array('CREDIT_NO'=>$CREDIT_NO 
				,'CREDIT_YEAR'=>$CREDIT_YEAR 
				,'ITEM_ID'=>$ITEM_ID 
				,'AMOUNT'=>$AMOUNT 
				,'TAX_AMOUNT'=>$TAX_AMOUNT 
				,'TAX_GROUP_ID'=>$TAX_GROUP_ID 
				,'COST_CENTER_ID'=>$COST_CENTER_ID 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('CREDIT_NO,CREDIT_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['CREDIT_NO'])
				$html .= '<option selected="selected" value="'.$row['CREDIT_NO'].'">'.$row['CREDIT_YEAR'].'</option>';	
			else
				$html .= '<option value="'.$row['CREDIT_NO'].'">'.$row['CREDIT_YEAR'].'</option>';	
		}
		return $html;
	}
	
	//END }
#BEGIN 	- USER DEFINED FUNCTIONS {
	public function getSavedDetails($creditNo,$creditYear)
	{
		$cols	= "mst_item.intMainCategory		 	AS MAIN_CAT_ID,
				   mst_item.intSubCategory  		AS SUB_CAT_ID,
				   mst_item.strName         		AS ITEM_NAME,
				   mst_item.intId         		    AS ITEM_ID,
				   mst_item.intUOM          		AS UNIT_ID,
				   AMOUNT							AS AMOUNT,
				   TAX_AMOUNT						AS TAX_AMOUNT,
				   AMOUNT+TAX_AMOUNT				AS TOTAL_AMOUNT,
				   TAX_GROUP_ID							AS TAX_GROUP_ID,
				   COST_CENTER_ID					AS COST_CENTER_ID,
				   (SELECT CG.CHART_OF_ACCOUNT_ID
					FROM finance_supplier_creditnote_gl CG
					INNER JOIN finance_mst_chartofaccount_subcategory COAS 
						ON COAS.CHART_OF_ACCOUNT_ID = CG.CHART_OF_ACCOUNT_ID
					WHERE CG.CREDIT_NO = finance_supplier_creditnote_details.CREDIT_NO 
						AND CG.CREDIT_YEAR = finance_supplier_creditnote_details.CREDIT_YEAR 
						AND COAS.SUBCATEGORY_ID = mst_item.intSubCategory) 								
													AS GLACCOUNT_ID,					
				  (SELECT
					 T.CHART_OF_ACCOUNT_ID
				   FROM finance_mst_chartofaccount_tax T
				   WHERE T.TAX_ID = finance_supplier_creditnote_details.TAX_GROUP_ID) 
				   									AS TAX_GL_ID ";
		
		$join	= "INNER JOIN mst_item
    				ON mst_item.intId = finance_supplier_creditnote_details.ITEM_ID";
		
		$where	= "CREDIT_NO = $creditNo AND CREDIT_YEAR = $creditYear";

		return $this->select($cols,$join,$where);
	}
	
	public function getTotalAmount($creditNo,$creditYear)
	{
		$cols	= "ROUND(SUM(AMOUNT + TAX_AMOUNT),2) AS TOTAL_AMOUNT ";

		$where	= "CREDIT_NO = $creditNo AND CREDIT_YEAR = $creditYear";
		
		return $this->select($cols,null,$where);
	}
	
	public function getApprovedInvoiceAmount($invoiceNo,$invoiceYear)
	{
		$cols	= "ROUND(COALESCE(SUM(AMOUNT),0),2) AS TOTAL_AMOUNT ";
	
		$join 	= "INNER JOIN finance_supplier_creditnote_header H
				       ON H.CREDIT_NO = finance_supplier_creditnote_details.CREDIT_NO
					   AND H.CREDIT_YEAR = finance_supplier_creditnote_details.CREDIT_YEAR";
		
		$where	= "H.STATUS = 1
				   AND H.PURCHASE_INVOICE_NO = $invoiceNo 
				   AND H.PURCHASE_INVOICE_YEAR = $invoiceYear";
		
		$result = $this->select($cols,$join,$where);
		$row	= mysqli_fetch_array($result);
		return $row['TOTAL_AMOUNT'];
	}
#END 	- USER DEFINED FUNCTIONS }
}
?>
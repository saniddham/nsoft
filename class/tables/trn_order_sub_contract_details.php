
<?php

class trn_order_sub_contract_details{
 
	private $db;
	private $table= "trn_order_sub_contract_details";
	
	function __construct($db)
	{
		$this->db = $db;
		$this->db->field_array_set(array('SUB_CONTRACT_NO'=>'SUB_CONTRACT_NO',
										'SUB_CONTRACT_YEAR'=>'SUB_CONTRACT_YEAR',
										'SALES_ORDER_ID'=>'SALES_ORDER_ID',
										'SUB_CONTR_JOB_ID'=>'SUB_CONTR_JOB_ID',
										'QTY'=>'QTY',
										'PRICE'=>'PRICE',
										'PDS_DATE'=>'PDS_DATE',
										'DELIVERY_DATE'=>'DELIVERY_DATE',
										));
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		return $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  
		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
}
?>

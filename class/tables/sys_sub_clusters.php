<?php
class sys_sub_clusters{

	private $db;
	private $table= "sys_sub_clusters";

	//private property
	private $ID;
	private $MAIN_CLUSTER;
	private $CODE;
	private $NAME;
	private $STATUS;
	private $REMARKS;
	private $commitArray = array();
	private $field_array = array();
	

	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('ID'=>'ID',
										'MAIN_CLUSTER'=>'MAIN_CLUSTER',
										'CODE'=>'CODE',
										'NAME'=>'NAME',
										'STATUS'=>'STATUS',
										'REMARKS'=>'REMARKS',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit );
		return $result;
	}

	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data);
		return $this->db->getResult();


	}

	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}

	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();
	}

	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}

	public function get_field_array(){
		return $this->field_array;
	}

	function commit($type='update')
	{
		//commit update
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{
			$data[$v] = $this->$v;
		}
		$where		= "ID = ".$this->ID."" ;
		$this->commitArray = array();

		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}

	//BEGIN - public functions for private properties {
	
	//retun ID
	function getID()
	{
		$this->validate();
		return $this->ID;
	}
	
	//retun MAIN_CLUSTER
	function getMAIN_CLUSTER()
	{
		$this->validate();
		return $this->MAIN_CLUSTER;
	}
	
	//retun CODE
	function getCODE()
	{
		$this->validate();
		return $this->CODE;
	}
	
	//retun NAME
	function getNAME()
	{
		$this->validate();
		return $this->NAME;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//retun REMARKS
	function getREMARKS()
	{
		$this->validate();
		return $this->REMARKS;
	}
	
	//END }

	//BEGIN - public function set {
	
	//set ID
	function setID($ID)
	{
		array_push($this->commitArray,'ID');
		$this->ID = $ID;
	}
	
	//set MAIN_CLUSTER
	function setMAIN_CLUSTER($MAIN_CLUSTER)
	{
		array_push($this->commitArray,'MAIN_CLUSTER');
		$this->MAIN_CLUSTER = $MAIN_CLUSTER;
	}
	
	//set CODE
	function setCODE($CODE)
	{
		array_push($this->commitArray,'CODE');
		$this->CODE = $CODE;
	}
	
	//set NAME
	function setNAME($NAME)
	{
		array_push($this->commitArray,'NAME');
		$this->NAME = $NAME;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//set REMARKS
	function setREMARKS($REMARKS)
	{
		array_push($this->commitArray,'REMARKS');
		$this->REMARKS = $REMARKS;
	}
	
	//END }

	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}

	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}

	public function set($ID)
	{
		$cols	= "*";

		$join	= NULL;

		$where	= "ID='$ID'";

		$result = $this->select($cols,$join,$where);
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}

	//insert as parameters
	public function insertRec($MAIN_CLUSTER,$CODE,$NAME,$STATUS,$REMARKS){
		$data = array('MAIN_CLUSTER'=>$MAIN_CLUSTER 
				,'CODE'=>$CODE 
				,'NAME'=>$NAME 
				,'STATUS'=>$STATUS 
				,'REMARKS'=>$REMARKS 
				);
		return $this->insert($data);
	}

	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('ID,NAME',  null, $where = $where);
		$html = '<option value=""></option>';
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['ID'])
				$html .= '<option selected="selected" value="'.$row['ID'].'">'.$row['NAME'].'</option>';
			else
				$html .= '<option value="'.$row['ID'].'">'.$row['NAME'].'</option>';
		}
		return $html;
	}
	
	//END }
}
?>
<?php
class finance_mst_pettycash_item{
 
	private $db;
	private $table= "finance_mst_pettycash_item";
	
	//private property
	private $ID;
	private $ITEM_ID;
	private $CATEGORY_ID;
	private $CODE;
	private $NAME;
	private $STATUS;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('ID'=>'ID',
										'ITEM_ID'=>'ITEM_ID',
										'CATEGORY_ID'=>'CATEGORY_ID',
										'CODE'=>'CODE',
										'NAME'=>'NAME',
										'STATUS'=>'STATUS',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "ID = ".$this->ID."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun ID
	function getID()
	{
		$this->validate();
		return $this->ID;
	}
	
	//retun ITEM_ID
	function getITEM_ID()
	{
		$this->validate();
		return $this->ITEM_ID;
	}
	
	//retun CATEGORY_ID
	function getCATEGORY_ID()
	{
		$this->validate();
		return $this->CATEGORY_ID;
	}
	
	//retun CODE
	function getCODE()
	{
		$this->validate();
		return $this->CODE;
	}
	
	//retun NAME
	function getNAME()
	{
		$this->validate();
		return $this->NAME;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set ID
	function setID($ID)
	{
		array_push($this->commitArray,'ID');
		$this->ID = $ID;
	}
	
	//set ITEM_ID
	function setITEM_ID($ITEM_ID)
	{
		array_push($this->commitArray,'ITEM_ID');
		$this->ITEM_ID = $ITEM_ID;
	}
	
	//set CATEGORY_ID
	function setCATEGORY_ID($CATEGORY_ID)
	{
		array_push($this->commitArray,'CATEGORY_ID');
		$this->CATEGORY_ID = $CATEGORY_ID;
	}
	
	//set CODE
	function setCODE($CODE)
	{
		array_push($this->commitArray,'CODE');
		$this->CODE = $CODE;
	}
	
	//set NAME
	function setNAME($NAME)
	{
		array_push($this->commitArray,'NAME');
		$this->NAME = $NAME;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "ID='$ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($ID,$ITEM_ID,$CATEGORY_ID,$CODE,$NAME,$STATUS){
		$data = array('ID'=>$ID 
				,'ITEM_ID'=>$ITEM_ID 
				,'CATEGORY_ID'=>$CATEGORY_ID 
				,'CODE'=>$CODE 
				,'NAME'=>$NAME 
				,'STATUS'=>$STATUS 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('ID,NAME',  null, $where = $where,"NAME");
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['ID'])
				$html .= '<option selected="selected" value="'.$row['ID'].'">'.$row['NAME'].'</option>';	
			else
				$html .= '<option value="'.$row['ID'].'">'.$row['NAME'].'</option>';	
		}
		return $html;
	}
	
	public function getPettyItem($category)
	{
		$cols = "ID AS itemId,
				NAME AS itemName";
				
		$where = "STATUS = 1 AND
				  CATEGORY_ID	= '$category' ";
		
		$result	= $this->select($cols,NULL,$where,"NAME",NULL);
		return $result;
	}
	public function getBudgetCategoryDetail($itemId,$location,$departmentId)
	{
		$cols	 = "BC.BUDGET_TYPE,
					BC.ITEM_ID,
					MI.strName AS item";
		$join 	= " INNER JOIN mst_budget_category_list BC ON BC.ITEM_ID=finance_mst_pettycash_item.ITEM_ID
					INNER JOIN mst_item MI ON MI.intId = BC.ITEM_ID
				  	INNER JOIN mst_budget_category_list_department_wise BCD ON BC.SERIAL_ID = BCD.SERIAL_ID";
		$where	= " finance_mst_pettycash_item.ID = '$itemId' AND
					BC.LOCATION_ID = '$location' AND 
					BCD.DEPARTMENT_ID = '$departmentId'";
		
		$result	= $this->select($cols,$join,$where,NULL,NULL);
		//$row	= mysqli_fetch_array($result);
		
		return $result;
	}
	 
	//END }
}
?>
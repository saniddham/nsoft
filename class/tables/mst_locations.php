<?php
// THIS CLASS MANUALLY EDITED BY LAHIRU
class mst_locations{
 
	private $db;
	private $table= "mst_locations";
	private $finance_sysNo_table = "trn_financemodule_sysNo";
	
	//private property
	private $intId;
	private $strCode;
	private $strName;
	private $intPlant;
	private $intCompanyId;
	private $intProduction;
    private $RESTRICTIONS;
	private $strAddress;
	private $strStreet;
	private $strCity;
	private $strDistrict;
	private $strPhoneNo;
	private $strFaxNo;
	private $strEmail;
	private $strZip;
	private $strRemarks;
	private $BANK_ACCOUNT_ID;
	private $intStatus;
	private $intBlocked;
	private $HEAD_OFFICE_FLAG;
	private $intCreator;
	private $dtmCreateDate;
	private $intModifyer;
	private $dtmModifyDate;
	private $COMPULSORY_INVOICE_UPLOAD_IN_GRN_FLAG;
	private $BANK_BALANCE;
	private $BANK_BALANCE_DATE;
	private $PAYROLL_LOCATION;
	private $BUDGET_PO_VALIDATION_FLAG;
	private $SAMPLE_RECIPES_CHANGE_FOR_APPROVED_PO;
	private $RAISE_CUSTOMER_PO_WITHOUT_RECIPES;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('intId'=>'intId',
										'strCode'=>'strCode',
										'strName'=>'strName',
										'intPlant'=>'intPlant',
										'intCompanyId'=>'intCompanyId',
										'intProduction'=>'intProduction',
                                        'RESTRICTIONS'=>'RESTRICTIONS',
										'strAddress'=>'strAddress',
										'strStreet'=>'strStreet',
										'strCity'=>'strCity',
										'strDistrict'=>'strDistrict',
										'strPhoneNo'=>'strPhoneNo',
										'strFaxNo'=>'strFaxNo',
										'strEmail'=>'strEmail',
										'strZip'=>'strZip',
										'strRemarks'=>'strRemarks',
										'BANK_ACCOUNT_ID'=>'BANK_ACCOUNT_ID',
										'intStatus'=>'intStatus',
										'intBlocked'=>'intBlocked',
										'HEAD_OFFICE_FLAG'=>'HEAD_OFFICE_FLAG',
										'intCreator'=>'intCreator',
										'dtmCreateDate'=>'dtmCreateDate',
										'intModifyer'=>'intModifyer',
										'dtmModifyDate'=>'dtmModifyDate',
										'COMPULSORY_INVOICE_UPLOAD_IN_GRN_FLAG'=>'COMPULSORY_INVOICE_UPLOAD_IN_GRN_FLAG',
										'BANK_BALANCE'=>'BANK_BALANCE',
										'BANK_BALANCE_DATE'=>'BANK_BALANCE_DATE',
										'PAYROLL_LOCATION'=>'PAYROLL_LOCATION',
										'BUDGET_PO_VALIDATION_FLAG'=>'BUDGET_PO_VALIDATION_FLAG',
										'SAMPLE_RECIPES_CHANGE_FOR_APPROVED_PO'=>'SAMPLE_RECIPES_CHANGE_FOR_APPROVED_PO',
										'RAISE_CUSTOMER_PO_WITHOUT_RECIPES'=>'RAISE_CUSTOMER_PO_WITHOUT_RECIPES',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit );
		return $result;
	}

	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data);
		return $this->db->getResult();


	}

    function insertFinanceData($data)
    {
        //insert into the table
        $this->db->insert($this->finance_sysNo_table,$data);
        return $this->db->getResult();
    }

	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}

	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();
	}

	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}

	public function get_field_array(){
		return $this->field_array;
	}

	function commit($type='update')
	{
		//commit update
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{
			$data[$v] = $this->$v;
		}
		$where		= "intId = ".$this->intId."" ;
		$this->commitArray = array();

		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}

	//BEGIN - public functions for private properties {
	
	//retun intId
	function getintId()
	{
		$this->validate();
		return $this->intId;
	}
	
	//retun strCode
	function getstrCode()
	{
		$this->validate();
		return $this->strCode;
	}
	
	//retun strName
	function getstrName()
	{
		$this->validate();
		return $this->strName;
	}
	
	//retun intPlant
	function getintPlant()
	{
		$this->validate();
		return $this->intPlant;
	}
	
	//retun intCompanyId
	function getintCompanyId()
	{
		$this->validate();
		return $this->intCompanyId;
	}
	
	//retun intProduction
	function getintProduction()
	{
		$this->validate();
		return $this->intProduction;
	}

    function getRESTRICTIONS()
    {
        $this->validate();
        return $this->RESTRICTIONS;
    }
	//retun strAddress
	function getstrAddress()
	{
		$this->validate();
		return $this->strAddress;
	}
	
	//retun strStreet
	function getstrStreet()
	{
		$this->validate();
		return $this->strStreet;
	}
	
	//retun strCity
	function getstrCity()
	{
		$this->validate();
		return $this->strCity;
	}
	
	//retun strDistrict
	function getstrDistrict()
	{
		$this->validate();
		return $this->strDistrict;
	}
	
	//retun strPhoneNo
	function getstrPhoneNo()
	{
		$this->validate();
		return $this->strPhoneNo;
	}
	
	//retun strFaxNo
	function getstrFaxNo()
	{
		$this->validate();
		return $this->strFaxNo;
	}
	
	//retun strEmail
	function getstrEmail()
	{
		$this->validate();
		return $this->strEmail;
	}
	
	//retun strZip
	function getstrZip()
	{
		$this->validate();
		return $this->strZip;
	}
	
	//retun strRemarks
	function getstrRemarks()
	{
		$this->validate();
		return $this->strRemarks;
	}
	
	//retun BANK_ACCOUNT_ID
	function getBANK_ACCOUNT_ID()
	{
		$this->validate();
		return $this->BANK_ACCOUNT_ID;
	}
	
	//retun intStatus
	function getintStatus()
	{
		$this->validate();
		return $this->intStatus;
	}
	
	//retun intBlocked
	function getintBlocked()
	{
		$this->validate();
		return $this->intBlocked;
	}
	
	//retun HEAD_OFFICE_FLAG
	function getHEAD_OFFICE_FLAG()
	{
		$this->validate();
		return $this->HEAD_OFFICE_FLAG;
	}
	
	//retun intCreator
	function getintCreator()
	{
		$this->validate();
		return $this->intCreator;
	}
	
	//retun dtmCreateDate
	function getdtmCreateDate()
	{
		$this->validate();
		return $this->dtmCreateDate;
	}
	
	//retun intModifyer
	function getintModifyer()
	{
		$this->validate();
		return $this->intModifyer;
	}
	
	//retun dtmModifyDate
	function getdtmModifyDate()
	{
		$this->validate();
		return $this->dtmModifyDate;
	}
	
	//retun COMPULSORY_INVOICE_UPLOAD_IN_GRN_FLAG
	function getCOMPULSORY_INVOICE_UPLOAD_IN_GRN_FLAG()
	{
		$this->validate();
		return $this->COMPULSORY_INVOICE_UPLOAD_IN_GRN_FLAG;
	}
	
	//retun BANK_BALANCE
	function getBANK_BALANCE()
	{
		$this->validate();
		return $this->BANK_BALANCE;
	}
	
	//retun BANK_BALANCE_DATE
	function getBANK_BALANCE_DATE()
	{
		$this->validate();
		return $this->BANK_BALANCE_DATE;
	}
	
	//retun PAYROLL_LOCATION
	function getPAYROLL_LOCATION()
	{
		$this->validate();
		return $this->PAYROLL_LOCATION;
	}
	
	//retun BUDGET_PO_VALIDATION_FLAG
	function getBUDGET_PO_VALIDATION_FLAG()
	{
		$this->validate();
		return $this->BUDGET_PO_VALIDATION_FLAG;
	}
	
	//retun SAMPLE_RECIPES_CHANGE_FOR_APPROVED_PO
	function getSAMPLE_RECIPES_CHANGE_FOR_APPROVED_PO()
	{
		$this->validate();
		return $this->SAMPLE_RECIPES_CHANGE_FOR_APPROVED_PO;
	}
	
	//retun RAISE_CUSTOMER_PO_WITHOUT_RECIPES
	function getRAISE_CUSTOMER_PO_WITHOUT_RECIPES()
	{
		$this->validate();
		return $this->RAISE_CUSTOMER_PO_WITHOUT_RECIPES;
	}
	
	//END }

	//BEGIN - public function set {
	
	//set intId
	function setintId($intId)
	{
		array_push($this->commitArray,'intId');
		$this->intId = $intId;
	}
	
	//set strCode
	function setstrCode($strCode)
	{
		array_push($this->commitArray,'strCode');
		$this->strCode = $strCode;
	}
	
	//set strName
	function setstrName($strName)
	{
		array_push($this->commitArray,'strName');
		$this->strName = $strName;
	}
	
	//set intPlant
	function setintPlant($intPlant)
	{
		array_push($this->commitArray,'intPlant');
		$this->intPlant = $intPlant;
	}
	
	//set intCompanyId
	function setintCompanyId($intCompanyId)
	{
		array_push($this->commitArray,'intCompanyId');
		$this->intCompanyId = $intCompanyId;
	}
	
	//set intProduction
	function setintProduction($intProduction)
	{
		array_push($this->commitArray,'intProduction');
		$this->intProduction = $intProduction;
	}

    function setRESTRICTIONS($RESTRICTIONS)
    {
        array_push($this->commitArray,'RESTRICTIONS');
        $this->RESTRICTIONS = $RESTRICTIONS;
    }
	
	//set strAddress
	function setstrAddress($strAddress)
	{
		array_push($this->commitArray,'strAddress');
		$this->strAddress = $strAddress;
	}
	
	//set strStreet
	function setstrStreet($strStreet)
	{
		array_push($this->commitArray,'strStreet');
		$this->strStreet = $strStreet;
	}
	
	//set strCity
	function setstrCity($strCity)
	{
		array_push($this->commitArray,'strCity');
		$this->strCity = $strCity;
	}
	
	//set strDistrict
	function setstrDistrict($strDistrict)
	{
		array_push($this->commitArray,'strDistrict');
		$this->strDistrict = $strDistrict;
	}
	
	//set strPhoneNo
	function setstrPhoneNo($strPhoneNo)
	{
		array_push($this->commitArray,'strPhoneNo');
		$this->strPhoneNo = $strPhoneNo;
	}
	
	//set strFaxNo
	function setstrFaxNo($strFaxNo)
	{
		array_push($this->commitArray,'strFaxNo');
		$this->strFaxNo = $strFaxNo;
	}
	
	//set strEmail
	function setstrEmail($strEmail)
	{
		array_push($this->commitArray,'strEmail');
		$this->strEmail = $strEmail;
	}
	
	//set strZip
	function setstrZip($strZip)
	{
		array_push($this->commitArray,'strZip');
		$this->strZip = $strZip;
	}
	
	//set strRemarks
	function setstrRemarks($strRemarks)
	{
		array_push($this->commitArray,'strRemarks');
		$this->strRemarks = $strRemarks;
	}
	
	//set BANK_ACCOUNT_ID
	function setBANK_ACCOUNT_ID($BANK_ACCOUNT_ID)
	{
		array_push($this->commitArray,'BANK_ACCOUNT_ID');
		$this->BANK_ACCOUNT_ID = $BANK_ACCOUNT_ID;
	}
	
	//set intStatus
	function setintStatus($intStatus)
	{
		array_push($this->commitArray,'intStatus');
		$this->intStatus = $intStatus;
	}
	
	//set intBlocked
	function setintBlocked($intBlocked)
	{
		array_push($this->commitArray,'intBlocked');
		$this->intBlocked = $intBlocked;
	}
	
	//set HEAD_OFFICE_FLAG
	function setHEAD_OFFICE_FLAG($HEAD_OFFICE_FLAG)
	{
		array_push($this->commitArray,'HEAD_OFFICE_FLAG');
		$this->HEAD_OFFICE_FLAG = $HEAD_OFFICE_FLAG;
	}
	
	//set intCreator
	function setintCreator($intCreator)
	{
		array_push($this->commitArray,'intCreator');
		$this->intCreator = $intCreator;
	}
	
	//set dtmCreateDate
	function setdtmCreateDate($dtmCreateDate)
	{
		array_push($this->commitArray,'dtmCreateDate');
		$this->dtmCreateDate = $dtmCreateDate;
	}
	
	//set intModifyer
	function setintModifyer($intModifyer)
	{
		array_push($this->commitArray,'intModifyer');
		$this->intModifyer = $intModifyer;
	}
	
	//set dtmModifyDate
	function setdtmModifyDate($dtmModifyDate)
	{
		array_push($this->commitArray,'dtmModifyDate');
		$this->dtmModifyDate = $dtmModifyDate;
	}
	
	//set COMPULSORY_INVOICE_UPLOAD_IN_GRN_FLAG
	function setCOMPULSORY_INVOICE_UPLOAD_IN_GRN_FLAG($COMPULSORY_INVOICE_UPLOAD_IN_GRN_FLAG)
	{
		array_push($this->commitArray,'COMPULSORY_INVOICE_UPLOAD_IN_GRN_FLAG');
		$this->COMPULSORY_INVOICE_UPLOAD_IN_GRN_FLAG = $COMPULSORY_INVOICE_UPLOAD_IN_GRN_FLAG;
	}
	
	//set BANK_BALANCE
	function setBANK_BALANCE($BANK_BALANCE)
	{
		array_push($this->commitArray,'BANK_BALANCE');
		$this->BANK_BALANCE = $BANK_BALANCE;
	}
	
	//set BANK_BALANCE_DATE
	function setBANK_BALANCE_DATE($BANK_BALANCE_DATE)
	{
		array_push($this->commitArray,'BANK_BALANCE_DATE');
		$this->BANK_BALANCE_DATE = $BANK_BALANCE_DATE;
	}
	
	//set PAYROLL_LOCATION
	function setPAYROLL_LOCATION($PAYROLL_LOCATION)
	{
		array_push($this->commitArray,'PAYROLL_LOCATION');
		$this->PAYROLL_LOCATION = $PAYROLL_LOCATION;
	}
	
	//set BUDGET_PO_VALIDATION_FLAG
	function setBUDGET_PO_VALIDATION_FLAG($BUDGET_PO_VALIDATION_FLAG)
	{
		array_push($this->commitArray,'BUDGET_PO_VALIDATION_FLAG');
		$this->BUDGET_PO_VALIDATION_FLAG = $BUDGET_PO_VALIDATION_FLAG;
	}
	
	//set SAMPLE_RECIPES_CHANGE_FOR_APPROVED_PO
	function setSAMPLE_RECIPES_CHANGE_FOR_APPROVED_PO($SAMPLE_RECIPES_CHANGE_FOR_APPROVED_PO)
	{
		array_push($this->commitArray,'SAMPLE_RECIPES_CHANGE_FOR_APPROVED_PO');
		$this->SAMPLE_RECIPES_CHANGE_FOR_APPROVED_PO = $SAMPLE_RECIPES_CHANGE_FOR_APPROVED_PO;
	}
	
	//set RAISE_CUSTOMER_PO_WITHOUT_RECIPES
	function setRAISE_CUSTOMER_PO_WITHOUT_RECIPES($RAISE_CUSTOMER_PO_WITHOUT_RECIPES)
	{
		array_push($this->commitArray,'RAISE_CUSTOMER_PO_WITHOUT_RECIPES');
		$this->RAISE_CUSTOMER_PO_WITHOUT_RECIPES = $RAISE_CUSTOMER_PO_WITHOUT_RECIPES;
	}
	
	//END }

	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->intId=='')
			throw new exception("Set primary values first");
		else
			return true;
	}

	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}

	public function set($intId)
	{
		$cols	= "*";

		$join	= NULL;

		$where	= "intId='$intId'";

		$result = $this->select($cols,$join,$where);
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}

	//insert as parameters
	public function insertRec($intId,$strCode,$strName,$intPlant,$intCompanyId,$intProduction,$intPressing,$strAddress,$strStreet,$strCity,$strDistrict,$strPhoneNo,$strFaxNo,$strEmail,$strZip,$strRemarks,$BANK_ACCOUNT_ID,$intStatus,$intBlocked,$HEAD_OFFICE_FLAG,$intCreator,$dtmCreateDate,$intModifyer,$dtmModifyDate,$COMPULSORY_INVOICE_UPLOAD_IN_GRN_FLAG,$BANK_BALANCE,$BANK_BALANCE_DATE,$PAYROLL_LOCATION,$BUDGET_PO_VALIDATION_FLAG,$SAMPLE_RECIPES_CHANGE_FOR_APPROVED_PO,$RAISE_CUSTOMER_PO_WITHOUT_RECIPES){
		$data = array('intId'=>$intId 
				,'strCode'=>$strCode 
				,'strName'=>$strName 
				,'intPlant'=>$intPlant 
				,'intCompanyId'=>$intCompanyId 
				,'intProduction'=>$intProduction
                ,'RESTRICTIONS'=>$intPressing
				,'strAddress'=>$strAddress
				,'strStreet'=>$strStreet 
				,'strCity'=>$strCity 
				,'strDistrict'=>$strDistrict 
				,'strPhoneNo'=>$strPhoneNo 
				,'strFaxNo'=>$strFaxNo 
				,'strEmail'=>$strEmail 
				,'strZip'=>$strZip 
				,'strRemarks'=>$strRemarks 
				,'BANK_ACCOUNT_ID'=>$BANK_ACCOUNT_ID 
				,'intStatus'=>$intStatus 
				,'intBlocked'=>$intBlocked 
				,'HEAD_OFFICE_FLAG'=>$HEAD_OFFICE_FLAG 
				,'intCreator'=>$intCreator 
				,'dtmCreateDate'=>$dtmCreateDate 
				,'intModifyer'=>$intModifyer 
				,'dtmModifyDate'=>$dtmModifyDate 
				,'COMPULSORY_INVOICE_UPLOAD_IN_GRN_FLAG'=>$COMPULSORY_INVOICE_UPLOAD_IN_GRN_FLAG 
				,'BANK_BALANCE'=>$BANK_BALANCE 
				,'BANK_BALANCE_DATE'=>$BANK_BALANCE_DATE 
				,'PAYROLL_LOCATION'=>$PAYROLL_LOCATION 
				,'BUDGET_PO_VALIDATION_FLAG'=>$BUDGET_PO_VALIDATION_FLAG 
				,'SAMPLE_RECIPES_CHANGE_FOR_APPROVED_PO'=>$SAMPLE_RECIPES_CHANGE_FOR_APPROVED_PO 
				,'RAISE_CUSTOMER_PO_WITHOUT_RECIPES'=>$RAISE_CUSTOMER_PO_WITHOUT_RECIPES 
				);
		return $this->insert($data);
	}

	public function insertFinanceModuleSysNo($locationId){
	    $data = array('COMPANY_ID'=> $locationId);
	    return $this->insertFinanceData($data);

    }
	public function setCodeToGetData($code){
	
		$result = $this->select('*',null,"strCode='$code'");	
		if($this->db->numRows()>0)
			$this->setVariables($result);
		else
			throw new Exception("Record not found.");
		
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('intId,strName',  null, $where = $where);
		$html = '<option value=""></option>';
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intId'])
				$html .= '<option selected="selected" value="'.$row['intId'].'">'.$row['strName'].'</option>';
			else
				$html .= '<option value="'.$row['intId'].'">'.$row['strName'].'</option>';
		}
		return $html;
	}
	public function getGroup()
	{
		global $sessions;
		
		$cols	= " QMG.intGroupId,QMG.strGroupName";
	
		$join	= " INNER JOIN ".$sessions->getHrDatabase().".mst_locations QML ON mst_locations.PAYROLL_LOCATION=QML.intId
					INNER JOIN ".$sessions->getHrDatabase().".mst_companies QMC ON QML.intCompanyId=QMC.intId
					INNER JOIN ".$sessions->getHrDatabase().".mst_groups QMG ON QMG.intCompanyId=QMC.intId ";
		
		$where	= " mst_locations.intCompanyId = '".$sessions->getCompanyId()."' AND QMG.intStatus = 1 AND QMG.intGroupType = 1 ";
		
		$order	= " QMG.strGroupName ";
		
		$result = $this->select($cols,$join,$where,$order,$limit=null);

		return $result;
	}
	
	public function getLocations($company)
	{
 		$cols		= "intId";
		
 		$where		= "intStatus = 1 
					   AND intCompanyId = $company";
		
		$result 	= $this->select($cols,$join = null,$where, $order = null, $limit = null);	
 		return $result;
	}
	
	public function getCompanyReportHeaderData($locationId)
	{
		$cols	= " C.strName								AS COMPANY_NAME,
					mst_locations.strName 					AS LOCATION_NAME,
					mst_locations.strAddress				AS LOCATION_ADDRESS,
					mst_locations.strStreet					AS LOCATION_STREET,
					mst_locations.strCity					AS LOCATION_CITY,
					CO.strCountryName						AS COMPANY_COUNTRY,		
					mst_locations.strPhoneNo				AS LOCATION_PHONE,
					mst_locations.strFaxNo					AS LOCATION_FAX,
					mst_locations.strEmail					AS LOCATION_EMAIL,
					C.strWebSite							AS LOCATION_WEB,
					mst_locations.strZip					AS LOCATION_ZIP,
					C.strVatNo								AS COMPANY_VAT,
					C.strSVatNo								AS COMPANY_SVAT,
					C.intBaseCurrencyId						AS BASE_CURRENCY_ID,
					CU.strCode								AS BASE_CURRENCY_CODE,
					CU.strSymbol							AS BASE_CURRENCY_SYMBOL ";
		
		$join	= " Inner Join mst_companies C ON mst_locations.intCompanyId = C.intId
					Inner Join mst_country CO ON C.intCountryId = CO.intCountryID
					INNER JOIN mst_financecurrency CU ON CU.intId = C.intBaseCurrencyId ";
		
		$where	= " mst_locations.intId =  '".$locationId."' ";
		
		$result = $this->select($cols,$join,$where);
		$row	= mysqli_fetch_array($result);
		
		return $row;
	}

    public function getFinanceSysNo($locationId)
    {
        $cols		= "FMS.COMPANYID";

        $join       = "Inner Join trn_financemodule_sysNo FMS ON mst_locations.intId = FMS.COMPANYID";

        $where		= "intStatus = 1 AND
					   mst_locations.intId =  '".$locationId."' ";

        $result 	= $this->select($cols,$join,$where, $order = null, $limit = null);
        $row	= mysqli_fetch_array($result);
        return $row['COMPANY_ID'];
    }
	//END }
}
?>
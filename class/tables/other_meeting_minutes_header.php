<?php
class other_meeting_minutes_header{
 
	private $db;
	private $table= "other_meeting_minutes_header";
	
	//private property
	private $MINUTE_ID;
	private $SUBJECT;
	private $MEETING_PLACE;
	private $CALLED_BY_LIST;
	private $ATTENDEES_LIST;
	private $DISTRIBUTION_LIST;
	private $OTHER_DISTRIBUTION_EMAILS;
	private $DATE;
	private $CREATED_BY;
	private $CREATED_DATE;
	private $STATUS;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('MINUTE_ID'=>'MINUTE_ID',
										'SUBJECT'=>'SUBJECT',
										'MEETING_PLACE'=>'MEETING_PLACE',
										'CALLED_BY_LIST'=>'CALLED_BY_LIST',
										'ATTENDEES_LIST'=>'ATTENDEES_LIST',
										'DISTRIBUTION_LIST'=>'DISTRIBUTION_LIST',
										'OTHER_DISTRIBUTION_EMAILS'=>'OTHER_DISTRIBUTION_EMAILS',
										'DATE'=>'DATE',
										'CREATED_BY'=>'CREATED_BY',
										'CREATED_DATE'=>'CREATED_DATE',
										'STATUS'=>'STATUS',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	function commit()
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "MINUTE_ID = ".$this->MINUTE_ID."" ;
		unset($this->commitArray);
		
		if($this->intId==NULL || $this->intId=='' || $this->intId=='NULL')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun MINUTE_ID
	function getMINUTE_ID()
	{
		$this->validate();
		return $this->MINUTE_ID;
	}
	
	//retun SUBJECT
	function getSUBJECT()
	{
		$this->validate();
		return $this->SUBJECT;
	}
	
	//retun MEETING_PLACE
	function getMEETING_PLACE()
	{
		$this->validate();
		return $this->MEETING_PLACE;
	}
	
	//retun CALLED_BY_LIST
	function getCALLED_BY_LIST()
	{
		$this->validate();
		return $this->CALLED_BY_LIST;
	}
	
	//retun ATTENDEES_LIST
	function getATTENDEES_LIST()
	{
		$this->validate();
		return $this->ATTENDEES_LIST;
	}
	
	//retun DISTRIBUTION_LIST
	function getDISTRIBUTION_LIST()
	{
		$this->validate();
		return $this->DISTRIBUTION_LIST;
	}
	
	//retun OTHER_DISTRIBUTION_EMAILS
	function getOTHER_DISTRIBUTION_EMAILS()
	{
		$this->validate();
		return $this->OTHER_DISTRIBUTION_EMAILS;
	}
	
	//retun DATE
	function getDATE()
	{
		$this->validate();
		return $this->DATE;
	}
	
	//retun CREATED_BY
	function getCREATED_BY()
	{
		$this->validate();
		return $this->CREATED_BY;
	}
	
	//retun CREATED_DATE
	function getCREATED_DATE()
	{
		$this->validate();
		return $this->CREATED_DATE;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set MINUTE_ID
	function setMINUTE_ID($MINUTE_ID)
	{
		array_push($this->commitArray,'MINUTE_ID');
		$this->MINUTE_ID = $MINUTE_ID;
	}
	
	//set SUBJECT
	function setSUBJECT($SUBJECT)
	{
		array_push($this->commitArray,'SUBJECT');
		$this->SUBJECT = $SUBJECT;
	}
	
	//set MEETING_PLACE
	function setMEETING_PLACE($MEETING_PLACE)
	{
		array_push($this->commitArray,'MEETING_PLACE');
		$this->MEETING_PLACE = $MEETING_PLACE;
	}
	
	//set CALLED_BY_LIST
	function setCALLED_BY_LIST($CALLED_BY_LIST)
	{
		array_push($this->commitArray,'CALLED_BY_LIST');
		$this->CALLED_BY_LIST = $CALLED_BY_LIST;
	}
	
	//set ATTENDEES_LIST
	function setATTENDEES_LIST($ATTENDEES_LIST)
	{
		array_push($this->commitArray,'ATTENDEES_LIST');
		$this->ATTENDEES_LIST = $ATTENDEES_LIST;
	}
	
	//set DISTRIBUTION_LIST
	function setDISTRIBUTION_LIST($DISTRIBUTION_LIST)
	{
		array_push($this->commitArray,'DISTRIBUTION_LIST');
		$this->DISTRIBUTION_LIST = $DISTRIBUTION_LIST;
	}
	
	//set OTHER_DISTRIBUTION_EMAILS
	function setOTHER_DISTRIBUTION_EMAILS($OTHER_DISTRIBUTION_EMAILS)
	{
		array_push($this->commitArray,'OTHER_DISTRIBUTION_EMAILS');
		$this->OTHER_DISTRIBUTION_EMAILS = $OTHER_DISTRIBUTION_EMAILS;
	}
	
	//set DATE
	function setDATE($DATE)
	{
		array_push($this->commitArray,'DATE');
		$this->DATE = $DATE;
	}
	
	//set CREATED_BY
	function setCREATED_BY($CREATED_BY)
	{
		array_push($this->commitArray,'CREATED_BY');
		$this->CREATED_BY = $CREATED_BY;
	}
	
	//set CREATED_DATE
	function setCREATED_DATE($CREATED_DATE)
	{
		array_push($this->commitArray,'CREATED_DATE');
		$this->CREATED_DATE = $CREATED_DATE;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->MINUTE_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($MINUTE_ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "MINUTE_ID='$MINUTE_ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
		//else
			//throw new Exception("Record not found.");
	}	
	
	
	//END }
}
?>
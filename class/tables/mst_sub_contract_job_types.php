
<?php

class mst_sub_contract_job_types{
 
	private $db;
	private $table= "mst_sub_contract_job_types";
	
	//private property
	private $ID;
	private $NAME;
	private $STATUS;
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->db->field_array_set(array('ID'=>'ID',
										'NAME'=>'NAME',
										'STATUS'=>'STATUS',
										));
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		return $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  
		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	//public functions for private properties
	
	//retun ID
	function getID()
	{
		validate();
		return $this->ID;
	}
	
	//retun NAME
	function getNAME()
	{
		validate();
		return $this->NAME;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		validate();
		return $this->STATUS;
	}
	
	//validate primary values
	private function validate()
	{
		if($this->ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	
}
?>

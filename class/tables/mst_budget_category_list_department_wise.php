<?php
class mst_budget_category_list_department_wise{
 
	private $db;
	private $table= "mst_budget_category_list_department_wise";
	
	//private property
	private $DEPARTMENT_ID;
	private $SERIAL_ID;
	private $commitArray = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->db->field_array_set(array('DEPARTMENT_ID'=>'DEPARTMENT_ID',
										'SERIAL_ID'=>'SERIAL_ID',
										));
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function commit()
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = ''.$this->$v.'';
		}
		$where		= "DEPARTMENT_ID = '".$this->DEPARTMENT_ID."' and SERIAL_ID = '".$this->SERIAL_ID."'" ;
		unset($this->commitArray);
		return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun DEPARTMENT_ID
	function getDEPARTMENT_ID()
	{
		$this->validate();
		return $this->DEPARTMENT_ID;
	}
	
	//retun SERIAL_ID
	function getSERIAL_ID()
	{
		$this->validate();
		return $this->SERIAL_ID;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set DEPARTMENT_ID
	function setDEPARTMENT_ID($DEPARTMENT_ID)
	{
		array_push($this->commitArray,'DEPARTMENT_ID');
		$this->DEPARTMENT_ID = DEPARTMENT_ID;
	}
	
	//set SERIAL_ID
	function setSERIAL_ID($SERIAL_ID)
	{
		array_push($this->commitArray,'SERIAL_ID');
		$this->SERIAL_ID = SERIAL_ID;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->DEPARTMENT_ID=='' || $this->SERIAL_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
		}
	}
	
	public function set($DEPARTMENT_ID , $SERIAL_ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "DEPARTMENT_ID='$DEPARTMENT_ID' and SERIAL_ID='$SERIAL_ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			$this->setVariables($result);
		else
			throw new Exception("Record not found.");
	}	
	
	
	//END }
}
?>
<?php
//Manually Functions Added 

class ware_stocktransactions{
	private $db;
	private $table= "ware_stocktransactions";
	
	//private property
	private $id;
	private $intCompanyId;
	private $intLocationId;
	private $intSubStores;
	private $intDocumentNo;
	private $intDocumntYear;
	private $intGRNNo;
	private $intGRNYear;
	private $dtGRNDate;
	private $intCurrencyId;
	private $dblGRNRate;
	private $intOrderNo;
	private $intOrderYear;
	private $intSalesOrderId;
	private $strStyleNo;
	private $intItemId;
	private $dblQty;
	private $strType;
	private $intUser;
	private $dtDate;
	private $PROGRAM;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('id'=>'id',
										'intCompanyId'=>'intCompanyId',
										'intLocationId'=>'intLocationId',
										'intSubStores'=>'intSubStores',
										'intDocumentNo'=>'intDocumentNo',
										'intDocumntYear'=>'intDocumntYear',
										'intGRNNo'=>'intGRNNo',
										'intGRNYear'=>'intGRNYear',
										'dtGRNDate'=>'dtGRNDate',
										'intCurrencyId'=>'intCurrencyId',
										'dblGRNRate'=>'dblGRNRate',
										'intOrderNo'=>'intOrderNo',
										'intOrderYear'=>'intOrderYear',
										'intSalesOrderId'=>'intSalesOrderId',
										'strStyleNo'=>'strStyleNo',
										'intItemId'=>'intItemId',
										'dblQty'=>'dblQty',
										'strType'=>'strType',
										'intUser'=>'intUser',
										'dtDate'=>'dtDate',
										'PROGRAM'=>'PROGRAM',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	function commit()
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "id = ".$this->id."" ;
		unset($this->commitArray);
		
		if($this->intId==NULL || $this->intId=='' || $this->intId=='NULL')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun id
	function getid()
	{
		$this->validate();
		return $this->id;
	}
	
	//retun intCompanyId
	function getintCompanyId()
	{
		$this->validate();
		return $this->intCompanyId;
	}
	
	//retun intLocationId
	function getintLocationId()
	{
		$this->validate();
		return $this->intLocationId;
	}
	
	//retun intSubStores
	function getintSubStores()
	{
		$this->validate();
		return $this->intSubStores;
	}
	
	//retun intDocumentNo
	function getintDocumentNo()
	{
		$this->validate();
		return $this->intDocumentNo;
	}
	
	//retun intDocumntYear
	function getintDocumntYear()
	{
		$this->validate();
		return $this->intDocumntYear;
	}
	
	//retun intGRNNo
	function getintGRNNo()
	{
		$this->validate();
		return $this->intGRNNo;
	}
	
	//retun intGRNYear
	function getintGRNYear()
	{
		$this->validate();
		return $this->intGRNYear;
	}
	
	//retun dtGRNDate
	function getdtGRNDate()
	{
		$this->validate();
		return $this->dtGRNDate;
	}
	
	//retun intCurrencyId
	function getintCurrencyId()
	{
		$this->validate();
		return $this->intCurrencyId;
	}
	
	//retun dblGRNRate
	function getdblGRNRate()
	{
		$this->validate();
		return $this->dblGRNRate;
	}
	
	//retun intOrderNo
	function getintOrderNo()
	{
		$this->validate();
		return $this->intOrderNo;
	}
	
	//retun intOrderYear
	function getintOrderYear()
	{
		$this->validate();
		return $this->intOrderYear;
	}
	
	//retun intSalesOrderId
	function getintSalesOrderId()
	{
		$this->validate();
		return $this->intSalesOrderId;
	}
	
	//retun strStyleNo
	function getstrStyleNo()
	{
		$this->validate();
		return $this->strStyleNo;
	}
	
	//retun intItemId
	function getintItemId()
	{
		$this->validate();
		return $this->intItemId;
	}
	
	//retun dblQty
	function getdblQty()
	{
		$this->validate();
		return $this->dblQty;
	}
	
	//retun strType
	function getstrType()
	{
		$this->validate();
		return $this->strType;
	}
	
	//retun intUser
	function getintUser()
	{
		$this->validate();
		return $this->intUser;
	}
	
	//retun dtDate
	function getdtDate()
	{
		$this->validate();
		return $this->dtDate;
	}
	
	//retun PROGRAM
	function getPROGRAM()
	{
		$this->validate();
		return $this->PROGRAM;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set id
	function setid($id)
	{
		array_push($this->commitArray,'id');
		$this->id = $id;
	}
	
	//set intCompanyId
	function setintCompanyId($intCompanyId)
	{
		array_push($this->commitArray,'intCompanyId');
		$this->intCompanyId = $intCompanyId;
	}
	
	//set intLocationId
	function setintLocationId($intLocationId)
	{
		array_push($this->commitArray,'intLocationId');
		$this->intLocationId = $intLocationId;
	}
	
	//set intSubStores
	function setintSubStores($intSubStores)
	{
		array_push($this->commitArray,'intSubStores');
		$this->intSubStores = $intSubStores;
	}
	
	//set intDocumentNo
	function setintDocumentNo($intDocumentNo)
	{
		array_push($this->commitArray,'intDocumentNo');
		$this->intDocumentNo = $intDocumentNo;
	}
	
	//set intDocumntYear
	function setintDocumntYear($intDocumntYear)
	{
		array_push($this->commitArray,'intDocumntYear');
		$this->intDocumntYear = $intDocumntYear;
	}
	
	//set intGRNNo
	function setintGRNNo($intGRNNo)
	{
		array_push($this->commitArray,'intGRNNo');
		$this->intGRNNo = $intGRNNo;
	}
	
	//set intGRNYear
	function setintGRNYear($intGRNYear)
	{
		array_push($this->commitArray,'intGRNYear');
		$this->intGRNYear = $intGRNYear;
	}
	
	//set dtGRNDate
	function setdtGRNDate($dtGRNDate)
	{
		array_push($this->commitArray,'dtGRNDate');
		$this->dtGRNDate = $dtGRNDate;
	}
	
	//set intCurrencyId
	function setintCurrencyId($intCurrencyId)
	{
		array_push($this->commitArray,'intCurrencyId');
		$this->intCurrencyId = $intCurrencyId;
	}
	
	//set dblGRNRate
	function setdblGRNRate($dblGRNRate)
	{
		array_push($this->commitArray,'dblGRNRate');
		$this->dblGRNRate = $dblGRNRate;
	}
	
	//set intOrderNo
	function setintOrderNo($intOrderNo)
	{
		array_push($this->commitArray,'intOrderNo');
		$this->intOrderNo = $intOrderNo;
	}
	
	//set intOrderYear
	function setintOrderYear($intOrderYear)
	{
		array_push($this->commitArray,'intOrderYear');
		$this->intOrderYear = $intOrderYear;
	}
	
	//set intSalesOrderId
	function setintSalesOrderId($intSalesOrderId)
	{
		array_push($this->commitArray,'intSalesOrderId');
		$this->intSalesOrderId = $intSalesOrderId;
	}
	
	//set strStyleNo
	function setstrStyleNo($strStyleNo)
	{
		array_push($this->commitArray,'strStyleNo');
		$this->strStyleNo = $strStyleNo;
	}
	
	//set intItemId
	function setintItemId($intItemId)
	{
		array_push($this->commitArray,'intItemId');
		$this->intItemId = $intItemId;
	}
	
	//set dblQty
	function setdblQty($dblQty)
	{
		array_push($this->commitArray,'dblQty');
		$this->dblQty = $dblQty;
	}
	
	//set strType
	function setstrType($strType)
	{
		array_push($this->commitArray,'strType');
		$this->strType = $strType;
	}
	
	//set intUser
	function setintUser($intUser)
	{
		array_push($this->commitArray,'intUser');
		$this->intUser = $intUser;
	}
	
	//set dtDate
	function setdtDate($dtDate)
	{
		array_push($this->commitArray,'dtDate');
		$this->dtDate = $dtDate;
	}
	
	//set PROGRAM
	function setPROGRAM($PROGRAM)
	{
		array_push($this->commitArray,'PROGRAM');
		$this->PROGRAM = $PROGRAM;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->id=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($id)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "id='$id'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($intCompanyId,$intLocationId,$intSubStores,$intDocumentNo,$intDocumntYear,$intGRNNo,$intGRNYear,$dtGRNDate,$intCurrencyId,$dblGRNRate,$intOrderNo,$intOrderYear,$intSalesOrderId,$strStyleNo,$intItemId,$dblQty,$strType,$intUser,$dtDate,$PROGRAM){
		$data = array(
				'intCompanyId'=>$intCompanyId 
				,'intLocationId'=>$intLocationId 
				,'intSubStores'=>$intSubStores 
				,'intDocumentNo'=>$intDocumentNo 
				,'intDocumntYear'=>$intDocumntYear 
				,'intGRNNo'=>$intGRNNo 
				,'intGRNYear'=>$intGRNYear 
				,'dtGRNDate'=>$dtGRNDate 
				,'intCurrencyId'=>$intCurrencyId 
				,'dblGRNRate'=>$dblGRNRate 
				,'intOrderNo'=>$intOrderNo 
				,'intOrderYear'=>$intOrderYear 
				,'intSalesOrderId'=>$intSalesOrderId 
				,'strStyleNo'=>$strStyleNo 
				,'intItemId'=>$intItemId 
				,'dblQty'=>$dblQty 
				,'strType'=>$strType 
				,'intUser'=>$intUser 
				,'dtDate'=>$dtDate 
				,'PROGRAM'=>$PROGRAM 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('id,intCompanyId',  null, $where = $where);
		while($row=mysqli_fetch_array($result)){
			$html .= '<option value="'.$row['id'].'">'.$row['intCompanyId'].'</option>';	
		}
		return $html;
	}
	
	public function getMainStoresOrderWiseItemBalance($location,$orderNo,$orderYear,$salesOrderId,$item,$deci){
		
		$cols		= " COALESCE(ROUND(SUM(dblQty),$deci+2),0) AS stock_bal ";
		
		$where		= " intItemId = '".$item."' AND
						intOrderNo = '".$orderNo."' AND
						intOrderYear = '".$orderYear."' AND
						intSalesOrderId = '".$salesOrderId."' ";
		if($location!=''){
		$where		.= " AND
						intLocationId = '".$location."' ";
		}
		
		$result 	= $this->select($cols,$join = null,$where, $order = null, $limit = null);	
		$row		= mysqli_fetch_array($result);
		return 		$row['stock_bal'];
	}
		
	public function getGrnWiseStyleStockBal_result($orderNo,$orderYear,$salesOrder,$itemId,$location,$deci)
	{
		$select_stock	= " ware_stocktransactions.intGRNNo,
							ware_stocktransactions.intGRNYear,
							ware_stocktransactions.dtGRNDate,
							ware_stocktransactions.dblGRNRate,
							ware_stocktransactions.intCurrencyId,
							COALESCE(ROUND(SUM(ware_stocktransactions.dblQty),".($deci+2)."),0) AS stockBal";
									
		$where_stock	= " ware_stocktransactions.intOrderNo = '$orderNo' AND
							ware_stocktransactions.intOrderYear = '$orderYear' AND
							ware_stocktransactions.intSalesOrderId = '$salesOrder' AND
							ware_stocktransactions.intItemId = '$itemId' AND
							ware_stocktransactions.intLocationId = '$location'
							
							GROUP BY
							ware_stocktransactions.intItemId,
							ware_stocktransactions.intGRNYear,
							ware_stocktransactions.dtGRNDate,
							ware_stocktransactions.dblGRNRate 
							HAVING stockBal > 0";
				
		$stockResult	= $this->select($select_stock,NULL,$where_stock,'ware_stocktransactions.dtGRNDate ASC',NULL);	
		return $stockResult;
	}
	
	public function getItemAvaragePrice($orderNo,$orderYear,$salesOrderId,$itemId,$convertCurrency,$convertDate,$deci)
	{
		$cols		= "ROUND(SUM((dblGRNRate * FE.dblBuying)/(SELECT FE1.dblBuying 
																FROM mst_financeexchangerate FE1 
																INNER JOIN mst_financecurrency 
																	ON FE1.intCurrencyId = mst_financecurrency.intId 
																WHERE FE1.intCurrencyId = '$convertCurrency' 
																	AND FE1.intCompanyId = ware_stocktransactions.intCompanyId 
																	AND FE1.dtmDate = '$convertDate') * dblQty)/SUM(dblQty),$deci+4) AS AVG_ITEM_PRICE ";
						
		$join		= " LEFT JOIN mst_financeexchangerate FE
						ON ware_stocktransactions.intCurrencyId = FE.intCurrencyId
						  AND ware_stocktransactions.dtGRNDate = FE.dtmDate
						  AND ware_stocktransactions.intCompanyId = FE.intCompanyId";
	  
		$where		= "intOrderNo = '$orderNo'
						AND intOrderYear = '$orderYear'
						AND intSalesOrderId = $salesOrderId
						AND intItemId = '$itemId'
						/*AND strType IN ('ISSUE','RETSTORES')*/";
		//echo ("SELECT $cols FROM ware_stocktransactions $join WHERE $where");
		$resultq = $this->select($cols,$join,$where,$order=null,$limit=null);
		$rowq	= mysqli_fetch_array($resultq);
		return $rowq['AVG_ITEM_PRICE'];
	}
	
	public function getItemActualCost($orderNo,$orderYear,$salesOrderId,$itemId,$convertCurrency,$convertDate,$deci)
	{
		$cols		= "ROUND(SUM((dblGRNRate * FE.dblBuying)/(SELECT FE1.dblBuying 
																FROM mst_financeexchangerate FE1 
																INNER JOIN mst_financecurrency 
																	ON FE1.intCurrencyId = mst_financecurrency.intId 
																WHERE FE1.intCurrencyId = '$convertCurrency' 
																	AND FE1.intCompanyId = ware_stocktransactions.intCompanyId 
																	AND FE1.dtmDate = '$convertDate') * dblQty),$deci+4) AS ACTUAL_COST ";
						
		$join		= " LEFT JOIN mst_financeexchangerate FE
						ON ware_stocktransactions.intCurrencyId = FE.intCurrencyId
						  AND ware_stocktransactions.dtGRNDate = FE.dtmDate
						  AND ware_stocktransactions.intCompanyId = FE.intCompanyId";
	  
		$where		= "intOrderNo = '$orderNo'
						AND intOrderYear = '$orderYear'
						AND intSalesOrderId = $salesOrderId
						AND intItemId = '$itemId'
						AND strType IN ('ISSUE','RETSTORES')";
						//echo ("SELECT $cols FROM ware_stocktransactions $join WHERE $where");
		$resultq = $this->select($cols,$join,$where,$order=null,$limit=null);
		$rowq	= mysqli_fetch_array($resultq);
		return $rowq['ACTUAL_COST']*-1;
	}
}
?>
<?php
class ink_day_end_process_color_room_weight{
 
	private $db;
	private $table= "ink_day_end_process_color_room_weight";
	
	//private property
	private $PROCESS_ID;
	private $ORDER_NO;
	private $ORDER_YEAR;
	private $SALES_ORDER_ID;
	private $INK_RM_ITEM;
	private $COLOR_ID;
	private $TECHNIQUE_ID;
	private $INK_TYPE_ID;
	private $INK_RM_ITEM_WEIGHT;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('PROCESS_ID'=>'PROCESS_ID',
										'ORDER_NO'=>'ORDER_NO',
										'ORDER_YEAR'=>'ORDER_YEAR',
										'SALES_ORDER_ID'=>'SALES_ORDER_ID',
										'INK_RM_ITEM'=>'INK_RM_ITEM',
										'COLOR_ID'=>'COLOR_ID',
										'TECHNIQUE_ID'=>'TECHNIQUE_ID',
										'INK_TYPE_ID'=>'INK_TYPE_ID',
										'INK_RM_ITEM_WEIGHT'=>'INK_RM_ITEM_WEIGHT',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "PROCESS_ID = ".$this->PROCESS_ID." and ORDER_NO = ".$this->ORDER_NO." and ORDER_YEAR = ".$this->ORDER_YEAR." and SALES_ORDER_ID = ".$this->SALES_ORDER_ID." and INK_RM_ITEM = ".$this->INK_RM_ITEM." and COLOR_ID = ".$this->COLOR_ID." and TECHNIQUE_ID = ".$this->TECHNIQUE_ID." and INK_TYPE_ID = ".$this->INK_TYPE_ID."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun PROCESS_ID
	function getPROCESS_ID()
	{
		$this->validate();
		return $this->PROCESS_ID;
	}
	
	//retun ORDER_NO
	function getORDER_NO()
	{
		$this->validate();
		return $this->ORDER_NO;
	}
	
	//retun ORDER_YEAR
	function getORDER_YEAR()
	{
		$this->validate();
		return $this->ORDER_YEAR;
	}
	
	//retun SALES_ORDER_ID
	function getSALES_ORDER_ID()
	{
		$this->validate();
		return $this->SALES_ORDER_ID;
	}
	
	//retun INK_RM_ITEM
	function getINK_RM_ITEM()
	{
		$this->validate();
		return $this->INK_RM_ITEM;
	}
	
	//retun COLOR_ID
	function getCOLOR_ID()
	{
		$this->validate();
		return $this->COLOR_ID;
	}
	
	//retun TECHNIQUE_ID
	function getTECHNIQUE_ID()
	{
		$this->validate();
		return $this->TECHNIQUE_ID;
	}
	
	//retun INK_TYPE_ID
	function getINK_TYPE_ID()
	{
		$this->validate();
		return $this->INK_TYPE_ID;
	}
	
	//retun INK_RM_ITEM_WEIGHT
	function getINK_RM_ITEM_WEIGHT()
	{
		$this->validate();
		return $this->INK_RM_ITEM_WEIGHT;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set PROCESS_ID
	function setPROCESS_ID($PROCESS_ID)
	{
		array_push($this->commitArray,'PROCESS_ID');
		$this->PROCESS_ID = $PROCESS_ID;
	}
	
	//set ORDER_NO
	function setORDER_NO($ORDER_NO)
	{
		array_push($this->commitArray,'ORDER_NO');
		$this->ORDER_NO = $ORDER_NO;
	}
	
	//set ORDER_YEAR
	function setORDER_YEAR($ORDER_YEAR)
	{
		array_push($this->commitArray,'ORDER_YEAR');
		$this->ORDER_YEAR = $ORDER_YEAR;
	}
	
	//set SALES_ORDER_ID
	function setSALES_ORDER_ID($SALES_ORDER_ID)
	{
		array_push($this->commitArray,'SALES_ORDER_ID');
		$this->SALES_ORDER_ID = $SALES_ORDER_ID;
	}
	
	//set INK_RM_ITEM
	function setINK_RM_ITEM($INK_RM_ITEM)
	{
		array_push($this->commitArray,'INK_RM_ITEM');
		$this->INK_RM_ITEM = $INK_RM_ITEM;
	}
	
	//set COLOR_ID
	function setCOLOR_ID($COLOR_ID)
	{
		array_push($this->commitArray,'COLOR_ID');
		$this->COLOR_ID = $COLOR_ID;
	}
	
	//set TECHNIQUE_ID
	function setTECHNIQUE_ID($TECHNIQUE_ID)
	{
		array_push($this->commitArray,'TECHNIQUE_ID');
		$this->TECHNIQUE_ID = $TECHNIQUE_ID;
	}
	
	//set INK_TYPE_ID
	function setINK_TYPE_ID($INK_TYPE_ID)
	{
		array_push($this->commitArray,'INK_TYPE_ID');
		$this->INK_TYPE_ID = $INK_TYPE_ID;
	}
	
	//set INK_RM_ITEM_WEIGHT
	function setINK_RM_ITEM_WEIGHT($INK_RM_ITEM_WEIGHT)
	{
		array_push($this->commitArray,'INK_RM_ITEM_WEIGHT');
		$this->INK_RM_ITEM_WEIGHT = $INK_RM_ITEM_WEIGHT;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->PROCESS_ID=='' || $this->ORDER_NO=='' || $this->ORDER_YEAR=='' || $this->SALES_ORDER_ID=='' || $this->INK_RM_ITEM=='' || $this->COLOR_ID=='' || $this->TECHNIQUE_ID=='' || $this->INK_TYPE_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($PROCESS_ID , $ORDER_NO , $ORDER_YEAR , $SALES_ORDER_ID , $INK_RM_ITEM , $COLOR_ID , $TECHNIQUE_ID , $INK_TYPE_ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "PROCESS_ID='$PROCESS_ID' and ORDER_NO='$ORDER_NO' and ORDER_YEAR='$ORDER_YEAR' and SALES_ORDER_ID='$SALES_ORDER_ID' and INK_RM_ITEM='$INK_RM_ITEM' and COLOR_ID='$COLOR_ID' and TECHNIQUE_ID='$TECHNIQUE_ID' and INK_TYPE_ID='$INK_TYPE_ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($PROCESS_ID,$ORDER_NO,$ORDER_YEAR,$SALES_ORDER_ID,$INK_RM_ITEM,$COLOR_ID,$TECHNIQUE_ID,$INK_TYPE_ID,$INK_RM_ITEM_WEIGHT){
		$data = array('PROCESS_ID'=>$PROCESS_ID 
				,'ORDER_NO'=>$ORDER_NO 
				,'ORDER_YEAR'=>$ORDER_YEAR 
				,'SALES_ORDER_ID'=>$SALES_ORDER_ID 
				,'INK_RM_ITEM'=>$INK_RM_ITEM 
				,'COLOR_ID'=>$COLOR_ID 
				,'TECHNIQUE_ID'=>$TECHNIQUE_ID 
				,'INK_TYPE_ID'=>$INK_TYPE_ID 
				,'INK_RM_ITEM_WEIGHT'=>$INK_RM_ITEM_WEIGHT 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('PROCESS_ID,ORDER_NO',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['PROCESS_ID'])
				$html .= '<option selected="selected" value="'.$row['PROCESS_ID'].'">'.$row['ORDER_NO'].'</option>';	
			else
				$html .= '<option value="'.$row['PROCESS_ID'].'">'.$row['ORDER_NO'].'</option>';	
		}
		return $html;
	}
	
	//END }
}
?>
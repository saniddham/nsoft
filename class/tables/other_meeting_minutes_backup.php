<?php
class other_meeting_minutes_backup{
 
	private $db;
	private $table= "other_meeting_minutes_backup";
	
	//private property
	private $ARRAY_HEADER;
	private $ARRAY_DETAILS;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('ARRAY_HEADER'=>'ARRAY_HEADER',
										'ARRAY_DETAILS'=>'ARRAY_DETAILS',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	/*public function get_field_array(){
		return $this->field_array;	
	}
	function commit()
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "" ;
		unset($this->commitArray);
		
		if($this->intId==NULL || $this->intId=='' || $this->intId=='NULL')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun ARRAY_HEADER
	function getARRAY_HEADER()
	{
		$this->validate();
		return $this->ARRAY_HEADER;
	}
	
	//retun ARRAY_DETAILS
	function getARRAY_DETAILS()
	{
		$this->validate();
		return $this->ARRAY_DETAILS;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set ARRAY_HEADER
	function setARRAY_HEADER($ARRAY_HEADER)
	{
		array_push($this->commitArray,'ARRAY_HEADER');
		$this->ARRAY_HEADER = $ARRAY_HEADER;
	}
	
	//set ARRAY_DETAILS
	function setARRAY_DETAILS($ARRAY_DETAILS)
	{
		array_push($this->commitArray,'ARRAY_DETAILS');
		$this->ARRAY_DETAILS = $ARRAY_DETAILS;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if()
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set()
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "1=1";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
		//else
			//throw new Exception("Record not found.");
	}	*/
	
	
	//END }
}
?>
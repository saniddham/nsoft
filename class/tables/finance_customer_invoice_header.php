<?php
class finance_customer_invoice_header{

	private $db;
	private $table= "finance_customer_invoice_header";

	//private property
	private $SERIAL_NO;
	private $SERIAL_YEAR;
	private $ORDER_NO;
	private $ORDER_YEAR;
	private $INVOICE_NO;
	private $CUSTOMER_ID;
	private $CUSTOMER_LOCATION;
	private $CURRENCY_ID;
	private $BANK_ACCOUNT_ID;
	private $LEDGER_ID;
	private $REMARKS;
	private $INVOICED_DATE;
	private $COMPLETE;
	private $STATUS;
	private $PRINT_STATUS;
	private $COMPANY_ID;
	private $LOCATION_ID;
	private $CREATED_BY;
	private $CREATED_DATE;
	private $commitArray = array();
	private $field_array = array();
	

	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('SERIAL_NO'=>'SERIAL_NO',
										'SERIAL_YEAR'=>'SERIAL_YEAR',
										'ORDER_NO'=>'ORDER_NO',
										'ORDER_YEAR'=>'ORDER_YEAR',
										'INVOICE_NO'=>'INVOICE_NO',
										'CUSTOMER_ID'=>'CUSTOMER_ID',
										'CUSTOMER_LOCATION'=>'CUSTOMER_LOCATION',
										'CURRENCY_ID'=>'CURRENCY_ID',
										'BANK_ACCOUNT_ID'=>'BANK_ACCOUNT_ID',
										'LEDGER_ID'=>'LEDGER_ID',
										'REMARKS'=>'REMARKS',
										'INVOICED_DATE'=>'INVOICED_DATE',
										'COMPLETE'=>'COMPLETE',
										'STATUS'=>'STATUS',
										'PRINT_STATUS'=>'PRINT_STATUS',
										'COMPANY_ID'=>'COMPANY_ID',
										'LOCATION_ID'=>'LOCATION_ID',
										'CREATED_BY'=>'CREATED_BY',
										'CREATED_DATE'=>'CREATED_DATE',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit );
		return $result;
	}

	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data);
		return $this->db->getResult();


	}

	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}

	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();
	}

	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}

	public function get_field_array(){
		return $this->field_array;
	}

	function commit($type='update')
	{
		//commit update
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{
			$data[$v] = $this->$v;
		}
		$where		= "SERIAL_NO = ".$this->SERIAL_NO." and SERIAL_YEAR = ".$this->SERIAL_YEAR."" ;
		$this->commitArray = array();

		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}

	//BEGIN - public functions for private properties {
	
	//retun SERIAL_NO
	function getSERIAL_NO()
	{
		$this->validate();
		return $this->SERIAL_NO;
	}
	
	//retun SERIAL_YEAR
	function getSERIAL_YEAR()
	{
		$this->validate();
		return $this->SERIAL_YEAR;
	}
	
	//retun ORDER_NO
	function getORDER_NO()
	{
		$this->validate();
		return $this->ORDER_NO;
	}
	
	//retun ORDER_YEAR
	function getORDER_YEAR()
	{
		$this->validate();
		return $this->ORDER_YEAR;
	}
	
	//retun INVOICE_NO
	function getINVOICE_NO()
	{
		$this->validate();
		return $this->INVOICE_NO;
	}
	
	//retun CUSTOMER_ID
	function getCUSTOMER_ID()
	{
		$this->validate();
		return $this->CUSTOMER_ID;
	}
	
	//retun CUSTOMER_LOCATION
	function getCUSTOMER_LOCATION()
	{
		$this->validate();
		return $this->CUSTOMER_LOCATION;
	}
	
	//retun CURRENCY_ID
	function getCURRENCY_ID()
	{
		$this->validate();
		return $this->CURRENCY_ID;
	}
	
	//retun BANK_ACCOUNT_ID
	function getBANK_ACCOUNT_ID()
	{
		$this->validate();
		return $this->BANK_ACCOUNT_ID;
	}
	
	//retun LEDGER_ID
	function getLEDGER_ID()
	{
		$this->validate();
		return $this->LEDGER_ID;
	}
	
	//retun REMARKS
	function getREMARKS()
	{
		$this->validate();
		return $this->REMARKS;
	}
	
	//retun INVOICED_DATE
	function getINVOICED_DATE()
	{
		$this->validate();
		return $this->INVOICED_DATE;
	}
	
	//retun COMPLETE
	function getCOMPLETE()
	{
		$this->validate();
		return $this->COMPLETE;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//retun PRINT_STATUS
	function getPRINT_STATUS()
	{
		$this->validate();
		return $this->PRINT_STATUS;
	}
	
	//retun COMPANY_ID
	function getCOMPANY_ID()
	{
		$this->validate();
		return $this->COMPANY_ID;
	}
	
	//retun LOCATION_ID
	function getLOCATION_ID()
	{
		$this->validate();
		return $this->LOCATION_ID;
	}
	
	//retun CREATED_BY
	function getCREATED_BY()
	{
		$this->validate();
		return $this->CREATED_BY;
	}
	
	//retun CREATED_DATE
	function getCREATED_DATE()
	{
		$this->validate();
		return $this->CREATED_DATE;
	}
	
	//END }

	//BEGIN - public function set {
	
	//set SERIAL_NO
	function setSERIAL_NO($SERIAL_NO)
	{
		array_push($this->commitArray,'SERIAL_NO');
		$this->SERIAL_NO = $SERIAL_NO;
	}
	
	//set SERIAL_YEAR
	function setSERIAL_YEAR($SERIAL_YEAR)
	{
		array_push($this->commitArray,'SERIAL_YEAR');
		$this->SERIAL_YEAR = $SERIAL_YEAR;
	}
	
	//set ORDER_NO
	function setORDER_NO($ORDER_NO)
	{
		array_push($this->commitArray,'ORDER_NO');
		$this->ORDER_NO = $ORDER_NO;
	}
	
	//set ORDER_YEAR
	function setORDER_YEAR($ORDER_YEAR)
	{
		array_push($this->commitArray,'ORDER_YEAR');
		$this->ORDER_YEAR = $ORDER_YEAR;
	}
	
	//set INVOICE_NO
	function setINVOICE_NO($INVOICE_NO)
	{
		array_push($this->commitArray,'INVOICE_NO');
		$this->INVOICE_NO = $INVOICE_NO;
	}
	
	//set CUSTOMER_ID
	function setCUSTOMER_ID($CUSTOMER_ID)
	{
		array_push($this->commitArray,'CUSTOMER_ID');
		$this->CUSTOMER_ID = $CUSTOMER_ID;
	}
	
	//set CUSTOMER_LOCATION
	function setCUSTOMER_LOCATION($CUSTOMER_LOCATION)
	{
		array_push($this->commitArray,'CUSTOMER_LOCATION');
		$this->CUSTOMER_LOCATION = $CUSTOMER_LOCATION;
	}
	
	//set CURRENCY_ID
	function setCURRENCY_ID($CURRENCY_ID)
	{
		array_push($this->commitArray,'CURRENCY_ID');
		$this->CURRENCY_ID = $CURRENCY_ID;
	}
	
	//set BANK_ACCOUNT_ID
	function setBANK_ACCOUNT_ID($BANK_ACCOUNT_ID)
	{
		array_push($this->commitArray,'BANK_ACCOUNT_ID');
		$this->BANK_ACCOUNT_ID = $BANK_ACCOUNT_ID;
	}
	
	//set LEDGER_ID
	function setLEDGER_ID($LEDGER_ID)
	{
		array_push($this->commitArray,'LEDGER_ID');
		$this->LEDGER_ID = $LEDGER_ID;
	}
	
	//set REMARKS
	function setREMARKS($REMARKS)
	{
		array_push($this->commitArray,'REMARKS');
		$this->REMARKS = $REMARKS;
	}
	
	//set INVOICED_DATE
	function setINVOICED_DATE($INVOICED_DATE)
	{
		array_push($this->commitArray,'INVOICED_DATE');
		$this->INVOICED_DATE = $INVOICED_DATE;
	}
	
	//set COMPLETE
	function setCOMPLETE($COMPLETE)
	{
		array_push($this->commitArray,'COMPLETE');
		$this->COMPLETE = $COMPLETE;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//set PRINT_STATUS
	function setPRINT_STATUS($PRINT_STATUS)
	{
		array_push($this->commitArray,'PRINT_STATUS');
		$this->PRINT_STATUS = $PRINT_STATUS;
	}
	
	//set COMPANY_ID
	function setCOMPANY_ID($COMPANY_ID)
	{
		array_push($this->commitArray,'COMPANY_ID');
		$this->COMPANY_ID = $COMPANY_ID;
	}
	
	//set LOCATION_ID
	function setLOCATION_ID($LOCATION_ID)
	{
		array_push($this->commitArray,'LOCATION_ID');
		$this->LOCATION_ID = $LOCATION_ID;
	}
	
	//set CREATED_BY
	function setCREATED_BY($CREATED_BY)
	{
		array_push($this->commitArray,'CREATED_BY');
		$this->CREATED_BY = $CREATED_BY;
	}
	
	//set CREATED_DATE
	function setCREATED_DATE($CREATED_DATE)
	{
		array_push($this->commitArray,'CREATED_DATE');
		$this->CREATED_DATE = $CREATED_DATE;
	}
	
	//END }

	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->SERIAL_NO=='' || $this->SERIAL_YEAR=='')
			throw new exception("Set primary values first");
		else
			return true;
	}

	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}

	public function set($SERIAL_NO , $SERIAL_YEAR)
	{
		$cols	= "*";

		$join	= NULL;

		$where	= "SERIAL_NO='$SERIAL_NO' and SERIAL_YEAR='$SERIAL_YEAR'";

		$result = $this->select($cols,$join,$where);
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}

	//insert as parameters
	public function insertRec($SERIAL_NO,$SERIAL_YEAR,$ORDER_NO,$ORDER_YEAR,$INVOICE_NO,$CUSTOMER_ID,$CUSTOMER_LOCATION,$CURRENCY_ID,$BANK_ACCOUNT_ID,$LEDGER_ID,$REMARKS,$INVOICED_DATE,$COMPLETE,$STATUS,$PRINT_STATUS,$COMPANY_ID,$LOCATION_ID,$CREATED_BY,$CREATED_DATE){
		$data = array('SERIAL_NO'=>$SERIAL_NO 
				,'SERIAL_YEAR'=>$SERIAL_YEAR 
				,'ORDER_NO'=>$ORDER_NO 
				,'ORDER_YEAR'=>$ORDER_YEAR 
				,'INVOICE_NO'=>$INVOICE_NO 
				,'CUSTOMER_ID'=>$CUSTOMER_ID 
				,'CUSTOMER_LOCATION'=>$CUSTOMER_LOCATION 
				,'CURRENCY_ID'=>$CURRENCY_ID 
				,'BANK_ACCOUNT_ID'=>$BANK_ACCOUNT_ID 
				,'LEDGER_ID'=>$LEDGER_ID 
				,'REMARKS'=>$REMARKS 
				,'INVOICED_DATE'=>$INVOICED_DATE 
				,'COMPLETE'=>$COMPLETE 
				,'STATUS'=>$STATUS 
				,'PRINT_STATUS'=>$PRINT_STATUS 
				,'COMPANY_ID'=>$COMPANY_ID 
				,'LOCATION_ID'=>$LOCATION_ID 
				,'CREATED_BY'=>$CREATED_BY 
				,'CREATED_DATE'=>$CREATED_DATE 
				);
		return $this->insert($data);
	}

	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('SERIAL_NO,SERIAL_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['SERIAL_NO'])
				$html .= '<option selected="selected" value="'.$row['SERIAL_NO'].'">'.$row['SERIAL_YEAR'].'</option>';
			else
				$html .= '<option value="'.$row['SERIAL_NO'].'">'.$row['SERIAL_YEAR'].'</option>';
		}
		return $html;
	}
	
	//END }
}
?>
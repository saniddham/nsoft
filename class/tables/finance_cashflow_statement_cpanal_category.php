<?php
class finance_cashflow_statement_cpanal_category{

	private $db;
	private $table= "finance_cashflow_statement_cpanal_category";

	//private property
	private $REPORT_CATEGORY_ID;
	private $REPORT_CATEGORY_NAME;
	private $ORDER_BY_ID;
	private $STATUS;
	private $CREATED_BY;
	private $CREATED_DATE;
	private $MODIFIED_BY;
	private $MODIFIED_DATE;
	private $commitArray = array();
	private $field_array = array();
	

	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('REPORT_CATEGORY_ID'=>'REPORT_CATEGORY_ID',
										'REPORT_CATEGORY_NAME'=>'REPORT_CATEGORY_NAME',
										'ORDER_BY_ID'=>'ORDER_BY_ID',
										'STATUS'=>'STATUS',
										'CREATED_BY'=>'CREATED_BY',
										'CREATED_DATE'=>'CREATED_DATE',
										'MODIFIED_BY'=>'MODIFIED_BY',
										'MODIFIED_DATE'=>'MODIFIED_DATE',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit );
		return $result;
	}

	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data);
		return $this->db->getResult();


	}

	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}

	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();
	}

	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}

	public function get_field_array(){
		return $this->field_array;
	}

	function commit($type='update')
	{
		//commit update
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{
			$data[$v] = $this->$v;
		}
		$where		= "REPORT_CATEGORY_ID = ".$this->REPORT_CATEGORY_ID."" ;
		$this->commitArray = array();

		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}

	//BEGIN - public functions for private properties {
	
	//retun REPORT_CATEGORY_ID
	function getREPORT_CATEGORY_ID()
	{
		$this->validate();
		return $this->REPORT_CATEGORY_ID;
	}
	
	//retun REPORT_CATEGORY_NAME
	function getREPORT_CATEGORY_NAME()
	{
		$this->validate();
		return $this->REPORT_CATEGORY_NAME;
	}
	
	//retun ORDER_BY_ID
	function getORDER_BY_ID()
	{
		$this->validate();
		return $this->ORDER_BY_ID;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//retun CREATED_BY
	function getCREATED_BY()
	{
		$this->validate();
		return $this->CREATED_BY;
	}
	
	//retun CREATED_DATE
	function getCREATED_DATE()
	{
		$this->validate();
		return $this->CREATED_DATE;
	}
	
	//retun MODIFIED_BY
	function getMODIFIED_BY()
	{
		$this->validate();
		return $this->MODIFIED_BY;
	}
	
	//retun MODIFIED_DATE
	function getMODIFIED_DATE()
	{
		$this->validate();
		return $this->MODIFIED_DATE;
	}
	
	//END }

	//BEGIN - public function set {
	
	//set REPORT_CATEGORY_ID
	function setREPORT_CATEGORY_ID($REPORT_CATEGORY_ID)
	{
		array_push($this->commitArray,'REPORT_CATEGORY_ID');
		$this->REPORT_CATEGORY_ID = $REPORT_CATEGORY_ID;
	}
	
	//set REPORT_CATEGORY_NAME
	function setREPORT_CATEGORY_NAME($REPORT_CATEGORY_NAME)
	{
		array_push($this->commitArray,'REPORT_CATEGORY_NAME');
		$this->REPORT_CATEGORY_NAME = $REPORT_CATEGORY_NAME;
	}
	
	//set ORDER_BY_ID
	function setORDER_BY_ID($ORDER_BY_ID)
	{
		array_push($this->commitArray,'ORDER_BY_ID');
		$this->ORDER_BY_ID = $ORDER_BY_ID;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//set CREATED_BY
	function setCREATED_BY($CREATED_BY)
	{
		array_push($this->commitArray,'CREATED_BY');
		$this->CREATED_BY = $CREATED_BY;
	}
	
	//set CREATED_DATE
	function setCREATED_DATE($CREATED_DATE)
	{
		array_push($this->commitArray,'CREATED_DATE');
		$this->CREATED_DATE = $CREATED_DATE;
	}
	
	//set MODIFIED_BY
	function setMODIFIED_BY($MODIFIED_BY)
	{
		array_push($this->commitArray,'MODIFIED_BY');
		$this->MODIFIED_BY = $MODIFIED_BY;
	}
	
	//set MODIFIED_DATE
	function setMODIFIED_DATE($MODIFIED_DATE)
	{
		array_push($this->commitArray,'MODIFIED_DATE');
		$this->MODIFIED_DATE = $MODIFIED_DATE;
	}
	
	//END }

#BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->REPORT_CATEGORY_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}

	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}

	public function set($REPORT_CATEGORY_ID)
	{
		$cols	= "*";

		$join	= NULL;

		$where	= "REPORT_CATEGORY_ID='$REPORT_CATEGORY_ID'";

		$result = $this->select($cols,$join,$where);
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}

	//insert as parameters
	public function insertRec($REPORT_CATEGORY_NAME,$ORDER_BY_ID,$STATUS,$CREATED_BY,$CREATED_DATE,$MODIFIED_BY,$MODIFIED_DATE){
		$data = array('REPORT_CATEGORY_NAME'=>$REPORT_CATEGORY_NAME 
				,'ORDER_BY_ID'=>$ORDER_BY_ID 
				,'STATUS'=>$STATUS 
				,'CREATED_BY'=>$CREATED_BY 
				,'CREATED_DATE'=>$CREATED_DATE 
				,'MODIFIED_BY'=>$MODIFIED_BY 
				,'MODIFIED_DATE'=>$MODIFIED_DATE 
				);
		return $this->insert($data);
	}

	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('REPORT_CATEGORY_ID,REPORT_CATEGORY_NAME',  null, $where = $where);
		$html = '<option value=""></option>';
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['REPORT_CATEGORY_ID'])
				$html .= '<option selected="selected" value="'.$row['REPORT_CATEGORY_ID'].'">'.$row['REPORT_CATEGORY_NAME'].'</option>';
			else
				$html .= '<option value="'.$row['REPORT_CATEGORY_ID'].'">'.$row['REPORT_CATEGORY_NAME'].'</option>';
		}
		return $html;
	}
	
#END }
	
#BEGIN  - USER DEFINED FUNCTIONS {
	public function getDetails()
	{
		$cols	= "REPORT_CATEGORY_ID,
				   REPORT_CATEGORY_NAME";

		$where 	= "STATUS = 1";
		
		$order 	= "ORDER_BY_ID";
		return $this->select($cols,$join=null,$where,$order,$limit=null);
		
	}
#END 	- USER DEFINED FUNCTIONS }
}
?>
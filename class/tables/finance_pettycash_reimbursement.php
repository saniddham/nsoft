<?php
class finance_pettycash_reimbursement{

	private $db;
	private $table= "finance_pettycash_reimbursement";

	//private property
	private $REIMBURSEMENT_NO;
	private $REIMBURSEMENT_YEAR;
	private $DATE;
	private $LOCATION_ID;
	private $AMOUNT;
	private $REMARKS;
	private $STATUS;
	private $APPROVE_LEVELS;
	private $USER_LOCATION_ID;
	private $CREATED_BY;
	private $CREATED_DATE;
	private $MODIFIED_BY;
	private $MODIFIED_DATE;
	private $commitArray = array();
	private $field_array = array();
	

	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('REIMBURSEMENT_NO'=>'REIMBURSEMENT_NO',
										'REIMBURSEMENT_YEAR'=>'REIMBURSEMENT_YEAR',
										'DATE'=>'DATE',
										'LOCATION_ID'=>'LOCATION_ID',
										'AMOUNT'=>'AMOUNT',
										'REMARKS'=>'REMARKS',
										'STATUS'=>'STATUS',
										'APPROVE_LEVELS'=>'APPROVE_LEVELS',
										'USER_LOCATION_ID'=>'USER_LOCATION_ID',
										'CREATED_BY'=>'CREATED_BY',
										'CREATED_DATE'=>'CREATED_DATE',
										'MODIFIED_BY'=>'MODIFIED_BY',
										'MODIFIED_DATE'=>'MODIFIED_DATE',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit );
		return $result;
	}

	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data);
		return $this->db->getResult();


	}

	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}

	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();
	}

	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}

	public function get_field_array(){
		return $this->field_array;
	}

	function commit($type='update')
	{
		//commit update
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{
			$data[$v] = $this->$v;
		}
		$where		= "REIMBURSEMENT_NO = ".$this->REIMBURSEMENT_NO." and REIMBURSEMENT_YEAR = ".$this->REIMBURSEMENT_YEAR."" ;
		$this->commitArray = array();

		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}

	//BEGIN - public functions for private properties {
	
	//retun REIMBURSEMENT_NO
	function getREIMBURSEMENT_NO()
	{
		$this->validate();
		return $this->REIMBURSEMENT_NO;
	}
	
	//retun REIMBURSEMENT_YEAR
	function getREIMBURSEMENT_YEAR()
	{
		$this->validate();
		return $this->REIMBURSEMENT_YEAR;
	}
	
	//retun DATE
	function getDATE()
	{
		$this->validate();
		return $this->DATE;
	}
	
	//retun LOCATION_ID
	function getLOCATION_ID()
	{
		$this->validate();
		return $this->LOCATION_ID;
	}
	
	//retun AMOUNT
	function getAMOUNT()
	{
		$this->validate();
		return $this->AMOUNT;
	}
	
	//retun REMARKS
	function getREMARKS()
	{
		$this->validate();
		return $this->REMARKS;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//retun APPROVE_LEVELS
	function getAPPROVE_LEVELS()
	{
		$this->validate();
		return $this->APPROVE_LEVELS;
	}
	
	//retun USER_LOCATION_ID
	function getUSER_LOCATION_ID()
	{
		$this->validate();
		return $this->USER_LOCATION_ID;
	}
	
	//retun CREATED_BY
	function getCREATED_BY()
	{
		$this->validate();
		return $this->CREATED_BY;
	}
	
	//retun CREATED_DATE
	function getCREATED_DATE()
	{
		$this->validate();
		return $this->CREATED_DATE;
	}
	
	//retun MODIFIED_BY
	function getMODIFIED_BY()
	{
		$this->validate();
		return $this->MODIFIED_BY;
	}
	
	//retun MODIFIED_DATE
	function getMODIFIED_DATE()
	{
		$this->validate();
		return $this->MODIFIED_DATE;
	}
	
	//END }

	//BEGIN - public function set {
	
	//set REIMBURSEMENT_NO
	function setREIMBURSEMENT_NO($REIMBURSEMENT_NO)
	{
		array_push($this->commitArray,'REIMBURSEMENT_NO');
		$this->REIMBURSEMENT_NO = $REIMBURSEMENT_NO;
	}
	
	//set REIMBURSEMENT_YEAR
	function setREIMBURSEMENT_YEAR($REIMBURSEMENT_YEAR)
	{
		array_push($this->commitArray,'REIMBURSEMENT_YEAR');
		$this->REIMBURSEMENT_YEAR = $REIMBURSEMENT_YEAR;
	}
	
	//set DATE
	function setDATE($DATE)
	{
		array_push($this->commitArray,'DATE');
		$this->DATE = $DATE;
	}
	
	//set LOCATION_ID
	function setLOCATION_ID($LOCATION_ID)
	{
		array_push($this->commitArray,'LOCATION_ID');
		$this->LOCATION_ID = $LOCATION_ID;
	}
	
	//set AMOUNT
	function setAMOUNT($AMOUNT)
	{
		array_push($this->commitArray,'AMOUNT');
		$this->AMOUNT = $AMOUNT;
	}
	
	//set REMARKS
	function setREMARKS($REMARKS)
	{
		array_push($this->commitArray,'REMARKS');
		$this->REMARKS = $REMARKS;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//set APPROVE_LEVELS
	function setAPPROVE_LEVELS($APPROVE_LEVELS)
	{
		array_push($this->commitArray,'APPROVE_LEVELS');
		$this->APPROVE_LEVELS = $APPROVE_LEVELS;
	}
	
	//set USER_LOCATION_ID
	function setUSER_LOCATION_ID($USER_LOCATION_ID)
	{
		array_push($this->commitArray,'USER_LOCATION_ID');
		$this->USER_LOCATION_ID = $USER_LOCATION_ID;
	}
	
	//set CREATED_BY
	function setCREATED_BY($CREATED_BY)
	{
		array_push($this->commitArray,'CREATED_BY');
		$this->CREATED_BY = $CREATED_BY;
	}
	
	//set CREATED_DATE
	function setCREATED_DATE($CREATED_DATE)
	{
		array_push($this->commitArray,'CREATED_DATE');
		$this->CREATED_DATE = $CREATED_DATE;
	}
	
	//set MODIFIED_BY
	function setMODIFIED_BY($MODIFIED_BY)
	{
		array_push($this->commitArray,'MODIFIED_BY');
		$this->MODIFIED_BY = $MODIFIED_BY;
	}
	
	//set MODIFIED_DATE
	function setMODIFIED_DATE($MODIFIED_DATE)
	{
		array_push($this->commitArray,'MODIFIED_DATE');
		$this->MODIFIED_DATE = $MODIFIED_DATE;
	}
	
	//END }

	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->REIMBURSEMENT_NO=='' || $this->REIMBURSEMENT_YEAR=='')
			throw new exception("Set primary values first");
		else
			return true;
	}

	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}

	public function set($REIMBURSEMENT_NO , $REIMBURSEMENT_YEAR)
	{
		$cols	= "*";

		$join	= NULL;

		$where	= "REIMBURSEMENT_NO='$REIMBURSEMENT_NO' and REIMBURSEMENT_YEAR='$REIMBURSEMENT_YEAR'";

		$result = $this->select($cols,$join,$where);
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}

	//insert as parameters
	public function insertRec($REIMBURSEMENT_NO,$REIMBURSEMENT_YEAR,$DATE,$LOCATION_ID,$AMOUNT,$REMARKS,$STATUS,$APPROVE_LEVELS,$USER_LOCATION_ID,$CREATED_BY,$CREATED_DATE,$MODIFIED_BY,$MODIFIED_DATE){
		$data = array('REIMBURSEMENT_NO'=>$REIMBURSEMENT_NO 
				,'REIMBURSEMENT_YEAR'=>$REIMBURSEMENT_YEAR 
				,'DATE'=>$DATE 
				,'LOCATION_ID'=>$LOCATION_ID 
				,'AMOUNT'=>$AMOUNT 
				,'REMARKS'=>$REMARKS 
				,'STATUS'=>$STATUS 
				,'APPROVE_LEVELS'=>$APPROVE_LEVELS 
				,'USER_LOCATION_ID'=>$USER_LOCATION_ID 
				,'CREATED_BY'=>$CREATED_BY 
				,'CREATED_DATE'=>$CREATED_DATE 
				,'MODIFIED_BY'=>$MODIFIED_BY 
				,'MODIFIED_DATE'=>$MODIFIED_DATE 
				);
		return $this->insert($data);
	}

	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('REIMBURSEMENT_NO,REIMBURSEMENT_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['REIMBURSEMENT_NO'])
				$html .= '<option selected="selected" value="'.$row['REIMBURSEMENT_NO'].'">'.$row['REIMBURSEMENT_YEAR'].'</option>';
			else
				$html .= '<option value="'.$row['REIMBURSEMENT_NO'].'">'.$row['REIMBURSEMENT_YEAR'].'</option>';
		}
		return $html;
	}
	
	//END }
}
?>
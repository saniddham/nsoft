<?php
class trn_sales_order_open_header{
 
	private $db;
	private $table= "trn_sales_order_open_header";
	
	//private property
	private $OPEN_NO;
	private $OPEN_YEAR;
	private $REQUISITION_NO;
	private $REQUISITION_YEAR;
	private $ORDER_NO;
	private $ORDER_YEAR;
	private $CREATED_DATE;
	private $CREATED_BY;
	private $MODIFIED_DATE;
	private $MODIFIED_BY;
	private $APPROVE_LEVELS;
	private $STATUS;
	private $LOCATION_ID;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('OPEN_NO'=>'OPEN_NO',
										'OPEN_YEAR'=>'OPEN_YEAR',
										'REQUISITION_NO'=>'REQUISITION_NO',
										'REQUISITION_YEAR'=>'REQUISITION_YEAR',
										'ORDER_NO'=>'ORDER_NO',
										'ORDER_YEAR'=>'ORDER_YEAR',
										'CREATED_DATE'=>'CREATED_DATE',
										'CREATED_BY'=>'CREATED_BY',
										'MODIFIED_DATE'=>'MODIFIED_DATE',
										'MODIFIED_BY'=>'MODIFIED_BY',
										'APPROVE_LEVELS'=>'APPROVE_LEVELS',
										'STATUS'=>'STATUS',
										'LOCATION_ID'=>'LOCATION_ID',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "OPEN_NO = ".$this->OPEN_NO." and OPEN_YEAR = ".$this->OPEN_YEAR."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun OPEN_NO
	function getOPEN_NO()
	{
		$this->validate();
		return $this->OPEN_NO;
	}
	
	//retun OPEN_YEAR
	function getOPEN_YEAR()
	{
		$this->validate();
		return $this->OPEN_YEAR;
	}
	
	//retun REQUISITION_NO
	function getREQUISITION_NO()
	{
		$this->validate();
		return $this->REQUISITION_NO;
	}
	
	//retun REQUISITION_YEAR
	function getREQUISITION_YEAR()
	{
		$this->validate();
		return $this->REQUISITION_YEAR;
	}

	function getORDER_NO()
	{
		$this->validate();
		return $this->ORDER_NO;
	}
	
	//retun REQUISITION_YEAR
	function getORDER_YEAR()
	{
		$this->validate();
		return $this->ORDER_YEAR;
	}
	
	//retun CREATED_DATE
	function getCREATED_DATE()
	{
		$this->validate();
		return $this->CREATED_DATE;
	}
	
	//retun CREATED_BY
	function getCREATED_BY()
	{
		$this->validate();
		return $this->CREATED_BY;
	}
	
	//retun MODIFIED_DATE
	function getMODIFIED_DATE()
	{
		$this->validate();
		return $this->MODIFIED_DATE;
	}
	
	//retun MODIFIED_BY
	function getMODIFIED_BY()
	{
		$this->validate();
		return $this->MODIFIED_BY;
	}
	
	//retun APPROVE_LEVELS
	function getAPPROVE_LEVELS()
	{
		$this->validate();
		return $this->APPROVE_LEVELS;
	}
	
	//retun LOCATION_ID
	function getLOCATION_ID()
	{
		$this->validate();
		return $this->LOCATION_ID;
	}

	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set OPEN_NO
	function setOPEN_NO($OPEN_NO)
	{
		array_push($this->commitArray,'OPEN_NO');
		$this->OPEN_NO = $OPEN_NO;
	}
	
	//set OPEN_YEAR
	function setOPEN_YEAR($OPEN_YEAR)
	{
		array_push($this->commitArray,'OPEN_YEAR');
		$this->OPEN_YEAR = $OPEN_YEAR;
	}
	
	//set REQUISITION_NO
	function setREQUISITION_NO($REQUISITION_NO)
	{
		array_push($this->commitArray,'REQUISITION_NO');
		$this->REQUISITION_NO = $REQUISITION_NO;
	}
	
	//set REQUISITION_YEAR
	function setREQUISITION_YEAR($REQUISITION_YEAR)
	{
		array_push($this->commitArray,'REQUISITION_YEAR');
		$this->REQUISITION_YEAR = $REQUISITION_YEAR;
	}
	
	//set REQUISITION_YEAR
	function setORDER_NO($ORDER_NO)
	{
		array_push($this->commitArray,'ORDER_NO');
		$this->setORDER_NO = $ORDER_NO;
	}

	//set REQUISITION_YEAR
	function setORDER_YEAR($ORDER_YEAR)
	{
		array_push($this->commitArray,'ORDER_YEAR');
		$this->ORDER_YEAR = $ORDER_YEAR;
	}

	//set CREATED_DATE
	function setCREATED_DATE($CREATED_DATE)
	{
		array_push($this->commitArray,'CREATED_DATE');
		$this->CREATED_DATE = $CREATED_DATE;
	}
	
	//set CREATED_BY
	function setCREATED_BY($CREATED_BY)
	{
		array_push($this->commitArray,'CREATED_BY');
		$this->CREATED_BY = $CREATED_BY;
	}
	
	//set MODIFIED_DATE
	function setMODIFIED_DATE($MODIFIED_DATE)
	{
		array_push($this->commitArray,'MODIFIED_DATE');
		$this->MODIFIED_DATE = $MODIFIED_DATE;
	}
	
	//set MODIFIED_BY
	function setMODIFIED_BY($MODIFIED_BY)
	{
		array_push($this->commitArray,'MODIFIED_BY');
		$this->MODIFIED_BY = $MODIFIED_BY;
	}
	
	//set APPROVE_LEVELS
	function setAPPROVE_LEVELS($APPROVE_LEVELS)
	{
		array_push($this->commitArray,'APPROVE_LEVELS');
		$this->APPROVE_LEVELS = $APPROVE_LEVELS;
	}
	
	//set STATUS
	function setLOCATION_ID($LOCATION_ID)
	{
		array_push($this->commitArray,'LOCATION_ID');
		$this->LOCATION_ID = $LOCATION_ID;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}

	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->OPEN_NO=='' || $this->OPEN_YEAR=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($OPEN_NO , $OPEN_YEAR)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "OPEN_NO='$OPEN_NO' and OPEN_YEAR='$OPEN_YEAR'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($OPEN_NO,$OPEN_YEAR,$REQUISITION_NO,$REQUISITION_YEAR,$ORDER_NO,$ORDER_YEAR,$CREATED_DATE,$CREATED_BY,$MODIFIED_DATE,$MODIFIED_BY,$APPROVE_LEVELS,$STATUS,$LOCATION_ID){
		$data = array('OPEN_NO'=>$OPEN_NO 
				,'OPEN_YEAR'=>$OPEN_YEAR 
				,'REQUISITION_NO'=>$REQUISITION_NO 
				,'REQUISITION_YEAR'=>$REQUISITION_YEAR 
				,'ORDER_NO'=>$ORDER_NO 
				,'ORDER_YEAR'=>$ORDER_YEAR 
				,'CREATED_DATE'=>$CREATED_DATE 
				,'CREATED_BY'=>$CREATED_BY 
				,'MODIFIED_DATE'=>$MODIFIED_DATE 
				,'MODIFIED_BY'=>$MODIFIED_BY 
				,'APPROVE_LEVELS'=>$APPROVE_LEVELS 
				,'STATUS'=>$STATUS  
				,'LOCATION_ID'=>$LOCATION_ID  
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('OPEN_NO,OPEN_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['OPEN_NO'])
				$html .= '<option selected="selected" value="'.$row['OPEN_NO'].'">'.$row['OPEN_YEAR'].'</option>';	
			else
				$html .= '<option value="'.$row['OPEN_NO'].'">'.$row['OPEN_YEAR'].'</option>';	
		}
		return $html;
	}
	
	//END }
}
?>
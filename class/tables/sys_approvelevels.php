
<?php

class sys_approvelevels{
 
	private $db;
	private $table= "sys_approvelevels";
	
	//private property
	private $intFormId;
	private $strCode;
	private $strName;
	private $intApprovalLevel;
	private $intCreator;
	private $dtmCreateDate;
	private $intModifyer;
	private $dtmModifyDate;
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->db->field_array_set(array('intFormId'=>'intFormId',
										'strCode'=>'strCode',
										'strName'=>'strName',
										'intApprovalLevel'=>'intApprovalLevel',
										'intCreator'=>'intCreator',
										'dtmCreateDate'=>'dtmCreateDate',
										'intModifyer'=>'intModifyer',
										'dtmModifyDate'=>'dtmModifyDate',
										));
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  
		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	//public functions for private properties
	
	//retun intFormId
	function getintFormId()
	{
		$this->validate();
		return $this->intFormId;
	}
	
	//retun strCode
	function getstrCode()
	{
		$this->validate();
		return $this->strCode;
	}
	
	//retun strName
	function getstrName()
	{
		$this->validate();
		return $this->strName;
	}
	
	//retun intApprovalLevel
	function getintApprovalLevel()
	{
		$this->validate();
		return $this->intApprovalLevel;
	}
	
	//retun intCreator
	function getintCreator()
	{
		$this->validate();
		return $this->intCreator;
	}
	
	//retun dtmCreateDate
	function getdtmCreateDate()
	{
		$this->validate();
		return $this->dtmCreateDate;
	}
	
	//retun intModifyer
	function getintModifyer()
	{
		$this->validate();
		return $this->intModifyer;
	}
	
	//retun dtmModifyDate
	function getdtmModifyDate()
	{
		$this->validate();
		return $this->dtmModifyDate;
	}
	
	//validate primary values
	private function validate()
	{
		if($this->intFormId=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
		}
	}
	
	public function set($strCode)
	{
		$result = $this->select('*',null,"strCode='$strCode'");	
		if($this->db->numRows()>0)
			$this->setVariables($result);
		else
			throw new Exception("Approve levels not define.Please contact system administrator.");
			
	}
	
	
}
?>

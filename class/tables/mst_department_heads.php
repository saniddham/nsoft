<?php
class mst_department_heads{
 
	private $db;
	private $table= "mst_department_heads";
	
	//private property
	private $LOCATION_ID;
	private $DEPARTMENT_ID;
	private $DEP_HEAD_ID;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('LOCATION_ID'=>'LOCATION_ID',
										'DEPARTMENT_ID'=>'DEPARTMENT_ID',
										'DEP_HEAD_ID'=>'DEP_HEAD_ID',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "LOCATION_ID = ".$this->LOCATION_ID." and DEPARTMENT_ID = ".$this->DEPARTMENT_ID."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun LOCATION_ID
	function getLOCATION_ID()
	{
		$this->validate();
		return $this->LOCATION_ID;
	}
	
	//retun DEPARTMENT_ID
	function getDEPARTMENT_ID()
	{
		$this->validate();
		return $this->DEPARTMENT_ID;
	}
	
	//retun DEP_HEAD_ID
	function getDEP_HEAD_ID()
	{
		$this->validate();
		return $this->DEP_HEAD_ID;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set LOCATION_ID
	function setLOCATION_ID($LOCATION_ID)
	{
		array_push($this->commitArray,'LOCATION_ID');
		$this->LOCATION_ID = $LOCATION_ID;
	}
	
	//set DEPARTMENT_ID
	function setDEPARTMENT_ID($DEPARTMENT_ID)
	{
		array_push($this->commitArray,'DEPARTMENT_ID');
		$this->DEPARTMENT_ID = $DEPARTMENT_ID;
	}
	
	//set DEP_HEAD_ID
	function setDEP_HEAD_ID($DEP_HEAD_ID)
	{
		array_push($this->commitArray,'DEP_HEAD_ID');
		$this->DEP_HEAD_ID = $DEP_HEAD_ID;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->LOCATION_ID=='' || $this->DEPARTMENT_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($LOCATION_ID , $DEPARTMENT_ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "LOCATION_ID='$LOCATION_ID' and DEPARTMENT_ID='$DEPARTMENT_ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($LOCATION_ID,$DEPARTMENT_ID,$DEP_HEAD_ID){
		$data = array('LOCATION_ID'=>$LOCATION_ID 
				,'DEPARTMENT_ID'=>$DEPARTMENT_ID 
				,'DEP_HEAD_ID'=>$DEP_HEAD_ID 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('LOCATION_ID,DEPARTMENT_ID',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['LOCATION_ID'])
				$html .= '<option selected="selected" value="'.$row['LOCATION_ID'].'">'.$row['DEPARTMENT_ID'].'</option>';	
			else
				$html .= '<option value="'.$row['LOCATION_ID'].'">'.$row['DEPARTMENT_ID'].'</option>';	
		}
		return $html;
	}
	
	public function checkAvailable($locationId,$deptId)
	{
		$cols	= "*";		
	
		$where	= "LOCATION_ID='$locationId' and DEPARTMENT_ID='$deptId'";
		
		$result = $this->select($cols,$join=null,$where);	
		if($this->db->numRows()>0)
			return true;
		else
			return false;
	}
	
	public function getDepartmentWiseApprovePermission($departmentId,$itemId,$userId,$location)
	{//echo $userId;
		$col	= " mst_department_heads.DEP_HEAD_ID";
		$where	= " mst_department_heads.DEPARTMENT_ID = '$departmentId' AND
					finance_pettycash_invoice_detail.PETTY_CASH_ITEM_ID = '$itemId' AND
					mst_department_heads.LOCATION_ID = '$location' AND
					finance_pettycash_invoice_detail.DEPT_APPROVE_STATUS > 1
					GROUP BY
					finance_pettycash_invoice_detail.DEPARTMENT_ID,
					finance_pettycash_invoice_detail.PETTY_CASH_ITEM_ID";
		$join	= " INNER JOIN finance_pettycash_invoice_detail ON mst_department_heads.DEPARTMENT_ID = finance_pettycash_invoice_detail.DEPARTMENT_ID";
				
		$result	= $this->select($col,$join,$where,NULL,NULL);
		while($row = mysqli_fetch_array($result))
		{
			if($row['DEP_HEAD_ID'] != $userId)
			$permision_confirm['permision']	= 0;
			else
			$permision_confirm['permision']	=1;
			//echo $permision_confirm['permision'];
			return $permision_confirm;	
		}	
	}
	//END }
}
?>
<?php
class sys_report_cluster_types{

	private $db;
	private $table= "sys_report_cluster_types";

	//private property
	private $NO;
	private $COMPANY_ID;
	private $REPORT_TYPE;
	private $CLUSTER_TYPE;
	private $MAIN_CLUSTER_ID;
	private $STATUS;
	private $TO;
	private $CC;
	private $BCC;
	private $WEEKLY_HOLIDAY;
	private $commitArray = array();
	private $field_array = array();
	

	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('NO'=>'NO',
										'COMPANY_ID'=>'COMPANY_ID',
										'REPORT_TYPE'=>'REPORT_TYPE',
										'CLUSTER_TYPE'=>'CLUSTER_TYPE',
										'MAIN_CLUSTER_ID'=>'MAIN_CLUSTER_ID',
										'STATUS'=>'STATUS',
										'TO'=>'TO',
										'CC'=>'CC',
										'BCC'=>'BCC',
										'WEEKLY_HOLIDAY'=>'WEEKLY_HOLIDAY',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit );
		return $result;
	}

	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data);
		return $this->db->getResult();


	}

	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}

	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();
	}

	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}

	public function get_field_array(){
		return $this->field_array;
	}

	function commit($type='update')
	{
		//commit update
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{
			$data[$v] = $this->$v;
		}
		$where		= "NO = ".$this->NO."" ;
		$this->commitArray = array();

		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}

	//BEGIN - public functions for private properties {
	
	//retun NO
	function getNO()
	{
		$this->validate();
		return $this->NO;
	}
	
	//retun COMPANY_ID
	function getCOMPANY_ID()
	{
		$this->validate();
		return $this->COMPANY_ID;
	}
	
	//retun REPORT_TYPE
	function getREPORT_TYPE()
	{
		$this->validate();
		return $this->REPORT_TYPE;
	}
	
	//retun CLUSTER_TYPE
	function getCLUSTER_TYPE()
	{
		$this->validate();
		return $this->CLUSTER_TYPE;
	}
	
	//retun MAIN_CLUSTER_ID
	function getMAIN_CLUSTER_ID()
	{
		$this->validate();
		return $this->MAIN_CLUSTER_ID;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//retun TO
	function getTO()
	{
		$this->validate();
		return $this->TO;
	}
	
	//retun CC
	function getCC()
	{
		$this->validate();
		return $this->CC;
	}
	
	//retun BCC
	function getBCC()
	{
		$this->validate();
		return $this->BCC;
	}
	
	//retun WEEKLY_HOLIDAY
	function getWEEKLY_HOLIDAY()
	{
		$this->validate();
		return $this->WEEKLY_HOLIDAY;
	}
	
	//END }

	//BEGIN - public function set {
	
	//set NO
	function setNO($NO)
	{
		array_push($this->commitArray,'NO');
		$this->NO = $NO;
	}
	
	//set COMPANY_ID
	function setCOMPANY_ID($COMPANY_ID)
	{
		array_push($this->commitArray,'COMPANY_ID');
		$this->COMPANY_ID = $COMPANY_ID;
	}
	
	//set REPORT_TYPE
	function setREPORT_TYPE($REPORT_TYPE)
	{
		array_push($this->commitArray,'REPORT_TYPE');
		$this->REPORT_TYPE = $REPORT_TYPE;
	}
	
	//set CLUSTER_TYPE
	function setCLUSTER_TYPE($CLUSTER_TYPE)
	{
		array_push($this->commitArray,'CLUSTER_TYPE');
		$this->CLUSTER_TYPE = $CLUSTER_TYPE;
	}
	
	//set MAIN_CLUSTER_ID
	function setMAIN_CLUSTER_ID($MAIN_CLUSTER_ID)
	{
		array_push($this->commitArray,'MAIN_CLUSTER_ID');
		$this->MAIN_CLUSTER_ID = $MAIN_CLUSTER_ID;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//set TO
	function setTO($TO)
	{
		array_push($this->commitArray,'TO');
		$this->TO = $TO;
	}
	
	//set CC
	function setCC($CC)
	{
		array_push($this->commitArray,'CC');
		$this->CC = $CC;
	}
	
	//set BCC
	function setBCC($BCC)
	{
		array_push($this->commitArray,'BCC');
		$this->BCC = $BCC;
	}
	
	//set WEEKLY_HOLIDAY
	function setWEEKLY_HOLIDAY($WEEKLY_HOLIDAY)
	{
		array_push($this->commitArray,'WEEKLY_HOLIDAY');
		$this->WEEKLY_HOLIDAY = $WEEKLY_HOLIDAY;
	}
	
	//END }

	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->NO=='')
			throw new exception("Set primary values first");
		else
			return true;
	}

	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}

	public function set($NO)
	{
		$cols	= "*";

		$join	= NULL;

		$where	= "NO='$NO'";

		$result = $this->select($cols,$join,$where);
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}

	//insert as parameters
	public function insertRec($NO,$COMPANY_ID,$REPORT_TYPE,$CLUSTER_TYPE,$MAIN_CLUSTER_ID,$STATUS,$TO,$CC,$BCC,$WEEKLY_HOLIDAY){
		$data = array('NO'=>$NO 
				,'COMPANY_ID'=>$COMPANY_ID 
				,'REPORT_TYPE'=>$REPORT_TYPE 
				,'CLUSTER_TYPE'=>$CLUSTER_TYPE 
				,'MAIN_CLUSTER_ID'=>$MAIN_CLUSTER_ID 
				,'STATUS'=>$STATUS 
				,'TO'=>$TO 
				,'CC'=>$CC 
				,'BCC'=>$BCC 
				,'WEEKLY_HOLIDAY'=>$WEEKLY_HOLIDAY 
				);
		return $this->insert($data);
	}

	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('NO,REPORT_TYPE',  null, $where = $where);
		$html = '<option value=""></option>';
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['NO'])
				$html .= '<option selected="selected" value="'.$row['NO'].'">'.$row['REPORT_TYPE'].'</option>';
			else
				$html .= '<option value="'.$row['NO'].'">'.$row['REPORT_TYPE'].'</option>';
		}
		return $html;
	}
	
	//END }
	
#BEGIN  - USER DEFINED FUNCTIONS {
	public function getReportTypeWiseDetails($reportType)
	{
		$cols	= "sys_report_cluster_types.COMPANY_ID		AS COMPANY_ID,
				   sys_report_cluster_types.TO				AS TO_USER_EMAIL_ID_LIST,
				   sys_report_cluster_types.CC				AS CC_USER_EMAIL_ID_LIST,
				   sys_report_cluster_types.BCC				AS BCC_USER_EMAIL_ID_LIST";
		
		$join 	= "INNER JOIN mst_companies
    			       ON mst_companies.intId = sys_report_cluster_types.COMPANY_ID";
				   
		$where 	= "sys_report_cluster_types.REPORT_TYPE = '$reportType'
    			       AND sys_report_cluster_types.STATUS = 1
    				   AND mst_companies.intStatus = 1 ";
		
					//   echo "select ".$cols." from sys_report_cluster_types " .$join." where ".$where;
		return $this->select($cols,$join,$where,$order=null,$limit=null);
	}
#END 	- USER DEFINED FUNCTIONS } 
}
?>
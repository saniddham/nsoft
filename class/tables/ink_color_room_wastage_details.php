<?php
class ink_color_room_wastage_details{
 
	private $db;
	private $table= "ink_color_room_wastage_details";
	
	//private property
	private $SERIAL_NO;
	private $SERIAL_YEAR;
	private $ORDER_NO;
	private $ORDER_YEAR;
	private $SALES_ORDER_ID;
	private $INK_WEIGHT;
	private $WASTAGE_REASON_ID;
	private $WASTAGE_REMARKS;
	private $COLOR_ROOM_PROCESS_ID;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('SERIAL_NO'=>'SERIAL_NO',
										'SERIAL_YEAR'=>'SERIAL_YEAR',
										'ORDER_NO'=>'ORDER_NO',
										'ORDER_YEAR'=>'ORDER_YEAR',
										'SALES_ORDER_ID'=>'SALES_ORDER_ID',
										'INK_WEIGHT'=>'INK_WEIGHT',
										'WASTAGE_REASON_ID'=>'WASTAGE_REASON_ID',
										'WASTAGE_REMARKS'=>'WASTAGE_REMARKS',
										'COLOR_ROOM_PROCESS_ID'=>'COLOR_ROOM_PROCESS_ID' 
										);  
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "SERIAL_NO = ".$this->SERIAL_NO." and SERIAL_YEAR = ".$this->SERIAL_YEAR." and ORDER_NO = ".$this->ORDER_NO." and ORDER_YEAR = ".$this->ORDER_YEAR." and SALES_ORDER_ID = ".$this->SALES_ORDER_ID."" ;
		//unset($this->commitArray);
		$commitArray	= array();
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun SERIAL_NO
	function getSERIAL_NO()
	{
		$this->validate();
		return $this->SERIAL_NO;
	}
	
	//retun SERIAL_YEAR
	function getSERIAL_YEAR()
	{
		$this->validate();
		return $this->SERIAL_YEAR;
	}
	
	//retun ORDER_NO
	function getORDER_NO()
	{
		$this->validate();
		return $this->ORDER_NO;
	}
	
	//retun ORDER_YEAR
	function getORDER_YEAR()
	{
		$this->validate();
		return $this->ORDER_YEAR;
	}
	
	//retun SALES_ORDER_ID
	function getSALES_ORDER_ID()
	{
		$this->validate();
		return $this->SALES_ORDER_ID;
	}
	
	//retun INK_WEIGHT
	function getINK_WEIGHT()
	{
		$this->validate();
		return $this->INK_WEIGHT;
	}
	
	//retun WASTAGE_REASON_ID
	function getWASTAGE_REASON_ID()
	{
		$this->validate();
		return $this->WASTAGE_REASON_ID;
	}
	
	//retun WASTAGE_REMARKS
	function getWASTAGE_REMARKS()
	{
		$this->validate();
		return $this->WASTAGE_REMARKS;
	}

	//retun COLOR_ROOM_PROCESS_ID
	function getCOLOR_ROOM_PROCESS_ID()
	{
		$this->validate();
		return $this->COLOR_ROOM_PROCESS_ID;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set SERIAL_NO
	function setSERIAL_NO($SERIAL_NO)
	{
		array_push($this->commitArray,'SERIAL_NO');
		$this->SERIAL_NO = $SERIAL_NO;
	}
	
	//set SERIAL_YEAR
	function setSERIAL_YEAR($SERIAL_YEAR)
	{
		array_push($this->commitArray,'SERIAL_YEAR');
		$this->SERIAL_YEAR = $SERIAL_YEAR;
	}
	
	//set ORDER_NO
	function setORDER_NO($ORDER_NO)
	{
		array_push($this->commitArray,'ORDER_NO');
		$this->ORDER_NO = $ORDER_NO;
	}
	
	//set ORDER_YEAR
	function setORDER_YEAR($ORDER_YEAR)
	{
		array_push($this->commitArray,'ORDER_YEAR');
		$this->ORDER_YEAR = $ORDER_YEAR;
	}
	
	//set SALES_ORDER_ID
	function setSALES_ORDER_ID($SALES_ORDER_ID)
	{
		array_push($this->commitArray,'SALES_ORDER_ID');
		$this->SALES_ORDER_ID = $SALES_ORDER_ID;
	}
	
	//set INK_WEIGHT
	function setINK_WEIGHT($INK_WEIGHT)
	{
		array_push($this->commitArray,'INK_WEIGHT');
		$this->INK_WEIGHT = $INK_WEIGHT;
	}
	
	//set WASTAGE_REASON_ID
	function setWASTAGE_REASON_ID($WASTAGE_REASON_ID)
	{
		array_push($this->commitArray,'WASTAGE_REASON_ID');
		$this->WASTAGE_REASON_ID = $WASTAGE_REASON_ID;
	}
	
	//set WASTAGE_REMARKS
	function setWASTAGE_REMARKS($WASTAGE_REMARKS)
	{
		array_push($this->commitArray,'WASTAGE_REMARKS');
		$this->WASTAGE_REMARKS = $WASTAGE_REMARKS;
	}
	
	//set WASTAGE_REMARKS
	function setCOLOR_ROOM_PROCESS_ID($COLOR_ROOM_PROCESS_ID)
	{
		array_push($this->commitArray,'COLOR_ROOM_PROCESS_ID');
		$this->COLOR_ROOM_PROCESS_ID = $COLOR_ROOM_PROCESS_ID;
	}

	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->SERIAL_NO=='' || $this->SERIAL_YEAR=='' || $this->ORDER_NO=='' || $this->ORDER_YEAR=='' || $this->SALES_ORDER_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($SERIAL_NO , $SERIAL_YEAR , $ORDER_NO , $ORDER_YEAR , $SALES_ORDER_ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "SERIAL_NO='$SERIAL_NO' and SERIAL_YEAR='$SERIAL_YEAR' and ORDER_NO='$ORDER_NO' and ORDER_YEAR='$ORDER_YEAR' and SALES_ORDER_ID='$SALES_ORDER_ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($SERIAL_NO,$SERIAL_YEAR,$ORDER_NO,$ORDER_YEAR,$SALES_ORDER_ID,$INK_WEIGHT,$WASTAGE_REASON_ID,$WASTAGE_REMARKS){
		$data = array('SERIAL_NO'=>$SERIAL_NO 
				,'SERIAL_YEAR'=>$SERIAL_YEAR 
				,'ORDER_NO'=>$ORDER_NO 
				,'ORDER_YEAR'=>$ORDER_YEAR 
				,'SALES_ORDER_ID'=>$SALES_ORDER_ID 
				,'INK_WEIGHT'=>$INK_WEIGHT 
				,'WASTAGE_REASON_ID'=>$WASTAGE_REASON_ID 
				,'WASTAGE_REMARKS'=>$WASTAGE_REMARKS 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('SERIAL_NO,SERIAL_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['SERIAL_NO'])
				$html .= '<option selected="selected" value="'.$row['SERIAL_NO'].'">'.$row['SERIAL_YEAR'].'</option>';	
			else
				$html .= '<option value="'.$row['SERIAL_NO'].'">'.$row['SERIAL_YEAR'].'</option>';	
		}
		return $html;
	}
	
	
	
	//END }
}
?>
<?php
class finance_customer_transaction{
 
	private $db;
	private $table= "finance_customer_transaction";
	
	//private property
	private $SERIAL_NO;
	private $ORDER_YEAR;
	private $ORDER_NO;
	private $CUSTOMER_ID;
	private $CURRENCY_ID;
	private $DOCUMENT_YEAR;
	private $DOCUMENT_NO;
	private $DOCUMENT_TYPE;
	private $INVOICE_YEAR;
	private $INVOICE_NO;
	private $LEDGER_ID;
	private $VALUE;
	private $COMPANY_ID;
	private $LOCATION_ID;
	private $USER_ID;
	private $TRANSACTION_DATE_TIME;
	private $SYSTEM_MODIFIED_DATE;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('SERIAL_NO'=>'SERIAL_NO',
										'ORDER_YEAR'=>'ORDER_YEAR',
										'ORDER_NO'=>'ORDER_NO',
										'CUSTOMER_ID'=>'CUSTOMER_ID',
										'CURRENCY_ID'=>'CURRENCY_ID',
										'DOCUMENT_YEAR'=>'DOCUMENT_YEAR',
										'DOCUMENT_NO'=>'DOCUMENT_NO',
										'DOCUMENT_TYPE'=>'DOCUMENT_TYPE',
										'INVOICE_YEAR'=>'INVOICE_YEAR',
										'INVOICE_NO'=>'INVOICE_NO',
										'LEDGER_ID'=>'LEDGER_ID',
										'VALUE'=>'VALUE',
										'COMPANY_ID'=>'COMPANY_ID',
										'LOCATION_ID'=>'LOCATION_ID',
										'USER_ID'=>'USER_ID',
										'TRANSACTION_DATE_TIME'=>'TRANSACTION_DATE_TIME',
										'SYSTEM_MODIFIED_DATE'=>'SYSTEM_MODIFIED_DATE',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "SERIAL_NO = ".$this->SERIAL_NO."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun SERIAL_NO
	function getSERIAL_NO()
	{
		$this->validate();
		return $this->SERIAL_NO;
	}
	
	//retun ORDER_YEAR
	function getORDER_YEAR()
	{
		$this->validate();
		return $this->ORDER_YEAR;
	}
	
	//retun ORDER_NO
	function getORDER_NO()
	{
		$this->validate();
		return $this->ORDER_NO;
	}
	
	//retun CUSTOMER_ID
	function getCUSTOMER_ID()
	{
		$this->validate();
		return $this->CUSTOMER_ID;
	}
	
	//retun CURRENCY_ID
	function getCURRENCY_ID()
	{
		$this->validate();
		return $this->CURRENCY_ID;
	}
	
	//retun DOCUMENT_YEAR
	function getDOCUMENT_YEAR()
	{
		$this->validate();
		return $this->DOCUMENT_YEAR;
	}
	
	//retun DOCUMENT_NO
	function getDOCUMENT_NO()
	{
		$this->validate();
		return $this->DOCUMENT_NO;
	}
	
	//retun DOCUMENT_TYPE
	function getDOCUMENT_TYPE()
	{
		$this->validate();
		return $this->DOCUMENT_TYPE;
	}
	
	//retun INVOICE_YEAR
	function getINVOICE_YEAR()
	{
		$this->validate();
		return $this->INVOICE_YEAR;
	}
	
	//retun INVOICE_NO
	function getINVOICE_NO()
	{
		$this->validate();
		return $this->INVOICE_NO;
	}
	
	//retun LEDGER_ID
	function getLEDGER_ID()
	{
		$this->validate();
		return $this->LEDGER_ID;
	}
	
	//retun VALUE
	function getVALUE()
	{
		$this->validate();
		return $this->VALUE;
	}
	
	//retun COMPANY_ID
	function getCOMPANY_ID()
	{
		$this->validate();
		return $this->COMPANY_ID;
	}
	
	//retun LOCATION_ID
	function getLOCATION_ID()
	{
		$this->validate();
		return $this->LOCATION_ID;
	}
	
	//retun USER_ID
	function getUSER_ID()
	{
		$this->validate();
		return $this->USER_ID;
	}
	
	//retun TRANSACTION_DATE_TIME
	function getTRANSACTION_DATE_TIME()
	{
		$this->validate();
		return $this->TRANSACTION_DATE_TIME;
	}
	
	//retun SYSTEM_MODIFIED_DATE
	function getSYSTEM_MODIFIED_DATE()
	{
		$this->validate();
		return $this->SYSTEM_MODIFIED_DATE;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set SERIAL_NO
	function setSERIAL_NO($SERIAL_NO)
	{
		array_push($this->commitArray,'SERIAL_NO');
		$this->SERIAL_NO = $SERIAL_NO;
	}
	
	//set ORDER_YEAR
	function setORDER_YEAR($ORDER_YEAR)
	{
		array_push($this->commitArray,'ORDER_YEAR');
		$this->ORDER_YEAR = $ORDER_YEAR;
	}
	
	//set ORDER_NO
	function setORDER_NO($ORDER_NO)
	{
		array_push($this->commitArray,'ORDER_NO');
		$this->ORDER_NO = $ORDER_NO;
	}
	
	//set CUSTOMER_ID
	function setCUSTOMER_ID($CUSTOMER_ID)
	{
		array_push($this->commitArray,'CUSTOMER_ID');
		$this->CUSTOMER_ID = $CUSTOMER_ID;
	}
	
	//set CURRENCY_ID
	function setCURRENCY_ID($CURRENCY_ID)
	{
		array_push($this->commitArray,'CURRENCY_ID');
		$this->CURRENCY_ID = $CURRENCY_ID;
	}
	
	//set DOCUMENT_YEAR
	function setDOCUMENT_YEAR($DOCUMENT_YEAR)
	{
		array_push($this->commitArray,'DOCUMENT_YEAR');
		$this->DOCUMENT_YEAR = $DOCUMENT_YEAR;
	}
	
	//set DOCUMENT_NO
	function setDOCUMENT_NO($DOCUMENT_NO)
	{
		array_push($this->commitArray,'DOCUMENT_NO');
		$this->DOCUMENT_NO = $DOCUMENT_NO;
	}
	
	//set DOCUMENT_TYPE
	function setDOCUMENT_TYPE($DOCUMENT_TYPE)
	{
		array_push($this->commitArray,'DOCUMENT_TYPE');
		$this->DOCUMENT_TYPE = $DOCUMENT_TYPE;
	}
	
	//set INVOICE_YEAR
	function setINVOICE_YEAR($INVOICE_YEAR)
	{
		array_push($this->commitArray,'INVOICE_YEAR');
		$this->INVOICE_YEAR = $INVOICE_YEAR;
	}
	
	//set INVOICE_NO
	function setINVOICE_NO($INVOICE_NO)
	{
		array_push($this->commitArray,'INVOICE_NO');
		$this->INVOICE_NO = $INVOICE_NO;
	}
	
	//set LEDGER_ID
	function setLEDGER_ID($LEDGER_ID)
	{
		array_push($this->commitArray,'LEDGER_ID');
		$this->LEDGER_ID = $LEDGER_ID;
	}
	
	//set VALUE
	function setVALUE($VALUE)
	{
		array_push($this->commitArray,'VALUE');
		$this->VALUE = $VALUE;
	}
	
	//set COMPANY_ID
	function setCOMPANY_ID($COMPANY_ID)
	{
		array_push($this->commitArray,'COMPANY_ID');
		$this->COMPANY_ID = $COMPANY_ID;
	}
	
	//set LOCATION_ID
	function setLOCATION_ID($LOCATION_ID)
	{
		array_push($this->commitArray,'LOCATION_ID');
		$this->LOCATION_ID = $LOCATION_ID;
	}
	
	//set USER_ID
	function setUSER_ID($USER_ID)
	{
		array_push($this->commitArray,'USER_ID');
		$this->USER_ID = $USER_ID;
	}
	
	//set TRANSACTION_DATE_TIME
	function setTRANSACTION_DATE_TIME($TRANSACTION_DATE_TIME)
	{
		array_push($this->commitArray,'TRANSACTION_DATE_TIME');
		$this->TRANSACTION_DATE_TIME = $TRANSACTION_DATE_TIME;
	}
	
	//set SYSTEM_MODIFIED_DATE
	function setSYSTEM_MODIFIED_DATE($SYSTEM_MODIFIED_DATE)
	{
		array_push($this->commitArray,'SYSTEM_MODIFIED_DATE');
		$this->SYSTEM_MODIFIED_DATE = $SYSTEM_MODIFIED_DATE;
	}
	
	//END }
	
	//validate primary values
	private function validate()
	{
		if($this->SERIAL_NO=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($SERIAL_NO)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "SERIAL_NO='$SERIAL_NO'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($ORDER_YEAR,$ORDER_NO,$CUSTOMER_ID,$CURRENCY_ID,$DOCUMENT_YEAR,$DOCUMENT_NO,$DOCUMENT_TYPE,$INVOICE_YEAR,$INVOICE_NO,$LEDGER_ID,$VALUE,$COMPANY_ID,$LOCATION_ID,$USER_ID,$TRANSACTION_DATE_TIME){
		$data = array(
				'ORDER_YEAR'=>$ORDER_YEAR 
				,'ORDER_NO'=>$ORDER_NO 
				,'CUSTOMER_ID'=>$CUSTOMER_ID 
				,'CURRENCY_ID'=>$CURRENCY_ID 
				,'DOCUMENT_YEAR'=>$DOCUMENT_YEAR 
				,'DOCUMENT_NO'=>$DOCUMENT_NO 
				,'DOCUMENT_TYPE'=>$DOCUMENT_TYPE 
				,'INVOICE_YEAR'=>$INVOICE_YEAR 
				,'INVOICE_NO'=>$INVOICE_NO 
				,'LEDGER_ID'=>$LEDGER_ID 
				,'VALUE'=>$VALUE 
				,'COMPANY_ID'=>$COMPANY_ID 
				,'LOCATION_ID'=>$LOCATION_ID 
				,'USER_ID'=>$USER_ID 
				,'TRANSACTION_DATE_TIME'=>$TRANSACTION_DATE_TIME 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('SERIAL_NO,ORDER_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['SERIAL_NO'])
				$html .= '<option selected="selected" value="'.$row['SERIAL_NO'].'">'.$row['ORDER_YEAR'].'</option>';	
			else
				$html .= '<option value="'.$row['SERIAL_NO'].'">'.$row['ORDER_YEAR'].'</option>';	
		}
		return $html;
	}

#BEGIN - USER DEFINED FUNCTIONS {	
	public function getPaymentReceivedAmount($orderNo,$orderYear,$deci)
	{
		$cols		= " ROUND(COALESCE(SUM(VALUE),0),$deci) AS PAYMENT_RECEIVED";

		$where		= " ORDER_NO = '$orderNo'
						AND ORDER_YEAR = '$orderYear'
						AND DOCUMENT_TYPE IN('PAYRECEIVE')";
					
		$result		= $this->select($cols,$join,$where,$order=null,$limit=null);
		return mysqli_fetch_array($result);
	}
	public function getExceedReceiveDetails($invoiceNo,$invoiceYear,$company,$currency,$documentType)
	{
		$cols		= " ORDER_YEAR,
						ORDER_NO,
						CUSTOMER_ID,
						CURRENCY_ID,
						DOCUMENT_TYPE,
						INVOICE_YEAR,
						INVOICE_NO,
						LEDGER_ID,
						SUM(VALUE),
						COMPANY_ID,
						LOCATION_ID ";
		
		$where		= " INVOICE_NO = $invoiceNo 
						AND INVOICE_YEAR = $invoiceYear
						AND COMPANY_ID = $company
						AND CURRENCY_ID = $currency
						AND DOCUMENT_TYPE = '".$documentType."' 
						GROUP BY INVOICE_NO,INVOICE_YEAR,COMPANY_ID,CURRENCY_ID,DOCUMENT_TYPE ";
		
		$result		= $this->select($cols,$join=null,$where,$order=null,$limit=null);
		return mysqli_fetch_array($result);
	}
	public function getLastPayReceiveNo($invoiceNo,$invoiceYear,$company,$currency)
	{
		$cols	= " DOCUMENT_NO,
					DOCUMENT_YEAR ";
		
		$where	= " INVOICE_NO = $invoiceNo 
					AND INVOICE_YEAR = $invoiceYear
					AND COMPANY_ID = $company
					AND CURRENCY_ID = $currency
					AND DOCUMENT_TYPE = 'PAYRECEIVE' ";
		
		$order	= " SYSTEM_MODIFIED_DATE DESC ";
		
		$limit	= " 1 ";			 
		
		$result		= $this->select($cols,$join=null,$where,$order,$limit);
		return mysqli_fetch_array($result);
	}
#END	}
}
?>
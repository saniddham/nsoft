<?php
//added manual <<function set_byName(){}>>
//dont re-create this file
class sys_users{

	private $db;
	private $table= "sys_users";

	//private property
	private $intUserId;
	private $strUserName;
	private $strPassword;
	private $strFullName;
	private $intDepartmentId;
	private $strContactNo;
	private $strDesignation;
	private $strGender;
	private $COPY_PRIVILEGE_USER_ID;
	private $strEmail;
	private $strRemark;
	private $intStatus;
	private $intHigherPermision;
	private $strResetPasswordTime;
	private $PASSWORD_LAST_MODIFIED_DATE;
	private $PASSWORD_ERROR_ATTEMPTS_LEFT;
	private $ACCESS_FROM_OUTSIDE_FLAG;
	private $ACCESS_FROM_OUTSIDE_EMAIL_FLAG;
	private $PERMISION_ERROR_SQL_VISIBLE;
	private $commitArray = array();
	private $field_array = array();
	

	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('intUserId'=>'intUserId',
										'strUserName'=>'strUserName',
										'strPassword'=>'strPassword',
										'strFullName'=>'strFullName',
										'intDepartmentId'=>'intDepartmentId',
										'strContactNo'=>'strContactNo',
										'strDesignation'=>'strDesignation',
										'strGender'=>'strGender',
										'COPY_PRIVILEGE_USER_ID'=>'COPY_PRIVILEGE_USER_ID',
										'strEmail'=>'strEmail',
										'strRemark'=>'strRemark',
										'intStatus'=>'intStatus',
										'intHigherPermision'=>'intHigherPermision',
										'strResetPasswordTime'=>'strResetPasswordTime',
										'PASSWORD_LAST_MODIFIED_DATE'=>'PASSWORD_LAST_MODIFIED_DATE',
										'PASSWORD_ERROR_ATTEMPTS_LEFT'=>'PASSWORD_ERROR_ATTEMPTS_LEFT',
										'ACCESS_FROM_OUTSIDE_FLAG'=>'ACCESS_FROM_OUTSIDE_FLAG',
										'ACCESS_FROM_OUTSIDE_EMAIL_FLAG'=>'ACCESS_FROM_OUTSIDE_EMAIL_FLAG',
										'PERMISION_ERROR_SQL_VISIBLE'=>'PERMISION_ERROR_SQL_VISIBLE',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit );
		return $result;
	}

	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data);
		return $this->db->getResult();


	}

	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}

	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();
	}

	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}

	public function get_field_array(){
		return $this->field_array;
	}

	function commit($type='update')
	{
		//commit update
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{
			$data[$v] = $this->$v;
		}
		$where		= "intUserId = ".$this->intUserId."" ;
		$this->commitArray = array();

		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}

	//BEGIN - public functions for private properties {
	
	//retun intUserId
	function getintUserId()
	{
		$this->validate();
		return $this->intUserId;
	}
	
	//retun strUserName
	function getstrUserName()
	{
		$this->validate();
		return $this->strUserName;
	}
	
	//retun strPassword
	function getstrPassword()
	{
		$this->validate();
		return $this->strPassword;
	}
	
	//retun strFullName
	function getstrFullName()
	{
		$this->validate();
		return $this->strFullName;
	}
	
	//retun intDepartmentId
	function getintDepartmentId()
	{
		$this->validate();
		return $this->intDepartmentId;
	}
	
	//retun strContactNo
	function getstrContactNo()
	{
		$this->validate();
		return $this->strContactNo;
	}
	
	//retun strDesignation
	function getstrDesignation()
	{
		$this->validate();
		return $this->strDesignation;
	}
	
	//retun strGender
	function getstrGender()
	{
		$this->validate();
		return $this->strGender;
	}
	
	//retun COPY_PRIVILEGE_USER_ID
	function getCOPY_PRIVILEGE_USER_ID()
	{
		$this->validate();
		return $this->COPY_PRIVILEGE_USER_ID;
	}
	
	//retun strEmail
	function getstrEmail()
	{
		$this->validate();
		return $this->strEmail;
	}
	
	//retun strRemark
	function getstrRemark()
	{
		$this->validate();
		return $this->strRemark;
	}
	
	//retun intStatus
	function getintStatus()
	{
		$this->validate();
		return $this->intStatus;
	}
	
	//retun intHigherPermision
	function getintHigherPermision()
	{
		$this->validate();
		return $this->intHigherPermision;
	}
	
	//retun strResetPasswordTime
	function getstrResetPasswordTime()
	{
		$this->validate();
		return $this->strResetPasswordTime;
	}
	
	//retun PASSWORD_LAST_MODIFIED_DATE
	function getPASSWORD_LAST_MODIFIED_DATE()
	{
		$this->validate();
		return $this->PASSWORD_LAST_MODIFIED_DATE;
	}
	
	//retun PASSWORD_ERROR_ATTEMPTS_LEFT
	function getPASSWORD_ERROR_ATTEMPTS_LEFT()
	{
		$this->validate();
		return $this->PASSWORD_ERROR_ATTEMPTS_LEFT;
	}
	
	//retun ACCESS_FROM_OUTSIDE_FLAG
	function getACCESS_FROM_OUTSIDE_FLAG()
	{
		$this->validate();
		return $this->ACCESS_FROM_OUTSIDE_FLAG;
	}
	
	//retun ACCESS_FROM_OUTSIDE_EMAIL_FLAG
	function getACCESS_FROM_OUTSIDE_EMAIL_FLAG()
	{
		$this->validate();
		return $this->ACCESS_FROM_OUTSIDE_EMAIL_FLAG;
	}
	
	//retun PERMISION_ERROR_SQL_VISIBLE
	function getPERMISION_ERROR_SQL_VISIBLE()
	{
		$this->validate();
		return $this->PERMISION_ERROR_SQL_VISIBLE;
	}
	
	//END }

	//BEGIN - public function set {
	
	//set intUserId
	function setintUserId($intUserId)
	{
		array_push($this->commitArray,'intUserId');
		$this->intUserId = $intUserId;
	}
	
	//set strUserName
	function setstrUserName($strUserName)
	{
		array_push($this->commitArray,'strUserName');
		$this->strUserName = $strUserName;
	}
	
	//set strPassword
	function setstrPassword($strPassword)
	{
		array_push($this->commitArray,'strPassword');
		$this->strPassword = $strPassword;
	}
	
	//set strFullName
	function setstrFullName($strFullName)
	{
		array_push($this->commitArray,'strFullName');
		$this->strFullName = $strFullName;
	}
	
	//set intDepartmentId
	function setintDepartmentId($intDepartmentId)
	{
		array_push($this->commitArray,'intDepartmentId');
		$this->intDepartmentId = $intDepartmentId;
	}
	
	//set strContactNo
	function setstrContactNo($strContactNo)
	{
		array_push($this->commitArray,'strContactNo');
		$this->strContactNo = $strContactNo;
	}
	
	//set strDesignation
	function setstrDesignation($strDesignation)
	{
		array_push($this->commitArray,'strDesignation');
		$this->strDesignation = $strDesignation;
	}
	
	//set strGender
	function setstrGender($strGender)
	{
		array_push($this->commitArray,'strGender');
		$this->strGender = $strGender;
	}
	
	//set COPY_PRIVILEGE_USER_ID
	function setCOPY_PRIVILEGE_USER_ID($COPY_PRIVILEGE_USER_ID)
	{
		array_push($this->commitArray,'COPY_PRIVILEGE_USER_ID');
		$this->COPY_PRIVILEGE_USER_ID = $COPY_PRIVILEGE_USER_ID;
	}
	
	//set strEmail
	function setstrEmail($strEmail)
	{
		array_push($this->commitArray,'strEmail');
		$this->strEmail = $strEmail;
	}
	
	//set strRemark
	function setstrRemark($strRemark)
	{
		array_push($this->commitArray,'strRemark');
		$this->strRemark = $strRemark;
	}
	
	//set intStatus
	function setintStatus($intStatus)
	{
		array_push($this->commitArray,'intStatus');
		$this->intStatus = $intStatus;
	}
	
	//set intHigherPermision
	function setintHigherPermision($intHigherPermision)
	{
		array_push($this->commitArray,'intHigherPermision');
		$this->intHigherPermision = $intHigherPermision;
	}
	
	//set strResetPasswordTime
	function setstrResetPasswordTime($strResetPasswordTime)
	{
		array_push($this->commitArray,'strResetPasswordTime');
		$this->strResetPasswordTime = $strResetPasswordTime;
	}
	
	//set PASSWORD_LAST_MODIFIED_DATE
	function setPASSWORD_LAST_MODIFIED_DATE($PASSWORD_LAST_MODIFIED_DATE)
	{
		array_push($this->commitArray,'PASSWORD_LAST_MODIFIED_DATE');
		$this->PASSWORD_LAST_MODIFIED_DATE = $PASSWORD_LAST_MODIFIED_DATE;
	}
	
	//set PASSWORD_ERROR_ATTEMPTS_LEFT
	function setPASSWORD_ERROR_ATTEMPTS_LEFT($PASSWORD_ERROR_ATTEMPTS_LEFT)
	{
		array_push($this->commitArray,'PASSWORD_ERROR_ATTEMPTS_LEFT');
		$this->PASSWORD_ERROR_ATTEMPTS_LEFT = $PASSWORD_ERROR_ATTEMPTS_LEFT;
	}
	
	//set ACCESS_FROM_OUTSIDE_FLAG
	function setACCESS_FROM_OUTSIDE_FLAG($ACCESS_FROM_OUTSIDE_FLAG)
	{
		array_push($this->commitArray,'ACCESS_FROM_OUTSIDE_FLAG');
		$this->ACCESS_FROM_OUTSIDE_FLAG = $ACCESS_FROM_OUTSIDE_FLAG;
	}
	
	//set ACCESS_FROM_OUTSIDE_EMAIL_FLAG
	function setACCESS_FROM_OUTSIDE_EMAIL_FLAG($ACCESS_FROM_OUTSIDE_EMAIL_FLAG)
	{
		array_push($this->commitArray,'ACCESS_FROM_OUTSIDE_EMAIL_FLAG');
		$this->ACCESS_FROM_OUTSIDE_EMAIL_FLAG = $ACCESS_FROM_OUTSIDE_EMAIL_FLAG;
	}
	
	//set PERMISION_ERROR_SQL_VISIBLE
	function setPERMISION_ERROR_SQL_VISIBLE($PERMISION_ERROR_SQL_VISIBLE)
	{
		array_push($this->commitArray,'PERMISION_ERROR_SQL_VISIBLE');
		$this->PERMISION_ERROR_SQL_VISIBLE = $PERMISION_ERROR_SQL_VISIBLE;
	}
	
	//END }

	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->intUserId=='')
			throw new exception("Set primary values first");
		else
			return true;
	}

	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}

	public function set($intUserId)
	{
		$cols	= "*";

		$join	= NULL;

		$where	= "intUserId='$intUserId'";

		$result = $this->select($cols,$join,$where);
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}

	//insert as parameters
	public function insertRec($intUserId,$strUserName,$strPassword,$strFullName,$intDepartmentId,$strContactNo,$strDesignation,$strGender,$strEmail,$strRemark,$intStatus,$intHigherPermision,$strResetPasswordTime,$PASSWORD_LAST_MODIFIED_DATE,$PASSWORD_ERROR_ATTEMPTS_LEFT,$ACCESS_FROM_OUTSIDE_FLAG,$ACCESS_FROM_OUTSIDE_EMAIL_FLAG,$PERMISION_ERROR_SQL_VISIBLE){
		$data = array('intUserId'=>$intUserId 
				,'strUserName'=>$strUserName 
				,'strPassword'=>$strPassword 
				,'strFullName'=>$strFullName 
				,'intDepartmentId'=>$intDepartmentId 
				,'strContactNo'=>$strContactNo 
				,'strDesignation'=>$strDesignation 
				,'strGender'=>$strGender 
				,'COPY_PRIVILEGE_USER_ID'=>$COPY_PRIVILEGE_USER_ID
				,'strEmail'=>$strEmail 
				,'strRemark'=>$strRemark 
				,'intStatus'=>$intStatus 
				,'intHigherPermision'=>$intHigherPermision 
				,'strResetPasswordTime'=>$strResetPasswordTime 
				,'PASSWORD_LAST_MODIFIED_DATE'=>$PASSWORD_LAST_MODIFIED_DATE 
				,'PASSWORD_ERROR_ATTEMPTS_LEFT'=>$PASSWORD_ERROR_ATTEMPTS_LEFT 
				,'ACCESS_FROM_OUTSIDE_FLAG'=>$ACCESS_FROM_OUTSIDE_FLAG 
				,'ACCESS_FROM_OUTSIDE_EMAIL_FLAG'=>$ACCESS_FROM_OUTSIDE_EMAIL_FLAG 
				,'PERMISION_ERROR_SQL_VISIBLE'=>$PERMISION_ERROR_SQL_VISIBLE 
				);
		return $this->insert($data);
	}

	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('intUserId,strFullName',  null, $where = $where,'strFullName');
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intUserId'])
				$html .= '<option selected="selected" value="'.$row['intUserId'].'">'.$row['strFullName'].'</option>';	
			else
				$html .= '<option value="'.$row['intUserId'].'">'.$row['strFullName'].'</option>';	
		}
		return $html;
	}
	
	public function set_byName($userName)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "strUserName='$userName'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	public function getLocationWiseCombo($defaultValue=null,$locId)
	{
		$cols		= "sys_users.intUserId,sys_users.strFullName";
		
		$join 		= "INNER JOIN mst_locations_user LU
							ON LU.intUserId = sys_users.intUserId";
		
		$where		= "LU.intLocationId = $locId
						AND sys_users.intHigherPermision <> 1 ";
		
		$order		= "strFullName";
		
		$result = $this->select($cols,$join,$where,$order);
		$html = '<option value="">&nbsp;</option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intUserId'])
				$html .= '<option selected="selected" value="'.$row['intUserId'].'">'.$row['strFullName'].'</option>';	
			else
				$html .= '<option value="'.$row['intUserId'].'">'.$row['strFullName'].'</option>';	
		}
		return $html;
	}
	public function getLocationWiseUserList($defaultValue=null,$locId)
	{
		$cols		= "sys_users.intUserId,CONCAT(sys_users.strUserName,' - ',sys_users.strFullName) as USER_NAME";
		
		$join 		= "INNER JOIN mst_locations_user LU
							ON LU.intUserId = sys_users.intUserId";
		
		$where		= "LU.intLocationId = $locId
						AND sys_users.intHigherPermision <> 1 ";
		
		$order		= "strFullName";
		
		$result = $this->select($cols,$join,$where,$order);
		$html = '<option value="">&nbsp;</option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intUserId'])
				$html .= '<option selected="selected" value="'.$row['intUserId'].'">'.$row['USER_NAME'].'</option>';	
			else
				$html .= '<option value="'.$row['intUserId'].'">'.$row['USER_NAME'].'</option>';	
		}
		return $html;
	}
	//END }
}
?>
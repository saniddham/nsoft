<?php
class ware_grn_fixed_assets_registry{

	private $db;
	private $table= "ware_grn_fixed_assets_registry";

	//private property
	private $ID;
	private $GRN_NO;
	private $GRN_YEAR;
	private $SUPPLIER_ID;
	private $ITEM_ID;
	private $SERIAL_NO;
	private $WARRANTY_MONTHS;
	private $WARRANTY_END_DATE;
	private $ASSET_CODE;
	private $LOCATION_ID;
	private $DEPARTMENT_ID;
	private $SUB_DEPARTMENT_ID;
	private $PURCHASE_DATE;
	private $GRN_DATE;
	private $ITEM_PRICE;
	private $DEPRECIATE_RATE;
	private $MODIFIED_DEPRECIATE_RATE;
	private $LAST_PROCESSED_DATE;
	private $ACCUMILATE_DEPRECIATE_AMOUNT;
	private $NET_AMOUNT;
	private $CURRENCY;
	private $MANUAL_FLAG;
	private $STATUS;
	private $CREATED_DATE;
	private $CREATED_BY;
	private $MODIFIED_DATE;
	private $MODIFIED_BY;
	private $commitArray = array();
	private $field_array = array();
	

	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('ID'=>'ID',
										'GRN_NO'=>'GRN_NO',
										'GRN_YEAR'=>'GRN_YEAR',
										'SUPPLIER_ID'=>'SUPPLIER_ID',
										'ITEM_ID'=>'ITEM_ID',
										'SERIAL_NO'=>'SERIAL_NO',
										'WARRANTY_MONTHS'=>'WARRANTY_MONTHS',
										'WARRANTY_END_DATE'=>'WARRANTY_END_DATE',
										'ASSET_CODE'=>'ASSET_CODE',
										'LOCATION_ID'=>'LOCATION_ID',
										'DEPARTMENT_ID'=>'DEPARTMENT_ID',
										'SUB_DEPARTMENT_ID'=>'SUB_DEPARTMENT_ID',
										'PURCHASE_DATE'=>'PURCHASE_DATE',
										'GRN_DATE'=>'GRN_DATE',
										'ITEM_PRICE'=>'ITEM_PRICE',
										'DEPRECIATE_RATE'=>'DEPRECIATE_RATE',
										'MODIFIED_DEPRECIATE_RATE'=>'MODIFIED_DEPRECIATE_RATE',
										'LAST_PROCESSED_DATE'=>'LAST_PROCESSED_DATE',
										'ACCUMILATE_DEPRECIATE_AMOUNT'=>'ACCUMILATE_DEPRECIATE_AMOUNT',
										'NET_AMOUNT'=>'NET_AMOUNT',
										'CURRENCY'=>'CURRENCY',
										'MANUAL_FLAG'=>'MANUAL_FLAG',
										'STATUS'=>'STATUS',
										'CREATED_DATE'=>'CREATED_DATE',
										'CREATED_BY'=>'CREATED_BY',
										'MODIFIED_DATE'=>'MODIFIED_DATE',
										'MODIFIED_BY'=>'MODIFIED_BY',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit );
		return $result;
	}

	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data);
		return $this->db->getResult();


	}

	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}

	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();
	}

	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}

	public function get_field_array(){
		return $this->field_array;
	}

	function commit($type='update')
	{
		//commit update
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{
			$data[$v] = $this->$v;
		}
		$where		= "ID = ".$this->ID."" ;
		$this->commitArray = array();

		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}

	//BEGIN - public functions for private properties {
	
	//retun ID
	function getID()
	{
		$this->validate();
		return $this->ID;
	}
	
	//retun GRN_NO
	function getGRN_NO()
	{
		$this->validate();
		return $this->GRN_NO;
	}
	
	//retun GRN_YEAR
	function getGRN_YEAR()
	{
		$this->validate();
		return $this->GRN_YEAR;
	}
	
	//retun SUPPLIER_ID
	function getSUPPLIER_ID()
	{
		$this->validate();
		return $this->SUPPLIER_ID;
	}
	
	//retun ITEM_ID
	function getITEM_ID()
	{
		$this->validate();
		return $this->ITEM_ID;
	}
	
	//retun SERIAL_NO
	function getSERIAL_NO()
	{
		$this->validate();
		return $this->SERIAL_NO;
	}
	
	//retun WARRANTY_MONTHS
	function getWARRANTY_MONTHS()
	{
		$this->validate();
		return $this->WARRANTY_MONTHS;
	}
	
	//retun WARRANTY_END_DATE
	function getWARRANTY_END_DATE()
	{
		$this->validate();
		return $this->WARRANTY_END_DATE;
	}
	
	//retun ASSET_CODE
	function getASSET_CODE()
	{
		$this->validate();
		return $this->ASSET_CODE;
	}
	
	//retun LOCATION_ID
	function getLOCATION_ID()
	{
		$this->validate();
		return $this->LOCATION_ID;
	}
	
	//retun DEPARTMENT_ID
	function getDEPARTMENT_ID()
	{
		$this->validate();
		return $this->DEPARTMENT_ID;
	}
	
	//retun SUB_DEPARTMENT_ID
	function getSUB_DEPARTMENT_ID()
	{
		$this->validate();
		return $this->SUB_DEPARTMENT_ID;
	}
	
	//retun PURCHASE_DATE
	function getPURCHASE_DATE()
	{
		$this->validate();
		return $this->PURCHASE_DATE;
	}
	
	//retun GRN_DATE
	function getGRN_DATE()
	{
		$this->validate();
		return $this->GRN_DATE;
	}
	
	//retun ITEM_PRICE
	function getITEM_PRICE()
	{
		$this->validate();
		return $this->ITEM_PRICE;
	}
	
	//retun DEPRECIATE_RATE
	function getDEPRECIATE_RATE()
	{
		$this->validate();
		return $this->DEPRECIATE_RATE;
	}
	
	//retun MODIFIED_DEPRECIATE_RATE
	function getMODIFIED_DEPRECIATE_RATE()
	{
		$this->validate();
		return $this->MODIFIED_DEPRECIATE_RATE;
	}
	
	//retun LAST_PROCESSED_DATE
	function getLAST_PROCESSED_DATE()
	{
		$this->validate();
		return $this->LAST_PROCESSED_DATE;
	}
	
	//retun ACCUMILATE_DEPRECIATE_AMOUNT
	function getACCUMILATE_DEPRECIATE_AMOUNT()
	{
		$this->validate();
		return $this->ACCUMILATE_DEPRECIATE_AMOUNT;
	}
	
	//retun NET_AMOUNT
	function getNET_AMOUNT()
	{
		$this->validate();
		return $this->NET_AMOUNT;
	}
	
	//retun CURRENCY
	function getCURRENCY()
	{
		$this->validate();
		return $this->CURRENCY;
	}
	
	//retun MANUAL_FLAG
	function getMANUAL_FLAG()
	{
		$this->validate();
		return $this->MANUAL_FLAG;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//retun CREATED_DATE
	function getCREATED_DATE()
	{
		$this->validate();
		return $this->CREATED_DATE;
	}
	
	//retun CREATED_BY
	function getCREATED_BY()
	{
		$this->validate();
		return $this->CREATED_BY;
	}
	
	//retun MODIFIED_DATE
	function getMODIFIED_DATE()
	{
		$this->validate();
		return $this->MODIFIED_DATE;
	}
	
	//retun MODIFIED_BY
	function getMODIFIED_BY()
	{
		$this->validate();
		return $this->MODIFIED_BY;
	}
	
	//END }

	//BEGIN - public function set {
	
	//set ID
	function setID($ID)
	{
		array_push($this->commitArray,'ID');
		$this->ID = $ID;
	}
	
	//set GRN_NO
	function setGRN_NO($GRN_NO)
	{
		array_push($this->commitArray,'GRN_NO');
		$this->GRN_NO = $GRN_NO;
	}
	
	//set GRN_YEAR
	function setGRN_YEAR($GRN_YEAR)
	{
		array_push($this->commitArray,'GRN_YEAR');
		$this->GRN_YEAR = $GRN_YEAR;
	}
	
	//set SUPPLIER_ID
	function setSUPPLIER_ID($SUPPLIER_ID)
	{
		array_push($this->commitArray,'SUPPLIER_ID');
		$this->SUPPLIER_ID = $SUPPLIER_ID;
	}
	
	//set ITEM_ID
	function setITEM_ID($ITEM_ID)
	{
		array_push($this->commitArray,'ITEM_ID');
		$this->ITEM_ID = $ITEM_ID;
	}
	
	//set SERIAL_NO
	function setSERIAL_NO($SERIAL_NO)
	{
		array_push($this->commitArray,'SERIAL_NO');
		$this->SERIAL_NO = $SERIAL_NO;
	}
	
	//set WARRANTY_MONTHS
	function setWARRANTY_MONTHS($WARRANTY_MONTHS)
	{
		array_push($this->commitArray,'WARRANTY_MONTHS');
		$this->WARRANTY_MONTHS = $WARRANTY_MONTHS;
	}
	
	//set WARRANTY_END_DATE
	function setWARRANTY_END_DATE($WARRANTY_END_DATE)
	{
		array_push($this->commitArray,'WARRANTY_END_DATE');
		$this->WARRANTY_END_DATE = $WARRANTY_END_DATE;
	}
	
	//set ASSET_CODE
	function setASSET_CODE($ASSET_CODE)
	{
		array_push($this->commitArray,'ASSET_CODE');
		$this->ASSET_CODE = $ASSET_CODE;
	}
	
	//set LOCATION_ID
	function setLOCATION_ID($LOCATION_ID)
	{
		array_push($this->commitArray,'LOCATION_ID');
		$this->LOCATION_ID = $LOCATION_ID;
	}
	
	//set DEPARTMENT_ID
	function setDEPARTMENT_ID($DEPARTMENT_ID)
	{
		array_push($this->commitArray,'DEPARTMENT_ID');
		$this->DEPARTMENT_ID = $DEPARTMENT_ID;
	}
	
	//set SUB_DEPARTMENT_ID
	function setSUB_DEPARTMENT_ID($SUB_DEPARTMENT_ID)
	{
		array_push($this->commitArray,'SUB_DEPARTMENT_ID');
		$this->SUB_DEPARTMENT_ID = $SUB_DEPARTMENT_ID;
	}
	
	//set PURCHASE_DATE
	function setPURCHASE_DATE($PURCHASE_DATE)
	{
		array_push($this->commitArray,'PURCHASE_DATE');
		$this->PURCHASE_DATE = $PURCHASE_DATE;
	}
	
	//set GRN_DATE
	function setGRN_DATE($GRN_DATE)
	{
		array_push($this->commitArray,'GRN_DATE');
		$this->GRN_DATE = $GRN_DATE;
	}
	
	//set ITEM_PRICE
	function setITEM_PRICE($ITEM_PRICE)
	{
		array_push($this->commitArray,'ITEM_PRICE');
		$this->ITEM_PRICE = $ITEM_PRICE;
	}
	
	//set DEPRECIATE_RATE
	function setDEPRECIATE_RATE($DEPRECIATE_RATE)
	{
		array_push($this->commitArray,'DEPRECIATE_RATE');
		$this->DEPRECIATE_RATE = $DEPRECIATE_RATE;
	}
	
	//set MODIFIED_DEPRECIATE_RATE
	function setMODIFIED_DEPRECIATE_RATE($MODIFIED_DEPRECIATE_RATE)
	{
		array_push($this->commitArray,'MODIFIED_DEPRECIATE_RATE');
		$this->MODIFIED_DEPRECIATE_RATE = $MODIFIED_DEPRECIATE_RATE;
	}
	
	//set LAST_PROCESSED_DATE
	function setLAST_PROCESSED_DATE($LAST_PROCESSED_DATE)
	{
		array_push($this->commitArray,'LAST_PROCESSED_DATE');
		$this->LAST_PROCESSED_DATE = $LAST_PROCESSED_DATE;
	}
	
	//set ACCUMILATE_DEPRECIATE_AMOUNT
	function setACCUMILATE_DEPRECIATE_AMOUNT($ACCUMILATE_DEPRECIATE_AMOUNT)
	{
		array_push($this->commitArray,'ACCUMILATE_DEPRECIATE_AMOUNT');
		$this->ACCUMILATE_DEPRECIATE_AMOUNT = $ACCUMILATE_DEPRECIATE_AMOUNT;
	}
	
	//set NET_AMOUNT
	function setNET_AMOUNT($NET_AMOUNT)
	{
		array_push($this->commitArray,'NET_AMOUNT');
		$this->NET_AMOUNT = $NET_AMOUNT;
	}
	
	//set CURRENCY
	function setCURRENCY($CURRENCY)
	{
		array_push($this->commitArray,'CURRENCY');
		$this->CURRENCY = $CURRENCY;
	}
	
	//set MANUAL_FLAG
	function setMANUAL_FLAG($MANUAL_FLAG)
	{
		array_push($this->commitArray,'MANUAL_FLAG');
		$this->MANUAL_FLAG = $MANUAL_FLAG;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//set CREATED_DATE
	function setCREATED_DATE($CREATED_DATE)
	{
		array_push($this->commitArray,'CREATED_DATE');
		$this->CREATED_DATE = $CREATED_DATE;
	}
	
	//set CREATED_BY
	function setCREATED_BY($CREATED_BY)
	{
		array_push($this->commitArray,'CREATED_BY');
		$this->CREATED_BY = $CREATED_BY;
	}
	
	//set MODIFIED_DATE
	function setMODIFIED_DATE($MODIFIED_DATE)
	{
		array_push($this->commitArray,'MODIFIED_DATE');
		$this->MODIFIED_DATE = $MODIFIED_DATE;
	}
	
	//set MODIFIED_BY
	function setMODIFIED_BY($MODIFIED_BY)
	{
		array_push($this->commitArray,'MODIFIED_BY');
		$this->MODIFIED_BY = $MODIFIED_BY;
	}
	
	//END }

	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}

	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}

	public function set($ID)
	{
		$cols	= "*";

		$join	= NULL;

		$where	= "ID='$ID'";

		$result = $this->select($cols,$join,$where);
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}

	//insert as parameters
	public function insertRec($GRN_NO,$GRN_YEAR,$SUPPLIER_ID,$ITEM_ID,$SERIAL_NO,$WARRANTY_MONTHS,$WARRANTY_END_DATE,$ASSET_CODE,$LOCATION_ID,$DEPARTMENT_ID,$SUB_DEPARTMENT_ID,$PURCHASE_DATE,$GRN_DATE,$ITEM_PRICE,$DEPRECIATE_RATE,$MODIFIED_DEPRECIATE_RATE,$ACCUMILATE_DEPRECIATE_AMOUNT,$NET_AMOUNT,$CURRENCY,$MANUAL_FLAG,$STATUS,$CREATED_DATE,$CREATED_BY){
		$data = array( 'GRN_NO'=>$GRN_NO 
				,'GRN_YEAR'=>$GRN_YEAR 
				,'SUPPLIER_ID'=>$SUPPLIER_ID 
				,'ITEM_ID'=>$ITEM_ID 
				,'SERIAL_NO'=>$SERIAL_NO 
				,'WARRANTY_MONTHS'=>$WARRANTY_MONTHS 
				,'WARRANTY_END_DATE'=>$WARRANTY_END_DATE 
				,'ASSET_CODE'=>$ASSET_CODE 
				,'LOCATION_ID'=>$LOCATION_ID 
				,'DEPARTMENT_ID'=>$DEPARTMENT_ID 
				,'SUB_DEPARTMENT_ID'=>$SUB_DEPARTMENT_ID 
				,'PURCHASE_DATE'=>$PURCHASE_DATE 
				,'GRN_DATE'=>$GRN_DATE 
				,'ITEM_PRICE'=>$ITEM_PRICE 
				,'DEPRECIATE_RATE'=>$DEPRECIATE_RATE 
				,'MODIFIED_DEPRECIATE_RATE'=>$MODIFIED_DEPRECIATE_RATE 
				,'ACCUMILATE_DEPRECIATE_AMOUNT'=>$ACCUMILATE_DEPRECIATE_AMOUNT 
				,'NET_AMOUNT'=>$NET_AMOUNT 
				,'CURRENCY'=>$CURRENCY 
				,'MANUAL_FLAG'=>$MANUAL_FLAG 
				,'STATUS'=>$STATUS 
				,'CREATED_DATE'=>$CREATED_DATE 
				,'CREATED_BY'=>$CREATED_BY 
 				);
		return $this->insert($data);
	}

	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('ID,GRN_NO',  null, $where = $where);
		$html = '<option value=""></option>';
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['ID'])
				$html .= '<option selected="selected" value="'.$row['ID'].'">'.$row['GRN_NO'].'</option>';
			else
				$html .= '<option value="'.$row['ID'].'">'.$row['GRN_NO'].'</option>';
		}
		return $html;
	}
	
 	
	//END }
}
?>
<?php
class trn_sample_specialtechniques_print_width_height{

	private $db;
	private $table= "trn_sample_specialtechniques_print_width_height";

	//private property
	private $intSampleNo;
	private $intSampleYear;
	private $intRevisionNo;
	private $strComboName;
	private $strPrintName;
	private $intItem;
	private $intSerialNo;
	private $intPrintWidth;
	private $intPrintHeight;
	private $commitArray = array();
	private $field_array = array();
	

	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('intSampleNo'=>'intSampleNo',
										'intSampleYear'=>'intSampleYear',
										'intRevisionNo'=>'intRevisionNo',
										'strComboName'=>'strComboName',
										'strPrintName'=>'strPrintName',
										'intItem'=>'intItem',
										'intSerialNo'=>'intSerialNo',
										'intPrintWidth'=>'intPrintWidth',
										'intPrintHeight'=>'intPrintHeight',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit );
		return $result;
	}

	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data);
		return $this->db->getResult();


	}

	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}

	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();
	}

	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}

	public function get_field_array(){
		return $this->field_array;
	}

	function commit($type='update')
	{
		//commit update
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{
			$data[$v] = $this->$v;
		}
		$where		= "intSampleNo = ".$this->intSampleNo." and intSampleYear = ".$this->intSampleYear." and intRevisionNo = ".$this->intRevisionNo." and strComboName = ".$this->strComboName." and strPrintName = ".$this->strPrintName." and intItem = ".$this->intItem." and intSerialNo = ".$this->intSerialNo."" ;
		$this->commitArray = array();

		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}

	//BEGIN - public functions for private properties {
	
	//retun intSampleNo
	function getintSampleNo()
	{
		$this->validate();
		return $this->intSampleNo;
	}
	
	//retun intSampleYear
	function getintSampleYear()
	{
		$this->validate();
		return $this->intSampleYear;
	}
	
	//retun intRevisionNo
	function getintRevisionNo()
	{
		$this->validate();
		return $this->intRevisionNo;
	}
	
	//retun strComboName
	function getstrComboName()
	{
		$this->validate();
		return $this->strComboName;
	}
	
	//retun strPrintName
	function getstrPrintName()
	{
		$this->validate();
		return $this->strPrintName;
	}
	
	//retun intItem
	function getintItem()
	{
		$this->validate();
		return $this->intItem;
	}
	
	//retun intSerialNo
	function getintSerialNo()
	{
		$this->validate();
		return $this->intSerialNo;
	}
	
	//retun intPrintWidth
	function getintPrintWidth()
	{
		$this->validate();
		return $this->intPrintWidth;
	}
	
	//retun intPrintHeight
	function getintPrintHeight()
	{
		$this->validate();
		return $this->intPrintHeight;
	}
	
	//END }

	//BEGIN - public function set {
	
	//set intSampleNo
	function setintSampleNo($intSampleNo)
	{
		array_push($this->commitArray,'intSampleNo');
		$this->intSampleNo = $intSampleNo;
	}
	
	//set intSampleYear
	function setintSampleYear($intSampleYear)
	{
		array_push($this->commitArray,'intSampleYear');
		$this->intSampleYear = $intSampleYear;
	}
	
	//set intRevisionNo
	function setintRevisionNo($intRevisionNo)
	{
		array_push($this->commitArray,'intRevisionNo');
		$this->intRevisionNo = $intRevisionNo;
	}
	
	//set strComboName
	function setstrComboName($strComboName)
	{
		array_push($this->commitArray,'strComboName');
		$this->strComboName = $strComboName;
	}
	
	//set strPrintName
	function setstrPrintName($strPrintName)
	{
		array_push($this->commitArray,'strPrintName');
		$this->strPrintName = $strPrintName;
	}
	
	//set intItem
	function setintItem($intItem)
	{
		array_push($this->commitArray,'intItem');
		$this->intItem = $intItem;
	}
	
	//set intSerialNo
	function setintSerialNo($intSerialNo)
	{
		array_push($this->commitArray,'intSerialNo');
		$this->intSerialNo = $intSerialNo;
	}
	
	//set intPrintWidth
	function setintPrintWidth($intPrintWidth)
	{
		array_push($this->commitArray,'intPrintWidth');
		$this->intPrintWidth = $intPrintWidth;
	}
	
	//set intPrintHeight
	function setintPrintHeight($intPrintHeight)
	{
		array_push($this->commitArray,'intPrintHeight');
		$this->intPrintHeight = $intPrintHeight;
	}
	
	//END }

	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->intSampleNo=='' || $this->intSampleYear=='' || $this->intRevisionNo=='' || $this->strComboName=='' || $this->strPrintName=='' || $this->intItem=='' || $this->intSerialNo=='')
			throw new exception("Set primary values first");
		else
			return true;
	}

	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}

	public function set($intSampleNo , $intSampleYear , $intRevisionNo , $strComboName , $strPrintName , $intItem , $intSerialNo)
	{
		$cols	= "*";

		$join	= NULL;

		$where	= "intSampleNo='$intSampleNo' and intSampleYear='$intSampleYear' and intRevisionNo='$intRevisionNo' and strComboName='$strComboName' and strPrintName='$strPrintName' and intItem='$intItem' and intSerialNo='$intSerialNo'";

		$result = $this->select($cols,$join,$where);
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}

	//insert as parameters
	public function insertRec($intSampleNo,$intSampleYear,$intRevisionNo,$strComboName,$strPrintName,$intItem,$intSerialNo,$intPrintWidth,$intPrintHeight){
		$data = array('intSampleNo'=>$intSampleNo 
				,'intSampleYear'=>$intSampleYear 
				,'intRevisionNo'=>$intRevisionNo 
				,'strComboName'=>$strComboName 
				,'strPrintName'=>$strPrintName 
				,'intItem'=>$intItem 
				,'intSerialNo'=>$intSerialNo 
				,'intPrintWidth'=>$intPrintWidth 
				,'intPrintHeight'=>$intPrintHeight 
				);
		return $this->insert($data);
	}

	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('intSampleNo,intSampleYear',  null, $where = $where);
		$html = '<option value=""></option>';
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intSampleNo'])
				$html .= '<option selected="selected" value="'.$row['intSampleNo'].'">'.$row['intSampleYear'].'</option>';
			else
				$html .= '<option value="'.$row['intSampleNo'].'">'.$row['intSampleYear'].'</option>';
		}
		return $html;
	}
	
	//END }
}
?>
<?php
session_start() ;
class sys_useraccess{

	private $db;
	private $table= "sys_useraccess";

	//private property
	private $accessID;
	private $userID;
	private $login;
	private $logout;
	private $IP;
	private $IPLong;
	private $browser;
	private $browserVersion;
	private $os;
	private $commitArray = array();
	private $field_array = array();

	public $lastId = " ";

		function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('accessID'=>'accessID',
										'userID'=>'userID',
										'login'=>'login',
										'logout'=>'logout',
										'IP'=>'IP',
										'IPLong'=>'IPLong',
										'browser'=>'browser',
										'browserVersion'=>'browserVersion',
										'os'=>'os',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit );
		return $result;
	}

	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data);
		$_SESSION['lastId'] = mysqli_insert_id();
		return $this->db->getResult();


	}

	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}

	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();
	}

	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}

	public function get_field_array(){
		return $this->field_array;
	}

	function commit($type='update')
	{
		//commit update
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{
			$data[$v] = $this->$v;
		}
		$where		= "accessID = ".$this->accessID."" ;
		$this->commitArray = array();

		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}

	//BEGIN - public functions for private properties {
	
	//retun accessID
	function getaccessID()
	{
		$this->validate();
		return $this->accessID;
	}
	
	//retun userID
	function getuserID()
	{
		$this->validate();
		return $this->userID;
	}
	
	//retun login
	function getlogin()
	{
		$this->validate();
		return $this->login;
	}
	
	//retun logout
	function getlogout()
	{
		$this->validate();
		return $this->logout;
	}
	
	//retun IP
	function getIP()
	{
		$this->validate();
		return $this->IP;
	}
	
	//retun IPLong
	function getIPLong()
	{
		$this->validate();
		return $this->IPLong;
	}
	
	//retun browser
	function getbrowser()
	{
		$this->validate();
		return $this->browser;
	}
	
	//retun browserVersion
	function getbrowserVersion()
	{
		$this->validate();
		return $this->browserVersion;
	}
	
	//retun os
	function getos()
	{
		$this->validate();
		return $this->os;
	}
	
	//END }

	//BEGIN - public function set {
	
	//set accessID
	function setaccessID($accessID)
	{
		array_push($this->commitArray,'accessID');
		$this->accessID = $accessID;
	}
	
	//set userID
	function setuserID($userID)
	{
		array_push($this->commitArray,'userID');
		$this->userID = $userID;
	}
	
	//set login
	function setlogin($login)
	{
		array_push($this->commitArray,'login');
		$this->login = $login;
	}
	
	//set logout
	function setlogout($logout)
	{
		array_push($this->commitArray,'logout');
		$this->logout = $logout;
	}
	
	//set IP
	function setIP($IP)
	{
		array_push($this->commitArray,'IP');
		$this->IP = $IP;
	}
	
	//set IPLong
	function setIPLong($IPLong)
	{
		array_push($this->commitArray,'IPLong');
		$this->IPLong = $IPLong;
	}
	
	//set browser
	function setbrowser($browser)
	{
		array_push($this->commitArray,'browser');
		$this->browser = $browser;
	}
	
	//set browserVersion
	function setbrowserVersion($browserVersion)
	{
		array_push($this->commitArray,'browserVersion');
		$this->browserVersion = $browserVersion;
	}
	
	//set os
	function setos($os)
	{
		array_push($this->commitArray,'os');
		$this->os = $os;
	}
	
	//END }

	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->accessID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}

	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}

	public function set($accessID)
	{
		$cols	= "*";

		$join	= NULL;

		$where	= "accessID='$accessID'";

		$result = $this->select($cols,$join,$where);
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}

	//insert as parameters
	public function insertRec($userID,$IP,$IPLong,$browser,$browserVersion,$os){
		$data = array('userID'=>$userID
				,'IP'=>$IP 
				,'IPLong'=>$IPLong 
				,'browser'=>$browser 
				,'browserVersion'=>$browserVersion 
				,'os'=>$os 
				);

		return $this->insert($data);
	}

	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('accessID,userID',  null, $where = $where);
		$html = '<option value=""></option>';
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['accessID'])
				$html .= '<option selected="selected" value="'.$row['accessID'].'">'.$row['userID'].'</option>';
			else
				$html .= '<option value="'.$row['accessID'].'">'.$row['userID'].'</option>';
		}
		return $html;
	}
	
	//END }
}
?>
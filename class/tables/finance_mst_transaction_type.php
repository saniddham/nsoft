<?php
class finance_mst_transaction_type{

	private $db;
	private $table= "finance_mst_transaction_type";

	//private property
	private $TYPE_ID;
	private $TYPE_NAME;
	private $DOCUMENT_TYPE;
	private $TRANSACTION_CATEGORY;
	private $CHART_OF_ACCOUNT_FIELD;
	private $STATUS;
	private $commitArray = array();
	private $field_array = array();
	

	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('TYPE_ID'=>'TYPE_ID',
										'TYPE_NAME'=>'TYPE_NAME',
										'DOCUMENT_TYPE'=>'DOCUMENT_TYPE',
										'TRANSACTION_CATEGORY'=>'TRANSACTION_CATEGORY',
										'CHART_OF_ACCOUNT_FIELD'=>'CHART_OF_ACCOUNT_FIELD',
										'STATUS'=>'STATUS',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit );
		return $result;
	}

	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data);
		return $this->db->getResult();


	}

	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}

	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();
	}

	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}

	public function get_field_array(){
		return $this->field_array;
	}

	function commit($type='update')
	{
		//commit update
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{
			$data[$v] = $this->$v;
		}
		$where		= "TYPE_ID = ".$this->TYPE_ID."" ;
		$this->commitArray = array();

		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}

	//BEGIN - public functions for private properties {
	
	//retun TYPE_ID
	function getTYPE_ID()
	{
		$this->validate();
		return $this->TYPE_ID;
	}
	
	//retun TYPE_NAME
	function getTYPE_NAME()
	{
		$this->validate();
		return $this->TYPE_NAME;
	}
	
	//retun DOCUMENT_TYPE
	function getDOCUMENT_TYPE()
	{
		$this->validate();
		return $this->DOCUMENT_TYPE;
	}
	
	//retun TRANSACTION_CATEGORY
	function getTRANSACTION_CATEGORY()
	{
		$this->validate();
		return $this->TRANSACTION_CATEGORY;
	}
	
	//retun CHART_OF_ACCOUNT_FIELD
	function getCHART_OF_ACCOUNT_FIELD()
	{
		$this->validate();
		return $this->CHART_OF_ACCOUNT_FIELD;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//END }

	//BEGIN - public function set {
	
	//set TYPE_ID
	function setTYPE_ID($TYPE_ID)
	{
		array_push($this->commitArray,'TYPE_ID');
		$this->TYPE_ID = $TYPE_ID;
	}
	
	//set TYPE_NAME
	function setTYPE_NAME($TYPE_NAME)
	{
		array_push($this->commitArray,'TYPE_NAME');
		$this->TYPE_NAME = $TYPE_NAME;
	}
	
	//set DOCUMENT_TYPE
	function setDOCUMENT_TYPE($DOCUMENT_TYPE)
	{
		array_push($this->commitArray,'DOCUMENT_TYPE');
		$this->DOCUMENT_TYPE = $DOCUMENT_TYPE;
	}
	
	//set TRANSACTION_CATEGORY
	function setTRANSACTION_CATEGORY($TRANSACTION_CATEGORY)
	{
		array_push($this->commitArray,'TRANSACTION_CATEGORY');
		$this->TRANSACTION_CATEGORY = $TRANSACTION_CATEGORY;
	}
	
	//set CHART_OF_ACCOUNT_FIELD
	function setCHART_OF_ACCOUNT_FIELD($CHART_OF_ACCOUNT_FIELD)
	{
		array_push($this->commitArray,'CHART_OF_ACCOUNT_FIELD');
		$this->CHART_OF_ACCOUNT_FIELD = $CHART_OF_ACCOUNT_FIELD;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//END }

	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->TYPE_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}

	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}

	public function set($TYPE_ID)
	{
		$cols	= "*";

		$join	= NULL;

		$where	= "TYPE_ID='$TYPE_ID'";

		$result = $this->select($cols,$join,$where);
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}

	//insert as parameters
	public function insertRec($TYPE_ID,$TYPE_NAME,$DOCUMENT_TYPE,$TRANSACTION_CATEGORY,$CHART_OF_ACCOUNT_FIELD,$STATUS){
		$data = array('TYPE_ID'=>$TYPE_ID 
				,'TYPE_NAME'=>$TYPE_NAME 
				,'DOCUMENT_TYPE'=>$DOCUMENT_TYPE 
				,'TRANSACTION_CATEGORY'=>$TRANSACTION_CATEGORY 
				,'CHART_OF_ACCOUNT_FIELD'=>$CHART_OF_ACCOUNT_FIELD 
				,'STATUS'=>$STATUS 
				);
		return $this->insert($data);
	}

	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('TYPE_ID,TYPE_NAME',  null, $where = $where,$order='TYPE_NAME');
		$html = '<option value=""></option>';
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['TYPE_ID'])
				$html .= '<option selected="selected" value="'.$row['TYPE_ID'].'">'.$row['TYPE_NAME'].'</option>';
			else
				$html .= '<option value="'.$row['TYPE_ID'].'">'.$row['TYPE_NAME'].'</option>';
		}
		return $html;
	}
	
	//END }
}
?>
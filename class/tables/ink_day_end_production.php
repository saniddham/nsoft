<?php
//THIS CLASS MANUALLY EDITED Y LAHIRU LINE NO 237
class ink_day_end_production{
 
	private $db;
	private $table= "ink_day_end_production";
	
	//private property
	private $ID;
	private $DATE;
	private $LOCATION_ID;
	private $APPROVE_STATUS;
	private $APPROVED_BY;
	private $CREATED_DATE;
	private $COLOR_ROOM_PROCESS_ID;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('ID'=>'ID',
										'DATE'=>'DATE',
										'LOCATION_ID'=>'LOCATION_ID',
										'APPROVE_STATUS'=>'APPROVE_STATUS',
										'APPROVED_BY'=>'APPROVED_BY',
										'CREATED_DATE'=>'CREATED_DATE',
										'COLOR_ROOM_PROCESS_ID'=>'COLOR_ROOM_PROCESS_ID',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "ID = ".$this->ID."" ;
		//unset($this->commitArray);
		$this->commitArray	= array();
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun ID
	function getID()
	{
		$this->validate();
		return $this->ID;
	}
	
	//retun DATE
	function getDATE()
	{
		$this->validate();
		return $this->DATE;
	}
	
	//retun LOCATION_ID
	function getLOCATION_ID()
	{
		$this->validate();
		return $this->LOCATION_ID;
	}
	
	//retun APPROVE_STATUS
	function getAPPROVE_STATUS()
	{
		$this->validate();
		return $this->APPROVE_STATUS;
	}
	
	//retun APPROVED_BY
	function getAPPROVED_BY()
	{
		$this->validate();
		return $this->APPROVED_BY;
	}
	
	//retun CREATED_DATE
	function getCREATED_DATE()
	{
		$this->validate();
		return $this->CREATED_DATE;
	}
	
	//retun COLOR_ROOM_PROCESS_ID
	function getCOLOR_ROOM_PROCESS_ID()
	{
		$this->validate();
		return $this->COLOR_ROOM_PROCESS_ID;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set ID
	function setID($ID)
	{
		array_push($this->commitArray,'ID');
		$this->ID = $ID;
	}
	
	//set DATE
	function setDATE($DATE)
	{
		array_push($this->commitArray,'DATE');
		$this->DATE = $DATE;
	}
	
	//set LOCATION_ID
	function setLOCATION_ID($LOCATION_ID)
	{
		array_push($this->commitArray,'LOCATION_ID');
		$this->LOCATION_ID = $LOCATION_ID;
	}
	
	//set APPROVE_STATUS
	function setAPPROVE_STATUS($APPROVE_STATUS)
	{
		array_push($this->commitArray,'APPROVE_STATUS');
		$this->APPROVE_STATUS = $APPROVE_STATUS;
	}
	
	//set APPROVED_BY
	function setAPPROVED_BY($APPROVED_BY)
	{
		array_push($this->commitArray,'APPROVED_BY');
		$this->APPROVED_BY = $APPROVED_BY;
	}
	
	//set CREATED_DATE
	function setCREATED_DATE($CREATED_DATE)
	{
		array_push($this->commitArray,'CREATED_DATE');
		$this->CREATED_DATE = $CREATED_DATE;
	}
	
	//set COLOR_ROOM_PROCESS_ID
	function setCOLOR_ROOM_PROCESS_ID($COLOR_ROOM_PROCESS_ID)
	{
		array_push($this->commitArray,'COLOR_ROOM_PROCESS_ID');
		$this->COLOR_ROOM_PROCESS_ID = $COLOR_ROOM_PROCESS_ID;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "ID='$ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($DATE,$LOCATION_ID,$APPROVE_STATUS,$APPROVED_BY,$CREATED_DATE,$COLOR_ROOM_PROCESS_ID){
		$data = array(
				 'DATE'=>$DATE 
				,'LOCATION_ID'=>$LOCATION_ID 
				,'APPROVE_STATUS'=>$APPROVE_STATUS 
				,'APPROVED_BY'=>$APPROVED_BY 
				,'CREATED_DATE'=>$CREATED_DATE 
				,'COLOR_ROOM_PROCESS_ID'=>$COLOR_ROOM_PROCESS_ID 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('ID,LOCATION_ID',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['ID'])
				$html .= '<option selected="selected" value="'.$row['ID'].'">'.$row['LOCATION_ID'].'</option>';	
			else
				$html .= '<option value="'.$row['ID'].'">'.$row['LOCATION_ID'].'</option>';	
		}
		return $html;
	}	
	//END }
#BEGIN - USER DEFINED FUNCTIONS {
	public function checkPendingProcessAvailable($date,$locationId)
	{
		$cols	= "ID";

		$where	= "DATE(CREATED_DATE)<= '$date'
					AND LOCATION_ID = $locationId
					AND COLOR_ROOM_PROCESS_ID IS NULL ";
		$result = $this->select($cols,$join,$where,$order = null, $limit = null);
		$row	= mysqli_fetch_array($result);
		if($row>0)
			return true;
		else
			return false;
	}
	
	public function getPendingProcess($date,$locationId)
	{
		$cols	= "ID";

		$where	= "DATE(CREATED_DATE)<= '$date'
					AND LOCATION_ID = $locationId
					AND COLOR_ROOM_PROCESS_ID IS NULL ";
		return $this->select($cols,$join,$where,$order = null, $limit = null);
	}
#END 	}
}
?>
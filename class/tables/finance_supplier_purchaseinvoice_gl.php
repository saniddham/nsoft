<?php
class finance_supplier_purchaseinvoice_gl{
 
	private $db;
	private $table= "finance_supplier_purchaseinvoice_gl";
	
	//private property
	private $PURCHASE_INVOICE_NO;
	private $PURCHASE_INVOICE_YEAR;
	private $GLACCOUNT_ID;
	private $AMOUNT;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('PURCHASE_INVOICE_NO'=>'PURCHASE_INVOICE_NO',
										'PURCHASE_INVOICE_YEAR'=>'PURCHASE_INVOICE_YEAR',
										'GLACCOUNT_ID'=>'GLACCOUNT_ID',
										'AMOUNT'=>'AMOUNT',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "PURCHASE_INVOICE_NO = ".$this->PURCHASE_INVOICE_NO." and PURCHASE_INVOICE_YEAR = ".$this->PURCHASE_INVOICE_YEAR." and GLACCOUNT_ID = ".$this->GLACCOUNT_ID."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun PURCHASE_INVOICE_NO
	function getPURCHASE_INVOICE_NO()
	{
		$this->validate();
		return $this->PURCHASE_INVOICE_NO;
	}
	
	//retun PURCHASE_INVOICE_YEAR
	function getPURCHASE_INVOICE_YEAR()
	{
		$this->validate();
		return $this->PURCHASE_INVOICE_YEAR;
	}
	
	//retun GLACCOUNT_ID
	function getGLACCOUNT_ID()
	{
		$this->validate();
		return $this->GLACCOUNT_ID;
	}
	
	//retun AMOUNT
	function getAMOUNT()
	{
		$this->validate();
		return $this->AMOUNT;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set PURCHASE_INVOICE_NO
	function setPURCHASE_INVOICE_NO($PURCHASE_INVOICE_NO)
	{
		array_push($this->commitArray,'PURCHASE_INVOICE_NO');
		$this->PURCHASE_INVOICE_NO = $PURCHASE_INVOICE_NO;
	}
	
	//set PURCHASE_INVOICE_YEAR
	function setPURCHASE_INVOICE_YEAR($PURCHASE_INVOICE_YEAR)
	{
		array_push($this->commitArray,'PURCHASE_INVOICE_YEAR');
		$this->PURCHASE_INVOICE_YEAR = $PURCHASE_INVOICE_YEAR;
	}
	
	//set GLACCOUNT_ID
	function setGLACCOUNT_ID($GLACCOUNT_ID)
	{
		array_push($this->commitArray,'GLACCOUNT_ID');
		$this->GLACCOUNT_ID = $GLACCOUNT_ID;
	}
	
	//set AMOUNT
	function setAMOUNT($AMOUNT)
	{
		array_push($this->commitArray,'AMOUNT');
		$this->AMOUNT = $AMOUNT;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->PURCHASE_INVOICE_NO=='' || $this->PURCHASE_INVOICE_YEAR=='' || $this->GLACCOUNT_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($PURCHASE_INVOICE_NO , $PURCHASE_INVOICE_YEAR , $GLACCOUNT_ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "PURCHASE_INVOICE_NO='$PURCHASE_INVOICE_NO' and PURCHASE_INVOICE_YEAR='$PURCHASE_INVOICE_YEAR' and GLACCOUNT_ID='$GLACCOUNT_ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($PURCHASE_INVOICE_NO,$PURCHASE_INVOICE_YEAR,$GLACCOUNT_ID,$AMOUNT){
		$data = array('PURCHASE_INVOICE_NO'=>$PURCHASE_INVOICE_NO 
				,'PURCHASE_INVOICE_YEAR'=>$PURCHASE_INVOICE_YEAR 
				,'GLACCOUNT_ID'=>$GLACCOUNT_ID 
				,'AMOUNT'=>$AMOUNT 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('PURCHASE_INVOICE_NO,PURCHASE_INVOICE_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['PURCHASE_INVOICE_NO'])
				$html .= '<option selected="selected" value="'.$row['PURCHASE_INVOICE_NO'].'">'.$row['PURCHASE_INVOICE_YEAR'].'</option>';	
			else
				$html .= '<option value="'.$row['PURCHASE_INVOICE_NO'].'">'.$row['PURCHASE_INVOICE_YEAR'].'</option>';	
		}
		return $html;
	}
	
	public function getInvoiceWiseGLDetails($invoiceNo,$invoiceYear)
	{
		$cols	= " GLACCOUNT_ID,AMOUNT,
					(
					SELECT GROUP_CONCAT(COAT.TAX_ID)
					FROM finance_mst_chartofaccount_tax AS COAT
					WHERE COAT.CHART_OF_ACCOUNT_ID=finance_supplier_purchaseinvoice_gl.GLACCOUNT_ID
					) AS TAX_ID ";
		
		$where	= " PURCHASE_INVOICE_NO = '".$invoiceNo."' AND 
					PURCHASE_INVOICE_YEAR = '".$invoiceYear."' ";
		
		return $this->select($cols,$join=null,$where);
	}
	//END }
}
?>
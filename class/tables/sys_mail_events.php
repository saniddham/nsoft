<?php
class sys_mail_events{
 
	private $db;
	private $table= "sys_mail_events";
	
	//private property
	private $intMailIventId;
	private $intModuleCode;
	private $strMailEventName;
	private $intStatus;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('intMailIventId'=>'intMailIventId',
										'intModuleCode'=>'intModuleCode',
										'strMailEventName'=>'strMailEventName',
										'intStatus'=>'intStatus',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "intMailIventId = ".$this->intMailIventId."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun intMailIventId
	function getintMailIventId()
	{
		$this->validate();
		return $this->intMailIventId;
	}
	
	//retun intModuleCode
	function getintModuleCode()
	{
		$this->validate();
		return $this->intModuleCode;
	}
	
	//retun strMailEventName
	function getstrMailEventName()
	{
		$this->validate();
		return $this->strMailEventName;
	}
	
	//retun intStatus
	function getintStatus()
	{
		$this->validate();
		return $this->intStatus;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set intMailIventId
	function setintMailIventId($intMailIventId)
	{
		array_push($this->commitArray,'intMailIventId');
		$this->intMailIventId = $intMailIventId;
	}
	
	//set intModuleCode
	function setintModuleCode($intModuleCode)
	{
		array_push($this->commitArray,'intModuleCode');
		$this->intModuleCode = $intModuleCode;
	}
	
	//set strMailEventName
	function setstrMailEventName($strMailEventName)
	{
		array_push($this->commitArray,'strMailEventName');
		$this->strMailEventName = $strMailEventName;
	}
	
	//set intStatus
	function setintStatus($intStatus)
	{
		array_push($this->commitArray,'intStatus');
		$this->intStatus = $intStatus;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->intMailIventId=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($intMailIventId)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "intMailIventId='$intMailIventId'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($intMailIventId,$intModuleCode,$strMailEventName,$intStatus){
		$data = array('intMailIventId'=>$intMailIventId 
				,'intModuleCode'=>$intModuleCode 
				,'strMailEventName'=>$strMailEventName 
				,'intStatus'=>$intStatus 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('intMailIventId,intModuleCode',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intMailIventId'])
				$html .= '<option selected="selected" value="'.$row['intMailIventId'].'">'.$row['intModuleCode'].'</option>';	
			else
				$html .= '<option value="'.$row['intMailIventId'].'">'.$row['intModuleCode'].'</option>';	
		}
		return $html;
	}
	
	//END }
}
?>
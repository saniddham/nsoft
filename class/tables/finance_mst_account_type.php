<?php
class finance_mst_account_type{
 
	private $db;
	private $table= "finance_mst_account_type";
	
	//private property
	private $FINANCE_TYPE_ID;
	private $FINANCE_TYPE_CODE;
	private $FINANCE_TYPE_NAME;
	private $STATUS;
	private $TRANSACTION_ADD_TYPE;
	private $CREATED_BY;
	private $CREATED_DATE;
	private $LAST_MODIFYED_BY;
	private $LAST_MODIFYED_DATE;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('FINANCE_TYPE_ID'=>'FINANCE_TYPE_ID',
										'FINANCE_TYPE_CODE'=>'FINANCE_TYPE_CODE',
										'FINANCE_TYPE_NAME'=>'FINANCE_TYPE_NAME',
										'STATUS'=>'STATUS',
										'TRANSACTION_ADD_TYPE'=>'TRANSACTION_ADD_TYPE',
										'CREATED_BY'=>'CREATED_BY',
										'CREATED_DATE'=>'CREATED_DATE',
										'LAST_MODIFYED_BY'=>'LAST_MODIFYED_BY',
										'LAST_MODIFYED_DATE'=>'LAST_MODIFYED_DATE',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "FINANCE_TYPE_ID = ".$this->FINANCE_TYPE_ID."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun FINANCE_TYPE_ID
	function getFINANCE_TYPE_ID()
	{
		$this->validate();
		return $this->FINANCE_TYPE_ID;
	}
	
	//retun FINANCE_TYPE_CODE
	function getFINANCE_TYPE_CODE()
	{
		$this->validate();
		return $this->FINANCE_TYPE_CODE;
	}
	
	//retun FINANCE_TYPE_NAME
	function getFINANCE_TYPE_NAME()
	{
		$this->validate();
		return $this->FINANCE_TYPE_NAME;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//retun TRANSACTION_ADD_TYPE
	function getTRANSACTION_ADD_TYPE()
	{
		$this->validate();
		return $this->TRANSACTION_ADD_TYPE;
	}
	
	//retun CREATED_BY
	function getCREATED_BY()
	{
		$this->validate();
		return $this->CREATED_BY;
	}
	
	//retun CREATED_DATE
	function getCREATED_DATE()
	{
		$this->validate();
		return $this->CREATED_DATE;
	}
	
	//retun LAST_MODIFYED_BY
	function getLAST_MODIFYED_BY()
	{
		$this->validate();
		return $this->LAST_MODIFYED_BY;
	}
	
	//retun LAST_MODIFYED_DATE
	function getLAST_MODIFYED_DATE()
	{
		$this->validate();
		return $this->LAST_MODIFYED_DATE;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set FINANCE_TYPE_ID
	function setFINANCE_TYPE_ID($FINANCE_TYPE_ID)
	{
		array_push($this->commitArray,'FINANCE_TYPE_ID');
		$this->FINANCE_TYPE_ID = $FINANCE_TYPE_ID;
	}
	
	//set FINANCE_TYPE_CODE
	function setFINANCE_TYPE_CODE($FINANCE_TYPE_CODE)
	{
		array_push($this->commitArray,'FINANCE_TYPE_CODE');
		$this->FINANCE_TYPE_CODE = $FINANCE_TYPE_CODE;
	}
	
	//set FINANCE_TYPE_NAME
	function setFINANCE_TYPE_NAME($FINANCE_TYPE_NAME)
	{
		array_push($this->commitArray,'FINANCE_TYPE_NAME');
		$this->FINANCE_TYPE_NAME = $FINANCE_TYPE_NAME;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//set TRANSACTION_ADD_TYPE
	function setTRANSACTION_ADD_TYPE($TRANSACTION_ADD_TYPE)
	{
		array_push($this->commitArray,'TRANSACTION_ADD_TYPE');
		$this->TRANSACTION_ADD_TYPE = $TRANSACTION_ADD_TYPE;
	}
	
	//set CREATED_BY
	function setCREATED_BY($CREATED_BY)
	{
		array_push($this->commitArray,'CREATED_BY');
		$this->CREATED_BY = $CREATED_BY;
	}
	
	//set CREATED_DATE
	function setCREATED_DATE($CREATED_DATE)
	{
		array_push($this->commitArray,'CREATED_DATE');
		$this->CREATED_DATE = $CREATED_DATE;
	}
	
	//set LAST_MODIFYED_BY
	function setLAST_MODIFYED_BY($LAST_MODIFYED_BY)
	{
		array_push($this->commitArray,'LAST_MODIFYED_BY');
		$this->LAST_MODIFYED_BY = $LAST_MODIFYED_BY;
	}
	
	//set LAST_MODIFYED_DATE
	function setLAST_MODIFYED_DATE($LAST_MODIFYED_DATE)
	{
		array_push($this->commitArray,'LAST_MODIFYED_DATE');
		$this->LAST_MODIFYED_DATE = $LAST_MODIFYED_DATE;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->FINANCE_TYPE_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($FINANCE_TYPE_ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "FINANCE_TYPE_ID='$FINANCE_TYPE_ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($FINANCE_TYPE_ID,$FINANCE_TYPE_CODE,$FINANCE_TYPE_NAME,$STATUS,$TRANSACTION_ADD_TYPE,$CREATED_BY,$CREATED_DATE,$LAST_MODIFYED_BY,$LAST_MODIFYED_DATE){
		$data = array('FINANCE_TYPE_ID'=>$FINANCE_TYPE_ID 
				,'FINANCE_TYPE_CODE'=>$FINANCE_TYPE_CODE 
				,'FINANCE_TYPE_NAME'=>$FINANCE_TYPE_NAME 
				,'STATUS'=>$STATUS 
				,'TRANSACTION_ADD_TYPE'=>$TRANSACTION_ADD_TYPE 
				,'CREATED_BY'=>$CREATED_BY 
				,'CREATED_DATE'=>$CREATED_DATE 
				,'LAST_MODIFYED_BY'=>$LAST_MODIFYED_BY 
				,'LAST_MODIFYED_DATE'=>$LAST_MODIFYED_DATE 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('FINANCE_TYPE_ID,FINANCE_TYPE_NAME',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['FINANCE_TYPE_ID'])
				$html .= '<option selected="selected" value="'.$row['FINANCE_TYPE_ID'].'">'.$row['FINANCE_TYPE_NAME'].'</option>';	
			else
				$html .= '<option value="'.$row['FINANCE_TYPE_ID'].'">'.$row['FINANCE_TYPE_NAME'].'</option>';	
		}
		return $html;
	}
	
	//END }
}
?>
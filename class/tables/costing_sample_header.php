<?php
class costing_sample_header{

	private $db;
	private $table= "costing_sample_header";

	//private property
	private $SAMPLE_NO;
	private $SAMPLE_YEAR;
	private $REVISION;
	private $COMBO;
	private $PRINT;
	private $PRODUCTION_LOCATION;
	private $UPS;
	private $ESTIMATED_ORDER_QTY;
	private $NO_OF_COLOURS;
	private $SHOTS;
	private $COST_PER_SHOT;
	private $INK_COST;
	private $SPECIAL_RM_COST;
	private $PROCESS_COST;
	private $SHOT_COST;
	private $OTHER_COST;
	private $OTHER_COST_REMARKS;
	private $TOTAL_COST;
	private $MARGIN_PERCENTAGE;
	private $MARGIN;
	private $SCREENLINE_PRICE;
	private $TARGET_PRICE;
	private $APPROVED_PRICE;
	private $COPPIED_FLAG;
	private $COPPIED_TYPE;
	private $COPPIED_FROM_SAMPLE_NO;
	private $COPPIED_FROM_SAMPLE_YEAR;
	private $COPPIED_FROM_REVISION;
	private $COPPIED_FROM_COMBO;
	private $COPPIED_FROM_PRINT;
	private $STATUS;
	private $APPROVE_LEVELS;
	private $SAVED_BY;
	private $SAVED_TIME;
    private $QTY_PRICES;
	private $commitArray = array();
	private $field_array = array();
	

	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('SAMPLE_NO'=>'SAMPLE_NO',
										'SAMPLE_YEAR'=>'SAMPLE_YEAR',
										'REVISION'=>'REVISION',
										'COMBO'=>'COMBO',
										'PRINT'=>'PRINT',
										'PRODUCTION_LOCATION'=>'PRODUCTION_LOCATION',
										'UPS'=>'UPS',
										'ESTIMATED_ORDER_QTY'=>'ESTIMATED_ORDER_QTY',
										'NO_OF_COLOURS'=>'NO_OF_COLOURS',
										'SHOTS'=>'SHOTS',
										'COST_PER_SHOT'=>'COST_PER_SHOT',
										'INK_COST'=>'INK_COST',
										'SPECIAL_RM_COST'=>'SPECIAL_RM_COST',
										'PROCESS_COST'=>'PROCESS_COST',
										'SHOT_COST'=>'SHOT_COST',
										'OTHER_COST'=>'OTHER_COST',
										'OTHER_COST_REMARKS'=>'OTHER_COST_REMARKS',
										'TOTAL_COST'=>'TOTAL_COST',
										'MARGIN_PERCENTAGE'=>'MARGIN_PERCENTAGE',
										'MARGIN'=>'MARGIN',
										'SCREENLINE_PRICE'=>'SCREENLINE_PRICE',
										'TARGET_PRICE'=>'TARGET_PRICE',
										'APPROVED_PRICE'=>'APPROVED_PRICE',
										'COPPIED_FLAG'=>'COPPIED_FLAG',
										'COPPIED_TYPE'=>'COPPIED_TYPE',
										'COPPIED_FROM_SAMPLE_NO'=>'COPPIED_FROM_SAMPLE_NO',
										'COPPIED_FROM_SAMPLE_YEAR'=>'COPPIED_FROM_SAMPLE_YEAR',
										'COPPIED_FROM_REVISION'=>'COPPIED_FROM_REVISION',
										'COPPIED_FROM_COMBO'=>'COPPIED_FROM_COMBO',
										'COPPIED_FROM_PRINT'=>'COPPIED_FROM_PRINT',
										'STATUS'=>'STATUS',
										'APPROVE_LEVELS'=>'APPROVE_LEVELS',
										'SAVED_BY'=>'SAVED_BY',
										'SAVED_TIME'=>'SAVED_TIME',
                                        'QTY_PRICES'=>'QTY_PRICES',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit );
		return $result;
	}

	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data);
		return $this->db->getResult();


	}

	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}

	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();
	}

	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}

	public function get_field_array(){
		return $this->field_array;
	}

	function commit($type='update')
	{
		//commit update
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{
			$data[$v] = $this->$v;
		}
		$where		= "SAMPLE_NO = ".$this->SAMPLE_NO." and SAMPLE_YEAR = ".$this->SAMPLE_YEAR." and REVISION = ".$this->REVISION." and COMBO = '".$this->COMBO."' and PRINT = '".$this->PRINT."'" ;
		$this->commitArray = array();

		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}

	//BEGIN - public functions for private properties {
	
	//retun SAMPLE_NO
	function getSAMPLE_NO()
	{
		$this->validate();
		return $this->SAMPLE_NO;
	}
	
	//retun SAMPLE_YEAR
	function getSAMPLE_YEAR()
	{
		$this->validate();
		return $this->SAMPLE_YEAR;
	}
	
	//retun REVISION
	function getREVISION()
	{
		$this->validate();
		return $this->REVISION;
	}
	
	//retun COMBO
	function getCOMBO()
	{
		$this->validate();
		return $this->COMBO;
	}
	
	//retun PRINT
	function getPRINT()
	{
		$this->validate();
		return $this->PRINT;
	}
	
	//retun PRODUCTION_LOCATION
	function getPRODUCTION_LOCATION()
	{
		$this->validate();
		return $this->PRODUCTION_LOCATION;
	}
	
	//retun UPS
	function getUPS()
	{
		$this->validate();
		return $this->UPS;
	}
	
	//retun ESTIMATED_ORDER_QTY
	function getESTIMATED_ORDER_QTY()
	{
		$this->validate();
		return $this->ESTIMATED_ORDER_QTY;
	}
	
	//retun NO_OF_COLOURS
	function getNO_OF_COLOURS()
	{
		$this->validate();
		return $this->NO_OF_COLOURS;
	}
	
	//retun SHOTS
	function getSHOTS()
	{
		$this->validate();
		return $this->SHOTS;
	}
	
	//retun COST_PER_SHOT
	function getCOST_PER_SHOT()
	{
		$this->validate();
		return $this->COST_PER_SHOT;
	}
	
	//retun INK_COST
	function getINK_COST()
	{
		$this->validate();
		return $this->INK_COST;
	}
	
	//retun SPECIAL_RM_COST
	function getSPECIAL_RM_COST()
	{
		$this->validate();
		return $this->SPECIAL_RM_COST;
	}
	
	//retun PROCESS_COST
	function getPROCESS_COST()
	{
		$this->validate();
		return $this->PROCESS_COST;
	}
	
	//retun SHOT_COST
	function getSHOT_COST()
	{
		$this->validate();
		return $this->SHOT_COST;
	}
	
	//retun OTHER_COST
	function getOTHER_COST()
	{
		$this->validate();
		return $this->OTHER_COST;
	}
	
	//retun OTHER_COST_REMARKS
	function getOTHER_COST_REMARKS()
	{
		$this->validate();
		return $this->OTHER_COST_REMARKS;
	}
	
	//retun TOTAL_COST
	function getTOTAL_COST()
	{
		$this->validate();
		return $this->TOTAL_COST;
	}
	
	//retun MARGIN_PERCENTAGE
	function getMARGIN_PERCENTAGE()
	{
		$this->validate();
		return $this->MARGIN_PERCENTAGE;
	}
	
	//retun MARGIN
	function getMARGIN()
	{
		$this->validate();
		return $this->MARGIN;
	}
	
	//retun SCREENLINE_PRICE
	function getSCREENLINE_PRICE()
	{
		$this->validate();
		return $this->SCREENLINE_PRICE;
	}
	
	//retun TARGET_PRICE
	function getTARGET_PRICE()
	{
		$this->validate();
		return $this->TARGET_PRICE;
	}
	
	//retun APPROVED_PRICE
	function getAPPROVED_PRICE()
	{
		$this->validate();
		return $this->APPROVED_PRICE;
	}
	
	//retun COPPIED_FLAG
	function getCOPPIED_FLAG()
	{
		$this->validate();
		return $this->COPPIED_FLAG;
	}
	
	//retun COPPIED_FLAG
	function getCOPPIED_TYPE()
	{
		$this->validate();
		return $this->COPPIED_TYPE;
	}

	//retun COPPIED_FROM_SAMPLE_NO
	function getCOPPIED_FROM_SAMPLE_NO()
	{
		$this->validate();
		return $this->COPPIED_FROM_SAMPLE_NO;
	}
	
	//retun COPPIED_FROM_SAMPLE_YEAR
	function getCOPPIED_FROM_SAMPLE_YEAR()
	{
		$this->validate();
		return $this->COPPIED_FROM_SAMPLE_YEAR;
	}
	
	//retun COPPIED_FROM_REVISION
	function getCOPPIED_FROM_REVISION()
	{
		$this->validate();
		return $this->COPPIED_FROM_REVISION;
	}
	
	//retun COPPIED_FROM_COMBO
	function getCOPPIED_FROM_COMBO()
	{
		$this->validate();
		return $this->COPPIED_FROM_COMBO;
	}
	
	//retun COPPIED_FROM_PRINT
	function getCOPPIED_FROM_PRINT()
	{
		$this->validate();
		return $this->COPPIED_FROM_PRINT;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//retun APPROVE_LEVELS
	function getAPPROVE_LEVELS()
	{
		$this->validate();
		return $this->APPROVE_LEVELS;
	}
	
	//retun SAVED_BY
	function getSAVED_BY()
	{
		$this->validate();
		return $this->SAVED_BY;
	}
	
	//retun SAVED_TIME
	function getSAVED_TIME()
	{
		$this->validate();
		return $this->SAVED_TIME;
	}

    function getQTY_PRICES()
    {
        $this->validate();
        return $this->QTY_PRICES;
    }
	
	//END }

	//BEGIN - public function set {
	
	//set SAMPLE_NO
	function setSAMPLE_NO($SAMPLE_NO)
	{
		array_push($this->commitArray,'SAMPLE_NO');
		$this->SAMPLE_NO = $SAMPLE_NO;
	}
	
	//set SAMPLE_YEAR
	function setSAMPLE_YEAR($SAMPLE_YEAR)
	{
		array_push($this->commitArray,'SAMPLE_YEAR');
		$this->SAMPLE_YEAR = $SAMPLE_YEAR;
	}
	
	//set REVISION
	function setREVISION($REVISION)
	{
		array_push($this->commitArray,'REVISION');
		$this->REVISION = $REVISION;
	}
	
	//set COMBO
	function setCOMBO($COMBO)
	{
		array_push($this->commitArray,'COMBO');
		$this->COMBO = $COMBO;
	}
	
	//set PRINT
	function setPRINT($PRINT)
	{
		array_push($this->commitArray,'PRINT');
		$this->PRINT = $PRINT;
	}
	
	//set PRODUCTION_LOCATION
	function setPRODUCTION_LOCATION($PRODUCTION_LOCATION)
	{
		array_push($this->commitArray,'PRODUCTION_LOCATION');
		$this->PRODUCTION_LOCATION = $PRODUCTION_LOCATION;
	}
	
	//set UPS
	function setUPS($UPS)
	{
		array_push($this->commitArray,'UPS');
		$this->UPS = $UPS;
	}
	
	//set ESTIMATED_ORDER_QTY
	function setESTIMATED_ORDER_QTY($ESTIMATED_ORDER_QTY)
	{
		array_push($this->commitArray,'ESTIMATED_ORDER_QTY');
		$this->ESTIMATED_ORDER_QTY = $ESTIMATED_ORDER_QTY;
	}
	
	//set NO_OF_COLOURS
	function setNO_OF_COLOURS($NO_OF_COLOURS)
	{
		array_push($this->commitArray,'NO_OF_COLOURS');
		$this->NO_OF_COLOURS = $NO_OF_COLOURS;
	}
	
	//set SHOTS
	function setSHOTS($SHOTS)
	{
		array_push($this->commitArray,'SHOTS');
		$this->SHOTS = $SHOTS;
	}
	
	//set COST_PER_SHOT
	function setCOST_PER_SHOT($COST_PER_SHOT)
	{
		array_push($this->commitArray,'COST_PER_SHOT');
		$this->COST_PER_SHOT = $COST_PER_SHOT;
	}
	
	//set INK_COST
	function setINK_COST($INK_COST)
	{
		array_push($this->commitArray,'INK_COST');
		$this->INK_COST = $INK_COST;
	}
	
	//set SPECIAL_RM_COST
	function setSPECIAL_RM_COST($SPECIAL_RM_COST)
	{
		array_push($this->commitArray,'SPECIAL_RM_COST');
		$this->SPECIAL_RM_COST = $SPECIAL_RM_COST;
	}
	
	//set PROCESS_COST
	function setPROCESS_COST($PROCESS_COST)
	{
		array_push($this->commitArray,'PROCESS_COST');
		$this->PROCESS_COST = $PROCESS_COST;
	}
	
	//set SHOT_COST
	function setSHOT_COST($SHOT_COST)
	{
		array_push($this->commitArray,'SHOT_COST');
		$this->SHOT_COST = $SHOT_COST;
	}
	
	//set OTHER_COST
	function setOTHER_COST($OTHER_COST)
	{
		array_push($this->commitArray,'OTHER_COST');
		$this->OTHER_COST = $OTHER_COST;
	}
	
	//set OTHER_COST_REMARKS
	function setOTHER_COST_REMARKS($OTHER_COST_REMARKS)
	{
		array_push($this->commitArray,'OTHER_COST_REMARKS');
		$this->OTHER_COST_REMARKS = $OTHER_COST_REMARKS;
	}
	
	//set TOTAL_COST
	function setTOTAL_COST($TOTAL_COST)
	{
		array_push($this->commitArray,'TOTAL_COST');
		$this->TOTAL_COST = $TOTAL_COST;
	}
	
	//set MARGIN_PERCENTAGE
	function setMARGIN_PERCENTAGE($MARGIN_PERCENTAGE)
	{
		array_push($this->commitArray,'MARGIN_PERCENTAGE');
		$this->MARGIN_PERCENTAGE = $MARGIN_PERCENTAGE;
	}
	
	//set MARGIN
	function setMARGIN($MARGIN)
	{
		array_push($this->commitArray,'MARGIN');
		$this->MARGIN = $MARGIN;
	}
	
	//set SCREENLINE_PRICE
	function setSCREENLINE_PRICE($SCREENLINE_PRICE)
	{
		array_push($this->commitArray,'SCREENLINE_PRICE');
		$this->SCREENLINE_PRICE = $SCREENLINE_PRICE;
	}
	
	//set TARGET_PRICE
	function setTARGET_PRICE($TARGET_PRICE)
	{
		array_push($this->commitArray,'TARGET_PRICE');
		$this->TARGET_PRICE = $TARGET_PRICE;
	}
	
	//set APPROVED_PRICE
	function setAPPROVED_PRICE($APPROVED_PRICE)
	{
		array_push($this->commitArray,'APPROVED_PRICE');
		$this->APPROVED_PRICE = $APPROVED_PRICE;
	}
	
	//set COPPIED_FLAG
	function setCOPPIED_FLAG($COPPIED_FLAG)
	{
		array_push($this->commitArray,'COPPIED_FLAG');
		$this->COPPIED_FLAG = $COPPIED_FLAG;
	}
	
	//set COPPIED_FLAG
	function setCOPPIED_TYPE($COPPIED_TYPE)
	{
		array_push($this->commitArray,'COPPIED_TYPE');
		$this->COPPIED_TYPE = $COPPIED_TYPE;
	}
	
	//set COPPIED_FROM_SAMPLE_NO
	function setCOPPIED_FROM_SAMPLE_NO($COPPIED_FROM_SAMPLE_NO)
	{
		array_push($this->commitArray,'COPPIED_FROM_SAMPLE_NO');
		$this->COPPIED_FROM_SAMPLE_NO = $COPPIED_FROM_SAMPLE_NO;
	}
	
	//set COPPIED_FROM_SAMPLE_YEAR
	function setCOPPIED_FROM_SAMPLE_YEAR($COPPIED_FROM_SAMPLE_YEAR)
	{
		array_push($this->commitArray,'COPPIED_FROM_SAMPLE_YEAR');
		$this->COPPIED_FROM_SAMPLE_YEAR = $COPPIED_FROM_SAMPLE_YEAR;
	}
	
	//set COPPIED_FROM_REVISION
	function setCOPPIED_FROM_REVISION($COPPIED_FROM_REVISION)
	{
		array_push($this->commitArray,'COPPIED_FROM_REVISION');
		$this->COPPIED_FROM_REVISION = $COPPIED_FROM_REVISION;
	}
	
	//set COPPIED_FROM_COMBO
	function setCOPPIED_FROM_COMBO($COPPIED_FROM_COMBO)
	{
		array_push($this->commitArray,'COPPIED_FROM_COMBO');
		$this->COPPIED_FROM_COMBO = $COPPIED_FROM_COMBO;
	}
	
	//set COPPIED_FROM_PRINT
	function setCOPPIED_FROM_PRINT($COPPIED_FROM_PRINT)
	{
		array_push($this->commitArray,'COPPIED_FROM_PRINT');
		$this->COPPIED_FROM_PRINT = $COPPIED_FROM_PRINT;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//set APPROVE_LEVELS
	function setAPPROVE_LEVELS($APPROVE_LEVELS)
	{
		array_push($this->commitArray,'APPROVE_LEVELS');
		$this->APPROVE_LEVELS = $APPROVE_LEVELS;
	}
	
	//set SAVED_BY
	function setSAVED_BY($SAVED_BY)
	{
		array_push($this->commitArray,'SAVED_BY');
		$this->SAVED_BY = $SAVED_BY;
	}
	
	//set SAVED_TIME
	function setSAVED_TIME($SAVED_TIME)
	{
		array_push($this->commitArray,'SAVED_TIME');
		$this->SAVED_TIME = $SAVED_TIME;
	}

    function setQTY_PRICES($QTY_PRICES)
    {
        array_push($this->commitArray,'QTY_PRICES');
        $this->QTY_PRICES = $QTY_PRICES;
    }
	
	//END }

	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->SAMPLE_NO=='' || $this->SAMPLE_YEAR=='' || $this->REVISION=='' || $this->COMBO=='' || $this->PRINT=='')
			throw new exception("Set primary values first");
		else
			return true;
	}

	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}

	public function set($SAMPLE_NO , $SAMPLE_YEAR , $REVISION , $COMBO , $PRINT)
	{
		$cols	= "*";

		$join	= NULL;

		$where	= "SAMPLE_NO='$SAMPLE_NO' and SAMPLE_YEAR='$SAMPLE_YEAR' and REVISION='$REVISION' and COMBO='$COMBO' and PRINT='$PRINT'";

		$result = $this->select($cols,$join,$where);
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}

	//insert as parameters
	public function insertRec($SAMPLE_NO,$SAMPLE_YEAR,$REVISION,$COMBO,$PRINT,$PRODUCTION_LOCATION,$UPS,$ESTIMATED_ORDER_QTY,$NO_OF_COLOURS,$SHOTS,$COST_PER_SHOT,$INK_COST,$SPECIAL_RM_COST,$PROCESS_COST,$SHOT_COST,$OTHER_COST,$OTHER_COST_REMARKS,$TOTAL_COST,$MARGIN_PERCENTAGE,$MARGIN,$SCREENLINE_PRICE,$TARGET_PRICE,$APPROVED_PRICE,$COPPIED_FLAG,$COPPIED_TYPE,$COPPIED_FROM_SAMPLE_NO,$COPPIED_FROM_SAMPLE_YEAR,$COPPIED_FROM_REVISION,$COPPIED_FROM_COMBO,$COPPIED_FROM_PRINT,$STATUS,$APPROVE_LEVELS,$SAVED_BY,$SAVED_TIME,$QTY_PRICES){
		$data = array('SAMPLE_NO'=>$SAMPLE_NO 
				,'SAMPLE_YEAR'=>$SAMPLE_YEAR 
				,'REVISION'=>$REVISION 
				,'COMBO'=>$COMBO 
				,'PRINT'=>$PRINT 
				,'PRODUCTION_LOCATION'=>$PRODUCTION_LOCATION 
				,'UPS'=>$UPS 
				,'ESTIMATED_ORDER_QTY'=>$ESTIMATED_ORDER_QTY 
				,'NO_OF_COLOURS'=>$NO_OF_COLOURS 
				,'SHOTS'=>$SHOTS 
				,'COST_PER_SHOT'=>$COST_PER_SHOT 
				,'INK_COST'=>$INK_COST 
				,'SPECIAL_RM_COST'=>$SPECIAL_RM_COST 
				,'PROCESS_COST'=>$PROCESS_COST 
				,'SHOT_COST'=>$SHOT_COST 
				,'OTHER_COST'=>$OTHER_COST 
				,'OTHER_COST_REMARKS'=>$OTHER_COST_REMARKS 
				,'TOTAL_COST'=>$TOTAL_COST 
				,'MARGIN_PERCENTAGE'=>$MARGIN_PERCENTAGE 
				,'MARGIN'=>$MARGIN 
				,'SCREENLINE_PRICE'=>$SCREENLINE_PRICE 
				,'TARGET_PRICE'=>$TARGET_PRICE 
				,'APPROVED_PRICE'=>$APPROVED_PRICE 
				,'COPPIED_FLAG'=>$COPPIED_FLAG 
				,'COPPIED_TYPE'=>$COPPIED_TYPE 
				,'COPPIED_FROM_SAMPLE_NO'=>$COPPIED_FROM_SAMPLE_NO 
				,'COPPIED_FROM_SAMPLE_YEAR'=>$COPPIED_FROM_SAMPLE_YEAR 
				,'COPPIED_FROM_REVISION'=>$COPPIED_FROM_REVISION 
				,'COPPIED_FROM_COMBO'=>$COPPIED_FROM_COMBO 
				,'COPPIED_FROM_PRINT'=>$COPPIED_FROM_PRINT 
				,'STATUS'=>$STATUS 
				,'APPROVE_LEVELS'=>$APPROVE_LEVELS 
				,'SAVED_BY'=>$SAVED_BY 
				,'SAVED_TIME'=>$SAVED_TIME
                ,'QTY_PRICES'=>$QTY_PRICES
        );
		return $this->insert($data);
	}

	public function getCombo_samp($defaultValue=null,$where=null){
		$result = $this->select('SAMPLE_NO,SAMPLE_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['SAMPLE_NO'])
				$html .= '<option selected="selected" value="'.$row['SAMPLE_NO'].'">'.$row['SAMPLE_YEAR'].'</option>';
			else
				$html .= '<option value="'.$row['SAMPLE_NO'].'">'.$row['SAMPLE_YEAR'].'</option>';
		}
		return $html;
	}
	
	public function get_costing_sample($time_from,$time_to,$CLUSTER,$COMPANIES_INCLUDED,$COMPANIES_EXCLUDED,$BRAND_CATEGORY_INCLUDED,$BRAND_CATEGORY_EXCLUDED,$CUSTOMERS_INCLUDED,$CUSTOMERS_EXCLUDED){
		
		
		
		$cols	= "
					(if(costing_sample_header.STATUS=1,'APPROVED',IF(costing_sample_header.STATUS=0,'REJECTED',IF(costing_sample_header.STATUS>1,'PENDING','')))) AS STATUS_STR,
					costing_sample_header.SAMPLE_NO,
					costing_sample_header.SAMPLE_YEAR,
					costing_sample_header.REVISION,
					costing_sample_header.COMBO,
					costing_sample_header.PRINT,
					costing_sample_header.QTY_PRICES,
					mst_customer.strName,
					trn_sampleinfomations_printsize.intWidth,
					trn_sampleinfomations_printsize.intHeight,
					costing_sample_header.SHOTS,
					costing_sample_header.UPS,
					costing_sample_header.APPROVED_PRICE,
					/* mst_techniques.strName AS TECHNIQUE_NAME,*/
					(SELECT
					GROUP_CONCAT(distinct mst_techniques.strName)
					FROM
					trn_sampleinfomations_details
					INNER JOIN mst_techniques ON trn_sampleinfomations_details.intTechniqueId = mst_techniques.intId
					WHERE trn_sampleinfomations_details.intSampleNo = costing_sample_header.SAMPLE_NO AND trn_sampleinfomations_details.intSampleYear = costing_sample_header.SAMPLE_YEAR AND trn_sampleinfomations_details.intRevNo = costing_sample_header.REVISION AND trn_sampleinfomations_details.strComboName = costing_sample_header.COMBO AND trn_sampleinfomations_details.strPrintName = costing_sample_header.PRINT
					 ) as TECHNIQUE_NAME,   
					(select mst_costingprocesses.strProcess
					from  trn_sampleinfomations_combo_print_details_processes AS CPDP
					INNER JOIN mst_costingprocesses ON CPDP.PROCESS = mst_costingprocesses.intId AND mst_costingprocesses.printingMethod = 1
					 where  costing_sample_header.SAMPLE_NO = CPDP.SAMPLE_NO AND trn_sampleinfomations.intSampleYear = CPDP.SAMPLE_YEAR AND costing_sample_header.REVISION = CPDP.REVISION AND costing_sample_header.COMBO = CPDP.COMBO AND costing_sample_header.PRINT = CPDP.PRINT
LIMIT 1) as strProcess,
					-- mst_costingprocesses.strProcess,
					ifnull(costing_sample_header.SPECIAL_RM_COST,0) as SPECIAL_RM_COST ,
					ifnull(costing_sample_header.INK_COST,0) as INK_COST ,
					mst_brand.strName AS brand,
					mst_part.strName AS part,
					trn_sampleinfomations.strGraphicRefNo as graphic,
					sys_users.strUserName as user ";		
		$join	=" INNER JOIN trn_sampleinfomations ON costing_sample_header.SAMPLE_NO = trn_sampleinfomations.intSampleNo AND costing_sample_header.SAMPLE_YEAR = trn_sampleinfomations.intSampleYear AND costing_sample_header.REVISION = trn_sampleinfomations.intRevisionNo 
		INNER JOIN sys_users ON trn_sampleinfomations.intCreator = sys_users.intUserId
INNER JOIN mst_customer ON trn_sampleinfomations.intCustomer = mst_customer.intId
INNER JOIN trn_sampleinfomations_printsize ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_printsize.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_printsize.intSampleYear AND trn_sampleinfomations.intRevisionNo = trn_sampleinfomations_printsize.intRevisionNo   AND costing_sample_header.PRINT = trn_sampleinfomations_printsize.strPrintName 
/*INNER JOIN trn_sampleinfomations_details ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_details.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_details.intSampleYear AND trn_sampleinfomations.intRevisionNo = trn_sampleinfomations_details.intRevisionNo
 INNER JOIN mst_techniques ON trn_sampleinfomations_details.intTechniqueId = mst_techniques.intId */
/*LEFT JOIN trn_sampleinfomations_combo_print_details_processes ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_combo_print_details_processes.SAMPLE_NO AND trn_sampleinfomations.intSampleYear = trn_sampleinfomations_combo_print_details_processes.SAMPLE_YEAR AND trn_sampleinfomations.intRevisionNo = trn_sampleinfomations_combo_print_details_processes.REVISION AND costing_sample_header.COMBO = trn_sampleinfomations_combo_print_details_processes.COMBO AND costing_sample_header.PRINT = trn_sampleinfomations_combo_print_details_processes.PRINT
LEFT JOIN mst_costingprocesses ON trn_sampleinfomations_combo_print_details_processes.PROCESS = mst_costingprocesses.intId AND mst_costingprocesses.printingMethod = 1 */
LEFT JOIN costing_sample_header_approved_by ON costing_sample_header.SAMPLE_NO = costing_sample_header_approved_by.SAMPLE_NO AND costing_sample_header.SAMPLE_YEAR = costing_sample_header_approved_by.SAMPLE_YEAR AND costing_sample_header.REVISION = costing_sample_header_approved_by.REVISION_NO AND costing_sample_header.COMBO = costing_sample_header_approved_by.COMBO AND costing_sample_header.PRINT = costing_sample_header_approved_by.PRINT
left JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
left JOIN mst_part ON trn_sampleinfomations_printsize.intPart = mst_part.intId
INNER JOIN mst_locations ON trn_sampleinfomations.intCompanyId = mst_locations.intId
INNER JOIN mst_plant ON mst_locations.intPlant = mst_plant.intPlantId
					";
		
 		$where	.= " 1=1   
		AND ((costing_sample_header.`STATUS` = 1 AND
costing_sample_header.COPPIED_FROM_SAMPLE_NO IS NULL AND
costing_sample_header_approved_by.APPROVED_LEVEL_NO = costing_sample_header.`STATUS` AND
costing_sample_header_approved_by.`STATUS` = 0  AND
costing_sample_header_approved_by.APPROVED_DATE >= '$time_from' AND
costing_sample_header_approved_by.APPROVED_DATE <= '$time_to' ) /* OR 
					(
costing_sample_header.`STATUS` <> 1 AND
costing_sample_header.COPPIED_FROM_SAMPLE_NO IS NULL AND
costing_sample_header.SAVED_TIME >= '$time_from' AND
costing_sample_header.SAVED_TIME <= '$time_to'					)*/ ) "; 

if($CLUSTER!='')
$where	.= " AND mst_plant.MAIN_CLUSTER_ID = '$CLUSTER' ";
if($COMPANIES_INCLUDED!='')
$where	.= " AND mst_locations.intCompanyId IN (".$COMPANIES_INCLUDED.")";
if($COMPANIES_EXCLUDED!='')
$where	.= " AND mst_locations.intCompanyId NOT IN (".$COMPANIES_EXCLUDED.")";
if($BRAND_CATEGORY_INCLUDED!='')
$where	.= " AND mst_brand.CATEGORY IN (".$BRAND_CATEGORY_INCLUDED.")";
if($BRAND_CATEGORY_EXCLUDED!='')
$where	.= " AND mst_brand.CATEGORY NOT IN (".$BRAND_CATEGORY_EXCLUDED.")";
if($CUSTOMERS_INCLUDED!='')
$where	.= " AND trn_sampleinfomations.intCustomer IN (".$CUSTOMERS_INCLUDED.")";
if($CUSTOMERS_EXCLUDED!='')
$where	.= " AND trn_sampleinfomations.intCustomer NOT IN (".$CUSTOMERS_EXCLUDED.")";



$where	.= "GROUP BY 
	costing_sample_header.SAMPLE_NO,
	costing_sample_header.SAMPLE_YEAR,
	costing_sample_header.REVISION,
	costing_sample_header.COMBO,
	costing_sample_header.PRINT  
";


	//echo $sql	="select ".$cols." from costing_sample_header ".$join." where ".$where;
	return  $result = $this->select($cols,$join,$where);	
	//echo $this->db->getsql();
		
	}
	
	//END }
}
?>
<?php
class finance_pettycash_budget_details{
 
	private $db;
	private $table= "finance_pettycash_budget_details";
	
	//private property
	private $YEAR;
	private $MONTH;
	private $LOCATION_ID;
	private $AMOUNT;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('YEAR'=>'YEAR',
										'MONTH'=>'MONTH',
										'LOCATION_ID'=>'LOCATION_ID',
										'AMOUNT'=>'AMOUNT',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "YEAR = ".$this->YEAR." and MONTH = ".$this->MONTH." and LOCATION_ID = ".$this->LOCATION_ID."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun YEAR
	function getYEAR()
	{
		$this->validate();
		return $this->YEAR;
	}
	
	//retun MONTH
	function getMONTH()
	{
		$this->validate();
		return $this->MONTH;
	}
	
	//retun LOCATION_ID
	function getLOCATION_ID()
	{
		$this->validate();
		return $this->LOCATION_ID;
	}
	
	//retun AMOUNT
	function getAMOUNT()
	{
		$this->validate();
		return $this->AMOUNT;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set YEAR
	function setYEAR($YEAR)
	{
		array_push($this->commitArray,'YEAR');
		$this->YEAR = $YEAR;
	}
	
	//set MONTH
	function setMONTH($MONTH)
	{
		array_push($this->commitArray,'MONTH');
		$this->MONTH = $MONTH;
	}
	
	//set LOCATION_ID
	function setLOCATION_ID($LOCATION_ID)
	{
		array_push($this->commitArray,'LOCATION_ID');
		$this->LOCATION_ID = $LOCATION_ID;
	}
	
	//set AMOUNT
	function setAMOUNT($AMOUNT)
	{
		array_push($this->commitArray,'AMOUNT');
		$this->AMOUNT = $AMOUNT;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->YEAR=='' || $this->MONTH=='' || $this->LOCATION_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($YEAR , $MONTH , $LOCATION_ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "YEAR='$YEAR' and MONTH='$MONTH' and LOCATION_ID='$LOCATION_ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($YEAR,$MONTH,$LOCATION_ID,$AMOUNT){
		$data = array('YEAR'=>$YEAR 
				,'MONTH'=>$MONTH 
				,'LOCATION_ID'=>$LOCATION_ID 
				,'AMOUNT'=>$AMOUNT 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('YEAR,MONTH',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['YEAR'])
				$html .= '<option selected="selected" value="'.$row['YEAR'].'">'.$row['MONTH'].'</option>';	
			else
				$html .= '<option value="'.$row['YEAR'].'">'.$row['MONTH'].'</option>';	
		}
		return $html;
	}
	public function getBudgetAmount($month,$year,$location)
	{
		$cols 	= " IFNULL(AMOUNT,0) AS budgetAmount";
		$where 	= " YEAR = '$year' AND
					MONTH = '$month' AND
					LOCATION_ID = '$location' ";
		
		$result = $this->select($cols,NULL,$where,NULL,NULL);
		$row	= mysqli_fetch_array($result);
		return $row['budgetAmount'];
	}
	//END }
}
?>
<?php
//Manually Functions Added 
include_once MAIN_ROOT."class/sessions.php";					$sessions				= new sessions($db);

class ware_stocktransactions_bulk{
 
	private $db;
	private $table= "ware_stocktransactions_bulk";
	
	//private property
	private $id;
	private $intCompanyId;
	private $intLocationId;
	private $intSubStores;
	private $intDocumentNo;
	private $intDocumntYear;
	private $intGRNNo;
	private $intGRNYear;
	private $dtGRNDate;
	private $intCurrencyId;
	private $dblGRNRate;
	private $intItemId;
	private $dblQty;
	private $strType;
	private $intOrderNo;
	private $intOrderYear;
	private $intSalesOrderId;
	private $intUser;
	private $dtDate;
	private $PROGRAM;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('id'=>'id',
										'intCompanyId'=>'intCompanyId',
										'intLocationId'=>'intLocationId',
										'intSubStores'=>'intSubStores',
										'intDocumentNo'=>'intDocumentNo',
										'intDocumntYear'=>'intDocumntYear',
										'intGRNNo'=>'intGRNNo',
										'intGRNYear'=>'intGRNYear',
										'dtGRNDate'=>'dtGRNDate',
										'intCurrencyId'=>'intCurrencyId',
										'dblGRNRate'=>'dblGRNRate',
										'intItemId'=>'intItemId',
										'dblQty'=>'dblQty',
										'strType'=>'strType',
										'intOrderNo'=>'intOrderNo',
										'intOrderYear'=>'intOrderYear',
										'intSalesOrderId'=>'intSalesOrderId',
										'intUser'=>'intUser',
										'dtDate'=>'dtDate',
										'PROGRAM'=>'PROGRAM',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	function commit()
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "id = ".$this->id."" ;
		unset($this->commitArray);
		
		if($this->intId==NULL || $this->intId=='' || $this->intId=='NULL')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun id
	function getid()
	{
		$this->validate();
		return $this->id;
	}
	
	//retun intCompanyId
	function getintCompanyId()
	{
		$this->validate();
		return $this->intCompanyId;
	}
	
	//retun intLocationId
	function getintLocationId()
	{
		$this->validate();
		return $this->intLocationId;
	}
	
	//retun intSubStores
	function getintSubStores()
	{
		$this->validate();
		return $this->intSubStores;
	}
	
	//retun intDocumentNo
	function getintDocumentNo()
	{
		$this->validate();
		return $this->intDocumentNo;
	}
	
	//retun intDocumntYear
	function getintDocumntYear()
	{
		$this->validate();
		return $this->intDocumntYear;
	}
	
	//retun intGRNNo
	function getintGRNNo()
	{
		$this->validate();
		return $this->intGRNNo;
	}
	
	//retun intGRNYear
	function getintGRNYear()
	{
		$this->validate();
		return $this->intGRNYear;
	}
	
	//retun dtGRNDate
	function getdtGRNDate()
	{
		$this->validate();
		return $this->dtGRNDate;
	}
	
	//retun intCurrencyId
	function getintCurrencyId()
	{
		$this->validate();
		return $this->intCurrencyId;
	}
	
	//retun dblGRNRate
	function getdblGRNRate()
	{
		$this->validate();
		return $this->dblGRNRate;
	}
	
	//retun intItemId
	function getintItemId()
	{
		$this->validate();
		return $this->intItemId;
	}
	
	//retun dblQty
	function getdblQty()
	{
		$this->validate();
		return $this->dblQty;
	}
	
	//retun strType
	function getstrType()
	{
		$this->validate();
		return $this->strType;
	}
	
	//retun intOrderNo
	function getintOrderNo()
	{
		$this->validate();
		return $this->intOrderNo;
	}
	
	//retun intOrderYear
	function getintOrderYear()
	{
		$this->validate();
		return $this->intOrderYear;
	}
	
	//retun intSalesOrderId
	function getintSalesOrderId()
	{
		$this->validate();
		return $this->intSalesOrderId;
	}
	
	//retun intUser
	function getintUser()
	{
		$this->validate();
		return $this->intUser;
	}
	
	//retun dtDate
	function getdtDate()
	{
		$this->validate();
		return $this->dtDate;
	}
	
	//retun PROGRAM
	function getPROGRAM()
	{
		$this->validate();
		return $this->PROGRAM;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set id
	function setid($id)
	{
		array_push($this->commitArray,'id');
		$this->id = $id;
	}
	
	//set intCompanyId
	function setintCompanyId($intCompanyId)
	{
		array_push($this->commitArray,'intCompanyId');
		$this->intCompanyId = $intCompanyId;
	}
	
	//set intLocationId
	function setintLocationId($intLocationId)
	{
		array_push($this->commitArray,'intLocationId');
		$this->intLocationId = $intLocationId;
	}
	
	//set intSubStores
	function setintSubStores($intSubStores)
	{
		array_push($this->commitArray,'intSubStores');
		$this->intSubStores = $intSubStores;
	}
	
	//set intDocumentNo
	function setintDocumentNo($intDocumentNo)
	{
		array_push($this->commitArray,'intDocumentNo');
		$this->intDocumentNo = $intDocumentNo;
	}
	
	//set intDocumntYear
	function setintDocumntYear($intDocumntYear)
	{
		array_push($this->commitArray,'intDocumntYear');
		$this->intDocumntYear = $intDocumntYear;
	}
	
	//set intGRNNo
	function setintGRNNo($intGRNNo)
	{
		array_push($this->commitArray,'intGRNNo');
		$this->intGRNNo = $intGRNNo;
	}
	
	//set intGRNYear
	function setintGRNYear($intGRNYear)
	{
		array_push($this->commitArray,'intGRNYear');
		$this->intGRNYear = $intGRNYear;
	}
	
	//set dtGRNDate
	function setdtGRNDate($dtGRNDate)
	{
		array_push($this->commitArray,'dtGRNDate');
		$this->dtGRNDate = $dtGRNDate;
	}
	
	//set intCurrencyId
	function setintCurrencyId($intCurrencyId)
	{
		array_push($this->commitArray,'intCurrencyId');
		$this->intCurrencyId = $intCurrencyId;
	}
	
	//set dblGRNRate
	function setdblGRNRate($dblGRNRate)
	{
		array_push($this->commitArray,'dblGRNRate');
		$this->dblGRNRate = $dblGRNRate;
	}
	
	//set intItemId
	function setintItemId($intItemId)
	{
		array_push($this->commitArray,'intItemId');
		$this->intItemId = $intItemId;
	}
	
	//set dblQty
	function setdblQty($dblQty)
	{
		array_push($this->commitArray,'dblQty');
		$this->dblQty = $dblQty;
	}
	
	//set strType
	function setstrType($strType)
	{
		array_push($this->commitArray,'strType');
		$this->strType = $strType;
	}
	
	//set intOrderNo
	function setintOrderNo($intOrderNo)
	{
		array_push($this->commitArray,'intOrderNo');
		$this->intOrderNo = $intOrderNo;
	}
	
	//set intOrderYear
	function setintOrderYear($intOrderYear)
	{
		array_push($this->commitArray,'intOrderYear');
		$this->intOrderYear = $intOrderYear;
	}
	
	//set intSalesOrderId
	function setintSalesOrderId($intSalesOrderId)
	{
		array_push($this->commitArray,'intSalesOrderId');
		$this->intSalesOrderId = $intSalesOrderId;
	}
	
	//set intUser
	function setintUser($intUser)
	{
		array_push($this->commitArray,'intUser');
		$this->intUser = $intUser;
	}
	
	//set dtDate
	function setdtDate($dtDate)
	{
		array_push($this->commitArray,'dtDate');
		$this->dtDate = $dtDate;
	}
	
	//set PROGRAM
	function setPROGRAM($PROGRAM)
	{
		array_push($this->commitArray,'PROGRAM');
		$this->PROGRAM = $PROGRAM;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->id=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($id)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "id='$id'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($intCompanyId,$intLocationId,$intSubStores,$intDocumentNo,$intDocumntYear,$intGRNNo,$intGRNYear,$dtGRNDate,$intCurrencyId,$dblGRNRate,$intItemId,$dblQty,$strType,$intOrderNo,$intOrderYear,$intSalesOrderId,$intUser,$dtDate,$PROGRAM){
		$data = array(
				'intCompanyId'=>$intCompanyId 
				,'intLocationId'=>$intLocationId 
				,'intSubStores'=>$intSubStores 
				,'intDocumentNo'=>$intDocumentNo 
				,'intDocumntYear'=>$intDocumntYear 
				,'intGRNNo'=>$intGRNNo 
				,'intGRNYear'=>$intGRNYear 
				,'dtGRNDate'=>$dtGRNDate 
				,'intCurrencyId'=>$intCurrencyId 
				,'dblGRNRate'=>$dblGRNRate 
				,'intItemId'=>$intItemId 
				,'dblQty'=>$dblQty 
				,'strType'=>$strType 
				,'intOrderNo'=>$intOrderNo 
				,'intOrderYear'=>$intOrderYear 
				,'intSalesOrderId'=>$intSalesOrderId 
				,'intUser'=>$intUser 
				,'dtDate'=>$dtDate 
				,'PROGRAM'=>$PROGRAM 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('id,intCompanyId',  null, $where = $where);
		while($row=mysqli_fetch_array($result)){
			$html .= '<option value="'.$row['id'].'">'.$row['intCompanyId'].'</option>';	
		}
		return $html;
	}
	
	public function getMainStoresItemBalance($location,$item,$deci){
	
	$cols		= " COALESCE(ROUND(SUM(dblQty),$deci+2),0) AS stock_bal ";
	
	$where		= " intItemId = '".$item."' ";
	
	if($location!=''){
	$where		.= " AND
					intLocationId = '".$location."' ";
	}
	
	$result 	= $this->select($cols,$join = null,$where, $order = null, $limit = null);	
	$row		= mysqli_fetch_array($result);
	return 		$row['stock_bal'];
	}

	public function getMainStoresGrnWiseItemBalance_result($location,$item,$deci){
		
		$cols		= " 
						COALESCE(ROUND(SUM(ware_stocktransactions_bulk.dblQty),".($deci+2)."),0) AS stockBal,
						ware_stocktransactions_bulk.intGRNNo,
						ware_stocktransactions_bulk.intGRNYear,
						ware_stocktransactions_bulk.dtGRNDate,
						ware_stocktransactions_bulk.intCurrencyId,
						ware_stocktransactions_bulk.dblGRNRate
					 ";
		
		$where		= " 
						intItemId = '".$item."' AND
						intLocationId = '".$location."' 
						GROUP BY
						ware_stocktransactions_bulk.intItemId,
						ware_stocktransactions_bulk.intGRNNo,
						ware_stocktransactions_bulk.intGRNYear, 
						ware_stocktransactions_bulk.dblGRNRate 
						HAVING stockBal > 0	
					 ";
		$order	= 
					 "
						ware_stocktransactions_bulk.dtGRNDate ASC
					 ";
		
		$result 	= $this->select($cols,$join = null,$where, $order, $limit = null);	
 		return 		$result;
	}
	
	function getMainStoresData_docNoWise($docNo,$docYear,$allocStockType,$unallocStockType,$deci)
	{
		$cols	= " intOrderNo,
					intOrderYear,
					intSalesOrderId,
					intItemId,
					(
					SELECT COALESCE(ROUND(SUM(dblQty),6+2),0) AS qty
					FROM ware_stocktransactions ST
					WHERE 
					ST.intOrderNo = ware_stocktransactions_bulk.intOrderNo AND
					ST.intOrderYear = ware_stocktransactions_bulk.intOrderYear AND
					ST.intLocationId = ware_stocktransactions_bulk.intLocationId AND
					ST.intDocumentNo = ware_stocktransactions_bulk.intDocumentNo AND
					ST.intDocumntYear = ware_stocktransactions_bulk.intDocumntYear AND
					ST.intItemId = ware_stocktransactions_bulk.intItemId AND
					ST.intSalesOrderId = ware_stocktransactions_bulk.intSalesOrderId AND
					ST.strType = '$allocStockType'
					) allocate_qty,
					(
					SELECT COALESCE(ROUND(SUM(dblQty),6+2),0) AS qty
					FROM ware_stocktransactions_bulk USTB
					WHERE 
					USTB.intOrderNo = ware_stocktransactions_bulk.intOrderNo AND
					USTB.intOrderYear = ware_stocktransactions_bulk.intOrderYear AND
					USTB.intLocationId = ware_stocktransactions_bulk.intLocationId AND
					USTB.intDocumentNo = ware_stocktransactions_bulk.intDocumentNo AND
					USTB.intDocumntYear = ware_stocktransactions_bulk.intDocumntYear AND
					USTB.intItemId = ware_stocktransactions_bulk.intItemId AND
					USTB.intSalesOrderId = ware_stocktransactions_bulk.intSalesOrderId AND
					USTB.strType = '$unallocStockType'
					) unallocate_qty ";
		
		$where	= " strType IN ('".$allocStockType."','".$unallocStockType."') AND
					intDocumentNo = '".$docNo."' AND
					intDocumntYear = '".$docYear."'
					GROUP BY intOrderNo,intOrderYear,intDocumentNo,intDocumntYear,intSalesOrderId,intItemId ";
	
		return $this->select($cols,$join=null,$where,$order=null,$limit=null);
	}
	
public function getMainStoresData($orderNo,$orderYear,$allocStockType,$unallocStockType,$docNo,$docYear,$deci)
{
 	global $sessions;
	
	$cols	= " intOrderNo,
				intOrderYear,
				intSalesOrderId,
				intItemId,
				(
				SELECT COALESCE(ROUND(SUM(dblQty),6+2),0) AS qty
				FROM ware_stocktransactions ST
				WHERE 
				ST.intOrderNo = ware_stocktransactions_bulk.intOrderNo AND
				ST.intOrderYear = ware_stocktransactions_bulk.intOrderYear AND
				ST.intLocationId = ware_stocktransactions_bulk.intLocationId AND
				ST.intDocumentNo = ware_stocktransactions_bulk.intDocumentNo AND
				ST.intDocumntYear = ware_stocktransactions_bulk.intDocumntYear AND
				ST.intItemId = ware_stocktransactions_bulk.intItemId AND
				ST.intSalesOrderId = ware_stocktransactions_bulk.intSalesOrderId AND
				ST.strType = '$allocStockType'
				) allocate_qty,
				(
				SELECT COALESCE(ROUND(SUM(dblQty),6+2),0) AS qty
				FROM ware_stocktransactions_bulk USTB
				WHERE 
				USTB.intOrderNo = ware_stocktransactions_bulk.intOrderNo AND
				USTB.intOrderYear = ware_stocktransactions_bulk.intOrderYear AND
				USTB.intLocationId = ware_stocktransactions_bulk.intLocationId AND
				USTB.intDocumentNo = ware_stocktransactions_bulk.intDocumentNo AND
				USTB.intDocumntYear = ware_stocktransactions_bulk.intDocumntYear AND
				USTB.intItemId = ware_stocktransactions_bulk.intItemId AND
				USTB.intSalesOrderId = ware_stocktransactions_bulk.intSalesOrderId AND
				USTB.strType = '$unallocStockType'
				) unallocate_qty ";
	
	$where	= " intOrderNo = '".$orderNo."' AND
				intOrderYear = '".$orderYear."' AND
				strType IN ('".$allocStockType."','".$unallocStockType."') AND
				/*intLocationId = '".$sessions->getLocationId()."'  AND*/
				intDocumentNo = '".$docNo."' AND
				intDocumntYear = '".$docYear."'
				GROUP BY intOrderNo,intOrderYear,intDocumentNo,intDocumntYear,intSalesOrderId,intItemId ";

	$result	= $this->select($cols,$join=null,$where,$order=null,$limit=null);
	
	return $result;
}	
	//END }
}
?>
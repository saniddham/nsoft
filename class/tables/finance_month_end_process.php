<?php
class finance_month_end_process{
 
	private $db;
	private $table= "finance_month_end_process";
	
	//private property
	private $PERIOD_ID;
	private $YEAR;
	private $MONTH;
	private $COMPANY_ID;
	private $PROCESS_STATUS;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('PERIOD_ID'=>'PERIOD_ID',
										'YEAR'=>'YEAR',
										'MONTH'=>'MONTH',
										'COMPANY_ID'=>'COMPANY_ID',
										'PROCESS_STATUS'=>'PROCESS_STATUS',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "PERIOD_ID = ".$this->PERIOD_ID." and YEAR = ".$this->YEAR." and MONTH = ".$this->MONTH." and COMPANY_ID = ".$this->COMPANY_ID."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun PERIOD_ID
	function getPERIOD_ID()
	{
		$this->validate();
		return $this->PERIOD_ID;
	}
	
	//retun YEAR
	function getYEAR()
	{
		$this->validate();
		return $this->YEAR;
	}
	
	//retun MONTH
	function getMONTH()
	{
		$this->validate();
		return $this->MONTH;
	}
	
	//retun COMPANY_ID
	function getCOMPANY_ID()
	{
		$this->validate();
		return $this->COMPANY_ID;
	}
	
	//retun PROCESS_STATUS
	function getPROCESS_STATUS()
	{
		$this->validate();
		return $this->PROCESS_STATUS;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set PERIOD_ID
	function setPERIOD_ID($PERIOD_ID)
	{
		array_push($this->commitArray,'PERIOD_ID');
		$this->PERIOD_ID = $PERIOD_ID;
	}
	
	//set YEAR
	function setYEAR($YEAR)
	{
		array_push($this->commitArray,'YEAR');
		$this->YEAR = $YEAR;
	}
	
	//set MONTH
	function setMONTH($MONTH)
	{
		array_push($this->commitArray,'MONTH');
		$this->MONTH = $MONTH;
	}
	
	//set COMPANY_ID
	function setCOMPANY_ID($COMPANY_ID)
	{
		array_push($this->commitArray,'COMPANY_ID');
		$this->COMPANY_ID = $COMPANY_ID;
	}
	
	//set PROCESS_STATUS
	function setPROCESS_STATUS($PROCESS_STATUS)
	{
		array_push($this->commitArray,'PROCESS_STATUS');
		$this->PROCESS_STATUS = $PROCESS_STATUS;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->PERIOD_ID=='' || $this->YEAR=='' || $this->MONTH=='' || $this->COMPANY_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($PERIOD_ID , $YEAR , $MONTH , $COMPANY_ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "PERIOD_ID='$PERIOD_ID' and YEAR='$YEAR' and MONTH='$MONTH' and COMPANY_ID='$COMPANY_ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($PERIOD_ID,$YEAR,$MONTH,$COMPANY_ID,$PROCESS_STATUS){
		$data = array('PERIOD_ID'=>$PERIOD_ID 
				,'YEAR'=>$YEAR 
				,'MONTH'=>$MONTH 
				,'COMPANY_ID'=>$COMPANY_ID 
				,'PROCESS_STATUS'=>$PROCESS_STATUS 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('PERIOD_ID,YEAR',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['PERIOD_ID'])
				$html .= '<option selected="selected" value="'.$row['PERIOD_ID'].'">'.$row['YEAR'].'</option>';	
			else
				$html .= '<option value="'.$row['PERIOD_ID'].'">'.$row['YEAR'].'</option>';	
		}
		return $html;
	}
	public function getIsMonthEndProcessLocked($date,$companyId)
	{
		$cols	= " PROCESS_STATUS ";
		
		$where	= " YEAR = YEAR('$date')
					AND MONTH = MONTH('$date')
					AND PROCESS_STATUS = 1
					AND COMPANY_ID = $companyId ";
		
		$result	= $this->select($cols,$join=null,$where);
		$row	= mysqli_fetch_array($result);
		
		if($row['PROCESS_STATUS']==1)
			return true;
		else
			return false;
	}
	//END }
}
?>
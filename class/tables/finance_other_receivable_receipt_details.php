<?php
class finance_other_receivable_receipt_details{
 
	private $db;
	private $table= "finance_other_receivable_receipt_details";
	
	//private property
	private $RECEIPT_NO;
	private $RECEIPT_YEAR;
	private $BILL_INVOICE_NO;
	private $BILL_INVOICE_YEAR;
	private $AMOUNT;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('RECEIPT_NO'=>'RECEIPT_NO',
										'RECEIPT_YEAR'=>'RECEIPT_YEAR',
										'BILL_INVOICE_NO'=>'BILL_INVOICE_NO',
										'BILL_INVOICE_YEAR'=>'BILL_INVOICE_YEAR',
										'AMOUNT'=>'AMOUNT',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "RECEIPT_NO = ".$this->RECEIPT_NO." and RECEIPT_YEAR = ".$this->RECEIPT_YEAR." and BILL_INVOICE_NO = ".$this->BILL_INVOICE_NO." and BILL_INVOICE_YEAR = ".$this->BILL_INVOICE_YEAR."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun RECEIPT_NO
	function getRECEIPT_NO()
	{
		$this->validate();
		return $this->RECEIPT_NO;
	}
	
	//retun RECEIPT_YEAR
	function getRECEIPT_YEAR()
	{
		$this->validate();
		return $this->RECEIPT_YEAR;
	}
	
	//retun BILL_INVOICE_NO
	function getBILL_INVOICE_NO()
	{
		$this->validate();
		return $this->BILL_INVOICE_NO;
	}
	
	//retun BILL_INVOICE_YEAR
	function getBILL_INVOICE_YEAR()
	{
		$this->validate();
		return $this->BILL_INVOICE_YEAR;
	}
	
	//retun AMOUNT
	function getAMOUNT()
	{
		$this->validate();
		return $this->AMOUNT;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set RECEIPT_NO
	function setRECEIPT_NO($RECEIPT_NO)
	{
		array_push($this->commitArray,'RECEIPT_NO');
		$this->RECEIPT_NO = $RECEIPT_NO;
	}
	
	//set RECEIPT_YEAR
	function setRECEIPT_YEAR($RECEIPT_YEAR)
	{
		array_push($this->commitArray,'RECEIPT_YEAR');
		$this->RECEIPT_YEAR = $RECEIPT_YEAR;
	}
	
	//set BILL_INVOICE_NO
	function setBILL_INVOICE_NO($BILL_INVOICE_NO)
	{
		array_push($this->commitArray,'BILL_INVOICE_NO');
		$this->BILL_INVOICE_NO = $BILL_INVOICE_NO;
	}
	
	//set BILL_INVOICE_YEAR
	function setBILL_INVOICE_YEAR($BILL_INVOICE_YEAR)
	{
		array_push($this->commitArray,'BILL_INVOICE_YEAR');
		$this->BILL_INVOICE_YEAR = $BILL_INVOICE_YEAR;
	}
	
	//set AMOUNT
	function setAMOUNT($AMOUNT)
	{
		array_push($this->commitArray,'AMOUNT');
		$this->AMOUNT = $AMOUNT;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->RECEIPT_NO=='' || $this->RECEIPT_YEAR=='' || $this->BILL_INVOICE_NO=='' || $this->BILL_INVOICE_YEAR=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($RECEIPT_NO , $RECEIPT_YEAR , $BILL_INVOICE_NO , $BILL_INVOICE_YEAR)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "RECEIPT_NO='$RECEIPT_NO' and RECEIPT_YEAR='$RECEIPT_YEAR' and BILL_INVOICE_NO='$BILL_INVOICE_NO' and BILL_INVOICE_YEAR='$BILL_INVOICE_YEAR'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($RECEIPT_NO,$RECEIPT_YEAR,$BILL_INVOICE_NO,$BILL_INVOICE_YEAR,$AMOUNT){
		$data = array('RECEIPT_NO'=>$RECEIPT_NO 
				,'RECEIPT_YEAR'=>$RECEIPT_YEAR 
				,'BILL_INVOICE_NO'=>$BILL_INVOICE_NO 
				,'BILL_INVOICE_YEAR'=>$BILL_INVOICE_YEAR 
				,'AMOUNT'=>$AMOUNT 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('RECEIPT_NO,RECEIPT_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['RECEIPT_NO'])
				$html .= '<option selected="selected" value="'.$row['RECEIPT_NO'].'">'.$row['RECEIPT_YEAR'].'</option>';	
			else
				$html .= '<option value="'.$row['RECEIPT_NO'].'">'.$row['RECEIPT_YEAR'].'</option>';	
		}
		return $html;
	}
	public function getReceiptDetail_result($receiptNo,$receiptYear)
	{
		$cols	= " finance_other_receivable_receipt_details.RECEIPT_NO,
					finance_other_receivable_receipt_details.RECEIPT_YEAR,
					finance_other_receivable_receipt_details.BILL_INVOICE_NO,
					finance_other_receivable_receipt_details.BILL_INVOICE_YEAR,
					CONCAT(finance_other_receivable_receipt_details.BILL_INVOICE_NO,'/',finance_other_receivable_receipt_details.BILL_INVOICE_YEAR) AS CON_INVOICE,
					finance_other_receivable_receipt_details.AMOUNT,
					finance_other_receivable_invoice_header.REFERENCE_NO,
					finance_other_receivable_invoice_header.BILL_DATE,
					finance_other_receivable_invoice_header.`STATUS`";
		
		$join	= " INNER JOIN finance_other_receivable_invoice_header ON finance_other_receivable_receipt_details.BILL_INVOICE_NO = finance_other_receivable_invoice_header.BILL_INVOICE_NO AND finance_other_receivable_receipt_details.BILL_INVOICE_YEAR = finance_other_receivable_invoice_header.BILL_INVOICE_YEAR";
		
		$where	= " finance_other_receivable_receipt_details.RECEIPT_NO = '$receiptNo' AND
					finance_other_receivable_receipt_details.RECEIPT_YEAR = '$receiptYear'";	
		
		$result	= $this->select($cols,$join,$where,NULL,NULL);
		return $result;
	}
	public function getPdfReceiptDetail_result($receiptNo,$receiptYear,$invoiceNo,$invoiceYear)
	{
		$cols	= " finance_other_receivable_receipt_details.RECEIPT_NO,
					finance_other_receivable_receipt_details.RECEIPT_YEAR,
					finance_other_receivable_receipt_details.BILL_INVOICE_NO,
					finance_other_receivable_receipt_details.BILL_INVOICE_YEAR,
					CONCAT(finance_other_receivable_receipt_details.BILL_INVOICE_NO,'/',finance_other_receivable_receipt_details.BILL_INVOICE_YEAR) AS CON_INVOICE,
					finance_other_receivable_receipt_details.AMOUNT,
					finance_other_receivable_invoice_header.REFERENCE_NO,
					finance_other_receivable_invoice_header.BILL_DATE,
					finance_other_receivable_invoice_header.`STATUS`,
					finance_other_receivable_invoice_details.ITEM_DESCRIPTION,
					finance_other_receivable_invoice_details.CHART_OF_ACCOUNT_ID,
					finance_other_receivable_invoice_details.QTY,
					finance_other_receivable_invoice_details.UNIT_PRICE,
					finance_other_receivable_invoice_details.DISCOUNT,
					finance_other_receivable_invoice_details.TAX_AMOUNT,
					(SELECT 
					finance_mst_chartofaccount.CHART_OF_ACCOUNT_NAME
					FROM finance_mst_chartofaccount
					 WHERE
					 CHART_OF_ACCOUNT_ID = finance_other_receivable_invoice_details.CHART_OF_ACCOUNT_ID
					) AS GL_NAME";
		
		$join	= " INNER JOIN finance_other_receivable_invoice_header ON finance_other_receivable_receipt_details.BILL_INVOICE_NO = finance_other_receivable_invoice_header.BILL_INVOICE_NO AND finance_other_receivable_receipt_details.BILL_INVOICE_YEAR = finance_other_receivable_invoice_header.BILL_INVOICE_YEAR
					INNER JOIN finance_other_receivable_invoice_details ON finance_other_receivable_invoice_header.BILL_INVOICE_NO = finance_other_receivable_invoice_details.BILL_INVOICE_NO AND finance_other_receivable_invoice_header.BILL_INVOICE_YEAR = finance_other_receivable_invoice_details.BILL_INVOICE_YEAR";
		
		$where	= " finance_other_receivable_receipt_details.RECEIPT_NO = '$receiptNo' AND
					finance_other_receivable_receipt_details.RECEIPT_YEAR = '$receiptYear'
					AND finance_other_receivable_invoice_details.BILL_INVOICE_NO = '$invoiceNo' 
					AND finance_other_receivable_invoice_details.BILL_INVOICE_YEAR = '$invoiceYear'";	
		
		$result	= $this->select($cols,$join,$where,NULL,NULL);
		return $result;
	}
	public function getReceiptAmount($receiveNo,$receiveYear)
	{
		$cols	= " finance_other_receivable_receipt_details.BILL_INVOICE_NO,
					finance_other_receivable_receipt_details.BILL_INVOICE_YEAR,
					CONCAT(finance_other_receivable_receipt_details.BILL_INVOICE_NO,'/',finance_other_receivable_receipt_details.BILL_INVOICE_YEAR) AS CON_INVOICE,
					finance_other_receivable_receipt_details.AMOUNT,
					finance_other_receivable_receipt_details.RECEIPT_NO,
					finance_other_receivable_receipt_details.RECEIPT_YEAR";
		$join	= " INNER JOIN finance_other_receivable_receipt_header ON finance_other_receivable_receipt_header.RECEIPT_NO = finance_other_receivable_receipt_details.RECEIPT_NO AND finance_other_receivable_receipt_header.RECEIPT_YEAR = finance_other_receivable_receipt_details.RECEIPT_YEAR";
		$where	= " finance_other_receivable_receipt_header.RECEIPT_NO = '$receiveNo' AND 
					finance_other_receivable_receipt_header.RECEIPT_YEAR = '$receiveYear'";
		
		$result	= $this->select($cols,$join,$where,NULL,NULL);
		return $result;	
	}
	//END }
}
?>
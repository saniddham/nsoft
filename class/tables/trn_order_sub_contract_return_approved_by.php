
<?php

class trn_order_sub_contract_return_approved_by{
 
	private $db;
	private $table= "trn_order_sub_contract_return_approved_by";
	
	//private property
	private $SUB_CONTRACT_RETURN_NO;
	private $SUB_CONTRACT_RETURN_YEAR;
	private $LEVELS;
	private $USER;
	private $DATE;
	private $STATUS;
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->db->field_array_set(array('SUB_CONTRACT_RETURN_NO'=>'SUB_CONTRACT_RETURN_NO',
										'SUB_CONTRACT_RETURN_YEAR'=>'SUB_CONTRACT_RETURN_YEAR',
										'LEVELS'=>'LEVELS',
										'USER'=>'USER',
										'DATE'=>'DATE',
										'STATUS'=>'STATUS',
										));
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  
		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	//public functions for private properties
	
	//retun SUB_CONTRACT_RETURN_NO
	function getSUB_CONTRACT_RETURN_NO()
	{
		$this->validate();
		return $this->SUB_CONTRACT_RETURN_NO;
	}
	
	//retun SUB_CONTRACT_RETURN_YEAR
	function getSUB_CONTRACT_RETURN_YEAR()
	{
		$this->validate();
		return $this->SUB_CONTRACT_RETURN_YEAR;
	}
	
	//retun LEVELS
	function getLEVELS()
	{
		$this->validate();
		return $this->LEVELS;
	}
	
	//retun USER
	function getUSER()
	{
		$this->validate();
		return $this->USER;
	}
	
	//retun DATE
	function getDATE()
	{
		$this->validate();
		return $this->DATE;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//validate primary values
	private function validate()
	{
		if($this->SUB_CONTRACT_RETURN_NO=='' || $this->SUB_CONTRACT_RETURN_YEAR=='' || $this->LEVELS=='' || $this->STATUS=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
		}
	}
	
	public function set($SUB_CONTRACT_RETURN_NO , $SUB_CONTRACT_RETURN_YEAR , $LEVELS , $STATUS)
	{
		$result = $this->select('*',null,"SUB_CONTRACT_RETURN_NO='$SUB_CONTRACT_RETURN_NO' and SUB_CONTRACT_RETURN_YEAR='$SUB_CONTRACT_RETURN_YEAR' and LEVELS='$LEVELS' and STATUS='$STATUS'");	
		if($this->db->numRows()>0)
			$this->setVariables($result);
		else
			throw new Exception("Record not found.");
			
	}
	
	
}
?>


<?php

class trn_sampleinfomations_details{
 
	private $db;
	private $table= "trn_sampleinfomations_details";
	
	//private property
	private $intSampleNo;
	private $intSampleYear;
	private $intRevNo;
	private $strPrintName;
	private $strComboName;
	private $intColorId;
	private $intPrintMode;
	private $intWashStanderd;
	private $intGroundColor;
	private $intFabricType;
	private $intTechniqueId;
	private $intItem;
	private $dblQty;
	private $size_w;
	private $size_h;
	private $intOrderBy;
	private $intCompanyId;
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->db->field_array_set(array('intSampleNo'=>'intSampleNo',
										'intSampleYear'=>'intSampleYear',
										'intRevNo'=>'intRevNo',
										'strPrintName'=>'strPrintName',
										'strComboName'=>'strComboName',
										'intColorId'=>'intColorId',
										'intPrintMode'=>'intPrintMode',
										'intWashStanderd'=>'intWashStanderd',
										'intGroundColor'=>'intGroundColor',
										'intFabricType'=>'intFabricType',
										'intTechniqueId'=>'intTechniqueId',
										'intItem'=>'intItem',
										'dblQty'=>'dblQty',
										'size_w'=>'size_w',
										'size_h'=>'size_h',
										'intOrderBy'=>'intOrderBy',
										'intCompanyId'=>'intCompanyId',
										));
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  
		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	//public functions for private properties
	
	//retun intSampleNo
	function getintSampleNo()
	{
		$this->validate();
		return $this->intSampleNo;
	}
	
	//retun intSampleYear
	function getintSampleYear()
	{
		$this->validate();
		return $this->intSampleYear;
	}
	
	//retun intRevNo
	function getintRevNo()
	{
		$this->validate();
		return $this->intRevNo;
	}
	
	//retun strPrintName
	function getstrPrintName()
	{
		$this->validate();
		return $this->strPrintName;
	}
	
	//retun strComboName
	function getstrComboName()
	{
		$this->validate();
		return $this->strComboName;
	}
	
	//retun intColorId
	function getintColorId()
	{
		$this->validate();
		return $this->intColorId;
	}
	
	//retun intPrintMode
	function getintPrintMode()
	{
		$this->validate();
		return $this->intPrintMode;
	}
	
	//retun intWashStanderd
	function getintWashStanderd()
	{
		$this->validate();
		return $this->intWashStanderd;
	}
	
	//retun intGroundColor
	function getintGroundColor()
	{
		$this->validate();
		return $this->intGroundColor;
	}
	
	//retun intFabricType
	function getintFabricType()
	{
		$this->validate();
		return $this->intFabricType;
	}
	
	//retun intTechniqueId
	function getintTechniqueId()
	{
		$this->validate();
		return $this->intTechniqueId;
	}
	
	//retun intItem
	function getintItem()
	{
		$this->validate();
		return $this->intItem;
	}
	
	//retun dblQty
	function getdblQty()
	{
		$this->validate();
		return $this->dblQty;
	}
	
	//retun size_w
	function getsize_w()
	{
		$this->validate();
		return $this->size_w;
	}
	
	//retun size_h
	function getsize_h()
	{
		$this->validate();
		return $this->size_h;
	}
	
	//retun intOrderBy
	function getintOrderBy()
	{
		$this->validate();
		return $this->intOrderBy;
	}
	
	//retun intCompanyId
	function getintCompanyId()
	{
		$this->validate();
		return $this->intCompanyId;
	}
	
	//validate primary values
	private function validate()
	{
		if($this->intSampleNo=='' || $this->intSampleYear=='' || $this->intRevNo=='' || $this->strPrintName=='' || $this->strComboName=='' || $this->intColorId=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
		}
	}
	
	public function set($intSampleNo , $intSampleYear , $intRevNo , $strPrintName , $strComboName , $intColorId)
	{
		$result = $this->select('*',null,"intSampleNo='$intSampleNo' and intSampleYear='$intSampleYear' and intRevNo='$intRevNo' and strPrintName='$strPrintName' and strComboName='$strComboName' and intColorId='$intColorId'");	
		if($this->db->numRows()>0)
			$this->setVariables($result);
		else
			throw new Exception("Record not found.");
			
	}
	
	
}
?>

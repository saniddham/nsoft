<?php
class finance_supplier_transaction{
 
	private $db;
	private $table= "finance_supplier_transaction";
	
	//private property
	private $SERIAL_NO;
	private $PO_YEAR;
	private $PO_NO;
	private $SUPPLIER_ID;
	private $CURRENCY_ID;
	private $DOCUMENT_YEAR;
	private $DOCUMENT_NO;
	private $DOCUMENT_TYPE;
	private $PURCHASE_INVOICE_YEAR;
	private $PURCHASE_INVOICE_NO;
	private $LEDGER_ID;
	private $VALUE;
	private $COMPANY_ID;
	private $LOCATION_ID;
	private $USER_ID;
	private $TRANSACTION_DATE_TIME;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('SERIAL_NO'=>'SERIAL_NO',
										'PO_YEAR'=>'PO_YEAR',
										'PO_NO'=>'PO_NO',
										'SUPPLIER_ID'=>'SUPPLIER_ID',
										'CURRENCY_ID'=>'CURRENCY_ID',
										'DOCUMENT_YEAR'=>'DOCUMENT_YEAR',
										'DOCUMENT_NO'=>'DOCUMENT_NO',
										'DOCUMENT_TYPE'=>'DOCUMENT_TYPE',
										'PURCHASE_INVOICE_YEAR'=>'PURCHASE_INVOICE_YEAR',
										'PURCHASE_INVOICE_NO'=>'PURCHASE_INVOICE_NO',
										'LEDGER_ID'=>'LEDGER_ID',
										'VALUE'=>'VALUE',
										'COMPANY_ID'=>'COMPANY_ID',
										'LOCATION_ID'=>'LOCATION_ID',
										'USER_ID'=>'USER_ID',
										'TRANSACTION_DATE_TIME'=>'TRANSACTION_DATE_TIME',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "SERIAL_NO = ".$this->SERIAL_NO."" ;
		$this->commitArray = array();
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun SERIAL_NO
	function getSERIAL_NO()
	{
		$this->validate();
		return $this->SERIAL_NO;
	}
	
	//retun PO_YEAR
	function getPO_YEAR()
	{
		$this->validate();
		return $this->PO_YEAR;
	}
	
	//retun PO_NO
	function getPO_NO()
	{
		$this->validate();
		return $this->PO_NO;
	}
	
	//retun SUPPLIER_ID
	function getSUPPLIER_ID()
	{
		$this->validate();
		return $this->SUPPLIER_ID;
	}
	
	//retun CURRENCY_ID
	function getCURRENCY_ID()
	{
		$this->validate();
		return $this->CURRENCY_ID;
	}
	
	//retun DOCUMENT_YEAR
	function getDOCUMENT_YEAR()
	{
		$this->validate();
		return $this->DOCUMENT_YEAR;
	}
	
	//retun DOCUMENT_NO
	function getDOCUMENT_NO()
	{
		$this->validate();
		return $this->DOCUMENT_NO;
	}
	
	//retun DOCUMENT_TYPE
	function getDOCUMENT_TYPE()
	{
		$this->validate();
		return $this->DOCUMENT_TYPE;
	}
	
	//retun PURCHASE_INVOICE_YEAR
	function getPURCHASE_INVOICE_YEAR()
	{
		$this->validate();
		return $this->PURCHASE_INVOICE_YEAR;
	}
	
	//retun PURCHASE_INVOICE_NO
	function getPURCHASE_INVOICE_NO()
	{
		$this->validate();
		return $this->PURCHASE_INVOICE_NO;
	}
	
	//retun LEDGER_ID
	function getLEDGER_ID()
	{
		$this->validate();
		return $this->LEDGER_ID;
	}
	
	//retun VALUE
	function getVALUE()
	{
		$this->validate();
		return $this->VALUE;
	}
	
	//retun COMPANY_ID
	function getCOMPANY_ID()
	{
		$this->validate();
		return $this->COMPANY_ID;
	}
	
	//retun LOCATION_ID
	function getLOCATION_ID()
	{
		$this->validate();
		return $this->LOCATION_ID;
	}
	
	//retun USER_ID
	function getUSER_ID()
	{
		$this->validate();
		return $this->USER_ID;
	}
	
	//retun TRANSACTION_DATE_TIME
	function getTRANSACTION_DATE_TIME()
	{
		$this->validate();
		return $this->TRANSACTION_DATE_TIME;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set SERIAL_NO
	function setSERIAL_NO($SERIAL_NO)
	{
		array_push($this->commitArray,'SERIAL_NO');
		$this->SERIAL_NO = $SERIAL_NO;
	}
	
	//set PO_YEAR
	function setPO_YEAR($PO_YEAR)
	{
		array_push($this->commitArray,'PO_YEAR');
		$this->PO_YEAR = $PO_YEAR;
	}
	
	//set PO_NO
	function setPO_NO($PO_NO)
	{
		array_push($this->commitArray,'PO_NO');
		$this->PO_NO = $PO_NO;
	}
	
	//set SUPPLIER_ID
	function setSUPPLIER_ID($SUPPLIER_ID)
	{
		array_push($this->commitArray,'SUPPLIER_ID');
		$this->SUPPLIER_ID = $SUPPLIER_ID;
	}
	
	//set CURRENCY_ID
	function setCURRENCY_ID($CURRENCY_ID)
	{
		array_push($this->commitArray,'CURRENCY_ID');
		$this->CURRENCY_ID = $CURRENCY_ID;
	}
	
	//set DOCUMENT_YEAR
	function setDOCUMENT_YEAR($DOCUMENT_YEAR)
	{
		array_push($this->commitArray,'DOCUMENT_YEAR');
		$this->DOCUMENT_YEAR = $DOCUMENT_YEAR;
	}
	
	//set DOCUMENT_NO
	function setDOCUMENT_NO($DOCUMENT_NO)
	{
		array_push($this->commitArray,'DOCUMENT_NO');
		$this->DOCUMENT_NO = $DOCUMENT_NO;
	}
	
	//set DOCUMENT_TYPE
	function setDOCUMENT_TYPE($DOCUMENT_TYPE)
	{
		array_push($this->commitArray,'DOCUMENT_TYPE');
		$this->DOCUMENT_TYPE = $DOCUMENT_TYPE;
	}
	
	//set PURCHASE_INVOICE_YEAR
	function setPURCHASE_INVOICE_YEAR($PURCHASE_INVOICE_YEAR)
	{
		array_push($this->commitArray,'PURCHASE_INVOICE_YEAR');
		$this->PURCHASE_INVOICE_YEAR = $PURCHASE_INVOICE_YEAR;
	}
	
	//set PURCHASE_INVOICE_NO
	function setPURCHASE_INVOICE_NO($PURCHASE_INVOICE_NO)
	{
		array_push($this->commitArray,'PURCHASE_INVOICE_NO');
		$this->PURCHASE_INVOICE_NO = $PURCHASE_INVOICE_NO;
	}
	
	//set LEDGER_ID
	function setLEDGER_ID($LEDGER_ID)
	{
		array_push($this->commitArray,'LEDGER_ID');
		$this->LEDGER_ID = $LEDGER_ID;
	}
	
	//set VALUE
	function setVALUE($VALUE)
	{
		array_push($this->commitArray,'VALUE');
		$this->VALUE = $VALUE;
	}
	
	//set COMPANY_ID
	function setCOMPANY_ID($COMPANY_ID)
	{
		array_push($this->commitArray,'COMPANY_ID');
		$this->COMPANY_ID = $COMPANY_ID;
	}
	
	//set LOCATION_ID
	function setLOCATION_ID($LOCATION_ID)
	{
		array_push($this->commitArray,'LOCATION_ID');
		$this->LOCATION_ID = $LOCATION_ID;
	}
	
	//set USER_ID
	function setUSER_ID($USER_ID)
	{
		array_push($this->commitArray,'USER_ID');
		$this->USER_ID = $USER_ID;
	}
	
	//set TRANSACTION_DATE_TIME
	function setTRANSACTION_DATE_TIME($TRANSACTION_DATE_TIME)
	{
		array_push($this->commitArray,'TRANSACTION_DATE_TIME');
		$this->TRANSACTION_DATE_TIME = $TRANSACTION_DATE_TIME;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->SERIAL_NO=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($SERIAL_NO)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "SERIAL_NO='$SERIAL_NO'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($PO_YEAR,$PO_NO,$SUPPLIER_ID,$CURRENCY_ID,$DOCUMENT_YEAR,$DOCUMENT_NO,$DOCUMENT_TYPE,$PURCHASE_INVOICE_YEAR,$PURCHASE_INVOICE_NO,$LEDGER_ID,$VALUE,$COMPANY_ID,$LOCATION_ID,$USER_ID,$TRANSACTION_DATE_TIME){
		$data = array('PO_YEAR'=>$PO_YEAR 
				,'PO_NO'=>$PO_NO 
				,'SUPPLIER_ID'=>$SUPPLIER_ID 
				,'CURRENCY_ID'=>$CURRENCY_ID 
				,'DOCUMENT_YEAR'=>$DOCUMENT_YEAR 
				,'DOCUMENT_NO'=>$DOCUMENT_NO 
				,'DOCUMENT_TYPE'=>$DOCUMENT_TYPE 
				,'PURCHASE_INVOICE_YEAR'=>$PURCHASE_INVOICE_YEAR 
				,'PURCHASE_INVOICE_NO'=>$PURCHASE_INVOICE_NO 
				,'LEDGER_ID'=>$LEDGER_ID 
				,'VALUE'=>$VALUE 
				,'COMPANY_ID'=>$COMPANY_ID 
				,'LOCATION_ID'=>$LOCATION_ID 
				,'USER_ID'=>$USER_ID 
				,'TRANSACTION_DATE_TIME'=>$TRANSACTION_DATE_TIME 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('SERIAL_NO,SUPPLIER_ID',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['SERIAL_NO'])
				$html .= '<option selected="selected" value="'.$row['SERIAL_NO'].'">'.$row['SUPPLIER_ID'].'</option>';	
			else
				$html .= '<option value="'.$row['SERIAL_NO'].'">'.$row['SUPPLIER_ID'].'</option>';	
		}
		return $html;
	}
	
	public function getInvoiceTransactionDetails($invoiceNo,$invoiceYear)
	{
		$sql = "SELECT 	SPIH.PURCHASE_DATE AS INVOICE_DATE,
				
				COALESCE((SELECT ROUND(SUM(ST.VALUE),2)
				FROM finance_supplier_transaction ST
				WHERE ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
				ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR AND
				ST.DOCUMENT_TYPE='INVOICE'),0) AS invoiceAmount,
				
				COALESCE((SELECT ROUND((SUM(ST.VALUE)*-1),2)
				FROM finance_supplier_transaction ST
				WHERE ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
				ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR AND
				ST.DOCUMENT_TYPE='ADVANCE'),0) AS advancedAmount,
				
				COALESCE((SELECT ROUND(SUM(ST.VALUE),2)
				FROM finance_supplier_transaction ST
				WHERE ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
				ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR AND
				ST.DOCUMENT_TYPE='CREDIT'),0) AS creditAmount,
				
				COALESCE((SELECT ROUND((SUM(ST.VALUE)*-1),2)
				FROM finance_supplier_transaction ST
				WHERE ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
				ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR AND
				ST.DOCUMENT_TYPE='DEBIT'),0) AS debitAmount,
				
				COALESCE((SELECT ROUND((SUM(ST.VALUE)*-1),2)
				FROM finance_supplier_transaction ST
				WHERE ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
				ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR AND
				ST.DOCUMENT_TYPE='PAYMENT'),0) AS paidAmount,
				
				COALESCE((SELECT ROUND(SUM(ST.VALUE),2)
				FROM finance_supplier_transaction ST
				WHERE ST.PURCHASE_INVOICE_NO=SPIH.PURCHASE_INVOICE_NO AND
				ST.PURCHASE_INVOICE_YEAR=SPIH.PURCHASE_INVOICE_YEAR),0) AS toBeDebited
				
				FROM finance_supplier_purchaseinvoice_header SPIH
				WHERE SPIH.STATUS = 1 AND
				SPIH.PURCHASE_INVOICE_NO = '".$invoiceNo."' AND
				SPIH.PURCHASE_INVOICE_YEAR = '".$invoiceYear."'
				GROUP BY SPIH.PURCHASE_INVOICE_NO,SPIH.PURCHASE_INVOICE_YEAR ";
		
		return $this->db->RunQuery($sql);
	}
	public function getAdvancedDetails($supplierId,$currencyId,$companyId)
	{
		$cols	= " finance_supplier_transaction.DOCUMENT_NO,
					finance_supplier_transaction.DOCUMENT_YEAR,
					finance_supplier_transaction.PO_NO,
					finance_supplier_transaction.PO_YEAR,
					SAPH.REMARKS,
					COALESCE(ABS(SUM(VALUE)),0) AS balAmount ";
			
		$join	= " INNER JOIN finance_supplier_advancepayment_header SAPH ON SAPH.ADVANCE_PAYMENT_NO=finance_supplier_transaction.DOCUMENT_NO AND	
					SAPH.ADVANCE_PAYMENT_YEAR=finance_supplier_transaction.DOCUMENT_YEAR ";
		
		$where	= " finance_supplier_transaction.DOCUMENT_TYPE = 'ADVANCE' AND
					finance_supplier_transaction.SUPPLIER_ID = '$supplierId' AND
					finance_supplier_transaction.CURRENCY_ID = '$currencyId' AND
					finance_supplier_transaction.COMPANY_ID = '$companyId' AND
					finance_supplier_transaction.PURCHASE_INVOICE_NO IS NULL AND
					finance_supplier_transaction.PURCHASE_INVOICE_YEAR IS NULL
					GROUP BY finance_supplier_transaction.DOCUMENT_NO,finance_supplier_transaction.DOCUMENT_YEAR
					HAVING balAmount>0 ";
		
		return $this->select($cols,$join,$where);
	}
	public function getAdvanceBalance($advanceNo,$advanceYear)
	{
		$cols	= " COALESCE(ABS(SUM(VALUE)),0) AS balAmount ";
		
		$where	= " finance_supplier_transaction.DOCUMENT_TYPE = 'ADVANCE' AND
					finance_supplier_transaction.PURCHASE_INVOICE_NO IS NULL AND
					finance_supplier_transaction.PURCHASE_INVOICE_YEAR IS NULL
					AND finance_supplier_transaction.DOCUMENT_NO = '$advanceNo'
					AND finance_supplier_transaction.DOCUMENT_YEAR = '$advanceYear'
					GROUP BY finance_supplier_transaction.DOCUMENT_NO,
					finance_supplier_transaction.DOCUMENT_YEAR ";
		
		$result	= $this->select($cols,$join,$where);
		$row	= mysqli_fetch_array($result);
		
		return $row['balAmount'];
	}
	//END }
}
?>
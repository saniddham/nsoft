<?php
class ware_fabricreceiveddetails{
 
	private $db;
	private $table= "ware_fabricreceiveddetails";
	
	//private property
	private $intFabricReceivedNo;
	private $intFabricReceivedYear;
	private $strCutNo;
	private $intSalesOrderId;
	private $intPart;
	private $intGroundColor;
	private $strLineNo;
	private $strSize;
	private $dblQty;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('intFabricReceivedNo'=>'intFabricReceivedNo',
										'intFabricReceivedYear'=>'intFabricReceivedYear',
										'strCutNo'=>'strCutNo',
										'intSalesOrderId'=>'intSalesOrderId',
										'intPart'=>'intPart',
										'intGroundColor'=>'intGroundColor',
										'strLineNo'=>'strLineNo',
										'strSize'=>'strSize',
										'dblQty'=>'dblQty',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "intFabricReceivedNo = ".$this->intFabricReceivedNo." and intFabricReceivedYear = ".$this->intFabricReceivedYear." and strCutNo = ".$this->strCutNo." and intSalesOrderId = ".$this->intSalesOrderId." and strSize = ".$this->strSize."" ;
		$this->commitArray	= array();
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun intFabricReceivedNo
	function getintFabricReceivedNo()
	{
		$this->validate();
		return $this->intFabricReceivedNo;
	}
	
	//retun intFabricReceivedYear
	function getintFabricReceivedYear()
	{
		$this->validate();
		return $this->intFabricReceivedYear;
	}
	
	//retun strCutNo
	function getstrCutNo()
	{
		$this->validate();
		return $this->strCutNo;
	}
	
	//retun intSalesOrderId
	function getintSalesOrderId()
	{
		$this->validate();
		return $this->intSalesOrderId;
	}
	
	//retun intPart
	function getintPart()
	{
		$this->validate();
		return $this->intPart;
	}
	
	//retun intGroundColor
	function getintGroundColor()
	{
		$this->validate();
		return $this->intGroundColor;
	}
	
	//retun strLineNo
	function getstrLineNo()
	{
		$this->validate();
		return $this->strLineNo;
	}
	
	//retun strSize
	function getstrSize()
	{
		$this->validate();
		return $this->strSize;
	}
	
	//retun dblQty
	function getdblQty()
	{
		$this->validate();
		return $this->dblQty;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set intFabricReceivedNo
	function setintFabricReceivedNo($intFabricReceivedNo)
	{
		array_push($this->commitArray,'intFabricReceivedNo');
		$this->intFabricReceivedNo = $intFabricReceivedNo;
	}
	
	//set intFabricReceivedYear
	function setintFabricReceivedYear($intFabricReceivedYear)
	{
		array_push($this->commitArray,'intFabricReceivedYear');
		$this->intFabricReceivedYear = $intFabricReceivedYear;
	}
	
	//set strCutNo
	function setstrCutNo($strCutNo)
	{
		array_push($this->commitArray,'strCutNo');
		$this->strCutNo = $strCutNo;
	}
	
	//set intSalesOrderId
	function setintSalesOrderId($intSalesOrderId)
	{
		array_push($this->commitArray,'intSalesOrderId');
		$this->intSalesOrderId = $intSalesOrderId;
	}
	
	//set intPart
	function setintPart($intPart)
	{
		array_push($this->commitArray,'intPart');
		$this->intPart = $intPart;
	}
	
	//set intGroundColor
	function setintGroundColor($intGroundColor)
	{
		array_push($this->commitArray,'intGroundColor');
		$this->intGroundColor = $intGroundColor;
	}
	
	//set strLineNo
	function setstrLineNo($strLineNo)
	{
		array_push($this->commitArray,'strLineNo');
		$this->strLineNo = $strLineNo;
	}
	
	//set strSize
	function setstrSize($strSize)
	{
		array_push($this->commitArray,'strSize');
		$this->strSize = $strSize;
	}
	
	//set dblQty
	function setdblQty($dblQty)
	{
		array_push($this->commitArray,'dblQty');
		$this->dblQty = $dblQty;
	}
	
	//END }
	
	//validate primary values
	private function validate()
	{
		if($this->intFabricReceivedNo=='' || $this->intFabricReceivedYear=='' || $this->strCutNo=='' || $this->intSalesOrderId=='' || $this->strSize=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($intFabricReceivedNo , $intFabricReceivedYear , $strCutNo , $intSalesOrderId , $strSize)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "intFabricReceivedNo='$intFabricReceivedNo' and intFabricReceivedYear='$intFabricReceivedYear' and strCutNo='$strCutNo' and intSalesOrderId='$intSalesOrderId' and strSize='$strSize'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($intFabricReceivedNo,$intFabricReceivedYear,$strCutNo,$intSalesOrderId,$intPart,$intGroundColor,$strLineNo,$strSize,$dblQty){
		$data = array('intFabricReceivedNo'=>$intFabricReceivedNo 
				,'intFabricReceivedYear'=>$intFabricReceivedYear 
				,'strCutNo'=>$strCutNo 
				,'intSalesOrderId'=>$intSalesOrderId 
				,'intPart'=>$intPart 
				,'intGroundColor'=>$intGroundColor 
				,'strLineNo'=>$strLineNo 
				,'strSize'=>$strSize 
				,'dblQty'=>$dblQty 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('intFabricReceivedNo,intFabricReceivedYear',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intFabricReceivedNo'])
				$html .= '<option selected="selected" value="'.$row['intFabricReceivedNo'].'">'.$row['intFabricReceivedYear'].'</option>';	
			else
				$html .= '<option value="'.$row['intFabricReceivedNo'].'">'.$row['intFabricReceivedYear'].'</option>';	
		}
		return $html;
	}
#BEGIN - USER DEFINED FUNCTIONS {
	public function getFabricInQty($orderNo,$orderYear,$salesOrderId,$deci)
	{
		if($salesOrderId!='')
			$para	= "AND ware_fabricreceiveddetails.intSalesOrderId = $salesOrderId";
			
		$cols		= " ROUND(COALESCE(SUM(dblQty),0),$deci) AS FABRIC_IN_QTY";
		
		$join		= "INNER JOIN ware_fabricreceivedheader H
						ON H.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo
						  AND H.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear";
		
		$where		= "H.intStatus = 1
						AND H.intOrderNo = $orderNo
						AND H.intOrderYear = $orderYear
						$para";
		
		$result		= $this->select($cols,$join,$where,$order=null,$limit=null);
		return mysqli_fetch_array($result);
	}
	
	public function getLastAndFirstDate($orderNo,$orderYear,$salesOrderId,$deci)
	{
		if($salesOrderId!='')
			$para	= "AND ware_fabricreceiveddetails.intSalesOrderId = $salesOrderId";
			
		$cols		= " DATE(MAX(dtmCreateDate)) AS MAX_DATE,
  						DATE(MIN(dtmCreateDate)) AS MIN_DATE";
		
		$join		= "INNER JOIN ware_fabricreceivedheader H
    					ON H.intFabricReceivedNo = ware_fabricreceiveddetails.intFabricReceivedNo
      					AND H.intFabricReceivedYear = ware_fabricreceiveddetails.intFabricReceivedYear";
		
		$where		= "H.intStatus = 1
						AND H.intOrderNo = $orderNo
						AND H.intOrderYear = $orderYear
						$para";
		
		$result		= $this->select($cols,$join,$where,$order=null,$limit=null);
		return mysqli_fetch_array($result);
	}
#END	}
}
?>
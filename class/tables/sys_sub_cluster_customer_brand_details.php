
<?php

class sys_sub_cluster_customer_brand_details{
 
	private $db;
	private $table= "sys_sub_cluster_customer_brand_details";
	
	function __construct($db)
	{
		$this->db = $db;
		$this->db->field_array_set(array('SUB_CLUSTER_ID'=>'SUB_CLUSTER_ID',
										'CUSTOMER_ID'=>'CUSTOMER_ID',
										'CUST_LOCATION_ID'=>'CUST_LOCATION_ID',
										'BRAND'=>'BRAND',
										'EMAIL_TYPE'=>'EMAIL_TYPE',
										'EMAIL_LOCATION'=>'EMAIL_LOCATION',
										'EMAIL_BRAND'=>'EMAIL_BRAND',
										'COMMON'=>'COMMON',
										'MARKETER'=>'MARKETER',
										'MARKETING_ASSISTANT'=>'MARKETING_ASSISTANT',
										'ACCOUNTS'=>'ACCOUNTS',
										'OTHERS'=>'OTHERS',
										'CLUSTER_HEAD'=>'CLUSTER_HEAD',
										));
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  
		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	
}
?>

<?php
class mst_financecurrency{
 
	private $db;
	private $table= "mst_financecurrency";
	
	//private property
	private $intId;
	private $strCode;
	private $strSymbol;
	private $strDescription;
	private $CBSLCURRENCY;
	private $intChartOfAccountId;
	private $intStatus;
	private $intCreator;
	private $dtmCreateDate;
	private $intModifyer;
	private $dtmModifyDate;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('intId'=>'intId',
										'strCode'=>'strCode',
										'strSymbol'=>'strSymbol',
										'strDescription'=>'strDescription',
										'CBSLCURRENCY'=>'CBSLCURRENCY',
										'intChartOfAccountId'=>'intChartOfAccountId',
										'intStatus'=>'intStatus',
										'intCreator'=>'intCreator',
										'dtmCreateDate'=>'dtmCreateDate',
										'intModifyer'=>'intModifyer',
										'dtmModifyDate'=>'dtmModifyDate',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "intId = ".$this->intId."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun intId
	function getintId()
	{
		$this->validate();
		return $this->intId;
	}
	
	//retun strCode
	function getstrCode()
	{
		$this->validate();
		return $this->strCode;
	}
	
	//retun strSymbol
	function getstrSymbol()
	{
		$this->validate();
		return $this->strSymbol;
	}
	
	//retun strDescription
	function getstrDescription()
	{
		$this->validate();
		return $this->strDescription;
	}
	
	//retun CBSLCURRENCY
	function getCBSLCURRENCY()
	{
		$this->validate();
		return $this->CBSLCURRENCY;
	}
	
	//retun intChartOfAccountId
	function getintChartOfAccountId()
	{
		$this->validate();
		return $this->intChartOfAccountId;
	}
	
	//retun intStatus
	function getintStatus()
	{
		$this->validate();
		return $this->intStatus;
	}
	
	//retun intCreator
	function getintCreator()
	{
		$this->validate();
		return $this->intCreator;
	}
	
	//retun dtmCreateDate
	function getdtmCreateDate()
	{
		$this->validate();
		return $this->dtmCreateDate;
	}
	
	//retun intModifyer
	function getintModifyer()
	{
		$this->validate();
		return $this->intModifyer;
	}
	
	//retun dtmModifyDate
	function getdtmModifyDate()
	{
		$this->validate();
		return $this->dtmModifyDate;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set intId
	function setintId($intId)
	{
		array_push($this->commitArray,'intId');
		$this->intId = $intId;
	}
	
	//set strCode
	function setstrCode($strCode)
	{
		array_push($this->commitArray,'strCode');
		$this->strCode = $strCode;
	}
	
	//set strSymbol
	function setstrSymbol($strSymbol)
	{
		array_push($this->commitArray,'strSymbol');
		$this->strSymbol = $strSymbol;
	}
	
	//set strDescription
	function setstrDescription($strDescription)
	{
		array_push($this->commitArray,'strDescription');
		$this->strDescription = $strDescription;
	}
	
	//set CBSLCURRENCY
	function setCBSLCURRENCY($CBSLCURRENCY)
	{
		array_push($this->commitArray,'CBSLCURRENCY');
		$this->CBSLCURRENCY = $CBSLCURRENCY;
	}
	
	//set intChartOfAccountId
	function setintChartOfAccountId($intChartOfAccountId)
	{
		array_push($this->commitArray,'intChartOfAccountId');
		$this->intChartOfAccountId = $intChartOfAccountId;
	}
	
	//set intStatus
	function setintStatus($intStatus)
	{
		array_push($this->commitArray,'intStatus');
		$this->intStatus = $intStatus;
	}
	
	//set intCreator
	function setintCreator($intCreator)
	{
		array_push($this->commitArray,'intCreator');
		$this->intCreator = $intCreator;
	}
	
	//set dtmCreateDate
	function setdtmCreateDate($dtmCreateDate)
	{
		array_push($this->commitArray,'dtmCreateDate');
		$this->dtmCreateDate = $dtmCreateDate;
	}
	
	//set intModifyer
	function setintModifyer($intModifyer)
	{
		array_push($this->commitArray,'intModifyer');
		$this->intModifyer = $intModifyer;
	}
	
	//set dtmModifyDate
	function setdtmModifyDate($dtmModifyDate)
	{
		array_push($this->commitArray,'dtmModifyDate');
		$this->dtmModifyDate = $dtmModifyDate;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->intId=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($intId)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "intId='$intId'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}
	
	public function setCodeToGetData($code){
	
		$result = $this->select('*',null,"strCode='$code'");	
		if($this->db->numRows()>0)
			$this->setVariables($result);
		else
			throw new Exception("Record not found.");
		
	}
		
	
	//insert as parameters
	public function insertRec($intId,$strCode,$strSymbol,$strDescription,$CBSLCURRENCY,$intChartOfAccountId,$intStatus,$intCreator,$dtmCreateDate,$intModifyer,$dtmModifyDate){
		$data = array('intId'=>$intId 
				,'strCode'=>$strCode 
				,'strSymbol'=>$strSymbol 
				,'strDescription'=>$strDescription 
				,'CBSLCURRENCY'=>$CBSLCURRENCY 
				,'intChartOfAccountId'=>$intChartOfAccountId 
				,'intStatus'=>$intStatus 
				,'intCreator'=>$intCreator 
				,'dtmCreateDate'=>$dtmCreateDate 
				,'intModifyer'=>$intModifyer 
				,'dtmModifyDate'=>$dtmModifyDate 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('intId,strCode',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intId'])
				$html .= '<option selected="selected" value="'.$row['intId'].'">'.$row['strCode'].'</option>';	
			else
				$html .= '<option value="'.$row['intId'].'">'.$row['strCode'].'</option>';	
		}
		return $html;
	}
	public function getExceedReceiveCurrencyCombo($defaultValue=null,$companyId)
	{
		$cols	= " DISTINCT mst_financecurrency.intId   AS CURRENCY_ID,
				    mst_financecurrency.strCode AS CURRENCY_NAME ";
		
		$joins	= " INNER JOIN finance_customer_transaction FCT
					ON FCT.CURRENCY_ID = mst_financecurrency.intId ";
		
		$where	= " FCT.COMPANY_ID = '$companyId'
					AND mst_financecurrency.intStatus  = 1
					GROUP BY FCT.INVOICE_NO,FCT.INVOICE_YEAR ";
		
		$order	= " mst_financecurrency.strCode ";
		
		$result = $this->select($cols,$joins,$where,$order);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result))
		{
			if($defaultValue==$row['CURRENCY_ID'])
				$html .= '<option selected="selected" value="'.$row['CURRENCY_ID'].'">'.$row['CURRENCY_NAME'].'</option>';	
			else
				$html .= '<option value="'.$row['CURRENCY_ID'].'">'.$row['CURRENCY_NAME'].'</option>';	
		}
		return $html;
	}
	//END }
}
?>
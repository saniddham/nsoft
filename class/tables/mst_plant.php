<?php
class mst_plant{

	private $db;
	private $table= "mst_plant";

	//private property
	private $intPlantId;
	private $strPlantName;
	private $intStatus;
	private $strPlantHeadName;
	private $strPlantHeadContactNo;
	private $SUB_CLUSTER_ID;
	private $MAIN_CLUSTER_ID;
	private $commitArray = array();
	private $field_array = array();
	

	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('intPlantId'=>'intPlantId',
										'strPlantName'=>'strPlantName',
										'intStatus'=>'intStatus',
										'strPlantHeadName'=>'strPlantHeadName',
										'strPlantHeadContactNo'=>'strPlantHeadContactNo',
										'SUB_CLUSTER_ID'=>'SUB_CLUSTER_ID',
										'MAIN_CLUSTER_ID'=>'MAIN_CLUSTER_ID',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit );
		return $result;
	}

	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data);
		return $this->db->getResult();


	}

	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}

	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();
	}

	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}

	public function get_field_array(){
		return $this->field_array;
	}

	function commit($type='update')
	{
		//commit update
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{
			$data[$v] = $this->$v;
		}
		$where		= "intPlantId = ".$this->intPlantId."" ;
		$this->commitArray = array();

		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}

	//BEGIN - public functions for private properties {
	
	//retun intPlantId
	function getintPlantId()
	{
		$this->validate();
		return $this->intPlantId;
	}
	
	//retun strPlantName
	function getstrPlantName()
	{
		$this->validate();
		return $this->strPlantName;
	}
	
	//retun intStatus
	function getintStatus()
	{
		$this->validate();
		return $this->intStatus;
	}
	
	//retun strPlantHeadName
	function getstrPlantHeadName()
	{
		$this->validate();
		return $this->strPlantHeadName;
	}
	
	//retun strPlantHeadContactNo
	function getstrPlantHeadContactNo()
	{
		$this->validate();
		return $this->strPlantHeadContactNo;
	}
	
	//retun SUB_CLUSTER_ID
	function getSUB_CLUSTER_ID()
	{
		$this->validate();
		return $this->SUB_CLUSTER_ID;
	}
	
	//retun MAIN_CLUSTER_ID
	function getMAIN_CLUSTER_ID()
	{
		$this->validate();
		return $this->MAIN_CLUSTER_ID;
	}
	
	//END }

	//BEGIN - public function set {
	
	//set intPlantId
	function setintPlantId($intPlantId)
	{
		array_push($this->commitArray,'intPlantId');
		$this->intPlantId = $intPlantId;
	}
	
	//set strPlantName
	function setstrPlantName($strPlantName)
	{
		array_push($this->commitArray,'strPlantName');
		$this->strPlantName = $strPlantName;
	}
	
	//set intStatus
	function setintStatus($intStatus)
	{
		array_push($this->commitArray,'intStatus');
		$this->intStatus = $intStatus;
	}
	
	//set strPlantHeadName
	function setstrPlantHeadName($strPlantHeadName)
	{
		array_push($this->commitArray,'strPlantHeadName');
		$this->strPlantHeadName = $strPlantHeadName;
	}
	
	//set strPlantHeadContactNo
	function setstrPlantHeadContactNo($strPlantHeadContactNo)
	{
		array_push($this->commitArray,'strPlantHeadContactNo');
		$this->strPlantHeadContactNo = $strPlantHeadContactNo;
	}
	
	//set SUB_CLUSTER_ID
	function setSUB_CLUSTER_ID($SUB_CLUSTER_ID)
	{
		array_push($this->commitArray,'SUB_CLUSTER_ID');
		$this->SUB_CLUSTER_ID = $SUB_CLUSTER_ID;
	}
	
	//set MAIN_CLUSTER_ID
	function setMAIN_CLUSTER_ID($MAIN_CLUSTER_ID)
	{
		array_push($this->commitArray,'MAIN_CLUSTER_ID');
		$this->MAIN_CLUSTER_ID = $MAIN_CLUSTER_ID;
	}
	
	//END }

	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->intPlantId=='')
			throw new exception("Set primary values first");
		else
			return true;
	}

	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}

	public function set($intPlantId)
	{
		$cols	= "*";

		$join	= NULL;

		$where	= "intPlantId='$intPlantId'";

		$result = $this->select($cols,$join,$where);
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}

	//insert as parameters
	public function insertRec($strPlantName,$intStatus,$strPlantHeadName,$strPlantHeadContactNo,$SUB_CLUSTER_ID,$MAIN_CLUSTER_ID){
		$data = array('strPlantName'=>$strPlantName 
				,'intStatus'=>$intStatus 
				,'strPlantHeadName'=>$strPlantHeadName 
				,'strPlantHeadContactNo'=>$strPlantHeadContactNo 
				,'SUB_CLUSTER_ID'=>$SUB_CLUSTER_ID 
				,'MAIN_CLUSTER_ID'=>$MAIN_CLUSTER_ID 
				);
		return $this->insert($data);
	}

	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('intPlantId,strPlantName',  null, $where = $where);
		$html = '<option value=""></option>';
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intPlantId'])
				$html .= '<option selected="selected" value="'.$row['intPlantId'].'">'.$row['strPlantName'].'</option>';
			else
				$html .= '<option value="'.$row['intPlantId'].'">'.$row['strPlantName'].'</option>';
		}
		return $html;
	}
	
	//END }
}
?>
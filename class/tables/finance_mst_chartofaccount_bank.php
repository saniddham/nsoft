<?php
class finance_mst_chartofaccount_bank{
 
	private $db;
	private $table= "finance_mst_chartofaccount_bank";
	
	//private property
	private $BANK_ID;
	private $CURRENCY_ID;
	private $CHART_OF_ACCOUNT_ID;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('BANK_ID'=>'BANK_ID',
										'CURRENCY_ID'=>'CURRENCY_ID',
										'CHART_OF_ACCOUNT_ID'=>'CHART_OF_ACCOUNT_ID',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "CHART_OF_ACCOUNT_ID = ".$this->CHART_OF_ACCOUNT_ID."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun BANK_ID
	function getBANK_ID()
	{
		$this->validate();
		return $this->BANK_ID;
	}
	
	//retun CURRENCY_ID
	function getCURRENCY_ID()
	{
		$this->validate();
		return $this->CURRENCY_ID;
	}
	
	//retun CHART_OF_ACCOUNT_ID
	function getCHART_OF_ACCOUNT_ID()
	{
		$this->validate();
		return $this->CHART_OF_ACCOUNT_ID;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set BANK_ID
	function setBANK_ID($BANK_ID)
	{
		array_push($this->commitArray,'BANK_ID');
		$this->BANK_ID = $BANK_ID;
	}
	
	//set CURRENCY_ID
	function setCURRENCY_ID($CURRENCY_ID)
	{
		array_push($this->commitArray,'CURRENCY_ID');
		$this->CURRENCY_ID = $CURRENCY_ID;
	}
	
	//set CHART_OF_ACCOUNT_ID
	function setCHART_OF_ACCOUNT_ID($CHART_OF_ACCOUNT_ID)
	{
		array_push($this->commitArray,'CHART_OF_ACCOUNT_ID');
		$this->CHART_OF_ACCOUNT_ID = $CHART_OF_ACCOUNT_ID;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->CHART_OF_ACCOUNT_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($CHART_OF_ACCOUNT_ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "CHART_OF_ACCOUNT_ID='$CHART_OF_ACCOUNT_ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($BANK_ID,$CURRENCY_ID,$CHART_OF_ACCOUNT_ID){
		$data = array('BANK_ID'=>$BANK_ID 
				,'CURRENCY_ID'=>$CURRENCY_ID 
				,'CHART_OF_ACCOUNT_ID'=>$CHART_OF_ACCOUNT_ID 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('BANK_ID,CHART_OF_ACCOUNT_ID',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['BANK_ID'])
				$html .= '<option selected="selected" value="'.$row['BANK_ID'].'">'.$row['CHART_OF_ACCOUNT_ID'].'</option>';	
			else
				$html .= '<option value="'.$row['BANK_ID'].'">'.$row['CHART_OF_ACCOUNT_ID'].'</option>';	
		}
		return $html;
	}
	public function checkBankForGL($glID)
	{
		$selectRslt	= $this->select('BANK_ID',NULL,"CHART_OF_ACCOUNT_ID ='$glID'",NULL,NULL);
		if($this->db->numRows()<=0)
		{
			$response['type']	= false;
			$response['msg']	= 'Bank is not available for selected Chart of Account';
		}
		else
		{
			$response['type']	= true;
			$response['msg']	= '';	
		}
		return $response;
	}
	//END }
}
?>
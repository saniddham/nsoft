<?php
include_once (MAIN_ROOT."class/tables/mst_financetaxgroup.php");		$mst_financetaxgroup			= new mst_financetaxgroup($db);

class finance_mst_chartofaccount_tax{
 
	private $db;
	private $table= "finance_mst_chartofaccount_tax";
	
	//private property
	private $TAX_ID;
	private $CHART_OF_ACCOUNT_ID;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('TAX_ID'=>'TAX_ID',
										'CHART_OF_ACCOUNT_ID'=>'CHART_OF_ACCOUNT_ID',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "TAX_ID = ".$this->TAX_ID."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun TAX_ID
	function getTAX_ID()
	{
		$this->validate();
		return $this->TAX_ID;
	}
	
	//retun CHART_OF_ACCOUNT_ID
	function getCHART_OF_ACCOUNT_ID()
	{
		$this->validate();
		return $this->CHART_OF_ACCOUNT_ID;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set TAX_ID
	function setTAX_ID($TAX_ID)
	{
		array_push($this->commitArray,'TAX_ID');
		$this->TAX_ID = $TAX_ID;
	}
	
	//set CHART_OF_ACCOUNT_ID
	function setCHART_OF_ACCOUNT_ID($CHART_OF_ACCOUNT_ID)
	{
		array_push($this->commitArray,'CHART_OF_ACCOUNT_ID');
		$this->CHART_OF_ACCOUNT_ID = $CHART_OF_ACCOUNT_ID;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->TAX_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($TAX_ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "TAX_ID='$TAX_ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($TAX_ID,$CHART_OF_ACCOUNT_ID){
		$data = array('TAX_ID'=>$TAX_ID 
				,'CHART_OF_ACCOUNT_ID'=>$CHART_OF_ACCOUNT_ID 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('TAX_ID,CHART_OF_ACCOUNT_ID',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['TAX_ID'])
				$html .= '<option selected="selected" value="'.$row['TAX_ID'].'">'.$row['CHART_OF_ACCOUNT_ID'].'</option>';	
			else
				$html .= '<option value="'.$row['TAX_ID'].'">'.$row['CHART_OF_ACCOUNT_ID'].'</option>';	
		}
		return $html;
	}
	public function getTaxGL_ID($taxID)
	{
		global $mst_financetaxgroup;
		$taxGLResult	= $this->select('CHART_OF_ACCOUNT_ID',NULL,"TAX_ID = '$taxID'",NULL,NULL);
		$rowTaxGL		= mysqli_fetch_array($taxGLResult);
		$taxGL			= $rowTaxGL['CHART_OF_ACCOUNT_ID'];
		
		if($taxGL == '')
		{
			$mst_financetaxgroup->set($taxID);			
			$taxCode	= $mst_financetaxgroup->getstrCode();
			throw new Exception("No GL Account available for tax code :". $taxCode);
		}
		else
			return $taxGL;
		}
	//END }
}
?>
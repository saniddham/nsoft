<?php
class sys_mail_eventusers{
 
	private $db;
	private $table= "sys_mail_eventusers";
	
	//private property
	private $intMailEventId;
	private $intUserId;
	private $intCompanyId;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('intMailEventId'=>'intMailEventId',
										'intUserId'=>'intUserId',
										'intCompanyId'=>'intCompanyId',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "intMailEventId = ".$this->intMailEventId." and intUserId = ".$this->intUserId." and intCompanyId = ".$this->intCompanyId."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun intMailEventId
	function getintMailEventId()
	{
		$this->validate();
		return $this->intMailEventId;
	}
	
	//retun intUserId
	function getintUserId()
	{
		$this->validate();
		return $this->intUserId;
	}
	
	//retun intCompanyId
	function getintCompanyId()
	{
		$this->validate();
		return $this->intCompanyId;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set intMailEventId
	function setintMailEventId($intMailEventId)
	{
		array_push($this->commitArray,'intMailEventId');
		$this->intMailEventId = $intMailEventId;
	}
	
	//set intUserId
	function setintUserId($intUserId)
	{
		array_push($this->commitArray,'intUserId');
		$this->intUserId = $intUserId;
	}
	
	//set intCompanyId
	function setintCompanyId($intCompanyId)
	{
		array_push($this->commitArray,'intCompanyId');
		$this->intCompanyId = $intCompanyId;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->intMailEventId=='' || $this->intUserId=='' || $this->intCompanyId=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($intMailEventId , $intUserId , $intCompanyId)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "intMailEventId='$intMailEventId' and intUserId='$intUserId' and intCompanyId='$intCompanyId'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($intMailEventId,$intUserId,$intCompanyId){
		$data = array('intMailEventId'=>$intMailEventId 
				,'intUserId'=>$intUserId 
				,'intCompanyId'=>$intCompanyId 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('intMailEventId,intUserId',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intMailEventId'])
				$html .= '<option selected="selected" value="'.$row['intMailEventId'].'">'.$row['intUserId'].'</option>';	
			else
				$html .= '<option value="'.$row['intMailEventId'].'">'.$row['intUserId'].'</option>';	
		}
		return $html;
	}
	
	//END }
	
#BEGIN - USER DEFINED FUNCTIONS {	
	public function getUserEmail($eventId,$locationId)
	{
		$cols	= "SU.strFullName AS USER_NAME,
  					SU.strEmail    AS USER_EMAIL";
		$join 	= "INNER JOIN sys_users SU
   						ON SU.intUserId = sys_mail_eventusers.intUserId";
		$where	= "intMailEventId = $eventId
						AND intCompanyId = $locationId
						AND SU.intStatus = 1";
		return $this->select($cols,$join,$where,$order = null, $limit = null);
	}
#END	}
}
?>
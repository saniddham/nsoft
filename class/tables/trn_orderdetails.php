
<?php

class trn_orderdetails{
 
	private $db;
	private $table= "trn_orderdetails";
	
	//private property
	private $intOrderNo;
	private $intOrderYear;
	private $strSalesOrderNo;
	private $intSalesOrderId;
	private $strLineNo;
	private $strStyleNo;
	private $strGraphicNo;
	private $intSampleNo;
	private $intSampleYear;
	private $strCombo;
	private $strPrintName;
	private $intRevisionNo;
	private $intPart;
	private $intQty;
	private $dblPrice;
	private $dblToleratePercentage;
	private $dblOverCutPercentage;
	private $dblDamagePercentage;
	private $dtPSD;
	private $dtDeliveryDate;
	private $intViewInSalesProjection;
	private $STATUS;
	private $TECHNIQUE_GROUP_ID;
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->db->field_array_set(array('intOrderNo'=>'intOrderNo',
										'intOrderYear'=>'intOrderYear',
										'strSalesOrderNo'=>'strSalesOrderNo',
										'intSalesOrderId'=>'intSalesOrderId',
										'strLineNo'=>'strLineNo',
										'strStyleNo'=>'strStyleNo',
										'strGraphicNo'=>'strGraphicNo',
										'intSampleNo'=>'intSampleNo',
										'intSampleYear'=>'intSampleYear',
										'strCombo'=>'strCombo',
										'strPrintName'=>'strPrintName',
										'intRevisionNo'=>'intRevisionNo',
										'intPart'=>'intPart',
										'intQty'=>'intQty',
										'dblPrice'=>'dblPrice',
										'dblToleratePercentage'=>'dblToleratePercentage',
										'dblOverCutPercentage'=>'dblOverCutPercentage',
										'dblDamagePercentage'=>'dblDamagePercentage',
										'dtPSD'=>'dtPSD',
										'dtDeliveryDate'=>'dtDeliveryDate',
										'intViewInSalesProjection'=>'intViewInSalesProjection',
										'TECHNIQUE_GROUP_ID'=>'TECHNIQUE_GROUP_ID',
										'STATUS'=>'STATUS'
										));
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  
		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	//public functions for private properties
	
	//retun intOrderNo
	function getintOrderNo()
	{
		$this->validate();
		return $this->intOrderNo;
	}
	
	//retun intOrderYear
	function getintOrderYear()
	{
		$this->validate();
		return $this->intOrderYear;
	}
	
	//retun strSalesOrderNo
	function getstrSalesOrderNo()
	{
		$this->validate();
		return $this->strSalesOrderNo;
	}
	
	//retun intSalesOrderId
	function getintSalesOrderId()
	{
		$this->validate();
		return $this->intSalesOrderId;
	}
	
	//retun strLineNo
	function getstrLineNo()
	{
		$this->validate();
		return $this->strLineNo;
	}
	
	//retun strStyleNo
	function getstrStyleNo()
	{
		$this->validate();
		return $this->strStyleNo;
	}
	
	//retun strGraphicNo
	function getstrGraphicNo()
	{
		$this->validate();
		return $this->strGraphicNo;
	}
	
	//retun intSampleNo
	function getintSampleNo()
	{
		$this->validate();
		return $this->intSampleNo;
	}
	
	//retun intSampleYear
	function getintSampleYear()
	{
		$this->validate();
		return $this->intSampleYear;
	}
	
	//retun strCombo
	function getstrCombo()
	{
		$this->validate();
		return $this->strCombo;
	}
	
	//retun strPrintName
	function getstrPrintName()
	{
		$this->validate();
		return $this->strPrintName;
	}
	
	//retun intRevisionNo
	function getintRevisionNo()
	{
		$this->validate();
		return $this->intRevisionNo;
	}
	
	//retun intPart
	function getintPart()
	{
		$this->validate();
		return $this->intPart;
	}
	
	//retun intQty
	function getintQty()
	{
		$this->validate();
		return $this->intQty;
	}
	
	//retun dblPrice
	function getdblPrice()
	{
		$this->validate();
		return $this->dblPrice;
	}
	
	//retun dblToleratePercentage
	function getdblToleratePercentage()
	{
		$this->validate();
		return $this->dblToleratePercentage;
	}
	
	//retun dblOverCutPercentage
	function getdblOverCutPercentage()
	{
		$this->validate();
		return $this->dblOverCutPercentage;
	}
	
	//retun dblDamagePercentage
	function getdblDamagePercentage()
	{
		$this->validate();
		return $this->dblDamagePercentage;
	}
	
	//retun dtPSD
	function getdtPSD()
	{
		$this->validate();
		return $this->dtPSD;
	}
	
	//retun dtDeliveryDate
	function getdtDeliveryDate()
	{
		$this->validate();
		return $this->dtDeliveryDate;
	}
	
	//retun intViewInSalesProjection
	function getintViewInSalesProjection()
	{
		$this->validate();
		return $this->intViewInSalesProjection;
	}
	
	function getTECHNIQUE_GROUP_ID()
	{
		$this->validate();
		return $this->TECHNIQUE_GROUP_ID;
	}

	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}

	//validate primary values
	private function validate()
	{
		if($this->intOrderNo=='' || $this->intOrderYear=='' || $this->intSalesOrderId=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
		}
	}
	
	public function set($intOrderNo , $intOrderYear , $intSalesOrderId)
	{
		$result = $this->select('*',null,"intOrderNo='$intOrderNo' and intOrderYear='$intOrderYear' and intSalesOrderId='$intSalesOrderId'");	
		if($this->db->numRows()>0)
			$this->setVariables($result);
		else
			throw new Exception("Record not found.");
			
	}
	
	public function getsalesOrderForOrder($orderNo,$orderYear,$salesOrder)
	{
		$cols	= " trn_orderdetails.intSalesOrderId";
		
		$where  = "trn_orderdetails.intOrderNo = '$orderNo' AND
				   trn_orderdetails.intOrderYear = '$orderYear'";
		if($salesOrder != '')
		$where  .= " AND trn_orderdetails.intSalesOrderId = '$salesOrder' ";
			//echo "select ".$cols." from trn_orderdetails where ".$where;	
		$result	= $this->select($cols,$join = null,$where, $order = null, $limit = null);	
 		return 	$result;
	}
	
	public function getOpenedOrderYears($defaultValue)
	{
		$cols	= "DISTINCT trn_orderheader.intOrderYear ";
		
		$join	= " INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear 
 ";
		
		$where	= "( STATUS <>-10 OR STATUS IS NULL )  and trn_orderheader.intStatus=1  ";
		//	echo $sql	="select ".$cols." from trn_orderdetails ".$join." where".$where;
		$result = $this->select($cols,$join,$where);	
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intOrderYear'])
				$html .= '<option selected="selected" value="'.$row['intOrderYear'].'">'.$row['intOrderYear'].'</option>';	
			else
				$html .= '<option value="'.$row['intOrderYear'].'">'.$row['intOrderYear'].'</option>';	
		}
	//////////////////saved///////////
		$cols	= "DISTINCT trn_sales_order_close_header.ORDER_YEAR AS intOrderYear ";
		
		$join	= " 
		RIGHT JOIN trn_sales_order_close_header ON trn_orderdetails.intOrderYear=trn_sales_order_close_header.ORDER_YEAR AND trn_orderdetails.intOrderNo=trn_sales_order_close_header.ORDER_NO
		LEFT JOIN trn_orderheader ON trn_orderheader.intOrderYear=trn_sales_order_close_header.ORDER_YEAR AND trn_orderheader.intOrderNo=trn_sales_order_close_header.ORDER_NO 
 ";
		
		$where	= "( trn_orderdetails.STATUS =-10 OR trn_orderheader.intStatus <> 1 ) AND trn_sales_order_close_header.ORDER_YEAR='$defaultValue' ";
		//	echo $sql	="select ".$cols." from trn_orderdetails ".$join." where".$where;
		$result = $this->select($cols,$join,$where);	
		$html .= '';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intOrderYear'])
				$html .= '<option selected="selected" value="'.$row['intOrderYear'].'">'.$row['intOrderYear'].'</option>';	
			else
				$html .= '<option value="'.$row['intOrderYear'].'">'.$row['intOrderYear'].'</option>';	
		}
	
		return $html;
	}
	
	public function getOpenedOrderNos($year,$defaultValue)
	{
		$cols	= "DISTINCT trn_orderheader.intOrderNo ";
		
		$join	= " INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear 
 ";
		
		$where	= "( STATUS <>-10 OR STATUS IS NULL )  and trn_orderheader.intStatus=1 AND trn_orderheader.intOrderYear='$year'  ";
			//echo $sql	="select ".$cols." from trn_orderdetails ".$join." where".$where;
		$result = $this->select($cols,$join,$where);	
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intOrderNo'])
				$html .= '<option selected="selected" value="'.$row['intOrderNo'].'">'.$row['intOrderNo'].'</option>';	
			else
				$html .= '<option value="'.$row['intOrderNo'].'">'.$row['intOrderNo'].'</option>';	
		}
		
		//////////////SAVED////////////////////
		$cols	= "DISTINCT trn_sales_order_close_header.ORDER_NO AS intOrderNo ";
		
		$join	= " 
		RIGHT JOIN trn_sales_order_close_header ON trn_orderdetails.intOrderYear=trn_sales_order_close_header.ORDER_YEAR AND trn_orderdetails.intOrderNo=trn_sales_order_close_header.ORDER_NO
		LEFT JOIN trn_orderheader ON trn_orderheader.intOrderYear=trn_sales_order_close_header.ORDER_YEAR AND trn_orderheader.intOrderNo=trn_sales_order_close_header.ORDER_NO 
 ";
		
		$where	= "( trn_orderdetails.STATUS =-10 OR trn_orderheader.intStatus <> 1 ) AND trn_sales_order_close_header.ORDER_NO='$defaultValue' ";
			//echo $sql	="select ".$cols." from trn_orderdetails ".$join." where".$where;
		$result = $this->select($cols,$join,$where);	
		$html .= '';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intOrderNo'])
				$html .= '<option selected="selected" value="'.$row['intOrderNo'].'">'.$row['intOrderNo'].'</option>';	
			else
				$html .= '<option value="'.$row['intOrderNo'].'">'.$row['intOrderNo'].'</option>';	
		}
		
		return $html;
	}
	
	public function getOpenedOrderDetails($orderYear,$orderNo,$serialNo,$serialYear){

		
		
		$cols	= "
					trn_orderdetails.strSalesOrderNo,intSalesOrderId,
					trn_orderdetails.strGraphicNo,
					trn_orderdetails.intSampleNo,
					trn_orderdetails.intSampleYear,
					trn_orderdetails.strCombo,
					trn_orderdetails.strLineNo,
					trn_orderdetails.strStyleNo,
					trn_orderdetails.strPrintName,
					trn_orderdetails.intRevisionNo,
					trn_orderdetails.intPart,
					trn_orderdetails.intQty,
					trn_orderdetails.dblPrice,
					trn_orderdetails.dblOverCutPercentage,
					trn_orderdetails.dblDamagePercentage,
					trn_orderdetails.dtPSD,
					trn_orderdetails.dtDeliveryDate,
					trn_orderdetails.TECHNIQUE_GROUP_ID,
					if(trn_orderdetails.`STATUS`=-10,'Closed','Open') as STATUS,
					if(trn_sales_order_close_details.`SALES_ORDER_ID` IS NOT NULL,1,0) as SAVED_STATUS 
				 ";		
 		$join	=" LEFT JOIN trn_sales_order_close_details ON trn_orderdetails.intOrderNo = trn_sales_order_close_details.ORDER_NO AND trn_orderdetails.intOrderYear = trn_sales_order_close_details.ORDER_YEAR AND trn_orderdetails.intSalesOrderId = trn_sales_order_close_details.SALES_ORDER_ID
			
					";
		
		if($serialNo=='' || $serialYear==''){
		$where	.= "(trn_orderdetails.`STATUS` <>-10 OR trn_orderdetails.`STATUS` IS NULL) AND 
					trn_orderdetails.intOrderYear = '$orderYear' AND 
					trn_orderdetails.intOrderNo = '$orderNo' ";
		}
		else{			
		$where	.= "trn_sales_order_close_details.CLOSE_NO = '$serialNo' AND 
					trn_sales_order_close_details.CLOSE_YEAR = '$serialYear' ";
		}
					
		$where	.= "GROUP BY
					trn_orderdetails.intOrderYear, 
					trn_orderdetails.intOrderNo, 
					trn_orderdetails.intSalesOrderId 
					ORDER BY 
					trn_orderdetails.intOrderYear ASC, 
					trn_orderdetails.intOrderNo ASC, 
					trn_orderdetails.intSalesOrderId ";
		//echo $sql	="select ".$cols." from trn_orderdetails ".$join." where ".$where;
		return $result = $this->select($cols,$join,$where);	
	}
	
	
	public function getClosedOrderYears($defaultValue)
	{
		$cols	= "DISTINCT trn_orderheader.intOrderYear ";
		
		$join	= " INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear 
 ";
		
		$where	= "( STATUS =-10)  and trn_orderheader.intStatus=1  ";
		//	echo $sql	="select ".$cols." from trn_orderdetails ".$join." where".$where;
		$result = $this->select($cols,$join,$where);	
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intOrderYear'])
				$html .= '<option selected="selected" value="'.$row['intOrderYear'].'">'.$row['intOrderYear'].'</option>';	
			else
				$html .= '<option value="'.$row['intOrderYear'].'">'.$row['intOrderYear'].'</option>';	
		}
		//////////////SAVED////////////////////
		$cols	= "DISTINCT trn_sales_order_open_requisition_header.ORDER_YEAR AS intOrderYear ";
		
		$join	= " 
		RIGHT JOIN trn_sales_order_open_requisition_header ON trn_orderdetails.intOrderYear=trn_sales_order_open_requisition_header.ORDER_YEAR AND trn_orderdetails.intOrderNo=trn_sales_order_open_requisition_header.ORDER_NO
		LEFT JOIN trn_orderheader ON trn_orderheader.intOrderYear=trn_sales_order_open_requisition_header.ORDER_YEAR AND trn_orderheader.intOrderNo=trn_sales_order_open_requisition_header.ORDER_NO 
 ";
		
		$where	= "( trn_orderdetails.STATUS =<> -10 OR trn_orderheader.intStatus <> 1 )";
		//	echo $sql	="select ".$cols." from trn_orderdetails ".$join." where".$where;
		$result = $this->select($cols,$join,$where);	
		$html .= '';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intOrderYear'])
				$html .= '<option selected="selected" value="'.$row['intOrderYear'].'">'.$row['intOrderYear'].'</option>';	
			else
				$html .= '<option value="'.$row['intOrderYear'].'">'.$row['intOrderYear'].'</option>';	
		}
		
		
		return $html;
	}
	
	public function getClosedOrderNos($year,$defaultValue)
	{
		$cols	= "DISTINCT trn_orderheader.intOrderNo ";
		
		$join	= " INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear 
 ";
		
		$where	= "( STATUS =-10)  and trn_orderheader.intStatus=1 and trn_orderheader.intOrderYear ='$year'  ";
		//	echo $sql	="select ".$cols." from trn_orderdetails ".$join." where".$where;
		$result = $this->select($cols,$join,$where);	
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intOrderNo'])
				$html .= '<option selected="selected" value="'.$row['intOrderNo'].'">'.$row['intOrderNo'].'</option>';	
			else
				$html .= '<option value="'.$row['intOrderNo'].'">'.$row['intOrderNo'].'</option>';	
		}
		//////////////SAVED////////////////////
		$cols	= "DISTINCT trn_sales_order_open_requisition_header.ORDER_NO AS intOrderNo ";
		
		$join	= " 
		RIGHT JOIN trn_sales_order_open_requisition_header ON trn_orderdetails.intOrderYear=trn_sales_order_open_requisition_header.ORDER_YEAR AND trn_orderdetails.intOrderNo=trn_sales_order_open_requisition_header.ORDER_NO
		LEFT JOIN trn_orderheader ON trn_orderheader.intOrderYear=trn_sales_order_open_requisition_header.ORDER_YEAR AND trn_orderheader.intOrderNo=trn_sales_order_open_requisition_header.ORDER_NO 
 ";
		
		$where	= "( trn_orderdetails.STATUS =<> -10 OR trn_orderheader.intStatus <> 1 )  and trn_orderheader.intOrderYear ='$year' ";
		//	echo $sql	="select ".$cols." from trn_orderdetails ".$join." where".$where;
		$result = $this->select($cols,$join,$where);	
		$html .= '';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intOrderNo'])
				$html .= '<option selected="selected" value="'.$row['intOrderNo'].'">'.$row['intOrderNo'].'</option>';	
			else
				$html .= '<option value="'.$row['intOrderNo'].'">'.$row['intOrderNo'].'</option>';	
		}
		
		
		return $html;
	}
	
	public function getClosedOrderDetails($orderYear,$orderNo,$serialNo,$serialYear){

		
		
		$cols	= "
					trn_orderdetails.strSalesOrderNo,intSalesOrderId,
					trn_orderdetails.strGraphicNo,
					trn_orderdetails.intSampleNo,
					trn_orderdetails.intSampleYear,
					trn_orderdetails.strCombo,
					trn_orderdetails.strLineNo,
					trn_orderdetails.strStyleNo,
					trn_orderdetails.strPrintName,
					trn_orderdetails.intRevisionNo,
					trn_orderdetails.intPart,
					trn_orderdetails.intQty,
					trn_orderdetails.dblPrice,
					trn_orderdetails.dblOverCutPercentage,
					trn_orderdetails.dblDamagePercentage,
					trn_orderdetails.dtPSD,
					trn_orderdetails.dtDeliveryDate,
					trn_orderdetails.TECHNIQUE_GROUP_ID,
					if(trn_orderdetails.`STATUS`=-10,'Closed','Open') as STATUS,
					if(trn_sales_order_open_requisition_details.`SALES_ORDER_ID` IS NOT NULL,1,0) as SAVED_STATUS 
				 ";		
 		$join	=" LEFT JOIN trn_sales_order_open_requisition_details ON trn_orderdetails.intOrderNo = trn_sales_order_open_requisition_details.ORDER_NO AND trn_orderdetails.intOrderYear = trn_sales_order_open_requisition_details.ORDER_YEAR AND trn_orderdetails.intSalesOrderId = trn_sales_order_open_requisition_details.SALES_ORDER_ID
			
					";
		
		if($serialNo=='' || $serialYear==''){
		$where	.= "trn_orderdetails.`STATUS` =-10 AND 
					trn_orderdetails.intOrderYear = '$orderYear' AND 
					trn_orderdetails.intOrderNo = '$orderNo' ";
		}
		else{			
		$where	.= "trn_sales_order_open_requisition_details.REQUISITION_NO = '$serialNo' AND 
					trn_sales_order_open_requisition_details.REQUISITION_YEAR = '$serialYear' ";
		}
					
		$where	.= "GROUP BY
					trn_orderdetails.intOrderYear, 
					trn_orderdetails.intOrderNo, 
					trn_orderdetails.intSalesOrderId 
					ORDER BY 
					trn_orderdetails.intOrderYear ASC, 
					trn_orderdetails.intOrderNo ASC, 
					trn_orderdetails.intSalesOrderId ";
		//echo $sql	="select ".$cols." from trn_orderdetails ".$join." where ".$where;
		return $result = $this->select($cols,$join,$where);	
	}
	
}
?>

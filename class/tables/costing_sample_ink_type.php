<?php
class costing_sample_ink_type{

	private $db;
	private $table= "costing_sample_ink_type";

	//private property
	private $SAMPLE_NO;
	private $SAMPLE_YEAR;
	private $REVISION;
	private $COMBO;
	private $PRINT;
	private $COLOUR;
	private $TECHNIQUE;
	private $INK_TYPE;
	private $SHOTS;
	private $COST_PER_SQ_INCH;
	private $SQUARE_INCHES;
	private $USAGE;
	private $TOTAL_COST_PRICE;
	private $commitArray = array();
	private $field_array = array();
	

	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('SAMPLE_NO'=>'SAMPLE_NO',
										'SAMPLE_YEAR'=>'SAMPLE_YEAR',
										'REVISION'=>'REVISION',
										'COMBO'=>'COMBO',
										'PRINT'=>'PRINT',
										'COLOUR'=>'COLOUR',
										'TECHNIQUE'=>'TECHNIQUE',
										'INK_TYPE'=>'INK_TYPE',
										'SHOTS'=>'SHOTS',
										'COST_PER_SQ_INCH'=>'COST_PER_SQ_INCH',
										'SQUARE_INCHES'=>'SQUARE_INCHES',
										'USAGE'=>'USAGE',
										'TOTAL_COST_PRICE'=>'TOTAL_COST_PRICE',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit );
		return $result;
	}

	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data);
		return $this->db->getResult();


	}

	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}

	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();
	}

	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}

	public function get_field_array(){
		return $this->field_array;
	}

	function commit($type='update')
	{
		//commit update
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{
			$data[$v] = $this->$v;
		}
		$where		= "SAMPLE_NO = ".$this->SAMPLE_NO." and SAMPLE_YEAR = ".$this->SAMPLE_YEAR." and REVISION = ".$this->REVISION." and COMBO = '".$this->COMBO."' and PRINT = '".$this->PRINT."' and COLOUR = ".$this->COLOUR." and TECHNIQUE = ".$this->TECHNIQUE." and INK_TYPE = ".$this->INK_TYPE."" ;
		$this->commitArray = array();

		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}

	//BEGIN - public functions for private properties {
	
	//retun SAMPLE_NO
	function getSAMPLE_NO()
	{
		$this->validate();
		return $this->SAMPLE_NO;
	}
	
	//retun SAMPLE_YEAR
	function getSAMPLE_YEAR()
	{
		$this->validate();
		return $this->SAMPLE_YEAR;
	}
	
	//retun REVISION
	function getREVISION()
	{
		$this->validate();
		return $this->REVISION;
	}
	
	//retun COMBO
	function getCOMBO()
	{
		$this->validate();
		return $this->COMBO;
	}
	
	//retun PRINT
	function getPRINT()
	{
		$this->validate();
		return $this->PRINT;
	}
	
	//retun COLOUR
	function getCOLOUR()
	{
		$this->validate();
		return $this->COLOUR;
	}
	
	//retun TECHNIQUE
	function getTECHNIQUE()
	{
		$this->validate();
		return $this->TECHNIQUE;
	}
	
	//retun INK_TYPE
	function getINK_TYPE()
	{
		$this->validate();
		return $this->INK_TYPE;
	}
	
	//retun SHOTS
	function getSHOTS()
	{
		$this->validate();
		return $this->SHOTS;
	}
	
	//retun COST_PER_SQ_INCH
	function getCOST_PER_SQ_INCH()
	{
		$this->validate();
		return $this->COST_PER_SQ_INCH;
	}
	
	//retun SQUARE_INCHES
	function getSQUARE_INCHES()
	{
		$this->validate();
		return $this->SQUARE_INCHES;
	}
	
	//retun USAGE
	function getUSAGE()
	{
		$this->validate();
		return $this->USAGE;
	}
	
	//retun TOTAL_COST_PRICE
	function getTOTAL_COST_PRICE()
	{
		$this->validate();
		return $this->TOTAL_COST_PRICE;
	}
	
	//END }

	//BEGIN - public function set {
	
	//set SAMPLE_NO
	function setSAMPLE_NO($SAMPLE_NO)
	{
		array_push($this->commitArray,'SAMPLE_NO');
		$this->SAMPLE_NO = $SAMPLE_NO;
	}
	
	//set SAMPLE_YEAR
	function setSAMPLE_YEAR($SAMPLE_YEAR)
	{
		array_push($this->commitArray,'SAMPLE_YEAR');
		$this->SAMPLE_YEAR = $SAMPLE_YEAR;
	}
	
	//set REVISION
	function setREVISION($REVISION)
	{
		array_push($this->commitArray,'REVISION');
		$this->REVISION = $REVISION;
	}
	
	//set COMBO
	function setCOMBO($COMBO)
	{
		array_push($this->commitArray,'COMBO');
		$this->COMBO = $COMBO;
	}
	
	//set PRINT
	function setPRINT($PRINT)
	{
		array_push($this->commitArray,'PRINT');
		$this->PRINT = $PRINT;
	}
	
	//set COLOUR
	function setCOLOUR($COLOUR)
	{
		array_push($this->commitArray,'COLOUR');
		$this->COLOUR = $COLOUR;
	}
	
	//set TECHNIQUE
	function setTECHNIQUE($TECHNIQUE)
	{
		array_push($this->commitArray,'TECHNIQUE');
		$this->TECHNIQUE = $TECHNIQUE;
	}
	
	//set INK_TYPE
	function setINK_TYPE($INK_TYPE)
	{
		array_push($this->commitArray,'INK_TYPE');
		$this->INK_TYPE = $INK_TYPE;
	}
	
	//set SHOTS
	function setSHOTS($SHOTS)
	{
		array_push($this->commitArray,'SHOTS');
		$this->SHOTS = $SHOTS;
	}
	
	//set COST_PER_SQ_INCH
	function setCOST_PER_SQ_INCH($COST_PER_SQ_INCH)
	{
		array_push($this->commitArray,'COST_PER_SQ_INCH');
		$this->COST_PER_SQ_INCH = $COST_PER_SQ_INCH;
	}
	
	//set SQUARE_INCHES
	function setSQUARE_INCHES($SQUARE_INCHES)
	{
		array_push($this->commitArray,'SQUARE_INCHES');
		$this->SQUARE_INCHES = $SQUARE_INCHES;
	}
	
	//set USAGE
	function setUSAGE($USAGE)
	{
		array_push($this->commitArray,'USAGE');
		$this->USAGE = $USAGE;
	}
	
	//set TOTAL_COST_PRICE
	function setTOTAL_COST_PRICE($TOTAL_COST_PRICE)
	{
		array_push($this->commitArray,'TOTAL_COST_PRICE');
		$this->TOTAL_COST_PRICE = $TOTAL_COST_PRICE;
	}
	
	//END }

	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->SAMPLE_NO=='' || $this->SAMPLE_YEAR=='' || $this->REVISION=='' || $this->COMBO=='' || $this->PRINT=='' || $this->COLOUR=='' || $this->TECHNIQUE=='' || $this->INK_TYPE=='')
			throw new exception("Set primary values first");
		else
			return true;
	}

	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}

	public function set($SAMPLE_NO , $SAMPLE_YEAR , $REVISION , $COMBO , $PRINT , $COLOUR , $TECHNIQUE , $INK_TYPE)
	{
		$cols	= "*";

		$join	= NULL;

		$where	= "SAMPLE_NO='$SAMPLE_NO' and SAMPLE_YEAR='$SAMPLE_YEAR' and REVISION='$REVISION' and COMBO='$COMBO' and PRINT='$PRINT' and COLOUR='$COLOUR' and TECHNIQUE='$TECHNIQUE' and INK_TYPE='$INK_TYPE'";

		$result = $this->select($cols,$join,$where);
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}

	//insert as parameters
	public function insertRec($SAMPLE_NO,$SAMPLE_YEAR,$REVISION,$COMBO,$PRINT,$COLOUR,$TECHNIQUE,$INK_TYPE,$SHOTS,$COST_PER_SQ_INCH,$SQUARE_INCHES,$USAGE,$TOTAL_COST_PRICE){
		$data = array('SAMPLE_NO'=>$SAMPLE_NO 
				,'SAMPLE_YEAR'=>$SAMPLE_YEAR 
				,'REVISION'=>$REVISION 
				,'COMBO'=>$COMBO 
				,'PRINT'=>$PRINT 
				,'COLOUR'=>$COLOUR 
				,'TECHNIQUE'=>$TECHNIQUE 
				,'INK_TYPE'=>$INK_TYPE 
				,'SHOTS'=>$SHOTS 
				,'COST_PER_SQ_INCH'=>$COST_PER_SQ_INCH 
				,'SQUARE_INCHES'=>$SQUARE_INCHES 
				,'USAGE'=>$USAGE 
				,'TOTAL_COST_PRICE'=>$TOTAL_COST_PRICE 
				);
		return $this->insert($data);
	}

	public function getCombo_samp($defaultValue=null,$where=null){
		$result = $this->select('SAMPLE_NO,SAMPLE_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['SAMPLE_NO'])
				$html .= '<option selected="selected" value="'.$row['SAMPLE_NO'].'">'.$row['SAMPLE_YEAR'].'</option>';
			else
				$html .= '<option value="'.$row['SAMPLE_NO'].'">'.$row['SAMPLE_YEAR'].'</option>';
		}
		return $html;
	}
	
	//END }
}
?>
<?php
class finance_pettycash_employees{

	private $db;
	private $table= "finance_pettycash_employees";

	//private property
	private $EMPLOYEE_ID;
	private $commitArray = array();
	private $field_array = array();
	

	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('EMPLOYEE_ID'=>'EMPLOYEE_ID',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit );
		return $result;
	}

	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data);
		return $this->db->getResult();


	}

	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}

	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();
	}

	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}

	public function get_field_array(){
		return $this->field_array;
	}

	function commit($type='update')
	{
		//commit update
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{
			$data[$v] = $this->$v;
		}
		$where		= "" ;
		$this->commitArray = array();

		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}

	//BEGIN - public functions for private properties {
	
	//retun EMPLOYEE_ID
	function getEMPLOYEE_ID()
	{
		$this->validate();
		return $this->EMPLOYEE_ID;
	}
	
	//END }

	//BEGIN - public function set {
	
	//set EMPLOYEE_ID
	function setEMPLOYEE_ID($EMPLOYEE_ID)
	{
		array_push($this->commitArray,'EMPLOYEE_ID');
		$this->EMPLOYEE_ID = $EMPLOYEE_ID;
	}
	
	//END }

	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		//if($this->EMPLOYEE_ID == '')
			//throw new exception("Set primary values first");
		//else
			return true;
	}

	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}

	public function set()
	{
		$cols	= "*";

		$join	= NULL;

		$where	= "1=1";

		$result = $this->select($cols,$join,$where);
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}

	//insert as parameters
	public function insertRec($EMPLOYEE_ID){
		$data = array('EMPLOYEE_ID'=>$EMPLOYEE_ID 
				);
		return $this->insert($data);
	}

	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('EMPLOYEE_ID,EMPLOYEE_ID',  null, $where = $where);
		$html = '<option value=""></option>';
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['EMPLOYEE_ID'])
				$html .= '<option selected="selected" value="'.$row['EMPLOYEE_ID'].'">'.$row['EMPLOYEE_ID'].'</option>';
			else
				$html .= '<option value="'.$row['EMPLOYEE_ID'].'">'.$row['EMPLOYEE_ID'].'</option>';
		}
		return $html;
	}
	public function getPettyEmploye($empID=null,$dbName,$locationId)
	{//echo $empID;
		$result	= $this->getPettyEmploye_result($dbName,$locationId);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($empID==$row['EMPLOYEE_ID'])
				$html .= '<option selected="selected" value="'.$row['EMPLOYEE_ID'].'">'.$row['EMP_NAME'].'</option>';	
			else
				$html .= '<option value="'.$row['EMPLOYEE_ID'].'">'.$row['EMP_NAME'].'</option>';	
		}
		return $html;	
	}
	
	public function getPettyEmploye_result($dbName,$locationId)
	{
		$cols	= "	finance_pettycash_employees.EMPLOYEE_ID,
					QE.strInitialName AS EMP_NAME";
		$join	= " INNER JOIN $dbName.mst_employee QE ON finance_pettycash_employees.EMPLOYEE_ID = QE.intEmployeeId
					INNER JOIN $dbName.mst_locations QML ON QML.intId=QE.intLocationId
					INNER JOIN mst_locations NML ON NML.PAYROLL_LOCATION=QML.intId";
		$where	= "	QE.Active <> -10 AND
					NML.intId = '$locationId'"	;
					
		$result	= $this->select($cols,$join,$where,"EMP_NAME",NULL);
		return $result;
		/*$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($empID==$row['EMPLOYEE_ID'])
				$html .= '<option selected="selected" value="'.$row['EMPLOYEE_ID'].'">'.$row['EMP_NAME'].'</option>';	
			else
				$html .= '<option value="'.$row['EMPLOYEE_ID'].'">'.$row['EMP_NAME'].'</option>';	
		}
		return $html;
	*/
	}
	public function getEmpNameByID($empID,$dbName,$locationId)
	{
		$cols	= "	finance_pettycash_employees.EMPLOYEE_ID,
					QE.strInitialName AS EMP_NAME";
		$join	= " INNER JOIN $dbName.mst_employee QE ON finance_pettycash_employees.EMPLOYEE_ID = QE.intEmployeeId
					INNER JOIN $dbName.mst_locations QML ON QML.intId=QE.intLocationId
					INNER JOIN mst_locations NML ON NML.PAYROLL_LOCATION=QML.intId";
		$where	= "	QE.Active = 1 AND
					NML.intId = '$locationId' AND
					finance_pettycash_employees.EMPLOYEE_ID = '$empID'" ;
					
		$result	= $this->select($cols,$join,$where,NULL,NULL);
		$row	= mysqli_fetch_array($result);
		return $row['EMP_NAME'];
	}
	//END }
}
?>
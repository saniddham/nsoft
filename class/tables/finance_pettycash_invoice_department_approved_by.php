<?php
class finance_pettycash_invoice_department_approved_by{
 
	private $db;
	private $table= "finance_pettycash_invoice_department_approved_by";
	
	//private property
	private $PETTY_INVOICE_NO;
	private $PETTY_INVOICE_YEAR;
	private $DEPARTMENT_ID;
	private $ITEM_ID;
	private $APPROVE_LEVEL;
	private $APPROVE_BY;
	private $APPROVE_DATE;
	private $STATUS;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('PETTY_INVOICE_NO'=>'PETTY_INVOICE_NO',
										'PETTY_INVOICE_YEAR'=>'PETTY_INVOICE_YEAR',
										'DEPARTMENT_ID'=>'DEPARTMENT_ID',
										'ITEM_ID'=>'ITEM_ID',
										'APPROVE_LEVEL'=>'APPROVE_LEVEL',
										'APPROVE_BY'=>'APPROVE_BY',
										'APPROVE_DATE'=>'APPROVE_DATE',
										'STATUS'=>'STATUS',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "PETTY_INVOICE_NO = ".$this->PETTY_INVOICE_NO." and PETTY_INVOICE_YEAR = ".$this->PETTY_INVOICE_YEAR." and DEPARTMENT_ID = ".$this->DEPARTMENT_ID." and APPROVE_LEVEL = ".$this->APPROVE_LEVEL." and STATUS = ".$this->STATUS." and ITEM_ID = ".$this->ITEM_ID."" ;
		$this->commitArray = array();
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun PETTY_INVOICE_NO
	function getPETTY_INVOICE_NO()
	{
		$this->validate();
		return $this->PETTY_INVOICE_NO;
	}
	
	//retun PETTY_INVOICE_YEAR
	function getPETTY_INVOICE_YEAR()
	{
		$this->validate();
		return $this->PETTY_INVOICE_YEAR;
	}
	
	//retun DEPARTMENT_ID
	function getDEPARTMENT_ID()
	{
		$this->validate();
		return $this->DEPARTMENT_ID;
	}
	
	//retun ITEM_ID
	function getITEM_ID()
	{
		$this->validate();
		return $this->ITEM_ID;
	}
	
	//retun APPROVE_LEVEL
	function getAPPROVE_LEVEL()
	{
		$this->validate();
		return $this->APPROVE_LEVEL;
	}
	
	//retun APPROVE_BY
	function getAPPROVE_BY()
	{
		$this->validate();
		return $this->APPROVE_BY;
	}
	
	//retun APPROVE_DATE
	function getAPPROVE_DATE()
	{
		$this->validate();
		return $this->APPROVE_DATE;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set PETTY_INVOICE_NO
	function setPETTY_INVOICE_NO($PETTY_INVOICE_NO)
	{
		array_push($this->commitArray,'PETTY_INVOICE_NO');
		$this->PETTY_INVOICE_NO = $PETTY_INVOICE_NO;
	}
	
	//set PETTY_INVOICE_YEAR
	function setPETTY_INVOICE_YEAR($PETTY_INVOICE_YEAR)
	{
		array_push($this->commitArray,'PETTY_INVOICE_YEAR');
		$this->PETTY_INVOICE_YEAR = $PETTY_INVOICE_YEAR;
	}
	
	//set DEPARTMENT_ID
	function setDEPARTMENT_ID($DEPARTMENT_ID)
	{
		array_push($this->commitArray,'DEPARTMENT_ID');
		$this->DEPARTMENT_ID = $DEPARTMENT_ID;
	}
	
	//set ITEM_ID
	function setITEM_ID($ITEM_ID)
	{
		array_push($this->commitArray,'ITEM_ID');
		$this->ITEM_ID = $ITEM_ID;
	}
	
	//set APPROVE_LEVEL
	function setAPPROVE_LEVEL($APPROVE_LEVEL)
	{
		array_push($this->commitArray,'APPROVE_LEVEL');
		$this->APPROVE_LEVEL = $APPROVE_LEVEL;
	}
	
	//set APPROVE_BY
	function setAPPROVE_BY($APPROVE_BY)
	{
		array_push($this->commitArray,'APPROVE_BY');
		$this->APPROVE_BY = $APPROVE_BY;
	}
	
	//set APPROVE_DATE
	function setAPPROVE_DATE($APPROVE_DATE)
	{
		array_push($this->commitArray,'APPROVE_DATE');
		$this->APPROVE_DATE = $APPROVE_DATE;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->PETTY_INVOICE_NO=='' || $this->PETTY_INVOICE_YEAR=='' || $this->DEPARTMENT_ID=='' || $this->APPROVE_LEVEL=='' || $this->STATUS=='' || $this->ITEM_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($PETTY_INVOICE_NO , $PETTY_INVOICE_YEAR , $DEPARTMENT_ID , $APPROVE_LEVEL , $STATUS , $ITEM_ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "PETTY_INVOICE_NO='$PETTY_INVOICE_NO' and PETTY_INVOICE_YEAR='$PETTY_INVOICE_YEAR' and DEPARTMENT_ID='$DEPARTMENT_ID' and APPROVE_LEVEL='$APPROVE_LEVEL' and STATUS='$STATUS' and ITEM_ID='$ITEM_ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($PETTY_INVOICE_NO,$PETTY_INVOICE_YEAR,$DEPARTMENT_ID,$ITEM_ID,$APPROVE_LEVEL,$APPROVE_BY,$APPROVE_DATE,$STATUS){
		$data = array('PETTY_INVOICE_NO'=>$PETTY_INVOICE_NO 
				,'PETTY_INVOICE_YEAR'=>$PETTY_INVOICE_YEAR 
				,'DEPARTMENT_ID'=>$DEPARTMENT_ID 
				,'ITEM_ID'=>$ITEM_ID 
				,'APPROVE_LEVEL'=>$APPROVE_LEVEL 
				,'APPROVE_BY'=>$APPROVE_BY 
				,'APPROVE_DATE'=>$APPROVE_DATE 
				,'STATUS'=>$STATUS 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('PETTY_INVOICE_NO,PETTY_INVOICE_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['PETTY_INVOICE_NO'])
				$html .= '<option selected="selected" value="'.$row['PETTY_INVOICE_NO'].'">'.$row['PETTY_INVOICE_YEAR'].'</option>';	
			else
				$html .= '<option value="'.$row['PETTY_INVOICE_NO'].'">'.$row['PETTY_INVOICE_YEAR'].'</option>';	
		}
		return $html;
	}
	
	public function getDeptWiseApproveDetails($invoiceNo,$invoiceYear,$departmentId,$itemId)
	{
		$cols	= "	finance_pettycash_invoice_department_approved_by.APPROVE_DATE AS dtApprovedDate ,
					sys_users.strUserName as UserName,
					finance_pettycash_invoice_department_approved_by.APPROVE_LEVEL as intApproveLevelNo";
		$join	= " Inner Join sys_users ON finance_pettycash_invoice_department_approved_by.APPROVE_BY = sys_users.intUserId";
		$where	= " finance_pettycash_invoice_department_approved_by.PETTY_INVOICE_NO =  '$invoiceNo' AND
					finance_pettycash_invoice_department_approved_by.PETTY_INVOICE_YEAR =  '$invoiceYear'  AND
					finance_pettycash_invoice_department_approved_by.DEPARTMENT_ID =  '$departmentId' AND 
					finance_pettycash_invoice_department_approved_by.ITEM_ID	= '$itemId' AND
					finance_pettycash_invoice_department_approved_by.STATUS = 0     
					order by APPROVE_DATE asc"	;
				
		$result	= $this->select($cols,$join,$where,NULL,NULL);
		return $result;
	}
	
	public function updateMaxStatus($invoiceNo,$invoiceYear)
	{
		$select		= " MAX(STATUS) AS MAX_STATUS ";
		$where		= " PETTY_INVOICE_NO = '".$invoiceNo."' AND
						PETTY_INVOICE_YEAR = '".$invoiceYear."' ";
		
		$result 	= $this->select($select,NULL,$where, NULL,NULL);	
		$row		= mysqli_fetch_array($result);
		
		$maxStatus	= $row['MAX_STATUS']+1;
		
		$data  		= array('STATUS' => $maxStatus);
		
		$where		= ' PETTY_INVOICE_NO = "'.$invoiceNo.'" AND 
						PETTY_INVOICE_YEAR = "'.$invoiceYear.'" AND
						STATUS = 0 ' ;
		$result_arr	= $this->update($data,$where);
		if(!$result_arr['status'])
			throw new Exception($result_arr['msg']);	
	}
	//END }
}
?>
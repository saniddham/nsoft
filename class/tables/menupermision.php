<?php
class menupermision{

	private $db;
	private $table= "menupermision";

	//private property
	private $intMenuId;
	private $intUserId;
	private $intView;
	private $intAdd;
	private $intEdit;
	private $intDelete;
	private $int1Approval;
	private $int2Approval;
	private $int3Approval;
	private $int4Approval;
	private $int5Approval;
	private $intSendToApproval;
	private $intCancel;
	private $intReject;
	private $intRevise;
	private $intExportToExcel;
	private $intExportToPDF;
	private $commitArray = array();
	private $field_array = array();
	

	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('intMenuId'=>'intMenuId',
										'intUserId'=>'intUserId',
										'intView'=>'intView',
										'intAdd'=>'intAdd',
										'intEdit'=>'intEdit',
										'intDelete'=>'intDelete',
										'int1Approval'=>'int1Approval',
										'int2Approval'=>'int2Approval',
										'int3Approval'=>'int3Approval',
										'int4Approval'=>'int4Approval',
										'int5Approval'=>'int5Approval',
										'intSendToApproval'=>'intSendToApproval',
										'intCancel'=>'intCancel',
										'intReject'=>'intReject',
										'intRevise'=>'intRevise',
										'intExportToExcel'=>'intExportToExcel',
										'intExportToPDF'=>'intExportToPDF',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit );
		return $result;
	}

	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data);
		return $this->db->getResult();


	}

	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}

	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();
	}

	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}

	public function get_field_array(){
		return $this->field_array;
	}

	function commit($type='update')
	{
		//commit update
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{
			$data[$v] = $this->$v;
		}
		$where		= "intMenuId = ".$this->intMenuId." and intUserId = ".$this->intUserId."" ;
		$this->commitArray = array();

		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}

	//BEGIN - public functions for private properties {
	
	//retun intMenuId
	function getintMenuId()
	{
		$this->validate();
		return $this->intMenuId;
	}
	
	//retun intUserId
	function getintUserId()
	{
		$this->validate();
		return $this->intUserId;
	}
	
	//retun intView
	function getintView()
	{
		$this->validate();
		return $this->intView;
	}
	
	//retun intAdd
	function getintAdd()
	{
		$this->validate();
		return $this->intAdd;
	}
	
	//retun intEdit
	function getintEdit()
	{
		$this->validate();
		return $this->intEdit;
	}
	
	//retun intDelete
	function getintDelete()
	{
		$this->validate();
		return $this->intDelete;
	}
	
	//retun int1Approval
	function getint1Approval()
	{
		$this->validate();
		return $this->int1Approval;
	}
	
	//retun int2Approval
	function getint2Approval()
	{
		$this->validate();
		return $this->int2Approval;
	}
	
	//retun int3Approval
	function getint3Approval()
	{
		$this->validate();
		return $this->int3Approval;
	}
	
	//retun int4Approval
	function getint4Approval()
	{
		$this->validate();
		return $this->int4Approval;
	}
	
	//retun int5Approval
	function getint5Approval()
	{
		$this->validate();
		return $this->int5Approval;
	}
	
	//retun intSendToApproval
	function getintSendToApproval()
	{
		$this->validate();
		return $this->intSendToApproval;
	}
	
	//retun intCancel
	function getintCancel()
	{
		$this->validate();
		return $this->intCancel;
	}
	
	//retun intReject
	function getintReject()
	{
		$this->validate();
		return $this->intReject;
	}
	
	//retun intRevise
	function getintRevise()
	{
		$this->validate();
		return $this->intRevise;
	}
	
	//retun intExportToExcel
	function getintExportToExcel()
	{
		$this->validate();
		return $this->intExportToExcel;
	}
	
	//retun intExportToPDF
	function getintExportToPDF()
	{
		$this->validate();
		return $this->intExportToPDF;
	}
	
	//END }

	//BEGIN - public function set {
	
	//set intMenuId
	function setintMenuId($intMenuId)
	{
		array_push($this->commitArray,'intMenuId');
		$this->intMenuId = $intMenuId;
	}
	
	//set intUserId
	function setintUserId($intUserId)
	{
		array_push($this->commitArray,'intUserId');
		$this->intUserId = $intUserId;
	}
	
	//set intView
	function setintView($intView)
	{
		array_push($this->commitArray,'intView');
		$this->intView = $intView;
	}
	
	//set intAdd
	function setintAdd($intAdd)
	{
		array_push($this->commitArray,'intAdd');
		$this->intAdd = $intAdd;
	}
	
	//set intEdit
	function setintEdit($intEdit)
	{
		array_push($this->commitArray,'intEdit');
		$this->intEdit = $intEdit;
	}
	
	//set intDelete
	function setintDelete($intDelete)
	{
		array_push($this->commitArray,'intDelete');
		$this->intDelete = $intDelete;
	}
	
	//set int1Approval
	function setint1Approval($int1Approval)
	{
		array_push($this->commitArray,'int1Approval');
		$this->int1Approval = $int1Approval;
	}
	
	//set int2Approval
	function setint2Approval($int2Approval)
	{
		array_push($this->commitArray,'int2Approval');
		$this->int2Approval = $int2Approval;
	}
	
	//set int3Approval
	function setint3Approval($int3Approval)
	{
		array_push($this->commitArray,'int3Approval');
		$this->int3Approval = $int3Approval;
	}
	
	//set int4Approval
	function setint4Approval($int4Approval)
	{
		array_push($this->commitArray,'int4Approval');
		$this->int4Approval = $int4Approval;
	}
	
	//set int5Approval
	function setint5Approval($int5Approval)
	{
		array_push($this->commitArray,'int5Approval');
		$this->int5Approval = $int5Approval;
	}
	
	//set intSendToApproval
	function setintSendToApproval($intSendToApproval)
	{
		array_push($this->commitArray,'intSendToApproval');
		$this->intSendToApproval = $intSendToApproval;
	}
	
	//set intCancel
	function setintCancel($intCancel)
	{
		array_push($this->commitArray,'intCancel');
		$this->intCancel = $intCancel;
	}
	
	//set intReject
	function setintReject($intReject)
	{
		array_push($this->commitArray,'intReject');
		$this->intReject = $intReject;
	}
	
	//set intRevise
	function setintRevise($intRevise)
	{
		array_push($this->commitArray,'intRevise');
		$this->intRevise = $intRevise;
	}
	
	//set intExportToExcel
	function setintExportToExcel($intExportToExcel)
	{
		array_push($this->commitArray,'intExportToExcel');
		$this->intExportToExcel = $intExportToExcel;
	}
	
	//set intExportToPDF
	function setintExportToPDF($intExportToPDF)
	{
		array_push($this->commitArray,'intExportToPDF');
		$this->intExportToPDF = $intExportToPDF;
	}
	
	//END }

	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->intMenuId=='' || $this->intUserId=='')
			throw new exception("Set primary values first");
		else
			return true;
	}

	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set_byId($intMenuId , $intUserId)
	{
		$cols	= "*";
		
		$join	= "";
		
		$where	= "menupermision.intMenuId =  '$intMenuId' AND
menupermision.intUserId =  '$intUserId'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			$this->setVariables($result);
		else
			throw new Exception("Record not found.");
	}
	
	public function set($progCode , $intUserId)
	{
		$cols	= "menupermision.*";
		
		$join	= "Inner Join menus ON menupermision.intMenuId = menus.intId";
		
		$where	= "menus.strCode =  '$progCode' AND
menupermision.intUserId =  '$intUserId'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			$this->setVariables($result);
		else
			throw new Exception("Record not found.");
	}

	//insert as parameters
	public function insertRec($intMenuId,$intUserId,$intView,$intAdd,$intEdit,$intDelete,$int1Approval,$int2Approval,$int3Approval,$int4Approval,$int5Approval,$intSendToApproval,$intCancel,$intReject,$intRevise,$intExportToExcel,$intExportToPDF){
		$data = array('intMenuId'=>$intMenuId 
				,'intUserId'=>$intUserId 
				,'intView'=>$intView 
				,'intAdd'=>$intAdd 
				,'intEdit'=>$intEdit 
				,'intDelete'=>$intDelete 
				,'int1Approval'=>$int1Approval 
				,'int2Approval'=>$int2Approval 
				,'int3Approval'=>$int3Approval 
				,'int4Approval'=>$int4Approval 
				,'int5Approval'=>$int5Approval 
				,'intSendToApproval'=>$intSendToApproval 
				,'intCancel'=>$intCancel 
				,'intReject'=>$intReject 
				,'intRevise'=>$intRevise 
				,'intExportToExcel'=>$intExportToExcel 
				,'intExportToPDF'=>$intExportToPDF 
				);
		return $this->insert($data);
	}

	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('intMenuId,intUserId',  null, $where = $where);
		$html = '<option value=""></option>';
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intMenuId'])
				$html .= '<option selected="selected" value="'.$row['intMenuId'].'">'.$row['intUserId'].'</option>';
			else
				$html .= '<option value="'.$row['intMenuId'].'">'.$row['intUserId'].'</option>';
		}
		return $html;
	}
	
	public function checkMenuPermision($type, $savedStatus=null, $savedApproveLevels=null)
	{
		$permission		= 0;
		$error_msg		= '';
		$success_msg	= '';

		if($savedStatus!=null)
			$nextLevel 		= $savedApproveLevels+2-$savedStatus;
		if($type == 'Edit')
		{			
			$fieldName	= "getint$type";
			if($savedStatus==1)
				$error_msg	= "Final Approval raised. Can't save";
			else if($savedStatus > 1 && $savedStatus <= $savedApproveLevels)
				$error_msg	= "This is approved. Can't save";
			else if($savedStatus==-2)
				$error_msg	= "This is cancelled. Can't save";
			else if($savedStatus==-10)
				$error_msg	= "This is completed. Can't save";
			else if(!$this->$fieldName())
				$error_msg	= "No permision to save";
			else
				$permission	=1;
		}
		else if($type == 'Cancel')
		{			
			$fieldName	= "getint$type";
			
			if($savedStatus==-2)
				$error_msg	= "Already Cancelled";
			else if($savedStatus!=1)
				$error_msg	= "Final Approval not raised. Can't cancel";
			else if(!$this->$fieldName())
				$error_msg	= "No permision to cancel";
			else
				$permission	=1;
		}
		else if($type == 'Revise')
		{			
			$fieldName	= "getint$type";
			if($savedStatus!=1)
				$error_msg	= "Final Approval not raised. Can't revise";
			else if(!$this->$fieldName())
				$error_msg	= "No permision to revise";
			else
				$permission	=1;
		}
		else if($type == 'Complete')
		{			
			$fieldName	= "getint$type";
			if($savedStatus!=1)
				$error_msg	= "Final Approval not raised. Can't complete";
			else if(!$this->$fieldName())
				$error_msg	= "No permision to Complete";
			else
				$permission	=1;
		}
		else if($type == 'Approve')
		{			
			$fieldName	= "getint{$nextLevel}Approval";
			if($savedStatus==1)
				$error_msg	= "Final Approval already raised";
			else if($savedStatus==0)
				$error_msg	= "Please save this before approval";
			else if($savedStatus==-2)
				$error_msg	= "This is cancelled.Cannot approve.";
			else if($savedStatus==-10)
				$error_msg	= "This is completed.Cannot approve.";
			else if($savedStatus==-1)
				$error_msg	= "This is revised.Cannot approve.";
			else if(!$this->$fieldName())
				$error_msg	= "No permision for approval level $nextLevel";
			else
				$permission	=1;
		}
		else if($type == 'Reject')
		{			
			$fieldName	= "getint{$nextLevel}Approval";
			if($savedStatus==1)
				$error_msg	= "This is finaly approved";
			else if($savedStatus==0)
				$error_msg	= "This is already rejected";
			else if($savedStatus==-2)
				$error_msg	= "This is cancelled.Cannot reject.";
			else if($savedStatus==-10)
				$error_msg	= "This is completed.Cannot reject.";
			else if($savedStatus==-1)
				$error_msg	= "This is revised.Cannot reject.";
			else if(!$this->$fieldName())
				$error_msg	= "No permision to reject";
			else
				$permission	= 1;
		}
		else if($type =='Delete')
		{		
			$fieldName	= "getint$type";
			if($savedStatus==1)
				$error_msg	= "This is finaly approved";
			else if($savedStatus==0)
				$error_msg	= "This is already delete";
			else if($savedStatus==-2)
				$error_msg	= "This is cancelled.Cannot delete.";
			else if($savedStatus==-10)
				$error_msg	= "This is completed.Cannot delete.";
			else if($savedStatus==-1)
				$error_msg	= "This is revised.Cannot delete.";
			else if(!$this->$fieldName())
				$error_msg	= "No permision to delete";
			else
				$permission	=1;	
		}
		else if($type == 'Add')
		{			
			$fieldName	= "getint$type";
			if($savedStatus==1)
				$error_msg	= "This is finaly approved. Can't add";
			else if($savedStatus > 1 && $savedStatus <= $savedApproveLevels)
				$error_msg	= "This is approved. Can't add";
			else if($savedStatus==-2)
				$error_msg	= "This is cancelled. Can't add";
			else if($savedStatus==-10)
				$error_msg	= "This is completed. Can't add";
			else if(!$this->$fieldName())
				$error_msg	= "No permision to add";
			else
				$permission	= 1;
		}
		
		
		if($permission==0){
			$response['type']	= false;
			$response['msg'] 	= $error_msg;
		}
		else if($permission==1) {
			$response['type']	= true;
			$response['msg']	= $success_msg;			
		}
		return $response;		
 	}
	
	public function checkPermission($intMenuId,$intUserId)
	{
		$cols	= "*";
		
		$join	= "";
		
		$where	= " menupermision.intMenuId =  '$intMenuId' AND
					menupermision.intUserId =  '$intUserId'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0){
			$this->setVariables($result);
			return true;
		}else
			return false;
	}
	
	public function copyUserPermission($copyUserId,$userId)
	{
		$sql = "REPLACE INTO menupermision 
				(
				intMenuId, intUserId, intView, intAdd, 
				intEdit, intDelete, int1Approval, int2Approval, 
				int3Approval, int4Approval, int5Approval, intSendToApproval, 
				intCancel, intReject, intRevise, intExportToExcel, 
				intExportToPDF
				)
				(
				SELECT 	intMenuId, $userId AS userId, intView, intAdd, intEdit, 
				intDelete, int1Approval, int2Approval, int3Approval, 
				int4Approval, int5Approval, intSendToApproval, intCancel, 
				intReject, intRevise, intExportToExcel, intExportToPDF 
				FROM 
				menupermision 
				WHERE intUserId = '$copyUserId'
				) ";
		
		$result	= $this->db->RunQuery($sql);
		return $result;
	}
	
	public function getNextApproveUsers($programCode,$status,$approveLevel,$companyId,$locationId,$loginUserId)
	{
		$field = "int".(($approveLevel+2)-$status)."Approval";
		
		$cols	= "	DISTINCT
					  menupermision.intUserId AS USER_ID,
					  SU.strUserName          AS USER_NAME,
					  SU.strFullName          AS USER_FULL_NAME,
					  SU.strEmail             AS USER_EMAIL";
					  
		$join 	= "INNER JOIN menus
						ON menupermision.intMenuId = menus.intId
					  INNER JOIN sys_users SU
						ON SU.intUserId = menupermision.intUserId
					  INNER JOIN mst_locations_user
						ON mst_locations_user.intUserId = SU.intUserId
					  INNER JOIN mst_locations
						ON mst_locations.intId = mst_locations_user.intLocationId";
	
		$where	= "mst_locations.intCompanyId = '$companyId'
						AND menus.strCode = '$programCode'
						AND menupermision.$field = '1'
						AND menupermision.intUserId <> '$loginUserId'
						AND SU.intStatus = 1";
		return $this->select($cols,$join,$where,$order = null, $limit = null);
	}	
	//END }
}
?>
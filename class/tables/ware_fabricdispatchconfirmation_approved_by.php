<?php
class ware_fabricdispatchconfirmation_approved_by{
 
	private $db;
	private $table= "ware_fabricdispatchconfirmation_approved_by";
	
	//private property
	private $DISP_CONFIRM_NO;
	private $DISP_CONFIRM_YEAR;
	private $APPROVE_LEVEL_NO;
	private $APPROVED_BY;
	private $APPROVED_DATE;
	private $STATUS;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('DISP_CONFIRM_NO'=>'DISP_CONFIRM_NO',
										'DISP_CONFIRM_YEAR'=>'DISP_CONFIRM_YEAR',
										'APPROVE_LEVEL_NO'=>'APPROVE_LEVEL_NO',
										'APPROVED_BY'=>'APPROVED_BY',
										'APPROVED_DATE'=>'APPROVED_DATE',
										'STATUS'=>'STATUS',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "DISP_CONFIRM_NO = ".$this->DISP_CONFIRM_NO." and DISP_CONFIRM_YEAR = ".$this->DISP_CONFIRM_YEAR." and APPROVE_LEVEL_NO = ".$this->APPROVE_LEVEL_NO." and STATUS = ".$this->STATUS."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun DISP_CONFIRM_NO
	function getDISP_CONFIRM_NO()
	{
		$this->validate();
		return $this->DISP_CONFIRM_NO;
	}
	
	//retun DISP_CONFIRM_YEAR
	function getDISP_CONFIRM_YEAR()
	{
		$this->validate();
		return $this->DISP_CONFIRM_YEAR;
	}
	
	//retun APPROVE_LEVEL_NO
	function getAPPROVE_LEVEL_NO()
	{
		$this->validate();
		return $this->APPROVE_LEVEL_NO;
	}
	
	//retun APPROVED_BY
	function getAPPROVED_BY()
	{
		$this->validate();
		return $this->APPROVED_BY;
	}
	
	//retun APPROVED_DATE
	function getAPPROVED_DATE()
	{
		$this->validate();
		return $this->APPROVED_DATE;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set DISP_CONFIRM_NO
	function setDISP_CONFIRM_NO($DISP_CONFIRM_NO)
	{
		array_push($this->commitArray,'DISP_CONFIRM_NO');
		$this->DISP_CONFIRM_NO = $DISP_CONFIRM_NO;
	}
	
	//set DISP_CONFIRM_YEAR
	function setDISP_CONFIRM_YEAR($DISP_CONFIRM_YEAR)
	{
		array_push($this->commitArray,'DISP_CONFIRM_YEAR');
		$this->DISP_CONFIRM_YEAR = $DISP_CONFIRM_YEAR;
	}
	
	//set APPROVE_LEVEL_NO
	function setAPPROVE_LEVEL_NO($APPROVE_LEVEL_NO)
	{
		array_push($this->commitArray,'APPROVE_LEVEL_NO');
		$this->APPROVE_LEVEL_NO = $APPROVE_LEVEL_NO;
	}
	
	//set APPROVED_BY
	function setAPPROVED_BY($APPROVED_BY)
	{
		array_push($this->commitArray,'APPROVED_BY');
		$this->APPROVED_BY = $APPROVED_BY;
	}
	
	//set APPROVED_DATE
	function setAPPROVED_DATE($APPROVED_DATE)
	{
		array_push($this->commitArray,'APPROVED_DATE');
		$this->APPROVED_DATE = $APPROVED_DATE;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->DISP_CONFIRM_NO=='' || $this->DISP_CONFIRM_YEAR=='' || $this->APPROVE_LEVEL_NO=='' || $this->STATUS=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($DISP_CONFIRM_NO , $DISP_CONFIRM_YEAR , $APPROVE_LEVEL_NO , $STATUS)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "DISP_CONFIRM_NO='$DISP_CONFIRM_NO' and DISP_CONFIRM_YEAR='$DISP_CONFIRM_YEAR' and APPROVE_LEVEL_NO='$APPROVE_LEVEL_NO' and STATUS='$STATUS'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($DISP_CONFIRM_NO,$DISP_CONFIRM_YEAR,$APPROVE_LEVEL_NO,$APPROVED_BY,$APPROVED_DATE,$STATUS){
		$data = array('DISP_CONFIRM_NO'=>$DISP_CONFIRM_NO 
				,'DISP_CONFIRM_YEAR'=>$DISP_CONFIRM_YEAR 
				,'APPROVE_LEVEL_NO'=>$APPROVE_LEVEL_NO 
				,'APPROVED_BY'=>$APPROVED_BY 
				,'APPROVED_DATE'=>$APPROVED_DATE 
				,'STATUS'=>$STATUS 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('DISP_CONFIRM_NO,DISP_CONFIRM_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['DISP_CONFIRM_NO'])
				$html .= '<option selected="selected" value="'.$row['DISP_CONFIRM_NO'].'">'.$row['DISP_CONFIRM_YEAR'].'</option>';	
			else
				$html .= '<option value="'.$row['DISP_CONFIRM_NO'].'">'.$row['DISP_CONFIRM_YEAR'].'</option>';	
		}
		return $html;
	}
	
	public function updateMaxStatus($serialNo,$serialYear)
	{ 
		$select		= " MAX(STATUS) AS MAX_STATUS ";
		$where		= " DISP_CONFIRM_NO = '".$serialNo."' AND
						DISP_CONFIRM_YEAR = '".$serialYear."' ";
		
		$result 	= $this->select($select,NULL,$where, NULL,NULL);	
		$row		= mysqli_fetch_array($result);
		
		$maxStatus	= $row['MAX_STATUS']+1;
		
		$data  		= array('STATUS' => $maxStatus);
		
		$where		= ' DISP_CONFIRM_NO = "'.$serialNo.'" AND 
						DISP_CONFIRM_YEAR = "'.$serialYear.'" AND
						STATUS = 0 ' ;
		$result_arr	= $this->update($data,$where);
		if(!$result_arr['status'])
			throw new Exception($result_arr['msg']);		

	}
	
	//END }
}
?>

<?php

class trn_order_sub_contract_header{
 
	private $db;
	private $table= "trn_order_sub_contract_header";
	
	function __construct($db)
	{
		$this->db = $db;

		$this->db->field_array_set(array('SUB_CONTRACT_NO'=>'SUB_CONTRACT_NO',
										'SUB_CONTRACT_YEAR'=>'SUB_CONTRACT_YEAR',
										'ORDER_NO'=>'ORDER_NO',
										'ORDER_YEAR'=>'ORDER_YEAR',
										'COMPANY_TO_TYPE'=>'COMPANY_TO_TYPE',
										'COMPANY_TO_ID'=>'COMPANY_TO_ID',
										'LOCATION_TO_ID'=>'LOCATION_TO_ID',
										'SUB_CONTRACTOR_ID'=>'SUB_CONTRACTOR_ID',
										'REMARKS'=>'REMARKS',
										'STATUS'=>'STATUS',
										'LEVELS'=>'LEVELS',
										'DELIVERY_DATE'=>'DELIVERY_DATE',
										'DATE'=>'DATE',
										'CREATED_DATE'=>'CREATED_DATE',
										'CREATED_BY'=>'CREATED_BY',
										'MODIFIED_DATE'=>'MODIFIED_DATE',
										'MODIFIED_BY'=>'MODIFIED_BY',
										'LOCATION_ID'=>'LOCATION_ID',

		
										));
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		return $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  
		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//upgrade table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();	
	}
	
	public function getStatus($subContractorNo,$subContractorYear)
	{
		$this->db->connect();
		$cols	= "STATUS	AS STATUS";		
		$where	= 'SUB_CONTRACT_NO = "'.$subContractorNo.'" AND SUB_CONTRACT_YEAR = "'.$subContractorYear.'" ' ;
		$result = $this->select($cols,$join = null,$where, $order = null, $limit = null);	
		$row	= mysqli_fetch_array($result);
		$this->db->disconnect();
		return $row['STATUS'];
	}
	public function getApproveLevels($subContractorNo,$subContractorYear)
	{
		$this->db->connect();
		$cols	= "LEVELS	AS LEVELS";		
		$where	= 'SUB_CONTRACT_NO = "'.$subContractorNo.'" AND SUB_CONTRACT_YEAR = "'.$subContractorYear.'" ' ;
		$result = $this->select($cols,$join = null,$where, $order = null, $limit = null);	
		$row	= mysqli_fetch_array($result);
		$this->db->disconnect();
		return $row['LEVELS'];
	}
}
?>

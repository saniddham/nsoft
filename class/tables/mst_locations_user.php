<?php
class mst_locations_user{

	private $db;
	private $table= "mst_locations_user";

	//private property
	private $intUserId;
	private $intLocationId;
	private $commitArray = array();
	private $field_array = array();
	

	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('intUserId'=>'intUserId',
										'intLocationId'=>'intLocationId',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit );
		return $result;
	}

	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data);
		return $this->db->getResult();


	}

	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}

	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();
	}

	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}

	public function get_field_array(){
		return $this->field_array;
	}

	function commit($type='update')
	{
		//commit update
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{
			$data[$v] = $this->$v;
		}
		$where		= "intUserId = ".$this->intUserId." and intLocationId = ".$this->intLocationId."" ;
		$this->commitArray = array();

		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}

	//BEGIN - public functions for private properties {
	
	//retun intUserId
	function getintUserId()
	{
		$this->validate();
		return $this->intUserId;
	}
	
	//retun intLocationId
	function getintLocationId()
	{
		$this->validate();
		return $this->intLocationId;
	}
	
	//END }

	//BEGIN - public function set {
	
	//set intUserId
	function setintUserId($intUserId)
	{
		array_push($this->commitArray,'intUserId');
		$this->intUserId = $intUserId;
	}
	
	//set intLocationId
	function setintLocationId($intLocationId)
	{
		array_push($this->commitArray,'intLocationId');
		$this->intLocationId = $intLocationId;
	}
	
	//END }

	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->intUserId=='' || $this->intLocationId=='')
			throw new exception("Set primary values first");
		else
			return true;
	}

	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}

	public function set($intUserId , $intLocationId)
	{
		$cols	= "*";

		$join	= NULL;

		$where	= "intUserId='$intUserId' and intLocationId='$intLocationId'";

		$result = $this->select($cols,$join,$where);
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}

	//insert as parameters
	public function insertRec($intUserId,$intLocationId){
		$data = array('intUserId'=>$intUserId 
				,'intLocationId'=>$intLocationId 
				);
		return $this->insert($data);
	}

	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('intUserId,intLocationId',  null, $where = $where);
		$html = '<option value=""></option>';
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['intUserId'])
				$html .= '<option selected="selected" value="'.$row['intUserId'].'">'.$row['intLocationId'].'</option>';
			else
				$html .= '<option value="'.$row['intUserId'].'">'.$row['intLocationId'].'</option>';
		}
		return $html;
	}
	public function getLocationDepartmentwiseUsers($location,$department)
	{
		$cols	= " sys_users.intUserId,
					sys_users.strUserName ";
		
		$join	= " INNER JOIN sys_users ON sys_users.intUserId = mst_locations_user.intUserId ";
		
		$where	= " mst_locations_user.intLocationId =  '$location' 
					AND intHigherPermision <> 1 
					AND intDepartmentId='$department' ";
		
		$order	= " strUserName ";
		
		return $this->select($cols,$join,$where,$order);	
	}
	//END }
}
?>
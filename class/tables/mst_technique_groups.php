<?php
class mst_technique_groups{
 
	private $db;
	private $table= "mst_technique_groups";
	
	//private property
	private $TECHNIQUE_GROUP_ID;
	private $TECHNIQUE_GROUP_NAME;
	private $CREATED_BY;
	private $CREATED_DATE;
	private $STATUS;
	private $PRIORITY_LEVEL;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('TECHNIQUE_GROUP_ID'=>'TECHNIQUE_GROUP_ID',
										'TECHNIQUE_GROUP_NAME'=>'TECHNIQUE_GROUP_NAME',
										'CREATED_BY'=>'CREATED_BY',
										'CREATED_DATE'=>'CREATED_DATE',
										'STATUS'=>'STATUS',
										'PRIORITY_LEVEL'=>'PRIORITY_LEVEL',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "TECHNIQUE_GROUP_ID = ".$this->TECHNIQUE_GROUP_ID."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun TECHNIQUE_GROUP_ID
	function getTECHNIQUE_GROUP_ID()
	{
		$this->validate();
		return $this->TECHNIQUE_GROUP_ID;
	}
	
	//retun TECHNIQUE_GROUP_NAME
	function getTECHNIQUE_GROUP_NAME()
	{
		$this->validate();
		return $this->TECHNIQUE_GROUP_NAME;
	}
	
	//retun CREATED_BY
	function getCREATED_BY()
	{
		$this->validate();
		return $this->CREATED_BY;
	}
	
	//retun CREATED_DATE
	function getCREATED_DATE()
	{
		$this->validate();
		return $this->CREATED_DATE;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//retun PRIORITY_LEVEL
	function getPRIORITY_LEVEL()
	{
		$this->validate();
		return $this->PRIORITY_LEVEL;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set TECHNIQUE_GROUP_ID
	function setTECHNIQUE_GROUP_ID($TECHNIQUE_GROUP_ID)
	{
		array_push($this->commitArray,'TECHNIQUE_GROUP_ID');
		$this->TECHNIQUE_GROUP_ID = $TECHNIQUE_GROUP_ID;
	}
	
	//set TECHNIQUE_GROUP_NAME
	function setTECHNIQUE_GROUP_NAME($TECHNIQUE_GROUP_NAME)
	{
		array_push($this->commitArray,'TECHNIQUE_GROUP_NAME');
		$this->TECHNIQUE_GROUP_NAME = $TECHNIQUE_GROUP_NAME;
	}
	
	//set CREATED_BY
	function setCREATED_BY($CREATED_BY)
	{
		array_push($this->commitArray,'CREATED_BY');
		$this->CREATED_BY = $CREATED_BY;
	}
	
	//set CREATED_DATE
	function setCREATED_DATE($CREATED_DATE)
	{
		array_push($this->commitArray,'CREATED_DATE');
		$this->CREATED_DATE = $CREATED_DATE;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//set PRIORITY_LEVEL
	function setPRIORITY_LEVEL($PRIORITY_LEVEL)
	{
		array_push($this->commitArray,'PRIORITY_LEVEL');
		$this->PRIORITY_LEVEL = $PRIORITY_LEVEL;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->TECHNIQUE_GROUP_ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($TECHNIQUE_GROUP_ID)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "TECHNIQUE_GROUP_ID='$TECHNIQUE_GROUP_ID'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($TECHNIQUE_GROUP_ID,$TECHNIQUE_GROUP_NAME,$CREATED_BY,$CREATED_DATE,$STATUS,$PRIORITY_LEVEL){
		$data = array('TECHNIQUE_GROUP_ID'=>$TECHNIQUE_GROUP_ID 
				,'TECHNIQUE_GROUP_NAME'=>$TECHNIQUE_GROUP_NAME 
				,'CREATED_BY'=>$CREATED_BY 
				,'CREATED_DATE'=>$CREATED_DATE 
				,'STATUS'=>$STATUS 
				,'PRIORITY_LEVEL'=>$PRIORITY_LEVEL 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('TECHNIQUE_GROUP_ID,TECHNIQUE_GROUP_NAME',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['TECHNIQUE_GROUP_ID'])
				$html .= '<option selected="selected" value="'.$row['TECHNIQUE_GROUP_ID'].'">'.$row['TECHNIQUE_GROUP_NAME'].'</option>';	
			else
				$html .= '<option value="'.$row['TECHNIQUE_GROUP_ID'].'">'.$row['TECHNIQUE_GROUP_NAME'].'</option>';	
		}
		return $html;
	}
	
	//END }
}
?>
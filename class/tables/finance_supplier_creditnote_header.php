<?php
class finance_supplier_creditnote_header{
 
	private $db;
	private $table= "finance_supplier_creditnote_header";
	
	//private property
	private $CREDIT_NO;
	private $CREDIT_YEAR;
	private $PURCHASE_INVOICE_NO;
	private $PURCHASE_INVOICE_YEAR;
	private $SUPPLIER_ID;
	private $CURRENCY_ID;
	private $CREDIT_DATE;
	private $REMARKS;
	private $STATUS;
	private $APPROVE_LEVELS;
	private $PRINT_STATUS;
	private $COMPANY_ID;
	private $LOCATION_ID;
	private $CREATED_BY;
	private $CREATED_DATE;
	private $LAST_MODIFY_BY;
	private $LAST_MODIFY_DATE;
	private $commitArray = array();
	private $field_array = array();
	
	
	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('CREDIT_NO'=>'CREDIT_NO',
										'CREDIT_YEAR'=>'CREDIT_YEAR',
										'PURCHASE_INVOICE_NO'=>'PURCHASE_INVOICE_NO',
										'PURCHASE_INVOICE_YEAR'=>'PURCHASE_INVOICE_YEAR',
										'SUPPLIER_ID'=>'SUPPLIER_ID',
										'CURRENCY_ID'=>'CURRENCY_ID',
										'CREDIT_DATE'=>'CREDIT_DATE',
										'REMARKS'=>'REMARKS',
										'STATUS'=>'STATUS',
										'APPROVE_LEVELS'=>'APPROVE_LEVELS',
										'PRINT_STATUS'=>'PRINT_STATUS',
										'COMPANY_ID'=>'COMPANY_ID',
										'LOCATION_ID'=>'LOCATION_ID',
										'CREATED_BY'=>'CREATED_BY',
										'CREATED_DATE'=>'CREATED_DATE',
										'LAST_MODIFY_BY'=>'LAST_MODIFY_BY',
										'LAST_MODIFY_DATE'=>'LAST_MODIFY_DATE',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $result;
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  

		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	public function get_field_array(){
		return $this->field_array;	
	}
	
	function commit($type='update')
	{
		//commit update		
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{			
			$data[$v] = $this->$v;
		}
		$where		= "CREDIT_NO = ".$this->CREDIT_NO." and CREDIT_YEAR = ".$this->CREDIT_YEAR."" ;
		unset($this->commitArray);
		
		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}
	
	//BEGIN - public functions for private properties {
	
	//retun CREDIT_NO
	function getCREDIT_NO()
	{
		$this->validate();
		return $this->CREDIT_NO;
	}
	
	//retun CREDIT_YEAR
	function getCREDIT_YEAR()
	{
		$this->validate();
		return $this->CREDIT_YEAR;
	}
	
	//retun PURCHASE_INVOICE_NO
	function getPURCHASE_INVOICE_NO()
	{
		$this->validate();
		return $this->PURCHASE_INVOICE_NO;
	}
	
	//retun PURCHASE_INVOICE_YEAR
	function getPURCHASE_INVOICE_YEAR()
	{
		$this->validate();
		return $this->PURCHASE_INVOICE_YEAR;
	}
	
	//retun SUPPLIER_ID
	function getSUPPLIER_ID()
	{
		$this->validate();
		return $this->SUPPLIER_ID;
	}
	
	//retun CURRENCY_ID
	function getCURRENCY_ID()
	{
		$this->validate();
		return $this->CURRENCY_ID;
	}
	
	//retun CREDIT_DATE
	function getCREDIT_DATE()
	{
		$this->validate();
		return $this->CREDIT_DATE;
	}
	
	//retun REMARKS
	function getREMARKS()
	{
		$this->validate();
		return $this->REMARKS;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//retun APPROVE_LEVELS
	function getAPPROVE_LEVELS()
	{
		$this->validate();
		return $this->APPROVE_LEVELS;
	}
	
	//retun PRINT_STATUS
	function getPRINT_STATUS()
	{
		$this->validate();
		return $this->PRINT_STATUS;
	}
	
	//retun COMPANY_ID
	function getCOMPANY_ID()
	{
		$this->validate();
		return $this->COMPANY_ID;
	}
	
	//retun LOCATION_ID
	function getLOCATION_ID()
	{
		$this->validate();
		return $this->LOCATION_ID;
	}
	
	//retun CREATED_BY
	function getCREATED_BY()
	{
		$this->validate();
		return $this->CREATED_BY;
	}
	
	//retun CREATED_DATE
	function getCREATED_DATE()
	{
		$this->validate();
		return $this->CREATED_DATE;
	}
	
	//retun LAST_MODIFY_BY
	function getLAST_MODIFY_BY()
	{
		$this->validate();
		return $this->LAST_MODIFY_BY;
	}
	
	//retun LAST_MODIFY_DATE
	function getLAST_MODIFY_DATE()
	{
		$this->validate();
		return $this->LAST_MODIFY_DATE;
	}
	
	//END }
	
	//BEGIN - public function set {
	
	//set CREDIT_NO
	function setCREDIT_NO($CREDIT_NO)
	{
		array_push($this->commitArray,'CREDIT_NO');
		$this->CREDIT_NO = $CREDIT_NO;
	}
	
	//set CREDIT_YEAR
	function setCREDIT_YEAR($CREDIT_YEAR)
	{
		array_push($this->commitArray,'CREDIT_YEAR');
		$this->CREDIT_YEAR = $CREDIT_YEAR;
	}
	
	//set PURCHASE_INVOICE_NO
	function setPURCHASE_INVOICE_NO($PURCHASE_INVOICE_NO)
	{
		array_push($this->commitArray,'PURCHASE_INVOICE_NO');
		$this->PURCHASE_INVOICE_NO = $PURCHASE_INVOICE_NO;
	}
	
	//set PURCHASE_INVOICE_YEAR
	function setPURCHASE_INVOICE_YEAR($PURCHASE_INVOICE_YEAR)
	{
		array_push($this->commitArray,'PURCHASE_INVOICE_YEAR');
		$this->PURCHASE_INVOICE_YEAR = $PURCHASE_INVOICE_YEAR;
	}
	
	//set SUPPLIER_ID
	function setSUPPLIER_ID($SUPPLIER_ID)
	{
		array_push($this->commitArray,'SUPPLIER_ID');
		$this->SUPPLIER_ID = $SUPPLIER_ID;
	}
	
	//set CURRENCY_ID
	function setCURRENCY_ID($CURRENCY_ID)
	{
		array_push($this->commitArray,'CURRENCY_ID');
		$this->CURRENCY_ID = $CURRENCY_ID;
	}
	
	//set CREDIT_DATE
	function setCREDIT_DATE($CREDIT_DATE)
	{
		array_push($this->commitArray,'CREDIT_DATE');
		$this->CREDIT_DATE = $CREDIT_DATE;
	}
	
	//set REMARKS
	function setREMARKS($REMARKS)
	{
		array_push($this->commitArray,'REMARKS');
		$this->REMARKS = $REMARKS;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//set APPROVE_LEVELS
	function setAPPROVE_LEVELS($APPROVE_LEVELS)
	{
		array_push($this->commitArray,'APPROVE_LEVELS');
		$this->APPROVE_LEVELS = $APPROVE_LEVELS;
	}
	
	//set PRINT_STATUS
	function setPRINT_STATUS($PRINT_STATUS)
	{
		array_push($this->commitArray,'PRINT_STATUS');
		$this->PRINT_STATUS = $PRINT_STATUS;
	}
	
	//set COMPANY_ID
	function setCOMPANY_ID($COMPANY_ID)
	{
		array_push($this->commitArray,'COMPANY_ID');
		$this->COMPANY_ID = $COMPANY_ID;
	}
	
	//set LOCATION_ID
	function setLOCATION_ID($LOCATION_ID)
	{
		array_push($this->commitArray,'LOCATION_ID');
		$this->LOCATION_ID = $LOCATION_ID;
	}
	
	//set CREATED_BY
	function setCREATED_BY($CREATED_BY)
	{
		array_push($this->commitArray,'CREATED_BY');
		$this->CREATED_BY = $CREATED_BY;
	}
	
	//set CREATED_DATE
	function setCREATED_DATE($CREATED_DATE)
	{
		array_push($this->commitArray,'CREATED_DATE');
		$this->CREATED_DATE = $CREATED_DATE;
	}
	
	//set LAST_MODIFY_BY
	function setLAST_MODIFY_BY($LAST_MODIFY_BY)
	{
		array_push($this->commitArray,'LAST_MODIFY_BY');
		$this->LAST_MODIFY_BY = $LAST_MODIFY_BY;
	}
	
	//set LAST_MODIFY_DATE
	function setLAST_MODIFY_DATE($LAST_MODIFY_DATE)
	{
		array_push($this->commitArray,'LAST_MODIFY_DATE');
		$this->LAST_MODIFY_DATE = $LAST_MODIFY_DATE;
	}
	
	//END }
	
	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->CREDIT_NO=='' || $this->CREDIT_YEAR=='')
			throw new exception("Set primary values first");
		else
			return true;
	}
	
	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}
	
	public function set($CREDIT_NO , $CREDIT_YEAR)
	{
		$cols	= "*";
		
		$join	= NULL;
		
		$where	= "CREDIT_NO='$CREDIT_NO' and CREDIT_YEAR='$CREDIT_YEAR'";
		
		$result = $this->select($cols,$join,$where);	
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}	
	
	//insert as parameters
	public function insertRec($CREDIT_NO,$CREDIT_YEAR,$PURCHASE_INVOICE_NO,$PURCHASE_INVOICE_YEAR,$SUPPLIER_ID,$CURRENCY_ID,$CREDIT_DATE,$REMARKS,$STATUS,$APPROVE_LEVELS,$COMPANY_ID,$LOCATION_ID,$CREATED_BY,$CREATED_DATE,$LAST_MODIFY_BY,$LAST_MODIFY_DATE){
		$data = array('CREDIT_NO'=>$CREDIT_NO 
				,'CREDIT_YEAR'=>$CREDIT_YEAR 
				,'PURCHASE_INVOICE_NO'=>$PURCHASE_INVOICE_NO 
				,'PURCHASE_INVOICE_YEAR'=>$PURCHASE_INVOICE_YEAR 
				,'SUPPLIER_ID'=>$SUPPLIER_ID 
				,'CURRENCY_ID'=>$CURRENCY_ID 
				,'CREDIT_DATE'=>$CREDIT_DATE 
				,'REMARKS'=>$REMARKS 
				,'STATUS'=>$STATUS 
				,'APPROVE_LEVELS'=>$APPROVE_LEVELS 
				,'PRINT_STATUS'=>0 
				,'COMPANY_ID'=>$COMPANY_ID 
				,'LOCATION_ID'=>$LOCATION_ID 
				,'CREATED_BY'=>$CREATED_BY 
				,'CREATED_DATE'=>$CREATED_DATE 
				,'LAST_MODIFY_BY'=>$LAST_MODIFY_BY 
				,'LAST_MODIFY_DATE'=>$LAST_MODIFY_DATE 
				);
		return $this->insert($data);
	}
	
	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('CREDIT_NO,CREDIT_YEAR',  null, $where = $where);
		$html = '<option value=""></option>';	
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['CREDIT_NO'])
				$html .= '<option selected="selected" value="'.$row['CREDIT_NO'].'">'.$row['CREDIT_YEAR'].'</option>';	
			else
				$html .= '<option value="'.$row['CREDIT_NO'].'">'.$row['CREDIT_YEAR'].'</option>';	
		}
		return $html;
	}
	
	//END }
}
?>
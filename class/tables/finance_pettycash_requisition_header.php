
<?php

class finance_pettycash_requisition_header{
 
	private $db;
	private $table= "finance_pettycash_requisition_header";
	
	function __construct($db)
	{
		$this->db = $db;
		$this->db->field_array_set(array('REQUISITION_NO'=>'REQUISITION_NO',
										'REQUISITION_YEAR'=>'REQUISITION_YEAR',
										'TOTAL_AMOUNT'=>'TOTAL_AMOUNT',
										'REMARKS'=>'REMARKS',
										'REQUEST_DATE'=>'REQUEST_DATE',
										'STATUS'=>'STATUS',
										'APPROVE_LEVELS'=>'APPROVE_LEVELS',
										'COMPANY_ID'=>'COMPANY_ID',
										'LOCATION_ID'=>'LOCATION_ID',
										'CREATED_BY'=>'CREATED_BY',
										'CREATED_DATE'=>'CREATED_DATE',
										'MODIFIED_BY'=>'MODIFIED_BY',
										'MODIFIED_DATE'=>'MODIFIED_DATE',
										));
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$this->db->select($this->table, $cols , $join , $where , $order , $limit ); 
		return $this->db->getResult();
	}
	
	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data); 
		return $this->db->getResult();  
		
	}
	
	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}
	
	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();	
	}
	
}
?>

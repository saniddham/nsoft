<?php
class finance_mst_pettycash_category{

	private $db;
	private $table= "finance_mst_pettycash_category";

	//private property
	private $ID;
	private $GL_ID;
	private $CODE;
	private $NAME;
	private $STATUS;
	private $IOU_STATUS;
	private $BLOCK_STATUS;
	private $commitArray = array();
	private $field_array = array();
	

	function __construct($db)
	{
		$this->db = $db;
		$this->field_array=array('ID'=>'ID',
										'GL_ID'=>'GL_ID',
										'CODE'=>'CODE',
										'NAME'=>'NAME',
										'STATUS'=>'STATUS',
										'IOU_STATUS'=>'IOU_STATUS',
										'BLOCK_STATUS'=>'BLOCK_STATUS',
										);
		$this->db->field_array_set($this->field_array);
	}

	function select($cols = '*', $join = null, $where = null, $order = null, $limit = null)
	{
		//select * data
		$result = $this->db->select($this->table, $cols , $join , $where , $order , $limit );
		return $result;
	}

	function insert($data)
	{
		//insert into the table
		$this->db->insert($this->table,$data);
		return $this->db->getResult();


	}

	function update($data,$where)
	{
		//update table
		$this->db->update($this->table,$data,$where);
		return $this->db->getResult();
	}

	function delete($where)
	{
		//update table
		$this->db->delete($this->table,$where);
		return $this->db->getResult();
	}

	function upgrade($data,$where)
	{
		//update table
		$this->db->upgrade($this->table,$data,$where);
		return $this->db->getResult();
	}

	public function get_field_array(){
		return $this->field_array;
	}

	function commit($type='update')
	{
		//commit update
		$data	= array();
		foreach($this->commitArray as $k=>$v)
		{
			$data[$v] = $this->$v;
		}
		$where		= "ID = ".$this->ID."" ;
		$this->commitArray = array();

		if($type=='insert')
			return $this->insert($data);
		else
			return $this->update($data,$where);
	}

	//BEGIN - public functions for private properties {
	
	//retun ID
	function getID()
	{
		$this->validate();
		return $this->ID;
	}
	
	//retun GL_ID
	function getGL_ID()
	{
		$this->validate();
		return $this->GL_ID;
	}
	
	//retun CODE
	function getCODE()
	{
		$this->validate();
		return $this->CODE;
	}
	
	//retun NAME
	function getNAME()
	{
		$this->validate();
		return $this->NAME;
	}
	
	//retun STATUS
	function getSTATUS()
	{
		$this->validate();
		return $this->STATUS;
	}
	
	//retun IOU_STATUS
	function getIOU_STATUS()
	{
		$this->validate();
		return $this->IOU_STATUS;
	}
	
	//retun BLOCK_STATUS
	function getBLOCK_STATUS()
	{
		$this->validate();
		return $this->BLOCK_STATUS;
	}
	
	//END }

	//BEGIN - public function set {
	
	//set ID
	function setID($ID)
	{
		array_push($this->commitArray,'ID');
		$this->ID = $ID;
	}
	
	//set GL_ID
	function setGL_ID($GL_ID)
	{
		array_push($this->commitArray,'GL_ID');
		$this->GL_ID = $GL_ID;
	}
	
	//set CODE
	function setCODE($CODE)
	{
		array_push($this->commitArray,'CODE');
		$this->CODE = $CODE;
	}
	
	//set NAME
	function setNAME($NAME)
	{
		array_push($this->commitArray,'NAME');
		$this->NAME = $NAME;
	}
	
	//set STATUS
	function setSTATUS($STATUS)
	{
		array_push($this->commitArray,'STATUS');
		$this->STATUS = $STATUS;
	}
	
	//set IOU_STATUS
	function setIOU_STATUS($IOU_STATUS)
	{
		array_push($this->commitArray,'IOU_STATUS');
		$this->IOU_STATUS = $IOU_STATUS;
	}
	
	//set BLOCK_STATUS
	function setBLOCK_STATUS($BLOCK_STATUS)
	{
		array_push($this->commitArray,'BLOCK_STATUS');
		$this->BLOCK_STATUS = $BLOCK_STATUS;
	}
	
	//END }

	//BEGIN - validate primary values and set {
	
	//validate primary values
	private function validate()
	{
		if($this->ID=='')
			throw new exception("Set primary values first");
		else
			return true;
	}

	//set variables
	private function setVariables($result)
	{
		$row = mysqli_fetch_assoc($result);
		$data = array();
		foreach ( $row as $k=>$v )
		{
			$this->$k = $v;
			$data[$k] = $v;
		}
		return $data;
	}

	public function set($ID)
	{
		$cols	= "*";

		$join	= NULL;

		$where	= "ID='$ID'";

		$result = $this->select($cols,$join,$where);
		if($this->db->numRows()>0)
			return $this->setVariables($result);
	}

	//insert as parameters
	public function insertRec($GL_ID,$CODE,$NAME,$STATUS,$IOU_STATUS){
		$data = array(
				'GL_ID'=>$GL_ID 
				,'CODE'=>$CODE 
				,'NAME'=>$NAME 
				,'STATUS'=>$STATUS 
				,'IOU_STATUS'=>$IOU_STATUS 
				);
		return $this->insert($data);
	}

	public function getCombo($defaultValue=null,$where=null){
		$result = $this->select('ID,NAME',  null, $where = $where);
		$html = '<option value=""></option>';
		while($row=mysqli_fetch_array($result)){
			if($defaultValue==$row['ID'])
				$html .= '<option selected="selected" value="'.$row['ID'].'">'.$row['NAME'].'</option>';
			else
				$html .= '<option value="'.$row['ID'].'">'.$row['NAME'].'</option>';
		}
		return $html;
	}
	
	public function getIOUID()
	{
		$cols	= " finance_mst_pettycash_category.ID";
		
		$where	= "finance_mst_pettycash_category.IOU_STATUS = 1 AND finance_mst_pettycash_category.STATUS = 1";
		
		$result	= $this->select($cols,NULL,$where,NULL,NULL);
		$row	= mysqli_fetch_array($result);
		return	$row['ID'];
	}
	public function getIOUStatus()
	{
		$cols	= " COUNT(finance_mst_pettycash_category.IOU_STATUS) AS IOU_STATUS";
		$where	= " finance_mst_pettycash_category.`STATUS` = 1 AND
					finance_mst_pettycash_category.IOU_STATUS = 1";
		
		$result	= $this->select($cols,NULL,$where,NULL,NULL);
		$row	= mysqli_fetch_array($result);
		return	$row['IOU_STATUS'];
		
	}	
	//END }
}
?>
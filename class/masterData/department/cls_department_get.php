<?php
class cls_department_get
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;	
	}

	public function getDepartmentSelection($company)
	{
		return $this->getDepartmentSelection_sql($company);
	}

	private function getDepartmentSelection_sql($company)
	{
		$result = $this->getDepartmentSql();
			$html = "<option value=\"".""."\">".""."</option>";
		while($row = mysqli_fetch_array($result))
		{
			if($company==$row["DEPARTMENT_ID"])
				$html .= "<option value=\"".$row["DEPARTMENT_ID"]."\" selected=\"selected\">".$row["DEPARTMENT_NAME"]."</option>";
			else
				$html .= "<option value=\"".$row["DEPARTMENT_ID"]."\">".$row["DEPARTMENT_NAME"]."</option>";
		}
		return $html;
	}

	private function getDepartmentSql()
	{
		$sql = "SELECT
				  DE.intId   AS DEPARTMENT_ID,
				  DE.strName AS DEPARTMENT_NAME
				FROM mst_department DE
				WHERE DE.intStatus = 1
				ORDER BY DE.strName";
		return $this->db->RunQuery($sql);
	}
}
?>
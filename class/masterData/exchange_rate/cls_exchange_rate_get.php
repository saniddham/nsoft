<?php
class cls_exchange_rate_get
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;	
	}
	
	public function GetAllValues($id,$deci,$date,$companyId,$executeType)
	{
		return $this->GetAllValues_sql($id,$deci,$date,$companyId,$executeType);
	}
	
	public function GetAllBankValues($currencyId,$bankId,$deci,$date,$companyId,$executeType)
	{
		return $this->GetAllBankValues_sql($currencyId,$bankId,$deci,$date,$companyId,$executeType);
	}

	private function GetAllValues_sql($id,$deci,$date,$companyId,$executeType)
	{
		$sql = "SELECT 
					ROUND(ER.dblSellingRate,$deci)		AS SELLING_RATE,
					ROUND(ER.dblBuying,$deci)			AS BUYING_RATE,
					ROUND(ER.dblExcAvgRate,$deci)		AS AVERAGE_RATE 
				FROM mst_financeexchangerate ER 
				WHERE 
					ER.intCurrencyId = $id
					AND ER.dtmDate = '$date'
					AND ER.intCompanyId = '$companyId'";
		$result = $this->db->$executeType($sql);
		return	mysqli_fetch_array($result);
	}
	
	private function GetAllBankValues_sql($currencyId,$bankId,$deci,$date,$companyId,$executeType)
	{
		$sql = "SELECT
				  ROUND(ER.SELLING_RATE,2) AS SELLING_RATE,
				  ROUND(ER.BUYING_RATE,2) AS BUYING_RATE,
				  ROUND(ER.AVERAGE_RATE,2) AS AVERAGE_RATE
				FROM mst_financeexchangerate_bankwise ER
				WHERE ER.BANK_ID = $bankId
					AND ER.CURRENCY_ID = $currencyId
					AND ER.DATE = '$date'
					AND ER.COMPANY_ID = '$companyId'";
		$result = $this->db->$executeType($sql);
		return	mysqli_fetch_array($result);
	}
}
?>
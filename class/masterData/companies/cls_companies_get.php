<?php
class cls_companies_get
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;	
	}
	
	public function GetLocationFields($fieldName,$locationId)
	{
		return $this->GetLocationFields_sql($fieldName,$locationId);
	}
	
	public function GetCompanyReportHeader($locationId)
	{
		return $this->GetCompanyReportHeader_sql($locationId);
	}
	
	public function getCompanySelection($company)
	{
		return $this->getCompanySelection_sql($company);
	}
	
	public function getLocationSelection($companyId,$location)
	{
		return $this->getLocationSelection_sql($companyId,$location);
	}
	
	private function GetLocationFields_sql($fieldName,$locationId)
	{
		$sql = "SELECT $fieldName FROM mst_locations WHERE intId = $locationId";
		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		return $row[$fieldName];
	}
	
	private function GetCompanyReportHeader_sql($locationId)
	{
		$sql = "SELECT
				C.strName					AS COMPANY_NAME,
				L.strName 					AS LOCATION_NAME,
				L.strAddress				AS LOCATION_ADDRESS,
				L.strStreet					AS LOCATION_STREET,
				L.strCity					AS LOCATION_CITY,
				CO.strCountryName			AS COMPANY_COUNTRY,		
				L.strPhoneNo				AS LOCATION_PHONE,
				L.strFaxNo					AS LOCATION_FAX,
				L.strEmail					AS LOCATION_EMAIL,
				C.strWebSite				AS LOCATION_WEB,
				L.strZip					AS LOCATION_ZIP,
				C.strVatNo					AS COMPANY_VAT,
				C.strSVatNo					AS COMPANY_SVAT,
				C.intBaseCurrencyId			AS BASE_CURRENCY_ID,
				CU.strCode					AS BASE_CURRENCY_CODE,
				CU.strSymbol				AS BASE_CURRENCY_SYMBOL
				FROM
				mst_locations L
				Inner Join mst_companies C ON L.intCompanyId = C.intId
				Inner Join mst_country CO ON C.intCountryId = CO.intCountryID
				INNER JOIN mst_financecurrency CU ON CU.intId = C.intBaseCurrencyId 
				WHERE
				L.intId =  '$locationId'";
		$result = $this->db->RunQuery($sql);
		return mysqli_fetch_array($result);
	}
	
	private function getCompanySelection_sql($company)
	{
		$result = $this->getCompanySql();
			$html = "<option value=\"".""."\">".""."</option>";
		while($row = mysqli_fetch_array($result))
		{
			if($company==$row["COMPANY_ID"])
				$html .= "<option value=\"".$row["COMPANY_ID"]."\" selected=\"selected\">".$row["COMPANY_NAME"]."</option>";
			else
				$html .= "<option value=\"".$row["COMPANY_ID"]."\">".$row["COMPANY_NAME"]."</option>";
		}
		return $html;
	}
	
	private function getLocationSelection_sql($companyId,$location)
	{
		$result = $this->getLocationSql($companyId);
		while($row = mysqli_fetch_array($result))
		{
			if($location==$row["LOCATION_ID"])
				$html .= "<option value=\"".$row["LOCATION_ID"]."\" selected=\"selected\">".$row["LOCATION_NAME"]."</option>";
			else
				$html .= "<option value=\"".$row["LOCATION_ID"]."\">".$row["LOCATION_NAME"]."</option>";
		}
		return $html;
	}
	
	private function getCompanySql()
	{
		$sql = "SELECT
				  CO.intId		AS COMPANY_ID,
				  CO.strName	AS COMPANY_NAME
				FROM mst_companies CO
				WHERE CO.intStatus = 1
				ORDER BY strName";
		return $this->db->RunQuery($sql);
	}
	
	private function getLocationSql($companyId)
	{
		$sql = "SELECT
				  LO.intId		AS LOCATION_ID,
				  LO.strName	AS LOCATION_NAME
				FROM mst_locations LO
				WHERE LO.intStatus = 1
					AND LO.intCompanyId = $companyId
				ORDER BY strName";
		return $this->db->RunQuery($sql);
	}
}
?>
<?php
class cls_tickets_set
{
	private $db;
	function __construct($db)
	{
		$this->db = $db;
	}
	/////////////////////Save Ticket///////////////////////////////
	////////////////////////////////////////////////////////////////////
	public function saveDevhrs($devHrs,$ticketId)
	{
		$sql 	= "UPDATE tickets SET develpmentHoures='$devHrs' WHERE no='$ticketId'";
		$result = $this->db->RunQuery($sql);
		if($result)
		{ //////// sucssess ///////////////////
			$response['msg'] = 'Develeopment Hrs Saved.';
			$response['type']= 'pass';
		}
		else
		{
			$response['msg'] 	= 'Saving Error.'.mysqli_error();
			$response['sql'] 	= $sql;
			$response['error']  = mysqli_error();
			$response['type'] = "fail";
		}
		
		return $response;
	}
	
	/////////////////////Save Ticket///////////////////////////////
	////////////////////////////////////////////////////////////////////
	public function changeTrack($track,$ticketId)
	{
		$sql 	= "UPDATE tickets SET intTrack='$track' WHERE no='$ticketId'";
		$result = $this->db->RunQuery($sql);
		if($result)
		{ //////// sucssess ///////////////////
			$response['msg'] = 'Track Changed.';
			$response['type']= 'pass';
		}
		else
		{
			$response['msg'] 	= 'Saving Error.'.mysqli_error();
			$response['sql'] 	= $sql;
			$response['error']  = mysqli_error();
			$response['type'] = "fail";
		}
		
		return $response;
	}
	/////////////////////Save Ticket///////////////////////////////
	////////////////////////////////////////////////////////////////////
	public function completeTicket($ticketId)
	{
		$sql 	= "UPDATE tickets SET caitStatus='2' WHERE no='$ticketId'";
		$result = $this->db->RunQuery($sql);
		if($result)
		{ //////// sucssess ///////////////////
			$response['msg'] = 'Completed Successfully.';
			$response['type']= 'pass';
		}
		else
		{
			$response['msg'] 	= 'Saving Error.'.mysqli_error();
			$response['sql'] 	= $sql;
			$response['error']  = mysqli_error();
			$response['type'] = "fail";
		}
		
		return $response;
	}
	/////////////////////Save Ticket///////////////////////////////
	////////////////////////////////////////////////////////////////////
	public function saveComment($txtComment,$ticketNo,$userId)
	{
		$sql 	= "INSERT INTO t_comments(ticketId,dateTime,comment,userId)values
		($ticketNo,now(),'$txtComment','$userId')";
		$result = $this->db->RunQuery($sql);
		if($result)
		{ //////// sucssess ///////////////////
		
			$response['msg'] = 'Saved Successfully.';
			$response['type']= 'pass';
		}
		else
		{
			$response['msg'] 	= 'Saving Error.'.mysqli_error();
			$response['sql'] 	= $sql;
			$response['error']  = mysqli_error();
			$response['type'] = "fail";
		}
		
		return $response;
	}
	/////////////////////Save Cait Side///////////////////////////////
	////////////////////////////////////////////////////////////////////
	public function saveCaitSide($devHrs,$statusCait,$no,$track,$comment){
		$sql = "UPDATE tickets SET dtmCompleteDate=now() ,caitTrack='$track',caitStatus='$statusCait',
		develpmentHoures='$devHrs', caitComment='$comment' WHERE no='$no'";
		$result = $this->db->RunQuery($sql);
		if($result)
		{ //////// sucssess ///////////////////
			$response['msg'] = 'Saving Successfully.';
			$response['type']= 'pass';
		}
		else
		{
			$response['msg'] 	= 'Saving Error.'.mysqli_error();
			$response['sql'] 	= $sql;
			$response['error']  = mysqli_error();
			$response['type'] = "fail";
		}
		
		return $response;
	}
	
	/////////////////////Ticket Approve///////////////////////////////
	////////////////////////////////////////////////////////////////////
	public function appr($ticketNo,$userName){
		$sql = "UPDATE tickets SET intAdminApproveBy='1', dtmApprouveDate=now(), approveBy='$userName'  WHERE no='$ticketNo'";
		$result = $this->db->RunQuery($sql);
		if($result)
		{ //////// sucssess ///////////////////
			$response['msg'] = 'Approve Successfully.';
			$response['type']= 'pass';
			$response['successAppr'] = "<h1>Ticket # $ticketNo <small>( <b><font color='green'>Ticket Approved</font></b> )
			</small></h1>";
		}
		else
		{
			$response['msg'] 	= 'Saving Error.'.mysqli_error();
			$response['sql'] 	= $sql;
			$response['error']  = mysqli_error();
			$response['type'] = "fail";
		}
		
		return $response;
	}
	
	/////////////////////Save Ticket///////////////////////////////
	////////////////////////////////////////////////////////////////////
	public function saveReport($module,$program,$note,$priorty,$contactNo,$contactEmail,$userId,$userName,$cboTrack)
	{
		$sql 	= "INSERT INTO tickets(reportdatetime,userId,userName,note,
		fixStatus,userStatus,module,program,priorty,intAdminApproveBy,intTrack,caitStatus)values
		(now(),'$userId','$userName','$note','0','0','$module','$program','$priorty','0','$cboTrack','1')";
		$result = $this->db->RunQuery($sql);
		if($result)
		{ //////// sucssess ///////////////////
		$sql1 = "UPDATE `sys_users` SET `strContactNo`='$contactNo',`strEmail`='$contactEmail' WHERE (`intUserId`='$userId')  ";
		$result1 = $this->db->RunQuery($sql1);
		if($result1){
			$response['msg'] = 'Saved Successfully.';
			$response['type']= 'pass';
		}
		}
		else
		{
			$response['msg'] 	= 'Saving Error.'.mysqli_error();
			$response['sql'] 	= $sql;
			$response['error']  = mysqli_error();
			$response['type'] = "fail";
		}
		
		return $response;
	}
	
	/////////////////////Update ticket///////////////////////////////
	////////////////////////////////////////////////////////////////////
	public function updateReport($module,$program,$note,$priorty,$contactNo,$contactEmail,$userId,$userName,$id,$cboTrack)
	{
		$sql 	= "UPDATE tickets SET userId='$userId', userName='$userName',note='$note',
		module='$module',program='$program',priorty='$priorty', intAdminApproveBy='0', intTrack='$cboTrack' , 
		caitStatus='1' WHERE (no='$id')";
		$result = $this->db->RunQuery($sql);
		if($result)
		{ //////// sucssess ///////////////////
			$sql1 = "UPDATE `sys_users` SET `strContactNo`='$contactNo',`strEmail`='$contactEmail' WHERE (`intUserId`='$userId')  ";
		$result1 = $this->db->RunQuery($sql1);
		if($result1){
			$response['msg'] = 'Saved Successfully.';
			$response['type']= 'pass';
		}
		}
		else
		{
			$response['msg'] 	= 'Saving Error.'.mysqli_error();
			$response['sql'] 	= $sql;
			$response['error']  = mysqli_error();
			$response['type'] = "fail";
		}
		
		return $response;
	}

}
?>

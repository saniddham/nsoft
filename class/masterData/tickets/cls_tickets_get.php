<?php
class cls_tickets_get
{
	private $db;
	function __construct($db)
	{
		$this->db = $db;	
	}
	
	/////////////////////Load Track///////////////////////////////
	////////////////////////////////////////////////////////////////////
	public function getLoadTrack() 
	{
		$sql = "SELECT
				t_trackers.intTrackerId,
				t_trackers.strTrackerName,
				t_trackers.intStatus
				FROM
				t_trackers WHERE t_trackers.intTrackerId<'4'";
		$result = $this->db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$option .="<option value=\"".$row['intTrackerId']."\">".$row['strTrackerName']."</option>";
		}	
		return $option;
	
	}
	/////////////////////Load Admin Approve Page///////////////////////////////
	////////////////////////////////////////////////////////////////////
	public function goAdminTicket($ticketId,$userId){
		$sql = "SELECT
tickets.`no`,
tickets.reportdatetime,
tickets.userId,
tickets.username,
tickets.note,
tickets.fixStatus,
tickets.fixtime,
tickets.userStatus,
tickets.module,
tickets.program,
tickets.priorty,
tickets.intAdminApproveBy,
tickets.dtmApprouveDate,
tickets.intTrack,
tickets.approveBy
FROM
tickets
WHERE
tickets.`no` =  '$ticketId'";
		$result = $this->db->RunQuery($sql);
		$row= mysqli_fetch_array($result);
		$sql2 = "SELECT
menus.strName
FROM
menus
WHERE
menus.intId =  ".$row['module']."
";
$getName = $this->db->RunQuery($sql2);
$row2 = mysqli_fetch_array($getName);

$sql3 = "SELECT
menus.strName
FROM
menus
WHERE
menus.intId =  ".$row['program']."
";
$getName1 = $this->db->RunQuery($sql3);
$row3 = mysqli_fetch_array($getName1);
		////// sucssess //
		if($row['intTrack']=="1"){
				$track = "Bug";
			}
			else if($row['intTrack']=="2"){
				$track = "Feature";
			}
			else if($row['intTrack']=="3"){
				$track = "Support";
			}else{
				$track = "";
			}
		if($row['intAdminApproveBy']=="0"){
			$sql3 		= "SELECT intAdminApproveBy FROM t_user_privileges ";
			$chekAppr 	= $this->db->RunQuery($sql3);
			$row3=mysqli_fetch_array($chekAppr);
			if($row3['intAdminApproveBy']==$userId){
			$link = "<td colspan='4'><div align='center'><img  type='submit' border='0' 
			src='../../../images/Tapprove.png' alt='Save' name='butApp'width='92' height='24'  
			class='mouseover' id='butApp' tabindex='24'/></div></td>";
			}else{
				$link = "";
			}
		}else if($row['intAdminApproveBy']=="1"){
			$link = "";
		}
		if($row['priorty']=="1"){
			$priorty = "Low";
		}else if($row['priorty']=="2"){
			$priorty = "Normal";
		}else if($row['priorty']=="3"){
			$priorty = "High";
		}
		if($row['intAdminApproveBy']=="1"){
			$successAppr 	= "<b><font color='green'>Approved</font></b>";
			$aby 			= "".$row['approveBy']."";
			$adate 			= "".$row['dtmApprouveDate']."";
		}else if($row['intAdminApproveBy']=="0"){
			$successAppr 	= "<b><font color='red'>Not Approved</font></b>";
			$aby 			= "--";
			$adate 			= "--";
		}
			$response['userName'] 		= $row['username'];
			$response['note'] 			= "<textarea name='reportAdminNote' id='reportAdminNote' 
			cols='45' rows='5' style='width:350px' >".$row['note']."</textarea>";
			$response['module'] 		= $row2[0];
			$response['program'] 		= $row3[0];
			$response['priorty'] 		= $priorty;
			$response['appr']			= "$link";
			$response['successAppr'] 	= "<h1>Ticket # ".$row['no']." <small>($successAppr)</small></h1>";
			$response['aby']			= "$aby";
			$response['adate']			= "$adate";
			$response['track']			= "$track";
			return $response;
	}
	
	/////////////////////Load User Contacts///////////////////////////////
	////////////////////////////////////////////////////////////////////
	public function loadContact($userId) 
	{
		$sql 	= "SELECT strContactNo,strEmail from sys_users WHERE intUserId='$userId'";
		$result = $this->db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		
			$response['contactNo'] 		= "".$row['strContactNo']."";
			$response['contactEmail'] 	= "".$row['strEmail']."";		
			
		return $response;
	}
	
	/////////////////////////////////////
	public function getNote($ticketNo) 
	{
		$sql 	= "	SELECT
						tickets.note
					FROM tickets
					WHERE
						tickets.`no` =  '$ticketNo'
					";
		$result = $this->db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		return $row['note'];
		
	}
	//////////////////////////////////////////////////
	public function getCaitStatus($ticketNo) 
	{
		$sql 	= "	SELECT
						tickets.caitStatus
					FROM tickets
					WHERE
						tickets.`no` =  '$ticketNo'
					";
		$result = $this->db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		return $row['caitStatus'];
		
	}
	/////////////////////Load ticket comment///////////////////////////////
	////////////////////////////////////////////////////////////////////
	public function changeCait($ticketId,$userId) 
	{
			$sql 	= "SELECT
						t_user_privileges.intCaitApprovalBy
						FROM
						t_user_privileges
					";
			$result = $this->db->RunQuery($sql);
			$row=mysqli_fetch_array($result);
			######################################
			$sql2 	= "SELECT
					tickets.intTrack,
					tickets.caitStatus,
					tickets.develpmentHoures
					FROM
					tickets
					WHERE
					tickets.`no` =  '$ticketId'

					";
			$result2 = $this->db->RunQuery($sql2);
			$row2=mysqli_fetch_array($result2);
			######################################
			
			if($row['intCaitApprovalBy']==$userId){
				$response['msg'] = "";
			}else{
				$response['msg'] = "dont give permission";
			}
			
				$response['track']  	= $row2['intTrack'];
			
			$response['devHrs'] 	= $row2['develpmentHoures'];
			$response['chkbutton'] 	= $row2['caitStatus'];
		return $response;
	}
	/////////////////////Load ticket comment///////////////////////////////
	////////////////////////////////////////////////////////////////////
	public function loadComments($ticketNo) 
	{
			$sql 	= "SELECT
			sys_users.strUserName,
			t_comments.`comment`
			FROM
			t_comments
			Inner Join sys_users ON sys_users.intUserId = t_comments.userId
			WHERE
			t_comments.ticketId =  '$ticketNo'
			ORDER BY
			t_comments.`dateTime` ASC
					";
			$result = $this->db->RunQuery($sql);
			$response = $result;
		return $response;
	}
	///////////////Load Pogram///////////////////////////////
	////////////////////////////////////////////////////////////////////
	public function loadProgram2() 
	{
		$sql 	= "SELECT intId,strName from menus";
		$result = $this->db->RunQuery($sql);
		$option ="<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$option .="<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}	
		return $option;
	
	}
	
	/////////////////////Load Program///////////////////////////////
	////////////////////////////////////////////////////////////////////
	public function loadProgram1($programId) 
	{
		$sql 				= "SELECT intId,strName from menus WHERE intParentId='$programId'";
		$result 			= $this->db->RunQuery($sql);
		$option1['msg'] 	="<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$option1['msg'] .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}	
		return $option1;
	}
	
	/////////////////////Load Program///////////////////////////////
	////////////////////////////////////////////////////////////////////
	public function loadProgram($programId) 
	{
		$sql 			= "SELECT intId,strName from menus WHERE intParentId='$programId'";
		$result 		= $this->db->RunQuery($sql);
		$option1['msg'] ="<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$option1['msg'] .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}	
		return $option1;
	}
	
	/////////////////////Load Report 2nd Stage///////////////////////////////
	////////////////////////////////////////////////////////////////////
	public function loadReport($id){
		$sql = "SELECT
tickets.reportdatetime,
tickets.userId,
tickets.username,
tickets.note,
tickets.fixStatus,
tickets.fixtime,
tickets.userStatus,
tickets.module,
tickets.program,
tickets.priorty,
tickets.intAdminApproveBy,
tickets.dtmApprouveDate,
tickets.intTrack,
tickets.approveBy,
tickets.`no`,
sys_users.strContactNo,
sys_users.strEmail
FROM
tickets
Inner Join sys_users ON sys_users.intUserId = tickets.userId
WHERE
tickets.`no` =  '$id'
";
		$result = $this->db->RunQuery($sql);
		$row= mysqli_fetch_array($result);
		////// sucssess //
		if($row['intAdminApproveBy']=="0"){
			$link = "<td colspan='4'><div align='center'><img  type='submit' 
			border='0' src='../../../images/approve.png' alt='Save' name='butApp'width='101' 
			height='22'  class='mouseover' id='butApp' tabindex='24'/></div></td>";
		}else if($row['intAdminApproveBy']=="1"){
			$link = "";
		}
			$response['userName'] 		= $row['username'];
			$response['note'] 			= $row['note'];
			$response['module'] 		= $row['module'];
			$response['program'] 		= $row['program'];
			$response['priorty'] 		= $row['priorty'];
			$response['contactNo'] 		= $row['strContactNo'];
			$response['contactEmail'] 	= $row['strEmail'];
			$response['track'] 			= $row['intTrack'];
			$response['appr']			= "$link";
			return $response;
	}
	
	/////////////////////Get New ticket no///////////////////////////////
	////////////////////////////////////////////////////////////////////
	public function newTicketNo()
	{
		/// chek//////////
		
		$sql 	= "SELECT no FROM tickets ORDER BY no DESC";
		$result = $this->db->RunQuery($sql);
		if($result){
		$row= mysqli_fetch_array($result);
		////// sucssess //
			$newTicketNoNew = $row['no'] + 1;
			$response 		= $newTicketNoNew;
		}
			return $response;
	}
	
	/////////////////////Load Ticket list by Add user///////////////////////////////
	////////////////////////////////////////////////////////////////////
	public function loadAddBy() 
	{
		$sql 	= "SELECT DISTINCT username, userId FROM tickets ORDER BY username DESC";
		$result = $this->db->RunQuery($sql);
		$option ="<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$option .="<option value=\"".$row['userId']."\">".$row['username']."</option>";
		}	
		return $option;
	}
	
	/////////////////////Load Module///////////////////////////////
	////////////////////////////////////////////////////////////////////
	public function loadModule() 
	{
		 ////////////////  Load Module/////////////
		$sql 	= "SELECT intId,strName from menus WHERE intParentId='0'";
		$result = $this->db->RunQuery($sql);
		$option ="<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$option .="<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}	
		return $option;
	}
	public function loadTicketList($cboAddBy,$userId,$status) 
	{
		if($status=="1"){
			$status1 = "0";
		}else if($status=="2"){
			$status1 = "1";
		}
		if($statusId=="1"){
			$status = "0";
		}else if($statusId=="2"){
			$status = "1";
		}
		if($cboAddBy==""){
			$sqlSelect = "AND intAdminApproveBy='$status1' ";
		}else{
			$sqlSelect = "AND tickets.userId='$cboAddBy' AND intAdminApproveBy='$status1'";
		}
		$sql = "		
SELECT
tickets.`no`,
tickets.reportdatetime,
tickets.userId,
tickets.username,
tickets.note,
tickets.fixStatus,
tickets.fixtime,
tickets.userStatus,
tickets.module,
tickets.program,
tickets.priorty,
tickets.intAdminApproveBy,
tickets.intTrack,
tickets.caitStatus,
tickets.develpmentHoures,
tickets.caitComment,
tickets.caitTrack,
tickets.dtmCompleteDate,
tickets.dtmApprouveDate,
tickets.approveBy,
menus.strName
FROM
tickets
Inner Join menus ON menus.intId = tickets.module
WHERE
menus.intParentId =  '0' $sqlSelect
ORDER BY
tickets.reportdatetime DESC
		";
		
		$result 		= $this->db->RunQuery($sql);
		$link['msg'] 	= " <thead id='caitPerm'>
        <tr class='normalfnt'>
          <th colspan='9'>SCREENLINE (PVT) LTD</th>
          <th colspan='5'>CAIT</th>
        </tr>
        <tr class='normalfnt'>
          <th width='6%'>Ticket No</th>
          <th width='5%'>Track</th>
          <th width='5%'>Add By</th>
          <th width='8%'>Add Date</th>
          <th width='8%'>Module</th>
          <th width='9%'>Program</th>
          <th width='13%'>Note</th>
          <th width='6%'>Priority</th>
          <th width='8%'>Approve</th>
          <th width='8%'>Track</th>
          <th width='12%'>Development Houres</th>
          <th width='6%'>Status</th>
		  <th width='6%'>Comment</th>
          <th width='6%'>Save</th>
          </tr>
         </thead>";
		while($row=mysqli_fetch_array($result)){
			if($row['priorty']=="1"){
				$priorty = "Low";
			}else if($row['priorty']=="2"){
				$priorty = "Normal";
			}else if($row['priorty']=="3"){
				$priorty = "High";
			}
			if($row['intTrack']=="1"){
				$track = "Bug";
			}
			else if($row['intTrack']=="2"){
				$track = "Feature";
			}
			else if($row['intTrack']=="3"){
				$track = "Support";
			}else{
				$track = "";
			}
			$sql1 		= "SELECT strName FROM menus WHERE intId='".$row['program']."'";
			$program1 	= $this->db->RunQuery($sql1);
			$row1		=mysqli_fetch_array($program1);
			
			$note1=substr($row['note'] ,0, 10);
			###############################################################################
			$sql2 = "SELECT
				t_trackers.intTrackerId,
				t_trackers.strTrackerName,
				t_trackers.intStatus
				FROM
				t_trackers WHERE t_trackers.intTrackerId <'4'";
				$result2 = $this->db->RunQuery($sql2);
				$myoption="<select name='cboTrack2' id='cboTrack2' style='width:100px'><option value=''></option>";
				while($row2=mysqli_fetch_array($result2))
				{
					$myoption .="<option value=\"".$row2['intTrackerId']."\">".$row2['strTrackerName']."</option>";
				}
					$myoption .="</select>";
			###############################################################################	
			$sql4 = "SELECT strTrackerName FROM t_trackers WHERE intTrackerId='".$row['caitTrack']."'";
			$chekAppr1 = $this->db->RunQuery($sql4);
			$caitTrackName=mysqli_fetch_array($chekAppr1);
			###############################################################################
			$sql3 		= "SELECT intAdminApproveBy,intCaitApprovalBy FROM t_user_privileges ";
			$chekAppr 	= $this->db->RunQuery($sql3);
			$row3=mysqli_fetch_array($chekAppr);
			
			if($row['intAdminApproveBy']=="0"){
				if($row3['intAdminApproveBy']==$userId){
				$approve = "<a href='admin-tickets.php?ticketId=".$row['no']."&appr=1' target='_blank'>Approve</a>";
			}else{
				$approve = "";
			}
			}else if($row['intAdminApproveBy']=="1"){
				$approve = "(".$row['approveBy'].")<br/>".$row['dtmApprouveDate']."";
			}
				if($row['caitStatus']=="1"){
				if($row3['intCaitApprovalBy']==$userId){
					if($row['intAdminApproveBy']=="1"){
				$caitTrack 	= $myoption;
				$comment 	= "<textarea name='caitComment' id='caitComment' style='width:150px; height:27px'></textarea>";
				$caitDevHrs = "<input type='text' name='txtDevHoure' id='txtDevHoure' 
				style='width:100px' value='".$row['develpmentHoures']."'/>";
				$caitStatus = " <select name='cboCaitStatus' id='cboCaitStatus'>
		  <option value='1'>Pending</option>
		  <option value='2'>Complete</option>
		  </select>";
		  		$save 		= "<img  type='submit' border='0' src='../../../images/Tsave.jpg' 
		  alt='Save' name='butSaveCait'width='92' height='24'  class='mouseover' id='butSaveCait' tabindex='24'/>";
					}else if($row['intTrack']=="1"){
						$caitTrack 	= $myoption;
				$comment 	= "<textarea name='caitComment' id='caitComment' style='width:150px; height:27px'></textarea>";
				$caitDevHrs = "<input type='text' name='txtDevHoure' id='txtDevHoure' 
				style='width:100px'/>";
				$caitStatus = " <select name='cboCaitStatus' id='cboCaitStatus'>
		  <option value='1'>Pending</option>
		  <option value='2'>Complete</option>
		  </select>";
		  $save 			= "<img  type='submit' border='0' src='../../../images/Tsave.jpg' 
		  alt='Save' name='butSaveCait'width='92' height='24'  class='mouseover' id='butSaveCait' tabindex='24'/>";
					}
			}else{
				if($row['caitStatus']=="1"){
					$caitStatusView = "Pending";
				}else if($row['caitStatus']=="2"){
					$caitStatusView = "Complete";
				}
				$comment = substr($row['caitComment'] ,0, 10);
				$caitTrack  = $caitTrackName['strTrackerName'];
				$caitDevHrs = $row['develpmentHoures'];
				$caitStatus = $caitStatusView ;
				$save 		= $row['dtmCompleteDate'];
			}
			}else{
				if($row['caitStatus']=="1"){
					$caitStatusView = "Pending";
				}else if($row['caitStatus']=="2"){
					$caitStatusView = "Complete";
				}
				$comment = substr($row['caitComment'] ,0, 10);
				$caitTrack  = $caitTrackName['strTrackerName'];
				$caitDevHrs = $row['develpmentHoures'];
				$caitStatus = $caitStatusView ;
				$save 		= $row['dtmCompleteDate'];
			}
				
			###############################################################################
			$link['msg'] .= "<tr id=\"".$row['no']."\">
          <td><a href='admin-tickets.php?&ticketId=".$row['no']."'  target='_blank' id='ticketIdNo' name='ticketIdNo'># ".$row['no']."				          </a><input type='hidden' name='intId' id='intId' value='".$row['no']."' style='width:10px' class='tdtd'></td>
		  <td>$track<input type='hidden' name='tNo' id='tNo' value='".$row['no']."'></td>
          <td>".$row['username']."</td>
          <td>".$row['reportdatetime']."</td>
         <td>".$row['strName']."</td>
          <td id='tdProgram'>$row1[0]</td>
          <td><a href='admin-tickets.php?&ticketId=".$row['no']."'  target='_blank' id='ticketIdNo' name='ticketIdNo'>$note1</a></td>
          <td>$priorty</td>
          <td>$approve</td>
		  <td>$caitTrack</td>
		  <td>$caitDevHrs</td>
		  <td>$caitStatus</td>
		  <td>$comment</td>
		  <td>$save</td>
        </tr>";
		}
		return $link;
	}
	
	/////////////////////Load Tickets list Defult///////////////////////////////
	////////////////////////////////////////////////////////////////////
	public function getErrorList($userId){
			$sql3 = "SELECT intAdminApproveBy,intCaitApprovalBy FROM t_user_privileges ";
			$chekAppr = $this->db->RunQuery($sql3);
			$row3=mysqli_fetch_array($chekAppr);
			
			if($row3['intAdminApproveBy']==$userId){
				$chek = "intAdminApproveBy";
				$ff = "0";
			}
			else if($row3['intCaitApprovalBy']==$userId){
				$chek = "caitStatus";
				$ff = "1";
			}else{
				$chek = "intAdminApproveBy";
				$ff = "0";
			}
			
		$sql = "		
				SELECT
				tickets.`no`,
				tickets.reportdatetime,
				tickets.userId,
				tickets.username,
				tickets.note,
				tickets.fixStatus,
				tickets.fixtime,
				tickets.userStatus,
				tickets.module,
				tickets.program,
				tickets.priorty,
				tickets.intAdminApproveBy,
				tickets.intTrack,
				tickets.caitStatus,
				tickets.develpmentHoures,
				tickets.caitComment,
				tickets.caitTrack,
				tickets.dtmCompleteDate,
				tickets.dtmApprouveDate,
				tickets.approveBy,
				menus.strName
				FROM
				tickets
				Inner Join menus ON menus.intId = tickets.module
				WHERE
				menus.intParentId =  '0' AND $chek ='$ff'
				ORDER BY
				tickets.`no` DESC
		";
		$result 		= $this->db->RunQuery($sql);
		$link['msg'] 	= "  <thead id='caitPerm'>
        <tr class='normalfnt'>
          <th colspan='9'>SCREENLINE (PVT) LTD</th>
          <th colspan='5'>CAIT</th>
        </tr>
        <tr class='normalfnt'>
          <th width='6%'>Ticket No</th>
          <th width='5%'>Track</th>
          <th width='5%'>Add By</th>
          <th width='8%'>Add Date</th>
          <th width='8%'>Module</th>
          <th width='9%'>Program</th>
          <th width='13%'>Note</th>
          <th width='6%'>Priority</th>
          <th width='8%'>Approve</th>
          <th width='8%'>Track</th>
          <th width='12%'>Development Houres</th>
          <th width='6%'>Status</th>
		  <th width='6%'>Comment</th>
          <th width='6%'>Save</th>
          </tr>
         </thead>";
		while($row=mysqli_fetch_array($result)){

			if($row['priorty']=="1"){
				$priorty = "Low";
			}else if($row['priorty']=="2"){
				$priorty = "Normal";
			}else if($row['priorty']=="3"){
				$priorty = "High";
			}
			if($row['intTrack']=="1"){
				$track = "Bug";
			}
			else if($row['intTrack']=="2"){
				$track = "Feature";
			}
			else if($row['intTrack']=="3"){
				$track = "Support";
			}else{
				$track = "";
			}
			$sql1 		= "SELECT strName FROM menus WHERE intId='".$row['program']."'";
			$program1 	= $this->db->RunQuery($sql1);
			$row1		= mysqli_fetch_array($program1);
			$note1=substr($row['note'] ,0, 10);
			###############################################################################
			$sql2 = "SELECT
				t_trackers.intTrackerId,
				t_trackers.strTrackerName,
				t_trackers.intStatus
				FROM
				t_trackers WHERE t_trackers.intTrackerId<'4'";
				$result2 	= $this->db->RunQuery($sql2);
				$myoption	="<select name='cboTrack2' id='cboTrack2' style='width:100px'><option value=''></option>";
				while($row2=mysqli_fetch_array($result2))
				{
					$myoption 	.="<option value=\"".$row2['intTrackerId']."\">".$row2['strTrackerName']."</option>";
				}
					$myoption 	.="</select>";
			###############################################################################	
			$sql4 = "SELECT strTrackerName FROM t_trackers WHERE intTrackerId='".$row['caitTrack']."'";
			$chekAppr1 = $this->db->RunQuery($sql4);
			$caitTrackName=mysqli_fetch_array($chekAppr1);
			###############################################################################
			if($row['intAdminApproveBy']=="0"){
				if($row3['intAdminApproveBy']==$userId){
				$approve = "<a href='admin-tickets.php?ticketId=".$row['no']."&appr=1' target='_blank'>Approve</a>";
			}else{
				$approve = "";
			}
			}else if($row['intAdminApproveBy']=="1"){
				$approve = "(".$row['approveBy'].")<br/>".$row['dtmApprouveDate']."";
			}
				if($row['caitStatus']=="1"){
				if($row3['intCaitApprovalBy']==$userId){
					if($row['intAdminApproveBy']=="1"){
				$caitTrack 	= $myoption;
				$comment 	= "<textarea name='caitComment' id='caitComment' style='width:150px; height:27px'></textarea>";
				$caitDevHrs = "<input type='text' name='txtDevHoure' id='txtDevHoure' 
				style='width:100px' value='".$row['develpmentHoures']."'/>";
				$caitStatus = " <select name='cboCaitStatus' id='cboCaitStatus'>
		  <option value='1'>Pending</option>
		  <option value='2'>Complete</option>
		  </select>";
		  $save 			= "<img  type='submit' border='0' src='../../../images/Tsave.jpg' 
		  alt='Save' name='butSaveCait'width='92' height='24'  class='mouseover' id='butSaveCait' tabindex='24'/>";
					}else if($row['intTrack']=="1"){
						$caitTrack 	= $myoption;
				$comment 	= "<textarea name='caitComment' id='caitComment' style='width:150px; height:27px'></textarea>";
				$caitDevHrs = "<input type='text' name='txtDevHoure' id='txtDevHoure' 
				style='width:100px'/>";
				$caitStatus = " <select name='cboCaitStatus' id='cboCaitStatus'>
		  <option value='1'>Pending</option>
		  <option value='2'>Complete</option>
		  </select>";
		  $save 			= "<img  type='submit' border='0' src='../../../images/Tsave.jpg' 
		  alt='Save' name='butSaveCait'width='92' height='24'  class='mouseover' id='butSaveCait' tabindex='24'/>";
					}
			}else{
				if($row['caitStatus']=="1"){
					$caitStatusView = "Pending";
				}else if($row['caitStatus']=="2"){
					$caitStatusView = "Complete";
				}
				$comment = substr($row['caitComment'] ,0, 10);
				$caitTrack  = $row['caitTrackName'];
				$caitDevHrs = $row['develpmentHoures'];
				$caitStatus = $caitStatusView ;
				$save 		= $row['dtmCompleteDate'];
			}
			}else{
				if($row['caitStatus']=="1"){
					$caitStatusView = "Pending";
				}else if($row['caitStatus']=="2"){
					$caitStatusView = "Complete";
				}
				$comment = substr($row['caitComment'] ,0, 10);
				$caitTrack  = $row['caitTrackName'];
				$caitDevHrs = $row['develpmentHoures'];
				$caitStatus = $caitStatusView ;
				$save 		= $row['dtmCompleteDate'];
				
			}
			
			$link['msg'] .= "<tr id=\"".$row['no']."\">
          <td><a href='admin-tickets.php?&ticketId=".$row['no']."'  target='_blank' id='ticketIdNo' name='ticketIdNo'># ".$row['no']."          </a><input type='hidden' name='intId' id='intId' value='".$row['no']."' style='width:10px'></td>
		  <td>$track</td>
          <td>".$row['username']."</td>
          <td>".$row['reportdatetime']."</td>
         <td>".$row['strName']."</td>
          <td id='tdProgram'>$row1[0]</td>
          <td><a href='admin-tickets.php?&ticketId=".$row['no']."'  target='_blank' id='ticketIdNo' name='ticketIdNo'>$note1</a></td>
          <td>$priorty</td>
          <td>$approve</td>
		  <td>$caitTrack</td>
		  <td>$caitDevHrs</td>
		  <td>$caitStatus</td>
		  <td>$comment</td>
		   <td>$save</td>
        </tr>";
		}
		return $link;
	}
	
	/////////////////////Load ticket list by Admin Status///////////////////////////////
	////////////////////////////////////////////////////////////////////
	public function loadList(){
		$sql 		= "		
SELECT
tickets.`no`,
tickets.reportdatetime,
tickets.userId,
tickets.username,
tickets.note,
tickets.fixStatus,
tickets.fixtime,
tickets.userStatus,
tickets.module,
tickets.program,
tickets.priorty,
tickets.caitStatus,
tickets.develpmentHoures,
tickets.intTrack,
menus.strName
FROM
tickets
Inner Join menus ON menus.intId = tickets.module
WHERE
menus.intParentId =  '0' 
ORDER BY
tickets.reportdatetime DESC
		";
		$result 		= $this->db->RunQuery($sql);
		$link	= "<br/>
        <tr class='normalfnt'>
          <th width='6%'>Ticket No</th>
          <th width='5%'>Track</th>
          <th width='5%'>Add By</th>
          <th width='8%'>Add Date</th>
          <th width='8%'>Module</th>
          <th width='9%'>Program</th>
          <th width='13%'>Note</th>
          <th width='6%'>Priority</th>
		  <th width='8%'>Development Houre</th>
          <th width='8%'>Status</th>
          <th width='8%'>View</th>
          </tr>";
		while($row=mysqli_fetch_array($result)){
			if($row['priorty']=="1"){
				$priorty = "Low";
			}else if($row['priorty']=="2"){
				$priorty = "Normal";
			}else if($row['priorty']=="3"){
				$priorty = "High";
			}
			if($row['intTrack']=="1"){
				$track = "Bug";
			}
			else if($row['intTrack']=="2"){
				$track = "Feature";
			}
			else if($row['intTrack']=="3"){
				$track = "Support";
			}else{
				$track = "";
			}
			if($row['caitStatus']=="1"){
				$status = "Pending";
			}else if($row['caitStatus']=="2"){
				$status = "Complete";
			}
			$sql1 		= "SELECT strName FROM menus WHERE intId='".$row['program']."'";
			$program1 	= $this->db->RunQuery($sql1);
			$row1=mysqli_fetch_array($program1);

			$note1=substr($row['note'] ,0, 10);
			
			$link .= "<tr id=\"".$row['no']."\">
          <td><a href='admin-tickets.php?&ticketId=".$row['no']."'  target='_blank' id='ticketIdNo' name='ticketIdNo'># ".$row['no']."				          </a><input type='hidden' name='intId' id='intId' value='".$row['no']."' style='width:10px' class='tdtd'></td>
		  <td>$track<input type='hidden' name='tNo' id='tNo' value='".$row['no']."'></td>
          <td>".$row['username']."</td>
          <td>".$row['reportdatetime']."</td>
         <td>".$row['strName']."</td>
          <td id='tdProgram'>$row1[0]</td>
          <td><a href='admin-tickets.php?&ticketId=".$row['no']."'  target='_blank' id='ticketIdNo' name='ticketIdNo'>$note1</a></td>
          <td>$priorty</td>
		  <td>".$row['develpmentHoures']."</td>
          <td>$status</td>
		  <td><a href='comment-ticket.php?ticketId=".$row['no']." '  target='_blank'>View</td>
        </tr>";
		}
		return $link;
	}
	
	/////////////////////Load ticket list by Admin Status///////////////////////////////
	////////////////////////////////////////////////////////////////////
	public function loadListByCait($status,$userId){
		$sql 		= "		
SELECT
tickets.`no`,
tickets.reportdatetime,
tickets.userId,
tickets.username,
tickets.note,
tickets.fixStatus,
tickets.fixtime,
tickets.userStatus,
tickets.module,
tickets.program,
tickets.priorty,
tickets.intAdminApproveBy,
tickets.intTrack,
tickets.caitStatus,
tickets.develpmentHoures,
tickets.caitComment,
tickets.caitTrack,
tickets.dtmCompleteDate,
tickets.dtmApprouveDate,
tickets.approveBy,
menus.strName
FROM
tickets
Inner Join menus ON menus.intId = tickets.module
WHERE
menus.intParentId =  '0' AND tickets.caitStatus='$status'
ORDER BY
tickets.reportdatetime DESC
		";
		$result 		= $this->db->RunQuery($sql);
		$link['msg'] 	= "  <thead id='caitPerm'>
        <tr class='normalfnt'>
          <th colspan='9'>SCREENLINE (PVT) LTD</th>
          <th colspan='5'>CAIT</th>
        </tr>
        <tr class='normalfnt'>
          <th width='6%'>Ticket No</th>
          <th width='5%'>Track</th>
          <th width='5%'>Add By</th>
          <th width='8%'>Add Date</th>
          <th width='8%'>Module</th>
          <th width='9%'>Program</th>
          <th width='13%'>Note</th>
          <th width='6%'>Priority</th>
          <th width='8%'>Approve</th>
          <th width='8%'>Track</th>
          <th width='12%'>Development Houres</th>
          <th width='6%'>Status</th>
		  <th width='6%'>Comment</th>
          <th width='6%'>Save</th>
          </tr>
         </thead>";
		while($row=mysqli_fetch_array($result)){
			if($row['priorty']=="1"){
				$priorty = "Low";
			}else if($row['priorty']=="2"){
				$priorty = "Normal";
			}else if($row['priorty']=="3"){
				$priorty = "High";
			}
			if($row['intTrack']=="1"){
				$track = "Bug";
			}
			else if($row['intTrack']=="2"){
				$track = "Feature";
			}
			else if($row['intTrack']=="3"){
				$track = "Support";
			}else{
				$track = "";
			}
			$sql1 		= "SELECT strName FROM menus WHERE intId='".$row['program']."'";
			$program1 	= $this->db->RunQuery($sql1);
			$row1=mysqli_fetch_array($program1);

			$note1=substr($row['note'] ,0, 10);
			###############################################################################
			$sql2 = "SELECT
				t_trackers.intTrackerId,
				t_trackers.strTrackerName,
				t_trackers.intStatus
				FROM
				t_trackers WHERE t_trackers.intTrackerId <'4'";
				$result2 = $this->db->RunQuery($sql2);
				$myoption="<select name='cboTrack2' id='cboTrack2' style='width:100px'><option value=''></option>";
				while($row2=mysqli_fetch_array($result2))
				{
					$myoption 	.="<option value=\"".$row2['intTrackerId']."\">".$row2['strTrackerName']."</option>";
				}
					$myoption 	.="</select>";
			###############################################################################	
			$sql4 = "SELECT strTrackerName FROM t_trackers WHERE intTrackerId='".$row['caitTrack']."'";
			$chekAppr1 = $this->db->RunQuery($sql4);
			$caitTrackName=mysqli_fetch_array($chekAppr1);
			###############################################################################
				$sql3 = "SELECT intAdminApproveBy,intCaitApprovalBy FROM t_user_privileges ";
				$chekAppr = $this->db->RunQuery($sql3);
				$row3=mysqli_fetch_array($chekAppr);
			
			if($row['intAdminApproveBy']=="0"){
				if($row3['intAdminApproveBy']==$userId){
				$approve = "<a href='admin-tickets.php?ticketId=".$row['no']."&appr=1' target='_blank'>Approve</a>";
			}else{
				$approve = "";
			}
			}else if($row['intAdminApproveBy']=="1"){
				$approve = "(".$row['approveBy'].")<br/>".$row['dtmApprouveDate']."";
			}
			if($row['caitStatus']=="1"){
				if($row3['intCaitApprovalBy']==$userId){
					if($row['intAdminApproveBy']=="1"){
				$caitTrack 	= $myoption;
				$comment 	= "<textarea name='caitComment' id='caitComment' style='width:150px; height:27px'></textarea>";
				$caitDevHrs = "<input type='text' name='txtDevHoure' id='txtDevHoure' 
				style='width:100px'/>";
				$caitStatus = " <select name='cboCaitStatus' id='cboCaitStatus'>
		  <option value='1'>Pending</option>
		  <option value='2'>Complete</option>
		  </select>";
		  $save 			= "<img  type='submit' border='0' src='../../../images/Tsave.jpg' 
		  alt='Save' name='butSaveCait'width='92' height='24'  class='mouseover' id='butSaveCait' tabindex='24'/>";
					}else if($row['intTrack']=="1"){
						$caitTrack 	= $myoption;
				$comment 	= "<textarea name='caitComment' id='caitComment' style='width:150px; height:27px'></textarea>";
				$caitDevHrs = "<input type='text' name='txtDevHoure' id='txtDevHoure' 
				style='width:100px'/>";
				$caitStatus = " <select name='cboCaitStatus' id='cboCaitStatus'>
		  <option value='1'>Pending</option>
		  <option value='2'>Complete</option>
		  </select>";
		  $save 			= "<img  type='submit' border='0' src='../../../images/Tsave.jpg' 
		  alt='Save' name='butSaveCait'width='92' height='24'  class='mouseover' id='butSaveCait' tabindex='24'/>";
					}
					
			}else{
				if($row['caitStatus']=="1"){
					$caitStatusView = "Pending";
				}else if($row['caitStatus']=="2"){
					$caitStatusView = "Complete";
				}
				$comment = substr($row['caitComment'] ,0, 10);
				$caitTrack  = $caitTrackName['strTrackerName'];
				$caitDevHrs = $row['develpmentHoures'];
				$caitStatus = $caitStatusView ;
				$save 		= $row['dtmCompleteDate'];
			}
			}else{
				if($row['caitStatus']=="1"){
					$caitStatusView = "Pending";
				}else if($row['caitStatus']=="2"){
					$caitStatusView = "Complete";
				}
				$comment = substr($row['caitComment'] ,0, 10);
				$caitTrack  = $caitTrackName['strTrackerName'];
				$caitDevHrs = $row['develpmentHoures'];
				$caitStatus = $caitStatusView ;
				$save 		= $row['dtmCompleteDate'];
				
			}
				
			###############################################################################
			$link['msg'] .= "<tr id=\"".$row['no']."\">
          <td><a href='admin-tickets.php?&ticketId=".$row['no']."'  target='_blank' id='ticketIdNo' name='ticketIdNo'># ".$row['no']."				          </a><input type='hidden' name='intId' id='intId' value='".$row['no']."' style='width:10px' class='tdtd'></td>
		  <td>$track<input type='hidden' name='tNo' id='tNo' value='".$row['no']."'></td>
          <td>".$row['username']."</td>
          <td>".$row['reportdatetime']."</td>
         <td>".$row['strName']."</td>
          <td id='tdProgram'>$row1[0]</td>
          <td><a href='admin-tickets.php?&ticketId=".$row['no']."'  target='_blank' id='ticketIdNo' name='ticketIdNo'>$note1</a></td>
          <td>$priorty</td>
          <td>$approve</td>
		  <td>$caitTrack</td>
		  <td>$caitDevHrs</td>
		  <td>$caitStatus</td>
		  <td>$comment</td>
		   <td>$save</td>
        </tr>";
		}
		return $link;
	}	
}
?>

<?php
class cls_currency_get
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;	
	}
	
	public function GetAllValues($id,$executeType)
	{
		return $this->GetAllValues_sql($id,$executeType);
	}

	private function GetAllValues_sql($id,$executeType)
	{
		$sql = "SELECT 
					FC.strCode			AS CODE,
					FC.strSymbol		AS SYMBOL
				FROM mst_financecurrency FC 
				WHERE 
					FC.intId = $id ";
		$result = $this->db->$executeType($sql);
		return	mysqli_fetch_array($result);
	}
	public function Get_SVAT($executeType)
	{
		$sql = "SELECT Finance_tax.dblRate AS SVAT_Rate FROM mst_financetaxisolated AS Finance_tax WHERE intId = 3";
		$result = $this->db->$executeType($sql);
		$row = mysqli_fetch_array($result);
		return $row['SVAT_Rate'];
	}
}
?>
<?php
/**
 * Created by PhpStorm.
 * User: Hasitha
 * Date: 4/29/2019
 * Time: 5:04 PM
 */

class cls_azureDBconnection
{
    private $configs;

    function connectAzureDB()
    {
        $this->configs = include($_SESSION['ROOT_PATH']. "config/azureDBconnection.php");
        $serverName = $this->configs['SERVER_NAME'];
        $connectionArray = $this->configs['CONNECTION_OPTIONS'];
        return sqlsrv_connect($serverName, $connectionArray);
    }

    function runQuery($conn,$sql)
    {
        $result = sqlsrv_query($conn, $sql);
        if($result == FALSE){
            return false;
        }
        else{
            return $result;

        }
    }

    function fetchResults($getResults){
        if ($getResults == FALSE)
            echo (sqlsrv_errors());
        else {
            return sqlsrv_fetch_array($getResults, SQLSRV_FETCH_ASSOC);
        }
    }

    function freeResourses($getResults){
        return sqlsrv_free_stmt($getResults);
    }

    function getEnvironment(){
        $this->configs = include($_SESSION['ROOT_PATH']. "config/azureDBconnection.php");
        return $this->configs['ENVIRONMENT'];
    }
}

?>
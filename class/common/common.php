<?php
class cls_common
{	
	public function ceil_deci($val,$decimal_places){
		$val	=   $val*pow(10,$decimal_places);	
		$val1	=	floor($val);
		$val2	=	round(($val - $val1),3);
		if($val2>0){
			$val1	+=1;
		}
		
		$fin_val	= $val1/pow(10,$decimal_places);
		
		return $fin_val;
	}	
}
?>
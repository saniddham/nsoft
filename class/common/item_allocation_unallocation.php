<?php
include_once (MAIN_ROOT."class/sessions.php");   					$sessions				= new sessions($db);
include_once (MAIN_ROOT."class/dateTime.php");   					$dateTimes				= new dateTimes($db);
include_once (MAIN_ROOT."class/common/common.php");   				$common					= new cls_common($db);
include_once (MAIN_ROOT."class/common/consumptions.php");   		$consumptions			= new consumptions($db);
include_once (MAIN_ROOT."class/tables/mst_item.php");				$mst_item				= new mst_item($db);
include_once (MAIN_ROOT."class/tables/ware_stocktransactions_color_room_color.php"); 	$ware_stocktransactions_color_room_color 	= new ware_stocktransactions_color_room_color($db);
include_once (MAIN_ROOT."class/tables/ware_stocktransactions_color_room_item.php");		$ware_stocktransactions_color_room_item		= new ware_stocktransactions_color_room_item($db);
include_once (MAIN_ROOT."class/tables/sys_company_item_allocation_percentages.php");	$sys_company_item_allocation_percentages	= new sys_company_item_allocation_percentages($db);
include_once (MAIN_ROOT."class/tables/ware_stocktransactions_bulk.php");				$ware_stocktransactions_bulk				= new ware_stocktransactions_bulk($db);
include_once (MAIN_ROOT."class/tables/ware_stocktransactions.php");	$ware_stocktransactions	= new ware_stocktransactions($db);
include_once (MAIN_ROOT."class/tables/trn_orderdetails.php");		$trn_orderdetails		= new trn_orderdetails($db);
include_once (MAIN_ROOT."class/tables/trn_ordersizeqty.php");		$trn_ordersizeqty		= new trn_ordersizeqty($db);
include_once (MAIN_ROOT."class/tables/ware_grnheader.php");			$ware_grnheader			= new ware_grnheader($db);
include_once (MAIN_ROOT."class/tables/ware_fabricdispatchconfirmation_details.php");						$ware_fabricdispatchconfirmation_details 	= new ware_fabricdispatchconfirmation_details($db);

class item_allocation_unallocation // class name request by shanka approved by hemanthi
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}

	public function allocateUnallocateProcess($orderNo,$orderYear,$salesOrderId,$itemId,$allocateQty,$allocType,$prgType,$docNo,$docYear,$location,$deci)
	{
		/*ALLOCATION
			1.FIRST WE ALLOCATE EXTRA ITEMS TO ORDER WHICH ARE IN COLOR ROOM STOCK.
			2.IF THERE IS NO ENOUGH EXTRA STOCK IN COLOR ROOM, WE ALLOCATE BULK STOCK ITEMS TO ORDER
		*/
		global $mst_item;
		global $trn_orderdetails;
		global $consumptions;
		global $sys_company_item_allocation_percentages;
		global $trn_ordersizeqty;
		global $ware_stocktransactions_bulk;
		global $sessions;
		global $ware_stocktransactions_color_room_item;
		global $ware_stocktransactions;
		global $common;
		global $dateTimes;
		global $ware_fabricdispatchconfirmation_details;		
		$savedStatus		= true;
		$savedMessage		= '' ;
		$error_sql			= '';
			
		$allocatable_qty	= 0;
		$save_qty			= 0;
		
		$allocStockType		= '';
		$unallocStockType	= '';
 	
	$flag=1;
	if($allocType=='UNALLOC' && ($allocateQty==0|| $allocateQty==''))
		$flag=0;
	
	if($flag != 0){	
		
		if($prgType=='COLOR_ROOM')
		{
			$allocStockType		= "ALLOC_C";
			$unallocStockType	= "UNALLOC_C";
		}
		if($prgType=='ORDER')
		{
			$allocStockType		= "ALLOC_O";
			$unallocStockType	= "UNALLOC_O";
		}
		if($prgType=='MANUAL')
		{
			$allocStockType		= "ALLOC_M";
			$unallocStockType	= "UNALLOC_M";
		}
		
		
		$mst_item->set($itemId);
		//GET ALLOCATABLE QTY FOR INK ITEM*************************
		if($mst_item->getITEM_TYPE()==1)
		{
			$allocatable_qty		= $consumptions->getOrderWiseInkItemAllocatableQty($orderNo,$orderYear,$salesOrderId,$itemId,$locationId,$deci);
			
			$allocatedQty			= $this->getInkItemAllocatedQty($orderNo,$orderYear,$salesOrderId,$itemId,$deci);

		}
		//********************************************************
		//GET ALLOCATABLE QTY FOR FOIL/SPECIAL ITEM
		if($mst_item->getITEM_TYPE()==2 || $mst_item->getITEM_TYPE()==3)
		{
			
			$trn_orderdetails->set($orderNo , $orderYear , $salesOrderId);
			$sys_company_item_allocation_percentages->set($sessions->getCompanyId());
			
			$fullAlloc_percn	= $sys_company_item_allocation_percentages->getFULL_ALLOCATION();
 				
			if($mst_item->getITEM_TYPE()==2)
				$extraAlloc_percn		= $sys_company_item_allocation_percentages->getFOIL_EXTRA_ALLOCATION();
			if($mst_item->getITEM_TYPE()==3)
				$extraAlloc_percn		= $sys_company_item_allocation_percentages->getSP_RM_EXTRA_ALLOCATION();
			
			$order_size_result	= $trn_ordersizeqty->getsizeWiseOrderQty($orderNo,$orderYear,$salesOrderId);
				
			$totOrderQty=0;	
			while($row = mysqli_fetch_array($order_size_result))
			{
				$orderQty	= $row['dblQty']+$row['dblQty']*($trn_orderdetails->getdblOverCutPercentage()+$trn_orderdetails->getdblToleratePercentage())/100;
				$orderQty	+=$ware_fabricdispatchconfirmation_details->loadDamageReturnedQty_confirmed($orderNo,$orderYear,$salesOrderId);
				$orderQty			= $common->ceil_deci($orderQty,$deci);
				$totOrderQty		+=$orderQty;	
 		
				if($mst_item->getITEM_TYPE()==2)
				{
					$foilconPc			= $consumptions->foil_item_cosumption($orderNo,$orderYear,$salesOrderId,$itemId,$row['strSize']);
					$allocatable_qty   += $foilconPc*$orderQty*(($fullAlloc_percn+$extraAlloc_percn)/100);
				}
				if($mst_item->getITEM_TYPE()==3)
				{
					$spconPc			= $consumptions->special_item_cosumption($orderNo,$orderYear,$salesOrderId,$itemId,$row['strSize']);
					$allocatable_qty   += $spconPc*$orderQty*(($fullAlloc_percn+$extraAlloc_percn)/100);
				}	
					 
			}
 			
			$actualPCS	=$consumptions->color_getActualProductionPCS($orderNo,$orderYear,$salesOrderId);
			
			//$bal_pcs_before means 'balance to production qty' of already raised orders before config the color room process
			$bal_pcs_before	=$trn_orderdetails->getBAL_PRODUCTION_BEFORE_CR_PROCESS();
			if($bal_pcs_before>0)
			$totOrderQty	=$bal_pcs_before;
			
			//if($itemId==35)	
			//echo '('.($totOrderQty-$actualPCS).'/'.$totOrderQty.')*'.$allocatable_qty;
		
			$allocatable_qty= (($totOrderQty-$actualPCS)/$totOrderQty)*$allocatable_qty;
			
			$allocatedQty			= $this->getFoilSpAllocatedQty($orderNo,$orderYear,$salesOrderId,$itemId,$deci);
  			
			$allocatable_qty		= $allocatable_qty-$allocatedQty;
			$allocatable_qty	 	=  $common->ceil_deci($allocatable_qty,$deci);	
			
		}
 		//************************************************************
		
		if($allocateQty=='')//IF NO QTY ENTERED
			$allocateQty = $allocatable_qty;//(THIS MAY +/-)
			
			
			
		//CHECKS BALANCE TO ALLOCATE QTY AND QTY WHICH IS GOING TO ALLOCATE
		if($allocateQty>0)//ALLOCATION VALIDATION
		{
			if($allocateQty>$allocatable_qty && $savedStatus)
			{
				if($prgType=='MANUAL'){
					$savedStatus	= false;
					$savedMessage	= "Cannot allocate.<br>Allocatable qty : ".$allocatable_qty;
				}
				$allocatable_qty = $allocatable_qty;
			}
			else
			{
				$allocatable_qty = $allocateQty;
			}
		}
		else if($allocateQty==0)
		{
				$allocatable_qty = $allocatable_qty;
		}
		else if($allocateQty<0)//UN ALLOCATION VALIDATION
		{
			$styleStockBal		= $ware_stocktransactions->getMainStoresOrderWiseItemBalance($location,$orderNo,$orderYear,$salesOrderId,$itemId,$deci);
			$colorOrderStockBal = $ware_stocktransactions_color_room_item->getColorRoomOrderWiseItemBalance($location,$orderNo,$orderYear,$salesOrderId,$itemId,$deci);
			$styleStockBal		= $common->ceil_deci($styleStockBal,$deci);
			$colorOrderStockBal	= $common->ceil_deci($colorOrderStockBal,$deci);
			
			if($allocateQty*-1 > ($styleStockBal+$colorOrderStockBal) && $savedStatus)
			{
				if($prgType=='MANUAL'){
					$savedStatus	= false;
					$savedMessage	= "Cannot allocate.<br>Allocatable qty : ".($styleStockBal+$colorOrderStockBal);
				}
				$allocatable_qty = ($styleStockBal+$colorOrderStockBal)*-1;
			}
			else
			{
				$allocatable_qty = $allocateQty;
			}
		}
		
		
		//************************************************************
			
		$bulkStockBal		= $ware_stocktransactions_bulk->getMainStoresItemBalance($location,$itemId,$deci);
		$colorExtraStockBal	= $ware_stocktransactions_color_room_item->getItemWiseExtraStockBalance($itemId,$location,$deci);
		$bulkStockBal		= $common->ceil_deci($bulkStockBal,$deci);
		$colorExtraStockBal	= $common->ceil_deci($colorExtraStockBal,$deci);


		//ALLOCATION
		if($allocatable_qty>0)
		{
			
			//HERE WILL BE ALLOCATE ONLY STOCK EXISTING QTYS(MINIMUM FROM '$allocatable_qty','$colorExtraStockBal'
			
			if($mst_item->getITEM_TYPE()==1)//INK
				$save_qty			= ($allocatable_qty<($bulkStockBal+$colorExtraStockBal)?$allocatable_qty:($bulkStockBal+$colorExtraStockBal));	
			else//FOIL/SPECIAL RM
				$save_qty			= ($allocatable_qty<($bulkStockBal)?$allocatable_qty:($bulkStockBal));	
			//COLOR ROOM.

			if($save_qty>0)
			{
				if($mst_item->getITEM_TYPE()==1)
				{
					$CRQty		= $colorExtraStockBal;
					$CRQty	= ($CRQty<($save_qty)?$CRQty:$save_qty);	
					$savedQty	= 0;
					$totSaved_c	= 0;
					$resultCG	= $ware_stocktransactions_color_room_item->getColorRoomGrnWiseExtraItemBalance_result($location,$itemId,$deci);

					while($rowCG = mysqli_fetch_array($resultCG))
					{
						if($CRQty>0 && $rowCG['stockBal']>0)
						{
							if($CRQty <= $rowCG['stockBal'])
							{
								$savedQty	= $CRQty;
								$CRQty	= 0;	
							}
							if($CRQty > $rowCG['stockBal'])
							{
								$savedQty	= $rowCG['stockBal'];
								$CRQty		= $CRQty-$savedQty;
							}
							
							$grnNo		= $rowCG['intGRNNo'];
							$grnYear	= $rowCG['intGRNYear'];
							$grnDate	= $rowCG['dtGRNDate'];
							$grnRate	= $rowCG['dblGRNRate'];	
							$currency	= $this->loadCurrency($grnNo,$grnYear,$itemId);
							$savedQty	= $common->ceil_deci($savedQty,$deci);
							$totSaved_c	+= $savedQty;
							
							if($savedQty>0)
							{
								$result_arr	= $ware_stocktransactions_color_room_item->insertRec(0,$sessions->getCompanyId(),$location,'NULL',$docNo,$docYear,$grnNo,$grnYear,$grnDate,$currency,$grnRate,$itemId,$savedQty*-1,$allocStockType,'EXTRA',$sessions->getUserId(),$dateTimes->getCurruntDateTime(),$orderNo,$orderYear,$salesOrderId,'NULL','NULL','NULL');
								if(!$result_arr['status'] && $savedStatus)
								{
									$savedStatus		= false;
									$savedMessage		= $result_arr['msg'];
									$error_sql			= $result_arr['sql'];
								}
								$extraStockBal	= $ware_stocktransactions_color_room_item->getItemWiseExtraStockBalance($itemId,$location,$deci);
								if($extraStockBal<0 && $savedStatus)
								{
									$savedStatus		= false;
									$savedMessage		= "No stock balance to allocate the Item.";
								}
								
								$result_arr	= $ware_stocktransactions_color_room_item->insertRec(0,$sessions->getCompanyId(),$location,'NULL',$docNo,$docYear,$grnNo,$grnYear,$grnDate,$currency,$grnRate,$itemId,$savedQty,$allocStockType,'ORDER',$sessions->getUserId(),$dateTimes->getCurruntDateTime(),$orderNo,$orderYear,$salesOrderId,'NULL','NULL','NULL');
								if(!$result_arr['status'] && $savedStatus)
								{
									$savedStatus		= false;
									$savedMessage		= $result_arr['msg'];
									$error_sql			= $result_arr['sql'];
								}
								
								$orderStockBal	= $ware_stocktransactions_color_room_item->getColorRoomOrderWiseItemBalance($location,$orderNo,$orderYear,$salesOrderId,$itemId,$deci);
								if($orderStockBal<0 && $savedStatus)
								{
									$savedStatus		= false;
									$savedMessage		= "No stock balance to allocate the Item.";
								}
							}
						}
					}
				} // END OF ITEM TYPE =1
			}
			
			//MAIN STOCK
			//HERE WILL BE SAVE MINIMUM FROM ($balToSave,$bulkStockBal)
			// AND HERE WILL BE ALLOCATE FROM BULK STOCK.
			if($mst_item->getITEM_TYPE()==1)//INK
				$balToSave  = ($save_qty-$totSaved_c);
			else//FOIL/SPECIAL RM
				$balToSave  = ($save_qty);
				
			$CRQty		= ($bulkStockBal<$balToSave?$bulkStockBal:$balToSave);
  		
			if($CRQty>0){//IF THERE AVAILABE BALANCE TO UN ALLOCATE QTY
				$savedQty	= 0;
				$resultBG	= $ware_stocktransactions_bulk->getMainStoresGrnWiseItemBalance_result($location,$itemId,$deci);
				while($rowBG = mysqli_fetch_array($resultBG))
				{
						
					if($CRQty>0 && $rowBG['stockBal'])
					{
						if($CRQty <= $rowBG['stockBal'])
						{
							$savedQty	= $CRQty;
							$CRQty		= 0;	
						}
						if($CRQty > $rowBG['stockBal'])
						{
							$savedQty	= $rowBG['stockBal'];
							$CRQty		= $CRQty-$savedQty;
						}
	
						$grnNo		= $rowBG['intGRNNo'];
						$grnYear	= $rowBG['intGRNYear'];
						$grnDate	= $rowBG['dtGRNDate'];
						$grnRate	= $rowBG['dblGRNRate'];	
						$currency	= $this->loadCurrency($grnNo,$grnYear,$itemId);
						$savedQty	= $common->ceil_deci($savedQty,$deci);
						
						if($savedQty>0)
						{

							$result_arr	= $ware_stocktransactions_bulk->insertRec($sessions->getCompanyId(),$location,'NULL',$docNo,$docYear,$grnNo,$grnYear,$grnDate,$currency,$grnRate,$itemId,$savedQty*-1,$allocStockType,$orderNo,$orderYear,$salesOrderId,$sessions->getUserId(),$dateTimes->getCurruntDateTime(),'NULL');
							if(!$result_arr['status'] && $savedStatus)
							{
								$savedStatus		= false;
								$savedMessage		= $result_arr['msg'];
								$error_sql			= $result_arr['sql'];
							}
							
							$bulkStockBal	= $ware_stocktransactions_bulk->getMainStoresItemBalance($location,$itemId,$deci);
							if($bulkStockBal<0 && $savedStatus)
							{
								$savedStatus		= false;
								$savedMessage		= "No stock balance to allocate the Item.";
							}
	
							$result_arr	= $ware_stocktransactions->insertRec($sessions->getCompanyId(),$location,'NULL',$docNo,$docYear,$grnNo,$grnYear,$grnDate,$currency,$grnRate,$orderNo,$orderYear,$salesOrderId,'NULL',$itemId,$savedQty,$allocStockType,$sessions->getUserId(),$dateTimes->getCurruntDateTime(),'NULL');
							if(!$result_arr['status'] && $savedStatus)
							{
								$savedStatus		= false;
								$savedMessage		= $result_arr['msg'];
								$error_sql			= $result_arr['sql'];
							}
							
							$styleStockBal	= $ware_stocktransactions->getMainStoresOrderWiseItemBalance($location,$orderNo,$orderYear,$salesOrderId,$itemId,$deci);
							if($styleStockBal<0 && $savedStatus)
							{
								$savedStatus		= false;
								$savedMessage		= "No stock balance to allocate the Item.";
							}
						}
					}
				}
			}
			
		}
		//END OF ALLOCATION
			
	
		//UN ALLOCATION
		
		if($allocatable_qty<0) 
		{

			$allocatable_qty     = $allocatable_qty*-1;
			$styleStockBal		= $ware_stocktransactions->getMainStoresOrderWiseItemBalance($location,$orderNo,$orderYear,$salesOrderId,$itemId,$deci);
			$colorOrderStockBal = $ware_stocktransactions_color_room_item->getColorRoomOrderWiseItemBalance($location,$orderNo,$orderYear,$salesOrderId,$itemId,$deci);
			$styleStockBal		= $common->ceil_deci($styleStockBal,$deci);
			$colorOrderStockBal	= $common->ceil_deci($colorOrderStockBal,$deci);
			
			if($mst_item->getITEM_TYPE()==1)//INK
			$save_qty			= ($allocatable_qty<($styleStockBal+$colorOrderStockBal)?$allocatable_qty:($styleStockBal+$colorOrderStockBal));
			else
			$save_qty			= ($allocatable_qty<($styleStockBal)?$allocatable_qty:($styleStockBal));
			
			//COLOR ROOM
			if($save_qty>0)
			{
				if($mst_item->getITEM_TYPE()==1)
				{
					$CRQty		= $save_qty;
					$savedQty	= 0;
					$resultCG	= $ware_stocktransactions_color_room_item->getColorRoomOrderAndGrnWiseItemBalance_result($location,$orderNo,$orderYear,$salesOrderId,$itemId,$deci);
						while($rowCG = mysqli_fetch_array($resultCG))
						{
							if($CRQty>0 && $rowCG['stockBal']>0)
							{
								if($CRQty <= $rowCG['stockBal'])
								{
									$savedQty	= $CRQty;
									$CRQty		= 0;	
								}
								if($CRQty > $rowCG['stockBal'])
								{
									$savedQty	= $rowCG['stockBal'];
									$CRQty		= $CRQty-$savedQty;
								}
								
								$grnNo		= $rowCG['intGRNNo'];
								$grnYear	= $rowCG['intGRNYear'];
								$grnDate	= $rowCG['dtGRNDate'];
								$grnRate	= $rowCG['dblGRNRate'];	
								$currency	= $this->loadCurrency($grnNo,$grnYear,$itemId);
								$savedQty	= $common->ceil_deci($savedQty,$deci);
								
								if($savedQty>0)
								{
									$result_arr	= $ware_stocktransactions_color_room_item->insertRec(0,$sessions->getCompanyId(),$location,'NULL',$docNo,$docYear,$grnNo,$grnYear,$grnDate,$currency,$grnRate,$itemId,$savedQty,$unallocStockType,'EXTRA',$sessions->getUserId(),$dateTimes->getCurruntDateTime(),$orderNo,$orderYear,$salesOrderId,'NULL','NULL','NULL');
									if(!$result_arr['status'] && $savedStatus)
									{
										$savedStatus		= false;
										$savedMessage		= $result_arr['msg'];
										$error_sql			= $result_arr['sql'];
									}
									
									$extraStockBal	= $ware_stocktransactions_color_room_item->getItemWiseExtraStockBalance($itemId,$location,$deci);
									if($extraStockBal<0 && $savedStatus)
									{
										$savedStatus		= false;
										$savedMessage		= "No stock balance to allocate the Item.";
									}
									
									$result_arr	= $ware_stocktransactions_color_room_item->insertRec(0,$sessions->getCompanyId(),$location,'NULL',$docNo,$docYear,$grnNo,$grnYear,$grnDate,$currency,$grnRate,$itemId,$savedQty*-1,$unallocStockType,'ORDER',$sessions->getUserId(),$dateTimes->getCurruntDateTime(),$orderNo,$orderYear,$salesOrderId,'NULL','NULL','NULL');
									if(!$result_arr['status'] && $savedStatus)
									{
										$savedStatus		= false;
										$savedMessage		= $result_arr['msg'];
										$error_sql			= $result_arr['sql'];
									}
									
									$orderStockBal	= $ware_stocktransactions_color_room_item->getColorRoomOrderWiseItemBalance($location,$orderNo,$orderYear,$salesOrderId,$itemId,$deci);
									if($extraStockBal<0 && $savedStatus)
									{
										$savedStatus		= false;
										$savedMessage		= "No stock balance to allocate the Item.";
									}
								}
							}
						}
				}
			}
	
			//MAIN STOCK
			//HERE WILL BE SAVE MINIMUM FROM ($balToSave,styleStockBal)
			// AND HERE WILL BE UN ALLOCATE FROM STYLE STOCK.
			if($mst_item->getITEM_TYPE()==1)//INK
				$balToSave  = ($save_qty-$colorOrderStockBal);
			else//FOIL/SPECIAL RM
				$balToSave  = ($save_qty);
				
 				$CRQty		= ($bulkStockBal<$balToSave?$bulkStockBal:$balToSave);
		
				$savedQty	= 0;
				$resultSG	= $ware_stocktransactions->getGrnWiseStyleStockBal_result($orderNo,$orderYear,$salesOrderId,$itemId,$location,$deci);
				while($rowSG = mysqli_fetch_array($resultSG))
				{
					if($CRQty>0 && $rowSG['stockBal'])
					{
						if($CRQty <= $rowSG['stockBal'])
						{
							$savedQty	= $CRQty;
							$CRQty	= 0;	
						}
						if($CRQty > $rowSG['stockBal'])
						{
							$savedQty	= $rowSG['stockBal'];
							$CRQty		= $CRQty-$savedQty;
						}
						
						$grnNo		= $rowSG['intGRNNo'];
						$grnYear	= $rowSG['intGRNYear'];
						$grnDate	= $rowSG['dtGRNDate'];
						$grnRate	= $rowSG['dblGRNRate'];	
						$currency	= $this->loadCurrency($grnNo,$grnYear,$itemId);
						$savedQty	= $common->ceil_deci($savedQty,$deci);
						
						if($savedQty>0)
						{
							$result_arr	= $ware_stocktransactions_bulk->insertRec($sessions->getCompanyId(),$location,'NULL',$docNo,$docYear,$grnNo,$grnYear,$grnDate,$currency,$grnRate,$itemId,$savedQty,$unallocStockType,$orderNo,$orderYear,$salesOrderId,$sessions->getUserId(),$dateTimes->getCurruntDateTime(),'NULL');
							if(!$result_arr['status'] && $savedStatus)
							{
								$savedStatus		= false;
								$savedMessage		= $result_arr['msg'];
								$error_sql			= $result_arr['sql'];
							}
							
							$bulkStockBal	= $ware_stocktransactions_bulk->getMainStoresItemBalance($location,$itemId,$deci);
							if($bulkStockBal<0 && $savedStatus)
							{
								$savedStatus		= false;
								$savedMessage		= "No stock balance to allocate the Item.";
							}

							$result_arr	= $ware_stocktransactions->insertRec($sessions->getCompanyId(),$location,'NULL',$docNo,$docYear,$grnNo,$grnYear,$grnDate,$currency,$grnRate,$orderNo,$orderYear,$salesOrderId,'NULL',$itemId,$savedQty*-1,$unallocStockType,$sessions->getUserId(),$dateTimes->getCurruntDateTime(),'NULL');
							if(!$result_arr['status'] && $savedStatus)
							{
								$savedStatus		= false;
								$savedMessage		= $result_arr['msg'];
								$error_sql			= $result_arr['sql'];
							}
							
							$styleStockBal	= $ware_stocktransactions->getMainStoresOrderWiseItemBalance($location,$orderNo,$orderYear,$salesOrderId,$itemId,$deci);
							if($styleStockBal<0 && $savedStatus)
							{
								$savedStatus		= false;
								$savedMessage		= "No stock balance to allocate the Item.";
							}
						}
					}
				}
		
		
			
		}
	}
				
				
		 
		if(!$savedStatus)	
		{
			$data['type']	= 'fail';
			$data['msg']	= $savedMessage;
			$data['sql']	= $error_sql;
		}

		return $data;
	}
	public function getWastedColorWeight($orderNo,$orderYear,$salesOrderId,$deci)
	{
		global $ware_stocktransactions_color_room_color;
		
		$cols	= " COALESCE(ROUND(SUM(dblQty),$deci+2),0) as wasted_weight ";
		
		$where	= " intOrderNo = '".$orderNo."' AND
					intOrderYear = '".$orderYear."' AND
					intSalesOrderId = '".$salesOrderId."' AND
					strType = 'COLOR_WASTAGE' ";
		
		$result	= $ware_stocktransactions_color_room_color->select($cols,$join=null,$where,$order=null,$limit=null);
		$row	= mysqli_fetch_array($result);
		
		return $row['wasted_weight'];
	}
	public function getActualItemPropotion($orderNo,$orderYear,$salesOrderId,$itemId,$deci)
	{
		global $ware_stocktransactions_color_room_item;
		
		$sql = "SELECT COALESCE(ROUND(actPrpItemWise/actPrpOrderWise,$deci+2),0) AS actual_item_propotion
				FROM
				(
				SELECT
				(SELECT COALESCE(SUM(dblQty*-1),0)
				FROM ware_stocktransactions_color_room_item
				WHERE
				intOrderNo = '".$orderNo."' AND
				intOrderYear = '".$orderYear."' AND
				intSalesOrderId = '".$salesOrderId."' AND
				intItemId = '".$itemId."' AND
				strType = 'COLOR_CREATE') AS actPrpItemWise,
				
				(SELECT COALESCE(SUM(dblQty*-1),0)
				FROM ware_stocktransactions_color_room_item
				WHERE
				intOrderNo = '".$orderNo."' AND
				intOrderYear = '".$orderYear."' AND
				intSalesOrderId = '".$salesOrderId."' AND
				strType = 'COLOR_CREATE') AS actPrpOrderWise
				) AS tb1 ";
		
		$result	= $this->db->RunQuery($sql);
		
		return $result[0]['actual_item_propotion'];
		
	}
	public function getStyleStockBal($orderNo,$orderYear,$salesOrderId,$itemId,$type,$deci)
	{
		global $ware_stocktransactions;
		
		if($type=='UNALLOCATE')
			$cols	= " COALESCE(ROUND(SUM(dblQty*-1),$deci+2),0) AS stock_bal ";
		
		if($type=='ALLOCATE')
			$cols	= " COALESCE(ROUND(SUM(dblQty),$deci+2),0) AS stock_bal ";
		
		if($type=='C_ROOM_ISSUE')
			$cols	= " COALESCE(ROUND(SUM(dblQty*-1),$deci+2),0) AS stock_bal ";
		
		$where	= " intOrderNo = '".$orderNo."'
					AND intOrderYear = '".$orderYear."'
					AND intSalesOrderId = '".$salesOrderId."'
					AND intItemId = '".$itemId."' ";
		
		if($type=='ALLOCATE')
			$where	.= " AND strType IN ('ALLOC_M','ALLOC_O','ALLOC_C') ";
		
		if($type=='C_ROOM_ISSUE')
			$where	.= " AND strType IN ('C_ROOM_ISSUE') ";
		
		if($type=='UNALLOCATE')
			$where	.= " AND strType IN ('UNALLOC_M','UNALLOC_O','UNALLOC_C') ";
					
		
		$result	= $ware_stocktransactions->select($cols,$join=null,$where,$order=null,$limit=null);
		$row	= mysqli_fetch_array($result);
		
		return $row['stock_bal'];	
	}
	public function colorRoomItemStockBal($orderNo,$orderYear,$salesOrderId,$itemId,$type,$deci)
	{
		global $ware_stocktransactions_color_room_item;
		
		if($type=='EXTRA')
			$cols	= " COALESCE(ROUND(SUM(dblQty),$deci+2),0) AS stock_bal ";
		
		if($type=='ALLOCATE')
			$cols	= " COALESCE(ROUND(SUM(dblQty),$deci+2),0) AS stock_bal ";
		
		if($type=='C_ROOM_ISSUE')
			$cols	= " COALESCE(ROUND(SUM(dblQty),$deci+2),0) AS stock_bal ";
	
		if($type=='UNALLOCATE')
			$cols	= " COALESCE(ROUND(SUM(dblQty*-1),$deci+2),0) AS stock_bal ";
		
	
		$where	= " intOrderNo = '".$orderNo."' 
					AND intOrderYear = '".$orderYear."' 
					AND intSalesOrderId = '".$salesOrderId."' 
					AND intItemId = '".$itemId."' ";
		
		if($type=='EXTRA')
			$where	.= " AND strType = 'EX_ORDER_ALLOC' AND STOCK_CATEGORY = 'ORDER' ";
		
		if($type=='ALLOCATE')
			$where	.= " AND strType IN ('ALLOC_M','ALLOC_O','ALLOC_C') AND STOCK_CATEGORY = 'ORDER' ";
		
		if($type=='UNALLOCATE')
			$where	.= " AND strType IN ('UNALLOC_M','UNALLOC_O','UNALLOC_C') AND STOCK_CATEGORY = 'ORDER' ";
		
		if($type=='C_ROOM_ISSUE')
			$where	.= " AND strType IN ('C_ROOM_ISSUE') ";
	
		$result	= $ware_stocktransactions_color_room_item->select($cols,$join=null,$where,$order=null,$limit=null);
		$row	= mysqli_fetch_array($result);
		
		return $row['stock_bal'];
	}
	public function loadCurrency($grnNo,$grnYear,$item)
	{
		global $db;
		global $ware_grnheader;
		global $mst_item;
		
		if(($grnNo!=0) && ($grnYear!=0))
		{
			$cols	= " trn_poheader.intCurrency as currency ";
			
			$join	= " INNER JOIN trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND 
						ware_grnheader.intPoYear = trn_poheader.intPOYear ";
			
			$where	= " ware_grnheader.intGrnNo = '".$grnNo."' AND
						ware_grnheader.intGrnYear = '".$grnYear."' ";
			
			$result	= $ware_grnheader->select($cols,$join,$where,$order=null,$limit=null);
			$row	= mysqli_fetch_array($result);
			
			return $row['currency'];
			 
		}
		else
		{
			$mst_item->set($item);
			return $mst_item->getintCurrency();
		}
	}
	public function getInkItemAllocatedQty($orderNo,$orderYear,$salesOrderId,$itemId,$deci)
	{
		global $ware_stocktransactions_color_room_item;
		
		$stylStockAllocQty		= $this->getStyleStockBal($orderNo,$orderYear,$salesOrderId,$itemId,'ALLOCATE',$deci);
		$stylStockUNAllocQty	= $this->getStyleStockBal($orderNo,$orderYear,$salesOrderId,$itemId,'UNALLOCATE',$deci);
		$stylStockCRoomIssue	= $this->getStyleStockBal($orderNo,$orderYear,$salesOrderId,$itemId,'C_ROOM_ISSUE',$deci);
		$colorRoomEXStockQty	= $this->colorRoomItemStockBal($orderNo,$orderYear,$salesOrderId,$itemId,'EXTRA',$deci);
		$colorRoomAStockQty		= $this->colorRoomItemStockBal($orderNo,$orderYear,$salesOrderId,$itemId,'ALLOCATE',$deci);
		$colorRoomUAStockQty	= $this->colorRoomItemStockBal($orderNo,$orderYear,$salesOrderId,$itemId,'UNALLOCATE',$deci);
		$colorRoomCRoomIssue	= $this->colorRoomItemStockBal($orderNo,$orderYear,$salesOrderId,$itemId,'C_ROOM_ISSUE',$deci);
		
		
	
		$wastedQty				= $ware_stocktransactions_color_room_item->getInkItemWastage_noneProcessed($orderNo,$orderYear,$salesOrderId,$itemId,$deci);
		//$actItmPropotion		= $this->getActualItemPropotion($orderNo,$orderYear,$salesOrderId,$itemId,$deci);
		
		$coloRoomWastgQty		= $wastedQty;
		
		//2014-10-10
		//$allocatedQty			= $stylStockAllocQty-$stylStockUNAllocQty+$colorRoomEXStockQty-$colorRoomUAStockQty+$colorRoomAStockQty+$coloRoomWastgQty;//calculation responsible by hemanthi
		$allocatedQty			= $stylStockAllocQty-$stylStockUNAllocQty+$colorRoomEXStockQty-$colorRoomUAStockQty+$colorRoomAStockQty-$coloRoomWastgQty;//calculation responsible by hemanthi
		//if($itemId==723)
		//echo  $stylStockAllocQty.'-'.$stylStockUNAllocQty.'+'.$colorRoomEXStockQty.'-'.$colorRoomUAStockQty.'+'.$colorRoomAStockQty.'-'.$coloRoomWastgQty;
		return $allocatedQty;
	}
	public function getFoilSpAllocatedQty($orderNo,$orderYear,$salesOrderId,$itemId,$deci)
	{
		$stylStockAllocQty		= $this->getStyleStockBal($orderNo,$orderYear,$salesOrderId,$itemId,'ALLOCATE',$deci);
		$stylStockUNAllocQty	= $this->getStyleStockBal($orderNo,$orderYear,$salesOrderId,$itemId,'UNALLOCATE',$deci);
		
		$allocatedQty			= $stylStockAllocQty-$stylStockUNAllocQty;//calculation responsible by hemanthi
		
		return $allocatedQty;
	}
}

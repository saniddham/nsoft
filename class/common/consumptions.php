<?php
include_once (MAIN_ROOT."class/sessions.php");   										$sessions									= new sessions($db);
include_once (MAIN_ROOT."class/common/common.php");   									$common										= new cls_common($db);
include_once (MAIN_ROOT."class/common/item_allocation_unallocation.php");   			$item_allocation_unallocation				= new item_allocation_unallocation($db);
include_once (MAIN_ROOT."class/tables/ink_day_end_process_color_room_pcs.php");   		$ink_day_end_process_color_room_pcs			= new ink_day_end_process_color_room_pcs($db);
include_once (MAIN_ROOT."class/tables/ink_day_end_process_color_room_weight.php"); 		$ink_day_end_process_color_room_weight		= new ink_day_end_process_color_room_weight($db);
include_once (MAIN_ROOT."class/tables/trn_sample_color_recipes.php");					$trn_sample_color_recipes					= new trn_sample_color_recipes($db);
include_once (MAIN_ROOT."class/tables/trn_orderdetails.php");							$trn_orderdetails							= new trn_orderdetails($db);
include_once (MAIN_ROOT."class/tables/trn_ordersizeqty.php");							$trn_ordersizeqty							= new trn_ordersizeqty($db);
include_once (MAIN_ROOT."class/tables/trn_sampleinfomations_details.php");				$trn_sampleinfomations_details				= new trn_sampleinfomations_details($db);
include_once (MAIN_ROOT."class/tables/sys_company_item_allocation_percentages.php");	$sys_company_item_allocation_percentages	= new sys_company_item_allocation_percentages($db);
include_once (MAIN_ROOT."class/tables/mst_item.php");									$mst_item 									= new mst_item($db);
include_once (MAIN_ROOT."class/tables/mst_units.php");									$mst_units 									= new mst_units($db);
include_once (MAIN_ROOT."class/tables/ink_color_room_issue_details.php");				$ink_color_room_issue_details 				= new ink_color_room_issue_details($db);
include_once (MAIN_ROOT."class/tables/ink_color_room_requisition_details.php");			$ink_color_room_requisition_details 		= new ink_color_room_requisition_details($db);
include_once (MAIN_ROOT."class/tables/ware_stocktransactions_color_room_item.php");		$ware_stocktransactions_color_room_item 	= new ware_stocktransactions_color_room_item($db);
include_once (MAIN_ROOT."class/tables/ware_stocktransactions.php");						$ware_stocktransactions 					= new ware_stocktransactions($db);
include_once (MAIN_ROOT."class/tables/ink_color_room_issue_to_production_header.php");						$ink_color_room_issue_to_production_header 	= new ink_color_room_issue_to_production_header($db);
include_once (MAIN_ROOT."class/tables/ware_fabricdispatchconfirmation_details.php");						$ware_fabricdispatchconfirmation_details 	= new ware_fabricdispatchconfirmation_details($db);



class consumptions  
{
	
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	
	/**************************************************************************
			INK 
	**************************************************************************/
	
	//IF ACTUAL CUNSUMPTIONS EXISTS, RETURNS ACTUAL CONS.ELSE RETURNS SAMPLE CONS FOR SALES ORDER USED ITEM
	public function ink_item_cosumption($orderNo,$orderYear,$salesOrder,$item){
		
		if(!$this->ink_item_actualCosumption($orderNo,$orderYear,$salesOrder,$item))
		$cons	= $this->ink_item_sampleCosumption($orderNo,$orderYear,$salesOrder,$item);
		else
		$cons	= $this->ink_item_actualCosumption($orderNo,$orderYear,$salesOrder,$item);
		
		return $cons;
	}
	
	public function ink_item_cosumption_color_wise($orderNo,$orderYear,$salesOrder,$color,$tehnique,$inkType,$item){
		
		if(!$this->ink_item_actualCosumption_color_wise($orderNo,$orderYear,$salesOrder,$color,$tehnique,$inkType,$item))
		$cons	= $this->ink_item_sampleCosumption_color_wise($orderNo,$orderYear,$salesOrder,$color,$tehnique,$inkType,$item);
		else
		$cons	= $this->ink_item_actualCosumption_color_wise($orderNo,$orderYear,$salesOrder,$color,$tehnique,$inkType,$item);
		
		return $cons;
	}
	
	//RETURNS SAMPLE CONSUMPTION OF SALES ORDER USED ITEM
	public function ink_item_sampleCosumption($orderNo,$orderYear,$salesOrder,$item){
		global $trn_orderdetails;
		$trn_orderdetails->set($orderNo,$orderYear,$salesOrder);
		$sampCons	= $this->ink_item_sampleCosumptionForSampleNo($trn_orderdetails->getintSampleNo(),$trn_orderdetails->getintSampleYear(),$trn_orderdetails->getintRevisionNo(),$trn_orderdetails->getstrCombo(),$trn_orderdetails->getstrPrintName(),$trn_orderdetails->getintPart(),$item);
 		return $sampCons;	
	}
	
	public function ink_item_sampleCosumption_color_wise($orderNo,$orderYear,$salesOrder,$color,$tehnique,$inkType,$item){
		global $trn_orderdetails;
		$trn_orderdetails->set($orderNo,$orderYear,$salesOrder);
		$sampCons	= $this->ink_item_sampleCosumptionForSampleNo_color_wise($trn_orderdetails->getintSampleNo(),$trn_orderdetails->getintSampleYear(),$trn_orderdetails->getintRevisionNo(),$trn_orderdetails->getstrCombo(),$trn_orderdetails->getstrPrintName(),$trn_orderdetails->getintPart(),$color,$tehnique,$inkType,$item);
 		return $sampCons;	
	}

	//RETURNS ACTUAL CONSUMPTION OF SALES ORDER USED ITEM
	public function ink_item_actualCosumption($orderNo,$orderYear,$salesOrder,$item){
		$actualCons	= 0;
		if($this->ink_item_getActualProductionPCS($orderNo,$orderYear,$salesOrder,$item) > 0)
			$actualCons	= $this->ink_item_getUsedWeight($orderNo,$orderYear,$salesOrder,$item)/$this->ink_item_getActualProductionPCS($orderNo,$orderYear,$salesOrder,$item);
		return $actualCons;	
	}
	
	public function ink_item_actualCosumption_day_wise($orderNo,$orderYear,$salesOrder,$item,$date){
		$actualCons	= 0;
		if($this->ink_item_getActualProductionPCS_day_wise($orderNo,$orderYear,$salesOrder,$item,$date) > 0)
			$actualCons	= $this->ink_item_getUsedWeight_day_wise($orderNo,$orderYear,$salesOrder,$item,$date)/$this->ink_item_getActualProductionPCS_day_wise($orderNo,$orderYear,$salesOrder,$item,$date);
		return $actualCons;	
	}
	
	public function ink_item_actualCosumption_color_wise($orderNo,$orderYear,$salesOrder,$color,$tehnique,$inkType,$item){
		$actualCons	= 0;
		if($this->ink_item_getActualProductionPCS($orderNo,$orderYear,$salesOrder,$item) > 0)
			$actualCons	= $this->ink_item_getUsedWeight_color_wise($orderNo,$orderYear,$salesOrder,$color,$tehnique,$inkType,$item)/$this->ink_item_getActualProductionPCS($orderNo,$orderYear,$salesOrder,$item);
		return $actualCons;	
	}
	
	
	//RETURNS USED ITEM'S WEIGHT IN ACTUAL PRODUCTION OF SALES ORDER
	public function ink_item_getUsedWeight($orderNo,$orderYear,$salesOrder,$item)
	{		
		global $ink_day_end_process_color_room_weight;
		
		$weight	= 0;
		
		$cols	= "COALESCE(ROUND(SUM(INK_RM_ITEM_WEIGHT/dblNoOfPcs),9),0) AS WEIGHT";
		$join	= "
					Inner Join mst_item ON mst_item.intId = ink_day_end_process_color_room_weight.INK_RM_ITEM 
					Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
				  ";
		$where	= "ORDER_NO='$orderNo' AND ORDER_YEAR='$orderYear' AND SALES_ORDER_ID='$salesOrder' AND INK_RM_ITEM='$item'";
		$order	= NULL;
		$limit	= NULL;

		$result	= $ink_day_end_process_color_room_weight->select($cols,$join,$where,$order,$limit);
		$row 	= mysqli_fetch_array($result);
		$weight	= $row['WEIGHT']==''?0:$row['WEIGHT'];
		return $weight;
	}
	
	public function ink_item_getUsedWeight_day_wise($orderNo,$orderYear,$salesOrder,$item,$date)
	{		
		global $ink_day_end_process_color_room_weight;
		
		$weight	= 0;
		
		$cols	= "COALESCE(SUM(INK_RM_ITEM_WEIGHT/dblNoOfPcs),0) AS WEIGHT";
		
		$join	= " Inner Join ink_day_end_process_color_room_header ON ink_day_end_process_color_room_header.PROCESS_ID = ink_day_end_process_color_room_weight.PROCESS_ID 
					Inner Join mst_item ON mst_item.intId = ink_day_end_process_color_room_weight.INK_RM_ITEM 
					Inner Join mst_units ON mst_item.intUOM = mst_units.intId ";
					
		$where	= "ORDER_NO = '$orderNo' 
					AND ORDER_YEAR = '$orderYear' 
					AND SALES_ORDER_ID = '$salesOrder' 
					AND INK_RM_ITEM = '$item' 
					AND DATE(ink_day_end_process_color_room_header.PROCESS_DATE) = '$date'";
		$order	= NULL;
		$limit	= NULL;

		$result	= $ink_day_end_process_color_room_weight->select($cols,$join,$where,$order,$limit);
		$row 	= mysqli_fetch_array($result);
		$weight	= $row['WEIGHT']==''?0:$row['WEIGHT'];
		return $weight;
	}
	
	//RETURNS USED ITEM'S WEIGHT IN ACTUAL PRODUCTION OF SALES ORDER
	public function ink_item_getUsedWeight_color_wise($orderNo,$orderYear,$salesOrder,$color,$tehnique,$inkType,$item)
	{		
		global $ink_day_end_process_color_room_weight;
		
		$weight	= 0;
		
		$cols	= "COALESCE(SUM(INK_RM_ITEM_WEIGHT/dblNoOfPcs),0) AS WEIGHT";
		$join	= "
					Inner Join mst_item ON mst_item.intId = ink_day_end_process_color_room_weight.INK_RM_ITEM 
					Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
				  ";
		$where	= "ORDER_NO='$orderNo' AND ORDER_YEAR='$orderYear' AND SALES_ORDER_ID='$salesOrder' AND INK_RM_ITEM='$item' 
				    AND COLOR_ID='$color'  AND TECHNIQUE_ID='$tehnique'  AND INK_TYPE_ID='$inkType' 	";
		$order	= NULL;
		$limit	= NULL;

		$result	= $ink_day_end_process_color_room_weight->select($cols,$join,$where,$order,$limit);
		$row 	= mysqli_fetch_array($result);
		$weight	= $row['WEIGHT']==''?0:$row['WEIGHT'];
		return $weight;
	}
	
	//RETURNS ACTUAL PRODUCTION PIECES OF SALES ORDER THAT USED ITEM
	public function ink_item_getActualProductionPCS($orderNo,$orderYear,$salesOrder,$item){		
		
		$sql	= $this->ink_item_getActualProductionPCS_sql($orderNo,$orderYear,$salesOrder,$item,'');
		$result = $this->db->RunQuery($sql);
 		$row 	= mysqli_fetch_array($result);
		return  $row['QTY'];		
	}
	
	public function ink_item_getActualProductionPCS_day_wise($orderNo,$orderYear,$salesOrder,$item,$date){		
		
		$sql	= $this->ink_item_getActualProductionPCS_sql($orderNo,$orderYear,$salesOrder,$item,$date);
		$result = $this->db->RunQuery($sql);
 		$row 	= mysqli_fetch_array($result);
		return  $row['QTY'];		
	}
	
	//RETURNS ACTUAL PRODUCTION PIECES OF SALES ORDER THAT USED COLOR
	public function color_getActualProductionPCS($orderNo,$orderYear,$salesOrder){
		
		global $ink_day_end_process_color_room_pcs;
		$cols	= "SUM(PRODUCTION_QTY) AS QTY";
		$join	= NULL;
		$where	= "ORDER_NO='$orderNo' AND ORDER_YEAR='$orderYear'  /*AND COLOR_ID='$color' AND TECHNIQUE_ID='$tehnique' AND INK_TYPE_ID='$inkType'*/";
		if($salesOrder!='')
		$where	.= " AND SALES_ORDER_ID='$salesOrder' ";
		
		$order	= NULL;
		$limit	= NULL;

		$result	= $ink_day_end_process_color_room_pcs->select($cols,$join,$where,$order,$limit);
		$row 	= mysqli_fetch_array($result);
		return  $row['QTY'];	
	}
	
	public function ink_item_getActualProductionPCS_sql($orderNo,$orderYear,$salesOrder,$item,$date)
	{
		if($date!='')
			$para	= "AND DATE(ink_day_end_process_color_room_header.PROCESS_DATE) = '$date'";
		$sql	= "
					SELECT 
					SUM(PRODUCTION_QTY) AS QTY 
					FROM 
					(SELECT
					SUM(ink_day_end_process_color_room_pcs.PRODUCTION_QTY) AS PRODUCTION_QTY 
					FROM
					ink_day_end_process_color_room_pcs
					INNER JOIN ink_day_end_process_color_room_weight 
						ON ink_day_end_process_color_room_pcs.PROCESS_ID = ink_day_end_process_color_room_weight.PROCESS_ID 
						AND ink_day_end_process_color_room_pcs.ORDER_NO = ink_day_end_process_color_room_weight.ORDER_NO 
						AND ink_day_end_process_color_room_pcs.ORDER_YEAR = ink_day_end_process_color_room_weight.ORDER_YEAR 
						AND ink_day_end_process_color_room_pcs.SALES_ORDER_ID = ink_day_end_process_color_room_weight.SALES_ORDER_ID 						
					INNER JOIN ink_day_end_process_color_room_header 
						ON ink_day_end_process_color_room_header.PROCESS_ID = ink_day_end_process_color_room_pcs.PROCESS_ID
					WHERE
					ink_day_end_process_color_room_weight.INK_RM_ITEM = '$item' 
					AND ink_day_end_process_color_room_pcs.ORDER_NO = '$orderNo' 
					AND ink_day_end_process_color_room_pcs.ORDER_YEAR = '$orderYear' 
					AND ink_day_end_process_color_room_pcs.SALES_ORDER_ID = '$salesOrder' 
					$para
					
					GROUP BY
					ink_day_end_process_color_room_pcs.ORDER_NO,
					ink_day_end_process_color_room_pcs.ORDER_YEAR,
					ink_day_end_process_color_room_pcs.SALES_ORDER_ID
					) as TB		";	
		return $sql;
		
	}
	
	
	//RETURNS CONSUMPTION OF ITEM WHICH IS IN SAMPLE
	public function ink_item_sampleCosumptionForSampleNo($sampleNo,$sampleYear,$revision,$combo,$print,$part,$item){
		global $trn_sampleinfomations_details_technical;
		$sql	= $this->ink_item_sampleCosumptionForSampleNo_sql($sampleNo,$sampleYear,$revision,$combo,$print,$part,$item);
		$result = $this->db->RunQuery($sql);
 		$row 	= mysqli_fetch_array($result);
		return  $row['CONSUMPTION'];	
	}
	
	public function ink_item_sampleCosumptionForSampleNo_color_wise($sampleNo,$sampleYear,$revision,$combo,$print,$part,$color,$tehnique,$inkType,$item){
		global $trn_sampleinfomations_details_technical;
		$sql	= $this->ink_item_sampleCosumptionForSampleNo_color_wise_sql($sampleNo,$sampleYear,$revision,$combo,$print,$part,$color,$tehnique,$inkType,$item);
		$result = $this->db->RunQuery($sql);
 		$row 	= mysqli_fetch_array($result);
		return  $row['CONSUMPTION'];	
	}
	
	
	public function ink_item_sampleCosumptionForSampleNo_sql($sampleNo,$sampleYear,$revision,$combo,$print,$part,$item){
		
		$sql	= "
					select  ROUND(SUM(dblColorWeight/sumWeight*dblWeight /dblNoOfPcs),9) as CONSUMPTION 
					 from ( SELECT 
					 
					SCR.dblWeight,
					trn_sampleinfomations_details_technical.dblColorWeight ,
					mst_units.dblNoOfPcs ,
					
					 (SELECT Sum(trn_sample_color_recipes.dblWeight) AS sumWeight 
					FROM trn_sample_color_recipes 
					WHERE trn_sample_color_recipes.intSampleNo = SCR.intSampleNo 
					AND trn_sample_color_recipes.intSampleYear = SCR.intSampleYear 
					AND trn_sample_color_recipes.intRevisionNo = SCR.intRevisionNo 
					AND trn_sample_color_recipes.strCombo = SCR.strCombo 
					AND trn_sample_color_recipes.strPrintName = SCR.strPrintName 
					AND trn_sample_color_recipes.intTechniqueId = SCR.intTechniqueId 
					AND trn_sample_color_recipes.intInkTypeId = SCR.intInkTypeId 
					AND trn_sample_color_recipes.intColorId = SCR.intColorId 
					) as sumWeight 
					FROM  trn_sample_color_recipes  as SCR 
					Inner Join trn_sampleinfomations_details_technical ON trn_sampleinfomations_details_technical.intSampleNo = SCR.intSampleNo 
					AND trn_sampleinfomations_details_technical.intSampleYear = SCR.intSampleYear 
					AND trn_sampleinfomations_details_technical.intRevNo = SCR.intRevisionNo 
					AND trn_sampleinfomations_details_technical.strComboName = SCR.strCombo 
					AND trn_sampleinfomations_details_technical.strPrintName = SCR.strPrintName 
					AND trn_sampleinfomations_details_technical.intColorId = SCR.intColorId 
					AND trn_sampleinfomations_details_technical.intInkTypeId = SCR.intInkTypeId 
					Inner Join mst_item ON mst_item.intId = SCR.intItem 
					Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
					Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory 
					Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory 
					WHERE SCR.intSampleNo='$sampleNo' 
					AND SCR.intSampleYear='$sampleYear' 
					AND SCR.intRevisionNo='$revision' 
					AND SCR.strCombo='$combo' 
					AND SCR.strPrintName='$print' 
					AND SCR.intItem='$item' 
					) as t
		";
		return $sql;
		
	}

	public function ink_item_sampleCosumptionForSampleNo_color_wise_sql($sampleNo,$sampleYear,$revision,$combo,$print,$part,$color,$tehnique,$inkType,$item){
		
		$sql	= "
					select  ROUND(SUM(dblColorWeight/sumWeight*dblWeight/dblNoOfPcs),9) as CONSUMPTION 
					 from ( SELECT 
					 
					SCR.dblWeight,
					trn_sampleinfomations_details_technical.dblColorWeight ,
					mst_units.dblNoOfPcs ,
					
					 (SELECT Sum(trn_sample_color_recipes.dblWeight) AS sumWeight 
					FROM trn_sample_color_recipes 
					WHERE trn_sample_color_recipes.intSampleNo = SCR.intSampleNo 
					AND trn_sample_color_recipes.intSampleYear = SCR.intSampleYear 
					AND trn_sample_color_recipes.intRevisionNo = SCR.intRevisionNo 
					AND trn_sample_color_recipes.strCombo = SCR.strCombo 
					AND trn_sample_color_recipes.strPrintName = SCR.strPrintName 
					AND trn_sample_color_recipes.intTechniqueId = SCR.intTechniqueId 
					AND trn_sample_color_recipes.intInkTypeId = SCR.intInkTypeId 
					AND trn_sample_color_recipes.intColorId = SCR.intColorId 
					) as sumWeight 
					FROM  trn_sample_color_recipes  as SCR 
					Inner Join trn_sampleinfomations_details_technical ON trn_sampleinfomations_details_technical.intSampleNo = SCR.intSampleNo 
					AND trn_sampleinfomations_details_technical.intSampleYear = SCR.intSampleYear 
					AND trn_sampleinfomations_details_technical.intRevNo = SCR.intRevisionNo 
					AND trn_sampleinfomations_details_technical.strComboName = SCR.strCombo 
					AND trn_sampleinfomations_details_technical.strPrintName = SCR.strPrintName 
					AND trn_sampleinfomations_details_technical.intColorId = SCR.intColorId 
					AND trn_sampleinfomations_details_technical.intInkTypeId = SCR.intInkTypeId 
					Inner Join mst_item ON mst_item.intId = SCR.intItem 
					Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
					Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory 
					Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory 
					WHERE SCR.intSampleNo='$sampleNo' 
					AND SCR.intSampleYear='$sampleYear' 
					AND SCR.intRevisionNo='$revision' 
					AND SCR.strCombo='$combo' 
					AND SCR.strPrintName='$print' 
					AND SCR.intColorId='$color' 
					AND SCR.intTechniqueId='$tehnique' 
					AND SCR.intInkTypeId='$inkType' 
					AND SCR.intItem='$item' 
					) as t
		";
		return $sql;
		
	} 
	
	/**************************************************************************
			COLOR 
	**************************************************************************/
	
	//IF ACTUAL CUNSUMPTIONS EXISTS, RETURNS ACTUAL CONS.ELSE RETURNS SAMPLE CONS FOR SALES ORDER USED COLOR
	public function color_cosumption($orderNo,$orderYear,$salesOrder,$color,$tehnique,$inkType){
		
		if(!$this->color_actualCosumption($orderNo,$orderYear,$salesOrder,$color,$tehnique,$inkType))
		$cons	= $this->color_sampleCosumption($orderNo,$orderYear,$salesOrder,$color,$tehnique,$inkType);
		else
		$cons	= $this->color_actualCosumption($orderNo,$orderYear,$salesOrder,$color,$tehnique,$inkType);
		
		return $cons;
	}
	
	
	//RETURNS SAMPLE COLOR SAMPLE CONSUMPTION OF SALES ORDER USED COLOR
	public function color_sampleCosumption($orderNo,$orderYear,$salesOrder,$color,$tehnique,$inkType){
		global $trn_orderdetails;
		$trn_orderdetails->set($orderNo,$orderYear,$salesOrder);
 		$sampCons	= $this->color_sampleCosumptionForSampleNo($trn_orderdetails->getintSampleNo(),$trn_orderdetails->getintSampleYear(),$trn_orderdetails->getintRevisionNo(),$trn_orderdetails->getstrCombo(),$trn_orderdetails->getstrPrintName(),$trn_orderdetails->getintPart(),$color,$tehnique,$inkType);
 		return $sampCons;	
	}
	
	//RETURNS ACTUAL COLOR ACTUAL CONSUMPTION OF SALES ORDER USED COLOR
	public function color_actualCosumption($orderNo,$orderYear,$salesOrder,$color,$tehnique,$inkType){
 			if($this->color_getActualProductionPCS($orderNo,$orderYear,$salesOrder) > 0)
			$actualCons	= $this->color_getUsedWeight($orderNo,$orderYear,$salesOrder,$color,$tehnique,$inkType)/$this->color_getActualProductionPCS($orderNo,$orderYear,$salesOrder);
		return $actualCons;	
	}
	
	//RETURNS USED COLOR'S WEIGHT IN ACTUAL PRODUCTION OF SALES ORDER
	public function color_getUsedWeight($orderNo,$orderYear,$salesOrder,$color,$tehnique,$inkType){
		
		global $ink_day_end_process_color_room_weight;
		$cols	= "SUM(INK_RM_ITEM_WEIGHT) AS WEIGHT";
		$join	= NULL;
		$where	= "ORDER_NO='$orderNo' AND ORDER_YEAR='$orderYear' AND SALES_ORDER_ID='$salesOrder' AND COLOR_ID='$color' AND TECHNIQUE_ID='$tehnique' AND INK_TYPE_ID='$inkType'";
		$order	= NULL;
		$limit	= NULL;

		$result	= $ink_day_end_process_color_room_weight->select($cols,$join,$where,$order,$limit);
		$row 	= mysqli_fetch_array($result);
		return  $row['WEIGHT'];	
	}
	
	//RETURNS CONSUMPTION OF ITEM WHICH IS IN SAMPLE
	public function color_sampleCosumptionForSampleNo($sampleNo,$sampleYear,$revision,$combo,$print,$part,$color,$tehnique,$inkType){
		global $trn_sampleinfomations_details_technical;
		$sql	= $this->color_sampleCosumptionForSampleNo_sql($sampleNo,$sampleYear,$revision,$combo,$print,$part,$color,$tehnique,$inkType);
		$result = $this->db->RunQuery($sql);
 		$row 	= mysqli_fetch_array($result);
		return  $row['CONSUMPTION'];	
	}
	
	
	/**************************************************************************
			FOIL 
	**************************************************************************/

	public function foil_specialRM_item_actualCosumption_average($orderNo,$orderYear,$salesOrder,$item){
		
		global $trn_ordersizeqty;
		global $mst_item;
		global $deci;
		$actCon			 	= 0;
		$avgConPc			= 0;
		
		$mst_item->set($item);
		$order_size_result	= $trn_ordersizeqty->getsizeWiseOrderQty($orderNo,$orderYear,$salesOrder);
		while($rowSW = mysqli_fetch_array($order_size_result))
		{
			$actCon	    = $this->foil_specialRM_item_actualCosumption($orderNo,$orderYear,$salesOrder,$item,$rowSW['strSize']);
			
			if($actCon>0)
			{
				$tot_100_Con		+= $actCon*$rowSW['dblQty'];
				$tot_100_orderQty	+= $rowSW['dblQty'];
			}
		}
		
		if($mst_item->getITEM_TYPE()==3){//special rm(pcs)
			
			if($tot_100_orderQty>0 && $tot_100_Con>0 )
			$avgConPc	= round($tot_100_Con/$tot_100_orderQty,$deci+2);
			//$avgConPc	= round($tot_100_Con/$tot_100_orderQty+0.5);
		}
		else if($mst_item->getITEM_TYPE()==2){
			if($tot_100_orderQty>0 && $tot_100_Con>0 )
				$avgConPc	= round($tot_100_Con/$tot_100_orderQty,$deci+2);
		}
		return $avgConPc;
	}
	
	public function foil_specialRM_item_actualCosumption_average_day_wise($orderNo,$orderYear,$salesOrder,$item,$date,$deci)
	{		
		$result	= $this->foil_specialRM_item_actualCosumption_average_day_wise_sql($orderNo,$orderYear,$salesOrder,$item,$date,$deci);
		$row	= mysqli_fetch_array($result);
		return $row['AVG_CONPC'];
	}
	
	public function foil_specialRM_item_actualCosumption_average_day_wise_lase_date($orderNo,$orderYear,$salesOrder,$item,$date,$deci)
	{		
		$result	= $this->foil_specialRM_item_actualCosumption_average_day_wise_lase_date_sql($orderNo,$orderYear,$salesOrder,$item,$date,$deci);
		$row	= mysqli_fetch_array($result);
		return $row['AVG_CONPC'];
	}
	
	public function check_foil_specialRM_item_actualCosumption_average_day_wise($orderNo,$orderYear,$salesOrder,$item,$date,$deci)
	{		
		$result	= $this->foil_specialRM_item_actualCosumption_average_day_wise_sql($orderNo,$orderYear,$salesOrder,$item,$date,$deci);
		$count	= mysqli_affected_rows($result);
		
		if($count>0)
		{
			return $this->foil_specialRM_item_actualCosumption_average_day_wise($orderNo,$orderYear,$salesOrder,$item,$date,$deci);
		}
		else
		{
			return $this->foil_specialRM_item_actualCosumption_average_day_wise_lase_date($orderNo,$orderYear,$salesOrder,$item,$date,$deci);
		}
	}
	
	
	public function foil_item_cosumption($orderNo,$orderYear,$salesOrder,$item,$size){
		
		if(!$this->foil_specialRM_item_actualCosumption($orderNo,$orderYear,$salesOrder,$item,$size))
		$cons	= $this->foil_item_sampleCosumption($orderNo,$orderYear,$salesOrder,$item);
		else
		$cons	= $this->foil_specialRM_item_actualCosumption($orderNo,$orderYear,$salesOrder,$item,$size);
		
		return $cons;
	}
	
	//RETURNS SAMPLE CONSUMPTION OF SALES ORDER USED ITEM
	public function foil_item_sampleCosumption($orderNo,$orderYear,$salesOrder,$item){
		global $trn_orderdetails;
		$trn_orderdetails->set($orderNo,$orderYear,$salesOrder);
		$sampCons	=$this->foil_item_sampleCosumptionForSampleNo($trn_orderdetails->getintSampleNo(),$trn_orderdetails->getintSampleYear(),$trn_orderdetails->getintRevisionNo(),$trn_orderdetails->getstrCombo(),$trn_orderdetails->getstrPrintName(),$trn_orderdetails->getintPart(),$item);
 		return $sampCons;	
	}

	//RETURNS ACTUAL CONSUMPTION OF SALES ORDER USED ITEM
	public function foil_specialRM_item_actualCosumption($orderNo,$orderYear,$salesOrder,$item,$size){
		
		$sql	= $this->foil_specialRM_item_actualCosumption_size_wise_sql($orderNo,$orderYear,$salesOrder,$item,$size);
		$result = $this->db->RunQuery($sql);
		$val1	=0;	
		$val2	=0;	
		while($row 	= mysqli_fetch_array($result)){
			
			$val1 +=$row['QTY']*$row['CONSUMPTION'];	
			$val2 +=$row['QTY'];	
		}
		
		if($val2 <=0)
		$actualCons	=0;
		else
		$actualCons	= $val1/$val2;
		
 		return $actualCons;	
	}
	
	//RETURNS CONSUMPTION OF ITEM WHICH IS IN SAMPLE
	public function foil_item_sampleCosumptionForSampleNo($sampleNo,$sampleYear,$revision,$combo,$print,$part,$item){
		global $trn_sampleinfomations_details_technical;
		$sql	= $this->foil_item_sampleCosumptionForSampleNo_sql($sampleNo,$sampleYear,$revision,$combo,$print,$part,$item);
		$result = $this->db->RunQuery($sql);
 		$row 	= mysqli_fetch_array($result);
		return  $row['CONSUMPTION'];	
	}
	
	public function foil_item_sampleCosumptionForSampleNo_sql($sampleNo,$sampleYear,$revision,$combo,$print,$part,$item){
	
		$sql	="
					SELECT
					sum(trn_sample_foil_consumption.dblMeters) as CONSUMPTION
					FROM
					trn_sample_foil_consumption  
					Inner Join mst_item ON mst_item.intId = trn_sample_foil_consumption.intItem
					Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
					Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
					Inner Join mst_units ON mst_units.intId = mst_item.intUOM
					Inner Join mst_financecurrency ON mst_financecurrency.intId = mst_item.intCurrency
					WHERE
					trn_sample_foil_consumption.intSampleNo = '$sampleNo' AND
					trn_sample_foil_consumption.intSampleYear = '$sampleYear' AND
					trn_sample_foil_consumption.intRevisionNo = '$revision' AND
					trn_sample_foil_consumption.strCombo = '$combo' AND
					trn_sample_foil_consumption.strPrintName = '$print' AND
					trn_sample_foil_consumption.intItem = '$item'
					GROUP BY
					mst_maincategory.intId,
					mst_subcategory.intId,
					mst_item.intId				";
		
		return $sql;
		
	}
	
	public function foil_specialRM_item_actualCosumption_average_day_wise_sql($orderNo,$orderYear,$salesOrder,$item,$date,$deci)
	{
		global $db;
		$sql = "SELECT 
				  ROUND(SUM(OS.dblQty * CD.CONSUMPTION)/SUM(OS.dblQty),$deci+2)		AS AVG_CONPC
				FROM ink_foil_special_rm_actual_conpc_header CH
				  INNER JOIN ink_foil_special_rm_actual_conpc_details CD
					ON CH.SERIAL_NO = CD.SERIAL_NO
					  AND CH.SERIAL_YEAR = CD.SERIAL_YEAR
				  INNER JOIN trn_ordersizeqty OS
					ON CH.ORDER_NO = OS.intOrderNo
					  AND CH.ORDER_YEAR = OS.intOrderYear
					  AND CH.SALES_ORDER_ID = OS.intSalesOrderId
					  AND CD.SIZE = OS.strSize
				WHERE CH.ORDER_NO = '$orderNo'
					AND CH.ORDER_YEAR = '$orderYear'
					AND CH.SALES_ORDER_ID = '$salesOrder'
					AND CH.`STATUS` = 1
					AND CD.ITEM_ID = '$item'
					AND DATE(CREATED_DATE) = '$date'";
		return $this->db->RunQuery($sql);
	}
	
	public function foil_specialRM_item_actualCosumption_average_day_wise_lase_date_sql($orderNo,$orderYear,$salesOrder,$item,$date,$deci)
	{
		global $db;
		$sql = "SELECT 
				  ROUND(
					SUM(OS.dblQty * CD.CONSUMPTION) / SUM(OS.dblQty),$deci+2 ) AS AVG_CONPC 
				FROM
				  ink_foil_special_rm_actual_conpc_header CH 
				  INNER JOIN ink_foil_special_rm_actual_conpc_details CD 
					ON CH.SERIAL_NO = CD.SERIAL_NO 
					AND CH.SERIAL_YEAR = CD.SERIAL_YEAR 
				  INNER JOIN trn_ordersizeqty OS 
					ON CH.ORDER_NO = OS.intOrderNo 
					AND CH.ORDER_YEAR = OS.intOrderYear 
					AND CH.SALES_ORDER_ID = OS.intSalesOrderId 
					AND CD.SIZE = OS.strSize 
				WHERE CH.ORDER_NO = '$orderNo' 
				  AND CH.ORDER_YEAR = '$orderYear' 
				  AND CH.SALES_ORDER_ID = '$salesOrder' 
				  AND CH.`STATUS` = 1 
				  AND CD.ITEM_ID = '$item' 
				  AND DATE(CREATED_DATE) = 
				  (SELECT 
					MAX(DATE(CREATED_DATE)) 
				  FROM
					ink_foil_special_rm_actual_conpc_header CH 
					INNER JOIN ink_foil_special_rm_actual_conpc_details CD 
					  ON CH.SERIAL_NO = CD.SERIAL_NO 
					  AND CH.SERIAL_YEAR = CD.SERIAL_YEAR 
				  WHERE CH.ORDER_NO = '$orderNo' 
					AND CH.ORDER_YEAR = '$orderYear' 
					AND CH.SALES_ORDER_ID = '$salesOrder' 
					AND CH.`STATUS` = 1 
					AND CD.ITEM_ID = '$item')";
		return $this->db->RunQuery($sql);
	}
	
	public function foil_specialRM_item_actualCosumption_size_wise_sql($orderNo,$orderYear,$salesOrder,$item,$size)	{
		
		$sql	="
			SELECT
			 trn_ordersizeqty.dblQty as QTY,
			ink_foil_special_rm_actual_conpc_details.CONSUMPTION 
			FROM
			ink_foil_special_rm_actual_conpc_header
			INNER JOIN ink_foil_special_rm_actual_conpc_details ON ink_foil_special_rm_actual_conpc_header.SERIAL_NO = ink_foil_special_rm_actual_conpc_details.SERIAL_NO AND ink_foil_special_rm_actual_conpc_header.SERIAL_YEAR = ink_foil_special_rm_actual_conpc_details.SERIAL_YEAR
			INNER JOIN trn_ordersizeqty ON ink_foil_special_rm_actual_conpc_header.ORDER_NO = trn_ordersizeqty.intOrderNo AND ink_foil_special_rm_actual_conpc_header.ORDER_YEAR = trn_ordersizeqty.intOrderYear AND ink_foil_special_rm_actual_conpc_header.SALES_ORDER_ID = trn_ordersizeqty.intSalesOrderId AND ink_foil_special_rm_actual_conpc_details.SIZE = trn_ordersizeqty.strSize
			WHERE
			ink_foil_special_rm_actual_conpc_header.ORDER_NO = '$orderNo' AND
			ink_foil_special_rm_actual_conpc_header.ORDER_YEAR = '$orderYear' AND
			ink_foil_special_rm_actual_conpc_header.SALES_ORDER_ID = '$salesOrder' AND
			ink_foil_special_rm_actual_conpc_header.`STATUS` = 1 AND
			ink_foil_special_rm_actual_conpc_details.ITEM_ID = '$item'  AND
			ink_foil_special_rm_actual_conpc_details.SIZE = '$size'  AND 
			
			CONSUMPTION_REVISION_ID=(select Max(ink_foil_special_rm_actual_conpc_header.CONSUMPTION_REVISION_ID) FROM
			ink_foil_special_rm_actual_conpc_header
			INNER JOIN ink_foil_special_rm_actual_conpc_details ON ink_foil_special_rm_actual_conpc_header.SERIAL_NO = ink_foil_special_rm_actual_conpc_details.SERIAL_NO AND ink_foil_special_rm_actual_conpc_header.SERIAL_YEAR = ink_foil_special_rm_actual_conpc_details.SERIAL_YEAR
			INNER JOIN trn_ordersizeqty ON ink_foil_special_rm_actual_conpc_header.ORDER_NO = trn_ordersizeqty.intOrderNo AND ink_foil_special_rm_actual_conpc_header.ORDER_YEAR = trn_ordersizeqty.intOrderYear AND ink_foil_special_rm_actual_conpc_header.SALES_ORDER_ID = trn_ordersizeqty.intSalesOrderId AND ink_foil_special_rm_actual_conpc_details.SIZE = trn_ordersizeqty.strSize
			WHERE
			ink_foil_special_rm_actual_conpc_header.ORDER_NO = '$orderNo' AND
			ink_foil_special_rm_actual_conpc_header.ORDER_YEAR = '$orderYear' AND
			ink_foil_special_rm_actual_conpc_header.SALES_ORDER_ID = '$salesOrder' AND
			ink_foil_special_rm_actual_conpc_header.`STATUS` = 1 AND
			ink_foil_special_rm_actual_conpc_details.ITEM_ID = '$item'  AND
			ink_foil_special_rm_actual_conpc_details.SIZE = '$size'  ) 
			GROUP BY ink_foil_special_rm_actual_conpc_details.SIZE	";
		
		return $sql;
	}
	
	/**************************************************************************
			SPECIAL RM 
	**************************************************************************/
	public function special_item_cosumption($orderNo,$orderYear,$salesOrder,$item,$size){
		
		if(!$this->foil_specialRM_item_actualCosumption($orderNo,$orderYear,$salesOrder,$item,$size))
		$cons	= $this->special_item_sampleCosumption($orderNo,$orderYear,$salesOrder,$item);
		else
		$cons	= $this->foil_specialRM_item_actualCosumption($orderNo,$orderYear,$salesOrder,$item,$size);
		
		return $cons;
	}
	
	//RETURNS SAMPLE CONSUMPTION OF SALES ORDER USED ITEM
	public function special_item_sampleCosumption($orderNo,$orderYear,$salesOrder,$item){
		global $trn_orderdetails;
		$trn_orderdetails->set($orderNo,$orderYear,$salesOrder);
		$sampCons	=$this->special_item_sampleCosumptionForSampleNo($trn_orderdetails->getintSampleNo(),$trn_orderdetails->getintSampleYear(),$trn_orderdetails->getintRevisionNo(),$trn_orderdetails->getstrCombo(),$trn_orderdetails->getstrPrintName(),$trn_orderdetails->getintPart(),$item);
 		return $sampCons;	
	}

	public function special_item_sampleCosumptionForSampleNo($sampleNo,$sampleYear,$revision,$combo,$print,$part,$item){
		global $trn_sampleinfomations_details_technical;
		$sql	= $this->special_item_sampleCosumptionForSampleNo_sql($sampleNo,$sampleYear,$revision,$combo,$print,$part,$item);
		$result = $this->db->RunQuery($sql);
 		$row 	= mysqli_fetch_array($result);
		return  $row['CONSUMPTION'];	
	}

	public function special_item_sampleCosumptionForSampleNo_sql($sampleNo,$sampleYear,$revision,$combo,$print,$part,$item){
		
		$sql	= 
				"
				SELECT
				sum(trn_sample_spitem_consumption.dblQty/dblNoOfPcs) as CONSUMPTION
				FROM
				trn_sample_spitem_consumption 
				Inner Join mst_item ON mst_item.intId = trn_sample_spitem_consumption.intItem
				Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
				Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
				Inner Join mst_units ON mst_units.intId = mst_item.intUOM
				Inner Join mst_financecurrency ON mst_financecurrency.intId = mst_item.intCurrency
				WHERE
				trn_sample_spitem_consumption.intSampleNo = '$sampleNo' AND
				trn_sample_spitem_consumption.intSampleYear = '$sampleYear' AND
				trn_sample_spitem_consumption.intRevisionNo = '$revision' AND
				trn_sample_spitem_consumption.strCombo = '$combo' AND
				trn_sample_spitem_consumption.strPrintName = '$print' AND
				trn_sample_spitem_consumption.intItem = '$item'
				GROUP BY
				mst_maincategory.intId,
				mst_subcategory.intId,
				mst_item.intId";
		
		return $sql;
	 
	}	
	
	public function color_sampleCosumptionForSampleNo_sql($sampleNo,$sampleYear,$revision,$combo,$print,$part,$color,$tehnique,$inkType){
/*		$sql	= "
					select  sum(round(dblColorWeight/sumWeight*dblWeight,6))as CONSUMPTION 
					 from ( SELECT 
					 
					SCR.dblWeight,
					trn_sampleinfomations_details_technical.dblColorWeight ,
					mst_units.dblNoOfPcs ,
					
					 (SELECT Sum(trn_sample_color_recipes.dblWeight) AS sumWeight 
					FROM trn_sample_color_recipes 
					WHERE trn_sample_color_recipes.intSampleNo = SCR.intSampleNo 
					AND trn_sample_color_recipes.intSampleYear = SCR.intSampleYear 
					AND trn_sample_color_recipes.intRevisionNo = SCR.intRevisionNo 
					AND trn_sample_color_recipes.strCombo = SCR.strCombo 
					AND trn_sample_color_recipes.strPrintName = SCR.strPrintName 
					AND trn_sample_color_recipes.intTechniqueId = SCR.intTechniqueId 
					AND trn_sample_color_recipes.intInkTypeId = SCR.intInkTypeId 
					AND trn_sample_color_recipes.intColorId = SCR.intColorId 
					) as sumWeight 
					FROM  trn_sample_color_recipes  as SCR 
					Inner Join trn_sampleinfomations_details_technical ON trn_sampleinfomations_details_technical.intSampleNo = SCR.intSampleNo 
					AND trn_sampleinfomations_details_technical.intSampleYear = SCR.intSampleYear 
					AND trn_sampleinfomations_details_technical.intRevNo = SCR.intRevisionNo 
					AND trn_sampleinfomations_details_technical.strComboName = SCR.strCombo 
					AND trn_sampleinfomations_details_technical.strPrintName = SCR.strPrintName 
					AND trn_sampleinfomations_details_technical.intColorId = SCR.intColorId 
					AND trn_sampleinfomations_details_technical.intInkTypeId = SCR.intInkTypeId 
					Inner Join mst_item ON mst_item.intId = SCR.intItem 
					Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
					Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory 
					Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory 
					WHERE SCR.intSampleNo='$sampleNo' 
					AND SCR.intSampleYear='$sampleYear' 
					AND SCR.intRevisionNo='$revision' 
					AND SCR.strCombo='$combo' 
					AND SCR.strPrintName='$print' 
					AND SCR.intColorId='$color' 
					AND SCR.intTechniqueId='$tehnique' 
					AND SCR.intInkTypeId='$inkType' 
					) as t
		";
*/		
		$sql	= "
					select  ROUND(SUM(dblColorWeight),6) as CONSUMPTION 
				    FROM 
					( SELECT 
 					SDT.dblColorWeight 
					FROM 
					trn_sampleinfomations_details_technical AS SDT
					WHERE SDT.intSampleNo='$sampleNo' 
					AND SDT.intSampleYear='$sampleYear' 
					AND SDT.intRevNo='$revision' 
					AND SDT.strComboName='$combo' 
					AND SDT.strPrintName='$print' 
					AND SDT.intColorId='$color' 
					AND SDT.intInkTypeId='$inkType' 
					) as t
		";
		
		return $sql;
		
	}
	
	
	public function getOrderWiseRequestableQty_BKP($orderNo,$orderYear,$salesOrderId,$itemId,$locationId,$deci)
	{
		global $trn_orderdetails;
		global $ink_color_room_issue_details;	
		global $sys_company_item_allocation_percentages;
		global $sessions;
		global $common;
		global $ink_color_room_requisition_details;
		global $ware_stocktransactions_color_room_item;
		global $item_allocation_unallocation;
		
		$trn_orderdetails->set($orderNo,$orderYear,$salesOrderId);
		$orderQty			= $trn_orderdetails->getintQty();
		$wastedQty			= $ware_stocktransactions_color_room_item->getInkItemWastage($orderNo,$orderYear,$salesOrderId,$itemId,$deci);
		#$actItmPropotion	= $item_allocation_unallocation->getActualItemPropotion($orderNo,$orderYear,$salesOrderId,$itemId,$deci);
		#$issuedQty			= $ink_color_room_issue_details->getColorRoomIssuedItemQty($locationId,"'".$orderNo.'/'.$orderYear.'/'.$salesOrderId."'",$itemId,$crnNo='',$crnYear='',$deci);
		$issuedQty			= $ink_color_room_requisition_details->getRequestItemQty($locationId,"'".$orderNo.'/'.$orderYear.'/'.$salesOrderId."'",$itemId,$deci);
		$issuedQty			= $issuedQty - $wastedQty;
		
		$sys_company_item_allocation_percentages->set($sessions->getCompanyId());
		$sampleAllocation 	= ($sys_company_item_allocation_percentages->getSAMPLE_ALLOCATION());
		$fullAllocation 	= ($sys_company_item_allocation_percentages->getFULL_ALLOCATION());
		$srnExtra 			= ($sys_company_item_allocation_percentages->getINK_ITEM_EXTRA_SRN());
		
		if($this->ink_item_getActualProductionPCS($orderNo,$orderYear,$salesOrderId,$itemId)>0)
		{
			$actualCons 	= $this->ink_item_actualCosumption($orderNo,$orderYear,$salesOrderId,$itemId);
			$issuable		= ($orderQty * $actualCons * $fullAllocation/100) * (1 + $srnExtra/100);
			$balToIssue		= ($issuable - $issuedQty);
			$balToIssue		= $common->ceil_deci($balToIssue,$deci);
			return $balToIssue;	
		}
		else
		{
			$sampleConpc 	= $this->ink_item_sampleCosumption($orderNo,$orderYear,$salesOrderId,$itemId);
			$issuable		= ($orderQty * $sampleConpc * $sampleAllocation/100) * (1 + $srnExtra/100);
			$balToIssue		= ($issuable - $issuedQty);
			$balToIssue		= $common->ceil_deci($balToIssue,$deci);
			
			return $balToIssue;
		}
	}

	
	public function getOrderWiseRequestableQty($orderNo,$orderYear,$salesOrderId,$itemId,$locationId,$deci)
	{
		global $trn_orderdetails;
		global $ink_color_room_issue_details;	
		global $sys_company_item_allocation_percentages;
		global $sessions;
		global $common;
		global $ink_color_room_requisition_details;
		global $ware_stocktransactions_color_room_item;
		global $ware_stocktransactions_color_room_color;
		global $item_allocation_unallocation;
		global $ink_day_end_process_color_room_pcs;
		global $ink_day_end_process_color_room_weight;
		global $mst_units;
		global $trn_sample_color_recipes;
		global $ware_fabricdispatchconfirmation_details;
		global $ink_color_room_issue_to_production_header;
		
		
		//gel bal to production qty
		$trn_orderdetails->set($orderNo,$orderYear,$salesOrderId);
 		
		$orderQty	=$trn_orderdetails->getintQty()+$trn_orderdetails->getintQty()*($trn_orderdetails->getdblOverCutPercentage()+$trn_orderdetails->getdblToleratePercentage())/100;
		$orderQty	+=$ware_fabricdispatchconfirmation_details->loadDamageReturnedQty_confirmed($orderNo,$orderYear,$salesOrderId);
 		
		$bal_pcs_before	=$trn_orderdetails->getBAL_PRODUCTION_BEFORE_CR_PROCESS();
		if($bal_pcs_before>0)
		$orderQty	=$bal_pcs_before;
	
		$orderQty	= $orderQty-$this->ink_item_getActualProductionPCS($orderNo,$orderYear,$salesOrderId,$itemId);
		
		
		if($orderQty < 0){
			$orderQty=0;
		}
		
		//wasted qty
		$wastedQty			= $ware_stocktransactions_color_room_item->getInkItemWastage_noneProcessed($orderNo,$orderYear,$salesOrderId,$itemId,$deci);

		//already requested Qty (none used in production)
		$requestedQty		= $ink_color_room_requisition_details->getRequestItemQty($locationId,"'".$orderNo.'/'.$orderYear.'/'.$salesOrderId."'",$itemId,$deci);

 		$resultn = $trn_sample_color_recipes->getColorsForSelectedOrdersAndItem($orderNo,$orderYear,$salesOrderId,$itemId);
		$used_processed =0;
		while($rown = mysqli_fetch_array($resultn))
		{
 			$used_processed1		= $ink_color_room_issue_to_production_header->getProcessedUsageOfColor('',$orderNo,$orderYear,$salesOrderId,$rown['COLOR_ID'],$rown['TECHNIQUE_ID'],$rown['INK_TYPE'],$deci);
			$itemPropotion 			= $ware_stocktransactions_color_room_item->getActualItemPropotion($orderNo,$orderYear,$salesOrderId,$itemId,$rown['COLOR_ID'],$rown['TECHNIQUE_ID'],$rown['INK_TYPE'],$deci);
			$used_processed		   += ($used_processed1 * $itemPropotion);
		}
		$used_processed	   =($mst_units->conversion('G',$used_processed,$itemId,6));	

 		$requestedQty		= $requestedQty;
		
		
		//color room order item stock
		$colRoomItemStk		= $ware_stocktransactions_color_room_item->getOrderWiseColorRoomItemStockBalance_without_location($orderNo,$orderYear,$salesOrderId,$itemId);
	
		//item weight from stock existing color weight
		//ticke #284  -CRN - color stock (item prapotion)
		$colorStock=0;
		$res_cs	=$trn_sample_color_recipes->getColorsForSelectedOrdersAndItem($orderNo,$orderYear,$salesOrderId,$itemId);
	while($row = mysqli_fetch_array($res_cs))
	{
		$color		= $row['COLOR_ID'];
		$ink  		= $row['INK_TYPE'];
		$technique 	= $row['TECHNIQUE_ID'];
		
		$colorStock		   +=($ware_stocktransactions_color_room_color->getColorRoomColorBalance('',$orderNo,$orderYear,$salesOrderId,$color,$technique,$ink,$deci))*($ware_stocktransactions_color_room_item->getActualItemPropotion($orderNo,$orderYear,$salesOrderId,$itemId,$color,$technique,$ink,$deci));
 		//if($itemId==723)
		//echo ($ware_stocktransactions_color_room_color->getColorRoomColorBalance('',$orderNo,$orderYear,$salesOrderId,$color,$technique,$ink,$deci)).'*'.($ware_stocktransactions_color_room_item->getActualItemPropotion($orderNo,$orderYear,$salesOrderId,$itemId,$color,$technique,$ink,$deci));
		
	}
		
 		
		$colorStock		   =($mst_units->conversion('G',$colorStock,$itemId,$deci));	
	
		//production used item weght(processed)
 		$dayEndItemUsed		=($this->ink_item_getUsedWeight($orderNo,$orderYear,$salesOrderId,$itemId));
		
		//production used item weght(processed)
 		$issuedToCRoom		=($ink_color_room_issue_details->getColorRoomIssuedItemQty('',"'".$orderNo.'/'.$orderYear.'/'.$salesOrderId."'",$itemId,$crnNo,$crnYear,$deci));

		//-------
		$resultn = $trn_sample_color_recipes->getColorsForSelectedOrdersAndItem($orderNo,$orderYear,$salesOrderId,$itemId);
		$nonReturned =0;
		while($rown = mysqli_fetch_array($resultn))
		{

			$nonReturned1			= $ink_color_room_issue_to_production_header->getNoneProcessedUsageOfColor('',$orderNo,$orderYear,$salesOrderId,$rown['COLOR_ID'],$rown['TECHNIQUE_ID'],$rown['INK_TYPE'],$deci);
			$itemPropotion 			= $ware_stocktransactions_color_room_item->getActualItemPropotion($orderNo,$orderYear,$salesOrderId,$itemId,$rown['COLOR_ID'],$rown['TECHNIQUE_ID'],$rown['INK_TYPE'],$deci);
			$nonReturned		   += ($nonReturned1 * $itemPropotion);
		}
		$nonReturned	   =($mst_units->conversion('G',$nonReturned,$itemId,6));	
		//----------

		$sys_company_item_allocation_percentages->set($sessions->getCompanyId());
		$sampleAllocation 	= ($sys_company_item_allocation_percentages->getSAMPLE_ALLOCATION());
		$fullAllocation 	= ($sys_company_item_allocation_percentages->getFULL_ALLOCATION());
		$srnExtra 			= ($sys_company_item_allocation_percentages->getINK_ITEM_EXTRA_SRN());
		
		//get required total
		if($this->ink_item_getActualProductionPCS($orderNo,$orderYear,$salesOrderId,$itemId)>0)
		{
			$actualCons 	= $this->ink_item_actualCosumption($orderNo,$orderYear,$salesOrderId,$itemId);
			$requiredTot	= ($orderQty * $actualCons * $fullAllocation/100) * (1 + $srnExtra/100);
			
		}
		else
		{
			$sampleConpc 	= $this->ink_item_sampleCosumption($orderNo,$orderYear,$salesOrderId,$itemId);
			$requiredTot		= ($orderQty * $sampleConpc * $sampleAllocation/100) * (1 + $srnExtra/100);
		}
		
 		//if($itemId==723)
			//echo 'A'.'('.$requiredTot.'-'.$colRoomItemStk.' - '.$colorStock.'+'.$wastedQty.'-'.'(('.$requestedQty.'-'.$used_processed.')-'.$issuedToCRoom.'))';			
			
			$balToIssue		= ($requiredTot - $colRoomItemStk-$colorStock+$wastedQty-(($requestedQty-$used_processed)-$nonReturned));
			if($orderQty==0)
			$balToIssue=0;
			$balToIssue		= $common->ceil_deci($balToIssue,$deci);
			
			return $balToIssue;
		
		
	}
	
	
	public function getOrderWiseInkItemAllocatableQty($orderNo,$orderYear,$salesOrderId,$itemId,$locationId,$deci)
	{
		global $trn_orderdetails;
		global $ink_color_room_issue_details;	
		global $sys_company_item_allocation_percentages;
		global $sessions;
		global $common;
		global $ink_color_room_requisition_details;
		global $ware_stocktransactions_color_room_item;
		global $ware_stocktransactions_color_room_color;
		global $ware_stocktransactions;
		global $item_allocation_unallocation;
		global $ink_day_end_process_color_room_pcs;
		global $ink_day_end_process_color_room_weight;
		global $mst_units;
		global $ware_fabricdispatchconfirmation_details;
		global $ink_color_room_issue_to_production_header;
		global $trn_sample_color_recipes;
		
		
		//gel bal to production qty
		$trn_orderdetails->set($orderNo,$orderYear,$salesOrderId);
 		
		$orderQty	= $trn_orderdetails->getintQty()+$trn_orderdetails->getintQty()*($trn_orderdetails->getdblOverCutPercentage()+$trn_orderdetails->getdblToleratePercentage())/100;
		$orderQty	+=$ware_fabricdispatchconfirmation_details->loadDamageReturnedQty_confirmed($orderNo,$orderYear,$salesOrderId);
 		
		$bal_pcs_before	=$trn_orderdetails->getBAL_PRODUCTION_BEFORE_CR_PROCESS();
		if($bal_pcs_before>0)
		$orderQty	=$bal_pcs_before;
		
		$orderQty	= $orderQty-$this->ink_item_getActualProductionPCS($orderNo,$orderYear,$salesOrderId,$itemId);
 		
		if($orderQty < 0){
			$orderQty=0;
		}
		$orderQty	= $common->ceil_deci($orderQty,$deci);
		
 		$colRoomItemStk		= $ware_stocktransactions_color_room_item->getOrderWiseColorRoomItemStockBalance_without_location($orderNo,$orderYear,$salesOrderId,$itemId);

		$styleStorsItemStk = $ware_stocktransactions->getMainStoresOrderWiseItemBalance('',$orderNo,$orderYear,$salesOrderId,$itemId,$deci);
		
		
		//wasted
		$wastedQty			= $ware_stocktransactions_color_room_item->getInkItemWastage_noneProcessed($orderNo,$orderYear,$salesOrderId,$itemId,$deci);

		//$colorStock			=($ware_stocktransactions_color_room_color->getColorRoomColorBalance('',$orderNo,$orderYear,$salesOrderId,'','','',$deci))*($ware_stocktransactions_color_room_item->getActualItemPropotion_so($orderNo,$orderYear,$salesOrderId,$itemId,$deci));
	
		$colorStock		   =0;	
		$resultw = $trn_sample_color_recipes->getColorsForSelectedOrdersAndItem($orderNo,$orderYear,$salesOrderId,$itemId);
		while($roww = mysqli_fetch_array($resultw))
		{

			$colorStock1			= $ware_stocktransactions_color_room_color->getColorRoomColorBalance('',$orderNo,$orderYear,$salesOrderId,$roww['COLOR_ID'],$roww['TECHNIQUE_ID'],$roww['INK_TYPE'],$deci);
			$itemPropotion 		= $ware_stocktransactions_color_room_item->getActualItemPropotion($orderNo,$orderYear,$salesOrderId,$itemId,$roww['COLOR_ID'],$roww['TECHNIQUE_ID'],$roww['INK_TYPE'],$deci);
			$colorStock		   += round($colorStock1 * $itemPropotion,$deci);
		}

		$colorStock		   =($mst_units->conversion('G',$colorStock,$itemId,$deci));	
	
	//////////////////////////////////////////
		$resultn = $trn_sample_color_recipes->getColorsForSelectedOrdersAndItem($orderNo,$orderYear,$salesOrderId,$itemId);
		$nonReturned =0;
		while($rown = mysqli_fetch_array($resultn))
		{

			$nonReturned1			= $ink_color_room_issue_to_production_header->getNoneProcessedUsageOfColor('',$orderNo,$orderYear,$salesOrderId,$rown['COLOR_ID'],$rown['TECHNIQUE_ID'],$rown['INK_TYPE'],$deci);
			$itemPropotion 		= $ware_stocktransactions_color_room_item->getActualItemPropotion($orderNo,$orderYear,$salesOrderId,$itemId,$rown['COLOR_ID'],$rown['TECHNIQUE_ID'],$rown['INK_TYPE'],$deci);
			
			 		//if($itemId==723)
					//echo "A".$nonReturned1 .'* '.$itemPropotion;
			$nonReturned		   += round($nonReturned1 * $itemPropotion,$deci);
		}
		
 		
		$nonReturned	   =($mst_units->conversion('G',$nonReturned,$itemId,$deci));	
		
		$sys_company_item_allocation_percentages->set($sessions->getCompanyId());
		$sampleAllocation 	= ($sys_company_item_allocation_percentages->getSAMPLE_ALLOCATION());
		$fullAllocation 	= ($sys_company_item_allocation_percentages->getFULL_ALLOCATION());
		$extra 				= ($sys_company_item_allocation_percentages->getINK_ITEM_EXTRA_ALLOCATION());
		
		if($this->ink_item_getActualProductionPCS($orderNo,$orderYear,$salesOrderId,$itemId)>0)
		{
			$actualCons 	= $this->ink_item_actualCosumption($orderNo,$orderYear,$salesOrderId,$itemId);
			$required		= ($orderQty * $actualCons * $fullAllocation/100) * (1 + $extra/100);
		}
		else
		{
			$sampleConpc 	= $this->ink_item_sampleCosumption($orderNo,$orderYear,$salesOrderId,$itemId);
			$required		= ($orderQty * $sampleConpc * $fullAllocation/100) * (1 + $extra/100);
		}
		
 		//if($itemId==863)
			//echo 'A'.'('.$required.' -'.$styleStorsItemStk.' - '.$colRoomItemStk.'-('.$nonReturned.')-'.$colorStock.'+'.$wastedQty.')';			
			
			$balToAlloc		= ($required -$styleStorsItemStk - $colRoomItemStk-($nonReturned)-$colorStock+$wastedQty);
			if($orderQty==0)
			$balToAlloc=0;
			$balToAlloc		= $common->ceil_deci($balToAlloc,$deci);
			
			return $balToAlloc;
		
		
	}
	
	
}
?>
<?php

class cls_commonFunctions_get
{
	
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}

	public function boolPermision($userId,$programCode,$field)
	{
		$sql = "SELECT
					menupermision.$field as value
				FROM
					menupermision
					Inner Join menus ON menus.intId = menupermision.intMenuId
				WHERE
					menus.strCode 			=  '$programCode' AND
					menupermision.intUserId =  '$userId'
				";
		$result = $this->db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return $row['value'];	
	}
	
	public function get_approve_levels($code,$execution){
		
	 	$sql = "SELECT
					sys_approvelevels.intApprovalLevel
				FROM sys_approvelevels
				WHERE
					sys_approvelevels.strCode =  '$code'
				";
		$result = $this->db->$execution($sql);
		$row = mysqli_fetch_array($result);
		return $row['intApprovalLevel'];
	}
	
	public function getApproveLevels($name){
		
	 	$sql = "SELECT
					sys_approvelevels.intApprovalLevel
				FROM sys_approvelevels
				WHERE
					sys_approvelevels.strName =  '$name'
				";
		$result = $this->db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		return $row['intApprovalLevel'];
	}
	
	public function getApproveLevels_new($code)
	{		
	 	$sql = "SELECT
				  COUNT(*)            	AS COUNT,
				  AL.intApprovalLevel 	AS APPROVE_LEVEL
				FROM sys_approvelevels AL
				WHERE AL.strCode = '$code'";
		$result = $this->db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		if($row["COUNT"]=='0')
		{
			$response['type']		= false;
			$response['msg']		= "Approve Levels not assign in the system.";
		}
		else
		{
			$response['type']		= true;
			$response['ApproLevel'] = $row["APPROVE_LEVEL"];
		}
		return $response;
	}
	
	public function getApproveLevels_new1($code,$executeType)
	{		
	 	$sql = "SELECT
				  COUNT(*)            	AS COUNT,
				  AL.intApprovalLevel 	AS APPROVE_LEVEL
				FROM sys_approvelevels AL
				WHERE AL.strCode = '$code'";
		$result = $this->db->$executeType($sql);
		$row = mysqli_fetch_array($result);
		if($row["COUNT"]=='0')
		{
			$response['type']		= false;
			$response['msg']		= "Approve Levels not assign in the system.";
		}
		else
		{
			$response['type']		= true;
			$response['ApproLevel'] = $row["APPROVE_LEVEL"];
		}
		return $response;
	}
	
	public function get_special_approve_levels($name,$executionType){
		
	 	$sql = "SELECT
				sys_special_approvelevels.intApprovalLevel
				FROM `sys_special_approvelevels`
				WHERE
				sys_special_approvelevels.strName = '$name'
				";
		$result = $this->db->$executionType($sql);
		$row = mysqli_fetch_array($result);
		return $row['intApprovalLevel'];
	}
	
	public function getPermission($progCode,$user,$confLevel,$status){
		
			    $sql = "SELECT
					menupermision.int".$confLevel."Approval 
					FROM menupermision 
					Inner Join menus ON menupermision.intMenuId = menus.intId
					WHERE
					menus.strCode =  '$progCode' AND
					menupermision.intUserId =  '$user'";	
							
				$result = $this->db->RunQuery2($sql);
				$row = mysqli_fetch_array($result);
					 if($status==1){
					 $confirmatonMode=0;
					 $editMode=0;
					 }
					 else if($row['int'.$confLevel.'Approval']==1){
					 $confirmatonMode=1;
					 }
					 else{
					 $confirmatonMode=0;//no user permission to confirm
					 }
					 
		return $confirmatonMode;
	}
	
	public function Load_menupermision($progCode,$userId,$field){
		
		$sql = "SELECT
				menupermision.".$field."  as permition 
				FROM menupermision 
				Inner Join menus ON menupermision.intMenuId = menus.intId
				WHERE
				menus.strCode =  '$progCode' AND
				menupermision.intUserId =  '$userId'";	
		
		$result = $this->db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return $row['permition'];
		
	}
	function Load_menupermision2($progCode,$userId,$field){
		
		$sql = "SELECT
				menupermision.".$field."  as permition 
				FROM menupermision 
				Inner Join menus ON menupermision.intMenuId = menus.intId
				WHERE
				menus.strCode =  '$progCode' AND
				menupermision.intUserId =  '$userId'";	
		
		$result = $this->db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		return $row['permition'];
	}
 	
	public function getUsers_options($location){
				   	$sql = "SELECT
							sys_users.intUserId,
							sys_users.strUserName
							FROM sys_users
							Inner Join mst_locations_user ON sys_users.intUserId = mst_locations_user.intUserId
							WHERE
							mst_locations_user.intLocationId =  '$location' AND 
							sys_users.intStatus =  '1' 
							ORDER BY sys_users.strUserName ASC "; 
					$result = $this->db->RunQuery2($sql);
					$combo ="<option value=\"\"></option>";
					while($row=mysqli_fetch_array($result))
					{
						$combo .="<option value=\"".$row['intUserId']."\">".$row['strUserName']."</option>";	
					}
				 return $combo;
	}
	
	
	public function getDepartments_options(){
				   	$sql = "SELECT
							mst_department.intId,
							mst_department.strName
							FROM mst_department
							WHERE
							mst_department.intStatus =  '1'
							ORDER BY
							mst_department.strName ASC "; 
					$result = $this->db->RunQuery2($sql);
					$combo ="<option value=\"\"></option>";
					while($row=mysqli_fetch_array($result))
					{
						$combo .="<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				 return $combo;
	}
	
	public function getConfirmPermission($progCode,$user,$confLevel,$status){
		
			    $sql = "SELECT
					menupermision.int".$confLevel."Approval 
					FROM menupermision 
					Inner Join menus ON menupermision.intMenuId = menus.intId
					WHERE
					menus.strCode =  '$progCode' AND
					menupermision.intUserId =  '$user'";	
							
				$result = $this->db->RunQuery2($sql);
				$row = mysqli_fetch_array($result);
				 if($row['int'.$confLevel.'Approval']==1){
				 $confirmatonMode=1;
				 }
				 else{
				 $confirmatonMode=0;//no user permission to confirm
				 }
					 
		return $confirmatonMode;
	}
	
	public function getEditPermission($progCode,$user,$confLevel,$status){
		
			    $sql = "SELECT
					menupermision.intEdit  
					FROM menupermision 
					Inner Join menus ON menupermision.intMenuId = menus.intId
					WHERE
					menus.strCode =  '$progCode' AND
					menupermision.intUserId =  '$user'";	
							
				$result = $this->db->RunQuery2($sql);
				$row = mysqli_fetch_array($result);
				if($row['intEdit']==1){
				$editMode=1;
				}
				else{
				$editMode=0;//no user permission to confirm
				}
					 
		return $editMode;
	}

	public function getConfirmableMode($confLevel,$status){

				 if(($status>1) && ($confLevel>=($status-1))){
				 $confirmatonMode=1;
				 }
				 else{
				 $confirmatonMode=0;//no user permission to confirm
				 }
					 
		return $confirmatonMode;
	}
	
	public function getEditableMode($confLevel,$status){
		
				if($status==0){
				$editMode=1;
				}
				else if(($confLevel+1)==$status){
				$editMode=1;
				}
				else{
				$editMode=0;//no user permission to confirm
				}
					 
		return $editMode;
	}
	
		public function getLocationType($locationId){
		
			    $sql = "SELECT
				mst_locations.intProduction, mst_locations.RESTRICTIONS
				FROM `mst_locations`
				WHERE
				mst_locations.intId = '$locationId'
				";	
							
				$result = $this->db->RunQuery($sql);
				$row = mysqli_fetch_array($result);
				if($row['intProduction']==1){
				$type=1;
				if ($row['RESTRICTIONS'] ==1){
                    $type = 2;
                }
				}
				else{
				$type=0;//no user permission to confirm
				}
					 
		return $type;
	}
		public function getLocationType2($locationId){
		
			    $sql = "SELECT
				mst_locations.intProduction
				FROM `mst_locations`
				WHERE
				mst_locations.intId = '$locationId'
				";	
							
				$result = $this->db->RunQuery2($sql);
				$row = mysqli_fetch_array($result);
				if($row['intProduction']==1){
				$type=1;
				}
				else{
				$type=0;//no user permission to confirm
				}
					 
		return $type;
	}
	public function GetSystemMaxNo($fieldName,$companyId)
	{		
		$sql 	= "SELECT $fieldName as max_no FROM sys_no WHERE intCompanyId=$companyId";
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		if(mysqli_num_rows($result)>0)
		{
			$max_no	= $row["max_no"];
			$sql 	= "UPDATE sys_no SET $fieldName=$fieldName+1 WHERE intCompanyId=$companyId";
			$result = $this->db->RunQuery2($sql);
			if(($result) && ($max_no !=''))
			{
				$data['rollBackFlag'] 	= 0 ;
				$data['max_no'] 		= $max_no ;
			}
			else if(($max_no =='') || ($max_no ==0)){
				$data['rollBackFlag'] 	= 1 ;
				$data['msg'] 			= 'System No not initialized.' ;
				$data['q'] 				= $sql ;
			}
			else
			{
				$data['rollBackFlag'] 	= 1 ;
				$data['msg'] 			= 'System No update error.' ;
				$data['q'] 				= $sql ;
			}
		}
		else
		{
			$data['rollBackFlag'] 	= 1 ;
			$data['msg'] 			= 'System No not defined.' ;
			$data['q'] 				= $sql ;
		}
		return $data;
	}
	
	public function GetSystemMaxNoNew($fieldName,$companyId,$executeType)
	{		
		$sql 	= "SELECT ifnull($fieldName,START_SEQUENCE_NO) as max_no FROM sys_no WHERE intCompanyId=$companyId";
		
		//echo $sql;
		$result = $this->db->$executeType($sql);
		$row	= mysqli_fetch_array($result);
		if(mysqli_num_rows($result)>0)
		{
			$max_no	= $row["max_no"];
			$sql 	= "UPDATE sys_no SET $fieldName=ifnull($fieldName,0)+1 WHERE intCompanyId=$companyId";
			$result = $this->db->$executeType($sql);
			if(($result) && ($max_no !=''))
			{
				$data['type'] 			= true;
				$data['max_no'] 		= $max_no;
			}
			else if(($max_no =='') || ($max_no ==0)){
				$data['type'] 			= false;
				$data['errorMsg'] 		= 'System No not initialized.';
				$data['errorSql'] 		= $sql;
			}
			else
			{
				$data['type'] 			= false;
				$data['errorMsg'] 		= 'System No update error.';
				$data['errorSql'] 		= $sql;
			}
		}
		else
		{
			$data['type'] 				= false;
			$data['errorMsg'] 			= 'System No not defined.';
			$data['errorSql'] 			= $sql;
		}
		return $data;
	}
	
	public function replace($str)
	{
		  $str 	= 	str_replace('\\', "\\\\",	$str);
		  $str 	= 	str_replace("'","\'",	$str);
		  $str	=	str_replace("\n", "<br>",$str);  
		  
		  return $str;
	}	
	
	public function get_permision_save($status,$levels,$session_userId,$pgrmCode)
	{
 		$permition=0;
 		$perm= $this->Load_menupermision($pgrmCode,$session_userId,'intEdit');
 		if(($status == 0) && ($perm ==1)){
			$permition=1;
		}
		if(($status == '') && ($perm ==1)){
			$permition=1;
		}
		if(($status==($levels+1)) && ($perm ==1)){
			$permition=1;
		}
 		
		return $permition;
	}
	public function get_permision_confirm($status,$levels,$session_userId,$pgrmCode)
	{
 		$permition=0;
 		$k=$levels-$status+2;
		$field='int'.$k.'Approval';
 		$perm= $this->Load_menupermision($pgrmCode,$session_userId,$field);
		if(($status>1) && ($perm ==1)){
			$permition=1;
		}
		
		return $permition;
	}
	public function get_permision_reject($status,$levels,$session_userId,$pgrmCode)
	{
 		$permition=0;
 		$perm= $this->Load_menupermision($pgrmCode,$session_userId,'intReject');
		if(($status>1) && ($status<=$levels) && ($perm ==1)){
			$permition=1;
		}
		
		return $permition;
	}
	public function get_permision_cancel($status,$levels,$session_userId,$pgrmCode)
	{
 		$permition=0;
 		$perm= $this->Load_menupermision($pgrmCode,$session_userId,'intCancel');
		if(($status==1) && ($perm ==1) && ($status!=10)){
			$permition=1;
		}
		
		return $permition;
	}
	public function get_permision_save2($status,$levels,$session_userId,$pgrmCode)
	{
 		$permition=0;
 		$perm= $this->Load_menupermision2($pgrmCode,$session_userId,'intEdit');
 		if(($status == 0) && ($perm ==1)){
			$permition=1;
		}
		if(($status == '') && ($perm ==1)){
			$permition=1;
		}
		if(($status==($levels+1)) && ($perm ==1)){
			$permition=1;
		}
 		
		return $permition;
	}
	public function get_permision_confirm2($status,$levels,$session_userId,$pgrmCode)
	{
 		$permition=0;
 		$k=$levels-$status+2;
		$field='int'.$k.'Approval';
 		$perm= $this->Load_menupermision2($pgrmCode,$session_userId,$field);
		if(($status>1) && ($perm ==1)){
			$permition=1;
		}
		
		return $permition;
	}
	public function get_permision_reject2($status,$levels,$session_userId,$pgrmCode)
	{
 		$permition=0;
 		$perm= $this->Load_menupermision2($pgrmCode,$session_userId,'intReject');
		if(($status>1) && ($status<=$levels) && ($perm ==1)){
			$permition=1;
		}       
		
		return $permition;
	}
	public function get_permision_cancel2($status,$levels,$session_userId,$pgrmCode)
	{
 		$permition=0;
 		$perm= $this->Load_menupermision($pgrmCode,$session_userId,'intCancel');
		if(($status==1) && ($perm ==1) && ($status!=10)){
			$permition=1;
		}
		
		return $permition;
	}
	public function getMonthName($month)
	{
		$sqlp = "SELECT
				mst_month.strMonth
				FROM `mst_month`
				WHERE
				mst_month.intMonthId = $month
				";	
		$resultp = $this->db->RunQuery($sqlp);
		$rowp=mysqli_fetch_array($resultp);
		return $rowp['strMonth'];
	}
	public function get_month_name($month,$executeType)
	{
		$sqlp = "SELECT
				mst_month.strMonth
				FROM `mst_month`
				WHERE
				mst_month.intMonthId = $month
				";	
		$resultp = $this->db->$executeType($sqlp);
		$rowp=mysqli_fetch_array($resultp);
		return $rowp['strMonth'];
	}
	public function getUserName($id)
	{
		$sqlp = "SELECT
				sys_users.strFullName
				FROM `sys_users`
				WHERE
				sys_users.intUserId = '$id'
 				";	
		$resultp = $this->db->RunQuery($sqlp);
		$rowp=mysqli_fetch_array($resultp);
		return $rowp['strFullName'];
	}
	public function getCurrencyName($id)
	{
		$sqlp = "SELECT
				mst_financecurrency.strCode
				FROM
				mst_financecurrency
				WHERE
				mst_financecurrency.intId = '$id'
				";	
		$resultp = $this->db->RunQuery($sqlp);
		$rowp=mysqli_fetch_array($resultp);
		return $rowp['strCode'];
	}
	
	public function get_currency_name_of_grn_item($grnNo,$grnYear,$item,$executeType)
	{
	if(($grnNo!=0) && ($grnYear!=0)){
	  $sql = "SELECT
			trn_poheader.intCurrency as currency 
			FROM
			ware_grnheader
			Inner Join trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
			WHERE
			ware_grnheader.intGrnNo =  '$grnNo' AND
			ware_grnheader.intGrnYear =  '$grnYear'";
	}
	else{
	$sql = "SELECT
			mst_item.intCurrency as currency 
			FROM mst_item
			WHERE
			mst_item.intId =  '$item'";
	}

	$result = $this->db->$executeType($sql);
	$row=mysqli_fetch_array($result);
	return $row['currency'];;	
	}
	
	public function getEmailList($idList){
		
	  $sql = "SELECT
					group_concat(sys_users.strEmail) as emails
			FROM
				sys_users
			WHERE 
				sys_users.intStatus = 1 AND 
				 sys_users.intUserId in($idList) 
			";
         
	$result = $this->db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	return $row['emails']; 
 	}
	
	public function getEmailList2($idList){
		
	  $sql = "SELECT
					group_concat(sys_users.strEmail) as emails
			FROM
				sys_users
			WHERE 
				sys_users.intStatus = 1 AND 
				 sys_users.intUserId in($idList) 
			";	
	$result = $this->db->RunQuery2($sql);
	$row 	= mysqli_fetch_array($result);
	return $row['emails']; 
 	}

	public function getEmailListAll($idList){
		
	  $sql = "SELECT
					group_concat(sys_users.strEmail) as emails
			FROM
				sys_users
			WHERE 
				sys_users.intUserId in($idList) 
			";	
	$result = $this->db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	return $row['emails']; 
 	}
	
public function getClusters($reportType){
		
		global $db ;
		
		$sql = "SELECT 
		sys_report_cluster_types.REPORT_TYPE,
		sys_report_cluster_types.CLUSTER_TYPE,
		sys_report_cluster_types.`NO`,
		sys_report_cluster_types.`TO`,
		sys_report_cluster_types.CC,
		sys_report_cluster_types.BCC ,
		sys_report_cluster_types.WEEKLY_HOLIDAY 
		FROM
		sys_report_cluster_types
		WHERE
		sys_report_cluster_types.REPORT_TYPE = '$reportType' AND
		sys_report_cluster_types.`STATUS` = 1
		ORDER BY
		sys_report_cluster_types.`STATUS` ASC ";
		$result = $this->db->RunQuery($sql);
		
		return $result;
	}
//2015-06-18	
public function getMainClusters(){
		
		global $db ;
		
		$sql = "SELECT 
		sys_main_clusters.ID,
		sys_main_clusters.NAME,
		sys_main_clusters.`STATUS`,
		sys_main_clusters.`COMPANY`,
		sys_main_clusters.WEEKLY_HOLIDAY AS HOLY_ID,
		mst_days_of_week.ID AS WEEKLY_HOLIDAY   
		FROM
		sys_main_clusters 
		LEFT JOIN mst_days_of_week ON sys_main_clusters.WEEKLY_HOLIDAY = mst_days_of_week.ID
		WHERE
		sys_main_clusters.`STATUS` = 1
		ORDER BY
		sys_main_clusters.`NAME` ASC ";
		$result = $this->db->RunQuery($sql);
		
		return $result;
	}

public function getSubClusters($mainCluster){
		
		global $db ;
		
		$sql = "SELECT 
		sys_sub_clusters.ID,
		sys_sub_clusters.NAME,
		sys_sub_clusters.`STATUS` 
		FROM
		sys_sub_clusters 
		WHERE
		sys_sub_clusters.`STATUS` = 1 AND 
		MAIN_CLUSTER ='$mainCluster' 
		ORDER BY
		sys_sub_clusters.`NAME` ASC ";
		$result = $this->db->RunQuery($sql);
		
		return $result;
	}
	
public function get_main_cluster_plants($mainCluster,$subCluster){
		
		global $db ;
		
		$sql = "SELECT
				mst_plant.intPlantId,
				mst_plant.strPlantName
				FROM `mst_plant`
				WHERE 
				1=1 AND ";
		if($mainCluster!='')
		$sql .= " mst_plant.MAIN_CLUSTER_ID = '$mainCluster' AND ";
		if($subCluster!='')
		$sql .= " mst_plant.SUB_CLUSTER_ID = '$subCluster' AND ";
		$sql .= "mst_plant.intStatus = 1 order by mst_plant.strPlantName asc";
		$result = $this->db->RunQuery($sql);
		
		return $result;
	}

public function getMainClusterReportType($cluster,$reportId){
		
	 global $db ;
		
	$sql = "SELECT
		sys_daily_reports_of_main_clusters.REPORT_TYPE
		FROM `sys_daily_reports_of_main_clusters`
		WHERE
		sys_daily_reports_of_main_clusters.REPORT_ID = '$reportId' AND
		sys_daily_reports_of_main_clusters.MAIN_CLUSTER_ID = '$cluster'	";
			
	$result = $this->db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	return $row['REPORT_TYPE'];
		
	}	
	
public function get_marketerClusters($marketer){
		
		global $db ;
		
		$sql = "SELECT
		sys_report_cluster_types.REPORT_TYPE,
		sys_report_cluster_types.CLUSTER_TYPE,
		sys_report_cluster_types.`NO`,
		sys_report_cluster_types.`TO`,
		sys_report_cluster_types.CC,
		sys_report_cluster_types.BCC 
		FROM
		mst_marketer
		INNER JOIN sys_report_cluster_types ON FIND_IN_SET(sys_report_cluster_types.`NO`,mst_marketer.SALES_PROJECTION_TYPE)  
		WHERE
		mst_marketer.intUserId = '$marketer'  AND
		sys_report_cluster_types.`STATUS` = 1
		ORDER BY
		sys_report_cluster_types.`STATUS` ASC ";
		$result = $this->db->RunQuery($sql);
		
		return $result;
	}
	
public function getPlants(){
		
	 global $db ;
		
	  $sql = "SELECT
				mst_plant.strPlantName,
				mst_plant.intPlantId,
				mst_plant.MAIN_CLUSTER_ID 
				FROM `mst_plant`
				WHERE
				mst_plant.intStatus = 1
				ORDER BY
				mst_plant.strPlantName ASC
 				";
		$result = $this->db->RunQuery($sql);
 		return $result;
	}	
	
public function getClusterReportCompany($cluster){
		
	 global $db ;
		
	 $sql = "SELECT
				mst_companies.intId,
				mst_companies.strCode,
				mst_companies.strName,
				mst_companies.intCountryId,
				mst_companies.strWebSite,
				mst_companies.strRemarks,
				mst_companies.strAccountNo,
				mst_companies.strRegistrationNo,
				mst_companies.strVatNo,
				mst_companies.strSVatNo,
				mst_companies.intBaseCurrencyId,
				mst_companies.intStatus,
				mst_companies.intCreator,
				mst_companies.dtmCreateDate,
				mst_companies.intModifyer,
				mst_companies.dtmModifyDate
				FROM
				sys_report_cluster_types
				INNER JOIN mst_companies ON sys_report_cluster_types.COMPANY_ID = mst_companies.intId
				WHERE
				sys_report_cluster_types.`NO` = '$cluster'
				";
		$result = $this->db->RunQuery($sql);
		$row 	= mysqli_fetch_array($result);
		return $row;
	}	
	
	public function get_exchane_rate($currency,$date,$company,$executionType){
		
	  $sql = "SELECT
				mst_financeexchangerate.dblExcAvgRate
				FROM `mst_financeexchangerate`
				WHERE
				mst_financeexchangerate.intCurrencyId = '$currency' AND
				mst_financeexchangerate.dtmDate = '$date' AND
				mst_financeexchangerate.intCompanyId = '$company' 
			";	
	$result = $this->db->$executionType($sql);
	$row 	= mysqli_fetch_array($result);
	return $row['dblExcAvgRate']; 
 	}
	
	public function ValidateSpecialPermission($permissionId,$userId,$transType)
	{
		$sql = "SELECT MS.intStatus
				FROM menus M
				INNER JOIN menus_special MS ON M.intId=MS.intMenuId
				INNER JOIN menus_special_permision SP ON MS.intId=SP.intSpMenuId
				WHERE MS.intId = '$permissionId' AND
					SP.intUser='$userId' ";
		$result = $this->db->$transType($sql);
		$row = mysqli_fetch_array($result);
		
		return $row['intStatus'];
	}
	
	public function days_in_month($month, $year)
	{
		// calculate number of days in a month
		return $month == 2 ? ($year % 4 ? 28 : ($year % 100 ? 29 : ($year % 400 ? 28 : 29))) : (($month - 1) % 7 % 2 ? 30 : 31);
	}
	public function loadLocation($company,$transType)
	{
		$sql = "SELECT intId,strName
				FROM mst_locations
				WHERE intCompanyId='$company' AND
				intStatus = 1 
				ORDER BY strName";
		
		$result = $this->db->$transType($sql);
		
		return $result;
	}
	public function loadCompany_for_location($location,$transType)
	{
		$sql = "SELECT mst_locations.intCompanyId,mst_companies.strName as company_name 
				FROM mst_locations 
				INNER JOIN mst_companies ON mst_companies.intId = mst_locations.intCompanyId
				WHERE mst_locations.intId='$location' ";
		
		$result = $this->db->$transType($sql);
		
		return $result;
	}
	public function loadLocation_with_selected_val($company,$location,$transType)
	{
		$sql = "SELECT intId,strName
				FROM mst_locations
				WHERE intCompanyId='$company' ";
		if($location != ''){
			$sql.=" AND mst_locations.intId = '$location'";
		} else {
            $sql.=" AND mst_locations.intId IN (210, 183, 182, 57, 15)";
        }
		$sql .= " AND
				intStatus = 1 
				ORDER BY strName";
		//echo $sql;
		$result = $this->db->$transType($sql);
		
		return $result;
	}
	
	public function loadCompany_result($company,$transType)
	{
		$sql = "SELECT 
				mst_companies.intId,
				mst_companies.strName,
				mst_companies.intBaseCurrencyId,
				mst_financecurrency.strCode AS currency 
				FROM mst_companies 
				LEFT JOIN mst_financecurrency ON mst_companies.intBaseCurrencyId = mst_financecurrency.intId 
				WHERE 
				mst_companies.intStatus = 1  ";
		if($company!='')		
		$sql .= " AND mst_companies.intId='$company' ";
		$sql .= " ORDER BY mst_companies.strName"; 
		
		$result = $this->db->$transType($sql);
		
		return $result;
	}
	
	public function loadMainCatName($mainCat,$transType)
	{
		$sql = "SELECT intId,strName
				FROM mst_maincategory
				WHERE intId='$mainCat'  
				ORDER BY strName";
		
		$result = $this->db->$transType($sql);
		
		return $result;
	}
	public function loadSubCatName($subCat,$transType)
	{
		$sql = "SELECT intId,strName
				FROM mst_subcategory
				WHERE intId='$subCat'  
				ORDER BY strName";
		
		$result = $this->db->$transType($sql);
		
		return $result;
	}
	
	public function loadLocationName($location,$transType)
	{
		$sql = "SELECT intId,strName
				FROM mst_locations
				WHERE intId='$location'  
				ORDER BY strName";
		
		$result = $this->db->$transType($sql);
		
		return $result;
	}
	
	public function load_sub_stores_result($subStores,$transType)
	{
		$sql = "SELECT intId,strName
				FROM mst_substores
				WHERE intId='$subStores' AND
				intStatus = 1 
				ORDER BY strName";
		
		$result = $this->db->$transType($sql);
		
		return $result;
	}
	
	public function load_sales_order_result($orderNo,$orderYear,$salesOrder,$transType)
	{
		$sql = "SELECT
				trn_orderdetails.intSalesOrderId,
				trn_orderdetails.strSalesOrderNo,
				trn_orderdetails.strCombo,
				trn_orderdetails.strPrintName,
				mst_part.strName, 
				concat(strSalesOrderNo,'/',strCombo,'/',strPrintName,'/',strName,'/',intRevisionNo) as salesOrder 
				FROM
				trn_orderdetails
				INNER JOIN mst_part ON trn_orderdetails.intPart = mst_part.intId
				WHERE
				trn_orderdetails.intOrderNo = '$orderNo' AND
				trn_orderdetails.intOrderYear = '$orderYear' AND 
				trn_orderdetails.intSalesOrderId = '$salesOrder'   
				";
		
		$result = $this->db->$transType($sql);
		
		return $result;
	}
	
	public function loadLoctionComanyDetails($location,$transType)
	{
		$sql = "SELECT m_com.strName,m_loc.strName as locationName,m_loc.strAddress,
                m_loc.strStreet,m_loc.strCity,m_loc.strZip,m_ctry.strCountryName,m_loc.strPhoneNo, m_loc.strFaxNo, 
                m_loc.strEMail, m_com.strWebSite 
                FROM
				mst_companies m_com
				Inner Join mst_country m_ctry ON m_com.intCountryId = m_ctry.intCountryID
				INNER JOIN mst_locations m_loc ON m_com.intId = m_loc.intCompanyId
				WHERE
				m_loc.intId = '$location' 
				";
		
		$result = $this->db->$transType($sql);
		
		return $result;
	}
	
	public function loadDepartment($transType)
	{
		$sql = "SELECT intId,strName
				FROM mst_department
				WHERE intStatus = 1 
				ORDER BY strName";
		
		$result = $this->db->$transType($sql);
		
		return $result;
	}
	public function loadMonth($transType)
	{
		$sql = "SELECT intMonthId,strMonth
				FROM mst_month
				ORDER BY intMonthId";
		
		$result = $this->db->$transType($sql);
		
		return $result;
	}
	public function getCountry($transType)
	{
		$sql = "SELECT intCountryID,strCountryName
				FROM mst_country
				WHERE intStatus=1
				ORDER BY strCountryName ";
		
		$result = $this->db->$transType($sql);
		
		return $result;
	}
	public function get_auto_allocation_percentages($company,$transType)
	{
		$sql = "SELECT
				sys_company_item_allocation_percentages.`SAMPLE_ALLOCATION_%`,
				sys_company_item_allocation_percentages.`FULL_ALLOCATION_%`,
				sys_company_item_allocation_percentages.`FOIL_EXTRA_ALLOCATION_%`,
				sys_company_item_allocation_percentages.`SP_RM_EXTRA_ALLOCATION_%`,
				sys_company_item_allocation_percentages.`INK_ITEM_EXTRA_ALLOCATION_%`,
				sys_company_item_allocation_percentages.`INK_ITEM_EXTRA_SRN_%` 
				FROM `sys_company_item_allocation_percentages`
				WHERE
				sys_company_item_allocation_percentages.COMPANY_ID = '$company'";
		$result = $this->db->$transType($sql);
		$row = mysqli_fetch_array($result);
		
		return $row;
	}
	public function get_item_name($item,$transType)
	{
		$sql = "SELECT
				mst_item.intId,
				mst_item.intMainCategory,
				mst_item.intSubCategory,
				mst_maincategory.strName AS main_category,
				mst_subcategory.strName AS sub_category,
				mst_item.strName AS item
				FROM
				mst_item
				INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
				WHERE
				mst_item.intId = '$item'";
		$result = $this->db->$transType($sql);
		$row = mysqli_fetch_array($result);
		
		return $row['item'];
	}
	public function get_sub_category_name($subCat,$transType)
	{
		$sql = "SELECT
				mst_subcategory.intId,
				mst_subcategory.strName AS sub_category 
				FROM
				mst_subcategory
				WHERE
				mst_subcategory.intId = '$subCat'";
		$result = $this->db->$transType($sql);
		$row = mysqli_fetch_array($result);
		
		return $row['sub_category'];
	}
	
	public function get_item_details($item,$transType)
	{
		$sql = "SELECT
				mst_item.intId,
				mst_item.intMainCategory,
				mst_item.intSubCategory,
				mst_maincategory.strName AS main_category,
				mst_subcategory.strName AS sub_category,
				mst_item.strName AS item
				FROM
				mst_item
				INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
				WHERE
				mst_item.intId = '$item'";
		$result = $this->db->$transType($sql);
		$row = mysqli_fetch_array($result);
		
		return $row;
	}
	
	public function checkAmountWithRS($grandTotal,$PONo,$POYear,$supplierId,$invoice,$excType)
	{		
		$sqlPO = "SELECT SUM(ROUND((POD.dblUnitPrice*POD.dblQty)*((100-POD.dblDiscount)/100)+COALESCE(POD.dblTaxAmmount,0),2)) AS POVALUE
				FROM trn_poheader PH
				INNER JOIN trn_podetails POD ON POD.intPONo=PH.intPONo AND POD.intPOYear=PH.intPOYear
				WHERE PH.intPONo = '$PONo' AND
				PH.intPOYear = '$POYear' ";
		
		$resultPO = $this->db->$excType($sqlPO);
		$rowPO 	= mysqli_fetch_array($resultPO);
		
		$POValue	= $rowPO['POVALUE'];
		
		$sqlRS	= "SELECT COALESCE(ROUND(RSD.dblQty*GD.dblGrnRate,2)+ROUND((PD.dblTaxAmmount/(PD.dblUnitPrice*PD.dblQty*(100-PD.dblDiscount)/100))*(RSD.dblQty*GD.dblGrnRate),2),0) AS RETURN_TO_SUP_VALUE
					FROM ware_returntosupplierdetails RSD
					INNER JOIN ware_returntosupplierheader RSH ON RSH.intReturnNo=RSD.intReturnNo AND RSH.intReturnYear=RSD.intReturnYear
					INNER JOIN ware_grnheader GH ON GH.intGrnNo=RSH.intGrnNo AND GH.intGrnYear=RSH.intGrnYear
					INNER JOIN ware_grndetails GD ON GD.intGrnNo=GH.intGrnNo AND GD.intGrnYear=GH.intGrnYear AND GD.intItemId=RSD.intItemId
					INNER JOIN trn_podetails PD ON PD.intPONo = GH.intPoNo AND PD.intPOYear = GH.intPoYear AND GD.intItemId = PD.intItem
					INNER JOIN trn_poheader POH ON POH.intPONo = PD.intPONo AND POH.intPOYear=PD.intPOYear

					WHERE RSH.intStatus = 1 AND
					GH.intPoNo = '$PONo' AND
					GH.intPoYear = '$POYear' AND
					GH.strInvoiceNo = '$invoice' AND
					POH.intSupplier = '$supplierId'  limit 1";
		
		$resultRS = $this->db->$excType($sqlRS);
		$rowRS 	= mysqli_fetch_array($resultRS);
		
		$RSValue	= $rowRS['RETURN_TO_SUP_VALUE'];
		$invBal		= $POValue-$RSValue;
		
		if($grandTotal>$invBal && (($grandTotal-$invBal)*100)>3)
		{
			$data['chkStatus']		= 'fail';
			$data['POValue']		= $POValue;
			$data['RSValue']		= $RSValue;
			$data['balToInvoice']	= $invBal;
		}
		return $data;
	}
	
	
	public function load_company_locations_options($company,$executeType){
		$sql = $this->Load_Report_approval_details_sql($company);
		$result = $this->db->$executeType($sql);
			$combo ="<option value=\"\"></option>";
			while($row=mysqli_fetch_array($result))
			{
				$combo .="<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
			}
		 return $combo;
		
	}
	
	
	public function ceil_to_decimal_places($val ,$decimal_places){
		$val	=   $val*pow(10,$decimal_places);	
		$val1	=	floor($val);
		$val2	=	round(($val - $val1),3);
		if($val2>0){
			$val1	+=1;
		}
		
		$fin_val	= $val1/pow(10,$decimal_places);
		
		return $fin_val;
	}
	
	
	public function get_sys_user_msg($code,$mainDb,$executeType){

		$sql = $this->get_user_message_sql($code,$mainDb);
		$result = $this->db->$executeType($sql);
		$row=mysqli_fetch_array($result);

		return $row['MESSAGE'];
		
	}
	
	public function getDatesBetweenTwoDateRange($strDateFrom,$strDateTo)
	{
		$aryRange	= array();
	
		$iDateFrom	= mktime(1,0,0,substr($strDateFrom,5,2),substr($strDateFrom,8,2),substr($strDateFrom,0,4));
		$iDateTo	= mktime(1,0,0,substr($strDateTo,5,2),substr($strDateTo,8,2),substr($strDateTo,0,4));
	
		if ($iDateTo>=$iDateFrom)
		{
			array_push($aryRange,date('Y-m-d',$iDateFrom)); // first entry
			while ($iDateFrom<$iDateTo)
			{
				$iDateFrom	+= 86400; // add 24 hours
				array_push($aryRange,date('Y-m-d',$iDateFrom));
			}
		}
		return $aryRange;
	}
	
	public function getMonthBetweenTwoDateRange($strDateFrom,$strDateTo)
	{
		$aryRange	= array();
		$startDate 	= strtotime($strDateFrom);
		$endDate   	= strtotime($strDateTo);
		
		$currentDate = $endDate;
		
		while ($currentDate >= $startDate) {
			array_push($aryRange,date('Y-M',$currentDate));
			$currentDate = strtotime( date('Y/m/01/',$currentDate).' -1 month');
		}
		krsort($aryRange);
		return $aryRange;
	}
	
	public function get_customer_order_techniques($executeType){
		
		$sql = $this->get_customer_order_techniques_sql();
		$result = $this->db->$executeType($sql);
		return $result;
	}
	
	public function get_group_techniques($executeType){
		
		$sql = $this->get_group_techniques_sql();
		$result = $this->db->$executeType($sql);
		return $result;
	}
	
	private function get_customer_order_techniques_sql()
	{		
		$sql	="select * from (SELECT
					mst_customer_po_techniques.ID,
					mst_customer_po_techniques.NAME
					FROM `mst_customer_po_techniques`
					WHERE
					mst_customer_po_techniques.STATUS = '1' 
					order by mst_customer_po_techniques.NAME asc 
				) as t1 
				UNION
				(
					select 
					0 as ID,
					'None' as NAME 
				) ";		
		return $sql;
	}
	
	private function get_group_techniques_sql()
	{		
		$sql	="select * from (SELECT
					mst_technique_groups.TECHNIQUE_GROUP_ID as ID,
					mst_technique_groups.TECHNIQUE_GROUP_NAME as NAME
					FROM `mst_technique_groups`
					WHERE
					mst_technique_groups.STATUS = '1' 
					order by TECHNIQUE_GROUP_NAME asc 
				) as t1 
				/* UNION
				(
					select 
					0 as ID,
					'None' as NAME 
				) */ ";		
		return $sql;
	}

	
	private function Load_Report_approval_details_sql($company)
	{		
		$sql	="SELECT
				mst_locations.intId,
				mst_locations.strName
				FROM `mst_locations`
				WHERE
				mst_locations.intCompanyId = '$company'";		
		return $sql;
	}
	
	private function get_user_message_sql($code,$mainDb)
	{		
		$sql = "SELECT
				MESSAGE  
				FROM $mainDb.brndx_sys_user_messages  AS M 
				WHERE
				M.CODE =  '$code' ";		
		return $sql;
	}
	public function validateDuplicateSerialNoWithSysNo($serialNo,$fieldName,$location)
	{
		$sql 	= "SELECT $fieldName as max_no FROM sys_no WHERE intCompanyId=$location";
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		
		if($serialNo>=$row['max_no'])
		{
			$sql 	= "UPDATE sys_no SET $fieldName=$fieldName+1 WHERE intCompanyId=$location";
			$result = $this->db->RunQuery2($sql);
			if(!$result)
			{
				$data['type'] 		= "fail" ;
				$data['msg'] 		= $this->db->errormsg;
			}
		}
		
		return $data;
	}
	
	public function getQpayEmployees($hrDB,$locationId,$empID)
	{
		$sql	= " SELECT
					QE.intEmployeeId,
					QE.strInitialName
					FROM
					$hrDB.mst_employee QE
					INNER JOIN $hrDB.mst_locations QML ON QML.intId=QE.intLocationId 
					INNER JOIN mst_locations NML ON NML.PAYROLL_LOCATION=QML.intId 
					WHERE
					QE.Active <> -10 AND 
					NML.intId = '$locationId' ";
		if($empID != '')
		$sql	.= " AND QE.intEmployeeId = '$empID'";
		
		$result	= $this->db->RunQuery($sql);
		return $result;
		
	
	}
	public function getSubEmailIds($reportId,$mainCluster,$subCluster,$company,$type)
	{
		
		if($company!=''){
			if($type=='TO'){
			$sql="SELECT
					GROUP_CONCAT(tb.`TO`) as ids
					FROM `sys_daily_reports_company_to` as tb
					WHERE
					tb.REPORT_ID = '$reportId' AND
					tb.COMPANY_ID = '$company'
					";
			}
			else if($type=='CC'){
				
			$sql="SELECT
					GROUP_CONCAT(tb.`CC`) as ids
					FROM `sys_daily_reports_company_cc` as tb
					WHERE
					tb.REPORT_ID = '$reportId' AND
					tb.COMPANY_ID = '$company'
					";
			}
			else if($type=='BCC'){
				
			$sql="SELECT
					GROUP_CONCAT(tb.`BCC`) as ids
					FROM `sys_daily_reports_company_bcc` as tb
					WHERE
					tb.REPORT_ID = '$reportId' AND
					tb.COMPANY_ID = '$company'
					";
			}
		}
		else if($subCluster!=''){
			if($type=='TO'){
			$sql="SELECT
					GROUP_CONCAT(tb.`TO`) as ids
					FROM `sys_daily_reports_sub_cluster_to` as tb
					WHERE
					tb.REPORT_ID = '$reportId' AND
					tb.SUB_CLUSTER = '$subCluster'
					";
			}
			else if($type=='CC'){
				
			$sql="SELECT
					GROUP_CONCAT(tb.`CC`) as ids
					FROM `sys_daily_reports_sub_cluster_cc` as tb
					WHERE
					tb.REPORT_ID = '$reportId' AND
					tb.SUB_CLUSTER = '$subCluster'
					";
			}
			else if($type=='BCC'){
				
			$sql="SELECT
					GROUP_CONCAT(tb.`BCC`) as ids
					FROM `sys_daily_reports_sub_cluster_bcc` as tb
					WHERE
					tb.REPORT_ID = '$reportId' AND
					tb.SUB_CLUSTER = '$subCluster'
					";
			}
		}
		
		
		else if($mainCluster!=''){
			
			if($type=='TO'){
			$sql="SELECT
					GROUP_CONCAT(tb.`TO`) as ids
					FROM `sys_daily_reports_main_cluster_to` as tb
					WHERE
					tb.REPORT_ID = '$reportId' AND
					tb.MAIN_CLUSTER = '$mainCluster'
					";
			}
			else if($type=='CC'){
				
			$sql="SELECT
					GROUP_CONCAT(tb.`CC`) as ids
					FROM `sys_daily_reports_main_cluster_cc` as tb
					WHERE
					tb.REPORT_ID = '$reportId' AND
					tb.MAIN_CLUSTER = '$mainCluster'
					";
			}
			else if($type=='BCC'){
				
			$sql="SELECT
					GROUP_CONCAT(tb.`BCC`) as ids
					FROM `sys_daily_reports_main_cluster_bcc` as tb
					WHERE
					tb.REPORT_ID = '$reportId' AND
					tb.MAIN_CLUSTER = '$mainCluster'
					";
			}
			
		}
		

		$result = $this->db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return $row['ids'];
		
	}
	
	public function getDailyMailsRecipients($reportId,$mainCluster,$subCluster,$company,$type)
	{
		
		if($company!=''){
			if($type=='TO'){
			$sql="SELECT
					GROUP_CONCAT(tb.`TO`) as ids
					FROM `sys_daily_reports_company_to` as tb
					WHERE
					tb.REPORT_ID = '$reportId' AND
					tb.COMPANY_ID = '$company'
					";
			}
			else if($type=='CC'){
				
			$sql="SELECT
					GROUP_CONCAT(tb.`CC`) as ids
					FROM `sys_daily_reports_company_cc` as tb
					WHERE
					tb.REPORT_ID = '$reportId' AND
					tb.COMPANY_ID = '$company'
					";
			}
			else if($type=='BCC'){
				
			$sql="SELECT
					GROUP_CONCAT(tb.`BCC`) as ids
					FROM `sys_daily_reports_company_bcc` as tb
					WHERE
					tb.REPORT_ID = '$reportId' AND
					tb.COMPANY_ID = '$company'
					";
			}
		}
		else if($subCluster!=''){

			$sql="SELECT
					GROUP_CONCAT(tb.`USER`) as ids
					FROM `sys_daily_reports_sub_cluster_emails_recipients` as tb
					WHERE
					tb.REPORT_ID = '$reportId' AND
					tb.SUB_CLUSTER = '$subCluster' AND
					tb.TYPE = '$type'
					";
		}
		
		
		else if($mainCluster!=''){
			
			$sql="SELECT
					GROUP_CONCAT(tb.`USER`) as ids
					FROM `sys_daily_reports_main_cluster_emails_recipients` as tb
					WHERE
					tb.REPORT_ID = '$reportId' AND
					tb.MAIN_CLUSTER = '$mainCluster' AND
					tb.TYPE = '$type'
					";
		}
		

		$result = $this->db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return $row['ids'];
		
	}
	
	
	public function getPlantEmailIds($reportId,$mainCluster,$subCluster,$company,$plant,$type)
	{

		$sql="SELECT
				GROUP_CONCAT(tb.`USER_ID`) as ids
				FROM `sys_daily_reports_plant_mails` as tb
				WHERE
				tb.REPORT_ID = '$reportId' AND
				tb.PLANT_ID = '$plant'  AND
				tb.MAIL_TYPE = '$type'
				";
		$result = $this->db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return $row['ids'];
		
	}
	
	public function getCronjobEmailIds($cronjobId,$type)
	{
			if($type=='TO'){
			$sql="SELECT
					GROUP_CONCAT(tb.`TO`) as ids
					FROM `sys_cronjob_emails_to` as tb
					WHERE
					tb.CRONJOB_ID = '$cronjobId'
					";
			}
			else if($type=='CC'){
				
			$sql="SELECT
					GROUP_CONCAT(tb.`CC`) as ids
					FROM `sys_cronjob_emails_cc` as tb
					WHERE
					tb.CRONJOB_ID = '$cronjobId'
					";
			}
			else if($type=='BCC'){
				
			$sql="SELECT
					GROUP_CONCAT(tb.`BCC`) as ids
					FROM `sys_cronjob_emails_bcc` as tb
					WHERE
					tb.CRONJOB_ID = '$cronjobId'
					";
			}
			
		$result = $this->db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return $row['ids'];
		
	}
	
	public function getCustomerOrders()
	{
		global $db ;
		
		$sql = 
		"SELECT
		trn_orderheader_dispatch_emails.ORDER_NO,
		trn_orderheader_dispatch_emails.ORDER_YEAR,
		GROUP_CONCAT( DISTINCT trn_orderheader_dispatch_emails.strBCC_EMAIL) AS bccEmail,
		GROUP_CONCAT( DISTINCT trn_orderheader_dispatch_emails.strCC_EMAIL) AS ccEmail,
		GROUP_CONCAT( DISTINCT trn_orderheader_dispatch_emails.strTO_EMAIL) AS toEmail,
		mst_customer.strName
		-- trn_orderheader_dispatch_emails.TYPE
		FROM
		trn_orderheader_dispatch_emails
		INNER JOIN
		ware_stocktransactions_fabric ON
		trn_orderheader_dispatch_emails.ORDER_NO = ware_stocktransactions_fabric.intOrderNo AND
		trn_orderheader_dispatch_emails.ORDER_YEAR = ware_stocktransactions_fabric.intOrderYear
		INNER JOIN 
		trn_orderheader ON trn_orderheader.intOrderNo = trn_orderheader_dispatch_emails.ORDER_NO AND
		trn_orderheader.intOrderYear = trn_orderheader_dispatch_emails.ORDER_YEAR
		INNER JOIN mst_customer ON mst_customer.intId = trn_orderheader.intCustomer
		WHERE
		 DATE(ware_stocktransactions_fabric.dtDate) = DATE(NOW()) - 1 AND
		ware_stocktransactions_fabric.strType  LIKE  '%Dispatch%' AND
		trn_orderheader_dispatch_emails.TYPE IN (1,2)
		GROUP BY
		ware_stocktransactions_fabric.intOrderNo";
		//echo $sql;
		$result = $this->db->RunQuery($sql);
		return $result;

	}

    public function getClustersReportToBeSent($reportId){
        $sql = "SELECT
					GROUP_CONCAT(DISTINCT tb.`MAIN_CLUSTER`) as clusterIds
					FROM `sys_daily_reports_main_cluster_emails_recipients` as tb
					WHERE
					tb.REPORT_ID = '$reportId'";
        $result = $this->db->RunQuery($sql);
        $row = mysqli_fetch_array($result);
        return $row['clusterIds'];

    }

    public function getMainClusterMailRecipients($reportId,$mainCluster,$type){
        $sqlC = "SELECT
					GROUP_CONCAT(tb.`USER`) as ids
					FROM `sys_daily_reports_main_cluster_emails_recipients` as tb
					WHERE
					tb.REPORT_ID = '$reportId' AND
					tb.MAIN_CLUSTER = '$mainCluster' AND
					tb.TYPE = '$type'
					";
        $result = $this->db->RunQuery($sqlC);
        $row = mysqli_fetch_array($result);
        return $row['ids'];

    }

    public function getClustersAndCorrespondingCompanies($clusterList)
    {

        $sql = "SELECT
                sys_main_clusters.ID AS clusterId,
                sys_main_clusters.COMPANY AS companyId,
                mst_companies.strName AS companyName
            FROM
                sys_main_clusters
            INNER JOIN mst_companies ON mst_companies.intId = sys_main_clusters.COMPANY
            WHERE
                mst_companies.intStatus = 1
            AND sys_main_clusters.ID IN ($clusterList)";

        $result = $this->db->RunQuery($sql);
        return $result;
    }
	
}
?>
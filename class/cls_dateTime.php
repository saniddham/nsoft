<?php
class cls_dateTime
{
	private $db;
	private $date_selected;
	
	
	function __construct($db)
	{
		$date_selected	=$this->getCurruntDate();
	}
	
	public function set($arr)
	{
		foreach ( $arr as $k=>$v )
		{
			$this->$k = $v;
		}
	}
	
	public function set_date($date)
	{
		$this->$date_selected	=$date;
 	}
	
	public function getCurruntDateTime()
	{
		return date('Y-m-d G:i:s');
  	}
	
	public function getCurruntTime()
	{
		return date('G:i:s');
  	}
	
	public function getCurruntTimeWithAMPM()
	{
		return date('g:i:sa');
  	}
	
	public function getCurruntDate()
	{
		return   date('Y-m-d');	
  	}
	
	public function getCurruntDay()
	{
		return date('d');	
 	}
	
	public function getCurruntMonth()
	{
		return	 date('m');	
 	}
	
	public function getCurruntYear()
	{
		return date('Y');	
 	}
	
	public function getYearOfDate(){
 		$year  = substr($this->date_selected, 0, 4);	
		return $year;	
	}
	
	public function getMonthOfDate(){
		$month = substr($this->date_selected, 5, 2);
		return $month;	
	}
	
	public function getDayOfDate(){
 		$day   = substr($this->date_selected, 8, 2);
		return $day;	
	}
	
	public function getMonthName()
	{
		$month		= $this->getMonthOfDate();
		$month_name = date("F", mktime(0, 0, 0, $month, 10));	
		return $month_name;
	}
	
	public function getPreviousDate()
	{
 		$val		= date('Y-m-d', strtotime('-1 day', strtotime($this->date_selected)));
		return $val;
	}
	
	public function getNextDate()
	{
 		$val		= date('Y-m-d', strtotime('+1 day', strtotime($this->date_selected)));
		return $val;
	}
	
	public function getDatesArrayForDateRange($first,$last)
	{
		$step = '+1 day';
		$format = 'Y-m-d' ;

		$dates = array();
		$current = strtotime($first);
		$last = strtotime($last);
	
		while( $current <= $last ) { 
			$dates[] = date($format, $current);
			$current = strtotime($step, $current);
		}	
			
		return $dates;
		
	}
	
	public function getDateName()
	{
 		$val		= date('l', strtotime($this->date_selected));
		return $val;
	}
	
	
	public function getDifferenceBetweenTwoDates($first,$last)
	{
		$first		=date_create($first);
		$last		=date_create($last);
		$diff  		= date_diff($first, $last);
	
		 return $diff->format("%a");	
	}
	
	public function getDateAfterNoOfMonths($aditionalMonths)
	{
		$date		= strtotime($this->date_selected);
		$d=strtotime($aditionalMonths." Months",$date);
		return date("Y-m-d", $d) ;
	}
	
	public function getDateAfterNoOfWeeks($aditionalWeeks)
	{
		$date		= strtotime($this->date_selected);
		$d=strtotime($aditionalWeeks." Weeks",$date);
		return date("Y-m-d", $d) ;
	}
	
	public function getDateAfterNoOfDays($aditionalDays)
	{
		$date		= strtotime($this->date_selected);
		$d=strtotime($aditionalDays." Days",$date);
		return date("Y-m-d", $d) ;
	}
	
}
?>
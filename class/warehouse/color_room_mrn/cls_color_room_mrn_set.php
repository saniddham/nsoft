<?php
include_once  ("../../../../class/cls_commonFunctions_get.php");
include_once ("../../../../class/finance/accountant/journalEntry/cls_journalEntry_get.php");
include_once  ("../../../../class/cls_commonErrorHandeling_get.php");
//ini_set('display_errors', 1); 

$obj_commonErr		= new cls_commonErrorHandeling_get($db);
$obj_common			= new cls_commonFunctions_get($db);
$obj_fin_journal_get	= new Cls_JournalEntry_Get($db);

$session_userId		= $_SESSION["userId"];
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];
$programCode		='P0726';

class Cls_color_room_mrn_Set
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	
	public function save_header($serialYear,$serialNo,$stores,$date,$note,$userId,$location,$approveLevels,$status,$executeType)
	{ 

   		$sql	= $this->save_header_sql($serialYear,$serialNo,$stores,$date,$note,$userId,$location,$approveLevels,$status);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		}
			return $response;
 	}
	
	public function update_header($serialYear,$serialNo,$stores,$date,$note,$userId,$location,$approveLevels,$status,$executeType)
	{ 

   		$sql	= $this->update_header_sql($serialYear,$serialNo,$stores,$date,$note,$userId,$location,$approveLevels,$status);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		}
			return $response;
 	}
	
	public function update_header_status($serialYear,$serialNo,$executeType)
	{ 

   		$sql	= $this->update_header_status_sql($serialYear,$serialNo);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		}
			return $response;
 	}
	
	public function approved_by_insert($serialYear,$serialNo,$userId,$approval,$executeType)
	{ 

   		$sql	= $this->approved_by_insert_sql($serialYear,$serialNo,$userId,$approval);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		}
			return $response;
 	}
	
	public function approved_by_update($serialYear,$serialNo,$maxAppByStatus,$executeType)
	{ 

   		$sql	= $this->approved_by_update_sql($serialYear,$serialNo,$maxAppByStatus);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		}
			return $response;
 	}

	public function delete_details($serialYear,$serialNo,$executeType)
	{ 

   		$sql	= $this->delete_details_sql($serialYear,$serialNo);
		$result = $this->db->$executeType($sql);
		/*if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{*/
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		/*}*/
			return $response;
 	}
	
	public function save_details($serialYear,$serialNo,$orderNo,$orderYear,$salesOrderNo,$item,$qty,$extraQty,$executeType)
	{ 

   		$sql	= $this->save_details_sql($serialYear,$serialNo,$orderNo,$orderYear,$salesOrderNo,$item,$qty,$extraQty);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		}
			return $response;
 	}
	
	
	private function save_header_sql($serialYear,$serialNo,$stores,$date,$note,$userId,$location,$approveLevels,$status)
	{
		
	 	$sql = "INSERT INTO ware_color_room_mrn_header 
				(MRN_NO, 
				MRN_YEAR,
				SUB_STORES,
 				STATUS, 
 				APPROVE_LEVELS, 
 				DATE, 
 				REMARKS, 
 				CREATED_DATE, 
 				CREATED_BY, 
				LOCATION_ID)
				VALUES
				('$serialNo', 
				'$serialYear',
				'$stores',
 				'$status',
 				'$approveLevels',
 				'$date',
 				'$note',
 				 now(),
				'$userId',
 				'$location');";
		return $sql;
	}
	
	private function update_header_sql($serialYear,$serialNo,$stores,$date,$note,$userId,$location,$approveLevels,$status)
	{
 		
		$sql = "UPDATE ware_color_room_mrn_header 
				SET 
 				SUB_STORES='$stores',
 				STATUS='$status', 
 				APPROVE_LEVELS='$approveLevels', 
 				DATE='$date', 
 				REMARKS='$note', 
  				MODIFIED_DATE=now(),
				MODIFIED_BY='$userId' 
				WHERE
				ware_color_room_mrn_header.MRN_NO = '$serialNo' AND
				ware_color_room_mrn_header.MRN_YEAR = '$serialYear'";
 		return $sql;
	}
	private function update_header_status_sql($serialYear,$serialNo)
	{
 		
		$sql = "UPDATE ware_color_room_mrn_header 
				SET 
				STATUS=STATUS-1 
				WHERE
				ware_color_room_mrn_header.MRN_NO = '$serialNo' AND
				ware_color_room_mrn_header.MRN_YEAR = '$serialYear'";
 		return $sql;
	}
	
	private function approved_by_insert_sql($serialYear,$serialNo,$userId,$approval){
 		
		$sql = "INSERT INTO `ware_color_room_mrn_approved_by` (`MRN_NO`,`MRN_YEAR`,`APPROVE_LEVEL`,APPROVED_BY,APPROVED_DAYE,STATUS) 
			VALUES ('$serialNo','$serialYear','$approval','$userId',now(),0)";
 		
		return $sql;
	}
	
	
	private function approved_by_update_sql($serialYear,$serialNo,$maxAppByStatus){
 		
		  	$sql = "UPDATE `ware_color_room_mrn_approved_by` SET STATUS ='$maxAppByStatus' 
					WHERE (`MRN_NO`='$serialNo') AND (`MRN_YEAR`='$serialYear') AND (`STATUS`='0')";

		return $sql;
	}
	
	
	private function delete_details_sql($serialYear,$serialNo)
	{
 		
		$sql	= "DELETE 
					FROM ware_color_room_mrn_details 
					WHERE
					ware_color_room_mrn_details.MRN_NO = '$serialNo' AND
					ware_color_room_mrn_details.MRN_YEAR = '$serialYear'";
 		return $sql;
	}
	
	private function save_details_sql($serialYear,$serialNo,$orderNo,$orderYear,$salesOrderNo,$item,$qty,$extraQty)
	{
		
	 	$sql	= "INSERT INTO ware_color_room_mrn_details 
					(MRN_NO, 
					MRN_YEAR, 
					ORDER_NO,
					ORDER_YEAR,
					SALES_ORDER_ID,
					ITEM_ID,
					QTY,
					EXTRA_QTY)
					VALUES
					('$serialNo', 
					'$serialYear', 
					'$orderNo',
					'$orderYear',
					'$salesOrderNo',
					'$item',
					'$qty',
					'$extraQty');";
		return $sql;
	}
	
 
 }
?>
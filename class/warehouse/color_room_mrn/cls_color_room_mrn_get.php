<?php
include_once  ("../../../../class/cls_commonFunctions_get.php");
include_once  ("../../../../class/cls_commonErrorHandeling_get.php");
include_once ("../../../../class/finance/cls_common_get.php");
include_once ("../../../../class/customerAndOperation/cls_textile_stores.php");
//ini_set('display_errors', 1); 
 
$obj_commonErr		= new cls_commonErrorHandeling_get($db);
$obj_common			= new cls_commonFunctions_get($db);
$obj_common_get		= new Cls_Common_Get($db);

$progrmCode			='P0726';

$session_userId		= $_SESSION["userId"];
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];

class Cls_color_room_mrn_Get
{
	private $db;
 	
	function __construct($db)
	{
		$this->db = $db;
 	}

//BEGIN - PUBLIC FUNCTIONS {	
	public function get_header_array($serialYear,$serialNo,$executeType)
	{
		
		$sql = $this->Load_header_sql($serialYear,$serialNo);
		$result = $this->db->$executeType($sql);
		$row = mysqli_fetch_array($result);
		return $row;
	}
	public function get_report_header_array($serialYear,$serialNo,$executeType)
	{
 		$sql = $this->Load_report_header_sql($serialYear,$serialNo);
		$result = $this->db->$executeType($sql);
		$row = mysqli_fetch_array($result);
		return $row;
	}
	public function get_report_details_array($serialYear,$serialNo,$executeType)
	{
		$sql = $this->Load_report_details_sql($serialYear,$serialNo);
		$result = $this->db->$executeType($sql);
		return $result;
	}
	public function get_Report_approval_details_result($serialYear,$serialNo,$executeType)
	{
		$sql = $this->Load_Report_approval_details_sql($serialYear,$serialNo);
		$result = $this->db->$executeType($sql);
		return $result;
	}
	public function get_details_result($serialYear,$serialNo,$executeType)
	{
		$sql = $this->Load_details_sql($serialYear,$serialNo);
		$result = $this->db->$executeType($sql);
		return $result;
	}
 	
	public function ValidateBeforeSave($serialYear,$serialNo,$executeType)
	{ 
		global $session_userId;
		global $obj_commonErr;	
		global $progrmCode;
		$this->db->begin();
   		$header_arr			= $this->get_header_array2($serialYear,$serialNo,$executeType);
 		$permision_arr		= $obj_commonErr->get_permision_withApproval_save($header_arr['STATUS'],$header_arr['LEVELS'],$session_userId,$progrmCode,$executeType);
		if($permision_arr['type'] =='fail'){
			$rollBack			=	1;
			$response['msg'] 	=	$permision_arr['msg'];	
			$response['type'] 	=	'fail';
			return $response;
		}
		$this->db->commit();
	}
	
	public function ValidateBeforeApprove($serialYear,$serialNo,$executeType)
	{ 
		global $session_userId;
		global $obj_commonErr;	
		global $progrmCode;
		$this->db->begin();
   		$header_arr			= $this->get_header_array2($serialYear,$serialNo,$executeType);
 		$permision_arr		= $obj_commonErr->get_permision_withApproval_confirm($header_arr['STATUS'],$header_arr['LEVELS'],$session_userId,$progrmCode,$executeType);
		$this->db->commit();
		if($permision_arr['type'] =='fail'){
			$rollBack			=	1;
			$response['msg'] 	=	$permision_arr['msg'];	
			$response['type'] 	=	'fail';
 			return $response;
		}
		
	}

	public function ValidateBeforeReject($serialYear,$serialNo,$executeType)
	{ 
		global $session_userId;
		global $obj_commonErr;	
		global $progrmCode;
		$this->db->begin();
   		$header_arr			= $this->get_header_array2($serialYear,$serialNo,$executeType);
 		$permision_arr		= $obj_commonErr->get_permision_withApproval_reject($header_arr['STATUS'],$header_arr['LEVELS'],$session_userId,$progrmCode,$executeType);
		if($permision_arr['type'] =='fail'){
			$rollBack			=	1;
			$response['msg'] 	=	$permision_arr['msg'];	
			$response['type'] 	=	'fail';
 			return $response;
		}
		$this->db->commit();
		
	}
	public function ValidateBeforeCancel($serialYear,$serialNo,$executeType)
	{ 
		global $session_userId;
		global $obj_commonErr;	
		global $progrmCode;
		$this->db->begin();
   		$header_arr			= $this->get_header_array2($serialYear,$serialNo,$executeType);
 		$permision_arr		= $obj_commonErr->get_permision_withApproval_cancel($header_arr['STATUS'],$header_arr['LEVELS'],$session_userId,$progrmCode,$executeType);
		if($permision_arr['type'] =='fail'){
			$rollBack			=	1;
			$response['msg'] 	=	$permision_arr['msg'];	
			$response['type'] 	=	'fail';
			return $response; 
 		}
		$this->db->commit();
		
	}
	
	public function get_issued_to_color_room_item_amount($orderNo,$orderYear,$salesOrderNo,$location,$item,$executeType){
		  global $db;
			$sql = $this->get_issued_to_color_room_item_amount_sql($orderNo,$orderYear,$salesOrderNo,$location,$item);
			$result = $db->$executeType($sql);
			$row=mysqli_fetch_array($result);
			return $row['issued'];
	}
	public function get_required_to_color_room_item_amount($orderNo,$orderYear,$salesOrderNo,$location,$item,$executeType){
		  global $db;
			$sql = $this->get_required_to_color_room_item_amount_sql($orderNo,$orderYear,$salesOrderNo,$location,$item);
			$result = $db->$executeType($sql);
			$row=mysqli_fetch_array($result);
			return $row['required'];
	}
	public function get_MaxAppByStatus($serialYear,$serialNo,$executeType){
		  global $db;
			$sql = $this->get_MaxAppByStatus_sql($serialYear,$serialNo);
			$result = $db->$executeType($sql);
			$row=mysqli_fetch_array($result);
			return $row['status'];
	}
	
 //END	- PUBLIC FUNCTIONS }

//BEGIN - PRIVATE FUNCTIONS {	
	private function Load_header_sql($serialYear,$serialNo)
	{
		global $session_companyId;
		$sql="SELECT
				ware_color_room_mrn_header.SUB_STORES,
				ware_color_room_mrn_header.`STATUS`,
				ware_color_room_mrn_header.APPROVE_LEVELS,
				ware_color_room_mrn_header.DATE,
				ware_color_room_mrn_header.REMARKS,
				ware_color_room_mrn_header.CREATED_DATE,
				ware_color_room_mrn_header.CREATED_BY,
				ware_color_room_mrn_header.LOCATION_ID,
				ware_color_room_mrn_header.MODIFIED_DATE,
				ware_color_room_mrn_header.MODIFIED_BY,
				mst_substores.strName AS SUB_STORES,
				mst_locations.strName AS LOCATION,
				mst_companies.strName AS COMPANY,
				sys_users.strUserName AS `USER`
				FROM
				ware_color_room_mrn_header
				INNER JOIN mst_substores ON ware_color_room_mrn_header.SUB_STORES = mst_substores.intId
				INNER JOIN mst_locations ON ware_color_room_mrn_header.LOCATION_ID = mst_locations.intId
				INNER JOIN mst_companies ON mst_locations.intCompanyId = mst_companies.intId
				INNER JOIN sys_users ON ware_color_room_mrn_header.CREATED_BY = sys_users.intUserId
				WHERE
				ware_color_room_mrn_header.MRN_NO = '$serialYear' AND
				ware_color_room_mrn_header.MRN_YEAR = '$serialNo'
				";
 		
  		return $sql;
 	}
	
	private function Load_details_sql($serialYear,$serialNo)
	{
		global $session_companyId;
		
  		return $sql;
	}
	private function Load_report_header_sql($serialYear,$serialNo)
	{
		global $session_companyId;
 		
  		return $sql;
 	}
	
	private function Load_report_details_sql($serialYear,$serialNo)
	{
		global $session_companyId;
		
  		return $sql;
	}

 	private function getTotAmmount_sql($serialYear,$serialNo,$type)
	{
 				
 		return $sql;
 	}
	private function Load_balanceToSetele_jernal_sql($serialYear,$serialNo,$orderId)
	{
		global $session_companyId;
		
  		return $sql;
	}
	private function Load_Report_approval_details_sql($serialYear,$serialNo){
		
 				
 		return $sql;
	}
	private function getPopupDetails_sql($currDate,$monthBeforDate)
	{
		global $session_companyId;
		
 		$result	= $this->db->RunQuery($sql);
		
		return $result;
	}
	private function loadJournalEntry_sql($entryNo,$entryYear)
	{
		global $obj_common_get;
		global $obj_GLData_get;
		
 		return $arrDetailData;
	}
	private function loadSearchData_sql($entryNo,$referenceNo,$currDate,$monthBeforDate)
	{
		global $session_companyId;
 		return $arrDetailData;
	}
	
	
	
	private function get_issued_to_color_room_item_amount_sql($orderNo,$orderYear,$salesOrderNo,$location,$item){
		$sql = "SELECT
				IFNULL(Sum(ware_sub_stocktransactions_bulk.dblQty),0) as issued
				FROM `ware_sub_stocktransactions_bulk`
				WHERE
				ware_sub_stocktransactions_bulk.strType = 'ISSUE' AND
				ware_sub_stocktransactions_bulk.intOrderNo = '$orderNo' AND
				ware_sub_stocktransactions_bulk.intOrderYear = '$orderYear' AND
				ware_sub_stocktransactions_bulk.intSalesOrderId = '$salesOrderNo' AND
				ware_sub_stocktransactions_bulk.intItemId = '$item' AND
				ware_sub_stocktransactions_bulk.intLocationId = '$location'
				";
		return $sql;
 	}
	
	private function get_required_to_color_room_item_amount_sql($orderNo,$orderYear,$salesOrderNo,$location,$item){
		$sql = "select 
				mst_item.strName as itemName,
				mst_item.intId, 
				mst_item.intMainCategory,
				mst_item.intSubCategory,
				mst_maincategory.strName as mainCatName,
				mst_subcategory.strName as subCatName,
				mst_item.strCode , 
				mst_units.strCode as uom ,
				trn_orderdetails.intQty as orderQty,
				IFNULL(consum,0)  as consum ,
				IFNULL(trn_orderdetails.intQty*consum,0)  as required
 						
						 from ( select 
						1 as type, 
						sc.intSampleNo, 
						sc.intSampleYear, 
						sc.intRevisionNo, 
						sc.strCombo, 
						sc.strPrintName,
						 mst_item.intId as intItem,
						 sum(round(dblColorWeight/((SELECT
						Sum(trn_sample_color_recipes.dblWeight)  
						FROM trn_sample_color_recipes
						WHERE
						trn_sample_color_recipes.intSampleNo = sc.intSampleNo AND
						trn_sample_color_recipes.intSampleYear = sc.intSampleYear AND
						trn_sample_color_recipes.intRevisionNo =  sc.intRevisionNo AND
						trn_sample_color_recipes.strCombo =  sc.strCombo AND
						trn_sample_color_recipes.strPrintName =  sc.strPrintName AND
						trn_sample_color_recipes.intTechniqueId =  sc.intTechniqueId AND
						trn_sample_color_recipes.intInkTypeId =  trn_sampleinfomations_details_technical.intInkTypeId AND
						trn_sample_color_recipes.intColorId =  trn_sampleinfomations_details_technical.intColorId 
						 ))*dblWeight /dblNoOfPcs,6))as consum 
						FROM
						trn_sample_color_recipes as sc
						Inner Join trn_sampleinfomations_details_technical ON trn_sampleinfomations_details_technical.intSampleNo = sc.intSampleNo 
						AND trn_sampleinfomations_details_technical.intSampleYear = sc.intSampleYear 
						AND trn_sampleinfomations_details_technical.intRevNo = sc.intRevisionNo 
						AND trn_sampleinfomations_details_technical.strComboName = sc.strCombo 
						AND trn_sampleinfomations_details_technical.strPrintName = sc.strPrintName 
						AND trn_sampleinfomations_details_technical.intColorId = sc.intColorId 
						AND trn_sampleinfomations_details_technical.intInkTypeId = sc.intInkTypeId
						Inner Join mst_item ON mst_item.intId = sc.intItem
						Inner Join mst_units ON mst_item.intUOM = mst_units.intId
						group by 
						sc.intSampleNo, 
						sc.intSampleYear, 
						sc.intRevisionNo, 
						sc.strCombo, 
						sc.strPrintName ,
						 mst_item.intId 
 				 ) as MAIN  
				
				
				INNER JOIN trn_orderdetails ON trn_orderdetails.intSampleNo=MAIN.intSampleNo AND 
				trn_orderdetails.intSampleYear=MAIN.intSampleYear AND 
				trn_orderdetails.intRevisionNo=MAIN.intRevisionNo AND 
				trn_orderdetails.strCombo=MAIN.strCombo AND 
				trn_orderdetails.strPrintName=MAIN.strPrintName  
				INNER JOIN mst_item ON mst_item.intId=MAIN.intItem 
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
				WHERE  
				trn_orderdetails.intOrderNo='$orderNo' AND 
				trn_orderdetails.intOrderYear='$orderYear' AND 
				trn_orderdetails.intSalesOrderId='$salesOrderNo' AND 
				MAIN.intItem = '$item' 
				";
		return $sql;
 	}
	
	private function get_MaxAppByStatus_sql($serialYear,$serialNo){
 		
	   $sql = "SELECT
				max(ware_color_room_mrn_approved_by.STATUS) as status 
				FROM
				ware_color_room_mrn_approved_by
				WHERE
				ware_color_room_mrn_approved_by.MRN_NO =  '$serialNo' AND
				ware_color_room_mrn_approved_by.MRN_YEAR =  '$serialYear'";
				
		return $sql;
				
  	}
	
}

?>
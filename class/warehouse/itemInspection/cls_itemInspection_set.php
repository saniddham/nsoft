<?php
date_default_timezone_set('Asia/Kolkata');
//ini_set('display_errors',1);//echo $_SESSION['ROOT_PATH'];
require_once $backwardseperator."class/cls_commonFunctions_get.php";
require_once $backwardseperator."class/warehouse/cls_warehouse_get.php";
require_once $backwardseperator."class/warehouse/cls_warehouse_set.php";
require_once $backwardseperator."class/warehouse/itemInspection/cls_itemInspection_get.php";


class cls_itemInspection_set
{
	
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	

	
	public function update_ware_grndetails($company,$location,$grnNo,$grnYear,$itemId,$goodQty,$dammagedQty,$remarks,$userId){
		
	 	$objcomfnget= new cls_commonFunctions_get($this->db);
		//$objwhouseget= new cls_warehouse_get($db);
		
		//$trnsQty =$objwhouseget->getStockInspectedGoodQty($grnNo,$grnYear,$itemId);
		$saveQty=$goodQty+$trnsQty;

	 	// $savedLevel = $objcomfnget->getApproveLevels('Item Inspection')+1;
		// $approveLevels= $objcomfnget->getApproveLevels('Item Inspection');
		 
	 	 $savedLevel = 2;
		 $approveLevels= 1;
		
	 	// $approveLevels = $this->getApproveLevel('Item Inspection')+1;
		  $sql = "UPDATE `ware_grndetails` SET intInspected ='$savedLevel', 
													  intInspectedAppLevels ='$approveLevels', 
													  intInspectedBy ='$userId', 
													  dblInspectedGoodQty ='$saveQty', 
													  dblInspectedDammagedQty ='$dammagedQty', 
													  strInspectionRemarks ='$remarks', 
													  dtmInspectedDate =now() 
				WHERE (`intGrnNo`='$grnNo') AND (`intGrnYear`='$grnYear') AND (`intItemId`='$itemId')";
				$result=$this->db->RunQuery2($sql);
				
				$data['result'] = $result;
				if($result==1){
				$data['type'] = 'pass';
				$data['q'] = '';
				$data['msg'] = '';
				}
				else{
				$data['type'] = 'fail';
				$data['q'] = $sql;
				$data['msg'] = $db->errormsg;
				}
				return $data;
	}
	
	public function confirmItemInspection($company,$location,$grnNo,$grnYear,$itemId,$userId){

		$objwhouseget= new cls_warehouse_get($this->db);
		$objwhouseset= new cls_warehouse_set($this->db);
		$objiteminspectget= new cls_itemInspection_get($this->db);
		
		$grnDate =$objwhouseget->getGRNDate($grnNo,$grnYear);
		$rate =$objwhouseget->getGRNRate($grnNo,$grnYear,$itemId);
		$currency =$objwhouseget->getGRNCurrency($grnNo,$grnYear,$itemId);
		$inspectGoodQty =$objwhouseget->getGRNinspectedGoodQty($grnNo,$grnYear,$itemId);
		$inspectDmgQty =$objwhouseget->getGRNinspectedDmgQty($grnNo,$grnYear,$itemId);
		//$trnsQty =$objwhouseget->getStockInspectedGoodQty($grnNo,$grnYear,$itemId);
	//	$saveQty =$inspectGoodQty-$trnsQty;
		$saveQty =$inspectGoodQty;
		$location =$objiteminspectget->getLocation($grnNo,$grnYear);
		$company =$objiteminspectget->getCompany($grnNo,$grnYear);
		$totInspectedQty=$inspectGoodQty+$inspectDmgQty;
		 
		$result1 = $objwhouseset->insert_ware_stocktransactions_bulk($company,$location,$grnNo,$grnYear,$grnNo,$grnYear,$grnDate,$rate,$currency,$itemId,$saveQty,'GRN',$userId);
		 
		if($result1){
		$sql = "UPDATE `ware_grndetails` SET  
										  intInspectionConfirmBy ='$userId', 
										  dblConfirmedInspectQty =IFNULL(dblConfirmedInspectQty,0)+'$totInspectedQty', 
										  dblInspectedGoodQty ='0', 
										  dblInspectedDammagedQty ='0', 
										  dtnInspectionConfirmDate =now() 
		WHERE (`intGrnNo`='$grnNo') AND (`intGrnYear`='$grnYear') AND (`intItemId`='$itemId')";
		$result=$this->db->RunQuery2($sql);
		 }
				
		$data['result'] = $result;
		if($result==1){
		$data['type'] = 'pass';
		$data['q'] = '';
		$data['msg'] = '';
		}
		else{
		$data['type'] = 'fail';
		$data['q'] = $sql;
		$data['msg'] = $db->errormsg.$sql;
		}
		return $data;
	}
	
//--------------------------------------------------------------
}
?>
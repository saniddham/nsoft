<?php
date_default_timezone_set('Asia/Kolkata');
// ini_set('display_errors',1);//$_SESSION['ROOT_PATH'].
//include $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";


class cls_itemInspection_get
{
	
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	
	public function getLocation($grnNo,$grnYear){
		$sql = "SELECT
				ware_grnheader.intCompanyId
				FROM ware_grnheader
				WHERE
				ware_grnheader.intGrnNo =  '$grnNo' AND
				ware_grnheader.intGrnYear =  '$grnYear'
				";
		$result = $this->db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		return $row['intCompanyId'];	
	}
	
	public function getCompany($grnNo,$grnYear){
		$sql = "SELECT
				mst_locations.intCompanyId
				FROM
				ware_grnheader
				Inner Join mst_locations ON ware_grnheader.intCompanyId = mst_locations.intId
				WHERE
				ware_grnheader.intGrnNo =  '$grnNo' AND
				ware_grnheader.intGrnYear =  '$grnYear'  
				";
		$result = $this->db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		return $row['intCompanyId'];	
	}
	
	public function getDetails($grnNo,$grnYear){
		
	//	$objcomfnget= new cls_commonFunctions_get($this->db);
		
		
		     $sql="SELECT DISTINCT
				mst_item.intMainCategory,
				mst_maincategory.strName as mainCatName,
				mst_item.intSubCategory,
				mst_subcategory.strName as subCatName,
				tb1.intItemId,
				mst_item.strCode,
                mst_item.strCode as SUP_ITEM_CODE,
				mst_item.strName as itemName,
				IFNULL(tb1.dblInspectedGoodQty,0) as goodQty,
				IFNULL(tb1.dblInspectedDammagedQty,0) as dammagedQty,
				IFNULL(tb1.dblGrnQty,0) as grnQty,  
				IFNULL(tb1.dblConfirmedInspectQty,0) as confQty,  
				IFNULL((select sum(dblQty) from ware_stocktransactions_bulk where ware_stocktransactions_bulk.strType='GRN' AND ware_stocktransactions_bulk.intDocumentNo=tb1.intGrnNo AND ware_stocktransactions_bulk.intDocumntYear=tb1.intGrnYear AND ware_stocktransactions_bulk.intItemId=tb1.intItemId ),0) as stockQty, 
				
				IFNULL(tb1.strInspectionRemarks,'') as remarks, 
				tb1.intInspected as inspectionStatus, 
				tb1.intInspectedAppLevels as approveLevels , 
				mst_units.strCode as uom 
				FROM
				ware_grndetails as tb1
				Inner Join mst_item ON tb1.intItemId = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
                                LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
				WHERE
				tb1.intInspectionItem =  '1' AND 
				tb1.intGrnNo =  '$grnNo' AND
				tb1.intGrnYear =  '$grnYear' 
				group by 
				tb1.intGrnNo, 
				tb1.intGrnYear, 
				tb1.intItemId 
				";
			 	
			$result = $this->db->RunQuery2($sql);
		while($row=mysqli_fetch_array($result))
		{
			$arrData[] = $row;
		}
		$response['arrData'] 	= $arrData;
		return $arrData;
	}
	
	
	
}
?>
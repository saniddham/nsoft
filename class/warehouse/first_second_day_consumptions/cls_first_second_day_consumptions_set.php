<?php
include_once  ("../../../../class/cls_commonFunctions_get.php");
include_once  ("../../../../class/cls_commonErrorHandeling_get.php");
//ini_set('display_errors', 1); 

$obj_commonErr		= new cls_commonErrorHandeling_get($db);
$obj_common			= new cls_commonFunctions_get($db);

$session_userId		= $_SESSION["userId"];
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];
$programCode		='P0829';

class cls_first_second_day_consumptions_set
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	
	public function save_header($year,$orderNo,$salesOrderNo,$type,$approveLevels,$status,$user,$executeType)
	{ 

   		$sql	= $this->save_header_sql($year,$orderNo,$salesOrderNo,$type,$approveLevels,$status,$user);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		}
			return $response;
 	}
	
	public function update_header($year,$orderNo,$salesOrderNo,$type,$approveLevels,$status,$user,$executeType)
	{ 

   		$sql	= $this->update_header_sql($year,$orderNo,$salesOrderNo,$type,$approveLevels,$status,$user);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		}
			return $response;
 	}
	
	public function updateHeaderStatus($year,$orderNo,$salesOrderNo,$type,$status,$executeType){
   		$sql	= $this->update_header_status_sql($year,$orderNo,$salesOrderNo,$type,$status);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		}
			return $response;
		
	}
	
	
	public function approved_by_insert($year,$orderNo,$salesOrderNo,$type,$userId,$approval,$executeType){
   		$sql	= $this->save_approved_by_header_sql($year,$orderNo,$salesOrderNo,$type,$userId,$approval);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		}
			return $response;
	}
	
	public function approved_by_update($year,$orderNo,$salesOrderNo,$type,$maxAppByStatus,$user,$executeType)
	{ 

   		$sql	= $this->load_update_approved_by_sql($year,$orderNo,$salesOrderNo,$type,$maxAppByStatus,$user);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		}
			return $response;
 	}

	public function delete_details($orderNo,$year,$salesOrder,$type,$executeType)
	{ 

   		$sql	= $this->delete_details_sql($orderNo,$year,$type,$salesOrder);
		$result = $this->db->$executeType($sql);
		/*if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{*/
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		/*}*/
			return $response;
 	}
	
	public function save_details($orderNo,$year,$salesOrder,$type,$item,$qty,$size,$executeType)
	{ 

   		$sql	= $this->save_details_sql($orderNo,$year,$salesOrder,$type,$item,$qty,$size);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		}
			return $response;
 	}
	
	
 	
	
	private function delete_details_sql($orderNo,$year,$type,$salesOrder)
	{
 		
		$sql	= "DELETE 
					FROM trn_sample_size_wise_item_consumptions 
					WHERE
					trn_sample_size_wise_item_consumptions.ORDER_NO = '$orderNo' AND
					trn_sample_size_wise_item_consumptions.ORDER_YEAR = '$year' AND
					trn_sample_size_wise_item_consumptions.SALES_ORDER_ID = '$salesOrder' AND
					trn_sample_size_wise_item_consumptions.CONSUMPTION_TYPE_ID = '$type'";
 		return $sql;
	}
	
	private function save_details_sql($orderNo,$year,$salesOrder,$type,$item,$qty,$size)
	{
		
	 	$sql	= "INSERT INTO trn_sample_size_wise_item_consumptions 
					(ORDER_NO, 
					ORDER_YEAR, 
					SALES_ORDER_ID,
					CONSUMPTION_TYPE_ID,
					ITEM_ID,
					SIZE,
					CONSUMPTION)
					VALUES
					('$orderNo', 
					'$year', 
					'$salesOrder',
					'$type',
					'$item',
					'$size',
					'$qty');";
		return $sql;
	}
	
	private function save_header_sql($year,$orderNo,$salesOrderNo,$type,$approveLevels,$status,$user){
	 
	 	$sql	= "INSERT INTO trn_sample_size_wise_item_consumptions_header 
					(ORDER_NO, 
					ORDER_YEAR, 
					SALES_ORDER_ID,
					CONSUMPTION_TYPE_ID,
					STATUS,
					LEVELS,
					SAVED_BY,
					SAVED_DATE)
					VALUES
					('$orderNo', 
					'$year', 
					'$salesOrderNo',
					'$type',
					'$status',
					'$approveLevels',
					'$user',
					now());";
		return $sql;
		
	}
	private function update_header_sql($year,$orderNo,$salesOrderNo,$type,$approveLevels,$status,$user){
			$sql = "UPDATE `trn_sample_size_wise_item_consumptions_header` 
			SET  
			STATUS ='$status',
			LEVELS ='$approveLevels',
			SAVED_BY ='$user',
			SAVED_DATE =now()
 					WHERE (`ORDER_YEAR`='$year') AND (`ORDER_NO`='$orderNo')  
					AND (`SALES_ORDER_ID`='$salesOrderNo') AND (`CONSUMPTION_TYPE_ID`='$type')";
		return $sql;
	}
	
	private function update_header_status_sql($year,$orderNo,$salesOrderNo,$type,$status){
			
		if($status=='') 
			$para='STATUS-1';
 		else
			$para=$status;
			$sql = 
			"UPDATE `trn_sample_size_wise_item_consumptions_header` 
			SET  
			STATUS=".$para." 
  					WHERE (`ORDER_YEAR`='$year') AND (`ORDER_NO`='$orderNo')  
					AND (`SALES_ORDER_ID`='$salesOrderNo') AND (`CONSUMPTION_TYPE_ID`='$type')";
		return $sql;
	}
	
 	private function load_update_approved_by_sql($year,$orderNo,$salesOrderNo,$type,$maxAppByStatus,$user){
		 	$sql = "UPDATE `trn_sample_size_wise_item_consumptions_approve_by` 
			SET  
			STATUS ='$maxAppByStatus' 
 					WHERE (`ORDER_YEAR`='$year') AND (`ORDER_NO`='$orderNo')  
					AND (`SALES_ORDER_ID`='$salesOrderNo') AND (`CONSUMPTION_TYPE_ID`='$type')";
		return $sql;
		
	}
	
	private function save_approved_by_header_sql($year,$orderNo,$salesOrderNo,$type,$userId,$approval){
		
	 	$sql	= "INSERT INTO trn_sample_size_wise_item_consumptions_approve_by 
					(ORDER_NO, 
					ORDER_YEAR, 
					SALES_ORDER_ID,
					CONSUMPTION_TYPE_ID,
					LEVELS,
					SAVED_BY,
					SAVED_DATE,
					STATUS)
					VALUES
					('$orderNo', 
					'$year', 
					'$salesOrderNo',
					'$type',
					'$approval',
					'$userId',
					now(),
					'0');";
		return $sql;
		
	}
 
 }
?>
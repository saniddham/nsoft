<?php
include_once  ("../../../class/cls_commonFunctions_get.php");
include_once  ("../../../class/cls_commonErrorHandeling_get.php");
include_once ("../../../class/finance/cls_common_get.php");
include_once ("../../../class/customerAndOperation/cls_textile_stores.php");
//ini_set('display_errors', 1); 
 
$obj_commonErr		= new cls_commonErrorHandeling_get($db);
$obj_common			= new cls_commonFunctions_get($db);
$obj_common_get		= new Cls_Common_Get($db);

$progrmCode			='P0726';

$session_userId		= $_SESSION["userId"];
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];

class cls_first_second_day_consumptions_get
{
	private $db;
 	
	function __construct($db)
	{
		$this->db = $db;
 	}

//BEGIN - PUBLIC FUNCTIONS {	
	public function get_header_array($order_year,$order_no,$sales_order,$type,$executeType)
	{
		
		$sql = $this->Load_header_sql($order_year,$order_no,$sales_order,$type);
		$result = $this->db->$executeType($sql);
		$row = mysqli_fetch_array($result);
		return $row;
	}
	
	public function get_saved_flag($order_year,$order_no,$sales_order,$type,$executeType)
	{
		
		$sql = $this->Load_header_sql($order_year,$order_no,$sales_order,$type);
		$result = $this->db->$executeType($sql);
		if( mysqli_num_rows($result)>0)
			$flag = 1;
			else
			$flag = 0;
		return $flag;
	}

	public function get_rpt_header_array($order_year,$order_no,$sales_order,$type,$executeType)
	{
		
		$sql = $this->Load_header_sql($order_year,$order_no,$sales_order,$type);
		$result = $this->db->$executeType($sql);
		$row = mysqli_fetch_array($result);
		return $row;
	}
	
	public function getMaxAppByStatus($order_year,$order_no,$sales_order,$type,$executeType)
	{
		
		$sql = $this->Load_max_approve_by_sql($order_year,$order_no,$sales_order,$type);
		$result = $this->db->$executeType($sql);
		$row = mysqli_fetch_array($result);
		return $row['status'];
	}
	
	public function get_details_result($order_year,$order_no,$sales_order,$type,$executeType)
	{
		$sql = $this->Load_details_sql($order_year,$order_no,$sales_order,$type);
		$result = $this->db->$executeType($sql);
		return $result;
	}
 	public function get_type_combo($value,$executeType)
	{
		$sql = $this->Load_types_sql();
		$result = $this->db->$executeType($sql);
		$option ='<option value=""></option>';
		while($row = mysqli_fetch_array($result)){
			if($value==$row['ID'])
				$option .="<option value=\"".$row['ID']."\" selected=\"selected\">".$row['DESCRIPTION']."</option>";
			else
				$option .="<option value=\"".$row['ID']."\">".$row['DESCRIPTION']."</option>";
		}
 
		return $option;
	}
	public function get_Report_approval_details_result($order_no,$order_year,$sales_order,$type){
		$sql = $this->Load_Report_approval_details_sql($order_no,$order_year,$sales_order,$type);
		$result = $this->db->RunQuery($sql);
		return $result;
	}
	
 	
	public function ValidateBeforeSave($serialYear,$serialNo,$executeType)
	{ 
		global $session_userId;
		global $obj_commonErr;	
		global $progrmCode;
		$this->db->begin();
   		$header_arr			= $this->get_header_array2($serialYear,$serialNo,$executeType);
 		$permision_arr		= $obj_commonErr->get_permision_withApproval_save($header_arr['STATUS'],$header_arr['LEVELS'],$session_userId,$progrmCode,$executeType);
		if($permision_arr['type'] =='fail'){
			$rollBack			=	1;
			$response['msg'] 	=	$permision_arr['msg'];	
			$response['type'] 	=	'fail';
			return $response;
		}
		$this->db->commit();
	}
	

  //END	- PUBLIC FUNCTIONS }

//BEGIN - PRIVATE FUNCTIONS {	
	private function Load_header_sql($order_year,$order_no,$sales_order,$type)
	{
		global $session_companyId;
		  $sql="SELECT trn_sample_size_wise_item_consumptions_header.ORDER_NO, trn_sample_size_wise_item_consumptions_header.ORDER_YEAR, 
				trn_sample_size_wise_item_consumptions_header.SALES_ORDER_ID, trn_orderdetails.strSalesOrderNo, trn_sample_size_wise_item_consumptions_header.CONSUMPTION_TYPE_ID,
				 mst_sample_consumption_types.DESCRIPTION as COMSUMPTION_TYPE, trn_sample_size_wise_item_consumptions_header.`STATUS`, 
				trn_sample_size_wise_item_consumptions_header.LEVELS, trn_sample_size_wise_item_consumptions_header.SAVED_BY, 
				DATE(trn_sample_size_wise_item_consumptions_header.SAVED_DATE) as SAVED_DATE, 
				sys_users.strFullName 
				FROM trn_sample_size_wise_item_consumptions_header 
				INNER JOIN sys_users ON trn_sample_size_wise_item_consumptions_header.SAVED_BY = sys_users.intUserId 
				INNER JOIN trn_orderdetails ON trn_sample_size_wise_item_consumptions_header.ORDER_NO = trn_orderdetails.intOrderNo 
				AND trn_sample_size_wise_item_consumptions_header.ORDER_YEAR = trn_orderdetails.intOrderYear 
				AND trn_sample_size_wise_item_consumptions_header.SALES_ORDER_ID = trn_orderdetails.intSalesOrderId 
				INNER JOIN mst_sample_consumption_types ON trn_sample_size_wise_item_consumptions_header.CONSUMPTION_TYPE_ID = mst_sample_consumption_types.ID 
				WHERE
				trn_sample_size_wise_item_consumptions_header.ORDER_NO = '$order_no' AND
				trn_sample_size_wise_item_consumptions_header.ORDER_YEAR = '$order_year' AND
				trn_sample_size_wise_item_consumptions_header.SALES_ORDER_ID = '$sales_order' AND
				trn_sample_size_wise_item_consumptions_header.CONSUMPTION_TYPE_ID = '$type'
				";
 		
  		return $sql;
 	}
	
	private function Load_details_sql($order_year,$order_no,$sales_order,$type)
	{
		global $session_companyId;
		
  		return $sql;
	}
	
	private function Load_types_sql(){
		
		$sql = 'SELECT
				mst_sample_consumption_types.DESCRIPTION,
				mst_sample_consumption_types.`STATUS`,
				mst_sample_consumption_types.ID
				FROM `mst_sample_consumption_types`
				WHERE
				mst_sample_consumption_types.`STATUS` = 1
				ORDER BY
				mst_sample_consumption_types.DESCRIPTION ASC';
		
		return $sql;
		
	}
	private function Load_max_approve_by_sql($order_year,$order_no,$sales_order,$type){
		$sql = "SELECT
				Max(trn_sample_size_wise_item_consumptions_approve_by.`STATUS`) status
				FROM `trn_sample_size_wise_item_consumptions_approve_by`
				WHERE
				trn_sample_size_wise_item_consumptions_approve_by.ORDER_NO = '$order_no' AND
				trn_sample_size_wise_item_consumptions_approve_by.ORDER_YEAR = '$order_year' AND
				trn_sample_size_wise_item_consumptions_approve_by.SALES_ORDER_ID = '$sales_order' AND
				trn_sample_size_wise_item_consumptions_approve_by.CONSUMPTION_TYPE_ID = '$type'";
		
		return $sql;
	
	}
	
	private function Load_Report_approval_details_sql($order_no,$order_year,$sales_order,$type){
		  $sql= "SELECT
				trn_sample_size_wise_item_consumptions_approve_by.`STATUS`,
				trn_sample_size_wise_item_consumptions_approve_by.ORDER_NO,
				trn_sample_size_wise_item_consumptions_approve_by.SAVED_BY,
				sys_users.strUserName as UserName ,
				trn_sample_size_wise_item_consumptions_approve_by.SAVED_DATE as dtApprovedDate,
				trn_sample_size_wise_item_consumptions_approve_by.LEVELS as intApproveLevelNo 
		
				FROM `trn_sample_size_wise_item_consumptions_approve_by`
				Inner Join sys_users ON trn_sample_size_wise_item_consumptions_approve_by.SAVED_BY = sys_users.intUserId
				WHERE
				trn_sample_size_wise_item_consumptions_approve_by.ORDER_NO = '$order_no' AND
				trn_sample_size_wise_item_consumptions_approve_by.ORDER_YEAR = '$order_year' AND
				trn_sample_size_wise_item_consumptions_approve_by.SALES_ORDER_ID = '$sales_order' AND
				trn_sample_size_wise_item_consumptions_approve_by.CONSUMPTION_TYPE_ID = '$type' 
				ORDER BY SAVED_DATE asc
				";
 		return $sql;
	}
 	
}

?>
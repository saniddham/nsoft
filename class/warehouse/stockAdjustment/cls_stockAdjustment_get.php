<?php

class cls_stockAdjustment_get
{
	
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	
	public function getDetails($location,$subCatId,$itemId){
		
		$subCat  = $_REQUEST['subCat'];
		$itemId  = $_REQUEST['itemId'];
		
		     $sql="SELECT
				round(Sum(ware_stocktransactions_bulk.dblQty),4) AS stockBal,
				mst_item.intMainCategory,
				mst_maincategory.strName as mainCatName,
				mst_item.intSubCategory,
				mst_subcategory.strName as subCatName,
				ware_stocktransactions_bulk.intGRNNo,
				ware_stocktransactions_bulk.intGRNYear,
				ware_stocktransactions_bulk.dtGRNDate,
				ware_stocktransactions_bulk.dblGRNRate, 
				ware_stocktransactions_bulk.intCurrencyId,
				ware_stocktransactions_bulk.intItemId, 
				mst_item.strName as itemName, 
				mst_item.strCode,
                mst_item.strCode as SUP_ITEM_CODE,
				mst_item.intUOM,
				mst_units.strCode as uom , 
				mst_financecurrency.strCode as currency 
				FROM
				ware_stocktransactions_bulk
				Inner Join mst_item ON ware_stocktransactions_bulk.intItemId = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
				Inner Join mst_financecurrency ON ware_stocktransactions_bulk.intCurrencyId = mst_financecurrency.intId
                                LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
				WHERE
				ware_stocktransactions_bulk.intLocationId =  '$location' ";
			if($itemId!=''){ 
			$sql .="  AND ware_stocktransactions_bulk.intItemId =  '$itemId' "; 
			}
			$sql .="  AND mst_item.intSubCategory =  '$subCatId'  "; 
			$sql .=" GROUP BY
				ware_stocktransactions_bulk.intItemId, 
				ware_stocktransactions_bulk.intGRNNo,
				ware_stocktransactions_bulk.intGRNYear,
				ware_stocktransactions_bulk.dtGRNDate,
				ware_stocktransactions_bulk.dblGRNRate";
				//echo $sql;
			$result = $this->db->RunQuery2($sql);
		while($row=mysqli_fetch_array($result))
		{
			$arrData[] = $row;
		}
		$response['arrData'] 	= $arrData;
		return $arrData;
	}
}
?>
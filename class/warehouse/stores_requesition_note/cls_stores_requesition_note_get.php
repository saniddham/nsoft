<?php

class cls_stores_requesition_note_get
{
	
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}

	public function get_order_years_options($year,$executeType)
	{
		
		$sql = $this->get_order_years_sql();
		$result = $this->db->$executeType($sql);
		$option ='<option value=""></option>';
		while($row = mysqli_fetch_array($result)){
			if($year==$row['intOrderYear'])
				$option .="<option value=\"".$row['intOrderYear']."\" selected=\"selected\">".$row['intOrderYear']."</option>";
			else
				$option .="<option value=\"".$row['intOrderYear']."\">".$row['intOrderYear']."</option>";
		}
 
		return $option;
	}


	public function get_graphics_options($year,$graphic_selected,$executeType)
	{
		$sql = $this->get_graphics_sql($year);
		$result = $this->db->$executeType($sql);
		$option ='<option value=""></option>';
		while($row = mysqli_fetch_array($result)){
 			if($graphic_selected==$row['strGraphicNo']) 
				$option .="<option value=\"".$row['strGraphicNo']."\" selected=\"selected\">".$row['strGraphicNo']."</option>";
			else
				$option .="<option value=\"".$row['strGraphicNo']."\">".$row['strGraphicNo']."</option>";
		}
 
		return $option;
	}


	public function get_grapic_order_color_items($year,$graphic,$executeType){
		
		$sql = $this->get_grapic_order_color_items_sql($year,$graphic);
		$result = $this->db->$executeType($sql);
		return $result;
	}
	

	public function other_stores_requisition_header_select($serialNo,$year,$executeType){
		
		$sql	=$this->other_stores_requisition_header_select_sql($serialNo,$year);
		$result = $this->db->$executeType($sql);
		$row 	= mysqli_fetch_array($result);
		return $row;
		
	}
	
	public function other_stores_requisition_details_result($serialNo,$serialYear,$executeType){

		$sql	=$this->other_stores_requisition_details_sql($serialNo,$serialYear);
		$result = $this->db->$executeType($sql);
		return $result;
		
	}
	
	public function get_graphic_wise_ink_item_maximum_srn_qty($company,$orderYear,$graphicNo,$item,$obj_warehouse_get,$obj_common,$item_type,$arr_perc,$executeType){
		  global 	$db;
		  $max_normal_qty	=0;
		  $max_excess_qty	=0;
		  $details_25		=0;
		  $details_100		=0;
		  
			$result_s	= $obj_warehouse_get->get_sales_orders_for_graphic($orderYear,$graphicNo,$item,$executeType);
			while($row_s=mysqli_fetch_array($result_s)){//sales order wise
			  
				$orderNo			= $row_s['intOrderNo'];
				$orderYear			= $row_s['intOrderYear'];	
				$salesOrder			= $row_s['intSalesOrderId'];
				
				$day_cons_ink_itm	= $obj_warehouse_get->get_cumulateve_ink_item_consumption($orderNo,$orderYear,$salesOrder,$item,$executeType);
				$samp_conPC			= $obj_warehouse_get->get_order_item_sample_consumption($orderNo,$orderYear,$salesOrder,$item,$executeType);
			
				if($day_cons_ink_itm > 0){
					$details_100	= $obj_warehouse_get->maximum_order_item_allocatable_details($orderNo,$orderYear,$salesOrder,$item,$item_type,$arr_perc['foil']['100'],$arr_perc['ink']['100'],$arr_perc['special']['100'],$executeType);
					$Qty_to_srn_100	+= (($obj_common->ceil_to_decimal_places($details_100['tot_qty_for_allocate'],2)) - ($obj_common->ceil_to_decimal_places($details_100['color_room_stock_bal'],2)));
				}
				else{
					$details_25		= $obj_warehouse_get->maximum_order_item_allocatable_details($orderNo,$orderYear,$salesOrder,$item,$item_type,$arr_perc['foil']['25'],$arr_perc['ink']['25'],$arr_perc['special']['25'],$executeType);
					//echo "+".$item."-".$Qty_to_srn_250	= (($obj_common->ceil_to_decimal_places($details_25['tot_qty_for_allocate'],2)) - ($obj_common->ceil_to_decimal_places($details_25['color_room_stock_bal'],2)));
					$Qty_to_srn_25	+= (($details_25['tot_qty_for_allocate']) - ($obj_common->ceil_to_decimal_places($details_25['color_room_stock_bal'],2)));
				}
			
		  }
		  
  		
			return $obj_common->ceil_to_decimal_places(($Qty_to_srn_100+$Qty_to_srn_25),2); 
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public function get_max_qty($graphics,$orderYear,$item,$company,$executeType){
		
		$flag_25		=0;
		$flag_100		=0;
		$tot			=0;
		$totTrnsaf		=0;
		foreach($ordersArray as $orderNo){

			$sql				= $this->get_sample_cons_sql($orderNo,$orderYear,$item);
			$result 			= $this->db->$executeType($sql);
			$row 				= mysqli_fetch_array($result);
			$sampCons 			= $row['consum'];
			
			$dayCons			= $this->get_production_ink_first_day_consumptions($orderNo,$orderYear,$item,$executeType);	
	
			$sql				= $this->get_order_qty($orderNo,$orderYear);	
 			$result 			= $this->db->$executeType($sql);
			$row 				= mysqli_fetch_array($result);
			$orderQty 			= $row['orderQty'];
			
 			
			$sql				= $this->get_auto_allocation_percentages($company);
 			$result 			= $this->db->$executeType($sql);
			$res 				= mysqli_fetch_array($result);
			
			$sql				= $this->get_confirmed_srn_qty($orderNo,$orderYear,$item);	
			$result 			= $this->db->$executeType($sql);
			$row 				= mysqli_fetch_array($result);
			$transferedQty		= $row['qty'];
			
			
			
			if($dayConsn > 0){
				$flag_100		=1;
				$maxQty			= $dayConsn*$orderQty*($res['FULL_ALLOCATION_%']/100)*(1+$res['INK_ITEM_EXTRA_SRN_%']/100);
					//echo "BB".$sampCons.'*'.$orderQty.'*'.($res['SAMPLE_ALLOCATION_%']/100).'*1+'.$res['INK_ITEM_EXTRA_SRN_%']/100;
			}
			else{
				$flag_25		=1;
				$maxQty			= $sampCons*$orderQty*($res['SAMPLE_ALLOCATION_%']/100)*(1+$res['INK_ITEM_EXTRA_SRN_%']/100);
				//if($item=='1287')
					//echo "KK".$orderNo.'*'.$sampCons.'*'.$orderQty.'*'.($res['SAMPLE_ALLOCATION_%']/100).'*1+'.$res['INK_ITEM_EXTRA_SRN_%']/100;
			}
			
			if($maxQty==''){
				$maxQty = 0;
			}
			if($transferedQty==''){
				$transferedQty = 0;
			}
			$tot 		+=$maxQty;
			$totTrnsaf	+=$transferedQty;
		}
		
		$data['flag_100']	=$flag_100;
		$data['flag_25']	=$flag_25;
		$data['maxQty']		=$tot-$totTrnsaf;
		
		return $data;
 		
	}

	public function get_25_MaxAppByStatus($serialNo,$year,$executeType){

		$sql				= $this->get_25_MaxAppByStatus_sql($serialNo,$year);
		$result 			= $this->db->$executeType($sql);
		$row 				= mysqli_fetch_array($result);
		$st		 			= $row['status'];
		return  $st;
	
	}
	public function get_100_MaxAppByStatus($serialNo,$year,$executeType){

		$sql				= $this->get_100_MaxAppByStatus_sql($serialNo,$year);
		$result 			= $this->db->$executeType($sql);
		$row 				= mysqli_fetch_array($result);
		$st		 			= $row['status'];
		return  $st;
	
	}
	public function get_excess_MaxAppByStatus($serialNo,$year,$executeType){

		$sql				= $this->get_excess_MaxAppByStatus_sql($serialNo,$year);
		$result 			= $this->db->$executeType($sql);
		$row 				= mysqli_fetch_array($result);
		$st		 			= $row['status'];
		return  $st;
	}
	
	public function get_extra_qty($srn_no,$srn_year,$item,$executeType){
	
		$sql				= $this->get_extra_qty_sql($srn_no,$srn_year,$item);
		$result 			= $this->db->$executeType($sql);
		$row 				= mysqli_fetch_array($result);
		$qty	 			= $row['qty'];
		return $qty;
	}

	public function get_permision_confirm($company,$program,$app_type,$user,$executeType){
	 
		$perm				= $this->get_permision_confirm_val($company,$program,$app_type,$user,$executeType);
 		return $perm;
	 
	}
	
	
	
	private function get_orders_sql($year,$graphics){
		
		$sql = "SELECT DISTINCT 
				trn_orderdetails.intOrderNo as no 
				FROM
				trn_orderdetails
				INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
				WHERE 
				trn_orderheader.intStatus = 1 ";
		//if($year!=''){		
		$sql .= "AND trn_orderdetails.intOrderYear = '$year' ";
		//}
		//if($graphics!=''){
		$sql .= "AND trn_orderdetails.strGraphicNo IN ($graphics) "; 
		//}
		$sql .= " ORDER BY trn_orderheader.intOrderNo ASC"; 
	//	echo $sql;		

  		return $sql;
 	}
	private function get_graphics_sql($year){
		
  		
		$sql .= "SELECT DISTINCT ";
 		$sql	.= " trn_orderdetails.strGraphicNo  
				FROM
				trn_orderdetails
				INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
				WHERE 
				trn_orderheader.intStatus = 1 ";
		//if($year!=''){		
		$sql .= " AND trn_orderdetails.intOrderYear = '$year' ";
 		$sql .= "ORDER BY strGraphicNo ASC ";
 		//echo $sql;		
 		return $sql;
 	}
	private function get_order_years_sql(){
		
		$sql = "SELECT DISTINCT
				trn_orderheader.intOrderYear
				FROM
				trn_orderdetails ,
				trn_orderheader
				WHERE
				trn_orderheader.intStatus = 1
				";
 		return $sql;
 	}
	
	private function get_grapic_order_color_items_sql($year,$graphic){
		
 		$sql = "select 
				type as itemType,
				mst_item.strName as itemName,
				mst_item.intId, 
				mst_item.intMainCategory,
				mst_item.intSubCategory,
				mst_maincategory.strName as mainCatName,
				mst_subcategory.strName as subCatName,
				mst_item.strCode , 
				mst_units.strCode as uom ,
				sum(trn_orderdetails.intQty) as orderQty,
				IFNULL(consum,0)  as consum 
 						
						 from ( select 
						1 as type, 
						sc.intSampleNo, 
						sc.intSampleYear, 
						sc.intRevisionNo, 
						sc.strCombo, 
						sc.strPrintName,
						 mst_item.intId as intItem,
						 sum(round(dblColorWeight/((SELECT
						Sum(trn_sample_color_recipes.dblWeight)  
						FROM trn_sample_color_recipes
						WHERE
						trn_sample_color_recipes.intSampleNo = sc.intSampleNo AND
						trn_sample_color_recipes.intSampleYear = sc.intSampleYear AND
						trn_sample_color_recipes.intRevisionNo =  sc.intRevisionNo AND
						trn_sample_color_recipes.strCombo =  sc.strCombo AND
						trn_sample_color_recipes.strPrintName =  sc.strPrintName AND
						trn_sample_color_recipes.intTechniqueId =  sc.intTechniqueId AND
						trn_sample_color_recipes.intInkTypeId =  trn_sampleinfomations_details_technical.intInkTypeId AND
						trn_sample_color_recipes.intColorId =  trn_sampleinfomations_details_technical.intColorId 
						 ))*dblWeight /dblNoOfPcs,6))as consum 
						FROM
						trn_sample_color_recipes as sc
						Inner Join trn_sampleinfomations_details_technical ON trn_sampleinfomations_details_technical.intSampleNo = sc.intSampleNo 
						AND trn_sampleinfomations_details_technical.intSampleYear = sc.intSampleYear 
						AND trn_sampleinfomations_details_technical.intRevNo = sc.intRevisionNo 
						AND trn_sampleinfomations_details_technical.strComboName = sc.strCombo 
						AND trn_sampleinfomations_details_technical.strPrintName = sc.strPrintName 
						AND trn_sampleinfomations_details_technical.intColorId = sc.intColorId 
						AND trn_sampleinfomations_details_technical.intInkTypeId = sc.intInkTypeId
						Inner Join mst_item ON mst_item.intId = sc.intItem
						Inner Join mst_units ON mst_item.intUOM = mst_units.intId
						group by 
						sc.intSampleNo, 
						sc.intSampleYear, 
						sc.intRevisionNo, 
						sc.strCombo, 
						sc.strPrintName ,
						 mst_item.intId 
 				 ) as MAIN  
				
				
				INNER JOIN trn_orderdetails ON trn_orderdetails.intSampleNo=MAIN.intSampleNo AND 
				trn_orderdetails.intSampleYear=MAIN.intSampleYear AND 
				trn_orderdetails.intRevisionNo=MAIN.intRevisionNo AND 
				trn_orderdetails.strCombo=MAIN.strCombo AND 
				trn_orderdetails.strPrintName=MAIN.strPrintName  
				INNER JOIN mst_item ON mst_item.intId=MAIN.intItem 
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
				WHERE  
 				mst_item.intStatus='1'"; 
				$sql.=" AND trn_orderdetails.intOrderYear = '$year'";
 				if($graphic!=''){
				$sql.=" AND  trn_orderdetails.strGraphicNo  = '$graphic'";
				}
				$sql.=" GROUP BY mst_item.intId ";
				$sql.=" Order by mst_maincategory.strName asc, mst_subcategory.strName asc, mst_item.strName asc";
				//echo $sql;
				return $sql;
		
	}
	
	private function total_ink_allocated_item_weight($orderNo,$orderYear,$item){
		
		$sql	="SELECT
		(ware_sub_stocktransactions_bulk.dblQty*-1) as itemWeight
		FROM
		ware_sub_stocktransactions_bulk
		WHERE
		ware_sub_stocktransactions_bulk.intOrderNo = '$orderNo' AND
		ware_sub_stocktransactions_bulk.intOrderYear = '$orderYear' AND
 		ware_sub_stocktransactions_bulk.intItemId = '$item' AND
		ware_sub_stocktransactions_bulk.strType = 'INK_ALLOC'	";	
	}
	private function total_ink_allocated_ink_weight($orderNo,$orderYear,$ink_list){
		
		$sql	="SELECT
		(ware_sub_stocktransactions_bulk.dblQty*-1) as inkWeight
		FROM
		ware_sub_stocktransactions_bulk
		WHERE
		ware_sub_stocktransactions_bulk.intOrderNo = '$orderNo' AND
		ware_sub_stocktransactions_bulk.intOrderYear = '$orderYear' AND
  		ware_sub_stocktransactions_bulk.strType = 'INK_ALLOC' AND 
		concat(ware_sub_stocktransactions_bulk.intColorId,'/',ware_sub_stocktransactions_bulk.intTechnique,'/',ware_sub_stocktransactions_bulk.intInkType) IN ($ink_list)
	";	
	}
	private function total_first_day_production_allocated_ink_weight($orderNo,$orderYear,$ink_list){
		
		$sql	="SELECT
				trn_inktype_production_allocation.dblOrderQty,
				trn_inktype_production_allocation.dblIssueQty,
				trn_inktype_production_allocation.dblReturnedQty,
				trn_inktype_production_allocation.dblWastageQty,
				((trn_inktype_production_allocation.dblIssueQty-trn_inktype_production_allocation.dblReturnedQty)/trn_inktype_production_allocation.dblOrderQty) as producWeight 
				FROM `trn_inktype_production_allocation`
				WHERE
				trn_inktype_production_allocation.intOrderYear = '$orderYear' AND
				trn_inktype_production_allocation.intOrderNo = '$orderNo'  AND 
		concat(ware_sub_stocktransactions_bulk.intColorId,'/',ware_sub_stocktransactions_bulk.intTechnique,'/',ware_sub_stocktransactions_bulk.intInkType) IN ($ink_list)
	 limit 1";	
	}
	
	private function other_stores_requisition_header_select_sql($serialNo,$year){
		
		$sql ="SELECT
			ware_storesrequesitionheader.intRequisitionNo,
			ware_storesrequesitionheader.intRequisitionYear,
			ware_storesrequesitionheader.intReqToStores,
			ware_storesrequesitionheader.intReqFromStores,
			ware_storesrequesitionheader.strGraphicNo,
			ware_storesrequesitionheader.intOrderYear, 
			ware_storesrequesitionheader.strNote,
			ware_storesrequesitionheader.intStatus,
			ware_storesrequesitionheader.intApproveLevels,
			ware_storesrequesitionheader.dtDate,
			ware_storesrequesitionheader.dtmCreateDate,
			ware_storesrequesitionheader.intUser,
			ware_storesrequesitionheader.intLocationId,
			ware_storesrequesitionheader.dtmModifiedDate,
			ware_storesrequesitionheader.intModifiedBy
			FROM `ware_storesrequesitionheader`
			WHERE
			ware_storesrequesitionheader.intRequisitionNo = '$serialNo' AND
			ware_storesrequesitionheader.intRequisitionYear = '$year'
			";
			
			return $sql;
	}
	
	private function get_item_ink_list_sql($orderNo,$orderYear,$item){
		$sql	= "SELECT
			concat(ware_sub_stocktransactions_bulk.intColorId,'/',
			ware_sub_stocktransactions_bulk.intTechnique,'/',
			ware_sub_stocktransactions_bulk.intInkType) as inkList
			FROM `ware_sub_stocktransactions_bulk`
			WHERE
			ware_sub_stocktransactions_bulk.intOrderNo = '$orderNo' AND
			ware_sub_stocktransactions_bulk.intOrderYear = '$orderYear' AND
			ware_sub_stocktransactions_bulk.intItemId = '$item'";
			return $sql;
		
	}
	
	private function get_sample_cons_sql($orderNo,$orderYear,$item){
 	
 		$sql = "select 
				mst_item.strName as itemName,
				mst_item.intId, 
				mst_item.intMainCategory,
				mst_item.intSubCategory,
				mst_maincategory.strName as mainCatName,
				mst_subcategory.strName as subCatName,
				mst_item.strCode , 
				mst_units.strCode as uom ,
				trn_orderdetails.intQty as orderQty,
				IFNULL(consum,0)  as consum ,
				IFNULL(trn_orderdetails.intQty*consum,0)  as required
 						
						 from ( select 
						1 as type, 
						sc.intSampleNo, 
						sc.intSampleYear, 
						sc.intRevisionNo, 
						sc.strCombo, 
						sc.strPrintName,
						 mst_item.intId as intItem,
						 sum(round(dblColorWeight/((SELECT
						Sum(trn_sample_color_recipes.dblWeight)  
						FROM trn_sample_color_recipes
						WHERE
						trn_sample_color_recipes.intSampleNo = sc.intSampleNo AND
						trn_sample_color_recipes.intSampleYear = sc.intSampleYear AND
						trn_sample_color_recipes.intRevisionNo =  sc.intRevisionNo AND
						trn_sample_color_recipes.strCombo =  sc.strCombo AND
						trn_sample_color_recipes.strPrintName =  sc.strPrintName AND
						trn_sample_color_recipes.intTechniqueId =  sc.intTechniqueId AND
						trn_sample_color_recipes.intInkTypeId =  trn_sampleinfomations_details_technical.intInkTypeId AND
						trn_sample_color_recipes.intColorId =  trn_sampleinfomations_details_technical.intColorId 
						 ))*dblWeight /dblNoOfPcs,6))as consum 
						FROM
						trn_sample_color_recipes as sc
						Inner Join trn_sampleinfomations_details_technical ON trn_sampleinfomations_details_technical.intSampleNo = sc.intSampleNo 
						AND trn_sampleinfomations_details_technical.intSampleYear = sc.intSampleYear 
						AND trn_sampleinfomations_details_technical.intRevNo = sc.intRevisionNo 
						AND trn_sampleinfomations_details_technical.strComboName = sc.strCombo 
						AND trn_sampleinfomations_details_technical.strPrintName = sc.strPrintName 
						AND trn_sampleinfomations_details_technical.intColorId = sc.intColorId 
						AND trn_sampleinfomations_details_technical.intInkTypeId = sc.intInkTypeId
						Inner Join mst_item ON mst_item.intId = sc.intItem
						Inner Join mst_units ON mst_item.intUOM = mst_units.intId
						group by 
						sc.intSampleNo, 
						sc.intSampleYear, 
						sc.intRevisionNo, 
						sc.strCombo, 
						sc.strPrintName ,
						 mst_item.intId 
 				 ) as MAIN  
				
				
				INNER JOIN trn_orderdetails ON trn_orderdetails.intSampleNo=MAIN.intSampleNo AND 
				trn_orderdetails.intSampleYear=MAIN.intSampleYear AND 
				trn_orderdetails.intRevisionNo=MAIN.intRevisionNo AND 
				trn_orderdetails.strCombo=MAIN.strCombo AND 
				trn_orderdetails.strPrintName=MAIN.strPrintName  
				INNER JOIN mst_item ON mst_item.intId=MAIN.intItem 
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
				WHERE  
				trn_orderdetails.intOrderNo='$orderNo' AND 
				trn_orderdetails.intOrderYear='$orderYear' AND 
 				mst_item.intId =  '$item'";
				//echo $sql;
				return $sql;
		
	}
	
	private function get_order_qty($orderNo,$orderYear){
		
		$sql = "SELECT
				Sum(trn_orderdetails.intQty) as orderQty  
				FROM `trn_orderdetails`
				WHERE
				trn_orderdetails.intOrderNo = '$orderNo' AND
				trn_orderdetails.intOrderYear = '$orderYear'";
		
		return $sql;

		
	}
	
	private function get_auto_allocation_percentages($company){
			
			$sql = "SELECT
			sys_company_item_allocation_percentages.`SAMPLE_ALLOCATION_%`,
			sys_company_item_allocation_percentages.`FULL_ALLOCATION_%`,
			sys_company_item_allocation_percentages.`FOIL_EXTRA_ALLOCATION_%`,
			sys_company_item_allocation_percentages.`SP_RM_EXTRA_ALLOCATION_%`,
			sys_company_item_allocation_percentages.`INK_ITEM_EXTRA_ALLOCATION_%`,
			sys_company_item_allocation_percentages.`INK_ITEM_EXTRA_SRN_%` 
			FROM `sys_company_item_allocation_percentages`
			WHERE
			sys_company_item_allocation_percentages.COMPANY_ID = '$company'";
			
			return $sql;
 	
 }
 
 public function get_confirmed_srn_qty($orderNo,$orderYear,$item){
	 
	 $sql	="SELECT
			Sum(ware_storesrequesitiondetails.dblQty) as qty 
			FROM
			ware_storesrequesitionheader
			INNER JOIN ware_storesrequesitiondetails ON ware_storesrequesitionheader.intRequisitionNo = ware_storesrequesitiondetails.intRequisitionNo AND ware_storesrequesitionheader.intRequisitionYear = ware_storesrequesitiondetails.intRequisitionYear
			WHERE
			ware_storesrequesitionheader.intStatus = 1 AND
			ware_storesrequesitionheader.intOrderYear = '$orderYear' AND
			FIND_IN_SET('$orderNo' ,ware_storesrequesitionheader.strGraphicNo) AND 
			ware_storesrequesitiondetails.intItemId = '$item'";
	 return $sql;
 }
 
 
 private function other_stores_requisition_details_sql($serialNo,$year){
	 
			$sql = "SELECT
					mst_maincategory.strName as mainCat,
					mst_subcategory.strName as subCat,
					ware_storesrequesitiondetails.intItemId, 
					mst_item.strName as item,
					mst_units.strCode as uom, 
					ware_storesrequesitiondetails.dblQty,
					ware_storesrequesitiondetails.dblExQty
					FROM
					ware_storesrequesitiondetails
					INNER JOIN mst_item ON ware_storesrequesitiondetails.intItemId = mst_item.intId
					INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
					INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
					INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
					WHERE
					ware_storesrequesitiondetails.intRequisitionNo = '$serialNo' AND
					ware_storesrequesitiondetails.intRequisitionYear = '$year'
					ORDER BY
					mst_item.strName ASC";
			
			return $sql;
	 
 }
 
	 private function get_25_MaxAppByStatus_sql($serialNo,$year){
			$sql = "SELECT
					max(ware_srn_25_exceeding_approval.intStatus) as status 
					FROM
					ware_srn_25_exceeding_approval
					WHERE
					ware_srn_25_exceeding_approval.intRequisitionNo =  '$serialNo' AND
					ware_srn_25_exceeding_approval.intYear =  '$year'";
			return $sql;	
	}
						
	private function get_100_MaxAppByStatus_sql($serialNo,$year){
			$sql = "SELECT
					max(ware_srn_100_exceeding_approval.intStatus) as status 
					FROM
					ware_srn_100_exceeding_approval
					WHERE
					ware_srn_100_exceeding_approval.intRequisitionNo =  '$serialNo' AND
					ware_srn_100_exceeding_approval.intYear =  '$year'";
			return $sql;	
	}
	 
	private function get_excess_MaxAppByStatus_sql($serialNo,$year){
			$sql = "SELECT
					max(ware_srn_excess_approval.intStatus) as status 
					FROM
					ware_srn_excess_approval
					WHERE
					ware_srn_excess_approval.intRequisitionNo =  '$serialNo' AND
					ware_srn_excess_approval.intYear =  '$year'";
			return $sql;	
	}
	
	private function get_extra_qty_sql($srn_no,$srn_year,$item){
	
			$sql = "SELECT
					IFNULL(sum(ware_storesrequesitiondetails_sales_order_wise.dblExQty),0) as qty
					FROM `ware_storesrequesitiondetails_sales_order_wise`
					WHERE
					ware_storesrequesitiondetails_sales_order_wise.intRequisitionNo = '$srn_no' AND
					ware_storesrequesitiondetails_sales_order_wise.intRequisitionYear = '$srn_year' AND
					ware_storesrequesitiondetails_sales_order_wise.intItemId = '$item'";
			return $sql;	
		
	}
	
		private function get_permision_confirm_val($companyId,$programCode,$app_type,$user,$executeType){
		
 		$sql = "select * from( (
				SELECT DISTINCT 
					1 as approval ,
					menupermision.intUserId,
					sys_users.strFullName
				FROM
					menus
					Inner Join menupermision ON menupermision.intMenuId = menus.intId
					Inner Join sys_users ON sys_users.intUserId = menupermision.intUserId
					Inner Join mst_locations_user ON mst_locations_user.intUserId = sys_users.intUserId
					Inner Join mst_locations ON mst_locations.intId = mst_locations_user.intLocationId
				WHERE
					mst_locations.intCompanyId 	=  '$companyId' AND
					menus.strCode				=  '$programCode' AND 
					menupermision.int1Approval 		=  '1' AND   
					sys_users.intStatus			= 1
					)
					UNION 
					(
				SELECT DISTINCT 
					2 as approval ,
					menupermision.intUserId,
					sys_users.strFullName
				FROM
					menus
					Inner Join menupermision ON menupermision.intMenuId = menus.intId
					Inner Join sys_users ON sys_users.intUserId = menupermision.intUserId
					Inner Join mst_locations_user ON mst_locations_user.intUserId = sys_users.intUserId
					Inner Join mst_locations ON mst_locations.intId = mst_locations_user.intLocationId
				WHERE
					mst_locations.intCompanyId 	=  '$companyId' AND
					menus.strCode				=  '$programCode' AND 
					menupermision.int2Approval 		=  '1' AND   
					sys_users.intStatus			= 1
					)
					UNION 
					(
				SELECT DISTINCT 
					3 as approval ,
					menupermision.intUserId,
					sys_users.strFullName
				FROM
					menus
					Inner Join menupermision ON menupermision.intMenuId = menus.intId
					Inner Join sys_users ON sys_users.intUserId = menupermision.intUserId
					Inner Join mst_locations_user ON mst_locations_user.intUserId = sys_users.intUserId
					Inner Join mst_locations ON mst_locations.intId = mst_locations_user.intLocationId
				WHERE
					mst_locations.intCompanyId 	=  '$companyId' AND
					menus.strCode				=  '$programCode' AND 
					menupermision.int3Approval 		=  '1' AND  
					sys_users.intStatus			= 1
					)
					UNION 
					(
				SELECT DISTINCT 
					4 as approval ,
					menupermision.intUserId,
					sys_users.strFullName
				FROM
					menus
					Inner Join menupermision ON menupermision.intMenuId = menus.intId
					Inner Join sys_users ON sys_users.intUserId = menupermision.intUserId
					Inner Join mst_locations_user ON mst_locations_user.intUserId = sys_users.intUserId
					Inner Join mst_locations ON mst_locations.intId = mst_locations_user.intLocationId
				WHERE
					mst_locations.intCompanyId 	=  '$companyId' AND
					menus.strCode				=  '$programCode' AND 
					menupermision.int4Approval 		=  '1' AND  
					sys_users.intStatus			= 1
					)
					) as tb where  approval >= $app_type and intUserId = '$user'
					
				";
		//echo $sql;
		$result = $this->db->$executeType($sql);
		$row = mysqli_fetch_array($result);
		if($row['approval'] != ''){
		return 1;
		}
		else{
		return 0;
		}
		
	}

 
 
}
?>
<?php

class cls_stores_requesition_note_set
{
	
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}

 	
	public function other_stores_requisition_header_update($serialNo,$year,$status,$levels,$srnFrom,$srnTo,$orderYear,$orders_list,$date,$userId,$location,$executeType)
	{
 		$sql = "UPDATE ware_storesrequesitionheader 
				SET
				intReqToStores = '$srnTo' , 
				intReqFromStores = '$srnFrom' , 
				intOrderYear = '$orderYear' , 
 				strGraphicNo ='$orders_list',
				strNote = '$note' , 
				intStatus = $levels+1 , 
				intApproveLevels = $levels , 
				dtDate = '$date' , 
				intUser = '$userId' , 
				intLocationId = '$location' , 
				intModifiedBy = '$userId' , 
				dtmModifiedDate = now()
				WHERE
				intRequisitionNo = '$serialNo' AND 
				intRequisitionYear = '$year' ";
		$result = $this->db->$executeType($sql);
		if(!$result)
		{
			$response['type']	= 'fail' ;
			$response['msg']	= $db->errormsg;
			$response['sql']	= $sql;
		}
		else
		{
			$response['type']	= 'pass' ;
			$response['msg']	= $db->errormsg;
			$response['sql']	= $sql;
		}
		return $response;
	}
	
	public function other_stores_requisition_header_insert($serialNo,$year,$status,$levels,$srnFrom,$srnTo,$orderYear,$orders_list,$date,$userId,$location,$executeType)
	{
		$sql = "INSERT INTO ware_storesrequesitionheader 
				(
				intRequisitionNo, intRequisitionYear, intReqToStores, 
				intReqFromStores,intOrderYear,strGraphicNo, strNote, intStatus, 
				intApproveLevels, dtDate, dtmCreateDate, 
				intUser, intLocationId
				)
				VALUES
				(
				'$serialNo', '$year', '$srnTo', 
				'$srnFrom','$orderYear','$orders_list', '$note', $levels+1, 
				$levels, '$date', now(), 
				'$userId', '$location'
				);";
		$result = $this->db->$executeType($sql);
		if(!$result)
		{
			$response['type']	= 'fail' ;
			$response['msg']	= $db->errormsg;
			$response['sql']	= $sql;
		}
		else
		{
			$response['type']	= 'pass' ;
			$response['msg']	= $db->errormsg;
			$response['sql']	= $sql;
		}
		return $response;
	}
	
	public function other_stores_requisition_details_delete($serialNo,$year,$executeType)
	{
		$sql = "DELETE FROM ware_storesrequesitiondetails 
						WHERE
						intRequisitionNo = '$serialNo' AND 
						intRequisitionYear = '$year' ";
		$result = $this->db->$executeType($sql);
		if(!$result)
		{
			$response['type']	= 'fail' ;
			$response['msg']	= $db->errormsg;
			$response['sql']	= $sql;
		}
		else
		{
			$response['type']	= 'pass' ;
			$response['msg']	= $db->errormsg;
			$response['sql']	= $sql;
		}
		return $response;
	}

	public function other_stores_requisition_details_sales_order_wise_delete($serialNo,$year,$executeType)
	{
		$sql = "DELETE FROM ware_storesrequesitiondetails_sales_order_wise 
						WHERE
						intRequisitionNo = '$serialNo' AND 
						intRequisitionYear = '$year' ";
		$result = $this->db->$executeType($sql);
		if(!$result)
		{
			$response['type']	= 'fail' ;
			$response['msg']	= $db->errormsg;
			$response['sql']	= $sql;
		}
		else
		{
			$response['type']	= 'pass' ;
			$response['msg']	= $db->errormsg;
			$response['sql']	= $sql;
		}
		return $response;
	}

	public function other_stores_requisition_details_insert($serialNo,$year,$item,$qty,$qty_exceed_25,$qty_exceed_100,$exQty,$executeType)
	{
		$sql = "INSERT INTO ware_storesrequesitiondetails 
				(
				intRequisitionNo, intRequisitionYear, 
				intItemId, dblQty, dblExceeded_25, dblExceeded_100,  dblExQty
				)
				VALUES
				(
				'$serialNo', '$year', 
				'$item', '$qty','$qty_exceed_25','$qty_exceed_100',  '$exQty'
				) ";
		$result = $this->db->$executeType($sql);
		if(!$result)
		{
			$response['type']	= 'fail' ;
			$response['msg']	= $db->errormsg;
			$response['sql']	= $sql;
		}
		else
		{
			$response['type']	= 'pass' ;
			$response['msg']	= $db->errormsg;
			$response['sql']	= $sql;
		}
		return $response;
	}
	
	public function other_stores_requisition_details_sales_order_wise_insert($serialNo,$year,$orderNo,$orderYear,$salesOrder,$item,$qty,$qty_exceed_25,$qty_exceed_100,$exQty,$executeType)
	{
		$sql = "INSERT INTO ware_storesrequesitiondetails_sales_order_wise 
				(
				intRequisitionNo, intRequisitionYear,intOrderNo,intOrderYear,intSalesOrderId, 
				intItemId, dblQty, dblExceeded_25, dblExceeded_100, dblExQty
				)
				VALUES
				(
				'$serialNo', '$year', '$orderNo','$orderYear','$salesOrder',
				'$item', '$qty','$qty_exceed_25','$qty_exceed_100', '$exQty'
				) ";
		$result = $this->db->$executeType($sql);
		if(!$result)
		{
			$response['type']	= 'fail' ;
			$response['msg']	= $db->errormsg;
			$response['sql']	= $sql;
		}
		else
		{
			$response['type']	= 'pass' ;
			$response['msg']	= $db->errormsg;
			$response['sql']	= $sql;
		}
		return $response;
	}
	
	
 }
?>
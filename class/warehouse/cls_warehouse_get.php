<?php
//ini_set('display_errors',1);

class cls_warehouse_get
{
	
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	
	public function getMainCategory_options(){
		
				   	$sql = "SELECT DISTINCT
							mst_maincategory.intId,
							mst_maincategory.strName
							FROM
							mst_subcategory
							Inner Join mst_maincategory ON mst_subcategory.intMainCategory = mst_maincategory.intId
							WHERE
							mst_maincategory.intStatus =  '1' "; 
					$result = $this->db->RunQuery2($sql);
					$combo ="<option value=\"\"></option>";
					while($row=mysqli_fetch_array($result))
					{
						$combo .="<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				 return $combo;
	}
	public function getMainCategoryId($subCatId){
		
					$sql = "SELECT 
							mst_maincategory.intId 
							FROM
							mst_subcategory
							Inner Join mst_maincategory ON mst_subcategory.intMainCategory = mst_maincategory.intId
							WHERE
							mst_maincategory.intStatus =  '1' AND  mst_subcategory.intId='$subCatId'"; 
					$result = $this->db->RunQuery2($sql);
					$row=mysqli_fetch_array($result);
					return $row['intId'];
	}
	public function getSubCategory_options($mainCatId){
		
					$sql = "SELECT
							mst_subcategory.intId,
							mst_subcategory.strCode,
							mst_subcategory.strName
							FROM mst_subcategory ";  
					if($mainCatId!=''){
					$sql .= "WHERE
							mst_subcategory.intMainCategory =  '$mainCatId'";
					}
					$result = $this->db->RunQuery2($sql);
					$combo ="<option value=\"\"></option>";
					while($row=mysqli_fetch_array($result))
					{
						$combo .="<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				 return $combo;
	}
	
	public function getItem_options($subCatId){
		
				 $sql = "SELECT
						mst_item.intId,
						mst_item.strName
						FROM mst_item
						WHERE
						mst_item.intSubCategory =  '$subCatId'";
					$result = $this->db->RunQuery2($sql);
					$combo ="<option value=\"\"></option>";
					while($row=mysqli_fetch_array($result))
					{
						$combo .="<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				 return $combo;
	}
	
	public function getStockBalance_bulk($location,$itemId,$grnNo,$grnYear,$grnDate,$oldRate){
 	   $sql = "SELECT
			Sum(ware_stocktransactions_bulk.dblQty) AS stockBal,
			ware_stocktransactions_bulk.intGRNNo,
			ware_stocktransactions_bulk.intGRNYear,
			ware_stocktransactions_bulk.dtGRNDate,
			ware_stocktransactions_bulk.dblGRNRate 
			FROM ware_stocktransactions_bulk 
			WHERE
			ware_stocktransactions_bulk.intItemId =  '$itemId' AND
			ware_stocktransactions_bulk.intLocationId =  '$location' AND
			ware_stocktransactions_bulk.dblGRNRate =  '$oldRate' AND
			ware_stocktransactions_bulk.dtGRNDate =  '$grnDate' AND
			ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
			ware_stocktransactions_bulk.intGRNYear =  '$grnYear'
			GROUP BY
			ware_stocktransactions_bulk.intItemId,
			ware_stocktransactions_bulk.dblGRNRate,
			ware_stocktransactions_bulk.dtGRNDate,
			ware_stocktransactions_bulk.intGRNNo,
			ware_stocktransactions_bulk.intGRNYear 
			ORDER BY
			ware_stocktransactions_bulk.dtGRNDate ASC";
			//echo $sql;
			$result =  $this->db->RunQuery2($sql);
			$row=mysqli_fetch_array($result);
			return  $row['stockBal'];
	}

	public function get_grn_wise_style_stock($location,$orderNo,$orderYear,$salesOrder,$item,$executeType){
		
	   $sql = "SELECT
			Sum(ware_stocktransactions.dblQty) AS stockBal,
			ware_stocktransactions.intGRNNo,
			ware_stocktransactions.intGRNYear,
			ware_stocktransactions.dtGRNDate,
			ware_stocktransactions.dblGRNRate 
			FROM ware_stocktransactions 
			WHERE
			ware_stocktransactions.intItemId =  '$item' AND
			ware_stocktransactions.intLocationId =  '$location' 
			GROUP BY
			ware_stocktransactions.intItemId,
			ware_stocktransactions.dblGRNRate,
			ware_stocktransactions.dtGRNDate,
			ware_stocktransactions.intGRNNo,
			ware_stocktransactions.intGRNYear 
			ORDER BY
			ware_stocktransactions.dtGRNDate ASC";
			//echo $sql;
			$result =  $this->db->$executeType($sql);
 			return  $result;
	}

	public function get_grn_wise_sub_stock($location,$orderNo,$orderYear,$salesOrder,$item,$executeType){
		
	   $sql = "SELECT
			Sum(ware_sub_stocktransactions_bulk.dblQty) AS stockBal,
			ware_sub_stocktransactions_bulk.intGRNNo,
			ware_sub_stocktransactions_bulk.intGRNYear,
			ware_sub_stocktransactions_bulk.dtGRNDate,
			ware_sub_stocktransactions_bulk.dblGRNRate 
			FROM ware_sub_stocktransactions_bulk 
			WHERE 
			ware_sub_stocktransactions_bulk.intOrderNo =  '$orderNo' AND
			ware_sub_stocktransactions_bulk.intOrderYear =  '$orderYear' AND
			ware_sub_stocktransactions_bulk.intSalesOrderId =  '$salesOrder' AND
			ware_sub_stocktransactions_bulk.intItemId =  '$item' AND
			ware_sub_stocktransactions_bulk.intLocationId =  '$location' 
			GROUP BY 
			ware_sub_stocktransactions_bulk.intOrderNo,
			ware_sub_stocktransactions_bulk.intOrderYear,
			ware_sub_stocktransactions_bulk.intSalesOrderId,
			ware_sub_stocktransactions_bulk.intItemId,
			ware_sub_stocktransactions_bulk.dblGRNRate,
			ware_sub_stocktransactions_bulk.dtGRNDate,
			ware_sub_stocktransactions_bulk.intGRNNo,
			ware_sub_stocktransactions_bulk.intGRNYear 
			ORDER BY
			ware_sub_stocktransactions_bulk.dtGRNDate ASC";
			//echo $sql;
			$result =  $this->db->$executeType($sql);
 			return  $result;
	}



	public function get_stock_balance_bulk($location,$itemId,$grnNo,$grnYear,$grnDate,$oldRate,$executeType){
		
	   $sql = "SELECT
			round(Sum(ware_stocktransactions_bulk.dblQty),4) AS stockBal,
			ware_stocktransactions_bulk.intGRNNo,
			ware_stocktransactions_bulk.intGRNYear,
			ware_stocktransactions_bulk.dtGRNDate,
			ware_stocktransactions_bulk.dblGRNRate 
			FROM ware_stocktransactions_bulk 
			WHERE
			ware_stocktransactions_bulk.intItemId =  '$itemId' AND
			ware_stocktransactions_bulk.intLocationId =  '$location' AND
			ware_stocktransactions_bulk.dblGRNRate =  '$oldRate' AND
			ware_stocktransactions_bulk.dtGRNDate =  '$grnDate' AND
			ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
			ware_stocktransactions_bulk.intGRNYear =  '$grnYear'
			GROUP BY
			ware_stocktransactions_bulk.intItemId,
			ware_stocktransactions_bulk.dblGRNRate,
			ware_stocktransactions_bulk.dtGRNDate,
			ware_stocktransactions_bulk.intGRNNo,
			ware_stocktransactions_bulk.intGRNYear 
			ORDER BY
			ware_stocktransactions_bulk.dtGRNDate ASC";
			//echo $sql;
			$result =  $this->db->$executeType($sql);
			$row=mysqli_fetch_array($result);
			return  $row['stockBal'];
	}
	
	public function getGRNYears_options(){
		
				   	$sql = "SELECT DISTINCT
							ware_grnheader.intGrnYear
							FROM ware_grnheader
							ORDER BY
							ware_grnheader.intGrnYear DESC"; 
					$result = $this->db->RunQuery2($sql);
					$combo ="";
					while($row=mysqli_fetch_array($result))
					{
						$combo .="<option value=\"".$row['intGrnYear']."\">".$row['intGrnYear']."</option>";	
					}
				 return $combo;
	}
	
	public function getGRNNosWithInpections_options($grnYear){
		
				   	$sql = "SELECT DISTINCT
							ware_grnheader.intGrnNo
							FROM
							ware_grnheader
							Inner Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear
							WHERE
							ware_grnheader.intStatus =  '1' AND
							ware_grnheader.intGrnYear =  '$grnYear' AND
							ware_grndetails.intInspectionItem =  '1'
							ORDER BY
							ware_grnheader.intGrnYear DESC,  
							ware_grnheader.intGrnNo DESC
							"; 
					$result = $this->db->RunQuery2($sql);
					$combo ="<option value=\"\"></option>";
					while($row=mysqli_fetch_array($result))
					{
						$combo .="<option value=\"".$row['intGrnNo']."\">".$row['intGrnNo']."</option>";	
					}
				 return $combo;
	}
	
	public function getGRNDate($grnNo,$grnYear){
		
	   $sql = "SELECT
				ware_grnheader.datdate
				FROM ware_grnheader
				WHERE
				ware_grnheader.intGrnNo =  '$grnNo' AND
				ware_grnheader.intGrnYear =  '$grnYear'";
			$result =  $this->db->RunQuery2($sql);
			$row=mysqli_fetch_array($result);
			return  $row['datdate'];
	}
	
	public function getGRNRate($grnNo,$grnYear,$itemId){
		
	   $sql = "SELECT
				ware_grndetails.dblGrnRate
				FROM ware_grndetails
				WHERE
				ware_grndetails.intGrnNo =  '$grnNo' AND
				ware_grndetails.intGrnYear =  '$grnYear' AND
				ware_grndetails.intItemId =  '$itemId'";
			$result =  $this->db->RunQuery2($sql);
			$row=mysqli_fetch_array($result);
			return  $row['dblGrnRate'];
	}
	
	public function getGRNCurrency($grnNo,$grnYear,$itemId){
		
		if(($grnNo!=0) && ($grnYear!=0)){
		  $sql = "SELECT
				trn_poheader.intCurrency as currency 
				FROM
				ware_grnheader
				Inner Join trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
				WHERE
				ware_grnheader.intGrnNo =  '$grnNo' AND
				ware_grnheader.intGrnYear =  '$grnYear'";
		}
		else{
		$sql = "SELECT
				mst_item.intCurrency as currency 
				FROM mst_item
				WHERE
				mst_item.intId =  '$itemId'";
		}
				$result =  $this->db->RunQuery2($sql);
				$row=mysqli_fetch_array($result);
				return  $row['currency'];
	}
	
	public function getGRNQty($grnNo,$grnYear,$itemId){
		
	   $sql = "SELECT
				ware_grndetails.dblGrnQty
				FROM ware_grndetails
				WHERE
				ware_grndetails.intGrnNo =  '$grnNo' AND
				ware_grndetails.intGrnYear =  '$grnYear' AND
				ware_grndetails.intItemId =  '$itemId'";
			$result =  $this->db->RunQuery2($sql);
			$row=mysqli_fetch_array($result);
			return  $row['dblGrnQty'];
	}

	public function getGRNinspectedGoodQty($grnNo,$grnYear,$itemId){
		
	   $sql = "SELECT
				ware_grndetails.dblInspectedGoodQty
				FROM ware_grndetails
				WHERE
				ware_grndetails.intGrnNo =  '$grnNo' AND
				ware_grndetails.intGrnYear =  '$grnYear' AND
				ware_grndetails.intItemId =  '$itemId'";
			$result =  $this->db->RunQuery2($sql);
			$row=mysqli_fetch_array($result);
			return  $row['dblInspectedGoodQty'];
	}

	public function getGRNinspectedDmgQty($grnNo,$grnYear,$itemId){
		
	   $sql = "SELECT
				ware_grndetails.dblInspectedDammagedQty
				FROM ware_grndetails
				WHERE
				ware_grndetails.intGrnNo =  '$grnNo' AND
				ware_grndetails.intGrnYear =  '$grnYear' AND
				ware_grndetails.intItemId =  '$itemId'";
			$result =  $this->db->RunQuery2($sql);
			$row=mysqli_fetch_array($result);
			return  $row['dblInspectedDammagedQty'];
	}


	public function getStockInspectedGoodQty($grnNo,$grnYear,$itemId){
		
	   $sql = "SELECT
				sum(ware_stocktransactions_bulk.dblQty) as dblQty 
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.strType =  'GRN' AND
				ware_stocktransactions_bulk.intDocumentNo =  '$grnNo' AND
				ware_stocktransactions_bulk.intDocumntYear =  '$grnYear' AND
				ware_stocktransactions_bulk.intItemId =  '$itemId'
				";
			$result =  $this->db->RunQuery2($sql);
			$row=mysqli_fetch_array($result);
			return  $row['dblQty'];
	}

	
	public function getGRNStatus($grnNo,$grnYear){
		
	   $sql = "SELECT
				ware_grnheader.intStatus 
				FROM ware_grnheader
				WHERE
				ware_grnheader.intGrnNo =  '$grnNo' AND
				ware_grnheader.intGrnYear =  '$grnYear'";
			$result =  $this->db->RunQuery2($sql);
			$row=mysqli_fetch_array($result);
			return  $row['intStatus'];
	}
	
	public function getOrderItems($itemId,$grnNo,$grnYear){
		
	   $sql = "SELECT
				ware_grndetails.dblGrnQty
				FROM ware_grndetails
				WHERE
				ware_grndetails.intGrnNo =  '$grnNo' AND
				ware_grndetails.intGrnYear =  '$grnYear' AND
				ware_grndetails.intItemId =  '$itemId'";
			$result =  $this->db->RunQuery2($sql);
			$row=mysqli_fetch_array($result);
			return  $row['dblGrnQty'];
	}
	
	public function getLocationValidation($type,$location,$serialNo,$year){
		
		if($type=='GRN'){
		        $sql="SELECT
					trn_poheader.intDeliveryTo as location 
					FROM trn_poheader
					WHERE
					trn_poheader.intPONo =  '$serialNo' AND
					trn_poheader.intPOYear =  '$year'";
		}
		else if($type=='TRANSFERIN'){
		         $sql="SELECT
					ware_gatepassheader.intGPToLocation as location 
					FROM
					ware_gatepassheader
					WHERE
					ware_gatepassheader.intGatePassNo =  '$serialNo' AND
					ware_gatepassheader.intGatePassYear =  '$year'";
		}
		else if($type=='RTSUP'){
		         $sql="SELECT
					ware_grnheader.intCompanyId as location 
					FROM ware_grnheader
					WHERE
					ware_grnheader.intGrnNo =  '$serialNo' AND
					ware_grnheader.intGrnYear =  '$year'
					";
		}
		else if($type=='RETSTORES'){
		            $sql="SELECT
					ware_issueheader.intCompanyId as location 
					FROM ware_issueheader
					WHERE
					ware_issueheader.intIssueNo =  '$serialNo' AND
					ware_issueheader.intIssueYear =  '$year'
					";
		}
		else if($type=='ISSUE'){
		            $sql="SELECT
					ware_mrnheader.intCompanyId as location 
					FROM ware_mrnheader
					WHERE
					ware_mrnheader.intMrnNo =  '$serialNo' AND
					ware_mrnheader.intMrnYear =  '$year'
					";
		}
		else if($type=='INVOICE'){
		            $sql="SELECT
					mst_supplier.intInterLocation as location 
					FROM
					trn_poheader
					Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
					WHERE
					trn_poheader.intPONo =  '$serialNo' AND
					trn_poheader.intPOYear =  '$year'
					";
		}
		else if($type=='INSPECT'){
		            $sql="SELECT
					ware_grnheader.intCompanyId as location 
					FROM ware_grnheader
					WHERE
					ware_grnheader.intGrnNo =  '$serialNo' AND
					ware_grnheader.intGrnYear =  '$year'
					";
		}

			$result = $this->db->RunQuery2($sql);
		    $row=mysqli_fetch_array($result);
		 	   //  echo $row['location']."==".$location;
				
				if($row['location']==$location){
				$data['type'] = 'pass';
				$data['msg'] = '';
				}
				else{
				$data['type'] = 'fail';
				$data['msg'] = 'Invalid Save Location';
				}
				return $data;
	}

	public function get_sub_stores_options($location,$executeType){
		
			$sql = $this->Load_sub_stores_sql($location);
			$result = $this->db->$executeType($sql);
			$combo ="<option value=\"\"></option>";
			while($row=mysqli_fetch_array($result))
			{
				$combo .="<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
			}
		 return $combo;
	}
	public function get_order_years__options($location,$executeType){
		  global $db;
			$sql = $this->get_order_years_options_sql($location);
			$result = $db->$executeType($sql);
			$options ="<option value=\"\"></option>";
			while($row=mysqli_fetch_array($result))
			{
				$options .="<option value=\"".$row['intOrderYear']."\">".$row['intOrderYear']."</option>";	
			}
		 return $options;
	}
	public function get_order_nos_options($location,$year,$executeType){
		  global $db;
			$sql = $this->get_order_nos_options_sql($location,$year);
			$result = $db->$executeType($sql);
			$options ="<option value=\"\"></option>";
			while($row=mysqli_fetch_array($result))
			{
				$options .="<option value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";	
			}
		 return $options;
	}
	
	
	public function get_order_nos_combo($location,$executeType){
		  global $db;
			$sql = $this->get_order_nos_sql($location);
			$result = $db->$executeType($sql);
			$combo ="<option value=\"\"></option>";
			while($row=mysqli_fetch_array($result))
			{
				$combo .="<option value=\"".$row['intOrderNo']."/".$row['intOrderYear']."\">".$row['intOrderNo']."/".$row['intOrderYear']."</option>";	
			}
		 return $combo;
	}
	public function get_sales_order_nos_options($orderNo,$year,$executeType){
		  global $db;
			$sql = $this->get_sales_oreder_nos_sql($orderNo,$year,'');
			$result = $db->$executeType($sql);
			$combo ="<option value=\"\"></option>";
			while($row=mysqli_fetch_array($result))
			{
				$combo .="<option value=\"".$row['intSalesOrderId']."\">".$row['strSalesOrderNo']."</option>";	
			}
		 return $combo;
	}
	
	public function get_sales_order_nos_result($orderNo,$year,$salesOrder,$executeType){
		  global $db;
			$sql = $this->get_sales_oreder_nos_sql($orderNo,$year,$salesOrder);
			$result = $db->$executeType($sql);
 		 return $result;
	}
	
	
	
	public function get_sales_order_no_wise_result($orderNo,$orderYear,$item,$toSaveQty,$executeType){
		  global $db;
 			$sql = $this->get_sales_oreder_no_wises_sql($orderNo,$orderYear,$item,$toSaveQty);
			$result = $db->$executeType($sql);
 		 return $result;
	}
	
	public function get_Allocate_Qty($orderNo,$orderYear,$salesOrderId,$itemId,$allocateQty,$executeType){
		  global $db;
		  
				$sql = $this->get_order_item_sample_consumption_sales_order_wise_sql($orderNo,$orderYear,$itemId,'');
				$result = $db->$executeType($sql);
				$row=mysqli_fetch_array($result);
				$tot_cons = $row['required'];
			
				$sql = $this->get_order_item_sample_consumption_sales_order_wise_sql($orderNo,$orderYear,$itemId,$salesOrderId);
				$result = $db->$executeType($sql);
				$row=mysqli_fetch_array($result);
				$cons = $row['required'];
				
				$val = ($cons/$tot_cons)*$allocateQty;
  		 return $val;
	}
	
	public function get_main_category_options($executeType){
		  global $db;
			$sql = $this->get_main_category_sql($orderNo,$year);
			$result = $db->$executeType($sql);
			$combo ="<option value=\"\"></option>";
			while($row=mysqli_fetch_array($result))
			{
				$combo .="<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
			}
		 return $combo;
	}
	public function get_sub_category_options($mainCategory,$executeType){
		  global $db;
			$sql = $this->get_sub_category_sql($mainCategory);
			$result = $db->$executeType($sql);
			$combo ="<option value=\"\"></option>";
			while($row=mysqli_fetch_array($result))
			{
				$combo .="<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
			}
		 return $combo;
	}
	public function get_stores_trans_items($orderNo,$orderYear,$salesOrderNo,$mainCategory,$subCategory,$description,$executeType){
		  global $db;
			$sql = $this->get_stores_trans_items_sql($orderNo,$orderYear,$salesOrderNo,$mainCategory,$subCategory,$description);
			$result = $db->$executeType($sql);
			return $result;
	}
	
	public function get_foil_special_items($orderNo,$orderYear,$salesOrderNo,$mainCategory,$subCategory,$description,$executeType){
		  global $db;
			$sql = $this->get_foil_special_items_sql($orderNo,$orderYear,$salesOrderNo,$mainCategory,$subCategory,$description);
			
			$result = $db->$executeType($sql);
			return $result;
	}
	
	public function get_bulk_stock_balance($location,$item,$executeType){
		  global $db;
			$sql = $this->get_bulk_stock_balance_sql($location,$item);
			$result = $db->$executeType($sql);
			$row=mysqli_fetch_array($result);
			return $row['bal'];
	}
	
	public function get_bulk_stock_balance_to_given_date($location,$item,$toDate,$executeType){
		  global $db;
			$sql = $this->get_bulk_stock_balance_to_given_date_sql($location,$item,$toDate);
			$result = $db->$executeType($sql);
			$row=mysqli_fetch_array($result);
			return $row['bal'];
	}
	
	public function get_bulk_stock_balance_with_or_without_location($location,$item,$executeType){
		  global $db;
			$sql = $this->get_bulk_stock_balance_with_or_without_location_sql($location,$item);
			$result = $db->$executeType($sql);
			$row=mysqli_fetch_array($result);
			return $row['bal'];
	}

	public function get_bulk_stock_val($location,$item,$to_currency,$executeType){
		  global $db;
			$sql = $this->get_bulk_stock_val_sql($location,$item,$to_currency);
			$result = $db->$executeType($sql);
			$row=mysqli_fetch_array($result);
			return $row['val'];
	}
	
	public function get_bulk_stock_val_with_or_without_location($location,$item,$to_currency,$executeType){
		  global $db;
			$sql = $this->get_bulk_stock_val_with_or_without_location_sql($location,$item,$to_currency);
			$result = $db->$executeType($sql);
			$row=mysqli_fetch_array($result);
			return $row['val'];
	}
	
	public function get_bulk_stock_val_to_given_date($location,$item,$toDate,$to_currency,$executeType){
		  global $db;
			$sql = $this->get_bulk_stock_val_to_given_date_sql($location,$item,$toDate,$to_currency,$executeType);
			$result = $db->$executeType($sql);
			$row=mysqli_fetch_array($result);
			return $row['val'];
	}
	
	public function get_order_qty($orderNo,$orderYear,$salesOrder,$executeType){
		  global $db;
			$sql = $this->get_order_qty_sql($orderNo,$orderYear,$salesOrder);
			$result = $db->$executeType($sql);
			$row=mysqli_fetch_array($result);
			return $row['qty'];
	}
	public function get_production_order_qty($orderNo,$orderYear,$salesOrder,$executeType){
		  global $db;
		  		//echo $orderNo.','.$orderYear.','.$salesOrder.','.$executeType."|";

			$sql = $this->get_order_production_qty_sql($orderNo,$orderYear,$salesOrder);
			$result = $db->$executeType($sql);
			$row=mysqli_fetch_array($result);
			return  $row['qty'];
	}
	public function get_order_item_sample_consumption_per_piece($orderNo,$orderYear,$salesOrder,$item,$executeType){
		  global $db;
			$sql = $this->get_order_item_sample_consumption_sql($orderNo,$orderYear,$item,$salesOrder);
			$result = $db->$executeType($sql);
			$row=mysqli_fetch_array($result);
			return $row['consum'];
	}
	public function get_order_item_styleAllocation($orderNo,$orderYear,$salesOrder,$item,$executeType)
	{
		$sql = $this->get_order_item_sample_consumption_sql($orderNo,$orderYear,$item,$salesOrder);
		$result = $this->db->$executeType($sql);	
		
		return $result;
	}
	
	public function get_order_item_sample_consumption($orderNo,$orderYear,$salesOrder,$item,$executeType){
		  global $db;

 		
			$tot	=	0;
			$sql = $this->get_sales_oreder_nos_sql($orderNo,$orderYear,$salesOrder);
			$result = $db->$executeType($sql);
 			while($row=mysqli_fetch_array($result))
			{
				$salesOrder	= $row['intSalesOrderId'];

				//$val	= $this->get_production_ink_first_day_consumptions_salesOrder_wise($orderNo,$orderYear,$salesOrder,$item,$executeType);
				$cons		= $this->get_order_item_sample_consumption_sales_order_wise($orderNo,$orderYear,$salesOrder,$item,$executeType);
				$orderQty	= $this->get_production_order_qty($orderNo,$orderYear,$salesOrder,$executeType);
				if($cons>0){
					$totQty  += $orderQty;
					$totCons += $cons*$orderQty;
					if($item=='728'){
						//echo $orderQty.'|';
						//echo $cons.'/';
					}
				}
			}
			$consum	=	$totCons/$totQty;
 
			return $consum;
			
 	}
	
	
	public function get_order_item_sample_consumption_sales_order_wise($orderNo,$orderYear,$salesOrder,$item,$executeType){
		  global $db;
			$sql = $this->get_order_item_sample_consumption_sales_order_wise_sql($orderNo,$orderYear,$item,$salesOrder);
			$result = $db->$executeType($sql);
			$row=mysqli_fetch_array($result);
			return $row['consum'];
	}
	public function get_order_item_styleAllocation_auto($orderNo,$orderYear,$salesOrder,$item,$executeType)
	{
		$sql = $this->get_order_item_sample_consumption_sales_order_wise_sql($orderNo,$orderYear,$item,$salesOrder);
		$result = $this->db->$executeType($sql);	
		//if($item!='')
		//echo $sql;
		
		return $result;
	}
	
	public function get_order_item_first_second_consumption($orderNo,$orderYear,$salesOrder,$item,$executeType){
		  global $db;
			$sql = $this-> get_order_item_first_consump_type($orderNo,$orderYear,$item,$salesOrder);
			$result = $db->$executeType($sql);
			$row=mysqli_fetch_array($result);
			$type	= $row['type'];
			$sql = $this->get_order_item_first_second_consumption_sql($orderNo,$orderYear,$item,$salesOrder,$type);
			$result = $db->$executeType($sql);
			while($row=mysqli_fetch_array($result)){
				$totQty  += $row['sizeQty'];
				$totCons += $row['sizeQty']*$row['CONSUMPTION'];
			}
			$consum	=	$totCons/$totQty;
			return $consum;
	}

	public function get_order_item_first_second_consumption_auto($orderNo,$orderYear,$item,$executeType){
 			global $db;
			$tot	=	0;
			$sql = $this->get_sales_oreder_nos_sql($orderNo,$orderYear,'');
			$result = $db->$executeType($sql);
 			while($row=mysqli_fetch_array($result))
			{
				$salesOrder	= $row['intSalesOrderId'];
				$val=$this->get_order_item_first_second_consumption($orderNo,$orderYear,$salesOrder,$item,$executeType);
				$tot +=  $val;
			}
			
			return $tot;
	}
	
	public function get_production_cumulateve_ink_item_consumption($orderNo,$orderYear,$salesOrder,$item,$executeType){
		  global $db;
			$tot	=	0;
			$sql = $this->get_sales_oreder_nos_sql($orderNo,$orderYear,$salesOrder);
			$result = $db->$executeType($sql);
 			while($row=mysqli_fetch_array($result))
			{
				$salesOrder	= $row['intSalesOrderId'];
				//$val	= $this->get_production_ink_first_day_consumptions_salesOrder_wise($orderNo,$orderYear,$salesOrder,$item,$executeType);
				//$cons		= $this->get_cumulateve_ink_item_consumption($orderNo,$orderYear,$salesOrder,$item,$executeType);
				//$orderQty	= $this->get_production_order_qty($orderNo,$orderYear,$salesOrder,$executeType);
				$sql		=	$this->get_processed_ink_item_cumulative_conpc_sql($orderNo,$orderYear,$salesOrder,$item);
				$result 	=	$db->$executeType($sql);
				$row		=	mysqli_fetch_array($result);
				$cons		=	round($row['CUMULATIVE_CONPC'],6);
				$orderQty	=	$row['ACTUAL_PCS'];
				
				if($cons>0){
					$totQty  += $orderQty;
					$totCons += $cons*$orderQty;
				}
			}
			$consum	=	$totCons/$totQty;
 			
			return $consum;
 	}
	
	public function get_production_cumulateve_special_foil_item_consumption($orderNo,$orderYear,$salesOrder,$item,$executeType){
		  global $db;
			$tot	=	0;
			$sql = $this->get_sales_oreder_nos_sql($orderNo,$orderYear,$salesOrder);
			$result = $db->$executeType($sql);
 			while($row=mysqli_fetch_array($result))
			{
				$salesOrder	= $row['intSalesOrderId'];
				$cons		= $this->get_cumulateve_special_foil_consumption($orderNo,$orderYear,$salesOrder,$item,$executeType);
				$orderQty	= $this->get_production_order_qty($orderNo,$orderYear,$salesOrder,$executeType);
				if($cons>0){
					$totQty  += $orderQty;
					$totCons += $cons*$orderQty;
				}
			}
			$consum	=	$totCons/$totQty;
 			
			return $consum;
 	}
	
	public function get_order_item_issued_qty($orderNo,$orderYear,$salesOrder,$item,$executeType){
		  global $db;
			$sql = $this->get_order_item_issued_qty_sql($orderNo,$orderYear,$item,$salesOrder);
			$result = $db->$executeType($sql);
			$row=mysqli_fetch_array($result);
			return $row['IssuedQty'];
	}
	
	public function get_order_item_stock_bal($location,$orderNo,$orderYear,$salesOrder,$item,$executeType){
		  global $db;
			$sql = $this->get_order_item_stock_bal_sql($location,$orderNo,$orderYear,$salesOrder,$item);
			$result = $db->$executeType($sql);
			$row=mysqli_fetch_array($result);
			return $row['stockBal'];
	}
	
 	public function get_order_item_stock_bal_without_location($orderNo,$orderYear,$salesOrder,$item,$executeType){
		  global $db;
			$sql = $this->get_order_item_stock_bal_without_location_sql($orderNo,$orderYear,$salesOrder,$item);
			$result = $db->$executeType($sql);
			$row=mysqli_fetch_array($result);
			return $row['stockBal'];
	}
	
 	public function get_order_item_color_room_stock_bal_without_location($orderNo,$orderYear,$salesOrder,$item,$executeType){
		  global $db;
			$sql = $this->get_order_item_color_room_stock_bal_without_location_sql($orderNo,$orderYear,$salesOrder,$item);
			$result = $db->$executeType($sql);
			$row=mysqli_fetch_array($result);
			return $row['stockBal'];
	}
	
	public function get_sizes_array($orderNo,$orderYear,$salesOrder,$executeType){
		  global $db;
			$sql = $this->get_sizes_sql($orderNo,$orderYear,$salesOrder);
			$result = $db->$executeType($sql);
			$j=0;
			while($row=mysqli_fetch_array($result)){
 				$sizes[$j]=$row['strSize'];
				$j++;
			}
			return $sizes;
	}
	public function get_consump_saved_flag($orderNo,$orderYear,$salesOrder,$item,$size,$type,$executeType){
		  global $db;
			$sql	= $this->get_day_consumptions_sql($orderNo,$orderYear,$salesOrder,$item,$type,$size);
			$result = $db->$executeType($sql);
			if( mysqli_num_rows($result)>0)
 				$flag = 1;
				else
 				$flag = 0;
  			
			return $flag;
	}

	public function get_day_consumptions($orderNo,$orderYear,$salesOrder,$item,$size,$type,$executeType){
		  global $db;
			$sql	= $this->get_day_consumptions_sql($orderNo,$orderYear,$salesOrder,$item,$type,$size);
			$result = $db->$executeType($sql);
 			$row	= mysqli_fetch_array($result);
 			$cons	= $row['CONSUMPTION'];
			if($cons==''){
				$cons = 0;
			}
 			return $cons;
	}
	
	public function get_item_name($item,$executeType){
		  global $db;
			$sql	= $this->get_item_name_sql($item);
			$result = $db->$executeType($sql);
 			$row	= mysqli_fetch_array($result);
			return $row;
	}
	
	public function get_auto_allocation_percentages($company,$transType)
	{
		$sql = "SELECT
				sys_company_item_allocation_percentages.`SAMPLE_ALLOCATION_%`,
				sys_company_item_allocation_percentages.`FULL_ALLOCATION_%`,
				sys_company_item_allocation_percentages.`FOIL_EXTRA_ALLOCATION_%`,
				sys_company_item_allocation_percentages.`SP_RM_EXTRA_ALLOCATION_%`,
				sys_company_item_allocation_percentages.`INK_ITEM_EXTRA_ALLOCATION_%`,
				sys_company_item_allocation_percentages.`INK_ITEM_EXTRA_SRN_%` 
				FROM `sys_company_item_allocation_percentages`
				WHERE
				sys_company_item_allocation_percentages.COMPANY_ID = '$company'";
		$result = $this->db->$transType($sql);
		$row = mysqli_fetch_array($result);
		
		return $row;
	}

	public function get_cumulateve_special_foil_consumption($orderNo,$orderYear,$salesOrder,$item,$executeType){
		  global $db;
		  $day_cons_rcds	=	0;
			$sql	= $this->get_cumulateve_special_foil_consumption_sql($orderNo,$orderYear,$salesOrder,$item);
			$result = $db->$executeType($sql);
 			while($row=mysqli_fetch_array($result))
			{
				$tot_qty 	+=  $row['QTY'];
				$tot_cons 	+=  $row['CONS'];
				$day_cons_rcds++;
			}
			
			$val	=	round($tot_cons/$tot_qty,6);
			
			/*if($day_cons_rcds	== '0'){
				$val	=	$this->get_order_item_sample_consumption_sales_order_wise_sql($orderNo,$orderYear,$item,$salesOrder);
				$val	=	round($val,4);
			}*/
			
			return $val;
	}
	
	public function get_sales_orders_for_graphic($orderYear,$graphic,$item,$executeType){
			global $db;
			//echo $orderYear.','.$graphic.','.$item.','.$executeType."|";
			$sql	= $this->get_sales_orders_for_graphic_sql($orderYear,$graphic,$item);
			$result = $db->$executeType($sql);
			return $result;
	}
	
	public function get_daily_ink_item_used_result($orderNo,$orderYear,$salesOrder,$item,$executeType){
			global $db;
			$sql	= $this->get_daily_ink_item_used_sql($orderNo,$orderYear,$salesOrder,$item,$executeType);
			$result = $db->$executeType($sql);
			return $result;
	}

	public function get_day_actual_production_qty_result($orderNo,$orderYear,$salesOrder,$date,$executeType){
			global $db;
			$sql	= $this->get_day_actual_production_qty_sql($orderNo,$orderYear,$salesOrder,$date);
			$result = $db->$executeType($sql);
			return $result;
	}
	
	
	public function get_cumulateve_ink_item_consumption($orderNo,$orderYear,$salesOrder,$item,$executeType){
		  global $db;
			
			$sql	=	$this->get_processed_ink_item_cumulative_conpc_sql($orderNo,$orderYear,$salesOrder,$item);
			$result =	$db->$executeType($sql);
 			$row	=	mysqli_fetch_array($result);
 			$val	=	round($row['CUMULATIVE_CONPC'],6);
 
			return $val;
	}
	
	public function get_cumulateve_actual_produced_pcs($orderNo,$orderYear,$salesOrder,$item,$executeType){
		  global $db;
			
			$sql	=	$this->get_processed_ink_item_cumulative_conpc_sql($orderNo,$orderYear,$salesOrder,$item);
			$result =	$db->$executeType($sql);
 			$row	=	mysqli_fetch_array($result);
 			$val	=	round($row['ACTUAL_PCS']);
 
			return $val;
	}
	
	public function get_already_allocated_qty($orderYear,$orderNo,$salesOrderId,$itemId,$executeType)
	{
		 global $db;
	
		$sql	=	$this->get_already_allocated_sql($orderYear,$orderNo,$salesOrderId,$itemId);
		$result =	$this->db->$executeType($sql);
		$row 	=	mysqli_fetch_array($result);
		$qty	=	$row['qty'];
		
		return $qty;
	}
	
	public function get_already_STN_qty($orderYear,$graphicNo,$itemId,$executeType)
	{
		 global $db;
	
		$sql	= $this->get_already_STN_sql($orderYear,$graphicNo,$itemId);
		$result = $this->db->$executeType($sql);
		$row 	= mysqli_fetch_array($result);
 		
		return $row;
	}
	
	public function get_already_STN_Returned_qty($orderYear,$graphicNo,$itemId,$executeType)
	{
		 global $db;
	
		$sql	= $this->get_already_STN_Returned_sql($orderYear,$graphicNo,$itemId);
		$result = $this->db->$executeType($sql);
		$row 	= mysqli_fetch_array($result);
 		
		return $row;
	}
	
	public function maximum_order_item_allocatable_details($orderNo,$orderYear,$salesOrder,$item,$item_type,$percentage_foil,$percentage_ink,$percentage_sp_rm,$executeType){
 	
		global $db;	
		
		$qty					=	$this->get_production_order_qty($orderNo,$orderYear,$salesOrder,$executeType);
		$actual_produced_qty	=	$this->get_cumulateve_actual_produced_pcs($orderNo,$orderYear,$salesOrder,$item,$executeType);
		//echo $qty.' - '.$actual_produced_qty.'|||';
		$qty_bal				=	$qty - $actual_produced_qty;	
		$day_cons_foil_sp_rm	=   $this->get_cumulateve_special_foil_consumption($orderNo,$orderYear,$salesOrder,$item,$executeType);
		$day_cons_ink_itm	=   $this->get_cumulateve_ink_item_consumption($orderNo,$orderYear,$salesOrder,$item,$executeType);
		$samp_conPC				=   $this->get_order_item_sample_consumption($orderNo,$orderYear,$salesOrder,$item,$executeType);
		//echo $item.'-'.$samp_conPC."|";	
		
		if($item_type == '1'){
		$day_cons 	= round($day_cons_ink_itm,6);
		$presentage	= $percentage_ink;
		}
		else if($item_type == '3'){
		$day_cons = round($day_cons_foil_sp_rm,6);
		$presentage	 = $percentage_foil;
		}
		else if($item_type == '2'){
		$day_cons = round($day_cons_foil_sp_rm,6);
		$presentage	= $percentage_sp_rm;
		}
		
		if($day_cons == '' || $day_cons =='0'){
		$cons	= round($samp_conPC,6);
		$cons_flag=1;
		}
		else{
		$cons	= $day_cons;
		$cons_flag=2;
		}

		
		$tot_qty_for_allocate	= $cons*$qty_bal*$presentage/100;
		//$allocated_qty			= $this->get_already_allocated_qty($orderYear,$orderNo,$salesOrder,$item,$executeType);			
		//$Qty_to_save			= ($obj_commom->ceil_to_decimal_places($tot_qty_for_allocate,2)) - ($obj_commom->ceil_to_decimal_places($allocated_qty,2));
		$style_stock_bal		= $this->get_order_item_stock_bal_without_location($orderNo,$orderYear,$salesOrder,$item,$executeType);			
		$color_room_stock_bal	= $this->get_order_item_color_room_stock_bal_without_location($orderNo,$orderYear,$salesOrder,$item,$executeType);			
	
		$data['tot_qty_for_allocate']					= $tot_qty_for_allocate;
		$data['style_stock_bal']						= $style_stock_bal;
		$data['color_room_stock_bal']					= $color_room_stock_bal;
		$data['consumption_type']						= $cons_flag;

			//	echo "+".$item.'-'.$presentage;
		
		return $data;
	
	}
	
	public function get_sales_order_bal_to_production_order_qty_prapotion($orderNo,$orderYear,$salesOrder,$graphicNo,$item,$prec_100,$prec_25,$executeType){
		
		$result_s	= $this->get_sales_orders_for_graphic($orderYear,$graphicNo,$item,$executeType);
		while($row_s=mysqli_fetch_array($result_s)){//sales order wise
		  
			$orderNo_t			= $row_s['intOrderNo'];
			$orderYear_t		= $row_s['intOrderYear'];	
			$salesOrder_t		= $row_s['intSalesOrderId'];
			$qty_t					=	$this->get_production_order_qty($orderNo_t,$orderYear_t,$salesOrder_t,$executeType);
			$actual_produced_qty_t	=	$this->get_cumulateve_actual_produced_pcs($orderNo_t,$orderYear_t,$salesOrder_t,$item,$executeType);
			
			$day_cons_ink_itm	= $this->get_cumulateve_ink_item_consumption($orderNo_t,$orderYear_t,$salesOrder_t,$item,$executeType);
			$samp_conPC			= $this->get_order_item_sample_consumption($orderNo_t,$orderYear_t,$salesOrder_t,$item,$executeType);
			
			$color_room_stock_bal	= $this->get_order_item_color_room_stock_bal_without_location($orderNo_t,$orderYear_t,$salesOrder_t,$item,$executeType);			
			
			if($day_cons_ink_itm > 0){
			$cons	= $day_cons_ink_itm;
			$cons_tot_qty	+=(($qty_t - $actual_produced_qty_t)*$cons*$prec_100/100) - ($color_room_stock_bal);
			}
			else{
			$cons			= $samp_conPC;
			$cons_tot_qty	+=(($qty_t - $actual_produced_qty_t)*$cons*$prec_25/100) - ($color_room_stock_bal);
			}
		}

		$day_cons_ink_itm	= $this->get_cumulateve_ink_item_consumption($orderNo,$orderYear,$salesOrder,$item,$executeType);
		$samp_conPC			= $this->get_order_item_sample_consumption($orderNo,$orderYear,$salesOrder,$item,$executeType);
		if($day_cons_ink_itm > 0){
			$cons	= $day_cons_ink_itm;
			$per	=$prec_100;
		}
		else{
			$cons	= $samp_conPC;
			$per	=$prec_25;
		}
		
		$qty					=	$this->get_production_order_qty($orderNo,$orderYear,$salesOrder,$executeType);
		$actual_produced_qty	=	$this->get_cumulateve_actual_produced_pcs($orderNo,$orderYear,$salesOrder,$item,$executeType);
		$color_room_stock_bal	= $this->get_order_item_color_room_stock_bal_without_location($orderNo,$orderYear,$salesOrder,$item,$executeType);			
		$cons_qty				=	(($qty - $actual_produced_qty)*$cons*$per/100) - ($color_room_stock_bal);

		$prapotion	=	$cons_qty/$cons_tot_qty;
			

	
		return $prapotion;
		
	}

	public function get_expiry_items_sql($mainCategory,$subCategory,$itemId){
		$sql	= "SELECT
			mst_maincategory.strName AS MAIN_CAT,
			mst_subcategory.strName AS SUB_CAT,
			mst_item.strName AS ITEM,
			mst_item.strCode AS ITEM_CODE,
			mst_item.strCode as SUP_ITEM_CODE,	
			mst_item.intId as ITEM_ID ,
			mst_item.intUOM,
			mst_units.strName AS UOM
			FROM
			mst_item
			INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
			INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
			INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
                        LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
			WHERE 
			1=1 ";
			if($mainCategory !='')
	$sql	.= " AND mst_item.intMainCategory = '$mainCategory' ";
			if($subCategory !='')
	$sql	.= " AND 	mst_item.intSubCategory = '$subCategory' ";
			if($itemId !='')
	$sql	.= " AND 	mst_item.intId = '$itemId'";
			
	$sql .= " ORDER BY mst_maincategory.strName ASC, mst_subcategory.strName ASC, mst_item.strName ASC";
		
        return $sql;
    }

    public function get_expiry_data_sql($company,$location,$itemId,$year,$month){
		$sql	= "";
		$sql	= "
				SELECT 
				expiry_type,
				mainCat,
				subCat,
				item,
				itemId, 
				sum(stock) as stock ,
				IFNULL(sum(IF(DIFF <= 0, stock ,0)),0)  as expired,
				IFNULL(sum(IF(DIFF > 0 || DIFF IS NULL, stock, 0)),0)  as none_expired 
				FROM
				(
				SELECT
				1 as expiry_type,
				mst_maincategory.strName AS mainCat,
				mst_subcategory.strName AS subCat,
				mst_item.strName as item,
				ware_stocktransactions_bulk.intItemId as itemId, 
				(ware_stocktransactions_bulk.dblQty) as stock, 
				IFNULL(DATEDIFF(DATE(ware_grndetails.EXPIRE_DATE),concat($year,'-',LPAD($month+1,2,0),'-',LPAD('01',2,0))),1) as DIFF ,
				ware_stocktransactions_bulk.dtGRNDate as grnDate 
				FROM
				ware_stocktransactions_bulk
				INNER JOIN mst_item ON ware_stocktransactions_bulk.intItemId = mst_item.intId
				INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
				INNER JOIN ware_grnheader ON ware_stocktransactions_bulk.intGRNNo = ware_grnheader.intGrnNo AND ware_stocktransactions_bulk.intGRNYear = ware_grnheader.intGrnYear AND ware_stocktransactions_bulk.dtGRNDate = ware_grnheader.datdate
				LEFT JOIN ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear AND ware_stocktransactions_bulk.intItemId = ware_grndetails.intItemId
				WHERE
				/*mst_item.intMainCategory = '$mainCategory' AND
				mst_item.intSubCategory = '$subCategory' AND */
				ware_stocktransactions_bulk.intItemId = '$itemId' AND
				ware_stocktransactions_bulk.intCompanyId = '$company' AND
				ware_stocktransactions_bulk.intLocationId = '$location'  AND 
				DATEDIFF(DATE(ware_stocktransactions_bulk.dtDate ),concat($year,'-',LPAD($month+1,2,0),'-',LPAD('01',2,0))) < 0
				
 				) AS TB1 
				GROUP BY 
				TB1.itemId 
				ORDER BY 
				TB1.mainCat asc,
				TB1.subCat asc,
				TB1.item asc,
				TB1.expiry_type asc  
				
					";
				//echo $sql;
	
	return $sql;
	}
	
	public function get_aging_data_sql($company,$location,$itemId,$toDate,$from,$to,$base_currency_id){
		global $db;
		$sql = "SELECT
					mst_financeexchangerate.dblBuying
				FROM
					mst_financeexchangerate
					INNER JOIN mst_locations ON mst_financeexchangerate.intCompanyId = mst_locations.intCompanyId
				WHERE
					mst_financeexchangerate.intCurrencyId =  '$base_currency_id' AND
					mst_locations.intId =  '$location'
				ORDER BY
					mst_financeexchangerate.dtmDate DESC
				LIMIT 1
				";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$toCurrencyRate = $row['dblBuying'];
		if($toCurrencyRate<=0)
			$toCurrencyRate=1;
	
		if($to == '')
		$para	= " HAVING DIFF > $from";
		else if($from==0)
		$para	= " HAVING DIFF >= $from  AND DIFF <= $to ";
		else
		$para	= " HAVING DIFF > $from  AND DIFF <= $to ";
		
		$sql	= "
				SELECT 
				mainCat,
				subCat,
				item,
				itemId, 
				sum(stock) as stock ,
				sum(val) as val 
 				FROM
				(
				SELECT
				mst_maincategory.strName AS mainCat,
				mst_subcategory.strName AS subCat,
				mst_item.strName as item,
				ware_stocktransactions_bulk.intItemId as itemId, 
				(ware_stocktransactions_bulk.dblQty) as stock, 
				/*IFNULL((ware_stocktransactions_bulk.dblQty*ware_stocktransactions_bulk.dblGRNRate*saved_exc.dblBuying/convert_exc.dblBuying),0) as val,  */ /* commented on 2017-05-05) */
				round(( (ware_grnheader.dblExchangeRate *ware_stocktransactions_bulk.dblQty * ware_stocktransactions_bulk.dblGRNRate)/$toCurrencyRate),4   ) AS val ,
				DATEDIFF(DATE(".($toDate==''?'NOW()':"'$toDate'").") , DATE(ware_stocktransactions_bulk.dtGRNDate)) as DIFF ,
				ware_stocktransactions_bulk.dtGRNDate as grnDate ,
				ware_stocktransactions_bulk.dblGRNRate as grnRate,
				ware_stocktransactions_bulk.intCurrencyId ,   
				ware_stocktransactions_bulk.intCompanyId 
				FROM
				ware_stocktransactions_bulk
				INNER JOIN mst_item ON ware_stocktransactions_bulk.intItemId = mst_item.intId
				INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
				INNER JOIN ware_grnheader ON ware_stocktransactions_bulk.intGRNNo = ware_grnheader.intGrnNo AND ware_stocktransactions_bulk.intGRNYear = ware_grnheader.intGrnYear /*AND ware_stocktransactions_bulk.dtGRNDate = ware_grnheader.datdate*/
				LEFT JOIN ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear AND ware_stocktransactions_bulk.intItemId = ware_grndetails.intItemId
			
				LEFT JOIN mst_financeexchangerate as saved_exc ON ware_stocktransactions_bulk.intCurrencyId = saved_exc.intCurrencyId  
				AND DATE(ware_stocktransactions_bulk.dtGRNDate) = saved_exc.dtmDate AND 
				ware_stocktransactions_bulk.intCompanyId = saved_exc.intCompanyId 
			
				LEFT JOIN mst_financeexchangerate as convert_exc ON $base_currency_id = convert_exc.intCurrencyId  
				AND DATE(ware_stocktransactions_bulk.dtGRNDate) = convert_exc.dtmDate AND 
				ware_stocktransactions_bulk.intCompanyId = convert_exc.intCompanyId 
				
				
				WHERE
				/*mst_item.intMainCategory = '$mainCategory' AND
				mst_item.intSubCategory = '$subCategory' AND */
				ware_stocktransactions_bulk.intItemId = '$itemId' AND
				ware_stocktransactions_bulk.intCompanyId = '$company' AND
				ware_stocktransactions_bulk.intLocationId = '$location' AND 
				DATE(ware_stocktransactions_bulk.dtDate) <= '$toDate'  
				$para 
 				) AS TB1 
				
				 
				GROUP BY 
				TB1.itemId 
				ORDER BY 
				TB1.mainCat asc,
				TB1.subCat asc,
				TB1.item asc  
				
					";
				//echo $sql;
	
	return $sql;
	}
	
	public function get_bulk_gate_pass_details($date_from,$date_to,$company,$location,$mainCat,$subCat,$item,$base_currency_id){
		
 		
		$sql	= "
				SELECT 
				mst_companies.strName 								AS COMPANY,
				mst_locations.strName 								AS LOCATION,
				TOLOC.strName										AS TOLOCATION,
				TB1.intMainCategory 								AS MAIN_CAT_ID,
				TB1.intSubCategory	 								AS SUB_CAT_ID,
				mst_maincategory.strName 							AS MAIN_CAT,
				mst_subcategory.strName								AS SUB_CAT,
				item												AS ITEM,
				itemId												AS ITEM_ID, 
				mst_units.strCode									AS UNIT, 
				CONCAT(TB1.intDocumentNo,'/',TB1.intDocumntYear)	AS SERIAL_NO,
				sum(TB1.dblQty*-1) as QTY, 
				sum(IFNULL((TB1.dblQty*-1*TB1.dblGRNRate*ware_grnheader.dblExchangeRate),0)) as VALUE ,  								DATE(GPH.datdate)									AS GP_DATE 
				FROM
				
				(
				SELECT 
				ware_stocktransactions_bulk.intCompanyId,
				ware_stocktransactions_bulk.intLocationId, 
				ware_stocktransactions_bulk.intDocumentNo,
				ware_stocktransactions_bulk.intDocumntYear,
				mst_item.intMainCategory,	
				mst_item.intSubCategory,
				mst_item.strName as item,
				mst_item.intUOM as unitId,
				ware_stocktransactions_bulk.intItemId as itemId, 
				ware_stocktransactions_bulk.intGRNNo, 
				ware_stocktransactions_bulk.intGRNYear, 
				ware_stocktransactions_bulk.dtGRNDate  ,
				ware_stocktransactions_bulk.dblGRNRate ,
				ware_stocktransactions_bulk.intCurrencyId,
				ware_stocktransactions_bulk.dblQty 
				 
				FROM
				ware_stocktransactions_bulk
				INNER JOIN mst_item ON ware_stocktransactions_bulk.intItemId = mst_item.intId
				WHERE 
				ware_stocktransactions_bulk.strType = 'GATEPASS' ";
				if($date_from != '')
				$sql .= " AND DATEDIFF(DATE(ware_stocktransactions_bulk.dtDate) ,'$date_from') >= 0 ";
				if($date_to != '')
				$sql .= " AND DATEDIFF(DATE(ware_stocktransactions_bulk.dtDate) ,'$date_to' ) <= 0";
				if($mainCat != '')
				$sql .= " AND mst_item.intMainCategory = '$mainCat' ";
				if($subCat != '')
				$sql .= " AND mst_item.intSubCategory = '$subCat' ";
				if($item != '')
				$sql .= " AND ware_stocktransactions_bulk.intItemId = '$item' ";
				if($company != '')
				$sql .= " AND ware_stocktransactions_bulk.intCompanyId = '$company' ";
				if($location != '')
				$sql .= " AND ware_stocktransactions_bulk.intLocationId = '$location' ";   
 				$sql .= ") AS TB1  
				
				INNER JOIN mst_maincategory ON TB1.intMainCategory = mst_maincategory.intId
				INNER JOIN mst_subcategory ON TB1.intSubCategory = mst_subcategory.intId
				INNER JOIN ware_grnheader ON TB1.intGRNNo = ware_grnheader.intGrnNo AND TB1.intGRNYear = ware_grnheader.intGrnYear AND TB1.dtGRNDate = ware_grnheader.datdate
				LEFT JOIN ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear 
				AND TB1.itemId = ware_grndetails.intItemId
				INNER JOIN mst_locations ON mst_locations.intId	= TB1.intLocationId 
				INNER JOIN mst_companies ON TB1.intCompanyId	= mst_companies.intId  
				INNER JOIN mst_units ON  TB1.unitId	= mst_units.intId  
				
				LEFT JOIN mst_financeexchangerate as saved_exc ON TB1.intCurrencyId = saved_exc.intCurrencyId  
				AND DATE(TB1.dtGRNDate) = saved_exc.dtmDate AND 
				TB1.intCompanyId = saved_exc.intCompanyId 
			
				LEFT JOIN mst_financeexchangerate as convert_exc ON $base_currency_id = convert_exc.intCurrencyId  
				AND DATE(TB1.dtGRNDate) = convert_exc.dtmDate AND 
				TB1.intCompanyId = convert_exc.intCompanyId 
				
				LEFT JOIN ware_gatepassheader AS GPH ON TB1.intDocumentNo = GPH.intGatePassNo AND TB1.intDocumntYear = GPH.intGatePassYear
				INNER JOIN mst_locations AS TOLOC ON TOLOC.intId = GPH.intGPToLocation
				GROUP BY  
				TB1.intCompanyId,
				TB1.intLocationId,
				TB1.intDocumntYear,
				TB1.intDocumentNo,
				TB1.itemId 
				ORDER BY  
				
				mst_companies.strName ASC,
				mst_locations.strName ASC,
				TB1.intDocumntYear ASC,
				TB1.intDocumentNo ASC,
				mst_maincategory.strName ASC,
				mst_subcategory.strName ASC,
				item	 ASC,
				TB1.intDocumntYear ASC ,
				TB1.intDocumentNo ASC 		";
		
		return $sql;
	}
    public function get_bulk_tranferin_details($date_from,$date_to,$company,$location,$mainCat,$subCat,$item,$base_currency_id){


        $sql	= "
				SELECT 
				mst_companies.strName 								AS COMPANY,
				mst_locations.strName 								AS LOCATION,
				CONCAT(GPTH.intGatePassNo,'/',GPTH.intGatePassYear) AS TOLOCATION,
				TB1.intMainCategory 								AS MAIN_CAT_ID,
				TB1.intSubCategory	 								AS SUB_CAT_ID,
				mst_maincategory.strName 							AS MAIN_CAT,
				mst_subcategory.strName								AS SUB_CAT,
				item												AS ITEM,
				itemId												AS ITEM_ID, 
				mst_units.strCode									AS UNIT, 
				CONCAT(TB1.intDocumentNo,'/',TB1.intDocumntYear)	AS SERIAL_NO,
				sum(TB1.dblQty*-1) as QTY, 
				sum(IFNULL((TB1.dblQty*-1*TB1.dblGRNRate*ware_grnheader.dblExchangeRate),0)) as VALUE ,  
				DATE(GPTH.datdate)									AS GP_DATE 
				FROM
				
				(
				SELECT 
				ware_stocktransactions_bulk.intCompanyId,
				ware_stocktransactions_bulk.intLocationId, 
				ware_stocktransactions_bulk.intDocumentNo,
				ware_stocktransactions_bulk.intDocumntYear,
				mst_item.intMainCategory,	
				mst_item.intSubCategory,
				mst_item.strName as item,
				mst_item.intUOM as unitId,
				ware_stocktransactions_bulk.intItemId as itemId, 
				ware_stocktransactions_bulk.intGRNNo, 
				ware_stocktransactions_bulk.intGRNYear, 
				ware_stocktransactions_bulk.dtGRNDate  ,
				ware_stocktransactions_bulk.dblGRNRate ,
				ware_stocktransactions_bulk.intCurrencyId,
				ware_stocktransactions_bulk.dblQty 
				 
				FROM
				ware_stocktransactions_bulk
				INNER JOIN mst_item ON ware_stocktransactions_bulk.intItemId = mst_item.intId
				WHERE 
				ware_stocktransactions_bulk.strType = 'TRANSFERIN' ";
        if($date_from != '')
            $sql .= " AND DATEDIFF(DATE(ware_stocktransactions_bulk.dtDate) ,'$date_from') >= 0 ";
        if($date_to != '')
            $sql .= " AND DATEDIFF(DATE(ware_stocktransactions_bulk.dtDate) ,'$date_to' ) <= 0";
        if($mainCat != '')
            $sql .= " AND mst_item.intMainCategory = '$mainCat' ";
        if($subCat != '')
            $sql .= " AND mst_item.intSubCategory = '$subCat' ";
        if($item != '')
            $sql .= " AND ware_stocktransactions_bulk.intItemId = '$item' ";
        if($company != '')
            $sql .= " AND ware_stocktransactions_bulk.intCompanyId = '$company' ";
        if($location != '')
            $sql .= " AND ware_stocktransactions_bulk.intLocationId = '$location' ";
        $sql .= ") AS TB1  
				
				INNER JOIN mst_maincategory ON TB1.intMainCategory = mst_maincategory.intId
				INNER JOIN mst_subcategory ON TB1.intSubCategory = mst_subcategory.intId
				INNER JOIN ware_grnheader ON TB1.intGRNNo = ware_grnheader.intGrnNo AND TB1.intGRNYear = ware_grnheader.intGrnYear AND TB1.dtGRNDate = ware_grnheader.datdate
				LEFT JOIN ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear 
				AND TB1.itemId = ware_grndetails.intItemId
				INNER JOIN mst_locations ON mst_locations.intId	= TB1.intLocationId 
				INNER JOIN mst_companies ON TB1.intCompanyId	= mst_companies.intId  
				INNER JOIN mst_units ON  TB1.unitId	= mst_units.intId  
				
				LEFT JOIN mst_financeexchangerate as saved_exc ON TB1.intCurrencyId = saved_exc.intCurrencyId  
				AND DATE(TB1.dtGRNDate) = saved_exc.dtmDate AND 
				TB1.intCompanyId = saved_exc.intCompanyId 
			
				LEFT JOIN mst_financeexchangerate as convert_exc ON $base_currency_id = convert_exc.intCurrencyId  
				AND DATE(TB1.dtGRNDate) = convert_exc.dtmDate AND 
				TB1.intCompanyId = convert_exc.intCompanyId 
				
				LEFT JOIN ware_gatepasstransferinheader AS GPTH ON TB1.intDocumentNo = GPTH.intGpTransfNo AND TB1.intDocumntYear = GPTH.intGpTransfYear 
				GROUP BY  
				TB1.intCompanyId,
				TB1.intLocationId,
				TB1.intDocumntYear,
				TB1.intDocumentNo,
				TB1.itemId 
				ORDER BY  
				
				mst_companies.strName ASC,
				mst_locations.strName ASC,
				TB1.intDocumntYear ASC,
				TB1.intDocumentNo ASC,
				mst_maincategory.strName ASC,
				mst_subcategory.strName ASC,
				item	 ASC,
				TB1.intDocumntYear ASC ,
				TB1.intDocumentNo ASC 		";
        return $sql;
    }

    private function Load_sub_stores_sql($location){
		
		 $sql = "SELECT
				mst_substores.intId,
				mst_substores.strName
				FROM mst_substores
				WHERE
				mst_substores.intStatus = 1 
				AND intLocation = '$location'" ;
		return $sql;
	}
	private function get_order_nos_sql($location){
		
		$sql = "SELECT DISTINCT
				trn_orderheader.intOrderNo,
				trn_orderheader.intOrderYear
				FROM trn_orderheader 
				WHERE
				trn_orderheader.intStatus =  '1' and intLocationId='$location' order by  intOrderYear,intOrderNo desc";
 		
		return $sql;
	}

	private function get_sales_oreder_nos_sql($orderNo,$orderYear,$salesOrder){
		
		 $sql = "SELECT DISTINCT 
		 		trn_orderdetails.intSalesOrderId, 
				trn_orderdetails.strSalesOrderNo,
				mst_part.strName
				FROM
				trn_orderdetails
				Inner Join mst_part ON trn_orderdetails.intPart = mst_part.intId 
				WHERE 
				trn_orderdetails.intOrderNo =  '$orderNo' AND
				trn_orderdetails.intOrderYear =  '$orderYear'";
				
				if($salesOrder != ''){
		$sql .=	" AND trn_orderdetails.intSalesOrderId =  '$salesOrder' ";		
				}
 		
		return $sql;
	}
	

	private function get_main_category_sql(){
		
		$sql = "SELECT DISTINCT
				mst_maincategory.intId,
				mst_maincategory.strName
				FROM
				mst_subcategory
				Inner Join mst_maincategory ON mst_subcategory.intMainCategory = mst_maincategory.intId
				WHERE
				mst_maincategory.intStatus =  '1' "; 
 		
		return $sql;
	}

	private function get_sub_category_sql($mainCatId){
		
		$sql = "SELECT
				mst_subcategory.intId,
				mst_subcategory.strCode,
				mst_subcategory.strName
				FROM mst_subcategory ";  
		if($mainCatId!=''){
		$sql .= "WHERE
				mst_subcategory.intMainCategory =  '$mainCatId'";
		}

		return $sql;
	}
	
	private function get_stores_trans_items_sql($orderNo,$orderYear,$salesOrderNo,$mainCategory,$subCategory,$description)
	{
	
 		$sql = "select 
				mst_item.strName as itemName,
				mst_item.intId, 
				mst_item.intMainCategory,
				mst_item.intSubCategory,
				mst_maincategory.strName as mainCatName,
				mst_subcategory.strName as subCatName,
				mst_item.strCode , 
				mst_units.strCode as uom ,
				trn_orderdetails.intQty as orderQty,
				IFNULL(consum,0)  as consum ,
				IFNULL(trn_orderdetails.intQty*consum,0)  as required
 						
						 from ( select 
						1 as type, 
						sc.intSampleNo, 
						sc.intSampleYear, 
						sc.intRevisionNo, 
						sc.strCombo, 
						sc.strPrintName,
						 mst_item.intId as intItem,
						 sum(round(dblColorWeight/((SELECT
						Sum(trn_sample_color_recipes.dblWeight)  
						FROM trn_sample_color_recipes
						WHERE
						trn_sample_color_recipes.intSampleNo = sc.intSampleNo AND
						trn_sample_color_recipes.intSampleYear = sc.intSampleYear AND
						trn_sample_color_recipes.intRevisionNo =  sc.intRevisionNo AND
						trn_sample_color_recipes.strCombo =  sc.strCombo AND
						trn_sample_color_recipes.strPrintName =  sc.strPrintName AND
						trn_sample_color_recipes.intTechniqueId =  sc.intTechniqueId AND
						trn_sample_color_recipes.intInkTypeId =  trn_sampleinfomations_details_technical.intInkTypeId AND
						trn_sample_color_recipes.intColorId =  trn_sampleinfomations_details_technical.intColorId 
						 ))*dblWeight /dblNoOfPcs,6))as consum 
						FROM
						trn_sample_color_recipes as sc
						Inner Join trn_sampleinfomations_details_technical ON trn_sampleinfomations_details_technical.intSampleNo = sc.intSampleNo 
						AND trn_sampleinfomations_details_technical.intSampleYear = sc.intSampleYear 
						AND trn_sampleinfomations_details_technical.intRevNo = sc.intRevisionNo 
						AND trn_sampleinfomations_details_technical.strComboName = sc.strCombo 
						AND trn_sampleinfomations_details_technical.strPrintName = sc.strPrintName 
						AND trn_sampleinfomations_details_technical.intColorId = sc.intColorId 
						AND trn_sampleinfomations_details_technical.intInkTypeId = sc.intInkTypeId
						Inner Join mst_item ON mst_item.intId = sc.intItem
						Inner Join mst_units ON mst_item.intUOM = mst_units.intId
						group by 
						sc.intSampleNo, 
						sc.intSampleYear, 
						sc.intRevisionNo, 
						sc.strCombo, 
						sc.strPrintName ,
						 mst_item.intId 
 				 ) as MAIN  
				
				
				INNER JOIN trn_orderdetails ON trn_orderdetails.intSampleNo=MAIN.intSampleNo AND 
				trn_orderdetails.intSampleYear=MAIN.intSampleYear AND 
				trn_orderdetails.intRevisionNo=MAIN.intRevisionNo AND 
				trn_orderdetails.strCombo=MAIN.strCombo AND 
				trn_orderdetails.strPrintName=MAIN.strPrintName  
				INNER JOIN mst_item ON mst_item.intId=MAIN.intItem 
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
				WHERE  
				trn_orderdetails.intOrderNo='$orderNo' AND 
				trn_orderdetails.intOrderYear='$orderYear' AND 
				trn_orderdetails.intSalesOrderId='$salesOrderNo' AND 
				mst_item.intMainCategory =  '$mainCategory' AND 
				mst_item.intStatus='1'";
				if($subCategory!='')
				$sql.=" AND mst_item.intSubCategory =  '$subCategory'";
				if($description!=''){
				$sql.=" AND
				mst_item.strName LIKE  '%$description%'";
				}
				$sql.=" Order by mst_maincategory.strName asc, mst_subcategory.strName asc, mst_item.strName asc";
				//echo $sql;
				return $sql;
		
	}
	
	private function get_bulk_stock_balance_sql($location,$item){
		$sql = "SELECT
				IFNULL(Sum(ware_stocktransactions_bulk.dblQty),0) as bal 
				FROM `ware_stocktransactions_bulk`
				WHERE
				ware_stocktransactions_bulk.intItemId = '$item' AND
				ware_stocktransactions_bulk.intLocationId = '$location'
				";
		return $sql;
 	}
	
	private function get_bulk_stock_balance_to_given_date_sql($location,$item,$toDate){
		$sql = "SELECT
				IFNULL(Sum(ware_stocktransactions_bulk.dblQty),0) as bal 
				FROM `ware_stocktransactions_bulk`
				WHERE
				ware_stocktransactions_bulk.intItemId = '$item' AND
				ware_stocktransactions_bulk.intLocationId = '$location' 
				and DATE(dtDate) <= '$toDate'
				";
		return $sql;
 	}
	
	
	private function get_bulk_stock_balance_with_or_without_location_sql($location,$item){
		$sql = "SELECT
				IFNULL(Sum(ware_stocktransactions_bulk.dblQty),0) as bal 
				FROM `ware_stocktransactions_bulk`
				WHERE
				ware_stocktransactions_bulk.intItemId = '$item' ";
				
		if($location != ''){
				$sql .= " AND
				ware_stocktransactions_bulk.intLocationId = '$location'
				";
		}
		return $sql;
 	}
	

	private function get_bulk_stock_val_sql($location,$item,$to_currency){
		$sql = "SELECT
		
				/*IFNULL(Sum(ware_stocktransactions_bulk.dblQty),0) as val */
				IFNULL(sum(ware_stocktransactions_bulk.dblQty*ware_stocktransactions_bulk.dblGRNRate*saved_exc.dblBuying/convert_exc.dblBuying),0) as val   
			
				FROM `ware_stocktransactions_bulk`
				
				LEFT JOIN mst_financeexchangerate as saved_exc ON ware_stocktransactions_bulk.intCurrencyId = saved_exc.intCurrencyId  
				AND DATE(ware_stocktransactions_bulk.dtGRNDate) = saved_exc.dtmDate AND 
				ware_stocktransactions_bulk.intCompanyId = saved_exc.intCompanyId 
			
				LEFT JOIN mst_financeexchangerate as convert_exc ON $to_currency = convert_exc.intCurrencyId  
				AND DATE(ware_stocktransactions_bulk.dtGRNDate) = convert_exc.dtmDate AND 
				ware_stocktransactions_bulk.intCompanyId = convert_exc.intCompanyId 
				
				WHERE
				ware_stocktransactions_bulk.intItemId = '$item' AND
				ware_stocktransactions_bulk.intLocationId = '$location'
				";
				
				
		return $sql;
 	}
	
	private function get_bulk_stock_val_with_or_without_location_sql($location,$item,$to_currency){
		$sql = "SELECT
		
				/*IFNULL(Sum(ware_stocktransactions_bulk.dblQty),0) as val */
				IFNULL(sum(ware_stocktransactions_bulk.dblQty*ware_stocktransactions_bulk.dblGRNRate*saved_exc.dblBuying/convert_exc.dblBuying),0) as val   
			
				FROM `ware_stocktransactions_bulk`
				
				LEFT JOIN mst_financeexchangerate as saved_exc ON ware_stocktransactions_bulk.intCurrencyId = saved_exc.intCurrencyId  
				AND DATE(ware_stocktransactions_bulk.dtGRNDate) = saved_exc.dtmDate AND 
				ware_stocktransactions_bulk.intCompanyId = saved_exc.intCompanyId 
			
				LEFT JOIN mst_financeexchangerate as convert_exc ON $to_currency = convert_exc.intCurrencyId  
				AND DATE(ware_stocktransactions_bulk.dtGRNDate) = convert_exc.dtmDate AND 
				ware_stocktransactions_bulk.intCompanyId = convert_exc.intCompanyId 
				
				WHERE
				ware_stocktransactions_bulk.intItemId = '$item' ";
				if($location !=''){
					$sql .= " AND
					ware_stocktransactions_bulk.intLocationId = '$location'
				";
				}
				
				
		return $sql;
		
		
	}
	
	
	private function get_bulk_stock_val_to_given_date_sql($location,$item,$toDate,$to_currency,$executeType){
		
		global $db;
		$sql = "SELECT
					mst_financeexchangerate.dblBuying
				FROM
					mst_financeexchangerate
					INNER JOIN mst_locations ON mst_financeexchangerate.intCompanyId = mst_locations.intCompanyId
				WHERE
					mst_financeexchangerate.intCurrencyId =  '$to_currency' AND
					mst_locations.intId =  '$location'
				ORDER BY
					mst_financeexchangerate.dtmDate DESC
				LIMIT 1
				";
		$result = $db->$executeType($sql);
		$row=mysqli_fetch_array($result);
		$toCurrencyRate = $row['dblBuying'];
		if($toCurrencyRate<=0)
			$toCurrencyRate=1;
		
		$sql = "SELECT
		
				/*IFNULL(Sum(ware_stocktransactions_bulk.dblQty),0) as val */
				/*IFNULL(sum(ware_stocktransactions_bulk.dblQty*ware_stocktransactions_bulk.dblGRNRate*saved_exc.dblBuying/convert_exc.dblBuying),0) as val*/   
				round(  sum( (ware_grnheader.dblExchangeRate *ware_stocktransactions_bulk.dblQty * ware_stocktransactions_bulk.dblGRNRate)/$toCurrencyRate),4   ) AS val /*commented on 2017-05-04*/
/*				round(IFNULL(sum(ware_stocktransactions_bulk.dblQty*ware_stocktransactions_bulk.dblGRNRate*saved_exc.dblBuying/convert_exc.dblBuying),0),4) as val */ 
			
				FROM `ware_stocktransactions_bulk`
				
				LEFT JOIN mst_financeexchangerate as saved_exc ON ware_stocktransactions_bulk.intCurrencyId = saved_exc.intCurrencyId  
				AND DATE(ware_stocktransactions_bulk.dtGRNDate) = saved_exc.dtmDate AND 
				ware_stocktransactions_bulk.intCompanyId = saved_exc.intCompanyId 
			
				LEFT JOIN mst_financeexchangerate as convert_exc ON $to_currency = convert_exc.intCurrencyId  
				AND DATE(ware_stocktransactions_bulk.dtGRNDate) = convert_exc.dtmDate AND 
				ware_stocktransactions_bulk.intCompanyId = convert_exc.intCompanyId 
			
				left Join ware_grnheader ON ware_grnheader.intGrnNo = ware_stocktransactions_bulk.intGRNNo AND ware_grnheader.intGrnYear = ware_stocktransactions_bulk.intGRNYear
				
				WHERE
				ware_stocktransactions_bulk.intItemId = '$item' AND
				ware_stocktransactions_bulk.intLocationId = '$location' 
				and DATE(ware_stocktransactions_bulk.dtDate) <= '$toDate'
				GROUP BY
					ware_stocktransactions_bulk.intLocationId,
					ware_stocktransactions_bulk.intItemId
				";
				
				
		return $sql;
 	}

	
	private function get_order_qty_sql($orderNo,$orderYear,$salesOrder){
		$sql = "SELECT
				Sum(trn_orderdetails.intQty) as qty 
				FROM `trn_orderdetails`
				WHERE
				trn_orderdetails.intOrderNo = '$orderNo' AND
				trn_orderdetails.intOrderYear = '$orderYear' AND
				trn_orderdetails.intSalesOrderId = '$salesOrder'
				";
		return $sql;
 	}
	
	private function get_order_production_qty_sql($orderNo,$orderYear,$salesOrder){
	
		$sql = "SELECT
				Sum(trn_orderdetails.intQty+trn_orderdetails.intQty*trn_orderdetails.dblDamagePercentage/100) as qty 
				FROM `trn_orderdetails`
				WHERE
				trn_orderdetails.intOrderNo = '$orderNo' AND
				trn_orderdetails.intOrderYear = '$orderYear' AND
				trn_orderdetails.intSalesOrderId = '$salesOrder'
				";
		return $sql;
		
	}
	
	private function  get_order_item_first_consump_type($orderNo,$orderYear,$item,$salesOrder){
		$sql	="SELECT
				MIN(trn_sample_size_wise_item_consumptions.CONSUMPTION_TYPE_ID) as type
				FROM `trn_sample_size_wise_item_consumptions`
				WHERE
				trn_sample_size_wise_item_consumptions.ORDER_NO = '$orderNo' AND
				trn_sample_size_wise_item_consumptions.ORDER_YEAR = '$orderYear' AND
				trn_sample_size_wise_item_consumptions.SALES_ORDER_ID = '$salesOrder' AND
				trn_sample_size_wise_item_consumptions.ITEM_ID = '$item' AND
				trn_sample_size_wise_item_consumptions.CONSUMPTION IS NOT NULL";
		return $sql;
	}
	
	private function get_order_item_sample_consumption_sql($orderNo,$orderYear,$item,$salesOrder){
		
 		$sql = "select 
				mst_item.strName as itemName,
				mst_item.intId, 
				mst_item.intMainCategory,
				mst_item.intSubCategory,
				mst_maincategory.strName as mainCatName,
				mst_subcategory.strName as subCatName,
				mst_item.strCode , 
				mst_units.strCode as uom ,
				/*
				trn_orderdetails.intQty as orderQty,
				IFNULL(consum,0)  as consum ,
				IFNULL(trn_orderdetails.intQty*consum,0)  as required,
				*/
				round(SUM(trn_orderdetails.intQty+trn_orderdetails.intQty*trn_orderdetails.dblDamagePercentage/100)) as orderQty,
				SUM(IFNULL(consum,0))  as consum ,
				round(SUM(IFNULL((trn_orderdetails.intQty+trn_orderdetails.intQty*trn_orderdetails.dblDamagePercentage/100)*consum,0)))  as required,
				
				MAIN.TYPE AS allocationType
 						
						 from (( select 
						1 as type, 
						sc.intSampleNo, 
						sc.intSampleYear, 
						sc.intRevisionNo, 
						sc.strCombo, 
						sc.strPrintName,
						 mst_item.intId as intItem,
						 sum(round(dblColorWeight/((SELECT
						Sum(trn_sample_color_recipes.dblWeight)  
						FROM trn_sample_color_recipes
						WHERE
						trn_sample_color_recipes.intSampleNo = sc.intSampleNo AND
						trn_sample_color_recipes.intSampleYear = sc.intSampleYear AND
						trn_sample_color_recipes.intRevisionNo =  sc.intRevisionNo AND
						trn_sample_color_recipes.strCombo =  sc.strCombo AND
						trn_sample_color_recipes.strPrintName =  sc.strPrintName AND
						trn_sample_color_recipes.intTechniqueId =  sc.intTechniqueId AND
						trn_sample_color_recipes.intInkTypeId =  trn_sampleinfomations_details_technical.intInkTypeId AND
						trn_sample_color_recipes.intColorId =  trn_sampleinfomations_details_technical.intColorId 
						 ))*dblWeight /dblNoOfPcs,6))as consum 
						FROM
						trn_sample_color_recipes as sc
						Inner Join trn_sampleinfomations_details_technical ON trn_sampleinfomations_details_technical.intSampleNo = sc.intSampleNo 
						AND trn_sampleinfomations_details_technical.intSampleYear = sc.intSampleYear 
						AND trn_sampleinfomations_details_technical.intRevNo = sc.intRevisionNo 
						AND trn_sampleinfomations_details_technical.strComboName = sc.strCombo 
						AND trn_sampleinfomations_details_technical.strPrintName = sc.strPrintName 
						AND trn_sampleinfomations_details_technical.intColorId = sc.intColorId 
						AND trn_sampleinfomations_details_technical.intInkTypeId = sc.intInkTypeId
						Inner Join mst_item ON mst_item.intId = sc.intItem
						Inner Join mst_units ON mst_item.intUOM = mst_units.intId
						group by 
						sc.intSampleNo, 
						sc.intSampleYear, 
						sc.intRevisionNo, 
						sc.strCombo, 
						sc.strPrintName ,
						 mst_item.intId 
 				 )
UNION
(
SELECT  
		2 as type, 
trn_sample_spitem_consumption.intSampleNo, 
trn_sample_spitem_consumption.intSampleYear, 
trn_sample_spitem_consumption.intRevisionNo, 
trn_sample_spitem_consumption.strCombo, 
trn_sample_spitem_consumption.strPrintName,
 mst_item.intId, 
       sum(trn_sample_spitem_consumption.dblQty/dblNoOfPcs) as consum 
       FROM
	trn_sample_spitem_consumption 
        Inner Join mst_item ON mst_item.intId = trn_sample_spitem_consumption.intItem
	Inner Join mst_units ON mst_units.intId = mst_item.intUOM
group by 
trn_sample_spitem_consumption.intSampleNo, 
trn_sample_spitem_consumption.intSampleYear, 
trn_sample_spitem_consumption.intRevisionNo, 
trn_sample_spitem_consumption.strCombo, 
trn_sample_spitem_consumption.strPrintName ,
 mst_item.intId 
)

		UNION 
		(
		SELECT    
				3 as type, 
		trn_sample_foil_consumption.intSampleNo, 
		trn_sample_foil_consumption.intSampleYear, 
		trn_sample_foil_consumption.intRevisionNo, 
		trn_sample_foil_consumption.strCombo, 
		trn_sample_foil_consumption.strPrintName,
		 mst_item.intId, 
				sum(trn_sample_foil_consumption.dblMeters) as consum 
			FROM 
				trn_sample_foil_consumption  
				Inner Join mst_item ON mst_item.intId = trn_sample_foil_consumption.intItem
				Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
				Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
				Inner Join mst_units ON mst_units.intId = mst_item.intUOM
		group by 
		trn_sample_foil_consumption.intSampleNo, 
		trn_sample_foil_consumption.intSampleYear, 
		trn_sample_foil_consumption.intRevisionNo, 
		trn_sample_foil_consumption.strCombo, 
		trn_sample_foil_consumption.strPrintName ,
		 mst_item.intId 
		)				 
			)	  as MAIN  
				
				
				INNER JOIN trn_orderdetails ON trn_orderdetails.intSampleNo=MAIN.intSampleNo AND 
				trn_orderdetails.intSampleYear=MAIN.intSampleYear AND 
				trn_orderdetails.intRevisionNo=MAIN.intRevisionNo AND 
				trn_orderdetails.strCombo=MAIN.strCombo AND 
				trn_orderdetails.strPrintName=MAIN.strPrintName  
				INNER JOIN mst_item ON mst_item.intId=MAIN.intItem 
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
				WHERE  
				trn_orderdetails.intOrderNo='$orderNo' AND 
				trn_orderdetails.intOrderYear='$orderYear' AND 
				mst_item.intStatus='1'  ";
				if($salesOrder!=''){
				$sql .=" AND trn_orderdetails.intSalesOrderId = '$salesOrder'";
				}
				if($item!=''){
				$sql .=" AND mst_item.intId = '$item'";
				}

				return $sql;

	}
	
	private function get_order_item_sample_consumption_sales_order_wise_sql($orderNo,$orderYear,$item,$salesOrder){
		
 		$sql = "select 
				trn_orderdetails.intSalesOrderId,
				mst_item.strName as itemName,
				mst_item.intId, 
				mst_item.intMainCategory,
				mst_item.intSubCategory,
				mst_maincategory.strName as mainCatName,
				mst_subcategory.strName as subCatName,
				mst_item.strCode , 
				mst_units.strCode as uom ,
				/*
				SUM(trn_orderdetails.intQty) as orderQty,
				SUM(IFNULL(consum,0))  as consum ,
				SUM(IFNULL(trn_orderdetails.intQty*consum,0))  as required,
				*/
				round(SUM(trn_orderdetails.intQty+trn_orderdetails.intQty*trn_orderdetails.dblDamagePercentage/100)) as orderQty,
				round(IFNULL(consum,0),6)  as consum ,
				/*'0.000001' as consum, */
				round(SUM(IFNULL((trn_orderdetails.intQty+trn_orderdetails.intQty*trn_orderdetails.dblDamagePercentage/100)*consum,0)))  as required,
				
				MAIN.TYPE AS allocationType,
				IF(MAIN.TYPE = 1 ,'Ink Item' , IF(MAIN.TYPE = 2,'Special Item' ,'Foil Item' )) as type 
 						
						 from (( select 
						1 as type, 
						sc.intSampleNo, 
						sc.intSampleYear, 
						sc.intRevisionNo, 
						sc.strCombo, 
						sc.strPrintName,
						 mst_item.intId as intItem,
						 sum(round(dblColorWeight/((SELECT
						Sum(trn_sample_color_recipes.dblWeight)  
						FROM trn_sample_color_recipes
						WHERE
						trn_sample_color_recipes.intSampleNo = sc.intSampleNo AND
						trn_sample_color_recipes.intSampleYear = sc.intSampleYear AND
						trn_sample_color_recipes.intRevisionNo =  sc.intRevisionNo AND
						trn_sample_color_recipes.strCombo =  sc.strCombo AND
						trn_sample_color_recipes.strPrintName =  sc.strPrintName AND
						trn_sample_color_recipes.intTechniqueId =  sc.intTechniqueId AND
						trn_sample_color_recipes.intInkTypeId =  trn_sampleinfomations_details_technical.intInkTypeId AND
						trn_sample_color_recipes.intColorId =  trn_sampleinfomations_details_technical.intColorId 
						 ))*dblWeight /dblNoOfPcs,6))as consum 
						FROM
						trn_sample_color_recipes as sc
						Inner Join trn_sampleinfomations_details_technical ON trn_sampleinfomations_details_technical.intSampleNo = sc.intSampleNo 
						AND trn_sampleinfomations_details_technical.intSampleYear = sc.intSampleYear 
						AND trn_sampleinfomations_details_technical.intRevNo = sc.intRevisionNo 
						AND trn_sampleinfomations_details_technical.strComboName = sc.strCombo 
						AND trn_sampleinfomations_details_technical.strPrintName = sc.strPrintName 
						AND trn_sampleinfomations_details_technical.intColorId = sc.intColorId 
						AND trn_sampleinfomations_details_technical.intInkTypeId = sc.intInkTypeId
						Inner Join mst_item ON mst_item.intId = sc.intItem
						Inner Join mst_units ON mst_item.intUOM = mst_units.intId
						group by 
						sc.intSampleNo, 
						sc.intSampleYear, 
						sc.intRevisionNo, 
						sc.strCombo, 
						sc.strPrintName ,
						 mst_item.intId 
 				 )
UNION
(
SELECT  
		2 as type, 
trn_sample_spitem_consumption.intSampleNo, 
trn_sample_spitem_consumption.intSampleYear, 
trn_sample_spitem_consumption.intRevisionNo, 
trn_sample_spitem_consumption.strCombo, 
trn_sample_spitem_consumption.strPrintName,
 mst_item.intId, 
       sum(trn_sample_spitem_consumption.dblQty/dblNoOfPcs) as consum 
       FROM
	trn_sample_spitem_consumption 
        Inner Join mst_item ON mst_item.intId = trn_sample_spitem_consumption.intItem
	Inner Join mst_units ON mst_units.intId = mst_item.intUOM
group by 
trn_sample_spitem_consumption.intSampleNo, 
trn_sample_spitem_consumption.intSampleYear, 
trn_sample_spitem_consumption.intRevisionNo, 
trn_sample_spitem_consumption.strCombo, 
trn_sample_spitem_consumption.strPrintName ,
 mst_item.intId 
)

		UNION 
		(
		SELECT    
				3 as type, 
		trn_sample_foil_consumption.intSampleNo, 
		trn_sample_foil_consumption.intSampleYear, 
		trn_sample_foil_consumption.intRevisionNo, 
		trn_sample_foil_consumption.strCombo, 
		trn_sample_foil_consumption.strPrintName,
		 mst_item.intId, 
				sum(trn_sample_foil_consumption.dblMeters) as consum 
			FROM 
				trn_sample_foil_consumption  
				Inner Join mst_item ON mst_item.intId = trn_sample_foil_consumption.intItem
				Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
				Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
				Inner Join mst_units ON mst_units.intId = mst_item.intUOM
		group by 
		trn_sample_foil_consumption.intSampleNo, 

		trn_sample_foil_consumption.intSampleYear, 
		trn_sample_foil_consumption.intRevisionNo, 
		trn_sample_foil_consumption.strCombo, 
		trn_sample_foil_consumption.strPrintName ,
		 mst_item.intId 
		)				 
			)	  as MAIN  
				
				
				INNER JOIN trn_orderdetails ON trn_orderdetails.intSampleNo=MAIN.intSampleNo AND 
				trn_orderdetails.intSampleYear=MAIN.intSampleYear AND 
				trn_orderdetails.intRevisionNo=MAIN.intRevisionNo AND 
				trn_orderdetails.strCombo=MAIN.strCombo AND 
				trn_orderdetails.strPrintName=MAIN.strPrintName  
				INNER JOIN mst_item ON mst_item.intId=MAIN.intItem 
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
				WHERE  
				trn_orderdetails.intOrderNo='$orderNo' AND 
				trn_orderdetails.intOrderYear='$orderYear' ";
				
				if($salesOrder!=''){
					$sql .=" AND 
					trn_orderdetails.intSalesOrderId='$salesOrder'  ";
				}
 				
				if($item!=''){
					$sql .=" AND mst_item.intId = '$item'";
				}
				
			
					$sql .=" GROUP BY  
							mst_item.intId ";
				//if($salesOrder!=''){
				if($item!=''){
					$sql .=" ,trn_orderdetails.intSalesOrderId ";
				}
				//echo $sql;

				return $sql;
				
					$sql .=" ORDER BY  
							mst_item.strName ASC ";
	}
	
	private function get_order_item_first_second_consumption_sql($orderNo,$orderYear,$item,$salesOrder,$type){
		
	 	$sql	=	"SELECT
					trn_orderdetails.intOrderNo,
					trn_orderdetails.intOrderYear,
					trn_orderdetails.intSalesOrderId,
					trn_orderdetails.strSalesOrderNo,
					trn_orderdetails.intQty,
					trn_ordersizeqty.strSize,
					IFNULL(trn_ordersizeqty.dblQty,0) as sizeQty,
					trn_sample_size_wise_item_consumptions.CONSUMPTION_TYPE_ID,
					trn_sample_size_wise_item_consumptions.SIZE,
					trn_sample_size_wise_item_consumptions.ITEM_ID,
					IFNULL(trn_sample_size_wise_item_consumptions.CONSUMPTION,0) as CONSUMPTION 
					FROM
					trn_orderdetails
					LEFT JOIN trn_ordersizeqty ON trn_orderdetails.intOrderNo = trn_ordersizeqty.intOrderNo AND trn_orderdetails.intOrderYear = trn_ordersizeqty.intOrderYear 
					AND trn_orderdetails.intSalesOrderId = trn_ordersizeqty.intSalesOrderId
					LEFT JOIN trn_sample_size_wise_item_consumptions ON trn_ordersizeqty.intOrderNo = trn_sample_size_wise_item_consumptions.ORDER_NO 
					AND trn_ordersizeqty.intOrderYear = trn_sample_size_wise_item_consumptions.ORDER_YEAR 
					 AND trn_ordersizeqty.intSalesOrderId = trn_sample_size_wise_item_consumptions.SALES_ORDER_ID 
					AND trn_ordersizeqty.strSize = trn_sample_size_wise_item_consumptions.SIZE
					WHERE
					trn_orderdetails.intOrderNo = '$orderNo' AND
					trn_orderdetails.intOrderYear = '$orderYear' AND
					trn_orderdetails.intSalesOrderId = '$salesOrder' AND
					trn_sample_size_wise_item_consumptions.CONSUMPTION_TYPE_ID = '$type' AND
					trn_sample_size_wise_item_consumptions.ITEM_ID = '$item'
					";
		
				return $sql;
	}
	
	private function get_order_item_issued_qty_sql($orderNo,$orderYear,$item,$salesOrder){
		
		$sql = "SELECT
				IFNULL(SUM(ware_stocktransactions.dblQty)*(-1),0) as IssuedQty
				FROM `ware_stocktransactions`
				WHERE
				ware_stocktransactions.intOrderNo = '$orderNo' AND
				ware_stocktransactions.intOrderYear = '$orderYear' AND
				ware_stocktransactions.intSalesOrderId = '$salesOrder' AND
				ware_stocktransactions.intItemId = '$item' AND
				ware_stocktransactions.strType IN ('ISSUE','RETSTORES')";
				
		return $sql;
	}
	
	private function get_order_item_stock_bal_sql($location,$orderNo,$orderYear,$salesOrder,$item){
		
		//later-----------style stock + color room ink item stock +ink stock
		
		$sql = "SELECT
				IFNULL(SUM(ware_stocktransactions.dblQty),0) as stockBal
				FROM `ware_stocktransactions`
				WHERE 
				ware_stocktransactions.intLocationId = '$location' AND
				ware_stocktransactions.intOrderNo = '$orderNo' AND
				ware_stocktransactions.intOrderYear = '$orderYear' ";
		if($salesOrder != '')		
		$sql .= " AND ware_stocktransactions.intSalesOrderId = '$salesOrder'";
				
		$sql .= " AND ware_stocktransactions.intItemId = '$item' ";
				
		return $sql;
	}

	private function get_order_item_stock_bal_without_location_sql($orderNo,$orderYear,$salesOrder,$item){
		
		$sql = "SELECT
				IFNULL(SUM(ware_stocktransactions.dblQty),0) as stockBal
				FROM `ware_stocktransactions`
				WHERE 
				ware_stocktransactions.intOrderNo = '$orderNo' AND
				ware_stocktransactions.intOrderYear = '$orderYear' ";
		if($salesOrder != '')		
		$sql .= " AND ware_stocktransactions.intSalesOrderId = '$salesOrder'";
				
		$sql .= " AND ware_stocktransactions.intItemId = '$item' ";
				
		return $sql;
	}
	
	
		private function get_order_item_color_room_stock_bal_without_location_sql($orderNo,$orderYear,$salesOrder,$item){
		
		$sql = "SELECT
				IFNULL(SUM(ware_sub_stocktransactions_bulk.dblQty),0) as stockBal
				FROM `ware_sub_stocktransactions_bulk`
				WHERE 
				ware_sub_stocktransactions_bulk.intOrderNo = '$orderNo' AND
				ware_sub_stocktransactions_bulk.intOrderYear = '$orderYear' ";
		if($salesOrder != '')		
		$sql .= " AND ware_sub_stocktransactions_bulk.intSalesOrderId = '$salesOrder'";
				
		$sql .= " AND ware_sub_stocktransactions_bulk.intItemId = '$item' ";
				
		return $sql;
	}

	
	
	private function get_sizes_sql($orderNo,$orderYear,$salesOrder){
		
		$sql = "SELECT
				trn_ordersizeqty.strSize
				FROM `trn_ordersizeqty`
				WHERE
				trn_ordersizeqty.intOrderNo = $orderNo AND
				trn_ordersizeqty.intOrderYear = '$orderYear' AND
				trn_ordersizeqty.intSalesOrderId = '$salesOrder'";
				
		return $sql;
	}
	
	private function get_day_consumptions_sql($orderNo,$orderYear,$salesOrder,$item,$type,$size){
		
		$sql = "SELECT
				IFNULL(trn_sample_size_wise_item_consumptions.CONSUMPTION,0) as CONSUMPTION  
				FROM `trn_sample_size_wise_item_consumptions`
				WHERE
				trn_sample_size_wise_item_consumptions.ORDER_YEAR = '$orderYear' AND
				trn_sample_size_wise_item_consumptions.ORDER_NO = '$orderNo' AND
				trn_sample_size_wise_item_consumptions.SALES_ORDER_ID = '$salesOrder' AND
				trn_sample_size_wise_item_consumptions.ITEM_ID = '$item' AND
				trn_sample_size_wise_item_consumptions.CONSUMPTION_TYPE_ID = '$type' AND
				trn_sample_size_wise_item_consumptions.SIZE = '$size'
				";
				
		return $sql;
	}
	
	private function get_foil_special_items_sql($orderNo,$orderYear,$salesOrderNo,$mainCategory,$subCategory,$description){
	
 		$sql = "select  
				MAIN.intSampleNo, 
				MAIN.intSampleYear, 
				MAIN.intRevisionNo, 
				MAIN.strCombo, 
				MAIN.strPrintName,
				trn_sampleinfomations.strGraphicRefNo,
				mst_part.intId as partId,
				mst_part.strName as partName,
				mst_item.strName as itemName,
				mst_item.intId, 
				mst_item.intMainCategory,
				mst_item.intSubCategory,
				mst_maincategory.strName as mainCatName,
				mst_subcategory.strName as subCatName,
				mst_item.strCode , 
				mst_units.strCode as uom ,
				trn_orderdetails.intQty as orderQty,
				IFNULL(consum,0)  as consum ,
				IFNULL(trn_orderdetails.intQty*consum,0)  as required
 						
						 from (
						 /*( select 
						1 as type, 
						sc.intSampleNo, 
						sc.intSampleYear, 
						sc.intRevisionNo, 
						sc.strCombo, 
						sc.strPrintName,
						 mst_item.intId as intItem,
						 sum(round(dblColorWeight/((SELECT
						Sum(trn_sample_color_recipes.dblWeight)  
						FROM trn_sample_color_recipes
						WHERE
						trn_sample_color_recipes.intSampleNo = sc.intSampleNo AND
						trn_sample_color_recipes.intSampleYear = sc.intSampleYear AND
						trn_sample_color_recipes.intRevisionNo =  sc.intRevisionNo AND
						trn_sample_color_recipes.strCombo =  sc.strCombo AND
						trn_sample_color_recipes.strPrintName =  sc.strPrintName AND
						trn_sample_color_recipes.intTechniqueId =  sc.intTechniqueId AND
						trn_sample_color_recipes.intInkTypeId =  trn_sampleinfomations_details_technical.intInkTypeId AND
						trn_sample_color_recipes.intColorId =  trn_sampleinfomations_details_technical.intColorId 
						 ))*dblWeight /dblNoOfPcs,6))as consum 
						FROM
						trn_sample_color_recipes as sc
						Inner Join trn_sampleinfomations_details_technical ON trn_sampleinfomations_details_technical.intSampleNo = sc.intSampleNo 
						AND trn_sampleinfomations_details_technical.intSampleYear = sc.intSampleYear 
						AND trn_sampleinfomations_details_technical.intRevNo = sc.intRevisionNo 
						AND trn_sampleinfomations_details_technical.strComboName = sc.strCombo 
						AND trn_sampleinfomations_details_technical.strPrintName = sc.strPrintName 
						AND trn_sampleinfomations_details_technical.intColorId = sc.intColorId 
						AND trn_sampleinfomations_details_technical.intInkTypeId = sc.intInkTypeId
						Inner Join mst_item ON mst_item.intId = sc.intItem
						Inner Join mst_units ON mst_item.intUOM = mst_units.intId
						group by 
						sc.intSampleNo, 
						sc.intSampleYear, 
						sc.intRevisionNo, 
						sc.strCombo, 
						sc.strPrintName ,
						 mst_item.intId 
 				 ) 
UNION */
(
SELECT  
		2 as type, 
trn_sample_spitem_consumption.intSampleNo, 
trn_sample_spitem_consumption.intSampleYear, 
trn_sample_spitem_consumption.intRevisionNo, 
trn_sample_spitem_consumption.strCombo, 
trn_sample_spitem_consumption.strPrintName,
 mst_item.intId, 
       sum(trn_sample_spitem_consumption.dblQty/dblNoOfPcs) as consum 
       FROM
	trn_sample_spitem_consumption 
        Inner Join mst_item ON mst_item.intId = trn_sample_spitem_consumption.intItem
	Inner Join mst_units ON mst_units.intId = mst_item.intUOM
group by 
trn_sample_spitem_consumption.intSampleNo, 
trn_sample_spitem_consumption.intSampleYear, 
trn_sample_spitem_consumption.intRevisionNo, 
trn_sample_spitem_consumption.strCombo, 
trn_sample_spitem_consumption.strPrintName ,
 mst_item.intId 
)

		UNION 
		(
		SELECT    
				3 as type, 
		trn_sample_foil_consumption.intSampleNo, 
		trn_sample_foil_consumption.intSampleYear, 
		trn_sample_foil_consumption.intRevisionNo, 
		trn_sample_foil_consumption.strCombo, 
		trn_sample_foil_consumption.strPrintName,
		 mst_item.intId, 
				sum(trn_sample_foil_consumption.dblMeters) as consum 
			FROM 
				trn_sample_foil_consumption  
				Inner Join mst_item ON mst_item.intId = trn_sample_foil_consumption.intItem
				Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
				Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
				Inner Join mst_units ON mst_units.intId = mst_item.intUOM
		group by 
		trn_sample_foil_consumption.intSampleNo, 
		trn_sample_foil_consumption.intSampleYear, 
		trn_sample_foil_consumption.intRevisionNo, 
		trn_sample_foil_consumption.strCombo, 
		trn_sample_foil_consumption.strPrintName ,
		 mst_item.intId 
		)				 
			)	  as MAIN  
				
				
				INNER JOIN trn_sampleinfomations ON MAIN.intSampleNo = trn_sampleinfomations.intSampleNo AND MAIN.intSampleYear = trn_sampleinfomations.intSampleYear AND MAIN.intRevisionNo = trn_sampleinfomations.intRevisionNo
 				INNER JOIN trn_orderdetails ON trn_orderdetails.intSampleNo=MAIN.intSampleNo AND 
				trn_orderdetails.intSampleYear=MAIN.intSampleYear AND 
				trn_orderdetails.intRevisionNo=MAIN.intRevisionNo AND 
				trn_orderdetails.strCombo=MAIN.strCombo AND 
				trn_orderdetails.strPrintName=MAIN.strPrintName  
				INNER JOIN mst_item ON mst_item.intId=MAIN.intId 
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId  
				INNER JOIN mst_part ON mst_part.intId = trn_orderdetails.intPart

				WHERE  
				trn_orderdetails.intOrderNo='$orderNo' AND 
				trn_orderdetails.intOrderYear='$orderYear' AND 
				trn_orderdetails.intSalesOrderId='$salesOrderNo' AND 
				mst_item.intStatus='1' ";

				return $sql;
		
	}
	private function get_order_years_options_sql($location){
		$sql = "SELECT DISTINCT
 				trn_orderheader.intOrderYear
				FROM trn_orderheader 
				WHERE
				trn_orderheader.intStatus =  '1' and intLocationId='$location' order by  intOrderYear  desc";
 		
		return $sql;
	}
	
	private function get_order_nos_options_sql($location,$year){
		$sql = "SELECT DISTINCT
				trn_orderheader.intOrderNo 
				FROM trn_orderheader 
				WHERE
				trn_orderheader.intStatus =  '1' and intLocationId='$location' and trn_orderheader.intOrderYear='$year' order by  intOrderYear,intOrderNo asc";
 		
		return $sql;
	}
	private function total_ink_allocated_item_weight($orderNo,$orderYear,$salesOrder,$item){
		
		$sql	="SELECT
		(ware_sub_stocktransactions_bulk.dblQty*-1) as itemWeight
		FROM
		ware_sub_stocktransactions_bulk
		WHERE
		ware_sub_stocktransactions_bulk.intOrderNo = '$orderNo' AND
		ware_sub_stocktransactions_bulk.intOrderYear = '$orderYear' AND
 		ware_sub_stocktransactions_bulk.intItemId = '$item' AND
		ware_sub_stocktransactions_bulk.intSalesOrderId = '$salesOrder' AND
		ware_sub_stocktransactions_bulk.strType = 'INK_ALLOC'	";	
	}
	private function get_item_ink_list_sql($orderNo,$orderYear,$salesOrder,$item){
		
		$sql	= "SELECT
			concat(ware_sub_stocktransactions_bulk.intColorId,'/',
			ware_sub_stocktransactions_bulk.intTechnique,'/',
			ware_sub_stocktransactions_bulk.intInkType) as inkList
			FROM `ware_sub_stocktransactions_bulk`
			WHERE
			ware_sub_stocktransactions_bulk.intOrderNo = '$orderNo' AND
			ware_sub_stocktransactions_bulk.intOrderYear = '$orderYear' AND
			ware_sub_stocktransactions_bulk.intItemId = '$item' AND
			ware_sub_stocktransactions_bulk.intSalesOrderId = '$salesOrder' ";
			return $sql;	
	}
	private function total_ink_allocated_ink_weight($orderNo,$orderYear,$salesOrder,$ink_list){
		
		$sql	="SELECT
		(ware_sub_stocktransactions_bulk.dblQty*-1) as inkWeight
		FROM
		ware_sub_stocktransactions_bulk
		WHERE
		ware_sub_stocktransactions_bulk.intOrderNo = '$orderNo' AND
		ware_sub_stocktransactions_bulk.intOrderYear = '$orderYear' AND
  		ware_sub_stocktransactions_bulk.strType = 'INK_ALLOC' AND 
		ware_sub_stocktransactions_bulk.intSalesOrderId = '$salesOrder' AND
		concat(ware_sub_stocktransactions_bulk.intColorId,'/',ware_sub_stocktransactions_bulk.intTechnique,'/',ware_sub_stocktransactions_bulk.intInkType) IN ($ink_list)
	";	
	return $sql;
	}
	private function total_first_day_production_allocated_ink_weight($orderNo,$orderYear,$salesOrder,$ink_list){
		
		$sql	="SELECT
				trn_inktype_production_allocation.dblOrderQty,
				trn_inktype_production_allocation.dblIssueQty,
				trn_inktype_production_allocation.dblReturnedQty,
				trn_inktype_production_allocation.dblWastageQty,
				((trn_inktype_production_allocation.dblIssueQty-trn_inktype_production_allocation.dblReturnedQty)/trn_inktype_production_allocation.dblOrderQty) as producWeight 
				FROM `trn_inktype_production_allocation`
				WHERE
				trn_inktype_production_allocation.intOrderYear = '$orderYear' AND
				trn_inktype_production_allocation.intOrderNo = '$orderNo'  AND 
				trn_inktype_production_allocation.intSalesOrderId = '$salesOrder' AND
		concat(ware_sub_stocktransactions_bulk.intColorId,'/',ware_sub_stocktransactions_bulk.intTechnique,'/',ware_sub_stocktransactions_bulk.intInkType) IN ($ink_list)
	 limit 1";
	 
	 return $sql;	
	}
	
	
	private function get_item_name_sql($item){
		$sql = "SELECT *
				FROM `mst_item`
				WHERE
				mst_item.intId = '$item'
				";
		return $sql;
		
	}
	
	private function get_sales_oreder_no_wises_sql($orderNo,$orderYear,$item,$toSaveQty){
		
		$sql = "SELECT
				OD.intOrderNo,
				OD.intOrderYear,
				OD.intSalesOrderId,
				OD.intQty as salesOrderQty,
				(select sum(OD1.intQty+OD1.intQty*OD1.dblDamagePercentage/100) from trn_orderdetails as OD1 where OD1.intOrderNo=OD.intOrderNo AND OD1.intOrderYear=OD.intOrderYear ) AS orderQty,
				(((OD.intQty+OD.intQty*OD.dblDamagePercentage/100)/(select sum(OD2.intQty+OD2.intQty*OD2.dblDamagePercentage/100) from trn_orderdetails as OD2 where OD2.intOrderNo=OD.intOrderNo AND OD2.intOrderYear=OD.intOrderYear))*$toSaveQty) as qty
				FROM
	
 			 (( select 
						1 as type, 
						sc.intSampleNo, 
						sc.intSampleYear, 
						sc.intRevisionNo, 
						sc.strCombo, 
						sc.strPrintName,
						 mst_item.intId as intItem,
						 sum(round(dblColorWeight/((SELECT
						Sum(trn_sample_color_recipes.dblWeight)  
						FROM trn_sample_color_recipes
						WHERE
						trn_sample_color_recipes.intSampleNo = sc.intSampleNo AND
						trn_sample_color_recipes.intSampleYear = sc.intSampleYear AND
						trn_sample_color_recipes.intRevisionNo =  sc.intRevisionNo AND
						trn_sample_color_recipes.strCombo =  sc.strCombo AND
						trn_sample_color_recipes.strPrintName =  sc.strPrintName AND
						trn_sample_color_recipes.intTechniqueId =  sc.intTechniqueId AND
						trn_sample_color_recipes.intInkTypeId =  trn_sampleinfomations_details_technical.intInkTypeId AND
						trn_sample_color_recipes.intColorId =  trn_sampleinfomations_details_technical.intColorId 
						 ))*dblWeight /dblNoOfPcs,6))as consum 
						FROM
						trn_sample_color_recipes as sc
						Inner Join trn_sampleinfomations_details_technical ON trn_sampleinfomations_details_technical.intSampleNo = sc.intSampleNo 
						AND trn_sampleinfomations_details_technical.intSampleYear = sc.intSampleYear 
						AND trn_sampleinfomations_details_technical.intRevNo = sc.intRevisionNo 
						AND trn_sampleinfomations_details_technical.strComboName = sc.strCombo 
						AND trn_sampleinfomations_details_technical.strPrintName = sc.strPrintName 
						AND trn_sampleinfomations_details_technical.intColorId = sc.intColorId 
						AND trn_sampleinfomations_details_technical.intInkTypeId = sc.intInkTypeId
						Inner Join mst_item ON mst_item.intId = sc.intItem
						Inner Join mst_units ON mst_item.intUOM = mst_units.intId
						group by 
						sc.intSampleNo, 
						sc.intSampleYear, 
						sc.intRevisionNo, 
						sc.strCombo, 
						sc.strPrintName ,
						 mst_item.intId 
 				 )
UNION
(
SELECT  
		2 as type, 
trn_sample_spitem_consumption.intSampleNo, 
trn_sample_spitem_consumption.intSampleYear, 
trn_sample_spitem_consumption.intRevisionNo, 
trn_sample_spitem_consumption.strCombo, 
trn_sample_spitem_consumption.strPrintName,
 mst_item.intId, 
       sum(trn_sample_spitem_consumption.dblQty/dblNoOfPcs) as consum 
       FROM
	trn_sample_spitem_consumption 
        Inner Join mst_item ON mst_item.intId = trn_sample_spitem_consumption.intItem
	Inner Join mst_units ON mst_units.intId = mst_item.intUOM
group by 
trn_sample_spitem_consumption.intSampleNo, 
trn_sample_spitem_consumption.intSampleYear, 
trn_sample_spitem_consumption.intRevisionNo, 
trn_sample_spitem_consumption.strCombo, 
trn_sample_spitem_consumption.strPrintName ,
 mst_item.intId 
)

		UNION 
		(
		SELECT    
				3 as type, 
		trn_sample_foil_consumption.intSampleNo, 
		trn_sample_foil_consumption.intSampleYear, 
		trn_sample_foil_consumption.intRevisionNo, 
		trn_sample_foil_consumption.strCombo, 
		trn_sample_foil_consumption.strPrintName,
		 mst_item.intId, 
				sum(trn_sample_foil_consumption.dblMeters) as consum 
			FROM 
				trn_sample_foil_consumption  
				Inner Join mst_item ON mst_item.intId = trn_sample_foil_consumption.intItem
				Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
				Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
				Inner Join mst_units ON mst_units.intId = mst_item.intUOM
		group by 
		trn_sample_foil_consumption.intSampleNo, 

		trn_sample_foil_consumption.intSampleYear, 
		trn_sample_foil_consumption.intRevisionNo, 
		trn_sample_foil_consumption.strCombo, 
		trn_sample_foil_consumption.strPrintName ,
		 mst_item.intId 
		)				 
			)	  as MAIN  
				
				
				INNER JOIN trn_orderdetails AS OD ON 
				OD.intSampleNo=MAIN.intSampleNo AND 
				OD.intSampleYear=MAIN.intSampleYear AND 
				OD.intRevisionNo=MAIN.intRevisionNo AND 
				OD.strCombo=MAIN.strCombo AND 
				OD.strPrintName=MAIN.strPrintName  
				INNER JOIN mst_item ON mst_item.intId=MAIN.intItem 
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
				WHERE  
				OD.intOrderNo='$orderNo' AND 
				OD.intOrderYear='$orderYear' ";
				
				if($item!=''){
					$sql .=" AND 
					mst_item.intId='$item'  ";
				}				
		
				/*trn_orderheader as OH 
				INNER JOIN trn_orderdetails as OD ON OH.intOrderNo = OD.intOrderNo AND OH.intOrderYear = OD.intOrderYear
				WHERE
				OD.intOrderNo = '$orderNo' AND
				OD.intOrderYear = '$orderYear'
				GROUP BY
				OD.intOrderNo,
				OD.intOrderYear,
				OD.intSalesOrderId ";*/
				
		return $sql;
	}
	
	private function get_cumulateve_special_foil_consumption_sql($orderNo,$orderYear,$salesOrder,$item){
		
		$sql	="SELECT
					trn_sample_size_wise_item_consumptions.ORDER_NO,
					trn_sample_size_wise_item_consumptions.ORDER_YEAR,
					trn_sample_size_wise_item_consumptions.SALES_ORDER_ID,
					trn_sample_size_wise_item_consumptions.ITEM_ID,
					trn_sample_size_wise_item_consumptions.SIZE,
					(trn_sample_size_wise_item_consumptions.CONSUMPTION) AS cumu_cons,
					trn_actual_production.QTY,
					DATE(trn_sample_size_wise_item_consumptions_header.SAVED_DATE) as date,
					(trn_sample_size_wise_item_consumptions.CONSUMPTION*trn_actual_production.QTY) as CONS
					FROM
					trn_sample_size_wise_item_consumptions_header
					INNER JOIN trn_sample_size_wise_item_consumptions_approve_by ON trn_sample_size_wise_item_consumptions_header.ORDER_NO = trn_sample_size_wise_item_consumptions_approve_by.ORDER_NO AND trn_sample_size_wise_item_consumptions_header.ORDER_YEAR = trn_sample_size_wise_item_consumptions_approve_by.ORDER_YEAR AND trn_sample_size_wise_item_consumptions_header.SALES_ORDER_ID = trn_sample_size_wise_item_consumptions_approve_by.SALES_ORDER_ID AND trn_sample_size_wise_item_consumptions_header.CONSUMPTION_TYPE_ID = trn_sample_size_wise_item_consumptions_approve_by.CONSUMPTION_TYPE_ID
					INNER JOIN trn_sample_size_wise_item_consumptions ON trn_sample_size_wise_item_consumptions_approve_by.ORDER_NO = trn_sample_size_wise_item_consumptions.ORDER_NO AND trn_sample_size_wise_item_consumptions_approve_by.ORDER_YEAR = trn_sample_size_wise_item_consumptions.ORDER_YEAR AND trn_sample_size_wise_item_consumptions_approve_by.SALES_ORDER_ID = trn_sample_size_wise_item_consumptions.SALES_ORDER_ID AND trn_sample_size_wise_item_consumptions_approve_by.CONSUMPTION_TYPE_ID = trn_sample_size_wise_item_consumptions.CONSUMPTION_TYPE_ID
					LEFT JOIN trn_actual_production ON trn_sample_size_wise_item_consumptions.ORDER_NO = trn_actual_production.ORDER_NO AND trn_sample_size_wise_item_consumptions.ORDER_YEAR = trn_actual_production.`YEAR` AND trn_sample_size_wise_item_consumptions.SALES_ORDER_ID = trn_actual_production.SALES_ORDER_ID AND trn_sample_size_wise_item_consumptions.SIZE = trn_actual_production.SIZE AND DATE(trn_sample_size_wise_item_consumptions_header.SAVED_DATE) = trn_actual_production.DATE
					WHERE
					trn_sample_size_wise_item_consumptions.ORDER_NO = '$orderNo' AND
					trn_sample_size_wise_item_consumptions.ORDER_YEAR = '$orderYear' AND
					trn_sample_size_wise_item_consumptions.SALES_ORDER_ID = '$salesOrder' AND
					trn_sample_size_wise_item_consumptions.ITEM_ID = '$item'
					GROUP BY 
					trn_sample_size_wise_item_consumptions_header.SAVED_DATE,
					trn_sample_size_wise_item_consumptions.ORDER_NO,
					trn_sample_size_wise_item_consumptions.ORDER_YEAR,
					trn_sample_size_wise_item_consumptions.SALES_ORDER_ID,
					trn_sample_size_wise_item_consumptions.ITEM_ID,
					trn_sample_size_wise_item_consumptions.SIZE
					";
		
		return $sql;
		
	}
	
	
	private function get_daily_ink_item_used_sql($orderNo,$orderYear,$salesOrder,$item){
		
		$sql	=	"SELECT 
					trn_ink_item_allocated_and_wastage_actuals.DATE_PRODUCTION, 
					Sum(trn_ink_item_allocated_and_wastage_actuals.WEIGHT) AS WEIGHT
					FROM
					trn_ink_item_allocated_and_wastage_actuals
					WHERE
					trn_ink_item_allocated_and_wastage_actuals.TYPE = 'ALLOCATED' AND
					trn_ink_item_allocated_and_wastage_actuals.ORDER_NO = '$orderNo' AND
					trn_ink_item_allocated_and_wastage_actuals.ORDER_YEAR = '$orderYear' AND
					trn_ink_item_allocated_and_wastage_actuals.SALES_ORDER = '$salesOrder' AND
					trn_ink_item_allocated_and_wastage_actuals.ITEM = '$item'
					GROUP BY
					trn_ink_item_allocated_and_wastage_actuals.DATE_PRODUCTION";
		
		return $sql;
		
	}
	
	
	private function get_day_actual_production_qty_sql($orderNo,$orderYear,$salesOrder,$date_production){
		
		$sql	=" SELECT
					Sum(trn_actual_production.QTY) as QTY 
					FROM `trn_actual_production`
					WHERE
					trn_actual_production.ORDER_NO = '$orderNo' AND
					trn_actual_production.`YEAR` = '$orderYear' AND
					trn_actual_production.SALES_ORDER_ID = '$salesOrder' AND 
					trn_actual_production.DATE = '$date_production' ";
		return $sql;
		
	}

	private function get_already_allocated_sql($orderYear,$orderNo,$salesOrderId,$itemId)
	{
		$sql = "SELECT sum(dblQty) as qty FROM 
				ware_stocktransactions
				WHERE /*intLocationId = '$location' AND*/
				intItemId = '$itemId' AND
				intOrderNo = '$orderNo' AND
				intOrderYear = '$orderYear' ";
		if($salesOrderId != ''){		
		$sql .= " AND
				intSalesOrderId = '$salesOrderId'  ";
		}
				
		$sql .= "AND
				strType IN ('25_ITEM_ALLOC','100_ITEM_ALLOC','ALLOCATE','UNALLOCATE')
				";
		
 		return $sql;	
	}
	
	
	private function get_sales_orders_for_graphic_sql($orderYear,$graphic,$item){
		
		$sql = "SELECT
				OD.intOrderNo,
				OD.intOrderYear,
				OD.intSalesOrderId,
				OD.intQty as salesOrderQty,
				OD.intQty as dblDamagePercentage,
				(select sum(OD1.intQty+OD1.intQty*OD1.dblDamagePercentage/100) from trn_orderdetails as OD1 where OD1.intOrderNo=OD.intOrderNo AND OD1.intOrderYear=OD.intOrderYear ) AS orderQty 
  			 FROM (( select 
						1 as type, 
						sc.intSampleNo, 
						sc.intSampleYear, 
						sc.intRevisionNo, 
						sc.strCombo, 
						sc.strPrintName,
						 mst_item.intId as intItem,
						 sum(round(dblColorWeight/((SELECT
						Sum(trn_sample_color_recipes.dblWeight)  
						FROM trn_sample_color_recipes
						WHERE
						trn_sample_color_recipes.intSampleNo = sc.intSampleNo AND
						trn_sample_color_recipes.intSampleYear = sc.intSampleYear AND
						trn_sample_color_recipes.intRevisionNo =  sc.intRevisionNo AND
						trn_sample_color_recipes.strCombo =  sc.strCombo AND
						trn_sample_color_recipes.strPrintName =  sc.strPrintName AND
						trn_sample_color_recipes.intTechniqueId =  sc.intTechniqueId AND
						trn_sample_color_recipes.intInkTypeId =  trn_sampleinfomations_details_technical.intInkTypeId AND
						trn_sample_color_recipes.intColorId =  trn_sampleinfomations_details_technical.intColorId 
						 ))*dblWeight /dblNoOfPcs,6))as consum 
						FROM
						trn_sample_color_recipes as sc
						Inner Join trn_sampleinfomations_details_technical ON trn_sampleinfomations_details_technical.intSampleNo = sc.intSampleNo 
						AND trn_sampleinfomations_details_technical.intSampleYear = sc.intSampleYear 
						AND trn_sampleinfomations_details_technical.intRevNo = sc.intRevisionNo 
						AND trn_sampleinfomations_details_technical.strComboName = sc.strCombo 
						AND trn_sampleinfomations_details_technical.strPrintName = sc.strPrintName 
						AND trn_sampleinfomations_details_technical.intColorId = sc.intColorId 
						AND trn_sampleinfomations_details_technical.intInkTypeId = sc.intInkTypeId
						Inner Join mst_item ON mst_item.intId = sc.intItem
						Inner Join mst_units ON mst_item.intUOM = mst_units.intId
						group by 
						sc.intSampleNo, 
						sc.intSampleYear, 
						sc.intRevisionNo, 
						sc.strCombo, 
						sc.strPrintName ,
						 mst_item.intId 
 				 )
UNION
(
SELECT  
		2 as type, 
trn_sample_spitem_consumption.intSampleNo, 
trn_sample_spitem_consumption.intSampleYear, 
trn_sample_spitem_consumption.intRevisionNo, 
trn_sample_spitem_consumption.strCombo, 
trn_sample_spitem_consumption.strPrintName,
 mst_item.intId, 
       sum(trn_sample_spitem_consumption.dblQty/dblNoOfPcs) as consum 
       FROM
	trn_sample_spitem_consumption 
        Inner Join mst_item ON mst_item.intId = trn_sample_spitem_consumption.intItem
	Inner Join mst_units ON mst_units.intId = mst_item.intUOM
group by 
trn_sample_spitem_consumption.intSampleNo, 
trn_sample_spitem_consumption.intSampleYear, 
trn_sample_spitem_consumption.intRevisionNo, 
trn_sample_spitem_consumption.strCombo, 
trn_sample_spitem_consumption.strPrintName ,
 mst_item.intId 
)

		UNION 
		(
		SELECT    
				3 as type, 
		trn_sample_foil_consumption.intSampleNo, 
		trn_sample_foil_consumption.intSampleYear, 
		trn_sample_foil_consumption.intRevisionNo, 
		trn_sample_foil_consumption.strCombo, 
		trn_sample_foil_consumption.strPrintName,
		 mst_item.intId, 
				sum(trn_sample_foil_consumption.dblMeters) as consum 
			FROM 
				trn_sample_foil_consumption  
				Inner Join mst_item ON mst_item.intId = trn_sample_foil_consumption.intItem
				Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
				Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
				Inner Join mst_units ON mst_units.intId = mst_item.intUOM
		group by 
		trn_sample_foil_consumption.intSampleNo, 

		trn_sample_foil_consumption.intSampleYear, 
		trn_sample_foil_consumption.intRevisionNo, 
		trn_sample_foil_consumption.strCombo, 
		trn_sample_foil_consumption.strPrintName ,
		 mst_item.intId 
		)				 
			)	  as MAIN  
				
				
				INNER JOIN trn_orderdetails AS OD ON 
				OD.intSampleNo=MAIN.intSampleNo AND 
				OD.intSampleYear=MAIN.intSampleYear AND 
				OD.intRevisionNo=MAIN.intRevisionNo AND 
				OD.strCombo=MAIN.strCombo AND 
				OD.strPrintName=MAIN.strPrintName  
				INNER JOIN mst_item ON mst_item.intId=MAIN.intItem 
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
				WHERE  
				OD.intOrderYear='$orderYear' ";
				
				if($graphic!=''){
					$sql .=" AND 
					OD.strGraphicNo='$graphic'  ";
				}
				if($item!=''){
					$sql .=" AND 
					mst_item.intId='$item'  ";
				}
			$sql .=" GROUP BY 
				OD.intOrderNo,
				OD.intOrderYear,
				OD.intSalesOrderId ";
					
		
 				
		return $sql;
 		
 	}
	
	
	private function get_already_STN_sql($orderYear,$graphicNo,$itemId){
		
		$sql	="SELECT
				IFNULL(sum(ware_storestransferdetails.dblQty),0) AS transfered_normal_qty,
				IFNULL(sum(ware_storestransferdetails.dblExcessQty),0) AS transfered_excess_qty
				FROM
				ware_storestransferheader
				INNER JOIN ware_storestransferdetails ON ware_storestransferheader.intTransfNo = ware_storestransferdetails.intTransfNo AND ware_storestransferheader.intTransfYear = ware_storestransferdetails.intTransfYear
				INNER JOIN ware_storesrequesitionheader ON ware_storestransferheader.intRequisitionNo = ware_storesrequesitionheader.intRequisitionNo AND ware_storestransferheader.intRequisitionYear = ware_storesrequesitionheader.intRequisitionYear
				WHERE
				ware_storesrequesitionheader.intOrderYear = '$orderYear' AND
				ware_storesrequesitionheader.strGraphicNo = '$graphicNo' AND
				ware_storestransferdetails.intItemId = '$itemId' AND
				ware_storestransferheader.intStatus = 1";
		return $sql;
		
	}
	
	private function get_already_STN_Returned_sql($orderYear,$graphicNo,$itemId){
		
		$sql	="SELECT
				IFNULL(Sum(ware_storestransfer_return_details.dblQty),0) AS returned_normal_qty,
				IFNULL(Sum(ware_storestransfer_return_details.dblExcess),0) AS returned_excess_qty
				FROM
				ware_storestransfer_return_details
				INNER JOIN ware_storestransfer_return_header ON ware_storestransfer_return_details.intReturnNo = ware_storestransfer_return_header.intReturnNo AND ware_storestransfer_return_details.intReturnYear = ware_storestransfer_return_header.intReturnYear
				INNER JOIN ware_storestransferheader ON ware_storestransfer_return_header.intTransfNo = ware_storestransferheader.intTransfNo AND ware_storestransfer_return_header.intTransfYear = ware_storestransferheader.intTransfYear
				INNER JOIN ware_storesrequesitionheader ON ware_storestransferheader.intRequisitionNo = ware_storesrequesitionheader.intRequisitionNo AND ware_storestransferheader.intRequisitionYear = ware_storesrequesitionheader.intRequisitionYear
				WHERE 
				ware_storesrequesitionheader.intOrderYear = '$orderYear' AND
				ware_storesrequesitionheader.strGraphicNo = '$graphicNo' AND
				ware_storestransfer_return_details.intItemId = '$itemId' AND
				ware_storestransfer_return_header.intStatus = 1";
		return $sql;
		
	}
	
	private function get_processed_ink_item_cumulative_conpc_sql($orderNo,$orderYear,$salesOrder,$item){
	
		$sql	= "SELECT
					IFNULL(Sum(ware_processed_actual_ink_used_details.INK_ITEM_WEIGHT),0) AS USED_ITEM_WEIGHT,
					IFNULL(Sum(ware_processed_actual_ink_used_details.ACTUAL_PCS),0) AS ACTUAL_PCS,
					IFNULL(((Sum(ware_processed_actual_ink_used_details.INK_ITEM_WEIGHT))/(Sum(ware_processed_actual_ink_used_details.ACTUAL_PCS))),0) AS CUMULATIVE_CONPC
					FROM `ware_processed_actual_ink_used_details`
					WHERE
					ware_processed_actual_ink_used_details.ORDER_NO = '$orderNo' AND
					ware_processed_actual_ink_used_details.ORDER_YEAR = '$orderYear' AND
					ware_processed_actual_ink_used_details.SALES_ORDER_ID = '$salesOrder' AND
					ware_processed_actual_ink_used_details.ITEM_ID = '$item' AND
					ware_processed_actual_ink_used_details.`STATUS` = 1
					GROUP BY
					ware_processed_actual_ink_used_details.ORDER_NO,
					ware_processed_actual_ink_used_details.ORDER_YEAR,
					ware_processed_actual_ink_used_details.SALES_ORDER_ID,
					ware_processed_actual_ink_used_details.ITEM_ID";
	
		return $sql;
	}
	
}
 ?>

<?php
class Cls_order_auto_allocation_set
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	public function saveBulkTransaction($company,$location,$docNo,$docYear,$grnNo,$grnYear,$grnDate,$currency,$grnRate,$itemId,$saveQty,$type,$orderNo,$orderYear,$salesOrderId,$userId)
	{
		$sql = "INSERT INTO ware_stocktransactions_bulk 
				(
				intCompanyId, 
				intLocationId,  
				intDocumentNo, 
				intDocumntYear, 
				intGRNNo, 
				intGRNYear, 
				dtGRNDate, 
				intCurrencyId, 
				dblGRNRate, 
				intItemId, 
				dblQty, 
				strType, 
				intOrderNo, 
				intOrderYear, 
				intSalesOrderId, 
				intUser, 
				dtDate
				)
				VALUES
				(
				'$company', 
				'$location', 
				'$docNo', 
				'$docYear', 
				'$grnNo', 
				'$grnYear', 
				'$grnDate', 
				'$currency', 
				'$grnRate', 
				'$itemId', 
				'$saveQty', 
				'$type', 
				'$orderNo', 
				'$orderYear', 
				'$salesOrderId', 
				'$userId', 
				NOW()
				)";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
	public function saveStyleTransaction($company,$location,$docNo,$docYear,$grnNo,$grnYear,$grnDate,$currency,$grnRate,$itemId,$saveQty,$type,$orderNo,$orderYear,$salesOrderId,$userId)
	{
		$sql = "INSERT INTO ware_stocktransactions 
				( 
				intCompanyId, 
				intLocationId, 
				intDocumentNo, 
				intDocumntYear, 
				intGRNNo, 
				intGRNYear, 
				dtGRNDate, 
				intCurrencyId, 
				dblGRNRate, 
				intOrderNo, 
				intOrderYear, 
				intSalesOrderId, 
				intItemId, 
				dblQty, 
				strType, 
				intUser, 
				dtDate
				)
				VALUES
				(
				'$company', 
				'$location', 
				'$docNo', 
				'$docYear', 
				'$grnNo', 
				'$grnYear', 
				'$grnDate', 
				'$currency', 
				'$grnRate', 
				'$orderNo', 
				'$orderYear', 
				'$salesOrderId', 
				'$itemId', 
				'$saveQty', 
				'$type', 
				'$userId', 
				NOW()
				)";
		
		$result = $this->db->RunQuery2($sql);
		if(!$result)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= $this->db->errormsg;
			$data['error_sql']		= $sql;
		}
		return $data;
	}
 }
?>
<?php

$session_userId		= $_SESSION["userId"];
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];

class Cls_order_auto_allocation_get
{
	private $db;
 	
	function __construct($db)
	{
		$this->db = $db;
 	}
	public function getOrderYearCombo($exectionType,$year)
	{
		
		$sql		= $this->loadOrderYear_sql();
		$html	 	= $this->getOrderYear_combo($sql,$exectionType,$year);
		return $html;
	}
	public function loadAllComboes($slected,$orderYear,$orderNo)
	{
		$sql = $this->loadAllComboes_sql($slected,$orderYear,$orderNo);
		$html = $this->loadAllComboes_combo($sql);
		return $html;
	}
	public function getOrderQty($orderNo,$orderYear,$salesOrder)
	{
		return $this->getOrderQty_sql($orderNo,$orderYear,$salesOrder);
	}
	public function getOrderQty_with_dammage($orderNo,$orderYear,$salesOrder)
	{
		return $this->getOrderQty_with_dammage_sql($orderNo,$orderYear,$salesOrder);
	}
	
	public function getAllocatedQty($orderNo,$orderYear,$salesOrder,$itemId,$executionType)
	{
		$sql 	= $this->getAllocatedQty_sql($orderNo,$orderYear,$salesOrder,$itemId);
		$result = $this->db->$executionType($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row['allocatedQty'];
	}
	public function getAllocatedQty_auto($orderNo,$orderYear,$itemId,$executionType)
	{
		$sql 	= $this->getAllocatedQty_auto_sql($orderNo,$orderYear,$itemId);
		$result = $this->db->$executionType($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row['allocatedQty'];
	}
	public function getStyleAllocateQty($item,$orderNo,$orderYear,$salesOrder,$executionType)
	{
		$sql 	= $this->getStyleAllocateQty_sql($item,$orderNo,$orderYear,$salesOrder);
		$result = $this->db->$executionType($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row['allocStyleQty'];
	}
	public function getBulkStockBalance($item,$executionType)
	{
		return $this->getBulkStockBalance_sql($item,$executionType);
	}
	public function getColorRoomStockBalance($item,$orderNo,$orderYear,$salesOrder,$executionType)
	{
		return $this->getColorRoomStockBalance_sql($item,$orderNo,$orderYear,$salesOrder,$executionType);
	}
	public function getColorRoomStockBalance_auto($item,$orderNo,$orderYear,$executionType)
	{
		return $this->getColorRoomStockBalance_auto_sql($item,$orderNo,$orderYear,$executionType);
	}
	public function getIssuedQty($item,$orderNo,$orderYear,$salesOrder,$executionType)
	{
		return $this->getIssuedQty_sql($item,$orderNo,$orderYear,$salesOrder,$executionType);
	}
	public function getIssuedQty_auto($item,$orderNo,$orderYear,$executionType)
	{
		return $this->getIssuedQty_auto_sql($item,$orderNo,$orderYear,$executionType);
	}
	public function getReturnedQty($item,$orderNo,$orderYear,$salesOrder,$executionType)
	{
		return $this->getReturnedQty_sql($item,$orderNo,$orderYear,$salesOrder,$executionType);
	}
	public function getReturnedQty_auto($item,$orderNo,$orderYear,$executionType)
	{
		return $this->getReturnedQty_auto_sql($item,$orderNo,$orderYear,$executionType);
	}
	public function getAllocationPercentages($company,$executionType)
	{
		return $this->getAllocationPercentages_sql($company,$executionType);
	}
	public function getGrnWiseStockBalance_bulk($location,$item)
	{
		return $this->getGrnWiseStockBalance_bulk_sql($location,$item);
	}
	public function loadCurrency($grnNo,$grnYear,$item)
	{
		return $this->loadCurrency_sql($grnNo,$grnYear,$item);
	}
	public function getAllocatedData($orderYear,$orderNo,$salesOrderId,$itemId,$location,$executionType)
	{
		return $this->getAllocatedData_sql($orderYear,$orderNo,$salesOrderId,$itemId,$location,$executionType);
	}
	public function get_order_wise_stock_balance_result($location,$itemId,$executionType)
	{
		$sql = $this->get_order_wise_stock_balance_sql($location,$itemId);
		$result = $this->db->$executionType($sql);
		return $result;
	}
	
	
	private function loadOrderYear_sql()
	{
		global $session_locationId;
			
		$sql = "SELECT DISTINCT
				trn_orderheader.intOrderYear
				FROM trn_orderheader
				WHERE
				trn_orderheader.intStatus = 1 /*AND
				intLocationId = '$session_locationId'*/
				ORDER BY
				trn_orderheader.intOrderYear DESC";
				
		return $sql;
	}
	private function getOrderYear_combo($sql,$exectionType,$year)
	{
		$result = $this->db->$exectionType($sql);
		$string ="<option value=\"\"></option>";
		while($row = mysqli_fetch_array($result))
		{
			if($row['intOrderYear']==$year)
				$string.="<option value=\"".$row['intOrderYear']."\" selected=\"selected\">".$row['intOrderYear']."</option>";
			else
				$string.="<option value=\"".$row['intOrderYear']."\">".$row['intOrderYear']."</option>";
		}
		return $string;
	}
	private function loadAllComboes_sql($slected,$orderYear,$orderNo)
	{
		global $session_locationId;
		
		$sql = "SELECT DISTINCT ";
		if($slected=='orderYear')
		{
			$sql .= "OD.intOrderNo as value,
					 OD.intOrderNo as id ";
		}
		if($slected=='orderNo')
		{
			$sql .= "OD.strSalesOrderNo as value, 
					 OD.intSalesOrderId as id ";
		}
		$sql .= "FROM trn_orderdetails OD 
				 INNER JOIN trn_orderheader OH ON OH.intOrderNo=OD.intOrderNo AND OH.intOrderYear=OD.intOrderYear
				 WHERE OH.intStatus = 1 /*AND
				 OH.intLocationId = '$session_locationId'*/ ";
		
		if($orderYear!='')
			$sql .= "AND OD.intOrderYear = '$orderYear' ";
		if($orderNo!='')
			$sql .= "AND OD.intOrderNo = '$orderNo' ";
		
		if($slected=='orderYear')
			$sql .= "ORDER BY OD.intOrderNo DESC";	
		if($slected=='orderNo')
			$sql .= "ORDER BY OD.strSalesOrderNo DESC";
		//echo $sql;
		return $sql ;
		
	}
	private function loadAllComboes_combo($sql)
	{
		$result = $this->db->RunQuery($sql);
		$string ="<option value=\"\"></option>";
		while($row = mysqli_fetch_array($result))
		{
			$string.="<option value=\"".$row['id']."\">".$row['value']."</option>";
		}
		return $string;
	}
	private function getOrderQty_sql($orderNo,$orderYear,$salesOrder)
	{
		$sql = "SELECT SUM(intQty) AS orderQty
				FROM trn_orderdetails
				WHERE intOrderNo = '$orderNo' AND
				intOrderYear = '$orderYear' ";
		if($salesOrder != ''){		
		$sql .= " AND
				intSalesOrderId = '$salesOrder' ";
		}
		
		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row['orderQty'];
	}
	private function getOrderQty_with_dammage_sql($orderNo,$orderYear,$salesOrder)
	{
		$sql = "SELECT SUM(trn_orderdetails.intQty+trn_orderdetails.intQty*trn_orderdetails.dblDamagePercentage/100) AS orderQty
				FROM trn_orderdetails
				WHERE intOrderNo = '$orderNo' AND
				intOrderYear = '$orderYear' ";
		if($salesOrder != ''){		
		$sql .= " AND
				intSalesOrderId = '$salesOrder' ";
		}
		
		$result = $this->db->RunQuery($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row['orderQty'];
	}
	
	
	private function getAllocatedQty_sql($orderNo,$orderYear,$salesOrder,$itemId)
	{
		global $session_locationId;
		
		$sql = "SELECT ABS(SUM(dblQty)) as allocatedQty
				FROM ware_stocktransactions_bulk
				WHERE strType IN ('25_ITEM_ALLOC','100_ITEM_ALLOC','UNALLOCATE','ALLOCATE') AND
				/*intLocationId = '$session_locationId' AND*/
				intOrderNo = '$orderNo' AND 
				intOrderYear = '$orderYear' AND 
				intSalesOrderId = '$salesOrder' AND 
				intItemId = '$itemId' ";
		
		return $sql;
	}
	private function getAllocatedQty_auto_sql($orderNo,$orderYear,$itemId)
	{
		global $session_locationId;
		
		$sql = "SELECT ABS(SUM(dblQty)) as allocatedQty
				FROM ware_stocktransactions_bulk
				WHERE strType IN ('25_ITEM_ALLOC','100_ITEM_ALLOC','UNALLOCATE','ALLOCATE') AND
				/*intLocationId = '$session_locationId' AND */
				intOrderNo = '$orderNo' AND 
				intOrderYear = '$orderYear' AND 
 				intItemId = '$itemId' ";
		
		return $sql;
	}
	
	
	private function getStyleAllocateQty_sql($item,$orderNo,$orderYear,$salesOrder)
	{
		global $session_locationId;
		
		$sql = "SELECT SUM(dblQty) AS allocStyleQty
				FROM ware_stocktransactions
				WHERE strType = '25_ITEM_ALLOC' AND
				intLocationId = '$session_locationId' AND
				intItemId = '$item' AND
				intOrderNo = '$orderNo' AND
				intOrderYear = '$orderYear' AND
				intSalesOrderId = '$salesOrder' ";
				
		return $sql;
	}
	private function getBulkStockBalance_sql($item,$executionType)
	{
		global $session_locationId;
		
		$sql = "SELECT IFNULL(SUM(dblQty),0) bulkStockBal
				FROM ware_stocktransactions_bulk
				WHERE intItemId = '$item' AND
				intLocationId = '$session_locationId' ";
		
		$result = $this->db->$executionType($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row['bulkStockBal'];
	}
	private function getColorRoomStockBalance_sql($item,$orderNo,$orderYear,$salesOrder,$executionType)
	{
		global $session_locationId;
		
		$sql = "SELECT SUM(dblQty) colorRoomStock
				FROM ware_stocktransactions
				WHERE intItemId = '$item' AND
				intLocationId = '$session_locationId' AND
				intOrderNo = '$orderNo' AND
				intOrderYear = '$orderYear'";
		if($salesOrder !=''){
		$sql .=" AND
				intSalesOrderId = '$salesOrder' ";
		}
				
				
		
		$result = $this->db->$executionType($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row['colorRoomStock'];
	}
	private function getColorRoomStockBalance_auto_sql($item,$orderNo,$orderYear,$executionType)
	{
		global $session_locationId;
		
		$sql = "SELECT SUM(dblQty) colorRoomStock
				FROM ware_stocktransactions
				WHERE intItemId = '$item' AND
				intLocationId = '$session_locationId' AND
				intOrderNo = '$orderNo' AND
				intOrderYear = '$orderYear' ";
		
		$result = $this->db->$executionType($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row['colorRoomStock'];
	}
	private function getIssuedQty_sql($item,$orderNo,$orderYear,$salesOrder,$executionType)
	{
		global $session_locationId;
		
		$sql = "SELECT SUM(dblQty) issuedQty
				FROM ware_stocktransactions
				WHERE intItemId = '$item' AND
				intLocationId = '$session_locationId' AND
				intOrderNo = '$orderNo' AND
				intOrderYear = '$orderYear' AND
				strType IN ('ISSUE','STORESTRANSFER') ";
		if($salesOrder !=''){
		$sql .="  AND
				intSalesOrderId = '$salesOrder' ";
		}
		
		$result = $this->db->$executionType($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row['issuedQty'];
	}
	private function getReturnedQty_sql($item,$orderNo,$orderYear,$salesOrder,$executionType)
	{
		global $session_locationId;
		
		$sql = "SELECT SUM(dblQty) returnedQty
				FROM ware_stocktransactions
				WHERE intItemId = '$item' AND
				intLocationId = '$session_locationId' AND
				intOrderNo = '$orderNo' AND
				intOrderYear = '$orderYear' AND
				strType = 'RETSTORES' ";
		if($salesOrder !=''){
		$sql .="  AND
				intSalesOrderId = '$salesOrder' ";
		}
		
		$result = $this->db->$executionType($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row['returnedQty'];
	}
	
	private function getIssuedQty_auto_sql($item,$orderNo,$orderYear,$executionType)
	{
		global $session_locationId;
		
		$sql = "SELECT SUM(dblQty) issuedQty
				FROM ware_stocktransactions
				WHERE intItemId = '$item' AND
				intLocationId = '$session_locationId' AND
				intOrderNo = '$orderNo' AND
				intOrderYear = '$orderYear' AND
 				strType = 'ISSUE' ";
		
		$result = $this->db->$executionType($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row['issuedQty'];
	}
	private function getReturnedQty_auto_sql($item,$orderNo,$orderYear,$executionType)
	{
		global $session_locationId;
		
		$sql = "SELECT SUM(dblQty) returnedQty
				FROM ware_stocktransactions
				WHERE intItemId = '$item' AND
				intLocationId = '$session_locationId' AND
				intOrderNo = '$orderNo' AND
				intOrderYear = '$orderYear' AND
 				strType = 'RETSTORES' ";
		
		$result = $this->db->$executionType($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row['returnedQty'];
	}
	
	
	private function getAllocationPercentages_sql($company,$executionType)
	{
		$sql 	= "SELECT * FROM sys_company_item_allocation_percentages WHERE COMPANY_ID = '$company' ";
		$result = $this->db->$executionType($sql);
		$row	= mysqli_fetch_array($result);
 		return $row;
	}
	private function getGrnWiseStockBalance_bulk_sql($location,$item)
	{
		 $sql = "SELECT
				Sum(ware_stocktransactions_bulk.dblQty) AS stockBal,
				ware_stocktransactions_bulk.intGRNNo,
				ware_stocktransactions_bulk.intGRNYear,
				ware_stocktransactions_bulk.dtGRNDate,
				ware_stocktransactions_bulk.dblGRNRate 
				FROM ware_stocktransactions_bulk 
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$location'
				GROUP BY
				ware_stocktransactions_bulk.intItemId,
				ware_stocktransactions_bulk.intGRNNo,
				ware_stocktransactions_bulk.intGRNYear, 
				ware_stocktransactions_bulk.dblGRNRate 
				ORDER BY
				ware_stocktransactions_bulk.dtGRNDate ASC";
		
		$result = $this->db->RunQuery2($sql);
		return $result;	
	}
	private function loadCurrency_sql($grnNo,$grnYear,$item)
	{
		if(($grnNo!=0) && ($grnYear!=0))
		{
			$sql = "SELECT
					trn_poheader.intCurrency as currency 
					FROM
					ware_grnheader
					Inner Join trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
					WHERE
					ware_grnheader.intGrnNo =  '$grnNo' AND
					ware_grnheader.intGrnYear =  '$grnYear'";
		}
		else
		{
			$sql = "SELECT
					mst_item.intCurrency as currency 
					FROM mst_item
					WHERE
					mst_item.intId =  '$item'";
		}
		
		$result = $this->db->RunQuery2($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row['currency'];
	}
	private function getAllocatedData_sql($orderYear,$orderNo,$salesOrderId,$itemId,$location,$executionType)
	{
		$sql = "SELECT * FROM 
				ware_stocktransactions_bulk
				WHERE intLocationId = '$location' AND
				intItemId = '$itemId' AND
				intOrderNo = '$orderNo' AND
				intOrderYear = '$orderYear' ";
		if($salesOrderId != ''){		
		$sql .= " AND
				intSalesOrderId = '$salesOrderId'  ";
		}
				
		$sql .= "AND
				strType IN ('25_ITEM_ALLOC','100_ITEM_ALLOC')
				ORDER BY strType ASC,dtGRNDate ASC ";
		
		$result = $this->db->$executionType($sql);
		return $result;	
	}
	
	private function get_order_wise_stock_balance_sql($location,$itemId){
		
		$sql ="SELECT
			ware_stocktransactions.intOrderNo,
			ware_stocktransactions.intOrderYear,
			ware_stocktransactions.intSalesOrderId,
			ware_stocktransactions.intItemId,
			mst_item.strName,
			Sum(ware_stocktransactions.dblQty) as stk_bal 
			FROM
			ware_stocktransactions
			INNER JOIN mst_item ON ware_stocktransactions.intItemId = mst_item.intId
			WHERE
			ware_stocktransactions.intOrderNo IS NOT NULL AND
			ware_stocktransactions.intOrderYear IS NOT NULL AND
			ware_stocktransactions.intSalesOrderId IS NOT NULL AND
			ware_stocktransactions.intItemId = '$itemId' AND
			ware_stocktransactions.intLocationId = '$location'
			GROUP BY
			ware_stocktransactions.intOrderNo,
			ware_stocktransactions.intOrderYear,
			ware_stocktransactions.intSalesOrderId,
			ware_stocktransactions.intItemId";
		
		return $sql;
		
	}
	
}

?>
<?php
class cls_issue_get
{
	
	private $db;
		
	function __construct($db)
	{
		$this->db = $db;	
	}
//BEGIN - PUBLIC FUNCTIONS {	
	public function get_header($serialNo,$serialYear,$executeType)
	{
		$sql	= $this->get_header_sql($serialNo,$serialYear);
		$row	= $this->get_row($sql,$executeType);
		return $row;
	}
	
	public function get_details_results($serialNo,$serialYear,$executeType)
	{
		$sql	= $this->get_details_sql($serialNo,$serialYear);
		$result	= $this->get_results($sql,$executeType);
		return $result;
	}
	
//END	- PUBLIC FUNCTIONS }
	
//BEGIN - PRIVATE FUNCTIONS {
	private function get_header_sql($serialNo,$serialYear)
	{		
		$sql	= "SELECT
					  IH.intCompanyId       AS LOCATION_ID,
					  LO.strName            AS LOCATION_NAME,
					  IH.intDepartment      AS DEPARTMENT_ID,
					  D.strName             AS DEPARTMENT_NAME,
					  IH.intStatus      	AS STATUS,
					  IH.intApproveLevels   AS LEVELS,
					  IH.strRemarks         AS REMARKS,
					  IH.intUser,
					  IH.dtmCreateDate,
					  IH.intModifiedBy,
					  IH.dtmModifiedDate,
					  IH.datdate            AS DATE,
					  sys_users.strUserName AS USER
					FROM `ware_issueheader` IH
					  INNER JOIN sys_users
						ON IH.intUser = sys_users.intUserId
					  INNER JOIN mst_locations LO
						ON LO.intId = IH.intCompanyId
					  INNER JOIN mst_department D
						ON D.intId = IH.intDepartment
					WHERE IH.intIssueNo = '$serialNo'
						AND IH.intIssueYear = '$serialYear'";		
		return $sql;		
	}

	private function get_details_sql($serialNo,$serialYear){
		
		$sql	= "SELECT
					  ware_issuedetails.intIssueNo   AS ORDER_NO,
					  ware_issuedetails.intOrderYear AS ORDER_YEAR,
					  ware_issuedetails.strStyleNo   AS SALES_ORDER_ID,
					  mst_maincategory.intId         AS MAIN_CATEGORY_ID,
					  mst_maincategory.strName       AS MAIN_CATEGORY_NAME,
					  mst_item.intSubCategory        AS SUB_CATEGORY_ID,
					  mst_subcategory.strName        AS SUB_CATEGORY_NAME,
					  ware_issuedetails.intItemId    AS ITEM_ID,
					  mst_item.strName               AS ITEM_NAME,
					  ware_issuedetails.dblQty       AS QTY,
					  ware_issuedetails.dblReturnQty AS RETURN_QTY
					FROM `ware_issuedetails`
					  LEFT JOIN mst_item
						ON ware_issuedetails.intItemId = mst_item.intId
					  INNER JOIN mst_subcategory
						ON mst_item.intSubCategory = mst_subcategory.intId
					  INNER JOIN mst_maincategory
						ON mst_item.intMainCategory = mst_maincategory.intId
					WHERE ware_issuedetails.intIssueNo = '$serialNo'
						AND ware_issuedetails.intIssueYear = '$serialYear' ";		
		return $sql;
		
	}

	private function get_results($sql,$executeType){
	
		$result	= $this->db->$executeType($sql);
		return $result;		
	}
	private function get_row($sql,$executeType){
		$result	= $this->db->$executeType($sql);
		$row=mysqli_fetch_array($result);
		return $row;		
	}


//END 	- PRIVATE FUNCTIONS }
}
?>
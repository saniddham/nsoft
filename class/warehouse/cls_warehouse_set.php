<?php

class cls_warehouse_set
{
	
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}

	
	public function insert_ware_stocktransactions_bulk($company,$location,$docNo,$docYear,$grnNo,$grnYear,$grnDate,$grnRate,$currency,$item,$saveQty,$type,$userId){
		
				$sql = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intUser,dtDate) 
							VALUES ('$company','$location','$docNo','$docYear','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$item','$saveQty','$type','$userId',now())";
				$result=$this->db->RunQuery2($sql);
				
				$data['result'] = $result;
				if($result==1){
				$data['type'] = 'pass';
				$data['q'] = $sql;
				}
				else{
				$data['type'] = 'fail';
				$data['q'] = $sql;
				}
				return $data;
	}
	
	public function insert_ware_stocktransactions_bulk_order_wise($company,$location,$docNo,$docYear,$grnNo,$grnYear,$grnDate,$grnRate,$currency,$orderNo,$orderYear,$salesOrder,$item,$saveQty,$type,$userId){
		
				$sql = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intUser,dtDate) 
							VALUES ('$company','$location','$docNo','$docYear','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$item','$saveQty','$type','$userId',now())";
				$result=$this->db->RunQuery2($sql);
				
				$data['result'] = $result;
				if($result==1){
				$data['type'] = 'pass';
				$data['q'] = '';
				}
				else{
				$data['type'] = 'fail';
				$data['q'] = $sql;
				}
				return $data;
	}
	
	
	public function insert_ware_stocktransactions($company,$location,$docNo,$docYear,$grnNo,$grnYear,$grnDate,$grnRate,$currency,$orderNo,$orderYear,$salesOrder,$item,$saveQty,$type,$userId){
		
				$sql = "INSERT INTO `ware_stocktransactions` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intOrderNo,intOrderYear,intSalesOrderId,intItemId,dblQty,strType,intUser,dtDate) 
							VALUES ('$company','$location','$docNo','$docYear','$grnNo','$grnYear','$grnDate','$grnRate','$currency',$orderNo,$orderYear,$salesOrder,'$item','$saveQty','$type','$userId',now())";
				$result=$this->db->RunQuery2($sql);
				
				$data['result'] = $result;
				if($result==1){
				$data['type'] = 'pass';
				$data['q'] = '';
				}
				else{
				$data['type'] = 'fail';
				$data['q'] = $sql;
				}
				return $data;
	}
	
	public function ware_sub_stocktransactions_bulk($company,$location,$docNo,$docYear,$grnNo,$grnYear,$grnDate,$grnRate,$currency,$item,$saveQty,$type,$userId){
		
				$sql = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intUser,dtDate) 
							VALUES ('$company','$location','$docNo','$docYear','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$item','$saveQty','$type','$userId',now())";
				$result=$this->db->RunQuery2($sql);
				
				$data['result'] = $result;
				if($result==1){
				$data['type'] = 'pass';
				$data['q'] = '';
				}
				else{
				$data['type'] = 'fail';
				$data['q'] = $sql;
				}
				return $data;
	}
	
	public function ware_sub_stocktransactions_bulk_order_wise($company,$location,$docNo,$docYear,$grnNo,$grnYear,$grnDate,$grnRate,$currency,$orderNo,$orderYear,$salesOrder,$item,$saveQty,$type,$userId){
		
				$sql = "INSERT INTO `ware_sub_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intOrderNo,intOrderYear,intSalesOrderId,intItemId,dblQty,strType,intUser,dtDate) 
							VALUES ('$company','$location','$docNo','$docYear','$grnNo','$grnYear','$grnDate','$grnRate','$currency',$orderNo,$orderYear,$salesOrder,'$item','$saveQty','$type','$userId',now())";
				$result=$this->db->RunQuery2($sql);
				
				$data['result'] = $result;
				if($result==1){
				$data['type'] = 'pass';
				$data['q'] = '';
				}
				else{
				$data['type'] = 'fail';
				$data['q'] = $sql;
				}
				return $data;
	}
	
	
	public function ware_sub_color_stocktransactions_bulk($company,$location,$docNo,$docYear,$grnNo,$grnYear,$grnDate,$grnRate,$currency,$item,$saveQty,$type,$userId){
		
				$sql = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intUser,dtDate) 
							VALUES ('$company','$location','$docNo','$docYear','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$item','$saveQty','$type','$userId',now())";
				$result=$this->db->RunQuery2($sql);
				
				$data['result'] = $result;
				if($result==1){
				$data['type'] = 'pass';
				$data['q'] = '';
				}
				else{
				$data['type'] = 'fail';
				$data['q'] = $sql;
				}
				return $data;
	}

	
	public function ware_sub_color_stocktransactions_bulk_order_wise($company,$location,$docNo,$docYear,$grnNo,$grnYear,$grnDate,$grnRate,$currency,$orderNo,$orderYear,$salesOrder,$item,$saveQty,$type,$userId){
		
				$sql = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intUser,dtDate) 
							VALUES ('$company','$location','$docNo','$docYear','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$item','$saveQty','$type','$userId',now())";
				$result=$this->db->RunQuery2($sql);
				
				$data['result'] = $result;
				if($result==1){
				$data['type'] = 'pass';
				$data['q'] = '';
				}
				else{
				$data['type'] = 'fail';
				$data['q'] = $sql;
				}
				return $data;
	}
	
	
	
	
	
	
	
	private function update($company,$location,$arr){
		
	}
	
	
	private function delete($company,$location,$arr){
		
	}
	
//--------------------------------------------------------------
}
?>
<?php
class cls_returnToStores_get
{
	
	private $db;
		
	function __construct($db)
	{
		$this->db = $db;	
	}
//BEGIN - PUBLIC FUNCTIONS {	
	public function get_header($serialNo,$serialYear,$executeType)
	{
		$sql	= $this->get_header_sql($serialNo,$serialYear);
		$row	= $this->get_row($sql,$executeType);
		return $row;
	}
	
	public function get_details_results($serialNo,$serialYear,$executeType)
	{
		$sql	= $this->get_details_sql($serialNo,$serialYear);
		$result	= $this->get_results($sql,$executeType);
		return $result;
	}
	
//END	- PUBLIC FUNCTIONS }
	
//BEGIN - PRIVATE FUNCTIONS {
	private function get_header_sql($serialNo,$serialYear)
	{		
		$sql	= "SELECT 
					LO.intCompanyId					AS COMPANY_ID,
					RSH.intCompanyId				AS LOCATION_ID,
					LO.strName 						AS LOCATION_NAME,
					MH.intDepartment				AS DEPARTMENT_ID,
					D.strName  						AS DEPARTMENT_NAME,
					RSH.intStatus					AS STATUS,
					RSH.intApproveLevels 			AS LEVELS,
					RSH.intUser,
					RSH.dtmCreateDate,
					RSH.intModifiedBy,
					RSH.dtmModifiedDate,
					RSH.datdate 					AS DATE, 
					sys_users.strUserName 			AS USER,
					MH.datdate 						AS MRN_DATE ,
					MONTH(MH.datdate)				AS BUDG_MONTH ,
					RSH.intIssueNo					AS ISSUE_NO,
					RSH.intIssueYear 				AS ISSUE_YEAR 
					FROM `ware_returntostoresheader` RSH
					INNER JOIN ware_issueheader AS IH ON RSH.intIssueNo = IH.intIssueNo AND RSH.intIssueYear = IH.intIssueYear
					INNER JOIN ware_issuedetails AS ID ON IH.intIssueNo = ID.intIssueNo AND IH.intIssueYear = ID.intIssueYear
					INNER JOIN ware_mrnheader AS MH ON ID.intMrnNo = MH.intMrnNo AND ID.intMrnYear = MH.intMrnYear
					INNER JOIN sys_users ON RSH.intUser = sys_users.intUserId
					INNER JOIN mst_locations LO ON LO.intId = RSH.intCompanyId
					INNER JOIN mst_department D ON D.intId = MH.intDepartment
					WHERE
					RSH.intReturnNo = '$serialNo' AND
					RSH.intReturnYear = '$serialYear'";
		
		return $sql;		
	}

	private function get_details_sql($serialNo,$serialYear){
		
		 $sql	= "SELECT 
					RSD.strOrderNo 					AS ORDER_NO,
					RSD.intOrderYear 				AS ORDER_YEAR,
					RSD.strStyleNo 					AS SALES_ORDER_ID,
					mst_maincategory.intId 			AS MAIN_CATEGORY_ID,
					mst_maincategory.strName		AS MAIN_CATEGORY_NAME,
					mst_item.intSubCategory			AS SUB_CATEGORY_ID,
					mst_subcategory.strName			AS SUB_CATEGORY_NAME,
					RSD.intItemId					AS ITEM_ID,
					mst_item.strName 				AS ITEM_NAME,
					RSD.dblQty						AS QTY 
					FROM `ware_returntostoresdetails` AS RSD
					LEFT JOIN mst_item ON RSD.intItemId = mst_item.intId 
					INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
					INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId 
					WHERE
					RSD.intReturnNo = '$serialNo' AND
					RSD.intReturnYear = '$serialYear' ";
		
		return $sql;
		
	}

	private function get_results($sql,$executeType){
	
		$result	= $this->db->$executeType($sql);
		return $result;		
	}
	private function get_row($sql,$executeType){
		$result	= $this->db->$executeType($sql);
		$row=mysqli_fetch_array($result);
		return $row;		
	}
 

//END 	- PRIVATE FUNCTIONS }
}
?>
<?php
include_once  ("../../../../class/cls_commonFunctions_get.php");
include_once  ("../../../../class/cls_commonErrorHandeling_get.php");
//ini_set('display_errors', 1); 

$obj_commonErr		= new cls_commonErrorHandeling_get($db);
$obj_common			= new cls_commonFunctions_get($db);

$session_userId		= $_SESSION["userId"];
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];
$programCode		='P0790';

class cls_general_stock_dispose_set
{
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	
	public function save_header($serialYear,$serialNo,$date,$note,$approveLevels,$status,$location,$userId,$executeType)
	{ 

   		$sql	= $this->save_header_sql($serialYear,$serialNo,$date,$note,$approveLevels,$status,$location,$userId);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		}
			return $response;
 	}
	
	public function update_header($serialYear,$serialNo,$date,$note,$approveLevels,$status,$location,$userId,$executeType)
	{ 

   		$sql	= $this->update_header_sql($serialYear,$serialNo,$date,$note,$approveLevels,$status,$location,$userId);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		}
			return $response;
 	}
	
	public function updateHeaderStatus($serialYear,$serialNo,$status,$executeType){
   		$sql	= $this->update_header_status_sql($serialYear,$serialNo,$status);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		}
			return $response;
		
	}
	
	
	public function approved_by_insert($serialYear,$serialNo,$user,$approval,$executeType){
   		$sql	= $this->save_approved_by_header_sql($serialYear,$serialNo,$user,$approval);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		}
			return $response;
	}
	
	public function approved_by_update($serialYear,$serialNo,$maxAppByStatus,$executeType)
	{ 

   		$sql	= $this->load_update_approved_by_sql($serialYear,$serialNo,$maxAppByStatus);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		}
			return $response;
 	}

	public function delete_details($serialYear,$serialNo,$executeType)
	{ 

   		$sql	= $this->delete_details_sql($serialYear,$serialNo);
		$result = $this->db->$executeType($sql);
		/*if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{*/
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		/*}*/
			return $response;
 	}
	
	public function save_details($serialNo,$serialYear,$company,$location,$grnNo,$grnYear,$grnDate,$rate,$currency,$itemId,$saveQty,$userId,$executeType)
	{ 

   		$sql	= $this->save_details_sql($serialNo,$serialYear,$company,$location,$grnNo,$grnYear,$grnDate,$rate,$currency,$itemId,$saveQty,$userId);
		$result = $this->db->$executeType($sql);
		if(!$result){
			$response['type']	= 'fail';
			$response['msg']	= $this->db->errormsg;
			$response['sql']	= $sql;
		}
		else{
			$response['type']	= 'pass';
			$response['msg']	= '';
			$response['sql']	= $sql;
		}
			return $response;
 	}
	
public function sendFinlConfirmedEmailToUser($serialNo,$year,$objMail,$mainPat,$root_path,$executeType){
			global $db;

			$sql = $this->Load_mail_user_details_sql($serialNo,$year);
			$result = $db->$executeType($sql);
			$row=mysqli_fetch_array($result);
			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="CONFIRMED DISPOSAL ('$serialNo'/'$year')"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST = NULL;
			
			$_REQUEST['program']='GENERAL ITEMDISPOSAL';
			$_REQUEST['field1']='Dispose No';
			$_REQUEST['field2']='Dispose Year';
			$_REQUEST['field3']='';
			$_REQUEST['field4']='';
			$_REQUEST['field5']='';
			$_REQUEST['value1']=$serialNo;
			$_REQUEST['value2']=$year;
			$_REQUEST['value3']='';
			$_REQUEST['value4']='';
			$_REQUEST['value5']='';
			
			$_REQUEST['subject']="CONFIRMED DISPOSAL ('$serialNo'/'$year')";
			
			$_REQUEST['statement1']="Approved this";
			$_REQUEST['statement2']="to view this";
			$_REQUEST['link']=base64_encode($mainPath."presentation/warehouse/general_stock_dispose/listing/rpt_general_stock_dispose.php?serialNo=$serialNo&year=$year&mode=0"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}
 
public function sendRejecedEmailToUser($serialNo,$year,$objMail,$mainPat,$root_path,$executeType){
			global $db;

			$sql = $this->Load_mail_user_details_sql($serialNo,$year);
			$result = $db->$executeType($sql);
			$row=mysqli_fetch_array($result);
			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="REJECTED DISPOSAL ('$serialNo'/'$year')"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST = NULL;
			
			$_REQUEST['program']='GENERAL ITEMDISPOSAL';
			$_REQUEST['field1']='Dispose No';
			$_REQUEST['field2']='Dispose Year';
			$_REQUEST['field3']='';
			$_REQUEST['field4']='';
			$_REQUEST['field5']='';
			$_REQUEST['value1']=$serialNo;
			$_REQUEST['value2']=$year;
			$_REQUEST['value3']='';
			$_REQUEST['value4']='';
			$_REQUEST['value5']='';
			
			$_REQUEST['subject']="REJECTED DISPOSAL ('$serialNo'/'$year')";
			
			$_REQUEST['statement1']="Rejected this";
			$_REQUEST['statement2']="to view this";
			$_REQUEST['link']=base64_encode($mainPath."presentation/warehouse/general_stock_dispose/listing/rpt_general_stock_dispose.php?serialNo=$serialNo&year=$year&mode=0"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}
 	
	
	private function delete_details_sql($serialYear,$serialNo)
	{
 		
		$sql	= "DELETE 
					FROM ware_general_item_disposal_details 
					WHERE
					ware_general_item_disposal_details.DISPOSE_NO = '$serialNo' AND
					ware_general_item_disposal_details.YEAR = '$serialYear'";
 		return $sql;
	}
	
	private function save_details_sql($serialNo,$serialYear,$company,$location,$grnNo,$grnYear,$grnDate,$rate,$currency,$itemId,$saveQty,$userId)
	{
		
	 	$sql	= "INSERT INTO ware_general_item_disposal_details 
					(DISPOSE_NO, 
					YEAR, 
					GRN_NO,
					GRN_YEAR,
					CURRENCY,
					RATE,
					GRN_DATE,
					ITEM,
					QTY)
					VALUES
					('$serialNo', 
					'$serialYear', 
					'$grnNo',
					'$grnYear',
					'$currency',
					'$rate',
					'$grnDate',
					'$itemId',
					'$saveQty');";
		return $sql;
	}
	
	private function save_header_sql($serialYear,$serialNo,$date,$note,$approveLevels,$status,$location,$userId){
	 
	  	$sql	= "INSERT INTO ware_general_item_disposal_header 
					(DISPOSE_NO, 
					YEAR, 
					NOTE,
					STATUS,
					LEVELS,
					DATE,
					CREATE_DATE,
					USER,
					LOCATION)
					VALUES
					('$serialNo', 
					'$serialYear', 
					'$note',
					'$status',
					'$approveLevels',
					'$date',
					now(),
					'$userId',
					'$location');";
		return $sql;
		
	}
	private function update_header_sql($serialYear,$serialNo,$date,$note,$approveLevels,$status,$location,$userId){
			$sql = "UPDATE `ware_general_item_disposal_header` 
			SET  
			NOTE ='$note',
			DATE ='$date',
			STATUS ='$status',
			LEVELS ='$approveLevels',
			MODIFIED_BY ='$userId',
			MODIFIED_DATE =now()
 					WHERE (`DISPOSE_NO`='$serialNo') AND (`YEAR`='$serialYear') ";
		return $sql;
	}
	
	private function update_header_status_sql($serialYear,$serialNo,$status){
		if($status !=''){
			$sql= "UPDATE `ware_general_item_disposal_header` 
			SET  
			STATUS=$status  
  					WHERE (`DISPOSE_NO`='$serialNo') AND (`YEAR`='$serialYear')";
		}
		else{
			$sql= "UPDATE `ware_general_item_disposal_header` 
			SET  
			STATUS=STATUS-1 
  					WHERE (`DISPOSE_NO`='$serialNo') AND (`YEAR`='$serialYear')";
		}
		return $sql;
	}
	
 	private function load_update_approved_by_sql($serialYear,$serialNo,$maxAppByStatus){
		 	$sql = "UPDATE `ware_general_item_disposal_approveby` 
			SET  
			intStatus ='$maxAppByStatus' 
 					WHERE (`intDisposeNo`='$serialNo') AND (`intYear`='$serialYear')  AND (`intStatus`='0')";
		return $sql;
		
	}
	
	private function save_approved_by_header_sql($serialYear,$serialNo,$userId,$approval){
		
		$sql = "INSERT INTO `ware_general_item_disposal_approveby` (`intDisposeNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
			VALUES ('$serialNo','$serialYear','$approval','$userId',now(),0)";
		return $sql;
		
	}
	
	private function Load_mail_user_details_sql($serialNo,$year){
	
		    $sql = "SELECT
			sys_users.strFullName,
			sys_users.strEmail 
			FROM 
			ware_general_item_disposal_header
			Inner Join sys_users ON ware_general_item_disposal_header.USER = sys_users.intUserId
			WHERE
			ware_general_item_disposal_header.DISPOSE_NO =  '$serialNo' AND
			ware_general_item_disposal_header.YEAR =  '$year'";
			
			return $sql;
		
	}
 
 }
?>
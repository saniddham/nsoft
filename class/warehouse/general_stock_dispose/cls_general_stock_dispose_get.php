<?php
include_once  ("../../../../class/cls_commonErrorHandeling_get.php");
//ini_set('display_errors', 1); 
 
$obj_commonErr		= new cls_commonErrorHandeling_get($db);

$progrmCode			='P0804';

$session_userId		= $_SESSION["userId"];
$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];

class cls_general_stock_dispose_get
{
	private $db;
 	
	function __construct($db)
	{
		$this->db = $db;
 	}

//BEGIN - PUBLIC FUNCTIONS {	
	public function get_header_array($serialNo,$year,$executeType)
	{
		
		$sql = $this->Load_header_sql($serialNo,$year);
		$result = $this->db->$executeType($sql);
		$row = mysqli_fetch_array($result);
		return $row;
	}
	public function get_rpt_header_array($serialNo,$year,$executeType)
	{
		
		$sql = $this->Load_header_sql($serialNo,$year);
		$result = $this->db->$executeType($sql);
		$row = mysqli_fetch_array($result);
		return $row;
	}
	
	public function getMaxAppByStatus($serialNo,$year,$executeType)
	{
		
		$sql = $this->Load_max_approve_by_sql($serialNo,$year);
		$result = $this->db->$executeType($sql);
		$row = mysqli_fetch_array($result);
		return $row['status'];
	}
	
	public function get_details_result($serialNo,$year,$executeType)
	{
		$sql = $this->Load_details_sql($serialNo,$year);
		$result = $this->db->$executeType($sql);
		return $result;
	}
	public function get_Report_approval_details_result($serialNo,$year,$executeType){
		$sql = $this->Load_Report_approval_details_sql($serialNo,$year);
		$result = $this->db->$executeType($sql);
		return $result;
	}
	
 	
	public function ValidateBeforeSave($serialNo,$serialYear,$executeType)
	{ 
		global $session_userId;
		global $obj_commonErr;	
		global $progrmCode;
		$this->db->begin();
   		$header_arr			= $this->get_header_array2($serialYear,$serialNo,$executeType);
 		$permision_arr		= $obj_commonErr->get_permision_withApproval_save($header_arr['STATUS'],$header_arr['LEVELS'],$session_userId,$progrmCode,$executeType);
		if($permision_arr['type'] =='fail'){
			$rollBack			=	1;
			$response['msg'] 	=	$permision_arr['msg'];	
			$response['type'] 	=	'fail';
			return $response;
		}
		$this->db->commit();
	}
	public function getDetails($location,$subCatId,$itemId,$executeType){
		
		$subCat  = $_REQUEST['subCat'];
		$itemId  = $_REQUEST['itemId'];
		
		     $sql="SELECT
				round(Sum(ware_stocktransactions_bulk.dblQty),4) AS stockBal,
				mst_item.intMainCategory,
				mst_maincategory.strName as mainCatName,
				mst_item.intSubCategory,
				mst_subcategory.strName as subCatName,
				ware_stocktransactions_bulk.intGRNNo,
				ware_stocktransactions_bulk.intGRNYear,
				ware_stocktransactions_bulk.dtGRNDate,
				ware_stocktransactions_bulk.dblGRNRate, 
				ware_stocktransactions_bulk.intCurrencyId,
				ware_stocktransactions_bulk.intItemId, 
				mst_item.strName as itemName, 
				mst_item.strCode,
                mst_item.strCode as SUP_ITEM_CODE,
				mst_item.intUOM,
				mst_units.strCode as uom , 
				mst_financecurrency.strCode as currency 
				FROM
				ware_stocktransactions_bulk
				Inner Join mst_item ON ware_stocktransactions_bulk.intItemId = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
				Inner Join mst_financecurrency ON ware_stocktransactions_bulk.intCurrencyId = mst_financecurrency.intId
                                LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
				WHERE
				ware_stocktransactions_bulk.intLocationId =  '$location' ";
			if($itemId!=''){ 
			$sql .="  AND ware_stocktransactions_bulk.intItemId =  '$itemId' "; 
			}
			$sql .="  AND mst_item.intSubCategory =  '$subCatId'  "; 
			$sql .=" GROUP BY
				ware_stocktransactions_bulk.intItemId, 
				ware_stocktransactions_bulk.intGRNNo,
				ware_stocktransactions_bulk.intGRNYear,
				ware_stocktransactions_bulk.dtGRNDate,
				ware_stocktransactions_bulk.dblGRNRate 
				HAVING stockBal > 0";
				//echo $sql;
			$result = $this->db->$executeType($sql);
		while($row=mysqli_fetch_array($result))
		{
			$arrData[] = $row;
		}
		$response['arrData'] 	= $arrData;
		return $arrData;
	}
	
private function Load_details_sql($serialNo,$year){
	$sql = "SELECT
			ware_general_item_disposal_details.DISPOSE_NO,
			ware_general_item_disposal_details.`YEAR`,
			ware_general_item_disposal_details.GRN_NO,
			ware_general_item_disposal_details.GRN_YEAR,
			ware_general_item_disposal_details.CURRENCY,
			ware_general_item_disposal_details.RATE,
			ware_general_item_disposal_details.GRN_DATE,
			ware_general_item_disposal_details.ITEM,
			ware_general_item_disposal_details.QTY,
			mst_financecurrency.strCode as curr,
			mst_item.strCode,
            mst_item.strCode as SUP_ITEM_CODE,
			mst_item.strName AS itemNm,
			mst_item.intUOM,
			mst_units.strCode AS unit,
			mst_item.intMainCategory,
			mst_item.intSubCategory,
			mst_subcategory.strName as subCat,
			mst_maincategory.strName as mainCat,
			ware_grnheader.dblExchangeRate AS EXCH_RATE 
			FROM
			ware_general_item_disposal_header
			INNER JOIN ware_general_item_disposal_details ON ware_general_item_disposal_header.DISPOSE_NO = ware_general_item_disposal_details.DISPOSE_NO AND ware_general_item_disposal_header.`YEAR` = ware_general_item_disposal_details.`YEAR`
			INNER JOIN mst_financecurrency ON ware_general_item_disposal_details.CURRENCY = mst_financecurrency.intId
			INNER JOIN mst_item ON ware_general_item_disposal_details.ITEM = mst_item.intId
			INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
			INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
			INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
			LEFT JOIN ware_grnheader ON ware_general_item_disposal_details.GRN_NO = ware_grnheader.intGrnNo AND ware_general_item_disposal_details.GRN_YEAR = ware_grnheader.intGrnYear
			LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
                        WHERE
			ware_general_item_disposal_header.DISPOSE_NO = '$serialNo' AND
			ware_general_item_disposal_header.`YEAR` = '$year'
";
	
	return $sql;

}

  //END	- PUBLIC FUNCTIONS }

//BEGIN - PRIVATE FUNCTIONS {	
	private function Load_header_sql($serialNo,$year)
	{
		global $session_companyId;
		  $sql="SELECT
				ware_general_item_disposal_header.DISPOSE_NO,
				ware_general_item_disposal_header.`YEAR`,
				ware_general_item_disposal_header.NOTE,
				ware_general_item_disposal_header.`STATUS`,
				ware_general_item_disposal_header.LEVELS,
				ware_general_item_disposal_header.DATE,
				ware_general_item_disposal_header.CREATE_DATE,
				ware_general_item_disposal_header.`USER`,
				sys_users.strUserName as CREATOR, 
				ware_general_item_disposal_header.LOCATION,
				ware_general_item_disposal_header.MODIFIED_BY,
				ware_general_item_disposal_header.MODIFIED_DATE
				FROM `ware_general_item_disposal_header` 
				INNER JOIN sys_users ON ware_general_item_disposal_header.`USER` = sys_users.intUserId
				WHERE
				ware_general_item_disposal_header.DISPOSE_NO = '$serialNo' AND
				ware_general_item_disposal_header.`YEAR` = '$year'
				";
 		
  		return $sql;
 	}
	
	
	private function Load_max_approve_by_sql($serialNo,$year){
		$sql = "SELECT
				Max(trn_sample_size_wise_item_consumptions_approve_by.`STATUS`) status
				FROM `trn_sample_size_wise_item_consumptions_approve_by`
				WHERE
				trn_sample_size_wise_item_consumptions_approve_by.ORDER_NO = '$order_no' AND
				trn_sample_size_wise_item_consumptions_approve_by.ORDER_YEAR = '$order_year' AND
				trn_sample_size_wise_item_consumptions_approve_by.SALES_ORDER_ID = '$sales_order' AND
				trn_sample_size_wise_item_consumptions_approve_by.CONSUMPTION_TYPE_ID = '$type'";
		
		return $sql;
	
	}
	
	private function Load_Report_approval_details_sql($serialNo,$year){
	  	$sql = "SELECT
				ware_general_item_disposal_approveby.intApproveUser,
				ware_general_item_disposal_approveby.dtApprovedDate,
				sys_users.strUserName as UserName,
				ware_general_item_disposal_approveby.intApproveLevelNo
				FROM
				ware_general_item_disposal_approveby
				Inner Join sys_users ON ware_general_item_disposal_approveby.intApproveUser = sys_users.intUserId
				WHERE
				ware_general_item_disposal_approveby.intDisposeNo =  '$serialNo' AND
				ware_general_item_disposal_approveby.intYear =  '$year'      order by dtApprovedDate asc";
 		return $sql;
	}
	
	public function get_25_MaxAppByStatus($year,$serialNo,$executeType){

		$sql				= $this->get_25_MaxAppByStatus_sql($serialNo,$year);
		$result 			= $this->db->$executeType($sql);
		$row 				= mysqli_fetch_array($result);
		$sampCons 			= $row['status'];
		return $sampCons;
	
	}
	
	 private function get_25_MaxAppByStatus_sql($serialNo,$year){
			$sql = "SELECT
					max(ware_general_item_disposal_approveby.intStatus) as status 
					FROM
					ware_general_item_disposal_approveby
					WHERE
					ware_general_item_disposal_approveby.intDisposeNo =  '$serialNo' AND
					ware_general_item_disposal_approveby.intYear =  '$year'";
			return $sql;	
	}
}

?>
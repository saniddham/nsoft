<?php
class cls_gatepass_get
{
	private $db;
		
	function __construct($db)
	{
		$this->db = $db;	
	}
	public function get_header($serialNo,$year,$executionType)
	{
		return $this->get_header_sql($serialNo,$year,$executionType);
	}
	public function getSubCategory($itemId,$executionType)
	{
		return $this->getSubCategory_sql($itemId,$executionType);
	}
	private function get_header_sql($serialNo,$year,$executionType)
	{
		$sql = "SELECT intGatePassNo, 
				intGatePassYear, 
				intGPToLocation, 
				strNote, 
				intStatus, 
				intApproveLevels, 
				datdate, 
				dtmCreateDate, 
				intUser, 
				intCompanyId, 
				intModifiedBy, 
				dtmModifiedDate
				 
				FROM 
				ware_gatepassheader 
				WHERE intGatePassNo = '$serialNo' AND
				intGatePassYear = '$year' ";
		
		$result	= $this->db->$executionType($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row;
	}
	private function getSubCategory_sql($itemId,$executionType)
	{
		$sql = "SELECT intSubCategory
				FROM mst_item
				WHERE intId = '$itemId' ";
		
		$result	= $this->db->$executionType($sql);
		$row	= mysqli_fetch_array($result);
		
		return $row['intSubCategory'];
	}
}
?>
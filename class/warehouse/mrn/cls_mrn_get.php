<?php
class cls_mrn_get
{
	
	private $db;
		
	function __construct($db)
	{
		$this->db = $db;	
	}
//BEGIN - PUBLIC FUNCTIONS {	
	public function get_header($serialNo,$serialYear,$executeType)
	{
		$sql	= $this->get_header_sql($serialNo,$serialYear);
		$row	= $this->get_row($sql,$executeType);
		return $row;
	}
	
	public function get_details_results($serialNo,$serialYear,$executeType)
	{
		$sql	= $this->get_details_sql($serialNo,$serialYear);
		$result	= $this->get_results($sql,$executeType);
		return $result;
	}
	
//END	- PUBLIC FUNCTIONS }
	
//BEGIN - PRIVATE FUNCTIONS {
	private function get_header_sql($serialNo,$serialYear)
	{		
		$sql	= "SELECT 
					LO.intCompanyId					AS COMPANY_ID,
					MH.intCompanyId					AS LOCATION_ID,
					LO.strName 						AS LOCATION_NAME,
					MH.intDepartment				AS DEPARTMENT_ID,
					D.strName  						AS DEPARTMENT_NAME,
					MH.intStatus					AS STATUS,
					MH.intApproveLevels 			AS LEVELS,
					MH.strRemarks					AS REMARKS,
					MH.intUser,
					MH.dtmCreateDate,
					MH.intModifiedBy,
					MH.dtmModifiedDate,
					mh.datdate 						AS DATE, 
					sys_users.strUserName AS USER
					FROM `ware_mrnheader` MH
					INNER JOIN sys_users ON MH.intUser = sys_users.intUserId
					INNER JOIN mst_locations LO ON LO.intId = MH.intCompanyId
					INNER JOIN mst_department D ON D.intId = MH.intDepartment
					WHERE
					MH.intMrnNo = '$serialNo' AND
					MH.intMrnYear = '$serialYear'";
		
		return $sql;		
	}

	private function get_details_sql($serialNo,$serialYear){
		
		 $sql	= "SELECT 
					ware_mrndetails.intOrderNo 					AS ORDER_NO,
					ware_mrndetails.intOrderYear 				AS ORDER_YEAR,
					ware_mrndetails.strStyleNo 					AS SALES_ORDER_ID,
					mst_maincategory.intId 						AS MAIN_CATEGORY_ID,
					mst_maincategory.strName 					AS MAIN_CATEGORY_NAME,
					mst_item.intSubCategory						AS SUB_CATEGORY_ID,
					mst_subcategory.strName 					AS SUB_CATEGORY_NAME,
					ware_mrndetails.intItemId					AS ITEM_ID,
					mst_item.strName 							AS ITEM_NAME,
					ware_mrndetails.dblQty						AS QTY,
					ware_mrndetails.dblIssudQty					AS ISSUED_QTY ,
					ware_mrndetails.dblMRNClearQty				AS MRN_CLEAR_QTY   
					FROM `ware_mrndetails` 
					LEFT JOIN mst_item ON ware_mrndetails.intItemId = mst_item.intId 
					INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
					INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId 
					WHERE
					ware_mrndetails.intMrnNo = '$serialNo' AND
					ware_mrndetails.intMrnYear = '$serialYear' ";
		
		return $sql;
		
	}

	private function get_results($sql,$executeType){
	
		$result	= $this->db->$executeType($sql);
		return $result;		
	}
	private function get_row($sql,$executeType){
		$result	= $this->db->$executeType($sql);
		$row=mysqli_fetch_array($result);
		return $row;		
	}


//END 	- PRIVATE FUNCTIONS }
}
?>
<?php
 session_start();
include_once $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
include_once $_SESSION['ROOT_PATH']."class/cls_commonErrorHandeling_get.php";
  
$obj_commonErr		= new cls_commonErrorHandeling_get($db);
$obj_common			= new cls_commonFunctions_get($db);
 
class cls_customer_revenue_get
{
	private $db;
 	
	function __construct($db)
	{
		$this->db 	= $db;
 	}

 	public function get_customers_result($month,$year,$toCurrency,$cluster)
	{
 		$sql		= $this->load_customers_sql($month,$year,$toCurrency,$cluster);
 		$result		= $this->db->RunQuery($sql);
 		return $result;
	}
 	public function get_saved_customers_result($monthArr,$ayearArr,$cluster)
	{
 		$sql		= $this->load_saved_customers_sql($monthArr,$ayearArr,$cluster);
 		$result		= $this->db->RunQuery($sql);
 		return $result;
	}
	
	public function getCustomerRevenue($customerId,$month,$year,$toCurrency,$cluster)
	{
 		$sqlp		= $this->load_CustomerRevenue_sql($customerId,$month,$year,$toCurrency,$cluster);
		$resultp	= $this->db->RunQuery($sqlp);
		$row 		= mysqli_fetch_array($resultp);
		return $row['REVENUE'];
 	}
	public function getCustomerRM($customerId,$month,$year,$toCurrency,$cluster)
	{
 		$sqlp		= $this->load_CustomerRM_sql($customerId,$month,$year,$toCurrency,$cluster);
		$resultp	= $this->db->RunQuery($sqlp);
		$row 		= mysqli_fetch_array($resultp);
		return $row['RM_COST'];
 	}
	
	public function get_old_customer_revenue($customerId,$month,$year,$toCurrency,$cluster)
	{
 		$sqlp		= $this->load_old_customer_revenue_sql($customerId,$month,$year,$toCurrency,$cluster);
		$resultp	= $this->db->RunQuery($sqlp);
		$row 		= mysqli_fetch_array($resultp);
		return $row;
 	}
	
	
	
  
//BEGIN - PRIVATE FUNCTIONS {	
	private function load_customers_sql($month,$year,$toCurrency,$cluster)
	{
	  	 
		   $sql = "SELECT DISTINCT 
					mst_customer.intId,
					mst_customer.strName,
				IFNULL(sum(tb1.Qty1),0) as REVENUE
				from (SELECT 
				trn_orderheader.intCustomer, 
				/*Sum(ware_stocktransactions_fabric.dblQty*-1*trn_orderdetails.dblPrice*Ex_E.dblBuying/Ex_O.dblBuying) AS Qty1,*/
				Sum(ware_stocktransactions_fabric.dblQty*-1*trn_orderdetails.dblPrice) AS Qty1,
				mst_locations.intCompanyId 
				FROM
				ware_stocktransactions_fabric
				left JOIN mst_locations ON ware_stocktransactions_fabric.intLocationId = mst_locations.intId
				left JOIN trn_orderheader ON ware_stocktransactions_fabric.intOrderNo = trn_orderheader.intOrderNo AND ware_stocktransactions_fabric.intOrderYear = trn_orderheader.intOrderYear
				left JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear AND ware_stocktransactions_fabric.intSalesOrderId = trn_orderdetails.intSalesOrderId
				left JOIN mst_financeexchangerate as Ex_E ON trn_orderheader.intCurrency = Ex_E.intCurrencyId AND date(trn_orderheader.dtDate) = Ex_E.dtmDate AND mst_locations.intCompanyId = Ex_E.intCompanyId
				left JOIN mst_financeexchangerate as Ex_O ON $toCurrency = Ex_O.intCurrencyId AND date(trn_orderheader.dtDate) = Ex_O.dtmDate AND mst_locations.intCompanyId = Ex_O.intCompanyId
				LEFT JOIN mst_plant on mst_locations.intPlant = mst_plant.intPlantId
				WHERE  
				month(ware_stocktransactions_fabric.dtDate) 	= '$month' AND
				year(ware_stocktransactions_fabric.dtDate) 	= '$year' AND
				ware_stocktransactions_fabric.strType 		= 'Dispatched_G' AND 
				mst_plant.MAIN_CLUSTER_ID	='$cluster'
				
				GROUP BY trn_orderheader.intCustomer
				)
				 as tb1 
				left join mst_customer ON tb1.intCustomer=mst_customer.intId 
				GROUP BY tb1.intCustomer
				HAVING REVENUE > 0
				";	
			//	echo $sql;
  		return $sql;
 	}
	
	private function load_saved_customers_sql($monthArr,$yearArr,$cluster){
		$list='';
		for($j=0; $j<count($yearArr) ; $j++){
			$list	.=$yearArr[$j]."/".$monthArr[$j].",";
		}
		$list=substr($list, 0, -1);
		 $sql="select 
		 	DISTINCT  
			mst_customer.intId,
			mst_customer.strName
			FROM(
			SELECT 
			daily_mails_customer_revenue_old.CUSTOMER_ID,
			concat(YEAR,'/',MONTH) AS YM 
			FROM `daily_mails_customer_revenue_old` 
			WHERE daily_mails_customer_revenue_old.CLUSTER_ID = '$cluster'
			HAVING 
			FIND_IN_SET(YM,'$list')) as TB1 
			LEFT JOIN mst_customer on mst_customer.intId =  TB1.CUSTOMER_ID 
			ORDER BY mst_customer.strName ASC";
		
  		return $sql;
	}
	
//BEGIN - PRIVATE FUNCTIONS {	

 	private function load_CustomerRevenue_sql($customerId,$month,$year,$toCurrency,$cluster){
		
		$sql = "select 
				IFNULL((tb1.Qty1),0) as REVENUE
				from (SELECT 
				/*Sum(ware_stocktransactions_fabric.dblQty*-1*trn_orderdetails.dblPrice*Ex_E.dblBuying/Ex_O.dblBuying) AS Qty1 */
				Sum(ware_stocktransactions_fabric.dblQty*-1*trn_orderdetails.dblPrice) AS Qty1 
				FROM
				ware_stocktransactions_fabric
				left JOIN mst_locations ON ware_stocktransactions_fabric.intLocationId = mst_locations.intId
				left JOIN trn_orderheader ON ware_stocktransactions_fabric.intOrderNo = trn_orderheader.intOrderNo AND ware_stocktransactions_fabric.intOrderYear = trn_orderheader.intOrderYear
				left JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear AND ware_stocktransactions_fabric.intSalesOrderId = trn_orderdetails.intSalesOrderId
				left JOIN mst_financeexchangerate as Ex_E ON trn_orderheader.intCurrency = Ex_E.intCurrencyId AND date(trn_orderheader.dtDate) = Ex_E.dtmDate AND mst_locations.intCompanyId = Ex_E.intCompanyId
				left JOIN mst_financeexchangerate as Ex_O ON $toCurrency = Ex_O.intCurrencyId AND date(trn_orderheader.dtDate) = Ex_O.dtmDate AND mst_locations.intCompanyId = Ex_O.intCompanyId
				LEFT JOIN mst_plant on mst_locations.intPlant = mst_plant.intPlantId
				WHERE  
				trn_orderheader.intCustomer = '$customerId'		 AND
				month(ware_stocktransactions_fabric.dtDate) 	= '$month' AND
				year(ware_stocktransactions_fabric.dtDate) 	= '$year' AND
				ware_stocktransactions_fabric.strType 		= 'Dispatched_G' AND 
				mst_plant.MAIN_CLUSTER_ID	='$cluster'
				
 				)
				 as tb1  
				";	
			
			return $sql;
	}
 	private function load_CustomerRM_sql($customerId,$month,$year,$toCurrency,$cluster){	
			$sql="select 
				IFNULL((tb1.Qty1),0) as RM_COST
				from (SELECT 
				/*Sum(trn_sample_item_consumption.dblQty*trn_orderdetails.dblPrice*Ex_E.dblBuying/Ex_O.dblBuying) AS Qty1*/ 
				Sum(item_consump.consum*trn_orderdetails.dblPrice) AS Qty1 
				FROM
				ware_stocktransactions_fabric
				left JOIN mst_locations ON ware_stocktransactions_fabric.intLocationId = mst_locations.intId
				left JOIN trn_orderheader ON ware_stocktransactions_fabric.intOrderNo = trn_orderheader.intOrderNo AND ware_stocktransactions_fabric.intOrderYear = trn_orderheader.intOrderYear
				left JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear AND ware_stocktransactions_fabric.intSalesOrderId = trn_orderdetails.intSalesOrderId
				
				left Join 
				(select 
					t.type,
					t.intSampleNo,
					t.intSampleYear,
					t.intRevisionNo ,
					t.strCombo ,
					t.strPrintName ,
					 t.intId ,
					 sum(consum) as consum
					
					 from ((
					SELECT     
					1 as type, 
					sc.intSampleNo, 
					sc.intSampleYear, 
					sc.intRevisionNo, 
					sc.strCombo, 
					sc.strPrintName,
					 mst_item.intId,
					 sum(round(dblColorWeight/((SELECT
					Sum(trn_sample_color_recipes.dblWeight)  
					FROM trn_sample_color_recipes
					WHERE
					trn_sample_color_recipes.intSampleNo = sc.intSampleNo AND
					trn_sample_color_recipes.intSampleYear = sc.intSampleYear AND
					trn_sample_color_recipes.intRevisionNo =  sc.intRevisionNo AND
					trn_sample_color_recipes.strCombo =  sc.strCombo AND
					trn_sample_color_recipes.strPrintName =  sc.strPrintName AND
					trn_sample_color_recipes.intTechniqueId =  sc.intTechniqueId AND
					trn_sample_color_recipes.intInkTypeId =  trn_sampleinfomations_details_technical.intInkTypeId AND
					trn_sample_color_recipes.intColorId =  trn_sampleinfomations_details_technical.intColorId 
					 ))*dblWeight /dblNoOfPcs,6))as consum 
					FROM
					trn_sample_color_recipes as sc
					Inner Join trn_sampleinfomations_details_technical ON trn_sampleinfomations_details_technical.intSampleNo = sc.intSampleNo 
					AND trn_sampleinfomations_details_technical.intSampleYear = sc.intSampleYear 
					AND trn_sampleinfomations_details_technical.intRevNo = sc.intRevisionNo 
					AND trn_sampleinfomations_details_technical.strComboName = sc.strCombo 
					AND trn_sampleinfomations_details_technical.strPrintName = sc.strPrintName 
					AND trn_sampleinfomations_details_technical.intColorId = sc.intColorId 
					AND trn_sampleinfomations_details_technical.intInkTypeId = sc.intInkTypeId
					Inner Join mst_item ON mst_item.intId = sc.intItem
					Inner Join mst_units ON mst_item.intUOM = mst_units.intId
					group by 
					sc.intSampleNo, 
					sc.intSampleYear, 
					sc.intRevisionNo, 
					sc.strCombo, 
					sc.strPrintName ,
					 mst_item.intId 
					)
					UNION
					(
					SELECT  
							2 as type, 
					trn_sample_spitem_consumption.intSampleNo, 
					trn_sample_spitem_consumption.intSampleYear, 
					trn_sample_spitem_consumption.intRevisionNo, 
					trn_sample_spitem_consumption.strCombo, 
					trn_sample_spitem_consumption.strPrintName,
					 mst_item.intId, 
						   sum(trn_sample_spitem_consumption.dblQty/dblNoOfPcs) as consum 
						   FROM
						trn_sample_spitem_consumption 
							Inner Join mst_item ON mst_item.intId = trn_sample_spitem_consumption.intItem
						Inner Join mst_units ON mst_units.intId = mst_item.intUOM
					group by 
					trn_sample_spitem_consumption.intSampleNo, 
					trn_sample_spitem_consumption.intSampleYear, 
					trn_sample_spitem_consumption.intRevisionNo, 
					trn_sample_spitem_consumption.strCombo, 
					trn_sample_spitem_consumption.strPrintName ,
					 mst_item.intId 
					)
					
					UNION 
					(
					SELECT    
							3 as type, 
					trn_sample_foil_consumption.intSampleNo, 
					trn_sample_foil_consumption.intSampleYear, 
					trn_sample_foil_consumption.intRevisionNo, 
					trn_sample_foil_consumption.strCombo, 
					trn_sample_foil_consumption.strPrintName,
					 mst_item.intId, 
							sum(trn_sample_foil_consumption.dblMeters) as consum 
						FROM 
							trn_sample_foil_consumption  
							Inner Join mst_item ON mst_item.intId = trn_sample_foil_consumption.intItem
							Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
							Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
							Inner Join mst_units ON mst_units.intId = mst_item.intUOM
					group by 
					trn_sample_foil_consumption.intSampleNo, 
					trn_sample_foil_consumption.intSampleYear, 
					trn_sample_foil_consumption.intRevisionNo, 
					trn_sample_foil_consumption.strCombo, 
					trn_sample_foil_consumption.strPrintName ,
					 mst_item.intId 
					)) as t
					
					GROUP BY 
					t.type,
					t.intSampleNo,
					t.intSampleYear,
					t.intRevisionNo ,
					t.strCombo ,
					t.strPrintName,
					 t.intId) as item_consump
				ON item_consump.intSampleNo = trn_orderdetails.intSampleNo AND item_consump.intSampleYear = trn_orderdetails.intSampleYear AND item_consump.intRevisionNo = trn_orderdetails.intRevisionNo AND item_consump.strCombo = trn_orderdetails.strCombo AND item_consump.strPrintName = trn_orderdetails.strPrintName 
				left JOIN mst_financeexchangerate as Ex_E ON trn_orderheader.intCurrency = Ex_E.intCurrencyId AND date(trn_orderheader.dtDate) = Ex_E.dtmDate AND mst_locations.intCompanyId = Ex_E.intCompanyId
				left JOIN mst_financeexchangerate as Ex_O ON $toCurrency = Ex_O.intCurrencyId AND date(trn_orderheader.dtDate) = Ex_O.dtmDate AND mst_locations.intCompanyId = Ex_O.intCompanyId
				LEFT JOIN mst_plant on mst_locations.intPlant = mst_plant.intPlantId
				WHERE  
				trn_orderheader.intCustomer = '$customerId'		 AND
				month(ware_stocktransactions_fabric.dtDate) 	= '$month' AND
				year(ware_stocktransactions_fabric.dtDate) 	= '$year' AND
				ware_stocktransactions_fabric.strType 		= 'Dispatched_G'  AND 
				mst_plant.MAIN_CLUSTER_ID	='$cluster'
				)
				 as tb1 ";
			return $sql;
	}
	
 	private function load_old_customer_revenue_sql($customerId,$month,$year,$toCurrency,$cluster){
		
 	$sql = "SELECT
			daily_mails_customer_revenue_old.REVENUE,
			daily_mails_customer_revenue_old.RM_COST
			FROM `daily_mails_customer_revenue_old`
			WHERE
			daily_mails_customer_revenue_old.`YEAR` = '$year' AND
			daily_mails_customer_revenue_old.`MONTH` = '$month' AND
			daily_mails_customer_revenue_old.CUSTOMER_ID = '$customerId' AND
			daily_mails_customer_revenue_old.CLUSTER_ID = '$cluster'  "; 
			return $sql;
	}
	
 }

?>
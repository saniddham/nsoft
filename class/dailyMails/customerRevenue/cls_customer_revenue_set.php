<?php
 
class cls_customer_revenue_set
{
	private $db;
 	
	function __construct($db)
	{
		$this->db = $db;
 	}

//BEGIN - PUBLIC FUNCTIONS {	
	public function set_customer_revenue_old_data($customerId,$plant,$year,$month,$day,$revenue,$cluster)
	{
		
		$sql 	= $this->set_customer_revenue_old_sql($customerId,$plant,$year,$month,$day,$revenue,$cluster);
		$result = $this->db->RunQuery($sql);
 		return $result;
	}
	public function delete_customer_revenue_old_data($customerId,$year,$month,$cluster)
	{
		
		$sql 	= $this->delete_customer_revenue_old_sql($customerId,$year,$month,$cluster);
		$result = $this->db->RunQuery($sql);
 		return $result;
	}
	
	private function delete_customer_revenue_old_sql($customerId,$year,$month,$cluster){
		
		$date		=$year.'-'.sprintf("%02d",$month).'-'.$day;
		$sql = "DELETE FROM `daily_mails_customer_revenue_old`
				WHERE 
				daily_mails_customer_revenue_old.CUSTOMER_ID = '$customerId' AND
				daily_mails_customer_revenue_old.`MONTH` = '$month' AND 
				daily_mails_customer_revenue_old.`YEAR` = '$year' AND 
				daily_mails_customer_revenue_old.`CLUSTER_ID` = '$cluster'   
				";
		return $sql;
	}
	private function set_customer_revenue_old_sql($customerId,$year,$month,$revenue,$RMcost,$cluster){
		
		$date		=$year.'-'.sprintf("%02d",$month).'-'.$day;
		$sql = "INSERT INTO `daily_mails_customer_revenue_old` (CLUSTER_ID,CUSTOMER_ID,`YEAR`,`MONTH`,`REVENUE`,`RM_COST`) 
			VALUES ('$cluster','$customerId','$year','$month','$revenue','$RMcost')";
		return $sql;
	}
	
 }

?>
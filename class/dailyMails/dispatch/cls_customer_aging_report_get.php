<?php
 
include_once $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
include_once $_SESSION['ROOT_PATH']."class/cls_commonErrorHandeling_get.php";
  
$obj_commonErr		= new cls_commonErrorHandeling_get($db);
$obj_common			= new cls_commonFunctions_get($db);
 
class cls_customer_aging_report_get
{
	private $db;
 	
	function __construct($db)
	{
		$this->db 	= $db;
 	}

 	public function get_customers_delivery_rpt_result($payment_term_array,$type)
	{
 		$sql		= $this->load_customers_delivery_rpt_sql($payment_term_array,$type);
 		$result		= $this->db->RunQuery($sql);
 		return $result;
	}
	
 	public function load_customers_bal_to_invoice_result($company)
	{
 		$sql		= $this->load_customers_bal_to_invoice_sql($company);
 		$result		= $this->db->RunQuery($sql);
 		return $result;
	}

 	public function load_customers_bal_to_invoice_none_moving_result($company)
	{
 		$sql		= $this->load_customers_bal_to_invoice_none_moving_result_sql($company);
 		$result		= $this->db->RunQuery($sql);
 		return $result;
	}
	
	public function GetPaymentTerm()
	{
		global $db;
		$i = 0;
		$sql = "SELECT DISTINCT
				   strName
				FROM mst_financepaymentsterms
				WHERE strName <> 0 
				ORDER BY strName";
		$result = $db->RunQuery($sql);
		while($row = mysqli_fetch_array($result))
		{
			$new_array[$i++] = $row["strName"];
		}
			$new_array[$i++] = "More";
		return $new_array;
	}
	
	public function get_customers_dispatched_not_invoiced($payment_term_array,$startDate)
	{
		$sql		= $this->get_customers_dispatched_not_invoiced_sql($payment_term_array,$startDate);
		//echo $sql;
 		$result		= $this->db->RunQuery($sql);
 		return $result;
	}
	
	public function get_customers_delivery_rpt_base_invoice_result($customerId,$company)
	{
 		$sql		= $this->load_customers_delivery_base_invoiced_rpt_sql($customerId,$company);
 		$result		= $this->db->RunQuery($sql);
 		return $result;
	}
	
  
//BEGIN - PRIVATE FUNCTIONS {	
	private function load_customers_delivery_rpt_sql1($payment_term_array)
	{
	       $sql = "SELECT
					TB.CUSTOMER,
					SUM(TB.TOT_ORDER_QTY) 										AS TOT_ORDER_QTY,
					SUM(TB.TOT_DISP_QTY) 										AS TOT_DISP_QTY,
					SUM(TB.CUST_APP_DISP_QTY) 									AS CUST_APP_DISP_QTY,
					ROUND((SUM(TB.TOT_DISP_QTY))/(SUM(TB.TOT_ORDER_QTY))*100) 
																				AS DISP_PROGRESS,
					SUM(TB.DISP_AMOUNT) 										AS DISP_AMOUNT,
					SUM(TB.CUST_APP_DISP_AMOUNT) 								AS CUST_APP_DISP_AMOUNT,
					(SUM(TB.DISP_AMOUNT)-SUM(TB.CUST_APP_DISP_AMOUNT)) 			AS VARIATION ,
					TB.INVOICE_AMOUNT,
					(TB.PAYMENT_RECEIVED_AMOUNT - TB.ADVANCE_SETTLED - TB.CREDIT_NOTE_AMOUNT + TB.DEBIT_NOTE_AMOUNT) AS PAYMENT_RECEIVED_AMOUNT ,
					(SUM(TB.DISP_AMOUNT)-TB.INVOICE_AMOUNT) 					AS BAL_TO_INV,
					DIFF_DATES,
					PAYMENT_TERM,";
					
			$sql .= "
					ROUND(IF((DIFF_DATES >= 0  && DIFF_DATES <= 15 && DIFF_DATES <= PAYMENT_TERM),((SUM(DISP_AMOUNT) - PAYMENT_RECEIVED_AMOUNT - ADVANCE_SETTLED - CREDIT_NOTE_AMOUNT + DEBIT_NOTE_AMOUNT)),2)) AS NOT_EXCEED_TREM_QTY0, 
					ROUND(IF((DIFF_DATES >= 0  && DIFF_DATES <= 15 && DIFF_DATES > PAYMENT_TERM),((SUM(DISP_AMOUNT) - PAYMENT_RECEIVED_AMOUNT - ADVANCE_SETTLED - CREDIT_NOTE_AMOUNT + DEBIT_NOTE_AMOUNT)),2)) AS EXCEED_TREM_QTY1, 
					
					ROUND(IF((DIFF_DATES >= 16  && DIFF_DATES <= 30 && DIFF_DATES <= PAYMENT_TERM),((SUM(DISP_AMOUNT) - PAYMENT_RECEIVED_AMOUNT - ADVANCE_SETTLED - CREDIT_NOTE_AMOUNT + DEBIT_NOTE_AMOUNT)),2)) AS NOT_EXCEED_TREM_QTY1,
					ROUND(IF((DIFF_DATES >= 16  && DIFF_DATES <= 30 && DIFF_DATES > PAYMENT_TERM),((SUM(DISP_AMOUNT) - PAYMENT_RECEIVED_AMOUNT - ADVANCE_SETTLED - CREDIT_NOTE_AMOUNT + DEBIT_NOTE_AMOUNT)),2)) AS EXCEED_TREM_QTY2, 
					
					ROUND(IF((DIFF_DATES >= 31  && DIFF_DATES <= 45 && DIFF_DATES <= PAYMENT_TERM),((SUM(DISP_AMOUNT) - PAYMENT_RECEIVED_AMOUNT - ADVANCE_SETTLED - CREDIT_NOTE_AMOUNT + DEBIT_NOTE_AMOUNT)),2)) AS NOT_EXCEED_TREM_QTY2,
					ROUND(IF((DIFF_DATES >= 31  && DIFF_DATES <= 45 && DIFF_DATES > PAYMENT_TERM),((SUM(DISP_AMOUNT) - PAYMENT_RECEIVED_AMOUNT - ADVANCE_SETTLED - CREDIT_NOTE_AMOUNT + DEBIT_NOTE_AMOUNT)),2)) AS EXCEED_TREM_QTY3,
					
					ROUND(IF((DIFF_DATES >= 46  && DIFF_DATES <= 60 && DIFF_DATES <= PAYMENT_TERM),((SUM(DISP_AMOUNT) - PAYMENT_RECEIVED_AMOUNT - ADVANCE_SETTLED - CREDIT_NOTE_AMOUNT + DEBIT_NOTE_AMOUNT)),0)) AS NOT_EXCEED_TREM_QTY3,
					ROUND(IF((DIFF_DATES >= 46  && DIFF_DATES <= 60 && DIFF_DATES > PAYMENT_TERM),((SUM(DISP_AMOUNT) - PAYMENT_RECEIVED_AMOUNT - ADVANCE_SETTLED - CREDIT_NOTE_AMOUNT + DEBIT_NOTE_AMOUNT)),2)) AS EXCEED_TREM_QTY4, 
					
					ROUND(IF((DIFF_DATES >61),((SUM(DISP_AMOUNT) - PAYMENT_RECEIVED_AMOUNT - ADVANCE_SETTLED - CREDIT_NOTE_AMOUNT + DEBIT_NOTE_AMOUNT)),2)) AS NOT_EXCEED_TREM_QTY4,
					ROUND(IF((DIFF_DATES >61),((SUM(DISP_AMOUNT) - PAYMENT_RECEIVED_AMOUNT - ADVANCE_SETTLED - CREDIT_NOTE_AMOUNT + DEBIT_NOTE_AMOUNT)),2)) AS EXCEED_TREM_QTY5, 
					";
					
			$sql .= "(ROUND((SUM(TB.DISP_AMOUNT)),2) - PAYMENT_RECEIVED_AMOUNT - ADVANCE_SETTLED - CREDIT_NOTE_AMOUNT + DEBIT_NOTE_AMOUNT) 	AS BALANCE_TO_PAYMENT_RECEIVED 
			
			FROM(
					
			SELECT					
					OH.intCustomer 													AS CUSTOMER_ID,
					mst_customer.strName 											AS CUSTOMER,
					OH.intOrderNo 													AS ORDER_NO,
					OH.intOrderYear 												AS ORDER_YEAR,
					OD.intSalesOrderId 												AS SALES_ORDER_ID,
					OD.strSalesOrderNo 												AS SALES_ORDER_NO,
					Sum(OD.intQty) 													AS TOT_ORDER_QTY,					

					(SELECT 
						SUM(FDD.dblGoodQty)
					FROM ware_fabricdispatchdetails FDD 
					INNER JOIN ware_fabricdispatchheader FDH 
						ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo 
						AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
					WHERE FDH.intOrderNo = OH.intOrderNo 
						AND FDH.intOrderYear = OH.intOrderYear 
						AND FDH.intStatus = 1)										AS TOT_DISP_QTY,
					
					(SELECT
						Sum(FCIB.INVOICED_QTY)
					FROM
						finance_customer_invoice_balance FCIB
					WHERE
						FCIB.ORDER_YEAR = OD.intOrderYear AND
						FCIB.ORDER_NO = OD.intOrderNo AND
						FCIB.SALES_ORDER_ID = OD.intSalesOrderId)					AS CUST_APP_DISP_QTY,
					
					(SELECT
						Sum(FCIB.INVOICED_QTY*OD.dblPrice)
					FROM finance_customer_invoice_balance FCIB
					WHERE
						FCIB.ORDER_YEAR = OD.intOrderYear AND
						FCIB.ORDER_NO = OD.intOrderNo AND
						FCIB.SALES_ORDER_ID =OD.intSalesOrderId)					AS CUST_APP_DISP_AMOUNT,
					
					OD.dblPrice 													AS PRICE,
					
					(SELECT 
						SUM(FDD.dblGoodQty * OD.dblPrice)
					FROM ware_fabricdispatchdetails FDD 
					INNER JOIN ware_fabricdispatchheader FDH 
						ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo 
						AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
					WHERE FDH.intOrderNo = OH.intOrderNo 
						AND FDH.intOrderYear = OH.intOrderYear 
						AND FDH.intStatus = 1)										AS DISP_AMOUNT,					
					
			  		FPT.strName       												AS PAYMENT_TERM,
					
				    DATEDIFF(NOW(),(SELECT
						DATE(FDH.dtmdate)
				    FROM ware_fabricdispatchdetails FDD
					INNER JOIN ware_fabricdispatchheader FDH
						ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo
						AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
				    WHERE FDH.intOrderNo = OH.intOrderNo
						AND FDH.intOrderYear = OH.intOrderYear
						AND FDD.intSalesOrderId = OD.intSalesOrderId
						AND FDH.intStatus = 1 
				    ORDER BY 
						DATE(FDH.dtmdate) DESC LIMIT 1)) 							AS DIFF_DATES,
					
					(SELECT
						DATE(FDH.dtmdate)
					FROM ware_fabricdispatchdetails FDD
					INNER JOIN ware_fabricdispatchheader FDH
						ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo
						AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
					WHERE FDH.intOrderNo = OH.intOrderNo
						AND FDH.intOrderYear = OH.intOrderYear
						AND FDD.intSalesOrderId = OD.intSalesOrderId
						AND FDH.intStatus = 1 
					ORDER BY 
						DATE(FDH.dtmdate) DESC LIMIT 1)				 				AS DISPATCH_DATE,
				
					ROUND(COALESCE((SELECT SUM(VALUE) * -1
					FROM finance_customer_transaction FCT
					WHERE FCT.CUSTOMER_ID = OH.intCustomer
						AND FCT.DOCUMENT_TYPE = 'PAYRECEIVE'),0),2)					AS PAYMENT_RECEIVED_AMOUNT,			
					
					ROUND(COALESCE((SELECT SUM(VALUE) * -1
					FROM finance_customer_transaction FCT
					WHERE FCT.CUSTOMER_ID = OH.intCustomer
						AND FCT.INVOICE_NO IS NOT NULL
						AND FCT.INVOICE_YEAR IS NOT NULL
						AND DOCUMENT_TYPE = 'ADVANCE'),0),2)						AS ADVANCE_SETTLED,
					
					ROUND(COALESCE((SELECT SUM(VALUE) * -1
					FROM finance_customer_transaction FCT
					WHERE FCT.CUSTOMER_ID = OH.intCustomer
						AND FCT.DOCUMENT_TYPE = 'CREDIT'),0),2)						AS CREDIT_NOTE_AMOUNT,
					
					ROUND(COALESCE((SELECT SUM(VALUE)
					FROM finance_customer_transaction FCT
					WHERE FCT.CUSTOMER_ID = OH.intCustomer
						AND FCT.DOCUMENT_TYPE = 'DEBIT'),0),2)						AS DEBIT_NOTE_AMOUNT, 
					
					ROUND(COALESCE((SELECT SUM(VALUE) 
					FROM finance_customer_transaction FCT
					WHERE FCT.CUSTOMER_ID = OH.intCustomer
						AND DOCUMENT_TYPE = 'INVOICE'),0),2)						AS INVOICE_AMOUNT 
					
					FROM
					trn_orderheader as OH
					INNER JOIN trn_orderdetails as OD 
						ON OH.intOrderNo = OD.intOrderNo 
						AND OH.intOrderYear = OD.intOrderYear
					INNER JOIN mst_financepaymentsterms FPT 
						ON FPT.intId = OH.intPaymentTerm
					INNER JOIN mst_customer 
						ON OH.intCustomer = mst_customer.intId
					WHERE
						OH.intStatus = 1
						
					GROUP BY
						OH.intCustomer,
						OH.intOrderNo,
						OH.intOrderYear
					HAVING (TOT_DISP_QTY/TOT_ORDER_QTY) >=1
					) AS TB 
					GROUP BY 
						TB.CUSTOMER_ID
					ORDER BY 
						CUSTOMER ASC";
				//echo $sql;
  		return $sql;
 	}
	
	private function load_customers_delivery_rpt_sql($payment_term_array,$type)
	{
		$wSql	= '';
		
		if($type=='global')
			$wSql = " LO.intCompanyId = 5 ";
			
		if($type=='screenLine')
			$wSql = " LO.intCompanyId = 1 ";
		
		if($type=='banga')
			$wSql = " LO.intCompanyId = 9 ";
			
	       $sql = "SELECT 
		   			CUSTOMER AS CUSTOMER,
					CUSTOMER_ID AS CUSTOMER_ID,
					-- IFNULL(SUM(TOT_LCAMOUNT),0)	AS TOT_LCAMOUNT,
					SUM(TOT_ORDER_QTY)			AS TOT_ORDER_QTY,
					SUM(TOT_DISP_QTY)			AS TOT_DISP_QTY,
					SUM(CUST_APP_DISP_QTY)		AS CUST_APP_DISP_QTY,
					ROUND(SUM(TOT_DISP_QTY)/(SUM(TOT_ORDER_QTY))*100) AS DISP_PROGRESS,
					SUM(DISP_AMOUNT)			AS DISP_AMOUNT,
					SUM(CUST_APP_DISP_AMOUNT)	AS CUST_APP_DISP_AMOUNT,
					SUM(VARIATION)				AS VARIATION,
					SUM(INVOICE_AMOUNT)				AS INVOICE_AMOUNT,
					SUM(PAYMENT_RECEIVED_AMOUNT)		AS PAYMENT_RECEIVED_AMOUNT,
					SUM(BAL_TO_INV)				AS BAL_TO_INV,
					SUM(BALANCE_TO_PAYMENT_RECEIVED)	AS BALANCE_TO_PAYMENT_RECEIVED,
					
					ROUND(SUM(IF((DIFF_DATES >= 0  && DIFF_DATES <= 15 && DIFF_DATES <= PAYMENT_TERM),((BALANCE_TO_PAYMENT_RECEIVED)),0))) AS NOT_EXCEED_TREM_QTY0, 
					ROUND(SUM(IF((DIFF_DATES >= 0  && DIFF_DATES <= 15 && DIFF_DATES > PAYMENT_TERM),((BALANCE_TO_PAYMENT_RECEIVED)),0))) AS EXCEED_TREM_QTY1, 
					
					ROUND(SUM(IF((DIFF_DATES >= 16  && DIFF_DATES <= 30 && DIFF_DATES <= PAYMENT_TERM),((BALANCE_TO_PAYMENT_RECEIVED)),0))) AS NOT_EXCEED_TREM_QTY1,
					ROUND(SUM(IF((DIFF_DATES >= 16  && DIFF_DATES <= 30 && DIFF_DATES > PAYMENT_TERM),((BALANCE_TO_PAYMENT_RECEIVED)),0))) AS EXCEED_TREM_QTY2, 
					
					ROUND(SUM(IF((DIFF_DATES >= 31  && DIFF_DATES <= 45 && DIFF_DATES <= PAYMENT_TERM),((BALANCE_TO_PAYMENT_RECEIVED)),0))) AS NOT_EXCEED_TREM_QTY2,
					ROUND(SUM(IF((DIFF_DATES >= 31  && DIFF_DATES <= 45 && DIFF_DATES > PAYMENT_TERM),((BALANCE_TO_PAYMENT_RECEIVED)),0))) AS EXCEED_TREM_QTY3,
					
					ROUND(SUM(IF((DIFF_DATES >= 46  && DIFF_DATES <= 60 && DIFF_DATES <= PAYMENT_TERM),((BALANCE_TO_PAYMENT_RECEIVED)),0))) AS NOT_EXCEED_TREM_QTY3,
					ROUND(SUM(IF((DIFF_DATES >= 46  && DIFF_DATES <= 60 && DIFF_DATES > PAYMENT_TERM),((BALANCE_TO_PAYMENT_RECEIVED)),0))) AS EXCEED_TREM_QTY4, 
					
					ROUND(SUM(IF((DIFF_DATES >=61),((0)),0))) AS NOT_EXCEED_TREM_QTY4,
					ROUND(SUM(IF((DIFF_DATES >=61),((BALANCE_TO_PAYMENT_RECEIVED)),0))) AS EXCEED_TREM_QTY5
					
					FROM
					(SELECT 
					TB.CUSTOMER,
					CUSTOMER_ID,
					-- TB.TOT_LCAMOUNT											AS TOT_LCAMOUNT,
					TB.TOT_ORDER_QTY 										AS TOT_ORDER_QTY,
					TB.TOT_DISP_QTY 										AS TOT_DISP_QTY,
					TB.CUST_APP_DISP_QTY 									AS CUST_APP_DISP_QTY,
					/*ROUND((TB.TOT_DISP_QTY)/(TB.TOT_ORDER_QTY)*100) 
								AS DISP_PROGRESS,*/
					TB.DISP_AMOUNT 										AS DISP_AMOUNT,
					TB.CUST_APP_DISP_AMOUNT 								AS CUST_APP_DISP_AMOUNT,
					(TB.DISP_AMOUNT - TB.CUST_APP_DISP_AMOUNT) 										AS VARIATION ,
					TB.INVOICE_AMOUNT,
					(TB.PAYMENT_RECEIVED_AMOUNT + TB.ADVANCE_SETTLED + TB.CREDIT_NOTE_AMOUNT) AS PAYMENT_RECEIVED_AMOUNT ,
					(TB.DISP_AMOUNT - TB.INVOICE_AMOUNT) AS BAL_TO_INV,
					DIFF_DATES,
					PAYMENT_TERM,
					(ROUND((TB.INVOICE_AMOUNT),2) - (TB.PAYMENT_RECEIVED_AMOUNT + TB.ADVANCE_SETTLED + TB.CREDIT_NOTE_AMOUNT )) AS BALANCE_TO_PAYMENT_RECEIVED 
					FROM (
					
					SELECT					
					OH.intCustomer 													AS CUSTOMER_ID,
					mst_customer.strName 												AS CUSTOMER,
					OH.intOrderNo 													AS ORDER_NO,
					OH.intOrderYear 												AS ORDER_YEAR,
					OD.intSalesOrderId 												AS SALES_ORDER_ID,
					OD.strSalesOrderNo 												AS SALES_ORDER_NO,
					SUM(OD.intQty) 													AS TOT_ORDER_QTY,					
					
					(SELECT 
					SUM(FDD.dblGoodQty)
					FROM ware_fabricdispatchdetails FDD 
					INNER JOIN ware_fabricdispatchheader FDH 
					ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo 
					AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
					WHERE FDH.intOrderNo = OH.intOrderNo 
					AND FDH.intOrderYear = OH.intOrderYear 
					AND FDH.intStatus = 1)										AS TOT_DISP_QTY,
					
					/*(SELECT SUM(LC_AMOUNT) AS totLCAmount
					FROM trn_lc_header LC
					WHERE LC.ORDER_NO = OH.intOrderNo
					AND LC.ORDER_YEAR = OH.intOrderYear 
					AND LC.STATUS = 1
					) 															AS TOT_LCAMOUNT,*/
					
					(SELECT
					SUM(FCIB.INVOICED_QTY)
					FROM
					finance_customer_invoice_balance FCIB
					WHERE
					FCIB.ORDER_YEAR = OD.intOrderYear AND
					FCIB.ORDER_NO = OD.intOrderNo )						AS CUST_APP_DISP_QTY,
					
					ROUND(COALESCE((SELECT
                	SUM(FCIB.INVOICED_QTY*OD1.dblPrice)
              		FROM finance_customer_invoice_balance FCIB 
					INNER JOIN trn_orderdetails AS OD1 
					ON FCIB.ORDER_NO = OD1.intOrderNo
					AND FCIB.ORDER_YEAR = OD1.intOrderYear 
					AND FCIB.SALES_ORDER_ID = OD1.intSalesOrderId
            		WHERE FCIB.ORDER_YEAR = OD.intOrderYear
                   	AND FCIB.ORDER_NO = OD.intOrderNo),0),2) 			AS CUST_APP_DISP_AMOUNT,
					
					OD.dblPrice 													AS PRICE,
					
					ROUND(COALESCE((SELECT 
					SUM(FDD.dblGoodQty * OD2.dblPrice) 
					FROM ware_fabricdispatchdetails FDD 
					INNER JOIN ware_fabricdispatchheader FDH 
					ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo 
					AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
					INNER JOIN trn_orderdetails AS OD2 
					ON FDH.intOrderNo = OD2.intOrderNo 
					AND FDH.intOrderYear = OD2.intOrderYear 
					AND FDD.intSalesOrderId = OD2.intSalesOrderId
					WHERE FDH.intOrderNo = OH.intOrderNo 
					AND FDH.intOrderYear = OH.intOrderYear 
					AND FDH.intStatus = 1),0),2)									AS DISP_AMOUNT,					
					
					FPT.strName       												AS PAYMENT_TERM,
					
					DATEDIFF(NOW(),(SELECT
					DATE(FDH.dtmdate)
					FROM ware_fabricdispatchdetails FDD
					INNER JOIN ware_fabricdispatchheader FDH
					ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo
					AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
					WHERE FDH.intOrderNo = OH.intOrderNo
					AND FDH.intOrderYear = OH.intOrderYear
					AND FDD.intSalesOrderId = OD.intSalesOrderId
					AND FDH.intStatus = 1 
					ORDER BY 
					DATE(FDH.dtmdate) DESC LIMIT 1)) 							AS DIFF_DATES,
					
					(SELECT
					DATE(FDH.dtmdate)
					FROM ware_fabricdispatchdetails FDD
					INNER JOIN ware_fabricdispatchheader FDH
					ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo
					AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
					WHERE FDH.intOrderNo = OH.intOrderNo
					AND FDH.intOrderYear = OH.intOrderYear
					AND FDD.intSalesOrderId = OD.intSalesOrderId
					AND FDH.intStatus = 1 
					ORDER BY 
					DATE(FDH.dtmdate) DESC LIMIT 1)				 				AS DISPATCH_DATE,
					
					ROUND(COALESCE((SELECT SUM(VALUE) * -1
					FROM finance_customer_transaction FCT
					WHERE FCT.ORDER_NO = OH.intOrderNo
						AND FCT.ORDER_YEAR = OH.intOrderYear
					AND FCT.DOCUMENT_TYPE = 'PAYRECEIVE'),0),2)					AS PAYMENT_RECEIVED_AMOUNT,			
					
					ROUND(COALESCE((SELECT SUM(VALUE) * -1
					FROM finance_customer_transaction FCT
					WHERE FCT.ORDER_NO = OH.intOrderNo
						AND FCT.ORDER_YEAR = OH.intOrderYear
					AND FCT.INVOICE_NO IS NOT NULL
					AND FCT.INVOICE_YEAR IS NOT NULL
					AND DOCUMENT_TYPE IN('INV_SETTLEMENT','ADVANCE')),0),2)                        AS ADVANCE_SETTLED,
					
					ROUND(COALESCE((SELECT SUM(VALUE) * -1
					FROM finance_customer_transaction FCT
					WHERE FCT.ORDER_NO = OH.intOrderNo
						AND FCT.ORDER_YEAR = OH.intOrderYear
					AND FCT.DOCUMENT_TYPE = 'CREDIT'),0),2)						AS CREDIT_NOTE_AMOUNT,
					
					/*ROUND(COALESCE((SELECT SUM(VALUE)
					FROM finance_customer_transaction FCT
					WHERE FCT.CUSTOMER_ID = OH.intCustomer
					AND FCT.DOCUMENT_TYPE = 'DEBIT'),0),2)						AS DEBIT_NOTE_AMOUNT, */
					
					ROUND(COALESCE((SELECT SUM(VALUE) 
					FROM finance_customer_transaction FCT
					WHERE FCT.ORDER_NO = OH.intOrderNo
						AND FCT.ORDER_YEAR = OH.intOrderYear
					AND DOCUMENT_TYPE = 'INVOICE'),0),2)						AS INVOICE_AMOUNT 
					
					FROM trn_orderheader AS OH
					INNER JOIN trn_orderdetails AS OD 
						ON OH.intOrderNo = OD.intOrderNo 
						AND OH.intOrderYear = OD.intOrderYear
					INNER JOIN mst_financepaymentsterms FPT 
						ON FPT.intId = OH.intPaymentTerm
					INNER JOIN mst_customer 
						ON OH.intCustomer = mst_customer.intId
					INNER JOIN mst_locations LO
						ON LO.intId = OH.intLocationId
					WHERE
						-- OH.intStatus = 1
						-- AND OH.PAYMENT_RECEIVE_COMPLETED_FLAG = 0
						$wSql
						-- AND OH.intCustomer = 60
					GROUP BY
					OH.intCustomer,
					OH.intOrderNo,
					OH.intOrderYear
					HAVING((TOT_DISP_QTY/TOT_ORDER_QTY)>=1 OR INVOICE_AMOUNT>0)
					) AS TB
					GROUP BY ORDER_NO,
					ORDER_YEAR
					HAVING(BALANCE_TO_PAYMENT_RECEIVED<>0)
					) AS TB1
					GROUP BY CUSTOMER_ID
					ORDER BY CUSTOMER";
  		return $sql;
 	}
	
 
	private function load_customers_bal_to_invoice_sql($company)
	{
		$wSql	= '';
		
 			$wSql = "AND LO.intCompanyId = $company ";
 			
	       $sql = "SELECT 
		   			CUSTOMER AS CUSTOMER,
					ORDER_NO, 
					ORDER_YEAR, 
					CUSTOMER_PO, 
					TOT_ORDER_QTY,
					SUM(INVOICE_AMOUNT)					AS INVOICE_AMOUNT,
					SUM(PAYMENT_RECEIVED_AMOUNT)		AS PAYMENT_RECEIVED_AMOUNT,
					SUM(BAL_TO_INV)						AS BAL_TO_INV,
					SUM(BALANCE_TO_PAYMENT_RECEIVED)	AS BALANCE_TO_PAYMENT_RECEIVED,
					DISPATCH_DATE, 
					DIFF_DATES 
					
					FROM
					(SELECT 
					TB.ORDER_NO,
					TB.ORDER_YEAR,
					TB.CUSTOMER_PO,
					TB.CUSTOMER,
					CUSTOMER_ID,
					TB.TOT_ORDER_QTY 										AS TOT_ORDER_QTY,
					TB.TOT_DISP_QTY 										AS TOT_DISP_QTY,
					TB.TOT_ALL_TYPE_DISP_QTY								AS TOT_ALL_TYPE_DISP_QTY, 
					TB.CUST_APP_DISP_QTY 									AS CUST_APP_DISP_QTY,
					TB.DISP_AMOUNT 											AS DISP_AMOUNT,
					TB.CUST_APP_DISP_AMOUNT 								AS CUST_APP_DISP_AMOUNT,
					TB.INVOICE_AMOUNT,
					(TB.PAYMENT_RECEIVED_AMOUNT + TB.ADVANCE_SETTLED + TB.CREDIT_NOTE_AMOUNT - TB.DEBIT_NOTE_AMOUNT) 			AS PAYMENT_RECEIVED_AMOUNT ,
					(TB.DISP_AMOUNT - TB.INVOICE_AMOUNT) 					AS BAL_TO_INV,
					(ROUND((TB.DISP_AMOUNT),2) - (TB.PAYMENT_RECEIVED_AMOUNT + TB.ADVANCE_SETTLED + TB.CREDIT_NOTE_AMOUNT - TB.DEBIT_NOTE_AMOUNT)) 	AS BALANCE_TO_PAYMENT_RECEIVED ,
					DISPATCH_DATE, 
					DIFF_DATES 
					FROM (
					
					SELECT					
					OH.intCustomer 													AS CUSTOMER_ID,
					mst_customer.strName 												AS CUSTOMER,
					OH.intOrderNo 													AS ORDER_NO,
					OH.intOrderYear 												AS ORDER_YEAR,
					OH.strCustomerPoNo 												AS CUSTOMER_PO,
					OD.intSalesOrderId 												AS SALES_ORDER_ID,
					OD.strSalesOrderNo 												AS SALES_ORDER_NO,
					SUM(OD.intQty) 													AS TOT_ORDER_QTY,					
					
					(SELECT 
					SUM(FDD.dblGoodQty)
					FROM ware_fabricdispatchdetails FDD 
					INNER JOIN ware_fabricdispatchheader FDH 
					ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo 
					AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
					WHERE FDH.intOrderNo = OH.intOrderNo 
					AND FDH.intOrderYear = OH.intOrderYear 
					AND FDH.intStatus = 1)										AS TOT_DISP_QTY,
					
					(SELECT 
					SUM(FDD.dblSampleQty+FDD.dblGoodQty+FDD.dblEmbroideryQty+FDD.dblPDammageQty+FDD.dblFdammageQty+FDD.dblCutRetQty)
					FROM ware_fabricdispatchdetails FDD 
					INNER JOIN ware_fabricdispatchheader FDH 
					ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo 
					AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
					WHERE FDH.intOrderNo = OH.intOrderNo 
					AND FDH.intOrderYear = OH.intOrderYear 
					AND FDH.intStatus = 1)										AS TOT_ALL_TYPE_DISP_QTY,
					
					
					
					(SELECT
					SUM(FCIB.INVOICED_QTY)
					FROM
					finance_customer_invoice_balance FCIB
					WHERE
					FCIB.ORDER_YEAR = OD.intOrderYear AND
					FCIB.ORDER_NO = OD.intOrderNo AND
					FCIB.SALES_ORDER_ID = OD.intSalesOrderId)						AS CUST_APP_DISP_QTY,
					
					ROUND(COALESCE((SELECT
                	SUM(FCIB.INVOICED_QTY*OD.dblPrice)
              		FROM finance_customer_invoice_balance FCIB
            		WHERE FCIB.ORDER_YEAR = OD.intOrderYear
                   	AND FCIB.ORDER_NO = OD.intOrderNo
                   	AND FCIB.SALES_ORDER_ID = OD.intSalesOrderId),0),2) 			AS CUST_APP_DISP_AMOUNT,
					
					OD.dblPrice 													AS PRICE,
					
					ROUND(COALESCE((SELECT 
					SUM(FDD.dblGoodQty * ROUND(SUM(OD.intQty * OD.dblPrice)/SUM(OD.intQty),4))
					FROM ware_fabricdispatchdetails FDD 
					INNER JOIN ware_fabricdispatchheader FDH 
					ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo 
					AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
					WHERE FDH.intOrderNo = OH.intOrderNo 
					AND FDH.intOrderYear = OH.intOrderYear 
					AND FDH.intStatus = 1),0),2)									AS DISP_AMOUNT,					
					
					
					DATEDIFF(NOW(),(SELECT
					DATE(FDH.dtmdate)
					FROM ware_fabricdispatchdetails FDD
					INNER JOIN ware_fabricdispatchheader FDH
					ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo
					AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
					WHERE FDH.intOrderNo = OH.intOrderNo
					AND FDH.intOrderYear = OH.intOrderYear
					AND FDD.intSalesOrderId = OD.intSalesOrderId
					AND FDH.intStatus = 1 
					ORDER BY 
					DATE(FDH.dtmdate) DESC LIMIT 1)) 							AS DIFF_DATES,
					
					(SELECT
					DATE(FDH.dtmdate)
					FROM ware_fabricdispatchdetails FDD
					INNER JOIN ware_fabricdispatchheader FDH
					ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo
					AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
					WHERE FDH.intOrderNo = OH.intOrderNo
					AND FDH.intOrderYear = OH.intOrderYear
					AND FDD.intSalesOrderId = OD.intSalesOrderId
					AND FDH.intStatus = 1 
					ORDER BY 
					DATE(FDH.dtmdate) DESC LIMIT 1)				 				AS DISPATCH_DATE,
					
					ROUND(COALESCE((SELECT SUM(VALUE) * -1
					FROM finance_customer_transaction FCT
					WHERE FCT.ORDER_NO = OH.intOrderNo
						AND FCT.ORDER_YEAR = OH.intOrderYear
					AND FCT.DOCUMENT_TYPE = 'PAYRECEIVE'),0),2)					AS PAYMENT_RECEIVED_AMOUNT,			
					
					ROUND(COALESCE((SELECT SUM(VALUE) * -1
					FROM finance_customer_transaction FCT
					WHERE FCT.ORDER_NO = OH.intOrderNo
						AND FCT.ORDER_YEAR = OH.intOrderYear
					AND FCT.INVOICE_NO IS NOT NULL
					AND FCT.INVOICE_YEAR IS NOT NULL
					AND DOCUMENT_TYPE = 'ADVANCE'),0),2)						AS ADVANCE_SETTLED,
					
					ROUND(COALESCE((SELECT SUM(VALUE) * -1
					FROM finance_customer_transaction FCT
					WHERE FCT.ORDER_NO = OH.intOrderNo
						AND FCT.ORDER_YEAR = OH.intOrderYear
					AND FCT.DOCUMENT_TYPE = 'CREDIT'),0),2)						AS CREDIT_NOTE_AMOUNT,
					
					ROUND(COALESCE((SELECT SUM(VALUE)
					FROM finance_customer_transaction FCT
					WHERE FCT.CUSTOMER_ID = OH.intCustomer
					AND FCT.DOCUMENT_TYPE = 'DEBIT_1'),0),2)						AS DEBIT_NOTE_AMOUNT, 
					
					ROUND(COALESCE((SELECT SUM(VALUE) 
					FROM finance_customer_transaction FCT
					WHERE FCT.ORDER_NO = OH.intOrderNo
						AND FCT.ORDER_YEAR = OH.intOrderYear
					AND DOCUMENT_TYPE = 'INVOICE'),0),2)						AS INVOICE_AMOUNT 
					
					FROM trn_orderheader AS OH
					INNER JOIN trn_orderdetails AS OD 
						ON OH.intOrderNo = OD.intOrderNo 
						AND OH.intOrderYear = OD.intOrderYear
					INNER JOIN mst_financepaymentsterms FPT 
						ON FPT.intId = OH.intPaymentTerm
					INNER JOIN mst_customer 
						ON OH.intCustomer = mst_customer.intId
					INNER JOIN mst_locations LO
						ON LO.intId = OH.intLocationId
					WHERE
						(OH.intStatus <> -2 AND OH.intStatus <> -10) 
						AND OH.PAYMENT_RECEIVE_COMPLETED_FLAG = 0
						$wSql
					GROUP BY
					OH.intCustomer,
					OH.intOrderNo,
					OH.intOrderYear
					HAVING TOT_ORDER_QTY <= TOT_ALL_TYPE_DISP_QTY  
 					) AS TB
					GROUP BY ORDER_NO,
					ORDER_YEAR) AS TB1
					GROUP BY CUSTOMER_ID,
					ORDER_NO,
					ORDER_YEAR  
					HAVING  BAL_TO_INV > 0  
 					ORDER BY CUSTOMER ASC , ORDER_NO ASC, ORDER_YEAR ASC";
					
  		return $sql;
 	}
	
	
	private function load_customers_bal_to_invoice_none_moving_result_sql($company)
	{
		$wSql	= '';
		
 			$wSql = "AND LO.intCompanyId = $company ";
 			
	       $sql = "SELECT 
		   			CUSTOMER AS CUSTOMER,
					ORDER_NO, 
					ORDER_YEAR, 
					CUSTOMER_PO, 
					TOT_ORDER_QTY,
					SUM(INVOICE_AMOUNT)					AS INVOICE_AMOUNT,
					SUM(PAYMENT_RECEIVED_AMOUNT)		AS PAYMENT_RECEIVED_AMOUNT,
					SUM(BAL_TO_INV)						AS BAL_TO_INV,
					SUM(BALANCE_TO_PAYMENT_RECEIVED)	AS BALANCE_TO_PAYMENT_RECEIVED,
					DISPATCH_DATE, 
					DIFF_DATES 
					
					FROM
					(SELECT 
					TB.ORDER_NO,
					TB.ORDER_YEAR,
					TB.CUSTOMER_PO,
					TB.CUSTOMER,
					CUSTOMER_ID,
					TB.TOT_ORDER_QTY 										AS TOT_ORDER_QTY,
					TB.TOT_DISP_QTY 										AS TOT_DISP_QTY,
					TB.TOT_ALL_TYPE_DISP_QTY								AS TOT_ALL_TYPE_DISP_QTY, 
					TB.CUST_APP_DISP_QTY 									AS CUST_APP_DISP_QTY,
					TB.DISP_AMOUNT 											AS DISP_AMOUNT,
					TB.CUST_APP_DISP_AMOUNT 								AS CUST_APP_DISP_AMOUNT,
					TB.INVOICE_AMOUNT,
					(TB.PAYMENT_RECEIVED_AMOUNT + TB.ADVANCE_SETTLED - TB.CREDIT_NOTE_AMOUNT + TB.DEBIT_NOTE_AMOUNT) 			AS PAYMENT_RECEIVED_AMOUNT ,
					(TB.DISP_AMOUNT - TB.INVOICE_AMOUNT) 					AS BAL_TO_INV,
					(ROUND((TB.DISP_AMOUNT),2) - PAYMENT_RECEIVED_AMOUNT - ADVANCE_SETTLED - CREDIT_NOTE_AMOUNT + DEBIT_NOTE_AMOUNT) 	AS BALANCE_TO_PAYMENT_RECEIVED ,
					DISPATCH_DATE, 
					DIFF_DATES 
					FROM (
					
					SELECT					
					OH.intCustomer 													AS CUSTOMER_ID,
					mst_customer.strName 												AS CUSTOMER,
					OH.intOrderNo 													AS ORDER_NO,
					OH.intOrderYear 												AS ORDER_YEAR,
					OH.strCustomerPoNo 												AS CUSTOMER_PO,
					OD.intSalesOrderId 												AS SALES_ORDER_ID,
					OD.strSalesOrderNo 												AS SALES_ORDER_NO,
					SUM(OD.intQty) 													AS TOT_ORDER_QTY,					
					
					(SELECT 
					SUM(FDD.dblGoodQty)
					FROM ware_fabricdispatchdetails FDD 
					INNER JOIN ware_fabricdispatchheader FDH 
					ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo 
					AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
					WHERE FDH.intOrderNo = OH.intOrderNo 
					AND FDH.intOrderYear = OH.intOrderYear 
					AND FDH.intStatus = 1)										AS TOT_DISP_QTY,
					
					(SELECT 
					SUM(FDD.dblSampleQty+FDD.dblGoodQty+FDD.dblEmbroideryQty+FDD.dblPDammageQty+FDD.dblFdammageQty+FDD.dblCutRetQty)
					FROM ware_fabricdispatchdetails FDD 
					INNER JOIN ware_fabricdispatchheader FDH 
					ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo 
					AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
					WHERE FDH.intOrderNo = OH.intOrderNo 
					AND FDH.intOrderYear = OH.intOrderYear 
					AND FDH.intStatus = 1)										AS TOT_ALL_TYPE_DISP_QTY,
					
					
					
					(SELECT
					SUM(FCIB.INVOICED_QTY)
					FROM
					finance_customer_invoice_balance FCIB
					WHERE
					FCIB.ORDER_YEAR = OD.intOrderYear AND
					FCIB.ORDER_NO = OD.intOrderNo AND
					FCIB.SALES_ORDER_ID = OD.intSalesOrderId)						AS CUST_APP_DISP_QTY,
					
					ROUND(COALESCE((SELECT
                	SUM(FCIB.INVOICED_QTY*OD.dblPrice)
              		FROM finance_customer_invoice_balance FCIB
            		WHERE FCIB.ORDER_YEAR = OD.intOrderYear
                   	AND FCIB.ORDER_NO = OD.intOrderNo
                   	AND FCIB.SALES_ORDER_ID = OD.intSalesOrderId),0),2) 			AS CUST_APP_DISP_AMOUNT,
					
					OD.dblPrice 													AS PRICE,
					
					ROUND(COALESCE((SELECT 
					SUM(FDD.dblGoodQty * ROUND(SUM(OD.intQty * OD.dblPrice)/SUM(OD.intQty),4))
					FROM ware_fabricdispatchdetails FDD 
					INNER JOIN ware_fabricdispatchheader FDH 
					ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo 
					AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
					WHERE FDH.intOrderNo = OH.intOrderNo 
					AND FDH.intOrderYear = OH.intOrderYear 
					AND FDH.intStatus = 1),0),2)									AS DISP_AMOUNT,					
					
					
					DATEDIFF(NOW(),(SELECT
					DATE(FDH.dtmdate)
					FROM ware_fabricdispatchdetails FDD
					INNER JOIN ware_fabricdispatchheader FDH
					ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo
					AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
					WHERE FDH.intOrderNo = OH.intOrderNo
					AND FDH.intOrderYear = OH.intOrderYear
					AND FDD.intSalesOrderId = OD.intSalesOrderId
					AND FDH.intStatus = 1 
					ORDER BY 
					DATE(FDH.dtmdate) DESC LIMIT 1)) 							AS DIFF_DATES,
					
					(SELECT
					DATE(FDH.dtmdate)
					FROM ware_fabricdispatchdetails FDD
					INNER JOIN ware_fabricdispatchheader FDH
					ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo
					AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
					WHERE FDH.intOrderNo = OH.intOrderNo
					AND FDH.intOrderYear = OH.intOrderYear
					AND FDD.intSalesOrderId = OD.intSalesOrderId
					AND FDH.intStatus = 1 
					ORDER BY 
					DATE(FDH.dtmdate) DESC LIMIT 1)				 				AS DISPATCH_DATE,
					
					ROUND(COALESCE((SELECT SUM(VALUE) * -1
					FROM finance_customer_transaction FCT
					WHERE FCT.ORDER_NO = OH.intOrderNo
						AND FCT.ORDER_YEAR = OH.intOrderYear
					AND FCT.DOCUMENT_TYPE = 'PAYRECEIVE'),0),2)					AS PAYMENT_RECEIVED_AMOUNT,			
					
					ROUND(COALESCE((SELECT SUM(VALUE) * -1
					FROM finance_customer_transaction FCT
					WHERE FCT.ORDER_NO = OH.intOrderNo
						AND FCT.ORDER_YEAR = OH.intOrderYear
					AND FCT.INVOICE_NO IS NOT NULL
					AND FCT.INVOICE_YEAR IS NOT NULL
					AND DOCUMENT_TYPE = 'ADVANCE'),0),2)						AS ADVANCE_SETTLED,
					
					ROUND(COALESCE((SELECT SUM(VALUE) * -1
					FROM finance_customer_transaction FCT
					WHERE FCT.ORDER_NO = OH.intOrderNo
						AND FCT.ORDER_YEAR = OH.intOrderYear
					AND FCT.DOCUMENT_TYPE = 'CREDIT'),0),2)						AS CREDIT_NOTE_AMOUNT,
					
					ROUND(COALESCE((SELECT SUM(VALUE)
					FROM finance_customer_transaction FCT
					WHERE FCT.CUSTOMER_ID = OH.intCustomer
					AND FCT.DOCUMENT_TYPE = 'DEBIT_1'),0),2)						AS DEBIT_NOTE_AMOUNT, 
					
					ROUND(COALESCE((SELECT SUM(VALUE) 
					FROM finance_customer_transaction FCT
					WHERE FCT.ORDER_NO = OH.intOrderNo
						AND FCT.ORDER_YEAR = OH.intOrderYear
					AND DOCUMENT_TYPE = 'INVOICE'),0),2)						AS INVOICE_AMOUNT 
					
					FROM trn_orderheader AS OH
					INNER JOIN trn_orderdetails AS OD 
						ON OH.intOrderNo = OD.intOrderNo 
						AND OH.intOrderYear = OD.intOrderYear
					INNER JOIN mst_financepaymentsterms FPT 
						ON FPT.intId = OH.intPaymentTerm
					INNER JOIN mst_customer 
						ON OH.intCustomer = mst_customer.intId
					INNER JOIN mst_locations LO
						ON LO.intId = OH.intLocationId
					WHERE
						(OH.intStatus <> -2 AND OH.intStatus <> -10) 
						AND OH.PAYMENT_RECEIVE_COMPLETED_FLAG = 0
						$wSql
					GROUP BY
					OH.intCustomer,
					OH.intOrderNo,
					OH.intOrderYear
					HAVING TOT_ORDER_QTY > TOT_ALL_TYPE_DISP_QTY  
 					) AS TB
					GROUP BY ORDER_NO,
					ORDER_YEAR) AS TB1
					GROUP BY CUSTOMER_ID,
					ORDER_NO,
					ORDER_YEAR  
					HAVING  BAL_TO_INV > 0  
					AND DIFF_DATES >30 
 					ORDER BY CUSTOMER ASC , ORDER_NO ASC, ORDER_YEAR ASC";
					
  		return $sql;
 	}
	private function get_customers_dispatched_not_invoiced_sql($payment_term_array,$startDate)
	{
		
		$sql = "SELECT 
		   			CUSTOMER AS CUSTOMER,
					ORDER_NO, 
					ORDER_YEAR,
					-- IFNULL(SUM(TOT_LCAMOUNT),0)	AS TOT_LCAMOUNT,
					SUM(TOT_ORDER_QTY)			AS TOT_ORDER_QTY,
					SUM(TOT_DISP_QTY)			AS TOT_DISP_QTY,
					SUM(CUST_APP_DISP_QTY)		AS CUST_APP_DISP_QTY,
					ROUND(SUM(TOT_DISP_QTY)/(SUM(TOT_ORDER_QTY))*100) AS DISP_PROGRESS,
					SUM(DISP_AMOUNT)			AS DISP_AMOUNT,
					SUM(CUST_APP_DISP_AMOUNT)	AS CUST_APP_DISP_AMOUNT,
					SUM(VARIATION)				AS VARIATION,
					SUM(INVOICE_AMOUNT)				AS INVOICE_AMOUNT,
					SUM(PAYMENT_RECEIVED_AMOUNT)		AS PAYMENT_RECEIVED_AMOUNT,
					SUM(BAL_TO_INV)				AS BAL_TO_INV,
					SUM(BALANCE_TO_PAYMENT_RECEIVED)	AS BALANCE_TO_PAYMENT_RECEIVED,
					DISP_PLANTS					AS DISP_PLANTS
					
					/*ROUND(SUM(IF((DIFF_DATES >= 0  && DIFF_DATES <= 15 && DIFF_DATES <= PAYMENT_TERM),((BALANCE_TO_PAYMENT_RECEIVED)),0))) AS NOT_EXCEED_TREM_QTY0, 
					ROUND(SUM(IF((DIFF_DATES >= 0  && DIFF_DATES <= 15 && DIFF_DATES > PAYMENT_TERM),((BALANCE_TO_PAYMENT_RECEIVED)),0))) AS EXCEED_TREM_QTY1, 
					
					ROUND(SUM(IF((DIFF_DATES >= 16  && DIFF_DATES <= 30 && DIFF_DATES <= PAYMENT_TERM),((BALANCE_TO_PAYMENT_RECEIVED)),0))) AS NOT_EXCEED_TREM_QTY1,
					ROUND(SUM(IF((DIFF_DATES >= 16  && DIFF_DATES <= 30 && DIFF_DATES > PAYMENT_TERM),((BALANCE_TO_PAYMENT_RECEIVED)),0))) AS EXCEED_TREM_QTY2, 
					
					ROUND(SUM(IF((DIFF_DATES >= 31  && DIFF_DATES <= 45 && DIFF_DATES <= PAYMENT_TERM),((BALANCE_TO_PAYMENT_RECEIVED)),0))) AS NOT_EXCEED_TREM_QTY2,
					ROUND(SUM(IF((DIFF_DATES >= 31  && DIFF_DATES <= 45 && DIFF_DATES > PAYMENT_TERM),((BALANCE_TO_PAYMENT_RECEIVED)),0))) AS EXCEED_TREM_QTY3,
					
					ROUND(SUM(IF((DIFF_DATES >= 46  && DIFF_DATES <= 60 && DIFF_DATES <= PAYMENT_TERM),((BALANCE_TO_PAYMENT_RECEIVED)),0))) AS NOT_EXCEED_TREM_QTY3,
					ROUND(SUM(IF((DIFF_DATES >= 46  && DIFF_DATES <= 60 && DIFF_DATES > PAYMENT_TERM),((BALANCE_TO_PAYMENT_RECEIVED)),0))) AS EXCEED_TREM_QTY4, 
					
					ROUND(SUM(IF((DIFF_DATES >=61),((0)),0))) AS NOT_EXCEED_TREM_QTY4,
					ROUND(SUM(IF((DIFF_DATES >=61),((BALANCE_TO_PAYMENT_RECEIVED)),0))) AS EXCEED_TREM_QTY5*/
					
					FROM
					(SELECT 
					TB.CUSTOMER,
					CUSTOMER_ID,
					-- TB.TOT_LCAMOUNT											AS TOT_LCAMOUNT,
					TB.TOT_ORDER_QTY 										AS TOT_ORDER_QTY,
					TB.TOT_DISP_QTY 										AS TOT_DISP_QTY,
					TB.CUST_APP_DISP_QTY 									AS CUST_APP_DISP_QTY,
					/*ROUND((TB.TOT_DISP_QTY)/(TB.TOT_ORDER_QTY)*100) 
								AS DISP_PROGRESS,*/
					TB.DISP_AMOUNT 										AS DISP_AMOUNT,
					TB.CUST_APP_DISP_AMOUNT 								AS CUST_APP_DISP_AMOUNT,
					(TB.DISP_AMOUNT - TB.CUST_APP_DISP_AMOUNT) 										AS VARIATION ,
					TB.INVOICE_AMOUNT,
					(TB.PAYMENT_RECEIVED_AMOUNT + TB.ADVANCE_SETTLED + TB.CREDIT_NOTE_AMOUNT) AS PAYMENT_RECEIVED_AMOUNT ,
					(TB.DISP_AMOUNT - TB.INVOICE_AMOUNT) 											AS BAL_TO_INV,
					DIFF_DATES,
					ORDER_NO, 
					ORDER_YEAR,
					PAYMENT_TERM,
					(ROUND((TB.INVOICE_AMOUNT),2) - (TB.PAYMENT_RECEIVED_AMOUNT + TB.ADVANCE_SETTLED + TB.CREDIT_NOTE_AMOUNT)) 	AS BALANCE_TO_PAYMENT_RECEIVED ,
					TB.DISP_PLANTS				AS DISP_PLANTS
					FROM (
					
					SELECT					
					OH.intCustomer 													AS CUSTOMER_ID,
					mst_customer.strName 												AS CUSTOMER,
					OH.intOrderNo 													AS ORDER_NO,
					OH.dtDate														AS ORDER_DATE,
					OH.intOrderYear 												AS ORDER_YEAR,
					OD.intSalesOrderId 												AS SALES_ORDER_ID,
					OD.strSalesOrderNo 												AS SALES_ORDER_NO,
					SUM(OD.intQty) 													AS TOT_ORDER_QTY,					
					
					(SELECT 
					SUM(FDD.dblGoodQty)
					FROM ware_fabricdispatchdetails FDD 
					INNER JOIN ware_fabricdispatchheader FDH 
					ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo 
					AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
					WHERE FDH.intOrderNo = OH.intOrderNo 
					AND FDH.intOrderYear = OH.intOrderYear 
					AND FDH.intStatus = 1)										AS TOT_DISP_QTY,
					
					/*(SELECT SUM(LC_AMOUNT) AS totLCAmount
					FROM trn_lc_header LC
					WHERE LC.ORDER_NO = OH.intOrderNo
					AND LC.ORDER_YEAR = OH.intOrderYear 
					AND LC.STATUS = 1
					) AS TOT_LCAMOUNT,*/
					
					 ( SELECT
					GROUP_CONCAT(DISTINCT(mst_plant.strPlantName)) as plants
					FROM
					ware_fabricdispatchheader as FDH
					INNER JOIN ware_fabricdispatchdetails AS FDD ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
					INNER JOIN mst_locations ON FDH.intCompanyId = mst_locations.intId
					INNER JOIN mst_plant ON mst_locations.intPlant = mst_plant.intPlantId 
					WHERE FDH.intOrderNo = OH.intOrderNo
				   AND FDH.intOrderYear = OH.intOrderYear
				   AND FDH.intStatus = 1 ) AS DISP_PLANTS 	,														
					
					(SELECT
					SUM(FCIB.INVOICED_QTY)
					FROM
					finance_customer_invoice_balance FCIB
					WHERE
					FCIB.ORDER_YEAR = OD.intOrderYear AND
					FCIB.ORDER_NO = OD.intOrderNo )						AS CUST_APP_DISP_QTY,
					
					ROUND(COALESCE((SELECT
                	SUM(FCIB.INVOICED_QTY*OD1.dblPrice)
              		FROM finance_customer_invoice_balance FCIB 
					INNER JOIN trn_orderdetails AS OD1 
					ON FCIB.ORDER_NO = OD1.intOrderNo
					AND FCIB.ORDER_YEAR = OD1.intOrderYear 
					AND FCIB.SALES_ORDER_ID = OD1.intSalesOrderId
            		WHERE FCIB.ORDER_YEAR = OD.intOrderYear
                   	AND FCIB.ORDER_NO = OD.intOrderNo),0),2) 			AS CUST_APP_DISP_AMOUNT,
					
					OD.dblPrice 													AS PRICE,
					
					ROUND(COALESCE((SELECT 
					SUM(FDD.dblGoodQty * OD2.dblPrice) 
					FROM ware_fabricdispatchdetails FDD 
					INNER JOIN ware_fabricdispatchheader FDH 
					ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo 
					AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
					INNER JOIN trn_orderdetails AS OD2 
					ON FDH.intOrderNo = OD2.intOrderNo 
					AND FDH.intOrderYear = OD2.intOrderYear 
					AND FDD.intSalesOrderId = OD2.intSalesOrderId
					WHERE FDH.intOrderNo = OH.intOrderNo 
					AND FDH.intOrderYear = OH.intOrderYear 
					AND FDH.intStatus = 1),0),2)									AS DISP_AMOUNT,					
					
					FPT.strName       												AS PAYMENT_TERM,
					
					DATEDIFF(NOW(),(SELECT
					DATE(FDH.dtmdate)
					FROM ware_fabricdispatchdetails FDD
					INNER JOIN ware_fabricdispatchheader FDH
					ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo
					AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
					WHERE FDH.intOrderNo = OH.intOrderNo
					AND FDH.intOrderYear = OH.intOrderYear
					AND FDD.intSalesOrderId = OD.intSalesOrderId
					AND FDH.intStatus = 1 
					ORDER BY 
					DATE(FDH.dtmdate) DESC LIMIT 1)) 							AS DIFF_DATES,
					
					(SELECT
					DATE(FDH.dtmdate)
					FROM ware_fabricdispatchdetails FDD
					INNER JOIN ware_fabricdispatchheader FDH
					ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo
					AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
					WHERE FDH.intOrderNo = OH.intOrderNo
					AND FDH.intOrderYear = OH.intOrderYear
					AND FDD.intSalesOrderId = OD.intSalesOrderId
					AND FDH.intStatus = 1 
					ORDER BY 
					DATE(FDH.dtmdate) DESC LIMIT 1)				 				AS DISPATCH_DATE,
					
					ROUND(COALESCE((SELECT SUM(VALUE) * -1
					FROM finance_customer_transaction FCT
					WHERE FCT.ORDER_NO = OH.intOrderNo
						AND FCT.ORDER_YEAR = OH.intOrderYear
					AND FCT.DOCUMENT_TYPE = 'PAYRECEIVE'),0),2)					AS PAYMENT_RECEIVED_AMOUNT,			
					
					ROUND(COALESCE((SELECT SUM(VALUE) * -1
					FROM finance_customer_transaction FCT
					WHERE FCT.ORDER_NO = OH.intOrderNo
						AND FCT.ORDER_YEAR = OH.intOrderYear
					AND FCT.INVOICE_NO IS NOT NULL
					AND FCT.INVOICE_YEAR IS NOT NULL
					AND DOCUMENT_TYPE = 'ADVANCE'),0),2)						AS ADVANCE_SETTLED,
					
					ROUND(COALESCE((SELECT SUM(VALUE) * -1
					FROM finance_customer_transaction FCT
					WHERE FCT.ORDER_NO = OH.intOrderNo
						AND FCT.ORDER_YEAR = OH.intOrderYear
					AND FCT.DOCUMENT_TYPE = 'CREDIT'),0),2)						AS CREDIT_NOTE_AMOUNT,
					
					/*ROUND(COALESCE((SELECT SUM(VALUE)
					FROM finance_customer_transaction FCT
					WHERE FCT.CUSTOMER_ID = OH.intCustomer
					AND FCT.DOCUMENT_TYPE = 'DEBIT_1'),0),2)						AS DEBIT_NOTE_AMOUNT, */
					
					ROUND(COALESCE((SELECT SUM(VALUE) 
					FROM finance_customer_transaction FCT
					WHERE FCT.ORDER_NO = OH.intOrderNo
						AND FCT.ORDER_YEAR = OH.intOrderYear
					AND DOCUMENT_TYPE = 'INVOICE'),0),2)						AS INVOICE_AMOUNT ,
					
					(SELECT TRANSACTION_DATE_TIME 
					FROM finance_customer_transaction FCT
					WHERE FCT.ORDER_NO = OH.intOrderNo
						AND FCT.ORDER_YEAR = OH.intOrderYear
					AND DOCUMENT_TYPE = 'INVOICE')								AS INVOICE_DATE 
					
					FROM trn_orderheader AS OH
					INNER JOIN trn_orderdetails AS OD 
						ON OH.intOrderNo = OD.intOrderNo 
						AND OH.intOrderYear = OD.intOrderYear
					INNER JOIN mst_financepaymentsterms FPT 
						ON FPT.intId = OH.intPaymentTerm
					INNER JOIN mst_customer 
						ON OH.intCustomer = mst_customer.intId
					INNER JOIN mst_locations LO
						ON LO.intId = OH.intLocationId
					LEFT JOIN finance_customer_invoice_header IH ON IH.ORDER_NO = OH.intOrderNo AND IH.ORDER_YEAR = OH.intOrderYear AND IH.STATUS = 1
					WHERE
						OH.intStatus = 1
						AND OH.PAYMENT_RECEIVE_COMPLETED_FLAG = 0 AND
						dtDate >= '$startDate' AND
						IH.SERIAL_NO IS NULL AND
						LO.intCompanyId = 1
						-- AND OH.intCustomer = 60
					GROUP BY
					OH.intOrderNo,
					OH.intOrderYear
					HAVING((TOT_DISP_QTY/TOT_ORDER_QTY)>=1 && DIFF_DATES >= 5)
					) AS TB
					GROUP BY ORDER_NO,
					ORDER_YEAR
					) AS TB1
					GROUP BY ORDER_NO, ORDER_YEAR 

					-- HAVING(BALANCE_TO_PAYMENT_RECEIVED>0) 
					ORDER BY CUSTOMER ";
  		return $sql;
	}
	private function load_customers_delivery_base_invoiced_rpt_sql($customerId,$company)
	{

		   $wSql = " LO.intCompanyId = $company ";
			
	       $sql = "SELECT tb1.*,
					(ROUND((tb1.INVOICE_AMOUNT),2) - (tb1.PAYMENT_RECEIVED_AMOUNT + tb1.ADVANCE_SETTLED + tb1.CREDIT_NOTE_AMOUNT)) AS BALANCE_TO_PAYMENT_RECEIVED 
					FROM
					(
					SELECT					
					OH.intCustomer 													AS CUSTOMER_ID,
					mst_customer.strName 											AS CUSTOMER,
					OH.intOrderNo 													AS ORDER_NO,
					OH.intOrderYear 												AS ORDER_YEAR,
					OD.intSalesOrderId 												AS SALES_ORDER_ID,
					OD.strSalesOrderNo 												AS SALES_ORDER_NO,
					SUM(OD.intQty) 													AS TOT_ORDER_QTY,					
					
					(SELECT 
					SUM(FDD.dblGoodQty)
					FROM ware_fabricdispatchdetails FDD 
					INNER JOIN ware_fabricdispatchheader FDH 
					ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo 
					AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
					WHERE FDH.intOrderNo = OH.intOrderNo 
					AND FDH.intOrderYear = OH.intOrderYear 
					AND FDH.intStatus = 1)										AS TOT_DISP_QTY,
					
					/*(SELECT SUM(LC_AMOUNT) AS totLCAmount
					FROM trn_lc_header LC
					WHERE LC.ORDER_NO = OH.intOrderNo
					AND LC.ORDER_YEAR = OH.intOrderYear 
					AND LC.STATUS = 1
					) 															AS TOT_LCAMOUNT,*/
					
					(SELECT
					SUM(FCIB.INVOICED_QTY)
					FROM
					finance_customer_invoice_balance FCIB
					WHERE
					FCIB.ORDER_YEAR = OD.intOrderYear AND
					FCIB.ORDER_NO = OD.intOrderNo )						AS CUST_APP_DISP_QTY,
					
					ROUND(COALESCE((SELECT
                	SUM(FCIB.INVOICED_QTY*OD1.dblPrice)
              		FROM finance_customer_invoice_balance FCIB 
					INNER JOIN trn_orderdetails AS OD1 
					ON FCIB.ORDER_NO = OD1.intOrderNo
					AND FCIB.ORDER_YEAR = OD1.intOrderYear 
					AND FCIB.SALES_ORDER_ID = OD1.intSalesOrderId
            		WHERE FCIB.ORDER_YEAR = OD.intOrderYear
                   	AND FCIB.ORDER_NO = OD.intOrderNo),0),2) 			AS CUST_APP_DISP_AMOUNT,
					
					OD.dblPrice 													AS PRICE,
					
					ROUND(COALESCE((SELECT 
					SUM(FDD.dblGoodQty * OD2.dblPrice) 
					FROM ware_fabricdispatchdetails FDD 
					INNER JOIN ware_fabricdispatchheader FDH 
					ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo 
					AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
					INNER JOIN trn_orderdetails AS OD2 
					ON FDH.intOrderNo = OD2.intOrderNo 
					AND FDH.intOrderYear = OD2.intOrderYear 
					AND FDD.intSalesOrderId = OD2.intSalesOrderId
					WHERE FDH.intOrderNo = OH.intOrderNo 
					AND FDH.intOrderYear = OH.intOrderYear 
					AND FDH.intStatus = 1),0),2)									AS DISP_AMOUNT,					
					
					FPT.strName       												AS PAYMENT_TERM,
					
					COALESCE(DATEDIFF(NOW(),(SELECT
					DATE(FIH.INVOICED_DATE)
					FROM finance_customer_invoice_header FIH
					WHERE FIH.ORDER_NO = OH.intOrderNo
					AND FIH.ORDER_YEAR = OH.intOrderYear
					AND FIH.STATUS = 1 
					ORDER BY 
					DATE(FIH.INVOICED_DATE) DESC LIMIT 1)),0) 						AS DIFF_DATES,
					
					(SELECT
					DATE(FDH.dtmdate)
					FROM ware_fabricdispatchdetails FDD
					INNER JOIN ware_fabricdispatchheader FDH
					ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo
					AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
					WHERE FDH.intOrderNo = OH.intOrderNo
					AND FDH.intOrderYear = OH.intOrderYear
					AND FDD.intSalesOrderId = OD.intSalesOrderId
					AND FDH.intStatus = 1 
					ORDER BY 
					DATE(FDH.dtmdate) DESC LIMIT 1)				 				AS DISPATCH_DATE,
					
					ROUND(COALESCE((SELECT SUM(VALUE) * -1
					FROM finance_customer_transaction FCT
					WHERE FCT.ORDER_NO = OH.intOrderNo
						AND FCT.ORDER_YEAR = OH.intOrderYear
					AND FCT.DOCUMENT_TYPE = 'PAYRECEIVE'),0),2)					AS PAYMENT_RECEIVED_AMOUNT,			
					
					ROUND(COALESCE((SELECT SUM(VALUE) * -1
					FROM finance_customer_transaction FCT
					WHERE FCT.ORDER_NO = OH.intOrderNo
						AND FCT.ORDER_YEAR = OH.intOrderYear
					AND FCT.INVOICE_NO IS NOT NULL
					AND FCT.INVOICE_YEAR IS NOT NULL
					AND DOCUMENT_TYPE IN('INV_SETTLEMENT','ADVANCE')),0),2)                        AS ADVANCE_SETTLED,
					
					ROUND(COALESCE((SELECT SUM(VALUE) * -1
					FROM finance_customer_transaction FCT
					WHERE FCT.ORDER_NO = OH.intOrderNo
						AND FCT.ORDER_YEAR = OH.intOrderYear
					AND FCT.DOCUMENT_TYPE = 'CREDIT'),0),2)						AS CREDIT_NOTE_AMOUNT,
					
					/*ROUND(COALESCE((SELECT SUM(VALUE)
					FROM finance_customer_transaction FCT
					WHERE FCT.CUSTOMER_ID = OH.intCustomer
					AND FCT.DOCUMENT_TYPE = 'DEBIT'),0),2)						AS DEBIT_NOTE_AMOUNT,*/ 
					
					ROUND(COALESCE((SELECT SUM(VALUE) 
					FROM finance_customer_transaction FCT
					WHERE FCT.ORDER_NO = OH.intOrderNo
						AND FCT.ORDER_YEAR = OH.intOrderYear
					AND DOCUMENT_TYPE = 'INVOICE'),0),2)						AS INVOICE_AMOUNT 
					
					FROM trn_orderheader AS OH
					INNER JOIN trn_orderdetails AS OD 
						ON OH.intOrderNo = OD.intOrderNo 
						AND OH.intOrderYear = OD.intOrderYear
					INNER JOIN mst_financepaymentsterms FPT 
						ON FPT.intId = OH.intPaymentTerm
					INNER JOIN mst_customer 
						ON OH.intCustomer = mst_customer.intId
					INNER JOIN mst_locations LO
						ON LO.intId = OH.intLocationId
					WHERE
						-- OH.intStatus = 1
						-- AND OH.PAYMENT_RECEIVE_COMPLETED_FLAG = 0
					$wSql
					AND OH.intCustomer = $customerId
					GROUP BY
					OH.intCustomer,
					OH.intOrderNo,
					OH.intOrderYear
					HAVING((TOT_DISP_QTY/TOT_ORDER_QTY)>=1 OR INVOICE_AMOUNT>0)
					) as tb1
					HAVING (BALANCE_TO_PAYMENT_RECEIVED<>0)";
  		return $sql;
	}
 }		  


?>
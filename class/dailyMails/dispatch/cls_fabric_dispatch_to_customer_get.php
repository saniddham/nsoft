<?php
 
include_once $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
include_once $_SESSION['ROOT_PATH']."class/cls_commonErrorHandeling_get.php";
  
$obj_commonErr		= new cls_commonErrorHandeling_get($db);
$obj_common			= new cls_commonFunctions_get($db);
 
class cls_fabric_dispatch_to_customer_get
{
	private $db;
 	
	function __construct($db)
	{
		$this->db 	= $db;
 	}

 	public function getCustomers($cluster)
	{
 		$sql		= $this->load_customer_sql($cluster);
 		$result		= $this->db->RunQuery($sql);
 		return $result;
	}
 	public function getCustomers_dispatch($customerId,$customerLocationId,$pub_brandId,$cluster)
	{
 		$sql		= $this->load_customers_dispatch_sql($customerId,$customerLocationId,$pub_brandId,$cluster);
 		$result		= $this->db->RunQuery($sql);
 		return $result;
	}
  
//BEGIN - PRIVATE FUNCTIONS {	
	private function load_customer_sql($cluster)
	{
	   $sql = "SELECT DISTINCT
					ware_fabricdispatchheader.intCustLocation,
					trn_orderheader.intCustomer,
					mst_customer.strName AS customerName,
					mst_customer_locations_header.strName AS customerLocationName,
					mst_customer_brand_wise_emails.BRAND_ID,
					mst_customer_brand_wise_emails.EMAIL as strEmailAddress,
					mst_customer_brand_wise_emails.EMAIL_CC as strCCEmailAddress
				FROM
					ware_fabricdispatchheader
				INNER JOIN ware_fabricdispatchdetails ON ware_fabricdispatchdetails.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo
					 AND ware_fabricdispatchdetails.intBulkDispatchNoYear = ware_fabricdispatchheader.intBulkDispatchNoYear
				INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = ware_fabricdispatchheader.intOrderNo 
					AND trn_orderheader.intOrderYear = ware_fabricdispatchheader.intOrderYear
				INNER JOIN ware_fabricdispatchheader_approvedby ON 
					ware_fabricdispatchheader_approvedby.intBulkDispatchNo = ware_fabricdispatchheader.intBulkDispatchNo 
					AND ware_fabricdispatchheader_approvedby.intYear = ware_fabricdispatchheader.intBulkDispatchNoYear
				INNER JOIN mst_customer ON mst_customer.intId = trn_orderheader.intCustomer
				INNER JOIN mst_customer_locations_header ON mst_customer_locations_header.intId = ware_fabricdispatchheader.intCustLocation
				INNER JOIN mst_customer_locations ON mst_customer.intId = mst_customer_locations.intCustomerId AND mst_customer_locations_header.intId = mst_customer_locations.intLocationId
				INNER JOIN mst_customer_brand_wise_emails ON mst_customer_brand_wise_emails.CUSTOMER_ID = trn_orderheader.intCustomer AND mst_customer_brand_wise_emails.LOCATION_ID = ware_fabricdispatchheader.intCustLocation
				INNER JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
				INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intBrand = mst_customer_brand_wise_emails.BRAND_ID AND trn_orderdetails.intSampleNo = trn_sampleinfomations.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo AND trn_sampleinfomations.intCustomer = mst_customer_brand_wise_emails.CUSTOMER_ID
				LEFT JOIN mst_locations ON trn_orderheader.intLocationId = mst_locations.intId 
				LEFT JOIN mst_plant ON mst_locations.intPlant = mst_plant.intPlantId 
				WHERE
					mst_plant.MAIN_CLUSTER_ID = '$cluster' AND 
					ware_fabricdispatchheader_approvedby.dtApprovedDate  < CONCAT(DATE(NOW()),' ','05:00:00')  AND  
					ware_fabricdispatchheader_approvedby.dtApprovedDate >= CONCAT(DATE(DATE_ADD(NOW(),INTERVAL -1 DAY )),' ','05:00:00')  AND
					ware_fabricdispatchheader.intStatus = 1 AND 
					mst_customer.intSizeWiseSummary  =  1 		AND
					mst_customer.intBrandWiseEmails  = 1 AND
					
					
					(
					ware_fabricdispatchdetails.dblSampleQty+
					ware_fabricdispatchdetails.dblGoodQty+
					ware_fabricdispatchdetails.dblEmbroideryQty+
					ware_fabricdispatchdetails.dblPDammageQty+
					ware_fabricdispatchdetails.dblFdammageQty+
					ware_fabricdispatchdetails.dblCutRetQty
					) >0
		
				ORDER BY intCustomer
				";
  		return $sql;
 	}
	
	private function load_customers_dispatch_sql($customerId,$customerLocationId,$pub_brandId,$cluster)
	{
	   $sql = "SELECT
				distinct 
				ware_fabricdispatchheader.intBulkDispatchNo,
				ware_fabricdispatchheader.intBulkDispatchNoYear,
				ware_fabricdispatchheader_approvedby.dtApprovedDate,
				trn_orderdetails.strStyleNo,
				trn_orderdetails.strGraphicNo,
				trn_orderheader.strCustomerPoNo,
				ware_fabricdispatchheader.intOrderNo,
				ware_fabricdispatchheader.intOrderYear,
				trn_orderdetails.strSalesOrderNo,
				ware_fabricdispatchdetails.strSize,
				Sum(ware_fabricdispatchdetails.dblSampleQty) AS dblSampleQty,
				Sum(ware_fabricdispatchdetails.dblGoodQty) AS dblGoodQty,
				Sum(ware_fabricdispatchdetails.dblEmbroideryQty) AS dblEmbroideryQty,
				Sum(ware_fabricdispatchdetails.dblPDammageQty) AS dblPDammageQty,
				Sum(ware_fabricdispatchdetails.dblFDammageQty) AS dblFDammageQty,
				Sum(ware_fabricdispatchdetails.dblCutRetQty) AS dblCutRetQty,
				mst_brand.strName AS brandName,
				trn_orderdetails.dblDamagePercentage,
				mst_plant.strPlantName								 							AS plantName,
				CONCAT(mst_plant.strPlantHeadName,'(', mst_plant.strPlantName  ,')',' - ',strPlantHeadContactNo) 						AS PlantHeadName,
				
				ROUND(COALESCE((SELECT
				  SUM(dblQty)
				FROM ware_fabricreceivedheader FRH
				  INNER JOIN ware_fabricreceiveddetails FRD
					ON FRD.intFabricReceivedNo = FRH.intFabricReceivedNo
					  AND FRD.intFabricReceivedYear = FRH.intFabricReceivedYear
				  INNER JOIN trn_orderdetails SUB_OD 
					ON SUB_OD.intOrderNo = FRH.intOrderNo 
					  AND SUB_OD.intOrderYear = FRH.intOrderYear
					  AND SUB_OD.intSalesOrderId = FRD.intSalesOrderId 
				WHERE 
					FRH.intOrderNo = trn_orderheader.intOrderNo
					AND FRH.intOrderYear = trn_orderheader.intOrderYear
					AND SUB_OD.strGraphicNo = trn_orderdetails.strGraphicNo
					AND FRD.strSize = ware_fabricdispatchdetails.strSize
					AND FRH.intStatus = 1),0))						AS TOTAL_IN,
				
				ROUND(COALESCE((SELECT SUM(dblGoodQty)
				FROM ware_fabricdispatchheader FDH
				  INNER JOIN ware_fabricdispatchdetails FDD
					ON FDD.intBulkDispatchNo = FDH.intBulkDispatchNo
					  AND FDD.intBulkDispatchNoYear = FDH.intBulkDispatchNoYear
				  INNER JOIN trn_orderdetails SUB_OD 
					ON SUB_OD.intOrderNo = FDH.intOrderNo 
					  AND SUB_OD.intOrderYear = FDH.intOrderYear
					  AND SUB_OD.intSalesOrderId = FDD.intSalesOrderId
				WHERE 
					FDH.intOrderNo = trn_orderheader.intOrderNo
					AND FDH.intOrderYear = trn_orderheader.intOrderYear
					AND SUB_OD.strGraphicNo = trn_orderdetails.strGraphicNo
					AND FDD.strSize = ware_fabricdispatchdetails.strSize
					AND FDH.intStatus = 1),0))														AS TOTAL_DELEVERY
					
				FROM
				trn_orderdetails
				Inner Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
				Inner Join ware_fabricdispatchheader ON ware_fabricdispatchheader.intOrderNo = trn_orderdetails.intOrderNo AND ware_fabricdispatchheader.intOrderYear = trn_orderdetails.intOrderYear
				Inner Join ware_fabricdispatchdetails ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear AND ware_fabricdispatchdetails.intSalesOrderId = trn_orderdetails.intSalesOrderId
				Left Join mst_part ON ware_fabricdispatchdetails.intPart = mst_part.intId
				Left Join mst_colors_ground ON ware_fabricdispatchdetails.intGroundColor = mst_colors_ground.intId
				Inner Join ware_fabricdispatchheader_approvedby ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchheader_approvedby.intBulkDispatchNo AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchheader_approvedby.intYear AND ware_fabricdispatchheader_approvedby.intApproveLevelNo = ware_fabricdispatchheader.intApproveLevels
				Inner Join trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_orderdetails.intSampleNo AND trn_sampleinfomations.intSampleYear = trn_orderdetails.intSampleYear AND trn_sampleinfomations.intRevisionNo = trn_orderdetails.intRevisionNo
				Inner Join mst_brand ON mst_brand.intId = trn_sampleinfomations.intBrand
				INNER JOIN mst_locations 
					ON mst_locations.intId = ware_fabricdispatchheader.intCompanyId
				INNER JOIN mst_companies
					ON mst_companies.intId = mst_locations.intCompanyId
				INNER JOIN mst_plant 
					ON mst_plant.intPlantId = mst_locations.intPlant
				WHERE  
					mst_plant.MAIN_CLUSTER_ID = '$cluster' AND 
					ware_fabricdispatchheader_approvedby.dtApprovedDate < CONCAT(DATE(NOW()),' ','05:00:00')  AND  
					ware_fabricdispatchheader_approvedby.dtApprovedDate >= CONCAT(DATE(DATE_ADD(NOW(),INTERVAL -1 DAY )),' ','05:00:00')  AND  
					ware_fabricdispatchheader.intStatus =  '1'  
					AND trn_orderheader.intCustomer = $customerId 
					AND ware_fabricdispatchheader.intCustLocation = $customerLocationId
					AND trn_sampleinfomations.intBrand = $pub_brandId 
				GROUP BY
					trn_orderdetails.strSalesOrderNo,
					ware_fabricdispatchheader.intOrderNo,
					ware_fabricdispatchheader.intOrderYear,
					trn_sampleinfomations.intBrand,
					ware_fabricdispatchheader.intBulkDispatchNo,
					ware_fabricdispatchheader.intBulkDispatchNoYear,
					ware_fabricdispatchdetails.strSize
				ORDER BY
					mst_brand.strName,
					ware_fabricdispatchheader.intOrderYear ASC,
					ware_fabricdispatchheader.intOrderNo ASC,
					trn_orderdetails.strSalesOrderNo ASC,
					trn_orderdetails.strStyleNo ASC,
					trn_orderheader.strCustomerPoNo ASC,
					ware_fabricdispatchheader.intBulkDispatchNo ASC,
					ware_fabricdispatchheader.intBulkDispatchNoYear ASC,
					ware_fabricdispatchdetails.strSize ASC
								";
  		return $sql;
 	}
 	
 }

?>
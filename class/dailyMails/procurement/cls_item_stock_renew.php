<?php
session_start();

include_once $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
include_once $_SESSION['ROOT_PATH']."class/cls_commonErrorHandeling_get.php";

$obj_commonErr		= new cls_commonErrorHandeling_get($db);
$obj_common	        = new cls_commonFunctions_get($db);

class cls_item_stock_renew
{
    private $db;

    function __construct($db)
    {
        $this->db 	= $db;
    }

    public function getItemListToBeRenewedSql($company,$curYear)
    {

        $sql		= $this->getStockBalanceReportForCompany($company,$curYear);
        $result	= $this->db->RunQuery($sql);
        return $result;
    }

    public function get_ho_loc($company)
    {

        $sql="SELECT
			mst_locations.intId
			FROM `mst_locations`
			WHERE
			mst_locations.COMPANY_MAIN_STORES_LOCATION = 1 AND
			mst_locations.intCompanyId = '$company'
			";

        $result = $this->db->RunQuery($sql);
        $row = mysqli_fetch_array($result);
        return $row['intId'];
    }

    public function getStockBalanceReportForCompany($company,$curYear){

        $head_office_loc = $this->get_ho_loc($company);
        $sql =

            "
SELECT
TB_ALL_1.type as type,
TB_ALL_1.ORDER_COMPANY as ORDER_COMPANY,
TB_ALL_1.location as location,
TB_ALL_1.mainCatId as mainCatId,
TB_ALL_1.subCatId as subCatId,
TB_ALL_1.ITEM as ITEM,
TB_ALL_1.re_order_policy as re_order_policy,
TB_ALL_1.mainCatName as mainCatName,
TB_ALL_1.subCatName as subCatName,
TB_ALL_1.strCode as strCode,
TB_ALL_1.itemName as itemName,
TB_ALL_1.MOQ as MOQ,
TB_ALL_1.rolQty AS rolQty,
TB_ALL_1.TODATE_ORDERS as TODATE_ORDERS,
TB_ALL_1.REQUIRED_TODATE as REQUIRED_TODATE,
TB_ALL_1.REQUIRED_NO as REQUIRED_NO,
TB_ALL_1.PRN_NO as PRN_NO,
TB_ALL_1.PRN_QTY_TODATE as PRN_QTY_TODATE,
TB_ALL_1.PO_QTY_TODATE as PO_QTY_TODATE,
TB_ALL_1.MRN_QTY_TODATE as MRN_QTY_TODATE,
TB_ALL_1.pending_mrn_qry as pending_mrn_qry ,
TB_ALL_1.ISSUE_QTY_TODATE as ISSUE_QTY_TODATE,
TB_ALL_1.RET_TO_STORE_QTY_TODATE as RET_TO_STORE_QTY_TODATE,
TB_ALL_1.GP_FROM_HO_TODATE as GP_FROM_HO_TODATE,
TB_ALL_1.GP_IN_QTY_TO_HO_TODATE as GP_IN_QTY_TO_HO_TODATE,
TB_ALL_1.UOM as UOM,
TB_ALL_1.ISSUE_QTY_ALL_HO as ISSUE_QTY_ALL_HO,
TB_ALL_1.GP_FROM_HO_ALL as GP_FROM_HO_ALL,
TB_ALL_1.ISSUE_QTY_ALL_NON_HO as ISSUE_QTY_ALL_NON_HO,
(TB_ALL_1.MRNids) AS MRNids,
(TB_ALL_1.ISSUEids) AS ISSUEids

from
 (SELECT
	TB_ALL.type,
	TB_ALL.date AS DATES_TODATE,
	TB_ALL.date AS DATES_OTHER,
	TB_ALL.ORDER_COMPANY,
	TB_ALL.location AS location,
	TB_ALL.mainCatId,
	TB_ALL.subCatId,
	TB_ALL.ITEM,
	TB_ALL.re_order_policy,
	TB_ALL.mainCatName,
	TB_ALL.subCatName,
	TB_ALL.strCode,
	TB_ALL.itemName,
	TB_ALL.MOQ,
	TB_ALL.rolQty,
	TB_ALL.otherOrders AS TODATE_ORDERS,
	sum(TB_ALL.REQUIRED) AS REQUIRED_TODATE,
	GROUP_CONCAT(TB_ALL.REQUIRED_NO) AS REQUIRED_NO,
	GROUP_CONCAT(TB_ALL.PRN_NO) AS PRN_NO,
	sum(TB_ALL.PRN_QTY) AS PRN_QTY_TODATE,
	sum(TB_ALL.PO_QTY) AS PO_QTY_TODATE,
	sum((
		ifnull(TB_ALL.MRN_QTY, 0) + ifnull(TB_ALL.MRN_QTY_OTHER_LOC, 0)
	)) AS MRN_QTY_TODATE,

    round((sum(TB_ALL.REQUIRED)-sum(ifnull(TB_ALL.MRN_QTY, 0) + ifnull(TB_ALL.MRN_QTY_OTHER_LOC, 0))), 4) as pending_mrn_qry,

	sum((
		ifnull(TB_ALL.ISSUE_QTY, 0) + ifnull(
			TB_ALL.ISSUE_QTY_OTHER_LOC,
			0
		)
	)) AS ISSUE_QTY_TODATE,
	sum((
		ifnull(TB_ALL.RET_TO_STORE_QTY, 0) + ifnull(
			TB_ALL.RET_TO_STORE_QTY_OTHER_LOC,
			0
		)
	) )AS RET_TO_STORE_QTY_TODATE,
	sum(TB_ALL.GP_QTY) AS GP_FROM_HO_TODATE,
	sum(TB_ALL.GP_IN_QTY) AS GP_IN_QTY_TO_HO_TODATE,
	TB_ALL.UOM,
	TB_ALL.otherOrders AS OTHER_ORDERS,
	sum((ifnull(TB_ALL.ISSUE_QTY, 0))) AS ISSUE_QTY_ALL_HO,
	sum((ifnull(TB_ALL.GP_QTY, 0))) AS GP_FROM_HO_ALL,
	sum((
		ifnull(
			TB_ALL.ISSUE_QTY_OTHER_LOC,
			0
		)
	) )AS ISSUE_QTY_ALL_NON_HO,
	TB_ALL.PO_QTY AS PO_QTY_OTHER,
	TB_ALL.ORDER_NO,
	TB_ALL.ORDER_STATUS,
	TB_ALL.ORDER_TYPE,
	TB_ALL.CREATE_DATE,
	TB_ALL.SO,
	GROUP_CONCAT(TB_ALL.MRNids) as MRNids,
	GROUP_CONCAT(TB_ALL.ISSUEids) as ISSUEids	

from
(select tb.type,
tb.ORDER_COMPANY,
tb.location AS location,
tb.date,
tb.ITEM,
GROUP_CONCAT(DISTINCT tb.ORDER_NO,'/',tb.ORDER_YEAR) as otherOrders,

SUM(tb.REQUIRED) as REQUIRED,
GROUP_CONCAT(tb.REQUIRED_NO) as REQUIRED_NO,
GROUP_CONCAT(tb.PRN_NO) as PRN_NO,
tb.TRANSACTION_LOCATION,
sum(tb.PO_QTY) as PO_QTY,
sum(tb.PRN_QTY) as PRN_QTY,
tb.mainCatId,
tb.subCatId,
tb.mainCatName,
tb.subCatName,
tb.strCode,
tb.itemName,
tb.MOQ,
tb.rolQty,
tb.re_order_policy,
tb.UOM,
(SUM(tb.MRN_QTY) + SUM(tb.mrn_pending)) AS MRN_QTY,
SUM(tb.MRN_EXTRA_QTY) as MRN_EXTRA_QTY,
SUM(tb.ISSUE_QTY) as ISSUE_QTY,
SUM(tb.GP_QTY) as GP_QTY,
SUM(tb.GP_IN_QTY) as GP_IN_QTY,
SUM(tb.RET_TO_STORE_QTY) as RET_TO_STORE_QTY,
SUM(tb.MRN_QTY_OTHER_LOC) as MRN_QTY_OTHER_LOC,
SUM(tb.MRN_EXTRA_QTY_OTHER_LOC) as MRN_EXTRA_QTY_OTHER_LOC,
SUM(tb.ISSUE_QTY_OTHER_LOC) as ISSUE_QTY_OTHER_LOC,
SUM(tb.GP_QTY_OTHER_LOC) as GP_QTY_OTHER_LOC,
SUM(tb.GP_IN_QTY_OTHER_LOC) as GP_IN_QTY_OTHER_LOC,
SUM(tb.RET_TO_STORE_QTY_OTHER_LOC) as RET_TO_STORE_QTY_OTHER_LOC ,
tb.ORDER_NO,
tb.ORDER_STATUS,
tb.ORDER_TYPE,
tb.CREATE_DATE,
tb.SO,
GROUP_CONCAT(tb.MRNids) as MRNids,
GROUP_CONCAT(tb.ISSUEids) as ISSUEids
from(
SELECT TB3.type,
TB3.ORDER_COMPANY,
TB3.location AS location,
TB3.date,
TB3.ITEM,
TB3.ORDER_NO,
TB3.ORDER_YEAR,
(
SELECT
sum(
trn_po_prn_details.REQUIRED
)
FROM
trn_po_prn_details
WHERE
trn_po_prn_details.ORDER_NO = TB3.ORDER_NO
AND trn_po_prn_details.ORDER_YEAR = TB3.ORDER_YEAR
AND trn_po_prn_details.ITEM = TB3.ITEM
) AS REQUIRED, 
(SELECT GROUP_CONCAT(CONCAT(trn_po_prn_details.ORDER_NO,'-',trn_po_prn_details.ORDER_YEAR,'/',trn_po_prn_details.REQUIRED))
FROM
trn_po_prn_details
WHERE
trn_po_prn_details.ORDER_NO = TB3.ORDER_NO
AND trn_po_prn_details.ORDER_YEAR = TB3.ORDER_YEAR
AND trn_po_prn_details.ITEM = TB3.ITEM
) AS REQUIRED_NO,
(SELECT GROUP_CONCAT(CONCAT(trn_po_prn_details.ORDER_NO,'-',trn_po_prn_details.ORDER_YEAR,'/',trn_po_prn_details.PRN_QTY))
FROM
trn_po_prn_details
WHERE
trn_po_prn_details.ORDER_NO = TB3.ORDER_NO
AND trn_po_prn_details.ORDER_YEAR = TB3.ORDER_YEAR
AND trn_po_prn_details.ITEM = TB3.ITEM
) AS PRN_NO, 

(
SELECT
sum(
trn_po_prn_details.PURCHASED_QTY)
FROM trn_po_prn_details
WHERE trn_po_prn_details.ORDER_NO = TB3.ORDER_NO
AND trn_po_prn_details.ORDER_YEAR = TB3.ORDER_YEAR
AND trn_po_prn_details.ITEM = TB3.ITEM ) AS PO_QTY,

(SELECT SUM(trn_po_prn_details.PRN_QTY)
FROM trn_po_prn_details
WHERE trn_po_prn_details.ORDER_NO = TB3.ORDER_NO AND
trn_po_prn_details.ORDER_YEAR = TB3.ORDER_YEAR AND
trn_po_prn_details.ITEM = TB3.ITEM ) AS PRN_QTY,";

        $sql .= "

TB3.TRANSACTION_LOCATION,
SUM(TB3.mrn_pending) AS mrn_pending,
TB3.mainCatId,
TB3.subCatId,
TB3.mainCatName,
TB3.subCatName,
TB3.strCode,
TB3.itemName,
TB3.MOQ,
TB3.re_order_policy,
TB3.UOM,
TB3.rolQty,
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.MRN_QTY,0)) as MRN_QTY,
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.MRN_EXTRA_QTY,0)) as MRN_EXTRA_QTY,
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.ISSUE_QTY,0)) as ISSUE_QTY,
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.GP_QTY,0)) as GP_QTY,
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.GP_IN_QTY,0)) as GP_IN_QTY,
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.RET_TO_STORE_QTY,0)) as RET_TO_STORE_QTY,
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.MRN_QTY,0)) as MRN_QTY_OTHER_LOC,
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.MRN_EXTRA_QTY,0)) as MRN_EXTRA_QTY_OTHER_LOC,
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.ISSUE_QTY,0)) as ISSUE_QTY_OTHER_LOC,
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.GP_QTY,0)) as GP_QTY_OTHER_LOC,
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.GP_IN_QTY,0)) as GP_IN_QTY_OTHER_LOC,
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.RET_TO_STORE_QTY,0)) as RET_TO_STORE_QTY_OTHER_LOC,
TB3.ORDER_STATUS,
TB3.ORDER_TYPE,
TB3.CREATE_DATE,
TB3.SO,
GROUP_CONCAT(TB3.MRNids) as MRNids,
GROUP_CONCAT(TB3.ISSUEids) as ISSUEids
FROM
 (SELECT TB2.ORDER_COMPANY,
TB2.ORDER_NO,
TB2.ORDER_YEAR,
TB2.location,
TB2.ITEM,
TB2.SALES_ORDER,
TB2.type,
(TB2.pending_mrn_qty_new) AS mrn_pending,
CONCAT(trn_orderdetails.strSalesOrderNo,'/',trn_orderdetails.strCombo,'/',trn_orderdetails.strPrintName) AS SALES_ORDER_String,
'' AS date,
mst_maincategory.intId as mainCatId,
mst_subcategory.intId AS subCatId,
mst_maincategory.strName AS mainCatName,
mst_subcategory.strName AS subCatName,
mst_item.strCode,
mst_item.intId,
mst_item.strName AS itemName,
mst_item.dblMinimumOrderQty AS MOQ,
mst_item.intReorderPolicy AS re_order_policy,
mst_units.strName AS UOM,
ROL.qty AS rolQty,";

        $sql .= "trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID AS TRANSACTION_LOCATION,
(SELECT GROUP_CONCAT(
											CONCAT(
												trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
												'-',
												trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
												'/',
												(
													trn_po_prn_details_sales_order_stock_transactions.QTY
												)
											))
										FROM
											trn_po_prn_details_sales_order_stock_transactions
										WHERE
											trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = TB2.ORDER_NO
										AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = TB2.ORDER_YEAR
										AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = TB2.SALES_ORDER
										AND trn_po_prn_details_sales_order_stock_transactions.ITEM = TB2.ITEM
										AND trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'MRN'
									) AS MRNids,
(SELECT GROUP_CONCAT(
											CONCAT(
												trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
												'-',
												trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
												'/',
												(
													trn_po_prn_details_sales_order_stock_transactions.QTY
												)
											))
										FROM
											trn_po_prn_details_sales_order_stock_transactions
										WHERE
											trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = TB2.ORDER_NO
										AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = TB2.ORDER_YEAR
										AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = TB2.SALES_ORDER
										AND trn_po_prn_details_sales_order_stock_transactions.ITEM = TB2.ITEM
										AND trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'ISSUE'
									) AS ISSUEids,
SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'MRN',
trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS MRN_QTY,
SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'MRN_EXTRA',
trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS MRN_EXTRA_QTY,
SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'ISSUE',
trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS ISSUE_QTY,
SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'GATE_PASS',
trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS GP_QTY,
SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'GATE_PASS_IN',
trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS GP_IN_QTY,
SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'RETURNED_TO_STORES',
trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS RET_TO_STORE_QTY,
TB2.ORDER_STATUS,
TB2.ORDER_TYPE,
TB2.CREATE_DATE,
(
trn_orderdetails.strSalesOrderNo
) AS SO
FROM
( SELECT TB1.ORDER_NO,
									TB1.ORDER_YEAR,
									TB1.SALES_ORDER,
									TB1.ITEM,
									TB1.ORDER_COMPANY,
									TB1.location,
									sum(TB1.pending_mrn_qty) AS pending_mrn_qty_new,
									TB1.type,
									TB1.ORDER_STATUS,
									TB1.ORDER_TYPE,
									TB1.CREATE_DATE FROM 
(
SELECT
trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER,
trn_po_prn_details_sales_order_stock_transactions.ITEM,
mst_locations.intCompanyId AS ORDER_COMPANY,
mst_locations.strName AS location,
0 as pending_mrn_qty,
( SELECT GROUP_CONCAT(mst_locations.intCompanyId)
FROM ware_fabricreceivedheader
INNER JOIN mst_locations
ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId
WHERE
ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO
AND ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
)AS FABRIC_IN_COMPANIES,
1 AS type,
trn_orderheader.intStatus AS ORDER_STATUS,
trn_orderheader.PO_TYPE AS ORDER_TYPE,
trn_orderheader.dtDate AS CREATE_DATE
FROM trn_po_prn_details_sales_order_stock_transactions
INNER JOIN trn_po_prn_details_sales_order
ON
trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = trn_po_prn_details_sales_order.ORDER_NO
AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR
AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER
AND trn_po_prn_details_sales_order_stock_transactions.ITEM = trn_po_prn_details_sales_order.ITEM
INNER JOIN trn_orderheader
ON trn_orderheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO AND
trn_orderheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
AND trn_orderheader.intStatus = 1
INNER JOIN mst_locations
ON mst_locations.intId = trn_orderheader.intLocationId

GROUP BY trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER,
trn_po_prn_details_sales_order_stock_transactions.ITEM
HAVING FIND_IN_SET('$company', FABRIC_IN_COMPANIES) OR ORDER_COMPANY = '$company'

UNION ALL
SELECT
trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER,
trn_po_prn_details_sales_order_stock_transactions.ITEM,
mst_locations.intCompanyId AS ORDER_COMPANY,
mst_locations.strName AS location,
0 as pending_mrn_qty,
( SELECT GROUP_CONCAT( DISTINCT mst_locations.intCompanyId )
FROM ware_fabricreceivedheader
INNER JOIN mst_locations
ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId
WHERE
ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO AND
ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
) AS FABRIC_IN_COMPANIES,
2 AS type,
trn_orderheader.intStatus AS ORDER_STATUS,
trn_orderheader.PO_TYPE AS ORDER_TYPE,
trn_orderheader.dtDate AS CREATE_DATE
FROM trn_po_prn_details_sales_order_stock_transactions

LEFT JOIN trn_po_prn_details_sales_order
ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = trn_po_prn_details_sales_order.ORDER_NO AND
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR AND
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER AND
trn_po_prn_details_sales_order_stock_transactions.ITEM = trn_po_prn_details_sales_order.ITEM
INNER JOIN trn_orderheader ON
trn_orderheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO AND
trn_orderheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
AND trn_orderheader.intStatus = 1
INNER JOIN mst_locations ON mst_locations.intId = trn_orderheader.intLocationId

WHERE trn_po_prn_details_sales_order.ORDER_NO IS NULL

GROUP BY
trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER,
trn_po_prn_details_sales_order_stock_transactions.ITEM
HAVING ( FIND_IN_SET('$company', FABRIC_IN_COMPANIES) OR ORDER_COMPANY = '$company' )


UNION ALL
SELECT
trn_po_prn_details_sales_order.ORDER_NO,
trn_po_prn_details_sales_order.ORDER_YEAR,
trn_po_prn_details_sales_order.SALES_ORDER,
trn_po_prn_details_sales_order.ITEM,
mst_locations.intCompanyId AS ORDER_COMPANY,
mst_locations.strName AS location,
0 as pending_mrn_qty,
( SELECT GROUP_CONCAT( DISTINCT mst_locations.intCompanyId )
FROM ware_fabricreceivedheader
INNER JOIN mst_locations
ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId
WHERE ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order.ORDER_NO
AND ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order.ORDER_YEAR
) AS FABRIC_IN_COMPANIES,
1 AS type,
trn_orderheader.intStatus AS ORDER_STATUS,
trn_orderheader.PO_TYPE AS ORDER_TYPE,
trn_orderheader.dtDate AS CREATE_DATE
FROM trn_po_prn_details_sales_order_stock_transactions

RIGHT JOIN
trn_po_prn_details_sales_order
ON
trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = trn_po_prn_details_sales_order.ORDER_NO AND
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR AND
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER AND
trn_po_prn_details_sales_order_stock_transactions.ITEM = trn_po_prn_details_sales_order.ITEM
INNER JOIN trn_orderheader
ON trn_orderheader.intOrderNo = trn_po_prn_details_sales_order.ORDER_NO AND
trn_orderheader.intOrderYear = trn_po_prn_details_sales_order.ORDER_YEAR
AND trn_orderheader.intStatus = 1
INNER JOIN mst_locations
ON mst_locations.intId = trn_orderheader.intLocationId

WHERE trn_po_prn_details_sales_order_stock_transactions.ORDER_NO IS NULL

GROUP BY
trn_po_prn_details_sales_order.ORDER_NO,
trn_po_prn_details_sales_order.ORDER_YEAR,
trn_po_prn_details_sales_order.SALES_ORDER,
trn_po_prn_details_sales_order.ITEM

HAVING ( FIND_IN_SET('$company', FABRIC_IN_COMPANIES) OR ORDER_COMPANY = '$company' ) UNION ALL 		
SELECT
ware_mrndetails.intOrderNo AS ORDER_NO,
ware_mrndetails.intOrderYear AS ORDER_YEAR,
ware_mrndetails.strStyleNo AS SALES_ORDER,
ware_mrndetails.intItemId AS ITEM,
mst_locations.intCompanyId AS ORDER_COMPANY,
mst_locations.strName AS location,
ware_mrndetails.dblQty as pending_mrn_qty,

(
SELECT
GROUP_CONCAT(
DISTINCT mst_locations.intCompanyId
)
FROM
ware_fabricreceivedheader
INNER JOIN mst_locations ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId
WHERE
ware_fabricreceivedheader.intOrderNo = ware_mrndetails.intOrderNo
AND ware_fabricreceivedheader.intOrderYear = ware_mrndetails.intOrderYear
) AS FABRIC_IN_COMPANIES,
2 AS type,

0 AS ORDER_STATUS,
0 AS ORDER_TYPE,
0 AS CREATE_DATE
FROM
trn_po_prn_details_sales_order_stock_transactions
RIGHT JOIN ware_mrndetails ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = ware_mrndetails.intOrderNo
AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = ware_mrndetails.intOrderYear
AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = ware_mrndetails.strStyleNo
AND trn_po_prn_details_sales_order_stock_transactions.ITEM = ware_mrndetails.intItemId
LEFT JOIN trn_po_prn_details_sales_order ON ware_mrndetails.intOrderNo = trn_po_prn_details_sales_order.ORDER_NO
AND ware_mrndetails.intOrderYear = trn_po_prn_details_sales_order.ORDER_YEAR
AND ware_mrndetails.strStyleNo = trn_po_prn_details_sales_order.SALES_ORDER
AND ware_mrndetails.intItemId = trn_po_prn_details_sales_order.ITEM
INNER JOIN ware_mrnheader ON ware_mrndetails.intMrnNo = ware_mrnheader.intMrnNo
AND ware_mrndetails.intMrnYear = ware_mrnheader.intMrnYear
INNER JOIN mst_locations ON ware_mrnheader.intCompanyId = mst_locations.intId

WHERE
(
ware_mrndetails.intOrderNo > 0
AND (
ware_mrnheader.intStatus <= (
ware_mrnheader.intApproveLevels + 1
) && ware_mrnheader.intStatus > 1
) 
)
AND mst_locations.intCompanyId IN ($company)
GROUP BY
ware_mrndetails.intOrderNo,
ware_mrndetails.intOrderYear,
ware_mrndetails.strStyleNo,
ware_mrndetails.intItemId
HAVING
(
FIND_IN_SET('$company', FABRIC_IN_COMPANIES)
OR ORDER_COMPANY = '$company'
)) AS TB1 
GROUP BY
TB1.ORDER_NO,
TB1.ORDER_YEAR,
TB1.SALES_ORDER,
TB1.ITEM ) AS TB2
LEFT JOIN
trn_po_prn_details_sales_order_stock_transactions
ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = TB2.ORDER_NO AND
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = TB2.ORDER_YEAR AND
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = TB2.SALES_ORDER AND
trn_po_prn_details_sales_order_stock_transactions.ITEM = TB2.ITEM
INNER JOIN trn_orderheader
ON trn_orderheader.intOrderNo = TB2.ORDER_NO AND
trn_orderheader.intOrderYear = TB2.ORDER_YEAR AND
trn_orderheader.intStatus = 1
LEFT JOIN trn_po_prn_details_sales_order ON trn_po_prn_details_sales_order.ORDER_NO = TB2.ORDER_NO
AND trn_po_prn_details_sales_order.ORDER_YEAR = TB2.ORDER_YEAR
AND trn_po_prn_details_sales_order.SALES_ORDER = TB2.SALES_ORDER
AND trn_po_prn_details_sales_order.ITEM = TB2.ITEM
INNER JOIN trn_orderdetails
ON TB2.ORDER_NO = trn_orderdetails.intOrderNo
AND TB2.ORDER_YEAR = trn_orderdetails.intOrderYear
AND TB2.SALES_ORDER = trn_orderdetails.intSalesOrderId
INNER JOIN mst_item
ON TB2.ITEM = mst_item.intId
INNER JOIN mst_units
ON mst_item.intUOM = mst_units.intId
INNER JOIN mst_maincategory
ON mst_item.intMainCategory = mst_maincategory.intId
INNER JOIN mst_subcategory
ON mst_item.intSubCategory = mst_subcategory.intId
INNER JOIN item_rolling_stock ROL ON ROL.intItemId = TB2.ITEM and ROL.`year` = '$curYear'
GROUP BY
TB2.ORDER_NO,
TB2.ORDER_YEAR,
TB2.SALES_ORDER,
TB2.ITEM,
TB2.ORDER_COMPANY
) AS TB3 ";

        $sql .= " GROUP BY
TB3.ORDER_NO,
TB3.ORDER_YEAR,
TB3.ITEM )
as
tb
GROUP BY
tb.ORDER_NO,
tb.ORDER_YEAR,
tb.ITEM
) as TB_ALL
GROUP BY
TB_ALL.ITEM) as 	TB_ALL_1
WHERE
1 = 1  AND TB_ALL_1.ORDER_COMPANY = $company 
HAVING TB_ALL_1.rolQty > 0
ORDER BY  
TB_ALL_1.mainCatId,
TB_ALL_1.subCatId,
TB_ALL_1.ITEM
ASC";
        return $sql;

    }

    public function getHOStock($item,$head_office_loc)
    {

        global $db;

        $sql = "SELECT
			Sum(ware_stocktransactions_bulk.dblQty) as qty 
			FROM `ware_stocktransactions_bulk`
			WHERE
			ware_stocktransactions_bulk.intItemId = '$item' AND
			ware_stocktransactions_bulk.intLocationId = '$head_office_loc' 
			";

        $result = $db->RunQuery($sql);
        $row=mysqli_fetch_array($result);
        return $row['qty'];
    }




}
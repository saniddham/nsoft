<?php
 
class cls_marketer_revenue_set
{
	private $db;
 	
	function __construct($db)
	{
		$this->db = $db;
 	}

//BEGIN - PUBLIC FUNCTIONS {	
	public function set_marketer_revenue_old_data($techniqueId,$marketerId,$plant,$locationId,$year,$month,$day,$revenue,$HEAT_SEAL_STICKER_COST)
	{
		
		$sql 	= $this->set_marketer_revenue_old_sql($techniqueId,$marketerId,$plant,$locationId,$year,$month,$day,$revenue,$HEAT_SEAL_STICKER_COST);
		$result = $this->db->RunQuery($sql);
 		return $result;
	}
	public function delete_marketer_revenue_old_data($techniqueId,$marketerId,$plant, $locationId, $year,$month,$day)
	{
		
		$sql 	= $this->delete_marketer_revenue_old_sql($techniqueId,$marketerId,$plant,$locationId,$year,$month,$day);
		$result = $this->db->RunQuery($sql);
 		return $result;
	}
	
	private function delete_marketer_revenue_old_sql($techniqueId,$marketerId,$plant,$locationId,$year,$month,$day){
		
		$date		=$year.'-'.sprintf("%02d",$month).'-'.$day;
		$sql = "DELETE FROM `daily_mails_revenue_old`
				WHERE 
				daily_mails_revenue_old.PO_TECH_GROPU_NEW= $techniqueId AND
				daily_mails_revenue_old.PLANT_ID 	= $plant AND
				daily_mails_revenue_old.MARKETER_ID = '$marketerId' AND
				daily_mails_revenue_old.`DATE` 		= '$date' AND 
				daily_mails_revenue_old.`LOCATION_ID` = '$locationId'
				";
		return $sql;
	}
	private function set_marketer_revenue_old_sql($techniqueId,$marketerId,$plant,$location,$year,$month,$day,$revenue,$HEAT_SEAL_STICKER_COST){
		//PO_TECHNIQUE untill 2015-07-24 are not correct, so added PO_TECH_GROPU_NEW to save correct grp
		$date		=$year.'-'.sprintf("%02d",$month).'-'.$day;

		$sql = "INSERT INTO `daily_mails_revenue_old` (PLANT_ID, `LOCATION_ID`, `MARKETER_ID`,`DATE`,`PO_TECHNIQUE`,`PO_TECH_GROPU_NEW`,`REVENUE`,HEAT_SEAL_STICKER_COST) 
			VALUES ('$plant','$location','$marketerId','$date','$techniqueId','$techniqueId','$revenue','$HEAT_SEAL_STICKER_COST')";

		return $sql;
	}
	
 }

?>
<?php

 session_start();
 
include_once $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
include_once $_SESSION['ROOT_PATH']."class/cls_commonErrorHandeling_get.php";
  
$obj_commonErr		= new cls_commonErrorHandeling_get($db);
$obj_common			= new cls_commonFunctions_get($db);
 
class cls_marketer_revenue_get
{
	private $db;
 	
	function __construct($db)
	{
		$this->db 	= $db;
 	}

 	public function get_marketers_result($year,$month,$day,$plant,$cluster)
	{
 		$sql		= $this->load_marketers_sql($year,$month,$day,$plant,$cluster);
 		$result		= $this->db->RunQuery($sql);
 		return $result;
	}
 	public function get_marketers_and_targets_result($year,$month,$day,$plant,$cluster)
	{
 		$sql		= $this->load_marketers_and_targets_sql($year,$month,$day,$plant,$cluster);
 		$result		= $this->db->RunQuery($sql);
 		return $result;
	}
 	public function get_main_cluster_marketers_result($date,$cluster,$reportId,$month)
	{
 		$sql		= $this->load_main_cluster_marketers_sql($date,$cluster,$reportId,$month);
 		$result		= $this->db->RunQuery($sql);
 		return $result;
	}
	
 	public function get_plant_marketers_result($date,$plant,$reportId,$month)
	{
 		$sql		= $this->load_plant_marketers_sql($date,$plant,$reportId,$month);
 		$result		= $this->db->RunQuery($sql);
 		return $result;
	}
	
 	public function get_sub_cluster_marketers_result($date,$sub_cluster,$reportId)
	{
 		$sql		= $this->load_sub_cluster_marketers_sql($date,$sub_cluster,$reportId);
 		$result		= $this->db->RunQuery($sql);
 		return $result;
	}
 	public function get_cluster_marketers_result($date,$cluster)
	{
 		$sql		= $this->load_cluster_marketers_sql($date,$cluster);
 		$result		= $this->db->RunQuery($sql);
 		return $result;
	}
	public function getMarketerRevenue($techniqueId,$marketerId,$plant,$month,$year,$day,$toCurrency)
	{
 		$sqlp		= $this->load_MarketerRevenue_sql($techniqueId,$marketerId,$plant,$month,$year,$day,$toCurrency);
		$resultp	= $this->db->RunQuery($sqlp);
		$row 		= mysqli_fetch_array($resultp);
		return $row['REVENUE'];
 	}
	public function getMarketerRevenue_stock($month,$year,$day,$cmonth,$cyear,$cday)
	{
 		$sqlp		= $this->getMarketerRevenue_stock_sql($month,$year,$day,$cmonth,$cyear,$cday);
		$resultp	= $this->db->RunQuery($sqlp);
		return $resultp;
 	}
	public function get_old_cluster_plant_revenue($plant,$date,$toCurrency)
	{
 		 $TOTAL = $this->load_old_cluster_plant_revenue_sql($plant, $date, $toCurrency);
       // $resultp = $this->db->RunQuery($sqlp);
        //$row = mysqli_fetch_array($resultp);
                return $TOTAL;
 	}

    public function get_old_cluster_location_revenue($plant,$location,$date,$toCurrency)
    {
        $TOTAL = $this->load_old_cluster_location_revenue_sql($plant,$location, $date, $toCurrency);
        // $resultp = $this->db->RunQuery($sqlp);
        //$row = mysqli_fetch_array($resultp);
        return $TOTAL;
    }

    public function get_old_cluster_location_technique_revenue($technique,$plant,$location,$date,$toCurrency)
    {
        $sqlp		= $this->load_old_cluster_location_technique_revenue_sql($technique,$plant,$location,$date,$toCurrency);
        $resultp	= $this->db->RunQuery($sqlp);
        $row 		= mysqli_fetch_array($resultp);
        return $row['REVENUE'];
    }

	public function get_old_cluster_plant_technique_revenue($technique,$plant,$date,$toCurrency)
	{
 		$sqlp		= $this->load_old_cluster_plant_technique_revenue_sql($technique,$plant,$date,$toCurrency);
		$resultp	= $this->db->RunQuery($sqlp);
		$row 		= mysqli_fetch_array($resultp);
		return $row['REVENUE'];
 	}
	
	public function get_old_cluster_marketer_revenue($marketerId_list,$plant_list,$date,$toCurrency)
	{
 		$sqlp		= $this->load_old_cluster_marketer_revenue_sql($marketerId_list,$plant_list,$date,$toCurrency);
		$resultp	= $this->db->RunQuery($sqlp);
		$row 		= mysqli_fetch_array($resultp);
		return $row['REVENUE'];
 	}

    public function get_old_sub_cluster_marketer_revenue($marketerId_list,$plant_list,$date,$toCurrency)
    {
        $sqlp		= $this->load_old_sub_cluster_marketer_revenue_sql($marketerId_list,$plant_list,$date,$toCurrency);
        $resultp	= $this->db->RunQuery($sqlp);
        $row 		= mysqli_fetch_array($resultp);
        return $row['REVENUE'];
    }
	public function get_cluster_plants($cluster)
	{
 		$sqlp		= $this->load_cluster_plants_sql($cluster);
		$resultp	= $this->db->RunQuery($sqlp);
 		return $resultp;
 	}
	//2015-06-18
	public function get_main_cluster_plants($cluster,$year,$month)
	{
 		$sqlp		= $this->load_main_cluster_plants_sql($cluster,$year,$month);
		$resultp	= $this->db->RunQuery($sqlp);
 		return $resultp;
 	}
    public function get_main_cluster_locations($cluster,$year,$month)
    {
        $sqlp		= $this->load_cluster_locations_sql($cluster,$year,$month);
        $resultp	= $this->db->RunQuery($sqlp);
        return $resultp;
    }

	public function get_sub_cluster_plants($sub_cluster,$year)
	{
 		$sqlp		= $this->load_sub_cluster_plants_sql($sub_cluster,$year);
		$resultp	= $this->db->RunQuery($sqlp);
 		return $resultp;
 	}

	public function get_given_plants($plant,$year,$month)
	{
 		$sqlp		= $this->load_plants_sql($plant,$year,$month);
		$resultp	= $this->db->RunQuery($sqlp);
 		return $resultp;
 	}


    public function get_given_locations($plant,$year,$month)
    {
        $sqlp		= $this->load_locations_sql($plant,$year,$month);
        $resultp	= $this->db->RunQuery($sqlp);
        return $resultp;
    }
	
	public function get_plant_calender($plant,$date)
	{
 		$sqlp		= $this->load_plant_calender_sql($plant,$date);
		$resultp	= $this->db->RunQuery($sqlp);
 		return $resultp;
 	}
	
	
	public function xx($techniqueId,$marketerId,$plant,$month,$year,$day,$toCurrency)	{
		$resultp	= $this->db->RunQuery("select 1 as t ");
		$row 		= mysqli_fetch_array($resultp);
		return $row['t'];
		
	}
  
//BEGIN - PRIVATE FUNCTIONS {	
	private function load_marketers_sql($year,$month,$day,$plant,$cluster)
	{
 		if($month==12){
			$yearN		=$year+1;	
			$monthN		=1;
		}
		else{
			$yearN		=$year;	
			$monthN		=$month+1;	
		}
		$minDate		=$year.'-'.sprintf("%02d",$month).'-01';
		$maxDate		=$yearN.'-'.sprintf("%02d",$monthN).'-01';
	  	 
		   $sql = "SELECT * FROM (SELECT
					mst_marketer.intUserId,
					sys_users.strFullName,
					mst_marketer_cluster_target.ANUAL_TARGET as dblAnualaTarget,
					mst_marketer_cluster_target.MONTHLY_TARGET as dblMonthlyTarget , 
					(SELECT
					Sum(daily_mails_revenue_old.REVENUE)
					FROM
					daily_mails_revenue_old  
 					WHERE 
					daily_mails_revenue_old.PLANT_ID = $plant AND 
					daily_mails_revenue_old.MARKETER_ID = mst_marketer.intUserId  AND 
					daily_mails_revenue_old.DATE >= $minDate AND 
					daily_mails_revenue_old.DATE < $maxDate  
					) as TOT_REVENUE
 					FROM
					mst_marketer
					INNER JOIN sys_users ON mst_marketer.intUserId = sys_users.intUserId 
 					LEFT JOIN mst_marketer_cluster_target ON mst_marketer.intUserId = mst_marketer_cluster_target.MARKETER_ID 
					AND mst_marketer_cluster_target.YEAR='$year'  AND mst_marketer_cluster_target.CLUSTER_ID='$cluster'
 					WHERE 
					/*sys_users.intStatus = 1 AND */
					FIND_IN_SET('$cluster',mst_marketer.REVENUE_TYPES)
 					) AS TB1 
					ORDER BY 
					TB1.TOT_REVENUE DESC,
					TB1.strFullName ASC
			";
  		return $sql;
 	}

//2015-06-18
	private function load_marketers_and_targets_sql($year,$month,$day,$plant,$cluster)
	{
 		if($month==12)
		{
			$yearN		=$year+1;	
			$monthN		=1;
		}
		else{
			$yearN		=$year;	
			$monthN		=$month+1;	
		}
		$minDate		=$year.'-'.sprintf("%02d",$month).'-01';
		$maxDate		=$yearN.'-'.sprintf("%02d",$monthN).'-01';
	  	 
		   $sql = "SELECT * FROM (SELECT
					mst_marketer.intUserId,
					sys_users.strFullName 
 					FROM
					mst_marketer
					INNER JOIN sys_users ON mst_marketer.intUserId = sys_users.intUserId 
 					INNER JOIN mst_marketer_cluster ON mst_marketer.intUserId = mst_marketer_cluster.MARKETER
 					WHERE 
					 mst_marketer_cluster.CLUSTER='$cluster' AND mst_marketer_cluster.HIDE_FROM_REVENUE_REPORT <> 1
					/* mst_marketer_cluster.CLUSTER='$cluster' */
					/*sys_users.intStatus = 1 AND */
					/*FIND_IN_SET('$cluster',mst_marketer.REVENUE_TYPES)*/
 					) AS TB1 
					ORDER BY
					TB1.strFullName ASC
			";
  		return $sql;
 	}

	
//BEGIN - PRIVATE FUNCTIONS {	
	private function load_cluster_marketers_sql($date,$cluster)
	{
 		list($year, $month, $day) = explode('-', $date);
		
		if($month==12){
			$yearN		=$year+1;	
			$monthN		=1;
		}
		else{
			$yearN		=$year;	
			$monthN		=$month+1;	
		}
		$minDate		=$year.'-'.sprintf("%02d",$month).'-01';
		$maxDate		=$yearN.'-'.sprintf("%02d",$monthN).'-01';
	  	 
		   $sql = "SELECT * FROM (SELECT 
 					mst_marketer.intUserId,
					sys_users.strFullName,
					IF(((mst_marketer.OTHER_CATEGORY_FLAG=1) || (mst_marketer.STATUS <> 1)),'Others',sys_users.strUserName) AS marketerName,
					IF(((mst_marketer.OTHER_CATEGORY_FLAG=1) || (mst_marketer.STATUS <> 1)),2,1) as orderId,
					IF(((mst_marketer.OTHER_CATEGORY_FLAG=1) || (mst_marketer.STATUS <> 1)),1,mst_marketer.OTHER_CATEGORY_FLAG) AS OTHER_CATEGORY_FLAG,
					mst_marketer_cluster_target.ANUAL_TARGET,
					mst_marketer_cluster_target.MONTHLY_TARGET , 
					(mst_marketer_cluster_target.MONTHLY_TARGET/26) as dayTarget,
					(SELECT GROUP_CONCAT(mst_plant.intPlantId) as plantList FROM `mst_plant` WHERE mst_plant.MAIN_CLUSTER_ID = '$cluster') as plant_list,
					(SELECT
					Sum(daily_mails_revenue_old.REVENUE)
					FROM
					daily_mails_revenue_old  
 					WHERE 
					FIND_IN_SET(daily_mails_revenue_old.PLANT_ID,(SELECT GROUP_CONCAT(mst_plant.intPlantId) as plantList FROM `mst_plant` WHERE mst_plant.REVENUE_TYPE = '$cluster')) AND 
					daily_mails_revenue_old.MARKETER_ID = mst_marketer.intUserId  AND 
					daily_mails_revenue_old.DATE >= '$minDate' AND 
					daily_mails_revenue_old.DATE < '$maxDate' 
					) as TOT_REVENUE
 					FROM
					mst_marketer
					INNER JOIN sys_users ON mst_marketer.intUserId = sys_users.intUserId 
 					LEFT JOIN mst_marketer_cluster_target ON mst_marketer.intUserId = mst_marketer_cluster_target.MARKETER_ID 
					AND mst_marketer_cluster_target.YEAR='$year'  AND mst_marketer_cluster_target.CLUSTER_ID='$cluster'
 					WHERE  
					/*mst_marketer_cluster_target.OTHER_CATEGORY_FLAT <>1 AND*/ 
					/*sys_users.intStatus = 1 AND */
					FIND_IN_SET('$cluster',mst_marketer.REVENUE_TYPES)
 					) AS TB1 
					ORDER BY 
					TB1.orderId ASC,
					TB1.TOT_REVENUE DESC,
					TB1.marketerName ASC
			";
  		return $sql;
 	}
 //2015-06-19
	private function load_main_cluster_marketers_sql($date,$cluster,$reportId,$month) //suvini
	{   
	
	
 		list($year, $month, $day) = explode('-', $date);
		
		if($month==12){
			$yearN		=$year+1;	
			$monthN		=1;
		}
		else{
			$yearN		=$year;	
			$monthN		=$month+1;	
		}
		$minDate		=$year.'-'.sprintf("%02d",$month).'-01';
		$maxDate		=$yearN.'-'.sprintf("%02d",$monthN).'-01';
		$yearMax 		=$year+1;
		$yearMin 		=$year-1;
		$minDate1		=$yearMin.'-'.sprintf("%02d",4).'-01';
		$maxDate1		=$year.'-'.sprintf("%02d",4).'-01';
		$minDate2		=$year.'-'.sprintf("%02d",4).'-01';
		$maxDate2		=$yearMax.'-'.sprintf("%02d",4).'-01';
	  	 
		   $sql = "SELECT * FROM (SELECT 
					if(mst_marketer.REPLACEMENT_FOR >0,(concat(mst_marketer.intUserId,',',mst_marketer.REPLACEMENT_FOR)),mst_marketer.intUserId) as intUserId, 
					mst_marketer.intUserId as currentMarketer,
					sys_users.strFullName,
					IF(((mst_marketer.OTHER_CATEGORY_FLAG=1) || (mst_marketer.STATUS <> 1)),'Others',sys_users.strUserName) AS marketerName,
					IF(((mst_marketer.OTHER_CATEGORY_FLAG=1) || (mst_marketer.STATUS <> 1)),2,1) as orderId,
					IF(((mst_marketer.OTHER_CATEGORY_FLAG=1) || (mst_marketer.STATUS <> 1)),1,mst_marketer.OTHER_CATEGORY_FLAG) AS OTHER_CATEGORY_FLAG,
					mst_marketer_monthly_targets.`MONTH`,
					mst_marketer_monthly_targets.TARGET_MONTHLY, 
					(mst_marketer_monthly_targets.TARGET_MONTHLY/26) as dayTarget,
                     mst_month.strMonth,
					 
					 /*(select sum(annual.TARGET_MONTHLY) 
            FROM mst_marketer_monthly_targets as annual 
            where  annual.`YEAR` = '$year' 
			AND annual.MARKETER_ID=mst_marketer_monthly_targets.MARKETER_ID
        			 ) AS TARGET_ANNUALY,*/
					 
					 ( select sum(annual.TARGET_MONTHLY) 
            FROM mst_marketer_monthly_targets as annual
			where  
			annual.MARKETER_ID=mst_marketer_monthly_targets.MARKETER_ID"
			;
			 if($month<=3)
			 {
			/* $sql.=" AND  $minDate1 <=
			CONCAT(annual.`YEAR`,'-',annual.MONTH,'-',01) < $maxDate1";*/
			$sql.=" AND  annual.STATUS=0 ";	 
			 }else
			 {
			$sql.=" AND  annual.STATUS=0 ";	 
			//$sql.=" AND $minDate2 <= CONCAT(annual.`YEAR`,'-',annual.MONTH,'-',01) < $maxDate2";
			 }
                
		   $sql.=")TARGET_ANNUALY,";
					 
					 
					 
				$sql.="	(SELECT GROUP_CONCAT(mst_plant.intPlantId) as plantList FROM `mst_plant` WHERE mst_plant.MAIN_CLUSTER_ID = '$cluster') as plant_list,
					(SELECT
					Sum(daily_mails_revenue_old.REVENUE)
					FROM
					daily_mails_revenue_old  
 					WHERE 
					FIND_IN_SET(daily_mails_revenue_old.PLANT_ID,(SELECT GROUP_CONCAT(mst_plant.intPlantId) as plantList FROM `mst_plant` WHERE mst_plant.MAIN_CLUSTER_ID = '$cluster')) AND 
					daily_mails_revenue_old.MARKETER_ID = mst_marketer.intUserId  AND 
					daily_mails_revenue_old.DATE >= '$minDate' AND 
					daily_mails_revenue_old.DATE < '$maxDate' 
					) as TOT_REVENUE
 					FROM
					mst_marketer 
					INNER JOIN mst_marketer_cluster ON mst_marketer.intUserId = mst_marketer_cluster.MARKETER AND mst_marketer_cluster.CLUSTER = '$cluster'
					INNER JOIN sys_users ON mst_marketer.intUserId = sys_users.intUserId 
 					left join mst_marketer_monthly_targets ON mst_marketer.intUserId = mst_marketer_monthly_targets.MARKETER_ID
					AND mst_marketer_monthly_targets.YEAR='$year'  AND mst_marketer_monthly_targets.CLUSTER_ID='$cluster' 
					AND mst_marketer_monthly_targets.REPORT_ID='$reportId' AND  mst_marketer_monthly_targets.MONTH='$month'
					
         			LEFT JOIN mst_month ON mst_marketer_monthly_targets.`MONTH` = mst_month.intMonthId
 					WHERE 
					mst_marketer_cluster.HIDE_FROM_REVENUE_REPORT <> 1 AND  
					mst_marketer_cluster.CLUSTER = '$cluster'  AND 
				 	mst_marketer.REPLACED_FLAG IS NULL 
					/*mst_marketer_main_cluster_target.OTHER_CATEGORY_FLAT <>1 AND*/ 
					/*sys_users.intStatus = 1 AND */
					/*FIND_IN_SET('$cluster',mst_marketer.REVENUE_TYPES)*/
 					) AS TB1 
					ORDER BY 
					TB1.orderId ASC,
					TB1.TOT_REVENUE DESC,
					TB1.marketerName ASC
			";
	
  		return $sql;
 	}
 
	private function load_plant_marketers_sql($date,$plant,$reportId,$month) //suvini
	{
		
		$cluster	=1;
 		list($year, $month, $day) = explode('-', $date);
		
		if($month==12){
			$yearN		=$year+1;	
			$monthN		=1;
		}
		else{
			$yearN		=$year;	
			$monthN		=$month+1;	
		}
		$minDate		=$year.'-'.sprintf("%02d",$month).'-01';
		$maxDate		=$yearN.'-'.sprintf("%02d",$monthN).'-01';
		
		$yearMax 		= $year+1;
		$yearMin 		= $year-1;
		$minDate1		=$yearMin.'-'.sprintf("%02d",4).'-01';
		$maxDate1		=$year.'-'.sprintf("%02d",4).'-01';
		$minDate2		=$year.'-'.sprintf("%02d",4).'-01';
		$maxDate2		=$yearMax.'-'.sprintf("%02d",4).'-01';
	  	 
		   $sql = "SELECT * FROM (SELECT 
					if(mst_marketer.REPLACEMENT_FOR >0,(concat(mst_marketer.intUserId,',',mst_marketer.REPLACEMENT_FOR)),mst_marketer.intUserId) as intUserId, 
					mst_marketer.intUserId as currentMarketer,
					sys_users.strFullName,
					IF(((mst_marketer.OTHER_CATEGORY_FLAG=1) || (mst_marketer.STATUS <> 1)),'Others',sys_users.strUserName) AS marketerName,
					IF(((mst_marketer.OTHER_CATEGORY_FLAG=1) || (mst_marketer.STATUS <> 1)),2,1) as orderId,
					IF(((mst_marketer.OTHER_CATEGORY_FLAG=1) || (mst_marketer.STATUS <> 1)),1,mst_marketer.OTHER_CATEGORY_FLAG) AS OTHER_CATEGORY_FLAG,
					mst_marketer_monthly_targets.MONTH,
					 mst_month.strMonth,
					mst_marketer_monthly_targets.TARGET_MONTHLY , 
					(mst_marketer_monthly_targets.TARGET_MONTHLY/26) as dayTarget,
					/*(select sum(annual.TARGET_MONTHLY) 
            FROM mst_marketer_monthly_targets as annual 
            where  annual.`YEAR` = '$year' 
			AND annual.MARKETER_ID=mst_marketer_monthly_targets.MARKETER_ID
        			 ) AS TARGET_ANNUALY,*/
					 
					  ( select sum(annual.TARGET_MONTHLY) 
            FROM mst_marketer_monthly_targets as annual
			where  
			annual.MARKETER_ID=mst_marketer_monthly_targets.MARKETER_ID"
			;
			 if($month<=3)
			 {
			/* $sql.=" AND  $minDate1 <=
			CONCAT(annual.`YEAR`,'-',annual.MONTH,'-',01) < $maxDate1";*/
			$sql.=" AND  annual.STATUS=0 ";	 

			 }else
			 {
			//$sql.=" AND $minDate2 <= CONCAT(annual.`YEAR`,'-',annual.MONTH,'-',01) < $maxDate2";
			$sql.=" AND  annual.STATUS=0 ";	 
			 }
                
		   $sql.=")TARGET_ANNUALY,";
		   
			$sql.="(SELECT GROUP_CONCAT(mst_plant.intPlantId) as plantList FROM `mst_plant` WHERE mst_plant.intPlantId = '$plant') as plant_list,
					(SELECT
					Sum(daily_mails_revenue_old.REVENUE)
					FROM
					daily_mails_revenue_old  
 					WHERE 
					FIND_IN_SET(daily_mails_revenue_old.PLANT_ID,(SELECT GROUP_CONCAT(mst_plant.intPlantId) as plantList FROM `mst_plant` WHERE mst_plant.intPlantId = '$plant')) AND 
					daily_mails_revenue_old.MARKETER_ID = mst_marketer.intUserId  AND 
					daily_mails_revenue_old.DATE >= '$minDate' AND 
					daily_mails_revenue_old.DATE < '$maxDate' 
					) as TOT_REVENUE
 					FROM
					mst_marketer 
					INNER JOIN mst_marketer_cluster ON mst_marketer.intUserId = mst_marketer_cluster.MARKETER AND mst_marketer_cluster.CLUSTER = '$cluster'
					INNER JOIN sys_users ON mst_marketer.intUserId = sys_users.intUserId 
					
 					LEFT JOIN mst_marketer_monthly_targets ON mst_marketer.intUserId = mst_marketer_monthly_targets.MARKETER_ID 
					AND mst_marketer_monthly_targets.YEAR ='$year'  AND mst_marketer_monthly_targets.CLUSTER_ID='$cluster' 
					AND mst_marketer_monthly_targets.REPORT_ID='$reportId' AND mst_marketer_monthly_targets.MONTH ='$month'
					LEFT JOIN mst_month ON mst_marketer_monthly_targets.`MONTH` = mst_month.intMonthId
 					WHERE 
					mst_marketer_cluster.HIDE_FROM_REVENUE_REPORT <> 1 AND  
					mst_marketer_cluster.CLUSTER = '$cluster'  AND 
				 	mst_marketer.REPLACED_FLAG IS NULL 
					/*mst_marketer_monthly_targets.OTHER_CATEGORY_FLAT <>1 AND*/ 
					/*sys_users.intStatus = 1 AND */
					/*FIND_IN_SET('$cluster',mst_marketer.REVENUE_TYPES)*/
 					) AS TB1 
					ORDER BY 
					TB1.orderId ASC,
					TB1.TOT_REVENUE DESC,
					TB1.marketerName ASC
			";
			
  		return $sql;
 	}
	
	private function load_sub_cluster_marketers_sql($date,$sub_cluster,$reportId)
	{
 		list($year, $month, $day) = explode('-', $date);
		
		if($month==12){
			$yearN		=$year+1;	
			$monthN		=1;
		}
		else{
			$yearN		=$year;	
			$monthN		=$month+1;	
		}
		$minDate		=$year.'-'.sprintf("%02d",$month).'-01';
		$maxDate		=$yearN.'-'.sprintf("%02d",$monthN).'-01';
	  	 
		   $sql = "SELECT * FROM (SELECT 
 					mst_marketer.intUserId,
					sys_users.strFullName,
					IF(((mst_marketer.OTHER_CATEGORY_FLAG=1) || (mst_marketer.STATUS <> 1)),'Others',sys_users.strUserName) AS marketerName,
					IF(((mst_marketer.OTHER_CATEGORY_FLAG=1) || (mst_marketer.STATUS <> 1)),2,1) as orderId,
					IF(((mst_marketer.OTHER_CATEGORY_FLAG=1) || (mst_marketer.STATUS <> 1)),1,mst_marketer.OTHER_CATEGORY_FLAG) AS OTHER_CATEGORY_FLAG,
					mst_marketer_sub_cluster_target.TARGET_ANUAL AS ANUAL_TARGET,
					mst_marketer_sub_cluster_target.TARGET_MONTHLY AS MONTHLY_TARGET , 
					(mst_marketer_sub_cluster_target.TARGET_MONTHLY/26) as dayTarget,
					(SELECT GROUP_CONCAT(mst_plant.intPlantId) as plantList FROM `mst_plant` WHERE mst_plant.SUB_CLUSTER_ID = '$sub_cluster') as plant_list,
					(SELECT
					Sum(daily_mails_revenue_old.REVENUE)
					FROM
					daily_mails_revenue_old  
 					WHERE 
					FIND_IN_SET(daily_mails_revenue_old.PLANT_ID,(SELECT GROUP_CONCAT(mst_plant.intPlantId) as plantList FROM `mst_plant` WHERE mst_plant.SUB_CLUSTER_ID = '$sub_cluster')) AND 
					daily_mails_revenue_old.MARKETER_ID = mst_marketer.intUserId  AND 
					daily_mails_revenue_old.DATE >= '$minDate' AND 
					daily_mails_revenue_old.DATE < '$maxDate' 
					) as TOT_REVENUE
 					FROM
					mst_marketer 
					INNER JOIN mst_marketer_sub_cluster ON mst_marketer.intUserId = mst_marketer_sub_cluster.MARKETER AND mst_marketer_sub_cluster.SUB_CLUSTER = '$sub_cluster'
					INNER JOIN sys_users ON mst_marketer.intUserId = sys_users.intUserId 
 					LEFT JOIN mst_marketer_sub_cluster_target ON mst_marketer.intUserId = mst_marketer_sub_cluster_target.MARKETER_ID 
					AND mst_marketer_sub_cluster_target.YEAR='$year'  AND mst_marketer_sub_cluster_target.SUB_CLUSTER_ID='$sub_cluster' 
					AND mst_marketer_sub_cluster_target.REPORT_ID='$reportId' 
 					WHERE 
					mst_marketer_sub_cluster.HIDE_FROM_REVENUE_REPORT <> 1 AND 
					mst_marketer_sub_cluster.SUB_CLUSTER = '$sub_cluster'
					/*mst_marketer_SUB_cluster_target.OTHER_CATEGORY_FLAT <>1 AND*/ 
					/*sys_users.intStatus = 1 AND */
					/*FIND_IN_SET('$cluster',mst_marketer.REVENUE_TYPES)*/
 					) AS TB1 
					ORDER BY 
					TB1.orderId ASC,
					TB1.TOT_REVENUE DESC,
					TB1.marketerName ASC
			";
		
  		return $sql;
 	}
 
 	private function load_MarketerRevenue_sql($month,$year,$day){
		
		$date		=$year.'-'.sprintf("%02d",$month).'-'.$day;
 		
		$sql = "select 
				IFNULL(sum(tb1.Qty1),0) as REVENUE
				from (SELECT 
				Sum(STF.dblQty*-1*trn_orderdetails.dblPrice) AS Qty1 
				FROM
				(select * from ware_stocktransactions_fabric where date(ware_stocktransactions_fabric.dtDate) 	= '$date' AND
				ware_stocktransactions_fabric.strType = 'Dispatched_G') as STF
				inner JOIN mst_locations ON STF.intLocationId = mst_locations.intId
				inner JOIN mst_plant ON mst_locations.intPlant = mst_plant.intPlantId
				inner JOIN trn_orderheader ON STF.intOrderNo = trn_orderheader.intOrderNo AND STF.intOrderYear = trn_orderheader.intOrderYear
				inner JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear AND STF.intSalesOrderId = trn_orderdetails.intSalesOrderId
				inner JOIN mst_marketer ON trn_orderheader.intMarketer = mst_marketer.intUserId
				WHERE  
				trn_orderdetails.TECHNIQUE_GROUP_ID = $techniqueId AND 
				mst_locations.intPlant = $plant AND
				/*mst_plant.REVENUE_TYPE='$revenueType' AND */
				mst_marketer.intUserId 						IN  ($marketerId)				 AND
				date(STF.dtDate) 	= '$date' AND
				STF.strType 		= 'Dispatched_G'   
				
 				)
				 as tb1  
				";	
			
			return $sql;
	}
	//2015-07-27
	function getMarketerRevenue_stock_sql($month, $year, $day,$cmonth, $cyear, $cday) {

        $fromDate= $year . '-' . sprintf("%02d", $month) . '-' . $day." 06:00:00";
        $toDate = $cyear . '-' . sprintf("%02d", $cmonth) . '-' . $cday ." 05:59:59";
        $sql = " SELECT
	
	sum(
		ifnull(
			(
				STF.dblQty *- 1 * trn_orderdetails.dblPrice
			),
			0
		)
	) AS REVENUE,
	sum(

		IF (
			(
				costing_sample_routing_process.PROCESS_ID = 14 AND trn_orderdetails.SO_TYPE=0
			),
			STF.dblQty *- 1 * (trn_orderdetails.dblPrice-costing_sample_routing_process.COST),
			0
		)
	) AS sticker_pressing_cost,
	mst_locations.intPlant,
	mst_locations.intId ,
	mst_marketer.intUserId,
	trn_orderdetails.TECHNIQUE_GROUP_ID,
	trn_orderdetails.intSampleNo,
	trn_orderdetails.intSampleYear
	
FROM
	(
		SELECT
			*
		FROM
			ware_stocktransactions_fabric
		WHERE
			(ware_stocktransactions_fabric.dtDate BETWEEN  '$fromDate' AND '$toDate')
		AND ware_stocktransactions_fabric.strType = 'Dispatched_G'
	) AS STF
            INNER JOIN mst_locations ON STF.intLocationId = mst_locations.intId
            INNER JOIN mst_plant ON mst_locations.intPlant = mst_plant.intPlantId
            INNER JOIN trn_orderheader ON STF.intOrderNo = trn_orderheader.intOrderNo
            AND STF.intOrderYear = trn_orderheader.intOrderYear
            INNER JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo
            AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
            AND STF.intSalesOrderId = trn_orderdetails.intSalesOrderId
            INNER JOIN mst_marketer ON trn_orderheader.intMarketer = mst_marketer.intUserId
            LEFT JOIN costing_sample_routing_process ON trn_orderdetails.intSampleNo = costing_sample_routing_process.SAMPLE_NO
            AND costing_sample_routing_process.SAMPLE_YEAR = trn_orderdetails.intSampleYear
            AND costing_sample_routing_process.REVISION = trn_orderdetails.intRevisionNo
            AND costing_sample_routing_process.PRINT = trn_orderdetails.strPrintName
            AND costing_sample_routing_process.COMBO = trn_orderdetails.strCombo
            AND costing_sample_routing_process.PROCESS_ID = 14
            WHERE
                    (STF.dtDate BETWEEN  '$fromDate' AND '$toDate')
            AND STF.strType = 'Dispatched_G'
            GROUP BY
                    mst_locations.intPlant,
                    mst_locations.intId,
                    mst_marketer.intUserId,
                    trn_orderdetails.TECHNIQUE_GROUP_ID ";

        return $sql;
    }

	
 	private function load_old_cluster_plant_revenue_sql($plant, $date, $toCurrency) {

        $sqlp = "SELECT
			Sum(daily_mails_revenue_old.REVENUE-daily_mails_revenue_old.HEAT_SEAL_STICKER_COST) AS REVENUE
			FROM `daily_mails_revenue_old`
			WHERE 
			daily_mails_revenue_old.PLANT_ID = '$plant' AND
			daily_mails_revenue_old.DATE = '$date'";
                        if($plant==14) //HEAT SEAL
                        {
                           $HEATSEAL_STICKER_COST =  $this->load_heat_seal_sticker_cost($date);
                        }else
                        {
                           $HEATSEAL_STICKER_COST  = 0;
                        }
                        
                        $resultp            = $this->db->RunQuery($sqlp);
                        $row                = mysqli_fetch_array($resultp);
                        $REVENUE            =  $row['REVENUE'];
                        $REVENUE_SUM        = $REVENUE+ $HEATSEAL_STICKER_COST;
        
                        return $REVENUE_SUM ;
    }

    private function load_old_cluster_location_revenue_sql($plant,$location, $date, $toCurrency) {

        $sqlp = "SELECT
			Sum(daily_mails_revenue_old.REVENUE-daily_mails_revenue_old.HEAT_SEAL_STICKER_COST) AS REVENUE
			FROM `daily_mails_revenue_old`
			WHERE 
			daily_mails_revenue_old.PLANT_ID = '$plant' AND
			daily_mails_revenue_old.DATE = '$date' and daily_mails_revenue_old.LOCATION_ID='$location'";
        if ($location==252) //HEAT SEAL
        {
            $HEATSEAL_STICKER_COST =  $this->load_heat_seal_sticker_cost($date);
        }else
        {
            $HEATSEAL_STICKER_COST  = 0;
        }

        $resultp            = $this->db->RunQuery($sqlp);
        $row                = mysqli_fetch_array($resultp);
        $REVENUE            =  $row['REVENUE'];
        $REVENUE_SUM        = $REVENUE+ $HEATSEAL_STICKER_COST;

        return $REVENUE_SUM ;
    }
     function load_heat_seal_sticker_cost($date)
    {
       $sql = "SELECT
			Sum(daily_mails_revenue_old.HEAT_SEAL_STICKER_COST) AS HEATSEAL_STICKER_COST
			FROM `daily_mails_revenue_old`
			WHERE 
			
			daily_mails_revenue_old.DATE = '$date'"; 
       
                        $result = $this->db->RunQuery($sql);
                        $row = mysqli_fetch_array($result);
                        //return $row['HEATSEAL_STICKER_COST']; 
                        if($row['HEATSEAL_STICKER_COST']== '' || $row['HEATSEAL_STICKER_COST']==0)
                        {
                           return 0;
                        } else 
                            
                        {
                            return $row['HEATSEAL_STICKER_COST']; 
                        }
                            
       
        
    }
 	private function load_old_cluster_plant_technique_revenue_sql($technique,$plant,$date,$toCurrency){
		
 	$sql = "SELECT
			Sum(daily_mails_revenue_old.REVENUE) AS REVENUE
			FROM `daily_mails_revenue_old`
			WHERE 
			daily_mails_revenue_old.PO_TECH_GROPU_NEW = '$technique' AND
			daily_mails_revenue_old.PLANT_ID = '$plant' AND
			daily_mails_revenue_old.DATE = '$date'";
				
			
			return $sql;
	}

    private function load_old_cluster_location_technique_revenue_sql($technique,$plant, $location, $date,$toCurrency){

        $sql = "SELECT
			Sum(daily_mails_revenue_old.REVENUE) AS REVENUE
			FROM `daily_mails_revenue_old`
			WHERE 
			daily_mails_revenue_old.PO_TECH_GROPU_NEW = '$technique' AND
			daily_mails_revenue_old.PLANT_ID = '$plant' AND
			daily_mails_revenue_old.LOCATION_ID = '$location' AND
			daily_mails_revenue_old.DATE = '$date'";


        return $sql;
    }
	
 	private function load_old_cluster_marketer_revenue_sql($marketerId_list,$plant_list,$date,$toCurrency){


 		$sql = "SELECT
			Sum(if(daily_mails_revenue_old.PLANT_ID != 18,daily_mails_revenue_old.REVENUE,daily_mails_revenue_old.HEAT_SEAL_STICKER_COST)) AS REVENUE
			FROM `daily_mails_revenue_old`
			WHERE
			FIND_IN_SET(daily_mails_revenue_old.MARKETER_ID,'$marketerId_list') AND
			FIND_IN_SET(daily_mails_revenue_old.PLANT_ID,'$plant_list') AND
			daily_mails_revenue_old.DATE = '$date'";	
			
			return $sql;
	}

    private function load_old_sub_cluster_marketer_revenue_sql($marketerId_list,$plant_list,$date,$toCurrency){


        $sql = "SELECT
			Sum(if(daily_mails_revenue_old.HEAT_SEAL_STICKER_COST = 0,daily_mails_revenue_old.REVENUE,daily_mails_revenue_old.REVENUE-daily_mails_revenue_old.HEAT_SEAL_STICKER_COST)) AS REVENUE
			FROM `daily_mails_revenue_old`
			WHERE
			FIND_IN_SET(daily_mails_revenue_old.MARKETER_ID,'$marketerId_list') AND
			FIND_IN_SET(daily_mails_revenue_old.PLANT_ID,'$plant_list') AND
			daily_mails_revenue_old.DATE = '$date'";

        return $sql;
    }

	private function load_cluster_plants_sql($cluster){
		
		$sql = "SELECT
			mst_plant.intPlantId,
			mst_plant.strPlantName,
			mst_plant.dblAnualaTarget,
			mst_plant.dblMonthlyTarget,
			mst_plant.dblDayTarget
			FROM `mst_plant`
			where mst_plant.intStatus=1 
			AND mst_plant.REVENUE_TYPE='$cluster'
			ORDER BY
			mst_plant.intPlantId ASC";
			
			return $sql;
	}
	
	private function load_main_cluster_plants_sql($cluster,$year,$month){//suvini
	
	
	
	$yearMax = $year+1;
	$yearMin = $year-1;
	$minDate		=$yearMin.'-'.sprintf("%02d",4).'-01';
	$maxDate		=$year.'-'.sprintf("%02d",4).'-01';
	$minDate1		=$year.'-'.sprintf("%02d",4).'-01';
	$maxDate1		=$yearMax.'-'.sprintf("%02d",4).'-01';
	
	
	
	
		
		$sql = "SELECT 
				mst_plant.intPlantId, 
				mst_plant_monthly_targets.MONTH,
				mst_plant_monthly_targets.TARGET_MONTHLY,
				mst_plant_monthly_targets.TARGET_DAILY,
				mst_plant.strPlantName,
				mst_plant.strPlantHeadName,
                mst_month.strMonth,
				mst_plant.strPlantHeadContactNo,
				( select sum(annual.TARGET_MONTHLY) 
            FROM mst_plant_monthly_targets as annual
			where  
			annual.PLANT_ID=mst_plant_monthly_targets.PLANT_ID"
			;
			 if($month<=3)
			 {
			/* $sql.=" AND  $minDate <=
			CONCAT(annual.`YEAR`,'-',annual.MONTH,'-',01) < $maxDate"; */
			$sql.=" AND  annual.STATUS=0 ";	 
			 }else
			 {
				$sql.=" AND  annual.STATUS=0 ";
			//$sql.=" AND $minDate1 <= CONCAT(annual.`YEAR`,'-',annual.MONTH,'-',01) < $maxDate1";
			 }
                
		   $sql.=")TARGET_ANNUALY";
				 
				$sql.=" FROM
				mst_plant
       
				LEFT JOIN mst_plant_monthly_targets ON mst_plant.intPlantId = mst_plant_monthly_targets.PLANT_ID  AND
				mst_plant_monthly_targets.`YEAR` = '$year' AND 
				mst_plant_monthly_targets.MONTH  = '$month' 
                left JOIN mst_month on mst_month.intMonthId = mst_plant_monthly_targets.`MONTH`
				WHERE 
				mst_plant.intStatus = 1 AND 
				mst_plant.MAIN_CLUSTER_ID = '$cluster'
			ORDER BY
			mst_plant.intPlantId ASC";
			
			return $sql;
	}

    private function load_cluster_locations_sql($cluster,$year,$month)
    {
        $sql = "SELECT 
				mst_plant.intPlantId, 
				mst_locations.intId,
				mst_month.strMonth, 
				mst_location_monthly_targets.MONTH,
				mst_location_monthly_targets.TARGET_MONTHLY,
				mst_location_monthly_targets.TARGET_DAILY,
				mst_locations.strName,
				mst_plant.strPlantHeadName,
				mst_plant.strPlantHeadContactNo,(select sum(annual.TARGET_MONTHLY) 
                FROM mst_location_monthly_targets as annual
			    where  
			    annual.LOCATION_ID=mst_location_monthly_targets.LOCATION_ID AND  annual.STATUS=0 ) as TARGET_ANNUALY
				FROM mst_locations left join mst_plant on mst_locations.intPlant = mst_plant.intPlantId
				LEFT JOIN mst_location_monthly_targets ON mst_locations.intId = mst_location_monthly_targets.LOCATION_ID  AND mst_location_monthly_targets.YEAR = '$year'  AND mst_location_monthly_targets.MONTH = '$month'
				LEFT JOIN mst_month on mst_month.intMonthId = mst_location_monthly_targets.`MONTH`
				WHERE 
				mst_locations.intStatus = 1 AND  mst_locations.intProduction = 1 AND 
				mst_plant.intStatus = 1 AND  
				mst_plant.MAIN_CLUSTER_ID = '$cluster'
			ORDER BY
			mst_locations.intId ASC";

        return $sql;
    }
	
	private function load_sub_cluster_plants_sql($sub_cluster,$year){ 
		
		$sql = "SELECT 
				mst_plant.intPlantId, 
				mst_plant_targets.TARGET_ANUAL,
				mst_plant_targets.TARGET_MONTHLY,
				mst_plant_targets.TARGET_DAILY,
				mst_plant.strPlantName,
				mst_plant.strPlantHeadName,
				mst_plant.strPlantHeadContactNo
				FROM
				mst_plant
				LEFT JOIN mst_plant_targets ON mst_plant.intPlantId = mst_plant_targets.PLANT_ID  AND mst_plant_targets.`YEAR` = '$year' 
				WHERE 
				mst_plant.intStatus = 1 AND 
				mst_plant.SUB_CLUSTER_ID = '$sub_cluster' 
			ORDER BY
			mst_plant.intPlantId ASC";
			
			return $sql;
	}
	private function load_plants_sql($plant,$year,$month)
	{ //suvini
	
	$yearMax = $year+1;
	$yearMin = $year-1;
	$minDate		=$yearMin.'-'.sprintf("%02d",4).'-01';
	$maxDate		=$year.'-'.sprintf("%02d",4).'-01';
	$minDate1		=$year.'-'.sprintf("%02d",4).'-01';
	$maxDate1		=$yearMax.'-'.sprintf("%02d",4).'-01';
	
		$sql = "SELECT 
				mst_plant.intPlantId,
				mst_month.strMonth, 
				mst_plant_monthly_targets.MONTH,
				mst_plant_monthly_targets.TARGET_MONTHLY,
				mst_plant_monthly_targets.TARGET_DAILY,
				mst_plant.strPlantName,
				mst_plant.strPlantHeadName,
				mst_plant.strPlantHeadContactNo,
				/*( select sum(annual.TARGET_MONTHLY) 
            FROM mst_plant_monthly_targets as annual 
            where  annual.`YEAR` = '$year' 
			AND annual.PLANT_ID=mst_plant_monthly_targets.PLANT_ID
                ) TARGET_ANNUALY*/
				
				( select sum(annual.TARGET_MONTHLY) 
            FROM mst_plant_monthly_targets as annual
			where  
			annual.PLANT_ID=mst_plant_monthly_targets.PLANT_ID"
			;
			 if($month<=3)
			 {
			$sql.=" AND  annual.STATUS=0 ";	 
			/* $sql.=" AND  $minDate <=
			CONCAT(annual.`YEAR`,'-',annual.MONTH,'-',01) < $maxDate"; */
			 }else
			 {
			//$sql.=" AND $minDate1 <= CONCAT(annual.`YEAR`,'-',annual.MONTH,'-',01) < $maxDate1";
				$sql.=" AND  annual.STATUS=0 ";

			 }
                
		   $sql.=")TARGET_ANNUALY";
		   
				$sql.=" FROM
				mst_plant
				LEFT JOIN mst_plant_monthly_targets ON mst_plant.intPlantId = mst_plant_monthly_targets.PLANT_ID  AND mst_plant_monthly_targets.YEAR = '$year'  AND mst_plant_monthly_targets.MONTH = '$month'
				
				LEFT JOIN mst_month on mst_month.intMonthId = mst_plant_monthly_targets.`MONTH`
				WHERE 
				mst_plant.intStatus = 1 AND 
				mst_plant.intPlantId = '$plant'
			ORDER BY
			mst_plant.intPlantId ASC";
			
			return $sql;
	}

    private function load_locations_sql($plant,$year,$month)
    {
        $sql = "SELECT 
				mst_locations.intPlant,
				mst_locations.intId,
				mst_month.strMonth, 
				mst_location_monthly_targets.MONTH,
				mst_location_monthly_targets.TARGET_MONTHLY,
				mst_location_monthly_targets.TARGET_DAILY,
				mst_locations.strName,
				mst_plant.strPlantHeadName,
				mst_plant.strPlantHeadContactNo,
				(select sum(annual.TARGET_MONTHLY) 
            FROM mst_location_monthly_targets as annual
			where  
			annual.LOCATION_ID=mst_location_monthly_targets.LOCATION_ID AND  annual.STATUS=0 ) as TARGET_ANNUALY FROM mst_locations left join mst_plant on mst_locations.intPlant = mst_plant.intPlantId
				LEFT JOIN mst_location_monthly_targets ON mst_locations.intId = mst_location_monthly_targets.LOCATION_ID  AND mst_location_monthly_targets.YEAR = '$year'  AND mst_location_monthly_targets.MONTH = '$month'
				LEFT JOIN mst_month on mst_month.intMonthId = mst_location_monthly_targets.`MONTH`
				WHERE 
				mst_locations.intStatus = 1 AND mst_locations.intProduction = 1 AND 
				mst_locations.intPlant = '$plant'
			ORDER BY
			mst_locations.intId ASC";

        return $sql;
    }


	private function load_plant_calender_sql($plant,$date)
	{
		
		$sql = "SELECT
						mst_plant_calender.dtDate,
						mst_plant_calender.intDayType
						FROM
						mst_locations
						INNER JOIN mst_plant_calender ON mst_locations.intPlant = mst_plant_calender.intPlantId
						WHERE
						mst_plant_calender.intPlantId = '$plant' AND
						mst_plant_calender.dtDate = '$date'";
			
			return $sql;
	}
 
	
 }

?>
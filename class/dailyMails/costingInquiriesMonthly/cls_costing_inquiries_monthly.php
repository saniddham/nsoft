
<?php

session_start();

include_once $_SESSION['ROOT_PATH'] . "class/cls_commonFunctions_get.php";
include_once $_SESSION['ROOT_PATH'] . "class/cls_commonErrorHandeling_get.php";

$obj_commonErr = new cls_commonErrorHandeling_get($db);
$obj_common = new cls_commonFunctions_get($db);

//VS customer int type = 13 mst_brand.category = 1 for VS brands
class cls_costing_inquiries_monthly {

    private $db;

    function __construct($db) {
        $this->db = $db;
    }

    public function get_cluster_marketers($vs_flag, $pervious_year, $current_year) {
        $sqlp = $this->get_cluster_marketers_sql($vs_flag, $pervious_year, $current_year);
        $resultp = $this->db->RunQuery($sqlp);
        return $resultp;
    }

    public function get_brand($vs_flag, $pervious_year, $current_year) {
        $sqlp = $this->get_brand_sql($vs_flag, $pervious_year, $current_year);
        $resultp = $this->db->RunQuery($sqlp);
        return $resultp;
    }

    public function get_customer($vs_flag, $pervious_year, $current_year) {
        $sqlp = $this->get_customer_sql($vs_flag, $pervious_year, $current_year);
        $resultp = $this->db->RunQuery($sqlp);
        return $resultp;
    }

    public function get_marketer_costing_VS($marketer, $year) {
        $sql = "SELECT
count(t.intSampleNo) as count,
t.month1
FROM
(
SELECT
trn_sampleinfomations.intSampleNo,
trn_sampleinfomations.intRevisionNo as rev,
costing_sample_header.COPPIED_FLAG ,
costing_sample_header_approved_by.APPROVED_DATE,
MONTH(costing_sample_header_approved_by.APPROVED_DATE) as month1,
mst_brand.strName,
sys_users.strUserName,
mst_marketer.intUserId,
costing_sample_header_approved_by.`STATUS`
FROM
mst_marketer
INNER JOIN sys_users ON sys_users.intUserId = mst_marketer.intUserId
INNER JOIN mst_marketer_cluster ON mst_marketer_cluster.MARKETER = mst_marketer.intUserId
INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intMarketer = mst_marketer.intUserId
INNER JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
inner JOIN costing_sample_header ON trn_sampleinfomations.intSampleNo = costing_sample_header.SAMPLE_NO AND
trn_sampleinfomations.intSampleYear = costing_sample_header.SAMPLE_YEAR AND
trn_sampleinfomations.intRevisionNo = costing_sample_header.REVISION
INNER JOIN costing_sample_header_approved_by ON costing_sample_header_approved_by.SAMPLE_NO = costing_sample_header.SAMPLE_NO
AND costing_sample_header_approved_by.SAMPLE_YEAR = costing_sample_header.SAMPLE_YEAR
AND costing_sample_header_approved_by.REVISION_NO = costing_sample_header.REVISION 
AND costing_sample_header_approved_by.COMBO = costing_sample_header.COMBO
AND costing_sample_header_approved_by.PRINT = costing_sample_header.PRINT
WHERE

mst_marketer_cluster.CLUSTER 							 = 1
AND mst_brand.CATEGORY 										 = 1 AND
costing_sample_header.COPPIED_FLAG         =  0 AND
costing_sample_header_approved_by.APPROVED_DATE    BETWEEN '2017-12-31 00:00:00' AND '2018-12-31 11:59:00'

AND costing_sample_header.`STATUS`        = 1  

-- AND mst_marketer.intUserId = 675


GROUP BY MONTH(costing_sample_header_approved_by.APPROVED_DATE),trn_sampleinfomations.intSampleNo
)

AS t

GROUP BY
t.month1";

        //$resultp		= 	$this->db->RunQuery($sql);
        //$row			=	mysqli_fetch_array($resultp);
        $resultp = $this->db->RunQuery($sql);
        return $resultp;
    }

    public function get_brand_costing($brand, $year, $month, $vs_flag) {
        $sql_r = " SELECT
                COUNT(DISTINCT costing_sample_header.SAMPLE_NO) as count
              FROM
              trn_sampleinfomations
              INNER JOIN costing_sample_header ON costing_sample_header.SAMPLE_NO = trn_sampleinfomations.intSampleNo AND
              costing_sample_header.SAMPLE_YEAR = trn_sampleinfomations.intSampleYear
              INNER JOIN mst_brand ON mst_brand.intId = trn_sampleinfomations.intBrand
              INNER JOIN mst_customer_brand ON mst_customer_brand.intBrandId = mst_brand.intId
              INNER JOIN mst_customer ON mst_customer.intId = mst_customer_brand.intCustomerId
              WHERE
              costing_sample_header.COPPIED_FLAG        =  0 AND
              year(costing_sample_header.SAVED_TIME)    = $year AND
              MONTH(costing_sample_header.SAVED_TIME)   = $month AND
              trn_sampleinfomations.intBrand            = $brand AND
              costing_sample_header.`STATUS`            = 1     AND  ";
        if ($vs_flag == 0) {
            $sql_r .= "mst_customer.intTypeId          != 13";
        } else {
            $sql_r .= "mst_customer.intTypeId           = 13";
        }

        $result_r = $this->db->RunQuery($sql_r);
        $row_r = mysqli_fetch_array($result_r);
        return $row_r['count'];
    }

    function get_customer_wise_costing($customer, $year, $month, $vs_flag) {
        $sql_c = "   SELECT
                 COUNT(DISTINCT costing_sample_header.SAMPLE_NO) as count
                 FROM
                 trn_sampleinfomations
                 INNER JOIN costing_sample_header ON costing_sample_header.SAMPLE_NO = trn_sampleinfomations.intSampleNo AND
                 costing_sample_header.SAMPLE_YEAR = trn_sampleinfomations.intSampleYear
		 INNER JOIN mst_customer ON mst_customer.intId = trn_sampleinfomations.intCustomer
                 WHERE
                 costing_sample_header.COPPIED_FLAG         =  0 AND
                 year(costing_sample_header.SAVED_TIME)     =  $year AND
                 MONTH(costing_sample_header.SAVED_TIME)    =  $month AND
                 costing_sample_header.`STATUS`             = 1      AND ";
        if ($vs_flag == 0) {
            $sql_c .= "mst_customer.intTypeId          != 13";
        } else {
            $sql_c .= "mst_customer.intTypeId           = 13";
        }
        $sql_c .= " AND  trn_sampleinfomations.intCustomer    =  $customer";
        //echo $sql_c;		
        $result_c = $this->db->RunQuery($sql_c);
        $row_c = mysqli_fetch_array($result_c);
        return $row_c['count'];
    }

    function get_brand_costing_others($current_year, $current_month_num, $vs_flag, $year, $month) {
        $sql_s = "  SELECT
GROUP_CONCAT(t.intId) as brand	
FROM
(					
SELECT
                count(DISTINCT costing_sample_header.SAMPLE_NO) as count,
                 mst_brand.intId,
                 mst_brand.strName
                 FROM
                 trn_sampleinfomations
                 INNER JOIN costing_sample_header ON costing_sample_header.SAMPLE_NO = trn_sampleinfomations.intSampleNo AND
                 costing_sample_header.SAMPLE_YEAR = trn_sampleinfomations.intSampleYear
                 INNER JOIN mst_brand ON mst_brand.intId = trn_sampleinfomations.intBrand
                 INNER JOIN mst_customer_brand ON mst_customer_brand.intBrandId = mst_brand.intId
                 INNER JOIN mst_customer ON mst_customer.intId = mst_customer_brand.intCustomerId
                 WHERE
                 costing_sample_header.COPPIED_FLAG         =  0 AND
                 year(costing_sample_header.SAVED_TIME)     =  $current_year AND
                 MONTH(costing_sample_header.SAVED_TIME)    =  $current_month_num AND
                 costing_sample_header.`STATUS`             = 1       AND  ";
        if ($vs_flag == 0) {
            $sql_s .= "mst_customer.intTypeId                != 13";
        } else {
            $sql_s .= "mst_customer.intTypeId                 = 13";
        }
        $sql_s .= "     GROUP BY
                 mst_brand.intId
                 ORDER BY
                 count desc
                 LIMIT 15 ) as t";
        // echo  $sql_s;
        $result_s = $this->db->RunQuery($sql_s);
        $row_s = mysqli_fetch_array($result_s);
        $brand = $row_s['brand'];

        if ($brand == "")
            $brand = 0;


        $sql_r = " SELECT
                COUNT(DISTINCT costing_sample_header.SAMPLE_NO) as tot
              FROM
              trn_sampleinfomations
              INNER JOIN costing_sample_header ON costing_sample_header.SAMPLE_NO = trn_sampleinfomations.intSampleNo AND
              costing_sample_header.SAMPLE_YEAR = trn_sampleinfomations.intSampleYear
              INNER JOIN mst_brand ON mst_brand.intId = trn_sampleinfomations.intBrand
              INNER JOIN mst_customer_brand ON mst_customer_brand.intBrandId = mst_brand.intId
              INNER JOIN mst_customer ON mst_customer.intId = mst_customer_brand.intCustomerId
              WHERE
              costing_sample_header.COPPIED_FLAG        =  0 AND
              year(costing_sample_header.SAVED_TIME)    = $year AND
              MONTH(costing_sample_header.SAVED_TIME)   = $month AND
             
              costing_sample_header.`STATUS`            = 1   AND ";
        if ($vs_flag == 0) {
            $sql_r .= "mst_customer.intTypeId                != 13";
        } else {
            $sql_r .= "mst_customer.intTypeId                 = 13";
        }
        $sql_r .= " AND trn_sampleinfomations.intBrand     not in ($brand )";
        //echo $sql_r;                                                 


        $result_r = $this->db->RunQuery($sql_r);
        $row_r = mysqli_fetch_array($result_r);
        return $row_r['tot'];
    }

    function get_customer_wise_costing_others($current_year, $current_month_num, $vs_flag, $year, $month) {

        $sql_t = "select GROUP_CONCAT(t.intId) as customer
    FROM
    (SELECT
     COUNT(DISTINCT costing_sample_header.SAMPLE_NO) as count,
     mst_customer.intId,
     mst_customer.strName
     FROM
     trn_sampleinfomations
     INNER JOIN costing_sample_header ON costing_sample_header.SAMPLE_NO = trn_sampleinfomations.intSampleNo AND
     costing_sample_header.SAMPLE_YEAR = trn_sampleinfomations.intSampleYear
     INNER JOIN mst_customer ON mst_customer.intId = trn_sampleinfomations.intCustomer
     WHERE
     costing_sample_header.COPPIED_FLAG         =  0 AND
     year(costing_sample_header.SAVED_TIME)     =  $current_year AND
     MONTH(costing_sample_header.SAVED_TIME)    =  $current_month_num AND
     costing_sample_header.`STATUS`             =  1  AND  ";
        if ($vs_flag == 0) {
            $sql_t .= "mst_customer.intTypeId           != 13";
        } else {
            $sql_t .= "mst_customer.intTypeId            = 13";
        }
        $sql_t .= " GROUP BY
    mst_customer.intId 
    ORDER BY
    count DESC
    LIMIT 15) as t";

        $result_t = $this->db->RunQuery($sql_t);
        $row_t = mysqli_fetch_array($result_t);
        $customer = $row_t['customer'];

        if ($customer == "")
            $customer = 0;


        $sql_p = "SELECT
                 COUNT(DISTINCT costing_sample_header.SAMPLE_NO) as tot,
                 mst_customer.intId
                 FROM
                 trn_sampleinfomations
                 INNER JOIN costing_sample_header ON costing_sample_header.SAMPLE_NO = trn_sampleinfomations.intSampleNo AND
                 costing_sample_header.SAMPLE_YEAR = trn_sampleinfomations.intSampleYear
		 INNER JOIN mst_customer ON mst_customer.intId = trn_sampleinfomations.intCustomer
                 WHERE
                 costing_sample_header.COPPIED_FLAG         =  0 AND
                 year(costing_sample_header.SAVED_TIME)     =  $year AND
                 MONTH(costing_sample_header.SAVED_TIME)    =  $month AND
                 costing_sample_header.`STATUS`             = 1     AND ";
        if ($vs_flag == 0) {
            $sql_p .= "mst_customer.intTypeId           != 13";
        } else {
            $sql_p .= "mst_customer.intTypeId            = 13";
        }
        $sql_p .= " AND  trn_sampleinfomations.intCustomer  not in ($customer)";

        //echo $sql_p;                                                  
        $result_p = $this->db->RunQuery($sql_p);
        $row_p = mysqli_fetch_array($result_p);
        return $row_p['tot'];
    }

    function get_cluster_marketers_sql($vs_flag, $pervious_year, $current_year) {

        $sql_current_month_top_marketers = "
                SELECT
	GROUP_CONCAT(z.marketerID) as top_marketer
        FROM
	(

                SELECT
			w.intUserId AS marketerID,
			w.count
		FROM
			(
		SELECT
			SUM(m.count) as count,
			m.`user`,
			m.intUserId
		FROM
			(
				SELECT
					count(t.intSampleNo) AS count,
					t. YEAR,
					t.month1 AS MONTH,
					t.intUserId,
					t.strUserName AS USER
				FROM
					(
						SELECT
							trn_sampleinfomations.intSampleNo,
							trn_sampleinfomations.intRevisionNo AS rev,
							costing_sample_header.COPPIED_FLAG,
							costing_sample_header_approved_by.APPROVED_DATE,
							YEAR (
								costing_sample_header_approved_by.APPROVED_DATE
							) AS YEAR,
							MONTH (
								costing_sample_header_approved_by.APPROVED_DATE
							) AS month1,
							mst_brand.strName,
							sys_users.strUserName,
							mst_marketer.intUserId,
							costing_sample_header_approved_by.`STATUS`
						FROM
							mst_marketer
						INNER JOIN sys_users ON sys_users.intUserId = mst_marketer.intUserId
						INNER JOIN mst_marketer_cluster ON mst_marketer_cluster.MARKETER = mst_marketer.intUserId
						INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intMarketer = mst_marketer.intUserId
						INNER JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
						INNER JOIN costing_sample_header ON trn_sampleinfomations.intSampleNo = costing_sample_header.SAMPLE_NO
						AND trn_sampleinfomations.intSampleYear = costing_sample_header.SAMPLE_YEAR
						AND trn_sampleinfomations.intRevisionNo = costing_sample_header.REVISION
						INNER JOIN costing_sample_header_approved_by ON costing_sample_header_approved_by.SAMPLE_NO = costing_sample_header.SAMPLE_NO
						AND costing_sample_header_approved_by.SAMPLE_YEAR = costing_sample_header.SAMPLE_YEAR
						AND costing_sample_header_approved_by.REVISION_NO = costing_sample_header.REVISION
						AND costing_sample_header_approved_by.COMBO = costing_sample_header.COMBO
						AND costing_sample_header_approved_by.PRINT = costing_sample_header.PRINT
                                                INNER JOIN trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
                                            AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
                                            AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo 
                                            INNER JOIN mst_locations ON mst_locations.intId = trn_sampleinfomations_details.intCompanyId
						WHERE
							mst_marketer_cluster.CLUSTER = 1";
                                            if ($vs_flag == 0) { //non vs
                                                $sql_current_month_top_marketers .= " AND mst_brand.CATEGORY 	!=1 ";
                                            } else {
                                                $sql_current_month_top_marketers .= " AND mst_brand.CATEGORY 		= 1 ";
                                            }
                                            $sql_current_month_top_marketers .= " AND costing_sample_header.COPPIED_FLAG = 0
                                            AND costing_sample_header_approved_by.APPROVED_DATE BETWEEN '$pervious_year-01-01 00:00:00'
                                            AND '$current_year-12-31 11:59:00'
                                            AND costing_sample_header_approved_by.`STATUS` = 0
                                            AND MONTH (
                                            costing_sample_header_approved_by.APPROVED_DATE
                                            ) = MONTH(CURRENT_DATE())
                                            AND YEAR (
                                            costing_sample_header_approved_by.APPROVED_DATE
                                            ) = YEAR(CURRENT_DATE())
                                            /*AND MONTH (
                                                    costing_sample_header_approved_by.APPROVED_DATE
                                            ) = 6
                                            AND YEAR (
                                                    costing_sample_header_approved_by.APPROVED_DATE
                                            ) = 2018*/
                                            AND mst_locations.intCompanyId in (1,7,36)
                                            AND mst_marketer.`STATUS` = 1	
                                            GROUP BY
                                            MONTH (
                                                    costing_sample_header_approved_by.APPROVED_DATE
                                            ),
                                            trn_sampleinfomations.intSampleNo
                                                                    ) AS t
                                            GROUP BY
                                                    t.month1,
                                                    t.`year`,
                                                    t.intUserId
                                            ORDER BY
                                                    t.strUserName,
                                                    t.month1,
                                                    t.`year`
                                    ) AS m
                                                            GROUP BY
                                                                    m.`user`
                                            )AS w
                                                            ORDER BY
                                                                    w.count DESC
                                          ) AS z	";
                                //echo $sql_current_month_top_marketers;
                                $result_top	  = 	$this->db->RunQuery($sql_current_month_top_marketers);
                                $row_top	  =	mysqli_fetch_array($result_top);
                                $top_marketer =     $row_top['top_marketer'];

        $sql = " SELECT
count(t.intSampleNo) as count,
t.year,
t.month1 as month,
t.intUserId,
t.strUserName as user
FROM
(
SELECT
trn_sampleinfomations.intSampleNo,
trn_sampleinfomations.intRevisionNo as rev,
costing_sample_header.COPPIED_FLAG ,
costing_sample_header_approved_by.APPROVED_DATE,
year(costing_sample_header_approved_by.APPROVED_DATE) as year,
MONTH(costing_sample_header_approved_by.APPROVED_DATE) as month1,
mst_brand.strName,
sys_users.strUserName,
mst_marketer.intUserId,
costing_sample_header_approved_by.`STATUS`
FROM
mst_marketer
INNER JOIN sys_users ON sys_users.intUserId = mst_marketer.intUserId
INNER JOIN mst_marketer_cluster ON mst_marketer_cluster.MARKETER = mst_marketer.intUserId
INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intMarketer = mst_marketer.intUserId
INNER JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
inner JOIN costing_sample_header ON trn_sampleinfomations.intSampleNo = costing_sample_header.SAMPLE_NO AND
trn_sampleinfomations.intSampleYear = costing_sample_header.SAMPLE_YEAR AND
trn_sampleinfomations.intRevisionNo = costing_sample_header.REVISION
INNER JOIN costing_sample_header_approved_by ON costing_sample_header_approved_by.SAMPLE_NO = costing_sample_header.SAMPLE_NO
AND costing_sample_header_approved_by.SAMPLE_YEAR = costing_sample_header.SAMPLE_YEAR
AND costing_sample_header_approved_by.REVISION_NO = costing_sample_header.REVISION 
AND costing_sample_header_approved_by.COMBO = costing_sample_header.COMBO
AND costing_sample_header_approved_by.PRINT = costing_sample_header.PRINT
INNER JOIN trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
                                            AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
                                            AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo 
                                            INNER JOIN mst_locations ON mst_locations.intId = trn_sampleinfomations_details.intCompanyId
WHERE

mst_marketer_cluster.CLUSTER 		= 1";
        if ($vs_flag == 0) { //non vs
            $sql .= " AND mst_brand.CATEGORY 	!=1 ";
        } else {
            $sql .= " AND mst_brand.CATEGORY 		= 1 ";
        }
        $sql .= " AND costing_sample_header.COPPIED_FLAG      =  0  and

    costing_sample_header_approved_by.APPROVED_DATE    BETWEEN '$pervious_year-01-01 00:00:00' AND '$current_year-12-31 11:59:00'

    AND costing_sample_header_approved_by.`STATUS` = 0
    AND mst_marketer.intUserId IN ($top_marketer)
     AND mst_locations.intCompanyId in (1,7,36)
     	AND mst_marketer.`STATUS` = 1	
    GROUP BY MONTH(costing_sample_header_approved_by.APPROVED_DATE),trn_sampleinfomations.intSampleNo
    )

    AS t

    GROUP BY
    t.month1,t.`year`,t.intUserId
    ORDER BY
    t.strUserName,t.month1,t.`year`
    ";
      //echo $sql;                       
        return $sql;
    }

    function get_brand_sql($vs_flag, $pervious_year, $current_year) {

        
        $sql_current_month_top_brand = "SELECT
	GROUP_CONCAT(z.brandId) as top_brand
FROM
	(
SELECT
			w.brandId AS brandId,
			w.count
		FROM
			(
		SELECT
			m.brandId,
			sum(m.count) as count
		FROM
			(
				SELECT
					count(t.intSampleNo) AS count,
					t. YEAR,
					t.month1 AS MONTH,
					t.brand,
					t.brandId
				FROM
					(
						SELECT
							trn_sampleinfomations.intSampleNo,
							trn_sampleinfomations.intRevisionNo AS rev,
							costing_sample_header.COPPIED_FLAG,
							costing_sample_header_approved_by.APPROVED_DATE,
							YEAR (
								costing_sample_header_approved_by.APPROVED_DATE
							) AS YEAR,
							MONTH (
								costing_sample_header_approved_by.APPROVED_DATE
							) AS month1,
							mst_brand.strName AS brand,
							mst_brand.intId AS brandId,
							costing_sample_header_approved_by.`STATUS`
						FROM
							mst_marketer
						INNER JOIN sys_users ON sys_users.intUserId = mst_marketer.intUserId
						INNER JOIN mst_marketer_cluster ON mst_marketer_cluster.MARKETER = mst_marketer.intUserId
						INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intMarketer = mst_marketer.intUserId
						INNER JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
						INNER JOIN costing_sample_header ON trn_sampleinfomations.intSampleNo = costing_sample_header.SAMPLE_NO
						AND trn_sampleinfomations.intSampleYear = costing_sample_header.SAMPLE_YEAR
						AND trn_sampleinfomations.intRevisionNo = costing_sample_header.REVISION
						INNER JOIN costing_sample_header_approved_by ON costing_sample_header_approved_by.SAMPLE_NO = costing_sample_header.SAMPLE_NO
						AND costing_sample_header_approved_by.SAMPLE_YEAR = costing_sample_header.SAMPLE_YEAR
						AND costing_sample_header_approved_by.REVISION_NO = costing_sample_header.REVISION
						AND costing_sample_header_approved_by.COMBO = costing_sample_header.COMBO
						AND costing_sample_header_approved_by.PRINT = costing_sample_header.PRINT
						INNER JOIN trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
                                                AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
                                                AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo 
                                                INNER JOIN mst_locations ON mst_locations.intId = trn_sampleinfomations_details.intCompanyId
                                                WHERE
							mst_marketer_cluster.CLUSTER = 1";
						 if ($vs_flag == 0) { //non vs
            $sql_current_month_top_brand .= " AND mst_brand.CATEGORY 	!=1 ";
        } else {
            $sql_current_month_top_brand .= " AND mst_brand.CATEGORY 		= 1 ";
    }
						$sql_current_month_top_brand.= " AND costing_sample_header.COPPIED_FLAG = 0
						AND costing_sample_header_approved_by.APPROVED_DATE BETWEEN '$pervious_year-01-01 00:00:00'
						AND '$current_year-12-31 11:59:00'
						AND costing_sample_header_approved_by.`STATUS` = 0
                                                AND MONTH (
                costing_sample_header_approved_by.APPROVED_DATE
                ) = MONTH(CURRENT_DATE())
                AND YEAR (
                costing_sample_header_approved_by.APPROVED_DATE
                ) = YEAR(CURRENT_DATE())
               /*AND MONTH (
                        costing_sample_header_approved_by.APPROVED_DATE
                ) = 6
                AND YEAR (
                        costing_sample_header_approved_by.APPROVED_DATE
                ) = 2018*/
                 AND mst_locations.intCompanyId in (1,7,36)
                 -- AND mst_marketer.`STATUS` = 1	
GROUP BY
							MONTH (
								costing_sample_header_approved_by.APPROVED_DATE
							),
							trn_sampleinfomations.intSampleNo
					) AS t
				GROUP BY
					t.month1,
					t.`year`,
					t.brandId
				ORDER BY
					t.brandId,
					t.month1,
					t.`year`
			) AS m
		GROUP BY
			m.brandId
) AS w
		ORDER BY
			w.count DESC
		LIMIT 10
		) AS z";
                     //echo   $sql_current_month_top_brand;                         
            $result_top	  = 	$this->db->RunQuery($sql_current_month_top_brand);
            $row_top	  =	mysqli_fetch_array($result_top);
            $top_brand    =     $row_top['top_brand'];                                 
        
        $sql = "SELECT
	count(t.intSampleNo) AS count,
	t.YEAR,
	t.month1 AS MONTH,
	t.brand,
	t.brandId
FROM
	(
		SELECT
			trn_sampleinfomations.intSampleNo,
			trn_sampleinfomations.intRevisionNo AS rev,
			costing_sample_header.COPPIED_FLAG,
			costing_sample_header_approved_by.APPROVED_DATE,
			YEAR (
				costing_sample_header_approved_by.APPROVED_DATE
			) AS YEAR,
			MONTH (
				costing_sample_header_approved_by.APPROVED_DATE
			) AS month1,
			mst_brand.strName AS brand,
			mst_brand.intId AS brandId,
			
			costing_sample_header_approved_by.`STATUS`
		FROM
			mst_marketer
		INNER JOIN sys_users ON sys_users.intUserId = mst_marketer.intUserId
		INNER JOIN mst_marketer_cluster ON mst_marketer_cluster.MARKETER = mst_marketer.intUserId
		INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intMarketer = mst_marketer.intUserId
		INNER JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
		INNER JOIN costing_sample_header ON trn_sampleinfomations.intSampleNo = costing_sample_header.SAMPLE_NO
		AND trn_sampleinfomations.intSampleYear = costing_sample_header.SAMPLE_YEAR
		AND trn_sampleinfomations.intRevisionNo = costing_sample_header.REVISION
		INNER JOIN costing_sample_header_approved_by ON costing_sample_header_approved_by.SAMPLE_NO = costing_sample_header.SAMPLE_NO
		AND costing_sample_header_approved_by.SAMPLE_YEAR = costing_sample_header.SAMPLE_YEAR
		AND costing_sample_header_approved_by.REVISION_NO = costing_sample_header.REVISION
		AND costing_sample_header_approved_by.COMBO = costing_sample_header.COMBO
		AND costing_sample_header_approved_by.PRINT = costing_sample_header.PRINT
		INNER JOIN trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
                AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
                AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo 
                INNER JOIN mst_locations ON mst_locations.intId = trn_sampleinfomations_details.intCompanyId

WHERE
                mst_marketer_cluster.CLUSTER = 1";
        if ($vs_flag == 0) { //non vs
            $sql .= " AND mst_brand.CATEGORY != 1";
        } else {
            $sql .= " AND mst_brand.CATEGORY = 1";
        }
        $sql .= " AND costing_sample_header.COPPIED_FLAG = 0
		AND costing_sample_header_approved_by.APPROVED_DATE BETWEEN '$pervious_year-01-01 00:00:00'
		AND '$current_year-12-31 11:59:00'
		AND costing_sample_header_approved_by.`STATUS` = 0
                AND mst_brand.intId IN ($top_brand)
                 AND mst_locations.intCompanyId in (1,7,36)
                 	-- AND mst_marketer.`STATUS` = 1	
		GROUP BY
			MONTH (
				costing_sample_header_approved_by.APPROVED_DATE
			),
			trn_sampleinfomations.intSampleNo
	) AS t
GROUP BY
	t.month1,
	t.`year`,
	t.brandId
ORDER BY
	t.brandId,t.month1,t.`year`";
        //echo $sql;
        return $sql;
    }

    function get_customer_sql($vs_flag, $pervious_year, $current_year) {

        $sql_current_month_top_customers = "
SELECT
GROUP_CONCAT(z.cusID) as top_customers
FROM
(
SELECT
        w.cus_id AS cusID,
        w.count_sum
FROM
(
SELECT
SUM(count) AS count_sum,
m.cusId AS cus_id,
m.cusName
FROM
(
SELECT
count(t.intSampleNo) AS count,
t. YEAR,
t.month1 AS MONTH,
t.cusId,
t.cusName
FROM
(
SELECT
trn_sampleinfomations.intSampleNo,
trn_sampleinfomations.intRevisionNo AS rev,
costing_sample_header.COPPIED_FLAG,
costing_sample_header_approved_by.APPROVED_DATE,
YEAR (
        costing_sample_header_approved_by.APPROVED_DATE
) AS YEAR,
MONTH (
        costing_sample_header_approved_by.APPROVED_DATE
) AS month1,
mst_customer.intId AS cusId,
mst_customer.strName AS cusName,
costing_sample_header_approved_by.`STATUS`
FROM
mst_marketer
INNER JOIN sys_users ON sys_users.intUserId = mst_marketer.intUserId
INNER JOIN mst_marketer_cluster ON mst_marketer_cluster.MARKETER = mst_marketer.intUserId
INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intMarketer = mst_marketer.intUserId
INNER JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
INNER JOIN costing_sample_header ON trn_sampleinfomations.intSampleNo = costing_sample_header.SAMPLE_NO
AND trn_sampleinfomations.intSampleYear = costing_sample_header.SAMPLE_YEAR
AND trn_sampleinfomations.intRevisionNo = costing_sample_header.REVISION
INNER JOIN costing_sample_header_approved_by ON costing_sample_header_approved_by.SAMPLE_NO = costing_sample_header.SAMPLE_NO
AND costing_sample_header_approved_by.SAMPLE_YEAR = costing_sample_header.SAMPLE_YEAR
AND costing_sample_header_approved_by.REVISION_NO = costing_sample_header.REVISION
AND costing_sample_header_approved_by.COMBO = costing_sample_header.COMBO
AND costing_sample_header_approved_by.PRINT = costing_sample_header.PRINT
INNER JOIN mst_customer ON mst_customer.intId = trn_sampleinfomations.intCustomer
INNER JOIN trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
                                            AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
                                            AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo 
                                            INNER JOIN mst_locations ON mst_locations.intId = trn_sampleinfomations_details.intCompanyId
WHERE
        mst_marketer_cluster.CLUSTER = 1";
        if ($vs_flag == 0) { //non vs
            $sql_current_month_top_customers .= " AND mst_brand.CATEGORY != 1";
        } else {
            $sql_current_month_top_customers .= " AND mst_brand.CATEGORY = 1";
        }
        $sql_current_month_top_customers .= "  AND costing_sample_header.COPPIED_FLAG = 0
AND costing_sample_header_approved_by.APPROVED_DATE BETWEEN '$pervious_year-01-01 00:00:00'
AND '$current_year-12-31 11:59:00'
AND costing_sample_header_approved_by.`STATUS` = 0 
AND MONTH (
costing_sample_header_approved_by.APPROVED_DATE
) = MONTH(CURRENT_DATE())
AND YEAR (
costing_sample_header_approved_by.APPROVED_DATE
) = YEAR(CURRENT_DATE())
/*AND MONTH (
    costing_sample_header_approved_by.APPROVED_DATE
) = 6
AND YEAR (
    costing_sample_header_approved_by.APPROVED_DATE
) = 2018*/
 AND mst_locations.intCompanyId in (1,7,36)
 	-- AND mst_marketer.`STATUS` = 1	
GROUP BY
    MONTH (
            costing_sample_header_approved_by.APPROVED_DATE
    ),
    trn_sampleinfomations.intSampleNo
) AS t
GROUP BY
t.month1,
t.`year`,
t.cusId
ORDER BY
t.cusId,
t.month1,
t.`year`
) AS m
GROUP BY
        m.cusId
) AS w
ORDER BY
w.count_sum DESC
LIMIT 10
) AS z";
        //echo $sql_current_month_top_customers;
        $result_top = $this->db->RunQuery($sql_current_month_top_customers);
        $row_top = mysqli_fetch_array($result_top);
        $top_customers = $row_top['top_customers'];




        $sql = "SELECT
	count(t.intSampleNo) AS count,
	t. YEAR,
	t.month1 AS MONTH,
	t.cusId,
	t.cusName
FROM
	(
		SELECT
			trn_sampleinfomations.intSampleNo,
			trn_sampleinfomations.intRevisionNo AS rev,
			costing_sample_header.COPPIED_FLAG,
			costing_sample_header_approved_by.APPROVED_DATE,
			YEAR (
				costing_sample_header_approved_by.APPROVED_DATE
			) AS YEAR,
			MONTH (
				costing_sample_header_approved_by.APPROVED_DATE
			) AS month1,
			mst_customer.intId as cusId,
			mst_customer.strName as cusName,
			costing_sample_header_approved_by.`STATUS`
		FROM
			mst_marketer
		INNER JOIN sys_users ON sys_users.intUserId = mst_marketer.intUserId
		INNER JOIN mst_marketer_cluster ON mst_marketer_cluster.MARKETER = mst_marketer.intUserId
		INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intMarketer = mst_marketer.intUserId
		INNER JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
		INNER JOIN costing_sample_header ON trn_sampleinfomations.intSampleNo = costing_sample_header.SAMPLE_NO
		AND trn_sampleinfomations.intSampleYear = costing_sample_header.SAMPLE_YEAR
		AND trn_sampleinfomations.intRevisionNo = costing_sample_header.REVISION
		INNER JOIN costing_sample_header_approved_by ON costing_sample_header_approved_by.SAMPLE_NO = costing_sample_header.SAMPLE_NO
		AND costing_sample_header_approved_by.SAMPLE_YEAR = costing_sample_header.SAMPLE_YEAR
		AND costing_sample_header_approved_by.REVISION_NO = costing_sample_header.REVISION
		AND costing_sample_header_approved_by.COMBO = costing_sample_header.COMBO
		AND costing_sample_header_approved_by.PRINT = costing_sample_header.PRINT
		INNER JOIN mst_customer on mst_customer.intId = trn_sampleinfomations.intCustomer
		INNER JOIN trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
                AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
                AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo 
                INNER JOIN mst_locations ON mst_locations.intId = trn_sampleinfomations_details.intCompanyId

                WHERE
                mst_marketer_cluster.CLUSTER = 1";
        if ($vs_flag == 0) { //non vs
            $sql .= " AND mst_brand.CATEGORY != 1";
        } else {
            $sql .= " AND mst_brand.CATEGORY = 1 ";
        }

        $sql .= " AND costing_sample_header.COPPIED_FLAG = 0
		AND costing_sample_header_approved_by.APPROVED_DATE BETWEEN '$pervious_year-01-01 00:00:00'
		AND '$current_year-12-31 11:59:00'
		AND costing_sample_header_approved_by.`STATUS` = 0
		AND mst_customer.intId IN ($top_customers)
                 AND mst_locations.intCompanyId in (1,7,36)
                 -- AND mst_marketer.`STATUS` = 1
		GROUP BY
			MONTH (
				costing_sample_header_approved_by.APPROVED_DATE
			),
			trn_sampleinfomations.intSampleNo
	) AS t
GROUP BY
	t.month1,
	t.`year`,
	t.cusId
ORDER BY
	
	t.cusId,t.month1,t.`year` ";
        //echo  $sql;
        return $sql;
    }

    function get_marketer_tot($year1, $pervious_year, $current_year, $j, $vs_flag) {
        $sql_q = "SELECT
	SUM(m.count) as sum
FROM
	(
		SELECT
			count(t.intSampleNo) AS count,
			t. YEAR,
			t.month1 AS MONTH,
			t.intUserId,
			t.strUserName AS USER
		FROM
			(
				SELECT
					trn_sampleinfomations.intSampleNo,
					trn_sampleinfomations.intRevisionNo AS rev,
					costing_sample_header.COPPIED_FLAG,
					costing_sample_header_approved_by.APPROVED_DATE,
					YEAR (
						costing_sample_header_approved_by.APPROVED_DATE
					) AS YEAR,
					MONTH (
						costing_sample_header_approved_by.APPROVED_DATE
					) AS month1,
					mst_brand.strName,
					sys_users.strUserName,
					mst_marketer.intUserId,
					costing_sample_header_approved_by.`STATUS`
				FROM
					mst_marketer
				INNER JOIN sys_users ON sys_users.intUserId = mst_marketer.intUserId
				INNER JOIN mst_marketer_cluster ON mst_marketer_cluster.MARKETER = mst_marketer.intUserId
				INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intMarketer = mst_marketer.intUserId
				INNER JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
				INNER JOIN costing_sample_header ON trn_sampleinfomations.intSampleNo = costing_sample_header.SAMPLE_NO
				AND trn_sampleinfomations.intSampleYear = costing_sample_header.SAMPLE_YEAR
				AND trn_sampleinfomations.intRevisionNo = costing_sample_header.REVISION
				INNER JOIN costing_sample_header_approved_by ON costing_sample_header_approved_by.SAMPLE_NO = costing_sample_header.SAMPLE_NO
				AND costing_sample_header_approved_by.SAMPLE_YEAR = costing_sample_header.SAMPLE_YEAR
				AND costing_sample_header_approved_by.REVISION_NO = costing_sample_header.REVISION
				AND costing_sample_header_approved_by.COMBO = costing_sample_header.COMBO
				AND costing_sample_header_approved_by.PRINT = costing_sample_header.PRINT
				INNER JOIN trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
                                AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
                                AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo 
                                INNER JOIN mst_locations ON mst_locations.intId = trn_sampleinfomations_details.intCompanyId
                                WHERE
                                mst_marketer_cluster.CLUSTER = 1";
        if ($vs_flag == 0) {  //non vs
            $sql_q .= " AND mst_brand.CATEGORY != 1";
        } else {
            $sql_q .= " AND mst_brand.CATEGORY = 1";
        }
        $sql_q .= " AND costing_sample_header.COPPIED_FLAG = 0
				AND costing_sample_header_approved_by.APPROVED_DATE BETWEEN '$pervious_year-01-01 00:00:00'
				AND '$current_year-12-31 11:59:00'
				AND costing_sample_header_approved_by.`STATUS` = 0
				AND MONTH (
					costing_sample_header_approved_by.APPROVED_DATE
				) = $j
				AND YEAR (
					costing_sample_header_approved_by.APPROVED_DATE
				) = $year1 
                                 AND mst_locations.intCompanyId in (1,7,36)
                                 -- AND mst_marketer.`STATUS` = 1
				GROUP BY
					MONTH (
						costing_sample_header_approved_by.APPROVED_DATE
					),
					trn_sampleinfomations.intSampleNo
			) AS t
		GROUP BY
			t.month1,
			t.`year`,
			t.intUserId
		ORDER BY
			t.strUserName,
			t.month1,
			t.`year`
	) AS m";
        // echo $sql_q;
        $result_q = $this->db->RunQuery($sql_q);
        $row_q = mysqli_fetch_array($result_q);
        return $row_q['sum'];
    }

    function get_brand_tot($year, $pervious_year, $current_year, $j, $vs_flag) {
        $sql_p = "
SELECT SUM(m.count) AS sum
FROM
(

SELECT
	count(t.intSampleNo) AS count,
	t.YEAR,
	t.month1 AS MONTH,
	t.brand,
	t.brandId
FROM
	(
		SELECT
			trn_sampleinfomations.intSampleNo,
			trn_sampleinfomations.intRevisionNo AS rev,
			costing_sample_header.COPPIED_FLAG,
			costing_sample_header_approved_by.APPROVED_DATE,
			YEAR (
				costing_sample_header_approved_by.APPROVED_DATE
			) AS YEAR,
			MONTH (
				costing_sample_header_approved_by.APPROVED_DATE
			) AS month1,
			mst_brand.strName AS brand,
			mst_brand.intId AS brandId,
			-- sys_users.strUserName,
			-- mst_marketer.intUserId,
			costing_sample_header_approved_by.`STATUS`
		FROM
			mst_marketer
		INNER JOIN sys_users ON sys_users.intUserId = mst_marketer.intUserId
		INNER JOIN mst_marketer_cluster ON mst_marketer_cluster.MARKETER = mst_marketer.intUserId
		INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intMarketer = mst_marketer.intUserId
		INNER JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
		INNER JOIN costing_sample_header ON trn_sampleinfomations.intSampleNo = costing_sample_header.SAMPLE_NO
		AND trn_sampleinfomations.intSampleYear = costing_sample_header.SAMPLE_YEAR
		AND trn_sampleinfomations.intRevisionNo = costing_sample_header.REVISION
		INNER JOIN costing_sample_header_approved_by ON costing_sample_header_approved_by.SAMPLE_NO = costing_sample_header.SAMPLE_NO
		AND costing_sample_header_approved_by.SAMPLE_YEAR = costing_sample_header.SAMPLE_YEAR
		AND costing_sample_header_approved_by.REVISION_NO = costing_sample_header.REVISION
		AND costing_sample_header_approved_by.COMBO = costing_sample_header.COMBO
		AND costing_sample_header_approved_by.PRINT = costing_sample_header.PRINT
		INNER JOIN trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
                                            AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
                                            AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo 
                                            INNER JOIN mst_locations ON mst_locations.intId = trn_sampleinfomations_details.intCompanyId
                    WHERE
     mst_marketer_cluster.CLUSTER = 1";
        if ($vs_flag == 0) {
            $sql_p .= " AND mst_brand.CATEGORY != 1";
        } else {
            $sql_p .= " AND mst_brand.CATEGORY = 1";
        }
        $sql_p .= " AND costing_sample_header.COPPIED_FLAG = 0
		AND costing_sample_header_approved_by.APPROVED_DATE BETWEEN '$pervious_year-01-01 00:00:00'
		AND '$current_year-12-31 11:59:00'
		AND costing_sample_header_approved_by.`STATUS` = 0
		AND MONTH (costing_sample_header_approved_by.APPROVED_DATE) = $j
		AND YEAR (costing_sample_header_approved_by.APPROVED_DATE) = $year
                 AND mst_locations.intCompanyId in (1,7,36)
                 -- AND mst_marketer.`STATUS` = 1
		GROUP BY
			MONTH (
				costing_sample_header_approved_by.APPROVED_DATE
			),
			trn_sampleinfomations.intSampleNo
	) AS t
GROUP BY
	t.month1,
	t.`year`,
	t.brandId
ORDER BY
	t.brandId,t.month1,t.`year`
) as m";

        $result_q = $this->db->RunQuery($sql_p);
        $row_q = mysqli_fetch_array($result_q);
        return $row_q['sum'];
    }

    function get_customer_tot($year, $pervious_year, $current_year, $j, $vs_flag) {
        $sql_q = "SELECT
SUM(m.count) as sum
FROM
(
SELECT
	count(t.intSampleNo) AS count,
	t. YEAR,
	t.month1 AS MONTH,
	t.cusId,
	t.cusName
FROM
	(
		SELECT
			trn_sampleinfomations.intSampleNo,
			trn_sampleinfomations.intRevisionNo AS rev,
			costing_sample_header.COPPIED_FLAG,
			costing_sample_header_approved_by.APPROVED_DATE,
			YEAR (
				costing_sample_header_approved_by.APPROVED_DATE
			) AS YEAR,
			MONTH (
				costing_sample_header_approved_by.APPROVED_DATE
			) AS month1,
			mst_customer.intId as cusId,
			mst_customer.strName as cusName,
			costing_sample_header_approved_by.`STATUS`
		FROM
			mst_marketer
		INNER JOIN sys_users ON sys_users.intUserId = mst_marketer.intUserId
		INNER JOIN mst_marketer_cluster ON mst_marketer_cluster.MARKETER = mst_marketer.intUserId
		INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intMarketer = mst_marketer.intUserId
		INNER JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
		INNER JOIN costing_sample_header ON trn_sampleinfomations.intSampleNo = costing_sample_header.SAMPLE_NO
		AND trn_sampleinfomations.intSampleYear = costing_sample_header.SAMPLE_YEAR
		AND trn_sampleinfomations.intRevisionNo = costing_sample_header.REVISION
		INNER JOIN costing_sample_header_approved_by ON costing_sample_header_approved_by.SAMPLE_NO = costing_sample_header.SAMPLE_NO
		AND costing_sample_header_approved_by.SAMPLE_YEAR = costing_sample_header.SAMPLE_YEAR
		AND costing_sample_header_approved_by.REVISION_NO = costing_sample_header.REVISION
		AND costing_sample_header_approved_by.COMBO = costing_sample_header.COMBO
		AND costing_sample_header_approved_by.PRINT = costing_sample_header.PRINT
		INNER JOIN mst_customer on mst_customer.intId = trn_sampleinfomations.intCustomer
		INNER JOIN trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
                AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
                AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo 
                INNER JOIN mst_locations ON mst_locations.intId = trn_sampleinfomations_details.intCompanyId

                WHERE
			mst_marketer_cluster.CLUSTER = 1";
        if ($vs_flag == 0) {
            $sql_q .= " AND mst_brand.CATEGORY != 1";
        } else {
            $sql_q .= " AND mst_brand.CATEGORY = 1 ";
        }
        $sql_q .= " AND costing_sample_header.COPPIED_FLAG = 0
		AND costing_sample_header_approved_by.APPROVED_DATE BETWEEN '$pervious_year-01-01 00:00:00'
		AND '$current_year-12-31 11:59:00'
		AND costing_sample_header_approved_by.`STATUS` = 0
		AND MONTH (costing_sample_header_approved_by.APPROVED_DATE) = $j
		AND YEAR (costing_sample_header_approved_by.APPROVED_DATE) = $year
		 AND mst_locations.intCompanyId in (1,7,36)
                 -- AND mst_marketer.`STATUS` = 1
		GROUP BY
			MONTH (
				costing_sample_header_approved_by.APPROVED_DATE
			),
			trn_sampleinfomations.intSampleNo
	) AS t
GROUP BY
	t.month1,
	t.`year`,
	t.cusId
ORDER BY
	
	t.cusId,t.month1,t.`year` ) as m";

        $result_q = $this->db->RunQuery($sql_q);
        $row_q = mysqli_fetch_array($result_q);
        return $row_q['sum'];
    }

    function get_marketer_other($year, $pervious_year, $current_year, $j, $vs_flag) {
        
         $sql_current_month_top_marketers = "
                SELECT
	GROUP_CONCAT(z.marketerID) as top_marketer
        FROM
	(

                SELECT
			w.intUserId AS marketerID,
			w.count
		FROM
			(
		SELECT
			SUM(m.count) as count,
			m.`user`,
			m.intUserId
		FROM
			(
				SELECT
					count(t.intSampleNo) AS count,
					t. YEAR,
					t.month1 AS MONTH,
					t.intUserId,
					t.strUserName AS USER
				FROM
					(
						SELECT
							trn_sampleinfomations.intSampleNo,
							trn_sampleinfomations.intRevisionNo AS rev,
							costing_sample_header.COPPIED_FLAG,
							costing_sample_header_approved_by.APPROVED_DATE,
							YEAR (
								costing_sample_header_approved_by.APPROVED_DATE
							) AS YEAR,
							MONTH (
								costing_sample_header_approved_by.APPROVED_DATE
							) AS month1,
							mst_brand.strName,
							sys_users.strUserName,
							mst_marketer.intUserId,
							costing_sample_header_approved_by.`STATUS`
						FROM
							mst_marketer
						INNER JOIN sys_users ON sys_users.intUserId = mst_marketer.intUserId
						INNER JOIN mst_marketer_cluster ON mst_marketer_cluster.MARKETER = mst_marketer.intUserId
						INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intMarketer = mst_marketer.intUserId
						INNER JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
						INNER JOIN costing_sample_header ON trn_sampleinfomations.intSampleNo = costing_sample_header.SAMPLE_NO
						AND trn_sampleinfomations.intSampleYear = costing_sample_header.SAMPLE_YEAR
						AND trn_sampleinfomations.intRevisionNo = costing_sample_header.REVISION
						INNER JOIN costing_sample_header_approved_by ON costing_sample_header_approved_by.SAMPLE_NO = costing_sample_header.SAMPLE_NO
						AND costing_sample_header_approved_by.SAMPLE_YEAR = costing_sample_header.SAMPLE_YEAR
						AND costing_sample_header_approved_by.REVISION_NO = costing_sample_header.REVISION
						AND costing_sample_header_approved_by.COMBO = costing_sample_header.COMBO
						AND costing_sample_header_approved_by.PRINT = costing_sample_header.PRINT
						INNER JOIN trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
                                            AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
                                            AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo 
                                            INNER JOIN mst_locations ON mst_locations.intId = trn_sampleinfomations_details.intCompanyId

WHERE
							mst_marketer_cluster.CLUSTER = 1";
        if ($vs_flag == 0) { //non vs
            $sql_current_month_top_marketers .= " AND mst_brand.CATEGORY 	!=1 ";
        } else {
            $sql_current_month_top_marketers .= " AND mst_brand.CATEGORY 		= 1 ";
        }
        $sql_current_month_top_marketers .= " AND costing_sample_header.COPPIED_FLAG = 0
        AND costing_sample_header_approved_by.APPROVED_DATE BETWEEN '$pervious_year-01-01 00:00:00'
        AND '$current_year-12-31 11:59:00'
        AND costing_sample_header_approved_by.`STATUS` = 0
	AND 
        MONTH (
                costing_sample_header_approved_by.APPROVED_DATE
                ) = MONTH(CURRENT_DATE())
                AND YEAR (
                costing_sample_header_approved_by.APPROVED_DATE
                ) = YEAR(CURRENT_DATE())
               /* AND MONTH (
                        costing_sample_header_approved_by.APPROVED_DATE
                ) = 6
                AND YEAR (
                        costing_sample_header_approved_by.APPROVED_DATE
                ) = 2018*/
                 AND mst_locations.intCompanyId in (1,7,36)
                 AND mst_marketer.`STATUS` = 1
                GROUP BY
                MONTH (
                        costing_sample_header_approved_by.APPROVED_DATE
                ),
                trn_sampleinfomations.intSampleNo
					) AS t
				GROUP BY
					t.month1,
					t.`year`,
					t.intUserId
				ORDER BY
					t.strUserName,
					t.month1,
					t.`year`
			) AS m
		GROUP BY
			m.`user`
)AS w
		ORDER BY
			w.count DESC
LIMIT 10) AS z	";
            //echo $sql_current_month_top_marketers;
            $result_top	  = 	$this->db->RunQuery($sql_current_month_top_marketers);
            $row_top	  =	mysqli_fetch_array($result_top);
            $top_marketer =     $row_top['top_marketer'];

        $sql_sum = "SELECT
	SUM(r.sum_count)as sum_count
FROM
	(
		SELECT
			SUM(m.count) AS sum_count,
			m.`user`
		FROM
			(
				SELECT
					count(t.intSampleNo) AS count,
					t. YEAR,
					t.month1 AS MONTH,
					t.intUserId,
					t.strUserName AS USER
				FROM
					(
						SELECT
							trn_sampleinfomations.intSampleNo,
							trn_sampleinfomations.intRevisionNo AS rev,
							costing_sample_header.COPPIED_FLAG,
							costing_sample_header_approved_by.APPROVED_DATE,
							YEAR (
								costing_sample_header_approved_by.APPROVED_DATE
							) AS YEAR,
							MONTH (
								costing_sample_header_approved_by.APPROVED_DATE
							) AS month1,
							mst_brand.strName,
							sys_users.strUserName,
							mst_marketer.intUserId,
							costing_sample_header_approved_by.`STATUS`
						FROM
							mst_marketer
						INNER JOIN sys_users ON sys_users.intUserId = mst_marketer.intUserId
						INNER JOIN mst_marketer_cluster ON mst_marketer_cluster.MARKETER = mst_marketer.intUserId
						INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intMarketer = mst_marketer.intUserId
						INNER JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
						INNER JOIN costing_sample_header ON trn_sampleinfomations.intSampleNo = costing_sample_header.SAMPLE_NO
						AND trn_sampleinfomations.intSampleYear = costing_sample_header.SAMPLE_YEAR
						AND trn_sampleinfomations.intRevisionNo = costing_sample_header.REVISION
						INNER JOIN costing_sample_header_approved_by ON costing_sample_header_approved_by.SAMPLE_NO = costing_sample_header.SAMPLE_NO
						AND costing_sample_header_approved_by.SAMPLE_YEAR = costing_sample_header.SAMPLE_YEAR
						AND costing_sample_header_approved_by.REVISION_NO = costing_sample_header.REVISION
						AND costing_sample_header_approved_by.COMBO = costing_sample_header.COMBO
						AND costing_sample_header_approved_by.PRINT = costing_sample_header.PRINT
				            INNER JOIN trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
                                            AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
                                            AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo 
                                            INNER JOIN mst_locations ON mst_locations.intId = trn_sampleinfomations_details.intCompanyId
WHERE
						mst_marketer_cluster.CLUSTER = 1";
        if ($vs_flag == 0) {
            $sql_sum .= " AND mst_brand.CATEGORY != 1";
        } else {
            $sql_sum .= " AND mst_brand.CATEGORY = 1";
        }
        $sql_sum .= " AND costing_sample_header.COPPIED_FLAG = 0
						AND costing_sample_header_approved_by.APPROVED_DATE BETWEEN '$pervious_year-01-01 00:00:00'
						AND '$current_year-12-31 11:59:00'
						AND costing_sample_header_approved_by.`STATUS` = 0
						AND MONTH (
							costing_sample_header_approved_by.APPROVED_DATE
						) = $j
						AND YEAR (
							costing_sample_header_approved_by.APPROVED_DATE
						) = $year 
						AND mst_marketer.intUserId IN ($top_marketer)
                                                 AND mst_locations.intCompanyId in (1,7,36)
                                                 AND mst_marketer.`STATUS` = 1
                                                GROUP BY
							MONTH (
								costing_sample_header_approved_by.APPROVED_DATE
							),
							trn_sampleinfomations.intSampleNo
					) AS t
				GROUP BY
					t.month1,
					t.`year`,
					t.intUserId
				ORDER BY
					t.strUserName,
					t.month1,
					t.`year`
			) AS m
		GROUP BY
			m.`user` 
	) AS r";



       //echo $sql_sum;
        $result_sum = $this->db->RunQuery($sql_sum);
        $row_sum = mysqli_fetch_array($result_sum);
        $sum_of_users = $row_sum['sum_count'];


        $sql_tot = "SELECT
	SUM(m.count) as sum
FROM
	(
		SELECT
			count(t.intSampleNo) AS count,
			t. YEAR,
			t.month1 AS MONTH,
			t.intUserId,
			t.strUserName AS USER
		FROM
			(
				SELECT
					trn_sampleinfomations.intSampleNo,
					trn_sampleinfomations.intRevisionNo AS rev,
					costing_sample_header.COPPIED_FLAG,
					costing_sample_header_approved_by.APPROVED_DATE,
					YEAR (
						costing_sample_header_approved_by.APPROVED_DATE
					) AS YEAR,
					MONTH (
						costing_sample_header_approved_by.APPROVED_DATE
					) AS month1,
					mst_brand.strName,
					sys_users.strUserName,
					mst_marketer.intUserId,
					costing_sample_header_approved_by.`STATUS`
				FROM
					mst_marketer
				INNER JOIN sys_users ON sys_users.intUserId = mst_marketer.intUserId
				INNER JOIN mst_marketer_cluster ON mst_marketer_cluster.MARKETER = mst_marketer.intUserId
				INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intMarketer = mst_marketer.intUserId
				INNER JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
				INNER JOIN costing_sample_header ON trn_sampleinfomations.intSampleNo = costing_sample_header.SAMPLE_NO
				AND trn_sampleinfomations.intSampleYear = costing_sample_header.SAMPLE_YEAR
				AND trn_sampleinfomations.intRevisionNo = costing_sample_header.REVISION
				INNER JOIN costing_sample_header_approved_by ON costing_sample_header_approved_by.SAMPLE_NO = costing_sample_header.SAMPLE_NO
				AND costing_sample_header_approved_by.SAMPLE_YEAR = costing_sample_header.SAMPLE_YEAR
				AND costing_sample_header_approved_by.REVISION_NO = costing_sample_header.REVISION
				AND costing_sample_header_approved_by.COMBO = costing_sample_header.COMBO
				AND costing_sample_header_approved_by.PRINT = costing_sample_header.PRINT
				INNER JOIN trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
                                            AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
                                            AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo 
                                            INNER JOIN mst_locations ON mst_locations.intId = trn_sampleinfomations_details.intCompanyId
WHERE
					mst_marketer_cluster.CLUSTER = 1";
        if ($vs_flag == 0) {     //non vs
            $sql_tot .= " AND mst_brand.CATEGORY != 1";
        } else {
            $sql_tot .= " AND mst_brand.CATEGORY = 1";
        }
        $sql_tot .= " AND costing_sample_header.COPPIED_FLAG = 0
				AND costing_sample_header_approved_by.APPROVED_DATE BETWEEN '$pervious_year-01-01 00:00:00'
				AND '$current_year-12-31 11:59:00'
				AND costing_sample_header_approved_by.`STATUS` = 0
				AND MONTH (
					costing_sample_header_approved_by.APPROVED_DATE
				) = $j
				AND YEAR (
					costing_sample_header_approved_by.APPROVED_DATE
				) = $year
                                 AND mst_locations.intCompanyId in (1,7,36)
                                 -- AND mst_marketer.`STATUS` = 1
				GROUP BY
					MONTH (
						costing_sample_header_approved_by.APPROVED_DATE
					),
					trn_sampleinfomations.intSampleNo
			) AS t
		GROUP BY
			t.month1,
			t.`year`,
			t.intUserId
		ORDER BY
			t.strUserName,
			t.month1,
			t.`year`
	) AS m";
         //echo $sql_tot;
        $result_tot = $this->db->RunQuery($sql_tot);
        $row_tot = mysqli_fetch_array($result_tot);
        $total_count = $row_tot['sum'];

        $other_tot = $total_count - $sum_of_users;

        return $other_tot;
    }

    function get_brand_other($year, $pervious_year, $current_year, $j, $vs_flag) {
         
        $sql_current_month_top_brand = "SELECT
	GROUP_CONCAT(z.brandId) as top_brand
FROM
	(
SELECT
			w.brandId AS brandId,
			w.count
		FROM
			(
		SELECT
			m.brandId,
			sum(m.count) as count
		FROM
			(
				SELECT
					count(t.intSampleNo) AS count,
					t. YEAR,
					t.month1 AS MONTH,
					t.brand,
					t.brandId
				FROM
					(
						SELECT
							trn_sampleinfomations.intSampleNo,
							trn_sampleinfomations.intRevisionNo AS rev,
							costing_sample_header.COPPIED_FLAG,
							costing_sample_header_approved_by.APPROVED_DATE,
							YEAR (
								costing_sample_header_approved_by.APPROVED_DATE
							) AS YEAR,
							MONTH (
								costing_sample_header_approved_by.APPROVED_DATE
							) AS month1,
							mst_brand.strName AS brand,
							mst_brand.intId AS brandId,
							costing_sample_header_approved_by.`STATUS`
						FROM
							mst_marketer
						INNER JOIN sys_users ON sys_users.intUserId = mst_marketer.intUserId
						INNER JOIN mst_marketer_cluster ON mst_marketer_cluster.MARKETER = mst_marketer.intUserId
						INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intMarketer = mst_marketer.intUserId
						INNER JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
						INNER JOIN costing_sample_header ON trn_sampleinfomations.intSampleNo = costing_sample_header.SAMPLE_NO
						AND trn_sampleinfomations.intSampleYear = costing_sample_header.SAMPLE_YEAR
						AND trn_sampleinfomations.intRevisionNo = costing_sample_header.REVISION
						INNER JOIN costing_sample_header_approved_by ON costing_sample_header_approved_by.SAMPLE_NO = costing_sample_header.SAMPLE_NO
						AND costing_sample_header_approved_by.SAMPLE_YEAR = costing_sample_header.SAMPLE_YEAR
						AND costing_sample_header_approved_by.REVISION_NO = costing_sample_header.REVISION
						AND costing_sample_header_approved_by.COMBO = costing_sample_header.COMBO
						AND costing_sample_header_approved_by.PRINT = costing_sample_header.PRINT
						INNER JOIN trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
                                            AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
                                            AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo 
                                            INNER JOIN mst_locations ON mst_locations.intId = trn_sampleinfomations_details.intCompanyId

WHERE
							mst_marketer_cluster.CLUSTER = 1";
						 if ($vs_flag == 0) { //non vs
            $sql_current_month_top_brand .= " AND mst_brand.CATEGORY 	!=1 ";
        } else {
            $sql_current_month_top_brand .= " AND mst_brand.CATEGORY 		= 1 ";
    }
						$sql_current_month_top_brand.= " AND costing_sample_header.COPPIED_FLAG = 0
						AND costing_sample_header_approved_by.APPROVED_DATE BETWEEN '$pervious_year-01-01 00:00:00'
						AND '$current_year-12-31 11:59:00'
						AND costing_sample_header_approved_by.`STATUS` = 0
                                                AND MONTH (
                costing_sample_header_approved_by.APPROVED_DATE
                ) = MONTH(CURRENT_DATE())
                AND YEAR (
                costing_sample_header_approved_by.APPROVED_DATE
                ) = YEAR(CURRENT_DATE())
                /*AND MONTH (
                        costing_sample_header_approved_by.APPROVED_DATE
                ) = 6
                AND YEAR (
                        costing_sample_header_approved_by.APPROVED_DATE
                ) = 2018*/
                 AND mst_locations.intCompanyId in (1,7,36)
                 -- AND mst_marketer.`STATUS` = 1
						GROUP BY
							MONTH (
								costing_sample_header_approved_by.APPROVED_DATE
							),
							trn_sampleinfomations.intSampleNo
					) AS t
				GROUP BY
					t.month1,
					t.`year`,
					t.brandId
				ORDER BY
					t.brandId,
					t.month1,
					t.`year`
			) AS m
		GROUP BY
			m.brandId
) AS w
		ORDER BY
			w.count DESC
		LIMIT 10
		) AS z";
                     //echo   $sql_current_month_top_brand;                         
            $result_top	  = 	$this->db->RunQuery($sql_current_month_top_brand);
            $row_top	  =	mysqli_fetch_array($result_top);
            $top_brand    =     $row_top['top_brand']; 

        $sql_sum = "SELECT
SUM(r.all_sum) as all_count
FROM
(

SELECT
sum(m.count) as all_sum,
m.brandId,
m.brand
FROM
(

SELECT
	count(t.intSampleNo) AS count,
	t. YEAR,
	t.month1 AS MONTH,
	t.brand,
	t.brandId
FROM
	(
		SELECT
			trn_sampleinfomations.intSampleNo,
			trn_sampleinfomations.intRevisionNo AS rev,
			costing_sample_header.COPPIED_FLAG,
			costing_sample_header_approved_by.APPROVED_DATE,
			YEAR (
				costing_sample_header_approved_by.APPROVED_DATE
			) AS YEAR,
			MONTH (
				costing_sample_header_approved_by.APPROVED_DATE
			) AS month1,
			mst_brand.strName AS brand,
			mst_brand.intId AS brandId,
			costing_sample_header_approved_by.`STATUS`
		FROM
			mst_marketer
		INNER JOIN sys_users ON sys_users.intUserId = mst_marketer.intUserId
		INNER JOIN mst_marketer_cluster ON mst_marketer_cluster.MARKETER = mst_marketer.intUserId
		INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intMarketer = mst_marketer.intUserId
		INNER JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
		INNER JOIN costing_sample_header ON trn_sampleinfomations.intSampleNo = costing_sample_header.SAMPLE_NO
		AND trn_sampleinfomations.intSampleYear = costing_sample_header.SAMPLE_YEAR
		AND trn_sampleinfomations.intRevisionNo = costing_sample_header.REVISION
		INNER JOIN costing_sample_header_approved_by ON costing_sample_header_approved_by.SAMPLE_NO = costing_sample_header.SAMPLE_NO
		AND costing_sample_header_approved_by.SAMPLE_YEAR = costing_sample_header.SAMPLE_YEAR
		AND costing_sample_header_approved_by.REVISION_NO = costing_sample_header.REVISION
		AND costing_sample_header_approved_by.COMBO = costing_sample_header.COMBO
		AND costing_sample_header_approved_by.PRINT = costing_sample_header.PRINT
		INNER JOIN trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
                                            AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
                                            AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo 
                                            INNER JOIN mst_locations ON mst_locations.intId = trn_sampleinfomations_details.intCompanyId
WHERE 
                mst_marketer_cluster.CLUSTER = 1";
        if ($vs_flag == 0) {     //non vs
            $sql_sum .= " AND mst_brand.CATEGORY != 1";
        } else {
            $sql_sum .= " AND mst_brand.CATEGORY = 1";
        }
        $sql_sum .= " AND costing_sample_header.COPPIED_FLAG = 0
		AND costing_sample_header_approved_by.APPROVED_DATE BETWEEN '$pervious_year-01-01 00:00:00'
		AND '$current_year-12-31 11:59:00'
		AND costing_sample_header_approved_by.`STATUS` = 0
                AND MONTH (
                costing_sample_header_approved_by.APPROVED_DATE
                ) = $j
                AND YEAR (
                costing_sample_header_approved_by.APPROVED_DATE
                ) = $year 
                 AND mst_brand.intId IN ($top_brand)
                  AND mst_locations.intCompanyId in (1,7,36)
                  -- AND mst_marketer.`STATUS` = 1
		GROUP BY
			MONTH (
				costing_sample_header_approved_by.APPROVED_DATE
			),
			trn_sampleinfomations.intSampleNo
	) AS t
GROUP BY
	t.month1,
	t.`year`,
	t.brandId
ORDER BY
	t.brandId,
	t.month1,
	t.`year`) as m

GROUP BY m.brandId

	) as r";
        //echo $sql_sum;
        $result_sum = $this->db->RunQuery($sql_sum);
        $row_sum = mysqli_fetch_array($result_sum);
        $sum_of_brands = $row_sum['all_count'];


        $sql_tot = "
SELECT SUM(m.count) AS sum
FROM
(

SELECT
	count(t.intSampleNo) AS count,
	t.YEAR,
	t.month1 AS MONTH,
	t.brand,
	t.brandId
FROM
	(
		SELECT
			trn_sampleinfomations.intSampleNo,
			trn_sampleinfomations.intRevisionNo AS rev,
			costing_sample_header.COPPIED_FLAG,
			costing_sample_header_approved_by.APPROVED_DATE,
			YEAR (
				costing_sample_header_approved_by.APPROVED_DATE
			) AS YEAR,
			MONTH (
				costing_sample_header_approved_by.APPROVED_DATE
			) AS month1,
			mst_brand.strName AS brand,
			mst_brand.intId AS brandId,
			-- sys_users.strUserName,
			-- mst_marketer.intUserId,
			costing_sample_header_approved_by.`STATUS`
		FROM
			mst_marketer
		INNER JOIN sys_users ON sys_users.intUserId = mst_marketer.intUserId
		INNER JOIN mst_marketer_cluster ON mst_marketer_cluster.MARKETER = mst_marketer.intUserId
		INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intMarketer = mst_marketer.intUserId
		INNER JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
		INNER JOIN costing_sample_header ON trn_sampleinfomations.intSampleNo = costing_sample_header.SAMPLE_NO
		AND trn_sampleinfomations.intSampleYear = costing_sample_header.SAMPLE_YEAR
		AND trn_sampleinfomations.intRevisionNo = costing_sample_header.REVISION
		INNER JOIN costing_sample_header_approved_by ON costing_sample_header_approved_by.SAMPLE_NO = costing_sample_header.SAMPLE_NO
		AND costing_sample_header_approved_by.SAMPLE_YEAR = costing_sample_header.SAMPLE_YEAR
		AND costing_sample_header_approved_by.REVISION_NO = costing_sample_header.REVISION
		AND costing_sample_header_approved_by.COMBO = costing_sample_header.COMBO
		AND costing_sample_header_approved_by.PRINT = costing_sample_header.PRINT
		INNER JOIN trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
                                            AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
                                            AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo 
                                            INNER JOIN mst_locations ON mst_locations.intId = trn_sampleinfomations_details.intCompanyId
WHERE
                mst_marketer_cluster.CLUSTER = 1";
        if ($vs_flag == 0) {  //non vs
            $sql_tot .= " AND mst_brand.CATEGORY != 1";
        } else {
            $sql_tot .= " AND mst_brand.CATEGORY = 1";
        }
        $sql_tot .= " AND costing_sample_header.COPPIED_FLAG = 0
		AND costing_sample_header_approved_by.APPROVED_DATE BETWEEN '$pervious_year-01-01 00:00:00'
		AND '$current_year-12-31 11:59:00'
		AND costing_sample_header_approved_by.`STATUS` = 0 
		AND MONTH (costing_sample_header_approved_by.APPROVED_DATE) = $j
		AND YEAR (costing_sample_header_approved_by.APPROVED_DATE) = $year
                 AND mst_locations.intCompanyId in (1,7,36)
                 -- AND mst_marketer.`STATUS` = 1
		GROUP BY
			MONTH (
				costing_sample_header_approved_by.APPROVED_DATE
			),
			trn_sampleinfomations.intSampleNo
	) AS t
GROUP BY
	t.month1,
	t.`year`,
	t.brandId
ORDER BY
	t.brandId,t.month1,t.`year`
) as m";
        //echo  $sql_tot;
        $result_tot = $this->db->RunQuery($sql_tot);
        $row_tot = mysqli_fetch_array($result_tot);
        $total_count = $row_tot['sum'];

        $other_tot = $total_count - $sum_of_brands;


        return $other_tot;
    }

    function get_cus_other($year, $pervious_year, $current_year, $j, $vs_flag) {

        $sql_current_month_top_customers = "
SELECT
GROUP_CONCAT(z.cusID) as top_customers
FROM
(
SELECT
        w.cus_id AS cusID
FROM
(
SELECT
SUM(count) AS count_sum,
m.cusId AS cus_id,
m.cusName
FROM
(
SELECT
count(t.intSampleNo) AS count,
t. YEAR,
t.month1 AS MONTH,
t.cusId,
t.cusName
FROM
(
SELECT
trn_sampleinfomations.intSampleNo,
trn_sampleinfomations.intRevisionNo AS rev,
costing_sample_header.COPPIED_FLAG,
costing_sample_header_approved_by.APPROVED_DATE,
YEAR (
        costing_sample_header_approved_by.APPROVED_DATE
) AS YEAR,
MONTH (
        costing_sample_header_approved_by.APPROVED_DATE
) AS month1,
mst_customer.intId AS cusId,
mst_customer.strName AS cusName,
costing_sample_header_approved_by.`STATUS`
FROM
mst_marketer
INNER JOIN sys_users ON sys_users.intUserId = mst_marketer.intUserId
INNER JOIN mst_marketer_cluster ON mst_marketer_cluster.MARKETER = mst_marketer.intUserId
INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intMarketer = mst_marketer.intUserId
INNER JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
INNER JOIN costing_sample_header ON trn_sampleinfomations.intSampleNo = costing_sample_header.SAMPLE_NO
AND trn_sampleinfomations.intSampleYear = costing_sample_header.SAMPLE_YEAR
AND trn_sampleinfomations.intRevisionNo = costing_sample_header.REVISION
INNER JOIN costing_sample_header_approved_by ON costing_sample_header_approved_by.SAMPLE_NO = costing_sample_header.SAMPLE_NO
AND costing_sample_header_approved_by.SAMPLE_YEAR = costing_sample_header.SAMPLE_YEAR
AND costing_sample_header_approved_by.REVISION_NO = costing_sample_header.REVISION
AND costing_sample_header_approved_by.COMBO = costing_sample_header.COMBO
AND costing_sample_header_approved_by.PRINT = costing_sample_header.PRINT
INNER JOIN mst_customer ON mst_customer.intId = trn_sampleinfomations.intCustomer
INNER JOIN trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo 
INNER JOIN mst_locations ON mst_locations.intId = trn_sampleinfomations_details.intCompanyId
WHERE
        mst_marketer_cluster.CLUSTER = 1";
        if ($vs_flag == 0) { //non vs
            $sql_current_month_top_customers .= " AND mst_brand.CATEGORY != 1";
        } else {
            $sql_current_month_top_customers .= " AND mst_brand.CATEGORY = 1";
        }
        $sql_current_month_top_customers .= "  AND costing_sample_header.COPPIED_FLAG = 0
AND costing_sample_header_approved_by.APPROVED_DATE BETWEEN '$pervious_year-01-01 00:00:00'
AND '$current_year-12-31 11:59:00'
AND costing_sample_header_approved_by.`STATUS` = 0
AND MONTH (
costing_sample_header_approved_by.APPROVED_DATE
) = MONTH(CURRENT_DATE())
AND YEAR (
costing_sample_header_approved_by.APPROVED_DATE
) = YEAR(CURRENT_DATE())
/*AND MONTH (
    costing_sample_header_approved_by.APPROVED_DATE
) = 6
AND YEAR (
    costing_sample_header_approved_by.APPROVED_DATE
) = 2018*/
 AND mst_locations.intCompanyId in (1,7,36)
 -- AND mst_marketer.`STATUS` = 1
GROUP BY
    MONTH (
            costing_sample_header_approved_by.APPROVED_DATE
    ),
    trn_sampleinfomations.intSampleNo
) AS t
GROUP BY
t.month1,
t.`year`,
t.cusId
ORDER BY
t.cusId,
t.month1,
t.`year`
) AS m
GROUP BY
        m.cusId
) AS w
ORDER BY
w.count_sum DESC
LIMIT 10
) AS z";
        //echo $sql_current_month_top_customers;
        $result_limit = $this->db->RunQuery($sql_current_month_top_customers);
        $row_limit = mysqli_fetch_array($result_limit);
        $customers = $row_limit['top_customers'];

        $sql_sum = "SELECT
SUM(r.count_all) as all_sum
FROM
(
SELECT
sum(m.count) as count_all,
m.cusId as cus_id
FROM
(

SELECT
	count(t.intSampleNo) AS count,
	t. YEAR,
	t.month1 AS MONTH,
	t.cusId,
	t.cusName
FROM
	(
		SELECT
			trn_sampleinfomations.intSampleNo,
			trn_sampleinfomations.intRevisionNo AS rev,
			costing_sample_header.COPPIED_FLAG,
			costing_sample_header_approved_by.APPROVED_DATE,
			YEAR (
				costing_sample_header_approved_by.APPROVED_DATE
			) AS YEAR,
			MONTH (
				costing_sample_header_approved_by.APPROVED_DATE
			) AS month1,
			mst_customer.intId as cusId,
			mst_customer.strName as cusName,
			costing_sample_header_approved_by.`STATUS`
		FROM
			mst_marketer
		INNER JOIN sys_users ON sys_users.intUserId = mst_marketer.intUserId
		INNER JOIN mst_marketer_cluster ON mst_marketer_cluster.MARKETER = mst_marketer.intUserId
		INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intMarketer = mst_marketer.intUserId
		INNER JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
		INNER JOIN costing_sample_header ON trn_sampleinfomations.intSampleNo = costing_sample_header.SAMPLE_NO
		AND trn_sampleinfomations.intSampleYear = costing_sample_header.SAMPLE_YEAR
		AND trn_sampleinfomations.intRevisionNo = costing_sample_header.REVISION
		INNER JOIN costing_sample_header_approved_by ON costing_sample_header_approved_by.SAMPLE_NO = costing_sample_header.SAMPLE_NO
		AND costing_sample_header_approved_by.SAMPLE_YEAR = costing_sample_header.SAMPLE_YEAR
		AND costing_sample_header_approved_by.REVISION_NO = costing_sample_header.REVISION
		AND costing_sample_header_approved_by.COMBO = costing_sample_header.COMBO
		AND costing_sample_header_approved_by.PRINT = costing_sample_header.PRINT
		INNER JOIN mst_customer on mst_customer.intId = trn_sampleinfomations.intCustomer
		INNER JOIN trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
                AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
                AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo 
                INNER JOIN mst_locations ON mst_locations.intId = trn_sampleinfomations_details.intCompanyId
                WHERE
                mst_marketer_cluster.CLUSTER = 1";

        if ($vs_flag == 0) {
            $sql_sum .= " AND mst_brand.CATEGORY != 1";
        } else {
            $sql_sum .= " AND mst_brand.CATEGORY = 1";
        }


        $sql_sum .= " AND costing_sample_header.COPPIED_FLAG = 0
		AND costing_sample_header_approved_by.APPROVED_DATE BETWEEN '$pervious_year-01-01 00:00:00'
		AND '$current_year-12-31 11:59:00'
		AND costing_sample_header_approved_by.`STATUS` = 0
                 AND MONTH (
                costing_sample_header_approved_by.APPROVED_DATE
                ) = $j
                AND YEAR (
                costing_sample_header_approved_by.APPROVED_DATE
                ) = $year 
		AND mst_customer.intId IN($customers)
		 AND mst_locations.intCompanyId in (1,7,36)
                 -- AND mst_marketer.`STATUS` = 1
		GROUP BY
			MONTH (
				costing_sample_header_approved_by.APPROVED_DATE
			),
			trn_sampleinfomations.intSampleNo
	) AS t
GROUP BY
	t.month1,
	t.`year`,
	t.cusId
ORDER BY
	
	t.cusId,t.month1,t.`year`
) as m

GROUP BY m.cusId

) as r";
        //echo $sql_sum;
        $result_sum = $this->db->RunQuery($sql_sum);
        $row_sum = mysqli_fetch_array($result_sum);
        $sum_of_cus = $row_sum['all_sum'];


        $sql_tot = "
SELECT
SUM(m.count) as sum
FROM
(
SELECT
	count(t.intSampleNo) AS count,
	t. YEAR,
	t.month1 AS MONTH,
	t.cusId,
	t.cusName
FROM
	(
		SELECT
			trn_sampleinfomations.intSampleNo,
			trn_sampleinfomations.intRevisionNo AS rev,
			costing_sample_header.COPPIED_FLAG,
			costing_sample_header_approved_by.APPROVED_DATE,
			YEAR (
				costing_sample_header_approved_by.APPROVED_DATE
			) AS YEAR,
			MONTH (
				costing_sample_header_approved_by.APPROVED_DATE
			) AS month1,
			mst_customer.intId as cusId,
			mst_customer.strName as cusName,
			costing_sample_header_approved_by.`STATUS`
		FROM
			mst_marketer
		INNER JOIN sys_users ON sys_users.intUserId = mst_marketer.intUserId
		INNER JOIN mst_marketer_cluster ON mst_marketer_cluster.MARKETER = mst_marketer.intUserId
		INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intMarketer = mst_marketer.intUserId
		INNER JOIN mst_brand ON trn_sampleinfomations.intBrand = mst_brand.intId
		INNER JOIN costing_sample_header ON trn_sampleinfomations.intSampleNo = costing_sample_header.SAMPLE_NO
		AND trn_sampleinfomations.intSampleYear = costing_sample_header.SAMPLE_YEAR
		AND trn_sampleinfomations.intRevisionNo = costing_sample_header.REVISION
		INNER JOIN costing_sample_header_approved_by ON costing_sample_header_approved_by.SAMPLE_NO = costing_sample_header.SAMPLE_NO
		AND costing_sample_header_approved_by.SAMPLE_YEAR = costing_sample_header.SAMPLE_YEAR
		AND costing_sample_header_approved_by.REVISION_NO = costing_sample_header.REVISION
		AND costing_sample_header_approved_by.COMBO = costing_sample_header.COMBO
		AND costing_sample_header_approved_by.PRINT = costing_sample_header.PRINT
		INNER JOIN mst_customer on mst_customer.intId = trn_sampleinfomations.intCustomer
                INNER JOIN trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_sampleinfomations.intSampleNo
                AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
                AND trn_sampleinfomations_details.intRevNo = trn_sampleinfomations.intRevisionNo 
                INNER JOIN mst_locations ON mst_locations.intId = trn_sampleinfomations_details.intCompanyId	
                WHERE
			mst_marketer_cluster.CLUSTER = 1";
        if ($vs_flag == 0) {
            $sql_tot .= " AND mst_brand.CATEGORY != 1";
        } else {
            $sql_tot .= " AND mst_brand.CATEGORY = 1 ";
        }
        $sql_tot .= " AND costing_sample_header.COPPIED_FLAG = 0
		AND costing_sample_header_approved_by.APPROVED_DATE BETWEEN '$pervious_year-01-01 00:00:00'
		AND '$current_year-12-31 11:59:00'
		AND costing_sample_header_approved_by.`STATUS` = 0
		AND MONTH (costing_sample_header_approved_by.APPROVED_DATE) = $j
		AND YEAR (costing_sample_header_approved_by.APPROVED_DATE) = $year
		 AND mst_locations.intCompanyId in (1,7,36)
                 -- AND mst_marketer.`STATUS` = 1
		GROUP BY
			MONTH (
				costing_sample_header_approved_by.APPROVED_DATE
			),
			trn_sampleinfomations.intSampleNo
	) AS t
GROUP BY
	t.month1,
	t.`year`,
	t.cusId
ORDER BY
	
	t.cusId,t.month1,t.`year` ) as m";
    //echo $sql_tot;
        $result_tot = $this->db->RunQuery($sql_tot);
        $row_tot = mysqli_fetch_array($result_tot);
        $total_count = $row_tot['sum'];

        $other_tot = $total_count - $sum_of_cus;


        return $other_tot;
    }

}


?>
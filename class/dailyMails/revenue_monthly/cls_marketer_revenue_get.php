<?php
 session_start();
include_once $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
include_once $_SESSION['ROOT_PATH']."class/cls_commonErrorHandeling_get.php";
  
$obj_commonErr		= new cls_commonErrorHandeling_get($db);
$obj_common			= new cls_commonFunctions_get($db);
 
class cls_marketer_revenue_get
{
	private $db;
 	
	function __construct($db)
	{
		$this->db 	= $db;
 	}

 	public function get_marketers_result($year,$month,$day,$plant,$cluster)
	{
 		$sql		= $this->load_marketers_sql($year,$month,$day,$plant,$cluster);
 		$result		= $this->db->RunQuery($sql);
 		return $result;
	}
 	public function get_cluster_marketers_result($date,$cluster)
	{
 		$sql		= $this->load_cluster_marketers_sql($date,$cluster);
 		$result		= $this->db->RunQuery($sql);
 		return $result;
	}
	public function getMarketerRevenue($marketerId,$plant,$month,$year,$day,$toCurrency)
	{
 		$sqlp		= $this->load_MarketerRevenue_sql($marketerId,$plant,$month,$year,$day,$toCurrency);
		$resultp	= $this->db->RunQuery($sqlp);
		$row 		= mysqli_fetch_array($resultp);
		return $row['REVENUE'];
 	}
	public function get_old_cluster_plant_revenue($plant,$date,$toCurrency)
	{
 		$sqlp		= $this->load_old_cluster_plant_revenue_sql($plant,$date,$toCurrency);
		$resultp	= $this->db->RunQuery($sqlp);
		$row 		= mysqli_fetch_array($resultp);
		return $row['REVENUE'];
 	}
	public function get_old_cluster_plant_monthly_revenue($plant,$year,$month,$toCurrency)
	{
 		$sqlp		= $this->load_old_cluster_plant_monthly_revenue_sql($plant,$year,$month,$toCurrency);
		$resultp	= $this->db->RunQuery($sqlp);
		$row 		= mysqli_fetch_array($resultp);
		return $row['REVENUE'];
 	}
	public function get_old_cluster_marketer_revenue($marketerId_list,$plant_list,$date,$toCurrency)
	{
 		$sqlp		= $this->load_old_cluster_marketer_revenue_sql($marketerId_list,$plant_list,$date,$toCurrency);
		$resultp	= $this->db->RunQuery($sqlp);
		$row 		= mysqli_fetch_array($resultp);
		return $row['REVENUE'];
 	}
	public function get_cluster_plants($cluster)
	{
 		$sqlp		= $this->load_cluster_plants_sql($cluster);
		$resultp	= $this->db->RunQuery($sqlp);
 		return $resultp;
 	}
	//2015-06-18
	public function get_main_cluster_plants($cluster,$year)
	{
 		$sqlp		= $this->load_main_cluster_plants_sql($cluster,$year);
		$resultp	= $this->db->RunQuery($sqlp);
 		return $resultp;
 	}
	public function get_sub_cluster_plants($sub_cluster,$year)
	{
 		$sqlp		= $this->load_sub_cluster_plants_sql($sub_cluster,$year);
		$resultp	= $this->db->RunQuery($sqlp);
 		return $resultp;
 	}
	
	public function get_plant_calender($plant,$date)
	{
 		$sqlp		= $this->load_plant_calender_sql($plant,$date);
		$resultp	= $this->db->RunQuery($sqlp);
 		return $resultp;
 	}
	
	
  
//BEGIN - PRIVATE FUNCTIONS {	
	private function load_marketers_sql($year,$month,$day,$plant,$cluster)
	{
 		if($month==12){
			$yearN		=$year+1;	
			$monthN		=1;
		}
		else{
			$yearN		=$year;	
			$monthN		=$month+1;	
		}
		$minDate		=$year.'-'.sprintf("%02d",$month).'-01';
		$maxDate		=$yearN.'-'.sprintf("%02d",$monthN).'-01';
	  	 
		   $sql = "SELECT * FROM (SELECT
					mst_marketer.intUserId,
					sys_users.strFullName,
					mst_marketer_cluster_target.ANUAL_TARGET as dblAnualaTarget,
					mst_marketer_cluster_target.MONTHLY_TARGET as dblMonthlyTarget , 
					(SELECT
					Sum(daily_mails_revenue_old.REVENUE)
					FROM
					daily_mails_revenue_old  
 					WHERE 
					daily_mails_revenue_old.PLANT_ID = $plant AND 
					daily_mails_revenue_old.MARKETER_ID = mst_marketer.intUserId  AND 
					daily_mails_revenue_old.DATE >= $minDate AND 
					daily_mails_revenue_old.DATE < $maxDate  
					) as TOT_REVENUE
 					FROM
					mst_marketer
					INNER JOIN sys_users ON mst_marketer.intUserId = sys_users.intUserId 
 					LEFT JOIN mst_marketer_cluster_target ON mst_marketer.intUserId = mst_marketer_cluster_target.MARKETER_ID 
					AND mst_marketer_cluster_target.YEAR='$year'  AND mst_marketer_cluster_target.CLUSTER_ID='$cluster'
 					WHERE 
					/*sys_users.intStatus = 1 AND */
					FIND_IN_SET('$cluster',mst_marketer.REVENUE_TYPES)
 					) AS TB1 
					ORDER BY 
					TB1.TOT_REVENUE DESC,
					TB1.strFullName ASC
			";
  		return $sql;
 	}
	
//BEGIN - PRIVATE FUNCTIONS {	
	private function load_cluster_marketers_sql($date,$cluster)
	{
 		list($year, $month, $day) = explode('-', $date);
		
		if($month==12){
			$yearN		=$year+1;	
			$monthN		=1;
		}
		else{
			$yearN		=$year;	
			$monthN		=$month+1;	
		}
		$minDate		=$year.'-'.sprintf("%02d",$month).'-01';
		$maxDate		=$yearN.'-'.sprintf("%02d",$monthN).'-01';
	  	 
		   $sql = "SELECT * FROM (SELECT 
 					mst_marketer.intUserId,
					sys_users.strFullName,
					IF(((mst_marketer.OTHER_CATEGORY_FLAG=1) || (sys_users.intStatus <> 1)),'Others',sys_users.strUserName) AS marketerName,
					IF(((mst_marketer.OTHER_CATEGORY_FLAG=1) || (sys_users.intStatus <> 1)),2,1) as orderId,
					IF(((mst_marketer.OTHER_CATEGORY_FLAG=1) || (sys_users.intStatus <> 1)),1,mst_marketer.OTHER_CATEGORY_FLAG) AS OTHER_CATEGORY_FLAG,
					mst_marketer_cluster_target.ANUAL_TARGET,
					mst_marketer_cluster_target.MONTHLY_TARGET , 
					(mst_marketer_cluster_target.MONTHLY_TARGET/26) as dayTarget,
					(SELECT GROUP_CONCAT(mst_plant.intPlantId) as plantList FROM `mst_plant` WHERE mst_plant.REVENUE_TYPE = '$cluster') as plant_list,
					(SELECT
					Sum(daily_mails_revenue_old.REVENUE)
					FROM
					daily_mails_revenue_old  
 					WHERE 
					FIND_IN_SET(daily_mails_revenue_old.PLANT_ID,(SELECT GROUP_CONCAT(mst_plant.intPlantId) as plantList FROM `mst_plant` WHERE mst_plant.REVENUE_TYPE = '$cluster')) AND 
					daily_mails_revenue_old.MARKETER_ID = mst_marketer.intUserId  AND 
					daily_mails_revenue_old.DATE >= '$minDate' AND 
					daily_mails_revenue_old.DATE < '$maxDate' 
					) as TOT_REVENUE
 					FROM
					mst_marketer
					INNER JOIN sys_users ON mst_marketer.intUserId = sys_users.intUserId 
 					LEFT JOIN mst_marketer_cluster_target ON mst_marketer.intUserId = mst_marketer_cluster_target.MARKETER_ID 
					AND mst_marketer_cluster_target.YEAR='$year'  AND mst_marketer_cluster_target.CLUSTER_ID='$cluster'
 					WHERE  
					/*mst_marketer_cluster_target.OTHER_CATEGORY_FLAT <>1 AND*/ 
					/*sys_users.intStatus = 1 AND */
					FIND_IN_SET('$cluster',mst_marketer.REVENUE_TYPES)
 					) AS TB1 
					ORDER BY 
					TB1.orderId ASC,
					TB1.TOT_REVENUE DESC,
					TB1.marketerName ASC
			";
  		return $sql;
 	}
 
 	private function load_MarketerRevenue_sql($marketerId,$plant,$month,$year,$day,$toCurrency){
		
		$date		=$year.'-'.sprintf("%02d",$month).'-'.$day;
 		
		$sql = "select 
				IFNULL(sum(tb1.Qty1),0) as REVENUE
				from (SELECT 
				Sum(ware_stocktransactions_fabric.dblQty*-1*trn_orderdetails.dblPrice) AS Qty1,
				date(trn_orderheader.dtDate) as date,
				mst_locations.intCompanyId 
				FROM
				ware_stocktransactions_fabric
				left JOIN mst_locations ON ware_stocktransactions_fabric.intLocationId = mst_locations.intId
				left JOIN mst_plant ON mst_locations.intPlant = mst_plant.intPlantId
				left JOIN trn_orderheader ON ware_stocktransactions_fabric.intOrderNo = trn_orderheader.intOrderNo AND ware_stocktransactions_fabric.intOrderYear = trn_orderheader.intOrderYear
				left JOIN trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear AND ware_stocktransactions_fabric.intSalesOrderId = trn_orderdetails.intSalesOrderId
				left JOIN mst_marketer ON trn_orderheader.intMarketer = mst_marketer.intUserId
				left JOIN mst_financeexchangerate ON trn_orderheader.intCurrency = mst_financeexchangerate.intCurrencyId AND date(trn_orderheader.dtDate) = mst_financeexchangerate.dtmDate AND mst_locations.intCompanyId = mst_financeexchangerate.intCompanyId
				WHERE  
				mst_locations.intPlant = $plant		 AND
				/*mst_plant.REVENUE_TYPE='$revenueType' AND */
				mst_marketer.intUserId 						IN  ($marketerId)				 AND
				date(ware_stocktransactions_fabric.dtDate) 	= '$date' AND
				ware_stocktransactions_fabric.strType 		= 'Dispatched_G'
				
				GROUP BY date(trn_orderheader.dtDate),mst_locations.intCompanyId
				)
				 as tb1 
				left JOIN mst_financeexchangerate ON $toCurrency = mst_financeexchangerate.intCurrencyId 
				AND tb1.date 								= mst_financeexchangerate.dtmDate 
				AND tb1.intCompanyId 						= mst_financeexchangerate.intCompanyId
				";	
			
			return $sql;
	}
	
 	private function load_old_cluster_plant_revenue_sql($plant,$date,$toCurrency){
		
 	$sql = "SELECT
			Sum(daily_mails_revenue_old.REVENUE) AS REVENUE
			FROM `daily_mails_revenue_old`
			WHERE
			daily_mails_revenue_old.PLANT_ID = '$plant' AND
			daily_mails_revenue_old.DATE = '$date'";	
			
			return $sql;
	}
 	private function load_old_cluster_plant_monthly_revenue_sql($plant,$year,$month,$toCurrency){
		
 	$sql = "SELECT
			Sum(daily_mails_revenue_old.REVENUE) AS REVENUE
			FROM `daily_mails_revenue_old`
			WHERE
			daily_mails_revenue_old.PLANT_ID = '$plant' AND 
			year(daily_mails_revenue_old.DATE) = '$year' AND 	
			month(daily_mails_revenue_old.DATE) = '$month'";	
			
			return $sql;
	}
	
 	private function load_old_cluster_marketer_revenue_sql($marketerId_list,$plant_list,$date,$toCurrency){
		
 		$sql = "SELECT
			Sum(daily_mails_revenue_old.REVENUE) AS REVENUE
			FROM `daily_mails_revenue_old`
			WHERE
			FIND_IN_SET(daily_mails_revenue_old.MARKETER_ID,'$marketerId_list') AND
			FIND_IN_SET(daily_mails_revenue_old.PLANT_ID,'$plant_list') AND
			daily_mails_revenue_old.DATE = '$date'";	
			
			return $sql;
	}
	
	private function load_cluster_plants_sql($cluster){
		
		$sql = "SELECT
			mst_plant.intPlantId,
			mst_plant.strPlantName,
			mst_plant.dblAnualaTarget,
			mst_plant.dblMonthlyTarget,
			mst_plant.dblDayTarget
			FROM `mst_plant`
			where (mst_plant.intStatus=1 or mst_plant.intStatus=2)
			AND mst_plant.REVENUE_TYPE='$cluster'
			ORDER BY
			mst_plant.intPlantId ASC";
			
			return $sql;
	}
	
	private function load_main_cluster_plants_sql($cluster,$year){
		
		$sql = "SELECT 
				mst_plant.intPlantId, 
				mst_plant_targets.TARGET_ANUAL,
				mst_plant_targets.TARGET_MONTHLY,
				mst_plant_targets.TARGET_DAILY,
				mst_plant.strPlantName,
				mst_plant.strPlantHeadName,
				mst_plant.strPlantHeadContactNo
				FROM
				mst_plant
				LEFT JOIN mst_plant_targets ON mst_plant.intPlantId = mst_plant_targets.PLANT_ID
				WHERE 
				mst_plant.intStatus = 1 AND 
				mst_plant.MAIN_CLUSTER_ID = '$cluster' AND
				mst_plant_targets.`YEAR` = '$year'
			ORDER BY
			mst_plant.intPlantId ASC";
			
			return $sql;
	}
	
	private function load_sub_cluster_plants_sql($sub_cluster,$year){
		
		$sql = "SELECT 
				mst_plant.intPlantId, 
				mst_plant_targets.TARGET_ANUAL,
				mst_plant_targets.TARGET_MONTHLY,
				mst_plant_targets.TARGET_DAILY,
				mst_plant.strPlantName,
				mst_plant.strPlantHeadName,
				mst_plant.strPlantHeadContactNo
				FROM
				mst_plant
				LEFT JOIN mst_plant_targets ON mst_plant.intPlantId = mst_plant_targets.PLANT_ID  AND mst_plant_targets.`YEAR` = '$year' 
				WHERE 
				mst_plant.intStatus = 1 AND 
				mst_plant.SUB_CLUSTER_ID = '$sub_cluster'
			ORDER BY
			mst_plant.intPlantId ASC";
			
			return $sql;
	}
	
	
	private function load_plant_calender_sql($plant,$date){
		
		$sql = "SELECT
						mst_plant_calender.dtDate,
						mst_plant_calender.intDayType
						FROM
						mst_locations
						INNER JOIN mst_plant_calender ON mst_locations.intPlant = mst_plant_calender.intPlantId
						WHERE
						mst_plant_calender.intPlantId = '$plant' AND
						mst_plant_calender.dtDate = '$date'";
			
			return $sql;
	}
 
	
 }

?>
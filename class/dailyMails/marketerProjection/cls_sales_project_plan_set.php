<?php
 
class cls_sales_project_plan_set
{
	private $db;
 	
	function __construct($db)
	{
		$this->db = $db;
 	}

//BEGIN - PUBLIC FUNCTIONS {	
	public function set_sales_projections_old_data($marketer,$cluster,$year,$month,$projection,$actual)
	{
		
		$sql 	= $this->set_sales_projections_old_sql($marketer,$cluster,$year,$month,$projection,$actual);
		$result = $this->db->RunQuery($sql);
 		return $result;
	}
	public function delete_sales_projections_old_data($marketer,$cluster,$year,$month)
	{
		
		$sql 	= $this->delete_sales_projections_old_sql($marketer,$cluster,$year,$month);
		$result = $this->db->RunQuery($sql);
 		return $result;
	}
	
	private function delete_sales_projections_old_sql($marketer,$cluster,$year,$month){
		
		$sql = "DELETE FROM `daily_mails_sales_projections_old_data`
				WHERE 
				daily_mails_sales_projections_old_data.CLUSTER_ID 	= $cluster AND
				daily_mails_sales_projections_old_data.MARKETER_ID = '$marketer' AND
				daily_mails_sales_projections_old_data.`YEAR` = '$year' AND
				daily_mails_sales_projections_old_data.`MONTH` = '$month'
				";
		return $sql;
	}
	private function set_sales_projections_old_sql($marketer,$cluster,$year,$month,$projection,$actual){
		
		$sql = "INSERT INTO `daily_mails_sales_projections_old_data` (CLUSTER_ID,`MARKETER_ID`,`YEAR`,`MONTH`,PROJECTION,ACTUAL,CLUSTER_ID_NEW) 
			VALUES ('$cluster','$marketer','$year','$month','$projection','$actual','$cluster')";
		return $sql;
	}
	
 }

?>
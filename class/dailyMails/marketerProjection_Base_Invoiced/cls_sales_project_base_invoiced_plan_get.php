<?php
 include_once $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
include_once $_SESSION['ROOT_PATH']."class/cls_commonErrorHandeling_get.php";
  
$obj_commonErr		= new cls_commonErrorHandeling_get($db);
$obj_common			= new cls_commonFunctions_get($db);

class cls_sales_project_base_invoiced_plan_get
{
	private $db;
 	
	function __construct($db)
	{
		$this->db 	= $db;
 	}

 	public function get_marketers_result($year,$cluster,$curruntMonth)
	{
		
		$sql		= $this->load_marketers_sql($year,$cluster,$curruntMonth);
		
		$result		= $this->db->RunQuery($sql);
 		return $result;
	}

 	public function load_marketers_of_cluster($year,$cluster,$curruntMonth,$reportId)
	{
		
		$sql		= $this->load_marketers_of_cluster_sql($year,$cluster,$curruntMonth,$reportId);
		
		$result		= $this->db->RunQuery($sql);
 		return $result;
	}
	
	public function getMarketerNewPlanQty($marketer,$cluster,$month,$year)
	{
		
		$sqlp		= $this->load_MarketerNewPlanQty_sql($marketer,$cluster,$month,$year);
		$resultp	= $this->db->RunQuery($sqlp);
		$row 		= mysqli_fetch_array($resultp);
		return $row['planQty'];
 	}
	public function getMarketerNewPlanQty1($marketer,$cluster,$month,$year)
	{
		
		$sql		= $this->load_MarketerNewPlanQty_sql($marketer,$cluster,$month,$year);
 		return $sql;
 	}
	public function getMarketerOrderQtyWithoutDispatch($marketer,$cluster,$month,$year,$toCurrency)
	{
		
		$sql 		= $this->load_MarketerOrderQtyWithoutDispatch_sql($marketer,$cluster,$month,$year,$toCurrency);
		$result 	= $this->db->RunQuery($sql);
		$row 		= mysqli_fetch_array($result);
		return $row['amount'];
	}
	public function getMarketerDispatchValue($marketer,$cluster,$month,$year,$toCurrency)
	{
		
		$val 		= $this->load_MarketerDispatchValue($marketer,$cluster,$month,$year,$toCurrency);
  		return $val;
	}
	public function getMarketerInvoicedValue($marketer,$cluster,$month,$year,$toCurrency)
	{
		
		$val 		= $this->load_MarketerInvoicedValue($marketer,$cluster,$month,$year,$toCurrency);
  		return $val;
	}
	public function getOldPlanValue($marketer,$cluster,$month,$year,$toCurrency)
	{
		
		$val 		= $this->load_OldPlanValue($marketer,$cluster,$month,$year,$toCurrency);
  		return $val;
	}
	public function getMarketerDispatchValue_total($marketer,$cluster,$month,$year,$toCurrency,$yearMonthArr)
	{
		
		$val 		= $this->load_getMarketerDispatchValue_total($marketer,$cluster,$month,$year,$toCurrency,$yearMonthArr);
  		return $val;
	}
	public function load_marketers_of_cluster_monthly($current_year,$cluster,$current_month,$reportId)
	{
		$sql		= $this->load_marketers_of_cluster_monthly_sql($current_year,$cluster,$current_month,$reportId);
		$result		= $this->db->RunQuery($sql);
 		
		return $result;
	}
	public function getMarketerMonthlyTarget($marketerId,$cluster,$month,$year,$reportId)
	{
		$sql		= $this->getMarketerMonthlyTarget_sql($marketerId,$cluster,$month,$year,$reportId);
		$result		= $this->db->RunQuery($sql);
		$row		= mysqli_fetch_array($result);
 		
		return $row['monthly_target'];
	}
	
  
//BEGIN - PRIVATE FUNCTIONS {	
	private function load_marketers_sql($year,$cluster,$month)
	{
  		
	  	   $sql = "SELECT
					mst_marketer.intUserId,
					sys_users.strFullName,
					mst_marketer_cluster_target.ANUAL_TARGET as dblAnualaTarget,
					mst_marketer_cluster_target.MONTHLY_TARGET as dblMonthlyTarget ,
					sys_report_cluster_types.REPORT_TYPE,
					sys_report_cluster_types.CLUSTER_TYPE
					FROM
					mst_marketer
					INNER JOIN sys_users ON mst_marketer.intUserId = sys_users.intUserId 
					INNER JOIN sys_report_cluster_types ON sys_report_cluster_types.`NO` = mst_marketer.MAIN_CLUSTER_ID
					LEFT JOIN mst_marketer_cluster_target ON mst_marketer.intUserId = mst_marketer_cluster_target.MARKETER_ID 
					AND mst_marketer_cluster_target.YEAR='$year' AND mst_marketer_cluster_target.CLUSTER_ID='$cluster'
					
					
					LEFT JOIN daily_mails_sales_projections_old_data 
					ON mst_marketer.intUserId = daily_mails_sales_projections_old_data.MARKETER_ID 
					AND daily_mails_sales_projections_old_data.`MONTH` = '$month'
					AND daily_mails_sales_projections_old_data.`YEAR` = '$year'	
					AND daily_mails_sales_projections_old_data.`CLUSTER_ID_NEW` = '$cluster'
					
									
					WHERE 
					sys_users.intStatus = 1 AND 
					FIND_IN_SET($cluster,mst_marketer.MAIN_CLUSTER_ID) 
					ORDER BY 
					daily_mails_sales_projections_old_data.ACTUAL DESC,
					sys_users.strFullName ASC
			";
 		return $sql;
 	}
	
		private function load_marketers_of_cluster_sql($year,$cluster,$month, $reportId)
	{
  		
	  		   $sql = "SELECT
 					mst_marketer.intUserId, 
					mst_marketer.intUserId as currentMarketer,
					-- mst_marketer.intUserId,
					sys_users.strFullName,
					mst_marketer_main_cluster_target.ANUAL_TARGET as dblAnualaTarget,
					mst_marketer_main_cluster_target.MONTHLY_TARGET as dblMonthlyTarget  
					FROM
					mst_marketer 
					INNER JOIN sys_users ON mst_marketer.intUserId = sys_users.intUserId 
					INNER JOIN mst_marketer_cluster ON mst_marketer.intUserId = mst_marketer_cluster.MARKETER
					LEFT JOIN mst_marketer_main_cluster_target ON mst_marketer_cluster.MARKETER = mst_marketer_main_cluster_target.MARKETER_ID 
					AND mst_marketer_main_cluster_target.YEAR='$year' AND mst_marketer_main_cluster_target.CLUSTER_ID='$cluster' 
					AND mst_marketer_main_cluster_target.REPORT_ID='$reportId'
					
					
					LEFT JOIN daily_mails_sales_projections_base_invoiced_old_data 
					ON mst_marketer.intUserId = daily_mails_sales_projections_base_invoiced_old_data.MARKETER_ID 
					AND daily_mails_sales_projections_base_invoiced_old_data.`MONTH` = '$month'
					AND daily_mails_sales_projections_base_invoiced_old_data.`YEAR` = '$year'	
					AND daily_mails_sales_projections_base_invoiced_old_data.`CLUSTER_ID_NEW` = '$cluster'
															
					WHERE 
					mst_marketer_cluster.STATUS = 1 AND 
					mst_marketer_cluster.CLUSTER = '$cluster'  
					AND mst_marketer_cluster.`HIDE_FROM_PROJECTION_REPORT` <> 1  
					ORDER BY 
					daily_mails_sales_projections_base_invoiced_old_data.ACTUAL DESC,
					sys_users.strFullName ASC
			";
 		return $sql;
 	}

	
 	private function load_MarketerNewPlanQty_sql($marketer,$cluster,$month,$year){
		
		if($month==12){
			$yearN		=$year+1;	
			$monthN		=1;
		}
		else{
			$monthN		=$month+1;	
			$yearN		=$year;	
		}
		$maxPsdDate		=$yearN.'-'.sprintf("%02d",$monthN).'-01';
		
 		$sql = "SELECT IFNULL(SUM(dblAmount),0) AS planQty
				FROM plan_sales_project
				INNER JOIN mst_marketer ON mst_marketer.intUserId=plan_sales_project.intMarketer
				LEFT JOIN mst_locations ON plan_sales_project.LOCATION_ID = mst_locations.intId
				LEFT JOIN mst_plant ON mst_locations.intPlant = mst_plant.intPlantId
				WHERE  
				mst_plant.MAIN_CLUSTER_ID = '$cluster' AND 
				plan_sales_project.strCostomerPO='' AND
				DATEDIFF((concat(intYear,'-',LPAD(intMonth,2,0),'-',LPAD(dtPSDDay,2,0))),'$maxPsdDate')<0 AND 
				mst_marketer.intUserId IN ($marketer) ";
		
		return $sql;
	}
	
 	
	private function load_MarketerOrderQtyWithoutDispatch_sql($marketer,$cluster,$month,$year,$toCurrency){
		
		if($month==12){
			$yearN		=$year+1;	
			$monthN		=1;
		}
		else{
			$monthN		=$month+1;	
			$yearN		=$year;	
		}
	
		$maxPsdDate		=$yearN.'-'.sprintf("%02d",$monthN).'-01';
		$maxDispDate	=$year.'-'.sprintf("%02d",($month)).'-01';
 		
		$sql = "SELECT SUM(TB1.amount) AS amount
						FROM(
		 					SELECT 
							/*ROUND(SUM(IFNULL(OD.dblPrice*OD.intQty*mst_financeexchangerate.dblBuying/exc.dblBuying,0)),4) AS amount, */
							ROUND(SUM(IFNULL(OD.dblPrice*OD.intQty,0)),4) AS amount,
								(SELECT
							Min(FDHA.dtApprovedDate)
							FROM
							ware_fabricdispatchheader as FDH 
							INNER JOIN ware_fabricdispatchdetails as FDD ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo 
							AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear 
							INNER JOIN ware_fabricdispatchheader_approvedby as FDHA 
							ON FDH.intBulkDispatchNo = FDHA.intBulkDispatchNo 
							AND FDH.intBulkDispatchNoYear = FDHA.intYear 
							AND FDH.intApproveLevels = FDHA.intApproveLevelNo 
							AND FDHA.intStatus = 0 AND (FDHA.dtApprovedDate) < '$maxDispDate' 
		
							INNER JOIN trn_orderdetails as OD2 ON FDH.intOrderNo = OD2.intOrderNo AND FDH.intOrderYear = OD2.intOrderYear AND FDD.intSalesOrderId = OD2.intSalesOrderId
							
							WHERE FDH.intStatus=1 AND FDH.intOrderNo = OD.intOrderNo AND FDH.intOrderYear = OD.intOrderYear AND OD2.strStyleNo = OD.strStyleNo
							AND FDHA.dtApprovedDate IS NOT NULL
 							
 							ORDER BY
							FDHA.dtApprovedDate ASC) AS dispDate, 
							
							(SELECT MB.strName FROM trn_sampleinfomations TSI INNER JOIN mst_brand MB ON MB.intId=TSI.intBrand 
							WHERE TSI.intSampleNo = OD.intSampleNo AND TSI.intSampleYear = OD.intSampleYear AND TSI.intRevisionNo = OD.intRevisionNo ) AS brand, 
							
							OH.strCustomerPoNo  
							
							/*IFNULL((SELECT FDH.dtmdate FROM ware_fabricdispatchheader as FDH  
							INNER JOIN ware_fabricdispatchdetails as FDD ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo 
							AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear 
							WHERE FDH.intOrderNo = OH.intOrderNo AND FDH.intOrderYear = OH.intOrderYear 
							AND FDD.intSalesOrderId = OD.intSalesOrderId AND FDH.intStatus = 1 
							ORDER BY FDH.dtmdate ASC LIMIT 1),IF((month(OD.dtPSD) < month(now()) && (year(OD.dtPSD) <= year(now()))),now(),OD.dtPSD)) as date */
							
							FROM trn_orderheader OH 
							
							INNER JOIN trn_orderdetails OD ON OH.intOrderNo=OD.intOrderNo AND OH.intOrderYear=OD.intOrderYear 
							INNER JOIN mst_part ON OD.intPart = mst_part.intId
							
							INNER JOIN mst_customer MC ON MC.intId=OH.intCustomer 
							LEFT JOIN mst_locations ON OH.intLocationId = mst_locations.intId 
							LEFT JOIN mst_plant ON mst_locations.intPlant = mst_plant.intPlantId 
						
							/*LEFT JOIN mst_financeexchangerate ON OH.intCurrency = mst_financeexchangerate.intCurrencyId 
							AND DATE(OH.dtDate) = mst_financeexchangerate.dtmDate 
							AND mst_locations.intCompanyId = mst_financeexchangerate.intCompanyId 
							
							LEFT JOIN mst_financeexchangerate as exc ON $toCurrency = exc.intCurrencyId 
							AND DATE(OH.dtDate) = exc.dtmDate 
							AND mst_locations.intCompanyId = exc.intCompanyId*/
							
							WHERE  
							mst_plant.MAIN_CLUSTER_ID = '$cluster' AND 
							(OD.dtPSD) < '$maxPsdDate'  AND 
							
							OH.intMarketer IN (SELECT DISTINCT(intUserId) 
							FROM mst_marketer
							WHERE
							MARKETING_HEAD IN ($marketer)
							)
							
							AND OH.intStatus!='-2' AND OH.intStatus!='0'  AND OD.intViewInSalesProjection <>'0' 
							
 							GROUP BY OD.intOrderNo,OD.intOrderYear,OD.strStyleNo 
							HAVING dispDate IS NULL 
							ORDER BY OD.dtPSD ASC ) AS TB1";
 	
		return $sql;
		
		
	}
 	
	private function load_OldPlanValue($marketer,$cluster,$month,$year,$toCurrency){
		
 		$SQL = "SELECT
				SUM(daily_mails_sales_projections_base_invoiced_old_data.PROJECTION) AS PROJECTION,
				SUM(daily_mails_sales_projections_base_invoiced_old_data.ACTUAL) AS  ACTUAL 
				FROM `daily_mails_sales_projections_base_invoiced_old_data`
			WHERE 
				daily_mails_sales_projections_base_invoiced_old_data.CLUSTER_ID_NEW 	= $cluster AND
				daily_mails_sales_projections_base_invoiced_old_data.MARKETER_ID 	IN ($marketer) AND
				daily_mails_sales_projections_base_invoiced_old_data.`YEAR` 		= $year AND
				daily_mails_sales_projections_base_invoiced_old_data.`MONTH` 		= $month
			";
		$result = $this->db->RunQuery($SQL);
		$row 	= mysqli_fetch_array($result);
		if($row['PROJECTION']!=NULL){
			$data['result_p']	= 'pass';
			$data['PROJECTION']	= $row['PROJECTION'];
		}
		else {
			$data['result_p']	= 'fail';
			$data['PROJECTION']	= 0;
		}

		if($row['ACTUAL']!=NULL){
			$data['result_a']	= 'pass';
			$data['ACTUAL']		= $row['ACTUAL'];
		}
		else {
			$data['result_a']	= 'fail';
			$data['ACTUAL']		= 0;
		}
		return $data;
	}
		
	private function load_MarketerDispatchValue($marketer,$cluster,$month,$year,$toCurrency){
	
 	$sqlp = "SELECT 
			/*IFNULL(sum(table1.intQty*table1.dblPrice*mst_financeexchangerate.dblBuying/exc.dblBuying),0) as dispQty*/ 
			IFNULL(sum(table1.intQty*table1.dblPrice),0) as dispQty 
			FROM (SELECT 
			OD.intQty,
			OD.dblPrice,
			(SELECT
			Min(FDHA.dtApprovedDate)
			FROM
			ware_fabricdispatchheader
			INNER JOIN ware_fabricdispatchdetails ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo 
			AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear 
 			
			INNER JOIN ware_fabricdispatchheader_approvedby as FDHA 
			ON ware_fabricdispatchheader.intBulkDispatchNo = FDHA.intBulkDispatchNo 
			AND ware_fabricdispatchheader.intBulkDispatchNoYear = FDHA.intYear 
			AND ware_fabricdispatchheader.intApproveLevels = FDHA.intApproveLevelNo 
			AND FDHA.intStatus = 0 AND
			MONTH(FDHA.dtApprovedDate) = '$month' AND
			YEAR(FDHA.dtApprovedDate) = '$year'
		
			INNER JOIN trn_orderdetails as OD2 ON ware_fabricdispatchheader.intOrderNo = OD2.intOrderNo AND ware_fabricdispatchheader.intOrderYear = OD2.intOrderYear AND ware_fabricdispatchdetails.intSalesOrderId = OD2.intSalesOrderId
			
			WHERE ware_fabricdispatchheader.intStatus=1 AND ware_fabricdispatchheader.intOrderNo = OD.intOrderNo AND ware_fabricdispatchheader.intOrderYear = OD.intOrderYear AND OD2.strStyleNo = OD.strStyleNo
			AND FDHA.dtApprovedDate IS NOT NULL
 			
			ORDER BY
			FDHA.dtApprovedDate ASC) AS dispDate, 
			 
			(SELECT
			Min(FDHA.dtApprovedDate)
			FROM
			ware_fabricdispatchheader
			INNER JOIN ware_fabricdispatchdetails ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo 
			AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear 
 			
			INNER JOIN ware_fabricdispatchheader_approvedby as FDHA 
			ON ware_fabricdispatchheader.intBulkDispatchNo = FDHA.intBulkDispatchNo 
			AND ware_fabricdispatchheader.intBulkDispatchNoYear = FDHA.intYear 
			AND ware_fabricdispatchheader.intApproveLevels = FDHA.intApproveLevelNo 
			AND FDHA.intStatus = 0 AND
			date(FDHA.dtApprovedDate) < '$year-$month-01'  
		
			INNER JOIN trn_orderdetails as OD2 ON ware_fabricdispatchheader.intOrderNo = OD2.intOrderNo AND ware_fabricdispatchheader.intOrderYear = OD2.intOrderYear AND ware_fabricdispatchdetails.intSalesOrderId = OD2.intSalesOrderId
			
			WHERE ware_fabricdispatchheader.intStatus=1 AND ware_fabricdispatchheader.intOrderNo = OD.intOrderNo AND ware_fabricdispatchheader.intOrderYear = OD.intOrderYear AND OD2.strStyleNo = OD.strStyleNo
			AND FDHA.dtApprovedDate IS NOT NULL
 			
			ORDER BY
			FDHA.dtApprovedDate ASC) AS dispDatePrier, 
			OH.dtDate AS date,
			OH.intLocationId,
			OH.intMarketer,
			OH.intCurrency
			FROM
			trn_orderdetails as OD
			INNER JOIN trn_orderheader as OH ON OD.intOrderNo = OH.intOrderNo AND OD.intOrderYear = OH.intOrderYear  AND 
			OH.intMarketer='$marketer' 
			  AND
			OH.intStatus!='-2' AND OH.intStatus!='0'
			GROUP BY OD.intOrderNo,OD.intOrderYear,OD.intSalesOrderId  
			HAVING dispDatePrier IS NULL
			) as table1
			
			INNER JOIN mst_marketer ON table1.intMarketer = mst_marketer.intUserId 
			LEFT JOIN mst_locations ON table1.intLocationId = mst_locations.intId 
			LEFT JOIN mst_plant ON mst_locations.intPlant = mst_plant.intPlantId 
					
			LEFT JOIN mst_financeexchangerate ON table1.intCurrency = mst_financeexchangerate.intCurrencyId  
			AND DATE(table1.date) = mst_financeexchangerate.dtmDate AND 
			mst_locations.intCompanyId = mst_financeexchangerate.intCompanyId 
			LEFT JOIN mst_financeexchangerate as exc ON $toCurrency = exc.intCurrencyId  
			AND DATE(table1.date) = exc.dtmDate AND 
			mst_locations.intCompanyId = exc.intCompanyId
			WHERE 
			mst_plant.MAIN_CLUSTER_ID ='$cluster' 
			AND
			MONTH(table1.dispDate) = '$month' AND
			YEAR(table1.dispDate) = '$year' 
			";
	$resultp = $this->db->RunQuery($sqlp);
	$rowp = mysqli_fetch_array($resultp);
	return $rowp['dispQty'];
	}
	
	private function load_MarketerInvoicedValue($marketer,$cluster,$month,$year,$toCurrency)
	{
		$sql = "SELECT IFNULL(SUM(VALUE),0) AS invoiced_amount
				FROM
				finance_customer_transaction FCT
				INNER JOIN trn_orderheader OH ON FCT.ORDER_NO = OH.intOrderNo AND FCT.ORDER_YEAR = OH.intOrderYear
				INNER JOIN mst_locations ML ON ML.intId = FCT.LOCATION_ID
				INNER JOIN mst_plant MP ON MP.intPlantId = ML.intPlant
				WHERE
				OH.intStatus != '-2' AND 
				OH.intStatus != '0' AND
				OH.intMarketer IN (SELECT DISTINCT(intUserId) 
				FROM mst_marketer
				WHERE
				MARKETING_HEAD IN ($marketer) 
				)
				AND
				FCT.DOCUMENT_TYPE = 'INVOICE' AND
				MONTH(FCT.TRANSACTION_DATE_TIME) = '$month' AND
				YEAR(FCT.TRANSACTION_DATE_TIME) = '$year' AND
				MP.MAIN_CLUSTER_ID = '$cluster' ";
		
		$result = $this->db->RunQuery($sql);
		$row 	= mysqli_fetch_array($result);
		return $row['invoiced_amount'];
	}

	private function load_getMarketerDispatchValue_total($marketer,$cluster,$month,$year,$toCurrency,$yearMonthArr){
			
		$fromDate		=$year.'-'.($month).'-01';
 		$fromDate		=date('Y-m-d', strtotime(date('Y-m-d')." -1 day", strtotime($fromDate)));
 		if($month==12){
			$yearN		=$year+1;	
			$monthN		=1;
		}
		else{
			$yearN		=$year;	
			$monthN		=$month+1;	
		}
		$toDate			=$yearN.'-'.($monthN).'-01';
		
/*		 $sqlp = "SELECT 
				IFNULL(sum(table1.intQty*table1.dblPrice*mst_financeexchangerate.dblBuying/exc.dblBuying),0) as dispQty 
				 
				FROM (SELECT 
				OD.intQty,
				OD.dblPrice,
				(SELECT
				Min(FDHA.dtApprovedDate)
				FROM
				ware_fabricdispatchheader
				INNER JOIN ware_fabricdispatchdetails ON ware_fabricdispatchheader.intBulkDispatchNo = ware_fabricdispatchdetails.intBulkDispatchNo 
				AND ware_fabricdispatchheader.intBulkDispatchNoYear = ware_fabricdispatchdetails.intBulkDispatchNoYear 
				
				INNER JOIN ware_fabricdispatchheader_approvedby as FDHA 
				ON ware_fabricdispatchheader.intBulkDispatchNo = FDHA.intBulkDispatchNo 
				AND ware_fabricdispatchheader.intBulkDispatchNoYear = FDHA.intYear 
				AND ware_fabricdispatchheader.intApproveLevels = FDHA.intApproveLevelNo 
				AND FDHA.intStatus = 0 AND 
 				FDHA.dtApprovedDate>'$fromDate' and FDHA.dtApprovedDate<'$toDate'
				
				WHERE ware_fabricdispatchheader.intStatus=1 AND ware_fabricdispatchheader.intOrderNo = OD.intOrderNo AND ware_fabricdispatchheader.intOrderYear = OD.intOrderYear AND ware_fabricdispatchdetails.intSalesOrderId = OD.intSalesOrderId
				ORDER BY
				FDHA.dtApprovedDate ASC) AS dispDate,  
				OH.dtDate AS date,
				OH.intLocationId,
				OH.intMarketer,
				OH.intCurrency
				FROM
				trn_orderdetails as OD
				INNER JOIN trn_orderheader as OH ON OD.intOrderNo = OH.intOrderNo AND OD.intOrderYear = OH.intOrderYear AND 
				OH.intMarketer='$marketer'
				  AND
				OH.intStatus!='-2' AND OH.intStatus!='0'
				GROUP BY OD.intOrderNo,OD.intOrderYear,OD.intSalesOrderId 
				) as table1
				
				INNER JOIN mst_marketer ON table1.intMarketer = mst_marketer.intUserId 
				LEFT JOIN mst_locations ON table1.intLocationId = mst_locations.intId 
				LEFT JOIN mst_plant ON mst_locations.intPlant = mst_plant.intPlantId 
				
				LEFT JOIN mst_financeexchangerate ON table1.intCurrency = mst_financeexchangerate.intCurrencyId  
				AND DATE(table1.date) = mst_financeexchangerate.dtmDate AND 
				mst_locations.intCompanyId = mst_financeexchangerate.intCompanyId 
				LEFT JOIN mst_financeexchangerate as exc ON $toCurrency = exc.intCurrencyId  
				AND DATE(table1.date) = exc.dtmDate AND 
				mst_locations.intCompanyId = exc.intCompanyId
				WHERE 
				mst_plant.MAIN_CLUSTER_ID = '$cluster' 
				AND 
 				table1.dispDate>'$fromDate' and table1.dispDate<'$toDate'			
				";
			$resultp 		= $this->db->RunQuery($sqlp);
			$rowp 			= mysqli_fetch_array($resultp);
			$total_actual 	=  ($rowp['dispQty']==''?0:$rowp['dispQty']);
*/			
			$year_month_string 	= implode(",", $yearMonthArr);
			$year_month_string ='';
			foreach($yearMonthArr as $x){
				
				$year_month_string .= "'".$x."',";
			}
			$year_month_string =substr($year_month_string, 0, -1);
		
		  	 $SQL = "SELECT
					ifnull(sum(daily_mails_sales_projections_old_data.ACTUAL),0) as ACTUAL
					FROM `daily_mails_sales_projections_old_data`
				WHERE
					daily_mails_sales_projections_old_data.CLUSTER_ID_NEW 	= $cluster AND
					daily_mails_sales_projections_old_data.MARKETER_ID 	= $marketer AND
					CONCAT(daily_mails_sales_projections_old_data.`YEAR`,'/',daily_mails_sales_projections_old_data.`MONTH`) in ($year_month_string)
				";
		 
			$result 	= $this->db->RunQuery($SQL);
			$row 		= mysqli_fetch_array($result);
			$old_values = $row['ACTUAL'];
			//return $old_values+$total_actual;
			return $old_values;
 			
	}
	private function load_marketers_of_cluster_monthly_sql($current_year,$cluster,$current_month,$reportId)
	{
		$next_year	= $current_year+1;
		$sql = "SELECT
					if(mst_marketer.REPLACEMENT_FOR >0,(concat(mst_marketer.intUserId,',',mst_marketer.REPLACEMENT_FOR)),mst_marketer.intUserId) as intUserId, 
					/*mst_marketer.intUserId,*/
					sys_users.strFullName,
					((SELECT SUM(MONTHLY_TARGET)
					FROM mst_marketer_main_cluster_monthly_target
					WHERE
					MARKETER_ID = mst_marketer.intUserId AND
					REPORT_ID = $reportId AND
					CLUSTER_ID = $cluster AND
					YEAR = $current_year AND
					MONTH BETWEEN (4) AND (12)
					) +
					(SELECT SUM(MONTHLY_TARGET)
					FROM mst_marketer_main_cluster_monthly_target
					WHERE
					MARKETER_ID = mst_marketer.intUserId AND
					REPORT_ID = $reportId AND
					CLUSTER_ID = $cluster AND
					YEAR = $next_year AND
					MONTH BETWEEN (1) AND (3)
					)) AS ANUAL_TARGET
					FROM
					mst_marketer 
					INNER JOIN sys_users ON mst_marketer.intUserId = sys_users.intUserId 
					INNER JOIN mst_marketer_cluster ON mst_marketer.intUserId = mst_marketer_cluster.MARKETER

					LEFT JOIN daily_mails_sales_projections_base_invoiced_old_data 
					ON mst_marketer.intUserId = daily_mails_sales_projections_base_invoiced_old_data.MARKETER_ID 
					AND daily_mails_sales_projections_base_invoiced_old_data.`MONTH` = '$current_month'
					AND daily_mails_sales_projections_base_invoiced_old_data.`YEAR` = '$current_year'	
					AND daily_mails_sales_projections_base_invoiced_old_data.`CLUSTER_ID_NEW` = '$cluster'
															
					WHERE 
					mst_marketer_cluster.STATUS = 1 AND 
					mst_marketer_cluster.CLUSTER = '$cluster' 
					AND mst_marketer_cluster.`HIDE_FROM_PROJECTION_REPORT` <> 1    AND 
				 	mst_marketer.REPLACED_FLAG IS NULL 
					ORDER BY 
					daily_mails_sales_projections_base_invoiced_old_data.ACTUAL DESC,
					sys_users.strFullName ASC";
		
		return $sql;
	}
	private function getMarketerMonthlyTarget_sql($marketerId,$cluster,$month,$year,$reportId)
	{
		$sql = "SELECT IFNULL(MONTHLY_TARGET,0) AS monthly_target
				FROM mst_marketer_main_cluster_monthly_target
				WHERE
				MARKETER_ID = '$marketerId' AND
				REPORT_ID = '$reportId' AND
				CLUSTER_ID = '$cluster' AND
				YEAR = '$year' AND
				MONTH = '$month' ";
				
		return $sql;
	}
}

?>
<?php
date_default_timezone_set('Asia/Kolkata');
// ini_set('display_errors',1);//$_SESSION['ROOT_PATH'].
//include $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";


class cls_bom_get
{
	
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
	
	public function getProjects_options($company,$year){
			$sql = "SELECT
					trn_orderheader.intOrderNo,
					trn_orderheader.strOrderCode as strCode
					FROM trn_orderheader 
					Inner Join mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
					WHERE 
					trn_orderheader.intStatus =  '1' AND 
					mst_locations.intCompanyId = '$company' AND 
					trn_orderheader.intOrderYear = '$year' 
					ORDER BY
					trn_orderheader.intOrderNo DESC"; 
			$result = $this->db->RunQuery2($sql);
			$combo ="<option value=\"\"></option>";
			while($row=mysqli_fetch_array($result))
			{
				$combo .="<option value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";	
			}
		 return $combo;
	}
	public function getProjectsYear_options($company){
			$sql = "SELECT DISTINCT 
					trn_orderheader.intOrderYear 
					FROM trn_orderheader 
					Inner Join mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
					WHERE
					trn_orderheader.intStatus =  '1' AND 
					mst_locations.intCompanyId = '$company' 
					ORDER BY
					trn_orderheader.intOrderYear DESC"; 
			$result = $this->db->RunQuery2($sql);
			$combo ="";
			while($row=mysqli_fetch_array($result))
			{
				$combo .="<option value=\"".$row['intOrderYear']."\">".$row['intOrderYear']."</option>";	
			}
		 return $combo;
	}
	public function getSubProjects_options($project,$year){
			$sql = "SELECT
			trn_orderdetails.intSalesOrderId,
			trn_orderdetails.strSalesOrderNo
			FROM trn_orderdetails 
			WHERE
			trn_orderdetails.intOrderNo =  '$project' AND 
			trn_orderdetails.intOrderYear =  '$year'"; 
			$result = $this->db->RunQuery2($sql);
			$combo ="<option value=\"\"></option>";
			while($row=mysqli_fetch_array($result))
			{
				$combo .="<option value=\"".$row['intSalesOrderId']."\">".$row['strSalesOrderNo']."</option>";	
			}
		 return $combo;
	}
	
	public function getMainCat_options(){
			$sql = "SELECT
					mst_maincategory.intId,
					mst_maincategory.strName
					FROM mst_maincategory
					WHERE
					mst_maincategory.intStatus =  '1'
					ORDER BY
					mst_maincategory.strName ASC"; 
			$result = $this->db->RunQuery2($sql);
			$combo ="<option value=\"\"></option>";
			while($row=mysqli_fetch_array($result))
			{
				$combo .="<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
			}
		 return $combo;
	}
	
	public function getSubCat_options($mainCat){
		
			$sql = "SELECT
					mst_subcategory.intId,
					mst_subcategory.strName
					FROM mst_subcategory
					WHERE
					mst_subcategory.intMainCategory =  '$mainCat' AND 
					mst_subcategory.intStatus =  '1'
					ORDER BY
					mst_subcategory.strName ASC"; 
			$result = $this->db->RunQuery2($sql);
			$combo ="<option value=\"\"></option>";
			while($row=mysqli_fetch_array($result))
			{
				$combo .="<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
			}
		 return $combo;
	}

	public function getItem_options($mainCat,$subCat){
			$sql = "SELECT
					mst_item.intId,
					mst_item.strName
					FROM mst_item
					WHERE
					mst_item.intMainCategory =  '$mainCat' AND
					mst_item.intSubCategory =  '$subCat' AND 
					mst_item.intStatus =  '1'
					ORDER BY
					mst_item.strName ASC"; 
			$result = $this->db->RunQuery2($sql);
			$combo ="<option value=\"\"></option>";
			while($row=mysqli_fetch_array($result))
			{
				$combo .="<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
			}
		 return $combo;
	}
	
	public function getCurrency_options(){
			$sql = "SELECT
					mst_financecurrency.intId,
					mst_financecurrency.strCode
					FROM
					mst_financecurrency
					WHERE
					mst_financecurrency.intStatus =  '1'
					ORDER BY
					mst_financecurrency.strCode ASC"; 
			$result = $this->db->RunQuery2($sql);
			$combo ="<option value=\"\"></option>";
			while($row=mysqli_fetch_array($result))
			{
				$combo .="<option value=\"".$row['intId']."\">".$row['strCode']."</option>";	
			}
		 return $combo;
	}
	
	
	public function getItem_Details($item,$bomCurrency,$company,$bomDate){
		
		$response['arrData']='';
		     $sql="SELECT
					mst_item.intUOM,
					mst_units.strCode as uom,
					mst_item.intCurrency,
					mst_financecurrency.strCode as currency,
					round(mst_item.dblLastPrice*(SELECT
				mst_financeexchangerate.dblBuying
				FROM mst_financeexchangerate
				WHERE
				mst_financeexchangerate.intCurrencyId =  mst_item.intCurrency and mst_financeexchangerate.dtmDate =  '$bomDate' and intCompanyId='$company')/(SELECT
				mst_financeexchangerate.dblBuying
				FROM mst_financeexchangerate
				WHERE
				mst_financeexchangerate.intCurrencyId =  '$bomCurrency' and mst_financeexchangerate.dtmDate =  '$bomDate' and intCompanyId='$company'),4) as dblLastPrice
					FROM
					mst_item
					Inner Join mst_units ON mst_item.intUOM = mst_units.intId
					Inner Join mst_financecurrency ON mst_item.intCurrency = mst_financecurrency.intId
					WHERE
					mst_item.intId =  '$item'
				";
			 	   //echo $sql;
			$result = $this->db->RunQuery2($sql);
		while($row=mysqli_fetch_array($result))
		{
			$arrData[] = $row;
		}
		$response['arrData'] 	= $arrData;
		return $arrData;
	}
	
	public function loadUnitPrice($itemUnitPrice,$itemCurrency,$bomCurrency,$company,$bomDate){
	//	global $db;
		
	  	$sql = "SELECT
				mst_financeexchangerate.dblBuying
				FROM mst_financeexchangerate
				WHERE
				mst_financeexchangerate.intCurrencyId =  '$itemCurrency' and mst_financeexchangerate.dtmDate =  '$bomDate' and intCompanyId='$company'";
		$result = $db->RunQuery2($sql);
		$row=mysqli_fetch_array($result);
		$excRateItem = $row['dblBuying'];
		
		$sql = "SELECT
				mst_financeexchangerate.dblBuying
				FROM mst_financeexchangerate
				WHERE
				mst_financeexchangerate.intCurrencyId =  '$bomCurrency' and mst_financeexchangerate.dtmDate =  '$bomDate' and intCompanyId='$company'";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$excRatePO = $row['dblBuying'];
		
		return $itemUnitPrice*$excRateItem/$excRatePO;
	}
	
	public function loadBomUnitPriceEditFlag($menuID,$userId)
	{
	
	  $sql = "SELECT
			menus_special.intStatus
			FROM
			menus_special
			Inner Join menus_special_permision ON menus_special.intId = menus_special_permision.intSpMenuId
			WHERE
			menus_special_permision.intUser =  '$userId' AND menus_special.intStatus='1' AND 
			menus_special.intMenuId =  '$menuID'";
			$result = $this->db->RunQuery($sql);
			$row=mysqli_fetch_array($result);
			$editFlag 	= $row['intStatus'];
			 return $editFlag;
	}
	
	public function getAllreadySavedFlag($project,$subProject,$year)
	{
	
	  $sql = "SELECT
			trn_bom_header.intProjectId
			FROM
			trn_bom_header 
			WHERE
			trn_bom_header.intProjectId =  '$project' AND trn_bom_header.intYear =  '$year'  AND trn_bom_header.intSubProjectId='$subProject'";
			$result = $this->db->RunQuery2($sql);
			$row=mysqli_fetch_array($result);
			$savedFlag 	= $row['intProjectId'];
			if($savedFlag!=''){
				$savedFlag=1;
			}
			else{
				$savedFlag=0;
			}
				
			 return $savedFlag;
	}
	
	public function getEditableFlag($project,$subProject,$year)
	{
	
	  $sql = "SELECT
			trn_bom_header.intStatus, 
			trn_bom_header.intApproveLevels 
			FROM
			trn_bom_header 
			WHERE
			trn_bom_header.intProjectId =  '$project' AND trn_bom_header.intYear =  '$year'  AND trn_bom_header.intSubProjectId='$subProject'";
			$result = $this->db->RunQuery2($sql);
			$row=mysqli_fetch_array($result);
			$editFlag 	= $row['intStatus'];
			
			if($row['intStatus']==0)
				$editFlag=1;
			else if($row['intStatus']==($row['intApproveLevels']+1)) 
				$editFlag=1;
			else 
				$editFlag=0;
				
			 return $editFlag;
	}
	
	public function getEditableFlag1($project,$subProject,$year)
	{
	
	  $sql = "SELECT
			trn_bom_header.intStatus, 
			trn_bom_header.intApproveLevels 
			FROM
			trn_bom_header 
			WHERE
			trn_bom_header.intProjectId =  '$project' AND trn_bom_header.intYear =  '$year' AND trn_bom_header.intSubProjectId='$subProject'";
			$result = $this->db->RunQuery($sql);
			$row=mysqli_fetch_array($result);
			$editFlag 	= $row['intStatus'];
			
			if($row['intStatus']==0)
				$editFlag=1;
			else if($row['intStatus']==($row['intApproveLevels']+1)) 
				$editFlag=1;
			else 
				$editFlag=0;
				
			 return $editFlag;
	}
	
	
	
	
	
	////////////////////////////////////////////
	public function get_Header($project,$subProject,$year){
		
		     $sql="SELECT 
				trn_bom_header.intStatus,
				trn_bom_header.intApproveLevels,
				trn_bom_header.dtmDate,
				trn_bom_header.strRemarks,
				trn_bom_header.intCurrencyId
				FROM trn_bom_header
				WHERE
				trn_bom_header.intProjectId =  '$project' AND 
				trn_bom_header.intYear =  '$year' AND 
				trn_bom_header.intSubProjectId =  '$subProject'
				";
			 	// echo $sql;
			$result = $this->db->RunQuery2($sql);
		while($row=mysqli_fetch_array($result))
		{
			$arrData[] = $row;
		}
		$response['arrData'] 	= $arrData;
		return $arrData;
	}
	
	public function get_GridData($project,$subProject,$year){
		
		$response['arrData']='';
		     $sql="SELECT
					mst_item.intMainCategory,
					mst_item.intSubCategory,
					trn_bom_details.intItemId,
					trn_bom_details.dblQty,
					trn_bom_details.dblUnitPrice
					FROM
					trn_bom_details
					Inner Join mst_item ON trn_bom_details.intItemId = mst_item.intId
					WHERE
					trn_bom_details.intProjectId =  '$project' AND 
					trn_bom_details.intYear =  '$year' AND 
					trn_bom_details.intSubProjectId =  '$subProject'
				";
			 	   //echo $sql;
			$result = $this->db->RunQuery2($sql);
		while($row=mysqli_fetch_array($result))
		{
			$arrData[] = $row;
		}
		$response['arrData'] 	= $arrData;
		return $arrData;
	}
	
	
	
}
?>
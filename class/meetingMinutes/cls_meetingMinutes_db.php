<?php

class cls_meetingMinutes_db
{
	
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}

	public function replace($str)
	{
		  $str 	= 	str_replace('\\', "\\\\",	$str);
		  $str 	= 	str_replace("'","\'",	$str);
		  $str	=	str_replace("\n", "<br>",$str);  
		  return $str;
	}
	
	public function toMeEmail($serialNo)
	{
	$sql = "SELECT
				sys_users.strEmail
			FROM
				other_meeting_minutes_header
				INNER JOIN sys_users ON other_meeting_minutes_header.CREATED_BY = sys_users.intUserId
			WHERE 
				sys_users.intStatus = 1 AND 
				other_meeting_minutes_header.MINUTE_ID = '$serialNo' 
			";	
	$result = $this->db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	return $row['strEmail']; 
	}
	public function toEmails($serialNo){
		
	 $sql = "SELECT  
				GROUP_CONCAT(other_meeting_minutes_details.RESPONSIBLE_LIST) as users
			FROM
				other_meeting_minutes_header
				INNER JOIN other_meeting_minutes_details ON other_meeting_minutes_header.MINUTE_ID = other_meeting_minutes_details.MINUTE_ID
			WHERE 
				other_meeting_minutes_details.RESPONSIBLE_LIST !=''  
				AND
				other_meeting_minutes_details.RESPONSIBLE_LIST IS NOT NULL 
				AND
				other_meeting_minutes_header.MINUTE_ID = '$serialNo'";	
	$result = $this->db->RunQuery($sql);
	$rows 	= mysqli_fetch_array($result);
	$idList = $rows['users']; 
 	
  	$toEmails =  $this->getEmailList($idList);
	$toEmails=preg_replace('/,,+/', ',', $toEmails);
 	$toEmails = trim($toEmails, ',');  
	return $toEmails;
	}
	
	public function ccEmails($serialNo){
		
	  $toEmailsIds=$this->toEmailsIds($serialNo);
	    $sql = "SELECT
					GROUP_CONCAT(other_meeting_minutes_header.CALLED_BY_LIST,',',
					other_meeting_minutes_header.ATTENDEES_LIST,',',
					other_meeting_minutes_header.DISTRIBUTION_LIST,',',
					other_meeting_minutes_details.CONCERN_RAISED_BY) as emailList
				FROM
					other_meeting_minutes_header
					INNER JOIN other_meeting_minutes_details ON other_meeting_minutes_header.MINUTE_ID = other_meeting_minutes_details.MINUTE_ID
				WHERE 
					other_meeting_minutes_details.MINUTE_ID = $serialNo
			";	
		$result = $this->db->RunQuery($sql);
		$row 	= mysqli_fetch_array($result);
		$idList = $row['emailList']; 
		
		$idList=preg_replace('/,,+/', ',', $idList);
		$idList = trim($idList, ',');  
  			
	    $sql = "SELECT
					group_concat(sys_users.strEmail) as emails
				FROM
					sys_users
				WHERE 
				  sys_users.intStatus = 1 AND
					sys_users.intUserId in($idList) 
					AND intUserId not in($toEmailsIds)
			";	
		$result = $this->db->RunQuery($sql);
		$row 	= mysqli_fetch_array($result);
		$emailsList=$row['emails'];
		
		$idList=preg_replace('/,,+/', ',', $idList);
		$idList = trim($idList, ',');  
	
	     $sql = "SELECT
					GROUP_CONCAT(other_meeting_minutes_header.OTHER_DISTRIBUTION_EMAILS) as emailList
				FROM
					other_meeting_minutes_header
				WHERE  
					other_meeting_minutes_header.OTHER_DISTRIBUTION_EMAILS !=''  
					AND
					other_meeting_minutes_header.OTHER_DISTRIBUTION_EMAILS IS NOT NULL 
					AND 
					other_meeting_minutes_header.MINUTE_ID = $serialNo
			";	
	$result = $this->db->RunQuery($sql);
  	while($row=mysqli_fetch_array($result))
	{
		$emailsList .=','.$row['emailList'];
	}
	$emailsList=preg_replace('/,,+/', ',', $emailsList);
	$emailsList = trim($emailsList, ',');  
	$emailsList = trim($emailsList, '<br>');  
	return $emailsList;
	
 	}
	
	public function getEmailList($idList){
		
	  $sql = "SELECT
					group_concat(sys_users.strEmail) as emails
			FROM
				sys_users
			WHERE 
				sys_users.intStatus = 1 AND 
				 sys_users.intUserId in($idList) 
			";	
	$result = $this->db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	return $row['emails']; 
 	}
	
	public function toEmailsIds($serialNo){
		
	  $sql = "SELECT  
					GROUP_CONCAT(other_meeting_minutes_details.RESPONSIBLE_LIST) as users
				FROM
					other_meeting_minutes_header
					INNER JOIN other_meeting_minutes_details ON other_meeting_minutes_header.MINUTE_ID = other_meeting_minutes_details.MINUTE_ID
				WHERE 
					other_meeting_minutes_details.RESPONSIBLE_LIST !=''  
					AND
					other_meeting_minutes_details.RESPONSIBLE_LIST IS NOT NULL 
					AND
					other_meeting_minutes_header.MINUTE_ID = '$serialNo'";	
	$result = $this->db->RunQuery($sql);
	$rows 	= mysqli_fetch_array($result);
	$idList = $rows['users']; 
	return $idList; 
 	}

	public function getReportDetailSql($serialNo,$type,$intUser){
		
     $sqlM="SELECT 
				concat('MOM',LPAD(other_meeting_minutes_header.`MINUTE_ID`,4,0))  as Meeting_No,
				other_meeting_minutes_details.COMPLETED_FLAG, 
				other_meeting_minutes_header.`SUBJECT`,
				other_meeting_minutes_header.MEETING_PLACE,
				(SELECT
				GROUP_CONCAT(sys_users.strFullName) as fn 
				FROM `sys_users`
				WHERE
				FIND_IN_SET(sys_users.intUserId, other_meeting_minutes_header.ATTENDEES_LIST)) as Attendees,
				/*other_meeting_minutes_header.CALLED_BY_NAMES as Called_by,*/
				(SELECT
				GROUP_CONCAT(sys_users.strFullName) as fn 
				FROM `sys_users`
				WHERE
				FIND_IN_SET(sys_users.intUserId, other_meeting_minutes_header.CALLED_BY_LIST)) as Called_by,
				(SELECT
				GROUP_CONCAT(sys_users.strFullName) as fn 
				FROM `sys_users`
				WHERE
				FIND_IN_SET(sys_users.intUserId, other_meeting_minutes_header.DISTRIBUTION_LIST)) as Distribution,
				other_meeting_minutes_header.OTHER_DISTRIBUTION_EMAILS as otherDistribution,
				other_meeting_minutes_header.DATE as meetingDate,
				date(other_meeting_minutes_header.CREATED_DATE) AS createDate,
				sys_users.strFullName AS createBy,
				other_meeting_minutes_header.STATUS as status  
			FROM
				other_meeting_minutes_header 
				INNER JOIN other_meeting_minutes_details ON other_meeting_minutes_header.MINUTE_ID = other_meeting_minutes_details.MINUTE_ID
				INNER JOIN sys_users ON sys_users.intUserId = other_meeting_minutes_header.CREATED_BY
			WHERE
				other_meeting_minutes_header.MINUTE_ID = '$serialNo'  
				group by other_meeting_minutes_header.MINUTE_ID AND 
				(
				find_in_set($intUser,other_meeting_minutes_header.CALLED_BY_LIST) OR
				find_in_set($intUser,other_meeting_minutes_header.ATTENDEES_LIST) OR
				find_in_set($intUser,other_meeting_minutes_header.CREATED_BY) OR
				find_in_set($intUser,other_meeting_minutes_header.DISTRIBUTION_LIST) OR
				find_in_set($intUser,other_meeting_minutes_details.RESPONSIBLE_LIST) OR
				find_in_set($intUser,other_meeting_minutes_details.COMPLETED_BY) OR
				find_in_set($intUser,other_meeting_minutes_details.CONCERN_RAISED_BY)
				)";
		 return $sqlM;

 	}
	

	public function getReportDetail_daily_mail_Sql($serialNo,$type){
		
     $sqlM="SELECT 
				concat('MOM',LPAD(other_meeting_minutes_header.`MINUTE_ID`,4,0))  as Meeting_No,
				other_meeting_minutes_details.COMPLETED_FLAG, 
				other_meeting_minutes_header.`SUBJECT`,
				other_meeting_minutes_header.MEETING_PLACE,
				(SELECT
				GROUP_CONCAT(sys_users.strFullName) as fn 
				FROM `sys_users`
				WHERE
				FIND_IN_SET(sys_users.intUserId, other_meeting_minutes_header.ATTENDEES_LIST)) as Attendees,
				/*other_meeting_minutes_header.CALLED_BY_NAMES as Called_by,*/
				(SELECT
				GROUP_CONCAT(sys_users.strFullName) as fn 
				FROM `sys_users`
				WHERE
				FIND_IN_SET(sys_users.intUserId, other_meeting_minutes_header.CALLED_BY_LIST)) as Called_by,
				(SELECT
				GROUP_CONCAT(sys_users.strFullName) as fn 
				FROM `sys_users`
				WHERE
				FIND_IN_SET(sys_users.intUserId, other_meeting_minutes_header.DISTRIBUTION_LIST)) as Distribution,
				other_meeting_minutes_header.OTHER_DISTRIBUTION_EMAILS as otherDistribution,
				other_meeting_minutes_header.DATE as meetingDate,
				date(other_meeting_minutes_header.CREATED_DATE) AS createDate,
				sys_users.strFullName AS createBy,
				other_meeting_minutes_header.STATUS as status  
			FROM
				other_meeting_minutes_header 
				INNER JOIN other_meeting_minutes_details ON other_meeting_minutes_header.MINUTE_ID = other_meeting_minutes_details.MINUTE_ID
				INNER JOIN sys_users ON sys_users.intUserId = other_meeting_minutes_header.CREATED_BY
			WHERE
				other_meeting_minutes_header.MINUTE_ID = '$serialNo'  
				group by other_meeting_minutes_header.MINUTE_ID ";
		 return $sqlM;

 	}	
	public function loadEditMode($programCode,$intUser,$serialNo,$createdBy){
		
		$editMode=0;
		$sqlp = "SELECT
					menupermision.intEdit  
				FROM 
					menupermision 
					Inner Join menus ON menupermision.intMenuId = menus.intId
				WHERE
					menus.strCode =  '$programCode' AND
					menupermision.intUserId =  '$intUser'";	
			
			 $resultp = $this->db->RunQuery($sqlp);
			 $rowp=mysqli_fetch_array($resultp);
			 
			 if(($rowp['intEdit']==1) && ($createdBy=='')){
					 $editMode=1;
			 }
				 
		  $sqlp = "SELECT
						other_meeting_minutes_edit_permission.USER_ID
				FROM
						other_meeting_minutes_edit_permission
				WHERE
						other_meeting_minutes_edit_permission.USER_ID = '$intUser'
				";	
				
		 $resultp = $this->db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 if(($rowp['USER_ID']==$intUser) || ($intUser==$createdBy)){
				 $editMode=1;
		 }
 		// mom status
		  $sqls = "SELECT
						other_meeting_minutes_header.STATUS
				FROM
						other_meeting_minutes_header
				WHERE
						other_meeting_minutes_header.MINUTE_ID = '$serialNo'
				";	
				
		 $results = $this->db->RunQuery($sqls);
		 $rows=mysqli_fetch_array($results);
		 if(($rows['STATUS']==-2) && ($serialNo !='')){
				 $editMode=0;
		 }
	
	
	 
	
		return $editMode;
 	}
	
	public function loadDeleteMode($programCode,$intUser){
		
		$delMode =	0;
		$sqlp = "SELECT
					menupermision.intDelete  
				FROM
					menupermision 
					Inner Join menus ON menupermision.intMenuId = menus.intId
				WHERE
					menus.strCode =  '$programCode' AND
					menupermision.intUserId =  '$intUser'";	
				
			 $resultp = $this->db->RunQuery($sqlp);
			 $rowp=mysqli_fetch_array($resultp);
			 
			 if($rowp['intDelete']==1){
					 $delMode=1;
			 }
				 
		return $delMode;
 	}

	public function loadDeleteMode2($programCode,$intUser){
		
		$delMode=0;
		$sqlp = "SELECT
					menupermision.intDelete  
				FROM
					menupermision 
					Inner Join menus ON menupermision.intMenuId = menus.intId
				WHERE
					menus.strCode =  '$programCode' AND
					menupermision.intUserId =  '$intUser'";	
				
			 $resultp = $this->db->RunQuery2($sqlp);
			 $rowp=mysqli_fetch_array($resultp);
			 
			 if($rowp['intDelete']==1){
					 $delMode=1;
			 }
				 
		return $delMode;
 	}
	
	public function getCompletePermission($intUser,$MOMId,$taskId){

 	$completePermission=0;
 
	  $sqlp = "SELECT
					other_meeting_minutes_header.CREATED_BY,
					other_meeting_minutes_details.RESPONSIBLE_LIST,
					other_meeting_minutes_details.COMPLETED_FLAG
			FROM
					other_meeting_minutes_header
					INNER JOIN other_meeting_minutes_details ON other_meeting_minutes_header.MINUTE_ID = other_meeting_minutes_details.MINUTE_ID
			WHERE
					other_meeting_minutes_details.MINUTE_ID = '$MOMId' AND
					other_meeting_minutes_details.TASK_ID = '$taskId' AND 
					( CREATED_BY = '$intUser' or FIND_IN_SET('$intUser',RESPONSIBLE_LIST)) 
  			";	
				
	 $resultp = $this->db->RunQuery($sqlp);
	 $rowp=mysqli_fetch_array($resultp);
	 if($rowp['CREATED_BY']!=''){
			 $completePermission=1;
	 }
 	 
	 if($compFlag==1){
			 $completePermission=0;
	 }
			 
	return $completePermission;

	}
	public function loadCompletionFlag($serial){
		
		$flag_completed=0;
	
	  $sqlp = "SELECT
					other_meeting_minutes_header.MINUTE_ID 
			FROM
					other_meeting_minutes_header
					INNER JOIN other_meeting_minutes_details ON other_meeting_minutes_header.MINUTE_ID = other_meeting_minutes_details.MINUTE_ID
			WHERE
					other_meeting_minutes_details.MINUTE_ID = '$serial' AND
					(other_meeting_minutes_details.COMPLETED_FLAG=1 || other_meeting_minutes_details.COMPLETED_FLAG_LEVEL_1=1)  
  			";	
				
		 $resultp = $this->db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 if($rowp['MINUTE_ID']!=''){
			 $flag_completed=1;
		 }

		return $flag_completed;
		
	}
	
	public function getFinalCompletePermision($intUser,$progCode){

 	  $sqlp = "SELECT
				menupermision.int2Approval
				FROM `menupermision`
				WHERE
				menupermision.intMenuId = '$progCode' AND
				menupermision.intUserId = '$intUser'
				";	
				
	 $resultp = $this->db->RunQuery($sqlp);
	 $rowp=mysqli_fetch_array($resultp);
			 
		return $rowp['int2Approval'];

	}
	
	public function formHeader($serialNo){
		
		$delMode=0;
		$sql="SELECT
					other_meeting_minutes_header.`SUBJECT`,
					other_meeting_minutes_header.MEETING_PLACE,
					other_meeting_minutes_header.ATTENDEES_LIST,
					other_meeting_minutes_header.CALLED_BY_LIST,
					other_meeting_minutes_header.DISTRIBUTION_LIST,
					other_meeting_minutes_header.OTHER_DISTRIBUTION_EMAILS, 
					other_meeting_minutes_header.DATE,
					other_meeting_minutes_header.CREATED_DATE ,
					other_meeting_minutes_header.CREATED_BY, 
					sys_users.strFullName as createdByName
			FROM 
					`other_meeting_minutes_header`
					INNER JOIN sys_users ON sys_users.intUserId = other_meeting_minutes_header.CREATED_BY
			WHERE
					other_meeting_minutes_header.MINUTE_ID = '$serialNo'
			";
				
			 $result = $this->db->RunQuery($sql);
			 $row=mysqli_fetch_array($result);
 				 
		return $row;
 	}
	
	public function formDetails($serialNo){
		
		$sql="SELECT
					other_meeting_minutes_details.TASK_ID,
					other_meeting_minutes_details.CONCERN,
					other_meeting_minutes_details.RECOMMENDATION,
					other_meeting_minutes_details.CONCERN_RAISED_BY,
		/*						(SELECT
					 su.strFullName  as fn 
					FROM `sys_users` as su 
					WHERE
					su.intUserId = other_meeting_minutes_details.CONCERN_RAISED_BY) as CONCERN_RAISED_BY,
		*/						other_meeting_minutes_details.ACTION_PLAN,
					other_meeting_minutes_details.RESPONSIBLE_LIST,
					other_meeting_minutes_details.RESPONSIBLE_LIST,
					other_meeting_minutes_details.DUE_DATE, 
					other_meeting_minutes_details.INITIAL_DUE_DATE, 
				/*	IFNULL((
						SELECT
						other_meeting_minutes_details_history.DUE_DATE
						FROM `other_meeting_minutes_details_history`
						WHERE
						other_meeting_minutes_details_history.MINUTE_ID = other_meeting_minutes_details.MINUTE_ID AND
						other_meeting_minutes_details_history.TASK_ID = other_meeting_minutes_details.TASK_ID
						ORDER BY
						other_meeting_minutes_details_history.ID ASC
						limit 1	
					),other_meeting_minutes_details.DUE_DATE) as DUE_DATE_INI ,
					*/
					sys_users.strFullName,
					su1.strFullName as strFullName_level_1,
					other_meeting_minutes_details.COMPETED_DATE,
					other_meeting_minutes_details.COMPETED_DATE_LEVEL_1,					other_meeting_minutes_details.COMPLETED_FLAG_LEVEL_1,
					other_meeting_minutes_details.COMPLETED_FLAG
			FROM
					other_meeting_minutes_header
					INNER JOIN other_meeting_minutes_details ON other_meeting_minutes_header.MINUTE_ID = other_meeting_minutes_details.MINUTE_ID
					left JOIN sys_users ON other_meeting_minutes_details.COMPLETED_BY = sys_users.intUserId
					left JOIN sys_users as su1 ON other_meeting_minutes_details.COMPLETED_BY_LEVEL_1 = su1.intUserId
			WHERE
					other_meeting_minutes_header.MINUTE_ID = '$serialNo'
			ORDER BY
					other_meeting_minutes_details.TASK_ID ASC
			";
	 
		return $sql;
 	}
	
	public function userList(){
		
		$delMode=0;
		$sql = "
			SELECT
				sys_users.strFullName, 
				sys_users.intUserId 
			FROM
				sys_users
			WHERE
				sys_users.intStatus = 1 OR sys_users.intHigherPermision = 1 
			ORDER BY
				sys_users.strFullName ASC";
				
  		return $sql;
 	}
	
	public function getNextSerial(){
	
		global $db;
		$sql = "SELECT
					(IFNULL(Max(other_meeting_minutes_header.MINUTE_ID),0)+1) AS serial
				FROM 
					`other_meeting_minutes_header`";
		
		$result = $this->db->RunQuery($sql);
		$row =mysqli_fetch_array($result);
		$serial= $row['serial'];
 			
		return $serial;
	}
	
	public function other_meeting_minutes_backup_insert($arrHeader,$arrDetails){
		
 		$sql = "INSERT INTO `other_meeting_minutes_backup` 
		(
			`ARRAY_HEADER`,
			`ARRAY_DETAILS`
		) 			
		VALUES 
		(
			'$arrHeader',
			'$arrDetails'
		)";
 		return $sql;
 	}
	public function other_meeting_minutes_header_select($serialNo){
		
		$sql = "SELECT
					*
				FROM `other_meeting_minutes_header`
				WHERE
					other_meeting_minutes_header.MINUTE_ID = '$serialNo'";
 		return $sql;
 	}
	public function other_meeting_minutes_header_update($serialNo,$subject,$meetingPlace,$arrCalledBy,$arrAttendance,$arrDistribution,$arrOtherDistribution,$date,$userId){
		
		$sql = "UPDATE `other_meeting_minutes_header` SET SUBJECT 					='$subject', 
														MEETING_PLACE 				='$meetingPlace', 
														CALLED_BY_LIST		 		='$arrCalledBy', 
														ATTENDEES_LIST 				='$arrAttendance', 
														DISTRIBUTION_LIST	 		='$arrDistribution', 
														OTHER_DISTRIBUTION_EMAILS 	='$arrOtherDistribution', 
														DATE 						=$date, 
														/*CREATED_BY 					='$userId', 
														CREATED_DATE 				= now(), */
														STATUS 						= 1  
				WHERE (`MINUTE_ID`='$serialNo')";
 		return $sql;
 	}
	public function other_meeting_minutes_header_insert($serialNo,$subject,$meetingPlace,$arrCalledBy,$arrAttendance,$arrDistribution,$arrOtherDistribution,$date,$userId){
		
		$sql = "INSERT INTO `other_meeting_minutes_header` (
					`MINUTE_ID`,
					`SUBJECT`,
					`MEETING_PLACE`,
					CALLED_BY_LIST,
					ATTENDEES_LIST,
					DISTRIBUTION_LIST,
					OTHER_DISTRIBUTION_EMAILS,
					DATE,
					CREATED_BY,
					CREATED_DATE,
					STATUS
					) 			
				VALUES (
					'$serialNo',
					'$subject',
					'$meetingPlace',
					'$arrCalledBy',
					'$arrAttendance',
					'$arrDistribution',
					'$arrOtherDistribution',
					$date,
					'$userId',
					now(),
					'1'
				)";
 		return $sql;
 	}
	
	public function other_meeting_minutes_header_history_insert($serialNo,$subject,$meetingPlace,$arrCalledBy,$arrAttendance,$arrDistribution,$arrOtherDistribution,$date,$createdDate,$creator,$userId,$mode){
		
		$sql = "INSERT INTO `other_meeting_minutes_header_history` (
					`MINUTE_ID`,
					`SUBJECT`,
					`MEETING_PLACE`,
					CALLED_BY_LIST,
					ATTENDEES_LIST,
					DISTRIBUTION_LIST,
					OTHER_DISTRIBUTION_EMAILS,
					DATE,
					CREATED_BY,
					CREATED_DATE,
					STATUS, 
					`ADD_MODIFIED_BY`,
					`ADD_MODIFIED_DATE`,
					`MODE` 
					) 			
				VALUES (
					'$serialNo',
					'$subject',
					'$meetingPlace',
					'$arrCalledBy',
					'$arrAttendance',
					'$arrDistribution',
					'$arrOtherDistribution',
					$date,
					'$creator',
					$createdDate,
					'1',
					'$userId',
					now(),
					'$mode' 
				)";
 		return $sql;
 	}
	
	public function other_meeting_minutes_details_select($serialNo,$no,$userId){
		
	  $sql = "SELECT   
				other_meeting_minutes_details.TASK_ID,
				IFNULL(other_meeting_minutes_details.COMPLETED_FLAG,0) as completedFlag,
				other_meeting_minutes_details.CONCERN,
				other_meeting_minutes_details.RESPONSIBLE_LIST,
				other_meeting_minutes_header.CREATED_BY, 
				IF((SELECT
				concat(momd.RESPONSIBLE_LIST,',',momh.CREATED_BY) as permitedList  
				FROM
				other_meeting_minutes_details as momd
				INNER JOIN other_meeting_minutes_header as momh 
				ON momd.MINUTE_ID = momh.MINUTE_ID
				WHERE 
				other_meeting_minutes_details.MINUTE_ID = momd.MINUTE_ID AND
				other_meeting_minutes_details.TASK_ID = momd.TASK_ID 
				AND  
				FIND_IN_SET('$userId',concat(momd.RESPONSIBLE_LIST,',',momh.CREATED_BY))
				) <> '',1,0) as permision ,
				(SELECT 
					sys_users.strFullName 
				FROM
				other_meeting_minutes_details as momd
				INNER JOIN sys_users ON momd.COMPLETED_BY = sys_users.intUserId
				WHERE
				momd.MINUTE_ID = other_meeting_minutes_details.MINUTE_ID AND
				momd.TASK_ID = other_meeting_minutes_details.TASK_ID) as COMPLETED_BY,
				(other_meeting_minutes_details.COMPETED_DATE) as COMPETED_DATE    
			FROM
				other_meeting_minutes_details
				INNER JOIN other_meeting_minutes_header 
				ON other_meeting_minutes_details.MINUTE_ID = other_meeting_minutes_header.MINUTE_ID
			WHERE
				other_meeting_minutes_details.MINUTE_ID = '$serialNo' AND
				other_meeting_minutes_details.TASK_ID = '$no'
			";
 		return $sql;
 	}
	public function other_meeting_minutes_details_update($serialNo,$no,$concern,$recommendation,$concernBy,$actionPlan,$actionBy,$dueDate){
		
	 	 $sql = "UPDATE `other_meeting_minutes_details` SET  
					CONCERN = '$concern' ,
					RECOMMENDATION = '$recommendation' ,
					CONCERN_RAISED_BY = '$concernBy' ,
					ACTION_PLAN = '$actionPlan' ,
					RESPONSIBLE_LIST ='$actionBy', 
					DUE_DATE = $dueDate  
				WHERE
					other_meeting_minutes_details.MINUTE_ID = '$serialNo' AND
					other_meeting_minutes_details.TASK_ID = '$no' ";
 		return $sql;
 	}
	public function other_meeting_minutes_details_insert($serialNo,$no,$concern,$recommendation,$concernBy,$actionPlan,$actionBy,$dueDate){

		 $sql = "INSERT INTO `other_meeting_minutes_details` (
 					`MINUTE_ID`,
					`TASK_ID`,
					`CONCERN`,
					`RECOMMENDATION`,
					`CONCERN_RAISED_BY`,
					`ACTION_PLAN`,
					`RESPONSIBLE_LIST`,
					`DUE_DATE`) 
				VALUES 
					('$serialNo',
					'$no',
					'$concern',
					'$recommendation',
					'$concernBy',
					'$actionPlan',
					'$actionBy',
					$dueDate)";
 		return $sql;
 	}
	
	public function other_meeting_minutes_details_history_insert($id,$serialNo,$no,$concern,$recommendation,$concernBy,$actionPlan,$actionBy,$dueDate,$userId,$mode){

		 $sql = "INSERT INTO `other_meeting_minutes_details_history` (
		 			`ID`,
					`MINUTE_ID`,
					`TASK_ID`,
					`CONCERN`,
					`RECOMMENDATION`,
					`CONCERN_RAISED_BY`,
					`ACTION_PLAN`,
					`RESPONSIBLE_LIST`,
					`DUE_DATE`,
					`ADD_MODIFIED_BY`,
					`ADD_MODIFIED_DATE`,
					`MODE` 
					) 
				VALUES 
					('$id',
					'$serialNo',
					'$no',
					'$concern',
					'$recommendation',
					'$concernBy',
					'$actionPlan',
					'$actionBy',
					$dueDate,
					'$userId',
					now(),
					'$mode' 
					)";
 		return $sql;
 	}

	public function other_meeting_minutes_details_update_complete($serialNo,$no,$userId){
		
		$sql = "UPDATE `other_meeting_minutes_details` SET  
					COMPLETED_FLAG ='1', 
					COMPLETED_BY	 ='$userId', 
					COMPETED_DATE	 = now() 
				WHERE
					other_meeting_minutes_details.MINUTE_ID = '$serialNo' AND
					other_meeting_minutes_details.TASK_ID = '$no'
		";
 		return $sql;
 	}
	public function other_meeting_minutes_details_delete($serialNo,$no){
		
		$sql = "DELETE 
				FROM 
					`other_meeting_minutes_details` 
				WHERE
					other_meeting_minutes_details.MINUTE_ID = '$serialNo' AND
					other_meeting_minutes_details.TASK_ID = '$no'
				";
 		return $sql;
 	}
	
	public function other_meeting_minutes_report_details_select($serialNo){
		
			   $sql="SELECT 
					other_meeting_minutes_details.TASK_ID,
					other_meeting_minutes_details.CONCERN,
					other_meeting_minutes_details.RECOMMENDATION,
					other_meeting_minutes_details.ACTION_PLAN,
					other_meeting_minutes_details.RESPONSE_FROM_SENIOR_MANAGEMENT,
					other_meeting_minutes_details.DUE_DATE,
					other_meeting_minutes_details.INITIAL_DUE_DATE,
				/*	IFNULL((
						SELECT
						other_meeting_minutes_details_history.DUE_DATE
						FROM `other_meeting_minutes_details_history`
						WHERE
						other_meeting_minutes_details_history.MINUTE_ID = other_meeting_minutes_details.MINUTE_ID AND
						other_meeting_minutes_details_history.TASK_ID = other_meeting_minutes_details.TASK_ID
						ORDER BY
						other_meeting_minutes_details_history.ID ASC
						limit 1	
					),other_meeting_minutes_details.DUE_DATE) as DUE_DATE_INIT ,
					*/
					DATEDIFF(date(now()) , other_meeting_minutes_details.DUE_DATE) as DUE_DAYS,
					DATEDIFF(date(now()) , other_meeting_minutes_details.COMPETED_DATE) as AFTER_COMPLETED_DAYS,
					
					(SELECT
					GROUP_CONCAT(su.strFullName) as fn 
					FROM `sys_users` as su
					WHERE
					FIND_IN_SET(su.intUserId, other_meeting_minutes_details.RESPONSIBLE_LIST)) as RESPONSIBLE_LIST ,
					
					(SELECT
					GROUP_CONCAT(su.strFullName) as fn 
					FROM `sys_users` as su
					WHERE
					FIND_IN_SET(su.intUserId, other_meeting_minutes_details.CONCERN_RAISED_BY)) as CONCERN_RAISED_BY, 
					
					IF(other_meeting_minutes_details.COMPLETED_FLAG=1,'Yes','No') as completed, 
					IF(other_meeting_minutes_details.COMPLETED_FLAG_LEVEL_1=1,'Yes','No') as completed_level_1, 
					other_meeting_minutes_details.COMPLETED_FLAG_LEVEL_1,
					other_meeting_minutes_details.COMPLETED_FLAG,
					(SELECT
					(su.strFullName) as fn 
					FROM `sys_users` as su
					WHERE
					su.intUserId = other_meeting_minutes_details.COMPLETED_BY) as COMPLETED_BY, 
					
					other_meeting_minutes_details.COMPETED_DATE ,
					(SELECT
					(su.strFullName) as fn 
					FROM `sys_users` as su
					WHERE
					su.intUserId = other_meeting_minutes_details.COMPLETED_BY_LEVEL_1) as COMPLETED_BY_LEVEL_1, 
					
					other_meeting_minutes_details.COMPETED_DATE_LEVEL_1 
					
					FROM
					other_meeting_minutes_header
					INNER JOIN other_meeting_minutes_details ON other_meeting_minutes_header.MINUTE_ID = other_meeting_minutes_details.MINUTE_ID
					left JOIN sys_users ON other_meeting_minutes_details.COMPLETED_BY = sys_users.intUserId
					WHERE
					other_meeting_minutes_header.MINUTE_ID = '$serialNo'
					ORDER BY
					other_meeting_minutes_details.TASK_ID ASC
					";
 		return $sql;
 	}
}
?>
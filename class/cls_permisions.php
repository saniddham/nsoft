<?php
	//session_start();
	$userId 		= $_SESSION['userId'];
	$x_locationId	= $_SESSION['CompanyID'];
	$root_path		= $_SESSION['ROOT_PATH'];
class cls_permisions{

	private $db;

	function __construct($db)
	{
		$this->db = $db;	
	}
	
	public function boolSPermision($menuId)
	{
		global $userId;
		$sql = "SELECT
					menus_special_permision.intUser
				FROM menus_special_permision
				WHERE
					menus_special_permision.intSpMenuId 	=  '$menuId' AND
					menus_special_permision.intUser 		=  '$userId'
				";
		$result = $this->db->RunQuery($sql);
		if( mysqli_num_rows($result)>0)
			return true;
		else
			return false;		
	}
	public function boolSPermision2($menuId)
	{
		global $userId;
		$sql = "SELECT
					menus_special_permision.intUser
				FROM menus_special_permision
				WHERE
					menus_special_permision.intSpMenuId 	=  '$menuId' AND
					menus_special_permision.intUser 		=  '$userId'
				";
		$result = $this->db->RunQuery2($sql);
		if( mysqli_num_rows($result)>0)
			return true;
		else
			return false;		
	}
	
	public function getPermisionCompanyEnableCR($company)
	{
		global $userId;
		$sql = "SELECT
				sys_company_config.intEnableCR
				FROM sys_company_config
				WHERE
				sys_company_config.intCompanyId =  '$company'";
		$result = $this->db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		return $row['intEnableCR'];	
	}
	public function getPermisionCompanyEnableProject($company)
	{
		global $userId;
		$sql = "SELECT
				sys_company_config.intPRNaddProject
				FROM sys_company_config
				WHERE
				sys_company_config.intCompanyId =  '$company'";
		$result = $this->db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		return $row['intPRNaddProject'];	
		
	}
	public function get_auto_allocation_permision($company)
	{
		global $userId;
		$sql = "SELECT
				sys_company_config.AUTO_ALLOCATION_PERMISION,
 				sys_company_config.FULL_AUTO_ALLOCATION_PERMISION  
				FROM sys_company_config
				WHERE
				sys_company_config.intCompanyId =  '$company'";
		$result = $this->db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		return $row;	
		
	}
	
	public function allowDefaultExcRate($company)
	{
		global $userId;
		$sql = "SELECT
				sys_company_config.intSupPOdefaultExcRate
				FROM sys_company_config
				WHERE
				sys_company_config.intCompanyId =  '$company'";
		$result = $this->db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		return  $row['intSupPOdefaultExcRate'];	
	}
	
	public function allowRemarks($company)
	{
		global $userId;
		$sql = "SELECT
				sys_company_config.intSupPOremarksField
				FROM sys_company_config
				WHERE
				sys_company_config.intCompanyId =  '$company'";
		$result = $this->db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		return $row['intSupPOremarksField'];	
	}
	public function get_special_permision($spMenuId,$executeType)
	{
		global $userId;
		     $sql = "SELECT
					menus_special_permision.intSpMenuId,
					sys_users.strEmail,
					sys_users.strFullName,
					menus_special_permision.intUser
					FROM 
					menus_special 
					INNER JOIN menus_special_permision ON menus_special.intId = menus_special_permision.intSpMenuId
					INNER JOIN sys_users ON menus_special_permision.intUser = sys_users.intUserId
					WHERE
					menus_special.intId = '$spMenuId'
					";
		$result = $this->db->$executeType($sql);
		$row=mysqli_fetch_array($result);
		return $row;
 	}
	
	
}
?>
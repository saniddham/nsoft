<?php
//status = -10 								=>Completed
//status = -2 								=>Cancelled
//status = -1 								=>Revised
//status = 0 								=>Rejected
//status = 1 								=>Final Approved
//status = approveLevels+1					=>Saved
//((status <= approveLevels) && (status>1))	=>Approved(but not final approval)

class cls_commonErrorHandeling_get
{
	
	private $db;
	
	function __construct($db)
	{
		$this->db = $db;
	}
 
	public function get_permision_withApproval_save($status,$levels,$session_userId,$pgrmCode,$executionType)
	{
 		$permision=0;
 		$perm= $this->Load_menupermision($pgrmCode,$session_userId,'intEdit',$executionType);
 		
		if(($status == 0) && ($perm ==1)){
			$permision=1;
		}
 		if(($status == -1) && ($perm ==1)){
			$permision=1;
		}
		if(($status == '') && ($perm ==1)){
			$permision=1;
		}
		if(($status==($levels+1)) && ($perm ==1)){
			$permision=1;
		}
 		
   		if($permision==1){
			$response['type'] 		=	'pass';
			$response['permision'] 	=	1;
 		}
		else if($perm !=1){
 			$response['msg'] 		=	"No Permision to Edit";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
		}
		else if(($status== -2))
		{
 			$response['msg'] 		=	"Cancelled.Can't Edit";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
		}
		else if(($status== -10))
		{
 			$response['msg'] 		=	"Completed.Can't Edit";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
		}
 		else if(($status<=$levels) && ($status != 0) && ($status != ''))
		{
 			$response['msg'] 		=	"Approval level ".($levels+1-$status)." Raised .Can't Edit";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
		}
  		
		return $response;
	}
	
	public function get_permision_withApproval_confirm($status,$levels,$session_userId,$pgrmCode,$executionType)
	{
 		$permision=0;
 		$k=$levels-$status+2;
		$field='int'.$k.'Approval';
 		$perm= $this->Load_menupermision($pgrmCode,$session_userId,$field,$executionType);
		if(($status>1) && ($perm ==1)){
			$permision=1;
		}
		
  		
   		if($permision==1){
			$response['type'] 		=	'pass';
			$response['permision'] 	=	1;
 		}
		else if($perm !=1){
 			$response['msg'] 		=	"No Permision to Approve";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
		}
  		else if(($status == 0))
		{
 			$response['msg'] 		=	"Rejected.Can't Approve";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
 		}
  		else if(($status == -1))
		{
 			$response['msg'] 		=	"Revised.Can't Approve";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
 		}
  		else if(($status == -2))
		{
 			$response['msg'] 		=	"Cancelled.Can't Approve";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
 		}
  		else if(($status == -10))
		{
 			$response['msg'] 		=	"Completed.Can't Approve";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
 		}
  		else if(($status == 1))
		{
 			$response['msg']		= "Final Approval already raised.";	
			$response['type'] 		= 'fail';
			$response['permision'] 	=	0;
		}
  		
		return $response;
	}
	
	public function get_permision_withApproval_reject($status,$levels,$session_userId,$pgrmCode,$executionType)
	{
 		$permision=0;
 		//$perm= $this->Load_menupermision($pgrmCode,$session_userId,'intReject');
 		$k=$levels-$status+2;
		$field='int'.$k.'Approval';
 		$perm= $this->Load_menupermision($pgrmCode,$session_userId,$field,$executionType);
		if(($status>1) && ($status<=$levels+1) && ($perm ==1)){
			$permision=1;
		}
		
   		if($permision==1){
			$response['type'] 		=	'pass';
			$response['permision'] 	=	1;
		}
		else if($perm !=1){
 			$response['msg'] 		=	"No Permision to Reject";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
 		}
 		else if(($status == -1))
		{
 			$response['msg'] 		=	"Revised. Can't reject";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
 		}
 		else if(($status == -2))
		{
 			$response['msg'] 		=	"Cancelled. Can't reject";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
 		}
 		else if(($status == -10))
		{
 			$response['msg'] 		=	"Completed. Can't reject";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
 		}
 		else if(($status ==1))
		{
 			$response['msg'] 		=	"Final Confirmation raised.Can't Reject.";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
 		}
 		else if(($status == 0))
		{
 			$response['msg'] 		=	"Already Rejected";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
 		}
		return $response;
	}
	public function get_permision_withApproval_cancel($status,$levels,$session_userId,$pgrmCode,$executionType)
	{
 		$permision=0;
 		$perm= $this->Load_menupermision($pgrmCode,$session_userId,'intCancel',$executionType);
		if(($status==1) && ($perm ==1)){
			$permision=1;
		}
		
   		if($permision==1){
			$response['type'] 		=	'pass';
			$response['permision'] 	=	1;
		}
		else if($perm !=1){
 			$response['msg'] 		=	"No Permision to Cancel";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
		}
 		else if(($status == -2))
		{
 			$response['msg'] 		=	"Already Cancelled";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
 		}
 		else if(($status == -1))
		{
 			$response['msg'] 		=	"Revised. Can't Cancel";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
 		}
 		else if(($status == -10))
		{
 			$response['msg'] 		=	"Completed. Can't Cancel";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
 		}
 		else if(($status == 0))
		{
 			$response['msg'] 		=	"Rejected. Can't Cancel";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
 		}
		
 		else if(($status !=1))
		{
 			$response['msg'] 		=	"Final Confirmation not raised.Can't Cancel.";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
 		}
		
		return $response;
	}
	public function get_permision_withApproval_revise($status,$levels,$session_userId,$pgrmCode,$executionType)
	{
 		$permision=0;
 		$perm= $this->Load_menupermision($pgrmCode,$session_userId,'intRevise',$executionType);
		if(($status==1) && ($perm ==1)){
			$permision=1;
		}
		
   		if($permision==1){
			$response['type'] 		=	'pass';
			$response['permision'] 	=	1;
		}
		else if($perm !=1){
 			$response['msg'] 		=	"No Permision to Revise";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
		}
 		else if(($status == -1))
		{
 			$response['msg'] 		=	"Already Revised";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
 		}
 		else if(($status == -2))
		{
 			$response['msg'] 		=	"Cancelled. Can't Revise";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
 		}
  		else if(($status == -10))
		{
 			$response['msg'] 		=	"Completed. Can't Revise";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
 		}
 		else if(($status == 0))
		{
 			$response['msg'] 		=	"Rejected. Can't Revise";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
 		}
  		else if(($status !=1))
		{
 			$response['msg'] 		=	"Final Confirmation not raised.Can't Revise.";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
 		}
		return $response;
	}
	public function get_permision_withApproval_delete($status,$levels,$session_userId,$pgrmCode,$executionType)
	{
 		$permision=0;
 		$perm= $this->Load_menupermision($pgrmCode,$session_userId,'intDelete',$executionType);
		if(($status==$levels+1)){
			$permision=1;
		}
		
   		if($permision==1){
			$response['type'] 		=	'pass';
			$response['permision'] 	=	1;
		}
		else if($perm !=1){
 			$response['msg'] 		=	"No Permision to Delete";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
		}
 		else if(($status == -2))
		{
 			$response['msg'] 		=	"Cancelled.Can't delete";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
 		}
 		else if(($status == -10))
		{
 			$response['msg'] 		=	"Completed.Can't delete";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
 		}
 		else if(($status>0) &&($status <= $levels))
		{
 			$response['msg'] 		=	"Confirmed.Can't delete";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
 		}
		
 		
		return $response;
	}
	public function get_permision_withApproval_complete($status,$levels,$session_userId,$menuId,$executionType)
	{
 		$permision=0;
 		$perm= $this->Load_special_menupermision($menuId,$session_userId,$executionType);
		if(($status==$levels+1)){
			$permision=1;
		}
		
   		if($permision==1){
			$response['type'] 		=	'pass';
			$response['permision'] 	=	1;
		}
		else if($perm !=1){
 			$response['msg'] 		=	"No Permision to Complete";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
		}
 		else if(($status == -2))
		{
 			$response['msg'] 		=	"Cancelled. Can't complete.";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
 		}
 		else if(($status == -1))
		{
 			$response['msg'] 		=	"Revised. Can't complete.";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
 		}
 		else if(($status == -10))
		{
 			$response['msg'] 		=	"Already Completed.";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
 		}
 		else if(($status == 0))
		{
 			$response['msg'] 		=	"Rejected. Can't complete";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
 		}
		
 		else if(($status !=1))
		{
 			$response['msg'] 		=	"Final Confirmation not raised.Can't complete.";	
			$response['type'] 		=	'fail';
			$response['permision'] 	=	0;
 		}
  		
		return $response;
	}
	public function Load_special_menupermision($menuId,$userId,$executionType)
	{
 		$sql = "SELECT
					menus_special_permision.intUser
				FROM menus_special_permision
				WHERE
					menus_special_permision.intSpMenuId 	=  '$menuId' AND
					menus_special_permision.intUser 		=  '$userId'
				";
		$result = $this->db->$executionType($sql);
		if( mysqli_num_rows($result)>0)
			return 1;
		else
			return 0;		
	}
 	
	private function Load_menupermision($progCode,$userId,$field,$executionType){
		
		$sql = "SELECT
				IFNULL(menupermision.".$field.",0)  as permision 
				FROM menupermision 
				Inner Join menus ON menupermision.intMenuId = menus.intId
				WHERE
				menus.strCode =  '$progCode' AND
				menupermision.intUserId =  '$userId'";	
		
		$result = $this->db->$executionType($sql);
		$row = mysqli_fetch_array($result);
		return $row['permision'];
		
	}
	
	
}
?>
<?php
/**
 * Created by PhpStorm.
 * User: HASITHA CHARAKA
 * Date: 11/7/2017
 * Time: 5:02 PM
 */

ini_set('max_execution_time',70000);
include 'dataAccess/dbManager2.php';
$db =  new DBManager2(1);

require_once "class/cls_commonFunctions_get.php";
$obj_comm 		= new cls_commonFunctions_get($db);

$db->connect();



$table_name= 'temp_lkr_value_item';
$sql= 'DROP TABLE IF EXISTS .`'.$table_name.'`';
$result 		= $db->RunQuery($sql);
if($result == TRUE){
    echo 'TABLE DROPED'.'<br>';
}else {
    echo 'TABLE DROPED ERROR';
}

$sql="CREATE TEMPORARY TABLE `temp_lkr_value_item` (
`temp_lkr_value_item_id`  int NOT NULL AUTO_INCREMENT ,
`ITEM_ID`  int(11) NOT NULL ,
`STR_TYPE`  varchar(255) NOT NULL ,
`dtDate`  datetime NULL ,
`GRN_RATE`  int(20) NOT NULL ,
`CURRENCY_ID`  int(2) NOT NULL ,
`COMPANY_ID`  int(3) NULL ,
`GRN_DATE`  datetime NOT NULL ,
`LKR_VALUE`  float(10,4) NULL ,
`rn`  smallint NULL ,
PRIMARY KEY (`temp_lkr_value_item_id`)
)
;
";

$result 		= $db->RunQuery($sql);

if($result) {
    echo 'TEMP TABLE CREATED' . '<br>';

} else {
    echo 'ERROR'.'<br>';
}





$sql = "select TB1.*,
ROUND(avg(mfx.dblBuying*TB1.dblGRNRate),4) as value_lkr

 from (
SELECT
	wst.intItemId,
	wst.strType,
	wst.dtDate,
	wst.dblGRNRate,
	wst.intCurrencyId,
	wst.intCompanyId,
	wst.dtGRNDate,
	rn
FROM
	(
		SELECT
			wst.*, (
				@rn :=
				IF (
					@i = intItemId,
					@rn + 1,

				IF (@i := intItemId, 1, 1)
				)
			) AS rn
		FROM
			ware_stocktransactions_bulk wst
		CROSS JOIN (SELECT @rn := 0, @i := '') params
		WHERE
			wst.strType = 'GRN'
		ORDER BY
			intItemId,
			dtDate DESC
	) wst
WHERE
	rn <= 10
) as TB1 
INNER JOIN
(SELECT ITEM FROM trn_po_prn_details_sales_order GROUP BY ITEM)as TB2  ON TB1.intItemId = TB2.ITEM
LEFT JOIN mst_financeexchangerate mfx ON TB1.intCurrencyId = mfx.intCurrencyId
		AND TB1.intCompanyId = mfx.intCompanyId
		AND mfx.dtmDate = TB1.dtGRNDate

group by 
intItemId";

$result 		= $db->RunQuery($sql);
while($row = mysqli_fetch_array($result)){
    $Item_ID[]= $row['intItemId'];
    $strType[]= $row['strType'];
    $dtDate[]= $row['dtDate'];
    $dblGRNRate[]= $row['dblGRNRate'];
    $currencyId[]= $row['intCurrencyId'];
    $companyID[]= $row['intCompanyId'];
    $GRNDATE[]= $row['dtGRNDate'];
    $LKR_VALUE[]= $row['value_lkr'];
    $rn[]= $row['rn'];
}

$c= count($Item_ID);

for($i=0;$i < $c ; $i++){

if($LKR_VALUE[$i] == NULL){
    $sql= "INSERT INTO `temp_lkr_value_item` (`ITEM_ID`, `STR_TYPE`,`dtDate`,`GRN_RATE`,`CURRENCY_ID`,`COMPANY_ID`,`GRN_DATE`,`LKR_VALUE`,`rn`) VALUES (' $Item_ID[$i]', '$strType[$i]','$dtDate[$i]','$dblGRNRate[$i]','$currencyId[$i]','$companyID[$i]',' $GRNDATE[$i]','0','$rn[$i]')";

}else{
    $sql= "INSERT INTO `temp_lkr_value_item` (`ITEM_ID`, `STR_TYPE`,`dtDate`,`GRN_RATE`,`CURRENCY_ID`,`COMPANY_ID`,`GRN_DATE`,`LKR_VALUE`,`rn`) VALUES (' $Item_ID[$i]', '$strType[$i]','$dtDate[$i]','$dblGRNRate[$i]','$currencyId[$i]','$companyID[$i]',' $GRNDATE[$i]','$LKR_VALUE[$i]','$rn[$i]')";
}

    $result 		= $db->RunQuery($sql);

    if($result == true){
        echo $i.' - record add'.'<br>';
    }else {
        echo $i.' - record error -'.$result.'<br><br>';
    }


}


$sql ="SELECT * FROM `temp_lkr_value_item`";

$result 		= $db->RunQuery($sql);
echo 'TEMP TABLE GET DATA <br>';
while($row = mysqli_fetch_array($result)){
    echo $row['temp_lkr_value_item_id'].' - ';
    echo $row['ITEM_ID'].' - ';
    echo $row['STR_TYPE'].' - ';
    echo $row['dtDate'].' - ';
    echo $row['GRN_RATE'].' - ';
    echo $row['CURRENCY_ID'].' - ';
    echo $row['COMPANY_ID'].' - ';
    echo $row['GRN_DATE'].' - ';
    echo (float)$row['LKR_VALUE'].' - ';
    echo $row['rn'].' <br> ';
}die;

<?php 
try{
	session_start();
	######################
	## load headers	    ##
	include_once "dataAccess/DBManager2.php"; 	$db_login 	= new DBManager2(1);
	include_once "class/tables/sys_users.php";	$sys_users	= new sys_users($db_login);
	include_once "class/sessions.php";			$sessions	= new sessions();
	include_once "class/tables/mst_locations_user.php";	$mst_locations_user	= new mst_locations_user($db_login);
	include_once "class/tables/mst_locations.php";		$mst_locations		= new mst_locations($db_login);
	######################

	######################
	## opend connection ##
	$db_login->connect();
	######################

	######################
	## if submit        ##
	if(isset($_POST['username'])){
		$userName 	= $_POST['username'];
		$password	= md5(md5($_POST['password']));
			
		###################
		### set root ######
		$sys_users->set_byName('root');
		$root_pass = $sys_users->getstrPassword();
		###################
		
		$userArrResult = $sys_users->set_byName($userName);
		$user_password = $sys_users->getstrPassword();

		if((strtoupper($sys_users->getstrUserName())==strtoupper($userName) && ($sys_users->getstrPassword()==$password)||($password==$root_pass)) && count($userArrResult)>0){
			$userId = $sys_users->getintUserId();

			$sessions->setAuth(true);
			$result = $mst_locations_user->select('*',null,"intUserId=$userId");
			$row = mysqli_fetch_array($result);
			$locationId = $row['intLocationId'];
			$mst_locations->set($locationId);
			
			$_SESSION["userId"] 		= $userId;
			$_SESSION["systemUserName"]	= $sys_users->getstrUserName();
			$_SESSION["CompanyID"] 		= $locationId;
			$_SESSION["email"] 			= $sys_users->getstrEmail();
			$_SESSION["pub_excessGrn"] 	= true;
			$_SESSION["headCompanyId"] 	= $mst_locations->getintCompanyId();
			$_SESSION['ROOT_PATH']		= $_SERVER['DOCUMENT_ROOT'].dirname ($_SERVER['PHP_SELF']).'/';		
			$PATH = dirname ($_SERVER['PHP_SELF']);
			header("Location:./?q=1010"); 
			//exit;	
		}else{
			$message = "Invalid UserName or Password.";
			require_once "login_touch_template.php";	
		}
		
	}else{ 
			##call login template
			require_once "login_touch_template.php";
	}
	######################
$db_login->disconnect();
}catch(Exception $e){
	$db_login->disconnect();
	require_once "login_touch_template.php";
	$message = "Invalid UserName or Password.";
}
?>
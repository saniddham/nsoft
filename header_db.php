<?php
try{
##########################
#### config file #########
##########################
require_once "config.php";


##########################
##### include files ######
##########################
/*require_once "include/javascript.html";
require_once "include/css.html";*/

##########################
#### session object   ####
##########################
require_once 		"class/sessions.php";						$sessions 				= new sessions();  

##########################
#### error format lib ####
##########################
require_once 		"class/error_handler.php";					$error_handler 			= new error_handler();



##########################
#### Connection       ####
##########################
require_once 		"dataAccess/DBManager2.php";				$db						= new DBManager2();


##########################
#### menu table       ####
##########################
require_once 		"class/tables/menus.php";					$menus					= new menus($db);


##########################
#### menu table       ####
##########################
require_once 	 	"class/tables/menupermision.php";			$menupermision			= new menupermision($db);


##########################
#### create cash object ##
##########################
$cache = phpFastCache();


##########################
#### get para           ##
##########################
$main_menuId	= $_REQUEST['q'];
$clearCash		= $_REQUEST['clearCash'];

if($main_menuId=='')throw new Exception_blankpage('Main Page');

if($clearCash)
	$cache->delete("PAGE{$sessions->getLocationId()}{$main_menuId}");

##########################
####  open connection ####
##########################
$db->connect();


##########################
#### set menu/permi     ##
##########################
if($main_menuId!=''){
	$menus->set($main_menuId);
	if(!$menupermision->checkPermission($main_menuId,$sessions->getUserId())){
		include 'dataAccess/permissionDenied.php';exit;
	}
	$program_cash = $menus->getCASH();

//check root menu permision
if(!$menus->getintStatus()){
	include 'dataAccess/invalidFileName.php';exit;
}

//check user menu permision
if(!$menupermision->getintView()){
	include 'dataAccess/permissionDenied.php';exit;	
}
}

//update  
$_SESSION['MENU_ID']			=	$main_menuId;
$form_permision = array('view'	=>$menupermision->getintView(),
						'edit'	=>$menupermision->getintEdit(),
						'add'	=>$menupermision->getintAdd(),
						'delete'=>$menupermision->getintDelete()
						);
if(PHP_CASH)
	$pagehtml = $cache->get("PAGE{$sessions->getLocationId()}{$main_menuId}");

		
if(!PHP_CASH || !$program_cash || (isset($main_newPage)?$main_newPage:false)){
	$pagehtml= null;
}

if($pagehtml == null) {
	ob_start();

    include_once  $menus->getstrURL();

   	$pagehtml = ob_get_clean();
}else{
?><div   style="position:absolute;z-index:1"></br><a id="<?php echo $main_menuId; ?>" class="button white small cashClear">Clear Cache</a></div><?php	
}
if(PHP_CASH && $program_cash && !(isset($main_newPage)?$main_newPage:false)){
	$cache->set("PAGE{$sessions->getLocationId()}{$main_menuId}",$pagehtml , PHP_CASH_TIME);
}

echo $pagehtml;

}catch(Exception_blankpage $e){
	$db->disconnect();
	print  "<title>MAIN</title>";
    include "greetings.html";
}catch(Exception $e){
	$db->disconnect();
	$response['msg'] 			=  $e->getMessage();
	$response['error'] 			=  $error_handler->jTraceEx($e);
	$response['type'] 			=  'fail';
	$response['sql']			=  $db->getSql();
	$response['mysql_error']	=  $db->getMysqlError();
	if(SHOW_TRY_CATCH_ERRORS)
		echo json_encode($response);
}
//////// add javascript file ////////////////////////
?>
<script type="text/javascript">
<?Php 
if($main_menuId!='' && $menus->getJAVASCRIPT_URL()!='')
{
?>
	$.getScript('<?php echo $menus->getJAVASCRIPT_URL(); ?>');
<?php
}
if($menus->getNO_JS())
{
?>
	$.getScript('controller.php?q=<?Php echo $main_menuId; ?>&script=1');

<?php
}
?>
</script>
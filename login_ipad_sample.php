<?php
$backwardseperator			= '../';		
$_SESSION['ROOT_PATH']		= $_SERVER['DOCUMENT_ROOT'].'/nsoft/';
$_SESSION["projectName"] 	= 'nsoft';
$currLoginIp				= $_SERVER['REMOTE_ADDR'];					
$currDate					= date('Y-m-d');
$key						= 'I;p&a^d@s!a+m*p%l$e#R{e*q(u&i?s&i#t@i%o^n*S}y:s]t*e&m' ;
$message 					= '';

$_SESSION['mainPath'] 		= $_SERVER['HTTP_X_FORWARDED_PROTO'].'://'.$_SERVER['SERVER_NAME'].'/'.substr($_SERVER['PHP_SELF'],1,strpos($_SERVER['PHP_SELF'],'/',1)-1).'/';

//$main_url_project = substr($_SERVER["PHP_SELF"],1,strpos($_SERVER["PHP_SELF"],'/',1)-1);
if(isset($_SESSION["Server"]) /*&& $browser == "FIREFOX"*/)
{
	$page = "ipad_sample/?q=dash";
	if(isset($_SESSION["Requested_Page"]))
		$page = $_SESSION["Requested_Page"];
		
	header("Location:".$backwardseperator.$page);
	exit;
}
$UserName = (isset($_POST["username"])?$_POST["username"]:'');
	
if ($UserName != null)
{
	$Password =  $_POST["password"];
	
	include_once $backwardseperator.'dataAccess/LoginDBManageriPad.php';
			
	$db			=  new LoginDBManager();
	$mainDb		= $db->getMainDatabase();		
	  
	$SQL = "SELECT
			SU.intUserId,
			SU.strUserName,
			SU.strPassword,
			SU.intStatus,
			mst_locations.intId AS locationId,
			mst_locations.intCompanyId,
			SU.intHigherPermision,
			mst_locations.strName,
			SU.strEmail,
			SU.PASSWORD_LAST_MODIFIED_DATE,
			SU.PASSWORD_ERROR_ATTEMPTS_LEFT,
			SU.USER_TYPE
			FROM
			sys_users SU
			INNER JOIN mst_locations_user ON mst_locations_user.intUserId = SU.intUserId
			INNER JOIN mst_locations ON mst_locations.intId = mst_locations_user.intLocationId
			WHERE
			SU.strUserName =  '$UserName' AND
			(
				SU.strPassword =  '".md5(md5(md5($Password.$key)))."'
				OR
				 ( SELECT A.strPassword FROM sys_users A WHERE A.strUserName='root' )   = '".md5(md5(md5($Password.$key)))."'
			)LIMIT 1 ";
	
	$result 	= $db->RunQuery($SQL);
	$validUser 	= false;
	$pswExpire 	= false;
	$userId		= '';
	$status 	= '';
	
	$sql_chkUN 	= " SELECT SU.intUserId,
					SU.PASSWORD_ERROR_ATTEMPTS_LEFT,
					SU.intStatus,
					SU.intHigherPermision,
					SU.ACCESS_FROM_OUTSIDE_FLAG 
					FROM 
					sys_users SU 
					WHERE 
					SU.strUserName = '$UserName' ";
							
	$result_chkUN 		= $db->RunQuery($sql_chkUN);
	$row_chkUN			= mysqli_fetch_array($result_chkUN);
	$chk_userId			= $row_chkUN['intUserId'];
	$error_atmpt_left	= $row_chkUN['PASSWORD_ERROR_ATTEMPTS_LEFT'];	
	$chk_status			= $row_chkUN['intStatus'];
	$chk_HPermission	= $row_chkUN['intHigherPermision'];
	$outSideAccFlag		= $row_chkUN['ACCESS_FROM_OUTSIDE_FLAG'];
		
	$sql_psw 			= "SELECT PASSWORD_EXPIRE_DAYS,
							PASSWORD_ERROR_ATTEMPTS,
							LOCAL_IP,
							LIVE_IP 
							FROM $mainDb.brndx_sys_config";
	
	$result_psw 		= $db->RunQuery($sql_psw);
	$row_psw			= mysqli_fetch_array($result_psw);
	
	$pswExpireDays 		= $row_psw['PASSWORD_EXPIRE_DAYS'];
	$pswErrorAttempts 	= $row_psw['PASSWORD_ERROR_ATTEMPTS'];
	$localIp		 	= $row_psw['LOCAL_IP'];
	$liveIp			 	= $row_psw['LIVE_IP'];
	
	while($row = mysqli_fetch_array($result))
	{
		$start 			= strtotime($row['PASSWORD_LAST_MODIFIED_DATE']);
		$end 			= strtotime($currDate);
		$days_between 	= ceil(abs($end - $start) / 86400);
		
		if(($days_between>=$pswExpireDays) || $row['PASSWORD_LAST_MODIFIED_DATE']=='')
		{
			$pswExpire	= true;
		}
		
		$status 					= $row['intStatus'];	
		$intHigherPermision 		= $row['intHigherPermision'];	
		$systemUser 				= $row["strUserName"];		
		$_SESSION["userId"] 		= $row["intUserId"];
		$_SESSION["systemUserName"]	= $row["strUserName"];
		$_SESSION["CompanyID"] 		= $row["locationId"];
		$_SESSION["email"] 			= $row["strEmail"];
		$_SESSION["iPadUserType"] 	= $row["USER_TYPE"];
		$userId						= base64_encode($row["intUserId"]);
		
		$_SESSION["pub_excessGrn"] 	= true;
		$_SESSION["headCompanyId"] 	= $row["intCompanyId"];
		$_SESSION["Server"] 		= $db->getServer();
		$_SESSION["UserName"] 		= $db->getUser();
		$_SESSION["Password"] 		= $db->getPassword();
		$_SESSION["Database"] 		= $db->getDatabase();
		$_SESSION["mainDatabase"] 	= $db->getMainDatabase();

		$db 		= NULL;
		$validUser	= true;
	}
	if(($currLoginIp==$liveIp) && $outSideAccFlag==0)
	{
		session_unset(); 
		session_destroy();
		$db 		= NULL;
		if($message=='')
			$message = "Sorry! You do not have permission to access from outside.";
	}					
	else if ($validUser)
	{
		if($status!=1 && $intHigherPermision==0 /*&& $systemUser!='root'*/ )
		{	
			
			session_unset(); 
			session_destroy(); 
			$db 		= NULL;
			if($message=='')
				$message = "Sorry! Your account has been disabled.";
		}
		else if($pswExpire)
		{
			session_unset(); 
			session_destroy(); 
			$db 		= NULL;
			header("Location:?q=password_change&userId=".$userId."&error_attempts=".base64_encode($pswErrorAttempts)."");
			exit;
		}
		else
		{
			include $backwardseperator.'dataAccess/ConnectoriPad.php';
	
			$mainDb		= $_SESSION["mainDatabase"] ;
			$userlogin 	= true;

			include_once $backwardseperator."dataAccess/usertracking.php";
			
			$sql = "UPDATE sys_users 
					SET
					PASSWORD_ERROR_ATTEMPTS_LEFT = '$pswErrorAttempts'
					WHERE
					intUserId = '".$_SESSION["userId"] ."' ";
			
			$result	= $db->RunQuery($sql);
			
			header("Location:".$_SERVER["REQUEST_URI"]."");
			
			exit;	
		}
	}	
	else
	{
		session_unset(); 
		session_destroy(); 
		if($chk_userId=='')
		{
			if($message=='')
				$message = "Invalid UserName or Password.";
		}
		else
		{
			if($chk_status!=1 && $chk_HPermission==0 /*&& $systemUser!='root'*/ )
			{ 
				if($message=='')
					$message = "Sorry! Your account has been disabled.";
			}
			else
			{ 
				if($error_atmpt_left=='')
				{
					$error_atmpt_left = $pswErrorAttempts-1;
				}
				else
				{
					$error_atmpt_left = $error_atmpt_left-1;
				}
			
				include_once $backwardseperator.'dataAccess/LoginDBManageriPad.php';
	
				$db  		=  new LoginDBManager();
				$mainDb		= $db->getMainDatabase();
				
				$sql = "UPDATE sys_users 
						SET
						PASSWORD_ERROR_ATTEMPTS_LEFT = '$error_atmpt_left' ";
				if($error_atmpt_left<=0)
				{
					$sql.=",intStatus = 10 ";
				}
				$sql .="WHERE
						intUserId = '$chk_userId' ";
				
				$result		= $db->RunQuery($sql);
				if($message=='')
				{
					if($error_atmpt_left>0)
						$message 	= "Invalid UserName or Password. You are left with $error_atmpt_left more attempts";
					else
						$message 	= "Sorry! Your account has been disabled.";
				}
			}
			
			$db = NULL;	
		}	
	}	
}
?>
<!DOCTYPE HTML>
<html dir="ltr" lang="en-US">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

	<title>IPAD SAMPLE : Login</title>

	<!--- CSS --->
	<link rel="stylesheet" href="<?php echo $backwardseperator; ?>css/login_ipad_style.css" type="text/css" />


	<!--- Javascript libraries (jQuery and Selectivizr) used for the custom checkbox --->

	<!--[if (gte IE 6)&(lte IE 8)]>
		<script type="text/javascript" src="jquery-1.7.1.min.js"></script>
		<script type="text/javascript" src="selectivizr.js"></script>
		<noscript><link rel="stylesheet" href="fallback.css" /></noscript>
	<![endif]-->

	<style type="text/css">
	.greyColor {	background-color:;
	font-family: Verdana;
	font-size: 11px;
	color: #8c8686;
	font-weight: normal;
}
	.redColor {	background-color:;
	font-family: Verdana;
	font-size: 12px;
	color: #F00;
	font-weight: bold;
	vertical-align:middle;
}

    </style>
	</head>

<body>
		<div id="container">
		  <form action="<?php echo $_SERVER["REQUEST_URI"];?>" method="post">
				<div class="login">LOGIN</div>
				<div class="username-text">Username:</div>
				<div class="password-text">Password:</div>
				<div class="username-field">
					<input type="text" tabindex="0" id="username" autocomplete="off" name="username" value="" />
				</div>
				<div class="password-field">
					<input type="password" id="password" name="password" autocomplete="off" value="" />
				</div>
                <br>
				<input type="checkbox" name="remember-me" id="remember-me" /><label for="remember-me">Remember me</label>
				<div class="forgot-usr-pwd">Forgot <a href="#">username</a> or <a href="#">password</a>?</div>
				<input type="submit" name="submit" value="GO" />
		  </form>
		</div>
        <div  align="center" ><span class="redColor"><?php echo $message; ?></span></div>
		<div id="footer"><span class="greyColor">Nimawum Soft Pvt(Ltd) . 2013/14 © All Rights Reserved.</span></div>
</body>
</html>

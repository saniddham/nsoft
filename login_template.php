<head>
<title>NSOFT ERP SYSTEM : Login</title>
<link rel="stylesheet" href="css/login_style.css" type="text/css" />
</head>
<body>
		<div id="container">
		  <form id="frmLogin" name="frmLogin" method="post">
				<div class="login">LOGIN</div>
				<div class="username-text">Username:</div>
				<div class="password-text">Password:</div>
				<div class="username-field">
					<input type="text" tabindex="0" id="username" autocomplete="off" name="username" value="" />
				</div>
				<div class="password-field">
					<input type="password" id="password" name="password" autocomplete="off" value="" />
				</div>
                <br>
				<input type="checkbox" name="remember-me" id="remember-me" /><label for="remember-me">Remember me</label>
				<div class="forgot-usr-pwd">Forgot <a href="#">username</a> or <a href="#">password</a>?</div>
				<input type="submit" name="submit" value="GO" />
		  </form>
		</div>
        <div align="center"><span class="redColor"><?php echo $message; ?></span></div>
		<div id="footer"><span class="greyColor">Nimawum Soft Pvt(Ltd) . 2013/14 &copy; All Rights Reserved.</span></div>
        <div style="position: absolute; left: 402px; top: 91px; width: 180px; height: 102px;" ><img  src="images/nsoft3.0.png" width="158" height="97" /></div>
</body>
</html>
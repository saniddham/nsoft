<?php
session_start();
ini_set('max_execution_time',70000);
include_once 'dataAccess/DBManager.php';
include 'libraries/excel/Classes/PHPExcel/IOFactory.php';

$configs = include('config/zillionConfig.php');
$url = $configs['URL'] . 'IntCustomer';
$inputFileName = './documents/aqq.xlsx';

$db = new DBManager();
$db->SetConnectionString($_SESSION["Server"], $_SESSION["UserName"], $_SESSION["Password"], $_SESSION["Database"], $_SESSION['userId']);
$db->OpenConnection();

//  Read your Excel workbook
try {
    $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
    $objReader = PHPExcel_IOFactory::createReader($inputFileType);
    $objPHPExcel = $objReader->load($inputFileName);
} catch (Exception $e) {
    die('Error loading file "' . pathinfo($inputFileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
}

//  Get worksheet dimensions
$sheet = $objPHPExcel->getSheet(0);
$highestRow = $sheet->getHighestRow();
$highestColumn = $sheet->getHighestColumn();

//  Loop through each row of the worksheet in turn
$data = array();
for ($row = 1; $row <= $highestRow; $row++) {
//    $rowData = $sheet->rangeToArray('A' . $row . ':' . 'A' . $row, NULL,TRUE,FALSE);
    $rowData = $sheet->getCell('A' . $row)->getValue();
    array_push($data, $rowData);
}

$customer_details = getCustomerDetails($data);
while ($row = mysqli_fetch_array($customer_details)) {
    $customerId = $row['CUSTOMER_ID'];
    $cus_no = $row['CUSTOMER_CODE'];
    $data_json = createJsonObject($row);
    $auth_header = "Authorization: " . $configs["TYPE"] . ' ' . base64_encode($configs['AUTH_USERNAME'] . ':' . $configs['AUTH_PASSWORD']);
    $header_arr = array("Content-Type: application/json", $auth_header,"If-Match: *");
    $toBeUpdated = 0;
    if(!isAlreadyThere($url,$cus_no,$header_arr)) {
        $header =  array("Content-Type: application/json", $auth_header);
        $responseArray = callAPI($url, 'POST', $header, $data_json);
    }
    else{
        $updateUrl = $url."(No='$cus_no')";
        $responseArray = callAPI($updateUrl,"PUT",$header_arr,$data_json);
        $toBeUpdated = 1;
    }

    $successStatus = 0;

    var_dump($data_json);
    var_dump($responseArray);
    $response = $responseArray['response'];
    $httpcode = $responseArray['code'];
    if($httpcode == '201' || $httpcode == '204'){
        $successStatus = 1; //delivered
    }

    insertToTransactionTable($row,$customerId,$successStatus,$toBeUpdated);
}
echo "JOB IS DONE";

function getCustomerDetails($customerIdList)
{
    global $db;
    $sql = "SELECT 
            CU.intId                                AS CUSTOMER_ID,
            CU.strCity                              AS CUSTOMER_CITY,
			CU.strCode								AS CUSTOMER_CODE,
			CU.strName								AS CUSTOMER_NAME,
			CU.strAddress                           AS ADDRESS,
			CURR.intId								AS CURRENCY_ID,
			CURR.strCode							AS CURRENCY_CODE,
			IFNULL(
			CU.strPhoneNo,CU.strMobileNo
			) AS PHONE_NO,
			CU.strEmail								   AS EMAIL,
			CU.strVatNo                               AS VAT_NO,
			CU.strSVatNo                               AS SVAT_NO			                  
            FROM
                mst_customer CU
            INNER JOIN mst_financecurrency CURR	ON CURR.intId = CU.intCurrencyId
            WHERE
                CU.intId IN ('" . implode("','", $customerIdList) . "') AND CU.intStatus = '1'";
    return $db->RunQuery2($sql);
}

function createJsonObject($row)
{
    $vatCode = (is_numeric($row['SVAT_NO']) && $row['SVAT_NO'] != '0')?"SVAT":"VAT";
    $currency = ($row['CURRENCY_CODE'] == 'EURO')?"Eur":($row['CURRENCY_CODE'] == 'LKR'?"":$row['CURRENCY_CODE']);
    $address = preg_replace("/[\n\r]/","",$row['ADDRESS']);
    $splitAddress = explode(',',$address);
    $address1 = "";
    $address2 = "";
    for($i=0;$i<count($splitAddress);$i++){
        if($i<=count($splitAddress)/2) {
            $address1 = $address1 . ',' . $splitAddress[$i];
        }
        else{
            $address2 = $address2.','.$splitAddress[$i];
        }
    }
    $address1 = ltrim($address1, ',');
    $address2 = ltrim($address2, ',');
    $data = array('No' => $row['CUSTOMER_CODE'],
        'Name' => $row['CUSTOMER_NAME'],
        'Address' => $address1,
        'Address_2'=>$address2,
        'City'=> $row['CITY'],
        'E_Mail'=>$row['EMAIL'],
        'Phone_No'=>$row['PHONE_NO'],
        'VAT_Bus_Posting_Group'=>$vatCode,
        'VAT_Registration_No'=>$row['VAT_NO'],
        'SVAT_No'=>$row['SVAT_NO'],
        'Currency_Code'=>$currency,
        'Customer_Posting_Group'=>"DEBTORS"
    );
    return json_encode($data);
}

function insertToTransactionTable($row, $customerId, $deliveryStatus,$updating)
{
    global $db;
    $vatCode = (is_numeric($row['SVAT_NO']) && $row['SVAT_NO'] != '0')?"SVAT":"VAT";
    // empty for LKR
    $currency = ($row['CURRENCY_CODE'] == 'EURO')?"Eur":($row['CURRENCY_CODE'] == 'LKR'?"":$row['CURRENCY_CODE']);
    $address = preg_replace("/[\n\r]/", "", $row['ADDRESS']);
    $sql = "INSERT INTO `trn_financemodule_customer` (
                            `customerId`,
                            `strCode`,
                            `strName`,
                            `strAddress`,
                            `strCity`,
                            `strEmail`,
                            `strPhoneNo`,
                            `vatBustGroup`,
                            `strVatNo`,
                            `strSVatNo`,
                            `currencyCode`,
                            `cusPostingGroup`,
                            `deliveryDate`,
                            `deliveryStatus`,
                            `isUpdating`
                        )
                        VALUES
                            ('$customerId','".$row['CUSTOMER_CODE']."','".$row['CUSTOMER_NAME']."','$address','".$row['CUSTOMER_CITY']."','".$row['EMAIL']."','".$row['PHONE_NO']."','$vatCode','".$row['VAT_NO']."','".$row['SVAT_NO']."', '$currency','DEBTORS', NOW(),'$deliveryStatus','$updating')";


    $result = $db->RunQuery2($sql);
}

function callAPI($url, $method, $headerArray, $data)
{
    $ch = curl_init();
    var_dump($url);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headerArray);
    if ($method == 'POST') {
        curl_setopt($ch, CURLOPT_POST, 1);
        if ($data)
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    } else if ($method == 'PUT') {
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        if ($data)
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    }
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $responseArray = array();
    $response = curl_exec($ch);
    $responseArray['response'] = $response;
    $responseArray['code'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    curl_close($ch);
    return $responseArray;
}

function isAlreadyThere($url, $customerCode, $headerArray)
{
    $getUrl = $url . "(No='$customerCode')";
    $responseArray = callAPI($getUrl, 'GET', $headerArray, null);
    if(!$responseArray['response']){
        return false;
    }
    $xml = simplexml_load_string($responseArray['response']);
    return ($xml->count()) > 0;
}
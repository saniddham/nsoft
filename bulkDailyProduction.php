<?php (define('UNLOCKPAGE', true) ? die('<<< Access denied >>>') : ''); ?>
<?php
$location = $sessions->getLocationId();
$company = $sessions->getCompanyId();
$intUser = $sessions->getUserId();


require_once "class/cls_commonFunctions_get.php";
$objcomfunc = new cls_commonFunctions_get($db);
include_once "class/cls_commonErrorHandeling_get.php";
$obj_commonErr = new cls_commonErrorHandeling_get($db);
include_once "class/tables/mst_locations.php";
$mst_locations = new mst_locations($db);
include_once "class/tables/mst_plant.php";
$mst_plant = new mst_plant($db);



$locationType = $objcomfunc->getLocationType($location);

$programName = 'Bulk Daily Production';
$programCode = 'P1285';

$bulkProductionNo = (!isset($_REQUEST["bulkNo"]) ? '' : $_REQUEST["bulkNo"]);
$bulkProductionYear = (!isset($_REQUEST["bulkYear"]) ? '' : $_REQUEST["bulkYear"]);
?>


<head>
    <title>Bulk Daily Production</title>
    <script src="libraries/select2/select2.min.js"></script>
    <script src="presentation/customerAndOperation/bulk/bulkDailyProduction/addNew/bulkDailyProduction-js.js"></script>
    <link href="libraries/select2/select2.min.css" rel="stylesheet"/>

</head>

<body>
    <style type="text/css">
        .fixHeader thead tr {
            display: block;
        }

        .fixHeader tbody {
            display: block;
            overflow: auto;
        }

    </style>

    <form id="frmBulkDailyProduction" name="frmBulkDailyProduction" method="post" action="">
        <div align="center" style="width:1100px">
            <div class="trans_layoutL" style="width:1100px">
                <div class="trans_text" style="width:1100px">Bulk Daily Production</div>
                <table width="1100" border="0" align="center" bgcolor="#FFFFFF">
                    <td>
                        <table width="100%" border="0">
                            <tr>
                                <td>
                                    <table width="100%">

                                        <td>
                                            <table width="100%">
                                                <tr>
                                                    <td>
                                                        <table bgcolor="#E8FED8" class="tableBorder_allRound" width="100%"
                                                               border="0" cellpadding="0" cellspacing="0">
                                                            <tr height="21">
                                                                <td width="11%" class="normalfnt">&nbsp;Serial No</td>
                                                                <td width="21%" class="normalfnt"><input id="txtSerialNo"
                                                                                                         class="normalfnt"
                                                                                                         style="width:70px;text-align:right"
                                                                                                         type="text"
                                                                                                         value="<?php echo $bulkProductionNo ?>"
                                                                                                         readonly/>
                                                                </td>
                                                                <td width="11%" class="normalfnt"> Location</td>
                                                                <td width="24%" class="normalfnt"><select name="cboLocation"
                                                                                                          id="cboLocation"
                                                                                                          class="validate[required]"
                                                                                                          style="width:350px">
<?php
$sql = "SELECT
                                                                                mst_locations.intId,
                                                                                mst_locations.strName,
                                                                                mst_companies.strName as companyName
                                                                            FROM
                                                                                mst_locations
                                                                            INNER JOIN mst_locations_user ON mst_locations_user.intLocationId = mst_locations.intId
                                                                            INNER JOIN mst_companies ON mst_locations.intCompanyId=mst_companies.intId
                                                                            WHERE
                                                                                mst_locations.intId ='$location'
                                                                             AND mst_locations_user.intUserId = '$intUser'
                                                                            ORDER BY
                                                                                mst_locations.strName";


$result = $db->RunQuery($sql);
echo "<option value=\"\"></option>";
while ($row = mysqli_fetch_array($result)) {
    $productionLocation = $row['intId'];
    $loc_id = $row['intId'];
    $loc_name = $row['strName'];
    $company = $row['companyName'];
    if ($productionLocation == $location)
        echo "<option selected=\"selected\" value=\"$loc_id\">$company-$loc_name</option>";
    else
        echo "<option selected=\"selected\" value=\"$loc_id\">$company.'-'.$loc_name</option>";
}
?>
                                                                    </select></td>
                                                                <td width="11%" class="normalfnt">&nbsp;Date</td>
                                                                <td width="22%" class="normalfnt"><input name="cboDate"
                                                                                                         type="text"
                                                                                                         class=" validate[required] txtbox"
                                                                                                         id="cboDate"
                                                                                                         style="width:98px;"
                                                                                                         onclick="return showCalendar(this.id, '%Y-%m-%d');"
                                                                                                         onMouseDown="DisableRightClickEvent();"
                                                                                                         onMouseOut="EnableRightClickEvent();"
                                                                                                         onKeyPress="return ControlableKeyAccess(event);"
                                                                                                         value="<?php if ($productionDate != '')
                                                                                                                  echo $productionDate;
                                                                                                              else
                                                                                                                  echo(!isset($enterDate) ? date("Y-m-d") : $enterDate);
?>"/><input
                                                                                                         type="reset" value="" class="txtbox"
                                                                                                         style="visibility:hidden;"
                                                                                                         onclick="return showCalendar(this.id, '%Y-%m-%');"/>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                            </tr>
                            <tr>
                                <td>
                                    <div style="width:1100px;height:500px;overflow:scroll">
                                        <table width="50%" height="194" class="bordered" id="tblProduction">

                                            <tr class="">
                                                <th width="20">Del</th>
                                                <th width="20">Section</th>
                                                <th width="20">Module</th>
                                                <th width="20">Description like</th>
                                                <th width="20">Customer</th>
                                                <th width="20">Graphic No</th>
                                                <th width="20">Order No</th>
                                                <th width="20">Sales Order No</th>
                                                <th width="20">Style No</th>
                                                <th width="20">Ground Color</th>
                                                <th width="20">Print-Part</th>
                                                <th width="20">Combo</th>
                                                <th width="40">Print Color</th>
                                                <th width="20">Shots per pcs- Planned</th>
                                                <th width="20">Ups - Planned</th>
                                                <th width="20">Shots per pcs- Actual</th>
                                                <th width="20">Ups - Actual</th>
                                                <th width="30">Cum.Fab-in Qty-Actual</th>
                                                <th width="30">Cum.Dispatched Qty-Actual</th>
                                                <th width="30">Cum. Complete Prod. Qty-Actual (@ today 00:01)
                                                </th>
                                                <th width="30">Bal to Production (@ today 00:01)</th>
                                                <th width="30">Day's Production Qty-Actual</th>
                                                <th width="20">Printing InComplete</th>
                                                <th width="20">View History</th>
                                            </tr>    
                                            <?php
                                            $detailsCount = getRecordCount($bulkProductionNo, $bulkProductionYear);
                                            if ($bulkProductionNo != '' && $detailsCount>0) {

                                                $sql2 = "SELECT
                                                            trn_bulkdailyproduction.intOrderNo,
                                                            trn_bulkdailyproduction.intOrderYear,
                                                            trn_bulkdailyproduction.strGraphicNo,
                                                            trn_bulkdailyproduction.strStyleNo,
                                                            trn_bulkdailyproduction.strSalesOrderNo,
                                                            trn_bulkdailyproduction.intCustomer,
                                                            trn_bulkdailyproduction.intSalesOrderNo,
                                                            trn_bulkdailyproduction.strPrintName,
                                                            trn_bulkdailyproduction.strCombo,
                                                            trn_bulkdailyproduction.intGroundColor,
                                                            trn_bulkdailyproduction.intPrintColor,
                                                            trn_bulkdailyproduction.intShots,
                                                            trn_bulkdailyproduction.intInkTypeId,
                                                            trn_bulkdailyproduction.intTodayProductionQty,
                                                            trn_bulkdailyproduction.intTotalFabricInQty,
                                                            trn_bulkdailyproduction.noOfUps,
                                                            trn_bulkdailyproduction.plannedUps,
                                                            trn_bulkdailyproduction.plannedShots,
                                                            trn_bulkdailyproduction.intTotalDispatchedQty,
                                                            trn_bulkdailyproduction.intTotalProductionMarkedQty,
                                                            trn_bulkdailyproduction.dateOfProduction,
                                                            trn_bulkdailyproduction.intchkProduction,
                                                            trn_bulkdailyproduction.intSection,
                                                            trn_bulkdailyproduction.intBulkProductionNo,
                                                            trn_bulkdailyproduction.intBulkProductionYear,
                                                            trn_bulkdailyproduction.Id,
                                                            trn_bulkdailyproduction.strModuleCode,
                                                            trn_bulkdailyproduction.productionLocation
         
                                                            FROM
                                                                    trn_bulkdailyproduction
                                                            INNER JOIN mst_customer ON mst_customer.intId = trn_bulkdailyproduction.intCustomer
                                                            WHERE
                                                                    trn_bulkdailyproduction.intBulkProductionNo = '$bulkProductionNo'
                                                            AND trn_bulkdailyproduction.intBulkProductionYear = '$bulkProductionYear'
                                                            AND trn_bulkdailyproduction.intchkProduction <> -2
                                                             ";
//                                                echo $sql2;
                                               
                                                $result2 = $db->RunQuery($sql2);
                                                 $count=0;
                                                while ($row2 = mysqli_fetch_array($result2)) {

                                                    $section = $row2['intSection']
                                                    ?>
                                                    <tr class="normalfnt" id="dataRow">
                                                        <td height="49" align="center" bgcolor="#FFFFFF"><img
                                                                src="images/del.png" width="15" height="15"
                                                                class="delImg"/>
                                                        </td>
                                                        <td bgcolor="#FFFFFF"><select name="cboSection" id="cboSection"
                                                                                      style="width:100px"
                                                                                      class=" validate[required] selcStyle">
                                                                <option value=""></option>
                                                                    <?php
                                                                    $sql = "SELECT DISTINCT
                                                                                    mst_section.intId,
                                                                                    mst_section.strName
                                                                            FROM
                                                                                    mst_section
                                                                            INNER JOIN mst_module ON mst_module.intSection = mst_section.intId
                                                                            WHERE
                                                                                    mst_section.intStatus NOT IN ('-1', 0)
                                                                            AND mst_module.intLocation = '$location' ";
                                                                    $result = $db->RunQuery($sql);
                                                                    while ($row = mysqli_fetch_array($result)) {
                                                                        if ($row['intId'] == $section)
                                                                            echo "<option selected=\"selected\" value=\"" . $row['intId'] . "\">" . $row['strName'] . "</option>";
                                                                        else
                                                                            echo "<option  value=\"" . $row['intId'] . "\">" . $row['strName'] . "</option>";
                                                                    }
                                                                    ?>
                                                            </select></td>
                                                        <td bgcolor="#FFFFFF"><select name="cboModule" id="cboModule"
                                                                                      style="width:100px"
                                                                                      class=" validate[required] selcStyle">
                                                                <option value=""></option>
                                                                    <?php
                                                                    $sql = "SELECT intId,strName,strCode FROM mst_module WHERE mst_module.intSection='$section' AND mst_module.intLocation='$location' AND mst_module.intStatus<>0 ";
                                                                   // echo $sql;
                                                                    $result = $db->RunQuery($sql);
                                                                    while ($row = mysqli_fetch_array($result)) {

                                                                        if ($row['intId'] == $row2['strModuleCode'])
                                                                            echo "<option selected=\"selected\" value=\"" . $row['intId'] . "\">" . $row['strName'] . "</option>";
                                                                        else
                                                                            echo "<option  value=\"" . $row['intId'] . "\">" . $row['strName'] . "</option>";
                                                                    }
                                                                    ?>
                                                            </select></td>
                                                        <td width="20%" bgcolor="#FFFFFF" id="cboDescCollumn">
                                                            <select class="js-example-data-ajax normalfnt" id="cboDesc"
                                                                    name="cboDesc">
                                                                <option value="" selected="selected">Please Clear
                                                                    Cache
                                                                </option>
                                                            </select>
                                                        </td>
                                                        <td bgcolor="#FFFFFF"><select name="cboCustomer"

                                                                                      id="cboCustomer"
                                                                                      style="width:150px"
                                                                                      class=" validate[required] selcStyle">
                                                                <option value=""></option>


                                                                    <?php
                                                                    $sql = "SELECT DISTINCT
                                                                        mst_customer.intId,
                                                                        mst_customer.strName
                                                                    FROM
                                                                        mst_customer
                                                                    WHERE
                                                                         mst_customer.intStatus=1 ORDER BY mst_customer.strName";
                                                                    $result = $db->RunQuery($sql);
                                                                    while($row = mysqli_fetch_array($result)) {
                                                                        $customer = $row['intId'];
                                                                        $strCustomer = $row['strName'];
                                                                        if ($customer == $row2['intCustomer'])
                                                                            echo "<option selected=\"selected\" value=\"$customer\">$strCustomer</option>";
                                                                        else
                                                                            echo "<option value=\"$customer\">$strCustomer</option>";
                                                                    }
                                                                    ?>
                                                            </select></td>

                                                        <td bgcolor="#FFFFFF"><select name="cboGraphicNo"
                                                                                      id="cboGraphicNo"
                                                                                      style="width:100px"
                                                                                      class=" validate[required] selcStyle graphicNo">
                                                                <option value=""></option>


                                                                    <?php
                                                                    $sql = "SELECT DISTINCT
                                                                        trn_orderdetails.strGraphicNo,
                                                                        trn_orderdetails.intOrderYear
                                                                    FROM
                                                                        trn_orderdetails
                                                                    INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo=trn_orderheader.intOrderNo AND
                                                                    trn_orderdetails.intOrderYear=trn_orderheader.intOrderYear
                                                                    WHERE
                                                                         trn_orderheader.intCustomer= ".$row2['intCustomer'] . 
                                                                    " ORDER BY trn_orderdetails.intOrderYear DESC ";
                                                                    $result = $db->RunQuery($sql);
                                                                    while ($row = mysqli_fetch_array($result)) {
                                                                        if ($row['strGraphicNo'] == $row2['strGraphicNo']) {
                                                                            echo "<option selected=\"selected\" value=\"" . $row['strGraphicNo'] . "\" >" . $row['strGraphicNo'] . "</option>";
                                                                        } else {
                                                                            echo "<option value=\"" . $row['strGraphicNo'] . "\">" . $row['strGraphicNo'] . "</option>";
                                                                        }
                                                                    }
                                                                    ?>
                                                            </select></td>

                                                        <td bgcolor="#FFFFFF"><select name="cboOrderNo" id="cboOrderNo"
                                                                                      style="width:100px"
                                                                                      class="selcStyle validate[required]">
                                                                <option value=""></option>
                                                                <?php
                                                                 $sql = "SELECT DISTINCT
                                                                        CONCAT(
                                                                                trn_orderdetails.intOrderNo,
                                                                                '/',
                                                                                trn_orderdetails.intOrderYear
                                                                        ) AS intOrderNo,
                                                                        trn_orderheader.intStatus,
                                                                        trn_orderheader.PO_TYPE
                                                                FROM
                                                                        trn_orderdetails
                                                                INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo
                                                                WHERE
                                                                        trn_orderdetails.strGraphicNo = '".$row2['strGraphicNo']."'
                                                                AND trn_orderheader.PO_TYPE != 1
                                                                AND trn_orderheader.intStatus NOT IN (- 10 ,- 2 ,- 1, 4, 0)";
                                                                 //echo $sql;
//                                                                $sql = "SELECT DISTINCT
//                                                                        CONCAT(
//                                                                                trn_orderdetails.intOrderNo,
//                                                                                '/',
//                                                                        trn_orderdetails.intOrderYear 
//                                                                       ) AS intOrderNo
//                                                               FROM
//                                                                        trn_orderdetails 
//                                                               INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo 
//                                                               WHERE
//                                                                        trn_orderdetails.strGraphicNo ='".$row2['strGraphicNo']."'";
                                                          //  echo $sql;
                                                                $result = $db->RunQuery($sql);
                                                                while ($row = mysqli_fetch_array($result)) {

                                                                    $orderString = $row['intOrderNo'];
                                                                    $splitOrder = explode('/', $orderString);
                                                                    $x_orderNo = $splitOrder[0];
                                                                    if ($x_orderNo == $row2['intOrderNo']) {
                                                                        // echo "<option selected=\"selected\" value=\"" . $x_orderNo . "\" >" . $orderString . "</option>";
                                                                        echo "<option value=\"" . $orderString . "\" selected=\"selected\">" . $orderString . "</option>";
                                                                    } else {
                                                                        echo "<option value=\"" . $orderString . "\">" . $orderString . "</option>";
                                                                    }
                                                                }
                                                                ?>
                                                            </select></td>


                                                        <td bgcolor="#FFFFFF"><select name="cboSalesOrderNo"
                                                                                      id="cboSalesOrderNo"
                                                                                      style="width:100px"
                                                                                      class="selcStyle validate[required]">
                                                                <option value=""></option>


                                                                <?php
                                                                $sql = "SELECT DISTINCT                                                                     
                                                                        trn_orderdetails.strSalesOrderNo
                                                                     FROM
                                                                        trn_orderdetails
                                                                     WHERE
                                                                        trn_orderdetails.strGraphicNo = '".$row2['strGraphicNo']."'".
                                                                    "AND trn_orderdetails.intOrderNo ='".$row2['intOrderNo']."'";


                                                                $result = $db->RunQuery($sql);
                                                                while ($row = mysqli_fetch_array($result)) {
                                                                    $x_salesOrderId = $row['intSalesOrderId'];
                                                                    $x_salesOrderNo = $row['strSalesOrderNo'];
                                                                    if ($x_salesOrderNo == $row2['strSalesOrderNo']) {
                                                                        echo "<option selected=\"selected\" value=\"" . $x_salesOrderNo . "\" >" . $x_salesOrderNo . "</option>";
                                                                    } else {
                                                                        echo "<option value=\"" . $x_salesOrderNo . "\">" . $x_salesOrderNo . "</option>";
                                                                    }
                                                                }
                                                                ?>
                                                            </select></td>

                                                        <td bgcolor="#FFFFFF"><select name="cboStyleNo" id="cboStyleNo"
                                                                                      style="width:100px"
                                                                                      class="selcStyle">
                                                                <option value=""></option>

                                                                <?php
                                                                $sql = "SELECT DISTINCT 
                                                                        trn_orderdetails.strStyleNo
                                                                    FROM
                                                                        trn_orderdetails
                                                                    INNER JOIN trn_sampleinfomations ON trn_orderdetails.intSampleNo = trn_sampleinfomations.intSampleNo
                                                                    AND trn_orderdetails.intSampleYear = trn_sampleinfomations.intSampleYear
                                                                    AND trn_orderdetails.strGraphicNo = trn_sampleinfomations.strGraphicRefNo
                                                                    WHERE
                                                                        trn_orderdetails.strGraphicNo = '".$row2['strGraphicNo']."'
                                                                    AND trn_orderdetails.intOrderNo = '".$row2['intOrderNo']."'
                                                                    AND trn_orderdetails.strSalesOrderNo='".$row2['strSalesOrderNo']."'";
                                                                echo $sql;

                                                                $result = $db->RunQuery($sql);
                                                                while ($row = mysqli_fetch_array($result)) {
                                                                    if ($row['strStyleNo'] == $row2['strStyleNo'])
                                                                        echo "<option selected=\"selected\" value=\"" . $row['strStyleNo'] . "\">" . $row['strStyleNo'] . "</option>";
                                                                    else
                                                                        echo "<option value=\"" . $row['strStyleNo'] . "\">" . $row['strStyleNo'] . "</option>";
                                                                }
                                                                ?>
                                                            </select></td>
                                                        <td bgcolor="#FFFFFF"><select name="cboGroundColor"
                                                                                      id="cboGroundColor"
                                                                                      style="width:100px"
                                                                                      class="selcStyle validate[required]">
                                                                <option value=""></option>

                                                                <?php
                                                                $sql = "SELECT DISTINCT
                                                                                trn_sampleinfomations_details.intGroundColor,
                                                                                mst_colors_ground.strName
                                                                        FROM
                                                                                trn_sampleinfomations_details
                                                                        INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_details.intSampleNo
                                                                        AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
                                                                        INNER JOIN trn_orderdetails ON trn_orderdetails.intSampleNo = trn_sampleinfomations.intSampleNo
                                                                        AND trn_orderdetails.intSampleYear = trn_sampleinfomations.intSampleYear
                                                                        AND trn_orderdetails.strGraphicNo = trn_sampleinfomations.strGraphicRefNo
                                                                        INNER JOIN mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
                                                                        AND trn_sampleinfomations.intRevisionNo = trn_sampleinfomations_details.intRevNo
                                                                        AND trn_sampleinfomations.intRevisionNo = trn_orderdetails.intRevisionNo
                                                                        WHERE
                                                                                trn_orderdetails.strGraphicNo = '".$row2['strGraphicNo']."'".
                                                                        "AND trn_orderdetails.intOrderNo = '".$row2['intOrderNo']."'".
                                                                        "AND trn_orderdetails.strSalesOrderNo = '".$row2['strSalesOrderNo']."'";

                                                                $result = $db->RunQuery($sql);
                                                                echo $sql;
                                                                while ($row = mysqli_fetch_array($result)) {

                                                                    if ($row['intGroundColor'] == $row2['intGroundColor'])
                                                                        echo "<option selected=\"selected\" value=\"" . $row['intGroundColor'] . "\">" . $row['strName'] . "</option>";
                                                                    else
                                                                        echo "<option value=\"" . $row['intGroundColor'] . "\">" . $row['strName'] . "</option>";
                                                                }
                                                                ?>
                                                            </select></td>
                                                        <td bgcolor="#FFFFFF"><select name="cboPart" id="cboPart"
                                                                                      style="width:100px"
                                                                                      class="selcStyle validate[required]">
                                                                <option value=""></option>
                                                                <?php
                                                                $sql = "SELECT DISTINCT
                                                                                trn_orderdetails.strPrintName
                                                                        FROM
                                                                                trn_orderdetails
                                                                        INNER JOIN trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_orderdetails.intSampleNo
                                                                        AND trn_sampleinfomations_details.intSampleYear = trn_orderdetails.intSampleYear
                                                                        WHERE
                                                                                trn_orderdetails.strGraphicNo = '".$row2['strGraphicNo']."'
                                                                        AND trn_orderdetails.intOrderNo = '".$row2['intOrderNo']."'
                                                                        AND trn_orderdetails.strSalesOrderNo = '".$row2['strSalesOrderNo']."'";
                                                                //echo $sql;
                                                                $result = $db->RunQuery($sql);
                                                                while ($row = mysqli_fetch_array($result)) {
                                                                    $x_part = $row['intPart'];
                                                                    $strPart = $row['strPrintName'];
                                                                    if ($row['strPrintName'] == $row2['strPrintName']) {
                                                                        echo "<option selected=\"selected\" value=\"" . $row['strPrintName'] . "\" >" . $row['strPrintName'] . "</option>";
                                                                    } else {
                                                                        echo "<option value=\"" . $row['strPrintName'] . "\">" . $row['strPrintName'] . "</option>";
                                                                    }
                                                                }
                                                                ?>
                                                            </select></td>
                                                        <td bgcolor="#FFFFFF"><select name="cboCombo" id="cboCombo"
                                                                                      style="width:100px"
                                                                                      class="selcStyle validate[required]">
                                                                <option value=""></option>
                                                                <?php
                                                                $d = date('Y');
                                                                $sql = "SELECT DISTINCT
                                                                                trn_orderdetails.strCombo
                                                                        FROM
                                                                                trn_orderdetails
                                                                        WHERE
                                                                                trn_orderdetails.strGraphicNo = '".$row2['strGraphicNo']."'
                                                                        AND trn_orderdetails.intOrderNo = '".$row2['intOrderNo']."'
                                                                         AND trn_orderdetails.strSalesOrderNo = '".$row2['strSalesOrderNo']."' AND trn_orderdetails.strPrintName='".$row2['strPrintName']."'";
                                                                echo $sql;
                                                                //check printname

                                                                $result = $db->RunQuery($sql);
                                                                $detailArrayIndex = 0;
                                                                while ($row = mysqli_fetch_array($result)) {
                                                                    $showExcelReportStatus = 1;
                                                                    $strCombo = $row['strCombo'];
                                                                    if ($row['strCombo'] == $row2['strCombo'])
                                                                        echo "<option selected=\"selected\" value=\"" . $strCombo . "\">" . $strCombo . "</option>";
                                                                    else
                                                                        echo "<option value=\"" . $strCombo . "\">" . $strCombo . "</option>";
                                                                }
                                                                ?>
                                                            </select></td>

                                                        <td bgcolor="#FFFFFF" id="cboPrintColorCloumn">
                                                            <select class="chosen-select clsorder " multiple="multiple"
                                                                    data-placeholder="Select Your Option"
                                                                    style="width:134px; height:50px"
                                                                    id="cboPrintColor">

                                                                <?php
                                                                $rev= getMaxRev($row2['intOrderNo'], $row2['strGraphicNo'], $row2['strSalesOrderNo'], $row2['strPrintName'], $row2['strCombo']);
                                                                $html = "<option value=\"\">ALL</option>";
                                                                
                                                                $sql = "SELECT DISTINCT concat(trn_sampleinfomations_details.intColorId,'/',trn_sampleinfomations_details_technical.intInkTypeId) as intId,
                                                                            mst_colors.strName AS color,
                                                                            mst_inktypes.strName AS inktype
                                                                        FROM
                                                                            trn_sampleinfomations_details
                                                                        INNER JOIN trn_orderdetails ON trn_sampleinfomations_details.intSampleNo = trn_orderdetails.intSampleNo
                                                                        AND trn_sampleinfomations_details.intSampleYear = trn_orderdetails.intSampleYear
                                                                        INNER JOIN trn_sampleinfomations_details_technical ON trn_sampleinfomations_details_technical.intSampleNo = trn_sampleinfomations_details.intSampleNo
                                                                        AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations_details_technical.intSampleYear
                                                                        AND trn_sampleinfomations_details.intColorId = trn_sampleinfomations_details_technical.intColorId
                                                                        INNER JOIN mst_colors ON trn_sampleinfomations_details.intColorId = mst_colors.intId
                                                                        INNER JOIN mst_inktypes ON trn_sampleinfomations_details_technical.intInkTypeId = mst_inktypes.intId
                                                                        AND trn_sampleinfomations_details_technical.strComboName = trn_sampleinfomations_details.strComboName
                                                                        AND trn_sampleinfomations_details_technical.strPrintName = trn_sampleinfomations_details.strPrintName
                                                                        AND trn_sampleinfomations_details.intRevNo = trn_orderdetails.intRevisionNo
                                                                        AND trn_sampleinfomations_details.strComboName = trn_orderdetails.strCombo
                                                                        AND trn_sampleinfomations_details.strPrintName = trn_orderdetails.strPrintName
                                                                        AND trn_sampleinfomations_details_technical.intRevNo = trn_sampleinfomations_details.intRevNo
                                                                        WHERE
                                                                            trn_orderdetails.intOrderNo = '".$row2['intOrderNo']."'
                                                                        AND trn_orderdetails.strGraphicNo = '".$row2['strGraphicNo']."'
                                                                        AND trn_orderdetails.strSalesOrderNo = '".$row2['strSalesOrderNo']."'
                                                                        AND trn_orderdetails.strPrintName = '".$row2['strPrintName']."'
                                                                        AND trn_orderdetails.strCombo = '".$row2['strCombo']."'
                                                                        AND trn_orderdetails.intRevisionNo = '$rev'";
                                                                //echo $sql;


                                                                // $html = "<option value=\"\"></option>";
                                                                $result = $db->RunQuery($sql);
                                                                $rowcount=mysqli_num_rows($result);
                                                                $selectedId = $row2['intPrintColor'].'/'.$row2['intInkTypeId'];

                                                                while ($row = mysqli_fetch_array($result)) {
                                                                    if ($selectedId == $row['intId']){
                                                                        $html .= "<option selected=\"selected\" value=\"" . $row['intId']. "\">" . $row['color'] .' - '.$row['inktype']. "</option>";
                                                                    } else if($selectedId==-1){
                                                                        $html = "<option value=\"-1/0\" selected=\"selected\">ALL</option>";
                                                                    }
                                                                    else {
                                                                        $html .= "<option value=\"" . $row['intId']. "\">" . $row['color'] .' - '.$row['inktype']. "</option>";
                                                                    }
                                                                }

                                                                echo $html;
                                                                ?>
                                                            </select>


                                                        </td>

                                                        <td bgcolor="#FFFFFF"><input name="plannedShots" type="text"
                                                                                     id="plannedShots"
                                                                                     style="width:60px; text-align:right"

                                                                                     value="<?php echo $row2['plannedShots'] ; ?>"
                                                                                     class="validate[min[0],custom[integer]],validate[required] " readonly/>

                                                        </td>
                                                        <td bgcolor="#FFFFFF"><input name="plannedUps" type="text"
                                                                                     id="plannedUps"
                                                                                     style="width:60px; text-align:right"

                                                                                     value="<?php echo $row2['plannedUps'];  ?>"
                                                                                     class="validate[min[0],custom[integer]],validate[required] "readonly/>

                                                        </td>

                                                        <td bgcolor="#FFFFFF"><input name="txtNoOfShots" type="text"
                                                                                     id="txtNoOfShots"
                                                                                     style="width:60px; text-align:right"

                                                                                     value="<?php echo $row2['intShots']; ?>"
                                                                                     class="validate[min[0],custom[integer]],validate[required] "/>

                                                        </td>

                                                        <td bgcolor="#FFFFFF"><input name="txtNoUps" type="text"
                                                                                     id="txtNoUps"
                                                                                     style="width:60px; text-align:right"
                                                                                     value="<?php echo $row2['noOfUps']; ?>"
                                                                                     class="validate[min[0],custom[integer]],validate[required]"/>

                                                        </td>



                                                        <td bgcolor="#FFFFFF"><input name="txtFabricInQty" type="text"
                                                                                     id="txtFabricInQty"
                                                                                     style="width:60px; text-align:right"
                                                                                     value="<?php echo 0; ?>"
                                                                                     class="validate[min[0],custom[integer]],validate[required] clsCalculateQty"
                                                                                     readonly/></td>

                                                        <td bgcolor="#FFFFFF"><input name="txtDispatchQty" type="text"
                                                                                     id="txtDispatchQty"
                                                                                     style="width:60px; text-align:right"
                                                                                     value="<?php echo $row2['intTotalDispatchedQty']  ; ?>"
                                                                                     class="validate[min[0],custom[integer],validate[required] clsCalculateQty"
                                                                                     readonly/>
                                                        </td>
                                                        <td bgcolor="#FFFFFF"><input name="txtProductionInQty"
                                                                                     type="text" id="txtProductionInQty"
                                                                                     style="width:60px; text-align:right"
                                                                                     value="<?php echo $row2['intTotalProductionMarkedQty']  ; ?>"
                                                                                     class="validate[min[0],custom[integer]] clsCalculateQty"
                                                                                     readonly/>
                                                        </td>
                                                        <td bgcolor="#FFFFFF"><input name="txtBalQty" type="text"
                                                                                     id="txtBalQty"
                                                                                     style="width:60px; text-align:right"
                                                                                     value="<?php echo ($row2['intTotalFabricInQty'] - $row2['intTotalProductionMarkedQty']); ?>"
                                                                                     class="validate[min[0],custom[integer]],validate[required] clsCalculateQty"
                                                                                     readonly/>
                                                        </td>
                                                        <td bgcolor="#FFFFFF"><input name="txtActualProductionQty"
                                                                                     type="text"
                                                                                     id="txtActualProductionQty"
                                                                                     style="width:60px; text-align:right"
                                                                                     value="<?php echo $row2['intTodayProductionQty']; ?>"
                                                                                     class="validate[min[0],custom[integer]],validate[required] clsCalculateQty"/>
                                                        </td>
                                                        <td bgcolor="#FFFFFF">
                                                            <input type="checkbox" <?php if ($row2['intchkProduction'] == 1){ echo "checked";} ?> name="chkProduction" class="chek"
                                                                   id="chkProduction"
                                                                   value="1">
                                                        </td>

                                                        <td bgcolor="#FFFFFF">
                                                            <a id="butView" class="button green small"
                                                               title="Click here to view history"
                                                               name="butView">View</a>
                                                        </td>

                                            </tr>
                                            <?php 
                                                }
                                            } else {?>
                                                  <tr class="normalfnt" id="dataRow">
                                                        <td height="49" align="center" bgcolor="#FFFFFF"><img
                                                                src="images/del.png" width="15" height="15"
                                                                class="delImg"/>
                                                        </td>
                                                        <td bgcolor="#FFFFFF"><select name="cboSection" id="cboSection"
                                                                                      style="width:100px"
                                                                                      class=" validate[required] selcStyle">
                                                                <option value=""></option>
                                                                    <?php
                                                                    $sql = "SELECT DISTINCT
                                                                                    mst_section.intId,
                                                                                    mst_section.strName
                                                                            FROM
                                                                                    mst_section
                                                                            INNER JOIN mst_module ON mst_module.intSection = mst_section.intId
                                                                            WHERE
                                                                                    mst_section.intStatus NOT IN ('-1', 0)
                                                                            AND mst_module.intLocation = '$location' ";
                                                                    $result = $db->RunQuery($sql);
                                                                    while ($row = mysqli_fetch_array($result)) {
                                                                        if ($row['intId'] == $row2['intSection'])
                                                                            echo "<option selected=\"selected\" value=\"" . $row['intId'] . "\">" . $row['strName'] . "</option>";
                                                                        else
                                                                            echo "<option  value=\"" . $row['intId'] . "\">" . $row['strName'] . "</option>";
                                                                    }
                                                                    ?>
                                                            </select></td>
                                                        <td bgcolor="#FFFFFF"><select name="cboModule" id="cboModule"
                                                                                      style="width:100px"
                                                                                      class=" validate[required] selcStyle">
                                                                <option value=""></option>
                                                                    <?php
                                                                    $sql = "SELECT intId,strName,strCode FROM mst_module WHERE mst_module.intSection='$section' AND mst_module.intLocation='$location' AND mst_module.intStatus<>0 ";
                                                                    echo $sql;
                                                                    $result = $db->RunQuery($sql);
                                                                    while ($row = mysqli_fetch_array($result)) {

                                                                        if ($row['intId'] = $row2['strModuleCode'])
                                                                            echo "<option selected=\"selected\" value=\"" . $row['intId'] . "\">" . $row['strName'] . "</option>";
                                                                        else
                                                                            echo "<option  value=\"" . $row['intId'] . "\">" . $row['strName'] . "</option>";
                                                                    }
                                                                    ?>
                                                            </select></td>
                                                        <td width="20%" bgcolor="#FFFFFF" id="cboDescCollumn">
                                                            <select class="js-example-data-ajax normalfnt" id="cboDesc"
                                                                    name="cboDesc">
                                                                <option value="" selected="selected">Please Clear
                                                                    Cache
                                                                </option>
                                                            </select>
                                                        </td>
                                                        <td bgcolor="#FFFFFF"><select name="cboCustomer"

                                                                                      id="cboCustomer"
                                                                                      style="width:150px"
                                                                                      class=" validate[required] selcStyle">
                                                                <option value=""></option>


                                                                    <?php
                                                                    $sql = "SELECT DISTINCT
                                                                        mst_customer.intId,
                                                                        mst_customer.strName
                                                                    FROM
                                                                        mst_customer
                                                                    WHERE
                                                                         mst_customer.intStatus=1 ORDER BY mst_customer.strName";
                                                                    $result = $db->RunQuery($sql);
                                                                    while($row = mysqli_fetch_array($result)) {
                                                                        $customer = $row['intId'];
                                                                        $strCustomer = $row['strName'];
                                                                        if ($customer == $row2['intCustomer'])
                                                                            echo "<option selected=\"selected\" value=\"$customer\">$strCustomer</option>";
                                                                        else
                                                                            echo "<option value=\"$customer\">$strCustomer</option>";
                                                                    }
                                                                    ?>
                                                            </select></td>

                                                        <td bgcolor="#FFFFFF"><select name="cboGraphicNo"
                                                                                      id="cboGraphicNo"
                                                                                      style="width:100px"
                                                                                      class=" validate[required] selcStyle graphicNo">
                                                                <option value=""></option>


                                                                    <?php
                                                                    $sql = "SELECT DISTINCT
                                                                        trn_orderdetails.strGraphicNo,
                                                                        trn_orderdetails.intOrderYear
                                                                    FROM
                                                                        trn_orderdetails
                                                                    INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo=trn_orderheader.intOrderNo AND
                                                                    trn_orderdetails.intOrderYear=trn_orderheader.intOrderYear
                                                                    WHERE
                                                                         trn_orderheader.intCustomer= '$intCustomer'  
                                                                    ORDER BY trn_orderdetails.intOrderYear DESC ";

                                                                    $result = $db->RunQuery($sql);
                                                                    while ($row = mysqli_fetch_array($result)) {
                                                                        if ($row['strGraphicNo'] == $row2['strGraphicNo']) {
                                                                            echo "<option selected=\"selected\" value=\"" . $row['strGraphicNo'] . "\" >" . $row['strGraphicNo'] . "</option>";
                                                                        } else {
                                                                            echo "<option value=\"" . $row['strGraphicNo'] . "\">" . $row['strGraphicNo'] . "</option>";
                                                                        }
                                                                    }
                                                                    ?>
                                                            </select></td>

                                                        <td bgcolor="#FFFFFF"><select name="cboOrderNo" id="cboOrderNo"
                                                                                      style="width:100px"
                                                                                      class="selcStyle validate[required]">
                                                                <option value=""></option>
                                                                <?php
                                                            $sql = "SELECT DISTINCT
                                                                        CONCAT(
                                                                                trn_orderdetails.intOrderNo,
                                                                                '/',
                                                                                trn_orderdetails.intOrderYear
                                                                        ) AS intOrderNo,
                                                                        trn_orderheader.intStatus,
                                                                        trn_orderheader.PO_TYPE
                                                                FROM
                                                                        trn_orderdetails
                                                                INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo
                                                                WHERE
                                                                        trn_orderdetails.strGraphicNo = '$graphicNo'
                                                                AND trn_orderheader.PO_TYPE != 1
                                                                AND trn_orderheader.intStatus NOT IN (- 10 ,- 2 ,- 1, 4, 0)";
                                                           // echo $sql;

//                                                                $sql = "SELECT DISTINCT
//                                                                        CONCAT(
//                                                                                trn_orderdetails.intOrderNo,
//                                                                                '/',
//                                                                        trn_orderdetails.intOrderYear 
//                                                                       ) AS intOrderNo
//                                                               FROM
//                                                                        trn_orderdetails 
//                                                               INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo 
//                                                               WHERE
//                                                                        trn_orderdetails.strGraphicNo = '$graphicNo' ";
//                                                            echo $sql;
                                                                $result = $db->RunQuery($sql);
                                                                while ($row = mysqli_fetch_array($result)) {

                                                                    $orderString = $row['intOrderNo'];
                                                                    $splitOrder = explode('/', $orderString);
                                                                    $x_orderNo = $splitOrder[0];
                                                                    if ($x_orderNo == $row2['intOrderNo']) {
                                                                        // echo "<option selected=\"selected\" value=\"" . $x_orderNo . "\" >" . $orderString . "</option>";
                                                                        echo "<option value=\"" . $x_orderNo . "\" selected=\"selected\">" . $orderString . "</option>";
                                                                    } else {
                                                                        echo "<option value=\"" . $x_orderNo . "\">" . $orderString . "</option>";
                                                                    }
                                                                }
                                                                ?>
                                                            </select></td>


                                                        <td bgcolor="#FFFFFF"><select name="cboSalesOrderNo"
                                                                                      id="cboSalesOrderNo"
                                                                                      style="width:100px"
                                                                                      class="selcStyle validate[required]">
                                                                <option value=""></option>


                                                                    <?php
                                                                    $sql = "SELECT DISTINCT                                                                     
                                                                                                                                        trn_orderdetails.strSalesOrderNo
                                                                                                                                     FROM
                                                                                                                                        trn_orderdetails
                                                                                                                                     WHERE
                                                                                                                                        trn_orderdetails.strGraphicNo = '$graphicNo'
                                                                                                                                    AND trn_orderdetails.intOrderNo = '$orderNo'";

                                                                    $result = $db->RunQuery($sql);
                                                                    while ($row = mysqli_fetch_array($result)) {
                                                                        $x_salesOrderId = $row['intSalesOrderId'];
                                                                        $x_salesOrderNo = $row['strSalesOrderNo'];
                                                                        if ($x_salesOrderNo == $row2['strSalesOrderNo']) {
                                                                            echo "<option selected=\"selected\" value=\"" . $x_salesOrderNo . "\" >" . $x_salesOrderNo . "</option>";
                                                                        } else {
                                                                            echo "<option value=\"" . $x_salesOrderNo . "\">" . $x_salesOrderNo . "</option>";
                                                                        }
                                                                    }
                                                                    ?>
                                                            </select></td>

                                                        <td bgcolor="#FFFFFF"><select name="cboStyleNo" id="cboStyleNo"
                                                                                      style="width:100px"
                                                                                      class="selcStyle">
                                                                <option value=""></option>

                                                                <?php
                                                                $sql = "SELECT DISTINCT 
                                                                        trn_orderdetails.strStyleNo
                                                                    FROM
                                                                        trn_orderdetails
                                                                    INNER JOIN trn_sampleinfomations ON trn_orderdetails.intSampleNo = trn_sampleinfomations.intSampleNo
                                                                    AND trn_orderdetails.intSampleYear = trn_sampleinfomations.intSampleYear
                                                                    AND trn_orderdetails.strGraphicNo = trn_sampleinfomations.strGraphicRefNo
                                                                    WHERE
                                                                        trn_orderdetails.strGraphicNo = '$graphicNo'
                                                                    AND trn_orderdetails.intOrderNo = '$orderNo'
                                                                    AND trn_orderdetails.strSalesOrderNo='$salesOrderNo'";
                                                                echo $sql;

                                                                $result = $db->RunQuery($sql);
                                                                while ($row = mysqli_fetch_array($result)) {
                                                                    if ($row['strStyleNo'] == $row2['strStyleNo'])
                                                                        echo "<option selected=\"selected\" value=\"" . $row['strStyleNo'] . "\">" . $row['strStyleNo'] . "</option>";
                                                                    else
                                                                        echo "<option value=\"" . $row['strStyleNo'] . "\">" . $row['strStyleNo'] . "</option>";
                                                                }
                                                                ?>
                                                            </select></td>
                                                        <td bgcolor="#FFFFFF"><select name="cboGroundColor"
                                                                                      id="cboGroundColor"
                                                                                      style="width:100px"
                                                                                      class="selcStyle validate[required]">
                                                                <option value=""></option>

                                                                <?php
                                                                $sql = "SELECT DISTINCT
                                                                                trn_sampleinfomations_details.intGroundColor,
                                                                                mst_colors_ground.strName
                                                                        FROM
                                                                                trn_sampleinfomations_details
                                                                        INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_details.intSampleNo
                                                                        AND trn_sampleinfomations_details.intSampleYear = trn_sampleinfomations.intSampleYear
                                                                        INNER JOIN trn_orderdetails ON trn_orderdetails.intSampleNo = trn_sampleinfomations.intSampleNo
                                                                        AND trn_orderdetails.intSampleYear = trn_sampleinfomations.intSampleYear
                                                                        AND trn_orderdetails.strGraphicNo = trn_sampleinfomations.strGraphicRefNo
                                                                        INNER JOIN mst_colors_ground ON trn_sampleinfomations_details.intGroundColor = mst_colors_ground.intId
                                                                        AND trn_sampleinfomations.intRevisionNo = trn_sampleinfomations_details.intRevNo
                                                                        AND trn_sampleinfomations.intRevisionNo = trn_orderdetails.intRevisionNo
                                                                        WHERE
                                                                                trn_orderdetails.strGraphicNo = '$graphicNo'
                                                                        AND trn_orderdetails.intOrderNo = '$orderNo'
                                                                        AND trn_orderdetails.strSalesOrderNo = '$salesOrderNo'
                                                                    ";


                                                                $result = $db->RunQuery($sql);
                                                                while ($row = mysqli_fetch_array($result)) {
                                                                    if ($row['intGroundColor'] == $row2['intGroundColor'])
                                                                        echo "<option selected=\"selected\" value=\"" . $row['intGroundColor'] . "\">" . $row['strName'] . "</option>";
                                                                    else
                                                                        echo "<option value=\"" . $row['intGroundColor'] . "\">" . $row['strName'] . "</option>";
                                                                }
                                                                ?>
                                                            </select></td>
                                                        <td bgcolor="#FFFFFF"><select name="cboPart" id="cboPart"
                                                                                      style="width:100px"
                                                                                      class="selcStyle validate[required]">
                                                                <option value=""></option>
                                                                <?php
                                                                $sql = "SELECT DISTINCT
                                                                                trn_orderdetails.strPrintName
                                                                        FROM
                                                                                trn_orderdetails
                                                                        INNER JOIN trn_sampleinfomations_details ON trn_sampleinfomations_details.intSampleNo = trn_orderdetails.intSampleNo
                                                                        AND trn_sampleinfomations_details.intSampleYear = trn_orderdetails.intSampleYear
                                                                        WHERE
                                                                                trn_orderdetails.strGraphicNo = '$graphicNo'
                                                                        AND trn_orderdetails.intOrderNo = '$orderNo'
                                                                        AND trn_orderdetails.strSalesOrderNo = '$salesOrderNo'";
                                                                //echo $sql;
                                                                $result = $db->RunQuery($sql);
                                                                while ($row = mysqli_fetch_array($result)) {
                                                                    $x_part = $row['intPart'];
                                                                    $strPart = $row['strPrintName'];
                                                                    if ($row['strPrintName'] == $row2['strPrintName']) {
                                                                        echo "<option selected=\"selected\" value=\"" . $strPart . "\" >" . $strPart . "</option>";
                                                                    } else {
                                                                        echo "<option value=\"" . $strPart . "\">" . $strPart . "</option>";
                                                                    }
                                                                }
                                                                ?>
                                                            </select></td>
                                                        <td bgcolor="#FFFFFF"><select name="cboCombo" id="cboCombo"
                                                                                      style="width:100px"
                                                                                      class="selcStyle validate[required]">
                                                                <option value=""></option>
                                                                <?php
                                                                $d = date('Y');
                                                                $sql = "SELECT DISTINCT
                                                                                trn_orderdetails.strCombo
                                                                        FROM
                                                                                trn_orderdetails
                                                                        WHERE
                                                                                trn_orderdetails.strGraphicNo = '$graphicNo'
                                                                        AND trn_orderdetails.intOrderNo = '$orderNo'
                                                                         AND trn_orderdetails.strSalesOrderNo = '$salesOrderNo' AND trn_orderdetails.strPrintName='$part'";
                                                                echo $sql;
                                                                //check printname

                                                                $result = $db->RunQuery($sql);
                                                                $detailArrayIndex = 0;
                                                                while ($row = mysqli_fetch_array($result)) {
                                                                    $showExcelReportStatus = 1;
                                                                    $strCombo = $row['strCombo'];
                                                                    if ($row['strCombo'] == $row2['strCombo'])
                                                                        echo "<option selected=\"selected\" value=\"" . $strCombo . "\">" . $strCombo . "</option>";
                                                                    else
                                                                        echo "<option value=\"" . $strCombo . "\">" . $strCombo . "</option>";
                                                                }
                                                                ?>
                                                            </select></td>

                                                        <td bgcolor="#FFFFFF" id="cboPrintColorCloumn">
                                                            <select class="chosen-select clsorder " multiple="multiple"
                                                                    data-placeholder="Select Your Option"
                                                                    style="width:134px; height:50px"
                                                                    id="cboPrintColor">

                                                                <?php
                                                                $db->connect();
                                                                $db->begin();

                                                                $html = "<option value=\"\"></option>";
                                                                echo $html;
                                                                ?>
                                                            </select>


                                                        </td>                                                                                                      

                                                        <td bgcolor="#FFFFFF"><input name="plannedShots" type="text"
                                                                                     id="plannedShots"
                                                                                     style="width:60px; text-align:right"

                                                                                     value="<?php echo $row2['plannedShots'];?>"
                                                                                     class="validate[min[0],custom[integer]],validate[required] " readonly/>

                                                        </td>
                                                        <td bgcolor="#FFFFFF"><input name="plannedUps" type="text"
                                                                                     id="plannedUps"
                                                                                     style="width:60px; text-align:right"

                                                                                     value="<?php echo $row2['plannedUps'];?>"
                                                                                     class="validate[min[0],custom[integer]],validate[required] "readonly/>

                                                        </td>

                                                        <td bgcolor="#FFFFFF"><input name="txtNoOfShots" type="text"
                                                                                     id="txtNoOfShots"
                                                                                     style="width:60px; text-align:right"

                                                                                     value="<?php echo $row2['intShots']; ?>"
                                                                                     class="validate[min[0],custom[integer]],validate[required] "/>

                                                        </td>

                                                        <td bgcolor="#FFFFFF"><input name="txtNoUps" type="text"
                                                                                     id="txtNoUps"
                                                                                     style="width:60px; text-align:right"
                                                                                     value="<?php echo $row2['noOfUps']; ?>"
                                                                                     class="validate[min[0],custom[integer]],validate[required]"/>

                                                        </td>



                                                        <td bgcolor="#FFFFFF"><input name="txtFabricInQty" type="text"
                                                                                     id="txtFabricInQty"
                                                                                     style="width:60px; text-align:right"
                                                                                     value="<?php echo $row2['intTotalFabricInQty']; ?>"
                                                                                     class="validate[min[0],custom[integer]],validate[required] clsCalculateQty"
                                                                                     readonly/></td>

                                                        <td bgcolor="#FFFFFF"><input name="txtDispatchQty" type="text"
                                                                                     id="txtDispatchQty"
                                                                                     style="width:60px; text-align:right"
                                                                                     value="<?php echo $row2['intTotalDispatchedQty'];?>"
                                                                                     class="validate[min[0],custom[integer],validate[required] clsCalculateQty"
                                                                                     readonly/>
                                                        </td>
                                                        <td bgcolor="#FFFFFF"><input name="txtProductionInQty"
                                                                                     type="text" id="txtProductionInQty"
                                                                                     style="width:60px; text-align:right"
                                                                                     value="<?php echo $row2['intTotalProductionMarkedQty'];?>"
                                                                                     class="validate[min[0],custom[integer]] clsCalculateQty"
                                                                                     readonly/> 
                                                        </td>
                                                        <td bgcolor="#FFFFFF"><input name="txtBalQty" type="text"
                                                                                     id="txtBalQty"
                                                                                     style="width:60px; text-align:right"
                                                                                     value="<?php echo ($row2['intTotalFabricInQty']-$row2['intTotalProductionMarkedQty']);?>"
                                                                                     class="validate[min[0],custom[integer]],validate[required] clsCalculateQty"
                                                                                     readonly/>
                                                        </td>
                                                        <td bgcolor="#FFFFFF"><input name="txtActualProductionQty"
                                                                                     type="text"
                                                                                     id="txtActualProductionQty"
                                                                                     style="width:60px; text-align:right"
                                                                                     value="<?php echo $row2['intTodayProductionQty']; ?>"
                                                                                     class="validate[min[0],custom[integer]],validate[required] clsCalculateQty"/>
                                                        </td>
                                                        <td bgcolor="#FFFFFF">
                                                            <input type="checkbox" name="chkProduction" class="chek"
                                                                   id="chkProduction"
                                                                   value="1" <?php if($row2['intchkProduction']=='1'){?> checked="true" <?php }?>>
                                                        </td>

                                                        <td bgcolor="#FFFFFF">
                                                            <a id="butView" class="button green small"
                                                               title="Click here to view history"
                                                               name="butView">View</a>
                                                        </td>

                                            </tr>
                                            <?php
                                            }
                                            ?>
                                            




                                            <div id="newRow"></div>
                                            <tr class="dataRow">
                                                <td colspan="21" align="left" bgcolor="#FFFFFF"
                                                    class="cls_insertRow">
                                                    <a id="butInsertRow"
                                                       class="button green small"
                                                       title="Click here to add more rows"
                                                       name="butInsertRow">+</a>
                                                </td>
                                            </tr>

                                        </table>



                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td align="center" class="tableBorder_allRound">
                                    <a id="butNew" class="button green medium"
                                       title="Click here for new bulk order" name="butNew">New</a>
                                    <a id="butSave" class="button green medium"
                                       title="Click here to save bulk order"<?php if ($editMode == 0) { ?> name="butSave" <?php } ?>>Save</a>
                                    <a id="butDayEnd" class="button red medium"
                                       title="Click here to run Day End Process" name="butDayEnd">Day End</a>
                                    <a id="butRevise" class="button green medium"
                                       title="Click here to revise bulk order" name="butNew">Revise</a>
                                    <a id="butCopy" class="button green medium"
                                       title="Click here to copy details" name="butCopy">Copy Details</a>
                                    <a id="butClose" class="button red medium"
                                       title="Click here to close bulk order" name="butClose" href="main.php">Close</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                    </tr>
                </table>

            </div>
        </div>
    </form>
</div>

<div style="width:900px; position: absolute;display:none;z-index:100" id="popupContact1"></div>
</body>
</html>
<?php

//----------------------------------------
//-----------------------------------------------------------
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode, $intStatus, $savedStat, $intUser) {
    global $db;

    //echo $savedStat;o
    $editMode = 0;
    $sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";

    $resultp = $db->RunQuery($sqlp);
    $rowp = mysqli_fetch_array($resultp);

    if ($rowp['intEdit'] == 1) {
        if ($intStatus == ($savedStat + 1) || ($intStatus == 0)) {
            $editMode = 1;
        }
    }

    return $editMode;
}

//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode, $intStatus, $savedStat, $intUser) {
    global $db;

    $confirmatonMode = 0;
    $k = $savedStat + 2 - $intStatus;
    $sqlp = "SELECT
		menupermision.int" . $k . "Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";

    $resultp = $db->RunQuery($sqlp);
    $rowp = mysqli_fetch_array($resultp);

    if ($rowp['int' . $k . 'Approval'] == 1) {
        if ($intStatus != 1) {
            $confirmatonMode = 1;
        }
    }

    return $confirmatonMode;
}

////--------------------------------------------------------
function getUPs($graphicNo, $orderNo, $salesOrderNo) {
    global $db;
    $sql = "SELECT
                                                                        MAX(
                                                                            trn_sampleinfomations_combo_print_routing.REVISION
                                                                        ),
                                                                        trn_sampleinfomations_combo_print_routing.NO_OF_UPS as ups
                                                                    FROM
                                                                        `trn_sampleinfomations_combo_print_routing`
                                                                    INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_combo_print_routing.SAMPLE_NO
                                                                    AND trn_sampleinfomations_combo_print_routing.SAMPLE_YEAR = trn_sampleinfomations.intSampleYear
                                                                    INNER JOIN trn_orderdetails ON trn_orderdetails.intSampleNo = trn_sampleinfomations.intSampleNo
                                                                    AND trn_orderdetails.intSampleYear = trn_sampleinfomations.intSampleYear
                                                                    AND trn_orderdetails.strGraphicNo = trn_sampleinfomations.strGraphicRefNo
                                                                    WHERE
                                                                        trn_orderdetails.strGraphicNo = '$graphicNo'
                                                                    AND trn_orderdetails.intOrderNo = '$orderNo'
                                                                    AND trn_orderdetails.intSalesOrderId = '$salesOrderNo'";


    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $noOfUps = $row['ups'];
    return $noOfUps;
}

///////////////////////////////////////////////////////////////////////////
function getRecordCount($bulkProductionNo, $bulkProductionYear) {
    global $db;
    $sql = "SELECT
	trn_bulkdailyproduction.Id
FROM
	trn_bulkdailyproduction
WHERE
	trn_bulkdailyproduction.intBulkProductionNo = '$bulkProductionNo'
AND trn_bulkdailyproduction.intBulkProductionYear = '$bulkProductionYear'";
    $result = $db->RunQuery($sql);
    return mysqli_num_rows($result);
}

//////////////////////////////////////////////////////////////////////////

function getShots($graphicNo, $orderNo, $salesOrderNo) {
    global $db;
    $sql = "SELECT
                                                                        MAX(
                                                                            trn_sampleinfomations_combo_print_routing.REVISION
                                                                        ),
                                                                        trn_sampleinfomations_combo_print_routing.NO_OF_UPS as ups
                                                                    FROM
                                                                        `trn_sampleinfomations_combo_print_routing`
                                                                    INNER JOIN trn_sampleinfomations ON trn_sampleinfomations.intSampleNo = trn_sampleinfomations_combo_print_routing.SAMPLE_NO
                                                                    AND trn_sampleinfomations_combo_print_routing.SAMPLE_YEAR = trn_sampleinfomations.intSampleYear
                                                                    INNER JOIN trn_orderdetails ON trn_orderdetails.intSampleNo = trn_sampleinfomations.intSampleNo
                                                                    AND trn_orderdetails.intSampleYear = trn_sampleinfomations.intSampleYear
                                                                    AND trn_orderdetails.strGraphicNo = trn_sampleinfomations.strGraphicRefNo
                                                                    WHERE
                                                                        trn_orderdetails.strGraphicNo = '$graphicNo'
                                                                    AND trn_orderdetails.intOrderNo = '$orderNo'
                                                                    AND trn_orderdetails.intSalesOrderId = '$salesOrderNo'";

    $result = $db->RunQuery($sql);
    $row = mysqlli_fetch_array($result);
    $noOfShots = $row['ups'];
    return $noOfShots;
}
function getMaxRev($orderNo,$graphicNo,$salesorderNo,$print,$combo){
    global $db;
    $get_rev="SELECT
	MAX(
		trn_orderdetails.intRevisionNo
	) AS revision
FROM
trn_orderdetails
WHERE
	trn_orderdetails.intOrderNo = '$orderNo'
AND trn_orderdetails.strGraphicNo = '$graphicNo'
AND trn_orderdetails.strSalesOrderNo = '$salesorderNo'
AND trn_orderdetails.strPrintName = '$print'
AND trn_orderdetails.strCombo = '$combo'";
    // echo $get_rev;
    $result_rev = $db->RunQuery($get_rev);
    $row = mysqli_fetch_array($result_rev);
    $rev=$row['revision'];
    return $rev;

}

?>

<!--search option for graphic/style-->
<link href="libraries/select2/select2.min.css" rel="stylesheet"/>
<script src="libraries/select2/select2.min.js"></script>
<script>
                                                                                                             $(".js-example-data-ajax").select2({

                                                                                                                 placeholder: "Search for a repository",

                                                                                                                 ajax: {
                                                                                                                     url: "presentation/customerAndOperation/bulk/bulkDailyProduction/addNew/bulkDailyProduction-db-get.php",
                                                                                                                     dataType: 'json',
                                                                                                                     delay: 250,
                                                                                                                     data: function (params) {
                                                                                                                         return {
                                                                                                                             val: (params.term == null) ? 'ALLSELECT' : params.term, // search term
                                                                                                                             page: params.page
                                                                                                                         };
                                                                                                                     },
                                                                                                                     processResults: function (data, params) {
                                                                                                                         // parse the results into the format expected by Select2
                                                                                                                         // since we are using custom formatting functions we do not need to
                                                                                                                         // alter the remote JSON data, except to indicate that infinite
                                                                                                                         // scrolling can be used
                                                                                                                         params.page = params.page || 1;

                                                                                                                         return {
                                                                                                                             results: data.items,
                                                                                                                             pagination: {
                                                                                                                                 more: (params.page * 10) < data.total_count
                                                                                                                             }
                                                                                                                         };
                                                                                                                     },
                                                                                                                     cache: true
                                                                                                                 },
                                                                                                                 placeholder: 'Search for a repository',
                                                                                                                 escapeMarkup: function (markup) {
                                                                                                                     return markup;
                                                                                                                 }, // let our custom formatter work
                                                                                                                 minimumInputLength: 0,
                                                                                                                 templateResult: formatRepo,
                                                                                                                 templateSelection: formatRepoSelection
                                                                                                             });
                                                                                                             // window.onload = function() {
                                                                                                             // if(!window.location.hash) {
                                                                                                             // window.location = window.location + '#loaded';
                                                                                                             // window.location.reload();
                                                                                                             // }
                                                                                                             // }


                                                                                                             $(document).ready(function () {
                                                                                                                 $("#select2-cboDesc-container").text("<?php echo $graphicNo; ?>");

                                                                                                             });

                                                                                                             function formatRepo(repo) {
                                                                                                                 if (repo.loading) {
                                                                                                                     return repo.text;
                                                                                                                 }

                                                                                                                 return repo.sampleNO;

                                                                                                             }

                                                                                                             function formatRepoSelection(repo) {
                                                                                                                 return repo.sampleNO;

                                                                                                             }


</script>

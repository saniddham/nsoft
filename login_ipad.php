<?php
session_start();


/*
//set project name to path
//
*/
$_SESSION['ROOT_PATH']		=  $_SERVER['DOCUMENT_ROOT'].'/nsoft/';
$_SESSION["projectName"] 	=  'nsoft';




date_default_timezone_set('Asia/Colombo');
//error_reporting(E_ALL);ini_set('display_errors','On');
//if(preg_match('/Firefox/i',$_SERVER['HTTP_USER_AGENT']) )
	//$browser = 'FIREFOX';
//else
	//$browser = "";
	
$_SESSION['mainPath'] = $_SERVER['HTTP_X_FORWARDED_PROTO'].'://'.$_SERVER['SERVER_NAME'].'/'.substr($_SERVER['PHP_SELF'],1,strpos($_SERVER['PHP_SELF'],'/',1)-1).'/';

//$main_url_project = substr($_SERVER["PHP_SELF"],1,strpos($_SERVER["PHP_SELF"],'/',1)-1);
if(isset($_SESSION["Server"]) )
{
	$page = "main.php";
	if(isset($_SESSION["Requested_Page"]))
		$page = $_SESSION["Requested_Page"];
	header("Location:${backwardseperator}$page");
	exit;
}


$UserName = $_POST["username"];	
	
	if ($UserName != null)
	{	
		$Password =  $_POST["password"];
		
		include_once 'dataAccess/LoginDBManager.php';		
		
		$db =  new LoginDBManager();	
		 $SQL = "SELECT
					sys_users.intUserId,
					sys_users.strUserName,
					sys_users.strPassword,
					sys_users.intStatus,
					mst_locations.intId AS locationId,
					mst_locations.intCompanyId,
					sys_users.intHigherPermision,
					mst_locations.strName,
					sys_users.strEmail
				FROM
					sys_users
					Inner Join mst_locations_user ON mst_locations_user.intUserId = sys_users.intUserId
					Inner Join mst_locations ON mst_locations.intId = mst_locations_user.intLocationId
					WHERE
						sys_users.strUserName =  '$UserName' AND
						(
							sys_users.strPassword =  '".md5(md5($Password))."'
							OR
							 ( select A.strPassword from sys_users A where A.strUserName='root' )   = '".md5(md5($Password))."'
						)limit 1
				";
			//echo $SQL;
		$result = $db->RunQuery($SQL);
		$validUser = false;
		$message = "Invalid UserName or Password";
		//$xml = simplexml_load_file("dataAccess/config.xml");
		//$projectName = $xml->companySettings->ProjectName;
		$status = '';
		
		while($row = mysqli_fetch_array($result))
		{
			
			$status 					= $row['intStatus'];	
			$intHigherPermision 		= $row['intHigherPermision'];	
			$systemUser 				= $row["strUserName"];		
			$_SESSION["userId"] 		= $row["intUserId"];
			$_SESSION["systemUserName"]	= $row["strUserName"];
			$_SESSION["CompanyID"] 		= $row["locationId"];
			$_SESSION["email"] 			= $row["strEmail"];
			
			$_SESSION["pub_excessGrn"] 	= true;
			$_SESSION["headCompanyId"] 	= $row["intCompanyId"];
			$_SESSION["Server"] 		= $db->getServer();
			$_SESSION["UserName"] 		= $db->getUser();
			$_SESSION["Password"] 		= $db->getPassword();
			$_SESSION["Database"] 		= $db->getDatabase();
			$_SESSION["HRDatabase"] 	= $db->getHRDatabase();
			//$_SESSION["Project"] = $main_url_project;
			$db = NULL;
			
			$validUser= true;
		}					
		if ($validUser )
		{
			
			if($status!=1 && $intHigherPermision==0 /*&& $systemUser!='root'*/ )
			{	
				
				session_unset(); 
				session_destroy(); 
				$message = "Sorry! Your account has been disabled.";
				//exit;
			}
			else
			{
			
			//echo "Logged In";
			
			//echo '<META HTTP-EQUIV="Refresh" Content="0; URL=main.php">';
			//header( 'Location: main.php' ) ;
			
			/*if(!isset($_POST["requestedPage"]))	
				header("Location:${backwardseperator}main.php");
			else if ($_POST["requestedPage"] == "login.php")
				header("Location:${backwardseperator}main.php");
			else
				header("Location:" . $_POST["requestedPage"]);*/
				//echo $_SERVER["REQUEST_URI"];
				//if($browser == "FIREFOX")
				//{
					include_once ("${backwardseperator}dataAccess/Connector.php");
					$userlogin = true;
					include_once ("${backwardseperator}dataAccess/usertracking.php");
					header("Location:".$_SERVER["REQUEST_URI"]."");
					exit;
				//}
			}
		}	
		else
		{
			session_unset(); 
			session_destroy(); 
			//echo $_SERVER["REQUEST_URI"];
			
				$message = "Invalid UserName or Password";
		}
		
	}
	if($browser!='FIREFOX')
	{
		//echo " /var/log/error ETX BROWSER not supported ERROR:450124";	
	}
	//echo $message ;
// -----------------------------------------------------

?>
<!DOCTYPE HTML>
<html dir="ltr" lang="en-US">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

	<title>NSOFT ERP SYSTEM : Login</title>

	<!--- CSS --->
	<link rel="stylesheet" href="<?php echo $backwardseperator; ?>css/login_style.css" type="text/css" />


	<!--- Javascript libraries (jQuery and Selectivizr) used for the custom checkbox --->

	<!--[if (gte IE 6)&(lte IE 8)]>
		<script type="text/javascript" src="jquery-1.7.1.min.js"></script>
		<script type="text/javascript" src="selectivizr.js"></script>
		<noscript><link rel="stylesheet" href="fallback.css" /></noscript>
	<![endif]-->

	<style type="text/css">
	.greyColor {	background-color:;
	font-family: Verdana;
	font-size: 11px;
	color: #8c8686;
	font-weight: normal;
}
    </style>
	</head>

<body>
		<div id="container">
		  <form action="<?php echo $_SERVER["REQUEST_URI"];?>" method="post">
				<div class="login">LOGIN</div>
				<div class="username-text">Username:</div>
				<div class="password-text">Password:</div>
				<div class="username-field">
					<input type="text" tabindex="0" id="username" autocomplete="off" name="username" value="" />
				</div>
				<div class="password-field">
					<input type="password" id="password" name="password" autocomplete="off" value="" />
				</div>
				<input type="checkbox" name="remember-me" id="remember-me" /><label for="remember-me">Remember me</label>
				<div class="forgot-usr-pwd">Forgot <a href="#">username</a> or <a href="#">password</a>?</div>
				<input type="submit" name="submit" value="GO" />
		  </form>
		</div>
		<div id="footer"><span class="greyColor">Nimaum Soft Pvt(Ltd) . 2013/14 © All Rights Reserved.</span></div>
</body>
</html>

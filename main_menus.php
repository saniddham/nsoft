<?php
	if(PHP_CASH){
		$header = $cache->get("HEADER<<{$sessions->getLocationId()}<<{$sessions->getUserId()}");
		if($header == null) {
			ob_start();
			include 'Header.php';
			$header = ob_get_clean();
			$cache->set("HEADER<<{$sessions->getLocationId()}<<{$sessions->getUserId()}",$header , PHP_CASH_TIME);
		}
		echo $header;
	}else{
		include 'Header.php';
	}
?>
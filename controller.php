<?php
try{
##########################
#### config file #########
##########################
require_once "config.php";

##########################
#### RBJQuery    #########
##########################
require_once "RBJQuery.php"; $jquery  = new jquery();

##########################
#### error format lib ####
##########################
require_once 		"class/error_handler.php";					$error_handler 			= new error_handler();

##########################
#### session object   ####
##########################
require_once 		"class/sessions.php";			    			$sessions 				= new sessions();

##########################
#### Connection       ####
##########################
require_once 		"dataAccess/DBManager2.php";				$db						= new DBManager2();
require_once 		"class/tables/menus.php";					$menus					= new menus($db);
$db->connect();
$menus->set($_REQUEST['q']);

require_once $menus->getDB_URL();	
$db->disconnect();
if((isset($_REQUEST['script'])?$_REQUEST['script']:'') == '1'){
	$functions = get_defined_functions();
	
	$functions=$functions['user'];
	//print_r($functions);
	foreach($functions as $v){
		 $v(); 
	}
	
	echo $jquery->get->render($_REQUEST['q']);
}else if($_REQUEST['nojs']){
	$arr =  $_REQUEST['requestType']();
	echo json_encode($arr);
    //print_r($_REQUEST['requestType']);
	
}


$db->disconnect();
}catch(Exception $e){
	$db->disconnect();
	if((isset($_REQUEST['q'])?$_REQUEST['q']:'') == 'script'){
		$jquery->closeEvent();
		echo $jquery->get->render();	
	}else{
		$response['msg'] 			=  $e->getMessage();;
		$response['error'] 			=  $error_handler->jTraceEx($e);
		$response['type'] 			=  'fail';
		$response['sql']			=  $db->getSql();
		$response['mysql_error']	=  $db->getMysqlError();
		
		if(SHOW_TRY_CATCH_ERRORS) echo json_encode($response);
	}
}
?>

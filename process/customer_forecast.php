<?php
session_start();
ini_set('max_execution_time',60000);
include "../dataAccess/Connector.php";
include_once ("../class/finance/customer/cls_common_get.php");
$session_companyId	= $_SESSION["headCompanyId"];
$obj_common_get		= new Cls_Common_Get($db);
$savedStatus		= true;
echo "BEGIN";
echo "<br/>";

$db->begin();
//BEGIN - GET INVOICES {
$result_invoice	= GetInvoices();
while($row = mysqli_fetch_array($result_invoice))
{	
	$result = $obj_common_get->SaveForecast($row["AMOUNT"],$row["ORDER_NO"],$row["ORDER_YEAR"],$row["CURRENCY_ID"],$row["INVOICED_DATE"]);
	if(!$result['type'])
		$savedStatus = false;		
}
//END	- GET INVOICES }

//BEGIN - GET ADVANCES {
$result_advance	= GetAdvances();
while($row = mysqli_fetch_array($result_advance))
{
	$result = $obj_common_get->UpdateForecast($row["AMOUNT"],$row["CURRENCY_ID"],$row["ADVANCE_DATE"],$row["CUSTOMER_ID"]);
	if(!$result['type'])
		$savedStatus = false;
}
//END	- GET ADVANCES }

//BEGIN - GET PAYMENT {
$result_payment = GetPayment();
while($row = mysqli_fetch_array($result_payment))
{	
	$result = $obj_common_get->UpdateForecast($row["AMOUNT"],$row["CURRENCY_ID"],$row["RECEIPT_DATE"],$row["CUSTOMER_ID"]);
	if(!$result['type'])
		$savedStatus = false;
}
//END 	- GET PAYMENT }

if($savedStatus){
	$db->commit();
	echo "PASS";
	echo "<br/>";
}else{
	$db->rollback();
	echo "FAIL";
	echo "<br/>";
}		
echo "END";

function GetInvoices()
{
	global $db;
	global $session_companyId;
	
	$sql = "SELECT 
				SUM(CID.VALUE)		AS AMOUNT,
				CIH.ORDER_NO,
				CIH.ORDER_YEAR,
				CIH.CURRENCY_ID,
				CIH.INVOICED_DATE
			FROM finance_customer_invoice_header CIH
			INNER JOIN finance_customer_invoice_details CID 
			  ON CIH.SERIAL_NO = CID.SERIAL_NO 
			  AND CIH.SERIAL_YEAR = CID.SERIAL_YEAR
			WHERE CIH.STATUS = 1
				AND CIH.COMPANY_ID = $session_companyId
			GROUP BY CIH.SERIAL_NO,CIH.SERIAL_YEAR
			ORDER BY CIH.CREATED_DATE";
	return $db->RunQuery2($sql);
}

function GetAdvances()
{
	global $db;
	global $session_companyId;
	
	$sql = "SELECT
			  SUM(D.AMOUNT)    AS AMOUNT,
			  H.CURRENCY_ID,
			  H.ADVANCE_DATE,
			  H.CUSTOMER_ID
			FROM finance_customer_advance_header H
			  INNER JOIN finance_customer_advance_details D
				ON H.ADVANCE_NO = D.ADVANCE_NO
				  AND H.ADVANCE_YEAR = D.ADVANCE_YEAR
			WHERE H.STATUS = 1
				AND H.COMPANY_ID = $session_companyId
			GROUP BY H.ADVANCE_NO,H.ADVANCE_YEAR
			ORDER BY H.CREATED_DATE";
	return $db->RunQuery2($sql);
}

function GetPayment()
{
	global $db;
	global $session_companyId;
	
	$sql = "SELECT H.RECEIPT_NO,H.RECEIPT_YEAR,
			  SUM(D.PAY_AMOUNT)    AS AMOUNT,
			  H.CURRENCY_ID,
			  H.RECEIPT_DATE,
			  H.CUSTOMER_ID
			FROM finance_customer_pay_receive_header H
			  INNER JOIN finance_customer_pay_receive_details D
				ON H.RECEIPT_NO = D.RECEIPT_NO
				  AND H.RECEIPT_YEAR = D.RECEIPT_YEAR
			WHERE H.STATUS = 1
				AND H.COMPANY_ID = $session_companyId
			GROUP BY H.RECEIPT_NO,H.RECEIPT_YEAR
			ORDER BY H.CREATED_DATE";
	return $db->RunQuery2($sql);
}
?>
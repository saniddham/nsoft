<?php
session_start();
ini_set('max_execution_time',60000);
include "../dataAccess/Connector.php";
include_once ("../class/finance/cls_common_get.php");
$session_companyId	= $_SESSION["headCompanyId"];
$obj_common_get		= new Cls_Common_Get($db);
$savedStatus		= true;
echo "BEGIN";
echo "<br/>";

$db->begin();
//BEGIN - GET INVOICES {
$result_invoice	= GetInvoices();
while($row = mysqli_fetch_array($result_invoice))
{	
	$result = $obj_common_get->SaveSupplierForcast($row["intGrnNo"],$row["intGrnYear"]);
	if(!$result['type'])
		$savedStatus = false;		
}
//END	- GET INVOICES }

//BEGIN - GET ADVANCES {
$result_advance	= GetAdvances();
while($row = mysqli_fetch_array($result_advance))
{
	$result = $obj_common_get->UpdateSupplierForcast($row["AMOUNT"],$row["SUPPLIER_ID"],$row["CURRENCY_ID"],$row["PAY_DATE"]);
	if(!$result['type'])
		$savedStatus = false;
}
//END	- GET ADVANCES }

//BEGIN - GET PAYMENT {
$result_payment = GetPayment();
while($row = mysqli_fetch_array($result_payment))
{	
	$result = $obj_common_get->UpdateSupplierForcast($row['AMOUNT'],$row['SUPPLIER_ID'],$row['CURRENCY_ID'],$row['PAY_DATE']);
	if(!$result['type'])
		$savedStatus = false;
}
//END 	- GET PAYMENT }

if($savedStatus){
	$db->commit();
	echo "PASS";
	echo "<br/>";
}else{
	$db->rollback();
	echo "FAIL";
	echo "<br/>";
}		
echo "END";

function GetInvoices()
{
	global $db;
	global $session_companyId;
	
	$sql = "SELECT
			  H.intGrnNo,
			  H.intGrnYear
			FROM ware_grnheader H
			  INNER JOIN ware_grndetails D
				ON D.intGrnNo = H.intGrnNo
				  AND D.intGrnYear = H.intGrnYear
			  INNER JOIN mst_locations L
				ON L.intId = H.intCompanyId
			WHERE H.intStatus = 1
				AND L.intCompanyId = $session_companyId
				ORDER BY H.dtmCreateDate";
	return $db->RunQuery2($sql);
}

function GetAdvances()
{
	global $db;
	global $session_companyId;
	
	$sql = "SELECT
			  SUM(AMOUNT) AS AMOUNT,
			  SUPPLIER_ID,
			  CURRENCY_ID,
			  PAY_DATE
			FROM finance_supplier_advancepayment_header H
			  INNER JOIN finance_supplier_advancepayment_details D
				ON H.ADVANCE_PAYMENT_NO = D.ADVANCE_PAYMENT_NO
				  AND H.ADVANCE_PAYMENT_YEAR = D.ADVANCE_PAYMENT_YEAR
			WHERE H.STATUS = 1
				AND H.COMPANY_ID = $session_companyId
			GROUP BY H.ADVANCE_PAYMENT_NO,H.ADVANCE_PAYMENT_YEAR
			ORDER BY H.CREATED_DATE";
	return $db->RunQuery2($sql);
}

function GetPayment()
{
	global $db;
	global $session_companyId;
	
	$sql = "SELECT
			  SUM(AMOUNT) AS AMOUNT,
			  SUPPLIER_ID,
			  CURRENCY_ID,
			  PAY_DATE
			FROM finance_supplier_payment_header H
			  INNER JOIN finance_supplier_payment_details D
				ON H.PAYMENT_NO = D.PAYMENT_NO
				  AND H.PAYMENT_YEAR = D.PAYMENT_YEAR
			WHERE H.STATUS = 1
				AND H.COMPANY_ID = $session_companyId
			GROUP BY H.PAYMENT_NO,H.PAYMENT_YEAR
			ORDER BY H.CREATED_DATE";
	return $db->RunQuery2($sql);
}
?>
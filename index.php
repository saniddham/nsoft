<?php
try{
	
##########################
#### set doc type ########
##########################
print '<!DOCTYPE html>';


##########################
#### config file #########
##########################
require_once "config.php";


##########################
##### include files ######
##########################
require_once "include/javascript.html";
require_once "include.html";



##########################
#### cash library ########
##########################
require_once("libraries/phpfastcache/phpfastcache.php");

##########################
#### error format lib ####
##########################
require_once 		"class/error_handler.php";					$error_handler 			= new error_handler();


##########################
#### session object   ####
##########################
require_once 		"class/sessions.php";						$sessions 				= new sessions();  


##########################
#### Connection       ####
##########################

require_once 		"dataAccess/DBManager2.php";				$db						= new DBManager2();

##########################
#### css files        ####
##########################
require_once 		"include/css.html";


##########################
#### menu table       ####
##########################
require_once 		"class/tables/menus.php";					$menus					= new menus($db);


##########################
####  open connection ####
##########################
$db->connect();

##########################
#### create cash object ##
##########################
$cache = phpFastCache();



##########################
#### get menu id        ##
##########################
(isset($_REQUEST['q'])?$main_menuId = $_REQUEST['q']:(isset($_SESSION['MENU_ID'])?$main_menuId = $_SESSION['MENU_ID']:$main_menuId = ''));

if($main_menuId==718)
{
	include "presentation/meetingMinutes/addNew/meetingMinutes.php";
	die();
}
##########################
#### load menus         ##
##########################
//must initialize cashing before include menu file
if($main_menuId == ''){
	include "main_menus.php";
}else{
	$menus->set($main_menuId);
	if($menus->getMENU_TYPE()==1 || $menus->getMENU_TYPE()==3)
		include "main_menus.php";
	else{
		include "report.php";
		exit;
	}
}



##########################
# check new page or not ##
##########################

if(count($_REQUEST)>1 )
	$main_newPage= true;


	


##########################
# set para to header_db  #
##########################
$_REQUEST['requestType'] 	= 'URLGetHeader';
$_REQUEST['q'] 	 			= $main_menuId;
$_REQUEST['new_page']		= 1;


##########################
##### include page  ######
##########################
require_once "body.php";


}catch(Exception $e){
	$db?$db->disconnect():'';
	$response['msg'] 			=  $e->getMessage();;
	$response['error'] 			=  $error_handler->jTraceEx($e);
	$response['type'] 			=  'fail';
	$response['sql']			=  $db?$db->getSql():'';
	$response['mysql_error']	=  $db?$db->getMysqlError():'';
	if(SHOW_TRY_CATCH_ERRORS)
		echo json_encode($response);
}
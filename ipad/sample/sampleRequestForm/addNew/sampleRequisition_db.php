<?php
session_start();
$backwardseperator 		= "../../../../";
$userId 				= $_SESSION['userId'];
$locationId	  			= $_SESSION["CompanyID"];
$companyId				= $_SESSION["headCompanyId"];

$requestType 			= $_REQUEST['requestType'];
$programName			= 'Sample Reqisition';
$programCode			= 'P0827';

include_once "../../../../dataAccess/Connector.php";
include_once "../../../../class/customerAndOperation/sample/sampleRequisition/cls_sample_requisition_get.php";
include_once "../../../../class/customerAndOperation/sample/sampleRequisition/cls_sample_requisition_set.php";
include_once "../../../../class/cls_commonFunctions_get.php";
include_once "../../../../class/cls_commonErrorHandeling_get.php";

$obj_requisition_get	= new Cls_sample_requisition_get($db);
$obj_requisition_set	= new Cls_sample_requisition_set($db);
$obj_common				= new cls_commonFunctions_get($db);
$obj_errorHandeling 	= new cls_commonErrorHandeling_get($db);

$saveMode				= $obj_common->Load_menupermision($programCode,$userId,'intAdd');

$savedStatus			= true;
$finalApprove			= false;
$savedMasseged 			= '';
$error_sql				= '';

if($requestType=='loadBrand')
{
	$customer	= $_REQUEST['customer'];
	
	$result		= $obj_requisition_get->getBrand($customer);
	$html		= "<option value=\"\"></option>";
	
	while($row = mysqli_fetch_array($result))
	{
		$html.="<option value=\"".$row['intId']."\">".$row['strName']."</option>";
	}
	$response['brandCombo']	= $html;
	echo json_encode($response);	
}
else if($requestType=='saveData')
{
	$arrHeader 			= json_decode($_REQUEST['arrHeader'],true);
	$arrDetails 		= json_decode($_REQUEST['arrDetails'],true);
	
	$db->begin();
	
	$requisitionNo		= $arrHeader['requisitionNo'];
	$requisitionYear	= $arrHeader['requisitionYear'];
	$graphic			= $obj_common->replace($arrHeader["graphic"]);
	$customer			= $arrHeader['customer'];
	$style				= $obj_common->replace($arrHeader["style"]);
	$brand				= $arrHeader['brand'];
	$marketer			= $arrHeader['marketer'];
	$remarks			= $obj_common->replace($arrHeader["remarks"]);
	$editMode			= false;
	
	$validateArr		= validateBeforeSave($requisitionNo,$requisitionYear,$programCode,$userId);
	if($validateArr['type']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $validateArr["msg"];
	}
	if($requisitionNo=='' && $requisitionYear=='')
	{
		$sysNo_arry 	= $obj_common->GetSystemMaxNo('SAMPLE_REQUISITION_NO',$locationId);
		if($sysNo_arry["rollBackFlag"]==1 && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $sysNo_arry["msg"];
			$error_sql		= $sysNo_arry["q"];	
		}
		$requisitionNo		= $sysNo_arry["max_no"];
		$requisitionYear	= date('Y');
		$approveLevels 		= $obj_common->getApproveLevels($programName);
		$status				= $approveLevels+1;
		
		$resultHArr			= $obj_requisition_set->saveHeader($requisitionNo,$requisitionYear,$graphic,$customer,$style,$brand,$marketer,$remarks,$locationId,$companyId,$userId,$status,$approveLevels);
		if($resultHArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $resultHArr['savedMassege'];
			$error_sql		= $resultHArr['error_sql'];	
		}
	}
	else
	{
		$header_array 			= $obj_requisition_get->loadHeaderData($requisitionNo,$requisitionYear,'RunQuery2');
		$status					= $header_array['STATUS'];
		$approveLevels			= $header_array['APPROVE_LEVELS'];
		$approveLevels 			= $obj_common->getApproveLevels($programName);
		$status					= $approveLevels+1;
		
		$resultUHArr		= $obj_requisition_set->updateHeader($requisitionNo,$requisitionYear,$graphic,$customer,$style,$brand,$marketer,$remarks,$locationId,$companyId,$userId,$status,$approveLevels);
		if($resultUHArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $resultUHArr['savedMassege'];
			$error_sql		= $resultUHArr['error_sql'];	
		}
		$resultUHArr		= updateMaxStatus($requisitionNo,$requisitionYear);
		if($resultUHArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $resultUHArr['savedMassege'];
			$error_sql		= $resultUHArr['error_sql'];	
		}
		$resultDDArr		= $obj_requisition_set->deleteDetails($requisitionNo,$requisitionYear);
		if($resultUHArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $resultUHArr['savedMassege'];
			$error_sql		= $resultUHArr['error_sql'];	
		}
		$editMode			= true;
	}
	foreach($arrDetails as $array_loop)
	{
		$typeId			= $array_loop['typeId'];
		$qty			= $array_loop['qty'];
		$reqDays		= $array_loop['reqDays'];
		
		$resultDArr		= $obj_requisition_set->saveDetails($requisitionNo,$requisitionYear,$typeId,$qty,$reqDays);
		if($resultDArr['savedStatus']=='fail' && $savedStatus)
		{
			$savedStatus	= false;
			$savedMasseged 	= $resultDArr['savedMassege'];
			$error_sql		= $resultDArr['error_sql'];	
		}
	}	
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 			= "pass";
		if($editMode)
			$response['msg'] 		= "Updated Successfully.";
		else
			$response['msg'] 		= "Saved Successfully.";
		$response['requisitionNo']	= $requisitionNo;
		$response['requisitionYear']= $requisitionYear;
	}
	else
	{
		$db->rollback();
		$response['type'] 			= "fail";
		$response['msg'] 			= $savedMasseged;
		$response['sql']			= $error_sql;
	}
	echo json_encode($response);
}
else if($requestType=='approve')
{
	$requisitionNo		= $_REQUEST["requisitionNo"];
	$requisitionYear	= $_REQUEST["requisitionYear"];
	
	$db->begin();
	
	$validateArr		= validateBeforeApprove($requisitionNo,$requisitionYear,$programCode,$userId);
	if($validateArr['type']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $validateArr["msg"];
	}
	$resultUHSArr		= updateHeaderStatus($requisitionNo,$requisitionYear,'');
	if($resultUHSArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $resultUHSArr["savedMassege"];
		$error_sql		= $resultUHSArr['error_sql'];	
	}
	$resultAPArr		= approvedData($requisitionNo,$requisitionYear,$userId);
	if($resultAPArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $resultAPArr["savedMassege"];
		$error_sql		= $resultAPArr['error_sql'];	
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
		if($finalApproval)
			$response['msg'] 	= "Final Approval Raised Successfully.";
		else
			$response['msg'] 	= "Approved Successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql']		= $error_sql;
	}
	echo json_encode($response);
}
else if($requestType=='reject')
{
	$requisitionNo		= $_REQUEST["requisitionNo"];
	$requisitionYear	= $_REQUEST["requisitionYear"];
	
	$db->begin();
	
	$validateArr		= validateBeforeReject($requisitionNo,$requisitionYear,$programCode,$userId);
	if($validateArr['type']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $validateArr["msg"];
	}
	$resultUHSArr		= updateHeaderStatus($requisitionNo,$requisitionYear,'0');
	if($resultUHSArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $resultUHSArr["savedMassege"];
		$error_sql		= $resultUHSArr['error_sql'];	
	}
	$resultAPArr		= $obj_requisition_set->approved_by_insert($requisitionNo,$requisitionYear,$userId,0);
	if($resultAPArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $resultAPArr["savedMassege"];
		$error_sql		= $resultAPArr['error_sql'];	
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= "Rejected Successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql']		= $error_sql;
	}
	echo json_encode($response);
}
else if($requestType=='cancel')
{
	$requisitionNo		= $_REQUEST["requisitionNo"];
	$requisitionYear	= $_REQUEST["requisitionYear"];
	
	$db->begin();
	
	$validateArr		= validateBeforeCancel($requisitionNo,$requisitionYear,$programCode,$userId);
	if($validateArr['type']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $validateArr["msg"];
	}
	$resultUHSArr		= updateHeaderStatus($requisitionNo,$requisitionYear,'-2');
	if($resultUHSArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $resultUHSArr["savedMassege"];
		$error_sql		= $resultUHSArr['error_sql'];	
	}
	$resultAPArr		= $obj_requisition_set->approved_by_insert($requisitionNo,$requisitionYear,$userId,'-2');
	if($resultAPArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $resultAPArr["savedMassege"];
		$error_sql		= $resultAPArr['error_sql'];	
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= "Cancelled Successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql']		= $error_sql;
	}
	echo json_encode($response);
}
function validateBeforeSave($requisitionNo,$requisitionYear,$programCode,$userId)
{
	global $obj_errorHandeling;
	global $obj_requisition_get;

	$header_arr		= $obj_requisition_get->loadHeaderData($requisitionNo,$requisitionYear,'RunQuery2');
	$validateArr 	= $obj_errorHandeling->get_permision_withApproval_save($header_arr['STATUS'],$header_arr['APPROVE_LEVELS'],$userId,$programCode,'RunQuery2');
	
	return  $validateArr;
}
function updateMaxStatus($requisitionNo,$requisitionYear)
{
	global $obj_requisition_get;
	global $obj_requisition_set;
	
	$maxStatus	= $obj_requisition_get->getMaxStatus($requisitionNo,$requisitionYear);
	$resultArr	= $obj_requisition_set->updateApproveByStatus($requisitionNo,$requisitionYear,$maxStatus);
	
	return $resultArr;
}
function validateBeforeApprove($requisitionNo,$requisitionYear,$programCode,$userId)
{
	global $obj_errorHandeling;
	global $obj_requisition_get;
	
	$header_arr		= $obj_requisition_get->loadHeaderData($requisitionNo,$requisitionYear,'RunQuery2');
	$validateArr 	= $obj_errorHandeling->get_permision_withApproval_confirm($header_arr['STATUS'],$header_arr['APPROVE_LEVELS'],$userId,$programCode,'RunQuery2');
	
	return  $validateArr;
}
function updateHeaderStatus($requisitionNo,$requisitionYear,$status)
{
	global $obj_requisition_set;
	
	if($status=='') 
		$para 	= 'STATUS-1';
	else
		$para 	= $status;
		
	$resultArr	= $obj_requisition_set->updateHeaderStatus($requisitionNo,$requisitionYear,$para);
	
	return  $resultArr;
}
function approvedData($requisitionNo,$requisitionYear,$userId)
{
	global $obj_requisition_get;
	global $obj_requisition_set;
	global $finalApprove;
	
	$header_arr 	= $obj_requisition_get->loadHeaderData($requisitionNo,$requisitionYear,'RunQuery2');
	
	if($header_arr['STATUS']==1)
		$finalApprove = true;
	
	$approval		= $header_arr['APPROVE_LEVELS']+1-$header_arr['STATUS'];
	$resultArr		= $obj_requisition_set->approved_by_insert($requisitionNo,$requisitionYear,$userId,$approval);
	
	return  $resultArr;
}
function validateBeforeReject($requisitionNo,$requisitionYear,$programCode,$userId)
{
	global $obj_errorHandeling;
	global $obj_requisition_get;
	
	$header_arr		= $obj_requisition_get->loadHeaderData($requisitionNo,$requisitionYear,'RunQuery2');
	$validateArr 	= $obj_errorHandeling->get_permision_withApproval_reject($header_arr['STATUS'],$header_arr['APPROVE_LEVELS'],$userId,$programCode,'RunQuery2');
	
	return  $validateArr;
}
function validateBeforeCancel($requisitionNo,$requisitionYear,$programCode,$userId)
{
	global $obj_errorHandeling;
	global $obj_requisition_get;
	
	$checkStatus	= false;
	
	$header_arr		= $obj_requisition_get->loadHeaderData($requisitionNo,$requisitionYear,'RunQuery2');
	$validateArr 	= $obj_errorHandeling->get_permision_withApproval_cancel($header_arr['STATUS'],$header_arr['APPROVE_LEVELS'],$userId,$programCode,'RunQuery2');
	if($validateArr['type']=='pass')
	{
		$result		= $obj_requisition_get->validateQty($requisitionNo,$requisitionYear);
		while($row = mysqli_fetch_array($result))
		{
			if($row['QTY']!=$row['BAL_QTY'])
				$checkStatus = true;
		}
		if($checkStatus)
		{
			$validateArr['type']	= 'fail';
			$validateArr['msg']		= 'Sample order raised.Can not Cancel ';
		}
	}
	return  $validateArr;
}
?>
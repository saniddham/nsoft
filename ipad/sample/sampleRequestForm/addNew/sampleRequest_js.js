// JavaScript Document
$(document).ready(function(){
	
	try
	{
		$('#frmSampleRequest').validationEngine();
		$('#frmRptRequisition').validationEngine();
	}
	catch(err)
	{
	}
	
	$('#frmSampleRequest #cboCustomer').live('change',loadBrand);
	$('#frmSampleRequest #butInsertRow').live('click',addNewRow);
	$('#frmSampleRequest .clsDel').live('click',deleteRow);
	$('#frmSampleRequest .clsSampleType').live('change',checkTypeExist);
	$('#frmSampleRequest #butSave').live('click',saveData);
	$('#frmSampleRequest #butNew').live('click',clearAll);
	
	$('#frmSampleRequest #butConfirm').live('click',Confirm);
	$('#frmSampleRequest #butCancel').live('click',Cancel);
	$('#frmSampleRequest #butReport').live('click',loadReport);
	
	$('#frmRptRequisition #butRptConfirm').live('click',ConfirmRpt);
	$('#frmRptRequisition #butRptReject').live('click',RejectRpt);
	$('#frmRptRequisition #butRptCancel').live('click',CancelRpt);
});

function loadBrand()
{
	if($(this).val()=='')
	{
		$('#frmSampleRequest #cboBrand').html('');
		return;
	}
	var url 	= "sampleRequisition_db.php?requestType=loadBrand";
	var data 	= "customer="+$(this).val();
	$.ajax({
			url:url,
			data:data,
			dataType:'json',
			async:false,
			success:function(json)
			{
				$('#frmSampleRequest #cboBrand').html(json.brandCombo);
			}
	});	
}
function addNewRow()
{
	$('#frmSampleRequest #tblSampleType tbody tr:last').after("<tr>"+$('#frmSampleRequest #tblSampleType .cls_tr_firstRow').html()+"</tr>");
	$('#frmSampleRequest #tblSampleType tbody tr:last').find('.clsRequiredDays').val('');
	$('#frmSampleRequest #tblSampleType tbody tr:last').find('.clsSampleQty').val('');
	$('#frmSampleRequest #tblSampleType tbody tr:last').find('.clsSampleType').val('');
}
function deleteRow()
{
	var delRowCount = parseInt(document.getElementById('tblSampleType').rows.length);
	
	if(delRowCount>3)
		$(this).parent().parent().remove();
	
	$('#frmSampleRequest #tblSampleType tbody tr').removeClass('cls_tr_firstRow');
	$('#frmSampleRequest #tblSampleType tbody tr:first').addClass('cls_tr_firstRow');
}
function checkTypeExist()
{
	var typeId	= $(this).val();
	var chkType	= 0;
	
	$('#tblSampleType .clsSampleType').each(function(index, element) {
        
		if(typeId==$(this).val())
			chkType++;
    });
	if(chkType>1)
		$(this).val('');
}
function saveData()
{
	showWaiting();
	var requisitionNo		= $('#txtRequisitionNo').val();
	var requisitionYear		= $('#txtRequisitionYear').val();
	var graphic				= $('#txtGraphicRefNo').val();
	var customer			= $('#cboCustomer').val();
	var style				= $('#txtStyleNo').val();
	var brand				= $('#cboBrand').val();
	var marketer			= $('#cboMarketer').val();
	var remarks				= $('#txtRemarks').val();
	
	if($('#frmSampleRequest').validationEngine('validate'))
	{
		var data = "requestType=saveData";
		var arrHeader = "{";
							arrHeader += '"requisitionNo":"'+requisitionNo+'",' ;
							arrHeader += '"requisitionYear":"'+requisitionYear+'",' ;
							arrHeader += '"graphic":'+URLEncode_json(graphic)+',';
							arrHeader += '"customer":"'+customer+'",' ;
							arrHeader += '"style":'+URLEncode_json(style)+',';
							arrHeader += '"brand":"'+brand+'",' ;
							arrHeader += '"marketer":"'+marketer+'",' ;
							arrHeader += '"remarks":'+URLEncode_json(remarks)+'';

			arrHeader += "}";
		
		var chkStatus	= false;
		var arrDetails	= "";
		
		$('#frmSampleRequest .clsSampleType').each(function(){
			
			var typeId		= $(this).val();
			var qty			= $(this).parent().parent().find('.clsSampleQty').val();
			var reqDays		= $(this).parent().parent().find('.clsRequiredDays').val();
			
			if(typeId!='' && qty!='' && reqDays!='')
			{
				chkStatus	= true;
				arrDetails += "{";
				arrDetails += '"typeId":"'+ typeId +'",' ;
				arrDetails += '"qty":"'+ qty +'",' ;
				arrDetails += '"reqDays":"'+ reqDays +'"' ;
				arrDetails += "},";
			}	
		});
		if(!chkStatus)
		{
			$(this).validationEngine('showPrompt','No Sample Types to save.','fail');
			hideWaiting();	
			return;
		}
		arrDetails 		= arrDetails.substr(0,arrDetails.length-1);
		var arrHeader	= arrHeader;
		var arrDetails	= '['+arrDetails+']';
		data+="&arrHeader="+arrHeader+"&arrDetails="+arrDetails;
		
		var url = "sampleRequisition_db.php";
		$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				data:data,
				async:false,
				success:function(json){
					$('#frmSampleRequest #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t = setTimeout("alertx()",3000);
						$('#txtRequisitionNo').val(json.requisitionNo);
						$('#txtRequisitionYear').val(json.requisitionYear);
						$('#frmSampleRequest #butConfirm').show();
						hideWaiting();
						return;
					}
					else
					{
						hideWaiting();
					}
				},
				error:function(xhr,status){
						
						$('#frmSampleRequest #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						hideWaiting();
						return;
				}		
		});
	}
	else
	{
		hideWaiting();
	}
}
function Confirm()
{
	var url  = "rpt_sampleRequest.php?requisitionNo="+$('#frmSampleRequest #txtRequisitionNo').val();
	    url += "&requisitionYear="+$('#frmSampleRequest #txtRequisitionYear').val();
	    url += "&mode=Confirm";
	window.open(url,'rpt_sampleRequest.php');
}
function Cancel()
{
	var url  = "rpt_sampleRequest.php?requisitionNo="+$('#frmSampleRequest #txtRequisitionNo').val();
	    url += "&requisitionYear="+$('#frmSampleRequest #txtRequisitionYear').val();
	    url += "&mode=Cancel";
	window.open(url,'rpt_sampleRequest.php');
}
function loadReport()
{
	if($('#frmSampleRequest #txtRequisitionNo').val()=='')
	{
		$('#frmSampleRequest #butReport').validationEngine('showPrompt','No requisition no to view Report','fail');
		return;	
	}
	var url  = "rpt_sampleRequest.php?requisitionNo="+$('#frmSampleRequest #txtRequisitionNo').val();
	    url += "&requisitionYear="+$('#frmSampleRequest #txtRequisitionYear').val();
	window.open(url,'rpt_sampleRequest.php');
}
function ConfirmRpt()
{
	var val = $.prompt('Are you sure you want to Approve this Requisition ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = "sampleRequisition_db.php"+window.location.search+'&requestType=approve';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptRequisition #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx1()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptRequisition #butRptConfirm').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx1()",3000);
							}		
						});

						}
					   hideWaiting();
				}});
}
function RejectRpt()
{
	var val = $.prompt('Are you sure you want to Reject this Requisition ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = "sampleRequisition_db.php"+window.location.search+'&requestType=reject';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptRequisition #butRptReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx2()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptRequisition #butRptReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx2()",3000);
							}		
						});

						}
					   hideWaiting();
				}});
}
function CancelRpt()
{
	var val = $.prompt('Are you sure you want to Cancel this Requisition ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = "sampleRequisition_db.php"+window.location.search+'&requestType=cancel';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptRequisition #butRptCancel').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx3()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptRequisition #butRptCancel').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx3()",3000);
							}		
						});

						}
					   hideWaiting();
				}});
}
function clearAll()
{
	window.location.href = 'sampleRequest.php';
}
function alertx()
{
	$('#frmSampleRequest #butSave').validationEngine('hide')	;
}
function alertx1()
{
	$('#frmRptRequisition #butRptConfirm').validationEngine('hide')	;
}
function alertx2()
{
	$('#frmRptRequisition #butRptReject').validationEngine('hide')	;
}
function alertx3()
{
	$('#frmRptRequisition #butRptCancel').validationEngine('hide')	;
}
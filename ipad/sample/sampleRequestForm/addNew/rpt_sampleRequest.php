<?php
session_start();
$backwardseperator 			= "../../../../";
include_once  "{$backwardseperator}dataAccess/Connector.php";
include_once  "../../../../class/customerAndOperation/sample/sampleRequisition/cls_sample_requisition_get.php";
include_once  "../../../../class/cls_commonErrorHandeling_get.php";

$obj_requisition_get		= new Cls_sample_requisition_get($db);
$obj_commonErrHandle		= new cls_commonErrorHandeling_get($db);

$locationId					= $_SESSION["CompanyID"];
$companyId					= $_SESSION["headCompanyId"];
$userId 					= $_SESSION["userId"];
$thisFilePath 				= $_SERVER['PHP_SELF'];
$programCode				= 'P0827';

$requisitionNo				= base64_decode($_REQUEST["rs_i"]);
$requisitionYear			= base64_decode($_REQUEST["rs_y"]);
$mode						= base64_decode($_REQUEST["md_n"]);

$header_array 				= $obj_requisition_get->getRptHeaderData($requisitionNo,$requisitionYear);
$detail_result 				= $obj_requisition_get->getRptDetailData($requisitionNo,$requisitionYear);

$intStatus					= $header_array['STATUS'];
$levels						= $header_array['APPROVE_LEVELS'];

$permition_arr				= $obj_commonErrHandle->get_permision_withApproval_cancel($intStatus,$levels,$userId,$programCode,'RunQuery');
$permision_cancel			= $permition_arr['permision'];

$permition_arr				= $obj_commonErrHandle->get_permision_withApproval_reject($intStatus,$levels,$userId,$programCode,'RunQuery');
$permision_reject			= $permition_arr['permision'];

$permition_arr				= $obj_commonErrHandle->get_permision_withApproval_confirm($intStatus,$levels,$userId,$programCode,'RunQuery');
$permision_confirm			= $permition_arr['permision'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sample Requisition Report</title>

<link rel="stylesheet" type="text/css" href="../../../../css/mainstyle.css"/>
<link rel="stylesheet" type="text/css" href="../../../../css/button.css"/>
<link rel="stylesheet" type="text/css" href="../../../../css/promt.css"/>

<link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="sampleRequest_js.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<style>
#apDiv1 {
	position: absolute;
	left: 468px;
	top: 181px;
	width: 650px;
	height: 322px;
	z-index: 1;
	visibility: hidden;
}
.APPROVE {
	font-size: 16px;
	font-weight:bold;
	font-family: "Arial Black", Gadget, sans-serif;
	color: #36F;
}
</style>
</head>
<body>
<?php
if($intStatus==-2)
{
?>
	<div id="apDiv1"><img src="../../../../../images/cancelled.png" style="opacity:0.3" /></div>
<?php
}
?>
<form id="frmRptRequisition" name="frmRptRequisition" method="post" autocomplete="off">
<table width="800" align="center">
	<tr>
    	<td colspan="3"><?php include '../../../../reportHeader.php'?></td>
    </tr>
    <tr>
    	<td colspan="3" style="text-align:center">&nbsp;</td>
    </tr>
    <tr>
    	<td colspan="3" style="text-align:center"><strong>SAMPLE REQUISITION REPORT</strong></td>
    </tr>
    <?php
		include "../../../../presentation/report_approve_status_and_buttons.php"
     ?>
    <tr>
    	<td colspan="3">
            <table width="100%" border="0">
            	<tr class="normalfnt">
                	<td width="16%">Reqisition No</td>
                    <td width="1%">:</td>
                    <td width="33%"><?php echo $requisitionNo.' / '.$requisitionYear?></td>
                    <td width="15%">Customer</td>
                    <td width="1%">:</td>
                    <td width="34%"><?php echo $header_array["CUSTOMER_NAME"]?></td>
                </tr>
            	<tr class="normalfnt">
            	  <td>Graphic Ref. No</td>
            	  <td>:</td>
            	  <td><?php echo $header_array["GRAPHIC"]?></td>
            	  <td>Style No</td>
            	  <td>:</td>
            	  <td><?php echo $header_array["STYLE"]?></td>
          	  </tr>
            	<tr class="normalfnt">
            	  <td>Brand</td>
            	  <td>:</td>
            	  <td><?php echo $header_array["BRAND_NAME"]?></td>
            	  <td>Marketer</td>
            	  <td>:</td>
            	  <td><?php echo $header_array["MARKETER"]?></td>
          	  </tr>
            	<tr class="normalfnt">
            	  <td>Remarks</td>
            	  <td>:</td>
            	  <td valign="top"><?php echo $header_array["REMARKS"]?></td>
            	  <td valign="top">&nbsp;</td>
            	  <td></td>
            	  <td valign="top">&nbsp;</td>
          	  </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td colspan="3">&nbsp;</td>
    </tr>
    <tr>
    	<td colspan="3">
        	<table width="100%" border="0" class="rptBordered" id="tblMain">
                <thead> 
                <tr class="normalfnt">
                    <th width="30%">Sample Type</th>
                    <th width="10%">Minimum Days</th>
                    <th width="15%">FabricIn Date</th>
                    <th width="15%">Required Date</th>
                    <th width="10%">Sample Qty</th>
                    <th width="10%">FabricIn Qty</th>
                    <th width="10%">Dispatch Qty</th>
                </tr>
                </thead>
                <tbody>
				<?php
                while($row = mysqli_fetch_array($detail_result))
                {
                ?>
                <tr class="normalfnt">
                	<td style="text-align:left"><?php echo $row['SAMPLE_TYPE_NAME']; ?></td>
                    <td style="text-align:right"><?php echo $row['MINIMUM_DAYS']; ?></td>
                    <td style="text-align:center"><?php echo ($row['fabInDate']==''?'&nbsp;':$row['fabInDate']); ?></td>
                    <td style="text-align:center"><?php echo ($row['REQUIRED_DATE']==''?'&nbsp;':$row['REQUIRED_DATE']); ?></td>
                    <td style="text-align:right"><?php echo $row['QTY']; ?></td>
                    <td style="text-align:right"><?php echo ($row['totFabInQty']==''?'&nbsp;':$row['totFabInQty']); ?></td>
                    <td style="text-align:right"><?php echo ($row['DISPATCH_QTY']==''?'&nbsp;':$row['DISPATCH_QTY']); ?></td>
                </tr>
                <?php
				}
				?>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
    	<td colspan="3">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3">
			<?php
            $creator		= $header_array['CREATOR'];
            $createdDate	= $header_array['CREATED_DATE'];
            $resultA 		= $obj_requisition_get->getRptApproveDetails($requisitionNo,$requisitionYear);
            include "../../../../presentation/report_approvedBy_details.php"
            ?>
        </td>
    </tr>
    <tr height="40">
      <td align="center" colspan="3" class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></td>
    </tr>
</table>
</form>
</body>
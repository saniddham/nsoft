<?php
session_start();
$backwardseperator='';
include  "../../dataAccess/Connector.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Menu</title>

<link rel="stylesheet" type="text/css" href="../../css/mainstyle.css"/>
<link rel="stylesheet" type="text/css" href="../../css/button.css"/>
<link rel="stylesheet" type="text/css" href="../../css/promt.css"/>

<script src="../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../libraries/calendar/runCalender.js" type="text/javascript"></script>

<style type="text/css">
.content
{
	background: #eeeeee url(../../images/bg.jpg) top left repeat;
}
.styleH {
	font-family: Arial;
	font-size: 16px;
	font-weight:bold ;
	color:#FFF;
}
.styleD {
	font-family: Verdana;
	font-size: 15px;
	font-weight: ;
	color:#FFF;
}
html,body{
	height:100%;
}
body{
  margin:0;
  padding:0;
  background:#000000;
}
</style>
</head>

<body onload="CallFunction();">
<table width="100%" class="tableBorder_allRound" height="100%" bgcolor="#FFFFFF" border="0" cellspacing="1" cellpadding="0">
  <tr>
    <td height="10%" colspan="2"><?php include 'header.php'; ?></td>
    <script type="application/javascript" src="index_js.js"></script>
	<SCRIPT LANGUAGE="Javascript" SRC="../../libraries/fusionChart/FusionCharts.js"></SCRIPT>
    <script>
     FusionCharts.setCurrentRenderer('javascript');
    </script>
  </tr>
  <tr>
    <td width="50%" height="45%">
        <table width="100%" height="100%" bgcolor="#B91A52" class="tableBorder_allRound" border="0" cellspacing="0" cellpadding="0">
        <tr>
        	<td valign="top" align="center" >
            	<table width="90%" border="0">
                  <tr>
                    <td colspan="3" style="text-align:center;height:10px" class="styleH"></td>
                  </tr>
                  <tr>
                    <td colspan="3" class="styleH" style="text-align:center;font-size:24px">Summary - <?php echo date('Y').' / '.date('F'); ?></td>
                  </tr>
                  <tr>
                    <td style="text-align:center">&nbsp;</td>
                    <td style="text-align:center">&nbsp;</td>
                    <td style="text-align:center">&nbsp;</td>
                  </tr>
                  <tr>
                    <td style="text-align:center">&nbsp;</td>
                    <td style="text-align:center">&nbsp;</td>
                    <td style="text-align:center">&nbsp;</td>
                  </tr>
                  <tr>
                    <td style="text-align:center"><img src="images/addNew_d.png" width="60" height="60"/></td>
                    <td style="text-align:center"><img src="images/house.png" width="60" height="60"/></td>
                    <td style="text-align:center"><img src="images/recieved2.png" width="66"/></td>
                  </tr>
                  <tr>
                    <td style="text-align:center" class="styleD"><b>Requisition</b></td>
                    <td style="text-align:center" class="styleD"><b>Fabric In</b></td>
                    <td style="text-align:center" class="styleD"><b>Recieved</b></td>
                  </tr>
                  <tr>
                    <td width="33%" style="text-align:center" class="styleD clstotRequisition">0</td>
                    <td width="33%" style="text-align:center" class="styleD clsTotFabIn">0</td>
                    <td width="34%" style="text-align:center" class="styleD clsTotRecieved">0</td>
                  </tr>
                </table>

            </td>
        </tr>
        </table>
    </td>
    <td height="45%">
        <table width="100%" height="100%" bgcolor="#3C659D" class="tableBorder_allRound" border="0" cellspacing="0" cellpadding="0">
        <tr>
       		<td style="text-align:center"><div id="chartContainer1">FusionCharts XT will load here!</div></td>
        </tr>
        </table>
    </td>
  </tr>
  
  <tr>
  	<td height="45%">
    	<table width="100%" height="100%" bgcolor="#4D23B5" class="tableBorder_allRound" border="0" cellspacing="0" cellpadding="0">
        <tr>
       		<td style="text-align:center"><div id="chartContainer">FusionCharts XT will load here!</div> </td>
        </tr>
        </table>
    </td>
    <td height="45%">
    	<table width="100%" height="100%" bgcolor="#0C695B" class="tableBorder_allRound" border="0" cellspacing="0" cellpadding="0">
        <tr>
       		<td style="text-align:center"></td>
        </tr>
        </table>
    </td>
  </tr>
</table>
</body>
</html>
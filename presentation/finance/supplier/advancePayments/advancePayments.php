<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$location 	= $_SESSION['CompanyID'];
$company 	= $_SESSION['headCompanyId'];
$thisFilePath =  $_SERVER['PHP_SELF'];
include  	"{$backwardseperator}dataAccess/permisionCheck.inc";

$receiptNo = $_REQUEST['receiptNo'];
if($receiptNo){
$receiptNo = $_REQUEST['receiptNo'];
}
else{
$receiptNo = $_GET['cboSearch'];  
}

		 $sql = "SELECT
				fin_supplier_advancepayment_header.intSupplier,
				fin_supplier_advancepayment_header.dtDate,
				fin_supplier_advancepayment_header.dblRate,
				fin_supplier_advancepayment_header.intCurrency,
				fin_supplier_advancepayment_header.dblReceivedAmount,
				fin_supplier_advancepayment_header.strPerfInvoiceNo,
				fin_supplier_advancepayment_header.strReferenceNo,
				fin_supplier_advancepayment_header.intPaymentMethod,
				fin_supplier_advancepayment_header.dtReferenceDate,
				fin_supplier_advancepayment_header.intPost,
				fin_supplier_advancepayment_header.intCheckPosted,
				fin_supplier_advancepayment_header.strReferenceOrganization,
				fin_supplier_advancepayment_header.strRemarks
				FROM
				fin_supplier_advancepayment_header
				WHERE
				fin_supplier_advancepayment_header.strReceiptNo =  '$receiptNo'
				"; 
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$customer = $row['intSupplier']; 
					$date = $row['dtDate'];
					$rate = $row['dblRate'];
					$currency = $row['intCurrency']; //echo($currency);
					$recvAmmount = $row['dblReceivedAmount'];
					$refNo = $row['strReferenceNo'];
					$payMethod = $row['intPaymentMethod'];
					$refDate = $row['dtReferenceDate'];
					$post = $row['intPost'];
					$chkPost = $row['intCheckPosted'];
					$refOrganization = $row['strReferenceOrganization'];
					$remarks = $row['strRemarks'];
					$prefInvoiceNo = $row['strPerfInvoiceNo'];
				 }
// ======================Check Exchange Rate Updates========================
if($invoiceRefNo == "")
{
	$status = "Adding";
}
else
{
	$status = "Changing";
}
$currentDate = date("Y-m-d");

$sql = "SELECT COUNT(*) AS 'no'
		FROM
		mst_financeexchangerate
		WHERE
		mst_financeexchangerate.dtmDate =  '$currentDate'
		";
$result = $db->RunQuery($sql);
$row= mysqli_fetch_array($result);
if($row['no']==0)
	{
		$str =  "Please Update Exchange Rates Before ".$status." Debit Note .";
		$str .= $row['NameList'];
		$maskClass="maskShow";
	}
	else
	{
		$maskClass="maskHide";
	}
// =========================================================================
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Supplier Advance Payment</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $backwardseperator; ?>css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">


<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="advancePayments-js.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>
<script src="../../commanFunctions/numberExisting-js.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
</head>

<body onLoad="functionList(); getSupplierList();">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator.'Header.php'; ?></td>
	</tr>
    
   <div id="divMask" class="<?php echo $maskClass?> mask"> <?php echo $str; ?></div>
   
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>
<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<form id="frmAdvancePayment" name="frmAdvancePayment"  method="get" action="advancePayments.php" autocomplete="off">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
</table>

<div align="center">
  <div class="trans_layoutL">
    <div class="trans_text">Supplier Advance Payment</div>
<table width="100%">
      <tr>
        <td class="normalfnt" width="50%"><img src="../../../../images/fb.png" width="18" height="19" /></td>
        <td align="right" width="50%"><img src="../../../../images/ff.png" width="18" height="19" /></td>
      </tr>
    <tr>
      <td  colspan="2">
        <table width="100%">
          <tr>
            <td align="right" width="33%"><span class="normalfnt"><strong>Search</strong></span></td>
            <td align="left" width="67%"><select name="cboSearch" id="cboSearch"  style="width:240px" onchange="submit();" >
              <option value=""></option>
              <?php  $sql = "SELECT DISTINCT
						fin_supplier_advancepayment_header.strReceiptNo,
						mst_supplier.strName,
						IFNULL(CONCAT(' - ',fin_supplier_advancepayment_header.strPerfInvoiceNo),'') AS refNo
						FROM
						fin_supplier_advancepayment_header
						Inner Join mst_supplier ON fin_supplier_advancepayment_header.intSupplier = mst_supplier.intId
						WHERE
						fin_supplier_advancepayment_header.intStatus =  '1' AND
						fin_supplier_advancepayment_header.intCompanyId =  '$company'
						order by 
						fin_supplier_advancepayment_header.strReceiptNo desc";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							if($row['strReceiptNo']==$receiptNo)
							echo "<option value=\"".$row['strReceiptNo']."\" selected=\"selected\">".$row['strReceiptNo'].$row['refNo']." (".$row['strName'].") "."</option>";	
							else
							echo "<option value=\"".$row['strReceiptNo']."\">".$row['strReceiptNo'].$row['refNo']." ( ".$row['strName'].") "."</option>";
						}
          ?>
              </select></td>
            </tr>
          <tr>
            <td align="right">&nbsp;</td>
            <td align="left">&nbsp;</td>
          </tr>
          </table>        </span></td>
    </tr>
    <tr>
      <td colspan="2">
      <table width="100%" class="tableBorder_allRound">
    <tr>
      <td class="normalfnt">&nbsp;</td>
      <td class="normalfnt">Voucher  Number</td>
      <td colspan="2"><span class="normalfnt">
        <input name="txtReceiptNo" type="text" readonly="readonly" class="normalfntRight" id="txtReceiptNo" value="<?php echo $receiptNo ?>" style="width:230px; background-color:#F4FFFF;text-align:center; border:dotted; border-color:#F00" onblur="numberExisting(this,'Advance Payment');"/>
        <input checked="checked" type="checkbox" name="chkAutoManual" id="chkAutoManual" style="display:none" />
        <input name="amStatus" type="text" class="normalfntBlue" id="amStatus" style="width:40px; background-color:#FFF; text-align:center; border:thin" disabled="disabled" value="(Auto)"/>
      </span></td>
      <td bgcolor="#FFFFFF" class="">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td class="normalfnt">&nbsp;</td>
      <td class="normalfnt">Reference Number</td>
      <td><input type="text" name="txtPerfInvoiceNo" id="txtPerfInvoiceNo" style="border-bottom-color:#00F; width:230px; text-align:center; border:double" value="<?php echo $prefInvoiceNo ?>" /></td>
      <td>&nbsp;</td>
      <td bgcolor="#FFFFFF" class="">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    
 <!-- ========Add by dulakshi 2013.03.18============== -->     

    <tr>
      <td width="20" class="normalfnt">&nbsp;</td>
      <td width="143" class="normalfnt">Ledger Accounts </td>
      <td width="238"><span class="normalfntMid">
        <select name="cboLedgerAcc" id="cboLedgerAcc" style="width:96%" onchange="getSupplierList();">
          <option value=""></option>
          <?php  $sql2 = "SELECT DISTINCT
								mst_financechartofaccounts.intId,
								mst_financechartofaccounts.strCode,
								mst_financechartofaccounts.strName
							FROM
								mst_financechartofaccounts
									Inner Join mst_financesupplieractivate ON mst_financechartofaccounts.intId = mst_financesupplieractivate.intChartOfAccountId
							WHERE
								mst_financesupplieractivate.intCompanyId =  '$company'
							GROUP BY  mst_financechartofaccounts.intId 
							order by strCode ";
						$result2 = $db->RunQuery($sql2);
						while($row2=mysqli_fetch_array($result2))
						{
						   echo "<option value=\"".$row2['intId']."\">".$row2['strCode']."-".$row2['strName']."</option>";
						}
        ?> 
        </select>
      </span></td>
     
    </tr>
    <!-- ================================ --> 
           
    <tr>
      <td width="20" class="normalfnt">&nbsp;</td>
      <td width="143" class="normalfnt">Supplier <span class="compulsoryRed">*</span></td>
      
    <!-- ========Edited by dulakshi 2013.03.18============== --> 
	  <td width="238"><span class="normalfnt">
	       <select name="cboSupplier" id="cboSupplier"  style="width:96%" class="validate[required]">
	       	<option value=""></option>
	       		<?php
					$sql = "SELECT
								mst_supplier.intId,
								mst_supplier.strName
							FROM
								mst_financesupplieractivate
								Inner Join mst_supplier ON mst_supplier.intId = mst_financesupplieractivate.intSupplierId
							WHERE
								mst_supplier.intStatus =  '1' AND
								mst_financesupplieractivate.intCompanyId =  '$company'
							ORDER BY
								mst_supplier.strName ASC";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$customer)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
	       	
	       </select>
	  </span></td>	  
	  
    <!-- ================================== --> 
      
      <td width="146"><span class="normalfnt">Date <span class="compulsoryRed">*</span></span></td>
      <td width="315" bgcolor="#FFFFFF" class=""><input name="txtDate" type="text" value="<?Php echo ($date==''?date("Y-m-d"):$date) ?>" class="validate[required]  txtbox" id="txtDate" style="width:98px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" onblur="backDateExisting(this,'Advance Payment');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
      <td width="18">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>           
      <td><span class="normalfnt">Currency <span class="compulsoryRed">*</span></span></td>
      <td><span class="normalfnt">
        <select name="cboCurrency" id="cboCurrency" style="width:100px"  class="validate[required]">
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_financecurrency.intId,
							mst_financecurrency.strCode
							FROM mst_financecurrency";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$currency)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strCode']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strCode']."</option>";	
					}
				?>
          </select>
        </span></td>
      <td><span class="normalfnt">Rate<span class="compulsoryRed"> *</span></span></td>
      <td><span class="normalfnt"><span class="normalfntMid">
        <input type="radio" name="radio" id="exchSelling" value="" checked="checked"/>
        Selling
        <input type="radio" name="radio" id="exchBuying" value="" />
        Buying 
        <input class="rdoRate" type="radio" name="radio" id="rdoAverage" value="" />
		Average
<input type="text" name="txtRate" id="txtRate" style="width:75px; text-align:right" disabled="disabled" value="<?php echo $rate ?>" class="validate[required,custom[number]]" />
        <input type="checkbox" name="chkEdit" id="chkEdit" />
        </span></span></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="normalfnt">Pay Amount</span></td>
      <td><span class="normalfntMid">
        <input type="text" name="txtRecvAmmount" id="txtRecvAmmount" style="width:100px; text-align:right" class="validate[required,custom[number]] normalfntBlue" value="<?php echo $recvAmmount ?>" readonly="readonly" />
      </span></td>
      <td class="normalfnt">&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong class="normalfnt">Payment Method</strong></td>
      <td><span class="normalfntMid">
        <select name="cboPaymentMethod" id="cboPaymentMethod" style="width:150px"  class="validate[required]">
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_financepaymentsmethods.intId,
							mst_financepaymentsmethods.strName
							FROM mst_financepaymentsmethods
							WHERE
							mst_financepaymentsmethods.intStatus =  '1'";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$payMethod)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
					?>
        </select>
      </span></td>
      <td><strong class="normalfnt">Bank Reference Number</strong></td>
      <td><span class="normalfntMid">
        <input type="text" name="txtRafNo" id="txtRafNo" style="width:105px" value="<?php echo $refNo; ?>" />
      </span></td>
      <td>&nbsp;</td>
    </tr>
    <tr id="rwChequeDetails" style="display:none">
      <td>&nbsp;</td>
      <td><strong class="normalfnt">Reference</strong> <strong class="normalfnt">Date</strong></td>
      <td><input name="txtRefDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="txtbox" id="txtRefDate" style="width:98px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /> 
        <input type="checkbox" name="chkPosted" id="chkPosted" />
        <span class="normalfnt">Posted</span></td>
      <td><strong class="normalfnt">Reference Organization</strong></td>
      <td><span class="normalfntMid">
        <input type="text" name="txtRefOrganization" id="txtRefOrganization" style="width:195px" value="<?php echo $refOrganization ?>" />
      </span></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="normalfnt">Memo</span></td>
      <td colspan="2"><textarea name="txtRemarks" id="txtRemarks" cols="45" rows="2"><?php echo $remarks ?></textarea></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
      </table>
      </td>
      </tr>
      <tr>
      <td colspan="2" align="center">
      </td>
      </tr>
      <tr>
      <td colspan="2" align="center">
      <table width="100%" class="tableBorder_allRound">
      <tr>
      <td align="center" bgcolor="#FFFFFF" class="normalfntMid"><strong>Credit Account</strong></td>
      </tr>
      </table>
      </td>
      </tr>
      </tr>
            <tr>
      <td colspan="2" align="right"><img src="../../../../images/Tadd.jpg" width="92" height="24" id="insertRow" /></td>
      </tr>
    <tr>
      <td colspan="2"><table width="100%">
        <tr>
          <td>
                  <div style="overflow:scroll;width:900px;height:150px;" id="divGrid"><table width="100%" id="tblCreditAccount" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
        <tr class="">
          <td width="22" bgcolor="#FAD163" class="normalfntMid">Del</td>
          <td width="211"  height="24" bgcolor="#FAD163" class="normalfntMid"><strong>Account <span class="compulsoryRed">*</span></strong></td>
          <td width="135"  bgcolor="#FAD163" class="normalfntMid"><strong>Amount <span class="compulsoryRed">*</span></strong></td>
          <td width="399" bgcolor="#FAD163" class="normalfntMid"  ><strong>Memo</strong></td>
          <td width="100" bgcolor="#FAD163" class="normalfntMid"  ><strong><span class="compulsoryRed">*</span> Cost Center</strong></td>
        </tr>
        
        <?php
			            $sqlm = "SELECT
						fin_supplier_advancepayment_details.intAccountId,
						fin_supplier_advancepayment_details.dblAmount,
						fin_supplier_advancepayment_details.strMemo,
						fin_supplier_advancepayment_details.intDimension
						FROM fin_supplier_advancepayment_details
						WHERE
						fin_supplier_advancepayment_details.strReceiptNo =  '$receiptNo'";
				$resultm = $db->RunQuery($sqlm);
				$existingRws=0;
				$totAmnt=0;
				while($rowm=mysqli_fetch_array($resultm))
				{
					$existingRws++;
					$accountId=$rowm['intAccountId'];
					$ammount=$rowm['dblAmount'];
					$memo=$rowm['strMemo'];
					$dimention=$rowm['intDimension'];
					$totAmnt+=$ammount;
		?>
        
<tr class="normalfnt">
          <td bgcolor="#FFFFFF" class="normalfntMid"><img src="../../../../images/del.png" width="15" height="15" class="delImg" /></td>
          <td bgcolor="#FFFFFF" class="normalfntMid"><select name="cboAccounts" id="cboAccounts" style="width:210px" class="duplicate">
                  <?php
					$sql = "SELECT
							mst_financechartofaccounts.intId,
							mst_financechartofaccounts.strCode,
							mst_financechartofaccounts.strName,
							mst_financechartofaccounts.intStatus,
							mst_financechartofaccounts_companies.intCompanyId,
							mst_financechartofaccounts.strType
							FROM
							mst_financechartofaccounts
							Inner Join mst_financechartofaccounts_companies ON mst_financechartofaccounts.intId = mst_financechartofaccounts_companies.intChartOfAccountId
							WHERE
							mst_financechartofaccounts.intStatus =  '1' AND
							mst_financechartofaccounts.intFinancialTypeId =  '24' AND
							mst_financechartofaccounts_companies.intCompanyId =  '$company' AND
							mst_financechartofaccounts.strType =  'Posting'
							ORDER BY strCode";//type=Bank
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$accountId)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strCode']."-".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strCode']."-".$row['strName']."</option>";	
					}
					?>
          </select></td>
          <td bgcolor="#FFFFFF"><input name="txtAmmount" type="text" id="txtAmmount" style="width:135px; text-align:right" class="validate[required,custom[number]] calTot" value="<?php echo $ammount ?>" /></td>
          <td  bgcolor="#FFFFFF"><input type="text" name="txtMemo" id="txtMemo" style="width:400px" value="<?php echo $memo ?>" /></td>
          <td  bgcolor="#FFFFFF" class="normalfntMid"><select name="cboDimention" id="cboDimention" class="validate[required]" style="width:100px">
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_financedimension.intId,
							mst_financedimension.strName
							FROM mst_financedimension
							WHERE
							mst_financedimension.intStatus =  '1'
							";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$dimention)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
					?>
              </select>
          </td>
        </tr>        
        <?php
		}
		if($existingRws==0){
		?>
        <tr class="normalfnt">
          <td bgcolor="#FFFFFF" class="normalfntMid"><img src="../../../../images/del.png" width="15" height="15" class="delImg" /></td>
          <td bgcolor="#FFFFFF" class="normalfntMid"><select name="cboAccounts" id="cboAccounts" style="width:210px" class="duplicate">
                  <?php
					$sql = "SELECT
							mst_financechartofaccounts.intId,
							mst_financechartofaccounts.strCode,
							mst_financechartofaccounts.strName,
							mst_financechartofaccounts.intStatus,
							mst_financechartofaccounts_companies.intCompanyId,
							mst_financechartofaccounts.strType
							FROM
							mst_financechartofaccounts
							Inner Join mst_financechartofaccounts_companies ON mst_financechartofaccounts.intId = mst_financechartofaccounts_companies.intChartOfAccountId
							WHERE
							mst_financechartofaccounts.intStatus =  '1' AND
							mst_financechartofaccounts.intFinancialTypeId =  '24' AND
							mst_financechartofaccounts_companies.intCompanyId =  '$company' AND
							mst_financechartofaccounts.strType =  'Posting'
							ORDER BY strCode";//type=Bank
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strCode']."-".$row['strName']."</option>";	
					}
					?>
          </select></td>
          <td bgcolor="#FFFFFF"><input name="txtAmmount" type="text" id="txtAmmount" style="width:135px; text-align:right" class="validate[required,custom[number]] calTot" /></td>
          <td  bgcolor="#FFFFFF"><input type="text" name="txtMemo" id="txtMemo" style="width:400px" /></td>
          <td  bgcolor="#FFFFFF" class="normalfntMid"><select name="cboDimention" id="cboDimention" class="validate[required]" style="width:100px">
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_financedimension.intId,
							mst_financedimension.strName
							FROM mst_financedimension
							WHERE
							mst_financedimension.intStatus =  '1'
							";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
					?>
              </select>
          </td>
        </tr>
        <?php
		}
		?>
      </table>
      </div>
            </td>
          </tr>
          <tr>
          <td colspan="2">
          <table width="476">
                    <tr>
                      <td width="227" align="right" class="normalfntRight">Total </td>
                      <td width="135"><input name="txtTotal" type="text" disabled="disabled" id="txtTotal" style="width:135px; text-align:right" value="<?php echo $totAmnt; ?>" /></td>
                      <td width="98" class="normalfnt">&nbsp;</td>
                    </tr>
                  </table>
          </td>
          </tr>
        </table></td>
    </tr>
      <tr>
        <td colspan="2">
          <table width="100%">
            <tr>
              <td width="100%" height="34" class="tableBorder_allRound"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                <tr>
                  <td width="100%" align="center" bgcolor=""><img border="0" src="../../../../images/Tnew.jpg" alt="New" name="butNew" width="92" height="24" class="mouseover" id="butNew" tabindex="28"/><img src="../../../../images/Tsave.jpg" width="92" height="24" id="butSave" name="butSave"  class="mouseover" /><img style="display:none" border="0" src="../../../../images/Tprint.jpg" alt="Print" name="butPrint" width="92" height="24" class="mouseover" id="butPrint" tabindex="25"/><img border="0" src="../../../../images/Tdelete.jpg" alt="Delete" name="Delete" width="92" height="24"  class="mouseover" id="butDelete" tabindex="25"/><a href="../../../../main.php"><img src="../../../../images/Tclose.jpg" alt="Close" name="Close" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                </tr>
              </table></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    
</div>
</div>
</form>
</body>
</html>
// JavaScript Document
var amStatus = "Auto";

function functionList()
{
	//---------------------------------------------------
		amStatus = "Auto";
		//document.getElementById("chkAutoManual").style.display='none';
		document.getElementById("amStatus").style.display='none';
		$("#frmAdvancePayment #txtReceiptNo").attr("readonly","readonly");
	 //---------------------------------------------------
	if($('#frmAdvancePayment #cboSearch').val()=='')
	{
	//---------------------------------------------------
		existingMsgDate = "";
		amStatus = "Auto";
		//document.getElementById("chkAutoManual").style.display='';
		document.getElementById("amStatus").style.display='';
		$('#frmAdvancePayment #txtReceiptNo').removeClass('validate[required]');
		$("#frmAdvancePayment #txtReceiptNo").attr("readonly","readonly");
	//---------------------------------------------------
	}
}
$(document).ready(function() {
	
	// TODO: ===========Add by dulaskhi 2013.03.18===========  
	
    $("#frmAdvancePayment").validationEngine();
    $('#frmAdvancePayment #cboSupplier').focus();
		
    $("#insertRow").click(function(){
        insertRow();
    });
    //-------------------------------------
    $('.calTot').keyup(function(){
        calculateTotalVal();
    });
    //-------------------------------------
    $('.delImg').click(function(){
        var rowCount = document.getElementById('tblCreditAccount').rows.length;
        if(rowCount>2)
            $(this).parent().parent().remove();
        calculateTotalVal();
    });
    //-------------------------------------
    $('.duplicate').change(function(){
        checkForDuplicate();
    });
    //-------------------------------------
    //--------------------------------------------
		
    //permision for add 
    if(intAddx)
    {
        $('#frmAdvancePayment #butNew').show();
        $('#frmAdvancePayment #butSave').show();
		$('#frmAdvancePayment #butPrint').show();
    }
  
    //permision for edit 
    if(intEditx)
    {
        $('#frmAdvancePayment #butSave').show();
    //$('#frmAdvancePayment #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
		$('#frmAdvancePayment #butPrint').show();
    }
  
    //permision for delete
    if(intDeletex)
    {
        $('#frmAdvancePayment #butDelete').show();
    //$('#frmAdvancePayment #cboSearch').removeAttr('disabled');
    }
  
    //permision for view
    if(intViewx)
    {
    //$('#frmAdvancePayment #cboSearch').removeAttr('disabled');
    }
//===================================================================
 	$('#frmAdvancePayment #chkAutoManual').click(function(){
	  if($('#frmAdvancePayment #chkAutoManual').attr('checked'))
	  {
		  amStatus = "Auto";
		  $('#frmAdvancePayment #amStatus').val('Auto');
		  $('#frmAdvancePayment #txtReceiptNo').val('');
		  $("#frmAdvancePayment #txtReceiptNo").attr("readonly","readonly");
		  $('#frmAdvancePayment #txtReceiptNo').removeClass('validate[required]');
	  }
	  else
	  {
		  amStatus = "Manual";
		  $('#frmAdvancePayment #amStatus').val('Manual');
		  $('#frmAdvancePayment #txtReceiptNo').val('');
		  $("#frmAdvancePayment #txtReceiptNo").attr("readonly","");
		  $('#frmAdvancePayment #txtReceiptNo').focus();
		  $('#frmAdvancePayment #txtReceiptNo').addClass('validate[required]');
	  }
  });
//===================================================================
  
    //-------------------------------------------- 
    $('#frmAdvancePayment #cboCurrency').change(function(){
        var currency = $('#cboCurrency').val();
        var date = $('#txtDate').val();
        var url 		= "advancePayments-db-get.php?requestType=loadExchangeRates";
        var httpobj = $.ajax({
            url:url,
            dataType:'json',
            data:"currency="+currency+'&date='+date,
            async:false,
            success:function(json){

                document.getElementById("txtRate").value=json.sellingRate;
                document.getElementById("exchSelling").value=json.sellingRate;
                document.getElementById("exchBuying").value=json.buyingRate;
                document.getElementById("rdoAverage").value=((eval(json.buyingRate) + eval(json.sellingRate))/2).toFixed(4);
            //document.getElementById("txtRate").innerHTML=json.buyingRate;
            }
        });
		
    });
    //-------------------------------------------------------
    $('#frmAdvancePayment #exchSelling').click(function(){
        document.getElementById("txtRate").value=document.getElementById("exchSelling").value;
    //document.getElementById("txtRate").innerHTML=json.buyingRate;
    });
    //-------------------------------------------------------
    $('#frmAdvancePayment #exchBuying').click(function(){
        document.getElementById("txtRate").value=document.getElementById("exchBuying").value;
    });
    //-------------------------------------------------------------
    //-------------------------------------------------------
    $('#frmAdvancePayment #rdoAverage').click(function(){
        document.getElementById("txtRate").value=document.getElementById("rdoAverage").value;
    });
    //-------------------------------------------------------------
    $('#frmAdvancePayment #chkEdit').click(function(){
        if(document.getElementById("chkEdit").checked==true)
            $('#frmAdvancePayment #txtRate').removeAttr('disabled');
        else
            $('#frmAdvancePayment #txtRate').attr("disabled",true);

    });
    //------------------------------------------------------------
    $('#frmAdvancePayment #cboPaymentMethod').change(function(){
        var payMethod = $('#cboPaymentMethod').val();
        if(payMethod==2){
            document.getElementById("rwChequeDetails").style.display='';
        }
        else{
            document.getElementById("rwChequeDetails").style.display='none';
        }
    });
    //----------------------------------------------------------
    $('.calTot').keyup(function(){
        calculateTotalVal();
    });
    //-----------------------------------------------------------
    $('#frmAdvancePayment #butDelete').click(function(){
        var serial=URLEncode($('#frmAdvancePayment #txtReceiptNo').val());
        if($('#frmAdvancePayment #txtReceiptNo').val()=='')
        {
        //$('#frmBankDeposit #butDelete').validationEngine('showPrompt', 'Please select Deposit No.', 'fail');
        //	var t=setTimeout("alertDelete()",1000);	
        }
        else
        {
            var $supplierId=$('#frmAdvancePayment #cboSupplier').val()
            var val = $.prompt('Are you sure you want to delete "'+serial+'" ?',{
                buttons: {
                    Ok: true, 
                    Cancel: false
                },
                callback: function(v,m,f){
                    if(v)
                    {
                        var url = "advancePayments-db-set.php";
                        var httpobj = $.ajax({
                            url:url,
                            dataType:'json',
                            data:'requestType=delete&serialNo='+serial+'&supplier='+$supplierId,
                            async:false,
                            success:function(json){
												
                                $('#frmAdvancePayment #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
                                if(json.type=='pass')
                                {
                                    $('#frmAdvancePayment').get(0).reset();
                                    clearForm();
                                    loadCombo_search();
                                    var t=setTimeout("alertDelete()",1000);
                                    return;
                                }
                                var t=setTimeout("alertDelete()",3000);
                            }	 
                        });
                    }
                }
            });
			
        }
		
    });
    //-----------------------------------------------------------
 
    $('#frmAdvancePayment #butSave').click(function(){
	if(existingMsgDate == "")
	{	
        calculateTotalVal();
        var RecvedAmmount=parseFloat(document.getElementById("txtRecvAmmount").value).toFixed(2);
        var totAccQty=parseFloat(document.getElementById("txtTotal").value).toFixed(2);
	  
        var requestType = '';
        if ($('#frmAdvancePayment').validationEngine('validate'))   
        { 
            if(checkForDuplicate()==false){
                return false;  
            }
            if(RecvedAmmount!=totAccQty){
                alert('Recieved Ammount does not tally with total Account Ammount');
                return false;  
            }
            if(document.getElementById('chkPosted').checked==true)
                var posted = 1;
            else
                var posted = 0;

            var data = "requestType=save";
		
            data+="&serialNo="		+	URLEncode($('#cboSearch').val());
            data+="&supplier="	+	$('#cboSupplier').val();
            data+="&currency="	+	$('#cboCurrency').val();
            data+="&recvAmmount="			+	$('#txtRecvAmmount').val();
            data+="&invoiceNo="	+	URLEncode($('#txtPerfInvoiceNo').val()); // --> this is fnRefNo (Reference Number)
            data+="&paymentMethod="		+	$('#cboPaymentMethod').val();
            data+="&txtpaymentMethod="		+	$('#cboPaymentMethod').text();
            data+="&posted="	+	posted;
            data+="&remarks="			+	URLEncode($('#txtRemarks').val());
            data+="&date="		+	$('#txtDate').val();
            data+="&refDate="		+	$('#txtRefDate').val();
            data+="&rate="		+	$('#txtRate').val();
            data+="&refNo="	+	$('#txtRafNo').val();
            data+="&refOrganization=" +	$('#txtRefOrganization').val();
			data+="&amStatus="		+	amStatus;
			data+="&manualNo="		+	$('#txtReceiptNo').val();


            var rowCount = document.getElementById('tblCreditAccount').rows.length;
            if(rowCount==1){
                alert("No Accounts selected");
                return false;				
            }
            var row = 0;
			
            var arr="[";
			
            for(var i=1;i<rowCount;i++)
            {
					 
                var account = 	document.getElementById('tblCreditAccount').rows[i].cells[1].childNodes[0].value;
                var ammount = 	document.getElementById('tblCreditAccount').rows[i].cells[2].childNodes[0].value;
                var memo = 	document.getElementById('tblCreditAccount').rows[i].cells[3].childNodes[0].value;
                var dimention = 	document.getElementById('tblCreditAccount').rows[i].cells[4].childNodes[0].value;
					
                arr += "{";
                arr += '"account":"'+		account +'",' ;
                arr += '"ammount":"'+		ammount +'",' ;
                arr += '"memo":"'+		URLEncode(memo) +'",' ;
                arr += '"dimention":"'+		dimention  +'"' ;
                arr +=  '},';
						
            }
            arr = arr.substr(0,arr.length-1);
            arr += " ]";
			
            data+="&arr="	+	arr;
            ///////////////////////////// save main infomations /////////////////////////////////////////
            var url = "advancePayments-db-set.php";
            var obj = $.ajax({
                url:url,
			
                dataType: "json",  
                data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
                //data:'{"requestType":"addsampleInfomations"}',
                async:false,
                success:function(json){

                    $('#frmAdvancePayment #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
                    if(json.type=='pass')
                    {
                        //$('#frmSampleInfomations').get(0).reset();
                        //saveImage(json.sampleNo,json.sampleYear,json.revisionNo);
                        loadCombo_search();
                        var t=setTimeout("alertx()",1000);
                        $('#txtReceiptNo').val(json.serialNo);
                        $('#cboSearch').val(json.serialNo);
                        //	$('#txtRetSupYear').val(json.year);
                        return;
                    }
                },
                error:function(xhr,status){
					
                    $('#frmAdvancePayment #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
                    var t=setTimeout("alertx()",3000);
                //function (xhr, status){errormsg(status)}
                }		
            });
        }
	}
	else
	{
		$('#frmAdvancePayment #butSave').validationEngine('showPrompt', existingMsgDate,'fail');
		var t=setTimeout("alertx()",5000);
	}
    });
   

    //--------------refresh the form----------
    $('#frmAdvancePayment #butNew').click(function(){
//---------------------------------------------------
	existingMsgDate = "";
	amStatus = "Auto";
	$("#frmAdvancePayment #txtReceiptNo").attr("readonly","readonly");
	$('#frmAdvancePayment #chkAutoManual').attr('checked')
	$("#frmAdvancePayment #chkAutoManual").attr("disabled","");
	//document.getElementById("chkAutoManual").style.display='';
	document.getElementById("amStatus").style.display='';
	$('#frmAdvancePayment #txtReceiptNo').removeClass('validate[required]');
//---------------------------------------------------
        $('#frmAdvancePayment').get(0).reset();
        clearRows();
        $('#frmAdvancePayment #txtReceiptNo').val('');
        $('#frmAdvancePayment #cboSearch').val('');
        $('#frmAdvancePayment #cboSupplier').val('');
        $('#frmAdvancePayment #cboCurrency').val('');
        $('#frmAdvancePayment #txtRecvAmmount').val('');
        $('#frmAdvancePayment #txtPerfInvoiceNo').val('');
        $('#frmAdvancePayment #cboPaymentMethod').val('');
        $('#frmAdvancePayment #chkPosted').val('');
        $('#frmAdvancePayment #txtRemarks').val('');
        $('#frmAdvancePayment #txtDate').val('');
        $('#frmAdvancePayment #txtRefDate').val('');
        $('#frmAdvancePayment #txtRate').val('');
        $('#frmAdvancePayment #txtRafNo').val('');
        $('#frmAdvancePayment #txtRefOrganization').val('');
        $('#frmAdvancePayment #txtTotal').val('');
        //$('#frmAdvancePayment #cboSupplier').focus();
		$('#frmAdvancePayment #txtPerfInvoiceNo').focus();
		
        var currentTime = new Date();
        var month = currentTime.getMonth()+1 ;
        var day = currentTime.getDate();
        var year = currentTime.getFullYear();
        if(day<10)
            day='0'+day;
        if(month<10)
            month='0'+month;
        d=year+'-'+month+'-'+day;
		
        $('#frmAdvancePayment #txtDate').val(d);
        $('#frmAdvancePayment #txtRefDate').val(d);
    });
    //----------------------------	
    $('.delImg').click(function(){
        var rowCount = document.getElementById('tblCreditAccount').rows.length;
        if(rowCount>2)
            $(this).parent().parent().remove();
    });


    //-----------------------------------
    $('#butReport').click(function(){
        if($('#txtRetSupNo').val()!=''){
            window.open('../listing/rptAdvancePayments.php?retSupNo='+$('#txtRetSupNo').val()+'&year='+$('#txtRetSupYear').val());	
        }
        else{
            alert("There is no Return Note to view");
        }
    });
    //----------------------------------	
    $('#butConfirm').click(function(){
        if($('#txtRetSupNo').val()!=''){
            window.open('../listing/rptAdvancePayments.php?retSupNo='+$('#txtRetSupNo').val()+'&year='+$('#txtRetSupYear').val()+'&approveMode=1');	
        }
        else{
            alert("There is no Return Note to confirm");
        }
    });
    //-----------------------------------------------------
    $('#butClose').click(function(){
        //load('main.php');	
        });
    //--------------refresh the form-----------------------
    $('#frmAdvancePayment #butNew').click(function(){
        $('#frmAdvancePayment').get(0).reset();
        clearRows();
        $('#frmAdvancePayment #txtReceiptNo').val('');
        $('#frmAdvancePayment #cboSearch').val('');
        $('#frmAdvancePayment #cboSupplier').val('');
        $('#frmAdvancePayment #cboCurrency').val('');
        $('#frmAdvancePayment #txtRecvAmmount').val('');
        $('#frmAdvancePayment #txtPerfInvoiceNo').val('');
        $('#frmAdvancePayment #cboPaymentMethod').val('');
        $('#frmAdvancePayment #chkPosted').val('');
        $('#frmAdvancePayment #txtRemarks').val('');
        $('#frmAdvancePayment #txtDate').val('');
        $('#frmAdvancePayment #txtRefDate').val('');
        $('#frmAdvancePayment #txtRate').val('');
        $('#frmAdvancePayment #txtRafNo').val('');
        $('#frmAdvancePayment #txtRefOrganization').val('');
        $('#frmAdvancePayment #txtTotal').val('');
        //$('#frmAdvancePayment #cboSupplier').focus();
		$('#frmAdvancePayment #txtPerfInvoiceNo').focus();
		
        var currentTime = new Date();
        var month = currentTime.getMonth()+1 ;
        var day = currentTime.getDate();
        var year = currentTime.getFullYear();
        if(day<10)
            day='0'+day;
        if(month<10)
            month='0'+month;
        d=year+'-'+month+'-'+day;
		
        $('#frmAdvancePayment #txtDate').val(d);
        $('#frmAdvancePayment #txtRefDate').val(d);
    });
//-----------------------------------------------------

	$('#frmAdvancePayment #butPrint').click(function(){
		if($('#frmAdvancePayment #txtReceiptNo').val()=='')
		{
			$('#frmAdvancePayment #butPrint').validationEngine('showPrompt', 'Please select Voucher.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var myurl = 'listing/advancePaymentDetails.php?id='+URLEncode($('#frmAdvancePayment #txtReceiptNo').val());
    		window.open(myurl); 
		}
	});
});

//----------end of ready -------------------------------

//-------------------------------------------------------
function loadPopup()
{
    popupWindow3('1');
    var grnNo = $('#txtGrnNo').val();
    $('#popupContact1').load('advancePaymentsPopup.php?grnNo='+grnNo,function(){
        //checkAlreadySelected();
        $('#butAdd').click(addClickedRows);
        $('#butClose1').click(disablePopup);
        //-------------------------------------------- 
        $('#frmAdvancePaymentPopup #cboMainCategory').change(function(){
            var mainCategory = $('#cboMainCategory').val();
            var url 		= "advancePayments-db-get.php?requestType=loadSubCategory&mainCategory="+mainCategory;
            var httpobj 	= $.ajax({
                url:url,
                async:false
            })
            document.getElementById('cboSubCategory').innerHTML=httpobj.responseText;
						
        });
        //-------------------------------------------- 
        $('#frmAdvancePaymentPopup #imgSearchItems').click(function(){
            var rowCount = document.getElementById('tblItemsPopup').rows.length;
            for(var i=1;i<rowCount;i++)
            {
                document.getElementById('tblItemsPopup').deleteRow(1);
            }
            var grnNo = $('#txtPopGrnNo').val();
            var mainCategory = $('#cboMainCategory').val();
            var subCategory = $('#cboSubCategory').val();
            var description = $('#txtItmDesc').val();
            var url 		= "advancePayments-db-get.php?requestType=loadItems";
            var httpobj = $.ajax({
                url:url,
                dataType:'json',
                data:"grnNo="+grnNo+"&mainCategory="+mainCategory+"&subCategory="+subCategory+"&description="+description,
                async:false,
                success:function(json){

                    var length = json.arrCombo.length;
                    var arrCombo = json.arrCombo;


                    for(var i=0;i<length;i++)
                    {
                        var itemId=arrCombo[i]['itemId'];	
                        var maincatId=arrCombo[i]['maincatId'];	
                        var subCatId=arrCombo[i]['subCatId'];	
                        var code=arrCombo[i]['code'];	
                        var itemName=arrCombo[i]['itemName'];	
                        var grnQty=arrCombo[i]['grnQty'];	
                        var stockBal=arrCombo[i]['stockBal'];	
                        var mainCatName=arrCombo[i]['mainCatName'];
                        var subCatName=arrCombo[i]['subCatName'];	
                        var Qty=arrCombo[i]['Qty'];
										
                        var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF" id=""><input type="checkbox" id="chkDisp" /></td>';
                        content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'">'+mainCatName+'</td>';
                        content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'">'+subCatName+'</td>';
                        content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'">'+code+'</td>';
                        content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'">'+itemName+'</td>';
                        content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" style="display:none" >'+grnQty+'</td>';
                        content +='<td align="center" bgcolor="#FFFFFF" id="'+Qty+'" style="display:none" >'+stockBal+'</td></tr>';

                        add_new_row('#frmAdvancePaymentPopup #tblItemsPopup',content);
                    }
                    checkAlreadySelected();

                }
            });
						
        });
        //-------------------------------------------------------
        $('#butAdd').click(addClickedRows);
        $('#butClose1').click(disablePopup);
    //-------------------------------------------------------
    });	
}//-------------------------------------------------------
function loadMain()
{
//alert(1);
/*	$("#butAddPendingDispatch").click(function(){
		popupWindow3('1');
		//$('#iframeMain1').attr('src','http://localhost/qpet/presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php?id='+$('#cboDispatchTo').val());
		$('#popupContact1').load('sampleDispatchPopup.php');	
		
	});*/
//$("#iframeMain1").contents().find("#butAdd").click(abc);
//$("#frmAdvancePaymentPopup").contents().find("#butAdd").click(abc);
//	$('#frmAdvancePaymentPopup #butClose').click(abc);
}

//-------------------------------------
function alertx()
{
    $('#frmAdvancePayment #butSave').validationEngine('hide')	;
}
function alertDelete()
{
    $('#frmAdvancePayment #butDelete').validationEngine('hide')	;
	$('#frmAdvancePayment #butPrint').validationEngine('hide') ;
}

//----------------------------------------------
function add_new_row(table,rowcontent){
    if ($(table).length>0){
        if ($(table+' > tbody').length==0) $(table).append('<tbody />');
        ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
    }
}
//----------------------------------------------- 
function clearRows()
{
    var rowCount = document.getElementById('tblCreditAccount').rows.length;

    for(var i=2;i<rowCount;i++)
    {
        document.getElementById('tblCreditAccount').deleteRow(1);
    }
    document.getElementById('tblCreditAccount').rows[1].cells[1].childNodes[0].value='';
    document.getElementById('tblCreditAccount').rows[1].cells[2].childNodes[0].value='';
    document.getElementById('tblCreditAccount').rows[1].cells[3].childNodes[0].value='';
    document.getElementById('tblCreditAccount').rows[1].cells[4].childNodes[0].value='';
}
//-----------------------------------------------------------------
function insertRow()
{
    var tbl = document.getElementById('tblCreditAccount');	
    var rows = tbl.rows.length;
    //tbl.rows[1].cells[5].childNodes[0].nodeValue;
    tbl.insertRow(rows);
    tbl.rows[rows].innerHTML = tbl.rows[rows-1].innerHTML;
    document.getElementById('tblCreditAccount').rows[rows].cells[1].childNodes[0].value='';
    document.getElementById('tblCreditAccount').rows[rows].cells[2].childNodes[0].value='';
    document.getElementById('tblCreditAccount').rows[rows].cells[3].childNodes[0].value='';
    document.getElementById('tblCreditAccount').rows[rows].cells[4].childNodes[0].value='';
    //-------------------------------------
    $('.calTot').keyup(function(){
        calculateTotalVal();
    });
    //-------------------------------------
    $('.delImg').click(function(){
        var rowCount = document.getElementById('tblCreditAccount').rows.length;
        if(rowCount>2)
            $(this).parent().parent().remove();
        calculateTotalVal();
    });
    //-------------------------------------
    $('.duplicate').change(function(){
        checkForDuplicate();
    });
//-------------------------------------
}			
//------------------------------------------------------------------------------
function calculateTotalVal(){
    var rowCount = document.getElementById('tblCreditAccount').rows.length;
    var row = 0;
    var totQty=0;
	
    for(var i=1;i<rowCount;i++)
    {
        if(document.getElementById('tblCreditAccount').rows[i].cells[2].childNodes[0].value==''){
            var qty=0;
        }
        else{
            var qty= 	document.getElementById('tblCreditAccount').rows[i].cells[2].childNodes[0].value;
        }
        if(isNaN(qty)==true){
            qty=0; 
        }
        totQty+=parseFloat(qty);
    }
    totQty=totQty.toFixed(2);

    $('#txtTotal').val(totQty);
    $('#txtRecvAmmount').val(totQty);
    if(isNaN(totQty)==true){
        $('#txtTotal').val('0');
        $('#txtRecvAmmount').val('0');
    }

}
//--------------------------------------------------------------------------------
function checkForDuplicate(){
    var rowCount = document.getElementById('tblCreditAccount').rows.length;
    //var row=this.parentNode.parentNode.rowIndex;
    var k=0;
    for(var row=1;row<rowCount;row++){
        for(var i=1;i<row;i++){
            if(document.getElementById('tblCreditAccount').rows[i].cells[1].childNodes[0].value==document.getElementById('tblCreditAccount').rows[row].cells[1].childNodes[0].value)	{
                k=1;		
            }
        }
    }
    if(k==0){
        return true
    }
    else{
        alert("duplicate Accounts existing");
        return false
    }
}
//-----------------------------------------------------------------------------
function loadCombo_search()
{
    var url 	= "advancePayments-db-get.php?requestType=loadCombo";
    var httpobj = $.ajax({
        url:url,
        async:false
    })
    $('#frmAdvancePayment #cboSearch').html(httpobj.responseText);
}
//------------------------------------------------------------------------------
function clearForm(){
    $('#frmAdvancePayment').get(0).reset();
    clearRows();
    $('#frmAdvancePayment #txtReceiptNo').val('');
    $('#frmAdvancePayment #cboSearch').val('');
    $('#frmAdvancePayment #cboSupplier').val('');
    $('#frmAdvancePayment #cboCurrency').val('');
    $('#frmAdvancePayment #txtRecvAmmount').val('');
    $('#frmAdvancePayment #txtPerfInvoiceNo').val('');
    $('#frmAdvancePayment #cboPaymentMethod').val('');
    $('#frmAdvancePayment #chkPosted').val('');
    $('#frmAdvancePayment #txtRemarks').val('');
    $('#frmAdvancePayment #txtDate').val('');
    $('#frmAdvancePayment #txtRefDate').val('');
    $('#frmAdvancePayment #txtRate').val('');
    $('#frmAdvancePayment #txtRafNo').val('');
    $('#frmAdvancePayment #txtRefOrganization').val('');
    $('#frmAdvancePayment #txtTotal').val('');
    //$('#frmAdvancePayment #cboSupplier').focus();
	$('#frmAdvancePayment #txtPerfInvoiceNo').focus();
		
    var currentTime = new Date();
    var month = currentTime.getMonth()+1 ;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();
    if(day<10)
        day='0'+day;
    if(month<10)
        month='0'+month;
    d=year+'-'+month+'-'+day;
		
    $('#frmAdvancePayment #txtDate').val(d);
    $('#frmAdvancePayment #txtRefDate').val(d);
}
//----------------------------------------------------------------------------

//=========Add by dulakshi 2013.03.18===========
//====================Get Supplier List==============================
function getSupplierList()
{	
	$ledgerAccId = $('#frmAdvancePayment #cboLedgerAcc').val();
	$searchId = $('#frmAdvancePayment #cboSearch').val();
	
	var url = "advancePayments-db-get.php?requestType=loadSupplier&ledgerAcc="+$ledgerAccId;
	var httpobj = $.ajax({url:url,async:false})
	
	if($searchId == '')
		{
			$('#frmAdvancePayment #cboSupplier').html(httpobj.responseText);
		}
	
	
	//$('#frmAdvancePayment #cboSupplier').html(httpobj.responseText);
}
<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	$company 	= $_SESSION['headCompanyId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	///
	/////////// type of print load part /////////////////////
	if($requestType=='loadCombo')
	{
		$sql = "SELECT DISTINCT
				fin_supplier_advancepayment_header.strReceiptNo,
				mst_supplier.strName,
				IFNULL(CONCAT(' - ',fin_supplier_advancepayment_header.strPerfInvoiceNo),'') AS refNo
				FROM
				fin_supplier_advancepayment_header
				Inner Join mst_supplier ON fin_supplier_advancepayment_header.intSupplier = mst_supplier.intId
				WHERE
				fin_supplier_advancepayment_header.intStatus =  '1' AND
				fin_supplier_advancepayment_header.intCompanyId =  '$company'
				order by 
				fin_supplier_advancepayment_header.strReceiptNo desc
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['strReceiptNo']."\">".$row['strReceiptNo'].$row['refNo']." (".$row['strName'].") "."</option>";
		}
		echo $html;
	}
	else if($requestType=='loadExchangeRates')
	{
		$currency  = $_REQUEST['currency'];
		$date  = $_REQUEST['date'];
		$sql = "SELECT
				mst_financeexchangerate.dblBuying, 
				mst_financeexchangerate.dblSellingRate  
				FROM mst_financeexchangerate
				WHERE
				mst_financeexchangerate.intCurrencyId =  '$currency' and mst_financeexchangerate.dtmDate =  '$date'";
		$result = $db->RunQuery($sql);
		
		$response['sellingRate'] = '';
		$response['buyingRate'] = '';
		while($row=mysqli_fetch_array($result))
		{
			$response['sellingRate'] = $row['dblSellingRate'];
			$response['buyingRate'] = $row['dblBuying'];
		}
		echo json_encode($response);
	}
	
	//===========Add by dulakshi 2013.03.18=========
	else if($requestType=='loadSupplier')
	{
		$ledAcc  = $_REQUEST['ledgerAcc'];		
							
		$condition == "";
			if($ledAcc != "")
			{	
				$condition .= "AND mst_financesupplieractivate.intChartOfAccountId =  '$ledAcc'";			
			}			
		
		$sql = "SELECT
					mst_supplier.intId,
					mst_supplier.strName,
					mst_financesupplieractivate.intCompanyId
				FROM
					mst_supplier
					Inner Join mst_financesupplieractivate ON mst_supplier.intId = mst_financesupplieractivate.intSupplierId
				WHERE
					mst_supplier.intStatus = '1' AND					
					mst_financesupplieractivate.intCompanyId = '$company'" .$condition . " order by mst_supplier.strName";
			//
		
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
	}
	//==============================================
	
	
?>
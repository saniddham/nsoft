// JavaScript Document
var toBePaidAmount = '';
function functionList()
{
	if(recRefNo!='')
	{
		$('#frmSupplierGainLoss #cboSearch').val(recRefNo);
		$('#frmSupplierGainLoss #cboSearch').change();
	}
}
var rows = 1;
function insertRow()
{
	var tbl = document.getElementById('tblMainGrid2');	
	rows = tbl.rows.length;
	tbl.insertRow(rows);
	tbl.rows[rows].innerHTML = tbl.rows[rows-1].innerHTML;
//	loadJs();
}
$(document).ready(function() {
	
	var id = '';
	$("#frmSupplierGainLoss").validationEngine();
	$('#frmSupplierGainLoss #cboSupplier').focus();

$('.delImg').live('click',function(){
	var rowId = $(this).parent().parent().parent().find('tr').length;
	if(rowId!=2)
	$(this).parent().parent().remove();
	finalAmount();
});

$('.delImg').css('cursor', 'pointer');

  //permision for add 
  if(intAddx)
  {
 	$('#frmSupplierGainLoss #butNew').show();
	$('#frmSupplierGainLoss #butSave').show();
  }
  //permision for edit 
  if(intEditx)
  {
  	$('#frmSupplierGainLoss #butSave').show();
	$('#frmSupplierGainLoss #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
  //permision for delete
  if(intDeletex)
  {
  	$('#frmSupplierGainLoss #butDelete').show();
	$('#frmSupplierGainLoss #cboSearch').removeAttr('disabled');
  }
  //permision for view
  if(intViewx)
  {
	$('#frmSupplierGainLoss #cboSearch').removeAttr('disabled');
  }
  
  $('#frmSupplierGainLoss #chkEdit').click(function(){
	  if($('#frmSupplierGainLoss #chkEdit').attr('checked'))
	  {
		  $("#frmSupplierGainLoss #txtRate").attr("readonly","");
		  $('#frmSupplierGainLoss #txtRate').focus();
	  }
	  else
	  {
		  $('#frmSupplierGainLoss #txtRate').val('');
		  $("#frmSupplierGainLoss #txtRate").attr("readonly","readonly");
		  $('#frmSupplierGainLoss #cboCurrency').change();
	  }
  });

$(".checkRow").live('click',function(){
if($(this).attr("checked") == true)
{
	$(this).parent().parent().addClass("highlight");
	receivedAmount();
}
else
{
	$(this).parent().parent().removeClass("highlight");
	receivedAmount();
}
});

///////////////////////////get supplier invoice////////////////////
$('#cboSupplier').change(function(){
	var url = "supplierGainLoss-db-get.php?requestType=getInvoice&supplierId="+$(this).val();
	var obj = $.ajax({url:url,async:false});
	document.getElementById('allInvoice').innerHTML=obj.responseText;
});
////////////////////////////////////////////////////////////////////

//------------------------------------------------------------
   $('#frmSupplierGainLoss #cboPaymentsMethods').change(function(){
	var payMethod = $('#cboPaymentsMethods').val();
	if(payMethod==2)
	{
		document.getElementById("rwChequeDetails").style.display='';
	}
	else
	{
		document.getElementById("rwChequeDetails").style.display='none';
	}
  });
//----------------------------------------------------------

///////////////////////////get G/L amount//////////////////////////
$('.glAccount').live('change',function(){
	$('#txtAccAmount').val($('#txtRecAmount').val());
});
//////////////////////////////////////////////////////////////////

////////////////////////get exchange rate//////////////////////////
$('#cboCurrency').change(function(){
	var url = "supplierGainLoss-db-get.php?requestType=getExchangeRate&currencyId="+$(this).val()+'&exchangeDate='+$('#txtDate').val();
	var obj = $.ajax({url:url,dataType:'json',success:function(json){
		
		$('#rdoBuying').val(json.buyingRate);
		$('#rdoSelling').val(json.sellingRate);
		$('#rdoSelling').click();
		},async:false});
});
///////////////////////////////////////////////////////////////////
$('.rdoRate').click(function(){
  $('#txtRate').val($(this).val());
});
//save button click event
$('#frmSupplierGainLoss #butSave').click(function(){
if(existingMsgDate == "")
{
//-------------------------------------------------------------------
	var receiptNO = "";
	var receiveAmount = "";
	var invoiceAmount = "";
			
 value="[ ";
	$('#tblMainGrid1 tr:not(:first)').each(function(){
		if ($(this).find('.checkRow').attr('checked')) 
		{
			receiptNo		= $(this).find(".refNo").attr('id');
			receiveAmount 	= $(this).find(".recAmount").html();
			invoiceAmount 	= $(this).find(".invAmount").html();	
			
			value += '{ "receiptNo":"'+receiptNo+'", "receiveAmount": "'+receiveAmount+'", "invoiceAmount": "'+invoiceAmount+'"},';
		}
	});
	
	value = value.substr(0,value.length-1);
	value += " ]";
//---------------------------------------------------------------------------
	var accId = "";
	var accAmount = "";
	var memo = "";
	var dimension = "";
			
 accValue="[ ";
	$('#tblMainGrid2 tr:not(:first)').each(function(){
		
		accId		= $(this).find(".glAccount").val();
		accAmount 	= $(this).find(".accAmount").val();
		memo 		= $(this).find(".memo").val();
		dimension 	= $(this).find(".dimension").val();
		
	accValue += '{ "accId":"'+accId+'", "accAmount": "'+accAmount+'", "memo": "'+memo+'", "dimension": "'+dimension+'"},';
	});
	
	accValue = accValue.substr(0,accValue.length-1);
	accValue += " ]";
//---------------------------------------------------------------------------
	var requestType = '';
	if ($('#frmSupplierGainLoss').validationEngine('validate'))
    {
		//showWaiting();
		if(value != '[ ]')
		{
			if((eval($('#txtRecAmount').val()) - eval($('#txtAccAmount').val()))==0)
			{
				$('#frmSupplierGainLoss #cboSupplier').attr("disabled","");
				if($('#txtNo').val()=='')
					requestType = 'add';
				else
					requestType = 'edit';
				
				var url = "supplierGainLoss-db-set.php";
				var obj = $.ajax({
					url:url,
					dataType: "json",
					type:'post',  
					data:$("#frmSupplierGainLoss").serialize()+'&requestType='+requestType+'&cboSearch='+id+'&glDetail='+value+'&glAccDetail='+accValue,
					async:false,
					
					success:function(json){
							$('#frmSupplierGainLoss #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
							if(json.type=='pass')
							{
								//$('#frmSupplierGainLoss').get(0).reset(); hideWaitng();
								$('#frmSupplierGainLoss #cboSupplier').attr("disabled","disabled");
								var t=setTimeout("alertx()",1000);
								$('#txtNo').val(json.GLNo);
								loadCombo_frmSupplierGainLoss();
								return;
							}
							var t=setTimeout("alertx()",3000);
						},
					error:function(xhr,status){
						$('#frmSupplierGainLoss #cboSupplier').attr("disabled","disabled");
							
							$('#frmSupplierGainLoss #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
							var t=setTimeout("alertx()",3000);
						}		
					});
			}
			else
			{
				$('#frmSupplierGainLoss #butSave').validationEngine('showPrompt', 'You cannot allow this process untill received  amount and total amount are same','fail');
				var t=setTimeout("alertx()",5000);
			}
		}
		else
		{
			$('#frmSupplierGainLoss #butSave').validationEngine('showPrompt', 'You cannot allow this process!','fail');
			var t=setTimeout("alertx()",5000);
		}
	}
}
else
{
	$('#frmSupplierGainLoss #butSave').validationEngine('showPrompt', existingMsgDate,'fail');
	var t=setTimeout("alertx()",5000);
}
});
/////////////////////////////////////////////////////
//// load invoice details //////////////////////////
/////////////////////////////////////////////////////
$('#frmSupplierGainLoss #cboSearch').click(function(){
   $('#frmSupplierGainLoss').validationEngine('hide');
});
$('#frmSupplierGainLoss #cboSearch').change(function(){
existingMsgDate = "";
$('#frmSupplierGainLoss').validationEngine('hide');
var url = "supplierGainLoss-db-get.php";
if($('#frmSupplierGainLoss #cboSearch').val()=='')
{
	document.getElementById('allInvoice').innerHTML = "";
		$('#tblMainGrid2 >tbody >tr').each(function(){
			if($(this).index()!=0 && $(this).index()!=1 )
			{
				$(this).remove();
			}
		});
	$('#frmSupplierGainLoss').get(0).reset();return;	
}
$('#txtNo').val($(this).val());
var httpobj = $.ajax({
	url:url,
	dataType:'json',
	data:'requestType=loadDetails&id='+URLEncode($(this).val()),
	async:false,
	success:function(json)
	{
		//json  = eval('('+json+')');
		$('#frmSupplierGainLoss #cboSupplier').val(json.supplier);
		$('#frmSupplierGainLoss #cboSupplier').attr("disabled","disabled");
		$('#frmSupplierGainLoss #txtDate').val(json.date);
		$('#frmSupplierGainLoss #txtRemarks').val(json.remark);
		
		//--------------------------------------------------
		$('#tblMainGrid1 >tbody >tr').each(function(){
			if($(this).index()!=0 && $(this).index()!=1 )
			{
				$(this).remove();
			}
		});
		document.getElementById('allInvoice').innerHTML = "";
		var tBodyDetail = "";

		if(json.detailVal!=null)
		{
			var rowId = $('#tblMainGrid1').find('tr').length;
			var tbl = document.getElementById('tblMainGrid1');
			rows = $('#tblMainGrid1').find('tr').length;
			for(var j=0;j<=json.detailVal.length-1;j++)
			{
				tBodyDetail	= json.detailVal[j].tBodyDetail;
				if(j != json.detailVal.length)
				{
					document.getElementById('allInvoice').innerHTML+=tBodyDetail;
				}
			}
			receivedAmount();
		}
		else
		{
			
		}
	   //--------------------------------------------------
	   //--------------------------------------------------
		$('#tblMainGrid2 >tbody >tr').each(function(){
			if($(this).index()!=0 && $(this).index()!=1 )
			{
				$(this).remove();
			}
		});
		var chartAcc 	= "";
		var amount 		= "";
		var memo		= "";
		var dimension	= "";
		if(json.detailAccVal!=null)
		{
			var rowId = $('#tblMainGrid2').find('tr').length;
			var tbl = document.getElementById('tblMainGrid2');
			rows = $('#tblMainGrid2').find('tr').length;
			for(var j=0;j<=json.detailAccVal.length-1;j++)
			{
				chartAcc	= json.detailAccVal[j].chartAcc;
				amount		= json.detailAccVal[j].amount;
				memo		= json.detailAccVal[j].memo;
				dimension	= json.detailAccVal[j].dimension;
				if(j != json.detailAccVal.length-1)
				{
					tbl.insertRow(rows);
					tbl.rows[rows].innerHTML = tbl.rows[rows-1].innerHTML;
					tbl.rows[rows].cells[1].childNodes[1].value = chartAcc;
					tbl.rows[rows].cells[2].childNodes[1].value = amount;
					tbl.rows[rows].cells[3].childNodes[1].value = memo;
					tbl.rows[rows].cells[4].childNodes[1].value = dimension;
				}
				else
				{
					tbl.rows[1].cells[1].childNodes[1].value = chartAcc;
					tbl.rows[1].cells[2].childNodes[1].value = amount;
					tbl.rows[1].cells[3].childNodes[1].value = memo;
					tbl.rows[1].cells[4].childNodes[1].value = dimension;
				}
			}
			finalAmount();
		}
		else
		{
			
		}
	   //--------------------------------------------------
	}
});
});
//////////// end of load details /////////////////

  	$('#frmSupplierGainLoss #butNew').click(function(){
		existingMsgDate = "";
		$('#frmSupplierGainLoss').get(0).reset();
		document.getElementById('allInvoice').innerHTML = "";
		$('#tblMainGrid2 >tbody >tr').each(function(){
			if($(this).index()!=0 && $(this).index()!=1 )
			{
				$(this).remove();
			}
		});
		$('#frmSupplierGainLoss #cboPaymentsMethods').change();//--->
		$('#frmSupplierGainLoss #cboSupplier').attr("disabled","");
		loadCombo_frmSupplierGainLoss();
		$('#frmSupplierGainLoss #cboSupplier').focus();
	});
	$('#frmSupplierGainLoss #butDelete').click(function(){
		if($('#frmSupplierGainLoss #cboSearch').val()=='')
		{
			$('#frmSupplierGainLoss #butDelete').validationEngine('showPrompt', 'Please select G/L Number.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmSupplierGainLoss #cboSearch option:selected').text()+'" ?',{
			buttons: { Ok: true, Cancel: false },
			callback: function(v,m,f){
			if(v)
			{
					var url = "supplierGainLoss-db-set.php";
					var httpobj = $.ajax({
						url:url,
						dataType:'json',
						data:'requestType=delete&cboSearch='+URLEncode($('#frmSupplierGainLoss #cboSearch').val()),
						async:false,
						success:function(json){
							
							$('#frmSupplierGainLoss #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
							
							if(json.type=='pass')
							{
								$('#frmSupplierGainLoss').get(0).reset();
								document.getElementById('allInvoice').innerHTML = "";
								$('#tblMainGrid2 >tbody >tr').each(function(){
									if($(this).index()!=0 && $(this).index()!=1 )
									{
										$(this).remove();
									}
								});
								$('#frmSupplierGainLoss #cboPaymentsMethods').change();//--->
								$('#frmSupplierGainLoss #cboSupplier').attr("disabled","");
								loadCombo_frmSupplierGainLoss();
								var t=setTimeout("alertDelete()",1000);return;
							}	
							var t=setTimeout("alertDelete()",3000);
						}	 
					});
			}
		}
			});	
		}
	});
});
////////////////////// calculation ////////////////////////////////
function receivedAmount()
{
	var receiveTotal = 0.0000;
	$(".gainLoss").each( function(){
		if($(this).parent().find('.checkRow').attr('checked'))
		{
			  receiveTotal += eval($(this).html()==''?0.0000:$(this).html());
		}
	});
	$('#txtRecAmount').val(receiveTotal.toFixed(4));
}
///////////////////////////////////////////////////////////////////
function loadCombo_frmSupplierGainLoss()
{
	var url 	= "supplierGainLoss-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmSupplierGainLoss #cboSearch').html(httpobj.responseText);
}
function alertx()
{
	$('#frmSupplierGainLoss #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmSupplierGainLoss #butDelete').validationEngine('hide') ;
}
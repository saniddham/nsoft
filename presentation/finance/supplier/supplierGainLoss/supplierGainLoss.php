<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];

$thisFilePath =  $_SERVER['PHP_SELF'];
include  "{$backwardseperator}dataAccess/permisionCheck.inc";
$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$companyId = $row['intCompanyId']; 
}
$receiptRefNo = $_REQUEST['id'];
// ======================Check Exchange Rate Updates========================
if($invoiceRefNo == "")
{
	$status = "Adding";
}
else
{
	$status = "Changing";
}
$currentDate = date("Y-m-d");

$sql = "SELECT COUNT(*) AS 'no'
		FROM
		mst_financeexchangerate
		WHERE
		mst_financeexchangerate.dtmDate =  '$currentDate'
		";
$result = $db->RunQuery($sql);
$row= mysqli_fetch_array($result);
if($row['no']==0)
	{
		$str =  "Please Update Exchange Rates Before ".$status." Received Payments .";
		$str .= $row['NameList'];
		$maskClass="maskShow";
	}
	else
	{
		$maskClass="maskHide";
	}
// =========================================================================
?>
<script type="application/javascript" >
var recRefNo = '<?php echo $receiptRefNo ?>';
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Supplier Gain-Loss</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/tblstyle.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script src="supplierGainLoss-js.js" type="text/javascript"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

<script src="../../commanFunctions/numberExisting-js.js" type="text/javascript"></script>

<link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css">

<style type="text/css">
.setProperty 
{
  width:80px;
  text-align:righ; 
  background-color:#FFF;
  border:none;
}
</style>

</head>

<body onLoad="functionList();">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<div id="divMask" class="<?php echo $maskClass?> mask"> <?php echo $str; ?></div>

<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<form id="frmSupplierGainLoss" name="frmSupplierGainLoss" method="post" action="supplierGainLoss-db-set.php" autocomplete="off">
<div align="center">
  <div class="trans_layoutL">
    <div class="trans_text">Supplier Gain-Loss</div>
<table width="100%">
      <tr>
      <td class="normalfnt" width="50%"><img src="../../../../images/fb.png" width="18" height="19" /></td>
      <td align="right" width="50%"><img src="../../../../images/ff.png" width="18" height="19" /></td>
    </tr>
    <tr>
    <td colspan="2" align="left">
    <table align="right" width="100%">
    <tr>
    <td width="47%" align="right"><span class="normalfnt">G/L Number</span>:</td>
      <td width="53%" align="left"><span class="normalfntMid">
        <select name="cboSearch" id="cboSearch"  style="width:195px" >
          <option value=""></option>
          <?php   $sql = "SELECT
							fin_supplier_gain_loss_header.strReferenceNo,
							fin_supplier_gain_loss_header.intGLNo
							FROM
							fin_supplier_gain_loss_header
							WHERE
							fin_supplier_gain_loss_header.intCompanyId =  '$companyId' AND
							fin_supplier_gain_loss_header.intDeleteStatus = '0'
							ORDER BY intGLNo DESC
							";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['strReferenceNo']."\">".$row['strReferenceNo']."</option>";
						}
          ?>
        </select>
      </span></td>
    </tr>
    <tr>
      <td align="right">&nbsp;</td>
      <td align="left">&nbsp;</td>
    </tr>
    </table>
    </td>  
    </tr>
    <tr>
      <td colspan="2"><table width="100%" class="tableBorder_allRound">
        <tr>
          <td class="normalfnt">&nbsp;</td>
          <td class="normalfnt">G/L Number</td>
          <td><span class="normalfnt">
            <input name="txtNo" type="text" readonly="readonly"class="normalfntRight" id="txtNo" style="width:180px; background-color:#F4FFFF;text-align:center; border:dotted; border-color:#F00" />
(Auto)</span></td>
          <td>&nbsp;</td>
          <td bgcolor="#FFFFFF" class="">&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td width="68" class="normalfnt">&nbsp;</td>
          <td width="134" class="normalfnt">Supplier <span class="compulsoryRed">*</span></td>
          <td width="241"><span class="normalfntMid">
            <select name="cboSupplier" id="cboSupplier"  style="width:211px" class="validate[required]" >
              <option value=""></option>
              <?php  $sql = "SELECT
								mst_supplier.intId,
								mst_supplier.strName,
								mst_financesupplieractivate.intCompanyId
								FROM
								mst_supplier
								Inner Join mst_financesupplieractivate ON mst_supplier.intId = mst_financesupplieractivate.intSupplierId
								WHERE
								mst_supplier.intStatus =  1 AND
								mst_financesupplieractivate.intCompanyId = '$companyId'
								order by strName
								";
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
								}
                   ?>
            </select>
          </span></td>
          <td width="158"><span class="normalfnt">Date <span class="compulsoryRed">*</span></span></td>
          <td width="240" bgcolor="#FFFFFF" class=""><input name="txtDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="validate[required]" id="txtDate" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" onblur="backDateExisting(this,'Supplier Gain-Loss');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
          <td width="39">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><span class="normalfnt">Remarks</span></td>
          <td colspan="2"><textarea name="txtRemarks" id="txtRemarks" cols="45" rows="2"></textarea></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
      </tr>
    <tr>
      <td colspan="2">
      <table width="100%">
      <tr>
      <td>
      <!--<div style="overflow:scroll;width:100%;height:140px;" id="divGrid">-->
		<table width="100%" id="tblMainGrid1" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
        <thead>
        <tr class="">
          <td width="22" bgcolor="#FAD163" class="normalfntMid">&nbsp;</td>
          <td width="148"  height="22" align="center" bgcolor="#FAD163" class="normalfntMid"><strong> Voucher  No.</strong></td>
          <td width="92"  bgcolor="#FAD163" class="normalfntMid"><strong>Date</strong></td>
          <td width="93" bgcolor="#FAD163" class="normalfntMid"  ><strong>Amount</strong></td>
          <td width="67" bgcolor="#FAD163" class="normalfntMid"  ><strong>Currency</strong></td>
          <td width="61" bgcolor="#FAD163" class="normalfntMid"  ><strong>Rate</strong></td>
          <td width="138" bgcolor="#FAD163" class="normalfntMid"  ><strong>Pay Amount LKR</strong></td>
          <td width="143" bgcolor="#FAD163" class="normalfntMid"  ><strong>Invoice Amount LKR</strong></td>
          <td width="110" bgcolor="#FAD163" class="normalfntMid"  ><strong>Gain / Loss LKR</strong></td>
          </tr>
          </thead>
          <tbody id="allInvoice" class="normalfnt" bgcolor="#FFFFFF">

         </tbody>
        </table>
        <!--</div>-->
      </td>
      </tr>
      </table>
      </td>
      <tr>
      <td colspan="2" align="right">
                <table width="100%">
                    <tr>
                      <td width="770" align="right" class="normalfntRight">Total </td>
                      <td width="110" align="right">
                      <input name="txtRecAmount" type="text" disabled="disabled" id="txtRecAmount" style="width:100%;text-align:right" /></td>
                    </tr>
                  </table>
      </td>
      </tr>
      <tr>
      <td colspan="2" align="center">
      <table width="100%" class="tableBorder_allRound">
      <tr>
      <td align="center" bgcolor="#FFFFFF" class="normalfntMid"><strong>Exchange Gain / Loss</strong></td>
      </tr>
      </table>
      </td>
      </tr>
      </tr>
    <tr>
      <td colspan="2"><table width="100%">
        <tr>
          <td>
           <table width="100%" id="tblMainGrid2" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
        <tr class="">
          <td width="19" bgcolor="#FAD163" class="normalfntMid">Del</td>
          <td width="210"  height="24" bgcolor="#FAD163" class="normalfntMid"><strong>Account <span class="compulsoryRed">*</span></strong></td>
          <td width="148"  bgcolor="#FAD163" class="normalfntMid"><strong>Amount <span class="compulsoryRed">*</span></strong></td>
          <td width="400" bgcolor="#FAD163" class="normalfntMid"  ><strong>Memo</strong></td>
          <td width="101" bgcolor="#FAD163" class="normalfntMid"  ><strong><span class="compulsoryRed">*</span> Cost Center</strong></td>
        </tr>
        <tr class="normalfnt">
          <td bgcolor="#FFFFFF" class="normalfntMid"><img src="../../../../images/del.png" width="15" height="15" /></td>
          <td bgcolor="#FFFFFF" class="normalfntMid">
          <select name="cboChartOfAcc" id="cboChartOfAcc" style="width:100%" class="validate[required] glAccount" >
            <option value=""></option>
            <?php  $sql2 = "SELECT
							mst_financechartofaccounts.intId,
							mst_financechartofaccounts.strCode,
							mst_financechartofaccounts.strName,
							mst_financechartofaccounts_companies.intCompanyId,
							mst_financechartofaccounts_companies.intChartOfAccountId
							FROM
							mst_financechartofaccounts
							Inner Join mst_financechartofaccounts_companies ON mst_financechartofaccounts_companies.intChartOfAccountId = mst_financechartofaccounts.intId
							WHERE
							intStatus = '1' AND  (intFinancialTypeId = '27' OR intFinancialTypeId = '29' OR intFinancialTypeId = '4') AND strType = 'Posting' AND intCompanyId = '$companyId'
							order by strCode

						";
						$result2 = $db->RunQuery($sql2);
						while($row2=mysqli_fetch_array($result2))
						{
						   echo "<option value=\"".$row2['intId']."\">".$row2['strCode']."-".$row2['strName']."</option>";
						}
        			?>
          </select></td>
          <td bgcolor="#FFFFFF">
          <input name="txtAccAmount" type="text" disabled="disabled" class="validate[required,custom[number]] accAmount" id="txtAccAmount" style="width:100%;text-align:right" /></td>
          <td  bgcolor="#FFFFFF">
          <input type="text" name="txtMemo" id="txtMemo" style="width:100%" class="memo" /></td>
          <td  bgcolor="#FFFFFF">
          <select name="cboDimension" class="validate[required] dimension" id="cboDimension"  style="width:100%;">
            <option value=""></option>
            <?php  $sql = "SELECT
									intId,
									strName
								FROM mst_financedimension
								WHERE
									intStatus = 1
								order by strName
								";
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
								}
                   ?>
          </select></td>
        </tr>
      </table>
            </td>
          </tr>
        </table></td>
    </tr>
      <tr>
        <td colspan="2">
          <table width="100%">
            <tr>
              <td width="100%" height="34" class="tableBorder_allRound"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                <tr>
<td width="100%" align="center" bgcolor=""><img style="display:none" border="0" src="../../../../images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:none" border="0" src="../../../../images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/><img style="display:none" border="0" src="../../../../images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="25"/><a href="../../../../main.php"><img  src="../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                </tr>
              </table></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
</div>
</div>
</form>
</body>
</html>
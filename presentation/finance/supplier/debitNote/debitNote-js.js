var pub_buyingRate 		= 0;
var pub_sellingRate		= 0;
var invStatus = '';
var amStatus = "Auto";

function functionList()
{
    invStatus = "onLoad";
    $('.taxGroup').change();
    $('#cboInvoiceNo').change();
	//---------------------------------------------------
		amStatus = "Auto";
		document.getElementById("chkAutoManual").style.display='none';
		document.getElementById("amStatus").style.display='none';
		$("#frmDebitNote #txtDebitNo").attr("readonly","readonly");
	//---------------------------------------------------
	if($('#frmDebitNote #cboSearch').val()=='')
	{
	//---------------------------------------------------
		existingMsgDate = "";
		amStatus = "Auto";
		document.getElementById("chkAutoManual").style.display='';
		document.getElementById("amStatus").style.display='';
		$('#frmDebitNote #txtDebitNo').removeClass('validate[required]');
		$("#frmDebitNote #txtDebitNo").attr("readonly","readonly");
	//---------------------------------------------------
	}
}

$(document).ready(function() {
	
    var id = '';
    $("#frmDebitNote").validationEngine();
    $('#frmDebitNote #cboSupplier').focus();

    //permision for add 
    if(intAddx)
    {
        $('#frmDebitNote #butNew').show();
        $('#frmDebitNote #butSave').show();
		$('#frmDebitNote #butPrint').show();
    }
    //permision for edit 
    if(intEditx)
    {
        $('#frmDebitNote #butSave').show();
        $('#frmDebitNote #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
		$('#frmDebitNote #butPrint').show();
    }
    //permision for delete
    if(intDeletex)
    {
        $('#frmDebitNote #butDelete').show();
        $('#frmDebitNote #cboSearch').removeAttr('disabled');
    }
    //permision for view
    if(intViewx)
    {
        $('#frmDebitNote #cboSearch').removeAttr('disabled');
    }
//===================================================================
 	$('#frmDebitNote #chkAutoManual').click(function(){
	  if($('#frmDebitNote #chkAutoManual').attr('checked'))
	  {
		  amStatus = "Auto";
		  $('#frmDebitNote #amStatus').val('Auto');
		  $('#frmDebitNote #txtDebitNo').val('');
		  $("#frmDebitNote #txtDebitNo").attr("readonly","readonly");
		  $('#frmDebitNote #txtDebitNo').removeClass('validate[required]');
	  }
	  else
	  {
		  amStatus = "Manual";
		  $('#frmDebitNote #amStatus').val('Manual');
		  $('#frmDebitNote #txtDebitNo').val('');
		  $("#frmDebitNote #txtDebitNo").attr("readonly","");
		  $('#frmDebitNote #txtDebitNo').focus();
		  $('#frmDebitNote #txtDebitNo').addClass('validate[required]');
	  }
  });
//===================================================================
  
    ///////////////////////////////////
    ///////get customer address ///////
    ///////////////////////////////////
    $('#cboSupplier').change(function(){
        invStatus = '';
        document.getElementById("rwInvType").style.display='none';        
        var url = "debitNote-db-get.php?requestType=getCustomerAddress&supplierId="+$(this).val();
        var obj = $.ajax({
            url:url,
            dataType:'json',
            success:function(json){
                $('#txtAddress').val(json.address);	
                $('#cboInvoiceNo').html(json.salesInvoiceList);
            },
            async:false
        });
        clearRows();
    });
  
  
    ///////////////////////////////////
    ///////get exchange rate //////////
    ///////////////////////////////////
    $('#cboCurrency').change(function(){
        var url = "debitNote-db-get.php?requestType=getExchangeRate&currencyId="+$(this).val()+'&exchangeDate='+$('#txtDate').val();
        var obj = $.ajax({
            url:url,
            dataType:'json',
            success:function(json){
			
                $('#rdoBuying').val(json.buyingRate);
                $('#rdoSelling').val(json.sellingRate);
                $('#rdoAverage').val(((eval(json.buyingRate) + eval(json.sellingRate))/2).toFixed(4));
                $('#rdoSelling').click();
            },
            async:false
        });
    });
    //-------------------------------------------------------------
    $('#frmDebitNote #chkEdit').click(function(){
        if(document.getElementById("chkEdit").checked==true)
            $('#frmDebitNote #txtRate').removeAttr('disabled');
        else
            $('#frmDebitNote #txtRate').attr("disabled",true);

    });  
    //------------------------------------------------------------
  
    $('.rdoRate').click(function(){
        $('#txtRate').val($(this).val());
    });
	
    $('#cboInvoiceNo').change(function(){
        //check automated
        if(invStatus == "")
        {
            $('#frmDebitNote #chkAuto').attr('disabled',false);
            var isChecked = $('#frmDebitNote #chkAuto').attr('checked')?true:false;               
            $('#frmDebitNote #chkAuto').attr('disabled',true);
            var debitNo=$('#frmDebitNote #cboSearch').val();
            //alert($(this).val());
            var url = 'debitNote-db-get.php?requestType=getItemList&salesInvoiceNo='+$(this).val()+'&automated='+isChecked+'&debitNo='+debitNo;
            var obj = $.ajax({
                url:url,
                dataType:'json',
                success:function(json){
                    $('.item').html(json.itemList);
                },
                async:false
            });
        }
        ///////////////////////////get customer invoice type////////////////////
        if($(this).val())
        {
            document.getElementById("rwInvType").style.display='';
            var url = "debitNote-db-get.php?requestType=getInvoiceType&invoiceId="+$(this).val();
            var httpobj = $.ajax({
                url:url,
                dataType:'json',
                async:false,
                success:function(json)
                {
                    $('.invTpe').attr('checked',false); //
                    $('#frmDebitNote #'+json.invoType).attr('checked',true); //;
                }
            });
        }
        else
        {
            document.getElementById("rwInvType").style.display='none';
        }
    ////////////////////////////////////////////////////////////////////
    });
	
    $('#butInsertRow').click(function(){
        var count = $('#tblMain >tbody >tr').length;
        var tr    = $('#tblMain >tbody >tr:eq(1)').html();
        $('#tblMain').append('<tr>'+tr+'</tr>');
        document.getElementById('tblMain').rows[count].cells[1].childNodes[0].value='';
        document.getElementById('tblMain').rows[count].cells[2].childNodes[0].value='';
        document.getElementById('tblMain').rows[count].cells[3].childNodes[0].value='';
        document.getElementById('tblMain').rows[count].cells[4].childNodes[0].value='';
        document.getElementById('tblMain').rows[count].cells[5].childNodes[0].value='';
        document.getElementById('tblMain').rows[count].cells[6].childNodes[0].value='';
        document.getElementById('tblMain').rows[count].cells[7].childNodes[0].value='';
        document.getElementById('tblMain').rows[count].cells[8].childNodes[0].value='';
        document.getElementById('tblMain').rows[count].cells[9].childNodes[0].value='';
        //document.getElementById('tblBankDeposit').rows[rows].cells[5].childNodes[0].value='';
        deleteRow();	
        //-------------------------------
        $('.delImg').click(function(){
            var rowCount = document.getElementById('tblMain').rows.length;
            if(rowCount>2)
                $(this).parent().parent().remove();
            calTax(this);
        });
        //--------------------------------------- 
        $('.taxGroup').live('change',function(){
            calTax(this);
        });
        //--------------------------------------- 
        $('.ammount').keyup(function(){
            calTax(this);
        });
    //-------------------------------------- 
    });
	
    deleteRow();
	
    //--------------------------------------
    $('#butSave').unbind('click');
    $('#butSave').click(saveDetails);
    //--------------------------------------
    $('#butDelete').unbind('click');
    $('#butDelete').click(deleteDetails);
    //--------------------------------------
    $('#butNew').unbind('click');
    $('#butNew').click(clearForm);
    //--------------------------------------- 
    $('.taxGroup').live('change',function(){
        calTax(this);
    });
    //-------------------------------------- 
    $('.delImg').click(function(){
        var rowCount = document.getElementById('tblMain').rows.length;
        if(rowCount>2)
            $(this).parent().parent().remove();
        calTax(this);
    });
    //--------------------------------------- 
    $('.ammount').keyup(function(){
        calTax(this);
    });
    //-------------------------------------- 
    //Dsisplay PO
    $('#frmDebitNote #txtPoNo').click(function(){
        $('#frmDebitNote #chkAuto').attr('disabled',false);
        if($('#frmDebitNote #chkAuto').attr('checked')){
            var po=$('#frmDebitNote #txtPoNo').val();
            var val=po.split("/");
            var url="../../../../presentation/procurement/purchaseOrder/listing/rptPurchaseOrder.php?poNo="+val[0]+"&year="+val[1];
            window.open(url);
        }
    
        $('#frmDebitNote #chkAuto').attr('disabled',true);
    });	
	
	$('#frmDebitNote #butPrint').click(function(){
		if($('#frmDebitNote #txtDebitNo').val()=='')
		{
			$('#frmDebitNote #butPrint').validationEngine('showPrompt', 'Please select Debit Note.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var myurl = 'listing/debitNoteDetails.php?id='+URLEncode($('#frmDebitNote #txtDebitNo').val());
    		window.open(myurl); 
		}
	});
	
	// TODO: ===========Add by dulaskhi 2013.03.18===========	
	getSupplierList();
	
});
//-----------------------------------------------------------------
function deleteRow()
{
    $('.delRow').unbind('click');
    $('.delRow').click(function(){
        var count = $('#tblMain >tbody >tr').length;
        if(count>2)
            $(this).parent().parent().remove();	
    });	
}
//--------------------------------------------------------------
function clearRows()
{
    var rowCount = document.getElementById('tblMain').rows.length;

    for(var i=2;i<rowCount;i++)
    {
        document.getElementById('tblMain').deleteRow(1);
    }
    document.getElementById('tblMain').rows[1].cells[1].childNodes[0].value='';
    document.getElementById('tblMain').rows[1].cells[2].childNodes[0].value='';
    document.getElementById('tblMain').rows[1].cells[3].childNodes[0].value='';
    document.getElementById('tblMain').rows[1].cells[4].childNodes[0].value='';
    document.getElementById('tblMain').rows[1].cells[5].childNodes[0].value='';
    document.getElementById('tblMain').rows[1].cells[6].childNodes[0].value='';
    document.getElementById('tblMain').rows[1].cells[7].childNodes[0].value='';
    document.getElementById('tblMain').rows[1].cells[8].childNodes[0].value='';
}
//-----------------------------------------------------------------

function saveDetailsssssssssssss()
{
    if ($('#frmDebitNote').validationEngine('validate'))   
    {
        var gridData = "[";
        $('#tblMain >tbody >tr:not(:eq(0))').each(function(){
            var item 		= $(this).find('#cboItem').val();
            var desc 		= $(this).find('#cboItem option:selected').text();
            var uom  		= $(this).find('#cboUOM').val();
            var qty  		= $(this).find('#txtQty').val();
            var rate 		= $(this).find('#txtRate').val();
            var discount 	= $(this).find('#txtDiscount').val();
            var amount 		= $(this).find('#txtAmount').val();
            var tax 		= $(this).find('#cboTax').val();
            var dimension 	= $(this).find('#cboDimension').val();
            gridData += '{"item":"'+item+'","uom":"'+uom+'","qty":"'+qty+'","rate":"'+rate+'","discount":"'+discount+'","amount":"'+amount+'","tax":"'+tax+'","dimension":"'+dimension+'"},';
        });
        gridData = gridData.substr(0,gridData.length-1)+']';
	
        var data = 'header=[{'+
        '"supplierId"	:"'+$('#cboSupplier').val()+'",'+
        '"dtDate"		:"'+$('#txtDate').val()+'",'+
        '"remarks"		:"'+URLEncode($('#txtRemarks').val())+'",'+
        '"currencyId"	:"'+$('#cboCurrency').val()+'",'+
        '"rate"			:"'+$('#txtRate').val()+'",'+
        '"invoiceNo"	:"'+$('#cboInvoiceNo').val()+'",'+
        '"poNo"			:"'+$('#txtPONo').val()+'",'+
        '"repId"		:"'+$('#txtGRNNo').val()+'",'+
        '"details"		:'+gridData +
        '}]';
        var url = "debitNote-db-set.php?requestType=saveDetails";
        $.ajax({
            url:url,
            dataType:"JSON",
            data:data,
            async:false,
            type:"post"
        });	
    }
}
//-----------------------------------------------------------------------
function saveDetails()
{
if(existingMsgDate == "")
{
    
    if ($('#frmDebitNote').validationEngine('validate'))   
    {
        changeDesable(false);
        if(checkForDuplicate()==false){
            return false;  
        }
        var data = "requestType=saveDetails";
        //check automated
        $('#frmDebitNote #chkAuto').attr('disabled',false);
        var isChecked = $('#frmDebitNote #chkAuto').attr('checked')?true:false;
        data+="&serialNo="		+	URLEncode($('#cboSearch').val());
        data+="&supplierId="		+	$('#cboSupplier').val();
        data+="&dtDate="	+	$('#txtDate').val();
        data+="&remarks="	+	URLEncode($('#txtRemarks').val());
        data+="&currencyId="			+	$('#cboCurrency').val();
        data+="&rate="		+	$('#txtRate').val();
        data+="&invoiceNo="		+	URLEncode($('#cboInvoiceNo').val());
        data+="&poNo="		+	$('#txtPONo').val();
        data+="&repId="		+	$('#txtGRNNo').val();
        data+="&automated="		+	isChecked;
		data+="&amStatus="		+	amStatus;
		data+="&manualNo="		+	URLEncode($('#txtDebitNo').val());

        var rowCount = document.getElementById('tblMain').rows.length;
        if(rowCount==1){
            alert("No Items selected");
            return false;				
        }
        var row = 0;
			
        var arr="[";
			
        $('#tblMain >tbody >tr:not(:eq(0))').each(function(){
            arr += "{";
            arr += '"item":"'+		URLEncode($(this).find('#cboItem').val()) +'",' ;
            arr += '"desc":"'+		$(this).find('#cboItem option:selected').text() +'",' ;
            arr += '"uom":"'+		$(this).find('#cboUOM').val() +'",' ;
            arr += '"qty":"'+		$(this).find('#txtQty').val() +'",' ;
            arr += '"rate":"'+		$(this).find('#txtRate').val() +'",' ;
            arr += '"discount":"'+		$(this).find('#txtDiscount').val() +'",' ;
            arr += '"amount":"'+		$(this).find('#txtAmount').val() +'",' ;
            arr += '"tax":"'+		$(this).find('#cboTax').val() +'",' ;
            arr += '"dimension":"'+		$(this).find('#cboDimension').val() +'",' ;
            arr += '"trnTaxVal":'+		$(this).find(".taxWith").attr('id') +"," ;
            arr += '"taxVal":"'+		$(this).find(".taxWith").val() +'"' ;
            arr +=  '},';
        });
        arr = arr.substr(0,arr.length-1);
        arr += " ]";
			
        data+="&arr="	+	arr;
        ///////////////////////////// save main infomations /////////////////////////////////////////
        var url = "debitNote-db-set.php";
        var obj = $.ajax({
            url:url,
			
            dataType: "json",  
            data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
            //data:'{"requestType":"addsampleInfomations"}',
            async:false,
            success:function(json){

                $('#frmDebitNote #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
                if(json.type=='pass')
                {
                    //$('#frmSampleInfomations').get(0).reset();
                    //saveImage(json.sampleNo,json.sampleYear,json.revisionNo);
                    //loadCombo_frmSampleInfomations();
                    loadCombo_search();
                    var t=setTimeout("alertx()",1000);
                    $('#txtDebitNo').val(json.serialNo);
                    $('#cboSearch').val(json.serialNo);
                    //	$('#txtRetSupYear').val(json.year);
                    return;
                }
            },
            error:function(xhr,status){
					
                $('#frmDebitNote #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
                var t=setTimeout("alertx()",3000);
            //function (xhr, status){errormsg(status)}
            }		
        });
        changeDesable(true);
        $('#frmDebitNote #chkAuto').attr('disabled',true);
    }
  }
else
{
	$('#frmDebitNote #butSave').validationEngine('showPrompt', existingMsgDate,'fail');
	var t=setTimeout("alertx()",5000);
} 
}

//-----------------------------------------------------------------------
function deleteDetails()
{
    $('#frmDebitNote #butDelete').click(function(){
        var serial=URLEncode($('#frmDebitNote #txtDebitNo').val());
        if($('#frmDebitNote #txtDebitNo').val()=='')
        {
        //$('#frmBankDeposit #butDelete').validationEngine('showPrompt', 'Please select Deposit No.', 'fail');
        //	var t=setTimeout("alertDelete()",1000);	
        }
        else
        {
            var val = $.prompt('Are you sure you want to delete "'+serial+'" ?',{
                buttons: {
                    Ok: true, 
                    Cancel: false
                },
                callback: function(v,m,f){
                    if(v)
                    {
                        var $supplierId=$('#frmDebitNote #cboSupplier').val()
                        var url = "debitNote-db-set.php";
                        var httpobj = $.ajax({
                            url:url,
                            dataType:'json',
                            data:'requestType=delete&serialNo='+serial+'&supplierId='+$supplierId,
                            async:false,
                            success:function(json){
												
                                $('#frmDebitNote #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
                                if(json.type=='pass')
                                {
                                    $('#frmDebitNote').get(0).reset();
                                    clearForm();
                                    loadCombo_search();
                                    var t=setTimeout("alertDelete()",1000);
                                    return;
                                }
                                var t=setTimeout("alertDelete()",3000);
                            }	 
                        });
                    }
                }
            });
			
        }
    });
//	clearForm();
}
//------------------------------------------------------------------------

function calTax(obje){
    
    //changeDesable(false);
    //---------------------------------------------- 
    var qty=$(obje).parent().parent().find('td').eq(3).children().val();
    var unitPrice=$(obje).parent().parent().find('td').eq(4).children().val();
    var discount=$(obje).parent().parent().find('td').eq(5).children().val();
    var am=qty*unitPrice*(100-discount)/100;
    var am=am.toFixed(4)
    $(obje).parent().parent().find('td').eq(6).children().val(am);
    //--------------------------------------------- 
    var amount = $(obje).parent().parent().find('td').eq(6).children().val();
    var taxId = callTaxProcess($(obje).parent().parent().find('td').eq(7).children().val());
    var arrTax = taxId.split('/');
    var operation = '';
	
    var jsonTaxCode="[ ";

    if(arrTax.length == 1)
    {
        operation = 'Isolated';
        jsonTaxCode += '{ "taxId":"'+taxId+'"},';
    }
    else if(arrTax.length > 1)
    {
        operation = arrTax[1];
        for(var i =0; i < arrTax.length; i=i+2)
        {
            jsonTaxCode += '{ "taxId":"'+arrTax[i]+'"},';
        }
    }
    jsonTaxCode = jsonTaxCode.substr(0,jsonTaxCode.length-1);
    jsonTaxCode += " ]";
    var url = "debitNote-db-get.php?requestType=getTaxValue&arrTaxCode="+jsonTaxCode+"&opType="+operation+"&valAmount="+amount;
    var obj = $.ajax({
        url:url,
        async:false
    });
    var values = obj.responseText.split('/');
    $(obje).parent().parent().find('td').eq(9).children().val(values[0]);
    if(arrTax.length == 1)
    {
        trnTax="[ ";
        trnTax += '{ "taxId":"'+taxId+'", "taxValue": "'+values[1]+'"},';
        trnTax = trnTax.substr(0,trnTax.length-1);
        trnTax += " ]";
    }
    else if(arrTax.length > 1)
    {
        trnTax="[ ";
        var k = 1;
        for(var i =0; i < arrTax.length; i=i+2)
        {
            trnTax += '{ "taxId":"'+arrTax[i]+'", "taxValue": "'+values[k]+'"},';
            k++;
        }
        trnTax = trnTax.substr(0,trnTax.length-1);
        trnTax += " ]";
    }
    $(obje).parent().parent().find('td').eq(9).children().attr('id',trnTax);
    //return obj.responseText;
    callTaxTotal();
//changeDesable(true);
}
//-----------------------------------------------------------------------------
function callTaxProcess(taxGroupId)
{
    var url = "debitNote-db-get.php?requestType=getTaxProcess&taxGroupId="+taxGroupId;
    var obj = $.ajax({
        url:url,
        async:false
    });
    return obj.responseText;
}
//---------------------------------------------------------------------------------
function callTaxTotal()
{
    var taxTotal = 0.00;
    $(".taxWith").each( function(){
        taxTotal += eval($(this).val()==''?0.00:$(this).val());
    });
    //alert(taxTotal);
    $('#txtTotalTax').val(taxTotal.toFixed(4));
    callTotalAmount();
}
//---------------------------------------------------------------------------------
function callTotalAmount()
{
    var subTot=callTotat().toFixed(2);
    var taxTotal=parseFloat($('#txtTotalTax').val()).toFixed(2);
    var tot=parseFloat(taxTotal)+parseFloat(subTot);
    tot=tot.toFixed(2);
    $('#txtTotal').val(tot);
    if(isNaN(tot)==true){
        $('#txtTotal').val(0);
    }
/*	var x = '10*3';
	alert(eval(x));
*/}
//---------------------------------------------------------------------------------
function callTotat()
{
    var subTotal = 0.00;
    var rowCount = document.getElementById('tblMain').rows.length;
    var row = 0;
	
    for(var i=1;i<rowCount;i++)
    {
        if(document.getElementById('tblMain').rows[i].cells[6].childNodes[0].value==''){
            var qty=0;
        }
        else{
            var qty= 	document.getElementById('tblMain').rows[i].cells[6].childNodes[0].value;
        }
        if(isNaN(qty)==true){
            qty=0; 
        }
        subTotal+=parseFloat(qty);
    }
    //	alert(subTotal);
    $('#txtSubTotal').val(subTotal.toFixed(4));
    return subTotal;
}
//------------------------------------------------------------------
//---------------------------------------------------------------------------------
function clearForm(){
//---------------------------------------------------
	existingMsgDate = "";
	amStatus = "Auto";
	$("#frmDebitNote #txtDebitNo").attr("readonly","readonly");
	$('#frmDebitNote #chkAutoManual').attr('checked')
	$("#frmDebitNote #chkAutoManual").attr("disabled","");
	document.getElementById("chkAutoManual").style.display='';
	document.getElementById("amStatus").style.display='';
	$('#frmDebitNote #txtDebitNo').removeClass('validate[required]');
//--------------------------------------------------
    
    $('#frmDebitNote').get(0).reset();
    clearRows();
    $('#frmDebitNote #txtDebitNo').val('');
    $('#frmDebitNote #cboSearch').val('');
    $('#frmDebitNote #cboSupplier').val('');
    $('#frmDebitNote #txtAddress').val('');
    $('#frmDebitNote #cboCurrency').val('');
    $('#frmDebitNote #txtRemarks').val('');
    $('#frmDebitNote #txtDate').val('');
    $('#frmDebitNote #txtRate').val('');
    $('#frmDebitNote #cboInvoiceNo').val('');
    $('#frmDebitNote #txtPONo').val('');
    $('#frmDebitNote #txtGRNNo').val('');
    $('#frmDebitNote #txtSubTotal').val('0.00');
    $('#frmDebitNote #txtTotalTax').val('0.00');
    $('#frmDebitNote #txtTotal').val('0.00');
    $('#frmDebitNote #cboSupplier').focus();
		
    var currentTime = new Date();
    var month = currentTime.getMonth()+1 ;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();
    if(day<10)
        day='0'+day;
    if(month<10)
        month='0'+month;
    d=year+'-'+month+'-'+day;
		
    $('#frmDebitNote #txtDate').val(d);
    changeDesable(false);
                
}
//--------------------------------------------------------------------
function alertx()
{
    $('#frmDebitNote #butSave').validationEngine('hide')	;
}
function alertDelete()
{
    $('#frmDebitNote #butDelete').validationEngine('hide')	;
}
//--------------------------------------------------------------------------------
function checkForDuplicate(){
    var rowCount = document.getElementById('tblMain').rows.length;
    //var row=this.parentNode.parentNode.rowIndex;
    var k=0;
    for(var row=1;row<rowCount;row++){
        for(var i=1;i<row;i++){
            if(document.getElementById('tblMain').rows[i].cells[1].childNodes[0].value==document.getElementById('tblMain').rows[row].cells[1].childNodes[0].value)	{
                k=1;		
            }
        }
    }
    if(k==0){
        return true
    }
    else{
        alert("duplicate Accounts existing");
        return false
    }
}
//-----------------------------------------------------------------------------
function loadCombo_search()
{
    var url 	= "debitNote-db-get.php?requestType=loadCombo";
    var httpobj = $.ajax({
        url:url,
        async:false
    })
    $('#frmDebitNote #cboSearch').html(httpobj.responseText);
}
//------------------------------------------------------------------------------
function changeDesable(status){
    $('#frmDebitNote #cboSupplier').attr('disabled',status);
    $('#frmDebitNote #txtDate').attr('disabled',status);
    $('#frmDebitNote #cboCurrency').attr('disabled',status);
    $('#frmDebitNote #cboInvoiceNo').attr('disabled',status);
    $('#frmDebitNote #txtPONo').attr('readOnly',status);
    $('#frmDebitNote #txtPoNo').attr('class',"normalfnt");
    
    $('#frmDebitNote #txtGRNNo').attr('disabled',status);
    $('#frmDebitNote #cboItem').attr('disabled',status);
    $('#frmDebitNote #cboUOM').attr('disabled',status);
    $('#frmDebitNote #txtQty').attr('disabled',status);
    $('#frmDebitNote #txtRate').attr('disabled',status);
    $('#frmDebitNote #txtDiscount').attr('disabled',status);
    $('#frmDebitNote #txtAmount').attr('disabled',status);
}

//=========Add by dulakshi 2013.03.18===========
//====================Get Supplier List==============================
function getSupplierList()
{	
	$ledgerAccId = $('#frmDebitNote #cboLedgerAcc').val();
	$searchId = $('#frmDebitNote #cboSearch').val();
	
	var url = "debitNote-db-get.php?requestType=loadSupplier&ledgerAcc="+$ledgerAccId;
	var httpobj = $.ajax({url:url,async:false})
	
	if($searchId == '')
		{
			$('#frmDebitNote #cboSupplier').html(httpobj.responseText);
		}
}
<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$locationId = $_SESSION["CompanyID"];
$companyId	= $_SESSION["headCompanyId"];

$thisFilePath =  $_SERVER['PHP_SELF'];
include  "{$backwardseperator}dataAccess/permisionCheck.inc";

$debitNo = $_REQUEST['debitNo'];
if($debitNo){
$debitNo = $_REQUEST['debitNo'];
}
else{
$debitNo = $_GET['cboSearch'];
}
$autoCheck=0;
$autoDesable=false;
//$depositNo = '5000003';

		   $sql = "SELECT
fin_supplier_debitnote_header.strDebitNoteNo,
fin_supplier_debitnote_header.strInvoiceNo,
fin_supplier_debitnote_header.intSupplier,
fin_supplier_debitnote_header.dtDate,
fin_supplier_debitnote_header.dblRate,
fin_supplier_debitnote_header.intCurrency,
fin_supplier_debitnote_header.strRemarks,
fin_supplier_debitnote_header.strPoNo,
fin_supplier_debitnote_header.strGrnNo,
fin_supplier_debitnote_header.intStatus,
fin_supplier_debitnote_header.intAutomated
FROM fin_supplier_debitnote_header
WHERE
fin_supplier_debitnote_header.strDebitNoteNo =  '$debitNo'";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$supplierId = $row['intSupplier'];
					$date = $row['dtDate'];
					$remarks = $row['strRemarks'];
					$currency = $row['intCurrency'];
					$rate = $row['dblRate'];
					if($rate==0){
						$rate='';
					}
					$invNo = $row['strInvoiceNo'];
					$poNo = $row['strPoNo'];
					$grnNo = $row['strGrnNo'];
					$sql2 = "SELECT 	mst_supplier.strAddress FROM mst_supplier	WHERE mst_supplier.intId =  '$supplierId'";
						$result2 = $db->RunQuery($sql2);
						$row2 = mysqli_fetch_array($result2);
					$invAddress =  $row2['strAddress'];
                                        $autoCheck=$row['intAutomated']==1?"checked":"";
                                        $autoDesable=$row['intAutomated']==1?"true":"false";                                        
				 }
				 
	$sql = "SELECT
			fin_supplier_purchaseinvoice_header.strReferenceNo,
			fin_supplier_purchaseinvoice_header.strSupInvoice
			FROM fin_supplier_purchaseinvoice_header
			WHERE
			fin_supplier_purchaseinvoice_header.intSupplierId =  '$supplierId' AND
			fin_supplier_purchaseinvoice_header.intCompanyId =  '$companyId' AND
			fin_supplier_purchaseinvoice_header.intDeleteStatus =  '0'
			";
	$result = $db->RunQuery($sql);
	$salesInvoiceList ='<option value=""></option>';
	while($row = mysqli_fetch_array($result))
	{
		if($row['strReferenceNo']==$invNo){
		$salesInvoiceList .="<option value=\"".$row['strReferenceNo']."\" selected=\"selected\">".$row['strSupInvoice']."</option>";	
		}
		else{
		$salesInvoiceList .="<option value=\"".$row['strReferenceNo']."\">".$row['strSupInvoice']."</option>";	
		}
	}
// ======================Check Exchange Rate Updates========================
if($invoiceRefNo == "")
{
	$status = "Adding";
}
else
{
	$status = "Changing";
}
$currentDate = date("Y-m-d");

$sql = "SELECT COUNT(*) AS 'no'
		FROM
		mst_financeexchangerate
		WHERE
		mst_financeexchangerate.dtmDate =  '$currentDate'
		";
$result = $db->RunQuery($sql);
$row= mysqli_fetch_array($result);
if($row['no']==0)
	{
		$str =  "Please Update Exchange Rates Before ".$status." Advance Payment .";
		$str .= $row['NameList'];
		$maskClass="maskShow";
	}
	else
	{
		$maskClass="maskHide";
	}
// =========================================================================
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Supplier Debit Note</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $backwardseperator; ?>css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">


<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script src="debitNote-js.js" type="text/javascript"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>
<script src="../../commanFunctions/numberExisting-js.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
</head>

<body onLoad="functionList();">
<form id="frmDebitNote" name="frmDebitNote"  method="get" action="debitNote.php" autocomplete="off">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<div id="divMask" class="<?php echo $maskClass?> mask"> <?php echo $str; ?></div>

<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<div align="center">
  <div class="trans_layoutL">
    <div class="trans_text">Supplier Debit Note</div>
<table width="100%" cellspacing="0">
      <tr>
        <td class="normalfnt" width="50%"><img src="../../../../images/fb.png" width="18" height="19" /></td>
        <td align="right" width="50%"><img src="../../../../images/ff.png" width="18" height="19" /></td>
      </tr>
    <tr>
      <td  colspan="2">
        <table width="100%">
          <tr>
            <td align="right" width="33%"><span class="normalfnt"><strong>Search</strong></span></td>
            <td align="left" width="67%"><select name="cboSearch" id="cboSearch"  style="width:350px" onchange="submit();" >
              <option value=""></option>
              <?php  $sql = "SELECT DISTINCT
						fin_supplier_debitnote_header.strDebitNoteNo,
						mst_supplier.strName
						FROM
						fin_supplier_debitnote_header
						Inner Join mst_supplier ON fin_supplier_debitnote_header.intSupplier = mst_supplier.intId
						WHERE
						fin_supplier_debitnote_header.intStatus =  '1' AND
						fin_supplier_debitnote_header.intCompanyId =  '$companyId'
						order by strDebitNoteNo desc";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							if($row['strDebitNoteNo']==$debitNo)
							echo "<option value=\"".$row['strDebitNoteNo']."\" selected=\"selected\">".$row['strDebitNoteNo']." - ".$row['strName']."</option>";	
							else
							echo "<option value=\"".$row['strDebitNoteNo']."\">".$row['strDebitNoteNo']." - ".$row['strName']."</option>";
						}
          ?>
              </select></td>
            </tr>
          <tr><td align="right" colspan="2"><span class="normalfntRight"><input <?php echo $autoCheck ?>  type="checkbox" name="chkAuto" id="chkAuto" value="" disabled="true" style="background-color:#F4FFFF;"/>Automated</span></td></tr>
          </table></span></td>
    </tr>
    <tr>
      <td colspan="2">
      <table width="100%" class="tableBorder_allRound">
    <tr>
      <td class="normalfnt">&nbsp;</td>
      <td class="normalfnt">Debit Number</td>
      <td colspan="2"><input name="txtDebitNo" type="text" readonly="readonly" class="normalfntRight" id="txtDebitNo" style="width:230px; background-color:#F4FFFF;text-align:center; border:dotted; border-color:#F00" value="<?php echo $debitNo; ?>" onblur="numberExisting(this,'Debit Note');" />
        <input checked="checked" type="checkbox" name="chkAutoManual" id="chkAutoManual" />
        <input name="amStatus" type="text" class="normalfntBlue" id="amStatus" style="width:40px; background-color:#FFF; text-align:center; border:thin" disabled="disabled" value="(Auto)"/></td>
      <td bgcolor="#FFFFFF" class="">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    
     <!-- ========Add by dulakshi 2013.03.18============== --> 
           <tr>
      <td width="32" class="normalfnt">&nbsp;</td>
      <td width="124" class="normalfnt">Ledger Accounts </td>
      <td width="240"><span class="normalfnt">
              <select name="cboLedgerAcc" id="cboLedgerAcc"  style="width:211px" onchange="getSupplierList();" >
          <option value=""></option>
          <?php  
          				$sql = "SELECT DISTINCT
										mst_financechartofaccounts.intId,
										mst_financechartofaccounts.strCode,
										mst_financechartofaccounts.strName
								FROM
										mst_financechartofaccounts
										      Inner Join mst_financesupplieractivate ON mst_financechartofaccounts.intId = mst_financesupplieractivate.intChartOfAccountId
								WHERE
										mst_financesupplieractivate.intCompanyId =  '$companyId'
								GROUP BY  mst_financechartofaccounts.intId 
								order by strCode";
						$result2 = $db->RunQuery($sql);
						while($row2=mysqli_fetch_array($result2))
						{
						   echo "<option value=\"".$row2['intId']."\">".$row2['strCode']."-".$row2['strName']."</option>";
						}
          ?>
        </select>
      </span></td>
    
    </tr>
     <!-- ================================ --> 
       
    <tr>
      <td width="32" class="normalfnt">&nbsp;</td>
      <td width="124" class="normalfnt">Supplier <span class="compulsoryRed">*</span></td>
      
    <!-- ========Edited by dulakshi 2013.03.18============== --> 
	  <td width="240"><span class="normalfnt">
	       <select name="cboSupplier" id="cboSupplier"  style="width:211px" class="validate[required]">
	       	<option value=""></option>
	       		<?php
					$sql = "SELECT
								mst_supplier.intId,
								mst_supplier.strName
							FROM
								mst_financesupplieractivate
								Inner Join mst_supplier ON mst_supplier.intId = mst_financesupplieractivate.intSupplierId
							WHERE
								mst_supplier.intStatus =  '1' AND
								mst_financesupplieractivate.intCompanyId =  '$companyId'
							ORDER BY
								mst_supplier.strName ASC";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$supplierId)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
	       	
	       </select>
	  </span></td>	  
	  
    <!-- ================================== --> 
      <td width="110"><span class="normalfnt">Date <span class="compulsoryRed">*</span></span></td>
      <td width="323" bgcolor="#FFFFFF" class=""><input name="txtDate" type="text" value="<?Php echo ($date==''?date("Y-m-d"):$date) ?>" class="txtbox" id="txtDate" style="width:98px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" onblur="backDateExisting(this,'Debit Note');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
      <td width="51">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="normalfnt">Invoice Address</span></td>
      <td><textarea disabled="disabled" name="txtAddress" id="txtAddress" style="width:211px" cols="30" rows="2"><?php echo $invAddress ?></textarea></td>
      <td><span class="normalfnt">Remarks</span></td>
      <td><textarea name="txtRemarks" id="txtRemarks" cols="45" rows="2"><?php echo $remarks; ?></textarea></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="normalfnt">Currency <span class="compulsoryRed">*</span></span></td>
      <td><select  name="cboCurrency" id="cboCurrency" style="width:211px" class="validate[required,custom[number]]">
        <option value=""></option>
        <?php  $sql = "SELECT
						intId,
						strCode
						FROM mst_financecurrency
						WHERE
							intStatus = 1
						order by strCode
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							if($row['intId']==$currency)
							echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strCode']."</option>";	
							else
							echo "<option value=\"".$row['intId']."\">".$row['strCode']."</option>";
						}
        				?>
      </select></td>
      <td><span class="normalfnt">Rate <span class="compulsoryRed">*</span></span></td>
      <td><span class="normalfnt"><span class="normalfntMid">
        <input class="rdoRate" type="radio" name="rate" id="rdoSelling" value="0" />
        Selling
        <input class="rdoRate" type="radio" name="rate" id="rdoBuying" value="0" />
        Buying 
        <input class="rdoRate" type="radio" name="rate" id="rdoAverage" value="" />
Average
<input type="text" name="txtRate" id="txtRate" style="width:75px; text-align:right" disabled="disabled" value="<?php echo $rate ?>"   class="validate[required,custom[number]]"/>
      <input type="checkbox" name="chkEdit" id="chkEdit" />
      </span></span></td>
      <td>&nbsp;</td>
      </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="normalfnt">Invoice Number <span class="compulsoryRed">*</span></span></td>
      <td colspan="2">
        <select name="cboInvoiceNo" id="cboInvoiceNo" style="width:211px"  class="validate[required]">
          <?php echo $salesInvoiceList; ?>
        </select>
      </td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr id="rwInvType" style="display:none;background-color:#00CCFF">
      <td>&nbsp;</td>
      <td><span class="normalfnt">Invoice Type</span></td>
      <td colspan="2"><span class="normalfnt">
        <input disabled="disabled" type="radio" class="invTpe" name="rdInvoType" id="Commercial" value="Commercial" />
Commercial
<input disabled="disabled" type="radio"  class="invTpe" name="rdInvoType" id="Tax" value="Tax" />
Tax
<input disabled="disabled" type="radio"  class="invTpe" name="rdInvoType" id="SVAT" value="SVAT" />
SVAT
<input disabled="disabled" type="radio"  class="invTpe" name="rdInvoType" id="NFE" value="NFE" />
NFE-Suspended</span></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
      </table>
      </td>
      </tr>
    <tr>
      <td colspan="2">
      <table width="100%" class="tableBorder_allRound">
      <tr>
      <td align="center">
		<table width="37%" id="tblMainGrid1" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
        <tr class="">
          <td width="80"  height="26" bgcolor="#FAD163" class="normalfntMid"><strong>P.O No.</strong></td>
          <td width="249"  bgcolor="#FAD163" class="normalfntMid"><strong>GRN No</strong></td>
          </tr>
        <tr class="normalfnt">
          <td bgcolor="#FFFFFF" class="normalfntMid"><input  type="text" name="txtPONo" id="txtPONo" style="width:80px"  class="normalfntMid" value="<?php echo $poNo ?>"/></td>
          <td bgcolor="#FFFFFF" class="normalfntMid"><input  type="text" name="txtGRNNo" id="txtGRNNo" style="width:95%"  class="" value="<?php echo $grnNo ?>"/></td>
          </tr>
      </table>
      </td>
      </tr>
      </table>
      </td>
      </tr>
            <tr>
      <td colspan="2"  align="right" valign="bottom"><img id="butInsertRow" src="../../../../images/Tadd.jpg" width="92" height="24" class="mouseover" /></td>
      </tr>
    <tr>
      <td colspan="2"><table width="100%">
        <tr>
          <td>
          <div style="overflow:scroll;width:900px;height:200px;" id="divGrid">
          <table width="100%" id="tblMain" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
            <tr class="">
              <td width="3%" bgcolor="#FAD163" class="normalfntMid">Del</td>
              <td width="13%" height="27" bgcolor="#FAD163" class="normalfntMid"><strong>Item / Expenses<span class="compulsoryRed">*</span></strong></td>
              <td width="8%" bgcolor="#FAD163" class="normalfntMid"><strong>UOM <span class="compulsoryRed">*</span></strong></td>
              <td width="8%" bgcolor="#FAD163" class="normalfntMid"><strong>Qty <span class="compulsoryRed">*</span></strong></td>
              <td  bgcolor="#FAD163" class="normalfntMid"  ><strong>U.P</strong></td>
              <td width="9%" bgcolor="#FAD163" class="normalfntMid"><strong>Discount %</strong></td>
              <td width="11%" bgcolor="#FAD163" class="normalfntMid"  ><strong>Amount <span class="compulsoryRed">*</span></strong></td>
              <td width="11%" bgcolor="#FAD163" class="normalfntMid"  ><strong>Tax</strong></td>
              <td width="11%" bgcolor="#FAD163" class="normalfntMid"  ><strong><span class="compulsoryRed">*</span> Cost Center</strong></td>
              <td width="11%" bgcolor="#FAD163" class="normalfntMid" style="display:none"><strong>Tax With</strong></td>
            </tr>
            <?php
            if($autoCheck){// for automated
                $sqlItems = "SELECT
                    mst_item.strCode as strName,
                    fin_supplier_debitnote_details.intItem
                FROM
                    mst_item
                    INNER JOIN fin_supplier_debitnote_details ON fin_supplier_debitnote_details.intItem = mst_item.intId
                WHERE
                    fin_supplier_debitnote_details.strDebitNoteNo = '$debitNo' ";
            }
            else
			{
                $sqlItems = "SELECT
				fin_supplier_purchaseinvoice_details.intItem,
				mst_financesupplieritem.strName
			FROM
				fin_supplier_purchaseinvoice_details
				Inner Join mst_financesupplieritem ON mst_financesupplieritem.intId = fin_supplier_purchaseinvoice_details.intItem
			WHERE
				fin_supplier_purchaseinvoice_details.strReferenceNo =  '$invNo'
			ORDER BY
				mst_financesupplieritem.intStatus ASC
			";
            }	 
			
	$sql = "SELECT
			fin_supplier_debitnote_details.strDebitNoteNo,
			fin_supplier_debitnote_details.intAccountId,
			fin_supplier_debitnote_details.intItem,
			fin_supplier_debitnote_details.intUOM,
			fin_supplier_debitnote_details.dblQty,
			fin_supplier_debitnote_details.dblRate,
			fin_supplier_debitnote_details.dblDiscount,
			fin_supplier_debitnote_details.intTaxGroup,
			fin_supplier_debitnote_details.dblTax,
			fin_supplier_debitnote_details.strItmType,
			fin_supplier_debitnote_details.intDimension
			FROM fin_supplier_debitnote_details
			WHERE
			fin_supplier_debitnote_details.strDebitNoteNo =  '$debitNo'";
			
 	$exsisting=0;
	$result = $db->RunQuery($sql);
		$subTot=0.00;
		$taxSum=0.00;
		$tot=0.00;
	while($row = mysqli_fetch_array($result))
	{
		$exsisting++;
		$item=$row['intItem']."^".$row['strItmType'];
		$uom=$row['intUOM'];
		$qty=$row['dblQty'];
		$up=$row['dblRate'];
		$discount=$row['dblDiscount'];
		$amount=$row['dblQty']*$row['dblRate']*(100-$discount)/100;
		$tax=$row['intTaxGroup'];
		$dimention=$row['intDimension'];
		$taxVal=$row['dblTax'];
		
		$subTot+=$amount;
		$taxSum+=$taxVal;
		?>
<tr class="normalfnt">
              <td bgcolor="#FFFFFF" class="normalfntMid"><img class="mouseover delImg" src="../../../../images/del.png" width="15" height="15" /></td>
              <td bgcolor="#FFFFFF" class="normalfntMid"><select name="cboItem" id="cboItem" style="width:130px" class="validate[required] item">
                <option value=""></option>
                       <?php  
						$resultItems = $db->RunQuery($sqlItems);
						while($rowItm=mysqli_fetch_array($resultItems))
						{
							if($rowItm['intItem']."^"."Itm"==$item)
							echo "<option value=\"".$rowItm['intItem']."^"."Itm"."\" selected=\"selected\">".$rowItm['strName']."</option>";	
							else
							echo "<option value=\"".$rowItm['intItem']."^"."Itm"."\">".$rowItm['strName']."</option>";
						}
						$sqlAcc = "SELECT
								mst_financechartofaccounts.intId,
								mst_financechartofaccounts.strCode,
								mst_financechartofaccounts.strName
								FROM
								mst_financechartofaccounts
								WHERE
								mst_financechartofaccounts.strType = 'Posting' AND
								mst_financechartofaccounts.intStatus =  '1' AND
								(mst_financechartofaccounts.intFinancialTypeId =  '6' OR mst_financechartofaccounts.intFinancialTypeId =  '5')
								";
						$resultAcc = $db->RunQuery($sqlAcc);
						while($rowAcc=mysqli_fetch_array($resultAcc))
						{
							if($rowAcc['intId']."^"."Acc"==$item)
							echo "<option value=\"".$rowAcc['intId']."^"."Acc"."\" selected=\"selected\">".$rowAcc['strCode']." - ".$rowAcc['strName']."</option>";	
							else
							echo "<option value=\"".$rowAcc['intId']."^"."Acc"."\">".$rowAcc['strCode']." - ".$rowAcc['strName']."</option>";
						}
        				?>
              </select></td>
              <td bgcolor="#FFFFFF"><select name="cboUOM" class="validate[required] uom" id="cboUOM" style="width:75px" >
                <option value=""></option>
                <?php  $sqlu = "SELECT
								intId,
								strName
								FROM mst_units
									WHERE intStatus=1
										order by strName";
										
								$resultu = $db->RunQuery($sqlu);
								while($rowu=mysqli_fetch_array($resultu))
								{
									if($rowu['intId']==$uom)
									echo "<option value=\"".$rowu['intId']."\" selected=\"selected\">".$rowu['strName']."</option>";	
									else
									echo "<option value=\"".$rowu['intId']."\">".$rowu['strName']."</option>";
								}
                   		?>
              </select></td>
              <td bgcolor="#FFFFFF" class="normalfntMid"><input name="txtQty" type="text" id="txtQty" style="width:75px; text-align:right"  class="validate[required,custom[number]] ammount" value="<?php echo $qty ?>"/></td>
              <td width="11%" align="center" bgcolor="#FFFFFF" class="normalfntMid"><input  name="txtRate" type="text"   class="validate[required,custom[number]] ammount" id="txtRate" style="width:70px; text-align:right"  value="<?php echo $up ?>"/></td>
              <td  bgcolor="#FFFFFF" class="normalfntMid"><input  name="txtDiscount" type="text" class="validate[custom[number],max[100]] ammount" id="txtDiscount" style="width:75px; text-align:right" value="<?php echo $discount ?>" /></td>
              <td  bgcolor="#FFFFFF" class="normalfntMid"><input   name="txtAmount" type="text" class="validate[required,custom[number]] ammount" id="txtAmount" style="width:100px; text-align:right"  disabled="disabled"  value="<?php echo $amount ?>" /></td>
              <td  bgcolor="#FFFFFF"><select name="cboTax"  id="cboTax"  style="width:100px;" class="taxGroup">
                <option value=""></option>
                <?php  
                    $sqlt = "SELECT
                                mst_financetaxgroup.intId,
                                mst_financetaxgroup.strCode,
                                mst_financetaxgroup.intStatus
                            FROM
                                mst_financetaxgroup
                            WHERE
                                intStatus = 1
                            order by 
                                strCode";
                    $resultt = $db->RunQuery($sqlt);
                    while($rowt=mysqli_fetch_array($resultt))
                    {
                    if($rowt['intId']==$tax)
                            echo "<option value=\"".$rowt['intId']."\" selected=\"selected\">".$rowt['strCode']."</option>";	
                    else
                            echo "<option value=\"".$rowt['intId']."\">".$rowt['strCode']."</option>";
                    }
                   ?>
              </select></td>
              <td  bgcolor="#FFFFFF"><select name="cboDimension"  class="validate[required]" id="cboDimension"  style="width:100px;">
                <option value=""></option>
                <?php  $sqld = "SELECT
									intId,
									strName
								FROM mst_financedimension
								WHERE
									intStatus = 1
								order by strName
								";
								$resultd = $db->RunQuery($sqld);
								while($rowd=mysqli_fetch_array($resultd))
								{
								if($rowd['intId']==$dimention)
									echo "<option value=\"".$rowd['intId']."\" selected=\"selected\">".$rowd['strName']."</option>";	
								else
									echo "<option value=\"".$rowd['intId']."\">".$rowd['strName']."</option>";
								}
                   ?>
              </select></td>
              <td  bgcolor="#FFFFFF" style="display:none"><input type="text" name="txtTaxWith" id="" style="width:80px; background-color:#F4FFFF; text-align:right" class="validate[custom[number]] taxWith" disabled="disabled" value="<?php echo $taxVal ?>"/></td>
            </tr>	<?php }
			
            if($exsisting==0){?>
            <tr class="normalfnt">
              <td bgcolor="#FFFFFF" class="normalfntMid"><img class="mouseover delImg" src="../../../../images/del.png" width="15" height="15" /></td>
              <td bgcolor="#FFFFFF" class="normalfntMid"><select name="cboItem" id="cboItem" style="width:130px" class="validate[required] item">
                <option value=""></option>
                <?php  $sql = "SELECT
						intId,
						strName
						FROM mst_financecustomeritem
						WHERE
							intStatus = 1
						order by strName
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							//echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
						}
        				?>
              </select></td>
              <td bgcolor="#FFFFFF"><select name="cboUOM" class="validate[required] uom" id="cboUOM" style="width:75px" >
                <option value=""></option>
                <?php  $sql = "SELECT
								intId,
								strName
								FROM mst_units
									WHERE intStatus=1
										order by strName";
										
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
								}
                   		?>
              </select></td>
              <td bgcolor="#FFFFFF" class="normalfntMid"><input name="txtQty" type="text" id="txtQty" style="width:75px; text-align:right"  class="validate[required,custom[number]] ammount"/></td>
              <td width="11%" align="center" bgcolor="#FFFFFF" class="normalfntMid"><input name="txtRate" type="text"   class="validate[required,custom[number]] ammount" id="txtRate" style="width:70px; text-align:right"  /></td>
              <td  bgcolor="#FFFFFF" class="normalfntMid"><input name="txtDiscount" type="text" class="validate[custom[number],max[100]] ammount" id="txtDiscount" style="width:75px; text-align:right" /></td>
              <td  bgcolor="#FFFFFF" class="normalfntMid"><input name="txtAmount" type="text" class="validate[required,custom[number]] ammount" id="txtAmount" style="width:100px; text-align:right"  disabled="disabled"  value="" /></td>
              <td  bgcolor="#FFFFFF"><select name="cboTax"  id="cboTax"  style="width:100px;" class="taxGroup">
                <option value=""></option>
                <?php  $sql = "SELECT
								mst_financetaxgroup.intId,
								mst_financetaxgroup.strCode,
								mst_financetaxgroup.intStatus
								FROM
								mst_financetaxgroup
								WHERE
									intStatus = 1
								order by strCode
								";
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intId']."\">".$row['strCode']."</option>";
								}
                   ?>
              </select></td>
              <td  bgcolor="#FFFFFF"><select name="cboDimension"  class="validate[required]" id="cboDimension"  style="width:100px;">
                <option value=""></option>
                <?php  $sql = "SELECT
									intId,
									strName
								FROM mst_financedimension
								WHERE
									intStatus = 1
								order by strName
								";
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
								}
                   ?>
              </select></td>
              <td  bgcolor="#FFFFFF" style="display:none"><input type="text" name="txtTaxWith" id="" style="width:80px; background-color:#F4FFFF; text-align:right" class="validate[custom[number]] taxWith" disabled="disabled" value="<?php echo $taxAmmount ?>"/></td>
            </tr>
            <?php } ?>
          </table>
                </div>
          </td>
        </tr>
      </table></td>
      </tr>
      <tr>
    	<td colspan="2">
    	<table width="100%">
        <tr>
        <td width="9%">&nbsp;</td>
        <td width="68%">&nbsp;</td>
        <td width="23%">
        <table width="100%">
        <?php
			 $tot=$subTot+$taxSum;
		?>
        <tr>
        <td width="111" align="right"><span class="normalfnt">Sub-Total</span></td>
        <td width="80" align="right"><span class="normalfntMid">
          <input name="txtSubTotal" type="text" disabled="disabled" id="txtSubTotal" style="width:75px; text-align:right" value="<?php echo number_format($subTot, 4) ; ?>" />
        </span></td>
        </tr>
        <tr>
          <td align="right"><span class="normalfnt">Tax</span></td>
          <td align="right"><span class="normalfntMid">
                  <input name="txtTotalTax" type="text" disabled="disabled" id="txtTotalTax" style="width:75px; text-align:right"  value="<?php echo number_format($taxSum, 4)  ; ?>" />
          </span></td>
        </tr>
        <tr>
          <td align="right"><span class="normalfnt">Total</span></td>
          <td align="right"><span class="normalfntMid">
            <input name="txtTotal" type="text" disabled="disabled" id="txtTotal" style="width:75px; text-align:right" value="<?php echo number_format($tot, 4) ; ?>"/>
          </span></td>
        </tr>
        </table>
        </td>
        </tr>
        </table>
    	</td>
      </tr>
    <tr>
      <td colspan="2">
        <table width="100%">
          <tr>
            <td width="100%" height="34" class="tableBorder_allRound"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td width="100%" align="center" bgcolor=""><img border="0" src="../../../../images/Tnew.jpg" alt="New" name="New" width="92" height="24" onclick="clearForm();" class="mouseover" id="butNew" tabindex="28"/><img border="0" src="../../../../images/Tsave.jpg" alt="Save" name="Save"width="92" height="24" class="mouseover" id="butSave" tabindex="24"/><img style="display:none" border="0" src="../../../../images/Tprint.jpg" alt="Print" name="butPrint" width="92" height="24" class="mouseover" id="butPrint" tabindex="25"/><img border="0" src="../../../../images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24"  class="mouseover" id="butDelete" tabindex="25"/><a href="../../../../main.php"><img src="../../../../images/Tclose.jpg" alt="Close" name="Close" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                </tr>
              </table>
              </td>
            </tr>
          </table>
        </td>
    </tr>
    </table>
</div>
</div>
<script>
    changeDesable(<?php echo $autoDesable ?>);
</script>
</form>
</body>
</html>
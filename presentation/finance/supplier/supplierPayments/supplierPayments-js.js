// JavaScript Document
var toBePaidAmount = '';
var amStatus = "Auto";
function functionList()
{
	if(recRefNo!='')
	{
		$('#frmSupplierPayments #cboSearch').val(recRefNo);
		$('#frmSupplierPayments #cboSearch').change();
	}
}
var rows = 1;
function insertRow()
{
	var tbl = document.getElementById('tblMainGrid2');	
	rows = tbl.rows.length;
	tbl.insertRow(rows);
	tbl.rows[rows].innerHTML = tbl.rows[rows-1].innerHTML;
//	loadJs();
}
$(document).ready(function() {
	
	var id = '';
	$("#frmSupplierPayments").validationEngine();
	$('#frmSupplierPayments #cboSupplier').focus();

$('.delImg').live('click',function(){
	var rowId = $(this).parent().parent().parent().find('tr').length;
	if(rowId!=2)
	$(this).parent().parent().remove();
	finalAmount();
});

$('.delImg').css('cursor', 'pointer');

  //permision for add 
  if(intAddx)
  {
 	$('#frmSupplierPayments #butNew').show();
	$('#frmSupplierPayments #butSave').show();
	$('#frmSupplierPayments #butPrint').show();
  }
  //permision for edit 
  if(intEditx)
  {
  	$('#frmSupplierPayments #butSave').show();
	$('#frmSupplierPayments #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
	$('#frmSupplierPayments #butPrint').show();
  }
  //permision for delete
  if(intDeletex)
  {
  	$('#frmSupplierPayments #butDelete').show();
	$('#frmSupplierPayments #cboSearch').removeAttr('disabled');
  }
  //permision for view
  if(intViewx)
  {
	$('#frmSupplierPayments #cboSearch').removeAttr('disabled');
  }
  
  $('#frmSupplierPayments #chkEdit').click(function(){
	  if($('#frmSupplierPayments #chkEdit').attr('checked'))
	  {
		  $("#frmSupplierPayments #txtRate").attr("readonly","");
		  $('#frmSupplierPayments #txtRate').focus();
	  }
	  else
	  {
		  $('#frmSupplierPayments #txtRate').val('');
		  $("#frmSupplierPayments #txtRate").attr("readonly","readonly");
		  $('#frmSupplierPayments #cboCurrency').change();
	  }
  });
  
  $('#frmSupplierPayments #chkCurrency').click(function(){
	  if($('#frmSupplierPayments #chkCurrency').attr('checked'))
	  {
		  $("#frmSupplierPayments #txtRecAmount").attr("readonly","");
		  $('#frmSupplierPayments #txtRecAmount').focus();
	  }
	  else
	  {
		  $("#frmSupplierPayments #txtRecAmount").attr("readonly","readonly");
	  }
  });
    //===================================================================
 	$('#frmSupplierPayments #chkAutoManual').click(function(){
	  if($('#frmSupplierPayments #chkAutoManual').attr('checked'))
	  {
		  amStatus = "Auto";
		  $('#frmSupplierPayments #amStatus').val('Auto');
		  $('#frmSupplierPayments #txtNo').val('');
		  $("#frmSupplierPayments #txtNo").attr("readonly","readonly");
		  $('#frmSupplierPayments #txtNo').removeClass('validate[required]');
	  }
	  else
	  {
		  amStatus = "Manual";
		  $('#frmSupplierPayments #amStatus').val('Manual');
		  $('#frmSupplierPayments #txtNo').val('');
		  $("#frmSupplierPayments #txtNo").attr("readonly","");
		  $('#frmSupplierPayments #txtNo').focus();
		  $('#frmSupplierPayments #txtNo').addClass('validate[required]');
	  }
  });
 //===================================================================
///////////////////////////////////////////////////////////////////
$("input[name^=txtPayAccAmount]").live("keyup", finalAmount);
$("input[name^=txtPayAmount]").live("keyup", receivedAmount);
///////////////////////////////////////////////////////////////////
$(".checkRow").live('click',function(){
if($(this).attr("checked") == true)
{
	$(this).parent().parent().addClass("highlight");
	$(this).parent().parent().find(".payAmount").attr("disabled","");
	toBePaidAmount = eval($(this).parent().parent().find(".toBePaid").html()) + eval($(this).parent().parent().find(".payAmount").val()==''?0.00:$(this).parent().parent().find(".payAmount").val());
	$(this).parent().parent().find(".payAmount").val(toBePaidAmount);
	$(this).parent().parent().find(".toBePaid").html(((toBePaidAmount)==''?0.00:toBePaidAmount - $(this).parent().parent().find(".payAmount").val()).toFixed(4));
	receivedAmount();
	remainingToBePaid();
}
else
{
	$(this).parent().parent().removeClass("highlight");
	$(this).parent().parent().find(".payAmount").attr("disabled","disabled");
	toBePaidAmount = eval(($(this).parent().parent().find(".payAmount").val())==''?0.00:$(this).parent().parent().find(".payAmount").val()) + eval($(this).parent().parent().find(".toBePaid").html());
	$(this).parent().parent().find(".toBePaid").html(toBePaidAmount.toFixed(4));
	$(this).parent().parent().find(".payAmount").val('');
	receivedAmount();
	remainingToBePaid();
}
});
$(".payAmount").live('click',function(){
	toBePaidAmount = eval($(this).parent().parent().find(".toBePaid").html()) + eval(($(this).parent().parent().find(".payAmount").val())==''?0.00:$(this).parent().parent().find(".payAmount").val());
});
///////////////////////////get supplier invoice////////////////////
$('#cboSupplier').change(function(){
	
	var url = "supplierPayments-db-get.php?requestType=getSupplierCurrency&supplierId="+$(this).val();
	var httpobj = $.ajax({
	url:url,
	dataType:'json',
	async:false,
	success:function(json)
	{
		$('.invTpe').attr('checked',false); //

		$('#frmSupplierPayments #cboCurrency').val(json.currency);
		$('#frmSupplierPayments #cboCurrency').change();	
	}
	});
	
	var url = "supplierPayments-db-get.php?requestType=getInvoice&supplierId="+$(this).val();
	var obj = $.ajax({url:url,async:false});
	document.getElementById('allInvoice').innerHTML=obj.responseText;
	callTotalAmount();
});
////////////////////////////////////////////////////////////////////

//------------------------------------------------------------
   $('#frmSupplierPayments #cboPaymentsMethods').change(function(){
	var payMethod = $('#cboPaymentsMethods').val();
	if(payMethod==2)
	{
		document.getElementById("rwChequeDetails").style.display='';
	}
	else
	{
		document.getElementById("rwChequeDetails").style.display='none';
	}
  });
//----------------------------------------------------------

///////////////////////////get pay amount//////////////////////////
$('.cusAccount').live('change',function(){
	$(this).parent().parent().find(".calTotAmt").val((eval($('#txtTotAmount').val()==''?0:$('#txtTotAmount').val()) - eval($('#txtFinalAmount').val()==''?0:$('#txtFinalAmount').val())).toFixed(4));
	finalAmount();
});
//////////////////////////////////////////////////////////////////

////////////////////////get exchange rate//////////////////////////
$('#cboCurrency').change(function(){
	var url = "supplierPayments-db-get.php?requestType=getExchangeRate&currencyId="+$(this).val()+'&exchangeDate='+$('#txtDate').val();
	var obj = $.ajax({url:url,dataType:'json',success:function(json){
		
		$('#rdoBuying').val(json.buyingRate);
		$('#rdoSelling').val(json.sellingRate);
		$('#rdoAverage').val(((eval(json.buyingRate) + eval(json.sellingRate))/2).toFixed(4));
		$('#rdoSelling').click();
		},async:false});
});
///////////////////////////////////////////////////////////////////
$('.rdoRate').click(function(){
  $('#txtRate').val($(this).val());
});
//save button click event
$('#frmSupplierPayments #butSave').click(function(){
if(existingMsgDate == "")
{
//-------------------------------------------------------------------
	var poNo = "";
	var grnNo = "";
	var docNo = "";
	var docDate = "";
	var amount = "";
	var toBePaid = "";
	var currencyId = "";
	var rate = "";
	var payAmount = "";
	var docType = "";
	var docRefNo = ""; 
			
 value="[ ";
	$('#tblMainGrid1 tr:not(:first):not(:last)').each(function(){
		if ($(this).find('.checkRow').attr('checked')) 
		{
			poNo		= $(this).find(".po").html();
			grnNo		= $(this).find(".grn").html();
			docNo 		= $(this).find(".docNo").attr('id');
			docDate 	= $(this).find(".docDate").html();
			amount 		= $(this).find(".amount").html();
			toBePaid 	= $(this).find(".toBePaid").html();
			currency 	= $(this).find(".docCurrency").html();
			rate	 	= $(this).find(".docRate").html();
			payAmount 	= $(this).find(".payAmount").val();
			docType		= $(this).find(".docType").html();
			docRefNo 	= $(this).find(".docType").attr('id');
			
		value += '{ "poNo":"'+poNo+'", "grnNo":"'+grnNo+'", "docNo": "'+docNo+'", "docDate": "'+docDate+'", "amount": "'+amount+'", "toBePaid": "'+toBePaid+'", "currency": "'+currency+'", "rate": "'+rate+'", "payAmount": "'+payAmount+'", "docType": "'+docType+'", "docRefNo":"'+docRefNo+'"},';
		}
	});
	
	value = value.substr(0,value.length-1);
	value += " ]";
//---------------------------------------------------------------------------
	var accId = "";
	var accAmount = "";
	var memo = "";
	var dimension = "";
			
 accValue="[ ";
	$('#tblMainGrid2 tr:not(:first)').each(function(){
		
		accId		= $(this).find(".cusAccount").val();
		accAmount 	= $(this).find(".calTotAmt").val();
		memo 		= $(this).find(".memo").val();
		dimension 	= $(this).find(".dimension").val();
		
	accValue += '{ "accId":"'+accId+'", "accAmount": "'+accAmount+'", "memo": "'+URLEncode(memo)+'", "dimension": "'+dimension+'"},';
	});
	
	accValue = accValue.substr(0,accValue.length-1);
	accValue += " ]";
//---------------------------------------------------------------------------
	var requestType = '';
	if ($('#frmSupplierPayments').validationEngine('validate')&& (getAvailability == "" || amStatus == "Auto"))
    {
		//showWaiting();
		if(value != '[ ]')
		{
			if($('#chkCurrency').attr('checked'))
			{
				if((eval($('#txtRecAmount').val()) - eval($('#txtFinalAmount').val()))==0)
				{
					$('#frmSupplierPayments #cboSupplier').attr("disabled","");
					if(($('#txtNo').val()=='' && amStatus == "Auto") || ($('#txtNo').val()!='' && amStatus == "Manual"))
						requestType = 'add';
					else
						requestType = 'edit';
					
					var url = "supplierPayments-db-set.php";
					var obj = $.ajax({
						url:url,
						dataType: "json",
						type:'POST',  
						data:$("#frmSupplierPayments").serialize()+'&requestType='+requestType+'&cboSearch='+id+'&recPayDetail='+value+'&recPayAccDetail='+accValue+'&amStatus='+amStatus,
						async:false,
						
						success:function(json){
								$('#frmSupplierPayments #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									//$('#frmSupplierPayments').get(0).reset(); hideWaitng();
									$('#frmSupplierPayments #cboSupplier').attr("disabled","disabled");
									var t=setTimeout("alertx()",1000);
									$('#txtNo').val(json.receiptNo);
									amStatus = "Auto";
									loadCombo_frmSupplierPayments();
									return;
								}
								var t=setTimeout("alertx()",3000);
							},
						error:function(xhr,status){
							$('#frmSupplierPayments #cboSupplier').attr("disabled","disabled");
								
								$('#frmSupplierPayments #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
				}
				else
				{
					$('#frmSupplierPayments #butSave').validationEngine('showPrompt', 'You cannot allow this process untill received  amount and total amount are same','fail');
					var t=setTimeout("alertx()",5000);
				}
			}
			else
			{
				if(((eval($('#txtRecAmount').val()) - eval($('#txtFinalAmount').val()))==0) && ((eval($('#txtRecAmount').val()) - eval($('#txtTotAmount').val()))==0))
				{
					$('#frmSupplierPayments #cboSupplier').attr("disabled","");
					if(($('#txtNo').val()=='' && amStatus == "Auto") || ($('#txtNo').val()!='' && amStatus == "Manual"))
						requestType = 'add';
					else
						requestType = 'edit';
					
					var url = "supplierPayments-db-set.php";
					var obj = $.ajax({
						url:url,
						dataType: "json",
						type:'POST',  
						data:$("#frmSupplierPayments").serialize()+'&requestType='+requestType+'&cboSearch='+id+'&recPayDetail='+value+'&recPayAccDetail='+accValue+'&amStatus='+amStatus,
						async:false,
						
						success:function(json){
								$('#frmSupplierPayments #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									//$('#frmSupplierPayments').get(0).reset(); hideWaitng();
									$('#frmSupplierPayments #cboSupplier').attr("disabled","disabled");
									var t=setTimeout("alertx()",1000);
									$('#txtNo').val(json.receiptNo);
									amStatus = "Auto";
									loadCombo_frmSupplierPayments();
									return;
								}
								var t=setTimeout("alertx()",3000);
							},
						error:function(xhr,status){
							$('#frmSupplierPayments #cboSupplier').attr("disabled","disabled");
								
								$('#frmSupplierPayments #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
				}
				else
				{
					$('#frmSupplierPayments #butSave').validationEngine('showPrompt', 'You cannot allow this process untill received  amount, paying amount and total amount are same','fail');
					var t=setTimeout("alertx()",5000);
				}
			}
		}
		else
		{
			$('#frmSupplierPayments #butSave').validationEngine('showPrompt', 'You cannot allow this process!','fail');
			var t=setTimeout("alertx()",5000);
		}
	}
}
else
{
	$('#frmSupplierPayments #butSave').validationEngine('showPrompt', existingMsgDate,'fail');
	var t=setTimeout("alertx()",5000);
}
});
/////////////////////////////////////////////////////
//// load invoice details //////////////////////////
/////////////////////////////////////////////////////
$('#frmSupplierPayments #cboSearch').click(function(){
   $('#frmSupplierPayments').validationEngine('hide');
});
$('#frmSupplierPayments #cboSearch').change(function(){
//---------------------------------------------------
	existingMsgDate = "";
	amStatus = "Auto";
	document.getElementById("chkAutoManual").style.display='none';
	document.getElementById("amStatus").style.display='none';
	$("#frmSupplierPayments #txtNo").attr("readonly","readonly");
//---------------------------------------------------
$('#frmSupplierPayments').validationEngine('hide');
var url = "supplierPayments-db-get.php";
if($('#frmSupplierPayments #cboSearch').val()=='')
{
//---------------------------------------------------
	if(intAddx)
	{
		$('#frmSupplierPayments #butSave').show();
	}
	if(intEditx)
	{
		$('#frmSupplierPayments #butSave').show();
	}
	amStatus = "Auto";
	document.getElementById("chkAutoManual").style.display='';
	document.getElementById("amStatus").style.display='';
	$('#frmSupplierPayments #txtNo').removeClass('validate[required]');
	$("#frmSupplierPayments #txtNo").attr("readonly","readonly");
//---------------------------------------------------
	document.getElementById("rwChequeDetails").style.display='none';
	document.getElementById('allInvoice').innerHTML = "";
		$('#tblMainGrid2 >tbody >tr').each(function(){
			if($(this).index()!=0 && $(this).index()!=1 )
			{
				$(this).remove();
			}
		});
	$('#frmSupplierPayments').get(0).reset();return;	
}
$('#frmSupplierPayments #butSave').hide();
$('#txtNo').val($(this).val());
var httpobj = $.ajax({
	url:url,
	dataType:'json',
	data:'requestType=loadDetails&id='+URLEncode($(this).val()),
	async:false,
	success:function(json)
	{
		//json  = eval('('+json+')');
		$('#frmSupplierPayments #cboSupplier').val(json.supplier);
		$('#frmSupplierPayments #cboSupplier').attr("disabled","disabled");
		$('#frmSupplierPayments #txtDate').val(json.date);
		$('#frmSupplierPayments #cboCurrency').val(json.currency);
		$('#frmSupplierPayments #txtRate').val(json.rate);
		//$('#frmSupplierPayments #txtRecAmount').val(json.amount);
		$('#frmSupplierPayments #cboPaymentsMethods').val(json.payMethod);
		$('#frmSupplierPayments #cboPaymentsMethods').change();//--->
		$('#frmSupplierPayments #txtRefNo').val(json.payRefNo);
		$('#frmSupplierPayments #txtRefDate').val(json.payRefDate);
		$('#frmSupplierPayments #chkPosted').attr('checked',json.isPosted);
		$('#frmSupplierPayments #txtRefOrg').val(json.payRefOrg);
		$('#frmSupplierPayments #txtRemarks').val(json.remark);
		
		//--------------------------------------------------
		$('#tblMainGrid1 >tbody >tr').each(function(){
			if($(this).index()!=0 && $(this).index()!=1 )
			{
				$(this).remove();
			}
		});
		document.getElementById('allInvoice').innerHTML = "";
		var tBodyDetail = "";

		if(json.detailVal!=null)
		{
			var rowId = $('#tblMainGrid1').find('tr').length;
			var tbl = document.getElementById('tblMainGrid1');
			rows = $('#tblMainGrid1').find('tr').length;
			for(var j=0;j<=json.detailVal.length-1;j++)
			{
				tBodyDetail	= json.detailVal[j].tBodyDetail;
				if(j != json.detailVal.length)
				{
					document.getElementById('allInvoice').innerHTML+=tBodyDetail;
				}
			}
			
			//------------------------------------------------------
//			var url = "supplierPayments-db-get.php?requestType=getInvoice&supplierId="+$('#cboSupplier').val();
//			var obj = $.ajax({url:url,async:false});
//			document.getElementById('allInvoice').innerHTML+=obj.responseText;
			callTotalAmount();
			receivedAmount();
			finalAmount();
			//------------------------------------------------------
		}
		else
		{
			
		}
	   //--------------------------------------------------
	   //--------------------------------------------------
		$('#tblMainGrid2 >tbody >tr').each(function(){
			if($(this).index()!=0 && $(this).index()!=1 )
			{
				$(this).remove();
			}
		});
		var chartAcc 	= "";
		var amount 		= "";
		var memo		= "";
		var dimension	= "";
		if(json.detailAccVal!=null)
		{
			var rowId = $('#tblMainGrid2').find('tr').length;
			var tbl = document.getElementById('tblMainGrid2');
			rows = $('#tblMainGrid2').find('tr').length;
			for(var j=0;j<=json.detailAccVal.length-1;j++)
			{
				chartAcc	= json.detailAccVal[j].chartAcc;
				amount		= json.detailAccVal[j].amount;
				memo		= json.detailAccVal[j].memo;
				dimension	= json.detailAccVal[j].dimension;
				if(j != json.detailAccVal.length-1)
				{
					tbl.insertRow(rows);
					tbl.rows[rows].innerHTML = tbl.rows[rows-1].innerHTML;
					tbl.rows[rows].cells[1].childNodes[1].value = chartAcc;
					tbl.rows[rows].cells[2].childNodes[1].value = amount;
					tbl.rows[rows].cells[3].childNodes[1].value = memo;
					tbl.rows[rows].cells[4].childNodes[1].value = dimension;
				}
				else
				{
					tbl.rows[1].cells[1].childNodes[1].value = chartAcc;
					tbl.rows[1].cells[2].childNodes[1].value = amount;
					tbl.rows[1].cells[3].childNodes[1].value = memo;
					tbl.rows[1].cells[4].childNodes[1].value = dimension;
				}
			}
			finalAmount();
		}
		else
		{
			
		}
	   //--------------------------------------------------
	}
});
});
//////////// end of load details /////////////////

  	$('#frmSupplierPayments #butNew').click(function(){
		//---------------------------------------------------
		if(intAddx)
		{
			$('#frmSupplierPayments #butSave').show();
		}
		if(intEditx)
		{
			$('#frmSupplierPayments #butSave').show();
		}
			existingMsgDate = "";
			amStatus = "Auto";
			$("#frmSupplierPayments #txtNo").attr("readonly","readonly");
			$('#frmSupplierPayments #chkAutoManual').attr('checked')
			$("#frmSupplierPayments #chkAutoManual").attr("disabled","");
			document.getElementById("chkAutoManual").style.display='';
			document.getElementById("amStatus").style.display='';
			$('#frmSupplierPayments #txtNo').removeClass('validate[required]');
		//---------------------------------------------------
		$('#frmSupplierPayments').get(0).reset();
		document.getElementById('allInvoice').innerHTML = "";
		$('#tblMainGrid2 >tbody >tr').each(function(){
			if($(this).index()!=0 && $(this).index()!=1 )
			{
				$(this).remove();
			}
		});
		$('#frmSupplierPayments #cboPaymentsMethods').change();//--->
		$('#frmSupplierPayments #cboSupplier').attr("disabled","");
		loadCombo_frmSupplierPayments();
		$('#frmSupplierPayments #cboSupplier').focus();
	});
	$('#frmSupplierPayments #butDelete').click(function(){
		if($('#frmSupplierPayments #cboSearch').val()=='')
		{
			$('#frmSupplierPayments #butDelete').validationEngine('showPrompt', 'Please select Receipt.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmSupplierPayments #cboSearch option:selected').text()+'" ?',{
			buttons: { Ok: true, Cancel: false },
			callback: function(v,m,f){
			if(v)
			{
					var url = "supplierPayments-db-set.php";
					var httpobj = $.ajax({
						url:url,
						dataType:'json',
						data:'requestType=delete&cboSearch='+URLEncode($('#frmSupplierPayments #cboSearch').val()),
						async:false,
						success:function(json){
							
							$('#frmSupplierPayments #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
							
							if(json.type=='pass')
							{
								$('#frmSupplierPayments').get(0).reset();
								document.getElementById('allInvoice').innerHTML = "";
								$('#tblMainGrid2 >tbody >tr').each(function(){
									if($(this).index()!=0 && $(this).index()!=1 )
									{
										$(this).remove();
									}
								});
								$('#frmSupplierPayments #cboPaymentsMethods').change();//--->
								$('#frmSupplierPayments #cboSupplier').attr("disabled","");
								loadCombo_frmSupplierPayments();
								if(intAddx)
								{
									$('#frmSupplierPayments #butSave').show();
								}
								if(intEditx)
								{
									$('#frmSupplierPayments #butSave').show();
								}
								var t=setTimeout("alertDelete()",1000);return;
							}	
							var t=setTimeout("alertDelete()",3000);
						}	 
					});
			}
		}
			});	
		}
	});
	
	$('#frmSupplierPayments #butPrint').click(function(){
		if($('#frmSupplierPayments #txtNo').val()=='')
		{
			$('#frmSupplierPayments #butPrint').validationEngine('showPrompt', 'Please select Voucher.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var myurl = 'supplierPaymentsDetails.php?id='+URLEncode($('#frmSupplierPayments #txtNo').val());
    		window.open(myurl); 
		}
	});
	
	// TODO: ===========Add by dulaskhi 2013.03.18===========
	getSupplierList();   
	
});
////////////////////// calculation ////////////////////////////////
function finalAmount()
{
	var finalTotal = 0.00;
	$(".calTotAmt").each( function(){
          finalTotal += eval($(this).val()==''?0.00:$(this).val());
	});
	$('#txtFinalAmount').val(finalTotal.toFixed(4));
	$('#frmSupplierPayments #txtRecAmount').val($('#txtFinalAmount').val());
}
///////////////////////////////////////////////////////////////////
function receivedAmount()
{
	$(this).parent().parent().find(".toBePaid").html(((toBePaidAmount)==''?0.00:toBePaidAmount - $(this).parent().parent().find(".payAmount").val()).toFixed(4));
	//alert($(this).val());
	
	var receiveTotal = 0.00;
	$(".payAmount").each( function(){
          receiveTotal += eval($(this).val()==''?0.00:$(this).val());
	});
	$('#txtRecAmount').val(receiveTotal.toFixed(4));
	$('#txtTotAmount').val(receiveTotal.toFixed(4));
	
	remainingToBePaid();	
}
///////////////////////////////////////////////////////////////////
function remainingToBePaid()
{
	var toBePaidTotal = 0.00;
	$(".toBePaid").each( function(){
          toBePaidTotal += eval($(this).html()==''?0.00:$(this).html());
	});
	$('#txttoBePaid').val(toBePaidTotal.toFixed(4));
}
///////////////////////////////////////////////////////////////////
function callTotalAmount()
{
	var totalAmount = 0.00;
	$(".amount").each( function(){
          totalAmount += eval($(this).html()==''?0.00:$(this).html());
	});
	$('#txtTotalAmount').val(totalAmount.toFixed(4));
	
	var totalToBePaid = 0.00;
	$(".toBePaid").each( function(){
          totalToBePaid += eval($(this).html()==''?0.00:$(this).html());
	});
	$('#txttoBePaid').val(totalToBePaid.toFixed(4));
	
	$val = 0.00;
	$('#txtRecAmount').val($val.toFixed(4));
	$('#txtTotAmount').val($val.toFixed(4));	
}
/////////////////////////////////////////////////////////////////////
function loadCombo_frmSupplierPayments()
{
	var url 	= "supplierPayments-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmSupplierPayments #cboSearch').html(httpobj.responseText);
}
function alertx()
{
	$('#frmSupplierPayments #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmSupplierPayments #butDelete').validationEngine('hide') ;
	$('#frmSupplierPayments #butPrint').validationEngine('hide') ;
}

//=========Add by dulakshi 2013.03.18===========
//====================Get Supplier List==============================
function getSupplierList()
{	
	$ledgerAccId = $('#frmSupplierPayments #cboLedgerAcc').val();
	
	var url = "supplierPayments-db-get.php?requestType=loadSupplier&ledgerAcc="+$ledgerAccId;
	var httpobj = $.ajax({url:url,async:false})
	$('#frmSupplierPayments #cboSupplier').html(httpobj.responseText);
}
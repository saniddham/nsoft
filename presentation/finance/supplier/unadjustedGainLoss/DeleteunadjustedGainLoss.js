$(document).ready(function() {
    $('#frmDeleteSuptUnadjustedGL #butDelete').click(function(){
        if($('#frmDeleteSuptUnadjustedGL #cboProcess').val()==''){
            
            $('#frmDeleteSuptUnadjustedGL #butDelete').validationEngine('showPrompt', 'Please select Process.', 'fail');
            var t=setTimeout("alertDelete()",1000);	
        }
        else{
            var val = $.prompt('Are you sure you want to delete process "'+$('#frmDeleteSuptUnadjustedGL #cboProcess option:selected').text()+'" ?',{
                buttons: { Ok: true, Cancel: false },
                callback: function(v,m,f){
                    if(v){
                        $('#divMaskProces').attr("class","maskShow mask");
                        var prosesId=$('#frmDeleteSuptUnadjustedGL #cboProcess').val();
                        var url ='deleteSupUnadjustedGainLoss-db-set.php?requestType=delete&prosesId='+prosesId;
                        var obj=$.ajax({
                            url:url,
                            dataType:'json',
                            success:function(json){
                                $('#divMaskProces').attr("class","maskHide mask");
                                $('#frmDeleteSuptUnadjustedGL #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
                                var t=setTimeout("alertDelete()",3000);

                            },
                            async:false
                        });
                    }
                }
            });
               
                    
        }
        
    });

});

function alertDelete(){
    $('#frmDeleteSuptUnadjustedGL #butDelete').validationEngine('hide') ;
}

// JavaScript Document
function functionList()
{
    if(invoRefNo!='')
    {
        $('#frmPurchaseInvoice #cboSearch').val(invoRefNo);
        $('#frmPurchaseInvoice #cboSearch').change();
    }
}
var rows = 1;
var invoType = "";
var trnTax = "";
function insertRow()
{
    var tbl = document.getElementById('tblMainGrid2');	
    rows = tbl.rows.length;
    //tbl.rows[1].cells[5].childNodes[0].nodeValue;
    tbl.insertRow(rows);
    tbl.rows[rows].innerHTML = tbl.rows[rows-1].innerHTML;
//loadJs();
}
$(document).ready(function() {
	
    var id = '';
    $("#frmPurchaseInvoice").validationEngine();
    $('#frmPurchaseInvoice #txtNo').focus();

    $('.delImg').live('click',function(){
        var rowId = $(this).parent().parent().parent().find('tr').length;
        if(rowId!=2)
            $(this).parent().parent().remove();
        callSubTotal();
        callTaxTotal();
    });

    $('.delImg').css('cursor', 'pointer');

    //permision for add 
    if(intAddx)
    {
        $('#frmPurchaseInvoice #butNew').show();
        $('#frmPurchaseInvoice #butSave').show();
		$('#frmPurchaseInvoice #butPrint').show();
    }
    //permision for edit 
    if(intEditx)
    {
        $('#frmPurchaseInvoice #butSave').show();
        $('#frmPurchaseInvoice #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
		$('#frmPurchaseInvoice #butPrint').show();
    }
    //permision for delete
    if(intDeletex)
    {
        $('#frmPurchaseInvoice #butDelete').show();
        $('#frmPurchaseInvoice #cboSearch').removeAttr('disabled');
    }
    //permision for view
    if(intViewx)
    {
        $('#frmPurchaseInvoice #cboSearch').removeAttr('disabled');
    }
  
    $('#frmPurchaseInvoice #chkEdit').click(function(){
        if($('#frmPurchaseInvoice #chkEdit').attr('checked'))
        {
            $("#frmPurchaseInvoice #txtRate").attr("readonly","");
            $('#frmPurchaseInvoice #txtRate').focus();
        }
        else
        {
            $('#frmPurchaseInvoice #txtRate').val('');
            $("#frmPurchaseInvoice #txtRate").attr("readonly","readonly");
            $('#frmPurchaseInvoice #cboCurrency').change();
        }
    });
	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	$('#frmPurchaseInvoice #chkBillTax').click(function(){
        if($('#frmPurchaseInvoice #chkBillTax').attr('checked'))
        {
            $("#frmPurchaseInvoice #txtBillTaxAmount").attr("disabled","");
			$("#frmPurchaseInvoice #cboOtherRec").attr("disabled","");
			$("#frmPurchaseInvoice #txtBillTaxAmount").addClass('validate[required]');
			$("#frmPurchaseInvoice #cboOtherRec").addClass('validate[required]');
            $('#frmPurchaseInvoice #cboOtherRec').focus();
        }
        else
        {
            $('#frmPurchaseInvoice #txtBillTaxAmount').val('');
			$('#frmPurchaseInvoice #cboOtherRec').val('');
			$("#frmPurchaseInvoice #txtBillTaxAmount").removeClass('validate[required]');
			$("#frmPurchaseInvoice #cboOtherRec").removeClass('validate[required]');
            $("#frmPurchaseInvoice #txtBillTaxAmount").attr("disabled","disabled");
			$("#frmPurchaseInvoice #cboOtherRec").attr("disabled","disabled");
        }
		calculateBillTax();
    });
	
	$("input[name^=txtBillTaxAmount]").live("keyup", calculateBillTax);
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    ///////////////////////////////////////////////////////////////////
    $("input[name^=txtQty]").live("keyup", calculate);
    $("input[name^=txtUnitPrice]").live("keyup", calculate);
    $("input[name^=txtDiscount]").live("keyup", calculate);
    ///////////////////////////////////////////////////////////////////

    ///////////////////////////get supplier address and invoice type////////////////////
    $('#cboSupplier').change(function(){
        var url = "purchaseInvoice-db-get.php?requestType=getSupplierAddress&supplierId="+$(this).val();
        var httpobj = $.ajax({
            url:url,
            dataType:'json',
            async:false,
            success:function(json)
            {
                $('.invTpe').attr('checked',false); //
		
                $('#txtAddress').val(json.address);
				if(json.invoType)
				{
                	$('#frmPurchaseInvoice #'+json.invoType).attr('checked',true); //
				}
				$('#frmPurchaseInvoice #cboCurrency').val(json.currency);
				$('#frmPurchaseInvoice #cboCurrency').change();
                invoType = json.invoType;	
            }
        });
    });
    ////////////////////////////////////////////////////////////////////

    ///////////////////////////get item description////////////////////
    $('.item').live('change',function(){
        var url = "purchaseInvoice-db-get.php?requestType=getItemDescription&itemId="+$(this).val();
        var obj = $.ajax({
            url:url,
            async:false
        });
        $(this).parent().parent().find('td').eq(2).children().val(obj.responseText);
    });
    ////////////////////////////////////////////////////////////////////

    ///////////////////////////get tax values//////////////////////////
    $('.taxGroup').live('change',function(){
        var operation = '';
        var amount = $(this).parent().parent().find('td').eq(7).children().val();
        var jsonTaxCode="[ ";
        //var taxCode = $(this).find('option:selected').text();
        var taxId = callTaxProcess($(this).val());
        var arrTax = taxId.split('/');
        //var arrTaxInclusive = taxId.split('/');
        //var arrTaxExclusive = taxId.split('-');

        if(arrTax.length == 1)
        {
            operation = 'Isolated';
            jsonTaxCode += '{ "taxId":"'+taxId+'"},';
        }
        else if(arrTax.length > 1)
        {
            operation = arrTax[1];
            for(var i =0; i < arrTax.length; i=i+2)
            {
                jsonTaxCode += '{ "taxId":"'+arrTax[i]+'"},';
            }
        }

        jsonTaxCode = jsonTaxCode.substr(0,jsonTaxCode.length-1);
        jsonTaxCode += " ]";
        var url = "purchaseInvoice-db-get.php?requestType=getTaxValue&arrTaxCode="+jsonTaxCode+"&opType="+operation+"&valAmount="+amount;	
        var obj = $.ajax({
            url:url,
            async:false
        });
        var values = obj.responseText.split('/');
        $(this).parent().parent().find('td').eq(10).children().val(values[0]);
	
        if(arrTax.length == 1)
        {
            trnTax="[ ";
            trnTax += '{ "taxId":"'+taxId+'", "taxValue": "'+values[1]+'"},';
            trnTax = trnTax.substr(0,trnTax.length-1);
            trnTax += " ]";
        }
        else if(arrTax.length > 1)
        {
            trnTax="[ ";
            var k = 1;
            for(var i =0; i < arrTax.length; i=i+2)
            {
                trnTax += '{ "taxId":"'+arrTax[i]+'", "taxValue": "'+values[k]+'"},';
                k++;
            }
            trnTax = trnTax.substr(0,trnTax.length-1);
            trnTax += " ]";
        }
        $(this).parent().parent().find('td:eq(10)').attr('id',trnTax);
        callTaxTotal();
    });
    ////////////////////////////////////////////////////////////////////

    ////////////////////////get exchange rate//////////////////////////
    $('#cboCurrency').change(function(){
        var url = "purchaseInvoice-db-get.php?requestType=getExchangeRate&currencyId="+$(this).val()+'&exchangeDate='+$('#txtDate').val();
        var obj = $.ajax({
            url:url,
            dataType:'json',
            success:function(json){
		
                $('#rdoBuying').val(json.buyingRate);
                $('#rdoSelling').val(json.sellingRate);
                $('#rdoAverage').val(((eval(json.buyingRate) + eval(json.sellingRate))/2).toFixed(4));
                $('#rdoSelling').click();
            },
            async:false
        });
    });
    ///////////////////////////////////////////////////////////////////
    $('.rdoRate').click(function(){
        $('#txtRate').val($(this).val());
    });
    //save button click event
    $('#frmPurchaseInvoice #butSave').click(function(){
	if(existingMsgDate == "")
	{
        changeReadOnly(false);
        $('#frmPurchaseInvoice #chkAuto').attr('disabled',false);  
		$("#frmPurchaseInvoice #txtBillTaxAmount").attr("disabled","");
		$("#frmPurchaseInvoice #cboOtherRec").attr("disabled","");  
        var itemId = "";
        var itemDesc = "";
        var uomId = "";
        var qty = "";
        var unitPrice = "";
        var discount = "";
        var taxGroupId = "";
        var dimensionId = "";
        var amount = "";
        var trnTaxVal	= "";
			
        value="[ ";
        $('#tblMainGrid2 tr:not(:first)').each(function(){
		
            itemId		= $(this).find(".item").val();
            itemDesc 	= $(this).find(".description").val();
            uomId 		= $(this).find(".uom").val();
            qty 		= $(this).find(".qty").val();
            unitPrice 	= $(this).find(".unitPrice").val();
            discount 	= $(this).find(".discount").val();
            taxAmount 	= $(this).find(".taxWith").val();
            taxGroupId 	= $(this).find(".taxGroup").val();
            dimensionId = $(this).find(".dimension").val();
            amount 		= $(this).find(".amount").val();
            trnTaxVal	= $(this).find(".taxVal").attr('id');
		
            value += '{ "itemId":"'+itemId+'", "itemDesc": "'+URLEncode(itemDesc)+'", "uomId": "'+uomId+'", "qty": "'+qty+'", "unitPrice": "'+unitPrice+'", "discount": "'+discount+'", "taxGroupId": "'+taxGroupId+'", "taxAmount": "'+taxAmount+'", "dimensionId": "'+dimensionId+'", "amount": "'+amount+'", "trnTaxVal":'+trnTaxVal+'},';
        });
	
        value = value.substr(0,value.length-1);
        value += " ]";

        var requestType = '';
        if ($('#frmPurchaseInvoice').validationEngine('validate'))   
        { 
            if($('#cboSearch').val()=='')
                requestType = 'add';
			
            else
                requestType = 'edit';
            var id=$('#cboSearch').val();
                    
            //check automated
            var isChecked = $('#frmPurchaseInvoice #chkAuto').attr('checked')?true:false;                
            var url = "purchaseInvoice-db-set.php";
            var obj = $.ajax({
                url:url,
                dataType: "json",
				type:'post', 
                data:$("#frmPurchaseInvoice").serialize()+'&requestType='+requestType+'&cboSearch='+id+'&purchaseDetails='+value+'&invoType='+invoType+'&automated='+isChecked,
                async:false,
			
                success:function(json){
                    $('#frmPurchaseInvoice #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
                    if(json.type=='pass')
                    {
                        //$('#frmPurchaseInvoice').get(0).reset();
                        loadCombo_frmPurchaseInvoice();
                        var t=setTimeout("alertx()",1000);
                        $('#cboSearch').val(json.invoiceNo);
                        return;
                    }
                    var t=setTimeout("alertx()",3000);
                },
                error:function(xhr,status){
					
                    $('#frmPurchaseInvoice #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
                    var t=setTimeout("alertx()",3000);
                }		
            });
        }
        //changeReadOnly(true);
        $('#frmPurchaseInvoice #chkAuto').attr('disabled',true);
		$("#frmPurchaseInvoice #txtBillTaxAmount").attr("disabled","disabled");
		$("#frmPurchaseInvoice #cboOtherRec").attr("disabled","disabled");
		}
		else
		{
			$('#frmPurchaseInvoice #butSave').validationEngine('showPrompt', existingMsgDate,'fail');
			var t=setTimeout("alertx()",5000);
		}
    });
    /////////////////////////////////////////////////////
    //// load invoice details //////////////////////////
    /////////////////////////////////////////////////////
    $('#frmPurchaseInvoice #cboSearch').click(function(){ 
        $('#frmPurchaseInvoice').validationEngine('hide');
    });
    $('#frmPurchaseInvoice #cboSearch').change(function(){  
		existingMsgDate = "";
        $('#frmPurchaseInvoice').validationEngine('hide');
        var url = "purchaseInvoice-db-get.php";
        if($('#frmPurchaseInvoice #cboSearch').val()=='')
        {
            $('#partPay').hide();
            $('#fullPay').hide();
            $('#overPay').hide();
            $('#pending').hide();
	
            $('#tblMainGrid2 >tbody >tr').each(function(){
                if($(this).index()!=0 && $(this).index()!=1 )
                {
                    $(this).remove();
                }
            });
            $('#frmPurchaseInvoice').get(0).reset();
            return;	
        }
        //$('#txtNo').val($(this).val());
        var httpobj = $.ajax({
            url:url,
            dataType:'json',
            data:'requestType=loadDetails&id='+URLEncode($(this).val()),
            async:false,
            success:function(json)
            {
                //json  = eval('('+json+')');
                $('.invTpe').attr('checked',false); //
                $('#frmPurchaseInvoice #txtNo').val(json.supInvoice);
                $('#frmPurchaseInvoice #cboSupplier').val(json.supplier);                
                $('#frmPurchaseInvoice #txtDate').val(json.date);
                $('#frmPurchaseInvoice #'+json.invoType).attr('checked',true); //
                invoType = json.invoType;	
                $('#frmPurchaseInvoice #txtAddress').val(json.address);
                $('#frmPurchaseInvoice #cboCurrency').val(json.currency);
                $('#frmPurchaseInvoice #txtRate').val(json.rate);
                $('#frmPurchaseInvoice #txtRemarks').val(json.remark);
                $('#frmPurchaseInvoice #txtPoNo').val(json.poNo);
                $('#frmPurchaseInvoice #cboPaymentsTerms').val(json.payTerms);
                $('#frmPurchaseInvoice #txtGrnNo').val(json.grnNo);
                $('#frmPurchaseInvoice #txtMessage').val(json.message);
				
				if(json.taxAuthority != null && json.billTaxAmount != '0')
				{
					$('#frmPurchaseInvoice #chkBillTax').attr('checked', true);
					$("#frmPurchaseInvoice #txtBillTaxAmount").attr("disabled","");
					$("#frmPurchaseInvoice #cboOtherRec").attr("disabled","");
					$("#frmPurchaseInvoice #txtBillTaxAmount").addClass('validate[required]');
					$("#frmPurchaseInvoice #cboOtherRec").addClass('validate[required]');
					$('#frmPurchaseInvoice #cboOtherRec').focus();
				}
				else
				{
					$('#frmPurchaseInvoice #chkBillTax').attr('checked', false);
					$('#frmPurchaseInvoice #txtBillTaxAmount').val('');
					$('#frmPurchaseInvoice #cboOtherRec').val('');
					$("#frmPurchaseInvoice #txtBillTaxAmount").removeClass('validate[required]');
					$("#frmPurchaseInvoice #cboOtherRec").removeClass('validate[required]');
					$("#frmPurchaseInvoice #txtBillTaxAmount").attr("disabled","disabled");
					$("#frmPurchaseInvoice #cboOtherRec").attr("disabled","disabled");
				}
				$('#frmPurchaseInvoice #cboOtherRec').val(json.taxAuthority);
				$('#frmPurchaseInvoice #txtBillTaxAmount').val(json.billTaxAmount);
                
                var automated=json.automated;
                if(automated==1){
                    $('#frmPurchaseInvoice #chkAuto').attr('checked',true);
                    changeReadOnly(true);                    
                }
                else{
                    $('#frmPurchaseInvoice #chkAuto').attr('checked',false);
                    changeReadOnly(false);
                }                
                $('#frmPurchaseInvoice #cboItem').html(json.autoItemList);

                if(json.totAmount != json.balAmount)
                {
                    if(json.balAmount > 0)
                    {
                        $('#partPay').show();
                        $('#fullPay').hide();
                        $('#overPay').hide();
                        $('#pending').hide();
                    }
                    else if(json.balAmount == 0)
                    {
                        $('#fullPay').show();
                        $('#partPay').hide();
                        $('#overPay').hide();
                        $('#pending').hide();
                    }
                    if(json.balAmount < 0)
                    {
                        $('#partPay').hide();
                        $('#fullPay').hide();
                        $('#overPay').show();
                        $('#pending').hide();
                    }
                }
                else
                {
                    $('#partPay').hide();
                    $('#fullPay').hide();
                    $('#overPay').hide();
                    $('#pending').show();
                }
		
                //--------------------------------------------------
                $('#tblMainGrid2 >tbody >tr').each(function(){
                    if($(this).index()!=0 && $(this).index()!=1 )
                    {
                        $(this).remove();
                    }
                });
                var itemId 		= "";
                var itemDesc 	= "";
                var uom			= "";
                var qty			= "";
                var unitPrice	= "";
                var discount	= "";
                var taxAmount 	= "";
                var tax			= "";
                var dimension 	= "";
                if(json.detailVal!=null)
                {
                    var rowId = $('#tblMainGrid2').find('tr').length;
                    var tbl = document.getElementById('tblMainGrid2');
                    rows = $('#tblMainGrid2').find('tr').length;
                    for(var j=0;j<=json.detailVal.length-1;j++)
                    {
                        itemId		= json.detailVal[j].itemId;
                        itemDesc	= json.detailVal[j].itemDesc;
                        uom			= json.detailVal[j].uom;
                        qty			= json.detailVal[j].qty;
                        unitPrice	= json.detailVal[j].unitPrice;
                        discount	= json.detailVal[j].discount;
                        taxAmount	= json.detailVal[j].taxAmount;
                        tax			= json.detailVal[j].taxGroup;
                        dimension	= json.detailVal[j].dimension;
                        if(j != json.detailVal.length-1)
                        {
                            //alert(json.detailVal.length);
                            tbl.insertRow(rows);
                            tbl.rows[rows].innerHTML = tbl.rows[rows-1].innerHTML;
                            tbl.rows[rows].cells[1].childNodes[1].value = itemId;
                            tbl.rows[rows].cells[2].childNodes[1].value = itemDesc;
                            tbl.rows[rows].cells[3].childNodes[1].value = uom;
                            tbl.rows[rows].cells[4].childNodes[1].value = qty;
                            tbl.rows[rows].cells[5].childNodes[1].value = unitPrice;
                            tbl.rows[rows].cells[6].childNodes[1].value = discount;
                            tbl.rows[rows].cells[7].childNodes[1].value = ((unitPrice*((100-discount)/100))*qty).toFixed(4);
                            //tbl.rows[rows].cells[8].childNodes[1].value = tax;
                            $('#tblMainGrid2 >tbody >tr:eq('+rows+')>td:eq(8)').find('.taxGroup').val(tax);
                            tbl.rows[rows].cells[9].childNodes[1].value = dimension;
                            tbl.rows[rows].cells[10].childNodes[1].value = taxAmount;
                            $('#tblMainGrid2 >tbody >tr:eq('+rows+')>td:eq(8)').find('.taxGroup').change();
                        }
                        else
                        {
                            tbl.rows[1].cells[1].childNodes[1].value = itemId;
                            tbl.rows[1].cells[2].childNodes[1].value = itemDesc;
                            tbl.rows[1].cells[3].childNodes[1].value = uom;
                            tbl.rows[1].cells[4].childNodes[1].value = qty;
                            tbl.rows[1].cells[5].childNodes[1].value = unitPrice;
                            tbl.rows[1].cells[6].childNodes[1].value = discount;
                            tbl.rows[1].cells[7].childNodes[1].value = ((unitPrice*((100-discount)/100))*qty).toFixed(4);
                            //tbl.rows[1].cells[8].childNodes[1].value = tax;
                            $('#tblMainGrid2 >tbody >tr:eq(1)>td:eq(8)').find('.taxGroup').val(tax);
                            tbl.rows[1].cells[9].childNodes[1].value = dimension;
                            tbl.rows[1].cells[10].childNodes[1].value = taxAmount;
                            $('#tblMainGrid2 >tbody >tr:eq(1)>td:eq(8)').find('.taxGroup').change();
                        }
                    //loadJs();
                    }
                    callSubTotal();
                    callTaxTotal();
                }
                else
                {
			
                }
            //--------------------------------------------------
            }
        });
    });
    //////////// end of load details /////////////////

    $('#frmPurchaseInvoice #butNew').click(function(){
		existingMsgDate = "";
        location.reload();
        changeReadOnly(false);
        $('#frmPurchaseInvoice').get(0).reset();
        $('#tblMainGrid2 >tbody >tr').each(function(){
            if($(this).index()!=0 && $(this).index()!=1 )
            {
                $(this).remove();
            }
        });
        $('#partPay').hide();
        $('#fullPay').hide();
        $('#overPay').hide();
        $('#pending').hide();
        loadCombo_frmPurchaseInvoice();
        $('#frmPurchaseInvoice #txtNo').focus();
    });
        
    $('.invTpe').click(function(){
        invoType = $(this).val();	
    });
    $('#frmPurchaseInvoice #butDelete').click(function(){
        if($('#frmPurchaseInvoice #cboSearch').val()=='')
        {
            $('#frmPurchaseInvoice #butDelete').validationEngine('showPrompt', 'Please select Invoice.', 'fail');
            var t=setTimeout("alertDelete()",1000);	
        }
        else
        {
            var supplierId=$('#frmPurchaseInvoice #cboSupplier').val()
            var $invoice=$('#frmPurchaseInvoice #txtNo').val()
            var val = $.prompt('Are you sure you want to delete "'+$('#frmPurchaseInvoice #cboSearch option:selected').text()+'" ?',{
                buttons: {
                    Ok: true, 
                    Cancel: false
                },
                callback: function(v,m,f){
                    if(v)
                    {
                        var url = "purchaseInvoice-db-set.php";
                        var httpobj = $.ajax({
                            url:url,
                            dataType:'json',
                            data:'requestType=delete&cboSearch='+URLEncode($('#frmPurchaseInvoice #cboSearch').val())+'&cboSupplier='+supplierId+'&txtNo='+$invoice,
                            async:false,
                            success:function(json){
							
                                $('#frmPurchaseInvoice #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
							
                                if(json.type=='pass')
                                {
                                    $('#frmPurchaseInvoice').get(0).reset();
                                    $('#tblMainGrid2 >tbody >tr').each(function(){
                                        if($(this).index()!=0 && $(this).index()!=1 )
                                        {
                                            $(this).remove();
                                        }
                                    });
                                    loadCombo_frmPurchaseInvoice();
                                    var t=setTimeout("alertDelete()",1000);
                                    return;
                                }	
                                var t=setTimeout("alertDelete()",3000);
                            }	 
                        });
                    }
                }
            });	
        }
    });
    //Dsisplay PO
    $('#frmPurchaseInvoice #txtPoNo').click(function(){
        $('#frmPurchaseInvoice #chkAuto').attr('disabled',false);
        if($('#frmPurchaseInvoice #chkAuto').attr('checked')){
            var po=$('#frmPurchaseInvoice #txtPoNo').val();
            var val=po.split("/");
            var url="../../../../presentation/procurement/purchaseOrder/listing/rptPurchaseOrder.php?poNo="+val[0]+"&year="+val[1];
            window.open(url);
        }
    
        $('#frmPurchaseInvoice #chkAuto').attr('disabled',true);
    });

	$('#frmPurchaseInvoice #butPrint').click(function(){
		if($('#frmPurchaseInvoice #cboSearch').val()=='')
		{
			$('#frmPurchaseInvoice #butPrint').validationEngine('showPrompt', 'Please select Invoice.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var myurl = 'purchaseInvoiceDetails.php?id='+URLEncode($('#frmPurchaseInvoice #cboSearch').val());
    		window.open(myurl); 
		}
	});
	// TODO: ===========Add by dulaskshi 2013.03.15===========
	getSupplierList();    

});

//-------------------------------------------functions area----------------------------------------------------------

////////////////////// calculation ////////////////////////////////
function calculate()
{ 
    var quantity =  ($(this).parent().parent().find('td').eq(4).children().val()==''?0:$(this).parent().parent().find('td').eq(4).children().val());

    var unitprice  = ($(this).parent().parent().find('td').eq(5).children().val()==''?0:$(this).parent().parent().find('td').eq(5).children().val());

    var discount  = ($(this).parent().parent().find('td').eq(6).children().val()==''?0:$(this).parent().parent().find('td').eq(6).children().val());
	
    var calAmount = parseFloat(quantity)*(parseFloat(unitprice)*(100-parseFloat(discount))/100);

    $(this).parent().parent().find('td').eq(7).children().val(calAmount.toFixed(4));
    callSubTotal();
    callTotalAmount();
    $(this).parent().parent().find(".taxGroup").change();
}
////////////////////////////////////////////////////////////////////
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function calculateBillTax()
{
	 $('#txtTotal').val((eval($('#txtTotalTax').val()) + eval($('#txtSubTotal').val()) + eval($('#txtBillTaxAmount').val()==''?0.00:$('#txtBillTaxAmount').val())).toFixed(4));
}
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
function callTaxProcess(taxGroupId)
{
    var url = "purchaseInvoice-db-get.php?requestType=getTaxProcess&taxGroupId="+taxGroupId;
    var obj = $.ajax({
        url:url,
        async:false
    });
    return obj.responseText;
}
function callSubTotal()
{
    var subTotal = 0.00;
    $(".amount").each( function(){
        subTotal += eval($(this).val()==''?0.00:$(this).val());
    });
    $('#txtSubTotal').val(subTotal.toFixed(4));	
}
function callTaxTotal()
{
    var taxTotal = 0.00;
    $(".taxWith").each( function(){
        taxTotal += eval($(this).val()==''?0.00:$(this).val());
    });
    $('#txtTotalTax').val(taxTotal.toFixed(4));
    callTotalAmount();
}
function callTotalAmount()
{
    $('#txtTotal').val((eval($('#txtTotalTax').val()) + eval($('#txtSubTotal').val()) + + eval($('#txtBillTaxAmount').val()==''?0.00:$('#txtBillTaxAmount').val())).toFixed(4));
}
function loadCombo_frmPurchaseInvoice()
{
    var url 	= "purchaseInvoice-db-get.php?requestType=loadCombo";
    var httpobj = $.ajax({
        url:url,
        async:false
    })
    $('#frmPurchaseInvoice #cboSearch').html(httpobj.responseText);
}
function alertx()
{
    $('#frmPurchaseInvoice #butSave').validationEngine('hide') ;
}
function alertDelete()
{
    $('#frmPurchaseInvoice #butDelete').validationEngine('hide') ;
	$('#frmPurchaseInvoice #butPrint').validationEngine('hide') ;
}
function loadJs()
{
}
function changeReadOnly(status){
    $('#frmPurchaseInvoice #txtNo').attr('disabled',status);
    $('#frmPurchaseInvoice #cboSupplier').attr('disabled',status);
    //$('#frmPurchaseInvoice #txtDate').attr('disabled',status);
    $('#frmPurchaseInvoice #rdInvoType').attr('disabled',status);//
    $('#frmPurchaseInvoice #txtAddress').attr('disabled',status);
    //$('#frmPurchaseInvoice #txtRemarks').attr('disabled',status);
    $('#frmPurchaseInvoice #cboCurrency').attr('disabled',status);//
    $('#frmPurchaseInvoice #txtPoNo').attr('readonly',status);
    $('#frmPurchaseInvoice #txtPoNo').attr('class',"normalfnt");
    
    $('#frmPurchaseInvoice #txtGrnNo').attr('disabled',status);
    $('#frmPurchaseInvoice #cboItem').attr('disabled',status);//
    $('#frmPurchaseInvoice #txtDesc').attr('disabled',status);//
    $('#frmPurchaseInvoice #cboUOM').attr('disabled',status);//
    $('#frmPurchaseInvoice #txtQty').attr('disabled',status);
    $('#frmPurchaseInvoice #txtUnitPrice').attr('disabled',status);
    $('#frmPurchaseInvoice #txtDiscount').attr('disabled',status);
    $('#frmPurchaseInvoice #txtAmount').attr('disabled',status);
    if(status){
        $('#frmPurchaseInvoice #btnTadd').hide();
    }
    else{
        $('#frmPurchaseInvoice #btnTadd').show();
    }
    
}

//=========Add by dulaskshi 2013.03.15===========
//====================Get Supplier List==============================
function getSupplierList()
{	
	$ledgerAccId = $('#frmPurchaseInvoice #cboLedgerAcc').val();
	
	var url = "purchaseInvoice-db-get.php?requestType=loadSupplier&ledgerAcc="+$ledgerAccId;
	var httpobj = $.ajax({url:url,async:false})
	$('#frmPurchaseInvoice #cboSupplier').html(httpobj.responseText);
}


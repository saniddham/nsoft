<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$companyId = $row['intCompanyId']; 
	}
	/////////// sales invoice load part /////////////////////
	if($requestType=='loadCombo')
	{
		$sql = "SELECT
                        fin_supplier_purchaseinvoice_header.strReferenceNo,
                        fin_supplier_purchaseinvoice_header.strSupInvoice,
                        mst_supplier.strName AS supName
                    FROM
                        fin_supplier_purchaseinvoice_header
                        INNER JOIN mst_supplier ON fin_supplier_purchaseinvoice_header.intSupplierId = mst_supplier.intId
                    WHERE
                        fin_supplier_purchaseinvoice_header.intCompanyId =  '$companyId' AND
                        fin_supplier_purchaseinvoice_header.intDeleteStatus =  '0'
                    ORDER BY
                        fin_supplier_purchaseinvoice_header.dtmDate DESC
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['strReferenceNo']."\">".$row['strSupInvoice']." - ".$row['supName']."</option>";
		}
		echo $html;
	}
	else if($requestType=='loadDetails')
	{
		$id  = $_REQUEST['id'];
		
		//-------------------------------------------------------------
		$sql = "SELECT
				fin_supplier_purchaseinvoice_details.intInvoiceNo,
				fin_supplier_purchaseinvoice_details.intAccPeriodId,
				fin_supplier_purchaseinvoice_details.intLocationId,
				fin_supplier_purchaseinvoice_details.intCompanyId,
				fin_supplier_purchaseinvoice_details.strReferenceNo,
				fin_supplier_purchaseinvoice_details.intItem,
				fin_supplier_purchaseinvoice_details.strItemDesc,
				fin_supplier_purchaseinvoice_details.intUom,
				fin_supplier_purchaseinvoice_details.dblQty,
				fin_supplier_purchaseinvoice_details.dblUnitPrice,
				fin_supplier_purchaseinvoice_details.dblDiscount,
				fin_supplier_purchaseinvoice_details.dblTaxAmount,
				fin_supplier_purchaseinvoice_details.intTaxGroupId,
				fin_supplier_purchaseinvoice_details.intDimensionId,
				fin_supplier_purchaseinvoice_details.strItmType,
				fin_supplier_purchaseinvoice_header.intDeleteStatus
				FROM
				fin_supplier_purchaseinvoice_details
				Inner Join fin_supplier_purchaseinvoice_header ON fin_supplier_purchaseinvoice_details.strReferenceNo = fin_supplier_purchaseinvoice_header.strReferenceNo
				WHERE
				fin_supplier_purchaseinvoice_details.strReferenceNo =  '$id' AND
				fin_supplier_purchaseinvoice_header.intDeleteStatus =  '0'
				";
		$result = $db->RunQuery($sql);
		$arrDetail;
		while($row=mysqli_fetch_array($result))
		{
			$val['itemId'] 		= $row['intItem']."^".$row['strItmType'];
			$val['itemDesc'] 	= $row['strItemDesc'];
			$val['uom'] 		= $row['intUom'];
			$val['qty'] 		= $row['dblQty'];
			$val['unitPrice'] 	= $row['dblUnitPrice'];
			$val['discount'] 	= $row['dblDiscount'];
			$val['taxAmount'] 	= $row['dblTaxAmount'];
			$val['taxGroup'] 	= $row['intTaxGroupId'];
			$val['dimension'] 	= $row['intDimensionId'];
			$arrDetail[] = $val;
		}
		$response['detailVal'] = $arrDetail;
		//-----------------------------------------------------------
		
		$sql   = "SELECT
				fin_supplier_purchaseinvoice_header.intInvoiceNo,
				fin_supplier_purchaseinvoice_header.intAccPeriodId,
				fin_supplier_purchaseinvoice_header.intLocationId,
				fin_supplier_purchaseinvoice_header.intCompanyId,
				fin_supplier_purchaseinvoice_header.strReferenceNo,
				fin_supplier_purchaseinvoice_header.intSupplierId,
				fin_supplier_purchaseinvoice_header.dtmDate,
				fin_supplier_purchaseinvoice_header.strInvoiceType,
				fin_supplier_purchaseinvoice_header.strAddress,
				fin_supplier_purchaseinvoice_header.intCurrencyId,
				fin_supplier_purchaseinvoice_header.dblRate,
				fin_supplier_purchaseinvoice_header.strRemark,
				fin_supplier_purchaseinvoice_header.strPoNo,
				fin_supplier_purchaseinvoice_header.intPaymentsTermsId,
				fin_supplier_purchaseinvoice_header.strGrnNo,
				fin_supplier_purchaseinvoice_header.strMessage,
                fin_supplier_purchaseinvoice_header.intAutomated,
				fin_supplier_purchaseinvoice_header.strSupInvoice,
				fin_supplier_purchaseinvoice_header.intTaxAuthorityId,
				fin_supplier_purchaseinvoice_header.dblBillTaxAmount
				FROM
				fin_supplier_purchaseinvoice_header
				WHERE
				fin_supplier_purchaseinvoice_header.strReferenceNo =  '$id' AND
				fin_supplier_purchaseinvoice_header.intDeleteStatus =  '0'
				";
		$result = $db->RunQuery($sql);
                $automated=0;
		while($row=mysqli_fetch_array($result))
		{
			$response['supplier'] 	= $row['intSupplierId'];
			$response['date'] 	= $row['dtmDate'];
			$response['invoType'] 	= $row['strInvoiceType'];
			$response['address'] 	= $row['strAddress'];
			$response['currency']	= $row['intCurrencyId'];
			$response['rate'] 	= $row['dblRate'];
			$response['remark'] 	= $row['strRemark'];
			$response['poNo'] 	= $row['strPoNo'];
			$response['payTerms'] 	= $row['intPaymentsTermsId'];
			$response['grnNo'] 	= $row['strGrnNo'];
			$response['message'] 	= $row['strMessage'];
            $automated              = $row['intAutomated'];
			$response['automated'] 	= $automated;
			$response['supInvoice'] = $row['strSupInvoice'];
			$response['taxAuthority'] = $row['intTaxAuthorityId'];
			$response['billTaxAmount'] = $row['dblBillTaxAmount'];
		}
		//--------------------------------------------------
                //---------- get the ware house items for automated purchase invoices--------------
                if($automated==1)//Auto mated  purchase invoices
				{
                    $sql="SELECT mst_item.intId,
                            mst_item.strCode,
                            mst_item.strName
                            FROM
                            mst_item"; 
                    $result = $db->RunQuery($sql);
                    $itemList="";
                    while($row=mysqli_fetch_array($result)){
                        $itemList.="<option value=\"".$row['intId']."^"."Itm"."\">".$row['strCode']."</option>";
                    }
                    $response['autoItemList']=$itemList;
                }
                else// Others
				{
                    $sql = "SELECT
                                mst_financesupplieritem.intId,
                                mst_financesupplieritem.strName,
                                mst_financesupplieritemactivate.intCompanyId
                            FROM
                                mst_financesupplieritem
                                Inner Join mst_financesupplieritemactivate ON mst_financesupplieritem.intId = mst_financesupplieritemactivate.intSupplierItemId
                            WHERE
                                mst_financesupplieritem.intStatus =  1 AND
                                mst_financesupplieritemactivate.intCompanyId = '$companyId'
                                order by strName";
                    $result = $db->RunQuery($sql);
                    $itemList="";
                    while($row=mysqli_fetch_array($result))
					{
                        $itemList.= "<option value=\"".$row['intId']."^"."Itm"."\">".$row['strName']."</option>";
                    }
					
					$sql = "SELECT
								mst_financechartofaccounts.intId,
								mst_financechartofaccounts.strCode,
								mst_financechartofaccounts.strName
								FROM
								mst_financechartofaccounts
								WHERE
								mst_financechartofaccounts.strType = 'Posting' AND
								mst_financechartofaccounts.intStatus =  '1' AND
								(mst_financechartofaccounts.intFinancialTypeId =  '6' OR mst_financechartofaccounts.intFinancialTypeId =  '5')
								";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							$itemList.= "<option value=\"".$row['intId']."^"."Acc"."\">".$row['strCode']." - ".$row['strName']."</option>";
						}
					
                    $response['autoItemList']=$itemList;
                }
                
                //-----------------------------------------------------------------
		$sql = "SELECT
				fin_supplier_purchaseinvoice_header.strReferenceNo,
				fin_supplier_purchaseinvoice_header.intsupplierId,
				INV.dblQty,
				INV.dblUnitPrice,
				INV.dblDiscount,
				INV.dblTaxAmount,
				fin_supplier_purchaseinvoice_header.dblBillTaxAmount,
				sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) + IFNULL(dblBillTaxAmount,0) AS amount,
				(
				sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) + IFNULL(dblBillTaxAmount,0)
				-
				IFNULL ((SELECT
				Sum(fin_supplier_payments_main_details.dblPayAmount )AS paidAmount
				FROM fin_supplier_payments_main_details
				Inner Join fin_supplier_payments_header ON fin_supplier_payments_main_details.strReferenceNo = fin_supplier_payments_header.strReferenceNo
				WHERE
				fin_supplier_payments_main_details.strDocNo =  INV.strReferenceNo AND
				fin_supplier_payments_header.intDeleteStatus =  '0' AND fin_supplier_payments_main_details.strDocType = 'P.Invoice'
				GROUP BY
				fin_supplier_payments_main_details.strDocNo),0)
				
				) AS balAmount
				FROM
				fin_supplier_purchaseinvoice_details AS INV
				Inner Join fin_supplier_purchaseinvoice_header ON fin_supplier_purchaseinvoice_header.strReferenceNo = INV.strReferenceNo AND INV.intInvoiceNo = fin_supplier_purchaseinvoice_header.intInvoiceNo AND INV.intAccPeriodId = fin_supplier_purchaseinvoice_header.intAccPeriodId AND INV.intCompanyId = fin_supplier_purchaseinvoice_header.intCompanyId
				Inner Join mst_supplier ON mst_supplier.intId = fin_supplier_purchaseinvoice_header.intsupplierId
				Inner Join mst_financecurrency ON mst_financecurrency.intId = fin_supplier_purchaseinvoice_header.intCurrencyId
				WHERE
				fin_supplier_purchaseinvoice_header.intDeleteStatus =  '0' AND fin_supplier_purchaseinvoice_header.strReferenceNo = '$id'
				GROUP BY
				fin_supplier_purchaseinvoice_header.strReferenceNo
				"; // AND INV.intLocationId = fin_supplier_purchaseinvoice_header.intLocationId
			$result = $db->RunQuery($sql);
			while($row=mysqli_fetch_array($result))
			{
				$response['totAmount'] 	= $row['amount'];
				$response['balAmount'] 	= $row['balAmount'];
			}
		//--------------------------------------------------
		echo json_encode($response);
	}
	else if($requestType=='getExchangeRate')
	{
		$currencyId  	= $_REQUEST['currencyId'];
		$exchangeDate	= $_REQUEST['exchangeDate'];
		
		$sql = "SELECT
					mst_financeexchangerate.dblSellingRate,
					mst_financeexchangerate.dblBuying
				FROM mst_financeexchangerate
				WHERE
					mst_financeexchangerate.intCurrencyId 	=  '$currencyId' AND
					mst_financeexchangerate.dtmDate 		=  '$exchangeDate'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		
		if(mysqli_num_rows($result)>0)
		{
			$arrValue['sellingRate'] 	= $row['dblSellingRate'];
			$arrValue['buyingRate'] 	= $row['dblBuying'];
		}
		else
		{
			$arrValue['sellingRate'] 	= "";
			$arrValue['buyingRate'] 	= "";
		}
		echo json_encode($arrValue);
	}
	else if($requestType=='getSupplierAddress') // with invoice type
	{
		$supplierId  = $_REQUEST['supplierId'];
		
		$sql = "SELECT 	mst_supplier.strAddress, mst_supplier.strInvoiceType, mst_supplier.intCurrencyId FROM mst_supplier WHERE mst_supplier.intId = '$supplierId'";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$response['address'] = $row['strAddress'];
		$response['invoType']= $row['strInvoiceType'];
		$response['currency']= $row['intCurrencyId'];
		echo json_encode($response);
	}
	else if($requestType=='getItemDescription')
	{
		$itemId  = $_REQUEST['itemId'];
		
		$sql = "SELECT
				mst_financesupplieritem.intId,
				mst_financesupplieritem.strRemark
				FROM
				mst_financesupplieritem
				WHERE
				mst_financesupplieritem.intId =  '$itemId'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		echo $row['strRemark'];
	}
	else if($requestType=='getTaxValue')
	{
		$operation  = $_REQUEST['opType'];
		$amount  = $_REQUEST['valAmount'];
		$taxCodes = json_decode($_REQUEST['arrTaxCode'], true);
		
		if(count($taxCodes) != 0)
		{
			foreach($taxCodes as $taxCode)
			{
				$codeValues[] = callTaxValue($taxCode['taxId']);
			}
		}
		if(count($codeValues) > 1)
		{
			if($operation == 'Inclusive')
			{
				$firstVal = ($amount*$codeValues[0])/100;
				$withTaxVal = $firstVal + ((($amount+$firstVal)*$codeValues[1])/100);
				$val1 = ($amount*$codeValues[0])/100;
				$val2 = ((($amount+$firstVal)*$codeValues[1])/100);
			}
			else if($operation == 'Exclusive')
			{
				$withTaxVal = ($amount*($codeValues[0] + $codeValues[1]))/100;
				$val1 = ($amount*$codeValues[0])/100;
				$val2 = ($amount*$codeValues[1])/100;
			}
		}
		else if(count($codeValues) == 1 && $operation == 'Isolated')
		{
			$withTaxVal = ($amount*$codeValues[0])/100;
			$val1 = ($amount*$codeValues[0])/100;
		}
		echo $withTaxVal."/".$val1."/".$val2;
	}
	else if($requestType=='getTaxProcess')
	{
		$taxGrpId  = $_REQUEST['taxGroupId'];
		
		$sql = "SELECT
				mst_financetaxgroup.intId,
				mst_financetaxgroup.strProcess
				FROM
				mst_financetaxgroup
				WHERE
				mst_financetaxgroup.intId =  '$taxGrpId'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		echo $row['strProcess'];
	}
	//===========Add by dulakshi 2013.03.15=========
	else if($requestType=='loadSupplier')
	{
		$ledAcc  = $_REQUEST['ledgerAcc'];		
							
		$condition == "";
			if($ledAcc != "")
			{	
				$condition .= "AND mst_financesupplieractivate.intChartOfAccountId =  '$ledAcc'";			
			}			
		
		$sql = "SELECT
					mst_supplier.intId,
					mst_supplier.strName,
					mst_financesupplieractivate.intCompanyId
				FROM
					mst_supplier
					Inner Join mst_financesupplieractivate ON mst_supplier.intId = mst_financesupplieractivate.intSupplierId
				WHERE
					mst_supplier.intStatus = '1' AND					
					mst_financesupplieractivate.intCompanyId = '$companyId'" .$condition . " order by mst_supplier.strName";
					
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
	}
	//==============================================
	
	function callTaxValue($taxId)
	{
		global $db;
		$sql = "SELECT
				mst_financetaxisolated.intId,
				mst_financetaxisolated.strCode,
				mst_financetaxisolated.dblRate
				FROM
				mst_financetaxisolated
				WHERE
				mst_financetaxisolated.intId = '$taxId'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$taxVal = $row['dblRate'];	
		return $taxVal;
	}
?>
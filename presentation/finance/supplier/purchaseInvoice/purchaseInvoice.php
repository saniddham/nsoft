<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];

$thisFilePath =  $_SERVER['PHP_SELF'];
include  "{$backwardseperator}dataAccess/permisionCheck.inc";
$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$companyId = $row['intCompanyId']; 
}
$invoiceRefNo = $_REQUEST['id'];
// ======================Check Exchange Rate Updates========================
if($invoiceRefNo == "")
{
	$status = "Adding";
}
else
{
	$status = "Changing";
}
$currentDate = date("Y-m-d");

$sql = "SELECT COUNT(*) AS 'no'
		FROM
		mst_financeexchangerate
		WHERE
		mst_financeexchangerate.dtmDate =  '$currentDate'
		";
$result = $db->RunQuery($sql);
$row= mysqli_fetch_array($result);
if($row['no']==0)
	{
		$str =  "Please Update Exchange Rates Before ".$status." Purchase Invoice .";
		$str .= $row['NameList'];
		$maskClass="maskShow";
	}
	else
	{
		$maskClass="maskHide";
	}
// =========================================================================
?>
<script type="application/javascript" >
var invoRefNo = '<?php echo $invoiceRefNo ?>';
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Purchase Invoice</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script src="purchaseInvoice-js.js" type="text/javascript"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<script src="../../commanFunctions/numberExisting-js.js" type="text/javascript"></script>

<link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css">

<style type="text/css">
.apDiv1 {
	position:absolute;
	left:179px;
	top:116px;
	width:auto;
	height:auto;
	z-index:0;
}
</style>

<link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

</head>

<body onLoad="functionList();">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<div id="divMask" class="<?php echo $maskClass?> mask"> <?php echo $str; ?></div>

<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<div id="partPay" class= "apDiv1 maskHide" ><img src="../../../../images/partPayment.png"  /></div>
<div id="fullPay" class= "apDiv1 maskHide" ><img src="../../../../images/paymentCompleted.png"  /></div>
<div id="overPay" class= "apDiv1 maskHide" ><img src="../../../../images/overPayment.png"  /></div>
<div id="pending" class= "apDiv1 maskHide" ><img src="../../../../images/pending2.png"  /></div>

<form id="frmPurchaseInvoice" name="frmPurchaseInvoice" method="post" action="purchaseInvoice-db-set.php" autocomplete="off">
<div align="center">
  <div class="trans_layoutL">
    <div class="trans_text">Purchase Invoice/ Bill</div>
 <table width="100%">
      <tr>
      <td class="normalfnt" width="33%"><img src="../../../../images/fb.png" width="18" height="19" /></td>
      <td align="right" width="67%"><img src="../../../../images/ff.png" width="18" height="19" /></td>
    </tr>
      <tr>
        <td align="right"><span class="normalfnt">Invoice: </span></td>
        <td align="left"><span class="normalfntMid">
          <select name="cboSearch" id="cboSearch"  style="width:350px" >
            <option value=""></option>
            <?php   
            $sql = "SELECT
                        fin_supplier_purchaseinvoice_header.strReferenceNo,
                        fin_supplier_purchaseinvoice_header.strSupInvoice,
                        mst_supplier.strName AS supName
                    FROM
                        fin_supplier_purchaseinvoice_header
                        INNER JOIN mst_supplier ON fin_supplier_purchaseinvoice_header.intSupplierId = mst_supplier.intId
                    WHERE
                        fin_supplier_purchaseinvoice_header.intCompanyId =  '$companyId' AND
                        fin_supplier_purchaseinvoice_header.intDeleteStatus =  '0'
                    ORDER BY
                        fin_supplier_purchaseinvoice_header.dtmDate DESC";
            $result = $db->RunQuery($sql);
            while($row=mysqli_fetch_array($result))
            {
                    echo "<option value=\"".$row['strReferenceNo']."\">".$row['strSupInvoice']." - ".$row['supName']."</option>";
            }
          ?>
          </select>
        </span></td>
      </tr>
      <tr>
          <td colspan="2" align="right"><span class="normalfntRight"><input type="checkbox" name="chkAuto" id="chkAuto" value="" disabled="true" style="background-color:#F4FFFF;"/>Automated</span></td>
      </tr>
      <tr>
        <td colspan="2">
          <table width="100%" class="tableBorder_allRound">
            <tr>
              <td class="normalfnt">&nbsp;</td>
              <td class="normalfnt"><span class="name">Invoice Number <span class="compulsoryRed">*</span></span></td>
              <td><span class="normalfnt">
                      <input name="txtNo"  type="text" class="validate[required]" id="txtNo" style="width:180px; background-color:#F4FFFF;text-align:center; border:outset; border-color:#F00" />
                </span></td>
              <td>&nbsp;</td>
              <td bgcolor="#FFFFFF" class="">&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
           <!-- ========Add by dulakshi 2013.03.15============== --> 
            <tr>
              <td class="normalfnt">&nbsp;</td>
              <td class="normalfnt"><span class="name">Ledger Accounts</span></td>
              <td width="318"><span class="normalfntMid">
                <select name="cboLedgerAcc" id="cboLedgerAcc"  style="width:211px" onChange="getSupplierList();" >
                	<option value=""></option>
                        <?php  $sql2 = "SELECT DISTINCT
												mst_financechartofaccounts.intId,
												mst_financechartofaccounts.strCode,
												mst_financechartofaccounts.strName
										FROM
												mst_financechartofaccounts
										                Inner Join mst_financesupplieractivate ON mst_financechartofaccounts.intId = mst_financesupplieractivate.intChartOfAccountId
												WHERE
													mst_financesupplieractivate.intCompanyId =  '$companyId'
										GROUP BY  mst_financechartofaccounts.intId 
										order by strCode ";
						$result2 = $db->RunQuery($sql2);
						while($row2=mysqli_fetch_array($result2))
						{
						   echo "<option value=\"".$row2['intId']."\">".$row2['strCode']."-".$row2['strName']."</option>";
						}
        				?> 
                 </select>
                </span>
              </td>  
               
              <td>&nbsp;</td>
              <td bgcolor="#FFFFFF" class="">&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <!-- ================================ --> 
            <tr>
              <td width="19" class="normalfnt">&nbsp;</td>
              <td width="126" class="normalfnt">Supplier <span class="compulsoryRed">*</span></td>
     <!-- ========Edited by dulakshi 2013.03.15============== --> 
              <td width="318"><span class="normalfntMid">
                  <select name="cboSupplier" id="cboSupplier"  style="width:211px" class="validate[required]" >          
                  </select>
                </span></td>
    <!-- ================================== --> 
              <td width="84"><span class="normalfnt">Date <span class="compulsoryRed">*</span></span></td>
              <td width="319" bgcolor="#FFFFFF" class=""><input name="txtDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="validate[required]" id="txtDate" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" onBlur="backDateExisting(this,'Purchase Invoice');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
              <td width="14">&nbsp;</td>
              </tr>
            <tr>
              <td>&nbsp;</td>
              <td><span class="normalfnt">Invoice Type</span></td>
              <td><span class="normalfnt">
                <input type="radio" class="invTpe" name="rdInvoType" id="Commercial" value="Commercial" />
                Commercial
                <input type="radio"  class="invTpe" name="rdInvoType" id="Tax" value="Tax" />
                Tax</span> <span class="normalfnt">
                  <input type="radio"  class="invTpe" name="rdInvoType" id="SVAT" value="SVAT" />
                  SVAT 
                  <input type="radio"  class="invTpe" name="rdInvoType" id="NFE" value="NFE" />
NFE-Suspended</span></td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td><span class="normalfnt">Invoice Address</span></td>
              <td><textarea name="txtAddress" id="txtAddress" cols="30" rows="2"></textarea></td>
              <td><span class="normalfnt">Memo</span></td>
              <td><textarea name="txtRemarks" id="txtRemarks" cols="30" rows="2"></textarea></td>
              <td>&nbsp;</td>
              </tr>
            <tr>
              <td>&nbsp;</td>
              <td><span class="normalfnt">Currency <span class="compulsoryRed">*</span></span></td>
              <td><select name="cboCurrency" id="cboCurrency" style="width:140px" class="validate[required]" >
                <option value=""></option>
                <?php  $sql = "SELECT
								mst_financecurrency.intId,
								mst_financecurrency.strCode,
								mst_financecurrencyactivate.intCompanyId
								FROM
								mst_financecurrency
								Inner Join mst_financecurrencyactivate ON mst_financecurrency.intId = mst_financecurrencyactivate.intCurrencyId
								WHERE
								mst_financecurrency.intStatus =  1 AND
								mst_financecurrencyactivate.intCompanyId = '$companyId'
								order by strCode
							";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strCode']."</option>";
						}
        				?>
                </select></td>
              <td><span class="normalfnt">Rate <span class="compulsoryRed">*</span></span></td>
              <td align="left" class="normalfnt"><span class="normalfnt"><span class="normalfntMid">
                <input class="rdoRate" type="radio" name="radio" id="rdoSelling" value="" />
                Selling
  <input class="rdoRate" type="radio" name="radio" id="rdoBuying" value="" />
                Buying 
                <input class="rdoRate" type="radio" name="radio" id="rdoAverage" value="" />
Average
<input type="text" name="txtRate" id="txtRate" style="width:75px; background-color:#9F9; border:thin; text-align:center" readonly class="validate[custom[number],required] normalfntBlue"/>
  <input type="checkbox" name="chkEdit" id="chkEdit" />
                </span></span></td>
              <td>&nbsp;</td>
              </tr>
            </table>
          </td>
      </tr>
    <tr>
      <td colspan="2">
      <table width="100%" class="tableBorder_allRound">
      <tr>
      <td align="center">
		<table width="49%" id="tblMainGrid1" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
        <tr class="">
          <td width="130"  height="26" bgcolor="#FAD163" class="normalfntMid"><strong>P.O No.</strong></td>
          <td width="182"  bgcolor="#FAD163" class="normalfntMid"><strong>Terms <span class="compulsoryRed">*</span></strong></td>
          <td width="153"  bgcolor="#FAD163" class="normalfntMid"><strong>GRN No.</strong></td>
          </tr>
        <tr class="normalfnt">
          <td bgcolor="#FFFFFF" class="normalfntMid">
          <input type="text" name="txtPoNo" id="txtPoNo" style="width:130px" /></td>
          <td bgcolor="#FFFFFF"><select name="cboPaymentsTerms" id="cboPaymentsTerms" style="width:182px" class="validate[required]" >
            <option value=""></option>
            <?php  $sql = "SELECT
                            intId,
                            strName
                            FROM mst_financepaymentsterms
                            WHERE
                                    intStatus = 0
                            order by strName
                            ";
                            $result = $db->RunQuery($sql);
                            while($row=mysqli_fetch_array($result))
                            {
                                    echo "<option value=\"".$row['intId']."\">".$row['strName']." days"."</option>";
                            }
        				?>
          </select></td>
          <td bgcolor="#FFFFFF"><span class="normalfntMid">
            <input type="text" name="txtGrnNo" id="txtGrnNo" style="width:130px" />
          </span></td>
          </tr>
      </table>
      </td>
      </tr>
      </table>
      </td>
      </tr>
            <tr>
                <td colspan="2" align="right"><img id="btnTadd" src="../../../../images/Tadd.jpg" width="92" height="24" onClick="insertRow();" /></td>
      </tr>
    <tr>
      <td colspan="2"><table width="100%">
        <tr>
          <td><div style="overflow:scroll;width:900px;height:200px;" id="divGrid">
            <table width="100%" id="tblMainGrid2" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
              <tr class="">
                <td width="2%" bgcolor="#FAD163" class="normalfntMid">Del</td>
                <td width="12%"   height="27" bgcolor="#FAD163" class="normalfntMid"><strong><span class="compulsoryRed">*</span> Item / Expenses</strong><strong><img id="butItem" src="../../../../images/add_new.png" width="15" height="15" style="display:none"/></strong></td>
                <td width="16%"    bgcolor="#FAD163" class="normalfntMid"><strong>Item Desc.</strong></td>
                <td width="9%"    bgcolor="#FAD163" class="normalfntMid"><strong><span class="compulsoryRed">*</span> UOM</strong></td>
                <td width="11%"    bgcolor="#FAD163" class="normalfntMid"><strong>Qty</strong></td>
                <td  bgcolor="#FAD163" class="normalfntMid"  ><strong>Unit Price</strong></td>
                <td width="8%"   bgcolor="#FAD163" class="normalfntMid"  ><strong>Discount %</strong></td>
                <td width="9%"   bgcolor="#FAD163" class="normalfntMid"  ><strong>Amount</strong></td>
                <td width="11%"   bgcolor="#FAD163" class="normalfntMid"  ><strong>Tax</strong> <strong> <img id="butTaxGroup" src="../../../../images/add_new.png" width="15" height="15" style="display:none"/></strong></td>
                <td width="11%"   bgcolor="#FAD163" class="normalfntMid"  ><strong> <span class="compulsoryRed">*</span> Cost Center<img id="butDimension" src="../../../../images/add_new.png" width="15" height="15" style="display:none" /></strong></td>
                <td width="11%" style="display:none"  bgcolor="#FAD163" class="normalfntMid"><strong>Tax With</strong></td>
              </tr>
              <tr class="normalfnt">
                <td bgcolor="#FFFFFF" class="normalfntMid"><img src="../../../../images/del.png" width="15" height="15" class="delImg"/></td>
                <td bgcolor="#FFFFFF" class="normalfntMid">
                <select name="cboItem" id="cboItem" style="width:130px" class="validate[required] item">
                  <option value=""></option>
                  <?php  
						
						$sql = "SELECT
								mst_financechartofaccounts.intId,
								mst_financechartofaccounts.strCode,
								mst_financechartofaccounts.strName
								FROM
								mst_financechartofaccounts
								WHERE
								mst_financechartofaccounts.strType = 'Posting' AND
								mst_financechartofaccounts.intStatus =  '1' AND
								(mst_financechartofaccounts.intFinancialTypeId =  '6' OR mst_financechartofaccounts.intFinancialTypeId =  '5' OR mst_financechartofaccounts.intFinancialTypeId =  '8')
								order by strCode
								";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."^"."Acc"."\">".$row['strCode']." - ".$row['strName']."</option>";
						}
						
						$sql = "SELECT
								mst_financesupplieritem.intId,
								mst_financesupplieritem.strName,
								mst_financesupplieritemactivate.intCompanyId
								FROM
								mst_financesupplieritem
								Inner Join mst_financesupplieritemactivate ON mst_financesupplieritem.intId = mst_financesupplieritemactivate.intSupplierItemId
								WHERE
								mst_financesupplieritem.intStatus =  1 AND
								mst_financesupplieritemactivate.intCompanyId = '$companyId'
								order by strName
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."^"."Itm"."\">".$row['strName']."</option>";
						}
						
        		?>
                </select></td>
                <td bgcolor="#FFFFFF">
                <input type="text" name="txtDesc" id="txtDesc" style="width:110px" class="description" /></td>
                <td bgcolor="#FFFFFF">
                <select name="cboUOM" class="validate[required] uom" id="cboUOM" style="width:75px" >
                  <option value=""></option>
                  <?php  $sql = "SELECT
								intId,
								strName
								FROM mst_units
									WHERE intStatus=1
										order by strName";
										
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
								}
                   		?>
                </select></td>
                <td bgcolor="#FFFFFF">
                <input type="text" name="txtQty" id="txtQty" style="width:75px;text-align:center" class="validate[custom[number] qty"/></td>
                <td width="11%" align="center" bgcolor="#FFFFFF" class="">
                <input type="text" name="txtUnitPrice" id="txtUnitPrice" style="width:100px;text-align:center" class="validate[custom[number]] unitPrice" /></td>
                <td  bgcolor="#FFFFFF">
                <input name="txtDiscount" type="text" class="validate[custom[number]] discount" id="txtDiscount" style="width:75px;text-align:center" /></td>
                <td  bgcolor="#FFFFFF">
                <input name="txtAmount" type="text" class="validate[custom[number]] amount normalfntBlue" id="txtAmount" style="width:80px; background-color:#F4FFFF; text-align:right; border:thin" readonly/></td>
                <td  bgcolor="#FFFFFF">
                    <select name="cboTaxGroup" class="taxGroup" id="cboTaxGroup" style="width:100px;">
                  <option value=""></option>
                  <?php  $sql = "SELECT
								mst_financetaxgroup.intId,
								mst_financetaxgroup.strCode,
								mst_financetaxgroup.intStatus
								FROM
								mst_financetaxgroup
								WHERE
									intStatus = 1
								order by strCode
								";
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intId']."\">".$row['strCode']."</option>";
								}
                   ?>
                </select></td>
                <td  bgcolor="#FFFFFF">
                <select name="cboDimension" class="validate[required] dimension" id="cboDimension"  style="width:100px;">
                  <option value=""></option>
                  <?php  $sql = "SELECT
									intId,
									strName
								FROM mst_financedimension
								WHERE
									intStatus = 1
								order by strName
								";
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
								}
                   ?>
                </select></td>
                <td  bgcolor="#FFFFFF" class="taxVal" style="display:none">
                <input type="text" name="txtTaxWith" id="txtTaxWith" style="width:80px; background-color:#F4FFFF; text-align:right" class="validate[custom[number]] taxWith" disabled="disabled"/></td>
              </tr>
            </table>
          </div></td>
        </tr>
      </table></td>
      </tr>
      <tr>
    	<td colspan="2">
    	<table width="100%">
        <tr>
        <td width="9%"><span class="normalfnt">Message</span></td>
        <td width="66%"><textarea name="txtMessage" cols="50" id="txtMessage"></textarea></td>
        <td width="25%">
        <table width="100%">
        <tr>
        <td width="86" align="right"><span class="normalfnt">Sub-Total</span></td>
        <td width="152" align="right"><span class="normalfntMid">
          <input name="txtSubTotal" type="text" disabled="disabled" id="txtSubTotal" style="width:100%;background-color:#F4FFFF; text-align:right" value="0.0000" />
        </span></td>
        </tr>
        <tr>
          <td align="right"><span class="normalfnt">Tax</span></td>
          <td align="right"><span class="normalfntMid">
            <input name="txtTotalTax" type="text" disabled="disabled" id="txtTotalTax" style="width:100%;background-color:#F4FFFF; text-align:right" value="0.0000" />
          </span></td>
        </tr>
        <tr>
          <td align="right"><span class="normalfnt">Total</span></td>
          <td align="right"><span class="normalfntMid">
            <input name="txtTotal" class="normalfntBlue" type="text" id="txtTotal" style="width:100%;background-color:#F4FFFF; text-align:right; border:solid" value="0.0000" readonly />
          </span></td>
        </tr>
        </table>
        </td>
        </tr>
        <tr>
          <td colspan="3">
          <table width="100%" style="border-style:dotted;border-color:#00F">
          <tr>
          <td width="1%" height="23" class="normalfntMid" ></td>
          <td width="3%" class="normalfntMid" ><input type="checkbox" name="chkBillTax" id="chkBillTax" /></td>
          <td width="17%" class="normalfnt"><strong><span class="compulsoryRed">Apply bill wise taxes</span></strong></td>
          <td width="11%"><span class="normalfnt">Tax Authority</span></td>
          <td width="40%" class="normalfnt"><select name="cboOtherRec" id="cboOtherRec"  style="width:100%" disabled="disabled">
              <option value=""></option>
              <?php  $sql = "SELECT
						mst_finance_service_customer.intId,
						mst_finance_service_customer.strName,
						mst_finance_service_customer_activate.intCompanyId
						FROM
						mst_finance_service_customer
						Inner Join mst_finance_service_customer_activate ON mst_finance_service_customer.intId = mst_finance_service_customer_activate.intCustomerId
						WHERE
						mst_finance_service_customer.intStatus =  1 AND
						mst_finance_service_customer_activate.intCompanyId =  '$companyId'
						order by strName";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
						}
          ?>
            </select></td>
          <td width="10%" class="normalfnt">Tax Amount</td>
          <td width="16%"><input type="text" name="txtBillTaxAmount" id="txtBillTaxAmount" style="width:100%;text-align:center" class="validate[custom[number]]" disabled="disabled" /></td>
          <td width="2%">&nbsp;</td>
          </tr>
          </table></td>
          </tr>
        </table>
    	</td>
      </tr>
    <tr>
      <td colspan="2">
        <table width="100%">
          <tr>
            <td width="100%" height="34" class="tableBorder_allRound"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
<td width="100%" align="center" bgcolor=""><img style="display:none" border="0" src="../../../../images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:none" border="0" src="../../../../images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/><img style="display:none" border="0" src="../../../../images/Tprint.jpg" alt="Print" name="butPrint" width="92" height="24" class="mouseover" id="butPrint" tabindex="25"/><img style="display:none" border="0" src="../../../../images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="25"/><a href="../../../../main.php"><img  src="../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                </tr>
              </table></td>
            </tr>
          </table>
        </td>
    </tr>
    </table>
</div>
</div>
</form>   
</body>
</html>
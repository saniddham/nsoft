<?php
	$backwardseperator = "../../../../";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Company Accounts</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<div align="center">
  <div class="trans_layoutD">
	<div class="trans_text">Company Accounts</div>
    <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="">
   	      <tr>
   	        <td bgcolor="#FFFFFF" class="normalfntMid">&nbsp;</td>
       	      <td bgcolor="#FFFFFF" class="normalfntMid">&nbsp;</td>
       	      <td bgcolor="#FFFFFF" class="normalfntb">Ledger Account</td>
          <tr>
            <td colspan="3" bgcolor="#FFFFFF" class="normalfntb">&nbsp;</td>
          <tr>
            <td bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
            <td bgcolor="#FFFFFF" class="normalfnt">VAT Input – Suspenses</td>
            <td bgcolor="#FFFFFF"><select style="width:210px" name="select3" id="select3">
              <option>&nbsp;</option>
              <option>Cost of Good Sold</option>
              <option>Stationery</option>
              <option>Advertising</option>
              <option>Office Equipment</option>
              <option>Raw Materials</option>
              <option>Other Materials</option>
            </select></td>
          <tr>
            <td bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
            <td bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
            <td bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
            <td bgcolor="#FFFFFF" class="normalfnt"><span class="">VAT Input- Tax</span></td>
            <td bgcolor="#FFFFFF"><select style="width:210px" name="select4" id="select4">
              <option>&nbsp;</option>
              <option>Cost of Good Sold</option>
              <option>Stationery</option>
              <option>Advertising</option>
              <option>Office Equipment</option>
              <option>Raw Materials</option>
              <option>Other Materials</option>
            </select></td>
          </tr>
      <tr>
        <td width="10%" bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
   	    <td width="42%" bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
   	    <td width="48%" bgcolor="#FFFFFF">&nbsp;</td>
        <tr>
          <td bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
          <td bgcolor="#FFFFFF" class="normalfnt"><span class="">VAT Out Put – Suspenses</span></td>
          <td bgcolor="#FFFFFF"><select style="width:210px" name="select5" id="select5">
            <option>&nbsp;</option>
            <option>Cost of Good Sold</option>
            <option>Stationery</option>
            <option>Advertising</option>
            <option>Office Equipment</option>
            <option>Raw Materials</option>
            <option>Other Materials</option>
          </select></td>
          <tr>
            <td bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
            <td bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
            <td bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
            <td bgcolor="#FFFFFF" class="normalfnt"><span class="">VAT Out put</span></td>
            <td bgcolor="#FFFFFF"><select style="width:210px" name="select6" id="select6">
              <option>&nbsp;</option>
              <option>Cost of Good Sold</option>
              <option>Stationery</option>
              <option>Advertising</option>
              <option>Office Equipment</option>
              <option>Raw Materials</option>
              <option>Other Materials</option>
            </select></td>
          <tr>
            <td bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
            <td bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
            <td bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
            <td bgcolor="#FFFFFF" class="normalfnt"><span class="">NBT Payable</span></td>
            <td bgcolor="#FFFFFF"><select style="width:210px" name="select7" id="select7">
              <option>&nbsp;</option>
              <option>Cost of Good Sold</option>
              <option>Stationery</option>
              <option>Advertising</option>
              <option>Office Equipment</option>
              <option>Raw Materials</option>
              <option>Other Materials</option>
            </select></td>
          <tr>
            <td bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
            <td bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
            <td bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
            <td bgcolor="#FFFFFF" class="normalfnt"><span class="">Revenue Accounts</span></td>
            <td bgcolor="#FFFFFF"><select style="width:210px" name="select" id="select">
              <option>&nbsp;</option>
              <option>Cost of Good Sold</option>
              <option>Stationery</option>
              <option>Advertising</option>
              <option>Office Equipment</option>
              <option>Raw Materials</option>
              <option>Other Materials</option>
            </select></td>
          <tr>
            <td bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
            <td bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
            <td bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
            <td bgcolor="#FFFFFF" class="normalfnt">Sub Contract Account</td>
            <td bgcolor="#FFFFFF"><select style="width:210px" name="select2" id="select2">
              <option>&nbsp;</option>
              <option>Cost of Good Sold</option>
              <option>Stationery</option>
              <option>Advertising</option>
              <option>Office Equipment</option>
              <option>Raw Materials</option>
              <option>Other Materials</option>
            </select></td>
              <tr>
                <td colspan="3" bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
              </table>
    <table width="100%">
      <tr>
            <td width="100%" height="34"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td width="100%" align="center" bgcolor=""><a href="../../../../main.php"><img border="0" src="../../../../images/Tnew.jpg" alt="New" name="New" width="92" height="24" onclick="ClearCompanyForm();" class="mouseover" id="butNew" tabindex="28"/><img border="0" src="../../../../images/Tsave.jpg" alt="Save" name="Save"width="92" height="24" onclick="butCommand1(this.name);" class="mouseover" id="butSave" tabindex="24"/><img border="0" src="../../../../images/Tdelete.jpg" alt="Delete" name="Delete" width="92" height="24" onclick="ConfirmDeleteCompany(this.name);" class="mouseover" id="butDelete" tabindex="25"/><img src="../../../../images/Tclose.jpg" alt="Close" name="Close" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
              </tr>
              </table></td>
      </tr>
    </table>
</div>
</div>
</body>
</html>
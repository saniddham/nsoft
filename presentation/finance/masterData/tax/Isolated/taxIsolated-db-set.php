<?php 
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	$id 			= $_REQUEST['cboSearch'];
	$code			= trim($_REQUEST['txtCode']);
	$description	= trim($_REQUEST['txtDesc']);
	$rate		= val(trim($_REQUEST['txtRate']));
	$chartofaccount	= null($_REQUEST['cboChartOfAcc']);
	$return	=  (trim($_REQUEST['cboReturn']));
	$intStatus		= ($_REQUEST['chkActive']?1:0);		
	/////////// tax isolated insert part /////////////////////
	if($requestType=='add')
	{
		 $sql = "INSERT INTO `mst_financetaxisolated` (`strCode`,`strDescription`,`dblRate`,`intChartOfAccountId`,`strTaxReturn`,`intStatus`,`intCreator`,dtmCreateDate) 
				VALUES ('$code','$description','$rate',$chartofaccount,'$return','$intStatus','$userId',now())";
		$result = $db->RunQuery($sql);
		if($result){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
	}
	/////////// tax isolated update part /////////////////////
	else if($requestType=='edit')
	{
		$sql = "UPDATE `mst_financetaxisolated` SET 	strCode		='$code',
												strDescription		='$description',
												dblRate				='$rate',
												intChartOfAccountId	=$chartofaccount,
												strTaxReturn		='$return',
												intStatus			='$intStatus',
												intModifyer			='$userId'
				WHERE (`intId`='$id')";
		$result = $db->RunQuery($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Updated successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	/////////// tax isolated delete part /////////////////////
	else if($requestType=='delete')
	{
		$sql = "DELETE FROM `mst_financetaxisolated` WHERE (`intId`='$id')  ";
		$result = $db->RunQuery($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Deleted successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	echo json_encode($response);
?>
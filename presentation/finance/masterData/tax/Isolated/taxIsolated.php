<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$sql 	= "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$companyId = $row['intCompanyId']; 
}
$tId = (!isset($_REQUEST['id'])?"":$_REQUEST['id']);
?>

<script type="text/javascript" >
var tId = '<?php echo $tId; ?>';
</script>

<title>Tax Isolated</title>
<?php
//include 		"include/javascript.html";
?>
<!--<script type="text/javascript" src="presentation/finance/masterData/tax/Isolated/taxIsolated-js.js"></script>-->

<form id="frmTaxIsolated" name="frmTaxIsolated" method="post" >
<div align="center">
  <div class="trans_layoutD">
	<div class="trans_text">Tax Isolated</div>
    <table width="100%">
<tr>
<td width="100%"><table width="100%">
  <tr>
    <td width="5" class="normalfnt">&nbsp;</td>
    <td width="94" class="normalfnt">Tax Code <span class="compulsoryRed">*</span></td>
    <td width="170" bgcolor="#FFFFFF" class="">
    <input type="text" name="txtCode" id="txtCode" style="width:95px" class="validate[required,maxSize[250]]" /></td>
    <td width="113" bgcolor="#FFFFFF" class=""><span class="normalfnt">Description <span class="compulsoryRed">*</span></span></td>
    <td width="170" bgcolor="#FFFFFF" class="">
    <input name="txtDesc" type="text" id="txtDesc" style="width:170px" class="validate[required,maxSize[250]]"/></td>
    <td width="12" bgcolor="#FFFFFF" class="">&nbsp;</td>
  </tr>
  <tr>
    <td class="normalfnt">&nbsp;</td>
    <td width="94" class="normalfnt">Tax Rate <span class="compulsoryRed">*</span></td>
    <td bgcolor="#FFFFFF" class="">
    <input name="txtRate" type="text" id="txtRate" style="width:65px" class="validate[required,custom[number]]" />
      <strong><span class="normalfnt">%</span></strong></td>
    <td bgcolor="#FFFFFF" class=""><span class="normalfnt">Active</span></td>
    <td bgcolor="#FFFFFF" class=""><input name="chkActive" type="checkbox" id="chkActive" checked="checked" /></td>
    <td bgcolor="#FFFFFF" class="">&nbsp;</td>
  </tr>
  <tr>
    <td class="normalfnt">&nbsp;</td>
    <td class="normalfnt">Tax Return</td>
    <td bgcolor="#FFFFFF" class=""><span class="normalfntMid">
      <select name="cboReturn" id="cboReturn" style="width:170px">
        <option>&nbsp;</option>
        <option value="OutputA">Output A</option>
        <option value="OutputB">Output B</option>
        <option value="OutputC">Output C</option>
        <option value="OutputD">Output D</option>
        <option value="OutputE">Output E</option>
        <option value="OutputF">Output F</option>
        <option value="OutputG">Output G</option>
        <option value="InputH">Input H</option>
        <option value="InputI">Input I </option>
        <option value="InputJ">Input J </option>
        <option value="InputK">Input K</option>
      </select>
    </span></td>
    <td bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
    <td bgcolor="#FFFFFF" class="">&nbsp;</td>
    <td bgcolor="#FFFFFF" class="">&nbsp;</td>
  </tr>
  <tr style="display:none">
    <td class="normalfnt">&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td bgcolor="#FFFFFF" class="">&nbsp;</td>
    <td bgcolor="#FFFFFF" class="normalfnt">Ledger Accounts <span class="compulsoryRed">*</span></td>
    <td bgcolor="#FFFFFF" class=""><span class="normalfntMid">
      <select name="cboChartOfAcc" id="cboChartOfAcc" style="width:170px" class="validate[required]">
        <option value=""></option>
        <?php  $sql = "SELECT
						intId,
						strCode,
						strName
						FROM mst_financechartofaccounts
						WHERE intCompanyId = $companyId
						AND
						intStatus = '1' AND  intFinancialTypeId = '7' AND strType = 'Posting'
						order by strCode
						"; // Financial Type Is (Balance Sheet) "Exchange Gain / Loss Account" --> 'Id - 27'
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strCode']."-".$row['strName']."</option>";
						}
        ?>
      </select>
    </span></td>
    <td bgcolor="#FFFFFF" class="">&nbsp;</td>
  </tr>
</table></td>
</tr>
<tr>
  <td><div style="overflow:scroll;width:100%;height:150px;" id="divGrid">
    <table width="100%" id="tblMainGrid" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
      <tr class="">
        <td width="12%" height="27"    bgcolor="#FAD163" class="normalfntMid"><span class="normalfnt"><strong>Tax Code</strong></span></td>
        <td width="29%"    bgcolor="#FAD163" class="normalfntMid"><span class="normalfnt"><strong>Description</strong></span></td>
        <td width="10%"    bgcolor="#FAD163" class="normalfntMid"><span class="normalfnt"><strong>Tax Rate</strong></span></td>
        <td  bgcolor="#FAD163" class="normalfntMid"  ><span class="normalfnt"><strong>Ledger Accounts</strong></span></td>
        <td  bgcolor="#FAD163" class="normalfntMid"  ><span class="normalfnt"><strong>Active</strong></span></td>
      </tr>
      <?php
	  $sql = "SELECT
						mst_financetaxisolated.intId AS taxId,
						mst_financetaxisolated.strCode,
						mst_financetaxisolated.dblRate,
						mst_financetaxisolated.strDescription,
						mst_financetaxisolated.strTaxReturn,
						mst_financetaxisolated.intStatus,
						(select A.intChartOfAccountId
							from 
							mst_financetaxactivate A where A.intTaxId = mst_financetaxisolated.intId 
							and A.intCompanyId='$companyId'
						) as chartId,
						(
						SELECT
						mst_financechartofaccounts.strName
						FROM
						mst_financetaxactivate AS A
						Inner Join mst_financechartofaccounts ON mst_financechartofaccounts.intId = A.intChartOfAccountId
						where A.intTaxId =mst_financetaxisolated.intId 
						and A.intCompanyId='$companyId'
						) as charOfAccName
						FROM
						mst_financetaxisolated";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
	  ?>
      <tr class="normalfnt">
     <td id=<?php echo $row['taxId'];?> class="loadId" align="center" bgcolor="#FFFFFF"><u><?php echo $row['strCode'];?></u></td>
        <td align="center" bgcolor="#FFFFFF"><?php echo $row['strDescription'];?></td>
        <td align="center" bgcolor="#FFFFFF"><?php echo $row['dblRate'];?></td>
        <td width="29%" align="center" bgcolor="#FFFFFF" class=""><?php echo $row['charOfAccName'];?></td>
        <td width="20%" align="center" bgcolor="#FFFFFF" class="">
       <input disabled="disabled" <?php echo($row['intStatus']?'checked':''); ?> type="checkbox"/></td>
      </tr>
      <?php 
        } 
       ?>
    </table>
  </div></td>
</tr>
<tr>
  <td width="100%" align="center" bgcolor=""><img border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24" class="mouseover" id="butNew" tabindex="28"/><?php echo($form_permision['edit']==1?'<img border="0" src="images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/>':'');echo($form_permision['delete']==1?'<img border="0" src="images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="25"/>':'');?><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
</tr>
</table>
</div>
</div>
</form>
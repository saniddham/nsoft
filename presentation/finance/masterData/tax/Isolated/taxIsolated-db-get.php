<?php
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	
	/////////// tax isolated load part /////////////////////
	if($requestType=='loadCombo')
	{
		$sql =  $sql = "SELECT
				intId,
				strCode,
				strDescription,
				dblRate,
				intChartOfAccountId,
				strTaxReturn,
				intStatus
				FROM mst_financetaxisolated";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['code'] 			= $row['strCode'];
			$response['description'] 	= $row['strDescription'];
			$response['taxrate'] 		= $row['dblRate'];
			$response['chartofaccount'] = $row['intChartOfAccountId'];
			$response['taxreturn']      = $row['strTaxReturn'];
			$response['status'] 		= ($row['intStatus']?true:false);
		}
		echo json_encode($response);
	}
	else if($requestType=='loadDetails')
	{
		$id  = $_REQUEST['id'];
		$sql =  $sql = "SELECT
				intId,
				strCode,
				strDescription,
				dblRate,
				intChartOfAccountId,
				strTaxReturn,
				intStatus
				FROM mst_financetaxisolated
				WHERE
					intId =  '$id'
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['code'] 			= $row['strCode'];
			$response['description'] 	= $row['strDescription'];
			$response['taxrate'] 		= $row['dblRate'];
			$response['chartofaccount'] = $row['intChartOfAccountId'];
			$response['taxreturn']      = $row['strTaxReturn'];
			$response['status'] 		= ($row['intStatus']?true:false);
		}
		echo json_encode($response);
	}
	
?>
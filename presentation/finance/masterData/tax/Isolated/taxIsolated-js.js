var id 			= '';
var taxCode 	= '';
var basePath 	= "presentation/finance/masterData/tax/Isolated/";
	
$(document).ready(function() {
  		
	$("#frmTaxIsolated").validationEngine();
	$('#frmTaxIsolated #txtCode').focus();
	
	if(tId!='')
	{
		var url 	= basePath+"taxIsolated-db-get.php";
		var httpobj = $.ajax({
						url:url,
						dataType:'json',
						data:'requestType=loadDetails&id='+tId,
						async:false,
						
						success:function(json){
							$('#frmTaxIsolated #txtCode').val(json.code);
							$('#frmTaxIsolated #txtDesc').val(json.description);
							$('#frmTaxIsolated #txtRate').val(json.taxrate);
							$('#frmTaxIsolated #cboChartOfAcc').val(json.chartofaccount);
							$('#frmTaxIsolated #cboReturn').val(json.taxreturn);
							
							if(json.status)
								$('#frmTaxIsolated #chkActive').attr('checked',true);
							else
								$('#frmTaxIsolated #chkActive').removeAttr('checked');
						}
			});
		taxCode = $('#frmTaxIsolated #txtCode').val();
	}
	
  
  $("#tblMainGrid").each(function () {
  $('.loadId').css('cursor', 'pointer');
  $('.loadId').off('mouseover').on('mouseover',function () {
  $(this).css('color', 'red');
  });
  $('.loadId').off('mouseout').on('mouseout',function () {
  $(this).css('color', 'black');
  });
  });
  
  ///save button click event
  $('#frmTaxIsolated #butSave').off('click').on('click',function(){
	var requestType = '';
	if ($('#frmTaxIsolated').validationEngine('validate'))   
    { 
		if(taxCode=='')
			requestType = 'add';
			
		else
			requestType = 'edit';
		
		var url = basePath+"taxIsolated-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json",  
			type: "post",
			data:$("#frmTaxIsolated").serialize()+'&requestType='+requestType+'&cboSearch='+id,
			async:false,
			
			success:function(json){
					$('#frmTaxIsolated #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						id = '';
						taxCode = '';
						$('#frmTaxIsolated').get(0).reset();
						var t=setTimeout("alertx()",1000);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmTaxIsolated #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
				}		
			});
	}
   });
   
   /////////////////////////////////////////////////////
   //// load tax isolated details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmTaxIsolated #tblMainGridRow').off('click').on('click',function(){
	   $('#frmTaxIsolated').validationEngine('hide');
   });
   $('#tblMainGrid .loadId').off('click').on('click',function() {
    	id = $(this).attr('id');
		$('#frmTaxIsolated').validationEngine('hide');
		var url = basePath+"taxIsolated-db-get.php";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:'requestType=loadDetails&id='+id,
			async:false,
			
			success:function(json){
					$('#frmTaxIsolated #txtCode').val(json.code);
					$('#frmTaxIsolated #txtDesc').val(json.description);
					$('#frmTaxIsolated #txtRate').val(json.taxrate);
					$('#frmTaxIsolated #cboChartOfAcc').val(json.chartofaccount);
					$('#frmTaxIsolated #cboReturn').val(json.taxreturn);
					if(json.status)
						$('#frmTaxIsolated #chkActive').attr('checked',true);
					else
						$('#frmTaxIsolated #chkActive').removeAttr('checked');
			}
	}
	);
	taxCode = $('#frmTaxIsolated #txtCode').val();
	//////////// end of load details /////////////////
	});
	
	$('#frmTaxIsolated #butNew').off('click').on('click',function(){
		$('#frmTaxIsolated').get(0).reset();
		id = '';
		taxCode = '';
		$('#frmTaxIsolated #txtName').focus();
		loadCombo_frmTaxIsolated();
	});
    $('#frmTaxIsolated #butDelete').off('click').on('click',function(){
		if(taxCode =='')
		{
			$('#frmTaxIsolated #butDelete').validationEngine('showPrompt', 'Please select code.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+taxCode+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = basePath+"taxIsolated-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											type: 'post',
											data:'requestType=delete&cboSearch='+id,
											async:false,
											success:function(json){
												$('#frmTaxIsolated #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													var t=setTimeout("alertDelete()",1000);
													return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
					}
				}
		 	});
		}
	});
});
function loadCombo_frmTaxIsolated()
{
	location.reload();
}
function alertx()
{
	$('#frmTaxIsolated #butSave').validationEngine('hide')	;
	loadCombo_frmTaxIsolated();
}
function alertDelete()
{
	$('#frmTaxIsolated #butDelete').validationEngine('hide')	;
	loadCombo_frmTaxIsolated();
}

<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$companyId 	= $_SESSION['headCompanyId'];

//---------------------------------------------New Grid-----------------------------------------------------
include_once "libraries/jqdrid/inc/jqgrid_dist.php";

$sql = "select * from(SELECT
		mst_financetaxisolated.intId,
		mst_financetaxisolated.intCreator,
		mst_financetaxisolated.dtmCreateDate,
		mst_financetaxisolated.intModifyer,
		mst_financetaxisolated.dtmModifyDate,
		user1.intUserId AS userOne,
		user1.strUserName AS creator,
		user2.intUserId AS userTwo,
		user2.strUserName AS modifyer,
		CONCAT(mst_financetaxisolated.strCode,' (' ,mst_financetaxisolated.strDescription,')') AS item
		FROM
		mst_financetaxisolated
		left outer Join sys_users AS user1 ON mst_financetaxisolated.intCreator = user1.intUserId
		left outer Join sys_users AS user2 ON mst_financetaxisolated.intModifyer = user2.intUserId) as t where 1=1";

$col 	= array();
$cols 	= array();
//Ket Item
$col["title"] 	= "Item"; // caption of column
$col["name"] 	= "item"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "8";
$col["align"] 	= "left";
$cols[] = $col;	$col=NULL;

//Creator
$col["title"] 	= "Created By"; // caption of column
$col["name"] 	= "creator"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
$col["align"] 	= "left";
$cols[] = $col;	$col=NULL;

//Date
$col["title"] = "Created Date"; // caption of column
$col["name"] = "dtmCreateDate"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Modifyer
$col["title"] = "Modified by"; // caption of column
$col["name"] = "modifyer"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Date
$col["title"] = "Modified Date"; // caption of column
$col["name"] = "dtmModifyDate"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

////Deleter
//$col["title"] = "Deleted By"; // caption of column
//$col["name"] = ""; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
//$col["width"] = "3";
//$col["align"] = "left";
//$cols[] = $col;	$col=NULL;
//
////Date
//$col["title"] = "Deleted Date"; // caption of column
//$col["name"] = ""; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
//$col["width"] = "3";
//$col["align"] = "center";
//$cols[] = $col;	$col=NULL;

$jq = new jqgrid('',$db);

$grid["caption"] 		= "Audit Trial Listing - TAX (Isolated)";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'intId'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes

//<><><><><><>======================Compulsory Code==============================<><><><><><>
	$jq->set_options($grid);
	$jq->select_command = $sql;
	$jq->set_columns($cols);
	$jq->set_actions(array(	
		"add"=>false, // allow/disallow add
		"edit"=>false, // allow/disallow edit
		"delete"=>false, // allow/disallow delete
		"rowactions"=>false, // show/hide row wise edit/del/save option
		"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
		"export"=>true
	) 
	);

$out = $jq->render("list1");
echo $out;
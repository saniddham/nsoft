<?php 
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	$id 			= $_REQUEST['cboSearch'];
	$code			= trim($_REQUEST['txtCode']);
	$description	= trim($_REQUEST['txtDesc']);
	$process		= trim($_REQUEST['taxProcess']);
	$intStatus		= ($_REQUEST['chkActive']?1:0);		
	/////////// tax group insert part /////////////////////
	if($requestType=='add')
	{
		 $sql = "INSERT INTO `mst_financetaxgroup` (`strCode`,`strDescription`,`strProcess`,`intStatus`,`intCreator`,dtmCreateDate) 
				VALUES ('$code','$description','$process','$intStatus','$userId',now())";
		$result = $db->RunQuery($sql);
		if($result){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
	}
	/////////// tax group update part /////////////////////
	else if($requestType=='edit')
	{
		$sql = "UPDATE `mst_financetaxgroup` SET 	strCode				='$code',
													strDescription		='$description',
													strProcess			='$process',
													intStatus			='$intStatus',
													intModifyer			='$userId'
				WHERE (`intId`='$id')";
		$result = $db->RunQuery($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Updated successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	/////////// tax group delete part /////////////////////
	else if($requestType=='delete')
	{
		$sql = "DELETE FROM `mst_financetaxgroup` WHERE (`intId`='$id')  ";
		$result = $db->RunQuery($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Deleted successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	echo json_encode($response);
?>
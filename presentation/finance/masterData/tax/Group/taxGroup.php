<?php
session_start();
$backwardseperator = "../../../../../";
$mainPath = $_SESSION['mainPath'];	
$thisFilePath =  $_SERVER['PHP_SELF'];
include  "{$backwardseperator}dataAccess/permisionCheck.inc";
$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$companyId = $row['intCompanyId']; 
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Tax Group</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../../css/promt.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="taxGroup-js.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" type="text/css" href="../../../../../libraries/calendar/theme.css" />
<script src="../../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

<link rel="stylesheet" href="../../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../../libraries/validate/template.css" type="text/css">

</head>

<body>
<table id="header" width="100%" border="0" align="center" bgcolor="#FFFFFF">
  <tr>
    <td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator.'Header.php'; ?></td>
  </tr>
</table>

<script src="../../../../../libraries/validate/jquery-1.js" type="text/javascript"></script> 
<script src="../../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script> 
<script src="../../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script> 
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.js"></script> 
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<form id="frmTaxGroup" name="frmTaxGroup" method="post" action="taxGroup-db-set.php" autocomplete="off" >
  <div align="center">
    <div class="trans_layoutD">
      <div class="trans_text">Tax Group</div>
      <table width="100%">
        <tr>
          <td width="100%"><table width="100%">
              <tr>
                <td width="10" class="normalfnt">&nbsp;</td>
                <td width="123" class="normalfnt">Tax Group Code <span class="compulsoryRed">*</span></td>
                <td width="95" bgcolor="#FFFFFF" class="">
                <input type="text" name="txtCode" id="txtCode" style="width:95px" class="validate[required,maxSize[250]]"/></td>
                <td width="145" align="center" bgcolor="#FFFFFF" class=""><span class="normalfntMid">Description <span class="compulsoryRed">*</span></span></td>
                <td width="180" bgcolor="#FFFFFF" class="">
                <input name="txtDesc" type="text" id="txtDesc" style="width:180px" class="validate[required,maxSize[250]]"/></td>
                <td width="11" bgcolor="#FFFFFF" class="">&nbsp;</td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td width="123" class="normalfnt">Active</td>
                <td bgcolor="#FFFFFF" class=""><input name="chkActive" type="checkbox" id="chkActive" checked="checked" /></td>
                <td bgcolor="#FFFFFF" class="">&nbsp;</td>
                <td bgcolor="#FFFFFF" class="">&nbsp;</td>
                <td bgcolor="#FFFFFF" class="">&nbsp;</td>
              </tr>
              <tr>
                <td colspan="6" class="normalfnt"><table width="100%" id="tblOperationMainGrid">
                    <?php 
  	$sql = "SELECT
			mst_financetaxisolated.intId,
			mst_financetaxisolated.strDescription
			FROM mst_financetaxisolated
			";
	$result = $db->RunQuery($sql);
	$i=0;
	$rowCount = mysqli_num_rows($result);
	while($row=mysqli_fetch_array($result))
	{
		$i++;
    ?>
                    <tr>
                      <td width="2%" class="normalfnt">&nbsp;</td>
                      <td align="center" bgcolor="#CCCCCC" class="tableBorder_allRound"><span class="normalfntMid">Tax Code</span></td>
                      <td width="27%" bgcolor="#FFFFFF" class=""><select  disabled="disabled" name="cboSearch" class="taxCodeValue" id="cboSearch"  style="width:150px;"  >
                          <option value=""></option>
                          <?php  $sql1 = "SELECT
										mst_financetaxisolated.intId,
										mst_financetaxisolated.strCode,
										mst_financetaxactivate.intCompanyId
										FROM
										mst_financetaxisolated
										Inner Join mst_financetaxactivate ON mst_financetaxisolated.intId = mst_financetaxactivate.intTaxId
										WHERE
										mst_financetaxactivate.intCompanyId = '$companyId'
										";
						$result1 = $db->RunQuery($sql1);
						while($row1=mysqli_fetch_array($result1))
						{
							echo "<option value=\"".$row1['intId']."\">".$row1['strCode']."</option>";	
						}
         ?>
                        </select></td>
                      <td width="39%" bgcolor="#FFFFFF" class="">&nbsp;</td>
                      <td width="2%" bgcolor="#FFFFFF" class="">&nbsp;</td>
                    </tr>
                    <?php
  if($i<>$rowCount)
  {
  ?>
                    <tr>
                      <td class="normalfnt">&nbsp;</td>
                      <td width="30%" align="right" bgcolor="#FFFFFF" class="">&nbsp;</td>
                      <td align="right" bgcolor="#FFFFFF"><select name="cboOperation" id="cboOperation" style="width:100px">
                          <option value="">&nbsp;</option>
                          <option value="Inclusive">Inclusive</option>
                          <option value="Exclusive">Exclusive </option>
                        </select></td>
                      <td align="center" bgcolor="#E4FCFC" class="tableBorder_allRound"><span class="normalfntMid">Operation</span></td>
                      <td bgcolor="#FFFFFF" class="">&nbsp;</td>
                    </tr>
                    <?php
 }
?>
                    <?php
}
?>
                  </table></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td><div style="overflow:scroll;width:100%;height:150px;" id="divGrid">
              <table width="100%" id="tblMainGrid" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
                <tr class="">
                  <td width="17%" height="22"    bgcolor="#FAD163" class="normalfntMid"><span class="normalfnt"><strong>Group Code</strong></span></td>
                  <td width="32%" bgcolor="#FAD163" class="normalfntMid"><span class="normalfnt"><strong>Description</strong></span></td>
                  <td  bgcolor="#FAD163" class="normalfntMid" style="display:none"><strong>Process</strong></td>
                  <td  bgcolor="#FAD163" class="normalfntMid"><span class="normalfnt"><strong>Active</strong></span></td>
                </tr>
                <?php
	  	$sql = "SELECT
				mst_financetaxgroup.intId,
				mst_financetaxgroup.strCode,
				mst_financetaxgroup.strDescription,
				mst_financetaxgroup.strProcess,
				mst_financetaxgroup.intStatus
				FROM
				mst_financetaxgroup";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
	  ?>
                <tr class="normalfnt">
                  <td id='<?php echo $row['intId'];?>' class="loadId" align="center" bgcolor="#FFFFFF">
				  <u><?php echo $row['strCode'];?></u></td>
                  <td align="center" bgcolor="#FFFFFF"><?php echo $row['strDescription'];?></td>
                  <td width="41%" align="center" bgcolor="#FFFFFF" style="display:none" class=""><?php echo $row['strProcess'];?></td>
                  <td width="10%" align="center" bgcolor="#FFFFFF" class=""><input disabled="disabled" <?php echo($row['intStatus']?'checked':''); ?> type="checkbox"/></td>
                </tr>
                <?php 
       } 
      ?>
              </table>
            </div></td>
        </tr>
        <tr>
          <td width="100%" align="center" bgcolor=""><img style="display:none" border="0" src="../../../../../images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:none" border="0" src="../../../../../images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/><img style="display:none" border="0" src="../../../../../images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="25"/><a href="../../../../../main.php"><img  src="../../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
        </tr>
      </table>
    </div>
  </div>
</form>
</body>
</html>
		
$(document).ready(function() {
		var id = '';
		var taxCode = '';
  		$("#frmTaxGroup").validationEngine();
		$('#frmTaxGroup #txtCode').focus();
  //permision for add 
  if(intAddx)
  {
 	$('#frmTaxGroup #butNew').show();
	$('#frmTaxGroup #butSave').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmTaxGroup #butSave').show();
	$('#frmTaxGroup #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmTaxGroup #butDelete').show();
	$('#frmTaxGroup #cboSearch').removeAttr('disabled');
  }
  
  //permision for view
  if(intViewx)
  {
	$('#frmTaxGroup #cboSearch').removeAttr('disabled');
  }

  $("#tblMainGrid").each(function () {
  $('.loadId').css('cursor', 'pointer');
  $('.loadId').mouseover(function () {
  $(this).css('color', 'red');
  });
  $('.loadId').mouseout(function () {
  $(this).css('color', 'black');
  });
  });
  
  //save button click event
  $('#frmTaxGroup #butSave').click(function(){
	  var taxGroupArr= ''
	  $('#tblOperationMainGrid tr').each(function()
	  {	
	  	if($(this).find('td').eq(2).find('select').val() != '')
		{
			taxGroupArr += $(this).find('td').eq(2).find('select').val() + '/';	
		}
	  });
	  
	taxGroupArr = taxGroupArr.substr(0,taxGroupArr.length-1);

		
	var requestType = '';
	if ($('#frmTaxGroup').validationEngine('validate'))   
    { 
		if(taxCode=='')
			requestType = 'add';
			
		else
			requestType = 'edit';
		
		var url = "taxGroup-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json",
			type:'post',  
			data:$("#frmTaxGroup").serialize()+'&requestType='+requestType+'&cboSearch='+id+'&taxProcess='+taxGroupArr,
			async:false,
			
			success:function(json){
					$('#frmTaxGroup #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						id = '';
						taxCode = '';
						$('#frmTaxGroup').get(0).reset();
						var t=setTimeout("alertx()",1000);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmTaxGroup #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
				}		
			});
	}
   });
   
   /////////////////////////////////////////////////////
   //// load tax group details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmTaxGroup #tblMainGridRow').click(function(){
	   $('#frmTaxGroup').validationEngine('hide');
   });
   $('#tblMainGrid .loadId').click(function() {
    	id = $(this).attr('id');
		$('#frmTaxGroup').validationEngine('hide');
		var url = "taxGroup-db-get.php";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:'requestType=loadDetails&id='+id,
			async:false,
			
			success:function(json){
					$('#frmTaxGroup #txtCode').val(json.code);
					$('#frmTaxGroup #txtDesc').val(json.description);
					//$('#frmTaxGroup #txtRate').val(json.taxrate);
					if(json.status)
						$('#frmTaxGroup #chkActive').attr('checked',true);
					else
						$('#frmTaxGroup #chkActive').removeAttr('checked');
			}
	}
	);
	taxCode = $('#frmTaxGroup #txtCode').val();
	//////////// end of load details /////////////////
	});
	
	$('#frmTaxGroup #butNew').click(function(){
		$('#frmTaxGroup').get(0).reset();
		id = '';
		taxCode = '';
		$('#frmTaxGroup #txtName').focus();
		loadCombo_frmTaxGroup();
	});
    $('#frmTaxGroup #butDelete').click(function(){
		if(taxCode =='')
		{
			$('#frmTaxGroup #butDelete').validationEngine('showPrompt', 'Please select code.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+taxCode+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = "taxGroup-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											data:'requestType=delete&cboSearch='+id,
											async:false,
											success:function(json){
												$('#frmTaxGroup #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													var t=setTimeout("alertDelete()",1000);
													return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
					}
				}
		 	});
		}
	});
});
function loadCombo_frmTaxGroup()
{
	location.reload();
}
function alertx()
{
	$('#frmTaxGroup #butSave').validationEngine('hide')	;
	loadCombo_frmTaxGroup();
}
function alertDelete()
{
	$('#frmTaxGroup #butDelete').validationEngine('hide')	;
	loadCombo_frmTaxGroup();
}

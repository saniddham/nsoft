<?php
session_start();
$backwardseperator = "../../../../../";
$thisFilePath =  $_SERVER['PHP_SELF'];

include  "{$backwardseperator}dataAccess/permisionCheck.inc";

$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$companyId = $row['intCompanyId']; 
}

if($_SERVER['REQUEST_METHOD'] == "POST")
{
	$optionValue = trim($_REQUEST['cboSearchOption']);
	$searchValue = trim($_REQUEST['txtSearchValue']);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Chart of Accounts Listing</title>

<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../../css/tblstyle.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../../libraries/jquery/jquery-1.3.2.min.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/scrolltable.js"></script>

<script type="application/javascript" src="../../../../../libraries/javascript/zebraStripe.js"></script>

<script type="text/javascript" language="javascript">
function pageSubmit()
{
	document.getElementById('frmChartOfAccountsListing').submit();	
}
function clearValue()
{
	document.getElementById('txtSearchValue').value = '';
	document.getElementById('txtSearchValue').focus();
}
</script>

</head>

<body>
    <form id="frmChartOfAccountsListing" name="frmChartOfAccountsListing" method="post" autocomplete="off" action="chartOfAccountsListing.php">
        <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
                <td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator . 'Header.php'; ?></td>
            </tr> 
        </table>
        <div align="center">
            <div class="trans_layoutL">
                <div class="trans_text">Chart of Accounts Listing</div>
                <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
                    <td><table width="100%" border="0">
                            <tr>
                                <td><table width="100%" border="0" cellpadding="1" cellspacing="0" class="tableBorder_allRound">
                                        <tr>
                                            <td width="16%" height="22" class="normalfnt">&nbsp;</td>
                                            <td width="18%" class="normalfntMid">Searching Option - Heading</td>
                                            <td width="17%" class="normalfnt">
<select name="cboSearchOption" id="cboSearchOption" onchange="clearValue();" style="width:150px">
<option value="">&nbsp;</option>
<option value="mst_financechartofaccounts.strCode" <?php echo($optionValue == 'mst_financechartofaccounts.strCode' ? 'selected' : '') ?> >Accounts Code</option>
<option value="mst_financechartofaccounts.strName" <?php echo($optionValue == 'mst_financechartofaccounts.strName' ? 'selected' : '') ?>>Accounts Name</option>
<option value="mst_financialsubtype.strName" <?php echo($optionValue == 'mst_financialsubtype.strName' ? 'selected' : '') ?>>Financial Type</option>
<!--<option value="mst_financechartofaccounts.strType" <?php echo($optionValue == 'mst_financechartofaccounts.strType' ? 'selected' : '') ?>>Type</option>
--></select></td>
                                            <td width="16%"><input value="<?php echo $searchValue; ?>" type="text" name="txtSearchValue" id="txtSearchValue" /></td>
                                            <td width="18%" class="normalfnt">
                                                <a href="#">
                                                    <img src="../../../../../images/search.png" width="20" height="20" onclick="pageSubmit();" /></a></td>
                                            <td width="15%">&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td height="22" class="normalfnt">&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td class="normalfnt">&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td>
                                    <div id="_table_wrap" style="overflow: hidden; display: inline-block; border: 1px solid rgb(136, 136, 136);">
                                        <div id="_head_wrap" style="width: 900px; overflow: hidden;">
                                            <table class="tbl1 scroll" id="_head" style="table-layout: fixed;">
                                                <thead>
                                                    <tr class="gridHeader">
                                                        <th height="22" style="width: 100px;" ><strong>Ledger Accounts   Code</strong></th>
                                                        <th style="width: 159px;" ><strong>Ledger Accounts   Name</strong></th>
                                                        <th style="width: 98px;" >Financial Type</th>
                                                        <th style="width: 69px;" ><strong>Type</strong></th>
                                                        <th style="width: 60px;" >Posting Level</th>
                                                        <th style="width: 40px;" ><strong>Active</strong></th>
                                                        <th style="width: 40px;" >View</th>
                                                        <th style="width: 10px;" ></th>
                                                    </tr>
                                            </table>
                                        </div>
                                        <div class="normalfnt" id="_body_wrap" style="width: 900px; height: 320px; overflow: scroll;">
                                            <table class="tbl1 scroll"> 
                                                <tbody>
                                                <?php

                                                    if($searchValue!='')
                                                        $wherePart = " and $optionValue like '%$searchValue%'";
				
                                                        $sql = "SELECT
                                                                        DISTINCT mst_financechartofaccounts.intId AS accId,
                                                                        mst_financialsubtype.intId,
                                                                        mst_financechartofaccounts.strCode,
                                                                        mst_financechartofaccounts.strName AS cName,
                                                                        mst_financechartofaccounts.strType,
                                                                        mst_financialsubtype.strName AS fName,
                                                                        mst_financechartofaccounts.intFinancialTypeId,
                                                                        mst_financechartofaccounts.strPostingLevel,
                                                                        mst_financechartofaccounts.intStatus,
                                                                        mst_financechartofaccounts_companies.intCompanyId,
                                                                        mst_financechartofaccounts_companies.intChartOfAccountId
                                                                        FROM
                                                                        mst_financechartofaccounts
                                                                        Inner Join mst_financialsubtype ON mst_financechartofaccounts.intFinancialTypeId = mst_financialsubtype.intId
                                                                        Inner Join mst_financechartofaccounts_companies ON mst_financechartofaccounts_companies.intChartOfAccountId = mst_financechartofaccounts.intId
                                                                        WHERE mst_financechartofaccounts.strType = 'Heading'
                                                                        $wherePart
																		GROUP BY mst_financechartofaccounts.intId
                                                                        ORDER BY
                                                                        mst_financechartofaccounts.strCode ASC";
																		// mst_financechartofaccounts_companies.intCompanyId = '$companyId' AND 
                                                    $result = $db->RunQuery($sql);
                                                    while($row=mysqli_fetch_array($result))
                                                    {
                                                        $id=$row['accId'];
                                                    ?>
                                                    <tr class="normalfnt">
                                                            <td class="accCode" height="16" align="center" bgcolor="#FFFFFF">
                                                                <?php echo $row['strCode']; ?></td>
                                                            <td class="accName"  align="center" bgcolor="#FFFFFF"><?php echo $row['cName']; ?></td>
                                                            <td class="fName" bgcolor="#FFFFFF"><?php echo $row['fName']; ?></td>
                                                            <td class="type" align="center" bgcolor="#FFFFFF"><?php echo $row['strType']; ?></td>
                                                            <td class="posLevel" align="center" bgcolor="#FFFFFF"><?php echo $row['strPostingLevel'] == 'null' ? '' : $row['strPostingLevel']; ?></td>
                                                            <td class="active" align="center" bgcolor="#FFFFFF">
                                                                <input disabled="disabled" <?php echo($row['intStatus'] ? 'checked' : ''); ?> type="checkbox"/></td>
                                                            <td class="view" align="center" bgcolor="#FFFFFF"><a target="_blank" href="<?php echo "../addNew/chartOfAccounts.php?id=$id"; ?>">More</a></td>
                                                        </tr>  
                                                    <tr class="normalfnt">
                                                        <td colspan="7">
                                                            <table class="tblSub1 scroll" > 
                                                                <tbody>
                                                        
                                                    <?php
                                                    $headerAcc=$row['accId'];
                                                      		$sqlSub = "SELECT
                                                            DISTINCT mst_financechartofaccounts.intId AS accId,
                                                            mst_financialsubtype.intId,
                                                            mst_financechartofaccounts.strCode,
                                                            mst_financechartofaccounts.strName AS cName,
                                                            mst_financechartofaccounts.strType,
                                                            mst_financialsubtype.strName AS fName,
                                                            mst_financechartofaccounts.intFinancialTypeId,
                                                            mst_financechartofaccounts.strPostingLevel,
                                                            mst_financechartofaccounts.intStatus,
                                                            mst_financechartofaccounts_companies.intCompanyId,
                                                            mst_financechartofaccounts_companies.intChartOfAccountId
                                                            FROM
                                                            mst_financechartofaccounts
                                                            Inner Join mst_financialsubtype ON mst_financechartofaccounts.intFinancialTypeId = mst_financialsubtype.intId
                                                            Inner Join mst_financechartofaccounts_companies ON mst_financechartofaccounts_companies.intChartOfAccountId = mst_financechartofaccounts.intId
                                                            WHERE mst_financechartofaccounts.headerAcc= $headerAcc
															GROUP BY mst_financechartofaccounts.intId
                                                            ORDER BY
                                                            mst_financechartofaccounts.strCode ASC";
															// mst_financechartofaccounts_companies.intCompanyId = '$companyId' AND
                                                            $resultSub = $db->RunQuery($sqlSub);
                                                            while($rowSub=mysqli_fetch_array($resultSub))
                                                            {
                                                                $idSub=$rowSub['accId'];
                                                    ?>
                                                    <tr class="normalfnt" style="background-color:red">
                                                        <td class="accCode" height="16" align="right" bgcolor="FFFFFF"><?php echo $rowSub['strCode']; ?></td>
                                                        <td class="accName"  align="center" bgcolor="#FFFFFF"><?php echo $rowSub['cName']; ?></td>
                                                        <td class="fName" bgcolor="#FFFFFF"><?php echo $rowSub['fName']; ?></td>
                                                        <td class="type" align="center" bgcolor="#FFFFFF"><?php echo $rowSub['strType']; ?></td>
                                                        <td class="posLevel" align="center" bgcolor="#FFFFFF"><?php echo $rowSub['strPostingLevel'] == 'null' ? '' : $rowSub['strPostingLevel']; ?></td>
                                                        <td class="active" align="center" bgcolor="#FFFFFF"><input disabled="disabled" <?php echo($rowSub['intStatus'] ? 'checked' : ''); ?> type="checkbox"/></td>
                                                        <td class="view" align="center" bgcolor="#FFFFFF"><a target="_blank" href="<?php echo "../addNew/chartOfAccounts.php?id=$idSub"; ?>">More</a></td>
                                                    </tr>
                                                    <?php 
                                                            }
                                                    ?>
                                                                    </tbody>
                                                                </table>
                                                            </td>                                                        
                                                    </tr>
                                                <?php
                                                    }
                                                ?>
                                                </tbody>
                                            </table>
                                        </div></div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    </tr>
                    <tr>
                        <td align="center" class="tableBorder_allRound"><a href="../../../../../main.php"><img  src="../../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                    </tr>
                </table></td>
                </tr>
                </table>
      
            </div>
        </div>
    </form>
</body>
</html>

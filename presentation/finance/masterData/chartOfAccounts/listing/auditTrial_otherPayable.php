<?php
session_start();
$backwardseperator = "../../../../../";
$thisFilePath =  $_SERVER['PHP_SELF'];

include  	"{$backwardseperator}dataAccess/Connector.php";

$companyId 	= $_SESSION['headCompanyId'];

//---------------------------------------------New Grid-----------------------------------------------------
include_once "../../../../../libraries/jqdrid/inc/jqgrid_dist.php";

$sql = "select * from(SELECT
		mst_finance_service_supplier_activate.intCreator,
		mst_finance_service_supplier_activate.dtmCreateDate,
		mst_finance_service_supplier_activate.intModifyer,
		mst_finance_service_supplier_activate.dtmModifyDate,
		user1.intUserId AS userOne,
		user1.strUserName AS creator,
		user2.intUserId AS userTwo,
		user2.strUserName AS modifyer,
		CONCAT(mst_finance_service_supplier.strCode,' - ',mst_finance_service_supplier.strName) AS item,
		mst_finance_service_supplier_activate.intCompanyId,
		mst_finance_service_supplier_activate.intSupplierId,
		mst_finance_service_supplier.intId
		FROM
		mst_finance_service_supplier_activate
		Left Outer Join sys_users AS user1 ON mst_finance_service_supplier_activate.intCreator = user1.intUserId
		Left Outer Join sys_users AS user2 ON mst_finance_service_supplier_activate.intModifyer = user2.intUserId
		Left Outer Join mst_finance_service_supplier ON mst_finance_service_supplier_activate.intSupplierId = mst_finance_service_supplier.intId
		WHERE
		mst_finance_service_supplier_activate.intCompanyId =  '$companyId') as t where 1=1";

//Key Item
$col["title"] 	= "Item"; // caption of column
$col["name"] 	= "item"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "8";
$col["align"] 	= "left";
$cols[] = $col;	$col=NULL;

//Creator
$col["title"] 	= "Created By"; // caption of column
$col["name"] 	= "creator"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
$col["align"] 	= "left";
$cols[] = $col;	$col=NULL;

//Date
$col["title"] = "Created Date"; // caption of column
$col["name"] = "dtmCreateDate"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Modifyer
$col["title"] = "Modified by"; // caption of column
$col["name"] = "modifyer"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Date
$col["title"] = "Modified Date"; // caption of column
$col["name"] = "dtmModifyDate"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

////Deleter
//$col["title"] = "Deleted By"; // caption of column
//$col["name"] = ""; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
//$col["width"] = "3";
//$col["align"] = "left";
//$cols[] = $col;	$col=NULL;
//
////Date
//$col["title"] = "Deleted Date"; // caption of column
//$col["name"] = ""; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
//$col["width"] = "3";
//$col["align"] = "center";
//$cols[] = $col;	$col=NULL;

$jq = new jqgrid('',$db);

$grid["caption"] 		= "Audit Trial Listing - Other Payable / Service Supplier Activation";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'intId'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes

//<><><><><><>======================Compulsory Code==============================<><><><><><>
	$jq->set_options($grid);
	$jq->select_command = $sql;
	$jq->set_columns($cols);
	$jq->set_actions(array(	
		"add"=>false, // allow/disallow add
		"edit"=>false, // allow/disallow edit
		"delete"=>false, // allow/disallow delete
		"rowactions"=>false, // show/hide row wise edit/del/save option
		"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
		"export"=>true
	) 
	);

$out = $jq->render("list1");
//<><><><><><>======================Compulsory Code==============================<><><><><><>

//---------------------------------------------New Grid-----------------------------------------------------

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Audit Trial Listing - Other Payable / Service Supplier Activation</title>
</head>

<body>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
<div align="center">
<!-------------------------------------------New Grid---------------------------------------------------->
<link rel="stylesheet" type="text/css" media="screen" href="../../../../../libraries/jqdrid/js/themes/smoothness/jquery-ui.custom.css"></link>	
<link rel="stylesheet" type="text/css" media="screen" href="../../../../../libraries/jqdrid/js/jqgrid/css/ui.jqgrid.css"></link>	

<script src="../../../../../libraries/jqdrid/js/jquery.min.js" type="text/javascript"></script>
<script src="../../../../../libraries/jqdrid/js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
<script src="../../../../../libraries/jqdrid/js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>	
<script src="../../../../../libraries/jqdrid/js/themes/jquery-ui.custom.min.js" type="text/javascript"></script>
<script src="../../../../../libraries/javascript/script.js" type="text/javascript"></script>
<!-------------------------------------------New Grid---------------------------------------------------->

<table  width="100%" border="0" align="center" bgcolor="#FFFFFF">
<tr>
 <td>
   <div align="center" style="margin:10px">
     <?php echo $out?>
   </div>
  </td>
 </tr>
</table>
</div>
</body>
</html>
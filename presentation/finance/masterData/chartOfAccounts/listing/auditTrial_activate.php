<?php
session_start();
$backwardseperator = "../../../../../";
$thisFilePath =  $_SERVER['PHP_SELF'];

include  "{$backwardseperator}dataAccess/permisionCheck2.inc";

$companyId 	= $_SESSION['headCompanyId'];

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Audit Trial Listing - Chart of Accounts Activate</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />

</head>

<body>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<div align="center">
  <div class="trans_layoutXL">
    <div class="trans_text">Audit Trial Listing - Chart of Accounts Activate</div>
    <table width="100%" cellspacing="0" cellpadding="5">
    <tr class="tableBorder_allRound">
      <td width="30%" align="center" class="normalfntMid tableBorder_allRound" bgcolor="#E4F9DF"><strong><a href="auditTrial_customer.php" target="_blank">Customer</a></strong></td>
      <td width="30%" align="center" class="normalfntMid tableBorder_allRound" bgcolor="#F5D3C2"><strong><a href="auditTrial_otherReceivable.php" target="_blank">Other Receivable / Service Customer Details</a></strong></td>
      <td width="30%" align="center" class="normalfntMid tableBorder_allRound" bgcolor="#F8F7DE"><strong><a href="auditTrial_customerItem.php" target="_blank">Customer Item</a></strong></td>
	</tr>
    <tr>
      <td width="30%" align="center" class="normalfntMid tableBorder_allRound" bgcolor="#F8F7DE"><strong><a href="auditTrial_supplier.php" target="_blank">Supplier</a></strong></td>
      <td width="30%" align="center" class="normalfntMid tableBorder_allRound" bgcolor="#E4F9DF"><strong><a href="auditTrial_otherPayable.php" target="_blank">Other Payable / Service Supplier Details</a></strong></td>
      <td width="30%" align="center" class="normalfntMid tableBorder_allRound" bgcolor="#F5D3C2"><strong><a href="auditTrial_supplierItem.php" target="_blank">Supplier Item</a></strong></td>
      </tr>
    <tr>
      <td align="center" class="normalfntMid tableBorder_allRound" bgcolor="#F5D3C2"><strong><a href="auditTrial_tax.php" target="_blank">Tax</a></strong></td>
      <td align="center" class="normalfntMid tableBorder_allRound" bgcolor="#F8F7DE"><strong><a href="auditTrial_currency.php" target="_blank">Currency</a></strong></td>
      <td align="center" class="normalfntMid tableBorder_allRound" bgcolor="#E4F9DF"><strong><a href="auditTrial_serviceProvider.php" target="_blank">Service Provider</a></strong></td>
    </tr>
    <tr>
      <td align="center">&nbsp;</td>
      <td align="center">&nbsp;</td>
      <td align="center">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="3" align="center" class="tableBorder_allRound"><a href="../../../../../main.php"><img  src="../../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
      </tr>
    <tr>
      <td colspan="3" align="center">&nbsp;</td>
    </tr>
    </table>
</div>
</div>
</body>
</html>
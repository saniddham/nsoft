<?php
session_start();
ob_start();
$backwardseperator = "../../../../../";
$thisFilePath =  $_SERVER['PHP_SELF'];

include  "{$backwardseperator}dataAccess/permisionCheck.inc";

$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$companyId = $row['intCompanyId']; 
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Chart of Accounts Activate</title>

<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../../css/tblstyle.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../../libraries/jquery/jquery-1.3.2.min.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/scrolltable.js"></script>

<script type="application/javascript" src="../../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="chartOfAccountsActivate-js.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/script.js"></script>

<script type="application/javascript" src="../../../../../libraries/javascript/zebraStripe.js"></script>

<link rel="stylesheet" href="../../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../../libraries/validate/template.css" type="text/css">

<script type="application/javascript" language="javascript">
      /* $(document).ready(function () {
            $(".collaps").each(function () {
                $parentTbl = $(this);
                $('tr', $(this)).each(function (i) {
                    if (i != 0) {
                        $(this).hide();
                    }
                });
				
                $('tr', $(this)).eq(0).css('cursor', 'pointer');
                $('tr', $(this)).eq(0).mouseover(function () {
                    $('strong', $(this)).css('color', 'red');
                });
                $('tr', $(this)).eq(0).mouseout(function () {
                    $('strong', $(this)).css('color', 'black');
                });
                $('tr', $(this)).eq(0).click(function () {
                    $('tr', $(this).parent()).each(function (i) {
                        if (i != 0) {
                            $(this).toggle();
                        }
                    });
                });
            });
        });*/
</script>

<body>
<form id="frmChartOfAccountsActivate" name="frmChartOfAccountsActivate" method="post" autocomplete="off" action="chartOfAccountsActivate-db-set.php">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
	<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
<script src="../../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">Chart of Accounts Activate</div>
	<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
      <tr>
        <td>
          <table id="tblOne" width="100%" border="0" class="collaps">
            <tr>
            <td style="width:15px">
              <input name="chkCustomer" type="checkbox" id="chkCustomer" /></td>
              <td><span class="normalfntBlue cusColor"><strong>Customer</strong></span></td>
              </tr>
            <tr class="tblOneClsMainRow" style="display:none">
              <td colspan="2">
               <table width="100%" border="0">
      <tr>
        <td>
          <div id="_table_wrap" style="overflow: hidden; display: inline-block; border: 1px solid rgb(136, 136, 136);">
            <div id="_head_wrap" style="width: 900px; overflow: hidden;">
              <table class="tbl1 scroll" id="_head" style="table-layout: fixed;">
                <thead>
                  <tr class="gridHeader">
                      <th height="22" style="width: 15px;" ><input type="checkbox" id="chkAllCustomers" name="chkAllCustomers" value="" checked="checked" /></th>
                    <th style="width: 161px;" ><span class="mainHeading4"><strong>Customer Name</strong></span></th>
                    <th style="width: 80px;" ><strong>Customer Code</strong></th>
                    <th style="width: 100px;" ><strong>Customer Type</strong></th>
                    <th style="width: 165px;" >Ledger Accounts</th>
                    <th style="width: 9px;" ></th>
                    </tr>
                  </table>
              </div>
            <div class="normalfnt" id="_body_wrap" style="width: 900px; height: 350px; overflow: scroll;">
              <table class="tbl1 scroll" id="tblCusMainGrid"> 
                <tbody>
                  <?php
	 	 		 $sql = "SELECT
						c.intId AS cusId,
						c.strCode,
						c.strName,
						mst_typeofmarketer.intId,
						mst_typeofmarketer.strName AS type
						FROM
						mst_customer AS c
						left outer Join mst_typeofmarketer ON c.intTypeId = mst_typeofmarketer.intId
						WHERE
						c.intStatus =  '1'
						ORDER BY
						c.strName ASC";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
	  			 ?>
                  <tr class="normalfnt">
                  	 <td class="accActSelect" height="16" align="center" bgcolor="#FFFFFF">
        <input type="checkbox" name=chkCus<?php echo $row['cusId'];?> id=chkCus<?php echo $row['cusId'];?> checked="checked" class="checkCustomer"/>
                     </td>
<td class="accActCusName" height="16" align="center" bgcolor="#FFFFFF">
                      	<a target="_blank" href="<?php echo "../../../../customerAndOperation/masterData/customer/addNew/customer.php?id=$row[cusId]"; ?>"><?php echo $row['strName'];?></a></td>
<td class="accActCusCode"  align="center" bgcolor="#FFFFFF" id=<?php echo $row['cusId'];?>><a target="_blank" href="<?php echo "../../../../customerAndOperation/masterData/customer/addNew/customer.php?id=$row[cusId]"; ?>"><?php echo $row['strCode'];?></a></td>
                    <td style="width: 100px;"  align="center" bgcolor="#FFFFFF"><?php echo $row['type'];?></td>
                    <td class="accActAcc" align="center" bgcolor="#FFFFFF">
                      <select name=cboCusAcc<?php echo $row['cusId'];?> id=cboCusAcc<?php echo $row['cusId'];?> style="width:100%" class="validate[required] cusAccount" >
                        <option value=""></option>
                        <?php  $sql2 = "SELECT
										mst_financechartofaccounts.intId,
										mst_financechartofaccounts.strCode,
										mst_financechartofaccounts.strName,
										mst_financechartofaccounts_companies.intCompanyId,
										mst_financechartofaccounts_companies.intChartOfAccountId
										FROM
										mst_financechartofaccounts
										Inner Join mst_financechartofaccounts_companies ON mst_financechartofaccounts_companies.intChartOfAccountId = mst_financechartofaccounts.intId
										WHERE
										intStatus = '1' AND  intFinancialTypeId = '10' AND strType = 'Posting' AND intCompanyId = '$companyId'
										order by strCode

						";
						$result2 = $db->RunQuery($sql2);
						while($row2=mysqli_fetch_array($result2))
						{
						   echo "<option value=\"".$row2['intId']."\">".$row2['strCode']."-".$row2['strName']."</option>";
						}
        			?>
                      </select></td>
                    </tr>
                  <?php 
        	    	} 
       		   	?>
                  </tbody>
                </table>
              </div></div>
          </td>
      </tr>
            </table>
              </td>
              </tr>
            </table>
          </td>
      </tr>
      <!--------------------------Other Receivable / Service Customer Details------------------->
      <tr>
        <td>
          <table id="tblOneOne" width="100%" border="0" class="collaps">
            <tr>
            <td style="width:15px">
              <input name="chkOtherCustomer" type="checkbox" id="chkOtherCustomer" /></td>
              <td><span class="normalfntBlue otherCusColor"><strong>Other Receivable / Service Customer Details</strong></span></td>
              </tr>
            <tr class="tblOneOneClsMainRow" style="display:none">
              <td colspan="2">
               <table width="100%" border="0">
      <tr>
        <td>
          <div id="_table_wrap" style="overflow: hidden; display: inline-block; border: 1px solid rgb(136, 136, 136);">
            <div id="_head_wrap" style="width: 900px; overflow: hidden;">
              <table class="tbl1 scroll" id="_head" style="table-layout: fixed;">
                <thead>
                  <tr class="gridHeader">
                      <th height="22" style="width: 15px;" ><input type="checkbox" id="chkAllOtherCustomers" name="chkAllOtherCustomers" value="" checked="checked" /></th>
                    <th style="width: 161px;" ><span class="mainHeading4"><strong>Other Receivable / Service Customer Name</strong></span></th>
                    <th style="width: 80px;" ><strong>Other Receivable / Service Customer Code</strong></th>
                    <th style="width: 100px;" ><strong>Other Receivable / Service Customer Type</strong></th>
                    <th style="width: 165px;" >Ledger Accounts</th>
                    <th style="width: 9px;" ></th>
                    </tr>
                  </table>
              </div>
            <div class="normalfnt" id="_body_wrap" style="width: 900px; height: 350px; overflow: scroll;">
              <table class="tbl1 scroll" id="tblOtherCusMainGrid"> 
                <tbody>
                  <?php
	 	 		 $sql = "SELECT
						c.intId AS cusId,
						c.strCode,
						c.strName
						FROM
						mst_finance_service_customer AS c
						WHERE
						c.intStatus =  '1'
						ORDER BY
						c.strName ASC";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
	  			 ?>
                  <tr class="normalfnt">
                  	 <td class="accActSelect" height="16" align="center" bgcolor="#FFFFFF">
       <input type="checkbox" name=chkCus<?php echo $row['cusId'];?> id=chkCus<?php echo $row['cusId'];?> checked="checked" class="checkOtherCustomer"/>
                     </td>
<td class="accActCusName" height="16" align="center" bgcolor="#FFFFFF">
                      	<a target="_blank" href="<?php echo "../../serviceCustomer/addNew/customer.php?id=$row[cusId]"; ?>"><?php echo $row['strName'];?></a></td>
<td class="accActCusCode accActOtherCusCode"  align="center" bgcolor="#FFFFFF" id=<?php echo $row['cusId'];?>><a target="_blank" href="<?php echo "../../serviceCustomer/addNew/customer.php?id=$row[cusId]"; ?>"><?php echo $row['strCode'];?></a></td>
                    <td style="width: 100px;"  align="center" bgcolor="#FFFFFF"><?php echo $row['type'];?></td>
                    <td class="accActAcc" align="center" bgcolor="#FFFFFF">
                    <select name=cboOtherCusAcc<?php echo $row['cusId'];?> id=cboOtherCusAcc<?php echo $row['cusId'];?> style="width:100%" class="validate[required] cusAccount otherCusAccount" >
                        <option value=""></option>
                        <?php  $sql2 = "SELECT
										mst_financechartofaccounts.intId,
										mst_financechartofaccounts.strCode,
										mst_financechartofaccounts.strName,
										mst_financechartofaccounts_companies.intCompanyId,
										mst_financechartofaccounts_companies.intChartOfAccountId
										FROM
										mst_financechartofaccounts
										Inner Join mst_financechartofaccounts_companies ON mst_financechartofaccounts_companies.intChartOfAccountId = mst_financechartofaccounts.intId
										WHERE
										intStatus = '1' AND  (intFinancialTypeId = '9' OR intFinancialTypeId = '11' OR intFinancialTypeId = '12' OR intFinancialTypeId = '21' OR intFinancialTypeId = '22') AND strType = 'Posting' AND intCompanyId = '$companyId'
										order by strCode

						";
						$result2 = $db->RunQuery($sql2);
						while($row2=mysqli_fetch_array($result2))
						{
						   echo "<option value=\"".$row2['intId']."\">".$row2['strCode']."-".$row2['strName']."</option>";
						}
        			?>
                      </select></td>
                    </tr>
                  <?php 
        	    	} 
       		   	?>
                  </tbody>
                </table>
              </div></div>
          </td>
      </tr>
            </table>
              </td>
              </tr>
            </table>
          </td>
      </tr>
      <!-------------------------------------------------------------------------------------------->
       <tr>
        <td>
          <table id="tblTwo" width="100%" border="0" class="collaps">
            <tr>
             <td style="width:15px">
              <input type="checkbox" name="chkCustomerItem" id="chkCustomerItem"/></td>
              <td><span class="normalfntBlue cusItmColor"><strong>Customer Item</strong></span></td>
              </tr>
            <tr class="tblTwoClsMainRow" style="display:none">
              <td colspan="2">
              <table width="100%" border="0">
      <tr>
        <td>
          <div id="_table_wrap" style="overflow: hidden; display: inline-block; border: 1px solid rgb(136, 136, 136);">
            <div id="_head_wrap" style="width: 900px; overflow: hidden;">
              <table class="tbl1 scroll" id="_head" style="table-layout: fixed;">
                <thead>
                  <tr class="gridHeader">
                    <th height="22" style="width: 15px;" ><input type="checkbox" id="chkAllCustomersItem" name="chkAllCustomersItem" value="" checked="checked" /></th>
                    <th style="width: 80px;" ><span class="mainHeading4"><strong>Customer Item Name</strong></span></th>
                    <th style="width: 161px;" ><strong>Customer Item Description</strong></th>
                    <th style="width: 165px;" >Ledger Accounts</th>
                    <th style="width: 7px;" ></th>
                    </tr>
                  </table>
              </div>
            <div class="normalfnt" id="_body_wrap" style="width: 900px; height: 350px; overflow: scroll;">
              <table class="tbl1 scroll" id="tblCusItmMainGrid"> 
                <tbody>
                  <?php
	 	 		 $sql = "SELECT
				 ci.intId,
				 ci.strName,
				 ci.strRemark
				FROM mst_financecustomeritem ci
				WHERE
				ci.intStatus =  '1'
				ORDER BY
				ci.strName ASC";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
	  			 ?>
                  <tr class="normalfnt">
                  	 <td class="accActSelect" height="16" align="center" bgcolor="#FFFFFF">
        <input type="checkbox" name=chkCusItem<?php echo $row['intId'];?> id=chkCusItem<?php echo $row['intId'];?> checked="checked" class="checkCustomerItem"/>
                     </td>
           <td class="accActCusItmCode" height="16" align="center" bgcolor="#FFFFFF" id=<?php echo $row['intId'];?>>
                      	<a target="_blank" href="<?php echo "../../customerItem/addNew/customerItem.php?id=$row[intId]";?>"><?php echo $row['strName'];?></a></td>
                    <td class="accActCusName"  align="center" bgcolor="#FFFFFF">
                      <?php echo $row['strRemark'];?></td>
                    <td class="accActAcc" align="center" bgcolor="#FFFFFF"><span class="normalfntMid">
                      <select name=cboCusItemAcc<?php echo $row['intId'];?> id=cboCusItemAcc<?php echo $row['intId'];?> style="width:100%" class="validate[required] cusItemAccount">
                        <option value=""></option>
                        <?php  $sql2 = "SELECT
										mst_financechartofaccounts.intId,
										mst_financechartofaccounts.strCode,
										mst_financechartofaccounts.strName,
										mst_financechartofaccounts_companies.intChartOfAccountId,
										mst_financechartofaccounts_companies.intCompanyId
										FROM
										mst_financechartofaccounts
										Inner Join mst_financechartofaccounts_companies ON mst_financechartofaccounts_companies.intChartOfAccountId = mst_financechartofaccounts.intId
										WHERE
										intStatus = '1' AND strType = 'Posting' AND intFinancialTypeId <> '10' AND intFinancialTypeId <> '18' AND intCompanyId = '$companyId'
										order by strCode
						"; 
						$result2 = $db->RunQuery($sql2);
						while($row2=mysqli_fetch_array($result2))
						{
							echo "<option value=\"".$row2['intId']."\">".$row2['strCode']."-".$row2['strName']."</option>";
						}
        ?>
                      </select>
                    </span></td>
                    </tr>
                  <?php 
        	    	} 
       		   	?>
                  </tbody>
                </table>
              </div></div>
          </td>
      </tr>
            </table>
              </td>
              </tr>
            </table>
          </td>
      </tr>
       <tr>
        <td>
          <table id="tblThree" width="100%" border="0" class="collaps">
            <tr>
            <td style="width:15px">
              <input type="checkbox" name="chkSupplier" id="chkSupplier"/></td>
              <td><span class="normalfntBlue supColor"><strong>Supplier</strong></span></td>
              </tr>
            <tr class="tblThreeClsMainRow" style="display:none">
              <td colspan="2">
                <table width="100%" border="0">
      <tr>
        <td>
          <div id="_table_wrap" style="overflow: hidden; display: inline-block; border: 1px solid rgb(136, 136, 136);">
            <div id="_head_wrap" style="width: 900px; overflow: hidden;">
              <table class="tbl1 scroll" id="_head" style="table-layout: fixed;">
                <thead>
                  <tr class="gridHeader">
                    <th height="22" style="width: 15px;" ><input type="checkbox" id="chkAllSupplier" name="chkAllSupplier" value="" checked="checked" /></th>
                    <th style="width: 161px;" ><span class="mainHeading4"><strong>Supplier Name</strong></span></th>
                    <th style="width: 80px;" ><strong>Supplier Code</strong></th>
                    <th style="width: 100px;" ><strong>Supplier Type</strong></th>
                    <th style="width: 165px;" >Ledger Accounts</th>
                    <th style="width: 9px;" ></th>
                    </tr>
                  </table>
              </div>
            <div class="normalfnt" id="_body_wrap" style="width: 900px; height: 350px; overflow: scroll;">
              <table class="tbl1 scroll" id="tblSupMainGrid"> 
                <tbody>
                  <?php
	 	 		 $sql = "SELECT
						s.intId AS supId,
						s.strCode,
						s.strName,
						mst_typeofsupplier.intId,
						mst_typeofsupplier.strName AS type
						FROM
						mst_supplier AS s
						left outer Join mst_typeofsupplier ON s.intTypeId = mst_typeofsupplier.intId
						WHERE
						s.intStatus =  '1'
						ORDER BY
						s.strName ASC";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
	  			 ?>
                  <tr class="normalfnt">
                  	 <td class="accActSelect" height="16" align="center" bgcolor="#FFFFFF">
        <input type="checkbox" name=chkSup<?php echo $row['supId'];?> id=chkSup<?php echo $row['supId'];?> checked="checked" class="checkSupplier"/>
                     </td>
                    <td class="accActCusName" height="16" align="center" bgcolor="#FFFFFF">
                      	<a target="_blank" href="<?php echo "../../../../customerAndOperation/masterData/suppliers/addNew/supplier.php?id=$row[supId]"; ?>"><?php echo $row['strName'];?></a></td>
                    <td class="accActSupCode"  align="center" bgcolor="#FFFFFF" id=<?php echo $row['supId'];?>><a target="_blank" href="<?php echo "../../../../customerAndOperation/masterData/suppliers/addNew/supplier.php?id=$row[supId]"; ?>"><?php echo $row['strCode'];?></a></td>
                    <td style="width: 100px;"  align="center" bgcolor="#FFFFFF"><?php echo $row['type'];?></td>
                    <td class="accActAcc" align="center" bgcolor="#FFFFFF">
                      <select name=cboSupAcc<?php echo $row['supId'];?> id=cboSupAcc<?php echo $row['supId'];?> style="width:100%" class="validate[required] supAccount" >
                        <option value=""></option>
                        <?php  $sql2 = "SELECT
										mst_financechartofaccounts.intId,
										mst_financechartofaccounts.strCode,
										mst_financechartofaccounts.strName,
										mst_financechartofaccounts_companies.intChartOfAccountId,
										mst_financechartofaccounts_companies.intCompanyId
										FROM
										mst_financechartofaccounts
										Inner Join mst_financechartofaccounts_companies ON mst_financechartofaccounts_companies.intChartOfAccountId = mst_financechartofaccounts.intId
										WHERE
										intStatus = '1' AND  (intFinancialTypeId = '18' OR intFinancialTypeId = '11' OR intFinancialTypeId = '28') AND strType = 'Posting' AND intCompanyId = '$companyId'
										order by strCode
						";
						$result2 = $db->RunQuery($sql2);
						while($row2=mysqli_fetch_array($result2))
						{
						   echo "<option value=\"".$row2['intId']."\">".$row2['strCode']."-".$row2['strName']."</option>";
						}
        			?>
                      </select></td>
                    </tr>
                  <?php 
        	    	} 
       		   	?>
                  </tbody>
                </table>
              </div></div>
          </td>
      </tr>
            </table>
                </td>
              </tr>
            </table>
          </td>
      </tr>
      <!--------------------------Other Payable / Service Supplier Details------------------->
      	<tr>
        <td>
          <table id="tblThreeOne" width="100%" border="0" class="collaps">
            <tr>
            <td style="width:15px">
              <input type="checkbox" name="chkOtherSupplier" id="chkOtherSupplier"/></td>
              <td><span class="normalfntBlue otherSupColor"><strong>Other Payable / Service Supplier Details</strong></span></td>
              </tr>
            <tr class="tblThreeOneClsMainRow" style="display:none">
              <td colspan="2">
                <table width="100%" border="0">
      <tr>
        <td>
          <div id="_table_wrap" style="overflow: hidden; display: inline-block; border: 1px solid rgb(136, 136, 136);">
            <div id="_head_wrap" style="width: 900px; overflow: hidden;">
              <table class="tbl1 scroll" id="_head" style="table-layout: fixed;">
                <thead>
                  <tr class="gridHeader">
                    <th height="22" style="width: 15px;" ><input type="checkbox" id="chkAllOtherSupplier" name="chkAllOtherSupplier" value="" checked="checked" /></th>
                    <th style="width: 161px;" ><span class="mainHeading4"><strong>Other Payable / Service Supplier Name</strong></span></th>
                    <th style="width: 80px;" ><strong>Other Payable / Service Supplier Code</strong></th>
                    <th style="width: 100px;" ><strong>Other Payable / Service Supplier Type</strong></th>
                    <th style="width: 165px;" >Ledger Accounts</th>
                    <th style="width: 9px;" ></th>
                    </tr>
                  </table>
              </div>
            <div class="normalfnt" id="_body_wrap" style="width: 900px; height: 350px; overflow: scroll;">
              <table class="tbl1 scroll" id="tblOtherSupMainGrid"> 
                <tbody>
                  <?php
	 	 		 $sql = "SELECT
						s.intId AS supId,
						s.strCode,
						s.strName,
						mst_typeofsupplier.intId,
						mst_typeofsupplier.strName AS type
						FROM
						mst_finance_service_supplier AS s
						left outer Join mst_typeofsupplier ON s.intTypeId = mst_typeofsupplier.intId
						WHERE
						s.intStatus =  '1'
						ORDER BY
						s.strName ASC";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
	  			 ?>
                  <tr class="normalfnt">
                  	 <td class="accActSelect" height="16" align="center" bgcolor="#FFFFFF">
        <input type="checkbox" name=chkSup<?php echo $row['supId'];?> id=chkSup<?php echo $row['supId'];?> checked="checked" class="checkOtherSupplier"/>
                     </td>
                    <td class="accActCusName" height="16" align="center" bgcolor="#FFFFFF">
                      	<a target="_blank" href="<?php echo "../../serviceSuppliers/addNew/supplier.php?id=$row[supId]"; ?>"><?php echo $row['strName'];?></a></td>
                    <td class="accActSupCode accActOtherSupCode"  align="center" bgcolor="#FFFFFF" id=<?php echo $row['supId'];?>><a target="_blank" href="<?php echo "../../serviceSuppliers/addNew/supplier.php?id=$row[supId]"; ?>"><?php echo $row['strCode'];?></a></td>
                    <td style="width: 100px;"  align="center" bgcolor="#FFFFFF"><?php echo $row['type'];?></td>
                    <td class="accActAcc" align="center" bgcolor="#FFFFFF">
                    <select name=cboOtherSupAcc<?php echo $row['supId'];?> id=cboOtherSupAcc<?php echo $row['supId'];?> style="width:100%" class="validate[required] otherSupAccount" >
                        <option value=""></option>
                        <?php  $sql2 = "SELECT
										mst_financechartofaccounts.intId,
										mst_financechartofaccounts.strCode,
										mst_financechartofaccounts.strName,
										mst_financechartofaccounts_companies.intChartOfAccountId,
										mst_financechartofaccounts_companies.intCompanyId
										FROM
										mst_financechartofaccounts
										Inner Join mst_financechartofaccounts_companies ON mst_financechartofaccounts_companies.intChartOfAccountId = mst_financechartofaccounts.intId
										WHERE
										intStatus = '1' AND  (intFinancialTypeId = '16' OR intFinancialTypeId = '17' OR intFinancialTypeId = '26' OR intFinancialTypeId = '28') AND strType = 'Posting' AND intCompanyId = '$companyId'
										order by strCode
						";
						$result2 = $db->RunQuery($sql2);
						while($row2=mysqli_fetch_array($result2))
						{
						   echo "<option value=\"".$row2['intId']."\">".$row2['strCode']."-".$row2['strName']."</option>";
						}
        			?>
                      </select></td>
                    </tr>
                  <?php 
        	    	} 
       		   	?>
                  </tbody>
                </table>
              </div></div>
          </td>
      </tr>
            </table>
                </td>
              </tr>
            </table>
          </td>
      </tr>
      <!---------------------------------------------------------------------------------------->
      <tr>
        <td>
          <table id="tblFour" width="100%" border="0" class="collaps">
            <tr>
            <td style="width:15px">
              <input type="checkbox" name="chkSupplierItem" id="chkSupplierItem"/></td>
              <td><span class="normalfntBlue supItmColor"><strong>Supplier Item</strong></span></td>
              </tr>
            <tr class="tblFourClsMainRow" style="display:none">
              <td colspan="2">
                <table width="100%" border="0">
      <tr>
        <td>
          <div id="_table_wrap" style="overflow: hidden; display: inline-block; border: 1px solid rgb(136, 136, 136);">
            <div id="_head_wrap" style="width: 900px; overflow: hidden;">
              <table class="tbl1 scroll" id="_head" style="table-layout: fixed;">
                <thead>
                  <tr class="gridHeader">
                    <th height="22" style="width: 15px;" ><input type="checkbox" id="chkAllSuppliersItem" name="chkAllSuppliersItem" value="" checked="checked" /></th>
                    <th style="width: 80px;" ><span class="mainHeading4"><strong>Supplier Item Name</strong></span></th>
                    <th style="width: 161px;" ><strong>Supplier Item Description</strong></th>
                    <th style="width: 165px;" >Ledger Accounts</th>
                    <th style="width: 7px;" ></th>
                    </tr>
                  </table>
              </div>
            <div class="normalfnt" id="_body_wrap" style="width: 900px; height: 350px; overflow: scroll;">
              <table class="tbl1 scroll" id="tblSupItmMainGrid"> 
                <tbody>
                  <?php
	 	 		 $sql = "SELECT
				 si.intId,
				 si.strName,
				 si.strRemark
				FROM mst_financesupplieritem si
				WHERE
				si.intStatus =  '1'
				ORDER BY
				si.strName ASC";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
	  			 ?>
                  <tr class="normalfnt">
                  	 <td class="accActSelect" height="16" align="center" bgcolor="#FFFFFF">
        <input type="checkbox" name=chkSupItem<?php echo $row['intId'];?> id=chkSupItem<?php echo $row['intId'];?> checked="checked" class="checkSupplierItem"/>
                     </td>
                 <td class="accActSupItmCode" height="16" align="center" bgcolor="#FFFFFF" id=<?php echo $row['intId'];?>>
                      	<a target="_blank" href="<?php echo "../../supplierItem/addNew/supplierItem.php?id=$row[intId]";?>"><?php echo $row['strName'];?></a></td>
                    <td class="accActCusName"  align="center" bgcolor="#FFFFFF">
                      <?php echo $row['strRemark'];?></td>
                    <td class="accActAcc" align="center" bgcolor="#FFFFFF"><span class="normalfntMid">
                      <select name=cboSupItemAcc<?php echo $row['intId'];?> id=cboSupItemAcc<?php echo $row['intId'];?> style="width:100%" class="validate[required] supItmAccount">
                        <option value=""></option>
                        <?php  $sql2 = "SELECT
										mst_financechartofaccounts.intId,
										mst_financechartofaccounts.strCode,
										mst_financechartofaccounts.strName,
										mst_financechartofaccounts_companies.intChartOfAccountId,
										mst_financechartofaccounts_companies.intCompanyId
										FROM
										mst_financechartofaccounts
										Inner Join mst_financechartofaccounts_companies ON mst_financechartofaccounts_companies.intChartOfAccountId = mst_financechartofaccounts.intId
										WHERE
																intStatus = '1' AND strType = 'Posting' AND intFinancialTypeId <> '10' AND intFinancialTypeId <> '18' AND intCompanyId = '$companyId'
										order by strCode
						"; 
						$result2 = $db->RunQuery($sql2);
						while($row2=mysqli_fetch_array($result2))
						{
							echo "<option value=\"".$row2['intId']."\">".$row2['strCode']."-".$row2['strName']."</option>";
						}
        ?>
                      </select>
                    </span></td>
                    </tr>
                  <?php 
        	    	} 
       		   	?>
                  </tbody>
                </table>
              </div></div>
          </td>
      </tr>
            </table>
                </td>
              </tr>
            </table>
          </td>
      </tr>
      <tr>
        <td>
          <table id="tblFive" width="100%" border="0" class="collaps">
            <tr>
            <td style="width:15px">
              <input type="checkbox" name="chkTax" id="chkTax"/></td>
              <td><span class="normalfntBlue taxColor"><strong>Tax</strong></span></td>
              </tr>
            <tr class="tblFiveClsMainRow" style="display:none">
              <td colspan="2">
                <table width="100%" border="0">
      <tr>
        <td>
          <div id="_table_wrap" style="overflow: hidden; display: inline-block; border: 1px solid rgb(136, 136, 136);">
            <div id="_head_wrap" style="width: 900px; overflow: hidden;">
              <table class="tbl1 scroll" id="_head" style="table-layout: fixed;">
                <thead>
                  <tr class="gridHeader">
                    <th height="22" style="width: 15px;" ><input type="checkbox" id="chkAllTax" name="chkAllTax" value="" checked="checked" /></th>
                    <th style="width: 80px;" ><span class="mainHeading4"><strong>Tax Code</strong></span></th>
                    <th style="width: 161px;" ><strong>Tax Description</strong></th>
                    <th style="width: 165px;" >Ledger Accounts</th>
                    <th style="width: 7px;" ></th>
                    </tr>
                  </table>
              </div>
            <div class="normalfnt" id="_body_wrap" style="width: 900px; height: 350px; overflow: scroll;">
              <table class="tbl1 scroll" id="tblTaxMainGrid"> 
                <tbody>
                  <?php
	 	 		 $sql = "SELECT
				 ti.intId,
				 ti.strCode,
				 ti.strDescription
				FROM mst_financetaxisolated ti
				WHERE
				ti.intStatus =  '1'
				ORDER BY
				ti.strCode ASC";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
	  			 ?>
                  <tr class="normalfnt">
                  	 <td class="accActSelect" height="16" align="center" bgcolor="#FFFFFF">
        <input type="checkbox" name=chkTax<?php echo $row['intId'];?> id=chkTax<?php echo $row['intId'];?> checked="checked" class = "checkTax"/>
                     </td>
                    <td class="accActTaxCode" height="16" align="center" bgcolor="#FFFFFF" id=<?php echo $row['intId'];?>>
                      	<a target="_blank" href="<?php echo "../../tax/Isolated/taxIsolated.php?id=$row[intId]";?>"><?php echo $row['strCode'];?></a></td>
                    <td class="accActCusName"  align="center" bgcolor="#FFFFFF">
                      <?php echo $row['strDescription'];?></td>
                    <td class="accActAcc" align="center" bgcolor="#FFFFFF"><span class="normalfntMid">
                      <select name=cboTaxAcc<?php echo $row['intId'];?> id=cboTaxAcc<?php echo $row['intId'];?> style="width:100%" class="validate[required] taxAccount">
                        <option value=""></option>
                        <?php  $sql2 = "SELECT
										mst_financechartofaccounts.intId,
										mst_financechartofaccounts.strCode,
										mst_financechartofaccounts.strName,
										mst_financechartofaccounts_companies.intChartOfAccountId,
										mst_financechartofaccounts_companies.intCompanyId
										FROM
										mst_financechartofaccounts
										Inner Join mst_financechartofaccounts_companies ON mst_financechartofaccounts_companies.intChartOfAccountId = mst_financechartofaccounts.intId
										WHERE
										intStatus = '1' AND  (intFinancialTypeId = '7' OR intFinancialTypeId = '28' OR intFinancialTypeId = '6' OR intFinancialTypeId = '12') AND strType = 'Posting' AND intCompanyId = '$companyId'
										order by strCode
						";
						$result2 = $db->RunQuery($sql2);
						while($row2=mysqli_fetch_array($result2))
						{
						   echo "<option value=\"".$row2['intId']."\">".$row2['strCode']."-".$row2['strName']."</option>";
						}
        ?>
                      </select>
                    </span></td>
                    </tr>
                  <?php 
        	    	} 
       		   	?>
                  </tbody>
                </table>
              </div></div>
          </td>
      </tr>
            </table>
                </td>
              </tr>
            </table>
          </td>
      </tr>
      <tr>
        <td>
          <table id="tblSix" width="100%" border="0" class="collaps">
            <tr>
            <td style="width:15px">
              <input type="checkbox" name="chkCurrency" id="chkCurrency"/></td>
              <td><span class="normalfntBlue curColor"><strong>Currency</strong></span></td>
              </tr>
            <tr class="tblSixClsMainRow" style="display:none">
              <td colspan="2">
                <table width="100%" border="0">
      <tr>
        <td>
          <div id="_table_wrap" style="overflow: hidden; display: inline-block; border: 1px solid rgb(136, 136, 136);">
            <div id="_head_wrap" style="width: 900px; overflow: hidden;">
              <table class="tbl1 scroll" id="_head" style="table-layout: fixed;">
                <thead>
                  <tr class="gridHeader">
                    <th height="22" style="width: 15px;" ><input type="checkbox" id="chkAllCurrency" name="chkAllCurrency" value="" checked="checked" /></th>
                    <th style="width: 101px;" ><span class="mainHeading4"><strong>Currency ISO Code</strong></span></th>
                    <th style="width: 100px;" ><strong>Currency Symbol</strong></th>
                    <th style="width: 155px;" >Realize Gain</th>
                    <th style="width: 155px;" >Realize Lost</th>
                    <th style="width: 155px;" >Unrealize Gain</th>
                    <th style="width: 155px;" >Unrealize Lost</th>
                    <th style="width: 15px;" ></th>
                    </tr>
                  </table>
              </div>
            <div class="normalfnt" id="_body_wrap" style="width: 900px; height: 350px; overflow: scroll;">
              <table class="tbl1 scroll" id="tblCurrencyMainGrid"> 
                <tbody>
                  <?php
	 	 		 $sql = "SELECT
				 cur.intId,
				 cur.strCode,
				 cur.strSymbol
				FROM mst_financecurrency cur
				WHERE
				cur.intStatus =  '1'
				ORDER BY
				cur.strCode ASC";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
	  			 ?>
                  <tr class="normalfnt">
                  	 <td class="accActSelect" height="16" align="center" bgcolor="#FFFFFF">
        <input type="checkbox" name=chkCurrency<?php echo $row['intId'];?> id=chkCurrency<?php echo $row['intId'];?> checked="checked" class="checkCurrency"/>
                     </td>
               <td class="accActCurrencyCode" height="16" align="center" bgcolor="#FFFFFF" id=<?php echo $row['intId'];?>>
                      	<a target="_blank" href="<?php echo "../../currency/addNew/currency.php?id=$row[intId]";?>"><?php echo $row['strCode'];?></a></td>
                    <td class="accActCurrencySymbol"  align="center" bgcolor="#FFFFFF">
                      <a target="_blank" href="<?php echo "../../currency/addNew/currency.php?id=$row[intId]";?>"><?php echo $row['strSymbol'];?></a></td>
                    <td class="accActCurrencyAcc" align="center" bgcolor="#FFFFFF"><span class="normalfntMid">
                      <select name=cboRealizeGainAcc<?php echo $row['intId'];?> id=cboRealizeGainAcc<?php echo $row['intId'];?> style="width:100%" class="validate[required] realizeGain">
                        <option value=""></option>
                        <?php  $sql2 = "SELECT
										mst_financechartofaccounts.intId,
										mst_financechartofaccounts.strCode,
										mst_financechartofaccounts.strName,
										mst_financechartofaccounts_companies.intChartOfAccountId,
										mst_financechartofaccounts_companies.intCompanyId
										FROM
										mst_financechartofaccounts
										Inner Join mst_financechartofaccounts_companies ON mst_financechartofaccounts_companies.intChartOfAccountId = mst_financechartofaccounts.intId
										WHERE
										intStatus = '1' AND  (intFinancialTypeId = '27' OR intFinancialTypeId = '4') AND strType = 'Posting' AND intCompanyId = '$companyId'
										order by strCode
						";
						$result2 = $db->RunQuery($sql2);
						while($row2=mysqli_fetch_array($result2))
						{
						   echo "<option value=\"".$row2['intId']."\">".$row2['strCode']."-".$row2['strName']."</option>";
						}
        ?>
                      </select>
                    </span></td>
                     <td class="accActCurrencyAcc" align="center" bgcolor="#FFFFFF"><span class="normalfntMid">
                       <select name="cboRealizeLostAcc<?php echo $row['intId'];?>" id="cboRealizeLostAcc<?php echo $row['intId'];?>" style="width:100%" class="validate[required] realizeLost">
                         <option value=""></option>
                         <?php  $sql2 = "SELECT
										mst_financechartofaccounts.intId,
										mst_financechartofaccounts.strCode,
										mst_financechartofaccounts.strName,
										mst_financechartofaccounts_companies.intChartOfAccountId,
										mst_financechartofaccounts_companies.intCompanyId
										FROM
										mst_financechartofaccounts
										Inner Join mst_financechartofaccounts_companies ON mst_financechartofaccounts_companies.intChartOfAccountId = mst_financechartofaccounts.intId
										WHERE
										intStatus = '1' AND  (intFinancialTypeId = '29' OR intFinancialTypeId = '4') AND strType = 'Posting' AND intCompanyId = '$companyId'
										order by strCode
						";
						$result2 = $db->RunQuery($sql2);
						while($row2=mysqli_fetch_array($result2))
						{
						   echo "<option value=\"".$row2['intId']."\">".$row2['strCode']."-".$row2['strName']."</option>";
						}
        ?>
                       </select>
                     </span></td>
                     <td class="accActCurrencyAcc" align="center" bgcolor="#FFFFFF"><span class="normalfntMid">
                       <select name="cboUnrealizeGainAcc<?php echo $row['intId'];?>" id="cboUnrealizeGainAcc<?php echo $row['intId'];?>" style="width:100%" class="validate[required] unrealizeGain">
                         <option value=""></option>
                         <?php  $sql2 = "SELECT
										mst_financechartofaccounts.intId,
										mst_financechartofaccounts.strCode,
										mst_financechartofaccounts.strName,
										mst_financechartofaccounts_companies.intChartOfAccountId,
										mst_financechartofaccounts_companies.intCompanyId
										FROM
										mst_financechartofaccounts
										Inner Join mst_financechartofaccounts_companies ON mst_financechartofaccounts_companies.intChartOfAccountId = mst_financechartofaccounts.intId
										WHERE
										intStatus = '1' AND  (intFinancialTypeId = '30' OR intFinancialTypeId = '4') AND strType = 'Posting' AND intCompanyId = '$companyId'
										order by strCode
						";
						$result2 = $db->RunQuery($sql2);
						while($row2=mysqli_fetch_array($result2))
						{
						   echo "<option value=\"".$row2['intId']."\">".$row2['strCode']."-".$row2['strName']."</option>";
						}
        ?>
                       </select>
                     </span></td>
                     <td class="accActCurrencyAcc" align="center" bgcolor="#FFFFFF"><span class="normalfntMid">
                       <select name="cboUnrealizeLostAcc<?php echo $row['intId'];?>" id="cboUnrealizeLostAcc<?php echo $row['intId'];?>" style="width:100%" class="validate[required] unrealizeLost">
                         <option value=""></option>
                         <?php  $sql2 = "SELECT
										mst_financechartofaccounts.intId,
										mst_financechartofaccounts.strCode,
										mst_financechartofaccounts.strName,
										mst_financechartofaccounts_companies.intChartOfAccountId,
										mst_financechartofaccounts_companies.intCompanyId
										FROM
										mst_financechartofaccounts
										Inner Join mst_financechartofaccounts_companies ON mst_financechartofaccounts_companies.intChartOfAccountId = mst_financechartofaccounts.intId
										WHERE
										intStatus = '1' AND  (intFinancialTypeId = '31' OR intFinancialTypeId = '4') AND strType = 'Posting' AND intCompanyId = '$companyId'
										order by strCode
						";
						$result2 = $db->RunQuery($sql2);
						while($row2=mysqli_fetch_array($result2))
						{
						   echo "<option value=\"".$row2['intId']."\">".$row2['strCode']."-".$row2['strName']."</option>";
						}
        ?>
                       </select>
                     </span></td>
                    </tr>
                  <?php 
        	    	} 
       		   	?>
                  </tbody>
                </table>
              </div></div>
          </td>
      </tr>
            </table>
                </td>
              </tr>
            </table>
          </td>
      </tr>
    
<!--    //=============================================================================//-->
      <tr>
        <td>
          <table id="tblSeven" width="100%" border="0" class="collaps">
            <tr>
            <td style="width:15px">
              <input type="checkbox" name="chkServiceProvider" id="chkServiceProvider"/></td>
              <td><span class="normalfntBlue serColor"><strong>Service Provider</strong></span></td>
              </tr>
            <tr class="tblSevenClsMainRow" style="display:none">
              <td colspan="2">
                <table width="100%" border="0">
      <tr>
        <td>
          <div id="_table_wrap" style="overflow: hidden; display: inline-block; border: 1px solid rgb(136, 136, 136);">
            <div id="_head_wrap" style="width: 900px; overflow: hidden;">
              <table class="tbl1 scroll" id="_head" style="table-layout: fixed;">
                <thead>
                  <tr class="gridHeader">
                    <th height="22" style="width:15px;" ><input type="checkbox" id="chkAllServiceProvider" name="chkAllServiceProvider" value="" checked="checked" /></th>
                    <th style="width: 200px;" ><span class="mainHeading4"><strong>Service Provider</strong></span></th>
                    <!--<th style="width: 161px;" ><strong>Currency Symbol</strong></th>-->
                    <th style="width: 206px;" >Ledger Accounts</th>
                    <th style="width: 7px;" ></th>
                    </tr>
                  </table>
              </div>
            <div class="normalfnt" id="_body_wrap" style="width: 900px; height: 350px; overflow: scroll;">
              <table class="tbl1 scroll" id="tblServiceProMainGrid"> 
                <tbody>
                  <?php
	 	 		 $sql = "SELECT
							mst_financeserviceprovider.intProviderId,
							mst_financeserviceprovider.strProviderName
							FROM
							mst_financeserviceprovider
							ORDER BY
							mst_financeserviceprovider.strProviderName ASC";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
	  			 ?>
                  <tr class="normalfnt">
                  	 <td class="accActSelect" height="16" align="center" bgcolor="#FFFFFF">
        <input type="checkbox" name=chkServicePro<?php echo $row['intProviderId'];?> id=chkServicePro<?php echo $row['intProviderId'];?> checked="checked" style="width: 32px;" class="checkServProvider"/>
                     </td>
               <td class="accActServProv" height="16" width = "417" align="center" bgcolor="#FFFFFF" id=<?php echo $row['intProviderId'];?>>
                      	<a target="_blank" href="<?php echo "../../../billManager/serviceProvider/addNew/serviceProvider.php?id=$row[intProviderId]"; ?>"><?php echo $row['strProviderName'];?></a></td>
                 
                    <td class="accActServProvAcc" align="center" bgcolor="#FFFFFF">
                      <select name=cboServProvAcc<?php echo $row['intProviderId'];?> id=cboServProvAcc<?php echo $row['intProviderId'];?> style="width:100%" class="validate[required] ServiceProAccount" >
                        <option value=""></option>
                        <?php  $sql2 = "SELECT
											mst_financechartofaccounts.intId,
											mst_financechartofaccounts.strCode,
											mst_financechartofaccounts.strName,
											mst_financechartofaccounts_companies.intCompanyId
										FROM
											mst_financechartofaccounts
											Inner Join mst_financechartofaccounts_companies ON mst_financechartofaccounts_companies.intChartOfAccountId = mst_financechartofaccounts.intId
										WHERE
											intStatus = '1' AND intFinancialTypeId = '18'  AND strType = 'Posting' AND intCompanyId = '$companyId'
											order by strCode";
						$result2 = $db->RunQuery($sql2);
						while($row2=mysqli_fetch_array($result2))
						{
						   echo "<option value=\"".$row2['intId']."\">".$row2['strCode']."-".$row2['strName']."</option>";
						}
        			?>
                      </select></td>
                    </tr>
                  <?php 
        	    	} 
       		   	?>
                  </tbody>
                </table>
              </div></div>
          </td>
      </tr>
            </table>
                </td>
              </tr>
            </table>
          </td>
      </tr>
      
      <tr>
        <td width="100%" align="center" bgcolor=""><img style="display:none" border="0" src="../../../../../images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:none" border="0" src="../../../../../images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/><img style="display:none" border="0" src="../../../../../images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="25"/><a href="../../../../../main.php"><img  src="../../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
      </tr>
          </table>
		  </td>
    </tr>
  </table>

  </div>
  </div>
</form>
</body>
</html>
<?php
//ob_end_flush();
$content = ob_get_clean();
echo $content; 
?>
<?php
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$companyId = $row['intCompanyId']; 
	}
	
	/////////// customer activate load part /////////////////////
	$values = json_decode($_REQUEST['myJson'], true);
	
	if($requestType=='loadDetails')
	{
		$arr;
		$loadCus  		= $_REQUEST['loadCus'];
		$loadOtherCus  	= $_REQUEST['loadOtherCus'];
		$loadCusItm  	= $_REQUEST['loadCusItm'];
		$loadSup  		= $_REQUEST['loadSup'];
		$loadOtherSup  	= $_REQUEST['loadOtherSup'];
		$loadSupItm  	= $_REQUEST['loadSupItm'];
		$loadTax  		= $_REQUEST['loadTax'];
		$loadCurrency   = $_REQUEST['loadCur'];
		$loadServicePro = $_REQUEST['loadServicePro'];      

			 if($loadCus == 'customer')
			 {
					$sql = "SELECT intCustomerId,
									intCompanyId,
									intChartOfAccountId
					FROM mst_financecustomeractivate
					WHERE (`intCompanyId`='$companyId')";
						$result = $db->RunQuery($sql);
						
					while($row=mysqli_fetch_array($result))
					{	
						$response['customerId'] = $row['intCustomerId'];
						$response['customerAccId'] = $row['intChartOfAccountId'];
						$response['loadStatus'] = $loadCus;
						$arr[] = $response;
					}
			}
			//----------------------------------------------------------
			if($loadOtherCus == 'otherCustomer')
			 {
					$sql = "SELECT intCustomerId,
									intCompanyId,
									intChartOfAccountId
					FROM mst_finance_service_customer_activate
					WHERE (`intCompanyId`='$companyId')";
						$result = $db->RunQuery($sql);
						
					while($row=mysqli_fetch_array($result))
					{	
						$response['otherCustomerId'] = $row['intCustomerId'];
						$response['otherCustomerAccId'] = $row['intChartOfAccountId'];
						$response['loadStatus'] = $loadOtherCus;
						$arr[] = $response;
					}
			}
			//----------------------------------------------------------
			if($loadCusItm == 'customerItem')      
			{
					$sql = "SELECT intCustomerItemId,
								   intCompanyId,
								   intChartOfAccountId
					FROM mst_financecustomeritemactivate
					WHERE (`intCompanyId`='$companyId')";
						$result = $db->RunQuery($sql);
						
					while($row=mysqli_fetch_array($result))
					{	
						$response['customerItmId'] = $row['intCustomerItemId'];
						$response['customerItmAccId'] = $row['intChartOfAccountId'];
						$response['loadStatus'] = $loadCusItm;
						$arr[] = $response;
					}
			}
			if($loadSup == 'supplier')
			{
					$sql = "SELECT intSupplierId,
								   intCompanyId,
								   intChartOfAccountId
					FROM mst_financesupplieractivate
					WHERE (`intCompanyId`='$companyId')";
						$result = $db->RunQuery($sql);
						
					while($row=mysqli_fetch_array($result))
					{	
						$response['supplierId'] = $row['intSupplierId'];
						$response['supplierAccId'] = $row['intChartOfAccountId'];
						$response['loadStatus'] = $loadSup;
						$arr[] = $response;
					}
			}
			//----------------------------------------------------------
			if($loadOtherSup == 'otherSupplier')
			{
					$sql = "SELECT intSupplierId,
								   intCompanyId,
								   intChartOfAccountId
					FROM mst_finance_service_supplier_activate
					WHERE (`intCompanyId`='$companyId')";
						$result = $db->RunQuery($sql);
						
					while($row=mysqli_fetch_array($result))
					{	
						$response['otherSupplierId'] = $row['intSupplierId'];
						$response['otherSupplierAccId'] = $row['intChartOfAccountId'];
						$response['loadStatus'] = $loadOtherSup;
						$arr[] = $response;
					}
			}
			//----------------------------------------------------------
			if($loadSupItm == 'supplierItem')
			{
					$sql = "SELECT intSupplierItemId,
								   intCompanyId,
								   intChartOfAccountId
					FROM mst_financesupplieritemactivate
					WHERE (`intCompanyId`='$companyId')";
						$result = $db->RunQuery($sql);
						
					while($row=mysqli_fetch_array($result))
					{	
						$response['supplierItmId'] = $row['intSupplierItemId'];
						$response['supplierItmAccId'] = $row['intChartOfAccountId'];
						$response['loadStatus'] = $loadSupItm;
						$arr[] = $response;
					}
			}
			if($loadTax == 'tax')
			{
					$sql = "SELECT intTaxId,
								   intCompanyId,
								   intChartOfAccountId
					FROM mst_financetaxactivate
					WHERE (`intCompanyId`='$companyId')";
						$result = $db->RunQuery($sql);
						
					while($row=mysqli_fetch_array($result))
					{	
						$response['taxId'] = $row['intTaxId'];
						$response['taxAccId'] = $row['intChartOfAccountId'];
						$response['loadStatus'] = $loadTax;
						$arr[] = $response;
					}
			}
			if($loadCurrency == 'currency')
			{
					$sql = "SELECT intCurrencyId,
								   intCompanyId,
								   intRealizeGainId,
								   intRealizeLostId,
								   intUnrealizeGainId,
								   intUnrealizeLostId
					FROM mst_financecurrencyactivate
					WHERE (`intCompanyId`='$companyId')";
						$result = $db->RunQuery($sql);
						
					while($row=mysqli_fetch_array($result))
					{	
						$response['currencyId']		= $row['intCurrencyId'];
						$response['realizeGain'] 	= $row['intRealizeGainId'];
						$response['realizeLost']	= $row['intRealizeLostId'];
						$response['unrealizeGain'] = $row['intUnrealizeGainId'];
						$response['unrealizeLost'] 	= $row['intUnrealizeLostId'];
						$response['loadStatus'] 	= $loadCurrency;
						$arr[] = $response;
					}
			}
			//--------------------------------------------------------------
			 if($loadServicePro == 'serviceProvider')  
			 {
					$sql = "SELECT
								mst_financeserviceprovideractivate.intServiceProviderId,
								mst_financeserviceprovideractivate.intCompanyId,
								mst_financeserviceprovideractivate.intChartOfAccountId
							FROM
								mst_financeserviceprovideractivate
							WHERE
								mst_financeserviceprovideractivate.intCompanyId =  '$companyId'";
						$result = $db->RunQuery($sql);
						
					while($row=mysqli_fetch_array($result))
					{	
						$response['serviceProviderId'] = $row['intServiceProviderId'];
						$response['serviceProviderAccId'] = $row['intChartOfAccountId'];
						$response['loadStatus'] = $loadServicePro;
						$arr[] = $response;
					}
			}
		echo json_encode($arr);
	}
	
?>
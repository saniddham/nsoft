var value = "";
var customerArr = "";
var otherCustomerArr
var customerItemArr = "";
var supplierArr = "";
var otherSupplierArr = "";
var supplierItemArr = "";
var taxArr = "";
var currencyArr = "";
var serviceProArr = "";
var loadCus = "";
var loadOtherCus = "";
var loadCusItm = "";	
var loadSup = "";	
var loadOtherSup = "";
var loadSupItm = "";	
var loadTax = "";	
var loadCur = "";
var loadServicePro	= "";
$(document).ready(function() {
	callDataHideRow();
  		//$("#frmChartOfAccountsActivate").validationEngine();
  //permision for add 
  if(intAddx)
  {
 	$('#frmChartOfAccountsActivate #butNew').show();
	$('#frmChartOfAccountsActivate #butSave').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmChartOfAccountsActivate #butSave').show();
  }
  
  //permision for view
  if(intViewx)
  {
	//
  }
$('#chkCustomer').click(function() {
	if($(this).attr('checked'))
	{
		$('.tblOneClsMainRow').show();
  		$('.cusColor').css('color', 'red');
		loadCus = 'customer';
		loadCombo_frmChartOfAccountsActivate();
	}
	else
	{
		$('.cusColor').css('color', '#0B3960');
		$('.tblOneClsMainRow').hide();
	}
});
//--------------------------------------------------
	$('#chkOtherCustomer').click(function() {
	if($(this).attr('checked'))
	{
		$('.tblOneOneClsMainRow').show();
		$('.otherCusColor').css('color', 'red');
		loadOtherCus = 'otherCustomer';
		loadCombo_frmChartOfAccountsActivate();
	}
	else
	{
		$('.otherCusColor').css('color', '#0B3960');
		$('.tblOneOneClsMainRow').hide();
	}
});
//--------------------------------------------------
$('#chkCustomerItem').click(function() {
	if($(this).attr('checked'))
	{
		$('.tblTwoClsMainRow').show();
		$('.cusItmColor').css('color', 'red');
		loadCusItm = 'customerItem';
		loadCombo_frmChartOfAccountsActivate();
	}
	else
	{
		$('.cusItmColor').css('color', '#0B3960');
		$('.tblTwoClsMainRow').hide();
	}
});
$('#chkSupplier').click(function() {
	if($(this).attr('checked'))
	{
		$('.tblThreeClsMainRow').show();
		$('.supColor').css('color', 'red');
		loadSup = 'supplier';
		loadCombo_frmChartOfAccountsActivate();
	}
	else
	{
		$('.supColor').css('color', '#0B3960');
		$('.tblThreeClsMainRow').hide();
	}
});
//--------------------------------------------------
	$('#chkOtherSupplier').click(function() {
	if($(this).attr('checked'))
	{
		$('.tblThreeOneClsMainRow').show();
		$('.otherSupColor').css('color', 'red');
		loadOtherSup = 'otherSupplier';
		loadCombo_frmChartOfAccountsActivate();
	}
	else
	{
		$('.otherSupColor').css('color', '#0B3960');
		$('.tblThreeOneClsMainRow').hide();
	}
});
//--------------------------------------------------
$('#chkSupplierItem').click(function() {
	if($(this).attr('checked'))
	{
		$('.tblFourClsMainRow').show();
		$('.supItmColor').css('color', 'red');
		loadSupItm = 'supplierItem';
		loadCombo_frmChartOfAccountsActivate();
	}
	else
	{
		$('.supItmColor').css('color', '#0B3960');
		$('.tblFourClsMainRow').hide();
	}
});
$('#chkTax').click(function() {
	if($(this).attr('checked'))
	{
		$('.tblFiveClsMainRow').show();
		$('.taxColor').css('color', 'red');
		loadTax = 'tax';
		loadCombo_frmChartOfAccountsActivate();
	}
	else
	{
		$('.taxColor').css('color', '#0B3960');
		$('.tblFiveClsMainRow').hide();
	}
});
$('#chkCurrency').click(function() {
	if($(this).attr('checked'))
	{
		$('.tblSixClsMainRow').show();
		$('.curColor').css('color', 'red');
		loadCur = 'currency';
		loadCombo_frmChartOfAccountsActivate();
	}
	else
	{
		$('.curColor').css('color', '#0B3960');
		$('.tblSixClsMainRow').hide();
	}
});

//----------Service Provider-----------------
$('#chkServiceProvider').click(function() {
	if($(this).attr('checked'))
	{
		$('.tblSevenClsMainRow').show();
  		$('.serColor').css('color', 'red');
		loadServicePro = 'serviceProvider';
		loadCombo_frmChartOfAccountsActivate();
	}
	else
	{
		$('.serColor').css('color', '#0B3960');
		$('.tblSevenClsMainRow').hide();
	}
});
//----------------------------------
  ///save button click event
  $('#frmChartOfAccountsActivate #butSave').click(function(){
	if($('#chkCustomer').attr('checked'))
	{
		customerArr="[ ";
		$('#tblCusMainGrid tr').each(function() {
			var customer = "";
			var deleteStatus = "";
			var insertStatus = "customer";
			if($(this).find(".checkCustomer").attr('checked'))
			{
				deleteStatus 	= 'false';
				customer 		= $(this).find(".accActCusCode").attr('id');
				var cusAccount	= $(this).find(".cusAccount").val();
				
				$(this).find(".cusAccount").addClass('validate[required]');
			}
			else
			{
				deleteStatus 	= 'true';
				customer 		= $(this).find(".accActCusCode").attr('id');
				
				$(this).find(".cusAccount").removeClass('validate[required]');
			}
		customerArr += '{ "cusId":"'+customer+'", "cusAccId": "'+cusAccount+'", "insertStatus": "'+insertStatus+'", "delStatus": "'+deleteStatus+'"},';	
		});
		customerArr = customerArr.substr(0,customerArr.length-1);
		customerArr += " ]";
		
		loadCus = 'customer';
	}
	//-------------------------------------------------------------------------------------------------------
	if($('#chkOtherCustomer').attr('checked'))
	{
		otherCustomerArr="[ ";
		$('#tblOtherCusMainGrid tr').each(function() {
			var customer = "";
			var deleteStatus = "";
			var insertStatus = "otherCustomer";
			if($(this).find(".checkOtherCustomer").attr('checked'))
			{
				deleteStatus 	= 'false';
				customer 		= $(this).find(".accActOtherCusCode").attr('id');
				var cusAccount	= $(this).find(".otherCusAccount").val();
				
				$(this).find(".cusAccount").addClass('validate[required]');
			}
			else
			{
				deleteStatus 	= 'true';
				customer 		= $(this).find(".accActOtherCusCode").attr('id');
				
				$(this).find(".otherCusAccount").removeClass('validate[required]');
			}
		otherCustomerArr += '{ "cusId":"'+customer+'", "cusAccId": "'+cusAccount+'", "insertStatus": "'+insertStatus+'", "delStatus": "'+deleteStatus+'"},';	
		});
		otherCustomerArr = otherCustomerArr.substr(0,otherCustomerArr.length-1);
		otherCustomerArr += " ]";
		loadOtherCus = 'otherCustomer';
	}
	//-------------------------------------------------------------------------------------------------------
	if($('#chkCustomerItem').attr('checked'))
	{
		customerItemArr="[ ";
		$('#tblCusItmMainGrid tr').each(function() {
			var customerItem = "";
			var deleteStatus = "";
			var insertStatus = "customerItem";
			if($(this).find(".checkCustomerItem").attr('checked'))
			{
				deleteStatus 	= 'false';
				customerItem 		= $(this).find(".accActCusItmCode").attr('id');
				var cusItmAccount	= $(this).find(".cusItemAccount").val();
				
				$(this).find(".cusItemAccount").addClass('validate[required]');
			}
			else
			{
				deleteStatus 	= 'true';
				customerItem 		= $(this).find(".accActCusItmCode").attr('id');
				
				$(this).find(".cusItemAccount").removeClass('validate[required]');
			}
		customerItemArr += '{ "cusItmId":"'+customerItem+'", "cusItmAccId": "'+cusItmAccount+'", "insertStatus": "'+insertStatus+'", "delStatus": "'+deleteStatus+'"},';	
		});
		customerItemArr = customerItemArr.substr(0,customerItemArr.length-1);
		customerItemArr += " ]";
		
		loadCusItm = 'customerItem';
	}
	if($('#chkSupplier').attr('checked'))
	{
		supplierArr="[ ";
		$('#tblSupMainGrid tr').each(function() {
			var supplier = "";
			var deleteStatus = "";
			var insertStatus = "supplier";
			if($(this).find(".checkSupplier").attr('checked'))
			{
				deleteStatus 	= 'false';
				supplier 		= $(this).find(".accActSupCode").attr('id');
				var supplierAccount	= $(this).find(".supAccount").val();
				
				$(this).find(".supAccount").addClass('validate[required]');
			}
			else
			{
				deleteStatus 	= 'true';
				supplier 		= $(this).find(".accActSupCode").attr('id');
				
				$(this).find(".supAccount").removeClass('validate[required]');
			}
		supplierArr += '{ "supId":"'+supplier+'", "supAccId": "'+supplierAccount+'", "insertStatus": "'+insertStatus+'", "delStatus": "'+deleteStatus+'"},';	
		});
		supplierArr = supplierArr.substr(0,supplierArr.length-1);
		supplierArr += " ]";
		
		loadSup = 'supplier';
	}
	
	//-------------------------------------------------------------------------------------------------------
		if($('#chkOtherSupplier').attr('checked'))
		{
			otherSupplierArr="[ ";
			$('#tblOtherSupMainGrid tr').each(function() {
				var supplier = "";
				var deleteStatus = "";
				var insertStatus = "otherSupplier";
				if($(this).find(".checkOtherSupplier").attr('checked'))
				{
					deleteStatus 	= 'false';
					supplier 		= $(this).find(".accActOtherSupCode").attr('id');
					var supplierAccount	= $(this).find(".otherSupAccount").val();
					
					$(this).find(".otherSupAccount").addClass('validate[required]');
				}
				else
				{
					deleteStatus 	= 'true';
					supplier 		= $(this).find(".accActSupCode").attr('id');
					
					$(this).find(".otherSupAccount").removeClass('validate[required]');
				}
			otherSupplierArr += '{ "supId":"'+supplier+'", "supAccId": "'+supplierAccount+'", "insertStatus": "'+insertStatus+'", "delStatus": "'+deleteStatus+'"},';	
			});
			otherSupplierArr = otherSupplierArr.substr(0,otherSupplierArr.length-1);
			otherSupplierArr += " ]";
			
			loadOtherSup = 'otherSupplier';
		}
	//--------------------------------------------------------------------------------------------------------
	if($('#chkSupplierItem').attr('checked'))
	{
		supplierItemArr="[ ";
		$('#tblSupItmMainGrid tr').each(function() {
			var supplierItem = "";
			var deleteStatus = "";
			var insertStatus = "supplierItem";
			if($(this).find(".checkSupplierItem").attr('checked'))
			{
				deleteStatus 	= 'false';
				supplierItem 		= $(this).find(".accActSupItmCode").attr('id');
				var supplierItemAccount	= $(this).find(".supItmAccount").val();
				
				$(this).find(".supItmAccount").addClass('validate[required]');
			}
			else
			{
				deleteStatus 	= 'true';
				supplierItem 		= $(this).find(".accActSupItmCode").attr('id');
				
				$(this).find(".supItmAccount").removeClass('validate[required]');
			}
		supplierItemArr += '{ "supItmId":"'+supplierItem+'", "supItmAccId": "'+supplierItemAccount+'", "insertStatus": "'+insertStatus+'", "delStatus": "'+deleteStatus+'"},';	
		});
		supplierItemArr = supplierItemArr.substr(0,supplierItemArr.length-1);
		supplierItemArr += " ]";
		
		loadSupItm = 'supplierItem';
	}
	if($('#chkTax').attr('checked'))
	{
		taxArr="[ ";
		$('#tblTaxMainGrid tr').each(function() {
			var tax = "";
			var deleteStatus = "";
			var insertStatus = "tax";
			if($(this).find(".checkTax").attr('checked'))
			{
				deleteStatus 	= 'false';
				tax 		= $(this).find(".accActTaxCode").attr('id');
				var taxAccount	= $(this).find(".taxAccount").val();
				
				$(this).find(".taxAccount").addClass('validate[required]');
			}
			else
			{
				deleteStatus 	= 'true';
				tax 		= $(this).find(".accActTaxCode").attr('id');
				
				$(this).find(".taxAccount").removeClass('validate[required]');
			}
		taxArr += '{ "taxId":"'+tax+'", "taxAccId": "'+taxAccount+'", "insertStatus": "'+insertStatus+'", "delStatus": "'+deleteStatus+'"},';	
		});
		taxArr = taxArr.substr(0,taxArr.length-1);
		taxArr += " ]";
		
		loadTax = 'tax';
	}
	if($('#chkCurrency').attr('checked'))
	{
		currencyArr="[ ";
		$('#tblCurrencyMainGrid tr').each(function() {
			var currency = "";
			var deleteStatus = "";
			var insertStatus = "currency";
			if($(this).find(".checkCurrency").attr('checked'))
			{
				deleteStatus 		= 'false';
				currency 			= $(this).find(".accActCurrencyCode").attr('id');
				var realizeGain		= $(this).find(".realizeGain").val();
				var realizeLost		= $(this).find(".realizeLost").val();
				var unrealizeGain	= $(this).find(".unrealizeGain").val();
				var unrealizeLost	= $(this).find(".unrealizeLost").val();
				
				$(this).find(".realizeGain").addClass('validate[required]');
				$(this).find(".realizeLost").addClass('validate[required]');
				$(this).find(".unrealizeGain").addClass('validate[required]');
				$(this).find(".unrealizeLost").addClass('validate[required]');
			}
			else
			{
				deleteStatus 	= 'true';
				currency 		= $(this).find(".accActCurrencyCode").attr('id');
				
				$(this).find(".realizeGain").removeClass('validate[required]');
				$(this).find(".realizeLost").removeClass('validate[required]');
				$(this).find(".unrealizeGain").removeClass('validate[required]');
				$(this).find(".unrealizeLost").removeClass('validate[required]');
			}
		currencyArr += '{ "currencyId":"'+currency+'", "realizeGain": "'+realizeGain+'", "realizeLost": "'+realizeLost+'", "unrealizeGain": "'+unrealizeGain+'", "unrealizeLost": "'+unrealizeLost+'", "insertStatus": "'+insertStatus+'", "delStatus": "'+deleteStatus+'"},';	
		});
		currencyArr = currencyArr.substr(0,currencyArr.length-1);
		currencyArr += " ]";
		
		loadCur = 'currency';
	}
	//------------------Service Provider--------------------
	if($('#chkServiceProvider').attr('checked'))
	{
		serviceProArr="[ ";
		$('#tblServiceProMainGrid tr').each(function() {
			var serviceProvider = "";
			var deleteStatus = "";
			var insertStatus = "serviceProvider";
			if($(this).find(".checkServProvider").attr('checked'))
			{
				deleteStatus 	= 'false';
				serviceProvider = $(this).find(".accActServProv").attr('id');
				var ServiceProAccount	= $(this).find(".ServiceProAccount").val();
				
				$(this).find(".ServiceProAccount").addClass('validate[required]');
			}
			else
			{
				deleteStatus 	= 'true';
				serviceProvider = $(this).find(".accActServProv").attr('id');
				
				$(this).find(".ServiceProAccount").removeClass('validate[required]');
			}
		serviceProArr += '{ "serviceProId":"'+serviceProvider+'", "serviceProAccId": "'+ServiceProAccount+'", "insertStatus": "'+insertStatus+'", "delStatus": "'+deleteStatus+'"},';	
		});
		serviceProArr = serviceProArr.substr(0,serviceProArr.length-1);
		serviceProArr += " ]";
		
		loadServicePro = 'serviceProvider';
	}
	//--------------------------------------	
	
	if ($('#frmChartOfAccountsActivate').validationEngine('validate'))   
    {
		var requestType = '';
		requestType = 'edit';

	    var url = "chartOfAccountsActivate-db-set.php"+"?requestType="+requestType;
     	var obj = $.ajax({
			url:url,
			dataType: "json",
			type:'post',  
			//data: {myJson:  value},
			data: {customer: customerArr, customerItem: customerItemArr, supplier: supplierArr, supplierItem: supplierItemArr, tax: taxArr, currency: currencyArr, serviceProvider: serviceProArr, otherSupplier: otherSupplierArr, otherCustomer:otherCustomerArr},
			async:false,
			
			success:function(json){
					$('#frmChartOfAccountsActivate #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmChartOfAccountsActivate').get(0).reset();
						loadCombo_frmChartOfAccountsActivate();
						var t=setTimeout("alertx()",1000);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmChartOfAccountsActivate #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}	
});
   
   /////////////////////////////////////////////////////
   //// account item activate details //////////////////
   /////////////////////////////////////////////////////
	
	$('#frmChartOfAccountsActivate #butNew').click(function(){
		$('#frmChartOfAccountsActivate').get(0).reset();
		loadCombo_frmChartOfAccountsActivate();
		callDataHideRow()
	});
    
    $('#frmChartOfAccountsActivate #chkAllCustomers').click(function(){
        var chkAll=document.getElementById("chkAllCustomers");
        var status=false;
        if(chkAll.checked){
            status=true;
        }
        else{
            status=false;
        }
        $('#frmChartOfAccountsActivate .checkCustomer').attr("checked",status);		
    });
	
	//-------------------------------------------------------------------------
		$('#frmChartOfAccountsActivate #chkAllOtherCustomers').click(function(){
        var chkAll=document.getElementById("chkAllOtherCustomers");
        var status=false;
        if(chkAll.checked){
            status=true;
        }
        else{
            status=false;
        }
        $('#frmChartOfAccountsActivate .checkOtherCustomer').attr("checked",status);		
    });
	//-------------------------------------------------------------------------
        
    $('#frmChartOfAccountsActivate #chkAllCustomersItem').click(function(){
        var chkAll=document.getElementById("chkAllCustomersItem");
        var status=false;
        if(chkAll.checked){
            status=true;
        }
        else{
            status=false;
        }
        $('#frmChartOfAccountsActivate .checkCustomerItem').attr("checked",status);		
    });
        
    $('#frmChartOfAccountsActivate #chkAllSupplier').click(function(){
        var chkAll=document.getElementById("chkAllSupplier");
        var status=false;
        if(chkAll.checked){
            status=true;
        }
        else{
            status=false;
        }
        $('#frmChartOfAccountsActivate .checkSupplier').attr("checked",status);		
    });
	
	///////////////////////////////////////////////////
		$('#frmChartOfAccountsActivate #chkAllOtherSupplier').click(function(){
        var chkAll=document.getElementById("chkAllOtherSupplier");
        var status=false;
        if(chkAll.checked){
            status=true;
        }
        else{
            status=false;
        }
        $('#frmChartOfAccountsActivate .checkOtherSupplier').attr("checked",status);		
    });
	//////////////////////////////////////////////////
        
    $('#frmChartOfAccountsActivate #chkAllSuppliersItem').click(function(){
        var chkAll=document.getElementById("chkAllSuppliersItem");
        var status=false;
        if(chkAll.checked){
            status=true;
        }
        else{
            status=false;
        }
        $('#frmChartOfAccountsActivate .checkSupplierItem').attr("checked",status);		
    });
        
    $('#frmChartOfAccountsActivate #chkAllTax').click(function(){
        var chkAll=document.getElementById("chkAllTax");
        var status=false;
        if(chkAll.checked){
            status=true;
        }
        else{
            status=false;
        }
        $('#frmChartOfAccountsActivate .checkTax').attr("checked",status);		
    });
        
    $('#frmChartOfAccountsActivate #chkAllCurrency').click(function(){
        var chkAll=document.getElementById("chkAllCurrency");
        var status=false;
        if(chkAll.checked){
            status=true;
        }
        else{
            status=false;
        }
        $('#frmChartOfAccountsActivate .checkCurrency').attr("checked",status);		
    });
    
        
    $('#frmChartOfAccountsActivate #chkAllServiceProvider').click(function(){
        var chkAll=document.getElementById("chkAllServiceProvider");
        var status=false;
        if(chkAll.checked){
            status=true;
        }
        else{
            status=false;
        }
        $('#frmChartOfAccountsActivate .checkServProvider').attr("checked",status);		
    });
        
        
 
});

function loadCombo_frmChartOfAccountsActivate()
{
		$('#frmChartOfAccountsActivate').validationEngine('hide');
 		var url = "chartOfAccountsActivate-db-get.php";
		var data ="requestType="+'loadDetails';
			data +="&loadCus="+loadCus;
			data +="&loadOtherCus="+loadOtherCus;
			data +="&loadCusItm="+loadCusItm;
			data +="&loadSup="+loadSup;
			data +="&loadOtherSup="+loadOtherSup;
			data +="&loadSupItm="+loadSupItm;
			data +="&loadTax="+loadTax;
			data +="&loadCur="+loadCur;
			data +="&loadServicePro="+loadServicePro;
			//data +="&loadSerPro="+loadSerPro;
			
     	var obj = $.ajax({
			url:url,
			dataType: "json",  
			data: data,
			async:false,
			success:function(json){
				if(eval(json)!=null)
				{
						for(var i=0;i<=json.length;i++)
						{
							if(json[i].loadStatus == 'customer')
							{
								 var cusId 		= json[i].customerId;
								 var cusAccId 	= json[i].customerAccId;
								 $('#frmChartOfAccountsActivate #cboCusAcc'+cusId).val(cusAccId);
							}
							if(json[i].loadStatus == 'otherCustomer')
							{
								var cusId 		= json[i].otherCustomerId;
							 	var cusAccId 	= json[i].otherCustomerAccId;
							 	$('#frmChartOfAccountsActivate #cboOtherCusAcc'+cusId).val(cusAccId);
							}
							if(json[i].loadStatus == 'customerItem')
							{
								var cusItmId 		= json[i].customerItmId;
							 	var cusItmAccId 	= json[i].customerItmAccId;
							 	$('#frmChartOfAccountsActivate #cboCusItemAcc'+cusItmId).val(cusItmAccId);
							}
							if(json[i].loadStatus == 'supplier')
							{
								var supId 		= json[i].supplierId;
							 	var supAccId 	= json[i].supplierAccId;
							 	$('#frmChartOfAccountsActivate #cboSupAcc'+supId).val(supAccId);
							}
							if(json[i].loadStatus == 'otherSupplier')
							{
								var supId 		= json[i].otherSupplierId;
							 	var supAccId 	= json[i].otherSupplierAccId;
							 	$('#frmChartOfAccountsActivate #cboOtherSupAcc'+supId).val(supAccId);
							}
							if(json[i].loadStatus == 'supplierItem')
							{
								var supItmId 		= json[i].supplierItmId;
							 	var supItmAccId 	= json[i].supplierItmAccId;
							 	$('#frmChartOfAccountsActivate #cboSupItemAcc'+supItmId).val(supItmAccId);
							}
							if(json[i].loadStatus == 'tax')
							{
								var taxId 		= json[i].taxId;
							 	var taxAccId 	= json[i].taxAccId;
							 	$('#frmChartOfAccountsActivate #cboTaxAcc'+taxId).val(taxAccId);
							}
							if(json[i].loadStatus == 'currency')
							{
								var currencyId 		= json[i].currencyId;
							 	var realizeGain 	= json[i].realizeGain;
								var realizeLost 	= json[i].realizeLost;
								var unrealizeGain 	= json[i].unrealizeGain;
								var unrealizeLost 	= json[i].unrealizeLost;

							 	$('#frmChartOfAccountsActivate #cboRealizeGainAcc'+currencyId).val(realizeGain);
								$('#frmChartOfAccountsActivate #cboRealizeLostAcc'+currencyId).val(realizeLost);
								$('#frmChartOfAccountsActivate #cboUnrealizeGainAcc'+currencyId).val(unrealizeGain);
								$('#frmChartOfAccountsActivate #cboUnrealizeLostAcc'+currencyId).val(unrealizeLost);
							}
							if(json[i].loadStatus == 'serviceProvider')
							{
								var serviceProId 		= json[i].serviceProviderId;
							 	var serviceProAccId 	= json[i].serviceProviderAccId;
							 	$('#frmChartOfAccountsActivate #cboServProvAcc'+serviceProId).val(serviceProAccId);
							}
						}
				}
			}
	});
}

function alertx()
{
	$('#frmChartOfAccountsActivate #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	//
}
function callDataHideRow()
{
	$('.tblOneClsMainRow').hide();
	$('.tblOneOneClsMainRow').hide(); // Other Receivable / Service Customer Details
	$('.tblTwoClsMainRow').hide();
	$('.tblThreeClsMainRow').hide();
	$('.tblThreeOneClsMainRow').hide(); // Other Payable / Service Supplier Details
	$('.tblFourClsMainRow').hide();
	$('.tblFiveClsMainRow').hide();
	$('.tblSixClsMainRow').hide();
	$('.tblSevenClsMainRow').hide();
}

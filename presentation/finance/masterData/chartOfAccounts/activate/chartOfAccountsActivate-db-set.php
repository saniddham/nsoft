<?php 
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$companyId = $row['intCompanyId']; 
	}
	
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	//$values = json_decode($_REQUEST['customer'], true);
	
	if($requestType=='edit')
	{	
		//--------------------customer activate---------------------------
		$values = json_decode($_REQUEST['customer'], true);
		foreach($values as $value)
		{
			if(count($value) != 0)
			{
				$insertStatus 	= $value['insertStatus'];
				if($insertStatus == 'customer')
				{
					 $delStatus = $value['delStatus'];
					 $cusId		= $value['cusId'];
					 $cusAccId	= null($value['cusAccId']);
					if($delStatus == 'true')
					{
						$response['type'] 		= 'pass';
						$response['msg'] 		= 'Saved successfully.';
					}
					else
					{
					$sql = "SELECT * FROM mst_financecustomeractivate WHERE (`intCustomerId`='$cusId' AND `intCompanyId`='$companyId')";
					 $result = $db->RunQuery($sql);
					 if(mysqli_num_rows($result)>0)
					 {
						/////////// customer activate update part /////////////////////
				$sql = "UPDATE `mst_financecustomeractivate` SET intChartOfAccountId	='$cusAccId',
																intModifyer				='$userId'
								WHERE (`intCustomerId`='$cusId' AND `intCompanyId`='$companyId')";
						$result = $db->RunQuery($sql);
					 }
					 else
					 {
						/////////// customer activate insert part /////////////////////
						$sql = "INSERT INTO `mst_financecustomeractivate` (`intCustomerId`,`intCompanyId`,`intChartOfAccountId`,`intCreator`,dtmCreateDate) 
								VALUES ('$cusId','$companyId',$cusAccId,'$userId',now())";
						$result = $db->RunQuery($sql);
					 }
					 if($result)
					 {
						$response['type'] 		= 'pass';
						$response['msg'] 		= 'Saved successfully.';
					 }
					 else
					 {
						$response['type'] 		= 'fail';
						$response['msg'] 		= $db->errormsg;
						$response['q'] 			= $sql;
					}
				   }
				}
			}
			else
			{
				$response['type'] 		= 'pass';
				$response['msg'] 		= 'Saved successfully.';
			}
		}
		//--------------------customer activate---------------------------
		
		//---------other receivable / service customer activate-----------
		$values = json_decode($_REQUEST['otherCustomer'], true);
		foreach($values as $value)
		{
			if(count($value) != 0)
			{
				$insertStatus 	= $value['insertStatus'];
				if($insertStatus == 'otherCustomer')
				{
					 $delStatus = $value['delStatus'];
					 $cusId		= $value['cusId'];
					 $cusAccId	= null($value['cusAccId']);
					if($delStatus == 'true')
					{
						$response['type'] 		= 'pass';
						$response['msg'] 		= 'Saved successfully.';
					}
					else
					{
					$sql = "SELECT * FROM mst_finance_service_customer_activate WHERE (`intCustomerId`='$cusId' AND `intCompanyId`='$companyId')";
					 $result = $db->RunQuery($sql);
					 if(mysqli_num_rows($result)>0)
					 {
						/////////// other receivable / service customer activate update part /////////////////////
				$sql = "UPDATE `mst_finance_service_customer_activate` SET intChartOfAccountId	='$cusAccId',
																intModifyer				='$userId'
								WHERE (`intCustomerId`='$cusId' AND `intCompanyId`='$companyId')";
						$result = $db->RunQuery($sql);
					 }
					 else
					 {
						/////////// other receivable / service customer activate insert part /////////////////////
						$sql = "INSERT INTO `mst_finance_service_customer_activate` (`intCustomerId`,`intCompanyId`,`intChartOfAccountId`,`intCreator`,dtmCreateDate) 
								VALUES ('$cusId','$companyId',$cusAccId,'$userId',now())";
						$result = $db->RunQuery($sql);
					 }
					 if($result)
					 {
						$response['type'] 		= 'pass';
						$response['msg'] 		= 'Saved successfully.';
					 }
					 else
					 {
						$response['type'] 		= 'fail';
						$response['msg'] 		= $db->errormsg;
						$response['q'] 			= $sql;
					}
				   }
				}
			}
			else
			{
				$response['type'] 		= 'pass';
				$response['msg'] 		= 'Saved successfully.';
			}
		}
		//---------other receivable / service customer activate-----------
		
		//--------------------customer item activate----------------------
		$values = json_decode($_REQUEST['customerItem'], true);
		foreach($values as $value)
		{
			if(count($value) != 0)
			{
				$insertStatus 	= $value['insertStatus'];
				if($insertStatus == 'customerItem')
				{
					 $delStatus = $value['delStatus'];
					 $cusItmId		= $value['cusItmId'];
					 $cusItmAccId	= null($value['cusItmAccId']);
					if($delStatus == 'true')
					{
						$response['type'] 		= 'pass';
						$response['msg'] 		= 'Saved successfully.';
					}
					else
					{
					$sql = "SELECT * FROM mst_financecustomeritemactivate WHERE (`intCustomerItemId`='$cusItmId' AND `intCompanyId`='$companyId')";
					 $result = $db->RunQuery($sql);
					 if(mysqli_num_rows($result)>0)
					 {
						/////////// customer item activate update part /////////////////////
				$sql = "UPDATE `mst_financecustomeritemactivate` SET intChartOfAccountId	='$cusItmAccId',
																	 intModifyer			='$userId'
								WHERE (`intCustomerItemId`='$cusItmId' AND `intCompanyId`='$companyId')";
						$result = $db->RunQuery($sql);
					 }
					 else
					 {
						/////////// customer item activate insert part /////////////////////
						$sql = "INSERT INTO `mst_financecustomeritemactivate` (`intCustomerItemId`,`intCompanyId`,`intChartOfAccountId`,`intCreator`,dtmCreateDate) 
								VALUES ('$cusItmId','$companyId',$cusItmAccId,'$userId',now())";
						$result = $db->RunQuery($sql);
					 }
					 if($result)
					 {
						$response['type'] 		= 'pass';
						$response['msg'] 		= 'Saved successfully.';
					 }
					 else
					 {
						$response['type'] 		= 'fail';
						$response['msg'] 		= $db->errormsg;
						$response['q'] 			= $sql;
					}
				   }
				}
			}
			else
			{
				$response['type'] 		= 'pass';
				$response['msg'] 		= 'Saved successfully.';
			}
		}
		//--------------------customer item activate----------------------
		
		//--------------------supplier activate----------------------
		$values = json_decode($_REQUEST['supplier'], true);
		foreach($values as $value)
		{
			if(count($value) != 0)
			{
				$insertStatus 	= $value['insertStatus'];
				if($insertStatus == 'supplier')
				{
					 $delStatus = $value['delStatus'];
					 $supId		= $value['supId'];
					 $supAccId	= null($value['supAccId']);
					if($delStatus == 'true')
					{
						$response['type'] 		= 'pass';
						$response['msg'] 		= 'Saved successfully.';
					}
					else
					{
					$sql = "SELECT * FROM mst_financesupplieractivate WHERE (`intSupplierId`='$supId' AND `intCompanyId`='$companyId')";
					 $result = $db->RunQuery($sql);
					 if(mysqli_num_rows($result)>0)
					 {
						/////////// supplier activate update part /////////////////////
				$sql = "UPDATE `mst_financesupplieractivate` SET intChartOfAccountId	='$supAccId',
																	 intModifyer		='$userId'
								WHERE (`intSupplierId`='$supId' AND `intCompanyId`='$companyId')";
						$result = $db->RunQuery($sql);
					 }
					 else
					 {
						/////////// supplier activate insert part /////////////////////
						$sql = "INSERT INTO `mst_financesupplieractivate` (`intSupplierId`,`intCompanyId`,`intChartOfAccountId`,`intCreator`,dtmCreateDate) 
								VALUES ('$supId','$companyId',$supAccId,'$userId',now())";
						$result = $db->RunQuery($sql);
					 }
					 if($result)
					 {
						$response['type'] 		= 'pass';
						$response['msg'] 		= 'Saved successfully.';
					 }
					 else
					 {
						$response['type'] 		= 'fail';
						$response['msg'] 		= $db->errormsg;
						$response['q'] 			= $sql;
					}
				   }
				}
			}
			else
			{
				$response['type'] 		= 'pass';
				$response['msg'] 		= 'Saved successfully.';
			}
		}
		//--------------------supplier activate----------------------
		
		//--------other payable / service supplier activate----------
		$values = json_decode($_REQUEST['otherSupplier'], true);
		foreach($values as $value)
		{
			if(count($value) != 0)
			{
				$insertStatus 	= $value['insertStatus'];
				if($insertStatus == 'otherSupplier')
				{
					 $delStatus = $value['delStatus'];
					 $supId		= $value['supId'];
					 $supAccId	= null($value['supAccId']);
					if($delStatus == 'true')
					{
						$response['type'] 		= 'pass';
						$response['msg'] 		= 'Saved successfully.';
					}
					else
					{
					$sql = "SELECT * FROM mst_finance_service_supplier_activate WHERE (`intSupplierId`='$supId' AND `intCompanyId`='$companyId')";
					 $result = $db->RunQuery($sql);
					 if(mysqli_num_rows($result)>0)
					 {
						/////////// other payable / service supplier activate update part /////////////////////
				$sql = "UPDATE `mst_finance_service_supplier_activate` SET intChartOfAccountId	='$supAccId',
																	 intModifyer		='$userId'
								WHERE (`intSupplierId`='$supId' AND `intCompanyId`='$companyId')";
						$result = $db->RunQuery($sql);
					 }
					 else
					 {
						/////////// other payable / service supplier activate insert part /////////////////////
						$sql = "INSERT INTO `mst_finance_service_supplier_activate` (`intSupplierId`,`intCompanyId`,`intChartOfAccountId`,`intCreator`,dtmCreateDate) 
								VALUES ('$supId','$companyId',$supAccId,'$userId',now())";
						$result = $db->RunQuery($sql);
					 }
					 if($result)
					 {
						$response['type'] 		= 'pass';
						$response['msg'] 		= 'Saved successfully.';
					 }
					 else
					 {
						$response['type'] 		= 'fail';
						$response['msg'] 		= $db->errormsg;
						$response['q'] 			= $sql;
					}
				   }
				}
			}
			else
			{
				$response['type'] 		= 'pass';
				$response['msg'] 		= 'Saved successfully.';
			}
		}
		//--------other payable / service supplier activate----------
		
		//--------------------supplier item activate-----------------
		$values = json_decode($_REQUEST['supplierItem'], true);
		foreach($values as $value)
		{
			if(count($value) != 0)
			{
				$insertStatus 	= $value['insertStatus'];
				if($insertStatus == 'supplierItem')
				{
					 $delStatus = $value['delStatus'];
					 $supItmId		= $value['supItmId'];
					 $supItmAccId	= null($value['supItmAccId']);
					if($delStatus == 'true')
					{
						$response['type'] 		= 'pass';
						$response['msg'] 		= 'Saved successfully.';
					}
					else
					{
					$sql = "SELECT * FROM mst_financesupplieritemactivate WHERE (`intSupplierItemId`='$supItmId' AND `intCompanyId`='$companyId')";
					 $result = $db->RunQuery($sql);
					 if(mysqli_num_rows($result)>0)
					 {
						/////////// supplier item activate update part /////////////////////
				$sql = "UPDATE `mst_financesupplieritemactivate` SET intChartOfAccountId	='$supItmAccId',
																	 intModifyer		='$userId'
								WHERE (`intSupplierItemId`='$supItmId' AND `intCompanyId`='$companyId')";
						$result = $db->RunQuery($sql);
					 }
					 else
					 {
						/////////// supplier item activate insert part /////////////////////
						$sql = "INSERT INTO `mst_financesupplieritemactivate` (`intSupplierItemId`,`intCompanyId`,`intChartOfAccountId`,`intCreator`,dtmCreateDate) 
								VALUES ('$supItmId','$companyId',$supItmAccId,'$userId',now())";
						$result = $db->RunQuery($sql);
					 }
					 if($result)
					 {
						$response['type'] 		= 'pass';
						$response['msg'] 		= 'Saved successfully.';
					 }
					 else
					 {
						$response['type'] 		= 'fail';
						$response['msg'] 		= $db->errormsg;
						$response['q'] 			= $sql;
					}
				   }
				}
			}
			else
			{
				$response['type'] 		= 'pass';
				$response['msg'] 		= 'Saved successfully.';
			}
		}
		//--------------------supplier item activate----------------------
		
		//------------------------tax activate----------------------------
		$values = json_decode($_REQUEST['tax'], true);
		foreach($values as $value)
		{
			if(count($value) != 0)
			{
				$insertStatus 	= $value['insertStatus'];
				if($insertStatus == 'tax')
				{
					 $delStatus = $value['delStatus'];
					 $taxId		= $value['taxId'];
					 $taxAccId	= null($value['taxAccId']);
					if($delStatus == 'true')
					{
						$response['type'] 		= 'pass';
						$response['msg'] 		= 'Saved successfully.';
					}
					else
					{
					$sql = "SELECT * FROM mst_financetaxactivate WHERE (`intTaxId`='$taxId' AND `intCompanyId`='$companyId')";
					 $result = $db->RunQuery($sql);
					 if(mysqli_num_rows($result)>0)
					 {
						/////////// tax activate update part /////////////////////
				$sql = "UPDATE `mst_financetaxactivate` SET intChartOfAccountId	='$taxAccId',
																	 intModifyer		='$userId'
								WHERE (`intTaxId`='$taxId' AND `intCompanyId`='$companyId')";
						$result = $db->RunQuery($sql);
					 }
					 else
					 {
						/////////// tax activate insert part /////////////////////
						$sql = "INSERT INTO `mst_financetaxactivate` (`intTaxId`,`intCompanyId`,`intChartOfAccountId`,`intCreator`,dtmCreateDate) 
								VALUES ('$taxId','$companyId',$taxAccId,'$userId',now())";
						$result = $db->RunQuery($sql);
					 }
					 if($result)
					 {
						$response['type'] 		= 'pass';
						$response['msg'] 		= 'Saved successfully.';
					 }
					 else
					 {
						$response['type'] 		= 'fail';
						$response['msg'] 		= $db->errormsg;
						$response['q'] 			= $sql;
					}
				   }
				}
			}
			else
			{
				$response['type'] 		= 'pass';
				$response['msg'] 		= 'Saved successfully.';
			}
		}
		//--------------------tax activate----------------------
		
		//------------------currency activate-------------------
		$values = json_decode($_REQUEST['currency'], true);
		foreach($values as $value)
		{
			if(count($value) != 0)
			{
				$insertStatus 	= $value['insertStatus'];
				if($insertStatus == 'currency')
				{
					 $delStatus 	= $value['delStatus'];
					 $currencyId	= $value['currencyId'];
					 $realizeGain	= null($value['realizeGain']);
					 $realizeLost	= null($value['realizeLost']);
					 $unrealizeGain= null($value['unrealizeGain']);
					 $unrealizeLost	= null($value['unrealizeLost']);
					if($delStatus == 'true')
					{
						$response['type'] 		= 'pass';
						$response['msg'] 		= 'Saved successfully.';
					}
					else
					{
					$sql = "SELECT * FROM mst_financecurrencyactivate WHERE (`intCurrencyId`='$currencyId' AND `intCompanyId`='$companyId')";
					 $result = $db->RunQuery($sql);
					 if(mysqli_num_rows($result)>0)
					 {
						/////////// currency activate update part /////////////////////
					 $sql = "UPDATE `mst_financecurrencyactivate` SET 	intRealizeGainId	='$realizeGain',
																		intRealizeLostId	='$realizeLost',
																		intUnrealizeGainId	='$unrealizeGain',
																		intUnrealizeLostId	='$unrealizeLost',
																	 	intModifyer		='$userId'
								WHERE (`intCurrencyId`='$currencyId' AND `intCompanyId`='$companyId')";
						$result = $db->RunQuery($sql);
					 }
					 else
					 {
						/////////// currency activate insert part /////////////////////
						$sql = "INSERT INTO `mst_financecurrencyactivate` (`intCurrencyId`,`intCompanyId`,`intRealizeGainId`,`intRealizeLostId`,`intUnrealizeGainId`,`intUnrealizeLostId`,`intCreator`,dtmCreateDate) 
								VALUES ('$currencyId','$companyId',$realizeGain,$realizeLost,$unrealizeGain,$unrealizeLost,'$userId',now())";
						$result = $db->RunQuery($sql);
					 }
					 if($result)
					 {
						$response['type'] 		= 'pass';
						$response['msg'] 		= 'Saved successfully.';
					 }
					 else
					 {
						$response['type'] 		= 'fail';
						$response['msg'] 		= $db->errormsg;
						$response['q'] 			= $sql;
					}
				   }
				}
			}
			else
			{
				$response['type'] 		= 'pass';
				$response['msg'] 		= 'Saved successfully.';
			}
		}
		//--------------------currency activate----------------------
		
		//--------------------Service Provider activate---------------------------
		$values = json_decode($_REQUEST['serviceProvider'], true);
		foreach($values as $value)
		{
			if(count($value) != 0)
			{
				$insertStatus 	= $value['insertStatus'];
				if($insertStatus == 'serviceProvider')
				{
					 $delStatus = $value['delStatus'];
					 $serviceProId		= $value['serviceProId'];
					 $serviceProAccId	= null($value['serviceProAccId']);
					if($delStatus == 'true')
					{
						$response['type'] 		= 'pass';
						$response['msg'] 		= 'Saved successfully.';
					}
					else
					{

					$sql = "SELECT * FROM mst_financeserviceprovideractivate WHERE mst_financeserviceprovideractivate.intServiceProviderId = '$serviceProId' AND mst_financeserviceprovideractivate.intCompanyId = '$companyId'";
					 $result = $db->RunQuery($sql);
					 if(mysqli_num_rows($result)>0)
					 {
						/////////// Service provider activate update part /////////////////////
				$sql = "UPDATE mst_financeserviceprovideractivate
						SET intChartOfAccountId ='$serviceProAccId',
							   intModifyer ='$userId' 
						WHERE (`intServiceProviderId`='$serviceProId') AND (`intCompanyId`='$companyId')";
						$result = $db->RunQuery($sql);
					 }
					 else
					 {
			         	/////////// Service provider activate insert part /////////////////////
						$sql = "INSERT INTO mst_financeserviceprovideractivate 
								(`intServiceProviderId`,`intCompanyId`,`intChartOfAccountId`,`intCreator`,`dtmCreateDate`) 
								VALUES ('$serviceProId','$companyId','$serviceProAccId','$userId',now())";
						$result = $db->RunQuery($sql);
					 }
					 if($result)
					 {
						$response['type'] 		= 'pass';
						$response['msg'] 		= 'Saved successfully.';
					 }
					 else
					 {
						$response['type'] 		= 'fail';
						$response['msg'] 		= $db->errormsg;
						$response['q'] 			= $sql;
					}
				   }
				}
			}
			else
			{
				$response['type'] 		= 'pass';
				$response['msg'] 		= 'Saved successfully.';
			}
		}
		//--------------------Service Prvider activate---------------------------
		

	}
	echo json_encode($response);
?>
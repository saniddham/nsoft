<?php 
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	/*$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$companyId = $row['intCompanyId']; 
	}*/
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	$id 			= $_REQUEST['cboSearch'];
	$code			= trim($_REQUEST['code']);
	$name			= trim($_REQUEST['name']);
	$financialType	= trim($_REQUEST['fnId']);
	$type			= trim($_REQUEST['type']);
	$postingLevel	= trim($_REQUEST['posLevel']);
	$intBlock		= ($_REQUEST['block']=='true'?1:0);
	$debit			= trim($_REQUEST['consDebit']);
	$credit			= trim($_REQUEST['consCredit']);
	$consolidation	= trim($_REQUEST['consolidation']);
	$adjustments	= trim($_REQUEST['adjustments']);
	$intStatus		= ($_REQUEST['status']=='true'?1:0);
    $headerAcc		= val($_REQUEST['headerAcc']);
	
	//------------------------------------------------------
		$companies = json_decode($_REQUEST['company'], true);
	//------------------------------------------------------
	
	/////////// chart Of accounts insert part /////////////////////
	if($requestType=='add')
	{
		$sql = "INSERT INTO `mst_financechartofaccounts` (`strCode`,`strName`,`intFinancialTypeId`,`strType`,`strPostingLevel`,`intPostingLevelBlock`,`strConsolDebitAC`,`strConsolCreditAC`,`strConsolTransaction`,`strExchangeRateAdj`,`intStatus`,`intCreator`,dtmCreateDate,headerAcc) 
					VALUES ('$code','$name','$financialType','$type','$postingLevel','$intBlock','$debit','$credit','$consolidation','$adjustments','$intStatus','$userId',now(),'$headerAcc')";
					
		$chatAccId = $db->autoInsertNo($sql);
		
		if(count($companies) != 0 && $chatAccId != '')
		{
			foreach($companies as $company)
			{
				$companyId = $company['companyId'];
				$sql = "INSERT INTO `mst_financechartofaccounts_companies`
						(`intChartOfAccountId`,`intCompanyId`,`intCreator`,dtmCreateDate) 
					   VALUES 
						('$chatAccId','$companyId','$userId',now())";
				$result = $db->RunQuery($sql);
			}
		}
		if($result){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
	}
	/////////// chart Of accounts update part /////////////////////
	else if($requestType=='edit')
	{
			$sql = "DELETE FROM `mst_financechartofaccounts_companies` WHERE (`intChartOfAccountId`='$id')";
			$db->RunQuery($sql);
		
			if(count($companies) != 0)
			{
				foreach($companies as $company)
				{
					$companyId = $company['companyId'];
					$sql = "INSERT INTO `mst_financechartofaccounts_companies`
							(`intChartOfAccountId`,`intCompanyId`,`intCreator`,dtmCreateDate) 
						   VALUES 
							('$id','$companyId','$userId',now())";
					$result = $db->RunQuery($sql);
				}
			}
			
			$sql = "UPDATE `mst_financechartofaccounts` SET 	strCode					='$code',
																strName					='$name',
																intFinancialTypeId		='$financialType',
																strType					='$type',
																strPostingLevel			='$postingLevel',
																intPostingLevelBlock	='$intBlock',
																strConsolDebitAC		='$debit',
																strConsolCreditAC		='$credit',
																strConsolTransaction	='$consolidation',
																strExchangeRateAdj		='$adjustments',
																intStatus				='$intStatus',
																intModifyer				='$userId',
                                                                headerAcc               ='$headerAcc'    
																WHERE (`intId`='$id')";
			$result = $db->RunQuery($sql);

		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Updated successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	/////////// chart Of accounts delete part /////////////////////
	else if($requestType=='delete')
	{
		$sql = "DELETE FROM `mst_financechartofaccounts` WHERE (`intId`='$id')  ";
		$result = $db->RunQuery($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Deleted successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	echo json_encode($response);
?>
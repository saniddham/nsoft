<?php
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$companyId = $row['intCompanyId']; 
	}
	
	/////////// chart Of accounts load part /////////////////////
	if($requestType=='loadCombo')
	{
		$sql = "SELECT
					intId,
					strCode,
					strName
					FROM mst_financechartofaccounts
					order by strCode
					";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html.= "<option value=\"".$row['intId']."\">".$row['strCode']."-".$row['strName']."</option>";
		}
		echo $html;
	}
	else if($requestType=='loadHeaderAccCombo')
	{
		$typId  = $_REQUEST['typeId'];
		$sql = "SELECT
					intId,
					strCode,
					strName
					FROM mst_financechartofaccounts
                    WHERE strType='Heading' AND intFinancialTypeId='$typId'
					order by strCode
					";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html.= "<option value=\"".$row['intId']."\">".$row['strCode']."-".$row['strName']."</option>";
		}
		echo $html;
	}
	else if($requestType=='loadDetails')
	{
		$id  = $_REQUEST['id'];
		
		//------------------------------
		$sql = "SELECT intCompanyId
				FROM mst_financechartofaccounts_companies
				WHERE
					intChartOfAccountId =  '$id'";
		$result = $db->RunQuery($sql);
	
		$arrCom;
		while($row=mysqli_fetch_array($result))
		{
			$val['comId'] 	= $row['intCompanyId'];
			$arrCom[] = $val;
		}
		
		$response['comVal'] = $arrCom;
		//------------------------------
		
		$sql = "SELECT
					strCode,
					strName	,		
					intFinancialTypeId,
					strType,		
					strPostingLevel,
					intPostingLevelBlock,
					strConsolDebitAC,	
					strConsolCreditAC,
					strConsolTransaction,
					strExchangeRateAdj,
					intStatus,
                    headerAcc		
				FROM mst_financechartofaccounts
				WHERE
					intId =  '$id'
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['code'] 			= $row['strCode'];
			$response['name'] 			= $row['strName'];
			$response['fnTypeId'] 		= $row['intFinancialTypeId'];
			$response['type'] 			= $row['strType'];
			$response['posLevel'] 		= $row['strPostingLevel'];
			$response['block'] 			= ($row['intPostingLevelBlock']?true:false);
			$response['debit'] 			= $row['strConsolDebitAC'];
			$response['credit'] 		= $row['strConsolCreditAC'];
			$response['transaction'] 	= $row['strConsolTransaction'];
			$response['adjustments'] 	= $row['strExchangeRateAdj'];
			$response['status'] 		= ($row['intStatus']?true:false);
			$response['headerAcc'] 		= $row['headerAcc'];
		}
		echo json_encode($response);
	}
	
?>

var fnTypeId = 0;
var type = '';
var postingLevel = '';
var conTransaction = '';
var exchangeAdj = '';
var headerAcc='';

function functionList()
{
    if(accId!='')
    {
        $('#frmChartOfAccounts #cboSearch').val(accId);
        $('#frmChartOfAccounts #cboSearch').change();
    }
}
$(document).ready(function() {
	

    $("#frmChartOfAccounts").validationEngine();
    $('#frmChartOfAccounts #txtCode').focus();
    //permision for add 
    if(intAddx)
    {
        $('#frmChartOfAccounts #butNew').show();
        $('#frmChartOfAccounts #butSave').show();
    }
  
    //permision for edit 
    if(intEditx)
    {
        $('#frmChartOfAccounts #butSave').show();
        $('#frmChartOfAccounts #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
    }
  
    //permision for delete
    if(intDeletex)
    {
        $('#frmChartOfAccounts #butDelete').show();
        $('#frmChartOfAccounts #cboSearch').removeAttr('disabled');
    }
  
    //permision for view
    if(intViewx)
    {
        $('#frmChartOfAccounts #cboSearch').removeAttr('disabled');
    }
  
    /*  $(':input').bind('keyup mouseup', function(){
      $(this).val($.trim($(this).val())) ;
    });*/

    ///save button click event
    $('#frmChartOfAccounts #butSave').click(function(){
	  
        var companyValue="[ ";
        $('.company:checked').each(function(){
            companyValue += '{ "companyId":"'+$(this).val()+'"},';
        });
        companyValue = companyValue.substr(0,companyValue.length-1);
        companyValue += " ]";
	//check header account 
        if(type=='Posting'){
            headerAcc=$('#frmChartOfAccounts #cmbHeaderAcc').val();
        }else{
            headerAcc=0;
        } 
        
        //$('#frmChartOfAccounts').submit();
        var requestType = '';
        if ($('#frmChartOfAccounts').validationEngine('validate'))   
        { 
            //$('#txtLocationCode').validationEngine('showPrompt', 'This is an example', 'pass'); //hide all alerts//
            if($("#frmChartOfAccounts #cboSearch").val()=='')
                requestType = 'add';
            else
                requestType = 'edit';
		
            var url = "chartOfAccounts-db-set.php";
            var data ="requestType="+requestType;
            data +="&cboSearch="+$('#frmChartOfAccounts #cboSearch').val();
            data +="&code="+$('#frmChartOfAccounts #txtCode').val();
            data +="&name="+$('#frmChartOfAccounts #txtName').val();
            data +="&fnId="+fnTypeId;
            data +="&type="+type;
            data +="&posLevel="+postingLevel;
            data +="&block="+$('#frmChartOfAccounts #chkBlock').attr('checked');
            data +="&consDebit="+$('#frmChartOfAccounts #txtComDebit').val();
            data +="&consCredit="+$('#frmChartOfAccounts #txtComCredid').val();
            data +="&consolidation="+conTransaction;
            data +="&adjustments="+exchangeAdj;
            data +="&status="+$('#frmChartOfAccounts #chkActive').attr('checked');
            data +="&company="+companyValue;
            data +="&headerAcc="+headerAcc;
            var obj = $.ajax({
                url:url,
                dataType: "json",
				type:'post',  
                //data:$("#frmChartOfAccounts").serialize()+'&requestType='+requestType,
                //'requestType=add&cboSearch='+$('#frmChartOfAccounts #cboSearch').val()
                data:data,
                async:false,
			
                success:function(json){
                    $('#frmChartOfAccounts #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
                    if(json.type=='pass')
                    {
                        $('#frmChartOfAccounts').get(0).reset();
                        loadCombo_frmChartOfAccounts();
                        loadHeaderAccCombo();
                        var t=setTimeout("alertx()",1000);
                        return;
                    }
                    var t=setTimeout("alertx()",3000);
                },
                error:function(xhr,status){
					
                    $('#frmChartOfAccounts #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
                    var t=setTimeout("alertx()",3000);
                //function (xhr, status){errormsg(status)}
                }		
            });
        }
    });
   
    /////////////////////////////////////////////////////
    //// load chart Of accounts details //////////////////////////
    /////////////////////////////////////////////////////
    $('#frmChartOfAccounts #cboSearch').click(function(){
        $('#frmChartOfAccounts').validationEngine('hide');
    });
    $('#frmChartOfAccounts #cboSearch').change(function(){
        $('#frmChartOfAccounts').validationEngine('hide');
		loadHeaderAccCombo();
        var url = "chartOfAccounts-db-get.php";
        if($('#frmChartOfAccounts #cboSearch').val()=='')
        {
            $('#frmChartOfAccounts').get(0).reset();
            return;	
        }
        var httpobj = $.ajax({
            url:url,
            dataType:'json',
            data:'requestType=loadDetails&id='+$(this).val(),
            async:false,
            success:function(json){
                $('#frmChartOfAccounts #txtCode').val(json.code);
                $('#frmChartOfAccounts #txtName').val(json.name);
                $('#frmChartOfAccounts #'+json.fnTypeId).attr('checked',true); //
                fnTypeId = json.fnTypeId;
                $('#frmChartOfAccounts #'+json.type).attr('checked',true); //
                type = json.type;
				if(type=="Posting")
				{
					$('#frmChartOfAccounts #cmbHeaderAcc').removeAttr('disabled');
					$('#frmChartOfAccounts #cmbHeaderAcc').attr('class','validate[required]');
				}
				else
				{        
					$('#frmChartOfAccounts #cmbHeaderAcc').attr('disabled',true);
					$('#frmChartOfAccounts #cmbHeaderAcc').removeAttr('class')
				}   
				if(json.posLevel)
				{
					$('#frmChartOfAccounts #'+json.posLevel).attr('checked',true); //
                	postingLevel = json.posLevel;
				}
//				else
//				{
//					$('#frmChartOfAccounts #'+json.posLevel).attr('checked',false); //
//                	postingLevel = json.posLevel;
//				}

                ///////
                if(json.block)
                    $('#frmChartOfAccounts #chkBlock').attr('checked',true);
                else
                    $('#frmChartOfAccounts #chkBlock').removeAttr('checked');
					
                $('#frmChartOfAccounts #txtComDebit').val(json.debit);
                $('#frmChartOfAccounts #txtComCredid').val(json.credit);
				
				if(json.transaction)
				{	
					$('#frmChartOfAccounts #'+json.transaction).attr('checked',true); //
					conTransaction = json.transaction;
				}
//				else
//				{
//					$('#frmChartOfAccounts #'+json.transaction).attr('checked',false); //
//					conTransaction = json.transaction;
//				}
				
				if(json.adjustments)
				{
					$('#frmChartOfAccounts #'+json.adjustments).attr('checked',true); //
					exchangeAdj = json.adjustments;
				}
//				else
//				{
//					$('#frmChartOfAccounts #'+json.adjustments).attr('checked',false); //
//					exchangeAdj = json.adjustments;
//				}
					
                if(json.status)
                    $('#frmChartOfAccounts #chkActive').attr('checked',true);
                else
                    $('#frmChartOfAccounts #chkActive').removeAttr('checked');
		
                $('#frmChartOfAccounts #cmbHeaderAcc').val(json.headerAcc);
                //--------------------------------------------------
                var companyId = "";
                if(json.comVal!=null){
                    $('.company:checkbox').removeAttr('checked');	
                    for(var i=0;i<=json.comVal.length-1;i++)
                    {
                        companyId = json.comVal[i].comId;
                        $('#frmChartOfAccounts #chkCompany'+companyId).attr('checked',true);					
                    }
                    }
            else
            {
                $('.company:checkbox').removeAttr('checked');	
            }
        //--------------------------------------------------
        }
        });
    //////////// end of load details /////////////////
	
    });
	
$('#frmChartOfAccounts #butNew').click(function(){
    $('#frmChartOfAccounts').get(0).reset();
    loadCombo_frmChartOfAccounts();
    loadHeaderAccCombo();
    $('#frmChartOfAccounts #txtCode').focus();
});
$('#frmChartOfAccounts #butDelete').click(function(){
    if($('#frmChartOfAccounts #cboSearch').val()=='')
    {
        $('#frmChartOfAccounts #butDelete').validationEngine('showPrompt', 'Please select Ledger Accounts.', 'fail');
        var t=setTimeout("alertDelete()",1000);	
    }
    else
    {
        var val = $.prompt('Are you sure you want to delete "'+$('#frmChartOfAccounts #cboSearch option:selected').text()+'" ?',{
            buttons: {
                Ok: true, 
                Cancel: false
            },
            callback: function(v,m,f){
                if(v)
                {
                    var url = "chartOfAccounts-db-set.php";
                    var httpobj = $.ajax({
                        url:url,
                        dataType:'json',
                        data:'requestType=delete&cboSearch='+$('#frmChartOfAccounts #cboSearch').val(),
                        async:false,
                        success:function(json){
												
                            $('#frmChartOfAccounts #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
                            if(json.type=='pass')
                            {
                                $('#frmChartOfAccounts').get(0).reset();
                                loadCombo_frmChartOfAccounts();
                                loadHeaderAccCombo();
                                var t=setTimeout("alertDelete()",1000);
                                return;
                            }	
                            var t=setTimeout("alertDelete()",3000);
                        }	 
                    });
                }
            }
        });
			
    }
});
	
$('.fnType').click(function(){
    fnTypeId = $(this).attr('id');
	loadHeaderAccCombo();	
});
	
$('.type').click(function(){
    type = $(this).val();
    if(type=="Posting")
	{
        $('#frmChartOfAccounts #cmbHeaderAcc').removeAttr('disabled');
        $('#frmChartOfAccounts #cmbHeaderAcc').attr('class','validate[required]');
		loadHeaderAccCombo();
    }
	else
	{        
        $('#frmChartOfAccounts #cmbHeaderAcc').attr('disabled',true);
        $('#frmChartOfAccounts #cmbHeaderAcc').removeAttr('class')
    }   
});
	
$('.posting').click(function(){
    postingLevel = $(this).val();	
});
	
$('.consolidation').click(function(){
    conTransaction = $(this).val();
});
	
$('.adjustments').click(function(){
    exchangeAdj = $(this).val();
});
});


function loadCombo_frmChartOfAccounts()
{
    var url 	= "chartOfAccounts-db-get.php?requestType=loadCombo";
    var httpobj = $.ajax({
        url:url,
        async:false
    })
    $('#frmChartOfAccounts #cboSearch').html(httpobj.responseText);
}
function loadHeaderAccCombo()
{
    var url 	= "chartOfAccounts-db-get.php?requestType=loadHeaderAccCombo&typeId="+fnTypeId;
    var httpobj = $.ajax({
        url:url,
        async:false
    })
    $('#frmChartOfAccounts #cmbHeaderAcc').html(httpobj.responseText);
}

function alertx()
{
    $('#frmChartOfAccounts #butSave').validationEngine('hide')	;
}
function alertDelete()
{
    $('#frmChartOfAccounts #butDelete').validationEngine('hide')	;
}
////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////

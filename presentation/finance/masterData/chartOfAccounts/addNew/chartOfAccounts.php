<?php
session_start();
$backwardseperator = "../../../../../";
$mainPath = $_SESSION['mainPath'];

$thisFilePath =  $_SERVER['PHP_SELF'];

include  "{$backwardseperator}dataAccess/permisionCheck.inc";
$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$companyId = $row['intCompanyId']; 
}
$accId = $_REQUEST['id'];
?>
<script type="application/javascript" >
var accId = '<?php echo $accId ?>';
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Chart of Accounts</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../../css/promt.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="chartOfAccounts-js.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" href="../../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../../libraries/validate/template.css" type="text/css">

</head>

<body onLoad="functionList();">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<script src="../../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<form id="frmChartOfAccounts" name="frmChartOfAccounts" method="post" action="chartOfAccounts-db-set.php" autocomplete="off"  >
<div align="center">
	<div class="trans_layoutD">
	<div class="trans_text">Chart of Accounts</div>
  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
  <tr>
    <td><table width="585" border="0" align="center">
      <tr>
        <td width="62%"><table width="100%" border="0" > 
          <tr>
            <td height="47" ><table width="100%" border="0" class="">
              <tr>
                <td width="2" class="normalfnt">&nbsp;</td>
                <td width="161" align="center" class="normalfntMid">Ledger Accounts Name</td>
                <td width="396" colspan="3">
				<select name="cboSearch" class="txtbox" id="cboSearch" style="width:361px">
<option value=""></option>
        <?php  $sql = "SELECT
						intId,
						strCode,
						strName
						FROM mst_financechartofaccounts
						order by strCode
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strCode']."-".$row['strName']."</option>";
						}
        ?>
                  </select></td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td colspan="4" class="normalfnt"><table width="100%" border="0" class="tableBorder">
                  <tr>
                    <td colspan="10" align="left" bgcolor="#E4E4E4" class="tableBorder">
                    <span class="normalfnt"><strong>Genaral</strong></span></td>
                  </tr>
                  <tr class="">
                   <td width="29%" class="normalfnt">Ledger Accounts   Code&nbsp;<span class="compulsoryRed">*</span></td>
                    <td width="71%" colspan="9" class="normalfnt"><input name="txtCode" type="text" id="txtCode" style="width:140px" class="validate[required,maxSize[10]]"/></td>
                  </tr>
                  <tr class="">
                    <td class="normalfnt">Ledger Accounts   Name&nbsp;<span class="compulsoryRed">*</span></td>
              <td colspan="9" class="normalfnt"><input name="txtName" type="text" id="txtName" style="width:260px" class="validate[required,maxSize[50]]" /></td>
                  </tr>
                  <tr class="">
                    <td class="normalfnt">Active<span class="compulsoryRed"></span></td>
                    <td colspan="9" class="normalfnt">
                    <input type="checkbox" name="chkActive" id="chkActive" checked="checked" /></td>
                  </tr>
                  <tr>
                    <td colspan="10" align="left" bgcolor="#E4E4E4" class="tableBorder">
                 <span class="normalfnt"><strong>Financial Type <span class="compulsoryRed">*</span></strong></span></td>
                  </tr>
                  <tr class="">
                    <td colspan="10">
                    <table width="100%">
                      <tr>
                       <?php
	  					$sql = "SELECT
						intId,
						strName,
						intFinancialMainTypeID
						FROM mst_financialsubtype
						WHERE
						intStatus = '1'
						ORDER BY intFinancialMainTypeID,intId ";
						$result = $db->RunQuery($sql);
						$data ='' ;
						$i = 0;
						$arr = '';
						$t = '';
						while($row=mysqli_fetch_array($result))
						{
							if($row['intFinancialMainTypeID']!=$t)
								$i = 0;
							$data[$i]['Id'] = $row['intId'];
							$data[$i]['Name'] = $row['strName'];
							$arr[$row['intFinancialMainTypeID']] = $data;
							$t = $row['intFinancialMainTypeID'];
							$i++;
						}
	  					?>
                        <td width="23%" align="center" bgcolor="#E7FAFE" class="tableBorder_allRound"><span class="normalfntMid">Manufacturing</span> <strong><u></u></strong></td>
                        <td width="7%" align="center" bgcolor="#FFFFFF">
               <input class="fnType validate[required]" type="radio" name="radio" id=<?php echo $arr[1][0]['Id']?> /></td>
                     <td width="28%" align="left"><span class="normalfntRight"><?php echo $arr[1][0]['Name']?></span></td>
                        <td width="6%" align="center">
               <input class="fnType validate[required]" type="radio" name="radio" id=<?php echo $arr[1][1]['Id']?> /></td>
                     <td width="23%" align="left"><span class="normalfntRight"><?php echo $arr[1][1]['Name']?></span></td>
                        <td width="13%" align="left">&nbsp;</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td colspan="10" align="left" bgcolor="#FFFFFF" class="tableBorder"><table width="100%">
                      <tr>
                        <td width="23%" rowspan="3" align="center" bgcolor="#E7FAFE" class="tableBorder_allRound"><span class="normalfntMid">Income Statements</span> <strong><u></u></strong></td>
                        <td width="7%" align="center" bgcolor="#FFFFFF">
              <input class="fnType validate[required]" type="radio" name="radio" id=<?php echo $arr[2][0]['Id']?> /></td>
                        <td width="14%" align="left"><span class="normalfnt"><?php echo $arr[2][0]['Name']?></span></td>
                        <td width="6%" align="center">
              <input class="fnType validate[required]" type="radio" name="radio" id=<?php echo $arr[2][1]['Id']?> /></td>
                        <td width="17%" align="left"><span class="normalfnt"><?php echo $arr[2][1]['Name']?></span></td>
                        <td width="6%" align="center">
              <input class="fnType validate[required]" type="radio" name="radio" id=<?php echo $arr[2][2]['Id']?> /></td>
                        <td width="27%" align="left"><span class="normalfnt"><?php echo $arr[2][2]['Name']?></span></td>
                      </tr>
                      <tr>
                        <td align="center" bgcolor="#FFFFFF">
              <input class="fnType validate[required]" type="radio" name="radio" id=<?php echo $arr[2][3]['Id']?> /></td>
                        <td align="left"><span class="normalfnt"><?php echo $arr[2][3]['Name']?></span></td>
                        <td align="center">
              <input class="fnType validate[required]" type="radio" name="radio" id=<?php echo $arr[2][4]['Id']?> /></td>
                        <td align="left"><span class="normalfnt"><?php echo $arr[2][4]['Name']?></span></td>
                        <td align="center">
              <input class="fnType validate[required]" type="radio" name="radio" id=<?php echo $arr[2][5]['Id']?> />
                        </td>
                        <td align="left"><span class="normalfnt"><?php echo $arr[2][5]['Name']?></span></td>
                      </tr>
                       <tr>
                        <td align="center" bgcolor="#FFFFFF">
              <input class="fnType validate[required]" type="radio" name="radio" id=<?php echo $arr[2][6]['Id']?> /></td>
                        <td align="left"><span class="normalfnt"><?php echo $arr[2][6]['Name']?></span></td>
                        <td align="center">
              <input class="fnType validate[required]" type="radio" name="radio" id=<?php echo $arr[2][7]['Id']?> /></td>
                        <td align="left" class="normalfnt"><?php echo $arr[2][7]['Name']?></td>
                        <td align="center">
              <input class="fnType validate[required]" type="radio" name="radio" id=<?php echo $arr[2][8]['Id']?> /></td>
                        <td align="left" class="normalfnt"><?php echo $arr[2][8]['Name']?></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td colspan="10" align="left" bgcolor="#FFFFFF" class="tableBorder"><table width="100%">
                      <tr>
                        <td width="23%" rowspan="11" align="center" bgcolor="#E7FAFE" class="normalfntMid">Balance Sheet </td>
                        <td width="7%" align="center" bgcolor="#FFFFFF">
             <input class="fnType validate[required]" type="radio" name="radio" id=<?php echo $arr[3][0]['Id']?> /></td>
                        <td width="32%" align="left" class="normalfnt"><?php echo $arr[3][0]['Name']?></td>
                        <td width="7%" align="center" class="exlink">
             <input class="fnType validate[required]" type="radio" name="radio" id=<?php echo $arr[3][11]['Id']?> /></td>
                        <td width="31%" align="left" class="normalfnt"><?php echo $arr[3][11]['Name']?></td>
                      </tr>
                      <tr>
                        <td align="center" bgcolor="#FFFFFF">
            <input class="fnType validate[required]" type="radio" name="radio" id=<?php echo $arr[3][1]['Id']?> /></td>
                        <td align="left" class="normalfnt"><?php echo $arr[3][1]['Name']?></td>
                        <td align="center" class="exlink">
            <input class="fnType validate[required]" type="radio" name="radio" id=<?php echo $arr[3][12]['Id']?> /></td>
                        <td align="left" class="normalfnt"><?php echo $arr[3][12]['Name']?></td>
                      </tr>
                      <tr>
                        <td align="center" bgcolor="#FFFFFF">
           <input class="fnType validate[required]" type="radio" name="radio" id=<?php echo $arr[3][2]['Id']?> /></td>
                        <td align="left" class="normalfnt"><?php echo $arr[3][2]['Name']?></td>
                        <td align="center" class="exlink">
           <input class="fnType validate[required]" type="radio" name="radio" id=<?php echo $arr[3][13]['Id']?> /></td>
                        <td align="left" class="normalfnt"><?php echo $arr[3][13]['Name']?></td>
                      </tr>
                      <tr>
                        <td align="center" bgcolor="#FFFFFF">
           <input class="fnType validate[required]" type="radio" name="radio" id=<?php echo $arr[3][3]['Id']?> /></td>
                        <td align="left" class="normalfnt"><?php echo $arr[3][3]['Name']?></td>
                        <td align="center" class="exlink">
           <input class="fnType validate[required]" type="radio" name="radio" id=<?php echo $arr[3][14]['Id']?> /></td>
                        <td align="left" class="normalfnt"><?php echo $arr[3][14]['Name']?></td>
                      </tr>
                      <tr>
                        <td align="center" bgcolor="#FFFFFF">
          <input class="fnType validate[required]" type="radio" name="radio" id=<?php echo $arr[3][4]['Id']?> /></td>
                        <td align="left" class="normalfnt"><?php echo $arr[3][4]['Name']?></td>
                        <td align="center" class="exlink">
          <input class="fnType validate[required]" type="radio" name="radio" id=<?php echo $arr[3][15]['Id']?> /></td>
                        <td align="left" class="normalfnt"><?php echo $arr[3][15]['Name']?></td>
                      </tr>
                      <tr>
                        <td align="center" bgcolor="#FFFFFF">
          <input class="fnType validate[required]" type="radio" name="radio" id=<?php echo $arr[3][5]['Id']?> /></td>
                        <td align="left" class="normalfnt"><?php echo $arr[3][5]['Name']?></td>
                        <td align="center" class="exlink">
          <input class="fnType validate[required]" type="radio" name="radio" id=<?php echo $arr[3][16]['Id']?> /></td>
                        <td align="left" class="normalfnt"><?php echo $arr[3][16]['Name']?></td>
                      </tr>
                      <tr>
                        <td align="center" bgcolor="#FFFFFF">
          <input class="fnType validate[required]" type="radio" name="radio" id=<?php echo $arr[3][6]['Id']?> /></td>
                        <td align="left" class="normalfnt"><?php echo $arr[3][6]['Name']?></td>
                        <td align="center" class="exlink">
          <input class="fnType validate[required]" type="radio" name="radio" id=<?php echo $arr[3][17]['Id']?> /></td>
                        <td align="left" class="normalfnt"><?php echo $arr[3][17]['Name']?></td>
                      </tr>
                      <tr>
                        <td align="center" bgcolor="#FFFFFF">
         <input class="fnType validate[required]" type="radio" name="radio" id=<?php echo $arr[3][7]['Id']?> /></td>
                        <td align="left" class="normalfnt"><?php echo $arr[3][7]['Name']?></td>
                        <td align="center" class="exlink">
         <input class="fnType validate[required]" type="radio" name="radio" id=<?php echo $arr[3][18]['Id']?> /></td>
                        <td align="left" class="normalfnt"><?php echo $arr[3][18]['Name']?></td>
                      </tr>
                      <tr>
                        <td align="center" bgcolor="#FFFFFF">
         <input class="fnType validate[required]" type="radio" name="radio" id=<?php echo $arr[3][8]['Id']?> /></td>
                        <td align="left" class="normalfnt"><?php echo $arr[3][8]['Name']?></td>
                        <td align="center" class="exlink">
         <input class="fnType validate[required]" type="radio" name="radio" id=<?php echo $arr[3][19]['Id']?> /></td>
                        <td align="left" class="normalfnt"><?php echo $arr[3][19]['Name']?></td>
                      </tr>
                      <tr>
                        <td align="center" bgcolor="#FFFFFF">
         <input class="fnType validate[required]" type="radio" name="radio" id=<?php echo $arr[3][9]['Id']?> /></td>
                        <td align="left" class="normalfnt"><?php echo $arr[3][9]['Name']?></td>
                        <td align="center" class="exlink">&nbsp;</td>
                        <td align="left" class="normalfnt">&nbsp;</td>
                      </tr>
                      <tr>
                        <td align="center" bgcolor="#FFFFFF">
         <input class="fnType validate[required]" type="radio" name="radio" id=<?php echo $arr[3][10]['Id']?> /></td>
                        <td align="left" class="normalfnt"><?php echo $arr[3][10]['Name']?></td>
                        <td align="center" class="exlink">&nbsp;</td>
                        <td align="left" class="normalfnt">&nbsp;</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td colspan="10" align="left" bgcolor="#E4E4E4" class="tableBorder"><span class="normalfnt"><strong>Type</strong> <strong><span class="compulsoryRed">*</span></strong></span></td>
                  </tr>
                    <tr>
                    <td colspan="10" align="left" bgcolor="#FFFFFF" class="tableBorder"><table width="100%">
                      <tr>
                          <td width="" height="23" align="right" class="normalfntRight"><strong class="normalfntMid"><u><strong class="normalfntMid"><u>
                                              <input  name="radio1" type="radio" class="type validate[required]" id="Heading" value="Heading" />
                                          </u></strong></u></strong></td>
                          <td width="" align="left" bgcolor="#FFFFFF"><span class="normalfntRight">Heading</span></td>
                          <td width="" align="center"><span class="normalfntMid">
                                  <input  name="radio1" type="radio" class="type validate[required]" id="Posting" value="Posting" />
                              </span></td>
                          <td width="" align="left"><span class="normalfntRight">Posting</span></td>
                          <td width="" align="right" bgcolor="#FFFFFF"><span class="normalfntRight">Account Header<strong><span class="compulsoryRed"> *</span></strong></span></td>
                        <td width="" align="center">
                            <span class="normalfntMid">
                                <select name="cmbHeaderAcc" class="validate[required]" id="cmbHeaderAcc" style="width:200px">
                                    <option value=""></option>
                                        <?php  
										$sql = "SELECT
										intId,
										strCode,
										strName
										FROM mst_financechartofaccounts WHERE strType='Heading'
										order by strCode
										";
										$result = $db->RunQuery($sql);
										while($row=mysqli_fetch_array($result))
										{
											echo "<option value=\"".$row['intId']."\">".$row['strCode']."-".$row['strName']."</option>";
										}
										?>
										</select> 
                              </span>
                        </td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td colspan="10" align="left" bgcolor="#FFFFFF" class="tableBorder"><table width="100%">
                      <tr>
                        <td width="23%" align="center" bgcolor="#E7FAFE" class="tableBorder_allRound"><span class="normalfntMid">Posting Level</span> <strong><u></u></strong></td>
                        <td width="7%" align="center" bgcolor="#FFFFFF">
                        <input class="posting" type="radio" name="radio3" id="Direct" value="Direct" /></td>
                        <td width="12%" align="left"><span class="normalfntRight">Direct</span></td>
                        <td width="8%" align="center">
                        <input class="posting" type="radio" name="radio3" id="Batch" value="Batch" /></td>
                        <td width="12%" align="left"><span class="normalfntRight">Batch</span></td>
                        <td width="7%" align="center">
                        <input type="checkbox" name="chkBlock" id="chkBlock" /></td>
                        <td width="31%" align="left"><span class="normalfntRight">Block</span></td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td colspan="10" align="left" bgcolor="#E4E4E4" class="tableBorder">
                    <span class="normalfnt"><strong>Consolidation</strong></span></td>
                  </tr>
                  <tr>
                    <td colspan="10" align="left" bgcolor="#FFFFFF" class="tableBorder"><table width="100%">
                      <tr>
                        <td width="31%" align="center" bgcolor="#E7FAFE" class="normalfntMid">Consolidation Debit A/C<strong><u></u></strong></td>
                        <td width="69%" align="left">
                   		<input name="txtComDebit" type="text" class="txtbox" id="txtComDebit" style="width:140px"/>
                       </td>
                        </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td colspan="10" align="left" bgcolor="#FFFFFF" class="tableBorder"><table width="100%">
                      <tr>
                     <td width="31%" align="center" bgcolor="#E7FAFE" class="normalfntMid">Consolidation Credit   A/C</td>
						<td width="69%" align="left">
						<input name="txtComCredid" type="text" class="txtbox" id="txtComCredid" style="width:140px"  />
                        </td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td colspan="10" align="left" bgcolor="#FFFFFF" class="tableBorder"><table width="100%">
                      <tr>
                        <td width="31%" rowspan="2" align="center" bgcolor="#E7FAFE" class="normalfntMid">Consolidation Transaction <strong><u></u></strong></td>
                        <td width="8%" align="center" bgcolor="#FFFFFF">
                    <input class="consolidation" type="radio" name="radio4" id="AverageRate" value="AverageRate" /></td>
                        <td width="18%" align="left" class="normalfnt">Average Rate</td>
                        <td width="8%" align="center">
                    <input class="consolidation" type="radio" name="radio4" id="ClosingRate" value="ClosingRate" /></td>
                      <td width="21%" align="left" class="normalfnt"><span class="normalfntRight">Closing Rate</span></td>
                        <td width="14%" align="left">&nbsp;</td>
                      </tr>
                      <tr>
                        <td align="center" bgcolor="#FFFFFF">
                <input class="consolidation" type="radio" name="radio4" id="HistoricalRate" value="HistoricalRate" /></td>
                        <td align="left" class="normalfnt">Historical Rate</td>
                        <td align="center">
                       <input class="consolidation" type="radio" name="radio4" id="EquityRate" value="EquityRate" /></td>
                        <td align="left" class="normalfnt">Equity Rate</td>
                        <td align="left">&nbsp;</td>
                      </tr>
                    </table></td>
                  </tr>
                  <tr>
                    <td colspan="10" align="left" bgcolor="#E4E4E4" class="tableBorder">
                    <span class="normalfnt"><strong>Reporting</strong></span></td>
                  </tr>
                  <tr>
                    <td colspan="10" align="left" bgcolor="#FFFFFF" class="tableBorder"><table width="100%">
                      <tr>
                        <td width="41%" align="center" bgcolor="#E7FAFE" class="tableBorder_allRound"><span class="normalfntMid">Exchange Rate Adjustments Direct</span> <strong></td>
                        <td width="9%" align="center">
                        <input class="adjustments" type="radio" name="radio6" id="NoAdjustments" value="NoAdjustments" /></td>
                        <td width="18%" align="left"><span class="normalfntRight">No Adjustments</span></td>
                        <td width="7%" align="center">
                        <input class="adjustments" type="radio" name="radio6" id="Adjustments" value="Adjustments" /></td>
                        <td width="25%" align="left"><span class="normalfntRight">Adjustments</span></td>
                        </tr>
                      </table></td>
                  </tr>
                    <tr>
                    <td colspan="10" align="left" bgcolor="#E4E4E4" class="tableBorder">
                    <span class="normalfnt"><strong>Company <span class="compulsoryRed">*</span></strong></span></td>
                  </tr>
                  <tr>
                  <td colspan="10">
                  <div id="divTable" style="overflow:scroll;width:100%;height:130px"  >
                  <table id="tblCompany" width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
                    <tr>
                      <td width="12%" height="19" bgcolor="#FAD163" class="normalfntMid"><strong>Select</strong></td>
                      <td width="27%" bgcolor="#FAD163" class="normalfntMid"><strong>Company Code</strong></td>
                      <td width="61%" bgcolor="#FAD163" class="normalfntMid"><strong>Company Name</strong></td>
                    </tr>
                    <?Php
						$sql = "SELECT
									mst_companies.intId,
									mst_companies.strCode,
									mst_companies.strName
									FROM
									mst_companies
									ORDER BY intId ASC ";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
						
					?>
                        <tr>
                        <td align="center" bgcolor="#FFFFFF"><input name="groupCompany" value="<?php echo $row['intId']; ?>"  type="checkbox" class="validate[minCheckbox[1]] company" id="chkCompany<?php echo $row['intId']; ?>" /></td>
                          <td bgcolor="#FFFFFF" class="normalfntMid"><?php echo $row['strCode']; ?></td>
                          <td bgcolor="#FFFFFF" class="normalfntMid"><?php echo $row['strName']; ?></td>
                        </tr>
                    <?php
					}
					?>
                  </table>
                </div>
                  </td>
                  </tr>
                  </table></td>
              </tr>
              </table></td>
            </tr>
          <tr>
            <td height="34"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
  <td width="100%" align="center" bgcolor=""><img style="display:none" border="0" src="../../../../../images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:none" border="0" src="../../../../../images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/><img style="display:none" border="0" src="../../../../../images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="25"/><a href="../../../../../main.php"><img  src="../../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                </tr>
              </table></td>
            </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
  </table>         
</div>
</div> 
</form>         
</body>
</html>
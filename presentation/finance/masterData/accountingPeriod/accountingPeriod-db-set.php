<?php 
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	$id 			= $_REQUEST['cboSearch'];
	$startingDate	= trim($_REQUEST['txtStartingDate']);
	$closingDate	= trim($_REQUEST['txtClosingDate']);
	$intStatus		= ($_REQUEST['chkActive']?1:0);	
	
	//------------------------------------------------------
		$companies = json_decode($_REQUEST['company'], true);
	//------------------------------------------------------
	
	/////////// accounting period insert part /////////////////////
	if($requestType=='add')
	{
		$sql = "INSERT INTO `mst_financeaccountingperiod` (`dtmStartingDate`,`dtmClosingDate`,`intStatus`,`intCreator`,dtmCreateDate) 
				VALUES ('$startingDate','$closingDate','$intStatus','$userId',now())";

		$periodId = $db->autoInsertNo($sql);
		if(count($companies) != 0 && $periodId != '')
		{
			foreach($companies as $company)
			{
				$companyId = $company['companyId'];
				$sql = "INSERT INTO `mst_financeaccountingperiod_companies`
						(`intPeriodId`,`intCompanyId`,`intCreator`,dtmCreateDate) 
					   VALUES 
						('$periodId','$companyId','$userId',now())";
				$result = $db->RunQuery($sql);
			}
		}
		if($result){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
	}
	/////////// accounting period update part /////////////////////
	else if($requestType=='edit')
	{
		$sql = "DELETE FROM `mst_financeaccountingperiod_companies` WHERE (`intPeriodId`='$id')";
		$db->RunQuery($sql);
		
		if(count($companies) != 0)
		{
			foreach($companies as $company)
			{
				$companyId = $company['companyId'];
				$sql = "INSERT INTO `mst_financeaccountingperiod_companies`
						(`intPeriodId`,`intCompanyId`,`intCreator`,dtmCreateDate) 
					   VALUES 
						('$id','$companyId','$userId',now())";
				$result = $db->RunQuery($sql);
			}
		}
		
		$sql = "UPDATE `mst_financeaccountingperiod` SET 	dtmStartingDate		='$startingDate',
															dtmClosingDate		='$closingDate',
															intStatus			='$intStatus',
															intModifyer			='$userId'
				WHERE (`intId`='$id')";
		$result = $db->RunQuery($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Updated successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	/////////// accounting period delete part /////////////////////
	else if($requestType=='delete')
	{
		$sql = "DELETE FROM `mst_financeaccountingperiod` WHERE (`intId`='$id')  ";
		$result = $db->RunQuery($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Deleted successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	echo json_encode($response);
?>
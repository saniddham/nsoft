var basePath	= "presentation/finance/masterData/accountingPeriod/";			
$(document).ready(function() {
		var id = '';
		var startingDate = '';
  		$("#frmAccountingPeriod").validationEngine();
		$('#frmAccountingPeriod #txtName').focus();
  //permision for add 
  /*if(intAddx)
  {
 	$('#frmAccountingPeriod #butNew').show();
	$('#frmAccountingPeriod #butSave').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmAccountingPeriod #butSave').show();
	$('#frmAccountingPeriod #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmAccountingPeriod #butDelete').show();
	$('#frmAccountingPeriod #cboSearch').removeAttr('disabled');
  }
  
  //permision for view
  if(intViewx)
  {
	$('#frmAccountingPeriod #cboSearch').removeAttr('disabled');
  }*/

$('#frmAccountingPeriod .loadId').css('cursor', 'pointer');
$('#frmAccountingPeriod .loadId').off('mouseover').on('mouseover',function () {
$(this).css('color', 'red');
});
$('#frmAccountingPeriod .loadId').off('mouseout').on('mouseout',function () {
$(this).css('color', 'black');
});

  ///save button click event
  $('#frmAccountingPeriod #butSave').off('click').on('click',function(){

 	if ($("#frmAccountingPeriod #txtClosingDate").val() <= $("#frmAccountingPeriod #txtStartingDate").val())
	{
		$('#frmAccountingPeriod #txtClosingDate').validationEngine('showPrompt', 'Please enter valid date range','fail');
		var t=setTimeout("alertx()",2000);
		return;
	}
		
		var companyValue="[ ";
		$('.company:checked').each(function(){
			companyValue += '{ "companyId":"'+$(this).val()+'"},';
		});
		companyValue = companyValue.substr(0,companyValue.length-1);
		companyValue += " ]";

	var requestType = '';
	if ($('#frmAccountingPeriod').validationEngine('validate'))   
    { 
		if(startingDate=='')
			requestType = 'add';
		else
			requestType = 'edit';
		
		var url = basePath+"accountingPeriod-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json",
			type:'post',  
			data:$("#frmAccountingPeriod").serialize()+'&requestType='+requestType+'&cboSearch='+id+'&company='+companyValue,
			async:false,
			
			success:function(json){
					$('#frmAccountingPeriod #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						id = '';
						startingDate = '';
						$('#frmAccountingPeriod').get(0).reset();
						var t=setTimeout("alertx()",1000);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					$('#frmAccountingPeriod #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
				}		
			});
	}
   });
   
   /////////////////////////////////////////////////////
   //// load accounting period details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmAccountingPeriod #tblMainGridRow').off('click').on('click',function(){
	   $('#frmAccountingPeriod').validationEngine('hide');
   });
   $('#frmAccountingPeriod #tblMainGrid .loadId').off('click').on('click',function() {
    	id = $(this).attr('id');
		$('#frmAccountingPeriod').validationEngine('hide');
		var url = basePath+"accountingPeriod-db-get.php";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:'requestType=loadDetails&id='+id,
			async:false,
			
			success:function(json){
					$('#frmAccountingPeriod #txtStartingDate').val(json.startingDate.substring(0,10));
					$('#frmAccountingPeriod #txtClosingDate').val(json.closingDate.substring(0,10));
					if(json.status)
						$('#frmAccountingPeriod #chkActive').attr('checked',true);
					else
						$('#frmAccountingPeriod #chkActive').removeAttr('checked');
						
				//--------------------------------------------------
						var companyId = "";
						if(json.comVal!=null){
						$('.company:checkbox').removeAttr('checked');	
						for(var i=0;i<=json.comVal.length-1;i++)
						{
							companyId = json.comVal[i].comId;
							$('#frmAccountingPeriod #chkCompany'+companyId).attr('checked',true);					
						}}
						else
						{
							$('.company:checkbox').removeAttr('checked');	
						}
				//--------------------------------------------------
			}
	}
	);
	startingDate = $('#frmAccountingPeriod #txtStartingDate').val();
	//////////// end of load details /////////////////
	});
	
	$('#frmAccountingPeriod #butNew').off('click').on('click',function(){
		$('#frmAccountingPeriod').get(0).reset();
		id = '';
		startingDate = '';
		$('#frmAccountingPeriod #txtName').focus();
		loadCombo_frmAccountingPeriod();
	});
    $('#frmAccountingPeriod #butDelete').off('click').on('click',function(){
		if(startingDate =='')
		{
			$('#frmAccountingPeriod #butDelete').validationEngine('showPrompt', 'Please select starting date.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+startingDate+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = basePath+"accountingPeriod-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											type: 'post',
											data:'requestType=delete&cboSearch='+id,
											async:false,
											success:function(json){
												$('#frmAccountingPeriod #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													var t=setTimeout("alertDelete()",1000);
													return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
					}
				}
		 	});
		}
	});
});
function loadCombo_frmAccountingPeriod()
{
	location.reload();
}
function alertx()
{
	$('#frmAccountingPeriod #butSave').validationEngine('hide')	;
	loadCombo_frmAccountingPeriod();
}
function alertDelete()
{
	$('#frmAccountingPeriod #butDelete').validationEngine('hide')	;
	loadCombo_frmAccountingPeriod();
}

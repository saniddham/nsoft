<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	
	/////////// accounting period load part /////////////////////
	if($requestType=='loadCombo')
	{
		$sql =  $sql = "SELECT
				intId,
				dtmStartingDate,
				dtmClosingDate,
				intStatus
				FROM mst_financeaccountingperiod";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['startingDate'] 	= $row['dtmStartingDate'];
			$response['closingDate'] 	= $row['dtmClosingDate'];
			$response['status'] 		= ($row['intStatus']?true:false);
		}
		echo json_encode($response);
	}
	else if($requestType=='loadDetails')
	{
		$id  = $_REQUEST['id'];
		
		//------------------------------
		$sql = "SELECT intCompanyId
				FROM mst_financeaccountingperiod_companies
				WHERE
					intPeriodId =  '$id'";
		$result = $db->RunQuery($sql);
	
		$arrCom;
		while($row=mysqli_fetch_array($result))
		{
			$val['comId'] 	= $row['intCompanyId'];
			$arrCom[] = $val;
		}
		
		$response['comVal'] = $arrCom;
		//------------------------------
		
		$sql =  $sql = "SELECT
				intId,
				dtmStartingDate,
				dtmClosingDate,
				intStatus
				FROM mst_financeaccountingperiod
				WHERE
					intId =  '$id'
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['startingDate'] 	= $row['dtmStartingDate'];
			$response['closingDate'] 	= $row['dtmClosingDate'];
			$response['status'] 		= ($row['intStatus']?true:false);
		}
		echo json_encode($response);
	}
	
?>
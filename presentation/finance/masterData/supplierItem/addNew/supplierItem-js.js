function functionList()
{
	if(supItmId!='')
	{
		$('#frmSupplierItem #cboSearch').val(supItmId);
		$('#frmSupplierItem #cboSearch').change();
	}
}		
$(document).ready(function() {
  		$("#frmSupplierItem").validationEngine();
		$('#frmSupplierItem #txtName').focus();
  //permision for add 
  if(intAddx)
  {
 	$('#frmSupplierItem #butNew').show();
	$('#frmSupplierItem #butSave').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmSupplierItem #butSave').show();
	$('#frmSupplierItem #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmSupplierItem #butDelete').show();
	$('#frmSupplierItem #cboSearch').removeAttr('disabled');
  }
  
  //permision for view
  if(intViewx)
  {
	$('#frmSupplierItem #cboSearch').removeAttr('disabled');
  }
  
/*  $(':input').bind('keyup mouseup', function(){
      $(this).val($.trim($(this).val())) ;
    });*/

  ///save button click event
  $('#frmSupplierItem #butSave').click(function(){
	//$('#frmSupplierItem').submit();
	var requestType = '';
	if ($('#frmSupplierItem').validationEngine('validate'))   
    { 
		//$('#txtLocationCode').validationEngine('showPrompt', 'This is an example', 'pass'); //hide all alerts//
		if($("#frmSupplierItem #cboSearch").val()=='')
			requestType = 'add';
		else
			requestType = 'edit';
		
		var url = "supplierItem-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json",  
			data:$("#frmSupplierItem").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addLocation"}',
			async:false,
			
			success:function(json){
					$('#frmSupplierItem #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmSupplierItem').get(0).reset();
						loadCombo_frmSupplierItem();
						var t=setTimeout("alertx()",1000);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmSupplierItem #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
   
   /////////////////////////////////////////////////////
   //// load supplier item details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmSupplierItem #cboSearch').click(function(){
	   $('#frmSupplierItem').validationEngine('hide');
   });
    $('#frmSupplierItem #cboSearch').change(function(){
		$('#frmSupplierItem').validationEngine('hide');
		var url = "supplierItem-db-get.php";
		if($('#frmSupplierItem #cboSearch').val()=='')
		{
			$('#frmSupplierItem').get(0).reset();return;	
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:'requestType=loadDetails&id='+$(this).val(),
			async:false,
			success:function(json){
					//json  = eval('('+json+')');
					$('#frmSupplierItem #txtName').val(json.name);
					$('#frmSupplierItem #txtRemark').val(json.remark);
					$('#frmSupplierItem #cboChartOfAcc').val(json.chartofaccount);
					$('#frmSupplierItem #chkActive').attr('checked',json.status);
			}
	});
	//////////// end of load details /////////////////
	
	});
	
	$('#frmSupplierItem #butNew').click(function(){
		$('#frmSupplierItem').get(0).reset();
		loadCombo_frmSupplierItem();
		$('#frmSupplierItem #txtName').focus();
	});
    $('#frmSupplierItem #butDelete').click(function(){
		if($('#frmSupplierItem #cboSearch').val()=='')
		{
			$('#frmSupplierItem #butDelete').validationEngine('showPrompt', 'Please select Item .', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmSupplierItem #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = "supplierItem-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											data:'requestType=delete&cboSearch='+$('#frmSupplierItem #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmSupplierItem #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													$('#frmSupplierItem').get(0).reset();
													loadCombo_frmSupplierItem();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
});


function loadCombo_frmSupplierItem()
{
	var url 	= "supplierItem-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmSupplierItem #cboSearch').html(httpobj.responseText);
}

function alertx()
{
	$('#frmSupplierItem #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmSupplierItem #butDelete').validationEngine('hide')	;
}

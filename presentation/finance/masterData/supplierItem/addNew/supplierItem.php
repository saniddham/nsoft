<?php
session_start();
$backwardseperator = "../../../../../";
$mainPath = $_SESSION['mainPath'];

$thisFilePath =  $_SERVER['PHP_SELF'];

include  "{$backwardseperator}dataAccess/permisionCheck.inc";
$supItmId = $_REQUEST['id'];
?>
<script type="application/javascript" >
var supItmId = <?php echo $supItmId ?>;
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Supplier Item</title>

<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../../css/promt.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="supplierItem-js.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" href="../../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../../libraries/validate/template.css" type="text/css">

</head>

<body onLoad="functionList();">
<form id="frmSupplierItem" name="frmSupplierItem" method="post" action="supplierItem-db-set.php" autocomplete="off">
<table width="100%" border="0" id="header" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<script src="../../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<div align="center">
		<div class="trans_layoutD">
		  <div class="trans_text">Supplier Item</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="585" border="0" align="center">
      <tr>
        <td width="62%"><table width="100%" border="0" > 
          <tr>
            <td height="47" ><table width="100%" border="0" class="">
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Item Name</td>
                <td colspan="3">
                <select name="cboSearch" class="txtbox" id="cboSearch" style="width:250px" tabindex="1">
                   <option value=""></option>
                 <?php  $sql = "SELECT
									intId,
									strName
								FROM mst_financesupplieritem
								order by strName
								";
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
								}
                   ?> 
                  </select></td>
              </tr>
              <tr>
                <td width="88" class="normalfnt">&nbsp;</td>
                <td width="123" class="normalfnt">&nbsp;</td>
                <td colspan="3">&nbsp;</td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Item Name&nbsp;<span class="compulsoryRed">*</span></td>
                <td colspan="3"><input name="txtName" type="text" class="validate[required,maxSize[50]]" id="txtName" style="width:250px" maxlength="50" tabindex="3"/></td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Description</td>
                <td><textarea name="txtRemark" style="width:250px" class="validate[maxSize[250]]"  rows="2" id="txtRemark"  tabindex="3"></textarea></td>
                <td class="normalfnt">&nbsp;</td>
                <td width="35">&nbsp;</td>
              </tr>
              <tr style="display:none">
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Account <span class="compulsoryRed">*</span></td>
                <td><span class="normalfntMid">
                  <select name="cboChartOfAcc" id="cboChartOfAcc" style="width:250px" class="validate[required]">
                    <option value=""></option>
                    <?php  $sql = "SELECT
						intId,
						strCode,
						strName
						FROM mst_financechartofaccounts
						WHERE
						intStatus = '1' AND strType = 'Posting' AND intFinancialTypeId <> '10' AND intFinancialTypeId <> '18'
						order by strCode
						"; 
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strCode']."-".$row['strName']."</option>";
						}
        ?>
                  </select>
                </span></td>
                <td class="normalfnt">&nbsp;</td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Active</td>
             <td width="250"><input type="checkbox" name="chkActive" id="chkActive" checked="checked" tabindex="24"/></td>
                <td width="55" class="normalfnt">&nbsp;</td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">&nbsp;</td>
                <td>&nbsp;</td>
                <td colspan="2"></td>
                </tr>
              </table></td>
            </tr>
          <tr>
            <td height="34"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td width="100%" align="center" bgcolor=""><img style="display:none" border="0" src="../../../../../images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:none" border="0" src="../../../../../images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/><img style="display:none" border="0" src="../../../../../images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="25"/><a href="../../../../../main.php"><img  src="../../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                </tr>
              </table></td>
            </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
  </table>
	</div>
  </div>
</form>
</body>
</html>

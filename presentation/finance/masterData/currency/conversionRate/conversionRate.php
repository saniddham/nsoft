<?php
session_start();
	$backwardseperator = "../../../../../";
	
	$thisFilePath =  $_SERVER['PHP_SELF'];
	
	$companyId 	= $_SESSION['headCompanyId'];

	include  "{$backwardseperator}dataAccess/permisionCheck.inc";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Currency Conversion Rate</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../../../../libraries/calendar/theme.css" />

<script type="application/javascript" src="../../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../../libraries/jquery/jquery-ui.js"></script>

<script src="../../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>


<script type="application/javascript">
$(document).ready(function(){

$("#cboSearchFrom").change(function()
{
	if($("#cboSearchFrom").val() != "" && $("#cboSearchTo").val() != "")
	{
		var value1 = $("#cboSearchFrom").val();
		var value2 = $("#cboSearchTo").val();
		var rate = eval(value1)/eval(value2)
		$("#txtRate").val(rate);
	}
	else
	{
		$("#txtRate").val('');
	}
});

$("#cboSearchTo").change(function()
{
	if($("#cboSearchFrom").val() != "" && $("#cboSearchTo").val() != "")
	{
		var value1 = $("#cboSearchFrom").val();
		var value2 = $("#cboSearchTo").val();
		var rate = eval(value1)/eval(value2)
		$("#txtRate").val(rate);
	}
	else
	{
		$("#txtRate").val('');
	}
});
});
</script>
<script type="application/javascript">
function loadComboCurrency()
{
	document.getElementById('txtRate').value = '';
	var selectDate = document.getElementById('adviceDate').value;
	var url 	= "conversionRate-db-get.php?requestType=loadCombo&date="+selectDate;
	var httpobj = $.ajax({url:url,async:false})
	$('#cboSearchFrom').html(httpobj.responseText);
	$('#cboSearchTo').html(httpobj.responseText);
}
</script>
</head>

<body>
<form id="frmConversionRate" name="frmConversionRate" method="post" autocomplete="off" action="conversionRate.php">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<div align="center">
  <div class="trans_layoutD">
	<div class="trans_text">Currency Conversion Rate</div>
<table width="100%">
<tr>
  <td width="40%" align="right" class="normalfntRight">Date</td>
  <td width="21%" bgcolor="#FFFFFF"><input name="adviceDateFrom" type="text" value="<?php echo date("Y-m-d"); ?>" class="txtbox" id="adviceDate" style="width:98px;" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;" onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
  <td width="39%" bgcolor="#FFFFFF"><a href="#">
  <img src="../../../../../images/search.png" width="20" height="20" onclick="loadComboCurrency();" /></a></td>
  </tr>
<tr>
  <td colspan="3">
    <table width="98%" id="tblMainGrid2" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
      <tr class="">
        <td width="34%" height="27"    bgcolor="#FAD163" class="normalfntMid"><span class="normalfnt"><strong>Currency</strong></span></td>
        <td  bgcolor="#FAD163" class="normalfntMid"  ><strong>To Currency</strong></td>
        <td  bgcolor="#FAD163" class="normalfntMid"  ><strong>Conversion Rate</strong></td>
        </tr>
      <tr class="normalfnt">
        <td align="center" bgcolor="#FFFFFF"><select name="cboSearchFrom" id="cboSearchFrom" style="width:200px" class="validate[required]">
          <option value=""></option>
          <?php  
		  					$dateValue = date("Y-m-d");
							$sql = "SELECT
							mst_financecurrency.intId,
							mst_financecurrency.strCode,
							mst_financecurrency.strSymbol,
							mst_financeexchangerate.intCurrencyId,
							mst_financeexchangerate.dtmDate,
							mst_financeexchangerate.dblSellingRate,
							mst_financeexchangerate.dblBuying
							FROM
							mst_financecurrency
							Inner Join mst_financeexchangerate ON mst_financeexchangerate.intCurrencyId = mst_financecurrency.intId WHERE
							mst_financeexchangerate.dtmDate = '$dateValue' AND mst_financeexchangerate.intCompanyId =  '$companyId';";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['dblBuying']."\">".$row['strCode']."</option>";
						}
        				?>
        </select></td>
        <td width="34%" align="center" bgcolor="#FFFFFF" class=""><select name="cboSearchTo" id="cboSearchTo" style="width:200px" class="validate[required]">
          <option value=""></option>
          <?php  			
		  					$dateValue = date("Y-m-d");
		  					$sql = "SELECT
							mst_financecurrency.intId,
							mst_financecurrency.strCode,
							mst_financecurrency.strSymbol,
							mst_financeexchangerate.intCurrencyId,
							mst_financeexchangerate.dtmDate,
							mst_financeexchangerate.dblSellingRate,
							mst_financeexchangerate.dblBuying
							FROM
							mst_financecurrency
							Inner Join mst_financeexchangerate ON mst_financeexchangerate.intCurrencyId = mst_financecurrency.intId WHERE
							mst_financeexchangerate.dtmDate = '$dateValue' AND mst_financeexchangerate.intCompanyId =  '$companyId'";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['dblBuying']."\">".$row['strCode']."</option>";
						}
        				?>
        </select></td>
        <td width="32%" align="center" bgcolor="#FFFFFF" class="">
        <input name="txtRate" type="text" disabled="disabled" id="txtRate" style="width:180px" /></td>
        </tr>
      </table>
</td>
</tr>
<tr>
  <td align="center">&nbsp;</td>
  <td align="center">&nbsp;</td>
  <td align="center">&nbsp;</td>
</tr>
<tr>
  <td colspan="3" align="center"><a href="../../../../../main.php"><img  src="../../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose"/></a></td>
  </tr>
</table>
</div>
</div>
</form>   
</body>
</html>
var basePath	= "presentation/finance/masterData/currency/addNew/";
			
$(document).ready(function() {
	$("#frmCurrency").validationEngine();
	$('#frmCurrency #txtCode').focus();
	
	
	if(cId!='')
	{
		$('#frmCurrency #cboSearch').val(cId);
		loadData();
	}
		
  //permision for add 
  /*if(intAddx)
  {
 	$('#frmCurrency #butNew').show();
	$('#frmCurrency #butSave').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmCurrency #butSave').show();
	$('#frmCurrency #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmCurrency #butDelete').show();
	$('#frmCurrency #cboSearch').removeAttr('disabled');
  }
  
  //permision for view
  if(intViewx)
  {
	$('#frmCurrency #cboSearch').removeAttr('disabled');
  }*/
  
/*  $(':input').bind('keyup mouseup', function(){
      $(this).val($.trim($(this).val())) ;
    });*/

  ///save button click event
  $('#frmCurrency #butSave').off('click').on('click',function(){
	//$('#frmCurrency').submit();
	var requestType = '';
	if ($('#frmCurrency').validationEngine('validate'))   
    { 
		//$('#txtLocationCode').validationEngine('showPrompt', 'This is an example', 'pass'); //hide all alerts//
		if($("#frmCurrency #cboSearch").val()=='')
			requestType = 'add';
		else
			requestType = 'edit';
		
		var url = basePath+"currency-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json",
			type: "post",  
			data:$("#frmCurrency").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addLocation"}',
			async:false,
			
			success:function(json){
					$('#frmCurrency #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmCurrency').get(0).reset();
						loadCombo_frmCurrency();
						var t=setTimeout("alertx()",1000);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmCurrency #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
   
   /////////////////////////////////////////////////////
   //// load currency details //////////////////////////
   /////////////////////////////////////////////////////
	$('#frmCurrency #cboSearch').off('click').on('click',function(){
		
		$('#frmCurrency').validationEngine('hide');
	});
	
	$('#frmCurrency #cboSearch').off('change').on('change',function(){
	
		loadData()
	});
	
	$('#frmCurrency #butNew').off('click').on('click',function(){
		$('#frmCurrency').get(0).reset();
		loadCombo_frmCurrency();
		$('#frmCurrency #txtCode').focus();
	});
    $('#frmCurrency #butDelete').off('click').on('click',function(){
		if($('#frmCurrency #cboSearch').val()=='')
		{
			$('#frmCurrency #butDelete').validationEngine('showPrompt', 'Please select Currency.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmCurrency #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = basePath+"currency-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											type: 'post',
											data:'requestType=delete&cboSearch='+$('#frmCurrency #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmCurrency #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													$('#frmCurrency').get(0).reset();
													loadCombo_frmCurrency();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
});
function loadData()
{
	$('#frmCurrency').validationEngine('hide');
	
	var url = basePath+"currency-db-get.php";
	if($('#frmCurrency #cboSearch').val()=='')
	{
		$('#frmCurrency').get(0).reset();return;	
	}
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		data:'requestType=loadDetails&id='+$('#frmCurrency #cboSearch').val(),
		async:false,
		success:function(json){
				//json  = eval('('+json+')');
				$('#frmCurrency #txtCode').val(json.code);
				$('#frmCurrency #txtSymbol').val(json.symbol);
				$('#frmCurrency #txtDesc').val(json.description);
				$('#frmCurrency #cboChartOfAcc').val(json.chartofaccount);
				if(json.status)
					$('#frmCurrency #chkActive').attr('checked',true);
				else
					$('#frmCurrency #chkActive').removeAttr('checked');
		}
	});
}
function loadCombo_frmCurrency()
{
	var url 	= basePath+"currency-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmCurrency #cboSearch').html(httpobj.responseText);
}

function alertx()
{
	$('#frmCurrency #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmCurrency #butDelete').validationEngine('hide')	;
}

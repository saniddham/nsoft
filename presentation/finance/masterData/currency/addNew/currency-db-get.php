<?php
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	
	/////////// currency load part /////////////////////
	if($requestType=='loadCombo')
	{
		$sql = "SELECT
					intId,
					strCode
				FROM mst_financecurrency
				order by strCode
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strCode']."</option>";
		}
		echo $html;
	}
	else if($requestType=='loadDetails')
	{
		$id  = $_REQUEST['id'];
		$sql = "SELECT
					strCode,
					strSymbol,
					strDescription,
					intChartOfAccountId,
					intStatus
				FROM mst_financecurrency
				WHERE
					intId =  '$id'
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['code'] 			= $row['strCode'];
			$response['symbol'] 		= $row['strSymbol'];
			$response['description'] 	= $row['strDescription'];
			$response['chartofaccount'] = $row['intChartOfAccountId'];
			$response['status'] 		= ($row['intStatus']?true:false);
		}
		echo json_encode($response);
	}
	
?>
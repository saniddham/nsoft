<?php 
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	$id 			= $_REQUEST['cboSearch'];
	$code			= trim($_REQUEST['txtCode']);
	$symbol			= trim($_REQUEST['txtSymbol']);
	$description	= trim($_REQUEST['txtDesc']);
	$chartofaccount	= null($_REQUEST['cboChartOfAcc']);
	$intStatus		= ($_REQUEST['chkActive']?1:0);	
	
	/////////// currency insert part /////////////////////
	if($requestType=='add')
	{
		 $sql = "INSERT INTO `mst_financecurrency` (`strCode`,`strSymbol`,`strDescription`,`intChartOfAccountId`,`intStatus`,`intCreator`,dtmCreateDate) 
				VALUES ('$code','$symbol','$description',$chartofaccount,'$intStatus','$userId',now())";
		$result = $db->RunQuery($sql);
		if($result){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
	}
	/////////// currency update part /////////////////////
	else if($requestType=='edit')
	{
		$sql = "UPDATE `mst_financecurrency` SET 	strCode		='$code',
											strSymbol			='$symbol',
											strDescription		='$description',
											intChartOfAccountId	=$chartofaccount,
											intStatus			='$intStatus',
											intModifyer			='$userId'
				WHERE (`intId`='$id')";
		$result = $db->RunQuery($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Updated successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	/////////// currency delete part /////////////////////
	else if($requestType=='delete')
	{
		$sql = "DELETE FROM `mst_financecurrency` WHERE (`intId`='$id')  ";
		$result = $db->RunQuery($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Deleted successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	echo json_encode($response);
?>
<?php
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	
	/////////// customer load part /////////////////////
	if($requestType=='loadCombo')
	{
		$sql = "SELECT
					intId,
					strName
				FROM mst_finance_service_customer
				order by strName
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
	}
	else if($requestType=='loadDetails')
	{
		$id  = $_REQUEST['id'];
		
		//------------------------------
		$sql = "SELECT intBrandId
				FROM mst_finance_service_customer_brand
				WHERE
					intCustomerId =  '$id'";
		$result = $db->RunQuery($sql);
	
		$arrBrn;
		while($row=mysqli_fetch_array($result))
		{
			$val1['brandId'] 	= $row['intBrandId'];
			$arrBrn[] = $val1;
		}
		
		$response['brnVal'] = $arrBrn;
		//------------------------------
		
		//------------------------------
		$sql = "SELECT intLocationId
				FROM mst_finance_service_customer_locations
				WHERE
					intCustomerId =  '$id'";
		$result = $db->RunQuery($sql);
	
		$arrlocat;
		while($row=mysqli_fetch_array($result))
		{
			$val2['locateId'] 	= $row['intLocationId'];
			$arrlocat[] = $val2;
		}
		
		$response['locatVal'] = $arrlocat;
		//-------------------------------
		
		$sql = "SELECT
					strCode,
					strName,
					intTypeId,
					strAddress,
					strContactPerson,
					strCity,
					intCountryId,
					intCurrencyId,
					strPhoneNo,
					strMobileNo,
					strFaxNo,
					strEmail,
					strWebSite,
					intShipmentId,
					strVatNo,
					strSVatNo,
					strRegistrationNo,
					strInvoiceType,
					intPaymentsTermsId,
					intPaymentsMethodsId,
					intCreditLimit,
					intChartOfAccountId,
					strBlocked,
					intRank,
					intStatus
				FROM mst_finance_service_customer
				WHERE
					intId =  '$id'
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['code'] 			= $row['strCode'];
			$response['name'] 			= $row['strName'];
			$response['type'] 			= $row['intTypeId'];
			$response['address'] 		= $row['strAddress'];
			$response['contact'] 		= $row['strContactPerson'];
			$response['city'] 			= $row['strCity'];
			$response['country'] 		= $row['intCountryId'];
			$response['currency'] 		= $row['intCurrencyId'];
			$response['phone'] 			= $row['strPhoneNo'];
			$response['mobile'] 		= $row['strMobileNo'];
			$response['fax'] 			= $row['strFaxNo'];
			$response['email'] 			= $row['strEmail'];
			$response['web'] 			= $row['strWebSite'];
			$response['shipment'] 		= $row['intShipmentId'];
			$response['vatNo'] 			= $row['strVatNo'];
			$response['sVatNo'] 		= $row['strSVatNo'];
			$response['regNo'] 			= $row['strRegistrationNo'];
			$response['invoType'] 		= $row['strInvoiceType'];
			$response['payterms'] 		= $row['intPaymentsTermsId'];
			$response['paymethods'] 	= $row['intPaymentsMethodsId'];
			$response['creditlimit'] 	= $row['intCreditLimit'];
			$response['chartofaccount'] = $row['intChartOfAccountId'];
			$response['blocked'] 		= $row['strBlocked'];
			$response['rank'] 			= $row['intRank'];
			$response['status'] 		= ($row['intStatus']?true:false);
		}
		echo json_encode($response);
	}
	
?>
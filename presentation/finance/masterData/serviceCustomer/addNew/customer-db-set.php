<?php 
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 		= $_REQUEST['requestType'];
	$id 				= $_REQUEST['cboSearch'];
	$code				= trim($_REQUEST['txtCode']);
	$name				= trim($_REQUEST['txtName']);
	$type				= ($_REQUEST['cboType']==''?'NULL':$_REQUEST['cboType']);
	$address			= $_REQUEST['txtAddress'];
	$contact			= $_REQUEST['txtContact'];
	$city				= trim($_REQUEST['txtCity']);
	$country			= ($_REQUEST['cboCountry']==''?'NULL':$_REQUEST['cboCountry']);
	$currency			= ($_REQUEST['cboCurrency']==''?'NULL':$_REQUEST['cboCurrency']);
	$phone				= $_REQUEST['txtPhone'];
	$mobile				= $_REQUEST['txtMobile'];
	$fax				= $_REQUEST['txtFax'];
	$email				= trim($_REQUEST['txtEMail']);
	$web				= $_REQUEST['txtWeb'];
	$shipment 			= ($_REQUEST['cboShipmentMethod']==''?'NULL':$_REQUEST['cboShipmentMethod']);
	$vatNo				= $_REQUEST['txtVatNo'];
	$sVatNo				= $_REQUEST['txtSvatNo'];
	$regNo				= $_REQUEST['txtRegNo'];
	$invoType			= $_REQUEST['invoType'];
	$paymentsterms 		= ($_REQUEST['cboPaymentsTerms']==''?'NULL':$_REQUEST['cboPaymentsTerms']);
	$paymentsmethods 	= ($_REQUEST['cboPaymentsMethods']==''?'NULL':$_REQUEST['cboPaymentsMethods']);
	$creditlimit 		= val($_REQUEST['txtCreditLimit']);
	$chartofaccount		= ($_REQUEST['cboChartOfAcc']==''?'NULL':$_REQUEST['cboChartOfAcc']);
	$blocked 			= $_REQUEST['cboBlocked'];
	$rank 				= val($_REQUEST['txtRank']);
	$intStatus		= ($_REQUEST['chkActive']?1:0);
	///////// customer insert part /////////////////////
	if($requestType=='add')
	{	
		$sql = "INSERT INTO `mst_finance_service_customer` 					(`strCode`,`strName`,`intTypeId`,`strAddress`,`strContactPerson`,`strCity`,`intCountryId`,`intCurrencyId`,`strPhoneNo`,`strMobileNo`,`strFaxNo`,`strEmail`,`strWebSite`,`intShipmentId`,`strVatNo`,`strSVatNo`,`strRegistrationNo`,`strInvoiceType`,`intPaymentsTermsId`,`intPaymentsMethodsId`,`intCreditLimit`,`intChartOfAccountId`,`strBlocked`,`intRank`,`intStatus`,`intCreator`,`dtmCreateDate`) 
				VALUES ('$code','$name',$type,'$address','$contact','$city',$country,$currency,'$phone','$mobile','$fax','$email','$web',$shipment,'$vatNo','$sVatNo','$regNo','$invoType',$paymentsterms,$paymentsmethods,'$creditlimit',$chartofaccount,'$blocked','$rank','$intStatus','$userId',now())";	
				
		$result = $db->RunQuery($sql);
	
		if($result){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
	}
	/////////// customer update part /////////////////////
	else if($requestType=='edit')
	{		
		
		$sql = "UPDATE `mst_finance_service_customer` SET 	strCode	='$code',
											strName					= '$name',
											intTypeId				= $type,
											strAddress				= '$address',
											strContactPerson		= '$contact',
											strCity					= '$city',
											intCountryId			= $country,
											intCurrencyId			= $currency,
											strPhoneNo				= '$phone',
											strMobileNo				= '$mobile',
											strFaxNo				= '$fax',
											strEmail				= '$email',
											strWebSite				= '$web',
											intShipmentId			= $shipment,
											strVatNo				= '$vatNo',
											strSVatNo				= '$sVatNo',
											strRegistrationNo		= '$regNo',
											strInvoiceType			= '$invoType',
											intPaymentsTermsId		= $paymentsterms,
											intPaymentsMethodsId	= $paymentsmethods,
											intCreditLimit			= '$creditlimit',
											intChartOfAccountId		= $chartofaccount,
											strBlocked				= '$blocked',
											intRank					= '$rank',
											intStatus				= '$intStatus',
											intModifyer				= '$userId'
				WHERE (`intId`='$id')";
		$result = $db->RunQuery($sql);
		
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Updated successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	/////////// customer delete part /////////////////////
	else if($requestType=='delete')
	{	
		$sql = "DELETE FROM `mst_finance_service_customer` WHERE (`intId`='$id')  ";
		$result = $db->RunQuery($sql);
		if(($result)){
			
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Deleted successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	echo json_encode($response);
?>
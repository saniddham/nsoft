
var arrLocate = [];
var arrBrand = [];

var invoType = "";
function functionList()
{
    if(cusId!='')
    {
        $('#frmServiceCustomer #cboSearch').val(cusId);
        $('#frmServiceCustomer #cboSearch').change();
    }
}
$(document).ready(function() {
  		$("#frmServiceCustomer").validationEngine();
		$('#frmServiceCustomer #txtCode').focus();
  //permision for add 
  if(intAddx)
  {
 	$('#frmServiceCustomer #butNew').show();
	$('#frmServiceCustomer #butSave').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmServiceCustomer #butSave').show();
	$('#frmServiceCustomer #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmServiceCustomer #butDelete').show();
	$('#frmServiceCustomer #cboSearch').removeAttr('disabled');
  }
  
  //permision for view
  if(intViewx)
  {
	$('#frmServiceCustomer #cboSearch').removeAttr('disabled');
  }
  
/*  $(':input').bind('keyup mouseup', function(){
      $(this).val($.trim($(this).val())) ;
    });*/

  ///save button click event
  $('#frmServiceCustomer #butSave').click(function(){
		
	var requestType = '';
	if ($('#frmServiceCustomer').validationEngine('validate'))   
    { 
		//$('#txtLocationCode').validationEngine('showPrompt', 'This is an example', 'pass'); //hide all alerts//
		if($("#frmServiceCustomer #cboSearch").val()=='')
			requestType = 'add';
		else
			requestType = 'edit';
		
		var url = "customer-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json",  
		data:$("#frmServiceCustomer").serialize()+'&requestType='+requestType+'&invoType='+invoType,
			//data:'{"requestType":"addLocation"}',
			async:false,
			
			success:function(json){
					$('#frmServiceCustomer #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmServiceCustomer').get(0).reset();
						loadCombo_frmServiceCustomer();
						var t=setTimeout("alertx()",1000);
						$('#txtCode').val(json.nxtNo);
						//location.reload(true);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmServiceCustomer #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
   
   /////////////////////////////////////////////////////
   //// load customer details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmServiceCustomer #cboSearch').click(function(){
	   $('#frmServiceCustomer').validationEngine('hide');
   });
    $('#frmServiceCustomer #cboSearch').change(function(){
		$('#frmServiceCustomer').validationEngine('hide');
		var url = "customer-db-get.php";
		if($('#frmServiceCustomer #cboSearch').val()=='')
		{
			$('#frmServiceCustomer').get(0).reset();return;	
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:'requestType=loadDetails&id='+$(this).val(),
			async:false,
			success:function(json){
					//json  = eval('('+json+')');
					
					$('#frmServiceCustomer #txtCode').val(json.code);
					$('#frmServiceCustomer #txtName').val(json.name);
					$('#frmServiceCustomer #cboType').val(json.type);
					$('#frmServiceCustomer #txtAddress').val(json.address);
					$('#frmServiceCustomer #txtContact').val(json.contact);
					$('#frmServiceCustomer #txtCity').val(json.city);
					$('#frmServiceCustomer #cboCountry').val(json.country);
					$('#frmServiceCustomer #cboCurrency').val(json.currency);
					$('#frmServiceCustomer #txtPhone').val(json.phone);
					
					$('#frmServiceCustomer #txtMobile').val(json.mobile);
					$('#frmServiceCustomer #txtFax').val(json.fax);
					$('#frmServiceCustomer #txtEMail').val(json.email);
					$('#frmServiceCustomer #txtWeb').val(json.web);
					$('#frmServiceCustomer #cboShipmentMethod').val(json.shipment);
					$('#frmServiceCustomer #txtVatNo').val(json.vatNo);
					$('#frmServiceCustomer #txtSvatNo').val(json.sVatNo);
					$('#frmServiceCustomer #txtRegNo').val(json.regNo);
					if(json.invoType=='')
						json.invoType='Tax';
					$('#frmServiceCustomer #'+json.invoType).attr('checked',true); //
					
					invoType = json.invoType;	
					
					$('#frmServiceCustomer #cboPaymentsTerms').val(json.payterms);
					$('#frmServiceCustomer #cboPaymentsMethods').val(json.paymethods);
					$('#frmServiceCustomer #txtCreditLimit').val(json.creditlimit);
					$('#frmServiceCustomer #cboChartOfAcc').val(json.chartofaccount);
					$('#frmServiceCustomer #cboBlocked').val(json.blocked);
					$('#frmServiceCustomer #txtRank').val(json.rank);
					if(json.status)
						$('#frmServiceCustomer #chkActive').attr('checked',true);
					else
						$('#frmServiceCustomer #chkActive').removeAttr('checked');
				
				//alert(json.locatVal.length);
			}
	});
	//////////// end of load details /////////////////
	
	});
	
	$('#frmServiceCustomer #butNew').click(function(){
		$('#frmServiceCustomer').get(0).reset();
		location.reload(true);
		loadCombo_frmServiceCustomer();
		$('#frmServiceCustomer #txtCode').focus();
	});
    $('#frmServiceCustomer #butDelete').click(function(){
		if($('#frmServiceCustomer #cboSearch').val()=='')
		{
			$('#frmServiceCustomer #butDelete').validationEngine('showPrompt', 'Please select Customer.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmServiceCustomer #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = "customer-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											data:'requestType=delete&cboSearch='+$('#frmServiceCustomer #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmServiceCustomer #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													$('#frmServiceCustomer').get(0).reset();
													loadCombo_frmServiceCustomer();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
	$('.invTpe').click(function(){
		invoType = $(this).val();	
	});
});


function loadCombo_frmServiceCustomer()
{
	var url 	= "customer-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmServiceCustomer #cboSearch').html(httpobj.responseText);
}

function alertx()
{
	$('#frmServiceCustomer #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmServiceCustomer #butDelete').validationEngine('hide')	;
}
<?php
session_start();
$backwardseperator = "../../../../../";
$thisFilePath =  $_SERVER['PHP_SELF'];
include  "{$backwardseperator}dataAccess/permisionCheck.inc";

$cusId = $_REQUEST['id'];
?>
<script type="application/javascript" >
var cusId = '<?php echo $cusId ?>';
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Customer Details</title>

<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../../css/promt.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="customer-js.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" href="../../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../../libraries/validate/template.css" type="text/css">

</head>

<body onLoad="functionList();">
<table id="header" width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<script src="../../../../../libraries/validate//jquery-1.js" type="text/javascript"></script>
<script src="../../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<form id="frmServiceCustomer" name="frmServiceCustomer" method="post" action="customer-db-set.php" autocomplete="off">
<div align="center">
		<div class="trans_layoutD">
		  <div class="trans_text">Other Receivable / Service Customer Details</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="585" border="0" align="center">
      <tr>
        <td width="62%"><table width="100%" border="0" > 
          <tr>
            <td height="47" ><table width="100%" border="0" class="">
              <tr>
                <td height="25" class="normalfnt">&nbsp;</td>
                <td align="left" class="normalfntBlue">Other Receivable / Service Customer</td>
                <td width="403" colspan="3">
                <select name="cboSearch" class="txtbox" id="cboSearch"  style="width:361px" >
                <option value=""></option>
                 <?php  $sql = "SELECT
								mst_finance_service_customer.intId,
								mst_finance_service_customer.strName,
								mst_finance_service_customer.strCode
								FROM mst_finance_service_customer
								order by strName

								";
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intId']."\">".$row['strName']." - ".$row['strCode']."</option>";
								}
                   ?> 
                </select>
                  </td>
              </tr>
              <tr>
                <td width="1" class="normalfnt">&nbsp;</td>
                <td width="155" class="normalfnt">&nbsp;</td>
                <td colspan="3">&nbsp;</td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td colspan="4" class="normalfnt"><table width="100%" border="0" class="tableBorder">
                  <tr>
                    <td colspan="4" align="left" bgcolor="#E4E4E4" class="tableBorder">
                    <span class="normalfnt"><strong>General</strong></span></td>
                    </tr>
                  <tr class="">
                    <td width="29%" class="normalfntBlue">Other Receivable / Service Customer Code&nbsp;<span class="compulsoryRed">*</span></td>
                    <td width="26%">
                   	<input value="" name="txtCode" type="text" class="validate[required,maxSize[10]]" id="txtCode" style="width:140px" />
                    </td>
                    <td width="15%" class="normalfntMid">Type</td>
                    <td width="30%">
                    <select name="cboType" class="txtbox" id="cboType" style="width:158px" >
                       <option value=""></option>
                      <?php  $sql = "SELECT
						intId,
						strName
						FROM mst_typeofmarketer
						WHERE
							intStatus = 1
						order by strName
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
						}
        				?>
                    </select></td>
                    </tr>
                  <tr class="">
                    <td height="30" class="normalfntBlue">Other Receivable / Service Customer Name <span class="compulsoryRed">*</span></td>
                    <td colspan="3">
                    <input name="txtName" class="validate[required,maxSize[50]]" type="text" id="txtName" style="width:300px" maxlength="50"/></td>
                    </tr>
                  <tr class="">
                    <td class="normalfnt">Address</td>
                    <td colspan="3">
                 <textarea name="txtAddress" style="width:300px"  rows="2" id="txtAddress"></textarea></td>
                    </tr>
                  <tr class="">
                    <td class="normalfnt">Contact Person</td>
                    <td colspan="3">
                <textarea name="txtContact" style="width:300px"  rows="2" id="txtContact" ></textarea></td>
                    </tr>
                  <tr class="">
                    <td class="normalfnt">City</td>
                    <td><input name="txtCity" type="text" id="txtCity" style="width:140px"/></td>
                    <td class="normalfnt">Country</td>
                    <td><select name="cboCountry" id="cboCountry" style="width:158px">
                      <option value=""></option>
                      <?php  $sql = "SELECT
									mst_country.intCountryID,
									mst_country.strCountryName
								FROM mst_country
								WHERE
									intStatus = 1
									order by strCountryName
								";
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intCountryID']."\">".$row['strCountryName']."</option>";
								}
                   ?>
                    </select></td>
                  </tr>
                  <tr class="">
                    <td class="normalfnt"> Currency <span class="compulsoryRed">*</span></td>
                    <td><select name="cboCurrency" class="validate[required]" id="cboCurrency" style="width:140px">
                      <option value=""></option>
                      <?php  $sql = "SELECT
						intId,
						strCode
						FROM mst_financecurrency
						WHERE
							intStatus = 1
						order by strCode
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strCode']."</option>";
						}
        				?>
                    </select></td>
                    <td class="normalfnt">&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                  <tr class="">
                    <td class="normalfnt">&nbsp;</td>
                    <td colspan="3">&nbsp;</td>
                  </tr>
                  </table></td>
              </tr>
              <tr>
              	<td>&nbsp;</td>
                <td colspan="5"><table width="100%" border="0" class="tableBorder">
                  <tr>
                    <td colspan="4" align="left" bgcolor="#E4E4E4" class="tableBorder">
                    <span class="normalfnt"><strong>Communication</strong></span></td>
                  </tr>
                  <tr>
                    <td width="23%"><span class="normalfnt">Phone No</span></td>
                    <td width="30%">
                    <input name="txtPhone" type="text" id="txtPhone" style="width:140px" /></td>
                    <td width="18%"><span class="normalfnt">Mobile No</span></td>
                    <td width="29%">
                    <input name="txtMobile" type="text" id="txtMobile" style="width:158px" /></td>
                  </tr>
                  <tr>
                    <td><span class="normalfnt">Fax</span></td>
                    <td>
                    <input name="txtFax" type="text" id="txtFax" style="width:140px" /></td>
                    <td><span class="normalfnt">E-Mail</span></td>
                    <td>
              <input name="txtEMail" type="text" id="txtEMail" class="validate[custom[email]]" style="width:158px" /></td>
                  </tr>
                  <tr>
                    <td><span class="normalfnt">Website</span></td>
                    <td colspan="3">
                    <input name="txtWeb" class="validate[custom[url]]" type="text" id="txtWeb" style="width:300px" /></td>
                    </tr>
                  <tr>
                    <td><span class="normalfnt">Shipment Method</span></td>
                    <td>
                 	<select name="cboShipmentMethod" id="cboShipmentMethod" style="width:140px" >
                    <option value=""></option>
                      <?php  $sql = "SELECT
						intId,
						strName
						FROM mst_shipmentmethod
						WHERE
							intStatus = 1
						order by strName
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
						}
        				?>
                    </select></td>
                    <td class="normalfnt">&nbsp;</td>
                    <td>&nbsp;</td>
                    </tr>
                </table></td>
              </tr>
              <tr>
                <td rowspan="3" class="normalfnt">&nbsp;</td>
                <td colspan="4" class="normalfnt"><table width="100%" border="0" class="tableBorder">
                  <tr>
                    <td colspan="4" align="left" bgcolor="#E4E4E4" class="tableBorder">
                    <span class="normalfnt"><strong>Legal Information</strong></span></td>
                    </tr>
                  <tr>
                    <td width="23%" class="normalfnt">VAT Reg No. <span class="compulsoryRed">*</span></td>
                    <td width="30%">
                    <input name="txtVatNo" class="validate[required]" type="text" id="txtVatNo" style="width:140px" /></td>
                    <td width="18%" class="normalfnt">SVAT No. <span class="compulsoryRed">*</span></td>
                    <td width="29%">
                    <input name="txtSvatNo" class="validate[required]" type="text" id="txtSvatNo" style="width:158px" /></td>
                    </tr>
                  <tr>
                    <td class="normalfnt">Business Reg No</td>
                    <td>
                    <input name="txtRegNo" type="text" id="txtRegNo" style="width:140px" /></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    </tr>
                  <tr>
                    <td class="normalfnt">Invoice Type</td>
                <td class="normalfnt">
                <input type="radio" class="invTpe" name="rdInvoType" id="Commercial" value="Commercial" /> Commercial</td>
                <td class="normalfnt">
                <input type="radio"  class="invTpe" name="rdInvoType" id="Tax" value="Tax" /> Tax</td>
                <td class="normalfnt">
                <input type="radio"  class="invTpe" name="rdInvoType" id="SVAT" value="SVAT"> SVAT</td>
                    </tr>
                  </table></td>
              </tr>
              <tr>
                <td colspan="4" class="normalfnt"><table width="100%" border="0" class="tableBorder">
                  <tr>
                    <td colspan="4" align="left" bgcolor="#E4E4E4" class="tableBorder">
                    <span class="normalfnt"><strong>Payments</strong></span></td>
                  </tr>
                  <tr>
                    <td width="23%" class="normalfnt">Payments Terms</td>
                    <td width="26%">
                   <select name="cboPaymentsTerms" id="cboPaymentsTerms" style="width:140px" >
                    <option value=""></option>
                      <?php  $sql = "SELECT
						intId,
						strName
						FROM mst_financepaymentsterms
						WHERE
							intStatus = 0
						order by strName
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strName']." days"."</option>";
						}
        				?>
                    </select></td>
                    <td width="22%" class="normalfnt">Payments Methods</td>
                    <td width="29%">
                 <select name="cboPaymentsMethods" id="cboPaymentsMethods" style="width:158px" >
                    <option value=""></option>
                      <?php  $sql = "SELECT
						intId,
						strName
						FROM mst_financepaymentsmethods
						WHERE
							intStatus = 1
						order by strName
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
						}
        				?>
                    </select></td>
                  </tr>
                  <tr>
                    <td class="normalfnt">Credit Limit</td>
                    <td>
                    <input name="txtCreditLimit" type="text" id="txtCreditLimit" style="width:140px" /></td>
                    <td class="normalfnt" style="display:none">Ledger Accounts</td>
                    <td style="display:none">
                    <select name="cboChartOfAcc" id="cboChartOfAcc" style="width:158px" >
                    <option value=""></option>
                 	<?php  $sql = "SELECT
						intId,
						strCode,
						strName
						FROM mst_financechartofaccounts
						WHERE
						intStatus = '1' AND  intFinancialTypeId = '10' AND strType = 'Posting'
						order by strCode
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strCode']."-".$row['strName']."</option>";
						}
        			?>
                    </select></td>
                  </tr>
                </table></td>
                </tr>
              <tr>
                <td colspan="4" class="normalfnt"><table width="100%" border="0" class="tableBorder">
                  <tr>
                    <td colspan="4" align="left" bgcolor="#E4E4E4" class="tableBorder">
                     <span class="normalfnt"><strong>Other</strong></span></td>
                    </tr>
                  <tr>
                    <td width="23%" class="normalfnt">Blocked</td>
                    <td width="27%">
                    <select name="cboBlocked" id="cboBlocked" style="width:140px" >
                      <option>None</option>
                      <option>Delivery</option>
                      <option>Invoice</option>
                      <option>All</option>
                    </select></td>
                    <td width="21%">&nbsp;</td>
                    <td width="29%">&nbsp;</td>
                    </tr>
                  <tr class="">
                    <td class="normalfnt">Rank</td>
                    <td colspan="3">
 <input name="txtRank" type="text" id="txtRank" style="width:20px" class="validate[custom[integer],min[0],max[10]]"/>
                      <span class="normalfnt" style="color:#CCC;font-size:9px">1 to 10. 1=Super ,10=Blocked</span></td>
                    </tr>
                  <tr class="">
                    <td class="normalfnt">Active</td>
                    <td colspan="3">
                    <input type="checkbox" name="chkActive" id="chkActive" checked="checked" /></td>
                    </tr>
                  </table></td>
              </tr>
              </table></td>
            </tr>
          <tr>
            <td height="34"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td width="100%" align="center" bgcolor=""><img style="display:none" border="0" src="../../../../../images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:none" border="0" src="../../../../../images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/><img style="display:none" border="0" src="../../../../../images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="25"/><a href="../../../../../main.php"><img  src="../../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                </tr>
              </table></td>
            </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
  </table>
	</div>
  </div> 
</form>
</body>
</html>

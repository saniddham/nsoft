<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$MenuId				= '150';
$companyId 			= $_SESSION['CompanyID'];

if($_SERVER['REQUEST_METHOD'] == "POST")
{
	$optionValue = trim($_REQUEST['cboSearchOption']);
	$searchValue = trim($_REQUEST['txtSearchValue']);
}
$searchValue	 = (!isset($searchValue)?'':$searchValue);   
?>
<title>Cost Center Listing</title>

<script type="text/javascript" language="javascript">
function pageSubmit()
{
	document.getElementById('frmDimensionListing').submit();	
}
function clearValue()
{
	document.getElementById('txtSearchValue').value = '';
	document.getElementById('txtSearchValue').focus();
}
</script>

<form id="frmDimensionListing" name="frmDimensionListing" method="post">
<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">Cost Center  Listing</div>
		  <table width="800" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="800" border="0">
      <tr>
        <td><table width="800" border="0" class="tableBorder_allRound">
          <tr>
            <td width="16%" height="22" class="normalfnt">&nbsp;</td>
            <td width="18%" class="normalfntMid">Searching Option</td>
            <td width="17%" class="normalfnt">
            <select name="cboSearchOption" id="cboSearchOption" onChange="clearValue();" style="width:150px">
              <option value="">&nbsp;</option>
<option value="d.strCode" <?php echo($optionValue=='d.strCode'?'selected':'') ?>>Cost Center Code</option>
<option value="d.strName" <?php echo($optionValue=='d.strName'?'selected':'') ?>>Cost Center Name</option>
            </select></td>
            <td width="16%"><input value="<?php echo $searchValue; ?>" type="text" name="txtSearchValue" id="txtSearchValue" /></td>
            <td width="18%" class="normalfnt">
            <a href="#">
            <img src="images/search.png" width="20" height="20" onClick="pageSubmit();" /></a></td>
            <td width="15%">&nbsp;</td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td>
          <!--<div id="_table_wrap" style="overflow: hidden; display: inline-block; border: 1px solid rgb(136, 136, 136);">
          <div id="_head_wrap" style="width: 800px; overflow: hidden;">-->
          <div style="overflow:hidden;width:800px;" id="divGrid">
          <table width="100%" class="bordered" id="tblMain" >
          <thead>
            <tr>
              <th height="22" style="width: 60px;" ><strong>Cost Center Code</strong></th>
              <th style="width: 60px;" ><strong>Cost Center Name</strong></th>
              <th style="width: 120px;" ><strong>Remarks</strong></th>
              <th style="width: 40px;" ><strong>Active</strong></th>
              <th style="width: 40px;" >View</th>
              </tr>
           </thead> 
           <tbody>
              <?php

				if($searchValue!='')
			  	$wherePart = "WHERE $optionValue like '%$searchValue%'";
				
				$wherePart = (!isset($wherePart)?'':$wherePart);
				
	 	 		 $sql = "SELECT
				 d.intId,
				 d.strCode,
				 d.strName,
				 d.strRemark,
				 d.intStatus
				 FROM mst_financedimension d
				 $wherePart
				 Order by intId";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$id 		=	 $row['intId'];
	  			 ?>
           		<tr class="normalfnt">
           		<td class="diCode" height="16" align="center" bgcolor="#FFFFFF">
		   		<?php echo $row['strCode'];?></td>
              	<td class="diName"  align="center" bgcolor="#FFFFFF"><?php echo $row['strName'];?></td>
              	<td class="diRemark" bgcolor="#FFFFFF"><?php echo $row['strRemark'];?></td>
              	<td class="active" align="center" bgcolor="#FFFFFF">
              	  <input disabled="disabled" <?php echo($row['intStatus']?'checked':''); ?> type="checkbox"/></td>
              	<td class="view" align="center" bgcolor="#FFFFFF"><a target="_blank" href="<?php echo "?q=$MenuId&id=$id";?>">More</a></td>
              </tr>
               <?php 
        	    } 
       		   ?>
              </tbody>
              </table>
            </div>
              </td>
              </tr>
            </table>
			</td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound"><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
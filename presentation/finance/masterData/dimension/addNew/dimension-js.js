var basePath	= "presentation/finance/masterData/dimension/addNew/";
		
$(document).ready(function() {
	
	$("#frmDimension").validationEngine();
	$('#frmDimension #txtCode').focus();
	
	if(dId!='')
	{
		$('#frmDimension #cboSearch').val(dId);
		loadData();
	}
 
  ///save button click event
  $('#frmDimension #butSave').off('click').on('click',function(){
	//$('#frmDimension').submit();
	var requestType = '';
	if ($('#frmDimension').validationEngine('validate'))   
    { 
		//$('#txtLocationCode').validationEngine('showPrompt', 'This is an example', 'pass'); //hide all alerts//
		if($("#frmDimension #cboSearch").val()=='')
			requestType = 'add';
		else
			requestType = 'edit';
		
		var url = basePath+"dimension-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json", 
			type: "post", 
			data:$("#frmDimension").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addLocation"}',
			async:false,
			
			success:function(json){
					$('#frmDimension #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmDimension').get(0).reset();
						loadCombo_frmDimension();
						var t=setTimeout("alertx()",1000);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmDimension #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
   
   /////////////////////////////////////////////////////
   //// load location details //////////////////////////
   /////////////////////////////////////////////////////
	$('#frmDimension #cboSearch').off('click').on('click',function(){
		
		$('#frmDimension').validationEngine('hide');
	});
	$('#frmDimension #cboSearch').off('change').on('change',function(){
		
		loadData();
	});
	
	$('#frmDimension #butNew').off('click').on('click',function(){
		$('#frmDimension').get(0).reset();
		loadCombo_frmDimension();
		$('#frmDimension #txtCode').focus();
	});
    $('#frmDimension #butDelete').off('click').on('click',function(){
		if($('#frmDimension #cboSearch').val()=='')
		{
			$('#frmDimension #butDelete').validationEngine('showPrompt', 'Please select dimension.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmDimension #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = basePath+"dimension-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											type: 'post',
											data:'requestType=delete&cboSearch='+$('#frmDimension #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmDimension #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													$('#frmDimension').get(0).reset();
													loadCombo_frmDimension();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
});

function loadData()
{
	$('#frmDimension').validationEngine('hide');
	
	var url = basePath+"dimension-db-get.php";
	if($('#frmDimension #cboSearch').val()=='')
	{
		$('#frmDimension').get(0).reset();return;	
	}
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		data:'requestType=loadDetails&id='+$('#frmDimension #cboSearch').val(),
		async:false,
		success:function(json){
				//json  = eval('('+json+')');
				$('#frmDimension #txtCode').val(json.code);
				$('#frmDimension #txtName').val(json.name);
				$('#frmDimension #txtRemark').val(json.remark);
				$('#frmDimension #cboLocation').val(json.location);
				$('#frmDimension #chkActive').attr('checked',json.status);
		}
	});
}
function loadCombo_frmDimension()
{
	var url 	= basePath+"dimension-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmDimension #cboSearch').html(httpobj.responseText);
}

function alertx()
{
	$('#frmDimension #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmDimension #butDelete').validationEngine('hide')	;
}

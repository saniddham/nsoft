<?php 
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	$id 			= $_REQUEST['cboSearch'];
	$code			= trim($_REQUEST['txtCode']);
	$name			= trim($_REQUEST['txtName']);
	$remark			= trim($_REQUEST['txtRemark']);
	$location		= trim($_REQUEST['cboLocation']);
	$intStatus		= ($_REQUEST['chkActive']?1:0);	
	
	/////////// location insert dimension /////////////////////
	if($requestType=='add')
	{
	    $sqlPrev = "SELECT strCostCenterCode from mst_financedimension where strCostCenterCode != '' ORDER BY strCostCenterCode DESC limit 1";
	    $resultPrev = $db->RunQuery($sqlPrev);
        $rowPO	= mysqli_fetch_array($resultPrev);
        if(mysqli_num_rows($resultPrev)==0)
        {
            $strCostCenterCode = 10;
        }
        else{
            $strCostCenterCode = $rowPO['strCostCenterCode']+1;
        }
       	$sql = "INSERT INTO `mst_financedimension` (`strCode`,`strCostCenterCode`,`strName`,`strRemark`,intLocation,`intCreator`,dtmCreateDate,intStatus) 
				VALUES ('$code','$strCostCenterCode','$name','$remark','$location','$userId',now(),'$intStatus')";
		$result = $db->RunQuery($sql);
		if($result){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
	}
	/////////// location update part /////////////////////
	else if($requestType=='edit')
	{
		$sql = "UPDATE `mst_financedimension` SET 	strCode		='$code',
											strName		='$name',
											strRemark	='$remark',
											intLocation ='$location',
											intStatus	='$intStatus',
											intModifyer	='$userId'
				WHERE (`intId`='$id')";
		$result = $db->RunQuery($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Updated successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	/////////// location delete part /////////////////////
	else if($requestType=='delete')
	{
		$sql = "DELETE FROM `mst_financedimension` WHERE (`intId`='$id')  ";
		$result = $db->RunQuery($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Deleted successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	echo json_encode($response);
?>
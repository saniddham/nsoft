var value = "";	
$(document).ready(function() {
  		//$("#frmActivateItem").validationEngine();
value="[ ";
		$('#tblMainGrid tr').each(function() {
			var itemClass 		= $(this).find(".itemClass").attr('id');
			var itemCategory 	= $(this).find(".itemCat").attr('id');
			var itemSubCategory = $(this).find(".itemSubCat").attr('id');
			var otherAccount	= $(this).find(".otherAccount").val();
			var stockAccount	= $(this).find(".stockAccount").val();
			var localAccount	= $(this).find(".localAccount").val();
			var impAccount		= $(this).find(".impAccount").val();
			
			value += '{ "classId":"'+itemClass+'", "catId": "'+itemCategory+'", "subCatId": "'+itemSubCategory+'", "otherAccId": "'+otherAccount+'", "stockAccId": "'+stockAccount+'", "localAccount": "'+localAccount+'", "impAccount": "'+impAccount+'"},';	
		});
		value = value.substr(0,value.length-1);
		value += " ]";

		loadCombo_frmActivateItem();
  //permision for add 
  if(intAddx)
  {
 	$('#frmActivateItem #butNew').show();
	$('#frmActivateItem #butSave').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmActivateItem #butSave').show();
  }
  
  //permision for view
  if(intViewx)
  {
	//
  }
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
	$('#frmActivateItem #chkAllItem').click(function(){
        var chkAll=document.getElementById("chkAllItem");
        var status=false;
        if(chkAll.checked){
            status=true;
        }
        else{
            status=false;
        }
        $('#frmActivateItem .checkValues').attr("checked",status);		
    });
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  ///save button click event
  $('#frmActivateItem #butSave').click(function(){
	 value="[ ";
		$('#tblMainGrid tr').each(function() {
			var itemClass = "";
			var itemCategory = "";
			var itemSubCategory = "";
			var deleteStatus = "";
			if($(this).find(".checkValues").attr('checked'))
			{
				deleteStatus 	= 'false';
				itemClass 		= $(this).find(".itemClass").attr('id');
				itemCategory 	= $(this).find(".itemCat").attr('id');
				itemSubCategory = $(this).find(".itemSubCat").attr('id');
				var otherAccount= $(this).find(".otherAccount").val();
				var stockAccount= $(this).find(".stockAccount").val();
				var localAccount= $(this).find(".localAccount").val();
				var impAccount	= 	$(this).find(".impAccount").val();
				
				//$(this).find(".otherAccount").addClass('validate[required]');
				if(itemClass == 1)
				{
					$(this).find(".otherAccount").removeClass('validate[required]');
					$(this).find(".stockAccount").addClass('validate[required]');
					$(this).find(".localAccount").addClass('validate[required]');
					$(this).find(".impAccount").addClass('validate[required]');
				}
				else
				{
					$(this).find(".otherAccount").addClass('validate[required]');
					$(this).find(".stockAccount").removeClass('validate[required]');
					$(this).find(".localAccount").removeClass('validate[required]');
					$(this).find(".impAccount").removeClass('validate[required]');
				}
			}
			else
			{
				deleteStatus 	= 'true';
				itemClass 		= $(this).find(".itemClass").attr('id');
				itemCategory 	= $(this).find(".itemCat").attr('id');
				itemSubCategory = $(this).find(".itemSubCat").attr('id');
				
				$(this).find(".otherAccount").removeClass('validate[required]');
				$(this).find(".stockAccount").removeClass('validate[required]');
				$(this).find(".localAccount").removeClass('validate[required]');
				$(this).find(".impAccount").removeClass('validate[required]');
				
			}
		value += '{ "classId":"'+itemClass+'", "catId": "'+itemCategory+'", "subCatId": "'+itemSubCategory+'", "otherAccId": "'+otherAccount+'", "stockAccId": "'+stockAccount+'", "delStatus": "'+deleteStatus+'", "localAccount": "'+localAccount+'", "impAccount": "'+impAccount+'"},';	
		});
		value = value.substr(0,value.length-1);
		value += " ]";
		
	var requestType = '';
	if ($('#frmActivateItem').validationEngine('validate'))   
    { 
		requestType = 'edit';

	    var url = "activateItem-db-set.php"+"?requestType="+requestType;
     	var obj = $.ajax({
			url:url,
			dataType: "json",
			type:'post',  
			data: {myJson:  value},
			async:false,
			
			success:function(json){
					$('#frmActivateItem #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmActivateItem').get(0).reset();
						loadCombo_frmActivateItem();
						var t=setTimeout("alertx()",1000);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmActivateItem #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
   
   /////////////////////////////////////////////////////
   //// account item activate details //////////////////
   /////////////////////////////////////////////////////
	
	$('#frmActivateItem #butNew').click(function(){
		$('#frmActivateItem').get(0).reset();
		loadCombo_frmActivateItem();
	});
 
});

function loadCombo_frmActivateItem()
{
		$('#frmActivateItem').validationEngine('hide');
 		var url = "activateItem-db-get.php"+"?requestType="+'loadDetails';
     	var obj = $.ajax({
			url:url,
			dataType: "json",
			type:'post', 
			data: {myJson:  value},
			async:false,
			success:function(json){
				var classId = "";
				var catId = "";
				var subId = "";
				var otherId = "";
				var stockId = "";
				var localAcc = "";
				var impAcc = "";
				if(eval(json)!=null)
				{
					for(var i=0;i<=json.length;i++)
					{
						classId 	= json[i].classId;
						catId 		= json[i].catId;
						subId 		= json[i].subId;
						otherId 	= json[i].otherAccId;
						stockId 	= json[i].stockAccId;
						localAcc 	= json[i].localAcc;
						impAcc 		= json[i].impAcc;
						$('#frmActivateItem #cboOtherAcc'+classId+catId+subId).val(otherId);
						$('#frmActivateItem #cboStockAcc'+classId+catId+subId).val(stockId);
						$('#frmActivateItem #cboLocalAcc'+classId+catId+subId).val(localAcc);
						$('#frmActivateItem #cboImpAcc'+classId+catId+subId).val(impAcc);
					}
				}
			}
	});
}

function alertx()
{
	$('#frmActivateItem #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	//
}

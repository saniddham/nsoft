<?php
session_start();
$backwardseperator = "../../../../../";
$thisFilePath =  $_SERVER['PHP_SELF'];

include  "{$backwardseperator}dataAccess/permisionCheck.inc";

$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$companyId = $row['intCompanyId']; 
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Stock Item Activate</title>

<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../../css/tblstyle.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../../libraries/jquery/jquery-1.3.2.min.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/scrolltable.js"></script>

<script type="application/javascript" src="../../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="activateItem-js.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/script.js"></script>

<script type="application/javascript" src="../../../../../libraries/javascript/zebraStripe.js"></script>

<link rel="stylesheet" href="../../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../../libraries/validate/template.css" type="text/css">

</head>

<body>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
	<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
<script src="../../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<form id="frmActivateItem" name="frmActivateItem" method="post" autocomplete="off" action="activateItem-db-set.php">
<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">Stock Item Activate</div>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
        <td>
          <div id="_table_wrap" style="overflow: hidden; display: inline-block; border: 1px solid rgb(136, 136, 136);">
            <div id="_head_wrap" style="width: 900px; overflow: hidden;">
              <table class="tbl1 scroll" id="_head" style="table-layout: fixed;">
                <thead>
                  <tr class="gridHeader">
                   <th height="22" style="width: 15px;" ><input type="checkbox" id="chkAllItem" name="chkAllItem" value="" checked="checked" /></th>
                    <th style="width: 60px;" ><strong>Item Class</strong></th>
                    <th style="width: 80px;" ><span class="mainHeading4"><strong>Main Category</strong></span></th>
                    <th style="width: 84px;" ><strong>Sub Category</strong></th>
                    <th style="width: 105px;" ><strong>Expenses/Fixed Assets</strong>/...</th>
                    <th style="width: 105px;" ><strong>Stock Account</strong></th>
                    <th style="width: 105px;" ><strong>Local Purchase</strong></th>
                    <th style="width: 105px;" ><strong>Imp. Purchase</strong></th>
                    <th style="width: 8px;" ></th>
                    </tr>
                  </table>
              </div>
            <div class="normalfnt" id="_body_wrap" style="width: 900px; height: 350px; overflow: scroll;">
              <table class="tbl1 scroll" id="tblMainGrid"> 
                <tbody>
                  <?php
	 	 		 $sql = "SELECT
				 fc.intId AS classId,
				 fc.strName AS className,
				 mc.intId AS catId,
				 mc.intFinanceClass,
				 mc.intId,
				 mc.strName AS catName,
				 sc.intId AS subId,
				 sc.strName AS subName,
				 sc.intMainCategory
				 FROM mst_financemaincategoryclass fc,mst_maincategory mc,mst_subcategory sc
				 WHERE
				 fc.intId  = mc.intFinanceClass AND mc.intId = sc.intMainCategory
				 Order by classId";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					 $classId = $row['classId'];
					 $catId = $row['catId'];
					 $subId = $row['subId'];
	  			 ?>
                  <tr class="normalfnt">
                  <td class="accActSelect" align="center" bgcolor="#FFFFFF">
        <input type="checkbox" name=chk<?php echo $classId.$catId.$subId?> id=chk<?php echo $classId.$catId.$subId?> checked="checked" class="checkValues"/>
                     </td>
                    <td class="itemClass" height="16" align="center" bgcolor="#FFFFFF" id=<?php echo $row['classId'];?>>
                      	<?php echo $row['className'];?></td>
                    <td class="itemCat"  align="center" bgcolor="#FFFFFF" id=<?php echo $row['catId'];?>><?php echo $row['catName'];?></td>
                    <td class="itemSubCat"  align="center" bgcolor="#FFFFFF" id=<?php echo $row['subId'];?>><?php echo $row['subName'];?></td>
                    <td class="itemAcc" bgcolor="#FFFFFF"><span class="normalfntMid">
                      <select name="cboChartOfAcc" id=cboOtherAcc<?php echo $classId.$catId.$subId?> style="width:100%" class="validate[required] otherAccount" <?php echo ($classId!='1'?' validate[required] "':'" disabled ');  ?>>
                        <option value=""></option>
                        <?php  $sql2 = "SELECT
										mst_financechartofaccounts.intId,
										mst_financechartofaccounts.strCode,
										mst_financechartofaccounts.strName,
										mst_financechartofaccounts_companies.intChartOfAccountId,
										mst_financechartofaccounts_companies.intCompanyId
										FROM
										mst_financechartofaccounts
										Inner Join mst_financechartofaccounts_companies ON mst_financechartofaccounts_companies.intChartOfAccountId = mst_financechartofaccounts.intId
										WHERE
										intStatus = '1' AND  (intFinancialTypeId = '1' OR  intFinancialTypeId = '2' OR  intFinancialTypeId = '5' OR  intFinancialTypeId = '6' OR  intFinancialTypeId = '8' OR  intFinancialTypeId = '21' OR  intFinancialTypeId = '12') AND strType = 'Posting' AND intCompanyId = '$companyId'
										order by strCode
						";
						$result2 = $db->RunQuery($sql2);
						while($row2=mysqli_fetch_array($result2))
						{
							echo "<option value=\"".$row2['intId']."\">".$row2['strCode']."-".$row2['strName']."</option>";
						}
        				?>
                      </select>
                    </span></td>
                    <td class="itemAcc" align="center" bgcolor="#FFFFFF"><span class="normalfntMid">
<select class="stockAccount" <?php echo ($classId=='1'?' validate[required] "':'" disabled ');  ?> name="cboChartOfAcc" id=cboStockAcc<?php echo $classId.$catId.$subId?>  style="width:100%">
                        <option value=""></option>
                        <?php  $sql2 = "SELECT
										mst_financechartofaccounts.intId,
										mst_financechartofaccounts.strCode,
										mst_financechartofaccounts.strName,
										mst_financechartofaccounts_companies.intChartOfAccountId,
										mst_financechartofaccounts_companies.intCompanyId
										FROM
										mst_financechartofaccounts
										Inner Join mst_financechartofaccounts_companies ON mst_financechartofaccounts_companies.intChartOfAccountId = mst_financechartofaccounts.intId
										WHERE
										intStatus = '1' AND  intFinancialTypeId = '20' AND strType = 'Posting' AND intCompanyId = '$companyId'
										order by strCode
						";
						$result2 = $db->RunQuery($sql2);
						while($row2=mysqli_fetch_array($result2))
						{
						echo "<option value=\"".$row2['intId']."\">".$row2['strCode']."-".$row2['strName']."</option>";
						}
        				?>
                      </select>
                    </span></td>
                    <td class="itemAcc" bgcolor="#FFFFFF"><span class="normalfntMid">
                      <select name="cboChartOfAcc" id=cboLocalAcc<?php echo $classId.$catId.$subId?> style="width:100%" class="validate[required] localAccount" <?php echo ($classId=='1'?' validate[required] "':'" disabled ');  ?>>
                        <option value=""></option>
                        <?php  $sql2 = "SELECT
										mst_financechartofaccounts.intId,
										mst_financechartofaccounts.strCode,
										mst_financechartofaccounts.strName,
										mst_financechartofaccounts_companies.intChartOfAccountId,
										mst_financechartofaccounts_companies.intCompanyId
										FROM
										mst_financechartofaccounts
										Inner Join mst_financechartofaccounts_companies ON mst_financechartofaccounts_companies.intChartOfAccountId = mst_financechartofaccounts.intId
										WHERE
										intStatus = '1' AND  intFinancialTypeId = '5' AND strType = 'Posting' AND intCompanyId = '$companyId'
										order by strCode
						";
						$result2 = $db->RunQuery($sql2);
						while($row2=mysqli_fetch_array($result2))
						{
						echo "<option value=\"".$row2['intId']."\">".$row2['strCode']."-".$row2['strName']."</option>";
						}
        				?>
                      </select>
                    </span></td>
                    <td class="itemAcc" bgcolor="#FFFFFF"><span class="normalfntMid">
                      <select name="cboChartOfAcc" id=cboImpAcc<?php echo $classId.$catId.$subId?> style="width:100%" class="validate[required] impAccount" <?php echo ($classId=='1'?' validate[required] "':'" disabled ');  ?>>
                        <option value=""></option>
                        <?php  $sql2 = "SELECT
										mst_financechartofaccounts.intId,
										mst_financechartofaccounts.strCode,
										mst_financechartofaccounts.strName,
										mst_financechartofaccounts_companies.intChartOfAccountId,
										mst_financechartofaccounts_companies.intCompanyId
										FROM
										mst_financechartofaccounts
										Inner Join mst_financechartofaccounts_companies ON mst_financechartofaccounts_companies.intChartOfAccountId = mst_financechartofaccounts.intId
										WHERE
										intStatus = '1' AND  intFinancialTypeId = '5' AND strType = 'Posting' AND intCompanyId = '$companyId'
										order by strCode
						";
						$result2 = $db->RunQuery($sql2);
						while($row2=mysqli_fetch_array($result2))
						{
						echo "<option value=\"".$row2['intId']."\">".$row2['strCode']."-".$row2['strName']."</option>";
						}
        				?>
                      </select>
                    </span></td>
                    </tr>
                  <?php 
        	    	} 
       		   	?>
                  </tbody>
                </table>
              </div></div>
          </td>
      </tr>
            </table>
			</td>
      </tr>
      <tr>
         <td width="100%" align="center" bgcolor=""><img style="display:none" border="0" src="../../../../../images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:none" border="0" src="../../../../../images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/><img style="display:none" border="0" src="../../../../../images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="25"/><a href="../../../../../main.php"><img  src="../../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
</body>
</html>

<?php
session_start();
$backwardseperator = "../../../../../";
$thisFilePath =  $_SERVER['PHP_SELF'];

include  "{$backwardseperator}dataAccess/permisionCheck.inc";

$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$companyId = $row['intCompanyId']; 
}

if($_SERVER['REQUEST_METHOD'] == "POST")
{
	$optionValue = trim($_REQUEST['cboSearchOption']);
	$searchValue = trim($_REQUEST['txtSearchValue']);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Account Item Activate Listing</title>

<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../../css/tblstyle.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../../libraries/jquery/jquery-1.3.2.min.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/scrolltable.js"></script>

<script type="application/javascript" src="../../../../../libraries/javascript/zebraStripe.js"></script>

<script type="text/javascript" language="javascript">
function pageSubmit()
{
	document.getElementById('frmChartOfAccountsListing').submit();	
}
function clearValue()
{
	document.getElementById('txtSearchValue').value = '';
	document.getElementById('txtSearchValue').focus();
}
</script>

</head>

<body>
<form id="frmAccountItemListing" name="frmAccountItemListing" method="post" autocomplete="off" action="accountItemListing.php">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">Account Item Activate Listing</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0" cellpadding="1" cellspacing="0" class="tableBorder_allRound">
          <tr>
            <td width="16%" height="22" class="normalfnt">&nbsp;</td>
            <td width="18%" class="normalfntMid">Searching Option</td>
            <td width="17%" class="normalfnt">
            <select name="cboSearchOption" id="cboSearchOption" onchange="clearValue();" style="width:150px">
              <option value="">&nbsp;</option>
<option value="mst_financemaincategoryclass.strName" <?php echo($optionValue=='mst_financemaincategoryclass.strName'?'selected':'') ?> >Item Class</option>
<option value="mst_maincategory.strName" <?php echo($optionValue=='mst_maincategory.strName'?'selected':'') ?>>Main Category</option>
<option value="mst_subcategory.strName" <?php echo($optionValue=='mst_subcategory.strName'?'selected':'') ?>>Sub Category</option>
            </select></td>
            <td width="16%"><input value="<?php echo $searchValue; ?>" type="text" name="txtSearchValue" id="txtSearchValue" /></td>
            <td width="18%" class="normalfnt">
            <a href="#">
            <img src="../../../../../images/search.png" width="20" height="20" onclick="pageSubmit();" /></a></td>
            <td width="15%">&nbsp;</td>
            </tr>
          <tr>
            <td height="22" class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td>
          <div id="_table_wrap" style="overflow: hidden; display: inline-block; border: 1px solid rgb(136, 136, 136);">
          <div id="_head_wrap" style="width: 900px; overflow: hidden;">
          <table class="tbl1 scroll" id="_head" style="table-layout: fixed;">
          <thead>
            <tr class="gridHeader">
              <th style="width: 60px;" ><strong>Item Class</strong></th>
                    <th style="width: 80px;" ><span class="mainHeading4"><strong>Main Category</strong></span></th>
                    <th style="width: 84px;" ><strong>Sub Category</strong></th>
                    <th style="width: 105px;" ><strong>Expenses/Fixed Assets</strong>/...</th>
                    <th style="width: 105px;" ><strong>Stock Account</strong></th>
                    <th style="width: 7px;" ></th>
              </tr>
           </table>
           </div>
           <div class="normalfnt" id="_body_wrap" style="width: 900px; height: 320px; overflow: scroll;">
           <table class="tbl1 scroll"> 
           <tbody>
              <?php

				if($searchValue!='')
			  	$wherePart = " and $optionValue like '%$searchValue%'";
				
	 	 		 $sql = "SELECT
						mst_financemaincategoryclass.strName AS className,
						mst_maincategory.strName AS mainCatName,
						mst_subcategory.strName AS subCatName,
						other.strName AS otherName,
						stock.strName AS stockName
						FROM
						mst_financeitemactivate
						left Join mst_financechartofaccounts AS other ON other.intId = mst_financeitemactivate.otherAccountId
						left Join mst_financechartofaccounts AS stock ON stock.intId = mst_financeitemactivate.stockAccountId
						left Join mst_maincategory ON mst_maincategory.intId = mst_financeitemactivate.mainCategoryId
						left Join mst_subcategory ON mst_subcategory.intId = mst_financeitemactivate.subCategoryId
						left Join mst_financemaincategoryclass ON mst_financemaincategoryclass.intId = mst_financeitemactivate.itemClassId WHERE mst_financeitemactivate.intCompanyId = '$companyId'
						$wherePart";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
	  			 ?>
           		<tr class="normalfnt">
           		<td class="itemClass" height="16" align="center" bgcolor="#FFFFFF">
		   		<?php echo $row['className'];?></td>
              	<td class="itemCat"  align="center" bgcolor="#FFFFFF"><?php echo $row['mainCatName'];?></td>
              	<td class="itemSubCat" bgcolor="#FFFFFF"><?php echo $row['subCatName'];?></td>
              	<td class="itemAcc" align="center" bgcolor="#FFFFFF"><?php echo $row['otherName'];?></td>
              	<td class="itemAcc" align="center" bgcolor="#FFFFFF"><?php echo $row['stockName'];?></td>
              	</tr>
               <?php 
        	    } 
       		   ?>
              </tbody>
             </table>
            </div></div>
              </td>
              </tr>
            </table>
			</td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound"><a href="../../../../../main.php"><img  src="../../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
</body>
</html>

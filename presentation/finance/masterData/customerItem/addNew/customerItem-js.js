function functionList()
{
	if(cusItmId!='')
	{
		$('#frmCustomerItem #cboSearch').val(cusItmId);
		$('#frmCustomerItem #cboSearch').change();
	}
}		
$(document).ready(function() {
  		$("#frmCustomerItem").validationEngine();
		$('#frmCustomerItem #txtName').focus();
  //permision for add 
  if(intAddx)
  {
 	$('#frmCustomerItem #butNew').show();
	$('#frmCustomerItem #butSave').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmCustomerItem #butSave').show();
	$('#frmCustomerItem #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmCustomerItem #butDelete').show();
	$('#frmCustomerItem #cboSearch').removeAttr('disabled');
  }
  
  //permision for view
  if(intViewx)
  {
	$('#frmCustomerItem #cboSearch').removeAttr('disabled');
  }
  
/*  $(':input').bind('keyup mouseup', function(){
      $(this).val($.trim($(this).val())) ;
    });*/

  ///save button click event
  $('#frmCustomerItem #butSave').click(function(){
	//$('#frmCustomerItem').submit();
	var requestType = '';
	if ($('#frmCustomerItem').validationEngine('validate'))   
    { 
		//$('#txtLocationCode').validationEngine('showPrompt', 'This is an example', 'pass'); //hide all alerts//
		if($("#frmCustomerItem #cboSearch").val()=='')
			requestType = 'add';
		else
			requestType = 'edit';
		
		var url = "customerItem-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json",  
			data:$("#frmCustomerItem").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addLocation"}',
			async:false,
			
			success:function(json){
					$('#frmCustomerItem #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmCustomerItem').get(0).reset();
						loadCombo_frmCustomerItem();
						var t=setTimeout("alertx()",1000);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmCustomerItem #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
   
   /////////////////////////////////////////////////////
   //// load customer item details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmCustomerItem #cboSearch').click(function(){
	   $('#frmCustomerItem').validationEngine('hide');
   });
    $('#frmCustomerItem #cboSearch').change(function(){
		$('#frmCustomerItem').validationEngine('hide');
		var url = "customerItem-db-get.php";
		if($('#frmCustomerItem #cboSearch').val()=='')
		{
			$('#frmCustomerItem').get(0).reset();return;	
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:'requestType=loadDetails&id='+$(this).val(),
			async:false,
			success:function(json){
					//json  = eval('('+json+')');
					$('#frmCustomerItem #txtName').val(json.name);
					$('#frmCustomerItem #txtRemark').val(json.remark);
					$('#frmCustomerItem #cboChartOfAcc').val(json.chartofaccount);
					$('#frmCustomerItem #chkActive').attr('checked',json.status);
			}
	});
	//////////// end of load details /////////////////
	
	});
	
	$('#frmCustomerItem #butNew').click(function(){
		$('#frmCustomerItem').get(0).reset();
		loadCombo_frmCustomerItem();
		$('#frmCustomerItem #txtName').focus();
	});
    $('#frmCustomerItem #butDelete').click(function(){
		if($('#frmCustomerItem #cboSearch').val()=='')
		{
			$('#frmCustomerItem #butDelete').validationEngine('showPrompt', 'Please select Item .', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmCustomerItem #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = "customerItem-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											data:'requestType=delete&cboSearch='+$('#frmCustomerItem #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmCustomerItem #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													$('#frmCustomerItem').get(0).reset();
													loadCombo_frmCustomerItem();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
});


function loadCombo_frmCustomerItem()
{
	var url 	= "customerItem-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmCustomerItem #cboSearch').html(httpobj.responseText);
}

function alertx()
{
	$('#frmCustomerItem #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmCustomerItem #butDelete').validationEngine('hide')	;
}

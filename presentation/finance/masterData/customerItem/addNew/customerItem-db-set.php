<?php 
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	$id 			= $_REQUEST['cboSearch'];
	$name			= trim($_REQUEST['txtName']);
	$remark			= trim($_REQUEST['txtRemark']);
	$chartofaccount	= null($_REQUEST['cboChartOfAcc']);
	$intStatus		= ($_REQUEST['chkActive']?1:0);	
	
	/////////// customer item insert part /////////////////////
	if($requestType=='add')
	{
		$sql = "INSERT INTO `mst_financecustomeritem` (`strName`,`strRemark`,`intChartOfAccountId`,`intCreator`,dtmCreateDate,intStatus) 
				VALUES ('$name','$remark',$chartofaccount,'$userId',now(),'$intStatus')";
		$result = $db->RunQuery($sql);
		if($result){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
	}
	/////////// customer item update part /////////////////////
	else if($requestType=='edit')
	{
		$sql = "UPDATE `mst_financecustomeritem` SET 	strName				='$name',
												strRemark			='$remark',
												intChartOfAccountId	=$chartofaccount,
												intStatus			='$intStatus',
												intModifyer			='$userId'
				WHERE (`intId`='$id')";
		$result = $db->RunQuery($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Updated successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	/////////// customer item delete part /////////////////////
	else if($requestType=='delete')
	{
		$sql = "DELETE FROM `mst_financecustomeritem` WHERE (`intId`='$id')  ";
		$result = $db->RunQuery($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Deleted successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	echo json_encode($response);
?>
<?php
session_start();
$backwardseperator = "../../../../../";
$companyId = $_SESSION['CompanyID'];
$thisFilePath =  $_SERVER['PHP_SELF'];

include  "{$backwardseperator}dataAccess/permisionCheck.inc";
$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$companyId = $row['intCompanyId']; 
}
if($_SERVER['REQUEST_METHOD'] == "POST")
{
	$optionValue = trim($_REQUEST['cboSearchOption']);
	$searchValue = trim($_REQUEST['txtSearchValue']);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Customer Item Listing</title>

<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../../css/tblstyle.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../../libraries/jquery/jquery-1.3.2.min.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/scrolltable.js"></script>

<script type="application/javascript" src="../../../../../libraries/javascript/zebraStripe.js"></script>

<script type="text/javascript" language="javascript">
function pageSubmit()
{
	document.getElementById('frmCurrencyListing').submit();	
}
function clearValue()
{
	document.getElementById('txtSearchValue').value = '';
	document.getElementById('txtSearchValue').focus();
}
</script>

</head>

<body>
<form id="frmCustomerItemListing" name="frmCustomerItemListing" method="post" autocomplete="off" action="customerItemListing.php">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">Customer Item Listing</div>
		  <table width="800" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="800" border="0">
      <tr>
        <td><table width="800" border="0" cellpadding="1" cellspacing="0" class="tableBorder_allRound">
          <tr>
            <td width="16%" height="22" class="normalfnt">&nbsp;</td>
            <td width="18%" class="normalfntMid">Searching Option</td>
            <td width="17%" class="normalfnt">
            <select name="cboSearchOption" id="cboSearchOption" onchange="clearValue();" style="width:150px">
              <option value="">&nbsp;</option>
<option value="mst_financecustomeritem.strName" <?php echo($optionValue=='mst_financecustomeritem.strName'?'selected':'') ?> >Item Name</option>
            </select></td>
            <td width="16%"><input value="<?php echo $searchValue; ?>" type="text" name="txtSearchValue" id="txtSearchValue" /></td>
            <td width="18%" class="normalfnt">
            <a href="#">
            <img src="../../../../../images/search.png" width="20" height="20" onclick="pageSubmit();" /></a></td>
            <td width="15%">&nbsp;</td>
            </tr>
          <tr>
            <td height="22" class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td>
          <div id="_table_wrap" style="overflow: hidden; display: inline-block; border: 1px solid rgb(136, 136, 136);">
          <div id="_head_wrap" style="width: 800px; overflow: hidden;">
          <table class="tbl1 scroll" id="_head" style="table-layout: fixed;">
          <thead>
            <tr class="gridHeader">
              <th height="22" style="width: 58px;" >Item Name</th>
              <th style="width: 110px;" ><strong>Description</strong></th>
              <th style="width: 170px;" ><strong> Account</strong></th>
              <th style="width: 40px;" ><strong>Active</strong></th>
              <th style="width: 40px;" >View</th>
              <th style="width: 8px;" ></th>
              </tr>
           </table>
           </div>
           <div class="normalfnt" id="_body_wrap" style="width: 800px; height: 310px; overflow: scroll;">
           <table class="tbl1 scroll"> 
           <tbody>
              <?php

				if($searchValue!='')
			  	$wherePart = "WHERE $optionValue like '%$searchValue%'";
				
	 	 		 $sql = "SELECT
						mst_financecustomeritem.intId AS cusItmId,
						mst_financecustomeritem.strName AS itemName,
						mst_financecustomeritem.strRemark AS strRemark,
						mst_financecustomeritem.intStatus,
						(select A.intChartOfAccountId
							from 
							mst_financecustomeritemactivate A where A.intCustomerItemId =mst_financecustomeritem.intId 
							and A.intCompanyId='$companyId'
						) as chartId,
						(
						SELECT
						mst_financechartofaccounts.strName
						FROM
						mst_financecustomeritemactivate AS A
						Inner Join mst_financechartofaccounts ON mst_financechartofaccounts.intId = A.intChartOfAccountId
						where A.intCustomerItemId =mst_financecustomeritem.intId 
						and A.intCompanyId='$companyId'
						) as charOfAccName
						FROM
						mst_financecustomeritem 
						$wherePart
						";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$id 		=	 $row['cusItmId'];
	  			 ?>
           		<tr class="normalfnt">
           		<td class="cusItemName" height="16" align="center" bgcolor="#FFFFFF">
           		  <?php echo $row['itemName'];?></td>
              	<td class="cusItemDesc" bgcolor="#FFFFFF"><?php echo $row['strRemark'];?></td>
              	<td class="cusItemAcc" align="center" bgcolor="#FFFFFF"><?php echo $row['charOfAccName'];?></td>
              	<td class="active" align="center" bgcolor="#FFFFFF">
              	  <input disabled="disabled" <?php echo($row['intStatus']?'checked':''); ?> type="checkbox"/></td>
              	<td class="view" align="center" bgcolor="#FFFFFF"><a target="_blank" href="<?php echo "../addNew/customerItem.php?id=$id";?>">More</a></td>
              </tr>
               <?php 
        	    } 
       		   ?>
              </tbody>
             </table>
            </div></div>
              </td>
              </tr>
            </table>
			</td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound"><a href="../../../../../main.php"><img  src="../../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
</body>
</html>

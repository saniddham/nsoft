var basePath	= "presentation/finance/masterData/paymentsMethods/" ;	
var menuId		= 582 ;		
$(document).ready(function() {
		var id = '';
		var paymentName = '';
  		$("#frmPaymentsMethods").validationEngine();
		$('#frmPaymentsMethods #txtName').focus();
  //permision for add 
  /*if(intAddx)
  {
 	$('#frmPaymentsMethods #butNew').show();
	$('#frmPaymentsMethods #butSave').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmPaymentsMethods #butSave').show();
	$('#frmPaymentsMethods #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmPaymentsMethods #butDelete').show();
	$('#frmPaymentsMethods #cboSearch').removeAttr('disabled');
  }
  
  //permision for view
  if(intViewx)
  {
	$('#frmPaymentsMethods #cboSearch').removeAttr('disabled');
  }*/
  
$('.loadId').css('cursor', 'pointer');
$('.loadId').off('mouseover').on('mouseover',function () {
$(this).css('color', 'red');
});
$('.loadId').off('mouseout').on('mouseout',function () {
$(this).css('color', 'black');
});
  ///save button click event
  $('#frmPaymentsMethods #butSave').off('click').on('click',function(){
	var requestType = '';
	if ($('#frmPaymentsMethods').validationEngine('validate'))   
    { 
		if(paymentName=='')
			requestType = 'add';
		else
			requestType = 'edit';
		
		var url = basePath+"paymentsMethods-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json",
			type: 'post',  
			data:$("#frmPaymentsMethods").serialize()+'&requestType='+requestType+'&cboSearch='+id,
			async:false,
			
			success:function(json){
					$('#frmPaymentsMethods #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						id = '';
						paymentName = '';
						$('#frmPaymentsMethods').get(0).reset();
						var t=setTimeout("alertx()",1000);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmPaymentsMethods #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
				}		
			});
	}
   });
   
   /////////////////////////////////////////////////////
   //// load payments methods details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmPaymentsMethods #tblMainGridRow').off('click').on('click',function(){
	   $('#frmPaymentsMethods').validationEngine('hide');
   });
   $('#tblMainGrid .loadId').off('click').on('click',function() {
    	id = $(this).attr('id');
		$('#frmPaymentsMethods').validationEngine('hide');
		var url = basePath+"paymentsMethods-db-get.php";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:'requestType=loadDetails&id='+id,
			async:false,
			
			success:function(json){
					$('#frmPaymentsMethods #txtName').val(json.name);
					$('#frmPaymentsMethods #txtDesc').val(json.description);
					$('#frmPaymentsMethods #txtDescount').val(json.discount);
					$('#frmPaymentsMethods #txtDescountDays').val(json.discountdays);
					if(json.status)
						$('#frmPaymentsMethods #chkActive').attr('checked',true);
					else
						$('#frmPaymentsMethods #chkActive').removeAttr('checked');
			}
	}
	);
	paymentName = $('#frmPaymentsMethods #txtName').val();
	//////////// end of load details /////////////////
	});
	
	$('#frmPaymentsMethods #butNew').off('click').on('click',function(){
		$('#frmPaymentsMethods').get(0).reset();
		id = '';
		paymentName = '';
		$('#frmPaymentsMethods #txtName').focus();
		loadCombo_frmPaymentsMethods();
	});
    $('#frmPaymentsMethods #butDelete').off('click').on('click',function(){
		if(paymentName =='')
		{
			$('#frmPaymentsMethods #butDelete').validationEngine('showPrompt', 'Please select Name.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+paymentName+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = basePath+"paymentsMethods-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											type:'post',
											data:'requestType=delete&cboSearch='+id,
											async:false,
											success:function(json){
												$('#frmPaymentsMethods #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													var t=setTimeout("alertDelete()",1000);
													return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
					}
				}
		 	});
		}
	});
});
function loadCombo_frmPaymentsMethods()
{
	window.location.href = '?q='+menuId;
}
function alertx()
{
	$('#frmPaymentsMethods #butSave').validationEngine('hide')	;
	loadCombo_frmPaymentsMethods();
}
function alertDelete()
{
	$('#frmPaymentsMethods #butDelete').validationEngine('hide')	;
	loadCombo_frmPaymentsMethods();
}

<?php 
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	$id 			= $_REQUEST['cboSearch'];
	$name			= trim($_REQUEST['txtName']);
	$description	= trim($_REQUEST['txtDesc']);
	$intStatus		= ($_REQUEST['chkActive']?1:0);		
	/////////// payments methods insert part /////////////////////
	if($requestType=='add')
	{
		 $sql = "INSERT INTO `mst_financepaymentsmethods` (`strName`,`strDescription`,`intStatus`,`intCreator`,dtmCreateDate) 
				VALUES ('$name','$description','$intStatus','$userId',now())";
		$result = $db->RunQuery($sql);
		if($result){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
	}
	/////////// payments methods update part /////////////////////
	else if($requestType=='edit')
	{
		$sql = "UPDATE `mst_financepaymentsmethods` SET 	strName			='$name',
													strDescription	='$description',
													intStatus		='$intStatus',
													intModifyer		='$userId'
				WHERE (`intId`='$id')";
		$result = $db->RunQuery($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Updated successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	/////////// payments methods delete part /////////////////////
	else if($requestType=='delete')
	{
		$sql = "DELETE FROM `mst_financepaymentsmethods` WHERE (`intId`='$id')  ";
		$result = $db->RunQuery($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Deleted successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	echo json_encode($response);
?>
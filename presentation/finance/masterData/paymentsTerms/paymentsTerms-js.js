var basePath	= "presentation/finance/masterData/paymentsTerms/";			
$(document).ready(function() {
		var id = '';
		var paymentName = '';
  		$("#frmPaymentsTerms").validationEngine();
		$('#frmPaymentsTerms #txtName').focus();
  //permision for add 
 /* if(intAddx)
  {
 	$('#frmPaymentsTerms #butNew').show();
	$('#frmPaymentsTerms #butSave').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmPaymentsTerms #butSave').show();
	$('#frmPaymentsTerms #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmPaymentsTerms #butDelete').show();
	$('#frmPaymentsTerms #cboSearch').removeAttr('disabled');
  }
  
  //permision for view
  if(intViewx)
  {
	$('#frmPaymentsTerms #cboSearch').removeAttr('disabled');
  }*/

$('.loadId').css('cursor', 'pointer');
$('.loadId').off('mouseover').on('mouseover',function () {
$(this).css('color', 'red');
});
$('.loadId').off('mouseout').on('mouseout',function () {
$(this).css('color', 'black');
});

  ///save button click event
  $('#frmPaymentsTerms #butSave').off('click').on('click',function(){
	var requestType = '';
	if ($('#frmPaymentsTerms').validationEngine('validate'))   
    { 
		if(paymentName=='')
			requestType = 'add';
		else
			requestType = 'edit';
		
		var url = basePath+"paymentsTerms-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json",  
			type: 'post',
			data:$("#frmPaymentsTerms").serialize()+'&requestType='+requestType+'&cboSearch='+id,
			async:false,
			
			success:function(json){
					$('#frmPaymentsTerms #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						id = '';
						paymentName = '';
						$('#frmPaymentsTerms').get(0).reset();
						var t=setTimeout("alertx()",1000);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmPaymentsTerms #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
				}		
			});
	}
   });
   
   /////////////////////////////////////////////////////
   //// load payments terms details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmPaymentsTerms #tblMainGridRow').off('click').on('click',function(){
	   $('#frmPaymentsTerms').validationEngine('hide');
   });
   $('#tblMainGrid .loadId').off('click').on('click',function() {
    	id = $(this).attr('id');
		$('#frmPaymentsTerms').validationEngine('hide');
		var url = basePath+"paymentsTerms-db-get.php";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:'requestType=loadDetails&id='+id,
			async:false,
			
			success:function(json){
					$('#frmPaymentsTerms #txtName').val(json.name);
					$('#frmPaymentsTerms #txtDesc').val(json.description);
					$('#frmPaymentsTerms #txtDescount').val(json.discount);
					$('#frmPaymentsTerms #txtDescountDays').val(json.discountdays);
					if(json.status)
						$('#frmPaymentsTerms #chkActive').attr('checked',true);
					else
						$('#frmPaymentsTerms #chkActive').removeAttr('checked');
			}
	}
	);
	paymentName = $('#frmPaymentsTerms #txtName').val();
	//////////// end of load details /////////////////
	});
	
	$('#frmPaymentsTerms #butNew').off('click').on('click',function(){
		$('#frmPaymentsTerms').get(0).reset();
		id = '';
		paymentName = '';
		$('#frmPaymentsTerms #txtName').focus();
		loadCombo_frmPaymentsTerms();
	});
    $('#frmPaymentsTerms #butDelete').off('click').on('click',function(){
		if(paymentName =='')
		{
			$('#frmPaymentsTerms #butDelete').validationEngine('showPrompt', 'Please select Name.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
			return;
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+paymentName+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = basePath+"paymentsTerms-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											type:'post',
											data:'requestType=delete&cboSearch='+id,
											async:false,
											success:function(json){
												$('#frmPaymentsTerms #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													var t=setTimeout("alertDelete()",1000);
													return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
					}
				}
		 	});
		}
	});
});
function loadCombo_frmPaymentsTerms()
{
	location.reload();
}
function alertx()
{
	$('#frmPaymentsTerms #butSave').validationEngine('hide')	;
	loadCombo_frmPaymentsTerms();
}
function alertDelete()
{
	$('#frmPaymentsTerms #butDelete').validationEngine('hide')	;
	loadCombo_frmPaymentsTerms();
}

<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>

<title>Payments Terms</title>
<!--<script type="text/javascript" src="presentation/finance/masterData/paymentsTerms/paymentsTerms-js.js"></script>-->

<form id="frmPaymentsTerms" name="frmPaymentsTerms" method="post">
<div align="center">
  <div class="trans_layoutD">
	<div class="trans_text">Payments Terms</div>
    <table width="100%">
<tr>
<td width="100%"><table width="100%">
  <tr>
    <td width="7" class="normalfnt">&nbsp;</td>
    <td width="119" class="normalfnt">Terms  <span class="compulsoryRed">*</span></td>
    <td colspan="2" bgcolor="#FFFFFF" class=""><input type="text" name="txtName" id="txtName" style="width:95px" class="validate[required,custom[integer],maxSize[100]]" /> <strong class="normalfnt">days</strong></td>
    <td width="133" bgcolor="#FFFFFF" class=""><span class="normalfnt">Description <span class="compulsoryRed">*</span></span></td>
    <td width="150" bgcolor="#FFFFFF" class=""><input name="txtDesc" type="text" id="txtDesc" style="width:150px" class="validate[required,maxSize[250]]"/></td>
    <td width="9" bgcolor="#FFFFFF" class="">&nbsp;</td>
  </tr>
  <tr>
    <td class="normalfnt">&nbsp;</td>
    <td width="119" class="normalfnt">Discount percentage</td>
    <td width="95" bgcolor="#FFFFFF" class="">
    <input name="txtDescount" type="text" id="txtDescount" class="validate[custom[number]]" style="width:65px"/>
      <strong><span class="normalfnt">%</span></strong></td>
    <td width="47" bgcolor="#FFFFFF" class="">&nbsp;</td>
    <td bgcolor="#FFFFFF" class=""><span class="normalfnt">Discount if paid within</span></td>
    <td bgcolor="#FFFFFF" class="">
    <input name="txtDescountDays" type="text" id="txtDescountDays" style="width:65px" class="validate[custom[integer]]" />      <strong class="normalfnt">days</strong></td>
    <td bgcolor="#FFFFFF" class="">&nbsp;</td>
  </tr>
  <tr>
    <td class="normalfnt">&nbsp;</td>
    <td class="normalfnt">Terms is inactive</td>
    <td bgcolor="#FFFFFF" class=""><input name="chkActive" type="checkbox" id="chkActive" checked="checked" /></td>
    <td bgcolor="#FFFFFF" class="">&nbsp;</td>
    <td bgcolor="#FFFFFF" class="">&nbsp;</td>
    <td bgcolor="#FFFFFF" class="">&nbsp;</td>
    <td bgcolor="#FFFFFF" class="">&nbsp;</td>
  </tr>
</table></td>
</tr>
<tr>
  <td><div style="overflow:scroll;width:100%;height:150px;" id="divGrid">
    <table width="100%" id="tblMainGrid" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
      <tr class="">
        <td width="12%" height="27"    bgcolor="#FAD163" class="normalfntMid"><span class="normalfnt"><strong>Terms</strong></span></td>
        <td width="18%"    bgcolor="#FAD163" class="normalfntMid"><span class="normalfnt"><strong>Description</strong></span></td>
        <td width="25%"    bgcolor="#FAD163" class="normalfntMid"><span class="normalfnt"><strong>Discount percentage</strong></span></td>
        <td  bgcolor="#FAD163" class="normalfntMid"  ><span class="normalfnt"><strong>Discount if paid within</strong></span></td>
        <td  bgcolor="#FAD163" class="normalfntMid"  ><span class="normalfnt"><strong>Terms is inactive</strong></span></td>
      </tr>
      <?php
	  $sql = "SELECT
				intId,
				strName,
				strDescription,
				dblDiscount,
				intDiscountDays,
				intStatus
				FROM mst_financepaymentsterms";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
	  ?>
      	<tr class="normalfnt">
<td id=<?php echo $row['intId'];?> class="loadId" align="center" bgcolor="#FFFFFF"><u><?php echo $row['strName'];?> days </u></td>
        <td align="center" bgcolor="#FFFFFF"><?php echo $row['strDescription'];?></td>
        <td align="center" bgcolor="#FFFFFF"><?php echo $row['dblDiscount'];?></td>
        <td align="center" bgcolor="#FFFFFF" class=""><?php echo $row['intDiscountDays'];?> days</td>
        <td align="center" bgcolor="#FFFFFF" class=""><input disabled="disabled" <?php echo($row['intStatus']?'checked':''); ?> type="checkbox"/></td>
      </tr>
      <?php 
        } 
       ?>
    </table>
  </div></td>
</tr>
<tr>
  <td width="100%" align="center" bgcolor=""><img border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24" class="mouseover" id="butNew" tabindex="28"/><?php echo($form_permision['edit']==1?'<img border="0" src="images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/>':'');echo($form_permision['delete']==1?'<img border="0" src="images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="25"/>':'');?><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
</tr>
</table>
</div>
</div>
</form>
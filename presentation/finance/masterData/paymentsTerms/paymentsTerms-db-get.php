<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	
	/////////// payments terms load part /////////////////////
	if($requestType=='loadCombo')
	{
		$sql =  $sql = "SELECT
				intId,
				strName,
				strDescription,
				dblDiscount,
				intDiscountDays,
				intStatus
				FROM mst_financepaymentsterms";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['name'] 			= $row['strName'];
			$response['description'] 	= $row['strDescription'];
			$response['discount'] 		= $row['dblDiscount'];
			$response['discountdays'] 	= $row['intDiscountDays'];
			$response['status'] 		= ($row['intStatus']?true:false);
		}
		echo json_encode($response);
	}
	else if($requestType=='loadDetails')
	{
		$id  = $_REQUEST['id'];
		$sql =  $sql = "SELECT
				intId,
				strName,
				strDescription,
				dblDiscount,
				intDiscountDays,
				intStatus
				FROM mst_financepaymentsterms
				WHERE
					intId =  '$id'
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['name'] 			= $row['strName'];
			$response['description'] 	= $row['strDescription'];
			$response['discount'] 		= $row['dblDiscount'];
			$response['discountdays'] 	= $row['intDiscountDays'];
			$response['status'] 		= ($row['intStatus']?true:false);
		}
		echo json_encode($response);
	}
	
?>
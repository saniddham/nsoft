<?php
include_once("class/tables/mst_customer.php");         $obj_mstCustomer = new mst_customer($db);
include_once("class/tables/mst_country.php");         $obj_mstCountry = new mst_country($db);
include_once("class/tables/mst_financecurrency.php");         $obj_mstfinancecurrency= new mst_financecurrency($db);
include_once("class/tables/mst_typeofmarketer.php");         $obj_msttypeofmarketer = new mst_typeofmarketer($db);
//echo $obj_mstCountry->getstrCountryName();

$cusID = $_REQUEST['id'];




?>

<head>
<title>Block Customer Details</title>
</head>
<body>
   
<form id="frmCustomer_block" name="frmCustomer_block" method="post" >
<div align="center">
		<div class="trans_layoutD">

                    <div class="trans_text">Block Customer Details</div>
                    
                    <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
                         <td><table width="585" border="0" align="center">
      <tr>
        <td width="62%"><table width="100%" border="0" > 
          <tr>
            <td height="47" ><table width="100%" border="0" class="">
                    
                <?php
                    if($cusID<0){
                ?>
                    <tr>
                <td height="25" class="normalfnt">&nbsp;</td>
                <td align="left" class="normalfnt">Customer</td>
                <td width="428" colspan="3">
                <select name="cboSearch_cus" class="txtbox" id="cboSearch_cus"  style="width:100%">
             
                <?php  
               $db->connect();$db->begin();
               
              echo $obj_mstCustomer ->getCombo($cusID, $where);
               
                 ?> 
                </select>
                  </td>
              </tr>
              <?php
                    } 
                    
              else{
             ?>
             
               <tr>
                <td height="25" class="normalfnt">&nbsp;</td>
                <td align="left" class="normalfnt">Customer</td>
                <td width="428" colspan="3">
                <select name="cboSearch_cus" class="txtbox" id="cboSearch_cus"  style="width:100%">
             
                 <?php  
               $db->connect();$db->begin();
               
              echo $obj_mstCustomer ->getCombo($cusID, $where);
               

                 ?> 
                </select>
                  </td>
              </tr>
             <?php
                 }
               ?>
              
                 <tr>
                <td width="1" class="normalfnt">&nbsp;</td>
                <td width="130" class="normalfnt">&nbsp;</td>
                <td colspan="3">&nbsp;</td>
              </tr>
              <tr>
             <td class="normalfnt">&nbsp;</td>
                 <td colspan="4" class="normalfnt"><table width="100%" border="0" class="tableBorder">
                    <tr>
                     <tr class="">
                    <td colspan="4" align="left" bgcolor="#E4E4E4" class="tableBorder">
                    <span class="normalfnt"><strong>General</strong></span></td>
                    </tr>  
                    
                    <td width="23%" class="normalfnt">Customer Code&nbsp;</td>
                    <td width="35%">
                        <input value="<?php  $raw_code=$obj_mstCustomer->set($cusID); 
                         if($cusID > 0){
                      echo $obj_mstCustomer->getstrCode($raw_code);} ?>"
                    name="txtCode" type="text"  id="txtCode" style="width:140px"  disabled="disabled"/>
                    </td>
                    
                    <td width="13%" class="normalfnt">Type&nbsp;</td>
                    <td width="29%">
                       <input  name="txtType" type="text"  id="txtType" style="width:140px" disabled="disabled" value="<?php
                       $raw_code=$obj_mstCustomer->set($cusID); 
                        if($cusID > 0){
                        //$raw_code=$obj_mstCustomer->set($cusID);
                       $type= $obj_mstCustomer->getintTypeId($raw_code);
                       if($type!=null){
                       $set_type =$obj_msttypeofmarketer->set($type);
                       echo $obj_msttypeofmarketer->getstrName($set_type);
                       }
                        }?>
                      "/> 
                    </td>
                    </tr>
                    
                      <tr class="">
                    <td height="30" class="normalfnt">Customer Name </td>
                    <td colspan="3">
                    <input name="txtName" type="text" id="txtName" style="width:100%" maxlength="50" disabled="disabled" value="<?php $raw_code=$obj_mstCustomer->set($cusID); 
                     if($cusID > 0){
                       echo $obj_mstCustomer->getstrName($raw_code);}?>"/></td>
                    </tr>
                    
                     <tr class="">
                    <td class="normalfnt">Address</td>
                    <td colspan="3">
                        <textarea name="txtAddress" style="width:100%"  rows="2" id="txtAddress" disabled="disabled"> <?php $raw_code=$obj_mstCustomer->set($cusID); 
                       if($cusID > 0){
                       echo $obj_mstCustomer->getstrAddress($raw_code);} ?></textarea></td>
                    </tr>
                    
                     <tr class="">
                    <td class="normalfnt">City</td>
                    <td><input name="txtCity" type="text" id="txtCity" style="width:140px" disabled="disabled" value="<?php  $raw_code=$obj_mstCustomer->set($cusID);  
                     if($cusID > 0){
                     echo $obj_mstCustomer->getstrCity($raw_code);} ?>"/></td>  
                    
                   
                    <td class="normalfnt">Country </td>
                    <td>
                        <input  name="txtCntry" type="text"  id="txtCntry" style="width:140px" disabled="disabled" value="<?php  $raw_code=$obj_mstCustomer->set($cusID); 
                       // $country= $obj_mstCustomer->getintCountryId($raw_code); $set_country=$obj_mstCountry->set($country);
                        if($cusID > 0){
                            $country= $obj_mstCustomer->getintCountryId($raw_code);
                            if($country!=null){
                            $set_country=$obj_mstCountry->set($country);
                            echo $obj_mstCountry->getstrCountryName($set_country);} }?>"/>
                    </td>
                  </tr
                  
                   <tr class="">
                    <td class="normalfnt">Contact Person</td>
                    <td colspan="3">
                <textarea name="txtContact" style="width:100%; height:20px"   id="txtContact" disabled="disabled"><?php  $raw_code=$obj_mstCustomer->set($cusID);  
                if($cusID > 0){
                echo $obj_mstCustomer->getstrContactPerson($raw_code);} ?></textarea></td>
                    </tr> 
                    
                    <tr class="">
                    <td class="normalfnt"> Currency </td>
                    <td>
                        <input value="<?php  $raw_code=$obj_mstCustomer->set($cusID);
                        if($cusID>0){
                        $currency= $obj_mstCustomer->getintCurrencyId($raw_code);
                        if($currency=!null){
                        $set_currency=$obj_mstfinancecurrency->set($currency);
                        echo $obj_mstfinancecurrency->getstrCode($set_currency);
                        }}?>
                       " name="txtCurrency" type="text"  id="txtCurrency" style="width:140px" disabled="disabled"/>
                    </td>
                    <td class="normalfnt">&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>
                     
                     
                  <tr class="">
                      <td class="normalfnt"> Block Customer</td>
                      <td colspan="3"> Sample <input type="checkbox" name="sample" id ="sample" value="1" style="width:50px"
                           <?php  $raw_code=$obj_mstCustomer->set($cusID);  
                if($cusID > 0){
                $data=$obj_mstCustomer->getactiveSample($raw_code);
                if($data==1){
                    
                    echo 'checked';
                }
                
                    } ?> >
                
                          
                        
                          Bulk <input type="checkbox" name="bulk"  id="bulk" value="1"  style="width:50px"  <?php  $raw_code=$obj_mstCustomer->set($cusID);  
                if($cusID > 0){
                $data=$obj_mstCustomer->getactiveBulk($raw_code);
                if($data==1){
                    
                    echo 'checked';
                }
                
                    } ?>  >
                      </td>
                      
                     
                  </tr>
                     
                   
                     
                     
                 </table>     
                  <tr class="">
                     
                    <td colspan="8" align="center"><a id="butsave" class="button green meedium" title="click here" name="save" >save</a></td>
                  <div style="hidden" id="saved"></div>
                 
                  </tr> 
                 </td>
                  
                  
              </tr>
              
              
              
              
           
              
              
              
                        
                        
                         </tr> </table></td>
                 </tr></table></td>
              </tr></table></td>
              </table>
                </div>
</div>
</form>
</body>
<?php
	$backwardseperator = "../../../../../";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Other Payable / Service Supplier Listing</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
</head>

<body>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
<div align="center">
<div class="trans_layoutL">
		  <div class="trans_text">Other Payable / Service Supplier - Listing</div>
          
          <table width="395">
          <tr>
          	<td width="275"><table width="100%" border="0" class="bcgl2">
          	  <tr>
          	    <td>&nbsp;</td>
          	    <td>&nbsp;</td>
          	    <td>&nbsp;</td>
          	    <td>&nbsp;</td>
          	    <td>&nbsp;</td>
          	    <td>&nbsp;</td>
       	      </tr>
          	  <tr>
          	    <td>&nbsp;</td>
          	    <td>&nbsp;</td>
          	    <td>&nbsp;</td>
          	    <td>&nbsp;</td>
          	    <td>&nbsp;</td>
          	    <td>&nbsp;</td>
       	      </tr>
       	    </table></td>
          </tr>
          <tr><td>
          <div style="overflow:scroll;width:800px;height:350px;margin-top:10px" id="divGrid">
            <table width="779" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
              <tr class="">
                <td width="90"  height="24" bgcolor="#FAD163" class="normalfntMid"><strong>Id</strong></td>
                <td width="179"  bgcolor="#FAD163" class="normalfntMid"><strong>Code</strong></td>
                <td width="443"  bgcolor="#FAD163" class="normalfntMid"><strong>Name</strong></td>
                <td width="62" bgcolor="#FAD163" class="normalfntMid"  ><strong>View</strong></td>
              </tr>
              <?php
	 	 		 $sql = "SELECT
				 intId,
				 strCode,
				 strName
				 FROM mst_finance_service_supplier
				 WHERE
				 intStatus = '1'
				 Order by intId";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
	  			 ?>
              <tr class="normalfnt" style="background:#DDECD5">
                <td bgcolor="#DDECD5" class="normalfntMid"><?php echo $row['intId'];?></td>
                <td bgcolor="#DDECD5"><?php echo $row['strCode'];?></td>
                <td bgcolor="#DDECD5"><?php echo $row['strName'];?></td>
                <td  bgcolor="#E9FFD2" class="normalfntMid"><a target="_blank" href="<?php echo "../addNew/supplier.php?id=$row[intId]"; ?>">More</a></td>
              </tr>
              <?php 
        		 } 
       		  ?>
            </table>
          
          </div>
          </td>
          </tr>
           <tr>
        <td align="center" class="tableBorder_allRound"><a href="../../../../../main.php"><img  src="../../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
      </tr>
          </table>
</div>
</div>

</body>
</html>

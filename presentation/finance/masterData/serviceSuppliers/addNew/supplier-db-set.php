<?php 
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 		= $_REQUEST['requestType'];
	$id 				= $_REQUEST['cboSearch'];
	$code				= trim($_REQUEST['txtCode']);
	$name				= trim($_REQUEST['txtName']);
	$type				= ($_REQUEST['cboType']==''?'NULL':$_REQUEST['cboType']);
	$address			= $_REQUEST['txtAddress'];
	$contact			= $_REQUEST['txtContact'];
	$chequename			= $_REQUEST['txtPrintCheque'];
	$city				= trim($_REQUEST['txtCity']);
	$country			= ($_REQUEST['cboCountry']==''?'NULL':$_REQUEST['cboCountry']);
	$currency			= ($_REQUEST['cboCurrency']==''?'NULL':$_REQUEST['cboCurrency']);
	$phone				= $_REQUEST['txtPhone'];
	$mobile				= $_REQUEST['txtMobile'];
	$fax				= $_REQUEST['txtFax'];
	$email				= trim($_REQUEST['txtEMail']);
	$web				= $_REQUEST['txtWeb'];
	$shipment 			= ($_REQUEST['cboShipmentMethod']==''?'NULL':$_REQUEST['cboShipmentMethod']);
	$partnercode		= $_REQUEST['txtIcCode'];
	$vatNo				= $_REQUEST['txtVatNo'];
	$sVatNo				= $_REQUEST['txtSvatNo'];
	$regNo				= $_REQUEST['txtRegNo'];
	$invoType			= $_REQUEST['invoType'];
	$accNo				= $_REQUEST['txtAccNo'];
	$paymentsterms 		= ($_REQUEST['cboPaymentsTerms']==''?'NULL':$_REQUEST['cboPaymentsTerms']);
	$paymentsmethods 	= ($_REQUEST['cboPaymentsMethods']==''?'NULL':$_REQUEST['cboPaymentsMethods']);
	$creditlimit 		= val($_REQUEST['txtCreditLimit']);
	$chartofaccount		= ($_REQUEST['cboChartOfAcc']==''?'NULL':$_REQUEST['cboChartOfAcc']);
	$leadtime			= $_REQUEST['txtLeadTime'];
	$blocked 			= $_REQUEST['cboBlocked'];
	$rank 				= val($_REQUEST['txtRank']);
	$intStatus		= ($_REQUEST['chkActive']?1:0);
	$interCompany 	= ($_REQUEST['chkInterComp']?1:0);
	$companyId 		= val($_REQUEST['cboInterCompany']);
	$locationId 		= val($_REQUEST['cboInterLocation']);
	if($companyId==''){
		$companyId=0;
	}
	if($locationId==''){
		$locationId=0;
	}
	//-----------------------------------------------
		//$locations = json_decode($_REQUEST['location'], true);
	//-----------------------------------------------
	///////// supplier insert part /////////////////////
	if($requestType=='add')
	{
		
		$sql = "INSERT INTO `mst_finance_service_supplier` (`strCode`,`strName`,`intTypeId`,`strAddress`,`strContactPerson`,`strPrintOnCheque`,`strCity`,`intCountryId`,`intCurrencyId`,`strPhoneNo`,`strMobileNo`,`strFaxNo`,`strEmail`,`strWebSite`,`intShipmentId`,`strIcCode`,`strVatNo`,`strSVatNo`,`strRegistrationNo`,`strInvoiceType`,`strAccNo`,`intPaymentsTermsId`,`intPaymentsMethodsId`,`intCreditLimit`,`intChartOfAccountId`,`strLeadTime`,`strBlocked`,`intRank`,`intStatus`,`intCreator`,`dtmCreateDate`,`intInterCompany`,`intInterLocation`,`intCompanyId`) 
				VALUES ('$code','$name',$type,'$address','$contact','$chequename','$city',$country,$currency,'$phone','$mobile','$fax','$email','$web',$shipment,'$partnercode','$vatNo','$sVatNo','$regNo','$invoType','$accNo',$paymentsterms,$paymentsmethods,'$creditlimit',$chartofaccount,'$leadtime','$blocked','$rank','$intStatus','$userId',now(),'$interCompany','$locationId','$companyId')";	
				
		$result = $db->autoInsertNo($sql);
		
/*		if(count($locations) != 0)
		{
		foreach($locations as $locat)
		{
			$locId = $locat['locationId'];
			$sql = "INSERT INTO `mst_finance_service_supplier_locations`
					(`intLocationId`,`intSupplierId`,`intCreator`,dtmCreateDate) 
					VALUES 
					('$locId','$supId','$userId',now())";
			$result = $db->RunQuery($sql);
		}
		}*/
		
		if($result){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
	}
	/////////// supplier update part /////////////////////
	else if($requestType=='edit')
	{
		/*$sql = "DELETE FROM `mst_finance_service_supplier_locations` WHERE (`intSupplierId`='$id')";
		$db->RunQuery($sql);
				
		foreach($locations as $locat)
		{
			$locId = $locat['locationId'];
			$sql = "INSERT INTO `mst_finance_service_supplier_locations`
					(`intLocationId`,`intSupplierId`,`intCreator`,dtmCreateDate) 
					VALUES 
					('$locId','$id','$userId',now())";
			$db->RunQuery($sql);
		}*/
		
		$sql = "UPDATE `mst_finance_service_supplier` SET 	strCode					='$code',
											strName					= '$name',
											intTypeId				= $type,
											strAddress				= '$address',
											strContactPerson		= '$contact',
											strPrintOnCheque		= '$chequename',
											strCity					= '$city',
											intCountryId			= $country,
											intCurrencyId			= $currency,
											strPhoneNo				= '$phone',
											strMobileNo				= '$mobile',
											strFaxNo				= '$fax',
											strEmail				= '$email',
											strWebSite				= '$web',
											intShipmentId			= $shipment,
											strIcCode				= '$partnercode',
											strVatNo				= '$vatNo',
											strSVatNo				= '$sVatNo',
											strRegistrationNo		= '$regNo',
											strInvoiceType			= '$invoType',
											strAccNo				= '$accNo',
											intPaymentsTermsId		= $paymentsterms,
											intPaymentsMethodsId	= $paymentsmethods,
											intCreditLimit			= '$creditlimit',
											intChartOfAccountId		= $chartofaccount,
											strLeadTime				= '$leadtime',
											strBlocked				= '$blocked',
											intRank					= '$rank',
											intStatus				= '$intStatus',
											intModifyer				= '$userId',
											intInterCompany			= '$interCompany',
											intInterLocation		= '$locationId',
											intCompanyId			= '$companyId'
											
				WHERE (`intId`='$id')";
		$result = $db->RunQuery($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Updated successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	/////////// supplier delete part /////////////////////
	else if($requestType=='delete')
	{	
		$sql = "DELETE FROM `mst_finance_service_supplier` WHERE (`intId`='$id')  ";
		$result = $db->RunQuery($sql);
		if(($result)){
			
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Deleted successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	echo json_encode($response);
?>

var arrLocate = [];

var invoType = "";
function functionList()
{
    if(supId!='')
    {
        $('#frmServiceSupplier #cboSearch').val(supId);
        $('#frmServiceSupplier #cboSearch').change();
    }
}
$(document).ready(function() {
  		$("#frmServiceSupplier").validationEngine();
		$('#frmServiceSupplier #txtCode').focus();
  //permision for add 
  if(intAddx)
  {
 	$('#frmServiceSupplier #butNew').show();
	$('#frmServiceSupplier #butSave').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmServiceSupplier #butSave').show();
	$('#frmServiceSupplier #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmServiceSupplier #butDelete').show();
	$('#frmServiceSupplier #cboSearch').removeAttr('disabled');
  }
  
  //permision for view
  if(intViewx)
  {
	$('#frmServiceSupplier #cboSearch').removeAttr('disabled');
  }
  
/*  $(':input').bind('keyup mouseup', function(){
      $(this).val($.trim($(this).val())) ;
    });*/
	
	//-------------Hemanthi-----26/06/2012------------
  $('#frmServiceSupplier #chkInterComp').click(function(){
		if(document.getElementById('chkInterComp').checked==true){ 
		$('#frmServiceSupplier #cboInterCompany').removeAttr('disabled');
		}
		else{
		$('#frmServiceSupplier #cboInterCompany').attr('disabled', true);
		$('#frmServiceSupplier #cboInterLocation').attr('disabled', true);
		$('#frmServiceSupplier #cboInterCompany').val('');
		$('#frmServiceSupplier #cboInterLocation').val('');
		}
  });
	//-------------Hemanthi-----26/06/2012------------
  $('#frmServiceSupplier #cboInterCompany').change(function(){
	    var company= $('#frmServiceSupplier #cboInterCompany').val();
		var url 	= "supplier-db-get.php?requestType=loadLocations"+"&company="+company;
		var httpobj = $.ajax({url:url,async:false})
		$('#frmServiceSupplier #cboInterLocation').html(httpobj.responseText);
		$('#frmServiceSupplier #cboInterLocation').removeAttr('disabled');
  });
	//-----------------------------------------------
  ///save button click event
  $('#frmServiceSupplier #butSave').click(function(){
	
		if(document.getElementById('chkInterComp').checked==true){ 
			if($('#frmServiceSupplier #cboInterCompany').val()==''){
			alert("Please select Company");
			return false;
			}
			else if($('#frmServiceSupplier #cboInterLocation').val()==''){
			alert("Please select Location");
			return false;
			}
		}
		
/*	var locationValue="[ ";
		$('.locations:checked').each(function(){
			locationValue += '{ "locationId":"'+$(this).val()+'"},';
		});
		locationValue = locationValue.substr(0,locationValue.length-1);
		locationValue += " ]";*/
		
	var requestType = '';
	if ($('#frmServiceSupplier').validationEngine('validate'))   
    { 
	
	
		//$('#txtLocationCode').validationEngine('showPrompt', 'This is an example', 'pass'); //hide all alerts//
		if($("#frmServiceSupplier #cboSearch").val()=='')
			requestType = 'add';
		else
			requestType = 'edit';
		
		var url = "supplier-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json",  
		data:$("#frmServiceSupplier").serialize()+'&requestType='+requestType+'&invoType='+invoType,
			//data:'{"requestType":"addLocation"}',
			async:false,
			
			success:function(json){
					$('#frmServiceSupplier #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmServiceSupplier').get(0).reset();
						loadCombo_frmServiceSupplier();
						var t=setTimeout("alertx()",1000);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmServiceSupplier #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
						$('#frmServiceSupplier #cboInterCompany').attr('disabled',true);
   });
   
   /////////////////////////////////////////////////////
   //// load supplier details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmServiceSupplier #cboSearch').click(function(){
	   $('#frmServiceSupplier').validationEngine('hide');
   });
   
    $('#frmServiceSupplier #cboSearch').change(function(){
		$('#frmServiceSupplier').validationEngine('hide');
		var url = "supplier-db-get.php";
		if($('#frmServiceSupplier #cboSearch').val()=='')
		{
			$('#frmServiceSupplier').get(0).reset();return;	
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:'requestType=loadDetails&id='+$(this).val(),
			async:false,
			success:function(json){
					//json  = eval('('+json+')');
					$('#frmServiceSupplier #txtCode').val(json.code);
					$('#frmServiceSupplier #txtName').val(json.name);
					$('#frmServiceSupplier #cboType').val(json.type);
					$('#frmServiceSupplier #txtAddress').val(json.address);
					$('#frmServiceSupplier #txtContact').val(json.contact);
					$('#frmServiceSupplier #txtPrintCheque').val(json.chequename);
					$('#frmServiceSupplier #txtCity').val(json.city);
					$('#frmServiceSupplier #cboCountry').val(json.country);
					$('#frmServiceSupplier #cboCurrency').val(json.currency);
					$('#frmServiceSupplier #txtPhone').val(json.phone);
					$('#frmServiceSupplier #txtMobile').val(json.mobile);
					$('#frmServiceSupplier #txtFax').val(json.fax);
					$('#frmServiceSupplier #txtEMail').val(json.email);
					$('#frmServiceSupplier #txtWeb').val(json.web);
					$('#frmServiceSupplier #cboShipmentMethod').val(json.shipment);
					$('#frmServiceSupplier #txtIcCode').val(json.partnercode);
					$('#frmServiceSupplier #txtVatNo').val(json.vatNo);
					$('#frmServiceSupplier #txtSvatNo').val(json.sVatNo);
					$('#frmServiceSupplier #txtRegNo').val(json.regNo);
					if(json.invoType){
					$('#frmServiceSupplier #'+json.invoType).attr('checked',true); //
					invoType = json.invoType;
					}
					$('#frmServiceSupplier #txtAccNo').val(json.accNo);
					$('#frmServiceSupplier #cboPaymentsTerms').val(json.payterms);
					$('#frmServiceSupplier #cboPaymentsMethods').val(json.paymethods);
					$('#frmServiceSupplier #txtCreditLimit').val(json.creditlimit);
					$('#frmServiceSupplier #cboChartOfAcc').val(json.chartofaccount);
					$('#frmServiceSupplier #txtLeadTime').val(json.leadtime);
					$('#frmServiceSupplier #cboBlocked').val(json.blocked);
					$('#frmServiceSupplier #txtRank').val(json.rank);

					if(json.status)
						$('#frmServiceSupplier #chkActive').attr('checked',true);
					else
						$('#frmServiceSupplier #chkActive').removeAttr('checked');
						

					if(json.interCompany==1){
						$('#frmServiceSupplier #chkInterComp').attr('checked',true);
						$('#frmServiceSupplier #cboInterCompany').val(json.companyId);
						$('#frmServiceSupplier #cboInterCompany').removeAttr('disabled');
						$('#frmServiceSupplier #cboInterLocation').val(json.locationId);
						$('#frmServiceSupplier #cboInterLocation').removeAttr('disabled');
					}
					else
					{
						$('#frmServiceSupplier #chkInterComp').removeAttr('checked');
						$('#frmServiceSupplier #cboInterCompany').val('');
						$('#frmServiceSupplier #cboInterCompany').attr('disabled',true);
						$('#frmServiceSupplier #cboInterLocation').val('');
						$('#frmServiceSupplier #cboInterLocation').attr('disabled',true);
					}
						
						
						

				//--------------------------------------------------
				var locId = "";
				if(json.locatVal!=null){
				$('.locations:checkbox').removeAttr('checked');	
				for(var j=0;j<=json.locatVal.length-1;j++)
				{
					locId = json.locatVal[j].locateId;
					$('#frmServiceSupplier #loc'+locId).attr('checked',true);					
				}}
				else
				{
					$('.locations:checkbox').removeAttr('checked');
				}
				//--------------------------------------------------
			}
	});
	//////////// end of load details /////////////////
	
	});
	
	$('#frmServiceSupplier #butNew').click(function(){
		$('#frmServiceSupplier').get(0).reset();
		loadCombo_frmServiceSupplier();
		$('#frmServiceSupplier #txtCode').focus();
	});
    $('#frmServiceSupplier #butDelete').click(function(){
		if($('#frmServiceSupplier #cboSearch').val()=='')
		{
			$('#frmServiceSupplier #butDelete').validationEngine('showPrompt', 'Please select Supplier.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmServiceSupplier #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = "supplier-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											data:'requestType=delete&cboSearch='+$('#frmServiceSupplier #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmServiceSupplier #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													$('#frmServiceSupplier').get(0).reset();
													loadCombo_frmServiceSupplier();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
		$('.invTpe').click(function(){
		invoType = $(this).val();	
	});
});


function loadCombo_frmServiceSupplier()
{
	var url 	= "supplier-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmServiceSupplier #cboSearch').html(httpobj.responseText);
}

function alertx()
{
	$('#frmServiceSupplier #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmServiceSupplier #butDelete').validationEngine('hide')	;
}

function popupLocations()
{
	
	popupWindow('1');	
}

function loadLocations()
{
	$("#butLocations").click(popupLocations);
}

function closePopUp()
{
	arrLocate = [];
	
	var i = 0;

		$('.locations:checked').each(function(){
			arrLocate[i++]=$(this).val();
		});

	$("#tblLocations").load('table-location.php',function(){
	for(var j=0;j<=arrLocate.length-1;j++)
	{
		$('#frmServiceSupplier #loc'+arrLocate[j]).attr('checked',true);					
	}
	});
}
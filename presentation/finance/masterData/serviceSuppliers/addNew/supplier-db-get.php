<?php
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	
	/////////// supplier load part /////////////////////
	if($requestType=='loadCombo')
	{
		$sql = "SELECT
					intId,
					strName
				FROM mst_finance_service_supplier
				order by strName
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
	}
	else if($requestType=='loadLocations')
	{
		$company  = $_REQUEST['company'];
		$sql = "SELECT
				mst_locations.strName,
				mst_locations.intId,
				mst_locations.strCode
				FROM mst_locations
				WHERE
				mst_locations.intStatus =  '1' AND
				mst_locations.intCompanyId =  '$company'
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strCode']."-".$row['strName']."</option>";
		}
		echo $html;
	}
	else if($requestType=='loadDetails')
	{
		$id  = $_REQUEST['id'];
		
		//------------------------------
/*		$sql = "SELECT intLocationId
				FROM mst_finance_service_supplier_locations
				WHERE
					intSupplierId =  '$id'";
		$result = $db->RunQuery($sql);
	
		$arrlocat;
		while($row=mysqli_fetch_array($result))
		{
			$val['locateId'] 	= $row['intLocationId'];
			$arrlocat[] = $val;
		}
		
		$response['locatVal'] = $arrlocat;*/
		//-------------------------------
		
		$sql = "SELECT
					strCode,
					strName,
					intTypeId,
					strAddress,
					strContactPerson,
					strPrintOnCheque,
					strCity,
					intCountryId,
					intCurrencyId,
					strPhoneNo,
					strMobileNo,
					strFaxNo,
					strEmail,
					strWebSite,
					intShipmentId,
					strIcCode,
					strVatNo,
					strSVatNo,
					strRegistrationNo,
					strInvoiceType,
					strAccNo,
					intPaymentsTermsId,
					intPaymentsMethodsId,
					intCreditLimit,
					intChartOfAccountId,
					strLeadTime,
					strBlocked,
					intRank,
					intStatus,
					intInterCompany,
					intInterLocation,
					intCompanyId 
				FROM mst_finance_service_supplier
				WHERE
					intId =  '$id'
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['code'] 			= $row['strCode'];
			$response['name'] 			= $row['strName'];
			$response['type'] 			= $row['intTypeId'];
			$response['address'] 		= $row['strAddress'];
			$response['contact'] 		= $row['strContactPerson'];
			$response['chequename'] 	= $row['strPrintOnCheque'];
			$response['city'] 			= $row['strCity'];
			$response['country'] 		= $row['intCountryId'];
			$response['currency'] 		= $row['intCurrencyId'];
			$response['phone'] 			= $row['strPhoneNo'];
			$response['mobile'] 		= $row['strMobileNo'];
			$response['fax'] 			= $row['strFaxNo'];
			$response['email'] 			= $row['strEmail'];
			$response['web'] 			= $row['strWebSite'];
			$response['shipment'] 		= $row['intShipmentId'];
			$response['partnercode'] 	= $row['strIcCode'];
			$response['vatNo'] 			= $row['strVatNo'];
			$response['sVatNo'] 		= $row['strSVatNo'];
			$response['regNo'] 			= $row['strRegistrationNo'];
			$response['invoType'] 		= $row['strInvoiceType'];
			$response['accNo'] 			= $row['strAccNo'];
			$response['payterms'] 		= $row['intPaymentsTermsId'];
			$response['paymethods'] 	= $row['intPaymentsMethodsId'];
			$response['creditlimit'] 	= $row['intCreditLimit'];
			$response['chartofaccount'] = $row['intChartOfAccountId'];
			$response['leadtime'] 		= $row['strLeadTime'];
			$response['blocked'] 		= $row['strBlocked'];
			$response['rank'] 			= $row['intRank'];
			$response['status'] 		= ($row['intStatus']?true:false);
			$response['interCompany'] 	= $row['intInterCompany'];
			$response['locationId'] 	= $row['intInterLocation'];
			$response['companyId'] 		= $row['intCompanyId'];
		}
		echo json_encode($response);
	}
	
?>
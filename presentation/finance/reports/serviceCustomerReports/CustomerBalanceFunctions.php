<?php
function getCustomerCurrentBalance($custId,$toDay,$currency,$companyId){
    $inv=getInvoiceCurrent($custId,$toDay,$currency,$companyId);
	$billTax=getBillWiseTaxesCurrent($custId,$toDay,$currency,$companyId);
    
    return ($inv+$billTax);
}
function getCustomerBalance($custId,$from,$to,$toDay,$currency,$companyId){
    $inv=getFromInvoice($custId,$from,$to,$toDay,$currency,$companyId);
    $bankDeposit=getBankDeposit($custId,$from,$to,$toDay,$currency,$companyId);
    $bankPayment=getBankPayments($custId,$from,$to,$toDay,$currency,$companyId);
    $pattyCash=getPettyCash($custId,$from,$to,$toDay,$currency,$companyId);
    $jurnal=getJurnelEntry($custId,$from,$to,$toDay,$currency,$companyId);
	$billTax=getBillWiseTaxes($custId,$from,$to,$toDay,$currency,$companyId);
    return ($inv+$bankDeposit+$bankPayment+$pattyCash+$jurnal+$billTax);
    
}

function getInvoiceCurrent($custId,$toDay,$currency,$companyId){
    if($companyId==0)$wareCom=" ";
    else $wareCom=" AND fin_other_receivable_invoice_header.intCompanyId=$companyId ";
    global $db;
    
     $sql="SELECT
				sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0) 
				-
				IFNULL ((SELECT
				SUM(fin_other_receivable_payments_main_details.dblPayAmount )AS paidAmount
				FROM fin_other_receivable_payments_main_details
				Inner Join fin_other_receivable_payments_header ON fin_other_receivable_payments_main_details.strReferenceNo = fin_other_receivable_payments_header.strReferenceNo
				WHERE
				fin_other_receivable_payments_main_details.strJobNo =  INV.strReferenceNo AND
				fin_other_receivable_payments_header.intDeleteStatus =  '0' AND fin_other_receivable_payments_main_details.strDocType = 'O.SInvoice' AND
                                fin_other_receivable_payments_header.dtmDate<='$toDay'),0)
				
				) AS balAmount
				FROM
				fin_other_receivable_invoice_details AS INV
				Inner Join fin_other_receivable_invoice_header ON fin_other_receivable_invoice_header.strReferenceNo = INV.strReferenceNo AND INV.intInvoiceNo = fin_other_receivable_invoice_header.intInvoiceNo AND INV.intAccPeriodId = fin_other_receivable_invoice_header.intAccPeriodId AND INV.intLocationId = fin_other_receivable_invoice_header.intLocationId AND INV.intCompanyId = fin_other_receivable_invoice_header.intCompanyId
				Inner Join mst_finance_service_customer ON mst_finance_service_customer.intId = fin_other_receivable_invoice_header.intCustomerId
				INNER JOIN mst_financepaymentsterms AS PT ON fin_other_receivable_invoice_header.intPaymentsTermsId = PT.intId
				WHERE
				fin_other_receivable_invoice_header.intCustomerId = $custId AND
                                fin_other_receivable_invoice_header.intDeleteStatus = '0' AND
                                fin_other_receivable_invoice_header.intCurrencyId = '$currency' AND
                                fin_other_receivable_invoice_header.dtmDate <='$toDay' AND
                                DATEDIFF('$toDay',fin_other_receivable_invoice_header.dtmDate) < (PT.strName) ".$wareCom; 
     $result = $db->RunQuery($sql);
   
    $row = mysqli_fetch_array($result);
    return $row['balAmount'];
}

function getFromInvoice($custId,$from,$to,$toDay,$currency,$companyId){
    if($to==x)$wareTo=" ";
    else $wareTo=" AND DATEDIFF('$toDay',SIH.dtmDate)<=($to+ PT.strName) ";
    
    if($companyId==0)$wareCom="";
    else $wareCom=" AND SIH.intCompanyId=$companyId ";
    global $db;
    
    $result = $db->RunQuery("SELECT
							sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0) 
							-
							IFNULL ((SELECT
							SUM(fin_other_receivable_payments_main_details.dblPayAmount )AS paidAmount
							FROM fin_other_receivable_payments_main_details
							Inner Join fin_other_receivable_payments_header ON fin_other_receivable_payments_main_details.strReferenceNo = fin_other_receivable_payments_header.strReferenceNo
							WHERE
							fin_other_receivable_payments_main_details.strJobNo =  INV.strReferenceNo AND
							fin_other_receivable_payments_header.intDeleteStatus =  '0' AND fin_other_receivable_payments_main_details.strDocType = 'O.SInvoice' AND
							fin_other_receivable_payments_header.dtmDate<='$toDay'),0)
		) AS balAmount
						FROM
							fin_other_receivable_invoice_details AS INV
							INNER JOIN fin_other_receivable_invoice_header AS SIH ON SIH.strReferenceNo = INV.strReferenceNo AND INV.intInvoiceNo = SIH.intInvoiceNo AND INV.intAccPeriodId = SIH.intAccPeriodId AND INV.intLocationId = SIH.intLocationId AND INV.intCompanyId = SIH.intCompanyId
							INNER JOIN mst_finance_service_customer ON mst_finance_service_customer.intId = SIH.intCustomerId
							INNER JOIN mst_financepaymentsterms AS PT ON SIH.intPaymentsTermsId = PT.intId
						WHERE
							SIH.intCustomerId = $custId AND
							SIH.intDeleteStatus = '0' AND
							SIH.intCurrencyId = '$currency' AND
							SIH.dtmDate<='$toDay' AND
							DATEDIFF('$toDay',SIH.dtmDate) >= ($from+ PT.strName)".$wareTo.$wareCom); 
    
    $row = mysqli_fetch_array($result);
    return $row['balAmount'];    
}
function getBankDeposit($custId,$from,$to,$toDay,$currency,$companyId){
    if($to==x)$wareTo="";
    else $wareTo=" AND DATEDIFF('$toDay',BND.dtDate)<=$to ";
    
    if($companyId==0)$wareCom="";
    else $wareCom=" AND BND.intCompanyId=$companyId ";
    global $db;
    
     $result = $db->RunQuery("SELECT
                                
				sum(fin_bankdeposit_details.dblAmmount 
				+
				IFNULL ((SELECT
				SUM(fin_other_receivable_payments_main_details.dblPayAmount )AS paidAmount
				FROM
				fin_other_receivable_payments_main_details
				Inner Join fin_other_receivable_payments_header ON fin_other_receivable_payments_main_details.strReferenceNo = fin_other_receivable_payments_header.strReferenceNo
				Inner Join fin_bankundeposit_header ON fin_other_receivable_payments_main_details.strJobNo = fin_bankundeposit_header.strUnDepositNo
				WHERE
				fin_other_receivable_payments_header.intDeleteStatus =  '0' AND fin_other_receivable_payments_main_details.strDocType = 'B.Deposit' AND
				fin_other_receivable_payments_header.dtmDate<='$toDay'
				AND
				fin_other_receivable_payments_main_details.strJobNo =  BND.strDepositNo),0)				
				) AS balAmount
				FROM
				fin_bankdeposit_header BND
				Inner Join fin_bankdeposit_details ON fin_bankdeposit_details.strDepositNo = BND.strDepositNo			
				Inner Join mst_financechartofaccounts ON fin_bankdeposit_details.intAccount = mst_financechartofaccounts.intId
				WHERE
				fin_bankdeposit_details.intRecvFrom =  '$custId' AND
				(mst_financechartofaccounts.intFinancialTypeId =  '22' OR mst_financechartofaccounts.intFinancialTypeId =  '21' OR mst_financechartofaccounts.intFinancialTypeId =  '11' OR mst_financechartofaccounts.intFinancialTypeId =  '12' OR mst_financechartofaccounts.intFinancialTypeId =  '9') AND
				BND.intStatus = '1' AND
                                BND.intCurrency= '$currency' AND
                                BND.dtDate<='$toDay' AND
                                DATEDIFF('$toDay',BND.dtDate)>= $from".$wareTo.$wareCom);
    $row = mysqli_fetch_array($result);
    return $row['balAmount']*(-1);    
}

function getBankPayments($custId,$from,$to,$toDay,$currency,$companyId){
    if($to==x)$wareTo="";
    else $wareTo=" AND DATEDIFF('$toDay',BNP.dtDate)<=$to ";//
    
    if($companyId==0)$wareCom="";
    else $wareCom=" AND BNP.intCompanyId=$companyId ";
    global $db;
    
    $result = $db->RunQuery("SELECT			
				
				SUM(fin_bankpayment_details.dblAmmount
				-
				IFNULL ((SELECT 
						Sum(fin_other_receivable_payments_main_details.dblPayAmount )AS paidAmount
						FROM
						fin_other_receivable_payments_main_details
						Inner Join fin_other_receivable_payments_header ON fin_other_receivable_payments_main_details.strReferenceNo = fin_other_receivable_payments_header.strReferenceNo
						Inner Join fin_bankpayment_header ON fin_other_receivable_payments_main_details.strJobNo = fin_bankpayment_header.strBankPaymentNo
						WHERE 
						fin_other_receivable_payments_header.intDeleteStatus = '0' 
						AND fin_other_receivable_payments_main_details.strDocType = 'B.Payment'  AND
						fin_other_receivable_payments_header.dtmDate<= '$toDay' AND
						fin_other_receivable_payments_main_details.strJobNo =  BNP.strBankPaymentNo),0)									
				) AS balAmount
				FROM
				fin_bankpayment_header BNP
				Inner Join fin_bankpayment_details ON fin_bankpayment_details.strBankPaymentNo = BNP.strBankPaymentNo				
				Inner Join mst_financechartofaccounts ON fin_bankpayment_details.intAccountId = mst_financechartofaccounts.intId
				WHERE
				fin_bankpayment_details.intPayTo =  '$custId' AND
				(mst_financechartofaccounts.intFinancialTypeId =  '22' OR mst_financechartofaccounts.intFinancialTypeId =  '21' OR mst_financechartofaccounts.intFinancialTypeId =  '11' OR mst_financechartofaccounts.intFinancialTypeId =  '12' OR mst_financechartofaccounts.intFinancialTypeId =  '9') AND
				BNP.intStatus = '1' AND
                                BNP.intCurrency= '$currency' AND
                                BNP.dtDate <='$toDay' AND
                                DATEDIFF('$toDay',BNP.dtDate)>= $from".$wareTo.$wareCom);
    $row = mysqli_fetch_array($result);
    return $row['balAmount'];   
}
function getPettyCash($custId,$from,$to,$toDay,$currency,$companyId){
    if($to==x)$wareTo="";
    else $wareTo=" AND DATEDIFF('$toDay',PTC.dtDate)<=$to ";//
    
    if($companyId==0)$wareCom="";
    else $wareCom=" AND PTC.intCompanyId=$companyId ";
    global $db;
    
    $result = $db->RunQuery("SELECT
				sum(fin_bankpettycash_details.dblAmmount 
				-
				IFNULL ((SELECT
				SUM(fin_other_receivable_payments_main_details.dblPayAmount )AS paidAmount
				FROM
				fin_other_receivable_payments_main_details
				Inner Join fin_other_receivable_payments_header ON fin_other_receivable_payments_main_details.strReferenceNo = fin_other_receivable_payments_header.strReferenceNo
				Inner Join fin_bankpettycash_header ON fin_other_receivable_payments_main_details.strJobNo = fin_bankpettycash_header.strPettyCashNo
				WHERE
				fin_other_receivable_payments_header.intDeleteStatus =  '0' AND fin_other_receivable_payments_main_details.strDocType = 'Petty Cash' AND
                fin_other_receivable_payments_header.dtmDate<='$toDay'
				AND
				fin_other_receivable_payments_main_details.strJobNo =  PTC.strPettyCashNo),0)
				
				) AS balAmount
                            FROM
				fin_bankpettycash_header AS PTC
				Inner Join fin_bankpettycash_details ON fin_bankpettycash_details.strPettyCashNo = PTC.strPettyCashNo
				Inner Join mst_financechartofaccounts ON fin_bankpettycash_details.intAccountId = mst_financechartofaccounts.intId
                            WHERE
				(mst_financechartofaccounts.intFinancialTypeId =  '22' OR mst_financechartofaccounts.intFinancialTypeId =  '21' OR mst_financechartofaccounts.intFinancialTypeId =  '11' OR mst_financechartofaccounts.intFinancialTypeId =  '12' OR mst_financechartofaccounts.intFinancialTypeId =  '9')
				AND
				fin_bankpettycash_details.intPayTo =  '$custId' AND
				PTC.intStatus = '1' AND
                                PTC.intCurrency= '$currency' AND
                                PTC.dtDate <='$toDay' AND
                                DATEDIFF('$toDay',PTC.dtDate)>= $from".$wareTo.$wareCom);
    $row = mysqli_fetch_array($result);
    return $row['balAmount'];   
}

function getJurnelEntry($custId,$from,$to,$toDay,$currency,$companyId){
    if($to==x)$wareTo="";
    else $wareTo=" AND DATEDIFF('$toDay', JH.dtmDate)<=$to ";//
    
    if($companyId==0)$wareCom="";
    else $wareCom=" AND JH.intCompanyId=$companyId ";
    global $db;
    
    $result = $db->RunQuery("SELECT 
                            
                           SUM( (JD.dblDebitAmount) - (JD.dbCreditAmount)  -
                           IFNULL((SELECT DISTINCT
							Sum(fin_other_receivable_payments_main_details.dblPayAmount )AS paidAmount
							FROM
							fin_other_receivable_payments_main_details
							Inner Join fin_other_receivable_payments_header ON fin_other_receivable_payments_main_details.strReferenceNo = fin_other_receivable_payments_header.strReferenceNo
							Inner Join fin_accountant_journal_entry_details ON fin_other_receivable_payments_main_details.strDocNo = fin_accountant_journal_entry_details.strReferenceNo
							WHERE
							fin_other_receivable_payments_main_details.strJobNo =  JH.strReferenceNo AND
							fin_other_receivable_payments_header.intDeleteStatus =  '0' AND
							fin_other_receivable_payments_main_details.strDocType =  'JN' AND
							fin_other_receivable_payments_header.intCustomerId =  '$custId' AND
							fin_other_receivable_payments_header.dtmDate <=  '$toDay' AND
							fin_accountant_journal_entry_details.strPersonType =  'ocus'
							GROUP BY
							fin_other_receivable_payments_main_details.strJobNo,intItemSerial),0)
                            ) AS balAmount
                            FROM
                            fin_accountant_journal_entry_header AS JH
                            INNER JOIN fin_accountant_journal_entry_details AS JD ON JH.strReferenceNo = JD.strReferenceNo                           
                            INNER JOIN mst_financechartofaccounts ON JD.intChartOfAccountId = mst_financechartofaccounts.intId
                            WHERE
                            JD.strPersonType = 'ocus' AND
                            JD.intNameId = $custId AND
                            JH.intDeleteStatus = 0 AND
                            (mst_financechartofaccounts.intFinancialTypeId =  '22' OR mst_financechartofaccounts.intFinancialTypeId =  '21' OR mst_financechartofaccounts.intFinancialTypeId =  '11' OR mst_financechartofaccounts.intFinancialTypeId =  '12' OR mst_financechartofaccounts.intFinancialTypeId =  '9') AND
                            JH.intCurrencyId= '$currency' AND
                            JH.dtmDate <='$toDay' AND
                            DATEDIFF('$toDay', JH.dtmDate)>= $from".$wareTo.$wareCom);
    $row = mysqli_fetch_array($result);
    return $row['balAmount'];   
}

function getBillWiseTaxesCurrent($custId,$toDay,$currency,$companyId){
	if($companyId==0)$wareCom=" ";
    else $wareCom=" AND fin_supplier_purchaseinvoice_header.intCompanyId=$companyId ";
    global $db;
	
	$sql="SELECT
				SUM(IFNULL(dblBillTaxAmount,0) 
				-
				IFNULL ((SELECT
				SUM(fin_other_receivable_payments_main_details.dblPayAmount )AS paidAmount
				FROM fin_other_receivable_payments_main_details
				Inner Join fin_other_receivable_payments_header ON fin_other_receivable_payments_main_details.strReferenceNo = fin_other_receivable_payments_header.strReferenceNo
				WHERE
				fin_other_receivable_payments_main_details.strJobNo =  fin_supplier_purchaseinvoice_header.strReferenceNo AND
				fin_other_receivable_payments_header.intDeleteStatus =  '0' AND fin_other_receivable_payments_main_details.strDocType = 'B.Tax'
				AND
				fin_other_receivable_payments_header.dtmDate<='$toDay'),0)
				) AS balAmount
				FROM
				fin_supplier_purchaseinvoice_header
				Inner Join mst_supplier ON mst_supplier.intId = fin_supplier_purchaseinvoice_header.intSupplierId
				INNER JOIN mst_financepaymentsterms AS PT ON fin_supplier_purchaseinvoice_header.intPaymentsTermsId = PT.intId
				WHERE
				fin_supplier_purchaseinvoice_header.intTaxAuthorityId =  '$custId' AND
				fin_supplier_purchaseinvoice_header.intDeleteStatus = '0' AND
				fin_supplier_purchaseinvoice_header.intCurrencyId = '$currency' AND
				fin_supplier_purchaseinvoice_header.dtmDate <='$toDay' AND
				DATEDIFF('$toDay',fin_supplier_purchaseinvoice_header.dtmDate) < (PT.strName) ".$wareCom; 
	$result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    return $row['balAmount'];    
}

function getBillWiseTaxes($custId,$from,$to,$toDay,$currency,$companyId){
    if($to==x)$wareTo=" ";
    else $wareTo=" AND DATEDIFF('$toDay',INV.dtmDate)<=($to+ PT.strName) ";
    
    if($companyId==0)$wareCom="";
    else $wareCom=" AND INV.intCompanyId=$companyId ";
    global $db;
    
    $result = $db->RunQuery("SELECT
							sum(IFNULL(INV.dblBillTaxAmount,0) 
							-
							IFNULL ((SELECT
							SUM(fin_other_receivable_payments_main_details.dblPayAmount )AS paidAmount
							FROM fin_other_receivable_payments_main_details
							Inner Join fin_other_receivable_payments_header ON fin_other_receivable_payments_main_details.strReferenceNo = fin_other_receivable_payments_header.strReferenceNo
							WHERE
							fin_other_receivable_payments_main_details.strJobNo =  INV.strReferenceNo AND
							fin_other_receivable_payments_header.intDeleteStatus =  '0' AND fin_other_receivable_payments_main_details.strDocType = 'B.Tax'
							AND
							fin_other_receivable_payments_header.dtmDate<='$toDay'),0)
							) AS balAmount
							FROM
							fin_supplier_purchaseinvoice_header AS INV
							Inner Join mst_supplier ON mst_supplier.intId = INV.intSupplierId
							INNER JOIN mst_financepaymentsterms AS PT ON INV.intPaymentsTermsId = PT.intId
							WHERE
							INV.intTaxAuthorityId =  '$custId' AND
							INV.intDeleteStatus = '0' AND
							INV.intCurrencyId = '$currency' AND
							INV.dtmDate<='$toDay' AND
							DATEDIFF('$toDay',INV.dtmDate) >= ($from+ PT.strName)".$wareTo.$wareCom); 
    
    $row = mysqli_fetch_array($result);
    return $row['balAmount'];    
}
?>

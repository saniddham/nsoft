<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];

$thisFilePath = $_SERVER['PHP_SELF'];
$companyId = $_SESSION['headCompanyId'];

include "{$backwardseperator}dataAccess/permisionCheck.inc";
$date = $_GET['txtDate'];
//$currency = $_GET['cboCurrency'];
//--------------------------------
$sql = "SELECT DISTINCT
		mst_financecurrency.intId,
		mst_financecurrency.strCode
		FROM mst_financecurrency ";
$result = $db->RunQuery($sql);
$a = 0;
while ($row = mysqli_fetch_array($result)) {
    $currency[$a] = $row["intId"];
    $currencyCode[$a] = $row["strCode"];
    $a++;
}
$colspan = count($currency);
//--------------------------------
?>
<script type="application/javascript" >
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Other Receivable / Service Customer - Balance Summery</title>
        <link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />

        <script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
        <script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
        <script type="application/javascript" src="customerBalance_summery-js.js"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

        <link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
        <script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

        <link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css"/>
        <link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css"/>

    </head>

    <body>
        <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
                <td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator . 'Header.php'; ?></td>
            </tr> 
        </table>

        <script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
        <script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
        <script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>
        <style>
            .normal{
                background-color: #ffffff;
            }
            .selected{
                background-color: #00CCFF;
            }
        </style>
        <form id="frmCustSummery" name="frmCustSummery"  method="get" action="customerBalance_summery.php" autocomplete="off"  >
            <div align="center">
                <div class="trans_layoutXL" >
                    <div class="trans_text" >Other Receivable / Service Customer - Balance Summery</div>
                    <table width="100%">
                        <tr>
                            <td align="center">
                                <table align="center">
                                    <tr>                                                    
                                        <td class="normalfnt" align="left">Date&nbsp;&nbsp;</td>
                                        <td>
                                            <input name="txtDate" type="text" value="<?Php echo ($date==''?date("Y-m-d"):$date) ?>" class="validate[required] txtbox" id="txtDate" style="width:98px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" />
                                        </td>                                                    
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <div id="divTable" style="overflow:scroll auto;height: 90px"  >
                                    <table width="40%" id="tblCompanies" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
                                        <tr class="">
                                            <td bgcolor="#FAD163" class="normalfnt"><input type="checkbox" name="chkAllComp" id="chkAllComp"  />
                                            </td>
                                            <td height="24" bgcolor="#FAD163" class="normalfntMid"><strong>Company</strong></td>
                                        </tr>
                                                        
                                        <?php
                                        if ($date == '') {
                                            $date = date("Y-m-d");
                                        }

                                        $sqlC = "SELECT
                                                                mst_companies.intId,
                                                                mst_companies.strName
                                                                FROM mst_companies
                                                                WHERE
                                                                mst_companies.intStatus =  '1'";
                                        $resultC = $db->RunQuery($sqlC);
                                        $k = 0;
                                        while ($rowC = mysqli_fetch_array($resultC)) {
                                            $compId = $rowC['intId'];
                                            $compName = $rowC['strName'];
                                        ?>
                                                            
                                        <tr class="normalfnt">
                                                <td bgcolor="#FFFFFF" class="normalfnt"><input name="chkCompt<?php echo $compId ?>" type="checkbox" value="<?php echo $compId; ?>" <?php if ($_GET["chkCompt$compId"] == $compId) { ?> checked="checked" <?php } ?> /></td>
                                                <td bgcolor="#FFFFFF" class="normalfnt" id="<?php echo $compId; ?>"><?php echo $compName ?></td>
                                            </tr>        
                                            <?php
                                            if ($_GET["chkCompt$compId"] == $compId) {
                                                $k++;
                                                $comp = $_GET["chkCompt$compId"];
                                                $companies[$k - 1] = $comp;
                                            }
                                        }
                                        ?>
                                        <tr class="normalfnt" bgcolor="#CCCCCC">
                                            <td class="normalfntMid"></td>
                                            <td class="normalfntMid" >Total</td>
                                        </tr>      
                                    </table>  
                                </div>   
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <img src="../../../../images/search.png" width="25" height="25" alt="search" id="butSerach"  />
                            </td>
                        </tr>
                        
                        
                        
                        <tr>
                            <td width="100%">
                                <table width="100%" align="center">
                                   <tr>
                                        <td width="1" class="normalfnt">&nbsp;</td>
                                        <td colspan="2" rowspan="5" class="normalfnt">
                                            <div id="divTable1" style="overflow:scroll;width:100%;height:300px"  >
                                                <table width="97%" id="tblCustomersAccount" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
                                                    <tr class="">
                                                        <td width="44" bgcolor="#FAD163" class="normalfntMid"><input type="checkbox" name="chkAll" id="chkAll" />
                                                        </td>
                                                        <td width="150"  height="24" bgcolor="#FAD163" class="normalfntMid"><strong>Service Customer</strong></td>
                                                        <?php
                                                        for ($j = 0; $j < count($companies); $j++) {
                                                            $cmp = $companies[$j];
                                                            $sqlC = "SELECT
									mst_companies.intId,
									mst_companies.strName
									FROM mst_companies
									WHERE 
									mst_companies.intId='$cmp' and 
									mst_companies.intStatus =  '1'";

                                                            $resultC = $db->RunQuery($sqlC);
                                                            $k = 0;
                                                            $rowC = mysqli_fetch_array($resultC);
                                                            $compName = $rowC['strName'];
                                                            ?>
                                                            <td width="100" class="normalfntMid" bgcolor="#FAD163"  colspan="<?php echo $colspan ?>"><strong><?PHP echo $compName; ?></strong></td>
                                                            <?php
                                                        }
                                                        ?>
                                                        <td width="100"  height="24" bgcolor="#FAD163"  class="normalfntMid" colspan="<?php echo $colspan ?>"><strong>Total</strong></td>
                                                    </tr>
                                                    <tr class="normal"  >
                                                        <td width="44" class="normalfntMid" >
                                                        </td>
                                                        <td width="150"  height="24" class="normalfntMid"></td>
                                                        <?php
                                                        for ($j = 0; $j < count($companies); $j++) {
                                                            for ($k = 0; $k < count($currency); $k++) {
                                                                ?>
                                                                <td width="100"  class="normalfntMid" bgcolor="#FAD163"><?php echo $currencyCode[$k]; ?></td>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                        <?php
                                                        for ($k = 0; $k < count($currency); $k++) {
                                                            ?>
                                                            <td width="100"  height="24" bgcolor="#FAD163" class="normalfntMid" ><?php echo $currencyCode[$k] ?></td>
                                                            <?php
                                                        }
                                                        ?>

                                                    </tr>


                                                    <?php
                                                    if ($date == '') {
                                                        $date = date("Y-m-d");
                                                    }
                                                    $date = $date . ' 23:59:59';                                                   

                                                    //-------------------------
                                                    $sql = "SELECT
                                                                mst_finance_service_customer.intId,
                                                                mst_finance_service_customer.strName 
                                                                FROM mst_finance_service_customer";
                                                    $result = $db->RunQuery($sql);
                                                    while ($row = mysqli_fetch_array($result)) {
                                                        $customerId = $row['intId'];
                                                        $customer = $row['strName'];
                                                        ?>
                                                        <tr class="normal" onMouseOver="select(this)" onMouseOut="normal(this)">
                                                            <td  class="normalfntMid"><input name="chkSelect" type="checkbox" value="<?php echo $customerId; ?>" /></td>
                                                            <td  class="normalfntMid" id="<?php echo $customerId; ?>"><?php echo $customer ?></td>

                                                            <?php
                                                            for ($j = 0; $j < count($companies); $j++) {
                                                                for ($k = 0; $k < count($currency); $k++) {

                                                                    $currencyId = $currency[$k];                                                                    
                                                                    $custBal=getCustomerBalance($customerId,$currencyId, $date,$companies[$j]);
                                                                    $custCompanyTotal[$customerId][$k]+=$custBal;
                                                                    $curremcySum[$k]+=$custBal;
                                                                    $companyWiaeCurrency[ $j][$k]+=$custBal;
                                                                    
                                                                    ?>
                                                            <td align="right" class="normalfntRight"><?php echo number_format($custBal, 2) ?>&nbsp;&nbsp;</td>
                                                                    <?php
                                                                }
                                                            }
                                                            for ($k = 0; $k < count($currency); $k++) {
                                                                ?>
                                                                <td  class="normalfntRight" ><strong><?php echo number_format($custCompanyTotal[$customerId][$k], 2) ?></strong></td>
                                                            <?php }
                                                            ?>
                                                        </tr>        
                                                        <?php
                                                    }
                                                    ?>

                                                    <tr>
                                                        <td></td>
                                                        <td class="normalfnt"><strong>Total</strong></td>
                                                        <?php
                                                        for ($j = 0; $j < count($companies); $j++) {
                                                            for ($k = 0; $k < count($currency); $k++) {
                                                                ?>
                                                                <td  class="normalfntRight" ><strong><?php echo number_format(round($companyWiaeCurrency[$j][$k], 2)) ?></strong></td>
                                                                <?php
                                                            }
                                                        }
                                                        for ($k = 0; $k < count($currency); $k++) {
                                                            ?>
                                                            <td  class="normalfntRight" ><strong><?php echo number_format($curremcySum[$k], 2) ?></strong></td>
                                                        <?php }
                                                        ?>
                                                    </tr> 

                                                </table>  
                                            </div> 
                                        </td>
                                        <td width="26" bgcolor="#FFFFFF" class="">&nbsp;</td>
                                        <td width="26" bgcolor="#FFFFFF" class="">&nbsp;</td>
                                    </tr>

                                    <tr>
                                        <td height="21" class="normalfnt">&nbsp;</td>
                                        <td bgcolor="#FFFFFF" class="">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="21" class="normalfnt">&nbsp;</td>
                                        <td bgcolor="#FFFFFF" class="">&nbsp;</td>
                                    </tr>
                                    <tr style="display:none">
                                        <td height="21" class="normalfnt">&nbsp;</td>
                                        <td bgcolor="#FFFFFF" class="">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="21" class="normalfnt">&nbsp;</td>
                                        <td bgcolor="#FFFFFF" class="">&nbsp;</td>
                                    </tr>
                                </table></td>
                        </tr>
                        <tr>
                            <td align="center">
                                <table width="100%" class="tableBorder_allRound">
                                    <tr>
                                        <td width="100%" align="center" bgcolor=""><img style="display:none" border="0" src="../../../../images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img border="0" src="../../../../images/Treport.jpg" alt="Summery Report" name="butReport"width="92" height="24"  class="mouseover" id="butReport" tabindex="24"/><a href="../../../../main.php"><img  src="../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">&nbsp;</td>
                        </tr>
                    </table>
                </div>
            </div>
            
        </form>
    </body>
</html>
<?php
function getCustomerBalance($customerId,$currencyId, $date,$companyId){
    global $db;
    $sql="  SELECT 
                SUM(tbl.amount) as totAmount 
            FROM(
                (SELECT
                    ((SUM(((CRN.dblQty*CRN.dblRate) *(100-CRN.dblDiscount)/100)+ IFNULL(CRN.dblTax,0))
                    +
                    IFNULL ((SELECT Sum(fin_other_receivable_payments_main_details.dblPayAmount )AS paidAmount
                            FROM fin_other_receivable_payments_main_details
                            Inner Join fin_other_receivable_payments_header ON fin_other_receivable_payments_main_details.strReferenceNo = fin_other_receivable_payments_header.strReferenceNo
                            WHERE
                                fin_other_receivable_payments_main_details.strJobNo =  CRN.strCreditNoteNo AND
                                fin_other_receivable_payments_header.intDeleteStatus =  '0' AND fin_other_receivable_payments_main_details.strDocType = 'C.Note' AND
                                fin_other_receivable_payments_header.dtmDate<='$date'
                            GROUP BY
                            fin_other_receivable_payments_main_details.strJobNo),0)
                            ) *-1)AS amount
                    FROM
                        fin_other_receivable_creditnote_details AS CRN
                        INNER JOIN fin_other_receivable_creditnote_header AS CH ON CRN.strCreditNoteNo = CH.strCreditNoteNo
                    WHERE
                        CH.intCustomer = $customerId AND
                        CH.intStatus = '1' AND
                        CH.dtDate <='$date' AND
                        CH.intCurrency=$currencyId AND CH.intCompanyId=$companyId
                )
            UNION
                (SELECT
                    SUM((ADV.dblReceivedAmount 
                        +
                        IFNULL ((SELECT
                        Sum(fin_other_receivable_payments_main_details.dblPayAmount )AS paidAmount
                        FROM fin_other_receivable_payments_main_details
                        Inner Join fin_other_receivable_payments_header ON fin_other_receivable_payments_main_details.strReferenceNo = fin_other_receivable_payments_header.strReferenceNo
                        WHERE
                        fin_other_receivable_payments_main_details.strJobNo =  ADV.strReceiptNo AND
                        fin_other_receivable_payments_header.intDeleteStatus =  '0' AND fin_other_receivable_payments_main_details.strDocType = 'A.Received' AND
                        fin_other_receivable_payments_header.dtmDate<='$date'
                        GROUP BY
                        fin_other_receivable_payments_main_details.strJobNo),0)
                        )*-1) AS amount
                    FROM
                        fin_other_receivable_advancereceived_header AS ADV
                    WHERE
                        ADV.intCustomer =  $customerId AND
                        ADV.intStatus = '1' AND
                        ADV.intCurrency=$currencyId AND 
                        ADV.dtDate<='$date' AND
                        ADV.intCompanyId=$companyId                
                )
            UNION
                (SELECT
                    (sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) 
                    -
                    IFNULL ((SELECT
                    Sum(fin_other_receivable_payments_main_details.dblPayAmount )AS paidAmount
                    FROM fin_other_receivable_payments_main_details
                    Inner Join fin_other_receivable_payments_header ON fin_other_receivable_payments_main_details.strReferenceNo = fin_other_receivable_payments_header.strReferenceNo
                    WHERE
                    fin_other_receivable_payments_main_details.strJobNo =  INV.strReferenceNo AND
                    fin_other_receivable_payments_header.intDeleteStatus =  '0' AND fin_other_receivable_payments_main_details.strDocType = 'S.Invoice' AND
                    fin_other_receivable_payments_header.dtmDate<='$date'
                    GROUP BY
                    fin_other_receivable_payments_main_details.strJobNo),0)

                    ) AS amount
                    FROM
                    fin_other_receivable_invoice_details AS INV
                    Inner Join fin_other_receivable_invoice_header ON fin_other_receivable_invoice_header.strReferenceNo = INV.strReferenceNo AND INV.intInvoiceNo = fin_other_receivable_invoice_header.intInvoiceNo AND INV.intAccPeriodId = fin_other_receivable_invoice_header.intAccPeriodId AND INV.intLocationId = fin_other_receivable_invoice_header.intLocationId AND INV.intCompanyId = fin_other_receivable_invoice_header.intCompanyId
                    Inner Join mst_finance_service_customer ON mst_finance_service_customer.intId = fin_other_receivable_invoice_header.intCustomerId
                    INNER JOIN mst_financepaymentsterms AS PT ON fin_other_receivable_invoice_header.intPaymentsTermsId = PT.intId
                    WHERE
                    fin_other_receivable_invoice_header.intCustomerId = $customerId AND
                    fin_other_receivable_invoice_header.intDeleteStatus = '0' AND
                    fin_other_receivable_invoice_header.intCurrencyId = $currencyId AND
                    fin_other_receivable_invoice_header.dtmDate <='$date' AND                    
                    fin_other_receivable_invoice_header.intCompanyId=$companyId
                    GROUP BY INV.strReferenceNo
                )
            UNION
                (SELECT
                    (
                    sum(fin_bankdeposit_details.dblAmmount )
                    +
                    IFNULL ((SELECT
                    Sum(fin_other_receivable_payments_main_details.dblPayAmount )AS paidAmount
                    FROM fin_other_receivable_payments_main_details
                    Inner Join fin_other_receivable_payments_header ON fin_other_receivable_payments_main_details.strReferenceNo = fin_other_receivable_payments_header.strReferenceNo
                    WHERE
                    fin_other_receivable_payments_main_details.strJobNo =  BND.strDepositNo AND
                    fin_other_receivable_payments_header.intDeleteStatus =  '0' AND fin_other_receivable_payments_main_details.strDocType = 'B.Deposit' AND
                        fin_other_receivable_payments_header.dtmDate<='$date'
                    GROUP BY
                    fin_other_receivable_payments_main_details.strJobNo),0)

                    ) * (-1)AS amount
                    FROM
                    fin_bankdeposit_header BND
                    Inner Join fin_bankdeposit_details ON fin_bankdeposit_details.strDepositNo = BND.strDepositNo			
                    Inner Join mst_financechartofaccounts ON fin_bankdeposit_details.intAccount = mst_financechartofaccounts.intId
                    WHERE
                    fin_bankdeposit_details.intRecvFrom =  '$customerId' AND
                    (mst_financechartofaccounts.intFinancialTypeId =  '22' OR mst_financechartofaccounts.intFinancialTypeId =  '21' OR mst_financechartofaccounts.intFinancialTypeId =  '11' OR mst_financechartofaccounts.intFinancialTypeId =  '12' OR mst_financechartofaccounts.intFinancialTypeId =  '9') AND
                    BND.intStatus = '1' AND
                    BND.intCurrency=$currencyId AND
                    BND.dtDate<='$date' AND
                    BND.intCompanyId=$companyId    
                )
            UNION
                (SELECT			
                    (
                    SUM(BNP.dblReceivedAmount )
                    -
                    IFNULL ((SELECT
                    Sum(fin_other_receivable_payments_main_details.dblPayAmount )AS paidAmount
                    FROM fin_other_receivable_payments_main_details
                    Inner Join fin_other_receivable_payments_header ON fin_other_receivable_payments_main_details.strReferenceNo = fin_other_receivable_payments_header.strReferenceNo
                    WHERE
                    fin_other_receivable_payments_main_details.strJobNo =  BNP.strBankPaymentNo AND
                    fin_other_receivable_payments_header.intDeleteStatus =  '0' AND fin_other_receivable_payments_main_details.strDocType = 'B.Payment' AND
                    fin_other_receivable_payments_header.dtmDate<='$date'
                    GROUP BY
                    fin_other_receivable_payments_main_details.strJobNo),0)

                    ) AS amount
                    FROM
                    fin_bankpayment_header BNP
                    Inner Join fin_bankpayment_details ON fin_bankpayment_details.strBankPaymentNo = BNP.strBankPaymentNo				
                    Inner Join mst_financechartofaccounts ON fin_bankpayment_details.intAccountId = mst_financechartofaccounts.intId
                    WHERE
                    fin_bankpayment_details.intPayTo = '$customerId' AND
                    (mst_financechartofaccounts.intFinancialTypeId =  '22' OR mst_financechartofaccounts.intFinancialTypeId =  '21' OR mst_financechartofaccounts.intFinancialTypeId =  '11' OR mst_financechartofaccounts.intFinancialTypeId =  '12' OR mst_financechartofaccounts.intFinancialTypeId =  '9') AND
                    BNP.intStatus = '1' AND
                    BNP.intCurrency=$currencyId AND
                    BNP.dtDate <='$date' AND
                    BNP.intCompanyId=$companyId
                )
            UNION
                (SELECT(
                    sum(PTC.dblReceivedAmount )
                    -
                    IFNULL ((SELECT
                    Sum(fin_other_receivable_payments_main_details.dblPayAmount )AS paidAmount
                    FROM fin_other_receivable_payments_main_details
                    Inner Join fin_other_receivable_payments_header ON fin_other_receivable_payments_main_details.strReferenceNo = fin_other_receivable_payments_header.strReferenceNo
                    WHERE
                    fin_other_receivable_payments_main_details.strJobNo =  PTC.strPettyCashNo AND
                    fin_other_receivable_payments_header.intDeleteStatus =  '0' AND fin_other_receivable_payments_main_details.strDocType = 'Petty Cash' AND
                        fin_other_receivable_payments_header.dtmDate<='$date'
                    GROUP BY
                    fin_other_receivable_payments_main_details.strJobNo),0)

                    ) AS amount
                FROM
                    fin_bankpettycash_header AS PTC
                    Inner Join fin_bankpettycash_details ON fin_bankpettycash_details.strPettyCashNo = PTC.strPettyCashNo
                    Inner Join mst_financechartofaccounts ON fin_bankpettycash_details.intAccountId = mst_financechartofaccounts.intId
                WHERE
                    (mst_financechartofaccounts.intFinancialTypeId =  '22' OR mst_financechartofaccounts.intFinancialTypeId =  '21' OR mst_financechartofaccounts.intFinancialTypeId =  '11' OR mst_financechartofaccounts.intFinancialTypeId =  '12' OR mst_financechartofaccounts.intFinancialTypeId =  '9')
                    AND
                    fin_bankpettycash_details.intPayTo =  '$customerId' AND
                    PTC.intStatus = '1' AND
                    PTC.intCurrency=$currencyId AND
                    PTC.dtDate <='$date' AND
                    PTC.intCompanyId=$companyId
                )
            UNION
                (SELECT 
                (
                SUM( (JD.dblDebitAmount) - (JD.dbCreditAmount) ) -
                IFNULL((SELECT
                            Sum(fin_other_receivable_payments_main_details.dblPayAmount )AS paidAmount
                        FROM fin_other_receivable_payments_main_details
                            Inner Join fin_other_receivable_payments_header ON fin_other_receivable_payments_main_details.strReferenceNo = fin_other_receivable_payments_header.strReferenceNo
                        WHERE
                            fin_other_receivable_payments_main_details.strJobNo =  JH.strReferenceNo AND
                            fin_other_receivable_payments_header.intDeleteStatus =  '0' AND fin_other_receivable_payments_main_details.strDocType = 'JN' AND
                        fin_other_receivable_payments_header.dtmDate<='$date'
                        GROUP BY
                            fin_other_receivable_payments_main_details.strJobNo),0)
                ) AS amount
                FROM
                fin_accountant_journal_entry_header AS JH
                INNER JOIN fin_accountant_journal_entry_details AS JD ON JH.intEntryNo = JD.intEntryNo                           
                INNER JOIN mst_financechartofaccounts ON JD.intChartOfAccountId = mst_financechartofaccounts.intId
                WHERE
                JD.strPersonType = 'ocus' AND
                JD.intNameId = $customerId AND
                JH.intDeleteStatus = 0 AND
                (mst_financechartofaccounts.intFinancialTypeId =  '22' OR mst_financechartofaccounts.intFinancialTypeId =  '21' OR mst_financechartofaccounts.intFinancialTypeId =  '11' OR mst_financechartofaccounts.intFinancialTypeId =  '12' OR mst_financechartofaccounts.intFinancialTypeId =  '9') AND
                JH.intCurrencyId=$currencyId AND
                JH.dtmDate <='$date' AND
                JH.intCompanyId=$companyId
                )
            ) as tbl";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    return $row['totAmount'];
}
?>
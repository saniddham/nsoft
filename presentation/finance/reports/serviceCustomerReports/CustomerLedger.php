<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];

$thisFilePath = $_SERVER['PHP_SELF'];

include "{$backwardseperator}dataAccess/permisionCheck.inc";
$companyId = $_SESSION['headCompanyId'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Other Receivable / Service Customer - Ledger</title>
        <link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
         <link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
        <script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
        <link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css" />
        <link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css" />
        <script type="application/javascript">
            function showReport(){
                var data	 = "custId="+document.getElementById("cboCust").value;
                	data	+= "&from="+document.getElementById("txtFrom").value;
                	data	+= "&to="+document.getElementById("txtTo").value;
					data	+= "&CurrencyId="+$('#cboCurrency').val();
					data	+= "&CurrencyName="+$('#cboCurrency option:selected').text();         
                window.open("CustomerLedgerRpt.php?"+data,'CustomerLedgerRpt.php');
            }
        </script>
    </head>
    <body>
        <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
                <td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator . 'Header.php'; ?></td>
            </tr> 
        </table>
        <form id="frmAgingRpts" name="frmAgingRpts"  method="get" action="customerBalance_summery.php" autocomplete="off"  >
            <div align="center">
                <div class="trans_layoutD">
                    <div class="trans_text">Other Receivable / Service Customer - Ledger</div>
                    <table>
                        <tr>
                            <td class="normalfnt" >Customer</td>
                            <td class="normalfnt" ><select name="cboCust" id="cboCust" style="width:211px" class="validate[required]">
                                                    <option value=""></option>
                                                    <?php
                                                            $sql = "SELECT  intId,strName FROM mst_finance_service_customer WHERE intStatus = 1 order by strName";
                                                            $result = $db->RunQuery($sql);
                                                            while ($row = mysqli_fetch_array($result)) {                                                                
                                                                echo "<option value=\"" . $row['intId'] . "\">" . $row['strName'] . "</option>";
                                                            }
                                                            ?>
                                                        </select></td>
                        </tr>
                        <tr>
                          <td class="normalfnt" >Currency</td>
                          <td class="normalfnt" ><select name="cboCurrency" id="cboCurrency" style="width:211px" class="validate[required]">
                            <option value="">Base Currency [LKR]</option>
                            <?php
                                                            $sql = "SELECT
						intId,
						strCode
						FROM mst_financecurrency
						WHERE
							intStatus = 1
						order by strCode
						";
                                                            $result = $db->RunQuery($sql);
                                                            while ($row = mysqli_fetch_array($result)) {
                                                                if ($row['intId'] == $currency)
                                                                    echo "<option value=\"" . $row['intId'] . "\" selected=\"selected\">" . $row['strCode'] . "</option>";
                                                                else
                                                                    echo "<option value=\"" . $row['intId'] . "\">" . $row['strCode'] . "</option>";
                                                            }
                                                            ?>
                          </select></td>
                        </tr>
                        <tr>
                            <td class="normalfnt" >From</td>
                            <td class="normalfnt" ><input name="txtFrom" type="text" value="<?Php echo ($date==''?date("Y-m-d"):$date) ?>" class="validate[required] txtbox" id="txtFrom" style="width:98px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                        </tr>
                        <tr>
                            <td class="normalfnt" >To</td>
                            <td class="normalfnt" ><input name="txtTo" type="text" value="<?Php echo ($date==''?date("Y-m-d"):$date) ?>" class="validate[required] txtbox" id="txtTo" style="width:98px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center" bgcolor=""><img onclick="showReport()" border="0" src="../../../../images/Treport.jpg" alt="Summery Report" name="butReport"width="92" height="24"  class="mouseover" id="butReport" tabindex="24"/><a href="../../../../main.php"><img  src="../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                        </tr>
                    </table>
                </div>
                </div>
                </form>
    </body>
</html>
<?php
session_start();
$backwardseperator = "../../../../";
$companyId = $_SESSION['headCompanyId'];
$location = $_SESSION['CompanyID'];
$intUser = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath = $_SERVER['PHP_SELF'];
include "{$backwardseperator}dataAccess/Connector.php";
include "../accountReports/AccReportFunctions.php";
$locationId = $location;

$custId 		= $_REQUEST['custId'];
$startDate 		= $_REQUEST['from'];
$endDate 		= $_REQUEST['to'];
$currencyId 	= $_REQUEST['CurrencyId'];
$currencyName 	= $_REQUEST['CurrencyName'];

$ledgerURL = "SelectedCustomerBalanceDetails.php?toDay=$endDate&company=$companyId&currency=$currencyId&details[0]=$custId ";

$sql = "SELECT mst_finance_service_customer.strName,mst_finance_service_customer.strAddress FROM mst_finance_service_customer WHERE mst_finance_service_customer.intId =  '$custId'";
$result = $db->RunQuery($sql);
$row 	= mysqli_fetch_array($result);
$customerName 	= $row['strName'];
$custAddress	= $row['strAddress'];

if($currencyId!="")
{
	$select_sql	= "Sum(fin_transactions_details.amount) AS crediAmount";
	$where_sql 	= "AND fin_transactions.currencyId = $currencyId ";						
}
else
{
	$select_sql	= "Sum((fin_transactions_details.amount * fin_transactions.currencyRate)) AS crediAmount";
	$where_sql 	= "";	
}
$sqlcredit="SELECT
                $select_sql
                FROM
                fin_transactions_details
                Inner Join fin_transactions ON fin_transactions_details.entryId = fin_transactions.entryId
                WHERE
                fin_transactions.entryDate <  '$startDate' AND
                fin_transactions.companyId =  '$companyId' AND
                fin_transactions.authorized =  '1' AND
                fin_transactions.delStatus =  '0' AND
                fin_transactions_details.personType =  'ocus' AND
                fin_transactions_details.personId =  '$custId' AND
                fin_transactions_details.`credit/debit` =  'C'
				$where_sql";
				
$resultCredit = $db->RunQuery($sqlcredit);
$rowC = mysqli_fetch_array($resultCredit);
$creditOpen=$rowC['crediAmount'];

if($currencyId!="")
{
	$select_sql	= "Sum(fin_transactions_details.amount) AS debitAmount";
	$where_sql 	= "AND fin_transactions.currencyId = $currencyId ";						
}
else
{
	$select_sql	= "Sum((fin_transactions_details.amount * fin_transactions.currencyRate)) AS debitAmount";
	$where_sql 	= "";	
}

$sqlDebit="SELECT
                $select_sql
                FROM
                fin_transactions_details
                Inner Join fin_transactions ON fin_transactions_details.entryId = fin_transactions.entryId
                WHERE
                fin_transactions.entryDate <  '$startDate' AND
                fin_transactions.companyId =  '$companyId' AND
                fin_transactions.authorized =  '1' AND
                fin_transactions.delStatus =  '0' AND
                fin_transactions_details.personType =  'ocus' AND
                fin_transactions_details.personId =  '$custId' AND
                fin_transactions_details.`credit/debit` =  'D'
				$where_sql";
				
$resultDebit = $db->RunQuery($sqlDebit);
$rowD = mysqli_fetch_array($resultDebit);
$debitOpen=$rowD['debitAmount'];

$openingBalance=($debitOpen-$creditOpen);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Other Receivable / Service Customer - Ledger</title>
        <link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/button.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
        <script type="application/javascript" src="../accountReports/DrillAccount.js"></script>
        <style>
            table.rptTable{
                width: 100%;
                border-collapse:collapse;
            }
            table.rptTable tr td{
                border:1px solid black;
            }
            .rptTblHeader {
                font-size: 11px;
                text-align: center;
                font-weight:bold;
                background-color: #E4E4E4;
            }
            .rptTblHeader1 {
                font-size: 11px;
                text-align: center;
                font-weight:bold;                
            }
            .rptTblBody {
                font-size: 10px;
            }
            .rptTblBodyTotal {
                font-size: 10px;
                font-weight:bold;
                background-color: #E4E4E4;
            }
            .drillLink{
                cursor:pointer;
            }
            a:hover
            { 
                font-weight: bold;
                text-decoration:underline;
                color: #0000FF;
            }            
        </style>
       
        </head>

    <body>
        <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td colspan="3"></td>
                </tr>
                <tr>
                    <td width="20%"></td>
                    <td width="60%" height="80" valign="top"><?php include '../../reportHeader.php' ?></td>
                    <td width="20%"></td>
                </tr>

                <tr>
                    <td colspan="3"></td>
                </tr>
            </table>
        <div align="center">
            <div style="background-color:#FFF" ><strong>Other Receivable / Service Customer - Ledger</strong><strong></strong></div>
            <div style="background-color:#FFF" ><strong>From</strong>&nbsp;&nbsp;&nbsp;<i><?php echo $startDate ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</i><strong>To</strong>&nbsp;&nbsp;&nbsp;<i><?php echo $endDate ?></i><strong></strong></div><br/>
            <div style="background-color:#FFF" ><strong>Service Customer:-</strong>&nbsp;&nbsp;&nbsp;<i><?php echo $customerName ?> <?php echo $custAddress ?></i><a class="button green small" href="<?php echo $ledgerURL?>" target="SelectedCustomerBalanceDetails.php">View Aging</a></div>
            <table width="90%" border="0" align="center" bgcolor="#FFFFFF">
                <tr>
                    <td><samp style="font-size: 11px;font-weight:bold;">All amounts in <?php echo $currencyName?></samp></td>
                </tr>
                
                <tr>
                    <td>
                        <table class="rptTable">  
                            <tr class="rptTblHeader1">
                                <td colspan="7" align="right">Opening Balance :</td>
                                <td align="right"><?php echo number_format($openingBalance,4) ?></td>
                            </tr>
                            <tr class="rptTblHeader">                                
                                <td>Transaction Type</td>
                                <td>Date</td>
                                <td>Number</td>
                                <td>Account</td>
                                <td>Memo</td>
                                <td>Debit</td>
                                <td>Credit</td>
                                <td>Balance</td>
                            </tr>
                            
                            <?php
							if($currencyId!="")
							{
								$select_sql	= "fin_transactions_details.amount as amount1";
								$where_sql 	= "AND fin_transactions.currencyId = $currencyId ";						
							}
							else
							{
								$select_sql	= "(fin_transactions_details.amount * fin_transactions.currencyRate) as  amount1";
								$where_sql 	= "";	
							}
							
                            $sqlDetails="  SELECT
                                                fin_transactions_details.`credit/debit`,
                                                fin_transactions_details.accountId,
                                                $select_sql,
                                                fin_transactions_details.details,
                                                fin_transactions.entryDate,
                                                fin_transactions.strProgramType,
                                                fin_transactions.documentNo,
                                                fin_transactions.entryId
                                            FROM
                                                fin_transactions_details
                                                Inner Join fin_transactions ON fin_transactions_details.entryId = fin_transactions.entryId
                                            WHERE
                                                fin_transactions_details.personType =  'ocus' AND
                                                fin_transactions_details.personId =  '$custId' AND                                                
                                                fin_transactions.companyId =  '$companyId' AND
                                                fin_transactions.authorized =  '1' AND
                                                fin_transactions.delStatus =  '0' AND
                                                fin_transactions.entryDate <='$endDate' AND
                                                fin_transactions.entryDate >='$startDate'
												$where_sql
                                            ORDER BY
                                                fin_transactions.entryDate ASC,
                                                fin_transactions.entryId ASC";
                                            $resultDetails = $db->RunQuery($sqlDetails);
                                            $closingBal=$openingBalance;
                                            $creditTot=0;
                                            $debitTot=0;
                                            while($rowDetails = mysqli_fetch_array($resultDetails)){
                                                $entryId=$rowDetails['entryId'];
                                                $debitAmount=0;
                                                $creditAmount=0;
                                                if($rowDetails['credit/debit']=='D'){
                                                    $debitAmount=$rowDetails['amount1'];
                                                    $creditAmount=0;
                                                }
                                                else{
                                                    $creditAmount=$rowDetails['amount1'];
                                                    $debitAmount=0;
                                                }    
                                                $closingBal+=($debitAmount-$creditAmount);
                                                $creditTot+=$creditAmount;
                                                $debitTot+=$debitAmount;
                            ?>
                            <tr class="rptTblBody">                                
                                <td><?php echo $rowDetails['strProgramType'] ?></td>
                                <td><?php echo $rowDetails['entryDate'] ?></td>
                                <td><a class="drillLink" onclick="leadgerDrill('<?php echo ($rowDetails['strProgramType']) ?>','<?php echo $rowDetails['documentNo']?>')">&nbsp;<?php echo $rowDetails['documentNo'] ?></a></td>
                                <td>
                                    <?php 
                                    $arOPC=  oppcitAccountsMulty($rowDetails['credit/debit'],$entryId);
                                    for($i=0; $i<count($arOPC); ++$i){
                                        echo $i+1;
                                        echo '.)';
                                        echo $arOPC[$i].'<br /><br />';
                                    }
                                    ?>
                                </td>
                                <td><?php echo $rowDetails['details'] ?>&nbsp;</td>
                                <td align="right" ><?php echo number_format($debitAmount,4) ?></td>
                                <td align="right" ><?php echo number_format($creditAmount,4) ?></td>
                                <td align="right" ><?php echo number_format($closingBal,4) ?></td>
                            </tr>
                            <?php 
                                }
                            ?>
                            <tr class="rptTblBodyTotal">
                                <td colspan="5"></td>
                                <td align="right"><?php echo number_format($debitTot, 4) ?></td>
                                <td align="right"><?php echo number_format($creditTot, 4) ?></td>
                                <td align="right"><?php echo number_format(($debitTot - $creditTot), 4) ?></td>

                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </body>
</html>
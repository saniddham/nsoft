<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];

$thisFilePath = $_SERVER['PHP_SELF'];

include "{$backwardseperator}dataAccess/permisionCheck.inc";
$companyId = $_SESSION['headCompanyId'];
$date = $_GET['txtDate'];
?>
<script type="application/javascript" >
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Other Receivable / Service Customer - Balance Details</title>
        <link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />

        <script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
        <script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
        <script type="application/javascript" src="customerBalance_details-js.js"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

        <link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
        <script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
        <link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css" />
        <link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css" />

    </head>

    <body>
        <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
                <td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator . 'Header.php'; ?></td>
            </tr> 
        </table>

        <script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
        <script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
        <script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

        <form id="frmCustDetails" name="frmCustDetails"  method="get" action="customerBalance_Details.php" autocomplete="off"  >
            <div align="center">
                <div class="trans_layoutD">
                    <div class="trans_text">Other Receivable / Service Customer - Balance Details</div>
                    <table width="100%">
                        <tr>
                            <td width="100%"><table width="100%">
                                    <tr>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td colspan="5" rowspan="5" class="normalfntRight">

                                            <div class="normalfnt" style="text-align:left; font-size:12px; background-color:#FFD8B0">Filter From</div>
                                            <div class="" style="border-style:double; border-color:#FF8000">

                                                <table width="100%" >
                                                    <tr>
                                                        <td class="normalfnt" align="left">Company&nbsp;&nbsp;</td>
                                                        <td bgcolor="#FFFFFF" class="" align="left"><select name="cboCompany" id="cboCompany" style="width:200px" class="validate[required]">
                                                                <option value="">All</option>
                                                                <?php
                                                                $sql = "SELECT
						mst_companies.intId,
						mst_companies.strName
						FROM mst_companies
						WHERE
						mst_companies.intStatus =  '1'
						";
                                                                $result = $db->RunQuery($sql);
                                                                while ($row = mysqli_fetch_array($result)) {
                                                                    if ($row['intId'] == $companyId)
                                                                        echo "<option value=\"" . $row['intId'] . "\" selected=\"selected\">" . $row['strName'] . "</option>";
                                                                    else
                                                                        echo "<option value=\"" . $row['intId'] . "\">" . $row['strName'] . "</option>";
                                                                }
                                                                ?>
                                                            </select></td>
                                                    </tr>        <tr>
                                                        <td width="20%" class="normalfnt" align="right">Date</td>
                                                        <td width="80%" align="left"><input name="txtDate" type="text" value="<?Php echo ($date==''?date("Y-m-d"):$date) ?>" class="validate[required] txtbox" id="txtDate" style="width:98px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="20%" class="normalfnt" align="right">Currency</td>
                                                        <td bgcolor="#FFFFFF" class="" align="left"><select name="cboCurrency" id="cboCurrency" style="width:100px" class="validate[required]">
                                                                <option value=""></option>
                                                                <?php
                                                                $sql = "SELECT
						intId,
						strCode
						FROM mst_financecurrency
						WHERE
							intStatus = 1
						order by strCode
						";
                                                                $result = $db->RunQuery($sql);
                                                                while ($row = mysqli_fetch_array($result)) {
                                                                    if ($row['intId'] == $currency)
                                                                        echo "<option value=\"" . $row['intId'] . "\" selected=\"selected\">" . $row['strCode'] . "</option>";
                                                                    else
                                                                        echo "<option value=\"" . $row['intId'] . "\">" . $row['strCode'] . "</option>";
                                                                }
                                                                ?>
                                                            </select></td>
                                                    </tr>        <tr>
                                                        <td colspan="2" class="normalfnt">
                                                            <div id="divTable1" style="overflow:scroll;width:100%;height:180px"  >
                                                                <table width="97%" id="tblCustomersAccount" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
                                                                    <tr class="">
                                                                        <td width="44" bgcolor="#FAD163" class="normalfntMid"><input type="checkbox" name="chkAll" id="chkAll" />
                                                                        </td>
                                                                        <td width="234"  height="24" bgcolor="#FAD163" class="normalfntMid"><strong>Customer</strong></td>
                                                                    </tr>

                                                                    <?php
                                                                    if ($date == '') {
                                                                        $date = date("Y-m-d");
                                                                    }

                                                                    $sqlm = "SELECT
                                                Sum(fin_transactions_details.amount) AS qty,
                                                mst_customer.intId,
                                                mst_customer.strName
                                                FROM
                                                fin_transactions_details
                                                Inner Join fin_transactions ON fin_transactions_details.entryId = fin_transactions.entryId
                                                Inner Join mst_customer ON fin_transactions_details.personId = mst_customer.intId
                                                WHERE
                                                fin_transactions.entryDate <=  '$date' AND
                                                fin_transactions.authorized =  '1' AND
                                                fin_transactions.delStatus =  '0' AND
                                                fin_transactions_details.personType =  'cus'
                                                GROUP BY
                                                mst_customer.strName
                                                ORDER BY
                                                mst_customer.strName ASC";
                                                                    $resultm = $db->RunQuery($sqlm);
                                                                    $existingRws = 0;
                                                                    $totAmnt = 0;
                                                                    while ($rowm = mysqli_fetch_array($resultm)) {
                                                                        $existingRws++;
                                                                        $customerId = $rowm['intId'];
                                                                        $customer = $rowm['strName'];
                                                                        $outstanding = $rowm['qty'];
                                                                        $totAmnt+=$outstanding;
                                                                        ?>

                                                                        <tr class="normalfnt">
                                                                            <td bgcolor="#FFFFFF" class="normalfntMid"><input name="chkSelect" type="checkbox" value="<?php echo $customerId; ?>" /></td>
                                                                            <td bgcolor="#FFFFFF" class="normalfntMid" id="<?php echo $customerId; ?>"><?php echo $customer ?></td>
                                                                        </tr>        
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </table>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" class="normalfnt">
                                                            <div id="divTable1" style="overflow:scroll;width:100%;height:150px"  >
                                                                <table width="97%" id="tblTransTypes" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
                                                                    <tr class="">
                                                                        <td width="44" bgcolor="#FAD163" class="normalfntMid"><input type="checkbox" name="chkAll2" id="chkAll2" />
                                                                        </td>
                                                                        <td width="234"  height="24" bgcolor="#FAD163" class="normalfntMid"><strong>Transaction Type</strong></td>
                                                                    </tr>


                                                                    <tr class="normalfnt">
                                                                        <td bgcolor="#FFFFFF" class="normalfntMid"><input name="chkSelect" type="checkbox" value="1" class="transType" /></td>
                                                                        <td bgcolor="#FFFFFF" class="normalfntMid" id="Advance Received">Advance Received</td>
                                                                    </tr>  
                                                                    <tr class="normalfnt">
                                                                        <td bgcolor="#FFFFFF" class="normalfntMid"><input name="chkSelect" type="checkbox" value="2" class="transType" /></td>
                                                                        <td bgcolor="#FFFFFF" class="normalfntMid" id="Bank Deposit">Bank Deposit</td>
                                                                    </tr>  
                                                                    <tr class="normalfnt">
                                                                        <td bgcolor="#FFFFFF" class="normalfntMid"><input name="chkSelect" type="checkbox" value="3" class="transType" /></td>
                                                                        <td bgcolor="#FFFFFF" class="normalfntMid" id="Petty Cash">Petty Cash</td>
                                                                    </tr>  
                                                                    <tr class="normalfnt">
                                                                        <td bgcolor="#FFFFFF" class="normalfntMid"><input name="chkSelect" type="checkbox" value="4" class="transType" /></td>
                                                                        <td bgcolor="#FFFFFF" class="normalfntMid" id="Bank Payment">Bank Payment</td>
                                                                    </tr>  
                                                                    <tr class="normalfnt">
                                                                        <td bgcolor="#FFFFFF" class="normalfntMid"><input name="chkSelect" type="checkbox" value="5" class="transType" /></td>
                                                                        <td bgcolor="#FFFFFF" class="normalfntMid" id="Credit Note">Credit Note</td>
                                                                    </tr>  
                                                                    <tr class="normalfnt">
                                                                        <td bgcolor="#FFFFFF" class="normalfntMid"><input name="chkSelect" type="checkbox" value="6" class="transType" /></td>
                                                                        <td bgcolor="#FFFFFF" class="normalfntMid" id="Sales Invoice">Sales Invoice</td>
                                                                    </tr>  
                                                                    <tr class="normalfnt">
                                                                        <td bgcolor="#FFFFFF" class="normalfntMid"><input name="chkSelect" type="checkbox" value="7" class="transType" /></td>
                                                                        <td bgcolor="#FFFFFF" class="normalfntMid" id="Received Payments">Received Payments</td>
                                                                    </tr>  

                                                                </table> </div>
                                                        </td>
                                                    </tr>
                                                </table></div>
                                        </td>
                                        <td bgcolor="#FFFFFF" class=""></td>
                                    </tr>
                                    <tr>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td bgcolor="#FFFFFF" class="">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="21" class="normalfnt">&nbsp;</td>
                                        <td width="44" bgcolor="#FFFFFF" class="">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="21" class="normalfnt">&nbsp;</td>
                                        <td bgcolor="#FFFFFF" class="">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="21" class="normalfnt">&nbsp;</td>
                                        <td bgcolor="#FFFFFF" class="">&nbsp;</td>
                                    </tr>
                                    <tr style="display:none">
                                        <td height="21" class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td bgcolor="#FFFFFF" class="">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="21" class="normalfnt">&nbsp;</td>
                                        <td colspan="5" rowspan="5" class="normalfnt">

                                            <div class="normalfnt" style="text-align:left; font-size:12px; background-color:#FFD8B0" >Required Fields For Report</div>
                                            <div style="border-style:double; border-color:#FF8000">
                                                <div id="divAllFields" style="overflow:scroll;width:100%;height:180px"  >
                                                    <div id="divCommonFields">
                                                        <div class="normalfnt" style="text-align:left; font-size:12px; background-color:#FFD8B0" >Common Fields</div>
                                                        <table width="100%" >
                                                            <tr>
                                                                <td width="33%"><input name="chkCust" id="chkCust" type="checkbox" value="1" checked="checked" class="check" />&nbsp;Customer</td>
                                                                <td width="33%"><input name="chkDate" id="chkDate" type="checkbox" value="1" checked="checked" class="check"/>&nbsp;Date</td>
                                                                <td width="33%"><input name="chkTran" id="chkTran" type="checkbox" value="1" checked="checked" class="check"/>&nbsp;Trans Action Type</td>

                                                            </tr>
                                                            <tr>
                                                                <td><input name="chkDocNo" id="chkDocNo" type="checkbox" value="1" checked="checked" class="check"/>&nbsp;Document No</td>
                                                                <td><input name="chkAcc" id="chkAcc" type="checkbox" value="1" checked="checked" class="check"/>&nbsp;Account</td>
                                                                <td><input name="chkCD" id="chkCD" type="checkbox" value="1" checked="checked" class="check"/>&nbsp;C/D</td>
                                                            </tr>
                                                            <tr>
                                                                <tr>
                                                                    <td><input name="chkCurr" id="chkCurr" type="checkbox" value="1" checked="checked" class="check"/>&nbsp;Currency</td>
                                                                    <td width="33%"><input name="chkClosBal" id="chkClosBal" type="checkbox" value="1" checked="checked" class="check"/>&nbsp;Closing Balance</td>
                                                                    <td width="33%"><input name="chkRemarks" id="chkRemarks" type="checkbox" value="1" checked="checked" class="check"/>&nbsp;Remarks</td>
                                                                </tr>
                                                                <tr>
                                                                    <td width="33%"></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                </tr>
                                                        </table></div>
                                                    <div id="div1" style="display:none">
                                                        <div class="normalfnt" style="text-align:left; font-size:12px; background-color:#FFD8B0" >Advance Recieved</div>
                                                        <table width="100%" id="tbl1" >
                                                            <tr>
                                                                <td><input name="chkPayMethod" id="chkPayMethod" type="checkbox" value="1"  class="check"/>&nbsp;Pay Method</td>
                                                                <td><input name="chkPayDate" id="chkPayDate" type="checkbox" value="1"  class="check"/>&nbsp;Reference Date</td>
                                                                <td><input name="chkOrganiz" id="chkOrganiz" type="checkbox" value="1"  class="check"/>&nbsp;Reference Organization</td>
                                                            </tr>
                                                            <tr>
                                                                <td><input name="chkRefNo" id="chkRefNo" type="checkbox" value="1"  class="check"/>&nbsp;Reference No</td>
                                                                <td><input name="chkPerfInvNo" id="chkPerfInvNo" type="checkbox" value="1"  class="check"/>&nbsp;Performe Invoice No</td>
                                                                <td></td>
                                                            </tr>
                                                        </table></div> <!--divBnkDepositFields-->
                                                    <div id="div2" style="display:none">
                                                        <div class="normalfnt" style="text-align:left; font-size:12px; background-color:#FFD8B0" >Bank Deposit</div>
                                                        <table width="100%"  id="tbl2">
                                                            <tr>
                                                                <td><input name="chkMemo" id="chkMemo" type="checkbox" value="1"  class="check"/>&nbsp;Memo</td>
                                                                <td><input name="chkDimention" id="chkDimention" type="checkbox" value="1"  class="check"/>&nbsp;Dimention</td>
                                                                <td></td>
                                                            </tr>
                                                        </table></div>    <!--divPettyCashFields-->  
                                                    <div id="div3" style="display:none">
                                                        <div class="normalfnt" style="text-align:left; font-size:12px; background-color:#FFD8B0" >Petty Cash</div>
                                                        <table width="100%" id="tbl3" >
                                                            <tr>
                                                                <td><input name="chkPayMethod" id="chkPayMethod1" type="checkbox" value="1"  class="check"/>&nbsp;Pay Method</td>
                                                                <td><input name="chkPayDate" id="chkPayDate1" type="checkbox" value="1"  class="check"/>&nbsp;Reference Date</td>
                                                                <td><input name="chkOrganiz" id="chkOrganiz1" type="checkbox" value="1"  class="check"/>&nbsp;Reference Organization</td>
                                                            </tr>
                                                            <tr>
                                                                <td><input name="chkRefNo" id="chkRefNo1" type="checkbox" value="1"  class="check"/>&nbsp;Reference No</td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                        </table></div>      <!--divBnkPaymntsFields-->
                                                    <div id="div4" style="display:none">
                                                        <div class="normalfnt" style="text-align:left; font-size:12px; background-color:#FFD8B0" >Bank Payments</div>
                                                        <table width="100%" id="tbl4" >
                                                            <tr>
                                                                <td><input name="chkPayMethod" id="chkPayMethod2" type="checkbox" value="1"  class="check"/>&nbsp;Pay Method</td>
                                                                <td><input name="chkPayDate" id="chkPayDate2" type="checkbox" value="1"  class="check"/>&nbsp;Reference Date</td>
                                                                <td><input name="chkOrganiz" id="chkOrganiz2" type="checkbox" value="1"  class="check"/>&nbsp;Reference Organization</td>
                                                            </tr>
                                                            <tr>
                                                                <td><input name="chkRefNo" id="chkRefNo2" type="checkbox" value="1"  class="check"/>&nbsp;Reference No</td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                        </table></div>      <!--divCreditNoteFields-->
                                                    <div id="div5" style="display:none">
                                                        <div class="normalfnt" style="text-align:left; font-size:12px; background-color:#FFD8B0" >Credit Note</div>
                                                        <table width="100%"  id="tbl5">
                                                            <tr>
                                                                <td><input name="chkInvNo" id="chkInvNo" type="checkbox" value="1"   class="check"/>&nbsp;Invoice No</td>
                                                                <td><input name="chkPoNo" id="chkPoNo" type="checkbox" value="1"   class="check"/>&nbsp;PO No</td>
                                                                <td><input name="chkMktUser" id="chkMktUser" type="checkbox" value="1"   class="check"/>&nbsp;Marketing User</td>
                                                            </tr>
                                                        </table></div>  <!--divSalesInvFields-->    
                                                    <div id="div6" style="display:none">
                                                        <div class="normalfnt" style="text-align:left; font-size:12px; background-color:#FFD8B0" >Sales Invoice</div>
                                                        <table width="100%" id="tbl6" >
                                                            <tr>
                                                                <td><input name="chkPoNo" id="chkPoNo3" type="checkbox" value="1"   class="check"/>&nbsp;PO No</td>
                                                                <td><input name="chkTerms" id="chkTerms" type="checkbox" value="1"   class="check"/>&nbsp;Terms</td>
                                                                <td><input name="chkMktUser3" id="chkMktUser3" type="checkbox" value="1"   class="check"/>&nbsp;Marketing User</td>
                                                            </tr>
                                                            <tr>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                        </table></div>      <!--divRcvPaymntsFields-->
                                                    <div id="div7" style="display:none">
                                                        <div class="normalfnt" style="text-align:left; font-size:12px; background-color:#FFD8B0" >Received Payments</div>
                                                        <table width="100%"  id="tbl7">
                                                            <tr>
                                                                <td><input name="chkPayMethod" id="chkPayMethod4" type="checkbox" value="1" />&nbsp;Pay Method</td>
                                                                <td><input name="chkPayDate" id="chkPayDate4" type="checkbox" value="1" />&nbsp;Reference Date</td>
                                                                <td><input name="chkOrganiz" id="chkOrganiz4" type="checkbox" value="1" />&nbsp;Reference Organization</td>
                                                            </tr>
                                                            <tr>
                                                                <td><input name="chkRefNo" id="chkRefNo4" type="checkbox" value="1" />&nbsp;Reference No</td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                        </table></div>      
                                                </div>
                                        </td>
                                        <td bgcolor="#FFFFFF" class="">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="21" class="normalfnt">&nbsp;</td>
                                        <td bgcolor="#FFFFFF" class="">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="21" class="normalfnt">&nbsp;</td>
                                        <td bgcolor="#FFFFFF" class="">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="21" class="normalfnt">&nbsp;</td>
                                        <td bgcolor="#FFFFFF" class="">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="21" class="normalfnt">&nbsp;</td>
                                        <td bgcolor="#FFFFFF" class="">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="21" class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td bgcolor="#FFFFFF" class="">&nbsp;</td>
                                    </tr>
                                </table></td>
                        </tr>
                        <tr>
                            <td align="center">
                                <table width="100%" class="tableBorder_allRound">
                                    <tr>
                                        <td width="100%" align="center" bgcolor=""><img style="display:none" border="0" src="../../../../images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img border="0" src="../../../../images/Treport.jpg" alt="Summery Report" name="butReport"width="92" height="24"  class="mouseover" id="butReport" tabindex="24"/><a href="../../../../main.php"><img  src="../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">&nbsp;</td>
                        </tr>
                    </table>
                </div>
            </div>
        </form>
    </body>
</html>
<?php
session_start();
$backwardseperator = "../../../../";
$companyId = $_SESSION['headCompanyId'];
$location = $_SESSION['CompanyID'];
$intUser = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath = $_SERVER['PHP_SELF'];
include "{$backwardseperator}dataAccess/Connector.php";

$ledgerHeaderAcc = $_REQUEST['ledgerHeaderAcc'];
$startDate = $_REQUEST['startDate'];
$endDate = $_REQUEST['endDate'];
$accPeriod = $_REQUEST['accPeriod'];

//get Header Ledger Account Details
$resultLedgHed = $db->RunQuery("SELECT
                            mst_financechartofaccounts.strCode,
                            mst_financechartofaccounts.strName                               
                        FROM
                            mst_financechartofaccounts
                        WHERE
                            mst_financechartofaccounts.intId =" . $ledgerHeaderAcc);
$rowLedgHed = mysqli_fetch_array($resultLedgHed);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Ledger Filter</title>
        <link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />

        <script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
        <script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
        <script type="application/javascript" src="AccountReports.js"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

        <link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
        <script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
        <link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css" />
        <link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css" /> 

    <!--<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
    <script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
    <script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
    <script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
    <script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script> -->
        <style>
            table.rptTable{
                width: 100%;
                border-collapse:collapse;
            }
            table.rptTable tr td{
                border:1px solid black;
            }
            .rptTblHeader {
                font-size: 14px;
                text-align: center;
                font-weight:bold;
            }
            .rptTblBody {
                font-size: 12px;
            }
            .drillLink{
                cursor:pointer;
            }
            a:hover
            { 
                font-weight: bold;
                text-decoration:underline;
                color: #0000FF;
            }
        </style>
    </head>
    <body>
        <div align="center">
            <div class="trans_layoutS">
                <div class="trans_text">Ledger Account</div> 
                <form name="frmLedFilter" id="frmLedFilter">
                    <table  width="100%" cellspacing="0">
                        <tr>
                            <td colspan="2" class="rptTblHeader">Ledger Header Account</td>
                        </tr>
                        <tr>
                            <td colspan="2" class="rptTblHeader">
                                <strong><?php echo $rowLedgHed['strName'] ?> - <?php echo $rowLedgHed['strCode'] ?></strong>
                            </td>
                        </tr>                                              
                        <tr>
                            <td class="normalfntMid">Start Date<input type="hidden" name="accPeriod" id="accPeriod" value="$accPeriod" /></td>
                            <td class="normalfntMid">End Date</td>
                        </tr>
                        <tr>
                            <td class="normalfntMid"><input name="startDate" type="text" value="<?php echo $startDate; ?>" class="validate[required]" id="startDate" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>                            
                            <td class="normalfntMid"><input name="endDate" type="text" value="<?php echo $endDate; ?>" class="validate[required]" id="endDate" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table align="center" style="width: 75%;" class="rptTable" >


                                    <?php
                                    //get subLedger Accounts from header account
                                    $resultLedgr = $db->RunQuery("SELECT
                            mst_financechartofaccounts.strCode,
                            mst_financechartofaccounts.strName,
                            mst_financechartofaccounts.intId
                        FROM
                            mst_financechartofaccounts
                        WHERE
                            mst_financechartofaccounts.headerAcc =" . $ledgerHeaderAcc);
                                    while ($rowLedgr = mysqli_fetch_array($resultLedgr)) {
                                        ?>
                                        <tr>
                                            <td><input type="checkbox" name="chkAcc" value="<?php echo $rowLedgr['intId'] ?>" /></td>
                                            <td class="normalfnt"><?php echo $rowLedgr['strName'] ?></td>
                                            <td class="normalfnt"><?php echo $rowLedgr['strCode'] ?></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="center"><img border="0" src="../../../../images/Treport.jpg" alt="Summery Report" name="butReport" width="92" height="24"  class="mouseover" id="butReport"/></td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </body>

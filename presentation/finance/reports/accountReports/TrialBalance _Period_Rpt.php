<?php
session_start();
$backwardseperator = "../../../../";
$companyId = $_SESSION['headCompanyId'];
$location = $_SESSION['CompanyID'];
$intUser = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath = $_SERVER['PHP_SELF'];
include "{$backwardseperator}dataAccess/Connector.php";

$locationId = $location; //this locationId use in report header(reportHeader.php)--------------------

$startDate= $_REQUEST['startDate'];
$endDate = $_REQUEST['endDate'];
$accPeriod = $_REQUEST['accPeriod'];
$ct=0;//Row Counter
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Trial Balance - Period</title>
        <link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
        <script type="application/javascript" src="DrillAccount.js"></script>
        <style>
            table.rptTable{
                width: 100%;
                border-collapse:collapse;
            }
            table.rptTable tr td{
                border:1px solid black;
            }
            .rptTblHeader {
                font-size: 14px;
                text-align: center;
                font-weight:bold;
            }
            .rptTblBody {
                font-size: 12px;
            }
            .drillLink{
                cursor:pointer;
            }
            a:hover
            { 
                font-weight: bold;
                text-decoration:underline;
                color: #0000FF;
            }
            
        </style>
    </head>
    <body>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="3"></td>
            </tr>
            <tr>
                <td width="20%"></td>
                <td width="60%" height="80" valign="top"><?php include '../../reportHeader.php' ?></td>
                <td width="20%"></td>
            </tr>
            <tr>
                <td colspan="3"></td>
            </tr>
        </table>
        <div align="center">
            <div style="background-color:#FFF" ><strong>Trial Balance - Period</strong><strong></strong></div>
            <div style="background-color:#FFF" ><strong>From</strong>&nbsp;&nbsp;&nbsp;<i><?php echo $startDate ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</i><strong>To</strong>&nbsp;&nbsp;&nbsp;<i><?php echo $endDate ?></i><strong></strong></div><br/>
            <table width="1100" border="0" align="center" bgcolor="#FFFFFF">
                <tr>
                    <td><samp style="font-size: 10px;font-weight:bold;">All amounts in LKR</samp></td>
                </tr>                
                <tr>
                    <td>
                        <table align="center" class="rptTable">
                            <tr class="rptTblHeader">
                                <td></td>
                                <td>Type</td>
                                <td>Code</td>                                
                                <td>Debit</td>
                                <td>Credit</td>                                
                            </tr>
                            <?php
                            $resultLedg = $db->RunQuery("SELECT
                                                            mst_financechartofaccounts.intId,
                                                            mst_financechartofaccounts.strCode,
                                                            mst_financechartofaccounts.strName,
                                                            mst_financechartofaccounts.strType,
                                                            mst_financialmaintype.intId AS mainTypeID
                                                            FROM
                                                            mst_financechartofaccounts
                                                            Inner Join mst_financialsubtype ON mst_financechartofaccounts.intFinancialTypeId = mst_financialsubtype.intId
                                                            Inner Join mst_financialmaintype ON mst_financialsubtype.intFinancialMainTypeID = mst_financialmaintype.intId
                                                            Inner Join mst_financechartofaccounts_companies ON mst_financechartofaccounts.intId = mst_financechartofaccounts_companies.intChartOfAccountId
                                                            WHERE
                                                            mst_financechartofaccounts.strType =  'Posting' AND
                                                            mst_financechartofaccounts_companies.intCompanyId =  '$companyId'
                                                            ORDER BY
                                                            mst_financechartofaccounts.strCode ASC");
                            while($rowLedg=mysqli_fetch_array($resultLedg)){
                            $ledgerAcc=$rowLedg['intId'];
                            $accPeriodSql="";
                            //get account period value for ' Manufacturing ' and 'Income Statements'
                            if($rowLedg['mainTypeID']==1 | $rowLedg['mainTypeID']==2){
                                $accPeriodSql=" AND fin_transactions.intAccPeriod='$accPeriod'";
                            }
                            else{// skip accounting period for 'Balance Sheet '
                                $accPeriodSql="";
                            }
                            //get Debit total amount                            
                            $resultDebitTot = $db->RunQuery("SELECT
                                                                SUM(fin_transactions_details.amount * fin_transactions.currencyRate) AS debitAmount
                                                            FROM
                                                                fin_transactions
                                                                INNER JOIN fin_transactions_details ON fin_transactions.entryId = fin_transactions_details.entryId
                                                            WHERE
                                                                fin_transactions_details.accountId = $ledgerAcc AND
                                                                fin_transactions_details.`credit/debit` = 'D' AND
                                                                fin_transactions.entryDate <= '$endDate' AND
                                                                fin_transactions.entryDate >=  '$startDate' AND
                                                                fin_transactions.authorized = 1 AND
                                                                fin_transactions.delStatus = 0 AND
                                                                fin_transactions.companyId = $companyId".$accPeriodSql);
                            $rowDebitTot=mysqli_fetch_array($resultDebitTot);
                            //get Credit total amount
                            $resultCreditTot = $db->RunQuery("SELECT
                                                                SUM(fin_transactions_details.amount * fin_transactions.currencyRate) AS creditAmount
                                                                FROM
                                                                    fin_transactions
                                                                    INNER JOIN fin_transactions_details ON fin_transactions.entryId = fin_transactions_details.entryId
                                                                WHERE
                                                                    fin_transactions_details.accountId = $ledgerAcc AND
                                                                    fin_transactions_details.`credit/debit` = 'C' AND
                                                                    fin_transactions.entryDate <= '$endDate' AND
                                                                    fin_transactions.entryDate >=  '$startDate' AND
                                                                    fin_transactions.authorized = 1 AND
                                                                    fin_transactions.delStatus = 0 AND
                                                                    fin_transactions.companyId = $companyId".$accPeriodSql);
                            $rowCreditTot=mysqli_fetch_array($resultCreditTot);
                            //caiculate balance
                            $balance=$rowDebitTot['debitAmount'] - $rowCreditTot['creditAmount'];

                            $debitBalance=0;
                            $creditBalance=0;
                            if($balance >= 0) {
                                $debitBalance=$balance;
                            }
                            else {
                                $creditBalance=$balance * (-1);
                            }
                            ?>
                            <tr class="rptTblBody">
                                <td><?php echo ++$ct ?></td>
                                <td><?php echo $rowLedg['strName'] ?></td>
                                <td><a class="drillLink" onclick="trialDril('<?php echo$startDate?>','<?php echo$endDate?>','<?php echo$ledgerAcc?>','<?php echo$accPeriod?>')"><?php echo $rowLedg['strCode'] ?></a></td>
                                <td align="right"><a class="drillLink" onclick="trialDril('<?php echo$startDate?>','<?php echo$endDate?>','<?php echo$ledgerAcc?>','<?php echo$accPeriod?>')"><?php echo  number_format($debitBalance,4) ?></a></td>
                                <td align="right"><a class="drillLink" onclick="trialDril('<?php echo$startDate?>','<?php echo$endDate?>','<?php echo$ledgerAcc?>','<?php echo$accPeriod?>')"><?php echo  number_format($creditBalance,4) ?></a></td>
                            </tr>
                            <?php
                                $dbTotal+=$debitBalance;
                                $crTotal+=$creditBalance;
                            }
                            ?>
                            <tr class="rptTblBody">
                                <td colspan="3" align="center"><strong>Total</strong></td>
                                <td align="right"><strong>0<?php echo number_format($dbTotal,4) ?></strong></td>
                                <td align="right"><strong><?php echo number_format($crTotal,4) ?></strong></td>
                                
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr height="40">
                    <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d H:i:s") ?></strong></span></td>
                </tr>
            </table>
        </div>
    </body>
</html>

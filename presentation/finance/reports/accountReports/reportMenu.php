<?php
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$companyId = $_SESSION['headCompanyId'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Accountant Reports</title>
        <link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
        
        <script src="JS/reportMenu.js" type="text/javascript"></script>
        
        <script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
        <script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
        <script type="application/javascript" src="AccountReports.js"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>
        
        <link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
        <script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
        <link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css" />
        <link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css" /> 
        
       
         
        <!--<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
        <script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
        <script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script> -->
        <style>
           table.custTable{
                font-family: Verdana;
                font-size: 11px;
                color: #000000;
                margin: 0px;
                font-weight: normal;
                text-align:left;
                border-collapse:collapse; 
                width: 100%;
            }
            table.custTable tr td{
                border:1px solid #ff7200;
            }
            
        </style>
    </head>    
    <body>
        <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
                <td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator . 'Header.php'; ?></td>
            </tr> 
        </table>
        <div align="center">
            <div class="trans_layoutL">
                <div class="trans_text">Accountant Reports</div>                
                <table width="100%" cellspacing="0" cellpadding="5">
                    <tr>
                        <td colspan="2">
                            <div class="normalfntMid"> Account Period
                                <select name="cmbAccPeriod" class="validate[required]" id="cmbAccPeriod" style="width:100px">                        
                                <?php
                        $sqlPeriod = "SELECT
                                    mst_financeaccountingperiod.intId,
                                    mst_financeaccountingperiod.dtmStartingDate,
                                    mst_financeaccountingperiod.dtmClosingDate
                                FROM
                                    mst_financeaccountingperiod
                                WHERE
                                    mst_financeaccountingperiod.intStatus =  '1'
                                ORDER BY
                                    mst_financeaccountingperiod.intId DESC";
                        $resultPeriod = $db->RunQuery($sqlPeriod);
                        while ($rowPeriod = mysqli_fetch_array($resultPeriod)) {
                            echo "<option value=\"" . $rowPeriod['intId'] . "\">" .date('Y', strtotime($rowPeriod['dtmStartingDate'])) . "/" . date('Y', strtotime($rowPeriod['dtmClosingDate'])) . "</option>";
                        }
                        ?>
                    </select>
                            </div> 
                        </td>
                    </tr>
                    <tr><td colspan="2"><div class="normalfntMid">General Ledger - LKR</div> </td></tr>
                    
                   
                    
                    <!-- ==============================Selected LKR========================= -->
                    <tr>
                        <td width="50%" align="center" class="tableBorder_bottomRound" colspan="2">
                            <form name="frmLedgerLKRSelected" method="post" id="frmLedgerLKRSelected" autocomplete="off">
                                <table style="width: 100%">
                                
                                     <!-- ######################################################################### -->
				                     <tr height='40px'>
				                    	<td colspan="2" align="left">
				                    		<div class="normalfntLeft"> DATE RANGE
				                                <select name="cmbDateRange" class="validate[required]" id="cmbDateRange" onchange="getDateRange('LKR',event)" style="width:100px">                        
				                                <?php
				                        			$sqlPeriod = "SELECT r.dr_id,r.date_range
																	FROM fin_date_range r 
																	WHERE r.stat != '0'
																	ORDER BY r.date_range ASC ";
							                        $resultPeriod = $db->RunQuery($sqlPeriod);
							                        while ($rowPeriod = mysqli_fetch_array($resultPeriod)) {
							                            echo "<option value=\"" . $rowPeriod['dr_id'] . "\">" .$rowPeriod['date_range'] . "</option>";
							                        }
							                        ?>
							                    </select>
				                            </div>
				                    	</td>
				                    </tr>
                    
                    				<!-- ######################################################################### -->
                                    <tr>
                                        <td>
                                             <table style="width: 100%;" align="center">                                   
                                                
                                            <tr><td class="normalfnt">Start Date<span class="compulsoryRed">*</span></td><td><div class="normalfnt">End Date<span class="compulsoryRed">*</span></div></td></tr>
                                            <tr align="left">
                                                <td><input name="startDateSelect" type="text" value="<?php echo date("Y-m-d"); ?>" class="validate[required]" id="startDateSelect" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                                                <td><input name="endDateSelect" type="text" value="<?php echo date("Y-m-d"); ?>" class="validate[required]" id="endDateSelect" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                                            </tr> 
                                            <tr>
                                                    <td colspan="2"><div class="normalfnt">Ledger Accounts <span class="compulsoryRed">*</span>
                                                        <a id="displayText" href="javascript:toggle('toggleText','displayText');">show</a> </div>
                                                        <div id="toggleText" style="display: none">
                                                            <table class="custTable">
                                                            	<tr>
							                                    	<td colspan="2" align="right"><img border="0" src="../../../../images/Treport.jpg" alt="Summery Report" 
							                                    name="butReportSelectedLedger" width="92" height="24"  class="mouseover" id="butReportSelectedLedger"/>
							                                       </td>
								                                </tr>
                                                                <tr>
                                                                    <td class="normalfnt"><input type="checkbox" name="chkAllAccLKR" id="chkAllAccLKR" onclick="hi()" /></td>
                                                                    <td class="normalfntMid"></td>
                                                                </tr>                                                            
                                                                <?php
                                                                $sql = "SELECT intId,strCode, strName FROM mst_financechartofaccounts WHERE strType='Posting' order by strCode";
                                                                $result = $db->RunQuery($sql);
                                                                while ($row = mysqli_fetch_array($result)) {                                                                    
                                                                    echo "<tr>";
                                                                    echo "<td><input type=\"checkbox\" name=\"chkAccLKR\" id=\"chkAccLKR1\" value=\"".$row['intId']."\"  /></td>";
                                                                    echo "<td>". $row['strCode'] . "-" . $row['strName'] ."</td>";
                                                                    echo "</tr>";
                                                                }
                                                                ?>
                                                            </table> 
                                                        </div>
                                                </td>
                                            </tr>
                                            
                                            
                                        </table>
                                        </td>
                                        <td valign="top">
                                            <table style="width: 100%;">
                                                <tr>
                                                    <td class="normalfnt"><input type="checkbox" name="chkSelect" value="PM" />Payment Method</td>
                                                    <td class="normalfnt"><input type="checkbox" name="chkSelect" value="PN" />Payment Number</td>
                                                </tr>
                                                <tr>
                                                    <td class="normalfnt"><input type="checkbox" name="chkSelect" value="CC" />Cost Center</td>
                                                    <td class="normalfnt"><input type="checkbox" name="chkSelect" value="NA" />Person Name</td>
                                                </tr>
                                                <tr>
                                                    <td class="normalfnt"><input type="checkbox" name="chkSelect" value="CT" />Currency Type</td>
                                                    <td class="normalfnt"><input type="checkbox" name="chkSelect" value="CR" />Currency Rate</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                    	<td colspan="2" align="center"><img border="0" src="../../../../images/Treport.jpg" alt="Summery Report" 
	                                    name="butReportSelectedLedger" style="padding-left: 90px" width="92" height="24"  class="mouseover" id="butReportSelectedLedger"/>
	                                    </td>
	                                </tr>
                                </table>                               
                            </form>
                        </td>                        
                    </tr>
                    <!-- ======================================================= -->
                    <tr><td colspan="2"><div class="normalfntMid">General Ledger - Multi Currency</div> </td></tr>
                    <!-- ==============================Selected Multi Currency========================= -->
                    <tr>
                        <td width="50%" align="center" class="tableBorder_bottomRound" colspan="2">
                            <form name="frmLedgerMLTSelected" method="post" id="frmLedgerMLTSelected" autocomplete="off">
                                <table style="width: 100%">
                                
                                	<!-- ######################################################################### -->
				                     <tr height='40px'>
				                    	<td colspan="2" align="left">
				                    		<div class="normalfntLeft"> DATE RANGE
				                                <select name="cmbDateRange" class="validate[required]" id="cmbDateRange" onchange="getDateRange('MLT',event)" style="width:100px">                        
				                                <?php
				                        			$sqlPeriod = "SELECT r.dr_id,r.date_range
																	FROM fin_date_range r 
																	WHERE r.stat != '0'
																	ORDER BY r.date_range ASC ";
							                        $resultPeriod = $db->RunQuery($sqlPeriod);
							                        while ($rowPeriod = mysqli_fetch_array($resultPeriod)) {
							                            echo "<option value=\"" . $rowPeriod['dr_id'] . "\">" .$rowPeriod['date_range'] . "</option>";
							                        }
							                        ?>
							                    </select>
				                            </div>
				                    	</td>
				                    </tr>
                    
                    				<!-- ######################################################################### -->
                                    <tr>
                                        <td>
                                             <table style="width: 100%;" align="center">                                   
                                                
                                            <tr><td class="normalfnt">Start Date<span class="compulsoryRed">*</span></td><td><div class="normalfnt">End Date<span class="compulsoryRed">*</span></div></td></tr>
                                            <tr align="left">
                                                <td><input name="startDateSelectMLT" type="text" value="<?php echo date("Y-m-d"); ?>" class="validate[required]" id="startDateSelectMLT" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                                                <td><input name="endDateSelectMlt" type="text" value="<?php echo date("Y-m-d"); ?>" class="validate[required]" id="endDateSelectMlt" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                                            </tr>
                                            <tr>
                                        <td><div class="normalfnt">Currency<span class="compulsoryRed">*</span></div></td>
                                        <td style="text-align: left;" align="left">
                                            <select name="cboCurrency" id="cboCurrency" style="width:140px" class="validate[required]">
                                                <option value=""></option>
                                                <?php  
                                                $sql = "SELECT
                                                            mst_financecurrency.intId,
                                                            mst_financecurrency.strCode
                                                            FROM
                                                            mst_financecurrency";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strCode']."</option>";
						}
                                                ?>
                                            </select>
                                        </td>
                                    </tr> 
                                            <tr>
                                                    <td colspan="2"><div class="normalfnt">Ledger Accounts <span class="compulsoryRed">*</span>
                                                        <a id="displayText1" href="javascript:toggle('toggleText1','displayText1');">show</a> </div>
                                                        <div id="toggleText1" style="display: none">
                                                            <table class="custTable">
                                                                <tr>
                                                                    <td class="normalfnt"><input type="checkbox" name="chkAllAccMLT" id="chkAllAccMLT" /></td>
                                                                    <td class="normalfntMid"></td>
                                                                </tr>                                                            
                                                                <?php
                                                                $sql = "SELECT intId,strCode, strName FROM mst_financechartofaccounts WHERE strType='Posting' order by strCode";
                                                                $result = $db->RunQuery($sql);
                                                                while ($row = mysqli_fetch_array($result)) {                                                                    
                                                                    echo "<tr>";
                                                                    echo "<td><input type=\"checkbox\" name=\"chkAccMLT\" id=\"chkAccMLT1\" value=\"".$row['intId']."\"  /></td>";
                                                                    echo "<td>". $row['strCode'] . "-" . $row['strName'] ."</td>";
                                                                    echo "</tr>";
                                                                }
                                                                ?>
                                                            </table> 
                                                        </div>
                                                </td>
                                            </tr>
                                        </table>
                                        </td>
                                        <td valign="top">
                                            <table style="width: 100%;">
                                                <tr>
                                                    <td class="normalfnt"><input type="checkbox" name="chkSelectMLT" value="PM" />Payment Method</td>
                                                    <td class="normalfnt"><input type="checkbox" name="chkSelectMLT" value="PN" />Payment Number</td>
                                                </tr>
                                                <tr>
                                                    <td class="normalfnt"><input type="checkbox" name="chkSelectMLT" value="CC" />Cost Center</td>
                                                    <td class="normalfnt"><input type="checkbox" name="chkSelectMLT" value="NA" />Person Name</td>
                                                </tr>
                                                <tr>
                                                    <td class="normalfnt"><input type="checkbox" name="chkSelectMLT" value="CT" />Currency Type</td>
                                                    <td class="normalfnt"><input type="checkbox" name="chkSelectMLT" value="CR" />Currency Rate</td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr><td colspan="2" align="center"><img border="0" src="../../../../images/Treport.jpg" alt="Summery Report" name="butReportSelectedLedgerMLT" width="92" height="24"  class="mouseover" id="butReportSelectedLedger"/></td></tr>
                                </table>                               
                            </form>
                        </td>                        
                    </tr>
                    <!-- ======================================================= -->
                    <tr><td colspan="2"><div class="normalfntMid">General Ledger - Filter from header Account</div> </td></tr>
                    <tr>
                        <td align="center" class="tableBorder_bottomRound" colspan="2">
                            <form name="frmLedgerLKR" method="post" id="frmLedgerLKR" autocomplete="off">
                                <table style="width: 50%;" align="center">
                                	
                                	<!-- ######################################################################### -->
				                     <tr height='40px'>
				                    	<td colspan="2" align="left">
				                    		<div class="normalfntLeft"> DATE RANGE
				                                <select name="cmbDateRange" class="validate[required]" id="cmbDateRange" onchange="getDateRange('HEADER_ACC',event)" style="width:100px">                        
				                                <?php
				                        			$sqlPeriod = "SELECT r.dr_id,r.date_range
																	FROM fin_date_range r 
																	WHERE r.stat != '0'
																	ORDER BY r.date_range ASC ";
							                        $resultPeriod = $db->RunQuery($sqlPeriod);
							                        while ($rowPeriod = mysqli_fetch_array($resultPeriod)) {
							                            echo "<option value=\"" . $rowPeriod['dr_id'] . "\">" .$rowPeriod['date_range'] . "</option>";
							                        }
							                        ?>
							                    </select>
				                            </div>
				                    	</td>
				                    </tr>
                    
                    				<!-- ######################################################################### -->
                                     
                                    <tr>
                                        <td class="normalfnt">Start Date<span class="compulsoryRed">*</span></td>
                                        <td><div class="normalfnt">End Date<span class="compulsoryRed">*</span></div></td></tr>
                                        <tr>
                                            <td class="normalfnt"><input name="startDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="validate[required]" id="startDate" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                                            <td><input name="endDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="validate[required]" id="endDate" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                                        </tr> 
                                    <tr>
                                        <td><div class="normalfnt">Header Account</div></td>
                                        <td style="text-align: left;" align="left">
                                            <select name="cmbLedgerHeaderAcc" id="cmbLedgerHeaderAcc" style="width:200px">
                                                <option value=""></option>
                                                <?php
                                                $sql = "SELECT
                                                    intId,
                                                    strCode,
                                                    strName
                                                    FROM mst_financechartofaccounts WHERE strType='Heading'
                                                    order by strCode
                                                    ";
                                                $result = $db->RunQuery($sql);
                                                while ($row = mysqli_fetch_array($result)) {
                                                    echo "<option value=\"" . $row['intId'] . "\">" . $row['strCode'] . "-" . $row['strName'] . "</option>";
                                                }
                                                ?>
                                            </select> 
                                        </td>
                                    </tr>
                                    <tr><td colspan="2" align="center"><img border="0" src="../../../../images/Treport.jpg" alt="Summery Report" name="butReportFilter" width="92" height="24"  class="mouseover" id="butReportFilter"/></td></tr>
                                </table>
                            </form>
                        </td>                        
                    </tr>
                    <tr><td colspan="2"></td></tr>
                                        
                    <tr><td colspan="2"><div class="normalfntMid">Trial Balance</div> </td></tr>
                    <tr>
                        <td width="50%" align="center" class="tableBorder_bottomRound">
                            <form name="frmTBCumu" method="post" id="frmTBCumu" autocomplete="off">
                                <table style="width: 75%;" align="center">
                                    <tr><td colspan="2"><div class="normalfntMid">Cumulative</div></td></tr>  
                                    
                                    <!-- ######################################################################### -->
				                     <tr height='40px'>
				                    	<td colspan="2" align="left">
				                    		<div class="normalfntLeft"> DATE RANGE
				                                <select name="cmbDateRange" class="validate[required]" id="cmbDateRange" onchange="getDateRange('CML',event)" style="width:100px">                        
				                                <?php
				                        			$sqlPeriod = "SELECT r.dr_id,r.date_range
																	FROM fin_date_range r 
																	WHERE r.stat != '0'
																	ORDER BY r.date_range ASC ";
							                        $resultPeriod = $db->RunQuery($sqlPeriod);
							                        while ($rowPeriod = mysqli_fetch_array($resultPeriod)) {
							                            echo "<option value=\"" . $rowPeriod['dr_id'] . "\">" .$rowPeriod['date_range'] . "</option>";
							                        }
							                        ?>
							                    </select>
				                            </div>
				                    	</td>
				                    </tr>
                    
                    				<!-- ######################################################################### -->
                                                                      
                                    <tr><td><div class="normalfnt">Start Date<span class="compulsoryRed">*</span></div></td><td><div class="normalfnt">End Date<span class="compulsoryRed">*</span></div></td></tr>
                                    <tr align="left">
                                        <td><input name="startDate3" type="text" value="<?php echo date("Y-m-d"); ?>" class="validate[required]" id="startDate3" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                                        <td><input name="endDate3" type="text" value="<?php echo date("Y-m-d"); ?>" class="validate[required]" id="endDate3" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                                    </tr> 
                                    <tr><td colspan="2" align="center"><img border="0" src="../../../../images/Treport.jpg" alt="Summery Report" name="butReport3" width="92" height="24"  class="mouseover" id="butReport3"/></td></tr>
                                </table>
                            </form>
                        </td>
                        <td width="50%" align="center" class="tableBorder_bottomRound">
                            <form name="frmTBCPeriod" method="post" id="frmTBCPeriod" autocomplete="off">
                                <table style="width: 75%;" align="center">
                                    <tr><td colspan="2"><div class="normalfntMid">Period</div></td></tr>
                                   
                                    <!-- ######################################################################### -->
				                     <tr height='40px'>
				                    	<td colspan="2" align="left">
				                    		<div class="normalfntLeft"> DATE RANGE
				                                <select name="cmbDateRange" class="validate[required]" id="cmbDateRange" onchange="getDateRange('PERIOD',event)" style="width:100px">                        
				                                <?php
				                        			$sqlPeriod = "SELECT r.dr_id,r.date_range
																	FROM fin_date_range r 
																	WHERE r.stat != '0'
																	ORDER BY r.date_range ASC ";
							                        $resultPeriod = $db->RunQuery($sqlPeriod);
							                        while ($rowPeriod = mysqli_fetch_array($resultPeriod)) {
							                            echo "<option value=\"" . $rowPeriod['dr_id'] . "\">" .$rowPeriod['date_range'] . "</option>";
							                        }
							                        ?>
							                    </select>
				                            </div>
				                    	</td>
				                    </tr>
                    
                    				<!-- ######################################################################### -->
                                                                      
                                    <tr><td><div class="normalfnt">Start Date<span class="compulsoryRed">*</span></div></td><td><div class="normalfnt">End Date<span class="compulsoryRed">*</span></div></td></tr>
                                    <tr align="left">
                                        <td><input name="startDate4" type="text" value="<?php echo date("Y-m-d"); ?>" class="validate[required]" id="startDate4" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                                        <td><input name="endDate4" type="text" value="<?php echo date("Y-m-d"); ?>" class="validate[required]" id="endDate4" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                                    </tr> 
                                    <tr><td colspan="2" align="center"><img border="0" src="../../../../images/Treport.jpg" alt="Summery Report" name="butReport4" width="92" height="24"  class="mouseover" id="butReport4"/></td></tr>
                                </table>
                            </form>
                        </td>
                    </tr>
                    <tr><td colspan="2"></td></tr>
                    <tr>
                        <td width="50%" align="center" class="tableBorder_bottomRound">
                            <form name="frmTBCumuAllCompany" method="post" id="frmTBCumuAllCompany" autocomplete="off">
                                <table style="width: 75%;" align="center">
                                    <tr><td colspan="2"><div class="normalfntMid">Cumulative - All Companys</div></td></tr>
                                    
                                    <!-- ######################################################################### -->
				                     <tr height='40px'>
				                    	<td colspan="2" align="left">
				                    		<div class="normalfntLeft"> DATE RANGE
				                                <select name="cmbDateRange" class="validate[required]" id="cmbDateRange" onchange="getDateRange('CMLA',event)" style="width:100px">                        
				                                <?php
				                        			$sqlPeriod = "SELECT r.dr_id,r.date_range
																	FROM fin_date_range r 
																	WHERE r.stat != '0'
																	ORDER BY r.date_range ASC ";
							                        $resultPeriod = $db->RunQuery($sqlPeriod);
							                        while ($rowPeriod = mysqli_fetch_array($resultPeriod)) {
							                            echo "<option value=\"" . $rowPeriod['dr_id'] . "\">" .$rowPeriod['date_range'] . "</option>";
							                        }
							                        ?>
							                    </select>
				                            </div>
				                    	</td>
				                    </tr>
                    
                    				<!-- ######################################################################### -->
                                                                      
                                    <tr><td><div class="normalfnt">Start Date<span class="compulsoryRed">*</span></div></td><td><div class="normalfnt">End Date<span class="compulsoryRed">*</span></div></td></tr>
                                    <tr align="left">
                                        <td><input name="startDateAll" type="text" value="<?php echo date("Y-m-d"); ?>" class="validate[required]" id="startDateAll" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                                        <td><input name="endDateAll" type="text" value="<?php echo date("Y-m-d"); ?>" class="validate[required]" id="endDateAll" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                                    </tr> 
                                    <tr><td colspan="2" align="center"><img border="0" src="../../../../images/Treport.jpg" alt="Summery Report" name="butReport3" width="92" height="24"  class="mouseover" id="butReport3"/></td></tr>
                                </table>
                            </form>
                        </td>
                        <td width="50%" align="center" class="tableBorder_bottomRound">
                            
                        </td>
                    </tr>
                </table>
                
            </div>
        </div>
    </body>
</html>
//############ CREATED BY :CHANDANA ################

function getDateRange(type,event){
	//alert(type+"-"+$(event.target).val());
	var DateRange_id = $(event.target).val();
	
	if(DateRange_id == '1'){ 	//custom
		
		var currentDate = new Date();
		
		var day = currentDate.getDate();
		var month = currentDate.getMonth() + 1;
		var year = currentDate.getFullYear();
		var edate = year+"-"+month+"-"+day; 
		var sdate = year+"-"+month+"-"+day; 
		
		DisplayDates(sdate,edate,type);
		
	}
	else if(DateRange_id == '2'){ 	// this month
				
		var currentDate = new Date();
		
		var day = currentDate.getDate();
		var month = currentDate.getMonth() + 1;
		var year = currentDate.getFullYear();
		var edate = year+"-"+month+"-"+day; 
		var sdate = year+"-"+month+"-"+"1"; 
		
		//$('#startDateSelect').val(sdate);
		//$('#endDateSelect').val(edate);
		
		DisplayDates(sdate,edate,type);
	}
	else if(DateRange_id == '3'){ 	// Last month
		
		var currentDate = new Date();
		var month ='0';
		var year = '0';
		var sday ='1';
		var lastDayOfMonth;
		
		month = currentDate.getMonth() + 1;
		
		if(month > 1 ){
			month = month - 1;
			year = currentDate.getFullYear();
			// ################# last day of a month ###########################
			lastDayOfMonth = new Date(year, month , 0);
		}
		else{
			month = '12';
			year = currentDate.getFullYear() - 1;
			
			// ################# last day of a month ###########################
			lastDayOfMonth = new Date(year, month , 0);
		}
		//alert(lastDayOfMonth.getDate("d"));
		
		
		var edate = year+"-"+month+"-"+lastDayOfMonth.getDate("d"); 
		var sdate = year+"-"+month+"-"+sday; 
		
		//$('#startDateSelect').val(sdate);
		//$('#endDateSelect').val(edate);
		
		DisplayDates(sdate,edate,type);
	}
	else if(DateRange_id == '4'){	// this financial year
		
				
		var url = "AJAX/reportMenu-get.php?requestType=getDateRange";
		var httpobj = $.ajax({
		url:url,
		dataType:'json',
		async:false,
		success:function(json)
		{
			//alert(json.stDate);
			
			var currentDate = new Date();
			var day = currentDate.getDate();
			var month = currentDate.getMonth() + 1;
			var year = currentDate.getFullYear();
			var edate = year+"-"+month+"-"+day; 
			
			//$('#startDateSelect').val(json.stDate); //financial starting date
			//$('#endDateSelect').val(edate);
			DisplayDates(json.stDate,edate,type);
		}
		});
	}
	else if(DateRange_id == '5'){ 	// Yesterday
				
		var currentDate = new Date();
		currentDate.setDate(currentDate.getDate() - 1);	// yesterday
		
		var day = currentDate.getDate();
		var month = currentDate.getMonth() + 1;
		var year = currentDate.getFullYear();
		var edate = year+"-"+month+"-"+day; 
		var sdate = year+"-"+month+"-"+day;
		
		
		//$('#startDateSelect').val(sdate);
		//$('#endDateSelect').val(edate);
		DisplayDates(sdate,edate,type);
	}
	else if(DateRange_id == '6'){ 	// this week
		
		var currentDate = new Date;
		var firstday = new Date(currentDate.setDate(currentDate.getDate() - currentDate.getDay()));
		var lastday = new Date(currentDate.setDate(currentDate.getDate() - currentDate.getDay()+6));
		
		var sday = firstday.getDate();
		var smonth = firstday.getMonth() + 1;
		var syear = firstday.getFullYear();
		var sdate = syear+"-"+smonth+"-"+sday;
		
		var lday = lastday.getDate();
		var lmonth = lastday.getMonth() + 1;
		var lyear = lastday.getFullYear();
		var edate = lyear+"-"+lmonth+"-"+lday;
		
				
		//$('#startDateSelect').val(sdate);
		//$('#endDateSelect').val(edate);
		DisplayDates(sdate,edate,type);
	}
	else if(DateRange_id == '7'){ 	// First financial quater
		//var d ;
		var url = "AJAX/reportMenu-get.php?requestType=getDateRange";
		var httpobj = $.ajax({
		url:url,
		dataType:'json',
		async:false,
		success:function(json)
		{
			
			d = new Date(json.stDate);
			
			 var quarter = Math.floor((d.getMonth() / 3));	
			  
			 var firstDate = new Date(d.getFullYear(), quarter * 3, 1);
			 var sday = firstDate.getDate();
			 var smonth = firstDate.getMonth() + 1;
			 var syear = firstDate.getFullYear();
			 var sdate = syear+"-"+smonth+"-"+sday;
			 
		    
		     var lastday = new Date(firstDate.getFullYear(), firstDate.getMonth() + 3, 0);
		     
		     var lday = lastday.getDate();
			 var lmonth = lastday.getMonth() + 1;
			 var lyear = lastday.getFullYear();
			 var edate = lyear+"-"+lmonth+"-"+lday;
			 
			 //$("#startDateSelect").val(sdate);
		    // $("#endDateSelect").val(edate);
		     DisplayDates(sdate,edate,type);
			
		}
		});
		
	
		
	}
	else if(DateRange_id == '8'){ 	// Second financial quater
		//var d ;
		var url = "AJAX/reportMenu-get.php?requestType=getDateRange";
		var httpobj = $.ajax({
		url:url,
		dataType:'json',
		async:false,
		success:function(json)
		{
			
			d = new Date(json.stDate);
			
			 var quarter = Math.floor(((d.getMonth()+ 3)  / 3));	
			 
			 var firstDate = new Date(d.getFullYear(), quarter * 3, 1);
			 var sday = firstDate.getDate();
			 var smonth = firstDate.getMonth() + 1;
			 var syear = firstDate.getFullYear();
			 var sdate = syear+"-"+smonth+"-"+sday;
			 
		     
		     var lastday = new Date(firstDate.getFullYear(), firstDate.getMonth() + 3, 0);
		     
		     var lday = lastday.getDate();
			 var lmonth = lastday.getMonth() + 1;
			 var lyear = lastday.getFullYear();
			 var edate = lyear+"-"+lmonth+"-"+lday;
			 
			 //$("#startDateSelect").val(sdate);
		     //$("#endDateSelect").val(edate);
			
		     DisplayDates(sdate,edate,type);
		}
		});
				
	}
	else if(DateRange_id == '9'){ 	// Third financial quater
		//var d ;
		var url = "AJAX/reportMenu-get.php?requestType=getDateRange";
		var httpobj = $.ajax({
		url:url,
		dataType:'json',
		async:false,
		success:function(json)
		{
			
			d = new Date(json.stDate);
			
			 var quarter = Math.floor(((d.getMonth()+ 6)  / 3));	
			 
			 var firstDate = new Date(d.getFullYear(), quarter * 3, 1);
			 var sday = firstDate.getDate();
			 var smonth = firstDate.getMonth() + 1;
			 var syear = firstDate.getFullYear();
			 var sdate = syear+"-"+smonth+"-"+sday;
			 
		     $("#startDateSelect").val(sdate);
		     var lastday = new Date(firstDate.getFullYear(), firstDate.getMonth() + 3, 0);
		     
		     var lday = lastday.getDate();
			 var lmonth = lastday.getMonth() + 1;
			 var lyear = lastday.getFullYear();
			 var edate = lyear+"-"+lmonth+"-"+lday;
			 
			 //$("#startDateSelect").val(sdate);
		    //$("#endDateSelect").val(edate);
			 DisplayDates(sdate,edate,type);
			
		}
		});
					
	}
	else if(DateRange_id == '10'){ 	// 4th financial quater
		//var d ;
		var url = "AJAX/reportMenu-get.php?requestType=getDateRange";
		var httpobj = $.ajax({
		url:url,
		dataType:'json',
		async:false,
		success:function(json)
		{
			
			d = new Date(json.stDate);
			
			 var quarter = Math.floor(((d.getMonth()+ 9)  / 3));	
			 
			 var firstDate = new Date(d.getFullYear(), quarter * 3, 1);
			 var sday = firstDate.getDate();
			 var smonth = firstDate.getMonth() + 1;
			 var syear = firstDate.getFullYear();
			 var sdate = syear+"-"+smonth+"-"+sday;
			 
		     
		     var lastday = new Date(firstDate.getFullYear(), firstDate.getMonth() + 3, 0);
		     
		     var lday = lastday.getDate();
			 var lmonth = lastday.getMonth() + 1;
			 var lyear = lastday.getFullYear();
			 var edate = lyear+"-"+lmonth+"-"+lday;
			
			 //$("#startDateSelect").val(sdate);
		     //$("#endDateSelect").val(edate);
			 DisplayDates(sdate,edate,type);
		}
		});
					
	}
};

function DisplayDates(sdate,edate,type){
	if(type == "LKR"){
		$('#startDateSelect').val(sdate);
		$('#endDateSelect').val(edate);
	}
	else if(type == "MLT"){
		$('#startDateSelectMLT').val(sdate);
		$('#endDateSelectMlt').val(edate);
	}
	else if(type == "HEADER_ACC"){
		$('#startDate').val(sdate);
		$('#endDate').val(edate);
	}
	else if(type == "CML"){
		$('#startDate3').val(sdate);
		$('#endDate3').val(edate);
	}
	else if(type == "PERIOD"){
		$('#startDate4').val(sdate);
		$('#endDate4').val(edate);
	}
	else if(type == "CMLA"){
		$('#startDateAll').val(sdate);
		$('#endDateAll').val(edate);
	}
}
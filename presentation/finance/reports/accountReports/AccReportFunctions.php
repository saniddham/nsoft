<?php

function oppcitAccounts($cdType, $entryNumber) {
    global $db;
    try {
        $sqlOp = "SELECT
                    (fin_transactions_details.amount * fin_transactions.currencyRate) AS am,
                    mst_financechartofaccounts.strName AS ledName,
                    fin_transactions_details.personType,
                    fin_transactions_details.personId
                FROM
                    fin_transactions
                    INNER JOIN fin_transactions_details ON fin_transactions.entryId = fin_transactions_details.entryId
                    INNER JOIN mst_financechartofaccounts ON fin_transactions_details.accountId = mst_financechartofaccounts.intId
                WHERE
                    fin_transactions_details.`credit/debit` <> '$cdType' AND
                    fin_transactions_details.entryId = '$entryNumber'";
        $resultOP12 = $db->RunQuery($sqlOp);
        $arr = array();
        while ($rowOp = mysqli_fetch_array($resultOP12)) {
            $name=getPersonNameForOppAc($rowOp['personType'],$rowOp['personId']);
            $s = $rowOp['ledName'] . ' - ' . $rowOp['am'].' - '.$name;
            $arr[] = $s;
        }
    } catch (Execption $e) {
        echo $e->getMessage();
        $arr[] = $e->getMessage();
        return $arr;
    }
    return $arr;
}
function oppcitAccountsMulty($cdType, $entryNumber) {
    global $db;
    try {
        $sqlOp = "SELECT
                    (fin_transactions_details.amount) AS am,
                    mst_financechartofaccounts.strName AS ledName,
                    fin_transactions_details.personType,
                    fin_transactions_details.personId
                FROM
                    fin_transactions
                    INNER JOIN fin_transactions_details ON fin_transactions.entryId = fin_transactions_details.entryId
                    INNER JOIN mst_financechartofaccounts ON fin_transactions_details.accountId = mst_financechartofaccounts.intId
                WHERE
                    fin_transactions_details.`credit/debit` <> '$cdType' AND
                    fin_transactions_details.entryId = '$entryNumber'";
        $resultOP12 = $db->RunQuery($sqlOp);
        $arr = array();
        while ($rowOp = mysqli_fetch_array($resultOP12)) {
            $name=getPersonNameForOppAc($rowOp['personType'],$rowOp['personId']);
            $s = $rowOp['ledName'] . ' - ' . $rowOp['am'].' - '.$name;
            $arr[] = $s;
        }
    } catch (Execption $e) {
        echo $e->getMessage();
        $arr[] = $e->getMessage();
        return $arr;
    }
    return $arr;
}

//get Customer or sublier name
function getPersonNameForOppAc($personType,$personId){
    //$stakeHolderType="supplier";$PearentHolder=9;
    global $db;
    if($personType=="sup"){
        $sql="SELECT mst_supplier.strName AS name FROM mst_supplier WHERE mst_supplier.intId = $personId";
    }
	else if($personType=="osup"){
        $sql="SELECT mst_finance_service_supplier.strName AS name FROM mst_finance_service_supplier WHERE mst_finance_service_supplier.intId = $personId";
    }
    else if($personType=="cus"){
        $sql="SELECT mst_customer.strName AS name FROM mst_customer WHERE mst_customer.intId = $personId";
    }
	else if($personType=="ocus"){
        $sql="SELECT mst_finance_service_customer.strName AS name FROM mst_finance_service_customer WHERE mst_finance_service_customer.intId = $personId";
    }
    else $sql=null;
    
    $name="-";
    if($sql!=null){
        $result = $db->RunQuery($sql);
        $row = mysqli_fetch_array($result);
        $name=$row['name'];
    }
    return $name;
}

?>

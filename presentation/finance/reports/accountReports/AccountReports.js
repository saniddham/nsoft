$(document).ready(function() {
    //Show ledger LKR report
    $('#frmLedgerLKR #butReport1').click(function(){
        data="ledgerAcc=" + $('#frmLedgerLKR #cmbLedgerAcc').val();
        data+="&startDate=" + $('#frmLedgerLKR #startDate').val();
        data+="&endDate=" + $('#frmLedgerLKR #endDate').val();
        data+="&accPeriod=" + $('#cmbAccPeriod').val();
        
        window.open('GeneralLedger_LKR_Rpt.php?'+data);
    });
    //Show ledger LKR filter
    $('#frmLedgerLKR #butReportFilter').click(function(){
        data="ledgerHeaderAcc=" + $('#frmLedgerLKR #cmbLedgerHeaderAcc').val();
        data+="&startDate=" + $('#frmLedgerLKR #startDate').val();
        data+="&endDate=" + $('#frmLedgerLKR #endDate').val();
        data+="&accPeriod=" + $('#cmbAccPeriod').val(); 

        window.open('GeneralLedger_LKR_Filter.php?'+data);
    });
    //Show ledger LKR filter Report
    $('#frmLedFilter #butReport').click(function(){        
        data="&startDate=" + $('#frmLedFilter #startDate').val();
        data+="&endDate=" + $('#frmLedFilter #endDate').val();
        data+="&accPeriod=" + $('#frmLedFilter #accPeriod').val();
        var accounts=document.getElementsByName("chkAcc");
        var accIds=new Array();
        var x=0;
        for(var i=0;i<accounts.length;++i ){
            if(accounts[i].checked){
                accIds[x++]=accounts[i].value;    
            }             
        }        
        data+="&accIds=" + accIds;       

        window.open('GeneralLedger_LKR_FilterReport.php?'+data);
    });
    //Show ledger MultyCurrency report
    $('#frmLedgerMultiCurrency #butReport2').click(function(){
        data="ledgerAcc=" + $('#frmLedgerMultiCurrency #cmbLedgerAcc').val();
        data+="&startDate=" + $('#frmLedgerMultiCurrency #startDate1').val();
        data+="&endDate=" + $('#frmLedgerMultiCurrency #endDate1').val();
        data+="&currencyId=" + $('#frmLedgerMultiCurrency #cboCurrency').val();
        data+="&accPeriod=" + $('#cmbAccPeriod').val();
        
        window.open('GeneralLedger_Multy_Rpt.php?'+data);
    });
    
    //Show ledger LKR report with Selected Fiels
    $('#frmLedgerLKRSelected #butReportSelectedLedger').click(function(){       
        var data="ledgerAcc=" + $('#frmLedgerLKRSelected #cmbLedgerAcc').val();
        data+="&startDate=" + $('#frmLedgerLKRSelected #startDateSelect').val();
        data+="&endDate=" + $('#frmLedgerLKRSelected #endDateSelect').val();
        data+="&accPeriod=" + $('#cmbAccPeriod').val();   
        
        var chkSelect=document.getElementsByName("chkSelect");
        for(var i=0;i<chkSelect.length;++i ){
            if(chkSelect[i].checked){
                var val=chkSelect[i].value
                data+="&fileds["+val+"]="+true;    
            }             
        }
        
        var chkAccounts=document.getElementsByName("chkAccLKR");
        var x=0;
        for(var i=0;i<chkAccounts.length;++i ){
            if(chkAccounts[i].checked){
                var val=chkAccounts[i].value
                data+="&accounts["+ x++ +"]="+val;    
            }             
        }
        
        window.open('GeneralLedgerSelected_LKR_Rpt.php?'+data);
    });
    
    //Show ledger Multy Currency report with Selected Fiels
    $('#frmLedgerMLTSelected #butReportSelectedLedger').click(function(){ 
        var data="startDate=" + $('#frmLedgerMLTSelected #startDateSelectMLT').val();
        data+="&endDate=" + $('#frmLedgerMLTSelected #endDateSelectMlt').val();
        data+="&currencyId=" + $('#frmLedgerMLTSelected #cboCurrency').val();
        data+="&accPeriod=" + $('#cmbAccPeriod').val();   
        
        var chkSelect=document.getElementsByName("chkSelectMLT");
        for(var i=0;i<chkSelect.length;++i ){
            if(chkSelect[i].checked){
                var val=chkSelect[i].value
                data+="&fileds["+val+"]="+true;    
            }             
        }
        
        var chkAccounts=document.getElementsByName("chkAccMLT");
        var x=0;
        for(var i=0;i<chkAccounts.length;++i ){
            if(chkAccounts[i].checked){
                var val=chkAccounts[i].value
                data+="&accounts["+ x++ +"]="+val;    
            }             
        }
        
        window.open('GeneralLedgerSelected_MultyCurrency_Rpt.php?'+data);
    });
    
    //Show Trial Balance - Cumulative report
    $('#frmTBCumu #butReport3').click(function(){
        data="startDate=" + $('#frmTBCumu #startDate3').val();
        data+="&endDate=" + $('#frmTBCumu #endDate3').val();
        data+="&accPeriod=" + $('#cmbAccPeriod').val();
        
        window.open('TrialBalance _Cumulative_Rpt.php?'+data);
    });
    //Show Trial Balance - Period report
    $('#frmTBCPeriod #butReport4').click(function(){
        data="startDate=" + $('#frmTBCPeriod #startDate4').val();
        data+="&endDate=" + $('#frmTBCPeriod #endDate4').val();
        data+="&accPeriod=" + $('#cmbAccPeriod').val();
        
        window.open('TrialBalance _Period_Rpt.php?'+data);
    });
    
    $('#frmTBCumuAllCompany #butReport3').click(function(){
        data="startDate=" + $('#frmTBCumuAllCompany #startDateAll').val();
        data+="&endDate=" + $('#frmTBCumuAllCompany #endDateAll').val();
        data+="&accPeriod=" + $('#cmbAccPeriod').val();
        
        window.open('TrialBalance _Cumulative_AllCompany_Rpt.php?'+data);
    });
    //======================Check Alll===============
    $('#frmLedgerLKRSelected #chkAllAccLKR').click(function(){  
        
        var chkAll=document.getElementById("chkAllAccLKR");
        var status=false;
        if(chkAll.checked){
            status=true;
        }
        else{
            status=false;
        }
        
        $('#frmLedgerLKRSelected input[name=chkAccLKR]').attr("checked",status);		
    });
    
    $('#frmLedgerMLTSelected #chkAllAccMLT').click(function(){  
        
        var chkAll=document.getElementById("chkAllAccMLT");
        var status=false;
        if(chkAll.checked){
            status=true;
        }
        else{
            status=false;
        }
        
        $('#frmLedgerMLTSelected input[name=chkAccMLT]').attr("checked",status);		
    });
    ///=====================================
});

function toggle(toggleText,displayText) {
	var ele = document.getElementById(toggleText);
	var text = document.getElementById(displayText);
	if(ele.style.display == "block") {
    		ele.style.display = "none";
		text.innerHTML = "show";
  	}
	else {
		ele.style.display = "block";
		text.innerHTML = "hide";
	}
}




<?php
session_start();
$backwardseperator = "../../../../";
$companyId = $_SESSION['headCompanyId'];
$location = $_SESSION['CompanyID'];
$intUser = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath = $_SERVER['PHP_SELF'];
include "{$backwardseperator}dataAccess/Connector.php";

$locationId = $location; //this locationId use in report header(reportHeader.php)--------------------

$startDate= $_REQUEST['startDate'];
$endDate = $_REQUEST['endDate'];
$accPeriod = $_REQUEST['accPeriod'];
$ct=0;//Row Counter
$resultCom = $db->RunQuery("SELECT c.intId,c.strName FROM mst_companies AS c WHERE c.intStatus =  '1'");
$x=0;
while($rowCom=mysqli_fetch_array($resultCom)){
    $coms[$x]['id']=$rowCom['intId'];
    $coms[$x]['name']=$rowCom['strName'];
    ++$x;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Trial Balance - Cumulative - All Company</title>
        <link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
        <script type="application/javascript" src="DrillAccount.js"></script>
        <style>
            table.rptTable{
                width: 100%;
                border-collapse:collapse;
            }
            table.rptTable tr td{
                border:1px solid black;
            }
            .rptTblHeader {
                font-size: 12px;
                text-align: center;
                font-weight:bold;
                background-color: #e3ab20;
            }
            .rptTblBody {
                font-size: 12px;
            }
            .rptTblfirstHeader {
                font-size: 12px;
                text-align: center;
            }
            .drillLink{
                cursor:pointer;
            }
            a:hover
            { 
                font-weight: bold;
                text-decoration:underline;
                color: #0000FF;
            }
            .rptTbltotal {
                font-size: 12px;
                text-align: right;
                font-weight:bold;
                background-color: #e3ab20;
            }
            .com1{
                background-color:#bab9b9;
                text-align: right;
            }
            .com2{
                background-color:#d5d5d5;
                text-align: right;
            }
            .total{
                background-color:#e5c678;
                text-align: right;
            }
            .normal{
                background-color: #ffffff;
            }
            .selected{
                background-color: #00CCFF;
            }
        </style>
    </head>
    <body>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="3"></td>
            </tr>
            <tr>
                <td width="20%"></td>
                <td width="60%" height="80" valign="top"><?php include '../../reportHeader.php' ?></td>
                <td width="20%"></td>
            </tr>
            <tr>
                <td colspan="3"></td>
            </tr>
        </table>
        <div align="center">
            <div style="background-color:#FFF" ><strong>Trial Balance - Cumulative - All Company</strong><strong></strong></div>
            <div style="background-color:#FFF" ><strong>From</strong>&nbsp;&nbsp;&nbsp;<i><?php echo $startDate ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</i><strong>To</strong>&nbsp;&nbsp;&nbsp;<i><?php echo $endDate ?></i><strong></strong></div><br/>
            <table width="1100" border="0" align="center" bgcolor="#FFFFFF">
                <tr>
                    <td><samp style="font-size: 10px;font-weight:bold;">All amounts in LKR</samp></td>
                </tr>  
                <tr>
                    <td>
                        <table align="center" class="rptTable">
                            <tr class="rptTblfirstHeader" >
                                <td colspan="3"></td>
                                <?php
                                    foreach($coms as $com){
                                ?>
                                <td colspan="2"><?php echo $com['name'] ?></td>
                                <?php } ?>
                                <td colspan="2">Total</td>
                            </tr> 
                            <tr class="rptTblHeader">
                                <td></td>
                                <td>Type</td>
                                <td>Code</td>
                                <?php
                                    foreach($coms as $com){
                                ?>
                                <td>Debit</td>
                                <td>Credit</td>
                                <?php } ?>
                                <td>Debit</td>
                                <td>Credit</td>
                            </tr>
                            <?php
                                $sql="SELECT
                                    mst_financechartofaccounts.intId,
                                    mst_financechartofaccounts.strCode,
                                    mst_financechartofaccounts.strName,
                                    mst_financechartofaccounts.strType,
                                    mst_financialmaintype.intId AS mainTypeID
                                    FROM
                                    mst_financechartofaccounts
                                    Inner Join mst_financialsubtype ON mst_financechartofaccounts.intFinancialTypeId = mst_financialsubtype.intId
                                    Inner Join mst_financialmaintype ON mst_financialsubtype.intFinancialMainTypeID = mst_financialmaintype.intId
                                    WHERE
                                    mst_financechartofaccounts.strType =  'Posting'
                                    ORDER BY
                                    mst_financechartofaccounts.strCode ASC";
                                $resultLedg = $db->RunQuery($sql);
                                 while($rowLedg=mysqli_fetch_array($resultLedg)){
                                    $ledgerAcc=$rowLedg['intId'];
                                    $accPeriodSql="";
                                    //get account period value for ' Manufacturing ' and 'Income Statements'
                                    if($rowLedg['mainTypeID']==1 | $rowLedg['mainTypeID']==2){
                                        $accPeriodSql=" AND fin_transactions.intAccPeriod='$accPeriod'";
                                    }
                                    else{// skip accounting period for 'Balance Sheet '
                                        $accPeriodSql="";
                                    }
                            ?>
                            <tr class="rptTblBody" onmouseover="select('mask')" onmouseout="normal('mask')"><div id="mask">
                                        <td><?php echo ++$ct ?></td>
                                        <td><?php echo $rowLedg['strName'] ?></td>
                                        <td><?php echo $rowLedg['strCode'] ?></td>
                                        <?php
                                        $rowCreditTotal=0;
                                        $rowDebitTotal=0;
                                        $y=0;
                                        foreach($coms as $com){
                                            $balance=0;
                                            $theCompanyId=$com['id'];
                                            //get Debit total amount                            
                                            $resultDebitTot = $db->RunQuery("SELECT
                                                                sum(fin_transactions_details.amount * fin_transactions.currencyRate) AS debitAmount
                                                            FROM
                                                                fin_transactions
                                                                INNER JOIN fin_transactions_details ON fin_transactions.entryId = fin_transactions_details.entryId
                                                            WHERE
                                                                fin_transactions_details.accountId = $ledgerAcc AND
                                                                fin_transactions_details.`credit/debit` = 'D' AND
                                                                fin_transactions.entryDate <= '$endDate' AND
                                                                fin_transactions.authorized = 1 AND
                                                                fin_transactions.delStatus = 0 AND
                                                                fin_transactions.companyId =$theCompanyId".$accPeriodSql);                            
                                            $rowDebitTot=mysqli_fetch_array($resultDebitTot);
                                            //get Credit total amount
                                            $resultCreditTot = $db->RunQuery("SELECT
                                                                sum(fin_transactions_details.amount * fin_transactions.currencyRate) AS creditAmount
                                                                FROM
                                                                    fin_transactions
                                                                    INNER JOIN fin_transactions_details ON fin_transactions.entryId = fin_transactions_details.entryId
                                                                WHERE
                                                                    fin_transactions_details.accountId = $ledgerAcc AND
                                                                    fin_transactions_details.`credit/debit` = 'C' AND
                                                                    fin_transactions.entryDate <= '$endDate' AND
                                                                    fin_transactions.authorized = 1 AND
                                                                    fin_transactions.delStatus = 0 AND
                                                                    fin_transactions.companyId = $theCompanyId".$accPeriodSql);
                                        $rowCreditTot=mysqli_fetch_array($resultCreditTot);
                                        //caiculate balance
                                        $balance=$rowDebitTot['debitAmount'] - $rowCreditTot['creditAmount'];

                                        $debitBalance=0;
                                        $creditBalance=0;
                                        if($balance >= 0) {
                                            $debitBalance=$balance;
                                        }
                                        else {
                                            $creditBalance=$balance * (-1);
                                        }
                                        if($y%2==0) $cla="com1";
                                        else $cla="com2";
                                        ++$y;
                                        ?>
                                        <td align="right" class="<?php echo $cla ?>"><a class="drillLink" onclick="trialDrilAll('<?php echo$startDate?>','<?php echo$endDate?>','<?php echo$ledgerAcc?>','<?php echo$accPeriod?>','<?php echo $theCompanyId?>')"><?php echo  ($debitBalance!=0?number_format($debitBalance,4):'') ?></a></td>
                                        <td align="right" class="<?php echo $cla ?>"><a class="drillLink" onclick="trialDrilAll('<?php echo$startDate?>','<?php echo$endDate?>','<?php echo$ledgerAcc?>','<?php echo$accPeriod?>','<?php echo $theCompanyId?>')"><?php echo  ($creditBalance!=0?number_format($creditBalance,4):'')?></a></td>
                            
                                        <?php
                                            $rowCreditTotal+=$creditBalance;
                                            $rowDebitTotal+=$debitBalance;
                                            
                                            $comCreditTotal[$theCompanyId]+=$creditBalance;
                                            $comDebitTotal[$theCompanyId]+=$debitBalance;
                                        }
                                        ?>
                                        <td class="total"><a class="drillLink" onclick="trialDrilAllCompany('<?php echo$startDate?>','<?php echo$endDate?>','<?php echo$ledgerAcc?>','<?php echo$accPeriod?>')"><?php echo ($rowDebitTotal!=0?number_format($rowDebitTotal,4):'')?></a></td>
                                        <td class="total"><a class="drillLink" onclick="trialDrilAllCompany('<?php echo$startDate?>','<?php echo$endDate?>','<?php echo$ledgerAcc?>','<?php echo$accPeriod?>')"><?php echo ($rowCreditTotal!=0?number_format($rowCreditTotal,4):'')?></a></td>
                            <?php
                                 }
                            ?>
                                        </div></tr>
                            <tr class="rptTbltotal">
                                <td colspan="3">Total</td>                                
                                <?php
                                    $totalCredit=0;
                                    $totalDebit=0;
                                    foreach($coms as $com){
                                        $theCompanyId=$com['id'];
                                        
                                        $totalCredit+=$comCreditTotal[$theCompanyId];
                                        $totalDebit+=$comDebitTotal[$theCompanyId];
                                        
                                ?>
                                    <td><?php echo  number_format($comDebitTotal[$theCompanyId],4) ?></td>
                                    <td><?php echo  number_format($comCreditTotal[$theCompanyId],4) ?></td>
                                <?php } ?>
                                <td align="right"><?php echo  number_format($totalDebit,4) ?></td>
                                <td align="right"><?php echo  number_format($totalCredit,4) ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
function leadgerDrill(docType,docNumber){   
    var url;    
    var path="/nsoft/presentation/finance/";
    if(docType=="Sales Invoice"){
        url=path+"customer/salesInvoice/salesInvoice.php?id="+docNumber;
    }
	else if(docType=="Debit Note Invoice"){
        url=path+"customer/debitNoteInvoice/debitNoteInvoice.php?id="+docNumber;
    }
    else if(docType=="Received Payments"){
        url=path+"customer/receivedPayments/receivedPayments.php?id="+docNumber;
    } 
    else if(docType=="Customer Gain-Loss"){
        url=path+"customer/customerGainLoss/customerGainLoss.php?id="+docNumber;
    } 
    else if(docType=="Credit Note"){
        url=path+"customer/creditNote/creditNote.php?id="+docNumber;
    } 
    else if(docType=="Petty Cash"){
        url=path+"bank/pettyCash/pettyCash.php?pettyCashNo="+docNumber;
    } 
    else if(docType=="Bank Deposit"){
        url=path+"bank/deposit/deposit.php?depositNo="+docNumber;
    } 
    else if(docType=="Bank Payment"){
        url=path+"bank/payments/payments.php?paymentNo="+docNumber;
    } 
    else if(docType=="Advance Payment"){
        url=path+"supplier/advancePayments/advancePayments.php?receiptNo="+docNumber;
    } 
    else if(docType=="Purchase Invoice"){
        url=path+"supplier/purchaseInvoice/purchaseInvoice.php?id="+docNumber;
    } 
    else if(docType=="Supplier Payments"){
        url=path+"supplier/supplierPayments/supplierPayments.php?id="+docNumber;
    } 
    else if(docType=="Cash Sales"){
        url=path+"customer/cashSales/cashSales.php?id="+docNumber;
    } 
    else if(docType=="Bill Payment"){/*/*/
        url=path+"salesInvoice/salesInvoice.php?id="+docNumber;
    }     
    else if(docType=="Supplier Gain-Loss"){
        url=path+"supplier/supplierGainLoss/supplierGainLoss.php?id="+docNumber;
    }   
    else if(docType=="Advance Received"){
        url=path+"customer/advanceReceived/advanceReceived.php?receiptNo="+docNumber;
    } 
    else if(docType=="Debit Note"){
        url=path+"supplier/debitNote/debitNote.php?debitNo="+docNumber;
    }    
    else if(docType=="Journal Entry"){
        url=path+"accountant/journalEntry/journalEntry.php?strReferenceNo="+docNumber;
    }
	//=============Other Payable==============
	else if(docType=="Other Bill"){
         url=path+"otherPayable/otherBill/otherBill.php?id="+docNumber;
    }
	else if(docType=="Other Bill Payments"){
         url=path+"otherPayable/otherBillPayments/otherBillPayments.php?id="+docNumber;
    }
	else if(docType=="Other Advance Payment"){
        url=path+"otherPayable/advancePayments/advancePayments.php?receiptNo="+docNumber;
    } 
	else if(docType=="Other Debit Note"){
         url=path+"otherPayable/debitNote/debitNote.php?debitNo="+docNumber;
    }
	else if(docType=="Other Gain-Loss"){
         url=path+"otherPayable/gainLoss/gainLoss.php?id="+docNumber;
    }
	//========================================   
	
	//=============Other Receivable==============
	else if(docType=="Other Invoice"){
        url=path+"otherReceivable/otherInvoice/otherInvoice.php?id="+docNumber;
    }
	else if(docType=="Other Receivable Payments"){
        url=path+"otherReceivable/receivedPayments/receivedPayments.php?id="+docNumber;
    }
	else if(docType=="Other Receivable Gain-Loss"){
        url=path+"otherReceivable/gainLoss/gainLoss.php?id="+docNumber;
    }
	 else if(docType=="Other Advance Received"){
        url=path+"otherReceivable/advanceReceived/advanceReceived.php?receiptNo="+docNumber;
    }
	 else if(docType=="Other Credit Note"){
        url=path+"otherReceivable/creditNote/creditNote.php?creditNo="+docNumber;
    } 
	//==========================================
	
	//++++++++++++++++++++++++++++++++++++++++++
	 else if(docType=="Funds Transfer"){
        url=path+"bank/fundsTransfer/fundsTransfer.php?fundTransafNo="+docNumber;
    } 
	//++++++++++++++++++++++++++++++++++++++++++
    
   window.open(url);
}

function trialDril(startDate,endDate,ledgerAcc,accPeriod){
    var data="ledgerAcc=" + ledgerAcc;
    data+="&startDate=" + startDate;
    data+="&endDate=" + endDate;
    data+="&accPeriod="+accPeriod;    
    window.open("GeneralLedger_LKR_Rpt.php?"+data);
}
function trialDrilAll(startDate,endDate,ledgerAcc,accPeriod,companyId){
    var data="ledgerAcc=" + ledgerAcc;
    data+="&startDate=" + startDate;
    data+="&endDate=" + endDate;
    data+="&accPeriod="+accPeriod;
    data+="&comId="+companyId;
    window.open("GeneralLedger_LKR_Rpt.php?"+data);
}

function trialDrilAllCompany(startDate,endDate,ledgerAcc,accPeriod){
    var data="ledgerAcc=" + ledgerAcc;
    data+="&startDate=" + startDate;
    data+="&endDate=" + endDate;
    data+="&accPeriod="+accPeriod;    
    window.open("GeneralLedger_LKR_AllCompanyRpt.php?"+data);
}
function select(sender){    
   var div=document.getElementById(sender);  
    div.setAttribute("class", "selected");
}
function normal(sender){
    var div=document.getElementById(sender);
    div.setAttribute("class", "normal");
}


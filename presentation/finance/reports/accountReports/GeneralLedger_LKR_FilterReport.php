<?php
session_start();
$backwardseperator = "../../../../";
$companyId = $_SESSION['headCompanyId'];
$location = $_SESSION['CompanyID'];
$intUser = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath = $_SERVER['PHP_SELF'];
include "{$backwardseperator}dataAccess/Connector.php";

include "AccReportFunctions.php";

$locationId = $location; //this locationId use in report header(reportHeader.php)--------------------

$ids= $_REQUEST['accIds'];
$accIds = explode(',',$ids);
$startDate= $_REQUEST['startDate'];
$endDate = $_REQUEST['endDate'];
$accPeriod = $_REQUEST['accPeriod'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>General Ledger - LKR</title>
        <link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
        <script type="application/javascript" src="DrillAccount.js"></script>
        <style>
            table.rptTable{
                width: 100%;
                border-collapse:collapse;
            }
            table.rptTable tr td{
                border:1px solid black;
            }
            .rptTblHeader {
                font-size: 11px;
                text-align: center;
                font-weight:bold;
            }
            .rptTblBody {
                font-size: 10px;
            }
            .drillLink{
                cursor:pointer;
            }
            a:hover
            { 
                font-weight: bold;
                text-decoration:underline;
                color: #0000FF;
            }
            
        </style>
    </head>
    <body>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="3"></td>
            </tr>
            <tr>
                <td width="20%"></td>
                <td width="60%" height="80" valign="top"><?php include '../../reportHeader.php' ?></td>
                <td width="20%"></td>
            </tr>
            <tr>
                <td colspan="3"></td>
            </tr>
        </table>
        <div align="center">
            <div style="background-color:#FFF" ><strong>General Ledger - LKR</strong><strong></strong></div>
            <div style="background-color:#FFF" ><strong>From</strong>&nbsp;&nbsp;&nbsp;<i><?php echo $startDate ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</i><strong>To</strong>&nbsp;&nbsp;&nbsp;<i><?php echo $endDate ?></i><strong></strong></div><br/>
            
            <table width="1100" border="0" align="center" bgcolor="#FFFFFF">
                <tr>
                    <td><samp style="font-size: 10px;font-weight:bold;">All amounts in LKR</samp></td>
                </tr>
                
                <?php
                
                    foreach ($accIds as &$ledgerAcc){
                        //get Ledger Account Details
                        $resultLedg = $db->RunQuery("SELECT
                                                        mst_financialsubtype.intId,
                                                        mst_financialsubtype.strType,
                                                        mst_financechartofaccounts.strCode,
                                                        mst_financechartofaccounts.strName,
                                                        mst_financialmaintype.intId as mainTypeID
                                                    FROM
                                                        mst_financechartofaccounts
                                                        Inner Join mst_financialsubtype ON mst_financechartofaccounts.intFinancialTypeId = mst_financialsubtype.intId
                                                        Inner Join mst_financialmaintype ON mst_financialsubtype.intFinancialMainTypeID = mst_financialmaintype.intId
                                                    WHERE
                                                        mst_financechartofaccounts.intId =".$ledgerAcc);
                        $rowLedg=mysqli_fetch_array($resultLedg);
                        $accPeriodSql="";
                        //get account period value for ' Manufacturing ' and 'Income Statements'
                        if($rowLedg['mainTypeID']==1 | $rowLedg['mainTypeID']==2){
                            $accPeriodSql="AND fin_transactions.intAccPeriod='$accPeriod'";
                        }
                        else{// skip accounting period for 'Balance Sheet '
                            $accPeriodSql="";
                        }

                        //get Data And calculate opening Balance
                        $resultDebitTot = $db->RunQuery("SELECT
                                                                SUM(fin_transactions_details.amount * fin_transactions.currencyRate) AS debitAmount
                                                            FROM
                                                                fin_transactions
                                                                INNER JOIN fin_transactions_details ON fin_transactions.entryId = fin_transactions_details.entryId
                                                            WHERE
                                                                fin_transactions_details.accountId = $ledgerAcc AND
                                                                fin_transactions_details.`credit/debit` = 'D' AND
                                                                fin_transactions.entryDate < '$startDate' AND
                                                                fin_transactions.authorized = 1 AND
                                                                fin_transactions.delStatus = 0 AND
                                                                fin_transactions.companyId = $companyId".$accPeriodSq);
                        $rowDebitTot=mysqli_fetch_array($resultDebitTot);

                        $resultCreditTot = $db->RunQuery("SELECT
                                                                SUM(fin_transactions_details.amount * fin_transactions.currencyRate) AS creditAmount
                                                                FROM
                                                                    fin_transactions
                                                                    INNER JOIN fin_transactions_details ON fin_transactions.entryId = fin_transactions_details.entryId
                                                                WHERE
                                                                    fin_transactions_details.accountId = $ledgerAcc AND
                                                                    fin_transactions_details.`credit/debit` = 'C' AND
                                                                    fin_transactions.entryDate < '$startDate' AND
                                                                    fin_transactions.authorized = 1 AND
                                                                    fin_transactions.delStatus = 0 AND
                                                                    fin_transactions.companyId = $companyId".$accPeriodSq);
                        $rowCreditTot=mysqli_fetch_array($resultCreditTot);
                        $openingBalance=0;
                        if ($rowLedg['strType']=='D'){
                            $openingBalance=($rowDebitTot['debitAmount']-$rowCreditTot['creditAmount']);
                        }else{
                            $openingBalance=($rowCreditTot['creditAmount'] - $rowDebitTot['debitAmount']);
                        }
                        $ct=0;//Row Counter
                ?>
                <tr>
                    <td align="center"><div style="background-color:#FFF" ><strong>Ledger Account</strong>&nbsp;&nbsp;&nbsp;<i><?php echo $rowLedg['strCode'] ?> - <?php echo $rowLedg['strName'] ?></i></div></td>
                </tr>
                <tr>
                    <td align="right"><span class="rptTblHeader">Opening Balance :</span>&nbsp;&nbsp;<span class="rptTblHeader" style="text-align:right;border:solid; border-width: 1px"><?php echo number_format($openingBalance,4) ?></span></td>
                </tr>
                <tr>
                    <td>
                        <table align="center" class="rptTable">
                            <tr class="rptTblHeader">
                                <td></td>
                                <td>Type</td>
                                <td>Date</td>
                                <td>Number</td>
                                <td>Payment Type</td>
                                <td>Payment Number</td>
                                <td>Transaction Details</td>
                                <td>Cost Center</td>
                                <td>Account</td>
                                <td>Debit Amount</td>
                                <td>Credit Amount</td>
                                <td>Closing Balance</td>
                            </tr>
                            <?php
                            $sqlDetalis="SELECT
                                            fin_transactions.entryDate AS dtDate,
                                            fin_transactions.strProgramType,
                                            fin_transactions_details.`credit/debit` AS `strCredit/Debit`,
                                            fin_transactions.documentNo AS strDocumentNo,
                                            (fin_transactions_details.amount * fin_transactions.currencyRate) AS amount,
                                            fin_transactions_details.details AS srtTransDetails,
                                            fin_transactions.paymentNumber,
                                            mst_financepaymentsmethods.strName AS payMethod,
                                            mst_financedimension.strName AS dimName,
                                            fin_transactions.entryId
                                        FROM
                                            fin_transactions
                                            LEFT OUTER JOIN fin_transactions_details ON fin_transactions.entryId = fin_transactions_details.entryId
                                            LEFT OUTER JOIN mst_financepaymentsmethods ON fin_transactions.payMethodId = mst_financepaymentsmethods.intId
                                            LEFT OUTER JOIN mst_financedimension ON fin_transactions_details.dimensionId = mst_financedimension.intId
                                        WHERE
                                            fin_transactions_details.accountId = $ledgerAcc AND
                                            fin_transactions.entryDate >= '$startDate' AND
                                            fin_transactions.entryDate <= '$endDate' AND
                                            fin_transactions.companyId = $companyId AND
                                            fin_transactions.authorized = 1 AND
                                            fin_transactions.delStatus = 0
                                        ORDER BY
                                            fin_transactions.entryDate ASC,
                                            fin_transactions.entryId ASC";
                            $resultdetails = $db->RunQuery($sqlDetalis);
                            $closingBalabce=$openingBalance;
                            $dbTotal=0;
                            $crTotal=0;                            
                            while($rowDetails=mysqli_fetch_array($resultdetails)){
                                $entryId=$rowDetails['entryId'];
                                $dbAmount=0;
                                $crAmount=0;
                                if($rowDetails['strCredit/Debit']=='D') $dbAmount=$rowDetails['amount'];
                                else $crAmount=$rowDetails['amount']; 
                                
                                //calculate closing balance according to the ledger type
                                if ($rowLedg['strType']=='D') $closingBalabce+=($dbAmount-$crAmount);
                                else $closingBalabce+=($crAmount - $dbAmount);                                
                            ?>
                            <tr class="rptTblBody">
                                <td><?php echo ++$ct ?></td>
                                <td><?php echo $rowDetails['strProgramType'] ?></td>
                                <td><?php echo $rowDetails['dtDate'] ?></td>
                                <td><a class="drillLink" onclick="leadgerDrill('<?php echo ($rowDetails['strProgramType']) ?>','<?php echo $rowDetails['strDocumentNo']?>')"> <?php echo $rowDetails['strDocumentNo'] ?> </a></td>
                                <td><?php echo $rowDetails['payMethod'] ?></td>
                                <td><?php echo $rowDetails['paymentNumber'] ?></td>
                                <td><?php echo $rowDetails['srtTransDetails'] ?></td>
                                <td><?php echo ($rowDetails['dimName']==''?'N/A':$rowDetails['dimName']) ?></td>
                                <td>
                                    <?php 
                                        $arr=  oppcitAccounts($rowDetails['strCredit/Debit'],$entryId);
                                        for($i=0; $i<count($arr); ++$i){
                                            echo $i+1;
                                            echo '.)';
                                            echo $arr[$i].'<br /><br />';
                                        }
                                    ?>
                                </td>
                                <td align="right"><?php echo number_format($dbAmount,4) ?></td>
                                <td align="right"><?php echo number_format($crAmount,4)?></td>
                                <td align="right"><?php echo number_format($closingBalabce,4) ?></td>
                            </tr>
                            <?php
                                $dbTotal+=$dbAmount;
                                $crTotal+=$crAmount;
                            }
                            ?>
                            <tr class="rptTblBody">
                                <td colspan="9" align="center"><strong>Total</strong></td>
                                <td align="right"><strong>0<?php echo number_format($dbTotal,4) ?></strong></td>
                                <td align="right"><strong><?php echo number_format($crTotal,4) ?></strong></td>
                                <td align="right"><strong><?php echo number_format($closingBalabce,4) ?></strong></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr><td><br/><br/></td></tr>
                <?php
                    }
                ?>

                <tr height="40">
                    <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d H:i:s") ?></strong></span></td>
                </tr>
            </table>
        </div>
    </body>
</html>
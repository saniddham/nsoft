<?php
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$companyId = $_SESSION['headCompanyId'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Company & Financial Reports</title>
        <link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />

        <script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
        <script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
        <script type="application/javascript" src="CompanyReports.js"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

        <link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
        <script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
        <link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css" />
        <link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css" /> 

<!--<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script> -->
        <style>
            .showReport{
                font-family: Verdana;
                font-size: 11px;
                color: #0000ff;
                margin: 0px;
                text-align: center;                
                height: 25px;
                text-decoration: underline;

            }
            a:link {color: #0000ff; text-decoration: underline; }
            a:active {color: #0000ff; text-decoration: underline; }
            a:visited {color: #0000ff; text-decoration: underline; }
            a:hover {color: #ff0000; text-decoration: none; cursor: pointer; }


        </style> 
    </head>    
    <body>
        <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
                <td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator . 'Header.php'; ?></td>
            </tr> 
        </table>
        <div align="center">
            <div class="trans_layoutL">
                <div class="trans_text">Company & Financial</div> 
                <form name="frmComSummery" method="post" id="frmComSummery" autocomplete="off">
                    <table width="100%" cellspacing="0" cellpadding="5">
                        <tr>
                            <td colspan="2">
                                <div class="normalfntMid"> Account Period
                                    <select name="cmbAccPeriod" class="validate[required]" id="cmbAccPeriod" style="width:100px">                        
                                        <?php
                                        $sqlPeriod = "SELECT
                                    mst_financeaccountingperiod.intId,
                                    mst_financeaccountingperiod.dtmStartingDate,
                                    mst_financeaccountingperiod.dtmClosingDate
                                FROM
                                    mst_financeaccountingperiod
                                WHERE
                                    mst_financeaccountingperiod.intStatus =  '1'
                                ORDER BY
                                    mst_financeaccountingperiod.intId DESC";
                                        $resultPeriod = $db->RunQuery($sqlPeriod);
                                        while ($rowPeriod = mysqli_fetch_array($resultPeriod)) {
                                            echo "<option value=\"" . $rowPeriod['intId'] . "\">" . date('Y', strtotime($rowPeriod['dtmStartingDate'])) . "/" . date('Y', strtotime($rowPeriod['dtmClosingDate'])) . "</option>";
                                        }
                                        ?>
                                    </select>
                                </div> 
                            </td>
                        </tr>
                        <tr><td colspan="2"><div class="normalfntMid">Summery</div> </td></tr>
                        <tr>
                            <td colspan="2" width="50%" align="center" class="tableBorder_bottomRound">

                                <div class="showReport" id="divBal"><a>Balance Sheet</a></div>

                                <div class="showReport" id="divIncome"><a>Income Statement</a></div>

                                <div class="showReport" id="divManu"><a>Manufacturing Statement</a></div>
                                
                                <div class="showReport" id="divIncomeGroup"><a>Group Income Statement</a></div>

                            </td>                       
                        </tr>

                    </table>
                </form>
            </div>
        </div>

    </body>
</html>

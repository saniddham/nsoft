<?php
session_start();
$backwardseperator = "../../../../";
$companyId = $_SESSION['headCompanyId'];
$locationId = $_SESSION['CompanyID'];
$intUser = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath = $_SERVER['PHP_SELF'];
include "{$backwardseperator}dataAccess/Connector.php";
include 'BalanceSheetFunctions.php';
include 'BalancesheetPNLFunctions.php';

$accPeriod = $_REQUEST['accPeriod'];
//get accounting period details
$resul = $db->RunQuery("SELECT FY.dtmStartingDate,FY.dtmClosingDate FROM mst_financeaccountingperiod AS FY WHERE FY.intStatus = 1 AND FY.intId = $accPeriod");
$row = mysqli_fetch_array($resul);
$startDate = $row['dtmStartingDate'];
$endDate = $row['dtmClosingDate'];
$monthRange = getDates($startDate, $endDate);
$AssTot = array();
$equTot = array();
?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Balance Sheet</title>
        <link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
        <script type="application/javascript" src="CompanyReportDril.js"></script>
        <style>
            table.maintable{
                width: 100%;
                width: 100%;
                border-collapse:collapse;
            }
            table.maintable tr td{
                border:1px solid black;
            }
            .maintableHeader{
                font-family: Verdana;
                font-size: 12px;
                color: #000000;
                margin: 0px;
                text-align: center;
            }
            .mainHed{
                font-family: Verdana;
                font-size: 11px;
                color: #000000;
                margin: 0px;
                text-align: left;
                font-variant-caps: all-petite-caps;
                font-weight: bold;
            }
            .subHed{
                font-family: Verdana;
                font-size: 11px;
                color: #000000;
                margin: 0px;
                text-align: left;                
                font-weight:bold;

            }
            .subHed1{
                font-family: Verdana;
                font-size: 11px;
                color: #000000;
                margin: 0px;
                text-align: left;                


            }
            .figurs{
                font-family: Verdana;
                font-size: 10px;
                color: #000000;
                margin: 0px;
                text-align: right;
                cursor: pointer;
            }
            .total{
                font-family: Verdana;
                font-size: 11px;
                color: #000000;
                margin: 0px;
                text-align: right;                
                font-weight:bolder

            }
            table.subTable{
                width: 100%;
                border-collapse:collapse;
            }
            table.subTable tr td{
                /* border:1px solid black;*/
            }
        </style>
    </head>
    <body>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr><td colspan="3"></td></tr>
            <tr>
                <td width="20%"></td>
                <td width="60%" height="80" valign="top"><?php include '../../reportHeader.php' ?></td>
                <td width="20%"></td>
            </tr>
            <tr><td colspan="3"></td></tr>
            <tr><td colspan="3" style="font-size: 14px;" class="normalfntMid">Balance Sheet - <?php echo $startDate . " To " . $endDate ?></td></tr>
        </table>
        <form name="frmBs" id="frmBs">
            <div align="center"><input type="hidden" name="txtAccPrtiod" id="txtAccPrtiod" value="<?php echo $accPeriod ?>" /><input type="hidden" name="txtrpType" id="txtrpType" value="Balance" />
                <table class="maintable"><!-- main table -->
                    <tr class="maintableHeader">
                        <td>Account</td>
                        <td><?php echo $monthRange[0] ?></td>
                        <td><?php echo $monthRange[1] ?></td>
                        <td><?php echo $monthRange[2] ?></td>
                        <td><?php echo $monthRange[3] ?></td>
                        <td><?php echo $monthRange[4] ?></td>
                        <td><?php echo $monthRange[5] ?></td>
                        <td><?php echo $monthRange[6] ?></td>
                        <td><?php echo $monthRange[7] ?></td>
                        <td><?php echo $monthRange[8] ?></td>
                        <td><?php echo $monthRange[9] ?></td>
                        <td><?php echo $monthRange[10] ?></td>
                        <td><?php echo $monthRange[11] ?></td>
                    </tr>
                    <tr class="mainHed"><td colspan="13">ASSETS</td></tr>
                    <tr class="subHed"><td colspan="13">Non Current Assets</td></tr>                    

                    <?php
                    $detailsArr = getAmounts($accPeriod, 8, $companyId);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];
                        $AssTot[] = $amounts
                        ?><!--Fixed Assets 0-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <td onClick="BSDrill(1,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[0], 4) ?></td>
                            <td onClick="BSDrill(2,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[1], 4) ?></td>
                            <td onClick="BSDrill(3,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[2], 4) ?></td>
                            <td onClick="BSDrill(4,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[3], 4) ?></td>
                            <td onClick="BSDrill(5,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[4], 4) ?></td>
                            <td onClick="BSDrill(6,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[5], 4) ?></td>
                            <td onClick="BSDrill(7,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[6], 4) ?></td>
                            <td onClick="BSDrill(8,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[7], 4) ?></td>
                            <td onClick="BSDrill(9,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[8], 4) ?></td>
                            <td onClick="BSDrill(10,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[9], 4) ?></td>
                            <td onClick="BSDrill(11,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[10], 4) ?></td>
                            <td onClick="BSDrill(12,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[11], 4) ?></td>
                        </tr>
                    <?php } ?>
                    
                    <?php
                    $detailsArr = getAmounts($accPeriod, 19, $companyId);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];
                        $AssTot[] = $amounts;
						$accDip = $amounts;
						//print_r($accDip);
                        ?><!--Acc. Dep. 1-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <td onClick="BSDrill(1,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[0], 4) ?></td>
                            <td onClick="BSDrill(2,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[1], 4) ?></td>
                            <td onClick="BSDrill(3,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[2], 4) ?></td>
                            <td onClick="BSDrill(4,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[3], 4) ?></td>
                            <td onClick="BSDrill(5,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[4], 4) ?></td>
                            <td onClick="BSDrill(6,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[5], 4) ?></td>
                            <td onClick="BSDrill(7,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[6], 4) ?></td>
                            <td onClick="BSDrill(8,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[7], 4) ?></td>
                            <td onClick="BSDrill(9,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[8], 4) ?></td>
                            <td onClick="BSDrill(10,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[9], 4) ?></td>
                            <td onClick="BSDrill(11,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[10], 4) ?></td>
                            <td onClick="BSDrill(12,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[11], 4) ?></td>
                        </tr>
<?php } ?>

                    
                    <?php
                    $detailsArr = getAmounts($accPeriod, 9, $companyId);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];
                        $AssTot[] = $amounts
                        ?><!--Other Non - Current Assets 2-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <td onClick="BSDrill(1,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[0], 4) ?></td>
                            <td onClick="BSDrill(2,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[1], 4) ?></td>
                            <td onClick="BSDrill(3,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[2], 4) ?></td>
                            <td onClick="BSDrill(4,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[3], 4) ?></td>
                            <td onClick="BSDrill(5,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[4], 4) ?></td>
                            <td onClick="BSDrill(6,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[5], 4) ?></td>
                            <td onClick="BSDrill(7,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[6], 4) ?></td>
                            <td onClick="BSDrill(8,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[7], 4) ?></td>
                            <td onClick="BSDrill(9,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[8], 4) ?></td>
                            <td onClick="BSDrill(10,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[9], 4) ?></td>
                            <td onClick="BSDrill(11,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[10], 4) ?></td>
                            <td onClick="BSDrill(12,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[11], 4) ?></td>
                        </tr>
<?php } ?>

                    <tr class="subHed"><td colspan="13">Current Assets</td></tr>

                    
                    <?php
                    $detailsArr = getAmounts($accPeriod, 20, $companyId);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];
                        $AssTot[] = $amounts
                        ?><!-- 	Inventories 3-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <td onClick="BSDrill(1,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[0], 4) ?></td>
                            <td onClick="BSDrill(2,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[1], 4) ?></td>
                            <td onClick="BSDrill(3,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[2], 4) ?></td>
                            <td onClick="BSDrill(4,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[3], 4) ?></td>
                            <td onClick="BSDrill(5,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[4], 4) ?></td>
                            <td onClick="BSDrill(6,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[5], 4) ?></td>
                            <td onClick="BSDrill(7,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[6], 4) ?></td>
                            <td onClick="BSDrill(8,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[7], 4) ?></td>
                            <td onClick="BSDrill(9,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[8], 4) ?></td>
                            <td onClick="BSDrill(10,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[9], 4) ?></td>
                            <td onClick="BSDrill(11,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[10], 4) ?></td>
                            <td onClick="BSDrill(12,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[11], 4) ?></td>
                        </tr>
                    <?php } ?>

                    
                    <?php
                    $detailsArr = getAmounts($accPeriod, 10, $companyId);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];
                        $AssTot[] = $amounts
                        ?><!-- 	Inventories 3-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <td onClick="BSDrill(1,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[0], 4) ?></td>
                            <td onClick="BSDrill(2,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[1], 4) ?></td>
                            <td onClick="BSDrill(3,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[2], 4) ?></td>
                            <td onClick="BSDrill(4,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[3], 4) ?></td>
                            <td onClick="BSDrill(5,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[4], 4) ?></td>
                            <td onClick="BSDrill(6,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[5], 4) ?></td>
                            <td onClick="BSDrill(7,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[6], 4) ?></td>
                            <td onClick="BSDrill(8,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[7], 4) ?></td>
                            <td onClick="BSDrill(9,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[8], 4) ?></td>
                            <td onClick="BSDrill(10,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[9], 4) ?></td>
                            <td onClick="BSDrill(11,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[10], 4) ?></td>
                            <td onClick="BSDrill(12,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[11], 4) ?></td>
                        </tr>
                    <?php } ?>

                    
                    <?php
                    $detailsArr = getAmounts($accPeriod, 21, $companyId);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];
                        $AssTot[] = $amounts
                        ?><!-- 	Inventories 3-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <td onClick="BSDrill(1,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[0], 4) ?></td>
                            <td onClick="BSDrill(2,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[1], 4) ?></td>
                            <td onClick="BSDrill(3,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[2], 4) ?></td>
                            <td onClick="BSDrill(4,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[3], 4) ?></td>
                            <td onClick="BSDrill(5,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[4], 4) ?></td>
                            <td onClick="BSDrill(6,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[5], 4) ?></td>
                            <td onClick="BSDrill(7,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[6], 4) ?></td>
                            <td onClick="BSDrill(8,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[7], 4) ?></td>
                            <td onClick="BSDrill(9,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[8], 4) ?></td>
                            <td onClick="BSDrill(10,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[9], 4) ?></td>
                            <td onClick="BSDrill(11,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[10], 4) ?></td>
                            <td onClick="BSDrill(12,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[11], 4) ?></td>
                        </tr>
                    <?php } ?>

                    
                    <?php
                    $detailsArr = getAmounts($accPeriod, 11, $companyId);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];
                        $AssTot[] = $amounts
                        ?><!-- 	Inventories 3-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <td onClick="BSDrill(1,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[0], 4) ?></td>
                            <td onClick="BSDrill(2,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[1], 4) ?></td>
                            <td onClick="BSDrill(3,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[2], 4) ?></td>
                            <td onClick="BSDrill(4,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[3], 4) ?></td>
                            <td onClick="BSDrill(5,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[4], 4) ?></td>
                            <td onClick="BSDrill(6,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[5], 4) ?></td>
                            <td onClick="BSDrill(7,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[6], 4) ?></td>
                            <td onClick="BSDrill(8,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[7], 4) ?></td>
                            <td onClick="BSDrill(9,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[8], 4) ?></td>
                            <td onClick="BSDrill(10,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[9], 4) ?></td>
                            <td onClick="BSDrill(11,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[10], 4) ?></td>
                            <td onClick="BSDrill(12,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[11], 4) ?></td>
                        </tr>
                    <?php } ?>

                    
                    <?php
                    $detailsArr = getAmounts($accPeriod, 12, $companyId);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];
                        $AssTot[] = $amounts
                        ?><!-- 	Inventories 3-->
                        <<tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <td onClick="BSDrill(1,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[0], 4) ?></td>
                            <td onClick="BSDrill(2,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[1], 4) ?></td>
                            <td onClick="BSDrill(3,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[2], 4) ?></td>
                            <td onClick="BSDrill(4,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[3], 4) ?></td>
                            <td onClick="BSDrill(5,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[4], 4) ?></td>
                            <td onClick="BSDrill(6,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[5], 4) ?></td>
                            <td onClick="BSDrill(7,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[6], 4) ?></td>
                            <td onClick="BSDrill(8,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[7], 4) ?></td>
                            <td onClick="BSDrill(9,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[8], 4) ?></td>
                            <td onClick="BSDrill(10,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[9], 4) ?></td>
                            <td onClick="BSDrill(11,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[10], 4) ?></td>
                            <td onClick="BSDrill(12,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[11], 4) ?></td>
                        </tr>
                    <?php } ?>

                    
                    <?php
                    $detailsArr = getAmounts($accPeriod, 23, $companyId);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];
                        $AssTot[] = $amounts
                    ?><!-- 	Inventories 3-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <td onClick="BSDrill(1,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[0], 4) ?></td>
                            <td onClick="BSDrill(2,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[1], 4) ?></td>
                            <td onClick="BSDrill(3,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[2], 4) ?></td>
                            <td onClick="BSDrill(4,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[3], 4) ?></td>
                            <td onClick="BSDrill(5,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[4], 4) ?></td>
                            <td onClick="BSDrill(6,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[5], 4) ?></td>
                            <td onClick="BSDrill(7,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[6], 4) ?></td>
                            <td onClick="BSDrill(8,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[7], 4) ?></td>
                            <td onClick="BSDrill(9,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[8], 4) ?></td>
                            <td onClick="BSDrill(10,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[9], 4) ?></td>
                            <td onClick="BSDrill(11,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[10], 4) ?></td>
                            <td onClick="BSDrill(12,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[11], 4) ?></td>
                        </tr>
                    <?php } ?>

                    
                    <?php
                    $detailsArr = getAmounts($accPeriod, 13, $companyId);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];
                        $AssTot[] = $amounts
                        ?><!-- 	Inventories 3-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <td onClick="BSDrill(1,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[0], 4) ?></td>
                            <td onClick="BSDrill(2,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[1], 4) ?></td>
                            <td onClick="BSDrill(3,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[2], 4) ?></td>
                            <td onClick="BSDrill(4,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[3], 4) ?></td>
                            <td onClick="BSDrill(5,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[4], 4) ?></td>
                            <td onClick="BSDrill(6,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[5], 4) ?></td>
                            <td onClick="BSDrill(7,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[6], 4) ?></td>
                            <td onClick="BSDrill(8,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[7], 4) ?></td>
                            <td onClick="BSDrill(9,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[8], 4) ?></td>
                            <td onClick="BSDrill(10,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[9], 4) ?></td>
                            <td onClick="BSDrill(11,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[10], 4) ?></td>
                            <td onClick="BSDrill(12,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[11], 4) ?></td>
                        </tr>
                    <?php } ?>

                    
                    <?php
                    $detailsArr = getAmounts($accPeriod, 24, $companyId);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];
                        $AssTot[] = $amounts
                        ?><!-- 	Inventories 3-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <td onClick="BSDrill(1,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[0], 4) ?></td>
                            <td onClick="BSDrill(2,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[1], 4) ?></td>
                            <td onClick="BSDrill(3,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[2], 4) ?></td>
                            <td onClick="BSDrill(4,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[3], 4) ?></td>
                            <td onClick="BSDrill(5,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[4], 4) ?></td>
                            <td onClick="BSDrill(6,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[5], 4) ?></td>
                            <td onClick="BSDrill(7,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[6], 4) ?></td>
                            <td onClick="BSDrill(8,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[7], 4) ?></td>
                            <td onClick="BSDrill(9,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[8], 4) ?></td>
                            <td onClick="BSDrill(10,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[9], 4) ?></td>
                            <td onClick="BSDrill(11,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[10], 4) ?></td>
                            <td onClick="BSDrill(12,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[11], 4) ?></td>
                        </tr>
                        <?php } ?>

                        <?php $total = getTotals100($AssTot,$accDip) ?>
                            <tr class="total">
                                <td>Total Assets</td>
                                <td><?php echo number_format($total[0], 4) ?></td>
                                <td><?php echo number_format($total[1], 4) ?></td>
                                <td><?php echo number_format($total[2], 4) ?></td>
                                <td><?php echo number_format($total[3], 4) ?></td>
                                <td><?php echo number_format($total[4], 4) ?></td>
                                <td><?php echo number_format($total[5], 4) ?></td>
                                <td><?php echo number_format($total[6], 4) ?></td>
                                <td><?php echo number_format($total[7], 4) ?></td>
                                <td><?php echo number_format($total[8], 4) ?></td>
                                <td><?php echo number_format($total[9], 4) ?></td>
                                <td><?php echo number_format($total[10], 4) ?></td>
                                <td><?php echo number_format($total[11], 4) ?></td>
                            </tr>               
                    <tr class="mainHed"><td colspan="13">EQUITY &AMP; LIABILITIES</td></tr>
                    <tr class="subHed"><td colspan="13">Equity...</td></tr>

                    
<?php
$detailsArr = getAmounts($accPeriod, 14, $companyId);
foreach ($detailsArr as $arr) {
    $amounts = $arr[1];
    $equTot[] = $amounts
    ?><!-- 	Equity 0-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <td onClick="BSDrill(1,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[0], 4) ?></td>
                            <td onClick="BSDrill(2,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[1], 4) ?></td>
                            <td onClick="BSDrill(3,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[2], 4) ?></td>
                            <td onClick="BSDrill(4,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[3], 4) ?></td>
                            <td onClick="BSDrill(5,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[4], 4) ?></td>
                            <td onClick="BSDrill(6,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[5], 4) ?></td>
                            <td onClick="BSDrill(7,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[6], 4) ?></td>
                            <td onClick="BSDrill(8,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[7], 4) ?></td>
                            <td onClick="BSDrill(9,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[8], 4) ?></td>
                            <td onClick="BSDrill(10,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[9], 4) ?></td>
                            <td onClick="BSDrill(11,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[10], 4) ?></td>
                            <td onClick="BSDrill(12,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[11], 4) ?></td>
                        </tr>
<?php } ?>

                    
<?php
$detailsArr = getAmounts($accPeriod, 25, $companyId);
foreach ($detailsArr as $arr) {
    $amounts = $arr[1];
    $equTot[] = $amounts
    ?><!-- 	Equity 0-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <td onClick="BSDrill(1,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[0], 4) ?></td>
                            <td onClick="BSDrill(2,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[1], 4) ?></td>
                            <td onClick="BSDrill(3,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[2], 4) ?></td>
                            <td onClick="BSDrill(4,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[3], 4) ?></td>
                            <td onClick="BSDrill(5,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[4], 4) ?></td>
                            <td onClick="BSDrill(6,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[5], 4) ?></td>
                            <td onClick="BSDrill(7,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[6], 4) ?></td>
                            <td onClick="BSDrill(8,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[7], 4) ?></td>
                            <td onClick="BSDrill(9,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[8], 4) ?></td>
                            <td onClick="BSDrill(10,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[9], 4) ?></td>
                            <td onClick="BSDrill(11,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[10], 4) ?></td>
                            <td onClick="BSDrill(12,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[11], 4) ?></td>
                        </tr>
<?php } ?>

                    
<?php
$detailsArr = getAmounts($accPeriod, 15, $companyId);
foreach ($detailsArr as $arr) {
    $amounts = $arr[1];
    $equTot[] = $amounts
    ?><!-- 	Equity 0-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <td onClick="BSDrill(1,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[0], 4) ?></td>
                            <td onClick="BSDrill(2,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[1], 4) ?></td>
                            <td onClick="BSDrill(3,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[2], 4) ?></td>
                            <td onClick="BSDrill(4,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[3], 4) ?></td>
                            <td onClick="BSDrill(5,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[4], 4) ?></td>
                            <td onClick="BSDrill(6,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[5], 4) ?></td>
                            <td onClick="BSDrill(7,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[6], 4) ?></td>
                            <td onClick="BSDrill(8,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[7], 4) ?></td>
                            <td onClick="BSDrill(9,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[8], 4) ?></td>
                            <td onClick="BSDrill(10,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[9], 4) ?></td>
                            <td onClick="BSDrill(11,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[10], 4) ?></td>
                            <td onClick="BSDrill(12,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[11], 4) ?></td>
                        </tr>
<?php } ?>

                              
                    <?php $amounts=getPNLForBS($accPeriod,$companyId);$equTot[]=$amounts ?><!--Get PNL-->
                    <tr class="figurs">
                        <td>PNL</td>
                        <td><?php echo number_format($amounts[0], 4) ?></td>
                        <td><?php echo number_format($amounts[1], 4) ?></td>
                        <td><?php echo number_format($amounts[2], 4) ?></td>
                        <td><?php echo number_format($amounts[3], 4) ?></td>
                        <td><?php echo number_format($amounts[4], 4) ?></td>
                        <td><?php echo number_format($amounts[5], 4) ?></td>
                        <td><?php echo number_format($amounts[6], 4) ?></td>
                        <td><?php echo number_format($amounts[7], 4) ?></td>
                        <td><?php echo number_format($amounts[8], 4) ?></td>
                        <td><?php echo number_format($amounts[9], 4) ?></td>
                        <td><?php echo number_format($amounts[10], 4) ?></td>
                        <td><?php echo number_format($amounts[11], 4) ?></td>
                    </tr>

                    <tr class="subHed"><td colspan="13">Other Non Current Liabilities</td></tr>

                    
<?php
$detailsArr = getAmounts($accPeriod, 26, $companyId);
foreach ($detailsArr as $arr) {
    $amounts = $arr[1];
    $equTot[] = $amounts
    ?><!-- 	Equity 0-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <td onClick="BSDrill(1,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[0], 4) ?></td>
                            <td onClick="BSDrill(2,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[1], 4) ?></td>
                            <td onClick="BSDrill(3,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[2], 4) ?></td>
                            <td onClick="BSDrill(4,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[3], 4) ?></td>
                            <td onClick="BSDrill(5,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[4], 4) ?></td>
                            <td onClick="BSDrill(6,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[5], 4) ?></td>
                            <td onClick="BSDrill(7,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[6], 4) ?></td>
                            <td onClick="BSDrill(8,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[7], 4) ?></td>
                            <td onClick="BSDrill(9,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[8], 4) ?></td>
                            <td onClick="BSDrill(10,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[9], 4) ?></td>
                            <td onClick="BSDrill(11,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[10], 4) ?></td>
                            <td onClick="BSDrill(12,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[11], 4) ?></td>
                        </tr>
<?php } ?>

                    
<?php
$detailsArr = getAmounts($accPeriod, 16, $companyId);
foreach ($detailsArr as $arr) {
    $amounts = $arr[1];
    $equTot[] = $amounts
    ?><!-- 	Equity 0-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <td onClick="BSDrill(1,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[0], 4) ?></td>
                            <td onClick="BSDrill(2,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[1], 4) ?></td>
                            <td onClick="BSDrill(3,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[2], 4) ?></td>
                            <td onClick="BSDrill(4,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[3], 4) ?></td>
                            <td onClick="BSDrill(5,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[4], 4) ?></td>
                            <td onClick="BSDrill(6,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[5], 4) ?></td>
                            <td onClick="BSDrill(7,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[6], 4) ?></td>
                            <td onClick="BSDrill(8,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[7], 4) ?></td>
                            <td onClick="BSDrill(9,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[8], 4) ?></td>
                            <td onClick="BSDrill(10,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[9], 4) ?></td>
                            <td onClick="BSDrill(11,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[10], 4) ?></td>
                            <td onClick="BSDrill(12,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[11], 4) ?></td>
                        </tr>
<?php } ?>

                    
                    <?php
                    $detailsArr = getAmounts($accPeriod, 17, $companyId);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];
                        $equTot[] = $amounts
                        ?><!-- 	Equity 0-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <td onClick="BSDrill(1,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[0], 4) ?></td>
                            <td onClick="BSDrill(2,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[1], 4) ?></td>
                            <td onClick="BSDrill(3,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[2], 4) ?></td>
                            <td onClick="BSDrill(4,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[3], 4) ?></td>
                            <td onClick="BSDrill(5,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[4], 4) ?></td>
                            <td onClick="BSDrill(6,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[5], 4) ?></td>
                            <td onClick="BSDrill(7,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[6], 4) ?></td>
                            <td onClick="BSDrill(8,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[7], 4) ?></td>
                            <td onClick="BSDrill(9,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[8], 4) ?></td>
                            <td onClick="BSDrill(10,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[9], 4) ?></td>
                            <td onClick="BSDrill(11,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[10], 4) ?></td>
                            <td onClick="BSDrill(12,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[11], 4) ?></td>
                        </tr>
<?php } ?>
                    <tr class="subHed"><td colspan="13">Current Liabilities</td></tr>

                    
                    <?php
                    $detailsArr = getAmounts($accPeriod, 18, $companyId);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];
                        $equTot[] = $amounts
                        ?><!-- 	Equity 0-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <td onClick="BSDrill(1,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[0], 4) ?></td>
                            <td onClick="BSDrill(2,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[1], 4) ?></td>
                            <td onClick="BSDrill(3,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[2], 4) ?></td>
                            <td onClick="BSDrill(4,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[3], 4) ?></td>
                            <td onClick="BSDrill(5,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[4], 4) ?></td>
                            <td onClick="BSDrill(6,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[5], 4) ?></td>
                            <td onClick="BSDrill(7,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[6], 4) ?></td>
                            <td onClick="BSDrill(8,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[7], 4) ?></td>
                            <td onClick="BSDrill(9,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[8], 4) ?></td>
                            <td onClick="BSDrill(10,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[9], 4) ?></td>
                            <td onClick="BSDrill(11,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[10], 4) ?></td>
                            <td onClick="BSDrill(12,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[11], 4) ?></td>
                        </tr>
<?php } ?>

                    
                    <?php
                    $detailsArr = getAmounts($accPeriod, 28, $companyId);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];
                        $equTot[] = $amounts
                        ?><!-- 	Equity 0-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <td onClick="BSDrill(1,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[0], 4) ?></td>
                            <td onClick="BSDrill(2,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[1], 4) ?></td>
                            <td onClick="BSDrill(3,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[2], 4) ?></td>
                            <td onClick="BSDrill(4,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[3], 4) ?></td>
                            <td onClick="BSDrill(5,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[4], 4) ?></td>
                            <td onClick="BSDrill(6,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[5], 4) ?></td>
                            <td onClick="BSDrill(7,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[6], 4) ?></td>
                            <td onClick="BSDrill(8,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[7], 4) ?></td>
                            <td onClick="BSDrill(9,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[8], 4) ?></td>
                            <td onClick="BSDrill(10,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[9], 4) ?></td>
                            <td onClick="BSDrill(11,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[10], 4) ?></td>
                            <td onClick="BSDrill(12,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[11], 4) ?></td>
                        </tr>
                    <?php } ?>
                        
                    <?php $total = getTotals($equTot) ?>
                    <tr class="total">
                        <td>Total Equity &AMP;Liabilities </td>
                        <td><?php echo number_format($total[0], 4) ?></td>
                        <td><?php echo number_format($total[1], 4) ?></td>
                        <td><?php echo number_format($total[2], 4) ?></td>
                        <td><?php echo number_format($total[3], 4) ?></td>
                        <td><?php echo number_format($total[4], 4) ?></td>
                        <td><?php echo number_format($total[5], 4) ?></td>
                        <td><?php echo number_format($total[6], 4) ?></td>
                        <td><?php echo number_format($total[7], 4) ?></td>
                        <td><?php echo number_format($total[8], 4) ?></td>
                        <td><?php echo number_format($total[9], 4) ?></td>
                        <td><?php echo number_format($total[10], 4) ?></td>
                        <td><?php echo number_format($total[11], 4) ?></td>
                    </tr>

                </table><!-- main table -->
            </div>
        </form>
    </body>
</html>
<?php



function getDates($startDate, $endDate) {
    $start = $month = strtotime($startDate);
    $end = strtotime($endDate);
    while ($month < $end) {
        $months[] = date('F', $month);
        $month = strtotime("+1 month", $month);
    }
    return $months;
}
?>
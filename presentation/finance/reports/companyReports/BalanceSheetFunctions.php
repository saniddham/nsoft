<?php
function getAmounts($accPeriod,$type,$companyId){
    global $db;
    //get accounting period details
    $resul = $db->RunQuery("SELECT FY.dtmStartingDate,FY.dtmClosingDate FROM mst_financeaccountingperiod AS FY WHERE FY.intStatus = 1 AND FY.intId = $accPeriod");
    $row=mysqli_fetch_array($resul);
    $startDate=$row['dtmStartingDate'];
    $endDate=$row['dtmClosingDate'];
    
//    $year= date('Y', strtotime($startDate));
//    $month=date('m', strtotime($startDate));
    
    $cc=getNumberOfmonths();
    //get the Heding accounts for sub type
     $resultLedg = $db->RunQuery("SELECT m.strType, CA.strName, CA.intId FROM mst_financialsubtype AS m INNER JOIN mst_financechartofaccounts AS CA ON CA.intFinancialTypeId = m.intId WHERE m.intId = $type AND CA.strType = 'Heading' ");
     $c=0;
    while($rowLedg=mysqli_fetch_array($resultLedg)){
        $strType=$rowLedg['strType'];
        $accId=$rowLedg['intId'];
        $accName=$rowLedg['strName'];
        
        $year= date('Y', strtotime($startDate));
        $month=date('m', strtotime($startDate));
        for($i=1;$i<=$cc;++$i){
            $sDate=$year.'-'.$month."-01";
            $eDate=$year.'-'.$month.'-'.getFirstAndLastDates2($month,$year);

            if($month==12){
                $month=0;
                $year=$year+1;
            }
            ++$month;
            $amounts[$i-1]=getMonthAmount($accId,$eDate,$companyId,$strType);    
        }
        
        $detailsArr[$c][0]=$accName;
        $detailsArr[$c][1]=$amounts;
        $detailsArr[$c][2]=$accId;
        ++$c;
        
    }   
    return $detailsArr;
}

function getMonthAmount($accId,$eDate,$companyId,$strType){
    global $db;  
    
    //get Debit total
    $resultDebitTot = $db->RunQuery("SELECT
                                        Sum(TD.amount *  TH.currencyRate) AS debitAmount
                                        FROM
                                        fin_transactions AS TH
                                        INNER JOIN fin_transactions_details AS TD ON TH.entryId = TD.entryId
                                        INNER JOIN mst_financechartofaccounts AS ST ON TD.accountId = ST.intId
                                        WHERE
                                        ST.headerAcc = $accId AND
                                        TD.`credit/debit` = 'D' AND
                                        TH.entryDate <= '$eDate' AND
                                        TH.companyId = $companyId AND
                                        TH.authorized = 1 AND
                                        TH.delStatus = 0");
    $rowDebitTot=mysqli_fetch_array($resultDebitTot);
    //get Credit total
    $resultCreditTot = $db->RunQuery("SELECT
                                        Sum(TD.amount *  TH.currencyRate) AS creditAmount
                                        FROM
                                        fin_transactions AS TH
                                        INNER JOIN fin_transactions_details AS TD ON TH.entryId = TD.entryId
                                        INNER JOIN mst_financechartofaccounts AS ST ON TD.accountId = ST.intId
                                        WHERE
                                        ST.headerAcc = $accId AND
                                        TD.`credit/debit` = 'C' AND
                                        TH.entryDate <= '$eDate' AND
                                        TH.companyId = $companyId AND
                                        TH.authorized = 1 AND
                                        TH.delStatus = 0");
    $rowCreditTot=mysqli_fetch_array($resultCreditTot);
    
    $openingBalance=0.00;
    if ($strType=='D'){
        $openingBalance=($rowDebitTot['debitAmount']-$rowCreditTot['creditAmount']);
    }else{
        $openingBalance=($rowCreditTot['creditAmount'] - $rowDebitTot['debitAmount']);
    }
    return $openingBalance;
}
function getFirstAndLastDates($month){
	
	
	
    $month=(int)$month;
    $monthArry[1]=31;
	
	//$lastday = date('t',strtotime("2/1/2009"));
	
    $monthArry[2]=31;
    $monthArry[3]=31;
    $monthArry[4]=30;
    $monthArry[5]=31;
    $monthArry[6]=30;
    $monthArry[7]=31;
    $monthArry[8]=31;
    $monthArry[9]=30;
    $monthArry[10]=31;
    $monthArry[11]=30;
    $monthArry[12]=31;
    
    return $monthArry[$month];
    
}

function getFirstAndLastDates2($month,$year){
	
    $month=(int)$month;
    $monthArry[1]=31;
	
	$lastday = date('t',strtotime("2/1/$year"));
	
    $monthArry[2]=$lastday;
    $monthArry[3]=31;
    $monthArry[4]=30;
    $monthArry[5]=31;
    $monthArry[6]=30;
    $monthArry[7]=31;
    $monthArry[8]=31;
    $monthArry[9]=30;
    $monthArry[10]=31;
    $monthArry[11]=30;
    $monthArry[12]=31;
    
    return $monthArry[$month];
    
}

function getTotals($AssTot){
    $cc=getNumberOfmonths();
    for($i=0; $i < $cc; ++$i){
        $theTotal=0;
        foreach ($AssTot as $tot){
            $theTotal+=$tot[$i];
        }
        $total[]=$theTotal;
    }
    return $total;
}
function getTotals100($AssTot,$accDip){
    $cc=getNumberOfmonths();
    for($i=0; $i < $cc; ++$i){
        $theTotal=0;
        foreach ($AssTot as $tot){
            $theTotal+=$tot[$i];
        }
		$theTotal-=$accDip[$i];
        $total[]=$theTotal;
    }
    return $total;
}

function getNumberOfmonths(){
    return 12;
}

function getFirstAndLastDatesofMonth($accPeriod,$monthIndex){
    global $db;
    //get accounting period details
    $resul = $db->RunQuery("SELECT FY.dtmStartingDate,FY.dtmClosingDate FROM mst_financeaccountingperiod AS FY WHERE FY.intStatus = 1 AND FY.intId = $accPeriod");
    $row=mysqli_fetch_array($resul);
    $startDate=$row['dtmStartingDate'];
    $endDate=$row['dtmClosingDate'];
    
    $year= date('Y', strtotime($startDate));
    $month=date('m', strtotime($startDate));
    
    $cc=getNumberOfmonths();
    
    for($i=1;$i<=$cc;++$i){
            if($i==$monthIndex){
                $sDate=$year.'-'.$month."-01";
                $eDate=$year.'-'.$month.'-'.getFirstAndLastDates2($month,$year);
                $arMonth['s']=$sDate;
                $arMonth['e']=$eDate;
                break;
            }
            if($month==12){
                $month=0;
                $year=$year+1;
            }
            ++$month;               
        }
        return $arMonth;
}
?>

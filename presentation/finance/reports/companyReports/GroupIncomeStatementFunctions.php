<?php

function getAmountsGIS($accPeriod,$type,$comspanys){      
    global $db;
    //get accounting period details
    $resul = $db->RunQuery("SELECT FY.dtmStartingDate,FY.dtmClosingDate FROM mst_financeaccountingperiod AS FY WHERE FY.intStatus = 1 AND FY.intId = $accPeriod");
    $row=mysqli_fetch_array($resul);
    $startDate=$row['dtmStartingDate'];
    $endDate=$row['dtmClosingDate'];
    
    //$year= date('Y', strtotime($startDate));
    //$month=date('m', strtotime($startDate));
    
    $cc=getNumberOfmonthsGSI();
    //get the Heding accounts for sub type
     $resultLedg = $db->RunQuery("SELECT m.strType, CA.strName, CA.intId FROM mst_financialsubtype AS m INNER JOIN mst_financechartofaccounts AS CA ON CA.intFinancialTypeId = m.intId WHERE m.intId = $type AND CA.strType = 'Heading' ");
     $c=0;
       
    while($rowLedg=mysqli_fetch_array($resultLedg)){
        $strType=$rowLedg['strType'];
        $accId=$rowLedg['intId'];
        $accName=$rowLedg['strName'];
        $totalAm;
        $month=date('m', strtotime($startDate));
        $year= date('Y', strtotime($startDate));        
        for($i=1;$i<=$cc;++$i){
            
           $sDate=$year.'-'.$month."-01";
           $eDate=$year.'-'.$month.'-'.getFirstAndLastDatesGIS($month,$year);

            if($month==12){
                $month=0;
                $year=$year+1;
            }
            ++$month;
            
            foreach($comspanys as $com){
               
                $companyId=$com['id'];
                $am=getMonthAmountGSI($accId,$eDate,$companyId,$strType,$accPeriod,$sDate);
                $amounts[$i-1][$companyId]=$am; 
                $totalAm[$companyId]+=$am;
            }
        }
        $amounts[$cc]=$totalAm;//add year to date to date total to amount array
        $detailsArr[$c][0]=$accName;
        $detailsArr[$c][1]=$amounts;
        $detailsArr[$c][2]=$accId;
        $detailsArr[$c][3]=$totalAm;
        ++$c;
        
    }   
    return $detailsArr;
}

function getMonthAmountGSI($accId,$eDate,$companyId,$strType,$accPeriod,$sDate){
    global $db;  
    
    //get Debit total
    $resultDebitTot = $db->RunQuery("SELECT
                                        Sum(TD.amount *  TH.currencyRate) AS debitAmount
                                        FROM
                                        fin_transactions AS TH
                                        INNER JOIN fin_transactions_details AS TD ON TH.entryId = TD.entryId
                                        INNER JOIN mst_financechartofaccounts AS ST ON TD.accountId = ST.intId
                                        WHERE
                                        ST.headerAcc = $accId AND
                                        TD.`credit/debit` = 'D' AND
                                        TH.entryDate <= '$eDate' AND
                                        TH.entryDate >= '$sDate' AND
                                        TH.companyId = $companyId AND
                                        TH.authorized = 1 AND
                                        TH.delStatus = 0 AND
                                        TH.accPeriod=$accPeriod");

    $rowDebitTot=mysqli_fetch_array($resultDebitTot);
    //get Credit total
    $resultCreditTot = $db->RunQuery("SELECT
                                        Sum(TD.amount *  TH.currencyRate) AS creditAmount
                                        FROM
                                        fin_transactions AS TH
                                        INNER JOIN fin_transactions_details AS TD ON TH.entryId = TD.entryId
                                        INNER JOIN mst_financechartofaccounts AS ST ON TD.accountId = ST.intId
                                        WHERE
                                        ST.headerAcc = $accId AND
                                        TD.`credit/debit` = 'C' AND
                                        TH.entryDate <= '$eDate' AND
                                        TH.entryDate >= '$sDate' AND
                                        TH.companyId = $companyId AND
                                        TH.authorized = 1 AND
                                        TH.delStatus = 0 AND
                                        TH.accPeriod=$accPeriod");
    $rowCreditTot=mysqli_fetch_array($resultCreditTot);
    
    $openingBalance=0.00;
    if ($strType=='D'){
        $openingBalance=($rowDebitTot['debitAmount']-$rowCreditTot['creditAmount']);
    }else{
        $openingBalance=($rowCreditTot['creditAmount'] - $rowDebitTot['debitAmount']);
    }
    return $openingBalance;
}
function getFirstAndLastDatesGIS($month,$year){
    $month=(int)$month;
    $monthArry[1]=31;
    //$monthArry[2]=26;
	$monthArry[2]=date('t',strtotime("2/1/$year"));
    $monthArry[3]=31;
    $monthArry[4]=30;
    $monthArry[5]=31;
    $monthArry[6]=30;
    $monthArry[7]=31;
    $monthArry[8]=31;
    $monthArry[9]=30;
    $monthArry[10]=31;
    $monthArry[11]=30;
    $monthArry[12]=31;
    
    return $monthArry[$month];
    
}
function getTotalsGIS($AssTot,$comId){
    $cc=(getNumberOfmonthsGSI());
    for($i=0; $i < $cc; ++$i){
        $theTotal=0;        
        foreach ($AssTot as $tot){            
            $theTotal+=$tot[$i][$comId];           
        }        
        $total[]=$theTotal;
    }    
    return $total;
}

function getGrossProitLossGIS($rev,$cos,$cop,$comId){    
    $revTot=getTotalsGIS($rev,$comId);
    $cosTot=getTotalsGIS($cos,$comId);
    $copTot=getTotalsGIS($cop,$comId);
    $cc=(getNumberOfmonthsGSI());    
    for($i=0; $i < $cc; ++$i){
        $total[]=($revTot[$i])-($cosTot[$i])-($copTot[$i]);
    }
    return $total;
}

function getNetProfitGIS($rev,$cos,$cop,$oi,$eg,$ex,$gl,$tax,$ureg,$urel,$comId){
    $revTot=getTotalsGIS($rev,$comId);
    $cosTot=getTotalsGIS($cos,$comId);
    $copTot=getTotalsGIS($cop,$comId);
    $oiTot=getTotalsGIS($oi,$comId);
    $egTot=getTotalsGIS($eg,$comId);
    $exTot=getTotalsGIS($ex,$comId);
    $glTot=getTotalsGIS($gl,$comId);
    $taxTot=getTotalsGIS($tax,$comId);
    $uregTot=getTotalsGIS($ureg,$comId);
    $urelTot=getTotalsGIS($urel,$comId);
    $cc=(getNumberOfmonthsGSI()); 
    for($i=0; $i < $cc; ++$i){
        $total[]=($revTot[$i]-$cosTot[$i]-$copTot[$i])+$oiTot[$i]+$egTot[$i]-($exTot[$i])-($glTot[$i])-($taxTot[$i])+($uregTot[$i])-($urelTot[$i]);
    }
    return $total;
}
function getCostOfProductionGIS($pri,$fac){
    
    $priTot=getTotalsGIS($pri);
    $facTot=getTotalsGIS($fac);     
    $cc=(getNumberOfmonthsGSI() + 1);     
    for($i=0; $i < $cc; ++$i){        
        $total[]=$priTot[$i]+$facTot[$i];
    }    
    return $total;
}
function getNumberOfmonthsGSI(){
    return 12;
}
?>

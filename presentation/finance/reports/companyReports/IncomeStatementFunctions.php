<?php

function getAmounts($accPeriod,$type,$companyId){
    global $db;
    //get accounting period details
    $resul = $db->RunQuery("SELECT FY.dtmStartingDate,FY.dtmClosingDate FROM mst_financeaccountingperiod AS FY WHERE FY.intStatus = 1 AND FY.intId = $accPeriod");
    $row=mysqli_fetch_array($resul);
    $startDate=$row['dtmStartingDate'];
    $endDate=$row['dtmClosingDate'];
    
    //$year= date('Y', strtotime($startDate));
    //$month=date('m', strtotime($startDate));
    
    $cc=getNumberOfmonths();
    //get the Heding accounts for sub type
     $resultLedg = $db->RunQuery("SELECT m.strType, CA.strName, CA.intId FROM mst_financialsubtype AS m INNER JOIN mst_financechartofaccounts AS CA ON CA.intFinancialTypeId = m.intId WHERE m.intId = $type AND CA.strType = 'Heading' ");
     $c=0;
    while($rowLedg=mysqli_fetch_array($resultLedg)){
        $strType=$rowLedg['strType'];
        $accId=$rowLedg['intId'];
        $accName=$rowLedg['strName'];
        $totalAm;
        $month=date('m', strtotime($startDate));
        $year= date('Y', strtotime($startDate));
        
        for($i=1;$i<=$cc;++$i){
            
           $sDate=$year.'-'.$month."-01";
           $eDate=$year.'-'.$month.'-'.getFirstAndLastDates($month,$year);

            if($month==12){
                $month=0;
                $year=$year+1;
            }
            ++$month;
            $am=getMonthAmount($accId,$eDate,$companyId,$strType,$accPeriod,$sDate);
            $amounts[$i-1]=$am; 
            $totalAm+=$am;
        }
        $amounts[$cc]=$totalAm;//add year to date to date total to amount array
        $detailsArr[$c][0]=$accName;
        $detailsArr[$c][1]=$amounts;
        $detailsArr[$c][2]=$accId;
        $detailsArr[$c][3]=$totalAm;
        ++$c;
        
    }   
    return $detailsArr;
}

function getMonthAmount($accId,$eDate,$companyId,$strType,$accPeriod,$sDate){
    global $db;  
    
    //get Debit total
    $resultDebitTot = $db->RunQuery("SELECT
                                        Sum(TD.amount *  TH.currencyRate) AS debitAmount
                                        FROM
                                        fin_transactions AS TH
                                        INNER JOIN fin_transactions_details AS TD ON TH.entryId = TD.entryId
                                        INNER JOIN mst_financechartofaccounts AS ST ON TD.accountId = ST.intId
                                        WHERE
                                        ST.headerAcc = $accId AND
                                        TD.`credit/debit` = 'D' AND
                                        TH.entryDate <= '$eDate' AND
                                        TH.entryDate >= '$sDate' AND
                                        TH.companyId = $companyId AND
                                        TH.authorized = 1 AND
                                        TH.delStatus = 0 AND
                                        TH.accPeriod=$accPeriod");

    $rowDebitTot=mysqli_fetch_array($resultDebitTot);
    //get Credit total
    $resultCreditTot = $db->RunQuery("SELECT
                                        Sum(TD.amount *  TH.currencyRate) AS creditAmount
                                        FROM
                                        fin_transactions AS TH
                                        INNER JOIN fin_transactions_details AS TD ON TH.entryId = TD.entryId
                                        INNER JOIN mst_financechartofaccounts AS ST ON TD.accountId = ST.intId
                                        WHERE
                                        ST.headerAcc = $accId AND
                                        TD.`credit/debit` = 'C' AND
                                        TH.entryDate <= '$eDate' AND
                                        TH.entryDate >= '$sDate' AND
                                        TH.companyId = $companyId AND
                                        TH.authorized = 1 AND
                                        TH.delStatus = 0 AND
                                        TH.accPeriod=$accPeriod");
    $rowCreditTot=mysqli_fetch_array($resultCreditTot);
    
    $openingBalance=0.00;
    if ($strType=='D'){
        $openingBalance=($rowDebitTot['debitAmount']-$rowCreditTot['creditAmount']);
    }else{
        $openingBalance=($rowCreditTot['creditAmount'] - $rowDebitTot['debitAmount']);
    }
    return $openingBalance;
}
function getFirstAndLastDates($month,$year){
    $month=(int)$month;
    $monthArry[1]=31;
    //$monthArry[2]=26; commented by roshan.2013-04-11
	$monthArry[2]=date('t',strtotime("2/1/$year"));
    $monthArry[3]=31;
    $monthArry[4]=30;
    $monthArry[5]=31;
    $monthArry[6]=30;
    $monthArry[7]=31;
    $monthArry[8]=31;
    $monthArry[9]=30;
    $monthArry[10]=31;
    $monthArry[11]=30;
    $monthArry[12]=31;
    
    return $monthArry[$month];
    
}
function getTotals($AssTot){
    $cc=(getNumberOfmonths() + 1);
    for($i=0; $i < $cc; ++$i){
        $theTotal=0;        
        foreach ($AssTot as $tot){
            $theTotal+=$tot[$i];           
        }        
        $total[]=$theTotal;
    }
    return $total;
}

function getGrossProitLoss($rev,$cos,$cop){
    $revTot=getTotals($rev);
    $cosTot=getTotals($cos);
    $copTot=getTotals($cop);
    $cc=(getNumberOfmonths() + 1);    
    for($i=0; $i < $cc; ++$i){
        $total[]=$revTot[$i]-$cosTot[$i]-$copTot[$i];
    }
    return $total;
}

function getNetProfit($rev,$cos,$cop,$oi,$eg,$ex,$gl,$tax,$ureg,$urel){
    $revTot=getTotals($rev);
    $cosTot=getTotals($cos);
    $copTot=getTotals($cop);
    $oiTot=getTotals($oi);
    $egTot=getTotals($eg);
    $exTot=getTotals($ex);
    $glTot=getTotals($gl);
    $taxTot=getTotals($tax);
    $uregTot=getTotals($ureg);
    $urelTot=getTotals($urel);
    $cc=(getNumberOfmonths() + 1); 
    for($i=0; $i < $cc; ++$i){
        $total[]=($revTot[$i]-$cosTot[$i]-$copTot[$i])+$oiTot[$i]+$egTot[$i]-($exTot[$i])-($glTot[$i])-($taxTot[$i])+($uregTot[$i])-($urelTot[$i]);
    }
    return $total;
}
function getCostOfProduction($pri,$fac){
    
    $priTot=getTotals($pri);
    $facTot=getTotals($fac);     
    $cc=(getNumberOfmonths() + 1);     
    for($i=0; $i < $cc; ++$i){        
        $total[]=$priTot[$i]+$facTot[$i];
    }    
    return $total;
}
function getNumberOfmonths(){
    return 12;
}
?>

<?php
session_start();
$backwardseperator = "../../../../";
//$companyId = $_SESSION['headCompanyId'];
$locationId = $_SESSION['CompanyID'];
$intUser = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath = $_SERVER['PHP_SELF'];
include "{$backwardseperator}dataAccess/Connector.php";
include 'GroupIncomeStatementFunctions.php';

$accPeriod = $_REQUEST['accPeriod'];
$fileds = $_REQUEST['fileds']; 
//get accounting period details
$resul = $db->RunQuery("SELECT FY.dtmStartingDate,FY.dtmClosingDate FROM mst_financeaccountingperiod AS FY WHERE FY.intStatus = 1 AND FY.intId = $accPeriod");
$row = mysqli_fetch_array($resul);
$startDate = $row['dtmStartingDate'];
$endDate = $row['dtmClosingDate'];
$monthRange = getDates($startDate, $endDate);

$indexs =array(0,1,2,3,4,5,6,7,8,9,10,11);
//get GroupComnays
$resulCom = $db->RunQuery("SELECT c.intId,c.strName FROM mst_companies AS c WHERE c.intStatus =  '1'");
$x=0;
while($rowCom = mysqli_fetch_array($resulCom)){
    $coms[$x]['id']=$rowCom['intId'];
    $coms[$x]['name']=$rowCom['strName'];
    ++$x;
    
}
$span=  count($coms);
$spanHead=  (count($coms)*12) + 2;

$revTot = array();
$cosTot = array();//cost of sale
$copTot = array();//cost of production
$oiTot = array();//Other incoe
$egTot = array();//Ex.gain
$exTot = array();//Expences
$elTot = array();//Ex. Loss
$taxTot = array();//tax
$urelTot = array();//EX unrialize Gain
$uregTot = array();//EX unrialize Loss
?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Group Income Statement</title>
        <link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
        <script type="application/javascript" src="CompanyReportDril.js"></script>
        <style>
            table.maintable{
                width: 100%;
                width: 100%;
                border-collapse:collapse;
            }
            table.maintable tr td{
                border:1px solid black;
            }
            .maintableHeader{
                font-family: Verdana;
                font-size: 12px;
                color: #000000;
                margin: 0px;
                text-align: center;
            }
            .mainHed{
                font-family: Verdana;
                font-size: 11px;
                color: #000000;
                margin: 0px;
                text-align: center;
                font-variant-caps: all-petite-caps;
                font-weight: bold;
            }
            .subHed{
                font-family: Verdana;
                font-size: 11px;
                color: #000000;
                margin: 0px;
                text-align: left;                
                font-weight:bold;

            }
                .subHed1{
                font-family: Verdana;
                font-size: 11px;
                color: #000000;
                margin: 0px;
                text-align: left;    
                


            }
            .figurs{
                font-family: Verdana;
                font-size: 10px;
                color: #000000;
                margin: 0px;
                text-align: right;
                cursor: pointer;
            }
            .total{
                font-family: Verdana;
                font-size: 11px;
                color: #000000;
                margin: 0px;
                text-align: right;                
                font-weight:bolder

            }
            table.subTable{
                width: 100%;
                border-collapse:collapse;
            }
            table.subTable tr td{
                /* border:1px solid black;*/
            }
            .showField{
                display: "";
            }
            .hodeFiled{
                display: none;
            }
            .ytd{
                background-color: #b3b3b3
            }
        </style>
    </head>
    <body>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr><td colspan="3"></td></tr>
            <tr>
                <td width="20%"></td>
                <td width="60%" height="80" valign="top"><?php include '../../reportHeader.php' ?></td>
                <td width="20%"></td>
            </tr>
            <tr><td colspan="3"></td></tr>
            <tr><td colspan="3" style="font-size: 14px;" class="normalfntMid">Group Income Statement - <?php echo $startDate . " To " . $endDate ?></td></tr>
        </table>
        <form name="frmBs" id="frmBs">
            <div align="center"><input type="hidden" name="txtAccPrtiod" id="txtAccPrtiod" value="<?php echo $accPeriod ?>" /><input type="hidden" name="txtrpType" id="txtrpType" value="Income" />
                <table class="maintable"><!-- main table -->
                    <tr class="maintableHeader">
                        <td></td>
                        <td class="<?php echo ($fileds[0]?'showField':'hodeFiled') ?>" colspan="<?php echo $span?>"><?php echo $monthRange[0] ?></td>
                        <td class="<?php echo ($fileds[1]?'showField':'hodeFiled') ?>" colspan="<?php echo $span?>"><?php echo $monthRange[1] ?></td>
                        <td class="<?php echo ($fileds[2]?'showField':'hodeFiled') ?>" colspan="<?php echo $span?>"><?php echo $monthRange[2] ?></td>
                        <td class="<?php echo ($fileds[3]?'showField':'hodeFiled') ?>" colspan="<?php echo $span?>"><?php echo $monthRange[3] ?></td>
                        <td class="<?php echo ($fileds[4]?'showField':'hodeFiled') ?>" colspan="<?php echo $span?>"><?php echo $monthRange[4] ?></td>
                        <td class="<?php echo ($fileds[5]?'showField':'hodeFiled') ?>" colspan="<?php echo $span?>"><?php echo $monthRange[5] ?></td>
                        <td class="<?php echo ($fileds[6]?'showField':'hodeFiled') ?>" colspan="<?php echo $span?>"><?php echo $monthRange[6] ?></td>
                        <td class="<?php echo ($fileds[7]?'showField':'hodeFiled') ?>" colspan="<?php echo $span?>"><?php echo $monthRange[7] ?></td>
                        <td class="<?php echo ($fileds[8]?'showField':'hodeFiled') ?>" colspan="<?php echo $span?>"><?php echo $monthRange[8] ?></td>
                        <td class="<?php echo ($fileds[9]?'showField':'hodeFiled') ?>" colspan="<?php echo $span?>"><?php echo $monthRange[9] ?></td>
                        <td class="<?php echo ($fileds[10]?'showField':'hodeFiled') ?>" colspan="<?php echo $span?>"><?php echo $monthRange[10] ?></td>
                        <td class="<?php echo ($fileds[11]?'showField':'hodeFiled') ?>" colspan="<?php echo $span?>"><?php echo $monthRange[11] ?></td>
                        <td colspan="<?php echo $span?>">Year To Date</td>
                        <td></td>
                    </tr>
                    <tr class="maintableHeader">
                        <td>Account</td>
                        <?php
                        foreach($indexs as $x){
                            foreach($coms as $com){
                                
                                echo "<td style='width:100px' class=". ($fileds[$x]?'showField':'hodeFiled').">".$com['name']."</td>";
                            }
                        }
                        foreach($coms as $com){//Year To Date
                                
                                echo "<td class='ytd'>".$com['name']."</td>";
                        }
                        ?>
                        <td>Group Total</td>
                    </tr>
                    
                    <tr class="subHed"><td colspan="<?php echo $spanHead?>">Revenue</td></tr>
                    
                    <?php
                    
                    $detailsArr = getAmountsGIS($accPeriod, 3, $coms);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];
                        $revTot[] = $amounts
                        ?><!--Fixed Assets 0-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <?php
                            foreach($indexs as $x){
                                foreach($coms as $com){
                                     $companyId=val($com['id']);                                     
                                ?>
                                    <td class="<?php echo ($fileds[$x]?'showField':'hodeFiled') ?>" onclick="GISDrill('<?php echo ($x+1) ?>','<?php echo $arr[2] ?>','<?php echo $companyId ?>');"><?php echo number_format($amounts[$x][$companyId], 4) ?></td>
                            <?php
                                }
                            }
                            foreach($coms as $com){
                                $companyId=val($com['id']);
                                $gTot+=$arr[3][$companyId];
                            ?>
                                <td class="ytd"><?php echo number_format($arr[3][$companyId], 4) ?></td>
                            <?php }?>
                            <td class="gt"><?php echo number_format($gTot, 4) ?></td>
                        </tr>
                    <?php } ?>
                        
                    <tr class="subHed"><td colspan="<?php echo $spanHead?>">Cost of Sale</td></tr>
                    
                    <?php
                    $detailsArr = getAmountsGIS($accPeriod, 5, $coms);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];       
                        $cosTot[] = $amounts
                        ?><!--Fixed Assets 0-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <?php
                            foreach($indexs as $x){
                                foreach($coms as $com){
                                     $companyId=val($com['id']);                                     
                                ?>
                                    <td class="<?php echo ($fileds[$x]?'showField':'hodeFiled') ?>" onclick="GISDrill('<?php echo ($x+1) ?>','<?php echo $arr[2] ?>','<?php echo $companyId ?>');"><?php echo number_format($amounts[$x][$companyId], 4) ?></td>
                            <?php
                                }
                            }  
                            foreach($coms as $com){
                                $companyId=val($com['id']);
                                $gTot+=$arr[3][$companyId];
                            ?>
                                <td class="ytd"><?php echo number_format($arr[3][$companyId], 4) ?></td>
                            <?php }?>
                            <td class="gt"><?php echo number_format($gTot, 4) ?></td>
                        </tr>
                    <?php } ?>
                        
                    <tr class="subHed"><td colspan="<?php echo $spanHead?>">Cost Of Production</td></tr>
                    
                    <?php
                    $detailsArr = getAmountsGIS($accPeriod, 1, $coms);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];                        
                        $copTot[] = $amounts
                        ?><!--Fixed Assets 0-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <?php
                            foreach($indexs as $x){
                                foreach($coms as $com){
                                     $companyId=val($com['id']);                                     
                                ?>
                                    <td class="<?php echo ($fileds[$x]?'showField':'hodeFiled') ?>" onclick="GISDrill('<?php echo ($x+1) ?>','<?php echo $arr[2] ?>','<?php echo $companyId ?>');"><?php echo number_format($amounts[$x][$companyId], 4) ?></td>
                            <?php
                                }
                            }  
                            foreach($coms as $com){
                                $companyId=val($com['id']);
                                $gTot+=$arr[3][$companyId];
                            ?>
                                <td class="ytd"><?php echo number_format($arr[3][$companyId], 4) ?></td>
                            <?php }?>
                            <td class="gt"><?php echo number_format($gTot, 4) ?></td>
                        </tr>
                    <?php } ?>
                    
                    <?php
                    $detailsArr = getAmountsGIS($accPeriod, 2, $coms);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];
                        $copTot[] = $amounts
                        ?><!--Fixed Assets 0-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <?php
                            foreach($indexs as $x){
                                foreach($coms as $com){
                                     $companyId=val($com['id']);                                  
                                     
                                ?>
                                    <td class="<?php echo ($fileds[$x]?'showField':'hodeFiled') ?>" onclick="GISDrill('<?php echo ($x+1) ?>','<?php echo $arr[2] ?>','<?php echo $companyId ?>');"><?php echo number_format($amounts[$x][$companyId], 4) ?></td>
                            <?php
                                }
                            }  
                            foreach($coms as $com){
                                $companyId=val($com['id']);
                                $gTot+=$arr[3][$companyId];
                            ?>
                                <td class="ytd"><?php echo number_format($arr[3][$companyId], 4) ?></td>
                            <?php }?>
                            <td class="gt"><?php echo number_format($gTot, 4) ?></td>
                        </tr>
                    <?php } ?>  
                        
                    <?php //$total = getGrossProitLoss($revTot,$cosTot,$copTot) ?>
                    <tr class="total">
                        <td>Gross Profit/(Loss)</td>
                        <?php
                            $grosTOT;
                            $grosGTOT=0;
                            foreach($indexs as $x){
                                foreach($coms as $com){
                                     $companyId=val($com['id']);                                     
                                     $totalGros = getGrossProitLossGIS($revTot,$cosTot,$copTot,$companyId);
                                     $grosTOT[$companyId]+=$totalGros[$x];
                                     
                                ?>
                                    <td class="<?php echo ($fileds[$x]?'showField':'hodeFiled') ?>"><?php echo number_format($totalGros[$x], 4) ?></td>
                            <?php
                                }
                            }  
                            foreach($coms as $com){
                                $companyId=val($com['id']);
                                $grosGTOT+=$grosTOT[$companyId];
                            ?>
                                <td class="ytd"><?php echo number_format($grosTOT[$companyId], 4) ?></td>
                            <?php }?>
                            <td class="gt"><?php echo number_format($grosGTOT, 4) ?></td>     
                    </tr>
                        
                    
                    <?php
                    $detailsArr = getAmountsGIS($accPeriod, 4, $coms);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];
                        $oiTot[] = $amounts
                        ?><!--Fixed Assets 0-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <?php
                            foreach($indexs as $x){
                                foreach($coms as $com){
                                     $companyId=val($com['id']);
                                ?>
                                    <td class="<?php echo ($fileds[$x]?'showField':'hodeFiled') ?>" onclick="GISDrill('<?php echo ($x+1) ?>','<?php echo $arr[2] ?>','<?php echo $companyId ?>');"><?php echo number_format($amounts[$x][$companyId], 4) ?></td>
                            <?php
                                }
                            }  
                            foreach($coms as $com){
                                $companyId=val($com['id']);
                                $gTot+=$arr[3][$companyId];
                            ?>
                                <td class="ytd"><?php echo number_format($arr[3][$companyId], 4) ?></td>
                            <?php }?>
                            <td class="gt"><?php echo number_format($gTot, 4) ?></td>
                        </tr>
                    <?php } ?>  
                        
                    
                    <?php
                    $detailsArr = getAmountsGIS($accPeriod, 27, $coms);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];
                        $egTot[] = $amounts
                        ?><!--Fixed Assets 0-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <?php
                            foreach($indexs as $x){
                                foreach($coms as $com){
                                     $companyId=val($com['id']);
                                ?>
                                    <td class="<?php echo ($fileds[$x]?'showField':'hodeFiled') ?>" onclick="GISDrill('<?php echo ($x+1) ?>','<?php echo $arr[2] ?>','<?php echo $companyId ?>');"><?php echo number_format($amounts[$x][$companyId], 4) ?></td>
                            <?php
                                }
                            }  
                            foreach($coms as $com){
                                $companyId=val($com['id']);
                                $gTot+=$arr[3][$companyId];
                            ?>
                                <td class="ytd"><?php echo number_format($arr[3][$companyId], 4) ?></td>
                            <?php }?>
                            <td class="gt"><?php echo number_format($gTot, 4) ?></td>
                        </tr>
                    <?php } ?>  
                        
                    
                    <?php
                    $detailsArr = getAmountsGIS($accPeriod, 6, $coms);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];
                        $exTot[] = $amounts
                        ?><!--Fixed Assets 0-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                           <?php
                            foreach($indexs as $x){
                                foreach($coms as $com){
                                     $companyId=val($com['id']);
                                ?>
                                    <td class="<?php echo ($fileds[$x]?'showField':'hodeFiled') ?>" onclick="GISDrill('<?php echo ($x+1) ?>','<?php echo $arr[2] ?>','<?php echo $companyId ?>');"><?php echo number_format($amounts[$x][$companyId], 4) ?></td>
                            <?php
                                }
                            }  
                            foreach($coms as $com){
                                $companyId=val($com['id']);
                                $gTot+=$arr[3][$companyId];
                            ?>
                                <td class="ytd"><?php echo number_format($arr[3][$companyId], 4) ?></td>
                            <?php }?>
                            <td class="gt"><?php echo number_format($gTot, 4) ?></td>
                        </tr>
                    <?php } ?> 
                        
                    
                    <?php
                    $detailsArr = getAmountsGIS($accPeriod, 29, $coms);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];
                        $elTot[] = $amounts
                        ?><!--Fixed Assets 0-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <?php
                            foreach($indexs as $x){
                                foreach($coms as $com){
                                     $companyId=val($com['id']);
                                ?>
                                    <td class="<?php echo ($fileds[$x]?'showField':'hodeFiled') ?>" onclick="GISDrill('<?php echo ($x+1) ?>','<?php echo $arr[2] ?>','<?php echo $companyId ?>');"><?php echo number_format($amounts[$x][$companyId], 4) ?></td>
                            <?php
                                }
                            }  
                            foreach($coms as $com){
                                $companyId=val($com['id']);
                                $gTot+=$arr[3][$companyId];
                            ?>
                                <td class="ytd"><?php echo number_format($arr[3][$companyId], 4) ?></td>
                            <?php }?>
                            <td class="gt"><?php echo number_format($gTot, 4) ?></td>
                        </tr>
                    <?php } ?> 
                        
                   
                    <?php
                    $detailsArr = getAmountsGIS($accPeriod, 7, $coms);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];
                        $taxTot[] = $amounts
                        ?><!--Tax-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <?php
                            foreach($indexs as $x){
                                foreach($coms as $com){
                                     $companyId=val($com['id']);
                                ?>
                                    <td class="<?php echo ($fileds[$x]?'showField':'hodeFiled') ?>" onclick="GISDrill('<?php echo ($x+1) ?>','<?php echo $arr[2] ?>','<?php echo $companyId ?>');"><?php echo number_format($amounts[$x][$companyId], 4) ?></td>
                            <?php
                                }
                            }  
                            foreach($coms as $com){
                                $companyId=val($com['id']);
                                $gTot+=$arr[3][$companyId];
                            ?>
                                <td class="ytd"><?php echo number_format($arr[3][$companyId], 4) ?></td>
                            <?php }?>
                            <td class="gt"><?php echo number_format($gTot, 4) ?></td>
                        </tr>
                    <?php } ?> 
                        
                    <?php
                    $detailsArr = getAmountsGIS($accPeriod, 30, $coms);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];
                        $uregTot[] = $amounts
                        ?><!--EX unrialize Gain-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <?php
                            foreach($indexs as $x){
                                foreach($coms as $com){
                                     $companyId=val($com['id']);
                                ?>
                                    <td class="<?php echo ($fileds[$x]?'showField':'hodeFiled') ?>" onclick="GISDrill('<?php echo ($x+1) ?>','<?php echo $arr[2] ?>','<?php echo $companyId ?>');"><?php echo number_format($amounts[$x][$companyId], 4) ?></td>
                            <?php
                                }
                            }  
                            foreach($coms as $com){
                                $companyId=val($com['id']);
                                $gTot+=$arr[3][$companyId];
                            ?>
                                <td class="ytd"><?php echo number_format($arr[3][$companyId], 4) ?></td>
                            <?php }?>
                            <td class="gt"><?php echo number_format($gTot, 4) ?></td>
                        </tr>
                    <?php } ?> 
                        
                    
                   <?php
                    $detailsArr = getAmountsGIS($accPeriod, 31, $coms);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];
                        $urelTot[] = $amounts
                        ?><!--unrialize Loss-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <?php
                            foreach($indexs as $x){
                                foreach($coms as $com){
                                     $companyId=val($com['id']);
                                ?>
                                    <td class="<?php echo ($fileds[$x]?'showField':'hodeFiled') ?>" onclick="GISDrill('<?php echo ($x+1) ?>','<?php echo $arr[2] ?>','<?php echo $companyId ?>');"><?php echo number_format($amounts[$x][$companyId], 4) ?></td>
                            <?php
                                }
                            }  
                            foreach($coms as $com){
                                $companyId=val($com['id']);
                                $gTot+=$arr[3][$companyId];
                            ?>
                                <td class="ytd"><?php echo number_format($arr[3][$companyId], 4) ?></td>
                            <?php }?>
                            <td class="gt"><?php echo number_format($gTot, 4) ?></td>
                        </tr>
                    <?php } ?>      
                        
                        
                    
                    <?php //$total = getNetProfit($revTot,$cosTot,$copTot,$oiTot,$egTot,$exTot,$elTot,$taxTot)?>
                        
                    <tr class="total">
                        <td>Net Profit/(Loss)</td>
                        <?php
                            $netTOT;
                            $netGTOT=0;
                            foreach($indexs as $x){
                                foreach($coms as $com){
                                     $companyId=val($com['id']);                                     
                                     $total = getNetProfitGIS($revTot,$cosTot,$copTot,$oiTot,$egTot,$exTot,$elTot,$taxTot,$uregTot,$urelTot,$companyId);
                                     $netTOT[$companyId]+=$total[$x];
                                     
                                ?>
                                    <td class="<?php echo ($fileds[$x]?'showField':'hodeFiled') ?>"><?php echo number_format($total[$x], 4) ?></td>
                            <?php
                                }
                            } 
                            foreach($coms as $com){
                                     $companyId=val($com['id']);
                                     $netGTOT+=$netTOT[$companyId];
                            ?>
                                <td class="ytd"><?php echo number_format($netTOT[$companyId], 4) ?></td>
                                <?php
                            }?>
                                <td class="gt"><?php echo number_format($netGTOT, 4) ?></td>
                    </tr>
                </table><!-- main table -->
            </div>
        </form>
    </body>
</html>
<?php

function getDates($startDate, $endDate) {
    $start = $month = strtotime($startDate);
    $end = strtotime($endDate);
    while ($month < $end) {
        $months[] = date('F', $month);
        $month = strtotime("+1 month", $month);
    }
    return $months;
}
?>
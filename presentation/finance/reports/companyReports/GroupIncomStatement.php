<?php
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$companyId = $_SESSION['headCompanyId'];
include "{$backwardseperator}dataAccess/Connector.php";


$accPeriod = $_REQUEST['accPeriod'];
//get accounting period details
$resul = $db->RunQuery("SELECT FY.dtmStartingDate,FY.dtmClosingDate FROM mst_financeaccountingperiod AS FY WHERE FY.intStatus = 1 AND FY.intId = $accPeriod");
$row = mysqli_fetch_array($resul);
$startDate = $row['dtmStartingDate'];
$endDate = $row['dtmClosingDate'];
$monthRange = getDates($startDate, $endDate);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Group Income Statement</title>
        <link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />

        <script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
        <script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
        <script type="application/javascript" src="CompanyReports.js"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

        <link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
        <script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
        <link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css" />
        <link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css" /> 

<!--<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script> -->
        <style>
            .showReport{
                font-family: Verdana;
                font-size: 11px;
                color: #0000ff;
                margin: 0px;
                text-align: center;                
                height: 25px;
                text-decoration: underline;

            }
            a:link {color: #0000ff; text-decoration: underline; }
            a:active {color: #0000ff; text-decoration: underline; }
            a:visited {color: #0000ff; text-decoration: underline; }
            a:hover {color: #ff0000; text-decoration: none; cursor: pointer; }


        </style> 
    </head>    
    <body>
        <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
                <td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator . 'Header.php'; ?></td>
            </tr> 
        </table>
        <div align="center">
            <div class="trans_layoutL">
                <div class="trans_text">Group Income Statement</div> 
                <form name="frmGroupIncom" method="post" id="frmGroupIncom" autocomplete="off">
                    <table width="80%" cellspacing="0" cellpadding="5"><input type="hidden" id="txtAccperiod" name="txtAccperiod" value="<?php echo $accPeriod ?>" />
                        <tr>
                            <td colspan="2" class="normalfntRight">Account Period:</td>
                            <td colspan="2" class="normalfnt"><?php echo $startDate . " To " .$endDate ?></td>
                        </tr>
                        <tr>
                            <td colspan="4" class="normalfntMid">Select Months to Display</td>                                                       
                        </tr>
                        <tr>
                            <td class="normalfnt"><input type="checkbox" name="chkMonths" value="0" /><?php echo $monthRange[0] ?></td>                                                       
                            <td class="normalfnt"><input type="checkbox" name="chkMonths" value="1" /><?php echo $monthRange[1] ?></td>                                                       
                            <td class="normalfnt"><input type="checkbox" name="chkMonths" value="2" /><?php echo $monthRange[2] ?></td>                                                       
                            <td class="normalfnt"><input type="checkbox" name="chkMonths" value="3" /><?php echo $monthRange[3] ?></td>                                                       
                        </tr>
                        <tr>
                            <td class="normalfnt"><input type="checkbox" name="chkMonths" value="4" /><?php echo $monthRange[4] ?></td>                                                       
                            <td class="normalfnt"><input type="checkbox" name="chkMonths" value="5" /><?php echo $monthRange[5] ?></td>                                                       
                            <td class="normalfnt"><input type="checkbox" name="chkMonths" value="6" /><?php echo $monthRange[6] ?></td>                                                       
                            <td class="normalfnt"><input type="checkbox" name="chkMonths" value="7" /><?php echo $monthRange[7] ?></td>                                                       
                        </tr>
                        <tr>
                            <td class="normalfnt"><input type="checkbox" name="chkMonths" value="8" /><?php echo $monthRange[8] ?></td>                                                       
                            <td class="normalfnt"><input type="checkbox" name="chkMonths" value="9" /><?php echo $monthRange[9] ?></td>                                                       
                            <td class="normalfnt"><input type="checkbox" name="chkMonths" value="10" /><?php echo $monthRange[10] ?></td>                                                       
                            <td class="normalfnt"><input type="checkbox" name="chkMonths" value="11" /><?php echo $monthRange[11] ?></td>                                                       
                        </tr>
                        <tr><td colspan="4" align="center"><img border="0" src="../../../../images/Treport.jpg" alt="Summery Report" name="butReport" width="92" height="24"  class="mouseover" id="butReport"/></td></tr>
                    </table>
                </form>
            </div>
        </div>

    </body>
</html>
<?php

function getDates($startDate, $endDate) {
    $start = $month = strtotime($startDate);
    $end = strtotime($endDate);
    while ($month < $end) {
        $months[] = date('F', $month);
        $month = strtotime("+1 month", $month);
    }
    return $months;
}
<?php
session_start();
$backwardseperator = "../../../../";
$companyId = $_SESSION['headCompanyId'];
$locationId = $_SESSION['CompanyID'];
$intUser = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath = $_SERVER['PHP_SELF'];
include "{$backwardseperator}dataAccess/Connector.php";
include 'IncomeStatementFunctions.php';

$accPeriod = $_REQUEST['accPeriod'];
//get accounting period details
$resul = $db->RunQuery("SELECT FY.dtmStartingDate,FY.dtmClosingDate FROM mst_financeaccountingperiod AS FY WHERE FY.intStatus = 1 AND FY.intId = $accPeriod");
$row = mysqli_fetch_array($resul);
$startDate = $row['dtmStartingDate'];
$endDate = $row['dtmClosingDate'];
$monthRange = getDates($startDate, $endDate);

$revTot = array();
$cosTot = array();//cost of sale
$copTot = array();//cost of production
$oiTot = array();//Other incoe
$egTot = array();//Ex.gain
$exTot = array();//Expences
$elTot = array();//Ex. Loss
$taxTot = array();//tax
$urelTot = array();//EX unrialize Loss
$uregTot = array();//EX unrialize Gain
?>
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Income Statement</title>
        <link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
        <script type="application/javascript" src="CompanyReportDril.js"></script>
        <style>
            table.maintable{
                width: 100%;
                width: 100%;
                border-collapse:collapse;
            }
            table.maintable tr td{
                border:1px solid black;
            }
            .maintableHeader{
                font-family: Verdana;
                font-size: 12px;
                color: #000000;
                margin: 0px;
                text-align: center;
            }
            .mainHed{
                font-family: Verdana;
                font-size: 11px;
                color: #000000;
                margin: 0px;
                text-align: center;
                font-variant-caps: all-petite-caps;
                font-weight: bold;
            }
            .subHed{
                font-family: Verdana;
                font-size: 11px;
                color: #000000;
                margin: 0px;
                text-align: left;                
                font-weight:bold;

            }
                .subHed1{
                font-family: Verdana;
                font-size: 11px;
                color: #000000;
                margin: 0px;
                text-align: left;    
                


            }
            .figurs{
                font-family: Verdana;
                font-size: 10px;
                color: #000000;
                margin: 0px;
                text-align: right;
                cursor: pointer;
            }
            .total{
                font-family: Verdana;
                font-size: 11px;
                color: #000000;
                margin: 0px;
                text-align: right;                
                font-weight:bolder

            }
            table.subTable{
                width: 100%;
                border-collapse:collapse;
            }
            table.subTable tr td{
                /* border:1px solid black;*/
            }
        </style>
    </head>
    <body>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr><td colspan="3"></td></tr>
            <tr>
                <td width="20%"></td>
                <td width="60%" height="80" valign="top"><?php include '../../reportHeader.php' ?></td>
                <td width="20%"></td>
            </tr>
            <tr><td colspan="3"></td></tr>
            <tr><td colspan="3" style="font-size: 14px;" class="normalfntMid">Income Statement - <?php echo $startDate . " To " . $endDate ?></td></tr>
        </table>
        <form name="frmBs" id="frmBs">
            <div align="center"><input type="hidden" name="txtAccPrtiod" id="txtAccPrtiod" value="<?php echo $accPeriod ?>" /><input type="hidden" name="txtrpType" id="txtrpType" value="Income" />
                <table class="maintable"><!-- main table -->
                    <tr class="maintableHeader">
                        <td>Account</td>
                        <td><?php echo $monthRange[0] ?></td>
                        <td><?php echo $monthRange[1] ?></td>
                        <td><?php echo $monthRange[2] ?></td>
                        <td><?php echo $monthRange[3] ?></td>
                        <td><?php echo $monthRange[4] ?></td>
                        <td><?php echo $monthRange[5] ?></td>
                        <td><?php echo $monthRange[6] ?></td>
                        <td><?php echo $monthRange[7] ?></td>
                        <td><?php echo $monthRange[8] ?></td>
                        <td><?php echo $monthRange[9] ?></td>
                        <td><?php echo $monthRange[10] ?></td>
                        <td><?php echo $monthRange[11] ?></td>
                        <td>Year To Date</td>
                    </tr>
                    <tr class="subHed"><td colspan="14">Revenue</td></tr>
                    
                    <?php
                    
                    $detailsArr = getAmounts($accPeriod, 3, $companyId);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];
                        $revTot[] = $amounts
                        ?><!--Fixed Assets 0-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <td onclick="BSDrill(1,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[0], 4) ?></td>
                            <td onclick="BSDrill(2,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[1], 4) ?></td>
                            <td onclick="BSDrill(3,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[2], 4) ?></td>
                            <td onclick="BSDrill(4,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[3], 4) ?></td>
                            <td onclick="BSDrill(5,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[4], 4) ?></td>
                            <td onclick="BSDrill(6,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[5], 4) ?></td>
                            <td onclick="BSDrill(7,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[6], 4) ?></td>
                            <td onclick="BSDrill(8,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[7], 4) ?></td>
                            <td onclick="BSDrill(9,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[8], 4) ?></td>
                            <td onclick="BSDrill(10,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[9], 4) ?></td>
                            <td onclick="BSDrill(11,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[10], 4) ?></td>
                            <td onclick="BSDrill(12,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[11], 4) ?></td>
                            <td><?php echo number_format($arr[3], 4) ?></td>
                        </tr>
                    <?php } ?>
                        
                    <tr class="subHed"><td colspan="14">Cost of Sale</td></tr>
                    
                    <?php
                    $detailsArr = getAmounts($accPeriod, 5, $companyId);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];                        
                        $cosTot[] = $amounts
                        ?><!--Fixed Assets 0-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <td onclick="BSDrill(1,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[0], 4) ?></td>
                            <td onclick="BSDrill(2,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[1], 4) ?></td>
                            <td onclick="BSDrill(3,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[2], 4) ?></td>
                            <td onclick="BSDrill(4,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[3], 4) ?></td>
                            <td onclick="BSDrill(5,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[4], 4) ?></td>
                            <td onclick="BSDrill(6,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[5], 4) ?></td>
                            <td onclick="BSDrill(7,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[6], 4) ?></td>
                            <td onclick="BSDrill(8,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[7], 4) ?></td>
                            <td onclick="BSDrill(9,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[8], 4) ?></td>
                            <td onclick="BSDrill(10,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[9], 4) ?></td>
                            <td onclick="BSDrill(11,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[10], 4) ?></td>
                            <td onclick="BSDrill(12,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[11], 4) ?></td>
                            <td><?php echo number_format($arr[3], 4) ?></td>
                        </tr>
                    <?php } ?>
                        
                    <tr class="subHed"><td colspan="14">Cost Of Production</td></tr>
                    
                    <?php
                    $detailsArr = getAmounts($accPeriod, 1, $companyId);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];
                        $copTot[] = $amounts
                        ?><!--Fixed Assets 0-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <td onclick="BSDrill(1,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[0], 4) ?></td>
                            <td onclick="BSDrill(2,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[1], 4) ?></td>
                            <td onclick="BSDrill(3,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[2], 4) ?></td>
                            <td onclick="BSDrill(4,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[3], 4) ?></td>
                            <td onclick="BSDrill(5,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[4], 4) ?></td>
                            <td onclick="BSDrill(6,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[5], 4) ?></td>
                            <td onclick="BSDrill(7,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[6], 4) ?></td>
                            <td onclick="BSDrill(8,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[7], 4) ?></td>
                            <td onclick="BSDrill(9,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[8], 4) ?></td>
                            <td onclick="BSDrill(10,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[9], 4) ?></td>
                            <td onclick="BSDrill(11,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[10], 4) ?></td>
                            <td onclick="BSDrill(12,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[11], 4) ?></td>
                            <td><?php echo number_format($arr[3], 4) ?></td>
                        </tr>
                    <?php } ?>
                    
                    <?php
                    $detailsArr = getAmounts($accPeriod, 2, $companyId);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];
                        $copTot[] = $amounts
                        ?><!--Fixed Assets 0-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <td onclick="BSDrill(1,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[0], 4) ?></td>
                            <td onclick="BSDrill(2,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[1], 4) ?></td>
                            <td onclick="BSDrill(3,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[2], 4) ?></td>
                            <td onclick="BSDrill(4,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[3], 4) ?></td>
                            <td onclick="BSDrill(5,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[4], 4) ?></td>
                            <td onclick="BSDrill(6,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[5], 4) ?></td>
                            <td onclick="BSDrill(7,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[6], 4) ?></td>
                            <td onclick="BSDrill(8,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[7], 4) ?></td>
                            <td onclick="BSDrill(9,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[8], 4) ?></td>
                            <td onclick="BSDrill(10,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[9], 4) ?></td>
                            <td onclick="BSDrill(11,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[10], 4) ?></td>
                            <td onclick="BSDrill(12,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[11], 4) ?></td>
                            <td><?php echo number_format($arr[3], 4) ?></td>
                        </tr>
                    <?php } ?>  
                        
                    <?php $total = getGrossProitLoss($revTot,$cosTot,$copTot) ?>
                    <tr class="total">
                        <td>Gross Profit/(Loss)</td>
                        <td><?php echo number_format($total[0], 4) ?></td>
                        <td><?php echo number_format($total[1], 4) ?></td>
                        <td><?php echo number_format($total[2], 4) ?></td>
                        <td><?php echo number_format($total[3], 4) ?></td>
                        <td><?php echo number_format($total[4], 4) ?></td>
                        <td><?php echo number_format($total[5], 4) ?></td>
                        <td><?php echo number_format($total[6], 4) ?></td>
                        <td><?php echo number_format($total[7], 4) ?></td>
                        <td><?php echo number_format($total[8], 4) ?></td>
                        <td><?php echo number_format($total[9], 4) ?></td>
                        <td><?php echo number_format($total[10], 4) ?></td>
                        <td><?php echo number_format($total[11], 4) ?></td>
                        <td><?php echo number_format($total[12], 4) ?></td>
                    </tr>
                        
                    
                    <?php
                    $detailsArr = getAmounts($accPeriod, 4, $companyId);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];
                        $oiTot[] = $amounts
                        ?><!--Fixed Assets 0-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <td onclick="BSDrill(1,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[0], 4) ?></td>
                            <td onclick="BSDrill(2,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[1], 4) ?></td>
                            <td onclick="BSDrill(3,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[2], 4) ?></td>
                            <td onclick="BSDrill(4,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[3], 4) ?></td>
                            <td onclick="BSDrill(5,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[4], 4) ?></td>
                            <td onclick="BSDrill(6,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[5], 4) ?></td>
                            <td onclick="BSDrill(7,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[6], 4) ?></td>
                            <td onclick="BSDrill(8,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[7], 4) ?></td>
                            <td onclick="BSDrill(9,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[8], 4) ?></td>
                            <td onclick="BSDrill(10,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[9], 4) ?></td>
                            <td onclick="BSDrill(11,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[10], 4) ?></td>
                            <td onclick="BSDrill(12,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[11], 4) ?></td>
                            <td><?php echo number_format($arr[3], 4) ?></td>
                        </tr>
                    <?php } ?>  
                        
                    
                    <?php
                    $detailsArr = getAmounts($accPeriod, 27, $companyId);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];
                        $egTot[] = $amounts
                        ?><!--Fixed Assets 0-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <td onclick="BSDrill(1,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[0], 4) ?></td>
                            <td onclick="BSDrill(2,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[1], 4) ?></td>
                            <td onclick="BSDrill(3,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[2], 4) ?></td>
                            <td onclick="BSDrill(4,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[3], 4) ?></td>
                            <td onclick="BSDrill(5,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[4], 4) ?></td>
                            <td onclick="BSDrill(6,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[5], 4) ?></td>
                            <td onclick="BSDrill(7,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[6], 4) ?></td>
                            <td onclick="BSDrill(8,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[7], 4) ?></td>
                            <td onclick="BSDrill(9,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[8], 4) ?></td>
                            <td onclick="BSDrill(10,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[9], 4) ?></td>
                            <td onclick="BSDrill(11,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[10], 4) ?></td>
                            <td onclick="BSDrill(12,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[11], 4) ?></td>
                            <td><?php echo number_format($arr[3], 4) ?></td>
                        </tr>
                    <?php } ?>  
                        
                    
                    <?php
                    $detailsArr = getAmounts($accPeriod, 6, $companyId);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];
                        $exTot[] = $amounts
                        ?><!--Fixed Assets 0-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <td onclick="BSDrill(1,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[0], 4) ?></td>
                            <td onclick="BSDrill(2,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[1], 4) ?></td>
                            <td onclick="BSDrill(3,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[2], 4) ?></td>
                            <td onclick="BSDrill(4,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[3], 4) ?></td>
                            <td onclick="BSDrill(5,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[4], 4) ?></td>
                            <td onclick="BSDrill(6,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[5], 4) ?></td>
                            <td onclick="BSDrill(7,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[6], 4) ?></td>
                            <td onclick="BSDrill(8,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[7], 4) ?></td>
                            <td onclick="BSDrill(9,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[8], 4) ?></td>
                            <td onclick="BSDrill(10,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[9], 4) ?></td>
                            <td onclick="BSDrill(11,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[10], 4) ?></td>
                            <td onclick="BSDrill(12,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[11], 4) ?></td>
                            <td><?php echo number_format($arr[3], 4) ?></td>
                        </tr>
                    <?php } ?> 
                        
                    
                    <?php
                    $detailsArr = getAmounts($accPeriod, 29, $companyId);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];
                        $elTot[] = $amounts
                        ?><!--Fixed Assets 0-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <td onclick="BSDrill(1,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[0], 4) ?></td>
                            <td onclick="BSDrill(2,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[1], 4) ?></td>
                            <td onclick="BSDrill(3,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[2], 4) ?></td>
                            <td onclick="BSDrill(4,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[3], 4) ?></td>
                            <td onclick="BSDrill(5,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[4], 4) ?></td>
                            <td onclick="BSDrill(6,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[5], 4) ?></td>
                            <td onclick="BSDrill(7,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[6], 4) ?></td>
                            <td onclick="BSDrill(8,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[7], 4) ?></td>
                            <td onclick="BSDrill(9,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[8], 4) ?></td>
                            <td onclick="BSDrill(10,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[9], 4) ?></td>
                            <td onclick="BSDrill(11,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[10], 4) ?></td>
                            <td onclick="BSDrill(12,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[11], 4) ?></td>
                            <td><?php echo number_format($arr[3], 4) ?></td>
                        </tr>
                    <?php } ?> 
                        
                   
                    <?php
                    $detailsArr = getAmounts($accPeriod, 7, $companyId);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];
                        $taxTot[] = $amounts
                        ?><!--Fixed Assets 0-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <td onclick="BSDrill(1,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[0], 4) ?></td>
                            <td onclick="BSDrill(2,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[1], 4) ?></td>
                            <td onclick="BSDrill(3,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[2], 4) ?></td>
                            <td onclick="BSDrill(4,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[3], 4) ?></td>
                            <td onclick="BSDrill(5,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[4], 4) ?></td>
                            <td onclick="BSDrill(6,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[5], 4) ?></td>
                            <td onclick="BSDrill(7,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[6], 4) ?></td>
                            <td onclick="BSDrill(8,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[7], 4) ?></td>
                            <td onclick="BSDrill(9,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[8], 4) ?></td>
                            <td onclick="BSDrill(10,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[9], 4) ?></td>
                            <td onclick="BSDrill(11,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[10], 4) ?></td>
                            <td onclick="BSDrill(12,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[11], 4) ?></td>
                            <td><?php echo number_format($arr[3], 4) ?></td>
                        </tr>
                    <?php } ?> 
                        
                    <?php
                    $detailsArr = getAmounts($accPeriod, 30, $companyId);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];
                        $uregTot[] = $amounts
                        ?><!--Unrealize Ex. Gain-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <td onclick="BSDrill(1,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[0], 4) ?></td>
                            <td onclick="BSDrill(2,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[1], 4) ?></td>
                            <td onclick="BSDrill(3,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[2], 4) ?></td>
                            <td onclick="BSDrill(4,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[3], 4) ?></td>
                            <td onclick="BSDrill(5,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[4], 4) ?></td>
                            <td onclick="BSDrill(6,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[5], 4) ?></td>
                            <td onclick="BSDrill(7,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[6], 4) ?></td>
                            <td onclick="BSDrill(8,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[7], 4) ?></td>
                            <td onclick="BSDrill(9,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[8], 4) ?></td>
                            <td onclick="BSDrill(10,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[9], 4) ?></td>
                            <td onclick="BSDrill(11,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[10], 4) ?></td>
                            <td onclick="BSDrill(12,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[11], 4) ?></td>
                            <td><?php echo number_format($arr[3], 4) ?></td>
                        </tr>
                    <?php } ?> 
                        
                    <?php
                    $detailsArr = getAmounts($accPeriod, 31, $companyId);
                    foreach ($detailsArr as $arr) {
                        $amounts = $arr[1];
                        $urelTot[] = $amounts
                        ?><!--Unrealize Ex.  Loss-->
                        <tr class="figurs">
                            <td><?php echo $arr[0] ?></td>
                            <td onclick="BSDrill(1,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[0], 4) ?></td>
                            <td onclick="BSDrill(2,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[1], 4) ?></td>
                            <td onclick="BSDrill(3,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[2], 4) ?></td>
                            <td onclick="BSDrill(4,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[3], 4) ?></td>
                            <td onclick="BSDrill(5,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[4], 4) ?></td>
                            <td onclick="BSDrill(6,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[5], 4) ?></td>
                            <td onclick="BSDrill(7,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[6], 4) ?></td>
                            <td onclick="BSDrill(8,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[7], 4) ?></td>
                            <td onclick="BSDrill(9,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[8], 4) ?></td>
                            <td onclick="BSDrill(10,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[9], 4) ?></td>
                            <td onclick="BSDrill(11,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[10], 4) ?></td>
                            <td onclick="BSDrill(12,'<?php echo $arr[2] ?>');"><?php echo number_format($amounts[11], 4) ?></td>
                            <td><?php echo number_format($arr[3], 4) ?></td>
                        </tr>
                    <?php } ?> 
                    
                    <?php $total = getNetProfit($revTot,$cosTot,$copTot,$oiTot,$egTot,$exTot,$elTot,$taxTot,$uregTot,$urelTot)?>
                    <tr class="total">
                        <td>Net Profit/(Loss)</td>
                        <td><?php echo number_format($total[0], 4) ?></td>
                        <td><?php echo number_format($total[1], 4) ?></td>
                        <td><?php echo number_format($total[2], 4) ?></td>
                        <td><?php echo number_format($total[3], 4) ?></td>
                        <td><?php echo number_format($total[4], 4) ?></td>
                        <td><?php echo number_format($total[5], 4) ?></td>
                        <td><?php echo number_format($total[6], 4) ?></td>
                        <td><?php echo number_format($total[7], 4) ?></td>
                        <td><?php echo number_format($total[8], 4) ?></td>
                        <td><?php echo number_format($total[9], 4) ?></td>
                        <td><?php echo number_format($total[10], 4) ?></td>
                        <td><?php echo number_format($total[11], 4) ?></td>
                        <td><?php echo number_format($total[12], 4) ?></td>
                    </tr>
                </table><!-- main table -->
            </div>
        </form>
    </body>
</html>
<?php

function getDates($startDate, $endDate) {
    $start = $month = strtotime($startDate);
    $end = strtotime($endDate);
    while ($month < $end) {
        $months[] = date('F', $month);
        $month = strtotime("+1 month", $month);
    }
    return $months;
}
?>
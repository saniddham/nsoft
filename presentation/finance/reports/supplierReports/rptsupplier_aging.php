<?php
session_start();

//BEGIN - SET PARAMETERS	{
$backwardseperator = "../../../../";
$companyId 			= $_SESSION['headCompanyId'];
$location 			= $_SESSION['CompanyID'];
$intUser 			= $_SESSION["userId"];

$cboSupplier		= $_REQUEST["cboSupplier"];
if(!isset($_REQUEST["cboCurrency"]))
	$cboTargetCurrency		= 1;
else
	$cboTargetCurrency		= $_REQUEST["cboCurrency"];

//BEGIN - INCLUDE FILES {
include  	"{$backwardseperator}dataAccess/Connector.php";
//END 	- INCLUDE FILES }
$payment_term_array	= GetPaymentTerm();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Supplier Invoice Aging Report</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>
<script type="application/javascript" src="rptsupplier_aging.js"></script>
<script type="application/javascript">
function Reload()
{
	document.frmSupplierAgingReport.submit();	
}
</script>
</head>
<body>
<form id="frmSupplierAgingReport" name="frmSupplierAgingReport" method="post" action="rptsupplier_aging.php">
  <div align="center">
  <div style="background-color:#FFF" ><strong>Supplier Invoice Aging Report</strong><strong></strong></div>
<table width="1100" border="0" align="center" bgcolor="#FFFFFF">
 <tr>
   <td><table width="464" border="0" class="normalfnt">
     <tr>
       <td width="223" style="text-align:center">Supplier</td>
       <td width="231" style="text-align:center">Currency</td>
       </tr>
     <tr>
       <td style="text-align:center"><select style="width:200px" name="cboSupplier" id="cboSupplier" onchange="Reload();">
       <option value="">ALL</option>
		<?php
		$result = GetSupplier();
		while($row = mysqli_fetch_array($result))
		{
			if($cboSupplier==$row["SUPPLIER_ID"])
				echo "<option value=\"".$row["SUPPLIER_ID"]."\" selected=\"selected\">".$row["SUPPLIER_NAME"]."</option>";
			else
				echo "<option value=\"".$row["SUPPLIER_ID"]."\">".$row["SUPPLIER_NAME"]."</option>";
		}
		?>
         </select></td>
       <td style="text-align:center"><select style="width:200px" name="cboCurrency" id="cboCurrency" onchange="Reload();">
		<?php
		$result = GetCurrency();
		while($row = mysqli_fetch_array($result))
		{
			if($cboTargetCurrency==$row["intId"])
				echo "<option value=\"".$row["intId"]."\" selected=\"selected\">".$row["strCode"]."</option>";
			else
				echo "<option value=\"".$row["intId"]."\">".$row["strCode"]."</option>";
		}
		?>
       </select></td>
       </tr>
   </table></td>
 </tr>
 <tr>
  <td><table  border="0" cellpadding="0" cellspacing="0" class="bordered" width="100%">
    <thead>
      <tr valign="bottom">
        <th width="2%">&nbsp;</th>
        <th width="34%">&nbsp;</th>
        <th width="6%">Credit Term</th>
        <th width="22%" colspan="<?php echo count($payment_term_array)?>">Invoice</th>
        </tr>
      <tr valign="bottom">
        <th>No</th>
        <th>Supplier Name</th>
        <th>&nbsp;</th>        
	    <?php for($i=0;$i<count($payment_term_array);$i++){?>
        <?php }?>
         <?php for($i=0;$i<count($payment_term_array);$i++){?>
         <th style="width:50px"><?php echo $payment_term_array[$i][1]?></th>
        <?php }?>
        </tr>
    </thead>
     <tbody>
<?php
$supplier_id ='';
$count	= 0;
$booAvi	= true;
$result = GetMainDetails();
while($row = mysqli_fetch_array($result))
{
	$final_value	= ConvertWithExchangeRates($row["FINAL_VALUE"],$row["PO_DATE"],$row["PO_SOURCE_CURRENCY"],$cboTargetCurrency);
?>

<?php
	if(!$booAvi && $row["SUPPLIER_ID"]!=$supplier_id)
	{
		$booAvi	= true;
		for($i=0;$i<count($payment_term_array);$i++)
		{
?>
        <td nowrap="nowrap" style="text-align:right;color:<?php echo $payment_term_array[$i][2]?>"><?php echo $aging_array[$payment_term_array[$i][0]]=='0'?'&nbsp;':number_format($aging_array[$payment_term_array[$i][0]])?></td>
<?php 	
		}
		$aging_array = array();
		echo " </tr>";
	}
?>

<?php
	if($row["SUPPLIER_ID"]!=$supplier_id)
	{
		$booAvi = false;
?>	
      <tr>
        <td nowrap="nowrap"><?php echo ++$count?>.</td>
        <td nowrap="nowrap"><a class="mouseover cls_td_supplier_name" style="text-decoration:underline;color:#00F" id="<?php echo $row["SUPPLIER_ID"]?>"><?php echo $row["SUPPLIER_NAME"]?></a></td>
        <td nowrap="nowrap"><?php echo $row["CREDIT_DAYS"]?> Days</td>        
<?php 
	}
	$supplier_id = $row["SUPPLIER_ID"];
	
	$start = 0;
	$boo	= true;
	for($i=0;$i<count($payment_term_array);$i++)
	{
		$diff_date 	= $row["GAP"];	
		$aging = 0;
		
		if($boo){
			if($diff_date<0){
				$aging = round($final_value);
				$boo	= false;
			}
		}
		
		if($boo){			
			if($start < $diff_date && $payment_term_array[$i][0] >= $diff_date)
			{
				$aging = round($final_value);		
			}
			else 
				$aging = 0;
		}
		$aging_array[$payment_term_array[$i][0]] += $aging;			
		$total_array[$payment_term_array[$i][0]] += $aging;
		$start = $payment_term_array[$i][0];
	}
?>
      
<?php	
}
?>

<?php
	if(!$booAvi && $row["SUPPLIER_ID"]!=$supplier_id)
	{
		$booAvi	= true;
		for($i=0;$i<count($payment_term_array);$i++)
		{
?>
        <td nowrap="nowrap" style="text-align:right;color:<?php echo $payment_term_array[$i][2]?>"><?php echo $aging_array[$payment_term_array[$i][0]]=='0'?'&nbsp;':number_format($aging_array[$payment_term_array[$i][0]])?></td>
<?php 	
		}
		$aging_array = array();
		echo " </tr>";
	}
?>

</tbody>
<tfoot>
	    <tr style="font-weight:bold">
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap" style="text-align:center">Total</td>
        <td nowrap="nowrap">&nbsp;</td>
        <?php for($i=0;$i<count($payment_term_array);$i++){?>
        <td nowrap="nowrap" style="text-align:right"><?php echo number_format($total_array[$payment_term_array[$i][0]])?></td>
        <?php }?>
       </tr>
</tfoot>	
  </table></td>
</tr>
            <tr>
<tr height="40">
  <td align="center" class="normalfntMid" ><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>
</form>
	<div style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1"></div>
	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
</body>
<script type="text/javascript">
$(document).ready(function()
{
	$('.bordered tbody tr').click(function(){
		if($(this).attr('style')!='background-color:#82FF82')
		{
			$('.bordered tbody tr').attr('style','background-color:#FFFFFF');  
			   $(this).attr('style','background-color:#82FF82');     
		}                                             
	})
});
</script>
</html>
<?php
function GetMainDetails()
{
	global $db;
	global $cboSupplier;

	if($cboSupplier!="")
		$where_sql	= "AND S.intId = $cboSupplier ";
		
	$sql = "SELECT 
				S.strName											AS SUPPLIER_NAME,
				S.intId												AS SUPPLIER_ID,
				PT.strName											AS CREDIT_DAYS,
				DATEDIFF(NOW(),PH.dtmPODate)						AS DATE_DIFF,
				PT.strName - DATEDIFF(NOW(),PH.dtmPODate)			AS GAP ,
				CONCAT(PH.intPONo,'/',PH.intPOYear) 				AS PO_NO,
				PH.intCurrency										AS PO_SOURCE_CURRENCY,
				PH.dtmPODate										AS PO_DATE,
			
				ROUND(COALESCE(SUM(PD.dblUnitPrice * dblQty),0),2) 	AS PO_AMOUNT,			
		
				COALESCE((SELECT SUM(dblPayAmount)
				FROM fin_supplier_payments_header SPH
				INNER JOIN fin_supplier_payments_main_details SPD
					ON SPD.intReceiptNo = SPH.intReceiptNo
					AND SPD.intAccPeriodId = SPH.intAccPeriodId
					AND SPD.intLocationId = SPH.intLocationId
					AND SPD.intCompanyId = SPH.intCompanyId
					AND SPD.strReferenceNo = SPH.strReferenceNo
				WHERE SPD.strPoNo = CONCAT(PH.intPONo,'/',PH.intPOYear)
					AND SPH.intDeleteStatus = 0),0)AS PAYMENT_RECEIVED,
					
				(ROUND(COALESCE(SUM(PD.dblUnitPrice * dblQty),0),2) - COALESCE((SELECT SUM(dblPayAmount)
				FROM fin_supplier_payments_header SPH
				INNER JOIN fin_supplier_payments_main_details SPD
					ON SPD.intReceiptNo = SPH.intReceiptNo
					AND SPD.intAccPeriodId = SPH.intAccPeriodId
					AND SPD.intLocationId = SPH.intLocationId
					AND SPD.intCompanyId = SPH.intCompanyId
					AND SPD.strReferenceNo = SPH.strReferenceNo
				WHERE SPD.strPoNo = CONCAT(PH.intPONo,'/',PH.intPOYear)
					AND SPH.intDeleteStatus = 0),0))					AS FINAL_VALUE
				
		FROM trn_poheader PH
		  INNER JOIN trn_podetails PD ON PD.intPONo = PH.intPONo AND PD.intPOYear = PH.intPOYear
		  INNER JOIN mst_financepaymentsterms PT ON PT.intId  = PH.intPaymentTerm
		  INNER JOIN mst_supplier S ON S.intId = PH.intSupplier
		WHERE PH.intStatus = 1
			$where_sql
		-- AND S.intId IN (86,103,57,483)
		 -- and concat(PH.intPONo,'/',PH.intPOYear) = '400129/2013'
		GROUP BY PH.intPOYear,PH.intPONo
		ORDER BY S.strName";
	return $db->RunQuery($sql);
}
function GetPaymentTerm()
{
	global $db;
	$array	= array();
	$array[0][0]	= "-1";
	$array[0][1]	= "Exceed Credit Days";
	$array[0][2]	= "#FF0000";
		
	$i		= 1;
	
	$sql = "SELECT PT.strName AS NAME,PT.strRange AS RANGES FROM mst_financepaymentsterms PT WHERE PT.strName <> 0 ORDER BY PT.strName";
	$result = $db->RunQuery($sql);
	while($row = mysqli_fetch_array($result))
	{
		$array[$i][0]	= $row["NAME"];
		$array[$i][1]	= $row["RANGES"];
		$array[$i][2]	= "#000000";
		$i++;
	}
	return $array;
}

function ConvertWithExchangeRates($final_value,$po_date,$po_source_currency,$target_currency)
{
	global $db;
	
	$sql 	= "SELECT dblSellingRate FROM mst_financeexchangerate WHERE dtmDate = '$po_date' AND intCurrencyId = $po_source_currency AND intCompanyId =1";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	$sourceExRate	= $row["dblSellingRate"];
	
	$sql 	= "SELECT dblSellingRate FROM mst_financeexchangerate WHERE dtmDate = '$po_date' AND intCurrencyId = $target_currency AND intCompanyId =1";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	$targetExRate	= $row["dblSellingRate"];
	
	$value = ($final_value/$targetExRate)*$sourceExRate;
	return $value;
}

function GetSupplier()
{
	global $db;
	
	$sql = "SELECT DISTINCT S.intId AS SUPPLIER_ID,S.strName AS SUPPLIER_NAME FROM mst_supplier S";
	
	/*$sql = "SELECT DISTINCT
			S.intId												AS SUPPLIER_ID,
			S.strName											AS SUPPLIER_NAME
				FROM trn_poheader PH
				  INNER JOIN trn_podetails PD ON PD.intPONo = PH.intPONo AND PD.intPOYear = PH.intPOYear
				  INNER JOIN mst_financepaymentsterms PT ON PT.intId  = PH.intPaymentTerm
				  INNER JOIN mst_supplier S ON S.intId = PH.intSupplier
				WHERE PH.intStatus = 1 
				GROUP BY PH.intPOYear,PH.intPONo
				HAVING (	(ROUND(COALESCE(SUM(PD.dblUnitPrice * dblQty),0),2) - COALESCE((SELECT SUM(dblPayAmount)
			FROM fin_supplier_payments_header SPH
			INNER JOIN fin_supplier_payments_main_details SPD
			ON SPD.intReceiptNo = SPH.intReceiptNo
			AND SPD.intAccPeriodId = SPH.intAccPeriodId
			AND SPD.intLocationId = SPH.intLocationId
			AND SPD.intCompanyId = SPH.intCompanyId
			AND SPD.strReferenceNo = SPH.strReferenceNo
			WHERE SPD.strPoNo = CONCAT(PH.intPONo,'/',PH.intPOYear)
			AND SPH.intDeleteStatus = 0),0)))
				ORDER BY S.strName";*/
	return $db->RunQuery($sql);
}

function GetCurrency()
{
	global $db;
	
	$sql = "SELECT DISTINCT intId , strCode FROM mst_financecurrency  WHERE intStatus = 1 ORDER BY intId";
	return $db->RunQuery($sql);
}
?>
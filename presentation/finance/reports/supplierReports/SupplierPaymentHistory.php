<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];

$thisFilePath = $_SERVER['PHP_SELF'];

include "{$backwardseperator}dataAccess/permisionCheck.inc";
$companyId = $_SESSION['headCompanyId'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Supplier Payments History</title>
        <link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
         <link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
        <script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
        <link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css" />
        <link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css" />
        <script type="application/javascript" src="SupplierPaymentHistory.js"></script>
        
        <style>
           table.custTable{
                font-family: Verdana;
                font-size: 11px;
                color: #000000;
                margin: 0px;
                font-weight: normal;
                text-align:left;
                border-collapse:collapse; 
                width: 100%;
            }
            table.custTable tr td{
                border:1px solid #ff7200;
            }
            
        </style>

    </head>

    <body>
        <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
                <td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator . 'Header.php'; ?></td>
            </tr> 
        </table>
        <form id="frmSuptPayHistory" name="frmSuptPayHistory"  method="get" autocomplete="off"  >
            <div align="center">
                <div class="trans_layoutD">
                    <div class="trans_text">Supplier Payments History</div>
                    <table>
                        <tr>
                            <td class="normalfnt" >From</td>
                            <td class="normalfnt" ><input name="txtFrom" type="text" <?php if ($date) { ?> value="<?php echo $date; ?>" <?php } else { ?> value="<?php echo date("Y-m-d"); ?>" <?php } ?>  class="validate[required] txtbox normalfnt" id="txtFrom" style="width:98px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" readonly="readonly"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                            <td class="normalfnt" >To</td>
                            <td class="normalfnt" ><input name="txtTo" type="text" <?php if ($date) { ?> value="<?php echo $date; ?>" <?php } else { ?> value="<?php echo date("Y-m-d"); ?>" <?php } ?>  class="validate[required] txtbox normalfnt" id="txtTo" style="width:98px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" readonly="readonly"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>                        
                        </tr>              
                        <tr>
                            <td class="normalfnt" colspan="4">
                                <div id="divTable1" style="overflow:scroll;width:100%;height:400px"  align="center">
                                <table class="custTable">
                                    <tr>
                                        <td class="normalfnt"><input type="checkbox" name="chkAllSup" id="chkAllSup" onclick="hi()" /></td>
                                        <td class="normalfntMid">Suppliers</td>
                                    </tr>
                                    <?php
                                    $sql = "SELECT  intId,strName FROM mst_supplier WHERE intStatus = 1 order by strName";
                                    $result = $db->RunQuery($sql);
                                    while ($row = mysqli_fetch_array($result)) {
                                        echo "<tr>";
                                        echo "<td><input type=\"checkbox\" name=\"chkSupplier\" id=\"chkSupplier\" value=\"".$row['intId']."\"  /></td>";
                                        echo "<td>".$row['strName']."</td>";
                                        echo "</tr>";
                                    }
                                    ?>
                                    
                                </table> 
                                </div>
                            </td>
                        </tr>
                        
                        <tr>
                            <td colspan="4" align="center" bgcolor=""><img onclick="showReport()" border="0" src="../../../../images/Treport.jpg" alt="Summery Report" name="butReport"width="92" height="24"  class="mouseover" id="butReport" tabindex="24"/><a href="../../../../main.php"><img  src="../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                        </tr>
                    </table>
                </div>
                </div>
        </form>
    </body>
</html>
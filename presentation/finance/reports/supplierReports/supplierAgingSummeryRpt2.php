<?php
session_start();
$backwardseperator = "../../../../";
$companyId = $_SESSION['headCompanyId'];
$location = $_SESSION['CompanyID'];
$intUser = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath = $_SERVER['PHP_SELF'];
include "{$backwardseperator}dataAccess/Connector.php";
include 'SupplierAgingFunctions2.php';

$locationId = $location; //this locationId use in report header(reportHeader.php)--------------------
//--------------------------------------------------------------	
$toDay = $_REQUEST['toDay'];
$fromDay = $_REQUEST['fromDay'];
$currency = $_REQUEST['currencyId'];
$filterCompany = $_REQUEST['companyId'];

//-------------------------
$sql = "SELECT intId,strCode FROM mst_financecurrency WHERE intId = '$currency'";
$result = $db->RunQuery($sql);
$row = mysqli_fetch_array($result);
$currencyDesc = $row['strCode'];
//-------------------------
$sql = "SELECT mst_companies.strName FROM mst_companies WHERE mst_companies.intId =  '$filterCompany'";
$result = $db->RunQuery($sql);
$row = mysqli_fetch_array($result);
$filterCompName = $row['strName'];
//-------------------------
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Supplier Aging Summery</title>
        <link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/promt.css" rel="stylesheet" type="text/css" /> 
        <script>
            function showDetails(supId,currency,company,toDay){                
                window.open("SupplierAgingDetails.php?supId="+supId+"&currency="+currency+"&company="+company+"&toDay="+toDay);
            }
        </script>
        <style>           
            .customerCss{
                cursor: pointer;
            }
        </style>
    </head>
    <body>
        <form id="frmGRNApprovalReport" name="frmGRNApprovalReport" method="post" action="customerBalance_summeryRpt.php">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td colspan="3"></td>
                </tr>
                <tr>
                    <td width="20%"></td>
                    <td width="60%" height="80" valign="top"><?php include '../../reportHeader.php' ?></td>
                    <td width="20%"></td>
                </tr>

                <tr>
                    <td colspan="3"></td>
                </tr>
            </table>
            <div align="center">
                <div style="background-color:#FFF" ><strong>Supplier Aging Summery AS AT <?php echo $toDay; ?></strong><strong></strong></div>
                <table width="1100" border="0" align="center" bgcolor="#FFFFFF">
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <th></th>
                                    <th width="6%" class="normalfnt"><strong>Currency:</strong></th>
                                    <th width="16%" align="left" class="normalfnt"> <?php echo $currencyDesc ?> </th>
                                    <?php
                                    if ($filterCompany == 0) {
                                        ?>
                                        <td width="8%" class="normalfnt"><strong>All Groups</strong></td>
                                        <?php
                                    } else {
                                        ?>
                                        <td width="49%" class="normalfnt"><strong>Company :</strong><?php echo $filterCompName ?></td>
                                        <?php
                                    }
                                    ?>
                                    <th width="2%" class="normalfnt"><strong></strong></th>
                                    <th width="2%" class="normalfnt"><strong></strong></th>
                                    <th width="6%" class="normalfnt"><strong></strong></th>
                                    <th width="8%" class="normalfnt"><strong></strong></th>
                                </tr>
                                <tr>
                                    <td width="3%">&nbsp;</td>
                                    <td colspan="7" class="normalfnt">
                                        <table  border="1" cellpadding="2" cellspacing="0" align="center" class="main" width="100%">
                                            <thead>
                                                <tr>
                                                    <th colspan="2"></th>
                                                    <th align="center">Debit Note</th>
                                                    <th align="center" colspan="4">Advanced Payments</th>
                                                    <th align="center" colspan="5">Invoice</th>
                                                    <th></th>
                                                </tr>
                                                <tr>
                                                    <th></th>
                                                    <th><strong>Supplier</strong></th>
                                                    <th><strong>Current</strong></th>                                                    
                                                    <th><strong>0 - 30 Days</strong></th>
                                                    <th><strong>31 - 60 Days </strong></th>
                                                    <th><strong>61 - 90 Days</strong></th>
                                                    <th><strong>More Than<br/>90 Days</strong></th> 
                                                    <th><strong>Current</strong></th>
                                                    <th><strong>0 - 30 Days</strong></th>
                                                    <th><strong>31 - 60 Days </strong></th>
                                                    <th><strong>61 - 90 Days</strong></th>
                                                    <th><strong>More Than<br/>90 Days</strong></th>
                                                    <th><strong>To Be Paid<br/>(Total)</strong></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $c = 0;
                                                $sql1 = "SELECT intId,strName FROM mst_supplier where intStatus = '1' order by strName";
                                                $result1 = $db->RunQuery($sql1);
                                                $creditNoteBalTot=0;
                                                $advl30;
                                                $advl60;
                                                $advl90;
                                                $advl91;
                                                $invtot30;
                                                $invtot60;
                                                $invtot90;
                                                $invtot91;
                                                $invctrtot;
                                                $totTobe;
                                                while ($rowAll = mysqli_fetch_array($result1)) {
                                                    $supId = $rowAll['intId'];
                                                    $c++;
                                                    $totlaToBe = 0;
                                                    
                                                ?>
                                                    <tr>
                                                        <td><?php echo $c; ?></td>
                                                        <td class="customerCss" onclick="showDetails('<?php echo $supId?>','<?php echo $currency?>','<?php echo $filterCompany?>','<?php echo $toDay?>')"><?php echo $rowAll['strName']?></td>                                                     
                                                        
                                                        <?PHP
                                                        //get debit notes details
                                                        $creditNoteBal=0;
                                                        
                                                        try {
                                                            if($filterCompany==0) $ware='';
                                                            else $ware=" AND fin_supplier_debitnote_header.intCompanyId=$filterCompany";
                                                            
                                                            $sql="SELECT
                                                                    (
                                                                        SUM(((CRN.dblQty*CRN.dblRate) *(100-CRN.dblDiscount)/100)+ IFNULL(CRN.dblTax,0))
                                                                        +
                                                                        IFNULL ((SELECT
                                                                        Sum(fin_supplier_payments_main_details.dblPayAmount )AS paidAmount
																		FROM
																		fin_supplier_payments_main_details
																		Inner Join fin_supplier_payments_header ON fin_supplier_payments_main_details.strReferenceNo = fin_supplier_payments_header.strReferenceNo
																		Inner Join fin_supplier_debitnote_header ON fin_supplier_debitnote_header.strDebitNoteNo = fin_supplier_payments_main_details.strDocNo
																		WHERE
																		fin_supplier_payments_header.intDeleteStatus =  '0' AND 		fin_supplier_payments_header.dtmDate<='$toDay' AND fin_supplier_payments_header.dtmDate>='$fromDay' AND 	fin_supplier_payments_main_details.strDocType = 'D.Note' AND fin_supplier_debitnote_header.intSupplier =  '$supId' AND fin_supplier_payments_main_details.strDocNo =  CRN.strDebitNoteNo),0)
                                                                        ) AS balAmount
                                                                        FROM
                                                                        fin_supplier_debitnote_details AS CRN
                                                                        Inner Join fin_supplier_debitnote_header ON CRN.strDebitNoteNo = fin_supplier_debitnote_header.strDebitNoteNo				
                                                                        WHERE
                                                                        fin_supplier_debitnote_header.intsupplier =  '$supId' AND
                                                                        fin_supplier_debitnote_header.intStatus = '1' AND
                                                                        fin_supplier_debitnote_header.dtDate <='$toDay' AND
																		fin_supplier_debitnote_header.dtDate >='$fromDay' AND
                                                                        fin_supplier_debitnote_header.intCurrency=$currency ".$ware;
                                                            $result = $db->RunQuery($sql);
                                                            $row = mysqli_fetch_array($result);
                                                            $creditNoteBal=$row['balAmount'] * (-1);
                                                            $totlaToBe+=$creditNoteBal;
                                                            $creditNoteBalTot+=$creditNoteBal;
//                                                            //Advanced Payments
                                                            if($filterCompany==0) $ware='';
                                                            else $ware=" AND ADV.intCompanyId=$filterCompany";
                                                            // 0-30
                                                            $l30;                                                            
                                                            $sql="SELECT
                                                                    (
                                                                    SUM(ADV.dblReceivedAmount)
                                                                    +
                                                                    IFNULL ((SELECT
																	Sum(fin_supplier_payments_main_details.dblPayAmount )AS paidAmount
																	FROM
																	fin_supplier_payments_main_details
																	Inner Join fin_supplier_payments_header ON fin_supplier_payments_main_details.strReferenceNo = fin_supplier_payments_header.strReferenceNo
																	Inner Join fin_supplier_advancepayment_header ON fin_supplier_payments_main_details.strDocNo = fin_supplier_advancepayment_header.strReceiptNo
																	WHERE
																	fin_supplier_payments_header.intDeleteStatus =  '0' AND  
																	fin_supplier_payments_main_details.strDocType = 'A.Payment' AND 
																	fin_supplier_payments_header.dtmDate<='$toDay' AND
																	fin_supplier_payments_header.dtmDate>='$fromDay' AND
																	 fin_supplier_advancepayment_header.intSupplier = '$supId' AND fin_supplier_payments_main_details.strDocNo =  ADV.strReceiptNo),0)

                                                                    ) AS balAmount
                                                                    FROM
                                                                    fin_supplier_advancepayment_header AS ADV
                                                                    Inner Join mst_financecurrency ON ADV.intCurrency = mst_financecurrency.intId
                                                                    WHERE
                                                                    ADV.intsupplier =  '$supId' AND
                                                                    ADV.intStatus = '1' AND
                                                                    ADV.intCurrency=$currency AND 
                                                                    ADV.dtDate<='$toDay' AND
																	ADV.dtDate>='$fromDay' AND
                                                                    DATEDIFF('$toDay',ADV.dtDate) <=30".$ware; 
                                                            $result = $db->RunQuery($sql);
                                                            
                                                            $row = mysqli_fetch_array($result);
                                                            $l30=$row['balAmount'] * (-1);
                                                            $totlaToBe+=$l30;
                                                            $advl30+=$l30;
                                                            // 31-60
                                                            $l60;
                                                            $sql="SELECT
                                                                    (
                                                                    SUM(ADV.dblReceivedAmount)
                                                                    +
                                                                    IFNULL ((SELECT
																	Sum(fin_supplier_payments_main_details.dblPayAmount )AS paidAmount
																	FROM
																	fin_supplier_payments_main_details
																	Inner Join fin_supplier_payments_header ON fin_supplier_payments_main_details.strReferenceNo = fin_supplier_payments_header.strReferenceNo
																	Inner Join fin_supplier_advancepayment_header ON fin_supplier_payments_main_details.strDocNo = fin_supplier_advancepayment_header.strReceiptNo
																	WHERE
																	fin_supplier_payments_header.intDeleteStatus =  '0' AND  
																	fin_supplier_payments_main_details.strDocType = 'A.Payment' AND 
																	fin_supplier_payments_header.dtmDate<='$toDay' AND
																	fin_supplier_payments_header.dtmDate>='$fromDay' AND fin_supplier_advancepayment_header.intSupplier = '$supId' AND fin_supplier_payments_main_details.strDocNo =  ADV.strReceiptNo),0)

                                                                    ) AS balAmount
                                                                    FROM
                                                                    fin_supplier_advancepayment_header AS ADV
                                                                    Inner Join mst_financecurrency ON ADV.intCurrency = mst_financecurrency.intId
                                                                    WHERE
                                                                    ADV.intsupplier =  '$supId' AND
                                                                    ADV.intStatus = '1' AND
                                                                    ADV.intCurrency=$currency AND 
                                                                    ADV.dtDate<='$toDay' AND
																	ADV.dtDate>='$fromDay' AND
                                                                    DATEDIFF('$toDay',ADV.dtDate) <=60 AND
                                                                    DATEDIFF('$toDay',ADV.dtDate) >=31".$ware;                                                       
                                                            $result = $db->RunQuery($sql);
                                                            $row = mysqli_fetch_array($result);
                                                            $l60=$row['balAmount']* (-1);
                                                            $totlaToBe+=$l60;
                                                            $advl60+=$l60;
//                                                            // 60-90
                                                            $l90;
                                                            $sql="SELECT
                                                                    (
                                                                    SUM(ADV.dblReceivedAmount)
                                                                    +
                                                                    IFNULL ((SELECT
																	Sum(fin_supplier_payments_main_details.dblPayAmount )AS paidAmount
																	FROM
																	fin_supplier_payments_main_details
																	Inner Join fin_supplier_payments_header ON fin_supplier_payments_main_details.strReferenceNo = fin_supplier_payments_header.strReferenceNo
																	Inner Join fin_supplier_advancepayment_header ON fin_supplier_payments_main_details.strDocNo = fin_supplier_advancepayment_header.strReceiptNo
																	WHERE
																	fin_supplier_payments_header.intDeleteStatus =  '0' AND  
																	fin_supplier_payments_main_details.strDocType = 'A.Payment' AND 
																	fin_supplier_payments_header.dtmDate<='$toDay' AND
																	fin_supplier_payments_header.dtmDate>='$fromDay' AND fin_supplier_advancepayment_header.intSupplier = '$supId' AND fin_supplier_payments_main_details.strDocNo =  ADV.strReceiptNo),0)

                                                                    ) AS balAmount
                                                                    FROM
                                                                    fin_supplier_advancepayment_header AS ADV
                                                                    Inner Join mst_financecurrency ON ADV.intCurrency = mst_financecurrency.intId
                                                                    WHERE
                                                                    ADV.intsupplier =  '$supId' AND
                                                                    ADV.intStatus = '1' AND
                                                                    ADV.intCurrency=$currency AND 
                                                                    ADV.dtDate<='$toDay' AND
																	ADV.dtDate>='$fromDay' AND
                                                                    DATEDIFF('$toDay',ADV.dtDate) <=90 AND
                                                                    DATEDIFF('$toDay',ADV.dtDate) >=61".$ware;
                                                            $result = $db->RunQuery($sql);
                                                            
                                                            $row = mysqli_fetch_array($result);
                                                            $l90=$row['balAmount']* (-1);
                                                            $totlaToBe+=$l90;
                                                            $advl90=$l90;
//                                                            // 91 >
                                                            $g91;
                                                            $sql="SELECT
                                                                    (
                                                                    SUM(ADV.dblReceivedAmount)
                                                                    +
                                                                    IFNULL ((SELECT
																	Sum(fin_supplier_payments_main_details.dblPayAmount )AS paidAmount
																	FROM
																	fin_supplier_payments_main_details
																	Inner Join fin_supplier_payments_header ON fin_supplier_payments_main_details.strReferenceNo = fin_supplier_payments_header.strReferenceNo
																	Inner Join fin_supplier_advancepayment_header ON fin_supplier_payments_main_details.strDocNo = fin_supplier_advancepayment_header.strReceiptNo
																	WHERE
																	fin_supplier_payments_header.intDeleteStatus =  '0' AND  
																	fin_supplier_payments_main_details.strDocType = 'A.Payment' AND 
																	fin_supplier_payments_header.dtmDate<='$toDay' AND
																	fin_supplier_payments_header.dtmDate>='$fromDay' AND fin_supplier_advancepayment_header.intSupplier = '$supId' AND fin_supplier_payments_main_details.strDocNo =  ADV.strReceiptNo),0)

                                                                    ) AS balAmount
                                                                    FROM
                                                                    fin_supplier_advancepayment_header AS ADV
                                                                    Inner Join mst_financecurrency ON ADV.intCurrency = mst_financecurrency.intId
                                                                    WHERE
                                                                    ADV.intsupplier =  '$supId' AND
                                                                    ADV.intStatus = '1' AND
                                                                    ADV.intCurrency=$currency AND
                                                                    ADV.dtDate<='$toDay' AND
																	ADV.dtDate>='$fromDay' AND
                                                                    DATEDIFF('$toDay',ADV.dtDate) >=91".$ware;
                                                            $result = $db->RunQuery($sql);
                                                            
                                                            $row = mysqli_fetch_array($result);
                                                            $g91=$row['balAmount']* (-1);
                                                            $totlaToBe+=$g91;
                                                            $advl91=$g91;
                                                            // invoice agin
                                                            $invCurrent=getSupplierCurrentBalance($supId,$fromDay,$toDay,$currency,$filterCompany);
                                                            $totlaToBe+=$invCurrent;
                                                            $invctrtot+=$invCurrent;
                                                            $inv30=getSupplierBalance($supId,0,30,$fromDay,$toDay,$currency,$filterCompany);
                                                            $totlaToBe+=$inv30;
                                                            $invtot30+=$inv30;
                                                            $inv60=getSupplierBalance($supId,31,60,$fromDay,$toDay,$currency,$filterCompany);
                                                            $totlaToBe+=$inv60;
                                                            $invtot60+=$inv60;
                                                            $inv90=getSupplierBalance($supId,61,90,$fromDay,$toDay,$currency,$filterCompany);
                                                            $totlaToBe+=$inv90;
                                                            $invtot90+=$inv90;
                                                            $inv91=getSupplierBalance($supId,91,'x',$fromDay,$toDay,$currency,$filterCompany);
                                                            $totlaToBe+=$inv91;
                                                            $invtot91+=$inv91;
                                                            $totTobe+=$totlaToBe;
                                                        } catch (Exception $e) {
                                                            echo "<tr><td colspan=\"300\"><b>" . $e->getMessage() . "</b></td></tr>";
                                                        }
                                                        ?>
                                                        <td><?php echo number_format($creditNoteBal,4) ?></td>
                                                        <td><?php echo number_format($l30,4) ?></td>
                                                        <td><?php echo number_format($l60,4) ?></td>
                                                        <td><?php echo number_format($l90,4) ?></td>
                                                        <td><?php echo number_format($g91,4) ?></td>
                                                        <td><?php echo number_format($invCurrent,4) ?></td>
                                                        <td><?php echo number_format($inv30,4) ?></td>
                                                        <td><?php echo number_format($inv60,4) ?></td>
                                                        <td><?php echo number_format($inv90,4) ?></td>
                                                        <td><?php echo number_format($inv91,4) ?></td>
                                                        <td><?php echo number_format($totlaToBe,4) ?></td>
                                                <?php
                                                }
                                                ?>
                                                        <tr>
                                                            <td></td>
                                                            <td></td>
                                                            <td><?php echo number_format($creditNoteBalTot,4) ?></td>
                                                            <td><?php echo number_format($advl30,4) ?></td>
                                                            <td><?php echo number_format($advl60,4) ?></td>
                                                            <td><?php echo number_format($advl90,4) ?></td>
                                                            <td><?php echo number_format($advl91,4) ?></td>
                                                            <td><?php echo number_format($invctrtot,4) ?></td>
                                                            <td><?php echo number_format($invtot30,4) ?></td>
                                                            <td><?php echo number_format($invtot60,4) ?></td>
                                                            <td><?php echo number_format($invtot90,4) ?></td>
                                                            <td><?php echo number_format($invtot91,4) ?></td>
                                                            <td><?php echo number_format($totTobe,4) ?></td>
                                                        </tr>

                                            </tbody> 
                                        </table>
                                    </td>
                                    <td width="8%">&nbsp;</td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                    <tr>
                        <tr height="40">
                            <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
                        </tr>
                </table>
            </div>
        </form>
    </body>
</html>
<?php
function getSupplierCurrentBalance($supId,$toDay,$currency,$companyId){
    $inv=getInvoiceCurrent($supId,$toDay,$currency,$companyId);
    
    return ($inv);
}

function getSupplierCurrentBalance_ledgerWise($supId,$toDay,$currency,$companyId){
    $inv=getInvoiceCurrent_ledgerWise($supId,$toDay,$currency,$companyId);
    
    return ($inv);
}

function getSupplierBalance($supId,$from,$to,$toDay,$currency,$companyId){
    $inv=getFromInvoice($supId,$from,$to,$toDay,$currency,$companyId);
    $bankDeposit=getBankDeposit($supId,$from,$to,$toDay,$currency,$companyId);
    $bankPayment=getBankPayments($supId,$from,$to,$toDay,$currency,$companyId);
    $pattyCash=getPettyCash($supId,$from,$to,$toDay,$currency,$companyId);
    $jurnal=getJurnelEntry($supId,$from,$to,$toDay,$currency,$companyId);
    
    return ($inv+$bankDeposit+$bankPayment+$pattyCash+$jurnal);
    
}

function getSupplierBalance_ledgerWise($supId,$from,$to,$toDay,$currency,$companyId){
    $inv=getFromInvoice_ledgerWise($supId,$from,$to,$toDay,$currency,$companyId);
    $bankDeposit=getBankDeposit_ledgerWise($supId,$from,$to,$toDay,$currency,$companyId);
    $bankPayment=getBankPayments_ledgerWise($supId,$from,$to,$toDay,$currency,$companyId);
    $pattyCash=getPettyCash_ledgerWise($supId,$from,$to,$toDay,$currency,$companyId);
    $jurnal=getJurnelEntry_ledgerWise($supId,$from,$to,$toDay,$currency,$companyId);
    
    return ($inv+$bankDeposit+$bankPayment+$pattyCash+$jurnal);
    
}

function getInvoiceCurrent($supId,$toDay,$currency,$companyId){
    if($companyId==0)$wareCom=" ";
    else $wareCom=" AND fin_supplier_purchaseinvoice_header.intCompanyId=$companyId ";
    global $db;
    
     $sql="SELECT
            sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0) + IFNULL(dblBillTaxAmount,0)
            -
            IFNULL ((SELECT
            Sum(fin_supplier_payments_main_details.dblPayAmount )AS paidAmount
            FROM fin_supplier_payments_main_details
            Inner Join fin_supplier_payments_header ON fin_supplier_payments_main_details.strReferenceNo = fin_supplier_payments_header.strReferenceNo
            WHERE
            fin_supplier_payments_main_details.strDocNo =  INV.strReferenceNo AND
            fin_supplier_payments_header.intDeleteStatus =  '0' AND fin_supplier_payments_header.dtmDate<='$toDay' AND fin_supplier_payments_main_details.strDocType = 'P.Invoice'),0)

            ) AS balAmount
            FROM
            fin_supplier_purchaseinvoice_details AS INV
            Inner Join fin_supplier_purchaseinvoice_header ON fin_supplier_purchaseinvoice_header.strReferenceNo = INV.strReferenceNo AND INV.intInvoiceNo = fin_supplier_purchaseinvoice_header.intInvoiceNo AND INV.intAccPeriodId = fin_supplier_purchaseinvoice_header.intAccPeriodId AND INV.intLocationId = fin_supplier_purchaseinvoice_header.intLocationId AND INV.intCompanyId = fin_supplier_purchaseinvoice_header.intCompanyId
            Inner Join mst_supplier ON mst_supplier.intId = fin_supplier_purchaseinvoice_header.intsupplierId
            INNER JOIN mst_financepaymentsterms AS PT ON fin_supplier_purchaseinvoice_header.intPaymentsTermsId = PT.intId
            WHERE
            fin_supplier_purchaseinvoice_header.intsupplierId =  '$supId' AND
            fin_supplier_purchaseinvoice_header.intDeleteStatus =  '0' AND
            fin_supplier_purchaseinvoice_header.intCurrencyId = $currency AND
            fin_supplier_purchaseinvoice_header.dtmDate <='$toDay' AND
            DATEDIFF('$toDay',fin_supplier_purchaseinvoice_header.dtmDate) < (PT.strName) ".$wareCom.
            " GROUP BY fin_supplier_purchaseinvoice_header.intsupplierId"; 
     $result = $db->RunQuery($sql);
   
    $row = mysqli_fetch_array($result);
    return $row['balAmount'];
}

function getInvoiceCurrent_ledgerWise($supId,$toDay,$currency,$companyId){
    if($companyId==0)$wareCom=" ";
    else $wareCom=" AND fin_supplier_purchaseinvoice_header.intCompanyId=$companyId ";
    global $db;
    
     $sql="SELECT
            sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0) + IFNULL(dblBillTaxAmount,0)
            -
            IFNULL ((SELECT
            Sum(fin_supplier_payments_main_details.dblPayAmount )AS paidAmount
            FROM fin_supplier_payments_main_details
            Inner Join fin_supplier_payments_header ON fin_supplier_payments_main_details.strReferenceNo = fin_supplier_payments_header.strReferenceNo
            WHERE
            fin_supplier_payments_main_details.strDocNo =  INV.strReferenceNo AND
            fin_supplier_payments_header.intDeleteStatus =  '0' AND 
			fin_supplier_payments_header.dtmDate<='$toDay' AND 
			fin_supplier_payments_main_details.strDocType = 'P.Invoice'),0)

            ) AS balAmount
            FROM
            fin_supplier_purchaseinvoice_details AS INV
            Inner Join fin_supplier_purchaseinvoice_header ON fin_supplier_purchaseinvoice_header.strReferenceNo = INV.strReferenceNo AND INV.intInvoiceNo = fin_supplier_purchaseinvoice_header.intInvoiceNo AND INV.intAccPeriodId = fin_supplier_purchaseinvoice_header.intAccPeriodId AND INV.intLocationId = fin_supplier_purchaseinvoice_header.intLocationId AND INV.intCompanyId = fin_supplier_purchaseinvoice_header.intCompanyId
            Inner Join mst_supplier ON mst_supplier.intId = fin_supplier_purchaseinvoice_header.intsupplierId
            INNER JOIN mst_financepaymentsterms AS PT ON fin_supplier_purchaseinvoice_header.intPaymentsTermsId = PT.intId
			INNER JOIN mst_financesupplieractivate FSA ON FSA.intSupplierId = fin_supplier_purchaseinvoice_header.intsupplierId
            WHERE
            FSA.intChartOfAccountId =  '$supId' AND
            fin_supplier_purchaseinvoice_header.intDeleteStatus =  '0' AND
            fin_supplier_purchaseinvoice_header.intCurrencyId = $currency AND
            fin_supplier_purchaseinvoice_header.dtmDate <='$toDay' AND
            DATEDIFF('$toDay',fin_supplier_purchaseinvoice_header.dtmDate) < (PT.strName) ".$wareCom.
            " GROUP BY FSA.intChartOfAccountId"; 
     $result = $db->RunQuery($sql);
   
    $row = mysqli_fetch_array($result);
    return $row['balAmount'];
}

function getFromInvoice($supId,$from,$to,$toDay,$currency,$companyId){
    if($to==x)$wareTo=" ";
    else $wareTo=" AND DATEDIFF('$toDay',fin_supplier_purchaseinvoice_header.dtmDate)<=($to+ PT.strName) ";
    
    if($companyId==0)$wareCom="";
    else $wareCom=" AND fin_supplier_purchaseinvoice_header.intCompanyId=$companyId";
    global $db;
    
    $result = $db->RunQuery("SELECT
            sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0) + IFNULL(dblBillTaxAmount,0)
            -
            IFNULL ((SELECT
            Sum(fin_supplier_payments_main_details.dblPayAmount )AS paidAmount
            FROM fin_supplier_payments_main_details
            Inner Join fin_supplier_payments_header ON fin_supplier_payments_main_details.strReferenceNo = fin_supplier_payments_header.strReferenceNo
            WHERE
            fin_supplier_payments_main_details.strDocNo =  INV.strReferenceNo AND
            fin_supplier_payments_header.intDeleteStatus =  '0' AND fin_supplier_payments_header.dtmDate<='$toDay' AND fin_supplier_payments_main_details.strDocType = 'P.Invoice'),0)

            ) AS balAmount
            FROM
            fin_supplier_purchaseinvoice_details AS INV
            Inner Join fin_supplier_purchaseinvoice_header ON fin_supplier_purchaseinvoice_header.strReferenceNo = INV.strReferenceNo AND INV.intInvoiceNo = fin_supplier_purchaseinvoice_header.intInvoiceNo AND INV.intAccPeriodId = fin_supplier_purchaseinvoice_header.intAccPeriodId AND INV.intLocationId = fin_supplier_purchaseinvoice_header.intLocationId AND INV.intCompanyId = fin_supplier_purchaseinvoice_header.intCompanyId
            Inner Join mst_supplier ON mst_supplier.intId = fin_supplier_purchaseinvoice_header.intsupplierId
            INNER JOIN mst_financepaymentsterms AS PT ON fin_supplier_purchaseinvoice_header.intPaymentsTermsId = PT.intId
            WHERE
            fin_supplier_purchaseinvoice_header.intsupplierId =  '$supId' AND
            fin_supplier_purchaseinvoice_header.intDeleteStatus =  '0' AND
            fin_supplier_purchaseinvoice_header.intCurrencyId = $currency AND
            fin_supplier_purchaseinvoice_header.dtmDate<='$toDay' AND
            DATEDIFF('$toDay',fin_supplier_purchaseinvoice_header.dtmDate) >= ($from+ PT.strName) ".$wareTo.$wareCom.
            " GROUP BY fin_supplier_purchaseinvoice_header.intsupplierId"); 	
    
    $row = mysqli_fetch_array($result);
    return $row['balAmount'];    
}

function getFromInvoice_ledgerWise($supId,$from,$to,$toDay,$currency,$companyId){
    if($to==x)$wareTo=" ";
    else $wareTo=" AND DATEDIFF('$toDay',fin_supplier_purchaseinvoice_header.dtmDate)<=($to+ PT.strName) ";
    
    if($companyId==0)$wareCom="";
    else $wareCom=" AND fin_supplier_purchaseinvoice_header.intCompanyId=$companyId";
    global $db;
    
    $result = $db->RunQuery("SELECT
            sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0) + IFNULL(dblBillTaxAmount,0)
            -
            IFNULL ((SELECT
            Sum(fin_supplier_payments_main_details.dblPayAmount )AS paidAmount
            FROM fin_supplier_payments_main_details
            Inner Join fin_supplier_payments_header ON fin_supplier_payments_main_details.strReferenceNo = fin_supplier_payments_header.strReferenceNo
            WHERE
            fin_supplier_payments_main_details.strDocNo =  INV.strReferenceNo AND
            fin_supplier_payments_header.intDeleteStatus =  '0' AND fin_supplier_payments_header.dtmDate<='$toDay' AND fin_supplier_payments_main_details.strDocType = 'P.Invoice'),0)

            ) AS balAmount
            FROM
            fin_supplier_purchaseinvoice_details AS INV
            Inner Join fin_supplier_purchaseinvoice_header ON fin_supplier_purchaseinvoice_header.strReferenceNo = INV.strReferenceNo 
				AND INV.intInvoiceNo = fin_supplier_purchaseinvoice_header.intInvoiceNo 
				AND INV.intAccPeriodId = fin_supplier_purchaseinvoice_header.intAccPeriodId 
				AND INV.intLocationId = fin_supplier_purchaseinvoice_header.intLocationId 
				AND INV.intCompanyId = fin_supplier_purchaseinvoice_header.intCompanyId
            Inner Join mst_supplier ON mst_supplier.intId = fin_supplier_purchaseinvoice_header.intsupplierId
            INNER JOIN mst_financepaymentsterms AS PT ON fin_supplier_purchaseinvoice_header.intPaymentsTermsId = PT.intId
			INNER JOIN mst_financesupplieractivate FSA ON FSA.intSupplierId = fin_supplier_purchaseinvoice_header.intsupplierId
            WHERE
            FSA.intChartOfAccountId =  '$supId' AND
            fin_supplier_purchaseinvoice_header.intDeleteStatus =  '0' AND
            fin_supplier_purchaseinvoice_header.intCurrencyId = $currency AND
            fin_supplier_purchaseinvoice_header.dtmDate<='$toDay' AND
            DATEDIFF('$toDay',fin_supplier_purchaseinvoice_header.dtmDate) >= ($from+ PT.strName) ".$wareTo.$wareCom.
            " GROUP BY FSA.intChartOfAccountId"); 	
    $row = mysqli_fetch_array($result);
    return $row['balAmount'];    
}

function getBankDeposit($supId,$from,$to,$toDay,$currency,$companyId){
    if($to==x)$wareTo="";
    else $wareTo=" AND DATEDIFF('$toDay',BND.dtDate)<=$to";
    
    if($companyId==0)$wareCom="";
    else $wareCom=" AND BND.intCompanyId=$companyId";
    global $db;
    
     $result = $db->RunQuery("SELECT
	 			BND.strDepositNo,
				SUM(fin_bankdeposit_details.dblAmmount 
				-
				IFNULL ((SELECT
				Sum(fin_supplier_payments_main_details.dblPayAmount )AS paidAmount
				FROM fin_supplier_payments_main_details
				Inner Join fin_supplier_payments_header ON fin_supplier_payments_main_details.strReferenceNo = fin_supplier_payments_header.strReferenceNo
				WHERE
				fin_supplier_payments_main_details.strDocNo =  BND.strDepositNo AND
				fin_supplier_payments_header.intDeleteStatus =  '0' AND fin_supplier_payments_header.dtmDate<='$toDay' AND fin_supplier_payments_main_details.strDocType = 'B.Deposit'),0)
				
				) AS balAmount
				FROM
				fin_bankdeposit_header BND
				Inner Join fin_bankdeposit_details ON fin_bankdeposit_details.strDepositNo = BND.strDepositNo			
				Inner Join mst_financechartofaccounts ON fin_bankdeposit_details.intAccount = mst_financechartofaccounts.intId
				WHERE
				fin_bankdeposit_details.intRecvFrom =  '$supId' AND
				mst_financechartofaccounts.intFinancialTypeId =  '18' AND
				BND.intStatus = '1' AND
                                BND.intCurrency=$currency AND
                                BND.dtDate<='$toDay' AND
                                DATEDIFF('$toDay',BND.dtDate)>= $from".$wareTo.$wareCom);
								
    $row = mysqli_fetch_array($result);
    return $row['balAmount'];    
}

function getBankDeposit_ledgerWise($supId,$from,$to,$toDay,$currency,$companyId){
    if($to==x)$wareTo="";
    else $wareTo=" AND DATEDIFF('$toDay',BND.dtDate)<=$to";
    
    if($companyId==0)$wareCom="";
    else $wareCom=" AND BND.intCompanyId=$companyId";
    global $db;
    
    $result = $db->RunQuery("SELECT
	 			BND.strDepositNo,
				SUM(fin_bankdeposit_details.dblAmmount 
				-
				IFNULL ((SELECT
				Sum(fin_supplier_payments_main_details.dblPayAmount )AS paidAmount
				FROM fin_supplier_payments_main_details
				Inner Join fin_supplier_payments_header ON fin_supplier_payments_main_details.strReferenceNo = fin_supplier_payments_header.strReferenceNo
				WHERE
				fin_supplier_payments_main_details.strDocNo =  BND.strDepositNo AND
				fin_supplier_payments_header.intDeleteStatus =  '0' AND fin_supplier_payments_header.dtmDate<='$toDay' AND fin_supplier_payments_main_details.strDocType = 'B.Deposit'),0)
				
				) AS balAmount
				FROM
				fin_bankdeposit_header BND
				Inner Join fin_bankdeposit_details ON fin_bankdeposit_details.strDepositNo = BND.strDepositNo			
				Inner Join mst_financechartofaccounts ON fin_bankdeposit_details.intAccount = mst_financechartofaccounts.intId
				INNER JOIN mst_financesupplieractivate FSA ON FSA.intSupplierId = fin_bankdeposit_details.intRecvFrom
				WHERE
				FSA.intChartOfAccountId =  '$supId' AND
				mst_financechartofaccounts.intFinancialTypeId =  '18' AND
				BND.intStatus = '1' AND
                                BND.intCurrency=$currency AND
                                BND.dtDate<='$toDay' AND
                                DATEDIFF('$toDay',BND.dtDate)>= $from".$wareTo.$wareCom);
								
    $row = mysqli_fetch_array($result);
    return $row['balAmount'];    
}

function getBankPayments($supId,$from,$to,$toDay,$currency,$companyId){
    if($to==x)$wareTo="";
    else $wareTo=" AND DATEDIFF('$toDay',BNP.dtDate)<=$to";//
    
    if($companyId==0)$wareCom="";
    else $wareCom=" AND BNP.intCompanyId=$companyId";
    global $db;
    
    $result = $db->RunQuery("SELECT	
				BNP.strBankPaymentNo,
				fin_bankpayment_details.dblAmmount,		
				SUM(fin_bankpayment_details.dblAmmount
				+
				IFNULL ((SELECT
				Sum(fin_supplier_payments_main_details.dblPayAmount )AS paidAmount
				FROM fin_supplier_payments_main_details
				Inner Join fin_supplier_payments_header ON fin_supplier_payments_main_details.strReferenceNo = fin_supplier_payments_header.strReferenceNo
				WHERE
				fin_supplier_payments_main_details.strDocNo =  BNP.strBankPaymentNo AND
				fin_supplier_payments_header.intDeleteStatus =  '0' AND fin_supplier_payments_header.dtmDate<='$toDay' AND fin_supplier_payments_main_details.strDocType = 'B.Payment'),0)
				
				) AS balAmount
				FROM
				fin_bankpayment_header BNP
				Inner Join fin_bankpayment_details ON fin_bankpayment_details.strBankPaymentNo = BNP.strBankPaymentNo				
				Inner Join mst_financechartofaccounts ON fin_bankpayment_details.intAccountId = mst_financechartofaccounts.intId
				WHERE
				fin_bankpayment_details.intPayTo =  '$supId' AND
				mst_financechartofaccounts.intFinancialTypeId =  '18' AND
				BNP.intStatus = '1' AND
                                BNP.intCurrency=$currency AND
                                BNP.dtDate <='$toDay' AND
                                DATEDIFF('$toDay',BNP.dtDate)>= $from".$wareTo.$wareCom);
								
    $row = mysqli_fetch_array($result);
    return $row['balAmount']*(-1);   
}

function getBankPayments_ledgerWise($supId,$from,$to,$toDay,$currency,$companyId){
    if($to==x)$wareTo="";
    else $wareTo=" AND DATEDIFF('$toDay',BNP.dtDate)<=$to";//
    
    if($companyId==0)$wareCom="";
    else $wareCom=" AND BNP.intCompanyId=$companyId";
    global $db;
    
    $result = $db->RunQuery("SELECT	
				BNP.strBankPaymentNo,
				fin_bankpayment_details.dblAmmount,		
				SUM(fin_bankpayment_details.dblAmmount
				+
				IFNULL ((SELECT
				Sum(fin_supplier_payments_main_details.dblPayAmount )AS paidAmount
				FROM fin_supplier_payments_main_details
				Inner Join fin_supplier_payments_header 
					ON fin_supplier_payments_main_details.strReferenceNo = fin_supplier_payments_header.strReferenceNo
				WHERE
					fin_supplier_payments_main_details.strDocNo =  BNP.strBankPaymentNo AND
					fin_supplier_payments_header.intDeleteStatus =  '0' AND 
					fin_supplier_payments_header.dtmDate<='$toDay' AND 
					fin_supplier_payments_main_details.strDocType = 'B.Payment'),0)				
				) AS balAmount
				FROM
				fin_bankpayment_header BNP
				Inner Join fin_bankpayment_details ON fin_bankpayment_details.strBankPaymentNo = BNP.strBankPaymentNo				
				Inner Join mst_financechartofaccounts ON fin_bankpayment_details.intAccountId = mst_financechartofaccounts.intId
				INNER JOIN mst_financesupplieractivate FSA ON FSA.intSupplierId = fin_bankpayment_details.intPayTo
				WHERE
				FSA.intChartOfAccountId =  '$supId' AND
				mst_financechartofaccounts.intFinancialTypeId =  '18' AND
				BNP.intStatus = '1' AND
                                BNP.intCurrency=$currency AND
                                BNP.dtDate <='$toDay' AND
                                DATEDIFF('$toDay',BNP.dtDate)>= $from".$wareTo.$wareCom);
								
    $row = mysqli_fetch_array($result);
    return $row['balAmount']*(-1);   
}
function getPettyCash($supId,$from,$to,$toDay,$currency,$companyId){
    if($to==x)$wareTo="";
    else $wareTo=" AND DATEDIFF('$toDay',PTC.dtDate)<=$to";//
    
    if($companyId==0)$wareCom="";
    else $wareCom=" AND PTC.intCompanyId=$companyId";
    global $db;
    
    $result = $db->RunQuery("SELECT
				PTC.strPettyCashNo,
				fin_bankpettycash_details.dblAmmount,
				SUM(fin_bankpettycash_details.dblAmmount
				+
				IFNULL ((SELECT
				Sum(fin_supplier_payments_main_details.dblPayAmount )AS paidAmount
				FROM fin_supplier_payments_main_details
				Inner Join fin_supplier_payments_header ON fin_supplier_payments_main_details.strReferenceNo = fin_supplier_payments_header.strReferenceNo
				WHERE
				fin_supplier_payments_main_details.strDocNo =  PTC.strPettyCashNo AND
				fin_supplier_payments_header.intDeleteStatus =  '0' AND fin_supplier_payments_header.dtmDate<='$toDay' AND fin_supplier_payments_main_details.strDocType = 'Petty Cash'),0)
				
				) AS balAmount
                            FROM
				fin_bankpettycash_header AS PTC
				Inner Join fin_bankpettycash_details ON fin_bankpettycash_details.strPettyCashNo = PTC.strPettyCashNo
				Inner Join mst_financechartofaccounts ON fin_bankpettycash_details.intAccountId = mst_financechartofaccounts.intId
                            WHERE
				mst_financechartofaccounts.intFinancialTypeId =  '18'
				AND
				fin_bankpettycash_details.intPayTo =  '$supId' AND
				PTC.intStatus = '1' AND
                                PTC.intCurrency=$currency AND
                                PTC.dtDate <='$toDay' AND
                                DATEDIFF('$toDay',PTC.dtDate)>= $from".$wareTo.$wareCom);
    $row = mysqli_fetch_array($result);
    return $row['balAmount']*(-1);   
}

function getPettyCash_ledgerWise($supId,$from,$to,$toDay,$currency,$companyId){
    if($to==x)$wareTo="";
    else $wareTo=" AND DATEDIFF('$toDay',PTC.dtDate)<=$to";//
    
    if($companyId==0)$wareCom="";
    else $wareCom=" AND PTC.intCompanyId=$companyId";
    global $db;
    
    $result = $db->RunQuery("SELECT
				PTC.strPettyCashNo,
				fin_bankpettycash_details.dblAmmount,
				SUM(fin_bankpettycash_details.dblAmmount
				+
				IFNULL ((SELECT
				Sum(fin_supplier_payments_main_details.dblPayAmount )AS paidAmount
				FROM fin_supplier_payments_main_details
				Inner Join fin_supplier_payments_header ON fin_supplier_payments_main_details.strReferenceNo = fin_supplier_payments_header.strReferenceNo
				WHERE
				fin_supplier_payments_main_details.strDocNo =  PTC.strPettyCashNo AND
				fin_supplier_payments_header.intDeleteStatus =  '0' AND fin_supplier_payments_header.dtmDate<='$toDay' AND fin_supplier_payments_main_details.strDocType = 'Petty Cash'),0)
				
				) AS balAmount
                            FROM
				fin_bankpettycash_header AS PTC
				Inner Join fin_bankpettycash_details ON fin_bankpettycash_details.strPettyCashNo = PTC.strPettyCashNo
				Inner Join mst_financechartofaccounts ON fin_bankpettycash_details.intAccountId = mst_financechartofaccounts.intId
				INNER JOIN mst_financesupplieractivate FSA ON FSA.intSupplierId = fin_bankpettycash_details.intPayTo
                WHERE
					mst_financechartofaccounts.intFinancialTypeId =  '18' AND
					FSA.intChartOfAccountId =  '$supId' AND
					PTC.intStatus = '1' AND
					PTC.intCurrency=$currency AND
					PTC.dtDate <='$toDay' AND
					DATEDIFF('$toDay',PTC.dtDate)>= $from".$wareTo.$wareCom);
    $row = mysqli_fetch_array($result);
    return $row['balAmount']*(-1);   
}

function getJurnelEntry($supId,$from,$to,$toDay,$currency,$companyId){
    if($to==x)$wareTo="";
    else $wareTo=" AND DATEDIFF('$toDay', JH.dtmDate)<=$to";//
    
    if($companyId==0)$wareCom="";
    else $wareCom=" AND JH.intCompanyId=$companyId";
    global $db;
    
    $sql="SELECT     
				JH.strReferenceNo,
				JD.dblDebitAmount,
                JD.dbCreditAmount,
				SUM(JD.dbCreditAmount - JD.dblDebitAmount
				-
                IFNULL ((SELECT
				Sum(fin_supplier_payments_main_details.dblPayAmount )AS paidAmount
				FROM
				fin_supplier_payments_main_details
				Inner Join fin_supplier_payments_header ON fin_supplier_payments_main_details.strReferenceNo = fin_supplier_payments_header.strReferenceNo
				Inner Join fin_accountant_journal_entry_details ON fin_supplier_payments_main_details.strDocNo = fin_accountant_journal_entry_details.strReferenceNo
				WHERE
				fin_supplier_payments_main_details.strDocNo =  'JH.strReferenceNo' AND
				fin_supplier_payments_header.intDeleteStatus =  '0' AND
				fin_supplier_payments_header.dtmDate <=  '$toDay' AND
				fin_supplier_payments_main_details.strDocType =  'JN' AND
				fin_supplier_payments_header.intSupplierId =  '$supId' AND
				fin_accountant_journal_entry_details.strPersonType =  'sup'),0)			
				) AS balAmount
                            FROM
                            fin_accountant_journal_entry_header AS JH
                            INNER JOIN fin_accountant_journal_entry_details AS JD ON JH.strReferenceNo = JD.strReferenceNo                           
                            INNER JOIN mst_financechartofaccounts ON JD.intChartOfAccountId = mst_financechartofaccounts.intId
                            WHERE
                            JD.strPersonType = 'sup' AND
                            JD.intNameId = $supId AND
                            JH.intDeleteStatus = 0 AND
                            mst_financechartofaccounts.intFinancialTypeId = 18 AND
                            JH.intCurrencyId=$currency AND
                            JH.dtmDate <='$toDay' AND
                            DATEDIFF('$toDay', JH.dtmDate)>= $from".$wareTo.$wareCom;
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
//	if($row['dblDebitAmount']!=0)
//	{
//		$toBePaid = $row['balAmount']*(-1);
//	}
//	else if($row['dbCreditAmount']!=0)
//	{
//		$toBePaid = $row['balAmount'];
//	} 
    return $row['balAmount'];
}

function getJurnelEntry_ledgerWise($supId,$from,$to,$toDay,$currency,$companyId){
    if($to==x)$wareTo="";
    else $wareTo=" AND DATEDIFF('$toDay', JH.dtmDate)<=$to";//
    
    if($companyId==0)$wareCom="";
    else $wareCom=" AND JH.intCompanyId=$companyId";
    global $db;
    
    $sql="SELECT     
				JH.strReferenceNo,
				JD.dblDebitAmount,
                JD.dbCreditAmount,
				SUM(JD.dbCreditAmount - JD.dblDebitAmount
				-
                IFNULL ((SELECT
				Sum(fin_supplier_payments_main_details.dblPayAmount )AS paidAmount
				FROM
				fin_supplier_payments_main_details
				Inner Join fin_supplier_payments_header ON fin_supplier_payments_main_details.strReferenceNo = fin_supplier_payments_header.strReferenceNo
				Inner Join fin_accountant_journal_entry_details ON fin_supplier_payments_main_details.strDocNo = fin_accountant_journal_entry_details.strReferenceNo
				WHERE
				fin_supplier_payments_main_details.strDocNo =  'JH.strReferenceNo' AND
				fin_supplier_payments_header.intDeleteStatus =  '0' AND
				fin_supplier_payments_header.dtmDate <=  '$toDay' AND
				fin_supplier_payments_main_details.strDocType =  'JN' AND
				fin_supplier_payments_header.intSupplierId = JD.intNameId AND
				fin_accountant_journal_entry_details.strPersonType =  'sup'),0)			
				) AS balAmount
				FROM
				fin_accountant_journal_entry_header AS JH
				INNER JOIN fin_accountant_journal_entry_details AS JD ON JH.strReferenceNo = JD.strReferenceNo                           
				INNER JOIN mst_financechartofaccounts ON JD.intChartOfAccountId = mst_financechartofaccounts.intId
				INNER JOIN mst_financesupplieractivate FSA ON FSA.intSupplierId = JD.intNameId
				WHERE
				JD.strPersonType = 'sup' AND
				FSA.intChartOfAccountId = $supId AND
				JH.intDeleteStatus = 0 AND
				mst_financechartofaccounts.intFinancialTypeId = 18 AND
				JH.intCurrencyId=$currency AND
				JH.dtmDate <='$toDay' AND
				DATEDIFF('$toDay', JH.dtmDate)>= $from".$wareTo.$wareCom;
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    return $row['balAmount'];
}
?>

<?php
session_start();

//BEGIN - SET PARAMETERS	{
$backwardseperator = "../../../../";
$companyId 			= $_SESSION['headCompanyId'];
$location 			= $_SESSION['CompanyID'];
$intUser 			= $_SESSION["userId"];
$supplierId			= $_REQUEST["SupplierId"];
$targetCurrency		= $_REQUEST["TargetCurrency"];;
//BEGIN - INCLUDE FILES {
include  	"{$backwardseperator}dataAccess/Connector.php";
//END 	- INCLUDE FILES }
$payment_term_array	= GetPaymentTerm();
$supplierName		= GetSupplierName($supplierId);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Supplier Invoice Aging Report</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>
<script type="application/javascript">
function Reload()
{
	document.frmSupplierAgingReport.submit();	
}
</script>
</head>
<body>
<form id="frmSupplierAging_subReport" name="frmSupplierAging_subReport" method="post" action="rptCustomer_invoice_aging.php">
  <div align="center">
  <div style="background-color:#FFF" ><strong><?php echo $supplierName?></strong><strong></strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
 <tr>
   <td><table  border="0" cellpadding="0" cellspacing="0" class="bordered" width="100%">
     <thead>
       <tr valign="bottom">
         <th width="28">&nbsp;</th>
         <th width="123">&nbsp;</th>
         <th width="467">&nbsp;</th>
         <th width="467">&nbsp;</th>
         <th width="467">&nbsp;</th>
         <th width="274" colspan="<?php echo count($payment_term_array)?>">Invoice</th>
         </tr>
       <tr valign="bottom">
         <th>No</th>
         <th>PO No</th>
         <th>GRN No</th>
         <th>Invoice No</th>
         <th>PO Credit Term</th>        
         <?php for($i=0;$i<count($payment_term_array);$i++){?>
         <?php }?>
         <?php for($i=0;$i<count($payment_term_array);$i++){?>
         <th style="width:50px"><?php echo $payment_term_array[$i][1]?></th>
         <?php }?>
         </tr>
       </thead>
     <tbody>
  <?php
$supplier_id ='';
$count	= 0;
$booAvi	= true;
$result = GetMainDetails();
while($row = mysqli_fetch_array($result))
{
	
	$final_value	= ConvertWithExchangeRates($row["FINAL_VALUE"],$row["PO_DATE"],$row["PO_SOURCE_CURRENCY"],$targetCurrency);
	
	$start = 0;
	$boo	= true;
	for($i=0;$i<count($payment_term_array);$i++)
	{
		$diff_date 	= $row["GAP"];	
		$aging = 0;
		
		if($boo){
			if($diff_date<0){
				$aging = round($final_value);
				$boo	= false;
			}
		}
		
		if($boo){			
			if($start < $diff_date && $payment_term_array[$i][0] >= $diff_date)
			{
				$aging = round($final_value);		
			}
			else 
				$aging = 0;
		}
		$aging_array[$payment_term_array[$i][0]] += $aging;			
		$total_array[$payment_term_array[$i][0]] += $aging;
		$start = $payment_term_array[$i][0];
	}
?>
         <tr>
           <td nowrap="nowrap"><?php echo ++$count?>.</td>
           <td nowrap="nowrap"><?php echo $row["PO_NO"]?></td>
           <td nowrap="nowrap"><?php echo $row["GRN_NOS"]?></td>
           <td nowrap="nowrap"><?php echo $row["INVOICE_NOS"]?></td>
           <td nowrap="nowrap"><?php echo $row["CREDIT_DAYS"]?> Days</td>   
<?php
		for($i=0;$i<count($payment_term_array);$i++)
		{
?>
       <td nowrap="nowrap" style="text-align:right"><?php echo $aging_array[$payment_term_array[$i][0]]=='0'?'&nbsp;':number_format($aging_array[$payment_term_array[$i][0]])?></td>
<?php 	
		}
		$aging_array = array();
		
?>
      </tr>
  <?php	
}
?>
  </tbody>
  <tfoot>
    <tr style="font-weight:bold">
      <td nowrap="nowrap">&nbsp;</td>
      <td nowrap="nowrap" style="text-align:center">Total</td>
      <td nowrap="nowrap">&nbsp;</td>
      <td nowrap="nowrap">&nbsp;</td>
      <td nowrap="nowrap">&nbsp;</td>
      <?php for($i=0;$i<count($payment_term_array);$i++){?>
      <td nowrap="nowrap" style="text-align:right"><?php echo number_format($total_array[$payment_term_array[$i][0]])?></td>
      <?php }?>
      </tr>
  </tfoot>	
     </table></td>
</tr>
            <tr>
</table>
</div>
</form>
</body>
<script type="text/javascript">
$(document).ready(function()
{
	$('.bordered tbody tr').click(function(){
		if($(this).attr('style')!='background-color:#82FF82')
		{
			$('.bordered tbody tr').attr('style','background-color:#FFFFFF');  
			   $(this).attr('style','background-color:#82FF82');     
		}                                             
	})
});
</script>
</html>
<?php
function GetMainDetails()
{
	global $db;
	global $supplierId;

	$sql = "SELECT 
				S.strName											AS SUPPLIER_NAME,
				S.intId												AS SUPPLIER_ID,
				PT.strName											AS CREDIT_DAYS,
				DATEDIFF(NOW(),PH.dtmPODate)						AS DATE_DIFF,
				PT.strName - DATEDIFF(NOW(),PH.dtmPODate)			AS GAP ,
				CONCAT(PH.intPONo,'/',PH.intPOYear) 				AS PO_NO,
				PH.intCurrency										AS PO_SOURCE_CURRENCY,
				PH.dtmPODate										AS PO_DATE,			
				ROUND(COALESCE(SUM(PD.dblUnitPrice * dblQty),0),2) 	AS PO_AMOUNT,
			
		
			COALESCE((SELECT SUM(dblPayAmount)
			FROM fin_supplier_payments_header SPH
			INNER JOIN fin_supplier_payments_main_details SPD
				ON SPD.intReceiptNo = SPH.intReceiptNo
				AND SPD.intAccPeriodId = SPH.intAccPeriodId
				AND SPD.intLocationId = SPH.intLocationId
				AND SPD.intCompanyId = SPH.intCompanyId
				AND SPD.strReferenceNo = SPH.strReferenceNo
			WHERE SPD.strPoNo = CONCAT(PH.intPONo,'/',PH.intPOYear)
				AND SPH.intDeleteStatus = 0),0)						AS PAYMENT_RECEIVED,
				
			(ROUND(COALESCE(SUM(PD.dblUnitPrice * dblQty),0),2) - COALESCE((SELECT SUM(dblPayAmount)
			FROM fin_supplier_payments_header SPH
			INNER JOIN fin_supplier_payments_main_details SPD
				ON SPD.intReceiptNo = SPH.intReceiptNo
				AND SPD.intAccPeriodId = SPH.intAccPeriodId
				AND SPD.intLocationId = SPH.intLocationId
				AND SPD.intCompanyId = SPH.intCompanyId
				AND SPD.strReferenceNo = SPH.strReferenceNo
			WHERE SPD.strPoNo = CONCAT(PH.intPONo,'/',PH.intPOYear)
				AND SPH.intDeleteStatus = 0),0))					AS FINAL_VALUE,
			
			(SELECT GROUP_CONCAT(DISTINCT CONCAT(GH.intGrnNo,'/',GH.intGrnYear))
			FROM ware_grnheader GH 
			WHERE GH.intStatus = 1
				AND GH.intPoNo = PH.intPONo
				AND GH.intPoYear = PH.intPOYear)					AS GRN_NOS,
				
			(SELECT GROUP_CONCAT(DISTINCT strInvoiceNo)
			FROM ware_grnheader GH 
			WHERE GH.intStatus = 1
				AND GH.intPoNo = PH.intPONo
				AND GH.intPoYear = PH.intPOYear)					AS INVOICE_NOS,
			
			PH.*
			
		FROM trn_poheader PH
		   INNER JOIN trn_podetails PD ON PD.intPONo = PH.intPONo AND PD.intPOYear = PH.intPOYear
		  INNER JOIN mst_financepaymentsterms PT ON PT.intId  = PH.intPaymentTerm
		  INNER JOIN mst_supplier S ON S.intId = PH.intSupplier
		WHERE PH.intStatus = 1 AND S.intId = $supplierId
		GROUP BY PH.intPOYear,PH.intPONo
		HAVING FINAL_VALUE <> 0
		ORDER BY S.strName";
	return $db->RunQuery($sql);
}
function GetPaymentTerm()
{
	global $db;
	$array	= array();
	$array[0][0]	= "-1";
	$array[0][1]	= "Exceed Credit Days";
		
	$i		= 1;
	
	$sql = "SELECT PT.strName AS NAME,PT.strRange AS RANGES FROM mst_financepaymentsterms PT WHERE PT.strName <> 0 ORDER BY PT.strName";
	$result = $db->RunQuery($sql);
	while($row = mysqli_fetch_array($result))
	{
		$array[$i][0]	= $row["NAME"];
		$array[$i][1]	= $row["RANGES"];
		$i++;
	}
	return $array;
}

function GetSupplierName($supplierId)
{
	global $db;
	
	$sql = "SELECT strName FROM mst_supplier WHERE intId = '$supplierId'";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	return $row["strName"];
}

function ConvertWithExchangeRates($final_value,$po_date,$po_source_currency,$target_currency)
{
	global $db;
	
	$sql 	= "SELECT dblSellingRate FROM mst_financeexchangerate WHERE dtmDate = '$po_date' AND intCurrencyId = $po_source_currency AND intCompanyId =1";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	$sourceExRate	= $row["dblSellingRate"];
	
	$sql 	= "SELECT dblSellingRate FROM mst_financeexchangerate WHERE dtmDate = '$po_date' AND intCurrencyId = $target_currency AND intCompanyId =1";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	$targetExRate	= $row["dblSellingRate"];
	
	$value = ($final_value/$targetExRate)*$sourceExRate;
	return $value;
}
?>
<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];

$thisFilePath = $_SERVER['PHP_SELF'];

include "{$backwardseperator}dataAccess/permisionCheck.inc";
$companyId = $_SESSION['headCompanyId'];
$date = $_GET['txtDate'];
?>
<script type="application/javascript" >
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Supplier Aging Summery</title>
        <link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />

        <script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
        <script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
        <script type="application/javascript" src="AgingSummery.js"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

        <link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
        <script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
        <link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css" />
        <link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css" />

    </head>

    <body>
        <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
                <td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator . 'Header.php'; ?></td>
            </tr> 
        </table>

        <script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
        <script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
        <script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

        <form id="frmAgingSummery" name="frmAgingSummery"  method="get" autocomplete="off"  >
            <div align="center">
                <div class="trans_layoutD">
                    <div class="trans_text">Supplier Aging Summery</div>
                    <table width="100%">
                        <tr>
                            <td width="100%"><table width="100%">
                                    <tr>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td colspan="5" rowspan="5" class="normalfntRight">


                                            <table width="100%" >
                                                <tr>
                                                    <td class="normalfnt" align="left">Company&nbsp;&nbsp;</td>
                                                    <td colspan="2" align="left" bgcolor="#FFFFFF" class=""><select name="cboCompany" id="cboCompany" style="width:200px" class="validate[required]">
                                                            <option value="0">All</option>
                                                            <?php
                                                            $sql = "SELECT
                                                                        mst_companies.intId,
                                                                        mst_companies.strName
                                                                        FROM mst_companies
                                                                        WHERE
                                                                        mst_companies.intStatus =  '1'";
                                                            $result = $db->RunQuery($sql);
                                                            while ($row = mysqli_fetch_array($result)) {
                                                                if ($row['intId'] == $companyId)
                                                                    echo "<option value=\"" . $row['intId'] . "\" selected=\"selected\">" . $row['strName'] . "</option>";
                                                                else
                                                                    echo "<option value=\"" . $row['intId'] . "\">" . $row['strName'] . "</option>";
                                                            }
                                                            ?>
                                                        </select></td>
                                                </tr>        <tr>
                                                    <td width="25%" class="normalfnt" align="right">Date</td>
                                                    <td colspan="2" align="left"><input name="txtDate" type="text" value="<?Php echo ($date==''?date("Y-m-d"):$date) ?>" class="validate[required] txtbox" id="txtDate" style="width:98px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                                                </tr>
                                                <tr>
                                                    <td width="25%" class="normalfnt" align="right">Currency</td>
                                                    <td colspan="2" align="left"><select name="cboCurrency" id="cboCurrency" style="width:211px" class="validate[required]">
                                                            <option value=""></option>
                                                            <?php
                                                            $sql = "SELECT
                                                                        intId,
                                                                        strCode
                                                                        FROM mst_financecurrency
                                                                        WHERE
                                                                                intStatus = 1
                                                                        order by strCode";
                                                            $result = $db->RunQuery($sql);
                                                            while ($row = mysqli_fetch_array($result)) {
                                                                if ($row['intId'] == $currency)
                                                                    echo "<option value=\"" . $row['intId'] . "\" selected=\"selected\">" . $row['strCode'] . "</option>";
                                                                else
                                                                    echo "<option value=\"" . $row['intId'] . "\">" . $row['strCode'] . "</option>";
                                                            }
                                                            ?>
                                                        </select></td>
                                                </tr>
                                                <tr>
                                                  <td class="normalfnt" align="right">Ledger Accounts</td>
                                                  <td colspan="2" align="left">                <select name="cboLedgerAcc" id="cboLedgerAcc"  style="width:211px" onChange="getSupplierList();" >
                	<option value=""></option>
                        <?php  $sql2 = "SELECT DISTINCT
											mst_financechartofaccounts.intId,
											mst_financechartofaccounts.strCode,
											mst_financechartofaccounts.strName
										FROM mst_financechartofaccounts
										Inner Join mst_financesupplieractivate 
											ON mst_financechartofaccounts.intId = mst_financesupplieractivate.intChartOfAccountId
										WHERE
											mst_financesupplieractivate.intCompanyId =  '$companyId'
										GROUP BY  mst_financechartofaccounts.intId 
										order by strCode ";
						$result2 = $db->RunQuery($sql2);
						while($row2=mysqli_fetch_array($result2))
						{
						   echo "<option value=\"".$row2['intId']."\">".$row2['strCode']."-".$row2['strName']."</option>";
						}
        				?> 
                 </select></td>
                                                </tr>
                                                <tr>
                                                  <td class="normalfnt" align="right">&nbsp;</td>
                                                  <td width="39%" align="left" class="normalfnt"><input type="radio" name="radio" id="rdoDetails" value="rdoDetails" checked="checked" />
                                                  <label for="rdoDetails">Details </label></td>
                                                  <td width="36%" align="left" class="normalfnt"s><input type="radio" name="radio" id="rdoSummary" value="rdoSummart" />
                                                  <label for="rdoSummart">Summary</label></td>
                                                </tr>

                                            </table>
                                        </td>
                                        <td bgcolor="#FFFFFF" class=""></td>
                                    </tr>
                                    <tr>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td bgcolor="#FFFFFF" class="">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="21" class="normalfnt">&nbsp;</td>
                                        <td width="44" bgcolor="#FFFFFF" class="">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="21" class="normalfnt">&nbsp;</td>
                                        <td bgcolor="#FFFFFF" class="">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="21" class="normalfnt">&nbsp;</td>
                                        <td bgcolor="#FFFFFF" class="">&nbsp;</td>
                                    </tr>
                                    <tr style="display:none">
                                        <td height="21" class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td bgcolor="#FFFFFF" class="">&nbsp;</td>
                                    </tr>
                                </table></td>
                        </tr>
                        <tr>
                            <td align="center">
                                <table width="100%" class="tableBorder_allRound">
                                    <tr>
                                        <td width="100%" align="center" bgcolor=""><img style="display:none" border="0" src="../../../../images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img border="0" src="../../../../images/Treport.jpg" alt="Summery Report" name="butReport"width="92" height="24"  class="mouseover" id="butReport" tabindex="24"/><a href="../../../../main.php"><img  src="../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">&nbsp;</td>
                        </tr>
                    </table>
                </div>
            </div>
        </form>
    </body>
</html>

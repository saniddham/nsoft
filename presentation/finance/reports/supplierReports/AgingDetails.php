<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];

$thisFilePath = $_SERVER['PHP_SELF'];

include "{$backwardseperator}dataAccess/permisionCheck.inc";
$companyId = $_SESSION['headCompanyId'];
$date = $_GET['txtDate'];
?>
<script type="application/javascript" >
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Supplier Aging Reports</title>
        <link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />

        <script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
        <script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
        <script type="application/javascript" src="AgingDetails.js"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

        <link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
        <script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
        <link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css" />
        <link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css" />

    </head>

    <body>
        <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
                <td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator . 'Header.php'; ?></td>
            </tr> 
        </table>

        <script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
        <script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
        <script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

        <form id="frmAgingRpts" name="frmAgingRpts"  method="get" action="customerBalance_summery.php" autocomplete="off"  >
            <div align="center">
                <div class="trans_layoutD">
                    <div class="trans_text">Supplier Aging Details</div>
                    <table width="100%">
                        <tr>
                            <td width="100%"><table width="100%">
                                    <tr>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td colspan="5" rowspan="5" class="normalfntRight">


                                            <table width="100%" >
                                                <tr>
                                                    <td class="normalfnt" align="left">Company&nbsp;&nbsp;</td>
                                                    <td bgcolor="#FFFFFF" class="" align="left"><select name="cboCompany" id="cboCompany" style="width:200px" class="validate[required]">
                                                            <option value="0">All</option>
                                                            <?php
                                                            $sql = "SELECT
						mst_companies.intId,
						mst_companies.strName
						FROM mst_companies
						WHERE
						mst_companies.intStatus =  '1'
						";
                                                            $result = $db->RunQuery($sql);
                                                            while ($row = mysqli_fetch_array($result)) {
                                                                if ($row['intId'] == $companyId)
                                                                    echo "<option value=\"" . $row['intId'] . "\" selected=\"selected\">" . $row['strName'] . "</option>";
                                                                else
                                                                    echo "<option value=\"" . $row['intId'] . "\">" . $row['strName'] . "</option>";
                                                            }
                                                            ?>
                                                        </select></td>
                                                </tr>
                                                <tr>
                                                    <td width="20%" class="normalfnt" align="right">Date</td>
                                                    <td width="80%" align="left"><input name="txtDate" type="text" <?php if ($date) { ?> value="<?php echo $date; ?>" <?php } else { ?> value="<?php echo date("Y-m-d"); ?>" <?php } ?>  class="validate[required] txtbox normalfnt" id="txtDate" style="width:98px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" readonly="readonly"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                                                </tr>
                                                <tr>
                                                    <td width="20%" class="normalfnt" align="right">Currency</td>
                                                    <td width="80%" align="left"><select name="cboCurrency" id="cboCurrency" style="width:211px" class="validate[required]">
                                                            <option value=""></option>
                                                            <?php
                                                            $sql = "SELECT
						intId,
						strCode
						FROM mst_financecurrency
						WHERE
							intStatus = 1
						order by strCode
						";
                                                            $result = $db->RunQuery($sql);
                                                            while ($row = mysqli_fetch_array($result)) {
                                                                if ($row['intId'] == $currency)
                                                                    echo "<option value=\"" . $row['intId'] . "\" selected=\"selected\">" . $row['strCode'] . "</option>";
                                                                else
                                                                    echo "<option value=\"" . $row['intId'] . "\">" . $row['strCode'] . "</option>";
                                                            }
                                                            ?>
                                                        </select></td>
                                                </tr>                                                
                                                <tr>
                                                    <td bgcolor="#FFFFFF" class="" colspan="2">
                                                        <div id="divTable1" style="overflow:scroll;width:100%;height:250px"  >
                                                            <table width="97%" id="tblCustomers" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
                                                                <tr class="">
                                                                    <td width="44" bgcolor="#FAD163" class="normalfntMid"><input type="checkbox" name="chkAll" id="chkAll" />
                                                                    </td>
                                                                    <td width="234"  height="24" bgcolor="#FAD163" class="normalfntMid"><strong>Supplier</strong></td>
                                                                </tr>

                                                                <?php
                                                                if ($date == '') {
                                                                    $date = date("Y-m-d");
                                                                }

                                                                $sqlm = "SELECT
                                                                        intId,                                                                         
                                                                        strName
                                                                        FROM mst_supplier 
																		where intStatus = '1'
                                                                        ORDER BY strName ASC";
                                                                $resultm = $db->RunQuery($sqlm);
                                                                while ($rowm = mysqli_fetch_array($resultm)) {                                                                    
                                                                    $customerId = $rowm['intId'];
                                                                    $customer = $rowm['strName'];
                                                                    
                                                                ?>

                                                                    <tr class="normalfnt">
                                                                        <td bgcolor="#FFFFFF" class="normalfntMid"><input name="chkSelect" type="checkbox" value="<?php echo $customerId; ?>" /></td>
                                                                        <td bgcolor="#FFFFFF" class="normalfnt"><?php echo $customer ?></td>
                                                                    </tr>        
                                                                    <?php
                                                                }
                                                                ?>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td bgcolor="#FFFFFF" class=""></td>
                                    </tr>
                                    <tr>
                                        <td class="normalfnt">&nbsp;</td>
                                        <td bgcolor="#FFFFFF" class="">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td width="21" class="normalfnt">&nbsp;</td>
                                        <td width="44" bgcolor="#FFFFFF" class="">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="21" class="normalfnt">&nbsp;</td>
                                        <td bgcolor="#FFFFFF" class="">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td height="21" class="normalfnt">&nbsp;</td>
                                        <td bgcolor="#FFFFFF" class="">&nbsp;</td>
                                    </tr>
                                </table></td>
                        </tr>
                        <tr>
                            <td align="center">
                                <table width="100%" class="tableBorder_allRound">
                                    <tr>
                                        <td width="100%" align="center" bgcolor=""><img style="display:none" border="0" src="../../../../images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img border="0" src="../../../../images/Treport.jpg" alt="Summery Report" name="butReport"width="92" height="24"  class="mouseover" id="butReport" tabindex="24"/><a href="../../../../main.php"><img  src="../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">&nbsp;</td>
                        </tr>
                    </table>
                </div>
            </div>
        </form>
    </body>
</html>
<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$thisFilePath = $_SERVER['PHP_SELF'];
$companyId = $_SESSION['headCompanyId'];

include "{$backwardseperator}dataAccess/permisionCheck.inc";
$date = $_GET['txtDate'];
//--------------------------------
$sql = "SELECT DISTINCT mst_financecurrency.intId,mst_financecurrency.strCode FROM mst_financecurrency ";
$result = $db->RunQuery($sql);
$a = 0;
while ($row = mysqli_fetch_array($result)) {
    $currency[$a] = $row["intId"];
    $currencyCode[$a] = $row["strCode"];
    $a++;
}
$colspan = count($currency);
//--------------------------------
?>
<script type="application/javascript" >
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Other Payable / Service Supplier -  Balance Summery</title>
        <link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />

        <script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
        <script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>        
        <script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

        <link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
        <script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

        <link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css"/>
        <link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css"/>
        <script type="application/javascript" src="SupBalanceSummery.js"></script>
        <style>
            .showDiv{
                overflow:scroll;width:100%;height:300px;
            }
            .hideDiv{
                overflow:scroll;width:100%;height:300px; display:none;
            }
            .hideByttons{
                display:none;
            }
            .ShowByttons{
                display:"";
            }
        </style>
    </head>

    <body>
        <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
          <tr>
                <td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator . 'Header.php'; ?></td>
            </tr> 
        </table>

        <script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
        <script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
        <script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>
        <style>
            .normal{
                background-color: #ffffff;
            }
            .selected{
                background-color: #00CCFF;
            }
        </style>
        <form id="frmSupSummery" name="frmSupSummery" autocomplete="off"  >
            <div align="center">
                <div class="trans_layoutXL" >
                    <div class="trans_text" >Other Payable / Service Supplier - Balance Summery</div>
                    <table width="100%">
                        <tr>
                            <td align="center">
                                <table align="center">
                                    <tr>                                                    
                                        <td class="normalfnt" align="left">Date&nbsp;&nbsp;</td>
                                        <td>
                                            <input name="txtDate" type="text" value="<?Php echo ($date==''?date("Y-m-d"):$date) ?>" class="validate[required] txtbox" id="txtDate" style="width:98px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" />
                                        </td>                                                    
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <div id="divTable" style="overflow:scroll auto;height: 90px"  >
                                    <table width="40%" id="tblCompanies" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
                                        <tr class="">
                                            <td bgcolor="#FAD163" class="normalfnt"><input type="checkbox" name="chkAllComp" id="chkAllComp"  />
                                            </td>
                                            <td height="24" bgcolor="#FAD163" class="normalfntMid"><strong>Company</strong></td>
                                        </tr>

                                        <?php
                                        $sqlC = "SELECT mst_companies.intId, mst_companies.strName FROM mst_companies WHERE mst_companies.intStatus =  '1'";
                                        $resultC = $db->RunQuery($sqlC);
                                        $k = 0;
                                        while ($rowC = mysqli_fetch_array($resultC)) {
                                            $compId = $rowC['intId'];
                                            $compName = $rowC['strName'];
                                            ?>

                                            <tr class="normalfnt">
                                                <td bgcolor="#FFFFFF" class="normalfnt"><input name="chkCompany" type="checkbox" value="<?php echo $compId; ?>"/></td>
                                                <td bgcolor="#FFFFFF" class="normalfnt" id="<?php echo $compId; ?>"><?php echo $compName ?></td>
                                            </tr>        
                                            <?php
                                            if ($_GET["chkCompt$compId"] == $compId) {
                                                $k++;
                                                $comp = $_GET["chkCompt$compId"];
                                                $companies[$k - 1] = $comp;
                                            }
                                        }
                                        ?>                                             
                                    </table>  
                                </div>   
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <img src="../../../../images/search.png" width="25" height="25" alt="search" id="butSerach"  />
                            </td>
                        </tr>              
                    </table>
                    <div id="divTable1" class="hideDiv"></div>
                    <div id="buttonsDiv" class="hideByttons">
                        <table width="100%" class="tableBorder_allRound">
                            <tr>
                                <td width="100%" align="center" bgcolor=""><img style="display:none" border="0" src="../../../../images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img border="0" src="../../../../images/Treport.jpg" alt="Summery Report" name="butReport"width="92" height="24"  class="mouseover" id="butReport" tabindex="24"/><a href="../../../../main.php"><img  src="../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

        </form>
    </body>
</html>
<?php


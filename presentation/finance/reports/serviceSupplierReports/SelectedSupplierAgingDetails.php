<?php
session_start();
$backwardseperator = "../../../../";
$companyId = $_SESSION['headCompanyId'];
$location = $_SESSION['CompanyID'];
$intUser = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath = $_SERVER['PHP_SELF'];
$locationId = $location; //this locationId use in report header(reportHeader.php)--------------------
include "{$backwardseperator}dataAccess/Connector.php";

$suppliers = $_REQUEST['details'];
$currency = $_REQUEST['currency'];
$filterCompany = $_REQUEST['company'];
$toDay = $_REQUEST['toDay'];

//-------------------------
$sql = "SELECT intId,strCode FROM mst_financecurrency WHERE intId = '$currency'";
$result = $db->RunQuery($sql);
$row = mysqli_fetch_array($result);
$currencyDesc = $row['strCode'];
//-------------------------
$sql = "SELECT mst_companies.strName FROM mst_companies WHERE mst_companies.intId =  '$filterCompany'";
$result = $db->RunQuery($sql);
$row = mysqli_fetch_array($result);
$filterCompName = $row['strName'];
//-------------------------

$ledgerURL = "SupplierLedgerRpt.php?startDate=".date("Y-m-d",strtotime("-1 months",strtotime($_REQUEST["toDay"])))."&endDate=".$_REQUEST["toDay"];
$xy = 0;
foreach ($suppliers as $supId)
{
	$ledgerURL .= "&suppliers[".$xy++."]=".$supId;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Other Payable / Service Supplier -  Aging Details</title>
        <link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
         <link href="../../../../css/button.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
        <script type="application/javascript" src="../accountReports/DrillAccount.js"></script>
        <style>
            .custname{
                font-family: Verdana;
                font-size: 13px;
                color: #000000;
                margin: 0px;
                text-align: center;
            }
            table.maintable{
                width: 75%;                
                border-collapse:collapse;
                border-bottom: 1px solid black;
                border-top:1px solid black;
                border-left:1px solid black;
                border-right:1px solid black;
            }
            table.maintable tr td{
                /*border:1px solid black;*/
            }
            .maintableHeader{
                font-family: Verdana;
                font-size: 12px;
                color: #000000;
                margin: 0px;
                text-align: center;
            }
            .subHed{
                font-family: Verdana;
                font-size: 11px;
                color: #000000;
                margin: 0px;
                text-align: left;                
                font-weight:bold;

            }
            .subHed1{
                font-family: Verdana;
                font-size: 11px;
                color: #000000;
                margin: 0px;
                text-align: left;
            }
            .figurs{
                font-family: Verdana;
                font-size: 10px;
                color: #000000;
                margin: 0px;
                text-align: right;
                cursor: pointer;
            }
            .total{
                font-family: Verdana;
                font-size: 11px;
                color: #000000;
                margin: 0px;
                text-align: right;                
                font-weight:bolder;
                border-bottom: 1px solid black;
                border-top:1px solid black;    

            }
            .total1{
                font-family: Verdana;
                font-size: 11px;
                color: #000000;
                margin: 0px;
                text-align: right;                
                font-weight:bolder;              

            }
			.dillLink{
                cursor: pointer;
            }
        </style>
    </head>
    <body>
      <form id="frmGRNApprovalReport" name="frmGRNApprovalReport" method="post" action="customerBalance_summeryRpt.php">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td colspan="3"></td>
                </tr>
                <tr>
                    <td width="20%"></td>
                    <td width="60%" height="80" valign="top"><?php include '../../reportHeader.php' ?></td>
                    <td width="20%"></td>
                </tr>

                <tr>
                    <td colspan="3"></td>
                </tr>
            </table>
            <div align="center">
                <div style="background-color:#FFF" ><strong>Other Payable / Service Supplier -  Aging Details AS AT <?php echo $toDay; ?></strong><strong></strong><a class="button green small" href="<?php echo $ledgerURL?>" target="SupplierLedgerRpt.php">View Ledger</a></div>                
                <table width="50%">
                    <tr>                    
                        <th width="6%" class="normalfnt"><strong>Currency:</strong></th>
                        <th width="16%" align="left" class="normalfnt"> <?php echo $currencyDesc ?> </th>
                        <?php
                        if ($filterCompany == 0) {
                            ?>
                            <td width="8%" class="normalfnt"><strong>All Groups</strong></td>
                            <?php
                        } else {
                            ?>
                            <td width="49%" class="normalfnt"><strong>Company :</strong><?php echo $filterCompName ?></td>
                            <?php
                        }
                        ?>
                    </tr>
                </table>
<?php 
	foreach ($suppliers as $supId)
	{ 
		$result = $db->RunQuery("SELECT strName,strAddress FROM mst_finance_service_supplier WHERE intId=$supId");
		$row 	= mysqli_fetch_array($result);
		$supName 	= $row['strName'];
		$supAddress = $row['strAddress'];
		$amountTot	= 0;
		$toBeTot	= 0;
?>
        <div class="custname" ><strong>Supplier:  <?php echo $supName; ?> - <?php echo $supAddress; ?></strong><strong></strong></div>
        <table class="maintable">
            <tr class="maintableHeader">
                <td>PO No</td>
                <td>GRN No</td>
                <td>Document No</td>
                <td>Date</td>
                <td>Amount</td>
                <td>To be Paid</td>
            </tr>
            <tr class="subHed"><td colspan="5">Debit Notes</td></tr>
<?php
		if($filterCompany==0)
			$ware	= '';
		else 
			$ware	= " AND  fin_other_payable_debitnote_header.intCompanyId=$filterCompany ";
		
		$sql = "SELECT
				fin_other_payable_debitnote_header.strDebitNoteNo,			
				fin_other_payable_debitnote_header.strInvoiceNo,
				fin_other_payable_debitnote_header.dtDate,				
				SUM(((CRN.dblQty*CRN.dblRate) *(100-CRN.dblDiscount)/100)+ IFNULL(CRN.dblTax,0)) AS amount,
				(
				SUM(((CRN.dblQty*CRN.dblRate) *(100-CRN.dblDiscount)/100)+ IFNULL(CRN.dblTax,0))
				+
				IFNULL ((SELECT
				Sum(fin_other_payable_payments_main_details.dblPayAmount )AS paidAmount
				FROM fin_other_payable_payments_main_details
				Inner Join fin_other_payable_payments_header 
					ON fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
				WHERE
				fin_other_payable_payments_main_details.strDocNo =  CRN.strDebitNoteNo AND
				fin_other_payable_payments_header.intDeleteStatus =  '0' AND 
				fin_other_payable_payments_header.dtmDate<='$toDay' AND
				fin_other_payable_payments_main_details.strDocType =  'O.DNote'
				GROUP BY
				fin_other_payable_payments_main_details.strDocNo),0)
				) AS balAmount
				FROM
				fin_other_payable_debitnote_details AS CRN
				Inner Join fin_other_payable_debitnote_header 
					ON CRN.strDebitNoteNo = fin_other_payable_debitnote_header.strDebitNoteNo				
				WHERE
					fin_other_payable_debitnote_header.intsupplier =  '$supId' AND
					fin_other_payable_debitnote_header.intStatus = '1' AND
					fin_other_payable_debitnote_header.dtDate <='$toDay' AND
					fin_other_payable_debitnote_header.intCurrency = $currency ".$ware.
				"GROUP BY
				CRN.strDebitNoteNo
				having balAmount<>0";
		$result = $db->RunQuery($sql);
		$crditAmountTot	= 0;
		$crditTobeTot	= 0;
		while($row=mysqli_fetch_array($result))
		{
			$debit 		= $row['strDebitNoteNo'];
			$invoice 	= $row['strInvoiceNo'];
			$amount 	= number_format(($row['amount'] *(-1)),4,'.','');
			$toBePaid 	= number_format(($row['balAmount']*(-1)),4,'.','');
			$date 		= $row['dtDate'];	
?>
            <tr class="">
                <td class="normalfntMid"></td>
                <td class="normalfntMid"></td>
                <td class="normalfntMid"><?php echo $debit?></td>
                <td class="normalfntMid"><?php echo $date?></td>
                <td class="figurs"><?php echo number_format($amount,4)?></td>
                <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
            </tr>           
<?php
            $crditAmountTot	+= $amount;
            $crditTobeTot	+= $toBePaid;           
        }
        	$amountTot		+= $crditAmountTot;
            $toBeTot		+= $crditTobeTot;
?>
                    <tr>
                        <td class="total1" colspan="4">Total</td>                        
                        <td class="total"><?php echo number_format($crditAmountTot,4)?></td>
                        <td class="total"><?php echo number_format($crditTobeTot,4)?></td>
                    </tr>
                    <tr class="subHed"><td colspan="5">Advance Payments</td></tr>
                    <tr class="subHed1"><td colspan="5">0-30 Days</td></tr>
<?php
		if($filterCompany==0) 
			$ware		= ' ';
		else 
			$ware		= " AND ADV.intCompanyId=$filterCompany ";
			
		$adv30AmountTot	= 0;
		$adv30TobeTot	= 0;
		
		$sql="	SELECT
		ADV.strReceiptNo,				
		ADV.dtDate,				
		ADV.dblReceivedAmount,				
		(
		ADV.dblReceivedAmount 
		+
		IFNULL ((SELECT
		Sum(fin_other_payable_payments_main_details.dblPayAmount )AS paidAmount
		FROM fin_other_payable_payments_main_details
		Inner Join fin_other_payable_payments_header 
			ON fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
		WHERE	
			fin_other_payable_payments_main_details.strDocNo =  ADV.strReceiptNo AND
			fin_other_payable_payments_header.intDeleteStatus =  '0' AND 
			fin_other_payable_payments_header.dtmDate<='$toDay' AND
			fin_other_payable_payments_main_details.strDocType =  'O.APayment'
		GROUP BY
		fin_other_payable_payments_main_details.strDocNo),0)		
		) AS balAmount
		FROM
		fin_other_payable_advancepayment_header AS ADV				
		WHERE
			ADV.intsupplier =  '$supId' AND
			ADV.intStatus = '1' AND
			ADV.intCurrency=$currency AND 
			ADV.dtDate<='$toDay' AND
			DATEDIFF('$toDay',ADV.dtDate) <=30".$ware.
		" having balAmount<>0 ";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$receipt 	= $row['strReceiptNo'];
			$amount 	= number_format(($row['dblReceivedAmount']*(-1)),4,'.','');
			$toBePaid 	= number_format(($row['balAmount']*(-1)),4,'.','');
			$date 		= $row['dtDate'];                    		
?>
			<tr class="">
				<td class="normalfntMid"></td>
				<td class="normalfntMid"></td>
				<td class="normalfntMid"><?php echo $receipt?></td>
				<td class="normalfntMid"><?php echo $date ?></td>
				<td class="figurs"><?php echo number_format($amount,4)?></td>
				<td class="figurs"><?php echo number_format($toBePaid,4)?></td>
			</tr>			
<?php
			$adv30AmountTot	+= $amount;
			$adv30TobeTot	+= $toBePaid;
		}
			$amountTot		+= $adv30AmountTot;
			$toBeTot		+= $adv30TobeTot;
?>
        <tr>
            <td class="total1" colspan="4">Total</td>                        
            <td class="total"><?php echo number_format($adv30AmountTot,4)?></td>
            <td class="total"><?php echo number_format($adv30TobeTot,4)?></td>
        </tr>
        	<tr class="subHed1">
            	<td colspan="5">31-60 Days</td>
        </tr>
<?php
		if($filterCompany==0) 
			$ware		= ' ';
		else 
			$ware		= " AND ADV.intCompanyId=$filterCompany ";
		
		$adv60AmountTot	= 0;
		$adv60TobeTot	= 0;
		
		$sql="	SELECT
		ADV.strReceiptNo,				
		ADV.dtDate,				
		ADV.dblReceivedAmount,				
		(
		ADV.dblReceivedAmount 
		+
		IFNULL ((SELECT
		Sum(fin_other_payable_payments_main_details.dblPayAmount )AS paidAmount
		FROM fin_other_payable_payments_main_details
		Inner Join fin_other_payable_payments_header 
			ON fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
		WHERE
			fin_other_payable_payments_main_details.strDocNo =  ADV.strReceiptNo AND
			fin_other_payable_payments_header.intDeleteStatus =  '0' AND 
			fin_other_payable_payments_header.dtmDate<='$toDay' AND
			fin_other_payable_payments_main_details.strDocType =  'O.APayment'
		GROUP BY
		fin_other_payable_payments_main_details.strDocNo),0)
		
		) AS balAmount
		FROM
		fin_other_payable_advancepayment_header AS ADV				
		WHERE
			ADV.intsupplier =  '$supId' AND
			ADV.intStatus = '1' AND ADV.intCurrency=$currency AND 
			ADV.dtDate<='$toDay' AND
			DATEDIFF('$toDay',ADV.dtDate) <=60 AND
			DATEDIFF('$toDay',ADV.dtDate) >=31".$ware.
		" having balAmount<>0";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$receipt 	= $row['strReceiptNo'];
			$amount 	= number_format(($row['dblReceivedAmount']*(-1)),4,'.','');
			$toBePaid 	= number_format(($row['balAmount']*(-1)),4,'.','');
			$date 		= $row['dtDate'];                    		
?>
            <tr class="">
                <td class="normalfntMid"></td>
                <td class="normalfntMid"></td>
                <td class="normalfntMid"><?php echo $receipt?></td>
                <td class="normalfntMid"><?php echo $date ?></td>
                <td class="figurs"><?php echo number_format($amount,4)?></td>
                <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
            </tr>                    
<?php
			$adv60AmountTot	+= $amount;
			$adv60TobeTot	+= $toBePaid;                   
		}
			$amountTot		+= $adv60AmountTot;
			$toBeTot		+= $adv60TobeTot;
?>
            <tr>
                <td class="total1" colspan="4">Total</td>                        
                <td class="total"><?php echo number_format($adv60AmountTot,4)?></td>
                <td class="total"><?php echo number_format($adv60TobeTot,4)?></td>
            </tr>
                    
            <tr class="subHed1">
              <td colspan="5">61-90 Days</td></tr>
<?php
		if($filterCompany==0) 
			$ware		= ' ';
		else 
			$ware		= " AND ADV.intCompanyId=$filterCompany ";
					
		$adv90AmountTot	= 0;
		$adv90TobeTot	= 0;
		
		$sql="	SELECT
		ADV.strReceiptNo,				
		ADV.dtDate,				
		ADV.dblReceivedAmount,				
		(
		ADV.dblReceivedAmount 
		+
		IFNULL ((SELECT
		Sum(fin_other_payable_payments_main_details.dblPayAmount )AS paidAmount
		FROM fin_other_payable_payments_main_details
		Inner Join fin_other_payable_payments_header 
			ON fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
		WHERE
		fin_other_payable_payments_main_details.strDocNo =  ADV.strReceiptNo AND
		fin_other_payable_payments_header.intDeleteStatus =  '0' AND fin_other_payable_payments_header.dtmDate<='$toDay' AND
		fin_other_payable_payments_main_details.strDocType =  'O.APayment'
		GROUP BY
		fin_other_payable_payments_main_details.strDocNo),0)
		
		) AS balAmount
		FROM
		fin_other_payable_advancepayment_header AS ADV				
		WHERE
		ADV.intsupplier =  '$supId' AND
		ADV.intStatus = '1' AND 
						ADV.intCurrency=$currency AND 
						ADV.dtDate<='$toDay' AND
						DATEDIFF('$toDay',ADV.dtDate) <=90 AND
						DATEDIFF('$toDay',ADV.dtDate) >=61".$ware.
		" having balAmount<>0";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result)){
                    $receipt = $row['strReceiptNo'];
                    $amount = number_format(($row['dblReceivedAmount']*(-1)),4,'.','');
                    $toBePaid = number_format(($row['balAmount']*(-1)),4,'.','');
                    $date = $row['dtDate'];                    		
?>
                    <tr class="">
                        <td class="normalfntMid"></td>
                        <td class="normalfntMid"></td>
                        <td class="normalfntMid"><?php echo $receipt?></td>
                        <td class="normalfntMid"><?php echo $date ?></td>
                        <td class="figurs"><?php echo number_format($amount,4)?></td>
                        <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
                    </tr>
                    
<?php
                    $adv90AmountTot+=$amount;
                    $adv90TobeTot+=$toBePaid;
                    
                    
                }
                $amountTot+=$adv90AmountTot;
                    $toBeTot+=$adv90TobeTot;
?>
                    <tr>
                        <td class="total1" colspan="4">Total</td>                        
                        <td class="total"><?php echo number_format($adv90AmountTot,4)?></td>
                        <td class="total"><?php echo number_format($adv90TobeTot,4)?></td>
                    </tr>
                    <tr class="subHed1"><td colspan="5">More Than 90 Days</td></tr>
<?php
		if($filterCompany==0) 
			$ware			= ' ';
		else 
			$ware			= " AND ADV.intCompanyId=$filterCompany ";
		
		$adv91AmountTot		= 0;
		$adv91TobeTot		= 0;
		
		$sql="	SELECT
		ADV.strReceiptNo,				
		ADV.dtDate,				
		ADV.dblReceivedAmount,				
		(
		ADV.dblReceivedAmount 
		+
		IFNULL ((SELECT
		Sum(fin_other_payable_payments_main_details.dblPayAmount )AS paidAmount
		FROM fin_other_payable_payments_main_details
		Inner Join fin_other_payable_payments_header 
			ON fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
		WHERE
		fin_other_payable_payments_main_details.strDocNo =  ADV.strReceiptNo AND
		fin_other_payable_payments_header.intDeleteStatus =  '0' AND fin_other_payable_payments_header.dtmDate<='$toDay' AND
		fin_other_payable_payments_main_details.strDocType =  'O.APayment'
		GROUP BY
		fin_other_payable_payments_main_details.strDocNo),0)
		
		) AS balAmount
		FROM
		fin_other_payable_advancepayment_header AS ADV				
		WHERE
		ADV.intsupplier =  '$supId' AND
		ADV.intStatus = '1' AND
		ADV.intCurrency=$currency AND
		ADV.dtDate<='$toDay' AND
		DATEDIFF('$toDay',ADV.dtDate) >=91".$ware.
		" having balAmount<>0";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$receipt 	= $row['strReceiptNo'];
			$amount 	= number_format(($row['dblReceivedAmount']*(-1)),4,'.','');
			$toBePaid 	= number_format(($row['balAmount']*(-1)),4,'.','');
			$date 		= $row['dtDate'];                    		
?>
			<tr class="">
				<td class="normalfntMid"></td>
				<td class="normalfntMid"></td>
				<td class="normalfntMid"><?php echo $receipt?></td>
				<td class="normalfntMid"><?php echo $date ?></td>
				<td class="figurs"><?php echo number_format($amount,4)?></td>
				<td class="figurs"><?php echo number_format($toBePaid,4)?></td>
			</tr>                    
<?php
			$adv91AmountTot	+= $amount;
			$adv91TobeTot	+= $toBePaid;
		}
			$amountTot	+= $adv91AmountTot;
			$toBeTot	+= $adv91TobeTot;
?>
                    <tr>
                        <td class="total1" colspan="4">Total</td>                        
                        <td class="total"><?php echo number_format($adv91AmountTot,4)?></td>
                        <td class="total"><?php echo number_format($adv91TobeTot,4)?></td>
                    </tr>
                    
                    <tr class="subHed"><td colspan="5">Invoice</td></tr>
                    <tr class="subHed1"><td colspan="5">Current</td></tr>
<?php
		if($filterCompany==0)
	   		$wareCom		= " ";
		else 
			$wareCom		= " AND fin_other_payable_bill_header.intCompanyId=$filterCompany";
			
		$invCurrAmountTot	= 0;
		$invCurrTobeTot		= 0;
		
		$sql="SELECT
		fin_other_payable_bill_header.strReferenceNo,
		fin_other_payable_bill_header.strPoNo,
		fin_other_payable_bill_header.strSupInvoice,
		fin_other_payable_bill_header.strGrnNo,				
		fin_other_payable_bill_header.intInvoiceNo,
		fin_other_payable_bill_header.dtmDate,				
		sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) AS amount,
		(
		sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) 
		-
		IFNULL ((SELECT
		Sum(fin_other_payable_payments_main_details.dblPayAmount )AS paidAmount
		FROM fin_other_payable_payments_main_details
		Inner Join fin_other_payable_payments_header 
			ON fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
		WHERE
			fin_other_payable_payments_main_details.strDocNo =  INV.strReferenceNo AND
			fin_other_payable_payments_header.intDeleteStatus =  '0' AND 
			fin_other_payable_payments_header.dtmDate<='$toDay' AND 
			fin_other_payable_payments_main_details.strDocType = 'O.Bill'
		GROUP BY
		fin_other_payable_payments_main_details.strDocNo),0)
		
		) AS balAmount
		FROM
		fin_other_payable_bill_details AS INV
		Inner Join fin_other_payable_bill_header 
			ON fin_other_payable_bill_header.strReferenceNo = INV.strReferenceNo AND 
			INV.intInvoiceNo = fin_other_payable_bill_header.intInvoiceNo AND 
			INV.intAccPeriodId = fin_other_payable_bill_header.intAccPeriodId AND 
			INV.intLocationId = fin_other_payable_bill_header.intLocationId AND 
			INV.intCompanyId = fin_other_payable_bill_header.intCompanyId
		Inner Join mst_finance_service_supplier 
			ON mst_finance_service_supplier.intId = fin_other_payable_bill_header.intsupplierId
		INNER JOIN mst_financepaymentsterms AS PT 
			ON fin_other_payable_bill_header.intPaymentsTermsId = PT.intId
		WHERE
		fin_other_payable_bill_header.intsupplierId =  '$supId' AND
		fin_other_payable_bill_header.intDeleteStatus =  '0' AND
					fin_other_payable_bill_header.intCurrencyId = $currency AND
					fin_other_payable_bill_header.dtmDate <='$toDay' AND
					DATEDIFF('$toDay',fin_other_payable_bill_header.dtmDate) < (PT.strName) ".$wareCom." 
					GROUP BY
		fin_other_payable_bill_header.strReferenceNo
		having balAmount<>0";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{			
			$invoice 	= $row['strReferenceNo'];
			$supInvoice = $row['strSupInvoice'];
			$poNo 		= $row['strPoNo'];
			$grnNo 		= $row['strGrnNo'];
			$amount 	= number_format($row['amount'],4,'.','');
			$toBePaid 	= number_format($row['balAmount'],4,'.','');
			$date 		= $row['dtmDate'];
?>
			<tr onclick=" leadgerDrill('Other Bill','<?php echo $invoice ?>')" class="dillLink">
				<td class="normalfntMid"><?php echo $poNo?></td>
				<td class="normalfntMid"><?php echo $grnNo?></td>
				<td class="normalfntMid"><?php echo $supInvoice?></td>
				<td class="normalfntMid"><?php echo $date ?></td>
				<td class="figurs"><?php echo number_format($amount,4)?></td>
				<td class="figurs"><?php echo number_format($toBePaid,4)?></td>
			</tr>		
<?php
			$invCurrAmountTot	+= $amount;
			$invCurrTobeTot		+= $toBePaid;
		}
			$amountTot			+= $invCurrAmountTot;
			$toBeTot			+= $invCurrTobeTot;
?>
                    <tr>
                        <td class="total1" colspan="4">Total</td>                        
                        <td class="total"><?php echo number_format($invCurrAmountTot,4)?></td>
                        <td class="total"><?php echo number_format($invCurrTobeTot,4)?></td>
                    </tr>
                <tr class="subHed1"><td colspan="5">0 - 30</td></tr>
                    <!-- Invoice -->
<?php
		if($filterCompany==0)
			$wareCom	= " ";
		else 
			$wareCom	= " AND fin_other_payable_bill_header.intCompanyId=$filterCompany ";
			
		$inv30AmountTot	= 0;
		$inv30TobeTot	= 0;
		
		$sql="SELECT
		fin_other_payable_bill_header.strReferenceNo,
		fin_other_payable_bill_header.strPoNo,
		fin_other_payable_bill_header.strSupInvoice,
		fin_other_payable_bill_header.strGrnNo,	
		fin_other_payable_bill_header.dtmDate,			
		fin_other_payable_bill_header.intInvoiceNo,				
		sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) AS amount,
		(
		sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) 
		-
		IFNULL ((SELECT
		Sum(fin_other_payable_payments_main_details.dblPayAmount )AS paidAmount
		FROM fin_other_payable_payments_main_details
		Inner Join fin_other_payable_payments_header 
			ON fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
		WHERE
			fin_other_payable_payments_main_details.strDocNo = INV.strReferenceNo AND
			fin_other_payable_payments_header.intDeleteStatus = '0' AND 
			fin_other_payable_payments_header.dtmDate <= '$toDay' AND 
			fin_other_payable_payments_main_details.strDocType = 'O.Bill'
		GROUP BY
		fin_other_payable_payments_main_details.strDocNo),0)				
		) AS balAmount
		FROM
		fin_other_payable_bill_details AS INV
		Inner Join fin_other_payable_bill_header 
			ON fin_other_payable_bill_header.strReferenceNo = INV.strReferenceNo AND 
			INV.intInvoiceNo = fin_other_payable_bill_header.intInvoiceNo AND 
			INV.intAccPeriodId = fin_other_payable_bill_header.intAccPeriodId AND 
			INV.intLocationId = fin_other_payable_bill_header.intLocationId AND 
			INV.intCompanyId = fin_other_payable_bill_header.intCompanyId
		Inner Join mst_finance_service_supplier 
			ON mst_finance_service_supplier.intId = fin_other_payable_bill_header.intsupplierId
		INNER JOIN mst_financepaymentsterms AS PT 
			ON fin_other_payable_bill_header.intPaymentsTermsId = PT.intId
		WHERE
			fin_other_payable_bill_header.intsupplierId =  '$supId' AND
			fin_other_payable_bill_header.intDeleteStatus =  '0' AND
			fin_other_payable_bill_header.intCurrencyId = $currency AND
			fin_other_payable_bill_header.dtmDate <='$toDay' AND
			DATEDIFF('$toDay',fin_other_payable_bill_header.dtmDate) >= (0+ PT.strName) AND
			DATEDIFF('$toDay',fin_other_payable_bill_header.dtmDate) <=(30+ PT.strName)
						   ".$wareCom.
		" GROUP BY
		fin_other_payable_bill_header.strReferenceNo
		having balAmount<>0";
		//die($sql);
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{                    
			$invoice 	= $row['strReferenceNo'];
			$supInvoice = $row['strSupInvoice'];
			$poNo 		= $row['strPoNo'];
			$grnNo 		= $row['strGrnNo'];
			$amount 	= number_format($row['amount'],4,'.','');
			$toBePaid	= number_format($row['balAmount'],4,'.','');
			$date 		= $row['dtmDate'];
?>
			<tr onclick=" leadgerDrill('Other Bill','<?php echo $invoice ?>')" class="dillLink">
				<td class="normalfntMid"><?php echo $poNo?></td>
				<td class="normalfntMid"><?php echo $grnNo?></td>
				<td class="normalfntMid"><?php echo $supInvoice?></td>
				<td class="normalfntMid"><?php echo $date ?></td>
				<td class="figurs"><?php echo number_format($amount,4)?></td>
				<td class="figurs"><?php echo number_format($toBePaid,4)?></td>
			</tr>			
<?php
			$inv30AmountTot	+= $amount;
			$inv30TobeTot	+= $toBePaid;
		}
?>
                <!-- Bank Deposit -->                
<?php
		if($filterCompany==0)
			$wareCom = " ";
		else 
			$wareCom = " AND BND.intCompanyId=$filterCompany ";
			
		$sql = "SELECT
		BND.strDepositNo,
		BND.dtDate,
						fin_bankdeposit_details.dblAmmount,
		(
		fin_bankdeposit_details.dblAmmount 
		-
		IFNULL ((SELECT
		Sum(fin_other_payable_payments_main_details.dblPayAmount )AS paidAmount
		FROM fin_other_payable_payments_main_details
		Inner Join fin_other_payable_payments_header 
			ON fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
		WHERE
			fin_other_payable_payments_main_details.strDocNo =  BND.strDepositNo AND
			fin_other_payable_payments_header.intDeleteStatus =  '0' AND 
			fin_other_payable_payments_header.dtmDate<='$toDay' AND 
			fin_other_payable_payments_main_details.strDocType = 'B.Deposit'
		GROUP BY
		fin_other_payable_payments_main_details.strDocNo),0)
		
		) AS balAmount
		FROM
		fin_bankdeposit_header BND
		Inner Join fin_bankdeposit_details 
			ON fin_bankdeposit_details.strDepositNo = BND.strDepositNo				
		Inner Join mst_financechartofaccounts 
			ON fin_bankdeposit_details.intAccount = mst_financechartofaccounts.intId
		WHERE
			fin_bankdeposit_details.intRecvFrom =  '$supId' AND
			(mst_financechartofaccounts.intFinancialTypeId = '26' OR 
			mst_financechartofaccounts.intFinancialTypeId = '16' OR 
			mst_financechartofaccounts.intFinancialTypeId = '17' OR 
			mst_financechartofaccounts.intFinancialTypeId = '28') AND
			BND.intStatus = '1' AND
			BND.intCurrency=$currency AND
			BND.dtDate <='$toDay' AND
			DATEDIFF('$toDay',BND.dtDate) >=0 AND
			DATEDIFF('$toDay',BND.dtDate) <=30".$wareCom.
		" having balAmount<>0";
		//die($sql);
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$deposit 	= $row['strDepositNo'];
			$amount 	= number_format(($row['dblAmmount']),4,'.','');
			$toBePaid 	= number_format(($row['balAmount']),4,'.','');
			$date 		= substr($row['dtDate'],0,10);
?>
                
                    <tr class="">
                        <td class="normalfntMid"></td>
                        <td class="normalfntMid"></td>
                        <td class="normalfntMid"><?php echo $deposit?></td>
                        <td class="normalfntMid"><?php echo $date ?></td>
                        <td class="figurs"><?php echo number_format($amount,4)?></td>
                        <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
                    </tr>
                    
<?php
                    $inv30AmountTot	+= $amount;
                    $inv30TobeTot	+= $toBePaid;
		}
?>
                <!-- Bank Payments -->                                
<?php
                if($filterCompany==0)
					$wareCom	= " ";
                else 
					$wareCom	= " AND BNP.intCompanyId=$filterCompany ";
					
                $sql = "SELECT
				BNP.strBankPaymentNo,
				BNP.dtDate,
                                BNP.dblReceivedAmount,
								fin_bankpayment_details.dblAmmount,
				(
				fin_bankpayment_details.dblAmmount 
				+
				IFNULL ((SELECT
				Sum(fin_other_payable_payments_main_details.dblPayAmount )AS paidAmount
				FROM fin_other_payable_payments_main_details
				Inner Join fin_other_payable_payments_header 
					ON fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
				WHERE
				fin_other_payable_payments_main_details.strDocNo =  BNP.strBankPaymentNo AND
				fin_other_payable_payments_header.intDeleteStatus =  '0' AND 
					fin_other_payable_payments_header.dtmDate<='$toDay' AND 
					fin_other_payable_payments_main_details.strDocType = 'B.Payment'
				GROUP BY
				fin_other_payable_payments_main_details.strDocNo),0)
				
				) AS balAmount
				FROM
				fin_bankpayment_header BNP
				Inner Join fin_bankpayment_details 
					ON fin_bankpayment_details.strBankPaymentNo = BNP.strBankPaymentNo				
				Inner Join mst_financechartofaccounts 
					ON fin_bankpayment_details.intAccountId = mst_financechartofaccounts.intId
				WHERE
				fin_bankpayment_details.intPayTo =  '$supId' AND
				(mst_financechartofaccounts.intFinancialTypeId = '26' OR 
				mst_financechartofaccounts.intFinancialTypeId = '16' OR 
				mst_financechartofaccounts.intFinancialTypeId = '17' OR 
				mst_financechartofaccounts.intFinancialTypeId = '28') AND
				BNP.intStatus = '1' AND
				BNP.intCurrency=$currency AND
				BNP.dtDate <='$toDay' AND
				DATEDIFF('$toDay',BNP.dtDate)>= 0 AND
				DATEDIFF('$toDay',BNP.dtDate)<= 30 ".$wareCom. "
				having balAmount<>0";
		//die($sql);
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$payment 	= $row['strBankPaymentNo'];
			$amount 	= number_format(($row['dblAmmount']*(-1)),4,'.','');
			$toBePaid 	= number_format(($row['balAmount']*(-1)),4,'.','');
			$date 		= substr($row['dtDate'],0,10);
?>
                
                    <tr class="">
                        <td class="normalfntMid"></td>
                        <td class="normalfntMid"></td>
                        <td class="normalfntMid"><?php echo $payment?></td>
                        <td class="normalfntMid"><?php echo $date ?></td>
                        <td class="figurs"><?php echo number_format($amount,4)?></td>
                        <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
                    </tr>
                    
<?php
                    $inv30AmountTot	+= $amount;
                    $inv30TobeTot	+= $toBePaid;
		}
?>
                <!-- Petty Cash -->                                
<?php
                if($filterCompany==0)
					$wareCom	= " ";
                else 
					$wareCom	= " AND PTC.intCompanyId=$filterCompany ";
					
                 $sql = "SELECT
				PTC.strPettyCashNo,
				PTC.dtDate,
				PTC.dblReceivedAmount,
				fin_bankpettycash_details.dblAmmount,
				(
				fin_bankpettycash_details.dblAmmount 
				+
				IFNULL ((SELECT
				Sum(fin_other_payable_payments_main_details.dblPayAmount )AS paidAmount
				FROM fin_other_payable_payments_main_details
				Inner Join fin_other_payable_payments_header ON 
					fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
				WHERE
					fin_other_payable_payments_main_details.strDocNo =  PTC.strPettyCashNo AND
					fin_other_payable_payments_header.intDeleteStatus =  '0' AND 
					fin_other_payable_payments_header.dtmDate<='$toDay' AND 
					fin_other_payable_payments_main_details.strDocType = 'Petty Cash'
				GROUP BY
				fin_other_payable_payments_main_details.strDocNo),0)
				
				) AS balAmount
				FROM
				fin_bankpettycash_header AS PTC
				Inner Join fin_bankpettycash_details 
					ON fin_bankpettycash_details.strPettyCashNo = PTC.strPettyCashNo
				Inner Join mst_financechartofaccounts 
					ON fin_bankpettycash_details.intAccountId = mst_financechartofaccounts.intId
				WHERE
					(mst_financechartofaccounts.intFinancialTypeId = '26' OR 
					mst_financechartofaccounts.intFinancialTypeId = '16' OR 
					mst_financechartofaccounts.intFinancialTypeId = '17' OR 
					mst_financechartofaccounts.intFinancialTypeId = '28') AND 
					fin_bankpettycash_details.intPayTo =  '$supId' AND
					PTC.intStatus = '1' AND
					PTC.intCurrency=$currency AND
					PTC.dtDate <='$toDay' AND
					DATEDIFF('$toDay',PTC.dtDate)>= 0 AND
					DATEDIFF('$toDay',PTC.dtDate)<= 30".$wareCom.
				" having balAmount<>0";
		//die($sql);
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$pettyCash 	= $row['strPettyCashNo'];
			$amount		= number_format(($row['dblAmmount']*(-1)),4,'.','');
			$toBePaid 	= number_format(($row['balAmount']*(-1)),4,'.','');
			$date 		= substr($row['dtDate'],0,10);
?>
                
                    <tr class="">
                        <td class="normalfntMid"></td>
                        <td class="normalfntMid"></td>
                        <td class="normalfntMid"><?php echo $payment?></td>
                        <td class="normalfntMid"><?php echo $date ?></td>
                        <td class="figurs"><?php echo number_format($amount,4)?></td>
                        <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
                    </tr>
                    
<?php
                    $inv30AmountTot	+= $amount;
                    $inv30TobeTot	+= $toBePaid;
                }
?>
                <!-- Jurnel Entry -->                                
<?php
		if($filterCompany==0)
			$wareCom	= " ";
		else
			$wareCom	= " AND JH.intCompanyId=$filterCompany ";
			
		$sql = "SELECT
					JH.strReferenceNo,
					JH.dtmDate, 
					JD.dblDebitAmount,
					JD.dbCreditAmount,
					(
					JD.dbCreditAmount - JD.dblDebitAmount -
					IFNULL ((SELECT
					Sum(fin_other_payable_payments_main_details.dblPayAmount )AS paidAmount
					FROM
					fin_other_payable_payments_main_details
					Inner Join fin_other_payable_payments_header 
						ON fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
					Inner Join fin_accountant_journal_entry_details 
						ON fin_other_payable_payments_main_details.strDocNo = fin_accountant_journal_entry_details.strReferenceNo
					WHERE
					fin_other_payable_payments_main_details.strDocNo =  JH.strReferenceNo AND
					fin_other_payable_payments_header.intDeleteStatus =  '0' AND
					fin_other_payable_payments_header.dtmDate <=  '$toDay' AND
					fin_other_payable_payments_main_details.strDocType =  'JN' AND
					fin_other_payable_payments_header.intSupplierId =  '$supId' AND
					fin_accountant_journal_entry_details.strPersonType =  'osup' AND 
					fin_accountant_journal_entry_details.intChartOfAccountId =  JD.intChartOfAccountId
					GROUP BY
					fin_other_payable_payments_main_details.strDocNo),0)
		
		) AS balAmount
					FROM
					fin_accountant_journal_entry_header AS JH
					INNER JOIN fin_accountant_journal_entry_details AS JD 
						ON JH.strReferenceNo = JD.strReferenceNo                            
					INNER JOIN mst_financechartofaccounts 
						ON JD.intChartOfAccountId = mst_financechartofaccounts.intId
					WHERE
					JD.strPersonType = 'osup' AND
					JD.intNameId = $supId AND
					JH.intDeleteStatus = 0 AND
					(mst_financechartofaccounts.intFinancialTypeId = '26' OR mst_financechartofaccounts.intFinancialTypeId = '16' OR mst_financechartofaccounts.intFinancialTypeId = '17' OR mst_financechartofaccounts.intFinancialTypeId = '28') AND
					JH.intCurrencyId=$currency AND
					JH.dtmDate <='$toDay' AND
					DATEDIFF('$toDay', JH.dtmDate)>= 0 AND
					DATEDIFF('$toDay', JH.dtmDate)<= 30".$wareCom.
					" HAVING balAmount <> 0";
		//die($sql);
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$payment 	= $row['strReferenceNo'];
			$amount 	= number_format(($row['dbCreditAmount'] - $row['dblDebitAmount']),4,'.','');
                        
					/*
					comment by roshan (2013-04-26) for minus debet value convert to 0;
					
					 //if($row['dblDebitAmount']!=0){
                            //$toBePaid = number_format(($row['balAmount']*(-1)),4,'.','');
                        //}
                        //else if($row['dbCreditAmount']!=0){
                            $toBePaid = number_format(($row['balAmount']),4,'.','');
                        //} */   
						
		/*	if($row['balAmount']<0){
				$toBePaid = number_format(0,4,'.','');
			}*/
			 $toBePaid =   $row['balAmount'];                      
			$date = substr($row['dtmDate'],0,10);
?>                
                    <tr class="">
                        <td class="normalfntMid"></td>
                        <td class="normalfntMid">&nbsp;</td>
                        <td class="normalfntMid"><?php echo $payment?></td>
                        <td class="normalfntMid"><?php echo $date ?></td>
                        <td class="figurs"><?php echo number_format($amount,4)?></td>
                        <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
                    </tr>                    
<?php
			$inv30AmountTot	+= $amount;
			$inv30TobeTot	+= $toBePaid;                   
		}
			$amountTot		+= $inv30AmountTot;
			$toBeTot		+= $inv30TobeTot;
?>
                <tr>
                    <td class="total1" colspan="4">Total</td>                        
                    <td class="total"><?php echo number_format($inv30AmountTot,4)?></td>
                    <td class="total"><?php echo number_format($inv30TobeTot,4)?></td>
                </tr>
                <tr class="subHed1"><td colspan="5">31 - 60</td></tr>
                    <!-- Invoice -->
<?php
		if($filterCompany==0)
			$wareCom 	= " ";
		else 
			$wareCom	= " AND fin_other_payable_bill_header.intCompanyId=$filterCompany ";
			
		$inv60AmountTot	= 0;
		$inv60TobeTot	= 0;
		
		$sql="SELECT
		fin_other_payable_bill_header.strReferenceNo,
		fin_other_payable_bill_header.strPoNo,
		fin_other_payable_bill_header.strSupInvoice,
		fin_other_payable_bill_header.strGrnNo,
		fin_other_payable_bill_header.dtmDate,				
		fin_other_payable_bill_header.intInvoiceNo,				
		sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) AS amount,
		(
		sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) 
		-
		IFNULL ((SELECT
		Sum(fin_other_payable_payments_main_details.dblPayAmount )AS paidAmount
		FROM fin_other_payable_payments_main_details
		Inner Join fin_other_payable_payments_header 
			ON fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
		WHERE
			fin_other_payable_payments_main_details.strDocNo =  INV.strReferenceNo AND
			fin_other_payable_payments_header.intDeleteStatus =  '0' AND 
			fin_other_payable_payments_header.dtmDate<='$toDay' AND 
			fin_other_payable_payments_main_details.strDocType = 'O.Bill' 
		GROUP BY
		fin_other_payable_payments_main_details.strDocNo),0)
		
		) AS balAmount
		FROM
		fin_other_payable_bill_details AS INV
		Inner Join fin_other_payable_bill_header 
			ON fin_other_payable_bill_header.strReferenceNo = INV.strReferenceNo AND 
			INV.intInvoiceNo = fin_other_payable_bill_header.intInvoiceNo AND 
			INV.intAccPeriodId = fin_other_payable_bill_header.intAccPeriodId AND 
			INV.intLocationId = fin_other_payable_bill_header.intLocationId AND 
			INV.intCompanyId = fin_other_payable_bill_header.intCompanyId
		Inner Join mst_finance_service_supplier 
			ON mst_finance_service_supplier.intId = fin_other_payable_bill_header.intsupplierId
		INNER JOIN mst_financepaymentsterms AS PT 
			ON fin_other_payable_bill_header.intPaymentsTermsId = PT.intId
		WHERE
		fin_other_payable_bill_header.intsupplierId =  '$supId' AND
		fin_other_payable_bill_header.intDeleteStatus =  '0' AND
							fin_other_payable_bill_header.intCurrencyId = $currency AND
							fin_other_payable_bill_header.dtmDate <='$toDay' AND
							DATEDIFF('$toDay',fin_other_payable_bill_header.dtmDate) >= (31+ PT.strName) AND
							DATEDIFF('$toDay',fin_other_payable_bill_header.dtmDate) <=(60+ PT.strName)
						   ".$wareCom.
		" GROUP BY
		fin_other_payable_bill_header.strReferenceNo
		having balAmount<>0";
		
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{				
			$invoice 	= $row['strReferenceNo'];
			$supInvoice = $row['strSupInvoice'];
			$poNo 		= $row['strPoNo'];
			$grnNo 		= $row['strGrnNo'];
			$amount 	= number_format($row['amount'],4,'.','');
			$toBePaid 	= number_format($row['balAmount'],4,'.','');
			$date 		= $row['dtmDate'];
?>
            <tr onclick=" leadgerDrill('Other Bill','<?php echo $invoice ?>')" class="dillLink">
                <td class="normalfntMid"><?php echo $poNo?></td>
                <td class="normalfntMid"><?php echo $grnNo?></td>
                <td class="normalfntMid"><?php echo $supInvoice?></td>
                <td class="normalfntMid"><?php echo $date ?></td>
                <td class="figurs"><?php echo number_format($amount,4)?></td>
                <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
            </tr>
                    
<?php
			$inv60AmountTot+=$amount;
			$inv60TobeTot+=$toBePaid;
		}
?>
                <!-- Bank Deposit -->                
<?php
		if($filterCompany==0)
			$wareCom	= " ";
		else 
			$wareCom	= " AND BND.intCompanyId=$filterCompany ";
			
		$sql = "SELECT
		BND.strDepositNo,
		BND.dtDate,
		fin_bankdeposit_details.dblAmmount,
		(
		fin_bankdeposit_details.dblAmmount 
		-
		IFNULL ((SELECT
		Sum(fin_other_payable_payments_main_details.dblPayAmount )AS paidAmount
		FROM fin_other_payable_payments_main_details
		Inner Join fin_other_payable_payments_header 
			ON fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
		WHERE
			fin_other_payable_payments_main_details.strDocNo = BND.strDepositNo AND
			fin_other_payable_payments_header.intDeleteStatus = '0' AND 
			fin_other_payable_payments_header.dtmDate <= '$toDay' AND 
			fin_other_payable_payments_main_details.strDocType = 'B.Deposit'
		GROUP BY
		fin_other_payable_payments_main_details.strDocNo),0)
		
		) AS balAmount
		FROM
		fin_bankdeposit_header BND
		Inner Join fin_bankdeposit_details 
			ON fin_bankdeposit_details.strDepositNo = BND.strDepositNo				
		Inner Join mst_financechartofaccounts 
			ON fin_bankdeposit_details.intAccount = mst_financechartofaccounts.intId
		WHERE
			fin_bankdeposit_details.intRecvFrom =  '$supId' AND
			(mst_financechartofaccounts.intFinancialTypeId = '26' OR 
			mst_financechartofaccounts.intFinancialTypeId = '16' OR 
			mst_financechartofaccounts.intFinancialTypeId = '17' OR 
			mst_financechartofaccounts.intFinancialTypeId = '28') AND
			BND.intStatus = '1' AND
			BND.intCurrency=$currency AND
			BND.dtDate <='$toDay' AND
			DATEDIFF('$toDay',BND.dtDate) >=31 AND
			DATEDIFF('$toDay',BND.dtDate) <=60".$wareCom.
		" having balAmount<>0";
		
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$deposit 	= $row['strDepositNo'];
			$amount 	= number_format(($row['dblAmmount']),4,'.','');
			$toBePaid 	= number_format(($row['balAmount']),4,'.','');
			$date 		= substr($row['dtDate'],0,10);
?>
                
                    <tr class="">
                        <td class="normalfntMid"></td>
                        <td class="normalfntMid"></td>
                        <td class="normalfntMid"><?php echo $deposit?></td>
                        <td class="normalfntMid"><?php echo $date ?></td>
                        <td class="figurs"><?php echo number_format($amount,4)?></td>
                        <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
                    </tr>
                    
<?php
			$inv60AmountTot	+= $amount;
			$inv60TobeTot	+= $toBePaid;
		}
?>
                <!-- Bank Payments -->                                
<?php
		if($filterCompany==0)
			$wareCom	= " ";
		else 
			$wareCom	= " AND BNP.intCompanyId=$filterCompany ";
			
		$sql = "SELECT
		BNP.strBankPaymentNo,
		BNP.dtDate,
						BNP.dblReceivedAmount,
						fin_bankpayment_details.dblAmmount,
		(
		fin_bankpayment_details.dblAmmount 
		+
		IFNULL ((SELECT
		Sum(fin_other_payable_payments_main_details.dblPayAmount )AS paidAmount
		FROM fin_other_payable_payments_main_details
		Inner Join fin_other_payable_payments_header 
			ON fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
		WHERE
			fin_other_payable_payments_main_details.strDocNo =  BNP.strBankPaymentNo AND
			fin_other_payable_payments_header.intDeleteStatus =  '0' AND 
			fin_other_payable_payments_header.dtmDate<='$toDay' AND 
			fin_other_payable_payments_main_details.strDocType = 'B.Payment'
		GROUP BY
		fin_other_payable_payments_main_details.strDocNo),0)
		
		) AS balAmount
		FROM
		fin_bankpayment_header BNP
		Inner Join fin_bankpayment_details 
			ON fin_bankpayment_details.strBankPaymentNo = BNP.strBankPaymentNo				
		Inner Join mst_financechartofaccounts 
			ON fin_bankpayment_details.intAccountId = mst_financechartofaccounts.intId
		WHERE
		fin_bankpayment_details.intPayTo =  '$supId' AND
		(mst_financechartofaccounts.intFinancialTypeId = '26' OR 
		mst_financechartofaccounts.intFinancialTypeId = '16' OR 
		mst_financechartofaccounts.intFinancialTypeId = '17' OR
		mst_financechartofaccounts.intFinancialTypeId = '28') AND
		BNP.intStatus = '1' AND
		BNP.intCurrency = $currency AND
		BNP.dtDate <='$toDay' AND
		DATEDIFF('$toDay',BNP.dtDate) >= 31 AND
		DATEDIFF('$toDay',BNP.dtDate) <= 60 ".$wareCom. "
		having balAmount<>0";
		
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$payment 	= $row['strBankPaymentNo'];
			$amount 	= number_format(($row['dblAmmount']*(-1)),4,'.','');
			$toBePaid 	= number_format(($row['balAmount']*(-1)),4,'.','');
			$date 		= substr($row['dtDate'],0,10);
?>
                
                    <tr class="">
                        <td class="normalfntMid"></td>
                        <td class="normalfntMid"></td>
                        <td class="normalfntMid"><?php echo $payment?></td>
                        <td class="normalfntMid"><?php echo $date ?></td>
                        <td class="figurs"><?php echo number_format($amount,4)?></td>
                        <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
                    </tr>
                    
<?php
			$inv60AmountTot	+= $amount;
			$inv60TobeTot	+= $toBePaid;
		}
?>
                <!-- Petty Cash -->                                
<?php
		if($filterCompany==0)
			$wareCom	= " ";
		else 
			$wareCom	= " AND PTC.intCompanyId=$filterCompany ";
			
		 $sql = "SELECT
		PTC.strPettyCashNo,
		PTC.dtDate,
		PTC.dblReceivedAmount,
		fin_bankpettycash_details.dblAmmount,
		(
		fin_bankpettycash_details.dblAmmount 
		+
		IFNULL ((SELECT
		Sum(fin_other_payable_payments_main_details.dblPayAmount )AS paidAmount
		FROM fin_other_payable_payments_main_details
		Inner Join fin_other_payable_payments_header ON fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
		WHERE
		fin_other_payable_payments_main_details.strDocNo =  PTC.strPettyCashNo AND
		fin_other_payable_payments_header.intDeleteStatus =  '0' AND fin_other_payable_payments_header.dtmDate<='$toDay' AND fin_other_payable_payments_main_details.strDocType = 'Petty Cash'
		GROUP BY
		fin_other_payable_payments_main_details.strDocNo),0)
		
		) AS balAmount
		FROM
		fin_bankpettycash_header AS PTC
		Inner Join fin_bankpettycash_details ON fin_bankpettycash_details.strPettyCashNo = PTC.strPettyCashNo
		Inner Join mst_financechartofaccounts ON fin_bankpettycash_details.intAccountId = mst_financechartofaccounts.intId
		WHERE
		(mst_financechartofaccounts.intFinancialTypeId = '26' OR mst_financechartofaccounts.intFinancialTypeId = '16' OR mst_financechartofaccounts.intFinancialTypeId = '17' OR mst_financechartofaccounts.intFinancialTypeId = '28')
		AND
		fin_bankpettycash_details.intPayTo =  '$supId' AND
		PTC.intStatus = '1' AND
						PTC.intCurrency=$currency AND
		PTC.dtDate <='$toDay' AND
						DATEDIFF('$toDay',PTC.dtDate)>= 31 AND
						DATEDIFF('$toDay',PTC.dtDate)<= 60".$wareCom.
		" having balAmount<>0";
		//die($sql);
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$pettyCash 	= $row['strPettyCashNo'];
			$amount 	= number_format(($row['dblAmmount']*(-1)),4,'.','');
			$toBePaid 	= number_format(($row['balAmount']*(-1)),4,'.','');
			$date 		= substr($row['dtDate'],0,10);
?>
                
                    <tr class="">
                        <td class="normalfntMid"></td>
                        <td class="normalfntMid"></td>
                        <td class="normalfntMid"><?php echo $payment?></td>
                        <td class="normalfntMid"><?php echo $date ?></td>
                        <td class="figurs"><?php echo number_format($amount,4)?></td>
                        <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
                    </tr>
                    
<?php
			$inv60AmountTot	+= $amount;
			$inv60TobeTot	+= $toBePaid;
		}
?>
                <!-- Jurnel Entry -->                                
<?php
		if($filterCompany==0)
			$wareCom	= " ";
		else  
			$wareCom	= " AND JH.intCompanyId = $filterCompany ";
			
		$sql = "SELECT
					JH.strReferenceNo,
					JH.dtmDate, 
					JD.dblDebitAmount,
					JD.dbCreditAmount,
					(
					JD.dbCreditAmount - JD.dblDebitAmount -
					IFNULL ((SELECT
					Sum(fin_other_payable_payments_main_details.dblPayAmount )AS paidAmount
					FROM
					fin_other_payable_payments_main_details
					Inner Join fin_other_payable_payments_header 
						ON fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
					Inner Join fin_accountant_journal_entry_details 
						ON fin_other_payable_payments_main_details.strDocNo = fin_accountant_journal_entry_details.strReferenceNo
					WHERE
					fin_other_payable_payments_main_details.strDocNo = JH.strReferenceNo AND
					fin_other_payable_payments_header.intDeleteStatus = '0' AND
					fin_other_payable_payments_header.dtmDate <= '$toDay' AND
					fin_other_payable_payments_main_details.strDocType = 'JN' AND
					fin_other_payable_payments_header.intSupplierId = '$supId' AND
					fin_accountant_journal_entry_details.strPersonType = 'osup'
					AND fin_accountant_journal_entry_details.intChartOfAccountId = JD.intChartOfAccountId
					GROUP BY
					fin_other_payable_payments_main_details.strDocNo),0)
		
		) AS balAmount
					FROM
					fin_accountant_journal_entry_header AS JH
					INNER JOIN fin_accountant_journal_entry_details AS JD 
						ON JH.strReferenceNo = JD.strReferenceNo                            
					INNER JOIN mst_financechartofaccounts 
						ON JD.intChartOfAccountId = mst_financechartofaccounts.intId
					WHERE
						JD.strPersonType = 'osup' AND
						JD.intNameId = $supId AND
						JH.intDeleteStatus = 0 AND
						(mst_financechartofaccounts.intFinancialTypeId = '26' OR mst_financechartofaccounts.intFinancialTypeId = '16' OR mst_financechartofaccounts.intFinancialTypeId = '17' OR mst_financechartofaccounts.intFinancialTypeId = '28') AND
						JH.intCurrencyId=$currency AND
			JH.dtmDate <='$toDay' AND
						DATEDIFF('$toDay', JH.dtmDate)>= 31 AND
						DATEDIFF('$toDay', JH.dtmDate)<= 60".$wareCom.
					" HAVING balAmount <> 0";
		//die($sql);			
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$payment 	= $row['strReferenceNo'];
			$amount 	= number_format(($row['dbCreditAmount'] - $row['dblDebitAmount']),4,'.','');
                        
			//if($row['dblDebitAmount']!=0){
				//$toBePaid = number_format(($row['balAmount']*(-1)),4,'.','');
			//}
			//else{
             $toBePaid = number_format(($row['balAmount']),4,'.','');
			//}                       
			                        
			$date 		= substr($row['dtmDate'],0,10);
?>                
            <tr class="">
                <td class="normalfntMid"></td>
                <td class="normalfntMid"></td>
                <td class="normalfntMid"><?php echo $payment?></td>
                <td class="normalfntMid"><?php echo $date ?></td>
                <td class="figurs"><?php echo number_format($amount,4)?></td>
                <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
            </tr>
                    
<?php
			$inv60AmountTot	+= $amount;
			$inv60TobeTot	+= $toBePaid;
                    
                    
		}
			$amountTot	+= $inv60AmountTot;
			$toBeTot	+= $inv60TobeTot;
?>
                <tr>
                    <td class="total1" colspan="4">Total</td>                        
                    <td class="total"><?php echo number_format($inv60AmountTot,4)?></td>
                    <td class="total"><?php echo number_format($inv60TobeTot,4)?></td>
                </tr>
                <tr class="subHed1"><td colspan="5">61 - 90</td></tr>
                    <!-- Invoice -->
<?php
		if($filterCompany==0)
			$wareCom	= " ";
		else 
			$wareCom	= " AND fin_other_payable_bill_header.intCompanyId=$filterCompany ";
			
		$inv90AmountTot	= 0;
		$inv90TobeTot	= 0;
		
		$sql="SELECT
		fin_other_payable_bill_header.strReferenceNo,
		fin_other_payable_bill_header.strPoNo,
		fin_other_payable_bill_header.strSupInvoice,
		fin_other_payable_bill_header.strGrnNo,	
		fin_other_payable_bill_header.dtmDate,			
		fin_other_payable_bill_header.intInvoiceNo,				
		sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) AS amount,
		(
		sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) 
		-
		IFNULL ((SELECT
		Sum(fin_other_payable_payments_main_details.dblPayAmount )AS paidAmount
		FROM fin_other_payable_payments_main_details
		Inner Join fin_other_payable_payments_header 
			ON fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
		WHERE
			fin_other_payable_payments_main_details.strDocNo = INV.strReferenceNo AND
			fin_other_payable_payments_header.intDeleteStatus = '0' AND 
			fin_other_payable_payments_header.dtmDate <= '$toDay' AND 
			fin_other_payable_payments_main_details.strDocType = 'O.Bill'
		GROUP BY
		fin_other_payable_payments_main_details.strDocNo),0)
		
		) AS balAmount
		FROM
		fin_other_payable_bill_details AS INV
		Inner Join fin_other_payable_bill_header 
			ON fin_other_payable_bill_header.strReferenceNo = INV.strReferenceNo AND 
			INV.intInvoiceNo = fin_other_payable_bill_header.intInvoiceNo AND 
			INV.intAccPeriodId = fin_other_payable_bill_header.intAccPeriodId AND 
			INV.intLocationId = fin_other_payable_bill_header.intLocationId AND 
			INV.intCompanyId = fin_other_payable_bill_header.intCompanyId
		Inner Join mst_finance_service_supplier 
			ON mst_finance_service_supplier.intId = fin_other_payable_bill_header.intsupplierId
		INNER JOIN mst_financepaymentsterms AS PT 
			ON fin_other_payable_bill_header.intPaymentsTermsId = PT.intId
		WHERE
			fin_other_payable_bill_header.intsupplierId =  '$supId' AND
			fin_other_payable_bill_header.intDeleteStatus =  '0' AND
			fin_other_payable_bill_header.intCurrencyId = $currency AND
			fin_other_payable_bill_header.dtmDate <='$toDay' AND
			DATEDIFF('$toDay',fin_other_payable_bill_header.dtmDate) >= (61+ PT.strName) AND
			DATEDIFF('$toDay',fin_other_payable_bill_header.dtmDate) <=(90+ PT.strName)
						   ".$wareCom.
		" GROUP BY
			fin_other_payable_bill_header.strReferenceNo
		having balAmount<>0";
		//die($sql);
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			
			$invoice 	= $row['strReferenceNo'];
			$supInvoice = $row['strSupInvoice'];
			$poNo 		= $row['strPoNo'];
			$grnNo 		= $row['strGrnNo'];
			$amount 	= number_format($row['amount'],4,'.','');
			$toBePaid 	= number_format($row['balAmount'],4,'.','');
			$date 		= $row['dtmDate'];
?>
            <tr onclick=" leadgerDrill('Other Bill','<?php echo $invoice ?>')" class="dillLink">
                <td class="normalfntMid"><?php echo $poNo?></td>
                <td class="normalfntMid"><?php echo $grnNo?></td>
                <td class="normalfntMid"><?php echo $supInvoice?></td>
                <td class="normalfntMid"><?php echo $date ?></td>
                <td class="figurs"><?php echo number_format($amount,4)?></td>
                <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
            </tr>                    
<?php
			$inv90AmountTot	+= $amount;
			$inv90TobeTot	+= $toBePaid;
		}
?>
                <!-- Bank Deposit -->                
<?php
		if($filterCompany==0)
			$wareCom	= " ";
		else 
			$wareCom	= " AND BND.intCompanyId=$filterCompany ";
		
		$sql = "SELECT
		BND.strDepositNo,
		BND.dtDate,
		fin_bankdeposit_details.dblAmmount,
		(
		fin_bankdeposit_details.dblAmmount 
		-
		IFNULL ((SELECT
		Sum(fin_other_payable_payments_main_details.dblPayAmount )AS paidAmount
		FROM fin_other_payable_payments_main_details
		Inner Join fin_other_payable_payments_header 
			ON fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
		WHERE
		fin_other_payable_payments_main_details.strDocNo = BND.strDepositNo AND
		fin_other_payable_payments_header.intDeleteStatus = '0' AND 
		fin_other_payable_payments_header.dtmDate <= '$toDay' AND 
		fin_other_payable_payments_main_details.strDocType = 'B.Deposit'
		GROUP BY
		fin_other_payable_payments_main_details.strDocNo),0)
		
		) AS balAmount
		FROM
		fin_bankdeposit_header BND
		Inner Join fin_bankdeposit_details 
			ON fin_bankdeposit_details.strDepositNo = BND.strDepositNo				
		Inner Join mst_financechartofaccounts 
			ON fin_bankdeposit_details.intAccount = mst_financechartofaccounts.intId
		WHERE
		fin_bankdeposit_details.intRecvFrom =  '$supId' AND
		(mst_financechartofaccounts.intFinancialTypeId = '26' OR 
		mst_financechartofaccounts.intFinancialTypeId = '16' OR 
		mst_financechartofaccounts.intFinancialTypeId = '17' OR 
		mst_financechartofaccounts.intFinancialTypeId = '28') AND
		BND.intStatus = '1' AND
		BND.intCurrency=$currency AND
		BND.dtDate <='$toDay' AND
		DATEDIFF('$toDay',BND.dtDate) >=61 AND
		DATEDIFF('$toDay',BND.dtDate) <=90".$wareCom.
		" having balAmount<>0";
		//die($sql);
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$deposit 	= $row['strDepositNo'];
			$amount 	= number_format(($row['dblAmmount']),4,'.','');
			$toBePaid 	= number_format(($row['balAmount']),4,'.','');
			$date 		= substr($row['dtDate'],0,10);
?>               
            <tr class="">
                <td class="normalfntMid"></td>
                <td class="normalfntMid"></td>
                <td class="normalfntMid"><?php echo $deposit?></td>
                <td class="normalfntMid"><?php echo $date ?></td>
                <td class="figurs"><?php echo number_format($amount,4)?></td>
                <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
            </tr>
                    
<?php
			$inv90AmountTot	+= $amount;
			$inv90TobeTot	+= $toBePaid;
		}
?>
                <!-- Bank Payments -->                                
<?php
		if($filterCompany==0)
			$wareCom	= " ";
		else 
			$wareCom	= " AND BNP.intCompanyId=$filterCompany ";
			
		$sql = "SELECT
		BNP.strBankPaymentNo,
		BNP.dtDate,
		BNP.dblReceivedAmount,
		fin_bankpayment_details.dblAmmount,
		(
		fin_bankpayment_details.dblAmmount 
		+
		IFNULL ((SELECT
		Sum(fin_other_payable_payments_main_details.dblPayAmount )AS paidAmount
		FROM fin_other_payable_payments_main_details
		Inner Join fin_other_payable_payments_header 
			ON fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
		WHERE
			fin_other_payable_payments_main_details.strDocNo = BNP.strBankPaymentNo AND
			fin_other_payable_payments_header.intDeleteStatus = '0' AND 
			fin_other_payable_payments_header.dtmDate <= '$toDay' AND 
			fin_other_payable_payments_main_details.strDocType = 'B.Payment'
		GROUP BY
		fin_other_payable_payments_main_details.strDocNo),0)		
		) AS balAmount
		FROM
		fin_bankpayment_header BNP
		Inner Join fin_bankpayment_details 
			ON fin_bankpayment_details.strBankPaymentNo = BNP.strBankPaymentNo				
		Inner Join mst_financechartofaccounts 
			ON fin_bankpayment_details.intAccountId = mst_financechartofaccounts.intId
		WHERE
		fin_bankpayment_details.intPayTo =  '$supId' AND
		(mst_financechartofaccounts.intFinancialTypeId = '26' OR 
		mst_financechartofaccounts.intFinancialTypeId = '16' OR 
		mst_financechartofaccounts.intFinancialTypeId = '17' OR 
		mst_financechartofaccounts.intFinancialTypeId = '28') AND
		BNP.intStatus = '1' AND
		BNP.intCurrency = $currency AND
		BNP.dtDate <='$toDay' AND
		DATEDIFF('$toDay',BNP.dtDate) >= 61 AND
		DATEDIFF('$toDay',BNP.dtDate) <= 90 ".$wareCom. "
		having balAmount<>0";
		//die($sql);
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$payment 	= $row['strBankPaymentNo'];
			$amount 	= number_format(($row['dblAmmount']*(-1)),4,'.','');
			$toBePaid 	= number_format(($row['balAmount']*(-1)),4,'.','');
			$date 		= substr($row['dtDate'],0,10);
?>
                
            <tr class="">
                <td class="normalfntMid"></td>
                <td class="normalfntMid"></td>
                <td class="normalfntMid"><?php echo $payment?></td>
                <td class="normalfntMid"><?php echo $date ?></td>
                <td class="figurs"><?php echo number_format($amount,4)?></td>
                <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
            </tr>
                    
<?php
			$inv90AmountTot	+= $amount;
			$inv90TobeTot	+= $toBePaid;
		}
?>
                <!-- Petty Cash -->                                
<?php
		if($filterCompany==0)
			$wareCom	= " ";
		else 
			$wareCom	= " AND PTC.intCompanyId=$filterCompany ";
			
		 $sql = "SELECT
		PTC.strPettyCashNo,
		PTC.dtDate,
						PTC.dblReceivedAmount,
						fin_bankpettycash_details.dblAmmount,
		(
		fin_bankpettycash_details.dblAmmount
		+
		IFNULL ((SELECT
		Sum(fin_other_payable_payments_main_details.dblPayAmount )AS paidAmount
		FROM fin_other_payable_payments_main_details
		Inner Join fin_other_payable_payments_header 
			ON fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
		WHERE
		fin_other_payable_payments_main_details.strDocNo = PTC.strPettyCashNo AND
		fin_other_payable_payments_header.intDeleteStatus = '0' AND 
		fin_other_payable_payments_header.dtmDate<='$toDay' AND 
		fin_other_payable_payments_main_details.strDocType = 'Petty Cash'
		GROUP BY
		fin_other_payable_payments_main_details.strDocNo),0)
		
		) AS balAmount
		FROM
		fin_bankpettycash_header AS PTC
		Inner Join fin_bankpettycash_details 
			ON fin_bankpettycash_details.strPettyCashNo = PTC.strPettyCashNo
		Inner Join mst_financechartofaccounts 
			ON fin_bankpettycash_details.intAccountId = mst_financechartofaccounts.intId
		WHERE
		(mst_financechartofaccounts.intFinancialTypeId = '26' OR 
		mst_financechartofaccounts.intFinancialTypeId = '16' OR 
		mst_financechartofaccounts.intFinancialTypeId = '17' OR 
		mst_financechartofaccounts.intFinancialTypeId = '28')
		AND
		fin_bankpettycash_details.intPayTo =  '$supId' AND
		PTC.intStatus = '1' AND
						PTC.intCurrency=$currency AND
		PTC.dtDate <='$toDay' AND
						DATEDIFF('$toDay',PTC.dtDate)>= 61 AND
						DATEDIFF('$toDay',PTC.dtDate)<= 90".$wareCom.
		" having balAmount<>0";
		
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$pettyCash 	= $row['strPettyCashNo'];
			$amount 	= number_format(($row['dblAmmount']*(-1)),4,'.','');
			$toBePaid 	= number_format(($row['balAmount']*(-1)),4,'.','');
			$date 		= substr($row['dtDate'],0,10);
?>
                
            <tr class="">
                <td class="normalfntMid"><?php echo $payment?></td>
                <td class="normalfntMid"><?php echo $payment?></td>
                <td class="normalfntMid"><?php echo $date ?></td>
                <td class="figurs"><?php echo number_format($amount,4)?></td>
                <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
            </tr>                    
<?php
			$inv90AmountTot	+= $amount;
			$inv90TobeTot	+= $toBePaid;
		}
?>
                <!-- Jurnel Entry -->                                
<?php
		if($filterCompany==0)
			$wareCom	= " ";
		else 
			$wareCom	= " AND JH.intCompanyId=$filterCompany ";
			
		$sql = "SELECT
					JH.strReferenceNo,
					JH.dtmDate, 
					JD.dblDebitAmount,
					JD.dbCreditAmount,
					(
					JD.dbCreditAmount - JD.dblDebitAmount -
					IFNULL ((SELECT
					Sum(fin_other_payable_payments_main_details.dblPayAmount )AS paidAmount
					FROM
					fin_other_payable_payments_main_details
					Inner Join fin_other_payable_payments_header 
						ON fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
					Inner Join fin_accountant_journal_entry_details 
						ON fin_other_payable_payments_main_details.strDocNo = fin_accountant_journal_entry_details.strReferenceNo
					WHERE
					fin_other_payable_payments_main_details.strDocNo = JH.strReferenceNo AND
					fin_other_payable_payments_header.intDeleteStatus = '0' AND
					fin_other_payable_payments_header.dtmDate <= '$toDay' AND
					fin_other_payable_payments_main_details.strDocType =  'JN' AND
					fin_other_payable_payments_header.intSupplierId = '$supId' AND
					fin_accountant_journal_entry_details.strPersonType = 'osup' AND
					fin_accountant_journal_entry_details.intChartOfAccountId = JD.intChartOfAccountId
					GROUP BY
					fin_other_payable_payments_main_details.strDocNo),0)
		
		) AS balAmount
					FROM
					fin_accountant_journal_entry_header AS JH
					INNER JOIN fin_accountant_journal_entry_details AS JD ON JH.strReferenceNo = JD.strReferenceNo                            
					INNER JOIN mst_financechartofaccounts ON JD.intChartOfAccountId = mst_financechartofaccounts.intId
					WHERE
					JD.strPersonType = 'osup' AND
					JD.intNameId = $supId AND
					JH.intDeleteStatus = 0 AND
					(mst_financechartofaccounts.intFinancialTypeId = '26' OR mst_financechartofaccounts.intFinancialTypeId = '16' OR mst_financechartofaccounts.intFinancialTypeId = '17' OR mst_financechartofaccounts.intFinancialTypeId = '28') AND
					JH.intCurrencyId=$currency AND
		JH.dtmDate <='$toDay' AND
					DATEDIFF('$toDay', JH.dtmDate)>= 61 AND
					DATEDIFF('$toDay', JH.dtmDate)<= 90".$wareCom.
					" HAVING balAmount <> 0";
					//die($sql);
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$payment 	= $row['strReferenceNo'];
			$amount 	= number_format(($row['dbCreditAmount'] - $row['dblDebitAmount']),4,'.','');
                        
			//if($row['dblDebitAmount']!=0){
				//$toBePaid = number_format(($row['balAmount']*(-1)),4,'.','');
			//}
		   //else{
				$toBePaid = number_format(($row['balAmount']),4,'.','');
			//}                       
			                        
			$date 		= substr($row['dtmDate'],0,10);
?>
                
            <tr class="">
                <td class="normalfntMid"></td>
                <td class="normalfntMid"></td>
                <td class="normalfntMid"><?php echo $payment?></td>
                <td class="normalfntMid"><?php echo $date ?></td>
                <td class="figurs"><?php echo number_format($amount,4)?></td>
                <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
            </tr>
                    
<?php
			$inv90AmountTot	+= $amount;
			$inv90TobeTot	+= $toBePaid;
		}
			$amountTot		+= $inv90AmountTot;
			$toBeTot		+= $inv90TobeTot;
?>
            <tr>
                <td class="total1" colspan="4">Total</td>                        
                <td class="total"><?php echo number_format($inv90AmountTot,4)?></td>
                <td class="total"><?php echo number_format($inv90TobeTot,4)?></td>
            </tr>
            <tr class="subHed1"><td colspan="5">More Than 90 Days</td></tr>
                    <!-- Invoice -->
<?php
		if($filterCompany==0)
			$wareCom	= " ";
		else 
			$wareCom 	= " AND fin_other_payable_bill_header.intCompanyId=$filterCompany ";
			
		$inv91AmountTot	= 0;
		$inv91TobeTot	= 0;
		
		$sql="SELECT
		fin_other_payable_bill_header.strReferenceNo,
		fin_other_payable_bill_header.strPoNo,
		fin_other_payable_bill_header.strSupInvoice,
		fin_other_payable_bill_header.strGrnNo,
		fin_other_payable_bill_header.dtmDate,				
		fin_other_payable_bill_header.intInvoiceNo,				
		sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) AS amount,
		(
		sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) 
		-
		IFNULL ((SELECT
		Sum(fin_other_payable_payments_main_details.dblPayAmount )AS paidAmount
		FROM fin_other_payable_payments_main_details
		Inner Join fin_other_payable_payments_header 
			ON fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
		WHERE
		fin_other_payable_payments_main_details.strDocNo =  INV.strReferenceNo AND
		fin_other_payable_payments_header.intDeleteStatus =  '0' AND 
		fin_other_payable_payments_header.dtmDate<='$toDay' AND 
		fin_other_payable_payments_main_details.strDocType = 'O.Bill'
		GROUP BY
		fin_other_payable_payments_main_details.strDocNo),0)
		
		) AS balAmount
		FROM
		fin_other_payable_bill_details AS INV
		Inner Join fin_other_payable_bill_header 
			ON fin_other_payable_bill_header.strReferenceNo = INV.strReferenceNo AND 
			INV.intInvoiceNo = fin_other_payable_bill_header.intInvoiceNo AND 
			INV.intAccPeriodId = fin_other_payable_bill_header.intAccPeriodId AND 
			INV.intLocationId = fin_other_payable_bill_header.intLocationId AND 
			INV.intCompanyId = fin_other_payable_bill_header.intCompanyId
		Inner Join mst_finance_service_supplier 
			ON mst_finance_service_supplier.intId = fin_other_payable_bill_header.intsupplierId
		INNER JOIN mst_financepaymentsterms AS PT 
			ON fin_other_payable_bill_header.intPaymentsTermsId = PT.intId
		WHERE
		fin_other_payable_bill_header.intsupplierId =  '$supId' AND
		fin_other_payable_bill_header.intDeleteStatus =  '0' AND
							fin_other_payable_bill_header.intCurrencyId = $currency AND
		fin_other_payable_bill_header.dtmDate <='$toDay' AND
		DATEDIFF('$toDay',fin_other_payable_bill_header.dtmDate) >= (91+ PT.strName)                                    
						   ".$wareCom.
		" GROUP BY
		fin_other_payable_bill_header.strReferenceNo
		having balAmount<>0";
		//die($sql);
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{			
			$invoice 	= $row['strReferenceNo'];
			$supInvoice = $row['strSupInvoice'];
			$poNo 		= $row['strPoNo'];
			$grnNo 		= $row['strGrnNo'];
			$amount 	= number_format($row['amount'],4,'.','');
			$toBePaid 	= number_format($row['balAmount'],4,'.','');
			$date 		= $row['dtmDate'];
?>
            <tr onclick=" leadgerDrill('Other Bill','<?php echo $invoice ?>')" class="dillLink">
                <td class="normalfntMid"><?php echo $poNo?></td>
                <td class="normalfntMid"><?php echo $grnNo?></td>
                <td class="normalfntMid"><?php echo $supInvoice?></td>
                <td class="normalfntMid"><?php echo $date ?></td>
                <td class="figurs"><?php echo number_format($amount,4)?></td>
                <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
            </tr>                    
<?php
			$inv91AmountTot	+= $amount;
			$inv91TobeTot	+= $toBePaid;
		}
?>
                <!-- Bank Deposit -->                
<?php
		if($filterCompany==0)
			$wareCom	= " ";
		else 
			$wareCom	= " AND BND.intCompanyId=$filterCompany ";
		
		$sql = "SELECT
		BND.strDepositNo,
		BND.dtDate,
						fin_bankdeposit_details.dblAmmount,
		(
		fin_bankdeposit_details.dblAmmount 
		-
		IFNULL ((SELECT
		Sum(fin_other_payable_payments_main_details.dblPayAmount )AS paidAmount
		FROM fin_other_payable_payments_main_details
		Inner Join fin_other_payable_payments_header ON fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
		WHERE
		fin_other_payable_payments_main_details.strDocNo =  BND.strDepositNo AND
		fin_other_payable_payments_header.intDeleteStatus =  '0' AND fin_other_payable_payments_header.dtmDate<='$toDay' AND fin_other_payable_payments_main_details.strDocType = 'B.Deposit'
		GROUP BY
		fin_other_payable_payments_main_details.strDocNo),0)
		
		) AS balAmount
		FROM
		fin_bankdeposit_header BND
		Inner Join fin_bankdeposit_details ON fin_bankdeposit_details.strDepositNo = BND.strDepositNo				
		Inner Join mst_financechartofaccounts ON fin_bankdeposit_details.intAccount = mst_financechartofaccounts.intId
		WHERE
		fin_bankdeposit_details.intRecvFrom =  '$supId' AND
		(mst_financechartofaccounts.intFinancialTypeId = '26' OR mst_financechartofaccounts.intFinancialTypeId = '16' OR mst_financechartofaccounts.intFinancialTypeId = '17' OR mst_financechartofaccounts.intFinancialTypeId = '28') AND
		BND.intStatus = '1' AND
						BND.intCurrency=$currency AND
		BND.dtDate <='$toDay' AND
						DATEDIFF('$toDay',BND.dtDate) >=91 ".$wareCom.
		" having balAmount<>0";
		//die($sql);
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$deposit 	= $row['strDepositNo'];
			$amount 	= number_format(($row['dblAmmount']),4,'.','');
			$toBePaid 	= number_format(($row['balAmount']),4,'.','');
			$date 		= substr($row['dtDate'],0,10);
?>               
            <tr class="">
                <td class="normalfntMid"></td>
                <td class="normalfntMid"></td>
                <td class="normalfntMid"><?php echo $deposit?></td>
                <td class="normalfntMid"><?php echo $date ?></td>
                <td class="figurs"><?php echo number_format($amount,4)?></td>
                <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
            </tr>                    
<?php
			$inv91AmountTot	+= $amount;
			$inv91TobeTot	+= $toBePaid;
		}
?>
                <!-- Bank Payments -->                                
<?php
		if($filterCompany==0)
			$wareCom	= " ";
		else 
			$wareCom	= " AND BNP.intCompanyId=$filterCompany ";
		
		$sql = "SELECT
		BNP.strBankPaymentNo,
		BNP.dtDate,
						BNP.dblReceivedAmount,
						fin_bankpayment_details.dblAmmount,
		(
		fin_bankpayment_details.dblAmmount
		+
		IFNULL ((SELECT
		Sum(fin_other_payable_payments_main_details.dblPayAmount )AS paidAmount
		FROM fin_other_payable_payments_main_details
		Inner Join fin_other_payable_payments_header ON fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
		WHERE
		fin_other_payable_payments_main_details.strDocNo =  BNP.strBankPaymentNo AND
		fin_other_payable_payments_header.intDeleteStatus =  '0' AND fin_other_payable_payments_header.dtmDate<='$toDay' AND fin_other_payable_payments_main_details.strDocType = 'B.Payment'
		GROUP BY
		fin_other_payable_payments_main_details.strDocNo),0)
		
		) AS balAmount
		FROM
		fin_bankpayment_header BNP
		Inner Join fin_bankpayment_details ON fin_bankpayment_details.strBankPaymentNo = BNP.strBankPaymentNo				
		Inner Join mst_financechartofaccounts ON fin_bankpayment_details.intAccountId = mst_financechartofaccounts.intId
		WHERE
		fin_bankpayment_details.intPayTo =  '$supId' AND
		(mst_financechartofaccounts.intFinancialTypeId = '26' OR mst_financechartofaccounts.intFinancialTypeId = '16' OR mst_financechartofaccounts.intFinancialTypeId = '17' OR mst_financechartofaccounts.intFinancialTypeId = '28') AND
		BNP.intStatus = '1' AND
						BNP.intCurrency=$currency AND
		BNP.dtDate <='$toDay' AND
						DATEDIFF('$toDay',BNP.dtDate)>= 91 ".$wareCom. "
		having balAmount<>0";
		//die($sql);
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$payment 	= $row['strBankPaymentNo'];
			$amount 	= number_format(($row['dblAmmount']*(-1)),4,'.','');
			$toBePaid 	= number_format(($row['balAmount']*(-1)),4,'.','');
			$date 		= substr($row['dtDate'],0,10);
?>                
            <tr class="">
                <td class="normalfntMid"></td>
                <td class="normalfntMid"></td>
                <td class="normalfntMid"><?php echo $payment?></td>
                <td class="normalfntMid"><?php echo $date ?></td>
                <td class="figurs"><?php echo number_format($amount,4)?></td>
                <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
            </tr>
                    
<?php
			$inv91AmountTot	+= $amount;
			$inv91TobeTot	+= $toBePaid;
		}
?>
                <!-- Petty Cash -->                                
<?php
		if($filterCompany==0)
			$wareCom	= " ";
		else 
			$wareCom	= " AND PTC.intCompanyId=$filterCompany ";
		
		 $sql = "SELECT
		PTC.strPettyCashNo,
		PTC.dtDate,
						PTC.dblReceivedAmount,
						fin_bankpettycash_details.dblAmmount,
		(
		fin_bankpettycash_details.dblAmmount
		+
		IFNULL ((SELECT
		Sum(fin_other_payable_payments_main_details.dblPayAmount )AS paidAmount
		FROM fin_other_payable_payments_main_details
		Inner Join fin_other_payable_payments_header 
			ON fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
		WHERE
			fin_other_payable_payments_main_details.strDocNo =  PTC.strPettyCashNo AND
			fin_other_payable_payments_header.intDeleteStatus =  '0' AND 
			fin_other_payable_payments_header.dtmDate<='$toDay' AND 
			fin_other_payable_payments_main_details.strDocType = 'Petty Cash'
		GROUP BY
		fin_other_payable_payments_main_details.strDocNo),0)
		
		) AS balAmount
		FROM
		fin_bankpettycash_header AS PTC
		Inner Join fin_bankpettycash_details 
			ON fin_bankpettycash_details.strPettyCashNo = PTC.strPettyCashNo
		Inner Join mst_financechartofaccounts 
			ON fin_bankpettycash_details.intAccountId = mst_financechartofaccounts.intId
		WHERE
		(mst_financechartofaccounts.intFinancialTypeId = '26' OR 
		mst_financechartofaccounts.intFinancialTypeId = '16' OR 
		mst_financechartofaccounts.intFinancialTypeId = '17' OR 
		mst_financechartofaccounts.intFinancialTypeId = '28') AND
		fin_bankpettycash_details.intPayTo = '$supId' AND
		PTC.intStatus = '1' AND
		PTC.intCurrency = $currency AND
		PTC.dtDate <='$toDay' AND
		DATEDIFF('$toDay',PTC.dtDate)>= 91 ".$wareCom.
		" having balAmount<>0";
		//die($sql);
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$pettyCash 	= $row['strPettyCashNo'];
			$amount 	= number_format(($row['dblAmmount']*(-1)),4,'.','');
			$toBePaid 	= number_format(($row['balAmount']*(-1)),4,'.','');
			$date 		= substr($row['dtDate'],0,10);
?>                
            <tr class="">
                <td class="normalfntMid"></td>
                <td class="normalfntMid"></td>
                <td class="normalfntMid"><?php echo $payment?></td>
                <td class="normalfntMid"><?php echo $date ?></td>
                <td class="figurs"><?php echo number_format($amount,4)?></td>
                <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
            </tr>
<?php
			$inv91AmountTot	+= $amount;
			$inv91TobeTot	+= $toBePaid;
		}
?>
                <!-- Jurnel Entry -->                                
<?php
		if($filterCompany==0)
			$wareCom	= " ";
		else 
			$wareCom	= " AND JH.intCompanyId=$filterCompany ";
		$sql = "SELECT
					JH.strReferenceNo,
					JH.dtmDate, 
					JD.dblDebitAmount,
					JD.dbCreditAmount,
					(
					JD.dbCreditAmount - JD.dblDebitAmount -
					IFNULL ((SELECT
					Sum(fin_other_payable_payments_main_details.dblPayAmount )AS paidAmount
					FROM
					fin_other_payable_payments_main_details
					Inner Join fin_other_payable_payments_header 
						ON fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
					Inner Join fin_accountant_journal_entry_details 
						ON fin_other_payable_payments_main_details.strDocNo = fin_accountant_journal_entry_details.strReferenceNo
					WHERE
						fin_other_payable_payments_main_details.strDocNo = JH.strReferenceNo AND
						fin_other_payable_payments_header.intDeleteStatus =  '0' AND
						fin_other_payable_payments_header.dtmDate <=  '$toDay' AND
						fin_other_payable_payments_main_details.strDocType =  'JN' AND
						fin_other_payable_payments_header.intSupplierId =  '$supId' AND
						fin_accountant_journal_entry_details.strPersonType =  'osup' AND
						fin_accountant_journal_entry_details.intChartOfAccountId = JD.intChartOfAccountId
					GROUP BY
					fin_other_payable_payments_main_details.strDocNo),0)
		
					) AS balAmount
					FROM
					fin_accountant_journal_entry_header AS JH
					INNER JOIN fin_accountant_journal_entry_details AS JD 
						ON JH.strReferenceNo = JD.strReferenceNo                            
					INNER JOIN mst_financechartofaccounts 
						ON JD.intChartOfAccountId = mst_financechartofaccounts.intId
					WHERE
					JD.strPersonType = 'osup' AND
					JD.intNameId = $supId AND
					JH.intDeleteStatus = 0 AND
					(mst_financechartofaccounts.intFinancialTypeId = '26' OR 
					mst_financechartofaccounts.intFinancialTypeId = '16' OR 
					mst_financechartofaccounts.intFinancialTypeId = '17' OR 
					mst_financechartofaccounts.intFinancialTypeId = '28') AND
					JH.intCurrencyId=$currency AND
					JH.dtmDate <='$toDay' AND
					DATEDIFF('$toDay', JH.dtmDate)>= 91 ".$wareCom.
					" HAVING balAmount <> 0";
		//die($sql);
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$payment 	= $row['strReferenceNo'];
			$amount 	= number_format(($row['dbCreditAmount'] - $row['dblDebitAmount']),4,'.','');                        
		//if($row['dblDebitAmount']!=0){
			//$toBePaid = number_format(($row['balAmount']*(-1)),4,'.','');
		//}
		//else{
			$toBePaid = number_format(($row['balAmount']),4,'.','');
		//}                    
			$date 		= substr($row['dtmDate'],0,10);
?>                
            <tr class="">
                <td class="normalfntMid"></td>
                <td class="normalfntMid"></td>
                <td class="normalfntMid"><?php echo $payment?></td>
                <td class="normalfntMid"><?php echo $date ?></td>
                <td class="figurs"><?php echo number_format($amount,4)?></td>
                <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
            </tr>                    
<?php
			$inv91AmountTot	+= $amount;
			$inv91TobeTot	+= $toBePaid;    
		}
             $amountTot	+= $inv91AmountTot;
             $toBeTot	+= $inv91TobeTot;
?>
                <tr>
                    <td class="total1" colspan="4">Total</td>                        
                    <td class="total"><?php echo number_format($inv91AmountTot,4)?></td>
                    <td class="total"><?php echo number_format($inv91TobeTot,4)?></td>
                </tr>
                <tr>
                    <td class="total1" colspan="4"><hr/></td>         
                </tr>
                <tr>
                    <td class="total1" colspan="4">Total</td>                        
                    <td class="total"><?php echo number_format($amountTot,4)?></td>
                    <td class="total"><?php echo number_format($toBeTot,4)?></td>
                </tr>
                </table>
<?php 
	} 
?>
            </div>
        </form>
    </body>
</html>
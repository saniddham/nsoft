<?php
session_start();
$backwardseperator = "../../../../";
$mainPath 	= $_SESSION['mainPath'];
$userId 	= $_SESSION['userId'];
//$companyId = $_SESSION['headCompanyId'];
$location = $_SESSION['CompanyID'];
include "{$backwardseperator}dataAccess/Connector.php";

$requestType 	= $_REQUEST['requestType'];
if($requestType=='loadDdetails'){
    
    $theDate=$_REQUEST['date'];
    $companys=$_REQUEST['details'];
    
    //get Currency
    $sql = "SELECT DISTINCT mst_financecurrency.intId,mst_financecurrency.strCode FROM mst_financecurrency ";
    $result = $db->RunQuery($sql);
    $a = 0;
    while ($row = mysqli_fetch_array($result)) {
        $currencyId[$a] = $row["intId"];
        $currencyCode[$a] = $row["strCode"];
        $a++;
    }
    $colspan = count($currencyId);
    
    
    $detailsList="<table width=\"95%\" id=\"tblCustomersAccount\ border=\0\ cellpadding=\"0\" cellspacing=\"1\" bgcolor=\"#FF9900\">";
    $detailsList.=" <tr>
                        <td bgcolor=\"#FAD163\" class=\"normalfntMid\"><input onclick=\"chkAllSup()\" type=\"checkbox\" name=\"chkAll\" id=\"chkAll\" /></td>
                        <td bgcolor=\"#FAD163\" class=\"normalfntMid\"><strong>Suppliers</strong></td>";
    for ($j = 0; $j < count($companys); $j++) {
        $comId=$companys[$j];        
        $sqlC = "SELECT mst_companies.intId,mst_companies.strName FROM mst_companies WHERE mst_companies.intId='$comId' and mst_companies.intStatus =  '1'";
        $resultC = $db->RunQuery($sqlC);
        $k = 0;
        $rowC = mysqli_fetch_array($resultC);
        $compName = $rowC['strName'];
        
        $detailsList.=" <td class=\"normalfntMid\" bgcolor=\"#FAD163\"  colspan=\"$colspan\"><strong>$compName</strong></td>";
    }
     $detailsList.="    <td height=\"24\" bgcolor=\"#FAD163\"  class=\"normalfntMid\" colspan=\"$colspan\"><strong>Total</strong></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>";
    for ($j = 0; $j < count($companys); $j++) {
        for ($k = 0; $k < count($currencyId); $k++) {
            $detailsList.="<td class=\"normalfntMid\" bgcolor=\"#FAD163\">$currencyCode[$k]</td>";                                                                
        }
    }
    for ($k = 0; $k < count($currencyId); $k++) {
        $detailsList.="<td bgcolor=\"#FAD163\" class=\"normalfntMid\" >$currencyCode[$k]</td>";
    }
    $detailsList.="</tr>";
    $sql = "SELECT mst_finance_service_supplier.intId,mst_finance_service_supplier.strName FROM mst_finance_service_supplier";
    $result = $db->RunQuery($sql);
    while ($row = mysqli_fetch_array($result)) {
        $supId = $row['intId'];
        $supplier = $row['strName'];
    
        $detailsList.="<tr class=\"normal\" onmouseover=\"select(this)\" onmouseout=\"normal(this)\">
                        <td  class=\"normalfntMid\"><input name=\"chkSelect\" type=\"checkbox\" value=\"$supId\"  /></td>
                        <td  class=\"normalfntMid\">$supplier</td>";
    
        for ($j = 0; $j < count($companys); $j++) {
            for ($k = 0; $k < count($currencyId); $k++) {
                $theCurrencyId = $currencyId[$k];                                                                    
                $custBal=getSupplierBalance($supId,$theCurrencyId, $theDate,$companys[$j]);
                $custCompanyTotal[$supId][$k]+=$custBal;
                $curremcySum[$k]+=$custBal;
                $companyWiaeCurrency[ $j][$k]+=$custBal; 
                $detailsList.="<td align=\"right\" class=\"normalfntRight\">".number_format($custBal, 2)."</td>";                                                                    
            }
        }
        for ($k = 0; $k < count($currencyId); $k++) {
            $detailsList.="<td  class=\"normalfntRight\" ><strong>".number_format($custCompanyTotal[$supId][$k], 2)."</strong></td>";
        }                                                          
        $detailsList.="</tr>";    
    }//wile
    $detailsList.=" <tr>
                        <td></td>
                        <td class=\"normalfnt\"><strong>Total</strong></td>";
                                                        
    for ($j = 0; $j < count($companys); $j++) {
        for ($k = 0; $k < count($currencyId); $k++) {            
            $detailsList.="<td  class=\"normalfntRight\" ><strong>". number_format(round($companyWiaeCurrency[$j][$k], 2))."</strong></td>";
            
        }
    }
    for ($k = 0; $k < count($currencyId); $k++) {       
        $detailsList.="<td  class=\"normalfntRight\" ><strong>".number_format($curremcySum[$k], 2)."</strong></td>";
    }    
    $detailsList.="</tr> ";
                                                    
    $detailsList.="</table>";
    $response['detailsList']=$detailsList;
    echo json_encode($response);
}
?>
<?php
function getSupplierBalance($SupplierId,$currencyId, $date,$companyId){
    global $db;
    //get Base Currency
    $resultBase = $db->RunQuery("SELECT mst_companies.intBaseCurrencyId FROM mst_companies WHERE mst_companies.intId =  '$companyId'");
    $rowBase = mysqli_fetch_array($resultBase);    
    $daseCurrencyId= $rowBase['intBaseCurrencyId'];
    if($daseCurrencyId==$currencyId){
        $ware=" AND fin_transactions.strProgramType <> Other Supplier Gain-Loss ";
    }
    else{
        $ware=" ";
    }
    $sqDebit="  SELECT                                                               
                    SUM(fin_transactions_details.amount) as tot
                FROM
                    fin_transactions_details
                    Inner Join fin_transactions ON fin_transactions_details.entryId = fin_transactions.entryId
                WHERE
                    fin_transactions_details.personType =  'osup' AND
                    fin_transactions_details.personId =  '$SupplierId' AND
                    fin_transactions.currencyId =  '$currencyId' AND
                    fin_transactions.companyId =  '$companyId' AND
                    fin_transactions.authorized =  '1' AND
                    fin_transactions.delStatus =  '0' AND
                    fin_transactions.entryDate <=  '$date' AND
                    fin_transactions_details.`credit/debit` =  'D' ".$ware;
    $resultDebit = $db->RunQuery($sqDebit);
    $rowDebit = mysqli_fetch_array($resultDebit);    
    $debitTotal= $rowDebit['tot'];
    
    $sqCredit="  SELECT                                                               
                    SUM(fin_transactions_details.amount) as tot
                FROM
                    fin_transactions_details
                    Inner Join fin_transactions ON fin_transactions_details.entryId = fin_transactions.entryId
                WHERE
                    fin_transactions_details.personType =  'osup' AND
                    fin_transactions_details.personId =  '$SupplierId' AND
                    fin_transactions.currencyId =  '$currencyId' AND
                    fin_transactions.companyId =  '$companyId' AND
                    fin_transactions.authorized =  '1' AND
                    fin_transactions.delStatus =  '0' AND
                    fin_transactions.entryDate <=  '$date' AND
                    fin_transactions_details.`credit/debit` =  'C' ".$ware;
    $resultCredit = $db->RunQuery($sqCredit);
    $rowCredit = mysqli_fetch_array($resultCredit);    
    $creditTotal= $rowCredit['tot'];
    
    return ($creditTotal-$debitTotal);
}
/*function getSupplierBalance1($SupplierId,$currencyId, $date,$companyId){
    global $db;
    $sql="  SELECT 
                SUM(tbl.amount) as totAmount 
            FROM(                    
                    (SELECT
                        (
                        SUM(((CRN.dblQty*CRN.dblRate) *(100-CRN.dblDiscount)/100)+ IFNULL(CRN.dblTax,0))
                        +
                        IFNULL ((SELECT
                        Sum(fin_supplier_payments_main_details.dblPayAmount )AS paidAmount
                        FROM fin_supplier_payments_main_details
                        Inner Join fin_supplier_payments_header ON fin_supplier_payments_main_details.strReferenceNo = fin_supplier_payments_header.strReferenceNo
                        WHERE
                        fin_supplier_payments_main_details.strDocNo =  CRN.strDebitNoteNo AND
                        fin_supplier_payments_header.intDeleteStatus =  '0' AND fin_supplier_payments_header.dtmDate<='$date'
                        GROUP BY
                        fin_supplier_payments_main_details.strDocNo),0)
                        ) * (-1) AS amount
                    FROM
                        fin_supplier_debitnote_details AS CRN
                        Inner Join fin_supplier_debitnote_header ON CRN.strDebitNoteNo = fin_supplier_debitnote_header.strDebitNoteNo				
                    WHERE
                        fin_supplier_debitnote_header.intsupplier =  '$SupplierId' AND
                        fin_supplier_debitnote_header.intStatus = '1' AND
                        fin_supplier_debitnote_header.dtDate <='$date' AND
                        fin_supplier_debitnote_header.intCurrency=$currencyId AND 
                        fin_supplier_debitnote_header.intCompanyId=$companyId
                    )
                UNION
                    (
                        SELECT
                            (
                            SUM(ADV.dblReceivedAmount)
                            +
                            IFNULL ((SELECT
                            Sum(fin_supplier_payments_main_details.dblPayAmount )AS paidAmount
                            FROM fin_supplier_payments_main_details
                            Inner Join fin_supplier_payments_header ON fin_supplier_payments_main_details.strReferenceNo = fin_supplier_payments_header.strReferenceNo
                            WHERE
                            fin_supplier_payments_main_details.strDocNo =  ADV.strReceiptNo AND
                            fin_supplier_payments_header.intDeleteStatus =  '0' AND fin_supplier_payments_header.dtmDate<='$date'
                            GROUP BY
                            fin_supplier_payments_main_details.strDocNo),0)

                            ) * -1 AS amount
                        FROM
                            fin_supplier_advancepayment_header AS ADV
                            Inner Join mst_financecurrency ON ADV.intCurrency = mst_financecurrency.intId
                        WHERE
                            ADV.intsupplier =  '$SupplierId' AND
                            ADV.intStatus = '1' AND
                            ADV.intCurrency=$currencyId AND 
                            ADV.dtDate<='$date' AND 
                            ADV.intCompanyId=$companyId               
                    )
                UNION
                    (
                        SELECT
                            (
                            sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) 
                            -
                            IFNULL ((SELECT
                            Sum(fin_supplier_payments_main_details.dblPayAmount )AS paidAmount
                            FROM fin_supplier_payments_main_details
                            Inner Join fin_supplier_payments_header ON fin_supplier_payments_main_details.strReferenceNo = fin_supplier_payments_header.strReferenceNo
                            WHERE
                            fin_supplier_payments_main_details.strDocNo =  INV.strReferenceNo AND
                            fin_supplier_payments_header.intDeleteStatus =  '0' AND fin_supplier_payments_header.dtmDate<='$date'
                            GROUP BY
                            fin_supplier_payments_main_details.strDocNo),0)

                            ) AS amount
                        FROM
                            fin_supplier_purchaseinvoice_details AS INV
                            Inner Join fin_supplier_purchaseinvoice_header ON fin_supplier_purchaseinvoice_header.strReferenceNo = INV.strReferenceNo AND INV.intInvoiceNo = fin_supplier_purchaseinvoice_header.intInvoiceNo AND INV.intAccPeriodId = fin_supplier_purchaseinvoice_header.intAccPeriodId AND INV.intLocationId = fin_supplier_purchaseinvoice_header.intLocationId AND INV.intCompanyId = fin_supplier_purchaseinvoice_header.intCompanyId
                            Inner Join mst_finance_service_supplier ON mst_finance_service_supplier.intId = fin_supplier_purchaseinvoice_header.intsupplierId                            
                        WHERE
                            fin_supplier_purchaseinvoice_header.intsupplierId =  '$SupplierId' AND
                            fin_supplier_purchaseinvoice_header.intDeleteStatus =  '0' AND
                            fin_supplier_purchaseinvoice_header.intCurrencyId = $currencyId AND
                            fin_supplier_purchaseinvoice_header.dtmDate <='$date' AND                            
                            fin_supplier_purchaseinvoice_header.intCompanyId=$companyId
                        GROUP BY 
                            fin_supplier_purchaseinvoice_header.intsupplierId
                    )
                UNION
                    (
                        SELECT
                            (
                            SUM(fin_bankdeposit_details.dblAmmount) 
                            +
                            IFNULL ((SELECT
                            Sum(fin_supplier_payments_main_details.dblPayAmount )AS paidAmount
                            FROM fin_supplier_payments_main_details
                            Inner Join fin_supplier_payments_header ON fin_supplier_payments_main_details.strReferenceNo = fin_supplier_payments_header.strReferenceNo
                            WHERE
                            fin_supplier_payments_main_details.strDocNo =  BND.strDepositNo AND
                            fin_supplier_payments_header.intDeleteStatus =  '0' AND fin_supplier_payments_header.dtmDate<='$date'
                            GROUP BY
                            fin_supplier_payments_main_details.strDocNo),0)

                            ) AS amount
                        FROM
                            fin_bankdeposit_header BND
                            Inner Join fin_bankdeposit_details ON fin_bankdeposit_details.strDepositNo = BND.strDepositNo			
                            Inner Join mst_financechartofaccounts ON fin_bankdeposit_details.intAccount = mst_financechartofaccounts.intId
                        WHERE
                            fin_bankdeposit_details.intRecvFrom =  '$SupplierId' AND
                            mst_financechartofaccounts.intFinancialTypeId =  '18' AND
                            BND.intStatus = '1' AND
                            BND.intCurrency=$currencyId AND
                            BND.dtDate<='$date' AND
                            BND.intCompanyId=$companyId
                    )
                UNION
                    (
                        SELECT			
                            (
                            SUM(BNP.dblReceivedAmount) 
                            -
                            IFNULL ((SELECT
                            Sum(fin_supplier_payments_main_details.dblPayAmount )AS paidAmount
                            FROM fin_supplier_payments_main_details
                            Inner Join fin_supplier_payments_header ON fin_supplier_payments_main_details.strReferenceNo = fin_supplier_payments_header.strReferenceNo
                            WHERE
                            fin_supplier_payments_main_details.strDocNo =  BNP.strBankPaymentNo AND
                            fin_supplier_payments_header.intDeleteStatus =  '0' AND fin_supplier_payments_header.dtmDate<='$date'
                            GROUP BY
                            fin_supplier_payments_main_details.strDocNo),0)

                            ) * -1 AS amount
                        FROM
                            fin_bankpayment_header BNP
                            Inner Join fin_bankpayment_details ON fin_bankpayment_details.strBankPaymentNo = BNP.strBankPaymentNo				
                            Inner Join mst_financechartofaccounts ON fin_bankpayment_details.intAccountId = mst_financechartofaccounts.intId
                        WHERE
                            fin_bankpayment_details.intPayTo =  '$SupplierId' AND
                            mst_financechartofaccounts.intFinancialTypeId =  '18' AND
                            BNP.intStatus = '1' AND
                            BNP.intCurrency=$currencyId AND
                            BNP.dtDate <='$date' AND
                            BNP.intCompanyId=$companyId
                    )
                UNION
                    (
                        SELECT
                            (
                            SUM(PTC.dblReceivedAmount) 
                            -
                            IFNULL ((SELECT
                            Sum(fin_supplier_payments_main_details.dblPayAmount )AS paidAmount
                            FROM fin_supplier_payments_main_details
                            Inner Join fin_supplier_payments_header ON fin_supplier_payments_main_details.strReferenceNo = fin_supplier_payments_header.strReferenceNo
                            WHERE
                            fin_supplier_payments_main_details.strDocNo =  PTC.strPettyCashNo AND
                            fin_supplier_payments_header.intDeleteStatus =  '0' AND fin_supplier_payments_header.dtmDate<='$date'
                            GROUP BY
                            fin_supplier_payments_main_details.strDocNo),0)

                            ) * -1 AS amount
                        FROM
                            fin_bankpettycash_header AS PTC
                            Inner Join fin_bankpettycash_details ON fin_bankpettycash_details.strPettyCashNo = PTC.strPettyCashNo
                            Inner Join mst_financechartofaccounts ON fin_bankpettycash_details.intAccountId = mst_financechartofaccounts.intId
                        WHERE
                            mst_financechartofaccounts.intFinancialTypeId =  '18'
                            AND
                            fin_bankpettycash_details.intPayTo =  '$SupplierId' AND
                            PTC.intStatus = '1' AND
                            PTC.intCurrency=$currencyId AND
                            PTC.dtDate <='$date' AND
                            PTC.intCompanyId=$companyId
                    )
                UNION
                    (
                        SELECT     
                            SUM(
                            ((JD.dbCreditAmount) - (JD.dblDebitAmount))
                            -
                            IFNULL ((SELECT
				Sum(fin_supplier_payments_main_details.dblPayAmount )AS paidAmount
				FROM fin_supplier_payments_main_details
				Inner Join fin_supplier_payments_header ON fin_supplier_payments_main_details.strReferenceNo = fin_supplier_payments_header.strReferenceNo
				WHERE
				fin_supplier_payments_main_details.strDocNo =    JH.strReferenceNo AND
				fin_supplier_payments_header.intDeleteStatus =  '0' AND fin_supplier_payments_header.dtmDate<='$date'
				GROUP BY
				fin_supplier_payments_main_details.strDocNo),0)
				
				) AS amount
                        FROM
                            fin_accountant_journal_entry_header AS JH
                            INNER JOIN fin_accountant_journal_entry_details AS JD ON JH.intEntryNo = JD.intEntryNo                           
                            INNER JOIN mst_financechartofaccounts ON JD.intChartOfAccountId = mst_financechartofaccounts.intId
                        WHERE
                            JD.strPersonType = 'sup' AND
                            JD.intNameId = $SupplierId AND
                            JH.intDeleteStatus = 0 AND
                            mst_financechartofaccounts.intFinancialTypeId = 18 AND
                            JH.intCurrencyId=$currencyId AND
                            JH.dtmDate <='$date' AND
                            JH.intCompanyId=$companyId
                    )
            ) as tbl";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);    
    return $row['totAmount'];
}*/
?>
<?php

session_start();
$backwardseperator = "../../../../";
$companyId = $_SESSION['headCompanyId'];
$locationId = $_SESSION['CompanyID'];
$intUser = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath = $_SERVER['PHP_SELF'];
include "{$backwardseperator}dataAccess/Connector.php";
include "../accountReports/AccReportFunctions.php"; 

$suppliers = $_REQUEST['suppliers'];
$startDate = $_REQUEST['startDate'];
$endDate = $_REQUEST['endDate'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Other Payable / Service Supplier - Payments History</title>
        <link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
        <script type="application/javascript" src="../accountReports/DrillAccount.js"></script>        
                   
        <style>
            table.rptTable{
                width: 100%;
                border-collapse:collapse;                
            }
            table.rptTable tr td{
                border:1px solid black;                
            }
            .rptTblHeader {
                font-size: 11px;
                text-align: center;
                font-weight:bold;
                background-color: #E4E4E4;
            }
            .rptTblHeader1 {
                font-size: 11px;
                text-align: center;
                font-weight:bold;                
            }
            .rptTblBody {
                font-size: 10px;
            }
            .rptTblBodyTotal {
                font-size: 10px;
                font-weight:bold;
                background-color: #E4E4E4;
            }
            .drillLink{
                cursor:pointer;
            }
            a:hover
            { 
                font-weight: bold;
                text-decoration:underline;
                color: #0000FF;
            }
            .divresize{                
                padding:0px;                 
                resize:horizontal;
                overflow:auto;
            }
        </style>
       
        </head>

    <body>
        <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td colspan="3"></td>
                </tr>
                <tr>
                    <td width="20%"></td>
                    <td width="60%" height="80" valign="top"><?php include '../../reportHeader.php' ?></td>
                    <td width="20%"></td>
                </tr>

                <tr>
                    <td colspan="3"></td>
                </tr>
            </table>
        <div align="center">
            <div style="background-color:#FFF" ><strong>Other Payable / Service Supplier - Payments History</strong><strong></strong></div>
            <div style="background-color:#FFF" ><strong>From</strong>&nbsp;&nbsp;&nbsp;<i><?php echo $startDate ?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</i><strong>To</strong>&nbsp;&nbsp;&nbsp;<i><?php echo $endDate ?></i><strong></strong></div><br/>
            
            
            <table class="rptTable" id="dataTable">
                <?php
                foreach($suppliers as $supId){
                    $sql = "SELECT strName,strAddress FROM mst_finance_service_supplier WHERE intId =  '$supId'";
                    $result = $db->RunQuery($sql);
                    $row = mysqli_fetch_array($result);
                    $supplierName = $row['strName'];
                    $supplierAddress= $row['strAddress'];
                ?>
                    <tr class="rptTblHeader1">
                        <td colspan="7" align="left"><?php echo $supplierName ." - ".$supplierAddress?></td>                    
                    </tr>
                    <tr class="rptTblHeader">                                
                        <td class="divresize">Date</td>
                        <td>Number</td>
                        <td>Payment Method</td>
                        <td>Payment Number</td>
                        <td>Amount</td>                    
                        <td>Currency</td>                    
                    </tr>
                    <?php
                    $sqlDetails="SELECT
                                    SP.dtmDate,
                                    SP.strReferenceNo,
                                    PM.strName AS payMethod,
                                    SP.strPayRefNo,
                                    SP.dblRecAmount,
                                    CU.strCode AS currency
									FROM
									fin_other_payable_payments_header AS SP
									left outer Join mst_financepaymentsmethods AS PM ON SP.intPayMethodId = PM.intId
									Inner Join mst_financecurrency AS CU ON SP.intCurrencyId = CU.intId
									WHERE
									SP.intCompanyId =  '$companyId' AND
									SP.intSupplierId =  '$supId' AND
									SP.dtmDate >=  '$startDate' AND
									SP.dtmDate <=  '$endDate' AND
									SP.intDeleteStatus =  '0'";
                    $resultDetails = $db->RunQuery($sqlDetails);
                    while($rowDet = mysqli_fetch_array($resultDetails)){
                    ?>
                <tr class="rptTblBody">                                
                    <td><?php echo $rowDet['dtmDate'] ?></td>
                    <td><a class="drillLink" onclick="leadgerDrill('<?php echo 'Other Bill Payments' ?>','<?php echo $rowDet['strReferenceNo']?>')"> <?php echo $rowDet['strReferenceNo'] ?></a></td>
                    <td><?php echo $rowDet['payMethod'] ?></td>
                    <td><?php echo $rowDet['strPayRefNo'] ?></td>
                    <td><?php echo $rowDet['dblRecAmount'] ?></td>
                    <td><?php echo $rowDet['currency'] ?></td>
                </tr>
                <?php
                    }
                }
                ?>
            </table>
               
        </div>
    </body>
</html>
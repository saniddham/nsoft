<?php
session_start();
//BEGIN - INCLUDE FILES 	{
include_once "../../../../dataAccess/LoginDBManager.php";
include_once "../../../../libraries/mail/mail.php";	
//END 	- INCLUDE FILES 	}

//BEGIN - SET PARAMETERS	{
$companyId 			= $_SESSION['headCompanyId'];
$location 			= $_SESSION['CompanyID'];
$intUser 			= $_SESSION["userId"];
if(!isset($_REQUEST["Year"]))
	$year				= date('Y');
else
	$year				= $_REQUEST["Year"];
	
if(!isset($_REQUEST["Month"]))
	$month				= date('m');
else
	$month				= $_REQUEST["Month"];
	
$openingDate		= $year.'-'.$month.'-01';
$monthDays			= cal_days_in_month(CAL_GREGORIAN, $month, $year);
//END 	- SET PARAMETERS	}

$db =  new LoginDBManager();
ini_set('max_execution_time', 11111111) ;
ob_start();

$loop	= 0;
for($i=1;$i<=$monthDays;$i++)
{
	$calander_array[$loop][0] = date('d',mktime(0,0,0,$month, $i, $year));;
	$calander_array[$loop][1] = date('D',mktime(0,0,0,$month, $i, $year));
	$calander_array[$loop][2] = date('M',mktime(0,0,0,$month, $i, $year));
	$calander_array[$loop][3] = date('Y-m-d',mktime(0,0,0,$month, $i, $year));
	$loop++;
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Debtor's Outstanding Statement</title>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>
</head>
<body>
<style type="text/css">

.normalfntMid {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:center;
}

#tblMain{
	border-right: 1px solid #ccc;
	border-bottom: 1px solid #ccc;
	border-spacing: 0;
}

#tblMain thead {
    background-color: #dce9f9;
	border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
	text-align:center;
	 padding: 2px;
}
#tblMain td {
	border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
	 padding: 2px;
}
.normalfnt {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
</style>
<form id="frmDebtorsOutstandingStatement" name="frmDebtorsOutstandingStatement" method="post" action="rptDebtors_outstanding_statement.php">
  <div align="center">
  <div align="left"><img style="vertical-align:" src="http://accsee.com/nsoft_logo.png"  /></div>
  <div style="background-color:#FFF" ><strong>Debtor's Outstanding Statement</strong><strong></strong></div>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
 <tr>
   <td >&nbsp;</td>
 </tr>
 <tr>
  <td><table border="0" cellpadding="0" cellspacing="0"  width="100%" class="normalfnt" id="tblMain">
  <thead>
      <tr>
        <td>Debtor's Outstanding Statement</td>
        <td colspan="2">Opening Balance</td>
        <?php for($loop=0;$loop<count($calander_array);$loop++){?>
        <td <?php echo $calander_array[$loop][1]=='Sun'? 'style="background-color:#FF99CC"':'style="background-color:#dce9f9"'?>><?php echo $calander_array[$loop][1]?></td>
        <?php }?>
        <td>Total</td>
      </tr>
      <tr>
        <td>(Currency - USD)</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>       
		<?php for($loop=0;$loop<count($calander_array);$loop++){?>
        <td <?php echo $calander_array[$loop][1]=='Sun'? 'style="background-color:#FF99CC"':'style="background-color:#dce9f9"'?>><?php echo $calander_array[$loop][0].'-'.$calander_array[$loop][2]?></td>   
        <?php }?>
        <td>&nbsp;</td>
        </tr>
     <thead>
     <tbody>
      <tr>
        <td nowrap="nowrap">Brandix Apparels</td>
        <?php 
			$row_total				 = 0;	
			$opening_array 	 		 = GetOpeningBalance('IN','28,38,105');
			$normal_col_row_total 	+= $opening_array[0];
			$due_col_row_total 		+= $opening_array[1];
			$row_total				+= $opening_array[0];
			$row_total				+= $opening_array[1];
		?>
        <td nowrap="nowrap" style="text-align:right"><?php echo number_format($opening_array[0])?></td>
        <td nowrap="nowrap" style="color:#F00;text-align:right"><?php echo number_format($opening_array[1])?></td>
		<?php
			for($loop=0;$loop<count($calander_array);$loop++){
			$amount = GetDebtorOutstanding($calander_array[$loop][3],'IN','28,38,105');
			$tot_array[$calander_array[$loop][3]] += $amount;
			$row_total += $amount;
		?>
        <td nowrap="nowrap"  <?php echo $calander_array[$loop][1]=='Sun'? 'style="background-color:#FF99CC;text-align:right"':'style="text-align:right"'?>><?php echo number_format($amount)?></td> 
        <?php }?>
        <td nowrap="nowrap" style="text-align:right"><?php echo $row_total=='0'? '&nbsp;':number_format($row_total)?></td>
       </tr>
      <tr>
        <td nowrap="nowrap">Unichela </td>
        <?php 
			$row_total 				 = 0; 
			$opening_array 			 = GetOpeningBalance('IN','13,55');
			$normal_col_row_total 	+= $opening_array[0];
			$due_col_row_total 		+= $opening_array[1];
			$row_total				+= $opening_array[0];
			$row_total				+= $opening_array[1];
		?>
        <td nowrap="nowrap" style="text-align:right"><?php echo number_format($opening_array[0])?></td>
        <td nowrap="nowrap" style="color:#F00;text-align:right"><?php echo number_format($opening_array[1])?></td>
        <?php
			
			for($loop=0;$loop<count($calander_array);$loop++){
			$amount = GetDebtorOutstanding($calander_array[$loop][3],'IN','13,55');
			$tot_array[$calander_array[$loop][3]] += $amount;
			$row_total += $amount;
		?>
        <td nowrap="nowrap" <?php echo $calander_array[$loop][1]=='Sun'? 'style="background-color:#FF99CC;text-align:right"':'style="text-align:right"'?>><?php echo number_format($amount)?></td> 
<?php 		
			}
?>
        <td nowrap="nowrap" style="text-align:right"><?php echo $row_total=='0'? '&nbsp;':number_format($row_total)?></td>
      </tr>
      <tr>
        <td nowrap="nowrap">ICG </td>
<?php 
			$row_total				 = 0; 
			$opening_array 			 = GetOpeningBalance('NOT IN','13,55,28,38,105,75');
			$normal_col_row_total 	+= $opening_array[0];
			$due_col_row_total 		+= $opening_array[1];
			$row_total				+= $opening_array[0];
			$row_total				+= $opening_array[1];
?>
        <td nowrap="nowrap" style="text-align:right"><?php echo number_format($opening_array[0])?></td>
        <td nowrap="nowrap" style="color:#F00;text-align:right"><?php echo number_format($opening_array[1])?></td>
        <?php			 
			for($loop=0;$loop<count($calander_array);$loop++){
			$amount = GetDebtorOutstanding($calander_array[$loop][3],'NOT IN','13,55,28,38,105,75');
			$tot_array[$calander_array[$loop][3]] += $amount;
			$row_total += $amount;
		?>
        <td nowrap="nowrap" <?php echo $calander_array[$loop][1]=='Sun'? 'style="background-color:#FF99CC;text-align:right"':'style="text-align:right"'?>><?php echo number_format($amount)?></td> 
        <?php }?>
        <td nowrap="nowrap" style="text-align:right"><?php echo $row_total=='0'? '&nbsp;':number_format($row_total)?></td>
      </tr>      
      <tr>
        <td nowrap="nowrap" style="background-color:#E6E6E6">Total O/S (Local)</td>
        <td nowrap="nowrap" style="background-color:#E6E6E6;text-align:right"><?php echo number_format($normal_col_row_total)?></td>
        <td nowrap="nowrap" style="background-color:#E6E6E6;color:#F00;text-align:right"><?php echo number_format($due_col_row_total)?></td>
        <?php 
			$row_grand_tot = $normal_col_row_total+$due_col_row_total;
			for($loop=0;$loop<count($calander_array);$loop++){
			$row_grand_tot	+= $tot_array[$calander_array[$loop][3]];
			?>
        <td nowrap="nowrap" <?php echo $calander_array[$loop][1]=='Sun'? 'style="background-color:#FF99CC;text-align:right"':'style="background-color:#E6E6E6;text-align:right"'?>><?php echo $tot_array[$calander_array[$loop][3]]=='0'? '&nbsp;':number_format($tot_array[$calander_array[$loop][3]])?></td> 
        <?php }?>
        <td nowrap="nowrap" style="background-color:#E6E6E6;text-align:right"><?php echo $row_grand_tot=='0'? '&nbsp;':number_format($row_grand_tot)?></td>
      </tr>
      <tr>
        <td nowrap="nowrap" >&nbsp;</td>
        <td nowrap="nowrap" >&nbsp;</td>
        <td nowrap="nowrap" >&nbsp;</td>
		<?php for($loop=0;$loop<count($calander_array);$loop++){?>
        <td nowrap="nowrap" <?php echo $calander_array[$loop][1]=='Sun'? 'style="background-color:#FF99CC;text-align:right"':'style="text-align:right"'?>>&nbsp;</td>
		<?php } ?>
        <td nowrap="nowrap">&nbsp;</td>
      </tr>
      <tr>
        <td nowrap="nowrap" >Order Completed  Value</td>
        <?php
		$row_total 				 = 0;
		$normal_col_row_total 	 = 0;
		$due_col_row_total 		 = 0;
		$opening_array 			 = GetOrderCompleted_OpeningBalance();
		$normal_col_row_total 	+= $opening_array[0];
		$due_col_row_total 		+= $opening_array[1];
		$row_total				+= $opening_array[0];
		$row_total				+= $opening_array[1];
		
		?>
        <td nowrap="nowrap" style="text-align:right"><?php echo number_format($opening_array[0])?></td>
        <td nowrap="nowrap" style="color:#F00;text-align:right"><?php echo number_format($opening_array[1])?></td>
		<?php
			$tot_array = array();			
			for($loop=0;$loop<count($calander_array);$loop++){
			$oredrCompleted	= GetOrderCompletedValue($calander_array[$loop][3]);
			$tot_array[$calander_array[$loop][3]] += $oredrCompleted;
			$row_total += $oredrCompleted;
		?>
        <td nowrap="nowrap" <?php echo $calander_array[$loop][1]=='Sun'? 'style="background-color:#FF99CC;text-align:right"':'style="text-align:right"'?>><?php echo number_format($oredrCompleted)?></td>
		<?php } ?>
        <td nowrap="nowrap" style="text-align:right"><?php echo $row_total=='0'? '&nbsp;':number_format($row_total)?></td>
      </tr>
      <tr>
        <td nowrap="nowrap" >On Going Order Value</td>
        <?php
		$row_total 		 		 = 0;
		$opening_array 		 	 = GetOnGoing_OpeningBalance();
		$normal_col_row_total 	+= $opening_array[0];
		$due_col_row_total 		+= $opening_array[1];
		$row_total				+= $opening_array[0];
		$row_total				+= $opening_array[1];
		
		?>
        <td nowrap="nowrap" style="text-align:right"><?php echo number_format($opening_array[0])?></td>
        <td nowrap="nowrap" style="color:#F00;text-align:right"><?php echo number_format($opening_array[1])?></td>
		<?php			 
			for($loop=0;$loop<count($calander_array);$loop++){
			$orderOngoing	 = GetOngoing($calander_array[$loop][3]);
			$tot_array[$calander_array[$loop][3]] += $orderOngoing;
			$row_total 		+= $orderOngoing;
		?>
        <td nowrap="nowrap" <?php echo $calander_array[$loop][1]=='Sun'? 'style="background-color:#FF99CC;text-align:right"':'style="text-align:right"'?>><?php echo number_format($orderOngoing)?></td>
		<?php } ?>
        <td nowrap="nowrap" style="text-align:right"><?php echo $row_total=='0'? '&nbsp;':number_format($row_total)?></td>
      </tr>
      <tr>
        <td nowrap="nowrap" style="background-color:#E6E6E6">To be Invoiced Value</td>
        <td nowrap="nowrap" style="background-color:#E6E6E6;text-align:right"><?php echo number_format($normal_col_row_total)?></td>
        <td nowrap="nowrap" style="background-color:#E6E6E6;color:#F00;text-align:right"><?php echo number_format($due_col_row_total)?></td>
        <?php 
			$row_grand_tot = $normal_col_row_total+$due_col_row_total;
			for($loop=0;$loop<count($calander_array);$loop++)
			{
				$row_grand_tot	+= $tot_array[$calander_array[$loop][3]];
		?>
        <td nowrap="nowrap" <?php echo $calander_array[$loop][1]=='Sun'? 'style="background-color:#FF99CC;text-align:right"':'style="background-color:#E6E6E6;text-align:right"'?>><?php echo $tot_array[$calander_array[$loop][3]]=='0'? '&nbsp;':number_format($tot_array[$calander_array[$loop][3]])?></td> 
        <?php }?>
        <td nowrap="nowrap" style="background-color:#E6E6E6;text-align:right"><?php echo $row_grand_tot=='0'? '&nbsp;':number_format($row_grand_tot)?></td>
      </tr>
     </tbody>
  </table></td>
</tr>
            <tr>
<tr height="40">
  <td align="center" class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></td>
</tr>
</table>
</div>
</form>
</body>
</html>
<?php
function GetDebtorOutstanding($date,$criteria,$customerList)
{
	global $db;
	$amount = 0;
	$sql = "SELECT SUM(SID.dblUnitPrice * SID.dblQty) AS AMOUNT1,
	          	(COALESCE(SUM(SID.dblUnitPrice * SID.dblQty),0) - COALESCE((SELECT
				  COALESCE(SUM(dblPayAmount),0)
				FROM fin_customer_receivedpayments_header CRPH
				  INNER JOIN fin_customer_receivedpayments_main_details CRPMD
					ON CRPMD.intReceiptNo = CRPH.intReceiptNo
					  AND CRPH.intAccPeriodId = CRPMD.intAccPeriodId 
					  AND CRPH.intLocationId = CRPMD.intLocationId 
					  AND CRPH.intCompanyId = CRPMD.intCompanyId 
					  AND CRPH.strReferenceNo = CRPMD.strReferenceNo 
				   WHERE CRPH.intDeleteStatus  = 0
				   AND CRPMD.strDocNo = SIH.strReferenceNo),0)) AS AMOUNT
   
			FROM fin_customer_salesinvoice_header SIH
			INNER JOIN fin_customer_salesinvoice_details SID ON SIH.intInvoiceNo = SID.intInvoiceNo
			INNER JOIN trn_orderheader OH
				ON OH.strCustomerPoNo = SIH.strPoNo
				AND OH.intCustomer = SIH.intCustomerId
			WHERE SIH.dtmDate = '$date' 
				AND OH.intStatus <> '-2'
				AND SIH.intDeleteStatus = 0
				AND intCustomerId $criteria ($customerList)";
	$result =  $db->RunQuery($sql);
	while($row = mysqli_fetch_array($result))
	{
		$amount = $row["AMOUNT"];
	}
	return ($amount=='0'?'&nbsp;':round($amount));
}

function GetDebtorOutstanding1($date,$criteria,$customerList)
{
	global $db;
	$amount = 0;
	$sql = "SELECT SIH.intCurrencyId,SUM(SID.dblUnitPrice * SID.dblQty) AS AMOUNT1,
	          	(COALESCE(SUM(SID.dblUnitPrice * SID.dblQty),0) - COALESCE((SELECT
				  COALESCE(SUM(dblPayAmount),0)
				FROM fin_customer_receivedpayments_header CRPH
				  INNER JOIN fin_customer_receivedpayments_main_details CRPMD
					ON CRPMD.intReceiptNo = CRPH.intReceiptNo
					  AND CRPH.intAccPeriodId = CRPMD.intAccPeriodId 
					  AND CRPH.intLocationId = CRPMD.intLocationId 
					  AND CRPH.intCompanyId = CRPMD.intCompanyId 
					  AND CRPH.strReferenceNo = CRPMD.strReferenceNo 
				   WHERE CRPH.intDeleteStatus  = 0
				   AND CRPMD.strDocNo = SIH.strReferenceNo),0)) AS AMOUNT
   
			FROM fin_customer_salesinvoice_header SIH
			INNER JOIN fin_customer_salesinvoice_details SID ON SIH.intInvoiceNo = SID.intInvoiceNo
			WHERE SIH.dtmDate = '$date' 
				AND SIH.intDeleteStatus = 0
				AND intCustomerId $criteria ($customerList)
			GROUP BY SIH.intCurrencyId";
	$result =  $db->RunQuery($sql);
	while($row = mysqli_fetch_array($result))
	{
		$currency	= GetExchangeRate($row["intCurrencyId"]);
		$amount = $row["AMOUNT"];
		
	}
	return ($amount=='0'?'&nbsp;':round($amount));
}

function GetExchangeRate($currencyId)
{
	global $db;
}

function GetOpeningBalance($criteria,$customerList)
{
	global $db;
	global $openingDate;
	
	$redQty		= 0;
	$normalQty	= 0;
	$data_array	= array();
	$sql = "SELECT 
				DATE(dtmDate)						AS INVOINCE_DATE,
				CSIH.intPaymentsTermsId				AS PAYMENT_TERM_ID,
				FPT.strName							AS CREDIT_TERM,
				DATEDIFF(DATE(NOW()),DATE(dtmDate)) AS DIFF_DATE,				
					   
				ROUND(COALESCE(SUM(CSID.dblUnitPrice * CSID.dblQty),0) - COALESCE((SELECT
							  COALESCE(SUM(dblPayAmount),0)
							FROM fin_customer_receivedpayments_header CRPH
							  INNER JOIN fin_customer_receivedpayments_main_details CRPMD
								ON CRPMD.intReceiptNo = CRPH.intReceiptNo
								  AND CRPH.intAccPeriodId = CRPMD.intAccPeriodId 
								  AND CRPH.intLocationId = CRPMD.intLocationId 
								  AND CRPH.intCompanyId = CRPMD.intCompanyId 
								  AND CRPH.strReferenceNo = CRPMD.strReferenceNo 
							   WHERE CRPH.intDeleteStatus  = 0
							   AND CRPMD.strDocNo = CSIH.strReferenceNo
							   AND CRPMD.strDocNo = CSIH.strReferenceNo
							   AND CRPMD.intCompanyId = CSIH.intCompanyId
							   AND CRPMD.intLocationId = CSIH.intLocationId
							   AND CRPMD.intAccPeriodId = CSIH.intAccPeriodId),0) - ROUND(COALESCE((SELECT
							  SUM(CCD.dblQty * CCD.dblUnitPrice)
							FROM fin_customer_creditnote_header CCH
							  INNER JOIN fin_customer_creditnote_details CCD
								ON CCD.intInvoiceNo = CCH.intInvoiceNo
								  AND CCD.intAccPeriodId = CCH.intAccPeriodId
								  AND CCD.intLocationId = CCH.intLocationId
								  AND CCD.intCompanyId = CCH.intCompanyId
								  AND CCD.strReferenceNo = CCH.strReferenceNo
							   WHERE CCH.intDeleteStatus  = 0
							   	AND CCH.strInvoiceNo = CSIH.strReferenceNo
							   	AND CCH.intAccPeriodId = CSIH.intAccPeriodId
							   	AND CCH.intLocationId = CSIH.intLocationId
							   	AND CCH.intCompanyId = CSIH.intCompanyId),0))) AS AMOUNT
   
			FROM fin_customer_salesinvoice_header CSIH
			INNER JOIN fin_customer_salesinvoice_details CSID
				ON CSID.intInvoiceNo = CSIH.intInvoiceNo
			INNER JOIN mst_financepaymentsterms FPT
				 ON FPT.intId = CSIH.intPaymentsTermsId 
				  AND CSID.intAccPeriodId = CSIH.intAccPeriodId
				  AND CSID.intLocationId = CSIH.intLocationId
				  AND CSID.intCompanyId = CSIH.intCompanyId
				  AND CSID.strReferenceNo = CSIH.strReferenceNo
			INNER JOIN trn_orderheader OH
				ON OH.strCustomerPoNo = CSIH.strPoNo
				AND OH.intCustomer = CSIH.intCustomerId
			WHERE intCustomerId $criteria ($customerList)
				  AND DATE(CSIH.dtmDate) < '$openingDate'
				  AND OH.intStatus <> '-2'
			GROUP BY CSIH.intInvoiceNo,CSIH.intAccPeriodId,CSIH.intLocationId,CSIH.intCompanyId,CSIH.strReferenceNo ";
	$result = $db->RunQuery($sql);
	while($row = mysqli_fetch_array($result))
	{
		$amount		= $row["AMOUNT"];
		if($row["CREDIT_TERM"]<$row["DIFF_DATE"])
			$redQty	+= $amount;
		else
			$normalQty	+= $amount;		
	}
	$data_array[0]	= round($normalQty);
	$data_array[1]	= round($redQty);
	return $data_array;
}

function GetOrderCompletedValue($date)
{
	global $db;
	$amount = 0;
	$sql = "SELECT t.*, 
				ROUND(ORDER_PRICE/ORDERQTY,4)AS PRICE,
				ROUND(DISPATCH * (ORDER_PRICE/ORDERQTY)) - (SALES_INVOICE_AMOUNT - CREDIT_NOTE_AMOUNT) 	AS DISPATCH_VALUE 
				
			FROM (SELECT
					  FDH.intOrderYear,
					  FDH.intOrderNo,
					  SUM(FDD.dblGoodQty) AS DISPATCH,
					  MAX(DATE(dtmdate))AS DATE,
					  (SELECT SUM(OD.intQty)
				  FROM trn_orderheader OH 
				  INNER JOIN trn_orderdetails OD
					ON OD.intOrderNo = OH.intOrderNo
					AND OD.intOrderYear = OH.intOrderYear
				  WHERE OH.intOrderNo = FDH.intOrderNo
				  AND OH.intOrderYear = FDH.intOrderYear)							AS ORDERQTY,
				  
				  (SELECT SUM(OD.intQty * OD.dblPrice)
				  FROM trn_orderheader OH 
				  INNER JOIN trn_orderdetails OD
					ON OD.intOrderNo = OH.intOrderNo
					AND OD.intOrderYear = OH.intOrderYear
				  WHERE OH.intOrderNo = FDH.intOrderNo
				  AND OH.intOrderYear = FDH.intOrderYear)							AS ORDER_PRICE,
				  
				  ROUND(COALESCE((SELECT SUM(dblUnitPrice * dblQty)
			   FROM fin_customer_salesinvoice_header CSIH
			   INNER JOIN fin_customer_salesinvoice_details CSID
				 ON CSIH.intInvoiceNo = CSID.intInvoiceNo 
				 AND CSIH.intAccPeriodId = CSID.intAccPeriodId 
				 AND CSIH.intLocationId = CSID.intLocationId 
				 AND CSIH.intCompanyId = CSID.intCompanyId 
				 AND CSIH.strReferenceNo = CSID.strReferenceNo
			   WHERE CSIH.strPoNo = OH1.strCustomerPoNo
			   		AND CSIH.intCustomerId = OH1.intCustomer
			     AND intDeleteStatus = 0),0))										AS SALES_INVOICE_AMOUNT,
				 
			ROUND(COALESCE((SELECT
			  SUM(CCD.dblQty * CCD.dblUnitPrice)
			FROM fin_customer_creditnote_header CCH
			  INNER JOIN fin_customer_creditnote_details CCD
				ON CCD.intInvoiceNo = CCH.intInvoiceNo
				  AND CCD.intAccPeriodId = CCH.intAccPeriodId
				  AND CCD.intLocationId = CCH.intLocationId
				  AND CCD.intCompanyId = CCH.intCompanyId
				  AND CCD.strReferenceNo = CCH.strReferenceNo
			  INNER JOIN fin_customer_salesinvoice_header CSIH
				ON CCH.strInvoiceNo = CSIH.strReferenceNo
				AND CCH.intAccPeriodId = CSIH.intAccPeriodId
				AND CCH.intLocationId = CSIH.intLocationId
				AND CCH.intCompanyId = CSIH.intCompanyId
			   WHERE CCH.intDeleteStatus  = 0
				AND CSIH.intDeleteStatus = 0
				AND CSIH.strPoNo = OH1.strCustomerPoNo
				AND CSIH.intCustomerId = OH1.intCustomer),0))						AS CREDIT_NOTE_AMOUNT
				  
				FROM ware_fabricdispatchheader FDH
				INNER JOIN ware_fabricdispatchdetails FDD
					ON FDD.intBulkDispatchNo = FDH.intBulkDispatchNo
					AND FDD.intBulkDispatchNoYear = FDH.intBulkDispatchNoYear
				INNER JOIN trn_orderheader OH1 
					ON OH1.intOrderNo = FDH.intOrderNo
					AND OH1.intOrderYear = FDH.intOrderYear
				WHERE 
					FDH.intStatus = '1'
					AND OH1.intStatus <> '-2'
					AND OH1.intCustomer NOT IN (181)
				GROUP BY FDH.intOrderYear,FDH.intOrderNo
				ORDER BY FDH.intOrderNo)AS t
			WHERE DISPATCH>=ORDERQTY
			AND DATE = '$date'";
	$result =  $db->RunQuery($sql);
	while($row = mysqli_fetch_array($result))
	{
		$amount += $row["DISPATCH_VALUE"];
	}
	return ($amount=='0'?'&nbsp;':round($amount));
}

function GetOngoing($date)
{
	global $db;
	$amount = 0;
	$sql = "SELECT t.*, 
				ROUND(ORDER_PRICE/ORDERQTY,4)										AS PRICE,
				ROUND(SUM(DISPATCH) * (ORDER_PRICE/ORDERQTY)) - (SALES_INVOICE_AMOUNT - CREDIT_NOTE_AMOUNT)	AS DISPATCH_VALUE 
			FROM (SELECT
				FDD.intBulkDispatchNo,
				FDD.intBulkDispatchNoYear,
				FDH.dtmdate															AS DISPATCHED_DATE,
			  	FDH.intOrderYear,
			  	FDH.intOrderNo,
			  	SUM(FDD.dblGoodQty) 												AS DISPATCH,
			  
			  	(SELECT SUM(OD.intQty)
			  	FROM trn_orderheader OH 
			 	INNER JOIN trn_orderdetails OD
					ON OD.intOrderNo = OH.intOrderNo
					AND OD.intOrderYear = OH.intOrderYear
			  	WHERE OH.intOrderNo = FDH.intOrderNo
			  	AND OH.intOrderYear = FDH.intOrderYear)								AS ORDERQTY,
			  
			    (SELECT  SUM(FDD1.dblGoodQty)
			  	FROM ware_fabricdispatchheader FDH1
				INNER JOIN ware_fabricdispatchdetails FDD1
					ON FDD1.intBulkDispatchNo = FDH1.intBulkDispatchNo
					AND FDD1.intBulkDispatchNoYear = FDH1.intBulkDispatchNoYear
				WHERE FDH1.intStatus = '1'
					AND FDH1.intOrderNo= FDH.intOrderNo
					AND FDH1.intOrderYear = FDH.intOrderYear) 						AS SUM_DISPATCH,
			  
				(SELECT SUM(OD.intQty * OD.dblPrice)
			  	FROM trn_orderheader OH 
			  	INNER JOIN trn_orderdetails OD
					ON OD.intOrderNo = OH.intOrderNo
					AND OD.intOrderYear = OH.intOrderYear
			  	WHERE OH.intOrderNo = FDH.intOrderNo
			 		AND OH.intOrderYear = FDH.intOrderYear)							AS ORDER_PRICE,
					
				ROUND(COALESCE((SELECT SUM(dblUnitPrice * dblQty)
			   FROM fin_customer_salesinvoice_header CSIH
			   INNER JOIN fin_customer_salesinvoice_details CSID
				 ON CSIH.intInvoiceNo = CSID.intInvoiceNo 
				 AND CSIH.intAccPeriodId = CSID.intAccPeriodId 
				 AND CSIH.intLocationId = CSID.intLocationId 
				 AND CSIH.intCompanyId = CSID.intCompanyId 
				 AND CSIH.strReferenceNo = CSID.strReferenceNo
			   WHERE CSIH.strPoNo = OH1.strCustomerPoNo
			   		AND CSIH.intCustomerId = OH1.intCustomer		
			     	AND intDeleteStatus = 0),0))									AS SALES_INVOICE_AMOUNT,
					
			ROUND(COALESCE((SELECT
			  SUM(CCD.dblQty * CCD.dblUnitPrice)
			FROM fin_customer_creditnote_header CCH
			  INNER JOIN fin_customer_creditnote_details CCD
				ON CCD.intInvoiceNo = CCH.intInvoiceNo
				  AND CCD.intAccPeriodId = CCH.intAccPeriodId
				  AND CCD.intLocationId = CCH.intLocationId
				  AND CCD.intCompanyId = CCH.intCompanyId
				  AND CCD.strReferenceNo = CCH.strReferenceNo
			  INNER JOIN fin_customer_salesinvoice_header CSIH
				ON CCH.strInvoiceNo = CSIH.strReferenceNo
				AND CCH.intAccPeriodId = CSIH.intAccPeriodId
				AND CCH.intLocationId = CSIH.intLocationId
				AND CCH.intCompanyId = CSIH.intCompanyId
			   WHERE CCH.intDeleteStatus  = 0
				AND CSIH.intDeleteStatus = 0
				AND CSIH.strPoNo = OH1.strCustomerPoNo
				AND CSIH.intCustomerId = OH1.intCustomer),0))						AS CREDIT_NOTE_AMOUNT
			  
			FROM ware_fabricdispatchheader FDH
			INNER JOIN ware_fabricdispatchdetails FDD
				ON FDD.intBulkDispatchNo = FDH.intBulkDispatchNo
				AND FDD.intBulkDispatchNoYear = FDH.intBulkDispatchNoYear
			INNER JOIN trn_orderheader OH1 
				ON OH1.intOrderNo = FDH.intOrderNo
				AND OH1.intOrderYear = FDH.intOrderYear
			WHERE 
				FDH.intStatus = '1'
				AND OH1.intStatus <> '-2'			
				AND OH1.intCustomer NOT IN (181)
				AND FDH.dtmdate = '$date'
			GROUP BY FDD.intBulkDispatchNo,FDD.intBulkDispatchNoYear
			ORDER BY FDH.intOrderNo)AS t
			WHERE SUM_DISPATCH < ORDERQTY
			-- AND DISPATCHED_DATE = '$date'
			ORDER BY intOrderNo,intOrderYear";
	$result =  $db->RunQuery($sql);
	while($row = mysqli_fetch_array($result))
	{
		$amount += $row["DISPATCH_VALUE"];
	}
	return ($amount=='0'?'&nbsp;':round($amount));
}

function GetOrderCompleted_OpeningBalance()
{
	global $db;
	global $openingDate;
	
	$sql = "SELECT t.*, 
				ROUND(ORDER_PRICE/ORDERQTY,4)									AS PRICE,
				ROUND(DISPATCH * (ORDER_PRICE/ORDERQTY)) 						AS DISPATCH_VALUE ,
				ROUND(DISPATCH * (ORDER_PRICE/ORDERQTY))- (SALES_INVOICE_AMOUNT - CREDIT_NOTE_AMOUNT) 	AS FINAL_VALUE
			FROM (SELECT
			  FDH.intOrderYear,
			  FDH.intOrderNo,
			  ROUND(SUM(FDD.dblGoodQty)) 										AS DISPATCH,
			  MAX(DATE(dtmdate))												AS DATE,
			  DATEDIFF(DATE(NOW()),MAX(DATE(dtmdate))) 							AS DATE_DIFF,
			  FPT.strName 														AS CREDIT_TERM,
			  
			  (SELECT SUM(OD.intQty)
			  FROM trn_orderheader OH 
			  INNER JOIN trn_orderdetails OD
				ON OD.intOrderNo = OH.intOrderNo
				AND OD.intOrderYear = OH.intOrderYear
			  WHERE OH.intOrderNo = FDH.intOrderNo
			  AND OH.intOrderYear = FDH.intOrderYear)							AS ORDERQTY,
			  
				(SELECT SUM(OD.intQty * OD.dblPrice)
			  FROM trn_orderheader OH 
			  INNER JOIN trn_orderdetails OD
				ON OD.intOrderNo = OH.intOrderNo
				AND OD.intOrderYear = OH.intOrderYear
			  WHERE OH.intOrderNo = FDH.intOrderNo
			  AND OH.intOrderYear = FDH.intOrderYear)							AS ORDER_PRICE,
			  
			  ROUND(COALESCE((SELECT SUM(dblUnitPrice * dblQty)
			   FROM fin_customer_salesinvoice_header CSIH
			   INNER JOIN fin_customer_salesinvoice_details CSID
				 ON CSIH.intInvoiceNo = CSID.intInvoiceNo 
				 AND CSIH.intAccPeriodId = CSID.intAccPeriodId 
				 AND CSIH.intLocationId = CSID.intLocationId 
				 AND CSIH.intCompanyId = CSID.intCompanyId 
				 AND CSIH.strReferenceNo = CSID.strReferenceNo
			   WHERE CSIH.strPoNo = OH1.strCustomerPoNo
			   		AND CSIH.intCustomerId = OH1.intCustomer
			     AND intDeleteStatus = 0),0))									AS SALES_INVOICE_AMOUNT,
				 
			ROUND(COALESCE((SELECT
			  SUM(CCD.dblQty * CCD.dblUnitPrice)
			FROM fin_customer_creditnote_header CCH
			  INNER JOIN fin_customer_creditnote_details CCD
				ON CCD.intInvoiceNo = CCH.intInvoiceNo
				  AND CCD.intAccPeriodId = CCH.intAccPeriodId
				  AND CCD.intLocationId = CCH.intLocationId
				  AND CCD.intCompanyId = CCH.intCompanyId
				  AND CCD.strReferenceNo = CCH.strReferenceNo
			  INNER JOIN fin_customer_salesinvoice_header CSIH
				ON CCH.strInvoiceNo = CSIH.strReferenceNo
				AND CCH.intAccPeriodId = CSIH.intAccPeriodId
				AND CCH.intLocationId = CSIH.intLocationId
				AND CCH.intCompanyId = CSIH.intCompanyId
			   WHERE CCH.intDeleteStatus  = 0
				AND CSIH.intDeleteStatus = 0
				AND CSIH.strPoNo = OH1.strCustomerPoNo
				AND CSIH.intCustomerId = OH1.intCustomer),0))						AS CREDIT_NOTE_AMOUNT
			  
			FROM ware_fabricdispatchheader FDH
			INNER JOIN ware_fabricdispatchdetails FDD
				ON FDD.intBulkDispatchNo = FDH.intBulkDispatchNo
				AND FDD.intBulkDispatchNoYear = FDH.intBulkDispatchNoYear
			INNER JOIN trn_orderheader OH1 
				ON OH1.intOrderNo = FDH.intOrderNo
				AND OH1.intOrderYear = FDH.intOrderYear
			INNER JOIN mst_financepaymentsterms FPT
				 ON FPT.intId = OH1.intPaymentTerm 
			WHERE 
				FDH.intStatus = '1'
				AND OH1.intStatus <> '-2'
				AND OH1.intCustomer NOT IN (181)
			GROUP BY FDH.intOrderYear,FDH.intOrderNo
			ORDER BY FDH.intOrderNo)AS t
			WHERE DISPATCH>=ORDERQTY
			AND DATE < '$openingDate'";
	$result = $db->RunQuery($sql);
	while($row = mysqli_fetch_array($result))
	{
		$amount		= $row["FINAL_VALUE"];
		if($row["CREDIT_TERM"]<$row["DATE_DIFF"])
			$redQty	+= $amount;
		else
			$normalQty	+= $amount;		
	}
	$data_array[0]	= round($normalQty);
	$data_array[1]	= round($redQty);
	return $data_array;
}

function GetOnGoing_OpeningBalance()
{
	global $db;
	global $openingDate;
	
	$sql = "SELECT t.*, 
				ROUND(ORDER_PRICE/ORDERQTY,4)																AS PRICE,
				ROUND(SUM(DISPATCH) * (ORDER_PRICE/ORDERQTY)) 												AS DISPATCH_VALUE,
				ROUND(SUM(DISPATCH) * (ORDER_PRICE/ORDERQTY))- (SALES_INVOICE_AMOUNT - CREDIT_NOTE_AMOUNT) 	AS FINAL_VALUE
			FROM (SELECT
				FDD.intBulkDispatchNo,
				FDD.intBulkDispatchNoYear,
				FDH.dtmdate	AS DISPATCHED_DATE,
			  	FDH.intOrderYear												AS ORDER_YEAR,
			  	FDH.intOrderNo													AS ORDER_NO,
				DATEDIFF(DATE(NOW()),MAX(DATE(FDH.dtmdate))) 					AS DATE_DIFF,
			  	SUM(FDD.dblGoodQty) 											AS DISPATCH,
				FPT.strName AS CREDIT_TERM,
			  
			  	(SELECT SUM(OD.intQty)
			  	FROM trn_orderheader OH 
			 	INNER JOIN trn_orderdetails OD
					ON OD.intOrderNo = OH.intOrderNo
					AND OD.intOrderYear = OH.intOrderYear
			  	WHERE OH.intOrderNo = FDH.intOrderNo
			  	AND OH.intOrderYear = FDH.intOrderYear)							AS ORDERQTY,
			  
			    (SELECT  SUM(FDD1.dblGoodQty)
			  	FROM ware_fabricdispatchheader FDH1
				INNER JOIN ware_fabricdispatchdetails FDD1
					ON FDD1.intBulkDispatchNo = FDH1.intBulkDispatchNo
					AND FDD1.intBulkDispatchNoYear = FDH1.intBulkDispatchNoYear
				WHERE FDH1.intStatus = '1'
					AND FDH1.intOrderNo= FDH.intOrderNo
					AND FDH1.intOrderYear = FDH.intOrderYear) 					AS SUM_DISPATCH,
			  
				(SELECT SUM(OD.intQty * OD.dblPrice)
			  	FROM trn_orderheader OH 
			  	INNER JOIN trn_orderdetails OD
					ON OD.intOrderNo = OH.intOrderNo
					AND OD.intOrderYear = OH.intOrderYear
			  	WHERE OH.intOrderNo = FDH.intOrderNo
			 		AND OH.intOrderYear = FDH.intOrderYear)						AS ORDER_PRICE,
					
				ROUND(COALESCE((SELECT SUM(dblUnitPrice * dblQty)
			   FROM fin_customer_salesinvoice_header CSIH
			   INNER JOIN fin_customer_salesinvoice_details CSID
				 ON CSIH.intInvoiceNo = CSID.intInvoiceNo 
				 AND CSIH.intAccPeriodId = CSID.intAccPeriodId 
				 AND CSIH.intLocationId = CSID.intLocationId 
				 AND CSIH.intCompanyId = CSID.intCompanyId 
				 AND CSIH.strReferenceNo = CSID.strReferenceNo
			   WHERE CSIH.strPoNo = OH1.strCustomerPoNo
			   		AND CSIH.intCustomerId = OH1.intCustomer
			     AND intDeleteStatus = 0),0))									AS SALES_INVOICE_AMOUNT,
				 
			ROUND(COALESCE((SELECT
			  SUM(CCD.dblQty * CCD.dblUnitPrice)
			FROM fin_customer_creditnote_header CCH
			  INNER JOIN fin_customer_creditnote_details CCD
				ON CCD.intInvoiceNo = CCH.intInvoiceNo
				  AND CCD.intAccPeriodId = CCH.intAccPeriodId
				  AND CCD.intLocationId = CCH.intLocationId
				  AND CCD.intCompanyId = CCH.intCompanyId
				  AND CCD.strReferenceNo = CCH.strReferenceNo
			  INNER JOIN fin_customer_salesinvoice_header CSIH
				ON CCH.strInvoiceNo = CSIH.strReferenceNo
				AND CCH.intAccPeriodId = CSIH.intAccPeriodId
				AND CCH.intLocationId = CSIH.intLocationId
				AND CCH.intCompanyId = CSIH.intCompanyId
			   WHERE CCH.intDeleteStatus  = 0
				AND CSIH.intDeleteStatus = 0
				AND CSIH.strPoNo = OH1.strCustomerPoNo
				AND CSIH.intCustomerId = OH1.intCustomer),0))					AS CREDIT_NOTE_AMOUNT
			  
			FROM ware_fabricdispatchheader FDH
			INNER JOIN ware_fabricdispatchdetails FDD
				ON FDD.intBulkDispatchNo = FDH.intBulkDispatchNo
				AND FDD.intBulkDispatchNoYear = FDH.intBulkDispatchNoYear
			INNER JOIN trn_orderheader OH1 
				ON OH1.intOrderNo = FDH.intOrderNo
				AND OH1.intOrderYear = FDH.intOrderYear
			INNER JOIN mst_financepaymentsterms FPT
				 ON FPT.intId = OH1.intPaymentTerm 
			WHERE 
				FDH.intStatus = '1'
				AND OH1.intStatus <> '-2'		
				AND OH1.intCustomer NOT IN (181)
			GROUP BY FDD.intBulkDispatchNo,FDD.intBulkDispatchNoYear
			ORDER BY FDH.intOrderNo)AS t
			WHERE SUM_DISPATCH < ORDERQTY
			AND DISPATCHED_DATE < '$openingDate'
			GROUP BY ORDER_YEAR,ORDER_NO
			ORDER BY ORDER_YEAR,ORDER_NO";
	$result = $db->RunQuery($sql);
	while($row = mysqli_fetch_array($result))
	{
		$amount		= $row["FINAL_VALUE"];
		if($row["CREDIT_TERM"]<$row["DATE_DIFF"])
			$redQty	+= $amount;
		else
			$normalQty	+= $amount;		
	}
	$data_array[0]	= round($normalQty);
	$data_array[1]	= round($redQty);
	return $data_array;
}

    $body 		= ob_get_clean();
	$nowDate 	= date('Y-m-d');
	$mailHeader	= "CUSTOMER INVOICE OUTSTANDING AGING REPORT ($nowDate)";
	$sql = "SELECT
				sys_users.strEmail,sys_users.strFullName
			FROM
			sys_mail_eventusers
				Inner Join sys_users ON sys_users.intUserId = sys_mail_eventusers.intUserId
			WHERE
				sys_mail_eventusers.intMailEventId =  '1013'";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		sendMessage('nsoft@screenlineholdings.com','NSOFT - SCREENLINE HOLDINGS.',$row['strEmail'],$mailHeader,$body);		
	}
	echo $body;	
	echo 'CUSTOMER INVOICE OUTSTANDING AGING REPORT -> OK <br>';
?>
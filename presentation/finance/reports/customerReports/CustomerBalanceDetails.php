<?php
session_start();
$backwardseperator = "../../../../";
$companyId = $_SESSION['headCompanyId'];
$location = $_SESSION['CompanyID'];
$intUser = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath = $_SERVER['PHP_SELF'];
$locationId = $location; //this locationId use in report header(reportHeader.php)--------------------
include "{$backwardseperator}dataAccess/Connector.php";

$custId = $_REQUEST['custId'];
$currency = $_REQUEST['currency'];
$filterCompany = $_REQUEST['company'];
$toDay = $_REQUEST['toDay'];

//-------------------------
$sql = "SELECT intId,strCode FROM mst_financecurrency WHERE intId = '$currency'";
$result = $db->RunQuery($sql);
$row = mysqli_fetch_array($result);
$currencyDesc = $row['strCode'];
//-------------------------
$sql = "SELECT mst_companies.strName FROM mst_companies WHERE mst_companies.intId =  '$filterCompany'";
$result = $db->RunQuery($sql);
$row = mysqli_fetch_array($result);
$filterCompName = $row['strName'];
//-------------------------
$result = $db->RunQuery("SELECT mst_customer.strName,mst_customer.strAddress FROM mst_customer WHERE mst_customer.intId=$custId");
$row = mysqli_fetch_array($result);
$cusName = $row['strName'];
$cusAddress = $row['strAddress'];

$amountTot=0;
$toBeTot=0;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Customer Balance Details</title>
        <link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
        <script type="application/javascript" src="../accountReports/DrillAccount.js"></script>
        <style>
            .custname{
                font-family: Verdana;
                font-size: 13px;
                color: #000000;
                margin: 0px;
                text-align: center;
            }
            table.maintable{
                width: 75%;                
                border-collapse:collapse;
                border-bottom: 1px solid black;
                border-top:1px solid black;
                border-left:1px solid black;
                border-right:1px solid black;
            }
            table.maintable tr td{
                /*border:1px solid black;*/
            }
            .maintableHeader{
                font-family: Verdana;
                font-size: 12px;
                color: #000000;
                margin: 0px;
                text-align: center;
            }
            .subHed{
                font-family: Verdana;
                font-size: 11px;
                color: #000000;
                margin: 0px;
                text-align: left;                
                font-weight:bold;

            }
            .subHed1{
                font-family: Verdana;
                font-size: 11px;
                color: #000000;
                margin: 0px;
                text-align: left;
            }
            .figurs{
                font-family: Verdana;
                font-size: 10px;
                color: #000000;
                margin: 0px;
                text-align: right;
                cursor: pointer;
            }
            .total{
                font-family: Verdana;
                font-size: 11px;
                color: #000000;
                margin: 0px;
                text-align: right;                
                font-weight:bolder;
                border-bottom: 1px solid black;
                border-top:1px solid black;    

            }
            .total1{
                font-family: Verdana;
                font-size: 11px;
                color: #000000;
                margin: 0px;
                text-align: right;                
                font-weight:bolder;  
            }
            .dillLink{
                cursor: pointer;
            }
        </style>
    </head>
    <body>
        <form id="frmGRNApprovalReport" name="frmGRNApprovalReport" method="post" action="customerBalance_summeryRpt.php">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td colspan="3"></td>
                </tr>
                <tr>
                    <td width="20%"></td>
                    <td width="60%" height="80" valign="top"><?php include '../../reportHeader.php' ?></td>
                    <td width="20%"></td>
                </tr>

                <tr>
                    <td colspan="3"></td>
                </tr>
            </table>
            <div align="center">
                <div style="background-color:#FFF" ><strong>Customer Balance Details AS AT <?php echo $toDay; ?></strong><strong></strong></div>
                <div class="custname" ><strong>Customer:  <?php echo $cusName; ?> - <?php echo $cusAddress; ?></strong><strong></strong></div>
                <table width="50%">
                    <tr>                    
                        <th width="6%" class="normalfnt"><strong>Currency:</strong></th>
                        <th width="16%" align="left" class="normalfnt"> <?php echo $currencyDesc ?> </th>
                        <?php
                        if ($filterCompany == 0) {
                            ?>
                            <td width="8%" class="normalfnt"><strong>All Groups</strong></td>
                            <?php
                        } else {
                            ?>
                            <td width="49%" class="normalfnt"><strong>Company :</strong><?php echo $filterCompName ?></td>
                            <?php
                        }
                        ?>
                    </tr>
                </table>
                <table class="maintable">
                    <tr class="maintableHeader">
                        <td>Job No</td>
                        <td>Document No</td>
                        <td>Date</td>
                        <td>Amount</td>
                        <td>To be Paid</td>
                    </tr>
                    <tr class="subHed"><td colspan="5">Credit Notes</td></tr>
                    <?php
                    if($filterCompany==0) $ware='';
                    else $ware=" AND  fin_customer_creditnote_header.intCompanyId=$filterCompany ";
					
					$sql = "SELECT
					fin_customer_creditnote_header.strReferenceNo,
					CRN.strReferenceNo,
					fin_customer_creditnote_header.intCustomerId,
					fin_customer_creditnote_header.strInvoiceNo,
					fin_customer_creditnote_header.dtmDate,
					fin_customer_creditnote_header.dblRate AS hRate,
					fin_customer_creditnote_header.intCurrencyId,
					CRN.intItem,
					CRN.intUom,
					CRN.dblQty,
					CRN.dblDiscount,
					mst_financecurrency.intId,
					mst_financecurrency.strCode,
					CRN.dblUnitPrice,
					CRN.dblTaxAmount,
					SUM(((CRN.dblQty*CRN.dblUnitPrice) *(100-CRN.dblDiscount)/100)+ IFNULL(CRN.dblTaxAmount,0)) AS amount,
					(
					SUM(((CRN.dblQty*CRN.dblUnitPrice) *(100-CRN.dblDiscount)/100)+ IFNULL(CRN.dblTaxAmount,0))
					+
					IFNULL ((SELECT
					Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
					FROM fin_customer_receivedpayments_main_details
					Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
					WHERE
					fin_customer_receivedpayments_main_details.strJobNo =  CRN.strReferenceNo AND
					fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'C.Note'
					GROUP BY
					fin_customer_receivedpayments_main_details.strJobNo),0)
					) AS balAmount
					FROM
					fin_customer_creditnote_details AS CRN
					Inner Join fin_customer_creditnote_header ON CRN.strReferenceNo = fin_customer_creditnote_header.strReferenceNo
					Inner Join mst_financecurrency ON fin_customer_creditnote_header.intCurrencyId = mst_financecurrency.intId
					WHERE
					fin_customer_creditnote_header.intCustomerId =  '$custId' AND
					fin_customer_creditnote_header.intDeleteStatus = '0' AND
					fin_customer_creditnote_header.dtmDate <='$toDay' AND
					fin_customer_creditnote_header.intCurrencyId=$currency ".$ware."
					GROUP BY
					CRN.strReferenceNo
					having balAmount<>0
					";
				
		$result = $db->RunQuery($sql);
                $crditAmountTot=0;
                $crditTobeTot=0;
		while($row=mysqli_fetch_array($result))
		{
			$credit = $row['strReferenceNo'];
			$invoice =  $row['strInvoiceNo'];
			$amount = number_format(($row['amount']*(-1)),4,'.','');
			$toBePaid = number_format(($row['balAmount']* (-1)),4,'.','');
			$date = $row['dtDate'];			
		
                ?>
                    <tr class="dillLink">
                        <td class="normalfntMid" onclick=" leadgerDrill('Credit Note','<?php echo $credit ?>')"><?php echo $credit?></td>
                        <td class="normalfntMid" onclick=" leadgerDrill('Sales Invoice','<?php echo $invoice ?>')"><?php echo $invoice?></td>
                        <td class="normalfntMid" onclick=" leadgerDrill('Credit Note','<?php echo $credit ?>')"><?php echo $date?></td>
                        <td class="figurs" onclick=" leadgerDrill('Credit Note','<?php echo $credit ?>')"><?php echo number_format($amount,4)?></td>
                        <td class="figurs" onclick=" leadgerDrill('Credit Note','<?php echo $credit ?>')"><?php echo number_format($toBePaid,4)?></td>
                    </tr>
                    
                <?php
                    $crditAmountTot+=$amount;
                    $crditTobeTot+=$toBePaid;        
                    
                }
                $amountTot+=$crditAmountTot;
                    $toBeTot+=$crditTobeTot;
                ?>
                    <tr>
                        <td class="total1" colspan="3">Total</td>                        
                        <td class="total"><?php echo number_format($crditAmountTot,4)?></td>
                        <td class="total"><?php echo number_format($crditTobeTot,4)?></td>
                    </tr>
                    <tr class="subHed"><td colspan="5">Advance Received</td></tr>
                    <tr class="subHed1"><td colspan="5">0-30 Days</td></tr>
                <?php
                if($filterCompany==0) $ware=' ';
                else $ware=" AND ADV.intCompanyId=$filterCompany ";
                $adv30AmountTot=0;
                $adv30TobeTot=0;
                $sql="	SELECT
				ADV.strReceiptNo,								
				ADV.dtDate,				
				ADV.dblReceivedAmount,				
				(
				ADV.dblReceivedAmount 
				+
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  ADV.strReceiptNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'A.Received' AND
                                fin_customer_receivedpayments_header.dtmDate<='$toDay'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				
				) AS balAmount
				FROM
				fin_customer_advancereceived_header AS ADV				
				WHERE
				ADV.intCustomer =  $custId AND
                                ADV.intStatus = '1' AND
                                ADV.intCurrency=$currency AND 
                                ADV.dtDate<='$toDay' AND
                                DATEDIFF('$toDay',ADV.dtDate) <=30 ".$ware.
				"having balAmount<>0";
                $result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result)){
                    $receipt = $row['strReceiptNo'];
                    $amount = ($row['dblReceivedAmount']*(-1));
                    $toBePaid = number_format(($row['balAmount']*(-1)),4,'.','');
                    $date = $row['dtDate'];		
                ?>
                    <tr onclick=" leadgerDrill('Advance Received','<?php echo $receipt ?>')" class="dillLink">
                        <td class="normalfntMid"><?php echo $receipt?></td>
                        <td class="normalfntMid"><?php echo $receipt?></td>
                        <td class="normalfntMid"><?php echo $date ?></td>
                        <td class="figurs"><?php echo number_format($amount,4)?></td>
                        <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
                    </tr>
                    
                <?php
                    $adv30AmountTot+=$amount;
                    $adv30TobeTot+=$toBePaid;
                    
                    
                }
                $amountTot+=$adv30AmountTot;
                    $toBeTot+=$adv30TobeTot;
                ?>
                    <tr>
                        <td class="total1" colspan="3">Total</td>                        
                        <td class="total"><?php echo number_format($adv30AmountTot,4)?></td>
                        <td class="total"><?php echo number_format($adv30TobeTot,4)?></td>
                    </tr>
                    <tr class="subHed1"><td colspan="5">31-60 Days</td></tr>
                <?php
                if($filterCompany==0) $ware=' ';
                else $ware=" AND ADV.intCompanyId=$filterCompany ";
                $adv60AmountTot=0;
                $adv60TobeTot=0;
                $sql="	SELECT
				ADV.strReceiptNo,								
				ADV.dtDate,				
				ADV.dblReceivedAmount,				
				(
				ADV.dblReceivedAmount 
				+
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  ADV.strReceiptNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'A.Received' AND
                                fin_customer_receivedpayments_header.dtmDate<='$toDay'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				
				) AS balAmount
				FROM
				fin_customer_advancereceived_header AS ADV				
				WHERE
				ADV.intCustomer =  $custId AND
                                ADV.intStatus = '1' AND
                                ADV.intCurrency=$currency AND
                                ADV.dtDate<='$toDay' AND
                                DATEDIFF('$toDay',ADV.dtDate) >=31 AND
                                DATEDIFF('$toDay',ADV.dtDate) <=60 ".$ware.
				"having balAmount<>0";
                $result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result)){
                    $receipt = $row['strReceiptNo'];
                    $amount = ($row['dblReceivedAmount']*(-1));
                    $toBePaid = number_format(($row['balAmount']*(-1)),4,'.','');
                    $date = $row['dtDate'];		
                ?>
                    <tr onclick=" leadgerDrill('Advance Received','<?php echo $receipt ?>')" class="dillLink">
                        <td class="normalfntMid"><?php echo $receipt?></td>
                        <td class="normalfntMid"><?php echo $receipt?></td>
                        <td class="normalfntMid"><?php echo $date ?></td>
                        <td class="figurs"><?php echo number_format($amount,4)?></td>
                        <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
                    </tr>
                    
                <?php
                    $adv60AmountTot+=$amount;
                    $adv60TobeTot+=$toBePaid;
                    
                    
                }
                $amountTot+=$adv60AmountTot;
                    $toBeTot+=$adv60TobeTot;
                ?>
                    <tr>
                        <td class="total1" colspan="3">Total</td>                        
                        <td class="total"><?php echo number_format($adv60AmountTot,4)?></td>
                        <td class="total"><?php echo number_format($adv60TobeTot,4)?></td>
                    </tr>
                    
                    <tr class="subHed1"><td colspan="5">60-90 Days</td></tr>
                <?php
                if($filterCompany==0) $ware=' ';
                else $ware=" AND ADV.intCompanyId=$filterCompany ";
                $adv90AmountTot=0;
                $adv90TobeTot=0;
                $sql="	SELECT
				ADV.strReceiptNo,								
				ADV.dtDate,				
				ADV.dblReceivedAmount,				
				(
				ADV.dblReceivedAmount 
				+
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  ADV.strReceiptNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'A.Received' AND
                                fin_customer_receivedpayments_header.dtmDate<='$toDay'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				
				) AS balAmount
				FROM
				fin_customer_advancereceived_header AS ADV				
				WHERE
				ADV.intCustomer =  $custId AND
                                ADV.intStatus = '1' AND
                                ADV.intCurrency=$currency AND
                                ADV.dtDate<='$toDay' AND
                                DATEDIFF('$toDay',ADV.dtDate) >=61 AND
                                DATEDIFF('$toDay',ADV.dtDate) <=90 ".$ware.
				"having balAmount<>0";
                $result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result)){
                    $receipt = $row['strReceiptNo'];
                    $amount = ($row['dblReceivedAmount']*(-1));
                    $toBePaid = number_format(($row['balAmount']*(-1)),4,'.','');
                    $date = $row['dtDate'];		
                ?>
                    <tr onclick=" leadgerDrill('Advance Received','<?php echo $receipt ?>')" class="dillLink">
                        <td class="normalfntMid"><?php echo $receipt?></td>
                        <td class="normalfntMid"><?php echo $receipt?></td>
                        <td class="normalfntMid"><?php echo $date ?></td>
                        <td class="figurs"><?php echo number_format($amount,4)?></td>
                        <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
                    </tr>
                    
                <?php
                    $adv90AmountTot+=$amount;
                    $adv90TobeTot+=$toBePaid;
                    
                    
                }
                $amountTot+=$adv90AmountTot;
                    $toBeTot+=$adv90TobeTot;
                ?>
                    <tr>
                        <td class="total1" colspan="3">Total</td>                        
                        <td class="total"><?php echo number_format($adv90AmountTot,4)?></td>
                        <td class="total"><?php echo number_format($adv90TobeTot,4)?></td>
                    </tr>
                    <tr class="subHed1"><td colspan="5">More Than 90 Days</td></tr>
                <?php
                if($filterCompany==0) $ware=' ';
                else $ware=" AND ADV.intCompanyId=$filterCompany ";
                $adv91AmountTot=0;
                $adv91TobeTot=0;
                $sql="	SELECT
				ADV.strReceiptNo,								
				ADV.dtDate,				
				ADV.dblReceivedAmount,				
				(
				ADV.dblReceivedAmount 
				+
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  ADV.strReceiptNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'A.Received' AND
                                fin_customer_receivedpayments_header.dtmDate<='$toDay'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				
				) AS balAmount
				FROM
				fin_customer_advancereceived_header AS ADV				
				WHERE
				ADV.intCustomer =  $custId AND
                                ADV.intStatus = '1' AND
                                ADV.intCurrency=$currency AND
                                ADV.dtDate<='$toDay' AND
                                DATEDIFF('$toDay',ADV.dtDate) >=91 ".$ware.
				"having balAmount<>0";
                $result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result)){
                    $receipt = $row['strReceiptNo'];
                    $amount = ($row['dblReceivedAmount']*(-1));
                    $toBePaid = number_format(($row['balAmount']*(-1)),4,'.','');
                    $date = $row['dtDate'];		
                ?>
                    <tr onclick=" leadgerDrill('Advance Received','<?php echo $receipt ?>')" class="dillLink">
                        <td class="normalfntMid"><?php echo $receipt?></td>
                        <td class="normalfntMid"><?php echo $receipt?></td>
                        <td class="normalfntMid"><?php echo $date ?></td>
                        <td class="figurs"><?php echo number_format($amount,4)?></td>
                        <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
                    </tr>
                    
                <?php
                    $adv91AmountTot+=$amount;
                    $adv91TobeTot+=$toBePaid;
                    
                    
                }
                $amountTot+=$adv91AmountTot;
                    $toBeTot+=$adv91TobeTot;
                ?>
                    <tr>
                        <td class="total1" colspan="3">Total</td>                        
                        <td class="total"><?php echo number_format($adv91AmountTot,4)?></td>
                        <td class="total"><?php echo number_format($adv91TobeTot,4)?></td>
                    </tr>
                    
                    <tr class="subHed"><td colspan="5">Invoice / Debit Note Invoice</td></tr>
                    <tr class="subHed1"><td colspan="5">Current</td></tr>
               <?php
               if($filterCompany==0)$wareCom=" ";
               else $wareCom=" AND fin_customer_salesinvoice_header.intCompanyId=$companyId";
               $invCurrAmountTot=0;
               $invCurrTobeTot=0;
               $sql="SELECT fin_customer_salesinvoice_header.strReferenceNo,			
				fin_customer_salesinvoice_header.dtmDate,				
				sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) AS amount,
				(
				sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) 
				-
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  INV.strReferenceNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'S.Invoice' AND
                                fin_customer_receivedpayments_header.dtmDate<='$toDay'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				
				) AS balAmount
				FROM
				fin_customer_salesinvoice_details AS INV
				Inner Join fin_customer_salesinvoice_header ON fin_customer_salesinvoice_header.strReferenceNo = INV.strReferenceNo AND INV.intInvoiceNo = fin_customer_salesinvoice_header.intInvoiceNo AND INV.intAccPeriodId = fin_customer_salesinvoice_header.intAccPeriodId AND INV.intLocationId = fin_customer_salesinvoice_header.intLocationId AND INV.intCompanyId = fin_customer_salesinvoice_header.intCompanyId
				Inner Join mst_customer ON mst_customer.intId = fin_customer_salesinvoice_header.intCustomerId
				INNER JOIN mst_financepaymentsterms AS PT ON fin_customer_salesinvoice_header.intPaymentsTermsId = PT.intId
				WHERE
				fin_customer_salesinvoice_header.intCustomerId = $custId AND
                                fin_customer_salesinvoice_header.intDeleteStatus = '0' AND
                                fin_customer_salesinvoice_header.intCurrencyId = $currency AND
                                fin_customer_salesinvoice_header.dtmDate <='$toDay' AND
                                DATEDIFF('$toDay',fin_customer_salesinvoice_header.dtmDate) < (PT.strName) ".$wareCom.
				" GROUP BY
				fin_customer_salesinvoice_header.strReferenceNo
				having balAmount<>0";
                $result = $db->RunQuery($sql);
                while($row=mysqli_fetch_array($result)){
                    
                    $invoice = $row['strReferenceNo'];
                    $amount = number_format($row['amount'],4,'.','');
                    $toBePaid = number_format($row['balAmount'],4,'.','');
                    $date = $row['dtmDate'];
               ?>
                    <tr onclick=" leadgerDrill('Sales Invoice','<?php echo $invoice ?>')" class="dillLink">
                        <td class="normalfntMid"><?php echo $invoice?></td>
                        <td class="normalfntMid"><?php echo $invoice?></td>
                        <td class="normalfntMid"><?php echo $date ?></td>
                        <td class="figurs"><?php echo number_format($amount,4)?></td>
                        <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
                    </tr>
                    
                <?php
                    $invCurrAmountTot+=$amount;
                    $invCurrTobeTot+=$toBePaid;
                    
                    
                }
                //$amountTot+=$invCurrAmountTot;
//                    $toBeTot+=$invCurrTobeTot;
                ?>
                    <!--<tr>
                        <td class="total1" colspan="3">Total</td>                        
                        <td class="total"><?php echo number_format($invCurrAmountTot,4)?></td>
                        <td class="total"><?php echo number_format($invCurrTobeTot,4)?></td>
                    </tr>-->
                 
                  <?php
               if($filterCompany==0)$wareCom=" ";
               else $wareCom=" AND fin_customer_debitnoteinvoice_header.intCompanyId=$companyId";
               //$invCurrAmountTot=0;
               //$invCurrTobeTot=0;
               $sql="SELECT fin_customer_debitnoteinvoice_header.strReferenceNo,
			   fin_customer_debitnoteinvoice_header.strInvoiceNo,			
				fin_customer_debitnoteinvoice_header.dtmDate,				
				sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) AS amount,
				(
				sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) 
				-
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  INV.strReferenceNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'D.Invoice' AND
                                fin_customer_receivedpayments_header.dtmDate<='$toDay'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				
				) AS balAmount
				FROM
				fin_customer_debitnoteinvoice_details AS INV
				Inner Join fin_customer_debitnoteinvoice_header ON fin_customer_debitnoteinvoice_header.strReferenceNo = INV.strReferenceNo AND INV.intInvoiceNo = fin_customer_debitnoteinvoice_header.intInvoiceNo AND INV.intAccPeriodId = fin_customer_debitnoteinvoice_header.intAccPeriodId AND INV.intLocationId = fin_customer_debitnoteinvoice_header.intLocationId AND INV.intCompanyId = fin_customer_debitnoteinvoice_header.intCompanyId
				Inner Join mst_customer ON mst_customer.intId = fin_customer_debitnoteinvoice_header.intCustomerId
				INNER JOIN mst_financepaymentsterms AS PT ON fin_customer_debitnoteinvoice_header.intPaymentsTermsId = PT.intId
				WHERE
				fin_customer_debitnoteinvoice_header.intCustomerId = $custId AND
                                fin_customer_debitnoteinvoice_header.intDeleteStatus = '0' AND
                                fin_customer_debitnoteinvoice_header.intCurrencyId = $currency AND
                                fin_customer_debitnoteinvoice_header.dtmDate <='$toDay' AND
                                DATEDIFF('$toDay',fin_customer_debitnoteinvoice_header.dtmDate) < (PT.strName) ".$wareCom.
				" GROUP BY
				fin_customer_debitnoteinvoice_header.strReferenceNo
				having balAmount<>0";
                $result = $db->RunQuery($sql);
                while($row=mysqli_fetch_array($result)){
                    
					$salesInvoice = $row['strInvoiceNo'];
                    $invoice = $row['strReferenceNo'];
                    $amount = number_format($row['amount'],4,'.','');
                    $toBePaid = number_format($row['balAmount'],4,'.','');
                    $date = $row['dtmDate'];
               ?>
                    <tr >
                    <td class="normalfntMid dillLink" onclick=" leadgerDrill('Debit Note Invoice','<?php echo $invoice ?>')"><?php echo $invoice?></td>
                    <td class="normalfntMid dillLink" onclick=" leadgerDrill('Sales Invoice','<?php echo $salesInvoice ?>')"><?php echo $salesInvoice?></td>
                    <td class="normalfntMid dillLink" onclick=" leadgerDrill('Debit Note Invoice','<?php echo $invoice ?>')"><?php echo $date ?></td>
                    <td class="figurs dillLink" onclick=" leadgerDrill('Debit Note Invoice','<?php echo $invoice ?>')"><?php echo number_format($amount,4)?></td>
                    <td class="figurs dillLink" onclick=" leadgerDrill('Debit Note Invoice','<?php echo $invoice ?>')"><?php echo number_format($toBePaid,4)?></td>
                    </tr>
                    
                <?php
                    $invCurrAmountTot+=$amount;
                    $invCurrTobeTot+=$toBePaid;
                    
                    
                }
                $amountTot+=$invCurrAmountTot;
                    $toBeTot+=$invCurrTobeTot;
                ?>
                    <tr>
                        <td class="total1" colspan="3">Total</td>                        
                        <td class="total"><?php echo number_format($invCurrAmountTot,4)?></td>
                        <td class="total"><?php echo number_format($invCurrTobeTot,4)?></td>
                    </tr>   
                 
                <tr class="subHed1"><td colspan="5">0 - 30</td></tr>
                <!-- Invoice -->
                <?php
                if($filterCompany==0)$wareCom=" ";
                else $wareCom=" AND fin_customer_salesinvoice_header.intCompanyId=$filterCompany ";
                $inv30AmountTot=0;
                $inv30TobeTot=0;
                $sql="SELECT fin_customer_salesinvoice_header.strReferenceNo,			
				fin_customer_salesinvoice_header.dtmDate,				
				sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) AS amount,
				(
				sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) 
				-
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  INV.strReferenceNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'S.Invoice' AND
                                fin_customer_receivedpayments_header.dtmDate<='$toDay'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				
				) AS balAmount
				FROM
				fin_customer_salesinvoice_details AS INV
				Inner Join fin_customer_salesinvoice_header ON fin_customer_salesinvoice_header.strReferenceNo = INV.strReferenceNo AND INV.intInvoiceNo = fin_customer_salesinvoice_header.intInvoiceNo AND INV.intAccPeriodId = fin_customer_salesinvoice_header.intAccPeriodId AND INV.intLocationId = fin_customer_salesinvoice_header.intLocationId AND INV.intCompanyId = fin_customer_salesinvoice_header.intCompanyId
				Inner Join mst_customer ON mst_customer.intId = fin_customer_salesinvoice_header.intCustomerId
				INNER JOIN mst_financepaymentsterms AS PT ON fin_customer_salesinvoice_header.intPaymentsTermsId = PT.intId
				WHERE
				fin_customer_salesinvoice_header.intCustomerId = $custId AND
                                    fin_customer_salesinvoice_header.intDeleteStatus = '0' AND
                                    fin_customer_salesinvoice_header.intCurrencyId = $currency AND
                                    fin_customer_salesinvoice_header.dtmDate <='$toDay' AND
                                    DATEDIFF('$toDay',fin_customer_salesinvoice_header.dtmDate) >= (0+ PT.strName) AND
                                    DATEDIFF('$toDay',fin_customer_salesinvoice_header.dtmDate) <=(30+ PT.strName)
                                   ".$wareCom.
				" GROUP BY
				fin_customer_salesinvoice_header.strReferenceNo
				having balAmount<>0";
                $result = $db->RunQuery($sql);
                while($row=mysqli_fetch_array($result)){
                    
                    $invoice = $row['strReferenceNo'];
                    $amount = number_format($row['amount'],4,'.','');
                    $toBePaid = number_format($row['balAmount'],4,'.','');
                    $date = $row['dtmDate'];
               ?>
                    <tr onclick=" leadgerDrill('Sales Invoice','<?php echo $invoice ?>')" class="dillLink">
                        <td class="normalfntMid"><?php echo $invoice?></td>
                        <td class="normalfntMid"><?php echo $invoice?></td>
                        <td class="normalfntMid"><?php echo $date ?></td>
                        <td class="figurs"><?php echo number_format($amount,4)?></td>
                        <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
                    </tr>
                    
                <?php
                    $inv30AmountTot+=$amount;
                    $inv30TobeTot+=$toBePaid;
                }
                ?>
                <!-- Debit Note Invoice -->
                <?php
                if($filterCompany==0)$wareCom=" ";
                else $wareCom=" AND fin_customer_debitnoteinvoice_header.intCompanyId=$filterCompany ";
                //$inv30AmountTot=0;
                //$inv30TobeTot=0;
                $sql="SELECT fin_customer_debitnoteinvoice_header.strReferenceNo,
				fin_customer_debitnoteinvoice_header.strInvoiceNo,			
				fin_customer_debitnoteinvoice_header.dtmDate,				
				sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) AS amount,
				(
				sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) 
				-
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  INV.strReferenceNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'D.Invoice' AND
                                fin_customer_receivedpayments_header.dtmDate<='$toDay'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				
				) AS balAmount
				FROM
				fin_customer_debitnoteinvoice_details AS INV
				Inner Join fin_customer_debitnoteinvoice_header ON fin_customer_debitnoteinvoice_header.strReferenceNo = INV.strReferenceNo AND INV.intInvoiceNo = fin_customer_debitnoteinvoice_header.intInvoiceNo AND INV.intAccPeriodId = fin_customer_debitnoteinvoice_header.intAccPeriodId AND INV.intLocationId = fin_customer_debitnoteinvoice_header.intLocationId AND INV.intCompanyId = fin_customer_debitnoteinvoice_header.intCompanyId
				Inner Join mst_customer ON mst_customer.intId = fin_customer_debitnoteinvoice_header.intCustomerId
				INNER JOIN mst_financepaymentsterms AS PT ON fin_customer_debitnoteinvoice_header.intPaymentsTermsId = PT.intId
				WHERE
				fin_customer_debitnoteinvoice_header.intCustomerId = $custId AND
                                    fin_customer_debitnoteinvoice_header.intDeleteStatus = '0' AND
                                    fin_customer_debitnoteinvoice_header.intCurrencyId = $currency AND
                                    fin_customer_debitnoteinvoice_header.dtmDate <='$toDay' AND
                                    DATEDIFF('$toDay',fin_customer_debitnoteinvoice_header.dtmDate) >= (0+ PT.strName) AND
                                    DATEDIFF('$toDay',fin_customer_debitnoteinvoice_header.dtmDate) <=(30+ PT.strName)
                                   ".$wareCom.
				" GROUP BY
				fin_customer_debitnoteinvoice_header.strReferenceNo
				having balAmount<>0";
                $result = $db->RunQuery($sql);
                while($row=mysqli_fetch_array($result)){
                    
					$salesInvoice = $row['strInvoiceNo'];
                    $invoice = $row['strReferenceNo'];
                    $amount = number_format($row['amount'],4,'.','');
                    $toBePaid = number_format($row['balAmount'],4,'.','');
                    $date = $row['dtmDate'];
               ?>
                    <tr class="dillLink">
                    <td class="normalfntMid" onclick=" leadgerDrill('Debit Note Invoice','<?php echo $invoice ?>')"><?php echo $invoice?></td>
                    <td class="normalfntMid" onclick=" leadgerDrill('Sales Invoice','<?php echo $salesInvoice ?>')"><?php echo $salesInvoice?></td>
                    <td class="normalfntMid" onclick=" leadgerDrill('Debit Note Invoice','<?php echo $invoice ?>')"><?php echo $date ?></td>
                    <td class="figurs" onclick=" leadgerDrill('Debit Note Invoice','<?php echo $invoice ?>')"><?php echo number_format($amount,4)?></td>
                    <td class="figurs" onclick=" leadgerDrill('Debit Note Invoice','<?php echo $invoice ?>')"><?php echo number_format($toBePaid,4)?></td>
                    </tr>
                    
                <?php
                    $inv30AmountTot+=$amount;
                    $inv30TobeTot+=$toBePaid;
                }
                ?>
                <!-- Bank Deposit -->                
                <?php
                if($filterCompany==0)$wareCom=" ";
                else $wareCom=" AND BND.intCompanyId=$filterCompany ";
                $sql = "SELECT
				BND.strDepositNo,
				BND.dtDate,
                                fin_bankdeposit_details.dblAmmount,
				(
				fin_bankdeposit_details.dblAmmount 
				+
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  BND.strDepositNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'B.Deposit' AND
                                fin_customer_receivedpayments_header.dtmDate<='$toDay'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				
				) AS balAmount
				FROM
				fin_bankdeposit_header BND
				Inner Join fin_bankdeposit_details ON fin_bankdeposit_details.strDepositNo = BND.strDepositNo				
				Inner Join mst_financechartofaccounts ON fin_bankdeposit_details.intAccount = mst_financechartofaccounts.intId
				WHERE
				fin_bankdeposit_details.intRecvFrom =  '$custId' AND
				mst_financechartofaccounts.intFinancialTypeId =  '10' AND
				BND.intStatus = '1' AND
                                BND.intCurrency=$currency AND
                                BND.dtDate <='$toDay' AND
                                DATEDIFF('$toDay',BND.dtDate) >=0 AND
                                DATEDIFF('$toDay',BND.dtDate) <=30".$wareCom.
				"having balAmount<>0";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$deposit = $row['strDepositNo'];
			$amount = number_format(($row['dblAmmount']*(-1)),4,'.','');
			$toBePaid = number_format(($row['balAmount']*(-1)),4,'.','');
			$date = substr($row['dtDate'],0,10);
                ?>
                
                    <tr onclick=" leadgerDrill('Bank Deposit','<?php echo $deposit ?>')" class="dillLink">
                        <td class="normalfntMid"><?php echo $deposit?></td>
                        <td class="normalfntMid"><?php echo $deposit?></td>
                        <td class="normalfntMid"><?php echo $date ?></td>
                        <td class="figurs"><?php echo number_format($amount,4)?></td>
                        <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
                    </tr>
                    
                <?php
                    $inv30AmountTot+=$amount;
                    $inv30TobeTot+=$toBePaid;
                }
                ?>
                <!-- Bank Payments -->                                
                <?php
                if($filterCompany==0)$wareCom=" ";
                else $wareCom=" AND BNP.intCompanyId=$filterCompany ";
                $sql = "SELECT
				BNP.strBankPaymentNo,
				BNP.dtDate,
                                BNP.dblReceivedAmount,
								fin_bankpayment_details.dblAmmount,
				(
				fin_bankpayment_details.dblAmmount 
				-
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  BNP.strBankPaymentNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'B.Payment' AND
                                fin_customer_receivedpayments_header.dtmDate<='$toDay'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				
				) AS balAmount
				FROM
				fin_bankpayment_header BNP
				Inner Join fin_bankpayment_details ON fin_bankpayment_details.strBankPaymentNo = BNP.strBankPaymentNo				
				Inner Join mst_financechartofaccounts ON fin_bankpayment_details.intAccountId = mst_financechartofaccounts.intId
				WHERE
				fin_bankpayment_details.intPayTo =  '$custId' AND
				mst_financechartofaccounts.intFinancialTypeId =  '10' AND
				BNP.intStatus = '1' AND
                                BNP.intCurrency=$currency AND
                                BNP.dtDate <='$toDay' AND
                                DATEDIFF('$toDay',BNP.dtDate)>= 0 AND
                                DATEDIFF('$toDay',BNP.dtDate)<= 30 ".$wareCom. "
				having balAmount<>0";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$payment = $row['strBankPaymentNo'];
			$amount = number_format($row['dblAmmount'],4,'.','');
			$toBePaid = number_format($row['balAmount'],4,'.','');
			$date = substr($row['dtDate'],0,10);
                ?>
                
                    <tr onclick=" leadgerDrill('Bank Payment','<?php echo $payment ?>')" class="dillLink">
                        <td class="normalfntMid"><?php echo $payment?></td>
                        <td class="normalfntMid"><?php echo $payment?></td>
                        <td class="normalfntMid"><?php echo $date ?></td>
                        <td class="figurs"><?php echo number_format($amount,4)?></td>
                        <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
                    </tr>
                    
                <?php
                    $inv30AmountTot+=$amount;
                    $inv30TobeTot+=$toBePaid;
                }
                ?>
                <!-- Petty Cash -->                                
                <?php
                if($filterCompany==0)$wareCom=" ";
                else $wareCom=" AND PTC.intCompanyId=$filterCompany ";
                 $sql = "SELECT
				PTC.strPettyCashNo,
				PTC.dtDate,
                                PTC.dblReceivedAmount,
								fin_bankpettycash_details.dblAmmount,
				(
				fin_bankpettycash_details.dblAmmount 
				-
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  PTC.strPettyCashNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'Petty Cash' AND
                                fin_customer_receivedpayments_header.dtmDate<='$toDay'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				
				) AS balAmount
				FROM
				fin_bankpettycash_header AS PTC
				Inner Join fin_bankpettycash_details ON fin_bankpettycash_details.strPettyCashNo = PTC.strPettyCashNo
				Inner Join mst_financechartofaccounts ON fin_bankpettycash_details.intAccountId = mst_financechartofaccounts.intId
				WHERE
				mst_financechartofaccounts.intFinancialTypeId =  '10'
				AND
				fin_bankpettycash_details.intPayTo =  '$custId' AND
				PTC.intStatus = '1' AND
                                PTC.intCurrency=$currency AND
                                PTC.dtDate <='$toDay' AND
                                DATEDIFF('$toDay',PTC.dtDate)>= 0 AND
                                DATEDIFF('$toDay',PTC.dtDate)<= 30".$wareCom.
				"having balAmount<>0";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$pettyCash = $row['strPettyCashNo'];
			$amount = number_format($row['dblAmmount'],4,'.','');
			$toBePaid = number_format($row['balAmount'],4,'.','');
			$date = substr($row['dtDate'],0,10);
                ?>
                
                    <tr onclick=" leadgerDrill('Petty Cash','<?php echo $pettyCash ?>')" class="dillLink">
                        <td class="normalfntMid"><?php echo $pettyCash?></td>
                        <td class="normalfntMid"><?php echo $pettyCash?></td>
                        <td class="normalfntMid"><?php echo $date ?></td>
                        <td class="figurs"><?php echo number_format($amount,4)?></td>
                        <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
                    </tr>
                    
                <?php
                    $inv30AmountTot+=$amount;
                    $inv30TobeTot+=$toBePaid;
                }
                ?>
                <!-- Jurnel Entry -->                                
                <?php
                if($filterCompany==0)$wareCom=" ";
                else $wareCom=" AND JH.intCompanyId=$filterCompany ";
                $sql = "SELECT
                            JH.strReferenceNo,
                            JH.dtmDate, 
                            JD.dblDebitAmount,
                            JD.dbCreditAmount,
                            (
                            JD.dblDebitAmount - JD.dbCreditAmount -
                            IFNULL((SELECT
							Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
							FROM
							fin_customer_receivedpayments_main_details
							Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
							Inner Join fin_accountant_journal_entry_details ON fin_customer_receivedpayments_main_details.strDocNo = fin_accountant_journal_entry_details.strReferenceNo
							WHERE
							fin_customer_receivedpayments_main_details.strJobNo =  'JH.strReferenceNo' AND
							fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND
							fin_customer_receivedpayments_main_details.strDocType =  'JN' AND
							fin_customer_receivedpayments_header.intCustomerId =  '$custId' AND
							fin_customer_receivedpayments_header.dtmDate <=  '$toDay' AND
							fin_accountant_journal_entry_details.strPersonType =  'cus'
							GROUP BY
							fin_customer_receivedpayments_main_details.strJobNo),0)
                            ) AS balAmount
                            FROM
                            fin_accountant_journal_entry_header AS JH
                            INNER JOIN fin_accountant_journal_entry_details AS JD ON JH.strReferenceNo = JD.strReferenceNo                            
                            INNER JOIN mst_financechartofaccounts ON JD.intChartOfAccountId = mst_financechartofaccounts.intId
                            WHERE
                            JD.strPersonType = 'cus' AND
                            JD.intNameId = $custId AND
                            JH.intDeleteStatus = 0 AND
                            mst_financechartofaccounts.intFinancialTypeId = 10 AND
                            JH.intCurrencyId=$currency AND
                            JH.dtmDate <='$toDay' AND
                            DATEDIFF('$toDay', JH.dtmDate)>= 0 AND
                            DATEDIFF('$toDay', JH.dtmDate)<= 30".$wareCom.
                            "HAVING
                            balAmount <> 0";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$payment = $row['strReferenceNo'];
			$amount = number_format(($row['dblDebitAmount'] - $row['dbCreditAmount']),4,'.','');
                        
                        //if($row['dbCreditAmount']!=0){
//                            $toBePaid = number_format(($row['balAmount']*(-1)),4,'.','');
//                        }
//                        else{
                            $toBePaid = number_format(($row['balAmount']),4,'.','');
                        //}                       
			                        
			$date = substr($row['dtmDate'],0,10);
                ?>
                
                    <tr onclick=" leadgerDrill('Journal Entry','<?php echo $payment ?>')" class="dillLink">
                        <td class="normalfntMid"><?php echo $payment?></td>
                        <td class="normalfntMid"><?php echo $payment?></td>
                        <td class="normalfntMid"><?php echo $date ?></td>
                        <td class="figurs"><?php echo number_format($amount,4)?></td>
                        <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
                    </tr>
                    
                <?php
                    $inv30AmountTot+=$amount;
                    $inv30TobeTot+=$toBePaid;
                    
                    
                }
                $amountTot+=$inv30AmountTot;
                    $toBeTot+=$inv30TobeTot;
                ?>
                <tr>
                    <td class="total1" colspan="3">Total</td>                        
                    <td class="total"><?php echo number_format($inv30AmountTot,4)?></td>
                    <td class="total"><?php echo number_format($inv30TobeTot,4)?></td>
                </tr>
                <tr class="subHed1"><td colspan="5">31 - 60</td></tr>
                    <!-- Invoice -->
                <?php
                if($filterCompany==0)$wareCom=" ";
                else $wareCom=" AND fin_customer_salesinvoice_header.intCompanyId=$filterCompany ";
                $inv60AmountTot=0;
                $inv60TobeTot=0;
                $sql="SELECT fin_customer_salesinvoice_header.strReferenceNo,			
				fin_customer_salesinvoice_header.dtmDate,				
				sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) AS amount,
				(
				sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) 
				-
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  INV.strReferenceNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'S.Invoice' AND
                                fin_customer_receivedpayments_header.dtmDate<='$toDay'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				
				) AS balAmount
				FROM
				fin_customer_salesinvoice_details AS INV
				Inner Join fin_customer_salesinvoice_header ON fin_customer_salesinvoice_header.strReferenceNo = INV.strReferenceNo AND INV.intInvoiceNo = fin_customer_salesinvoice_header.intInvoiceNo AND INV.intAccPeriodId = fin_customer_salesinvoice_header.intAccPeriodId AND INV.intLocationId = fin_customer_salesinvoice_header.intLocationId AND INV.intCompanyId = fin_customer_salesinvoice_header.intCompanyId
				Inner Join mst_customer ON mst_customer.intId = fin_customer_salesinvoice_header.intCustomerId
				INNER JOIN mst_financepaymentsterms AS PT ON fin_customer_salesinvoice_header.intPaymentsTermsId = PT.intId
				WHERE
				fin_customer_salesinvoice_header.intCustomerId = $custId AND
                                    fin_customer_salesinvoice_header.intDeleteStatus = '0' AND
                                    fin_customer_salesinvoice_header.intCurrencyId = $currency AND
                                    fin_customer_salesinvoice_header.dtmDate <='$toDay' AND
                                    DATEDIFF('$toDay',fin_customer_salesinvoice_header.dtmDate) >= (31+ PT.strName) AND
                                    DATEDIFF('$toDay',fin_customer_salesinvoice_header.dtmDate) <=(60+ PT.strName)
                                   ".$wareCom.
				" GROUP BY
				fin_customer_salesinvoice_header.strReferenceNo
				having balAmount<>0";
                $result = $db->RunQuery($sql);
                while($row=mysqli_fetch_array($result)){
                    
                    $invoice = $row['strReferenceNo'];
                    $amount = number_format($row['amount'],4,'.','');
                    $toBePaid = number_format($row['balAmount'],4,'.','');
                    $date = $row['dtmDate'];
               ?>
                    <tr onclick=" leadgerDrill('Sales Invoice','<?php echo $invoice ?>')" class="dillLink">
                        <td class="normalfntMid"><?php echo $invoice?></td>
                        <td class="normalfntMid"><?php echo $invoice?></td>
                        <td class="normalfntMid"><?php echo $date ?></td>
                        <td class="figurs"><?php echo number_format($amount,4)?></td>
                        <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
                    </tr>
                    
                <?php
                    $inv60AmountTot+=$amount;
                    $inv60TobeTot+=$toBePaid;
                }
                ?>
                
                <!-- Debit Note Invoice -->
                <?php
                if($filterCompany==0)$wareCom=" ";
                else $wareCom=" AND fin_customer_debitnoteinvoice_header.intCompanyId=$filterCompany ";
                //$inv60AmountTot=0;
                //$inv60TobeTot=0;
                $sql="SELECT fin_customer_debitnoteinvoice_header.strReferenceNo,
				fin_customer_debitnoteinvoice_header.strInvoiceNo,			
				fin_customer_debitnoteinvoice_header.dtmDate,				
				sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) AS amount,
				(
				sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) 
				-
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  INV.strReferenceNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'D.Invoice' AND
                                fin_customer_receivedpayments_header.dtmDate<='$toDay'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				
				) AS balAmount
				FROM
				fin_customer_debitnoteinvoice_details AS INV
				Inner Join fin_customer_debitnoteinvoice_header ON fin_customer_debitnoteinvoice_header.strReferenceNo = INV.strReferenceNo AND INV.intInvoiceNo = fin_customer_debitnoteinvoice_header.intInvoiceNo AND INV.intAccPeriodId = fin_customer_debitnoteinvoice_header.intAccPeriodId AND INV.intLocationId = fin_customer_debitnoteinvoice_header.intLocationId AND INV.intCompanyId = fin_customer_debitnoteinvoice_header.intCompanyId
				Inner Join mst_customer ON mst_customer.intId = fin_customer_debitnoteinvoice_header.intCustomerId
				INNER JOIN mst_financepaymentsterms AS PT ON fin_customer_debitnoteinvoice_header.intPaymentsTermsId = PT.intId
				WHERE
				fin_customer_debitnoteinvoice_header.intCustomerId = $custId AND
                                    fin_customer_debitnoteinvoice_header.intDeleteStatus = '0' AND
                                    fin_customer_debitnoteinvoice_header.intCurrencyId = $currency AND
                                    fin_customer_debitnoteinvoice_header.dtmDate <='$toDay' AND
                                    DATEDIFF('$toDay',fin_customer_debitnoteinvoice_header.dtmDate) >= (31+ PT.strName) AND
                                    DATEDIFF('$toDay',fin_customer_debitnoteinvoice_header.dtmDate) <=(60+ PT.strName)
                                   ".$wareCom.
				" GROUP BY
				fin_customer_debitnoteinvoice_header.strReferenceNo
				having balAmount<>0";
                $result = $db->RunQuery($sql);
                while($row=mysqli_fetch_array($result)){
                    
					$salesInvoice = $row['strInvoiceNo'];
                    $invoice = $row['strReferenceNo'];
                    $amount = number_format($row['amount'],4,'.','');
                    $toBePaid = number_format($row['balAmount'],4,'.','');
                    $date = $row['dtmDate'];
               ?>
            <tr class="dillLink">
                <td class="normalfntMid" onclick=" leadgerDrill('Debit Note Invoice','<?php echo $invoice ?>')"><?php echo $invoice?></td>
                <td class="normalfntMid" onclick=" leadgerDrill('Sales Invoice','<?php echo $salesInvoice ?>')"><?php echo $salesInvoice?></td>
                <td class="normalfntMid" onclick=" leadgerDrill('Debit Note Invoice','<?php echo $invoice ?>')"><?php echo $date ?></td>
                <td class="figurs" onclick=" leadgerDrill('Debit Note Invoice','<?php echo $invoice ?>')"><?php echo number_format($amount,4)?></td>
                <td class="figurs" onclick=" leadgerDrill('Debit Note Invoice','<?php echo $invoice ?>')"><?php echo number_format($toBePaid,4)?></td>
            </tr>
                    
                <?php
                    $inv60AmountTot+=$amount;
                    $inv60TobeTot+=$toBePaid;
                }
                ?>
                
                <!-- Bank Deposit -->                
                <?php
                if($filterCompany==0)$wareCom=" ";
                else $wareCom=" AND BND.intCompanyId=$filterCompany ";
                $sql = "SELECT
				BND.strDepositNo,
				BND.dtDate,
                                fin_bankdeposit_details.dblAmmount,
				(
				fin_bankdeposit_details.dblAmmount 
				+
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  BND.strDepositNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'B.Deposit' AND
                                fin_customer_receivedpayments_header.dtmDate<='$toDay'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				
				) AS balAmount
				FROM
				fin_bankdeposit_header BND
				Inner Join fin_bankdeposit_details ON fin_bankdeposit_details.strDepositNo = BND.strDepositNo				
				Inner Join mst_financechartofaccounts ON fin_bankdeposit_details.intAccount = mst_financechartofaccounts.intId
				WHERE
				fin_bankdeposit_details.intRecvFrom =  '$custId' AND
				mst_financechartofaccounts.intFinancialTypeId =  '10' AND
				BND.intStatus = '1' AND
                                BND.intCurrency=$currency AND
                                BND.dtDate <='$toDay' AND
                                DATEDIFF('$toDay',BND.dtDate) >=31 AND
                                DATEDIFF('$toDay',BND.dtDate) <=60".$wareCom.
				"having balAmount<>0";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$deposit = $row['strDepositNo'];
			$amount = number_format(($row['dblAmmount']*(-1)),4,'.','');
			$toBePaid = number_format(($row['balAmount']*(-1)),4,'.','');
			$date = substr($row['dtDate'],0,10);
                ?>
                
                    <tr onclick=" leadgerDrill('Bank Deposit','<?php echo $deposit ?>')" class="dillLink">
                        <td class="normalfntMid"><?php echo $deposit?></td>
                        <td class="normalfntMid"><?php echo $deposit?></td>
                        <td class="normalfntMid"><?php echo $date ?></td>
                        <td class="figurs"><?php echo number_format($amount,4)?></td>
                        <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
                    </tr>
                    
                <?php
                    $inv60AmountTot+=$amount;
                    $inv60TobeTot+=$toBePaid;
                }
                ?>
                <!-- Bank Payments -->                                
                <?php
                if($filterCompany==0)$wareCom=" ";
                else $wareCom=" AND BNP.intCompanyId=$filterCompany ";
                $sql = "SELECT
				BNP.strBankPaymentNo,
				BNP.dtDate,
                                BNP.dblReceivedAmount,
								fin_bankpayment_details.dblAmmount,
				(
				fin_bankpayment_details.dblAmmount
				-
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  BNP.strBankPaymentNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'B.Payment' AND
                                fin_customer_receivedpayments_header.dtmDate<='$toDay'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				
				) AS balAmount
				FROM
				fin_bankpayment_header BNP
				Inner Join fin_bankpayment_details ON fin_bankpayment_details.strBankPaymentNo = BNP.strBankPaymentNo				
				Inner Join mst_financechartofaccounts ON fin_bankpayment_details.intAccountId = mst_financechartofaccounts.intId
				WHERE
				fin_bankpayment_details.intPayTo =  '$custId' AND
				mst_financechartofaccounts.intFinancialTypeId =  '10' AND
				BNP.intStatus = '1' AND
                                BNP.intCurrency=$currency AND
                BNP.dtDate <='$toDay' AND
                                DATEDIFF('$toDay',BNP.dtDate)>= 31 AND
                                DATEDIFF('$toDay',BNP.dtDate)<= 60 ".$wareCom. "
				having balAmount<>0";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$payment = $row['strBankPaymentNo'];
			$amount = number_format($row['dblAmmount'],4,'.','');
			$toBePaid = number_format($row['balAmount'],4,'.','');
			$date = substr($row['dtDate'],0,10);
                ?>
                
                    <tr onclick=" leadgerDrill('Bank Payment','<?php echo $payment ?>')" class="dillLink">
                        <td class="normalfntMid"><?php echo $payment?></td>
                        <td class="normalfntMid"><?php echo $payment?></td>
                        <td class="normalfntMid"><?php echo $date ?></td>
                        <td class="figurs"><?php echo number_format($amount,4)?></td>
                        <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
                    </tr>
                    
                <?php
                    $inv60AmountTot+=$amount;
                    $inv60TobeTot+=$toBePaid;
                }
                ?>
                <!-- Petty Cash -->                                
                <?php
                if($filterCompany==0)$wareCom=" ";
                else $wareCom=" AND PTC.intCompanyId=$filterCompany ";
                 $sql = "SELECT
				PTC.strPettyCashNo,
				PTC.dtDate,
                                PTC.dblReceivedAmount,
								fin_bankpettycash_details.dblAmmount,
				(
				fin_bankpettycash_details.dblAmmount
				-
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  PTC.strPettyCashNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'Petty Cash' AND
                                fin_customer_receivedpayments_header.dtmDate<='$toDay'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				
				) AS balAmount
				FROM
				fin_bankpettycash_header AS PTC
				Inner Join fin_bankpettycash_details ON fin_bankpettycash_details.strPettyCashNo = PTC.strPettyCashNo
				Inner Join mst_financechartofaccounts ON fin_bankpettycash_details.intAccountId = mst_financechartofaccounts.intId
				WHERE
				mst_financechartofaccounts.intFinancialTypeId =  '10'
				AND
				fin_bankpettycash_details.intPayTo =  '$custId' AND
				PTC.intStatus = '1' AND
                                PTC.intCurrency=$currency AND
                PTC.dtDate <='$toDay' AND
                                DATEDIFF('$toDay',PTC.dtDate)>= 31 AND
                                DATEDIFF('$toDay',PTC.dtDate)<= 60".$wareCom.
				"having balAmount<>0";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$pettyCash = $row['strPettyCashNo'];
			$amount = number_format($row['dblAmmount'],4,'.','');
			$toBePaid = number_format($row['balAmount'],4,'.','');
			$date = substr($row['dtDate'],0,10);
                ?>
                
                    <tr onclick=" leadgerDrill('Petty Cash','<?php echo $pettyCash ?>')" class="dillLink">
                        <td class="normalfntMid"><?php echo $pettyCash?></td>
                        <td class="normalfntMid"><?php echo $pettyCash?></td>
                        <td class="normalfntMid"><?php echo $date ?></td>
                        <td class="figurs"><?php echo number_format($amount,4)?></td>
                        <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
                    </tr>
                    
                <?php
                    $inv60AmountTot+=$amount;
                    $inv60TobeTot+=$toBePaid;
                }
                ?>
                <!-- Jurnel Entry -->                                
                <?php
                if($filterCompany==0)$wareCom=" ";
                else $wareCom=" AND JH.intCompanyId=$filterCompany ";
                $sql = "SELECT
                            JH.strReferenceNo,
                            JH.dtmDate, 
                            JD.dblDebitAmount,
                            JD.dbCreditAmount,
                            (
                            JD.dblDebitAmount - JD.dbCreditAmount -
                            IFNULL((SELECT
							Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
							FROM
							fin_customer_receivedpayments_main_details
							Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
							Inner Join fin_accountant_journal_entry_details ON fin_customer_receivedpayments_main_details.strDocNo = fin_accountant_journal_entry_details.strReferenceNo
							WHERE
							fin_customer_receivedpayments_main_details.strJobNo =  'JH.strReferenceNo' AND
							fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND
							fin_customer_receivedpayments_main_details.strDocType =  'JN' AND
							fin_customer_receivedpayments_header.intCustomerId =  '$custId' AND
							fin_customer_receivedpayments_header.dtmDate <=  '$toDay' AND
							fin_accountant_journal_entry_details.strPersonType =  'cus'
							GROUP BY
							fin_customer_receivedpayments_main_details.strJobNo),0)
                            ) AS balAmount
                            FROM
                            fin_accountant_journal_entry_header AS JH
                            INNER JOIN fin_accountant_journal_entry_details AS JD ON JH.strReferenceNo = JD.strReferenceNo                            
                            INNER JOIN mst_financechartofaccounts ON JD.intChartOfAccountId = mst_financechartofaccounts.intId
                            WHERE
                            JD.strPersonType = 'cus' AND
                            JD.intNameId = $custId AND
                            JH.intDeleteStatus = 0 AND
                            mst_financechartofaccounts.intFinancialTypeId = 10 AND
                            JH.intCurrencyId=$currency AND
                JH.dtmDate <='$toDay' AND
                            DATEDIFF('$toDay', JH.dtmDate)>= 31 AND
                            DATEDIFF('$toDay', JH.dtmDate)<= 60".$wareCom.
                            "HAVING
                            balAmount <> 0";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$payment = $row['strReferenceNo'];
			$amount = number_format(($row['dblDebitAmount'] - $row['dbCreditAmount']),4,'.','');
                        
                        //if($row['dbCreditAmount']!=0){
//                            $toBePaid = number_format(($row['balAmount']*(-1)),4,'.','');
//                        }
//                        else{
                            $toBePaid = number_format(($row['balAmount']),4,'.','');
                        //}                       
			                        
			$date = substr($row['dtmDate'],0,10);
                ?>
                
                    <tr onclick=" leadgerDrill('Journal Entry','<?php echo $payment ?>')" class="dillLink">
                        <td class="normalfntMid"><?php echo $payment?></td>
                        <td class="normalfntMid"><?php echo $payment?></td>
                        <td class="normalfntMid"><?php echo $date ?></td>
                        <td class="figurs"><?php echo number_format($amount,4)?></td>
                        <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
                    </tr>
                    
                <?php
                    $inv60AmountTot+=$amount;
                    $inv60TobeTot+=$toBePaid;
                    
                    
                }
                $amountTot+=$inv60AmountTot;
                    $toBeTot+=$inv60TobeTot;
                ?>
                <tr>
                    <td class="total1" colspan="3">Total</td>                        
                    <td class="total"><?php echo number_format($inv60AmountTot,4)?></td>
                    <td class="total"><?php echo number_format($inv60TobeTot,4)?></td>
                </tr>
                <tr class="subHed1"><td colspan="5">61 - 90</td></tr>
                    <!-- Invoice -->
                <?php
                if($filterCompany==0)$wareCom=" ";
                else $wareCom=" AND fin_customer_salesinvoice_header.intCompanyId=$filterCompany ";
                $inv90AmountTot=0;
                $inv90TobeTot=0;
                $sql="SELECT fin_customer_salesinvoice_header.strReferenceNo,			
				fin_customer_salesinvoice_header.dtmDate,				
				sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) AS amount,
				(
				sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) 
				-
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  INV.strReferenceNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'S.Invoice' AND
                                fin_customer_receivedpayments_header.dtmDate<='$toDay'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				
				) AS balAmount
				FROM
				fin_customer_salesinvoice_details AS INV
				Inner Join fin_customer_salesinvoice_header ON fin_customer_salesinvoice_header.strReferenceNo = INV.strReferenceNo AND INV.intInvoiceNo = fin_customer_salesinvoice_header.intInvoiceNo AND INV.intAccPeriodId = fin_customer_salesinvoice_header.intAccPeriodId AND INV.intLocationId = fin_customer_salesinvoice_header.intLocationId AND INV.intCompanyId = fin_customer_salesinvoice_header.intCompanyId
				Inner Join mst_customer ON mst_customer.intId = fin_customer_salesinvoice_header.intCustomerId
				INNER JOIN mst_financepaymentsterms AS PT ON fin_customer_salesinvoice_header.intPaymentsTermsId = PT.intId
				WHERE
				fin_customer_salesinvoice_header.intCustomerId = $custId AND
                                    fin_customer_salesinvoice_header.intDeleteStatus = '0' AND
                                    fin_customer_salesinvoice_header.intCurrencyId = $currency AND
                fin_customer_salesinvoice_header.dtmDate <='$toDay' AND
                                    DATEDIFF('$toDay',fin_customer_salesinvoice_header.dtmDate) >= (61+ PT.strName) AND
                                    DATEDIFF('$toDay',fin_customer_salesinvoice_header.dtmDate) <=(90+ PT.strName)
                                   ".$wareCom.
				" GROUP BY
				fin_customer_salesinvoice_header.strReferenceNo
				having balAmount<>0";
                $result = $db->RunQuery($sql);
                while($row=mysqli_fetch_array($result)){
                    
                    $invoice = $row['strReferenceNo'];
                    $amount = number_format($row['amount'],4,'.','');
                    $toBePaid = number_format($row['balAmount'],4,'.','');
                    $date = $row['dtmDate'];
               ?>
                    <tr onclick=" leadgerDrill('Sales Invoice','<?php echo $invoice ?>')" class="dillLink">
                        <td class="normalfntMid"><?php echo $invoice?></td>
                        <td class="normalfntMid"><?php echo $invoice?></td>
                        <td class="normalfntMid"><?php echo $date ?></td>
                        <td class="figurs"><?php echo number_format($amount,4)?></td>
                        <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
                    </tr>
                    
                <?php
                    $inv90AmountTot+=$amount;
                    $inv90TobeTot+=$toBePaid;
                }
                ?>
                
                <!-- Debit Note Invoice -->
                <?php
                if($filterCompany==0)$wareCom=" ";
                else $wareCom=" AND fin_customer_debitnoteinvoice_header.intCompanyId=$filterCompany ";
                //$inv90AmountTot=0;
                //$inv90TobeTot=0;
                $sql="SELECT fin_customer_debitnoteinvoice_header.strReferenceNo,
				fin_customer_debitnoteinvoice_header.strInvoiceNo,			
				fin_customer_debitnoteinvoice_header.dtmDate,				
				sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) AS amount,
				(
				sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) 
				-
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  INV.strReferenceNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'D.Invoice' AND
                                fin_customer_receivedpayments_header.dtmDate<='$toDay'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				
				) AS balAmount
				FROM
				fin_customer_debitnoteinvoice_details AS INV
				Inner Join fin_customer_debitnoteinvoice_header ON fin_customer_debitnoteinvoice_header.strReferenceNo = INV.strReferenceNo AND INV.intInvoiceNo = fin_customer_debitnoteinvoice_header.intInvoiceNo AND INV.intAccPeriodId = fin_customer_debitnoteinvoice_header.intAccPeriodId AND INV.intLocationId = fin_customer_debitnoteinvoice_header.intLocationId AND INV.intCompanyId = fin_customer_debitnoteinvoice_header.intCompanyId
				Inner Join mst_customer ON mst_customer.intId = fin_customer_debitnoteinvoice_header.intCustomerId
				INNER JOIN mst_financepaymentsterms AS PT ON fin_customer_debitnoteinvoice_header.intPaymentsTermsId = PT.intId
				WHERE
				fin_customer_debitnoteinvoice_header.intCustomerId = $custId AND
                                    fin_customer_debitnoteinvoice_header.intDeleteStatus = '0' AND
                                    fin_customer_debitnoteinvoice_header.intCurrencyId = $currency AND
                fin_customer_debitnoteinvoice_header.dtmDate <='$toDay' AND
                                    DATEDIFF('$toDay',fin_customer_debitnoteinvoice_header.dtmDate) >= (61+ PT.strName) AND
                                    DATEDIFF('$toDay',fin_customer_debitnoteinvoice_header.dtmDate) <=(90+ PT.strName)
                                   ".$wareCom.
				" GROUP BY
				fin_customer_debitnoteinvoice_header.strReferenceNo
				having balAmount<>0";
                $result = $db->RunQuery($sql);
                while($row=mysqli_fetch_array($result)){
                    
					$salesInvoice = $row['strInvoiceNo'];
                    $invoice = $row['strReferenceNo'];
                    $amount = number_format($row['amount'],4,'.','');
                    $toBePaid = number_format($row['balAmount'],4,'.','');
                    $date = $row['dtmDate'];
               ?>
            <tr class="dillLink">
                <td class="normalfntMid" onclick=" leadgerDrill('Debit Note Invoice','<?php echo $invoice ?>')"><?php echo $invoice?></td>
                <td class="normalfntMid" onclick=" leadgerDrill('Sales Invoice','<?php echo $salesInvoice ?>')"><?php echo $salesInvoice?></td>
                <td class="normalfntMid" onclick=" leadgerDrill('Debit Note Invoice','<?php echo $invoice ?>')"><?php echo $date ?></td>
                <td class="figurs" onclick=" leadgerDrill('Debit Note Invoice','<?php echo $invoice ?>')"><?php echo number_format($amount,4)?></td>
                <td class="figurs" onclick=" leadgerDrill('Debit Note Invoice','<?php echo $invoice ?>')"><?php echo number_format($toBePaid,4)?></td>
            </tr>
                    
                <?php
                    $inv90AmountTot+=$amount;
                    $inv90TobeTot+=$toBePaid;
                }
                ?>
                
                <!-- Bank Deposit -->                
                <?php
                if($filterCompany==0)$wareCom=" ";
                else $wareCom=" AND BND.intCompanyId=$filterCompany ";
                $sql = "SELECT
				BND.strDepositNo,
				BND.dtDate,
                                fin_bankdeposit_details.dblAmmount,
				(
				fin_bankdeposit_details.dblAmmount 
				+
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  BND.strDepositNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'B.Deposit' AND
                                fin_customer_receivedpayments_header.dtmDate<='$toDay'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				
				) AS balAmount
				FROM
				fin_bankdeposit_header BND
				Inner Join fin_bankdeposit_details ON fin_bankdeposit_details.strDepositNo = BND.strDepositNo				
				Inner Join mst_financechartofaccounts ON fin_bankdeposit_details.intAccount = mst_financechartofaccounts.intId
				WHERE
				fin_bankdeposit_details.intRecvFrom =  '$custId' AND
				mst_financechartofaccounts.intFinancialTypeId =  '10' AND
				BND.intStatus = '1' AND
                                BND.intCurrency=$currency AND
                BND.dtDate <='$toDay' AND
                                DATEDIFF('$toDay',BND.dtDate) >=61 AND
                                DATEDIFF('$toDay',BND.dtDate) <=90".$wareCom.
				"having balAmount<>0";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$deposit = $row['strDepositNo'];
			$amount = number_format(($row['dblAmmount']*(-1)),4,'.','');
			$toBePaid = number_format(($row['balAmount']*(-1)),4,'.','');
			$date = substr($row['dtDate'],0,10);
                ?>
               
                    <tr onclick=" leadgerDrill('Bank Deposit','<?php echo $deposit ?>')" class="dillLink">
                        <td class="normalfntMid"><?php echo $deposit?></td>
                        <td class="normalfntMid"><?php echo $deposit?></td>
                        <td class="normalfntMid"><?php echo $date ?></td>
                        <td class="figurs"><?php echo number_format($amount,4)?></td>
                        <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
                    </tr>
                    
                <?php
                    $inv90AmountTot+=$amount;
                    $inv90TobeTot+=$toBePaid;
                }
                ?>
                <!-- Bank Payments -->                                
                <?php
                if($filterCompany==0)$wareCom=" ";
                else $wareCom=" AND BNP.intCompanyId=$filterCompany ";
                $sql = "SELECT
				BNP.strBankPaymentNo,
				BNP.dtDate,
                                BNP.dblReceivedAmount,
								fin_bankpayment_details.dblAmmount,
				(
				fin_bankpayment_details.dblAmmount
				-
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  BNP.strBankPaymentNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'B.Payment' AND
                                fin_customer_receivedpayments_header.dtmDate<='$toDay'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				
				) AS balAmount
				FROM
				fin_bankpayment_header BNP
				Inner Join fin_bankpayment_details ON fin_bankpayment_details.strBankPaymentNo = BNP.strBankPaymentNo				
				Inner Join mst_financechartofaccounts ON fin_bankpayment_details.intAccountId = mst_financechartofaccounts.intId
				WHERE
				fin_bankpayment_details.intPayTo =  '$custId' AND
				mst_financechartofaccounts.intFinancialTypeId =  '10' AND
				BNP.intStatus = '1' AND
                                BNP.intCurrency=$currency AND
                BNP.dtDate <='$toDay' AND
                                DATEDIFF('$toDay',BNP.dtDate)>= 61 AND
                                DATEDIFF('$toDay',BNP.dtDate)<= 90 ".$wareCom. "
				having balAmount<>0";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$payment = $row['strBankPaymentNo'];
			$amount = number_format($row['dblAmmount'],4,'.','');
			$toBePaid = number_format($row['balAmount'],4,'.','');
			$date = substr($row['dtDate'],0,10);
                ?>
                
                    <tr onclick=" leadgerDrill('Bank Payment','<?php echo $payment ?>')" class="dillLink">
                        <td class="normalfntMid"><?php echo $payment?></td>
                        <td class="normalfntMid"><?php echo $payment?></td>
                        <td class="normalfntMid"><?php echo $date ?></td>
                        <td class="figurs"><?php echo number_format($amount,4)?></td>
                        <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
                    </tr>
                    
                <?php
                    $inv90AmountTot+=$amount;
                    $inv90TobeTot+=$toBePaid;
                }
                ?>
                <!-- Petty Cash -->                                
                <?php
                if($filterCompany==0)$wareCom=" ";
                else $wareCom=" AND PTC.intCompanyId=$filterCompany ";
                 $sql = "SELECT
				PTC.strPettyCashNo,
				PTC.dtDate,
                                PTC.dblReceivedAmount,
								fin_bankpettycash_details.dblAmmount,
				(
				fin_bankpettycash_details.dblAmmount
				-
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  PTC.strPettyCashNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'Petty Cash' AND
                                fin_customer_receivedpayments_header.dtmDate<='$toDay'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				
				) AS balAmount
				FROM
				fin_bankpettycash_header AS PTC
				Inner Join fin_bankpettycash_details ON fin_bankpettycash_details.strPettyCashNo = PTC.strPettyCashNo
				Inner Join mst_financechartofaccounts ON fin_bankpettycash_details.intAccountId = mst_financechartofaccounts.intId
				WHERE
				mst_financechartofaccounts.intFinancialTypeId =  '10'
				AND
				fin_bankpettycash_details.intPayTo =  '$custId' AND
				PTC.intStatus = '1' AND
                                PTC.intCurrency=$currency AND
                PTC.dtDate <='$toDay' AND
                                DATEDIFF('$toDay',PTC.dtDate)>= 61 AND
                                DATEDIFF('$toDay',PTC.dtDate)<= 90".$wareCom.
				"having balAmount<>0";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$pettyCash = $row['strPettyCashNo'];
			$amount = number_format($row['dblAmmount'],4,'.','');
			$toBePaid = number_format($row['balAmount'],4,'.','');
			$date = substr($row['dtDate'],0,10);
                ?>
                
                    <tr onclick=" leadgerDrill('Petty Cash','<?php echo $pettyCash ?>')" class="dillLink">
                        <td class="normalfntMid"><?php echo $pettyCash?></td>
                        <td class="normalfntMid"><?php echo $pettyCash?></td>
                        <td class="normalfntMid"><?php echo $date ?></td>
                        <td class="figurs"><?php echo number_format($amount,4)?></td>
                        <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
                    </tr>
                    
                <?php
                    $inv90AmountTot+=$amount;
                    $inv90TobeTot+=$toBePaid;
                }
                ?>
                <!-- Jurnel Entry -->                                
                <?php
                if($filterCompany==0)$wareCom=" ";
                else $wareCom=" AND JH.intCompanyId=$filterCompany ";
                $sql = "SELECT
                            JH.strReferenceNo,
                            JH.dtmDate, 
                            JD.dblDebitAmount,
                            JD.dbCreditAmount,
                            (
                            JD.dblDebitAmount - JD.dbCreditAmount -
                            IFNULL((SELECT
							Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
							FROM
							fin_customer_receivedpayments_main_details
							Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
							Inner Join fin_accountant_journal_entry_details ON fin_customer_receivedpayments_main_details.strDocNo = fin_accountant_journal_entry_details.strReferenceNo
							WHERE
							fin_customer_receivedpayments_main_details.strJobNo =  'JH.strReferenceNo' AND
							fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND
							fin_customer_receivedpayments_main_details.strDocType =  'JN' AND
							fin_customer_receivedpayments_header.intCustomerId =  '$custId' AND
							fin_customer_receivedpayments_header.dtmDate <=  '$toDay' AND
							fin_accountant_journal_entry_details.strPersonType =  'cus'
							GROUP BY
							fin_customer_receivedpayments_main_details.strJobNo),0)
                            ) AS balAmount
                            FROM
                            fin_accountant_journal_entry_header AS JH
                            INNER JOIN fin_accountant_journal_entry_details AS JD ON JH.strReferenceNo = JD.strReferenceNo                            
                            INNER JOIN mst_financechartofaccounts ON JD.intChartOfAccountId = mst_financechartofaccounts.intId
                            WHERE
                            JD.strPersonType = 'cus' AND
                            JD.intNameId = $custId AND
                            JH.intDeleteStatus = 0 AND
                            mst_financechartofaccounts.intFinancialTypeId = 10 AND
                            JH.intCurrencyId=$currency AND
                JH.dtmDate <='$toDay' AND
                            DATEDIFF('$toDay', JH.dtmDate)>= 61 AND
                            DATEDIFF('$toDay', JH.dtmDate)<= 90".$wareCom.
                            "HAVING
                            balAmount <> 0";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$payment = $row['strReferenceNo'];
			$amount = number_format(($row['dblDebitAmount'] - $row['dbCreditAmount']),4,'.','');
                        
                        //if($row['dbCreditAmount']!=0){
//                            $toBePaid = number_format(($row['balAmount']*(-1)),4,'.','');
//                        }
//                        else{
                            $toBePaid = number_format(($row['balAmount']),4,'.','');
                        //}                       
			                        
			$date = substr($row['dtmDate'],0,10);
                ?>
                
                    <tr onclick=" leadgerDrill('Journal Entry','<?php echo $payment ?>')" class="dillLink">
                        <td class="normalfntMid"><?php echo $payment?></td>
                        <td class="normalfntMid"><?php echo $payment?></td>
                        <td class="normalfntMid"><?php echo $date ?></td>
                        <td class="figurs"><?php echo number_format($amount,4)?></td>
                        <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
                    </tr>
                    
                <?php
                    $inv90AmountTot+=$amount;
                    $inv90TobeTot+=$toBePaid;
                    
                    
                }
                $amountTot+=$inv90AmountTot;
                    $toBeTot+=$inv90TobeTot;
                ?>
                <tr>
                    <td class="total1" colspan="3">Total</td>                        
                    <td class="total"><?php echo number_format($inv90AmountTot,4)?></td>
                    <td class="total"><?php echo number_format($inv90TobeTot,4)?></td>
                </tr>
                <tr class="subHed1"><td colspan="5">More Than 90 Days</td></tr>
                    <!-- Invoice -->
                <?php
                if($filterCompany==0)$wareCom=" ";
                else $wareCom=" AND fin_customer_salesinvoice_header.intCompanyId=$filterCompany ";
                $inv91AmountTot=0;
                $inv91TobeTot=0;
                $sql="SELECT fin_customer_salesinvoice_header.strReferenceNo,			
				fin_customer_salesinvoice_header.dtmDate,				
				sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) AS amount,
				(
				sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) 
				-
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  INV.strReferenceNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'S.Invoice' AND
                                fin_customer_receivedpayments_header.dtmDate<='$toDay'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				
				) AS balAmount
				FROM
				fin_customer_salesinvoice_details AS INV
				Inner Join fin_customer_salesinvoice_header ON fin_customer_salesinvoice_header.strReferenceNo = INV.strReferenceNo AND INV.intInvoiceNo = fin_customer_salesinvoice_header.intInvoiceNo AND INV.intAccPeriodId = fin_customer_salesinvoice_header.intAccPeriodId AND INV.intLocationId = fin_customer_salesinvoice_header.intLocationId AND INV.intCompanyId = fin_customer_salesinvoice_header.intCompanyId
				Inner Join mst_customer ON mst_customer.intId = fin_customer_salesinvoice_header.intCustomerId
				INNER JOIN mst_financepaymentsterms AS PT ON fin_customer_salesinvoice_header.intPaymentsTermsId = PT.intId
				WHERE
				fin_customer_salesinvoice_header.intCustomerId = $custId AND
                                    fin_customer_salesinvoice_header.intDeleteStatus = '0' AND
                                    fin_customer_salesinvoice_header.intCurrencyId = $currency AND
                fin_customer_salesinvoice_header.dtmDate <='$toDay' AND
                                    DATEDIFF('$toDay',fin_customer_salesinvoice_header.dtmDate) >= (91+ PT.strName)                                    
                                   ".$wareCom.
				" GROUP BY
				fin_customer_salesinvoice_header.strReferenceNo
				having balAmount<>0";
                $result = $db->RunQuery($sql);
                while($row=mysqli_fetch_array($result)){
                    
                    $invoice = $row['strReferenceNo'];
                    $amount = number_format($row['amount'],4,'.','');
                    $toBePaid = number_format($row['balAmount'],4,'.','');
                    $date = $row['dtmDate'];
               ?>
                    <tr onclick=" leadgerDrill('Sales Invoice','<?php echo $invoice ?>')" class="dillLink">
                        <td class="normalfntMid"><?php echo $invoice?></td>
                        <td class="normalfntMid"><?php echo $invoice?></td>
                        <td class="normalfntMid"><?php echo $date ?></td>
                        <td class="figurs"><?php echo number_format($amount,4)?></td>
                        <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
                    </tr>
                    
                <?php
                    $inv91AmountTot+=$amount;
                    $inv91TobeTot+=$toBePaid;
                }
                ?>
                
                <!-- Debit Note Invoice -->
                <?php
                if($filterCompany==0)$wareCom=" ";
                else $wareCom=" AND fin_customer_debitnoteinvoice_header.intCompanyId=$filterCompany ";
                //$inv91AmountTot=0;
                //$inv91TobeTot=0;
                $sql="SELECT fin_customer_debitnoteinvoice_header.strReferenceNo,
				fin_customer_debitnoteinvoice_header.strInvoiceNo,			
				fin_customer_debitnoteinvoice_header.dtmDate,				
				sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) AS amount,
				(
				sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) 
				-
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  INV.strReferenceNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'D.Invoice' AND
                                fin_customer_receivedpayments_header.dtmDate<='$toDay'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				
				) AS balAmount
				FROM
				fin_customer_debitnoteinvoice_details AS INV
				Inner Join fin_customer_debitnoteinvoice_header ON fin_customer_debitnoteinvoice_header.strReferenceNo = INV.strReferenceNo AND INV.intInvoiceNo = fin_customer_debitnoteinvoice_header.intInvoiceNo AND INV.intAccPeriodId = fin_customer_debitnoteinvoice_header.intAccPeriodId AND INV.intLocationId = fin_customer_debitnoteinvoice_header.intLocationId AND INV.intCompanyId = fin_customer_debitnoteinvoice_header.intCompanyId
				Inner Join mst_customer ON mst_customer.intId = fin_customer_debitnoteinvoice_header.intCustomerId
				INNER JOIN mst_financepaymentsterms AS PT ON fin_customer_debitnoteinvoice_header.intPaymentsTermsId = PT.intId
				WHERE
				fin_customer_debitnoteinvoice_header.intCustomerId = $custId AND
                                    fin_customer_debitnoteinvoice_header.intDeleteStatus = '0' AND
                                    fin_customer_debitnoteinvoice_header.intCurrencyId = $currency AND
                fin_customer_debitnoteinvoice_header.dtmDate <='$toDay' AND
                                    DATEDIFF('$toDay',fin_customer_debitnoteinvoice_header.dtmDate) >= (91+ PT.strName)                                    
                                   ".$wareCom.
				" GROUP BY
				fin_customer_debitnoteinvoice_header.strReferenceNo
				having balAmount<>0";
                $result = $db->RunQuery($sql);
                while($row=mysqli_fetch_array($result)){
                    
					$salesInvoice = $row['strInvoiceNo'];
                    $invoice = $row['strReferenceNo'];
                    $amount = number_format($row['amount'],4,'.','');
                    $toBePaid = number_format($row['balAmount'],4,'.','');
                    $date = $row['dtmDate'];
               ?>
            <tr class="dillLink">
                <td class="normalfntMid" onclick=" leadgerDrill('Debit Note Invoice','<?php echo $invoice ?>')"><?php echo $invoice?></td>
                <td class="normalfntMid" onclick=" leadgerDrill('Sales Invoice','<?php echo $salesInvoice ?>')"><?php echo $salesInvoice?></td>
                <td class="normalfntMid" onclick=" leadgerDrill('Debit Note Invoice','<?php echo $invoice ?>')"><?php echo $date ?></td>
                <td class="figurs" onclick=" leadgerDrill('Debit Note Invoice','<?php echo $invoice ?>')"><?php echo number_format($amount,4)?></td>
                <td class="figurs" onclick=" leadgerDrill('Debit Note Invoice','<?php echo $invoice ?>')"><?php echo number_format($toBePaid,4)?></td>
            </tr>
                    
                <?php
                    $inv91AmountTot+=$amount;
                    $inv91TobeTot+=$toBePaid;
                }
                ?>
                
                <!-- Bank Deposit -->                
                <?php
                if($filterCompany==0)$wareCom=" ";
                else $wareCom=" AND BND.intCompanyId=$filterCompany ";
                $sql = "SELECT
				BND.strDepositNo,
				BND.dtDate,
                                fin_bankdeposit_details.dblAmmount,
				(
				fin_bankdeposit_details.dblAmmount 
				+
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  BND.strDepositNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'B.Deposit' AND
                                fin_customer_receivedpayments_header.dtmDate<='$toDay'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				
				) AS balAmount
				FROM
				fin_bankdeposit_header BND
				Inner Join fin_bankdeposit_details ON fin_bankdeposit_details.strDepositNo = BND.strDepositNo				
				Inner Join mst_financechartofaccounts ON fin_bankdeposit_details.intAccount = mst_financechartofaccounts.intId
				WHERE
				fin_bankdeposit_details.intRecvFrom =  '$custId' AND
				mst_financechartofaccounts.intFinancialTypeId =  '10' AND
				BND.intStatus = '1' AND
                                BND.intCurrency=$currency AND
                BND.dtDate <='$toDay' AND
                                DATEDIFF('$toDay',BND.dtDate) >=91 ".$wareCom.
				"having balAmount<>0";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$deposit = $row['strDepositNo'];
			$amount = number_format(($row['dblAmmount']*(-1)),4,'.','');
			$toBePaid = number_format(($row['balAmount']*(-1)),4,'.','');
			$date = substr($row['dtDate'],0,10);
                ?>
               
                    <tr onclick=" leadgerDrill('Bank Deposit','<?php echo $deposit ?>')" class="dillLink">
                        <td class="normalfntMid"><?php echo $deposit?></td>
                        <td class="normalfntMid"><?php echo $deposit?></td>
                        <td class="normalfntMid"><?php echo $date ?></td>
                        <td class="figurs"><?php echo number_format($amount,4)?></td>
                        <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
                    </tr>
                    
                <?php
                    $inv91AmountTot+=$amount;
                    $inv91TobeTot+=$toBePaid;
                }
                ?>
                <!-- Bank Payments -->                                
                <?php
                if($filterCompany==0)$wareCom=" ";
                else $wareCom=" AND BNP.intCompanyId=$filterCompany ";
                $sql = "SELECT
				BNP.strBankPaymentNo,
				BNP.dtDate,
                                BNP.dblReceivedAmount,
								fin_bankpayment_details.dblAmmount,
				(
				fin_bankpayment_details.dblAmmount
				-
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  BNP.strBankPaymentNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'B.Payment' AND
                                fin_customer_receivedpayments_header.dtmDate<='$toDay'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				
				) AS balAmount
				FROM
				fin_bankpayment_header BNP
				Inner Join fin_bankpayment_details ON fin_bankpayment_details.strBankPaymentNo = BNP.strBankPaymentNo				
				Inner Join mst_financechartofaccounts ON fin_bankpayment_details.intAccountId = mst_financechartofaccounts.intId
				WHERE
				fin_bankpayment_details.intPayTo =  '$custId' AND
				mst_financechartofaccounts.intFinancialTypeId =  '10' AND
				BNP.intStatus = '1' AND
                                BNP.intCurrency=$currency AND
                BNP.dtDate <='$toDay' AND
                                DATEDIFF('$toDay',BNP.dtDate)>= 91 ".$wareCom. "
				having balAmount<>0";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$payment = $row['strBankPaymentNo'];
			$amount = number_format($row['dblAmmount'],4,'.','');
			$toBePaid = number_format($row['balAmount'],4,'.','');
			$date = substr($row['dtDate'],0,10);
                ?>
                
                    <tr onclick=" leadgerDrill('Bank Payment','<?php echo $payment ?>')" class="dillLink">
                        <td class="normalfntMid"><?php echo $payment?></td>
                        <td class="normalfntMid"><?php echo $payment?></td>
                        <td class="normalfntMid"><?php echo $date ?></td>
                        <td class="figurs"><?php echo number_format($amount,4)?></td>
                        <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
                    </tr>
                    
                <?php
                    $inv91AmountTot+=$amount;
                    $inv91TobeTot+=$toBePaid;
                }
                ?>
                <!-- Petty Cash -->                                
                <?php
                if($filterCompany==0)$wareCom=" ";
                else $wareCom=" AND PTC.intCompanyId=$filterCompany ";
                 $sql = "SELECT
				PTC.strPettyCashNo,
				PTC.dtDate,
                                PTC.dblReceivedAmount,
								fin_bankpettycash_details.dblAmmount,
				(
				fin_bankpettycash_details.dblAmmount
				-
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  PTC.strPettyCashNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'Petty Cash' AND
                                fin_customer_receivedpayments_header.dtmDate<='$toDay'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				
				) AS balAmount
				FROM
				fin_bankpettycash_header AS PTC
				Inner Join fin_bankpettycash_details ON fin_bankpettycash_details.strPettyCashNo = PTC.strPettyCashNo
				Inner Join mst_financechartofaccounts ON fin_bankpettycash_details.intAccountId = mst_financechartofaccounts.intId
				WHERE
				mst_financechartofaccounts.intFinancialTypeId =  '10'
				AND
				fin_bankpettycash_details.intPayTo =  '$custId' AND
				PTC.intStatus = '1' AND
                                PTC.intCurrency=$currency AND
                PTC.dtDate <='$toDay' AND
                                DATEDIFF('$toDay',PTC.dtDate)>= 91 ".$wareCom.
				"having balAmount<>0";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$pettyCash = $row['strPettyCashNo'];
			$amount = number_format($row['dblAmmount'],4,'.','');
			$toBePaid = number_format($row['balAmount'],4,'.','');
			$date = substr($row['dtDate'],0,10);
                ?>
                
                    <tr onclick=" leadgerDrill('Petty Cash','<?php echo $pettyCash ?>')" class="dillLink">
                        <td class="normalfntMid"><?php echo $pettyCash?></td>
                        <td class="normalfntMid"><?php echo $pettyCash?></td>
                        <td class="normalfntMid"><?php echo $date ?></td>
                        <td class="figurs"><?php echo number_format($amount,4)?></td>
                        <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
                    </tr>
                    
                <?php
                    $inv91AmountTot+=$amount;
                    $inv91TobeTot+=$toBePaid;
                }
                ?>
                <!-- Jurnel Entry -->                                
                <?php
                if($filterCompany==0)$wareCom=" ";
                else $wareCom=" AND JH.intCompanyId=$filterCompany ";
                $sql = "SELECT
                            JH.strReferenceNo,
                            JH.dtmDate, 
                            JD.dblDebitAmount,
                            JD.dbCreditAmount,
                            (
                            JD.dblDebitAmount - JD.dbCreditAmount -
                            IFNULL((SELECT
							Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
							FROM
							fin_customer_receivedpayments_main_details
							Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
							Inner Join fin_accountant_journal_entry_details ON fin_customer_receivedpayments_main_details.strDocNo = fin_accountant_journal_entry_details.strReferenceNo
							WHERE
							fin_customer_receivedpayments_main_details.strJobNo =  'JH.strReferenceNo' AND
							fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND
							fin_customer_receivedpayments_main_details.strDocType =  'JN' AND
							fin_customer_receivedpayments_header.intCustomerId =  '$custId' AND
							fin_customer_receivedpayments_header.dtmDate <=  '$toDay' AND
							fin_accountant_journal_entry_details.strPersonType =  'cus'
							GROUP BY
							fin_customer_receivedpayments_main_details.strJobNo),0)
                            ) AS balAmount
                            FROM
                            fin_accountant_journal_entry_header AS JH
                            INNER JOIN fin_accountant_journal_entry_details AS JD ON JH.strReferenceNo = JD.strReferenceNo                            
                            INNER JOIN mst_financechartofaccounts ON JD.intChartOfAccountId = mst_financechartofaccounts.intId
                            WHERE
                            JD.strPersonType = 'cus' AND
                            JD.intNameId = $custId AND
                            JH.intDeleteStatus = 0 AND
                            mst_financechartofaccounts.intFinancialTypeId = 10 AND
                            JH.intCurrencyId=$currency AND
                            JH.dtmDate <='$toDay' AND
                            DATEDIFF('$toDay', JH.dtmDate)>= 91 ".$wareCom.
                            "HAVING
                            balAmount <> 0";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$payment = $row['strReferenceNo'];
			$amount = number_format(($row['dblDebitAmount'] - $row['dbCreditAmount']),4,'.','');
                       // 
//                        if($row['dbCreditAmount']!=0){
//                            $toBePaid = number_format(($row['balAmount']*(-1)),4,'.','');
//                        }
//                        else{
                            $toBePaid = number_format(($row['balAmount']),4,'.','');
                        //}                       
			                        
			$date = substr($row['dtmDate'],0,10);
                ?>
                
                    <tr onclick=" leadgerDrill('Journal Entry','<?php echo $payment ?>')" class="dillLink">
                        <td class="normalfntMid"><?php echo $payment?></td>
                        <td class="normalfntMid"><?php echo $payment?></td>
                        <td class="normalfntMid"><?php echo $date ?></td>
                        <td class="figurs"><?php echo number_format($amount,4)?></td>
                        <td class="figurs"><?php echo number_format($toBePaid,4)?></td>
                    </tr>
                    
                <?php
                    $inv91AmountTot+=$amount;
                    $inv91TobeTot+=$toBePaid;
                    
                    
                }
                $amountTot+=$inv91AmountTot;
                    $toBeTot+=$inv91TobeTot;
                ?>
                <tr>
                    <td class="total1" colspan="3">Total</td>                        
                    <td class="total"><?php echo number_format($inv91AmountTot,4)?></td>
                    <td class="total"><?php echo number_format($inv91TobeTot,4)?></td>
                </tr>
                <tr>
                    <td class="total1" colspan="3"><hr/></td>                        
                    
                </tr>
                <tr>
                    <td class="total1" colspan="3">Total</td>                        
                    <td class="total"><?php echo number_format($amountTot,4)?></td>
                    <td class="total"><?php echo number_format($toBeTot,4)?></td>
                </tr>
                </table>
            </div>
        </form>
    </body>
</html>
<?php
session_start();
$backwardseperator = "../../../../";
$companyId = $_SESSION['headCompanyId'];
$location 	= $_SESSION['CompanyID'];
$intUser  = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
include  	"{$backwardseperator}dataAccess/Connector.php";
				 
	$locationId = $location;//this locationId use in report header(reportHeader.php)--------------------
//--------------------------------------------------------------	
//$c_id = 21;
$c_id = $_REQUEST['sub_cat'];

$to_daY = $_REQUEST['to_daY'];
//$to_daY=date("Y-m-d");
//$to_daY='2012-05-20';

$currency = $_REQUEST['currency'];
$creditPeriod=30;

//-------------------------
 $sql = "SELECT
		intId,
		strCode
		FROM mst_financecurrency
		WHERE
		intId = '$currency'  
		";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$currencyDesc=$row['strCode'];
//-------------------------
 $sql = "SELECT
		mst_customer.intId,
		mst_customer.strName
		FROM mst_customer
		WHERE
		mst_customer.intId =  '$c_id' 
		";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$custDesc=$row['strName'];
//-------------------------
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Customer Age Analysis</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />


<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../warehouse/grn/listing/rptGrn-js.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:252px;
	top:173px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<form id="frmGRNApprovalReport" name="frmGRNApprovalReport" method="post" action="customerBalance_summeryRpt.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include '../../reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" >AGE ANALYSIS OF A CUSTOMER (SALES INVOICE) AS AT <?php echo $to_daY ; ?></div>
<table width="1100" border="0" align="center" bgcolor="#FFFFFF">

 <tr>
  <td>
    <table width="100%">
            <tr>
                <td colspan="2" style="border-right:0px; border-top:1px solid black;">
                    <table  border="0" cellpadding="2" cellspacing="0" align="center" class="main" width="100%">
                        <thead>
                            <tr>
                            <th></th>
                            <th class="normalfnt"><strong>Customer: <?php echo $custDesc ?></strong></th>
                            <th class="normalfnt"><strong></strong></th>
                            <th class="normalfnt"><strong>Currency:</strong></th>
                            <th class="normalfnt"><strong><?php echo $currencyDesc ?></strong></th>
                            <th class="normalfnt"><strong></strong></th>
                            <th class="normalfnt"><strong></strong></th>
                            <th class="normalfnt"><strong></strong></th>
                            </tr>
	                        <tr>
								<td colspan="9" align="center"><hr></hr></td>                        
	                        </tr>
                            <tr>
                        		<th></th>
                                <th class="normalfnt"><strong>Invoice No</strong></th>
                                <th class="normalfnt"><strong>Job No.</strong></th>
                                <th class="normalfnt"><strong>Dispatch No.</strong></th>
                                <th class="normalfnt"><strong>Customer PO</strong></th>
                                <th class="normalfnt"><strong>Date</strong></th>
                                <th class="normalfnt"><strong>No Of Days</strong></th>
                                <th class="normalfnt"><strong>Total Amount</strong></th>
                                <th class="normalfnt"><strong>To Be Paid</strong></th>
                            </tr>
                        </thead>

                        <tbody>
	                        <tr>
								<td colspan="9" align="center"><hr></hr></td>                        
	                        </tr>
                        <tr>
                            <th colspan="9" align="left" class="normalfnt"><strong>CURRENT OUTSTANDING</strong></th>
                        </tr>
                         <?PHP
						 try{
							 // GET INVOICE DETAULS details

							 $sql1="SELECT
									fin_customer_salesinvoice_header.strReferenceNo,
									fin_customer_salesinvoice_header.strPoNo,
									fin_customer_salesinvoice_header.dtmDate, 
									DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate) as noOfDays, 
									round(Sum(fin_customer_salesinvoice_details.dblTaxAmount+fin_customer_salesinvoice_details.dblQty*fin_customer_salesinvoice_details.dblUnitPrice*(100-fin_customer_salesinvoice_details.dblDiscount)/100) ,2) as amount 
									FROM
									fin_customer_salesinvoice_header
									Inner Join fin_customer_salesinvoice_details ON fin_customer_salesinvoice_header.intInvoiceNo = fin_customer_salesinvoice_details.intInvoiceNo
									WHERE
									fin_customer_salesinvoice_header.intCustomerId = '$c_id' AND 
									DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)>=0 AND DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)<=(30)   AND 
							fin_customer_salesinvoice_header.intCurrencyId='$currency' 
									GROUP BY
									fin_customer_salesinvoice_header.intCustomerId,
									fin_customer_salesinvoice_header.intInvoiceNo";
									$result  = $db->RunQuery($sql1);
									
									while($row = mysqli_fetch_assoc($result)){
										
									 $invNo=$row['strReferenceNo'];
									 $poNo=$row['strPoNo'];
									 $amount=$row['amount'];
									 $nonPaidAmnt=0;	
									  $sqls=" SELECT
											Sum(fin_customer_receivedpayments_main_details.dblToBePaid) as toBePaid 
											FROM
											fin_customer_receivedpayments_main_details
											left Join fin_customer_salesinvoice_header ON fin_customer_receivedpayments_main_details.strDocNo = fin_customer_salesinvoice_header.strReferenceNo
											left Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
											WHERE
											fin_customer_salesinvoice_header.intCustomerId =  '$c_id' AND 
											fin_customer_receivedpayments_main_details.strDocNo =  '$invNo' AND 
											fin_customer_receivedpayments_main_details.strDocType =  'S.Invoice' AND 
											 DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)>=0 AND DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)<=(30)   AND 
							fin_customer_salesinvoice_header.intCurrencyId='$currency' 
											GROUP BY
											fin_customer_salesinvoice_header.intCustomerId, 
											fin_customer_salesinvoice_header.intInvoiceNo";

						 
									$results  = $db->RunQuery($sqls);
									$rows = mysqli_fetch_assoc($results);
									
									if($rows['toBePaid']==''){
										$nonPaidAmnt=$amount;
									}
									$val = $rows['toBePaid']+$nonPaidAmnt;

						 ?>
                         <tr>
                         		<th></th>
                                <td align="center" class="normalfntMid">
                                	 <?php echo "<a href='.../.../../../../../customer/salesInvoice/salesInvoice.php?id=$invNo&currency=$currency' target='_blank'>$invNo</a>";
;?> 
                                </td >
                                <td align="center" class="normalfnt">
                                	<?php 
                                		if( $row['JOB_NO']!= 'CS'){
                                			?>
		                                	 <?php echo $row['JOB_NO'] ?>
		                                	 
                                			<?php 
                                		} 
                                		else{
                                			echo $row['JOB_NO'];
                                		}
                                	?>
                                </td>
                                <td align="center" class="normalfnt"></td>
                                <td align="center" class="normalfnt"><?php echo $poNo ?></td>
                                <td align="center" class="normalfnt"><?php echo $row['dtmDate']?> </td>
                                <td align="right" class="normalfnt"><?php echo $row['noOfDays']?></td>
                                <td align="right" class="normalfntRight"><?php echo number_format($amount, 2) ?></td>
                                <td align="right" class="normalfntRight"><?php echo number_format($val, 2) ?></td>
                            </tr>

                         <?PHP
						 			$totalAll+=$amount;
									$totlaToBe+=$val;
								}

                            }catch (Exception $e) {
                                echo "<tr><td colspan=\"300\"><b>". $e->getMessage() ."</b></td></tr>";
                            }
                            ?>
                            <tr>
							<td colspan="7" align="right" class="normalfntRight">Total</td>
                            <td align="right" style="border-right:0px; border-top:1px solid black;"  class="normalfntRight"><?php echo number_format($totalAll, 2) ?></td>
                            <td align="right" style="border-right:0px; border-top:1px solid black;"  class="normalfntRight"><?php echo number_format($totlaToBe, 2) ?></td>                        
                            <?php 
                            	$grandToBePaid = $grandToBePaid + $totlaToBe;
                            	$grandTotal = $grandTotal + $totalAll;
                            ?>                        
                        </tr>
	                        <tr>
								<td colspan="9" align="center"><hr></hr></td>                        
	                        </tr>
                        <tr>
                            <th colspan="9" align="left" class="normalfnt"><strong>0 - 30 Days</strong></th>
                        </tr>
                         <?PHP 
						 			$totalAll=0;
									$totlaToBe=0;
						 try{
							 $sql1="SELECT
									fin_customer_salesinvoice_header.strReferenceNo,
									fin_customer_salesinvoice_header.strPoNo,
									fin_customer_salesinvoice_header.dtmDate, 
									DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate) as noOfDays, 
									round(Sum(fin_customer_salesinvoice_details.dblTaxAmount+fin_customer_salesinvoice_details.dblQty*fin_customer_salesinvoice_details.dblUnitPrice*(100-fin_customer_salesinvoice_details.dblDiscount)/100) ,2) as amount 
									FROM
									fin_customer_salesinvoice_header
									Inner Join fin_customer_salesinvoice_details ON fin_customer_salesinvoice_header.intInvoiceNo = fin_customer_salesinvoice_details.intInvoiceNo
									WHERE
									fin_customer_salesinvoice_header.intCustomerId = '$c_id' AND 
									 DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)>30 AND DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)<=(30+$creditPeriod)   AND 
							fin_customer_salesinvoice_header.intCurrencyId='$currency' 
									GROUP BY
									fin_customer_salesinvoice_header.intCustomerId,
									fin_customer_salesinvoice_header.intInvoiceNo";
									$result  = $db->RunQuery($sql1);
									
									while($row = mysqli_fetch_assoc($result)){
										
									 $invNo=$row['strReferenceNo'];
									 $poNo=$row['strPoNo'];
									 $amount=$row['amount'];
									 $nonPaidAmnt=0;	
									  $sqls=" SELECT
											Sum(fin_customer_receivedpayments_main_details.dblToBePaid) as toBePaid 
											FROM
											fin_customer_receivedpayments_main_details
											left Join fin_customer_salesinvoice_header ON fin_customer_receivedpayments_main_details.strDocNo = fin_customer_salesinvoice_header.strReferenceNo
											left Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
											WHERE
											fin_customer_salesinvoice_header.intCustomerId =  '$c_id' AND 
											fin_customer_receivedpayments_main_details.strDocNo =  '$invNo' AND 
											fin_customer_receivedpayments_main_details.strDocType =  'S.Invoice' AND 
									 DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)>30 AND DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)<=(30+$creditPeriod)   AND 
							fin_customer_salesinvoice_header.intCurrencyId='$currency' 
											GROUP BY
											fin_customer_salesinvoice_header.intCustomerId, 
											fin_customer_salesinvoice_header.intInvoiceNo";

						 
									$results  = $db->RunQuery($sqls);
									$rows = mysqli_fetch_assoc($results);
									
									if($rows['toBePaid']==''){
										$nonPaidAmnt=$amount;
									}
									$val = $rows['toBePaid']+$nonPaidAmnt;

						 ?>
                         <tr>
                         		<th></th>
                                <td align="center" class="normalfntMid">
                                	 <?php echo "<a href='.../.../../../../../customer/salesInvoice/salesInvoice.php?id=$invNo&currency=$currency' target='_blank'>$invNo</a>"; ?>
                                </td >
                                <td align="center" class="normalfnt">
                                	<?php 
                                		if( $row['JOB_NO']!= 'CS'){
                                			?>
		                                	 <?php echo $row['JOB_NO'] ?>
		                                	 
                                			<?php 
                                		} 
                                		else{
                                			echo $row['JOB_NO'];
                                		}
                                	?>
                                </td>
                                <td align="center" class="normalfnt"></td>
                                <td align="center" class="normalfnt"><?php echo $poNo ?></td>
                                <td align="center" class="normalfnt"><?php echo $row['dtmDate']?> </td>
                                <td align="right" class="normalfnt"><?php echo $row['noOfDays']?></td>
                                <td align="right" class="normalfntRight"><?php echo number_format($amount, 2) ?></td>
                                <td align="right" class="normalfntRight"><?php echo number_format($val, 2) ?></td>
                            </tr>

                         <?PHP
						 			$totalAll+=$amount;
									$totlaToBe+=$val;
								}

                            }catch (Exception $e) {
                                echo "<tr><td colspan=\"300\"><b>". $e->getMessage() ."</b></td></tr>";
                            }
                            ?>
                            <tr>
							<td colspan="7" align="right" class="normalfntRight">Total</td>
                            <td align="right" style="border-right:0px; border-top:1px solid black;"  class="normalfntRight"><?php echo number_format($totalAll, 2) ?></td>
                            <td align="right" style="border-right:0px; border-top:1px solid black;"  class="normalfntRight"><?php echo number_format($totlaToBe, 2) ?></td>                        
                            <?php 
                            	$grandToBePaid = $grandToBePaid + $totlaToBe;
                            	$grandTotal = $grandTotal + $totalAll;
                            ?>                        
                        </tr>
	                        <tr>
								<td colspan="9" align="center"><hr></hr></td>                        
	                        </tr>
                        <tr>
                            <th colspan="9" align="left" class="normalfnt"><strong>31 - 60 Days</strong></th>
                        </tr>
                         <?PHP 
						 			$totalAll=0;
									$totlaToBe=0;
						 try{
							 $sql1="SELECT
									fin_customer_salesinvoice_header.strReferenceNo,
									fin_customer_salesinvoice_header.strPoNo,
									fin_customer_salesinvoice_header.dtmDate, 
									DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate) as noOfDays, 
									round(Sum(fin_customer_salesinvoice_details.dblTaxAmount+fin_customer_salesinvoice_details.dblQty*fin_customer_salesinvoice_details.dblUnitPrice*(100-fin_customer_salesinvoice_details.dblDiscount)/100) ,2) as amount 
									FROM
									fin_customer_salesinvoice_header
									Inner Join fin_customer_salesinvoice_details ON fin_customer_salesinvoice_header.intInvoiceNo = fin_customer_salesinvoice_details.intInvoiceNo
									WHERE
									fin_customer_salesinvoice_header.intCustomerId = '$c_id' AND 
									 DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)>30+$creditPeriod AND DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)<=(30+2*$creditPeriod)   AND 
							fin_customer_salesinvoice_header.intCurrencyId='$currency' 
									GROUP BY
									fin_customer_salesinvoice_header.intCustomerId,
									fin_customer_salesinvoice_header.intInvoiceNo";
									$result  = $db->RunQuery($sql1);
									
									while($row = mysqli_fetch_assoc($result)){
										
									 $invNo=$row['strReferenceNo'];
									 $poNo=$row['strPoNo'];
									 $amount=$row['amount'];
									 $nonPaidAmnt=0;	
									  $sqls=" SELECT
											Sum(fin_customer_receivedpayments_main_details.dblToBePaid) as toBePaid 
											FROM
											fin_customer_receivedpayments_main_details
											left Join fin_customer_salesinvoice_header ON fin_customer_receivedpayments_main_details.strDocNo = fin_customer_salesinvoice_header.strReferenceNo
											left Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
											WHERE
											fin_customer_salesinvoice_header.intCustomerId =  '$c_id' AND 
											fin_customer_receivedpayments_main_details.strDocNo =  '$invNo' AND 
											fin_customer_receivedpayments_main_details.strDocType =  'S.Invoice' AND 
									 DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)>30+$creditPeriod AND DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)<=(30+2*$creditPeriod)   AND 
							fin_customer_salesinvoice_header.intCurrencyId='$currency' 
											GROUP BY
											fin_customer_salesinvoice_header.intCustomerId, 
											fin_customer_salesinvoice_header.intInvoiceNo";

						 
									$results  = $db->RunQuery($sqls);
									$rows = mysqli_fetch_assoc($results);
									
									if($rows['toBePaid']==''){
										$nonPaidAmnt=$amount;
									}
									$val = $rows['toBePaid']+$nonPaidAmnt;

						 ?>
                         <tr>
                         		<th></th>
                                <td align="center" class="normalfntMid">
                                	 <?php echo "<a href='.../.../../../../../customer/salesInvoice/salesInvoice.php?id=$invNo&currency=$currency' target='_blank'>$invNo</a>"; ?>
                                </td >
                                <td align="center" class="normalfnt">
                                	<?php 
                                		if( $row['JOB_NO']!= 'CS'){
                                			?>
		                                	 <?php echo $row['JOB_NO'] ?>
		                                	 
                                			<?php 
                                		} 
                                		else{
                                			echo $row['JOB_NO'];
                                		}
                                	?>
                                </td>
                                <td align="center" class="normalfnt"></td>
                                <td align="center" class="normalfnt"><?php echo $poNo ?></td>
                                <td align="center" class="normalfnt"><?php echo $row['dtmDate']?> </td>
                                <td align="right" class="normalfnt"><?php echo $row['noOfDays']?></td>
                                <td align="right" class="normalfntRight"><?php echo number_format($amount, 2) ?></td>
                                <td align="right" class="normalfntRight"><?php echo number_format($val, 2) ?></td>
                            </tr>

                         <?PHP
						 			$totalAll+=$amount;
									$totlaToBe+=$val;
								}

                            }catch (Exception $e) {
                                echo "<tr><td colspan=\"300\"><b>". $e->getMessage() ."</b></td></tr>";
                            }
                            ?>
                            <tr>
							<td colspan="7" align="right" class="normalfntRight">Total</td>
                            <td align="right" style="border-right:0px; border-top:1px solid black;"  class="normalfntRight"><?php echo number_format($totalAll, 2) ?></td>
                            <td align="right" style="border-right:0px; border-top:1px solid black;"  class="normalfntRight"><?php echo number_format($totlaToBe, 2) ?></td>                        
                            <?php 
                            	$grandToBePaid = $grandToBePaid + $totlaToBe;
                            	$grandTotal = $grandTotal + $totalAll;
                            ?>                        
                        </tr>
	                        <tr>
								<td colspan="9" align="center"><hr></hr></td>                        
	                        </tr>
                        <tr>
                            <th colspan="9" align="left" class="normalfnt"><strong>61 - 90 Days</strong></th>
                        </tr>
                         <?PHP 
						 			$totalAll=0;
									$totlaToBe=0;
						 try{
							 $sql1="SELECT
									fin_customer_salesinvoice_header.strReferenceNo,
									fin_customer_salesinvoice_header.strPoNo,
									fin_customer_salesinvoice_header.dtmDate, 
									DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate) as noOfDays, 
									round(Sum(fin_customer_salesinvoice_details.dblTaxAmount+fin_customer_salesinvoice_details.dblQty*fin_customer_salesinvoice_details.dblUnitPrice*(100-fin_customer_salesinvoice_details.dblDiscount)/100) ,2) as amount 
									FROM
									fin_customer_salesinvoice_header
									Inner Join fin_customer_salesinvoice_details ON fin_customer_salesinvoice_header.intInvoiceNo = fin_customer_salesinvoice_details.intInvoiceNo
									WHERE
									fin_customer_salesinvoice_header.intCustomerId = '$c_id' AND 
									 DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)>30+2*$creditPeriod AND DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)<=(30+3*$creditPeriod)     AND 
							fin_customer_salesinvoice_header.intCurrencyId='$currency' 
									GROUP BY
									fin_customer_salesinvoice_header.intCustomerId,
									fin_customer_salesinvoice_header.intInvoiceNo";
									$result  = $db->RunQuery($sql1);
									
									while($row = mysqli_fetch_assoc($result)){
										
									 $invNo=$row['strReferenceNo'];
									 $poNo=$row['strPoNo'];
									 $amount=$row['amount'];
									 $nonPaidAmnt=0;	
									  $sqls=" SELECT
											Sum(fin_customer_receivedpayments_main_details.dblToBePaid) as toBePaid 
											FROM
											fin_customer_receivedpayments_main_details
											left Join fin_customer_salesinvoice_header ON fin_customer_receivedpayments_main_details.strDocNo = fin_customer_salesinvoice_header.strReferenceNo
											left Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
											WHERE
											fin_customer_salesinvoice_header.intCustomerId =  '$c_id' AND 
											fin_customer_receivedpayments_main_details.strDocNo =  '$invNo' AND 
											fin_customer_receivedpayments_main_details.strDocType =  'S.Invoice' AND 
									 DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)>30+2*$creditPeriod AND DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)<=(30+3*$creditPeriod)     AND 
							fin_customer_salesinvoice_header.intCurrencyId='$currency' 
											GROUP BY
											fin_customer_salesinvoice_header.intCustomerId, 
											fin_customer_salesinvoice_header.intInvoiceNo";

						 
									$results  = $db->RunQuery($sqls);
									$rows = mysqli_fetch_assoc($results);
									
									if($rows['toBePaid']==''){
										$nonPaidAmnt=$amount;
									}
									$val = $rows['toBePaid']+$nonPaidAmnt;

						 ?>
                         <tr>
                         		<th></th>
                                <td align="center" class="normalfntMid">
                                	 <?php echo "<a href='.../.../../../../../customer/salesInvoice/salesInvoice.php?id=$invNo&currency=$currency' target='_blank'>$invNo</a>"; ?>
                                </td >
                                <td align="center" class="normalfnt">
                                	<?php 
                                		if( $row['JOB_NO']!= 'CS'){
                                			?>
		                                	 <?php echo $row['JOB_NO'] ?>
		                                	 
                                			<?php 
                                		} 
                                		else{
                                			echo $row['JOB_NO'];
                                		}
                                	?>
                                </td>
                                <td align="center" class="normalfnt"></td>
                                <td align="center" class="normalfnt"><?php echo $poNo ?></td>
                                <td align="center" class="normalfnt"><?php echo $row['dtmDate']?> </td>
                                <td align="right" class="normalfnt"><?php echo $row['noOfDays']?></td>
                                <td align="right" class="normalfntRight"><?php echo number_format($amount, 2) ?></td>
                                <td align="right" class="normalfntRight"><?php echo number_format($val, 2) ?></td>
                            </tr>

                         <?PHP
						 			$totalAll+=$amount;
									$totlaToBe+=$val;
								}

                            }catch (Exception $e) {
                                echo "<tr><td colspan=\"300\"><b>". $e->getMessage() ."</b></td></tr>";
                            }
                            ?>
                            <tr>
							<td colspan="7" align="right" class="normalfntRight">Total</td>
                            <td align="right" style="border-right:0px; border-top:1px solid black;"  class="normalfntRight"><?php echo number_format($totalAll, 2) ?></td>
                            <td align="right" style="border-right:0px; border-top:1px solid black;"  class="normalfntRight"><?php echo number_format($totlaToBe, 2) ?></td>                        
                            <?php 
                            	$grandToBePaid = $grandToBePaid + $totlaToBe;
                            	$grandTotal = $grandTotal + $totalAll;
                            ?>                        
                        </tr>
	                        <tr>
								<td colspan="9" align="center"><hr></hr></td>                        
	                        </tr>
                        <tr>
                            <th colspan="9" align="left" class="normalfnt"><strong>More than 90 Days</strong></th>
                        </tr>
                         <?PHP 
						 			$totalAll=0;
									$totlaToBe=0;
						 try{
							 $sql1="SELECT
									fin_customer_salesinvoice_header.strReferenceNo,
									fin_customer_salesinvoice_header.strPoNo,
									fin_customer_salesinvoice_header.dtmDate, 
									DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate) as noOfDays, 
									round(Sum(fin_customer_salesinvoice_details.dblTaxAmount+fin_customer_salesinvoice_details.dblQty*fin_customer_salesinvoice_details.dblUnitPrice*(100-fin_customer_salesinvoice_details.dblDiscount)/100) ,2) as amount 
									FROM
									fin_customer_salesinvoice_header
									Inner Join fin_customer_salesinvoice_details ON fin_customer_salesinvoice_header.intInvoiceNo = fin_customer_salesinvoice_details.intInvoiceNo
									WHERE
									fin_customer_salesinvoice_header.intCustomerId = '$c_id' AND 
									 DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)>30+3*$creditPeriod    AND 
							fin_customer_salesinvoice_header.intCurrencyId='$currency'   
									GROUP BY
									fin_customer_salesinvoice_header.intCustomerId,
									fin_customer_salesinvoice_header.intInvoiceNo";
									$result  = $db->RunQuery($sql1);
									
									while($row = mysqli_fetch_assoc($result)){
										
									 $invNo=$row['strReferenceNo'];
									 $poNo=$row['strPoNo'];
									 $amount=$row['amount'];
									 $nonPaidAmnt=0;	
									  $sqls=" SELECT
											Sum(fin_customer_receivedpayments_main_details.dblToBePaid) as toBePaid 
											FROM
											fin_customer_receivedpayments_main_details
											left Join fin_customer_salesinvoice_header ON fin_customer_receivedpayments_main_details.strDocNo = fin_customer_salesinvoice_header.strReferenceNo
											left Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
											WHERE
											fin_customer_salesinvoice_header.intCustomerId =  '$c_id' AND 
											fin_customer_receivedpayments_main_details.strDocNo =  '$invNo' AND 
											fin_customer_receivedpayments_main_details.strDocType =  'S.Invoice' AND 
									 DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)>30+3*$creditPeriod     AND 
							fin_customer_salesinvoice_header.intCurrencyId='$currency'   
											GROUP BY
											fin_customer_salesinvoice_header.intCustomerId, 
											fin_customer_salesinvoice_header.intInvoiceNo";

						 
									$results  = $db->RunQuery($sqls);
									$rows = mysqli_fetch_assoc($results);
									
									if($rows['toBePaid']==''){
										$nonPaidAmnt=$amount;
									}
									$val = $rows['toBePaid']+$nonPaidAmnt;

						 ?>
                         <tr>
                         		<th></th>
                                <td align="center" class="normalfntMid">
                                	 <?php echo "<a href='.../.../../../../../customer/salesInvoice/salesInvoice.php?id=$invNo&currency=$currency' target='_blank'>$invNo</a>"; ?>
                                </td >
                                <td align="center" class="normalfnt">
                                	<?php 
                                		if( $row['JOB_NO']!= 'CS'){
                                			?>
		                                	 <?php echo $row['JOB_NO'] ?>
		                                	 
                                			<?php 
                                		} 
                                		else{
                                			echo $row['JOB_NO'];
                                		}
                                	?>
                                </td>
                                <td align="center" class="normalfnt"></td>
                                <td align="center" class="normalfnt"><?php echo $poNo ?></td>
                                <td align="center" class="normalfnt"><?php echo $row['dtmDate']?> </td>
                                <td align="right" class="normalfnt"><?php echo $row['noOfDays']?></td>
                                <td align="right" class="normalfntRight"><?php echo number_format($amount, 2) ?></td>
                                <td align="right" class="normalfntRight"><?php echo number_format($val, 2) ?></td>
                            </tr>

                         <?PHP
						 			$totalAll+=$amount;
									$totlaToBe+=$val;
								}

                            }catch (Exception $e) {
                                echo "<tr><td colspan=\"300\"><b>". $e->getMessage() ."</b></td></tr>";
                            }
                            ?>
                            <tr>
							<td colspan="7" align="right" class="normalfntRight">Total</td>
                            <td align="right" style="border-right:0px; border-top:1px solid black;"  class="normalfntRight"><?php echo number_format($totalAll, 2) ?></td>
                            <td align="right" style="border-right:0px; border-top:1px solid black;"  class="normalfntRight"><?php echo number_format($totlaToBe, 2) ?></td>                        
                            <?php 
                            	$grandToBePaid = $grandToBePaid + $totlaToBe;
                            	$grandTotal = $grandTotal + $totalAll;
                            ?>                        
                          </tbody>
                        <tfoot>                            
	                        <tr>
								<td colspan="9" align="center"><hr></hr></td>                        
	                        </tr>
	                       	<tr>
								<td colspan="7" align="right" class="normalfntRight"><strong>Grand Total</strong></td>
	                            <td align="right" style="border-right:0px;  solid black;" class="normalfntRight"><?php echo number_format($grandTotal, 2) ?></td>
	                            <td align="right" style="border-right:0px; solid black;" class="normalfntRight"><?php echo number_format($grandToBePaid, 2) ?></td>                        
	                        </tr>                        
                        </tfoot>
                    </table>
                </td>
            </tr>            
      </table>
    </td>
</tr>
            <tr>
<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid">Printed Date: <?php echo date("Y/m/d") ?></span></td>
</tr>
</table>
</div>
</form>
</body>
</html> 
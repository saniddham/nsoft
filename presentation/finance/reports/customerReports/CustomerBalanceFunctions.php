<?php
function getCustomerCurrentBalance($custId,$toDay,$currency,$companyId){
    $inv=getInvoiceCurrent($custId,$toDay,$currency,$companyId);
    
    return ($inv);
}
function getCustomerBalance($custId,$from,$to,$toDay,$currency,$companyId){
    $inv=getFromInvoice($custId,$from,$to,$toDay,$currency,$companyId);
    $bankDeposit=getBankDeposit($custId,$from,$to,$toDay,$currency,$companyId);
    $bankPayment=getBankPayments($custId,$from,$to,$toDay,$currency,$companyId);
    $pattyCash=getPettyCash($custId,$from,$to,$toDay,$currency,$companyId);
    $jurnal=getJurnelEntry($custId,$from,$to,$toDay,$currency,$companyId);
    return ($inv-$bankDeposit+$bankPayment+$pattyCash+$jurnal);
    
}

function getInvoiceCurrent($custId,$toDay,$currency,$companyId){
    if($companyId==0)$wareCom=" ";
    else $wareCom=" AND fin_customer_salesinvoice_header.intCompanyId=$companyId ";
    global $db;
	$totVal = "";
    
     $sql="SELECT
                                (
				sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) 
				-
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  INV.strReferenceNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'S.Invoice' AND
                                fin_customer_receivedpayments_header.dtmDate<='$toDay'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				
				) AS balAmount
				FROM
				fin_customer_salesinvoice_details AS INV
				Inner Join fin_customer_salesinvoice_header ON fin_customer_salesinvoice_header.strReferenceNo = INV.strReferenceNo AND INV.intInvoiceNo = fin_customer_salesinvoice_header.intInvoiceNo AND INV.intAccPeriodId = fin_customer_salesinvoice_header.intAccPeriodId AND INV.intLocationId = fin_customer_salesinvoice_header.intLocationId AND INV.intCompanyId = fin_customer_salesinvoice_header.intCompanyId
				Inner Join mst_customer ON mst_customer.intId = fin_customer_salesinvoice_header.intCustomerId
				INNER JOIN mst_financepaymentsterms AS PT ON fin_customer_salesinvoice_header.intPaymentsTermsId = PT.intId
				WHERE
				fin_customer_salesinvoice_header.intCustomerId = $custId AND
                                fin_customer_salesinvoice_header.intDeleteStatus = '0' AND
                                fin_customer_salesinvoice_header.intCurrencyId = $currency AND
                                fin_customer_salesinvoice_header.dtmDate <='$toDay' AND
                                DATEDIFF('$toDay',fin_customer_salesinvoice_header.dtmDate) < (PT.strName) ".$wareCom; 
     $result = $db->RunQuery($sql);
   
    $row = mysqli_fetch_array($result);
    //return $row['balAmount']; // --> Commented by Lasntha @ CAIT on 17/12/2012
	//++++++++++++++++++++++++++++++++++++Added by Lasntha @ CAIT on 17/12/2012+++++++++++++++++++++++++++++++
	$totVal = $row['balAmount'];
	
	if($companyId==0)$wareCom=" ";
    else $wareCom=" AND fin_customer_debitnoteinvoice_header.intCompanyId=$companyId ";
	$sql="SELECT
                                (
				sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) 
				-
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  INV.strReferenceNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'D.Invoice' AND
                                fin_customer_receivedpayments_header.dtmDate<='$toDay'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				
				) AS balAmount
				FROM
				fin_customer_debitnoteinvoice_details AS INV
				Inner Join fin_customer_debitnoteinvoice_header ON fin_customer_debitnoteinvoice_header.strReferenceNo = INV.strReferenceNo AND INV.intInvoiceNo = fin_customer_debitnoteinvoice_header.intInvoiceNo AND INV.intAccPeriodId = fin_customer_debitnoteinvoice_header.intAccPeriodId AND INV.intLocationId = fin_customer_debitnoteinvoice_header.intLocationId AND INV.intCompanyId = fin_customer_debitnoteinvoice_header.intCompanyId
				Inner Join mst_customer ON mst_customer.intId = fin_customer_debitnoteinvoice_header.intCustomerId
				INNER JOIN mst_financepaymentsterms AS PT ON fin_customer_debitnoteinvoice_header.intPaymentsTermsId = PT.intId
				WHERE
				fin_customer_debitnoteinvoice_header.intCustomerId = $custId AND
                                fin_customer_debitnoteinvoice_header.intDeleteStatus = '0' AND
                                fin_customer_debitnoteinvoice_header.intCurrencyId = $currency AND
                                fin_customer_debitnoteinvoice_header.dtmDate <='$toDay' AND
                                DATEDIFF('$toDay',fin_customer_debitnoteinvoice_header.dtmDate) < (PT.strName) ".$wareCom; 
     $result = $db->RunQuery($sql);
   
    $row = mysqli_fetch_array($result);
	
	return ($totVal + $row['balAmount']);
	
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
}

function getFromInvoice($custId,$from,$to,$toDay,$currency,$companyId){
    if($to==x)$wareTo=" ";
    else $wareTo=" AND DATEDIFF('$toDay',SIH.dtmDate)<=($to+ PT.strName) ";
    
    if($companyId==0)$wareCom="";
    else $wareCom=" AND SIH.intCompanyId=$companyId ";
    global $db;
    $totVal = "";
	
    $result = $db->RunQuery("SELECT
                                (
                                    sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) 
                                    -
                                    IFNULL ((SELECT
                                    Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
                                    FROM fin_customer_receivedpayments_main_details
                                    Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
                                    WHERE
                                    fin_customer_receivedpayments_main_details.strJobNo =  INV.strReferenceNo AND
                                    fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'S.Invoice' AND
                                    fin_customer_receivedpayments_header.dtmDate<='$toDay'
                                    GROUP BY
                                    fin_customer_receivedpayments_main_details.strJobNo),0)
				) AS balAmount
                                FROM
                                    fin_customer_salesinvoice_details AS INV
                                    INNER JOIN fin_customer_salesinvoice_header AS SIH ON SIH.strReferenceNo = INV.strReferenceNo AND INV.intInvoiceNo = SIH.intInvoiceNo AND INV.intAccPeriodId = SIH.intAccPeriodId AND INV.intLocationId = SIH.intLocationId AND INV.intCompanyId = SIH.intCompanyId
                                    INNER JOIN mst_customer ON mst_customer.intId = SIH.intCustomerId
                                    INNER JOIN mst_financepaymentsterms AS PT ON SIH.intPaymentsTermsId = PT.intId
                                WHERE
                                    SIH.intCustomerId = $custId AND
                                    SIH.intDeleteStatus = '0' AND
                                    SIH.intCurrencyId = $currency AND
                                    SIH.dtmDate<='$toDay' AND
                                    DATEDIFF('$toDay',SIH.dtmDate) >= ($from+ PT.strName)".$wareTo.$wareCom); 
    
    $row = mysqli_fetch_array($result);
	//return $row['balAmount']; // --> Commented by Lasntha @ CAIT on 17/12/2012
	//++++++++++++++++++++++++++++++++++++Added by Lasntha @ CAIT on 17/12/2012+++++++++++++++++++++++++++++++
	$totVal = $row['balAmount'];
	
	if($to==x)$wareTo=" ";
    else $wareTo=" AND DATEDIFF('$toDay',SIH.dtmDate)<=($to+ PT.strName) ";
    
    if($companyId==0)$wareCom="";
    else $wareCom=" AND SIH.intCompanyId=$companyId ";
	 $result = $db->RunQuery("SELECT
                                (
                                    sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) 
                                    -
                                    IFNULL ((SELECT
                                    Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
                                    FROM fin_customer_receivedpayments_main_details
                                    Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
                                    WHERE
                                    fin_customer_receivedpayments_main_details.strJobNo =  INV.strReferenceNo AND
                                    fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'D.Invoice' AND
                                    fin_customer_receivedpayments_header.dtmDate<='$toDay'
                                    GROUP BY
                                    fin_customer_receivedpayments_main_details.strJobNo),0)
				) AS balAmount
                                FROM
                                    fin_customer_debitnoteinvoice_details AS INV
                                    INNER JOIN fin_customer_debitnoteinvoice_header AS SIH ON SIH.strReferenceNo = INV.strReferenceNo AND INV.intInvoiceNo = SIH.intInvoiceNo AND INV.intAccPeriodId = SIH.intAccPeriodId AND INV.intLocationId = SIH.intLocationId AND INV.intCompanyId = SIH.intCompanyId
                                    INNER JOIN mst_customer ON mst_customer.intId = SIH.intCustomerId
                                    INNER JOIN mst_financepaymentsterms AS PT ON SIH.intPaymentsTermsId = PT.intId
                                WHERE
                                    SIH.intCustomerId = $custId AND
                                    SIH.intDeleteStatus = '0' AND
                                    SIH.intCurrencyId = $currency AND
                                    SIH.dtmDate<='$toDay' AND
                                    DATEDIFF('$toDay',SIH.dtmDate) >= ($from+ PT.strName)".$wareTo.$wareCom); 
    
    $row = mysqli_fetch_array($result);
	
	return ($totVal + $row['balAmount']);
	//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
}
function getBankDeposit($custId,$from,$to,$toDay,$currency,$companyId){
    if($to==x)$wareTo="";
    else $wareTo=" AND DATEDIFF('$toDay',BND.dtDate)<=$to ";
    
    if($companyId==0)$wareCom="";
    else $wareCom=" AND BND.intCompanyId=$companyId ";
    global $db;
    
     $result = $db->RunQuery("SELECT
                                (
				sum(fin_bankdeposit_details.dblAmmount )
				+
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  BND.strDepositNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'B.Deposit' AND
                                    fin_customer_receivedpayments_header.dtmDate<='$toDay'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				
				) AS balAmount
				FROM
				fin_bankdeposit_header BND
				Inner Join fin_bankdeposit_details ON fin_bankdeposit_details.strDepositNo = BND.strDepositNo			
				Inner Join mst_financechartofaccounts ON fin_bankdeposit_details.intAccount = mst_financechartofaccounts.intId
				WHERE
				fin_bankdeposit_details.intRecvFrom =  '$custId' AND
				mst_financechartofaccounts.intFinancialTypeId =  '10' AND
				BND.intStatus = '1' AND
                                BND.intCurrency=$currency AND
                                BND.dtDate<='$toDay' AND
                                DATEDIFF('$toDay',BND.dtDate)>= $from".$wareTo.$wareCom);
    $row = mysqli_fetch_array($result);
    return $row['balAmount'];    
}

function getBankPayments($custId,$from,$to,$toDay,$currency,$companyId){
    if($to==x)$wareTo="";
    else $wareTo=" AND DATEDIFF('$toDay',BNP.dtDate)<=$to ";//
    
    if($companyId==0)$wareCom="";
    else $wareCom=" AND BNP.intCompanyId=$companyId ";
    global $db;
    
    $result = $db->RunQuery("SELECT			
				(
				SUM(fin_bankpayment_details.dblAmmount)
				-
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  BNP.strBankPaymentNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'B.Payment' AND
                                    fin_customer_receivedpayments_header.dtmDate<='$toDay'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				
				) AS balAmount
				FROM
				fin_bankpayment_header BNP
				Inner Join fin_bankpayment_details ON fin_bankpayment_details.strBankPaymentNo = BNP.strBankPaymentNo				
				Inner Join mst_financechartofaccounts ON fin_bankpayment_details.intAccountId = mst_financechartofaccounts.intId
				WHERE
				fin_bankpayment_details.intPayTo =  '$custId' AND
				mst_financechartofaccounts.intFinancialTypeId =  '10' AND
				BNP.intStatus = '1' AND
                                BNP.intCurrency=$currency AND
                                BNP.dtDate <='$toDay' AND
                                DATEDIFF('$toDay',BNP.dtDate)>= $from".$wareTo.$wareCom);
    $row = mysqli_fetch_array($result);
    return $row['balAmount'];   
}
function getPettyCash($custId,$from,$to,$toDay,$currency,$companyId){
    if($to==x)$wareTo="";
    else $wareTo=" AND DATEDIFF('$toDay',PTC.dtDate)<=$to ";//
    
    if($companyId==0)$wareCom="";
    else $wareCom=" AND PTC.intCompanyId=$companyId ";
    global $db;
    
    $result = $db->RunQuery("SELECT(
				sum(fin_bankpettycash_details.dblAmmount)
				-
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  PTC.strPettyCashNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'Petty Cash' AND
                                    fin_customer_receivedpayments_header.dtmDate<='$toDay'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				
				) AS balAmount
                            FROM
				fin_bankpettycash_header AS PTC
				Inner Join fin_bankpettycash_details ON fin_bankpettycash_details.strPettyCashNo = PTC.strPettyCashNo
				Inner Join mst_financechartofaccounts ON fin_bankpettycash_details.intAccountId = mst_financechartofaccounts.intId
                            WHERE
				mst_financechartofaccounts.intFinancialTypeId =  '10'
				AND
				fin_bankpettycash_details.intPayTo =  '$custId' AND
				PTC.intStatus = '1' AND
                                PTC.intCurrency=$currency AND
                                PTC.dtDate <='$toDay' AND
                                DATEDIFF('$toDay',PTC.dtDate)>= $from".$wareTo.$wareCom);
    $row = mysqli_fetch_array($result);
    return $row['balAmount'];   
}

function getJurnelEntry($custId,$from,$to,$toDay,$currency,$companyId){
    if($to==x)$wareTo="";
    else $wareTo=" AND DATEDIFF('$toDay', JH.dtmDate)<=$to ";//
    
    if($companyId==0)$wareCom="";
    else $wareCom=" AND JH.intCompanyId=$companyId ";
    global $db;
    
    $result = $db->RunQuery("SELECT 
                            (
                           SUM( (JD.dblDebitAmount) - (JD.dbCreditAmount) ) -
                            IFNULL((SELECT
							Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
							FROM
							fin_customer_receivedpayments_main_details
							Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
							Inner Join fin_accountant_journal_entry_details ON fin_customer_receivedpayments_main_details.strDocNo = fin_accountant_journal_entry_details.strReferenceNo
							WHERE
							fin_customer_receivedpayments_main_details.strJobNo =  'JH.strReferenceNo' AND
							fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND
							fin_customer_receivedpayments_main_details.strDocType =  'JN' AND
							fin_customer_receivedpayments_header.intCustomerId =  '$custId' AND
							fin_customer_receivedpayments_header.dtmDate <=  '$toDay' AND
							fin_accountant_journal_entry_details.strPersonType =  'cus'
							GROUP BY
							fin_customer_receivedpayments_main_details.strJobNo),0)
                            ) AS balAmount
                            FROM
                            fin_accountant_journal_entry_header AS JH
                            INNER JOIN fin_accountant_journal_entry_details AS JD ON JH.strReferenceNo = JD.strReferenceNo                           
                            INNER JOIN mst_financechartofaccounts ON JD.intChartOfAccountId = mst_financechartofaccounts.intId
                            WHERE
                            JD.strPersonType = 'cus' AND
                            JD.intNameId = $custId AND
                            JH.intDeleteStatus = 0 AND
                            mst_financechartofaccounts.intFinancialTypeId = 10 AND
                            JH.intCurrencyId=$currency AND
                            JH.dtmDate <='$toDay' AND
                            DATEDIFF('$toDay', JH.dtmDate)>= $from".$wareTo.$wareCom);
    $row = mysqli_fetch_array($result);
    return $row['balAmount'];   
}
?>

<?php
session_start();

//BEGIN - SET PARAMETERS	{
$backwardseperator = "../../../../";
$companyId 			= $_SESSION['headCompanyId'];
$location 			= $_SESSION['CompanyID'];
$intUser 			= $_SESSION["userId"];

if(!isset($_POST["cboOrder"]))
	$cboOrder		= 1;
else
	$cboOrder		= $_REQUEST["cboOrder"];
	
$cboInvoice			= $_REQUEST["cboInvoice"];
$cboCreditPeriod	= $_REQUEST["cboCreditPeriod"];
//END 	- SET PARAMETERS	}

//BEGIN - INCLUDE FILES {
include  	"{$backwardseperator}dataAccess/Connector.php";
//END 	- INCLUDE FILES }

$lastPaymentRange		= GetLastPaymentRange();
$payment_term_array 	= GetPaymentTerm();
$payment_range 			= GetPaymentTerm_Range($cboCreditPeriod);

$payment_range_array	= explode('-',$payment_range);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Customer Invoice Aging Report</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>
<script type="application/javascript">
function Reload()
{
	document.frmCustomerInvoiceAgingReport.submit();	
}
</script>
</head>
<style type="text/css">
#progress {
 width: 100px;   
 border: 1px solid black;
 position: relative;
 padding: 1px;
}

#percent {
 position: absolute;   
 left: 50%;
}

#bar {
 height: 15px;
 background-color: #0C0;
}
</style>
<body>
<form id="frmCustomerInvoiceAgingReport" name="frmCustomerInvoiceAgingReport" method="post" action="rptCustomer_invoice_aging.php">
  <div align="center">
  <div style="background-color:#FFF" ><strong>Customer Invoice Aging Report</strong><strong></strong></div>
<table width="1100" border="0" align="center" bgcolor="#FFFFFF">
 <tr>
   <td><table width="800" border="0" class="normalfnt">
     <tr>
       <td width="257" style="text-align:center">Order</td>
       <td width="268" style="text-align:center">Invoice</td>
       <td width="261" style="text-align:center">Credit Period</td>
     </tr>
     <tr>
       <td style="text-align:center"><select style="width:200px" name="cboOrder" id="cboOrder" onchange="Reload();">
       		<option value="" <?php echo ($cboOrder==''?'selected=selected':'')?>>ALL</option>
       		<option value="1" <?php echo ($cboOrder=='1'?'selected=selected':'')?>>Order Completed</option>
            <option value="2" <?php echo ($cboOrder=='2'?'selected=selected':'')?>>Pending</option>
         </select></td>
       <td style="text-align:center"><select style="width:200px" name="cboInvoice" id="cboInvoice" onchange="Reload();">
       		<option value="" <?php echo ($cboInvoice==''?'selected=selected':'')?>>ALL</option>
       	 	<option value="1" <?php echo ($cboInvoice=='1'?'selected=selected':'')?>>Balance To Invoice</option>
            <option value="2" <?php echo ($cboInvoice=='2'?'selected=selected':'')?>>Balance Payment Received</option>
         </select></td>
       <td style="text-align:center"><select style="width:200px" name="cboCreditPeriod" id="cboCreditPeriod" onchange="Reload();">
       		<option value="" <?php echo ($cboCreditPeriod==''?'selected=selected':'')?>>All</option>
            <?php 
				$sql = "SELECT intId,strName FROM mst_financepaymentsterms WHERE strName <> 0 ORDER BY strName ";
				$result = $db->RunQuery($sql);
				while($row = mysqli_fetch_array($result))
				{
					if($cboCreditPeriod==$row["intId"])
            			echo "<option value=".$row["intId"]." selected=\"selected\">".$row["strName"]."</option>";
					else
						echo "<option value=".$row["intId"].">".$row["strName"]."</option>";
           		} 
		   ?>
           <option value="More" <?php echo ($cboCreditPeriod=='More'?'selected=selected':'')?>>More</option>
         </select></td>
     </tr>
   </table></td>
 </tr>
 <tr>
  <td><table  border="0" cellpadding="0" cellspacing="0" class="bordered" width="100%">
    <thead>
      <tr valign="bottom">
        <th>Customer Name</th>
        <th>PONo</th>
        <th>Order No</th>
        <th>Order Date</th>
        <th>Invoice No</th>
        <th>Graphic No's</th>
        <th>Order Qty</th>
        <th>Dispatch Qty</th>
        <th>Dispatch Prog</th>
        <th>Rate</th>
        <th>Dispatched Amount</th>
        <th>Invoiced Amount</th>
        <th>Payment Received Amount</th>
        <th>Balance To Invoice</th>
        <th>Balance To Payment Received</th>
        <th>Last Dispatch Date</th>
        <th>Credit Term</th>
        <?php for($i=0;$i<count($payment_term_array);$i++){?>
        <th style="width:40px"><?php echo $payment_term_array[$i]?></th>
        <?php }?>
        </tr>
    </thead>
     <tbody>
<?php
$result = GetMainDetails($cboOrder,$cboInvoice,$cboCreditPeriod);
while($row = mysqli_fetch_array($result))
{
	$progress 			= (round((round($row["DISPATCH_QTY"])/round($row["ORDER_QTY"]))*100,2)>100?100:round((round($row["DISPATCH_QTY"])/round($row["ORDER_QTY"]))*100,2));
?>
      <tr>
        <td nowrap="nowrap" title="Customer Name"><?php echo $row["CUSTOMER_NAME"]?></td>
        <td nowrap="nowrap" title="PONo"><?php echo $row["CUSTOMER_PONO"]?></td>
        <td nowrap="nowrap" title="Order No"><?php echo $row["ORDER_NO"]?></td>
        <td nowrap="nowrap" title="Order Date"><?php echo $row["ORDER_DATE"]?></td>
        <td nowrap="nowrap" title="Invoice No"><?php echo $row["INVOICE_NO"]?></td>
        <td nowrap="nowrap" title="Graphic No's"><?php echo $row["GRAPHIC_NO"]?></td>
        <td nowrap="nowrap" style="text-align:right" title="Order Qty"><?php echo number_format($row["ORDER_QTY"])?></td>
        <td nowrap="nowrap" style="text-align:right" title="Dispatch Qty"><?php echo number_format($row["DISPATCH_QTY"])?></td>
        <td nowrap="nowrap" title="Dispatch Prog"><div id="progress"><span id="percent"><?php echo $progress?>%</span><div id="bar" style="width:<?php echo $progress?>px"></div></div></td>
        <td nowrap="nowrap" style="text-align:right" title="Rate"><?php echo number_format($row["ORDER_RATE"],4)?></td>
        <td nowrap="nowrap" style="text-align:right" title="Dispatched Amount"><?php echo number_format($row["DISPATCH_AMOUNT"])?></td>
        <td nowrap="nowrap" style="text-align:right" title="Invoiced Amount"><?php echo number_format($row["INVOICE_AMOUNT_FINAL"])?></td>
        <td nowrap="nowrap" style="text-align:right" title="Payment Received Amount"><?php echo number_format($row["PAYMENT_RECEIVED_AMOUNT"])?></td>
        <td nowrap="nowrap" style="text-align:right" title="Balance To Invoice"><?php echo number_format($row["BALANCE_TO_INVOICE"])?></td>
        <td nowrap="nowrap" style="text-align:right" title="Balance To Payment Received"><?php echo number_format($row["BALANCE_TO_PAYMENT_RECEIVED"])?></td>
        <td nowrap="nowrap" style="text-align:right" title="Last Dispatch Date"><?php echo $row["DISPATCH_DATE"]?></td>
        <td nowrap="nowrap" style="text-align:right" title="Credit Term"><?php echo $row["PAYMENT_TERM"]?></td>
		<?php  $start = 0;
		if($row["PAYMENT_TERM"]<$row["DIFF_DATES"])
			$agingCSS = "color:#F00;font-weight:bold";
		else
			$agingCSS = "";
			
		for($i=0;$i<count($payment_term_array);$i++){
		$diff_date 	= $row["DIFF_DATES"];
		$aging		= 0;		
		
				if($start < $diff_date && $payment_term_array[$i] >= $diff_date)
				{
					$aging = round($row["DISPATCH_AMOUNT"]) - round($row["INVOICE_AMOUNT_FINAL"]);
					
				}
				else 
					$aging = 0;
			if($agingCSS=="")
				$aging_array_normal[$payment_term_array[$i]] += $aging;	
			else
				$aging_array_due[$payment_term_array[$i]] += $aging;	
				
			$start = $payment_term_array[$i];	
			?>
        <td nowrap="nowrap" style="text-align:right;<?php echo $agingCSS?>" title="<?php echo $payment_term_array[$i]?>"><?php echo $aging==0?'&nbsp;':number_format($aging)?></td>
        <?php }?>
       </tr>
<?php
$tot_order_qty						+= round($row["ORDER_QTY"]);
$tot_dispatch_qty					+= round($row["DISPATCH_QTY"]);
$tot_dispatched_amount 				+= round($row["DISPATCH_AMOUNT"]);
$tot_invoiced_amount 				+= round($row["INVOICE_AMOUNT_FINAL"]);
$tot_payment_received_amount		+= round($row["PAYMENT_RECEIVED_AMOUNT"]);
$tot_credit_note_amount				+= round($row["CREDIT_NOTE_AMOUNT"]);
$tot_balance_to_invoice				+= round($row["BALANCE_TO_INVOICE"]);
$tot_balance_to_payment_received	+= round($row["BALANCE_TO_PAYMENT_RECEIVED"]);
}
?>
	<tr style="font-weight:bold;text-align:right">
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap"><?php echo number_format($tot_order_qty)?></td>
        <td nowrap="nowrap"><?php echo number_format($tot_dispatch_qty)?></td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap"><?php echo number_format($tot_dispatched_amount)?></td>
        <td nowrap="nowrap"><?php echo number_format($tot_invoiced_amount)?></td>
        <td nowrap="nowrap"><?php echo number_format($tot_payment_received_amount)?></td>
        <td nowrap="nowrap"><?php echo number_format($tot_balance_to_invoice)?></td>
        <td nowrap="nowrap"><?php echo number_format($tot_balance_to_payment_received)?></td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
    	<?php for($i=0;$i<count($payment_term_array);$i++){?>
        <td nowrap="nowrap"><?php echo number_format($aging_array_normal[$payment_term_array[$i]])?></td>
        <?php } ?>
      </tr>
      <tr style="color:#F00;font-weight:bold">
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <td nowrap="nowrap">&nbsp;</td>
        <?php for($i=0;$i<count($payment_term_array);$i++){?>
        <td nowrap="nowrap"><?php echo number_format($aging_array_due[$payment_term_array[$i]])?></td>
        <?php } ?>
      </tr>
    </tbody>
  </table></td>
</tr>
            <tr>
<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>
</form>
</body>
<script type="text/javascript">
$(document).ready(function()
{
	$('.bordered tbody tr').click(function(){
		if($(this).attr('style')!='background-color:#82FF82')
		{
			$('.bordered tbody tr').attr('style','background-color:#FFFFFF');  
			   $(this).attr('style','background-color:#82FF82');     
		}                                             
	})
});
</script>
</html>
<?php
function GetMainDetails($cboOrder,$cboInvoice,$cboCreditPeriod)
{
	global $db;
	global $payment_range_array;
	global $lastPaymentRange;
	
	$sql = "SELECT SUB_1.*,
				ROUND((DISPATCH_QTY * ORDER_RATE))												AS DISPATCH_AMOUNT,
				INVOICE_AMOUNT - CREDIT_NOTE_AMOUNT												AS INVOICE_AMOUNT_FINAL,
				ROUND((DISPATCH_QTY * ORDER_RATE)) - (INVOICE_AMOUNT - CREDIT_NOTE_AMOUNT)		AS BALANCE_TO_INVOICE,
				ROUND((DISPATCH_QTY * ORDER_RATE)) - PAYMENT_RECEIVED_AMOUNT					AS BALANCE_TO_PAYMENT_RECEIVED
			FROM
			(SELECT
			  CU.strName														AS CUSTOMER_NAME,
			  OH.dtDate 														AS ORDER_DATE,
			  CONCAT(OH.intOrderYear,' - ',OH.intOrderNo) 						AS ORDER_NO,
			  OH.strCustomerPoNo 												AS CUSTOMER_PONO,
			  SUM(OD.intQty)          											AS ORDER_QTY,
			  ROUND(SUM(OD.intQty * OD.dblPrice),0) 							AS ORDER_AMOUNT,
			  ROUND(SUM(OD.intQty * OD.dblPrice)/SUM(OD.intQty),4) 				AS ORDER_RATE,			  
			  SUBSTRING(GROUP_CONCAT(DISTINCT OD.strGraphicNo),1,20)			AS GRAPHIC_NO,
			  FPT.strName        												AS PAYMENT_TERM,
			  
			  ROUND(COALESCE((SELECT SUM(FDD.dblGoodQty)
			  FROM ware_fabricdispatchdetails FDD 
			  INNER JOIN ware_fabricdispatchheader FDH 
				ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo 
				AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
			  WHERE FDH.intOrderNo = OH.intOrderNo 
			  	AND FDH.intOrderYear = OH.intOrderYear 
				AND FDH.intStatus = 1),0))										AS DISPATCH_QTY,
			  
			  SUBSTRING((SELECT GROUP_CONCAT(DISTINCT CSIH.strReferenceNo)
			   FROM fin_customer_salesinvoice_header CSIH  
			   WHERE CSIH.strPoNo = OH.strCustomerPoNo
				 AND intDeleteStatus = 0),1,20)									AS INVOICE_NO,
   
			 ROUND(COALESCE((SELECT SUM(dblUnitPrice * dblQty)
			   FROM fin_customer_salesinvoice_header CSIH
			   INNER JOIN fin_customer_salesinvoice_details CSID
				 ON CSIH.intInvoiceNo = CSID.intInvoiceNo 
				 AND CSIH.intAccPeriodId = CSID.intAccPeriodId 
				 AND CSIH.intLocationId = CSID.intLocationId 
				 AND CSIH.intCompanyId = CSID.intCompanyId 
				 AND CSIH.strReferenceNo = CSID.strReferenceNo
			   WHERE CSIH.strPoNo = OH.strCustomerPoNo
			   	AND CSIH.intCustomerId = OH.intCustomer
			     AND intDeleteStatus = 0),0))									AS INVOICE_AMOUNT,
				 
			  ROUND(COALESCE((SELECT
				  COALESCE(SUM(dblPayAmount),0)
				FROM fin_customer_receivedpayments_header CRPH
				  INNER JOIN fin_customer_receivedpayments_main_details CRPMD
					ON CRPMD.intReceiptNo = CRPH.intReceiptNo
					  AND CRPH.intAccPeriodId = CRPMD.intAccPeriodId 
					  AND CRPH.intLocationId = CRPMD.intLocationId 
					  AND CRPH.intCompanyId = CRPMD.intCompanyId 
					  AND CRPH.strReferenceNo = CRPMD.strReferenceNo
			   INNER JOIN fin_customer_salesinvoice_header CSIH
			     ON CSIH.strReferenceNo = CRPMD.strDocNo
			   WHERE CSIH.strPoNo = OH.strCustomerPoNo
			   	 AND CSIH.intCustomerId = OH.intCustomer
			     AND CSIH.intDeleteStatus = 0
			     AND CRPH.intDeleteStatus = 0),0))								AS PAYMENT_RECEIVED_AMOUNT,
				 
			ROUND(COALESCE((SELECT
			  SUM(CCD.dblQty * CCD.dblUnitPrice)
			FROM fin_customer_creditnote_header CCH
			  INNER JOIN fin_customer_creditnote_details CCD
				ON CCD.intInvoiceNo = CCH.intInvoiceNo
				  AND CCD.intAccPeriodId = CCH.intAccPeriodId
				  AND CCD.intLocationId = CCH.intLocationId
				  AND CCD.intCompanyId = CCH.intCompanyId
				  AND CCD.strReferenceNo = CCH.strReferenceNo
			  INNER JOIN fin_customer_salesinvoice_header CSIH
				ON CCH.strInvoiceNo = CSIH.strReferenceNo
				AND CCH.intAccPeriodId = CSIH.intAccPeriodId
				AND CCH.intLocationId = CSIH.intLocationId
				AND CCH.intCompanyId = CSIH.intCompanyId
			   WHERE CCH.intDeleteStatus  = 0
				AND CSIH.intDeleteStatus = 0
				AND CSIH.strPoNo = OH.strCustomerPoNo
				AND CSIH.intCustomerId = OH.intCustomer),0))						AS CREDIT_NOTE_AMOUNT,
			   
			   (SELECT
				 DATE(FDH.dtmdate)
			   FROM ware_fabricdispatchdetails FDD
				 INNER JOIN ware_fabricdispatchheader FDH
				   ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo
					 AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
			   WHERE FDH.intOrderNo = OH.intOrderNo
				   AND FDH.intOrderYear = OH.intOrderYear
				   AND FDH.intStatus = 1 
			   ORDER BY DATE(FDH.dtmdate) DESC LIMIT 1)							AS DISPATCH_DATE,
			    
			   DATEDIFF(NOW(),(SELECT
				 DATE(FDH.dtmdate)
			   FROM ware_fabricdispatchdetails FDD
				 INNER JOIN ware_fabricdispatchheader FDH
				   ON FDH.intBulkDispatchNo = FDD.intBulkDispatchNo
					 AND FDH.intBulkDispatchNoYear = FDD.intBulkDispatchNoYear
			   WHERE FDH.intOrderNo = OH.intOrderNo
				   AND FDH.intOrderYear = OH.intOrderYear
				   AND FDH.intStatus = 1 
			   ORDER BY DATE(FDH.dtmdate) DESC LIMIT 1)) 						AS DIFF_DATES
   
			FROM trn_orderheader OH
			INNER JOIN trn_orderdetails OD
				ON OD.intOrderNo = OH.intOrderNo
				AND OD.intOrderYear = OH.intOrderYear
			INNER JOIN mst_customer CU
				ON CU.intId = OH.intCustomer
			INNER JOIN mst_financepaymentsterms FPT
    			ON FPT.intId = OH.intPaymentTerm 
			 WHERE 1 = 1 
			 	AND OH.intStatus <> '-2'
				AND OH.intCustomer NOT IN (181)";
			 
	$sql .= "GROUP BY OH.intOrderYear,OH.intOrderNo
			 HAVING 1=1 ";
	
if($cboOrder=='1')
	$sql .= "AND round((round(DISPATCH_QTY)/round(ORDER_QTY))*100,2) >= 100 ";

if($cboOrder=='2')
	$sql .= "AND round((round(DISPATCH_QTY)/round(ORDER_QTY))*100,2) < 100 ";	
			 
if($cboCreditPeriod!="" && $cboCreditPeriod!="More")
	$sql .= "AND DIFF_DATES BETWEEN $payment_range_array[0] AND $payment_range_array[1] ";

if($cboCreditPeriod=="More")
	$sql .= "AND DIFF_DATES > $lastPaymentRange ";
	

	$sql .= "ORDER BY OH.intOrderYear,OH.intOrderNo) AS SUB_1
	HAVING 1=1 ";
	
if($cboInvoice == '1')
	$sql .= "AND BALANCE_TO_INVOICE > 0 ";

if($cboInvoice == '2')
	$sql .= "AND BALANCE_TO_PAYMENT_RECEIVED > 0 ";
	
			//die($sql);
	return $db->RunQuery($sql);
}

function GetPaymentTerm()
{
	global $db;
	$i = 0;
	$sql = "SELECT DISTINCT
			   strName
			FROM mst_financepaymentsterms
			WHERE strName <> 0 
			ORDER BY strName";
	$result = $db->RunQuery($sql);
	while($row = mysqli_fetch_array($result))
	{
		$new_array[$i++] = $row["strName"];
	}
		$new_array[$i++] = "More";
	return $new_array;
}

function GetPaymentTerm_Range($cboCreditPeriod)
{
	global $db;
	
	$sql = "SELECT
			  strRange
			FROM mst_financepaymentsterms
			WHERE  intId = '$cboCreditPeriod'";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	return $row["strRange"];
}

function GetLastPaymentRange()
{
	global $db;
	$sql = "SELECT DISTINCT
			   strName	AS LAST
			FROM mst_financepaymentsterms
			ORDER BY strName DESC
			LIMIT 1";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	return $row["LAST"];
}
?>
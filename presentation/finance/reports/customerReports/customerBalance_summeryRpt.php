<?php
session_start();
$backwardseperator = "../../../../";
$companyId = $_SESSION['headCompanyId'];
$location = $_SESSION['CompanyID'];
$intUser = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath = $_SERVER['PHP_SELF'];
include "{$backwardseperator}dataAccess/Connector.php";
include "../accountReports/AccReportFunctions.php";

$locationId = $location; //this locationId use in report header(reportHeader.php)--------------------
//--------------------------------------------------------------	

$date = $_REQUEST['date'];
//$date = "$date 23:59:59";
$currency = $_REQUEST['currency'];
$filterCompany = $_REQUEST['company'] ;

//--------------
$arr = json_decode($_REQUEST['arr'], true);
foreach ($arr as $arrVal) {
$customers.="$arrVal,";
}
$customersString = substr($customers, 0, -1);

//--------------
$arr1 = json_decode($_REQUEST['arr1'], true);
$k = 0;
foreach ($arr1 as $arrVal1) {
$k++;
$companies[$k - 1] = $arrVal1;
}

//--------------------------------
$sql = "SELECT DISTINCT
		mst_financecurrency.intId,
		mst_financecurrency.strCode
		FROM mst_financecurrency  
		";
$result = $db->RunQuery($sql);
$a = 0;
while ($row = mysqli_fetch_array($result)) {
$currency[$a] = $row["intId"];
$currencyCode[$a] = $row["strCode"];
$a++;
}
$colspan = count($currency);
//--------------------------------
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Customer Balance Details Report</title>
        <link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
        <script type="application/javascript" src="../accountReports/DrillAccount.js"></script>

        <link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css"/>
        <link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css"/>

        <script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
        <script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
        <script type="application/javascript" src="../../../warehouse/grn/listing/rptGrn-js.js"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

        <script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
        <script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
        <script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>
        <style>
            .break { page-break-before: always; }

            @media print {
                .noPrint 
                {
                    display:none;
                }
            }
            #apDiv1 {
                position:absolute;
                left:252px;
                top:173px;
                width:650px;
                height:322px;
                z-index:1;
            }
            .APPROVE {
                font-size: 18px;
                font-weight: bold;
            }
            .drillLink{
                cursor:pointer;
            }
        </style>
    </head>

    <body>
        <form id="frmGRNApprovalReport" name="frmGRNApprovalReport" method="post" action="customerBalance_summeryRpt.php">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr>
                    <td colspan="3"></td>
                </tr>
                <tr>
                    <td width="20%"></td>
                    <td width="60%" height="80" valign="top"><?php include '../../reportHeader.php' ?></td>
                    <td width="20%"></td>
                </tr>

                <tr>
                    <td colspan="3"></td>
                </tr>
            </table>
            <div align="center">
                <div style="background-color:#FFF" ><strong>Customer Balance Details Report</strong><strong></strong></div>
                <table width="1100" border="0" align="center" bgcolor="#FFFFFF">
                    <tr>
                        <td class="normalfntMid"><strong>ON : </strong><span class="normalfnt"><?php echo $date ?></span></td>
                    </tr>
                    <?php
                    for ($j = 0;$j < count($companies);$j++) {
                        for ($k = 0;$k < count($currency);$k++) {
                            $tmpCust == '';
                            $filterCompany = $companies[$j];
                            $currencyId = $currency[$k];

                            //-------------------------
                            $sql = "SELECT mst_companies.strName FROM mst_companies WHERE mst_companies.intId =  '$filterCompany'";
                            $result = $db->RunQuery($sql);
                            $row = mysqli_fetch_array($result);
                            $filterCompName = $row['strName'];
                            //-------------------------
                            $sql = "SELECT intId,strCode FROM mst_financecurrency WHERE intId = '$currencyId'";
                            $result = $db->RunQuery($sql);
                            $row = mysqli_fetch_array($result);
                            $currencyDesc = $row['strCode'];
                    ?>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td width="4%">&nbsp;</td>
                                    <th width="6%" class="normalfnt"><strong>Currency:</strong></th>
                                    <th width="7%" align="left" class="normalfnt"><?php echo $currencyDesc ?></th>
                                    <?php
                                    if ($filterCompany == '') {
                                    ?>
                                        <td width="21%" class="normalfnt"><strong>All Groups</strong></td>
                                    <?php
                                    } else {
                                    ?>
                                        <td width="25%" class="normalfnt">Company : <?php echo $filterCompName ?></td>
                                    <?php
                                    }
                                    ?>

                                    <td width="10%"><span class="normalfnt"></span></td>
                                    <td width="14%"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table width="100%">
                                <tr>
                                    <td width="5%">&nbsp;</td>
                                    <td colspan="7" class="normalfnt">
                                        <table width="100%" class="grid" id="tblMainGrid" cellspacing="0" cellpadding="0">
                                            <tr class="gridHeader">
                                                <td width="10%" >Customer</td>
                                                <td width="14%" >Transaction Type</td>
                                                <td width="16%" >Date</td>
                                                <td width="19%" >Number</td>
                                                <td width="19%" >Account</td>
                                                <td width="12%" >Memo</td>
                                                <td width="9%" >Debit</td>
                                                <td width="11%" >Credit</td>
                                                <td width="9%" >Balance</td>
                                            </tr>
                                            <?php
                                            foreach ($arr as $arrVal) {
                                                $customer = $arrVal;
                                                //-------------------------
                                                $sql = "SELECT mst_customer.strName FROM mst_customer WHERE mst_customer.intId =  '$customer'  			";
                                                $result = $db->RunQuery($sql);
                                                $row = mysqli_fetch_array($result);
                                                $customerName = $row['strName'];
                                                //-------------------------
                                                $sqlDetails="  SELECT
                                                                fin_transactions_details.`credit/debit`,
                                                                fin_transactions_details.accountId,
                                                                fin_transactions_details.amount,
                                                                fin_transactions_details.details,
                                                                fin_transactions.entryDate,
                                                                fin_transactions.strProgramType,
                                                                fin_transactions.documentNo,
                                                                fin_transactions.entryId
                                                            FROM
                                                                fin_transactions_details
                                                                Inner Join fin_transactions ON fin_transactions_details.entryId = fin_transactions.entryId
                                                            WHERE
                                                                fin_transactions_details.personType =  'cus' AND
                                                                fin_transactions_details.personId =  '$customer' AND
                                                                fin_transactions.currencyId =  '$currencyId' AND
                                                                fin_transactions.companyId =  '$filterCompany' AND
                                                                fin_transactions.authorized =  '1' AND
                                                                fin_transactions.delStatus =  '0' AND
                                                                fin_transactions.entryDate<='$date'
                                                            ORDER BY
                                                                fin_transactions.entryDate ASC,
                                                                fin_transactions.entryId ASC";
                                                    $resultDetails = $db->RunQuery($sqlDetails);
                                                    $rowNum=mysqli_num_rows($resultDetails);
                                                    if($rowNum > 0){
                                                ?>
                                            
                                                        <tr class="normalfnt" bgcolor="#E4E4E4">
                                                            <td class="normalfnt" colspan="9" >&nbsp;<?php echo $customerName ?>&nbsp;</td>
                                                        </tr>
                                           
                                                <?php
                                                
                                                    $closingBal=0;
                                                    $creditTot=0;
                                                    $debitTot=0;
                                                    while($rowDetails = mysqli_fetch_array($resultDetails)){
                                                        if($rowDetails['strProgramType']=='Customer Gain-Loss')  continue;
                                                        $entryId=$rowDetails['entryId'];
                                                        $debitAmount=0;
                                                        $creditAmount=0;
                                                        if($rowDetails['credit/debit']=='D'){
                                                            $debitAmount=$rowDetails['amount'];
                                                            $creditAmount=0;
                                                        }
                                                        else{
                                                            $creditAmount=$rowDetails['amount'];
                                                            $debitAmount=0;
                                                        }    
                                                        $closingBal+=($debitAmount-$creditAmount);
                                                        $creditTot+=$creditAmount;
                                                        $debitTot+=$debitAmount;
                                                ?>
                                                    <tr class="normalfnt" bgcolor="#FFFFFF">
                                                        <td class="normalfnt" ></td>
                                                        <td class="normalfnt" ><?php echo $rowDetails['strProgramType'] ?></td>
                                                        <td class="normalfnt" ><?php echo $rowDetails['entryDate'] ?></td>
                                                        <td class="normalfnt" ><a class="drillLink" onclick="leadgerDrill('<?php echo ($rowDetails['strProgramType']) ?>','<?php echo $rowDetails['documentNo']?>')">&nbsp;<?php echo $rowDetails['documentNo'] ?></a></td>
                                                        <td class="normalfnt" >
                                                            <?php 
                                                            $arOPC=  oppcitAccountsMulty($rowDetails['credit/debit'],$entryId);
                                                            for($i=0; $i<count($arOPC); ++$i){
                                                                echo $i+1;
                                                                echo '.)';
                                                                echo $arOPC[$i].'<br /><br />';
                                                            }
                                                            ?>
                                                        </td>
                                                        <td class="normalfnt" ><?php echo $rowDetails['details'] ?>&nbsp;</td>
                                                        <td class="normalfntRight" ><?php echo number_format($debitAmount,4) ?></td>
                                                        <td class="normalfntRight" ><?php echo number_format($creditAmount,4) ?></td>
                                                        <td class="normalfntRight" ><?php echo number_format($closingBal,4) ?></td>
                                                    </tr>
                                                <?php 
                                                    }
                                                ?>
                                            <tr style="background-color: #CCCCCC; ">
                                                <td colspan="6"></td>
                                                <td class="normalfntRight"><strong><?php echo number_format($debitTot, 4) ?></strong></td>
                                                <td class="normalfntRight"><strong><?php echo number_format($creditTot, 4) ?></strong></td>
                                                <td class="normalfntRight"><strong><?php echo number_format(($debitTot - $creditTot), 4) ?></strong></td>
                                                
                                            </tr>
                                            <?php
                                                }
                                            }
                                            ?> 
                                            
                                        </table>
                                    </td>
                                    <td width="6%">&nbsp;</td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <?php
                    }
                    ?>
                    <tr>
                        <td><hr/></td>
                    </tr>
                    <?php
                    }
                    ?>


                    <tr>
                        <tr height="40">
                            <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
                        </tr>
                </table>
            </div>
            
        </form>
    </body>
</html>
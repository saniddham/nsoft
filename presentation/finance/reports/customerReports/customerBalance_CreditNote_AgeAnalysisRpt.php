<?php
session_start();
$backwardseperator = "../../../../";
$companyId = $_SESSION['headCompanyId'];
$location 	= $_SESSION['CompanyID'];
$intUser  = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
include  	"{$backwardseperator}dataAccess/Connector.php";
				 
	$locationId = $location;//this locationId use in report header(reportHeader.php)--------------------
//--------------------------------------------------------------	
$to_daY = $_REQUEST['to_daY'];
$currency = $_REQUEST['currency'];

//$to_daY=date("Y-m-d");
//$to_daY='2012-05-20';
//$currency=1;
$creditPeriod=30;

//-------------------------
 $sql = "SELECT
		intId,
		strCode
		FROM mst_financecurrency
		WHERE
		intId = '$currency'  
		";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$currencyDesc=$row['strCode'];
//-------------------------
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Customer Balance Report-Credit Note</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />


<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../warehouse/grn/listing/rptGrn-js.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:252px;
	top:173px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<form id="frmGRNApprovalReport" name="frmGRNApprovalReport" method="post" action="customerBalance_summeryRpt.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include '../../reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>Customer Balance Summery (Credit Note) AS AT <?php echo $to_daY ; ?></strong><strong></strong></div>
<table width="1100" border="0" align="center" bgcolor="#FFFFFF">
 <tr>
  <td>
    <table width="100%">
                            <tr>
                            <th></th>
                            <th width="7%" class="normalfnt"><strong>Currency:</strong></th>
                            <th width="67%" align="left" class="normalfnt"><strong><?php echo $currencyDesc ?></strong></th>
                            <th width="3%" class="normalfnt"><strong></strong></th>
                            <th width="3%" class="normalfnt"><strong></strong></th>
                            <th width="3%" class="normalfnt"><strong></strong></th>
                            <th width="3%" class="normalfnt"><strong></strong></th>
                            <th width="3%" class="normalfnt"><strong></strong></th>
                            </tr>
      <tr>
        <td width="5%">&nbsp;</td>
        <td colspan="7" class="normalfnt">
          <table  border="1" cellpadding="2" cellspacing="0" align="center" class="main" width="100%">
                        <thead>
                            <tr>
                        		<th></th>
                                <th><strong>Customer</strong></th>
                                <th><strong>CURRENT</strong></th>
                                <th><strong>0 - 30 Days</strong></th>
                                <th><strong>31 - 60 Days </strong></th>
                                <th><strong>61 - 90 Days</strong></th>
                                <th><strong>More Than<br/>90 Dyas</strong></th>
                                <th><strong>To Be Paid<br/>(Total)</strong></th>
                            </tr>
                        </thead>

                        <tbody>
            <?php
            	$c = 0;
            $sql1 =  "SELECT
			mst_customer.intId,
			mst_customer.strName
			FROM mst_customer" ;
			$result1 = $db->RunQuery($sql1);
			$tmpCust='';
			$closingBal=0;
			$tot=0;
			$totCurrent=0;
			$tot30=0;
			$tot60=0;
			$tot90=0;
			$totMore90=0;
			while($rowAll=mysqli_fetch_array($result1))
			{
                    $c_id = $rowAll['intId'];
                    $c++;
                    $totlaToBe = 0;
            ?>
                        	<tr>
                        		<td><?php echo $c; ?></td>
                        		<td><?php echo $rowAll['strName'] ?></td>
                         <?PHP
						 try{
							  $sqlc=" SELECT
									fin_customer_creditnote_header.intCustomer , 
									fin_customer_receivedpayments_main_details.strDocNo , 
									round(Sum(fin_customer_creditnote_details.dblTax+fin_customer_creditnote_details.dblQty*fin_customer_creditnote_details.dblRate*(100-fin_customer_creditnote_details.dblDiscount)/100) ,2) as toBePaid
									FROM 
									fin_customer_creditnote_header  
									Inner Join fin_customer_creditnote_details ON fin_customer_creditnote_details.strCreditNoteNo = fin_customer_creditnote_header.strCreditNoteNo
									
									left Join fin_customer_receivedpayments_main_details ON fin_customer_receivedpayments_main_details.strDocNo = fin_customer_creditnote_header.strCreditNoteNo and fin_customer_receivedpayments_main_details.strDocType =  'C.Note' 
									WHERE
									fin_customer_receivedpayments_main_details.strDocNo IS NULL AND 
									fin_customer_creditnote_header.intCustomer =  '$c_id'  AND 
									fin_customer_creditnote_header.intCurrency =  '$currency'  AND 
									 DATEDIFF('$to_daY',fin_customer_creditnote_header.dtDate)>=0 AND DATEDIFF('$to_daY',fin_customer_creditnote_header.dtDate)<=(30)
									group by fin_customer_creditnote_header.intCustomer";
									$resultc  = $db->RunQuery($sqlc);
									$rowc = mysqli_fetch_assoc($resultc);
									$nonPaidAmnt=$rowc['toBePaid'];

							 // GET Current Outstanding Total
									 $sql1=" SELECT
											Sum(fin_customer_receivedpayments_main_details.dblToBePaid) as toBePaid 
											FROM
											fin_customer_receivedpayments_main_details
											left Join fin_customer_creditnote_header ON fin_customer_receivedpayments_main_details.strDocNo = fin_customer_creditnote_header.strCreditNoteNo
											left Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
											WHERE
											fin_customer_creditnote_header.intCustomer =  '$c_id' AND 
									fin_customer_creditnote_header.intCurrency =  '$currency'  AND 
											fin_customer_receivedpayments_main_details.strDocType =  'C.Note' AND 
											 DATEDIFF('$to_daY',fin_customer_creditnote_header.dtDate)>=0 AND DATEDIFF('$to_daY',fin_customer_creditnote_header.dtDate)<=(30)
											GROUP BY
											fin_customer_creditnote_header.intCustomer";

						 
									$result  = $db->RunQuery($sql1);
									$row = mysqli_fetch_assoc($result);
									$val = $row['toBePaid']+$nonPaidAmnt;
									echo "<td align='right'><a href='AgeAnalysisCustomerByInvoice_creditNote.php?sub_cat=$c_id&currency=$currency&to_daY=$to_daY' target='_blank'>".number_format($val, 2)."</a></td>";
									$totCurrent+=$val;
									$totlaToBe+=$val;
							
							 // GET 0 - 30 Outstanding Total
							  $sqlc=" SELECT
									fin_customer_creditnote_header.intCustomer , 
									fin_customer_receivedpayments_main_details.strDocNo , 
									round(Sum(fin_customer_creditnote_details.dblTax+fin_customer_creditnote_details.dblQty*fin_customer_creditnote_details.dblRate*(100-fin_customer_creditnote_details.dblDiscount)/100) ,2) as toBePaid
									FROM 
									fin_customer_creditnote_header  
									Inner Join fin_customer_creditnote_details ON fin_customer_creditnote_details.strCreditNoteNo = fin_customer_creditnote_header.strCreditNoteNo
									
									left Join fin_customer_receivedpayments_main_details ON fin_customer_receivedpayments_main_details.strDocNo = fin_customer_creditnote_header.strCreditNoteNo and fin_customer_receivedpayments_main_details.strDocType =  'C.Note' 
									WHERE
									fin_customer_receivedpayments_main_details.strDocNo IS NULL AND 
									fin_customer_creditnote_header.intCustomer =  '$c_id'  AND  
									fin_customer_creditnote_header.intCurrency =  '$currency'  AND 
									 DATEDIFF('$to_daY',fin_customer_creditnote_header.dtDate)>30 AND DATEDIFF('$to_daY',fin_customer_creditnote_header.dtDate)<=(30+$creditPeriod)
									group by fin_customer_creditnote_header.intCustomer";
									$resultc  = $db->RunQuery($sqlc);
									$rowc = mysqli_fetch_assoc($resultc);
									$nonPaidAmnt=$rowc['toBePaid'];
							  $sql1=" SELECT
									fin_customer_receivedpayments_main_details.strDocNo,
									Sum(fin_customer_receivedpayments_main_details.dblAmount),
									Sum(fin_customer_receivedpayments_main_details.dblToBePaid) as toBePaid,
									fin_customer_receivedpayments_main_details.strDocType
									FROM
									fin_customer_receivedpayments_main_details
									Inner Join fin_customer_creditnote_header ON fin_customer_receivedpayments_main_details.strDocNo = fin_customer_creditnote_header.strCreditNoteNo
									Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
									WHERE
									fin_customer_creditnote_header.intCustomer =  '$c_id' AND 
									fin_customer_creditnote_header.intCurrency =  '$currency'  AND 
									fin_customer_receivedpayments_main_details.strDocType =  'C.Note' AND 
									 DATEDIFF('$to_daY',fin_customer_creditnote_header.dtDate)>30 AND DATEDIFF('$to_daY',fin_customer_creditnote_header.dtDate)<=(30+$creditPeriod)
									GROUP BY
									fin_customer_creditnote_header.intCustomer";

									$result  = $db->RunQuery($sql1);
									$row = mysqli_fetch_assoc($result);
									$val = $row['toBePaid']+$nonPaidAmnt;
							echo "<td align='right'><a href='AgeAnalysisCustomerByInvoice_creditNote.php?sub_cat=$c_id&currency=$currency&to_daY=$to_daY' target='_blank'>".number_format($val, 2)."</a></td>";
							$tot30+=$val;
							$totlaToBe+=$val;
							
							 // GET 31 - 60 Outstanding Total
							  $sqlc=" SELECT
									fin_customer_creditnote_header.intCustomer , 
									fin_customer_receivedpayments_main_details.strDocNo , 
									round(Sum(fin_customer_creditnote_details.dblTax+fin_customer_creditnote_details.dblQty*fin_customer_creditnote_details.dblRate*(100-fin_customer_creditnote_details.dblDiscount)/100) ,2) as toBePaid
									FROM 
									fin_customer_creditnote_header  
									Inner Join fin_customer_creditnote_details ON fin_customer_creditnote_details.strCreditNoteNo = fin_customer_creditnote_header.strCreditNoteNo
									
									left Join fin_customer_receivedpayments_main_details ON fin_customer_receivedpayments_main_details.strDocNo = fin_customer_creditnote_header.strCreditNoteNo and fin_customer_receivedpayments_main_details.strDocType =  'C.Note' 
									WHERE
									fin_customer_receivedpayments_main_details.strDocNo IS NULL AND 
									fin_customer_creditnote_header.intCustomer =  '$c_id'  AND 
									fin_customer_creditnote_header.intCurrency =  '$currency'  AND 
									 DATEDIFF('$to_daY',fin_customer_creditnote_header.dtDate)>30+$creditPeriod AND DATEDIFF('$to_daY',fin_customer_creditnote_header.dtDate)<=(30+2*$creditPeriod)
									group by fin_customer_creditnote_header.intCustomer";
									$resultc  = $db->RunQuery($sqlc);
									$rowc = mysqli_fetch_assoc($resultc);
									$nonPaidAmnt=$rowc['toBePaid'];
							$sql1=" SELECT
									fin_customer_receivedpayments_main_details.strDocNo,
									Sum(fin_customer_receivedpayments_main_details.dblAmount),
									Sum(fin_customer_receivedpayments_main_details.dblToBePaid) as toBePaid,
									fin_customer_receivedpayments_main_details.strDocType
									FROM
									fin_customer_receivedpayments_main_details
									Inner Join fin_customer_creditnote_header ON fin_customer_receivedpayments_main_details.strDocNo = fin_customer_creditnote_header.strCreditNoteNo
									Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
									WHERE
									fin_customer_creditnote_header.intCustomer =  '$c_id' AND 
									fin_customer_creditnote_header.intCurrency =  '$currency'  AND 
									fin_customer_receivedpayments_main_details.strDocType =  'C.Note' AND 
									 DATEDIFF('$to_daY',fin_customer_creditnote_header.dtDate)>30+$creditPeriod AND DATEDIFF('$to_daY',fin_customer_creditnote_header.dtDate)<=(30+2*$creditPeriod)
									GROUP BY
									fin_customer_creditnote_header.intCustomer";

									$result  = $db->RunQuery($sql1);
									$row = mysqli_fetch_assoc($result);
									$val = $row['toBePaid']+$nonPaidAmnt;
							echo "<td align='right'><a href='AgeAnalysisCustomerByInvoice_creditNote.php?sub_cat=$c_id&currency=$currency&to_daY=$to_daY' target='_blank'>".number_format($val, 2)."</a></td>";
							$tot60+=$val;
							$totlaToBe+=$val;
														
							 // GET 61 - 90 Outstanding Total
							  $sqlc=" SELECT
									fin_customer_creditnote_header.intCustomer , 
									fin_customer_receivedpayments_main_details.strDocNo , 
									round(Sum(fin_customer_creditnote_details.dblTax+fin_customer_creditnote_details.dblQty*fin_customer_creditnote_details.dblRate*(100-fin_customer_creditnote_details.dblDiscount)/100) ,2) as toBePaid
									FROM 
									fin_customer_creditnote_header  
									Inner Join fin_customer_creditnote_details ON fin_customer_creditnote_details.strCreditNoteNo = fin_customer_creditnote_header.strCreditNoteNo
									
									left Join fin_customer_receivedpayments_main_details ON fin_customer_receivedpayments_main_details.strDocNo = fin_customer_creditnote_header.strCreditNoteNo and fin_customer_receivedpayments_main_details.strDocType =  'C.Note' 
									WHERE
									fin_customer_receivedpayments_main_details.strDocNo IS NULL AND 
									fin_customer_creditnote_header.intCustomer =  '$c_id'  AND 
									fin_customer_creditnote_header.intCurrency =  '$currency'  AND 
									 DATEDIFF('$to_daY',fin_customer_creditnote_header.dtDate)>30+2*$creditPeriod AND DATEDIFF('$to_daY',fin_customer_creditnote_header.dtDate)<=(30+3*$creditPeriod)
									group by fin_customer_creditnote_header.intCustomer";
									$resultc  = $db->RunQuery($sqlc);
									$rowc = mysqli_fetch_assoc($resultc);
									$nonPaidAmnt=$rowc['toBePaid'];
							$sql1=" SELECT
									fin_customer_receivedpayments_main_details.strDocNo,
									Sum(fin_customer_receivedpayments_main_details.dblAmount),
									Sum(fin_customer_receivedpayments_main_details.dblToBePaid) as toBePaid,
									fin_customer_receivedpayments_main_details.strDocType
									FROM
									fin_customer_receivedpayments_main_details
									Inner Join fin_customer_creditnote_header ON fin_customer_receivedpayments_main_details.strDocNo = fin_customer_creditnote_header.strCreditNoteNo
									Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
									WHERE
									fin_customer_creditnote_header.intCustomer =  '$c_id' AND 
									fin_customer_creditnote_header.intCurrency =  '$currency'  AND 
									fin_customer_receivedpayments_main_details.strDocType =  'C.Note' AND 
									 DATEDIFF('$to_daY',fin_customer_creditnote_header.dtDate)>30+2*$creditPeriod AND DATEDIFF('$to_daY',fin_customer_creditnote_header.dtDate)<=(30+3*$creditPeriod)
									GROUP BY
									fin_customer_creditnote_header.intCustomer";

									$result  = $db->RunQuery($sql1);
									$row = mysqli_fetch_assoc($result);
									$val = $row['toBePaid']+$nonPaidAmnt;
							echo "<td align='right'><a href='AgeAnalysisCustomerByInvoice_creditNote.php?sub_cat=$c_id&currency=$currency&to_daY=$to_daY' target='_blank'>".number_format($val, 2)."</a></td>";
							$tot90+=$val;
							$totlaToBe+=$val;
														
							 // GET More Than 90 Outstanding Total
							  $sqlc=" SELECT
									fin_customer_creditnote_header.intCustomer , 
									fin_customer_receivedpayments_main_details.strDocNo , 
									round(Sum(fin_customer_creditnote_details.dblTax+fin_customer_creditnote_details.dblQty*fin_customer_creditnote_details.dblRate*(100-fin_customer_creditnote_details.dblDiscount)/100) ,2) as toBePaid
									FROM 
									fin_customer_creditnote_header  
									Inner Join fin_customer_creditnote_details ON fin_customer_creditnote_details.strCreditNoteNo = fin_customer_creditnote_header.strCreditNoteNo
									
									left Join fin_customer_receivedpayments_main_details ON fin_customer_receivedpayments_main_details.strDocNo = fin_customer_creditnote_header.strCreditNoteNo and fin_customer_receivedpayments_main_details.strDocType =  'C.Note' 
									WHERE
									fin_customer_receivedpayments_main_details.strDocNo IS NULL AND 
									fin_customer_creditnote_header.intCustomer =  '$c_id'  AND 
									fin_customer_creditnote_header.intCurrency =  '$currency'  AND 
									 DATEDIFF('$to_daY',fin_customer_creditnote_header.dtDate)>30+3*$creditPeriod 
									group by fin_customer_creditnote_header.intCustomer";
									$resultc  = $db->RunQuery($sqlc);
									$rowc = mysqli_fetch_assoc($resultc);
									$nonPaidAmnt=$rowc['toBePaid'];
							$sql1=" SELECT
									fin_customer_receivedpayments_main_details.strDocNo,
									Sum(fin_customer_receivedpayments_main_details.dblAmount),
									Sum(fin_customer_receivedpayments_main_details.dblToBePaid) as toBePaid,
									fin_customer_receivedpayments_main_details.strDocType
									FROM
									fin_customer_receivedpayments_main_details
									Inner Join fin_customer_creditnote_header ON fin_customer_receivedpayments_main_details.strDocNo = fin_customer_creditnote_header.strCreditNoteNo
									Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
									WHERE
									fin_customer_creditnote_header.intCustomer =  '$c_id' AND 
									fin_customer_creditnote_header.intCurrency =  '$currency'  AND 
									fin_customer_receivedpayments_main_details.strDocType =  'C.Note' AND 
									 DATEDIFF('$to_daY',fin_customer_creditnote_header.dtDate)>30+3*$creditPeriod 
									GROUP BY
									fin_customer_creditnote_header.intCustomer";

									$result  = $db->RunQuery($sql1);
									$row = mysqli_fetch_assoc($result);
									$val = $row['toBePaid']+$nonPaidAmnt;
							echo "<td align='right'><a href='AgeAnalysisCustomerByInvoice_creditNote.php?sub_cat=$c_id&currency=$currency&to_daY=$to_daY' target='_blank'>".number_format($val, 2)."</a></td>";
							$totMore90+=$val;
							$totlaToBe+=$val;
							?>
							
							<td align='right'><?php echo number_format($totlaToBe, 2);?></td>
							</tr>
							<?php 
							$grandTotal += $totlaToBe;
                            }catch (Exception $e) {
                                echo "<tr><td colspan=\"300\"><b>". $e->getMessage() ."</b></td></tr>";
                            }
                         
       }
   ?>
				            <tr>
				            	<td align="center" > </td>
				            	<td align="center" > </td>
				            	<td align="right"><?php echo number_format($totCurrent, 2);?></td>
				            	<td align="right"><?php echo number_format($tot30, 2);?></td>
				            	<td align="right"><?php echo number_format($tot60, 2);?></td>
				            	<td align="right"><?php echo number_format($tot90, 2);?></td>
				            	<td align="right"><?php echo number_format($totMore90, 2);?></td>
				            	<td align="right"><?php echo number_format($grandTotal, 2);?></td>
				            </tr>
   
            			</tbody>
            		</table>
          </td>
        <td width="6%">&nbsp;</td>
        </tr>
      
      </table>
    </td>
</tr>
            <tr>
<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>
</form>
</body>
</html>
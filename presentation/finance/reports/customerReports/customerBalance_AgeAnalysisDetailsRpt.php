<?php
session_start();
$backwardseperator = "../../../../";
$companyId = $_SESSION['headCompanyId'];
$location 	= $_SESSION['CompanyID'];
$intUser  = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
include  	"{$backwardseperator}dataAccess/Connector.php";
				 
	$locationId = $location;//this locationId use in report header(reportHeader.php)--------------------
//--------------------------------------------------------------	
$to_daY = $_REQUEST['to_daY'];
$filterCompany = $_REQUEST['company'];
$currency = $_REQUEST['currency'];
$arr 		= json_decode($_REQUEST['arr'], true);
foreach($arr as $arrVal)
{
  $customers.="$arrVal,";
}
$customersString = substr($customers,0,-1);

//$to_daY=date("Y-m-d");
//$to_daY='2012-05-20';

//$currency=1;
$creditPeriod=30;

//-------------------------
 $sql = "SELECT
		intId,
		strCode
		FROM mst_financecurrency
		WHERE
		intId = '$currency'  
		";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$currencyDesc=$row['strCode'];
//-------------------------
 $sql = "SELECT
		mst_companies.strName
		FROM mst_companies
		WHERE
		mst_companies.intId =  '$filterCompany'";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$filterCompName=$row['strName'];
//-------------------------

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Customer Balance Report-Sales Invoice</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />


<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../warehouse/grn/listing/rptGrn-js.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:252px;
	top:173px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<form id="frmGRNApprovalReport" name="frmGRNApprovalReport" method="post" action="customerBalance_summeryRpt.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include '../../reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>Customer Balance Summery (Sales Invoice) AS AT <?php echo $to_daY ; ?></strong><strong></strong></div>
<table width="1100" border="0" align="center" bgcolor="#FFFFFF">
 <tr>
  <td>
    <table width="100%">
                            <tr>
                            <th></th>
                            <th width="8%" class="normalfnt"><strong>Currency:</strong></th>
                            <th width="66%" align="left" class="normalfnt"><strong><?php echo $currencyDesc ?></strong></th>
                            <th width="3%" class="normalfnt"><strong></strong></th>
                            <th width="3%" class="normalfnt"><strong></strong></th>
                            <th width="3%" class="normalfnt"><strong></strong></th>
                            <th width="3%" class="normalfnt"><strong></strong></th>
                            <th width="3%" class="normalfnt"><strong></strong></th>
                            <th width="3%" class="normalfnt"><strong></strong></th>
                            </tr>
      <tr>
        <td width="5%">&nbsp;</td>
        <td colspan="7" class="normalfnt">
          <table  border="1" cellpadding="2" cellspacing="0" align="center" class="main" width="100%">
                        <thead>
                            <tr>
                        		<th></th>
                                <th><strong>Customer</strong></th>
                                <th><strong>Invoice No</strong></th>
                                <th><strong>Current</strong></th>
                                <th><strong>0 - 30 Days</strong></th>
                                <th><strong>31 - 60 Days </strong></th>
                                <th><strong>61 - 90 Days</strong></th>
                                <th><strong>More Than<br/>90 Dyas</strong></th>
                                <th><strong>To Be Paid<br/>(Total)</strong></th>
                                <th><strong>No of days</strong></th>
                            </tr>
                        </thead>

                        <tbody>
            <?php
            	$c = 0;
						$sql1 =  "SELECT
						fin_customer_salesinvoice_header.intCustomerId,
						mst_customer.strName,
						fin_customer_salesinvoice_header.strReferenceNo
						FROM
						fin_customer_salesinvoice_header
						Inner Join mst_customer ON fin_customer_salesinvoice_header.intCustomerId = mst_customer.intId 
						Inner Join mst_locations ON fin_customer_salesinvoice_header.intLocationId = mst_locations.intId 
						WHERE  ";
						if($filterCompany!==''){
						$sql1 .=" mst_locations.intCompanyId =  '$filterCompany' AND "; 
						}
						$sql1 .=" mst_customer.intId IN (".$customersString.") 
						ORDER BY
						mst_customer.strName ASC,
						fin_customer_salesinvoice_header.intInvoiceNo ASC" ;
							
							
			$result1 = $db->RunQuery($sql1);
			$tmpCust='';
			$closingBal=0;
			$tot=0;
			$totCurrent=0;
			$tot30=0;
			$tot60=0;
			$tot90=0;
			$totMore90=0;
			$totDays=0;
			$tempCust='';
			
			while($rowAll=mysqli_fetch_array($result1))
			{
                    $c_id = $rowAll['intCustomerId'];
					$invioceNo= $rowAll['strReferenceNo'];
                    $totlaToBe = 0;
            ?>
                        	<tr>
                            <?php
							if($tempCust!=$rowAll['strName']){
							$c++;
							?>
                        		<td><?php echo $c; ?></td>
                        		<td><?php echo $rowAll['strName'] ?></td>
                            <?php
							} else {
							?>
                        		<td></td>
                        		<td></td>
                            <?php
							}
							?>
                                
                        		<td><?php echo $rowAll['strReferenceNo'] ?></td>
                         <?PHP
						 try{
							     $sqlc=" SELECT
									fin_customer_salesinvoice_header.intCustomerId , 
									fin_customer_receivedpayments_main_details.strDocNo, 
									 DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate) as noOfDays, 
									round(Sum(fin_customer_salesinvoice_details.dblTaxAmount+fin_customer_salesinvoice_details.dblQty*fin_customer_salesinvoice_details.dblUnitPrice*(100-fin_customer_salesinvoice_details.dblDiscount)/100) ,2) as toBePaid
									FROM 
									fin_customer_salesinvoice_header  
									Inner Join fin_customer_salesinvoice_details ON fin_customer_salesinvoice_details.intInvoiceNo = fin_customer_salesinvoice_header.intInvoiceNo
									
									left Join fin_customer_receivedpayments_main_details ON fin_customer_receivedpayments_main_details.strDocNo = fin_customer_salesinvoice_header.strReferenceNo and fin_customer_receivedpayments_main_details.strDocType =  'S.Invoice' 
									WHERE
									fin_customer_receivedpayments_main_details.strDocNo IS NULL AND  
									DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)>=0 AND DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)<=(30) AND 
									fin_customer_salesinvoice_header.intCustomerId =  '$c_id'  AND 
									fin_customer_salesinvoice_header.strReferenceNo =  '$invioceNo'  AND 
									fin_customer_salesinvoice_header.intCurrencyId =  '$currency' 
									group by fin_customer_salesinvoice_header.intCustomerId, fin_customer_salesinvoice_header.strReferenceNo";
									$resultc  = $db->RunQuery($sqlc);
									$rowc = mysqli_fetch_assoc($resultc);
									$nonPaidAmnt=$rowc['toBePaid'];
									$nonPaidDays=$rowc['noOfDays'];

							 // GET Current Outstanding Total
									 $sql1=" SELECT
											Sum(fin_customer_receivedpayments_main_details.dblToBePaid) as toBePaid , 
									 DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate) as noOfDays 
											FROM
											fin_customer_receivedpayments_main_details
											left Join fin_customer_salesinvoice_header ON fin_customer_receivedpayments_main_details.strDocNo = fin_customer_salesinvoice_header.strReferenceNo
											left Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
											WHERE
											fin_customer_salesinvoice_header.intCustomerId =  '$c_id' AND 
											fin_customer_salesinvoice_header.strReferenceNo =  '$invioceNo'  AND 
											fin_customer_salesinvoice_header.intCurrencyId =  '$currency' AND 
											fin_customer_receivedpayments_main_details.strDocType =  'S.Invoice' AND 
											 DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)>=0 AND DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)<=(30)
											GROUP BY
											fin_customer_salesinvoice_header.intCustomerId, fin_customer_salesinvoice_header.strReferenceNo";

						 
									$result  = $db->RunQuery($sql1);
									$row = mysqli_fetch_assoc($result);
									$val = $row['toBePaid']+$nonPaidAmnt;
									$paidDays=$row['noOfDays'];
									echo "<td align='right'><a href='AgeAnalysisCustomerByInvoice.php?sub_cat=$c_id&currency=$currency&to_daY=$to_daY' target='_blank'>".number_format($val, 2)."</a></td>";
									$totCurrent+=$val;
									$totlaToBe+=$val;
							
							 // GET 0 - 30 Outstanding Total
							   $sqlc=" SELECT
									fin_customer_salesinvoice_header.intCustomerId , 
									fin_customer_receivedpayments_main_details.strDocNo , 
									 DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate) as noOfDays ,
									round(Sum(fin_customer_salesinvoice_details.dblTaxAmount+fin_customer_salesinvoice_details.dblQty*fin_customer_salesinvoice_details.dblUnitPrice*(100-fin_customer_salesinvoice_details.dblDiscount)/100) ,2) as toBePaid
									FROM 
									fin_customer_salesinvoice_header  
									Inner Join fin_customer_salesinvoice_details ON fin_customer_salesinvoice_details.intInvoiceNo = fin_customer_salesinvoice_header.intInvoiceNo
									
									left Join fin_customer_receivedpayments_main_details ON fin_customer_receivedpayments_main_details.strDocNo = fin_customer_salesinvoice_header.strReferenceNo and fin_customer_receivedpayments_main_details.strDocType =  'S.Invoice' 
									WHERE
									fin_customer_receivedpayments_main_details.strDocNo IS NULL AND  
									 DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)>30 AND DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)<=(30+$creditPeriod) AND 
									fin_customer_salesinvoice_header.intCustomerId =  '$c_id' AND 
									fin_customer_salesinvoice_header.strReferenceNo =  '$invioceNo'  AND 
									fin_customer_salesinvoice_header.intCurrencyId =  '$currency' 
									group by fin_customer_salesinvoice_header.intCustomerId, fin_customer_salesinvoice_header.strReferenceNo";
									$resultc  = $db->RunQuery($sqlc);
									$rowc = mysqli_fetch_assoc($resultc);
									$nonPaidAmnt=$rowc['toBePaid'];
							  $sql1=" SELECT
									fin_customer_receivedpayments_main_details.strDocNo,
									Sum(fin_customer_receivedpayments_main_details.dblAmount),
									Sum(fin_customer_receivedpayments_main_details.dblToBePaid) as toBePaid,
									fin_customer_receivedpayments_main_details.strDocType, 
									 DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate) as noOfDays 
									FROM
									fin_customer_receivedpayments_main_details
									Inner Join fin_customer_salesinvoice_header ON fin_customer_receivedpayments_main_details.strDocNo = fin_customer_salesinvoice_header.strReferenceNo
									Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
									WHERE
									fin_customer_salesinvoice_header.intCustomerId =  '$c_id' AND  
									fin_customer_salesinvoice_header.strReferenceNo =  '$invioceNo'  AND 
									fin_customer_salesinvoice_header.intCurrencyId =  '$currency' AND 
									fin_customer_receivedpayments_main_details.strDocType =  'S.Invoice' AND 
									 DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)>30 AND DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)<=(30+$creditPeriod)
									GROUP BY
									fin_customer_salesinvoice_header.intCustomerId, fin_customer_salesinvoice_header.strReferenceNo";

									$result  = $db->RunQuery($sql1);
									$row = mysqli_fetch_assoc($result);
									$val = $row['toBePaid']+$nonPaidAmnt;
							echo "<td align='right'><a href='AgeAnalysisCustomerByInvoice.php?sub_cat=$c_id&currency=$currency&to_daY=$to_daY' target='_blank'>".number_format($val, 2)."</a></td>";
							$totlaToBe+=$val;
							$tot30+=$val;
							
							 // GET 31 - 60 Outstanding Total
							  $sqlc=" SELECT
									fin_customer_salesinvoice_header.intCustomerId , 
									fin_customer_receivedpayments_main_details.strDocNo , 
									 DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate) as noOfDays ,
									round(Sum(fin_customer_salesinvoice_details.dblTaxAmount+fin_customer_salesinvoice_details.dblQty*fin_customer_salesinvoice_details.dblUnitPrice*(100-fin_customer_salesinvoice_details.dblDiscount)/100) ,2) as toBePaid
									FROM 
									fin_customer_salesinvoice_header  
									Inner Join fin_customer_salesinvoice_details ON fin_customer_salesinvoice_details.intInvoiceNo = fin_customer_salesinvoice_header.intInvoiceNo
									
									left Join fin_customer_receivedpayments_main_details ON fin_customer_receivedpayments_main_details.strDocNo = fin_customer_salesinvoice_header.strReferenceNo and fin_customer_receivedpayments_main_details.strDocType =  'S.Invoice' 
									WHERE
									fin_customer_receivedpayments_main_details.strDocNo IS NULL AND  
									 DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)>30+$creditPeriod AND DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)<=(30+2*$creditPeriod) AND
									fin_customer_salesinvoice_header.intCustomerId =  '$c_id' AND 
									fin_customer_salesinvoice_header.strReferenceNo =  '$invioceNo'  AND 
									fin_customer_salesinvoice_header.intCurrencyId =  '$currency' 
									group by fin_customer_salesinvoice_header.intCustomerId, fin_customer_salesinvoice_header.strReferenceNo";
									$resultc  = $db->RunQuery($sqlc);
									$rowc = mysqli_fetch_assoc($resultc);
									$nonPaidAmnt=$rowc['toBePaid'];
							$sql1=" SELECT
									fin_customer_receivedpayments_main_details.strDocNo,
									Sum(fin_customer_receivedpayments_main_details.dblAmount),
									Sum(fin_customer_receivedpayments_main_details.dblToBePaid) as toBePaid,
									fin_customer_receivedpayments_main_details.strDocType, 
									 DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate) as noOfDays 
									FROM
									fin_customer_receivedpayments_main_details
									Inner Join fin_customer_salesinvoice_header ON fin_customer_receivedpayments_main_details.strDocNo = fin_customer_salesinvoice_header.strReferenceNo
									Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
									WHERE
									fin_customer_salesinvoice_header.intCustomerId =  '$c_id' AND 
									fin_customer_salesinvoice_header.intCurrencyId =  '$currency' AND 
									fin_customer_salesinvoice_header.strReferenceNo =  '$invioceNo'  AND 
									fin_customer_receivedpayments_main_details.strDocType =  'S.Invoice' AND 
									 DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)>30+$creditPeriod AND DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)<=(30+2*$creditPeriod)
									GROUP BY
									fin_customer_salesinvoice_header.intCustomerId, fin_customer_salesinvoice_header.strReferenceNo";

									$result  = $db->RunQuery($sql1);
									$row = mysqli_fetch_assoc($result);
									$val = $row['toBePaid']+$nonPaidAmnt;
							echo "<td align='right'><a href='AgeAnalysisCustomerByInvoice.php?sub_cat=$c_id&currency=$currency&to_daY=$to_daY' target='_blank'>".number_format($val, 2)."</a></td>";
							$totlaToBe+=$val;
							$tot60+=$val;
														
							 // GET 61 - 90 Outstanding Total
							  $sqlc=" SELECT
									fin_customer_salesinvoice_header.intCustomerId , 
									fin_customer_receivedpayments_main_details.strDocNo , 
									 DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate) as noOfDays ,
									round(Sum(fin_customer_salesinvoice_details.dblTaxAmount+fin_customer_salesinvoice_details.dblQty*fin_customer_salesinvoice_details.dblUnitPrice*(100-fin_customer_salesinvoice_details.dblDiscount)/100) ,2) as toBePaid
									FROM 
									fin_customer_salesinvoice_header  
									Inner Join fin_customer_salesinvoice_details ON fin_customer_salesinvoice_details.intInvoiceNo = fin_customer_salesinvoice_header.intInvoiceNo
									
									left Join fin_customer_receivedpayments_main_details ON fin_customer_receivedpayments_main_details.strDocNo = fin_customer_salesinvoice_header.strReferenceNo and fin_customer_receivedpayments_main_details.strDocType =  'S.Invoice' 
									WHERE
									fin_customer_receivedpayments_main_details.strDocNo IS NULL AND  
									 DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)>30+2*$creditPeriod AND DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)<=(30+3*$creditPeriod)  AND
									fin_customer_salesinvoice_header.intCustomerId =  '$c_id'  AND  
									fin_customer_salesinvoice_header.strReferenceNo =  '$invioceNo'  AND 
									fin_customer_salesinvoice_header.intCurrencyId =  '$currency' 
									group by fin_customer_salesinvoice_header.intCustomerId, fin_customer_salesinvoice_header.strReferenceNo";
									$resultc  = $db->RunQuery($sqlc);
									$rowc = mysqli_fetch_assoc($resultc);
									$nonPaidAmnt=$rowc['toBePaid'];
							$sql1=" SELECT
									fin_customer_receivedpayments_main_details.strDocNo,
									Sum(fin_customer_receivedpayments_main_details.dblAmount),
									Sum(fin_customer_receivedpayments_main_details.dblToBePaid) as toBePaid,
									fin_customer_receivedpayments_main_details.strDocType, 
									 DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate) as noOfDays 
									FROM
									fin_customer_receivedpayments_main_details
									Inner Join fin_customer_salesinvoice_header ON fin_customer_receivedpayments_main_details.strDocNo = fin_customer_salesinvoice_header.strReferenceNo
									Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
									WHERE
									fin_customer_salesinvoice_header.intCustomerId =  '$c_id' AND 
									fin_customer_salesinvoice_header.strReferenceNo =  '$invioceNo'  AND 
									fin_customer_salesinvoice_header.intCurrencyId =  '$currency' AND 
									fin_customer_receivedpayments_main_details.strDocType =  'S.Invoice' AND 
									 DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)>30+2*$creditPeriod AND DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)<=(30+3*$creditPeriod)
									GROUP BY
									fin_customer_salesinvoice_header.intCustomerId, fin_customer_salesinvoice_header.strReferenceNo";

									$result  = $db->RunQuery($sql1);
									$row = mysqli_fetch_assoc($result);
									$val = $row['toBePaid']+$nonPaidAmnt;
							echo "<td align='right'><a href='AgeAnalysisCustomerByInvoice.php?sub_cat=$c_id&currency=$currency&to_daY=$to_daY' target='_blank'>".number_format($val, 2)."</a></td>";
							$totlaToBe+=$val;
							$tot90+=$val;
														
							 // GET More Than 90 Outstanding Total
							    $sqlc=" SELECT
									fin_customer_salesinvoice_header.intCustomerId , 
									fin_customer_receivedpayments_main_details.strDocNo, 
									 DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate) as noOfDays, 
									round(Sum(fin_customer_salesinvoice_details.dblTaxAmount+fin_customer_salesinvoice_details.dblQty*fin_customer_salesinvoice_details.dblUnitPrice*(100-fin_customer_salesinvoice_details.dblDiscount)/100) ,2) as toBePaid
									FROM 
									fin_customer_salesinvoice_header  
									Inner Join fin_customer_salesinvoice_details ON fin_customer_salesinvoice_details.intInvoiceNo = fin_customer_salesinvoice_header.intInvoiceNo
									
									left Join fin_customer_receivedpayments_main_details ON fin_customer_receivedpayments_main_details.strDocNo = fin_customer_salesinvoice_header.strReferenceNo and fin_customer_receivedpayments_main_details.strDocType =  'S.Invoice' 
									WHERE
									fin_customer_receivedpayments_main_details.strDocNo IS NULL AND  
									 DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)>30+3*$creditPeriod  AND  
									fin_customer_salesinvoice_header.intCustomerId =  '$c_id'  AND 
									fin_customer_salesinvoice_header.strReferenceNo =  '$invioceNo'  AND 
									fin_customer_salesinvoice_header.intCurrencyId =  '$currency' 
									fin_customer_salesinvoice_header.intCustomerId, fin_customer_salesinvoice_header.strReferenceNo";
									$resultc  = $db->RunQuery($sqlc);
									$rowc = mysqli_fetch_assoc($resultc);
									  $nonPaidAmnt=$rowc['toBePaid'];
							 $sql1=" SELECT
									fin_customer_receivedpayments_main_details.strDocNo,
									Sum(fin_customer_receivedpayments_main_details.dblAmount),
									Sum(fin_customer_receivedpayments_main_details.dblToBePaid) as toBePaid,
									fin_customer_receivedpayments_main_details.strDocType, 
									 DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate) as noOfDays 
									FROM
									fin_customer_receivedpayments_main_details
									Inner Join fin_customer_salesinvoice_header ON fin_customer_receivedpayments_main_details.strDocNo = fin_customer_salesinvoice_header.strReferenceNo
									Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
									WHERE
									fin_customer_salesinvoice_header.intCustomerId =  '$c_id' AND 
									fin_customer_salesinvoice_header.strReferenceNo =  '$invioceNo'  AND 
									fin_customer_salesinvoice_header.intCurrencyId =  '$currency' AND 
									fin_customer_receivedpayments_main_details.strDocType =  'S.Invoice' AND 
									 DATEDIFF('$to_daY',fin_customer_salesinvoice_header.dtmDate)>30+3*$creditPeriod 
									GROUP BY
									fin_customer_salesinvoice_header.intCustomerId, fin_customer_salesinvoice_header.strReferenceNo";

									$result  = $db->RunQuery($sql1);
									$row = mysqli_fetch_assoc($result);
									 $val = $row['toBePaid']+$nonPaidAmnt;
							echo "<td align='right'><a href='AgeAnalysisCustomerByInvoice.php?sub_cat=$c_id&currency=$currency&to_daY=$to_daY' target='_blank'>".number_format($val, 2)."</a></td>";
							$totlaToBe+=$val;
							$totMore90+=$val;
							?>
							
							<td align='right'><?php echo number_format($totlaToBe, 2);?></td>
                            <?php
                            if($nonPaidDays!=''){
                          	 $days=$nonPaidDays;
                            }
                            else{
                          	  $days=$paidDays;
                            }
							
							if($days==''){
							$days=0;
							}
							$totDays+=$days;	
							?>
							<td align='right'><?php echo  $days ;?></td>
							</tr>
							<?php 
							$grandTotal += $totlaToBe;
                            }catch (Exception $e) {
                                echo "<tr><td colspan=\"300\"><b>". $e->getMessage() ."</b></td></tr>";
                            }
							
				$tempCust=$rowAll['strName'];							 
       }
   ?>
				            <tr>
				            	<td align="center" > </td>
				            	<td align="center" > </td>
				            	<td align="center" > </td>
				            	<td align="right"><?php echo number_format($totCurrent, 2);?></td>
				            	<td align="right"><?php echo number_format($tot30, 2);?></td>
				            	<td align="right"><?php echo number_format($tot60, 2);?></td>
				            	<td align="right"><?php echo number_format($tot90, 2);?></td>
				            	<td align="right"><?php echo number_format($totMore90, 2);?></td>
				            	<td align="right"><?php echo number_format($grandTotal, 2);?></td>
				            	<td align="right"><?php //echo $totDays ;?></td>
				            </tr>
   
            			</tbody> 
            		</table>
          </td>
        <td width="6%">&nbsp;</td>
        </tr>
      
      </table>
    </td>
</tr>
            <tr>
<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>
</form>
</body>
</html>
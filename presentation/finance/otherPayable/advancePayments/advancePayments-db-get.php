<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	$company 	= $_SESSION['headCompanyId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	///
	/////////// type of print load part /////////////////////
	if($requestType=='loadCombo')
	{
		$sql = "SELECT DISTINCT
				fin_other_payable_advancepayment_header.strReceiptNo,
				mst_finance_service_supplier.strName,
				IFNULL(CONCAT(' - ',fin_other_payable_advancepayment_header.strPerfInvoiceNo),'') AS refNo
				FROM
				fin_other_payable_advancepayment_header
				Inner Join mst_finance_service_supplier ON fin_other_payable_advancepayment_header.intSupplier = mst_finance_service_supplier.intId
				WHERE
				fin_other_payable_advancepayment_header.intStatus =  '1' AND
				fin_other_payable_advancepayment_header.intCompanyId =  '$company'
				order by 
				fin_other_payable_advancepayment_header.strReceiptNo desc
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['strReceiptNo']."\">".$row['strReceiptNo'].$row['refNo']." (".$row['strName'].") "."</option>";
		}
		echo $html;
	}
	
	//===========Add by dulakshi 2013.03.19=========
	else if($requestType=='loadSupplier')
	{
		$ledAcc  = $_REQUEST['ledgerAcc'];		
							
		$condition == "";
			if($ledAcc != "")
			{	
				$condition .= "AND mst_finance_service_supplier_activate.intChartOfAccountId =  '$ledAcc'";			
			}
				
		$sql = "SELECT
					mst_finance_service_supplier.intId,
					mst_finance_service_supplier.strName,
					mst_finance_service_supplier_activate.intCompanyId
				FROM
					mst_finance_service_supplier
						Inner Join mst_finance_service_supplier_activate ON mst_finance_service_supplier.intId = mst_finance_service_supplier_activate.intSupplierId
				WHERE
					mst_finance_service_supplier.intStatus =  '1' AND
					mst_finance_service_supplier_activate.intCompanyId =  '$company' " .$condition . " order by mst_finance_service_supplier.strName ASC";
				
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
	}
	//==============================================
	
	
	else if($requestType=='loadExchangeRates')
	{
		$currency  = $_REQUEST['currency'];
		$date  = $_REQUEST['date'];
		$sql = "SELECT
				mst_financeexchangerate.dblBuying, 
				mst_financeexchangerate.dblSellingRate  
				FROM mst_financeexchangerate
				WHERE
				mst_financeexchangerate.intCurrencyId =  '$currency' and mst_financeexchangerate.dtmDate =  '$date'";
		$result = $db->RunQuery($sql);
		
		$response['sellingRate'] = '';
		$response['buyingRate'] = '';
		while($row=mysqli_fetch_array($result))
		{
			$response['sellingRate'] = $row['dblSellingRate'];
			$response['buyingRate'] = $row['dblBuying'];
		}
		echo json_encode($response);
	}
	
	
?>
<?php 
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	include "{$backwardseperator}dataAccess/Connector.php";
    include "../../commanFunctions/CommanEditAndDelete.php";
	$response = array('type'=>'', 'msg'=>'');
	$companyId = $_SESSION['headCompanyId'];
	$locationId = $_SESSION["CompanyID"];
	////////////////////////// main parameters ////////////////////////////////
	$invoice		= trim($_REQUEST['txtNo']);
	$requestType 	= $_REQUEST['requestType'];
	$id 			= $_REQUEST['cboSearch'];        
	///////////////////// purchase invoice header parameters ////////////////////
	$supplier		= null(trim($_REQUEST['cboSupplier']));
	$date			= trim($_REQUEST['txtDate']);
	$invoType		= trim(($_REQUEST['invoType']==''?'NULL':"'".$_REQUEST['invoType']."'"));
	$address		= $_REQUEST['txtAddress'];
	$remarks 		= $_REQUEST['txtRemarks'];
	$currency		= null(trim($_REQUEST['cboCurrency']));
	$rate			= val(trim($_REQUEST['txtRate']));
	$poNo			= trim($_REQUEST['txtPoNo']);
	$paymentsTerms	= null(trim($_REQUEST['cboPaymentsTerms']));
	$grnNo			= trim($_REQUEST['txtGrnNo']);
	$message		= $_REQUEST['txtMessage'];
	$automated		= $_REQUEST['automated'];
	$supInvoiceNo 	= trim($invoice);
	///////////////////// purchase invoice detail parameters /////////////////////
	$details = json_decode($_REQUEST['purchaseDetails'], true);
	///////////////////////////////////////////////////////////////////////////
	$supTotalAmount	= $_REQUEST['txtTotal'];
	//////////////////////// purchase invoice insert part ///////////////////////
	if($requestType=='add')
	{
            try{
		$invoiceNumber 		= getNextInvoiceNo($companyId,$locationId);
		$accountPeriod 		= getLatestAccPeriod($companyId);
		$invoiceReference	= trim(encodeInvoiceNo($invoiceNumber,$accountPeriod,$companyId,$locationId,$date));
		$db->begin();
                
                //Add data to transaction header*******************************************
                $sql="INSERT INTO fin_transactions (entryDate, strProgramType, documentNo, currencyId, currencyRate, transDetails, payMethodId, paymentNumber, accPeriod, userId, companyId, createdOn) VALUES
                    ('$date','Other Bill','$supInvoiceNo',$currency,$rate,'$remarks',null,null,$accountPeriod,$userId,$companyId,now())";
                
                $db->RunQuery2($sql);
                $entryId=$db->insertId;                
                //********************************************************************************		
		$sql = "INSERT INTO `fin_other_payable_bill_header` (`intInvoiceNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strReferenceNo`,`intSupplierId`,`dtmDate`,`strInvoiceType`,`strAddress`,`intCurrencyId`,`dblRate`,`strRemark`,`strPoNo`,`intPaymentsTermsId`,`strGrnNo`,`strMessage`,`intCreator`,dtmCreateDate,`intDeleteStatus`,strSupInvoice,entryId) 
				VALUES ('$invoiceNumber','$accountPeriod','$locationId','$companyId','$invoiceReference',$supplier,'$date',$invoType,'$address',$currency,'$rate','$remarks','$poNo',$paymentsTerms,'$grnNo','$message','$userId',now(),'0','$supInvoiceNo',$entryId)";
		$firstResult = $db->RunQuery2($sql);
		
		if(count($details) != 0 && $firstResult)
		{
                    foreach($details as $detail)
                    {
                        //$item 			= $detail['itemId'];
						$items			= $detail['itemId'];
                        $itemDesc		= $detail['itemDesc'];
                        $uom			= null($detail['uomId']);
                        $quantity 		= val($detail['qty']);
                        $unitPrice		= val($detail['unitPrice']);
                        $discount		= val($detail['discount']);
                        $taxAmount		= val($detail['taxAmount']);
                        $taxGroup 		= null($detail['taxGroupId']);
                        $dimension		= null($detail['dimensionId']);
                        $itemAmount		= val($detail['amount']);
                        $taxDetails             = $detail['trnTaxVal'];
						
						list($item, $itmType) = explode('^', $items);
				
				$sql = "INSERT INTO `fin_other_payable_bill_details` (`intInvoiceNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strReferenceNo`,`intItem`,`strItemDesc`,`intUom`,`dblQty`,`dblUnitPrice`,`dblDiscount`,`dblTaxAmount`,`intTaxGroupId`,`intDimensionId`,`strItmType`,`intCreator`,dtmCreateDate) 
				VALUES ('$invoiceNumber','$accountPeriod','$locationId','$companyId','$invoiceReference','$item','$itemDesc',$uom,'$quantity','$unitPrice','$discount','$taxAmount',$taxGroup,$dimension,'$itmType','$userId',now())";
				
				$finalResult = $db->RunQuery2($sql);
				
				//>>>>>>>>>>>>>>>>>>>>>>transaction table process - item>>>>>>>>>>>>>>>>>>>>>>>>>                                
                if($itmType == "Itm")
				{ 
					$sql = "SELECT
					mst_financesupplieritemactivate.intChartOfAccountId
					FROM mst_financesupplieritemactivate
					WHERE
					mst_financesupplieritemactivate.intSupplierItemId = '$item' AND
					mst_financesupplieritemactivate.intCompanyId = '$companyId'";
                                    $result = $db->RunQuery2($sql);
                                    $row = mysqli_fetch_array($result);
                                    $itemAccount = $row['intChartOfAccountId'];
				}
				else if($itmType == "Acc")
				{
					$itemAccount = $item;
				}
			      
                        $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                                        ($entryId,'D',$itemAccount,$itemAmount,'$remarks',$dimension)";
			$trnResult = $db->RunQuery2($sql);
			//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			
			//>>>>>>>>>>>>>>>>>>>>>>>>>>>transaction table process - tax>>>>>>>>>>>>>>>>>>>>>>>>>
			if(count($taxDetails) != 0 && $trnResult)
			{
				foreach($taxDetails as $taxDetail)
				{
					$taxId 	= $taxDetail['taxId'];
					$taxAmount	= $taxDetail['taxValue'];
				
					$sql = "SELECT
					mst_financetaxactivate.intChartOfAccountId
					FROM mst_financetaxactivate
					WHERE
					mst_financetaxactivate.intTaxId = '$taxId' AND
					mst_financetaxactivate.intCompanyId = '$companyId'";
                                        $result = $db->RunQuery2($sql);
                                        $row = mysqli_fetch_array($result);
                                        $taxAccount = $row['intChartOfAccountId'];   
                                        
                                        $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                                            ($entryId,'D',$taxAccount,$taxAmount,'$remarks',null)";
                                        $db->RunQuery2($sql);
				}
			}
			//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			}
			//>>>>>>>>>>>>>>>>>>>>>>transaction table process - supplier>>>>>>>>>>>>>>>>>>>>>>>>>
			$sql = "SELECT
					mst_finance_service_supplier_activate.intChartOfAccountId
					FROM mst_finance_service_supplier_activate
					WHERE
					mst_finance_service_supplier_activate.intSupplierId =  '$supplier' AND
					mst_finance_service_supplier_activate.intCompanyId =  '$companyId'";
				 $result = $db->RunQuery2($sql);
				 $row = mysqli_fetch_array($result);
				 $suptAccount = $row['intChartOfAccountId'];				 
			     
                        $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId,personType, personId) VALUES 
                                ($entryId,'C',$suptAccount,$supTotalAmount,'$remarks',null,'osup',$supplier)";
			$trnResult=$db->RunQuery2($sql);			
			//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		}
		if($finalResult)
		{
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
			$response['invoiceNo'] 	= $invoiceReference;
                        $db->commit();
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
                        $db->rollback();//roalback
		}
                echo json_encode($response);            
            }catch(Exception $e){               
                $db->rollback();//roalback
                $response['type'] 		= 'fail';
                $response['msg'] 		= $e->getMessage();
                $response['q'] 			= $sql;                
               echo json_encode($response);                 
            }
	}
	////////////////////// purchase invoice update part ////////////////////////
	else if($requestType=='edit')
	{
            // ckeck Unrealize gain/loss for entry and if exist block edit and delete
            $chk=checkEditDeleteForUnrealize("osup",$supplier,'O.Bill',$invoice);            
            if($chk){
                $response['type'] = 'fail';
                $response['msg']  = "You cannot allow this process! Invoice has some Unrealize Gain Or Loss";
                echo json_encode($response);
            }
            else{
                try {
                    $db->begin();
		//------------------------------------------------------------------------------------------------------
		$sqlEdit = "SELECT
		fin_other_payable_bill_header.strReferenceNo,
		fin_other_payable_payments_main_details.strDocNo,
		fin_other_payable_payments_main_details.intCompanyId,
		fin_other_payable_payments_main_details.strDocType
		FROM
		fin_other_payable_payments_main_details
		Inner Join fin_other_payable_payments_header ON fin_other_payable_payments_main_details.intReceiptNo = fin_other_payable_payments_header.intReceiptNo AND fin_other_payable_payments_main_details.intAccPeriodId = fin_other_payable_payments_header.intAccPeriodId AND fin_other_payable_payments_main_details.intLocationId = fin_other_payable_payments_header.intLocationId AND fin_other_payable_payments_main_details.intCompanyId = fin_other_payable_payments_header.intCompanyId AND fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
		Inner Join fin_other_payable_bill_header ON fin_other_payable_bill_header.intAccPeriodId = fin_other_payable_payments_main_details.intAccPeriodId AND fin_other_payable_bill_header.intLocationId = fin_other_payable_payments_main_details.intLocationId AND fin_other_payable_bill_header.intCompanyId = fin_other_payable_payments_main_details.intCompanyId AND fin_other_payable_bill_header.strReferenceNo = fin_other_payable_payments_main_details.strDocNo
		WHERE
		fin_other_payable_payments_header.intDeleteStatus =  '0' AND
		fin_other_payable_payments_main_details.strDocType =  'O.Bill' AND
		fin_other_payable_payments_main_details.intCompanyId =  '$companyId' AND
		fin_other_payable_payments_main_details.strDocNo =  '$id'";
				$resultEdit = $db->RunQuery2($sqlEdit);
				if(!mysqli_num_rows($resultEdit))
				{		
                    $sql = "UPDATE `fin_other_payable_bill_header` 
                            SET 
                                intSupplierId	= $supplier,
                                dtmDate				='$date',
                                strInvoiceType		= $invoType,
                                strAddress			='$address',
                                intCurrencyId		= $currency,
                                dblRate				='$rate',
                                strRemark			='$remarks',
                                strPoNo				='$poNo',
                                intPaymentsTermsId	= $paymentsTerms,
                                strGrnNo			='$grnNo',
                                strMessage			='$message',
                                intModifyer			='$userId',
                                intDeleteStatus		= '0',
                                strSupInvoice		= '$supInvoiceNo'
                            WHERE (`strReferenceNo`='$id')";
                    $firstResult = $db->RunQuery2($sql);
                    if(count($details) != 0 && $firstResult)
                    {
                        $sql = "SELECT
                                    fin_other_payable_bill_header.intInvoiceNo,
                                    fin_other_payable_bill_header.intAccPeriodId,
                                    fin_other_payable_bill_header.strReferenceNo,
                                    fin_other_payable_bill_header.entryId
                                    FROM
                                    fin_other_payable_bill_header
                                    WHERE
                                    fin_other_payable_bill_header.strReferenceNo =  '$id'";
                            $result = $db->RunQuery2($sql);
                            while($row=mysqli_fetch_array($result))
                            {
                                    $invoiceNumber 	= $row['intInvoiceNo'];
                                    $accountPeriod 	= $row['intAccPeriodId'];
                                    $entryId= $row['entryId'];                                
                            }
                            //========update the transaction header====================
                            $sql="UPDATE fin_transactions SET 
                                        entryDate='$date',                                                        
                                        currencyId=$currency,
                                        currencyRate='$rate',
                                        transDetails='$remarks',                    
                                        accPeriod=$accountPeriod
                                WHERE entryId=$entryId";
                            $db->RunQuery2($sql);

                            $sqld = "DELETE FROM `fin_transactions_details` WHERE entryId=$entryId";
                            $resultd = $db->RunQuery2($sqld);
                            //=========================================================

                            $sql = "DELETE FROM `fin_other_payable_bill_details` WHERE (`strReferenceNo`='$id')";
                            $db->RunQuery2($sql);			

                            foreach($details as $detail)
                            {
                                    //$item 			= $detail['itemId'];
									$items			= $detail['itemId'];
                                    $itemDesc		= $detail['itemDesc'];
                                    $uom			= null($detail['uomId']);
                                    $quantity 		= val($detail['qty']);
                                    $unitPrice		= val($detail['unitPrice']);
                                    $discount		= val($detail['discount']);
                                    $taxAmount		= val($detail['taxAmount']);
                                    $taxGroup 		= null($detail['taxGroupId']);
                                    $dimension		= null($detail['dimensionId']);
                                    $itemAmount		= val($detail['amount']);
                                    $taxDetails 	= $detail['trnTaxVal'];
									
									list($item, $itmType) = explode('^', $items);

                                    $sql = "INSERT INTO `fin_other_payable_bill_details` (`intInvoiceNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strReferenceNo`,`intItem`,`strItemDesc`,`intUom`,`dblQty`,`dblUnitPrice`,`dblDiscount`,`dblTaxAmount`,`intTaxGroupId`,`intDimensionId`,`strItmType`,`intCreator`,dtmCreateDate) 
                                    VALUES ('$invoiceNumber','$accountPeriod','$locationId','$companyId','$id','$item','$itemDesc',$uom,'$quantity','$unitPrice','$discount','$taxAmount',$taxGroup,$dimension,'$itmType','$userId',now())";

                                    $finalResult = $db->RunQuery2($sql);

                                    //>>>>>>>>>>>>>>>>>>>>>>transaction table process - item>>>>>>>>>>>>>>>>>>>>>>>>>

                                    if($automated=='true'){//Automated invoices
                                        $sql = "SELECT
                                                    mst_financeitemactivate.otherAccountId,
                                                    mst_financeitemactivate.stockAccountId,
                                                    mst_financeitemactivate.itemClassId
                                                FROM
                                                    mst_financeitemactivate
                                                    INNER JOIN mst_item ON mst_financeitemactivate.subCategoryId = mst_item.intSubCategory
                                                WHERE
                                                    mst_financeitemactivate.intCompanyId = '$companyId' AND
                                                    mst_item.intId = '$item'";

                                        $result = $db->RunQuery2($sql);
                                        $row = mysqli_fetch_array($result);
                                        $itemManinClass=$row['itemClassId'];
                                        if($itemManinClass==1){//Inventy item class
                                            $itemAccount = $row['stockAccountId'];
                                        }
                                        else{//Other classes
                                            $itemAccount = $row['otherAccountId'];
                                        }

                                    }
                                    else
									{// fro manual Puechase invoses
										if($itmType == "Itm")
										{
											$sql = "SELECT
														mst_financesupplieritemactivate.intChartOfAccountId
													FROM mst_financesupplieritemactivate
													WHERE
														mst_financesupplieritemactivate.intSupplierItemId = '$item' AND
														mst_financesupplieritemactivate.intCompanyId = '$companyId'";
											$result = $db->RunQuery2($sql);
											$row = mysqli_fetch_array($result);
											$itemAccount = $row['intChartOfAccountId'];
										}
										else if($itmType == "Acc")
										{
											$itemAccount = $item;
										}
                                    }


                                $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                                    ($entryId,'D',$itemAccount,$itemAmount,'$remarks',$dimension)";			
                                $trnResult = $db->RunQuery2($sql);
                            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                            //>>>>>>>>>>>>>>>>>>>>>>>>>>>transaction table process - tax>>>>>>>>>>>>>>>>>>>>>>>>>
                            if(count($taxDetails) != 0 && $trnResult)
                            {
                                foreach($taxDetails as $taxDetail)
                                {
                                    $taxId 	= $taxDetail['taxId'];
                                    $taxAmount	= $taxDetail['taxValue'];

                                    $sql = "SELECT
                                        mst_financetaxactivate.intChartOfAccountId
                                        FROM mst_financetaxactivate
                                        WHERE
                                        mst_financetaxactivate.intTaxId = '$taxId' AND
                                        mst_financetaxactivate.intCompanyId = '$companyId'";
                                    $result = $db->RunQuery2($sql);
                                    $row = mysqli_fetch_array($result);
                                    $taxAccount = $row['intChartOfAccountId'];



                                    $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                                            ($entryId,'D',$taxAccount,$taxAmount,'$remarks',null)";
                                    $db->RunQuery2($sql);
                                }
                            }
                            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                            }
                            //>>>>>>>>>>>>>>>>>>>>>>transaction table process - supplier>>>>>>>>>>>>>>>>>>>>>>>>>
                            $sql = "SELECT
                                            mst_finance_service_supplier_activate.intChartOfAccountId
                                            FROM mst_finance_service_supplier_activate
                                            WHERE
                                            mst_finance_service_supplier_activate.intSupplierId =  '$supplier' AND
                                            mst_finance_service_supplier_activate.intCompanyId =  '$companyId'";
                                    $result = $db->RunQuery2($sql);
                                    $row = mysqli_fetch_array($result);
                                    $suptAccount = $row['intChartOfAccountId'];


                            $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId,personType, personId) VALUES 
                                            ($entryId,'C',$suptAccount,$supTotalAmount,'$remarks',null,'osup',$supplier)";
                            $trnResult = $db->RunQuery2($sql);
                            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    }
                    if($finalResult)
                    {
                        $db->commit();
                        $response['type'] 		= 'pass';
                        $response['msg'] 		= 'Updated successfully.';
                        $response['invoiceNo'] 	= $id;
                    }
                    else
                    {
                        $response['type'] 		= 'fail';
                        $response['msg'] 		= $db->errormsg;
                        $response['q'] 			=$sql;
                        $db->rollback();
                    }
                }
				else
				{
						$db->rollback();
						$response['type'] 		= 'fail';
						$response['msg'] 		= "You cannot allow this process! Invoice has some payements";
				}
				        echo json_encode($response);
				}
				//------------------------------------------------------------------------------------------------------
				catch (Exception $e) 
				{
                    $db->rollback(); //roalback
                    $response['type'] = 'fail';
                    $response['msg'] = $e->getMessage();
                    $response['q'] = $sql;
                    echo json_encode($response);
            }
            }
    }
	/////////// purchase delete part /////////////////////
	else if($requestType=='delete')
	{
            // ckeck Unrealize gain/loss for entry and if exist block edit and delete
            $chk=checkEditDeleteForUnrealize("osup",$supplier,'O.Bill',$invoice);            
            if($chk){
                $response['type'] = 'fail';
                $response['msg']  = "You cannot allow this process! Invoice has some Unrealize Gain Or Loss";
                echo json_encode($response);
            }
            else{
            try
			{
                $db->begin();
				$sql = "SELECT
						fin_other_payable_bill_header.strReferenceNo,
						fin_other_payable_payments_main_details.strDocNo,
						fin_other_payable_payments_main_details.intCompanyId,
						fin_other_payable_payments_main_details.strDocType
						FROM
						fin_other_payable_payments_main_details
						Inner Join fin_other_payable_payments_header ON fin_other_payable_payments_main_details.intReceiptNo = fin_other_payable_payments_header.intReceiptNo AND fin_other_payable_payments_main_details.intAccPeriodId = fin_other_payable_payments_header.intAccPeriodId AND fin_other_payable_payments_main_details.intLocationId = fin_other_payable_payments_header.intLocationId AND fin_other_payable_payments_main_details.intCompanyId = fin_other_payable_payments_header.intCompanyId AND fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
						Inner Join fin_other_payable_bill_header ON fin_other_payable_bill_header.intAccPeriodId = fin_other_payable_payments_main_details.intAccPeriodId AND fin_other_payable_bill_header.intLocationId = fin_other_payable_payments_main_details.intLocationId AND fin_other_payable_bill_header.intCompanyId = fin_other_payable_payments_main_details.intCompanyId AND fin_other_payable_bill_header.strReferenceNo = fin_other_payable_payments_main_details.strDocNo
						WHERE
						fin_other_payable_payments_header.intDeleteStatus =  '0' AND
						fin_other_payable_payments_main_details.strDocType =  'O.Bill' AND
						fin_other_payable_payments_main_details.intCompanyId =  '$companyId' AND
						fin_other_payable_payments_main_details.strDocNo =  '$id'";
				$result = $db->RunQuery2($sql);
				if(!mysqli_num_rows($result))
				{
					$sqlDn = "SELECT
							fin_other_payable_bill_header.strReferenceNo
							FROM
							fin_other_payable_bill_header
							Inner Join fin_supplier_debitnote_header ON fin_other_payable_bill_header.intCompanyId = fin_supplier_debitnote_header.intCompanyId AND fin_other_payable_bill_header.strReferenceNo = fin_supplier_debitnote_header.strInvoiceNo AND fin_other_payable_bill_header.intSupplierId = fin_supplier_debitnote_header.intSupplier
							WHERE
							fin_supplier_debitnote_header.intStatus =  '1' AND
							fin_supplier_debitnote_header.strInvoiceNo =  '$id' AND
							fin_supplier_debitnote_header.intCompanyId =  '$companyId'";
					$resultDn = $db->RunQuery2($sqlDn);
					if(!mysqli_num_rows($resultDn))
					{
						$sql = "UPDATE `fin_other_payable_bill_header` SET intDeleteStatus ='1', intModifyer ='$userId'
								WHERE (`strReferenceNo`='$id')";
						$result = $db->RunQuery2($sql);
						
						//==========UPDATE TRANS ACTION delete STATUS
						$sql="SElect fin_other_payable_bill_header.entryId FROM fin_other_payable_bill_header WHERE (`strReferenceNo`='$id')";
						$result = $db->RunQuery2($sql);
						$row = mysqli_fetch_array($result);
						$entryId=$row['entryId'];                        
						$sqld = "UPDATE `fin_transactions` SET delStatus=1 WHERE entryId=$entryId";
						$resultd = $db->RunQuery2($sqld);
						//============================
						if(($result))
						{
							$db->commit();
							$response['type'] 		= 'pass';
							$response['msg'] 		= 'Deleted successfully.';
						}
						else
						{
							$response['type'] 		= 'fail';
							$response['msg'] 		= $db->errormsg;
							$response['q'] 			=$sql;
							$db->rollback(); //roalback
						}
					}
					else
					{
							$db->rollback();
							$response['type'] 		= 'fail';
							$response['msg'] 		= "You cannot allow this process! Invoice has some debit note";
					}
            	}
				else
				{
						$db->rollback();
						$response['type'] 		= 'fail';
						$response['msg'] 		= "You cannot allow this process! Invoice has some payements";
				}
				echo json_encode($response);
			}
			catch (Exception $e)
		 	{
				$db->rollback(); //roalback
				$response['type'] = 'fail';
				$response['msg'] = $e->getMessage();
				$response['q'] = $sql;
				echo json_encode($response);
			} 
            }
		}
	
//--------------------------------------------------------------------------------------------
	function getNextInvoiceNo($companyId,$locationId)
	{
		global $db;
		$sql = "SELECT
				intOtherBill
				FROM sys_finance_no
				WHERE
				intCompanyId = '$companyId' AND intLocationId = '$locationId'
				";	
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$nextInvoiceNo = $row['intOtherBill'];
		
		$sql = "UPDATE `sys_finance_no` SET intOtherBill=intOtherBill+1 WHERE (intCompanyId = '$companyId' AND intLocationId = '$locationId')";
		$db->RunQuery($sql);
		return $nextInvoiceNo;
	}
//--------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------
	function getLatestAccPeriod($companyId)
	{
		global $db;
		$sql = "SELECT
				MAX(mst_financeaccountingperiod.intId) AS accId,
				mst_financeaccountingperiod.dtmStartingDate,
				mst_financeaccountingperiod.dtmClosingDate,
				mst_financeaccountingperiod.intStatus,
				mst_financeaccountingperiod_companies.intCompanyId,
				mst_financeaccountingperiod_companies.intPeriodId
				FROM
				mst_financeaccountingperiod
				Inner Join mst_financeaccountingperiod_companies ON mst_financeaccountingperiod_companies.intPeriodId = mst_financeaccountingperiod.intId
				WHERE
				mst_financeaccountingperiod_companies.intCompanyId =  '$companyId' AND mst_financeaccountingperiod.intStatus = '1'
				ORDER BY
				mst_financeaccountingperiod.intId DESC
				";	
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$latestAccPeriodId = $row['accId'];	
		return $latestAccPeriodId;
	}
//--------------------------------------------------------------------------------------------
//============================================================================================
	function encodeInvoiceNo($invoiceNumber,$accountPeriod,$companyId,$locationId,$date)
	{
		global $db;
		$sql = "SELECT
				mst_financeaccountingperiod.intId,
				mst_financeaccountingperiod.dtmStartingDate,
				mst_financeaccountingperiod.dtmClosingDate,
				mst_financeaccountingperiod.intStatus
				FROM
				mst_financeaccountingperiod
				WHERE
				mst_financeaccountingperiod.intId =  '$accountPeriod'
				";	
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		if($row['dtmStartingDate'] <= $date && $date <= $row['dtmClosingDate'])
		{
			$startDate = substr($row['dtmStartingDate'],0,4);
			$closeDate = substr($row['dtmClosingDate'],0,4);
		}
		else
		{
				$sql = "SELECT
					mst_financeaccountingperiod.intId,
					mst_financeaccountingperiod.dtmStartingDate,
					mst_financeaccountingperiod.dtmClosingDate,
					mst_financeaccountingperiod.intStatus
					FROM
					mst_financeaccountingperiod
					WHERE
					mst_financeaccountingperiod.dtmStartingDate <= '$date' AND
					mst_financeaccountingperiod.dtmClosingDate >=  '$date'
					";	
				$result = $db->RunQuery($sql);
				$row = mysqli_fetch_array($result);
				$startDate = substr($row['dtmStartingDate'],0,4);
				$closeDate = substr($row['dtmClosingDate'],0,4);
		}
		$sql = "SELECT
				mst_companies.strCode AS company,
				mst_companies.intId,
				mst_locations.intCompanyId,
				mst_locations.strCode AS location,
				mst_locations.intId
				FROM
				mst_companies
				Inner Join mst_locations ON mst_locations.intCompanyId = mst_companies.intId
				WHERE
				mst_locations.intId =  '$locationId' AND
				mst_companies.intId =  '$companyId'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$companyCode = $row['company'];
		$locationCode = $row['location'];
		$invoiceFormat = $companyCode."/".$locationCode."/".$startDate."-".$closeDate."/".$invoiceNumber;
		return $invoiceFormat;
	}
//============================================================================================
?>
<?php
session_start();
$backwardseperator = "../../../../";
$thisFilePath =  $_SERVER['PHP_SELF'];

include  "{$backwardseperator}dataAccess/permisionCheck2.inc";

//$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
//$result = $db->RunQuery($sql);
//while($row=mysqli_fetch_array($result))
//{
//	$companyId = $row['intCompanyId']; 
//}

$companyId 	= $_SESSION['headCompanyId'];

if($_SERVER['REQUEST_METHOD'] == "POST")
{
	$optionValue = trim($_REQUEST['cboSearchOption']);
	$searchValue = trim($_REQUEST['txtSearchValue']);
}

//---------------------------------------------New Grid-----------------------------------------------------
include_once "../../commanFunctions/jqgrid_dist.php";

					$sql = "select *,if(t.balAmount=Amount,'Pending',if(t.balAmount>0,'Part',if(t.balAmount=0,'Completed',if(t.balAmount<0,'Over','Pending')))) as Status from(SELECT
					fin_other_payable_bill_header.strReferenceNo AS InvoiceNumber , 'More' AS More,
					fin_other_payable_bill_header.strSupInvoice AS SupInvoice,
					fin_other_payable_bill_header.intSupplierId,
					INV.dblQty,
					INV.dblUnitPrice,
					INV.dblDiscount,
					INV.dblTaxAmount,
					Round(Sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)),4) AS Amount,
					mst_finance_service_supplier.strName AS Supplier,
					mst_financecurrency.strCode AS Currency,
					fin_other_payable_bill_header.strPoNo AS PONumber,
					fin_other_payable_bill_header.strGrnNo AS GrnNo,
					fin_other_payable_bill_header.dtmDate AS Date,
					fin_other_payable_bill_header.strRemark AS Memo,
					fin_other_payable_bill_header.intInvoiceNo,
					Round((
						sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) 
						-
						IFNULL ((SELECT
						Sum(fin_other_payable_payments_main_details.dblPayAmount )AS paidAmount
						FROM fin_other_payable_payments_main_details
						Inner Join fin_other_payable_payments_header ON fin_other_payable_payments_main_details.strReferenceNo = 				fin_other_payable_payments_header.strReferenceNo
						WHERE
						fin_other_payable_payments_main_details.strDocNo =  INV.strReferenceNo AND
						fin_other_payable_payments_header.intDeleteStatus =  '0' AND fin_other_payable_payments_main_details.strDocType = 'O.Bill'
						GROUP BY
						fin_other_payable_payments_main_details.strDocNo),0)
						
						),4) AS balAmount
						FROM
						fin_other_payable_bill_details AS INV
						Inner Join fin_other_payable_bill_header ON fin_other_payable_bill_header.strReferenceNo = INV.strReferenceNo AND INV.intInvoiceNo = fin_other_payable_bill_header.intInvoiceNo AND INV.intAccPeriodId = fin_other_payable_bill_header.intAccPeriodId AND INV.intCompanyId = fin_other_payable_bill_header.intCompanyId
						Inner Join mst_finance_service_supplier ON mst_finance_service_supplier.intId = fin_other_payable_bill_header.intsupplierId
						Inner Join mst_financecurrency ON mst_financecurrency.intId = fin_other_payable_bill_header.intCurrencyId
						WHERE
						fin_other_payable_bill_header.intDeleteStatus =  '0' AND fin_other_payable_bill_header.intCompanyId = '$companyId'
						GROUP BY
						fin_other_payable_bill_header.strReferenceNo
						) as t where 1=1"; // AND INV.intLocationId = fin_other_payable_bill_header.intLocationId

$invoiceLink = "otherBill.php?id={InvoiceNumber}";
$invoiceReport = "otherBillDetails.php?id={InvoiceNumber}";

//STATUS
$col["title"] 	= "Pay. Status"; // caption of column
$col["name"] 	= "Status"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
$col["align"] 	= "center";
$col["search"] = false;
$cols[] = $col;	$col=NULL;

//Customer
$col["title"] = "Supplier"; // caption of column
$col["name"] = "Supplier"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "6";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//PO Number
$col["title"] = "P.O. Number"; // caption of column
$col["name"] = "PONumber"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Marketer
$col["title"] = "G.R.N Number"; // caption of column
$col["name"] = "GrnNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Date
$col["title"] = "Date"; // caption of column
$col["name"] = "Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Currency
$col["title"] = "Currency"; // caption of column
$col["name"] = "Currency"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Memo
$col["title"] = "Memo"; // caption of column
$col["name"] = "Memo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Invoice Number
$col["title"] = "Invoice Number"; // caption of column
$col["name"] = "SupInvoice"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "center";
$col['link']	= $invoiceLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

//Invoice Number - Reference
$col["title"] = "Our Reference Number"; // caption of column
$col["name"] = "InvoiceNumber"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "8";
$col["align"] = "center";
$col['link']	= $invoiceLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
//$col["hidden"] = true;
$cols[] = $col;	$col=NULL;

//Amount
$col["title"] = "Amount"; // caption of column
$col["name"] = "Amount"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "right";
$col['link']	= $invoiceLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

//To be Paid
$col["title"] = "To be Paid"; // caption of column
$col["name"] = "balAmount"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "right";
$col['link']	= $invoiceLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

//View
$col["title"] = "View"; // caption of column
$col["name"] = "More"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "1";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $invoiceReport;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

$jq = new jqgrid('',$db);

$grid["caption"] 		= "Other Payable / Service Supplier - Bill Listing";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'intInvoiceNo'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes

//<><><><><><>======================Compulsory Code==============================<><><><><><>
	$jq->set_options($grid);
	$jq->select_command = $sql;
	$jq->set_columns($cols);
	$jq->set_actions(array(	
		"add"=>false, // allow/disallow add
		"edit"=>false, // allow/disallow edit
		"delete"=>false, // allow/disallow delete
		"rowactions"=>false, // show/hide row wise edit/del/save option
		"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
		"export"=>true
	) 
	);

$out = $jq->render("list1");
//<><><><><><>======================Compulsory Code==============================<><><><><><>

//---------------------------------------------New Grid-----------------------------------------------------

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Other Payable / Service Supplier -  Bill Listing</title>

<!--<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/tblstyle.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../libraries/jquery/jquery-1.3.2.min.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/scrolltable.js"></script>

<script type="application/javascript" src="../../../../libraries/javascript/zebraStripe.js"></script>-->

<script type="text/javascript" language="javascript">
function pageSubmit()
{
	document.getElementById('frmOtherBillListing').submit();	
}
function clearValue()
{
	document.getElementById('txtSearchValue').value = '';
	document.getElementById('txtSearchValue').focus();
}
</script>

</head>

<body>
<form id="frmOtherBillListing" name="frmOtherBillListing" method="post" autocomplete="off" action="otherBillListing.php">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
<div align="center">

<!-------------------------------------------New Grid---------------------------------------------------->
<link rel="stylesheet" type="text/css" media="screen" href="../../../../libraries/jqdrid/js/themes/smoothness/jquery-ui.custom.css"></link>	
<link rel="stylesheet" type="text/css" media="screen" href="../../../../libraries/jqdrid/js/jqgrid/css/ui.jqgrid.css"></link>	

<script src="../../../../libraries/jqdrid/js/jquery.min.js" type="text/javascript"></script>
<script src="../../../../libraries/jqdrid/js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
<script src="../../../../libraries/jqdrid/js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>	
<script src="../../../../libraries/jqdrid/js/themes/jquery-ui.custom.min.js" type="text/javascript"></script>
<script src="../../../../libraries/javascript/script.js" type="text/javascript"></script>
<!-------------------------------------------New Grid---------------------------------------------------->
		<!--<div class="trans_layoutL">
		  <div class="trans_text">Other Payable / Service Supplier - Bill Listing</div>-->
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <!--<tr>
        <td><table width="100%" border="0" cellpadding="1" cellspacing="0" class="tableBorder_allRound">
          <tr>
            <td width="16%" height="22" class="normalfnt">&nbsp;</td>
            <td width="18%" class="normalfntMid">Searching Option</td>
            <td width="17%" class="normalfnt">
            <select name="cboSearchOption" id="cboSearchOption" onchange="clearValue();" style="width:150px">
            <option value="">&nbsp;</option>
<option value="fin_other_payable_bill_header.strSupInvoice" <?php echo($optionValue=='fin_other_payable_bill_header.strSupInvoice'?'selected':'') ?>>Invoice Number</option>
<option value="mst_finance_service_supplier.strName">Other Payable / Service Supplier</option>
<option value="fin_other_payable_bill_header.strPoNo">P.O. Number</option>
<option value="fin_other_payable_bill_header.strGrnNo">G.R.N. Number</option>
            </select></td>
            <td width="16%"><input value="<?php echo $searchValue; ?>" type="text" name="txtSearchValue" id="txtSearchValue" /></td>
            <td width="18%" class="normalfnt">
            <a href="#">
            <img src="../../../../images/search.png" width="20" height="20" onclick="pageSubmit();" /></a></td>
            <td width="15%">&nbsp;</td>
            </tr>
          <tr>
            <td height="22" class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            </tr>
          </table></td>
      </tr>-->
      <!--<tr>
        <td>
          <div id="_table_wrap" style="overflow: hidden; display: inline-block; border: 1px solid rgb(136, 136, 136);">
          <div id="_head_wrap" style="width: 900px; overflow: hidden;">
          <table class="tbl1 scroll" id="_head" style="table-layout: fixed;">
          <thead>
            <tr class="gridHeader">
              <th height="22" style="width: 180px;" >Other Payable / Service Supplier</th>
              <th style="width: 80px;" >P.O. Number</th>
              <th style="width: 80px;" >G.R.N No.</th>
              <th style="width: 80px;" >Invoice Number</th>
              <th style="width: 38px;" >View</th>
              <th style="width: 9px;" ></th>
              </tr>
           </table>
           </div>
           <div class="normalfnt" id="_body_wrap" style="width: 900px; height: 320px; overflow: scroll;">
           <table class="tbl1 scroll"> 
           <tbody>
              <?php

				if($searchValue!='')
			  	$wherePart = " and $optionValue like '%$searchValue%'";
				
	 	 		$sql = "SELECT
						fin_other_payable_bill_header.intInvoiceNo,
						fin_other_payable_bill_header.intAccPeriodId,
						fin_other_payable_bill_header.intLocationId,
						fin_other_payable_bill_header.intCompanyId,
						fin_other_payable_bill_header.intSupplierId,
						mst_finance_service_supplier.intId,
						mst_finance_service_supplier.strName,
						fin_other_payable_bill_header.strPoNo,
						fin_other_payable_bill_header.strGrnNo,
						fin_other_payable_bill_header.strReferenceNo,
						fin_other_payable_bill_header.strSupInvoice
						FROM
						fin_other_payable_bill_header
						Left Outer Join mst_finance_service_supplier ON fin_other_payable_bill_header.intSupplierId = mst_finance_service_supplier.intId
						WHERE
						fin_other_payable_bill_header.intCompanyId = '$companyId' AND
						fin_other_payable_bill_header.intDeleteStatus =  '0'
						$wherePart
						ORDER BY
						mst_finance_service_supplier.strName ASC";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$id 		=	 $row['strReferenceNo'];
	  			 ?>
           		<tr class="normalfnt">
           		<td class="prInvoCusName" align="center" bgcolor="#FFFFFF"><?php echo $row['strName'];?></td>
              	<td class="prInvoPo"  align="center" bgcolor="#FFFFFF"><?php echo $row['strPoNo'];?></td>
              	<td class="prInvoGrn" bgcolor="#FFFFFF"><?php echo $row['strGrnNo'];?></td>
              	<td class="prInvoRef" align="center" bgcolor="#FFFFFF">
                <a target="_blank" href="<?php echo "otherBill.php?id=$id";?>">
				<?php echo $row['strSupInvoice'];?></a></td>
              	<td class="prInvoView" align="center" bgcolor="#FFFFFF">
                <a target="_blank" href="<?php echo "otherBillDetails.php?id=$id";?>">More</a></td>
              </tr>
               <?php 
        	    } 
       		   ?>
              </tbody>
             </table>
            </div></div>
              </td>
              </tr>-->
            </table>
			</td>
      </tr>
      <tr>
        <td>
        <div align="center" style="margin:10px">
			<?php echo $out?>
        </div>
        </td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound"><a href="../../../../main.php"><img  src="../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
      </tr>
    </table></td>
    </tr>
  </table>

  <!--</div>-->
  </div>
</form>
</body>
</html>

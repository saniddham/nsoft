// JavaScript Document
function functionList()
{
    if(invoRefNo!='')
    {
        $('#frmOtherBill #cboSearch').val(invoRefNo);
        $('#frmOtherBill #cboSearch').change();
    }
}
var rows = 1;
var invoType = "";
var trnTax = "";
function insertRow()
{
    var tbl = document.getElementById('tblMainGrid2');	
    rows = tbl.rows.length;
    //tbl.rows[1].cells[5].childNodes[0].nodeValue;
    tbl.insertRow(rows);
    tbl.rows[rows].innerHTML = tbl.rows[rows-1].innerHTML;
//loadJs();
}
$(document).ready(function() {
	
	// TODO: ===========Add by dulaskshi 2013.03.19===========
	getSupplierList();  
	
    var id = '';
    $("#frmOtherBill").validationEngine();
    $('#frmOtherBill #txtNo').focus();

    $('.delImg').live('click',function(){
        var rowId = $(this).parent().parent().parent().find('tr').length;
        if(rowId!=2)
            $(this).parent().parent().remove();
        callSubTotal();
        callTaxTotal();
    });

    $('.delImg').css('cursor', 'pointer');

    //permision for add 
    if(intAddx)
    {
        $('#frmOtherBill #butNew').show();
        $('#frmOtherBill #butSave').show();
		$('#frmOtherBill #butPrint').show();
    }
    //permision for edit 
    if(intEditx)
    {
        $('#frmOtherBill #butSave').show();
        $('#frmOtherBill #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
		$('#frmOtherBill #butPrint').show();
    }
    //permision for delete
    if(intDeletex)
    {
        $('#frmOtherBill #butDelete').show();
        $('#frmOtherBill #cboSearch').removeAttr('disabled');
    }
    //permision for view
    if(intViewx)
    {
        $('#frmOtherBill #cboSearch').removeAttr('disabled');
    }
  
    $('#frmOtherBill #chkEdit').click(function(){
        if($('#frmOtherBill #chkEdit').attr('checked'))
        {
            $("#frmOtherBill #txtRate").attr("readonly","");
            $('#frmOtherBill #txtRate').focus();
        }
        else
        {
            $('#frmOtherBill #txtRate').val('');
            $("#frmOtherBill #txtRate").attr("readonly","readonly");
            $('#frmOtherBill #cboCurrency').change();
        }
    });
    ///////////////////////////////////////////////////////////////////
    $("input[name^=txtQty]").live("keyup", calculate);
    $("input[name^=txtUnitPrice]").live("keyup", calculate);
    $("input[name^=txtDiscount]").live("keyup", calculate);
    ///////////////////////////////////////////////////////////////////

    ///////////////////////////get supplier address and invoice type////////////////////
    $('#cboSupplier').change(function(){
        var url = "otherBill-db-get.php?requestType=getSupplierAddress&supplierId="+$(this).val();
        var httpobj = $.ajax({
            url:url,
            dataType:'json',
            async:false,
            success:function(json)
            {
                $('.invTpe').attr('checked',false); //
		
                $('#txtAddress').val(json.address);
				if(json.invoType)
				{
                	$('#frmOtherBill #'+json.invoType).attr('checked',true); //
				}
				$('#frmOtherBill #cboCurrency').val(json.currency);
				$('#frmOtherBill #cboCurrency').change();
                invoType = json.invoType;	
            }
        });
    });
    ////////////////////////////////////////////////////////////////////

    ///////////////////////////get item description////////////////////
    $('.item').live('change',function(){
        var url = "otherBill-db-get.php?requestType=getItemDescription&itemId="+$(this).val();
        var obj = $.ajax({
            url:url,
            async:false
        });
        $(this).parent().parent().find('td').eq(2).children().val(obj.responseText);
    });
    ////////////////////////////////////////////////////////////////////

    ///////////////////////////get tax values//////////////////////////
    $('.taxGroup').live('change',function(){
        var operation = '';
        var amount = $(this).parent().parent().find('td').eq(7).children().val();
        var jsonTaxCode="[ ";
        //var taxCode = $(this).find('option:selected').text();
        var taxId = callTaxProcess($(this).val());
        var arrTax = taxId.split('/');
        //var arrTaxInclusive = taxId.split('/');
        //var arrTaxExclusive = taxId.split('-');

        if(arrTax.length == 1)
        {
            operation = 'Isolated';
            jsonTaxCode += '{ "taxId":"'+taxId+'"},';
        }
        else if(arrTax.length > 1)
        {
            operation = arrTax[1];
            for(var i =0; i < arrTax.length; i=i+2)
            {
                jsonTaxCode += '{ "taxId":"'+arrTax[i]+'"},';
            }
        }

        jsonTaxCode = jsonTaxCode.substr(0,jsonTaxCode.length-1);
        jsonTaxCode += " ]";
        var url = "otherBill-db-get.php?requestType=getTaxValue&arrTaxCode="+jsonTaxCode+"&opType="+operation+"&valAmount="+amount;	
        var obj = $.ajax({
            url:url,
            async:false
        });
        var values = obj.responseText.split('/');
        $(this).parent().parent().find('td').eq(10).children().val(values[0]);
	
        if(arrTax.length == 1)
        {
            trnTax="[ ";
            trnTax += '{ "taxId":"'+taxId+'", "taxValue": "'+values[1]+'"},';
            trnTax = trnTax.substr(0,trnTax.length-1);
            trnTax += " ]";
        }
        else if(arrTax.length > 1)
        {
            trnTax="[ ";
            var k = 1;
            for(var i =0; i < arrTax.length; i=i+2)
            {
                trnTax += '{ "taxId":"'+arrTax[i]+'", "taxValue": "'+values[k]+'"},';
                k++;
            }
            trnTax = trnTax.substr(0,trnTax.length-1);
            trnTax += " ]";
        }
        $(this).parent().parent().find('td:eq(10)').attr('id',trnTax);
        callTaxTotal();
    });
    ////////////////////////////////////////////////////////////////////

    ////////////////////////get exchange rate//////////////////////////
    $('#cboCurrency').change(function(){
        var url = "otherBill-db-get.php?requestType=getExchangeRate&currencyId="+$(this).val()+'&exchangeDate='+$('#txtDate').val();
        var obj = $.ajax({
            url:url,
            dataType:'json',
            success:function(json){
		
                $('#rdoBuying').val(json.buyingRate);
                $('#rdoSelling').val(json.sellingRate);
                $('#rdoAverage').val(((eval(json.buyingRate) + eval(json.sellingRate))/2).toFixed(4));
                $('#rdoSelling').click();
            },
            async:false
        });
    });
    ///////////////////////////////////////////////////////////////////
    $('.rdoRate').click(function(){
        $('#txtRate').val($(this).val());
    });
    //save button click event
    $('#frmOtherBill #butSave').click(function(){
	if(existingMsgDate == "")
	{
        changeReadOnly(false);
        $('#frmOtherBill #chkAuto').attr('disabled',false);    
        var itemId = "";
        var itemDesc = "";
        var uomId = "";
        var qty = "";
        var unitPrice = "";
        var discount = "";
        var taxGroupId = "";
        var dimensionId = "";
        var amount = "";
        var trnTaxVal	= "";
			
        value="[ ";
        $('#tblMainGrid2 tr:not(:first)').each(function(){
		
            itemId		= $(this).find(".item").val();
            itemDesc 	= $(this).find(".description").val();
            uomId 		= $(this).find(".uom").val();
            qty 		= $(this).find(".qty").val();
            unitPrice 	= $(this).find(".unitPrice").val();
            discount 	= $(this).find(".discount").val();
            taxAmount 	= $(this).find(".taxWith").val();
            taxGroupId 	= $(this).find(".taxGroup").val();
            dimensionId = $(this).find(".dimension").val();
            amount 		= $(this).find(".amount").val();
            trnTaxVal	= $(this).find(".taxVal").attr('id');
		
            value += '{ "itemId":"'+itemId+'", "itemDesc": "'+URLEncode(itemDesc)+'", "uomId": "'+uomId+'", "qty": "'+qty+'", "unitPrice": "'+unitPrice+'", "discount": "'+discount+'", "taxGroupId": "'+taxGroupId+'", "taxAmount": "'+taxAmount+'", "dimensionId": "'+dimensionId+'", "amount": "'+amount+'", "trnTaxVal":'+trnTaxVal+'},';
        });
	
        value = value.substr(0,value.length-1);
        value += " ]";

        var requestType = '';
        if ($('#frmOtherBill').validationEngine('validate'))   
        { 
            if($('#cboSearch').val()=='')
                requestType = 'add';
			
            else
                requestType = 'edit';
            var id=$('#cboSearch').val();
                    
            //check automated
            var isChecked = $('#frmOtherBill #chkAuto').attr('checked')?true:false;                
            var url = "otherBill-db-set.php";
            var obj = $.ajax({
                url:url,
                dataType: "json",
				type:'post', 
                data:$("#frmOtherBill").serialize()+'&requestType='+requestType+'&cboSearch='+id+'&purchaseDetails='+value+'&invoType='+invoType+'&automated='+isChecked,
                async:false,
			
                success:function(json){
                    $('#frmOtherBill #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
                    if(json.type=='pass')
                    {
                        //$('#frmOtherBill').get(0).reset();
                        loadCombo_frmOtherBill();
                        var t=setTimeout("alertx()",1000);
                        $('#cboSearch').val(json.invoiceNo);
                        return;
                    }
                    var t=setTimeout("alertx()",3000);
                },
                error:function(xhr,status){
					
                    $('#frmOtherBill #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
                    var t=setTimeout("alertx()",3000);
                }		
            });
        }
        //changeReadOnly(true);
        $('#frmOtherBill #chkAuto').attr('disabled',true);
		}
		else
		{
			$('#frmOtherBill #butSave').validationEngine('showPrompt', existingMsgDate,'fail');
			var t=setTimeout("alertx()",5000);
		}
    });
    /////////////////////////////////////////////////////
    //// load invoice details //////////////////////////
    /////////////////////////////////////////////////////
    $('#frmOtherBill #cboSearch').click(function(){
        $('#frmOtherBill').validationEngine('hide');
    });
    $('#frmOtherBill #cboSearch').change(function(){
		existingMsgDate = "";
        $('#frmOtherBill').validationEngine('hide');
        var url = "otherBill-db-get.php";
        if($('#frmOtherBill #cboSearch').val()=='')
        {
            $('#partPay').hide();
            $('#fullPay').hide();
            $('#overPay').hide();
            $('#pending').hide();
	
            $('#tblMainGrid2 >tbody >tr').each(function(){
                if($(this).index()!=0 && $(this).index()!=1 )
                {
                    $(this).remove();
                }
            });
            $('#frmOtherBill').get(0).reset();
            return;	
        }
        //$('#txtNo').val($(this).val());
        var httpobj = $.ajax({
            url:url,
            dataType:'json',
            data:'requestType=loadDetails&id='+URLEncode($(this).val()),
            async:false,
            success:function(json)
            {
                //json  = eval('('+json+')');
                $('.invTpe').attr('checked',false); //
                $('#frmOtherBill #txtNo').val(json.supInvoice);
                $('#frmOtherBill #cboSupplier').val(json.supplier);                
                $('#frmOtherBill #txtDate').val(json.date);
                $('#frmOtherBill #'+json.invoType).attr('checked',true); //
                invoType = json.invoType;	
                $('#frmOtherBill #txtAddress').val(json.address);
                $('#frmOtherBill #cboCurrency').val(json.currency);
                $('#frmOtherBill #txtRate').val(json.rate);
                $('#frmOtherBill #txtRemarks').val(json.remark);
                $('#frmOtherBill #txtPoNo').val(json.poNo);
                $('#frmOtherBill #cboPaymentsTerms').val(json.payTerms);
                $('#frmOtherBill #txtGrnNo').val(json.grnNo);
                $('#frmOtherBill #txtMessage').val(json.message);
                
                var automated=json.automated;
                if(automated==1){
                    $('#frmOtherBill #chkAuto').attr('checked',true);
                    changeReadOnly(true);                    
                }
                else{
                    $('#frmOtherBill #chkAuto').attr('checked',false);
                    changeReadOnly(false);
                }                
                $('#frmOtherBill #cboItem').html(json.autoItemList);

                if(json.totAmount != json.balAmount)
                {
                    if(json.balAmount > 0)
                    {
                        $('#partPay').show();
                        $('#fullPay').hide();
                        $('#overPay').hide();
                        $('#pending').hide();
                    }
                    else if(json.balAmount == 0)
                    {
                        $('#fullPay').show();
                        $('#partPay').hide();
                        $('#overPay').hide();
                        $('#pending').hide();
                    }
                    if(json.balAmount < 0)
                    {
                        $('#partPay').hide();
                        $('#fullPay').hide();
                        $('#overPay').show();
                        $('#pending').hide();
                    }
                }
                else
                {
                    $('#partPay').hide();
                    $('#fullPay').hide();
                    $('#overPay').hide();
                    $('#pending').show();
                }
		
                //--------------------------------------------------
                $('#tblMainGrid2 >tbody >tr').each(function(){
                    if($(this).index()!=0 && $(this).index()!=1 )
                    {
                        $(this).remove();
                    }
                });
                var itemId 		= "";
                var itemDesc 	= "";
                var uom			= "";
                var qty			= "";
                var unitPrice	= "";
                var discount	= "";
                var taxAmount 	= "";
                var tax			= "";
                var dimension 	= "";
                if(json.detailVal!=null)
                {
                    var rowId = $('#tblMainGrid2').find('tr').length;
                    var tbl = document.getElementById('tblMainGrid2');
                    rows = $('#tblMainGrid2').find('tr').length;
                    for(var j=0;j<=json.detailVal.length-1;j++)
                    {
                        itemId		= json.detailVal[j].itemId;
                        itemDesc	= json.detailVal[j].itemDesc;
                        uom			= json.detailVal[j].uom;
                        qty			= json.detailVal[j].qty;
                        unitPrice	= json.detailVal[j].unitPrice;
                        discount	= json.detailVal[j].discount;
                        taxAmount	= json.detailVal[j].taxAmount;
                        tax			= json.detailVal[j].taxGroup;
                        dimension	= json.detailVal[j].dimension;
                        if(j != json.detailVal.length-1)
                        {
                            //alert(json.detailVal.length);
                            tbl.insertRow(rows);
                            tbl.rows[rows].innerHTML = tbl.rows[rows-1].innerHTML;
                            tbl.rows[rows].cells[1].childNodes[1].value = itemId;
                            tbl.rows[rows].cells[2].childNodes[1].value = itemDesc;
                            tbl.rows[rows].cells[3].childNodes[1].value = uom;
                            tbl.rows[rows].cells[4].childNodes[1].value = qty;
                            tbl.rows[rows].cells[5].childNodes[1].value = unitPrice;
                            tbl.rows[rows].cells[6].childNodes[1].value = discount;
                            tbl.rows[rows].cells[7].childNodes[1].value = ((unitPrice*((100-discount)/100))*qty).toFixed(4);
                            //tbl.rows[rows].cells[8].childNodes[1].value = tax;
                            $('#tblMainGrid2 >tbody >tr:eq('+rows+')>td:eq(8)').find('.taxGroup').val(tax);
                            tbl.rows[rows].cells[9].childNodes[1].value = dimension;
                            tbl.rows[rows].cells[10].childNodes[1].value = taxAmount;
                            $('#tblMainGrid2 >tbody >tr:eq('+rows+')>td:eq(8)').find('.taxGroup').change();
                        }
                        else
                        {
                            tbl.rows[1].cells[1].childNodes[1].value = itemId;
                            tbl.rows[1].cells[2].childNodes[1].value = itemDesc;
                            tbl.rows[1].cells[3].childNodes[1].value = uom;
                            tbl.rows[1].cells[4].childNodes[1].value = qty;
                            tbl.rows[1].cells[5].childNodes[1].value = unitPrice;
                            tbl.rows[1].cells[6].childNodes[1].value = discount;
                            tbl.rows[1].cells[7].childNodes[1].value = ((unitPrice*((100-discount)/100))*qty).toFixed(4);
                            //tbl.rows[1].cells[8].childNodes[1].value = tax;
                            $('#tblMainGrid2 >tbody >tr:eq(1)>td:eq(8)').find('.taxGroup').val(tax);
                            tbl.rows[1].cells[9].childNodes[1].value = dimension;
                            tbl.rows[1].cells[10].childNodes[1].value = taxAmount;
                            $('#tblMainGrid2 >tbody >tr:eq(1)>td:eq(8)').find('.taxGroup').change();
                        }
                    //loadJs();
                    }
                    callSubTotal();
                    callTaxTotal();
                }
                else
                {
			
                }
            //--------------------------------------------------
            }
        });
    });
    //////////// end of load details /////////////////

    $('#frmOtherBill #butNew').click(function(){
		existingMsgDate = "";
        location.reload();
        changeReadOnly(false);
        $('#frmOtherBill').get(0).reset();
        $('#tblMainGrid2 >tbody >tr').each(function(){
            if($(this).index()!=0 && $(this).index()!=1 )
            {
                $(this).remove();
            }
        });
        $('#partPay').hide();
        $('#fullPay').hide();
        $('#overPay').hide();
        $('#pending').hide();
        loadCombo_frmOtherBill();
        $('#frmOtherBill #txtNo').focus();
    });
        
    $('.invTpe').click(function(){
        invoType = $(this).val();	
    });
    $('#frmOtherBill #butDelete').click(function(){
        if($('#frmOtherBill #cboSearch').val()=='')
        {
            $('#frmOtherBill #butDelete').validationEngine('showPrompt', 'Please select Invoice.', 'fail');
            var t=setTimeout("alertDelete()",1000);	
        }
        else
        {
            var supplierId=$('#frmOtherBill #cboSupplier').val()
            var $invoice=$('#frmOtherBill #txtNo').val()
            var val = $.prompt('Are you sure you want to delete "'+$('#frmOtherBill #cboSearch option:selected').text()+'" ?',{
                buttons: {
                    Ok: true, 
                    Cancel: false
                },
                callback: function(v,m,f){
                    if(v)
                    {
                        var url = "otherBill-db-set.php";
                        var httpobj = $.ajax({
                            url:url,
                            dataType:'json',
                            data:'requestType=delete&cboSearch='+URLEncode($('#frmOtherBill #cboSearch').val())+'&cboSupplier='+supplierId+'&txtNo='+$invoice,
                            async:false,
                            success:function(json){
							
                                $('#frmOtherBill #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
							
                                if(json.type=='pass')
                                {
                                    $('#frmOtherBill').get(0).reset();
                                    $('#tblMainGrid2 >tbody >tr').each(function(){
                                        if($(this).index()!=0 && $(this).index()!=1 )
                                        {
                                            $(this).remove();
                                        }
                                    });
                                    loadCombo_frmOtherBill();
                                    var t=setTimeout("alertDelete()",1000);
                                    return;
                                }	
                                var t=setTimeout("alertDelete()",3000);
                            }	 
                        });
                    }
                }
            });	
        }
    });
    //Dsisplay PO
    $('#frmOtherBill #txtPoNo').click(function(){
        $('#frmOtherBill #chkAuto').attr('disabled',false);
        if($('#frmOtherBill #chkAuto').attr('checked')){
            var po=$('#frmOtherBill #txtPoNo').val();
            var val=po.split("/");
            var url="../../../../presentation/procurement/purchaseOrder/listing/rptPurchaseOrder.php?poNo="+val[0]+"&year="+val[1];
            window.open(url);
        }
    
        $('#frmOtherBill #chkAuto').attr('disabled',true);
    });

$('#frmOtherBill #butPrint').click(function(){
		if($('#frmOtherBill #cboSearch').val()=='')
		{
			$('#frmOtherBill #butPrint').validationEngine('showPrompt', 'Please select Invoice.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var myurl = 'otherBillDetails.php?id='+URLEncode($('#frmOtherBill #cboSearch').val());
    		window.open(myurl); 
		}
	});	

});

//-------------------------------------------functions area----------------------------------------------------------

////////////////////// calculation ////////////////////////////////
function calculate()
{ 
    var quantity =  ($(this).parent().parent().find('td').eq(4).children().val()==''?0:$(this).parent().parent().find('td').eq(4).children().val());

    var unitprice  = ($(this).parent().parent().find('td').eq(5).children().val()==''?0:$(this).parent().parent().find('td').eq(5).children().val());

    var discount  = ($(this).parent().parent().find('td').eq(6).children().val()==''?0:$(this).parent().parent().find('td').eq(6).children().val());
	
    var calAmount = parseFloat(quantity)*(parseFloat(unitprice)*(100-parseFloat(discount))/100);

    $(this).parent().parent().find('td').eq(7).children().val(calAmount.toFixed(4));
    callSubTotal();
    callTotalAmount();
    $(this).parent().parent().find(".taxGroup").change();
}
////////////////////////////////////////////////////////////////////
function callTaxProcess(taxGroupId)
{
    var url = "otherBill-db-get.php?requestType=getTaxProcess&taxGroupId="+taxGroupId;
    var obj = $.ajax({
        url:url,
        async:false
    });
    return obj.responseText;
}
function callSubTotal()
{
    var subTotal = 0.00;
    $(".amount").each( function(){
        subTotal += eval($(this).val()==''?0.00:$(this).val());
    });
    $('#txtSubTotal').val(subTotal.toFixed(4));	
}
function callTaxTotal()
{
    var taxTotal = 0.00;
    $(".taxWith").each( function(){
        taxTotal += eval($(this).val()==''?0.00:$(this).val());
    });
    $('#txtTotalTax').val(taxTotal.toFixed(4));
    callTotalAmount();
}
function callTotalAmount()
{
    $('#txtTotal').val((eval($('#txtTotalTax').val()) + eval($('#txtSubTotal').val())).toFixed(4));
}
function loadCombo_frmOtherBill()
{
    var url 	= "otherBill-db-get.php?requestType=loadCombo";
    var httpobj = $.ajax({
        url:url,
        async:false
    })
    $('#frmOtherBill #cboSearch').html(httpobj.responseText);
}
function alertx()
{
    $('#frmOtherBill #butSave').validationEngine('hide') ;
}
function alertDelete()
{
    $('#frmOtherBill #butDelete').validationEngine('hide') ;
	$('#frmOtherBill #butPrint').validationEngine('hide') ;
}
function loadJs()
{
}
function changeReadOnly(status){
    $('#frmOtherBill #txtNo').attr('disabled',status);
    $('#frmOtherBill #cboSupplier').attr('disabled',status);
    //$('#frmOtherBill #txtDate').attr('disabled',status);
    $('#frmOtherBill #rdInvoType').attr('disabled',status);//
    $('#frmOtherBill #txtAddress').attr('disabled',status);
    //$('#frmOtherBill #txtRemarks').attr('disabled',status);
    $('#frmOtherBill #cboCurrency').attr('disabled',status);//
    $('#frmOtherBill #txtPoNo').attr('readonly',status);
    $('#frmOtherBill #txtPoNo').attr('class',"normalfnt");
    
    $('#frmOtherBill #txtGrnNo').attr('disabled',status);
    $('#frmOtherBill #cboItem').attr('disabled',status);//
    $('#frmOtherBill #txtDesc').attr('disabled',status);//
    $('#frmOtherBill #cboUOM').attr('disabled',status);//
    $('#frmOtherBill #txtQty').attr('disabled',status);
    $('#frmOtherBill #txtUnitPrice').attr('disabled',status);
    $('#frmOtherBill #txtDiscount').attr('disabled',status);
    $('#frmOtherBill #txtAmount').attr('disabled',status);
    if(status){
        $('#frmOtherBill #btnTadd').hide();
    }
    else{
        $('#frmOtherBill #btnTadd').show();
    }    
}

//=========Add by dulaskshi 2013.03.19===========
//====================Get Supplier List==============================
function getSupplierList()
{	
	$ledgerAccId = $('#frmOtherBill #cboLedgerAcc').val();
	
	var url = "otherBill-db-get.php?requestType=loadSupplier&ledgerAcc="+$ledgerAccId;
	var httpobj = $.ajax({url:url,async:false})
	$('#frmOtherBill #cboSupplier').html(httpobj.responseText);
}
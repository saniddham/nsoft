<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$companyId = $row['intCompanyId']; 
	}
	/////////// sales invoice load part /////////////////////
	if($requestType=='loadCombo')
	{
		$sql = "SELECT
				fin_other_payable_payments_header.strReferenceNo,
				fin_other_payable_payments_header.intReceiptNo,
				mst_finance_service_supplier.strName
				FROM
				fin_other_payable_payments_header
				Inner Join mst_finance_service_supplier ON fin_other_payable_payments_header.intSupplierId = mst_finance_service_supplier.intId
				WHERE
				fin_other_payable_payments_header.intCompanyId =  '$companyId' AND
				fin_other_payable_payments_header.intDeleteStatus = '0'
				ORDER BY intReceiptNo DESC
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['strReferenceNo']."\">".$row['strReferenceNo']." - ".$row['strName']."</option>";
		}
		echo $html;
	}
		else if($requestType=='getSupplierAddress') // with invoice type
	{
		$supplierId  = $_REQUEST['supplierId'];
		
		$sql = "SELECT 	mst_finance_service_supplier.strAddress, mst_finance_service_supplier.strInvoiceType, mst_finance_service_supplier.intCurrencyId FROM mst_finance_service_supplier WHERE mst_finance_service_supplier.intId = '$supplierId'";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$response['currency']= $row['intCurrencyId'];
		echo json_encode($response);
	}
	
	//===========Add by dulakshi 2013.03.19=========
	else if($requestType=='loadSupplier')
	{
		$ledAcc  = $_REQUEST['ledgerAcc'];		
							
		$condition == "";
			if($ledAcc != "")
			{	
				$condition .= "AND mst_finance_service_supplier_activate.intChartOfAccountId =  '$ledAcc'";			
			}
				
		$sql = "SELECT
					mst_finance_service_supplier.intId,
					mst_finance_service_supplier.strName,
					mst_finance_service_supplier_activate.intCompanyId
				FROM
					mst_finance_service_supplier
						Inner Join mst_finance_service_supplier_activate ON mst_finance_service_supplier.intId = mst_finance_service_supplier_activate.intSupplierId
				WHERE
					mst_finance_service_supplier.intStatus =  '1' AND
					mst_finance_service_supplier_activate.intCompanyId =  '$companyId' " .$condition . " order by mst_finance_service_supplier.strName ASC";
				
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
	}
	//==============================================
	
	
	else if($requestType=='loadDetails')
	{
		$id  = $_REQUEST['id'];
		
		//-------------------------------------------------------------
		$sql = "SELECT
				fin_other_payable_payments_main_details.intReceiptNo,
				fin_other_payable_payments_main_details.intAccPeriodId,
				fin_other_payable_payments_main_details.intLocationId,
				fin_other_payable_payments_main_details.intCompanyId,
				fin_other_payable_payments_main_details.strReferenceNo,
				fin_other_payable_payments_main_details.strPoNo,
				fin_other_payable_payments_main_details.strGrnNo,
				fin_other_payable_payments_main_details.strDocNo,
				fin_other_payable_payments_main_details.dtmDate,
				fin_other_payable_payments_main_details.dblAmount,
				fin_other_payable_payments_main_details.dblToBePaid,
				fin_other_payable_payments_main_details.strCurrency,
				fin_other_payable_payments_main_details.dblRate,
				fin_other_payable_payments_main_details.dblPayAmount,
				fin_other_payable_payments_main_details.strDocType,
				fin_other_payable_payments_main_details.strDocNo,
				fin_other_payable_payments_header.intDeleteStatus,
				fin_other_payable_bill_header.strSupInvoice,
				IFNULL(CONCAT(' - ref ',fin_other_payable_payments_main_details.strDocNoRef),'') AS refNo
				FROM
				fin_other_payable_payments_main_details
				Inner Join fin_other_payable_payments_header ON fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
				left outer Join fin_other_payable_bill_header ON fin_other_payable_bill_header.strReferenceNo = fin_other_payable_payments_main_details.strDocNo
				WHERE
				fin_other_payable_payments_main_details.strReferenceNo =  '$id' AND
				fin_other_payable_payments_header.intDeleteStatus =  '0'
				";
		$result = $db->RunQuery($sql);
		$arrDetail;
		while($row=mysqli_fetch_array($result))
		{
			$invoice 	= rawurlencode($row['strDocNo']);
			$poNo 		= $row['strPoNo'];
			$grnNo 		= $row['strGrnNo'];
			$docNo 		= rawurlencode($row['strDocNo']);
			$displayDoc = $row['strDocNo'].$row['refNo'];
			$date 		= $row['dtmDate'];
			$amount 	= $row['dblAmount'];
			$toBePaid 	= $row['dblToBePaid'];
			$currency 	= $row['strCurrency'];
			$rate 		= $row['dblRate'];
			$payAmount 	= $row['dblPayAmount'];
			$docType 	= $row['strDocType'];
			
			$supInv		= $row['strSupInvoice'];
			
			if($docType == 'O.Bill')
			{
				$docPath = "<a target=_blank href=../otherBill/otherBill.php?id=$docNo>$supInv</a>";
			}
			else if($docType == 'O.APayment')
			{
				$docPath = "<a target=_blank href=../advancePayments/advancePayments.php?receiptNo=$docNo>$displayDoc</a>";
			}
			else if($docType == 'O.DNote')
			{
				$docPath = "<a target=_blank href=../debitNote/debitNote.php?debitNo=$docNo>$displayDoc</a>";
			}
			else if($docType == 'B.Deposit')
			{
				$docPath = "<a target=_blank href=../../bank/deposit/deposit.php?depositNo=$docNo>$displayDoc</a>";
			}
			else if($docType == 'B.Payment')
			{
				$docPath = "<a target=_blank href=../../bank/payments/payments.php?paymentNo=$docNo>$displayDoc</a>";
			}
			else if($docType == 'Petty Cash')
			{
				$docPath = "<a target=_blank href=../../bank/pettyCash/pettyCash.php?pettyCashNo=$docNo>$displayDoc</a>";
			}
			else if($docType == 'JN')
			{
				$jobPath = $jobNo;
				$docPath = "<a target=_blank href=../../accountant/journalEntry/journalEntry.php?id=$docNo>$displayDoc</a>";
			}
			else
			{
				$docPath = $displayDoc;
			}
			
			$val['tBodyDetail'] = "<tr class=normalfnt bgcolor=#FFFFFF>
				<td align=center><input class=checkRow type=checkbox id=$invoice checked=checked /></td>
				<td align=center class=po id=$po>$poNo</td>
				<td align=center class=grn id=$grn>$grnNo</td>
				<td width=180 align=center class=docNo id=$docNo>$docPath</td>
				<td align=center class=docDate>$date</td>
				<td align=right class=amount>$amount</td>
				<td align=right class=toBePaid>$toBePaid</td>
				<td align=center class=docCurrency>$currency</td>
				<td align=right class=docRate>$rate</td>
				<td align=center><input  style=width:100%;text-align:right;background-color:#FFF;border:none name=txtPayAmount class=payAmount type=text id=txt$invoice value=$payAmount /></td> 
				<td align=center class=docType>$docType</td>
				</tr>"; // textBox --> disabled=disabled
			$arrDetail[] = $val;
			
		}
		$response['detailVal'] = $arrDetail;
		//-------------------------------------------------------------
		
		//-------------------------------------------------------------
		$sql = "SELECT
				fin_other_payable_payments_account_details.intReceiptNo,
				fin_other_payable_payments_account_details.intAccPeriodId,
				fin_other_payable_payments_account_details.intLocationId,
				fin_other_payable_payments_account_details.intCompanyId,
				fin_other_payable_payments_account_details.strReferenceNo,
				fin_other_payable_payments_account_details.intChartOfAccountId,
				fin_other_payable_payments_account_details.dblAmount,
				fin_other_payable_payments_account_details.strMemo,
				fin_other_payable_payments_account_details.intDimensionId,
				fin_other_payable_payments_header.intDeleteStatus
				FROM
				fin_other_payable_payments_account_details
				Inner Join fin_other_payable_payments_header ON fin_other_payable_payments_account_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
				WHERE
				fin_other_payable_payments_account_details.strReferenceNo =  '$id' AND
				fin_other_payable_payments_header.intDeleteStatus =  '0'
				";
		$result = $db->RunQuery($sql);
		$arrAccDetail;
		while($row=mysqli_fetch_array($result))
		{
			$val1['chartAcc'] 	= $row['intChartOfAccountId'];
			$val1['amount'] 		= $row['dblAmount'];
			$val1['memo'] 		= $row['strMemo'];
			$val1['dimension']	= $row['intDimensionId'];
			$arrAccDetail[] = $val1;
		}
		$response['detailAccVal'] = $arrAccDetail;
		//-----------------------------------------------------------
		
				$sql = "SELECT
				fin_other_payable_payments_header.intReceiptNo,
				fin_other_payable_payments_header.intAccPeriodId,
				fin_other_payable_payments_header.intLocationId,
				fin_other_payable_payments_header.intCompanyId,
				fin_other_payable_payments_header.strReferenceNo,
				fin_other_payable_payments_header.intsupplierId,
				fin_other_payable_payments_header.dtmDate,
				fin_other_payable_payments_header.intCurrencyId,
				fin_other_payable_payments_header.dblRate,
				fin_other_payable_payments_header.dblRecAmount,
				fin_other_payable_payments_header.intPayMethodId,
				fin_other_payable_payments_header.strPayRefNo,
				fin_other_payable_payments_header.dtmPayRefDate,
				fin_other_payable_payments_header.intIsPosted,
				fin_other_payable_payments_header.strPayRefOrg,
				fin_other_payable_payments_header.strRemark
				FROM
				fin_other_payable_payments_header
				WHERE
				fin_other_payable_payments_header.strReferenceNo =  '$id' AND
				fin_other_payable_payments_header.intDeleteStatus =  '0'
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['supplier'] 	= $row['intsupplierId'];
			$response['date'] 		= $row['dtmDate'];
			$response['currency']	= $row['intCurrencyId'];
			$response['rate'] 		= $row['dblRate'];
			$response['amount'] 	= $row['dblRecAmount'];
			$response['payMethod'] 	= $row['intPayMethodId'];
			$response['payRefNo'] 	= $row['strPayRefNo'];
			$response['payRefDate'] = $row['dtmPayRefDate'];
			$response['isPosted'] 	= $row['intIsPosted'];
			$response['payRefOrg'] 	= $row['strPayRefOrg'];
			$response['remark'] 	= $row['strRemark'];
		}
		echo json_encode($response);
	}
	else if($requestType=='getExchangeRate')
	{
		$currencyId  	= $_REQUEST['currencyId'];
		$exchangeDate	= $_REQUEST['exchangeDate'];
		
		$sql = "SELECT
					mst_financeexchangerate.dblSellingRate,
					mst_financeexchangerate.dblBuying
				FROM mst_financeexchangerate
				WHERE
					mst_financeexchangerate.intCurrencyId 	=  '$currencyId' AND
					mst_financeexchangerate.dtmDate 		=  '$exchangeDate'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		
		if(mysqli_num_rows($result)>0)
		{
			$arrValue['sellingRate'] 	= $row['dblSellingRate'];
			$arrValue['buyingRate'] 	= $row['dblBuying'];
		}
		else
		{
			$arrValue['sellingRate'] 	= "";
			$arrValue['buyingRate'] 	= "";
		}
		echo json_encode($arrValue);
	}
	else if($requestType=='getItemDescription')
	{
		$itemId  = $_REQUEST['itemId'];
		
		$sql = "SELECT
				mst_financesupplieritem.intId,
				mst_financesupplieritem.strRemark
				FROM
				mst_financesupplieritem
				WHERE
				mst_financesupplieritem.intId =  '$itemId'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		echo $row['strRemark'];
	}
	else if($requestType=='getTaxValue')
	{
		$operation  = $_REQUEST['opType'];
		$amount  = $_REQUEST['valAmount'];
		$taxCodes = json_decode($_REQUEST['arrTaxCode'], true);
		
		if(count($taxCodes) != 0)
		{
			foreach($taxCodes as $taxCode)
			{
				$codeValues[] = callTaxValue($taxCode['taxId']);
			}
		}
		if(count($codeValues) > 1)
		{
			if($operation == 'Inclusive')
			{
				$firstVal = ($amount*$codeValues[0])/100;
				$withTaxVal = $firstVal + ((($amount+$firstVal)*$codeValues[1])/100);
			}
			else if($operation == 'Exclusive')
			{
				$withTaxVal = ($amount*($codeValues[0] + $codeValues[1]))/100;
			}
		}
		else if(count($codeValues) == 1 && $operation == 'Isolated')
		{
			$withTaxVal = ($amount*$codeValues[0])/100;
		}
		echo $withTaxVal;
	}
	else if($requestType=='getTaxProcess')
	{
		$taxGrpId  = $_REQUEST['taxGroupId'];
		
		$sql = "SELECT
				mst_financetaxgroup.intId,
				mst_financetaxgroup.strProcess
				FROM
				mst_financetaxgroup
				WHERE
				mst_financetaxgroup.intId =  '$taxGrpId'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		echo $row['strProcess'];
	}
	//---------------------------------
	else if($requestType=='getInvoice')
	{
		$supplierId  = $_REQUEST['supplierId'];
		
		//////////////////////////Invoice//////////////////////////////////
		$sql = "SELECT
				fin_other_payable_bill_header.strReferenceNo,
				fin_other_payable_bill_header.strSupInvoice,
				fin_other_payable_bill_header.strPoNo,
				fin_other_payable_bill_header.strGrnNo,
				fin_other_payable_bill_header.intsupplierId,
				mst_finance_service_supplier.intId,
				mst_finance_service_supplier.strName,
				mst_financecurrency.intId,
				mst_financecurrency.strCode,
				fin_other_payable_bill_header.intInvoiceNo,
				fin_other_payable_bill_header.intAccPeriodId,
				fin_other_payable_bill_header.intLocationId,
				fin_other_payable_bill_header.intCompanyId,
				INV.intInvoiceNo,
				INV.intAccPeriodId,
				INV.intLocationId,
				INV.intCompanyId,
				fin_other_payable_bill_header.dtmDate,
				fin_other_payable_bill_header.dblRate,
				INV.dblQty,
				INV.dblUnitPrice,
				INV.dblDiscount,
				INV.dblTaxAmount,
				sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) AS amount,
				(
				sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) 
				-
				IFNULL ((SELECT
				Sum(fin_other_payable_payments_main_details.dblPayAmount )AS paidAmount
				FROM fin_other_payable_payments_main_details
				Inner Join fin_other_payable_payments_header ON fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
				WHERE
				fin_other_payable_payments_main_details.strDocNo =  INV.strReferenceNo AND
				fin_other_payable_payments_header.intDeleteStatus =  '0' AND fin_other_payable_payments_main_details.strDocType = 'O.Bill'
				GROUP BY
				fin_other_payable_payments_main_details.strDocNo),0)
				
				) AS balAmount
				FROM
				fin_other_payable_bill_details AS INV
				Inner Join fin_other_payable_bill_header ON fin_other_payable_bill_header.strReferenceNo = INV.strReferenceNo AND INV.intInvoiceNo = fin_other_payable_bill_header.intInvoiceNo AND INV.intAccPeriodId = fin_other_payable_bill_header.intAccPeriodId AND INV.intCompanyId = fin_other_payable_bill_header.intCompanyId
				Inner Join mst_finance_service_supplier ON mst_finance_service_supplier.intId = fin_other_payable_bill_header.intsupplierId
				Inner Join mst_financecurrency ON mst_financecurrency.intId = fin_other_payable_bill_header.intCurrencyId
				WHERE
				fin_other_payable_bill_header.intsupplierId =  '$supplierId' AND
				fin_other_payable_bill_header.intDeleteStatus =  '0'
				AND fin_other_payable_bill_header.intCompanyId =  '$companyId'
				GROUP BY
				fin_other_payable_bill_header.strReferenceNo
				having balAmount<>0
				"; // AND INV.intLocationId = fin_other_payable_bill_header.intLocationId
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$invoice = $row['strReferenceNo'];
			$supInvoice = $row['strSupInvoice'];
			$poNo = $row['strPoNo'];
			$grnNo = $row['strGrnNo'];
			$amount = number_format($row['amount'],4,'.','');
			$toBePaid = number_format($row['balAmount'],4,'.','');
			$date = $row['dtmDate'];
			$currency = $row['strCode'];
			$rate = $row['dblRate'];
			echo 
				"
				<tr class=normalfnt bgcolor=#00FF66>
				<td align=center><input class=checkRow type=checkbox id=$invoice /></td>
				<td align=center class=po id=$po>$poNo</td>
				<td align=center class=grn id=$grn>$grnNo</td>
		<td width=180 align=center class=docNo id=$invoice><a target=_blank href=../otherBill/otherBill.php?id=$invoice>$supInvoice</a></td>
				<td align=center class=docDate>$date</td>
				<td align=right class=amount>$amount</td>
				<td align=right class=toBePaid>$toBePaid</td>
				<td align=center class=docCurrency>$currency</td>
				<td align=right class=docRate>$rate</td>
				<td align=center><input disabled=disabled style=width:100%;text-align:right;background-color:#FFF;border:none name=txtPayAmount  class=payAmount type=text id=txt$invoice /></td>
				<td align=center class=docType>O.Bill</td>
				</tr>
				";
		} // readonly=readonly
		////////////////////////////Advance Payment////////////////////////////////
		$sql = "SELECT
				ADV.strReceiptNo,
				ADV.intsupplier,
				ADV.intCompanyId,
				ADV.dtDate,
				ADV.dblRate,
				ADV.intCurrency,
				ADV.dblReceivedAmount,
				mst_financecurrency.intId,
				mst_financecurrency.strCode,
				ADV.strPerfInvoiceNo,
				IFNULL(CONCAT(' - ref ',ADV.strPerfInvoiceNo),'') AS refNo,
				(
				ADV.dblReceivedAmount 
				+
				IFNULL ((SELECT
				Sum(fin_other_payable_payments_main_details.dblPayAmount )AS paidAmount
				FROM fin_other_payable_payments_main_details
				Inner Join fin_other_payable_payments_header ON fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
				WHERE
				fin_other_payable_payments_main_details.strDocNo =  ADV.strReceiptNo AND
				fin_other_payable_payments_header.intDeleteStatus =  '0' AND fin_other_payable_payments_main_details.strDocType = 'O.APayment'
				GROUP BY
				fin_other_payable_payments_main_details.strDocNo),0)
				
				) AS balAmount
				FROM
				fin_other_payable_advancepayment_header AS ADV
				Inner Join mst_financecurrency ON ADV.intCurrency = mst_financecurrency.intId
				WHERE
				ADV.intsupplier =  '$supplierId' AND
				ADV.intStatus = '1'
				AND ADV.intCompanyId =  '$companyId'
				having balAmount<>0
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$receipt = rawurlencode($row['strReceiptNo']);
			$displayNo = $row['strReceiptNo'].$row['refNo'];
			$amount = number_format($row['dblReceivedAmount'],4,'.','');
			$toBePaid = number_format(($row['balAmount']*(-1)),4,'.','');
			$date = $row['dtDate'];
			$currency = $row['strCode'];
			$rate = $row['dblRate'];
			
			$docRefNo = $row['strPerfInvoiceNo'];
			
			echo 
				"
				<tr class=normalfnt bgcolor=#CAFEB8>
				<td align=center><input class=checkRow type=checkbox id=$receipt /></td>
				<td align=center class=po id=$receipt></td>
				<td align=center class=grn id=$receipt></td>
	<td width=180 align=center class=docNo id=$receipt><a target=_blank href=../advancePayments/advancePayments.php?receiptNo=$receipt>$displayNo</a></td>
				<td align=center class=docDate>$date</td>
				<td align=right class=amount>$amount</td>
				<td align=right class=toBePaid>$toBePaid</td>
				<td align=center class=docCurrency>$currency</td>
				<td align=right class=docRate>$rate</td>
				<td align=center><input disabled=disabled style=width:100%;text-align:right;background-color:#FFF;border:none name=txtPayAmount  class=payAmount type=text id=txt$invoice /></td>
				<td align=center class=docType id=$docRefNo>O.APayment</td>
				</tr>
				";
		}
		//////////////////////////Debit Note//////////////////////////
		$sql = "SELECT
				fin_other_payable_debitnote_header.strDebitNoteNo,
				CRN.strDebitNoteNo,
				fin_other_payable_debitnote_header.strPoNo,
				fin_other_payable_debitnote_header.strGrnNo,
				fin_other_payable_debitnote_header.intsupplier,
				fin_other_payable_debitnote_header.strInvoiceNo,
				fin_other_payable_debitnote_header.dtDate,
				fin_other_payable_debitnote_header.dblRate AS hRate,
				fin_other_payable_debitnote_header.intCurrency,
				CRN.intItem,
				CRN.intUOM,
				CRN.dblQty,
				CRN.dblDiscount,
				fin_other_payable_debitnote_header.strInvoiceNo,
				mst_financecurrency.intId,
				mst_financecurrency.strCode,
				CRN.dblRate,
				CRN.dblTax,
				SUM(((CRN.dblQty*CRN.dblRate) *(100-CRN.dblDiscount)/100)+ IFNULL(CRN.dblTax,0)) AS amount,
				(
				SUM(((CRN.dblQty*CRN.dblRate) *(100-CRN.dblDiscount)/100)+ IFNULL(CRN.dblTax,0))
				+
				IFNULL ((SELECT
				Sum(fin_other_payable_payments_main_details.dblPayAmount )AS paidAmount
				FROM fin_other_payable_payments_main_details
				Inner Join fin_other_payable_payments_header ON fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
				WHERE
				fin_other_payable_payments_main_details.strDocNo =  CRN.strDebitNoteNo AND
				fin_other_payable_payments_header.intDeleteStatus =  '0' AND fin_other_payable_payments_main_details.strDocType = 'O.DNote'
				GROUP BY
				fin_other_payable_payments_main_details.strDocNo),0)
				) AS balAmount
				FROM
				fin_other_payable_debitnote_details AS CRN
				Inner Join fin_other_payable_debitnote_header ON CRN.strDebitNoteNo = fin_other_payable_debitnote_header.strDebitNoteNo
				Inner Join mst_financecurrency ON fin_other_payable_debitnote_header.intCurrency = mst_financecurrency.intId
				WHERE
				fin_other_payable_debitnote_header.intsupplier =  '$supplierId' AND
				fin_other_payable_debitnote_header.intStatus = '1'
				AND fin_other_payable_debitnote_header.intCompanyId =  '$companyId'
				GROUP BY
				CRN.strDebitNoteNo
				having balAmount<>0
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$debit = rawurlencode($row['strDebitNoteNo']);
			$displayNo_d = $row['strDebitNoteNo'];
			$invoice =  rawurlencode($row['strInvoiceNo']);
			$displayNo_i = $row['strInvoiceNo'];
			$amount = number_format($row['amount'],4,'.','');
			$toBePaid = number_format(($row['balAmount']*(-1)),4,'.','');
			$date = $row['dtDate'];
			$currency = $row['strCode'];
			$rate = $row['hRate'];
			$poNo = $row['strPoNo'];
			$grnNo = $row['strGrnNo'];
			
			echo 
				"
				<tr class=normalfnt bgcolor=#FFFFD7>
				<td align=center><input class=checkRow type=checkbox id=$debit /></td>
				<td align=center class=po id=$debit>$poNo</td>
				<td align=center class=grn id=$debit>$grnNo</td>
				<td align=center class=docNo id=$debit><a target=_blank href=../debitNote/debitNote.php?debitNo=$debit>$displayNo_d</a></td>
	<!--<td width=180 align=center class=docNo id=$invoice><a target=_blank href=../otherBill/otherBill.php?id=$invoice>$displayNo_i</a></td>-->
				<td align=center class=docDate>$date</td>
				<td align=right class=amount>$amount</td>
				<td align=right class=toBePaid>$toBePaid</td>
				<td align=center class=docCurrency>$currency</td>
				<td align=right class=docRate>$rate</td>
				<td align=center><input disabled=disabled style=width:80px;text-align:right;background-color:#FFF;border:none name=txtPayAmount  class=payAmount type=text id=txt$invoice /></td>
				<td align=center class=docType>O.DNote</td>
				</tr>
				";
		}
		//////////////////////////Bank Deposit/////////////////////////
		$sql = "SELECT
				BND.strDepositNo,
				BND.dtDate,
				BND.intCurrency,
				fin_bankdeposit_details.intAccount,
				fin_bankdeposit_details.intRecvFrom,
				fin_bankdeposit_details.dblAmmount,
				BND.dblRate,
				BND.intCompanyId,
				mst_financecurrency.strCode,
				mst_financecurrency.intId,
				mst_financechartofaccounts.intId,
				mst_financechartofaccounts.intFinancialTypeId,
				BND.strFnRefNo,
				IFNULL(CONCAT(' - ref ',BND.strFnRefNo),'') AS refNo,
				(
				fin_bankdeposit_details.dblAmmount 
				-
				IFNULL ((SELECT
				Sum(fin_other_payable_payments_main_details.dblPayAmount )AS paidAmount
				FROM fin_other_payable_payments_main_details
				Inner Join fin_other_payable_payments_header ON fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
				WHERE
				fin_other_payable_payments_main_details.strDocNo =  BND.strDepositNo AND
				fin_other_payable_payments_header.intDeleteStatus =  '0' AND fin_other_payable_payments_main_details.strDocType = 'B.Deposit'
				GROUP BY
				fin_other_payable_payments_main_details.strDocNo),0)
				
				) AS balAmount
				FROM
				fin_bankdeposit_header BND
				Inner Join fin_bankdeposit_details ON fin_bankdeposit_details.strDepositNo = BND.strDepositNo
				Inner Join mst_financecurrency ON BND.intCurrency = mst_financecurrency.intId
				Inner Join mst_financechartofaccounts ON fin_bankdeposit_details.intAccount = mst_financechartofaccounts.intId
				WHERE
				fin_bankdeposit_details.intRecvFrom =  '$supplierId' AND
				(mst_financechartofaccounts.intFinancialTypeId = '26' OR mst_financechartofaccounts.intFinancialTypeId = '16' OR mst_financechartofaccounts.intFinancialTypeId = '17' OR mst_financechartofaccounts.intFinancialTypeId = '28') AND
				BND.intStatus = '1'
				AND BND.intCompanyId =  '$companyId'
				having balAmount<>0
		";
		$result = $db->RunQuery($sql); // mst_financechartofaccounts.intFinancialTypeId =  '18' AND
		while($row=mysqli_fetch_array($result))
		{
			$deposit = rawurlencode($row['strDepositNo']);
			$displayNo = $row['strDepositNo'].$row['refNo'];
			$amount = number_format($row['dblAmmount'],4,'.','');
			$toBePaid = number_format(($row['balAmount']),4,'.','');
			$date = substr($row['dtDate'],0,10);
			$currency = $row['strCode'];
			$rate = $row['dblRate'];
			
			$docRefNo = $row['strFnRefNo'];
			
			echo 
				"
				<tr class=normalfnt bgcolor=#99CCFF>
				<td align=center><input class=checkRow type=checkbox id=$deposit /></td>
				<td align=center class=po id=$deposit></td>
				<td align=center class=grn id=$deposit></td>
			<td width=180 align=center class=docNo id=$deposit><a target=_blank href=../../bank/deposit/deposit.php?depositNo=$deposit>$displayNo</a></td>
				<td align=center class=docDate>$date</td>
				<td align=right class=amount>$amount</td>
				<td align=right class=toBePaid>$toBePaid</td>
				<td align=center class=docCurrency>$currency</td>
				<td align=right class=docRate>$rate</td>
				<td align=center><input disabled=disabled style=width:100%;text-align:right;background-color:#FFF;border:none name=txtPayAmount  class=payAmount type=text id=txt$invoice /></td>
				<td align=center class=docType id=$docRefNo>B.Deposit</td>
				</tr>
				";
		}
		///////////////////////////////////////////////////////////////////////
		
		//////////////////////////Bank Payments////////////////////////////////
		$sql = "SELECT
				BNP.strBankPaymentNo,
				fin_bankpayment_details.intAccountId,
				BNP.dtDate,
				BNP.dblRate,
				BNP.intCurrency,
				BNP.dblReceivedAmount,
				mst_financecurrency.intId,
				mst_financecurrency.strCode,
				fin_bankpayment_details.strBankPaymentNo,
				fin_bankpayment_details.intPayTo,
				fin_bankpayment_details.dblAmmount,
				mst_financechartofaccounts.intFinancialTypeId,
				mst_financechartofaccounts.intId,
				BNP.strPerfInvoiceNo,
				IFNULL(CONCAT(' - ref ',BNP.strPerfInvoiceNo),'') AS refNo,
				(
				fin_bankpayment_details.dblAmmount 
				+
				IFNULL ((SELECT
				Sum(fin_other_payable_payments_main_details.dblPayAmount )AS paidAmount
				FROM fin_other_payable_payments_main_details
				Inner Join fin_other_payable_payments_header ON fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
				WHERE
				fin_other_payable_payments_main_details.strDocNo =  BNP.strBankPaymentNo AND
				fin_other_payable_payments_header.intDeleteStatus =  '0' AND fin_other_payable_payments_main_details.strDocType = 'B.Payment'
				GROUP BY
				fin_other_payable_payments_main_details.strDocNo),0)
				
				) AS balAmount
				FROM
				fin_bankpayment_header BNP
				Inner Join fin_bankpayment_details ON fin_bankpayment_details.strBankPaymentNo = BNP.strBankPaymentNo
				Inner Join mst_financecurrency ON BNP.intCurrency = mst_financecurrency.intId
				Inner Join mst_financechartofaccounts ON fin_bankpayment_details.intAccountId = mst_financechartofaccounts.intId
				WHERE
				fin_bankpayment_details.intPayTo =  '$supplierId' AND
				(mst_financechartofaccounts.intFinancialTypeId = '26' OR mst_financechartofaccounts.intFinancialTypeId = '16' OR mst_financechartofaccounts.intFinancialTypeId = '17' OR mst_financechartofaccounts.intFinancialTypeId = '28') AND
				BNP.intStatus = '1'
				AND BNP.intCompanyId =  '$companyId'
				having balAmount<>0
		";
		$result = $db->RunQuery($sql); // mst_financechartofaccounts.intFinancialTypeId =  '18' AND
		while($row=mysqli_fetch_array($result))
		{
			$payment = rawurlencode($row['strBankPaymentNo']);
			$displayNo = $row['strBankPaymentNo'].$row['refNo'];
			$amount = number_format($row['dblAmmount'],4,'.','');
			$toBePaid = number_format(($row['balAmount']*(-1)),4,'.','');
			$date = substr($row['dtDate'],0,10);
			$currency = $row['strCode'];
			$rate = $row['dblRate'];
			
			$docRefNo = $row['strPerfInvoiceNo'];
			
			echo 
				"
				<tr class=normalfnt bgcolor=#FDF5E6>
				<td align=center><input class=checkRow type=checkbox id=$payment /></td>
				<td align=center class=po id=$payment></td>
				<td align=center class=grn id=$payment></td>
		<td width=180 align=center class=docNo id=$payment><a target=_blank href=../../bank/payments/payments.php?paymentNo=$payment>$displayNo</a></td>
				<td align=center class=docDate>$date</td>
				<td align=right class=amount>$amount</td>
				<td align=right class=toBePaid>$toBePaid</td>
				<td align=center class=docCurrency>$currency</td>
				<td align=right class=docRate>$rate</td>
				<td align=center><input disabled=disabled style=width:100%;text-align:right;background-color:#FFF;border:none name=txtPayAmount  class=payAmount type=text id=txt$invoice /></td>
				<td align=center class=docType id=$docRefNo>B.Payment</td>
				</tr>
				";
		}
		///////////////////////////////////////////////////////////////////////
		
		//////////////////////////Petty Cash//////////////////////////////////
		$sql = "SELECT
				PTC.strPettyCashNo,
				PTC.dtDate,
				PTC.dblRate,
				PTC.intCurrency,
				PTC.dblReceivedAmount,
				fin_bankpettycash_details.intAccountId,
				fin_bankpettycash_details.intPayTo,
				fin_bankpettycash_details.dblAmmount,
				mst_financecurrency.intId,
				mst_financecurrency.strCode,
				mst_financechartofaccounts.intId,
				mst_financechartofaccounts.intFinancialTypeId,
				PTC.strFnRefNo,
				IFNULL(CONCAT(' - ref ',PTC.strFnRefNo),'') AS refNo,
				(
				fin_bankpettycash_details.dblAmmount 
				+
				IFNULL ((SELECT
				Sum(fin_other_payable_payments_main_details.dblPayAmount )AS paidAmount
				FROM fin_other_payable_payments_main_details
				Inner Join fin_other_payable_payments_header ON fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
				WHERE
				fin_other_payable_payments_main_details.strDocNo =  PTC.strPettyCashNo AND
				fin_other_payable_payments_header.intDeleteStatus =  '0' AND fin_other_payable_payments_main_details.strDocType = 'Petty Cash'
				GROUP BY
				fin_other_payable_payments_main_details.strDocNo),0)
				
				) AS balAmount
				FROM
				fin_bankpettycash_header AS PTC
				Inner Join fin_bankpettycash_details ON fin_bankpettycash_details.strPettyCashNo = PTC.strPettyCashNo
				Inner Join mst_financechartofaccounts ON fin_bankpettycash_details.intAccountId = mst_financechartofaccounts.intId
				Inner Join mst_financecurrency ON PTC.intCurrency = mst_financecurrency.intId
				WHERE
				(mst_financechartofaccounts.intFinancialTypeId = '26' OR mst_financechartofaccounts.intFinancialTypeId = '16' OR mst_financechartofaccounts.intFinancialTypeId = '17' OR mst_financechartofaccounts.intFinancialTypeId = '28')
				AND
				fin_bankpettycash_details.intPayTo =  '$supplierId' AND
				PTC.intStatus = '1'
				AND PTC.intCompanyId =  '$companyId'
				having balAmount<>0
				";
		$result = $db->RunQuery($sql); // mst_financechartofaccounts.intFinancialTypeId =  '18'
		while($row=mysqli_fetch_array($result))
		{
			$pettyCash = rawurlencode($row['strPettyCashNo']);
			$displayNo = $row['strPettyCashNo'].$row['refNo'];
			$amount = number_format($row['dblAmmount'],4,'.','');
			$toBePaid = number_format(($row['balAmount']*(-1)),4,'.','');
			$date = substr($row['dtDate'],0,10);
			$currency = $row['strCode'];
			$rate = $row['dblRate'];
			
			$docRefNo = $row['strFnRefNo'];
			
			echo 
				"
				<tr class=normalfnt bgcolor=#CDCDC1>
				<td align=center><input class=checkRow type=checkbox id=$pettyCash /></td>
				<td align=center class=po id=$pettyCash></td>
				<td align=center class=grn id=$pettyCash></td>
                                <td width=180 align=center class=docNo id=$pettyCash><a target=_blank href=../../bank/pettyCash/pettyCash.php?pettyCashNo=$pettyCash>$displayNo</a></td>
				<td align=center class=docDate>$date</td>
				<td align=right class=amount>$amount</td>
				<td align=right class=toBePaid>$toBePaid</td>
				<td align=center class=docCurrency>$currency</td>
				<td align=right class=docRate>$rate</td>
				<td align=center><input disabled=disabled style=width:100%;text-align:right;background-color:#FFF;border:none name=txtPayAmount  class=payAmount type=text id=txt$invoice /></td>
				<td align=center class=docType id=$docRefNo>Petty Cash</td>
				</tr>
				";
		}
		///////////////////////////////////////////////////////////////////////
                
        //////////////////////////Jurnel Entry////////////////////////////////
		$sql = "SELECT
				JH.strReferenceNo,
				JH.dtmDate,
				JH.dblRate,
				JH.intCurrencyId,
				mst_financecurrency.strCode,
				JD.dblDebitAmount,
				JD.dbCreditAmount,
				JH.strFnRefNo,
				IFNULL(CONCAT(' - ref ',JH.strFnRefNo),'') AS refNo,
				(
				JD.dbCreditAmount - JD.dblDebitAmount -
				IFNULL ((SELECT
				Sum(fin_other_payable_payments_main_details.dblPayAmount )AS paidAmount
				FROM
				fin_other_payable_payments_main_details
				Inner Join fin_other_payable_payments_header ON fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
				Inner Join fin_accountant_journal_entry_details ON fin_other_payable_payments_main_details.strDocNo = fin_accountant_journal_entry_details.strReferenceNo
				WHERE
				fin_other_payable_payments_main_details.strDocNo =  'JH.strReferenceNo' AND
				fin_other_payable_payments_header.intDeleteStatus =  '0' AND
				fin_other_payable_payments_main_details.strDocType =  'JN' AND
				fin_other_payable_payments_header.intSupplierId =  '$supplierId' AND
				fin_accountant_journal_entry_details.strPersonType =  'osup'
				GROUP BY
				fin_other_payable_payments_main_details.strDocNo),0)
				) AS balAmount
				FROM
				fin_accountant_journal_entry_header AS JH
				INNER JOIN fin_accountant_journal_entry_details AS JD ON JH.strReferenceNo = JD.strReferenceNo
				INNER JOIN mst_financecurrency ON JH.intCurrencyId = mst_financecurrency.intId
				INNER JOIN mst_financechartofaccounts ON JD.intChartOfAccountId = mst_financechartofaccounts.intId
				WHERE
				JD.strPersonType = 'osup' AND
				JD.intNameId = $supplierId AND
				JH.intDeleteStatus = 0 AND 
				(mst_financechartofaccounts.intFinancialTypeId = '26' OR mst_financechartofaccounts.intFinancialTypeId = '16' OR mst_financechartofaccounts.intFinancialTypeId = '17' OR mst_financechartofaccounts.intFinancialTypeId = '28') 
				AND JH.intCompanyId =  '$companyId'
				HAVING
				balAmount <> 0";
		$result = $db->RunQuery($sql); // mst_financechartofaccounts.intFinancialTypeId = 18
		while($row=mysqli_fetch_array($result))
		{
			$payment = rawurlencode($row['strReferenceNo']);
			$displayNo = $row['strReferenceNo'].$row['refNo'];
			
			$docRefNo = $row['strFnRefNo'];
			
			$amount = number_format(($row['dbCreditAmount'] + $row['dblDebitAmount']),4,'.','');
			//if($row['dblDebitAmount']!=0){
                            //$toBePaid = number_format(($row['balAmount']*(-1)),4,'.','');
                        //}
                        //else{
                            $toBePaid = number_format(($row['balAmount']),4,'.','');
                        //}
			$date = substr($row['dtmDate'],0,10);
			$currency = $row['strCode'];
			$rate = $row['dblRate'];
			
			echo 
				"
				<tr class=normalfnt bgcolor=#FDF5E6>
				<td align=center><input class=checkRow type=checkbox id=$payment /></td>
				<td width=170 align=center class=jobNo id=$payment></td>
                <td align=center class=po id=$payment></td>
				<td width=170 align=center class=docNo id=$payment><a target=_blank href=../../accountant/journalEntry/journalEntry.php?strReferenceNo=$payment>$displayNo</a></td>
				<td align=center class=docDate>$date</td>
				<td align=right class=amount>$amount</td>
				<td align=right class=toBePaid>$toBePaid</td>
				<td align=center class=docCurrency>$currency</td>
				<td align=right class=docRate>$rate</td>
				<td align=center><input disabled=disabled style=width:80px;text-align:right;background-color:#FFF;border:none name=txtPayAmount  class=payAmount type=text id=txt$invoice /></td>
				<td align=center class=docType id=$docRefNo>JN</td>
				</tr>                      
                       
				";
		}
		///////////////////////////////////////////////////////////////////////
	}
	//---------------------------------
	function callTaxValue($taxId)
	{
		global $db;
		$sql = "SELECT
				mst_financetaxisolated.intId,
				mst_financetaxisolated.strCode,
				mst_financetaxisolated.dblRate
				FROM
				mst_financetaxisolated
				WHERE
				mst_financetaxisolated.intId = '$taxId'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$taxVal = $row['dblRate'];	
		return $taxVal;
	}
?>
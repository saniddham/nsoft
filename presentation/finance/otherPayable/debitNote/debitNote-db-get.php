<?php

session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$locationId = $_SESSION["CompanyID"];
$companyId = $_SESSION["headCompanyId"];

$thisFilePath = $_SERVER['PHP_SELF'];
include "{$backwardseperator}dataAccess/Connector.php";

$requestType = $_REQUEST['requestType'];

if ($requestType == 'loadCombo') {
    $sql = "SELECT DISTINCT
			fin_other_payable_debitnote_header.strDebitNoteNo,
			mst_finance_service_supplier.strName
			FROM
			fin_other_payable_debitnote_header
			Inner Join mst_finance_service_supplier ON fin_other_payable_debitnote_header.intSupplier = mst_finance_service_supplier.intId
			WHERE
			fin_other_payable_debitnote_header.intStatus =  '1' AND
			fin_other_payable_debitnote_header.intCompanyId =  '$companyId'
			order by strDebitNoteNo desc
			";
    $result = $db->RunQuery($sql);
    $html = "<option value=\"\"></option>";
    while ($row = mysqli_fetch_array($result)) {
        $html .= "<option value=\"" . $row['strDebitNoteNo'] . "\">" . $row['strDebitNoteNo']." - ".$row['strName']. "</option>";
    }
    echo $html;
} 

//===========Add by dulakshi 2013.03.19=========
	else if($requestType=='loadSupplier')
	{
		$ledAcc  = $_REQUEST['ledgerAcc'];		
							
		$condition == "";
			if($ledAcc != "")
			{	
				$condition .= "AND mst_finance_service_supplier_activate.intChartOfAccountId =  '$ledAcc'";			
			}
				
		$sql = "SELECT
					mst_finance_service_supplier.intId,
					mst_finance_service_supplier.strName,
					mst_finance_service_supplier_activate.intCompanyId
				FROM
					mst_finance_service_supplier
						Inner Join mst_finance_service_supplier_activate ON mst_finance_service_supplier.intId = mst_finance_service_supplier_activate.intSupplierId
				WHERE
					mst_finance_service_supplier.intStatus =  '1' AND
					mst_finance_service_supplier_activate.intCompanyId =  '$companyId' " .$condition . " order by mst_finance_service_supplier.strName ASC";
				
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
	}
	//==============================================


else if ($requestType == 'getCustomerAddress') {
    $supplierId = $_REQUEST['supplierId'];

    $sql = "SELECT 	mst_finance_service_supplier.strAddress FROM mst_finance_service_supplier	WHERE mst_finance_service_supplier.intId =  '$supplierId'";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $response['address'] = $row['strAddress'];

    $sql = "SELECT
			fin_other_payable_bill_header.strReferenceNo,
			fin_other_payable_bill_header.strSupInvoice
			FROM fin_other_payable_bill_header
			WHERE
			fin_other_payable_bill_header.intSupplierId =  '$supplierId' AND
			fin_other_payable_bill_header.intCompanyId =  '$companyId' AND
			fin_other_payable_bill_header.intDeleteStatus =  '0'
			";
    $result = $db->RunQuery($sql);
    $salesInvoiceList = '<option value=""></option>';
    while ($row = mysqli_fetch_array($result)) {
        $salesInvoiceList .="<option value=\"" . $row['strReferenceNo'] . "\">" . $row['strSupInvoice'] . "</option>";
    }
    $response['salesInvoiceList'] = $salesInvoiceList;

    echo json_encode($response);
}
else if($requestType=='getInvoiceType') // invoice type
{
	$invoiceId  = $_REQUEST['invoiceId'];
	
	$sql = "SELECT
			fin_other_payable_bill_header.strReferenceNo,
			fin_other_payable_bill_header.strInvoiceType
			FROM
			fin_other_payable_bill_header
			WHERE
			fin_other_payable_bill_header.strReferenceNo =  '$invoiceId'";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	$response['invoType']= $row['strInvoiceType'];
	echo json_encode($response);
}

if ($requestType == 'getExchangeRate') {
    $currencyId = $_REQUEST['currencyId'];
    $exchangeDate = $_REQUEST['exchangeDate'];

    $sql = "SELECT
				mst_financeexchangerate.dblSellingRate,
				mst_financeexchangerate.dblBuying
			FROM mst_financeexchangerate
			WHERE
				mst_financeexchangerate.intCurrencyId 	=  '$currencyId' AND
				mst_financeexchangerate.dtmDate 		=  '$exchangeDate'
			";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);

    if (mysqli_num_rows($result) > 0) {
        $arrValue['sellingRate'] = $row['dblSellingRate'];
        $arrValue['buyingRate'] = $row['dblBuying'];
    } else {
        $arrValue['sellingRate'] = '';
        $arrValue['buyingRate'] = '';
    }

    echo json_encode($arrValue);
}

if ($requestType == 'getItemList') {
    $salesInvoiceNo = $_REQUEST['salesInvoiceNo'];
    $automated = $_REQUEST['automated'];
    $debitNo = $_REQUEST['debitNo'];     
    if($automated=='true'){//FOR AUTOMATED DEBIT NOTE
        $sql = "SELECT
                    mst_item.strCode,
                    mst_item.strName,
                    fin_other_payable_debitnote_details.intItem
                FROM
                    mst_item
                    INNER JOIN fin_other_payable_debitnote_details ON fin_other_payable_debitnote_details.intItem = mst_item.intId
                WHERE
                    fin_other_payable_debitnote_details.strDebitNoteNo = '$debitNo' ";         
        $result = $db->RunQuery($sql);
        $html = '';
        while ($row = mysqli_fetch_array($result)) {
            $html .= '<option value="' . $row['intItem'] . '">' . $row['strCode'] . '</option>';
        }
    }
    else
	{//NORMAL DEBIT NOTES
        $sql = "SELECT
                    fin_other_payable_bill_details.intItem,
                    mst_financesupplieritem.strName
            FROM
                    fin_other_payable_bill_details
                    Inner Join mst_financesupplieritem ON mst_financesupplieritem.intId = fin_other_payable_bill_details.intItem
            WHERE
                    fin_other_payable_bill_details.strReferenceNo =  '$salesInvoiceNo' AND
					fin_other_payable_bill_details.strItmType =  'Itm'
            ORDER BY
                    mst_financesupplieritem.intStatus ASC";        
        $result = $db->RunQuery($sql);
        $html = '<option value=""></option>';
        while ($row = mysqli_fetch_array($result))
		{
            $html .= '<option value="' .$row['intItem']."^"."Itm". '">' . $row['strName'] . '</option>';
        }
		
		$sql = "SELECT
				fin_other_payable_bill_details.intItem,
				mst_financechartofaccounts.strCode,
				mst_financechartofaccounts.strName
				FROM
				fin_other_payable_bill_details
				Inner Join mst_financechartofaccounts ON fin_other_payable_bill_details.intItem = mst_financechartofaccounts.intId
				WHERE
				fin_other_payable_bill_details.strReferenceNo =  '$salesInvoiceNo' AND
				fin_other_payable_bill_details.strItmType =  'Acc'
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$html.= "<option value=\"".$row['intItem']."^"."Acc"."\">".$row['strCode']." - ".$row['strName']."</option>";
		}
    }    
    $response['itemList'] = $html;
    echo json_encode($response);
} else if ($requestType == 'getTaxValue') {
    $operation = $_REQUEST['opType'];
    $amount = $_REQUEST['valAmount'];
    $taxCodes = json_decode($_REQUEST['arrTaxCode'], true);

    if (count($taxCodes) != 0) {
        foreach ($taxCodes as $taxCode) {
            $codeValues[] = callTaxValue($taxCode['taxId']);
        }
    }
    if (count($codeValues) > 1) {
        if ($operation == 'Inclusive') {
            $firstVal = ($amount * $codeValues[0]) / 100;
            $withTaxVal = $firstVal + ((($amount + $firstVal) * $codeValues[1]) / 100);
            $val1 = ($amount * $codeValues[0]) / 100;
            $val2 = ((($amount + $firstVal) * $codeValues[1]) / 100);
        } else if ($operation == 'Exclusive') {
            $withTaxVal = ($amount * ($codeValues[0] + $codeValues[1])) / 100;
            $val1 = ($amount * $codeValues[0]) / 100;
            $val2 = ($amount * $codeValues[1]) / 100;
        }
    } else if (count($codeValues) == 1 && $operation == 'Isolated') {
        $withTaxVal = ($amount * $codeValues[0]) / 100;
        $val1 = ($amount * $codeValues[0]) / 100;
    }
    echo $withTaxVal . "/" . $val1 . "/" . $val2;
} else if ($requestType == 'getTaxProcess') {
    $taxGrpId = $_REQUEST['taxGroupId'];

    $sql = "SELECT
				mst_financetaxgroup.intId,
				mst_financetaxgroup.strProcess
				FROM
				mst_financetaxgroup
				WHERE
				mst_financetaxgroup.intId =  '$taxGrpId'
				";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    echo $row['strProcess'];
}

function callTaxValue($taxId) {
    global $db;
    $sql = "SELECT
				mst_financetaxisolated.intId,
				mst_financetaxisolated.strCode,
				mst_financetaxisolated.dblRate
				FROM
				mst_financetaxisolated
				WHERE
				mst_financetaxisolated.intId = '$taxId'
				";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $taxVal = $row['dblRate'];
    return $taxVal;
}

<?php
session_start();
$backwardseperator = "../../../../../";
$companyId = $_SESSION['CompanyID'];
$locationId = $_SESSION['CompanyID'];
$intUser  = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
include  	"{$backwardseperator}dataAccess/Connector.php";

include_once "../../../commanFunctions/Converter.php";

$invoiceRefNo = $_REQUEST['id'];

$sql = "SELECT
		fin_other_payable_debitnote_header.strDebitNoteNo,
		fin_other_payable_debitnote_header.strInvoiceNo,
		mst_finance_service_supplier.strName AS cusName,
		mst_finance_service_supplier.strAddress,
		fin_other_payable_debitnote_header.dtDate,
		fin_other_payable_debitnote_header.dblRate,
		fin_other_payable_debitnote_header.strPoNo,
		fin_other_payable_debitnote_header.strGrnNo,
		mst_financecurrency.strCode AS currency,
		fin_other_payable_bill_header.strInvoiceType,
		fin_other_payable_bill_header.dtmDate,
		fin_other_payable_debitnote_header.strRemarks,
		fin_other_payable_bill_header.strSupInvoice,
		mst_financepaymentsterms.strName AS paymentsTerms,
		fin_other_payable_debitnote_header.intCreator,
		user1.intUserId,
		user1.strUserName AS creater,
		user2.strUserName AS modifyer
		FROM
		fin_other_payable_debitnote_header
		Left Outer Join mst_finance_service_supplier ON fin_other_payable_debitnote_header.intSupplier = mst_finance_service_supplier.intId
		Left Outer Join mst_financecurrency ON fin_other_payable_debitnote_header.intCurrency = mst_financecurrency.intId
		Left Outer Join fin_other_payable_bill_header ON fin_other_payable_debitnote_header.strInvoiceNo = fin_other_payable_bill_header.strReferenceNo
		Left Outer Join mst_financepaymentsterms ON fin_other_payable_bill_header.intPaymentsTermsId = mst_financepaymentsterms.intId
		Left Outer Join sys_users AS user1 ON fin_other_payable_debitnote_header.intCreator = user1.intUserId
		Left Outer Join sys_users AS user2 ON fin_other_payable_debitnote_header.intModifyer = user2.intUserId
		WHERE
		fin_other_payable_debitnote_header.strDebitNoteNo =  '$invoiceRefNo'";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$supplier 	= $row['cusName'];
	$debitDate 	= $row['dtDate'];
	$invoDate 	= $row['dtmDate'];
	$currency 	= $row['currency'];
	$poNo 		= $row['strPoNo'];
	$rate		= $row['dblRate'];
	$payTerms	= $row['paymentsTerms'];
	$grnNo		= $row['strGrnNo'];
	$supInvoice	= $row['strSupInvoice'];
	$ourRef		= $row['strInvoiceNo'];
	$creater	= $row['creater'];
	$modifyer	= $row['modifyer'];
	$memo		= $row['strRemarks'];
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Debit Note Report</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../../css/promt.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/script.js"></script>

<script src="../../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:239px;
	top:172px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>

<form id="frmPurchaseInvoiceDetails" name="frmPurchaseInvoiceDetails" method="post" action="salesInvoiceDetails.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include '../../../../../reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>OTHER PAYABLE / SERVICE SUPPLIER - DEBIT NOTE REPORT</strong><strong></strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
  <tr>
    <td colspan="9" align="center" bgcolor="#FFDFCB">
  </tr>
  <tr>
    <td width="1%">&nbsp;</td>
    <td width="19%"><span class="normalfnt"><strong>Debit Note  No.</strong></span></td>
    <td width="3%" align="center" valign="middle"><strong>:</strong></td>
    <td width="31%"><span class="normalfnt"><?php echo $invoiceRefNo ?></span></td>
    <td width="18%" class="normalfnt"><strong>Supplier</strong></td>
    <td width="3%" align="center" valign="middle"><strong>:</strong></td>
    <td width="23%"><span class="normalfnt"><?php echo $supplier ?></span></td>
    <td width="1%"><div id="divPoNo" style="display:none"><?php echo $poNo ?>/<?php echo $year ?></div></td>
  <td width="1%"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Date</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $debitDate  ?></span></td>
    <td><span class="normalfnt"><strong>Payment Terms</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo ($payTerms==''?0:$payTerms) ?> days</span></td>
    <td class="normalfnt">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Currency</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $currency  ?></span></td>
    <td><span class="normalfnt"><strong>Rate</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $rate  ?></span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
<tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>P.O. No.</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $poNo  ?></span></td>
    <td><span class="normalfnt"><strong>G.R.N. No.</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $grnNo ?></span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
<tr>
  <td height="22">&nbsp;</td>
  <td class="normalfnt"><strong>Memo</strong></td>
  <td align="center" valign="middle"><strong>:</strong></td>
  <td colspan="4"><span class="normalfnt"><?php echo $memo  ?></span></td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
</tr>
<tr>
  <td>&nbsp;</td>
  <td class="normalfnt"><strong>Invoice Number</strong></td>
  <td align="center" valign="middle"><strong>:</strong></td>
  <td><span class="normalfnt"><?php echo $supInvoice ?></span></td>
  <td><span class="normalfnt"><strong>Invoice Date</strong></span></td>
  <td align="center" valign="middle"><strong>:</strong></td>
  <td><span class="normalfnt"><?php echo $invoDate  ?></span></td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
</tr>
<tr>
  <td>&nbsp;</td>
  <td class="normalfnt"><strong>Our Reference Number</strong></td>
  <td align="center" valign="middle"><strong>:</strong></td>
  <td colspan="2"><span class="normalfnt"><?php echo $ourRef ?></span></td>
  <td align="center" valign="middle">&nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
  <td>&nbsp;</td>
</tr>
  </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td width="1%">&nbsp;</td>
        <td colspan="7" class="normalfnt">
          <table width="100%" class="grid" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr>
              <td width="38%"  height="22" bgcolor="#FFFFFF"><strong>Item</strong></td>
              <td width="9%" bgcolor="#FFFFFF" class="normalfntMid"><strong>UOM</strong></td>
              <td width="11%" bgcolor="#FFFFFF" class="normalfntRight"><strong>Unit Price</strong></td>
              <td width="10%" bgcolor="#FFFFFF" class="normalfntRight"><strong>Discount(%)</strong></td>
              <td width="12%" bgcolor="#FFFFFF" class="normalfntMid"> <strong>Tax Code</strong></td>
              <td width="9%" bgcolor="#FFFFFF" class="normalfntRight"><strong>Qty</strong></td>
              <td width="11%" bgcolor="#FFFFFF" class="normalfntRight"><strong>Amount</strong></td>
              </tr>
            <?php 
			$totalTax = 0;
			//$operation = "";
	  	  	$sql1 = "SELECT
					fin_other_payable_debitnote_details.strDebitNoteNo,
					fin_other_payable_debitnote_details.intItem,
					fin_other_payable_debitnote_details.intUOM,
					fin_other_payable_debitnote_details.dblQty,
					fin_other_payable_debitnote_details.dblRate AS dblUnitPrice,
					fin_other_payable_debitnote_details.dblDiscount,
					fin_other_payable_debitnote_details.dblTax AS dblTaxAmount,
					fin_other_payable_debitnote_details.intTaxGroup,
					fin_other_payable_debitnote_details.intDimension,
					mst_financetaxgroup.intId,
					mst_financetaxgroup.strCode AS taxCode,
					mst_financetaxgroup.strProcess,
					mst_financedimension.intId,
					mst_financedimension.strName,
					fin_other_payable_debitnote_header.dblRate,
					mst_financesupplieritem.intId,
					mst_financesupplieritem.strName AS itemName,
					mst_units.intId,
					mst_units.strName AS uom,
					fin_other_payable_debitnote_details.strItmType
					FROM
					fin_other_payable_debitnote_details
					left outer Join fin_other_payable_debitnote_header ON fin_other_payable_debitnote_details.strDebitNoteNo = fin_other_payable_debitnote_header.strDebitNoteNo
					left outer Join mst_financetaxgroup ON fin_other_payable_debitnote_details.intTaxGroup = mst_financetaxgroup.intId
					left outer Join mst_financedimension ON fin_other_payable_debitnote_details.intDimension = mst_financedimension.intId
					left outer Join mst_financesupplieritem ON fin_other_payable_debitnote_details.intItem = mst_financesupplieritem.intId
					left outer Join mst_units ON fin_other_payable_debitnote_details.intUom = mst_units.intId
					WHERE
					fin_other_payable_debitnote_details.strDebitNoteNo =  '$invoiceRefNo'
					";
			$result1 = $db->RunQuery($sql1);

			$totQty=0;
			$totAmmount=0;
		while($row=mysqli_fetch_array($result1))
		{
			$subAmount = (($row['dblUnitPrice'])*((100-$row['dblDiscount'])/100))*$row['dblQty'];
			$totalTax = $totalTax + $row['dblTaxAmount'];
			if($row['strItmType'] == "Acc")
			{
				$sqlAcc = "SELECT
							CONCAT(mst_financechartofaccounts.strCode,'-' ,mst_financechartofaccounts.strName) AS acc
							FROM
							fin_other_payable_debitnote_details
							Inner Join mst_financechartofaccounts ON fin_other_payable_debitnote_details.intItem = mst_financechartofaccounts.intId
							WHERE
							fin_other_payable_debitnote_details.strDebitNoteNo =  '$invoiceRefNo' AND
							fin_other_payable_debitnote_details.strItmType =  'Acc'";
				$resultAcc = $db->RunQuery($sqlAcc);
				while($rowAcc=mysqli_fetch_array($resultAcc))
				{
					$itmName = $rowAcc['acc'];
				}
				$itmDesc = "";
			}
			else
			{
				$itmName = $row['itemName'];
				$itmDesc = $row['strItemDesc'];
			}
	  ?>
	  <tr class="normalfnt"  bgcolor="#FFFFFF">
   	  <td class="normalfnt">&nbsp;<?php echo  $itmName ?>&nbsp;</td>
      <td class="normalfntMid" >&nbsp;<?php echo $row['uom'] ?>&nbsp;</td>
      <td class="normalfntRight" >&nbsp;<?php echo $row['dblUnitPrice'] ?></td>
      <td class="normalfntRight" >&nbsp;<?php echo $row['dblDiscount'] ?>&nbsp;</td>
      <td class="normalfntMid" >&nbsp;<?php echo $row['taxCode'] ?>&nbsp;</td>
      <td class="normalfntRight" >&nbsp;<?php echo $row['dblQty'] ?></td>
      <td class="normalfntRight" >&nbsp;<?php echo number_format($subAmount, 2) ?></td>
</tr>
      <?php 
			$totQty+=$row['dblQty'];
			$totAmmount+=(($row['dblUnitPrice'])*((100-$row['dblDiscount'])/100))*$row['dblQty'];
			}
	  ?>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $totQty ?></td>
              <td class="normalfntRight" >&nbsp;<?php echo number_format($totAmmount, 2) ?></td>
              </tr>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td colspan="6" class="normalfntRight" ><strong>Total Tax Amount</strong></td>
              <td class="normalfntRight" ><strong>&nbsp;<?php echo number_format($totalTax, 2) ?></strong></td>
            </tr>
            <tr class="normalfnt"  bgcolor="#F5F5F5">
              <td colspan="6" bgcolor="#FFFFFF" class="normalfntRight" ><strong>Total Amount</strong></td>
              <td bgcolor="#FFFFFF" class="normalfntRight" ><strong>&nbsp;<?php echo number_format(($totalTax + $totAmmount), 2) ?></strong></td>
            </tr>
            <tr class="normalfnt"  bgcolor="#F5F5F5">
              <td colspan="6" bgcolor="#FFFFFF" class="normalfntRight" >&nbsp;</td>
              <td bgcolor="#FFFFFF" class="normalfntRight" >&nbsp;</td>
            </tr>
            <tr class="normalfnt"  bgcolor="#F5F5F5">
              <td colspan="7" bgcolor="#FFFFFF" class="normalfntRight" ><span class="normalfntGrey">
            <?php
				echo "Exchange Rate: <strong>Rs. ".number_format($rate,4).""."/".$symbol." '".number_format(($totalTax + $totAmmount)*$rate, 2)."'</strong>";
			?>
             <br />
			(<?php
				$val = ($totalTax + $totAmmount)*$rate;
				$inWord= convert_number($val);   
				$sence=explode(".",number_format($val,2));
				$sent=convert_number($sence[1]);
				if(strlen($sence[1])>0)
				{
					$inWord=$inWord." and ".$sent." cents";
				}
			   echo $inWord;
			   ?>)</span></td>
              </tr>
            </table>
          </td>
        <td width="1%">&nbsp;</td>
        </tr>
      </table>
    </td>
</tr>
<tr>
<td>
<table width="100%">
<tr>
    <td align="left" class="normalfnt">&nbsp;</td>
    <td align="center" class="normalfntMid">
    <?php
	if($modifyer == NULL)
	{
		echo $creater;
	}
	else
	{
		echo $modifyer;
	}
	?>
    </td>
    <td width="30%" align="center" class="normalfntMid">&nbsp;</td>
    <td width="34%" align="right" class="normalfntRight">&nbsp;</td>
    <td width="34%" align="right" class="normalfntRight">&nbsp;</td>
    <td width="34%" align="right" class="normalfntRight">&nbsp;</td>
    </tr>
<tr>
<td align="left" class="normalfnt">&nbsp;</td>
<td align="center" class="normalfntMid">........................................</td>
<td align="center" class="normalfntMid">....................................................</td>
<td align="right" class="normalfntMid">............................</td>
<td align="right" class="normalfntMid">............................</td>
<td align="right" class="normalfntMid">............................</td>
</tr>
<tr>
<td align="left" class="normalfnt">&nbsp;</td>
<td align="center" class="normalfntMid">Prepared By</td>
<td align="center" class="normalfntMid">Authorized by Accountant</td>
<td align="right" class="normalfntMid">Authorized By</td>
<td align="right" class="normalfntMid">Received By</td>
<td align="right" class="normalfntMid">Date</td>
</tr>
</table>
</td>
</tr>
<tr>
  <td class="normalfntGrey" align="center"><br /><br /></td>
</tr>
<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
</body>
</html>
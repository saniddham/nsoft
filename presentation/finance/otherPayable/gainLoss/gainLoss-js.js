// JavaScript Document
var toBePaidAmount = '';
function functionList()
{
	if(recRefNo!='')
	{
		$('#frmGainLoss #cboSearch').val(recRefNo);
		$('#frmGainLoss #cboSearch').change();
	}
}
var rows = 1;
function insertRow()
{
	var tbl = document.getElementById('tblMainGrid2');	
	rows = tbl.rows.length;
	tbl.insertRow(rows);
	tbl.rows[rows].innerHTML = tbl.rows[rows-1].innerHTML;
//	loadJs();
}
$(document).ready(function() {
	
	var id = '';
	$("#frmGainLoss").validationEngine();
	$('#frmGainLoss #cboSupplier').focus();

$('.delImg').live('click',function(){
	var rowId = $(this).parent().parent().parent().find('tr').length;
	if(rowId!=2)
	$(this).parent().parent().remove();
	finalAmount();
});

$('.delImg').css('cursor', 'pointer');

  //permision for add 
  if(intAddx)
  {
 	$('#frmGainLoss #butNew').show();
	$('#frmGainLoss #butSave').show();
  }
  //permision for edit 
  if(intEditx)
  {
  	$('#frmGainLoss #butSave').show();
	$('#frmGainLoss #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
  //permision for delete
  if(intDeletex)
  {
  	$('#frmGainLoss #butDelete').show();
	$('#frmGainLoss #cboSearch').removeAttr('disabled');
  }
  //permision for view
  if(intViewx)
  {
	$('#frmGainLoss #cboSearch').removeAttr('disabled');
  }
  
  $('#frmGainLoss #chkEdit').click(function(){
	  if($('#frmGainLoss #chkEdit').attr('checked'))
	  {
		  $("#frmGainLoss #txtRate").attr("readonly","");
		  $('#frmGainLoss #txtRate').focus();
	  }
	  else
	  {
		  $('#frmGainLoss #txtRate').val('');
		  $("#frmGainLoss #txtRate").attr("readonly","readonly");
		  $('#frmGainLoss #cboCurrency').change();
	  }
  });

$(".checkRow").live('click',function(){
if($(this).attr("checked") == true)
{
	$(this).parent().parent().addClass("highlight");
	receivedAmount();
}
else
{
	$(this).parent().parent().removeClass("highlight");
	receivedAmount();
}
});

///////////////////////////get supplier invoice////////////////////
$('#cboSupplier').change(function(){
	var url = "gainLoss-db-get.php?requestType=getInvoice&supplierId="+$(this).val();
	var obj = $.ajax({url:url,async:false});
	document.getElementById('allInvoice').innerHTML=obj.responseText;
});
////////////////////////////////////////////////////////////////////

//------------------------------------------------------------
   $('#frmGainLoss #cboPaymentsMethods').change(function(){
	var payMethod = $('#cboPaymentsMethods').val();
	if(payMethod==2)
	{
		document.getElementById("rwChequeDetails").style.display='';
	}
	else
	{
		document.getElementById("rwChequeDetails").style.display='none';
	}
  });
//----------------------------------------------------------

///////////////////////////get G/L amount//////////////////////////
$('.glAccount').live('change',function(){
	$('#txtAccAmount').val($('#txtRecAmount').val());
});
//////////////////////////////////////////////////////////////////

////////////////////////get exchange rate//////////////////////////
$('#cboCurrency').change(function(){
	var url = "gainLoss-db-get.php?requestType=getExchangeRate&currencyId="+$(this).val()+'&exchangeDate='+$('#txtDate').val();
	var obj = $.ajax({url:url,dataType:'json',success:function(json){
		
		$('#rdoBuying').val(json.buyingRate);
		$('#rdoSelling').val(json.sellingRate);
		$('#rdoSelling').click();
		},async:false});
});
///////////////////////////////////////////////////////////////////
$('.rdoRate').click(function(){
  $('#txtRate').val($(this).val());
});
//save button click event
$('#frmGainLoss #butSave').click(function(){
if(existingMsgDate == "")
{
//-------------------------------------------------------------------
	var receiptNO = "";
	var receiveAmount = "";
	var invoiceAmount = "";
			
 value="[ ";
	$('#tblMainGrid1 tr:not(:first)').each(function(){
		if ($(this).find('.checkRow').attr('checked')) 
		{
			receiptNo		= $(this).find(".refNo").attr('id');
			receiveAmount 	= $(this).find(".recAmount").html();
			invoiceAmount 	= $(this).find(".invAmount").html();	
			
			value += '{ "receiptNo":"'+receiptNo+'", "receiveAmount": "'+receiveAmount+'", "invoiceAmount": "'+invoiceAmount+'"},';
		}
	});
	
	value = value.substr(0,value.length-1);
	value += " ]";
//---------------------------------------------------------------------------
	var accId = "";
	var accAmount = "";
	var memo = "";
	var dimension = "";
			
 accValue="[ ";
	$('#tblMainGrid2 tr:not(:first)').each(function(){
		
		accId		= $(this).find(".glAccount").val();
		accAmount 	= $(this).find(".accAmount").val();
		memo 		= $(this).find(".memo").val();
		dimension 	= $(this).find(".dimension").val();
		
	accValue += '{ "accId":"'+accId+'", "accAmount": "'+accAmount+'", "memo": "'+memo+'", "dimension": "'+dimension+'"},';
	});
	
	accValue = accValue.substr(0,accValue.length-1);
	accValue += " ]";
//---------------------------------------------------------------------------
	var requestType = '';
	if ($('#frmGainLoss').validationEngine('validate'))
    {
		//showWaiting();
		if(value != '[ ]')
		{
			if((eval($('#txtRecAmount').val()) - eval($('#txtAccAmount').val()))==0)
			{
				$('#frmGainLoss #cboSupplier').attr("disabled","");
				if($('#txtNo').val()=='')
					requestType = 'add';
				else
					requestType = 'edit';
				
				var url = "gainLoss-db-set.php";
				var obj = $.ajax({
					url:url,
					dataType: "json",
					type:'post',  
					data:$("#frmGainLoss").serialize()+'&requestType='+requestType+'&cboSearch='+id+'&glDetail='+value+'&glAccDetail='+accValue,
					async:false,
					
					success:function(json){
							$('#frmGainLoss #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
							if(json.type=='pass')
							{
								//$('#frmGainLoss').get(0).reset(); hideWaitng();
								$('#frmGainLoss #cboSupplier').attr("disabled","disabled");
								var t=setTimeout("alertx()",1000);
								$('#txtNo').val(json.GLNo);
								loadCombo_frmGainLoss();
								return;
							}
							var t=setTimeout("alertx()",3000);
						},
					error:function(xhr,status){
						$('#frmGainLoss #cboSupplier').attr("disabled","disabled");
							
							$('#frmGainLoss #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
							var t=setTimeout("alertx()",3000);
						}		
					});
			}
			else
			{
				$('#frmGainLoss #butSave').validationEngine('showPrompt', 'You cannot allow this process untill received  amount and total amount are same','fail');
				var t=setTimeout("alertx()",5000);
			}
		}
		else
		{
			$('#frmGainLoss #butSave').validationEngine('showPrompt', 'You cannot allow this process!','fail');
			var t=setTimeout("alertx()",5000);
		}
	}
}
else
{
	$('#frmGainLoss #butSave').validationEngine('showPrompt', existingMsgDate,'fail');
	var t=setTimeout("alertx()",5000);
}
});
/////////////////////////////////////////////////////
//// load invoice details //////////////////////////
/////////////////////////////////////////////////////
$('#frmGainLoss #cboSearch').click(function(){
   $('#frmGainLoss').validationEngine('hide');
});
$('#frmGainLoss #cboSearch').change(function(){
	existingMsgDate = "";
$('#frmGainLoss').validationEngine('hide');
var url = "gainLoss-db-get.php";
if($('#frmGainLoss #cboSearch').val()=='')
{
	document.getElementById('allInvoice').innerHTML = "";
		$('#tblMainGrid2 >tbody >tr').each(function(){
			if($(this).index()!=0 && $(this).index()!=1 )
			{
				$(this).remove();
			}
		});
	$('#frmGainLoss').get(0).reset();return;	
}
$('#txtNo').val($(this).val());
var httpobj = $.ajax({
	url:url,
	dataType:'json',
	data:'requestType=loadDetails&id='+URLEncode($(this).val()),
	async:false,
	success:function(json)
	{
		//json  = eval('('+json+')');
		$('#frmGainLoss #cboSupplier').val(json.supplier);
		$('#frmGainLoss #cboSupplier').attr("disabled","disabled");
		$('#frmGainLoss #txtDate').val(json.date);
		$('#frmGainLoss #txtRemarks').val(json.remark);
		
		//--------------------------------------------------
		$('#tblMainGrid1 >tbody >tr').each(function(){
			if($(this).index()!=0 && $(this).index()!=1 )
			{
				$(this).remove();
			}
		});
		document.getElementById('allInvoice').innerHTML = "";
		var tBodyDetail = "";

		if(json.detailVal!=null)
		{
			var rowId = $('#tblMainGrid1').find('tr').length;
			var tbl = document.getElementById('tblMainGrid1');
			rows = $('#tblMainGrid1').find('tr').length;
			for(var j=0;j<=json.detailVal.length-1;j++)
			{
				tBodyDetail	= json.detailVal[j].tBodyDetail;
				if(j != json.detailVal.length)
				{
					document.getElementById('allInvoice').innerHTML+=tBodyDetail;
				}
			}
			receivedAmount();
		}
		else
		{
			
		}
	   //--------------------------------------------------
	   //--------------------------------------------------
		$('#tblMainGrid2 >tbody >tr').each(function(){
			if($(this).index()!=0 && $(this).index()!=1 )
			{
				$(this).remove();
			}
		});
		var chartAcc 	= "";
		var amount 		= "";
		var memo		= "";
		var dimension	= "";
		if(json.detailAccVal!=null)
		{
			var rowId = $('#tblMainGrid2').find('tr').length;
			var tbl = document.getElementById('tblMainGrid2');
			rows = $('#tblMainGrid2').find('tr').length;
			for(var j=0;j<=json.detailAccVal.length-1;j++)
			{
				chartAcc	= json.detailAccVal[j].chartAcc;
				amount		= json.detailAccVal[j].amount;
				memo		= json.detailAccVal[j].memo;
				dimension	= json.detailAccVal[j].dimension;
				if(j != json.detailAccVal.length-1)
				{
					tbl.insertRow(rows);
					tbl.rows[rows].innerHTML = tbl.rows[rows-1].innerHTML;
					tbl.rows[rows].cells[1].childNodes[1].value = chartAcc;
					tbl.rows[rows].cells[2].childNodes[1].value = amount;
					tbl.rows[rows].cells[3].childNodes[1].value = memo;
					tbl.rows[rows].cells[4].childNodes[1].value = dimension;
				}
				else
				{
					tbl.rows[1].cells[1].childNodes[1].value = chartAcc;
					tbl.rows[1].cells[2].childNodes[1].value = amount;
					tbl.rows[1].cells[3].childNodes[1].value = memo;
					tbl.rows[1].cells[4].childNodes[1].value = dimension;
				}
			}
			finalAmount();
		}
		else
		{
			
		}
	   //--------------------------------------------------
	}
});
});
//////////// end of load details /////////////////

  	$('#frmGainLoss #butNew').click(function(){
		existingMsgDate = "";
		$('#frmGainLoss').get(0).reset();
		document.getElementById('allInvoice').innerHTML = "";
		$('#tblMainGrid2 >tbody >tr').each(function(){
			if($(this).index()!=0 && $(this).index()!=1 )
			{
				$(this).remove();
			}
		});
		$('#frmGainLoss #cboPaymentsMethods').change();//--->
		$('#frmGainLoss #cboSupplier').attr("disabled","");
		loadCombo_frmGainLoss();
		$('#frmGainLoss #cboSupplier').focus();
	});
	$('#frmGainLoss #butDelete').click(function(){
		if($('#frmGainLoss #cboSearch').val()=='')
		{
			$('#frmGainLoss #butDelete').validationEngine('showPrompt', 'Please select G/L Number.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmGainLoss #cboSearch option:selected').text()+'" ?',{
			buttons: { Ok: true, Cancel: false },
			callback: function(v,m,f){
			if(v)
			{
					var url = "gainLoss-db-set.php";
					var httpobj = $.ajax({
						url:url,
						dataType:'json',
						data:'requestType=delete&cboSearch='+URLEncode($('#frmGainLoss #cboSearch').val()),
						async:false,
						success:function(json){
							
							$('#frmGainLoss #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
							
							if(json.type=='pass')
							{
								$('#frmGainLoss').get(0).reset();
								document.getElementById('allInvoice').innerHTML = "";
								$('#tblMainGrid2 >tbody >tr').each(function(){
									if($(this).index()!=0 && $(this).index()!=1 )
									{
										$(this).remove();
									}
								});
								$('#frmGainLoss #cboPaymentsMethods').change();//--->
								$('#frmGainLoss #cboSupplier').attr("disabled","");
								loadCombo_frmGainLoss();
								var t=setTimeout("alertDelete()",1000);return;
							}	
							var t=setTimeout("alertDelete()",3000);
						}	 
					});
			}
		}
			});	
		}
	});
});
////////////////////// calculation ////////////////////////////////
function receivedAmount()
{
	var receiveTotal = 0.0000;
	$(".gainLoss").each( function(){
		if($(this).parent().find('.checkRow').attr('checked'))
		{
			  receiveTotal += eval($(this).html()==''?0.0000:$(this).html());
		}
	});
	$('#txtRecAmount').val(receiveTotal.toFixed(4));
}
///////////////////////////////////////////////////////////////////
function loadCombo_frmGainLoss()
{
	var url 	= "gainLoss-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmGainLoss #cboSearch').html(httpobj.responseText);
}
function alertx()
{
	$('#frmGainLoss #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmGainLoss #butDelete').validationEngine('hide') ;
}
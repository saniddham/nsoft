$(document).ready(function() { 
    
     $('#frmSupUnadjustedGL #txtDate').blur(function(){         
         var theDate=$('#frmSupUnadjustedGL #txtDate').val();
         var url ='supUnadjustedGainLoss-db-get.php?requestType=loadCurrency&theDate='+theDate;
            var obj=$.ajax({
                url:url,
                dataType:'json',
                success:function(json){
                    $('#frmSupUnadjustedGL #curList').html(json.curList);
                    $('#frmSupUnadjustedGL #btnDisplay').attr("class","show");
                    
                },
                async:false
            });          
     });
     $('#frmSupUnadjustedGL #btnDisplay').click(function(){ 
         $('#divMaskProces').attr("class","maskShow mask");
         var theDate=$('#frmSupUnadjustedGL #txtDate').val();
         var data="&theDate="+theDate;
         var curIds=document.getElementsByName("txtCurId");
         for(var i=0;curIds.length > i;++i){
             var curId=curIds[i].value;
             var radios=document.getElementsByName("radio"+curId);
             for(var j=0;radios.length > j;++j){
                if(radios[j].checked){
                    var val=radios[j].value;                 
                    if(val=="bui"){data+="&curIds["+curId+"]="+  $('#frmSupUnadjustedGL #txtbuing'+curId).val();}
                    else if (val=="sel"){data+="&curIds["+curId+"]="+  $('#frmSupUnadjustedGL #txtseling'+curId).val();}   
                    else if (val=="oth"){data+="&curIds["+curId+"]="+  $('#frmSupUnadjustedGL #txtother'+curId).val();}  
                }
             }
         }
         var url ='supUnadjustedGainLoss-db-get.php?requestType=loadValues'+data;
            var obj=$.ajax({
                url:url,
                dataType:'json',
                success:function(json){                    
                    $('#frmSupUnadjustedGL #valueList').html(json.valueList);
                    $('#frmSupUnadjustedGL #btnGenerate').attr("class","show");
                    $('#divMaskProces').attr("class","maskHide mask"); 
                    $('#frmSupUnadjustedGL #btnGenerate').attr("disabled",false);
                },
                async:false
            });          
     });  
     
     //generate value
     $('#frmSupUnadjustedGL #btnGenerate').click(function(){
	if(existingMsgDate == "")
	{ 
         
         $('#divMaskProces').attr("class","maskShow mask");
         
         var theDate=$('#frmSupUnadjustedGL #txtDate').val();
         var data="&theDate="+theDate;
         var curIds=document.getElementsByName("txtCurId");
         for(var i=0;curIds.length > i;++i){
             var curId=curIds[i].value;
             var radios=document.getElementsByName("radio"+curId);
             for(var j=0;radios.length > j;++j){
                if(radios[j].checked){
                    var val=radios[j].value;                 
                    if(val=="bui"){data+="&curIds["+curId+"]="+  $('#frmSupUnadjustedGL #txtbuing'+curId).val();}
                    else if (val=="sel"){data+="&curIds["+curId+"]="+  $('#frmSupUnadjustedGL #txtseling'+curId).val();}   
                    else if (val=="oth"){data+="&curIds["+curId+"]="+  $('#frmSupUnadjustedGL #txtother'+curId).val();}  
                }
             }
         }
         var url ='supUnadjustedGainLoss-db-set.php?requestType=genEntres'+data;
            var obj=$.ajax({
                url:url,
                dataType:'json',
                success:function(json){
                    $('#divMaskProces').attr("class","maskHide mask");
                    $('#frmSupUnadjustedGL #btnGenerate').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
                    if(json.type=='pass')
                    {
                        $('#frmSupUnadjustedGL #btnGenerate').attr("disabled",true);
                        var count=json.reCcount;
                        var dis="<tr class='normalfntMid'><td colspan="+11+">"+ count +" Records Added</td></tr>";
                        $('#frmSupUnadjustedGL #valueList').html(dis);
                    }
                    var t=setTimeout("alertx()",3000);
                    
                },            
                error:function(xhr,status){
                    $('#divMaskProces').attr("class","maskHide mask");
                    $('#frmSupUnadjustedGL #btnGenerate').validationEngine('showPrompt', errormsg(xhr.status),'fail');
                    var t=setTimeout("alertx()",3000);
                }
            }); 
		}
	else
	{
		$('#frmSupUnadjustedGL #btnGenerate').validationEngine('showPrompt', existingMsgDate,'fail');
		var t=setTimeout("alertx()",5000);
	}         
     });        
     
    
});
function alertx()
{
	$('#frmSupUnadjustedGL #btnGenerate').validationEngine('hide')	;
}


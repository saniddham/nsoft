<?php
session_start();
$backwardseperator = "../../../../";
$mainPath 	= $_SESSION['mainPath'];
$userId 	= $_SESSION['userId'];
$requestType 	= $_REQUEST['requestType'];
$companyId = $_SESSION['headCompanyId'];
$location = $_SESSION['CompanyID'];
include "{$backwardseperator}dataAccess/Connector.php";

if($requestType=='loadOrders'){
    $ordNumber= trim($_REQUEST['ordNumber']);
    $ordYear=trim($_REQUEST['ordYear']); 
    /*$sql="  SELECT
                SF.intDocumentNo,
                SF.intDocumentYear,
                SF.intOrderNo,
                SF.intOrderYear,
                `ORD`.strSalesOrderNo,
                `ORD`.intSalesOrderId,
                `ORD`.strGraphicNo,
                `ORD`.strStyleNo,
                ORH.strCustomerPoNo,
                mst_customer.strName AS customer,
                mst_customer.strInvoiceType
            FROM
                ware_stocktransactions_fabric AS SF
                Inner Join mst_locations AS LOC ON SF.intLocationId = LOC.intId
                Inner Join trn_orderdetails AS `ORD` ON SF.intOrderNo = `ORD`.intOrderNo AND SF.intOrderYear = `ORD`.intOrderYear AND SF.intSalesOrderId = `ORD`.intSalesOrderId
                Inner Join trn_orderheader AS ORH ON `ORD`.intOrderNo = ORH.intOrderNo AND `ORD`.intOrderYear = ORH.intOrderYear
                Inner Join mst_customer ON ORH.intCustomer = mst_customer.intId
            WHERE
                SF.strType =  'Dispatched_G' AND
                SF.invoiced =  0 AND
                LOC.intCompanyId =  '$companyId' AND
                SF.intOrderNo =  '$ordNumber' AND
                SF.intOrderYear =  '$ordYear'";*/
    
    $sql="  SELECT
                DD.intSalesOrderId,
                Sum(DD.dblGoodQty) as disQty,
                DH.intOrderNo,
                DH.intOrderYear,
                DH.intBulkDispatchNo,
                DH.intBulkDispatchNoYear,
                OD.strSalesOrderNo,
                OD.strStyleNo,
                OD.strGraphicNo,
                OH.strCustomerPoNo,
                CUS.strName AS customer,
                CUS.strInvoiceType
            FROM
                ware_fabricdispatchheader AS DH
                Inner Join ware_fabricdispatchdetails AS DD ON DH.intBulkDispatchNo = DD.intBulkDispatchNo AND DH.intBulkDispatchNoYear = DD.intBulkDispatchNoYear
                Inner Join trn_orderdetails AS OD ON DH.intOrderNo = OD.intOrderNo AND DH.intOrderYear = OD.intOrderYear AND DD.intSalesOrderId = OD.intSalesOrderId
                Inner Join trn_orderheader AS OH ON OD.intOrderNo = OH.intOrderNo AND OD.intOrderYear = OH.intOrderYear
                Inner Join mst_customer AS CUS ON OH.intCustomer = CUS.intId
                Inner Join mst_locations AS LOC ON DH.intCompanyId = LOC.intId
            WHERE
                DH.intOrderNo =  '$ordNumber' AND
                DH.intOrderYear =  '$ordYear' AND
                DH.intStatus =  '1' AND
                LOC.intCompanyId =  '$companyId' AND
                DD.invoiced=0
            GROUP BY
                DD.intSalesOrderId,
                DH.intOrderNo,
                DH.intOrderYear,
                DH.intBulkDispatchNo,
                DH.intBulkDispatchNoYear";
    
    $result = $db->RunQuery($sql);
    $grnList="";
    $supplier;
    $poType;
    $dispatchList="<tr class=\"normalfnt mainRow\"><td></td><td>Dispatch</td><td>Sales Order</td><td>Graphic</td><td>Style</td><td>QTY</td></tr>";
    while($row=mysqli_fetch_array($result)){
        $disNo=$row['intBulkDispatchNo'];
        $disYear=$row['intBulkDispatchNoYear'];        
        $salesOrderId=$row['intSalesOrderId'];
        $salesOrderNo=$row['strSalesOrderNo'];
        $graphicNo=$row['strGraphicNo'];
        $styleNo=$row['strStyleNo'];
        $disQty=$row['disQty'];
        //-----------------------------------
        $customer=$row['customer'];
        $invType=$row['strInvoiceType'];
        $cusPo=($row['strCustomerPoNo']==''?'***':$row['strCustomerPoNo']);
        
        $chkValue=$disNo."-".$disYear."-".$salesOrderId;
        $dispatchList.= "<tr class=\"normalfnt mainRow\" bgcolor=#00FF66>
        <td width=\"5px\"><input type=\"checkbox\" class=\"salesOrder\" name=\"chkDispatch\" value=\"$chkValue\" /></td>
        <td class=\"selOrderNo\"><a target=\"_blank\"  href=\"{$backwardseperator}presentation/customerAndOperation/bulk/fabricDispatchNote/listing/rptFabricDispatchNote.php?serialNo=$disNo&year=$disYear\" >$disNo/$disYear</a></td>
        <td class=\"selOrderNo\">$salesOrderNo</td>
        <td class=\"selOrderNo\">$graphicNo</td>
        <td class=\"selOrderNo\">$styleNo</td>
        <td class=\"selOrderNo\">$disQty</td>
        </tr>
         ";
    }
    $disView="<a href=\"#\" >$disNumber/$disYear</a>";
    $poView="<a target=\"_blank\"  href=\"{$backwardseperator}presentation/customerAndOperation/bulk/placeOrder/listing/rptBulkOrder.php?orderNo=$ordNumber&orderYear=$ordYear\" >$cusPo</a>";
    $response['dispatchList']=$dispatchList;
    $response['customer']=$customer;
    $response['invType']=$invType;
    $response['cusPo']=$poView;
    $response['disView']=$disView;
    echo json_encode($response);
}
?>

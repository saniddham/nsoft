var amStatus = "Auto";
//permision for add 
if(intAddx)
{
    $('#frmSalesDayBook #butNew').show();
    $('#frmSalesDayBook #butSave').show();
}
//permision for edit 
if(intEditx)
{
    $('#frmSalesDayBook #butSave').show();
    $('#frmSalesDayBook #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
}
//permision for delete
if(intDeletex)
{
    $('#frmSalesDayBook #butDelete').show();
    $('#frmSalesDayBook #cboSearch').removeAttr('disabled');
}
//permision for view
if(intViewx)
{
    $('#frmSalesDayBook #cboSearch').removeAttr('disabled');
}
function alertx()
{
    $('#frmSalesDayBook #butSave').validationEngine('hide') ;
}
$(document).ready(function() {
    $("#frmSalesDayBook").validationEngine();
    
    //permision for add 
    if(intAddx)
    {
        $('#frmSalesDayBook #butSave').show();
    }
//--------------------------------------------------------------------------------------------
    $('#frmSalesDayBook #cmbDispatch').change(function(){
        //clear values
        $('#frmSalesDayBook #allSalesOrder').html("");                    
        $('#frmSalesDayBook #cus').html("");
        $('#frmSalesDayBook #invType').html("");
        $('#frmSalesDayBook #cusPoView').html("");                    
        $('#txtInvNo').val("");
        /////////////
        if($('#frmSalesDayBook #cmbDispatch').val()!=""){           
            
            var dispatch=($('#frmSalesDayBook #cmbDispatch').val()).split("-");
            var disNumber=dispatch[1];
            var disYear=dispatch[0];
            
            var url ='salesDayBook-db-get.php?requestType=loadOrders&ordNumber='+disNumber+"&ordYear="+disYear;
            var obj=$.ajax({
                url:url,
                dataType:'json',
                success:function(json){
                    $('#frmSalesDayBook #allSalesOrder').html(json.dispatchList);                    
                    $('#frmSalesDayBook #cus').html(json.customer);
                    $('#frmSalesDayBook #invType').html(json.invType);
                    $('#frmSalesDayBook #cusPoView').html(json.cusPo);                    
                    $('#txtInvNo').val("");
                },
                async:false
            });   
        }        
    });

 //===================================================================
 	$('#frmSalesDayBook #chkAutoManual').click(function(){
	  if($('#frmSalesDayBook #chkAutoManual').attr('checked'))
	  {
		  amStatus = "Auto";
		  $('#frmSalesDayBook #amStatus').val('Auto');
		  $('#frmSalesDayBook #txtInvNo').val('');
		  $("#frmSalesDayBook #txtInvNo").attr("readonly","readonly");
		  $('#frmSalesDayBook #txtInvNo').removeClass('validate[required]');
	  }
	  else
	  {
		  amStatus = "Manual";
		  $('#frmSalesDayBook #amStatus').val('Manual');
		  $('#frmSalesDayBook #txtInvNo').val('');
		  $("#frmSalesDayBook #txtInvNo").attr("readonly","");
		  $('#frmSalesDayBook #txtInvNo').focus();
		  $('#frmSalesDayBook #txtInvNo').addClass('validate[required]');
	  }
  });
 //===================================================================	

//-----------------------------save button click event--------------------------------------
$('#frmSalesDayBook #butSave').click(function(){       
		
    if ($('#frmSalesDayBook').validationEngine('validate')){ 
        var order=$('#frmSalesDayBook #cmbDispatch').val();
        var data="&order="+order;
        var chks=document.getElementsByName("chkDispatch");
        var x=0;
        
        for(var i=0;i<chks.length;++i){
            if(chks[i].checked){                
                data+="&dispatchNos["+ x++ +"]="+chks[i].value;
            }

        }
        
            var url = "salesDayBook-db-set.php";
            var obj = $.ajax({
                url:url,
                dataType: "json",  
                data:$("#frmSalesDayBook").serialize()+'&requestType=add'+data+'&amStatus='+amStatus,
                async:false,
			
                success:function(json){
                    $('#frmSalesDayBook #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
                    if(json.type=='pass')
                    {
                        //$('#frmSalesDayBook').get(0).reset();						
                        var t=setTimeout("alertx()",1000);
                        $('#txtInvNo').val(json.invoiceNo);
                        $('#frmSalesDayBook #allSalesOrder').html('');
                        return;
                    }
                    var t=setTimeout("alertx()",3000);
                },
                error:function(xhr,status){
					
                    $('#frmSalesDayBook #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
                    var t=setTimeout("alertx()",3000);
                }		
            });         
        }
    });
});

function alertx()
{
    $('#frmSalesDayBook #butSave').validationEngine('hide') ;
}
function alertDelete()
{
    $('#frmSalesDayBook #butSave').validationEngine('hide') ;
}

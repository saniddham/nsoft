<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$companyId = $_SESSION['headCompanyId'];
$location = $_SESSION['CompanyID'];
$thisFilePath =  $_SERVER['PHP_SELF'];
include  "{$backwardseperator}dataAccess/permisionCheck.inc";
// ======================Check Exchange Rate Updates========================

$currentDate = date("Y-m-d");
//
$sql = "SELECT COUNT(*) AS 'no'
		FROM
		mst_financeexchangerate
		WHERE
		mst_financeexchangerate.dtmDate =  '$currentDate'
		";
$result = $db->RunQuery($sql);
$row= mysqli_fetch_array($result);
if($row['no']==0)
	{
		$str =  "Please Update Exchange Rates Before Generating Invoice .";
		$str .= $row['NameList'];
		$maskClass="maskShow";
	}
	else
	{
		$maskClass="maskHide";
	}
// =========================================================================
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Sales Day Book(Dispatch)</title>
        
        <link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/tblstyle.css" rel="stylesheet" type="text/css" />

        <script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
        <script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
        <script src="salesDayBook.js" type="text/javascript"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

        <link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
        <script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

        <link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css">
        <link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css">   

    </head>

    <body>
        <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
                <td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator . 'Header.php'; ?></td>
            </tr> 
        </table>
        <div id="divMask" class="<?php echo $maskClass?> mask"> <?php echo $str; ?></div>

        <script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
        <script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
        <script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>
        <form id="frmSalesDayBook" name="frmSalesDayBook" method="post" action="salesDayBook-db-set.php" autocomplete="off">
            <div align="center">
                <div class="trans_layoutL">
                    <div class="trans_text">Sales Day Book</div>
                    <table width="100%">
                        <tr>
                            <td colspan="2">
                                <table width="100%" class="tableBorder_allRound">
                                    <tr>                                        
                                        <td bgcolor="#FFFFFF" class="normalfntRight">Order No :</td>
                                         <td bgcolor="#FFFFFF"><span class="normalfntMid">
                                                <select name="cmbDispatch" id="cmbDispatch" style="width:115px" class="validate[required]">
                                                    <option value=""></option>
                                                        <?php
                                                        $sql2 = "SELECT
                                                                    DISTINCT ware_fabricdispatchheader.intOrderNo,
                                                                    ware_fabricdispatchheader.intOrderYear
                                                                FROM
                                                                    ware_fabricdispatchheader
                                                                    Inner Join mst_locations ON ware_fabricdispatchheader.intCompanyId = mst_locations.intId
                                                                WHERE
                                                                    ware_fabricdispatchheader.intStatus =  '1' AND
                                                                    ware_fabricdispatchheader.invoiced =  '0' AND
                                                                    mst_locations.intCompanyId =  '$companyId'
                                                                ORDER BY
                                                                    ware_fabricdispatchheader.intOrderYear DESC,
                                                                    ware_fabricdispatchheader.intOrderNo DESC";
                                                        $result2 = $db->RunQuery($sql2);
                                                        while ($row2 = mysqli_fetch_array($result2)) {
                                                                echo "<option value=\"" . $row2['intOrderYear'] . "-" . $row2['intOrderNo'] . "\">" . $row2['intOrderNo'] . "/" . $row2['intOrderYear'] . "</option>";
                                                        }
									?>
                                                </select>
                                            </span></td>                                                                              
                                    </tr>                                    
                                     <tr>
                                        <td bgcolor="#FFFFFF" class="normalfntRight">View Customer Po :</td>
                                        <td bgcolor="#FFFFFF" class="normalfnt"><div id="cusPoView" class="po"></div></td>                                        
                                    </tr>
                                    <tr>
                                        <td bgcolor="#FFFFFF" class="normalfntRight">Customer :</td>
                                        <td bgcolor="#FFFFFF" class="normalfnt"><div id="cus"></div></td>                                        
                                    </tr>
                                    <tr>
                                        <td bgcolor="#FFFFFF" class="normalfntRight">Invoice Type :</td>
                                        <td bgcolor="#FFFFFF" class="normalfnt"><div id="invType"></div></td>                                        
                                    </tr>
                                    <tr>
                                        <td bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
                                        <td bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>                                        
                                    </tr>                                     
                                    <tr>  
                                        <td bgcolor="#FFFFFF" class="normalfntRight">Dispatch :</td> 
                                        <td valign="top" bgcolor="#FFFFFF">
                                            <div id="divGrid">
                                                <table width="100%" id="allSalesOrder">                                                     
                                                    <tbody class="normalfnt" bgcolor="#FFFFFF"><!-- load sales order -->
                                                    </tbody>
                                                </table>
                                            </div>              
                                        </td>                                        
                                    </tr>
                                    <tr>
                                        <td class="normalfntRight"><span class="name">Invoice Number :</span></td>
                                        <td bgcolor="#FFFFFF" class="normalfnt"><span class="normalfnt">
                                          <input name="txtInvNo" type="text" readonly="readonly"class="normalfntRight" id="txtInvNo" style="width:180px; background-color:#F4FFFF;text-align:center; border:dotted; border-color:#F00" />
                                          <input checked="checked" type="checkbox" name="chkAutoManual" id="chkAutoManual" />
                                          <input name="amStatus" type="text" class="normalfntBlue" id="amStatus" style="width:40px; background-color:#FFF; text-align:center; border:thin" disabled="disabled" value="(Auto)"/></td>                                        
                                    </tr>                            
                                    
                                </table>
                            </td>
                        </tr>                       
                        
                        <tr>
                            <td colspan="2">
                                <table width="100%">
                                    <tr>
                                        <td width="100%" height="34" class="tableBorder_allRound"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                                                <tr>
                                                    <td width="100%" align="center" bgcolor=""><img style="display:" border="0" src="../../../../images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/><img src="../../../../images/Tclose.jpg" alt="Close" name="Close" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                                                </tr>
                                            </table></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </form>
    </body>
</html>
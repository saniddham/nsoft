<?php

session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$userId = $_SESSION['userId'];
$companyId = $_SESSION['headCompanyId'];
$locationId = $_SESSION['CompanyID'];
$thisFilePath = $_SERVER['PHP_SELF'];
include "{$backwardseperator}dataAccess/Connector.php";

include "AccTaxCalculationFunctions.php";

$response = array('type' => '', 'msg' => '');
////////////////////////// main parameters ////////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	$amStatus 		= $_REQUEST['amStatus'];
	$invoice		= trim($_REQUEST['txtInvNo']);
///////////////////////////////////////////////////////////////////////////
try 
{
    if ($requestType == 'add'){        

        $dispatchNos	= $_REQUEST['dispatchNos'];
        $order	=  explode("-",$_REQUEST['order']);
        if(count($dispatchNos)>0){
            $custOrderYear=$order[0];
            $custOrderNumber=$order[1];           

            $accountPeriod 		= getLatestAccPeriod($companyId); 
            $invoiceNumber 		= getNextInvoiceNo($companyId, $locationId); 
			
			if($amStatus == "Auto")
			{
				$invoiceReference	= trim(encodeInvoiceNo($invoiceNumber,$accountPeriod,$companyId,$locationId));
			}
			else if($amStatus == "Manual")
			{
				$invoiceReference	= $invoice;
			}
            
            //get details for sales invoice header
            $sqlheader="SELECT
                            CUS.strInvoiceType,
                            OH.intCustomer,
                            CUS.strAddress,
                            OH.intCurrency,
                            OH.strCustomerPoNo,
                            OH.intMarketer,
                            CUS.intShipmentId,
                            CUS.intPaymentsTermsId,
                            OH.dtDeliveryDate
                        FROM
                            trn_orderheader AS OH
                            Inner Join mst_customer AS CUS ON OH.intCustomer = CUS.intId
                        WHERE
                            OH.intOrderNo =  '$custOrderNumber' AND
                            OH.intOrderYear =  '$custOrderYear'";
            $resultHeader = $db->RunQuery($sqlheader);
            $rowH     = mysqli_fetch_array($resultHeader);
            
            $customerId = $rowH['intCustomer'];
            $orderCurrency  = $rowH['intCurrency'];
            $delDate    = ($rowH['dtDeliveryDate']==null?'null':"'".$rowH['dtDeliveryDate']."'");
            $cusAddress = htmlspecialchars($rowH['strAddress'],ENT_QUOTES);
            $poNo       = $rowH['strCustomerPoNo'];
            $marketer   = ($rowH['intMarketer']==''?'null':$rowH['intMarketer']);
            $paymentsTerms = ($rowH['intPaymentsTermsId']==''?'null':$rowH['intPaymentsTermsId']);
            $cusInvoiceType=trim(($rowH['strInvoiceType']==''?'NULL':"'".$rowH['strInvoiceType']."'"));
            $shipmentMethod=trim(($rowH['intShipmentId']==''?'NULL':"'".$rowH['intShipmentId']."'"));
            
            $currentDate=date('Y-m-d');
            $remarks="Automated Dispatch - ";
		
            //get Currency details
            $resultCurr = $db->RunQuery("SELECT mst_financeexchangerate.dblBuying FROM mst_financeexchangerate WHERE mst_financeexchangerate.intCurrencyId =  '$orderCurrency' AND mst_financeexchangerate.dtmDate = '$currentDate' AND mst_financeexchangerate.intCompanyId = '$companyId'");
            $rowCurr = mysqli_fetch_array($resultCurr);
            $orderCurrencyRate = $rowCurr['dblBuying'];
            
            $db->begin();                
            //Add data to transaction header*******************************************
            $sql="INSERT INTO fin_transactions (entryDate, strProgramType, documentNo, currencyId, currencyRate, transDetails, payMethodId, paymentNumber, accPeriod, userId, companyId, createdOn,authorized) VALUES
                    ('$currentDate','Sales Invoice','$invoiceReference',$orderCurrency,$orderCurrencyRate,'$remarks',null,null,$accountPeriod,$userId,$companyId,now(),0)";
                
            $db->RunQuery2($sql);
            $entryId=$db->insertId;                
            //********************************************************************************
            //enter value to sales daybook header
            $sql = "INSERT INTO `fin_customer_salesinvoice_header` (`intInvoiceNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strReferenceNo`,`intCustomerId`,`dtmDate`,`strInvoiceType`,`strAddress`,`intCurrencyId`,`dblRate`,`strRemark`,`strPoNo`,`intPaymentsTermsId`,`intMarketerId`,`dtmShipDate`,`intShipmentId`,`intCreator`,dtmCreateDate,`intDeleteStatus`,`intAutomated`,entryId)
                            VALUES ('$invoiceNumber','$accountPeriod','$locationId','$companyId','$invoiceReference',$customerId,'$currentDate',$cusInvoiceType,'$cusAddress',$orderCurrency,'$orderCurrencyRate','$remarks','$poNo',$paymentsTerms,$marketer,$delDate,$shipmentMethod,'$userId',now(),'0','1',$entryId)";
            $firstResult = $db->RunQuery2($sql);  
            
            $i=0;
            //add details for detals table
            foreach($dispatchNos as $dispatchNoFUll){
                $dispatchNos1 	= explode("-", $dispatchNoFUll);
                $dispatchNo 	= $dispatchNos1[0];
                $dispatchYear 	= $dispatchNos1[1];
                $saleOrderId 	= $dispatchNos1[2];             
                
                
                $sqlDetails="SELECT
                                DD.intSalesOrderId,
                                Sum(DD.dblGoodQty) AS Qty,
                                OD.strSalesOrderNo,
                                OD.strLineNo,
                                OD.strStyleNo,
                                OD.strGraphicNo,
                                OD.dblPrice
                            FROM
                                ware_fabricdispatchheader AS DH
                                Inner Join ware_fabricdispatchdetails AS DD ON DH.intBulkDispatchNo = DD.intBulkDispatchNo AND DH.intBulkDispatchNoYear = DD.intBulkDispatchNoYear
                                Inner Join mst_locations AS LOC ON DH.intCompanyId = LOC.intId
                                Inner Join trn_orderdetails AS OD ON DH.intOrderNo = OD.intOrderNo AND DH.intOrderYear = OD.intOrderYear AND DD.intSalesOrderId = OD.intSalesOrderId
                            WHERE
                                DH.intBulkDispatchNo =  '$dispatchNo' AND
                                DH.intBulkDispatchNoYear =  '$dispatchYear' AND
                                DD.intSalesOrderId =  '$saleOrderId' AND
                                LOC.intCompanyId =  '$companyId'
                            GROUP BY
                                DD.intSalesOrderId";
                $resultDetails= $db->RunQuery2($sqlDetails);            
                $rowD    = mysqli_fetch_array($resultDetails);
                
                $item 	= 1;				
                $style	= trim(($rowD['strStyleNo']==''?'NULL':"'".$rowD['strStyleNo']."'"));
                $graphic= trim(($rowD['strGraphicNo']==''?'NULL':"'".$rowD['strGraphicNo']."'"));
                $order	= trim(($rowD['strSalesOrderNo']==''?'NULL':"'".$rowD['strSalesOrderNo']."'"));
                $line	= trim(($rowD['strLineNo']==''?'NULL':"'".$rowD['strLineNo']."'"));
                $locate	= trim(($rowD['locate']==''?'NULL':"'".$rowD['locate']."'"));
                $itemDesc	= trim(($rowD['itemDesc']==''?'NULL':"'".$rowD['itemDesc']."'"));
				
                $uom	= null($rowD['uomId']);
                $quantity 		= val($rowD['Qty']);
                $unitPrice		= val($rowD['dblPrice']);
                //$discount		= val($detail['discount']);
                //$taxAmount		= val($detail['taxAmount']);
                //$taxGroup 		= null($detail['taxGroupId']);
                //$dimension		= null($detail['dimensionId']);
                $itemAmount= $quantity * $unitPrice;
                //$taxDetails 	= $detail['trnTaxVal'];
                
                $sql = "INSERT INTO `fin_customer_salesinvoice_details` (`intInvoiceNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strReferenceNo`,`intItem`,`intItemSerial`,`strStyleNo`,`strGraphicNo`,`strOrderNo`,`strLineItem`,`strCusLocation`,`strItemDesc`,`intUom`,`dblQty`,`dblUnitPrice`,`intCreator`,dtmCreateDate) 
				VALUES ('$invoiceNumber','$accountPeriod','$locationId','$companyId','$invoiceReference','$item','$i', $style, $graphic, $order, $line, $locate, $itemDesc, $uom,'$quantity','$unitPrice','$userId',now())";				
                $finalResult = $db->RunQuery2($sql);
                ++$i;
                
                // update dispatch table as Pay
                $sql="UPDATE ware_fabricdispatchdetails SET invoiced=1, strReferenceNo	= '$invoiceReference'  WHERE
                                ware_fabricdispatchdetails.intBulkDispatchNo =  '$dispatchNo' AND
                                ware_fabricdispatchdetails.intBulkDispatchNoYear =  '$dispatchYear' AND
                                ware_fabricdispatchdetails.intSalesOrderId =  '$saleOrderId'";
                $finalResult = $db->RunQuery2($sql);
            }
            
        }
        //========================================================================
	if ($finalResult) 
	{
		$response['type'] = 'pass';
		$response['msg'] = 'Saved successfully.';
		$response['invoiceNo'] = $invoiceReference;
                $db->commit();
	} 
	else 
	{
		$response['type'] = 'fail';
		$response['msg'] = $db->errormsg;
		$response['q'] = $sql;
                $db->rollback();//roalback                
	}
    }
    echo json_encode($response);
}
catch (Exception $e)
{
    $db->rollback();//roalback
    $response['type'] = 'fail';
    $response['msg'] = $e->getMessage();
    $response['q'] = $sql;
    echo json_encode($response);
}




function getAllGrnNos($grnYears,$grnNumbers)
{    
    $allGrns="";
    for($i=0;$i < count($grnYears);++$i){
        $allGrns.=$grnNumbers[$i]."/".$grnYears[$i].", ";
    }
    return $allGrns;
}
//--------------------------------------------------------------------------------------------
	function getNextInvoiceNo($companyId,$locationId)
	{
		global $db;
		$sql = "SELECT
				intSalesInvoiceNo
				FROM sys_finance_no
				WHERE
				intCompanyId = '$companyId' AND intLocationId = '$locationId'
				";	
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$nextInvoiceNo = $row['intSalesInvoiceNo'];
		
		$sql = "UPDATE `sys_finance_no` SET intSalesInvoiceNo=intSalesInvoiceNo+1 WHERE (intCompanyId = '$companyId' AND intLocationId = '$locationId')";
		$db->RunQuery($sql);
		return $nextInvoiceNo;
	}
//--------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------
	function getLatestAccPeriod($companyId)
	{
		global $db;
		$sql = "SELECT
				MAX(mst_financeaccountingperiod.intId) AS accId,
				mst_financeaccountingperiod.dtmStartingDate,
				mst_financeaccountingperiod.dtmClosingDate,
				mst_financeaccountingperiod.intStatus,
				mst_financeaccountingperiod_companies.intCompanyId,
				mst_financeaccountingperiod_companies.intPeriodId
				FROM
				mst_financeaccountingperiod
				Inner Join mst_financeaccountingperiod_companies ON mst_financeaccountingperiod_companies.intPeriodId = mst_financeaccountingperiod.intId
				WHERE
				mst_financeaccountingperiod_companies.intCompanyId =  '$companyId' AND mst_financeaccountingperiod.intStatus = '1'
				ORDER BY
				mst_financeaccountingperiod.intId DESC
				";	
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$latestAccPeriodId = $row['accId'];	
		return $latestAccPeriodId;
	}
//--------------------------------------------------------------------------------------------
//============================================================================================
	function encodeInvoiceNo($invoiceNumber,$accountPeriod,$companyId,$locationId)
	{
		global $db;
		$sql = "SELECT
				mst_financeaccountingperiod.intId,
				mst_financeaccountingperiod.dtmStartingDate,
				mst_financeaccountingperiod.dtmClosingDate,
				mst_financeaccountingperiod.intStatus
				FROM
				mst_financeaccountingperiod
				WHERE
				mst_financeaccountingperiod.intId =  '$accountPeriod'
				";	
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$startDate = substr($row['dtmStartingDate'],0,4);
		$closeDate = substr($row['dtmClosingDate'],0,4);
		$sql = "SELECT
				mst_companies.strCode AS company,
				mst_companies.intId,
				mst_locations.intCompanyId,
				mst_locations.strCode AS location,
				mst_locations.intId
				FROM
				mst_companies
				Inner Join mst_locations ON mst_locations.intCompanyId = mst_companies.intId
				WHERE
				mst_locations.intId =  '$locationId' AND
				mst_companies.intId =  '$companyId'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$companyCode = $row['company'];
		$locationCode = $row['location'];
		$invoiceFormat = $companyCode."/".$locationCode."/".$startDate."-".$closeDate."/".$invoiceNumber;
		//$invoiceFormat = $companyCode."/".$startDate."-".$closeDate."/".$invoiceNumber;
		return $invoiceFormat;
	}
//============================================================================================
?>

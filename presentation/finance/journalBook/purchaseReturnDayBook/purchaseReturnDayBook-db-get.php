<?php
session_start();
$backwardseperator = "../../../../";
$mainPath 	= $_SESSION['mainPath'];
$userId 	= $_SESSION['userId'];
$requestType 	= $_REQUEST['requestType'];
$companyId = $_SESSION['headCompanyId'];
$location = $_SESSION['CompanyID'];
include "{$backwardseperator}dataAccess/Connector.php";

if($requestType=='loadGrns'){
    $poNumber= trim($_REQUEST['poNumber']);
    $poYear=trim($_REQUEST['poYear']); 
    //$fullPoNo=$poYear.'-'.$poNumber;
    $fullPoNo=$poNumber.'/'.$poYear;
    $sql="SELECT
                ware_returntosupplierheader.intReturnNo,
                ware_returntosupplierheader.intReturnYear,
                ware_returntosupplierheader.intGrnNo,
                ware_returntosupplierheader.intGrnYear
            FROM
                ware_returntosupplierheader
                Inner Join ware_grnheader ON ware_returntosupplierheader.intGrnNo = ware_grnheader.intGrnNo AND ware_returntosupplierheader.intGrnYear = ware_grnheader.intGrnYear
                Inner Join mst_locations ON ware_returntosupplierheader.intCompanyId = mst_locations.intId
                Inner Join trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
            WHERE
                ware_returntosupplierheader.intStatus =  '1' AND
                ware_returntosupplierheader.intPaystatus =  '0' AND
                mst_locations.intCompanyId =  '$companyId' AND
                trn_poheader.intPOYear =  '$poYear' AND
                trn_poheader.intPONo =  '$poNumber'  AND
                ware_grnheader.intPayStatus =  '1'
            ORDER BY
                ware_returntosupplierheader.intReturnNo DESC,
                ware_returntosupplierheader.intReturnYear DESC";
    
    $result = $db->RunQuery($sql);
    $grnList;
    while($row=mysqli_fetch_array($result)){       
         $srnNumber=$row['intReturnNo'];
         $srnYear=$row['intReturnYear'];
         $gNumber=$row['intGrnNo'];
         $gYear=$row['intGrnYear'];
         
    $grnList.= "<tr class=normalfnt bgcolor=#00FF66>
        <td><input type=\"checkbox\" name=\"chkGrns\" id=\"chkGrns\" value=\"$srnYear - $srnNumber\" /></td>
        <td><a target=\"_blank\"  href=\"{$backwardseperator}presentation/warehouse/returnToSupplier/listing/rptReturnToSupplier.php?retSupNo=$srnNumber&year=$srnYear\" >$srnNumber/$srnYear</a></td>    
        <td><a target=\"_blank\"  href=\"{$backwardseperator}presentation/warehouse/grn/listing/rptGrn.php?grnNo=$gNumber&year=$gYear\" >$gNumber/$gYear</a></td>        
        </tr>";
    }    
    $response['grnList']=$grnList;
    
    $sqlInv="SELECT
                fin_supplier_purchaseinvoice_header.strReferenceNo
            FROM
                fin_supplier_purchaseinvoice_header
            WHERE
                fin_supplier_purchaseinvoice_header.strPoNo =  '$fullPoNo' AND
                fin_supplier_purchaseinvoice_header.intCompanyId =  '$companyId' AND
                fin_supplier_purchaseinvoice_header.intAutomated =  '1'";
    $resultInv = $db->RunQuery($sqlInv);
    $salesInvoiceList ='<option value=""></option>';
    while($rowInv=mysqli_fetch_array($resultInv)){
        $salesInvoiceList .="<option value=\"".$rowInv['strReferenceNo']."\">".$rowInv['strReferenceNo']."</option>";
    } 
    $response['salesInvoiceList'] = $salesInvoiceList;
    
    //poLink
    $viewPo="<a target=\"_blank\" href=\"{$backwardseperator}presentation/procurement/purchaseOrder/listing/rptPurchaseOrder.php?poNo=$poNumber&year=$poYear\" >$poNumber/$poYear</a>";
    $response['poView']=$viewPo;
    
    echo json_encode($response);
}
?>

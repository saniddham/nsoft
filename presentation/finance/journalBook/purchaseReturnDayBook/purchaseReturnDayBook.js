//permision for add 
if(intAddx)
{
    $('#frmPurchaseReturnDayBook #butNew').show();
    $('#frmPurchaseReturnDayBook #butSave').show();
}
//permision for edit 
if(intEditx)
{
    $('#frmPurchaseReturnDayBook #butSave').show();
    $('#frmPurchaseReturnDayBook #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
}
//permision for delete
if(intDeletex)
{
    $('#frmPurchaseReturnDayBook #butDelete').show();
    $('#frmPurchaseReturnDayBook #cboSearch').removeAttr('disabled');
}
//permision for view
if(intViewx)
{
    $('#frmPurchaseReturnDayBook #cboSearch').removeAttr('disabled');
}
function alertx()
{
    $('#frmPurchaseReturnDayBook #butSave').validationEngine('hide') ;
}


$(document).ready(function() {
    $("#frmPurchaseReturnDayBook").validationEngine();
    
    //permision for add 
    if(intAddx)
    {
        $('#frmPurchaseReturnDayBook #butSave').show();
    }
    //get Grn from PO-------------------------------
    $('#frmPurchaseReturnDayBook #cmbPo').change(function(){
        if($('#frmPurchaseReturnDayBook #cmbPo').val()!=""){
            var po=($('#frmPurchaseReturnDayBook #cmbPo').val()).split("-");
            var poNumber=po[1];
            var poYear=po[0];
            
            var url ='purchaseReturnDayBook-db-get.php?requestType=loadGrns&poNumber='+poNumber+"&poYear="+poYear;            
            var obj=$.ajax({
                url:url,
                dataType:'json',
                success:function(json){
                    $('#frmPurchaseReturnDayBook #allgrns').html(json.grnList)
                    $('#cmbInvoceNo').html(json.salesInvoiceList);
                    $('#frmPurchaseReturnDayBook #poView').html(json.poView);
                },
                async:false
            });
        }
        
    });
    //generate the invoice with Grns(//save button click event)
    $('#frmPurchaseReturnDayBook #butSave').click(function(){
        if ($('#frmPurchaseReturnDayBook').validationEngine('validate')) { 
            var grns=document.getElementsByName("chkGrns");
            var grnYears=new Array();
            var grnNumbers=new Array();
            var x=0;
            for(var i=0; i< grns.length;++i){
                if(grns[i].checked){
                    var grn=(grns[i].value).split("-");
                    grnYears[x]=grn[0];
                    grnNumbers[x]=grn[1];
                    ++x;               
                }            
            }
            var invoiceNo=$('#frmPurchaseReturnDayBook #cmbInvoceNo').val();
            var poNo=$('#frmPurchaseReturnDayBook #cmbPo').val();            
            var url = "purchaseReturnDayBook-db-set.php";
            var obj = $.ajax({
                url:url,
                dataType: "json",  
                data:$("#frmPurchaseReturnDayBook").serialize()+'&requestType=add&grnYears='+grnYears+'&grnNumbers='+grnNumbers+'&invoiceNo='+invoiceNo+'&poNo='+poNo,
                async:false,
			
                success:function(json){
                    $('#frmPurchaseReturnDayBook #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
                    if(json.type=='pass')
                    {
                        $('#frmPurchaseReturnDayBook').get(0).reset();						
                        var t=setTimeout("alertx()",1000);
                        $('#frmPurchaseReturnDayBook #cmbPo').val('');
                        $('#frmPurchaseReturnDayBook #allgrns').html('');
                        $('#frmPurchaseDayBook #poView').html('');
                        return;
                    }
                    var t=setTimeout("alertx()",3000);
                },
                error:function(xhr,status){					
                    $('#frmPurchaseReturnDayBook #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
                    var t=setTimeout("alertx()",3000);
                }		
            }); 
        
        }
    });
});

function alertx()
{
    $('#frmPurchaseReturnDayBook #butSave').validationEngine('hide') ;
}
function alertDelete()
{
    $('#frmPurchaseReturnDayBook #butSave').validationEngine('hide') ;
}

<?php

session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$userId = $_SESSION['userId'];
$companyId = $_SESSION['headCompanyId'];
$locationId = $_SESSION['CompanyID'];
$thisFilePath = $_SERVER['PHP_SELF'];
include "{$backwardseperator}dataAccess/Connector.php";

include "../purchaseDayBook/AccTaxCalculationFunctions.php";
$response = array('type' => '', 'msg' => '');
try {
    $requestType = $_REQUEST['requestType'];
    if ($requestType == 'add') {
        $srnYears = explode(",", $_REQUEST['grnYears']);
        $srnNumbers = explode(",", $_REQUEST['grnNumbers']);
        $invoiceNo = $_REQUEST['invoiceNo'];
        $poNoFull = $_REQUEST['poNo'];
        $poNos = explode("-", $poNoFull);
        $poYear = $poNos[0];
        $poNo = $poNos[1];
        $poFormat = $poNo . "/" . $poYear;

        $invoiceNumber = getNextDebitNoteNo($companyId);
        $accountPeriod = getLatestAccPeriod($companyId);

        $db->begin();
        //get Po details        
        $resultPOH = $db->RunQuery2("SELECT trn_poheader.intSupplier,trn_poheader.intCurrency,trn_poheader.dtmPODate FROM trn_poheader WHERE trn_poheader.intPONo =  '$poNo' AND trn_poheader.intPOYear =  '$poYear'");
        $rowPoH = mysqli_fetch_array($resultPOH);
        $supplierId = $rowPoH['intSupplier'];
        $poCurrency = $rowPoH['intCurrency'];
        $poDate = $rowPoH['dtmPODate'];

        //get Currency details
        $resultCurr = $db->RunQuery2("SELECT mst_financeexchangerate.dblBuying FROM mst_financeexchangerate WHERE mst_financeexchangerate.intCurrencyId =  '$poCurrency' AND mst_financeexchangerate.dtmDate =  '$poDate' AND mst_financeexchangerate.intCompanyId =  '$companyId'");
        $rowCurr = mysqli_fetch_array($resultCurr);
        $poCurrencyRate = $rowCurr['dblBuying'];

        //get Supplier Details         
        $resultSup = $db->RunQuery2("SELECT mst_financesupplieractivate.intChartOfAccountId, mst_supplier.strInvoiceType, mst_supplier.strAddress, mst_supplier.intPaymentsTermsId FROM mst_financesupplieractivate Inner Join mst_supplier ON mst_supplier.intId = mst_financesupplieractivate.intSupplierId WHERE mst_financesupplieractivate.intCompanyId = '$companyId' AND mst_supplier.intId ='$supplierId' ");
        $rowSup = mysqli_fetch_array($resultSup);
        $paymentsTerms = ($rowSup['intPaymentsTermsId'] == '' ? 'null' : $rowSup['intPaymentsTermsId']);
        $supAddress = htmlspecialchars($rowSup['strAddress'], ENT_QUOTES);

        $currentDate = date('Y-m-d');
        $totalValue = 0.00;
        $remarks = "Automated Supplier Return, " . $invoiceNumber;
        $alGrns = getAllSrnNos($srnYears, $srnNumbers);
        $trnsDetails = "po " . $poFormat . "/" . $alGrns;
        if (count($srnYears) != 0) {

            //Add data to transaction header*******************************************
            $sql = "INSERT INTO fin_transactions (entryDate, strProgramType, documentNo, currencyId, currencyRate, transDetails, payMethodId, paymentNumber, accPeriod, userId, companyId, createdOn) VALUES
                ('$currentDate','Debit Note','$invoiceNumber',$poCurrency,$poCurrencyRate,'$remarks',null,null,$accountPeriod,$userId,$companyId,now())";

            $db->RunQuery2($sql);
            $entryId = $db->insertId;
            // add to Debit note heder details
            $sql = "INSERT INTO `fin_supplier_debitnote_header` (`strDebitNoteNo`,`strInvoiceNo`, `intSupplier`,`dtDate`,`dblRate`,
			`intCurrency`,`strRemarks`,`strPoNo`,`strGrnNo`,`intCreator`,`dtmCreateDate`,`intAutomated`,entryId)
	 VALUES ('$invoiceNumber','$invoiceNo','$supplierId','$currentDate','$poCurrencyRate',
			'$poCurrency','$remarks','$poFormat','Automated','$userId', now(),1,$entryId)";
            $firstResult = $db->RunQuery2($sql);
            $AllGrns = "";
            if ($firstResult) {
                //get selected SRNs
                for ($i = 0; $i < count($srnYears); ++$i) {
                    $sql = "SELECT
                            ware_returntosupplierdetails.intItemId,
                            ware_returntosupplierdetails.dblQty,
                            ware_grndetails.dblInvoiceRate,
                            ware_returntosupplierheader.intGrnNo,
                            ware_returntosupplierheader.intGrnYear
                        FROM
                            ware_returntosupplierdetails
                            Inner Join ware_returntosupplierheader ON ware_returntosupplierdetails.intReturnNo = ware_returntosupplierheader.intReturnNo AND ware_returntosupplierdetails.intReturnYear = ware_returntosupplierheader.intReturnYear
                            Inner Join ware_grndetails ON ware_returntosupplierheader.intGrnNo = ware_grndetails.intGrnNo AND ware_returntosupplierheader.intGrnYear = ware_grndetails.intGrnYear AND ware_returntosupplierdetails.intItemId = ware_grndetails.intItemId
                            Inner Join mst_locations ON ware_returntosupplierheader.intCompanyId = mst_locations.intId
                        WHERE
                            ware_returntosupplierdetails.intReturnNo =  '$srnNumbers[$i]' AND
                            ware_returntosupplierdetails.intReturnYear =  '$srnYears[$i]' AND
                            mst_locations.intCompanyId =  '$companyId'";
                    $resultSrnDetails = $db->RunQuery2($sql);
                    while ($rowSrnDetails = mysqli_fetch_array($resultSrnDetails)) {
                        $quantity = $rowSrnDetails['dblQty'];
                        $unitPrice = $rowSrnDetails['dblInvoiceRate'];
                        $item = $rowSrnDetails['intItemId'];
                        $AllGrns.=$rowSrnDetails['intGrnNo'] . "/" . $rowSrnDetails['intGrnYear'] . ", ";
                        //get details from PO Details table
                        $sqlPoDetails = "SELECT
                                            trn_podetails.dblDiscount,
                                            trn_podetails.intTaxCode,                                            
                                            mst_item.intSubCategory,
                                            mst_item.strCode,
                                            mst_item.strName,
                                            mst_item.intUOM
                                        FROM
                                            trn_podetails                                            
                                            Inner Join mst_item ON trn_podetails.intItem = mst_item.intId
                                        WHERE
                                            trn_podetails.intPONo =  '$poNo' AND
                                            trn_podetails.intPOYear =  '$poYear' AND
                                            trn_podetails.intItem =  '$item'";
                        $resultPoDetails = $db->RunQuery2($sqlPoDetails);
                        $rowPoDetails = mysqli_fetch_array($resultPoDetails);
                        $itemDesc = $rowPoDetails['strName'];
                        $uom = $rowPoDetails['intUOM'];
                        $itemSubcategory = $rowPoDetails['intSubCategory'];
                        $taxGroup = $rowPoDetails['intTaxCode'];

                        $discount = $rowPoDetails['dblDiscount']; // calculate discount
                        $discountValue = (($quantity * $unitPrice)) * (($rowPoDetails['dblDiscount'] / 100)); // calculate discount
                        $itemAmount = ($quantity * $unitPrice) - $discountValue;
                        if ($taxGroup != 0) {//cal culate Taxes (From a function)
                            $detals = calculateTax($itemAmount, $taxGroup);
                            $taxAmount = $detals[0];
                            $taxDetails = $detals[1];
                        } else {
                            $totalValue+=($itemAmount + $taxAmount);
                            $dimension = "null";
                        }

                        $totalValue+=($itemAmount + $taxAmount);
                        $dimension = "null";

                        //>>>>>>>>>>>>>>>>>>>>>>transaction table process - item>>>>>>>>>>>>>>>>>>>>>>>>>
                        $sql = "SELECT
                                    mst_financeitemactivate.otherAccountId,
                                    mst_financeitemactivate.stockAccountId,
                                    mst_financeitemactivate.itemClassId
                                FROM
                                    mst_financeitemactivate
                                WHERE
                                    mst_financeitemactivate.subCategoryId =  '$itemSubcategory' AND
                                    mst_financeitemactivate.intCompanyId =  '$companyId'";
                        $result = $db->RunQuery2($sql);
                        $row = mysqli_fetch_array($result);
                        $itemManinClass = $row['itemClassId'];
                        if ($itemManinClass == 1) {//Inventy item class
                            $itemAccount = $row['stockAccountId'];
                        } else {//Other classes
                            $itemAccount = $row['otherAccountId'];
                        }


                        $sql = "INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                                        ($entryId,'C',$itemAccount,$itemAmount,'$trnsDetails',$dimension)";
                        $trnResult = $db->RunQuery2($sql);
                        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        //insert into debit note details
                        $sql = "INSERT INTO `fin_supplier_debitnote_details` (`strDebitNoteNo`,`intAccountId`,`intItem`,`intUOM`,`dblQty`,`dblRate`,`dblDiscount`,`intTaxGroup`,`intDimension`,dblTax) 
					VALUES ('$invoiceNumber','$itemAccount','$item','$uom','$quantity','$unitPrice','$discount','$taxGroup',$dimension,'$taxAmount')";

                        $finalResult = $db->RunQuery2($sql);

                        //>>>>>>>>>>>>>>>>>>>>>>>>>>>transaction table process - tax>>>>>>>>>>>>>>>>>>>>>>>>>
                        if (count($taxDetails) != 0 && $trnResult) {
                            foreach ($taxDetails as $taxDetail) {
                                $taxId = $taxDetail['taxId'];
                                $taxAmount = $taxDetail['taxValue'];

                                $sql = "SELECT
					mst_financetaxactivate.intChartOfAccountId
					FROM mst_financetaxactivate
					WHERE
					mst_financetaxactivate.intTaxId = '$taxId' AND
					mst_financetaxactivate.intCompanyId = '$companyId'";
                                $result = $db->RunQuery2($sql);
                                $row = mysqli_fetch_array($result);
                                $taxAccount = $row['intChartOfAccountId'];

                                $sql = "INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                                        ($entryId,'C',$taxAccount,$taxAmount,'$trnsDetails',null)";
                                $db->RunQuery2($sql);
                            }
                        }
                        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>                      
                    }//SRNItems
                    //update SRN return to supplier table as Pay
                    $sql = "Update ware_returntosupplierheader as g, mst_locations as l
                            SET g.intPaystatus=1
                            where 
                                g.intReturnNo =  '$srnNumbers[$i]' AND
                                g.intReturnYear =  '$srnYears[$i]' AND
                                g.intCompanyId=l.intId And
                                l.intCompanyId =  '$companyId'";
                    $db->RunQuery2($sql);
                }//SRNs
                //update debit note table with grn numbers
                $sql = "UPDATE `fin_supplier_debitnote_header` SET strGrnNo='$AllGrns' WHERE strDebitNoteNo='$invoiceNumber'";
                $db->RunQuery2($sql);

                //>>>>>>>>>>>>>>>>>>>>>>transaction table process - supplier>>>>>>>>>>>>>>>>>>>>>>>>>
                $sql = "SELECT
					mst_financesupplieractivate.intChartOfAccountId
					FROM mst_financesupplieractivate
					WHERE
					mst_financesupplieractivate.intSupplierId =  '$supplierId' AND
					mst_financesupplieractivate.intCompanyId =  '$companyId'";
                $result = $db->RunQuery2($sql);
                $row = mysqli_fetch_array($result);
                $suptAccount = $row['intChartOfAccountId'];

                $sql = "INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId,personType, personId) VALUES 
                                        ($entryId,'D',$suptAccount,$totalValue,'$trnsDetails',null,'sup',$supplierId,)";
                $trnResult = $db->RunQuery2($sql);
            }
        }
        //========================================================================
        if ($trnResult) {
            $db->commit();
            $response['type'] = 'pass';
            $response['msg'] = 'Saved successfully.';
        } else {
            $db->rollback(); //roalback
            $response['type'] = 'fail';
            $response['msg'] = $db->errormsg;
            $response['q'] = $sql;
        }
    }
    echo json_encode($response);
} catch (Exception $e) {
    $db->rollback(); //roalback
    //echo $e->getMessage();
    $response['type'] = 'fail';
    $response['msg'] = $e->getMessage();
    $response['q'] = $sql;
    echo json_encode($response);
}

//**************
function getAllSrnNos($srnYears, $srnNumbers) {
    $allGrns = "";
    for ($i = 0; $i < count($srnYears); ++$i) {
        $allGrns.=$srnNumbers[$i] . "/" . $srnYears[$i] . ", ";
    }
    return $allGrns;
}

//--------------------------------------------------------------------------------------------
function getLatestAccPeriod($companyId) {
    global $db;
    $sql = "SELECT
				MAX(mst_financeaccountingperiod.intId) AS accId,
				mst_financeaccountingperiod.dtmStartingDate,
				mst_financeaccountingperiod.dtmClosingDate,
				mst_financeaccountingperiod.intStatus,
				mst_financeaccountingperiod_companies.intCompanyId,
				mst_financeaccountingperiod_companies.intPeriodId
				FROM
				mst_financeaccountingperiod
				Inner Join mst_financeaccountingperiod_companies ON mst_financeaccountingperiod_companies.intPeriodId = mst_financeaccountingperiod.intId
				WHERE
				mst_financeaccountingperiod_companies.intCompanyId =  '$companyId' AND mst_financeaccountingperiod.intStatus = '1'
				ORDER BY
				mst_financeaccountingperiod.intId DESC
				";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $latestAccPeriodId = $row['accId'];
    return $latestAccPeriodId;
}

//--------------------------------------------------------------------------------------------
function getNextDebitNoteNo($company) {
    global $db;
    global $locationId;

    $sql = "SELECT
			Max(mst_financeaccountingperiod.dtmCreateDate),
			mst_financeaccountingperiod.dtmStartingDate, 
			substring(mst_financeaccountingperiod.dtmStartingDate,1,4) as fromY,
			substring(mst_financeaccountingperiod.dtmClosingDate,1,4) as toY 
			FROM mst_financeaccountingperiod ";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $accPeriod = $row['fromY'] . "-" . $row['toY'];

    $sql = "SELECT 
			sys_finance_no.intSupplierDebitNote  
			FROM 
			sys_finance_no 
			WHERE 
			sys_finance_no.intLocationId =  '$locationId' and sys_finance_no.intCompanyId = '$company'";


    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $nextNo = $row['intSupplierDebitNote'];

    //------------------
    $sql = "SELECT
			mst_companies.strCode AS company,
			mst_companies.intId,
			mst_locations.intCompanyId,
			mst_locations.strCode AS location,
			mst_locations.intId
			FROM
			mst_companies
			Inner Join mst_locations ON mst_locations.intCompanyId = mst_companies.intId
			WHERE
			mst_locations.intId =  '$locationId' AND
			mst_companies.intId =  '$company'
			";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $companyCode = $row['company'];
    $locationCode = $row['location'];
    //---------------------

    $sql = "UPDATE `sys_finance_no` SET intSupplierDebitNote=intSupplierDebitNote+1 WHERE (`intLocationId`='$locationId' AND  sys_finance_no.intCompanyId = '$company')  ";
    $db->RunQuery($sql);

    return $companyCode . "/" . $locationCode . "/" . $accPeriod . "/" . (int) $nextNo;
}

//--------------------------------------------------------------------------------------------
?>

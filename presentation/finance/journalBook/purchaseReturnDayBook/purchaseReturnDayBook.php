<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$companyId = $_SESSION['headCompanyId'];
$location = $_SESSION['CompanyID'];
$thisFilePath =  $_SERVER['PHP_SELF'];
include  "{$backwardseperator}dataAccess/permisionCheck.inc";
// ======================Check Exchange Rate Updates========================

$currentDate = date("Y-m-d");
//
$sql = "SELECT COUNT(*) AS 'no'
		FROM
		mst_financeexchangerate
		WHERE
		mst_financeexchangerate.dtmDate =  '$currentDate'
		";
$result = $db->RunQuery($sql);
$row= mysqli_fetch_array($result);
if($row['no']==0)
	{
		$str =  "Please Update Exchange Rates Before ".$status." Received Payments .";
		$str .= $row['NameList'];
		$maskClass="maskShow";
	}
	else
	{
		$maskClass="maskHide";
	}
// =========================================================================
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Purchase Return Day Book - (SRN)</title>
        
        <link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/tblstyle.css" rel="stylesheet" type="text/css" />

        <script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
        <script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
        <script src="purchaseReturnDayBook.js" type="text/javascript"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

        <link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
        <script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

        <link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css">
        <link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css">   

    </head>

    <body>
        <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
                <td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator . 'Header.php'; ?></td>
            </tr> 
        </table>
        <div id="divMask" class="<?php echo $maskClass?> mask"> <?php echo $str; ?></div>

        <script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
        <script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
        <script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>
        <form id="frmPurchaseReturnDayBook" name="frmPurchaseReturnDayBook" method="post" action="purchaseDayBook-db-set.php" autocomplete="off">
            <div align="center">
                <div class="trans_layoutL">
                    <div class="trans_text">Purchase Return Day Book</div>
                    <table width="100%">
                        <tr>
                            <td colspan="2">
                                <table width="100%" class="tableBorder_allRound">
                                    <tr>                                        
                                        <td bgcolor="#FFFFFF" class="normalfntRight">Purchase Order</td>
                                         <td bgcolor="#FFFFFF"><span class="normalfntMid">
                                                <select name="cmbPo" id="cmbPo" style="width:115px" class="validate[required]">
                                                    <option value=""></option>
                                                    <?php
                                                    $sql2 = "SELECT
                                                                trn_poheader.intPONo,
                                                                trn_poheader.intPOYear
                                                            FROM
                                                                trn_poheader
                                                                Inner Join mst_locations ON trn_poheader.intCompany = mst_locations.intId
                                                            WHERE
                                                                mst_locations.intCompanyId =  '$companyId'
                                                            ORDER BY
                                                                trn_poheader.intPOYear DESC,
                                                                trn_poheader.intPONo DESC";
                                                    $result2 = $db->RunQuery($sql2);
                                                    while ($row2 = mysqli_fetch_array($result2)) {
                                                        echo "<option value=\"" . $row2['intPOYear'] . "-" . $row2['intPONo'] . "\">" . $row2['intPOYear'] . "-" . $row2['intPONo'] . "</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </span></td>                                                                              
                                    </tr>
                                    <tr>
                                        <td bgcolor="#FFFFFF" class="normalfntRight">View Po :</td>
                                        <td bgcolor="#FFFFFF" class="normalfnt"><div id="poView"></div></td>                                        
                                    </tr>
                                    <tr>
                                        <td bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
                                        <td bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>                                        
                                    </tr>                                     
                                    <tr>  
                                        <td bgcolor="#FFFFFF" class="normalfntRight">Return To Supplier Note</td> 
                                        <td valign="top" bgcolor="#FFFFFF">
                                            <div id="divGrid">
                                                <table width="100%">  
                                                    <tr>
                                                        <td class="normalfntMid"></td>
                                                        <td class="normalfntMid">Return</td>
                                                        <td class="normalfntMid">GRN</td>
                                                    </tr>
                                                    <tbody id="allgrns" class="normalfnt" bgcolor="#FFFFFF"><!-- load Grns -->
                                                    </tbody>
                                                </table>
                                            </div>              
                                        </td>                                        
                                    </tr>
                                    <tr>
                                        <td class="normalfntRight"><span class="name">Invoice Number <span class="compulsoryRed">*</span></span></td>
                                        <td bgcolor="#FFFFFF" class="normalfnt"><span class="normalfnt">
                                            <span class="normalfnt">
                                                <select name="cmbInvoceNo" id="cmbInvoceNo" style="width:115px" class="validate[required]">                                             
                                                </select>
                                            </span>
                                        </td>                                        
                                    </tr>                            
                                    
                                </table>
                            </td>
                        </tr>                       
                        
                        <tr>
                            <td colspan="2">
                                <table width="100%">
                                    <tr>
                                        <td width="100%" height="34" class="tableBorder_allRound"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                                                <tr>
                                                    <td width="100%" align="center" bgcolor=""><img style="display:" border="0" src="../../../../images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/><img src="../../../../images/Tclose.jpg" alt="Close" name="Close" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                                                </tr>
                                            </table></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </form>
    </body>
</html>
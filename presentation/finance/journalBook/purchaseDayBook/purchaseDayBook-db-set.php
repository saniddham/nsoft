<?php

session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$userId = $_SESSION['userId'];
$companyId = $_SESSION['headCompanyId'];
$locationId = $_SESSION['CompanyID'];
$thisFilePath = $_SERVER['PHP_SELF'];
include "{$backwardseperator}dataAccess/Connector.php";

include "AccTaxCalculationFunctions.php";

$response = array('type' => '', 'msg' => '');
try {
    $requestType = $_REQUEST['requestType'];
    if ($requestType == 'add') {
        $grnYears = explode(",",$_REQUEST['grnYears']);       
        $grnNumbers = explode(",",$_REQUEST['grnNumbers']);   
        $supInvoiceNo = $_REQUEST['invoiceNo'];
        $poNoFull=$_REQUEST['poNo'];
        $poNos = explode("/", $poNoFull);
        $poYear = $poNos[1];
        $poNo = $poNos[0];
        $formatPONo=$poNo."/".$poYear;

        $accountPeriod = getLatestAccPeriod($companyId); 
        $invoiceNumber = getNextInvoiceNo($companyId, $locationId);
        $invoiceReference	= trim(encodeInvoiceNo($invoiceNumber,$accountPeriod,$companyId,$locationId));
        //$invoiceReference 	= trim($invoice);
         
        $db->begin();
        //get Po details 
        $resultPOH = $db->RunQuery2("SELECT trn_poheader.intSupplier,trn_poheader.intCurrency,trn_poheader.dtmPODate,trn_poheader.intShipmentTerm FROM trn_poheader WHERE trn_poheader.intPONo =  '$poNo' AND trn_poheader.intPOYear =  '$poYear'");
        $rowPoH = mysqli_fetch_array($resultPOH);
        $supplierId = $rowPoH['intSupplier'];
        $poCurrency = $rowPoH['intCurrency'];
        $poDate = $rowPoH['dtmPODate'];
        $poType = $rowPoH['intShipmentTerm'];


        //get Currency details  
        $resultCurr = $db->RunQuery2("SELECT mst_financeexchangerate.dblBuying FROM mst_financeexchangerate WHERE mst_financeexchangerate.intCurrencyId =  '$poCurrency' AND mst_financeexchangerate.dtmDate =  '$poDate' AND mst_financeexchangerate.intCompanyId =  '$companyId'");
        $rowCurr = mysqli_fetch_array($resultCurr);
        $poCurrencyRate = $rowCurr['dblBuying'];

        //get Supplier Details           
        $resultSup = $db->RunQuery2("SELECT mst_financesupplieractivate.intChartOfAccountId, mst_supplier.strInvoiceType, mst_supplier.strAddress, mst_supplier.intPaymentsTermsId FROM mst_financesupplieractivate Inner Join mst_supplier ON mst_supplier.intId = mst_financesupplieractivate.intSupplierId WHERE mst_financesupplieractivate.intCompanyId = '$companyId' AND mst_supplier.intId ='$supplierId' ");        
        $rowSup = mysqli_fetch_array($resultSup);        
        $paymentsTerms = ($rowSup['intPaymentsTermsId']==''?'NULL':$rowSup['intPaymentsTermsId']);
        $supInvoiceType=trim(($rowSup['strInvoiceType']==''?'NULL':"'".$rowSup['strInvoiceType']."'"));
        $supAddress=htmlspecialchars($rowSup['strAddress'],ENT_QUOTES);
        
        $currentDate=date('Y-m-d');
        $totalValue=0.00;
        $remarks="Automated GRN";
        $alGrns=getAllGrnNos($grnYears,$grnNumbers);     
        $trnsDetails="po ".$formatPONo."/".$alGrns;
        if (count($grnYears) != 0) {
            
            //Add data to transaction header*******************************************
           $sql="INSERT INTO fin_transactions (entryDate, strProgramType, documentNo, currencyId, currencyRate, transDetails, payMethodId, paymentNumber, accPeriod, userId, companyId, createdOn) VALUES
                ('$currentDate','Purchase Invoice','$supInvoiceNo',$poCurrency,$poCurrencyRate,'$remarks',null,null,$accountPeriod,$userId,$companyId,now())";

            $db->RunQuery2($sql);
            $entryId=$db->insertId;
          //enter value to purchase daybook header
             $sql = "INSERT INTO `fin_supplier_purchaseinvoice_header` (`intInvoiceNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strReferenceNo`,`intSupplierId`,`dtmDate`,`strInvoiceType`,`strAddress`,`intCurrencyId`,`dblRate`,`strRemark`,`strPoNo`,`intPaymentsTermsId`,`strGrnNo`,`strMessage`,`intCreator`,dtmCreateDate,`intDeleteStatus`,`intAutomated`,strSupInvoice,entryId) 
                        VALUES ('$invoiceNumber','$accountPeriod','$locationId','$companyId','$invoiceReference',$supplierId,'$currentDate',$supInvoiceType,'$supAddress',$poCurrency,'$poCurrencyRate','Automated','$formatPONo',$paymentsTerms,'$alGrns','Automated Invoice','$userId',now(),'0','1','$supInvoiceNo',$entryId)";                

  $firstResult = $db->RunQuery2($sql);
            if($firstResult){
                //get selected grns
                for($i=0;$i < count($grnYears);++$i){
                    $sql="SELECT
                                        ware_grndetails.intItemId,
                                        ware_grndetails.dblGrnQty,
                                        ware_grndetails.dblInvoiceRate,
                                        ware_grndetails.dblFocQty
                                    FROM
                                        ware_grndetails
                                        Inner Join ware_grnheader ON ware_grndetails.intGrnNo = ware_grnheader.intGrnNo AND ware_grndetails.intGrnYear = ware_grnheader.intGrnYear
                                        Inner Join mst_locations ON ware_grnheader.intCompanyId = mst_locations.intId
                                    WHERE
                                        ware_grnheader.intGrnNo =  '$grnNumbers[$i]' AND
                                        ware_grnheader.intGrnYear =  '$grnYears[$i]' AND
                                        mst_locations.intCompanyId =  '$companyId'";
                    $resultGrnDetails = $db->RunQuery2($sql);
                    while($rowGrnDetails= mysqli_fetch_array($resultGrnDetails)){
                        $quantity =$rowGrnDetails['dblGrnQty'];
                        $focQty =$rowGrnDetails['dblFocQty'];
                        $unitPrice =$rowGrnDetails['dblInvoiceRate'];
                        $item =$rowGrnDetails['intItemId'];
                        //get details from PO Details table
                        $sqlPoDetails="SELECT
                                            trn_podetails.dblDiscount,
                                            trn_podetails.intTaxCode,                                            
                                            mst_item.intSubCategory,
                                            mst_item.strCode,
                                            mst_item.strName,
                                            mst_item.intUOM
                                        FROM
                                            trn_podetails                                            
                                            Inner Join mst_item ON trn_podetails.intItem = mst_item.intId
                                        WHERE
                                            trn_podetails.intPONo =  '$poNo' AND
                                            trn_podetails.intPOYear =  '$poYear' AND
                                            trn_podetails.intItem =  '$item'";
                        $resultPoDetails = $db->RunQuery2($sqlPoDetails);
                        $rowPoDetails= mysqli_fetch_array($resultPoDetails);
                        $itemDesc = $rowPoDetails['strName'];
                        $uom=$rowPoDetails['intUOM'];
                        $itemSubcategory=$rowPoDetails['intSubCategory'];
                        $taxGroup=$rowPoDetails['intTaxCode'];
                        
                        $discount=$rowPoDetails['dblDiscount'];// calculate discount
                        $discountValue=(($quantity * $unitPrice))*(($rowPoDetails['dblDiscount']/100));// calculate discount
                        $itemAmount=($quantity * $unitPrice) - $discountValue;
                        
                        if($taxGroup!=0){//cal culate Taxes (From a function)
                            $detals=calculateTax($itemAmount,$taxGroup); 
                            $taxAmount=$detals[0];
                            $taxDetails=$detals[1]; 
                        }
                        else{
                            $taxGroup="null";
                            $taxAmount=0;
                        }
                                                
                        $totalValue+=($itemAmount+$taxAmount);
                        $dimension="null";
                        
                        //insert to supplier invoice details
                        $sql = "INSERT INTO `fin_supplier_purchaseinvoice_details` (`intInvoiceNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strReferenceNo`,`intItem`,`strItemDesc`,`intUom`,`dblQty`,`dblUnitPrice`,`dblDiscount`,`dblTaxAmount`,`intTaxGroupId`,`intDimensionId`,`intCreator`,dtmCreateDate) 
				VALUES ('$invoiceNumber','$accountPeriod','$locationId','$companyId','$invoiceReference','$item','$itemDesc',$uom,'$quantity','$unitPrice','$discount','$taxAmount',$taxGroup,$dimension,'$userId',now())";
			
                        $finalResult = $db->RunQuery2($sql);
                        
                        //>>>>>>>>>>>>>>>>>>>>>>transaction table process - item>>>>>>>>>>>>>>>>>>>>>>>>>
			$sql = "SELECT
                                    mst_financeitemactivate.otherAccountId,
                                    mst_financeitemactivate.stockAccountId,
                                    mst_financeitemactivate.itemClassId,
                                    mst_financeitemactivate.localAccId,
                                    mst_financeitemactivate.impAccId
                                FROM
                                    mst_financeitemactivate
                                WHERE
                                    mst_financeitemactivate.subCategoryId =  '$itemSubcategory' AND
                                    mst_financeitemactivate.intCompanyId =  '$companyId'";
				 $result = $db->RunQuery2($sql);
				 $row = mysqli_fetch_array($result);
                                 $itemManinClass=$row['itemClassId'];
                                 if($itemManinClass==1){//Inventy item class
                                     $itemAccount = $row['stockAccountId'];
                                 }
                                 else{//Other classes
                                     $itemAccount = $row['otherAccountId'];
                                 }
                                 //For FOC Items
				 if ($potype == 2) {//import Item
                                    $focCrAcc = $row['impAccId'];
                                 } else {//localItem
                                    $focCrAcc = $row['localAccId'];
                                 }				 
         
                       $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                                        ($entryId,'D',$itemAccount,$itemAmount,'$trnsDetails',$dimension)";		
                        $trnResult = $db->RunQuery2($sql);
                        
                        if($focQty!= 0){//For Foc Qty>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                            $itemFOCValue= $focQty * $unitPrice;
                            //Foc value not add to the  purchase invoice details table Only to trans action tables                           
                            
                            //Debit Entory                            
                            $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                                        ($entryId,'D',$itemAccount,$itemFOCValue,'$trnsDetails',$dimension)";
                            $trnResult = $db->RunQuery2($sql);
                            //Credit Entory                            
                            $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                                        ($entryId,'C',$focCrAcc,$itemFOCValue,'$trnsDetails',$dimension)";
                            $trnResult = $db->RunQuery2($sql);
                            
                        }//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>FOC END>>>>>>>>>>>>>>>>>>>>>>>>>>>
			
                        //>>>>>>>>>>>>>>>>>>>>>>>>>>>transaction table process - tax>>>>>>>>>>>>>>>>>>>>>>>>>
			if(count($taxDetails) != 0 && $trnResult)
			{
				foreach($taxDetails as $taxDetail)
				{
					$taxId 	= $taxDetail['taxId'];
					$taxAmount	= $taxDetail['taxValue'];
				
					$sql = "SELECT
					mst_financetaxactivate.intChartOfAccountId
					FROM mst_financetaxactivate
					WHERE
					mst_financetaxactivate.intTaxId = '$taxId' AND
					mst_financetaxactivate.intCompanyId = '$companyId'";
				 $result = $db->RunQuery2($sql);
				 $row = mysqli_fetch_array($result);
				 $taxAccount = $row['intChartOfAccountId'];
				 
                        $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                                        ($entryId,'D',$taxAccount,$taxAmount,'$trnsDetails',null)"; 
                        $db->RunQuery2($sql);                                
				}
			}
			//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                    }
                    //update grn table as Pay
                   $sqlGG="Update ware_grnheader as g , strReferenceNo	= '$invoiceReference', mst_locations as l
                            SET g.intPayStatus=1
                            where 
                                g.intGrnNo =  '$grnNumbers[$i]' AND
                                g.intGrnYear =  '$grnYears[$i]' AND
                                g.intCompanyId=l.intId And
                                l.intCompanyId =  '$companyId'";
                    $db->RunQuery2($sqlGG);

                }//grn iterate
                //>>>>>>>>>>>>>>>>>>>>>>transaction table process - supplier>>>>>>>>>>>>>>>>>>>>>>>>>
			 	$sql_sup = "SELECT
							mst_financesupplieractivate.intChartOfAccountId as supAcc
							FROM mst_financesupplieractivate
							WHERE
							mst_financesupplieractivate.intSupplierId =  '$supplierId' AND
							mst_financesupplieractivate.intCompanyId =  '$companyId'";
                        
				 $result_sup = $db->RunQuery2($sql_sup);                                 
				 $row_sup = mysqli_fetch_array($result_sup);
				 $supp = $row_sup['supAcc'];                            
                                
                      $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId,personType, personId) VALUES 
                                        ($entryId,'C','$supp',$totalValue,'$trnsDetails',null,'sup',$supplierId)"; 
                        
                       $trnResult = $db->RunQuery2($sql);                        
            }
        }
        //========================================================================
	if ($trnResult) 
	{
		$response['type'] = 'pass';
		$response['msg'] = 'Saved successfully.';
		//$response['invoiceNo'] = $invoiceReference;
        $db->commit();
	} 
	else 
	{
		$response['type'] = 'fail';
		$response['msg'] = $db->errormsg;
		$response['q'] = $sql;
        $db->rollback();//roalback
	}
    }
	echo json_encode($response);
	}
	catch (Exception $e)
	{
		$db->rollback();//roalback
		//echo $e->getMessage();
		$response['type'] = 'fail';
		$response['msg'] = $e->getMessage();
		$response['q'] = $sql;
		echo json_encode($response);
	}

function getAllGrnNos($grnYears,$grnNumbers){    
    $allGrns="";
    for($i=0;$i < count($grnYears);++$i){
        $allGrns.=$grnNumbers[$i]."/".$grnYears[$i].", ";
    }
    return $allGrns;
}
//--------------------------------------------------------------------------------------------
function getLatestAccPeriod($companyId) {
    global $db;
    $sql = "SELECT
				MAX(mst_financeaccountingperiod.intId) AS accId,
				mst_financeaccountingperiod.dtmStartingDate,
				mst_financeaccountingperiod.dtmClosingDate,
				mst_financeaccountingperiod.intStatus,
				mst_financeaccountingperiod_companies.intCompanyId,
				mst_financeaccountingperiod_companies.intPeriodId
				FROM
				mst_financeaccountingperiod
				Inner Join mst_financeaccountingperiod_companies ON mst_financeaccountingperiod_companies.intPeriodId = mst_financeaccountingperiod.intId
				WHERE
				mst_financeaccountingperiod_companies.intCompanyId =  '$companyId' AND mst_financeaccountingperiod.intStatus = '1'
				ORDER BY
				mst_financeaccountingperiod.intId DESC
				";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $latestAccPeriodId = $row['accId'];
    return $latestAccPeriodId;
}

//--------------------------------------------------------------------------------------------
function getNextInvoiceNo($companyId, $locationId) {
    global $db;
    $sql = "SELECT
				intPurchaseInvoiceNo
				FROM sys_finance_no
				WHERE
				intCompanyId = '$companyId' AND intLocationId = '$locationId'
				";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $nextInvoiceNo = $row['intPurchaseInvoiceNo'];

    $sql = "UPDATE `sys_finance_no` SET intPurchaseInvoiceNo=intPurchaseInvoiceNo+1 WHERE (intCompanyId = '$companyId' AND intLocationId = '$locationId')";
    $db->RunQuery($sql);
    return $nextInvoiceNo;
}

//--------------------------------------------------------------------------------------------
//============================================================================================
function encodeInvoiceNo($invoiceNumber, $accountPeriod, $companyId, $locationId) {
    global $db;
    $sql = "SELECT
				mst_financeaccountingperiod.intId,
				mst_financeaccountingperiod.dtmStartingDate,
				mst_financeaccountingperiod.dtmClosingDate,
				mst_financeaccountingperiod.intStatus
				FROM
				mst_financeaccountingperiod
				WHERE
				mst_financeaccountingperiod.intId =  '$accountPeriod'
				";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $startDate = substr($row['dtmStartingDate'], 0, 4);
    $closeDate = substr($row['dtmClosingDate'], 0, 4);
    $sql = "SELECT
				mst_companies.strCode AS company,
				mst_companies.intId,
				mst_locations.intCompanyId,
				mst_locations.strCode AS location,
				mst_locations.intId
				FROM
				mst_companies
				Inner Join mst_locations ON mst_locations.intCompanyId = mst_companies.intId
				WHERE
				mst_locations.intId =  '$locationId' AND
				mst_companies.intId =  '$companyId'
				";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $companyCode = $row['company'];
    $locationCode = $row['location'];
    $invoiceFormat = $companyCode . "/" . $locationCode . "/" . $startDate . "-" . $closeDate . "/" . $invoiceNumber;
    return $invoiceFormat;
}
//============================================================================================
?>

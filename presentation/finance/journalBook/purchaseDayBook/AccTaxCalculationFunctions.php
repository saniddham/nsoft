<?php

function calculateTax($itemAmount,$taxCode){
     global $db;
     $sqlTax="SELECT mst_financetaxgroup.strProcess FROM mst_financetaxgroup WHERE mst_financetaxgroup.intId =  '$taxCode'";
     $resultTax = $db->RunQuery2($sqlTax);
     $rowTax= mysqli_fetch_array($resultTax);
     $taxProess=$rowTax['strProcess'];
     
     $taxDetails=array();
     $details=array();
     $proces=explode("/",$taxProess);
     if(count($proces)==3){
         if($proces[1] == 'Inclusive'){
             $taxFirtRate=getTaxrate($proces[0]);
             $firstTaxVal = ($itemAmount*$taxFirtRate)/100;
             //add details to array
             $taxDetails[0]['taxId']=$proces[0];
             $taxDetails[0]['taxValue']=$firstTaxVal;
             //second tax
             $taxSecontRate=getTaxrate($proces[2]);
             $secondValue=($itemAmount+$firstTaxVal)*($taxSecontRate/100);
             //add second details to array
             $taxDetails[1]['taxId']=$proces[2];
             $taxDetails[1]['taxValue']=$secondValue;
             
             $totalTaxValue=$firstTaxVal+$secondValue;             
             $details[0]=$totalTaxValue;
             $details[1]=$taxDetails;             
         }
         else if($proces[1] == 'Exclusive'){
             $taxFirtRate=getTaxrate($proces[0]);
             $firstTaxVal = ($itemAmount*$taxFirtRate)/100;
             //add details to array
             $taxDetails[0]['taxId']=$proces[0];
             $taxDetails[0]['taxValue']=$firstTaxVal;
             //second tax
             $taxSecontRate=getTaxrate($proces[2]);
             $secondValue=($itemAmount)*($taxSecontRate/100);
             //add second details to array
             $taxDetails[1]['taxId']=$proces[2];
             $taxDetails[1]['taxValue']=$secondValue;
             
             $totalTaxValue=$firstTaxVal+$secondValue;             
             $details[0]=$totalTaxValue;
             $details[1]=$taxDetails;  
         }		
     }
     else if(count($proces)==1){
         $taxFirtRate=getTaxrate($proces[0]);
         $firstTaxVal = ($itemAmount*$taxFirtRate)/100;
         //add details to array
         $taxDetails[0]['taxId']=$proces[0];
         $taxDetails[0]['taxValue']=$firstTaxVal;
         
         $details[0]=$firstTaxVal;
         $details[1]=$taxDetails;
     }
     return $details;
    //return "hi...";
}

function getTaxrate($taxIsCode){
    global $db;
    $resultIsTax = $db->RunQuery2("SELECT mst_financetaxisolated.dblRate FROM mst_financetaxisolated WHERE mst_financetaxisolated.intId =  '$taxIsCode'");
    $rowIsTax= mysqli_fetch_array($resultIsTax);
    return $rowIsTax['dblRate']; 
}
?>

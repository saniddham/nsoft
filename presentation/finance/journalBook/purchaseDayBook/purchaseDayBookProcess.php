<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$companyId = $_SESSION['headCompanyId'];
$location = $_SESSION['CompanyID'];

$poNumber= trim($_REQUEST['id']);

//$thisFilePath =  $_SERVER['PHP_SELF'];
include  "{$backwardseperator}dataAccess/permisionCheck.inc";
// ======================Check Exchange Rate Updates========================

$currentDate = date("Y-m-d");
//
$sql = "SELECT COUNT(*) AS 'no'
		FROM
		mst_financeexchangerate
		WHERE
		mst_financeexchangerate.dtmDate =  '$currentDate'
		";
$result = $db->RunQuery($sql);
$row= mysqli_fetch_array($result);
if($row['no']==0)
	{
		$str =  "Please Update Exchange Rates Before ".$status." Received Payments .";
		$str .= $row['NameList'];
		$maskClass="maskShow";
	}
	else
	{
		$maskClass="maskHide";
	}
// =========Add by dulakshi 2013.03.25=================================
	//	$supplier = $_post['cmbSupplier']; echo($supplier);
		
	
//==========================================================================
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Purchase Day Book (GRN)</title>
        
        <link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/tblstyle.css" rel="stylesheet" type="text/css" />

        <script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
        <script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
        <script src="purchaseDayBook.js" type="text/javascript"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

        <link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
        <script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

        <link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css">
        <link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css">   

    </head>

    <body>
        <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
                <td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator . 'Header.php'; ?></td>
            </tr> 
        </table>
        <div style="display:none" id="divMask" class="<?php echo $maskClass?> mask"> <?php echo $str; ?></div>

        <script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
        <script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
        <script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>
        <form id="frmPurchaseDayBook" name="frmPurchaseDayBook" method="post" action="purchaseDayBook-db-set.php" autocomplete="off">
            <div align="center">
                <div class="trans_layoutL">
                    <div class="trans_text">Purchase Day Book</div>
                    <table width="100%">
                    
<!-- ===========Add by dulakshi 2013.03.22========= -->
             <tr>
               <td>
                 <table width="100%" height="50px" class="tableBorder_allRound" style="display:none">
                   <tr>
                   <td width="90px"></td>                                                                            
                       <td bgcolor="#FFFFFF" class="normalfntRight" >Supplier</td>
                       <td bgcolor="#FFFFFF"><span class="normalfntMid">
                           <select name="cmbSupplier" id="cmbSupplier" style="width:150px" onchange ="LoadPoNo()">
                                <option value=""></option>
                    <?php
						$sql = "SELECT
									mst_supplier.intId,
									mst_supplier.strCode,
									mst_supplier.strName
								FROM mst_supplier
								WHERE mst_supplier.intStatus =  '1' order by strName ASC";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							if($row['intId']==$supplier)
							echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";	
							else
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
						}
					?>
                             </select>
                             </span></td>  
                                                                           
                          <td width="100" align="right"><span class="normalfntRight">Shipment Term </span></td>
              			  <td bgcolor="#FFFFFF"><span class="normalfntMid">
                           <select name="cmbShipTerm" id="cmbShipTerm" style="width:150px" onchange ="LoadPoNo()">
                           <option value=""></option>                            
                                <option value="1">Local</option>
                                <option value="2">Import</option>
                               
                            </select>
                         </span></td>           			  
                                                                                                  
                        </tr>
                        
                        <tr> 
                        <td width="90px"></td>                                           
                   		 <td bgcolor="#FFFFFF" class="normalfntRight">Year</td>
                   			 <td bgcolor="#FFFFFF"><span class="normalfntMid">
                           <select name="cmbYear" id="cmbYear" style="width:150px" onchange ="LoadPoNo()">
                           <option value=""></option>                            
                                <option value="2012">2012</option>
                                <option value="2013">2013</option>
                                <option value="2014">2014</option>
                                <option value="2015">2015</option>
                                <option value="2016">2016</option>
                                <option value="2017">2017</option>
                                <option value="2018">2018</option>
                                <option value="2019">2019</option>
                                <option value="2020">2020</option>
                            </select>
                         </span></td> 
                         
                         <td bgcolor="#FFFFFF" class="normalfntRight" >Month</td>
                   			 <td bgcolor="#FFFFFF"><span class="normalfntMid">
                           <select name="cmbMonth" id="cmbMonth" style="width:150px" onchange ="LoadPoNo()">
                           <option value=""></option>                            
                                <option value="01">January</option>
                                <option value="02">February</option>
                                <option value="03">March</option>
                                <option value="04">April</option>
                                <option value="05">May</option>
                                <option value="06">June</option>
                                <option value="07">July</option>
                                <option value="08">August</option>
                                <option value="09">September</option>
                                <option value="10">October</option>
                                <option value="11">November</option>
                                <option value="12">December</option>
                            </select>
                         </span></td> 
                   		</tr>
                   		
                   		 <tr>  
                   		 <td width="90px"></td>    
                   		  <td bgcolor="#FFFFFF" class="normalfntRight">From</td>
            			  <td align="left" >
            			  		<input name="fromDate" type="text" class="txtbox" id="fromDate" style="width:150px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" onblur ="LoadPoNo()"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" />
            			  </td>
                        
                          <td bgcolor="#FFFFFF" class="normalfntRight">To</td>
            			  <td align="left">
            			  		<input name="toDate" type="text" class="txtbox" id="toDate" style="width:150px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" onblur ="LoadPoNo()"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" />
            			  </td>
                   		</tr>
                        
                        
                      </table>
                     </td>
                   </tr>
                   
                   
                                    
                        <tr>
                            <td colspan="2">
                                <table width="100%" class="tableBorder_allRound">
                                 <!--     <tr>                                        
                                        <td bgcolor="#FFFFFF" class="normalfntRight">Purchase Order</td>
                                        <td width="318" bgcolor="#FFFFFF"><span class="normalfntMid">
							                <select name="cmbPo" id="cmbPo"  style="width:115px" class="validate[required]" >          
							                </select>
							            </span></td>    
							            <td width="125">&nbsp;</td>       
							                                                                                      
                                    </tr> --> 
                                    
                                    <tr>                                        
                                        <td bgcolor="#FFFFFF" class="normalfntRight">Purchase Order :</td>                                      
							            <td bgcolor="#FFFFFF" class="normalfnt"><span class="normalfnt">
                                            <span class="normalfnt"><input name="txtPONo"  type="text" class="validate[required]" id="txtPONo" value="<?php echo $poNumber;?>" style="width:180px; background-color:#F4FFFF;text-align:center;" onblur="LoadGRNDetails()"/>
                                            </span>
                                        </td>        
							            <td width="125">&nbsp;</td>       
							                                                                                      
                                    </tr>                             
                                    
                                    <tr>
                                        <td bgcolor="#FFFFFF" class="normalfntRight">View Po :</td>
                                        <td bgcolor="#FFFFFF" class="normalfnt"><div id="poView"></div></td>
                                        <td width="125">&nbsp;</td>                                         
                                    </tr>
                                    <tr>
                                        <td bgcolor="#FFFFFF" class="normalfntRight">Supplier :</td>
                                        <td bgcolor="#FFFFFF" class="normalfnt"><div id="sup"></div></td> 
                                                                               
                                    </tr>
                                    <tr>
                                        <td bgcolor="#FFFFFF" class="normalfntRight">PO Type :</td>
                                        <td bgcolor="#FFFFFF" class="normalfnt"><div id="poType"></div></td>                                        
                                    </tr>
                                    <tr>
                                        <td bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
                                        <td bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>                                        
                                    </tr>                                     
                                    <tr>  
                                        <td bgcolor="#FFFFFF" class="normalfntRight">Good Received Note</td> 
                                        <td valign="top" bgcolor="#FFFFFF">
                                            <div id="divGrid">
                                                <table width="100%">                                                     
                                                    <tbody id="allgrns" class="normalfnt" bgcolor="#FFFFFF"><!-- load Grns -->
                                                    </tbody>
                                                </table>
                                            </div>              
                                        </td>                                        
                                    </tr>
                                    <tr>
                                        <td class="normalfntRight"><span class="name">Invoice Number <span class="compulsoryRed">*</span></span></td>
                                        <td bgcolor="#FFFFFF" class="normalfnt"><span class="normalfnt">
                                            <span class="normalfnt"><input name="txtInvoceNo"  type="text" class="validate[required]" id="txtInvoceNo" style="width:180px; background-color:#F4FFFF;text-align:center;" />
                                            </span>
                                        </td>                                        
                                    </tr>                            
                                    
                                </table>
                            </td>
                        </tr>                       
                        
                        <tr>
                            <td colspan="2">
                                <table width="100%">
                                    <tr>
                                        <td width="100%" height="34" class="tableBorder_allRound"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                                                <tr>
                                                    <td width="100%" align="center" bgcolor=""><img style="display:" border="0" src="../../../../images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/><img src="../../../../images/Tclose.jpg" alt="Close" name="Close" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                                                </tr>
                                            </table></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </form>
    </body>
</html>
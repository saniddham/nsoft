//permision for add 
if(intAddx)
{
    $('#frmPurchaseDayBook #butNew').show();
    $('#frmPurchaseDayBook #butSave').show();
}
//permision for edit 
if(intEditx)
{
    $('#frmPurchaseDayBook #butSave').show();
    $('#frmPurchaseDayBook #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
}
//permision for delete
if(intDeletex)
{
    $('#frmPurchaseDayBook #butDelete').show();
    $('#frmPurchaseDayBook #cboSearch').removeAttr('disabled');
}
//permision for view
if(intViewx)
{
    $('#frmPurchaseDayBook #cboSearch').removeAttr('disabled');
}
function alertx()
{
    $('#frmPurchaseDayBook #butSave').validationEngine('hide') ;
}
$(document).ready(function() {
	
	$('#chkGrns').live('click',AddInvoice);
	// ===========Add by dulaskshi 2013.03.25===========
	LoadPoNo();
	LoadGRNDetails();
	
    $("#frmPurchaseDayBook").validationEngine();
    
    //permision for add 
    if(intAddx)
    {
        $('#frmPurchaseDayBook #butSave').show();
    }
    //get Grn from PO-------------------------------
   /* $('#frmPurchaseDayBook #cmbPo').change(function(){
        if($('#frmPurchaseDayBook #cmbPo').val()!=""){
            var po=($('#frmPurchaseDayBook #cmbPo').val()).split("-");
            var poNumber=po[1];
            var poYear=po[0];
            
            var url ='purchaseDayBook-db-get.php?requestType=loadGrns&poNumber='+poNumber+"&poYear="+poYear;
            var obj=$.ajax({
                url:url,
                dataType:'json',
                success:function(json){
                    $('#frmPurchaseDayBook #allgrns').html(json.grnList);                    
                    $('#frmPurchaseDayBook #sup').html(json.supplier);
                    $('#frmPurchaseDayBook #poType').html(json.poType);
                    $('#frmPurchaseDayBook #poView').html(json.poView);
                },
                async:false
            });   
            
        //document.getElementById('allgrns').innerHTML=obj.responseText;
        }
        
    });*/
    //generate the invoice with Grns(//save button click event)
    $('#frmPurchaseDayBook #butSave').click(function(){
        if ($('#frmPurchaseDayBook').validationEngine('validate')) { 
            var grns=document.getElementsByName("chkGrns"); 
            var grnYears=new Array();
            var grnNumbers=new Array();
            var x=0;
            for(var i=0; i<grns.length;++i){
                if(grns[i].checked){
                    var grn=(grns[i].value).split("-");
                    grnYears[x]=grn[0];
                    grnNumbers[x]=grn[1];
                    ++x;               
                }            
            }
            var invoiceNo=$('#frmPurchaseDayBook #txtInvoceNo').val();
            var poNo=$('#frmPurchaseDayBook #txtPONo').val();       
            var url = "purchaseDayBook-db-set.php";
            var obj = $.ajax({
                url:url,
                dataType: "json",  
                data:$("#frmPurchaseDayBook").serialize()+'&requestType=add&grnYears='+grnYears+'&grnNumbers='+grnNumbers+'&invoiceNo='+invoiceNo+'&poNo='+poNo,
                async:false,
			
                success:function(json){
                    $('#frmPurchaseDayBook #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
                    if(json.type=='pass')
                    {
                        $('#frmPurchaseDayBook').get(0).reset();						
                        var t=setTimeout("alertx()",1000);
                       // $('#frmPurchaseDayBook #cmbPo').val('');
                        $('#frmPurchaseDayBook #txtPONo').html('');
                        $('#frmPurchaseDayBook #allgrns').html('');                    
                        $('#frmPurchaseDayBook #sup').html('');
                        $('#frmPurchaseDayBook #poType').html('');
                        $('#frmPurchaseDayBook #poView').html('');
                        return;
                    }
                    var t=setTimeout("alertx()",3000);
                },
                error:function(xhr,status){
					
                    $('#frmPurchaseDayBook #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
                    var t=setTimeout("alertx()",3000);
                }		
            }); 
        //alert("end");
        }
    });
});

function alertx()
{
    $('#frmPurchaseDayBook #butSave').validationEngine('hide') ;
}
function alertDelete()
{
    $('#frmPurchaseDayBook #butSave').validationEngine('hide') ;
}

//===================Add by dulakshi 2013.03.25============
function LoadPoNo()
{	
	var supName = $('#frmPurchaseDayBook #cmbSupplier').val(); 
	var shipTerm = $('#frmPurchaseDayBook #cmbShipTerm').val(); 
	var year = $('#frmPurchaseDayBook #cmbYear').val(); 
	var month = $('#frmPurchaseDayBook #cmbMonth').val(); 
	var fromDate = $('#frmPurchaseDayBook #fromDate').val(); 
	var toDate = $('#frmPurchaseDayBook #toDate').val(); 
	
	var data = "&supplier="+supName+"&shipmentTerm="+shipTerm+"&year="+year+"&month="+month+"&fromDate="+fromDate+"&toDate="+toDate;
	var url  = "purchaseDayBook-db-get.php?requestType=loadPONumber" + data;
	
	var httpobj = $.ajax({url:url,async:false})
	$('#frmPurchaseDayBook #cmbPo').html(httpobj.responseText);		
}

function LoadGRNDetails()
{
	 if($('#frmPurchaseDayBook #txtPONo').val()!=""){
         var po=($('#frmPurchaseDayBook #txtPONo').val()).split("/");
         var poNumber=po[0];
         var poYear=po[1];
         
         var url ='purchaseDayBook-db-get.php?requestType=loadGrns&poNumber='+poNumber+"&poYear="+poYear;
         var obj=$.ajax({
             url:url,
             dataType:'json',
             success:function(json){
                 $('#frmPurchaseDayBook #allgrns').html(json.grnList);                    
                 $('#frmPurchaseDayBook #sup').html(json.supplier);    
                 $('#frmPurchaseDayBook #poType').html(json.poType);
                 $('#frmPurchaseDayBook #poView').html(json.poView);
             },
             async:false
         });   
         
     //document.getElementById('allgrns').innerHTML=obj.responseText;
     }
}

function AddInvoice()
{
	if($(this).is(':checked'))
		$('#txtInvoceNo').val($(this).parent().parent().find('.cls_td_invoice').html());
	else
		$('#txtInvoceNo').val('');
}
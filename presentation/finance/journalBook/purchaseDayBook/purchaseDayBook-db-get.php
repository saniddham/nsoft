<?php
session_start();
$backwardseperator = "../../../../";
$mainPath 	= $_SESSION['mainPath'];
$userId 	= $_SESSION['userId'];
$requestType 	= $_REQUEST['requestType'];
$companyId = $_SESSION['headCompanyId'];
$location = $_SESSION['CompanyID'];
include "{$backwardseperator}dataAccess/Connector.php";

if($requestType=='loadGrns'){
    $poNumber= trim($_REQUEST['poNumber']);
    $poYear=trim($_REQUEST['poYear']); 
    $sql="SELECT
            ware_grnheader.intGrnNo,
            ware_grnheader.intGrnYear,
            mst_supplier.strName as supplier,
            mst_shipmentterms.strName as terms,
			ware_grnheader.strInvoiceNo
            FROM
            ware_grnheader
            LEFT OUTER JOIN mst_locations ON ware_grnheader.intCompanyId = mst_locations.intId
            INNER JOIN trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
            INNER JOIN mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
            INNER JOIN mst_shipmentterms ON trn_poheader.intShipmentTerm = mst_shipmentterms.intId
            WHERE
            ware_grnheader.intPoNo =  '$poNumber' AND
            ware_grnheader.intPoYear =  '$poYear' AND
            ware_grnheader.intPaystatus =  '0' AND
            mst_locations.intCompanyId =  '$companyId' AND
            ware_grnheader.intStatus=1";
    
    $result = $db->RunQuery($sql);
    $grnList="";
    $supplier;
    $poType;
    while($row=mysqli_fetch_array($result)){
         $grnNumber=$row['intGrnNo'];
         $grnYear=$row['intGrnYear'];
         $supplier=$row['supplier'];
         $poType=$row['terms'];
		 $invoiceNo	= $row["strInvoiceNo"];
         
    $grnList.= "<tr class=normalfnt bgcolor=#00FF66>
        <td><input type=\"checkbox\" name=\"chkGrns\" id=\"chkGrns\" value=\"$grnYear-$grnNumber\" /></td>
        <td title=\"GRN No\"><a target=\"_blank\"  href=\"{$backwardseperator}presentation/warehouse/grn/listing/rptGrn.php?grnNo=$grnNumber&year=$grnYear\" >$grnNumber/$grnYear</a></td>
		<td title=\"Invoice No\" class=\"cls_td_invoice\">$invoiceNo</td>
        </tr>
         ";
    }
    $viewPo="<a target=\"_blank\" href=\"{$backwardseperator}presentation/procurement/purchaseOrder/listing/rptPurchaseOrder.php?poNo=$poNumber&year=$poYear\" >$poNumber/$poYear</a>";
    $response['grnList']=$grnList;
    $response['supplier']=$supplier;
    $response['poType']=$poType;
    $response['poView']=$viewPo;
    echo json_encode($response);
}

//================Add by dulakshi 2013.03.25==========================
else if($requestType=='loadPONumber'){

	$supName	= $_REQUEST['supplier'];
    $shipTerm	= $_REQUEST['shipmentTerm']; 
    $year		= $_REQUEST['year']; 
    $month		= $_REQUEST['month']; 
    $fromDate	= $_REQUEST['fromDate']; 
    $toDate		= $_REQUEST['toDate']; 
    
    $condition == "";
		if(isset($supName) && $supName != ""){
			$condition = "AND trn_poheader.intSupplier = '".$supName."'"; 	
		}
		
		if(isset($shipTerm) && $shipTerm != ""){
			$condition = "AND trn_poheader.intShipmentTerm = '".$shipTerm."'"; 	
		}
		
		if(isset($supName) && $supName != "" && isset($shipTerm) && $shipTerm != ""){
			$condition = "AND trn_poheader.intSupplier = '".$supName."' AND trn_poheader.intShipmentTerm = '".$shipTerm."'"; 	
		}
		
		if(isset($year) && $year != ""){
			$condition = "AND trn_poheader.intPOYear = '".$year."'"; 	
		}
		
		if(isset($supName) && $supName != "" && isset($year) && $year != ""){
			$condition = "AND trn_poheader.intSupplier = '".$supName."' AND trn_poheader.intPOYear = '".$year."'"; 	
		}	   
		
 		if(isset($month) && $month != ""){
			$condition = "AND SUBSTRING(trn_poheader.dtmPODate,6,2) = '".$month."'"; 	
		}
		
		if(isset($supName) && $supName != "" && isset($month) && $month != ""){
			$condition = "AND trn_poheader.intSupplier = '".$supName."' AND SUBSTRING(trn_poheader.dtmPODate,6,2) = '".$month."'"; 	
		}

		if(isset($year) && $year != "" && isset($month) && $month != ""){
			$condition .= "AND SUBSTRING(trn_poheader.dtmPODate,1,4) = '".$year."' AND SUBSTRING(trn_poheader.dtmPODate,6,2) = '".$month."' ";		
		}
		
 		if(isset($toDate) && $toDate != "" && isset($fromDate) && $fromDate != ""){
			$condition .= " AND trn_poheader.dtmPODate BETWEEN '".$fromDate."' AND '".$toDate."'";		
		}
		
		$sql = "SELECT
					trn_poheader.intPONo,
					trn_poheader.intPOYear,
					ware_grnheader.intCompanyId					
			 	FROM
					trn_poheader
					Inner Join mst_locations ON trn_poheader.intDeliveryTo = mst_locations.intId				
					Inner Join mst_shipmentterms ON trn_poheader.intShipmentTerm = mst_shipmentterms.intId
					Inner Join ware_grnheader ON trn_poheader.intPONo = ware_grnheader.intPoNo AND mst_locations.intId = ware_grnheader.intCompanyId
				 WHERE
					mst_locations.intCompanyId =  '$companyId' AND
					(mst_shipmentterms.intId =  '1' OR mst_shipmentterms.intId =  '2') AND
					ware_grnheader.intStatus =  '1' $condition AND
					trn_poheader.intStatus =  '1'
				 ORDER BY
					trn_poheader.intPOYear DESC,
					trn_poheader.intPONo DESC";
		
	/*	$sql = "SELECT
					trn_poheader.intPONo,
					trn_poheader.intPOYear,
					ware_grnheader.intCompanyId					
			 	FROM
					trn_poheader
					Inner Join mst_locations ON trn_poheader.intCompany = mst_locations.intId
					Inner Join mst_shipmentterms ON trn_poheader.intShipmentTerm = mst_shipmentterms.intId
					Inner Join ware_grnheader ON trn_poheader.intPONo = ware_grnheader.intPoNo AND mst_locations.intId = ware_grnheader.intCompanyId
				 WHERE
					mst_locations.intCompanyId =  '$companyId' AND
					(mst_shipmentterms.intId =  '1' OR mst_shipmentterms.intId =  '2') AND
					ware_grnheader.intStatus =  '1' $condition AND
					trn_poheader.intStatus =  '1'
				 ORDER BY
					trn_poheader.intPOYear DESC,
					trn_poheader.intPONo DESC";*/
		
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{			
			$html .= "<option value=\"" . $row['intPOYear'] . "-" . $row['intPONo'] . "\">" . $row['intPONo'] . "/" . $row['intPOYear'] . "</option>";                     
		}
		echo $html;      
}

?>

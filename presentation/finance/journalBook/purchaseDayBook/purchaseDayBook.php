<?php
session_start();
$backwardseperator = "../../../../";
$thisFilePath =  $_SERVER['PHP_SELF'];

include  "{$backwardseperator}dataAccess/permisionCheck2.inc";

//$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
//$result = $db->RunQuery($sql);
//while($row=mysqli_fetch_array($result))
//{
//	$companyId = $row['intCompanyId'];   
//}

$companyId 	= $_SESSION['headCompanyId'];

if($_SERVER['REQUEST_METHOD'] == "POST")
{
	$optionValue = trim($_REQUEST['cboSearchOption']);
	$searchValue = trim($_REQUEST['txtSearchValue']);
}

//---------------------------------------------New Grid-----------------------------------------------------
include_once "../../../../libraries/jqdrid/inc/jqgrid_dist.php";

				$sql = "select * from(SELECT
							trn_poheader.intSupplier,							
							mst_supplier.strName As SupplierName,
							mst_shipmentterms.intId,
							mst_shipmentterms.strName As ShipmentName,
							trn_poheader.dtmPODate As Date,
							ware_grnheader.intGrnNo As GRNNumber,
							trn_poheader.intPONo As PONumber,
							ware_grnheader.intCompanyId,
							trn_poheader.intPOYear As POYear,
							ware_grnheader.intGrnYear As GRNYear,
							concat(trn_poheader.intPONo, '/' ,trn_poheader.intPOYear) As poNumYear,
							concat(ware_grnheader.intGrnNo, '/' ,ware_grnheader.intGrnYear) As grnNumYear							
						FROM
							trn_poheader
							Inner Join mst_locations ON trn_poheader.intDeliveryTo = mst_locations.intId
							Inner Join mst_shipmentterms ON trn_poheader.intShipmentTerm = mst_shipmentterms.intId
							Inner Join ware_grnheader ON trn_poheader.intPONo = ware_grnheader.intPoNo AND mst_locations.intId = ware_grnheader.intCompanyId
							Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
						WHERE
							mst_locations.intCompanyId =  '$companyId' AND
							(mst_shipmentterms.intId =  '1' OR mst_shipmentterms.intId =  '2') AND
							ware_grnheader.intStatus =  '1' AND
							trn_poheader.intStatus =  '1') as t where 1=1";
				
				//ORDER BY
							//trn_poheader.intPOYear DESC,
							//trn_poheader.intPONo DESC

				/* $sql = "select * from(SELECT
						fin_customer_salesinvoice_header.intInvoiceNo,
						fin_customer_salesinvoice_header.strReferenceNo AS InvoiceNumber,
						'More' AS More,
						fin_customer_salesinvoice_header.intCustomerId,
						INV.dblQty AS Qty,
						INV.dblUnitPrice AS UnitPrice,
						INV.dblDiscount,
						INV.dblTaxAmount AS taxAmount,
						mst_customer.strName AS Customer,
						sys_users.strUserName AS Marketer,
						fin_customer_salesinvoice_header.intMarketerId,
						fin_customer_salesinvoice_header.strPoNo AS PONumber,
						fin_customer_salesinvoice_header.intCompanyId,
						fin_customer_salesinvoice_header.dtmDate AS `Date`,
						mst_financecurrency.strCode AS Currency,
						fin_customer_salesinvoice_header.strRemark AS Memo,
						INV.strStyleNo AS StyleNumber,
						INV.strGraphicNo GraphicNumber,
						INV.strOrderNo AS ScheduleNo,
						INV.strLineItem AS LineItem,
						INV.strCusLocation AS CusLocation
						FROM
						fin_customer_salesinvoice_details AS INV
						Inner Join fin_customer_salesinvoice_header ON fin_customer_salesinvoice_header.strReferenceNo = INV.strReferenceNo AND INV.intInvoiceNo = fin_customer_salesinvoice_header.intInvoiceNo AND INV.intAccPeriodId = fin_customer_salesinvoice_header.intAccPeriodId AND INV.intCompanyId = fin_customer_salesinvoice_header.intCompanyId
						Inner Join mst_customer ON mst_customer.intId = fin_customer_salesinvoice_header.intCustomerId
						Inner Join mst_financecurrency ON mst_financecurrency.intId = fin_customer_salesinvoice_header.intCurrencyId
						Inner Join sys_users ON fin_customer_salesinvoice_header.intMarketerId = sys_users.intUserId
						WHERE
						fin_customer_salesinvoice_header.intDeleteStatus =  '0' AND fin_customer_salesinvoice_header.intCompanyId = '$companyId') as t where 1=1"; // AND INV.intLocationId = fin_customer_salesinvoice_header.intLocationId
				*/				
				
$POLink = "purchaseDayBookProcess.php?id={poNumYear}"; 
//$POReport = "rptPurchaseOrder.php?id={poNumYear}";

//Supplier
$col["title"] = "Supplier"; // caption of column
$col["name"]  = "SupplierName"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "10";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Shipment Term
$col["title"] = "Shipment Term"; // caption of column
$col["name"]  = "ShipmentName"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Date
$col["title"] = "Date"; // caption of column
$col["name"]  = "Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//GRN Number
$col["title"] = "G.R.N. Number"; // caption of column
$col["name"]  = "grnNumYear"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//PO Number
$col["title"] = "P.O. Number"; // caption of column
$col["name"]  = "poNumYear"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "center";
$col['link']	= $POLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

/*//View // Query - 'More' AS More,
$col["title"] = "View"; // caption of column
$col["name"] = "More"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $POReport;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;*/

$jq = new jqgrid('',$db);

$grid["caption"] 		= "Purchase DayBook";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'PONumber'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes

//<><><><><><>======================Compulsory Code==============================<><><><><><>
	$jq->set_options($grid);
	$jq->select_command = $sql;
	$jq->set_columns($cols);
	$jq->set_actions(array(	
		"add"=>false, // allow/disallow add
		"edit"=>false, // allow/disallow edit
		"delete"=>false, // allow/disallow delete
		"rowactions"=>false, // show/hide row wise edit/del/save option
		"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
		"export"=>true
	) 
	);

$out = $jq->render("list1");
//<><><><><><>======================Compulsory Code==============================<><><><><><>

//---------------------------------------------New Grid-----------------------------------------------------
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Purchase DayBook</title>

<script type="text/javascript" language="javascript">
function pageSubmit()
{
	document.getElementById('frmPurchaseDayBook').submit();	
}
function clearValue()
{
	document.getElementById('txtSearchValue').value = '';
	document.getElementById('txtSearchValue').focus();
}
</script>

</head>

<body>
<form id="frmPurchaseDayBook" name="frmPurchaseDayBook" method="post" autocomplete="off" action="">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
<div align="center">
<!-------------------------------------------New Grid---------------------------------------------------->
<link rel="stylesheet" type="text/css" media="screen" href="../../../../libraries/jqdrid/js/themes/smoothness/jquery-ui.custom.css"></link>	
<link rel="stylesheet" type="text/css" media="screen" href="../../../../libraries/jqdrid/js/jqgrid/css/ui.jqgrid.css"></link>	

<script src="../../../../libraries/jqdrid/js/jquery.min.js" type="text/javascript"></script>
<script src="../../../../libraries/jqdrid/js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
<script src="../../../../libraries/jqdrid/js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>	
<script src="../../../../libraries/jqdrid/js/themes/jquery-ui.custom.min.js" type="text/javascript"></script>
<script src="../../../../libraries/javascript/script.js" type="text/javascript"></script>
<!-------------------------------------------New Grid---------------------------------------------------->

	<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
		<tr>
    		<td>
    			<table width="100%" border="0">     
            	</table>
			</td>
      	</tr>
        <tr>
       		<td>
        		<div align="center" style="margin:10px">
				<?php echo $out?>
        		</div>
        	</td>
       </tr>
       <tr>
        	<td align="center" class="tableBorder_allRound"><a href="../../../../main.php"><img  src="../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a>
        	</td>
       </tr>
    </table>
  
</div>
</form>
</body>
</html>

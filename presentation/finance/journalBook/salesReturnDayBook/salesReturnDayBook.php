<?php
	$backwardseperator = "../../../../";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Sales Return Day Book</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
<script src="deposit-js.js" type="text/javascript"></script>

</head>

<body>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<div align="center">
  <div class="trans_layoutL">
    <div class="trans_text">Sales Return Day Book</div>
<table width="100%">
      <tr>
        <td width="50%" colspan="2">
          <table width="100%" class="tableBorder_allRound">
            <tr>
              <td width="224" bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
              <td width="82" bgcolor="#FFFFFF" class="normalfnt">From</td>
              <td width="150" bgcolor="#FFFFFF"><input name="adviceDate2" type="text" value="<?php echo date("Y-m-d"); ?>" class="txtbox" id="adviceDate1" style="width:98px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
              <td width="67" bgcolor="#FFFFFF"><span class="normalfnt">To</span></td>
              <td width="139" bgcolor="#FFFFFF" class=""><input name="adviceDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="txtbox" id="adviceDate3" style="width:98px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
              <td width="220" bgcolor="#FFFFFF"><span class="normalfnt"><img src="../../../../images/go.png" width="30" height="22" /></span></td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="2" align="center">
        </td>
      </tr>
      <td width="50%"></tr>
      <tr>
        <td colspan="2"><table width="93%">
            <tr>
              <td>
                  <div style="overflow:scroll;width:900px;height:350px;" id="divGrid"><table width="185%" id="tblMainGrid2" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
                    <tr class="">
                      <td width="7%"  height="23" bgcolor="#FAD163" class="normalfntMid"><strong>Date</strong></td>
                      <td width="8%" align="center"  bgcolor="#FAD163" class="normalfntMid"><strong>Customer</strong></td>
                      <td width="6%" bgcolor="#FAD163" class="normalfntMid"  ><strong>Job No.</strong></td>
                      <td width="12%" bgcolor="#FAD163" class="normalfntMid"  ><strong>Sales Invoice No.</strong></td>
                      <td width="8%" bgcolor="#FAD163" class="normalfntMid"  ><strong>Credit Note No.</strong></td>
                      <td width="6%" bgcolor="#FAD163" class="normalfntMid"  ><strong>currency</strong></td>
                      <td width="3%" bgcolor="#FAD163" class="normalfntMid"  ><span class="normalfnt"><strong>Rate</strong></span></td>
                      <td width="16%" bgcolor="#FAD163" class="normalfntMid"  ><strong>Item Details</strong></td>
                      <td width="6%" bgcolor="#FAD163" class="normalfntMid"  ><strong>Accounts</strong></td>
                      <td width="12%" align="center" bgcolor="#FAD163" class="normalfntMid"  ><strong>Memo</strong></td>
                      <td width="8%" bgcolor="#FAD163" class="normalfntMid"  ><strong>Debit Amounts</strong></td>
                      <td width="34%" bgcolor="#FAD163" class="normalfntMid"  ><strong>Credit Amounts</strong></td>
                      <td width="6%" bgcolor="#FAD163" class="normalfntMid"  ><strong>Dimension</strong></td>
                    </tr>
                    <tr class="normalfnt">
                      <td bgcolor="#FFFFFF" class="normalfntMid">15-06-2012</td>
                      <td bgcolor="#FFFFFF">Brandix</td>
                      <td align="center"  bgcolor="#FFFFFF"><a href="#">JBN501</a></td>
                      <td align="center"  bgcolor="#FFFFFF"><a href="#">000000021</a></td>
                      <td align="center"  bgcolor="#FFFFFF"><a href="#">CN00001</a></td>
                      <td align="center"  bgcolor="#FFFFFF">LKR</td>
                      <td align="center"  bgcolor="#FFFFFF">1.00</td>
                      <td  bgcolor="#FFFFFF">Style 001, Style 002, ..</td>
                      <td  bgcolor="#FFFFFF"><span class="normalfntMid">
                        <select name="select" id="select" style="width:115px">
                          <option>Raw Materials</option>
                          <option>Advertising</option>
                          <option>Cost of Good Sold</option>
                          <option>Stationery</option>
                          <option>Office Equipment</option>
                          <option>Other Materials</option>
                        </select>
                      </span></td>
                      <td align="center"  bgcolor="#FFFFFF"><input type="text" name="textfield5" id="textfield5" style="width:200px" /></td>
                      <td align="right"  bgcolor="#FFFFFF">100000.00</td>
                      <td align="right"  bgcolor="#FFFFFF">&nbsp;</td>
                      <td  bgcolor="#FFFFFF"><span class="normalfntMid">
                        <select name="select6" id="select6" style="width:100px">
                          <option>&nbsp;</option>
                          <option>R &amp; D</option>
                          <option>Projects</option>
                          <option>Others</option>
                        </select>
                      </span></td>
                    </tr>
                    <tr class="normalfnt">
                      <td bgcolor="#FFFFFF" class="normalfntMid">15-06-2012</td>
                      <td bgcolor="#FFFFFF">Brandix</td>
                      <td align="center"  bgcolor="#FFFFFF"><a href="#">JBN501</a></td>
                      <td align="center"  bgcolor="#FFFFFF"><a href="#">000000021</a></td>
                      <td align="center"  bgcolor="#FFFFFF"><a href="#">CN00001</a></td>
                      <td align="center"  bgcolor="#FFFFFF">LKR</td>
                      <td align="center"  bgcolor="#FFFFFF">1.00</td>
                      <td  bgcolor="#FFFFFF">Style 001, Style 002, ..</td>
                      <td  bgcolor="#FFFFFF"><span class="normalfntMid">
                        <select name="select3" id="select3" style="width:115px">
                          <option>Advertising</option>
                          <option>Raw Materials</option>
                          <option>Cost of Good Sold</option>
                          <option>Stationery</option>
                          <option>Office Equipment</option>
                          <option>Other Materials</option>
                        </select>
                      </span></td>
                      <td align="center"  bgcolor="#FFFFFF"><input type="text" name="textfield" id="textfield" style="width:200px" /></td>
                      <td align="right"  bgcolor="#FFFFFF">12000.00</td>
                      <td align="right"  bgcolor="#FFFFFF">&nbsp;</td>
                      <td  bgcolor="#FFFFFF"><span class="normalfntMid">
                        <select name="select8" id="select8" style="width:100px">
                          <option>&nbsp;</option>
                          <option>R &amp; D</option>
                          <option>Projects</option>
                          <option>Others</option>
                        </select>
                      </span></td>
                    </tr>
                    <tr class="normalfnt">
                      <td bgcolor="#FFFFFF" class="normalfntMid">15-06-2012</td>
                      <td bgcolor="#FFFFFF">Brandix</td>
                      <td align="center"  bgcolor="#FFFFFF"><a href="#">JBN501</a></td>
                      <td align="center"  bgcolor="#FFFFFF"><a href="#">000000021</a></td>
                      <td align="center"  bgcolor="#FFFFFF"><a href="#">CN00001</a></td>
                      <td align="center"  bgcolor="#FFFFFF">LKR</td>
                      <td align="center"  bgcolor="#FFFFFF">1.00</td>
                      <td  bgcolor="#FFFFFF">Style 001, Style 002, ..</td>
                      <td  bgcolor="#FFFFFF"><span class="normalfntMid">
                        <select name="select2" id="select2" style="width:115px">
                          <option>Cost of Good Sold</option>
                          <option>Stationery</option>
                          <option>Advertising</option>
                          <option>Office Equipment</option>
                          <option>Raw Materials</option>
                          <option>Other Materials</option>
                        </select>
                      </span></td>
                      <td align="center"  bgcolor="#FFFFFF"><input type="text" name="textfield2" id="textfield2" style="width:200px" /></td>
                      <td align="right"  bgcolor="#FFFFFF">&nbsp;</td>
                      <td align="right"  bgcolor="#FFFFFF">112000.00</td>
                      <td  bgcolor="#FFFFFF"><span class="normalfntMid">
                        <select name="select9" id="select9" style="width:100px">
                          <option>&nbsp;</option>
                          <option>R &amp; D</option>
                          <option>Projects</option>
                          <option>Others</option>
                        </select>
                      </span></td>
                    </tr>
                    <tr class="normalfnt">
                      <td bgcolor="#FFFFD9" class="normalfntMid">&nbsp;</td>
                      <td bgcolor="#FFFFD9">&nbsp;</td>
                      <td align="center"  bgcolor="#FFFFD9">&nbsp;</td>
                      <td align="center"  bgcolor="#FFFFD9">&nbsp;</td>
                      <td align="center"  bgcolor="#FFFFD9">&nbsp;</td>
                      <td align="center"  bgcolor="#FFFFD9">&nbsp;</td>
                      <td align="center"  bgcolor="#FFFFD9">&nbsp;</td>
                      <td  bgcolor="#FFFFD9">&nbsp;</td>
                      <td  bgcolor="#FFFFD9">&nbsp;</td>
                      <td align="center"  bgcolor="#FFFFD9">&nbsp;</td>
                      <td align="right"  bgcolor="#00FF00">112000.00</td>
                      <td align="right"  bgcolor="#00FF00">112000.00</td>
                      <td  bgcolor="#FFFFD9">&nbsp;</td>
                    </tr>
                    <tr class="normalfnt">
                      <td bgcolor="#FFEBD7" class="normalfntMid">&nbsp;</td>
                      <td bgcolor="#FFEBD7">&nbsp;</td>
                      <td align="center"  bgcolor="#FFEBD7">&nbsp;</td>
                      <td align="center"  bgcolor="#FFEBD7">&nbsp;</td>
                      <td align="center"  bgcolor="#FFEBD7">&nbsp;</td>
                      <td align="center"  bgcolor="#FFEBD7">&nbsp;</td>
                      <td align="center"  bgcolor="#FFEBD7">&nbsp;</td>
                      <td  bgcolor="#FFEBD7">&nbsp;</td>
                      <td  bgcolor="#FFEBD7">&nbsp;</td>
                      <td align="center"  bgcolor="#FFEBD7">&nbsp;</td>
                      <td  bgcolor="#FFEBD7">&nbsp;</td>
                      <td  bgcolor="#FFEBD7">&nbsp;</td>
                      <td  bgcolor="#FFEBD7">&nbsp;</td>
                    </tr>
                    <tr class="normalfnt">
                      <td bgcolor="#FFFFFF" class="normalfntMid">12-07-2012</td>
                      <td bgcolor="#FFFFFF">Unichela</td>
                      <td align="center"  bgcolor="#FFFFFF"><a href="#">JBN121</a></td>
                      <td align="center"  bgcolor="#FFFFFF"><a href="#">000000132</a></td>
                      <td align="center"  bgcolor="#FFFFFF"><a href="#">CN00005</a></td>
                      <td align="center"  bgcolor="#FFFFFF">USD</td>
                      <td align="center"  bgcolor="#FFFFFF">114.25</td>
                      <td  bgcolor="#FFFFFF">Style 011, Style 022, ..</td>
                      <td  bgcolor="#FFFFFF"><span class="normalfntMid">
                        <select name="select7" id="select7" style="width:115px">
                          <option>Raw Materials</option>
                          <option>Advertising</option>
                          <option>Cost of Good Sold</option>
                          <option>Stationery</option>
                          <option>Office Equipment</option>
                          <option>Other Materials</option>
                        </select>
                      </span></td>
                      <td align="center"  bgcolor="#FFFFFF"><input type="text" name="textfield3" id="textfield3" style="width:200px" /></td>
                      <td align="right"  bgcolor="#FFFFFF">40000.00</td>
                      <td align="right"  bgcolor="#FFFFFF">&nbsp;</td>
                      <td  bgcolor="#FFFFFF"><span class="normalfntMid">
                        <select name="select10" id="select10" style="width:100px">
                          <option>&nbsp;</option>
                          <option>R &amp; D</option>
                          <option>Projects</option>
                          <option>Others</option>
                        </select>
                      </span></td>
                    </tr>
                    <tr class="normalfnt">
                      <td bgcolor="#FFFFFF" class="normalfntMid">12-07-2012</td>
                      <td bgcolor="#FFFFFF">Unichela</td>
                      <td align="center"  bgcolor="#FFFFFF"><a href="#">JBN121</a></td>
                      <td align="center"  bgcolor="#FFFFFF"><a href="#">000000132</a></td>
                      <td align="center"  bgcolor="#FFFFFF"><a href="#">CN00005</a></td>
                      <td align="center"  bgcolor="#FFFFFF">USD</td>
                      <td align="center"  bgcolor="#FFFFFF">114.25</td>
                      <td  bgcolor="#FFFFFF">Style 011, Style 022, ..</td>
                      <td  bgcolor="#FFFFFF"><span class="normalfntMid">
                        <select name="select5" id="select5" style="width:115px">
                          <option>Advertising</option>
                          <option>Raw Materials</option>
                          <option>Cost of Good Sold</option>
                          <option>Stationery</option>
                          <option>Office Equipment</option>
                          <option>Other Materials</option>
                        </select>
                      </span></td>
                      <td align="center"  bgcolor="#FFFFFF"><input type="text" name="textfield4" id="textfield4" style="width:200px" /></td>
                      <td align="right"  bgcolor="#FFFFFF">25000.00</td>
                      <td align="right"  bgcolor="#FFFFFF">&nbsp;</td>
                      <td  bgcolor="#FFFFFF"><span class="normalfntMid">
                        <select name="select11" id="select11" style="width:100px">
                          <option>&nbsp;</option>
                          <option>R &amp; D</option>
                          <option>Projects</option>
                          <option>Others</option>
                        </select>
                      </span></td>
                    </tr>
                    <tr class="normalfnt">
                      <td bgcolor="#FFFFFF" class="normalfntMid">12-07-2012</td>
                      <td bgcolor="#FFFFFF">Unichela</td>
                      <td align="center"  bgcolor="#FFFFFF"><a href="#">JBN121</a></td>
                      <td align="center"  bgcolor="#FFFFFF"><a href="#">000000132</a></td>
                      <td align="center"  bgcolor="#FFFFFF"><a href="#">CN00005</a></td>
                      <td align="center"  bgcolor="#FFFFFF">USD</td>
                      <td align="center"  bgcolor="#FFFFFF">114.25</td>
                      <td  bgcolor="#FFFFFF">Style 011, Style 022, ..</td>
                      <td  bgcolor="#FFFFFF"><span class="normalfntMid">
                        <select name="select4" id="select4" style="width:115px">
                          <option>Cost of Good Sold</option>
                          <option>Stationery</option>
                          <option>Advertising</option>
                          <option>Office Equipment</option>
                          <option>Raw Materials</option>
                          <option>Other Materials</option>
                        </select>
                      </span></td>
                      <td align="center"  bgcolor="#FFFFFF"><input type="text" name="textfield6" id="textfield6" style="width:200px" /></td>
                      <td align="right"  bgcolor="#FFFFFF">&nbsp;</td>
                      <td align="right"  bgcolor="#FFFFFF">65000.00</td>
                      <td  bgcolor="#FFFFFF"><span class="normalfntMid">
                        <select name="select12" id="select12" style="width:100px">
                          <option>&nbsp;</option>
                          <option>R &amp; D</option>
                          <option>Projects</option>
                          <option>Others</option>
                        </select>
                      </span></td>
                    </tr>
                    <tr class="normalfnt">
                      <td bgcolor="#FFFFD9" class="normalfntMid">&nbsp;</td>
                      <td bgcolor="#FFFFD9">&nbsp;</td>
                      <td align="center"  bgcolor="#FFFFD9">&nbsp;</td>
                      <td align="center"  bgcolor="#FFFFD9">&nbsp;</td>
                      <td align="center"  bgcolor="#FFFFD9">&nbsp;</td>
                      <td align="center"  bgcolor="#FFFFD9">&nbsp;</td>
                      <td align="center"  bgcolor="#FFFFD9">&nbsp;</td>
                      <td  bgcolor="#FFFFD9">&nbsp;</td>
                      <td  bgcolor="#FFFFD9">&nbsp;</td>
                      <td align="center"  bgcolor="#FFFFD9">&nbsp;</td>
                      <td align="right"  bgcolor="#00FF00">65000.00</td>
                      <td align="right"  bgcolor="#00FF00">65000.00</td>
                      <td  bgcolor="#FFFFD9">&nbsp;</td>
                    </tr>
                    <tr class="normalfnt">
                      <td bgcolor="#FFEBD7" class="normalfntMid">&nbsp;</td>
                      <td bgcolor="#FFEBD7">&nbsp;</td>
                      <td align="center"  bgcolor="#FFEBD7">&nbsp;</td>
                      <td align="center"  bgcolor="#FFEBD7">&nbsp;</td>
                      <td align="center"  bgcolor="#FFEBD7">&nbsp;</td>
                      <td align="center"  bgcolor="#FFEBD7">&nbsp;</td>
                      <td align="center"  bgcolor="#FFEBD7">&nbsp;</td>
                      <td  bgcolor="#FFEBD7">&nbsp;</td>
                      <td  bgcolor="#FFEBD7">&nbsp;</td>
                      <td align="center"  bgcolor="#FFEBD7">&nbsp;</td>
                      <td  bgcolor="#FFEBD7">&nbsp;</td>
                      <td  bgcolor="#FFEBD7">&nbsp;</td>
                      <td  bgcolor="#FFEBD7">&nbsp;</td>
                    </tr>
                    </table>
                </div>
              </td>
            </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan="2">
          <table width="100%">
            <tr>
              <td width="100%" height="34" class="tableBorder_allRound"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                <tr>
                  <td width="100%" align="center" bgcolor=""><a href="../../../../main.php"><img border="0" src="../../../../images/Tsave.jpg" alt="Save" name="Save"width="92" height="24" onclick="butCommand1(this.name);" class="mouseover" id="butSave" tabindex="24"/><img src="../../../../images/Tclose.jpg" alt="Close" name="Close" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                </tr>
              </table></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
</div>
</div>    
</body>
</html>
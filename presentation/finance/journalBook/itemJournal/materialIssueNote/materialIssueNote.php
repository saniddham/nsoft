<?php
session_start();
$backwardseperator = "../../../../../";
$mainPath = $_SESSION['mainPath'];
$companyId = $_SESSION['headCompanyId'];
$location = $_SESSION['CompanyID'];
$thisFilePath =  $_SERVER['PHP_SELF'];
include  "{$backwardseperator}dataAccess/permisionCheck.inc";
// ======================Check Exchange Rate Updates========================

$currentDate = date("Y-m-d");
$sql = "SELECT COUNT(*) AS 'no'
		FROM
		mst_financeexchangerate
		WHERE
		mst_financeexchangerate.dtmDate =  '$currentDate'
		";
$result = $db->RunQuery($sql);
$row = mysqli_fetch_array($result);
if ($row['no'] == 0) {
    $str = "Please Update Exchange Rates Before " . $status . " Item Issue Notes .";
    $str .= $row['NameList'];
    $maskClass = "maskShow";
} else {
    $maskClass = "maskHide";
}
// =========================================================================
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Item Jouranl - MIN</title>
    <link href="../../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
    <link href="../../../../../css/promt.css" rel="stylesheet" type="text/css" />
    <link href="../../../../../css/tblstyle.css" rel="stylesheet" type="text/css" />

    <script type="application/javascript" src="../../../../../libraries/jquery/jquery.js"></script>
    <script type="application/javascript" src="../../../../../libraries/jquery/jquery-ui.js"></script>
    <script src="materialIssueNote.js" type="text/javascript"></script>
    <script type="application/javascript" src="../../../../../libraries/javascript/script.js"></script>

    <link rel="stylesheet" type="text/css" href="../../../../../libraries/calendar/theme.css" />
    <script src="../../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
    <script src="../../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
    <script src="../../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

    <link rel="stylesheet" href="../../../../../libraries/validate/validationEngine.css" type="text/css">
    <link rel="stylesheet" href="../../../../../libraries/validate/template.css" type="text/css">  

</head>
<body>
    <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
        <tr>
            <td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator . 'Header.php'; ?></td>
        </tr> 
    </table>
    <div id="divMask" class="<?php echo $maskClass ?> mask"> <?php echo $str; ?></div>
    <script src="../../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
    <script src="../../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
    <script src="../../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
    <script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.js"></script>
    <script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.min.js"></script>
    
    <form id="frmMin" name="frmMin" method="post" action="purchaseDayBook-db-set.php" autocomplete="off">
        <div align="center">
            <div class="trans_layoutL">
                <div class="trans_text">Item Journal - Material Issue Note</div>
                <table width="100%">
                    <tr>
                        <td width="50%" colspan="2">
                            <table width="100%" class="tableBorder_allRound">
                                <tr>
                                    <td width="224" bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
                                    <td width="82" bgcolor="#FFFFFF" class="normalfnt">From</td>
                                    <td width="150" bgcolor="#FFFFFF"><input name="adviceDate2" type="text" value="<?php echo date("Y-m-d"); ?>" class="txtbox" id="startDate" style="width:98px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                                    <td width="67" bgcolor="#FFFFFF"><span class="normalfnt">To</span></td>
                                    <td width="137" bgcolor="#FFFFFF" class=""><input name="adviceDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="txtbox" id="endDate" style="width:98px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                                    <td width="220" bgcolor="#FFFFFF"><span class="normalfnt"><img src="../../../../../images/go.png" width="30" height="22" name="btnGo" id="btnGo" /></span></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                        </td>
                    </tr>
                    <td width="50%"></tr>
                        <tr>
                            <td colspan="2"><table width="93%">
                                    <tr>
                                        <td>
                                            <div style="overflow:scroll;width:900px;height:350px;" id="divGrid">
                                                <table width="100%" id="tblMainGrid2" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
                                                    <tr class="">
                                                        <td width="3%"  height="23" bgcolor="#FAD163" class="normalfntMid"></td>
                                                        <td width="8%"  height="23" bgcolor="#FAD163" class="normalfntMid"><strong>Date</strong></td>
                                                        <td width="14%" bgcolor="#FAD163" class="normalfntMid"  ><strong>Refrence Number</strong></td>
                                                        <td width="23%" align="center" bgcolor="#FAD163" class="normalfntMid"  ><strong>Memo</strong></td>                                                                                                               
                                                        <td width="11%" bgcolor="#FAD163" class="normalfntMid"  ><strong>Dimension</strong></td>
                                                    </tr>  
                                                    <tbody id="mins" class="normalfnt" bgcolor="#FFFFFF"><!-- load Mins -->
                                                    </tbody>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table width="100%">
                                    <tr>
                                        <td width="100%" height="34" class="tableBorder_allRound"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                                                <tr>
                                                    <td width="100%" align="center" bgcolor=""><img border="0" src="../../../../../images/Tsave.jpg" alt="Save" name="Save"width="92" height="24" class="mouseover" id="butSave" tabindex="24"/><img src="../../../../../images/Tclose.jpg" alt="Close" name="Close" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></td>
                                                </tr>
                                            </table></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                </table>
            </div>
        </div>
    </form>
</body>
</html>
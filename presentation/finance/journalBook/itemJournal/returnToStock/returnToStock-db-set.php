<?php
session_start();
$backwardseperator = "../../../../../";
$mainPath = $_SESSION['mainPath'];
$userId = $_SESSION['userId'];
$companyId = $_SESSION['headCompanyId'];
$locationId = $_SESSION['CompanyID'];
$thisFilePath = $_SERVER['PHP_SELF'];
include "{$backwardseperator}dataAccess/Connector.php";
$response = array('type' => '', 'msg' => '');

$sql = "SELECT
            mst_companies.intBaseCurrencyId
            FROM
            mst_companies
            WHERE
            mst_companies.intId = $companyId";
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);        
$baseCurrencyId =($row['intBaseCurrencyId']);

try {
    $requestType = $_REQUEST['requestType'];
    if ($requestType == 'add') {
        $details=$_REQUEST['details'];
        $accountPeriod = getLatestAccPeriod($companyId);
        
        $db->begin();
        //iterate MINs
        foreach ($details as $min){
            
            $currentDate=date('Y-m-d H:i:s');
            $minYear=$min['retuYear'];
            $minNo=$min['retuNo'];
            $fullMin="STRN-".$minYear."-".$minNo;
            $dimentionId=$min['dim'];
             $remarks="automated ".$fullMin;
            //Add data to transaction header*******************************************
           $sql="INSERT INTO fin_transactions (entryDate, strProgramType, documentNo, currencyId, currencyRate, transDetails, payMethodId, paymentNumber, accPeriod, userId, companyId, createdOn) VALUES
                ('$currentDate','STRN','$fullMin',$baseCurrencyId,1,'$remarks',null,null,$accountPeriod,$userId,$companyId,now())";

            $db->RunQuery2($sql);
            $entryId=$db->insertId;
            ////
            //get min items
            $sql="SELECT
                    ware_stocktransactions_bulk.intItemId,
                    ware_stocktransactions_bulk.dblGRNRate,
                    ware_stocktransactions_bulk.dblQty,
                    ware_stocktransactions_bulk.intCurrencyId,
                    ware_stocktransactions_bulk.dtGRNDate,
                    ware_grnheader.intGrnYear,
                    trn_poheader.intShipmentTerm
                FROM
                    ware_stocktransactions_bulk
                    INNER JOIN ware_returntostoresheader ON ware_returntostoresheader.intReturnNo = ware_stocktransactions_bulk.intDocumentNo AND ware_returntostoresheader.intReturnYear = ware_stocktransactions_bulk.intDocumntYear
                    INNER JOIN ware_grnheader ON ware_stocktransactions_bulk.intGRNNo = ware_grnheader.intGrnNo AND ware_stocktransactions_bulk.intGRNYear = ware_grnheader.intGrnYear
                    INNER JOIN trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
                WHERE
                    ware_stocktransactions_bulk.strType = 'RETSTORES' AND
                    ware_stocktransactions_bulk.intCompanyId = $companyId AND
                    ware_returntostoresheader.intReturnNo = $minNo AND
                    ware_returntostoresheader.intReturnYear = $minYear ";
            $result = $db->RunQuery2($sql);
            
            while($row = mysqli_fetch_array($result)){//iterate Items
                $item=$row['intItemId'];
                $unitPrice=$row['dblGRNRate'];
                $qty=$row['dblQty'];
                $currencyId=$row['intCurrencyId'];
                $grnDate=$row['dtGRNDate'];
                $potype=$row['intShipmentTerm'];
                
                //get Currency
                //get Currency details
                $resultCurr = $db->RunQuery2("SELECT mst_financeexchangerate.dblBuying FROM mst_financeexchangerate WHERE mst_financeexchangerate.intCurrencyId =  '$currencyId' AND mst_financeexchangerate.dtmDate =  '$grnDate' AND mst_financeexchangerate.intCompanyId =  '$companyId'");
                $rowCurr = mysqli_fetch_array($resultCurr);
                $currencyRate = $rowCurr['dblBuying'];
                
                $itemAmount=($unitPrice * $qty *   $currencyRate);
                
                //get Item details
                $sql="SELECT 
                        mst_financeitemactivate.itemClassId,mst_financeitemactivate.stockAccountId, mst_financeitemactivate.localAccId, mst_financeitemactivate.impAccId 
                    FROM
                        mst_financeitemactivate 
                        Inner Join mst_item ON mst_financeitemactivate.subCategoryId = mst_item.intSubCategory 
                    WHERE
                        mst_item.intId =  '$item' AND mst_financeitemactivate.intCompanyId =  '$companyId'";
                $result = $db->RunQuery2($sql);
                $row = mysqli_fetch_array($result);
                $itemManinClass=$row['itemClassId'];
                $trnsDetails=$fullMin."-".$item;
                if($itemManinClass==1){//Inventy item class(A*)
                    $itemAccStock = $row['stockAccountId'];
                    if($potype==2){//import Item
                        $itemAccOther=$row['impAccId'];
                    }
                    else{//localItem
                        $itemAccOther=$row['localAccId'];                        
                    }
                    //>>>>>>>>>>>>>>>>>>>>>>>>>>>transaction table process item - Credit ot Stock Account>>>>>>>>>>>>>>>>>>>>>>>>>
                    $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                                        ($entryId,'D',$itemAccStock,$itemAmount,'$trnsDetails',$dimentionId)";
                    $trnResult = $db->RunQuery2($sql);
                    //>>>>>>>>>>>>>>>>>>>>>>>>>>>transaction table process item - Debit ot Local or Imp Account>>>>>>>>>>>>>>>>>>>>>>>>>
                    
                    $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                                        ($entryId,'C',$itemAccOther,$itemAmount,'$trnsDetails',$dimentionId)";
                    $trnResult = $db->RunQuery2($sql);
                    $trnResult2=true;
                }//(A*) 
                else{
                    $trnResult2=false;
                }
            }//iterate Items
            //update MIN as accounted
            $sql="UPDATE ware_returntostoresheader H,mst_locations as L set H.intAccounted=1 
                WHERE H.intReturnNo =  '$minNo' AND H.intReturnYear =  '$minYear' AND H.intCompanyId=L.intId AND L.intCompanyId =  '$companyId'";
            $trnResult = $db->RunQuery2($sql);
        }//iterate MINs
        
        
        
        //*******************************************************
        if ($trnResult & $trnResult2) {
            $db->commit();
            $response['type'] = 'pass';
            $response['msg'] = 'Saved successfully.';
        } else {
            $db->rollback();//roalback
            $response['type'] = 'fail';
            $response['msg'] = $db->errormsg;
            $response['q'] = $sql;
        }
    }
    echo json_encode($response);
    
}catch (Exception $e){
    $db->rollback();//roalback
    $response['type'] = 'fail';
    $response['msg'] = $e->getMessage();
    $response['q'] = $sql;
    echo json_encode($response);
}

//--------------------------------------------------------------------------------------------
function getLatestAccPeriod($companyId) {
    global $db;
    $sql = "SELECT
				MAX(mst_financeaccountingperiod.intId) AS accId,
				mst_financeaccountingperiod.dtmStartingDate,
				mst_financeaccountingperiod.dtmClosingDate,
				mst_financeaccountingperiod.intStatus,
				mst_financeaccountingperiod_companies.intCompanyId,
				mst_financeaccountingperiod_companies.intPeriodId
				FROM
				mst_financeaccountingperiod
				Inner Join mst_financeaccountingperiod_companies ON mst_financeaccountingperiod_companies.intPeriodId = mst_financeaccountingperiod.intId
				WHERE
				mst_financeaccountingperiod_companies.intCompanyId =  '$companyId' AND mst_financeaccountingperiod.intStatus = '1'
				ORDER BY
				mst_financeaccountingperiod.intId DESC
				";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $latestAccPeriodId = $row['accId'];
    return $latestAccPeriodId;
}
?>
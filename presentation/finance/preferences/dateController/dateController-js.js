// JavaScript Document
$(document).ready(function() {
  //permision for add 
  if(intAddx)
  {
 	$('#frmDateController #butNew').show();
	$('#frmDateController #butSave').show();
  } 
  //permision for edit 
  if(intEditx)
  {
  	$('#frmDateController #butSave').show();
  } 
  //permision for view
  if(intViewx)
  {
  	//
  }
  $('#frmDateController .status').click(function(){
	  if($('#frmDateController #rdFixedDate').attr('checked'))
	  {
	  	$('#frmDateController .backDateValue').removeAttr('disabled');
		$('#frmDateController .noOfDate').attr('disabled', 'disabled');
		$('#frmDateController .noOfDate').removeClass('validate[required]');
		$('#frmDateController .noOfDate').validationEngine('hide');
		//$('#frmDateController .noOfDate').val('');
		$('#frmDateController .backDateValue').addClass('validate[required]');
		backStatus = 1;
	  }
	  else if($('#frmDateController #rdNoOfDate').attr('checked'))
	  {
		$('#frmDateController .noOfDate').removeAttr('disabled');
		$('#frmDateController .backDateValue').attr('disabled', 'disabled');
		$('#frmDateController .noOfDate').addClass('validate[required]');
		//$('#frmDateController .backDateValue').val('');
		$('#frmDateController .backDateValue').removeClass('validate[required]');
		backStatus = 0;
	  }
  });
  
  $('#frmDateController #chkAllBDate').click(function(){
        var chkAll=document.getElementById("chkAllBDate");
        var status=false;
        if(chkAll.checked){
            status=true;
        }
        else{
            status=false;
        }
        $('#frmDateController .backDate').attr("checked",status);		
    });
	
	 $('#frmDateController #chkAllFDate').click(function(){
        var chkAll=document.getElementById("chkAllFDate");
        var status=false;
        if(chkAll.checked){
            status=true;
        }
        else{
            status=false;
        }
        $('#frmDateController .futureDate').attr("checked",status);		
    });
	
	//save button click event
$('#frmDateController #butSave').click(function(){

		value="[ ";
	$('#tblMainGrid .mainRow').each(function(){
		
		trnType		= $(this).find(".trnType").html();
		isBack		= $(this).find(".backDate").attr('checked');
		bakDate		= $(this).find(".backDateValue").val();
		noOfDate	= $(this).find(".noOfDate").val();
		isFuture	= $(this).find(".futureDate").attr('checked');	
		
	value += '{ "trnType":"'+trnType+'", "isBack": "'+isBack+'", "bakDate": "'+bakDate+'", "noOfDate": "'+noOfDate+'", "isFuture": "'+isFuture+'"},';
	});
	
	value = value.substr(0,value.length-1);
	value += " ]";
	var requestType = '';
	if ($('#frmDateController').validationEngine('validate'))   
    {
		requestType = 'add';
	 
		var url = "dateController-db-set.php";
		var obj = $.ajax({
		url:url,
		dataType: "json",
		type:'post', 
		data:$("#frmDateController").serialize()+'&requestType='+requestType+'&trnDetails='+value+'&backStatus='+backStatus,
		async:false,
		
		success:function(json){
				$('#frmDateController #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
				if(json.type=='pass')
				{
					var t=setTimeout("alertx()",1000);
					//loadCombo_frmSalesInvoice();
					return;
				}
				var t=setTimeout("alertx()",3000);
			},
		error:function(xhr,status){
				
				$('#frmDateController #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
				var t=setTimeout("alertx()",3000);
			}		
		});
	}
});
  
});
function alertx()
{
	$('#frmDateController #butSave').validationEngine('hide')	;
}
<?php
	$backwardseperator = "../../../../";
	$mainPath = $_SESSION['mainPath'];

	$thisFilePath =  $_SERVER['PHP_SELF'];
	include  "{$backwardseperator}dataAccess/permisionCheck.inc";

	$sql = "SELECT
		fn_transaction_type.intId,
		fn_transaction_type.strTrnType,
		fin_preferences_datecontroller.strTransactionType,
		fin_preferences_datecontroller.intIsBackDate,
		fin_preferences_datecontroller.dtmBackDate,
		fin_preferences_datecontroller.intIsFutureDate,
		fin_preferences_datecontroller.intBackStatus,
		fin_preferences_datecontroller.intNoOfDate
		FROM
		fn_transaction_type
		left outer Join fin_preferences_datecontroller ON fn_transaction_type.strTrnType = fin_preferences_datecontroller.strTransactionType
		ORDER BY
		fn_transaction_type.strTrnType ASC
		";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
	  $bakStatus = $row['intBackStatus'];
	}
?>
<script type="application/javascript" >
var backStatus = '<?php echo $bakStatus ?>';
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Date Controller</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />

<script type="application/javascript" src="../../../../libraries/jquery/jquery-1.3.2.min.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/scrolltable.js"></script>

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="dateController-js.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

<link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css">

</head>

<body>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<form id="frmDateController" name="frmDateController" method="post" action="dateController-db-set.php" autocomplete="off">
<div align="center">
  <div class="trans_layoutD">
    <div class="trans_text">Date Controller</div>
	 <table class="tableBorder_allRound" id="tblMainGrid" width="100%"> 
          <thead>
          <tr class="gridHeader">
          <th width="2%">&nbsp</th>
           <th width="40%"><strong>Transaction Type</strong></th>
           <th colspan="2"><strong>Back Date </strong></th>
           <th width="17%"><strong>Future Date</strong></th>
          </tr>
          <tr class="normalfnt" style="background-color:#CCC">
          <th width="2%">&nbsp</th>
            <td>&nbsp;</td>
            <td align="center" bgcolor="#CCC" class="accActSelect"><div align="left">
              <input type="checkbox" name="chkAllBDate" id="chkAllBDate" checked="checked"/>
            </div></td>
            <td align="center" bgcolor="#CCC" class="accActSelect">
            <input type="radio" name="radio" id="rdFixedDate" value="Fixed" class="validate[required] status" <?php echo($bakStatus==1?'checked':'');?> />Fixed Date <input type="radio" name="radio" id="rdNoOfDate" value="NoOfDate" class="validate[required] status" <?php echo($bakStatus==0?'checked':'');?> />No. of Dates</td>
            <td class="accActSelect" align="center" bgcolor="#CCC">
            <input type="checkbox" name="chkAllFDate" id="chkAllFDate" checked="checked"/></td>
          </tr>
          </thead>
        <tbody>
          <?php
         $sql = "SELECT
				fn_transaction_type.intId,
				fn_transaction_type.strTrnType,
				fin_preferences_datecontroller.strTransactionType,
				fin_preferences_datecontroller.intIsBackDate,
				fin_preferences_datecontroller.dtmBackDate,
				fin_preferences_datecontroller.intIsFutureDate,
				fin_preferences_datecontroller.intBackStatus,
				fin_preferences_datecontroller.intNoOfDate
				FROM
				fn_transaction_type
				left outer Join fin_preferences_datecontroller ON fn_transaction_type.strTrnType = fin_preferences_datecontroller.strTransactionType
				ORDER BY
				fn_transaction_type.strTrnType ASC

				";
         $result = $db->RunQuery($sql);
         while($row=mysqli_fetch_array($result))
         {
			 $date = $row['dtmBackDate'];
         ?>
          <tr class="normalfnt mainRow">
           <td bgcolor="#CCCCCC"><?php echo $row['intId'];?></td>
          	<td bgcolor="#EAF8FB" class="trnType"><?php echo $row['strTrnType'];?></td>
          	<td width="5%" align="center" bgcolor="#FF9966" class="accActSelect"><div align="left">
          	  <input type="checkbox" name=chkBDate<?php echo $row['strTrnType'];?> id=chkBDate<?php echo $row['strTrnType'];?> <?php echo($row['intIsBackDate']?'checked':''); ?> class="backDate"/>
       	    </div></td>
            <td width="30%" align="center" bgcolor="#FF9966" class="accActSelect"><input name="txtDate<?php echo $row['strTrnType'];?>" type="text" value="<?Php echo ($date==''?date("Y-m-d"):$date) ?>" class="validate[required] backDateValue" id="txtDate<?php echo $row['strTrnType'];?>" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" disabled="disabled"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" />
            <input type="text" name="txt<?php echo $row['strTrnType'];?>" id="txt<?php echo $row['strTrnType'];?>" style="width:40px;text-align:center" disabled="disabled" class="noOfDate" value="<?php echo $row['intNoOfDate'];?>" /></td>
             <td class="accActSelect" align="center" bgcolor="#FFCC66"><input type="checkbox" name="chkFDate<?php echo $row['strTrnType'];?>3" id="chkFDate<?php echo $row['strTrnType'];?>" <?php echo($row['intIsFutureDate']?'checked':''); ?> class="futureDate"/></td>
          </tr>
          <?php 
            } 
          ?>
          </tbody>
      </table>
	 <table width="100%">
          <tr>
            <td width="100%" height="34" class="tableBorder_allRound"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
<td width="100%" align="center" bgcolor=""><img style="display:none" border="0" src="../../../../images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:none" border="0" src="../../../../images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/><img style="display:none" border="0" src="../../../../images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="25"/><a href="../../../../main.php"><img  src="../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                </tr>
              </table>
       </td>
       </tr>
    </table>
</div>
</div> 
</form> 
</body>
</html>
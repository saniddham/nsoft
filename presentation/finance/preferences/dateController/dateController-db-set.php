<?php 
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	include "{$backwardseperator}dataAccess/Connector.php";

	$response = array('type'=>'', 'msg'=>'');
        
	$companyId = $_SESSION['headCompanyId'];
	$locationId = $_SESSION["CompanyID"];
	
	$requestType 	= $_REQUEST['requestType'];
	$backStatus 	= $_REQUEST['backStatus'];

	$details = json_decode($_REQUEST['trnDetails'], true);

	if($requestType=='add')
	{
	try
		{            
		$db->begin();                
        
		if(count($details) != 0)
		{
			foreach($details as $detail)
			{
				
				$trnType	= trim(($detail['trnType']==''?'NULL':"'".$detail['trnType']."'"));
				$isBack		= trim($detail['isBack']=='true'?1:0);
				$bakDate	= trim(($detail['bakDate']==''?'NULL':"'".$detail['bakDate']."'"));
				$noOfDate	= trim(($detail['noOfDate']==''?'NULL':"'".$detail['noOfDate']."'"));
				$isFuture	= trim($detail['isFuture']=='true'?1:0);
				
				$sql = "SELECT
						COUNT(intId) AS 'no'
						FROM
						fin_preferences_datecontroller
						WHERE
						fin_preferences_datecontroller.strTransactionType =  $trnType";
				$result = $db->RunQuery2($sql);
				$row= mysqli_fetch_array($result);
				if($row['no']==0)
				{
						$trnSql = "INSERT INTO `fin_preferences_datecontroller` 		(`strTransactionType`,`intBackStatus`,`intIsBackDate`,`dtmBackDate`,`intNoOfDate`,`intIsFutureDate`,`intCreator`,`dtmCreateDate`) 
	VALUES ($trnType,'$backStatus','$isBack',$bakDate,$noOfDate,'$isFuture','$userId',now())";
				
						$trnResult = $db->RunQuery2($trnSql);
				}
				else
				{
					$trnSql = "UPDATE `fin_preferences_datecontroller` SET 	strTransactionType	= $trnType,
																			intBackStatus		= '$backStatus',
																			intIsBackDate		= '$isBack',
																			dtmBackDate			= $bakDate,
																			intNoOfDate			= $noOfDate,
																			intIsFutureDate		= '$isFuture',
																			intModifyer			= '$userId'
                    WHERE (`strTransactionType`=$trnType)";
                    $trnResult = $db->RunQuery2($trnSql);
				}
			}
		}
		if($trnResult)
		{                    
            $response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
            $db->commit();
		}
		else
		{                    
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
            $db->rollback();//roalback
		}

		echo json_encode($response);            
        }
		catch(Exception $e)
		{		   
			$db->rollback();//roalback
			$response['type'] 		= 'fail';
			$response['msg'] 		= $e->getMessage();
			$response['q'] 			= $sql;                
		    echo json_encode($response);                 
		}        
	}
?>
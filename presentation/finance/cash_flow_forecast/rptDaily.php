<?php
session_start();
$thisFilePath 		= $_SERVER['PHP_SELF'];
ini_set('max_execution_time',300);

//BEGIN - SET PARAMETERS	{
$companyId 			= 1;
//END 	- SET PARAMETERS	}

//BEGIN - INCLUDE FILES {
include_once $_SESSION['ROOT_PATH']."dataAccess/DBManager2.php";
include_once $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
include_once $_SESSION['ROOT_PATH']."libraries/mail/mail_bcc.php";
//END 	- INCLUDE FILES }

$db 				= new DBManager2(1);
$obj_common			= new cls_commonFunctions_get($db);

$db->connect();//open connection.

$target_currencyId	= 1;

//BEGIN - 
$fromDate		= '2013-01-01';
$toDate			= '2013-12-31';

$startDate 		= strtotime($fromDate);
$endDate   		= strtotime($toDate);

$currentDate 	= $endDate;
$i		= 0;
$array	= array();
while ($currentDate >= $startDate) {
    $array[$i][0]	= date('Y-m-d',$currentDate);
	$array[$i][1]	= date('y-M-d',$currentDate);
    $currentDate 	= strtotime(date('Y/m/d',$currentDate).' -1 day');
	$i++;
}
krsort($array);		
//END	-
ob_start();
?>
<style>
.break {
	page-break-before: always;
}
 @media print {
.noPrint {
	display: none;
}
}
#apDiv1 {
	position: absolute;
	left: 272px;
	top: 511px;
	width: 650px;
	height: 322px;
	z-index: 1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
table .bordered {
 *border-collapse: collapse; /* IE7 and lower */
	border-spacing: 0;
	width: 100%;
}
.bordered {
	border: solid #ccc 1px;
	/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
	/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size: 11px;
	font-family: Verdana;
}
.bordered tr:hover {
	background: #fbf8e9;
	/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
	transition: all 0.1s ease-in-out;
}
.bordered td {
	border-left: 1px solid #ccc;
	border-top: 1px solid #ccc;
	padding: 2px;
}
.bordered th {
	border-left: 1px solid #ccc;
	border-top: 1px solid #ccc;
	padding: 4px;
	text-align: center;
}
.bordered th {
	background-color: #dce9f9;
	background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
	background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
	background-image: -moz-linear-gradient(top, #ebf3fc, #dce9f9);
	background-image: -ms-linear-gradient(top, #ebf3fc, #dce9f9);
	background-image: -o-linear-gradient(top, #ebf3fc, #dce9f9);
	background-image: linear-gradient(top, #ebf3fc, #dce9f9);
	/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
	box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;
	border-top: none;
	text-shadow: 0 1px 0 rgba(255,255,255,.5);
}
.bordered td:first-child, .bordered th:first-child {
	border-left: none;
}
.bordered th:first-child {
	/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
	border-radius: 6px 0 0 0;
}
.bordered th:last-child {
	/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
	border-radius: 0 6px 0 0;
}
.bordered th:only-child {
	/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
	border-radius: 6px 6px 0 0;
}
.bordered tr:last-child td:first-child {
	/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
	border-radius: 0 0 0 6px;
}
.bordered tr:last-child td:last-child {
	/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
	border-radius: 0 0 6px 0;
}
.bordered {
	border: solid #ccc 1px;
	/*	-moz-border-radius: 6px;
	-webkit-border-radius: 6px;*/
	border-radius: 6px;
	/*	-webkit-box-shadow: 0 1px 1px #ccc; 
	-moz-box-shadow: 0 1px 1px #ccc; */
	box-shadow: 0 1px 1px #ccc;
	font-size: 11px;
	font-family: Verdana;
}
.bordered tr:hover {
	background: #fbf8e9;
	/*    -o-transition: all 0.1s ease-in-out;
    -webkit-transition: all 0.1s ease-in-out;
    -moz-transition: all 0.1s ease-in-out;
    -ms-transition: all 0.1s ease-in-out;*/
	transition: all 0.1s ease-in-out;
}
.bordered td {
	border-left: 1px solid #ccc;
	border-top: 1px solid #ccc;
	padding: 2px;
}
.bordered th {
	border-left: 1px solid #ccc;
	border-top: 1px solid #ccc;
	padding: 4px;
	text-align: center;
}
.bordered th {
	background-color: #dce9f9;
	background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
	background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
	background-image: -moz-linear-gradient(top, #ebf3fc, #dce9f9);
	background-image: -ms-linear-gradient(top, #ebf3fc, #dce9f9);
	background-image: -o-linear-gradient(top, #ebf3fc, #dce9f9);
	background-image: linear-gradient(top, #ebf3fc, #dce9f9);
	/*    -webkit-box-shadow: 0 1px 0 rgba(255,255,255,.8) inset; 
    -moz-box-shadow:0 1px 0 rgba(255,255,255,.8) inset; */ 
	box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;
	border-top: none;
	text-shadow: 0 1px 0 rgba(255,255,255,.5);
}
.bordered td:first-child, .bordered th:first-child {
	border-left: none;
}
.bordered th:first-child {
	/*    -moz-border-radius: 6px 0 0 0;
    -webkit-border-radius: 6px 0 0 0;*/
	border-radius: 6px 0 0 0;
}
.bordered th:last-child {
	/*    -moz-border-radius: 0 6px 0 0;
    -webkit-border-radius: 0 6px 0 0;*/
	border-radius: 0 6px 0 0;
}
.bordered th:only-child {
	/*    -moz-border-radius: 6px 6px 0 0;
    -webkit-border-radius: 6px 6px 0 0;*/
	border-radius: 6px 6px 0 0;
}
.bordered tr:last-child td:first-child {
	/*    -moz-border-radius: 0 0 0 6px;
    -webkit-border-radius: 0 0 0 6px;*/
	border-radius: 0 0 0 6px;
}
.bordered tr:last-child td:last-child {
	/*    -moz-border-radius: 0 0 6px 0;
    -webkit-border-radius: 0 0 6px 0;*/
	border-radius: 0 0 6px 0;
}
.odd {
	background-color: #F5F5F5;
}
.even {
	background-color: #FFFFFF;
}
.mouseover {
	cursor: pointer;
}
.txtNumber {
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align: right;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}
.txtText {
	font-family: Verdana;
	font-size: 11px;
	color: #20407B;
	text-align: left;
	border-top: 1px solid #B4B4B4;
	border-left: 1px solid #B4B4B4;
	border-right: 1px solid #B4B4B4;
	border-bottom: 1px solid #B4B4B4;
}
.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: left;
}
.normalfntsm {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: left;
}
.normalfntBlue {
	font-family: Verdana;
	font-size: 11px;
	color: #0B3960;
	margin: 0px;
	font-weight: normal;
	text-align: left;
}
.normalfntGrey {
	font-family: Verdana;
	font-size: 11px;
	color: #999;
	margin: 0px;
	font-weight: normal;
	text-align: left;
}
.normalfntMid {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: center;
}
.normalfntRight {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: right;
}
.reportHeader {
	font-size: 18px;
	/*text-align:center,*/
	font-style: normal;
	font-weight: bold;
	font-family: "Times New Roman", Times, serif;
}
</style>
<style type="text/css">
#progress {
 width: 100px;   
 border: 1px solid black;
 position: relative;
 padding: 1px;
}

#percent {
 position: absolute;   
 left: 50%;
}

#bar {
 height: 15px;
 background-color: #0C0;
}
</style>

<table width="1250" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td colspan="3"></td>
  </tr>
  <tr>
    <td colspan="9" align="center" ><table width="100%" cellpadding="0" cellspacing="0">
        <tr>
          <td class="tophead"><table width="100%" cellpadding="0" cellspacing="0">
              <tr>
                <td width="8%" class="normalfnt"><img style="vertical-align:" src="http://accsee.com/screenline.jpg"  /></td>
                <td align="center" valign="top" width="75%" class="topheadBLACK"><?php
	$result 			= $db->RunQuery($SQL);
	$result				= $obj_common->loadLoctionComanyDetails(2,'RunQuery');
	$row 				= mysqli_fetch_array($result);
	$companyName		= $row["strName"];
	$locationName		= $row["locationName"];
	$companyAddress1	= $row["strAddress"];
	$companyStreet		= $row["strStreet"];
	$companyCity		= $row["strCity"];
	$companyCountry		= $row["strCountryName"];
	$companyZipCode		= $row["strZip"];
	$companyPhone		= "(".$companyZipCode.")".$row["strPhoneNo"];
	$companyFax		 	= "(".$companyZipCode.")".$row["strFaxNo"];
	$companyEmail		= $row["strEMail"];
	$companyWeb		 	= $row["strWebSite"];	
	?>
                  <b><?php echo $companyName.""; ?></b>
                  <p class="normalfntMid">
                    <?php echo $companyAddress1." ".$companyStreet." ".$companyCity." ".$companyCountry."."."<br><b>Tel : </b>".$companyPhone." <b>Fax : </b>".$companyFax." <br><b>E-Mail : </b>".$companyEmail." <b>Web : </b>".$companyWeb;?></p></td>
                <td width="9%" class="tophead"><img src="http://accsee.com/nsoft_logo.png" style="vertical-align:"/></td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td align="center" valign="top" class="reportHeader">Daily Cash Flow Forecast [USD]</td>
                <td class="tophead">&nbsp;</td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td colspan="3"></td>
  </tr>
</table>
<table width="1100" border="0" align="center" bgcolor="#FFFFFF">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table  border="0" cellpadding="0" cellspacing="0" class="bordered" width="100%">
        <thead>
          <tr valign="bottom">
            <th width="21%">&nbsp;</th>
            <?php 
		foreach ($array as $key => $val)
		{
		?>
            <th nowrap="nowrap"><?php echo $val[1];?></th>
            <?php 
		}
		?>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td nowrap="nowrap" title="Customer Name" ><u><b>Add - Receipts</b></u></td>
            <?php 
		foreach ($array as $key => $val)
		{
		?>
            <td nowrap="nowrap" title="<?php echo $val[0];?>" bgcolor="<?php echo $val[4];?>">&nbsp;</td>
            <?php 
		}
		?>
          </tr>
          <tr>
            <td nowrap="nowrap" title="Customer Name" id="CUSTOMER">&nbsp;&nbsp;&nbsp;&nbsp;Monthly collection from customers</td>
            <?php 
		foreach ($array as $key => $val)
		{
			$customerAmount			 = round(GetCustomerAmount($val[0]),2);
			$array_total[$val[1]][0] = $customerAmount;
		?>
            <td nowrap="nowrap" title="<?php echo "Monthly collection from customers -> ".$val[0];?>" style="text-align:right" bgcolor="<?php echo $val[4];?>" class="cls_td_amount" id="<?php echo $val[1]?>"><?php echo $customerAmount==0?'-':number_format($customerAmount)?></td>
            <?php 
		}
		?>
          </tr>
          <tr>
            <td nowrap="nowrap"><u><b>Deduct - Payment</b></u></td>
            <?php 
		foreach ($array as $key => $val)
		{
		?>
            <td nowrap="nowrap" title="<?php echo $val[0];?>" bgcolor="<?php echo $val[4];?>">&nbsp;</td>
            <?php 
		}
		?>
          </tr>
          <tr>
            <td nowrap="nowrap" id="SUPPLIER">&nbsp;&nbsp;&nbsp;&nbsp;Monthly payment to supplier</td>
            <?php 
		foreach ($array as $key => $val)
		{
			$supplierAmount	= round(GetSupplierAmount($val[0]));
			$array_total[$val[1]][1] = $supplierAmount;
		?>
            <td nowrap="nowrap" title="<?php echo "Monthly payment to supplier -> ".$val[0];?>" style="text-align:right" bgcolor="<?php echo $val[4];?>" class="cls_td_amount" id="<?php echo $val[1]?>"><?php echo $supplierAmount=='0'?'-':'('.number_format($supplierAmount).')'?></td>
            <?php 
		}
		?>
          </tr>
          <tr>
            <td nowrap="nowrap">&nbsp;</td>
            <?php 
		foreach ($array as $key => $val)
		{
		?>
            <td nowrap="nowrap" title="<?php echo $val[0];?>" bgcolor="<?php echo $val[4];?>">&nbsp;</td>
            <?php 
		}
		?>
          </tr>
          <tr>
            <td nowrap="nowrap" title="Customer Name" ><b>Total</b></td>
            <?php 
		foreach ($array as $key => $val)
		{
			$total = $array_total[$val[1]][0] - $array_total[$val[1]][1];
		?>
            <td nowrap="nowrap" title="<?php echo "Total -> ".$val[0];?>" style="text-align:right" bgcolor="<?php echo $val[4];?>" class="cls_td_amount" id="<?php echo $val[1]?>"><b><?php echo ($total<0?'('.number_format(abs($total)).')':number_format(abs($total)))?></b></td>
            <?php 
		}
		?>
          </tr>
        </tbody>
      </table></td>
  </tr>
  <tr>
  <tr height="40">
    <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
  </tr>
</table>
<?php
$body = ob_get_clean();

$mailHeader 	= "Daily Cash Flow Forecast";
$FROM_NAME 		= 'NSOFT - SCREENLINE HOLDINGS.';
$FROM_EMAIL		= 'nsoft@screenlineholdings.com';

sendMessage($FROM_EMAIL,$FROM_NAME,$CRON_TABLE_TO,$mailHeader,$body,$CRON_TABLE_CC,$CRON_TABLE_BCC);

echo $body;

function GetCustomerAmount($date)
{
	global $db;
	global $companyId;
	global $target_currencyId;
	
	$sql = "SELECT 
				SUM(FT.BALANCE_AMOUNT) AS AMOUNT		
			FROM finance_forecast_transaction_customer FT
			WHERE DATE(FT.DATE) = '$date'
				AND FT.COMPANY_ID = $companyId ";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	return $row["AMOUNT"];
}

function GetSupplierAmount($date)
{
	global $db;
	global $companyId;
	global $target_currencyId;	
	
	$sql = "SELECT 
				SUM(FT.BALANCE_AMOUNT) AS AMOUNT		
			FROM finance_forecast_transaction_supplier FT
			WHERE DATE(FT.DATE) = '$date'
				AND FT.COMPANY_ID = $companyId ";
	$result = $db->RunQuery($sql);
	$row 	= mysqli_fetch_array($result);
	return $row["AMOUNT"];
}
?>
<?php
$db->commit();		
$db->disconnect();
?>


<?php
session_start();
$backwardseperator 	= "../../../../";
$mainPath 			= $_SESSION['mainPath'];
$userId 			= $_SESSION['userId'];
$requestType 		= $_REQUEST['requestType'];
$companyId 			= $_SESSION['headCompanyId'];
$location 			= $_SESSION['CompanyID'];
include "{$backwardseperator}dataAccess/Connector.php";
include "{$backwardseperator}presentation/finance/reports/accountReports/AccReportFunctions.php";

$toDay = date("Y-m-d");

if($requestType=='loadCombo')	//load existings
{
	$sql = "SELECT
				FBRH.recId,
				CONCAT(FBRH.recName,' - ',FBRH.recId) AS recName
			FROM
				fin_bank_rec_header FBRH
			WHERE
				FBRH.recStatus =1 AND
				FBRH.companyId = $companyId
			ORDER BY
				FBRH.recId DESC";
	$result = $db->RunQuery($sql);
	$recList="<option value=\"\"></option>";
	while ($row = mysqli_fetch_array($result))
	{
		$recList.= "<option value=\"" . $row['recId'] . "\">" . $row['recName'] ."</option>";
	}
	$response['recList'] = $recList;
	echo json_encode($response);
}
else if($requestType=='loadRecsCombo')	// load reconsiled
{
     $sql = "SELECT
                fin_bank_rec_header.recId,
                CONCAT(fin_bank_rec_header.recName,' - ',fin_bank_rec_header.recId) AS recName
            FROM
                fin_bank_rec_header
            WHERE
                fin_bank_rec_header.recStatus =2 AND
                fin_bank_rec_header.companyId = $companyId
            ORDER BY
                fin_bank_rec_header.recId DESC";
	$result  = $db->RunQuery($sql);
	$recList = "<option value=\"\"></option>";
	while ($row = mysqli_fetch_array($result))
	{
		$recList .= "<option value=\"" . $row['recId'] . "\">" . $row['recName'] ."</option>";
	}
	$response['recList'] = $recList;
	echo json_encode($response);
}
else if($requestType=='loadDetails')
{
    $recId = $_REQUEST['recId'];
	
    $sql = "SELECT
				fin_bank_rec_header.recId,
				fin_bank_rec_header.recName,
				fin_bank_rec_header.accId,
				fin_bank_rec_header.currencyId,
				fin_bank_rec_header.startDate,
				fin_bank_rec_header.endDate,
				fin_bank_rec_header.statmentDate,
				fin_bank_rec_header.biginBal,
				fin_bank_rec_header.endBalance,
				fin_bank_rec_header.recStatus
			FROM
				fin_bank_rec_header
			WHERE
				fin_bank_rec_header.recId = $recId";
    $result = $db->RunQuery($sql);
    $row 	= mysqli_fetch_array($result);
    
    $accId			= $row['accId'];
	$recName		= $row['recName'];
    $currencyId		= $row['currencyId'];    
    $endDate		= $row['endDate'];
    $biginBal		= $row['biginBal'];
    $endBalance		= $row['endBalance'];
    $recStatus		= $row['recStatus'];
    
    //header Details
    $response['recId']			= $recId;
	$response['recName']		= $recName;
    $response['accId']			= $accId;
    $response['currencyId']		= $currencyId;
    $response['startDate']		= $row['startDate'];  
    $response['endDate']		= $endDate;
    $response['statmentDate']	= $row['statmentDate'];  
    $response['biginBal']		= $biginBal;
    $response['endBalance']		= $endBalance;
    
    if($recStatus==1){// Active ones
        $cleardBalance 	= $biginBal;
        $differance		= 0.00;

        //get debit entries
        $sql = "SELECT
					fin_transactions_details.detailId,
					SUBSTRING(fin_transactions.entryDate,1,10) AS entryDate,
					fin_transactions.documentNo,
					fin_transactions_details.amount,
					fin_transactions.strProgramType,
					fin_transactions.personType,
					fin_transactions.personId,
					fin_transactions_details.details,
					fin_transactions.entryId,
					fin_transactions.paymentNumber,
					SUBSTRING(fin_transactions_details.presentedDate,1,10) AS presentedDate,
					fin_transactions_details.recType
            	FROM
                	fin_transactions
                INNER JOIN fin_transactions_details ON fin_transactions.entryId = fin_transactions_details.entryId
           		WHERE
					fin_transactions.companyId = $companyId AND
					fin_transactions.authorized = 1 AND
					fin_transactions.delStatus = 0 AND
					fin_transactions.currencyId = $currencyId AND
					fin_transactions_details.accountId = $accId AND
					fin_transactions_details.`credit/debit` = 'D' AND
					fin_transactions_details.recType <> 'R'";
					
        $result = $db->RunQuery($sql);
        $debitList="";
        while($row = mysqli_fetch_array($result))
		{
            $entryId		= $row['detailId'];
            $mainEntryId	= $row['entryId'];
            $entDate		= $row['entryDate'];//date_format($row['dtDate'], 'Y-m-d');
            $entNumber		= $row['documentNo'];
            $programType	= $row['strProgramType'];
			$payRefNo		= ($row['paymentNumber']==""?'&nbsp;':$row['paymentNumber']);
			$preDate		= $row['presentedDate'];
			$recDate 		= $preDate == 'null'? $entDate : $preDate;
            // get Oppercit account
            $arr			= oppcitAccountsMulty('D', $mainEntryId);
            $opcAcclist		= "";
            for($i=0; $i<count($arr); ++$i)
			{
                $opcAcclist.=($i+1).')'.$arr[$i].'<br/>';            
            }
            $entAmoubt		= $row['amount'];
            //get Customer/Supplier name
            $personType		= $row['personType'];
            $personId		= $row['personId'];
            $name			= getHolerName($personType,$personId);
            $transDetals	= ($row['details']==""?'&nbsp;':$row['details']);

            $checked		= "";
            if($row['recType']=='C'){
                $checked		= "checked";
                $cleardBalance += $entAmoubt;
            }

            $debitList.= "<tr>
                            <td><input class=\"cls_debit\" type=\"checkbox\"  name=\"chkDebit\" id=\"chkDebit\" value=\"$entryId\" $checked onchange=\"debitCheck(this)\" /></td>
                            <td>$entDate</td>
                            <td onclick=\" leadgerDrill('$programType','$entNumber')\" class=\"dillLink\">$entNumber</td>
                            <td class=\"cls_debitAmount\"><input type=\"text\" class=\"txtReadOnly\" readonly  name=\"debAmount$entryId\" id=\"debAmount$entryId\" style=\"width: 100%;\" value=\"$entAmoubt\"/></td>
                            <td>$opcAcclist</td>                            
                            <td>$payRefNo</td>                          
							<td class=\"cls_debitDate\"><input class=\"cls_debitDateInput\" name=\"preDate$entryId\" type=\"text\" value=\"$recDate\" id=\"preDate$entryId\" style=\"width:98px;\" onkeypress=\"return ControlableKeyAccess(event);\"  onclick=\"return showCalendar(this.id, '%Y-%m-%d');\"/><input type=\"reset\" value=\"\"  class=\"txtbox\" style=\"visibility:hidden;\"   onclick=\"return showCalendar(this.id, '%Y-%m-%');\" /></td>
                            <td>$transDetals</td>
                         </tr>";


        }
        $response['debitList'] = $debitList;

        //get Credit entries
        $sql="SELECT
                fin_transactions_details.detailId,
                SUBSTRING(fin_transactions.entryDate,1,10) AS entryDate,
                fin_transactions.documentNo,
                fin_transactions_details.amount,
                fin_transactions.strProgramType,
                fin_transactions.personType,
                fin_transactions.personId,
                fin_transactions_details.details,
                fin_transactions.entryId,
				fin_transactions.paymentNumber,
				SUBSTRING(fin_transactions_details.presentedDate,1,10) AS presentedDate,
                fin_transactions_details.recType
            FROM
                fin_transactions
                INNER JOIN fin_transactions_details ON fin_transactions.entryId = fin_transactions_details.entryId
            WHERE
                fin_transactions.companyId = $companyId AND
                fin_transactions.authorized = 1 AND
                fin_transactions.delStatus = 0 AND
                fin_transactions.currencyId = $currencyId AND
                fin_transactions_details.accountId = $accId AND
                fin_transactions_details.`credit/debit` = 'C' AND
                fin_transactions_details.recType <> 'R'";
        $result = $db->RunQuery($sql);
        $creditList="";
        while($row = mysqli_fetch_array($result))
		{
            $entryId		= $row['detailId'];
            $mainEntryId	= $row['entryId'];
            $entDate		= $row['entryDate'];//date_format($row['dtDate'], 'Y-m-d');
            $entNumber		= $row['documentNo'];
            $programType	= $row['strProgramType'];
			$payRefNo		= ($row['paymentNumber']==""?'&nbsp;':$row['paymentNumber']);
			$preDate		= $row['presentedDate'];
			$recDate 		= $preDate == 'null'? $entDate : $preDate;
            // get Oppercit account
            $arr			= oppcitAccountsMulty('C', $mainEntryId);
            $opcAcclist		= "";
            for($i=0; $i<count($arr); ++$i)
			{
                $opcAcclist.=($i+1).')'.$arr[$i].'<br/>';            
            }
            $entAmoubt		= $row['amount'];
            //get Customer/Supplier name
            $name			= getHolerName($row['personType'],$row['personId']);
           $transDetals	= ($row['details']==""?'&nbsp;':$row['details']);

            $checked		= "";
            if($row['recType']=='C'){
                $checked		 = "checked";
                $cleardBalance	-= $entAmoubt;
            }

            $creditList.= "<tr>
                            <td><input class=\"cls_credit\" type=\"checkbox\" name=\"chkCredit\" id=\"chkCredit\" value=\"$entryId\" $checked  onchange=\"creditCheck(this)\"/></td>
                            <td>$entDate</td>
                            <td onclick=\" leadgerDrill('$programType','$entNumber')\" class=\"dillLink\">$entNumber</td>
                            <td class=\"cls_creditAmount\"><input type=\"text\" class=\"txtReadOnly\" readonly  name=\"creAmount$entryId\" id=\"creAmount$entryId\" style=\"width: 100%;\" value=\"$entAmoubt\"/></td></td>
                            <td>$opcAcclist</td>                            
                            <td>$payRefNo</td>
                            <td class=\"cls_creditDate\"><input class=\"cls_creditDateInput\" name=\"preDate$entryId\" type=\"text\" value=\"$recDate\" id=\"preDate$entryId\" style=\"width:98px;\" onkeypress=\"return ControlableKeyAccess(event);\"  onclick=\"return showCalendar(this.id, '%Y-%m-%d');\"/><input type=\"reset\" value=\"\"  class=\"txtbox\" style=\"visibility:hidden;\" onclick=\"return showCalendar(this.id, '%Y-%m-%');\" /></td>
                            <td>$transDetals</td>
                         </tr>";


        }
        $response['creditList']	= $creditList;
        $differance				= $endBalance-$cleardBalance;
    }
    else if($recStatus==2){// Reconciled Ones
        $differance	= 0.00;

        //get debit entries
        $sql="(SELECT
                fin_transactions_details.detailId,
                SUBSTRING(fin_transactions.entryDate,1,10) AS entryDate,
                fin_transactions.documentNo,
                fin_transactions_details.amount,
                fin_transactions.strProgramType,
                fin_transactions.personType,
                fin_transactions.personId,
                fin_transactions_details.details,
                fin_transactions.entryId,
				fin_transactions.paymentNumber,
				SUBSTRING(fin_transactions_details.presentedDate,1,10) AS presentedDate,
                fin_transactions_details.recType
            FROM
                fin_transactions
                INNER JOIN fin_transactions_details ON fin_transactions.entryId = fin_transactions_details.entryId
            WHERE
                fin_transactions.companyId = $companyId AND
                fin_transactions.authorized = 1 AND
                fin_transactions.delStatus = 0 AND
                fin_transactions.currencyId = $currencyId AND
                fin_transactions_details.accountId = $accId AND
                fin_transactions_details.`credit/debit` = 'D' AND
                fin_transactions_details.recType = 'N')
				UNION 
				(SELECT
				  fin_transactions_details.detailId,
				  SUBSTRING(fin_transactions.entryDate,1,10) AS entryDate,
				  fin_transactions.documentNo,
				  fin_transactions_details.amount,
				  fin_transactions.strProgramType,
				  fin_transactions.personType,
				  fin_transactions.personId,
				  fin_transactions_details.details,
				  fin_transactions.entryId,
				  fin_transactions.paymentNumber,
				  SUBSTRING(fin_transactions_details.presentedDate,1,10) AS presentedDate,
				  fin_transactions_details.recType
				FROM fin_bank_rec_details BRD
				  INNER JOIN fin_transactions_details
					ON BRD.entryId = fin_transactions_details.detailId
				  INNER JOIN fin_transactions
					ON fin_transactions.entryId = fin_transactions_details.entryId
				WHERE BRD.recId = $recId
				AND fin_transactions_details.`credit/debit` = 'D')";
        $result = $db->RunQuery($sql);
        $debitList="";
		$checked = "";
        while($row = mysqli_fetch_array($result))
		{
            $entryId		= $row['detailId'];
            $mainEntryId	= $row['entryId'];
            $entDate		= $row['entryDate'];//date_format($row['dtDate'], 'Y-m-d');
            $entNumber		= $row['documentNo'];
            $programType	= $row['strProgramType'];
			$payRefNo		= ($row['paymentNumber']==""?'&nbsp;':$row['paymentNumber']);
			$preDate		= $row['presentedDate'];
			$recDate 		= $preDate == 'null'? $entDate : $preDate;
            // get Oppercit account
            $arr			= oppcitAccountsMulty('D', $mainEntryId);
            $opcAcclist		= "";
            for($i=0; $i<count($arr); ++$i)
			{
                $opcAcclist.=($i+1).')'.$arr[$i].'<br/>';            
            }
            $entAmoubt		= $row['amount'];
            //get Customer/Supplier name
            $holderType		= $row['personType'];
            $holderName		= $row['personId'];
            $name			= getHolerName($holderType,$holderName);
           $transDetals	= ($row['details']==""?'&nbsp;':$row['details']);
		   
            if($row["recType"]=='R')
				$checked = "checked";
            $debitList.= "<tr>
                            <td><input class=\"cls_debit\" type=\"checkbox\"  name=\"chkDebit\" id=\"chkDebit\" value=\"$entryId\" $checked onchange=\"debitCheck(this)\" /></td>
                            <td>$entDate</td>
                            <td>$entNumber</td>
							<td class=\"cls_debitAmount\"><input type=\"text\" class=\"txtReadOnly\" readonly  name=\"debAmount$entryId\" id=\"debAmount$entryId\" style=\"width: 100%;\" value=\"$entAmoubt\"/></td>
                            <td>$opcAcclist</td>                            
                            <td>$payRefNo</td>
							<td class=\"cls_debitDate\"><input class=\"cls_debitDateInput\" name=\"preDate$entryId\" type=\"text\" value=\"$recDate\" id=\"preDate$entryId\" style=\"width:98px;\" onkeypress=\"return ControlableKeyAccess(event);\"  onclick=\"return showCalendar(this.id, '%Y-%m-%d');\"/><input type=\"reset\" value=\"\"  class=\"txtbox\" style=\"visibility:hidden;\"   onclick=\"return showCalendar(this.id, '%Y-%m-%');\" /></td>
                            <td>$transDetals</td>
                         </tr>";


        }
        $response['debitList']	= $debitList;

        //get Credit entries
        $sql="(SELECT
                fin_transactions_details.detailId,
                SUBSTRING(fin_transactions.entryDate,1,10) AS entryDate,
                fin_transactions.documentNo,
                fin_transactions_details.amount,
                fin_transactions.strProgramType,
                fin_transactions.personType,
                fin_transactions.personId,
                fin_transactions_details.details,
                fin_transactions.entryId,
				fin_transactions.paymentNumber,
				SUBSTRING(fin_transactions_details.presentedDate,1,10) AS presentedDate,
                fin_transactions_details.recType
            FROM
                fin_transactions
                INNER JOIN fin_transactions_details ON fin_transactions.entryId = fin_transactions_details.entryId
            WHERE
                fin_transactions.companyId = $companyId AND
                fin_transactions.authorized = 1 AND
                fin_transactions.delStatus = 0 AND
                fin_transactions.currencyId = $currencyId AND
                fin_transactions_details.accountId = $accId AND
                fin_transactions_details.`credit/debit` = 'C' AND
                fin_transactions_details.recType = 'N')
				UNION
				(SELECT
				  fin_transactions_details.detailId,
				  SUBSTRING(fin_transactions.entryDate,1,10) AS entryDate,
				  fin_transactions.documentNo,
				  fin_transactions_details.amount,
				  fin_transactions.strProgramType,
				  fin_transactions.personType,
				  fin_transactions.personId,
				  fin_transactions_details.details,
				  fin_transactions.entryId,
				  fin_transactions.paymentNumber,
				  SUBSTRING(fin_transactions_details.presentedDate,1,10) AS presentedDate,
				  fin_transactions_details.recType
				FROM fin_bank_rec_details BRD
				  INNER JOIN fin_transactions_details
					ON BRD.entryId = fin_transactions_details.detailId
				  INNER JOIN fin_transactions
					ON fin_transactions.entryId = fin_transactions_details.entryId
				WHERE BRD.recId = $recId
				AND fin_transactions_details.`credit/debit` = 'C')";
        $result = $db->RunQuery($sql);
        $creditList="";
		$checked = "";
        while($row = mysqli_fetch_array($result))
		{
            $entryId		= $row['detailId'];
            $mainEntryId	= $row['entryId'];
            $entDate		= $row['entryDate'];//date_format($row['dtDate'], 'Y-m-d');
            $entNumber		= $row['documentNo'];
            $programType	= $row['strProgramType'];
			$payRefNo		= ($row['paymentNumber']==""?'&nbsp;':$row['paymentNumber']);
			$preDate		= $row['presentedDate'];
			$recDate 		= $preDate == 'null'? $entDate : $preDate;
            // get Oppercit account
            $arr			= oppcitAccountsMulty('C', $mainEntryId);
            $opcAcclist		= "";
            for($i=0; $i<count($arr); ++$i){
                $opcAcclist.=($i+1).')'.$arr[$i].'<br/>';            
            }
            $entAmoubt		= $row['amount'];
            //get Customer/Supplier name
            $name			= getHolerName($row['personType'],$row['personId']);
            $transDetals	= ($row['details']==""?'&nbsp;':$row['details']);
			     
			if($row["recType"]=='R')
				$checked = "checked";
            $creditList.= "<tr>
                            <td><input class=\"cls_credit\" type=\"checkbox\" name=\"chkCredit\" id=\"chkCredit\" value=\"$entryId\" $checked  onchange=\"creditCheck(this)\"/></td>
                            <td>$entDate</td>
                            <td>$entNumber</td>
	                        <td class=\"cls_creditAmount\"><input type=\"text\" class=\"txtReadOnly\" readonly  name=\"creAmount$entryId\" id=\"creAmount$entryId\" style=\"width: 100%;\" value=\"$entAmoubt\"/></td></td>
	                        <td>$opcAcclist</td>
                            <td>$payRefNo</td>
                            <td class=\"cls_creditDate\"><input class=\"cls_creditDateInput\" name=\"preDate$entryId\" type=\"text\" value=\"$recDate\" id=\"preDate$entryId\" style=\"width:98px;\" onkeypress=\"return ControlableKeyAccess(event);\"  onclick=\"return showCalendar(this.id, '%Y-%m-%d');\"/><input type=\"reset\" value=\"\"  class=\"txtbox\" style=\"visibility:hidden;\"   onclick=\"return showCalendar(this.id, '%Y-%m-%');\" /></td>
                            <td>$transDetals</td>
                         </tr>";


        }
        $response['creditList']	= $creditList;
        $differance				= $endBalance-$cleardBalance;        
    }    
    $response['cleardBalance']	= $cleardBalance;
    $response['differance']		= $differance;
    echo json_encode($response);
}

if($requestType=="URLLoadGrid")
{
	$account	= $_REQUEST["Account"];
	$currency	= $_REQUEST["Currency"];
	
	$openingBal = GetOpeningBalance($account,$currency);
	
	$result 	= GetDebitCreditSql('D',$account,$currency);
	while($row = mysqli_fetch_array($result))
	{
		$entryId		= $row['detailId'];
		$mainEntryId	= $row['entryId'];
		$entDate		= $row['entryDate'];
		$entNumber		= $row['documentNo'];
		$programType	= $row['strProgramType'];
		$payRefNo		= ($row['paymentNumber']==""?'&nbsp;':$row['paymentNumber']);
		$preDate		= $row['presentedDate'];
		$recDate 		= $preDate == 'null'? $entDate : $preDate;
		$arr			= oppcitAccountsMulty('D', $mainEntryId);
		$opcAcclist		= "";
		for($i=0; $i<count($arr); ++$i)
		{
			$opcAcclist.=($i+1).')'.$arr[$i].'<br/>';            
		}
		$entAmoubt		= $row['amount'];
		$personType		= $row['personType'];
		$personId		= $row['personId'];
		$name			= getHolerName($personType,$personId);
		$transDetals	= ($row['details']==""?'&nbsp;':$row['details']);
		$checked		= "";
		
		if($row['recType']=='C'){
			$checked		= "checked";
			$cleardBalance += $entAmoubt;
		}

		$debitList.= "<tr>
						<td><input class=\"cls_debit\" type=\"checkbox\"  name=\"chkDebit\" id=\"chkDebit\" value=\"$entryId\" $checked onchange=\"debitCheck(this)\" /></td>
						<td>$entDate</td>
						<td onclick=\" leadgerDrill('$programType','$entNumber')\" class=\"dillLink\">$entNumber</td>
						<td class=\"cls_debitAmount\"><input type=\"text\" class=\"txtReadOnly\" readonly  name=\"debAmount$entryId\" id=\"debAmount$entryId\" style=\"width: 100%;\" value=\"$entAmoubt\"/></td>
						<td>$opcAcclist</td>                            
						<td>$payRefNo</td>                          
						<td class=\"cls_debitDate\"><input class=\"cls_debitDateInput\" name=\"preDate$entryId\" type=\"text\" value=\"$recDate\" id=\"preDate$entryId\" style=\"width:98px;\" onkeypress=\"return ControlableKeyAccess(event);\"  onclick=\"return showCalendar(this.id, '%Y-%m-%d');\"/><input type=\"reset\" value=\"\"  class=\"txtbox\" style=\"visibility:hidden;\"   onclick=\"return showCalendar(this.id, '%Y-%m-%');\" /></td>
						<td>$transDetals</td>
					 </tr>";


	}
    $response['debitList'] = $debitList;
	
	$result 	= GetDebitCreditSql('C',$account,$currency);
	while($row = mysqli_fetch_array($result))
	{
		$entryId		= $row['detailId'];
		$mainEntryId	= $row['entryId'];
		$entDate		= $row['entryDate'];
		$entNumber		= $row['documentNo'];
		$programType	= $row['strProgramType'];
		$payRefNo		= ($row['paymentNumber']==""?'&nbsp;':$row['paymentNumber']);
		$preDate		= $row['presentedDate'];
		$recDate 		= $preDate == 'null'? $entDate : $preDate;
		$arr			= oppcitAccountsMulty('C', $mainEntryId);
		$opcAcclist		= "";
		for($i=0; $i<count($arr); ++$i)
		{
			$opcAcclist.=($i+1).')'.$arr[$i].'<br/>';            
		}
		$entAmoubt		= $row['amount'];
		$name			= getHolerName($row['personType'],$row['personId']);
		$transDetals	= ($row['details']==""?'&nbsp;':$row['details']);

		$checked		= "";
		if($row['recType']=='C'){
			$checked		 = "checked";
			$cleardBalance	-= $entAmoubt;
		}

		$creditList.= "<tr>
						<td><input class=\"cls_credit\" type=\"checkbox\" name=\"chkCredit\" id=\"chkCredit\" value=\"$entryId\" $checked  onchange=\"creditCheck(this)\"/></td>
						<td>$entDate</td>
						<td onclick=\" leadgerDrill('$programType','$entNumber')\" class=\"dillLink\">$entNumber</td>
						<td class=\"cls_creditAmount\"><input type=\"text\" class=\"txtReadOnly\" readonly  name=\"creAmount$entryId\" id=\"creAmount$entryId\" style=\"width: 100%;\" value=\"$entAmoubt\"/></td></td>
						<td>$opcAcclist</td>                            
						<td>$payRefNo</td>
						<td class=\"cls_creditDate\"><input class=\"cls_creditDateInput\" name=\"preDate$entryId\" type=\"text\" value=\"$recDate\" id=\"preDate$entryId\" style=\"width:98px;\" onkeypress=\"return ControlableKeyAccess(event);\"  onclick=\"return showCalendar(this.id, '%Y-%m-%d');\"/><input type=\"reset\" value=\"\"  class=\"txtbox\" style=\"visibility:hidden;\" onclick=\"return showCalendar(this.id, '%Y-%m-%');\" /></td>
						<td>$transDetals</td>
					 </tr>";


	}
	
    $response['creditList']	= $creditList;
	$response['OpeningBal']	= $openingBal;
	echo json_encode($response);
}

function GetOpeningBalance($account,$currency)
{
	global $db;
	$openingBal = 0;
	$sql = "SELECT endBalance FROM fin_bank_rec_header WHERE accId = '$account' AND currencyId = '$currency' ORDER BY recId DESC LIMIT 1";
	$result = $db->RunQuery($sql);
	while($row = mysqli_fetch_array($result))
	{
		$openingBal = $row["endBalance"];
	}
	return $openingBal;
}

function  GetDebitCreditSql($type,$account,$currency)
{
	global $db;
	global $companyId;
	
	$sql = "SELECT
				FTD.detailId,
				SUBSTRING(FT.entryDate,1,10) AS entryDate,
				FT.documentNo,
				FTD.amount,
				FT.strProgramType,
				FT.personType,
				FT.personId,
				FTD.details,
				FT.entryId,
				FT.paymentNumber,
				SUBSTRING(FTD.presentedDate,1,10) AS presentedDate,
				FTD.recType
			FROM fin_transactions FT
			INNER JOIN fin_transactions_details FTD
				ON FT.entryId = FTD.entryId
			WHERE
				FT.companyId = $companyId AND
				FT.authorized = 1 AND
				FT.delStatus = 0 AND
				FT.currencyId = $currency AND
				FTD.accountId = $account AND
				FTD.`credit/debit` = '$type' AND
				FTD.recType <> 'R'";
      return $db->RunQuery($sql);
}
//get Customer or sublier name
function getHolerName($personType,$personId){
    //$stakeHolderType="supplier";$PearentHolder=9;
    global $db;
    if($personType=="sup"){
        $sql="SELECT mst_supplier.strName AS name FROM mst_supplier WHERE mst_supplier.intId = $personId";
    }
    else if($personType=="cus"){
        $sql="SELECT mst_customer.strName AS name FROM mst_customer WHERE mst_customer.intId = $personId";
    }
    else $sql=null;
    
    $name="-";
    if($sql!=null){
        $result = $db->RunQuery($sql);
        $row 	= mysqli_fetch_array($result);
        $name	= $row['name'];
    }
    return $name;
}
?>

<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$companyId = $_SESSION['headCompanyId'];
$location = $_SESSION['CompanyID'];
$thisFilePath = $_SERVER['PHP_SELF'];
include "{$backwardseperator}dataAccess/permisionCheck.inc";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Bank Reconciliations</title>
        <link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/tblstyle.css" rel="stylesheet" type="text/css" />

        <script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
        <script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
        <script src="bankReconciliations.js" type="text/javascript"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

        <link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
        <script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

        <link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css"/>
        <link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css"/>  
        <script type="application/javascript" src="../../reports/accountReports/DrillAccount.js"></script>
        <script type="text/javascript" src="jquery_collapse.js"></script>
		<script type="text/javascript">
        $(document).ready(function(){
            $(".toggle_container").show();
            $("span.expand_heading").toggle(function(){
                $(this).addClass("active"); 
                }, function () {
                $(this).removeClass("active");
            });
            $("span.expand_heading").click(function(){
                $(this).next(".toggle_container").slideToggle("slow");
            });
        });
        </script>
        <style>
            .txtReadOnly{
                background-color: #FAD163; 
                border: 0px;
                text-align: right;
                font-family: Verdana;
                font-size: 11px;
                font-weight: normal;
            }
            .dillLink{
                cursor: pointer;
            }
        </style>
    <style type="text/css">

	.wrapper {
		width: 400px;
		margin: 0 auto;
	}
	.expand_top,.expand_wrapper
	{
		width: 400px;
		padding:0px;
		margin:0px 0px 5px 0px;
		float:left;
	}
	h1 {
		font: 4em normal Georgia, 'Times New Roman', Times, serif;
		text-align:center;
		padding: 20px 0;
		color: #ffffff;
	}
	
	span.expand_heading {
		padding: 0 0 0 20px;
		margin: 0 0 5px 0;
		background: url(expand_collapse.png) no-repeat;
		height: 30px;
		line-height: 30px;
		width: 98%;
		font-size: 1em;
		/*font-weight: normal;*/
		float: left;
	}
	.expand_all
	{
		cursor:default;
	}
	span.expand_heading a {
		color: #9A9651;
		text-decoration: none;
		display: block;
	}
	span.expand_heading a:hover {
	color: #FFF;
	}
	
	span.active {background-position: left bottom;}
	.toggle_container {
		margin: 0 0 5px;
		padding: 0;
		/*border-top: 1px solid #d6d6d6;*/
		background: #ffffff;
		overflow: hidden;
		font-size: 1.2em;
		width: 100%;
		clear: both;
	}
	.toggle_container h3 {
		font: 2.0em normal Georgia, "Times New Roman", Times, serif;
		margin: 0 0 5px;
		padding: 0 0 5px 0;
		color:#000000;
		border-bottom: 1px dotted #ccc;
	}
	</style>
    </head>

    <body>
        <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
                <td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator . 'Header.php'; ?></td>
            </tr> 
        </table>
        <script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
        <script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
        <script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>
         <link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />         
        <div align="center">
            <form id="frmBankRec" name="frmBankRec" method="post" autocomplete="off">
                <div class="trans_layoutXL">
                    <div class="trans_text">Bank Reconciliations</div>
                    <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
                        <tr>
                            <td>
                          <table width="100%" class="tableBorder_allRound">
                                    <tr>
                                        <td width="4%" bgcolor="#FFFFFF">&nbsp;</td>
                                        <td width="33%" bgcolor="#FFFFFF" class="normalfntRight">Existing</td>
                                        <td width="29%" bgcolor="#FFFFFF"><span class="normalfntMid">
                                                <select name="cboSearch" id="cboSearch" style="width:100%">
                                                    <option value=""></option>
                                                    <?php
                                                        $sql2 = "SELECT
                                                                    fin_bank_rec_header.recId,
                                                                    fin_bank_rec_header.recName
                                                                FROM
                                                                    fin_bank_rec_header
                                                                WHERE
                                                                    fin_bank_rec_header.recStatus =1 AND
                                                                    fin_bank_rec_header.companyId = $companyId
                                                                ORDER BY
                                                                    fin_bank_rec_header.recId DESC";
                                                        $result2 = $db->RunQuery($sql2);
                                                        while ($row2 = mysqli_fetch_array($result2)) {
                                                            echo "<option value=\"" . $row2['recId'] . "\">" . $row2['recName'] ."</option>";
                                                        }
                                                    ?>                                                
                                                </select>
                                          </span></td>
                                        <td width="34%" >&nbsp;<input type="checkbox" name="chkRecon" id="chkRecon" value="ON" /> 
                                          <span class="normalfnt">&nbsp;Reconciled reports</span></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                           <!-- <div class="expand_wrapper">-->
							<span class="expand_heading normalfntMid tableBorder_allRound" style="background-color:#dce9f9;font-size:14px"><a href="#"><strong>N e w   B a n k   R  e  c  o  n  c  i  l  i  a  t  i o  n  s</strong></a></span>
                   		  <div class="toggle_container">
                                <table width="100%" class="tableBorder_allRound">
                                    <!--<tr>
                                        <td align="center" bgcolor="#DADADA" class="tableBorder expand_heading"><span class="normalfntMid"><strong>New Bank Reconciliations</strong></span></td>
                                    </tr>-->
                                    <tr>
                                        <td>
                                            <table width="100%" class="">
                                                <tr>
                                                  <td>&nbsp;</td>
                                                  <td width="211" rowspan="2" class="normalfnt"><strong>Reconciliation Details</strong></td>
                                                  <td align="left" class="normalfnt">Number</td>
                                                  <td colspan="3" align="left" class="normalfnt"><input name="txtNo" type="text" readonly="readonly"class="normalfntRight" id="txtNo" style="width:320px; background-color:#F4FFFF;text-align:center; border:dotted; border-color:#F00" onblur="numberExisting(this,'Sales Invoice');" />
                                                  <input name="amStatus" type="text" class="normalfntBlue" id="amStatus" style="width:40px; background-color:#FFF; text-align:center; border:thin" disabled="disabled" value="(Auto)"/></td>
                                                  <td>&nbsp;</td>
                                                </tr>
                                                <tr>
                                                  <td width="38">&nbsp;</td>
                                                    <td width="186" align="left" class="normalfnt">Accounts <span class="compulsoryRed">*</span></td>
                                                    <td width="269"><span class="normalfntMid">
                                                            <select name="cmbAccount" id="cmbAccount" style="width:100%" class="validate[required] cls_loadGrid">
                                                                <option value=""></option>
                                                                <?php
                                                                    $sql2 = "SELECT
                                                                    mst_financechartofaccounts.intId,
                                                                    mst_financechartofaccounts.strCode,
                                                                    mst_financechartofaccounts.strName
                                                                FROM
                                                                    mst_financechartofaccounts
                                                                INNER JOIN mst_financechartofaccounts_companies ON mst_financechartofaccounts.intId = mst_financechartofaccounts_companies.intChartOfAccountId
                                                                    WHERE
                                                                    mst_financechartofaccounts.strType = 'Posting' AND
                                                                    mst_financechartofaccounts_companies.intCompanyId = $companyId AND
                                                                    mst_financechartofaccounts.intFinancialTypeId = 24
                                                                ORDER BY
                                                                    mst_financechartofaccounts.strCode DESC";
                                                                    $result2 = $db->RunQuery($sql2);
                                                                    while ($row2 = mysqli_fetch_array($result2)) {
                                                                        echo "<option value=\"" . $row2['intId'] . "\">" . $row2['strCode'] . "-" . $row2['strName'] . "</option>";
                                                                    }
                                                                ?>

                                                            </select>
                                                        </span></td>
                                                    <td width="109" class="normalfnt">Currency <span class="compulsoryRed">*</span></td>
                                                    <td><span class="normalfnt">
                                                            <select name="cboCurrency" id="cboCurrency" style="width:100px"  class="validate[required] cls_loadGrid">
                                                                <option value=""></option>
                                                                <?php
                                                                    $sql = "SELECT
                                                                                mst_financecurrency.intId,
                                                                                mst_financecurrency.strCode
                                                                            FROM mst_financecurrency";
                                                                    $result = $db->RunQuery($sql);
                                                                    while ($row = mysqli_fetch_array($result)) {
                                                                        echo "<option value=\"" . $row['intId'] . "\">" . $row['strCode'] . "</option>";
                                                                    }
                                                                ?>
                                                            </select>

                                                        </span>
                                                    </td>
                                                    <td width="247" class="normalfnt"><a href="#" class="cls_loadGrid_click">Reload Data</a></td>
                                                </tr>
                                                <tr>
                                                  <td align="left" bgcolor="#F4F4F4">&nbsp;</td>
                                                    <td align="left" bgcolor="#F4F4F4"><span class="normalfnt"><strong>Period</strong></span></td>
                                                    <td align="left" bgcolor="#F4F4F4"><span class="normalfntMid">From</span></td>
                                                    <td width="269" bgcolor="#F4F4F4"><input name="startDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="validate[required]" id="startDate" style="width:98px;" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                                                    <td align="left" bgcolor="#F4F4F4"><span class="normalfnt">To</span></td>
                                                    <td width="125" bgcolor="#F4F4F4"><input name="endDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="validate[required]" id="endDate" style="width:98px;" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                                                    <td bgcolor="#F4F4F4">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                  <td align="center" bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
                                                    <td align="center" bgcolor="#FFFFFF" class="normalfnt"><strong>Bank Statement Details</strong></td>
                                                    <td bgcolor="#FFFFFF"><span class="normalfntMid">Date</span></td>
                                                    <td width="269" bgcolor="#FFFFFF"><input name="statDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="validate[required]" id="statDate" style="width:98px;" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                                                    <td bgcolor="#FFFFFF">&nbsp;</td>
                                                    <td bgcolor="#FFFFFF">&nbsp;</td>
                                                    <td bgcolor="#FFFFFF">&nbsp;</td>
                                                </tr>
                                                <tr>
                                                  <td bgcolor="#F4F4F4">&nbsp;</td>
                                                    <td bgcolor="#F4F4F4">&nbsp;</td>
                                                    <td bgcolor="#F4F4F4"><span class="normalfntMid"><b>Opening Balance</b> <span class="compulsoryRed">*</span></span></td>
                                                    <td bgcolor="#F4F4F4"><span class="normalfnt">
                                                            <input type="text" name="biginBal" id="biginBal" style="width:98px" class="validate[required] cls_beginBalance"/>
                                                        </span></td>
                                                    <td bgcolor="#F4F4F4" ><span class="normalfntMid"><b>Closing Balance</b></span></td>
                                                    <td bgcolor="#F4F4F4"><span class="normalfnt">
                                                      <input name="eBal" type="text" disabled="disabled" id="eBal" style="width:98px" />
                                                    </span></td>
                                                    <td bgcolor="#F4F4F4">&nbsp;</td>
                                                </tr>
                                            </table>
                                        </td>
                                  </tr>
                                    <tr>
                                        <td align="center">&nbsp;</td>
                                    </tr>  <!--onclick="ClearCompanyForm();"--> <!--onclick="butCommand1(this.name);"-->
                                </table>
                                </div>
                          </td>
                        </tr>
                        <tr>
                            <td>
                            <table width="102%" class="" bgcolor="#FFFFFF">
                                    <tr>
                                        <td width="50%" align="center" bgcolor="#dce9f9" class="normalfntMid"><strong>Deposit</strong></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <table width="100%" class="">
                                                <tr>
                                                    <td class="tableBorder">
                                                        <div style="overflow:scroll;width:100%;height:200px;" id="divGrid">
                                                            <table width="100%" height="22" border="0" cellpadding="0" cellspacing="1" id="tblMainGrid2" class="bordered">
                                                                <thead>
                                                                <tr class="">
                                                                    <th width="15">&nbsp;</th>
                                                                    <th width="70" class="normalfntMid"><strong>Date</strong></th>
                                                                    <th width="179" class="normalfntMid"><strong>No.</strong></th>
                                                                    <th width="95" class="normalfntMid"><strong>Amount</strong></th>
                                                                    <th width="228" class="normalfntMid"><strong>Account</strong></th>                                                                    
                                                                    <th width="140" class="normalfntMid"><strong>Bank Reference No.</strong></th>
                                                                    <th width="105" class="normalfntMid"><strong>Presented Date</strong></th>
                                                                    <th width="191" class="normalfntMid"><strong>Memo</strong></th>
                                                                </tr>
                                                                </thead>
                                                                <tbody id="tblDebits" class="normalfntMid" bgcolor="#FFFFFF">                                                                    
                                                                </tbody>                                                                
                                                            </table>
                                                            <blockquote>&nbsp;</blockquote>
                                                      </div>
                                                    </td>
                                                </tr>
                                            </table>
                                      </td>
                                      </tr>
                                      <tr>
                                        <td width="50%" align="center" bgcolor="#dce9f9" class="normalfntMid"><strong>Payments</strong></td>
                                       </tr>
                                       <tr>
                                        <td>
                                            <table width="100%" class="">
                                                <tr>
                                                    <td class="tableBorder">
                                                        <div style="overflow:scroll;width:100%;height:200px;" id="divGrid">
                                                            <table width="100%" height="22" id="tblMainGrid" border="0" cellpadding="0" cellspacing="1" class="bordered">
                                                            <thead>
                                                                <tr class="">
                                                                    <th width="15" class="normalfntMid">&nbsp;</th>
                                                                    <th width="70" class="normalfntMid"><strong>Date</strong></th>
                                                                    <th width="179" class="normalfntMid"><strong>No.</strong></th>
                                                                    <th width="95" class="normalfntMid"  ><strong>Amount</strong></th>
                                                                    <th width="228" class="normalfntMid"  ><strong>Account</strong></th>                                                                    
                                                                    <th width="140" class="normalfntMid"  ><strong>Bank Reference No.</strong></th>
                                                                    <th width="105" class="normalfntMid"  ><strong>Presented Date</strong></th>
                                                                    <th width="191" class="normalfntMid"  ><strong>Memo</strong></th>
                                                                </tr>
                                                              </thead>
                                                                <tbody id="tblCredits" class="normalfntMid" bgcolor="#FFFFFF">                                                                    
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>     
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%">
                                    <tr style="display:none">
                                        <td width="14%">&nbsp;</td>
                                        <td width="14%"><span class="normalfntMid">Opening Balance</span></td>
                                        <td width="22%"><span class="normalfntMid">
                                                <input name="bBal" type="text" disabled="disabled" id="bBal" style="width:150px" />
                                            </span></td>
                                        <td width="11%">&nbsp;</td>
                                        <td width="39%">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                          <td align="center" class="tableBorder_allRound"><img border="0" src="../../../../images/Tnew.jpg" alt="New" name="New" width="92" height="24" class="mouseover" id="butNew" tabindex="28"/><img border="0" src="../../../../images/Tsave.jpg" alt="Save" name="Save"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/><img border="0" src="../../../../images/Treport.jpg" alt="Report" name="Report" width="92" height="24"  class="mouseover" id="butReport" tabindex="24"/><img style="display:none" border="0" src="../../../../images/Tdelete.jpg" alt="Delete" name="Delete" width="92" height="24" onclick="ConfirmDeleteCompany(this.name);" class="mouseover" id="butDelete" tabindex="25"/><a href="../../../../main.php"><img src="../../../../images/Tclose.jpg" alt="Close" name="Close" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                        </tr>
                        <tr style="display:none">
                            <td align="center" class="tableBorder_allRound"><input name="btnRec" type="button" class="calendar" id="btnRec" value="Reconcile" /> <input name="btnLeave" type="button" class="calendar" id="btnLeave" value="Leave" /> <input name="btnReport" type="button" class="calendar" id="btnReport" value="Report" disabled /></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </div>
            </form>
        </div>    
    </body>
</html>
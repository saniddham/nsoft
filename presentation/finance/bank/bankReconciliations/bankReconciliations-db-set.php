<?php

session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$userId = $_SESSION['userId'];
$companyId = $_SESSION['headCompanyId'];
$locationId = $_SESSION["CompanyID"];
include "{$backwardseperator}dataAccess/Connector.php";
$response = array('type' => '', 'msg' => '');

$requestType = $_REQUEST['requestType'];

if ($requestType == 'add')
{
    $accId = $_REQUEST['accId'];
    $currencyId = $_REQUEST['currencyId'];
    $startDate = $_REQUEST['startDate'];
    $endDate = $_REQUEST['endDate'];
    $stmDate = $_REQUEST['stDate'];
    $biginBal = $_REQUEST['biginBal'];
    $endBal = $_REQUEST['endBal'];
    $accName = $_REQUEST['accName'];

    $recName = $accName . "-" . $stmDate;
    //update previous reconsilation orger
    $sql = "UPDATE fin_bank_rec_header SET recOrder=recOrder+1 WHERE accId=$accId";
    $firstResult = $db->RunQuery($sql);

    $sql = "INSERT INTO fin_bank_rec_header (recName,accId,currencyId,startDate,endDate,statmentDate,biginBal,endBalance,companyId,userId,createdOn)
                VALUES ('$recName',$accId,$currencyId,'$startDate','$endDate','$stmDate',$biginBal,$endBal,$companyId,$userId,now())";
    $recId = $db->autoInsertNo($sql);

    $finalResult = true;
    if ($finalResult) {
        $response['type'] = 'pass';
        $response['msg'] = 'Saved successfully.';
        $response['recId'] = $recId;
    } else {
        $response['type'] = 'fail';
        $response['msg'] = $db->errormsg;
        $response['q'] = $sql;
    }
    echo json_encode($response);
} 

if ($requestType == 'edit')
{
    $recId = $_REQUEST['recId'];
    $startDate = $_REQUEST['startDate'];
    $stmDate = $_REQUEST['stDate'];
    $biginBal = $_REQUEST['biginBal'];
    $endBal = $_REQUEST['endBal'];
    $accName = $_REQUEST['accName'];

    $recName = $accName . "-" . $stmDate;
    //update previous reconsilation orger
    $sql = "UPDATE fin_bank_rec_header SET startDate='$startDate', statmentDate='$stmDate', biginBal=$biginBal, endBalance=$endBal
                WHERE recId=$recId";
    $finalResult = $db->RunQuery($sql);

    if ($finalResult) {
        $response['type'] = 'pass';
        $response['msg'] = 'Saved successfully.';
        $response['recId'] = $recId;
    } else {
        $response['type'] = 'fail';
        $response['msg'] = $db->errormsg;
        $response['q'] = $sql;
    }
    echo json_encode($response);
} 

if ($requestType == 'leave')
{
    $creditDetails = $_REQUEST['creditDetails'];
    $debitDetails = $_REQUEST['debitDetails'];

    //update Debits
    foreach ($debitDetails as $debit) {
		$preDate = $debit['preDate'] == null ? "null" : $debit['preDate']; // added by lasantha @ CAIT on 22/01/2013
        $type = $debit['status'] == 'true' ? 'D' : 'N';
        $entryId = $debit['entId'];
        $sql = "UPDATE fin_transactions_details SET recType='$type',presentedDate='$preDate' WHERE detailId=$entryId";
        $firstResult = $db->RunQuery($sql);
    }
    //update Credits
    foreach ($creditDetails as $credit) {
		$preDate = $credit['preDate'] == null ? "null" : $credit['preDate']; // added by lasantha @ CAIT on 22/01/2013
        $type = $credit['status'] == 'true' ? 'C' : 'N';
        $entryId = $credit['entId'];
        $sql = "UPDATE fin_transactions_details SET recType='$type',presentedDate='$preDate' WHERE detailId=$entryId";
        $secondResult = $db->RunQuery($sql);
    }

    if ($secondResult) {
        $response['type'] = 'pass';
        $response['msg'] = 'Saved successfully.';
    } else {
        $response['type'] = 'fail';
        $response['msg'] = $db->errormsg;
        $response['q'] = $sql;
    }

    echo json_encode($response);
} 

if ($requestType == 'rec')
{    
    $creditDetails = $_REQUEST['creditDetails'];
    $debitDetails = $_REQUEST['debitDetails'];
    $recId = $_REQUEST['recId'];

    $result = $db->RunQuery("SELECT fin_bank_rec_header.accId, fin_bank_rec_header.currencyId, fin_bank_rec_header.endDate FROM fin_bank_rec_header WHERE fin_bank_rec_header.recId = $recId");
    $row = mysqli_fetch_array($result);
    $currencyId = $row['currencyId'];
    $endDate = $row['endDate'];
    $accId = $row['accId'];
    $asAtBalance = getAsAtBalance($accId, $endDate, $currencyId);
    //update fin_bank_rec_header table status
    $sql = "UPDATE fin_bank_rec_header SET recStatus=2, asAtBanance=$asAtBalance WHERE recId=$recId";
    $theResult = $db->RunQuery($sql);
	$sql = "DELETE FROM fin_bank_rec_details WHERE recId = $recId";
    $db->RunQuery($sql);

    //update Debits
    foreach ($debitDetails as $debit) {
        $type = ($debit['status'] == 'true' ? 'R' : 'N');
        $entryId = $debit['entId'];
        $preDate = $debit['preDate'] == null ? "null" : $debit['preDate'];
        $sql = "UPDATE fin_transactions_details SET recType='$type',presentedDate='$preDate' WHERE detailId=$entryId";
        $firstResult = $db->RunQuery($sql);
        //INSERT bank_rec_details
        if ($debit['status'] == 'true') {
            $sql = "INSERT INTO fin_bank_rec_details (recId,entryId) VALUES($recId,$entryId)";
            $firstResult = $db->RunQuery($sql);
        }
    }
    //update Credits
    foreach ($creditDetails as $credit) {
        $type = ($credit['status'] == 'true' ? 'R' : 'N');
        $entryId = $credit['entId'];
        $preDate = $credit['preDate'] == null ? "null" : $credit['preDate'];
        $sql = "UPDATE fin_transactions_details SET recType='$type',presentedDate='$preDate' WHERE detailId=$entryId";
        $secondResult = $db->RunQuery($sql);
        //INSERT bank_rec_details
        if ($credit['status'] == 'true') {
            $sql = "INSERT INTO fin_bank_rec_details (recId,entryId) VALUES($recId,$entryId)";
            $secondResult = $db->RunQuery($sql);
        }
    }

    if ($secondResult) {
        $response['type'] = 'pass';
        $response['msg'] = 'Saved successfully.';
    } else {
        $response['type'] = 'fail';
        $response['msg'] = $db->errormsg;
        $response['q'] = $sql;
    }

    echo json_encode($response);
} 

if ($requestType == 'deleteRec') 
{
    $recId = $_REQUEST['recId'];    
    //rec Reconcilation  details
    $result = $db->RunQuery("SELECT fin_bank_rec_header.accId,fin_bank_rec_header.recOrder FROM fin_bank_rec_header WHERE fin_bank_rec_header.recId = $recId");
    $row = mysqli_fetch_array($result);
    $accId = $row['accId'];
    $recOrder = $row['recOrder'];
    if($recOrder == 0) {
        //update reconsilation order        
        $sql = "UPDATE fin_bank_rec_header SET recOrder=recOrder-1 WHERE accId=$accId";
        $firstResult = $db->RunQuery($sql);

        //update the fin_bank_rec_header
        $sql = "UPDATE fin_bank_rec_header SET recStatus=3,modifyBy=$userId,modifyOn=now() WHERE recId=$recId";
        $finalResult = $db->RunQuery($sql);

        //rec the details from bank_rec_details tables
        $sql = "SELECT fin_bank_rec_details.entryId FROM fin_bank_rec_details WHERE fin_bank_rec_details.recId = $recId";
        $result = $db->RunQuery($sql);
        while ($row = mysqli_fetch_array($result)) {
            $entryId = $row['entryId'];
            echo $entryId;
            //update tranc action tebail
            $sql = "UPDATE fin_transactions_details SET recType='N',presentedDate=null WHERE detailId=$entryId";
            $secondResult = $db->RunQuery($sql);
        }
        //delete from from bank_rec_details tables
        $sql = "DELETE FROM fin_bank_rec_details WHERE recId=$recId";
        $finalResult = $db->RunQuery($sql);

        $response['msg'] = $db->errormsg;
    }
    else {//Block delete if later reconcilation is existing for that lebger account
        $finalResult = false;
        $response['msg'] = 'Delete the first ' . $recOrder . ' Reconsilations First';
    } 

    if ($finalResult) {
        $response['type'] = 'pass';
        $response['msg'] = 'Deleted successfully.';
        $response['recId'] = $recId;
    } else {
        $response['type'] = 'fail';
        //$response['msg'] = $db->errormsg;
        $response['q'] = $sql;
    }
    echo json_encode($response);
}

if($requestType == "URLSave")
{
	$recId			= $_REQUEST["RecId"];
	$accountId		= $_REQUEST["AccountId"];
	$accountName	= $_REQUEST["AccountName"];
	$currencyId		= $_REQUEST["CurrencyId"];
	$startDate		= $_REQUEST["StartDate"];
	$endDate		= $_REQUEST["EndDate"];
	$openingBal		= $_REQUEST["OpeningBal"];
	$closingBal		= $_REQUEST["ClosingBal"];
	$debitDetails 	= $_REQUEST['debitDetails'];
	$creditDetails 	= $_REQUEST['creditDetails'];    
	
	if($recId == ""){
		$recId = Insert_fin_bank_rec_header($accountId,$accountName,$currencyId,$startDate,$endDate,$openingBal,$closingBal);
		UpdateRecOrder($accountId,$currencyId);
	}else
		$recId = Update_fin_bank_rec_header($recId,$startDate,$endDate,$openingBal,$closingBal);
		
	$asAtBalance = getAsAtBalance($accountId, $endDate, $currencyId);
	$sql = "UPDATE fin_bank_rec_header SET recStatus = 2, asAtBanance = $asAtBalance WHERE recId = $recId";
    $db->RunQuery($sql);
	
	$sql = "DELETE FROM fin_bank_rec_details WHERE recId = $recId";
    $db->RunQuery($sql);
	
    foreach ($debitDetails as $debit) {
        $type 		= ($debit['status'] == 'true' ? 'R' : 'N');
        $entryId 	= $debit['entId'];
        $preDate 	= $debit['preDate'] == null ? "null" : $debit['preDate'];
        
		$sql 		= "UPDATE fin_transactions_details SET recType='$type' WHERE detailId=$entryId";
        $firstResult = $db->RunQuery($sql);

        if ($debit['status'] == 'true') {
			$sql 		= "UPDATE fin_transactions_details SET presentedDate='$preDate' WHERE detailId=$entryId";
        $firstResult = $db->RunQuery($sql);
		
            $sql = "INSERT INTO fin_bank_rec_details (recId,entryId) VALUES($recId,$entryId)";
            $firstResult = $db->RunQuery($sql);
        }
    }

    foreach ($creditDetails as $credit) {
        $type 		= ($credit['status'] == 'true' ? 'R' : 'N');
        $entryId 	= $credit['entId'];
        $preDate 	= $credit['preDate'] == null ? "null" : $credit['preDate'];
        
		$sql = "UPDATE fin_transactions_details SET recType='$type' WHERE detailId=$entryId";
        $secondResult = $db->RunQuery($sql);

        if ($credit['status'] == 'true') {
			$sql = "UPDATE fin_transactions_details SET presentedDate='$preDate' WHERE detailId=$entryId";
        	$secondResult = $db->RunQuery($sql);
		
            $sql = "INSERT INTO fin_bank_rec_details (recId,entryId) VALUES($recId,$entryId)";
            $secondResult = $db->RunQuery($sql);
        }
    }
	
	if($secondResult){
        $response['type'] = 'pass';
        $response['msg'] = 'Saved successfully.';
    }else{
        $response['type'] = 'fail';
        $response['msg'] = $db->errormsg;
        $response['q'] = $sql;
    }

    echo json_encode($response);	
}

function UpdateRecOrder($accountId,$currencyId)
{
	global $db;
	
	 $sql = "UPDATE fin_bank_rec_header SET recOrder = recOrder+1 WHERE accId = $accountId AND currencyId = $currencyId";
	 $db->RunQuery($sql);
}

function Insert_fin_bank_rec_header($accountId,$accountName,$currencyId,$startDate,$endDate,$openingBal,$closingBal)
{
	global $db;
	global $companyId;
	global $userId;
	
	$recName = $accountName." - ".$startDate;
	$sql = "INSERT INTO fin_bank_rec_header (recName,accId,currencyId,startDate,endDate,statmentDate,biginBal,endBalance,companyId,userId,createdOn)
                VALUES ('$recName',$accountId,$currencyId,'$startDate','$endDate','$startDate',$openingBal,$closingBal,$companyId,$userId,now())";
    return $db->autoInsertNo($sql);
}

function Update_fin_bank_rec_header($recId,$startDate,$endDate,$openingBal,$closingBal)
{
	global $db;
	global $companyId;
	global $userId;
	
	 $sql = "UPDATE fin_bank_rec_header SET startDate = '$startDate', statmentDate = '$startDate', biginBal = $openingBal, endBalance = $closingBal
            	WHERE recId = $recId";
    $db->RunQuery($sql);
	return $recId;
}

function getAsAtBalance($accId, $endDate, $currencyId) {
    global $db;
    //get ledger account details        
    $resultLedg = $db->RunQuery("SELECT
                                mst_financialsubtype.intId,
                                mst_financialsubtype.strType,
                                mst_financechartofaccounts.strCode,
                                mst_financechartofaccounts.strName,
                                mst_financialmaintype.intId as mainTypeID
                            FROM
                                mst_financechartofaccounts
                                Inner Join mst_financialsubtype ON mst_financechartofaccounts.intFinancialTypeId = mst_financialsubtype.intId
                                Inner Join mst_financialmaintype ON mst_financialsubtype.intFinancialMainTypeID = mst_financialmaintype.intId
                            WHERE
                                mst_financechartofaccounts.intId =" . $accId);
    $rowLedg = mysqli_fetch_array($resultLedg);
    //get Data And calculate AS at Balance
    $resultDebitTot = $db->RunQuery("SELECT
                                            Sum(fin_transactions_details.amount) AS debitAmount
                                        FROM
                                            fin_transactions_details
                                            INNER JOIN fin_transactions ON fin_transactions.entryId = fin_transactions_details.entryId
                                        WHERE
                                            fin_transactions_details.accountId = $accId AND
                                            fin_transactions_details.`credit/debit` = 'D' AND
                                            fin_transactions.entryDate < '$endDate' AND
                                            fin_transactions.currencyId = $currencyId AND
                                            fin_transactions.companyId = $companyId AND
                                            fin_transactions.authorized = 1 AND
                                            fin_transactions.delStatus = 0");
    $rowDebitTot = mysqli_fetch_array($resultDebitTot);

    $resultCreditTot = $db->RunQuery("SELECT
                                            Sum(fin_transactions_details.amount) AS creditAmount
                                        FROM
                                            fin_transactions_details
                                            INNER JOIN fin_transactions ON fin_transactions.entryId = fin_transactions_details.entryId
                                        WHERE
                                            fin_transactions_details.accountId = $accId AND
                                            fin_transactions_details.`credit/debit` = 'C' AND
                                            fin_transactions.entryDate < '$endDate' AND
                                            fin_transactions.currencyId = $currencyId AND
                                            fin_transactions.companyId = $companyId AND
                                            fin_transactions.authorized = 1 AND
                                            fin_transactions.delStatus = 0");
    $rowCreditTot = mysqli_fetch_array($resultCreditTot);

    $asAtBalance = 0.00;
    if ($rowLedg['strType'] == 'D') {
        $asAtBalance = ($rowDebitTot['debitAmount'] - $rowCreditTot['creditAmount']);
    } else {
        $asAtBalance = ($rowCreditTot['creditAmount'] - $rowDebitTot['debitAmount']);
    }
    return $asAtBalance;
}

?>

<?php
session_start();
$backwardseperator = "../../../../";
$companyId = $_SESSION['headCompanyId'];
$location = $_SESSION['CompanyID'];
$intUser = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath = $_SERVER['PHP_SELF'];
include "{$backwardseperator}dataAccess/Connector.php";
//include "{$backwardseperator}finance/reports/accountReports/AccReportFunctions.php";

$locationId = $location; //this locationId use in report header(reportHeader.php)--------------------
$booCreditTotal 	= false;
$booDebitTotal 		= false;

$recId = $_REQUEST['recId'];
////rec Rec header details
$sql="SELECT
        fin_bank_rec_header.endDate,
        mst_financechartofaccounts.strCode AS ledgCode,
        mst_financechartofaccounts.strName AS ledgerName,
        mst_financecurrency.strCode AS currency,
        fin_bank_rec_header.asAtBanance
    FROM
        fin_bank_rec_header
        INNER JOIN mst_financechartofaccounts ON fin_bank_rec_header.accId = mst_financechartofaccounts.intId
        INNER JOIN mst_financecurrency ON fin_bank_rec_header.currencyId = mst_financecurrency.intId
    WHERE
        fin_bank_rec_header.recId = $recId";
$result=$db->RunQuery($sql);
$rowMainDetails=mysqli_fetch_array($result);
$asAtBalance=$rowMainDetails['asAtBanance'];

//get credit balance
$result=$db->RunQuery("SELECT Sum(fin_transactions_details.amount) AS cAmount FROM  fin_transactions_details INNER JOIN fin_bank_rec_details ON fin_transactions_details.detailId = fin_bank_rec_details.entryId WHERE fin_bank_rec_details.recId = $recId AND fin_transactions_details.`credit/debit` = 'C'");
$row=mysqli_fetch_array($result);
$ceditAmount=$row['cAmount'];

//get DEbit balance
$result=$db->RunQuery("SELECT Sum(fin_transactions_details.amount) AS dAmount FROM  fin_transactions_details INNER JOIN fin_bank_rec_details ON fin_transactions_details.detailId = fin_bank_rec_details.entryId WHERE fin_bank_rec_details.recId = $recId AND fin_transactions_details.`credit/debit` = 'D'");
$row=mysqli_fetch_array($result);
$debitAmount=$row['dAmount'];
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Bank Reconciliation as at  <?php echo $rowMainDetails['endDate'] ?></title>
        <link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
        <script type="application/javascript" src="../../../finance/reports/accountReports/DrillAccount.js"></script>
        <style>
            table.rptTable{
                width: 100%;
                border-collapse:collapse;
                font-size: 11px;
            }
            table.rptTable tr td{
                border:1px solid black;
            }
            .rptTblHeader {
                font-size: 11px;
                text-align: center;
                font-weight:bold;
            }
            .rptTblBody {
                font-size: 11px;
                text-align: right;
                font-weight:bold;;
            }
            .drillLink{
                cursor:pointer;
            }
            a:hover
            { 
                font-weight: bold;
                text-decoration:underline;
                color: #0000FF;
            }
            .textL{
                font-size: 12px;
                text-align: left;
            }
            .textR{
                font-size: 12px;
                text-align: right;
            }
            .tHeader{
                font-size: 11px;
                text-align: left;
                font-weight:bold;
            }
            
        </style>
    </head>
    <body>
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="3"></td>
            </tr>
            <tr>
                <td width="20%"></td>
                <td width="60%" height="80" valign="top"><?php include '../../reportHeader.php' ?></td>
                <td width="20%"></td>
            </tr>
            <tr>
                <td colspan="3"></td>
            </tr>
        </table>
        <div align="center">
            <div style="background-color:#FFF" ><strong>Bank Reconciliation as at  <?php echo $rowMainDetails['endDate'] ?></strong><strong></strong></div>
            <br/>
            <div style="background-color:#FFF" ><strong>Ledger Account</strong>&nbsp;&nbsp;&nbsp;<i><?php echo $rowMainDetails['ledgCode'] ?> - <?php echo $rowMainDetails['ledgerName'] ?> - <?php echo $rowMainDetails['currency'] ?></i></div>
            <table width="35%">
                <tr>
                    <td class="textL">Bank Balance as per bank book</td>
                    <td><?php echo $asAtBalance ?></td>
                </tr>
                <tr>
                    <td colspan="2" class="textL">Add</td>                    
                </tr>
                <tr>
                    <td class="textL">Issued but not yet presented (note A)</td>
                    <td class="textR"><?php echo $ceditAmount ?></td>
                </tr>
                <tr>
                    <td colspan="2" class="textL">Less</td>                    
                </tr>
                <tr>
                    <td class="textL">Deposited but not yet realized (note B)</td>
                    <td class="textR"><?php echo $debitAmount ?></td>
                </tr>
                <tr>
                    <td class="textL">Bank balance as per bank statement</td>
                    <td class="textR"><?php echo ($asAtBalance+$ceditAmount-$debitAmount) ?></td>
                </tr>
            </table>
            <table class="rptTable">
                <tr class="tHeader">
                    <td colspan="9">Issued but not yet presented (note A)</td>
                </tr>
                <tr class="rptTblHeader">
                    <td width="6%">Date</td>
                    <td width="9%">Number</td>
                    <td width="20%">Payment Number</td>                    
                    <td width="8%">Detail</td>
                    <td width="8%">Name</td>
                    <td width="16%">Account</td>
                    <td width="16%">Present Date</td>
                    <td width="9%">Amount[Cr]</td>
                    <td width="8%">Amount[De]</td>
                </tr>
                <?php
                    $sql="SELECT
                                fin_transactions.entryDate,
                                fin_transactions.strProgramType,
                                fin_transactions.paymentNumber,
                                fin_transactions_details.presentedDate,
                                fin_transactions_details.dimensionId,
                                fin_transactions_details.amount,
                                fin_transactions.documentNo,
                                fin_transactions_details.details,
                                fin_transactions.personType,
                                fin_transactions.personId,
                                fin_transactions.entryId
                            FROM
                                fin_transactions_details
                                INNER JOIN fin_bank_rec_details ON fin_bank_rec_details.entryId = fin_transactions_details.detailId
                                INNER JOIN fin_transactions ON fin_transactions.entryId = fin_transactions_details.entryId
                            WHERE
                                fin_bank_rec_details.recId = $recId AND
                                fin_transactions_details.`credit/debit` = 'C'";
                    $resul = $db->RunQuery($sql);
                    while($row=mysqli_fetch_array($resul)){
						$booCreditTotal = true;
                         //get Customer/Supplier name
                        $name=getHolerName($row['personType'],$row['personId']);
                        $mainEntId=$row['entryId'];
                ?>
                <tr>
                    <td><?php echo $row['entryDate'] ?></td>
                    <td><a class="drillLink" onclick="leadgerDrill('<?php echo ($row['strProgramType']) ?>','<?php echo $row['documentNo']?>')"><?php echo $row['documentNo'] ?></a></td>
                    <td><?php echo $row['paymentNumber'] ?></td>
                    <td><?php echo $row['details'] ?></td>
                    <td><?php echo $name ?></td>
                    <td><?php 
                            $arr=  oppcitAccountsMulty($row['strCredit/Debit'],$mainEntId=$row['entryId']);
                            for($i=0; $i<count($arr); ++$i){
                                echo $i+1;
                                echo '.)';
                                echo $arr[$i].'<br />';
                            }
                        ?>
                    </td>
                    <td ><?php echo ($row['presentedDate']=='null'?"":$row['presentedDate']) ?></td>
                    <td class="rptTblBody"><?php echo $row['amount'] ?></td>
                    <td class="rptTblBody">&nbsp;</td>
                </tr>
                <?php 
                $ceditTot+= $row['amount'];
                } ?>
                
                <?php if($booCreditTotal){?>
                <tr style="display:none">
                    <td colspan="6">Total</td>
                    <td class="rptTblBody">&nbsp;</td>
                    <td class="rptTblBody"><?php echo number_format($ceditTot,4) ?></td>
                    <td class="rptTblBody">&nbsp;</td>
                </tr>
                <?php } ?>
                
                <!--Deposited-->
                
                                <?php
                    $sql="SELECT
                                fin_transactions.entryDate,
                                fin_transactions.strProgramType,
                                fin_transactions.paymentNumber,
                                fin_transactions_details.presentedDate,
                                fin_transactions_details.dimensionId,
                                fin_transactions_details.amount,
                                fin_transactions.documentNo,
                                fin_transactions_details.details,
                                fin_transactions.personType,
                                fin_transactions.personId,
                                fin_transactions.entryId
                            FROM
                                fin_transactions_details
                                INNER JOIN fin_bank_rec_details ON fin_bank_rec_details.entryId = fin_transactions_details.detailId
                                INNER JOIN fin_transactions ON fin_transactions.entryId = fin_transactions_details.entryId
                            WHERE
                                fin_bank_rec_details.recId = $recId AND
                                fin_transactions_details.`credit/debit` = 'D'";
                    $resul = $db->RunQuery($sql);
                    while($row=mysqli_fetch_array($resul)){
						$booDebitTotal = true;
                         //get Customer/Supplier name
                        $name=getHolerName($row['personType'],$row['personId']);
                        $mainEntId=$row['entryId'];
                ?>
                <tr>
                    <td><?php echo $row['entryDate'] ?></td>
                    <td><a class="drillLink" onclick="leadgerDrill('<?php echo ($row['strProgramType']) ?>','<?php echo $row['documentNo']?>')"><?php echo $row['documentNo'] ?></a></td>
                    <td><?php echo $row['strChequeNo'] ?></td>
                    <td><?php echo $row['srtTransDetails'] ?></td>
                    <td><?php echo $name ?></td>                   
                    <td><?php 
                            $arr=  oppcitAccountsMulty($row['strCredit/Debit'],$mainEntId);
                            for($i=0; $i<count($arr); ++$i){
                                echo $i+1;
                                echo '.)';
                                echo $arr[$i].'<br /><br />';
                            }
                        ?>
                    </td>
                  <td><?php echo ($row['presentedDate']=='null'?"":$row['presentedDate'])?></td>
                  <td class="rptTblBody">&nbsp;</td>
                  <td class="rptTblBody"><?php echo $row['amount'] ?></td>
                    
                </tr>
                <?php 
                $debitTot+=$row['amount'];
                } ?>
                <?php if($booDebitTotal){?>
                <tr>
                    <td colspan="6">Total</td>
                    <td class="rptTblBody">&nbsp;</td>
                    <td class="rptTblBody"><?php echo number_format($ceditTot,4) ?></td>
                    <td class="rptTblBody"><?php echo number_format($debitTot,4) ?></td>
                </tr>
                <?php } ?>
            </table>
            <br/><br/>
        </div>
    </body>
</html>
<?php
function oppcitAccountsMulty($cdType, $entryNumber) {
    global $db;
    try {
        $sqlOp = "SELECT
                    (fin_transactions_details.amount) AS am,
                    mst_financechartofaccounts.strName AS ledName
                FROM
                    fin_transactions
                    INNER JOIN fin_transactions_details ON fin_transactions.entryId = fin_transactions_details.entryId
                    INNER JOIN mst_financechartofaccounts ON fin_transactions_details.accountId = mst_financechartofaccounts.intId
                WHERE
                    fin_transactions_details.`credit/debit` <> '$cdType' AND
                    fin_transactions_details.entryId = '$entryNumber'";
        $resultOP12 = $db->RunQuery($sqlOp);
        $arr = array();
        while ($rowOp = mysqli_fetch_array($resultOP12)) {
            $s = $rowOp['ledName'] . ' - ' . $rowOp['am'];
            $arr[] = $s;
        }
    } catch (Execption $e) {
        echo $e->getMessage();
        $arr[] = $e->getMessage();
        return $arr;
    }
    return $arr;
}

//get Customer or sublier name
function getHolerName($personType,$personId){
    //$stakeHolderType="supplier";$PearentHolder=9;
    global $db;
    if($personType=="sup"){
        $sql="SELECT mst_supplier.strName AS name FROM mst_supplier WHERE mst_supplier.intId = $personId";
    }
    else if($personType=="cus"){
        $sql="SELECT mst_customer.strName AS name FROM mst_customer WHERE mst_customer.intId = $personId";
    }
    else $sql=null;
    
    $name="-";
    if($sql!=null){
        $result = $db->RunQuery($sql);
        $row = mysqli_fetch_array($result);
        $name=$row['name'];
    }
    return $name;
}

?>
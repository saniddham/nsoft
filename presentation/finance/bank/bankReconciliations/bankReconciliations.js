var loadRecId = '';
//BEGIN - FORM BUTTION PERMISSION VALIDATION {
if(intAddx)
{
    $('#frmBankRec #butNew').show();
    $('#frmBankRec #butSave').show();
}
//permision for edit 
if(intEditx)
{
    $('#frmBankRec #butSave').show();
    $('#frmBankRec #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
}
//permision for delete
if(intDeletex)
{
    $('#frmBankRec #butDelete').show();
    $('#frmBankRec #cboSearch').removeAttr('disabled');
}
//permision for view
if(intViewx)
{
    $('#frmBankRec #cboSearch').removeAttr('disabled');
}
//END	- FORM BUTTION PERMISSION VALIDATION }

function alertx()
{
    $('#frmBankRec #butSave').validationEngine('hide') ;
}

$(document).ready(function() {
    
    $("#frmBankRec").validationEngine();
    
    //save new reconsile and load details
    $('#frmBankRec #butSave1').click(function(){        
        if ($('#frmBankRec').validationEngine('validate')) {
            saveRec();//save rec
            loadExisting();//load excisting comboBox
			$('#frmBankRec #cboSearch').val(loadRecId); //added by lasantha @ CAIT on 22/01/2013
        }
    });
    //load details
    $('#frmBankRec #cboSearch').change(function(){
		showWaiting();       
        var recId=$('#frmBankRec #cboSearch').val();
        loadRecDetails(recId);
		hideWaiting();
    });
    //Leave mail
    $('#frmBankRec #btnLeave').click(function(){        
        leavePage();
    });
    //Reconsile
    $('#frmBankRec #btnRec').click(function(){        
        reconsile();
    });
	//New
    $('#frmBankRec #butNew').click(function(){        
        $('#frmBankRec').get(0).reset();
		$('#frmBankRec #tblDebits').html('');
        $('#frmBankRec #tblCredits').html('');
		window.location.href = window.location.href;
    });
    // reconsile and exceisting changer
    $('#frmBankRec #chkRecon').click(function(){        
       var chkBool=$('#frmBankRec #chkRecon').attr("checked");
       if(chkBool){
           loadRecomsiled();           
       }
       else{
           loadExisting();           
       }
    });
    //detate reconcile 
    $('#frmBankRec #butDelete').click(function(){         
        var recId=$('#frmBankRec #cboSearch').val()
        deleteRec(recId);
    });
    //detate reconcile 
    $('#frmBankRec #butReport').click(function(){         
        showRecReport();
    });
	
	//BEGIN	- 
	$('#butLoadDebitGrid').live('click',function(){
			ReLoadGrid('D');
		});
	$('#butLoadCreditGrid').live('click',function(){
			ReLoadGrid('C');
		});
		
	$('#tblMainGrid2 .cls_debit').live('click',CalculateEndBalance);
	$('#tblMainGrid .cls_credit').live('click',CalculateEndBalance);
	
	$('.cls_loadGrid').live('change',LoadGrid);
	$('.cls_loadGrid_click').live('click',LoadGrid);
	
	$('.cls_beginBalance').live('keyup',function(){
		var balAmount = $(this).val()==''?0:$(this).val();
		$('#bBal').val(balAmount);
		CalculateEndBalance();
		});
		
	$('#frmBankRec #butSave').live('click',Save);
	$('.cls_debit').live('click',AddClass_Debit);
	$('.cls_credit').live('click',AddClass_Credit);
	//END - 
});

function AddClass_Debit()
{
	if($(this).is(':checked'))
		$(this).parent().parent().find('.cls_debitDate').children('.cls_debitDateInput').addClass('validate[required]');
	else
		$(this).parent().parent().find('.cls_debitDate').children('.cls_debitDateInput').removeClass('validate[required]');
}

function AddClass_Credit()
{
	if($(this).is(':checked'))
		$(this).parent().parent().find('.cls_creditDate').children('.cls_creditDateInput').addClass('validate[required]');
	else
		$(this).parent().parent().find('.cls_creditDate').children('.cls_creditDateInput').removeClass('validate[required]');
}

function saveRec(){
    var recId=$('#frmBankRec #cboSearch').val();   
    var requestType="";
    if(recId==""){
        requestType="add";
    }
    else{
        requestType="edit";
    }
    var accId=$('#frmBankRec #cmbAccount').val();
    var currencyId=$('#frmBankRec #cboCurrency').val();
    var startDate=$('#frmBankRec #startDate').val();
    var endDate=$('#frmBankRec #endDate').val();
    var stDate=$('#frmBankRec #statDate').val();
    var biginBal=$('#frmBankRec #biginBal').val();
    var endBal=$('#frmBankRec #endBal').val();
    var accName=$("#frmBankRec #cmbAccount option:selected").text();    
    var params="&accId="+accId + "&currencyId="+currencyId + "&startDate="+startDate + "&endDate="+endDate + "&stDate="+stDate + "&biginBal="+biginBal + "&endBal="+endBal+"&accName="+accName+"&recId="+recId;
    var url="bankReconciliations-db-set.php";
    
    var obj = $.ajax({
                url:url,
                dataType: "json",  
                data:$("#frmBankRec").serialize()+'&requestType='+requestType+params,
                async:false,
			
                success:function(json){
                    $('#frmBankRec #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
                    if(json.type=='pass')
                    {
                        //$('#frmBankRec').get(0).reset();						
                        var t=setTimeout("alertx()",1000); 
                        var recId=json.recId;
                        loadRecDetails(recId);                        
                        return;
                    }
                    var t=setTimeout("alertx()",3000);
                },
                error:function(xhr,status){					
                    $('#frmBankRec #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
                    var t=setTimeout("alertx()",3000);
                }		
            });     
}

//load the existing combobox
function loadExisting(){
    var url ='bankReconciliations-db-get.php?requestType=loadCombo';
            var obj=$.ajax({
                url:url,
                dataType:'json',
                success:function(json){
                    $('#frmBankRec #cboSearch').html(json.recList);
                    desableAll(false);
                },
                async:false
            }); 
}
//load the Recomsiled to combobox
function loadRecomsiled(){
    var url ='bankReconciliations-db-get.php?requestType=loadRecsCombo';
            var obj=$.ajax({
                url:url,
                dataType:'json',
                success:function(json){
                    $('#frmBankRec #cboSearch').html(json.recList);
                    desableAll(true);
                },
                async:false
            }); 
}
//load details
function loadRecDetails(recId){   
    if(recId!=""){
        var url ='bankReconciliations-db-get.php?requestType=loadDetails&recId='+recId;
            var obj=$.ajax({
                url:url,
                dataType:'json',
                success:function(json){
                    //header details
                    $('#frmBankRec #cmbAccount').val(json.accId); 
                    $('#frmBankRec #cboCurrency').val(json.currencyId);
                    $('#frmBankRec #startDate').val(json.startDate);
                    $('#frmBankRec #endDate').val(json.endDate);
                    $('#frmBankRec #statDate').val(json.statmentDate);
                    $('#frmBankRec #biginBal').val(json.biginBal);
                    $('#frmBankRec #endBal').val(json.endBalance);  
                    //debit details
                    $('#frmBankRec #tblDebits').html(json.debitList);
                    $('#frmBankRec #tblCredits').html(json.creditList);
                    
                    //reportDetails
                    $('#frmBankRec #txtClearBal').val(json.cleardBalance);
                    $('#frmBankRec #txtDif').val(json.differance);
                    $('#frmBankRec #bBal').val(json.biginBal);
                    $('#frmBankRec #eBal').val(json.endBalance); 
                    $('#frmBankRec #cboSearch').val(recId)
					loadRecId = recId; //added by lasantha @ CAIT on 22/01/2013
					$('#frmBankRec #txtNo').val(json.recName)                    
                },
                async:false
            });         
    } 
    else{
        $('#frmBankRec').get(0).reset();
		$('#frmBankRec #tblDebits').html('');
        $('#frmBankRec #tblCredits').html('');
    }
}

function debitCheck(sender){
	return;
    var txtName='#debAmount'+sender.value;
    var amount= parseFloat($(txtName).val());;
    var clearBalance= parseFloat($('#frmBankRec #txtClearBal').val());
    var endBalance= parseFloat($('#frmBankRec #eBal').val());    
    if(sender.checked){
        clearBalance+=amount;
    }
    else{
        clearBalance-=amount;
    } 
    var diffBalance=endBalance-clearBalance;
    $('#frmBankRec #txtClearBal').val(clearBalance.toFixed(4));
    $('#frmBankRec #txtDif').val(diffBalance.toFixed(4));
}

function creditCheck(sender){
	return;
    var txtName='#creAmount'+sender.value;
    var amount= parseFloat($(txtName).val());;
    var clearBalance= parseFloat($('#frmBankRec #txtClearBal').val());
    var endBalance= parseFloat($('#frmBankRec #eBal').val()); 
    if(sender.checked){
        clearBalance-=amount;
    }
    else{
        clearBalance+=amount;
    } 
    $('#frmBankRec #txtClearBal').val(clearBalance.toFixed(4));
    var diffBalance=endBalance-clearBalance;
    $('#frmBankRec #txtDif').val(diffBalance.toFixed(4));
}
function leavePage(){
    
    var params="";
    var debits=document.getElementsByName("chkDebit");    
    for(var i=0; i< debits.length;++i){
			var txtName='#preDate'+debits[i].value;
            var presentDate=$(txtName).val();
         params+="&debitDetails["+i+"][entId]="+debits[i].value;
         params+="&debitDetails["+i+"][status]="+debits[i].checked;
		  params+="&debitDetails["+i+"][preDate]="+presentDate; //added by lasantha @ CAIT on 22/01/2013
    }
    
    var credits=document.getElementsByName("chkCredit");
    for(var i=0; i< credits.length;++i){
			var txtName1='#preDate'+credits[i].value;
            var presentDate1=$(txtName1).val();
         params+="&creditDetails["+i+"][entId]="+credits[i].value;
         params+="&creditDetails["+i+"][status]="+credits[i].checked;  
		 params+="&creditDetails["+i+"][preDate]="+presentDate1; //added by lasantha @ CAIT on 22/01/2013       
    }
    var url = "bankReconciliations-db-set.php";
            var obj = $.ajax({
                url:url,
                dataType: "json",  
                data:$("#frmBankRec").serialize()+'&requestType=leave'+params,
                async:false,
			
                success:function(json){
                    $('#frmBankRec #btnLeave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
                    if(json.type=='pass')
                    {
                        $('#frmBankRec').get(0).reset();						
                        var t=setTimeout("alertx()",1000);                        
                        $('#frmBankRec #tblDebits').html('');
                        $('#frmBankRec #tblCredits').html('');
                        return;
                    }
                    var t=setTimeout("alertx()",3000);
                },
                error:function(xhr,status){					
                    $('#frmBankRec #btnLeave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
                    var t=setTimeout("alertx()",3000);
                }		
            }); 
    
}
function reconsile(){
	showWaiting();
    var recId=$('#frmBankRec #cboSearch').val();
    
    var diffBalance=parseFloat($('#frmBankRec #txtDif').val());   
    if(recId == ""){
		 $('#frmBankRec #cboSearch').validationEngine('showPrompt', "Please select the 'Bank Reconciliation Group'",'fail');
		 $('#frmBankRec #cboSearch').focus();
		 hideWaiting();
		 return;
	}
        var params="&recId="+recId;
        var debits=document.getElementsByName("chkDebit");    
        for(var i=0; i< debits.length;++i){
            
            var txtName='#preDate'+debits[i].value;
            var presentDate=$(txtName).val();
            
            params+="&debitDetails["+i+"][entId]="+debits[i].value;
            params+="&debitDetails["+i+"][status]="+debits[i].checked;
            params+="&debitDetails["+i+"][preDate]="+presentDate;
        }
    
        var credits=document.getElementsByName("chkCredit");
        for(var i=0; i< credits.length;++i){
            
            var txtName1='#preDate'+credits[i].value;
            var presentDate1=$(txtName1).val();
            
            params+="&creditDetails["+i+"][entId]="+credits[i].value;
            params+="&creditDetails["+i+"][status]="+credits[i].checked;         
            params+="&creditDetails["+i+"][preDate]="+presentDate1;         
        }
		var url = "bankReconciliations-db-set.php";
		var obj = $.ajax({
			url:url,
			dataType: "json",  
			data:$("#frmBankRec").serialize()+'&requestType=rec'+params,
			async:false,
		
			success:function(json){
				$('#frmBankRec #btnRec').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
				if(json.type=='pass')
				{
					$('#frmBankRec').get(0).reset();						
					var t=setTimeout("alertx()",1000);                        
					$('#frmBankRec #tblDebits').html('');
					$('#frmBankRec #tblCredits').html('');
					loadExisting();
					return;
				}
				var t=setTimeout("alertx()",3000);
			},
			error:function(xhr,status){					
				$('#frmBankRec #btnRec').validationEngine('showPrompt', errormsg(xhr.status),'fail');
				var t=setTimeout("alertx()",3000);
			}		
		});
		hideWaiting();  
}

//delete reconsile
function deleteRec(recId){
    if(recId!=""){
        var url = "bankReconciliations-db-set.php";
            var obj = $.ajax({
                url:url,
                dataType: "json",  
                data:$("#frmBankRec").serialize()+'&requestType=deleteRec&recId='+recId,
                async:false,
			
                success:function(json){
                    $('#frmBankRec #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
                    if(json.type=='pass')
                    {
                        $('#frmBankRec').get(0).reset();						
                        var t=setTimeout("alertx()",1000);                 
                        loadExisting();
                        return;
                    }
                    var t=setTimeout("alertx()",3000);
                },
                error:function(xhr,status){					
                    $('#frmBankRec #butDelete').validationEngine('showPrompt', errormsg(xhr.status),'fail');
                    var t=setTimeout("alertx()",3000);
                }		
            }); 
    }
    else{
        $('#frmBankRec #butDelete').validationEngine('showPrompt', "select report",'fail');
    }
    
}

function desableAll(status){
    var opa=false;
     $('#frmBankRec #cmbAccount').attr('disabled',status);
     $('#frmBankRec #cboCurrency').attr('disabled',status);
     $('#frmBankRec #startDate').attr('disabled',status);
     $('#frmBankRec #startDate').attr('disabled',status);
     $('#frmBankRec #endDate').attr('disabled',status);
     $('#frmBankRec #statDate').attr('disabled',status);
     $('#frmBankRec #biginBal').attr('disabled',status);
     $('#frmBankRec #endBal').attr('disabled',status);
     $('#frmBankRec #tblDebits').attr('disabled',status);
     $('#frmBankRec #tblCredits').attr('disabled',status);
   //  $('#frmBankRec #btnRec').attr('disabled',status);
     $('#frmBankRec #btnLeave').attr('disabled',status);
     $('#frmBankRec #butSave').attr('disabled',status);
     if(status==true){
         opa=false;
     }
     else{
        opa=true;
     }
     $('#frmBankRec #butReport').attr('disabled',opa);
}
function showRecReport(){
    var recId=$('#frmBankRec #cboSearch').val();
    if(recId==""){
        $('#frmBankRec #butReport').validationEngine('showPrompt', "select reconcilation",'fail');
    }
    else{        
        var url="rpt_reconsile.php?recId="+recId;
        window.open(url);
    }
    
}

function ReLoadGrid(type)
{
	var recId=$('#frmBankRec #cboSearch').val();
    if(recId!=""){
        var url ='bankReconciliations-db-get.php?requestType=loadDetails&recId='+recId;
            var obj=$.ajax({
                url:url,
                dataType:'json',
                success:function(json){
					if(type=='D')
                    	$('#frmBankRec #tblDebits').html(json.debitList);
					
					if(type=='C')
                    	$('#frmBankRec #tblCredits').html(json.creditList);
                },
                async:false
            });         
    } 
    else{
        $('#frmBankRec').get(0).reset();
		$('#frmBankRec #tblDebits').html('');
        $('#frmBankRec #tblCredits').html('');
    }
}

function CalculateEndBalance()
{
	var biginBalance = $('#frmBankRec #biginBal').val()==''?0:parseFloat($('#frmBankRec #biginBal').val());
	var credit 	= 0;
	var debit 	= 0;
	
	$('#tblMainGrid2 .cls_debit:checked').each(function(){
		debit += parseFloat($(this).parent().parent().find('.cls_debitAmount').children().val());
		});
		
	$('#tblMainGrid .cls_credit:checked').each(function(){
		credit += parseFloat($(this).parent().parent().find('.cls_creditAmount').children().val());
		});
		
	
	
	var amount = (biginBalance + debit) - credit;
	$('#frmBankRec #eBal').val(amount);
}

function LoadGrid()
{
	var url 	= "bankReconciliations-db-get.php?requestType=URLLoadGrid";
	var data 	= "Account="+$('#cmbAccount').val();
		data   += "&Currency="+$('#cboCurrency').val();

	var obj		= $.ajax({
		url:url,
		dataType:'json',
		type:'post',
		data:data,
		success:function(json){
			$('#frmBankRec #tblDebits').html(json.debitList);
			$('#frmBankRec #tblCredits').html(json.creditList);
			$('#frmBankRec #biginBal').val(json.OpeningBal);
			$('#frmBankRec #bBal').val(json.OpeningBal);
			$('#frmBankRec #eBal').val(json.OpeningBal);
			},
		async:false
	});
}

function Save()
{
	if(!$('#frmBankRec').validationEngine('validate'))
		return;
	showWaiting();	
	var recId	 = $('#frmBankRec #cboSearch').val();
	var url 	 = "bankReconciliations-db-set.php?requestType=URLSave";
	var data	 = "RecId="+recId;
		data 	+= "&AccountId="+$('#frmBankRec #cmbAccount').val();
		data 	+= "&AccountName="+$("#frmBankRec #cmbAccount option:selected").text();
	 	data 	+= "&CurrencyId="+$('#frmBankRec #cboCurrency').val();
	 	data 	+= "&StartDate="+$('#frmBankRec #startDate').val();
		data 	+= "&EndDate="+$('#frmBankRec #endDate').val();
		data 	+= "&OpeningBal="+$('#frmBankRec #bBal').val();
		data 	+= "&ClosingBal="+$('#frmBankRec #eBal').val();
	
	var debits=document.getElementsByName("chkDebit");    
	for(var i=0; i< debits.length;++i){
		
		var txtName		= '#preDate'+debits[i].value;
		var presentDate	= $(txtName).val();
		
		data	+= "&debitDetails["+i+"][entId]="+debits[i].value;
		data	+= "&debitDetails["+i+"][status]="+debits[i].checked;
		data	+= "&debitDetails["+i+"][preDate]="+presentDate;
	}

	var credits=document.getElementsByName("chkCredit");
	for(var i=0; i< credits.length;++i){
		
		var txtName1	= '#preDate'+credits[i].value;
		var presentDate1= $(txtName1).val();
		
		data	+= "&creditDetails["+i+"][entId]="+credits[i].value;
		data	+= "&creditDetails["+i+"][status]="+credits[i].checked;         
		data	+= "&creditDetails["+i+"][preDate]="+presentDate1;         
	}
		
	var obj = $.ajax({
			url:url,
			dataType: "json",  
			data:data,
			async:false,		
			success:function(json){
				$('#frmBankRec #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
				if(json.type=='pass')
				{
					var t=setTimeout("alertx()",1000); 
					var recId=json.recId;
					hideWaiting();  
					return;
				}
			},
			error:function(xhr,status){					
				$('#frmBankRec #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
				var t=setTimeout("alertx()",3000);
				hideWaiting();  
			}		
		}); 
		hideWaiting();  
}
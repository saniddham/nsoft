<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$locationId 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	///
	/////////// type of print load part /////////////////////
	if($requestType=='loadCombo')
	{
		$sql = "SELECT DISTINCT
				fin_bankpettycash_header.strPettyCashNo,
				IFNULL(CONCAT(' - ',fin_bankpettycash_header.strFnRefNo),'') AS refNo
				FROM fin_bankpettycash_header
				WHERE
				fin_bankpettycash_header.intStatus =  '1' AND
				fin_bankpettycash_header.intCompanyId =  '$company'
				order by strPettyCashNo desc
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['strPettyCashNo']."\">".$row['strPettyCashNo'].$row['refNo']."</option>";
		}
		echo $html;
	}
	else if($requestType=='loadExchangeRates')
	{
		$currency  = $_REQUEST['currency'];
		$date  = $_REQUEST['date'];
		$sql = "SELECT
				mst_financeexchangerate.dblBuying, 
				mst_financeexchangerate.dblSellingRate  
				FROM mst_financeexchangerate
				WHERE
				mst_financeexchangerate.intCurrencyId =  '$currency' and mst_financeexchangerate.dtmDate =  '$date'";
		$result = $db->RunQuery($sql);
		
		$response['sellingRate'] = '';
		$response['buyingRate'] = '';
		while($row=mysqli_fetch_array($result))
		{
			$response['sellingRate'] = $row['dblSellingRate'];
			$response['buyingRate'] = $row['dblBuying'];
		}
		echo json_encode($response);
	}
	else if($requestType=='loadCustORsuppliers')
	{
		$account  = $_REQUEST['account'];
		$sql = "SELECT   
				mst_financechartofaccounts.intId, 
				mst_financialsubtype.strName
				FROM
				mst_financialsubtype
				Inner Join mst_financechartofaccounts ON mst_financialsubtype.intId = mst_financechartofaccounts.intFinancialTypeId
				WHERE
				mst_financechartofaccounts.intId =  '$account'";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		
		$chartOfAcc=$row['intId'];
		if($row['strName']=='Accounts Receivable'){
		$sql = "SELECT
				mst_financecustomeractivate.intCustomerId as id,
				mst_customer.strName
				FROM
				mst_financecustomeractivate
				Inner Join mst_customer ON mst_financecustomeractivate.intCustomerId = mst_customer.intId
				WHERE
				mst_financecustomeractivate.intCompanyId =  '$company' AND
				mst_financecustomeractivate.intChartOfAccountId =  '$chartOfAcc'
				ORDER BY strName";
		}
		else if($row['strName']=='Accounts Payable'){
		$sql = "SELECT
				mst_financesupplieractivate.intSupplierId as id,
				mst_supplier.strName
				FROM
				mst_financesupplieractivate
				Inner Join mst_supplier ON mst_financesupplieractivate.intSupplierId = mst_supplier.intId
				WHERE
				mst_financesupplieractivate.intCompanyId =  '$company' AND
				mst_financesupplieractivate.intChartOfAccountId =  '$chartOfAcc'
				ORDER BY strName";
		}
		else if($row['strName']=='Lease' || $row['strName']=='Loan' || $row['strName']=='Other Non Current Liabilities' || $row['strName']=='Other Current Liabilities'){
		$sql = "SELECT
				mst_finance_service_supplier_activate.intSupplierId as id,
				mst_finance_service_supplier.strName
				FROM
				mst_finance_service_supplier_activate
				Inner Join mst_finance_service_supplier ON mst_finance_service_supplier_activate.intSupplierId = mst_finance_service_supplier.intId
				WHERE
				mst_finance_service_supplier_activate.intCompanyId =  '$company' AND
				mst_finance_service_supplier_activate.intChartOfAccountId =  '$chartOfAcc'
				ORDER BY strName";
		}
		else if($row['strName']=='Interest In suspenses' || $row['strName']=='Prepayments' || $row['strName']=='Inter Company' || $row['strName']=='Other Current Assets' || $row['strName']=='Other Non - Current Assets'){
		$sql = "SELECT
				mst_finance_service_customer_activate.intCustomerId as id,
				mst_finance_service_customer.strName
				FROM
				mst_finance_service_customer_activate
				Inner Join mst_finance_service_customer ON mst_finance_service_customer_activate.intCustomerId = mst_finance_service_customer.intId
				WHERE
				mst_finance_service_customer_activate.intCompanyId =  '$company' AND
				mst_finance_service_customer_activate.intChartOfAccountId =  '$chartOfAcc'
				ORDER BY strName";
		}
		else{
		$sql = "";	
		}
		//echo $sql;
		
		$result = $db->RunQuery($sql);
		$combo="<option value=\"\"></option>";
		
		while($rows=mysqli_fetch_array($result))
		{
			$combo .= "<option value=\"".$rows['id']."\">".$rows['strName']."</option>";	

		}
		$response['recievdFrom'] = $combo;
			
		echo json_encode($response);
	}
	else if($requestType=='getTaxValue')
	{
		$operation  = $_REQUEST['opType'];
		$amount  = $_REQUEST['valAmount'];
		$taxCodes = json_decode($_REQUEST['arrTaxCode'], true);
		
		if(count($taxCodes) != 0)
		{
			foreach($taxCodes as $taxCode)
			{
				$codeValues[] = callTaxValue($taxCode['taxId']);
			}
		}
		if(count($codeValues) > 1)
		{
			if($operation == 'Inclusive')
			{
				$firstVal = ($amount*$codeValues[0])/100;
				$withTaxVal = $firstVal + ((($amount+$firstVal)*$codeValues[1])/100);
				$val1 = ($amount*$codeValues[0])/100;
				$val2 = ((($amount+$firstVal)*$codeValues[1])/100);
			}
			else if($operation == 'Exclusive')
			{
				$withTaxVal = ($amount*($codeValues[0] + $codeValues[1]))/100;
				$val1 = ($amount*$codeValues[0])/100;
				$val2 = ($amount*$codeValues[1])/100;
			}
		}
		else if(count($codeValues) == 1 && $operation == 'Isolated')
		{
			$withTaxVal = ($amount*$codeValues[0])/100;
			$val1 = ($amount*$codeValues[0])/100;
		}
		echo $withTaxVal."/".$val1."/".$val2;
	}
	else if($requestType=='getTaxProcess')
	{
		$taxGrpId  = $_REQUEST['taxGroupId'];
		
		$sql = "SELECT
				mst_financetaxgroup.intId,
				mst_financetaxgroup.strProcess
				FROM
				mst_financetaxgroup
				WHERE
				mst_financetaxgroup.intId =  '$taxGrpId'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		echo $row['strProcess'];
	}
	function callTaxValue($taxId)
	{
		global $db;
		$sql = "SELECT
				mst_financetaxisolated.intId,
				mst_financetaxisolated.strCode,
				mst_financetaxisolated.dblRate
				FROM
				mst_financetaxisolated
				WHERE
				mst_financetaxisolated.intId = '$taxId'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$taxVal = $row['dblRate'];	
		return $taxVal;
	}
?>
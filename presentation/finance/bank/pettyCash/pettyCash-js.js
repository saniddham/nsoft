// JavaScript Document
var amStatus = "Auto";

function functionList()
{
		$('.taxGroup').change();
	//---------------------------------------------------
		amStatus = "Auto";
		//document.getElementById("chkAutoManual").style.display='none';
		document.getElementById("amStatus").style.display='none';
		$("#frmPettyCash #txtVoucherNo").attr("readonly","readonly");
	//---------------------------------------------------
	if($('#frmPettyCash #cboSearch').val()=='')
	{
	//---------------------------------------------------
		existingMsgDate = "";
		amStatus = "Auto";
		//document.getElementById("chkAutoManual").style.display='';
		document.getElementById("amStatus").style.display='';
		$('#frmPettyCash #txtVoucherNo').removeClass('validate[required]');
		$("#frmPettyCash #txtVoucherNo").attr("readonly","readonly");
	//---------------------------------------------------
	}
}

$(document).ready(function() {
	
  		$("#frmPettyCash").validationEngine();
		$('#frmPettyCash #cboPettyCashAccount').focus();
		
		$("#insertRow").click(function(){
			insertRow();
		});
		//-----------------------------------
		$('.accounts').change(function(){
			loadRecievedFrom(this);
		});
		//-------------------------------
		$('.delImg').click(function(){
			var rowCount = document.getElementById('tblPettyCash').rows.length;
			if(rowCount>2)
			$(this).parent().parent().remove();
			calTax(this);
			
		});
		//--------------------------------------- 
		$('.taxGroup').live('change',function(){
			calTax(this);
		});
		//--------------------------------------- 
		$('.ammount').keyup(function(){
			calTax(this);
		});
		//-------------------------------------- 
		
	//--------------------------------------------
		
  //permision for add 
  if(intAddx)
  {
 	$('#frmPettyCash #butNew').show();
	$('#frmPettyCash #butSave').show();
	$('#frmPettyCash #butPrint').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmPettyCash #butSave').show();
	//$('#frmPettyCash #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
	$('#frmPettyCash #butPrint').show();
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmPettyCash #butDelete').show();
	//$('#frmPettyCash #cboSearch').removeAttr('disabled');
  }
  
  //permision for view
  if(intViewx)
  {
	//$('#frmPettyCash #cboSearch').removeAttr('disabled');
  }
//===================================================================
 	$('#frmPettyCash #chkAutoManual').click(function(){
	  if($('#frmPettyCash #chkAutoManual').attr('checked'))
	  {
		  amStatus = "Auto";
		  $('#frmPettyCash #amStatus').val('Auto');
		  $('#frmPettyCash #txtVoucherNo').val('');
		  $("#frmPettyCash #txtVoucherNo").attr("readonly","readonly");
		  $('#frmPettyCash #txtVoucherNo').removeClass('validate[required]');
	  }
	  else
	  {
		  amStatus = "Manual";
		  $('#frmPettyCash #amStatus').val('Manual');
		  $('#frmPettyCash #txtVoucherNo').val('');
		  $("#frmPettyCash #txtVoucherNo").attr("readonly","");
		  $('#frmPettyCash #txtVoucherNo').focus();
		  $('#frmPettyCash #txtVoucherNo').addClass('validate[required]');
	  }
  });
//===================================================================
  
 //-------------------------------------------- 
  $('#frmPettyCash #cboCurrency').change(function(){
	    var currency = $('#cboCurrency').val();
	    var date = $('#txtDate').val();
		var url 		= "pettyCash-db-get.php?requestType=loadExchangeRates";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"currency="+currency+'&date='+date,
			async:false,
			success:function(json){

					document.getElementById("txtRate").value=json.sellingRate;
					document.getElementById("exchSelling").value=json.sellingRate;
					document.getElementById("exchBuying").value=json.buyingRate;
					document.getElementById("rdoAverage").value=((eval(json.buyingRate) + eval(json.sellingRate))/2).toFixed(4);
					//document.getElementById("txtRate").innerHTML=json.buyingRate;
			}
		});
		
  });
  //-------------------------------------------------------
  $('#frmPettyCash #exchSelling').click(function(){
					document.getElementById("txtRate").value=document.getElementById("exchSelling").value;
					//document.getElementById("txtRate").innerHTML=json.buyingRate;
  });
  //-------------------------------------------------------
  $('#frmPettyCash #exchBuying').click(function(){
		document.getElementById("txtRate").value=document.getElementById("exchBuying").value;
  });
  //-------------------------------------------------------------
  //-------------------------------------------------------
  $('#frmPettyCash #rdoAverage').click(function(){
		document.getElementById("txtRate").value=document.getElementById("rdoAverage").value;
  });
  //-------------------------------------------------------------
  $('#frmPettyCash #chkEdit').click(function(){
	  if(document.getElementById("chkEdit").checked==true)
		$('#frmPettyCash #txtRate').removeAttr('disabled');
		else
		$('#frmPettyCash #txtRate').attr("disabled",true);

  });
  //------------------------------------------------------------
   $('#frmPettyCash #cboPaymentMethod').change(function(){
	    var payMethod = $('#cboPaymentMethod').val();
		if(payMethod==2){
		document.getElementById("rwChequeDetails").style.display='';
		}
		else{
		document.getElementById("rwChequeDetails").style.display='none';
		}
  });
	//----------------------------------------------------------
	$('.calTot').keyup(function(){
		calculateTotalVal();
	});
	//-----------------------------------------------------------
    $('#frmPettyCash #butDelete').click(function(){
		var serial=URLEncode($('#frmPettyCash #txtVoucherNo').val());
		if($('#frmPettyCash #txtVoucherNo').val()=='')
		{
			//$('#frmBankDeposit #butDelete').validationEngine('showPrompt', 'Please select Deposit No.', 'fail');
		//	var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+serial+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = "pettyCash-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											data:'requestType=delete&serialNo='+serial,
											async:false,
											success:function(json){
												
												$('#frmPettyCash #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												if(json.type=='pass')
												{
													$('#frmPettyCash').get(0).reset();
													clearForm();
													loadCombo_search();
													var t=setTimeout("alertDelete()",1000);return;
												}
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
		
	//	clearForm();
	});
	//-----------------------------------------------------------
  $('#frmPettyCash #butSave').click(function(){
  if(existingMsgDate == "")
  {
	//  calculateTotalVal();
	  var RecvedAmmount=parseFloat(document.getElementById("txtRecvAmmount").value).toFixed(2); 
	  var totAccQty=callTotandTax().toFixed(2);
	  
	var requestType = '';
	if ($('#frmPettyCash').validationEngine('validate'))   
    { 
		  if(checkForDuplicate()==false){
			return false;  
		  }
		  else if(RecvedAmmount!=totAccQty){
			alert('Recieved Ammount does not tally with total Account Ammount');
			return false;  
		  }
		  if(document.getElementById('chkPosted').checked==true)
			var posted = 1;
		  else
			var posted = 0;
		  

		var data = "requestType=save";
		
			data+="&serialNo="		+	URLEncode($('#cboSearch').val());
			data+="&bankAccount="	+	$('#cboPettyCashAccount').val();
			data+="&currency="	+	$('#cboCurrency').val();
			data+="&recvAmmount="			+	$('#txtRecvAmmount').val();
			data+="&ammountsInWords=''";
			data+="&paymentMethod="		+	$('#cboPaymentMethod').val();
			data+="&txtpaymentMethod="		+	$('#cboPaymentMethod').text();
			data+="&posted="	+	posted;
			data+="&remarks="			+	URLEncode($('#txtRemarks').val());
			data+="&date="		+	$('#txtDate').val();
			data+="&refDate="		+	$('#txtRefDate').val();
			data+="&rate="		+	$('#txtRate').val();
			data+="&refNo="	+	$('#txtRafNo').val();
			data+="&refOrganization=" +	$('#txtRefOrganization').val();
			data+="&amStatus="		+	amStatus;
			data+="&manualNo="		+	$('#txtVoucherNo').val();
			
			data+="&fnRefNo="		+	URLEncode($('#txtFnRefNo').val());


			var rowCount = document.getElementById('tblPettyCash').rows.length;
			if(rowCount==1){
				alert("No Accounts selected");
				return false;				
			}
			var row = 0;
			
			var arr="[";
			
			$('#tblPettyCash >tbody >tr:not(:eq(0))').each(function(){
					arr += "{";
					arr += '"account":"'+		$(this).find('#cboAccounts').val() +'",' ;
					arr += '"ammount":"'+	$(this).find('#txtAmmount').val() +'",' ;
					arr += '"memo":"'+		URLEncode($(this).find('#txtMemo').val()) +'",' ;
					arr += '"dimention":"'+		$(this).find('#cboDimention').val() +'",' ;
					arr += '"payTo":"'+		$(this).find('#cboPayTo').val() +'",' ;
					arr += '"taxId":"'+		$(this).find('#cboTax').val() +'",' ;
					arr += '"taxAmmount":"'+		$(this).find(".taxWith").val() +'",' ;
					arr += '"trnTaxVal":'+		$(this).find(".taxWith").attr('id') +"," ;
					arr += '"taxVal":"'+		$(this).find(".taxWith").val() +'"' ;
					arr +=  '},';
			});
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;
		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = "pettyCash-db-set.php";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			success:function(json){

					$('#frmPettyCash #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						//$('#frmSampleInfomations').get(0).reset();
						//saveImage(json.sampleNo,json.sampleYear,json.revisionNo);
						loadCombo_search();
						var t=setTimeout("alertx()",1000);
						$('#txtVoucherNo').val(json.serialNo);
					 	$('#cboSearch').val(json.serialNo);
						return;
					}
				},
			error:function(xhr,status){
					
					$('#frmPettyCash #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
	}
	else
	{
		$('#frmPettyCash #butSave').validationEngine('showPrompt', existingMsgDate,'fail');
		var t=setTimeout("alertx()",5000);
	}
   });
   

	//--------------refresh the form----------
	$('#frmPettyCash #butNew').click(function(){
	//---------------------------------------------------
		existingMsgDate = "";
		amStatus = "Auto";
		$("#frmPettyCash #txtVoucherNo").attr("readonly","readonly");
		$('#frmPettyCash #chkAutoManual').attr('checked')
		$("#frmPettyCash #chkAutoManual").attr("disabled","");
		//document.getElementById("chkAutoManual").style.display='';
		document.getElementById("amStatus").style.display='';
		$('#frmPettyCash #txtVoucherNo').removeClass('validate[required]');
	//--------------------------------------------------
		$('#frmPettyCash').get(0).reset();
		clearRows();
		$('#frmPettyCash #txtVoucherNo').val('');
		$('#frmPettyCash #cboSearch').val('');
		$('#frmPettyCash #cboPettyCashAccount').val('');
		$('#frmPettyCash #cboCurrency').val('');
		$('#frmPettyCash #txtRecvAmmount').val('');
		$('#frmPettyCash #txtAmntsInWords').val('');
		$('#frmPettyCash #cboPaymentMethod').val('');
		$('#frmPettyCash #chkPosted').val('');
		$('#frmPettyCash #txtRemarks').val('');
		$('#frmPettyCash #txtDate').val('');
		$('#frmPettyCash #txtRefDate').val('');
		$('#frmPettyCash #txtRate').val('');
		$('#frmPettyCash #txtRafNo').val('');
		$('#frmPettyCash #txtTotalTax').val(0);
		$('#frmPettyCash #txtTotal').val(0);
		$('#frmPettyCash #txtRefOrganization').val('');
		//$('#frmPettyCash #cboPettyCashAccount').focus();
		$('#frmPettyCash #txtFnRefNo').val('');
		$('#frmPettyCash #txtFnRefNo').focus();
		
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmPettyCash #txtDate').val(d);
		$('#frmPettyCash #txtRefDate').val(d);
		
		document.getElementById("rwChequeDetails").style.display='none';
	});
//----------------------------	
			$('.delImg').click(function(){
			    var rowCount = document.getElementById('tblPettyCash').rows.length;
				if(rowCount>2)
				$(this).parent().parent().remove();
		    	calTax(this);
			});


//-----------------------------------
$('.accounts').change(function(){
	loadRecievedFrom(this);
});
//-------------------------------------
$('#butReport').click(function(){
	if($('#txtRetSupNo').val()!=''){
		window.open('../listing/rptAdvancePayments.php?retSupNo='+$('#txtRetSupNo').val()+'&year='+$('#txtRetSupYear').val());	
	}
	else{
		alert("There is no Return Note to view");
	}
});
//----------------------------------	
$('#butConfirm').click(function(){
	if($('#txtRetSupNo').val()!=''){
		window.open('../listing/rptAdvancePayments.php?retSupNo='+$('#txtRetSupNo').val()+'&year='+$('#txtRetSupYear').val()+'&approveMode=1');	
	}
	else{
		alert("There is no Return Note to confirm");
	}
});
//-----------------------------------------------------
$('#butClose').click(function(){
		//load('main.php');	
});
//--------------refresh the form-----------------------
	$('#frmPettyCash #butNew').click(function(){
		$('#frmPettyCash').get(0).reset();
		clearRows();
		$('#frmPettyCash #txtVoucherNo').val('');
		$('#frmPettyCash #cboSearch').val('');
		$('#frmPettyCash #cboPettyCashAccount').val('');
		$('#frmPettyCash #cboCurrency').val('');
		$('#frmPettyCash #txtRecvAmmount').val('');
		$('#frmPettyCash #txtAmntsInWords').val('');
		$('#frmPettyCash #cboPaymentMethod').val('');
		$('#frmPettyCash #chkPosted').val('');
		$('#frmPettyCash #txtRemarks').val('');
		$('#frmPettyCash #txtDate').val('');
		$('#frmPettyCash #txtRefDate').val('');
		$('#frmPettyCash #txtRate').val('');
		$('#frmPettyCash #txtRafNo').val('');
		$('#frmPettyCash #txtRefOrganization').val('');
		$('#frmPettyCash #txtTotalTax').val(0);
		$('#frmPettyCash #txtTotal').val(0);
		//$('#frmPettyCash #cboPettyCashAccount').focus();
		$('#frmPettyCash #txtFnRefNo').val('');
		$('#frmPettyCash #txtFnRefNo').focus();
		
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmPettyCash #txtDate').val(d);
		$('#frmPettyCash #txtRefDate').val(d);
		
		document.getElementById("rwChequeDetails").style.display='none';
	});
//-----------------------------------------------------

	$('#frmPettyCash #butPrint').click(function(){
		if($('#frmPettyCash #txtVoucherNo').val()=='')
		{
			$('#frmPettyCash #butPrint').validationEngine('showPrompt', 'Please select Voucher.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var myurl = 'listing/pettyCashDetails.php?id='+$('#frmPettyCash #txtVoucherNo').val();
    		window.open(myurl); 
		}
	});

});

//----------end of ready -------------------------------

//-------------------------------------------------------
function loadMain()
{
	//alert(1);
/*	$("#butAddPendingDispatch").click(function(){
		popupWindow3('1');
		//$('#iframeMain1').attr('src','http://localhost/qpet/presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php?id='+$('#cboDispatchTo').val());
		$('#popupContact1').load('sampleDispatchPopup.php');	
		
	});*/
	//$("#iframeMain1").contents().find("#butAdd").click(abc);
	//$("#frmPettyCashPopup").contents().find("#butAdd").click(abc);
//	$('#frmPettyCashPopup #butClose').click(abc);
}

//-------------------------------------
function alertx()
{
	$('#frmPettyCash #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmPettyCash #butDelete').validationEngine('hide')	;
	$('#frmPettyCash #butPrint').validationEngine('hide') ;
}

//----------------------------------------------
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
//----------------------------------------------- 
function clearRows()
{
	var rowCount = document.getElementById('tblPettyCash').rows.length;

	for(var i=2;i<rowCount;i++)
	{
			document.getElementById('tblPettyCash').deleteRow(1);
	}
	document.getElementById('tblPettyCash').rows[1].cells[1].childNodes[0].value='';
	document.getElementById('tblPettyCash').rows[1].cells[2].childNodes[0].value='';
	document.getElementById('tblPettyCash').rows[1].cells[3].childNodes[0].value='';
	document.getElementById('tblPettyCash').rows[1].cells[4].childNodes[0].value='';
	document.getElementById('tblPettyCash').rows[1].cells[5].childNodes[0].value='';
	document.getElementById('tblPettyCash').rows[1].cells[6].childNodes[0].value='';
	document.getElementById('tblPettyCash').rows[1].cells[7].childNodes[0].value=0;
}
//-----------------------------------------------------------------
function insertRow()
{
	var tbl = document.getElementById('tblPettyCash');	
	var rows = tbl.rows.length;
	//tbl.rows[1].cells[5].childNodes[0].nodeValue;
	tbl.insertRow(rows);
	tbl.rows[rows].innerHTML = tbl.rows[rows-1].innerHTML;
	document.getElementById('tblPettyCash').rows[rows].cells[1].childNodes[0].value='';
	document.getElementById('tblPettyCash').rows[rows].cells[2].childNodes[0].value='';
	document.getElementById('tblPettyCash').rows[rows].cells[3].childNodes[0].value='';
	document.getElementById('tblPettyCash').rows[rows].cells[4].childNodes[0].value='';
	document.getElementById('tblPettyCash').rows[rows].cells[5].childNodes[0].value='';
	document.getElementById('tblPettyCash').rows[rows].cells[7].childNodes[0].value=0;
	//-----------------------------------
	$('.accounts').change(function(){
		loadRecievedFrom(this);
	});
	//-------------------------------
	$('.delImg').click(function(){
		var rowCount = document.getElementById('tblPettyCash').rows.length;
		if(rowCount>2)
		$(this).parent().parent().remove();
		calTax(this);
	});
	//--------------------------------------- 
	$('.taxGroup').live('change',function(){
		calTax(this);
	});
	//--------------------------------------- 
	$('.ammount').keyup(function(){
		calTax(this);
	});
	//-------------------------------------- 
}			
//------------------------------------------------------------------------------
function calculateTotalVal(){
	var rowCount = document.getElementById('tblPettyCash').rows.length;
	var row = 0;
	var totQty=0;
	
	for(var i=1;i<rowCount;i++)
	{
		if(document.getElementById('tblPettyCash').rows[i].cells[3].childNodes[0].value==''){
		var qty=0;
		}
		else{
		var qty= 	document.getElementById('tblPettyCash').rows[i].cells[3].childNodes[0].value;
		}
		if(isNaN(qty)==true){
			qty=0; 
		}
		totQty+=parseFloat(qty);
	}
		totQty=totQty.toFixed(2);

	$('#txtTotal').val(totQty);
	$('#txtRecvAmmount').val(totQty);
	if(isNaN(totQty)==true){
		$('#txtTotal').val('0');
		$('#txtRecvAmmount').val('0');
	}

}
//--------------------------------------------------------------------------------
function checkForDuplicate(){
	var rowCount = document.getElementById('tblPettyCash').rows.length;
	//var row=this.parentNode.parentNode.rowIndex;
	var k=0;
	for(var row=1;row<rowCount;row++){
		for(var i=1;i<row;i++){
			if(document.getElementById('tblPettyCash').rows[i].cells[1].childNodes[0].value==document.getElementById('tblPettyCash').rows[row].cells[1].childNodes[0].value)	{
				k=1;		
			}
		}
	}
	if(k==0){
		return true
	}
	else{
		alert("duplicate Accounts existing");
		return false
	}
}
//----------------------------------------------------------------------------------
function loadRecievedFrom(obj){
		var row=obj.parentNode.parentNode.rowIndex;
		var account=document.getElementById('tblPettyCash').rows[row].cells[1].childNodes[0].value;
		
		var url 		= "pettyCash-db-get.php?requestType=loadCustORsuppliers";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"account="+account,
			async:false,
			success:function(json){
					if(json.recievdFrom =='<option value=""></option>')
					{
						 $('#frmPettyCash #cboPayTo').removeAttr('class')
					}
					else
					{
						 $('#frmPettyCash #cboPayTo').attr('class','validate[required]');
					}
					document.getElementById('tblPettyCash').rows[row].cells[4].childNodes[0].innerHTML=json.recievdFrom;
			}
		});
}
//---------------------------------------------------------------------------------
function calTax(obje){
	var amount = $(obje).parent().parent().find('td').eq(3).children().val();
	var taxId = callTaxProcess($(obje).parent().parent().find('td').eq(2).children().val());
	var arrTax = taxId.split('/');
	var operation = '';
	
	var jsonTaxCode="[ ";

	if(arrTax.length == 1)
	{
		operation = 'Isolated';
		jsonTaxCode += '{ "taxId":"'+taxId+'"},';
	}
	else if(arrTax.length > 1)
	{
		operation = arrTax[1];
		for(var i =0; i < arrTax.length; i=i+2)
		{
			jsonTaxCode += '{ "taxId":"'+arrTax[i]+'"},';
		}
	}
	jsonTaxCode = jsonTaxCode.substr(0,jsonTaxCode.length-1);
	jsonTaxCode += " ]";
	var url = "pettyCash-db-get.php?requestType=getTaxValue&arrTaxCode="+jsonTaxCode+"&opType="+operation+"&valAmount="+amount;
	var obj = $.ajax({url:url,async:false});
	var values = obj.responseText.split('/');
	$(obje).parent().parent().find('td').eq(7).children().val(values[0]);
	if(arrTax.length == 1)
	{
		trnTax="[ ";
		trnTax += '{ "taxId":"'+taxId+'", "taxValue": "'+values[1]+'"},';
		trnTax = trnTax.substr(0,trnTax.length-1);
		trnTax += " ]";
	}
	else if(arrTax.length > 1)
	{
		trnTax="[ ";
		var k = 1;
		for(var i =0; i < arrTax.length; i=i+2)
		{
			trnTax += '{ "taxId":"'+arrTax[i]+'", "taxValue": "'+values[k]+'"},';
			k++;
		}
		trnTax = trnTax.substr(0,trnTax.length-1);
		trnTax += " ]";
	}
	$(obje).parent().parent().find('td').eq(7).children().attr('id',trnTax);
	//return obj.responseText;
	callTaxTotal();
 }
//-----------------------------------------------------------------------------
function callTaxProcess(taxGroupId)
{
	var url = "pettyCash-db-get.php?requestType=getTaxProcess&taxGroupId="+taxGroupId;
	var obj = $.ajax({url:url,async:false});
	return obj.responseText;
}
//---------------------------------------------------------------------------------
function callTaxTotal()
{
	var taxTotal = 0.00;
	$(".taxWith").each( function(){
          taxTotal += eval($(this).val()==''?0.00:$(this).val());
	});
	$('#txtTotalTax').val(taxTotal.toFixed(2));
	callTotalAmount();
}
//---------------------------------------------------------------------------------
function callTotalAmount()
{
	var subTot=callTotat().toFixed(2);
	var taxTotal=parseFloat($('#txtTotalTax').val()).toFixed(2);
//	alert(subTot);
	var tot=parseFloat(taxTotal)+parseFloat(subTot);
	tot=tot.toFixed(2);
 	$('#txtTotal').val(tot);
 	$('#txtRecvAmmount').val(tot);
	if(isNaN(tot)==true){
		$('#txtTotal').val(0);
		$('#txtRecvAmmount').val(0);
	}
/*	var x = '10*3';
	alert(eval(x));
*/}
//---------------------------------------------------------------------------------

function callTotat()
{
	var subTotal = 0;
	var rowCount = document.getElementById('tblPettyCash').rows.length;
	var row = 0;
	
	for(var i=1;i<rowCount;i++)
	{
		if(document.getElementById('tblPettyCash').rows[i].cells[3].childNodes[0].value==''){
		var qty=0;
		}
		else{
		var qty= 	document.getElementById('tblPettyCash').rows[i].cells[3].childNodes[0].value;
		}
		if(isNaN(qty)==true){
			qty=0; 
		}
		subTotal+=parseFloat(qty);
	}
//	alert(subTotal);
	//$('#txtTotal').val(subTotal);
  return subTotal;
}
//-------------------------------------------------------------------------------
function callTotandTax()
{
	var subTotal = 0;
	var subTax = 0;
	var rowCount = document.getElementById('tblPettyCash').rows.length;
	var row = 0;
	
	for(var i=1;i<rowCount;i++)
	{
		if(document.getElementById('tblPettyCash').rows[i].cells[3].childNodes[0].value==''){
		var qty=0;
		}
		else{
		var qty= 	document.getElementById('tblPettyCash').rows[i].cells[3].childNodes[0].value;
		}
		
		if(document.getElementById('tblPettyCash').rows[i].cells[7].childNodes[0].value==''){
		var tax=0;
		}
		else{
		var tax= 	document.getElementById('tblPettyCash').rows[i].cells[7].childNodes[0].value;
		}
		
		if(isNaN(qty)==true){
			qty=0; 
		}
		if(isNaN(qty)==true){
			tax=0; 
		}
		
		subTotal+=parseFloat(qty);
		subTax+=parseFloat(tax);
	}
//	alert(subTotal);
	//$('#txtTotal').val(subTotal);
  return (subTotal+subTax);
}
//-----------------------------------------------------------------------------
function loadCombo_search()
{
	var url 	= "pettyCash-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmPettyCash #cboSearch').html(httpobj.responseText);
}
//------------------------------------------------------------------------------
function clearForm(){
		$('#frmPettyCash').get(0).reset();
		clearRows();
		$('#frmPettyCash #txtVoucherNo').val('');
		$('#frmPettyCash #cboSearch').val('');
		$('#frmPettyCash #cboPettyCashAccount').val('');
		$('#frmPettyCash #cboCurrency').val('');
		$('#frmPettyCash #txtRecvAmmount').val('');
		$('#frmPettyCash #txtAmntsInWords').val('');
		$('#frmPettyCash #cboPaymentMethod').val('');
		$('#frmPettyCash #chkPosted').val('');
		$('#frmPettyCash #txtRemarks').val('');
		$('#frmPettyCash #txtDate').val('');
		$('#frmPettyCash #txtRefDate').val('');
		$('#frmPettyCash #txtRate').val('');
		$('#frmPettyCash #txtRafNo').val('');
		$('#frmPettyCash #txtRefOrganization').val('');
		$('#frmPettyCash #txtTotalTax').val(0);
		$('#frmPettyCash #txtTotal').val(0);
		//$('#frmPettyCash #cboPettyCashAccount').focus();
		$('#frmPettyCash #txtFnRefNo').val('');
		$('#frmPettyCash #txtFnRefNo').focus();
		
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmPettyCash #txtDate').val(d);
		$('#frmPettyCash #txtRefDate').val(d);
		
		document.getElementById("rwChequeDetails").style.display='none';
}




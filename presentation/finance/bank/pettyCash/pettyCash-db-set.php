<?php

session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$userId = $_SESSION['userId'];
$locationId = $_SESSION['CompanyID'];
$companyId = $_SESSION['headCompanyId'];
include "{$backwardseperator}dataAccess/Connector.php";
include "../../commanFunctions/CommanEditAndDelete.php";

$response = array('type' => '', 'msg' => '');

/////////// parameters /////////////////////////////
/////////// parameters /////////////////////////////
$requestType = $_REQUEST['requestType'];

$serialNo = $_REQUEST['serialNo'];
$bankAccount = $_REQUEST['bankAccount'];
$currency = $_REQUEST['currency'];
$recvAmmount = $_REQUEST['recvAmmount'];
$ammountsInWords = $_REQUEST['ammountsInWords'];
$paymentMethod = $_REQUEST['paymentMethod'];
$txtpaymentMethod = $_REQUEST['txtpaymentMethod'];
$remarks = $_REQUEST['remarks'];
$date = $_REQUEST['date'];
$rate = $_REQUEST['rate'];
$refNo = $_REQUEST['refNo'];
$amStatus 	= $_REQUEST['amStatus'];
$manualNo 	= $_REQUEST['manualNo'];

$fnRefNo 	= $_REQUEST['fnRefNo'];

if ($paymentMethod == "2") {//cheque
    $posted = $_REQUEST['posted'];
    /* 	if($posted=='on'){
      $posted=1;
      }
      else{
      $posted=0;
      }
     */ $refDate = $_REQUEST['refDate'];
    $refOrganization = $_REQUEST['refOrganization'];
} else {
    $posted = '0';
    $refDate = '0000-00-00';
    $refOrganization = '';
}

$arr = json_decode($_REQUEST['arr'], true);

//------------save---------------------------	
if ($requestType == 'save') {
    //get Account Details
    $sql1 = "SELECT intAccountId,intPayTo FROM fin_bankpettycash_details WHERE strPettyCashNo='$serialNo'";
    $result1 = $db->RunQuery($sql1);
    $x = 0;
    while ($row1 = mysqli_fetch_array($result1)) {
        $arr1[$x]['account'] = $row1['intAccountId'];
        $arr1[$x]['recvFrom'] = $row1['intPayTo'];
        ++$x;
    }
    // ckeck Unrealize gain/loss for entry and if exist block edit and delete
    $chk = checkUnrealizeEntry('Petty Cash', $serialNo, $arr1);
    if ($chk) {
        $response['type'] = 'fail';
        $response['msg'] = "You cannot allow this process! Patty cash has some Unrealize Gain Or Loss for Customer or Supplier";
        //echo json_encode($response);
    } else {
    $accountPeriod = getLatestAccPeriod($companyId);
    $db->OpenConnection();
    $db->RunQuery2('Begin');

    if ($serialNo == '') {
		if($amStatus == "Auto")
		{
			$serialNo = getnextBankPettyCashNo($companyId);
		}
		else if($amStatus == "Manual")
		{
			$serialNo	= $manualNo;
		}
        $year = date('Y');
        $editMode = 0;
    } else {
        $editMode = 1;
    }
    //-----------delete and insert to header table-----------------------
    if ($editMode == 1) {
        $sql = "UPDATE `fin_bankpettycash_header` SET intAccount ='$bankAccount', 
                    dtDate ='$date', 
                    dblRate ='$rate', 
                    dblReceivedAmount ='$recvAmmount', 
                    intPaymentMethod ='$paymentMethod', 
                    intCurrency ='$currency', 
                    strAmountInWord ='$ammountsInWords', 
                    strReferenceNo ='$refNo', 
                    dtReferenceDate ='$refDate', 
                    intCheckPosted ='$posted', 
                    strReferenceOrganization ='$refOrganization', 
                    strRemarks ='$remarks', 
                    intModifyer ='$userId', 
                    intStatus ='1',
                    dtmModifyDate =now(),
					strFnRefNo = '$fnRefNo' 
                WHERE (`strPettyCashNo`='$serialNo')";
        $result = $db->RunQuery2($sql);
        
        //========update the transaction deader====================
        $sql="SELECT fin_bankpettycash_header.entryId FROM fin_bankpettycash_header WHERE (`strPettyCashNo`='$serialNo')";
        $result = $db->RunQuery2($sql);
        $row = mysqli_fetch_array($result);
        $entryId=$row['entryId'];               
        $sql="UPDATE fin_transactions SET 
                    entryDate='$date',                                                 
                    currencyId=$currency,
                    currencyRate='$rate',
                    payMethodId=$paymentMethod,
                    paymentNumber='$refNo',
                    transDetails='$remarks',                    
                    accPeriod=$accountPeriod
            WHERE entryId=$entryId";
        $db->RunQuery2($sql);

        $sqld = "DELETE FROM `fin_transactions_details` WHERE entryId=$entryId";
        $resultd = $db->RunQuery2($sqld);
        //=========================================================
    } else {
        $sql = "DELETE FROM `fin_bankpettycash_header` WHERE (`strPettyCashNo`='$serialNo')";
        $result1 = $db->RunQuery2($sql);
        
        //Add data to transaction header*******************************************
        $sql="INSERT INTO fin_transactions (entryDate, strProgramType, documentNo, currencyId, currencyRate, transDetails, payMethodId, paymentNumber, accPeriod, userId, companyId, createdOn) VALUES
            ('$date','Petty Cash','$serialNo',$currency,$rate,'$remarks',$paymentMethod,'$refNo',$accountPeriod,$userId,$companyId,now())";
                
        $db->RunQuery2($sql);
        $entryId=$db->insertId; 
        //*********************************

        $sql = "INSERT INTO `fin_bankpettycash_header` (`strPettyCashNo`,`intAccount`,`dtDate`,dblRate,dblReceivedAmount,strReferenceNo,dtReferenceDate,intCheckPosted,strReferenceOrganization,strRemarks,intCreator,dtmCreateDate,intCompanyId,intPost,intPaymentMethod,intCurrency,strAmountInWord,entryId,strFnRefNo) 
					VALUES ('$serialNo','$bankAccount','$date','$rate','$recvAmmount','$refNo','$refDate','$posted','$refOrganization','$remarks','$userId',now(),'$companyId','0','$paymentMethod','$currency','$ammountsInWords',$entryId,'$fnRefNo')";
        $result = $db->RunQuery2($sql);
    }
    //-----------delete and insert to detail table-----------------------
    if ($result) {
        $sql = "DELETE FROM `fin_bankpettycash_details` WHERE (`strPettyCashNo`='$serialNo')";
        $result2 = $db->RunQuery2($sql);       

        $toSave = 0;
        $saved = 0;
        $rollBackFlag = 0;
        $totAmnt = 0;
        foreach ($arr as $arrVal) {
            $account = $arrVal['account'];
            $sql = "SELECT
                        mst_financechartofaccounts.intId,
                        mst_financechartofaccounts.strName,
                        mst_financialsubtype.intId as finType,
                        mst_financialsubtype.strName
                        FROM
                        mst_financechartofaccounts
                        Inner Join mst_financialsubtype ON mst_financechartofaccounts.intFinancialTypeId = mst_financialsubtype.intId
                        WHERE
                        mst_financechartofaccounts.intId =  '$account'";
            $result = $db->RunQuery2($sql);
            $row = mysqli_fetch_array($result);
            $finType = $row['finType'];
            ;

            if ($finType == 10)
                $personType = "'cus'";
            else if ($finType == 18)
                $personType = "'sup'";
			else if ($finType == 26 || $finType == 16 || $finType == 17 || $finType == 28)
                 $personType = "'osup'";
			else if ($finType == 22 || $finType == 21 || $finType == 11 || $finType == 12 || $finType == 9)
                     $personType = "'ocus'";
            else
                $personType = 'null';
            $payTo = $arrVal['payTo'];
            if ($payTo == '') {
                $payTo = 0;
            }
            $taxId = $arrVal['taxId'];
            if ($taxId == '') {
                $taxId = '0';
            }
            $ammount = $arrVal['ammount'];
            $taxAmmount = $arrVal['taxAmmount'];
            $taxDetails = $arrVal['trnTaxVal'];
            $memo = $arrVal['memo'];
            $dimention = $arrVal['dimention'];
            if ($dimention == '') {
                $dimention = '0';
            }

            if ($rollBackFlag != 1) {
                $sql = "INSERT INTO `fin_bankpettycash_details` (`strPettyCashNo`,`intAccountId`,`intPayTo`,`intTaxId`,`dblAmmount`,`dblTaxAmmount`,`strMemo`,`intDimention`) 
					VALUES ('$serialNo','$account','$payTo','$taxId','$ammount','$taxAmmount','$memo','$dimention')";
                $result3 = $db->RunQuery2($sql);
                $tot = ($ammount + $taxAmmount);

                if ($ammount < 0) {//if minus ammount
                    $credDebType = 'C';
                } else {
                    $credDebType = 'D';
                }
                
                $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId,personType, personId) VALUES 
                                        ($entryId,'$credDebType',$account,$ammount,'$memo',$dimention,$personType,$payTo)";
                $trnResult = $db->RunQuery2($sql);
                //>>>>>>>>>>>>>>>>>>>>>>>>>>>transaction table process - tax>>>>>>>>>>>>>>>>>>>>>>>>>
                if (count($taxDetails) != 0 && $trnResult) {
                    foreach ($taxDetails as $taxDetail) {
                        $taxId = $taxDetail['taxId'];
                        $taxAmount = $taxDetail['taxValue'];

                        $sql = "SELECT
							mst_financetaxactivate.intChartOfAccountId
							FROM mst_financetaxactivate
							WHERE
							mst_financetaxactivate.intTaxId = '$taxId' AND
							mst_financetaxactivate.intCompanyId = '$companyId'";
                        $result = $db->RunQuery2($sql);
                        $row = mysqli_fetch_array($result);
                        $taxAccount = $row['intChartOfAccountId'];
                        
                         $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                                        ($entryId,'D',$taxAccount,$taxAmount,'$remarks',null)";                        
                        $db->RunQuery2($sql);
                    }
                }
                //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                $totAmnt+=$tot;
                if ($result3 == 1) {
                    $saved++;
                } else {
                    $rollBackFlag = 1;
                }
                $toSave++;
            }
        }//end of foreach
        if ($rollBackFlag != 1) {
            $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                                        ($entryId,'C',$bankAccount,$totAmnt,'$remarks',null)";
            $result4 = $db->RunQuery2($sql);
            if (!$result4) {
                $rollBackFlag = 1;
            }
        }
    }
    //echo $rollBackFlag;

    if ($rollBackFlag == 1) {
        $db->RunQuery2('Rollback');
        $response['type'] = 'fail';
        $response['msg'] = $db->errormsg;
        $response['q'] = $sql;
    } else if (($result) && ($toSave == $saved)) {
        $db->RunQuery2('Commit');
        $response['type'] = 'pass';
        if ($editMode == 1)
            $response['msg'] = 'Updated successfully.';
        else
            $response['msg'] = 'Saved successfully.';

        $response['serialNo'] = $serialNo;
        $response['year'] = $year;
    }
    else {
        $db->RunQuery2('Rollback');
        $response['type'] = 'fail';
        $response['msg'] = $db->errormsg;
        $response['q'] = $sql;
    }

    $db->CloseConnection();
    }
} else if ($requestType == 'delete') {
    //get Account Details
    $sql1 = "SELECT intAccountId,intPayTo FROM fin_bankpettycash_details WHERE strPettyCashNo='$serialNo'";
    $result1 = $db->RunQuery($sql1);
    $x = 0;
    while ($row1 = mysqli_fetch_array($result1)) {
        $arr1[$x]['account'] = $row1['intAccountId'];
        $arr1[$x]['recvFrom'] = $row1['intPayTo'];
        ++$x;
    }
    // ckeck Unrealize gain/loss for entry and if exist block edit and delete
    $chk = checkUnrealizeEntry('Petty Cash', $serialNo, $arr1);
    if ($chk) {
        $response['type'] = 'fail';
        $response['msg'] = "You cannot allow this process! Patty cash has some Unrealize Gain Or Loss for Customer or Supplier";
        //echo json_encode($response);
    } else {
    try {
        $db->begin();
		//----------------added by lasantha @ CAIT on 13/8/2012----------------------
		$sqlRp = "SELECT
				fin_customer_receivedpayments_main_details.strDocNo,
				fin_customer_receivedpayments_main_details.intCompanyId,
				fin_customer_receivedpayments_main_details.strDocType
				FROM
				fin_customer_receivedpayments_main_details
				Inner Join fin_bankpettycash_header ON fin_customer_receivedpayments_main_details.strDocNo = fin_bankpettycash_header.strPettyCashNo
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.intReceiptNo = fin_customer_receivedpayments_header.intReceiptNo AND fin_customer_receivedpayments_main_details.intAccPeriodId = fin_customer_receivedpayments_header.intAccPeriodId AND fin_customer_receivedpayments_main_details.intLocationId = fin_customer_receivedpayments_header.intLocationId AND fin_customer_receivedpayments_main_details.intCompanyId = fin_customer_receivedpayments_header.intCompanyId AND fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strDocNo =  '$serialNo' AND
				fin_customer_receivedpayments_main_details.strDocType =  'Petty Cash' AND
				fin_customer_receivedpayments_main_details.intCompanyId =  '$companyId' AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0'";
		$resultRp = $db->RunQuery2($sqlRp);
		$sqlSp = "SELECT
				fin_supplier_payments_main_details.strDocNo,
				fin_supplier_payments_main_details.intCompanyId,
				fin_supplier_payments_main_details.strDocType
				FROM
				fin_supplier_payments_main_details
				Inner Join fin_supplier_payments_header ON fin_supplier_payments_main_details.intReceiptNo = fin_supplier_payments_header.intReceiptNo AND fin_supplier_payments_main_details.intAccPeriodId = fin_supplier_payments_header.intAccPeriodId AND fin_supplier_payments_main_details.intLocationId = fin_supplier_payments_header.intLocationId AND fin_supplier_payments_main_details.intCompanyId = fin_supplier_payments_header.intCompanyId AND fin_supplier_payments_main_details.strReferenceNo = fin_supplier_payments_header.strReferenceNo
				Inner Join fin_bankpettycash_header ON fin_bankpettycash_header.intCompanyId = fin_supplier_payments_main_details.intCompanyId AND fin_bankpettycash_header.strPettyCashNo = fin_supplier_payments_main_details.strDocNo
				WHERE
				fin_supplier_payments_header.intDeleteStatus =  '0' AND
				fin_supplier_payments_main_details.strDocType =  'Petty Cash' AND
				fin_supplier_payments_main_details.intCompanyId =  '$companyId' AND
				fin_supplier_payments_main_details.strDocNo =  '$serialNo'";
		$resultSp = $db->RunQuery2($sqlSp);
		$sqlOsp = "SELECT
				fin_other_payable_payments_main_details.strDocNo,
				fin_other_payable_payments_main_details.intCompanyId,
				fin_other_payable_payments_main_details.strDocType
				FROM
				fin_other_payable_payments_main_details
				Inner Join fin_other_payable_payments_header ON fin_other_payable_payments_main_details.intReceiptNo = fin_other_payable_payments_header.intReceiptNo AND fin_other_payable_payments_main_details.intAccPeriodId = fin_other_payable_payments_header.intAccPeriodId AND fin_other_payable_payments_main_details.intLocationId = fin_other_payable_payments_header.intLocationId AND fin_other_payable_payments_main_details.intCompanyId = fin_other_payable_payments_header.intCompanyId AND fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
				Inner Join fin_bankpettycash_header ON fin_bankpettycash_header.intCompanyId = fin_other_payable_payments_main_details.intCompanyId AND fin_bankpettycash_header.strPettyCashNo = fin_other_payable_payments_main_details.strDocNo
				WHERE
				fin_other_payable_payments_header.intDeleteStatus =  '0' AND
				fin_other_payable_payments_main_details.strDocType =  'Petty Cash' AND
				fin_other_payable_payments_main_details.intCompanyId =  '$companyId' AND
				fin_other_payable_payments_main_details.strDocNo =  '$serialNo'";
		$resultOsp = $db->RunQuery2($sqlOsp);
		if(!mysqli_num_rows($resultRp) && !mysqli_num_rows($resultSp) && !mysqli_num_rows($resultOsp))
		{
			$serialNo = $_REQUEST['serialNo'];
	
	
			$sql = "UPDATE `fin_bankpettycash_header` SET intStatus ='0', intModifyer ='$userId'  
	 				WHERE (`strPettyCashNo`='$serialNo')  ";
			$result = $db->RunQuery2($sql);
	
			//==========UPDATE TRANS ACTION delete STATUS
			$sql = "SELECT fin_bankpettycash_header.entryId FROM fin_bankpettycash_header WHERE (`strPettyCashNo`='$serialNo')";
			$result = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($result);
			$entryId = $row['entryId'];
			$sqld = "UPDATE `fin_transactions` SET delStatus=1 WHERE entryId=$entryId";
			$resultd = $db->RunQuery2($sqld);
	
	
			if (($result)) {
				$db->commit();
				$response['type'] = 'pass';
				$response['msg'] = 'Deleted successfully.';
			} else {
				$db->rollback(); //roalback
				$response['type'] = 'fail';
				$response['msg'] = $db->errormsg;
				$response['q'] = $sql;
			}
		}
		else
		{
			$db->rollback();
			$response['type'] 		= 'fail';
			$response['msg'] 		= "You cannot allow this process! Petty Cash has some payements";
		}
		//------------------------------------------------------------------------------------------------------
    } 
	catch (Exception $e) {
        $db->rollback(); //roalback
        $response['type'] = 'fail';
        $response['msg'] = $e->getMessage();
        $response['q'] = $sql;
    }
    }
}
//----------------------------------------
echo json_encode($response);

//-----------------------------------------	
function getnextBankPettyCashNo($companyId) {
    global $db;
    global $locationId;

    $sql = "SELECT
				Max(mst_financeaccountingperiod.dtmCreateDate),
				mst_financeaccountingperiod.dtmStartingDate, 
				substring(mst_financeaccountingperiod.dtmStartingDate,1,4) as fromY,
				substring(mst_financeaccountingperiod.dtmClosingDate,1,4) as toY 
				FROM mst_financeaccountingperiod ";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $accPeriod = $row['fromY'] . "-" . $row['toY'];

    $sql = "SELECT
				sys_finance_no.intBankPettyCashNo 
				FROM
				sys_finance_no
				WHERE 
				sys_finance_no.intLocationId =  '$locationId' and sys_finance_no.intCompanyId = '$companyId'";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $bankPettyCashNo = $row['intBankPettyCashNo'];

    //------------------
    $sql = "SELECT
				mst_companies.strCode AS company,
				mst_companies.intId,
				mst_locations.intCompanyId,
				mst_locations.strCode AS location,
				mst_locations.intId
				FROM
				mst_companies
				Inner Join mst_locations ON mst_locations.intCompanyId = mst_companies.intId
				WHERE
				mst_locations.intId =  '$locationId' AND
				mst_companies.intId =  '$companyId'
				";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $companyCode = $row['company'];
    $locationCode = $row['location'];
    //---------------------

    $sql = "UPDATE `sys_finance_no` SET intBankPettyCashNo=intBankPettyCashNo+1  WHERE (`intLocationId`='$locationId' AND  sys_finance_no.intCompanyId = '$companyId')  ";
    $db->RunQuery2($sql);
    return $companyCode . "/" . $locationCode . "/" . $accPeriod . "/" . $bankPettyCashNo;
}

//----------------------------------------
function getCreditPeriod($companyId) {
    global $db;
    $sql = "SELECT
				MAX(mst_financeaccountingperiod.intId) AS accId,
				mst_financeaccountingperiod.dtmStartingDate,
				mst_financeaccountingperiod.dtmClosingDate,
				mst_financeaccountingperiod.intStatus,
				mst_financeaccountingperiod_companies.intCompanyId,
				mst_financeaccountingperiod_companies.intPeriodId
				FROM
				mst_financeaccountingperiod
				Inner Join mst_financeaccountingperiod_companies ON mst_financeaccountingperiod_companies.intPeriodId = mst_financeaccountingperiod.intId
				WHERE
				mst_financeaccountingperiod_companies.intCompanyId =  '$companyId' AND mst_financeaccountingperiod.intStatus = '1'
				ORDER BY
				mst_financeaccountingperiod.intId DESC
				";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $latestAccPeriodId = $row['accId'];
    return $latestAccPeriodId;
}

//---------------------------------------

function getLatestAccPeriod($companyId) {
    global $db;
    $sql = "SELECT
                        MAX(mst_financeaccountingperiod.intId) AS accId,
                        mst_financeaccountingperiod.dtmStartingDate,
                        mst_financeaccountingperiod.dtmClosingDate,
                        mst_financeaccountingperiod.intStatus,
                        mst_financeaccountingperiod_companies.intCompanyId,
                        mst_financeaccountingperiod_companies.intPeriodId
                        FROM
                        mst_financeaccountingperiod
                        Inner Join mst_financeaccountingperiod_companies ON mst_financeaccountingperiod_companies.intPeriodId = mst_financeaccountingperiod.intId
                        WHERE
                        mst_financeaccountingperiod_companies.intCompanyId =  '$companyId' AND mst_financeaccountingperiod.intStatus = '1'
                        ORDER BY
                        mst_financeaccountingperiod.intId DESC
                        ";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $latestAccPeriodId = $row['accId'];
    return $latestAccPeriodId;
}

?>
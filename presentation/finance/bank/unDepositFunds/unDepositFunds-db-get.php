<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$locationId 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";

	if($requestType=='loadCombo')
	{
		$sql = "SELECT DISTINCT
				fin_bankundeposit_header.strUnDepositNo
				FROM fin_bankundeposit_header
				WHERE
				fin_bankundeposit_header.intStatus =  '1'
				ORDER BY
				fin_bankundeposit_header.strUnDepositNo DESC
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['strUnDepositNo']."\">".$row['strUnDepositNo']."</option>";
		}
		echo $html;
	}
	else if($requestType=='loadExchangeRates')
	{
		$currency  = $_REQUEST['currency'];
		$date  = $_REQUEST['date'];
		$sql = "SELECT
				mst_financeexchangerate.dblBuying, 
				mst_financeexchangerate.dblSellingRate  
				FROM mst_financeexchangerate
				WHERE
				mst_financeexchangerate.intCurrencyId =  '$currency' and mst_financeexchangerate.dtmDate =  '$date'";
		$result = $db->RunQuery($sql);
		
		$response['sellingRate'] = '';
		$response['buyingRate'] = '';
		while($row=mysqli_fetch_array($result))
		{
			$response['sellingRate'] = $row['dblSellingRate'];
			$response['buyingRate'] = $row['dblBuying'];
		}
		echo json_encode($response);
	}
	else if($requestType=='loadCustORsuppliers')
	{
		$account  = $_REQUEST['account'];
		$sql = "SELECT   
				mst_financechartofaccounts.intId, 
				mst_financialsubtype.strName
				FROM
				mst_financialsubtype
				Inner Join mst_financechartofaccounts ON mst_financialsubtype.intId = mst_financechartofaccounts.intFinancialTypeId
				WHERE
				mst_financechartofaccounts.intId =  '$account'";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		
		$chartOfAcc=$row['intId'];
		if($row['strName']=='Accounts Receivable'){
		$sql = "SELECT
				mst_financecustomeractivate.intCustomerId as id,
				mst_customer.strName
				FROM
				mst_financecustomeractivate
				Inner Join mst_customer ON mst_financecustomeractivate.intCustomerId = mst_customer.intId
				WHERE
				mst_financecustomeractivate.intCompanyId =  '$company' AND
				mst_financecustomeractivate.intChartOfAccountId =  '$chartOfAcc'";
		}
		else if($row['strName']=='Accounts Payable'){
		$sql = "SELECT
				mst_financesupplieractivate.intSupplierId as id,
				mst_supplier.strName
				FROM
				mst_financesupplieractivate
				Inner Join mst_supplier ON mst_financesupplieractivate.intSupplierId = mst_supplier.intId
				WHERE
				mst_financesupplieractivate.intCompanyId =  '$company' AND
				mst_financesupplieractivate.intChartOfAccountId =  '$chartOfAcc'";
		}
		else{
		$sql = "";	
		}
		//echo $sql;
		
		$result = $db->RunQuery($sql);
		$combo="<option value=\"\"></option>";
		
		while($rows=mysqli_fetch_array($result))
		{
			$combo .= "<option value=\"".$rows['id']."\">".$rows['strName']."</option>";	

		}
		$response['recievdFrom'] = $combo;
			
		echo json_encode($response);
	}
	
?>
// JavaScript Document

$(document).ready(function() {

//-------------------------------
$('.chkSelect').click(function(){
	//if checked==true
	if($(this).parent().parent().find('.chkSelect').attr("checked")==true){ 
		//get selected values;
		var rw=this.parentNode.parentNode.rowIndex;
		var docNo=document.getElementById('tblMainGrid').rows[rw].cells[4].innerHTML;
		var AccNo=document.getElementById('tblMainGrid').rows[rw].cells[8].id;
		var AccName=document.getElementById('tblMainGrid').rows[rw].cells[8].innerHTML;
		var ammount=document.getElementById('tblMainGrid').rows[rw].cells[9].innerHTML;

		//insert row
		var rowCount = document.getElementById('tblBankUnDeposit').rows.length;
		var existFlag=0;
		for(var i=1;i<rowCount;i++)
		{
			if((document.getElementById('tblBankUnDeposit').rows[i].cells[1].innerHTML==docNo) && (document.getElementById('tblBankUnDeposit').rows[i].cells[2].id==AccNo)){
			 existFlag=1;	
			}
		}
		if(existFlag==0){
			if(document.getElementById('tblBankUnDeposit').rows[rowCount-1].cells[1].innerHTML!=''){
				insertRow();
			}
			//assign selected values
			var newRowCount = document.getElementById('tblBankUnDeposit').rows.length;
			document.getElementById('tblBankUnDeposit').rows[newRowCount-1].cells[1].innerHTML=docNo;
			document.getElementById('tblBankUnDeposit').rows[newRowCount-1].cells[2].innerHTML=AccName;
			document.getElementById('tblBankUnDeposit').rows[newRowCount-1].cells[2].id=AccNo;
			document.getElementById('tblBankUnDeposit').rows[newRowCount-1].cells[2].id=AccNo;
			document.getElementById('tblBankUnDeposit').rows[newRowCount-1].cells[9].childNodes[0].value=ammount;
		}
	}
	
	//if checked==false
	if($(this).parent().parent().find('.chkSelect').attr("checked")==false){ 
		var rw=this.parentNode.parentNode.rowIndex;
		var docNo=document.getElementById('tblMainGrid').rows[rw].cells[4].innerHTML;
		var AccNo=document.getElementById('tblMainGrid').rows[rw].cells[8].id;

		//delete row
		var rowCount = document.getElementById('tblBankUnDeposit').rows.length;
		var existFlag=0;
		var r=0;
		for(var i=1;i<rowCount;i++)
		{
			if((document.getElementById('tblBankUnDeposit').rows[i].cells[1].innerHTML==docNo) && (document.getElementById('tblBankUnDeposit').rows[i].cells[2].id==AccNo)){
				r== document.getElementById('tblBankUnDeposit').rows.length;
				if(r>2){
					document.getElementById('tblBankUnDeposit').deleteRow(i);
				}
				
				else{
					clearRow();	
				}
				
			}
		}
	}
		calculateTotalVal();
});
	//-------------------------------------
	$('.calTot').keyup(function(){
		calculateTotalVal();
	});
	//-------------------------------------
	$('.delImg').click(function(){
		var rowCount = document.getElementById('tblBankUnDeposit').rows.length;
		if(rowCount>2)
		$(this).parent().parent().remove();
		calculateTotalVal();
	});
	//------------------------------------------------------ 
    $('#frmUndeposit #butDelete').click(function(){
		var serial=$('#frmUndeposit #txtUnDepositNo').val();
		if($('#frmUndeposit #txtUnDepositNo').val()=='')
		{
			//$('#frmBankDeposit #butDelete').validationEngine('showPrompt', 'Please select Deposit No.', 'fail');
		//	var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+serial+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = "unDepositFunds-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											data:'requestType=delete&serialNo='+serial,
											async:false,
											success:function(json){
												
												$('#frmUndeposit #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												if(json.type=='pass')
												{
													$('#frmUndeposit').get(0).reset();
													clearForm(); 
													loadCombo_search();
													var t=setTimeout("alertDelete()",1000);return;
												}
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
		
	//	clearForm();
	});
	//-----------------------------------------------------------
 
  $('#frmUndeposit #butSave').click(function(){
	  calculateTotalVal();
	var requestType = '';
	if ($('#frmUndeposit').validationEngine('validate'))   
    { 

		var data = "requestType=save";
			data+="&serialNo="		+	$('#txtUnDepositNo').val();

			var rowCount = document.getElementById('tblBankUnDeposit').rows.length;
			if(rowCount==1){
				alert("No Accounts selected");
				return false;				
			}
			var row = 0;
			
			var arr="[";
			
			for(var i=1;i<rowCount;i++)
			{
					var docNo = 	document.getElementById('tblBankUnDeposit').rows[i].cells[1].innerHTML;
					var account = 	document.getElementById('tblBankUnDeposit').rows[i].cells[2].id;
					var depositTo = 	document.getElementById('tblBankUnDeposit').rows[i].cells[3].childNodes[0].value;
					var payMethod = 	document.getElementById('tblBankUnDeposit').rows[i].cells[4].childNodes[0].value;
					var refNo = 	document.getElementById('tblBankUnDeposit').rows[i].cells[5].childNodes[0].value;
					var refDate = 	document.getElementById('tblBankUnDeposit').rows[i].cells[6].childNodes[0].value;
					var posted = 	document.getElementById('tblBankUnDeposit').rows[i].cells[7].childNodes[0].value;
					if(document.getElementById('tblBankUnDeposit').rows[i].cells[7].childNodes[0].checked==true)
					var posted = 1;
					if(document.getElementById('tblBankUnDeposit').rows[i].cells[7].childNodes[0].checked==false)
					var posted = 0;
					var dimention = 	document.getElementById('tblBankUnDeposit').rows[i].cells[8].childNodes[0].value;
					var ammount = 	document.getElementById('tblBankUnDeposit').rows[i].cells[9].childNodes[0].value;
					var memo = 	document.getElementById('tblBankUnDeposit').rows[i].cells[10].childNodes[0].value;
					
					    arr += "{";
						arr += '"docNo":"'+		docNo +'",' ;
						arr += '"account":"'+		account +'",' ;
						arr += '"depositTo":"'+		depositTo +'",' ;
						arr += '"payMethod":"'+		payMethod +'",' ;
						arr += '"refNo":"'+		refNo  +'",' ;
						arr += '"refDate":"'+		refDate +'",' ;
						arr += '"posted":"'+		posted +'",' ;
						arr += '"dimention":"'+		dimention +'",' ;
						arr += '"ammount":"'+		ammount +'",' ;
						arr += '"memo":"'+		memo  +'"' ;
						arr +=  '},';
						
			}
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;
		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = "unDepositFunds-db-set.php";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			success:function(json){

					$('#frmUndeposit #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						//$('#frmSampleInfomations').get(0).reset();
						//saveImage(json.sampleNo,json.sampleYear,json.revisionNo);
					 	loadCombo_search();
						var t=setTimeout("alertx()",1000);
						$('#txtUnDepositNo').val(json.serialNo);
					 	$('#cboSearch').val(json.serialNo);
						return;
					}
				},
			error:function(xhr,status){
					
					$('#frmUndeposit #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
	//-----------------------------------------------------------








});
//end of ready 
//-------------------------------------
function alertx()
{
	$('#frmUndeposit #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmUndeposit #butDelete').validationEngine('hide')	;
}

//-------------------------------------
$('.delImg').click(function(){
	var rowCount = document.getElementById('tblBankUnDeposit').rows.length;
	if(rowCount>2)
	$(this).parent().parent().remove();
	calculateTotalVal();
});
//------------------------------------------------------ 
function insertRow()
{
	var tbl = document.getElementById('tblBankUnDeposit');	
	var rows = tbl.rows.length;
	//tbl.rows[1].cells[5].childNodes[0].nodeValue;
	tbl.insertRow(rows);
	tbl.rows[rows].innerHTML = tbl.rows[rows-1].innerHTML;
	
	//--------- 
	$('.delImg').click(function(){
		var rowCount = document.getElementById('tblBankUnDeposit').rows.length;
		if(rowCount>2)
		$(this).parent().parent().remove();
		calculateTotalVal();
	});
	//--------- 
}
//----------------------------------------------- 
function clearRow()
{
	var rowCount = document.getElementById('tblBankUnDeposit').rows.length;

	document.getElementById('tblBankUnDeposit').rows[1].cells[1].innerHTML='';
	document.getElementById('tblBankUnDeposit').rows[1].cells[1].id='';
	document.getElementById('tblBankUnDeposit').rows[1].cells[2].innerHTML='';
	document.getElementById('tblBankUnDeposit').rows[1].cells[2].id='';
	document.getElementById('tblBankUnDeposit').rows[1].cells[3].childNodes[0].value='';
	document.getElementById('tblBankUnDeposit').rows[1].cells[4].childNodes[0].value='';
	document.getElementById('tblBankUnDeposit').rows[1].cells[5].childNodes[0].value='';
	document.getElementById('tblBankUnDeposit').rows[1].cells[8].childNodes[0].value='';
	document.getElementById('tblBankUnDeposit').rows[1].cells[9].childNodes[0].value='';
	document.getElementById('tblBankUnDeposit').rows[1].cells[10].childNodes[0].value='';
}
//------------------------------------------------------------------------------
function calculateTotalVal(){
	var rowCount = document.getElementById('tblBankUnDeposit').rows.length;
	var row = 0;
	var totQty=0;
	
	for(var i=1;i<rowCount;i++)
	{
		if(document.getElementById('tblBankUnDeposit').rows[i].cells[9].childNodes[0].value==''){
		var qty=0;
		}
		else{
		var qty= 	document.getElementById('tblBankUnDeposit').rows[i].cells[9].childNodes[0].value;
		}
		if(isNaN(qty)==true){
			qty=0; 
		}
		totQty+=parseFloat(qty);
	}
		if(isNaN(totQty)==true){
			totQty=0; 
		}
		totQty=totQty.toFixed(2);
		$('#txtTotal').val(totQty);
}
//--------------------------------------------------------------------------------
function insertDuplicateRow(){
	insertRow();
	var rowCount = document.getElementById('tblBankUnDeposit').rows.length;
	document.getElementById('tblBankUnDeposit').rows[rowCount-1].cells[3].childNodes[0].value='';
	document.getElementById('tblBankUnDeposit').rows[rowCount-1].cells[9].childNodes[0].value='';
}
//-----------------------------------------------------------------------------
function loadCombo_search()
{
	var url 	= "unDepositFunds-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmUndeposit #cboSearch').html(httpobj.responseText);
}
//------------------------------------------------------------------------------
function clearForm()
{
	var rowCount = document.getElementById('tblBankUnDeposit').rows.length;
	
	for(var i=2;i<rowCount;i++)
	{
			document.getElementById('tblBankUnDeposit').deleteRow(i);
	}
	
	clearRow();
}
//------------------------------------------------------------------------------

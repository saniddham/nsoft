<?php

session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$userId = $_SESSION['userId'];
$locationId = $_SESSION['CompanyID'];

include "{$backwardseperator}dataAccess/Connector.php";

$response = array('type' => '', 'msg' => '');
$sql = "SELECT DISTINCT
			mst_locations.intCompanyId,
			mst_companies.intBaseCurrencyId
			FROM
			mst_locations
			Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
			WHERE mst_locations. intId=" . $_SESSION["CompanyID"] . "";
$result = $db->RunQuery($sql);
while ($row = mysqli_fetch_array($result)) {
    $companyId = $row['intCompanyId'];
    $baseCurrencyId = null($row['intBaseCurrencyId']);
}
/////////// parameters /////////////////////////////
/////////// parameters /////////////////////////////
$requestType = $_REQUEST['requestType'];

$serialNo = $_REQUEST['serialNo'];
$date = date();

$arr = json_decode($_REQUEST['arr'], true);

//------------save---------------------------	
if ($requestType == 'save') {
    $accountPeriod = getLatestAccPeriod($companyId);
    $db->OpenConnection();
    $db->RunQuery2('Begin');

    if ($serialNo == '') {
        $serialNo = getnextDepositNo($companyId);
        $year = date('Y');
        $editMode = 0;
    } else {
        $editMode = 1;
    }
    //-----------delete and insert to header table-----------------------
    if ($editMode == 1) {
        $sql = "UPDATE `fin_bankundeposit_header` SET intModifyer ='$userId', 
                                intStatus ='1',
                                dtmModifyDate =now() 
				WHERE (`strUnDepositNo`='$serialNo')";
        $result = $db->RunQuery2($sql);
        //========update the transaction deader====================
        $sql = "SELECT fin_bankundeposit_header.entryId FROM fin_bankundeposit_header WHERE (`strUnDepositNo`='$serialNo')";
        $result = $db->RunQuery2($sql);
        $row = mysqli_fetch_array($result);
        $entryId = $row['entryId'];
        $sql = "UPDATE fin_transactions SET                                                                      
                    currencyId=$baseCurrencyId,
                    currencyRate='1',                                       
                    accPeriod=$accountPeriod
            WHERE entryId=$entryId";
        $db->RunQuery2($sql);

        $sqld = "DELETE FROM `fin_transactions_details` WHERE entryId=$entryId";
        $resultd = $db->RunQuery2($sqld);
        //=========================================================
    } else {
        $sql = "DELETE FROM `fin_bankundeposit_header` WHERE (`strUnDepositNo`='$serialNo')";
        $result1 = $db->RunQuery2($sql);
        //Add data to transaction header*******************************************
        $sql = "INSERT INTO fin_transactions (entryDate, strProgramType, documentNo, currencyId, currencyRate, transDetails, payMethodId, paymentNumber, accPeriod, userId, companyId, createdOn) VALUES
            (now(),'Bank Deposit','$serialNo',$baseCurrencyId,1,null,null,null,$accountPeriod,$userId,$companyId,now())";

        $db->RunQuery2($sql);
        $entryId = $db->insertId;
        //*********************************

        $sql = "INSERT INTO `fin_bankundeposit_header` (`strUnDepositNo`,intCreator,dtmCreateDate,intCompanyId,entryId) 
					VALUES ('$serialNo','$userId',now(),'$locationId',$entryId)";
        $result = $db->RunQuery2($sql);
    }
    //-----------delete and insert to detail table-----------------------
    if ($result) {
        $sql = "DELETE FROM `fin_bankundeposit_details` WHERE (`strUnDepositNo`='$serialNo')";
        $result2 = $db->RunQuery2($sql);

        $toSave = 0;
        $saved = 0;
        $rollBackFlag = 0;
        $totAmnt = 0;
        foreach ($arr as $arrVal) {
            $account = $arrVal['account'];
            $sql = "SELECT
						mst_financechartofaccounts.intId,
						mst_financechartofaccounts.strName,
						mst_financialsubtype.intId as finType,
						mst_financialsubtype.strName
						FROM
						mst_financechartofaccounts
						Inner Join mst_financialsubtype ON mst_financechartofaccounts.intFinancialTypeId = mst_financialsubtype.intId
						WHERE
						mst_financechartofaccounts.intId =  '$account'";
            $result = $db->RunQuery2($sql);
            $row = mysqli_fetch_array($result);
            $finType = $row['finType'];
            ;

            if ($finType == 10)
                $stakeHolderType = 'customer';
            else if ($finType == 18)
                $stakeHolderType = 'supplier';
            else
                $stakeHolderType = 'account';

            $docNo = $arrVal['docNo'];
            $depositTo = $arrVal['depositTo'];
            $payMethod = $arrVal['payMethod'];
            $refNo = $arrVal['refNo'];
            $refDate = $arrVal['refDate'];
            $posted = $arrVal['posted'];
            /* 				if($posted=='on'){
              $posted=1;
              }
              else{
              $posted=0;
              }
             */
            $dimention = $arrVal['dimention'];
            if ($dimention == '') {
                $dimention = '0';
            }
            $ammount = $arrVal['ammount'];
            $memo = $arrVal['memo'];

            if ($rollBackFlag != 1) {
                $sql = "INSERT INTO `fin_bankundeposit_details` (`strUnDepositNo`,`strDocNo`,`intAccount`,`intDepositTo`,`intPayMethod`,`strRefNo`,`dtmRefDate`,`intPosted`,`intDimention`,`dblAmmount`,`strMemo`) 
					VALUES ('$serialNo','$docNo','$account','$depositTo','$payMethod','$refNo','$refDate','$posted','$dimention','$ammount','$memo')";
                $result3 = $db->RunQuery2($sql);

                if ($finType == 10)//customer 
                    $amm = $ammount * (-1);

                if ($ammount < 0) {//if minus ammount
                    $credDebType = 'D';
                } else {
                    $credDebType = 'C';
                }
                $sql = "INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                                            ($entryId,'C',$account,$amm,'$memo',$dimention)";
                $result2 = $db->RunQuery2($sql);

                $sql = "INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                                            ($entryId,'D',$account,$amm,'$memo',$dimention)";
                $result2 = $db->RunQuery2($sql);

                $totAmnt+=$ammount;
                if ($result3 == 1) {
                    $saved++;
                } else {
                    $rollBackFlag = 1;
                }
                $toSave++;
            }
        }//end of foreach
        if ($rollBackFlag != 1) {

            /* 						$sql = "INSERT INTO `fin_transactions` (`dtDate`,`strProgramType`,intPearentHolder,`intStakeHolder`,`strStakeHolderType`,`strCredit/Debit`,`intAccountId`,`strParentDocumentNo`,`strDocumentNo`,`dblAmount`,`intUserId`,`intLocationId`) 
              VALUES (now(),'Bank Deposit', '$depositTo', '$depositTo', 'deposit to', 'D', '$depositTo', '', '$serialNo', '$totAmnt', '$userId', '$locationId')";
              $result4 = $db->RunQuery2($sql);
              if(!$result4){
              $rollBackFlag=1;
              }
             */
        }
    }
    //echo $rollBackFlag;

    if ($rollBackFlag == 1) {
        $db->RunQuery2('Rollback');
        $response['type'] = 'fail';
        $response['msg'] = $db->errormsg;
        $response['q'] = $sql;
    } else if (($result) && ($toSave == $saved)) {
        $db->RunQuery2('Commit');
        $response['type'] = 'pass';
        if ($editMode == 1)
            $response['msg'] = 'Updated successfully.';
        else
            $response['msg'] = 'Saved successfully.';

        $response['serialNo'] = $serialNo;
        $response['year'] = $year;
    }
    else {
        $db->RunQuery2('Rollback');
        $response['type'] = 'fail';
        $response['msg'] = $db->errormsg;
        $response['q'] = $sql;
    }

    $db->CloseConnection();
} else if ($requestType == 'delete') {
    try {
        $db->begin();
        $serialNo = $_REQUEST['serialNo'];


        $sql = "UPDATE `fin_bankundeposit_header` SET intStatus ='0'  
 WHERE (`strUnDepositNo`='$serialNo')  ";
        $result = $db->RunQuery2($sql);

        //==========UPDATE TRANS ACTION delete STATUS
        $sql = "SELECT fin_bankundeposit_header.entryId FROM fin_bankundeposit_header WHERE (`strUnDepositNo`='$serialNo')";
        $result = $db->RunQuery2($sql);
        $row = mysqli_fetch_array($result);
        $entryId = $row['entryId'];
        $sqld = "UPDATE `fin_transactions` SET delStatus=1 WHERE entryId=$entryId";
        $resultd = $db->RunQuery2($sqld);


        if (($result)) {
            $db->commit();
            $response['type'] = 'pass';
            $response['msg'] = 'Deleted successfully.';
        } else {
            $db->rollback(); //roalback
            $response['type'] = 'fail';
            $response['msg'] = $db->errormsg;
            $response['q'] = $sql;
        }
    } catch (Exception $e) {
        $db->rollback(); //roalback
        $response['type'] = 'fail';
        $response['msg'] = $e->getMessage();
        $response['q'] = $sql;
    }
}
//----------------------------------------
echo json_encode($response);

//-----------------------------------------	
function getnextDepositNo($companyId) {
    global $db;
    global $locationId;

    $sql = "SELECT
				Max(mst_financeaccountingperiod.dtmCreateDate),
				mst_financeaccountingperiod.dtmStartingDate, 
				substring(mst_financeaccountingperiod.dtmStartingDate,1,4) as fromY,
				substring(mst_financeaccountingperiod.dtmClosingDate,1,4) as toY 
				FROM mst_financeaccountingperiod ";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $accPeriod = $row['fromY'] . "-" . $row['toY'];

    $sql = "SELECT
				sys_finance_no.intBankUnDepositNo  
				FROM
				sys_finance_no
				WHERE
				sys_finance_no.intLocationId =  '$locationId' and sys_finance_no.intCompanyId = '$companyId'";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $depNo = $row['intBankUnDepositNo'];

    //------------------
    $sql = "SELECT
				mst_companies.strCode AS company,
				mst_companies.intId,
				mst_locations.intCompanyId,
				mst_locations.strCode AS location,
				mst_locations.intId
				FROM
				mst_companies
				Inner Join mst_locations ON mst_locations.intCompanyId = mst_companies.intId
				WHERE
				mst_locations.intId =  '$locationId' AND
				mst_companies.intId =  '$companyId'
				";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $companyCode = $row['company'];
    $locationCode = $row['location'];
    //---------------------

    $sql = "UPDATE `sys_finance_no` SET intBankUnDepositNo=intBankUnDepositNo+1 WHERE (`intLocationId`='$locationId' AND  sys_finance_no.intCompanyId = '$companyId')  ";
    $db->RunQuery2($sql);

    return $companyCode . "/" . $locationCode . "/" . $accPeriod . "/" . $depNo;
}

//---------------------------------------------------	
function getLatestAccPeriod($companyId) {
    global $db;
    $sql = "SELECT
                        MAX(mst_financeaccountingperiod.intId) AS accId,
                        mst_financeaccountingperiod.dtmStartingDate,
                        mst_financeaccountingperiod.dtmClosingDate,
                        mst_financeaccountingperiod.intStatus,
                        mst_financeaccountingperiod_companies.intCompanyId,
                        mst_financeaccountingperiod_companies.intPeriodId
                        FROM
                        mst_financeaccountingperiod
                        Inner Join mst_financeaccountingperiod_companies ON mst_financeaccountingperiod_companies.intPeriodId = mst_financeaccountingperiod.intId
                        WHERE
                        mst_financeaccountingperiod_companies.intCompanyId =  '$companyId' AND mst_financeaccountingperiod.intStatus = '1'
                        ORDER BY
                        mst_financeaccountingperiod.intId DESC
                        ";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $latestAccPeriodId = $row['accId'];
    return $latestAccPeriodId;
}

?>
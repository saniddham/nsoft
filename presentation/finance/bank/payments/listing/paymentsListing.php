<?php
session_start();
$backwardseperator = "../../../../../";

$location = $_SESSION['CompanyID'];

$companyId 	= $_SESSION['headCompanyId'];

$mainPath = $_SESSION['mainPath'];
$intUser  = $_SESSION["userId"];
$thisFilePath =  $_SERVER['PHP_SELF'];

/*$programName='Supplier Return Note';
$programCode='P0093';
*/
//$formName		  	= 'frmItem';
include  	"{$backwardseperator}dataAccess/permisionCheck2.inc";

//---------------------------------------------New Grid-----------------------------------------------------
include_once "../../../../../libraries/jqdrid/inc/jqgrid_dist.php";

				$sql = "select * from(SELECT
						fin_bankpayment_header.strBankPaymentNo AS VoucherNumber,'More' AS More,
						ROUND(fin_bankpayment_header.dblReceivedAmount,4) AS Amount,
						mst_financecurrency.strCode AS Currency,
						fin_bankpayment_header.strPerfInvoiceNo,
						fin_bankpayment_header.dtDate AS Date,
						CONCAT(mst_financechartofaccounts.strCode, '-',mst_financechartofaccounts.strName) AS BankAccount,
						fin_bankpayment_header.strRemarks AS Memo,
						fin_bankpayment_header.intCompanyId
						FROM
						fin_bankpayment_header
						left outer Join mst_financecurrency ON fin_bankpayment_header.intCurrency = mst_financecurrency.intId
						left outer Join mst_financechartofaccounts ON fin_bankpayment_header.intBankAccount = mst_financechartofaccounts.intId
						WHERE
						fin_bankpayment_header.intCompanyId =  '$companyId' AND
						fin_bankpayment_header.intStatus =  '1') as t where 1=1";

$invoiceLink = "../payments.php?paymentNo={VoucherNumber}";
$invoiceReport = "paymentsDetails.php?id={VoucherNumber}";

//Bank Account
$col["title"] = "Pay To"; // caption of column
$col["name"] = "BankAccount"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "6";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Date
$col["title"] = "Date"; // caption of column
$col["name"] = "Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Currency
$col["title"] = "Currency"; // caption of column
$col["name"] = "Currency"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Memo
$col["title"] = "Memo"; // caption of column
$col["name"] = "Memo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "8";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Amount
$col["title"] = "Amount"; // caption of column
$col["name"] = "Amount"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

// 	Reference Number
$col["title"] = "Reference Number"; // caption of column
$col["name"] = "strPerfInvoiceNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "center";
$col['link']	= $invoiceLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

//Invoice Number
$col["title"] = "Voucher Number"; // caption of column
$col["name"] = "VoucherNumber"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "6";
$col["align"] = "center";
$col['link']	= $invoiceLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

//View
$col["title"] = "View"; // caption of column
$col["name"] = "More"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "1";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $invoiceReport;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

////Amount
//$col["title"] = "Invoice Amount"; // caption of column
//$col["name"] = "Amount"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
//$col["width"] = "3";
//$col["align"] = "right";
//$col['link']	= $invoiceLink;
//$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
//$cols[] = $col;	$col=NULL;
//
////To be Paid
//$col["title"] = "To be Paid"; // caption of column
//$col["name"] = "balAmount"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
//$col["width"] = "3";
//$col["align"] = "right";
//$col['link']	= $invoiceLink;
//$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
//$cols[] = $col;	$col=NULL;

////View
//$col["title"] = "View"; // caption of column
//$col["name"] = "More"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
//$col["width"] = "1";
//$col["search"] = false;
//$col["align"] = "center";
//$col['link']	= $invoiceReport;
//$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
//$cols[] = $col;	$col=NULL;

$jq = new jqgrid('',$db);

$grid["caption"] 		= "BANK PAYMENT LISTING";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'VoucherNumber'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes

//<><><><><><>======================Compulsory Code==============================<><><><><><>
	$jq->set_options($grid);
	$jq->select_command = $sql;
	$jq->set_columns($cols);
	$jq->set_actions(array(	
		"add"=>false, // allow/disallow add
		"edit"=>false, // allow/disallow edit
		"delete"=>false, // allow/disallow delete
		"rowactions"=>false, // show/hide row wise edit/del/save option
		"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
		"export"=>true
	) 
	);

$out = $jq->render("list1");
//<><><><><><>======================Compulsory Code==============================<><><><><><>

//---------------------------------------------New Grid-----------------------------------------------------

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bank Payment Listing</title>

<!--<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
-->

</head>

<body>
<form id="frmReturnlisting" name="frmReturnlisting" method="get" action="paymentsListing.php">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<?php
/*$stat = $_GET['cboReturnStatus'];
if($stat==''){
$stat='2';
}
$supReturnApproveLevel = (int)getApproveLevel($programName);
*/?>

<div align="center">
		<!--<div class="trans_layoutL">
		  <div class="trans_text">BANK PAYMENT LISTING</div>-->
<!-------------------------------------------New Grid---------------------------------------------------->
<link rel="stylesheet" type="text/css" media="screen" href="../../../../../libraries/jqdrid/js/themes/smoothness/jquery-ui.custom.css"></link>	
<link rel="stylesheet" type="text/css" media="screen" href="../../../../../libraries/jqdrid/js/jqgrid/css/ui.jqgrid.css"></link>	

<script src="../../../../../libraries/jqdrid/js/jquery.min.js" type="text/javascript"></script>
<script src="../../../../../libraries/jqdrid/js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
<script src="../../../../../libraries/jqdrid/js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>	
<script src="../../../../../libraries/jqdrid/js/themes/jquery-ui.custom.min.js" type="text/javascript"></script>
<script src="../../../../../libraries/javascript/script.js" type="text/javascript"></script>
<!-------------------------------------------New Grid---------------------------------------------------->
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <!--<tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableBorder_allRound">
          <tr>
            <td width="10%" height="22" class="normalfnt">&nbsp;</td>
            <td width="22%">&nbsp;</td>
            <td width="17%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="11%" class="normalfnt">&nbsp;</td>
            <td width="15%">&nbsp;</td>
            </tr>
          <tr>
            <td height="22" class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            </tr>
          </table></td>
      </tr>-->
      <!--<tr>
        <td><div style="width:900px;height:300px;overflow:scroll" >
          <table width="100%" class="grid" >
            <tr class="gridHeader">
              <td width="19%">Payment Number</td>
              <td width="23%" >Bank Account</td>
              <td width="17%" >Status</td>
              <td width="21%">Date</td>
              <td width="20%">User</td>
              </tr>
                 <?php
				 
	 	 		 $sql = "SELECT
						fin_bankpayment_header.strBankPaymentNo,
						fin_bankpayment_header.intBankAccount,
						fin_bankpayment_header.intStatus,
						mst_financechartofaccounts.strName, 
						sys_users.strUserName,
						fin_bankpayment_header.dtmCreateDate
						FROM
						fin_bankpayment_header
						Inner Join mst_financechartofaccounts ON fin_bankpayment_header.intBankAccount = mst_financechartofaccounts.intId  
						Inner Join sys_users ON fin_bankpayment_header.intCreator = sys_users.intUserId 
						Inner Join mst_locations ON fin_bankpayment_header.intCompanyId = mst_locations.intId WHERE 
						fin_bankpayment_header.intCompanyId = '$company' 
						GROUP BY
						fin_bankpayment_header.strBankPaymentNo";
//echo $sql;
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					 $paymentNo=$row['strBankPaymentNo'];
					 $payTo=$row['strName'];
					 $date=$row['dtmCreateDate'];
					 $user=$row['strUserName'];
					 $status=$row['intStatus'];
					 if($status==0){
						$status='Inactive'; 
					 }
					 else if($status==1){
						$status='Active'; 
					 }
	  			 ?>
            <tr class="normalfnt">
              <td align="center" bgcolor="#FFFFFF"><?php if($row['intStatus']==1){?><a href="../payments.php?paymentNo=<?php echo $paymentNo?>" target="_blank"><?php } ?><?php echo $paymentNo;?><?php if($row['intStatus']==1){?></a><?php } ?></td>
              <td bgcolor="#FFFFFF" class="normalfntMid"><?php echo $payTo?></td>
              <td bgcolor="#FFFFFF" class="normalfntMid"><?php echo $status?></td>
              <td bgcolor="#FFFFFF" class="normalfntMid"><?php echo $date?></td>
              <td bgcolor="#FFFFFF" class="normalfntMid"><?php echo $user?></td>
              </tr>
              	<?php 
        		 } 
       			 ?>
            </table>
          </div></td>
      </tr>-->
      <tr>
        <td>
        <div align="center" style="margin:10px">   
			<?php echo $out?>
        </div>
        </td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound"><img src="../../../../../planning/img/Tclose.jpg" width="92" height="24" class="mouseover" /></td>
      </tr>
    </table></td>
    </tr>
  </table>

<!--  </div>
-->  </div>
</form>
</body>
</html>

<?php
session_start();
$backwardseperator = "../../../../";
$thisFilePath =  $_SERVER['PHP_SELF'];

include  "{$backwardseperator}dataAccess/permisionCheck2.inc";

$companyId 	= $_SESSION['headCompanyId'];

//---------------------------------------------New Grid-----------------------------------------------------
include_once "../../../../libraries/jqdrid/inc/jqgrid_dist.php";

$sql = "select * from(SELECT
		fin_transactions.entryId AS entryId1,
		fin_transactions_details.entryId AS entryId2,
		SUBSTRING(fin_transactions.entryDate,1,10) AS entryDate,
		fin_transactions.strProgramType,
		fin_transactions.personType,
		fin_transactions.personId,
		fin_transactions.documentNo AS docNo,
		fin_transactions.currencyId,
		fin_transactions.currencyRate,
		fin_transactions.transDetails AS memo,
		fin_transactions.payMethodId,
		fin_transactions.paymentNumber,
		fin_transactions.accPeriod,
		fin_transactions.userId,
		fin_transactions.companyId,
		fin_transactions.createdOn AS createDate,
		IF(fin_transactions.delStatus,'Inactive','Active') AS `status`,
		fin_transactions_details.`credit/debit` AS creditDebit,
		fin_transactions_details.accountId,
		ROUND(fin_transactions_details.amount,2) AS amounts,
		fin_transactions_details.details,
		fin_transactions_details.recType,
		fin_transactions_details.presentedDate,
		mst_financepaymentsmethods.strName,
		fin_bankpayment_header.intModifyer,
		fin_bankpayment_header.dtmModifyDate AS modifyDate,
		mst_financecurrency.strCode AS currency,
		CONCAT(mst_financechartofaccounts.strCode,'-',mst_financechartofaccounts.strName ) AS accounts,
		user1.strUserName AS creator,
		user2.strUserName AS modifyer
		FROM
		fin_transactions
		Left Outer Join fin_transactions_details ON fin_transactions.entryId = fin_transactions_details.entryId
		Left Outer Join mst_financepaymentsmethods ON fin_transactions.payMethodId = mst_financepaymentsmethods.intId
		Left Outer Join fin_bankpayment_header ON fin_transactions.entryId = fin_bankpayment_header.entryId AND fin_transactions_details.entryId = fin_bankpayment_header.entryId
		Left Outer Join mst_financecurrency ON mst_financecurrency.intId = fin_transactions.currencyId
		Left Outer Join mst_financechartofaccounts ON fin_transactions_details.accountId = mst_financechartofaccounts.intId
		Left Outer Join sys_users AS user1 ON fin_transactions.userId = user1.intUserId
		Left Outer Join sys_users AS user2 ON fin_bankpayment_header.intModifyer = user2.intUserId
		WHERE
		fin_transactions.companyId =  '$companyId' AND
		fin_transactions.strProgramType =  'Bank Payment'
		) as t where 1=1";

//Key Item
$col["title"] 	= "Documents No."; // caption of column
$col["name"] 	= "docNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "8";
$col["align"] 	= "left";
$cols[] = $col;	$col=NULL;

//Currency
$col["title"] 	= "Currency"; // caption of column
$col["name"] 	= "currency"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
$col["align"] 	= "center";
$cols[] = $col;	$col=NULL;

//Amounts
$col["title"] 	= "Amounts"; // caption of column
$col["name"] 	= "amounts"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "4";
$col["align"] 	= "right";
$cols[] = $col;	$col=NULL;

//Credit/Debit
$col["title"] 	= "Cr/Dr"; // caption of column
$col["name"] 	= "creditDebit"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "2";
$col["align"] 	= "center";
$cols[] = $col;	$col=NULL;

//Accounts
$col["title"] 	= "Accounts"; // caption of column
$col["name"] 	= "accounts"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "7";
$col["align"] 	= "left";
$cols[] = $col;	$col=NULL;

//Entry Date
$col["title"] = "Entry Date"; // caption of column
$col["name"] = "entryDate"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

////Name
//$col["title"] 	= "Name"; // caption of column
//$col["name"] 	= "customer"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
//$col["width"] 	= "8";
//$col["align"] 	= "left";
//$cols[] = $col;	$col=NULL;

//Memo
$col["title"] 	= "Memo"; // caption of column
$col["name"] 	= "memo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "4";
$col["align"] 	= "left";
$cols[] = $col;	$col=NULL;

//Creator
$col["title"] 	= "Created By"; // caption of column
$col["name"] 	= "creator"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "4";
$col["align"] 	= "left";
$cols[] = $col;	$col=NULL;

//Date
$col["title"] = "Created Date"; // caption of column
$col["name"] = "createDate"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "6";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Modifyer
$col["title"] = "Modified by"; // caption of column
$col["name"] = "modifyer"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Date
$col["title"] = "Modified Date"; // caption of column
$col["name"] = "modifyDate"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "6";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Status
$col["title"] = "Status"; // caption of column
$col["name"] = "status"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

////Deleter
//$col["title"] = "Deleted By"; // caption of column
//$col["name"] = ""; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
//$col["width"] = "3";
//$col["align"] = "left";
//$cols[] = $col;	$col=NULL;
//
////Date
//$col["title"] = "Deleted Date"; // caption of column
//$col["name"] = ""; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
//$col["width"] = "3";
//$col["align"] = "center";
//$cols[] = $col;	$col=NULL;

$jq = new jqgrid('',$db);

$grid["caption"] 		= "Audit Trial Listing - Bank Payments";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'entryId1'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes

//<><><><><><>======================Compulsory Code==============================<><><><><><>
	$jq->set_options($grid);
	$jq->select_command = $sql;
	$jq->set_columns($cols);
	$jq->set_actions(array(	
		"add"=>false, // allow/disallow add
		"edit"=>false, // allow/disallow edit
		"delete"=>false, // allow/disallow delete
		"rowactions"=>false, // show/hide row wise edit/del/save option
		"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
		"export"=>true
	) 
	);

$out = $jq->render("list1");
//<><><><><><>======================Compulsory Code==============================<><><><><><>

//---------------------------------------------New Grid-----------------------------------------------------

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Audit Trial Listing - Bank Payments</title>
</head>

<body>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
<div align="center">
<!-------------------------------------------New Grid---------------------------------------------------->
<link rel="stylesheet" type="text/css" media="screen" href="../../../../libraries/jqdrid/js/themes/smoothness/jquery-ui.custom.css"></link>	
<link rel="stylesheet" type="text/css" media="screen" href="../../../../libraries/jqdrid/js/jqgrid/css/ui.jqgrid.css"></link>	

<script src="../../../../libraries/jqdrid/js/jquery.min.js" type="text/javascript"></script>
<script src="../../../../libraries/jqdrid/js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
<script src="../../../../libraries/jqdrid/js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>	
<script src="../../../../libraries/jqdrid/js/themes/jquery-ui.custom.min.js" type="text/javascript"></script>
<script src="../../../../libraries/javascript/script.js" type="text/javascript"></script>
<!-------------------------------------------New Grid---------------------------------------------------->

<table  width="100%" border="0" align="center" bgcolor="#FFFFFF">
<tr>
 <td>
   <div align="center" style="margin:10px">
     <?php echo $out?>
   </div>
  </td>
 </tr>
</table>
</div>
</body>
</html>
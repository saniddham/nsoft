// JavaScript Document
var amStatus = "Auto";

function functionList()
{
		$('.taxGroup').change();
	//---------------------------------------------------
		amStatus = "Auto";
		//document.getElementById("chkAutoManual").style.display='none';
		document.getElementById("amStatus").style.display='none';
		$("#frmBankPayments #txtPaymentNo").attr("readonly","readonly");
	//---------------------------------------------------
	if($('#frmBankPayments #cboSearch').val()=='')
	{
	//---------------------------------------------------
		existingMsgDate = "";
		amStatus = "Auto";
		//document.getElementById("chkAutoManual").style.display='';
		document.getElementById("amStatus").style.display='';
		$('#frmBankPayments #txtPaymentNo').removeClass('validate[required]');
		$("#frmBankPayments #txtPaymentNo").attr("readonly","readonly");
	//---------------------------------------------------
	}
}


$(document).ready(function() {
	
  		$("#frmBankPayments").validationEngine();
		$('#frmBankPayments #cboBankAccount').focus();
		
		$("#insertRow").click(function(){
			insertRow();
		});
		//--------------------------------------- 
		$('.taxGroup').live('change',function(){
			calTax(this);
		});
		//-------------------------------------- 
		$('.delImg').click(function(){
			var rowCount = document.getElementById('tblBankPayment').rows.length;
			if(rowCount>2)
			$(this).parent().parent().remove();
			calTax(this);
		});
		//-------------------------------------
		$('.duplicate').change(function(){
			checkForDuplicate();
		});
		//--------------------------------------- 
		$('.ammount').keyup(function(){
			calTax(this);
		});
		//-------------------------------------- 
	//--------------------------------------------
		
  //permision for add 
  if(intAddx)
  {
 	$('#frmBankPayments #butNew').show();
	$('#frmBankPayments #butSave').show();
	$('#frmBankPayments #butPrint').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmBankPayments #butSave').show();
	//$('#frmBankPayments #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
	$('#frmBankPayments #butPrint').show();
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmBankPayments #butDelete').show();
	//$('#frmBankPayments #cboSearch').removeAttr('disabled');
  }
  
  //permision for view
  if(intViewx)
  {
	//$('#frmBankPayments #cboSearch').removeAttr('disabled');
  }
//===================================================================
 	$('#frmBankPayments #chkAutoManual').click(function(){
	  if($('#frmBankPayments #chkAutoManual').attr('checked'))
	  {
		  amStatus = "Auto";
		  $('#frmBankPayments #amStatus').val('Auto');
		  $('#frmBankPayments #txtPaymentNo').val('');
		  $("#frmBankPayments #txtPaymentNo").attr("readonly","readonly");
		  $('#frmBankPayments #txtPaymentNo').removeClass('validate[required]');
	  }
	  else
	  {
		  amStatus = "Manual";
		  $('#frmBankPayments #amStatus').val('Manual');
		  $('#frmBankPayments #txtPaymentNo').val('');
		  $("#frmBankPayments #txtPaymentNo").attr("readonly","");
		  $('#frmBankPayments #txtPaymentNo').focus();
		  $('#frmBankPayments #txtPaymentNo').addClass('validate[required]');
	  }
  });
//===================================================================
  
 //-------------------------------------------- 
  $('#frmBankPayments #cboCurrency').change(function(){
	    var currency = $('#cboCurrency').val();
	    var date = $('#txtDate').val();
		var url 		= "payments-db-get.php?requestType=loadExchangeRates";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"currency="+currency+'&date='+date,
			async:false,
			success:function(json){

					document.getElementById("txtRate").value=json.sellingRate;
					document.getElementById("exchSelling").value=json.sellingRate;
					document.getElementById("exchBuying").value=json.buyingRate;
					document.getElementById("rdoAverage").value=((eval(json.buyingRate) + eval(json.sellingRate))/2).toFixed(4)
					//document.getElementById("txtRate").innerHTML=json.buyingRate;
			}
		});
		
  });
  //-------------------------------------------------------
  $('#frmBankPayments #exchSelling').click(function(){
					document.getElementById("txtRate").value=document.getElementById("exchSelling").value;
					//document.getElementById("txtRate").innerHTML=json.buyingRate;
  });
  //-------------------------------------------------------
  $('#frmBankPayments #exchBuying').click(function(){
		document.getElementById("txtRate").value=document.getElementById("exchBuying").value;
  });
  //-------------------------------------------------------------
  //-------------------------------------------------------
  $('#frmBankPayments #rdoAverage').click(function(){
		document.getElementById("txtRate").value=document.getElementById("rdoAverage").value;
  });
  //-------------------------------------------------------------
  $('#frmBankPayments #chkEdit').click(function(){
	  if(document.getElementById("chkEdit").checked==true)
		$('#frmBankPayments #txtRate').removeAttr('disabled');
		else
		$('#frmBankPayments #txtRate').attr("disabled",true);

  });
  //------------------------------------------------------------
   $('#frmBankPayments #cboPaymentMethod').change(function(){
	    var payMethod = $('#cboPaymentMethod').val();
		if(payMethod==2){
		document.getElementById("rwChequeDetails").style.display='';
		}
		else{
		document.getElementById("rwChequeDetails").style.display='none';
		}
  });
	//-----------------------------------------------------------
    $('#frmBankPayments #butDelete').click(function(){
		var serial=URLEncode($('#frmBankPayments #txtPaymentNo').val());
		if($('#frmBankPayments #txtPaymentNo').val()=='')
		{
			//$('#frmBankDeposit #butDelete').validationEngine('showPrompt', 'Please select Deposit No.', 'fail');
		//	var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+serial+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = "payments-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											data:'requestType=delete&serialNo='+serial,
											async:false,
											success:function(json){
												
												$('#frmBankPayments #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												if(json.type=='pass')
												{
													$('#frmBankPayments').get(0).reset();
													clearForm();
													loadCombo_search();
													var t=setTimeout("alertDelete()",1000);return;
												}
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
		
	//	clearForm();
	});
	//-----------------------------------------------------------
 
  $('#frmBankPayments #butSave').click(function(){
  if(existingMsgDate == "")
  {
	//  calculateTotalVal();
	  var RecvedAmmount=parseFloat(document.getElementById("txtRecvAmmount").value).toFixed(2);
	  var totAccQty=callTotandTax().toFixed(2);
	  
	var requestType = '';
	if ($('#frmBankPayments').validationEngine('validate'))   
    { 
		  if(checkForDuplicate()==false){
			return false;  
		  }
		  else if(RecvedAmmount!=totAccQty){
			alert('Recieved Ammount does not tally with total Account Ammount');
			return false;  
		  }
		  if(document.getElementById('chkPosted').checked==true)
			var posted = 1;
		  else
			var posted = 0;
		  

		var data = "requestType=save";
		
			data+="&serialNo="		+	URLEncode($('#cboSearch').val());
			data+="&bankAccount="	+	$('#cboBankAccount').val();
			data+="&currency="	+	$('#cboCurrency').val();
			data+="&recvAmmount="			+	$('#txtRecvAmmount').val();
			data+="&invoiceNo="	+	URLEncode($('#txtPerfInvoiceNo').val()); // --> this is fnRefNo (Reference Number)
			data+="&paymentMethod="		+	$('#cboPaymentMethod').val();
			data+="&txtpaymentMethod="		+	$('#cboPaymentMethod').text();
			data+="&posted="	+	posted;
			data+="&remarks="			+	URLEncode($('#txtRemarks').val());
			data+="&date="		+	$('#txtDate').val();
			data+="&refDate="		+	$('#txtRefDate').val();
			data+="&rate="		+	$('#txtRate').val();
			data+="&refNo="	+	$('#txtRafNo').val();
			data+="&refOrganization=" +	$('#txtRefOrganization').val();
			data+="&amStatus="		+	amStatus;
			data+="&manualNo="		+	$('#txtPaymentNo').val();


			var rowCount = document.getElementById('tblBankPayment').rows.length;
			if(rowCount==1){
				alert("No Accounts selected");
				return false;				
			}
			var row = 0;
			
			var arr="[";
			
	$('#tblBankPayment >tbody >tr:not(:eq(0))').each(function(){
			arr += "{";
			arr += '"account":"'+		$(this).find('#cboAccounts').val() +'",' ;
			arr += '"ammount":"'+	$(this).find('#txtAmmount').val() +'",' ;
			arr += '"memo":"'+		URLEncode($(this).find('#txtMemo').val()) +'",' ;
			arr += '"dimention":"'+		$(this).find('#cboDimention').val() +'",' ;
			arr += '"payTo":"'+		$(this).find('#cboPayTo').val() +'",' ;
			arr += '"taxId":"'+		$(this).find('#cboTax').val() +'",' ;
			arr += '"taxAmmount":"'+		$(this).find(".taxWith").val() +'",' ;
			arr += '"trnTaxVal":'+		$(this).find(".taxWith").attr('id') +"," ;
			arr += '"taxVal":"'+		$(this).find(".taxWith").val() +'"' ;
			arr +=  '},';
	});
			
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;
		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = "payments-db-set.php";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			success:function(json){

					$('#frmBankPayments #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						//$('#frmSampleInfomations').get(0).reset();
						//saveImage(json.sampleNo,json.sampleYear,json.revisionNo);
						loadCombo_search();
						var t=setTimeout("alertx()",1000);
						$('#txtPaymentNo').val(json.serialNo);
					 	$('#cboSearch').val(json.serialNo);
						return;
					}
				},
			error:function(xhr,status){
					
					$('#frmBankPayments #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
	}
	else
	{
		$('#frmBankPayments #butSave').validationEngine('showPrompt', existingMsgDate,'fail');
		var t=setTimeout("alertx()",5000);
	}
   });
   

	//--------------refresh the form----------
	$('#frmBankPayments #butNew').click(function(){
//---------------------------------------------------
	existingMsgDate = "";
	amStatus = "Auto";
	$("#frmBankPayments #txtPaymentNo").attr("readonly","readonly");
	$('#frmBankPayments #chkAutoManual').attr('checked')
	$("#frmBankPayments #chkAutoManual").attr("disabled","");
	//document.getElementById("chkAutoManual").style.display='';
	document.getElementById("amStatus").style.display='';
	$('#frmBankPayments #txtPaymentNo').removeClass('validate[required]');
//--------------------------------------------------
		$('#frmBankPayments').get(0).reset();
		clearRows();
		$('#frmBankPayments #txtPaymentNo').val('');
		$('#frmBankPayments #cboSearch').val('');
		$('#frmBankPayments #cboBankAccount').val('');
		$('#frmBankPayments #cboCurrency').val('');
		$('#frmBankPayments #txtRecvAmmount').val('');
		$('#frmBankPayments #txtPerfInvoiceNo').val('');
		$('#frmBankPayments #cboPaymentMethod').val('');
		$('#frmBankPayments #chkPosted').val('');
		$('#frmBankPayments #txtRemarks').val('');
		$('#frmBankPayments #txtDate').val('');
		$('#frmBankPayments #txtRefDate').val('');
		$('#frmBankPayments #txtRate').val('');
		$('#frmBankPayments #txtRafNo').val('');
		$('#frmBankPayments #txtRefOrganization').val('');
		$('#frmBankPayments #txtTotalTax').val(0);
		$('#frmBankPayments #txtTotal').val(0);
		//$('#frmBankPayments #cboBankAccount').focus();
		$('#frmBankPayments #txtPerfInvoiceNo').focus();
		
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmBankPayments #txtDate').val(d);
		$('#frmBankPayments #txtRefDate').val(d);
		
		document.getElementById("rwChequeDetails").style.display='none';
	});
//----------------------------	
			$('.delImg').click(function(){
			    var rowCount = document.getElementById('tblBankPayment').rows.length;
				if(rowCount>2)
				$(this).parent().parent().remove();
				calculateTotalVal();
			});


//-----------------------------------
$('.accounts').change(function(){
	loadRecievedFrom(this);
});
//-------------------------------------
$('#butReport').click(function(){
	if($('#txtRetSupNo').val()!=''){
		window.open('../listing/rptAdvancePayments.php?retSupNo='+$('#txtRetSupNo').val()+'&year='+$('#txtRetSupYear').val());	
	}
	else{
		alert("There is no Return Note to view");
	}
});
//----------------------------------	
$('#butConfirm').click(function(){
	if($('#txtRetSupNo').val()!=''){
		window.open('../listing/rptAdvancePayments.php?retSupNo='+$('#txtRetSupNo').val()+'&year='+$('#txtRetSupYear').val()+'&approveMode=1');	
	}
	else{
		alert("There is no Return Note to confirm");
	}
});
//-----------------------------------------------------
$('#butClose').click(function(){
		//load('main.php');	
});
//--------------refresh the form-----------------------
	$('#frmBankPayments #butNew').click(function(){
		$('#frmBankPayments').get(0).reset();
		clearRows();
		$('#frmBankPayments #txtPaymentNo').val('');
		$('#frmBankPayments #cboSearch').val('');
		$('#frmBankPayments #cboBankAccount').val('');
		$('#frmBankPayments #cboCurrency').val('');
		$('#frmBankPayments #txtRecvAmmount').val('');
		$('#frmBankPayments #txtPerfInvoiceNo').val('');
		$('#frmBankPayments #cboPaymentMethod').val('');
		$('#frmBankPayments #chkPosted').val('');
		$('#frmBankPayments #txtRemarks').val('');
		$('#frmBankPayments #txtDate').val('');
		$('#frmBankPayments #txtRefDate').val('');
		$('#frmBankPayments #txtRate').val('');
		$('#frmBankPayments #txtRafNo').val('');
		$('#frmBankPayments #txtRefOrganization').val('');
		$('#frmBankPayments #txtTotalTax').val(0);
		$('#frmBankPayments #txtTotal').val(0);
		//$('#frmBankPayments #cboBankAccount').focus();
		$('#frmBankPayments #txtPerfInvoiceNo').focus();
		
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmBankPayments #txtDate').val(d);
		$('#frmBankPayments #txtRefDate').val(d);
		
		document.getElementById("rwChequeDetails").style.display='none';
	});
//-----------------------------------------------------

	$('#frmBankPayments #butPrint').click(function(){
		if($('#frmBankPayments #txtPaymentNo').val()=='')
		{
			$('#frmBankPayments #butPrint').validationEngine('showPrompt', 'Please select Voucher.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var myurl = 'listing/paymentsDetails.php?id='+$('#frmBankPayments #txtPaymentNo').val();
    		window.open(myurl); 
		}
	});

});

//----------end of ready -------------------------------

//-------------------------------------------------------
function loadMain()
{
	//alert(1);
/*	$("#butAddPendingDispatch").click(function(){
		popupWindow3('1');
		//$('#iframeMain1').attr('src','http://localhost/qpet/presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php?id='+$('#cboDispatchTo').val());
		$('#popupContact1').load('sampleDispatchPopup.php');	
		
	});*/
	//$("#iframeMain1").contents().find("#butAdd").click(abc);
	//$("#frmBankPaymentsPopup").contents().find("#butAdd").click(abc);
//	$('#frmBankPaymentsPopup #butClose').click(abc);
}

//-------------------------------------
function alertx()
{
	$('#frmBankPayments #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmBankPayments #butDelete').validationEngine('hide')	;
	$('#frmBankPayments #butPrint').validationEngine('hide') ;
}

//----------------------------------------------
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
//----------------------------------------------- 
function clearRows()
{
	var rowCount = document.getElementById('tblBankPayment').rows.length;

	for(var i=2;i<rowCount;i++)
	{
			document.getElementById('tblBankPayment').deleteRow(1);
	}
	document.getElementById('tblBankPayment').rows[1].cells[1].childNodes[0].value='';
	document.getElementById('tblBankPayment').rows[1].cells[2].childNodes[0].value='';
	document.getElementById('tblBankPayment').rows[1].cells[3].childNodes[0].value='';
	document.getElementById('tblBankPayment').rows[1].cells[4].childNodes[0].value='';
	document.getElementById('tblBankPayment').rows[1].cells[5].childNodes[0].value='';
	document.getElementById('tblBankPayment').rows[1].cells[6].childNodes[0].value='';
	document.getElementById('tblBankPayment').rows[1].cells[7].childNodes[0].value=0;
}
//-----------------------------------------------------------------
function insertRow()
{
	var tbl = document.getElementById('tblBankPayment');	
	var rows = tbl.rows.length;
	//tbl.rows[1].cells[5].childNodes[0].nodeValue;
	tbl.insertRow(rows);
	tbl.rows[rows].innerHTML = tbl.rows[rows-1].innerHTML;
	document.getElementById('tblBankPayment').rows[rows].cells[1].childNodes[0].value='';
	document.getElementById('tblBankPayment').rows[rows].cells[2].childNodes[0].value='';
	document.getElementById('tblBankPayment').rows[rows].cells[3].childNodes[0].value='';
	document.getElementById('tblBankPayment').rows[rows].cells[4].childNodes[0].value='';
	document.getElementById('tblBankPayment').rows[rows].cells[5].childNodes[0].value='';
	document.getElementById('tblBankPayment').rows[rows].cells[7].childNodes[0].value=0;
	
	//-------------------------------------
	$('.duplicate').change(function(){
		checkForDuplicate();
	});
	//-------------------------------------
	$('.accounts').change(function(){
		loadRecievedFrom(this);
	});
	//-------------------------------
	$('.delImg').click(function(){
		var rowCount = document.getElementById('tblBankPayment').rows.length;
		if(rowCount>2)
		$(this).parent().parent().remove();
		calTax(this);
	});
	//--------------------------------------- 
	$('.taxGroup').live('change',function(){
		calTax(this);
	});
	//--------------------------------------- 
	$('.ammount').keyup(function(){
		calTax(this);
	});
	//-------------------------------------- 
}			
//------------------------------------------------------------------------------
function calculateTotalVal(){
	var rowCount = document.getElementById('tblBankPayment').rows.length;
	var row = 0;
	var totQty=0;
	
	for(var i=1;i<rowCount;i++)
	{
		if(document.getElementById('tblBankPayment').rows[i].cells[3].childNodes[0].value==''){
		var qty=0;
		}
		else{
		var qty= 	document.getElementById('tblBankPayment').rows[i].cells[3].childNodes[0].value;
		}
		if(isNaN(qty)==true){
			qty=0; 
		}
		totQty+=parseFloat(qty);
	}
		totQty=totQty.toFixed(2);

	$('#txtTotal').val(totQty);
	$('#txtRecvAmmount').val(totQty);
	if(isNaN(totQty)==true){
		$('#txtTotal').val('0');
		$('#txtRecvAmmount').val('0');
	}
}
//--------------------------------------------------------------------------------
function checkForDuplicate(){
	var rowCount = document.getElementById('tblBankPayment').rows.length;
	//var row=this.parentNode.parentNode.rowIndex;
	var k=0;
	for(var row=1;row<rowCount;row++){
		for(var i=1;i<row;i++){
			if((document.getElementById('tblBankPayment').rows[i].cells[1].childNodes[0].value==document.getElementById('tblBankPayment').rows[row].cells[1].childNodes[0].value) && (document.getElementById('tblBankPayment').rows[i].cells[4].childNodes[0].value==document.getElementById('tblBankPayment').rows[row].cells[4].childNodes[0].value)){
				k=1;		
			}
		}
	}
	if(k==0){
		return true
	}
	else{
		alert("duplicate Accounts existing");
		return false
	}
}
//----------------------------------------------------------------------------------
function loadRecievedFrom(obj){
		var row=obj.parentNode.parentNode.rowIndex;
		var account=document.getElementById('tblBankPayment').rows[row].cells[1].childNodes[0].value;
		
		var url 		= "payments-db-get.php?requestType=loadCustORsuppliers";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"account="+account,
			async:false,
			success:function(json){
					if(json.recievdFrom =='<option value=""></option>')
					{
						 $('#frmBankPayments #cboPayTo').removeAttr('class')
					}
					else
					{
						 $('#frmBankPayments #cboPayTo').attr('class','validate[required]');
					}
					document.getElementById('tblBankPayment').rows[row].cells[4].childNodes[0].innerHTML=json.recievdFrom;
			}
		});
}
//---------------------------------------------------------------------------------
function calTax(obje){
	var amount = $(obje).parent().parent().find('td').eq(3).children().val();
	var taxId = callTaxProcess($(obje).parent().parent().find('td').eq(2).children().val());
	var arrTax = taxId.split('/');
	var operation = '';
	
	var jsonTaxCode="[ ";

	if(arrTax.length == 1)
	{
		operation = 'Isolated';
		jsonTaxCode += '{ "taxId":"'+taxId+'"},';
	}
	else if(arrTax.length > 1)
	{
		operation = arrTax[1];
		for(var i =0; i < arrTax.length; i=i+2)
		{
			jsonTaxCode += '{ "taxId":"'+arrTax[i]+'"},';
		}
	}
	jsonTaxCode = jsonTaxCode.substr(0,jsonTaxCode.length-1);
	jsonTaxCode += " ]";
	var url = "payments-db-get.php?requestType=getTaxValue&arrTaxCode="+jsonTaxCode+"&opType="+operation+"&valAmount="+amount;
	var obj = $.ajax({url:url,async:false});
	var values = obj.responseText.split('/');
	$(obje).parent().parent().find('td').eq(7).children().val(values[0]);
	if(arrTax.length == 1)
	{
		trnTax="[ ";
		trnTax += '{ "taxId":"'+taxId+'", "taxValue": "'+values[1]+'"},';
		trnTax = trnTax.substr(0,trnTax.length-1);
		trnTax += " ]";
	}
	else if(arrTax.length > 1)
	{
		trnTax="[ ";
		var k = 1;
		for(var i =0; i < arrTax.length; i=i+2)
		{
			trnTax += '{ "taxId":"'+arrTax[i]+'", "taxValue": "'+values[k]+'"},';
			k++;
		}
		trnTax = trnTax.substr(0,trnTax.length-1);
		trnTax += " ]";
	}
	$(obje).parent().parent().find('td').eq(7).children().attr('id',trnTax);
	//return obj.responseText;
	callTaxTotal();
 }
//-----------------------------------------------------------------------------
function callTaxProcess(taxGroupId)
{
	var url = "payments-db-get.php?requestType=getTaxProcess&taxGroupId="+taxGroupId;
	var obj = $.ajax({url:url,async:false});
	return obj.responseText;
}
//---------------------------------------------------------------------------------
function callTaxTotal()
{
	var taxTotal = 0.00;
	$(".taxWith").each( function(){
          taxTotal += eval($(this).val()==''?0.00:$(this).val());
	});
	$('#txtTotalTax').val(taxTotal.toFixed(2));
	callTotalAmount();
}
//---------------------------------------------------------------------------------
function callTotalAmount()
{
	var subTot=callTotat().toFixed(2);
	var taxTotal=parseFloat($('#txtTotalTax').val()).toFixed(2);
	var tot=parseFloat(taxTotal)+parseFloat(subTot);
	tot=tot.toFixed(2);
 	$('#txtTotal').val(tot);
 	$('#txtRecvAmmount').val(tot);
	if(isNaN(tot)==true){
		$('#txtTotal').val(0);
		$('#txtRecvAmmount').val(0);
	}
	
/*	var x = '10*3';
	alert(eval(x));
*/}
//---------------------------------------------------------------------------------

function callTotat()
{
	var subTotal = 0;
	var rowCount = document.getElementById('tblBankPayment').rows.length;
	var row = 0;
	
	for(var i=1;i<rowCount;i++)
	{
		if(document.getElementById('tblBankPayment').rows[i].cells[3].childNodes[0].value==''){
		var qty=0;
		}
		else{
		var qty= 	document.getElementById('tblBankPayment').rows[i].cells[3].childNodes[0].value;
		}
		if(isNaN(qty)==true){
			qty=0; 
		}
		subTotal+=parseFloat(qty);
	}
//	alert(subTotal);
	//$('#txtTotal').val(subTotal);
  return subTotal;
}
//------------------------------------------------------------------

function callTotandTax()
{
	var subTotal = 0;
	var subTax = 0;
	var rowCount = document.getElementById('tblBankPayment').rows.length;
	var row = 0;
	
	for(var i=1;i<rowCount;i++)
	{
		if(document.getElementById('tblBankPayment').rows[i].cells[3].childNodes[0].value==''){
		var qty=0;
		}
		else{
		var qty= 	document.getElementById('tblBankPayment').rows[i].cells[3].childNodes[0].value;
		}
		
		if(document.getElementById('tblBankPayment').rows[i].cells[7].childNodes[0].value==''){
		var tax=0;
		}
		else{
		var tax= 	document.getElementById('tblBankPayment').rows[i].cells[7].childNodes[0].value;
		}
		
		if(isNaN(qty)==true){
			qty=0; 
		}
		if(isNaN(qty)==true){
			tax=0; 
		}
		
		subTotal+=parseFloat(qty);
		subTax+=parseFloat(tax);
	}
//	alert(subTotal);
	//$('#txtTotal').val(subTotal);
  return (subTotal+subTax);
}
//-----------------------------------------------------------------------------
function loadCombo_search()
{
	var url 	= "payments-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmBankPayments #cboSearch').html(httpobj.responseText);
}
//------------------------------------------------------------------------------
function clearForm(){
	$('#frmBankPayments').get(0).reset();
	clearRows();
	$('#frmBankPayments #txtPaymentNo').val('');
	$('#frmBankPayments #cboSearch').val('');
	$('#frmBankPayments #cboBankAccount').val('');
	$('#frmBankPayments #cboCurrency').val('');
	$('#frmBankPayments #txtRecvAmmount').val('');
	$('#frmBankPayments #txtPerfInvoiceNo').val('');
	$('#frmBankPayments #cboPaymentMethod').val('');
	$('#frmBankPayments #chkPosted').val('');
	$('#frmBankPayments #txtRemarks').val('');
	$('#frmBankPayments #txtDate').val('');
	$('#frmBankPayments #txtRefDate').val('');
	$('#frmBankPayments #txtRate').val('');
	$('#frmBankPayments #txtRafNo').val('');
	$('#frmBankPayments #txtRefOrganization').val('');
	$('#frmBankPayments #txtTotalTax').val(0);
	$('#frmBankPayments #txtTotal').val(0);
	//$('#frmBankPayments #cboBankAccount').focus();
	$('#frmBankPayments #txtPerfInvoiceNo').focus();
	
	var currentTime = new Date();
	var month = currentTime.getMonth()+1 ;
	var day = currentTime.getDate();
	var year = currentTime.getFullYear();
	if(day<10)
	day='0'+day;
	if(month<10)
	month='0'+month;
	d=year+'-'+month+'-'+day;
	
	$('#frmBankPayments #txtDate').val(d);
	$('#frmBankPayments #txtRefDate').val(d);
	
	document.getElementById("rwChequeDetails").style.display='none';
}




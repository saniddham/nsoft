<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$location 	= $_SESSION['CompanyID'];
$company 	= $_SESSION['headCompanyId'];
$thisFilePath =  $_SERVER['PHP_SELF'];
include  	"{$backwardseperator}dataAccess/permisionCheck.inc";

// ======================Check Exchange Rate Updates========================
if($receiptRefNo == "")
{
	$status = "Adding";
}
else
{
	$status = "Changing";
}
$currentDate = date("Y-m-d");

$sql = "SELECT COUNT(*) AS 'no'
		FROM
		mst_financeexchangerate
		WHERE
		mst_financeexchangerate.dtmDate =  '$currentDate'
		";
$result = $db->RunQuery($sql);
$row= mysqli_fetch_array($result);
if($row['no']==0)
	{
		$str =  "Please Update Exchange Rates Before ".$status." Bank Deposit .";
		$str .= $row['NameList'];
		$maskClass="maskShow";
	}
	else
	{
		$maskClass="maskHide";
	}
// =========================================================================
        
$depositNo = $_REQUEST['depositNo'];
//$depositNo = '5000003';
if($depositNo){
$depositNo = $_REQUEST['depositNo'];
}
else{
$depositNo = $_GET['cboSearch'];
}

		 $sql = "SELECT
				fin_bankdeposit_header.intDepositTo,
				fin_bankdeposit_header.dtDate,
				fin_bankdeposit_header.dblRate,
				fin_bankdeposit_header.intCurrency,
				fin_bankdeposit_header.strRemarks,
				fin_bankdeposit_header.strFnRefNo
				FROM
				fin_bankdeposit_header
				WHERE
				fin_bankdeposit_header.strDepositNo =  '$depositNo'
				";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$deliverTo = $row['intDepositTo'];
					$date = $row['dtDate'];
					$rate = $row['dblRate'];
					$currency = $row['intCurrency'];
					$remarks = $row['strRemarks'];
					$financeRefNo = $row['strFnRefNo'];
				 }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bank Deposit</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $backwardseperator; ?>css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">


<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="deposit-js.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>
<script src="../../commanFunctions/numberExisting-js.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
</head>

<body onLoad="functionList();">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator.'Header.php'; ?></td>
	</tr>
    
    <div id="divMask" class="<?php echo $maskClass?> mask"> <?php echo $str; ?></div>
    
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>
<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<form id="frmBankDeposit" name="frmBankDeposit"  method="get" action="deposit.php" autocomplete="off">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
</table>

<div align="center">
  <div class="trans_layoutL">
    <div class="trans_text">Bank Deposit</div>
<table width="100%">
      <tr>
        <td class="normalfnt" width="50%"><img src="../../../../images/fb.png" width="18" height="19" /></td>
        <td align="right" width="50%"><img src="../../../../images/ff.png" width="18" height="19" /></td>
      </tr>
    <tr>
      <td  colspan="2">
        <table width="100%">
          <tr>
            <td align="right" width="40%"><span class="normalfnt"><strong>Search</strong></span></td>
            <td align="left" width="60%"><select name="cboSearch" id="cboSearch"  style="width:240px" onchange="submit();" >
              <option value=""></option>
              <?php  $sql = "SELECT DISTINCT
						fin_bankdeposit_header.strDepositNo,
						IFNULL(CONCAT(' - ',fin_bankdeposit_header.strFnRefNo),'') AS refNo
						FROM
						fin_bankdeposit_header
						WHERE
						fin_bankdeposit_header.intStatus =  '1' AND
						fin_bankdeposit_header.intCompanyId =  '$company'
						order by strDepositNo desc";
								
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							if($row['strDepositNo']==$depositNo)
							echo "<option value=\"".$row['strDepositNo']."\" selected=\"selected\">".$row['strDepositNo'].$row['refNo']."</option>";	
							else
							echo "<option value=\"".$row['strDepositNo']."\">".$row['strDepositNo'].$row['refNo']."</option>";
						}
          ?>
              </select></td>
            </tr>
          <tr>
            <td align="right">&nbsp;</td>
            <td align="left">&nbsp;</td>
          </tr>
          </table>        </span></td>
    </tr>
    <tr>
      <td colspan="2">
      <table width="100%" class="tableBorder_allRound">
    <tr>
      <td class="normalfnt">&nbsp;</td>
      <td class="normalfnt">Deposit Number</td>
      <td colspan="2"><span class="normalfnt">
        <input name="txtDepositNo" type="text" readonly="readonly" class="normalfntRight" id="txtDepositNo" style="width:230px; background-color:#F4FFFF;text-align:center; border:dotted; border-color:#F00" value="<?php echo $depositNo ?>" onblur="numberExisting(this,'Bank Deposit');"/>
        <input checked="checked" type="checkbox" name="chkAutoManual" id="chkAutoManual" style="display:none" />
        <input name="amStatus" type="text" class="normalfntBlue" id="amStatus" style="width:40px; background-color:#FFF; text-align:center; border:thin" disabled="disabled" value="(Auto)"/>
      </span></td>
      <td bgcolor="#FFFFFF" class="">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td class="normalfnt">&nbsp;</td>
      <td class="normalfnt">Reference Number</td>
      <td colspan="2"><input type="text" name="txtFnRefNo" id="txtFnRefNo" style="border-bottom-color:#00F; width:230px; text-align:center; border:double" value="<?php echo $financeRefNo ?>" /></td>
      <td bgcolor="#FFFFFF" class="">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td width="35" class="normalfnt">&nbsp;</td>
      <td width="129" class="normalfnt">Deposit To <span class="compulsoryRed">*</span></td>
      <td width="241"><span class="normalfntMid">
        <select name="cboDepositTo" id="cboDepositTo" style="width:96%"  class="validate[required]">
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_financechartofaccounts.intId,
							mst_financechartofaccounts.strCode,
							mst_financechartofaccounts.strName,
							mst_financechartofaccounts.intStatus
							FROM mst_financechartofaccounts 
							Inner Join mst_financechartofaccounts_companies ON mst_financechartofaccounts.intId = mst_financechartofaccounts_companies.intChartOfAccountId
							WHERE
							mst_financechartofaccounts_companies.intCompanyId =  '$company' 
							AND
							mst_financechartofaccounts.intStatus =  '1' AND (mst_financechartofaccounts.intFinancialTypeId='24' OR mst_financechartofaccounts.intFinancialTypeId='13')
							AND
							mst_financechartofaccounts.strType =  'Posting'
							ORDER BY strCode";//type=Bank
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$deliverTo)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strCode']."-".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strCode']."-".$row['strName']."</option>";	
					}
					?>
        </select>
      </span></td>
      <td width="106"><span class="normalfnt">Date <span class="compulsoryRed">*</span></span></td>
      <td width="340" bgcolor="#FFFFFF" class=""><input name="txtDate" type="text" value="<?Php echo ($date==''?date("Y-m-d"):$date) ?>"  class="validate[required]  txtbox" id="txtDate" style="width:98px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" onblur="backDateExisting(this,'Bank Deposit');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
      <td width="29">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="normalfnt">Currency <span class="compulsoryRed">*</span></span></td>
      <td><span class="normalfnt">
                <select name="cboCurrency" id="cboCurrency" style="width:100px"  class="validate[required]">
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_financecurrency.intId,
							mst_financecurrency.strCode
							FROM mst_financecurrency";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$currency)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strCode']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strCode']."</option>";	
					}
				?>
          </select>

        </span></td>
      <td><span class="normalfnt">Rate <span class="compulsoryRed">*</span></span></td>
      <td><span class="normalfnt"><span class="normalfntMid">
        <input type="radio" name="radio" id="exchSelling" value="" checked="checked"/>
        Selling
        <input type="radio" name="radio" id="exchBuying" value="" />
        Buying 
        <input class="rdoRate" type="radio" name="radio" id="rdoAverage" value="" />
		Average
<input type="text" name="txtRate" id="txtRate" style="width:75px; text-align:right" disabled="disabled" value="<?php echo $rate ?>"    class="validate[required,custom[number]]" />
        <input type="checkbox" name="chkEdit" id="chkEdit" />
        </span></span></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="normalfnt">Memo</span></td>
      <td colspan="2"><textarea name="txtRemarks" id="txtRemarks" cols="45" rows="2"><?php echo $remarks ?></textarea></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
      </table>
      </td>
      </tr>
      <tr>
        <td colspan="2" align="center">
        </td>
      </tr>
      </tr>
            <tr>
      <td colspan="2" align="right"><img src="../../../../images/Tadd.jpg" width="92" height="24" onclick="insertRow();" /></td>
      </tr>
    <tr>
      <td colspan="2"><table width="93%">
        <tr>
          <td>
                  <div style="overflow:scroll;width:900px;height:150px;" id="divGrid"><table width="100%" id="tblBankDeposit" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
        <tr class="">
          <td width="19" bgcolor="#FAD163" class="normalfntMid">Del</td>
          <td width="87" align="center"  bgcolor="#FAD163" class="normalfntMid"><strong>Account <span class="compulsoryRed">*</span></strong></td>
          <td width="100"  height="23" bgcolor="#FAD163" class="normalfntMid"><strong>Received From</strong></td>
          <td width="87" bgcolor="#FAD163" class="normalfntMid"  ><strong>PMt. <span class="compulsoryRed">*</span></strong></td>
          <td width="82" bgcolor="#FAD163" class="normalfntMid"  ><strong>Bank R. No.</strong></td>
          <td width="140" bgcolor="#FAD163" class="normalfntMid"  ><strong>R. Date</strong></td>
          <td width="44" bgcolor="#FAD163" class="normalfntMid"  ><span class="normalfnt"><strong>Posted</strong></span></td>
          <td width="91" bgcolor="#FAD163" class="normalfntMid"  ><strong>R. Org.</strong></td>
          <td width="88" bgcolor="#FAD163" class="normalfntMid"  ><strong><span class="compulsoryRed">*</span> Cost Center</strong></td>
          <td width="70" bgcolor="#FAD163" class="normalfntMid"  ><strong>Amount</strong></td>
          <td width="80" bgcolor="#FAD163" class="normalfntMid"  ><strong>Memo</strong></td>
        </tr>
        <?php
			            $sqlm = "SELECT *
				FROM fin_bankdeposit_details
				WHERE
				fin_bankdeposit_details.strDepositNo =  '$depositNo'";
				$resultm = $db->RunQuery($sqlm);
				$existingRws=0;
				$totAmnt=0;
				while($rowm=mysqli_fetch_array($resultm))
				{
					$existingRws++;
					$account 	 = $rowm['intAccount'];
					$recvFrom 		 = $rowm['intRecvFrom'];
					$payMethod 	 = $rowm['intPayMethod'];
					$refNo 		 = $rowm['strRefNo'];
					$refDate 	 = $rowm['dtmRefDate'];
					$posted 		 = $rowm['intPosted'];
					$rOrg 	 = $rowm['strRorg'];
					$dimention 		 = $rowm['intDimention'];
					$ammount 	 = $rowm['dblAmmount'];
					$memo 		 = $rowm['strMemo'];
					$totAmnt+=$ammount;
					//----------------
		$sql = "SELECT   
				mst_financechartofaccounts.intId, 
				mst_financialsubtype.strName
				FROM
				mst_financialsubtype
				Inner Join mst_financechartofaccounts ON mst_financialsubtype.intId = mst_financechartofaccounts.intFinancialTypeId
				WHERE
				mst_financechartofaccounts.intId =  '$account'";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		
		$chartOfAcc=$row['intId'];
		if($row['strName']=='Accounts Receivable'){
		$sql = "SELECT
				mst_financecustomeractivate.intCustomerId as id,
				mst_customer.strName
				FROM
				mst_financecustomeractivate
				Inner Join mst_customer ON mst_financecustomeractivate.intCustomerId = mst_customer.intId
				WHERE
				mst_financecustomeractivate.intCompanyId =  '$company' AND
				mst_financecustomeractivate.intChartOfAccountId =  '$chartOfAcc'
				ORDER BY strName";
		}
		else if($row['strName']=='Accounts Payable'){
		$sql = "SELECT
				mst_financesupplieractivate.intSupplierId as id,
				mst_supplier.strName
				FROM
				mst_financesupplieractivate
				Inner Join mst_supplier ON mst_financesupplieractivate.intSupplierId = mst_supplier.intId
				WHERE
				mst_financesupplieractivate.intCompanyId =  '$company' AND
				mst_financesupplieractivate.intChartOfAccountId =  '$chartOfAcc'
				ORDER BY strName";
		}
		else if($row['strName']=='Lease' || $row['strName']=='Loan' || $row['strName']=='Other Non Current Liabilities' || $row['strName']=='Other Current Liabilities'){
		$sql = "SELECT
				mst_finance_service_supplier_activate.intSupplierId as id,
				mst_finance_service_supplier.strName
				FROM
				mst_finance_service_supplier_activate
				Inner Join mst_finance_service_supplier ON mst_finance_service_supplier_activate.intSupplierId = mst_finance_service_supplier.intId
				WHERE
				mst_finance_service_supplier_activate.intCompanyId =  '$company' AND
				mst_finance_service_supplier_activate.intChartOfAccountId =  '$chartOfAcc'
				ORDER BY strName";
		}
		else if($row['strName']=='Interest In suspenses' || $row['strName']=='Prepayments' || $row['strName']=='Inter Company' || $row['strName']=='Other Current Assets' || $row['strName']=='Other Non - Current Assets'){
		$sql = "SELECT
				mst_finance_service_customer_activate.intCustomerId as id,
				mst_finance_service_customer.strName
				FROM
				mst_finance_service_customer_activate
				Inner Join mst_finance_service_customer ON mst_finance_service_customer_activate.intCustomerId = mst_finance_service_customer.intId
				WHERE
				mst_finance_service_customer_activate.intCompanyId =  '$company' AND
				mst_finance_service_customer_activate.intChartOfAccountId =  '$chartOfAcc'
				ORDER BY strName";
		}
		else{
		$sql = "";	
		}
		//echo $sql;
		
		$result = $db->RunQuery($sql);
		$combo="<option value=\"\"></option>";
		
		while($rows=mysqli_fetch_array($result))
		{
			if($rows['id']==$recvFrom)
			$combo .= "<option value=\"".$rows['id']."\" selected=\"selected\">".$rows['strName']."</option>";	
			else
			$combo .= "<option value=\"".$rows['id']."\">".$rows['strName']."</option>";	

		}
					//----------------
					?>
        <tr class="normalfnt">
          <td bgcolor="#FFFFFF" class="normalfntMid"><img src="../../../../images/del.png" width="15" height="15" class="delImg" /></td>
          <td bgcolor="#FFFFFF" class="normalfntMid"><select name="cboAccounts" id="cboAccounts" style="width:85px" class="validate[required]  accounts">
                        <option value="">&nbsp;</option>

                  <?php
					$sql = "SELECT
							mst_financechartofaccounts.intId,
							mst_financechartofaccounts.strCode,
							mst_financechartofaccounts.strName
							FROM mst_financechartofaccounts 
							Inner Join mst_financechartofaccounts_companies ON mst_financechartofaccounts.intId = mst_financechartofaccounts_companies.intChartOfAccountId
							WHERE
							mst_financechartofaccounts_companies.intCompanyId =  '$company'
							AND
							mst_financechartofaccounts.strType =  'Posting' 
							AND
							mst_financechartofaccounts.intStatus =  '1'
							ORDER BY strCode";
					$result = $db->RunQuery($sql);
					
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$account)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strCode']."-".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strCode']."-".$row['strName']."</option>";	
					}
					?>
            </select>
          </td>
          <td bgcolor="#FFFFFF" class="normalfntMid"><select name="select4" id="select4" style="width:100px"><?php echo $combo; ?>
          </select></td>
          <td  bgcolor="#FFFFFF" class="normalfntMid"><select name="select3" id="select2" style="width:85px"  class="validate[required]">
                        <option value="">&nbsp;</option>
                  <?php
					$sql = "SELECT
							mst_financepaymentsmethods.intId,
							mst_financepaymentsmethods.strName
							FROM mst_financepaymentsmethods
							WHERE
							mst_financepaymentsmethods.intStatus =  '1'
";//type=Bank
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$payMethod)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
					?>
            </select>
          </td>
          <td  bgcolor="#FFFFFF" class="normalfntMid"><input type="text" name="textfield2" id="textfield" style="width:80px" class="" value="<?php echo $refNo; ?>" />
          </td>
          <td  bgcolor="#FFFFFF"><input name="adviceDate3" type="text" value="<?php echo substr($refDate,0,10); ?>" class="validate[required] txtbox" id="<?php echo 'adviceDate$existingRws' ?>" style="width:98px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
          <td align="center"  bgcolor="#FFFFFF"><input type="checkbox" name="checkbox3" id="checkbox3" <?php if($posted==1){?> checked="checked" <?php } ?> /></td>
          <td  bgcolor="#FFFFFF"  class="normalfntMid"><input type="text" name="textfield4" id="textfield2" style="width:90px"  class="" value="<?php echo $rOrg ?>" />
          </td>
          
          <td  bgcolor="#FFFFFF" class="normalfntMid"><select name="select6" id="select6" style="width:85px" class="validate[required]">
                        <option value="">&nbsp;</option>
                  <?php
					$sql = "SELECT
							mst_financedimension.intId,
							mst_financedimension.strName
							FROM mst_financedimension
							WHERE
							mst_financedimension.intStatus =  '1'
							";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$dimention)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
					?>
            </select>
          </td>
          <td  bgcolor="#FFFFFF" class="normalfntMid"><input type="text" name="textfield7" id="textfield7" style="width:70px; text-align:right"  class="validate[required],custom[number]  calTot" value="<?php echo $ammount; ?>"/>
         </td>
          <td  bgcolor="#FFFFFF" class="normalfntMid"><input type="text" name="textfield5" id="textfield3" style="width:80px"  class="" value="<?php echo $memo; ?>"/>
         </td>
         
         
        </tr>
        <?php
		}
		if($existingRws==0){
		?>
        
        <tr class="normalfnt">
          <td bgcolor="#FFFFFF" class="normalfntMid"><img src="../../../../images/del.png" width="15" height="15" class="delImg" /></td>
          <td bgcolor="#FFFFFF" class="normalfntMid"><select name="cboAccounts" id="cboAccounts" style="width:85px" class="validate[required]  accounts">
                        <option value="">&nbsp;</option>

                  <?php
					$sql = "SELECT
							mst_financechartofaccounts.intId,
							mst_financechartofaccounts.strName,
							mst_financechartofaccounts.strCode
							FROM
							mst_financechartofaccounts
							Inner Join mst_financechartofaccounts_companies ON mst_financechartofaccounts.intId = mst_financechartofaccounts_companies.intChartOfAccountId
							WHERE
							mst_financechartofaccounts.intStatus =  '1' AND
							mst_financechartofaccounts_companies.intCompanyId =  '$company' AND
							mst_financechartofaccounts.strType =  'Posting'
							ORDER BY strCode";
					$result = $db->RunQuery($sql);
					
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$account)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strCode']."-".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strCode']."-".$row['strName']."</option>";	
					}
					?>
            </select>
          </td>
          <td bgcolor="#FFFFFF" class="normalfntMid"><select name="select4" id="select4" style="width:100px">
          </select></td>
          <td  bgcolor="#FFFFFF" class="normalfntMid"><select name="select3" id="select2" style="width:85px"  class="validate[required]">
                        <option value="">&nbsp;</option>
                  <?php
					$sql = "SELECT
							mst_financepaymentsmethods.intId,
							mst_financepaymentsmethods.strName
							FROM mst_financepaymentsmethods
							WHERE
							mst_financepaymentsmethods.intStatus =  '1'";
							
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$paymentMethod)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
					?>
            </select>
          </td>
          <td  bgcolor="#FFFFFF" class="normalfntMid"><input type="text" name="textfield2" id="textfield" style="width:80px" class="" />
          </td>
          <td  bgcolor="#FFFFFF"><input name="adviceDate3" type="text" value="<?php echo date("Y-m-d"); ?>" class="validate[required] txtbox" id="adviceDate1" style="width:98px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
          <td align="center"  bgcolor="#FFFFFF"><input type="checkbox" name="checkbox3" id="checkbox3" /></td>
          <td  bgcolor="#FFFFFF"  class="normalfntMid"><input type="text" name="textfield4" id="textfield2" style="width:90px"  class="" />
          </td>
          
          <td  bgcolor="#FFFFFF" class="normalfntMid"><select name="select6" id="select6" style="width:85px" class="validate[required]">
                        <option value="">&nbsp;</option>
                  <?php
					$sql = "SELECT
							mst_financedimension.intId,
							mst_financedimension.strName
							FROM mst_financedimension
							WHERE
							mst_financedimension.intStatus =  '1'
							";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
/*						if($row['intId']==$dimention)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";	
						else
*/						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
					?>
            </select>
          </td>
          <td  bgcolor="#FFFFFF" class="normalfntMid"><input type="text" name="textfield7" id="textfield7" style="width:70px; text-align:right"  class="validate[required],custom[number]  calTot"/>
         </td>
          <td  bgcolor="#FFFFFF" class="normalfntMid"><input type="text" name="textfield5" id="textfield3" style="width:80px"  class=""/>
         </td>
         
         
        </tr>
        
        <?php
		
		}
		?>
      </table>
      </div>
            </td>
          </tr>
          <tr>
          <td colspan="2" align="right">
          <table width="634">
                    <tr>
                      <td width="322" align="right" class="normalfntRight">&nbsp;</td>
                      <td width="131" align="right"><span class="normalfntRight">Total</span></td>
                      <td width="70" class="normalfnt"><span class="normalfntMid">
                        <input name="txtTotal" type="text" disabled="disabled"  id="txtTotal"  style="width:70px; text-align:right" value="<?php echo $totAmnt; ?>"  />
                      </span></td>
                      <td width="91" class="normalfnt">&nbsp;</td>
                    </tr>
                  </table>
          </td>
          </tr>
        </table></td>
    </tr>
      <tr>
        <td colspan="2">
          <table width="100%">
            <tr>
              <td width="100%" height="34" class="tableBorder_allRound"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                <tr>
                  <td width="100%" align="center" bgcolor=""><img border="0" src="../../../../images/Tnew.jpg" alt="New" name="butNew" width="92" height="24" class="mouseover" id="butNew" tabindex="28"/><img src="../../../../images/Tsave.jpg" width="92" height="24" id="butSave" name="butSave"  class="mouseover" /><img style="display:none" border="0" src="../../../../images/Tprint.jpg" alt="Print" name="butPrint" width="92" height="24" class="mouseover" id="butPrint" tabindex="25"/><img border="0" src="../../../../images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="25"/><a href="../../../../main.php"><img src="../../../../images/Tclose.jpg" alt="Close" name="Close" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                </tr>
              </table></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
    
</div>
</div>
</form>
</body>
</html>
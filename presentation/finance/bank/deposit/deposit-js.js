// JavaScript Document
var amStatus = "Auto";

function functionList()
{
	//---------------------------------------------------
		amStatus = "Auto";
		//document.getElementById("chkAutoManual").style.display='none';
		document.getElementById("amStatus").style.display='none';
		$("#frmBankDeposit #txtDepositNo").attr("readonly","readonly");
	//---------------------------------------------------
	if($('#frmBankDeposit #cboSearch').val()=='')
	{
	//---------------------------------------------------
		existingMsgDate = "";
		amStatus = "Auto";
		//document.getElementById("chkAutoManual").style.display='';
		document.getElementById("amStatus").style.display='';
		$('#frmBankDeposit #txtDepositNo').removeClass('validate[required]');
		$("#frmBankDeposit #txtDepositNo").attr("readonly","readonly");
	//---------------------------------------------------
	}
}
$(document).ready(function() {
	
  		$("#frmBankDeposit").validationEngine();
		$('#frmBankDeposit #cboCustomer').focus();
		
		$("#insertRow").click(function(){
			insertRow();
			//-------------------------------------
			$('.calTot').keyup(function(){
				calculateTotalVal();
			});
			//-------------------------------------
			$('.delImg').click(function(){
			    var rowCount = document.getElementById('tblBankDeposit').rows.length;
				if(rowCount>2)
				$(this).parent().parent().remove();
			});
			//-------------------------------------
			$('.duplicate').change(function(){
				checkForDuplicate();
			});
			//-------------------------------------
		});
		
	//--------------------------------------------
		
  //permision for add 
  if(intAddx)
  {
 	$('#frmBankDeposit #butNew').show();
	$('#frmBankDeposit #butSave').show();
	$('#frmBankDeposit #butPrint').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmBankDeposit #butSave').show();
	//$('#frmBankDeposit #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
	$('#frmBankDeposit #butPrint').show();
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmBankDeposit #butDelete').show();
	//$('#frmBankDeposit #cboSearch').removeAttr('disabled');
  }
  
  //permision for view
  if(intViewx)
  {
	//$('#frmBankDeposit #cboSearch').removeAttr('disabled');
  }
  //===================================================================
 	$('#frmBankDeposit #chkAutoManual').click(function(){
	  if($('#frmBankDeposit #chkAutoManual').attr('checked'))
	  {
		  amStatus = "Auto";
		  $('#frmBankDeposit #amStatus').val('Auto');
		  $('#frmBankDeposit #txtDepositNo').val('');
		  $("#frmBankDeposit #txtDepositNo").attr("readonly","readonly");
		  $('#frmBankDeposit #txtDepositNo').removeClass('validate[required]');
	  }
	  else
	  {
		  amStatus = "Manual";
		  $('#frmBankDeposit #amStatus').val('Manual');
		  $('#frmBankDeposit #txtDepositNo').val('');
		  $("#frmBankDeposit #txtDepositNo").attr("readonly","");
		  $('#frmBankDeposit #txtDepositNo').focus();
		  $('#frmBankDeposit #txtDepositNo').addClass('validate[required]');
	  }
  });
//===================================================================
  
 //-------------------------------------------- 
  $('#frmBankDeposit #cboCurrency').change(function(){
	    var currency = $('#cboCurrency').val();
	    var date = $('#txtDate').val();
		var url 		= "deposit-db-get.php?requestType=loadExchangeRates";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"currency="+currency+'&date='+date,
			async:false,
			success:function(json){

					document.getElementById("txtRate").value=json.sellingRate;
					document.getElementById("exchSelling").value=json.sellingRate;
					document.getElementById("exchBuying").value=json.buyingRate;
					document.getElementById("rdoAverage").value=((eval(json.buyingRate) + eval(json.sellingRate))/2).toFixed(4);
					//document.getElementById("txtRate").innerHTML=json.buyingRate;
			}
		});
		
  });
  //-------------------------------------------------------
  $('#frmBankDeposit #exchSelling').click(function(){
					document.getElementById("txtRate").value=document.getElementById("exchSelling").value;
					//document.getElementById("txtRate").innerHTML=json.buyingRate;
  });
  //-------------------------------------------------------
  $('#frmBankDeposit #exchBuying').click(function(){
		document.getElementById("txtRate").value=document.getElementById("exchBuying").value;
  });
  //-------------------------------------------------------------
  //-------------------------------------------------------
  $('#frmBankDeposit #rdoAverage').click(function(){
		document.getElementById("txtRate").value=document.getElementById("rdoAverage").value;
  });
  //-------------------------------------------------------------
  $('#frmBankDeposit #chkEdit').click(function(){
	  if(document.getElementById("chkEdit").checked==true)
		$('#frmBankDeposit #txtRate').removeAttr('disabled');
		else
		$('#frmBankDeposit #txtRate').attr("disabled",true);

  });
  //------------------------------------------------------------
   $('#frmBankDeposit #cboPaymentMethod').change(function(){
	    var payMethod = $('#cboPaymentMethod').val();
		if(payMethod==2){
		document.getElementById("rwChequeDetails").style.display='';
		}
		else{
		document.getElementById("rwChequeDetails").style.display='none';
		}
  });
	//----------------------------------------------------------
	$('.calTot').keyup(function(){
		calculateTotalVal();
	});
	//-----------------------------------------------------------
    $('#frmBankDeposit #butDelete').click(function(){
		var serial=URLEncode($('#frmBankDeposit #txtDepositNo').val());
		if($('#frmBankDeposit #txtDepositNo').val()=='')
		{
			//$('#frmBankDeposit #butDelete').validationEngine('showPrompt', 'Please select Deposit No.', 'fail');
		//	var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
                    var val = $.prompt('Are you sure you want to delete "'+serial+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = "deposit-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											data:'requestType=delete&serialNo='+serial,
											async:false,
											success:function(json){
												
												$('#frmBankDeposit #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												if(json.type=='pass')
												{
													$('#frmBankDeposit').get(0).reset();
													clearForm();
													loadCombo_search();
													var t=setTimeout("alertDelete()",1000);return;
												}
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
		
	//	clearForm();
	});
	//-----------------------------------------------------------
 
  $('#frmBankDeposit #butSave').click(function(){
  if(existingMsgDate == "")
  {
	  calculateTotalVal();
	var requestType = '';
	if ($('#frmBankDeposit').validationEngine('validate'))   
    { 
		  if(checkForDuplicate()==false){
			return false;  
		  }
		var data = "requestType=save";
		
			data+="&serialNo="		+	URLEncode($('#cboSearch').val());
			data+="&depositTo="	+	$('#cboDepositTo').val();
			data+="&currency="	+	$('#cboCurrency').val();
			data+="&remarks="			+	URLEncode($('#txtRemarks').val());
			data+="&date="		+	$('#txtDate').val();
			data+="&refDate="		+	$('#txtRefDate').val();
			data+="&rate="		+	$('#txtRate').val();
			data+="&amStatus="		+	amStatus;
			data+="&manualNo="		+	$('#txtDepositNo').val();
			
			data+="&fnRefNo="		+	URLEncode($('#txtFnRefNo').val());

			var rowCount = document.getElementById('tblBankDeposit').rows.length;
			if(rowCount==1){
				alert("No Accounts selected");
				return false;				
			}
			var row = 0;
			
			var arr="[";
			
			for(var i=1;i<rowCount;i++)
			{
					 
					var account = 	document.getElementById('tblBankDeposit').rows[i].cells[1].childNodes[0].value;
					var recvFrom = 	document.getElementById('tblBankDeposit').rows[i].cells[2].childNodes[0].value;
					var payMethod = 	document.getElementById('tblBankDeposit').rows[i].cells[3].childNodes[0].value;
					var refNo = 	document.getElementById('tblBankDeposit').rows[i].cells[4].childNodes[0].value;
					var refDate = 	document.getElementById('tblBankDeposit').rows[i].cells[5].childNodes[0].value;
					var posted = 	document.getElementById('tblBankDeposit').rows[i].cells[6].childNodes[0].value;
					if(document.getElementById('tblBankDeposit').rows[i].cells[6].childNodes[0].checked==true)
					var posted = 1;
					if(document.getElementById('tblBankDeposit').rows[i].cells[6].childNodes[0].checked==false)
					var posted = 0;
					var rOrg = 	document.getElementById('tblBankDeposit').rows[i].cells[7].childNodes[0].value;
					var dimention = 	document.getElementById('tblBankDeposit').rows[i].cells[8].childNodes[0].value;
					var ammount = 	document.getElementById('tblBankDeposit').rows[i].cells[9].childNodes[0].value;
					var memo = 	document.getElementById('tblBankDeposit').rows[i].cells[10].childNodes[0].value;
					
					    arr += "{";
						arr += '"account":"'+		account +'",' ;
						arr += '"recvFrom":"'+		recvFrom +'",' ;
						arr += '"payMethod":"'+		payMethod +'",' ;
						arr += '"refNo":"'+		refNo  +'",' ;
						arr += '"refDate":"'+		refDate +'",' ;
						arr += '"posted":"'+		posted +'",' ;
						arr += '"rOrg":"'+		rOrg +'",' ;
						arr += '"dimention":"'+		dimention +'",' ;
						arr += '"ammount":"'+		ammount +'",' ;
						arr += '"memo":"'+		URLEncode(memo)  +'"' ;
						arr +=  '},';
						
			}
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;
		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = "deposit-db-set.php";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			success:function(json){

					$('#frmBankDeposit #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						//$('#frmSampleInfomations').get(0).reset();
						//saveImage(json.sampleNo,json.sampleYear,json.revisionNo);
						loadCombo_search();
						var t=setTimeout("alertx()",1000);
						$('#txtDepositNo').val(json.serialNo);
					 	$('#cboSearch').val(json.serialNo);
						return;
					}
				},
			error:function(xhr,status){
					
					$('#frmBankDeposit #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
	}
	else
	{
		$('#frmBankDeposit #butSave').validationEngine('showPrompt', existingMsgDate,'fail');
		var t=setTimeout("alertx()",5000);
	}
   });
   

	//--------------refresh the form----------
	$('#frmBankDeposit #butNew').click(function(){
//---------------------------------------------------
	existingMsgDate = "";
	amStatus = "Auto";
	$("#frmBankDeposit #txtDepositNo").attr("readonly","readonly");
	$('#frmBankDeposit #chkAutoManual').attr('checked')
	$("#frmBankDeposit #chkAutoManual").attr("disabled","");
	//document.getElementById("chkAutoManual").style.display='';
	document.getElementById("amStatus").style.display='';
	$('#frmBankDeposit #txtDepositNo').removeClass('validate[required]');
//--------------------------------------------------
		$('#frmBankDeposit').get(0).reset();
		clearRows();
		$('#frmBankDeposit #txtDepositNo').val('');
		$('#frmBankDeposit #cboSearch').val('');
		$('#frmBankDeposit #cboDepositTo').val('');
		$('#frmBankDeposit #cboCurrency').val('');
		$('#frmBankDeposit #txtRemarks').val('');
		$('#frmBankDeposit #txtDate').val('');
		$('#frmBankDeposit #txtRate').val('');
		$('#frmBankDeposit #txtTotal').val('');
		$('#frmBankDeposit #txtFnRefNo').val('');
		//$('#frmBankDeposit #cboDepositTo').focus();
		$('#frmBankDeposit #txtFnRefNo').focus();
		
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmBankDeposit #txtDate').val(d);
	});
//----------------------------	
			$('.delImg').click(function(){
			    var rowCount = document.getElementById('tblBankDeposit').rows.length;
				if(rowCount>2)
				$(this).parent().parent().remove();
			});

//-----------------------------------
$('.accounts').change(function(){
	loadRecievedFrom(this);
});
//-------------------------------------

$('#butReport').click(function(){
	if($('#txtRetSupNo').val()!=''){
		window.open('../listing/rptReturnToSupplier.php?retSupNo='+$('#txtRetSupNo').val()+'&year='+$('#txtRetSupYear').val());	
	}
	else{
		alert("There is no Return Note to view");
	}
});
//----------------------------------	
$('#butConfirm').click(function(){
	if($('#txtRetSupNo').val()!=''){
		window.open('../listing/rptReturnToSupplier.php?retSupNo='+$('#txtRetSupNo').val()+'&year='+$('#txtRetSupYear').val()+'&approveMode=1');	
	}
	else{
		alert("There is no Return Note to confirm");
	}
});
//-----------------------------------------------------
$('#butClose').click(function(){
		//load('main.php');	
});
//--------------refresh the form-----------------------
	$('#frmBankDeposit #butNew').click(function(){
		$('#frmBankDeposit').get(0).reset();
		clearRows();
		$('#frmBankDeposit #txtDepositNo').val('');
		$('#frmBankDeposit #cboSearch').val('');
		$('#frmBankDeposit #cboDepositTo').val('');
		$('#frmBankDeposit #cboCurrency').val('');
		$('#frmBankDeposit #txtRemarks').val('');
		$('#frmBankDeposit #txtDate').val('');
		$('#frmBankDeposit #txtRate').val('');
		$('#frmBankDeposit #txtTotal').val('');
		$('#frmBankDeposit #txtFnRefNo').val('');
		//$('#frmBankDeposit #cboDepositTo').focus();
		$('#frmBankDeposit #txtFnRefNo').focus();
		
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmBankDeposit #txtDate').val(d);
	});
//-----------------------------------------------------

	$('#frmBankDeposit #butPrint').click(function(){
		if($('#frmBankDeposit #txtDepositNo').val()=='')
		{
			$('#frmBankDeposit #butPrint').validationEngine('showPrompt', 'Please select Deposit.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var myurl = 'listing/depositDetails.php?id='+$('#frmBankDeposit #txtDepositNo').val();
    		window.open(myurl); 
		}
	});

});

//----------end of ready -------------------------------

//-------------------------------------
function alertx()
{
	$('#frmBankDeposit #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmBankDeposit #butDelete').validationEngine('hide')	;
	$('#frmBankDeposit #butPrint').validationEngine('hide') ;
}

//----------------------------------------------
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
//----------------------------------------------- 
function clearRows()
{
	var rowCount = document.getElementById('tblBankDeposit').rows.length;

	for(var i=2;i<rowCount;i++)
	{
			document.getElementById('tblBankDeposit').deleteRow(1);
	}
	document.getElementById('tblBankDeposit').rows[1].cells[1].childNodes[0].value='';
	document.getElementById('tblBankDeposit').rows[1].cells[2].childNodes[0].value='';
	document.getElementById('tblBankDeposit').rows[1].cells[3].childNodes[0].value='';
	document.getElementById('tblBankDeposit').rows[1].cells[4].childNodes[0].value='';
	//document.getElementById('tblBankDeposit').rows[1].cells[5].childNodes[0].value='';
	document.getElementById('tblBankDeposit').rows[1].cells[5].childNodes[0].id='adviceDate1';
	document.getElementById('tblBankDeposit').rows[1].cells[7].childNodes[0].value='';
	document.getElementById('tblBankDeposit').rows[1].cells[8].childNodes[0].value='';
	document.getElementById('tblBankDeposit').rows[1].cells[9].childNodes[0].value='';
	document.getElementById('tblBankDeposit').rows[1].cells[10].childNodes[0].value='';
}
//-----------------------------------------------------------------
function insertRow()
{
	var tbl = document.getElementById('tblBankDeposit');	
	var rows = tbl.rows.length;
	var k=rows;
	//tbl.rows[1].cells[5].childNodes[0].nodeValue;
	tbl.insertRow(rows);
	tbl.rows[rows].innerHTML = tbl.rows[rows-1].innerHTML;
	document.getElementById('tblBankDeposit').rows[rows].cells[1].childNodes[0].value='';
	document.getElementById('tblBankDeposit').rows[rows].cells[2].childNodes[0].value='';
	document.getElementById('tblBankDeposit').rows[rows].cells[3].childNodes[0].value='';
	document.getElementById('tblBankDeposit').rows[rows].cells[4].childNodes[0].value='';
	//document.getElementById('tblBankDeposit').rows[rows].cells[5].childNodes[0].value='';
	document.getElementById('tblBankDeposit').rows[rows].cells[5].childNodes[0].id='adviceDate'+k;
	document.getElementById('tblBankDeposit').rows[rows].cells[7].childNodes[0].value='';
	document.getElementById('tblBankDeposit').rows[rows].cells[8].childNodes[0].value='';
	document.getElementById('tblBankDeposit').rows[rows].cells[9].childNodes[0].value='';
	document.getElementById('tblBankDeposit').rows[rows].cells[10].childNodes[0].value='';
	
	//--------------------------------
	$('.accounts').change(function(){
	loadRecievedFrom(this);
	});
	//-------------------------------
	$('.delImg').click(function(){
		var rowCount = document.getElementById('tblBankDeposit').rows.length;
		if(rowCount>2)
		$(this).parent().parent().remove();
	});

	//-----------------------------------
}			
//------------------------------------------------------------------------------
function calculateTotalVal(){
	var rowCount = document.getElementById('tblBankDeposit').rows.length;
	var row = 0;
	var totQty=0;
	
	for(var i=1;i<rowCount;i++)
	{
		if(document.getElementById('tblBankDeposit').rows[i].cells[9].childNodes[0].value==''){
		var qty=0;
		}
		else{
		var qty= 	document.getElementById('tblBankDeposit').rows[i].cells[9].childNodes[0].value;
		}
		if(isNaN(qty)==true){
			qty=0; 
		}
		totQty+=parseFloat(qty);
	}
		if(isNaN(totQty)==true){
			totQty=0; 
		}
		totQty=totQty.toFixed(2);
		$('#txtTotal').val(totQty);
}
//--------------------------------------------------------------------------------
function checkForDuplicate(){
	var rowCount = document.getElementById('tblBankDeposit').rows.length;
	//var row=this.parentNode.parentNode.rowIndex;
	var k=0;
	for(var row=1;row<rowCount;row++){
		for(var i=1;i<row;i++){
			if((document.getElementById('tblBankDeposit').rows[i].cells[1].childNodes[0].value==document.getElementById('tblBankDeposit').rows[row].cells[1].childNodes[0].value) /*&& (document.getElementById('tblBankDeposit').rows[i].cells[2].childNodes[0].value==document.getElementById('tblBankDeposit').rows[row].cells[2].childNodes[0].value)*/)	{
				k=1;		
			}
		}
	}
	if(k==0){
		return true
	}
	else{
		alert("duplicate records existing");
		return false
	}
}
//----------------------------------------------------------------------------------
function loadRecievedFrom(obj){
		var row=obj.parentNode.parentNode.rowIndex;
		var account=document.getElementById('tblBankDeposit').rows[row].cells[1].childNodes[0].value;
		
		var url 		= "deposit-db-get.php?requestType=loadCustORsuppliers";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"account="+account,
			async:false,
			success:function(json){
					if(json.recievdFrom =='<option value=""></option>')
					{
						 $('#frmBankDeposit #select4').removeAttr('class')
					}
					else
					{
						 $('#frmBankDeposit #select4').attr('class','validate[required]');
					}
					
					document.getElementById('tblBankDeposit').rows[row].cells[2].childNodes[0].innerHTML=json.recievdFrom;
			}
		});
}
//-----------------------------------------------------------------------------
function loadCombo_search()
{
	var url 	= "deposit-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmBankDeposit #cboSearch').html(httpobj.responseText);
}
//------------------------------------------------------------------------------
function clearForm(){
		$('#frmBankDeposit').get(0).reset();
		clearRows();
		$('#frmBankDeposit #txtDepositNo').val('');
		$('#frmBankDeposit #cboSearch').val('');
		$('#frmBankDeposit #cboDepositTo').val('');
		$('#frmBankDeposit #cboCurrency').val('');
		$('#frmBankDeposit #txtRemarks').val('');
		$('#frmBankDeposit #txtDate').val('');
		$('#frmBankDeposit #txtRate').val('');
		$('#frmBankDeposit #txtTotal').val('');
		$('#frmBankDeposit #txtFnRefNo').val('');
		//$('#frmBankDeposit #cboDepositTo').focus();
		$('#frmBankDeposit #txtFnRefNo').focus();
		
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmBankDeposit #txtDate').val(d);
}

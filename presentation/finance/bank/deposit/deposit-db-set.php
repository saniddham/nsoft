<?php

session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$userId = $_SESSION['userId'];
$locationId = $_SESSION['CompanyID'];
$companyId = $_SESSION['headCompanyId'];
include "{$backwardseperator}dataAccess/Connector.php";
include "../../commanFunctions/CommanEditAndDelete.php";

$response = array('type' => '', 'msg' => '');


/////////// parameters /////////////////////////////
$requestType = $_REQUEST['requestType'];

$serialNo = $_REQUEST['serialNo'];
$depositTo = $_REQUEST['depositTo'];
$currency = $_REQUEST['currency'];
$remarks = $_REQUEST['remarks'];
$date = $_REQUEST['date'];
$rate = $_REQUEST['rate'];
$amStatus 	= $_REQUEST['amStatus'];
$manualNo 	= $_REQUEST['manualNo'];

$fnRefNo 	= $_REQUEST['fnRefNo'];

$arr = json_decode($_REQUEST['arr'], true);

//------------save---------------------------	
if ($requestType == 'save') {
    // ckeck Unrealize gain/loss for entry and if exist block edit and delete
    $chk = checkUnrealizeEntry('B.Deposit', $serialNo, $arr);
    if ($chk) {
        $response['type'] = 'fail';
        $response['msg'] = "You cannot allow this process! Bank Deposit has some Unrealize Gain Or Loss for Customer or Supplier";
        //echo json_encode($response);
    } else {
        $accountPeriod = getLatestAccPeriod($companyId);
        $db->OpenConnection();
        $db->RunQuery2('Begin');

        if ($serialNo == '') {
			if($amStatus == "Auto")
			{
				 $serialNo = getnextDepositNo($companyId);
			}
			else if($amStatus == "Manual")
			{
				$serialNo	= $manualNo;
			}
            $year = date('Y');
            $editMode = 0;
        } else {
            $editMode = 1;
        }
        //-----------delete and insert to header table-----------------------
        if ($editMode == 1) {
            $sql = "UPDATE `fin_bankdeposit_header` SET intDepositTo ='$depositTo', 
                    dtDate ='$date', 
                    dblRate ='$rate', 
                    intCurrency ='$currency', 
                    strRemarks ='$remarks', 
                    intModifyer ='$userId', 
                    intStatus ='1',
                    dtmModifyDate =now(),
					strFnRefNo = '$fnRefNo' 
                    WHERE (`strDepositNo`='$serialNo')";
            $result = $db->RunQuery2($sql);

            //========update the transaction deader====================
            $sql = "SELECT fin_bankdeposit_header.entryId FROM fin_bankdeposit_header WHERE (`strDepositNo`='$serialNo')";
            $result = $db->RunQuery2($sql);
            $row = mysqli_fetch_array($result);
            $entryId = $row['entryId'];

            $sql = "UPDATE fin_transactions SET 
                entryDate='$date',                                  
                currencyId=$currency,
                currencyRate='$rate',
                transDetails='$remarks',                    
                accPeriod=$accountPeriod
            WHERE entryId=$entryId";
            $db->RunQuery2($sql);

            $sqld = "DELETE FROM `fin_transactions_details` WHERE entryId=$entryId";
            $resultd = $db->RunQuery2($sqld);
            //=========================================================
        } else {
            $sql = "DELETE FROM `fin_bankdeposit_header` WHERE (`strDepositNo`='$serialNo')";
            $result1 = $db->RunQuery2($sql);

            //Add data to transaction header*******************************************
            $sql = "INSERT INTO fin_transactions (entryDate, strProgramType, documentNo, currencyId, currencyRate, transDetails, payMethodId, paymentNumber, accPeriod, userId, companyId, createdOn) VALUES
            ('$date','Bank Deposit','$serialNo',$currency,$rate,'$remarks',null,null,$accountPeriod,$userId,$companyId,now())";

            $db->RunQuery2($sql);
            $entryId = $db->insertId;
            //********************************************
            $sql = "INSERT INTO `fin_bankdeposit_header` (`strDepositNo`,`intDepositTo`,`dtDate`,dblRate,strRemarks,intCreator,dtmCreateDate,intCompanyId,intCurrency,entryId,strFnRefNo) 
                        VALUES ('$serialNo','$depositTo','$date','$rate','$remarks','$userId',now(),'$companyId','$currency',$entryId,'$fnRefNo')";
            $result = $db->RunQuery2($sql);
        }
        //-----------delete and insert to detail table-----------------------
        if ($result) {
            $sql = "DELETE FROM `fin_bankdeposit_details` WHERE (`strDepositNo`='$serialNo')";
            $result2 = $db->RunQuery2($sql);

            $toSave = 0;
            $saved = 0;
            $rollBackFlag = 0;
            $totAmnt = 0;
            foreach ($arr as $arrVal) {
                $account = $arrVal['account'];
                $sql = "SELECT
                        mst_financechartofaccounts.intId,
                        mst_financechartofaccounts.strName,
                        mst_financialsubtype.intId as finType,
                        mst_financialsubtype.strName
                    FROM
                        mst_financechartofaccounts
                        Inner Join mst_financialsubtype ON mst_financechartofaccounts.intFinancialTypeId = mst_financialsubtype.intId
                    WHERE
                        mst_financechartofaccounts.intId =  '$account'";
                $result = $db->RunQuery2($sql);
                $row = mysqli_fetch_array($result);
                $finType = $row['finType'];
                ;

                if ($finType == 10)
                    $personType = "'cus'";
                else if ($finType == 18)
                    $personType = "'sup'";
				else if ($finType == 26 || $finType == 16 || $finType == 17 || $finType == 28)
                     $personType = "'osup'";
				else if ($finType == 22 || $finType == 21 || $finType == 11 || $finType == 12 || $finType == 9)
                     $personType = "'ocus'";
                else
                    $personType = 'null';

                $recvFrom = $arrVal['recvFrom'];
                if ($recvFrom == '') {
                    $recvFrom = 0;
                }
                $payMethod = $arrVal['payMethod'];
                $refNo = $arrVal['refNo'];
                $refDate = $arrVal['refDate'];
                $posted = $arrVal['posted'];

                $rOrg = $arrVal['rOrg'];
                $dimention = $arrVal['dimention'];
                if ($dimention == '') {
                    $dimention = '0';
                }
                $ammount = $arrVal['ammount'];
                $memo = $arrVal['memo'];

                if ($rollBackFlag != 1) {
                    $sql = "INSERT INTO `fin_bankdeposit_details` (`strDepositNo`,`intAccount`,`intRecvFrom`,`intPayMethod`,`strRefNo`,`dtmRefDate`,`intPosted`,`strRorg`,`intDimention`,`dblAmmount`,`strMemo`) 
                    VALUES ('$serialNo','$account','$recvFrom','$payMethod','$refNo','$refDate','$posted','$rOrg','$dimention','$ammount','$memo')";
                    $result3 = $db->RunQuery2($sql);

                    if ($finType == 10)//customer 
                        $amm = $ammount;

                    /* if($ammount<0){//if minus ammount
                      $credDebType='D';
                      }
                      else{
                      $credDebType='C';
                      } */
                    $sql = "INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId,personType, personId) VALUES 
                        ($entryId,'C',$account,$ammount,'$memo',$dimention,$personType,$recvFrom)";
                    $result2 = $db->RunQuery2($sql);

                    $totAmnt+=$ammount;
                    if ($result3 == 1) {
                        $saved++;
                    } else {
                        $rollBackFlag = 1;
                    }
                    $toSave++;
                }
            }//end of foreach
            if ($rollBackFlag != 1) {
                $sql = "INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                    ($entryId,'D',$depositTo,$totAmnt,'$remarks',null)";
                $result4 = $db->RunQuery2($sql);
                if (!$result4) {
                    $rollBackFlag = 1;
                }
            }
        }
        if ($rollBackFlag == 1) {
            $db->RunQuery2('Rollback');
            $response['type'] = 'fail';
            $response['msg'] = $db->errormsg;
            $response['q'] = $sql;
        } else if (($result) && ($toSave == $saved)) {
            $db->RunQuery2('Commit');
            $response['type'] = 'pass';
            if ($editMode == 1)
                $response['msg'] = 'Updated successfully.';
            else
                $response['msg'] = 'Saved successfully.';

            $response['serialNo'] = $serialNo;
            $response['year'] = $year;
        }
        else {
            $db->RunQuery2('Rollback');
            $response['type'] = 'fail';
            $response['msg'] = $db->errormsg;
            $response['q'] = $sql;
        }

        $db->CloseConnection();
    }
} else if ($requestType == 'delete') {
    //get Account Details
    $sql1 = "SELECT intAccount,intRecvFrom FROM fin_bankdeposit_details WHERE strDepositNo='$serialNo'";
    $result1 = $db->RunQuery($sql1);
    $x=0;
    while ($row1 = mysqli_fetch_array($result1)) {
        $arr1[$x]['account'] = $row1['intAccount'];
        $arr1[$x]['recvFrom'] = $row1['intRecvFrom'];
        ++$x;
    }   
    // ckeck Unrealize gain/loss for entry and if exist block edit and delete
    $chk = checkUnrealizeEntry('B.Deposit', $serialNo, $arr1);
    if ($chk) {
        $response['type'] = 'fail';
        $response['msg'] = "You cannot allow this process! Bank Deposit has some Unrealize Gain Or Loss for Customer or Supplier";
        //echo json_encode($response);
    } else {
        try {
            $db->begin();
            //----------------added by lasantha @ CAIT on 13/8/2012----------------------
            $sqlRp = "SELECT
				fin_customer_receivedpayments_main_details.strDocNo,
				fin_customer_receivedpayments_main_details.intCompanyId,
				fin_customer_receivedpayments_main_details.strDocType
				FROM
				fin_customer_receivedpayments_main_details
				Inner Join fin_bankdeposit_header ON fin_customer_receivedpayments_main_details.strDocNo = fin_bankdeposit_header.strDepositNo
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.intReceiptNo = fin_customer_receivedpayments_header.intReceiptNo AND fin_customer_receivedpayments_main_details.intAccPeriodId = fin_customer_receivedpayments_header.intAccPeriodId AND fin_customer_receivedpayments_main_details.intLocationId = fin_customer_receivedpayments_header.intLocationId AND fin_customer_receivedpayments_main_details.intCompanyId = fin_customer_receivedpayments_header.intCompanyId
				WHERE
				fin_customer_receivedpayments_main_details.strDocNo =  '$serialNo' AND
				fin_customer_receivedpayments_main_details.strDocType =  'B.Deposit' AND
				fin_customer_receivedpayments_main_details.intCompanyId =  '$companyId' AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0'";
            $resultRp = $db->RunQuery2($sqlRp);
            $sqlSp = "SELECT
				fin_supplier_payments_main_details.strDocNo,
				fin_supplier_payments_main_details.intCompanyId,
				fin_supplier_payments_main_details.strDocType
				FROM
				fin_supplier_payments_main_details
				Inner Join fin_supplier_payments_header ON fin_supplier_payments_main_details.intReceiptNo = fin_supplier_payments_header.intReceiptNo AND fin_supplier_payments_main_details.intAccPeriodId = fin_supplier_payments_header.intAccPeriodId AND fin_supplier_payments_main_details.intLocationId = fin_supplier_payments_header.intLocationId AND fin_supplier_payments_main_details.intCompanyId = fin_supplier_payments_header.intCompanyId AND fin_supplier_payments_main_details.strReferenceNo = fin_supplier_payments_header.strReferenceNo
				Inner Join fin_bankdeposit_header ON fin_bankdeposit_header.intCompanyId = fin_supplier_payments_main_details.intCompanyId AND fin_bankdeposit_header.strDepositNo = fin_supplier_payments_main_details.strDocNo
				WHERE
				fin_supplier_payments_header.intDeleteStatus =  '0' AND
				fin_supplier_payments_main_details.strDocType =  'B.Deposit' AND
				fin_supplier_payments_main_details.intCompanyId =  '$companyId' AND
				fin_supplier_payments_main_details.strDocNo =  '$serialNo'";
            $resultSp = $db->RunQuery2($sqlSp);
			$sqlOsp = "SELECT
				fin_other_payable_payments_main_details.strDocNo,
				fin_other_payable_payments_main_details.intCompanyId,
				fin_other_payable_payments_main_details.strDocType
				FROM
				fin_other_payable_payments_main_details
				Inner Join fin_other_payable_payments_header ON fin_other_payable_payments_main_details.intReceiptNo = fin_other_payable_payments_header.intReceiptNo AND fin_other_payable_payments_main_details.intAccPeriodId = fin_other_payable_payments_header.intAccPeriodId AND fin_other_payable_payments_main_details.intLocationId = fin_other_payable_payments_header.intLocationId AND fin_other_payable_payments_main_details.intCompanyId = fin_other_payable_payments_header.intCompanyId AND fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
				Inner Join fin_bankdeposit_header ON fin_bankdeposit_header.intCompanyId = fin_other_payable_payments_main_details.intCompanyId AND fin_bankdeposit_header.strDepositNo = fin_other_payable_payments_main_details.strDocNo
				WHERE
				fin_other_payable_payments_header.intDeleteStatus =  '0' AND
				fin_other_payable_payments_main_details.strDocType =  'B.Deposit' AND
				fin_other_payable_payments_main_details.intCompanyId =  '$companyId' AND
				fin_other_payable_payments_main_details.strDocNo =  '$serialNo'";
            $resultOsp = $db->RunQuery2($sqlOsp);
            if (!mysqli_num_rows($resultRp) && !mysqli_num_rows($resultSp) && !mysqli_num_rows($resultOsp)) {
                $serialNo = $_REQUEST['serialNo'];
                $sql = "UPDATE `fin_bankdeposit_header` SET intStatus ='0', intModifyer ='$userId'  
				WHERE (`strDepositNo`='$serialNo')  ";
                $result = $db->RunQuery2($sql);

                //==========UPDATE TRANS ACTION delete STATUS
                $sql = "SELECT fin_bankdeposit_header.entryId FROM fin_bankdeposit_header WHERE (`strDepositNo`='$serialNo')";
                $result = $db->RunQuery2($sql);
                $row = mysqli_fetch_array($result);
                $entryId = $row['entryId'];
                $sqld = "UPDATE `fin_transactions` SET delStatus=1 WHERE entryId=$entryId";
                $resultd = $db->RunQuery2($sqld);

                if (($result)) {
                    $db->commit();
                    $response['type'] = 'pass';
                    $response['msg'] = 'Deleted successfully.';
                } else {
                    $db->rollback(); //roalback
                    $response['type'] = 'fail';
                    $response['msg'] = $db->errormsg;
                    $response['q'] = $sql;
                }
            } else {
                $db->rollback();
                $response['type'] = 'fail';
                $response['msg'] = "You cannot allow this process! Bank Deposit has some payements";
            }
            //------------------------------------------------------------------------------------------------------
        } catch (Exception $e) {
            $db->rollback(); //roalback
            $response['type'] = 'fail';
            $response['msg'] = $e->getMessage();
            $response['q'] = $sql;
        }
    }
}
//----------------------------------------
echo json_encode($response);

//-----------------------------------------	
function getnextDepositNo($companyId) {
    global $db;
    global $locationId;

    $sql = "SELECT
				Max(mst_financeaccountingperiod.dtmCreateDate),
				mst_financeaccountingperiod.dtmStartingDate, 
				substring(mst_financeaccountingperiod.dtmStartingDate,1,4) as fromY,
				substring(mst_financeaccountingperiod.dtmClosingDate,1,4) as toY 
				FROM mst_financeaccountingperiod ";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $accPeriod = $row['fromY'] . "-" . $row['toY'];

    $sql = "SELECT
				sys_finance_no.intBankDepositNo  
				FROM
				sys_finance_no
				WHERE
				sys_finance_no.intLocationId =  '$locationId' and sys_finance_no.intCompanyId = '$companyId'";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $depNo = $row['intBankDepositNo'];

    //------------------
    $sql = "SELECT
				mst_companies.strCode AS company,
				mst_companies.intId,
				mst_locations.intCompanyId,
				mst_locations.strCode AS location,
				mst_locations.intId
				FROM
				mst_companies
				Inner Join mst_locations ON mst_locations.intCompanyId = mst_companies.intId
				WHERE
				mst_locations.intId =  '$locationId' AND
				mst_companies.intId =  '$companyId'
				";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $companyCode = $row['company'];
    $locationCode = $row['location'];
    //---------------------

    $sql = "UPDATE `sys_finance_no` SET intBankDepositNo=intBankDepositNo+1 WHERE (`intLocationId`='$locationId' AND  sys_finance_no.intCompanyId = '$companyId')  ";
    $db->RunQuery2($sql);

    return $companyCode . "/" . $locationCode . "/" . $accPeriod . "/" . $depNo;
}

//----------------------------------------
function getCreditPeriod($companyId) {
    global $db;
    $sql = "SELECT
				MAX(mst_financeaccountingperiod.intId) AS accId,
				mst_financeaccountingperiod.dtmStartingDate,
				mst_financeaccountingperiod.dtmClosingDate,
				mst_financeaccountingperiod.intStatus,
				mst_financeaccountingperiod_companies.intCompanyId,
				mst_financeaccountingperiod_companies.intPeriodId
				FROM
				mst_financeaccountingperiod
				Inner Join mst_financeaccountingperiod_companies ON mst_financeaccountingperiod_companies.intPeriodId = mst_financeaccountingperiod.intId
				WHERE
				mst_financeaccountingperiod_companies.intCompanyId =  '$companyId' AND mst_financeaccountingperiod.intStatus = '1'
				ORDER BY
				mst_financeaccountingperiod.intId DESC
				";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $latestAccPeriodId = $row['accId'];
    return $latestAccPeriodId;
}

//---------------------------------------
//---------------------------------------

function getLatestAccPeriod($companyId) {
    global $db;
    $sql = "SELECT
                        MAX(mst_financeaccountingperiod.intId) AS accId,
                        mst_financeaccountingperiod.dtmStartingDate,
                        mst_financeaccountingperiod.dtmClosingDate,
                        mst_financeaccountingperiod.intStatus,
                        mst_financeaccountingperiod_companies.intCompanyId,
                        mst_financeaccountingperiod_companies.intPeriodId
                        FROM
                        mst_financeaccountingperiod
                        Inner Join mst_financeaccountingperiod_companies ON mst_financeaccountingperiod_companies.intPeriodId = mst_financeaccountingperiod.intId
                        WHERE
                        mst_financeaccountingperiod_companies.intCompanyId =  '$companyId' AND mst_financeaccountingperiod.intStatus = '1'
                        ORDER BY
                        mst_financeaccountingperiod.intId DESC
                        ";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $latestAccPeriodId = $row['accId'];
    return $latestAccPeriodId;
}

//--------------------------------------------------------------------------------------------
?>

<?php
session_start();
$backwardseperator = "../../../../../";
$companyId = $_SESSION['CompanyID'];
$locationId = $_SESSION['CompanyID'];
$intUser  = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
include  	"{$backwardseperator}dataAccess/Connector.php";

include_once "../../../commanFunctions/Converter.php";

$companyId 	= $_SESSION['headCompanyId'];

$depositNo = $_REQUEST['id'];

$sql = "SELECT
		fin_bankdeposit_header.strDepositNo AS DepositNumber,
		'More' AS More,
		fin_bankdeposit_header.intDepositTo,
		fin_bankdeposit_header.intStatus,
		mst_financechartofaccounts.strName AS DepositTo,
		fin_bankdeposit_header.dtmCreateDate AS `Date`,
		mst_financecurrency.strCode AS Currency,
		fin_bankdeposit_header.strRemarks AS Memo,
		ROUND(sum(fin_bankdeposit_details.dblAmmount),2) AS Amount,
		fin_bankdeposit_header.dblRate,
		fin_bankdeposit_header.intCreator,
		fin_bankdeposit_header.strFnRefNo,
		user1.intUserId,
		user1.strUserName AS creater,
		user2.strUserName AS modifyer
		FROM
		fin_bankdeposit_header
		Left Outer Join mst_financechartofaccounts ON fin_bankdeposit_header.intDepositTo = mst_financechartofaccounts.intId
		Left Outer Join mst_financecurrency ON fin_bankdeposit_header.intCurrency = mst_financecurrency.intId
		Inner Join fin_bankdeposit_details ON fin_bankdeposit_header.strDepositNo = fin_bankdeposit_details.strDepositNo
		Left Outer Join sys_users AS user1 ON fin_bankdeposit_header.intCreator = user1.intUserId
		Left Outer Join sys_users AS user2 ON fin_bankdeposit_header.intModifyer = user2.intUserId
		WHERE
		fin_bankdeposit_header.intCompanyId =  '$companyId' AND
		fin_bankdeposit_header.strDepositNo =  '$depositNo'
		GROUP BY
		fin_bankdeposit_header.strDepositNo
		";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$depositTo 	= $row['DepositTo'];
	$amounts 	= $row['Amount'];
	$date	 	= $row['Date'];
	$currency 	= $row['Currency'];
	$amount		= $row['amount'];
	$payMethod	= $row['payMethod'];
	$rate		= $row['dblRate'];
	$creater	= $row['creater'];
	$modifyer	= $row['modifyer'];
	$memo		= $row['Memo'];
	$financeRefNo = $row['strFnRefNo'];
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bank Deposit</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../../css/promt.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/script.js"></script>

<script src="../../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:239px;
	top:172px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>

<form id="frmReceivedPaymentsDetails" name="frmReceivedPaymentsDetails" method="post" action="salesInvoiceDetails.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include '../../../../../reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><h3><strong>Deposit</strong></h3></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
  <tr>
    <td colspan="9" align="center" bgcolor="#FFDFCB">
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td width="17%" rowspan="2"><span class="normalfnt"><strong>Deposit To</strong></span></td>
    <td width="2%" rowspan="2" align="center" valign="middle"><strong>:</strong></td>
    <td width="30%" rowspan="2"><span class="normalfnt"><?php echo $depositTo ?></span></td>
    <td class="normalfnt"><strong>Deposit No.</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $depositNo ?></span></td>
    <td width="2%" colspan="2" rowspan="5"><div id="divPoNo" style="display:none"><?php echo $poNo ?>/<?php echo $year ?></div></td>
    </tr>
  <tr>
    <td width="2%">&nbsp;</td>
    <td width="17%" class="normalfnt"><strong>Deposit Date</strong></td>
    <td width="2%" align="center" valign="middle"><strong>:</strong></td>
    <td width="28%"><span class="normalfnt"><?php echo $date  ?></span></td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Deposit </strong><strong>Amount</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td class="normalfnt"><?php echo number_format($amounts,2)  ?></td>
    <td><span class="normalfnt"><strong>Currency</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $currency  ?></span></td>
    </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><span class="normalfntGrey">(Amount in Word)</span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td colspan="4" class="normalfnt">
      <?php
		$val = $amounts;
        $inWord= convert_number($val);   
        $sence=explode(".",number_format($val,2));
        $sent=convert_number($sence[1]);
        if(strlen($sence[1])>0)
		{
        	$inWord=$inWord." and ".$sent." cents";
        }
       echo $inWord;
    ?>
      </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="6" class="normalfntGrey">
      <?php
		echo "Exchange Rate: <strong>Rs. ".number_format($rate,4).""."/".$symbol." '".number_format($amounts*$rate, 2)."'</strong>";
	?>
      <br />
      (<?php
            $val = $amounts*$rate;
            $inWord= convert_number($val);
            $sence=explode(".",number_format($val,2));
            $sent=convert_number($sence[1]);
            if(strlen($sence[1])>0)
            {
                $inWord=$inWord." and ".$sent." cents";
            }
           echo $inWord;
        ?>)
      <br /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Memo</strong></td>
    <td align="center"><strong>:</strong></td>
    <td colspan="4"><span class="normalfnt"><?php echo $memo ?></span></td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Reference Number</strong></td>
    <td align="center"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $financeRefNo ?></span></td>
    <td class="normalfntGrey">&nbsp;</td>
    <td class="normalfntGrey">&nbsp;</td>
    <td class="normalfntGrey">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="6" class="normalfntGrey">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="6" class="normalfntGrey">More Details</td>
    </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td colspan="6" class="normalfnt">
      <table width="100%" class="tableBorder_allRound">
        <tr>
        <td width="100%" style="vertical-align:top" >
          <table width="100%" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr>
              <td width="161"  height="22" class="normalfnt borderLine"><strong>Account</strong></td>
              <td width="173" class="normalfnt borderLine"><strong>Received From</strong></td>
              <td width="117" class="normalfnt borderLine"><strong>Payment Method</strong></td>
              <td width="106" class="normalfntMid borderLine"><strong>Bank Ref. No.</strong></td>
              <td width="84" class="normalfntMid borderLine"><strong>Ref.Date</strong></td>
              <td width="113" class="normalfntMid borderLine"><strong>Ref.Organization</strong></td>
              <!--<td width="7%" >Discount(%)</td>-->
             <!-- <td width="7%" >Tax Code</td>-->
              <td width="87" class="normalfntRight borderLine"><strong>Amount</strong></td>
              </tr>
            <?php 
			$totalTax = 0;
	  	  	$sql1 = "SELECT
					fin_bankdeposit_details.strDepositNo,
					mst_financechartofaccounts.intId AS AccId,
					mst_financechartofaccounts.strCode,
					mst_financechartofaccounts.strName AS account,
					mst_financepaymentsmethods.strName AS payMethod,
					fin_bankdeposit_details.strRefNo,
					fin_bankdeposit_details.dtmRefDate,
					fin_bankdeposit_details.strRorg,
					fin_bankdeposit_details.dblAmmount AS amount,
					fin_bankdeposit_details.intRecvFrom
					FROM
					fin_bankdeposit_details
					Inner Join mst_financechartofaccounts ON fin_bankdeposit_details.intAccount = mst_financechartofaccounts.intId
					Inner Join mst_financepaymentsmethods ON fin_bankdeposit_details.intPayMethod = mst_financepaymentsmethods.intId
					WHERE
					fin_bankdeposit_details.strDepositNo =  '$depositNo'
					";
			$result1 = $db->RunQuery($sql1);

			$totQty=0;
			$totAmmount=0;
		while($row=mysqli_fetch_array($result1))
		{
			$name = getName($row['AccId'], $row['intRecvFrom'], $companyId);
			$subAmount = $row['amount'];
	  ?>
	  <tr class="normalfnt borderLineIn"  bgcolor="#FFFFFF">
   	  <td class="normalfnt borderLineIn" height="50">&nbsp;<?php echo $row['strCode']; ?>-<?php echo $row['account']; ?>&nbsp;</td>
   	  <td class="normalfnt borderLineIn"><?php echo $name ?></td>
      <td class="normalfnt borderLineIn"><?php echo $row['payMethod']; ?>&nbsp;</td>
      <td class="normalfntMid borderLineIn" >&nbsp;<?php echo $row['strRefNo'] ?>&nbsp;</td>
      <td class="normalfntMid borderLineIn" >&nbsp;<?php echo substr($row['dtmRefDate'],0,10) ?>&nbsp;</td>
      <td class="normalfntMid borderLineIn" >&nbsp;<?php echo $row['strRorg'] ?>&nbsp;</td>
      <td class="normalfntRight borderLineIn" >&nbsp;<?php echo number_format($subAmount, 2) ?>&nbsp;</td>
</tr>
      <?php 
		}
	  ?>
            </table>
          </td>
        </tr>
        <tr>
        <td colspan="7" align="right"><strong>Total </strong>: <?php echo number_format($amounts,2)  ?>&nbsp;</td>
        </tr>
        </table>
    </td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
  </table>
  </td>
</tr>
<tr>
  <td class="normalfntGrey" align="center"><br /><br /></td>
</tr>
<tr>
<td>
<table width="100%">
<tr>
    <td align="left" class="normalfnt">&nbsp;</td>
    <td align="center" class="normalfntMid">
    <?php
	if($modifyer == NULL)
	{
		echo $creater;
	}
	else
	{
		echo $modifyer;
	}
	?>
    </td>
    <td width="30%" align="center" class="normalfntMid">&nbsp;</td>
    <td width="34%" align="right" class="normalfntRight">&nbsp;</td>
    <td width="34%" align="right" class="normalfntRight">&nbsp;</td>
    <td width="34%" align="right" class="normalfntRight">&nbsp;</td>
</tr>
<tr>
<td align="left" class="normalfnt">&nbsp;</td>
<td align="center" class="normalfntMid">........................................</td>
<td align="center" class="normalfntMid">....................................................</td>
<td align="right" class="normalfntMid">............................</td>
<td align="right" class="normalfntMid">............................</td>
<td align="right" class="normalfntMid">............................</td>
</tr>
<tr>
<td align="left" class="normalfnt">&nbsp;</td>
<td align="center" class="normalfntMid">Prepared By</td>
<td align="center" class="normalfntMid">Authorized by Accountant</td>
<td align="right" class="normalfntMid">Authorized By</td>
<td align="right" class="normalfntMid">Received By</td>
<td align="right" class="normalfntMid">Date</td>
</tr>
</table>
</td>
</tr>
<tr>
  <td class="normalfntGrey" align="center"><br /><br /></td>
</tr>
<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
</body>
</html>
// JavaScript Document
var amStatus = "Auto";

function functionList()
{
	//---------------------------------------------------
		amStatus = "Auto";
		document.getElementById("chkAutoManual").style.display='none';
		document.getElementById("amStatus").style.display='none';
		$("#frmFundsTransfer #txtFundTransafNo").attr("readonly","readonly");
	//---------------------------------------------------
	if($('#frmFundsTransfer #cboSearch').val()=='')
	{
	//---------------------------------------------------
		existingMsgDate = "";
		amStatus = "Auto";
		document.getElementById("chkAutoManual").style.display='';
		document.getElementById("amStatus").style.display='';
		$('#frmFundsTransfer #txtFundTransafNo').removeClass('validate[required]');
		$("#frmFundsTransfer #txtFundTransafNo").attr("readonly","readonly");
	//---------------------------------------------------
	}
}
$(document).ready(function() {
	
  		$("#frmFundsTransfer").validationEngine();
		$('#frmFundsTransfer #cboFundsTransferFrom').focus();
		
		$("#insertRow").click(function(){
			insertRow();
		});
		//-------------------------------------
		$('.calTot').keyup(function(){
			calculateTotalVal();
		});
		//-------------------------------------
		$('.delImg').click(function(){
			var rowCount = document.getElementById('tblFundsTransfer').rows.length;
			if(rowCount>2)
			$(this).parent().parent().remove();
			calculateTotalVal();
		});
		//-------------------------------------
		$('.duplicate').change(function(){
			checkForDuplicate();
		});
		//-------------------------------------
	//--------------------------------------------
		
  //permision for add 
  if(intAddx)
  {
 	$('#frmFundsTransfer #butNew').show();
	$('#frmFundsTransfer #butSave').show();
	$('#frmFundsTransfer #butPrint').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmFundsTransfer #butSave').show();
	//$('#frmFundsTransfer #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
	$('#frmFundsTransfer #butPrint').show();
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmFundsTransfer #butDelete').show();
	//$('#frmFundsTransfer #cboSearch').removeAttr('disabled');
  }
  
  //permision for view
  if(intViewx)
  {
	//$('#frmFundsTransfer #cboSearch').removeAttr('disabled');
  }
//===================================================================
 	$('#frmFundsTransfer #chkAutoManual').click(function(){
	  if($('#frmFundsTransfer #chkAutoManual').attr('checked'))
	  {
		  amStatus = "Auto";
		  $('#frmFundsTransfer #amStatus').val('Auto');
		  $('#frmFundsTransfer #txtFundTransafNo').val('');
		  $("#frmFundsTransfer #txtFundTransafNo").attr("readonly","readonly");
		  $('#frmFundsTransfer #txtFundTransafNo').removeClass('validate[required]');
	  }
	  else
	  {
		  amStatus = "Manual";
		  $('#frmFundsTransfer #amStatus').val('Manual');
		  $('#frmFundsTransfer #txtFundTransafNo').val('');
		  $("#frmFundsTransfer #txtFundTransafNo").attr("readonly","");
		  $('#frmFundsTransfer #txtFundTransafNo').focus();
		  $('#frmFundsTransfer #txtFundTransafNo').addClass('validate[required]');
	  }
  });
//===================================================================
  
 //-------------------------------------------- 
  $('#frmFundsTransfer #cboCurrency').change(function(){
	    var currency = $('#cboCurrency').val();
	    var date = $('#txtDate').val();
		var url 		= "fundsTransfer-db-get.php?requestType=loadExchangeRates";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"currency="+currency+'&date='+date,
			async:false,
			success:function(json){

					document.getElementById("txtRate").value=json.sellingRate;
					document.getElementById("exchSelling").value=json.sellingRate;
					document.getElementById("exchBuying").value=json.buyingRate;
					document.getElementById("rdoAverage").value=((eval(json.buyingRate) + eval(json.sellingRate))/2).toFixed(4);
					//document.getElementById("txtRate").innerHTML=json.buyingRate;
			}
		});
		
  });
  //-------------------------------------------------------
  $('#frmFundsTransfer #exchSelling').click(function(){
					document.getElementById("txtRate").value=document.getElementById("exchSelling").value;
					//document.getElementById("txtRate").innerHTML=json.buyingRate;
  });
  //-------------------------------------------------------
  $('#frmFundsTransfer #exchBuying').click(function(){
		document.getElementById("txtRate").value=document.getElementById("exchBuying").value;
  });
  //-------------------------------------------------------------
  //-------------------------------------------------------
  $('#frmFundsTransfer #rdoAverage').click(function(){
		document.getElementById("txtRate").value=document.getElementById("rdoAverage").value;
  });
  //-------------------------------------------------------------
  $('#frmFundsTransfer #chkEdit').click(function(){
	  if(document.getElementById("chkEdit").checked==true)
		$('#frmFundsTransfer #txtRate').removeAttr('disabled');
		else
		$('#frmFundsTransfer #txtRate').attr("disabled",true);

  });
  //------------------------------------------------------------
   $('#frmFundsTransfer #cboPaymentMethod').change(function(){
	    var payMethod = $('#cboPaymentMethod').val();
		if(payMethod==2){
		document.getElementById("rwChequeDetails").style.display='';
		}
		else{
		document.getElementById("rwChequeDetails").style.display='none';
		}
  });
	//----------------------------------------------------------
	$('.calTot').keyup(function(){
		calculateTotalVal();
	});
	//-----------------------------------------------------------
    $('#frmFundsTransfer #butDelete').click(function(){
		var serial=URLEncode($('#frmFundsTransfer #txtFundTransafNo').val());
		if($('#frmFundsTransfer #txtFundTransafNo').val()=='')
		{
			//$('#frmBankDeposit #butDelete').validationEngine('showPrompt', 'Please select Deposit No.', 'fail');
		//	var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+serial+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = "fundsTransfer-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											data:'requestType=delete&serialNo='+serial,
											async:false,
											success:function(json){
												
												$('#frmFundsTransfer #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												if(json.type=='pass')
												{
													$('#frmFundsTransfer').get(0).reset();
													clearForm();
													loadCombo_search();
													var t=setTimeout("alertDelete()",1000);return;
												}
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
		
	//	clearForm();
	});
	//-----------------------------------------------------------
  $('#frmFundsTransfer #butSave').click(function(){
  if(existingMsgDate == "")
  {
	  calculateTotalVal();
	  var RecvedAmmount=parseFloat(document.getElementById("txtRecvAmmount").value).toFixed(2);
	  var totAccQty=parseFloat(document.getElementById("txtTotal").value).toFixed(2);
	  
	var requestType = '';
	if ($('#frmFundsTransfer').validationEngine('validate'))   
    { 
	  if(checkForDuplicate()==false){
		return false;  
	  }
	  else if(RecvedAmmount!=totAccQty){
		alert('Recieved Ammount does not tally with total Account Ammount');
		return false;  
	  }
	  
	  if(document.getElementById('chkPosted').checked==true)
	  var posted = 1;
	  else 
	  var posted = 0;
	  
		var data = "requestType=save";
		
			data+="&serialNo="		+	URLEncode($('#cboSearch').val());
			data+="&fundsTransfFrom="	+	$('#cboFundsTransferFrom').val();
			data+="&currency="	+	$('#cboCurrency').val();
			data+="&recvAmmount="			+	$('#txtRecvAmmount').val();
			data+="&ammountInWords=''";
			data+="&paymentMethod="		+	$('#cboPaymentMethod').val();
			data+="&txtpaymentMethod="		+	$('#cboPaymentMethod').text();
			data+="&posted="	+	posted;
			data+="&remarks="			+	URLEncode($('#txtRemarks').val());
			data+="&date="		+	$('#txtDate').val();
			data+="&refDate="		+	$('#txtRefDate').val();
			data+="&rate="		+	$('#txtRate').val();
			data+="&refNo="	+	$('#txtRafNo').val();
			data+="&refOrganization=" +	$('#txtRefOrganization').val();
			data+="&amStatus="		+	amStatus;
			data+="&manualNo="		+	URLEncode($('#txtFundTransafNo').val());


			var rowCount = document.getElementById('tblFundsTransfer').rows.length;
			if(rowCount==1){
				alert("No Accounts selected");
				return false;				
			}
			var row = 0;
			
			var arr="[";
			
			for(var i=1;i<rowCount;i++)
			{
					 
					var account = 	document.getElementById('tblFundsTransfer').rows[i].cells[1].childNodes[0].value;
					var ammount = 	document.getElementById('tblFundsTransfer').rows[i].cells[2].childNodes[0].value;
					var memo = 	document.getElementById('tblFundsTransfer').rows[i].cells[3].childNodes[0].value;
					var dimention = 	document.getElementById('tblFundsTransfer').rows[i].cells[4].childNodes[0].value;
					
					    arr += "{";
						arr += '"account":"'+		account +'",' ;
						arr += '"ammount":"'+		ammount +'",' ;
						arr += '"memo":"'+		URLEncode(memo) +'",' ;
						arr += '"dimention":"'+		dimention  +'"' ;
						arr +=  '},';
						
			}
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;
		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = "fundsTransfer-db-set.php";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			success:function(json){

					$('#frmFundsTransfer #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						//$('#frmSampleInfomations').get(0).reset();
						//saveImage(json.sampleNo,json.sampleYear,json.revisionNo);
						loadCombo_search();
						var t=setTimeout("alertx()",1000);
						$('#txtFundTransafNo').val(json.serialNo);
					 	$('#cboSearch').val(json.serialNo);
						return;
					}
				},
			error:function(xhr,status){
					
					$('#frmFundsTransfer #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
	}
	else
	{
		$('#frmFundsTransfer #butSave').validationEngine('showPrompt', existingMsgDate,'fail');
		var t=setTimeout("alertx()",5000);
	}
   });
   

	//--------------refresh the form----------
	$('#frmFundsTransfer #butNew').click(function(){
	//---------------------------------------------------
		existingMsgDate = "";
		amStatus = "Auto";
		$("#frmFundsTransfer #txtFundTransafNo").attr("readonly","readonly");
		$('#frmFundsTransfer #chkAutoManual').attr('checked')
		$("#frmFundsTransfer #chkAutoManual").attr("disabled","");
		document.getElementById("chkAutoManual").style.display='';
		document.getElementById("amStatus").style.display='';
		$('#frmFundsTransfer #txtFundTransafNo').removeClass('validate[required]');
	//--------------------------------------------------
		$('#frmFundsTransfer').get(0).reset();
		clearRows();
		$('#frmFundsTransfer #txtFundTransafNo').val('');
		$('#frmFundsTransfer #cboSearch').val('');
		$('#frmFundsTransfer #cboFundsTransferFrom').val('');
		$('#frmFundsTransfer #cboCurrency').val('');
		$('#frmFundsTransfer #txtRecvAmmount').val('');
		$('#frmFundsTransfer #txtAmmntInWords').val('');
		$('#frmFundsTransfer #cboPaymentMethod').val('');
		$('#frmFundsTransfer #chkPosted').val('');
		$('#frmFundsTransfer #txtRemarks').val('');
		$('#frmFundsTransfer #txtDate').val('');
		$('#frmFundsTransfer #txtRefDate').val('');
		$('#frmFundsTransfer #txtRate').val('');
		$('#frmFundsTransfer #txtRafNo').val('');
		$('#frmFundsTransfer #txtRefOrganization').val('');
		$('#frmFundsTransfer #txtTotal').val(0);
		$('#frmFundsTransfer #cboFundsTransferFrom').focus();
		
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmFundsTransfer #txtDate').val(d);
		$('#frmFundsTransfer #txtRefDate').val(d);
		
		document.getElementById("rwChequeDetails").style.display='none';
	});
//----------------------------	
			$('.delImg').click(function(){
			    var rowCount = document.getElementById('tblFundsTransfer').rows.length;
				if(rowCount>2)
				$(this).parent().parent().remove();
				calculateTotalVal();
			});


//-----------------------------------
$('#butReport').click(function(){
	if($('#txtRetSupNo').val()!=''){
		window.open('../listing/rptReturnToSupplier.php?retSupNo='+$('#txtRetSupNo').val()+'&year='+$('#txtRetSupYear').val());	
	}
	else{
		alert("There is no Return Note to view");
	}
});
//----------------------------------	
$('#butConfirm').click(function(){
	if($('#txtRetSupNo').val()!=''){
		window.open('../listing/rptReturnToSupplier.php?retSupNo='+$('#txtRetSupNo').val()+'&year='+$('#txtRetSupYear').val()+'&approveMode=1');	
	}
	else{
		alert("There is no Return Note to confirm");
	}
});
//-----------------------------------------------------
$('#butClose').click(function(){
		//load('main.php');	
});
//--------------refresh the form-----------------------
	$('#frmFundsTransfer #butNew').click(function(){
		$('#frmFundsTransfer').get(0).reset();
		clearRows();
		$('#frmFundsTransfer #txtFundTransafNo').val('');
		$('#frmFundsTransfer #cboSearch').val('');
		$('#frmFundsTransfer #cboFundsTransferFrom').val('');
		$('#frmFundsTransfer #cboCurrency').val('');
		$('#frmFundsTransfer #txtRecvAmmount').val('');
		$('#frmFundsTransfer #txtAmmntInWords').val('');
		$('#frmFundsTransfer #cboPaymentMethod').val('');
		$('#frmFundsTransfer #chkPosted').val('');
		$('#frmFundsTransfer #txtRemarks').val('');
		$('#frmFundsTransfer #txtDate').val('');
		$('#frmFundsTransfer #txtRefDate').val('');
		$('#frmFundsTransfer #txtRate').val('');
		$('#frmFundsTransfer #txtRafNo').val('');
		$('#frmFundsTransfer #txtRefOrganization').val('');
		$('#frmFundsTransfer #txtTotal').val(0);
		$('#frmFundsTransfer #cboFundsTransferFrom').focus();
		
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmFundsTransfer #txtDate').val(d);
		$('#frmFundsTransfer #txtRefDate').val(d);
		
		document.getElementById("rwChequeDetails").style.display='none';
	});
//-----------------------------------------------------

	$('#frmFundsTransfer #butPrint').click(function(){
		if($('#frmFundsTransfer #txtFundTransafNo').val()=='')
		{
			$('#frmFundsTransfer #butPrint').validationEngine('showPrompt', 'Please select Voucher.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var myurl = 'listing/fundsTransferDetails.php?id='+URLEncode($('#frmFundsTransfer #txtFundTransafNo').val());
    		window.open(myurl); 
		}
	});

});

//----------end of ready -------------------------------

function alertx()
{
	$('#frmFundsTransfer #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmFundsTransfer #butDelete').validationEngine('hide')	;
	$('#frmFundsTransfer #butPrint').validationEngine('hide') ;
}

//----------------------------------------------
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
//----------------------------------------------- 
function clearRows()
{
	var rowCount = document.getElementById('tblFundsTransfer').rows.length;

	for(var i=2;i<rowCount;i++)
	{
			document.getElementById('tblFundsTransfer').deleteRow(1);
	}
	document.getElementById('tblFundsTransfer').rows[1].cells[1].childNodes[0].value='';
	document.getElementById('tblFundsTransfer').rows[1].cells[2].childNodes[0].value='';
	document.getElementById('tblFundsTransfer').rows[1].cells[3].childNodes[0].value='';
}
//-----------------------------------------------------------------
function insertRow()
{
	var tbl = document.getElementById('tblFundsTransfer');	
	var rows = tbl.rows.length;
	//tbl.rows[1].cells[5].childNodes[0].nodeValue;
	tbl.insertRow(rows);
	tbl.rows[rows].innerHTML = tbl.rows[rows-1].innerHTML;
	document.getElementById('tblFundsTransfer').rows[rows].cells[1].childNodes[0].value='';
	document.getElementById('tblFundsTransfer').rows[rows].cells[2].childNodes[0].value='';
	document.getElementById('tblFundsTransfer').rows[rows].cells[3].childNodes[0].value='';
		//-------------------------------------
		$('.calTot').keyup(function(){
			calculateTotalVal();
		});
		//-------------------------------------
		$('.delImg').click(function(){
			var rowCount = document.getElementById('tblFundsTransfer').rows.length;
			if(rowCount>2)
			$(this).parent().parent().remove();
			calculateTotalVal();
		});
		//-------------------------------------
		$('.duplicate').change(function(){
			checkForDuplicate();
		});
		//-------------------------------------
}			
//------------------------------------------------------------------------------
function calculateTotalVal(){
	var rowCount = document.getElementById('tblFundsTransfer').rows.length;
	var row = 0;
	var totQty=0;
	
	for(var i=1;i<rowCount;i++)
	{
		if(document.getElementById('tblFundsTransfer').rows[i].cells[2].childNodes[0].value==''){
		var qty=0;
		}
		else{
		var qty= 	document.getElementById('tblFundsTransfer').rows[i].cells[2].childNodes[0].value;
		}
		if(isNaN(qty)==true){
			qty=0; 
		}
		totQty+=parseFloat(qty);
	}
		if(isNaN(totQty)==true){
			totQty=0; 
		}
		totQty=totQty.toFixed(2);
		$('#txtTotal').val(totQty);
		$('#txtRecvAmmount').val(totQty);
}
//--------------------------------------------------------------------------------
function checkForDuplicate(){
	var rowCount = document.getElementById('tblFundsTransfer').rows.length;
	//var row=this.parentNode.parentNode.rowIndex;
	var k=0;
	for(var row=1;row<rowCount;row++){
		for(var i=1;i<row;i++){
			if(document.getElementById('tblFundsTransfer').rows[i].cells[1].childNodes[0].value==document.getElementById('tblFundsTransfer').rows[row].cells[1].childNodes[0].value)	{
				k=1;		
			}
		}
	}
	if(k==0){
		return true
	}
	else{
		alert("duplicate Accounts existing");
		return false
	}
}
//-----------------------------------------------------------------------------
function loadCombo_search()
{
	var url 	= "fundsTransfer-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmFundsTransfer #cboSearch').html(httpobj.responseText);
}
//------------------------------------------------------------------------------
function clearForm(){
		$('#frmFundsTransfer').get(0).reset();
		clearRows();
		$('#frmFundsTransfer #txtFundTransafNo').val('');
		$('#frmFundsTransfer #cboSearch').val('');
		$('#frmFundsTransfer #cboFundsTransferFrom').val('');
		$('#frmFundsTransfer #cboCurrency').val('');
		$('#frmFundsTransfer #txtRecvAmmount').val('');
		$('#frmFundsTransfer #txtAmmntInWords').val('');
		$('#frmFundsTransfer #cboPaymentMethod').val('');
		$('#frmFundsTransfer #chkPosted').val('');
		$('#frmFundsTransfer #txtRemarks').val('');
		$('#frmFundsTransfer #txtDate').val('');
		$('#frmFundsTransfer #txtRefDate').val('');
		$('#frmFundsTransfer #txtRate').val('');
		$('#frmFundsTransfer #txtRafNo').val('');
		$('#frmFundsTransfer #txtRefOrganization').val('');
		$('#frmFundsTransfer #txtTotal').val(0);
		$('#frmFundsTransfer #cboFundsTransferFrom').focus();
		
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmFundsTransfer #txtDate').val(d);
		$('#frmFundsTransfer #txtRefDate').val(d);
		
		document.getElementById("rwChequeDetails").style.display='none';
}
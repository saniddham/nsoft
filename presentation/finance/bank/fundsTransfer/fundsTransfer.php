<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$location 	= $_SESSION['CompanyID'];
$company 	= $_SESSION['headCompanyId'];
$thisFilePath =  $_SERVER['PHP_SELF'];
include  	"{$backwardseperator}dataAccess/permisionCheck.inc";
// ======================Check Exchange Rate Updates========================
if($receiptRefNo == "")
{
	$status = "Adding";
}
else
{
	$status = "Changing";
}
$currentDate = date("Y-m-d");

$sql = "SELECT COUNT(*) AS 'no'
		FROM
		mst_financeexchangerate
		WHERE
		mst_financeexchangerate.dtmDate =  '$currentDate'
		";
$result = $db->RunQuery($sql);
$row= mysqli_fetch_array($result);
if($row['no']==0)
	{
		$str =  "Please Update Exchange Rates Before ".$status." Funds Trnsfer .";
		$str .= $row['NameList'];
		$maskClass="maskShow";
	}
	else
	{
		$maskClass="maskHide";
	}
// =========================================================================
$fundTransafNo = $_REQUEST['fundTransafNo'];
//$fundTransafNo = '1000000';
if($fundTransafNo){
$fundTransafNo = $_REQUEST['fundTransafNo'];
}
else{
$fundTransafNo = $_GET['cboSearch'];
}

		 $sql = "SELECT
				fin_bankfundstransfer_header.intFunsTransfFrom,
				fin_bankfundstransfer_header.dtDate,
				fin_bankfundstransfer_header.dblRate,
				fin_bankfundstransfer_header.intCurrency,
				fin_bankfundstransfer_header.dblReceivedAmount,
				fin_bankfundstransfer_header.strAmmountInWords,
				fin_bankfundstransfer_header.strReferenceNo,
				fin_bankfundstransfer_header.intPaymentMethod,
				fin_bankfundstransfer_header.dtReferenceDate,
				fin_bankfundstransfer_header.intPost,
				fin_bankfundstransfer_header.intCheckPosted,
				fin_bankfundstransfer_header.strReferenceOrganization,
				fin_bankfundstransfer_header.strRemarks
				FROM
				fin_bankfundstransfer_header
				WHERE
				fin_bankfundstransfer_header.strFundsTransfNo =  '$fundTransafNo'
				AND
				fin_bankfundstransfer_header.intCompanyId =  '$company'
				";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$fundsTransfFrm = $row['intFunsTransfFrom'];
					$date = $row['dtDate'];
					$rate = $row['dblRate'];
					$currency = $row['intCurrency'];
					$recvAmmount = $row['dblReceivedAmount'];
					$refNo = $row['strReferenceNo'];
					$payMethod = $row['intPaymentMethod'];
					$refDate = $row['dtReferenceDate'];
					$post = $row['intPost'];
					$chkPost = $row['intCheckPosted'];
					$refOrganization = $row['strReferenceOrganization'];
					$remarks = $row['strRemarks'];
					$ammountInWords = $row['strAmmountInWords'];
				 }

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Funds Transfer</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $backwardseperator; ?>css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">


<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="fundsTransfer-js.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>
<script src="../../commanFunctions/numberExisting-js.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
</head>

<body onLoad="functionList();">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator.'Header.php'; ?></td>
	</tr>
    
    <div id="divMask" class="<?php echo $maskClass?> mask"> <?php echo $str; ?></div>
    
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>
<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<form id="frmFundsTransfer" name="frmFundsTransfer"  method="get" action="fundsTransfer.php" autocomplete="off">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
</table>


<div align="center">
  <div class="trans_layoutL">
    <div class="trans_text">Funds Transfer</div>
    <table width="100%">
      <tr>
        <td class="normalfnt" width="50%"><img src="../../../../images/fb.png" width="18" height="19" /></td>
        <td align="right" width="50%"><img src="../../../../images/ff.png" width="18" height="19" /></td>
      </tr>
    <tr>
      <td  colspan="2">
        <table width="100%">
          <tr>
            <td align="right" width="40%"><span class="normalfnt"><strong>Search</strong></span></td>
            <td align="left" width="60%"><select name="cboSearch" id="cboSearch"  style="width:240px" onchange="submit();" >
              <option value=""></option>
              <?php  $sql = "SELECT DISTINCT
						fin_bankfundstransfer_header.strFundsTransfNo
						FROM fin_bankfundstransfer_header
						WHERE
						fin_bankfundstransfer_header.intStatus =  '1' AND
						fin_bankfundstransfer_header.intCompanyId =  '$company'
						order by strFundsTransfNo desc";
								
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							if($row['strFundsTransfNo']==$fundTransafNo)
							echo "<option value=\"".$row['strFundsTransfNo']."\" selected=\"selected\">".$row['strFundsTransfNo']."</option>";	
							else
							echo "<option value=\"".$row['strFundsTransfNo']."\">".$row['strFundsTransfNo']."</option>";
						}
          ?>
              </select></td>
            </tr>
          <tr>
            <td align="right">&nbsp;</td>
            <td align="left">&nbsp;</td>
          </tr>
          </table>        </span></td>
    </tr>
    <tr>
      <td colspan="2">
      <table width="100%" class="tableBorder_allRound">
    <tr>
      <td class="normalfnt">&nbsp;</td>
      <td class="normalfnt">Voucher Number</td>
      <td colspan="2"><span class="normalfnt">
        <input name="txtFundTransafNo" type="text" readonly="readonly" class="normalfntRight" id="txtFundTransafNo" value="<?php echo $fundTransafNo ?>" style="width:230px; background-color:#F4FFFF;text-align:center; border:dotted; border-color:#F00" onblur="numberExisting(this,'Funds Transfer');"/>
        <input checked="checked" type="checkbox" name="chkAutoManual" id="chkAutoManual" />
        <input name="amStatus" type="text" class="normalfntBlue" id="amStatus" style="width:40px; background-color:#FFF; text-align:center; border:thin" disabled="disabled" value="(Auto)"/>
      </span></td>
      <td bgcolor="#FFFFFF" class="">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td width="16" class="normalfnt">&nbsp;</td>
      <td width="143" class="normalfnt">Funds Transfer From <span class="compulsoryRed">*</span></td>
      <td width="239"><span class="normalfntMid">
        <select name="cboFundsTransferFrom" id="cboFundsTransferFrom" style="width:96%"  class="validate[required]">
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_financechartofaccounts.intId,
							mst_financechartofaccounts.strCode,
							mst_financechartofaccounts.strName,
							mst_financechartofaccounts.intStatus
							FROM mst_financechartofaccounts
							WHERE
							mst_financechartofaccounts.intStatus =  '1' and mst_financechartofaccounts.intFinancialTypeId='24'
							AND
							mst_financechartofaccounts.strType =  'Posting'
							ORDER BY strCode";//type=Bank
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$fundsTransfFrm)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strCode']."-".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strCode']."-".$row['strName']."</option>";	
					}
					?>
          </select>
      </span></td>
      <td width="158"><span class="normalfnt">Date <span class="compulsoryRed">*</span></span></td>
      <td width="311" bgcolor="#FFFFFF" class=""><input name="txtDate" type="text" value="<?Php echo ($date==''?date("Y-m-d"):$date) ?>"  class="validate[required] txtbox" id="txtDate" style="width:98px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" onblur="backDateExisting(this,'Funds Transfer');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
      <td width="13">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>           
      <td><span class="normalfnt">Currency <span class="compulsoryRed">*</span></span></td>
      <td><span class="normalfnt">
        <select name="cboCurrency" id="cboCurrency" style="width:100px"  class="validate[required]">
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_financecurrency.intId,
							mst_financecurrency.strCode
							FROM mst_financecurrency";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$currency)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strCode']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strCode']."</option>";	
					}
				?>
          </select>
        </span></td>
      <td><span class="normalfnt">Rate <span class="compulsoryRed">*</span></span></td>
      <td><span class="normalfnt"><span class="normalfntMid">
        <input type="radio" name="radio" id="exchSelling" value="" checked="checked"/>
        Selling
        <input type="radio" name="radio" id="exchBuying" value="" />
        Buying 
        <input class="rdoRate" type="radio" name="radio" id="rdoAverage" value="" />
Average
<input type="text" name="txtRate" id="txtRate" style="width:75px; text-align:right" disabled="disabled" value="<?php echo $rate ?>"   class="validate[required,custom[number]]"/>
        <input type="checkbox" name="chkEdit" id="chkEdit" />
        </span></span></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="normalfnt">Amount</span></td>
      <td><span class="normalfntMid">
        <input type="text" name="txtRecvAmmount" id="txtRecvAmmount" style="width:100px; text-align:right" class="validate[required,custom[number]] normalfntMid" value="<?php echo $recvAmmount ?>" readonly="readonly" />
      </span></td>
      <td class="normalfnt"><strong class="normalfnt">Bank Reference Number</strong></td>
      <td><span class="normalfntMid">
        <input type="text" name="txtRafNo" id="txtRafNo" style="width:105px" value="<?php echo $refNo; ?>" />
      </span></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong class="normalfnt">Payment Method <span class="compulsoryRed">*</span></strong></td>
      <td><span class="normalfntMid">
        <select name="cboPaymentMethod" id="cboPaymentMethod" style="width:150px"  class="validate[required]">
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_financepaymentsmethods.intId,
							mst_financepaymentsmethods.strName
							FROM mst_financepaymentsmethods
							WHERE
							mst_financepaymentsmethods.intStatus =  '1'";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$payMethod)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
					?>
        </select>
      </span></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr id="rwChequeDetails" <?php if($payMethod!=2){?> style="display:none" <?php } ?>>
      <td>&nbsp;</td>
      <td><strong class="normalfnt">Reference</strong> <strong class="normalfnt">Date</strong></td>
      <td><input name="txtRefDate" type="text" value="<?php echo date("Y-m-d"); ?>"  class="validate[required] txtbox" id="txtRefDate" style="width:98px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /> 
        <input type="checkbox" name="chkPosted" id="chkPosted" <?php if($chkPost=='1'){ ?> checked="checked" <?php } ?>  />
        <span class="normalfnt">Posted</span></td>
      <td><strong class="normalfnt">Reference Organization</strong></td>
      <td><span class="normalfntMid">
        <input type="text" name="txtRefOrganization" id="txtRefOrganization" style="width:195px" value="<?php echo $refOrganization ?>" />
      </span></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="normalfnt">Memo</span></td>
      <td colspan="2"><textarea name="txtRemarks" id="txtRemarks" cols="45" rows="2"><?php echo $remarks ?></textarea></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
      </table>
      </td>
      </tr>
      <tr>
        <td colspan="2" align="center">
        </td>
      </tr>
      </tr>
            <tr>
      <td colspan="2" align="right"><img src="../../../../images/Tadd.jpg" width="92" height="24" id="insertRow" /></td>
      </tr>
    <tr>
      <td colspan="2"><table width="87%" height="190">
        <tr>
          <td>
                  <div style="overflow:scroll;width:900px;height:150px;" id="divGrid"><table width="100%" id="tblFundsTransfer" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
        <tr class="">
          <td width="19" bgcolor="#FAD163" class="normalfntMid">Del</td>
          <td width="238"  height="24" bgcolor="#FAD163" class="normalfntMid"><strong>Funds Transfer To <span class="compulsoryRed">*</span></strong></td>
          <td width="110" bgcolor="#FAD163" class="normalfntMid"  ><strong>Amount <span class="compulsoryRed">*</span></strong></td>
          <td width="400" align="center" bgcolor="#FAD163" class="normalfntMid"  ><strong>Memo</strong></td>
          <td width="100" bgcolor="#FAD163" class="normalfntMid"  ><strong><span class="compulsoryRed">*</span> Cost Center</strong></td>
        </tr>
        
        <?php
			            $sqlm = "SELECT
						fin_bankfundstransfer_details.intAccountId,
						fin_bankfundstransfer_details.dblAmount,
						fin_bankfundstransfer_details.strMemo,
						fin_bankfundstransfer_details.intDimension
						FROM fin_bankfundstransfer_details
						WHERE
						fin_bankfundstransfer_details.strFundsTransfNo =  '$fundTransafNo'";
				$resultm = $db->RunQuery($sqlm);
				$existingRws=0;
				$totAmnt=0;
				while($rowm=mysqli_fetch_array($resultm))
				{
					$existingRws++;
					$accountId=$rowm['intAccountId'];
					$ammount=$rowm['dblAmount'];
					$memo=$rowm['strMemo'];
					$dimention=$rowm['intDimension'];
					$totAmnt+=$ammount;
		?>
        
        <tr class="normalfnt">
          <td bgcolor="#FFFFFF" class="normalfntMid"><img src="../../../../images/del.png" width="15" height="15"  class="delImg" /></td>
          <td bgcolor="#FFFFFF" class="normalfntMid"><select name="select3" id="select2" style="width:240px" class="validate[required]">
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_financechartofaccounts.intId,
							mst_financechartofaccounts.strCode,
							mst_financechartofaccounts.strName,
							mst_financechartofaccounts.intStatus
							FROM mst_financechartofaccounts
							WHERE
							mst_financechartofaccounts.intStatus =  '1' AND (mst_financechartofaccounts.intFinancialTypeId='24' OR mst_financechartofaccounts.intFinancialTypeId='11')
							AND
							mst_financechartofaccounts.strType =  'Posting'
							ORDER BY strCode";//type=Bank
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$accountId)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strCode']."-".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strCode']."-".$row['strName']."</option>";	
					}
					?>
          </select>
          </td>
          <td  bgcolor="#FFFFFF" class="normalfntMid"><input name="txtAmmount" type="text" id="txtAmmount" style="width:135px; text-align:right" class="validate[required,custom[number]] calTot" value="<?php echo $ammount ?>" />
          </td>
          <td  bgcolor="#FFFFFF"><input type="text" name="textfield5" id="textfield5" style="width:370px" class="" value="<?php echo $memo ?>" /></td>
          <td  bgcolor="#FFFFFF" class="normalfntMid"><select name="cboDimention" id="cboDimention" style="width:100px" class="validate[required]">
                        <option value="">&nbsp;</option>
                  <?php
					$sql = "SELECT
							mst_financedimension.intId,
							mst_financedimension.strName
							FROM mst_financedimension
							WHERE
							mst_financedimension.intStatus =  '1'
							";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$dimention)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
					?>
              </select>
          </td>
        </tr>
        <?php
		}
		if($existingRws==0){
		?>
        <tr class="normalfnt">
          <td bgcolor="#FFFFFF" class="normalfntMid"><img src="../../../../images/del.png" width="15" height="15"  class="delImg" /></td>
          <td bgcolor="#FFFFFF" class="normalfntMid"><select name="select3" id="select2" style="width:240px" class="validate[required]">
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_financechartofaccounts.intId,
							mst_financechartofaccounts.strCode,
							mst_financechartofaccounts.strName,
							mst_financechartofaccounts.intStatus
							FROM mst_financechartofaccounts
							WHERE
							mst_financechartofaccounts.intStatus =  '1' AND (mst_financechartofaccounts.intFinancialTypeId='24' OR mst_financechartofaccounts.intFinancialTypeId='11')
							AND
							mst_financechartofaccounts.strType =  'Posting'
							ORDER BY strCode";//type=Bank
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$accountId)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strCode']."-".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strCode']."-".$row['strName']."</option>";	
					}
					?>
          </select>
          </td>
          <td  bgcolor="#FFFFFF" class="normalfntMid"><input name="txtAmmount" type="text" id="txtAmmount" style="width:135px; text-align:right" class="validate[required,custom[number]] calTot" value="" />
          </td>
          <td  bgcolor="#FFFFFF"><input type="text" name="textfield5" id="textfield5" style="width:370px" class="" /></td>
          <td  bgcolor="#FFFFFF" class="normalfntMid"><select name="cboDimention" id="cboDimention" style="width:100px" class="validate[required]">
                        <option value="">&nbsp;</option>
                  <?php
					$sql = "SELECT
							mst_financedimension.intId,
							mst_financedimension.strName
							FROM mst_financedimension
							WHERE
							mst_financedimension.intStatus =  '1'
							";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$dimention)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
					?>
              </select>
          </td>
        </tr>
        <?php
		}
		?>
      </table>
      </div>
            </td>
          </tr>
          <tr>
          <td colspan="2">
          <table width="476">
                    <tr>
                      <td width="271" align="right" class="normalfntRight">Total</td>
                      <td width="90"><span class="normalfntMid">
                        <input name="txtTotal" type="text" disabled="disabled" id="txtTotal" style="width:135px; text-align:right" value="<?php echo $totAmnt; ?>" />
                      </span></td>
                      <td width="99" class="normalfnt">&nbsp;</td>
                    </tr>
                  </table>
          </td>
          </tr>
        </table></td>
    </tr>
      <tr>
        <td colspan="2">
          <table width="100%">
            <tr>
              <td width="100%" height="34" class="tableBorder_allRound"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                <tr>
                  <td width="100%" align="center" bgcolor=""><img border="0" src="../../../../images/Tnew.jpg" alt="New" name="butNew" width="92" height="24" class="mouseover" id="butNew" tabindex="28"/><img src="../../../../images/Tsave.jpg" width="92" height="24" id="butSave" name="butSave"  class="mouseover" /><img style="display:none" border="0" src="../../../../images/Tprint.jpg" alt="Print" name="butPrint" width="92" height="24" class="mouseover" id="butPrint" tabindex="25"/><img border="0" src="../../../../images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="25"/><a href="../../../../main.php"><img src="../../../../images/Tclose.jpg" alt="Close" name="Close" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                </tr>
              </table></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
</div>
</div>
</form>
</body>
</html>
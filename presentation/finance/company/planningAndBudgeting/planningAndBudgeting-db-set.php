<?php 
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$companyId = $row['intCompanyId']; 
	}
	$locationId = $_SESSION["CompanyID"];
	//////////////////////////main parameters ////////////////////////////////
	$budgetNo		= trim($_REQUEST['txtNo']);
	$requestType 	= $_REQUEST['requestType'];
	$id 			= $_REQUEST['cboSearch'];
	/////////////////////planning and budgeting header parameters ////////////////////
	$period		= null(trim($_REQUEST['cboPeriod']));
	$budgetName	= trim($_REQUEST['txtBudgetName']);
	$mainFnType	= trim($_REQUEST['cboType']);
	$dimension	= trim($_REQUEST['cboDimension']);
	$memo		= trim($_REQUEST['txtMemo']);
	/////////////////////planning and budgeting detail parameters /////////////////////
	$mainDetails 	= json_decode($_REQUEST['detail'], true);
	///////////////////////////////////////////////////////////////////////////

	////////////////////////planning and budgeting insert part ///////////////////////
	if($requestType=='add')
	{
		$budgetNumber 		= getNextBudgetNo($companyId,$locationId);
		$accountPeriod 		= getLatestAccPeriod($companyId);
		$budgetReference	= trim(encodeBudgetNo($budgetNumber,$accountPeriod,$companyId,$locationId));
			
		$sql = "INSERT INTO `fin_company_planning_and_budgeting_header`
		(`intBudgetNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strReferenceNo`,`intBudgetAccPeriodId`,`strName`,`intFinancialMainTypeID`,`intDimensionId`,`strMemo`,`intCreator`,dtmCreateDate,`intDeleteStatus`)
		VALUES ('$budgetNumber','$accountPeriod','$locationId','$companyId','$budgetReference','$period','$budgetName','$mainFnType','$dimension','$memo','$userId',now(), '0')";

		$firstResult = $db->RunQuery($sql);

		if(count($mainDetails) != 0 && $firstResult)
		{
			foreach($mainDetails as $detail)
			{
				$accId 		= trim($detail['accId']);
				$valJan 	= val($detail['valJan']);
				$valFeb 	= val($detail['valFeb']);
				$valMar 	= val($detail['valMar']);
				$valApr 	= val($detail['valApr']);
				$valMay 	= val($detail['valMay']);
				$valJun 	= val($detail['valJun']);
				$valJul 	= val($detail['valJul']);
				$valAug 	= val($detail['valAug']);
				$valSep 	= val($detail['valSep']);
				$valOct 	= val($detail['valOct']);
				$valNov 	= val($detail['valNov']);
				$valDec 	= val($detail['valDec']);
				
				$sql = "INSERT INTO `fin_company_planning_and_budgeting_details` (`intBudgetNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strReferenceNo`,`intChartOfAccountId`,`strJanuary`,`strFebruary`,`strMarch`,`strApril`,`strMay`,`strJune`,`strJuly`,`strAugust`,`strSeptember`,`strOctober`,`strNovember`,`strDecember`,`intCreator`,dtmCreateDate) 
				VALUES ('$budgetNumber','$accountPeriod','$locationId','$companyId','$budgetReference','$accId','$valJan','$valFeb','$valMar','$valApr','$valMay','$valJun','$valJul','$valAug','$valSep','$valOct','$valNov','$valDec','$userId',now())";
				
				$mainDetailResult = $db->RunQuery($sql);
			}			
		}
		
		if($mainDetailResult && $firstResult)
		{
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
			$response['budgetNo'] 	= $budgetReference;
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
	}
	////////////////////// planning and budgeting update part ////////////////////////
	else if($requestType=='edit')
	{
	
	if(count($mainDetails) != 0)
	{
		
		$sql = "UPDATE `fin_company_planning_and_budgeting_header` SET 	intBudgetAccPeriodId	= $period,
																		strName					='$budgetName',
																		intFinancialMainTypeID	='$mainFnType',
																		intDimensionId			='$dimension',
																		strMemo					='$memo',
																		intModifyer				='$userId',
																		intDeleteStatus			='0'
		WHERE (`strReferenceNo`='$budgetNo')";
		$firstResult = $db->RunQuery($sql);
	}
		if(count($mainDetails) != 0 && $firstResult)
		{
			$sql = "SELECT
					fin_company_planning_and_budgeting_header.intBudgetNo,
					fin_company_planning_and_budgeting_header.intAccPeriodId,
					fin_company_planning_and_budgeting_header.strReferenceNo
					FROM
					fin_company_planning_and_budgeting_header
					WHERE
					fin_company_planning_and_budgeting_header.strReferenceNo =  '$budgetNo'
					";
			$result = $db->RunQuery($sql);
			while($row=mysqli_fetch_array($result))
			{
				$budgetNumber 	= $row['intBudgetNo'];
				$accountPeriod 	= $row['intAccPeriodId'];
			}
			
			$sql = "DELETE FROM `fin_company_planning_and_budgeting_details` WHERE (`strReferenceNo`='$budgetNo')";
			$db->RunQuery($sql);
			
			foreach($mainDetails as $detail)
			{
				$accId 		= trim($detail['accId']);
				$valJan 	= val($detail['valJan']);
				$valFeb 	= val($detail['valFeb']);
				$valMar 	= val($detail['valMar']);
				$valApr 	= val($detail['valApr']);
				$valMay 	= val($detail['valMay']);
				$valJun 	= val($detail['valJun']);
				$valJul 	= val($detail['valJul']);
				$valAug 	= val($detail['valAug']);
				$valSep 	= val($detail['valSep']);
				$valOct 	= val($detail['valOct']);
				$valNov 	= val($detail['valNov']);
				$valDec 	= val($detail['valDec']);
				
				$sql = "INSERT INTO `fin_company_planning_and_budgeting_details` (`intBudgetNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strReferenceNo`,`intChartOfAccountId`,`strJanuary`,`strFebruary`,`strMarch`,`strApril`,`strMay`,`strJune`,`strJuly`,`strAugust`,`strSeptember`,`strOctober`,`strNovember`,`strDecember`,`intCreator`,dtmCreateDate) 
				VALUES ('$budgetNumber','$accountPeriod','$locationId','$companyId','$budgetNo','$accId','$valJan','$valFeb','$valMar','$valApr','$valMay','$valJun','$valJul','$valAug','$valSep','$valOct','$valNov','$valDec','$userId',now())";
				
				$mainDetailResult = $db->RunQuery($sql);
			}
		}		
		if($mainDetailResult && $firstResult)
		{
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Updated successfully.';
			$response['budgetNo'] 	= $budgetNo;
		}
		else
		{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	else if($requestType=='delete')
	{
		$sql = "UPDATE `fin_company_planning_and_budgeting_header` SET intDeleteStatus ='1'
				WHERE (`strReferenceNo`='$id')";
		$result = $db->RunQuery($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Deleted successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	echo json_encode($response);
//--------------------------------------------------------------------------------------------
	function getNextBudgetNo($companyId,$locationId)
	{
		global $db;
		$sql = "SELECT
				intBudgetNo
				FROM sys_finance_no
				WHERE
				intCompanyId = '$companyId' AND intLocationId = '$locationId'
				";	
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$nextBudgetNo = $row['intBudgetNo'];
		
	$sql = "UPDATE `sys_finance_no` SET intBudgetNo=intBudgetNo+1 WHERE (intCompanyId = '$companyId' AND intLocationId = '$locationId')";
		
		$db->RunQuery($sql);
		return $nextBudgetNo;
	}
//--------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------
	function getLatestAccPeriod($companyId)
	{
		global $db;
		$sql = "SELECT
				MAX(mst_financeaccountingperiod.intId) AS accId,
				mst_financeaccountingperiod.dtmStartingDate,
				mst_financeaccountingperiod.dtmClosingDate,
				mst_financeaccountingperiod.intStatus,
				mst_financeaccountingperiod_companies.intCompanyId,
				mst_financeaccountingperiod_companies.intPeriodId
				FROM
				mst_financeaccountingperiod
				Inner Join mst_financeaccountingperiod_companies ON mst_financeaccountingperiod_companies.intPeriodId = mst_financeaccountingperiod.intId
				WHERE
				mst_financeaccountingperiod_companies.intCompanyId =  '$companyId' AND mst_financeaccountingperiod.intStatus = '1'
				ORDER BY
				mst_financeaccountingperiod.intId DESC
				";	
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$latestAccPeriodId = $row['accId'];	
		return $latestAccPeriodId;
	}
//--------------------------------------------------------------------------------------------
//============================================================================================
	function encodeBudgetNo($budgetNo,$accountPeriod,$companyId,$locationId)
	{
		global $db;
		$sql = "SELECT
				mst_financeaccountingperiod.intId,
				mst_financeaccountingperiod.dtmStartingDate,
				mst_financeaccountingperiod.dtmClosingDate,
				mst_financeaccountingperiod.intStatus
				FROM
				mst_financeaccountingperiod
				WHERE
				mst_financeaccountingperiod.intId =  '$accountPeriod'
				";	
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$startDate = substr($row['dtmStartingDate'],0,4);
		$closeDate = substr($row['dtmClosingDate'],0,4);
		$sql = "SELECT
				mst_companies.strCode AS company,
				mst_companies.intId,
				mst_locations.intCompanyId,
				mst_locations.strCode AS location,
				mst_locations.intId
				FROM
				mst_companies
				Inner Join mst_locations ON mst_locations.intCompanyId = mst_companies.intId
				WHERE
				mst_locations.intId =  '$locationId' AND
				mst_companies.intId =  '$companyId'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$companyCode = $row['company'];
		$locationCode = $row['location'];
		$budgetFormat = $companyCode."/".$locationCode."/".$startDate."-".$closeDate."/".$budgetNo;
		return $budgetFormat;
	}
//============================================================================================
?>
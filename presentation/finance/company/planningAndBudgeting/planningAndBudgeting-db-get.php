<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$companyId = $row['intCompanyId']; 
	}
	/////////// planning and budgeting load part /////////////////////
	if($requestType=='loadCombo')
	{
		$sql = "SELECT
				fin_company_planning_and_budgeting_header.strReferenceNo
				FROM
				fin_company_planning_and_budgeting_header
				WHERE
				fin_company_planning_and_budgeting_header.intCompanyId =  '$companyId' AND
				fin_company_planning_and_budgeting_header.intDeleteStatus = '0'
				ORDER BY intBudgetNo DESC
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['strReferenceNo']."\">".$row['strReferenceNo']."</option>";
		}
		echo $html;
	}
	else if($requestType=='loadDetails')
	{
		$id  = $_REQUEST['id'];		
		
		//-------------------------------------------------------------
		$sql = "SELECT
				fin_company_planning_and_budgeting_details.intChartOfAccountId,
				fin_company_planning_and_budgeting_details.strJanuary,
				fin_company_planning_and_budgeting_details.strFebruary,
				fin_company_planning_and_budgeting_details.strMarch,
				fin_company_planning_and_budgeting_details.strApril,
				fin_company_planning_and_budgeting_details.strMay,
				fin_company_planning_and_budgeting_details.strJune,
				fin_company_planning_and_budgeting_details.strJuly,
				fin_company_planning_and_budgeting_details.strAugust,
				fin_company_planning_and_budgeting_details.strSeptember,
				fin_company_planning_and_budgeting_details.strOctober,
				fin_company_planning_and_budgeting_details.strNovember,
				fin_company_planning_and_budgeting_details.strDecember
				FROM
				fin_company_planning_and_budgeting_header
				Inner Join fin_company_planning_and_budgeting_details ON fin_company_planning_and_budgeting_header.intBudgetNo = fin_company_planning_and_budgeting_details.intBudgetNo AND fin_company_planning_and_budgeting_header.intAccPeriodId = fin_company_planning_and_budgeting_details.intAccPeriodId AND fin_company_planning_and_budgeting_header.intLocationId = fin_company_planning_and_budgeting_details.intLocationId AND fin_company_planning_and_budgeting_header.intCompanyId = fin_company_planning_and_budgeting_details.intCompanyId AND fin_company_planning_and_budgeting_header.strReferenceNo = fin_company_planning_and_budgeting_details.strReferenceNo
				WHERE
				fin_company_planning_and_budgeting_header.intDeleteStatus =  '0' AND
				fin_company_planning_and_budgeting_header.strReferenceNo =  '$id'
				";
		$result = $db->RunQuery($sql);
		$arrDetail;
		while($row=mysqli_fetch_array($result))
		{
			$val['accId'] 	= $row['intChartOfAccountId'];
			$val['jan'] 	= $row['strJanuary'];
			$val['feb'] 	= $row['strFebruary'];
			$val['mar'] 	= $row['strMarch'];
			$val['apr'] 	= $row['strApril'];
			$val['may'] 	= $row['strMay'];
			$val['jun'] 	= $row['strJune'];
			$val['jul'] 	= $row['strJuly'];
			$val['aug'] 	= $row['strAugust'];
			$val['sep'] 	= $row['strSeptember'];
			$val['oct'] 	= $row['strOctober'];
			$val['nov'] 	= $row['strNovember'];
			$val['dec'] 	= $row['strDecember'];
			
			$arrDetail[] = $val;
		}
		$response['detailVal'] = $arrDetail;
		//-----------------------------------------------------------
		$sql   = "SELECT
					fin_company_planning_and_budgeting_header.intBudgetAccPeriodId,
					fin_company_planning_and_budgeting_header.strName,
					fin_company_planning_and_budgeting_header.intFinancialMainTypeID,
					fin_company_planning_and_budgeting_header.intDimensionId,
					fin_company_planning_and_budgeting_header.strMemo,
					fin_company_planning_and_budgeting_header.strReferenceNo
					FROM
					fin_company_planning_and_budgeting_header
					WHERE
					fin_company_planning_and_budgeting_header.intDeleteStatus =  '0' AND
					fin_company_planning_and_budgeting_header.strReferenceNo =  '$id'

					";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['accPeriod'] 	= $row['intBudgetAccPeriodId'];
			$response['budgetName'] = $row['strName'];
			$response['fnMainType'] = $row['intFinancialMainTypeID'];
			$response['dimension'] 	= $row['intDimensionId'];
			$response['memo'] 		= $row['strMemo'];
		}
		//-------------------------------------------------------------
		echo json_encode($response);
	}
	else if($requestType=='getAccount')
	{
		$accMainId  = $_REQUEST['accMainId'];
		$periodId  = $_REQUEST['periodId'];
		
		$sql = "SELECT
				mst_financeaccountingperiod.intId,
				mst_financeaccountingperiod.dtmStartingDate,
				mst_financeaccountingperiod.dtmClosingDate
				FROM
				mst_financeaccountingperiod
				Inner Join mst_financeaccountingperiod_companies ON mst_financeaccountingperiod.intId = mst_financeaccountingperiod_companies.intPeriodId
				WHERE
				mst_financeaccountingperiod.intId =  '$periodId' AND
				mst_financeaccountingperiod_companies.intCompanyId =  '$companyId'
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$startDate	 = $row['dtmStartingDate'];
			$endDate	 = $row['dtmClosingDate'];
		}
		$monthRange = getDates($startDate,$endDate);
		//////////////////////////accounts//////////////////////////////////
		$sql = "SELECT
				mst_financechartofaccounts.intId AS accId,
				mst_financechartofaccounts.strName,
				mst_financechartofaccounts.intStatus,
				mst_financialsubtype.intId,
				mst_financechartofaccounts.intFinancialTypeId,
				mst_financialmaintype.intId,
				mst_financialsubtype.intFinancialMainTypeID,
				mst_financechartofaccounts.strType
				FROM
				mst_financechartofaccounts
				inner Join mst_financialsubtype ON mst_financechartofaccounts.intFinancialTypeId = mst_financialsubtype.intId
				inner Join mst_financialmaintype ON mst_financialsubtype.intFinancialMainTypeID = mst_financialmaintype.intId
				WHERE
				mst_financechartofaccounts.strType =  'posting' AND
				mst_financialmaintype.intId =  '$accMainId' AND
				mst_financechartofaccounts.intStatus =  '1'
				";
			  $result = $db->RunQuery($sql);
			  echo "
					<tr class=normalfnt bgcolor=#CDCDB4 style=color:red>
					<td align=center width=20%><strong>Accounts</strong></td>
					<td align=center width=8%><strong>Total</strong></td>
					<td align=center width=6% id=$monthRange[0]><strong>$monthRange[0]</strong></td>
					<td align=center width=6% id=$monthRange[1]><strong>$monthRange[1]</strong></td>
					<td align=center width=6% id=$monthRange[2]><strong>$monthRange[2]</strong></td>
					<td align=center width=6% id=$monthRange[3]><strong>$monthRange[3]</strong></td>
					<td align=center width=6% id=$monthRange[4]><strong>$monthRange[4]</strong></td>
					<td align=center width=6% id=$monthRange[5]><strong>$monthRange[5]</strong></td>
					<td align=center width=6% id=$monthRange[6]><strong>$monthRange[6]</strong></td>
					<td align=center width=6% id=$monthRange[7]><strong>$monthRange[7]</strong></td>
					<td align=center width=6% id=$monthRange[8]><strong>$monthRange[8]</strong></td>
					<td align=center width=6% id=$monthRange[9]><strong>$monthRange[9]</strong></td>
					<td align=center width=6% id=$monthRange[10]><strong>$monthRange[10]</strong></td>
					<td align=center width=6% id=$monthRange[11]><strong>$monthRange[11]</strong></td>
					</tr>";
		while($row=mysqli_fetch_array($result))
		{
			$accId	 = $row['accId'];
			$accName = $row['strName'];
			echo 
				"
				<tr class=normalfnt bgcolor=#CDCDB4>
				<td align=left width=20% id=$accId class=accName><span style=color:blue>$accName</span></td>
				<td align=center width=8% id=$accId1><input class=\"total\" style=width:100%;text-align:center;background-color:#FFF;border:none type=text disabled=disabled/></td>
				<td align=center width=6% id=$accId1><input class=\"$monthRange[0] dateText\" style=width:100%;text-align:center;background-color:#FFF;border:none type=text /></td>
				<td align=center width=6% id=$accId2><input class=\"$monthRange[1]  dateText\" style=width:100%;text-align:center;background-color:#FFF;border:none type=text /></td>
				<td align=center width=6% id=$accId3><input class=\"$monthRange[2]  dateText\" style=width:100%;text-align:center;background-color:#FFF;border:none type=text /></td>
				<td align=center width=6% id=$accId4><input class=\"$monthRange[3]  dateText\" style=width:100%;text-align:center;background-color:#FFF;border:none type=text /></td>
				<td align=center width=6% id=$accId5><input class=\"$monthRange[4]  dateText\" style=width:100%;text-align:center;background-color:#FFF;border:none type=text /></td>
				<td align=center width=6% id=$accId6><input class=\"$monthRange[5]  dateText\" style=width:100%;text-align:center;background-color:#FFF;border:none type=text /></td>
				<td align=center width=6% id=$accId7><input class=\"$monthRange[6]  dateText\" style=width:100%;text-align:center;background-color:#FFF;border:none type=text /></td>
				<td align=center width=6% id=$accId8><input class=\"$monthRange[7]  dateText\" style=width:100%;text-align:center;background-color:#FFF;border:none type=text /></td>
				<td align=center width=6% id=$accId9><input class=\"$monthRange[8]  dateText\" style=width:100%;text-align:center;background-color:#FFF;border:none type=text /></td>
				<td align=center width=6% id=$accId10><input class=\"$monthRange[9]  dateText\" style=width:100%;text-align:center;background-color:#FFF;border:none type=text /></td>
				<td align=center width=6% id=$accId11><input class=\"$monthRange[10]  dateText\" style=width:100%;text-align:center;background-color:#FFF;border:none type=text /></td>
				<td align=center width=6% id=$accId12><input class=\"$monthRange[11]  dateText\" style=width:100%;text-align:center;background-color:#FFF;border:none type=text /></td>
				</tr>
				";
		}	
		//////////////////////////////////////////////////////////////////////
	}
	function getDates($startDate, $endDate)
	{		
		$start = $month = strtotime($startDate);
		$end = strtotime($endDate);
		while($month < $end)
		{
			 $months[] = date('F', $month);
			 $month = strtotime("+1 month", $month);
		}
		return $months;
	}
?>
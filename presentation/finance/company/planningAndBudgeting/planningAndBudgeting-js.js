// JavaScript Document
var toBePaidAmount = '';
function functionList()
{
	if(recRefNo!='')
	{
		$('#frmPlanningAndBudgeting #cboSearch').val(recRefNo);
		$('#frmPlanningAndBudgeting #cboSearch').change();
	}
}
var rows = 1;
function insertRow()
{
	var tbl = document.getElementById('tblMainGrid1');	
	rows = tbl.rows.length;
	tbl.insertRow(rows);
	tbl.rows[rows].innerHTML = tbl.rows[rows-1].innerHTML;
//	loadJs();
}
$(document).ready(function() {
	
	var id = '';
	$("#frmPlanningAndBudgeting").validationEngine();
	$('#frmPlanningAndBudgeting #cboCurrency').focus();

  //permision for add 
  if(intAddx)
  {
 	$('#frmPlanningAndBudgeting #butNew').show();
	$('#frmPlanningAndBudgeting #butSave').show();
  }
  //permision for edit 
  if(intEditx)
  {
  	$('#frmPlanningAndBudgeting #butSave').show();
	$('#frmPlanningAndBudgeting #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
  //permision for delete
  if(intDeletex)
  {
  	$('#frmPlanningAndBudgeting #butDelete').show();
	$('#frmPlanningAndBudgeting #cboSearch').removeAttr('disabled');
  }
  //permision for view
  if(intViewx)
  {
	$('#frmPlanningAndBudgeting #cboSearch').removeAttr('disabled');
  }
  
///////////////////////////////////////////////////////////////////
$(".dateText").live("keyup ", calculate);
///////////////////////////////////////////////////////////////////

///////////////////////////get chart of accounts////////////////////
$('#cboType').live('change',function(){
	if($('#cboPeriod').val() && $('#cboType').val())
	{
		var url = "planningAndBudgeting-db-get.php?requestType=getAccount&accMainId="+$(this).val()+'&periodId='+$('#cboPeriod').val();
		var obj = $.ajax({url:url,async:false});
		$('#tblDetails').html(obj.responseText);
	}
	else
	{
		$('#tblDetails').html("");
		$('#frmPlanningAndBudgeting #cboPeriod').validationEngine('showPrompt', 'Please select period and type!','fail');
		var t=setTimeout("alertx()",2000)
	}
});
////////////////////////////////////////////////////////////////////
$('#cboPeriod').live('change',function(){
	if($('#cboType').val() && $('#cboPeriod').val())
	{
		var url = "planningAndBudgeting-db-get.php?requestType=getAccount&accMainId="+$('#cboType').val()+'&periodId='+$('#cboPeriod').val();
		var obj = $.ajax({url:url,async:false});
		$('#tblDetails').html(obj.responseText);
	}
	else
	{
		$('#tblDetails').html("");
		$('#frmPlanningAndBudgeting #cboType').validationEngine('showPrompt', 'Please select period and type!','fail');
		var t=setTimeout("alertx()",2000)
	}
});
////////////////////////get exchange rate//////////////////////////
$('#cboCurrency').change(function(){
	var url = "planningAndBudgeting-db-get.php?requestType=getExchangeRate&currencyId="+$(this).val()+'&exchangeDate='+$('#txtDate').val();
	var obj = $.ajax({url:url,dataType:'json',success:function(json){
		
		$('#rdoBuying').val(json.buyingRate);
		$('#rdoSelling').val(json.sellingRate);
		$('#rdoSelling').click();
		},async:false});
});
///////////////////////////////////////////////////////////////////
$('.rdoRate').click(function(){
  $('#txtRate').val($(this).val());
});
//save button click event
$('#frmPlanningAndBudgeting #butSave').click(function(){
//-------------------------------------------------------------------
	var accId = "";
	var valJan = "";
	var valFeb = "";
	var valMar = "";
	var valApr = "";
	var valMay = "";
	var valJun = "";
	var valJul = "";
	var valAug = "";
	var valSep = "";
	var valOct = "";
	var valNov = "";
	var valDec = "";
			
 value="[ ";
	$('#tblDetails tr:not(:first)').each(function(){
		
		accId  = $(this).find(".accName").attr('id');
		valJan = $(this).find(".January").val();
		valFeb = $(this).find(".February").val();
		valMar = $(this).find(".March").val();
		valApr = $(this).find(".April").val();
		valMay = $(this).find(".May").val();
		valJun = $(this).find(".June").val();
		valJul = $(this).find(".July").val();
		valAug = $(this).find(".August").val();
		valSep = $(this).find(".September").val();
		valOct = $(this).find(".October").val();
		valNov = $(this).find(".November").val();
		valDec = $(this).find(".December").val();

		value += '{ "accId":"'+accId+'", "valJan": "'+valJan+'", "valFeb": "'+valFeb+'", "valMar": "'+valMar+'", "valApr": "'+valApr+'", "valMay": "'+valMay+'", "valJun": "'+valJun+'", "valJul": "'+valJul+'", "valAug": "'+valAug+'", "valSep": "'+valSep+'", "valOct": "'+valOct+'", "valNov": "'+valNov+'", "valDec": "'+valDec+'"},';

	});
	
	value = value.substr(0,value.length-1);
	value += " ]";

//---------------------------------------------------------------------------
	var requestType = '';
	if ($('#frmPlanningAndBudgeting').validationEngine('validate'))
    {
		//showWaiting();
		if(value != '[ ]')
		{
			if($('#txtNo').val()=='')
				requestType = 'add';
			else
				requestType = 'edit';
			
			var url = "planningAndBudgeting-db-set.php";
			var obj = $.ajax({
				url:url,
				dataType: "json",
				type:'post',  
				data:$("#frmPlanningAndBudgeting").serialize()+'&requestType='+requestType+'&cboSearch='+id+'&detail='+value,
				async:false,
				
				success:function(json){
						$('#frmPlanningAndBudgeting #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
						if(json.type=='pass')
						{
							var t=setTimeout("alertx()",1000);
							$('#txtNo').val(json.budgetNo);
							loadCombo_frmPlanningAndBudgeting();
							return;
						}
						var t=setTimeout("alertx()",3000);
					},
				error:function(xhr,status){
						$('#frmPlanningAndBudgeting #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						var t=setTimeout("alertx()",3000);
					}		
				});
		}
		else
		{
			$('#frmPlanningAndBudgeting #butSave').validationEngine('showPrompt', 'You cannot allow this process!','fail');
			var t=setTimeout("alertx()",5000);
		}
	}
});
/////////////////////////////////////////////////////
//// load invoice details //////////////////////////
/////////////////////////////////////////////////////
$('#frmPlanningAndBudgeting #cboSearch').click(function(){
   $('#frmPlanningAndBudgeting').validationEngine('hide');
});
$('#frmPlanningAndBudgeting #cboSearch').change(function(){
$('#frmPlanningAndBudgeting').validationEngine('hide');
var url = "planningAndBudgeting-db-get.php";
if($('#frmPlanningAndBudgeting #cboSearch').val()=='')
{
		$('#tblDetails').html("");
		
	$('#frmPlanningAndBudgeting').get(0).reset();return;	
}
$('#txtNo').val($(this).val());
var httpobj = $.ajax({
	url:url,
	dataType:'json',
	type:'post',
	data:'requestType=loadDetails&id='+$(this).val(),
	async:false,
	success:function(json)
	{
		$('#frmPlanningAndBudgeting #cboPeriod').val(json.accPeriod);
		$('#frmPlanningAndBudgeting #txtBudgetName').val(json.budgetName);
		$('#frmPlanningAndBudgeting #cboType').val(json.fnMainType);
		$('#frmPlanningAndBudgeting #cboDimension').val(json.dimension);
		$('#frmPlanningAndBudgeting #txtMemo').val(json.memo);
		
		//--------------------------------------------------
		$('#tblDetails').html("");
		
		var accId 	= "";
		var valJan 	= "";
		var valFeb 	= "";
		var valMar 	= "";
		var valApr 	= "";
		var valMay 	= "";
		var valJun	= "";
		var valJul 	= "";
		var valAug 	= "";
		var valSep 	= "";
		var valOct 	= "";
		var valNov 	= "";
		var valDec 	= "";
		
		if($('#cboType').val() && $('#cboPeriod').val())
		{
			$('#frmPlanningAndBudgeting #cboType').change();
		}
		if(json.detailVal!=null)
		{			
			for(var j=0;j<=json.detailVal.length-1;j++)
			{
				accId	= json.detailVal[j].accId;
				valJan	= json.detailVal[j].jan;
				valFeb	= json.detailVal[j].feb;
				valMar	= json.detailVal[j].mar;
				valApr	= json.detailVal[j].apr;
				valMay	= json.detailVal[j].may;
				valJun	= json.detailVal[j].jun;
				valJul	= json.detailVal[j].jul;
				valAug	= json.detailVal[j].aug;
				valSep	= json.detailVal[j].sep;
				valOct	= json.detailVal[j].oct;
				valNov	= json.detailVal[j].nov;
				valDec	= json.detailVal[j].dec;
				$('#tblDetails tr:not(:first)').each(function()
				{
					if($(this).find(".accName").attr('id') == accId)
					{
						$(this).find(".January").val(valJan);
						$(this).find(".February").val(valFeb);
						$(this).find(".March").val(valMar);
						$(this).find(".April").val(valApr);
						$(this).find(".May").val(valMay);
						$(this).find(".June").val(valJun);
						$(this).find(".July").val(valJul);
						$(this).find(".August").val(valAug);
						$(this).find(".September").val(valSep);
						$(this).find(".October").val(valOct);
						$(this).find(".November").val(valNov);
						$(this).find(".December").val(valDec);
						
						var value=0;
						value = parseFloat(valJan)+parseFloat(valFeb)+parseFloat(valMar)+parseFloat(valApr)+parseFloat(valMay)+parseFloat(valJun)+parseFloat(valJul)+parseFloat(valAug)+parseFloat(valSep)+parseFloat(valOct)+parseFloat(valNov)+parseFloat(valDec);
						$(this).find('.total').val(value);
					}
					else
					return;
				});
			}
		}
		else
		{
			
		}
	   //--------------------------------------------------
	}
});
});
//////////// end of load details /////////////////

  	$('#frmPlanningAndBudgeting #butNew').click(function(){
		$('#frmPlanningAndBudgeting').get(0).reset();
		
		$('#tblDetails').html("");
		
		$('#frmPlanningAndBudgeting #cboPaymentsMethods').change();//--->
		loadCombo_frmPlanningAndBudgeting();
	});
	$('#frmPlanningAndBudgeting #butDelete').click(function(){
		if($('#frmPlanningAndBudgeting #cboSearch').val()=='')
		{
			$('#frmPlanningAndBudgeting #butDelete').validationEngine('showPrompt', 'Please select G/L Number.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmPlanningAndBudgeting #cboSearch option:selected').text()+'" ?',{
			buttons: { Ok: true, Cancel: false },
			callback: function(v,m,f){
			if(v)
			{
					var url = "planningAndBudgeting-db-set.php";
					var httpobj = $.ajax({
						url:url,
						dataType:'json',
						type:'post',
						data:'requestType=delete&cboSearch='+$('#frmPlanningAndBudgeting #cboSearch').val(),
						async:false,
						success:function(json){
							
							$('#frmPlanningAndBudgeting #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
							
							if(json.type=='pass')
							{
								$('#frmPlanningAndBudgeting').get(0).reset();
								$('#tblMainGrid2 >tbody >tr').each(function(){
									if($(this).index()!=0 && $(this).index()!=1 )
									{
										$(this).remove();
									}
								});
								$('#frmPlanningAndBudgeting #cboPaymentsMethods').change();//--->
								loadCombo_frmPlanningAndBudgeting();
								var t=setTimeout("alertDelete()",1000);return;
							}	
							var t=setTimeout("alertDelete()",3000);
						}	 
					});
			}
		}
			});	
		}
	});
});
////////////////////// calculation ////////////////////////////////
function calculate()
{
	var value=0;
	for(var i=2;i<=13;i++)
	{
		value += parseFloat(($(this).parent().parent().find('td:eq('+i+') .dateText').val()==''?0:$(this).parent().parent().find('td:eq('+i+') .dateText').val()));
	}
	$(this).parent().parent().find('.total').val(value);
}
///////////////////////////////////////////////////////////////////
function loadCombo_frmPlanningAndBudgeting()
{
	var url 	= "planningAndBudgeting-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmPlanningAndBudgeting #cboSearch').html(httpobj.responseText);
}
function alertx()
{
	$('#frmPlanningAndBudgeting #butSave').validationEngine('hide');
	$('#frmPlanningAndBudgeting #cboPeriod').validationEngine('hide');
	$('#frmPlanningAndBudgeting #cboType').validationEngine('hide');
}
function alertDelete()
{
	$('#frmPlanningAndBudgeting #butDelete').validationEngine('hide') ;
}
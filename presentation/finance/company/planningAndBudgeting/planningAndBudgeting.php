<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];

$thisFilePath =  $_SERVER['PHP_SELF'];
include  "{$backwardseperator}dataAccess/permisionCheck.inc";
$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$companyId = $row['intCompanyId']; 
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Planning And Budgeting</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/tblstyle.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script src="planningAndBudgeting-js.js" type="text/javascript"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

<link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css">

<style type="text/css">
.setProperty 
{
  width:80px;
  text-align:righ; 
  background-color:#FFF;
  border:none;
}
</style>

</head>

<body>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<!--<div id="divMask" class="<?php echo $maskClass?> mask"> <?php echo $str; ?></div>-->

<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<form id="frmPlanningAndBudgeting" name="frmPlanningAndBudgeting" method="post" action="planningAndBudgeting-db-set.php" autocomplete="off">
<div align="center">
  <div class="trans_layoutXL">
    <div class="trans_text">Planning And Budgeting</div>
    <table width="100%">
        <tr>
        <td colspan="4" align="center" bgcolor="#FFFFFF" class="normalfntMid">
          <table width="100%">
            <tr>
              <td bgcolor="#FFFFFF" class="normalfntRight">&nbsp;</td>
              <td bgcolor="#FFFFFF">&nbsp;</td>
            </tr>
            <tr>
              <td bgcolor="#FFFFFF" class="normalfntRight">Budget Code:</td>
              <td bgcolor="#FFFFFF">
              	<select name="cboSearch" id="cboSearch"  style="width:195px" >
                <option value=""></option>
                <?php   $sql = "SELECT
								fin_company_planning_and_budgeting_header.strReferenceNo
								FROM
								fin_company_planning_and_budgeting_header
								WHERE
								fin_company_planning_and_budgeting_header.intCompanyId =  '$companyId' AND
								fin_company_planning_and_budgeting_header.intDeleteStatus = '0'
								ORDER BY intBudgetNo DESC";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['strReferenceNo']."\">".$row['strReferenceNo']."</option>";
						}
          ?>
              </select></td>
              </tr>
            <tr>
              <td width="41%" bgcolor="#FFFFFF" class="normalfntRight">&nbsp;</td>
              <td width="59%" bgcolor="#FFFFFF">&nbsp;</td>
              </tr>
            </table>
          </td>
      </tr>
      <tr>
      <td align="center">
      <table width="100%" class="tableBorder">
      <tr>
      <td bgcolor="#FFFFFF">
      <table width="100%">
      <tr>
        <td bgcolor="#FFFFFF" class="normalfnt" height="22px"><span class="normalfntMid">Budget Code</span></td>
        <td bgcolor="#FFFFFF"><span class="normalfnt">
          <input name="txtNo" type="text" readonly="readonly"class="normalfntRight" id="txtNo" style="width:200px; background-color:#F4FFFF;text-align:center; border:dotted; border-color:#F00" />
          (Auto)</span></td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
        <td bgcolor="#FFFFFF">&nbsp;</td>
      </tr>
      <tr>
        <td bgcolor="#FFFFFF" class="normalfnt"><span class="normalfntRight">Period: <span class="compulsoryRed">*</span></span></td>
        <td bgcolor="#FFFFFF">
        <select name="cboPeriod" id="cboPeriod" class="validate[required]"  style="width:225px" >
          <option value=""></option>
          <?php   $sql = "SELECT
		  					mst_financeaccountingperiod.intId,
							mst_financeaccountingperiod.dtmStartingDate,
							mst_financeaccountingperiod.dtmClosingDate,
							mst_financeaccountingperiod.intStatus
							FROM
							mst_financeaccountingperiod
							Inner Join mst_financeaccountingperiod_companies ON mst_financeaccountingperiod.intId = mst_financeaccountingperiod_companies.intPeriodId
							WHERE
							mst_financeaccountingperiod.intStatus =  '1' AND
							mst_financeaccountingperiod_companies.intCompanyId =  '$companyId'
							ORDER BY dtmStartingDate DESC
							";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['dtmStartingDate']."/".$row['dtmClosingDate']."</option>";
						}
          ?>
        </select></td>
        <td bgcolor="#FFFFFF"><span class="normalfnt">Budget Name</span></td>
        <td bgcolor="#FFFFFF"><span class="normalfntMid">
          <input type="text" name="txtBudgetName" id="txtBudgetName" style="width:200px" />
        </span></td>
      </tr>
      <tr>
        <td width="19%" bgcolor="#FFFFFF" class="normalfnt">Type <span class="compulsoryRed">*</span></td>
        <td width="39%" bgcolor="#FFFFFF">
        <select name="cboType" id="cboType" class="validate[required]" style="width:225px" >
          <option value=""></option>
          <?php   $sql = "SELECT
							mst_financialmaintype.intId,
							mst_financialmaintype.strName
							FROM
							mst_financialmaintype
							WHERE
							mst_financialmaintype.intStatus =  '1'
							ORDER BY intId DESC
							";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
						}
          ?>
        </select></td>
        <td width="16%" bgcolor="#FFFFFF"><span class="normalfnt">Cost Center <span class="compulsoryRed">*</span></span></td>
        <td width="26%" bgcolor="#FFFFFF">
        <select name="cboDimension" class="validate[required] dimension" id="cboDimension"  style="width:200px;">
          <option value=""></option>
          <?php  $sql = "SELECT
							intId,
							strName
							FROM mst_financedimension
							WHERE
								intStatus = 1
							order by strName
							";
							$result = $db->RunQuery($sql);
							while($row=mysqli_fetch_array($result))
							{
								echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
							}
                   ?>
        </select></td>
      </tr>
      <tr>
        <td bgcolor="#FFFFFF" class="normalfnt">Memo</td>
        <td colspan="3" bgcolor="#FFFFFF">
        <textarea name="txtMemo" id="txtMemo" cols="75" rows="2"></textarea></td>
        </tr>
      </table>
      </td>
      </tr>
      </table>
      </td>
      </tr>
      <tr>
        <td style="width:100%">
        <table id="tblDetails" width="100%" class="tableBorder">
        
        </table>
        </td>
      </tr>
      <tr>
        <td>&nbsp;</td>
      </tr>
      <tr>
        <td>
          <table width="100%">
            <tr>
              <td width="100%" height="34"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                <tr>
  <td width="100%" class="tableBorder_allRound" align="center" bgcolor=""><img style="display:none" border="0" src="../../../../images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:none" border="0" src="../../../../images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/><img style="display:none" border="0" src="../../../../images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="25"/><a href="../../../../main.php"><img  src="../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                  </tr>
                <tr>
                  <td align="center" bgcolor="">&nbsp;</td>
                  </tr>
                </table>
                </td>
              </tr>
            </table>
          </td>
      </tr>
    </table>   
</div>
</div>
</form>  
</body>
</html>
<?php 
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	include "../../commanFunctions/CommanEditAndDelete.php";
	$response = array('type'=>'', 'msg'=>'');
        
	$companyId = $_SESSION['headCompanyId'];
	$locationId = $_SESSION["CompanyID"];
	////////////////////////// main parameters ////////////////////////////////
	$invoice	= trim($_REQUEST['txtNo']);
	$requestType 	= $_REQUEST['requestType'];
	$amStatus 	= $_REQUEST['amStatus'];
	$id 		= $_REQUEST['cboSearch'];
	///////////////////// sales invoice header parameters ////////////////////
	$customer		= null(trim($_REQUEST['cboCustomer']));
	$date			= trim($_REQUEST['txtDate']);
	$invoType		= trim(($_REQUEST['invoType']==''?'NULL':"'".$_REQUEST['invoType']."'"));
	$address		= $_REQUEST['txtAddress'];
	$shipTo 		= $_REQUEST['txtShipTo'];
	$currency		= null(trim($_REQUEST['cboCurrency']));
	$rate			= val(trim($_REQUEST['txtRate']));
	$remarks 		= $_REQUEST['txtRemarks'];
	$poNo			= trim($_REQUEST['txtPoNo']);
	$paymentsTerms	= null(trim($_REQUEST['cboPaymentsTerms']));
	$marketer		= null(trim($_REQUEST['cboMarketer']));
	$shipDate 		= trim($_REQUEST['txtShipDate']);
	$shipmentMethod	= null(trim($_REQUEST['cboShipmentMethod']));
	$message		= $_REQUEST['txtMessage'];
	
	$headDimension	= null($_REQUEST['cboHeadDimension']);
	///////////////////// sales invoice detail parameters /////////////////////
	$details = json_decode($_REQUEST['salesDetails'], true);
	///////////////////////////////////////////////////////////////////////////
	$cusTotalAmount	= $_REQUEST['txtTotal'];
	//////////////////////// sales invoice insert part ///////////////////////
	if($requestType=='add')
	{
        try{
			$invoiceNumber 		= getNextInvoiceNo($companyId,$locationId);
			$accountPeriod 		= getLatestAccPeriod($companyId);
		if($amStatus == "Auto")
		{
			$invoiceReference	= trim(encodeInvoiceNo($invoiceNumber,$accountPeriod,$companyId,$locationId,$date));
		}
		else if($amStatus == "Manual")
		{
			$invoiceReference	= $invoice;
		}
                
		$db->begin();                
                //Add data to transaction header*******************************************
                $sql="INSERT INTO fin_transactions (entryDate, strProgramType, documentNo, currencyId, currencyRate, transDetails, payMethodId, paymentNumber, accPeriod, userId, companyId, createdOn) VALUES
                    ('$date','Sales Invoice','$invoiceReference',$currency,$rate,'$remarks',null,null,$accountPeriod,$userId,$companyId,now())";
                
                $db->RunQuery2($sql);
                $entryId=$db->insertId;                
                //********************************************************************************
                
		$sql = "INSERT INTO `fin_customer_salesinvoice_header` (`intInvoiceNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strReferenceNo`,`intCustomerId`,`dtmDate`,`strInvoiceType`,`strAddress`,`strShipTo`,`intCurrencyId`,`dblRate`,`strRemark`,`strPoNo`,`intPaymentsTermsId`,`intMarketerId`,`dtmShipDate`,`intShipmentId`,`strMessage`,`intCreator`,dtmCreateDate,`intDeleteStatus`,entryId,`intDimensionId`) 
				VALUES ('$invoiceNumber','$accountPeriod','$locationId','$companyId','$invoiceReference',$customer,'$date',$invoType,'$address','$shipTo',$currency,'$rate','$remarks','$poNo',$paymentsTerms,$marketer,'$shipDate',$shipmentMethod,'$message','$userId',now(),'0',$entryId,$headDimension)";

		$firstResult = $db->RunQuery2($sql); 
                
		if(count($details) != 0 && $firstResult)		{
			foreach($details as $detail)
			{
				$item 	= $detail['itemId'];
				
				$style	= trim(($detail['style']==''?'NULL':"'".$detail['style']."'"));
				$graphic= trim(($detail['graphic']==''?'NULL':"'".$detail['graphic']."'"));
				$order			= trim(($detail['order']==''?'NULL':"'".$detail['order']."'"));
				$line			= trim(($detail['line']==''?'NULL':"'".$detail['line']."'"));
				$locate			= trim(($detail['locate']==''?'NULL':"'".$detail['locate']."'"));
				$itemDesc		= trim(($detail['itemDesc']==''?'NULL':"'".$detail['itemDesc']."'"));
				
				$uom			= null($detail['uomId']);
				$quantity 		= val($detail['qty']);
				$unitPrice		= val($detail['unitPrice']);
				$discount		= val($detail['discount']);
				$taxAmount		= val($detail['taxAmount']);
				$taxGroup 		= null($detail['taxGroupId']);
				$dimension		= null($detail['dimensionId']);
				$itemAmount		= val($detail['amount']);
				$taxDetails 	= $detail['trnTaxVal'];
				
				$sql = "SELECT
						COUNT(intItemSerial) AS 'no',
						MAX(intItemSerial) AS val,
						fin_customer_salesinvoice_details.intItemSerial
						FROM
						fin_customer_salesinvoice_details
						WHERE
						fin_customer_salesinvoice_details.strReferenceNo =  '$invoiceReference'
						ORDER BY intItemSerial DESC
						";
				$result = $db->RunQuery2($sql);
				$row= mysqli_fetch_array($result);
				if($row['no']==0)
				{
					$itemSerial = 1;
				}
				else
				{
					$itemSerial = $row['val'] + 1;
				}
				
				$sql = "INSERT INTO `fin_customer_salesinvoice_details` (`intInvoiceNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strReferenceNo`,`intItem`,`intItemSerial`,`strStyleNo`,`strGraphicNo`,`strOrderNo`,`strLineItem`,`strCusLocation`,`strItemDesc`,`intUom`,`dblQty`,`dblUnitPrice`,`dblDiscount`,`dblTaxAmount`,`intTaxGroupId`,`intDimensionId`,`intCreator`,dtmCreateDate) 
				VALUES ('$invoiceNumber','$accountPeriod','$locationId','$companyId','$invoiceReference','$item','$itemSerial', $style, $graphic, $order, $line, $locate, $itemDesc, $uom,'$quantity','$unitPrice','$discount','$taxAmount',$taxGroup,$dimension,'$userId',now())";
				
				$finalResult = $db->RunQuery2($sql);
				
                                //>>>>>>>>>>>>>>>>>>>>>>transaction table process - item to trans Details>>>>>>>>>>>>>>>>>>>>>>>>>
                                $sql = "SELECT
                                                mst_financecustomeritemactivate.intChartOfAccountId
                                                FROM mst_financecustomeritemactivate
                                                WHERE
                                                mst_financecustomeritemactivate.intCustomerItemId = '$item' AND
                                                mst_financecustomeritemactivate.intCompanyId = '$companyId'";
                                        $result = $db->RunQuery2($sql);
                                        $row = mysqli_fetch_array($result);
                                        $itemAccount = $row['intChartOfAccountId'];

                                $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                                        ($entryId,'C',$itemAccount,$itemAmount,'$remarks',$dimension)";
                                $trnResult = $db->RunQuery2($sql);
			//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			
			//>>>>>>>>>>>>>>>>>>>>>>>>>>>transaction table process - tax>>>>>>>>>>>>>>>>>>>>>>>>>
                            if(count($taxDetails) != 0 && $trnResult)
                            {
                                foreach($taxDetails as $taxDetail)
                                {
                                        $taxId 	= $taxDetail['taxId'];
                                        $taxAmount	= $taxDetail['taxValue'];

                                        $sql = "SELECT
                                        mst_financetaxactivate.intChartOfAccountId
                                        FROM mst_financetaxactivate
                                        WHERE
                                        mst_financetaxactivate.intTaxId = '$taxId' AND
                                        mst_financetaxactivate.intCompanyId = '$companyId'";
                                    $result = $db->RunQuery2($sql);
                                    $row = mysqli_fetch_array($result);
                                    $taxAccount = $row['intChartOfAccountId'];				 

                                    $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                                            ($entryId,'C',$taxAccount,$taxAmount,'$remarks',null)";
                                    $db->RunQuery2($sql);
                                }		
                            }
			//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			}
			//>>>>>>>>>>>>>>>>>>>>>>transaction table process - customer>>>>>>>>>>>>>>>>>>>>>>>>>
			$sql = "SELECT
					mst_financecustomeractivate.intChartOfAccountId
					FROM mst_financecustomeractivate
					WHERE
					mst_financecustomeractivate.intCustomerId =  '$customer' AND
					mst_financecustomeractivate.intCompanyId =  '$companyId'";
				 $result = $db->RunQuery2($sql);
				 $row = mysqli_fetch_array($result);
				 $custAccount = $row['intChartOfAccountId'];
				 
			
                 $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId,personType, personId) VALUES 
                                ($entryId,'D',$custAccount,$cusTotalAmount,'$remarks',$headDimension,'cus',$customer)";
			$trnResult=$db->RunQuery2($sql);                        
			//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		}
                
		if($firstResult && $trnResult)
		{                    
                        $response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
			$response['invoiceNo'] 	= $invoiceReference; 
                        $db->commit();
		}
		else{                    
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
                        $db->rollback();//roalback
		}
                echo json_encode($response);            
            }catch(Exception $e){
               
                $db->rollback();//roalback
                $response['type'] 		= 'fail';
                $response['msg'] 		= $e->getMessage();
                $response['q'] 			= $sql;                
               echo json_encode($response);                 
            }
            
	}
        
	////////////////////// sales invoice update part ////////////////////////
        else if($requestType=='edit'){
            // ckeck Unrealize gain/loss for entry and if exist block edit and delete
            $chk=checkEditDeleteForUnrealize("cus",$customer,'S.Invoice',$invoice);            
            if($chk){
                $response['type'] = 'fail';
                $response['msg']  = "You cannot allow this process! Invoice has some Unrealize Gain Or Loss";
                echo json_encode($response);
            }
            else{
                try {
                    $db->begin(); 
                    ///Check Payments
                    $sql = "SELECT
                                    fin_customer_salesinvoice_header.strReferenceNo,                            
                                    fin_customer_receivedpayments_main_details.strDocNo,
                                    fin_customer_receivedpayments_main_details.intCompanyId,
                                    fin_customer_receivedpayments_main_details.strDocType
                                    FROM
                                    fin_customer_salesinvoice_header
                                    Inner Join fin_customer_receivedpayments_main_details ON fin_customer_salesinvoice_header.strReferenceNo = fin_customer_receivedpayments_main_details.strDocNo AND fin_customer_salesinvoice_header.intAccPeriodId = fin_customer_receivedpayments_main_details.intAccPeriodId AND fin_customer_salesinvoice_header.intLocationId = fin_customer_receivedpayments_main_details.intLocationId AND fin_customer_salesinvoice_header.intCompanyId = fin_customer_receivedpayments_main_details.intCompanyId
                                    Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.intReceiptNo = fin_customer_receivedpayments_header.intReceiptNo AND fin_customer_receivedpayments_main_details.intAccPeriodId = fin_customer_receivedpayments_header.intAccPeriodId AND fin_customer_receivedpayments_main_details.intLocationId = fin_customer_receivedpayments_header.intLocationId AND fin_customer_receivedpayments_main_details.intCompanyId = fin_customer_receivedpayments_header.intCompanyId AND fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
                                    WHERE
                                    fin_customer_receivedpayments_main_details.intCompanyId =  '$companyId' AND
                                    fin_customer_receivedpayments_main_details.strDocNo =  '$id' AND
                                    fin_customer_receivedpayments_main_details.strDocType =  'S.Invoice' AND
                                    fin_customer_receivedpayments_header.intDeleteStatus =  '0'";
                    $result = $db->RunQuery2($sql);                
                    if(!mysqli_num_rows($result)){
                            //CHeck creidt notes
                            $sqlCn = "SELECT
                                            fin_customer_salesinvoice_header.strReferenceNo,
                                            fin_customer_creditnote_header.intDeleteStatus
                                            FROM
                                            fin_customer_salesinvoice_header
                                            Inner Join fin_customer_creditnote_header ON fin_customer_salesinvoice_header.intCompanyId = fin_customer_creditnote_header.intCompanyId AND fin_customer_salesinvoice_header.strReferenceNo = fin_customer_creditnote_header.strInvoiceNo AND fin_customer_salesinvoice_header.intCustomerId = fin_customer_creditnote_header.intCustomerId
                                            WHERE
                                            fin_customer_creditnote_header.intCompanyId =  '$companyId' AND
                                            fin_customer_creditnote_header.intDeleteStatus =  '0' AND
                                            fin_customer_creditnote_header.strInvoiceNo =  '$id'
                                            ";
                            $resultCn = $db->RunQuery2($sqlCn);
                            if(!mysqli_num_rows($resultCn)){              


                                $sql = "UPDATE `fin_customer_salesinvoice_header` SET 	intCustomerId	= $customer,
                                            dtmDate		='$date',
                                            strInvoiceType	= $invoType,
                                            strAddress		='$address',
                                            strShipTo		='$shipTo',
                                            intCurrencyId	= $currency,
                                            dblRate		='$rate',
                                            strRemark		='$remarks',
                                            strPoNo		='$poNo',
                                            intPaymentsTermsId	= $paymentsTerms,
                                            intMarketerId	= $marketer,
                                            dtmShipDate		='$shipDate',
                                            intShipmentId	= $shipmentMethod,
                                            strMessage		='$message',
                                            intModifyer		='$userId',
                                            intDeleteStatus	= '0',
											intDimensionId = $headDimension
                                                WHERE (`strReferenceNo`='$invoice')";
                                $firstResult = $db->RunQuery2($sql);

                                if (count($details) != 0 && $firstResult) {
                                    $sql = "SELECT
                                                fin_customer_salesinvoice_header.intInvoiceNo,
                                                fin_customer_salesinvoice_header.intAccPeriodId,
                                                fin_customer_salesinvoice_header.strReferenceNo,
                                                fin_customer_salesinvoice_header.entryId
                                                FROM
                                                fin_customer_salesinvoice_header
                                                WHERE
                                                fin_customer_salesinvoice_header.strReferenceNo =  '$invoice'";
                                    $result = $db->RunQuery2($sql);
                                    while ($row = mysqli_fetch_array($result)) {
                                        $invoiceNumber = $row['intInvoiceNo'];
                                        $accountPeriod = $row['intAccPeriodId'];
                                        $entryId= $row['entryId'];
                                    }
                                    //========update the transaction deader====================
                                    $sql="UPDATE fin_transactions SET 
                                                entryDate='$date',                                                    
                                                currencyId=$currency,
                                                currencyRate='$rate',
                                                transDetails='$remarks',                    
                                                accPeriod=$accountPeriod,
                                                authorized=1
                                        WHERE entryId=$entryId";
                                    $db->RunQuery2($sql);

                                    $sqld = "DELETE FROM `fin_transactions_details` WHERE entryId=$entryId";
                                    $resultd = $db->RunQuery2($sqld);
                                    //=========================================================
                                    $sql = "DELETE FROM `fin_customer_salesinvoice_details` WHERE (`strReferenceNo`='$invoice')";
                                    $db->RunQuery2($sql);

                                    foreach ($details as $detail) {
                                        $item = $detail['itemId'];

                                        $style = trim(($detail['style'] == '' ? 'NULL' : "'" . $detail['style'] . "'"));
                                        $graphic = trim(($detail['graphic'] == '' ? 'NULL' : "'" . $detail['graphic'] . "'"));
                                        $order = trim(($detail['order'] == '' ? 'NULL' : "'" . $detail['order'] . "'"));
                                        $line = trim(($detail['line'] == '' ? 'NULL' : "'" . $detail['line'] . "'"));
                                        $locate = trim(($detail['locate'] == '' ? 'NULL' : "'" . $detail['locate'] . "'"));
                                        $itemDesc = trim(($detail['itemDesc'] == '' ? 'NULL' : "'" . $detail['itemDesc'] . "'"));

                                        $uom = null($detail['uomId']);
                                        $quantity = val($detail['qty']);
                                        $unitPrice = val($detail['unitPrice']);
                                        $discount = val($detail['discount']);
                                        $taxAmount = val($detail['taxAmount']);
                                        $taxGroup = null($detail['taxGroupId']);
                                        $dimension = null($detail['dimensionId']);
                                        $itemAmount = val($detail['amount']);
                                        $taxDetails = $detail['trnTaxVal'];

                                        $sql = "SELECT
                                                                                            COUNT(intItemSerial) AS 'no',
                                                                                            MAX(intItemSerial) AS val,
                                                                                            fin_customer_salesinvoice_details.intItemSerial
                                                                                            FROM
                                                                                            fin_customer_salesinvoice_details
                                                                                            WHERE
                                                                                            fin_customer_salesinvoice_details.strReferenceNo =  '$invoice'
                                                                                            ORDER BY intItemSerial DESC";
                                        $result = $db->RunQuery2($sql);
                                        $row = mysqli_fetch_array($result);
                                        if ($row['no'] == 0) {
                                            $itemSerial = 1;
                                        } else {
                                            $itemSerial = $row['val'] + 1;
                                        }

                                        $sql = "INSERT INTO `fin_customer_salesinvoice_details` (`intInvoiceNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strReferenceNo`,`intItem`,`intItemSerial`,`strStyleNo`,`strGraphicNo`,`strOrderNo`,`strLineItem`,`strCusLocation`,`strItemDesc`,`intUom`,`dblQty`,`dblUnitPrice`,`dblDiscount`,`dblTaxAmount`,`intTaxGroupId`,`intDimensionId`,`intCreator`,dtmCreateDate) 
                                                        VALUES ('$invoiceNumber','$accountPeriod','$locationId','$companyId','$invoice','$item','$itemSerial',$style,$graphic,$order,$line,$locate,$itemDesc,$uom,'$quantity','$unitPrice','$discount','$taxAmount',$taxGroup,$dimension,'$userId',now())";

                                        $finalResult = $db->RunQuery2($sql);

                                        //>>>>>>>>>>>>>>>>>>>>>>transaction table process - item>>>>>>>>>>>>>>>>>>>>>>>>>
                                        $sql = "SELECT
                                                        mst_financecustomeritemactivate.intChartOfAccountId
                                                        FROM mst_financecustomeritemactivate
                                                        WHERE
                                                        mst_financecustomeritemactivate.intCustomerItemId = '$item' AND
                                                        mst_financecustomeritemactivate.intCompanyId = '$companyId'";
                                        $result = $db->RunQuery2($sql);
                                        $row = mysqli_fetch_array($result);
                                        $itemAccount = $row['intChartOfAccountId'];

                                        $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                                                        ($entryId,'C',$itemAccount,$itemAmount,'$remarks',$dimension)";
                                        $trnResult = $db->RunQuery2($sql);
                                    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                    //>>>>>>>>>>>>>>>>>>>>>>>>>>>transaction table process - tax>>>>>>>>>>>>>>>>>>>>>>>>>
                                        if (count($taxDetails) != 0 && $trnResult) {
                                            foreach ($taxDetails as $taxDetail) {
                                                $taxId = $taxDetail['taxId'];
                                                $taxAmount = $taxDetail['taxValue'];

                                                $sql = "SELECT
                                                        mst_financetaxactivate.intChartOfAccountId
                                                        FROM mst_financetaxactivate
                                                        WHERE
                                                        mst_financetaxactivate.intTaxId = '$taxId' AND
                                                        mst_financetaxactivate.intCompanyId = '$companyId'";
                                                $result = $db->RunQuery2($sql);
                                                $row = mysqli_fetch_array($result);
                                                $taxAccount = $row['intChartOfAccountId'];

                                                $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                                                        ($entryId,'C',$taxAccount,$taxAmount,'$remarks',null)";
                                                $db->RunQuery2($sql);
                                            }
                                    }
                                    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                            }

                            //>>>>>>>>>>>>>>>>>>>>>>transaction table process - customer>>>>>>>>>>>>>>>>>>>>>>>>>
                            $sql = "SELECT
                                        mst_financecustomeractivate.intChartOfAccountId
                                    FROM mst_financecustomeractivate
                                    WHERE
                                        mst_financecustomeractivate.intCustomerId = '$customer' AND
                                        mst_financecustomeractivate.intCompanyId = '$companyId'";
                            $result = $db->RunQuery2($sql);
                            $row = mysqli_fetch_array($result);
                            $custAccount = $row['intChartOfAccountId'];

                            $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId,personType, personId) VALUES 
                                        ($entryId,'D',$custAccount,$cusTotalAmount,'$remarks',$headDimension,'cus',$customer)";
                                $trnResult=$db->RunQuery2($sql);  
                //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                        }
                        if ($finalResult && $trnResult) {
                            $db->commit();
                            $response['type'] = 'pass';
                            $response['msg'] = 'Updated successfully.';
                            $response['invoiceNo'] = $invoice;
                        } else {
                            $db->rollback();
                            $response['type'] = 'fail';
                            $response['msg'] = $db->errormsg;
                            $response['q'] = $sql;
                        }
                    }
                    else{
                        $db->rollback();
                        $response['type'] = 'fail';
                        $response['msg']  = "You cannot allow this process! Invoice has some credit note";
                    }
                    }else{
                        $db->rollback();
                        $response['type'] = 'fail';
                        $response['msg']  = "You cannot allow this process! Invoice has some payements";
                    }
                    echo json_encode($response);


                } catch (Exception $e) {
                    $db->rollback(); //roalback
                    $response['type'] = 'fail';
                    $response['msg'] = $e->getMessage();
                    $response['q'] = $sql;
                    echo json_encode($response);
                }
            }
        }
	/////////// sales invoice delete part /////////////////////
	else if($requestType=='delete')
	{
            // ckeck Unrealize gain/loss for entry and if exist block edit and delete
            $chk=checkEditDeleteForUnrealize("cus",$customer,'S.Invoice',$id);            
            if($chk){
                $response['type'] = 'fail';
                $response['msg']  = "You cannot allow this process! Invoice has some Unrealize Gain Or Loss";
                echo json_encode($response);
            }
            else{
                try{
                    $db->begin();		
                    $sql = "SELECT
                                    fin_customer_salesinvoice_header.strReferenceNo,                            
                                    fin_customer_receivedpayments_main_details.strDocNo,
                                    fin_customer_receivedpayments_main_details.intCompanyId,
                                    fin_customer_receivedpayments_main_details.strDocType
                                    FROM
                                    fin_customer_salesinvoice_header
                                    Inner Join fin_customer_receivedpayments_main_details ON fin_customer_salesinvoice_header.strReferenceNo = fin_customer_receivedpayments_main_details.strDocNo AND fin_customer_salesinvoice_header.intAccPeriodId = fin_customer_receivedpayments_main_details.intAccPeriodId AND fin_customer_salesinvoice_header.intLocationId = fin_customer_receivedpayments_main_details.intLocationId AND fin_customer_salesinvoice_header.intCompanyId = fin_customer_receivedpayments_main_details.intCompanyId
                                    Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.intReceiptNo = fin_customer_receivedpayments_header.intReceiptNo AND fin_customer_receivedpayments_main_details.intAccPeriodId = fin_customer_receivedpayments_header.intAccPeriodId AND fin_customer_receivedpayments_main_details.intLocationId = fin_customer_receivedpayments_header.intLocationId AND fin_customer_receivedpayments_main_details.intCompanyId = fin_customer_receivedpayments_header.intCompanyId AND fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
                                    WHERE
                                    fin_customer_receivedpayments_main_details.intCompanyId =  '$companyId' AND
                                    fin_customer_receivedpayments_main_details.strDocNo =  '$id' AND
                                    fin_customer_receivedpayments_main_details.strDocType =  'S.Invoice' AND
                                    fin_customer_receivedpayments_header.intDeleteStatus =  '0'";
                    $result = $db->RunQuery2($sql);
                    if(!mysqli_num_rows($result))
                    {
                            $sqlCn = "SELECT
                                            fin_customer_salesinvoice_header.strReferenceNo,
                                            fin_customer_creditnote_header.intDeleteStatus
                                            FROM
                                            fin_customer_salesinvoice_header
                                            Inner Join fin_customer_creditnote_header ON fin_customer_salesinvoice_header.intCompanyId = fin_customer_creditnote_header.intCompanyId AND fin_customer_salesinvoice_header.strReferenceNo = fin_customer_creditnote_header.strInvoiceNo AND fin_customer_salesinvoice_header.intCustomerId = fin_customer_creditnote_header.intCustomerId
                                            WHERE
                                            fin_customer_creditnote_header.intCompanyId =  '$companyId' AND
                                            fin_customer_creditnote_header.intDeleteStatus =  '0' AND
                                            fin_customer_creditnote_header.strInvoiceNo =  '$id'
                                            ";
                            $resultCn = $db->RunQuery2($sqlCn);
                            if(!mysqli_num_rows($resultCn))
                            {
                                    $sql = "UPDATE `fin_customer_salesinvoice_header` SET intDeleteStatus ='1', intModifyer ='$userId'
                                            WHERE (`strReferenceNo`='$id')";
                                    $result = $db->RunQuery2($sql);
                                        //==========UPDATE TRANS ACTION delete STATUS
                                        $sql="SELECT fin_customer_salesinvoice_header.entryId FROM fin_customer_salesinvoice_header WHERE (`strReferenceNo`='$id')";
                                        $result = $db->RunQuery2($sql);
                                        $row = mysqli_fetch_array($result);
                                        $entryId=$row['entryId'];                        
                                        $sqld = "UPDATE `fin_transactions` SET delStatus=1 WHERE entryId=$entryId";
                                        $resultd = $db->RunQuery2($sqld);
                                        //============================
										//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
										// update dispatch table as Pay
										$sql="UPDATE ware_fabricdispatchdetails SET invoiced=0 WHERE
														ware_fabricdispatchdetails.strReferenceNo = '$id'";
										$dispatchResult = $db->RunQuery2($sql);
										//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                                    if(($result && $resultd && $dispatchResult))
                                    {
                                        $db->commit();
                                        $response['type'] 		= 'pass';
                                        $response['msg'] 		= 'Deleted successfully.';
                                    }
                                    else
                                    {
                                        $db->rollback();
                                        $response['type'] 		= 'fail';
                                        $response['msg'] 		= $db->errormsg;
                                        $response['q'] 			=$sql;
                                    }
                            }
                            else
                            {
                                            $db->rollback();
                                            $response['type'] 		= 'fail';
                                            $response['msg'] 		= "You cannot allow this process! Invoice has some credit note";
                            }
                    }
                    else
                    {
                                    $db->rollback();
                                    $response['type'] 		= 'fail';
                                    $response['msg'] 		= "You cannot allow this process! Invoice has some payements";
                    }
                    echo json_encode($response);
                    } 
                    catch (Exception $e) 
                    {
                            $db->rollback(); //roalback
                            $response['type'] = 'fail';
                            $response['msg'] = $e->getMessage();
                            $response['q'] = $sql;
                            echo json_encode($response);
                    } 
            }
					 
	}
	
//--------------------------------------------------------------------------------------------
	function getNextInvoiceNo($companyId,$locationId)
	{
		global $db;
		$sql = "SELECT
				intSalesInvoiceNo
				FROM sys_finance_no
				WHERE
				intCompanyId = '$companyId' AND intLocationId = '$locationId'
				";	
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$nextInvoiceNo = $row['intSalesInvoiceNo'];
		
		$sql = "UPDATE `sys_finance_no` SET intSalesInvoiceNo=intSalesInvoiceNo+1 WHERE (intCompanyId = '$companyId' AND intLocationId = '$locationId')";
		$db->RunQuery($sql);
		return $nextInvoiceNo;
	}
//--------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------
	function getLatestAccPeriod($companyId)
	{
		global $db;
		$sql = "SELECT
				MAX(mst_financeaccountingperiod.intId) AS accId,
				mst_financeaccountingperiod.dtmStartingDate,
				mst_financeaccountingperiod.dtmClosingDate,
				mst_financeaccountingperiod.intStatus,
				mst_financeaccountingperiod_companies.intCompanyId,
				mst_financeaccountingperiod_companies.intPeriodId
				FROM
				mst_financeaccountingperiod
				Inner Join mst_financeaccountingperiod_companies ON mst_financeaccountingperiod_companies.intPeriodId = mst_financeaccountingperiod.intId
				WHERE
				mst_financeaccountingperiod_companies.intCompanyId =  '$companyId' AND mst_financeaccountingperiod.intStatus = '1'
				ORDER BY
				mst_financeaccountingperiod.intId DESC
				";	
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$latestAccPeriodId = $row['accId'];	
		return $latestAccPeriodId;
	}
//--------------------------------------------------------------------------------------------
//============================================================================================
	function encodeInvoiceNo($invoiceNumber,$accountPeriod,$companyId,$locationId,$date)
	{
		global $db;
		$sql = "SELECT
				mst_financeaccountingperiod.intId,
				mst_financeaccountingperiod.dtmStartingDate,
				mst_financeaccountingperiod.dtmClosingDate,
				mst_financeaccountingperiod.intStatus
				FROM
				mst_financeaccountingperiod
				WHERE
				mst_financeaccountingperiod.intId =  '$accountPeriod'
				";	
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		if($row['dtmStartingDate'] <= $date && $date <= $row['dtmClosingDate'])
		{
			$startDate = substr($row['dtmStartingDate'],0,4);
			$closeDate = substr($row['dtmClosingDate'],0,4);
		}
		else
		{
				$sql = "SELECT
					mst_financeaccountingperiod.intId,
					mst_financeaccountingperiod.dtmStartingDate,
					mst_financeaccountingperiod.dtmClosingDate,
					mst_financeaccountingperiod.intStatus
					FROM
					mst_financeaccountingperiod
					WHERE
					mst_financeaccountingperiod.dtmStartingDate <= '$date' AND
					mst_financeaccountingperiod.dtmClosingDate >=  '$date'
					";	
				$result = $db->RunQuery($sql);
				$row = mysqli_fetch_array($result);
				$startDate = substr($row['dtmStartingDate'],0,4);
				$closeDate = substr($row['dtmClosingDate'],0,4);
		}
		$sql = "SELECT
				mst_companies.strCode AS company,
				mst_companies.intId,
				mst_locations.intCompanyId,
				mst_locations.strCode AS location,
				mst_locations.intId
				FROM
				mst_companies
				Inner Join mst_locations ON mst_locations.intCompanyId = mst_companies.intId
				WHERE
				mst_locations.intId =  '$locationId' AND
				mst_companies.intId =  '$companyId'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$companyCode = $row['company'];
		$locationCode = $row['location'];
		$invoiceFormat = $companyCode."/".$locationCode."/".$startDate."-".$closeDate."/".$invoiceNumber;
		//$invoiceFormat = $companyCode."/".$startDate."-".$closeDate."/".$invoiceNumber;
		return $invoiceFormat;
	}
//============================================================================================
?>
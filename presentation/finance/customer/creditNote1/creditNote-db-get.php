<?php
session_start();
$backwardseperator = "../../../../";
$mainPath 		= $_SESSION['mainPath'];
$locationId 	= $_SESSION["CompanyID"];
$companyId		= $_SESSION["headCompanyId"];

$thisFilePath 	=  $_SERVER['PHP_SELF'];
include  "{$backwardseperator}dataAccess/Connector.php";

$requestType = $_REQUEST['requestType'];

if($requestType=='loadCombo')
{
	$sql = "SELECT DISTINCT
			fin_customer_creditnote_header.strCreditNoteNo,
			mst_customer.strName,
			fin_customer_creditnote_header.intStatus 
			FROM
			fin_customer_creditnote_header
			Inner Join mst_customer ON fin_customer_creditnote_header.intCustomer = mst_customer.intId
			where fin_customer_creditnote_header.intStatus='1'
			AND fin_customer_creditnote_header.intCompanyId =  '$companyId'
			order by strCreditNoteNo desc
			";
	$result = $db->RunQuery($sql);
	$html = "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($result))
	{
		$html .= "<option value=\"".$row['strCreditNoteNo']."\">".$row['strCreditNoteNo']." - ".$row['strName']."</option>";
	}
	echo $html;
}
else if($requestType=='getCustomerAddress')
{
	$customerId  = $_REQUEST['customerId'];
	
	$sql = "SELECT 	mst_customer.strAddress FROM mst_customer	WHERE mst_customer.intId =  '$customerId'";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	$response['address'] =  $row['strAddress'];
	
	$sql = "SELECT
			fin_customer_salesinvoice_header.strReferenceNo,
			fin_customer_salesinvoice_header.intDeleteStatus
			FROM fin_customer_salesinvoice_header
			WHERE
			fin_customer_salesinvoice_header.intCustomerId =  '$customerId' AND
			fin_customer_salesinvoice_header.intCompanyId =  '$companyId' AND
			fin_customer_salesinvoice_header.intDeleteStatus =  '0'
			";
	$result = $db->RunQuery($sql);
	$salesInvoiceList ='<option value=""></option>';
	while($row = mysqli_fetch_array($result))
	{
		$salesInvoiceList .="<option value=\"".$row['strReferenceNo']."\">".$row['strReferenceNo']."</option>";	
	}
	$response['salesInvoiceList']	 = $salesInvoiceList;
	
	echo json_encode($response);
}
else if($requestType=='getInvoiceType') // invoice type
{
	$invoiceId  = $_REQUEST['invoiceId'];
	
	$sql = "SELECT
			fin_customer_salesinvoice_header.strReferenceNo,
			fin_customer_salesinvoice_header.strInvoiceType
			FROM
			fin_customer_salesinvoice_header
			WHERE
			fin_customer_salesinvoice_header.strReferenceNo =  '$invoiceId'";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	$response['invoType']= $row['strInvoiceType'];
	echo json_encode($response);
}

if($requestType=='getExchangeRate')
{
	$currencyId  	= $_REQUEST['currencyId'];
	$exchangeDate	= $_REQUEST['exchangeDate'];
	
	$sql = "SELECT
				mst_financeexchangerate.dblSellingRate,
				mst_financeexchangerate.dblBuying
			FROM mst_financeexchangerate
			WHERE
				mst_financeexchangerate.intCurrencyId 	=  '$currencyId' AND
				mst_financeexchangerate.dtmDate 		=  '$exchangeDate'
			";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	
	if(mysqli_num_rows($result)>0)
	{
		$arrValue['sellingRate'] 	= $row['dblSellingRate'];
		$arrValue['buyingRate'] 	= $row['dblBuying'];
	}
	else
	{
		$arrValue['sellingRate'] 	= '';
		$arrValue['buyingRate'] 	= '';
	}
	
	echo json_encode($arrValue);
}

if($requestType=='getItemList')
{
	$salesInvoiceNo  	= $_REQUEST['salesInvoiceNo'];
	
	
	$sql = "SELECT
				fin_customer_salesinvoice_details.intItem,
				mst_financecustomeritem.strName
			FROM
				fin_customer_salesinvoice_details
				Inner Join mst_financecustomeritem ON mst_financecustomeritem.intId = fin_customer_salesinvoice_details.intItem
			WHERE
				fin_customer_salesinvoice_details.strReferenceNo =  '$salesInvoiceNo'
			ORDER BY
				mst_financecustomeritem.intStatus ASC
			";
	$result = $db->RunQuery($sql);
	$html = '<option value=""></option>';
	while($row = mysqli_fetch_array($result))
	{
		$html .= '<option value="'.$row['intItem'].'">'.$row['strName'].'</option>';	
	}
		$response['itemList'] = $html;
	echo json_encode($response);
}
	else if($requestType=='getTaxValue')
	{
/*		$operation  = $_REQUEST['opType'];
		$amount  = $_REQUEST['valAmount'];
		$taxCodes = json_decode($_REQUEST['arrTaxCode'], true);
		
		if(count($taxCodes) != 0)
		{
			foreach($taxCodes as $taxCode)
			{
				$codeValues[] = callTaxValue($taxCode['taxId']);
			}
		}
		if(count($codeValues) > 1)
		{
			if($operation == 'Inclusive')
			{
				$firstVal = ($amount*$codeValues[0])/100;
				$withTaxVal = $firstVal + ((($amount+$firstVal)*$codeValues[1])/100);
			}
			else if($operation == 'Exclusive')
			{
				$withTaxVal = ($amount*($codeValues[0] + $codeValues[1]))/100;
			}
		}
		else if(count($codeValues) == 1 && $operation == 'Isolated')
		{
			$withTaxVal = ($amount*$codeValues[0])/100;
		}
		echo $withTaxVal;
*/
		$operation  = $_REQUEST['opType'];
		$amount  = $_REQUEST['valAmount'];
		$taxCodes = json_decode($_REQUEST['arrTaxCode'], true);
		
		if(count($taxCodes) != 0)
		{
			foreach($taxCodes as $taxCode)
			{
				$codeValues[] = callTaxValue($taxCode['taxId']);
			}
		}
		if(count($codeValues) > 1)
		{
			if($operation == 'Inclusive')
			{
				$firstVal = ($amount*$codeValues[0])/100;
				$withTaxVal = $firstVal + ((($amount+$firstVal)*$codeValues[1])/100);
				$val1 = ($amount*$codeValues[0])/100;
				$val2 = ((($amount+$firstVal)*$codeValues[1])/100);
			}
			else if($operation == 'Exclusive')
			{
				$withTaxVal = ($amount*($codeValues[0] + $codeValues[1]))/100;
				$val1 = ($amount*$codeValues[0])/100;
				$val2 = ($amount*$codeValues[1])/100;
			}
		}
		else if(count($codeValues) == 1 && $operation == 'Isolated')
		{
			$withTaxVal = ($amount*$codeValues[0])/100;
			$val1 = ($amount*$codeValues[0])/100;
		}
		echo $withTaxVal."/".$val1."/".$val2;
	}
	else if($requestType=='getTaxProcess')
	{
		$taxGrpId  = $_REQUEST['taxGroupId'];
		
		$sql = "SELECT
				mst_financetaxgroup.intId,
				mst_financetaxgroup.strProcess
				FROM
				mst_financetaxgroup
				WHERE
				mst_financetaxgroup.intId =  '$taxGrpId'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		echo $row['strProcess'];
	}
	function callTaxValue($taxId)
	{
		global $db;
		$sql = "SELECT
				mst_financetaxisolated.intId,
				mst_financetaxisolated.strCode,
				mst_financetaxisolated.dblRate
				FROM
				mst_financetaxisolated
				WHERE
				mst_financetaxisolated.intId = '$taxId'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$taxVal = $row['dblRate'];	
		return $taxVal;
	}

<?php
session_start();
$backwardseperator = "../../../../";
$thisFilePath =  $_SERVER['PHP_SELF'];

include  "{$backwardseperator}dataAccess/permisionCheck2.inc";

//$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
//$result = $db->RunQuery($sql);
//while($row=mysqli_fetch_array($result))
//{
//	$companyId = $row['intCompanyId']; 
//}

$companyId 	= $_SESSION['headCompanyId'];

if($_SERVER['REQUEST_METHOD'] == "POST")
{
	$optionValue = trim($_REQUEST['cboSearchOption']);
	$searchValue = trim($_REQUEST['txtSearchValue']);
}

//---------------------------------------------New Grid-----------------------------------------------------
include_once "../../../../libraries/jqdrid/inc/jqgrid_dist.php";

				 $sql = "select * from(SELECT
				 		fin_customer_receivedpayments_header.intReceiptNo,
						fin_customer_receivedpayments_header.intCompanyId,
						fin_customer_receivedpayments_header.dtmDate AS Date,
						fin_customer_receivedpayments_header.strReferenceNo AS ReceiptNumber, 'More' AS More,
						fin_customer_receivedpayments_header.intCustomerId,
						fin_customer_receivedpayments_header.intCurrencyId,
						fin_customer_receivedpayments_header.dblRecAmount AS Amount,
						fin_customer_receivedpayments_header.strRemark AS Memo,
						mst_customer.strName AS Customer,
						mst_financecurrency.strCode AS Currency
						FROM
						fin_customer_receivedpayments_header
						Inner Join mst_customer ON fin_customer_receivedpayments_header.intCustomerId = mst_customer.intId
						Inner Join mst_financecurrency ON fin_customer_receivedpayments_header.intCurrencyId = mst_financecurrency.intId
						WHERE
						fin_customer_receivedpayments_header.intCompanyId =  '$companyId' AND
						fin_customer_receivedpayments_header.intDeleteStatus =  '0') as t where 1=1";

$invoiceLink = "receivedPayments.php?id={ReceiptNumber}";
$invoiceReport = "receivedPaymentsDetails.php?id={ReceiptNumber}";


//Customer
$col["title"] = "Customer"; // caption of column
$col["name"] = "Customer"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "6";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Date
$col["title"] = "Date"; // caption of column
$col["name"] = "Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Currency
$col["title"] = "Currency"; // caption of column
$col["name"] = "Currency"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Memo
$col["title"] = "Memo"; // caption of column
$col["name"] = "Memo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "6";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Amount
$col["title"] = "Received Amount"; // caption of column
$col["name"] = "Amount"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "right";
$col['link']	= $invoiceLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

//Invoice Number
$col["title"] = "Receipt Number"; // caption of column
$col["name"] = "ReceiptNumber"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "7";
$col["align"] = "center";
$col['link']	= $invoiceLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

//View
$col["title"] = "View"; // caption of column
$col["name"] = "More"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "1";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $invoiceReport;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

$jq = new jqgrid('',$db);

$grid["caption"] 		= "Received Payments Listing";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'intReceiptNo'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes

//<><><><><><>======================Compulsory Code==============================<><><><><><>
	$jq->set_options($grid);
	$jq->select_command = $sql;
	$jq->set_columns($cols);
	$jq->set_actions(array(	
		"add"=>false, // allow/disallow add
		"edit"=>false, // allow/disallow edit
		"delete"=>false, // allow/disallow delete
		"rowactions"=>false, // show/hide row wise edit/del/save option
		"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
		"export"=>true
	) 
	);

$out = $jq->render("list1");
//<><><><><><>======================Compulsory Code==============================<><><><><><>

//---------------------------------------------New Grid-----------------------------------------------------

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Received Payments Listing</title>

<!--<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/tblstyle.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../libraries/jquery/jquery-1.3.2.min.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/scrolltable.js"></script>

<script type="application/javascript" src="../../../../libraries/javascript/zebraStripe.js"></script>-->

<script type="text/javascript" language="javascript">
function pageSubmit()
{
	document.getElementById('frmReceivedPaymentsListing').submit();	
}
function clearValue()
{
	document.getElementById('txtSearchValue').value = '';
	document.getElementById('txtSearchValue').focus();
}
</script>

</head>

<body>
<form id="frmReceivedPaymentsListing" name="frmReceivedPaymentsListing" method="post" autocomplete="off" action="receivedPaymentsListing.php">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
<div align="center">
<!--		<div class="trans_layoutL">
		  <div class="trans_text">Received Payments Listing</div>-->
<!-------------------------------------------New Grid---------------------------------------------------->
<link rel="stylesheet" type="text/css" media="screen" href="../../../../libraries/jqdrid/js/themes/smoothness/jquery-ui.custom.css"></link>	
<link rel="stylesheet" type="text/css" media="screen" href="../../../../libraries/jqdrid/js/jqgrid/css/ui.jqgrid.css"></link>	

<script src="../../../../libraries/jqdrid/js/jquery.min.js" type="text/javascript"></script>
<script src="../../../../libraries/jqdrid/js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
<script src="../../../../libraries/jqdrid/js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>	
<script src="../../../../libraries/jqdrid/js/themes/jquery-ui.custom.min.js" type="text/javascript"></script>
<script src="../../../../libraries/javascript/script.js" type="text/javascript"></script>
<!-------------------------------------------New Grid---------------------------------------------------->
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0" cellpadding="1" cellspacing="0" class="tableBorder_allRound">
          <!--<tr>
            <td width="16%" height="22" class="normalfnt">&nbsp;</td>
            <td width="18%" class="normalfntMid">Searching Option</td>
            <td width="17%" class="normalfnt">
            <select name="cboSearchOption" id="cboSearchOption" onchange="clearValue();" style="width:150px">
              <option value="">&nbsp;</option>
<option value="mst_customer.strName" <?php echo($optionValue=='mst_customer.strName'?'selected':'') ?> >Customer</option>
<option value="fin_customer_receivedpayments_header.dtmDate" <?php echo($optionValue=='fin_customer_receivedpayments_header.dtmDate'?'selected':'') ?> >Date Range</option>
<option value="fin_customer_receivedpayments_header.strReferenceNo" <?php echo($optionValue=='fin_customer_receivedpayments_header.strReferenceNo'?'selected':'') ?>>Receipt Number</option>
            </select></td>
            <td width="16%"><input value="<?php echo $searchValue; ?>" type="text" name="txtSearchValue" id="txtSearchValue" /></td>
            <td width="18%" class="normalfnt">
            <a href="#">
            <img src="../../../../images/search.png" width="20" height="20" onclick="pageSubmit();" /></a></td>
            <td width="15%">&nbsp;</td>
            </tr>-->
          <tr>
            <td height="22" class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            </tr>
          </table></td>
      </tr>
      <!--<tr>
        <td>
          <div id="_table_wrap" style="overflow: hidden; display: inline-block; border: 1px solid rgb(136, 136, 136);">
          <div id="_head_wrap" style="width: 900px; overflow: hidden;">
          <table class="tbl1 scroll" id="_head" style="table-layout: fixed;">
          <thead>
            <tr class="gridHeader">
              <th height="22" style="width: 100px;" >Customer</th>
              <th style="width: 80px;" >Date</th>
              <th style="width: 70px;" >Currency</th>
              <th style="width: 90px;" >Received Amount</th>
              <th style="width: 125px;" >Receipt Number</th>
              <th style="width: 8px;" ></th>
              </tr>
           </table>
           </div>
           <div class="normalfnt" id="_body_wrap" style="width: 900px; height: 320px; overflow: scroll;">
           <table class="tbl1 scroll"> 
           <tbody>
              <?php

				if($searchValue!='')
			  	$wherePart = " and $optionValue like '%$searchValue%'";
				
	 	 		$sql = "SELECT
						fin_customer_receivedpayments_header.intCompanyId,
						fin_customer_receivedpayments_header.dtmDate,
						fin_customer_receivedpayments_header.strReferenceNo,
						fin_customer_receivedpayments_header.intCustomerId,
						fin_customer_receivedpayments_header.intCurrencyId,
						fin_customer_receivedpayments_header.dblRecAmount,
						mst_customer.strName,
						mst_financecurrency.strCode
						FROM
						fin_customer_receivedpayments_header
						Inner Join mst_customer ON fin_customer_receivedpayments_header.intCustomerId = mst_customer.intId
						Inner Join mst_financecurrency ON fin_customer_receivedpayments_header.intCurrencyId = mst_financecurrency.intId
						WHERE
						fin_customer_receivedpayments_header.intCompanyId =  '$companyId' AND
						fin_customer_receivedpayments_header.intDeleteStatus =  '0'
						$wherePart
						ORDER BY
						mst_customer.strName ASC";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$id 		=	 $row['strReferenceNo'];
	  			 ?>
           		<tr class="normalfnt">
           		<td class="recPayCusName" align="center" bgcolor="#FFFFFF"><?php echo $row['strName'];?></td>
                <td class="recPayDate" align="center" bgcolor="#FFFFFF"><?php echo $row['dtmDate'];?></td>
              	<td class="recPayCurrency"  align="center" bgcolor="#FFFFFF"><?php echo $row['strCode'];?></td>
              	<td class="recAmount" bgcolor="#FFFFFF"><?php echo $row['dblRecAmount'];?></td>
              	<td class="recPayRef" align="center" bgcolor="#FFFFFF">
                <a target="_blank" href="<?php echo "receivedPayments.php?id=$id";?>">
				<?php echo $row['strReferenceNo'];?></a></td>
              </tr>
               <?php 
        	    } 
       		   ?>
              </tbody>
             </table>
            </div></div>
              </td>
              </tr>-->
            </table>
			</td>
      </tr>
      <tr>
        <td>
        <div align="center" style="margin:10px">
			<?php echo $out?>
        </div>
        </td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound"><a href="../../../../main.php"><img  src="../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
</body>
</html>

<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$companyId = $row['intCompanyId']; 
	}
	/////////// sales invoice load part /////////////////////
	if($requestType=='loadCombo')
	{
		$sql = "SELECT
				fin_customer_receivedpayments_header.strReferenceNo,
				fin_customer_receivedpayments_header.intReceiptNo,
				mst_customer.strName
				FROM
				fin_customer_receivedpayments_header
				Inner Join mst_customer ON fin_customer_receivedpayments_header.intCustomerId = mst_customer.intId
				WHERE
				fin_customer_receivedpayments_header.intCompanyId =  '$companyId' AND
				fin_customer_receivedpayments_header.intDeleteStatus = '0'
				ORDER BY intReceiptNo DESC
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['strReferenceNo']."\">".$row['strReferenceNo']." - ".$row['strName']."</option>";
		}
		echo $html;
	}
	
	//===========Add by dulakshi 2013.03.20=========
	else if($requestType=='loadCustomer')
	{
		$ledAcc  = $_REQUEST['ledgerAcc'];		
							
		$condition == "";
			if($ledAcc != "")
			{	
				$condition .= "AND mst_financecustomeractivate.intChartOfAccountId =  '$ledAcc'	";			
			}			
			
			$sql = "SELECT
						mst_customer.intId,
						mst_customer.strName,
						mst_financecustomeractivate.intCompanyId
					FROM
						mst_customer
						Inner Join mst_financecustomeractivate ON mst_customer.intId = mst_financecustomeractivate.intCustomerId
					WHERE
						mst_customer.intStatus =  '1' AND
						mst_financecustomeractivate.intCompanyId =  '$companyId' ".$condition ." order by mst_customer.strName ASC";
							
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
	}
	//==============================================
	
	else if($requestType=='getCustomerCurrency') // currency
	{
		$customerId  = $_REQUEST['customerId'];
		
		$sql = "SELECT 	mst_customer.strAddress, mst_customer.strInvoiceType,
				mst_customer.intCurrencyId,
				mst_customer.intPaymentsTermsId
				FROM mst_customer	WHERE mst_customer.intId =  '$customerId'";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$response['currency']= $row['intCurrencyId'];
		echo json_encode($response);
	}
	else if($requestType=='loadDetails')
	{
		$id  = $_REQUEST['id'];
		
		//-------------------------------------------------------------
		$sql = "SELECT
				fin_customer_receivedpayments_main_details.intReceiptNo,
				fin_customer_receivedpayments_main_details.intAccPeriodId,
				fin_customer_receivedpayments_main_details.intLocationId,
				fin_customer_receivedpayments_main_details.intCompanyId,
				fin_customer_receivedpayments_main_details.strReferenceNo,
				fin_customer_receivedpayments_main_details.strJobNo,
				fin_customer_receivedpayments_main_details.strDocNo,
				fin_customer_receivedpayments_main_details.dtmDate,
				fin_customer_receivedpayments_main_details.dblAmount,
				fin_customer_receivedpayments_main_details.dblToBePaid,
				fin_customer_receivedpayments_main_details.strCurrency,
				fin_customer_receivedpayments_main_details.dblRate,
				fin_customer_receivedpayments_main_details.dblPayAmount,
				fin_customer_receivedpayments_main_details.strDocType,
				fin_customer_receivedpayments_main_details.strDocNo,
				fin_customer_receivedpayments_header.intDeleteStatus,
				IFNULL(CONCAT(' - ref ',fin_customer_receivedpayments_main_details.strDocNoRef),'') AS refNo
				FROM
				fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strReferenceNo =  '$id' AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0'
				";
		$result = $db->RunQuery($sql);
		$arrDetail;
		while($row=mysqli_fetch_array($result))
		{
			$invoice 	= rawurlencode($row['strDocNo']);
			$jobNo 		= rawurlencode($row['strJobNo']);
			$displayJob = $row['strJobNo'].$row['refNo'];
			$docNo 		= rawurlencode($row['strDocNo']);
			$displayDoc = $row['strDocNo'].$row['refNo'];
			$date 		= $row['dtmDate'];
			$amount 	= $row['dblAmount'];
			$toBePaid 	= $row['dblToBePaid'];
			$currency 	= $row['strCurrency'];
			$rate 		= $row['dblRate'];
			$payAmount 	= $row['dblPayAmount'];
			$docType 	= $row['strDocType'];
			
			if($docType == 'S.Invoice')
			{
				$jobPath = $displayJob;
				$docPath = "<a target=_blank href=../salesInvoice/salesInvoice.php?id=$docNo>$displayDoc</a>";
			}
			//+++++++++++++++++++++++Debit Note Invoice Added By Lasantha @ CAIT on 17/12/2012+++++++++++++++++++++++++++
			else if($docType == 'D.Invoice')
			{
				$jobPath = "<a target=_blank href=../debitNoteInvoice/debitNoteInvoice.php?id=$jobNo>$displayJob</a>";
				$docPath = "<a target=_blank href=../salesInvoice/salesInvoice.php?id=$docNo>$displayDoc</a>";
			}
			//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
			else if($docType == 'A.Received')
			{
				$jobPath = $displayJob;
				$docPath = "<a target=_blank href=../advanceReceived/advanceReceived.php?receiptNo=$docNo>$displayDoc</a>";
			}
			else if($docType == 'C.Note')
			{
				$jobPath = "<a target=_blank href=../creditNote/creditNote.php?id=$jobNo>$displayJob</a>";
				$docPath = "<a target=_blank href=../salesInvoice/salesInvoice.php?id=$docNo>$displayDoc</a>";
			}
			else if($docType == 'B.Deposit')
			{
				$jobPath = $displayJob;
				$docPath = "<a target=_blank href=../../bank/deposit/deposit.php?depositNo=$docNo>$displayDoc</a>";
			}
			else if($docType == 'B.Payment')
			{
				$jobPath = $displayJob;
				$docPath = "<a target=_blank href=../../bank/payments/payments.php?paymentNo=$docNo>$displayDoc</a>";
			}
			else if($docType == 'Petty Cash')
			{
				$jobPath = $displayJob;
				$docPath = "<a target=_blank href=../../bank/pettyCash/pettyCash.php?pettyCashNo=$docNo>$displayDoc</a>";
			}
			else if($docType == 'JN')
			{
				$jobPath = $displayJob;
				$docPath = "<a target=_blank href=../../accountant/journalEntry/journalEntry.php?strReferenceNo=$docNo>$displayDoc</a>";
			}
			else
			{
				$jobPath = $displayJob;
				$docPath = $displayDoc;
			}
			
			$val['tBodyDetail'] = "<tr class=normalfnt bgcolor=#FFFFFF>
				<td align=center><input class=checkRow type=checkbox id=$invoice checked=checked /></td>
				<td width=170 align=center class=jobNo id=$jobNo>$jobPath</td>
				<td width=170 align=center class=docNo id=$docNo>$docPath</td>
				<td align=center class=docDate>$date</td>
				<td align=right class=amount>$amount</td>
				<td align=right class=toBePaid>$toBePaid</td>
				<td align=center class=docCurrency>$currency</td>
				<td align=right class=docRate>$rate</td>
				<td align=center><input  style=width:80px;text-align:right;background-color:#FFF;border:none name=txtPayAmount class=payAmount type=text id=txt$invoice value=$payAmount /></td> 
				<td align=center class=docType>$docType</td>
				</tr>"; // textBox --> disabled=disabled
			$arrDetail[] = $val;
			
		}
		$response['detailVal'] = $arrDetail;
		//-------------------------------------------------------------
		
		//-------------------------------------------------------------
		$sql = "SELECT
				fin_customer_receivedpayments_account_details.intReceiptNo,
				fin_customer_receivedpayments_account_details.intAccPeriodId,
				fin_customer_receivedpayments_account_details.intLocationId,
				fin_customer_receivedpayments_account_details.intCompanyId,
				fin_customer_receivedpayments_account_details.strReferenceNo,
				fin_customer_receivedpayments_account_details.intChartOfAccountId,
				fin_customer_receivedpayments_account_details.dblAmount,
				fin_customer_receivedpayments_account_details.strMemo,
				fin_customer_receivedpayments_account_details.intDimensionId,
				fin_customer_receivedpayments_header.intDeleteStatus
				FROM
				fin_customer_receivedpayments_account_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_account_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_account_details.strReferenceNo =  '$id' AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0'
				";
		$result = $db->RunQuery($sql);
		$arrAccDetail;
		while($row=mysqli_fetch_array($result))
		{
			$val1['chartAcc'] 	= $row['intChartOfAccountId'];
			$val1['amount'] 		= $row['dblAmount'];
			$val1['memo'] 		= $row['strMemo'];
			$val1['dimension']	= $row['intDimensionId'];
			$arrAccDetail[] = $val1;
		}
		$response['detailAccVal'] = $arrAccDetail;
		//-----------------------------------------------------------
		
				$sql = "SELECT
				fin_customer_receivedpayments_header.intReceiptNo,
				fin_customer_receivedpayments_header.intAccPeriodId,
				fin_customer_receivedpayments_header.intLocationId,
				fin_customer_receivedpayments_header.intCompanyId,
				fin_customer_receivedpayments_header.strReferenceNo,
				fin_customer_receivedpayments_header.intCustomerId,
				fin_customer_receivedpayments_header.dtmDate,
				fin_customer_receivedpayments_header.intCurrencyId,
				fin_customer_receivedpayments_header.dblRate,
				fin_customer_receivedpayments_header.dblRecAmount,
				fin_customer_receivedpayments_header.intPayMethodId,
				fin_customer_receivedpayments_header.strPayRefNo,
				fin_customer_receivedpayments_header.dtmPayRefDate,
				fin_customer_receivedpayments_header.intIsPosted,
				fin_customer_receivedpayments_header.strPayRefOrg,
				fin_customer_receivedpayments_header.strRemark
				FROM
				fin_customer_receivedpayments_header
				WHERE
				fin_customer_receivedpayments_header.strReferenceNo =  '$id' AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0'
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['customer'] 	= $row['intCustomerId'];
			$response['date'] 		= $row['dtmDate'];
			$response['currency']	= $row['intCurrencyId'];
			$response['rate'] 		= $row['dblRate'];
			$response['amount'] 	= $row['dblRecAmount'];
			$response['payMethod'] 	= $row['intPayMethodId'];
			$response['payRefNo'] 	= $row['strPayRefNo'];
			$response['payRefDate'] = $row['dtmPayRefDate'];
			$response['isPosted'] 	= $row['intIsPosted'];
			$response['payRefOrg'] 	= $row['strPayRefOrg'];
			$response['remark'] 	= $row['strRemark'];
		}
		echo json_encode($response);
	}
	else if($requestType=='getExchangeRate')
	{
		$currencyId  	= $_REQUEST['currencyId'];
		$exchangeDate	= $_REQUEST['exchangeDate'];
		
		$sql = "SELECT
					mst_financeexchangerate.dblSellingRate,
					mst_financeexchangerate.dblBuying
				FROM mst_financeexchangerate
				WHERE
					mst_financeexchangerate.intCurrencyId 	=  '$currencyId' AND
					mst_financeexchangerate.dtmDate 		=  '$exchangeDate'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		
		if(mysqli_num_rows($result)>0)
		{
			$arrValue['sellingRate'] 	= $row['dblSellingRate'];
			$arrValue['buyingRate'] 	= $row['dblBuying'];
		}
		else
		{
			$arrValue['sellingRate'] 	= "";
			$arrValue['buyingRate'] 	= "";
		}
		echo json_encode($arrValue);
	}
	else if($requestType=='getItemDescription')
	{
		$itemId  = $_REQUEST['itemId'];
		
		$sql = "SELECT
				mst_financecustomeritem.intId,
				mst_financecustomeritem.strRemark
				FROM
				mst_financecustomeritem
				WHERE
				mst_financecustomeritem.intId =  '$itemId'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		echo $row['strRemark'];
	}
	else if($requestType=='getTaxValue')
	{
		$operation  = $_REQUEST['opType'];
		$amount  = $_REQUEST['valAmount'];
		$taxCodes = json_decode($_REQUEST['arrTaxCode'], true);
		
		if(count($taxCodes) != 0)
		{
			foreach($taxCodes as $taxCode)
			{
				$codeValues[] = callTaxValue($taxCode['taxId']);
			}
		}
		if(count($codeValues) > 1)
		{
			if($operation == 'Inclusive')
			{
				$firstVal = ($amount*$codeValues[0])/100;
				$withTaxVal = $firstVal + ((($amount+$firstVal)*$codeValues[1])/100);
			}
			else if($operation == 'Exclusive')
			{
				$withTaxVal = ($amount*($codeValues[0] + $codeValues[1]))/100;
			}
		}
		else if(count($codeValues) == 1 && $operation == 'Isolated')
		{
			$withTaxVal = ($amount*$codeValues[0])/100;
		}
		echo $withTaxVal;
	}
	else if($requestType=='getTaxProcess')
	{
		$taxGrpId  = $_REQUEST['taxGroupId'];
		
		$sql = "SELECT
				mst_financetaxgroup.intId,
				mst_financetaxgroup.strProcess
				FROM
				mst_financetaxgroup
				WHERE
				mst_financetaxgroup.intId =  '$taxGrpId'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		echo $row['strProcess'];
	}
	//---------------------------------
	else if($requestType=='getInvoice')
	{
		$customerId  = $_REQUEST['customerId'];
		
		//////////////////////////Invoice//////////////////////////////////
		$sql = "SELECT
				fin_customer_salesinvoice_header.strReferenceNo,
				fin_customer_salesinvoice_header.intCustomerId,
				mst_customer.intId,
				mst_customer.strName,
				mst_financecurrency.intId,
				mst_financecurrency.strCode,
				fin_customer_salesinvoice_header.intInvoiceNo,
				fin_customer_salesinvoice_header.intAccPeriodId,
				fin_customer_salesinvoice_header.intLocationId,
				fin_customer_salesinvoice_header.intCompanyId,
				INV.intInvoiceNo,
				INV.intAccPeriodId,
				INV.intLocationId,
				INV.intCompanyId,
				fin_customer_salesinvoice_header.dtmDate,
				fin_customer_salesinvoice_header.dblRate,
				INV.dblQty,
				INV.dblUnitPrice,
				INV.dblDiscount,
				INV.dblTaxAmount,
				sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) AS amount,
				(
				sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) 
				-
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  INV.strReferenceNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'S.Invoice'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				
				) AS balAmount
				FROM
				fin_customer_salesinvoice_details AS INV
				Inner Join fin_customer_salesinvoice_header ON fin_customer_salesinvoice_header.strReferenceNo = INV.strReferenceNo AND INV.intInvoiceNo = fin_customer_salesinvoice_header.intInvoiceNo AND INV.intAccPeriodId = fin_customer_salesinvoice_header.intAccPeriodId AND INV.intCompanyId = fin_customer_salesinvoice_header.intCompanyId
				Inner Join mst_customer ON mst_customer.intId = fin_customer_salesinvoice_header.intCustomerId
				Inner Join mst_financecurrency ON mst_financecurrency.intId = fin_customer_salesinvoice_header.intCurrencyId
				WHERE
				fin_customer_salesinvoice_header.intCustomerId =  '$customerId' AND
				fin_customer_salesinvoice_header.intDeleteStatus =  '0'
				AND fin_customer_salesinvoice_header.intCompanyId =  '$companyId'
				GROUP BY
				fin_customer_salesinvoice_header.strReferenceNo
				having balAmount<>0
				"; // AND INV.intLocationId = fin_customer_salesinvoice_header.intLocationId
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$invoice = rawurlencode($row['strReferenceNo']);
			$displayNo = $row['strReferenceNo'];
			$amount = number_format($row['amount'],4,'.','');
			$toBePaid = number_format($row['balAmount'],4,'.','');
			$date = $row['dtmDate'];
			$currency = $row['strCode'];
			$rate = $row['dblRate'];
			echo 
				"
				<tr class=normalfnt bgcolor=#00FF66>
				<td align=center><input class=checkRow type=checkbox id=$invoice /></td>
				<td width=170 align=center class=jobNo id=$invoice>$displayNo</td>
				<td width=170 align=center class=docNo id=$invoice><a target=_blank href=../salesInvoice/salesInvoice.php?id=$invoice>$displayNo</a></td>
				<td align=center class=docDate>$date</td>
				<td align=right class=amount>$amount</td>
				<td align=right class=toBePaid>$toBePaid</td>
				<td align=center class=docCurrency>$currency</td>
				<td align=right class=docRate>$rate</td>
				<td align=center><input disabled=disabled style=width:80px;text-align:right;background-color:#FFF;border:none name=txtPayAmount  class=payAmount type=text id=txt$invoice /></td>
				<td align=center class=docType>S.Invoice</td>
				</tr>
				";
		} // readonly=readonly
		
		//+++++++++++++++++++++++++++++++++++Debit Note Invoice Added By Lasantha @ CAIT on 17/12/2012++++++++++++++++++++++++++++++++++++++++
		//////////////////////////Debit Note Invoice//////////////////////////////////
		$sql = "SELECT
				fin_customer_debitnoteinvoice_header.strReferenceNo,
				fin_customer_debitnoteinvoice_header.intCustomerId,
				mst_customer.intId,
				mst_customer.strName,
				mst_financecurrency.intId,
				mst_financecurrency.strCode,
				fin_customer_debitnoteinvoice_header.intInvoiceNo,
				fin_customer_debitnoteinvoice_header.intAccPeriodId,
				fin_customer_debitnoteinvoice_header.intLocationId,
				fin_customer_debitnoteinvoice_header.intCompanyId,
				INV.intInvoiceNo,
				INV.intAccPeriodId,
				INV.intLocationId,
				INV.intCompanyId,
				fin_customer_debitnoteinvoice_header.dtmDate,
				fin_customer_debitnoteinvoice_header.dblRate,
				fin_customer_debitnoteinvoice_header.strInvoiceNo,
				INV.dblQty,
				INV.dblUnitPrice,
				INV.dblDiscount,
				INV.dblTaxAmount,
				sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) AS amount,
				(
				sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) 
				-
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  INV.strReferenceNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'D.Invoice'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				
				) AS balAmount
				FROM
				fin_customer_debitnoteinvoice_details AS INV
				Inner Join fin_customer_debitnoteinvoice_header ON fin_customer_debitnoteinvoice_header.strReferenceNo = INV.strReferenceNo AND INV.intInvoiceNo = fin_customer_debitnoteinvoice_header.intInvoiceNo AND INV.intAccPeriodId = fin_customer_debitnoteinvoice_header.intAccPeriodId AND INV.intCompanyId = fin_customer_debitnoteinvoice_header.intCompanyId
				Inner Join mst_customer ON mst_customer.intId = fin_customer_debitnoteinvoice_header.intCustomerId
				Inner Join mst_financecurrency ON mst_financecurrency.intId = fin_customer_debitnoteinvoice_header.intCurrencyId
				WHERE
				fin_customer_debitnoteinvoice_header.intCustomerId =  '$customerId' AND
				fin_customer_debitnoteinvoice_header.intDeleteStatus =  '0'
				AND fin_customer_debitnoteinvoice_header.intCompanyId =  '$companyId'
				GROUP BY
				fin_customer_debitnoteinvoice_header.strReferenceNo
				having balAmount<>0
				"; // AND INV.intLocationId = fin_customer_debitnoteinvoice_header.intLocationId
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{			
			$salesInvoice = rawurlencode($row['strInvoiceNo']);
			$displayNo_si = $row['strInvoiceNo'];
			$invoice =  rawurlencode($row['strReferenceNo']);
			$displayNo_i = $row['strReferenceNo'];
			
			$amount = number_format($row['amount'],4,'.','');
			$toBePaid = number_format($row['balAmount'],4,'.','');
			$date = $row['dtmDate'];
			$currency = $row['strCode'];
			$rate = $row['dblRate'];
			echo 
				"
				<tr class=normalfnt bgcolor=#00FF66>
				<td align=center><input class=checkRow type=checkbox id=$invoice /></td>
				<td width=170 align=center class=jobNo id=$invoice><a target=_blank href=../debitNoteInvoice/debitNoteInvoice.php?id=$invoice>$displayNo_i</a></td>
				<td width=170 align=center class=docNo id=$salesInvoice><a target=_blank href=../salesInvoice/salesInvoice.php?id=$salesInvoice>$displayNo_si</a></td>
				<td align=center class=docDate>$date</td>
				<td align=right class=amount>$amount</td>
				<td align=right class=toBePaid>$toBePaid</td>
				<td align=center class=docCurrency>$currency</td>
				<td align=right class=docRate>$rate</td>
				<td align=center><input disabled=disabled style=width:80px;text-align:right;background-color:#FFF;border:none name=txtPayAmount  class=payAmount type=text id=txt$invoice /></td>
				<td align=center class=docType>D.Invoice</td>
				</tr>
				";
		} // readonly=readonly
		//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
		////////////////////////////Advanced Received////////////////////////////////
		$sql = "SELECT
				ADV.strReceiptNo,
				ADV.intCustomer,
				ADV.intCompanyId,
				ADV.dtDate,
				ADV.dblRate,
				ADV.intCurrency,
				ADV.dblReceivedAmount,
				mst_financecurrency.intId,
				mst_financecurrency.strCode,
				ADV.strPerfInvoiceNo,
				IFNULL(CONCAT(' - ref ',ADV.strPerfInvoiceNo),'') AS refNo,
				(
				ADV.dblReceivedAmount 
				+
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  ADV.strReceiptNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'A.Received'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				) AS balAmount
				FROM
				fin_customer_advancereceived_header AS ADV
				Inner Join mst_financecurrency ON ADV.intCurrency = mst_financecurrency.intId
				WHERE
				ADV.intCustomer =  '$customerId' AND
				ADV.intStatus = '1'
				AND ADV.intCompanyId =  '$companyId'
				having balAmount<>0
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$receipt = rawurlencode($row['strReceiptNo']);
			$displayNo = $row['strReceiptNo'].$row['refNo'];
			$amount = number_format($row['dblReceivedAmount'],4,'.','');
			$toBePaid = number_format(($row['balAmount']*(-1)),4,'.','');
			$date = $row['dtDate'];
			$currency = $row['strCode'];
			$rate = $row['dblRate'];
			
			$docRefNo = rawurlencode($row['strPerfInvoiceNo']);
			
			echo 
				"
				<tr class=normalfnt bgcolor=#CAFEB8>
				<td align=center><input class=checkRow type=checkbox id=$receipt /></td>
				<td width=170 align=center class=jobNo id=$receipt>$displayNo</td>
				<td width=170 align=center class=docNo id=$receipt><a target=_blank href=../advanceReceived/advanceReceived.php?receiptNo=$receipt>$displayNo</a></td>
				<td align=center class=docDate>$date</td>
				<td align=right class=amount>$amount</td>
				<td align=right class=toBePaid>$toBePaid</td>
				<td align=center class=docCurrency>$currency</td>
				<td align=right class=docRate>$rate</td>
				<td align=center><input disabled=disabled style=width:80px;text-align:right;background-color:#FFF;border:none name=txtPayAmount  class=payAmount type=text id=txt$invoice /></td>
				<td align=center class=docType id=$docRefNo>A.Received</td>
				</tr>
				";
		}
		//////////////////////////Credit Note//////////////////////////
		$sql = "SELECT
				fin_customer_creditnote_header.strReferenceNo,
				CRN.strReferenceNo,
				fin_customer_creditnote_header.intCustomerId,
				fin_customer_creditnote_header.strInvoiceNo,
				fin_customer_creditnote_header.dtmDate,
				fin_customer_creditnote_header.dblRate AS hRate,
				fin_customer_creditnote_header.intCurrencyId,
				CRN.intItem,
				CRN.intUom,
				CRN.dblQty,
				CRN.dblDiscount,
				mst_financecurrency.intId,
				mst_financecurrency.strCode,
				CRN.dblUnitPrice,
				CRN.dblTaxAmount,
				SUM(((CRN.dblQty*CRN.dblUnitPrice) *(100-CRN.dblDiscount)/100)+ IFNULL(CRN.dblTaxAmount,0)) AS amount,
				(
				SUM(((CRN.dblQty*CRN.dblUnitPrice) *(100-CRN.dblDiscount)/100)+ IFNULL(CRN.dblTaxAmount,0))
				+
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  CRN.strReferenceNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'C.Note'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				) AS balAmount
				FROM
				fin_customer_creditnote_details AS CRN
				Inner Join fin_customer_creditnote_header ON CRN.strReferenceNo = fin_customer_creditnote_header.strReferenceNo
				Inner Join mst_financecurrency ON fin_customer_creditnote_header.intCurrencyId = mst_financecurrency.intId
				WHERE
				fin_customer_creditnote_header.intCustomerId =  '$customerId' AND
				fin_customer_creditnote_header.intDeleteStatus = '0'
				AND fin_customer_creditnote_header.intCompanyId =  '$companyId'
				GROUP BY
				CRN.strReferenceNo
				having balAmount<>0
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$credit = rawurlencode($row['strReferenceNo']);
			$displayNo_c = $row['strReferenceNo'];
			$invoice =  rawurlencode($row['strInvoiceNo']);
			$displayNo_i = $row['strInvoiceNo'];
			
			$amount = number_format($row['amount'],4,'.','');
			$toBePaid = number_format(($row['balAmount']*(-1)),4,'.','');
			$date = $row['dtmDate'];
			$currency = $row['strCode'];
			$rate = $row['hRate'];
			
			echo 
				"
				<tr class=normalfnt bgcolor=#FFFFD7>
				<td align=center><input class=checkRow type=checkbox id=$credit /></td>
				<td width=170 align=center class=jobNo id=$credit><a target=_blank href=../creditNote/creditNote.php?id=$credit>$displayNo_c</a></td>
				<td width=170 align=center class=docNo id=$invoice><a target=_blank href=../salesInvoice/salesInvoice.php?id=$invoice>$displayNo_i</a></td>
				<td align=center class=docDate>$date</td>
				<td align=right class=amount>$amount</td>
				<td align=right class=toBePaid>$toBePaid</td>
				<td align=center class=docCurrency>$currency</td>
				<td align=right class=docRate>$rate</td>
				<td align=center><input disabled=disabled style=width:80px;text-align:right;background-color:#FFF;border:none name=txtPayAmount  class=payAmount type=text id=txt$invoice /></td>
				<td align=center class=docType>C.Note</td>
				</tr>
				";
		}
		//////////////////////////Bank Deposit/////////////////////////
		$sql = "SELECT
				BND.strDepositNo,
				BND.dtDate,
				BND.intCurrency,
				fin_bankdeposit_details.intAccount,
				fin_bankdeposit_details.intRecvFrom,
				fin_bankdeposit_details.dblAmmount,
				BND.dblRate,
				BND.intCompanyId,
				mst_financecurrency.strCode,
				mst_financecurrency.intId,
				mst_financechartofaccounts.intId,
				mst_financechartofaccounts.intFinancialTypeId,
				BND.strFnRefNo,
				IFNULL(CONCAT(' - ref ',BND.strFnRefNo),'') AS refNo,
				(
				fin_bankdeposit_details.dblAmmount 
				+
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  BND.strDepositNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'B.Deposit'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				
				) AS balAmount
				FROM
				fin_bankdeposit_header BND
				Inner Join fin_bankdeposit_details ON fin_bankdeposit_details.strDepositNo = BND.strDepositNo
				Inner Join mst_financecurrency ON BND.intCurrency = mst_financecurrency.intId
				Inner Join mst_financechartofaccounts ON fin_bankdeposit_details.intAccount = mst_financechartofaccounts.intId
				WHERE
				fin_bankdeposit_details.intRecvFrom =  '$customerId' AND
				mst_financechartofaccounts.intFinancialTypeId =  '10' AND
				BND.intStatus = '1'
				AND BND.intCompanyId =  '$companyId'
				having balAmount<>0
		";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$deposit = rawurlencode($row['strDepositNo']);
			$displayNo = $row['strDepositNo'].$row['refNo'];
			$amount = number_format($row['dblAmmount'],4,'.','');
			$toBePaid = number_format(($row['balAmount']*(-1)),4,'.','');
			$date = substr($row['dtDate'],0,10);
			$currency = $row['strCode'];
			$rate = $row['dblRate'];
			
			$docRefNo = rawurlencode($row['strFnRefNo']);
			
			echo 
				"
				<tr class=normalfnt bgcolor=#99CCFF>
				<td align=center><input class=checkRow type=checkbox id=$deposit /></td>
				<td width=170 align=center class=jobNo id=$deposit>$displayNo</td>
				<td width=170 align=center class=docNo id=$deposit><a target=_blank href=../../bank/deposit/deposit.php?depositNo=$deposit>$displayNo</a></td>
				<td align=center class=docDate>$date</td>
				<td align=right class=amount>$amount</td>
				<td align=right class=toBePaid>$toBePaid</td>
				<td align=center class=docCurrency>$currency</td>
				<td align=right class=docRate>$rate</td>
				<td align=center><input disabled=disabled style=width:80px;text-align:right;background-color:#FFF;border:none name=txtPayAmount  class=payAmount type=text id=txt$invoice /></td>
				<td align=center class=docType id=$docRefNo>B.Deposit</td>
				</tr>
				";
		}
		///////////////////////////////////////////////////////////////////////
		
		//////////////////////////Bank Payments////////////////////////////////
		$sql = "SELECT
				BNP.strBankPaymentNo,
				fin_bankpayment_details.intAccountId,
				BNP.dtDate,
				BNP.dblRate,
				BNP.intCurrency,
				BNP.dblReceivedAmount,
				mst_financecurrency.intId,
				mst_financecurrency.strCode,
				fin_bankpayment_details.strBankPaymentNo,
				fin_bankpayment_details.intPayTo,
				fin_bankpayment_details.dblAmmount,
				mst_financechartofaccounts.intFinancialTypeId,
				mst_financechartofaccounts.intId,
				BNP.strPerfInvoiceNo,
				IFNULL(CONCAT(' - ref ',BNP.strPerfInvoiceNo),'') AS refNo,
				(
				fin_bankpayment_details.dblAmmount
				-
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  BNP.strBankPaymentNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'B.Payment'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				
				) AS balAmount
				FROM
				fin_bankpayment_header BNP
				Inner Join fin_bankpayment_details ON fin_bankpayment_details.strBankPaymentNo = BNP.strBankPaymentNo
				Inner Join mst_financecurrency ON BNP.intCurrency = mst_financecurrency.intId
				Inner Join mst_financechartofaccounts ON fin_bankpayment_details.intAccountId = mst_financechartofaccounts.intId
				WHERE
				fin_bankpayment_details.intPayTo =  '$customerId' AND
				mst_financechartofaccounts.intFinancialTypeId =  '10' AND
				BNP.intStatus = '1'
				AND BNP.intCompanyId =  '$companyId'
				having balAmount<>0
		";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$payment = rawurlencode($row['strBankPaymentNo']);
			$displayNo = $row['strBankPaymentNo'].$row['refNo'];
			$amount = number_format($row['dblAmmount'],4,'.','');
			$toBePaid = number_format($row['balAmount'],4,'.','');
			$date = substr($row['dtDate'],0,10);
			$currency = $row['strCode'];
			$rate = $row['dblRate'];
			
			$docRefNo = rawurlencode($row['strPerfInvoiceNo']);
			
			echo 
				"
				<tr class=normalfnt bgcolor=#FDF5E6>
				<td align=center><input class=checkRow type=checkbox id=$payment /></td>
				<td width=170 align=center class=jobNo id=$payment>$displayNo</td>
				<td width=170 align=center class=docNo id=$payment><a target=_blank href=../../bank/payments/payments.php?paymentNo=$payment>$displayNo</a></td>
				<td align=center class=docDate>$date</td>
				<td align=right class=amount>$amount</td>
				<td align=right class=toBePaid>$toBePaid</td>
				<td align=center class=docCurrency>$currency</td>
				<td align=right class=docRate>$rate</td>
				<td align=center><input disabled=disabled style=width:80px;text-align:right;background-color:#FFF;border:none name=txtPayAmount  class=payAmount type=text id=txt$invoice /></td>
				<td align=center class=docType id=$docRefNo>B.Payment</td>
				</tr>
				";
		}
		///////////////////////////////////////////////////////////////////////
		
		//////////////////////////Petty Cash//////////////////////////////////
		$sql = "SELECT
				PTC.strPettyCashNo,
				PTC.dtDate,
				PTC.dblRate,
				PTC.intCurrency,
				PTC.dblReceivedAmount,
				fin_bankpettycash_details.intAccountId,
				fin_bankpettycash_details.intPayTo,
				fin_bankpettycash_details.dblAmmount,
				mst_financecurrency.intId,
				mst_financecurrency.strCode,
				mst_financechartofaccounts.intId,
				mst_financechartofaccounts.intFinancialTypeId,
				PTC.strFnRefNo,
				IFNULL(CONCAT(' - ref ',PTC.strFnRefNo),'') AS refNo,
				(
				fin_bankpettycash_details.dblAmmount
				-
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  PTC.strPettyCashNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'Petty Cash'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				) AS balAmount
				FROM
				fin_bankpettycash_header AS PTC
				Inner Join fin_bankpettycash_details ON fin_bankpettycash_details.strPettyCashNo = PTC.strPettyCashNo
				Inner Join mst_financechartofaccounts ON fin_bankpettycash_details.intAccountId = mst_financechartofaccounts.intId
				Inner Join mst_financecurrency ON PTC.intCurrency = mst_financecurrency.intId
				WHERE
				mst_financechartofaccounts.intFinancialTypeId =  '10'
				AND
				fin_bankpettycash_details.intPayTo =  '$customerId' AND
				PTC.intStatus = '1'
				AND PTC.intCompanyId =  '$companyId'
				having balAmount<>0
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$pettyCash = rawurlencode($row['strPettyCashNo']);
			$displayNo = $row['strPettyCashNo'].$row['refNo'];
			$amount = number_format($row['dblAmmount'],4,'.','');
			$toBePaid = number_format($row['balAmount'],4,'.','');
			$date = substr($row['dtDate'],0,10);
			$currency = $row['strCode'];
			$rate = $row['dblRate'];
			
			$docRefNo = rawurlencode($row['strFnRefNo']);
			
			echo 
				"
				<tr class=normalfnt bgcolor=#CDCDC1>
				<td align=center><input class=checkRow type=checkbox id=$pettyCash /></td>
				<td width=170 align=center class=jobNo id=$pettyCash>$displayNo</td>
				<td width=170 align=center class=docNo id=$pettyCash><a target=_blank href=../../bank/pettyCash/pettyCash.php?pettyCashNo=$pettyCash>$displayNo</a></td>
				<td align=center class=docDate>$date</td>
				<td align=right class=amount>$amount</td>
				<td align=right class=toBePaid>$toBePaid</td>
				<td align=center class=docCurrency>$currency</td>
				<td align=right class=docRate>$rate</td>
				<td align=center><input disabled=disabled style=width:80px;text-align:right;background-color:#FFF;border:none name=txtPayAmount  class=payAmount type=text id=txt$invoice /></td>
				<td align=center class=docType id=$docRefNo>Petty Cash</td>
				</tr>
				";
		}
		///////////////////////////////////////////////////////////////////////
                
                //////////////////////////Jurnel Entry////////////////////////////////
		$sql = "SELECT
				JH.strReferenceNo,
				JH.dtmDate,
				JH.dblRate,
				JH.intCurrencyId,
				mst_financecurrency.strCode,
				JD.dblDebitAmount,
				JD.dbCreditAmount,
				JH.strFnRefNo,
				IFNULL(CONCAT(' - ref ',JH.strFnRefNo),'') AS refNo,
				(
				JD.dblDebitAmount - JD.dbCreditAmount -
				IFNULL((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM
				fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				Inner Join fin_accountant_journal_entry_details ON fin_customer_receivedpayments_main_details.strDocNo = fin_accountant_journal_entry_details.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  'JH.strReferenceNo' AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND
				fin_customer_receivedpayments_main_details.strDocType =  'JN' AND
				fin_customer_receivedpayments_header.intCustomerId =  '$customerId' AND
				fin_accountant_journal_entry_details.strPersonType =  'cus'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				) AS balAmount
				FROM
				fin_accountant_journal_entry_header AS JH
				INNER JOIN fin_accountant_journal_entry_details AS JD ON JH.strReferenceNo = JD.strReferenceNo
				INNER JOIN mst_financecurrency ON JH.intCurrencyId = mst_financecurrency.intId
				INNER JOIN mst_financechartofaccounts ON JD.intChartOfAccountId = mst_financechartofaccounts.intId
				WHERE
				JD.strPersonType = 'cus' AND
				JD.intNameId = $customerId AND
				JH.intDeleteStatus = 0 AND
				mst_financechartofaccounts.intFinancialTypeId = 10
				AND JH.intCompanyId =  '$companyId'
				HAVING
				balAmount <> 0";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$payment = rawurlencode($row['strReferenceNo']);
			$displayNo = $row['strReferenceNo'].$row['refNo'];
			
			$docRefNo = rawurlencode($row['strFnRefNo']);
			
			$amount = number_format(($row['dblDebitAmount'] + $row['dbCreditAmount']),4,'.','');
                        
                        //if($row['dbCreditAmount']!=0){
//                            $toBePaid = number_format(($row['balAmount']*(-1)),4,'.','');
//                        }
                        //else{
                            $toBePaid = number_format(($row['balAmount']),4,'.','');
                        //}                       
			                        
			$date = substr($row['dtmDate'],0,10);
			$currency = $row['strCode'];
			$rate = $row['dblRate'];
			
			echo 
				"
				<tr class=normalfnt bgcolor=#FDF5E6>
				<td align=center><input class=checkRow type=checkbox id=$payment /></td>
				<td width=170 align=center class=jobNo id=$payment>$displayNo</td>
				<td width=170 align=center class=docNo id=$payment><a target=_blank href=../../accountant/journalEntry/journalEntry.php?strReferenceNo=$payment>$displayNo</a></td>
				<td align=center class=docDate>$date</td>
				<td align=right class=amount>$amount</td>
				<td align=right class=toBePaid>$toBePaid</td>
				<td align=center class=docCurrency>$currency</td>
				<td align=right class=docRate>$rate</td>
				<td align=center><input disabled=disabled style=width:80px;text-align:right;background-color:#FFF;border:none name=txtPayAmount  class=payAmount type=text id=txt$invoice /></td>
				<td align=center class=docType id=$docRefNo>JN</td>
				</tr>
				";
		}
		///////////////////////////////////////////////////////////////////////
	}
	//---------------------------------
	function callTaxValue($taxId)
	{
		global $db;
		$sql = "SELECT
				mst_financetaxisolated.intId,
				mst_financetaxisolated.strCode,
				mst_financetaxisolated.dblRate
				FROM
				mst_financetaxisolated
				WHERE
				mst_financetaxisolated.intId = '$taxId'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$taxVal = $row['dblRate'];	
		return $taxVal;
	}
?>
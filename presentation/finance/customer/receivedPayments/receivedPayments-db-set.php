<?php 
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	include "UnAdgestedCurrencyGLFunctions.php";
	$response = array('type'=>'', 'msg'=>'');
	$sql = "SELECT DISTINCT
			mst_locations.intCompanyId,
			mst_companies.intBaseCurrencyId
			FROM
			mst_locations
			Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
			WHERE mst_locations. intId=".$_SESSION["CompanyID"]."";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$companyId 		= $row['intCompanyId'];
		$baseCurrencyId = null($row['intBaseCurrencyId']);
	}
	$locationId = $_SESSION["CompanyID"];
	////////////////////////// main parameters ////////////////////////////////
	$receipt		= trim($_REQUEST['txtNo']);
	$requestType 	= $_REQUEST['requestType'];
	$amStatus 		= $_REQUEST['amStatus'];
	$id 			= $_REQUEST['cboSearch'];
	///////////////////// received payments header parameters ////////////////////
	$customer		= null(trim($_REQUEST['cboCustomer']));
	$date			= trim($_REQUEST['txtDate']);
	$currency		= null(trim($_REQUEST['cboCurrency']));
	$rate			= val(trim($_REQUEST['txtRate']));
	$receivedAmount	= val(trim($_REQUEST['txtRecAmount']));
	$payMethod		= null($_REQUEST['cboPaymentsMethods']);
	$payRefNo 		= $_REQUEST['txtRefNo'];
	$payRefDate		= trim($_REQUEST['txtRefDate']);
	$isPosted		= null(($_REQUEST['chkPosted']?1:0));
	$payRefOrg		= trim($_REQUEST['txtRefOrg']);
	$remarks 		= $_REQUEST['txtRemarks'];
	
	///////////////////// received payments detail parameters /////////////////////
	$mainDetails 	= json_decode($_REQUEST['recPayDetail'], true);
	$accDetails 	= json_decode($_REQUEST['recPayAccDetail'], true);
	///////////////////////////////////////////////////////////////////////////

	//////////////////////// received payments insert part ///////////////////////
	if($requestType=='add')
	{
			try
			{
				$receiptNumber 		= getNextReceiptNo($companyId,$locationId);
				$accountPeriod 		= getLatestAccPeriod($companyId);
				if($amStatus == "Auto")
				{
					$receiptReference	= trim(encodeReceiptNo($receiptNumber,$accountPeriod,$companyId,$locationId,$date));
				}
				else if($amStatus == "Manual")
				{
					$receiptReference	= $receipt;
				}
				$GLNumber 			= getNextGLNo($companyId,$locationId);		
				$GLReference		= trim(encodeGLNo($GLNumber,$accountPeriod,$companyId,$locationId,$date));
						
				$db->begin();
                
                //Add data to transaction header*******************************************
                $sql="INSERT INTO fin_transactions (entryDate, strProgramType, documentNo, currencyId, currencyRate, transDetails, payMethodId, paymentNumber, accPeriod, userId, companyId, createdOn) VALUES
                    ('$date','Received Payments','$receiptReference',$currency,$rate,'$remarks',$payMethod,'$payRefNo',$accountPeriod,$userId,$companyId,now())";
                
                $db->RunQuery2($sql);
                $entryId=$db->insertId;                
                //********************************************************************************                
		$sql = "INSERT INTO `fin_customer_receivedpayments_header`
		(`intReceiptNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strReferenceNo`,`intCustomerId`,`dtmDate`,`intCurrencyId`,`dblRate`,`dblRecAmount`,`intPayMethodId`,`strPayRefNo`,`dtmPayRefDate`,`intIsPosted`,`strPayRefOrg`,`strRemark`,`intCreator`,dtmCreateDate,`intDeleteStatus`,entryId)
		VALUES ('$receiptNumber','$accountPeriod','$locationId','$companyId','$receiptReference',$customer,'$date',$currency,'$rate','$receivedAmount',$payMethod,'$payRefNo','$payRefDate',$isPosted,'$payRefOrg','$remarks','$userId',now(), '0',$entryId)";

		$firstResult = $db->RunQuery2($sql);
		if(count($mainDetails) != 0 && $firstResult)
		{
			foreach($mainDetails as $detail)
			{
				$jobNo 		= trim($detail['jobNo']);
				$docNo		= trim($detail['docNo']);
				$docDate	= $detail['docDate'];
				$amount 	= val($detail['amount']);
				$toBePaid	= val($detail['toBePaid']);
				$currencyCode	= trim($detail['currency']);
				$docRate		= val($detail['rate']);
				$payAmount 	= val($detail['payAmount']);
				$docType 	= trim($detail['docType']);
				$invAmount	+= (float)$payAmount*(float)$docRate;
				
				$docRefNo 	= trim($detail['docRefNo']);
							
				$sql = "INSERT INTO `fin_customer_receivedpayments_main_details` (`intReceiptNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strReferenceNo`,`strJobNo`,`strDocNo`,`dtmDate`,`dblAmount`,`dblToBePaid`,`strCurrency`,`dblRate`,`dblPayAmount`,`strDocType`,`intCreator`,dtmCreateDate,`strDocNoRef`) 
				VALUES ('$receiptNumber','$accountPeriod','$locationId','$companyId','$receiptReference','$jobNo','$docNo','$docDate','$amount','$toBePaid','$currencyCode','$docRate','$payAmount','$docType','$userId',now(),'$docRefNo')";
				
				$mainDetailResult = $db->RunQuery2($sql);
                                
                                //Revers unAjdested gen loss entries
                                reversCustomerEntry($customer,$detail,$date,$locationId,$userId,$receiptReference);
			}
		}
		if(count($accDetails) != 0 && $firstResult && $mainDetailResult)
		{
			foreach($accDetails as $detail)
			{
                            $accId 		= $detail['accId'];
                            $accAmount	= $detail['accAmount'];
                            $memo		= $detail['memo'];
                            $dimension 	= val($detail['dimension']);

                            $sql = "INSERT INTO `fin_customer_receivedpayments_account_details` (`intReceiptNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strReferenceNo`,`intChartOfAccountId`,`dblAmount`,`strMemo`,`intDimensionId`,`intCreator`,dtmCreateDate) 
                            VALUES ('$receiptNumber','$accountPeriod','$locationId','$companyId','$receiptReference','$accId','$accAmount','$memo','$dimension','$userId',now())";

                            $accDetailResult = $db->RunQuery2($sql);
				
                            //>>>>>>>>>>>>>>>>>>>>>>transaction table process - account>>>>>>>>>>>>>>>>>>>>>>>>>	
                            if($accDetailResult){		                  
                                $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                                            ($entryId,'D',$accId,$accAmount,'$memo',$dimension)";
                                $trnResult = $db->RunQuery2($sql);
                            }
			//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			}
		}
		//>>>>>>>>>>>>>>>>>>>>>>transaction table process - customer>>>>>>>>>>>>>>>>>>>>>>>>>
		if($mainDetailResult)	
		{
                    $sql = "SELECT
                            mst_financecustomeractivate.intChartOfAccountId
                                FROM mst_financecustomeractivate
                                WHERE
                                mst_financecustomeractivate.intCustomerId =  '$customer' AND
                                mst_financecustomeractivate.intCompanyId =  '$companyId'";
                    $result = $db->RunQuery2($sql);
                    $row = mysqli_fetch_array($result);
                    $custAccount = $row['intChartOfAccountId'];				 
			
                    $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId,personType, personId) VALUES 
                                    ($entryId,'C',$custAccount,$receivedAmount,'$remarks',null,'cus',$customer)";
                    $trnResult = $db->RunQuery2($sql);
		}
		//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		
		////////////////////////// customer Gain-Loss insert part //////////////////////////	
		
		$sql = "SELECT
				mst_financecurrencyactivate.intCurrencyId,
				mst_financecurrencyactivate.intCompanyId,
				mst_financecurrencyactivate.intRealizeGainId,
				mst_financecurrencyactivate.intRealizeLostId,
				mst_financecurrencyactivate.intUnrealizeGainId,
				mst_financecurrencyactivate.intUnrealizeLostId,
				mst_financecurrency.intId
				FROM
				mst_financecurrencyactivate
				Inner Join mst_financecurrency ON mst_financecurrency.intId = mst_financecurrencyactivate.intCurrencyId
				WHERE
				mst_financecurrencyactivate.intCurrencyId =  '$currency' AND
				mst_financecurrencyactivate.intCompanyId =  '$companyId'
				";
		
		$result = $db->RunQuery2($sql);
		while($row=mysqli_fetch_array($result))
		{
			$reGain 	= $row['intRealizeGainId'];
			$reLost 	= $row['intRealizeLostId'];
			$unReGain 	= $row['intUnrealizeGainId'];
			$unReLost 	= $row['intUnrealizeLostId'];
		}
		$accAmount	= ((float)$receivedAmount*(float)$rate - (float)$invAmount);
		$dimension 	= 0;
		
		if($accAmount > 0)
		{
			$accStatus 	= 'C';
			$cusStatus 	= 'D';
			$memo		= "Realize Exchange Gain";
			$accId		= $reGain;
		}
		else if($accAmount < 0)
		{
			$accStatus 	= 'D';
			$cusStatus 	= 'C';
			$memo		= "Realize Exchange Loss";
			$accId		= $reLost;
            $accAmount=$accAmount*(-1);
		}
		else if($accAmount == 0)
		{
			$accId		= 0;
			$gainSatus	= "NON";
			$memo		= "No Exchange Gain/Loss";
		}
                //Add data to transaction header for gain los*******************************************
                $sql="INSERT INTO fin_transactions (entryDate, strProgramType, documentNo, currencyId, currencyRate, transDetails, payMethodId, paymentNumber, accPeriod, userId, companyId, createdOn) VALUES
                    ('$date','Customer Gain-Loss','$GLReference',$baseCurrencyId,1,'$memo',null,null,$accountPeriod,$userId,$companyId,now())";
                
                $db->RunQuery2($sql);
                $entryIdG=$db->insertId;
		//==========================================
		$sql = "INSERT INTO `fin_customer_gain_loss_header`
                            (`intGLNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strReferenceNo`,`intCustomerId`,`dtmDate`,`strRemark`,`intChartOfAccountId`,`dblAmount`,`strMemo`,`intDimensionId`,`intCreator`,dtmCreateDate,`intDeleteStatus`,entryId)
                            VALUES ('$GLNumber','$accountPeriod','$locationId','$companyId','$GLReference',$customer,'$date','$memo','$accId','$accAmount','$memo','$dimension','$userId',now(), '0',$entryIdG)";

		$firstResult2 = $db->RunQuery2($sql);

		if($firstResult2)
		{
			$docNo 		= $receiptReference;
			$recAmount	= (float)$receivedAmount*(float)$rate;
							
			$sql = "INSERT INTO `fin_customer_gain_loss_details` (`intGLNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strReferenceNo`,`strDocNo`,`dblReceiveAmount`,`dblInvoiceAmount`,`intCreator`,dtmCreateDate) 
			VALUES ('$GLNumber','$accountPeriod','$locationId','$companyId','$GLReference','$docNo','$recAmount','$invAmount','$userId',now())";
			
			$mainDetailResult2 = $db->RunQuery2($sql);			
		}
		//>>>>>>>>>>>>>>>>>>>>>>transaction table process - customer>>>>>>>>>>>>>>>>>>>>>>>>>
		if($gainSatus != "NON")
		{
			if($mainDetailResult2 && $firstResult2)	
			{
                            $sql = "SELECT
                                            mst_financecustomeractivate.intChartOfAccountId
                                            FROM mst_financecustomeractivate
                                            WHERE
                                            mst_financecustomeractivate.intCustomerId =  '$customer' AND
                                            mst_financecustomeractivate.intCompanyId =  '$companyId'";
                                $result = $db->RunQuery2($sql);
                                $row = mysqli_fetch_array($result);
                                $custAccount = $row['intChartOfAccountId'];	
                                
                                $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId,personType, personId) VALUES 
                                            ($entryIdG,'$cusStatus',$custAccount,$accAmount,'$memo',null,'cus',$customer)";
				$trnResult = $db->RunQuery2($sql);
			}
			//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			
			//>>>>>>>>>>>>>>>>>>>>>>transaction table process - account>>>>>>>>>>>>>>>>>>>>>>>>>	
			if($mainDetailResult2 && $firstResult2 && $trnResult)	
			{		 
                            $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                                    ($entryIdG,'$accStatus',$accId,$accAmount,'$memo',null)";
                            $trnAccResult = $db->RunQuery2($sql);
			}
		}
		//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>	
		
		if($mainDetailResult && $accDetailResult && $mainDetailResult2 && $firstResult2 && (($trnResult && $trnAccResult)||($gainSatus == "NON")))
		{
                    $db->commit();
                    $response['type'] 		= 'pass';
                    $response['msg'] 		= 'Saved successfully.';
                    $response['receiptNo'] 	= $receiptReference;
		}
		else{
                    $db->rollback();//roalback
                    $response['type'] 		= 'fail';
                    $response['msg'] 		= $db->errormsg;
                    $response['q'] 			= $sql;
		}
                echo json_encode($response);            
            }catch(Exception $e){
               
                $db->rollback();//roalback
                $response['type'] 		= 'fail';
                $response['msg'] 		= $e->getMessage();
                $response['q'] 			= $sql;                
               echo json_encode($response);                 
            }
	}
	////////////////////// received payments update part ////////////////////////
	else if($requestType=='edit')
	{
            try {
                $db->begin(); 
                $sql = "UPDATE `fin_customer_receivedpayments_header` SET 	intCustomerId		= $customer,
                        dtmDate				='$date',
                        intCurrencyId		= $currency,
                        dblRate				='$rate',
                        dblRecAmount		='$receivedAmount',
                        intPayMethodId		= $payMethod,
                        strPayRefNo			='$payRefNo',
                        dtmPayRefDate		='$payRefDate',
                        intIsPosted			= $isPosted,
                        strPayRefOrg		='$payRefOrg',
                        strRemark			='$remarks',
                        intModifyer			='$userId',
                        intDeleteStatus		='0'
                                        WHERE (`strReferenceNo`='$receipt')";
		$firstResult = $db->RunQuery2($sql);
		if(count($mainDetails) != 0 && $firstResult)
		{
			$sql = "SELECT
					fin_customer_receivedpayments_header.intReceiptNo,
					fin_customer_receivedpayments_header.intAccPeriodId,
					fin_customer_receivedpayments_header.strReferenceNo,
                    fin_customer_receivedpayments_header.entryId
					FROM
					fin_customer_receivedpayments_header
					WHERE
					fin_customer_receivedpayments_header.strReferenceNo =  '$receipt'
					";
			$result = $db->RunQuery2($sql);
			while($row=mysqli_fetch_array($result))
			{
				$receiptNumber 	= $row['intReceiptNo'];
				$accountPeriod 	= $row['intAccPeriodId'];
                $entryId		= $row['entryId'];
			}
                        //========update the transaction header====================
                        $sql="UPDATE fin_transactions SET 
                                entryDate='$date',                                                    
                                currencyId=$currency,
                                currencyRate='$rate',
                                payMethodId=$payMethod,
                                paymentNumber='$payRefNo',
                                transDetails='$remarks',                    
                                accPeriod=$accountPeriod
                            WHERE entryId=$entryId";
                        $db->RunQuery2($sql);

                    $sqld = "DELETE FROM `fin_transactions_details` WHERE entryId=$entryId";
                    $resultd = $db->RunQuery2($sqld);
                    //=========================================================
			
			$sql = "DELETE FROM `fin_customer_receivedpayments_main_details` WHERE (`strReferenceNo`='$receipt')";
			$db->RunQuery2($sql);	
                        
                        //delete Unajested Revers
                        deleteCustomerrevers($receipt);
			//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			foreach($mainDetails as $detail)
			{
				$jobNo 		= trim($detail['jobNo']);
				$docNo		= trim($detail['docNo']);
				$docDate	= $detail['docDate'];
				$amount 	= val($detail['amount']);
				$toBePaid	= val($detail['toBePaid']);
				$currencyCode	= trim($detail['currency']);
				$docRate		= val($detail['rate']);
				$payAmount 	= val($detail['payAmount']);
				$docType 	= trim($detail['docType']);
				$invAmount	+= (float)$payAmount*(float)$docRate;
				
				$docRefNo 	= trim($detail['docRefNo']);
				
				$sql = "INSERT INTO `fin_customer_receivedpayments_main_details` (`intReceiptNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strReferenceNo`,`strJobNo`,`strDocNo`,`dtmDate`,`dblAmount`,`dblToBePaid`,`strCurrency`,`dblRate`,`dblPayAmount`,`strDocType`,`intCreator`,dtmCreateDate,`strDocNoRef`) 
				VALUES ('$receiptNumber','$accountPeriod','$locationId','$companyId','$receipt','$jobNo','$docNo','$docDate','$amount','$toBePaid','$currencyCode','$docRate','$payAmount','$docType','$userId',now(),'$docRefNo')";
				
				$mainDetailResult = $db->RunQuery2($sql);
                                
                                //Revers unAjdested gen loss entries
                                reversCustomerEntry($customer,$detail,$date,$locationId,$userId,$receipt);
			}
		}
		if(count($mainDetails) != 0 && $firstResult && $mainDetailResult)
		{
			$sql = "SELECT
					fin_customer_receivedpayments_header.intReceiptNo,
					fin_customer_receivedpayments_header.intAccPeriodId,
					fin_customer_receivedpayments_header.strReferenceNo
					FROM
					fin_customer_receivedpayments_header
					WHERE
					fin_customer_receivedpayments_header.strReferenceNo =  '$receipt'
					";
			$result = $db->RunQuery2($sql);
			while($row=mysqli_fetch_array($result))
			{
				$receiptNumber 	= $row['intReceiptNo'];
				$accountPeriod 	= $row['intAccPeriodId'];
			}

			$sql = "DELETE FROM `fin_customer_receivedpayments_account_details` WHERE (`strReferenceNo`='$receipt')";
			$db->RunQuery2($sql);
			
			foreach($accDetails as $detail)
			{
				$accId 		= $detail['accId'];
				$accAmount	= $detail['accAmount'];
				$memo		= $detail['memo'];
				$dimension 	= val($detail['dimension']);
				
				$sql = "INSERT INTO `fin_customer_receivedpayments_account_details` (`intReceiptNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strReferenceNo`,`intChartOfAccountId`,`dblAmount`,`strMemo`,`intDimensionId`,`intCreator`,dtmCreateDate) 
				VALUES ('$receiptNumber','$accountPeriod','$locationId','$companyId','$receipt','$accId','$accAmount','$memo','$dimension','$userId',now())";
				
				$accDetailResult = $db->RunQuery2($sql);
				
			//>>>>>>>>>>>>>>>>>>>>>>transaction table process - account>>>>>>>>>>>>>>>>>>>>>>>>>
			if($accDetailResult)	
			{                        
			    $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
							($entryId,'D',$accId,$accAmount,'$memo',$dimension)";
				$trnResult = $db->RunQuery2($sql);
			}
			//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			}
		}
		//>>>>>>>>>>>>>>>>>>>>>>transaction table process - customer>>>>>>>>>>>>>>>>>>>>>>>>>
		if($mainDetailResult)	
		{
                    		$sql = "SELECT
                                    mst_financecustomeractivate.intChartOfAccountId
                                    FROM mst_financecustomeractivate
                                    WHERE
                                    mst_financecustomeractivate.intCustomerId =  '$customer' AND
                                    mst_financecustomeractivate.intCompanyId =  '$companyId'";
                                $result = $db->RunQuery2($sql);
                                $row = mysqli_fetch_array($result);
                                $custAccount = $row['intChartOfAccountId'];			 

                    		$sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId,personType, personId) VALUES 
                                    ($entryId,'C',$custAccount,$receivedAmount,'$remarks',null,'cus',$customer)";
                    $trnResult = $db->RunQuery2($sql);
		}
		//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		
		////////////////////// customer Gain-Loss update part ////////////////////////

		$sql = "SELECT
				mst_financecurrencyactivate.intCurrencyId,
				mst_financecurrencyactivate.intCompanyId,
				mst_financecurrencyactivate.intRealizeGainId,
				mst_financecurrencyactivate.intRealizeLostId,
				mst_financecurrencyactivate.intUnrealizeGainId,
				mst_financecurrencyactivate.intUnrealizeLostId,
				mst_financecurrency.intId
				FROM
				mst_financecurrencyactivate
				Inner Join mst_financecurrency ON mst_financecurrency.intId = mst_financecurrencyactivate.intCurrencyId
				WHERE
				mst_financecurrencyactivate.intCurrencyId =  '$currency' AND
				mst_financecurrencyactivate.intCompanyId =  '$companyId'
				";
		
		$result = $db->RunQuery2($sql);
		while($row=mysqli_fetch_array($result))
		{
			$reGain 	= $row['intRealizeGainId'];
			$reLost 	= $row['intRealizeLostId'];
			$unReGain 	= $row['intUnrealizeGainId'];
			$unReLost 	= $row['intUnrealizeLostId'];
		}
		$accAmount	= ((float)$receivedAmount*(float)$rate - (float)$invAmount);
		$dimension 	= 0;
		
		if($accAmount > 0)
		{
			$accStatus 	= 'C';
			$cusStatus 	= 'D';
			$memo		= "Realize Exchange Gain";
			$accId		= $reGain;
		}
		else if($accAmount < 0)
		{
			$accStatus 	= 'D';
			$cusStatus 	= 'C';
			$memo		= "Realize Exchange Loss";
			$accId		= $reLost;
                        $accAmount=$accAmount*(-1);
		}
		else if($accAmount == 0)
		{
			$accId		= 0;
			$gainSatus	= "NON";
			$memo		= "No Exchange Gain/Loss";
		}
		
		$sql = "SELECT
					fin_customer_gain_loss_details.strDocNo,
					fin_customer_gain_loss_header.strReferenceNo,
					fin_customer_gain_loss_header.intAccPeriodId,
					fin_customer_gain_loss_header.intGLNo,
                    fin_customer_gain_loss_header.entryId
					FROM
					fin_customer_gain_loss_header
					Inner Join fin_customer_gain_loss_details ON fin_customer_gain_loss_header.intGLNo = fin_customer_gain_loss_details.intGLNo AND fin_customer_gain_loss_header.intAccPeriodId = fin_customer_gain_loss_details.intAccPeriodId AND fin_customer_gain_loss_header.intLocationId = fin_customer_gain_loss_details.intLocationId AND fin_customer_gain_loss_header.intCompanyId = fin_customer_gain_loss_details.intCompanyId AND fin_customer_gain_loss_header.strReferenceNo = fin_customer_gain_loss_details.strReferenceNo
					WHERE
					fin_customer_gain_loss_details.strDocNo =  '$receipt'
					";
			$result = $db->RunQuery2($sql);
			while($row=mysqli_fetch_array($result))
			{
				$glNo		= $row['strReferenceNo'];
				$GLNumber 	= $row['intGLNo'];
				$accountPeriod 	= $row['intAccPeriodId'];
                                $entryIdG= $row['entryId'];
			}
			
			$sql = "UPDATE `fin_customer_gain_loss_header` SET intCustomerId = $customer,
                                    dtmDate				='$date',
                                    strRemark			='$memo',
                                    intChartOfAccountId	='$accId',
                                    dblAmount			='$accAmount',
                                    strMemo				='$memo',
                                    intDimensionId		= 0,
                                    intModifyer			='$userId',
                                    intDeleteStatus		='0'
			WHERE (`strReferenceNo`='$glNo')";
			$firstResult2 = $db->RunQuery2($sql);
                        
                        //========update the transaction Header====================
                        $sql="UPDATE fin_transactions SET 
                                    entryDate='$date',                                                        
                                    currencyId=$baseCurrencyId,
                                    currencyRate='1',
                                    transDetails='$remarks',                    
                                    accPeriod=$accountPeriod
                            WHERE entryId=$entryIdG";
                            $db->RunQuery2($sql);

                            $sqld = "DELETE FROM `fin_transactions_details` WHERE entryId=$entryIdG";
                            $resultd = $db->RunQuery2($sqld);
                    //=========================================================
			
		if($firstResult2)
		{
			$sql = "DELETE FROM `fin_customer_gain_loss_details` WHERE (`strReferenceNo`='$glNo')";
			$db->RunQuery2($sql);
			
			if($firstResult2)
			{
				$docNo 		= $receipt;
				$recAmount	= (float)$receivedAmount*(float)$rate;
								
				$sql = "INSERT INTO `fin_customer_gain_loss_details` (`intGLNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strReferenceNo`,`strDocNo`,`dblReceiveAmount`,`dblInvoiceAmount`,`intCreator`,dtmCreateDate) 
				VALUES ('$GLNumber','$accountPeriod','$locationId','$companyId','$glNo','$docNo','$recAmount','$invAmount','$userId',now())";
				
				$mainDetailResult2 = $db->RunQuery2($sql);			
			}
		}
		//>>>>>>>>>>>>>>>>>>>>>>transaction table process - customer>>>>>>>>>>>>>>>>>>>>>>>>>
		if($gainSatus != "NON")
		{
			if($mainDetailResult2 && $firstResult2)	
			{
				$sql = "SELECT
						mst_financecustomeractivate.intChartOfAccountId
						FROM mst_financecustomeractivate
						WHERE
						mst_financecustomeractivate.intCustomerId =  '$customer' AND
						mst_financecustomeractivate.intCompanyId =  '$companyId'";
					 $result = $db->RunQuery2($sql);
					 $row = mysqli_fetch_array($result);
					 $custAccount = $row['intChartOfAccountId'];					 
				
                                $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId,personType, personId) VALUES 
                                        ($entryIdG,'$cusStatus',$custAccount,$accAmount,'$memo',null,'cus',$customer)";
				$trnResult = $db->RunQuery2($sql);
			}
			//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			
			//>>>>>>>>>>>>>>>>>>>>>>transaction table process - account>>>>>>>>>>>>>>>>>>>>>>>>>	
			if($mainDetailResult2 && $firstResult2 && $trnResult)	
			{		 
				
                                $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                                        ($entryIdG,'$accStatus',$accId,$accAmount,'$memo',null)";
				$trnAccResult = $db->RunQuery2($sql);
			}
		}
		//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		
		if($mainDetailResult && $accDetailResult && $mainDetailResult2 && $firstResult2 && (($trnResult && $trnAccResult)||($gainSatus == "NON")))
		{
                    $db->commit();
                    $response['type'] 		= 'pass';
                    $response['msg'] 		= 'Updated successfully.';
                    $response['receiptNo'] 	= $receipt;
		}
		else
		{
                    $db->rollback(); //roalback
                    $response['type'] 		= 'fail';
                    $response['msg'] 		= $db->errormsg;
                    $response['q'] 		=$sql;
		}
                echo json_encode($response);
            } catch (Exception $e) {
                $db->rollback(); //roalback
                $response['type'] = 'fail';
                $response['msg'] = $e->getMessage();
                $response['q'] = $sql;
                echo json_encode($response);
            }
	}
	/////////// received payments delete part /////////////////////
	else if($requestType=='delete')
	{
            try{
                $db->begin();
                $sql = "SELECT
                        fin_customer_gain_loss_details.strDocNo,
                        fin_customer_gain_loss_header.strReferenceNo,
                        fin_customer_gain_loss_header.intAccPeriodId,
                        fin_customer_gain_loss_header.intGLNo,
                        fin_customer_gain_loss_header.entryId
                        FROM
                        fin_customer_gain_loss_header
                        Inner Join fin_customer_gain_loss_details ON fin_customer_gain_loss_header.intGLNo = fin_customer_gain_loss_details.intGLNo AND fin_customer_gain_loss_header.intAccPeriodId = fin_customer_gain_loss_details.intAccPeriodId AND fin_customer_gain_loss_header.intLocationId = fin_customer_gain_loss_details.intLocationId AND fin_customer_gain_loss_header.intCompanyId = fin_customer_gain_loss_details.intCompanyId AND fin_customer_gain_loss_header.strReferenceNo = fin_customer_gain_loss_details.strReferenceNo
                        WHERE
                        fin_customer_gain_loss_details.strDocNo =  '$id'
					";
                $result = $db->RunQuery2($sql);
                while($row=mysqli_fetch_array($result))
                {
                    $glNo		= $row['strReferenceNo'];
                    $entryIdG		= $row['entryId'];
                }
                //>>>>>>>>>>>>>>>>>>>>>>transaction table process>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                $sqld = "DELETE FROM `fin_transactions` WHERE entryId=$entryIdG";
                $resultd = $db->RunQuery2($sqld);
				
				$sqld = "DELETE FROM `fin_transactions_details` WHERE entryId=$entryIdG";
                $resultd = $db->RunQuery2($sqld);
		
                $sql = "DELETE FROM `fin_customer_gain_loss_details` WHERE (`strReferenceNo`='$glNo')";
                $db->RunQuery2($sql);

                $sql = "DELETE FROM `fin_customer_gain_loss_header` WHERE (`strReferenceNo`='$glNo')";
                $db->RunQuery2($sql);
		//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		$sql = "UPDATE `fin_customer_receivedpayments_header` SET intDeleteStatus ='1', intModifyer ='$userId'
				WHERE (`strReferenceNo`='$id')";
		$result = $db->RunQuery2($sql);
                //==========UPDATE TRANS ACTION delete STATUS
                $sql="SELECT fin_customer_receivedpayments_header.entryId,fin_customer_receivedpayments_header.intReceiptNo FROM fin_customer_receivedpayments_header WHERE (`strReferenceNo`='$id')";
                $result = $db->RunQuery2($sql);
                $row = mysqli_fetch_array($result);
                $entryId=$row['entryId'];                        
                $receiptNumber=$row['intReceiptNo'];  
                
                $sqld = "UPDATE `fin_transactions` SET delStatus=1 WHERE entryId=$entryId";
                $resultd = $db->RunQuery2($sqld);
                
                //delete Unajested Revers
                deleteCustomerrevers($id);
		if(($result && $resultd)){
                    $db->commit();
                    $response['type'] 		= 'pass';
                    $response['msg'] 		= 'Deleted successfully.';
		}
		else{
                    $db->rollback(); //roalback
                    $response['type'] 		= 'fail';
                    $response['msg'] 		= $db->errormsg;
                    $response['q'] 			=$sql;
		}
                echo json_encode($response);
            } catch (Exception $e) {
                $db->rollback(); //roalback
                $response['type'] = 'fail';
                $response['msg'] = $e->getMessage();
                $response['q'] = $sql;
                echo json_encode($response);
            }  
	}
	//echo json_encode($response);
//--------------------------------------------------------------------------------------------
	function getNextReceiptNo($companyId,$locationId)
	{
		global $db;
		$sql = "SELECT
				intRecPayReceiptNo
				FROM sys_finance_no
				WHERE
				intCompanyId = '$companyId' AND intLocationId = '$locationId'
				";	
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$nextReceiptNo = $row['intRecPayReceiptNo'];
		
		$sql = "UPDATE `sys_finance_no` SET intRecPayReceiptNo=intRecPayReceiptNo+1 WHERE (intCompanyId = '$companyId' AND intLocationId = '$locationId')";
		$db->RunQuery($sql);	
		return $nextReceiptNo;
	}
//--------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------
	function getNextGLNo($companyId,$locationId)
	{
		global $db;
		$sql = "SELECT
				intCustomerGLNo
				FROM sys_finance_no
				WHERE
				intCompanyId = '$companyId' AND intLocationId = '$locationId'
				";	
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$nextReceiptNo = $row['intCustomerGLNo'];
		
		$sql = "UPDATE `sys_finance_no` SET intCustomerGLNo=intCustomerGLNo+1 WHERE (intCompanyId = '$companyId' AND intLocationId = '$locationId')";
		$db->RunQuery($sql);	
		return $nextReceiptNo;
	}
//--------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------
	function getLatestAccPeriod($companyId)
	{
		global $db;
		$sql = "SELECT
				MAX(mst_financeaccountingperiod.intId) AS accId,
				mst_financeaccountingperiod.dtmStartingDate,
				mst_financeaccountingperiod.dtmClosingDate,
				mst_financeaccountingperiod.intStatus,
				mst_financeaccountingperiod_companies.intCompanyId,
				mst_financeaccountingperiod_companies.intPeriodId
				FROM
				mst_financeaccountingperiod
				Inner Join mst_financeaccountingperiod_companies ON mst_financeaccountingperiod_companies.intPeriodId = mst_financeaccountingperiod.intId
				WHERE
				mst_financeaccountingperiod_companies.intCompanyId =  '$companyId' AND mst_financeaccountingperiod.intStatus = '1'
				ORDER BY
				mst_financeaccountingperiod.intId DESC
				";	
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$latestAccPeriodId = $row['accId'];	
		return $latestAccPeriodId;
	}
//--------------------------------------------------------------------------------------------
//============================================================================================
	function encodeReceiptNo($receiptNumber,$accountPeriod,$companyId,$locationId,$date)
	{
		global $db;
		$sql = "SELECT
				mst_financeaccountingperiod.intId,
				mst_financeaccountingperiod.dtmStartingDate,
				mst_financeaccountingperiod.dtmClosingDate,
				mst_financeaccountingperiod.intStatus
				FROM
				mst_financeaccountingperiod
				WHERE
				mst_financeaccountingperiod.intId =  '$accountPeriod'
				";	
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		if($row['dtmStartingDate'] <= $date && $date <= $row['dtmClosingDate'])
		{
			$startDate = substr($row['dtmStartingDate'],0,4);
			$closeDate = substr($row['dtmClosingDate'],0,4);
		}
		else
		{
				$sql = "SELECT
					mst_financeaccountingperiod.intId,
					mst_financeaccountingperiod.dtmStartingDate,
					mst_financeaccountingperiod.dtmClosingDate,
					mst_financeaccountingperiod.intStatus
					FROM
					mst_financeaccountingperiod
					WHERE
					mst_financeaccountingperiod.dtmStartingDate <= '$date' AND
					mst_financeaccountingperiod.dtmClosingDate >=  '$date'
					";	
				$result = $db->RunQuery($sql);
				$row = mysqli_fetch_array($result);
				$startDate = substr($row['dtmStartingDate'],0,4);
				$closeDate = substr($row['dtmClosingDate'],0,4);
		}
		$sql = "SELECT
				mst_companies.strCode AS company,
				mst_companies.intId,
				mst_locations.intCompanyId,
				mst_locations.strCode AS location,
				mst_locations.intId
				FROM
				mst_companies
				Inner Join mst_locations ON mst_locations.intCompanyId = mst_companies.intId
				WHERE
				mst_locations.intId =  '$locationId' AND
				mst_companies.intId =  '$companyId'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$companyCode = $row['company'];
		$locationCode = $row['location'];
		$receiptFormat = $companyCode."/".$locationCode."/".$startDate."-".$closeDate."/".$receiptNumber;
		//$receiptFormat = $companyCode."/".$startDate."-".$closeDate."/".$receiptNumber;
		return $receiptFormat;
	}
//============================================================================================
//============================================================================================
	function encodeGLNo($glNo,$accountPeriod,$companyId,$locationId,$date)
	{
		global $db;
		$sql = "SELECT
				mst_financeaccountingperiod.intId,
				mst_financeaccountingperiod.dtmStartingDate,
				mst_financeaccountingperiod.dtmClosingDate,
				mst_financeaccountingperiod.intStatus
				FROM
				mst_financeaccountingperiod
				WHERE
				mst_financeaccountingperiod.intId =  '$accountPeriod'
				";	
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		if($row['dtmStartingDate'] <= $date && $date <= $row['dtmClosingDate'])
		{
			$startDate = substr($row['dtmStartingDate'],0,4);
			$closeDate = substr($row['dtmClosingDate'],0,4);
		}
		else
		{
				$sql = "SELECT
					mst_financeaccountingperiod.intId,
					mst_financeaccountingperiod.dtmStartingDate,
					mst_financeaccountingperiod.dtmClosingDate,
					mst_financeaccountingperiod.intStatus
					FROM
					mst_financeaccountingperiod
					WHERE
					mst_financeaccountingperiod.dtmStartingDate <= '$date' AND
					mst_financeaccountingperiod.dtmClosingDate >=  '$date'
					";	
				$result = $db->RunQuery($sql);
				$row = mysqli_fetch_array($result);
				$startDate = substr($row['dtmStartingDate'],0,4);
				$closeDate = substr($row['dtmClosingDate'],0,4);
		}
		$sql = "SELECT
				mst_companies.strCode AS company,
				mst_companies.intId,
				mst_locations.intCompanyId,
				mst_locations.strCode AS location,
				mst_locations.intId
				FROM
				mst_companies
				Inner Join mst_locations ON mst_locations.intCompanyId = mst_companies.intId
				WHERE
				mst_locations.intId =  '$locationId' AND
				mst_companies.intId =  '$companyId'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$companyCode = $row['company'];
		$locationCode = $row['location'];
		$glFormat = $companyCode."/".$locationCode."/".$startDate."-".$closeDate."/".$glNo;
		//$glFormat = $companyCode."/".$startDate."-".$closeDate."/".$glNo;
		return $glFormat;
	}
//============================================================================================
?>
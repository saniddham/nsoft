<?php
function reversCustomerEntry($customerId,$detail,$theDate,$locationId,$userId,$receiptNumber){
    global $db;
    //get the unadjested entry List
    $docNo  = trim($detail['jobNo']);
    $docType = trim($detail['docType']);    
    
    $sql1="SELECT
            uc.entryId,uc.calAmount
            FROM
            fin_unadjusted_gl AS uc
            WHERE
            uc.`status` =  '1' AND
            uc.personType =  'cus' AND
            uc.personId =  '$customerId' AND
            uc.invoiceNumber =  '$docNo' AND
            uc.invoiceType =  '$docType' AND uc.entDate<='$theDate'";
    
    $resultEnts = $db->RunQuery2($sql1);
    while($rowEnt=mysqli_fetch_array($resultEnts)){
        $entryId=$rowEnt['entryId'];
        
        $payPrecentage=($detail['payAmount']/$rowEnt['calAmount'])*100;
        $newEntryId=reversEntry($entryId,$theDate,$locationId,$userId,$payPrecentage);
        $sql2="INSERT INTO fin_customer_receivedpayments_reversacc (intReceiptNo,entryId) VALUES('$receiptNumber',$newEntryId)";
        $db->RunQuery2($sql2);
    }
}

function deleteCustomerrevers($receiptNumber){
     global $db;
     $sql8="UPDATE 
                fin_transactions  T,fin_customer_receivedpayments_reversacc AS CRR 
            SET 
                T.delStatus=1 
            WHERE
                CRR.entryId = T.entryId AND
                CRR.intReceiptNo =  '$receiptNumber'";
    $db->RunQuery2($sql8);
    
    $sql9="DELETE FROM fin_customer_receivedpayments_reversacc WHERE intReceiptNo =  '$receiptNumber'";
    $db->RunQuery2($sql9);
}
//----------------------------Other Customer Reverse Entry--------------------------
function reversOtherCustomerEntry($customerId,$detail,$theDate,$locationId,$userId,$receiptNumber){
    global $db;
    //get the unadjested entry List
    $docNo  = trim($detail['jobNo']);
    $docType = trim($detail['docType']);    
    
    $sql1="SELECT
            uc.entryId,uc.calAmount
            FROM
            fin_unadjusted_gl AS uc
            WHERE
            uc.`status` =  '1' AND
            uc.personType =  'ocus' AND
            uc.personId =  '$customerId' AND
            uc.invoiceNumber =  '$docNo' AND
            uc.invoiceType =  '$docType' AND uc.entDate<='$theDate'";
    
    $resultEnts = $db->RunQuery2($sql1);
    while($rowEnt=mysqli_fetch_array($resultEnts)){
        $entryId=$rowEnt['entryId'];
        
        $payPrecentage=($detail['payAmount']/$rowEnt['calAmount'])*100;
        $newEntryId=reversEntry($entryId,$theDate,$locationId,$userId,$payPrecentage);
        $sql2="INSERT INTO fin_other_receivable_payments_reversacc (intReceiptNo,entryId) VALUES('$receiptNumber',$newEntryId)";
        $db->RunQuery2($sql2);
    }
}
function deleteOtherCustomerrevers($receiptNumber){
     global $db;
     $sql8="UPDATE 
                fin_transactions  T,fin_other_receivable_payments_reversacc AS CRR 
            SET 
                T.delStatus=1 
            WHERE
                CRR.entryId = T.entryId AND
                CRR.intReceiptNo =  '$receiptNumber'";
    $db->RunQuery2($sql8);
    
    $sql9="DELETE FROM fin_other_receivable_payments_reversacc WHERE intReceiptNo =  '$receiptNumber'";
    $db->RunQuery2($sql9);
}
//----------------------------------------------------------------------------------

//--------------------------------------Supplier------------------------------------
function reversSupplierEntry($supplierId,$detail,$theDate,$locationId,$userId,$receiptNumber){
    global $db;
    //get the unadjested entry List
    $docNo  = trim($detail['docNo']);
    $docType = trim($detail['docType']);   
    
    $sql1="SELECT
            uc.entryId, uc.calAmount
            FROM
            fin_unadjusted_gl AS uc
            WHERE
            uc.`status` =  '1' AND
            uc.personType =  'sup' AND
            uc.personId =  '$supplierId' AND
            uc.invoiceNumber =  '$docNo' AND
            uc.invoiceType =  '$docType' AND uc.entDate<='$theDate'";
    
    $resultEnts = $db->RunQuery2($sql1);
    while($rowEnt=mysqli_fetch_array($resultEnts)){
        $entryId=$rowEnt['entryId'];
        
        $payPrecentage=($detail['payAmount']/$rowEnt['calAmount'])*100;
        $newEntryId=reversEntry($entryId,$theDate,$locationId,$userId,$payPrecentage);
        $sql2="INSERT INTO fin_supplier_payments_reversacc (intReceiptNo,entryId) VALUES('$receiptNumber',$newEntryId)";
        $db->RunQuery2($sql2);
    }
}
function deleteSupplierrevers($receiptNumber){
     global $db;
     $sql8="UPDATE 
                fin_transactions  T,fin_supplier_payments_reversacc AS CRR 
            SET 
                T.delStatus=1 
            WHERE
                CRR.entryId = T.entryId AND
                CRR.intReceiptNo =  '$receiptNumber'";
    $db->RunQuery2($sql8);
    
    $sql9="DELETE FROM fin_supplier_payments_reversacc WHERE intReceiptNo =  '$receiptNumber'";
    $db->RunQuery2($sql9);
}

//----------------------------Other Supplier Reverse Entry-------------------------
function reversOtherSupplierEntry($supplierId,$detail,$theDate,$locationId,$userId,$receiptNumber){
    global $db;
    //get the unadjested entry List
    $docNo  = trim($detail['docNo']);
    $docType = trim($detail['docType']);   
    
    $sql1="SELECT
            uc.entryId, uc.calAmount
            FROM
            fin_unadjusted_gl AS uc
            WHERE
            uc.`status` =  '1' AND
            uc.personType =  'osup' AND
            uc.personId =  '$supplierId' AND
            uc.invoiceNumber =  '$docNo' AND
            uc.invoiceType =  '$docType' AND uc.entDate<='$theDate'";
    
    $resultEnts = $db->RunQuery2($sql1);
    while($rowEnt=mysqli_fetch_array($resultEnts)){
        $entryId=$rowEnt['entryId'];
        
        $payPrecentage=($detail['payAmount']/$rowEnt['calAmount'])*100;
        $newEntryId=reversEntry($entryId,$theDate,$locationId,$userId,$payPrecentage);
        $sql2="INSERT INTO fin_other_payable_payments_reversacc (intReceiptNo,entryId) VALUES('$receiptNumber',$newEntryId)";
        $db->RunQuery2($sql2);
    }
}
function deleteOtherSupplierrevers($receiptNumber){
     global $db;
     $sql8="UPDATE 
                fin_transactions  T,fin_other_payable_payments_reversacc AS CRR 
            SET 
                T.delStatus=1 
            WHERE
                CRR.entryId = T.entryId AND
                CRR.intReceiptNo =  '$receiptNumber'";
    $db->RunQuery2($sql8);
    
    $sql9="DELETE FROM fin_other_payable_payments_reversacc WHERE intReceiptNo =  '$receiptNumber'";
    $db->RunQuery2($sql9);
}
//============================================================================================================================
//----Comman ------------------
function reversEntry($entryId,$theDate,$locationId,$userId,$payPrecentage){
    global $db;
    $sql3="SELECT TH.* FROM fin_transactions AS TH WHERE TH.entryId =  '$entryId' ";
    $resultHed= $db->RunQuery2($sql3);
    $rowHed=mysqli_fetch_array($resultHed);
    
    $companyId=$rowHed['companyId'];
    $currency=$rowHed['currencyId'];
    $rate=$rowHed['currencyRate'];
    $remarks='Unrealize Gain-Loss - Revers';
    $accountPeriod=$rowHed['accPeriod'];   
    
    $uglNumber=getUnadjustedGL($companyId,$locationId);
    $uglReference= trim(encodeUnadjustedGL($uglNumber,$accountPeriod,$companyId,$locationId));
    //Add data to transaction header*******************************************
    $sql4="INSERT INTO fin_transactions (entryDate, strProgramType, documentNo, currencyId, currencyRate, transDetails, accPeriod, userId, companyId, createdOn) VALUES
        ('$theDate','Unrealize Gain-Loss','$uglReference',$currency,$rate,'$remarks',$accountPeriod,$userId,$companyId,now())";                
    $db->RunQuery2($sql4);
    $newEntryId=$db->insertId;                
    //********************************************************************************
    
    $sql5="SELECT TD.* FROM fin_transactions_details AS TD WHERE TD.entryId =  '$entryId'";
    $resultDel= $db->RunQuery2($sql5);
    while($rowDel=mysqli_fetch_array($resultDel)){
        //revers debit entry
        if($rowDel['credit/debit']=='D'){
            $accId=$rowDel['accountId'];
            $gainLoss=($rowDel['amount']*($payPrecentage/100)); //pay prentage=(pay amount/to be paid)*100
            $PersonId=($rowDel['personId']==null?'null':$rowDel['personId']);
            $personType = ($rowDel['personType']==null?'null':"'".$rowDel['personType']."'");
            $sql6="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId,personType, personId) VALUES 
                    ($newEntryId,'C',$accId,$gainLoss,'$remarks',null,$personType,$PersonId)";
            $db->RunQuery2($sql6);
        }
        if($rowDel['credit/debit']=='C'){
            $accId=$rowDel['accountId'];
            $gainLoss=($rowDel['amount']*($payPrecentage/100)); 
            $PersonId=($rowDel['personId']==null?'null':$rowDel['personId']);
            $personType = ($rowDel['personType']==null?'null':"'".$rowDel['personType']."'");
            $sql6="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId,personType, personId) VALUES 
                    ($newEntryId,'D',$accId,$gainLoss,'$remarks',null,$personType,$PersonId)";
            $db->RunQuery2($sql6);            
        }
    }
    return $newEntryId;
}



//--------------------------------------------------------------------------------------------
function getUnadjustedGL($companyId,$locationId)
{
    global $db;
    $sql = "SELECT intUnadjustedGL FROM sys_finance_no WHERE intCompanyId = '$companyId' AND intLocationId = '$locationId'";	
    $result = $db->RunQuery2($sql);    
    $row = mysqli_fetch_array($result);
    $nextReceiptNo = $row['intUnadjustedGL'];
    if(mysqli_num_rows($result)<=0 || $nextReceiptNo==null || $nextReceiptNo==0){
        $nextReceiptNo=1000001;
    }
    $sql = "UPDATE `sys_finance_no` SET intUnadjustedGL=($nextReceiptNo+1) WHERE (intCompanyId = '$companyId' AND intLocationId = '$locationId')";
    $db->RunQuery2($sql);	
    return $nextReceiptNo;
}
//--------------------------------------------------------------------------------------------
//=========================================================================================
function encodeUnadjustedGL($receiptNumber,$accountPeriod,$companyId,$locationId)
{
        global $db;
        $sql = "SELECT
                        mst_financeaccountingperiod.intId,
                        mst_financeaccountingperiod.dtmStartingDate,
                        mst_financeaccountingperiod.dtmClosingDate,
                        mst_financeaccountingperiod.intStatus
                        FROM
                        mst_financeaccountingperiod
                        WHERE
                        mst_financeaccountingperiod.intId =  '$accountPeriod'
                        ";	
        $result = $db->RunQuery2($sql);
        $row = mysqli_fetch_array($result);
        $startDate = substr($row['dtmStartingDate'],0,4);
        $closeDate = substr($row['dtmClosingDate'],0,4);
        $sql = "SELECT
                        mst_companies.strCode AS company,
                        mst_companies.intId,
                        mst_locations.intCompanyId,
                        mst_locations.strCode AS location,
                        mst_locations.intId
                        FROM
                        mst_companies
                        Inner Join mst_locations ON mst_locations.intCompanyId = mst_companies.intId
                        WHERE
                        mst_locations.intId =  '$locationId' AND
                        mst_companies.intId =  '$companyId'
                        ";
        $result = $db->RunQuery2($sql);
        $row = mysqli_fetch_array($result);
        $companyCode = $row['company'];
        $locationCode = $row['location'];
        $receiptFormat = $companyCode."/".$locationCode."/".$startDate."-".$closeDate."/".$receiptNumber;
        //$receiptFormat = $companyCode."/".$startDate."-".$closeDate."/".$receiptNumber;
        return $receiptFormat;
}
//============================================================================================
?>

<?php
session_start();
$backwardseperator = "../../../../";
$companyId = $_SESSION['CompanyID'];
$locationId = $_SESSION['CompanyID'];
$intUser  = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
include  	"{$backwardseperator}dataAccess/Connector.php";

include_once "../../commanFunctions/Converter.php";

$companyId 	= $_SESSION['headCompanyId'];

$receiptRefNo = $_REQUEST['id'];

$sql = "SELECT
		fin_customer_receivedpayments_header.intReceiptNo,
		fin_customer_receivedpayments_header.intCompanyId,
		fin_customer_receivedpayments_header.strReferenceNo,
		fin_customer_receivedpayments_header.intCustomerId,
		fin_customer_receivedpayments_header.dtmDate,
		fin_customer_receivedpayments_header.intCurrencyId,
		round(fin_customer_receivedpayments_header.dblRecAmount,2) AS amount,
		fin_customer_receivedpayments_header.intPayMethodId,
		fin_customer_receivedpayments_header.strPayRefNo,
		fin_customer_receivedpayments_header.dtmPayRefDate,
		fin_customer_receivedpayments_header.strPayRefOrg,
		mst_customer.strName AS customer,
		mst_financecurrency.strCode AS currency,
		mst_financepaymentsmethods.intId,
		mst_financepaymentsmethods.strName AS payMethod,
		mst_customer.strAddress,
		fin_customer_receivedpayments_header.dblRate,
		fin_customer_receivedpayments_header.intCreator,
		user1.intUserId,
		user1.strUserName AS creater,
		user2.strUserName AS modifyer,
		fin_customer_receivedpayments_header.strRemark
		FROM
		fin_customer_receivedpayments_header
		Left Outer Join mst_customer ON fin_customer_receivedpayments_header.intCustomerId = mst_customer.intId
		Left Outer Join mst_financecurrency ON fin_customer_receivedpayments_header.intCurrencyId = mst_financecurrency.intId
		Left Outer Join mst_financepaymentsmethods ON fin_customer_receivedpayments_header.intPayMethodId = mst_financepaymentsmethods.intId
		Left Outer Join sys_users AS user1 ON fin_customer_receivedpayments_header.intCreator = user1.intUserId
		Left Outer Join sys_users AS user2 ON fin_customer_receivedpayments_header.intModifyer = user2.intUserId
		WHERE
		fin_customer_receivedpayments_header.intCompanyId =  '$companyId' AND
		fin_customer_receivedpayments_header.strReferenceNo =  '$receiptRefNo'
		";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$customer 	= $row['customer'];
	$payDate 	= $row['dtmDate'];
	$address 	= $row['strAddress'];
	$currency 	= $row['currency'];
	$amount		= $row['amount'];
	$payMethod	= $row['payMethod'];
	$payRefNo	= $row['strPayRefNo'];
	$payRefDate	= $row['dtmPayRefDate'];
	$payOrg		= $row['strPayRefOrg'];
	$rate		= $row['dblRate'];
	$creater	= $row['creater'];
	$modifyer	= $row['modifyer'];
	$memo		= $row['strRemark'];
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Customer Received Payments - Receipt</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:239px;
	top:172px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>

<form id="frmReceivedPaymentsDetails" name="frmReceivedPaymentsDetails" method="post" action="salesInvoiceDetails.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include '../../../../reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><h3><strong>Receipt</strong></h3></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
  <tr>
    <td colspan="9" align="center" bgcolor="#FFDFCB">
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td width="17%" rowspan="2"><span class="normalfnt"><strong>Received From</strong></span></td>
    <td width="2%" rowspan="2" align="center" valign="middle"><strong>:</strong></td>
    <td width="34%" rowspan="2"><span class="normalfnt"><?php echo $customer ?></span><span class="normalfnt"><br /><?php echo $address ?></span></td>
    <td class="normalfnt"><strong>Receipt No.</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $receiptRefNo ?></span></td>
    <td width="2%" colspan="2" rowspan="11"><div id="divPoNo" style="display:none"><?php echo $poNo ?>/<?php echo $year ?></div></td>
    </tr>
  <tr>
    <td width="2%">&nbsp;</td>
    <td width="19%" class="normalfnt"><strong>Receipt Date</strong></td>
    <td width="2%" align="center" valign="middle"><strong>:</strong></td>
    <td width="22%"><span class="normalfnt"><?php echo $payDate  ?></span></td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Received</strong> <strong>Amount</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td class="normalfnt"><?php echo number_format($amount,2)  ?></td>
    <td><span class="normalfnt"><strong>Currency</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $currency  ?></span></td>
    </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><span class="normalfntGrey">(Amount in Word)</span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td colspan="4" class="normalfnt">
    <?php
		$val = $amount;
        $inWord= convert_number($val);   
        $sence=explode(".",number_format($val,2));
        $sent=convert_number($sence[1]);
        if(strlen($sence[1])>0)
		{
        	$inWord=$inWord." and ".$sent." cents";
        }
       echo $inWord;
    ?>
    </td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="6" class="normalfntGrey"><?php
		echo "Exchange Rate: <strong>Rs. ".number_format($rate,4).""."/".$symbol." '".number_format($amount*$rate, 2)."'</strong>";
	?>
      <br />
    (<?php
            $val = $amount*$rate;
            $inWord= convert_number($val);   
            $sence=explode(".",number_format($val,2));
            $sent=convert_number($sence[1]);
            if(strlen($sence[1])>0)
            {
                $inWord=$inWord." and ".$sent." cents";
            }
           echo $inWord;
        ?>)
    </td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfntGrey"><span class="normalfnt"><strong>Document No.</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td colspan="4" class="normalfntBlue">
    <?php
	$sql1 = "SELECT
			fin_customer_receivedpayments_main_details.strJobNo,
			fin_customer_receivedpayments_main_details.strDocType,
			round(fin_customer_receivedpayments_main_details.dblPayAmount,2) AS payAmount,
			IFNULL(CONCAT(' - ref ',fin_customer_receivedpayments_main_details.strDocNoRef),'') AS refNo
			FROM
			fin_customer_receivedpayments_header
			Inner Join fin_customer_receivedpayments_main_details ON fin_customer_receivedpayments_header.intReceiptNo = fin_customer_receivedpayments_main_details.intReceiptNo AND fin_customer_receivedpayments_header.intAccPeriodId = fin_customer_receivedpayments_main_details.intAccPeriodId AND fin_customer_receivedpayments_header.intLocationId = fin_customer_receivedpayments_main_details.intLocationId AND fin_customer_receivedpayments_header.intCompanyId = fin_customer_receivedpayments_main_details.intCompanyId AND fin_customer_receivedpayments_header.strReferenceNo = fin_customer_receivedpayments_main_details.strReferenceNo
			WHERE
			fin_customer_receivedpayments_main_details.strReferenceNo =  '$receiptRefNo' AND
			fin_customer_receivedpayments_main_details.intCompanyId =  '$companyId'
			";
			$result1 = $db->RunQuery($sql1);
			while($row=mysqli_fetch_array($result1))
			{
				$docNo .=$row['strDocType']." : [".$row['strJobNo'].$row['refNo']. "]->"."Paying Amount"."= ".number_format($row['payAmount'],2). "; ";
			}
			echo $docNo;
	?>
    </td>
    </tr>
    <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Memo</strong><strong></strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td colspan="4" class="normalfnt"><?php echo $memo  ?></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="6" class="normalfntGrey"><br /></td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="6" class="normalfntGrey">More Details</td>
    </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td colspan="6" class="normalfnt">
      <table width="100%" class="tableBorder_allRound">
        <tr>
          <td width="18%" class="normalfnt"><strong>Payment Method</strong></td>
          <td width="1%" align="center" valign="middle"><strong>:</strong></td>
          <td width="36%"><span class="normalfnt"><?php echo $payMethod  ?></span></td>
          <td width="20%"><span class="normalfnt"><strong>Reference No.</strong></span></td>
          <td width="2%" align="center" valign="middle"><strong>:</strong></td>
          <td width="23%"><span class="normalfnt"><?php echo $payRefNo ?></span></td>
          </tr>  
        <tr>
          <td class="normalfnt"><strong>Reference Date</strong></td>
          <td align="center" valign="middle"><strong>:</strong></td>
          <td><span class="normalfnt"><?php echo $payRefDate ?></span></td>
          <td><span class="normalfnt"><strong>Reference Organization</strong></span></td>
          <td align="center" valign="middle"><strong>:</strong></td>
          <td><span class="normalfnt"><?php echo $payOrg ?></span></td>
          </tr> 
        </table>
    </td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    </tr>
  <tr>
    <td height="21">&nbsp;</td>
    <td colspan="6" class="normalfnt"><table width="100%" class="tableBorder_allRound">
      <tr>
        <td width="100%" style="vertical-align:top" >
        <table width="100%" id="tblMainGrid" cellspacing="0" cellpadding="0">
          <tr>
            <td width="274"  height="22" class="normalfnt borderLine"><strong>Account</strong></td>
            <td width="353" class="normalfnt borderLine"><strong>Memo</strong></td>
            <td width="136" class="normalfnt borderLine"><strong>Cost Center</strong></td>
            <td width="78" class="normalfntRight borderLine"><strong>Amount</strong></td>
          </tr>
          <?php 
			$totalTax = 0;
	  	  	$sql1 = "SELECT
					CONCAT(mst_financechartofaccounts.strCode,'-' ,mst_financechartofaccounts.strName) AS acc,
					mst_financedimension.strName,
					fin_customer_receivedpayments_account_details.strMemo,
					fin_customer_receivedpayments_account_details.dblAmount
					FROM
					fin_customer_receivedpayments_account_details
					Inner Join mst_financechartofaccounts ON fin_customer_receivedpayments_account_details.intChartOfAccountId = mst_financechartofaccounts.intId
					Inner Join mst_financedimension ON fin_customer_receivedpayments_account_details.intDimensionId = mst_financedimension.intId
					WHERE
					fin_customer_receivedpayments_account_details.strReferenceNo =  '$receiptRefNo'";
			$result1 = $db->RunQuery($sql1);
		while($row=mysqli_fetch_array($result1))
		{
			$subAmount = $row['dblAmount'];
	  	?>
          <tr class="normalfnt borderLineIn"  bgcolor="#FFFFFF">
            <td class="normalfnt borderLineIn" height="22"><?php echo $row['acc']; ?>&nbsp;</td>
            <td class="normalfnt borderLineIn" ><?php echo $row['strMemo']; ?>&nbsp;</td>
            <td class="normalfnt borderLineIn" ><?php echo $row['strName']; ?>&nbsp;</td>
            <td class="normalfntRight borderLineIn" ><?php echo number_format($subAmount, 2) ?></td>
          </tr>
          <?php 
        }
      ?>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td height="21">&nbsp;</td>
    <td colspan="6" class="normalfnt"><strong><span class="compulsoryRed">*</span> This Receipt Valid Subject to Realization of the Cheque</strong></td>
    </tr>
  </table>
  </td>
</tr>
<tr>
  <td class="normalfntGrey" align="center"><br /><br /></td>
</tr>
<tr>
<td>
<table width="100%">
<tr>
    <td align="left" class="normalfnt">&nbsp;</td>
    <td align="center" class="normalfntMid">
    <?php
	if($modifyer == NULL)
	{
		echo $creater;
	}
	else
	{
		echo $modifyer;
	}
	?>
    </td>
    <td width="30%" align="center" class="normalfntMid">&nbsp;</td>
    <td width="34%" align="right" class="normalfntRight">&nbsp;</td>
</tr>
<tr>
<td align="left" class="normalfnt">&nbsp;</td>
<td align="center" class="normalfntMid">........................................</td>
<td align="center" class="normalfntMid">....................................................</td>
<td align="right" class="normalfntMid">............................</td>
</tr>
<tr>
<td align="left" class="normalfnt">&nbsp;</td>
<td align="center" class="normalfntMid">Prepared By</td>
<td align="center" class="normalfntMid">Authorized by Accountant</td>
<td align="right" class="normalfntMid">Authorized By</td>
</tr>
</table>
</td>
</tr>
<tr>
  <td class="normalfntGrey" align="center"><br /><br /></td>
</tr>
<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
</body>
</html>
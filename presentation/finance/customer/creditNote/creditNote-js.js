// JavaScript Document
var rows = 1;
var invoType = "";
var trnTax = "";
var amStatus = "Auto";
function functionList()
{
	if(invoRefNo!='')
	{
		$('#frmCreditNote #cboSearch').val(invoRefNo);
		$('#frmCreditNote #cboSearch').change();
	}
}
function insertRow()
{
	var tbl = document.getElementById('tblMainGrid2');	
	rows = tbl.rows.length;
	//tbl.rows[1].cells[5].childNodes[0].nodeValue;
	tbl.insertRow(rows);
	tbl.rows[rows].className='mainRow';

	tbl.rows[rows].innerHTML = tbl.rows[rows-1].innerHTML;
	//loadJs();
}
$(document).ready(function() {
	
	// TODO: ===========Add by dulaskshi 2013.03.20===========
	getSupplierList(); 
	
	var id = '';
	$("#frmCreditNote").validationEngine();
	$('#frmCreditNote #cboCustomer').focus();

$('.delImg').live('click',function(){
	var rowId = $(this).parent().parent().parent().find('.mainRow').length;
	if(rowId!=1)
	$(this).parent().parent().remove();
	callSubTotal();
	callTaxTotal();
});

$('.delImg').css('cursor', 'pointer');

  //permision for add 
  if(intAddx)
  {
 	$('#frmCreditNote #butNew').show();
	$('#frmCreditNote #butSave').show();
	$('#frmCreditNote #butPrint').show();
  }
  //permision for edit 
  if(intEditx)
  {
  	$('#frmCreditNote #butSave').show();
	$('#frmCreditNote #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
	$('#frmCreditNote #butPrint').show();
  }
  //permision for delete
  if(intDeletex)
  {
  	$('#frmCreditNote #butDelete').show();
	$('#frmCreditNote #cboSearch').removeAttr('disabled');
  }
  //permision for view
  if(intViewx)
  {
	$('#frmCreditNote #cboSearch').removeAttr('disabled');
  }
  
  $('#frmCreditNote #chkEdit').click(function(){
	  if($('#frmCreditNote #chkEdit').attr('checked'))
	  {
		  $("#frmCreditNote #txtRate").attr("readonly","");
		  $('#frmCreditNote #txtRate').focus();
	  }
	  else
	  {
		  $('#frmCreditNote #txtRate').val('');
		  $("#frmCreditNote #txtRate").attr("readonly","readonly");
		  $('#frmCreditNote #cboCurrency').change();
	  }
  });
 //===================================================================
 	$('#frmCreditNote #chkAutoManual').click(function(){
	  if($('#frmCreditNote #chkAutoManual').attr('checked'))
	  {
		  amStatus = "Auto";
		  $('#frmCreditNote #amStatus').val('Auto');
		  $('#frmCreditNote #txtNo').val('');
		  $("#frmCreditNote #txtNo").attr("readonly","readonly");
		  $('#frmCreditNote #txtNo').removeClass('validate[required]');
	  }
	  else
	  {
		  amStatus = "Manual";
		  $('#frmCreditNote #amStatus').val('Manual');
		  $('#frmCreditNote #txtNo').val('');
		  $("#frmCreditNote #txtNo").attr("readonly","");
		  $('#frmCreditNote #txtNo').focus();
		  $('#frmCreditNote #txtNo').addClass('validate[required]');
	  }
  });
 //===================================================================
///////////////////////////////////////////////////////////////////
$("input[name^=txtQty]").live("keyup ", calculate);
$("input[name^=txtUnitPrice]").live("keyup ", calculate);
$("input[name^=txtDiscount]").live("keyup ", calculate);
///////////////////////////////////////////////////////////////////

///////////////////////////get customer address, invoice type, currency and payment terms////////////////////
$('#cboCustomer').change(function(){
	var url = "creditNote-db-get.php?requestType=getCustomerAddress&customerId="+$(this).val();
	var httpobj = $.ajax({
	url:url,
	dataType:'json',
	async:false,
	success:function(json)
	{
		$('.invTpe').attr('checked',false); //
		$('#txtAddress').val(json.address);
		$('#frmCreditNote #'+json.invoType).attr('checked',true); //
		invoType = json.invoType;
		$('#frmCreditNote #cboCurrency').val(json.currency);
		$('#frmCreditNote #cboCurrency').change();
		$('#frmCreditNote #cboPaymentsTerms').val(json.payTerms);
		$('#frmCreditNote #cboInvoiceNo').html(json.salesInvoiceList);
	}
	});
});
////////////////////////////////////////////////////////////////////

///////////////////////////get item description////////////////////
$('.item').live('change',function(){
	var url = "creditNote-db-get.php?requestType=getItemDescription&itemId="+$(this).val();
	var obj = $.ajax({url:url,async:false});
	$(this).parent().parent().find('#tblSubGrid .itemDesc').val(obj.responseText);
});
////////////////////////////////////////////////////////////////////

///////////////////////////get tax values//////////////////////////
	$('.taxGroup').live('change',function(){
	var operation = '';
	var amount = $(this).parent().parent().find('.amount').val();
	var jsonTaxCode="[ ";
	//var taxCode = $(this).find('option:selected').text();
	var taxId = callTaxProcess($(this).val());
	var arrTax = taxId.split('/');
	//var arrTaxInclusive = taxId.split('/');
	//var arrTaxExclusive = taxId.split('-');

	if(arrTax.length == 1)
	{
		operation = 'Isolated';
		jsonTaxCode += '{ "taxId":"'+taxId+'"},';
	}
	else if(arrTax.length > 1)
	{
		operation = arrTax[1];
		for(var i =0; i < arrTax.length; i=i+2)
		{
			jsonTaxCode += '{ "taxId":"'+arrTax[i]+'"},';
		}
	}
	jsonTaxCode = jsonTaxCode.substr(0,jsonTaxCode.length-1);
	jsonTaxCode += " ]";
	var url = "creditNote-db-get.php?requestType=getTaxValue&arrTaxCode="+jsonTaxCode+"&opType="+operation+"&valAmount="+amount;
	var obj = $.ajax({url:url,async:false});
	var values = obj.responseText.split('/');
	$(this).parent().parent().find('.taxWith').val(values[0]);
	
	if(arrTax.length == 1)
	{
		trnTax="[ ";
		trnTax += '{ "taxId":"'+taxId+'", "taxValue": "'+values[1]+'"},';
		trnTax = trnTax.substr(0,trnTax.length-1);
		trnTax += " ]";
	}
	else if(arrTax.length > 1)
	{
		trnTax="[ ";
		var k = 1;
		for(var i =0; i < arrTax.length; i=i+2)
		{
			trnTax += '{ "taxId":"'+arrTax[i]+'", "taxValue": "'+values[k]+'"},';
			k++;
		}
		trnTax = trnTax.substr(0,trnTax.length-1);
		trnTax += " ]";
	}
	$(this).parent().parent().find('.taxVal').attr('id',trnTax);
	callTaxTotal();
});
////////////////////////////////////////////////////////////////////

////////////////////////get exchange rate//////////////////////////
$('#cboCurrency').change(function(){
	var url = "creditNote-db-get.php?requestType=getExchangeRate&currencyId="+$(this).val()+'&exchangeDate='+$('#txtDate').val();
	var obj = $.ajax({url:url,dataType:'json',success:function(json){
				
		$('#rdoBuying').val(json.buyingRate);
		$('#rdoSelling').val(json.sellingRate);
		$('#rdoAverage').val(((eval(json.buyingRate) + eval(json.sellingRate))/2).toFixed(4));
		$('#rdoSelling').click();
		},async:false});
});
///////////////////////////////////////////////////////////////////
$('.rdoRate').click(function(){
  $('#txtRate').val($(this).val());
});
//save button click event
$('#frmCreditNote #butSave').click(function(){
if(existingMsgDate == "")
{
        id=$('#frmCreditNote #txtNo').val();
	var itemId = "";
	var itemDesc = "";
	var uomId = "";
	var qty = "";
	var unitPrice = "";
	var discount = "";
	var taxGroupId = "";
	var dimensionId = "";
	var amount = "";
	var trnTaxVal	= "";
			
 value="[ ";
	$('#tblMainGrid2 .mainRow').each(function(){
		
		itemId		= $(this).find(".item").val();
		
		style 		= $(this).find("#tblSubGrid .styleNo").val();
		graphic 	= $(this).find("#tblSubGrid .graphicNo").val();
		order	 	= $(this).find("#tblSubGrid .orderNo").val();
		line 		= $(this).find("#tblSubGrid .lineItem").val();
		locate 		= $(this).find("#tblSubGrid .cusLocate").val();
		itemDesc 	= $(this).find("#tblSubGrid .itemDesc").val();
		
		uomId 		= $(this).find(".uom").val();
		qty 		= $(this).find(".qty").val();
		unitPrice 	= $(this).find(".unitPrice").val();
		discount 	= $(this).find(".discount").val();
		taxAmount 	= $(this).find(".taxWith").val();
		taxGroupId 	= $(this).find(".taxGroup").val();
		dimensionId = $(this).find(".dimension").val();
		amount 		= $(this).find(".amount").val();
		trnTaxVal	= $(this).find(".taxVal").attr('id');
		
	value += '{ "itemId":"'+itemId+'", "style": "'+URLEncode(style)+'", "graphic": "'+URLEncode(graphic)+'", "order": "'+URLEncode(order)+'", "line": "'+URLEncode(line)+'", "locate": "'+URLEncode(locate)+'", "itemDesc": "'+URLEncode(itemDesc)+'", "uomId": "'+uomId+'", "qty": "'+qty+'", "unitPrice": "'+unitPrice+'", "discount": "'+discount+'", "taxGroupId": "'+taxGroupId+'", "taxAmount": "'+taxAmount+'", "dimensionId": "'+dimensionId+'", "amount": "'+amount+'", "trnTaxVal":'+trnTaxVal+'},';
	});
	
	value = value.substr(0,value.length-1);
	value += " ]";
	var requestType = '';
	if (($('#frmCreditNote').validationEngine('validate')) && (getAvailability == "" || amStatus == "Auto"))   
    { 
		if(($('#txtNo').val()=='' && amStatus == "Auto") || ($('#txtNo').val()!='' && amStatus == "Manual"))
			requestType = 'add';
			
		else
			requestType = 'edit';
		
		var url = "creditNote-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json",
			type:'post', 
			data:$("#frmCreditNote").serialize()+'&requestType='+requestType+'&cboSearch='+id+'&salesDetails='+value+'&invoType='+invoType+'&amStatus='+amStatus,
			async:false,
			
			success:function(json){
					$('#frmCreditNote #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						//$('#frmCreditNote').get(0).reset();
						var t=setTimeout("alertx()",1000);
						$('#txtNo').val(json.invoiceNo);
						amStatus = "Auto";
						loadCombo_frmCreditNote();
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmCreditNote #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
				}		
			});
	}
}
else
{
	$('#frmCreditNote #butSave').validationEngine('showPrompt', existingMsgDate,'fail');
	var t=setTimeout("alertx()",5000);
}
});
/////////////////////////////////////////////////////
//// load invoice details //////////////////////////
/////////////////////////////////////////////////////
$('#frmCreditNote #cboSearch').click(function(){
   $('#frmCreditNote').validationEngine('hide');
});
$('#frmCreditNote #cboSearch').change(function(){
//---------------------------------------------------
	existingMsgDate = "";
	amStatus = "Auto";
	document.getElementById("chkAutoManual").style.display='none';
	document.getElementById("amStatus").style.display='none';
	$("#frmCreditNote #txtNo").attr("readonly","readonly");
	$('#frmCreditNote #cboInvoiceNo').html('');
//---------------------------------------------------
$('#frmCreditNote').validationEngine('hide');
var url = "creditNote-db-get.php";
if($('#frmCreditNote #cboSearch').val()=='')
{
	$('#partPay').hide();
	$('#fullPay').hide();
	$('#overPay').hide();
	$('#pending').hide();
//---------------------------------------------------
	amStatus = "Auto";
	document.getElementById("chkAutoManual").style.display='';
	document.getElementById("amStatus").style.display='';
	$('#frmCreditNote #txtNo').removeClass('validate[required]');
	$("#frmCreditNote #txtNo").attr("readonly","readonly");
//---------------------------------------------------
	
	$('#tblMainGrid2 >tbody >tr').each(function(){
		if($(this).index()!=0 && $(this).index()!=1 )
		{
			$(this).remove();
		}
	});
	$('#frmCreditNote').get(0).reset();return;
}

$('#txtNo').val($(this).val());
var httpobj = $.ajax({
	url:url,
	dataType:'json',
	data:'requestType=loadDetails&id='+URLEncode($(this).val()),
	async:false,
	success:function(json)
	{
		//json  = eval('('+json+')');
		$('.invTpe').attr('checked',false); //
		
		$('#frmCreditNote #cboCustomer').val(json.customer);
		$('#cboCustomer').change();
		$('#frmCreditNote #txtDate').val(json.date);
		$('#frmCreditNote #'+json.invoType).attr('checked',true); //
		invoType = json.invoType;	
		$('#frmCreditNote #txtAddress').val(json.address);
		$('#frmCreditNote #txtShipTo').val(json.shipTo);
		$('#frmCreditNote #cboCurrency').val(json.currency);
		$('#frmCreditNote #txtRate').val(json.rate);
		$('#frmCreditNote #txtRemarks').val(json.remark);
		$('#frmCreditNote #txtPoNo').val(json.poNo);
		$('#frmCreditNote #cboPaymentsTerms').val(json.payTerms);
		$('#frmCreditNote #cboMarketer').val(json.marketer);
		$('#frmCreditNote #txtShipDate').val(json.shipDate);
		$('#frmCreditNote #cboShipmentMethod').val(json.shipment);
		$('#frmCreditNote #txtMessage').val(json.message);
		$('#frmCreditNote #cboHeadDimension').val(json.headDimension);
		$('#frmCreditNote #cboInvoiceNo').val(json.cusInvoiceNo);
		
//		if(json.totAmount != json.balAmount)
//		{
//			if(json.balAmount > 0)
//			{
//				$('#partPay').show();
//				$('#fullPay').hide();
//				$('#overPay').hide();
//				$('#pending').hide();
//			}
//			else if(json.balAmount == 0)
//			{
//				$('#fullPay').show();
//				$('#partPay').hide();
//				$('#overPay').hide();
//				$('#pending').hide();
//			}
//			if(json.balAmount < 0)
//			{
//				$('#partPay').hide();
//				$('#fullPay').hide();
//				$('#overPay').show();
//				$('#pending').hide();
//			}
//		}
//		else
//		{
//			$('#partPay').hide();
//			$('#fullPay').hide();
//			$('#overPay').hide();
//			$('#pending').show();
//		}
		
		//--------------------------------------------------
		$('#tblMainGrid2 >tbody >tr').each(function(){
			if($(this).index()!=0 && $(this).index()!=1 )
			{
				$(this).remove();
			}
		});
		var itemId 		= "";
		var styleNo 	= "";
		var graphicNo 	= "";
		var orderNo 	= "";
		var lineItem 	= "";
		var cusLocate 	= "";
		var itemDesc 	= "";
		var uom			= "";
		var qty			= "";
		var unitPrice	= "";
		var discount	= "";
		var taxAmount 	= "";
		var tax			= "";
		var dimension 	= "";
		if(json.detailVal!=null)
		{
			var rowId = $('#tblMainGrid2').find('tr').length;
			var tbl = document.getElementById('tblMainGrid2');
			rows = $('#tblMainGrid2').find('.mainRow').length + 1;
			for(var j=0;j<=json.detailVal.length-1;j++)
			{
				itemId		= json.detailVal[j].itemId;
				styleNo		= json.detailVal[j].styleNo;
				graphicNo	= json.detailVal[j].graphicNo;
				orderNo		= json.detailVal[j].orderNo;
				lineItem	= json.detailVal[j].lineItem;
				cusLocate	= json.detailVal[j].cusLocate;
				itemDesc	= json.detailVal[j].itemDesc;
				uom			= json.detailVal[j].uom;
				qty			= json.detailVal[j].qty;
				unitPrice	= json.detailVal[j].unitPrice;
				discount	= json.detailVal[j].discount;
				taxAmount	= json.detailVal[j].taxAmount;
				tax			= json.detailVal[j].taxGroup;
				dimension	= json.detailVal[j].dimension;
				//alert(j);
				//if(j != json.detailVal.length-1)
				if(j==0)
				{
					tbl.rows[1].cells[1].childNodes[1].value = itemId;
					
					$('#tblMainGrid2 >tbody >tr:eq('+(1)+')').find('#tblSubGrid .styleNo').val(styleNo);
					$('#tblMainGrid2 >tbody >tr:eq('+(1)+')').find('#tblSubGrid .graphicNo').val(graphicNo);
					$('#tblMainGrid2 >tbody >tr:eq('+(1)+')').find('#tblSubGrid .orderNo').val(orderNo);
					$('#tblMainGrid2 >tbody >tr:eq('+(1)+')').find('#tblSubGrid .lineItem').val(lineItem);
					$('#tblMainGrid2 >tbody >tr:eq('+(1)+')').find('#tblSubGrid .cusLocate').val(cusLocate);
					$('#tblMainGrid2 >tbody >tr:eq('+(1)+')').find('#tblSubGrid .itemDesc').val(itemDesc);
					
					tbl.rows[1].cells[3].childNodes[1].value = uom;
					tbl.rows[1].cells[4].childNodes[1].value = qty;
					tbl.rows[1].cells[5].childNodes[1].value = unitPrice;
					tbl.rows[1].cells[6].childNodes[1].value = discount;
					tbl.rows[1].cells[7].childNodes[1].value = ((unitPrice*((100-discount)/100))*qty).toFixed(4);
					//tbl.rows[1].cells[8].childNodes[1].value = tax; //taxGroup
					$('#tblMainGrid2 >tbody >tr:eq(1)>td:eq(8)').find('.taxGroup').val(tax);
					tbl.rows[1].cells[9].childNodes[1].value = dimension;
					tbl.rows[1].cells[10].childNodes[1].value = taxAmount;
					$('#tblMainGrid2 >tbody >tr:eq(1)>td:eq(8)').find('.taxGroup').change();
				}
				else
				{
					tbl.insertRow(rows);
					tbl.rows[rows].className='mainRow';
					tbl.rows[rows].innerHTML = tbl.rows[rows-1].innerHTML;
					tbl.rows[rows].cells[1].childNodes[1].value = itemId;
					
					$('#tblMainGrid2 >tbody >tr:eq('+rows+')>td:eq(2)').find('#tblSubGrid  .styleNo').val(styleNo);
					$('#tblMainGrid2 >tbody >tr:eq('+rows+')>td:eq(2)').find('#tblSubGrid .graphicNo').val(graphicNo);
					$('#tblMainGrid2 >tbody >tr:eq('+rows+')>td:eq(2)').find('#tblSubGrid .orderNo').val(orderNo);
					$('#tblMainGrid2 >tbody >tr:eq('+rows+')>td:eq(2)').find('#tblSubGrid .lineItem').val(lineItem);
					$('#tblMainGrid2 >tbody >tr:eq('+rows+')>td:eq(2)').find('#tblSubGrid .cusLocate').val(cusLocate);
					$('#tblMainGrid2 >tbody >tr:eq('+rows+')>td:eq(2)').find('#tblSubGrid .itemDesc').val(itemDesc);
					
					//$('#tblMainGrid2 >tbody >tr:eq('+(j+1)+')').find('#tblSubGrid  .styleNo').val(styleNo);
					//$('#tblMainGrid2 >tbody >tr:eq('+(j+1)+')').find('#tblSubGrid .graphicNo').val(graphicNo);
					//$('#tblMainGrid2 >tbody >tr:eq('+(j+1)+')').find('#tblSubGrid .orderNo').val(orderNo);
					//$('#tblMainGrid2 >tbody >tr:eq('+(j+1)+')').find('#tblSubGrid .lineItem').val(lineItem);
					//$('#tblMainGrid2 >tbody >tr:eq('+(j+1)+')').find('#tblSubGrid .cusLocate').val(cusLocate);
					//$('#tblMainGrid2 >tbody >tr:eq('+(j+1)+')').find('#tblSubGrid .itemDesc').val(itemDesc);
					
					tbl.rows[rows].cells[3].childNodes[1].value = uom;
					tbl.rows[rows].cells[4].childNodes[1].value = qty;
					tbl.rows[rows].cells[5].childNodes[1].value = unitPrice;
					tbl.rows[rows].cells[6].childNodes[1].value = discount;
					tbl.rows[rows].cells[7].childNodes[1].value = ((unitPrice*((100-discount)/100))*qty).toFixed(4);
					//tbl.rows[rows].cells[8].childNodes[1].value = tax;
					$('#tblMainGrid2 >tbody >tr:eq('+rows+')>td:eq(8)').find('.taxGroup').val(tax);
					tbl.rows[rows].cells[9].childNodes[1].value = dimension;
					tbl.rows[rows].cells[10].childNodes[1].value = taxAmount;
					$('#tblMainGrid2 >tbody >tr:eq('+rows+')>td:eq(8)').find('.taxGroup').change();
				}
				//loadJs();
			}
			callSubTotal();
			callTaxTotal();
		}
		else
		{
			
		}
	   //--------------------------------------------------
	}
});
});
//////////// end of load details /////////////////

//++++++++++++++++++++++++Added by Lasantha @ CAIT on 14/12/2012+++++++++++++++++++++++++++++++
	$('#frmCreditNote #cboInvoiceNo').change(function(){
		var url = "creditNote-db-get.php";
		var httpobj = $.ajax({
		url:url,
		dataType:'json',
		data:'requestType=loadInvoiceDetails&id='+URLEncode($(this).val()),
		async:false,
		success:function(json)
		{
			//--------------------------------------------------
			$('#tblMainGrid2 >tbody >tr').each(function(){
				if($(this).index()!=0 && $(this).index()!=1 )
				{
					$(this).remove();
				}
			});
			var itemId 		= "";
			var styleNo 	= "";
			var graphicNo 	= "";
			var orderNo 	= "";
			var lineItem 	= "";
			var cusLocate 	= "";
			var itemDesc 	= "";
			var uom			= "";
			var qty			= "";
			var unitPrice	= "";
			var discount	= "";
			var taxAmount 	= "";
			var tax			= "";
			var dimension 	= "";
			if(json.detailVal!=null)
			{
				var rowId = $('#tblMainGrid2').find('tr').length;
				var tbl = document.getElementById('tblMainGrid2');
				rows = $('#tblMainGrid2').find('.mainRow').length + 1;
				for(var j=0;j<=json.detailVal.length-1;j++)
				{
					itemId		= json.detailVal[j].itemId;
					styleNo		= json.detailVal[j].styleNo;
					graphicNo	= json.detailVal[j].graphicNo;
					orderNo		= json.detailVal[j].orderNo;
					lineItem	= json.detailVal[j].lineItem;
					cusLocate	= json.detailVal[j].cusLocate;
					itemDesc	= json.detailVal[j].itemDesc;
					uom			= json.detailVal[j].uom;
					qty			= json.detailVal[j].qty;
					unitPrice	= json.detailVal[j].unitPrice;
					discount	= json.detailVal[j].discount;
					taxAmount	= json.detailVal[j].taxAmount;
					tax			= json.detailVal[j].taxGroup;
					dimension	= json.detailVal[j].dimension;
	
					if(j==0)
					{
						tbl.rows[1].cells[1].childNodes[1].value = itemId;
						
						$('#tblMainGrid2 >tbody >tr:eq('+(1)+')').find('#tblSubGrid .styleNo').val(styleNo);
						$('#tblMainGrid2 >tbody >tr:eq('+(1)+')').find('#tblSubGrid .graphicNo').val(graphicNo);
						$('#tblMainGrid2 >tbody >tr:eq('+(1)+')').find('#tblSubGrid .orderNo').val(orderNo);
						$('#tblMainGrid2 >tbody >tr:eq('+(1)+')').find('#tblSubGrid .lineItem').val(lineItem);
						$('#tblMainGrid2 >tbody >tr:eq('+(1)+')').find('#tblSubGrid .cusLocate').val(cusLocate);
						$('#tblMainGrid2 >tbody >tr:eq('+(1)+')').find('#tblSubGrid .itemDesc').val(itemDesc);
						
						tbl.rows[1].cells[3].childNodes[1].value = uom;
						tbl.rows[1].cells[4].childNodes[1].value = qty;
						tbl.rows[1].cells[5].childNodes[1].value = unitPrice;
						tbl.rows[1].cells[6].childNodes[1].value = discount;
						tbl.rows[1].cells[7].childNodes[1].value = ((unitPrice*((100-discount)/100))*qty).toFixed(4);
	
						$('#tblMainGrid2 >tbody >tr:eq(1)>td:eq(8)').find('.taxGroup').val(tax);
						tbl.rows[1].cells[9].childNodes[1].value = dimension;
						tbl.rows[1].cells[10].childNodes[1].value = taxAmount;
						$('#tblMainGrid2 >tbody >tr:eq(1)>td:eq(8)').find('.taxGroup').change();
					}
					else
					{
						tbl.insertRow(rows);
						tbl.rows[rows].className='mainRow';
						tbl.rows[rows].innerHTML = tbl.rows[rows-1].innerHTML;
						tbl.rows[rows].cells[1].childNodes[1].value = itemId;
						
						$('#tblMainGrid2 >tbody >tr:eq('+rows+')>td:eq(2)').find('#tblSubGrid  .styleNo').val(styleNo);
						$('#tblMainGrid2 >tbody >tr:eq('+rows+')>td:eq(2)').find('#tblSubGrid .graphicNo').val(graphicNo);
						$('#tblMainGrid2 >tbody >tr:eq('+rows+')>td:eq(2)').find('#tblSubGrid .orderNo').val(orderNo);
						$('#tblMainGrid2 >tbody >tr:eq('+rows+')>td:eq(2)').find('#tblSubGrid .lineItem').val(lineItem);
						$('#tblMainGrid2 >tbody >tr:eq('+rows+')>td:eq(2)').find('#tblSubGrid .cusLocate').val(cusLocate);
						$('#tblMainGrid2 >tbody >tr:eq('+rows+')>td:eq(2)').find('#tblSubGrid .itemDesc').val(itemDesc);
						
						//$('#tblMainGrid2 >tbody >tr:eq('+(j+1)+')').find('#tblSubGrid  .styleNo').val(styleNo);
						//$('#tblMainGrid2 >tbody >tr:eq('+(j+1)+')').find('#tblSubGrid .graphicNo').val(graphicNo);
						//$('#tblMainGrid2 >tbody >tr:eq('+(j+1)+')').find('#tblSubGrid .orderNo').val(orderNo);
						//$('#tblMainGrid2 >tbody >tr:eq('+(j+1)+')').find('#tblSubGrid .lineItem').val(lineItem);
						//$('#tblMainGrid2 >tbody >tr:eq('+(j+1)+')').find('#tblSubGrid .cusLocate').val(cusLocate);
						//$('#tblMainGrid2 >tbody >tr:eq('+(j+1)+')').find('#tblSubGrid .itemDesc').val(itemDesc);
						
						tbl.rows[rows].cells[3].childNodes[1].value = uom;
						tbl.rows[rows].cells[4].childNodes[1].value = qty;
						tbl.rows[rows].cells[5].childNodes[1].value = unitPrice;
						tbl.rows[rows].cells[6].childNodes[1].value = discount;
						tbl.rows[rows].cells[7].childNodes[1].value = ((unitPrice*((100-discount)/100))*qty).toFixed(4);
						$('#tblMainGrid2 >tbody >tr:eq('+rows+')>td:eq(8)').find('.taxGroup').val(tax);
						tbl.rows[rows].cells[9].childNodes[1].value = dimension;
						tbl.rows[rows].cells[10].childNodes[1].value = taxAmount;
						$('#tblMainGrid2 >tbody >tr:eq('+rows+')>td:eq(8)').find('.taxGroup').change();
					}
				}
				callSubTotal();
				callTaxTotal();
			}
			else
			{
				
			}
	   //--------------------------------------------------
		}
		});
	});
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  	$('#frmCreditNote #butNew').click(function(){
	//---------------------------------------------------
		existingMsgDate = "";
		amStatus = "Auto";
		$("#frmCreditNote #txtNo").attr("readonly","readonly");
		$('#frmCreditNote #chkAutoManual').attr('checked')
		$("#frmCreditNote #chkAutoManual").attr("disabled","");
		document.getElementById("chkAutoManual").style.display='';
		document.getElementById("amStatus").style.display='';
		$('#frmCreditNote #txtNo').removeClass('validate[required]');
	//---------------------------------------------------
	$('#frmCreditNote').get(0).reset();
	$('#frmCreditNote #cboInvoiceNo').html('');
	$('#tblMainGrid2 >tbody >tr').each(function(){
			if($(this).index()!=0 && $(this).index()!=1 )
			{
				$(this).remove();
			}
		});
	$('#partPay').hide();
	$('#fullPay').hide();
	$('#overPay').hide();
	$('#pending').hide();
	loadCombo_frmCreditNote();
	$('#frmCreditNote #cboCustomer').focus();
	});
	$('.invTpe').click(function(){
		invoType = $(this).val();	
	});
	$('#frmCreditNote #butDelete').click(function(){
		if($('#frmCreditNote #cboSearch').val()=='')
		{
			$('#frmCreditNote #butDelete').validationEngine('showPrompt', 'Please select Invoice.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
            var customerId=$('#frmCreditNote #cboCustomer').val()
			var val = $.prompt('Are you sure you want to delete "'+$('#frmCreditNote #cboSearch option:selected').text()+'" ?',{
			buttons: { Ok: true, Cancel: false },
			callback: function(v,m,f){
			if(v)
			{
					var url = "creditNote-db-set.php";
					var httpobj = $.ajax({
						url:url,
						dataType:'json',
						data:'requestType=delete&cboSearch='+URLEncode($('#frmCreditNote #cboSearch').val())+"&cboCustomer="+customerId,
						async:false,
						success:function(json){
							
							$('#frmCreditNote #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
							
							if(json.type=='pass')
							{
								$('#frmCreditNote').get(0).reset();
								$('#tblMainGrid2 >tbody >tr').each(function(){
									if($(this).index()!=0 && $(this).index()!=1 )
									{
										$(this).remove();
									}
								});
								loadCombo_frmCreditNote();
								var t=setTimeout("alertDelete()",1000);return;
							}	
							var t=setTimeout("alertDelete()",3000);
						}	 
					});
			}
		}
			});	
		}
	});
	
	$('#frmCreditNote #butPrint').click(function(){
		if($('#frmCreditNote #txtNo').val()=='')
		{
			$('#frmCreditNote #butPrint').validationEngine('showPrompt', 'Please select Invoice.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var myurl = 'listing/creditNoteDetails.php?id='+URLEncode($('#frmCreditNote #txtNo').val());
    		window.open(myurl); 
		}
	});
	
});

//-------------------------------------------functions area----------------------------------------------------------

////////////////////// calculation ////////////////////////////////
function calculate()
{ 
var quantity =  ($(this).parent().parent().find('.qty').val()==''?0:$(this).parent().parent().find('.qty').val());

var unitprice  = ($(this).parent().parent().find('.unitPrice').val()==''?0:$(this).parent().parent().find('.unitPrice').val());

var discount  = ($(this).parent().parent().find('.discount').val()==''?0:$(this).parent().parent().find('.discount').val());

var calAmount = parseFloat(quantity)*(parseFloat(unitprice)*(100-parseFloat(discount))/100);

$(this).parent().parent().find('.amount').val(calAmount.toFixed(4));
callSubTotal();
$(this).parent().parent().find(".taxGroup").change();
callTotalAmount();
}
////////////////////////////////////////////////////////////////////
function callTaxProcess(taxGroupId)
{
	var url = "creditNote-db-get.php?requestType=getTaxProcess&taxGroupId="+taxGroupId;
	var obj = $.ajax({url:url,async:false});
	return obj.responseText;
}
function callSubTotal()
{
	var subTotal = 0.00;
	$(".amount").each( function(){
          subTotal += eval($(this).val()==''?0.00:$(this).val());
	});
	$('#txtSubTotal').val(subTotal.toFixed(4));	
}
function callTaxTotal()
{
	var taxTotal = 0.00;
	$(".taxWith").each( function(){
          taxTotal += eval($(this).val()==''?0.00:$(this).val());
	});
	$('#txtTotalTax').val(taxTotal.toFixed(4));
	callTotalAmount();
}
function callTotalAmount()
{
	$('#txtTotal').val((eval($('#txtTotalTax').val()) + eval($('#txtSubTotal').val())).toFixed(4));
}
function loadCombo_frmCreditNote()
{
	var url 	= "creditNote-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmCreditNote #cboSearch').html(httpobj.responseText);
}
function alertx()
{
	$('#frmCreditNote #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmCreditNote #butDelete').validationEngine('hide') ;
	$('#frmCreditNote #butPrint').validationEngine('hide') ;
}
function loadJs()
{
//	inc('creditNote-js.js');//creditNote-js.js
//	inc('../../../../libraries/javascript/script.js');
//	inc('../../../../libraries/validate/jquery-1.js');
//	inc('../../../../libraries/validate/jquery_002.js');
//	inc('../../../../libraries/validate/jquery.js');
//	
//	$("input[name^=txtQty]").bind("keyup", calculate);
//	$("input[name^=txtUnitPrice]").bind("keyup", calculate);
//	$("input[name^=txtDiscount]").bind("keyup", calculate);	
//	$('#butSave').unbind('click');
//	$('.delImg').unbind('click');
//	$('.taxGroup').unbind('change');
//	$('.qty').unbind('keyup');
//	$('.unitPrice').unbind('keyup');
//	$('.txtDiscount').unbind('keyup');
}

//=========Add by dulaskshi 2013.03.20===========
//====================Get Supplier List==============================
function getSupplierList()
{	
	$ledgerAccId = $('#frmCreditNote #cboLedgerAcc').val();
	
	var url = "creditNote-db-get.php?requestType=loadCustomer&ledgerAcc="+$ledgerAccId;
	var httpobj = $.ajax({url:url,async:false})
	$('#frmCreditNote #cboCustomer').html(httpobj.responseText);
}
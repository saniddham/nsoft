<?php
session_start();
$backwardseperator = "../../../../";
$thisFilePath =  $_SERVER['PHP_SELF'];

include  "{$backwardseperator}dataAccess/permisionCheck.inc";

$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$companyId = $row['intCompanyId']; 
}

if($_SERVER['REQUEST_METHOD'] == "POST")
{
	$optionValue = trim($_REQUEST['cboSearchOption']);
	$searchValue = trim($_REQUEST['txtSearchValue']);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cash Sales Listing</title>

<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/tblstyle.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../libraries/jquery/jquery-1.3.2.min.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/scrolltable.js"></script>

<script type="application/javascript" src="../../../../libraries/javascript/zebraStripe.js"></script>

<script type="text/javascript" language="javascript">
function pageSubmit()
{
	document.getElementById('frmCashSalesListing').submit();	
}
function clearValue()
{
	document.getElementById('txtSearchValue').value = '';
	document.getElementById('txtSearchValue').focus();
}
</script>

</head>

<body>
<form id="frmCashSalesListing" name="frmCashSalesListing" method="post" autocomplete="off" action="cashSalesListing.php">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">Cash Sales Listing</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0" cellpadding="1" cellspacing="0" class="tableBorder_allRound">
          <tr>
            <td width="16%" height="22" class="normalfnt">&nbsp;</td>
            <td width="18%" class="normalfntMid">Searching Option</td>
            <td width="17%" class="normalfnt">
            <select name="cboSearchOption" id="cboSearchOption" onchange="clearValue();" style="width:150px">
              <option value="">&nbsp;</option>
<option value="mst_customer.strName" <?php echo($optionValue=='mst_customer.strName'?'selected':'') ?> >Customer</option>
<option value="fin_customer_cashsales_header.strPoNo" <?php echo($optionValue=='fin_customer_cashsales_header.strPoNo'?'selected':'') ?>>P.O. Number</option>
<option value="sys_users.strUserName" <?php echo($optionValue=='sys_users.strUserName'?'selected':'') ?>>Marketer</option>
<option value="fin_customer_cashsales_header.strReferenceNo" <?php echo($optionValue=='fin_customer_cashsales_header.strReferenceNo'?'selected':'') ?>>Invoice Number</option>
            </select></td>
            <td width="16%"><input value="<?php echo $searchValue; ?>" type="text" name="txtSearchValue" id="txtSearchValue" /></td>
            <td width="18%" class="normalfnt">
            <a href="#">
            <img src="../../../../images/search.png" width="20" height="20" onclick="pageSubmit();" /></a></td>
            <td width="15%">&nbsp;</td>
            </tr>
          <tr>
            <td height="22" class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td>
          <div id="_table_wrap" style="overflow: hidden; display: inline-block; border: 1px solid rgb(136, 136, 136);">
          <div id="_head_wrap" style="width: 900px; overflow: hidden;">
          <table class="tbl1 scroll" id="_head" style="table-layout: fixed;">
          <thead>
            <tr class="gridHeader">
              <th height="22" style="width: 120px;" >Customer</th>
              <th style="width: 80px;" >P.O. Number</th>
              <th style="width: 60px;" >Marketer</th>
              <th style="width: 120px;" >Invoice Number</th>
              <th style="width: 35px;" >View</th>
              <th style="width: 7px;" ></th>
              </tr>
           </table>
           </div>
           <div class="normalfnt" id="_body_wrap" style="width: 900px; height: 320px; overflow: scroll;">
           <table class="tbl1 scroll"> 
           <tbody>
              <?php

				if($searchValue!='')
			  	$wherePart = " and $optionValue like '%$searchValue%'";
				
	 	 		$sql = "SELECT
						fin_customer_cashsales_header.intInvoiceNo,
						fin_customer_cashsales_header.intAccPeriodId,
						fin_customer_cashsales_header.intLocationId,
						fin_customer_cashsales_header.intCompanyId,
						fin_customer_cashsales_header.intCustomerId,
						mst_customer.intId,
						mst_customer.strName,
						fin_customer_cashsales_header.strPoNo,
						fin_customer_cashsales_header.intMarketerId,
						sys_users.strUserName,
						fin_customer_cashsales_header.strReferenceNo
						FROM
						fin_customer_cashsales_header
					left outer Join mst_customer ON fin_customer_cashsales_header.intCustomerId = mst_customer.intId
					left outer Join sys_users ON fin_customer_cashsales_header.intMarketerId = sys_users.intUserId
						WHERE
						fin_customer_cashsales_header.intCompanyId = '$companyId' AND
						fin_customer_cashsales_header.intDeleteStatus =  '0'
						$wherePart
						ORDER BY
						mst_customer.strName ASC";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$id 		=	 $row['strReferenceNo'];
	  			 ?>
           		<tr class="normalfnt">
           		<td class="slInvoCusName" align="center" bgcolor="#FFFFFF"><?php echo $row['strName'];?></td>
              	<td class="slInvoPo"  align="center" bgcolor="#FFFFFF"><?php echo $row['strPoNo'];?></td>
              	<td class="slInvoMarketer" bgcolor="#FFFFFF"><?php echo $row['strUserName'];?></td>
              	<td class="slInvoRef" align="center" bgcolor="#FFFFFF">
                <a target="_blank" href="<?php echo "cashSales.php?id=$id";?>">
				<?php echo $row['strReferenceNo'];?></a></td>
              	<td class="slInvoView" align="center" bgcolor="#FFFFFF">
                <a target="_blank" href="<?php echo "cashSalesDetails.php?id=$id";?>">More</a></td>
              </tr>
               <?php 
        	    } 
       		   ?>
              </tbody>
             </table>
            </div></div>
              </td>
              </tr>
            </table>
			</td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound"><a href="../../../../main.php"><img  src="../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
</body>
</html>

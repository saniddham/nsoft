<?php 
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
        $companyId = $_SESSION['headCompanyId'];	
	$locationId = $_SESSION["CompanyID"];
	////////////////////////// main parameters ////////////////////////////////
	$invoice		= trim($_REQUEST['txtNo']);
	$requestType 	= $_REQUEST['requestType'];
	$id 			= $_REQUEST['cboSearch'];
	///////////////////// cash sales invoice header parameters ////////////////////
	$customer		= null(trim($_REQUEST['cboCustomer']));
	$date			= trim($_REQUEST['txtDate']);
	$address		= $_REQUEST['txtAddress'];
	$shipTo 		= $_REQUEST['txtShipTo'];
	$currency		= null(trim($_REQUEST['cboCurrency']));
	$rate			= val(trim($_REQUEST['txtRate']));
	$remarks 		= $_REQUEST['txtRemarks'];
	$poNo			= trim($_REQUEST['txtPoNo']);
	$paymentsTerms	= null(trim($_REQUEST['cboPaymentsTerms']));
	$marketer		= null(trim($_REQUEST['cboMarketer']));
	$shipDate 		= trim($_REQUEST['txtShipDate']);
	$shipmentMethod	= null(trim($_REQUEST['cboShipmentMethod']));
	$message		= $_REQUEST['txtMessage'];
	$chartOfAcc		= null($_REQUEST['cboChartOfAcc']);
	///////////////////// cash sales invoice detail parameters /////////////////////
	$details = json_decode($_REQUEST['salesDetails'], true);
	///////////////////////////////////////////////////////////////////////////
	$cusTotalAmount	= $_REQUEST['txtTotal'];
	$depositAccount = $_REQUEST['cboChartOfAcc'];
	//////////////////////// cash sales invoice insert part ///////////////////////
	if($requestType=='add')
	{
            try{
		$invoiceNumber 		= getNextInvoiceNo($companyId,$locationId);
		$accountPeriod 		= getLatestAccPeriod($companyId);
		$invoiceReference	= trim(encodeInvoiceNo($invoiceNumber,$accountPeriod,$companyId,$locationId));
                
		$db->begin();
                //Add data to transaction header*******************************************
                $sql="INSERT INTO fin_transactions (entryDate, strProgramType, documentNo, currencyId, currencyRate, transDetails, payMethodId, paymentNumber, accPeriod, userId, companyId, createdOn) VALUES
                    ('$date','Cash Sales','$invoiceReference',$currency,$rate,'$remarks',null,null,$accountPeriod,$userId,$companyId,now())";
                
                $db->RunQuery2($sql);
                $entryId=$db->insertId;                
                //********************************************************************************
                
		$sql = "INSERT INTO `fin_customer_cashsales_header` (`intInvoiceNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strReferenceNo`,`intCustomerId`,`dtmDate`,`strAddress`,`strShipTo`,`intCurrencyId`,`dblRate`,`strRemark`,`strPoNo`,`intPaymentsTermsId`,`intMarketerId`,`dtmShipDate`,`intShipmentId`,`strMessage`,`intChartOfAccountId`,`intCreator`,dtmCreateDate,`intDeleteStatus`,entryId) 
				VALUES ('$invoiceNumber','$accountPeriod','$locationId','$companyId','$invoiceReference',$customer,'$date','$address','$shipTo',$currency,'$rate','$remarks','$poNo',$paymentsTerms,$marketer,'$shipDate',$shipmentMethod,'$message',$chartOfAcc,'$userId',now(),'0',$entryId)";
		$firstResult = $db->RunQuery2($sql);
		
		if(count($details) != 0 && $firstResult)
		{
			foreach($details as $detail)
			{
				$item 			= $detail['itemId'];
				$itemDesc		= $detail['itemDesc'];
				$uom			= null($detail['uomId']);
				$quantity 		= val($detail['qty']);
				$unitPrice		= val($detail['unitPrice']);
				$discount		= val($detail['discount']);
				$taxAmount		= val($detail['taxAmount']);
				$taxGroup 		= null($detail['taxGroupId']);
				$dimension		= null($detail['dimensionId']);
				$itemAmount		= val($detail['amount']);
				$taxDetails 	= $detail['trnTaxVal'];
				
				$sql = "INSERT INTO `fin_customer_cashsales_details` (`intInvoiceNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strReferenceNo`,`intItem`,`strItemDesc`,`intUom`,`dblQty`,`dblUnitPrice`,`dblDiscount`,`dblTaxAmount`,`intTaxGroupId`,`intDimensionId`,`intCreator`,dtmCreateDate) 
				VALUES ('$invoiceNumber','$accountPeriod','$locationId','$companyId','$invoiceReference','$item','$itemDesc',$uom,'$quantity','$unitPrice','$discount','$taxAmount',$taxGroup,$dimension,'$userId',now())";
				
				$finalResult = $db->RunQuery2($sql);
				
			//>>>>>>>>>>>>>>>>>>>>>>transaction table process - item>>>>>>>>>>>>>>>>>>>>>>>>>
			$sql = "SELECT
					mst_financecustomeritemactivate.intChartOfAccountId
					FROM mst_financecustomeritemactivate
					WHERE
					mst_financecustomeritemactivate.intCustomerItemId = '$item' AND
					mst_financecustomeritemactivate.intCompanyId = '$companyId'";
				 $result = $db->RunQuery2($sql);
				 $row = mysqli_fetch_array($result);
				 $itemAccount = $row['intChartOfAccountId'];				 
			
                        $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                                        ($entryId,'C',$itemAccount,$itemAmount,'$remarks',$dimension)";
                                $trnResult = $db->RunQuery2($sql);
			//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			
			//>>>>>>>>>>>>>>>>>>>>>>>>>>>transaction table process - tax>>>>>>>>>>>>>>>>>>>>>>>>>
			if(count($taxDetails) != 0 && $trnResult)
			{
				foreach($taxDetails as $taxDetail)
				{
                                    $taxId 	= $taxDetail['taxId'];
                                    $taxAmount	= $taxDetail['taxValue'];

                                    $sql = "SELECT
                                    mst_financetaxactivate.intChartOfAccountId
                                    FROM mst_financetaxactivate
                                    WHERE
                                    mst_financetaxactivate.intTaxId = '$taxId' AND
                                    mst_financetaxactivate.intCompanyId = '$companyId'";
                                    $result = $db->RunQuery2($sql);
                                    $row = mysqli_fetch_array($result);
                                    $taxAccount = $row['intChartOfAccountId'];

                                    $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                                            ($entryId,'C',$taxAccount,$taxAmount,'$remarks',null)";
                                    $db->RunQuery2($sql);
				}		
			}
			//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			}
			//>>>>>>>>>>>>>>>>>>>>>>transaction table process - customer>>>>>>>>>>>>>>>>>>>>>>>>>			
                        $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                                ($entryId,'D',$depositAccount,$cusTotalAmount,'$remarks',null)";
			$trnResult=$db->RunQuery2($sql);
			//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		}
		if($finalResult)
		{
                    $db->commit();
                    $response['type'] 		= 'pass';
                    $response['msg'] 		= 'Saved successfully.';
                    $response['invoiceNo'] 	= $invoiceReference;
		}
		else{
                    $db->rollback();//roalback
                    $response['type'] 		= 'fail';
                    $response['msg'] 		= $db->errormsg;
                    $response['q'] 			= $sql;
		}
                echo json_encode($response);            
            }catch(Exception $e){
               
                $db->rollback();//roalback
                $response['type'] 		= 'fail';
                $response['msg'] 		= $e->getMessage();
                $response['q'] 			= $sql;                
               echo json_encode($response);                 
            }
	}
	////////////////////// cash sales invoice update part ////////////////////////
	else if($requestType=='edit')
	{
            try {
                $db->begin();
		$sql = "UPDATE `fin_customer_cashsales_header` SET 	intCustomerId		= $customer,
                            dtmDate				='$date',
                            strAddress			='$address',
                            strShipTo			='$shipTo',
                            intCurrencyId		= $currency,
                            dblRate				='$rate',
                            strRemark			='$remarks',
                            strPoNo				='$poNo',
                            intPaymentsTermsId	= $paymentsTerms,
                            intMarketerId		= $marketer,
                            dtmShipDate			='$shipDate',
                            intShipmentId		= $shipmentMethod,
                            strMessage			='$message',
                            intChartOfAccountId	= $chartOfAcc,
                            intModifyer			='$userId',
                            intDeleteStatus		= '0'
				WHERE (`strReferenceNo`='$invoice')";
		$firstResult = $db->RunQuery2($sql);
		if(count($details) != 0 && $firstResult)
		{
			$sql = "SELECT
					fin_customer_cashsales_header.intInvoiceNo,
					fin_customer_cashsales_header.intAccPeriodId,
					fin_customer_cashsales_header.strReferenceNo,
					fin_customer_cashsales_header.entryId
					FROM
					fin_customer_cashsales_header
					WHERE
					fin_customer_cashsales_header.strReferenceNo =  '$invoice'
					";
			$result = $db->RunQuery2($sql);
			while($row=mysqli_fetch_array($result))
			{
				$invoiceNumber 	= $row['intInvoiceNo'];
				$accountPeriod 	= $row['intAccPeriodId'];
                                $entryId= $row['entryId'];
			}
                        //========update the transaction deader====================
                        $sql="UPDATE fin_transactions SET 
                                    entryDate='$date',                                                         
                                    currencyId=$currency,
                                    currencyRate='$rate',
                                    transDetails='$remarks',                    
                                    accPeriod=$accountPeriod
                            WHERE entryId=$entryId";
                        $db->RunQuery2($sql);

                        $sqld = "DELETE FROM `fin_transactions_details` WHERE entryId=$entryId";
                        $resultd = $db->RunQuery2($sqld);
                    //=========================================================
			$sql = "DELETE FROM `fin_customer_cashsales_details` WHERE (`strReferenceNo`='$invoice')";
			$db->RunQuery2($sql);			
			
			foreach($details as $detail)
			{
				$item 			= $detail['itemId'];
				$itemDesc		= $detail['itemDesc'];
				$uom			= null($detail['uomId']);
				$quantity 		= val($detail['qty']);
				$unitPrice		= val($detail['unitPrice']);
				$discount		= val($detail['discount']);
				$taxAmount		= val($detail['taxAmount']);
				$taxGroup 		= null($detail['taxGroupId']);
				$dimension		= null($detail['dimensionId']);
				$itemAmount		= val($detail['amount']);
				$taxDetails 	= $detail['trnTaxVal'];
				
				$sql = "INSERT INTO `fin_customer_cashsales_details` (`intInvoiceNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strReferenceNo`,`intItem`,`strItemDesc`,`intUom`,`dblQty`,`dblUnitPrice`,`dblDiscount`,`dblTaxAmount`,`intTaxGroupId`,`intDimensionId`,`intCreator`,dtmCreateDate) 
				VALUES ('$invoiceNumber','$accountPeriod','$locationId','$companyId','$invoice','$item','$itemDesc',$uom,'$quantity','$unitPrice','$discount','$taxAmount',$taxGroup,$dimension,'$userId',now())";
				
				$finalResult = $db->RunQuery2($sql);
				
				//>>>>>>>>>>>>>>>>>>>>>>transaction table process - item>>>>>>>>>>>>>>>>>>>>>>>>>
			$sql = "SELECT
					mst_financecustomeritemactivate.intChartOfAccountId
					FROM mst_financecustomeritemactivate
					WHERE
					mst_financecustomeritemactivate.intCustomerItemId = '$item' AND
					mst_financecustomeritemactivate.intCompanyId = '$companyId'";
				 $result = $db->RunQuery2($sql);
				 $row = mysqli_fetch_array($result);
				 $itemAccount = $row['intChartOfAccountId'];
				 
			$sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                                        ($entryId,'C',$itemAccount,$itemAmount,'$remarks',$dimension)";
                        $trnResult = $db->RunQuery2($sql);
			//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			
			//>>>>>>>>>>>>>>>>>>>>>>>>>>>transaction table process - tax>>>>>>>>>>>>>>>>>>>>>>>>>
			if(count($taxDetails) != 0 && $trnResult)
			{
				foreach($taxDetails as $taxDetail)
				{
                                    $taxId 	= $taxDetail['taxId'];
                                    $taxAmount	= $taxDetail['taxValue'];

                                    $sql = "SELECT
                                    mst_financetaxactivate.intChartOfAccountId
                                    FROM mst_financetaxactivate
                                    WHERE
                                    mst_financetaxactivate.intTaxId = '$taxId' AND
                                    mst_financetaxactivate.intCompanyId = '$companyId'";
                                    $result = $db->RunQuery2($sql);
                                    $row = mysqli_fetch_array($result);
                                    $taxAccount = $row['intChartOfAccountId'];

                                    $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                                            ($entryId,'C',$taxAccount,$taxAmount,'$remarks',null)";
                                    $db->RunQuery2($sql);
				}		
			}
			//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			}
			//>>>>>>>>>>>>>>>>>>>>>>transaction table process - customer>>>>>>>>>>>>>>>>>>>>>>>>>			
                        
                        $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                                ($entryId,'D',$depositAccount,$cusTotalAmount,'$remarks',null)";
			$trnResult=$db->RunQuery2($sql);
			//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		}
		if($finalResult)
		{
                    $db->commit();
                    $response['type'] 		= 'pass';
                    $response['msg'] 		= 'Updated successfully.';
                    $response['invoiceNo'] 	= $invoice;
		}
		else
		{
                    $db->rollback(); //roalback
                    $response['type'] 		= 'fail';
                    $response['msg'] 		= $db->errormsg;
                    $response['q'] 			=$sql;
		}
                echo json_encode($response);
            } catch (Exception $e) {
                $db->rollback(); //roalback
                $response['type'] = 'fail';
                $response['msg'] = $e->getMessage();
                $response['q'] = $sql;
                echo json_encode($response);
            }
	}
	/////////// cash sales invoice delete part /////////////////////
	else if($requestType=='delete')
	{
            try{
                $db->begin();		
			
		$sql = "UPDATE `fin_customer_cashsales_header` SET intDeleteStatus ='1', intModifyer ='$userId'
				WHERE (`strReferenceNo`='$id')";
		$result = $db->RunQuery2($sql);
                
                //==========UPDATE TRANS ACTION delete STATUS
                        $sql="SElect fin_customer_cashsales_header.entryId FROM fin_customer_cashsales_header WHERE (`strReferenceNo`='$id')";
                        $result = $db->RunQuery2($sql);
                        $row = mysqli_fetch_array($result);
                        $entryId=$row['entryId'];                        
                        $sqld = "UPDATE `fin_transactions` SET delStatus=1 WHERE entryId=$entryId";
                        $resultd = $db->RunQuery2($sqld);
                        //============================
		if(($result && $resultd)){
                    $db->commit();
                    $response['type'] 		= 'pass';
                    $response['msg'] 		= 'Deleted successfully.';
		}
		else{
                    $db->rollback(); //roalback
                    $response['type'] 		= 'fail';
                    $response['msg'] 		= $db->errormsg;
                    $response['q'] 			=$sql;
		}
                echo json_encode($response);
            } catch (Exception $e) {
                $db->rollback(); //roalback
                $response['type'] = 'fail';
                $response['msg'] = $e->getMessage();
                $response['q'] = $sql;
                echo json_encode($response);
            }  
	}	
//--------------------------------------------------------------------------------------------
	function getNextInvoiceNo($companyId,$locationId)
	{
		global $db;
		$sql = "SELECT
				intCashSalesNo
				FROM sys_finance_no
				WHERE
				intCompanyId = '$companyId' AND intLocationId = '$locationId'
				";	
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$nextInvoiceNo = $row['intCashSalesNo'];
		
		$sql = "UPDATE `sys_finance_no` SET intCashSalesNo=intCashSalesNo+1 WHERE (intCompanyId = '$companyId' AND intLocationId = '$locationId')";
		$db->RunQuery($sql);
		return $nextInvoiceNo;
	}
//--------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------
	function getLatestAccPeriod($companyId)
	{
		global $db;
		$sql = "SELECT
				MAX(mst_financeaccountingperiod.intId) AS accId,
				mst_financeaccountingperiod.dtmStartingDate,
				mst_financeaccountingperiod.dtmClosingDate,
				mst_financeaccountingperiod.intStatus,
				mst_financeaccountingperiod_companies.intCompanyId,
				mst_financeaccountingperiod_companies.intPeriodId
				FROM
				mst_financeaccountingperiod
				Inner Join mst_financeaccountingperiod_companies ON mst_financeaccountingperiod_companies.intPeriodId = mst_financeaccountingperiod.intId
				WHERE
				mst_financeaccountingperiod_companies.intCompanyId =  '$companyId' AND mst_financeaccountingperiod.intStatus = '1'
				ORDER BY
				mst_financeaccountingperiod.intId DESC
				";	
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$latestAccPeriodId = $row['accId'];	
		return $latestAccPeriodId;
	}
//--------------------------------------------------------------------------------------------
//============================================================================================
	function encodeInvoiceNo($invoiceNumber,$accountPeriod,$companyId,$locationId)
	{
		global $db;
		$sql = "SELECT
				mst_financeaccountingperiod.intId,
				mst_financeaccountingperiod.dtmStartingDate,
				mst_financeaccountingperiod.dtmClosingDate,
				mst_financeaccountingperiod.intStatus
				FROM
				mst_financeaccountingperiod
				WHERE
				mst_financeaccountingperiod.intId =  '$accountPeriod'
				";	
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$startDate = substr($row['dtmStartingDate'],0,4);
		$closeDate = substr($row['dtmClosingDate'],0,4);
		$sql = "SELECT
				mst_companies.strCode AS company,
				mst_companies.intId,
				mst_locations.intCompanyId,
				mst_locations.strCode AS location,
				mst_locations.intId
				FROM
				mst_companies
				Inner Join mst_locations ON mst_locations.intCompanyId = mst_companies.intId
				WHERE
				mst_locations.intId =  '$locationId' AND
				mst_companies.intId =  '$companyId'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$companyCode = $row['company'];
		$locationCode = $row['location'];
		$invoiceFormat = $companyCode."/".$locationCode."/".$startDate."-".$closeDate."/".$invoiceNumber;
		//$invoiceFormat = $companyCode."/".$startDate."-".$closeDate."/".$invoiceNumber;
		return $invoiceFormat;
	}
//============================================================================================
?>
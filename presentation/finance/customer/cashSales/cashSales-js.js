// JavaScript Document
function functionList()
{
	if(invoRefNo!='')
	{
		$('#frmCashSales #cboSearch').val(invoRefNo);
		$('#frmCashSales #cboSearch').change();
	}
}
var rows = 1;
var trnTax = "";
function insertRow()
{
	var tbl = document.getElementById('tblMainGrid2');	
	rows = tbl.rows.length;
	//tbl.rows[1].cells[5].childNodes[0].nodeValue;
	tbl.insertRow(rows);
	tbl.rows[rows].innerHTML = tbl.rows[rows-1].innerHTML;
	//loadJs();
}
$(document).ready(function() {
	
	// TODO: ===========Add by dulaskshi 2013.03.20===========
	getSupplierList(); 
	
	var id = '';
	$("#frmCashSales").validationEngine();
	$('#frmCashSales #cboCustomer').focus();

$('.delImg').live('click',function(){
	var rowId = $(this).parent().parent().parent().find('tr').length;
	if(rowId!=2)
	$(this).parent().parent().remove();
	callSubTotal();
	callTaxTotal();
});

$('.delImg').css('cursor', 'pointer');

  //permision for add 
  if(intAddx)
  {
 	$('#frmCashSales #butNew').show();
	$('#frmCashSales #butSave').show();
	$('#frmCashSales #butPrint').show();
  }
  //permision for edit 
  if(intEditx)
  {
  	$('#frmCashSales #butSave').show();
	$('#frmCashSales #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
	$('#frmCashSales #butPrint').show();
  }
  //permision for delete
  if(intDeletex)
  {
  	$('#frmCashSales #butDelete').show();
	$('#frmCashSales #cboSearch').removeAttr('disabled');
  }
  //permision for view
  if(intViewx)
  {
	$('#frmCashSales #cboSearch').removeAttr('disabled');
  }
  
  $('#frmCashSales #chkEdit').click(function(){
	  if($('#frmCashSales #chkEdit').attr('checked'))
	  {
		  $("#frmCashSales #txtRate").attr("readonly","");
		  $('#frmCashSales #txtRate').focus();
	  }
	  else
	  {
		  $('#frmCashSales #txtRate').val('');
		  $("#frmCashSales #txtRate").attr("readonly","readonly");
		  $('#frmCashSales #cboCurrency').change();
	  }
  });
///////////////////////////////////////////////////////////////////
$("input[name^=txtQty]").live("keyup", calculate);
$("input[name^=txtUnitPrice]").live("keyup", calculate);
$("input[name^=txtDiscount]").live("keyup", calculate);
///////////////////////////////////////////////////////////////////

///////////////////////////get customer address////////////////////
$('#cboCustomer').change(function(){
	var url = "cashSales-db-get.php?requestType=getCustomerAddress&customerId="+$(this).val();
	var obj = $.ajax({url:url,async:false});
	$('#txtAddress').val(obj.responseText);
});
////////////////////////////////////////////////////////////////////

///////////////////////////get item description////////////////////
$('.item').live('change',function(){
	var url = "cashSales-db-get.php?requestType=getItemDescription&itemId="+$(this).val();
	var obj = $.ajax({url:url,async:false});
	$(this).parent().parent().find('td').eq(2).children().val(obj.responseText);
});
////////////////////////////////////////////////////////////////////

///////////////////////////get tax values///////////////////////////
	$('.taxGroup').live('change',function(){
	var operation = '';
	var amount = $(this).parent().parent().find('td').eq(7).children().val();
	var jsonTaxCode="[ ";
	//var taxCode = $(this).find('option:selected').text();
	var taxId = callTaxProcess($(this).val());
	var arrTax = taxId.split('/');
	//var arrTaxInclusive = taxId.split('/');
	//var arrTaxExclusive = taxId.split('-');

	if(arrTax.length == 1)
	{
		operation = 'Isolated';
		jsonTaxCode += '{ "taxId":"'+taxId+'"},';
	}
	else if(arrTax.length > 1)
	{
		operation = arrTax[1];
		for(var i =0; i < arrTax.length; i=i+2)
		{
			jsonTaxCode += '{ "taxId":"'+arrTax[i]+'"},';
		}
	}

	jsonTaxCode = jsonTaxCode.substr(0,jsonTaxCode.length-1);
	jsonTaxCode += " ]";
	var url = "cashSales-db-get.php?requestType=getTaxValue&arrTaxCode="+jsonTaxCode+"&opType="+operation+"&valAmount="+amount;
	var obj = $.ajax({url:url,async:false});
	var values = obj.responseText.split('/');
	$(this).parent().parent().find('td').eq(10).children().val(values[0]);
	
	if(arrTax.length == 1)
	{
		trnTax="[ ";
		trnTax += '{ "taxId":"'+taxId+'", "taxValue": "'+values[1]+'"},';
		trnTax = trnTax.substr(0,trnTax.length-1);
		trnTax += " ]";
	}
	else if(arrTax.length > 1)
	{
		trnTax="[ ";
		var k = 1;
		for(var i =0; i < arrTax.length; i=i+2)
		{
			trnTax += '{ "taxId":"'+arrTax[i]+'", "taxValue": "'+values[k]+'"},';
			k++;
		}
		trnTax = trnTax.substr(0,trnTax.length-1);
		trnTax += " ]";
	}
	$(this).parent().parent().find('td:eq(10)').attr('id',trnTax);
	callTaxTotal();
});
////////////////////////////////////////////////////////////////////

////////////////////////get exchange rate//////////////////////////
$('#cboCurrency').change(function(){
	var url = "cashSales-db-get.php?requestType=getExchangeRate&currencyId="+$(this).val()+'&exchangeDate='+$('#txtDate').val();
	var obj = $.ajax({url:url,dataType:'json',success:function(json){
		
		$('#rdoBuying').val(json.buyingRate);
		$('#rdoSelling').val(json.sellingRate);
		$('#rdoAverage').val(((eval(json.buyingRate) + eval(json.sellingRate))/2).toFixed(4));
		$('#rdoSelling').click();
		},async:false});
});
///////////////////////////////////////////////////////////////////
$('.rdoRate').click(function(){
  $('#txtRate').val($(this).val());
});
//save button click event
$('#frmCashSales #butSave').click(function(){
if(existingMsgDate == "")
{
	var itemId = "";
	var itemDesc = "";
	var uomId = "";
	var qty = "";
	var unitPrice = "";
	var discount = "";
	var taxGroupId = "";
	var dimensionId = "";
	var amount = "";
	var trnTaxVal	= "";
			
 value="[ ";
	$('#tblMainGrid2 tr:not(:first)').each(function(){
		
		itemId		= $(this).find(".item").val();
		itemDesc 	= $(this).find(".description").val();
		uomId 		= $(this).find(".uom").val();
		qty 		= $(this).find(".qty").val();
		unitPrice 	= $(this).find(".unitPrice").val();
		discount 	= $(this).find(".discount").val();
		taxAmount 	= $(this).find(".taxWith").val();
		taxGroupId 	= $(this).find(".taxGroup").val();
		dimensionId = $(this).find(".dimension").val();
		amount 		= $(this).find(".amount").val();
		trnTaxVal	= $(this).find(".taxVal").attr('id');
		
	value += '{ "itemId":"'+itemId+'", "itemDesc": "'+itemDesc+'", "uomId": "'+uomId+'", "qty": "'+qty+'", "unitPrice": "'+unitPrice+'", "discount": "'+discount+'", "taxGroupId": "'+taxGroupId+'", "taxAmount": "'+taxAmount+'", "dimensionId": "'+dimensionId+'", "amount": "'+amount+'", "trnTaxVal":'+trnTaxVal+'},';
	});
	
	value = value.substr(0,value.length-1);
	value += " ]";

	var requestType = '';
	if ($('#frmCashSales').validationEngine('validate'))   
    { 
		if($('#txtNo').val()=='')
			requestType = 'add';
			
		else
			requestType = 'edit';
		
		var url = "cashSales-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json",
			type:'post',
			data:$("#frmCashSales").serialize()+'&requestType='+requestType+'&cboSearch='+id+'&salesDetails='+value,
			async:false,
			
			success:function(json){
					$('#frmCashSales #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						//$('#frmCashSales').get(0).reset();
						var t=setTimeout("alertx()",1000);
						$('#txtNo').val(json.invoiceNo);
						loadCombo_frmCashSales();
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmCashSales #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
				}		
			});
	}
}
else
{
	$('#frmCashSales #butSave').validationEngine('showPrompt', existingMsgDate,'fail');
	var t=setTimeout("alertx()",5000);
}
});
/////////////////////////////////////////////////////
//// load invoice details //////////////////////////
/////////////////////////////////////////////////////
$('#frmCashSales #cboSearch').click(function(){
   $('#frmCashSales').validationEngine('hide');
});
$('#frmCashSales #cboSearch').change(function(){
$('#frmCashSales').validationEngine('hide');
var url = "cashSales-db-get.php";
if($('#frmCashSales #cboSearch').val()=='')
{
	existingMsgDate = "";
	$('#frmCashSales').get(0).reset();return;	
}
$('#txtNo').val($(this).val());
var httpobj = $.ajax({
	url:url,
	dataType:'json',
	data:'requestType=loadDetails&id='+URLEncode($(this).val()),
	async:false,
	success:function(json)
	{
		//json  = eval('('+json+')');
		$('#frmCashSales #cboCustomer').val(json.customer);
		$('#frmCashSales #txtDate').val(json.date);
		$('#frmCashSales #txtAddress').val(json.address);
		$('#frmCashSales #txtShipTo').val(json.shipTo);
		$('#frmCashSales #cboCurrency').val(json.currency);
		$('#frmCashSales #txtRate').val(json.rate);
		$('#frmCashSales #txtRemarks').val(json.remark);
		$('#frmCashSales #txtPoNo').val(json.poNo);
		$('#frmCashSales #cboPaymentsTerms').val(json.payTerms);
		$('#frmCashSales #cboMarketer').val(json.marketer);
		$('#frmCashSales #txtShipDate').val(json.shipDate);
		$('#frmCashSales #cboShipmentMethod').val(json.shipment);
		$('#frmCashSales #txtMessage').val(json.message);
		$('#frmCashSales #cboChartOfAcc').val(json.chartOfAcc);
		
		//--------------------------------------------------
		$('#tblMainGrid2 >tbody >tr').each(function(){
			if($(this).index()!=0 && $(this).index()!=1 )
			{
				$(this).remove();
			}
		});
		var itemId 		= "";
		var itemDesc 	= "";
		var uom			= "";
		var qty			= "";
		var unitPrice	= "";
		var discount	= "";
		var taxAmount 	= "";
		var tax			= "";
		var dimension 	= "";
		if(json.detailVal!=null)
		{
			var rowId = $('#tblMainGrid2').find('tr').length;
			var tbl = document.getElementById('tblMainGrid2');
			rows = $('#tblMainGrid2').find('tr').length;
			for(var j=0;j<=json.detailVal.length-1;j++)
			{
				itemId		= json.detailVal[j].itemId;
				itemDesc	= json.detailVal[j].itemDesc;
				uom			= json.detailVal[j].uom;
				qty			= json.detailVal[j].qty;
				unitPrice	= json.detailVal[j].unitPrice;
				discount	= json.detailVal[j].discount;
				taxAmount	= json.detailVal[j].taxAmount;
				tax			= json.detailVal[j].taxGroup;
				dimension	= json.detailVal[j].dimension;
				if(j != json.detailVal.length-1)
				{
					tbl.insertRow(rows);
					tbl.rows[rows].innerHTML = tbl.rows[rows-1].innerHTML;
					tbl.rows[rows].cells[1].childNodes[1].value = itemId;
					tbl.rows[rows].cells[2].childNodes[1].value = itemDesc;
					tbl.rows[rows].cells[3].childNodes[1].value = uom;
					tbl.rows[rows].cells[4].childNodes[1].value = qty;
					tbl.rows[rows].cells[5].childNodes[1].value = unitPrice;
					tbl.rows[rows].cells[6].childNodes[1].value = discount;
					tbl.rows[rows].cells[7].childNodes[1].value = ((unitPrice*((100-discount)/100))*qty).toFixed(4);
					//tbl.rows[rows].cells[8].childNodes[1].value = tax;
					$('#tblMainGrid2 >tbody >tr:eq('+rows+')>td:eq(8)').find('.taxGroup').val(tax);
					tbl.rows[rows].cells[9].childNodes[1].value = dimension;
					tbl.rows[rows].cells[10].childNodes[1].value = taxAmount;
					$('#tblMainGrid2 >tbody >tr:eq('+rows+')>td:eq(8)').find('.taxGroup').change();
				}
				else
				{
					tbl.rows[1].cells[1].childNodes[1].value = itemId;
					tbl.rows[1].cells[2].childNodes[1].value = itemDesc;
					tbl.rows[1].cells[3].childNodes[1].value = uom;
					tbl.rows[1].cells[4].childNodes[1].value = qty;
					tbl.rows[1].cells[5].childNodes[1].value = unitPrice;
					tbl.rows[1].cells[6].childNodes[1].value = discount;
					tbl.rows[1].cells[7].childNodes[1].value = ((unitPrice*((100-discount)/100))*qty).toFixed(4);
					//tbl.rows[1].cells[8].childNodes[1].value = tax;
					$('#tblMainGrid2 >tbody >tr:eq(1)>td:eq(8)').find('.taxGroup').val(tax);
					tbl.rows[1].cells[9].childNodes[1].value = dimension;
					tbl.rows[1].cells[10].childNodes[1].value = taxAmount;
					$('#tblMainGrid2 >tbody >tr:eq(1)>td:eq(8)').find('.taxGroup').change();
				}
				//loadJs();
			}
			callSubTotal();
			callTaxTotal();
		}
		else
		{
			
		}
	   //--------------------------------------------------
	}
});
});
//////////// end of load details /////////////////

  	$('#frmCashSales #butNew').click(function(){
	existingMsgDate = "";
	$('#frmCashSales').get(0).reset();
	$('#tblMainGrid2 >tbody >tr').each(function(){
	if($(this).index()!=0 && $(this).index()!=1 )
	{
		$(this).remove();
	}
	});
	loadCombo_frmCashSales();
	$('#frmCashSales #cboCustomer').focus();
	});
	$('#frmCashSales #butDelete').click(function(){
		if($('#frmCashSales #cboSearch').val()=='')
		{
			$('#frmCashSales #butDelete').validationEngine('showPrompt', 'Please select Invoice.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmCashSales #cboSearch option:selected').text()+'" ?',{
			buttons: { Ok: true, Cancel: false },
			callback: function(v,m,f){
			if(v)
			{
					var url = "cashSales-db-set.php";
					var httpobj = $.ajax({
						url:url,
						dataType:'json',
						data:'requestType=delete&cboSearch='+URLEncode($('#frmCashSales #cboSearch').val()),
						async:false,
						success:function(json){
							
							$('#frmCashSales #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
							
							if(json.type=='pass')
							{
								$('#frmCashSales').get(0).reset();
								$('#tblMainGrid2 >tbody >tr').each(function(){
									if($(this).index()!=0 && $(this).index()!=1 )
									{
										$(this).remove();
									}
								});
								loadCombo_frmCashSales();
								var t=setTimeout("alertDelete()",1000);return;
							}	
							var t=setTimeout("alertDelete()",3000);
						}	 
					});
			}
		}
			});	
		}
	});
	
	$('#frmCashSales #butPrint').click(function(){
		if($('#frmCashSales #txtNo').val()=='')
		{
			$('#frmCashSales #butPrint').validationEngine('showPrompt', 'Please select Invoice.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var myurl = 'cashSalesDetails.php?id='+URLEncode($('#frmCashSales #txtNo').val());
    		window.open(myurl); 
		}
	});
	
});

//-------------------------------------------functions area----------------------------------------------------------

////////////////////// calculation ////////////////////////////////
function calculate()
{ 
var quantity =  ($(this).parent().parent().find('td').eq(4).children().val()==''?0:$(this).parent().parent().find('td').eq(4).children().val());

var unitprice  = ($(this).parent().parent().find('td').eq(5).children().val()==''?0:$(this).parent().parent().find('td').eq(5).children().val());

var discount  = ($(this).parent().parent().find('td').eq(6).children().val()==''?0:$(this).parent().parent().find('td').eq(6).children().val());
	
var calAmount = parseFloat(quantity)*(parseFloat(unitprice)*(100-parseFloat(discount))/100);

$(this).parent().parent().find('td').eq(7).children().val(calAmount.toFixed(4));
callSubTotal();
callTotalAmount();
$(this).parent().parent().find(".taxGroup").change();
}
////////////////////////////////////////////////////////////////////
function callTaxProcess(taxGroupId)
{
	var url = "cashSales-db-get.php?requestType=getTaxProcess&taxGroupId="+taxGroupId;
	var obj = $.ajax({url:url,async:false});
	return obj.responseText;
}
function callSubTotal()
{
	var subTotal = 0.00;
	$(".amount").each( function(){
          subTotal += eval($(this).val()==''?0.00:$(this).val());
	});
	$('#txtSubTotal').val(subTotal.toFixed(4));	
}
function callTaxTotal()
{
	var taxTotal = 0.00;
	$(".taxWith").each( function(){
          taxTotal += eval($(this).val()==''?0.00:$(this).val());
	});
	$('#txtTotalTax').val(taxTotal.toFixed(4));
	callTotalAmount();
}
function callTotalAmount()
{
	$('#txtTotal').val((eval($('#txtTotalTax').val()) + eval($('#txtSubTotal').val())).toFixed(4));
}
function loadCombo_frmCashSales()
{
	var url 	= "cashSales-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmCashSales #cboSearch').html(httpobj.responseText);
}
function alertx()
{
	$('#frmCashSales #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmCashSales #butDelete').validationEngine('hide') ;
	$('#frmCashSales #butPrint').validationEngine('hide') ;

}
function loadJs()
{
}

//=========Add by dulaskshi 2013.03.20===========
//====================Get Supplier List==============================
function getSupplierList()
{	
	$ledgerAccId = $('#frmCashSales #cboLedgerAcc').val();
	
	var url = "cashSales-db-get.php?requestType=loadCustomer&ledgerAcc="+$ledgerAccId;
	var httpobj = $.ajax({url:url,async:false})
	$('#frmCashSales #cboCustomer').html(httpobj.responseText);
}
<?php
session_start();
$backwardseperator = "../../../../";
$companyId = $_SESSION['CompanyID'];
$locationId = $_SESSION['CompanyID'];
$intUser  = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
include  	"{$backwardseperator}dataAccess/Connector.php";

$invoiceRefNo = $_REQUEST['id'];

$sql = "SELECT
		fin_customer_cashsales_header.intInvoiceNo,
		fin_customer_cashsales_header.intAccPeriodId,
		fin_customer_cashsales_header.intLocationId,
		fin_customer_cashsales_header.intCompanyId,
		fin_customer_cashsales_header.strReferenceNo,
		fin_customer_cashsales_header.intCustomerId,
		fin_customer_cashsales_header.dtmDate,
		fin_customer_cashsales_header.strAddress,
		fin_customer_cashsales_header.strShipTo,
		fin_customer_cashsales_header.intCurrencyId,
		fin_customer_cashsales_header.dblRate,
		fin_customer_cashsales_header.strRemark,
		fin_customer_cashsales_header.strPoNo,
		fin_customer_cashsales_header.intPaymentsTermsId,
		fin_customer_cashsales_header.intMarketerId,
		fin_customer_cashsales_header.dtmShipDate,
		fin_customer_cashsales_header.intShipmentId,
		fin_customer_cashsales_header.strMessage,
		mst_customer.intId,
		mst_customer.strName AS cusName,
		mst_financecurrency.intId,
		mst_financecurrency.strCode AS currency,
		mst_financepaymentsterms.intId,
		mst_financepaymentsterms.strName AS paymentsTerms,
		sys_users.intUserId,
		sys_users.strUserName,
		mst_shipmentmethod.intId,
		mst_shipmentmethod.strName AS shipmentMethod
		FROM
		fin_customer_cashsales_header
		left outer Join mst_customer ON fin_customer_cashsales_header.intCustomerId = mst_customer.intId
		left outer Join mst_financecurrency ON fin_customer_cashsales_header.intCurrencyId = mst_financecurrency.intId
		left outer Join mst_financepaymentsterms ON fin_customer_cashsales_header.intPaymentsTermsId = mst_financepaymentsterms.intId
		left outer Join sys_users ON fin_customer_cashsales_header.intMarketerId = sys_users.intUserId
		left outer Join mst_shipmentmethod ON fin_customer_cashsales_header.intShipmentId = mst_shipmentmethod.intId
		WHERE
		fin_customer_cashsales_header.strReferenceNo =  '$invoiceRefNo'
		";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$customer 	= $row['cusName'];
	$invoDate 	= $row['dtmDate'];
	$shipTo 	= $row['strShipTo'];
	$currency 	= $row['currency'];
	$poNo 		= $row['strPoNo'];
	$rate		= $row['dblRate'];
	$payTerms	= $row['paymentsTerms'];
	$shipDate	= $row['dtmShipDate'];
	$shipVia 	= $row['shipmentMethod'];
	$marketer	= $row['strUserName'];
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Cash Sales Invoice Report</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="rptPurchaseOrder-js.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:239px;
	top:172px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>

<form id="frmCashSalesDetails" name="frmCashSalesDetails" method="post" action="salesInvoiceDetails.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include '../../../../reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>CASH SALES INVOICE REPORT</strong><strong></strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
  <tr>
    <td colspan="9" align="center" bgcolor="#FFDFCB">
  </tr>
  <tr>
    <td width="5%">&nbsp;</td>
    <td width="15%"><span class="normalfnt"><strong>Invoice No.</strong></span></td>
    <td width="3%" align="center" valign="middle"><strong>:</strong></td>
    <td width="30%"><span class="normalfnt"><?php echo $invoiceRefNo ?></span></td>
    <td width="17%" class="normalfnt"><strong>Customer</strong></td>
    <td width="2%" align="center" valign="middle"><strong>:</strong></td>
    <td width="22%"><span class="normalfnt"><?php echo $customer ?></span></td>
    <td width="3%"><div id="divPoNo" style="display:none"><?php echo $poNo ?>/<?php echo $year ?></div></td>
  <td width="3%"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Date</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $invoDate  ?></span></td>
    <td><span class="normalfnt"><strong>Ship To</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $shipTo  ?></span></td>
    <td class="normalfnt">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Currency</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $currency  ?></span></td>
    <td><span class="normalfnt"><strong>Rate</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $rate  ?></span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
<tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>P.O. No.</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $poNo  ?></span></td>
    <td><span class="normalfnt"><strong> Payment Terms</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo ($payTerms==''?0:$payTerms) ?> days</span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>  
 <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Ship Date</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $shipDate ?></span></td>
    <td><span class="normalfnt"><strong> Shipment Via</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $shipVia ?></span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr> 
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Marketer</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $marketer  ?></span></td>
    <td>&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td width="5%">&nbsp;</td>
        <td colspan="7" class="normalfnt">
          <table width="100%" class="grid" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr class="gridHeader">
              <td width="7%"  height="22">Item</td>
              <td width="10%" >Item Description</td>
              <td width="6%" >UOM</td>
              <td width="9%" >Unit Price</td>
              <td width="7%" >Discount(%)</td>
              <td width="7%" >Tax Code</td>
              <td width="4%" >Qty</td>
              <td width="6%" >Amount</td>
              </tr>
            <?php 
			$totalTax = 0;
			//$operation = "";
	  	  	$sql1 = "SELECT
					fin_customer_cashsales_details.intInvoiceNo,
					fin_customer_cashsales_details.intAccPeriodId,
					fin_customer_cashsales_details.intLocationId,
					fin_customer_cashsales_details.intCompanyId,
					fin_customer_cashsales_details.strReferenceNo,
					fin_customer_cashsales_header.intInvoiceNo,
					fin_customer_cashsales_header.intAccPeriodId,
					fin_customer_cashsales_header.intLocationId,
					fin_customer_cashsales_header.intCompanyId,
					fin_customer_cashsales_header.strReferenceNo,
					fin_customer_cashsales_details.intItem,
					fin_customer_cashsales_details.strItemDesc,
					fin_customer_cashsales_details.intUom,
					fin_customer_cashsales_details.dblQty,
					fin_customer_cashsales_details.dblUnitPrice,
					fin_customer_cashsales_details.dblDiscount,
					fin_customer_cashsales_details.dblTaxAmount,
					fin_customer_cashsales_details.intTaxGroupId,
					fin_customer_cashsales_details.intDimensionId,
					mst_financetaxgroup.intId,
					mst_financetaxgroup.strCode AS taxCode,
					mst_financetaxgroup.strProcess,
					mst_financedimension.intId,
					mst_financedimension.strName,
					fin_customer_cashsales_header.dblRate,
					mst_financecustomeritem.intId,
					mst_financecustomeritem.strName AS itemName,
					mst_units.intId,
					mst_units.strName AS uom
					FROM
					fin_customer_cashsales_details
					left outer Join fin_customer_cashsales_header ON fin_customer_cashsales_details.intInvoiceNo = fin_customer_cashsales_header.intInvoiceNo AND fin_customer_cashsales_details.intAccPeriodId = fin_customer_cashsales_header.intAccPeriodId AND fin_customer_cashsales_details.intLocationId = fin_customer_cashsales_header.intLocationId AND fin_customer_cashsales_details.intCompanyId = fin_customer_cashsales_header.intCompanyId AND fin_customer_cashsales_details.strReferenceNo = fin_customer_cashsales_header.strReferenceNo
					left outer Join mst_financetaxgroup ON fin_customer_cashsales_details.intTaxGroupId = mst_financetaxgroup.intId
					left outer Join mst_financedimension ON fin_customer_cashsales_details.intDimensionId = mst_financedimension.intId
					left outer Join mst_financecustomeritem ON fin_customer_cashsales_details.intItem = mst_financecustomeritem.intId
					left outer Join mst_units ON fin_customer_cashsales_details.intUom = mst_units.intId
					WHERE
					fin_customer_cashsales_details.strReferenceNo =  '$invoiceRefNo'
					";
			$result1 = $db->RunQuery($sql1);

			$totQty=0;
			$totAmmount=0;
		while($row=mysqli_fetch_array($result1))
		{
			$subAmount = (($row['dblUnitPrice'])*((100-$row['dblDiscount'])/100))*$row['dblQty'];
			$totalTax = $totalTax + $row['dblTaxAmount'];		
	  ?>
	  <tr class="normalfnt"  bgcolor="#FFFFFF">
   	  <td class="normalfnt">&nbsp;<?php echo $row['itemName']; ?>&nbsp;</td>
      <td class="normalfnt" >&nbsp;<?php echo $row['strItemDesc']?>&nbsp;</td>
      <td class="normalfntMid" >&nbsp;<?php echo $row['uom'] ?>&nbsp;</td>
      <td class="normalfntRight" >&nbsp;<?php echo $row['dblUnitPrice'] ?>&nbsp;</td>
      <td class="normalfntRight" >&nbsp;<?php echo $row['dblDiscount'] ?>&nbsp;</td>
      <td class="normalfntMid" >&nbsp;<?php echo $row['taxCode'] ?>&nbsp;</td>
      <td class="normalfntRight" >&nbsp;<?php echo $row['dblQty'] ?>&nbsp;</td>
      <td class="normalfntRight" >&nbsp;<?php echo number_format($subAmount, 2) ?>&nbsp;</td>
</tr>
      <?php 
			$totQty+=$row['dblQty'];
			$totAmmount+=(($row['dblUnitPrice'])*((100-$row['dblDiscount'])/100))*$row['dblQty'];
			}
	  ?>
            <tr class="normalfnt"  bgcolor="#CCCCCC">
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $totQty ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo number_format($totAmmount, 2) ?>&nbsp;</td>
              </tr>
            <tr class="normalfnt"  bgcolor="#FFFFEC">
              <td colspan="7" class="normalfntRight" >Total Tax Amount</td>
              <td class="normalfntRight" >&nbsp;<?php echo number_format($totalTax, 2) ?>&nbsp;</td>
            </tr>
            <tr class="normalfnt"  bgcolor="#F5F5F5">
              <td colspan="7" class="normalfntRight" >Total Amount</td>
              <td class="normalfntRight" >&nbsp;<?php echo number_format(($totalTax + $totAmmount), 2) ?>&nbsp;</td>
            </tr>
            </table>
          </td>
        <td width="6%">&nbsp;</td>
        </tr>
      </table>
    </td>
</tr>
<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
</body>
</html>
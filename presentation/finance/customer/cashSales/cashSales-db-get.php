<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$companyId = $row['intCompanyId']; 
	}
	/////////// cash sales invoice load part /////////////////////
	if($requestType=='loadCombo')
	{
		$sql = "SELECT
				fin_customer_cashsales_header.strReferenceNo,
				fin_customer_cashsales_header.intInvoiceNo,
				mst_customer.strName
				FROM
				fin_customer_cashsales_header
				Inner Join mst_customer ON fin_customer_cashsales_header.intCustomerId = mst_customer.intId
				WHERE
				fin_customer_cashsales_header.intCompanyId =  '$companyId' AND
				fin_customer_cashsales_header.intDeleteStatus =  '0'
				ORDER BY intInvoiceNo DESC
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['strReferenceNo']."\">".$row['strReferenceNo']." - ".$row['strName']."</option>";
		}
		echo $html;
	}
	
	//===========Add by dulakshi 2013.03.20=========
	else if($requestType=='loadCustomer')
	{
		$ledAcc  = $_REQUEST['ledgerAcc'];		
							
		$condition == "";
			if($ledAcc != "")
			{	
				$condition .= "AND mst_financecustomeractivate.intChartOfAccountId =  '$ledAcc'	";			
			}			
			
			$sql = "SELECT
						mst_customer.intId,
						mst_customer.strName,
						mst_financecustomeractivate.intCompanyId
					FROM
						mst_customer
						Inner Join mst_financecustomeractivate ON mst_customer.intId = mst_financecustomeractivate.intCustomerId
					WHERE
						mst_customer.intStatus =  '1' AND
						mst_financecustomeractivate.intCompanyId =  '$companyId' ".$condition ." order by mst_customer.strName ASC";
							
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
	}
	//==============================================
	
	else if($requestType=='loadDetails')
	{
		$id  = $_REQUEST['id'];
		
		//-------------------------------------------------------------
		$sql = "SELECT
				fin_customer_cashsales_details.intInvoiceNo,
				fin_customer_cashsales_details.intAccPeriodId,
				fin_customer_cashsales_details.intLocationId,
				fin_customer_cashsales_details.intCompanyId,
				fin_customer_cashsales_details.strReferenceNo,
				fin_customer_cashsales_details.intItem,
				fin_customer_cashsales_details.strItemDesc,
				fin_customer_cashsales_details.intUom,
				fin_customer_cashsales_details.dblQty,
				fin_customer_cashsales_details.dblUnitPrice,
				fin_customer_cashsales_details.dblDiscount,
				fin_customer_cashsales_details.dblTaxAmount,
				fin_customer_cashsales_details.intTaxGroupId,
				fin_customer_cashsales_details.intDimensionId,
				fin_customer_cashsales_header.intDeleteStatus
				FROM
				fin_customer_cashsales_details
				Inner Join fin_customer_cashsales_header ON fin_customer_cashsales_details.strReferenceNo = fin_customer_cashsales_header.strReferenceNo
				WHERE
				fin_customer_cashsales_details.strReferenceNo =  '$id' AND
				fin_customer_cashsales_header.intDeleteStatus =  '0'
				";
		$result = $db->RunQuery($sql);
		$arrDetail;
		while($row=mysqli_fetch_array($result))
		{
			$val['itemId'] 		= $row['intItem'];
			$val['itemDesc'] 	= $row['strItemDesc'];
			$val['uom'] 		= $row['intUom'];
			$val['qty'] 		= $row['dblQty'];
			$val['unitPrice'] 	= $row['dblUnitPrice'];
			$val['discount'] 	= $row['dblDiscount'];
			$val['taxAmount'] 	= $row['dblTaxAmount'];
			$val['taxGroup'] 	= $row['intTaxGroupId'];
			$val['dimension'] 	= $row['intDimensionId'];
			$arrDetail[] = $val;
		}
		$response['detailVal'] = $arrDetail;
		//-----------------------------------------------------------
		
		$sql   = "SELECT
				fin_customer_cashsales_header.intInvoiceNo,
				fin_customer_cashsales_header.intAccPeriodId,
				fin_customer_cashsales_header.intLocationId,
				fin_customer_cashsales_header.intCompanyId,
				fin_customer_cashsales_header.strReferenceNo,
				fin_customer_cashsales_header.intCustomerId,
				fin_customer_cashsales_header.dtmDate,
				fin_customer_cashsales_header.strAddress,
				fin_customer_cashsales_header.strShipTo,
				fin_customer_cashsales_header.intCurrencyId,
				fin_customer_cashsales_header.dblRate,
				fin_customer_cashsales_header.strRemark,
				fin_customer_cashsales_header.strPoNo,
				fin_customer_cashsales_header.intPaymentsTermsId,
				fin_customer_cashsales_header.intMarketerId,
				fin_customer_cashsales_header.dtmShipDate,
				fin_customer_cashsales_header.intShipmentId,
				fin_customer_cashsales_header.strMessage,
				fin_customer_cashsales_header.intChartOfAccountId
				FROM
				fin_customer_cashsales_header
				WHERE
				fin_customer_cashsales_header.strReferenceNo =  '$id' AND
				fin_customer_cashsales_header.intDeleteStatus =  '0'
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['customer'] 	= $row['intCustomerId'];
			$response['date'] 		= $row['dtmDate'];
			$response['address'] 	= $row['strAddress'];
			$response['shipTo'] 	= $row['strShipTo'];
			$response['currency']	= $row['intCurrencyId'];
			$response['rate'] 		= $row['dblRate'];
			$response['remark'] 	= $row['strRemark'];
			$response['poNo'] 		= $row['strPoNo'];
			$response['payTerms'] 	= $row['intPaymentsTermsId'];
			$response['marketer'] 	= $row['intMarketerId'];
			$response['shipDate'] 	= $row['dtmShipDate'];
			$response['shipment'] 	= $row['intShipmentId'];
			$response['message'] 	= $row['strMessage'];
			$response['chartOfAcc'] = $row['intChartOfAccountId'];
		}
		echo json_encode($response);
	}
	else if($requestType=='getExchangeRate')
	{
		$currencyId  	= $_REQUEST['currencyId'];
		$exchangeDate	= $_REQUEST['exchangeDate'];
		
		$sql = "SELECT
					mst_financeexchangerate.dblSellingRate,
					mst_financeexchangerate.dblBuying
				FROM mst_financeexchangerate
				WHERE
					mst_financeexchangerate.intCurrencyId 	=  '$currencyId' AND
					mst_financeexchangerate.dtmDate 		=  '$exchangeDate'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		
		if(mysqli_num_rows($result)>0)
		{
			$arrValue['sellingRate'] 	= $row['dblSellingRate'];
			$arrValue['buyingRate'] 	= $row['dblBuying'];
		}
		else
		{
			$arrValue['sellingRate'] 	= "";
			$arrValue['buyingRate'] 	= "";
		}
		echo json_encode($arrValue);
	}
	else if($requestType=='getCustomerAddress')
	{
		$customerId  = $_REQUEST['customerId'];
		
		$sql = "SELECT 	mst_customer.strAddress FROM mst_customer	WHERE mst_customer.intId =  '$customerId'";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		echo $row['strAddress'];
	}
	else if($requestType=='getItemDescription')
	{
		$itemId  = $_REQUEST['itemId'];
		
		$sql = "SELECT
				mst_financecustomeritem.intId,
				mst_financecustomeritem.strRemark
				FROM
				mst_financecustomeritem
				WHERE
				mst_financecustomeritem.intId =  '$itemId'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		echo $row['strRemark'];
	}
	else if($requestType=='getTaxValue')
	{
		$operation  = $_REQUEST['opType'];
		$amount  = $_REQUEST['valAmount'];
		$taxCodes = json_decode($_REQUEST['arrTaxCode'], true);
		
		if(count($taxCodes) != 0)
		{
			foreach($taxCodes as $taxCode)
			{
				$codeValues[] = callTaxValue($taxCode['taxId']);
			}
		}
		if(count($codeValues) > 1)
		{
			if($operation == 'Inclusive')
			{
				$firstVal = ($amount*$codeValues[0])/100;
				$withTaxVal = $firstVal + ((($amount+$firstVal)*$codeValues[1])/100);
				$val1 = ($amount*$codeValues[0])/100;
				$val2 = ((($amount+$firstVal)*$codeValues[1])/100);
			}
			else if($operation == 'Exclusive')
			{
				$withTaxVal = ($amount*($codeValues[0] + $codeValues[1]))/100;
				$val1 = ($amount*$codeValues[0])/100;
				$val2 = ($amount*$codeValues[1])/100;
			}
		}
		else if(count($codeValues) == 1 && $operation == 'Isolated')
		{
			$withTaxVal = ($amount*$codeValues[0])/100;
			$val1 = ($amount*$codeValues[0])/100;
		}
		echo $withTaxVal."/".$val1."/".$val2;
	}
	else if($requestType=='getTaxProcess')
	{
		$taxGrpId  = $_REQUEST['taxGroupId'];
		
		$sql = "SELECT
				mst_financetaxgroup.intId,
				mst_financetaxgroup.strProcess
				FROM
				mst_financetaxgroup
				WHERE
				mst_financetaxgroup.intId =  '$taxGrpId'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		echo $row['strProcess'];
	}
	function callTaxValue($taxId)
	{
		global $db;
		$sql = "SELECT
				mst_financetaxisolated.intId,
				mst_financetaxisolated.strCode,
				mst_financetaxisolated.dblRate
				FROM
				mst_financetaxisolated
				WHERE
				mst_financetaxisolated.intId = '$taxId'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$taxVal = $row['dblRate'];	
		return $taxVal;
	}
?>
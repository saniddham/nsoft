<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	$companyId	= $_SESSION["headCompanyId"];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	///
	/////////// type of print load part /////////////////////
	if($requestType=='loadCombo')
	{
		$sql = "SELECT
				fin_customer_advancereceived_header.strReceiptNo,
				mst_customer.strName,
				IFNULL(CONCAT(' - ',fin_customer_advancereceived_header.strPerfInvoiceNo),'') AS refNo
				FROM
				fin_customer_advancereceived_header
				Inner Join mst_customer ON fin_customer_advancereceived_header.intCustomer = mst_customer.intId
				WHERE
				fin_customer_advancereceived_header.intStatus =  '1'
				AND fin_customer_advancereceived_header.intCompanyId =  '$companyId'
				ORDER BY
				fin_customer_advancereceived_header.strReceiptNo DESC
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['strReceiptNo']."\">".$row['strReceiptNo'].$row['refNo']." (".$row['strName'].") "."</option>";
		}
		echo $html;
	}
	
	//===========Add by dulakshi 2013.03.20=========
	else if($requestType=='loadCustomer')
	{
		$ledAcc  = $_REQUEST['ledgerAcc'];		
							
		$condition == "";
			if($ledAcc != "")
			{	
				$condition .= "AND mst_financecustomeractivate.intChartOfAccountId =  '$ledAcc'	";			
			}			
			
			$sql = "SELECT
						mst_customer.intId,
						mst_customer.strName,
						mst_financecustomeractivate.intCompanyId
					FROM
						mst_customer
						Inner Join mst_financecustomeractivate ON mst_customer.intId = mst_financecustomeractivate.intCustomerId
					WHERE
						mst_customer.intStatus =  '1' AND
						mst_financecustomeractivate.intCompanyId =  '$companyId' ".$condition ." order by mst_customer.strName ASC";
							
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
	}
	//==============================================
	
	else if($requestType=='loadExchangeRates')
	{
		$currency  = $_REQUEST['currency'];
		$date  = $_REQUEST['date'];
		$sql = "SELECT
				mst_financeexchangerate.dblBuying, 
				mst_financeexchangerate.dblSellingRate  
				FROM mst_financeexchangerate
				WHERE
				mst_financeexchangerate.intCurrencyId =  '$currency' and mst_financeexchangerate.dtmDate =  '$date'";
		$result = $db->RunQuery($sql);
		
		$response['sellingRate'] = '';
		$response['buyingRate'] = '';
		while($row=mysqli_fetch_array($result))
		{
			$response['sellingRate'] = $row['dblSellingRate'];
			$response['buyingRate'] = $row['dblBuying'];
		}
		echo json_encode($response);
	}
	
	
?>
<?php
session_start();
$backwardseperator = "../../../../";
$locationId = $_SESSION['CompanyID'];
$intUser  = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
include  	"{$backwardseperator}dataAccess/Connector.php";

include_once "../../commanFunctions/Converter.php";

$sql = "SELECT DISTINCT
		mst_locations.intCompanyId,
		mst_locations.strName,
		mst_companies.strVatNo,
		mst_companies.strSVatNo
		FROM
		mst_locations
		Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
		WHERE
		mst_locations.intId =  ".$_SESSION["CompanyID"]."";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$companyId = $row['intCompanyId'];
	$companyVat = $row['strVatNo'];
	$companySvat = $row['strSVatNo']; 
	$companyName = $row['strName']; 
}
$invoiceRefNo = $_REQUEST['id'];

$sql = "SELECT
		fin_customer_debitnoteinvoice_header.intInvoiceNo,
		fin_customer_debitnoteinvoice_header.intAccPeriodId,
		fin_customer_debitnoteinvoice_header.intLocationId,
		fin_customer_debitnoteinvoice_header.intCompanyId,
		fin_customer_debitnoteinvoice_header.strReferenceNo,
		fin_customer_debitnoteinvoice_header.intCustomerId,
		fin_customer_debitnoteinvoice_header.dtmDate,
		fin_customer_debitnoteinvoice_header.strInvoiceType,
		fin_customer_debitnoteinvoice_header.strAddress,
		fin_customer_debitnoteinvoice_header.strShipTo,
		fin_customer_debitnoteinvoice_header.intCurrencyId,
		fin_customer_debitnoteinvoice_header.dblRate,
		fin_customer_debitnoteinvoice_header.strRemark,
		fin_customer_debitnoteinvoice_header.strPoNo,
		fin_customer_debitnoteinvoice_header.intPaymentsTermsId,
		fin_customer_debitnoteinvoice_header.intMarketerId,
		fin_customer_debitnoteinvoice_header.dtmShipDate,
		fin_customer_debitnoteinvoice_header.intShipmentId,
		fin_customer_debitnoteinvoice_header.strMessage,
		mst_customer.intId,
		mst_customer.strName AS cusName,
		mst_customer.strVatNo,
		mst_customer.strSVatNo,
		mst_customer.strContactPerson,
		mst_financecurrency.intId,
		mst_financecurrency.strCode AS currency,
		mst_financecurrency.strSymbol,
		mst_financepaymentsterms.intId,
		mst_financepaymentsterms.strName AS paymentsTerms,
		sys_users.intUserId,
		sys_users.strUserName,
		mst_shipmentmethod.intId,
		mst_shipmentmethod.strName AS shipmentMethod,
		fin_customer_debitnoteinvoice_header.intCreator,
		user1.intUserId,
		user1.strUserName AS creater,
		user2.strUserName AS modifyer
		FROM
		fin_customer_debitnoteinvoice_header
		Left Outer Join mst_customer ON fin_customer_debitnoteinvoice_header.intCustomerId = mst_customer.intId
		Left Outer Join mst_financecurrency ON fin_customer_debitnoteinvoice_header.intCurrencyId = mst_financecurrency.intId
		Left Outer Join mst_financepaymentsterms ON fin_customer_debitnoteinvoice_header.intPaymentsTermsId = mst_financepaymentsterms.intId
		Left Outer Join sys_users ON fin_customer_debitnoteinvoice_header.intMarketerId = sys_users.intUserId
		Left Outer Join mst_shipmentmethod ON fin_customer_debitnoteinvoice_header.intShipmentId = mst_shipmentmethod.intId
		Left Outer Join sys_users AS user1 ON fin_customer_debitnoteinvoice_header.intCreator = user1.intUserId
		Left Outer Join sys_users AS user2 ON fin_customer_debitnoteinvoice_header.intModifyer = user2.intUserId
		WHERE
		fin_customer_debitnoteinvoice_header.strReferenceNo =  '$invoiceRefNo'
		";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$customer 		= $row['cusName'];
	$invoAddress	 = $row['strAddress'];
	$invoDate 		= $row['dtmDate'];
	$invoType 		= $row['strInvoiceType'];
	$shipTo 		= $row['strShipTo'];
	$currency 		= $row['currency'];
	$symbol 		= $row['strSymbol'];
	$poNo 			= $row['strPoNo'];
	$rate			= $row['dblRate'];
	$payTerms		= $row['paymentsTerms'];
	$shipDate		= $row['dtmShipDate'];
	$shipVia 		= $row['shipmentMethod'];
	$marketer		= $row['strUserName'];
	$vatNo			= $row['strVatNo'];
	$svatNo			= $row['strSVatNo'];
	$attention		= $row['strContactPerson'];
	$creater		= $row['creater'];
	$modifyer		= $row['modifyer'];
}
if($invoType == 'Commercial')
{
	$invoiceType = "Commercial Debit Note Invoice";
}
else if($invoType == 'Tax')
{
	$invoiceType = "Tax Debit Note Invoice";
}
else if($invoType == 'SVAT')
{
	$invoiceType = "Suspended Tax Debit Note Invoice";
}
else if($invoType == 'NFE')
{
	$invoiceType = "NFE Suspended Tax Debit Note Invoice";
}

$sql = "SELECT
		DISTINCT fin_customer_debitnoteinvoice_details.intDimensionId,
		mst_financedimension.strName
		FROM
		fin_customer_debitnoteinvoice_details
		Inner Join mst_financedimension ON fin_customer_debitnoteinvoice_details.intDimensionId = mst_financedimension.intId
		WHERE
		fin_customer_debitnoteinvoice_details.intCompanyId =  '$companyId' AND
		fin_customer_debitnoteinvoice_details.strReferenceNo =  '$invoiceRefNo'
		";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$dimList 	.=  $row['strName']."  ";
}
?>
<?php
function convert_digit_to_words($no)
{   

$words = array('0'=> 'Zero' ,'1'=> 'One' ,'2'=> 'Two' ,'3' => 'Three','4' => 'Four','5' => 'Five','6' => 'Six','7' => 'Seven','8' => 'Eight','9' => 'Nine','10' => 'Ten','11' => 'Eleven','12' => 'Twelve','13' => 'Thirteen','14' => 'Fourteen','15' => 'Fifteen','16' => 'Sixteen','17' => 'Seventeen','18' => 'Eighteen','19' => 'Nineteen','20' => 'Twenty','30' => 'Thirty','40' => 'Forty','50' => 'Fifty','60' => 'Sixty','70' => 'Seventy','80' => 'Eighty','90' => 'Ninty','100' => 'Hundred','1000' => 'Thousand','100000' => 'Lakh','10000000' => 'Crore');
 
$cash=(int)$no;
$decpart = $no - $cash;

$decpart=sprintf("%01.2f",$decpart);

$decpart1=substr($decpart,2,1);
$decpart2=substr($decpart,3,1);

$decimalstr='';

if($decpart>0)
{
 $decimalstr.="and ".$words[$decpart1]." ".$words[$decpart2]." ". "Cents";
}
 
    if($no == 0)
        return ' ';
    else {
    $novalue='';
    $highno=$no;
    $remainno=0;
    $value=100;
    $value1=1000;       
            while($no>=100)    {
                if(($value <= $no) &&($no  < $value1))    {
                $novalue=$words["$value"];
                $highno = (int)($no/$value);
                $remainno = $no % $value;
                break;
                }
                $value= $value1;
                $value1 = $value * 100;
            }       
          if(array_key_exists("$highno",$words))
              return $words["$highno"]." ".$novalue." ".convert_digit_to_words($remainno).$decimalstr;
          else {
             $unit=$highno%10;
             $ten =(int)($highno/10)*10;            
             return $words["$ten"]." ".$words["$unit"]." ".$novalue." ".convert_digit_to_words($remainno
             ).$decimalstr;
           }
    }
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Debit Note Invoice Report</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:239px;
	top:172px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
.borderLine
{
	border: solid 1px black;
}
.borderLineIn
{
	border: solid 0.5px black;
	border-color:#999
}
</style>

<style>
    /*@media screen{thead{display:none;}}*/
    @media print{thead{display:table-header-group; margin-bottom:2px;}}
    @page{margin-top:1cm;margin-left:1cm;margin-right:1cm;margin-bottom:1.5cm;}}
</style>

</head>
<body>

<form id="frmDebitNoteInvoiceDetails" name="frmDebitNoteInvoiceDetails" method="post" action="debitNoteInvoiceDetails.php">
<div align="center">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
<thead>
<th colspan="6">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<th colspan="3"></th>
</tr>
<tr>
<td width="10%"></td>
<td width="80%" height="80" valign="top"><strong><?php echo $invoiceType?></strong><br /><?php include '../../../../reportHeader.php'?></td>
<td width="10%"></td>
</tr>
<tr>
<td colspan="3"></td>
</tr>
</table>
</th>
</tr>
<tr>
  <th>
  <table width="100%">
  <tr>
    <td colspan="6" align="center" bgcolor="#DBDBDB">
  </tr>
  <tr>
    <td width="34%" height="21" align="left">
    <span class="normalfnt"><strong>To:</strong><br /></span><span class="normalfnt"><?php echo $customer ?><br />
    <?php echo $invoAddress ?></span></td>
    <td align="left">&nbsp;</td>
    <td align="left">&nbsp;</td>
    <td colspan="3" class="normalfnt" align="left"><strong>Invoice No. :</strong> <?php echo $invoiceRefNo ?></td>
   </tr>
  <tr>
    <td class="normalfnt"><strong>P.O.:</strong> <?php echo $poNo ?></td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="3" align="left"><span class="normalfnt"><strong>Invoice Date :</strong> <?php echo $invoDate ?></span></td>
  </tr>
  <tr>
    <td class="normalfnt"><strong>Attention:</strong> <?php echo $attention ?></td>
    <td width="19%" align="left" valign="middle">&nbsp;</td>
    <td width="4%">&nbsp;</td>
    <td colspan="3" align="left"><span class="normalfnt"><strong>Order No. :</strong></span></td>
   </tr>
   <tr>
    <td class="normalfnt"><strong>Customer VAT No:</strong> <?php echo $vatNo ?></td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="3" align="left"><span class="normalfnt"><!--<?php echo $shipDate ?>--><strong>Company VAT No. :</strong> <?php echo $companyVat ?></span></td>
    </tr>
   <tr>
    <td class="normalfnt"><strong>Customer SVAT No:</strong> <?php echo $svatNo ?></td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="3" align="left"><span class="normalfnt"><strong>Company SVAT No. :</strong> <?php echo $companySvat ?></span></td>
    </tr> 
   <tr>
     <td height="21" class="normalfnt">&nbsp;</td>
     <td align="center" valign="middle">&nbsp;</td>
     <td>&nbsp;</td>
     <td width="25%">&nbsp;</td>
     <td width="8%" align="center" valign="middle">&nbsp;</td>
     <td width="10%">&nbsp;</td>
   </tr>
  </table>
  </th>
</tr>
</thead>
<tbody>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td colspan="7" class="normalfnt">
        <table width="100%">
        <tr>
        <td width="100%" style="vertical-align:top" >
          <table width="100%" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr>
              <td width="184"  height="22" class="normalfnt borderLine"><strong>Name</strong></td>
              <td width="302" class="normalfnt borderLine"><strong>Description</strong></td>
              <td width="134" class="normalfntMid borderLine"><strong>Unit</strong></td>
              <td width="120" class="normalfntRight borderLine"><strong>Quantity</strong></td>
              <td width="236" class="normalfntRight borderLine"><strong>Unit Price(Rs\Us $)</strong></td>
              <!--<td width="7%" >Discount(%)</td>-->
             <!-- <td width="7%" >Tax Code</td>-->
              <td width="118" class="normalfntRight borderLine"><strong>Total Value</strong></td>
              </tr>
            <?php 
			$totalTax = 0;
	  	  	$sql1 = "SELECT
					fin_customer_debitnoteinvoice_details.intInvoiceNo,
					fin_customer_debitnoteinvoice_details.intAccPeriodId,
					fin_customer_debitnoteinvoice_details.intLocationId,
					fin_customer_debitnoteinvoice_details.intCompanyId,
					fin_customer_debitnoteinvoice_details.strReferenceNo,
					fin_customer_debitnoteinvoice_header.intInvoiceNo,
					fin_customer_debitnoteinvoice_header.intAccPeriodId,
					fin_customer_debitnoteinvoice_header.intLocationId,
					fin_customer_debitnoteinvoice_header.intCompanyId,
					fin_customer_debitnoteinvoice_header.strReferenceNo,
					fin_customer_debitnoteinvoice_details.intItem,
					fin_customer_debitnoteinvoice_details.strStyleNo,
					fin_customer_debitnoteinvoice_details.strGraphicNo,
					fin_customer_debitnoteinvoice_details.strLineItem,
					fin_customer_debitnoteinvoice_details.strCusLocation,
					fin_customer_debitnoteinvoice_details.strItemDesc,
					fin_customer_debitnoteinvoice_details.intUom,
					fin_customer_debitnoteinvoice_details.dblQty,
					fin_customer_debitnoteinvoice_details.dblUnitPrice,
					fin_customer_debitnoteinvoice_details.dblDiscount,
					fin_customer_debitnoteinvoice_details.dblTaxAmount,
					fin_customer_debitnoteinvoice_details.intTaxGroupId,
					fin_customer_debitnoteinvoice_details.intDimensionId,
					mst_financetaxgroup.intId,
					mst_financetaxgroup.strCode AS taxCode,
					mst_financetaxgroup.strProcess,
					mst_financedimension.intId,
					mst_financedimension.strName,
					fin_customer_debitnoteinvoice_header.dblRate,
					mst_financecustomeritem.intId,
					mst_financecustomeritem.strName AS itemName,
					mst_units.intId,
					mst_units.strName AS uom
					FROM
					fin_customer_debitnoteinvoice_details
					left outer Join fin_customer_debitnoteinvoice_header ON fin_customer_debitnoteinvoice_details.intInvoiceNo = fin_customer_debitnoteinvoice_header.intInvoiceNo AND fin_customer_debitnoteinvoice_details.intAccPeriodId = fin_customer_debitnoteinvoice_header.intAccPeriodId AND fin_customer_debitnoteinvoice_details.intLocationId = fin_customer_debitnoteinvoice_header.intLocationId AND fin_customer_debitnoteinvoice_details.intCompanyId = fin_customer_debitnoteinvoice_header.intCompanyId AND fin_customer_debitnoteinvoice_details.strReferenceNo = fin_customer_debitnoteinvoice_header.strReferenceNo
					left outer Join mst_financetaxgroup ON fin_customer_debitnoteinvoice_details.intTaxGroupId = mst_financetaxgroup.intId
					left outer Join mst_financedimension ON fin_customer_debitnoteinvoice_details.intDimensionId = mst_financedimension.intId
					left outer Join mst_financecustomeritem ON fin_customer_debitnoteinvoice_details.intItem = mst_financecustomeritem.intId
					left outer Join mst_units ON fin_customer_debitnoteinvoice_details.intUom = mst_units.intId
					WHERE
					fin_customer_debitnoteinvoice_details.strReferenceNo =  '$invoiceRefNo'
					";
			$result1 = $db->RunQuery($sql1);

			$totQty=0;
			$totAmmount=0;
		while($row=mysqli_fetch_array($result1))
		{
			$subAmount = (($row['dblUnitPrice'])*((100-$row['dblDiscount'])/100))*$row['dblQty'];
			$totalTax = $totalTax + $row['dblTaxAmount'];
	  ?>
	  <tr class="normalfnt borderLineIn"  bgcolor="#FFFFFF">
   	  <td class="normalfnt borderLineIn" height="50">&nbsp;<?php echo $row['itemName']; ?>&nbsp;</td>
      <td class="normalfnt borderLineIn"><?php echo "Style - ".$row['strStyleNo']."<br/> Graphic - ".$row['strGraphicNo']."<br/> Line - ".$row['strLineItem']."<br/> Location - ".$row['strCusLocation']."<br/> Desc. - ".$row['strItemDesc']."<br/>"; ?>&nbsp;</td>
      <td class="normalfntMid borderLineIn" >&nbsp;<?php echo $row['uom'] ?>&nbsp;</td>
      <td class="normalfntRight borderLineIn" >&nbsp;<?php echo $row['dblQty'] ?>&nbsp;</td>
      <td class="normalfntRight borderLineIn" >&nbsp;<?php echo $row['dblUnitPrice'] ?>&nbsp;</td>
      <!--<td class="normalfntRight" >&nbsp;<?php echo $row['dblDiscount'] ?>&nbsp;</td>-->
      <!--<td class="normalfntMid" >&nbsp;<?php echo $row['taxCode'] ?>&nbsp;</td>-->
      <td class="normalfntRight borderLineIn" >&nbsp;<?php echo number_format($subAmount, 2) ?>&nbsp;</td>
</tr>
      <?php 
			$totQty+=$row['dblQty'];
			$totAmmount+=(($row['dblUnitPrice'])*((100-$row['dblDiscount'])/100))*$row['dblQty'];
			}
	  ?>
            </table>
          </td>
        </tr>
      </table>
      </td>
      </tr>
      </table>
    </td>
</tr>
<!--<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>-->
<tr height="40">
  <td align="center" class="normalfntMid">&nbsp;</td>
</tr>
</tbody>
<tr height="40">
  <td align="center" class="normalfntMid">
  <table width="100%">
  <tr>
    <td width="29%" align="left" class="normalfnt">&nbsp;</td>
    <td width="18%" align="center" class="normalfntMid">&nbsp;</td>
    <td colspan="2" align="right" class="normalfntRight">&nbsp;
      <?php 
	if($invoType != 'SVAT' || $invoType != 'NFE')
	{
		echo "Total Amount Exclude Tax: ".number_format($totAmmount, 2);
	}
	?>&nbsp;</td>
    </tr>
  <tr>
    <td align="left" class="normalfnt">Total Invoice Value</td>
    <td align="center" class="normalfntMid">&nbsp;</td>
    <td colspan="2" align="right" class="normalfntRight">&nbsp;
	<?php
	if($invoType != 'SVAT' || $invoType != 'NFE')
	{ 
		echo "Total Tax Amount: ".number_format($totalTax, 2);
	}
	?>&nbsp;</td>
    </tr>
  <tr>
    <td colspan="3" align="left" class="normalfnt"><strong><?php echo $currency ?>&nbsp;
	<?php
	if($invoType == 'SVAT' || $invoType == 'NFE')
	{
		//echo convert_digit_to_words($totAmmount);
		$val = $totAmmount;
        $inWord= convert_number($val);   
        $sence=explode(".",number_format($val,2));
        $sent=convert_number($sence[1]);
        if(strlen($sence[1])>0)
		{
        	$inWord=$inWord." and ".$sent." cents";
        }
       echo $inWord;
	}
	else
	{
		//echo convert_digit_to_words($totalTax + $totAmmount);
		$val = $totalTax + $totAmmount;
        $inWord= convert_number($val);   
        $sence=explode(".",number_format($val,2));
        $sent=convert_number($sence[1]);
        if(strlen($sence[1])>0)
		{
        	$inWord=$inWord." and ".$sent." cents";
        }
       echo $inWord;
	}
	?> Only</strong></td>
    <td width="18%" align="right" class="normalfntRight">&nbsp;
	<?php 
	if($invoType == 'SVAT' || $invoType == 'NFE')
	{
		echo number_format(($totAmmount), 2);
	}
	else
	{
		echo number_format(($totalTax + $totAmmount), 2);
	}
	?>&nbsp;</td>
  </tr>
  <tr>
    <td align="left" class="normalfnt">&nbsp;</td>
    <td align="center" class="normalfntMid">&nbsp;</td>
    <td width="35%" align="center" class="normalfntMid">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="left" class="normalfnt">
    <?php
	if($invoType == 'SVAT' || $invoType == 'NFE')
	{
		echo "<strong>For Vat Purpose Only:</strong><br/>Suspended Value Added Tax @ 12% = ". number_format((($totAmmount * 12)/100), 2)."";
	}
	?>
    </td>
    <td align="right" class="normalfntRight">&nbsp;</td>
  <tr>
    <td colspan="3" align="left" class="normalfnt">
	<?php
	if($invoType == 'SVAT' || $invoType == 'NFE')
	{
		echo "Exchange Rate: <strong>Rs. ".number_format($rate,4).""."/".$symbol." '".number_format($totAmmount*$rate, 2)."'</strong>";
		
		echo "<br />";
		
		$val = $totAmmount*$rate;
        $inWord= convert_number($val);   
        $sence=explode(".",number_format($val,2));
        $sent=convert_number($sence[1]);
        if(strlen($sence[1])>0)
		{
        	$inWord=$inWord." and ".$sent." cents";
        }
       echo $inWord;
	}
	?>
    </td>
    <td align="right" class="normalfntRight">&nbsp;</td>
  	</tr>
	<tr>
	  <td colspan="3" align="left" class="normalfnt">&nbsp;</td>
	  <td align="right" class="normalfntRight">&nbsp;</td>
	  </tr>
	<tr>
    <td colspan="3" align="left" class="normalfnt">Payment Terms: <?php echo $payTerms ." days, Draft\Cheques in favour of ".$companyName?></td>
    <td align="right" class="normalfntRight">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4" align="left" class="normalfnt" height="30px"><br /><br/>&nbsp;</td>
  </tr>
  <tr>
    <td align="left" class="normalfnt">&nbsp;</td>
    <td align="center" class="normalfntMid">
    <?php
	if($modifyer == NULL)
	{
		echo $creater;
	}
	else
	{
		echo $modifyer;
	}
	?>
    </td>
    <td align="center" class="normalfntMid">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" class="normalfnt">.........................................</td>
    <td align="center" class="normalfntMid">........................................</td>
    <td align="center" class="normalfntMid">....................................................</td>
    <td align="right" class="normalfntRight">............................</td>
  </tr>
  <tr>
    <td align="left" class="normalfnt">Customer Acknowledgement</td>
    <td align="center" class="normalfntMid">Prepared By</td>
    <td align="center" class="normalfntMid">Authorized by Accountant</td>
    <td align="right" class="normalfntRight">Authorized By</td>
  </tr>
  </table>
  </td>
</tr>
</table>
</div>        
</form>
</body>
</html>
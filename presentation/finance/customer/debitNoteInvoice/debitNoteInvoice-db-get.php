<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$companyId = $row['intCompanyId']; 
	}
	/////////// sales invoice load part /////////////////////
	if($requestType=='loadCombo')
	{
		$sql = "SELECT
				fin_customer_debitnoteinvoice_header.strReferenceNo,
				fin_customer_debitnoteinvoice_header.intInvoiceNo,
				mst_customer.strName
				FROM
				fin_customer_debitnoteinvoice_header
				Inner Join mst_customer ON fin_customer_debitnoteinvoice_header.intCustomerId = mst_customer.intId
				WHERE
				fin_customer_debitnoteinvoice_header.intCompanyId =  '$companyId' AND
				fin_customer_debitnoteinvoice_header.intDeleteStatus =  '0'
				ORDER BY intInvoiceNo DESC
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['strReferenceNo']."\">".$row['strReferenceNo']." - ".$row['strName']."</option>";
		}
		echo $html;
	}
	
	//===========Add by dulakshi 2013.03.20=========
	else if($requestType=='loadCustomer')
	{
		$ledAcc  = $_REQUEST['ledgerAcc'];		
							
		$condition == "";
			if($ledAcc != "")
			{	
				$condition .= "AND mst_financecustomeractivate.intChartOfAccountId =  '$ledAcc'	";			
			}			
			
			$sql = "SELECT
						mst_customer.intId,
						mst_customer.strName,
						mst_financecustomeractivate.intCompanyId
					FROM
						mst_customer
						Inner Join mst_financecustomeractivate ON mst_customer.intId = mst_financecustomeractivate.intCustomerId
					WHERE
						mst_customer.intStatus =  '1' AND
						mst_financecustomeractivate.intCompanyId =  '$companyId' ".$condition ." order by mst_customer.strName ASC";
							
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
	}
	//==============================================
	
	else if($requestType=='loadDetails')
	{
		$id  = $_REQUEST['id'];
		
		//-------------------------------------------------------------
		$sql = "SELECT
				fin_customer_debitnoteinvoice_details.intInvoiceNo,
				fin_customer_debitnoteinvoice_details.intAccPeriodId,
				fin_customer_debitnoteinvoice_details.intLocationId,
				fin_customer_debitnoteinvoice_details.intCompanyId,
				fin_customer_debitnoteinvoice_details.strReferenceNo,
				fin_customer_debitnoteinvoice_details.intItem,
				fin_customer_debitnoteinvoice_details.strStyleNo,
				fin_customer_debitnoteinvoice_details.strGraphicNo,
				fin_customer_debitnoteinvoice_details.strOrderNo,
				fin_customer_debitnoteinvoice_details.strLineItem,
				fin_customer_debitnoteinvoice_details.strCusLocation,
				fin_customer_debitnoteinvoice_details.strItemDesc,
				fin_customer_debitnoteinvoice_details.intUom,
				fin_customer_debitnoteinvoice_details.dblQty,
				fin_customer_debitnoteinvoice_details.dblUnitPrice,
				fin_customer_debitnoteinvoice_details.dblDiscount,
				fin_customer_debitnoteinvoice_details.dblTaxAmount,
				fin_customer_debitnoteinvoice_details.intTaxGroupId,
				fin_customer_debitnoteinvoice_details.intDimensionId,
				fin_customer_debitnoteinvoice_header.intDeleteStatus
				FROM
				fin_customer_debitnoteinvoice_details
				Inner Join fin_customer_debitnoteinvoice_header ON fin_customer_debitnoteinvoice_details.strReferenceNo = fin_customer_debitnoteinvoice_header.strReferenceNo
				WHERE
				fin_customer_debitnoteinvoice_details.strReferenceNo =  '$id' AND
				fin_customer_debitnoteinvoice_header.intDeleteStatus =  '0'
				";
		$result = $db->RunQuery($sql);
		$arrDetail;
		while($row=mysqli_fetch_array($result))
		{
			$val['itemId'] 		= $row['intItem'];
			
			$val['styleNo'] 	= $row['strStyleNo'];
			$val['graphicNo'] 	= $row['strGraphicNo'];
			$val['orderNo'] 	= $row['strOrderNo'];
			$val['lineItem'] 	= $row['strLineItem'];
			$val['cusLocate'] 	= $row['strCusLocation'];
			$val['itemDesc'] 	= $row['strItemDesc'];
			
			$val['uom'] 		= $row['intUom'];
			$val['qty'] 		= $row['dblQty'];
			$val['unitPrice'] 	= $row['dblUnitPrice'];
			$val['discount'] 	= $row['dblDiscount'];
			$val['taxAmount'] 	= $row['dblTaxAmount'];
			$val['taxGroup'] 	= $row['intTaxGroupId'];
			$val['dimension'] 	= $row['intDimensionId'];
			$arrDetail[] = $val;
		}
		$response['detailVal'] = $arrDetail;
		//-----------------------------------------------------------
		
		$sql   = "SELECT
				fin_customer_debitnoteinvoice_header.intInvoiceNo,
				fin_customer_debitnoteinvoice_header.intAccPeriodId,
				fin_customer_debitnoteinvoice_header.intLocationId,
				fin_customer_debitnoteinvoice_header.intCompanyId,
				fin_customer_debitnoteinvoice_header.strReferenceNo,
				fin_customer_debitnoteinvoice_header.intCustomerId,
				fin_customer_debitnoteinvoice_header.dtmDate,
				fin_customer_debitnoteinvoice_header.strInvoiceType,
				fin_customer_debitnoteinvoice_header.strAddress,
				fin_customer_debitnoteinvoice_header.strShipTo,
				fin_customer_debitnoteinvoice_header.intCurrencyId,
				fin_customer_debitnoteinvoice_header.dblRate,
				fin_customer_debitnoteinvoice_header.strRemark,
				fin_customer_debitnoteinvoice_header.strPoNo,
				fin_customer_debitnoteinvoice_header.intPaymentsTermsId,
				fin_customer_debitnoteinvoice_header.intMarketerId,
				fin_customer_debitnoteinvoice_header.dtmShipDate,
				fin_customer_debitnoteinvoice_header.intShipmentId,
				fin_customer_debitnoteinvoice_header.strMessage,
				fin_customer_debitnoteinvoice_header.intDimensionId,
				fin_customer_debitnoteinvoice_header.strInvoiceNo
				FROM
				fin_customer_debitnoteinvoice_header
				WHERE
				fin_customer_debitnoteinvoice_header.strReferenceNo =  '$id' AND
				fin_customer_debitnoteinvoice_header.intDeleteStatus =  '0'
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['customer'] 	= $row['intCustomerId'];
			$response['date'] 		= $row['dtmDate'];
			$response['invoType'] 	= $row['strInvoiceType'];
			$response['address'] 	= $row['strAddress'];
			$response['shipTo'] 	= $row['strShipTo'];
			$response['currency']	= $row['intCurrencyId'];
			$response['rate'] 		= $row['dblRate'];
			$response['remark'] 	= $row['strRemark'];
			$response['poNo'] 		= $row['strPoNo'];
			$response['payTerms'] 	= $row['intPaymentsTermsId'];
			$response['marketer'] 	= $row['intMarketerId'];
			$response['shipDate'] 	= $row['dtmShipDate'];
			$response['shipment'] 	= $row['intShipmentId'];
			$response['message'] 	= $row['strMessage'];
			$response['headDimension'] 	= $row['intDimensionId'];
			$response['cusInvoiceNo'] 	= $row['strInvoiceNo'];
		}
		//--------------------------------------------------
		$sql = "SELECT
				fin_customer_debitnoteinvoice_header.strReferenceNo,
				fin_customer_debitnoteinvoice_header.intCustomerId,
				INV.dblQty,
				INV.dblUnitPrice,
				INV.dblDiscount,
				INV.dblTaxAmount,
				sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) AS amount,
				(
				sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) 
				-
				IFNULL ((SELECT
				Sum(fin_customer_receivedpayments_main_details.dblPayAmount )AS paidAmount
				FROM fin_customer_receivedpayments_main_details
				Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
				WHERE
				fin_customer_receivedpayments_main_details.strJobNo =  INV.strReferenceNo AND
				fin_customer_receivedpayments_header.intDeleteStatus =  '0' AND fin_customer_receivedpayments_main_details.strDocType = 'D.Invoice'
				GROUP BY
				fin_customer_receivedpayments_main_details.strJobNo),0)
				) AS balAmount
				FROM
				fin_customer_debitnoteinvoice_details AS INV
				Inner Join fin_customer_debitnoteinvoice_header ON fin_customer_debitnoteinvoice_header.strReferenceNo = INV.strReferenceNo AND INV.intInvoiceNo = fin_customer_debitnoteinvoice_header.intInvoiceNo AND INV.intAccPeriodId = fin_customer_debitnoteinvoice_header.intAccPeriodId AND INV.intCompanyId = fin_customer_debitnoteinvoice_header.intCompanyId
				Inner Join mst_customer ON mst_customer.intId = fin_customer_debitnoteinvoice_header.intCustomerId
				Inner Join mst_financecurrency ON mst_financecurrency.intId = fin_customer_debitnoteinvoice_header.intCurrencyId
				WHERE
				fin_customer_debitnoteinvoice_header.intDeleteStatus =  '0' AND fin_customer_debitnoteinvoice_header.strReferenceNo = '$id'
				GROUP BY
				fin_customer_debitnoteinvoice_header.strReferenceNo
				"; // AND INV.intLocationId = fin_customer_debitnoteinvoice_header.intLocationId
			$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['totAmount'] 	= $row['amount'];
			$response['balAmount'] 	= $row['balAmount'];
		}
		//--------------------------------------------------
		echo json_encode($response);
	}
	else if($requestType=='loadInvoiceDetails')
	{
		$id  = $_REQUEST['id'];		
		//-------------------------------------------------------------
		$sql = "SELECT
				fin_customer_salesinvoice_details.intInvoiceNo,
				fin_customer_salesinvoice_details.intAccPeriodId,
				fin_customer_salesinvoice_details.intLocationId,
				fin_customer_salesinvoice_details.intCompanyId,
				fin_customer_salesinvoice_details.strReferenceNo,
				fin_customer_salesinvoice_details.intItem,
				fin_customer_salesinvoice_details.strStyleNo,
				fin_customer_salesinvoice_details.strGraphicNo,
				fin_customer_salesinvoice_details.strOrderNo,
				fin_customer_salesinvoice_details.strLineItem,
				fin_customer_salesinvoice_details.strCusLocation,
				fin_customer_salesinvoice_details.strItemDesc,
				fin_customer_salesinvoice_details.intUom,
				fin_customer_salesinvoice_details.dblQty,
				fin_customer_salesinvoice_details.dblUnitPrice,
				fin_customer_salesinvoice_details.dblDiscount,
				fin_customer_salesinvoice_details.dblTaxAmount,
				fin_customer_salesinvoice_details.intTaxGroupId,
				fin_customer_salesinvoice_details.intDimensionId,
				fin_customer_salesinvoice_header.intDeleteStatus
				FROM
				fin_customer_salesinvoice_details
				Inner Join fin_customer_salesinvoice_header ON fin_customer_salesinvoice_details.strReferenceNo = fin_customer_salesinvoice_header.strReferenceNo
				WHERE
				fin_customer_salesinvoice_details.strReferenceNo =  '$id' AND
				fin_customer_salesinvoice_header.intDeleteStatus =  '0'
				";
		$result = $db->RunQuery($sql);
		$arrDetail;
		while($row=mysqli_fetch_array($result))
		{
			$val['itemId'] 		= $row['intItem'];
			
			$val['styleNo'] 	= $row['strStyleNo'];
			$val['graphicNo'] 	= $row['strGraphicNo'];
			$val['orderNo'] 	= $row['strOrderNo'];
			$val['lineItem'] 	= $row['strLineItem'];
			$val['cusLocate'] 	= $row['strCusLocation'];
			$val['itemDesc'] 	= $row['strItemDesc'];
			
			$val['uom'] 		= $row['intUom'];
			$val['qty'] 		= $row['dblQty'];
			$val['unitPrice'] 	= $row['dblUnitPrice'];
			$val['discount'] 	= $row['dblDiscount'];
			$val['taxAmount'] 	= $row['dblTaxAmount'];
			$val['taxGroup'] 	= $row['intTaxGroupId'];
			$val['dimension'] 	= $row['intDimensionId'];
			$arrDetail[] = $val;
		}
		$response['detailVal'] = $arrDetail;
		//-----------------------------------------------------------
		echo json_encode($response);
	}
	else if($requestType=='getExchangeRate')
	{
		$currencyId  	= $_REQUEST['currencyId'];
		$exchangeDate	= $_REQUEST['exchangeDate'];
		
		$sql = "SELECT
					mst_financeexchangerate.dblSellingRate,
					mst_financeexchangerate.dblBuying
				FROM mst_financeexchangerate
				WHERE
					mst_financeexchangerate.intCurrencyId 	=  '$currencyId' AND
					mst_financeexchangerate.dtmDate 		=  '$exchangeDate'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		
		if(mysqli_num_rows($result)>0)
		{
			$arrValue['sellingRate'] 	= $row['dblSellingRate'];
			$arrValue['buyingRate'] 	= $row['dblBuying'];
		}
		else
		{
			$arrValue['sellingRate'] 	= "";
			$arrValue['buyingRate'] 	= "";
		}
		echo json_encode($arrValue);
	}
	else if($requestType=='getCustomerAddress') // with invoice type, currency and payment terms
	{
		$customerId  = $_REQUEST['customerId'];
		
		$sql = "SELECT 	mst_customer.strAddress, mst_customer.strInvoiceType,
				mst_customer.intCurrencyId,
				mst_customer.intPaymentsTermsId
				FROM mst_customer	WHERE mst_customer.intId =  '$customerId'";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$response['address'] = $row['strAddress'];
		$response['invoType']= $row['strInvoiceType'];
		$response['currency']= $row['intCurrencyId'];
		$response['payTerms']= $row['intPaymentsTermsId'];
		
		$sql = "SELECT
			fin_customer_salesinvoice_header.strReferenceNo,
			fin_customer_salesinvoice_header.intDeleteStatus
			FROM fin_customer_salesinvoice_header
			WHERE
			fin_customer_salesinvoice_header.intCustomerId =  '$customerId' AND
			fin_customer_salesinvoice_header.intCompanyId =  '$companyId' AND
			fin_customer_salesinvoice_header.intDeleteStatus =  '0'
			ORDER BY fin_customer_salesinvoice_header.strReferenceNo DESC
			";
			$result = $db->RunQuery($sql);
			$salesInvoiceList ='<option value=""></option>';
			while($row = mysqli_fetch_array($result))
			{
				$salesInvoiceList .="<option value=\"".$row['strReferenceNo']."\">".$row['strReferenceNo']."</option>";	
			}
			$response['salesInvoiceList']	 = $salesInvoiceList;
		
		echo json_encode($response);
	}
	else if($requestType=='getItemDescription')
	{
		$itemId  = $_REQUEST['itemId'];
		
		$sql = "SELECT
				mst_financecustomeritem.intId,
				mst_financecustomeritem.strRemark
				FROM
				mst_financecustomeritem
				WHERE
				mst_financecustomeritem.intId =  '$itemId'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		echo $row['strRemark'];
	}
	else if($requestType=='getTaxValue')
	{
		$operation  = $_REQUEST['opType'];
		$amount  = $_REQUEST['valAmount'];
		$taxCodes = json_decode($_REQUEST['arrTaxCode'], true);
		
		if(count($taxCodes) != 0)
		{
			foreach($taxCodes as $taxCode)
			{
				$codeValues[] = callTaxValue($taxCode['taxId']);
			}
		}
		if(count($codeValues) > 1)
		{
			if($operation == 'Inclusive')
			{
				$firstVal = ($amount*$codeValues[0])/100;
				$withTaxVal = $firstVal + ((($amount+$firstVal)*$codeValues[1])/100);
				$val1 = ($amount*$codeValues[0])/100;
				$val2 = ((($amount+$firstVal)*$codeValues[1])/100);
			}
			else if($operation == 'Exclusive')
			{
				$withTaxVal = ($amount*($codeValues[0] + $codeValues[1]))/100;
				$val1 = ($amount*$codeValues[0])/100;
				$val2 = ($amount*$codeValues[1])/100;
			}
		}
		else if(count($codeValues) == 1 && $operation == 'Isolated')
		{
			$withTaxVal = ($amount*$codeValues[0])/100;
			$val1 = ($amount*$codeValues[0])/100;
		}
		echo $withTaxVal."/".$val1."/".$val2;
	}
	else if($requestType=='getTaxProcess')
	{
		$taxGrpId  = $_REQUEST['taxGroupId'];
		
		$sql = "SELECT
				mst_financetaxgroup.intId,
				mst_financetaxgroup.strProcess
				FROM
				mst_financetaxgroup
				WHERE
				mst_financetaxgroup.intId =  '$taxGrpId'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		echo $row['strProcess'];
	}
	function callTaxValue($taxId)
	{
		global $db;
		$sql = "SELECT
				mst_financetaxisolated.intId,
				mst_financetaxisolated.strCode,
				mst_financetaxisolated.dblRate
				FROM
				mst_financetaxisolated
				WHERE
				mst_financetaxisolated.intId = '$taxId'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$taxVal = $row['dblRate'];	
		return $taxVal;
	}
?>
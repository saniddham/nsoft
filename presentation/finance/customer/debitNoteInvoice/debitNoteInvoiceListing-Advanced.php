<?php
session_start();
$backwardseperator = "../../../../";
$thisFilePath =  $_SERVER['PHP_SELF'];

include  "{$backwardseperator}dataAccess/permisionCheck2.inc";

//$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
//$result = $db->RunQuery($sql);
//while($row=mysqli_fetch_array($result))
//{
//	$companyId = $row['intCompanyId']; 
//}

$companyId 	= $_SESSION['headCompanyId'];

if($_SERVER['REQUEST_METHOD'] == "POST")
{
	$optionValue = trim($_REQUEST['cboSearchOption']);
	$searchValue = trim($_REQUEST['txtSearchValue']);
}

//---------------------------------------------New Grid-----------------------------------------------------
include_once "../../../../libraries/jqdrid/inc/jqgrid_dist.php";

				 $sql = "select * from(SELECT
						fin_customer_debitnoteinvoice_header.intInvoiceNo,
						fin_customer_debitnoteinvoice_header.strReferenceNo AS InvoiceNumber,
						'More' AS More,
						fin_customer_debitnoteinvoice_header.intCustomerId,
						INV.dblQty AS Qty,
						INV.dblUnitPrice AS UnitPrice,
						INV.dblDiscount,
						INV.dblTaxAmount AS taxAmount,
						mst_customer.strName AS Customer,
						sys_users.strUserName AS Marketer,
						fin_customer_debitnoteinvoice_header.intMarketerId,
						fin_customer_debitnoteinvoice_header.strPoNo AS PONumber,
						fin_customer_debitnoteinvoice_header.intCompanyId,
						fin_customer_debitnoteinvoice_header.dtmDate AS `Date`,
						mst_financecurrency.strCode AS Currency,
						fin_customer_debitnoteinvoice_header.strRemark AS Memo,
						INV.strStyleNo AS StyleNumber,
						INV.strGraphicNo GraphicNumber,
						INV.strOrderNo AS ScheduleNo,
						INV.strLineItem AS LineItem,
						INV.strCusLocation AS CusLocation
						FROM
						fin_customer_debitnoteinvoice_details AS INV
						Inner Join fin_customer_debitnoteinvoice_header ON fin_customer_debitnoteinvoice_header.strReferenceNo = INV.strReferenceNo AND INV.intInvoiceNo = fin_customer_debitnoteinvoice_header.intInvoiceNo AND INV.intAccPeriodId = fin_customer_debitnoteinvoice_header.intAccPeriodId AND INV.intCompanyId = fin_customer_debitnoteinvoice_header.intCompanyId
						Inner Join mst_customer ON mst_customer.intId = fin_customer_debitnoteinvoice_header.intCustomerId
						Inner Join mst_financecurrency ON mst_financecurrency.intId = fin_customer_debitnoteinvoice_header.intCurrencyId
						LEFT OUTER Join sys_users ON fin_customer_debitnoteinvoice_header.intMarketerId = sys_users.intUserId
						WHERE
						fin_customer_debitnoteinvoice_header.intDeleteStatus =  '0' AND fin_customer_debitnoteinvoice_header.intCompanyId = '$companyId') as t where 1=1"; // AND INV.intLocationId = fin_customer_debitnoteinvoice_header.intLocationId

$invoiceLink = "debitNoteInvoice.php?id={InvoiceNumber}";
$invoiceReport = "debitNoteInvoiceDetails.php?id={InvoiceNumber}";

////STATUS
//$col["title"] 	= "Pay. Status"; // caption of column
//$col["name"] 	= "Status"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
//$col["width"] 	= "3";
//$col["align"] 	= "center";
//$col["search"] = false;
//$cols[] = $col;	$col=NULL;

//Style Number
$col["title"] = "Style No."; // caption of column
$col["name"] = "StyleNumber"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Graphic Number
$col["title"] = "Graphic No."; // caption of column
$col["name"] = "GraphicNumber"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Sales Order No / Schedule No
$col["title"] = "Schedule No"; // caption of column
$col["name"] = "ScheduleNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Sales Order No / Schedule No
$col["title"] = "Line Item"; // caption of column
$col["name"] = "LineItem"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Customer Location
$col["title"] = "Customer Location"; // caption of column
$col["name"] = "CusLocation"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Customer
$col["title"] = "Customer"; // caption of column
$col["name"] = "Customer"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "6";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//PO Number
$col["title"] = "P.O. Number"; // caption of column
$col["name"] = "PONumber"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Marketer
$col["title"] = "Marketer"; // caption of column
$col["name"] = "Marketer"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Date
$col["title"] = "Date"; // caption of column
$col["name"] = "Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Currency
$col["title"] = "Currency"; // caption of column
$col["name"] = "Currency"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Memo
$col["title"] = "Memo"; // caption of column
$col["name"] = "Memo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;

//Invoice Number
$col["title"] = "Invoice Number"; // caption of column
$col["name"] = "InvoiceNumber"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "6";
$col["align"] = "center";
$col['link']	= $invoiceLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

////Amount
//$col["title"] = "Unit Price"; // caption of column
//$col["name"] = "UnitPrice"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
//$col["width"] = "3";
//$col["align"] = "right";
//$col['link']	= $invoiceLink;
//$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
//$cols[] = $col;	$col=NULL;
//
////To be Paid
//$col["title"] = "Tax Amount"; // caption of column
//$col["name"] = "taxAmount"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
//$col["width"] = "3";
//$col["align"] = "right";
//$col['link']	= $invoiceLink;
//$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
//$cols[] = $col;	$col=NULL;

//View
$col["title"] = "View"; // caption of column
$col["name"] = "More"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "1";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $invoiceReport;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

$jq = new jqgrid('',$db);

$grid["caption"] 		= "Debit Note Invoice Listing - Advanced";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'intInvoiceNo'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes

//<><><><><><>======================Compulsory Code==============================<><><><><><>
	$jq->set_options($grid);
	$jq->select_command = $sql;
	$jq->set_columns($cols);
	$jq->set_actions(array(	
		"add"=>false, // allow/disallow add
		"edit"=>false, // allow/disallow edit
		"delete"=>false, // allow/disallow delete
		"rowactions"=>false, // show/hide row wise edit/del/save option
		"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
		"export"=>true
	) 
	);

$out = $jq->render("list1");
//<><><><><><>======================Compulsory Code==============================<><><><><><>

//---------------------------------------------New Grid-----------------------------------------------------
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Debit Note Invoice Listing - Advanced</title>

<!--<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/tblstyle.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../libraries/jquery/jquery-1.3.2.min.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/scrolltable.js"></script>

<script type="application/javascript" src="../../../../libraries/javascript/zebraStripe.js"></script>
-->
<script type="text/javascript" language="javascript">
function pageSubmit()
{
	document.getElementById('frmDebitNoteInvoiceListing').submit();	
}
function clearValue()
{
	document.getElementById('txtSearchValue').value = '';
	document.getElementById('txtSearchValue').focus();
}
</script>

</head>

<body>
<form id="frmDebitNoteInvoiceListing" name="frmDebitNoteInvoiceListing" method="post" autocomplete="off" action="">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
<div align="center">
<!-------------------------------------------New Grid---------------------------------------------------->
<link rel="stylesheet" type="text/css" media="screen" href="../../../../libraries/jqdrid/js/themes/smoothness/jquery-ui.custom.css"></link>	
<link rel="stylesheet" type="text/css" media="screen" href="../../../../libraries/jqdrid/js/jqgrid/css/ui.jqgrid.css"></link>	

<script src="../../../../libraries/jqdrid/js/jquery.min.js" type="text/javascript"></script>
<script src="../../../../libraries/jqdrid/js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
<script src="../../../../libraries/jqdrid/js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>	
<script src="../../../../libraries/jqdrid/js/themes/jquery-ui.custom.min.js" type="text/javascript"></script>
<script src="../../../../libraries/javascript/script.js" type="text/javascript"></script>
<!-------------------------------------------New Grid---------------------------------------------------->
		<!--<div class="trans_layoutL">
		  <div class="trans_text">Debit Note Invoice Listing</div>-->
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <!--<tr>
        <td><table width="100%" border="0" cellpadding="1" cellspacing="0" class="tableBorder_allRound">
          <tr>
            <td width="16%" height="22" class="normalfnt">&nbsp;</td>
            <td width="18%" class="normalfntMid">Searching Option</td>
            <td width="17%" class="normalfnt">
            <select name="cboSearchOption" id="cboSearchOption" onchange="clearValue();" style="width:150px">
              <option value="">&nbsp;</option>
<option value="mst_customer.strName" <?php echo($optionValue=='mst_customer.strName'?'selected':'') ?> >Customer</option>
<option value="fin_customer_debitnoteinvoice_header.strPoNo" <?php echo($optionValue=='fin_customer_debitnoteinvoice_header.strPoNo'?'selected':'') ?>>P.O. Number</option>
<option value="sys_users.strUserName" <?php echo($optionValue=='sys_users.strUserName'?'selected':'') ?>>Marketer</option>
<option value="fin_customer_debitnoteinvoice_header.strReferenceNo" <?php echo($optionValue=='fin_customer_debitnoteinvoice_header.strReferenceNo'?'selected':'') ?>>Invoice Number</option>
            </select></td>
            <td width="16%"><input value="<?php echo $searchValue; ?>" type="text" name="txtSearchValue" id="txtSearchValue" /></td>
            <td width="18%" class="normalfnt">
            <a href="#">
            <img src="../../../../images/search.png" width="20" height="20" onclick="pageSubmit();" /></a></td>
            <td width="15%">&nbsp;</td>
            </tr>
          <tr>
            <td height="22" class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            </tr>
          </table></td>
      </tr>-->
      <!--<tr>
        <td>
          <div id="_table_wrap" style="overflow: hidden; display: inline-block; border: 1px solid rgb(136, 136, 136);">
          <div id="_head_wrap" style="width: 900px; overflow: hidden;">
          <table class="tbl1 scroll" id="_head" style="table-layout: fixed;">
          <thead>
            <tr class="gridHeader">
              <th height="22" style="width: 120px;" >Customer</th>
              <th style="width: 90px;" >P.O. Number</th>
              <th style="width: 60px;" >Marketer</th>
              <th style="width: 110px;" >Invoice Number</th>
              <th style="width: 35px;" >View</th>
              <th style="width: 7px;" ></th>
              </tr>
           </table>
           </div>
           <div class="normalfnt" id="_body_wrap" style="width: 900px; height: 320px; overflow: scroll;">
           <table class="tbl1 scroll"> 
           <tbody>
              <?php

				if($searchValue!='')
			  	$wherePart = " and $optionValue like '%$searchValue%'";
				
	 	 		$sql = "SELECT
						fin_customer_debitnoteinvoice_header.intInvoiceNo,
						fin_customer_debitnoteinvoice_header.intAccPeriodId,
						fin_customer_debitnoteinvoice_header.intLocationId,
						fin_customer_debitnoteinvoice_header.intCompanyId,
						fin_customer_debitnoteinvoice_header.intCustomerId,
						mst_customer.intId,
						mst_customer.strName,
						fin_customer_debitnoteinvoice_header.strPoNo,
						fin_customer_debitnoteinvoice_header.intMarketerId,
						sys_users.strUserName,
						fin_customer_debitnoteinvoice_header.strReferenceNo
						FROM
						fin_customer_debitnoteinvoice_header
					left outer Join mst_customer ON fin_customer_debitnoteinvoice_header.intCustomerId = mst_customer.intId
					left outer Join sys_users ON fin_customer_debitnoteinvoice_header.intMarketerId = sys_users.intUserId
						WHERE
						fin_customer_debitnoteinvoice_header.intCompanyId = '$companyId' AND
						fin_customer_debitnoteinvoice_header.intDeleteStatus =  '0'
						$wherePart
						ORDER BY
						mst_customer.strName ASC, intInvoiceNo DESC";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$id 		=	 $row['strReferenceNo'];
	  			 ?>
           		<tr class="normalfnt">
           		<td class="slInvoCusName" align="center" bgcolor="#FFFFFF"><?php echo $row['strName'];?></td>
              	<td class="slInvoPo"  align="center" bgcolor="#FFFFFF"><?php echo $row['strPoNo'];?></td>
              	<td class="slInvoMarketer" bgcolor="#FFFFFF"><?php echo $row['strUserName'];?></td>
              	<td class="slInvoRef" align="center" bgcolor="#FFFFFF">
                <a target="_blank" href="<?php echo "debitNoteInvoice.php?id=$id";?>">
				<?php echo $row['strReferenceNo'];?></a></td>
              	<td class="slInvoView" align="center" bgcolor="#FFFFFF">
                <a target="_blank" href="<?php echo "debitNoteInvoiceDetails.php?id=$id";?>">More</a></td>
              </tr>
               <?php 
        	    } 
       		   ?>
              </tbody>
             </table>
            </div></div>
              </td>
              </tr>-->
            </table>
			</td>
      </tr>
       <tr>
        <td>
        <div align="center" style="margin:10px">
			<?php echo $out?>
        </div>
        </td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound"><a href="../../../../main.php"><img  src="../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
      </tr>
    </table>
    </td>
    </tr>
  </table>

 <!-- </div>-->
  </div>
</form>
</body>
</html>

$(document).ready(function() { 
    
     $('#frmCustUnadjustedGL #txtDate').blur(function(){         
         var theDate=$('#frmCustUnadjustedGL #txtDate').val();
         var url ='unadjustedGainLoss-db-get.php?requestType=loadCurrency&theDate='+theDate;
            var obj=$.ajax({
                url:url,
                dataType:'json',
                success:function(json){
                    $('#frmCustUnadjustedGL #curList').html(json.curList);
                    $('#frmCustUnadjustedGL #btnDisplay').attr("class","show");
                    
                },
                async:false
            });          
     });
     $('#frmCustUnadjustedGL #btnDisplay').click(function(){ 
         $('#divMaskProces').attr("class","maskShow mask");
         var theDate=$('#frmCustUnadjustedGL #txtDate').val();
         var data="&theDate="+theDate;
         var curIds=document.getElementsByName("txtCurId");
         for(var i=0;curIds.length > i;++i){
             var curId=curIds[i].value;
             var radios=document.getElementsByName("radio"+curId);
             for(var j=0;radios.length > j;++j){
                if(radios[j].checked){
                    var val=radios[j].value;                 
                    if(val=="bui"){data+="&curIds["+curId+"]="+  $('#frmCustUnadjustedGL #txtbuing'+curId).val();}
                    else if (val=="sel"){data+="&curIds["+curId+"]="+  $('#frmCustUnadjustedGL #txtseling'+curId).val();}   
                    else if (val=="oth"){data+="&curIds["+curId+"]="+  $('#frmCustUnadjustedGL #txtother'+curId).val();}  
                }
             }
         }
         var url ='unadjustedGainLoss-db-get.php?requestType=loadValues'+data;
            var obj=$.ajax({
                url:url,
                dataType:'json',
                success:function(json){                    
                    $('#frmCustUnadjustedGL #valueList').html(json.valueList);
                    $('#frmCustUnadjustedGL #btnGenerate').attr("class","show");
                    $('#divMaskProces').attr("class","maskHide mask"); 
                    $('#frmCustUnadjustedGL #btnGenerate').attr("disabled",false);
                },
                async:false
            });          
     });  
     
     //generate value
     $('#frmCustUnadjustedGL #btnGenerate').click(function(){ 
	if(existingMsgDate == "")
	{
         
         $('#divMaskProces').attr("class","maskShow mask");
         
         var theDate=$('#frmCustUnadjustedGL #txtDate').val();
         var data="&theDate="+theDate;
         var curIds=document.getElementsByName("txtCurId");
         for(var i=0;curIds.length > i;++i){
             var curId=curIds[i].value;
             var radios=document.getElementsByName("radio"+curId);
             for(var j=0;radios.length > j;++j){
                if(radios[j].checked){
                    var val=radios[j].value;                 
                    if(val=="bui"){data+="&curIds["+curId+"]="+  $('#frmCustUnadjustedGL #txtbuing'+curId).val();}
                    else if (val=="sel"){data+="&curIds["+curId+"]="+  $('#frmCustUnadjustedGL #txtseling'+curId).val();}   
                    else if (val=="oth"){data+="&curIds["+curId+"]="+  $('#frmCustUnadjustedGL #txtother'+curId).val();}  
                }
             }
         }
         var url ='unadjustedGainLoss-db-set.php?requestType=genEntres'+data;
            var obj=$.ajax({
                url:url,
                dataType:'json',
                success:function(json){
                    $('#divMaskProces').attr("class","maskHide mask");
                    $('#frmCustUnadjustedGL #btnGenerate').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
                    if(json.type=='pass')
                    {
                        $('#frmCustUnadjustedGL #btnGenerate').attr("disabled",true);
                        var count=json.reCcount;
                        var dis="<tr class='normalfntMid'><td colspan="+11+">"+ count +" Records Added</td></tr>";
                        $('#frmCustUnadjustedGL #valueList').html(dis);
                    }
                    var t=setTimeout("alertx()",3000);
                    
                },            
                error:function(xhr,status){
                    $('#divMaskProces').attr("class","maskHide mask");
                    $('#frmCustUnadjustedGL #btnGenerate').validationEngine('showPrompt', errormsg(xhr.status),'fail');
                    var t=setTimeout("alertx()",3000);
                }
            }); 
}
else
{
	$('#frmCustUnadjustedGL #btnGenerate').validationEngine('showPrompt', existingMsgDate,'fail');
	var t=setTimeout("alertx()",5000);
}		         
});         
});
function alertx()
{
	$('#frmCustUnadjustedGL #btnGenerate').validationEngine('hide')	;
}


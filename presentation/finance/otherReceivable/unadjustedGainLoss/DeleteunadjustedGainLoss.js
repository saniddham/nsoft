$(document).ready(function() {
    $('#frmDeleteCustUnadjustedGL #butDelete').click(function(){
        if($('#frmDeleteCustUnadjustedGL #cboProcess').val()==''){
            
            $('#frmDeleteCustUnadjustedGL #butDelete').validationEngine('showPrompt', 'Please select Process.', 'fail');
            var t=setTimeout("alertDelete()",1000);	
        }
        else{
            var val = $.prompt('Are you sure you want to delete process "'+$('#frmDeleteCustUnadjustedGL #cboProcess option:selected').text()+'" ?',{
                buttons: { Ok: true, Cancel: false },
                callback: function(v,m,f){
                    if(v){
                        $('#divMaskProces').attr("class","maskShow mask");
                        var prosesId=$('#frmDeleteCustUnadjustedGL #cboProcess').val();
                        var url ='deleteCusUnadjustedGainLoss-db-set.php?requestType=delete&prosesId='+prosesId;
                        var obj=$.ajax({
                            url:url,
                            dataType:'json',
                            success:function(json){
                                $('#divMaskProces').attr("class","maskHide mask");
                                $('#frmDeleteCustUnadjustedGL #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
                                var t=setTimeout("alertDelete()",2000);
                            },
                            async:false
                        });
                    }
                }
            });
               
                    
        }
        
    });

});

function alertDelete(){
    $('#frmDeleteCustUnadjustedGL #butDelete').validationEngine('hide') ;
}

<?php
session_start();
$backwardseperator = "../../../../";
$mainPath 	= $_SESSION['mainPath'];
$userId 	= $_SESSION['userId'];
$requestType 	= $_REQUEST['requestType'];
$companyId = $_SESSION['headCompanyId'];
$location = $_SESSION['CompanyID'];
include "{$backwardseperator}dataAccess/Connector.php";

//==get base currency=============
$result = $db->RunQuery("SELECT com.intBaseCurrencyId FROM mst_companies AS com WHERE com.intId =  '$companyId'");
$row=mysqli_fetch_array($result);
$baseCurrencyId=$row['intBaseCurrencyId'];

if($requestType=='loadCurrency'){
    $theDate = $_REQUEST['theDate'];
    $sql="SELECT
            CUR.intId,
            CUR.strCode,
            ER.dblSellingRate,
            ER.dblBuying
            FROM
            mst_financecurrency AS CUR
            Inner Join mst_financecurrencyactivate AS CA ON CUR.intId = CA.intCurrencyId
            Inner Join mst_financeexchangerate AS ER ON CUR.intId = ER.intCurrencyId AND CA.intCompanyId = ER.intCompanyId
            WHERE
            CUR.intStatus =  1 AND
            CA.intCompanyId =  '$companyId' AND
            CUR.intId <>  '$baseCurrencyId' AND
            ER.dtmDate =  '$theDate'
            ORDER BY
            CUR.strCode ASC";
        $result = $db->RunQuery($sql);
        $curList="";
        while($row=mysqli_fetch_array($result)){
            $curId=$row['intId'];
            $buing=$row['dblBuying'];            
            $selling=$row['dblSellingRate'];
           $curList.= "<tr>";
            $curList.="<td class=\"normalfnt\">".$row['strCode']."
                            <input type=\"hidden\" name=\"txtCurId\" value=\"$curId\" />
                        </td>";
            $curList.="<td class=\"normalfntRight\">
                        <input class=\"rdoRate\" type=\"radio\" name=\"radio$curId\" id=\"radio$curId\" value=\"bui\" checked=\"true\"/>
                        <input type=\"text\" name=\"txtbuing$curId\" id=\"txtbuing$curId\" readonly=\"readonly\" class=\"validate[custom[number],required] normalfntBlue txtStyle\" value=\"$buing\"/> 
                       </td>";
            $curList.="<td class=\"normalfntRight\">
                            <input class=\"rdoRate\" type=\"radio\" name=\"radio$curId\" id=\"radio$curId\" value=\"sel\" />
                            <input type=\"text\" name=\"txtseling$curId\" id=\"txtseling$curId\" readonly=\"readonly\" class=\"validate[custom[number],required] normalfntBlue txtStyle\" value=\"$selling\"/> 
                       </td>";
            $curList.="<td class=\"normalfntRight\">
                            <input class=\"rdoRate\" type=\"radio\" name=\"radio$curId\" id=\"radio$curId\" value=\"oth\" />
                            <input type=\"text\" name=\"txtother$curId\" id=\"txtother$curId\" class=\"validate[custom[number],required] normalfntBlue txtStyle\" value=\"0\"/>
                       </td>";
           $curList.="</tr>";
        }
        $response['curList']=$curList;               
        echo json_encode($response);
}
else if($requestType=='loadValues'){
    $theDate = $_REQUEST['theDate'];
    $curRates = $_REQUEST['curIds'];
    //get ledger accounts of type Account Receiveble
    $sqlAcc="SELECT
                CA.intId,CA.strCode,CA.strName
                FROM
                mst_financechartofaccounts AS CA
                Inner Join mst_financialsubtype AS FT ON FT.intId = CA.intFinancialTypeId
                Inner Join mst_financechartofaccounts_companies AS COM ON CA.intId = COM.intChartOfAccountId
                WHERE
                (FT.intId =  '22'  OR FT.intId =  '21' OR FT.intId =  '11' OR FT.intId =  '12' OR FT.intId =  '9') AND CA.strType =  'Posting' AND COM.intCompanyId =  '$companyId'";
    $resultAcc = $db->RunQuery($sqlAcc);
        $valueList="";
        while($rowAcc=mysqli_fetch_array($resultAcc)){
            $accId=$rowAcc['intId'];
            $valueList.= "<tr>";
                $valueList.="<td class=\"Account1\" colspan=\"11\">".$rowAcc['strName']."-".$rowAcc['strCode']."</td>"; 
                
             //get Cusromers fro the account
            $sqlCust="SELECT
                        CUS.intId,CUS.strName
                        FROM
                        mst_finance_service_customer_activate AS CA
                        Inner Join mst_finance_service_customer AS CUS ON CA.intCustomerId = CUS.intId
                        WHERE
                        CA.intChartOfAccountId =  '$accId' AND CA.intCompanyId =  '$companyId'";
            $resultCust = $db->RunQuery($sqlCust);
            while($rowCust=mysqli_fetch_array($resultCust)){
                $custId=$rowCust['intId'];
                $valueList.= "<tr>";
                    $valueList.="<td class=\"Account1\"></td>";
                    $valueList.="<td class=\"Customer1\" colspan=\"10\">".$rowCust['strName']."</td>";                    
                    
                    //get Currency List
                    $sqlCur="SELECT
                            CUR.intId, CUR.strCode            
                        FROM
                            mst_financecurrency AS CUR
                            Inner Join mst_financecurrencyactivate AS CA ON CUR.intId = CA.intCurrencyId
                        WHERE
                            CUR.intStatus =  1 AND CA.intCompanyId =  '$companyId' AND CUR.intId <>  '$baseCurrencyId'";
                     $resultCur= $db->RunQuery($sqlCur);
                     while($rowCur=mysqli_fetch_array($resultCur)){
                         $curId=$rowCur['intId'];
                         $valueList.= "<tr>";
                            $valueList.="<td class=\"Account1\"></td>";
                            $valueList.="<td class=\"Customer1\"></td>";
                            $valueList.="<td class=\"Currency1\" colspan=\"9\">".$rowCur['strCode']."</td>";                            
                            //get invoice Details
                            $invDetails=getCustomerInvoices($custId,$companyId,$curId);                            
                            foreach ($invDetails as $inv){
                                $gainLoss=($inv['toBePaid'] * $curRates[$curId])-($inv['toBePaid'] * $inv['spotRate']);
                            $valueList.= "<tr>";
                                $valueList.="<td class=\"Account\"></td>";
                                $valueList.="<td class=\"Customer\"></td>";
                                $valueList.="<td class=\"Currency\"></td>";
                                $valueList.="<td class=\"Type\">".$inv['type']."</td>";
                                $valueList.="<td class=\"Number\">".$inv['docNumber']."</td>";
                                $valueList.="<td class=\"Date\">".$inv['date']."</td>";
                                $valueList.="<td class=\"Amount\">".$inv['amount']."</td>";
                                $valueList.="<td class=\"Tobe\">".$inv['toBePaid']."</td>";
                                $valueList.="<td class=\"Spot\">".$inv['spotRate']."</td>";
                                $valueList.="<td class=\"Rate\">".$curRates[$curId]."</td>";
                                $valueList.="<td class=\"Gain\">".$gainLoss."</td>";
                            $valueList.= "</tr>";
                            }//invoices
                            
                         $valueList.= "</tr>";
                     }//Curency
                
                $valueList.= "</tr>";
            }//Custtomer
            
            
            
            
            $valueList.= "</tr>";
        }//account
        $response['valueList']=$valueList;               
        echo json_encode($response);
}

//Get Customer invoice Details
function getCustomerInvoices($customerId,$companyId,$curencyId){
    global $db;
    $invoiceDetails;
    $x=0;
    //////////////////////////Invoice//////////////////////////////////
    $sql = "SELECT
                fin_other_receivable_invoice_header.strReferenceNo,
                fin_other_receivable_invoice_header.intCustomerId,
                mst_finance_service_customer.intId,
                mst_finance_service_customer.strName,
                mst_financecurrency.intId,
                mst_financecurrency.strCode,
                fin_other_receivable_invoice_header.intInvoiceNo,
                fin_other_receivable_invoice_header.intAccPeriodId,
                fin_other_receivable_invoice_header.intLocationId,
                fin_other_receivable_invoice_header.intCompanyId,
                INV.intInvoiceNo,
                INV.intAccPeriodId,
                INV.intLocationId,
                INV.intCompanyId,
                fin_other_receivable_invoice_header.dtmDate,
                fin_other_receivable_invoice_header.dblRate,
                INV.dblQty,
                INV.dblUnitPrice,
                INV.dblDiscount,
                INV.dblTaxAmount,
                sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) AS amount,
                (
                sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) 
                -
                IFNULL ((SELECT
                Sum(fin_other_receivable_payments_main_details.dblPayAmount )AS paidAmount
                FROM fin_other_receivable_payments_main_details
                Inner Join fin_other_receivable_payments_header ON fin_other_receivable_payments_main_details.strReferenceNo = fin_other_receivable_payments_header.strReferenceNo
                WHERE
                fin_other_receivable_payments_main_details.strJobNo =  INV.strReferenceNo AND
                fin_other_receivable_payments_header.intDeleteStatus =  '0' AND fin_other_receivable_payments_main_details.strDocType = 'O.SInvoice'
                GROUP BY
                fin_other_receivable_payments_main_details.strJobNo),0)

                ) AS balAmount
                FROM
                fin_other_receivable_invoice_details AS INV
                Inner Join fin_other_receivable_invoice_header ON fin_other_receivable_invoice_header.strReferenceNo = INV.strReferenceNo AND INV.intInvoiceNo = fin_other_receivable_invoice_header.intInvoiceNo AND INV.intAccPeriodId = fin_other_receivable_invoice_header.intAccPeriodId AND INV.intLocationId = fin_other_receivable_invoice_header.intLocationId AND INV.intCompanyId = fin_other_receivable_invoice_header.intCompanyId
                Inner Join mst_finance_service_customer ON mst_finance_service_customer.intId = fin_other_receivable_invoice_header.intCustomerId
                Inner Join mst_financecurrency ON mst_financecurrency.intId = fin_other_receivable_invoice_header.intCurrencyId
                WHERE
                fin_other_receivable_invoice_header.intCustomerId =  '$customerId' AND
                fin_other_receivable_invoice_header.intDeleteStatus =  '0'
                AND fin_other_receivable_invoice_header.intCompanyId =  '$companyId' AND
                mst_financecurrency.intId=$curencyId
                GROUP BY
                fin_other_receivable_invoice_header.strReferenceNo
                having balAmount<>0";
		$result = $db->RunQuery($sql);
    while($row=mysqli_fetch_array($result))
    {
        $invoice = $row['strReferenceNo'];
        $amount = number_format($row['amount'],4,'.','');
        $toBePaid = number_format($row['balAmount'],4,'.','');
        $date = $row['dtmDate'];
        $currency = $row['strCode'];
        $rate = $row['dblRate'];
        $invoiceDetails[$x]['docNumber']=$invoice;
        $invoiceDetails[$x]['date']=$date;
        $invoiceDetails[$x]['amount']=$amount;
        $invoiceDetails[$x]['toBePaid']=$toBePaid;
        $invoiceDetails[$x]['spotRate']=$rate;
        $invoiceDetails[$x]['type']='O.SInvoice';
        ++$x;			
    }
    ////////////////////////////Advanced Received////////////////////////////////
    $sql = "SELECT
                    ADV.strReceiptNo,
                    ADV.intCustomer,
                    ADV.intCompanyId,
                    ADV.dtDate,
                    ADV.dblRate,
                    ADV.intCurrency,
                    ADV.dblReceivedAmount,
                    mst_financecurrency.intId,
                    mst_financecurrency.strCode,
                    (
                    ADV.dblReceivedAmount 
                    +
                    IFNULL ((SELECT
                    Sum(fin_other_receivable_payments_main_details.dblPayAmount )AS paidAmount
                    FROM fin_other_receivable_payments_main_details
                    Inner Join fin_other_receivable_payments_header ON fin_other_receivable_payments_main_details.strReferenceNo = fin_other_receivable_payments_header.strReferenceNo
                    WHERE
                    fin_other_receivable_payments_main_details.strJobNo =  ADV.strReceiptNo AND
                   fin_other_receivable_payments_header.intDeleteStatus =  '0' AND fin_other_receivable_payments_main_details.strDocType = 'O.AReceived'
                    GROUP BY
                    fin_other_receivable_payments_main_details.strJobNo),0)
                    ) AS balAmount
                    FROM
                    fin_other_receivable_advancereceived_header AS ADV
                    Inner Join mst_financecurrency ON ADV.intCurrency = mst_financecurrency.intId
                    WHERE
                    ADV.intCustomer =  '$customerId' AND
                    ADV.intStatus = '1'
                    AND ADV.intCompanyId =  '$companyId' AND
                    ADV.intCurrency=$curencyId
                    having balAmount<>0
                    ";
    $result = $db->RunQuery($sql);
    while($row=mysqli_fetch_array($result))
    {
        $receipt = $row['strReceiptNo'];
        $amount = number_format($row['dblReceivedAmount'],4,'.','');
        $toBePaid = number_format(($row['balAmount']*(-1)),4,'.','');
        $date = $row['dtDate'];
        $currency = $row['strCode'];
        $rate = $row['dblRate'];

        $invoiceDetails[$x]['docNumber']=$receipt;
        $invoiceDetails[$x]['date']=$date;
        $invoiceDetails[$x]['amount']=$amount;
        $invoiceDetails[$x]['toBePaid']=$toBePaid;
        $invoiceDetails[$x]['spotRate']=$rate;
        $invoiceDetails[$x]['type']='O.AReceived';
        ++$x;			
    }
    //////////////////////////Credit Note//////////////////////////
    $sql = "SELECT
                    fin_other_receivable_creditnote_header.strCreditNoteNo,
                    CRN.strCreditNoteNo,
                    fin_other_receivable_creditnote_header.intCustomer,
                    fin_other_receivable_creditnote_header.strInvoiceNo,
                    fin_other_receivable_creditnote_header.dtDate,
                    fin_other_receivable_creditnote_header.dblRate AS hRate,
                    fin_other_receivable_creditnote_header.intCurrency,
                    CRN.intItem,
                    CRN.intUOM,
                    CRN.dblQty,
                    CRN.dblDiscount,
                    fin_other_receivable_creditnote_header.strInvoiceNo,
                    mst_financecurrency.intId,
                    mst_financecurrency.strCode,
                    CRN.dblRate,
                    CRN.dblTax,
                    SUM(((CRN.dblQty*CRN.dblRate) *(100-CRN.dblDiscount)/100)+ IFNULL(CRN.dblTax,0)) AS amount,
                    (
                    SUM(((CRN.dblQty*CRN.dblRate) *(100-CRN.dblDiscount)/100)+ IFNULL(CRN.dblTax,0))
                    +
                    IFNULL ((SELECT
                    Sum(fin_other_receivable_payments_main_details.dblPayAmount )AS paidAmount
                    FROM fin_other_receivable_payments_main_details
                    Inner Join fin_other_receivable_payments_header ON fin_other_receivable_payments_main_details.strReferenceNo = fin_other_receivable_payments_header.strReferenceNo
                    WHERE
                    fin_other_receivable_payments_main_details.strJobNo =  CRN.strCreditNoteNo AND
                    fin_other_receivable_payments_header.intDeleteStatus =  '0' AND fin_other_receivable_payments_main_details.strDocType = 'O.CNote'
                    GROUP BY
                    fin_other_receivable_payments_main_details.strJobNo),0)
                    ) AS balAmount
                    FROM
                    fin_other_receivable_creditnote_details AS CRN
                    Inner Join fin_other_receivable_creditnote_header ON CRN.strCreditNoteNo = fin_other_receivable_creditnote_header.strCreditNoteNo
                    Inner Join mst_financecurrency ON fin_other_receivable_creditnote_header.intCurrency = mst_financecurrency.intId
                    WHERE
                    fin_other_receivable_creditnote_header.intCustomer =  '$customerId' AND
                    fin_other_receivable_creditnote_header.intStatus = '1'
                    AND fin_other_receivable_creditnote_header.intCompanyId =  '$companyId' AND
                    mst_financecurrency.intId=$curencyId
                    GROUP BY
                    CRN.strCreditNoteNo
                    having balAmount<>0
                    ";
    $result = $db->RunQuery($sql);
    while($row=mysqli_fetch_array($result))
    {
            $credit = $row['strCreditNoteNo'];
            $invoice =  $row['strInvoiceNo'];
            $amount = number_format($row['amount'],4,'.','');
            $toBePaid = number_format(($row['balAmount']*(-1)),4,'.','');
            $date = $row['dtDate'];
            $currency = $row['strCode'];
            $rate = $row['hRate'];

            $invoiceDetails[$x]['docNumber']=$credit;
            $invoiceDetails[$x]['date']=$date;
            $invoiceDetails[$x]['amount']=$amount;
            $invoiceDetails[$x]['toBePaid']=$toBePaid;
            $invoiceDetails[$x]['spotRate']=$rate;
            $invoiceDetails[$x]['type']='O.CNote';
            ++$x;
    }
    //////////////////////////Bank Deposit/////////////////////////
    $sql = "SELECT
                    BND.strDepositNo,
                    BND.dtDate,
                    BND.intCurrency,
                    fin_bankdeposit_details.intAccount,
                    fin_bankdeposit_details.intRecvFrom,
                    fin_bankdeposit_details.dblAmmount,
                    BND.dblRate,
                    BND.intCompanyId,
                    mst_financecurrency.strCode,
                    mst_financecurrency.intId,
                    mst_financechartofaccounts.intId,
                    mst_financechartofaccounts.intFinancialTypeId,
                    (
                    fin_bankdeposit_details.dblAmmount 
                    +
                    IFNULL ((SELECT
                    Sum(fin_other_receivable_payments_main_details.dblPayAmount )AS paidAmount
                    FROM fin_other_receivable_payments_main_details
                    Inner Join fin_other_receivable_payments_header ON fin_other_receivable_payments_main_details.strReferenceNo = fin_other_receivable_payments_header.strReferenceNo
                    WHERE
                    fin_other_receivable_payments_main_details.strJobNo =  BND.strDepositNo AND
                    fin_other_receivable_payments_header.intDeleteStatus =  '0' AND fin_other_receivable_payments_main_details.strDocType = 'B.Deposit'
                    GROUP BY
                    fin_other_receivable_payments_main_details.strJobNo),0)

                    ) AS balAmount
                    FROM
                    fin_bankdeposit_header BND
                    Inner Join fin_bankdeposit_details ON fin_bankdeposit_details.strDepositNo = BND.strDepositNo
                    Inner Join mst_financecurrency ON BND.intCurrency = mst_financecurrency.intId
                    Inner Join mst_financechartofaccounts ON fin_bankdeposit_details.intAccount = mst_financechartofaccounts.intId
                    WHERE
                    fin_bankdeposit_details.intRecvFrom =  '$customerId' AND
                    (mst_financechartofaccounts.intFinancialTypeId =  '22' OR mst_financechartofaccounts.intFinancialTypeId =  '21' OR mst_financechartofaccounts.intFinancialTypeId =  '11' OR mst_financechartofaccounts.intFinancialTypeId =  '12' OR mst_financechartofaccounts.intFinancialTypeId =  '9') AND
                    BND.intStatus = '1'
                    AND BND.intCompanyId =  '$companyId' AND 
                    mst_financecurrency.intId=$curencyId
                    having balAmount<>0";
    $result = $db->RunQuery($sql);
    while($row=mysqli_fetch_array($result))
    {
        $deposit = $row['strDepositNo'];
        $amount = number_format($row['dblAmmount'],4,'.','');
        $toBePaid = number_format(($row['balAmount']*(-1)),4,'.','');
        $date = substr($row['dtDate'],0,10);        
        $rate = $row['dblRate'];
        
        $invoiceDetails[$x]['docNumber']=$deposit;
        $invoiceDetails[$x]['date']=$date;
        $invoiceDetails[$x]['amount']=$amount;
        $invoiceDetails[$x]['toBePaid']=$toBePaid;
        $invoiceDetails[$x]['spotRate']=$rate;
        $invoiceDetails[$x]['type']='B.Deposit';
        ++$x;            
    }
    //////////////////////////Bank Payments////////////////////////////////
    $sql = "SELECT
                    BNP.strBankPaymentNo,
                    fin_bankpayment_details.intAccountId,
                    BNP.dtDate,
                    BNP.dblRate,
                    BNP.intCurrency,
                    BNP.dblReceivedAmount,
                    mst_financecurrency.intId,
                    mst_financecurrency.strCode,
                    fin_bankpayment_details.strBankPaymentNo,
                    fin_bankpayment_details.intPayTo,
                    mst_financechartofaccounts.intFinancialTypeId,
                    mst_financechartofaccounts.intId,
                    (
                    BNP.dblReceivedAmount 
                    -
                    IFNULL ((SELECT
                    Sum(fin_other_receivable_payments_main_details.dblPayAmount )AS paidAmount
                    FROM fin_other_receivable_payments_main_details
                    Inner Join fin_other_receivable_payments_header ON fin_other_receivable_payments_main_details.strReferenceNo = fin_other_receivable_payments_header.strReferenceNo
                    WHERE
                    fin_other_receivable_payments_main_details.strJobNo =  BNP.strBankPaymentNo AND
                    fin_other_receivable_payments_header.intDeleteStatus =  '0' AND fin_other_receivable_payments_main_details.strDocType = 'B.Payment'
                    GROUP BY
                    fin_other_receivable_payments_main_details.strJobNo),0)

                    ) AS balAmount
                    FROM
                    fin_bankpayment_header BNP
                    Inner Join fin_bankpayment_details ON fin_bankpayment_details.strBankPaymentNo = BNP.strBankPaymentNo
                    Inner Join mst_financecurrency ON BNP.intCurrency = mst_financecurrency.intId
                    Inner Join mst_financechartofaccounts ON fin_bankpayment_details.intAccountId = mst_financechartofaccounts.intId
                    WHERE
                    fin_bankpayment_details.intPayTo =  '$customerId' AND
                    (mst_financechartofaccounts.intFinancialTypeId =  '22' OR mst_financechartofaccounts.intFinancialTypeId =  '21' OR mst_financechartofaccounts.intFinancialTypeId =  '11' OR mst_financechartofaccounts.intFinancialTypeId =  '12' OR mst_financechartofaccounts.intFinancialTypeId =  '9') AND
                    BNP.intStatus = '1'
                    AND BNP.intCompanyId =  '$companyId' AND
                    BNP.intCurrency=$curencyId
                    having balAmount<>0";
    $result = $db->RunQuery($sql);
    while($row=mysqli_fetch_array($result))
    {
            $payment = $row['strBankPaymentNo'];
            $amount = number_format($row['dblReceivedAmount'],4,'.','');
            $toBePaid = number_format($row['balAmount'],4,'.','');
            $date = substr($row['dtDate'],0,10);            
            $rate = $row['dblRate'];
            
            $invoiceDetails[$x]['docNumber']=$payment;
            $invoiceDetails[$x]['date']=$date;
            $invoiceDetails[$x]['amount']=$amount;
            $invoiceDetails[$x]['toBePaid']=$toBePaid;
            $invoiceDetails[$x]['spotRate']=$rate;
            $invoiceDetails[$x]['type']='B.Payment';
            ++$x;             
    }
    //////////////////////////Petty Cash//////////////////////////////////
    $sql = "SELECT
                    PTC.strPettyCashNo,
                    PTC.dtDate,
                    PTC.dblRate,
                    PTC.intCurrency,
                    PTC.dblReceivedAmount,
                    fin_bankpettycash_details.intAccountId,
                    fin_bankpettycash_details.intPayTo,
                    mst_financecurrency.intId,
                    mst_financecurrency.strCode,
                    mst_financechartofaccounts.intId,
                    mst_financechartofaccounts.intFinancialTypeId,
                    (
                    PTC.dblReceivedAmount 
                    -
                    IFNULL ((SELECT
                    Sum(fin_other_receivable_payments_main_details.dblPayAmount )AS paidAmount
                    FROM fin_other_receivable_payments_main_details
                    Inner Join fin_other_receivable_payments_header ON fin_other_receivable_payments_main_details.strReferenceNo = fin_other_receivable_payments_header.strReferenceNo
                    WHERE
                    fin_other_receivable_payments_main_details.strJobNo =  PTC.strPettyCashNo AND
                    fin_other_receivable_payments_header.intDeleteStatus =  '0' AND fin_other_receivable_payments_main_details.strDocType = 'Petty Cash'
                    GROUP BY
                    fin_other_receivable_payments_main_details.strJobNo),0)
                    ) AS balAmount
                    FROM
                    fin_bankpettycash_header AS PTC
                    Inner Join fin_bankpettycash_details ON fin_bankpettycash_details.strPettyCashNo = PTC.strPettyCashNo
                    Inner Join mst_financechartofaccounts ON fin_bankpettycash_details.intAccountId = mst_financechartofaccounts.intId
                    Inner Join mst_financecurrency ON PTC.intCurrency = mst_financecurrency.intId
                    WHERE
                    (mst_financechartofaccounts.intFinancialTypeId =  '22' OR mst_financechartofaccounts.intFinancialTypeId =  '21' OR mst_financechartofaccounts.intFinancialTypeId =  '11' OR mst_financechartofaccounts.intFinancialTypeId =  '12' OR mst_financechartofaccounts.intFinancialTypeId =  '9')
                    AND
                    fin_bankpettycash_details.intPayTo =  '$customerId' AND
                    PTC.intStatus = '1'
                    AND PTC.intCompanyId =  '$companyId' AND
                    PTC.intCurrency=$curencyId
                    having balAmount<>0";
    $result = $db->RunQuery($sql);
    while($row=mysqli_fetch_array($result))
    {
            $pettyCash = $row['strPettyCashNo'];
            $amount = number_format($row['dblReceivedAmount'],4,'.','');
            $toBePaid = number_format($row['balAmount'],4,'.','');
            $date = substr($row['dtDate'],0,10);            
            $rate = $row['dblRate'];
            
            $invoiceDetails[$x]['docNumber']=$pettyCash;
            $invoiceDetails[$x]['date']=$date;
            $invoiceDetails[$x]['amount']=$amount;
            $invoiceDetails[$x]['toBePaid']=$toBePaid;
            $invoiceDetails[$x]['spotRate']=$rate;
            $invoiceDetails[$x]['type']='Petty Cash';
            ++$x;            
    }
    //////////////////////////Jurnel Entry////////////////////////////////
    $sql = "SELECT
                    JH.strReferenceNo,
                    JH.dtmDate,
                    JH.dblRate,
                    JH.intCurrencyId,
                    mst_financecurrency.strCode,
                    JD.dblDebitAmount,
                    JD.dbCreditAmount,
                    (
                    JD.dblDebitAmount + JD.dbCreditAmount -
                    IFNULL((SELECT
                                    Sum(fin_other_receivable_payments_main_details.dblPayAmount )AS paidAmount
                            FROM fin_other_receivable_payments_main_details
                                    Inner Join fin_other_receivable_payments_header ON fin_other_receivable_payments_main_details.strReferenceNo = fin_other_receivable_payments_header.strReferenceNo
                            WHERE
                                    fin_other_receivable_payments_main_details.strJobNo =  JH.strReferenceNo AND
                                    fin_other_receivable_payments_header.intDeleteStatus =  '0' AND fin_other_receivable_payments_main_details.strDocType = 'JN'
                            GROUP BY
                                    fin_other_receivable_payments_main_details.strJobNo),0)
                    ) AS balAmount
                    FROM
                    fin_accountant_journal_entry_header AS JH
                    INNER JOIN fin_accountant_journal_entry_details AS JD ON JH.intEntryNo = JD.intEntryNo
                    INNER JOIN mst_financecurrency ON JH.intCurrencyId = mst_financecurrency.intId
                    INNER JOIN mst_financechartofaccounts ON JD.intChartOfAccountId = mst_financechartofaccounts.intId
                    WHERE
                    JD.strPersonType = 'ocus' AND
                    JD.intNameId = $customerId AND
                    JH.intDeleteStatus = 0 AND
                    (mst_financechartofaccounts.intFinancialTypeId =  '22' OR mst_financechartofaccounts.intFinancialTypeId =  '21' OR mst_financechartofaccounts.intFinancialTypeId =  '11' OR mst_financechartofaccounts.intFinancialTypeId =  '12' OR mst_financechartofaccounts.intFinancialTypeId =  '9')
                    AND JH.intCompanyId =  '$companyId' AND
                    JH.intCurrencyId=$curencyId
                    HAVING
                    balAmount <> 0";
    $result = $db->RunQuery($sql);
    while($row=mysqli_fetch_array($result))
    {
            $payment = $row['strReferenceNo'];
            $amount = number_format(($row['dblDebitAmount'] + $row['dbCreditAmount']),4,'.','');

            if($row['dbCreditAmount']!=0){
                $toBePaid = number_format(($row['balAmount']*(-1)),4,'.','');
            }
            else{
                $toBePaid = number_format(($row['balAmount']),4,'.','');
            }                       

            $date = substr($row['dtmDate'],0,10);            
            $rate = $row['dblRate'];
            
            $invoiceDetails[$x]['docNumber']=$payment;
            $invoiceDetails[$x]['date']=$date;
            $invoiceDetails[$x]['amount']=$amount;
            $invoiceDetails[$x]['toBePaid']=$toBePaid;
            $invoiceDetails[$x]['spotRate']=$rate;
            $invoiceDetails[$x]['type']='JN';
            ++$x;            
    }
    return $invoiceDetails;                
}
?>

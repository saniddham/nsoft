<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$companyId = $_SESSION['headCompanyId'];
$location = $_SESSION['CompanyID'];
$thisFilePath = $_SERVER['PHP_SELF'];
include "{$backwardseperator}dataAccess/permisionCheck.inc";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Delete Other Receivable / Service Customer - Unrealize Gain/Loss</title>

        <link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/tblstyle.css" rel="stylesheet" type="text/css" />

        <script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
        <script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
        <script src="DeleteunadjustedGainLoss.js" type="text/javascript"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

        <link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
        <script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

        <link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css"/>
        <link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css"/> 
        </head>
    <body>
        <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
                <td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator . 'Header.php'; ?></td>
            </tr> 
        </table>
        
        <div id="divMaskProces" class="maskHide mask"><img src="loading.gif" width="319" height="305" alt="loading"/>
        </div>

        <script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
        <script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
        <script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>
        <form id="frmDeleteCustUnadjustedGL" name="frmDeleteCustUnadjustedGL" method="post" autocomplete="off">
            <div align="center">
                <div class="trans_layoutL">
                    <div class="trans_text">Delete Other Receivable / Service Customer - Unrealize Gain/Loss</div>
                    <table>
                        <tr>
                            <td  class="normalfnt">Process <span class="compulsoryRed">*</span></td>
                            <td class="normalfnt">
                                <select name="cboProcess" id="cboProcess"  style="width:100px" class="validate[required]" >
                                <option value=""></option>
                                    <?php   $sql = "SELECT
                                                        H.processId,H.prosDate
                                                        FROM
                                                        fin_unadjusted_gl_header AS H
                                                        WHERE
                                                        H.`type` =  'Other Receiveble' AND H.`status` =  '1'";
                                            $result = $db->RunQuery($sql);
                                            while($row=mysqli_fetch_array($result)){
                                                echo "<option value=\"".$row['processId']."\">".$row['prosDate']." - ".$row['processId']."</option>";
                                            }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2"><img border="0" src="../../../../images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="25"/></td>
                        </tr>
                    </table>
                </div>
            </div>
        </form>
<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$locationId = $_SESSION["CompanyID"];
$companyId	= $_SESSION["headCompanyId"];

$thisFilePath =  $_SERVER['PHP_SELF'];
include  "{$backwardseperator}dataAccess/permisionCheck.inc";

$creditNo = $_REQUEST['creditNo'];
if($creditNo){
$creditNo = $_REQUEST['creditNo'];
}
else{
$creditNo = $_GET['cboSearch'];
}

//$depositNo = '5000003';

		  $sql = "SELECT
					fin_other_receivable_creditnote_header.strCreditNoteNo,
					fin_other_receivable_creditnote_header.strInvoiceNo,
					fin_other_receivable_creditnote_header.intCustomer,
					fin_other_receivable_creditnote_header.dtDate,
					fin_other_receivable_creditnote_header.dblRate,
					fin_other_receivable_creditnote_header.intCurrency,
					fin_other_receivable_creditnote_header.strRemarks,
					fin_other_receivable_creditnote_header.strPoNo,
					fin_other_receivable_creditnote_header.intMarketingUser,
					fin_other_receivable_creditnote_header.intStatus
					FROM fin_other_receivable_creditnote_header
					WHERE
					fin_other_receivable_creditnote_header.strCreditNoteNo =  '$creditNo'";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$customer = $row['intCustomer'];
					$date = $row['dtDate'];
					$remarks = $row['strRemarks'];
					$currency = $row['intCurrency'];
					$rate = $row['dblRate'];
					if($rate==0){
						$rate='';
					}
					$invNo = $row['strInvoiceNo'];
					$poNo = $row['strPoNo'];
					$rep = $row['intMarketingUser'];
						$sql2 = "SELECT 	mst_finance_service_customer.strAddress FROM mst_finance_service_customer	WHERE mst_finance_service_customer.intId =  '$customer'";
						$result2 = $db->RunQuery($sql2);
						$row2 = mysqli_fetch_array($result2);
					$invAddress =  $row2['strAddress'];
				 }
				 
	$sql = "SELECT
			fin_other_receivable_invoice_header.strReferenceNo,
			fin_other_receivable_invoice_header.intDeleteStatus
			FROM fin_other_receivable_invoice_header
			WHERE
			fin_other_receivable_invoice_header.intCustomerId =  '$customer' AND
			fin_other_receivable_invoice_header.intCompanyId =  '$companyId' AND
			fin_other_receivable_invoice_header.intDeleteStatus =  '0'
			";
	$result = $db->RunQuery($sql);
	$salesInvoiceList ='<option value=""></option>';
	while($row = mysqli_fetch_array($result))
	{
		if($row['strReferenceNo']==$invNo){
		$salesInvoiceList .="<option value=\"".$row['strReferenceNo']."\" selected=\"selected\">".$row['strReferenceNo']."</option>";	
		}
		else{
		$salesInvoiceList .="<option value=\"".$row['strReferenceNo']."\">".$row['strReferenceNo']."</option>";	
		}
	}
// ======================Check Exchange Rate Updates========================
if($invoiceRefNo == "")
{
	$status = "Adding";
}
else
{
	$status = "Changing";
}
$currentDate = date("Y-m-d");

$sql = "SELECT COUNT(*) AS 'no'
		FROM
		mst_financeexchangerate
		WHERE
		mst_financeexchangerate.dtmDate =  '$currentDate'
		";
$result = $db->RunQuery($sql);
$row= mysqli_fetch_array($result);
if($row['no']==0)
	{
		$str =  "Please Update Exchange Rates Before ".$status." Other Credit Note .";
		$str .= $row['NameList'];
		$maskClass="maskShow";
	}
	else
	{
		$maskClass="maskHide";
	}
// =========================================================================
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Other Receivable / Service Customer - Credit Note</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $backwardseperator; ?>css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">


<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script src="creditNote-js.js" type="text/javascript"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>
<script src="../../commanFunctions/numberExisting-js.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
</head>

<body onLoad="functionList();">
<form id="frmCreditNote" name="frmCreditNote" method="get" action="creditNote.php" autocomplete="off">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<div id="divMask" class="<?php echo $maskClass?> mask">  <?php echo $str; ?></div>

<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<div align="center">
  <div class="trans_layoutL" style="width:1000">
    <div class="trans_text">Other Receivable / Service Customer - Credit Note</div>
<table width="100%" cellspacing="0">
      <tr>
      <td class="normalfnt" width="50%"><img src="../../../../images/fb.png" width="18" height="19" /></td>
      <td align="right" width="50%"><img src="../../../../images/ff.png" width="18" height="19" /></td>
    </tr>
    <tr>
    <td colspan="2" align="left">

    </td>  
    </tr>
    <tr>
    <td  colspan="2">
    <table width="100%">
    <tr>
    <td align="right" width="36%"><span class="normalfnt"><strong>Search</strong></span></td>
    <td align="left" width="64%"><select name="cboSearch" id="cboSearch"  style="width:350px" onchange="submit();" >
          <option value=""></option>
          <?php  $sql = "SELECT DISTINCT
							fin_other_receivable_creditnote_header.strCreditNoteNo,
							mst_finance_service_customer.strName,
							fin_other_receivable_creditnote_header.intStatus 
							FROM
							fin_other_receivable_creditnote_header
							Inner Join mst_finance_service_customer ON fin_other_receivable_creditnote_header.intCustomer = mst_finance_service_customer.intId
							where fin_other_receivable_creditnote_header.intStatus='1'
							AND fin_other_receivable_creditnote_header.intCompanyId =  '$companyId'
							order by strCreditNoteNo desc";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							if($row['strCreditNoteNo']==$creditNo)
							echo "<option value=\"".$row['strCreditNoteNo']."\" selected=\"selected\">".$row['strCreditNoteNo']." - ".$row['strName']."</option>";	
							else
							echo "<option value=\"".$row['strCreditNoteNo']."\">".$row['strCreditNoteNo']." - ".$row['strName']."</option>";
						}
          ?>
        </select></td>
    </tr>
    <tr>
      <td align="right">&nbsp;</td>
      <td align="left">&nbsp;</td>
    </tr>
    </table>    </span></td>
    </tr>
    <tr>
      <td colspan="2">
      <table width="100%" class="tableBorder_allRound">
    <tr>
      <td class="normalfnt">&nbsp;</td>
      <td class="normalfnt">Credit Number</td>
      <td colspan="2"><input name="txtCreditNo" type="text" readonly="readonly" class="normalfntRight" id="txtCreditNo" style="width:230px; background-color:#F4FFFF;text-align:center; border:dotted; border-color:#F00" value="<?php echo $creditNo; ?>" onblur="numberExisting(this,'Other Credit Note');" />
        <input checked="checked" type="checkbox" name="chkAutoManual" id="chkAutoManual" />
        <input name="amStatus" type="text" class="normalfntBlue" id="amStatus" style="width:40px; background-color:#FFF; text-align:center; border:thin" disabled="disabled" value="(Auto)"/></td>
      <td bgcolor="#FFFFFF" class="">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
      <!-- ========Add by dulakshi 2013.03.20============== -->  
     <tr>
      <td width="37" class="normalfnt">&nbsp;</td>
      <td width="144" class="normalfnt">Ledger Accounts </td>
      <td width="242"><span class="normalfnt">
        <select name="cboLedgerAcc" id="cboLedgerAcc"  style="width:96%" onchange="getSupplierList();">
          <option value=""></option>
          <?php  $sql = "SELECT DISTINCT
								mst_financechartofaccounts.intId,
								mst_financechartofaccounts.strCode,
								mst_financechartofaccounts.strName
						 FROM
								mst_financechartofaccounts
								Inner Join mst_finance_service_customer_activate ON mst_financechartofaccounts.intId = mst_finance_service_customer_activate.intChartOfAccountId
						 WHERE
								mst_finance_service_customer_activate.intCompanyId =  '$companyId'
						 GROUP BY
								mst_financechartofaccounts.intId
						 ORDER BY
								mst_financechartofaccounts.strCode ASC";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
						}
          ?>
        </select>
      </span></td>
      
    </tr>
    <!-- ================================ -->   
    <tr>
      <td width="37" class="normalfnt">&nbsp;</td>
      <td width="144" class="normalfnt">Service Customer <span class="compulsoryRed">*</span></td>
      <td width="242"><span class="normalfnt">
        <select name="cboCustomer" class="validate[required]" id="cboCustomer"  style="width:96%" >
          <option value=""></option>
          <?php  $sql = "SELECT
							mst_finance_service_customer.intId,
							mst_finance_service_customer.strName,
							mst_finance_service_customer_activate.intCompanyId
						FROM
							mst_finance_service_customer
							Inner Join mst_finance_service_customer_activate ON mst_finance_service_customer.intId = mst_finance_service_customer_activate.intCustomerId
						WHERE
							mst_finance_service_customer.intStatus =  '1' AND
							mst_finance_service_customer_activate.intCompanyId = '$companyId' 					
						order by mst_finance_service_customer.strName ASC ";          
          
						
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							if($row['intId']==$customer)
							echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";	
							else
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
						}
          ?>
        </select>
      </span></td>     
      
      <td width="176"><span class="normalfnt">Date <span class="compulsoryRed">*</span></span></td>
      <td width="360" bgcolor="#FFFFFF" class=""><input name="txtDate" type="text" value="<?Php echo ($date==''?date("Y-m-d"):$date) ?>" class="txtbox" id="txtDate" style="width:98px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" onblur="backDateExisting(this,'Other Credit Note');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
      <td width="65">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="normalfnt">Invoice Address</span></td>
      <td><textarea disabled="disabled" name="txtAddress" id="txtAddress" style="width:230px" cols="30" rows="2"><?php echo $invAddress ?></textarea></td>
      <td><span class="normalfnt">Remarks</span></td>
      <td><textarea name="txtRemarks" id="txtRemarks" cols="45" rows="2"><?php echo $remarks; ?></textarea></td>
      <td>&nbsp;</td>
    </tr>
<tr>
      <td>&nbsp;</td>
      <td><span class="normalfnt">Currency <span class="compulsoryRed">*</span></span></td>
      <td><select name="cboCurrency" id="cboCurrency" style="width:230px" class="validate[required]">
        <option value=""></option>
        <?php  $sql = "SELECT
						intId,
						strCode
						FROM mst_financecurrency
						WHERE
							intStatus = 1
						order by strCode
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							if($row['intId']==$currency)
							echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strCode']."</option>";	
							else
							echo "<option value=\"".$row['intId']."\">".$row['strCode']."</option>";
						}
        				?>
      </select></td>
      <td><span class="normalfnt">Rate <span class="compulsoryRed">*</span></span></td>
      <td><span class="normalfntMid">
        <input class="rdoRate" type="radio" name="rate" id="rdoSelling" value="0" />
        Selling
        <input class="rdoRate" type="radio" name="rate" id="rdoBuying" value="0" />
        Buying 
        <input class="rdoRate" type="radio" name="rate" id="rdoAverage" value="0" />
		Average
<input type="text" name="txtRate" id="txtRate" style="width:75px; text-align:right" disabled="disabled" value="<?php echo $rate ?>"   class="validate[required,custom[number]]"/>
        <input type="checkbox" name="chkEdit" id="chkEdit" />
        </span></td>
      <td>&nbsp;</td>
      </tr>    <tr>
      <td>&nbsp;</td>
      <td><span class="normalfnt">Invoice Number <span class="compulsoryRed">*</span></span></td>
      <td colspan="2">
        <select name="cboInvoiceNo" id="cboInvoiceNo" style="width:230px"  class="validate[required]">
          <?php echo $salesInvoiceList; ?>
        </select>
      </td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
      <tr id="rwInvType" style="display:none;background-color:#00CCFF">
        <td>&nbsp;</td>
        <td><span class="normalfnt">Invoice Type</span></td>
        <td colspan="2"><span class="normalfnt">
          <input disabled="disabled" type="radio" class="invTpe" name="rdInvoType" id="Commercial" value="Commercial" />
            Commercial
            <input disabled="disabled" type="radio"  class="invTpe" name="rdInvoType" id="Tax" value="Tax" />
            Tax
            <input disabled="disabled" type="radio"  class="invTpe" name="rdInvoType" id="SVAT" value="SVAT" />
            SVAT
            <input disabled="disabled" type="radio"  class="invTpe" name="rdInvoType" id="NFE" value="NFE" />
NFE-Suspended</span></td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
      </tr>
      </table>
      </td>
      </tr>
    <tr>
      <td colspan="2">
      <table width="100%" class="tableBorder_allRound">
      <tr>
      <td align="center">
		<table width="37%" id="tblMainGrid1" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
        <tr class="">
          <td width="80"  height="26" bgcolor="#FAD163" class="normalfntMid"><strong>P.O No.</strong></td>
          <td width="249"  bgcolor="#FAD163" class="normalfntMid"><strong>Rep</strong></td>
          </tr>
        <tr class="normalfnt">
          <td bgcolor="#FFFFFF" class="normalfntMid"><input type="text" name="txtPONo" id="txtPONo" style="width:80px"  class="" value="<?php echo $poNo ?>"/></td>
          <td bgcolor="#FFFFFF"><span class="normalfntMid">
            <select name="cboMarketer" id="cboMarketer" style="width:250px"  class=" ">
             <option value=""></option>
		   <?php  $sql = "	SELECT
								sys_users.intUserId,
								sys_users.strUserName
							FROM
								mst_marketer
								Inner Join sys_users ON mst_marketer.intUserId = sys_users.intUserId
							order by strUserName
							";
            $result = $db->RunQuery($sql);
            while($row=mysqli_fetch_array($result))
            {
							if($row['intUserId']==$rep)
							echo "<option value=\"".$row['intUserId']."\" selected=\"selected\">".$row['strUserName']."</option>";	
							else
                echo "<option value=\"".$row['intUserId']."\">".$row['strUserName']."</option>";
            }
          ?>
              </select>
          </span></td>
          </tr>
      </table>
      </td>
      </tr>
      </table>
      </td>
      </tr>
            <tr>
      <td colspan="2"  align="right" valign="bottom"><img id="butInsertRow" src="../../../../images/Tadd.jpg" width="92" height="24" class="mouseover" /></td>
      </tr>
    <tr>
      <td colspan="2"><table width="100%">
        <tr>
          <td>
          <div style="overflow:scroll;width:1000px;height:200px;" id="divGrid">
          <table width="100%" id="tblMain" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
            <tr class="">
              <td width="3%" bgcolor="#FAD163" class="normalfntMid">Del</td>
              <td width="13%" height="27" bgcolor="#FAD163" class="normalfntMid"><strong>Item <span class="compulsoryRed">*</span></strong></td>
              <td width="30%"    bgcolor="#FAD163" class="normalfntMid"><strong>Description</strong></td>
              <td width="8%" bgcolor="#FAD163" class="normalfntMid"><strong>UOM <span class="compulsoryRed">*</span></strong></td>
              <td width="8%" bgcolor="#FAD163" class="normalfntMid"><strong>Qty <span class="compulsoryRed">*</span></strong></td>
              <td  width="11%" bgcolor="#FAD163" class="normalfntMid"  ><strong>U.P</strong></td>
              <td width="9%" bgcolor="#FAD163" class="normalfntMid"><strong>Discount %</strong></td>
              <td width="11%" bgcolor="#FAD163" class="normalfntMid"  ><strong>Amount <span class="compulsoryRed">*</span></strong></td>
              <td width="11%" bgcolor="#FAD163" class="normalfntMid"  ><strong>Tax</strong></td>
              <td width="11%" bgcolor="#FAD163" class="normalfntMid"  ><strong><span class="compulsoryRed">*</span> Cost Center</strong></td>
              <td width="11%" bgcolor="#FAD163" class="normalfntMid" style="display:none"><strong>Tax With</strong></td>
            </tr>
            <?php
	 $sqlItems = "SELECT
				fin_other_receivable_invoice_details.intItem,
				mst_financecustomeritem.strName
			FROM
				fin_other_receivable_invoice_details
				Inner Join mst_financecustomeritem ON mst_financecustomeritem.intId = fin_other_receivable_invoice_details.intItem
			WHERE
				fin_other_receivable_invoice_details.strReferenceNo =  '$invNo'
			ORDER BY
				mst_financecustomeritem.intStatus ASC
			";
			
	$sql = "SELECT
			fin_other_receivable_creditnote_details.strCreditNoteNo,
			fin_other_receivable_creditnote_details.intAccountId,
			fin_other_receivable_creditnote_details.intItem,
			fin_other_receivable_creditnote_details.intUOM,
			fin_other_receivable_creditnote_details.dblQty,
			fin_other_receivable_creditnote_details.dblRate,
			fin_other_receivable_creditnote_details.dblDiscount,
			fin_other_receivable_creditnote_details.intTaxGroup,
			fin_other_receivable_creditnote_details.dblTax,
			fin_other_receivable_creditnote_details.intDimension,
			fin_other_receivable_creditnote_details.strStyleNo,
			fin_other_receivable_creditnote_details.strGraphicNo,
			fin_other_receivable_creditnote_details.strOrderNo,
			fin_other_receivable_creditnote_details.strLineItem,
			fin_other_receivable_creditnote_details.strCusLocation,
			fin_other_receivable_creditnote_details.strItemDesc 
			FROM fin_other_receivable_creditnote_details
			WHERE
			fin_other_receivable_creditnote_details.strCreditNoteNo =  '$creditNo'";
			
 	$exsisting=0;
	$result = $db->RunQuery($sql);
		$subTot=0.00;
		$taxSum=0.00;
		$tot=0.00;
	while($row = mysqli_fetch_array($result))
	{
		$exsisting++;
		$item=$row['intItem'];
		$uom=$row['intUOM'];
		$qty=$row['dblQty'];
		$up=$row['dblRate'];
		$discount=$row['dblDiscount'];
		$amount=$row['dblQty']*$row['dblRate']*(100-$discount)/100;
		$tax=$row['intTaxGroup'];
		$dimention=$row['intDimension'];
		$taxVal=$row['dblTax'];
		
		$styleNo=$row['strStyleNo'];
		$graphicNo=$row['strGraphicNo'];
		$salesNo=$row['strOrderNo'];
		$lineItem=$row['strLineItem'];
		$custLocation=$row['strCusLocation'];
		$itemDesc=$row['strItemDesc'];
		
		
		$subTot+=$amount;
		$taxSum+=$taxVal;
		?>
<tr class="normalfnt mainRow">
              <td bgcolor="#FFFFFF" class="normalfntMid"><img class="mouseover delImg" src="../../../../images/del.png" width="15" height="15" /></td>
              <td bgcolor="#FFFFFF" class="normalfntMid"><select name="cboItem" id="cboItem" style="width:110px" class="validate[required] item">
                <option value=""></option>
                       <?php  
						$resultItems = $db->RunQuery($sqlItems);
						while($rowItm=mysqli_fetch_array($resultItems))
						{
							if($rowItm['intItem']==$item)
							echo "<option value=\"".$rowItm['intItem']."\" selected=\"selected\">".$rowItm['strName']."</option>";	
							else
							echo "<option value=\"".$rowItm['intItem']."\">".$rowItm['strName']."</option>";
						}
        				?>
              </select></td>
              <td bgcolor="#FFFFFF">

              <table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900" id="tblSubGrid">
              <tr class="normalfnt" style="display:none">
              <td width="55%" bgcolor="#FFFFFF">Style No</td>
              <td width="45%" bgcolor="#FFFFFF"><input type="text" name="txtStyleNo" id="txtStyleNo" style="width:100%;text-align:center" class="styleNo" value="<?php echo $styleNo?>" /></td>
              </tr>
              <tr class="normalfnt" style="display:none">
              <td bgcolor="#FFFFFF">Graphic No</td>
              <td width="45%" bgcolor="#FFFFFF"><input type="text" name="txtGraphicNo" id="txtGraphicNo" style="width:100%;text-align:center" class="graphicNo" value="<?php echo $graphicNo?>"/></td>
              </tr>
              <tr class="normalfnt" style="display:none">
              <td bgcolor="#FFFFFF">Sales Order No / Schedule No</td>
              <td width="45%" bgcolor="#FFFFFF"><input type="text" name="txtSalesNo" id="txtSalesNo" style="width:100%;text-align:center" class="orderNo" value="<?php echo $salesNo?>"/></td>
              </tr>
              <tr class="normalfnt" style="display:none">
              <td bgcolor="#FFFFFF">Line Item</td>
              <td width="45%" bgcolor="#FFFFFF"><input type="text" name="txtLineItem" id="txtLineItem" style="width:100%;text-align:center" class="lineItem" value="<?php echo $lineItem?>"/></td>
              </tr>
              <tr class="normalfnt" style="display:none">
              <td bgcolor="#FFFFFF">Customer Location</td>
              <td width="45%" bgcolor="#FFFFFF"><input type="text" name="txtLocationNo" id="txtLocationNo" style="width:100%;text-align:center" class="cusLocate" value="<?php echo $custLocation?>"/></td>
              </tr>
              <tr class="normalfnt">
                <td bgcolor="#FFFFFF" style="display:none">Item Desc.</td>
                <td bgcolor="#FFFFFF"><input type="text" name="txtItemMemo" id="txtItemMemo" style="width:100%;text-align:center" class="itemDesc" value="<?php echo $itemDesc?>" /></td>
              </tr>
              </table>              </td>
              <td bgcolor="#FFFFFF"><select name="cboUOM" class="validate[required] uom" id="cboUOM" style="width:75px" >
                <option value=""></option>
                <?php  $sqlu = "SELECT
								intId,
								strName
								FROM mst_units
									WHERE intStatus=1
										order by strName";
										
								$resultu = $db->RunQuery($sqlu);
								while($rowu=mysqli_fetch_array($resultu))
								{
									if($rowu['intId']==$uom)
									echo "<option value=\"".$rowu['intId']."\" selected=\"selected\">".$rowu['strName']."</option>";	
									else
									echo "<option value=\"".$rowu['intId']."\">".$rowu['strName']."</option>";
								}
                   		?>
              </select></td>
              <td bgcolor="#FFFFFF" class="normalfntMid"><input name="txtQty" type="text" id="txtQty" style="width:60px; text-align:right"  class="validate[required,custom[number]] ammount qty" value="<?php echo $qty ?>"/></td>
              <td align="center" bgcolor="#FFFFFF" class="normalfntMid"><input name="txtRate" type="text"   class="validate[required,custom[number]] ammount rate" id="txtRate" style="width:60px; text-align:right"  value="<?php echo $up ?>"/></td>
              <td  bgcolor="#FFFFFF" class="normalfntMid"><input name="txtDiscount" type="text" class="validate[custom[number],max[100]] ammount discount" id="txtDiscount" style="width:55px; text-align:right" value="<?php echo $discount ?>" /></td>
              <td  bgcolor="#FFFFFF" class="normalfntMid"><input name="txtAmount" type="text" class="validate[required,custom[number]] ammount amm" id="txtAmount" style="width:70px; text-align:right" disabled="disabled" value="<?php echo $amount ?>" /></td>
              <td  bgcolor="#FFFFFF"><select name="cboTax"  id="cboTax"  style="width:60px;" class="validate[custom[number]] taxGroup">
                <option value=""></option>
                <?php  $sqlt = "SELECT
								mst_financetaxgroup.intId,
								mst_financetaxgroup.strCode,
								mst_financetaxgroup.intStatus
								FROM
								mst_financetaxgroup
								WHERE
									intStatus = 1
								order by strCode
								";
								$resultt = $db->RunQuery($sqlt);
								while($rowt=mysqli_fetch_array($resultt))
								{
								if($rowt['intId']==$tax)
									echo "<option value=\"".$rowt['intId']."\" selected=\"selected\">".$rowt['strCode']."</option>";	
								else
									echo "<option value=\"".$rowt['intId']."\">".$rowt['strCode']."</option>";
								}
                   ?>
              </select></td>
              <td  bgcolor="#FFFFFF"><select name="cboDimension"  class="" id="cboDimension"  style="width:100px;">
                <option value=""></option>
                <?php  $sqld = "SELECT
									intId,
									strName
								FROM mst_financedimension
								WHERE
									intStatus = 1
								order by strName
								";
								$resultd = $db->RunQuery($sqld);
								while($rowd=mysqli_fetch_array($resultd))
								{
								if($rowd['intId']==$dimention)
									echo "<option value=\"".$rowd['intId']."\" selected=\"selected\">".$rowd['strName']."</option>";	
								else
									echo "<option value=\"".$rowd['intId']."\">".$rowd['strName']."</option>";
								}
                   ?>
              </select></td>
              <td  bgcolor="#FFFFFF" style="display:none"><input type="text" name="txtTaxWith" id="" style="width:80px; background-color:#F4FFFF; text-align:right" class="validate[custom[number]] taxWith" disabled="disabled" value="<?php // echo $taxVal; ?>"/></td>
            </tr>	<?php }
			
            if($exsisting==0){?>
            <tr class="normalfnt mainRow">
              <td bgcolor="#FFFFFF" class="normalfntMid"><img class="mouseover delImg" src="../../../../images/del.png" width="15" height="15" /></td>
              <td bgcolor="#FFFFFF" class="normalfntMid"><select name="cboItem" id="cboItem" style="width:110px" class="validate[required] item">
                <option value=""></option>
                <?php  $sql = "SELECT
						intId,
						strName
						FROM mst_financecustomeritem
						WHERE
							intStatus = 1
						order by strName
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							//echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
						}
        				?>
              </select></td>
              <td bgcolor="#FFFFFF">

              <table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900" id="tblSubGrid">
              <tr class="normalfnt" style="display:none">
              <td width="55%" bgcolor="#FFFFFF">Style No</td>
              <td width="45%" bgcolor="#FFFFFF"><input type="text" name="txtStyleNo" id="txtStyleNo" style="width:100%;text-align:center" class="styleNo" /></td>
              </tr>
              <tr class="normalfnt" style="display:none">
              <td bgcolor="#FFFFFF">Graphic No</td>
              <td width="45%" bgcolor="#FFFFFF"><input type="text" name="txtGraphicNo" id="txtGraphicNo" style="width:100%;text-align:center" class="graphicNo"/></td>
              </tr>
              <tr class="normalfnt" style="display:none">
              <td bgcolor="#FFFFFF">Sales Order No / Schedule No</td>
              <td width="45%" bgcolor="#FFFFFF"><input type="text" name="txtSalesNo" id="txtSalesNo" style="width:100%;text-align:center" class="orderNo"/></td>
              </tr>
              <tr class="normalfnt" style="display:none">
              <td bgcolor="#FFFFFF">Line Item</td>
              <td width="45%" bgcolor="#FFFFFF"><input type="text" name="txtLineItem" id="txtLineItem" style="width:100%;text-align:center" class="lineItem"/></td>
              </tr>
              <tr class="normalfnt" style="display:none">
              <td bgcolor="#FFFFFF">Customer Location</td>
              <td width="45%" bgcolor="#FFFFFF"><input type="text" name="txtLocationNo" id="txtLocationNo" style="width:100%;text-align:center" class="cusLocate"/></td>
              </tr>
              <tr class="normalfnt">
              <td bgcolor="#FFFFFF" style="display:none">Item Desc.</td>
              <td bgcolor="#FFFFFF"><input type="text" name="txtItemMemo" id="txtItemMemo" style="width:100%;text-align:center" class="itemDesc" /></td>
              </tr>
              </table>              </td>
              <td bgcolor="#FFFFFF"><select name="cboUOM" class="validate[required] uom" id="cboUOM" style="width:75px" >
                <option value=""></option>
                <?php  $sql = "SELECT
								intId,
								strName
								FROM mst_units
									WHERE intStatus=1
										order by strName";
										
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
								}
                   		?>
              </select></td>
              <td bgcolor="#FFFFFF" class="normalfntMid"><input name="txtQty" type="text" id="txtQty" style="width:60px; text-align:right"  class="validate[required,custom[number]] ammount qty"/></td>
              <td align="center" bgcolor="#FFFFFF" class="normalfntMid"><input name="txtRate" type="text"   class="validate[required,custom[number]] ammount rate" id="txtRate" style="width:60px; text-align:right"  /></td>
              <td  bgcolor="#FFFFFF" class="normalfntMid"><input name="txtDiscount" type="text" class="validate[custom[number],max[100]] ammount discount" id="txtDiscount" style="width:55px; text-align:right" /></td>
              <td  bgcolor="#FFFFFF" class="normalfntMid"><input name="txtAmount" type="text" class="validate[required,custom[number]] ammount amm" id="txtAmount" style="width:70px; text-align:right" disabled="disabled" value="" /></td>
              <td  bgcolor="#FFFFFF"><select name="cboTax"  id="cboTax"  style="width:60px;" class="validate[custom[number]] taxGroup">
                <option value=""></option>
                <?php  $sql = "SELECT
								mst_financetaxgroup.intId,
								mst_financetaxgroup.strCode,
								mst_financetaxgroup.intStatus
								FROM
								mst_financetaxgroup
								WHERE
									intStatus = 1
								order by strCode
								";
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intId']."\">".$row['strCode']."</option>";
								}
                   ?>
              </select></td>
              <td  bgcolor="#FFFFFF"><select name="cboDimension"  class="" id="cboDimension"  style="width:100px;">
                <option value=""></option>
                <?php  $sql = "SELECT
									intId,
									strName
								FROM mst_financedimension
								WHERE
									intStatus = 1
								order by strName
								";
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
								}
                   ?>
              </select></td>
              <td  bgcolor="#FFFFFF" style="display:none"><input type="text" name="txtTaxWith" id="" style="width:80px; background-color:#F4FFFF; text-align:right" class="validate[custom[number]] taxWith" disabled="disabled" value="<?php // echo $taxAmmount; ?>"/></td>
            </tr>
            <?php } ?>
          </table>
                </div>
          </td>
        </tr>
      </table></td>
      </tr>
      <tr>
    	<td colspan="2">
    	<table width="100%">
        <tr>
        <td width="9%">&nbsp;</td>
        <td width="68%">&nbsp;</td>
        <td width="23%">
        <table width="100%">
        <?php
			 $tot=$subTot+$taxSum;
		?>
        <tr>
        <td width="111" align="right"><span class="normalfnt">Sub-Total</span></td>
        <td width="80" align="right"><span class="normalfntMid">
          <input name="txtSubTotal" type="text" disabled="disabled" id="txtSubTotal" style="width:75px; text-align:right" value="<?php echo $subTot ; ?>" />
        </span></td>
        </tr>
        <tr>
          <td align="right"><span class="normalfnt">Tax</span></td>
          <td align="right"><span class="normalfntMid">
            <input name="txtTotalTax" type="text" disabled="disabled" id="txtTotalTax" style="width:75px; text-align:right"  value="<?php echo $taxSum ; ?>" />
          </span></td>
        </tr>
        <tr>
          <td align="right"><span class="normalfnt">Total</span></td>
          <td align="right"><span class="normalfntMid">
            <input name="txtTotal" type="text" disabled="disabled" id="txtTotal" style="width:75px; text-align:right" value="<?php echo $tot ; ?>"/>
          </span></td>
        </tr>
        </table>
        </td>
        </tr>
        </table>
    	</td>
      </tr>
    <tr>
      <td colspan="2">
        <table width="100%">
          <tr>
            <td width="100%" height="34" class="tableBorder_allRound"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                  <td width="100%" align="center" bgcolor=""><img border="0" src="../../../../images/Tnew.jpg" alt="New" name="butNew" width="92" height="24" class="mouseover" id="butNew" tabindex="28"/><img src="../../../../images/Tsave.jpg" width="92" height="24" id="butSave" name="butSave"  class="mouseover" /><img style="display:none" border="0" src="../../../../images/Tprint.jpg" alt="Print" name="butPrint" width="92" height="24" class="mouseover" id="butPrint" tabindex="25"/><img border="0" src="../../../../images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="25"/><a href="../../../../main.php"><img src="../../../../images/Tclose.jpg" alt="Close" name="Close" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                </tr>
              </table>
              </td>
            </tr>
          </table>
        </td>
    </tr>
    </table>
</div>
</div>
</form>
</body>
</html>

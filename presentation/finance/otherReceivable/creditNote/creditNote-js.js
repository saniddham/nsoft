var pub_buyingRate 		= 0;
var pub_sellingRate		= 0;
var invStatus = '';
var amStatus = "Auto";

function functionList()
{
	invStatus = "onLoad";
	$('.taxGroup').change();
	$('#cboInvoiceNo').change();
	//---------------------------------------------------
		amStatus = "Auto";
		document.getElementById("chkAutoManual").style.display='none';
		document.getElementById("amStatus").style.display='none';
		$("#frmCreditNote #txtCreditNo").attr("readonly","readonly");
	 //---------------------------------------------------
	if($('#frmCreditNote #cboSearch').val()=='')
	{
	//---------------------------------------------------
		existingMsgDate = "";
		amStatus = "Auto";
		document.getElementById("chkAutoManual").style.display='';
		document.getElementById("amStatus").style.display='';
		$('#frmCreditNote #txtCreditNo').removeClass('validate[required]');
		$("#frmCreditNote #txtCreditNo").attr("readonly","readonly");
	//---------------------------------------------------
	}
}


$(document).ready(function() {
	
	// TODO: ===========Add by dulaskshi 2013.03.20===========
	getSupplierList();
	
	var id = '';
	$("#frmCreditNote").validationEngine();
	$('#frmCreditNote #cboCustomer').focus();

  //permision for add 
  if(intAddx)
  {
 	$('#frmCreditNote #butNew').show();
	$('#frmCreditNote #butSave').show();
	$('#frmCreditNote #butPrint').show();
  }
  //permision for edit 
  if(intEditx)
  {
  	$('#frmCreditNote #butSave').show();
	$('#frmCreditNote #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
	$('#frmCreditNote #butPrint').show();
  }
  //permision for delete
  if(intDeletex)
  {
  	$('#frmCreditNote #butDelete').show();
	$('#frmCreditNote #cboSearch').removeAttr('disabled');
  }
  //permision for view
  if(intViewx)
  {
	$('#frmCreditNote #cboSearch').removeAttr('disabled');
  }
   //===================================================================
 	$('#frmCreditNote #chkAutoManual').click(function(){
	  if($('#frmCreditNote #chkAutoManual').attr('checked'))
	  {
		  amStatus = "Auto";
		  $('#frmCreditNote #amStatus').val('Auto');
		  $('#frmCreditNote #txtCreditNo').val('');
		  $("#frmCreditNote #txtCreditNo").attr("readonly","readonly");
		  $('#frmCreditNote #txtCreditNo').removeClass('validate[required]');
	  }
	  else
	  {
		  amStatus = "Manual";
		  $('#frmCreditNote #amStatus').val('Manual');
		  $('#frmCreditNote #txtCreditNo').val('');
		  $("#frmCreditNote #txtCreditNo").attr("readonly","");
		  $('#frmCreditNote #txtCreditNo').focus();
		  $('#frmCreditNote #txtCreditNo').addClass('validate[required]');
	  }
  });
 //===================================================================
 
  ///////////////////////////////////
  ///////get customer address ///////
  ///////////////////////////////////
  $('#cboCustomer').change(function(){
	  	invStatus = '';
	  	document.getElementById("rwInvType").style.display='none';
	    var url = "creditNote-db-get.php?requestType=getCustomerAddress&customerId="+$(this).val();
		var obj = $.ajax({url:url,dataType:'json',success:function(json){
			$('#txtAddress').val(json.address);	
			$('#cboInvoiceNo').html(json.salesInvoiceList);
		},async:false});
		clearRows();
  });
  
  
  ///////////////////////////////////
  ///////get exchange rate //////////
  ///////////////////////////////////
  $('#cboCurrency').change(function(){
	    var url = "creditNote-db-get.php?requestType=getExchangeRate&currencyId="+$(this).val()+'&exchangeDate='+$('#txtDate').val();
		var obj = $.ajax({url:url,dataType:'json',success:function(json){
			
			$('#rdoBuying').val(json.buyingRate);
			$('#rdoSelling').val(json.sellingRate);
			$('#rdoAverage').val(((eval(json.buyingRate) + eval(json.sellingRate))/2).toFixed(4));
			$('#rdoSelling').click();
			},async:false});
  });
  //-------------------------------------------------------------
  $('#frmCreditNote #chkEdit').click(function(){
	  if(document.getElementById("chkEdit").checked==true)
		$('#frmCreditNote #txtRate').removeAttr('disabled');
		else
		$('#frmCreditNote #txtRate').attr("disabled",true);

  });
  //------------------------------------------------------------
  	$('.rdoRate').click(function(){
	  $('#txtRate').val($(this).val());
	});
	
	$('#cboInvoiceNo').change(function(){
		if(invStatus == "")
		{
			var url = 'creditNote-db-get.php?requestType=getItemList&salesInvoiceNo='+$(this).val();
			var obj = $.ajax({url:url,dataType:'json',success:function(json){
				$('.item').html(json.itemList);
				},async:false});
		}			
///////////////////////////get customer invoice type////////////////////
	if($(this).val())
	{
		document.getElementById("rwInvType").style.display='';
		var url = "creditNote-db-get.php?requestType=getInvoiceType&invoiceId="+$(this).val();
		var httpobj = $.ajax({
		url:url,
		dataType:'json',
		async:false,
		success:function(json)
		{
			$('.invTpe').attr('checked',false); //
			$('#frmCreditNote #'+json.invoType).attr('checked',true); //;
		}
		});
	}
	else
	{
		document.getElementById("rwInvType").style.display='none';
	}
////////////////////////////////////////////////////////////////////
	});
	
	$('#butInsertRow').click(function(){
		var count = $('#tblMain >tbody >tr').length;
		var tr    = $('#tblMain >tbody >tr:eq(1)').html();
		$('#tblMain').append('<tr>'+tr+'</tr>');
		
		document.getElementById('tblMain').rows[count].cells[1].childNodes[0].value='';
		document.getElementById('tblSubGrid').rows[0].cells[1].childNodes[0].value='';
		document.getElementById('tblSubGrid').rows[1].cells[1].childNodes[0].value='';
		document.getElementById('tblSubGrid').rows[2].cells[1].childNodes[0].value='';
		document.getElementById('tblSubGrid').rows[3].cells[1].childNodes[0].value='';
		document.getElementById('tblSubGrid').rows[4].cells[1].childNodes[0].value='';
		document.getElementById('tblSubGrid').rows[5].cells[1].childNodes[0].value='';
		document.getElementById('tblMain').rows[count].cells[3].childNodes[0].value='';
		document.getElementById('tblMain').rows[count].cells[4].childNodes[0].value='';
		document.getElementById('tblMain').rows[count].cells[5].childNodes[0].value='';
		document.getElementById('tblMain').rows[count].cells[6].childNodes[0].value='';
		document.getElementById('tblMain').rows[count].cells[7].childNodes[0].value='';
		document.getElementById('tblMain').rows[count].cells[8].childNodes[0].value='';
		document.getElementById('tblMain').rows[count].cells[9].childNodes[0].value='';
		document.getElementById('tblMain').rows[count].cells[10].childNodes[0].value='';
		document.getElementById('tblMain').rows[count].cells[10].childNodes[0].id='';
		deleteRow();	
	//-------------------------------
	$('.delImg').click(function(){
		var rowCount = document.getElementById('tblMain').rows.length;
		if(rowCount>2)
		$(this).parent().parent().remove();
		calTax(this);
	});
	//--------------------------------------- 
	$('.taxGroup').live('change',function(){
		calTax(this);
	});
	//--------------------------------------- 
	$('.ammount').keyup(function(){
		calTax(this);
	});
	//-------------------------------------- 
	});
	
	deleteRow();
	
	//--------------------------------------
	$('#butSave').unbind('click');
	$('#butSave').click(saveDetails);
	//--------------------------------------
	$('#butDelete').unbind('click');
	$('#butDelete').click(deleteDetails);
	//--------------------------------------
	$('#butNew').unbind('click');
	$('#butNew').click(clearForm);
	//--------------------------------------- 
	$('.taxGroup').live('change',function(){
		calTax(this);
	});
	//-------------------------------------- 
	$('.delImg').click(function(){
		var rowCount = document.getElementById('tblMain').rows.length;
		if(rowCount>2)
		$(this).parent().parent().remove();
		calTax(this);
	});
	//--------------------------------------- 
	$('.ammount').keyup(function(){
		calTax(this);
	});
	//-------------------------------------- 
	
	$('#frmCreditNote #butPrint').click(function(){
		if($('#frmCreditNote #txtCreditNo').val()=='')
		{
			$('#frmCreditNote #butPrint').validationEngine('showPrompt', 'Please select Credit Note.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var myurl = 'listing/creditNoteDetails.php?id='+URLEncode($('#frmCreditNote #txtCreditNo').val());
    		window.open(myurl); 
		}
	});
	
});
//-----------------------------------------------------------------
function deleteRow()
{
	$('.delRow').unbind('click');
	$('.delRow').click(function(){
		var count = $('#tblMain >tbody >tr').length;
		if(count>2)
			$(this).parent().parent().remove();	
	});	
}
//--------------------------------------------------------------
function clearRows()
{
	var rowCount = document.getElementById('tblMain').rows.length;

	for(var i=2;i<rowCount;i++)
	{
			document.getElementById('tblMain').deleteRow(1);
	}
	document.getElementById('tblMain').rows[1].cells[1].childNodes[0].value='';
	
	document.getElementById('tblSubGrid').rows[0].cells[1].childNodes[0].value='';
	document.getElementById('tblSubGrid').rows[1].cells[1].childNodes[0].value='';
	document.getElementById('tblSubGrid').rows[2].cells[1].childNodes[0].value='';
	document.getElementById('tblSubGrid').rows[3].cells[1].childNodes[0].value='';
	document.getElementById('tblSubGrid').rows[4].cells[1].childNodes[0].value='';
	document.getElementById('tblSubGrid').rows[5].cells[1].childNodes[0].value='';
	
	document.getElementById('tblMain').rows[1].cells[3].childNodes[0].value='';
	document.getElementById('tblMain').rows[1].cells[4].childNodes[0].value='';
	document.getElementById('tblMain').rows[1].cells[5].childNodes[0].value='';
	document.getElementById('tblMain').rows[1].cells[6].childNodes[0].value='';
	document.getElementById('tblMain').rows[1].cells[7].childNodes[0].value='';
	document.getElementById('tblMain').rows[1].cells[8].childNodes[0].value='';
	document.getElementById('tblMain').rows[1].cells[9].childNodes[0].value='';
	document.getElementById('tblMain').rows[1].cells[10].childNodes[0].value='';
	document.getElementById('tblMain').rows[1].cells[10].childNodes[0].id='';
}
//-----------------------------------------------------------------

function saveDetailsssssssssssss()
{
	if ($('#frmCreditNote').validationEngine('validate'))   
	{
	var gridData = "[";
	$('#tblMain >tbody >tr:not(:eq(0))').each(function(){
			var item 		= $(this).find('#cboItem').val();
			var desc 		= $(this).find('#cboItem option:selected').text();
			var uom  		= $(this).find('#cboUOM').val();
			var qty  		= $(this).find('#txtQty').val();
			var rate 		= $(this).find('#txtRate').val();
			var discount 	= $(this).find('#txtDiscount').val();
			var amount 		= $(this).find('#txtAmount').val();
			var tax 		= $(this).find('#cboTax').val();
			var dimension 	= $(this).find('#cboDimension').val();
			gridData += '{"item":"'+item+'","uom":"'+uom+'","qty":"'+qty+'","rate":"'+rate+'","discount":"'+discount+'","amount":"'+amount+'","tax":"'+tax+'","dimension":"'+dimension+'"},';
	});
	gridData = gridData.substr(0,gridData.length-1)+']';
	
	var data = 'header=[{'+
		'"customerId"	:"'+$('#cboCustomer').val()+'",'+
		'"dtDate"		:"'+$('#txtDate').val()+'",'+
		'"remarks"		:"'+$('#txtRemarks').val()+'",'+
		'"currencyId"	:"'+$('#cboCurrency').val()+'",'+
		'"rate"			:"'+$('#txtRate').val()+'",'+
		'"invoiceNo"	:"'+$('#cboInvoiceNo').val()+'",'+
		'"poNo"			:"'+$('#txtPONo').val()+'",'+
		'"repId"		:"'+$('#cboMarketer').val()+'",'+
		'"details"		:'+gridData +
		'}]';
	var url = "creditNote-db-set.php?requestType=saveDetails";
	$.ajax({url:url,
	dataType:"JSON",
	data:data,
	async:false,
	type:"GET"
	});	
	}
}
//-----------------------------------------------------------------------
function saveDetails()
{
if(existingMsgDate == "")
{
	if ($('#frmCreditNote').validationEngine('validate'))   
    { 
/*		  if(checkForDuplicate()==false){
			return false;  
		  }
*/		var data = "requestType=saveDetails";
	
			data+="&serialNo="		+	URLEncode($('#cboSearch').val());
			data+="&customerId="		+	$('#cboCustomer').val();
			data+="&dtDate="	+	$('#txtDate').val();
			data+="&remarks="	+	URLEncode($('#txtRemarks').val());
			data+="&currencyId="			+	$('#cboCurrency').val();
			data+="&rate="		+	$('#txtRate').val();
			data+="&invoiceNo="		+	URLEncode($('#cboInvoiceNo').val());
			data+="&poNo="		+	URLEncode($('#txtPONo').val());
			data+="&repId="		+	$('#cboMarketer').val();
			data+="&amStatus="		+	amStatus;
			data+="&manualNo="		+	URLEncode($('#txtCreditNo').val());
			

			var rowCount = document.getElementById('tblMain').rows.length;
			if(rowCount==1){
				alert("No Items selected");
				return false;				
			}
			var row = 0;
			
			var arr="[";
			
	$('#tblMain >tbody >tr:not(:eq(0))').each(function(){
			arr += "{";
			arr += '"item":"'+		URLEncode($(this).find('#cboItem').val()) +'",' ;
			
			arr += '"style":"'+		$(this).find('#tblSubGrid .styleNo').val() +'",' ;
			arr += '"graphic":"'+		$(this).find('#tblSubGrid .graphicNo').val() +'",' ;
			arr += '"order":"'+		$(this).find('#tblSubGrid .orderNo').val() +'",' ;
			arr += '"line":"'+		$(this).find('#tblSubGrid .lineItem').val() +'",' ;
			arr += '"locate":"'+		$(this).find('#tblSubGrid .cusLocate').val() +'",' ;
			arr += '"itemDesc":"'+		URLEncode($(this).find('#tblSubGrid .itemDesc').val()) +'",' ;
			
			arr += '"desc":"'+		$(this).find('#cboItem option:selected').text() +'",' ;
			
			arr += '"uom":"'+		$(this).find('#cboUOM').val() +'",' ;
			arr += '"qty":"'+		$(this).find('#txtQty').val() +'",' ;
			arr += '"rate":"'+		$(this).find('#txtRate').val() +'",' ;
			arr += '"discount":"'+		$(this).find('#txtDiscount').val() +'",' ;
			arr += '"amount":"'+		$(this).find('#txtAmount').val() +'",' ;
			arr += '"tax":"'+		$(this).find('#cboTax').val() +'",' ;
			arr += '"dimension":"'+		$(this).find('#cboDimension').val() +'",' ;
			arr += '"trnTaxVal":'+		$(this).find(".taxWith").attr('id') +"," ;
			arr += '"taxVal":"'+		$(this).find(".taxWith").val() +'"' ;
			arr +=  '},';
	});
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			data+="&arr="	+	arr;
		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = "creditNote-db-set.php";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			success:function(json){

					$('#frmCreditNote #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						//$('#frmSampleInfomations').get(0).reset();
						//saveImage(json.sampleNo,json.sampleYear,json.revisionNo);
						//loadCombo_frmSampleInfomations();
						loadCombo_search();
						var t=setTimeout("alertx()",1000);
						$('#txtCreditNo').val(json.serialNo);
						$('#cboSearch').val(json.serialNo);
					//	$('#txtRetSupYear').val(json.year);
						return;
					}
				},
			error:function(xhr,status){
					
					$('#frmCreditNote #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
}
else
{
	$('#frmCreditNote #butSave').validationEngine('showPrompt', existingMsgDate,'fail');
	var t=setTimeout("alertx()",5000);
}
}

//-----------------------------------------------------------------------
function deleteDetails()
{			

  //  $('#frmCreditNote #butDelete').click(function(){
		
		var serial=URLEncode($('#frmCreditNote #txtCreditNo').val());
		if($('#frmCreditNote #txtCreditNo').val()=='')
		{
			//$('#frmBankDeposit #butDelete').validationEngine('showPrompt', 'Please select Deposit No.', 'fail');
		//	var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+serial+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
                                                                                var customerId=$('#frmCreditNote #cboCustomer').val()
										var url = "creditNote-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											data:'requestType=delete&serialNo='+serial+"&customerId="+customerId,
											async:false,
											success:function(json){
												
												$('#frmCreditNote #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												if(json.type=='pass')
												{
													$('#frmCreditNote').get(0).reset();
													clearForm();
													loadCombo_search();
													var t=setTimeout("alertDelete()",1000);return;
												}
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	//});
	//clearForm();
}
//------------------------------------------------------------------------

function calTax(obje){
			var tbl = document.getElementById('tblMain');
	//---------------------------------------------- 
	//var qty=$(obje).parent().parent().find('td').eq(3).children().val();
	//var unitPrice=$(obje).parent().parent().find('td').eq(4).children().val();
	//var discount=$(obje).parent().parent().find('td').eq(5).children().val();
	var qty=$(obje).parent().parent().find('.qty').val();
	var unitPrice=$(obje).parent().parent().find('.rate').val();
	var discount=$(obje).parent().parent().find('.discount').val();
	var am=qty*unitPrice*(100-discount)/100;
	//alert(am);
	
	//$(obje).parent().parent().find('td').eq(6).children().val(am);
	var amn=am.toFixed(4)
	$(obje).parent().parent().find('.amm').val(amn);
	//--------------------------------------------- 
	//var amount = $(obje).parent().parent().find('td').eq(6).children().val();
	//var taxId = callTaxProcess($(obje).parent().parent().find('td').eq(7).children().val());
	var amount = $(obje).parent().parent().find('.amm').val();
	var taxId = callTaxProcess( $(obje).parent().parent().find('.taxGroup').val());
	var arrTax = taxId.split('/');
	var operation = '';
	
	var jsonTaxCode="[ ";

	if(arrTax.length == 1)
	{
		operation = 'Isolated';
		jsonTaxCode += '{ "taxId":"'+taxId+'"},';
	}
	else if(arrTax.length > 1)
	{
		operation = arrTax[1];
		for(var i =0; i < arrTax.length; i=i+2)
		{
			jsonTaxCode += '{ "taxId":"'+arrTax[i]+'"},';
		}
	}
	jsonTaxCode = jsonTaxCode.substr(0,jsonTaxCode.length-1);
	jsonTaxCode += " ]";
	var url = "creditNote-db-get.php?requestType=getTaxValue&arrTaxCode="+jsonTaxCode+"&opType="+operation+"&valAmount="+amount;
	var obj = $.ajax({url:url,async:false});
	var values = obj.responseText.split('/');
	//$(obje).parent().parent().find('td').eq(9).children().val(values[0]);
	$(obje).parent().parent().find('.taxWith').val(values[0]);
	if(arrTax.length == 1)
	{
		trnTax="[ ";
		trnTax += '{ "taxId":"'+taxId+'", "taxValue": "'+values[1]+'"},';
		trnTax = trnTax.substr(0,trnTax.length-1);
		trnTax += " ]";
	}
	else if(arrTax.length > 1)
	{
		trnTax="[ ";
		var k = 1;
		for(var i =0; i < arrTax.length; i=i+2)
		{
			trnTax += '{ "taxId":"'+arrTax[i]+'", "taxValue": "'+values[k]+'"},';
			k++;
		}
		trnTax = trnTax.substr(0,trnTax.length-1);
		trnTax += " ]";
	}
	//$(obje).parent().parent().find('td').eq(9).children().attr('id',trnTax);
	$(obje).parent().parent().find('.taxWith').attr('id',trnTax);
	
	callTaxTotal();
 }
//-----------------------------------------------------------------------------
function callTaxProcess(taxGroupId)
{
	var url = "creditNote-db-get.php?requestType=getTaxProcess&taxGroupId="+taxGroupId;
	var obj = $.ajax({url:url,async:false});
	return obj.responseText;
}
//---------------------------------------------------------------------------------
function callTaxTotal()
{
	var taxTotal = 0.00;
	$(".taxWith").each( function(){
          taxTotal += eval($(this).val()==''?0.00:$(this).val());
	});
	//alert(taxTotal);
	$('#txtTotalTax').val(taxTotal.toFixed(2));
	callTotalAmount();
}
//---------------------------------------------------------------------------------
function callTotalAmount()
{
	var subTot=callTotat().toFixed(2);
	var taxTotal=parseFloat($('#txtTotalTax').val()).toFixed(2);
	var tot=parseFloat(taxTotal)+parseFloat(subTot);
	tot=tot.toFixed(2);
 	$('#txtTotal').val(tot);
	if(isNaN(tot)==true){
		$('#txtTotal').val(0);
	}
/*	var x = '10*3';
	alert(eval(x));
*/}
//---------------------------------------------------------------------------------
function callTotat()
{
	var subTotal = 0;
	var rowCount = document.getElementById('tblMain').rows.length;
	var row = 0;
	
	for(var i=1;i<rowCount;i++)
	{
		if(document.getElementById('tblMain').rows[i].cells[7].childNodes[0].value==''){
		var qty=0;
		}
		else{
		var qty= 	document.getElementById('tblMain').rows[i].cells[7].childNodes[0].value;
		}
		if(isNaN(qty)==true){
			qty=0; 
		}
		subTotal+=parseFloat(qty);
	}
//	alert(subTotal);
	$('#txtSubTotal').val(subTotal);
  return subTotal;
}
//-----------------------------------------------------------------------------
function loadCombo_search()
{
	var url 	= "creditNote-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmCreditNote #cboSearch').html(httpobj.responseText);
}
//------------------------------------------------------------------------------
function clearForm(){
	//---------------------------------------------------
		existingMsgDate = "";
		amStatus = "Auto";
		$("#frmCreditNote #txtCreditNo").attr("readonly","readonly");
		$('#frmCreditNote #chkAutoManual').attr('checked')
		$("#frmCreditNote #chkAutoManual").attr("disabled","");
		document.getElementById("chkAutoManual").style.display='';
		document.getElementById("amStatus").style.display='';
		$('#frmCreditNote #txtCreditNo').removeClass('validate[required]');
	//---------------------------------------------------
		$('#frmCreditNote').get(0).reset();
		clearRows();
		$('#frmCreditNote #txtCreditNo').val('');
		$('#frmCreditNote #cboSearch').val('');
		$('#frmCreditNote #cboCustomer').val('');
		$('#frmCreditNote #txtAddress').val('');
		$('#frmCreditNote #cboCurrency').val('');
		$('#frmCreditNote #txtRemarks').val('');
		$('#frmCreditNote #txtDate').val('');
		$('#frmCreditNote #txtRate').val('');
		$('#frmCreditNote #cboInvoiceNo').val('');
		$('#frmCreditNote #txtPONo').val('');
		$('#frmCreditNote #cboMarketer').val('');
		$('#frmCreditNote #txtSubTotal').val('0.00');
		$('#frmCreditNote #txtTotalTax').val('0.00');
		$('#frmCreditNote #txtTotal').val('0.00');
		$('#frmCreditNote #cboCustomer').focus();
		
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmCreditNote #txtDate').val(d);
}
//--------------------------------------------------------------------
function alertx()
{
	$('#frmCreditNote #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmCreditNote #butDelete').validationEngine('hide')	;
	$('#frmCreditNote #butPrint').validationEngine('hide')	;
}

//=========Add by dulaskshi 2013.03.20===========
//====================Get Supplier List==============================
function getSupplierList()
{	
	$ledgerAccId = $('#frmCreditNote #cboLedgerAcc').val();
	$searchId = $('#frmCreditNote #cboSearch').val();
	
	var url = "creditNote-db-get.php?requestType=loadCustomer&ledgerAcc="+$ledgerAccId;
	var httpobj = $.ajax({url:url,async:false})
	if($searchId == '')
		{
			$('#frmCreditNote #cboCustomer').html(httpobj.responseText);
		}	
}
//----------------------------------------------------------------------
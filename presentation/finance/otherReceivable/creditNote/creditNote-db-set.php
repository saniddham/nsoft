<?php 
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$locationId 	= $_SESSION['CompanyID'];
	
	$companyId = $_SESSION['headCompanyId'];
	include "{$backwardseperator}dataAccess/Connector.php";	
        include "../../commanFunctions/CommanEditAndDelete.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	
	$requestType 	= $_REQUEST['requestType'];
	$arrHeader 		=  json_decode($_REQUEST['header'],true);
	$arr 	=  $arrHeader['details'];
	
	$serialNo = $_REQUEST['serialNo'];
	$customerId = $_REQUEST['customerId'];
	$dtDate = $_REQUEST['dtDate'];
	$remarks = $_REQUEST['remarks'];
	$currencyId = $_REQUEST['currencyId'];
	$rate = $_REQUEST['rate'];
	$exchRate = $_REQUEST['rate'];
	$invoiceNo = $_REQUEST['invoiceNo'];
	$poNo = $_REQUEST['poNo'];
	$repId = $_REQUEST['repId'];
	$amStatus 	= $_REQUEST['amStatus'];
	$manualNo 	= $_REQUEST['manualNo'];
	if($repId==''){
		$repId=0;
	}
	
	$arr = json_decode($_REQUEST['arr'], true);
	
//------------save---------------------------	
	if($requestType=='saveDetails')
	{
            // ckeck Unrealize gain/loss for entry and if exist block edit and delete
            $chk=checkEditDeleteForUnrealize("ocus",$customerId,'O.CNote',$serialNo);            
            if($chk){
                $response['type'] = 'fail';
                $response['msg']  = "You cannot allow this process! Credit Note has some Unrealize Gain Or Loss";
                //echo json_encode($response);
            }
            else{
                $accountPeriod = getLatestAccPeriod($companyId);
                $db->OpenConnection();
                $db->RunQuery2('Begin');

                if($serialNo==''){
					if($amStatus == "Auto")
					{
						 $serialNo 	= getNextCreditNoteNo($companyId);
					}
					else if($amStatus == "Manual")
					{
						$serialNo	= $manualNo;
					}
                       
                        $companyCode 	= getCompanyCode();
                        $year = date('Y');
                        $editMode=0;
                }
                else{
                        $editMode=1;
                }

                //-----------delete and insert to header table-----------------------
                if($editMode==1){
                    $sql = "UPDATE `fin_other_receivable_creditnote_header` SET strInvoiceNo ='$invoiceNo', 
                                intCustomer ='$customerId', 
                                dtDate ='$dtDate', 
                                dblRate ='$rate', 
                                intCurrency ='$currencyId', 
                                strRemarks ='$remarks', 
                                strPoNo ='$poNo', 
                                intMarketingUser ='$repId', 
                                intModifyer ='$userId', 
                                intStatus ='1',
                                dtmModifyDate =now() 
                            WHERE (`strCreditNoteNo`='$serialNo')";
                    $result = $db->RunQuery2($sql);
                    //========update the transaction deader====================
                    $sql="SELECT fin_other_receivable_creditnote_header.entryId FROM fin_other_receivable_creditnote_header WHERE (`strCreditNoteNo`='$serialNo')";
                    $result = $db->RunQuery2($sql);
                    $row = mysqli_fetch_array($result);
                    $entryId=$row['entryId'];


                        $sql="UPDATE fin_transactions SET 
                                    entryDate='$dtDate',                                                    
                                    currencyId=$currencyId,
                                    currencyRate='$rate',
                                    transDetails='$remarks',                    
                                    accPeriod=$accountPeriod
                            WHERE entryId=$entryId";
                        $db->RunQuery2($sql);

                        $sqld = "DELETE FROM `fin_transactions_details` WHERE entryId=$entryId";
                        $resultd = $db->RunQuery2($sqld);
                        //=========================================================
                }
                else{
                    $sql = "DELETE FROM `fin_other_receivable_creditnote_header` WHERE (`strCreditNoteNo`='$serialNo')";
                    $result1 = $db->RunQuery2($sql);                

                    //Add data to transaction header*******************************************
                    $sql="INSERT INTO fin_transactions (entryDate, strProgramType, documentNo, currencyId, currencyRate, transDetails, payMethodId, paymentNumber, accPeriod, userId, companyId, createdOn) VALUES
                        ('$dtDate','Other Credit Note','$serialNo',$currencyId,$rate,'$remarks',null,null,$accountPeriod,$userId,$companyId,now())";

                    $db->RunQuery2($sql);
                    $entryId=$db->insertId;  

                    $sql = "INSERT INTO `fin_other_receivable_creditnote_header` (`strCreditNoteNo`,`strInvoiceNo`, `intCustomer`,`dtDate`,`dblRate`,
                            `intCurrency`,`strRemarks`,`strPoNo`,`intMarketingUser`,`intCreator`,
                            `dtmCreateDate`,`intCompanyId`,entryId)
                            VALUES ('$serialNo','$invoiceNo','$customerId','$dtDate','$rate',
                            '$currencyId','$remarks','$poNo','$repId','$userId', now(),'$companyId',$entryId)";
                    $result = $db->RunQuery2($sql);
                    //********************************************************************************
                }
                //-----------delete and insert to detail table-----------------------
                if($result){
                    $sql = "DELETE FROM `fin_other_receivable_creditnote_details` WHERE (`strCreditNoteNo`='$serialNo')";
                    $result2 = $db->RunQuery2($sql);		

                    $toSave=0;
                    $saved=0;
                    $rollBackFlag=0;
                    $totAmnt=0;
                    $msg='';
                    foreach($arr as $arrVal)
                    {
                        $item 		 = $arrVal['item'];

                        $style			= trim(($arrVal['style']==''?'NULL':"'".$arrVal['style']."'"));
                        $graphic		= trim(($arrVal['graphic']==''?'NULL':"'".$arrVal['graphic']."'"));
                        $order			= trim(($arrVal['order']==''?'NULL':"'".$arrVal['order']."'"));
                        $line			= trim(($arrVal['line']==''?'NULL':"'".$arrVal['line']."'"));
                        $locate			= trim(($arrVal['locate']==''?'NULL':"'".$arrVal['locate']."'"));
                        $itemDesc		= trim(($arrVal['itemDesc']==''?'NULL':"'".$arrVal['itemDesc']."'"));

                        $desc 		 = $arrVal['desc'];
                        $uom 		 = $arrVal['uom'];
                        $qty 		 = $arrVal['qty'];
                        $rate 		 = $arrVal['rate'];
                        $discount 		 = $arrVal['discount'];
                        $ammount 		 = $arrVal['amount'];
                        $tax 		 = $arrVal['tax'];
                        $taxVal 		 = $arrVal['taxVal'];
                        $taxDetails 		 = $arrVal['trnTaxVal'];
                        $dimension 		 = $arrVal['dimension'];
                        if($discount==''){
                            $discount='0';
                        }
                        if($dimension==''){
                            $dimension='0';
                        }
                        if($tax==''){
                            $tax='0';
                        }

                        $sql = "SELECT
                                    mst_financecustomeritemactivate.intChartOfAccountId
                                    FROM mst_financecustomeritemactivate
                                    WHERE
                                    mst_financecustomeritemactivate.intCustomerItemId =  '$item' AND
                                    mst_financecustomeritemactivate.intCompanyId =  '$companyId'";
                        $result = $db->RunQuery2($sql);
                        $row = mysqli_fetch_array($result);
                        $account=$row['intChartOfAccountId'];
                        if(!$account){
                            $rollBackFlag=1;	
                            $msg.="no account exist for item ".$desc."\n"; 	
                        }

                        if($rollBackFlag!=1){
                            $sql = "INSERT INTO `fin_other_receivable_creditnote_details` (`strCreditNoteNo`,`intAccountId`,`intItem`,`strStyleNo`,`strGraphicNo`,`strOrderNo`,`strLineItem`,`strCusLocation`,`strItemDesc`,`intUOM`,`dblQty`,`dblRate`,`dblDiscount`,`intTaxGroup`,`intDimension`,dblTax) 
                                    VALUES ('$serialNo','$account','$item', $style, $graphic, $order, $line, $locate, $itemDesc, '$uom','$qty','$rate','$discount','$tax','$dimension','$taxVal')";
                            $result3 = $db->RunQuery2($sql);

                            $tot=($ammount+$taxVal);

                            $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                                            ($entryId,'D',$account,$ammount,'$remarks',$dimension)";
                            $trnResult = $db->RunQuery2($sql);


                        //>>>>>>>>>>>>>>>>>>>>>>>>>>>transaction table process - tax>>>>>>>>>>>>>>>>>>>>>>>>>
                            if(count($taxDetails) != 0 && $trnResult){
                                foreach($taxDetails as $taxDetail)
                                {
                                    $taxId 	= $taxDetail['taxId'];
                                    $taxAmount	= $taxDetail['taxValue'];

                                    $sql = "SELECT
                                    mst_financetaxactivate.intChartOfAccountId
                                    FROM mst_financetaxactivate
                                    WHERE
                                    mst_financetaxactivate.intTaxId = '$taxId' AND
                                    mst_financetaxactivate.intCompanyId = '$companyId'";
                                    $result = $db->RunQuery2($sql);
                                    $row = mysqli_fetch_array($result);
                                    $taxAccount = $row['intChartOfAccountId'];

                                    $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                                            ($entryId,'D',$taxAccount,$taxAmount,'$remarks',null)";
                                    $db->RunQuery2($sql);
                                }
                            }
                            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

                            $totAmnt+=$tot;
                            if($result3==1){
                                $saved++;
                            }
                            else{
                                $rollBackFlag=1	;
                            }
                            $toSave++;
                        }
                    }//end of foreach
                    if($rollBackFlag!=1){
                        $sql = "SELECT
                                    mst_finance_service_customer_activate.intChartOfAccountId
                                    FROM mst_finance_service_customer_activate
                                    WHERE
                                    mst_finance_service_customer_activate.intCustomerId =  '$customerId' AND
                                    mst_finance_service_customer_activate.intCompanyId =  '$companyId'";
                        $result = $db->RunQuery2($sql);
                        $row=mysqli_fetch_array($result);
                        $custAccount = $row['intChartOfAccountId'];
                        $totAmnt=$totAmnt;      

                        $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId,personType, personId) VALUES 
                            ($entryId,'C',$custAccount,$totAmnt,'$remarks',null,'ocus',$customerId)";
                        $result4 = $db->RunQuery2($sql);
                        if(!$result4){
                            $rollBackFlag=1;
                        }
                    }
                }	
                if($rollBackFlag==1){
                    if($msg==''){
                            $msg=$db->errormsg;
                    }
                    $db->RunQuery2('Rollback');
                    $response['type'] 		= 'fail';
                    $response['msg'] 		= $msg;
                    $response['q'] 			= $sql;
                }
                else if(($result) && ($toSave==$saved)){
                    $db->RunQuery2('Commit');
                    $response['type'] 		= 'pass';
                    if($editMode==1)
                    $response['msg'] 		= 'Updated successfully.';
                    else
                    $response['msg'] 		= 'Saved successfully.';

                    $response['serialNo'] 		= $serialNo;
                    $response['year'] 		= $year;
                }
                else{
                    $db->RunQuery2('Rollback');
                    $response['type'] 		= 'fail';
                    $response['msg'] 		= $db->errormsg;
                    $response['q'] 			= $sql;
                }		
                $db->CloseConnection();
            }//main Else
	}
        
	else if($requestType=='delete')
	{
            // ckeck Unrealize gain/loss for entry and if exist block edit and delete
            $chk=checkEditDeleteForUnrealize("ocus",$customerId,'O.CNote',$serialNo);            
            if($chk){
                $response['type'] = 'fail';
                $response['msg']  = "You cannot allow this process! Credit Note has some Unrealize Gain Or Loss";
                //echo json_encode($response);
            }
            else{
                try{
                    $db->begin();
                                    $sql = "SELECT
                                                    fin_other_receivable_payments_main_details.strJobNo,
                                                    fin_other_receivable_payments_main_details.intCompanyId,
                                                    fin_other_receivable_payments_main_details.strDocType
                                                    FROM
                                                    fin_other_receivable_payments_main_details
                                                    Inner Join fin_other_receivable_creditnote_header ON fin_other_receivable_creditnote_header.strCreditNoteNo = fin_other_receivable_payments_main_details.strJobNo AND fin_other_receivable_creditnote_header.strInvoiceNo = fin_other_receivable_payments_main_details.strDocNo
                                                    Inner Join fin_other_receivable_payments_header ON fin_other_receivable_payments_main_details.intReceiptNo = fin_other_receivable_payments_header.intReceiptNo AND fin_other_receivable_payments_main_details.intAccPeriodId = fin_other_receivable_payments_header.intAccPeriodId AND fin_other_receivable_payments_main_details.intLocationId = fin_other_receivable_payments_header.intLocationId AND fin_other_receivable_payments_main_details.intCompanyId = fin_other_receivable_payments_header.intCompanyId AND fin_other_receivable_payments_main_details.strReferenceNo = fin_other_receivable_payments_header.strReferenceNo
                                                    WHERE
                                                    fin_other_receivable_payments_main_details.intCompanyId =  '$companyId' AND
                                                    fin_other_receivable_payments_main_details.strJobNo =  '$serialNo' AND
                                                    fin_other_receivable_payments_main_details.strDocType =  'O.CNote' AND
                                                    fin_other_receivable_payments_header.intDeleteStatus =  '0'";
                                    $result = $db->RunQuery2($sql);
                                    if(!mysqli_num_rows($result))
                                    {
                                            $serialNo = $_REQUEST['serialNo'];


                                            $sql = "UPDATE `fin_other_receivable_creditnote_header` SET intStatus ='0', intModifyer ='$userId'  
                                                            WHERE (`strCreditNoteNo`='$serialNo')  ";
                                            $result = $db->RunQuery2($sql);
                                            //==========UPDATE TRANS ACTION delete STATUS
                                            $sql="SELECT fin_other_receivable_creditnote_header.entryId FROM fin_other_receivable_creditnote_header WHERE (`strCreditNoteNo`='$serialNo')";
                                            $result = $db->RunQuery2($sql);
                                            $row = mysqli_fetch_array($result);
                                            $entryId=$row['entryId'];                        
                                            $sqld = "UPDATE `fin_transactions` SET delStatus=1 WHERE entryId=$entryId";
                                            $resultd = $db->RunQuery2($sqld);		

                                            if(($result)){
                                                    $db->commit();
                                                    $response['type'] 	= 'pass';
                                                    $response['msg'] 	= 'Deleted successfully.';
                                            }
                                            else
                                            {
                                                    $response['type'] 	= 'fail';
                                                    $response['msg'] 	= $db->errormsg;
                                                    $response['q'] 	=$sql;
                                                    $db->rollback(); //roalback
                                            }
                                    }
                                    else
                                    {
                                            $db->rollback();
                                            $response['type'] 		= 'fail';
                                            $response['msg'] 		= "You cannot allow this process! Credit Note has some payements";
                                    }
                } 
                            catch (Exception $e) {
                    $db->rollback(); //roalback
                    $response['type'] = 'fail';
                    $response['msg'] = $e->getMessage();
                    $response['q'] = $sql;                
                } 
            }
	}
//----------------------------------------
	echo json_encode($response);
//-----------------------------------------	
function getCompanyCode()
{
	global $companyId;
	global $db;
	$sql = "SELECT
				mst_companies.strCode
			FROM mst_companies
			WHERE
				mst_companies.intId =  '$companyId'
			";	
	$result = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($result);
	return $row['strCode'];
}
//----------------------------------------
function getNextCreditNoteNo($companyId)
{
	global $db;
	global $locationId;
	
	//-----------------
	$sql = "SELECT
			Max(mst_financeaccountingperiod.dtmCreateDate),
			mst_financeaccountingperiod.dtmStartingDate, 
			substring(mst_financeaccountingperiod.dtmStartingDate,1,4) as fromY,
			substring(mst_financeaccountingperiod.dtmClosingDate,1,4) as toY 
			FROM mst_financeaccountingperiod ";	
	$result = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($result);
	$accPeriod = $row['fromY']."-".$row['toY'];
	
	//-----------------
	  $sql = "SELECT
			sys_finance_no.intOtherCreditNote 
			FROM sys_finance_no
			WHERE
			sys_finance_no.intLocationId =  '$locationId' and sys_finance_no.intCompanyId = '$companyId'
			";	
	$result = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($result);
	$nextNo = $row['intOtherCreditNote'];
	
	//------------------
	 $sql = "SELECT
			mst_companies.strCode AS company,
			mst_companies.intId,
			mst_locations.intCompanyId,
			mst_locations.strCode AS location,
			mst_locations.intId
			FROM
			mst_companies
			Inner Join mst_locations ON mst_locations.intCompanyId = mst_companies.intId
			WHERE
			mst_locations.intId =  '$locationId' AND
			mst_companies.intId =  '$companyId'
			";
	$result = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($result);
	$companyCode = $row['company'];
	$locationCode = $row['location'];
	//---------------------
	
	$sql = "UPDATE `sys_finance_no` SET intOtherCreditNote=intOtherCreditNote+1 WHERE (`intLocationId`='$locationId' AND  sys_finance_no.intCompanyId = '$companyId')  ";
	$db->RunQuery2($sql);
	
	return $companyCode."/".$locationCode."/".$accPeriod."/".(int)$nextNo;
	
}
//----------------------------------------
function getCreditPeriod($companyId)
{
	global $db;
		$sql = "SELECT
				MAX(mst_financeaccountingperiod.intId) AS accId,
				mst_financeaccountingperiod.dtmStartingDate,
				mst_financeaccountingperiod.dtmClosingDate,
				mst_financeaccountingperiod.intStatus,
				mst_financeaccountingperiod_companies.intCompanyId,
				mst_financeaccountingperiod_companies.intPeriodId
				FROM
				mst_financeaccountingperiod
				Inner Join mst_financeaccountingperiod_companies ON mst_financeaccountingperiod_companies.intPeriodId = mst_financeaccountingperiod.intId
				WHERE
				mst_financeaccountingperiod_companies.intCompanyId =  '$companyId' AND mst_financeaccountingperiod.intStatus = '1'
				ORDER BY
				mst_financeaccountingperiod.intId DESC
				";	
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$latestAccPeriodId = $row['accId'];	
		return $latestAccPeriodId;
}
//---------------------------------------

function getLatestAccPeriod($companyId)
{
        global $db;
        $sql = "SELECT
                        MAX(mst_financeaccountingperiod.intId) AS accId,
                        mst_financeaccountingperiod.dtmStartingDate,
                        mst_financeaccountingperiod.dtmClosingDate,
                        mst_financeaccountingperiod.intStatus,
                        mst_financeaccountingperiod_companies.intCompanyId,
                        mst_financeaccountingperiod_companies.intPeriodId
                        FROM
                        mst_financeaccountingperiod
                        Inner Join mst_financeaccountingperiod_companies ON mst_financeaccountingperiod_companies.intPeriodId = mst_financeaccountingperiod.intId
                        WHERE
                        mst_financeaccountingperiod_companies.intCompanyId =  '$companyId' AND mst_financeaccountingperiod.intStatus = '1'
                        ORDER BY
                        mst_financeaccountingperiod.intId DESC
                        ";	
        $result = $db->RunQuery($sql);
        $row = mysqli_fetch_array($result);
        $latestAccPeriodId = $row['accId'];	
        return $latestAccPeriodId;
}
//--------------------------------------------------------------------------------------------
?>
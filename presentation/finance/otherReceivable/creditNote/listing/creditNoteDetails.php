<?php
session_start();
$backwardseperator = "../../../../../";
$locationId = $_SESSION['CompanyID'];
$intUser  = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
include  	"{$backwardseperator}dataAccess/Connector.php";

include_once "../../../commanFunctions/Converter.php";

$sql = "SELECT DISTINCT
		mst_locations.intCompanyId,
		mst_locations.strName,
		mst_companies.strVatNo,
		mst_companies.strSVatNo
		FROM
		mst_locations
		Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
		WHERE
		mst_locations.intId =  ".$_SESSION["CompanyID"]."";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$companyId = $row['intCompanyId'];
	$companyVat = $row['strVatNo'];
	$companySvat = $row['strSVatNo']; 
	$companyName = $row['strName']; 
}
$invoiceRefNo = $_REQUEST['id'];

$sql = "SELECT
		fin_other_receivable_creditnote_header.strCreditNoteNo,
		fin_other_receivable_creditnote_header.strInvoiceNo,
		mst_finance_service_customer.strName AS cusName,
		mst_finance_service_customer.strAddress,
		mst_finance_service_customer.strVatNo,
		mst_finance_service_customer.strSVatNo,
		mst_finance_service_customer.strContactPerson,
		mst_financecurrency.strCode AS currency,
		fin_other_receivable_creditnote_header.intCreator,
		user1.intUserId,
		user1.strUserName AS creater,
		user2.strUserName AS modifyer,
		fin_other_receivable_creditnote_header.strPoNo,
		fin_other_receivable_creditnote_header.strRemarks,
		fin_other_receivable_creditnote_header.dtDate,
		fin_other_receivable_creditnote_header.dblRate,
		fin_other_receivable_invoice_header.strInvoiceType,
		fin_other_receivable_invoice_header.dtmDate,
		mst_financepaymentsterms.strName AS paymentsTerms
		FROM
		fin_other_receivable_creditnote_header
		Left Outer Join mst_finance_service_customer ON fin_other_receivable_creditnote_header.intCustomer = mst_finance_service_customer.intId
		Left Outer Join mst_financecurrency ON fin_other_receivable_creditnote_header.intCurrency = mst_financecurrency.intId
		Left Outer Join sys_users AS user1 ON fin_other_receivable_creditnote_header.intCreator = user1.intUserId
		Left Outer Join sys_users AS user2 ON fin_other_receivable_creditnote_header.intModifyer = user2.intUserId
		Left Outer Join fin_other_receivable_invoice_header ON fin_other_receivable_creditnote_header.strInvoiceNo = fin_other_receivable_invoice_header.strReferenceNo
		Inner Join mst_financepaymentsterms ON fin_other_receivable_invoice_header.intPaymentsTermsId = mst_financepaymentsterms.intId
		WHERE
		fin_other_receivable_creditnote_header.strCreditNoteNo =  '$invoiceRefNo'";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$customer 		= $row['cusName'];
	$invoAddress	= $row['strAddress'];
	$creditDate		= $row['dtDate'];
	$invoType 		= $row['strInvoiceType'];
	$currency 		= $row['currency'];
	$poNo 			= $row['strPoNo'];
	$rate			= $row['dblRate'];
	$vatNo			= $row['strVatNo'];
	$svatNo			= $row['strSVatNo'];
	$attention		= $row['strContactPerson'];
	$creater		= $row['creater'];
	$modifyer		= $row['modifyer'];
	$invoNo 		= $row['strInvoiceNo'];
	$invoDate 		= $row['dtmDate'];
	$payTerms		= $row['paymentsTerms'];
}
if($invoType == 'Commercial')
{
	$invoiceType = "Credit Note";
}
else if($invoType == 'Tax')
{
	$invoiceType = "Credit Note";
}
else if($invoType == 'SVAT')
{
	$invoiceType = "SVAT - Credit Note";
}
else if($invoType == 'NFE')
{
	$invoiceType = "NFE Suspended - Credit Note";
}

$sql = "SELECT
		DISTINCT fin_other_receivable_creditnote_details.intDimension,
		mst_financedimension.strName
		FROM
		fin_other_receivable_creditnote_details
		Inner Join mst_financedimension ON fin_other_receivable_creditnote_details.intDimension = mst_financedimension.intId
		WHERE
		fin_other_receivable_creditnote_details.intCompanyId =  '$companyId' AND
		fin_other_receivable_creditnote_details.strCreditNoteNo =  '$invoiceRefNo'
		";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$dimList 	.=  $row['strName']."  ";
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Credit Note Report</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../../css/promt.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/script.js"></script>

<script src="../../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:239px;
	top:172px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
.borderLine
{
	border: solid 1px black;
}
.borderLineIn
{
	border: solid 0.5px black;
	border-color:#999
}
</style>

<style>
    /*@media screen{thead{display:none;}}*/
    @media print{thead{display:table-header-group; margin-bottom:2px;}}
    @page{margin-top:1cm;margin-left:1cm;margin-right:1cm;margin-bottom:1.5cm;}}
</style>

</head>
<body>

<form id="frmSalesInvoiceDetails" name="frmSalesInvoiceDetails" method="post" action="salesInvoiceDetails.php">
<div align="center">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
<thead>
<th colspan="6">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<th colspan="3"></th>
</tr>
<tr>
<td width="10%"></td>
<td width="80%" height="80" valign="top"><strong><br /><?php echo $invoiceType?></strong><br /><?php include '../../../../../reportHeader.php'?></td>
<td width="10%"></td>
</tr>
<tr>
<td colspan="3"></td>
</tr>
</table>
</th>
</tr>
<tr>
  <th>
  <table width="100%">
  <tr>
    <td colspan="6" align="center" bgcolor="#DBDBDB">
  </tr>
  <tr>
    <td width="34%" height="21" align="left">
    <span class="normalfnt"><strong>To:</strong><br /></span><span class="normalfnt"><?php echo $customer ?><br />
    <?php echo $invoAddress ?></span></td>
    <td align="left">&nbsp;</td>
    <td align="left">&nbsp;</td>
    <td colspan="3" class="normalfnt" align="left"><strong>Credit No. :</strong> <?php echo $invoiceRefNo ?></td>
   </tr>
  <tr>
    <td class="normalfnt"><strong>P.O.:</strong> <?php echo $poNo ?></td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="3" align="left"><span class="normalfnt"><strong> Date :</strong> <?php echo $creditDate ?></span></td>
  </tr>
  <tr>
    <td class="normalfnt"><strong>Attention:</strong> <?php echo $attention ?></td>
    <td width="19%" align="left" valign="middle">&nbsp;</td>
    <td width="4%">&nbsp;</td>
    <td colspan="3" align="left"><span class="normalfnt"><strong>Order No. :</strong></span></td>
   </tr>
   <tr>
    <td class="normalfnt"><strong>Customer VAT No:</strong> <?php echo $vatNo ?></td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="3" align="left"><span class="normalfnt"><!--<?php echo $shipDate ?>--><strong>Company VAT No. :</strong> <?php echo $companyVat ?></span></td>
    </tr>
   <tr>
    <td class="normalfnt"><strong>Customer SVAT No:</strong> <?php echo $svatNo ?></td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td colspan="3" align="left"><span class="normalfnt"><strong>Company SVAT No. :</strong> <?php echo $companySvat ?></span></td>
    </tr>
   <tr>
     <td height="21" class="normalfnt"><strong>Invoice No. :</strong> <?php echo $invoNo ?></td>
     <td align="center" valign="middle">&nbsp;</td>
     <td>&nbsp;</td>
     <td width="25%" align="left"><span class="normalfnt" align="left"><strong>Invoice Date :</strong> <?php echo $invoDate ?></span></td>
     <td width="8%" align="left" valign="middle">&nbsp;</td>
     <td width="10%">&nbsp;</td>
   </tr>
  </table>
  </th>
</tr>
</thead>
<tbody>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td colspan="7" class="normalfnt">
        <table width="100%">
        <tr>
        <td width="100%" style="vertical-align:top" >
          <table width="100%" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr>
              <td width="184"  height="22" class="normalfnt borderLine"><strong>Name</strong></td>
              <td width="302" class="normalfnt borderLine"><strong>Description</strong></td>
              <td width="134" class="normalfntMid borderLine"><strong>Unit</strong></td>
              <td width="120" class="normalfntRight borderLine"><strong>Quantity</strong></td>
              <td width="236" class="normalfntRight borderLine"><strong>Unit Price(Rs\Us $)</strong></td>
              <!--<td width="7%" >Discount(%)</td>-->
             <!-- <td width="7%" >Tax Code</td>-->
              <td width="118" class="normalfntRight borderLine"><strong>Total Value</strong></td>
              </tr>
            <?php 
			$totalTax = 0;
	  	  	$sql1 = "SELECT
					fin_other_receivable_creditnote_details.strCreditNoteNo,
					fin_other_receivable_creditnote_header.strInvoiceNo,
					fin_other_receivable_creditnote_details.strItemDesc,
					fin_other_receivable_creditnote_details.intUOM,
					fin_other_receivable_creditnote_details.dblQty,
					fin_other_receivable_creditnote_details.dblRate AS dblUnitPrice,
					fin_other_receivable_creditnote_details.dblDiscount,
					fin_other_receivable_creditnote_details.dblTax AS dblTaxAmount,
					fin_other_receivable_creditnote_details.intTaxGroup,
					fin_other_receivable_creditnote_details.intDimension,
					mst_financetaxgroup.intId,
					mst_financetaxgroup.strCode AS taxCode,
					mst_financetaxgroup.strProcess,
					mst_financedimension.intId,
					mst_financedimension.strName,
					fin_other_receivable_creditnote_header.dblRate,
					mst_financecustomeritem.intId,
					mst_financecustomeritem.strName AS itemName,
					mst_units.intId,
					mst_units.strName AS uom
					FROM
					fin_other_receivable_creditnote_details
					left outer Join fin_other_receivable_creditnote_header ON fin_other_receivable_creditnote_details.strCreditNoteNo = fin_other_receivable_creditnote_header.strCreditNoteNo
					left outer Join mst_financetaxgroup ON fin_other_receivable_creditnote_details.intTaxGroup = mst_financetaxgroup.intId
					left outer Join mst_financedimension ON fin_other_receivable_creditnote_details.intDimension = mst_financedimension.intId
					left outer Join mst_financecustomeritem ON fin_other_receivable_creditnote_details.intItem = mst_financecustomeritem.intId
					left outer Join mst_units ON fin_other_receivable_creditnote_details.intUOM = mst_units.intId
					WHERE
					fin_other_receivable_creditnote_details.strCreditNoteNo =  '$invoiceRefNo'
					";
			$result1 = $db->RunQuery($sql1);

			$totQty=0;
			$totAmmount=0;
		while($row=mysqli_fetch_array($result1))
		{
			$subAmount = (($row['dblUnitPrice'])*((100-$row['dblDiscount'])/100))*$row['dblQty'];
			$totalTax = $totalTax + $row['dblTaxAmount'];
	  ?>
	  <tr class="normalfnt borderLineIn"  bgcolor="#FFFFFF">
   	  <td class="normalfnt borderLineIn" height="50">&nbsp;<?php echo $row['itemName']; ?>&nbsp;</td>
      <td class="normalfnt borderLineIn"><?php echo $row['strItemDesc']; ?>&nbsp;</td>
      <td class="normalfntMid borderLineIn" >&nbsp;<?php echo $row['uom'] ?>&nbsp;</td>
      <td class="normalfntRight borderLineIn" >&nbsp;<?php echo $row['dblQty'] ?>&nbsp;</td>
      <td class="normalfntRight borderLineIn" >&nbsp;<?php echo $row['dblUnitPrice'] ?>&nbsp;</td>
      <!--<td class="normalfntRight" >&nbsp;<?php echo $row['dblDiscount'] ?>&nbsp;</td>-->
      <!--<td class="normalfntMid" >&nbsp;<?php echo $row['taxCode'] ?>&nbsp;</td>-->
      <td class="normalfntRight borderLineIn" >&nbsp;<?php echo number_format($subAmount, 2) ?>&nbsp;</td>
</tr>
      <?php 
			$totQty+=$row['dblQty'];
			$totAmmount+=(($row['dblUnitPrice'])*((100-$row['dblDiscount'])/100))*$row['dblQty'];
			}
	  ?>
            </table>
          </td>
        </tr>
      </table>
      </td>
      </tr>
      </table>
    </td>
</tr>
<!--<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>-->
<tr height="40">
  <td align="center" class="normalfntMid">&nbsp;</td>
</tr>
</tbody>
<tr height="40">
  <td align="center" class="normalfntMid">
  <table width="100%">
  <tr>
    <td width="29%" align="left" class="normalfnt">&nbsp;</td>
    <td width="18%" align="center" class="normalfntMid">&nbsp;</td>
    <td colspan="2" align="right" class="normalfntRight">&nbsp;
      <?php 
	if($invoType != 'SVAT' || $invoType != 'NFE')
	{
		echo "Total Amount Exclude Tax: ".number_format($totAmmount, 2);
	}
	?>&nbsp;</td>
    </tr>
  <tr>
    <td align="left" class="normalfnt">Total Invoice Value</td>
    <td align="center" class="normalfntMid">&nbsp;</td>
    <td colspan="2" align="right" class="normalfntRight">&nbsp;
	<?php
	if($invoType != 'SVAT' || $invoType != 'NFE')
	{ 
		echo "Total Tax Amount: ".number_format($totalTax, 2);
	}
	?>&nbsp;</td>
    </tr>
  <tr>
    <td colspan="3" align="left" class="normalfnt"><strong><?php echo $currency ?>&nbsp;
	<?php
	if($invoType == 'SVAT' || $invoType == 'NFE')
	{
		//echo convert_digit_to_words($totAmmount);
		$val = $totAmmount;
        $inWord= convert_number($val);   
        $sence=explode(".",number_format($val,2));
        $sent=convert_number($sence[1]);
        if(strlen($sence[1])>0)
		{
        	$inWord=$inWord." and ".$sent." cents";
        }
       echo $inWord;
	}
	else
	{
		//echo convert_digit_to_words($totalTax + $totAmmount);
		$val = $totalTax + $totAmmount;
        $inWord= convert_number($val);   
        $sence=explode(".",number_format($val,2));
        $sent=convert_number($sence[1]);
        if(strlen($sence[1])>0)
		{
        	$inWord=$inWord." and ".$sent." cents";
        }
       echo $inWord;
	}
	?> Only</strong></td>
    <td width="18%" align="right" class="normalfntRight">&nbsp;
	<?php 
	if($invoType == 'SVAT' || $invoType == 'NFE')
	{
		echo number_format(($totAmmount), 2);
	}
	else
	{
		echo number_format(($totalTax + $totAmmount), 2);
	}
	?>&nbsp;</td>
  </tr>
  <tr>
    <td align="left" class="normalfnt">&nbsp;</td>
    <td align="center" class="normalfntMid">&nbsp;</td>
    <td width="35%" align="center" class="normalfntMid">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" align="left" class="normalfnt">
    <?php
	if($invoType == 'SVAT' || $invoType == 'NFE')
	{
		echo "<strong>For Vat Purpose Only:</strong><br/>Suspended Value Added Tax @ 12% = ". number_format((($totAmmount * 12)/100), 2)."";
	}
	?>
    </td>
    <td align="right" class="normalfntRight">&nbsp;</td>
  <tr>
    <td colspan="3" align="left" class="normalfnt">
	<?php
	if($invoType == 'SVAT' || $invoType == 'NFE')
	{
		echo "Exchange Rate: <strong>Rs. ".number_format($rate,4).""."/".$symbol." '".number_format($totAmmount*$rate, 2)."'</strong>";
		
		echo "<br />";
		
		$val = $totAmmount*$rate;
        $inWord= convert_number($val);   
        $sence=explode(".",number_format($val,2));
        $sent=convert_number($sence[1]);
        if(strlen($sence[1])>0)
		{
        	$inWord=$inWord." and ".$sent." cents";
        }
       echo $inWord;
	}
	?>
    </td>
    <td align="right" class="normalfntRight">&nbsp;</td>
  	</tr>
	<tr>
	  <td colspan="3" align="left" class="normalfnt">&nbsp;</td>
	  <td align="right" class="normalfntRight">&nbsp;</td>
	  </tr>
	<tr>
    <td colspan="3" align="left" class="normalfnt">Payment Terms: <?php echo $payTerms ." days, Draft\Cheques in favour of ".$companyName?></td>
    <td align="right" class="normalfntRight">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="4" align="left" class="normalfnt" height="30px"><br /><br/>&nbsp;</td>
  </tr>
  <tr>
    <td align="left" class="normalfnt">&nbsp;</td>
    <td align="center" class="normalfntMid">
    <?php
	if($modifyer == NULL)
	{
		echo $creater;
	}
	else
	{
		echo $modifyer;
	}
	?>
    </td>
    <td align="center" class="normalfntMid">&nbsp;</td>
    <td align="right" class="normalfntRight">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" class="normalfnt">.........................................</td>
    <td align="center" class="normalfntMid">........................................</td>
    <td align="center" class="normalfntMid">....................................................</td>
    <td align="right" class="normalfntRight">............................</td>
  </tr>
  <tr>
    <td align="left" class="normalfnt">Customer Acknowledgement</td>
    <td align="center" class="normalfntMid">Prepared By</td>
    <td align="center" class="normalfntMid">Authorized by Accountant</td>
    <td align="right" class="normalfntRight">Authorized By</td>
  </tr>
  </table>
  </td>
</tr>
</table>
</div>        
</form>
</body>
</html>
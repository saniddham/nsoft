<?php 
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	$sql = "SELECT DISTINCT
			mst_locations.intCompanyId,
			mst_companies.intBaseCurrencyId
			FROM
			mst_locations
			Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
			WHERE mst_locations. intId=".$_SESSION["CompanyID"]."";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$companyId 		= $row['intCompanyId'];
		$baseCurrencyId = null($row['intBaseCurrencyId']);
	}
	$locationId = $_SESSION["CompanyID"];
	////////////////////////// main parameters ////////////////////////////////
	$glNo		= trim($_REQUEST['txtNo']);
	$requestType 	= $_REQUEST['requestType'];
	$id 			= $_REQUEST['cboSearch'];
	///////////////////// customer Gain-Loss header parameters ////////////////////
	$customer		= null(trim($_REQUEST['cboCustomer']));
	$date			= trim($_REQUEST['txtDate']);
	$remarks 		= $_REQUEST['txtRemarks'];
	///////////////////// customer Gain-Loss detail parameters /////////////////////
	$mainDetails 	= json_decode($_REQUEST['glDetail'], true);
	$accDetails 	= json_decode($_REQUEST['glAccDetail'], true);
	///////////////////////////////////////////////////////////////////////////

	//////////////////////// customer Gain-Loss insert part ///////////////////////
	if($requestType=='add')
	{
            try{
		$GLNumber 		= getNextGLNo($companyId,$locationId);
		$accountPeriod 	= getLatestAccPeriod($companyId);
		$GLReference	= trim(encodeGLNo($GLNumber,$accountPeriod,$companyId,$locationId));
		
                $db->begin(); 
		if(count($accDetails) != 0)
		{
                    foreach($accDetails as $detail)
                    {
                            $accId 		= $detail['accId'];
                            $accAmount	= $detail['accAmount'];
                            $memo		= $detail['memo'];
                            $dimension 	= val($detail['dimension']);
                    }
                //Add data to transaction header*******************************************
                    $sql="INSERT INTO fin_transactions (entryDate, strProgramType, documentNo, currencyId, currencyRate, transDetails, payMethodId, paymentNumber, accPeriod, userId, companyId, createdOn) VALUES
                    ('$date','Other Receivable Gain-Loss','$GLReference',$baseCurrencyId,1,'$memo',null,null,$accountPeriod,$userId,$companyId,now())";
                
                $db->RunQuery2($sql);
                $entryId=$db->insertId;  
                
			$sql = "INSERT INTO `fin_other_receivable_gain_loss_header`
			(`intGLNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strReferenceNo`,`intCustomerId`,`dtmDate`,`strRemark`,`intChartOfAccountId`,`dblAmount`,`strMemo`,`intDimensionId`,`intCreator`,dtmCreateDate,`intDeleteStatus`,entryId)
			VALUES ('$GLNumber','$accountPeriod','$locationId','$companyId','$GLReference',$customer,'$date','$remarks','$accId','$accAmount','$memo','$dimension','$userId',now(), '0',$entryId)";

			$firstResult = $db->RunQuery2($sql);
		}
		if(count($mainDetails) != 0 && $firstResult)
		{
			foreach($mainDetails as $detail)
			{
				$docNo 		= trim($detail['receiptNo']);
				$recAmount	= trim($detail['receiveAmount']);
				$invAmount	= $detail['invoiceAmount'];
								
				$sql = "INSERT INTO `fin_other_receivable_gain_loss_details` (`intGLNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strReferenceNo`,`strDocNo`,`dblReceiveAmount`,`dblInvoiceAmount`,`intCreator`,dtmCreateDate) 
				VALUES ('$GLNumber','$accountPeriod','$locationId','$companyId','$GLReference','$docNo','$recAmount','$invAmount','$userId',now())";
				
				$mainDetailResult = $db->RunQuery2($sql);
			}
			
		}
		if(($recAmount - $invAmount) > 0)
		{
			$accStatus = 'C';
			$cusStatus = 'D';
		}
		else if(($recAmount - $invAmount) < 0)
		{
			$accStatus = 'D';
			$cusStatus = 'C';
		}
		else if(($payAmount - $invAmount) == 0)
		{
			$gainSatus	= "NON";
		}
		//>>>>>>>>>>>>>>>>>>>>>>transaction table process - customer>>>>>>>>>>>>>>>>>>>>>>>>>
		if(($mainDetailResult && $firstResult) && ($gainSatus != "NON"))	
		{
                    $sql = "SELECT
                                    mst_finance_service_customer_activate.intChartOfAccountId
                                    FROM mst_finance_service_customer_activate
                                    WHERE
                                    mst_finance_service_customer_activate.intCustomerId =  '$customer' AND
                                    mst_finance_service_customer_activate.intCompanyId =  '$companyId'";
                    $result = $db->RunQuery2($sql);
                    $row = mysqli_fetch_array($result);
                    $custAccount = $row['intChartOfAccountId'];
				 
                    $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId, personType, personId) VALUES 
                                    ($entryId,'$cusStatus',$custAccount,$accAmount,'$memo',$dimension,'ocus',$customer)";
                    $trnResult = $db->RunQuery2($sql);
		}
		//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		
		//>>>>>>>>>>>>>>>>>>>>>>transaction table process - account>>>>>>>>>>>>>>>>>>>>>>>>>	
		if(($mainDetailResult && $firstResult && $trnResult) && ($gainSatus != "NON"))	
		{		 
                    $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                                    ($entryId,'$accStatus',$accId,$accAmount,'$memo',$dimension)";
                    $trnAccResult = $db->RunQuery2($sql);
		}
		//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		
		if($mainDetailResult && $firstResult && (($trnResult && $trnAccResult)) || ($gainSatus == "NON"))
		{
                    $db->commit();
                    $response['type'] 		= 'pass';
                    $response['msg'] 		= 'Saved successfully.';
                    $response['GLNo'] 	= $GLReference;
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
                       $db->rollback();//roalback
		}
                }catch(Exception $e){
               
                $db->rollback();//roalback
                $response['type'] 		= 'fail';
                $response['msg'] 		= $e->getMessage();
                $response['q'] 			= $sql;                
               echo json_encode($response);                 
            }
	}
	////////////////////// customer Gain-Loss update part ////////////////////////
	else if($requestType=='edit')
	{
            try {
                $db->begin();
                if(count($accDetails) != 0)
                {   
                    foreach($accDetails as $detail)
                    {
                        $accId 		= $detail['accId'];
                        $accAmount	= $detail['accAmount'];
                        $memo		= $detail['memo'];
                        $dimension 	= val($detail['dimension']);
                    }
                    $sql = "UPDATE `fin_other_receivable_gain_loss_header` SET intCustomerId = $customer,
                            dtmDate				='$date',
                            strRemark			='$remarks',
                            intChartOfAccountId	='$accId',
                            dblAmount			='$accAmount',
                            strMemo				='$memo',
                            intDimensionId		='$dimension',
                            intModifyer			='$userId',
                            intDeleteStatus		='0'
                            WHERE (`strReferenceNo`='$glNo')";
                            $firstResult = $db->RunQuery2($sql);
            }
            if(count($mainDetails) != 0 && $firstResult)
            {
                $sql = "SELECT
                                fin_other_receivable_gain_loss_header.intGLNo,
                                fin_other_receivable_gain_loss_header.intAccPeriodId,
                                fin_other_receivable_gain_loss_header.strReferenceNo,
                                fin_other_receivable_gain_loss_header.entryId
                                FROM
                                fin_other_receivable_gain_loss_header
                                WHERE
                                fin_other_receivable_gain_loss_header.strReferenceNo =  '$glNo'";
                $result = $db->RunQuery2($sql);
                while($row=mysqli_fetch_array($result))
                {
                    $GLNumber 	= $row['intGLNo'];
                    $accountPeriod 	= $row['intAccPeriodId'];
                    $entryId= $row['entryId'];
                }
                //========update the transaction deader====================
                $sql="UPDATE fin_transactions SET 
                            entryDate='$date',                                                
                            currencyId=$baseCurrencyId,
                            currencyRate='1',
                            transDetails='$remarks',                    
                            accPeriod=$accountPeriod
                    WHERE entryId=$entryId";
                    $db->RunQuery2($sql);

                $sqld = "DELETE FROM `fin_transactions_details` WHERE entryId=$entryId";
                $resultd = $db->RunQuery2($sqld);
                //=========================================================
			
                $sql = "DELETE FROM `fin_other_receivable_gain_loss_details` WHERE (`strReferenceNo`='$glNo')";
                $db->RunQuery2($sql);
			
                foreach($mainDetails as $detail)
                {
                    $docNo 		= trim($detail['receiptNo']);
                    $recAmount	= trim($detail['receiveAmount']);
                    $invAmount	= $detail['invoiceAmount'];

                    $sql = "INSERT INTO `fin_other_receivable_gain_loss_details` (`intGLNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strReferenceNo`,`strDocNo`,`dblReceiveAmount`,`dblInvoiceAmount`,`intCreator`,dtmCreateDate) 
                    VALUES ('$GLNumber','$accountPeriod','$locationId','$companyId','$glNo','$docNo','$recAmount','$invAmount','$userId',now())";

                    $mainDetailResult = $db->RunQuery2($sql);
                }
            }
            if(($recAmount - $invAmount) > 0)
            {
                $accStatus = 'C';
                $cusStatus = 'D';
            }
            else if(($recAmount - $invAmount) < 0)
            {
                $accStatus = 'D';
                $cusStatus = 'C';
            }
            else if(($payAmount - $invAmount) == 0)
            {
                $gainSatus	= "NON";
            }
            //>>>>>>>>>>>>>>>>>>>>>>transaction table process - customer>>>>>>>>>>>>>>>>>>>>>>>>>
            if(($mainDetailResult && $firstResult) && ($gainSatus != "NON"))	
            {
                $sql = "SELECT
                                mst_finance_service_customer_activate.intChartOfAccountId
                                FROM mst_finance_service_customer_activate
                                WHERE
                                mst_finance_service_customer_activate.intCustomerId =  '$customer' AND
                                mst_finance_service_customer_activate.intCompanyId =  '$companyId'";
                $result = $db->RunQuery2($sql);
                $row = mysqli_fetch_array($result);
                $custAccount = $row['intChartOfAccountId'];
				 
                $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId, personType, personId) VALUES 
                            ($entryId,'$cusStatus',$custAccount,$accAmount,'$memo',$dimension,'ocus',$customer)";
                $trnResult = $db->RunQuery2($sql);
            }
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		
            //>>>>>>>>>>>>>>>>>>>>>>transaction table process - account>>>>>>>>>>>>>>>>>>>>>>>>>	
            if(($mainDetailResult && $firstResult && $trnResult) && ($gainSatus != "NON"))	
            {		 
                $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                            ($entryId,'$accStatus',$accId,$accAmount,'$memo',$dimension)";
                $trnAccResult = $db->RunQuery2($sql);
            }
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            if($mainDetailResult && $firstResult && (($trnResult && $trnAccResult) || ($gainSatus == "NON")))
            {
                $db->commit();
                $response['type'] 		= 'pass';
                $response['msg'] 		= 'Updated successfully.';
                $response['GLNo'] 	= $glNo;
            }
            else
            {
                $response['type'] 		= 'fail';
                $response['msg'] 		= $db->errormsg;
                $response['q'] 			=$sql;
                $db->rollback(); //roalback
            }
            echo json_encode($response);
        }catch (Exception $e) {
            $db->rollback(); //roalback
            $response['type'] = 'fail';
            $response['msg'] = $e->getMessage();
            $response['q'] = $sql;
            echo json_encode($response);
        }
    }
	/////////// customer Gain-Loss delete part /////////////////////
	else if($requestType=='delete')
	{
            try {
                $db->begin();
		$sql="SELECT fin_other_receivable_gain_loss_header.entryId
                        FROM fin_other_receivable_gain_loss_header WHERE (`strReferenceNo`='$id')";
                $result = $db->RunQuery2($sql);
                $row=mysqli_fetch_array($result);
                $entryId= $row['entryId'];
			
		$sql = "UPDATE `fin_other_receivable_gain_loss_header` SET intDeleteStatus ='1', intModifyer ='$userId'
				WHERE (`strReferenceNo`='$id')";
		$result = $db->RunQuery2($sql);
                
                $sqld = "UPDATE `fin_transactions` SET delStatus=1 WHERE entryId=$entryId";
                $resultd = $db->RunQuery2($sqld);
		if(($result && $resultd)){
                    $db->commit();
                    $response['type'] 		= 'pass';
                    $response['msg'] 		= 'Deleted successfully.';
		}
		else{
                    $response['type'] 		= 'fail';
                    $response['msg'] 		= $db->errormsg;
                    $response['q'] 			=$sql;
                    $db->rollback(); //roalback
		}
                echo json_encode($response);
            }catch (Exception $e) {
                $db->rollback(); //roalback
                $response['type'] = 'fail';
                $response['msg'] = $e->getMessage();
                $response['q'] = $sql;
                echo json_encode($response);
            }
	}
	
//--------------------------------------------------------------------------------------------
	function getNextGLNo($companyId,$locationId)
	{
		global $db;
		$sql = "SELECT
				intCustomerGLNo
				FROM sys_finance_no
				WHERE
				intCompanyId = '$companyId' AND intLocationId = '$locationId'
				";	
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$nextReceiptNo = $row['intCustomerGLNo'];
		
		$sql = "UPDATE `sys_finance_no` SET intCustomerGLNo=intCustomerGLNo+1 WHERE (intCompanyId = '$companyId' AND intLocationId = '$locationId')";
		$db->RunQuery($sql);	
		return $nextReceiptNo;
	}
//--------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------
	function getLatestAccPeriod($companyId)
	{
		global $db;
		$sql = "SELECT
				MAX(mst_financeaccountingperiod.intId) AS accId,
				mst_financeaccountingperiod.dtmStartingDate,
				mst_financeaccountingperiod.dtmClosingDate,
				mst_financeaccountingperiod.intStatus,
				mst_financeaccountingperiod_companies.intCompanyId,
				mst_financeaccountingperiod_companies.intPeriodId
				FROM
				mst_financeaccountingperiod
				Inner Join mst_financeaccountingperiod_companies ON mst_financeaccountingperiod_companies.intPeriodId = mst_financeaccountingperiod.intId
				WHERE
				mst_financeaccountingperiod_companies.intCompanyId =  '$companyId' AND mst_financeaccountingperiod.intStatus = '1'
				ORDER BY
				mst_financeaccountingperiod.intId DESC
				";	
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$latestAccPeriodId = $row['accId'];	
		return $latestAccPeriodId;
	}
//--------------------------------------------------------------------------------------------
//============================================================================================
	function encodeGLNo($glNo,$accountPeriod,$companyId,$locationId)
	{
		global $db;
		$sql = "SELECT
				mst_financeaccountingperiod.intId,
				mst_financeaccountingperiod.dtmStartingDate,
				mst_financeaccountingperiod.dtmClosingDate,
				mst_financeaccountingperiod.intStatus
				FROM
				mst_financeaccountingperiod
				WHERE
				mst_financeaccountingperiod.intId =  '$accountPeriod'
				";	
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$startDate = substr($row['dtmStartingDate'],0,4);
		$closeDate = substr($row['dtmClosingDate'],0,4);
		$sql = "SELECT
				mst_companies.strCode AS company,
				mst_companies.intId,
				mst_locations.intCompanyId,
				mst_locations.strCode AS location,
				mst_locations.intId
				FROM
				mst_companies
				Inner Join mst_locations ON mst_locations.intCompanyId = mst_companies.intId
				WHERE
				mst_locations.intId =  '$locationId' AND
				mst_companies.intId =  '$companyId'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$companyCode = $row['company'];
		$locationCode = $row['location'];
		$glFormat = $companyCode."/".$locationCode."/".$startDate."-".$closeDate."/".$glNo;
		//$glFormat = $companyCode."/".$startDate."-".$closeDate."/".$glNo;
		return $glFormat;
	}
//============================================================================================
?>
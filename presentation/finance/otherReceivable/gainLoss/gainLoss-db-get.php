<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$companyId = $row['intCompanyId']; 
	}
	/////////// customer Gain-Loss load part /////////////////////
	if($requestType=='loadCombo')
	{
		$sql = "SELECT
				fin_other_receivable_gain_loss_header.strReferenceNo,
				fin_other_receivable_gain_loss_header.intGLNo,
				mst_finance_service_customer.strName
				FROM
				fin_other_receivable_gain_loss_header
				Inner Join mst_finance_service_customer ON fin_other_receivable_gain_loss_header.intCustomerId = mst_finance_service_customer.intId
				WHERE
				fin_other_receivable_gain_loss_header.intCompanyId =  '$companyId' AND
				fin_other_receivable_gain_loss_header.intDeleteStatus = '0'
				ORDER BY intGLNo DESC
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['strReferenceNo']."\">".$row['strReferenceNo']." - ".$row['strName']."</option>";
		}
		echo $html;
	}
	else if($requestType=='loadDetails')
	{
		$id  = $_REQUEST['id'];
		
		//-------------------------------------------------------------
		$sql = "SELECT
				fin_other_receivable_payments_header.dtmDate,
				fin_other_receivable_payments_header.intCurrencyId,
				fin_other_receivable_payments_header.dblRate,
				fin_other_receivable_payments_header.dblRecAmount,
				fin_other_receivable_gain_loss_details.strDocNo,
				fin_other_receivable_gain_loss_details.dblReceiveAmount,
				fin_other_receivable_gain_loss_details.dblInvoiceAmount,
				mst_financecurrency.strCode
				FROM
				fin_other_receivable_gain_loss_header
				Inner Join fin_other_receivable_gain_loss_details ON fin_other_receivable_gain_loss_header.intGLNo = fin_other_receivable_gain_loss_details.intGLNo AND fin_other_receivable_gain_loss_header.intAccPeriodId = fin_other_receivable_gain_loss_details.intAccPeriodId AND fin_other_receivable_gain_loss_header.intLocationId = fin_other_receivable_gain_loss_details.intLocationId AND fin_other_receivable_gain_loss_header.intCompanyId = fin_other_receivable_gain_loss_details.intCompanyId AND fin_other_receivable_gain_loss_header.strReferenceNo = fin_other_receivable_gain_loss_details.strReferenceNo
				Inner Join fin_other_receivable_payments_header ON fin_other_receivable_gain_loss_details.strDocNo = fin_other_receivable_payments_header.strReferenceNo AND fin_other_receivable_payments_header.intCompanyId = fin_other_receivable_gain_loss_details.intCompanyId
				Inner Join mst_financecurrency ON fin_other_receivable_payments_header.intCurrencyId = mst_financecurrency.intId
				WHERE
				fin_other_receivable_payments_header.intCompanyId =  '$companyId' AND
				fin_other_receivable_gain_loss_header.strReferenceNo =  '$id'
				";
		$result = $db->RunQuery($sql);
		$arrDetail;
		while($row=mysqli_fetch_array($result))
		{
			$refNo 		= rawurlencode($row['strDocNo']);
			$displayNo 	= $row['strDocNo'];
			$date 		= $row['dtmDate'];
			$amount 	= number_format($row['dblRecAmount'],4,'.','');
			$currency 	= $row['strCode'];
			$rate 		= number_format($row['dblRate'],4,'.','');
			$recAmount 	= number_format($row['dblReceiveAmount'],4,'.','');
			$invAmount 	= number_format($row['dblInvoiceAmount'],4,'.','');
			$glVal 		= $recAmount - $invAmount;
			$gainLoss 	= number_format($glVal,4,'.','');
			
			$val['tBodyDetail'] = "<tr class=normalfnt bgcolor=#CAFEB8>
				<td align=center><input class=checkRow type=checkbox id=$refNo checked=checked /></td>
			<td width=170 align=center class=refNo id=$refNo><a target=_blank href=../receivedPayments/receivedPayments.php?id=$refNo>$displayNo</a></td>
				<td align=center class=docDate>$date</td>
				<td align=right class=amount>$amount</td>
				<td align=center class=docCurrency>$currency</td>
				<td align=right class=docRate>$rate</td>
				<td align=right class=recAmount>$recAmount</td>
				<td align=right class=invAmount>$invAmount</td>
				<td align=right class=gainLoss>$gainLoss</td>
				</tr>"; // textBox --> disabled=disabled
			$arrDetail[] = $val;
		}
		$response['detailVal'] = $arrDetail;
		//-------------------------------------------------------------
		
		//-------------------------------------------------------------
		$sql = "SELECT
				fin_other_receivable_gain_loss_header.strReferenceNo,
				fin_other_receivable_gain_loss_header.intCustomerId,
				fin_other_receivable_gain_loss_header.dtmDate,
				fin_other_receivable_gain_loss_header.strRemark,
				fin_other_receivable_gain_loss_header.intChartOfAccountId,
				fin_other_receivable_gain_loss_header.dblAmount,
				fin_other_receivable_gain_loss_header.strMemo,
				fin_other_receivable_gain_loss_header.intDimensionId,
				fin_other_receivable_gain_loss_header.intDeleteStatus,
				mst_finance_service_customer.strName,
				fin_other_receivable_gain_loss_header.intCompanyId
				FROM
				fin_other_receivable_gain_loss_header
				Inner Join mst_finance_service_customer ON fin_other_receivable_gain_loss_header.intCustomerId = mst_finance_service_customer.intId
				WHERE
				fin_other_receivable_gain_loss_header.intDeleteStatus =  '0' AND
				fin_other_receivable_gain_loss_header.strReferenceNo =  '$id' AND
				fin_other_receivable_gain_loss_header.intCompanyId =  '$companyId'
				";
		$result = $db->RunQuery($sql);
		$arrAccDetail;
		while($row=mysqli_fetch_array($result))
		{
			$response['customer'] 	= $row['intCustomerId'];
			$response['remark'] 	= $row['strRemark'];
			$response['date'] 		= $row['dtmDate'];
			$val1['chartAcc'] 		= $row['intChartOfAccountId'];
			$val1['amount'] 		= $row['dblAmount'];
			$val1['memo'] 			= $row['strMemo'];
			$val1['dimension']		= $row['intDimensionId'];
			$arrAccDetail[] 		= $val1;
		}
		$response['detailAccVal'] = $arrAccDetail;

		echo json_encode($response);
	}
	else if($requestType=='getInvoice')
	{
		$customerId  = $_REQUEST['customerId'];
		
		//////////////////////////Receipt//////////////////////////////////
		$sql = "SELECT
				fin_other_receivable_payments_header.strReferenceNo,
				fin_other_receivable_payments_header.dblRecAmount,
				fin_other_receivable_payments_header.dblRate AS hRate,
				fin_other_receivable_payments_main_details.dblPayAmount,
				fin_other_receivable_payments_main_details.dblRate,
				fin_other_receivable_payments_header.dblRecAmount* fin_other_receivable_payments_header.dblRate AS recAmount,
				Sum(fin_other_receivable_payments_main_details.dblPayAmount * fin_other_receivable_payments_main_details.dblRate) AS invoAmount,
				fin_other_receivable_payments_header.intCustomerId,
				fin_other_receivable_payments_header.intDeleteStatus,
				fin_other_receivable_payments_header.dtmDate,
				mst_financecurrency.strCode
				FROM
				fin_other_receivable_payments_header
				Inner Join fin_other_receivable_payments_main_details ON fin_other_receivable_payments_header.intReceiptNo = fin_other_receivable_payments_main_details.intReceiptNo AND fin_other_receivable_payments_header.intAccPeriodId = fin_other_receivable_payments_main_details.intAccPeriodId AND fin_other_receivable_payments_header.intLocationId = fin_other_receivable_payments_main_details.intLocationId AND fin_other_receivable_payments_header.intCompanyId = fin_other_receivable_payments_main_details.intCompanyId AND fin_other_receivable_payments_header.strReferenceNo = fin_other_receivable_payments_main_details.strReferenceNo
				Inner Join mst_financecurrency ON fin_other_receivable_payments_header.intCurrencyId = mst_financecurrency.intId
				WHERE
				fin_other_receivable_payments_header.intCustomerId =  '$customerId' AND
				fin_other_receivable_payments_header.intCompanyId =  '$companyId' AND
				fin_other_receivable_payments_header.intDeleteStatus =  '0' AND fin_other_receivable_payments_header.strReferenceNo 
				not in (SELECT
						fin_other_receivable_gain_loss_details.strDocNo
						FROM
						fin_other_receivable_gain_loss_details
						Inner Join fin_other_receivable_gain_loss_header ON fin_other_receivable_gain_loss_header.intGLNo = fin_other_receivable_gain_loss_details.intGLNo AND fin_other_receivable_gain_loss_header.intAccPeriodId = fin_other_receivable_gain_loss_details.intAccPeriodId AND fin_other_receivable_gain_loss_header.intLocationId = fin_other_receivable_gain_loss_details.intLocationId AND fin_other_receivable_gain_loss_header.intCompanyId = fin_other_receivable_gain_loss_details.intCompanyId AND fin_other_receivable_gain_loss_header.strReferenceNo = fin_other_receivable_gain_loss_details.strReferenceNo
						WHERE
						fin_other_receivable_gain_loss_details.intCompanyId =  '$companyId' AND
						fin_other_receivable_gain_loss_header.intDeleteStatus =  '0'
						)
				GROUP BY
				fin_other_receivable_payments_main_details.strReferenceNo
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$refNo = rawurlencode($row['strReferenceNo']);
			$displayNo = $row['strReferenceNo'];
			$date = $row['dtmDate'];
			$amount = number_format($row['dblRecAmount'],4,'.','');
			$currency = $row['strCode'];
			$rate = number_format($row['hRate'],4,'.','');
			$recAmount = number_format($row['recAmount'],4,'.','');
			$invAmount = number_format($row['invoAmount'],4,'.','');
			$val = $recAmount - $invAmount;
			$gainLoss = number_format($val,4,'.','');
			echo 
				"
				<tr class=normalfnt bgcolor=#CAFEB8>
				<td align=center><input class=checkRow type=checkbox id=$refNo /></td>
			<td width=170 align=center class=refNo id=$refNo><a target=_blank href=../receivedPayments/receivedPayments.php?id=$refNo>$displayNo</a></td>
				<td align=center class=docDate>$date</td>
				<td align=right class=amount>$amount</td>
				<td align=center class=docCurrency>$currency</td>
				<td align=right class=docRate>$rate</td>
				<td align=right class=recAmount>$recAmount</td>
				<td align=right class=invAmount>$invAmount</td>
				<td align=right class=gainLoss>$gainLoss</td>
				</tr>
				";
		} // readonly=readonly
		//////////////////////////////////////////////////////////////////////
	}
?>
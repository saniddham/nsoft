// JavaScript Document
var toBePaidAmount = '';
function functionList()
{
	if(recRefNo!='')
	{
		$('#frmOtherGainLoss #cboSearch').val(recRefNo);
		$('#frmOtherGainLoss #cboSearch').change();
	}
}
var rows = 1;
function insertRow()
{
	var tbl = document.getElementById('tblMainGrid2');	
	rows = tbl.rows.length;
	tbl.insertRow(rows);
	tbl.rows[rows].innerHTML = tbl.rows[rows-1].innerHTML;
//	loadJs();
}
$(document).ready(function() {
	
	var id = '';
	$("#frmOtherGainLoss").validationEngine();
	$('#frmOtherGainLoss #cboCustomer').focus();

$('.delImg').live('click',function(){
	var rowId = $(this).parent().parent().parent().find('tr').length;
	if(rowId!=2)
	$(this).parent().parent().remove();
	finalAmount();
});

$('.delImg').css('cursor', 'pointer');

  //permision for add 
  if(intAddx)
  {
 	$('#frmOtherGainLoss #butNew').show();
	$('#frmOtherGainLoss #butSave').show();
  }
  //permision for edit 
  if(intEditx)
  {
  	$('#frmOtherGainLoss #butSave').show();
	$('#frmOtherGainLoss #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
  //permision for delete
  if(intDeletex)
  {
  	$('#frmOtherGainLoss #butDelete').show();
	$('#frmOtherGainLoss #cboSearch').removeAttr('disabled');
  }
  //permision for view
  if(intViewx)
  {
	$('#frmOtherGainLoss #cboSearch').removeAttr('disabled');
  }
  
  $('#frmOtherGainLoss #chkEdit').click(function(){
	  if($('#frmOtherGainLoss #chkEdit').attr('checked'))
	  {
		  $("#frmOtherGainLoss #txtRate").attr("readonly","");
		  $('#frmOtherGainLoss #txtRate').focus();
	  }
	  else
	  {
		  $('#frmOtherGainLoss #txtRate').val('');
		  $("#frmOtherGainLoss #txtRate").attr("readonly","readonly");
		  $('#frmOtherGainLoss #cboCurrency').change();
	  }
  });

$(".checkRow").live('click',function(){
if($(this).attr("checked") == true)
{
	$(this).parent().parent().addClass("highlight");
	receivedAmount();
}
else
{
	$(this).parent().parent().removeClass("highlight");
	receivedAmount();
}
});

///////////////////////////get customer invoice////////////////////
$('#cboCustomer').change(function(){
	var url = "gainLoss-db-get.php?requestType=getInvoice&customerId="+$(this).val();
	var obj = $.ajax({url:url,async:false});
	document.getElementById('allInvoice').innerHTML=obj.responseText;
});
////////////////////////////////////////////////////////////////////

//------------------------------------------------------------
   $('#frmOtherGainLoss #cboPaymentsMethods').change(function(){
	var payMethod = $('#cboPaymentsMethods').val();
	if(payMethod==2)
	{
		document.getElementById("rwChequeDetails").style.display='';
	}
	else
	{
		document.getElementById("rwChequeDetails").style.display='none';
	}
  });
//----------------------------------------------------------

///////////////////////////get G/L amount//////////////////////////
$('.glAccount').live('change',function(){
	$('#txtAccAmount').val($('#txtRecAmount').val());
});
//////////////////////////////////////////////////////////////////

////////////////////////get exchange rate//////////////////////////
$('#cboCurrency').change(function(){
	var url = "gainLoss-db-get.php?requestType=getExchangeRate&currencyId="+$(this).val()+'&exchangeDate='+$('#txtDate').val();
	var obj = $.ajax({url:url,dataType:'json',success:function(json){
		
		$('#rdoBuying').val(json.buyingRate);
		$('#rdoSelling').val(json.sellingRate);
		$('#rdoSelling').click();
		},async:false});
});
///////////////////////////////////////////////////////////////////
$('.rdoRate').click(function(){
  $('#txtRate').val($(this).val());
});
//save button click event
$('#frmOtherGainLoss #butSave').click(function(){
if(existingMsgDate == "")
{
//-------------------------------------------------------------------
	var receiptNO = "";
	var receiveAmount = "";
	var invoiceAmount = "";
			
 value="[ ";
	$('#tblMainGrid1 tr:not(:first)').each(function(){
		if ($(this).find('.checkRow').attr('checked')) 
		{
			receiptNo		= $(this).find(".refNo").attr('id');
			receiveAmount 	= $(this).find(".recAmount").html();
			invoiceAmount 	= $(this).find(".invAmount").html();	
			
			value += '{ "receiptNo":"'+receiptNo+'", "receiveAmount": "'+receiveAmount+'", "invoiceAmount": "'+invoiceAmount+'"},';
		}
	});
	
	value = value.substr(0,value.length-1);
	value += " ]";
//---------------------------------------------------------------------------
	var accId = "";
	var accAmount = "";
	var memo = "";
	var dimension = "";
			
 accValue="[ ";
	$('#tblMainGrid2 tr:not(:first)').each(function(){
		
		accId		= $(this).find(".glAccount").val();
		accAmount 	= $(this).find(".accAmount").val();
		memo 		= $(this).find(".memo").val();
		dimension 	= $(this).find(".dimension").val();
		
	accValue += '{ "accId":"'+accId+'", "accAmount": "'+accAmount+'", "memo": "'+memo+'", "dimension": "'+dimension+'"},';
	});
	
	accValue = accValue.substr(0,accValue.length-1);
	accValue += " ]";
//---------------------------------------------------------------------------
	var requestType = '';
	if ($('#frmOtherGainLoss').validationEngine('validate'))
    {
		//showWaiting();
		if(value != '[ ]')
		{
			if((eval($('#txtRecAmount').val()) - eval($('#txtAccAmount').val()))==0)
			{
				$('#frmOtherGainLoss #cboCustomer').attr("disabled","");
				if($('#txtNo').val()=='')
					requestType = 'add';
				else
					requestType = 'edit';
				
				var url = "gainLoss-db-set.php";
				var obj = $.ajax({
					url:url,
					dataType: "json",
					type:'post', 
					data:$("#frmOtherGainLoss").serialize()+'&requestType='+requestType+'&cboSearch='+id+'&glDetail='+value+'&glAccDetail='+accValue,
					async:false,
					
					success:function(json){
							$('#frmOtherGainLoss #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
							if(json.type=='pass')
							{
								//$('#frmOtherGainLoss').get(0).reset(); hideWaitng();
								$('#frmOtherGainLoss #cboCustomer').attr("disabled","disabled");
								var t=setTimeout("alertx()",1000);
								$('#txtNo').val(json.GLNo);
								loadCombo_frmOtherGainLoss();
								return;
							}
							var t=setTimeout("alertx()",3000);
						},
					error:function(xhr,status){
						$('#frmOtherGainLoss #cboCustomer').attr("disabled","disabled");
							
							$('#frmOtherGainLoss #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
							var t=setTimeout("alertx()",3000);
						}		
					});
			}
			else
			{
				$('#frmOtherGainLoss #butSave').validationEngine('showPrompt', 'You cannot allow this process untill received  amount and total amount are same','fail');
				var t=setTimeout("alertx()",5000);
			}
		}
		else
		{
			$('#frmOtherGainLoss #butSave').validationEngine('showPrompt', 'You cannot allow this process!','fail');
			var t=setTimeout("alertx()",5000);
		}
	}
}
else
{
	$('#frmOtherGainLoss #butSave').validationEngine('showPrompt', existingMsgDate,'fail');
	var t=setTimeout("alertx()",5000);
}
});
/////////////////////////////////////////////////////
//// load invoice details //////////////////////////
/////////////////////////////////////////////////////
$('#frmOtherGainLoss #cboSearch').click(function(){
   $('#frmOtherGainLoss').validationEngine('hide');
});
$('#frmOtherGainLoss #cboSearch').change(function(){
$('#frmOtherGainLoss').validationEngine('hide');
var url = "gainLoss-db-get.php";
if($('#frmOtherGainLoss #cboSearch').val()=='')
{
	existingMsgDate = "";
	document.getElementById('allInvoice').innerHTML = "";
		$('#tblMainGrid2 >tbody >tr').each(function(){
			if($(this).index()!=0 && $(this).index()!=1 )
			{
				$(this).remove();
			}
		});
	$('#frmOtherGainLoss').get(0).reset();return;	
}
$('#txtNo').val($(this).val());
var httpobj = $.ajax({
	url:url,
	dataType:'json',
	data:'requestType=loadDetails&id='+URLEncode($(this).val()),
	async:false,
	success:function(json)
	{
		//json  = eval('('+json+')');
		$('#frmOtherGainLoss #cboCustomer').val(json.customer);
		$('#frmOtherGainLoss #cboCustomer').attr("disabled","disabled");
		$('#frmOtherGainLoss #txtDate').val(json.date);
		$('#frmOtherGainLoss #txtRemarks').val(json.remark);
		
		//--------------------------------------------------
		$('#tblMainGrid1 >tbody >tr').each(function(){
			if($(this).index()!=0 && $(this).index()!=1 )
			{
				$(this).remove();
			}
		});
		document.getElementById('allInvoice').innerHTML = "";
		var tBodyDetail = "";

		if(json.detailVal!=null)
		{
			var rowId = $('#tblMainGrid1').find('tr').length;
			var tbl = document.getElementById('tblMainGrid1');
			rows = $('#tblMainGrid1').find('tr').length;
			for(var j=0;j<=json.detailVal.length-1;j++)
			{
				tBodyDetail	= json.detailVal[j].tBodyDetail;
				if(j != json.detailVal.length)
				{
					document.getElementById('allInvoice').innerHTML+=tBodyDetail;
				}
			}
			receivedAmount();
		}
		else
		{
			
		}
	   //--------------------------------------------------
	   //--------------------------------------------------
		$('#tblMainGrid2 >tbody >tr').each(function(){
			if($(this).index()!=0 && $(this).index()!=1 )
			{
				$(this).remove();
			}
		});
		var chartAcc 	= "";
		var amount 		= "";
		var memo		= "";
		var dimension	= "";
		if(json.detailAccVal!=null)
		{
			var rowId = $('#tblMainGrid2').find('tr').length;
			var tbl = document.getElementById('tblMainGrid2');
			rows = $('#tblMainGrid2').find('tr').length;
			for(var j=0;j<=json.detailAccVal.length-1;j++)
			{
				chartAcc	= json.detailAccVal[j].chartAcc;
				amount		= json.detailAccVal[j].amount;
				memo		= json.detailAccVal[j].memo;
				dimension	= json.detailAccVal[j].dimension;
				if(j != json.detailAccVal.length-1)
				{
					tbl.insertRow(rows);
					tbl.rows[rows].innerHTML = tbl.rows[rows-1].innerHTML;
					tbl.rows[rows].cells[1].childNodes[1].value = chartAcc;
					tbl.rows[rows].cells[2].childNodes[1].value = amount;
					tbl.rows[rows].cells[3].childNodes[1].value = memo;
					tbl.rows[rows].cells[4].childNodes[1].value = dimension;
				}
				else
				{
					tbl.rows[1].cells[1].childNodes[1].value = chartAcc;
					tbl.rows[1].cells[2].childNodes[1].value = amount;
					tbl.rows[1].cells[3].childNodes[1].value = memo;
					tbl.rows[1].cells[4].childNodes[1].value = dimension;
				}
			}
			finalAmount();
		}
		else
		{
			
		}
	   //--------------------------------------------------
	}
});
});
//////////// end of load details /////////////////

  	$('#frmOtherGainLoss #butNew').click(function(){
		existingMsgDate = "";
		$('#frmOtherGainLoss').get(0).reset();
		document.getElementById('allInvoice').innerHTML = "";
		$('#tblMainGrid2 >tbody >tr').each(function(){
			if($(this).index()!=0 && $(this).index()!=1 )
			{
				$(this).remove();
			}
		});
		$('#frmOtherGainLoss #cboPaymentsMethods').change();//--->
		$('#frmOtherGainLoss #cboCustomer').attr("disabled","");
		loadCombo_frmOtherGainLoss();
		$('#frmOtherGainLoss #cboCustomer').focus();
	});
	$('#frmOtherGainLoss #butDelete').click(function(){
		if($('#frmOtherGainLoss #cboSearch').val()=='')
		{
			$('#frmOtherGainLoss #butDelete').validationEngine('showPrompt', 'Please select G/L Number.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmOtherGainLoss #cboSearch option:selected').text()+'" ?',{
			buttons: { Ok: true, Cancel: false },
			callback: function(v,m,f){
			if(v)
			{
					var url = "gainLoss-db-set.php";
					var httpobj = $.ajax({
						url:url,
						dataType:'json',
						data:'requestType=delete&cboSearch='+URLEncode($('#frmOtherGainLoss #cboSearch').val()),
						async:false,
						success:function(json){
							
							$('#frmOtherGainLoss #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
							
							if(json.type=='pass')
							{
								$('#frmOtherGainLoss').get(0).reset();
								document.getElementById('allInvoice').innerHTML = "";
								$('#tblMainGrid2 >tbody >tr').each(function(){
									if($(this).index()!=0 && $(this).index()!=1 )
									{
										$(this).remove();
									}
								});
								$('#frmOtherGainLoss #cboPaymentsMethods').change();//--->
								$('#frmOtherGainLoss #cboCustomer').attr("disabled","");
								loadCombo_frmOtherGainLoss();
								var t=setTimeout("alertDelete()",1000);return;
							}	
							var t=setTimeout("alertDelete()",3000);
						}	 
					});
			}
		}
			});	
		}
	});
});
////////////////////// calculation ////////////////////////////////
function receivedAmount()
{
	var receiveTotal = 0.00;
	$(".gainLoss").each( function(){
		if($(this).parent().find('.checkRow').attr('checked'))
		{
			  receiveTotal += eval($(this).html()==''?0.00:$(this).html());
		}
	});
	$('#txtRecAmount').val(receiveTotal.toFixed(4));
}
///////////////////////////////////////////////////////////////////
function loadCombo_frmOtherGainLoss()
{
	var url 	= "gainLoss-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmOtherGainLoss #cboSearch').html(httpobj.responseText);
}
function alertx()
{
	$('#frmOtherGainLoss #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmOtherGainLoss #butDelete').validationEngine('hide') ;
}
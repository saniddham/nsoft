<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];

$thisFilePath =  $_SERVER['PHP_SELF'];
include  "{$backwardseperator}dataAccess/permisionCheck.inc";
$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$companyId = $row['intCompanyId']; 
}
$receiptRefNo = $_REQUEST['id'];
// ======================Check Exchange Rate Updates========================
if($receiptRefNo == "")
{
	$status = "Adding";
}
else
{
	$status = "Changing";
}
$currentDate = date("Y-m-d");

$sql = "SELECT COUNT(*) AS 'no'
		FROM
		mst_financeexchangerate
		WHERE
		mst_financeexchangerate.dtmDate =  '$currentDate'
		";
$result = $db->RunQuery($sql);
$row= mysqli_fetch_array($result);
if($row['no']==0)
	{
		$str =  "Please Update Exchange Rates Before ".$status." Received Payments .";
		$str .= $row['NameList'];
		$maskClass="maskShow";
	}
	else
	{
		$maskClass="maskHide";
	}
// =========================================================================
?>
<script type="application/javascript" >
var recRefNo = '<?php echo $receiptRefNo ?>';
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Other Receivable / Service Customer -  Payments</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/tblstyle.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script src="receivedPayments-js.js" type="text/javascript"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
<script src="../../commanFunctions/numberExisting-js.js" type="text/javascript"></script>

<link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css">

<style type="text/css">
.setProperty 
{
  width:80px;
  text-align:righ; 
  background-color:#FFF;
  border:none;
}
</style>

</head>

<body onLoad="functionList();">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<div id="divMask" class="<?php echo $maskClass?> mask"> <?php echo $str; ?></div>

<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<form id="frmReceivedPayments" name="frmReceivedPayments" method="post" action="receivedPayments-db-set.php" autocomplete="off">
<div align="center">
  <div class="trans_layoutL">
    <div class="trans_text">Other Receivable / Service Customer - Payments</div>
<table width="100%">
      <tr>
        <td class="normalfnt"><img src="../../../../images/fb.png" width="18" height="19" /></td>
        <td align="right"><img src="../../../../images/ff.png" width="18" height="19" /></td>
      </tr>
      <tr>
      <td class="normalfntRight" width="34%">Receipt: </td>
      <td align="left" width="66%"><span class="normalfntMid">
        <select name="cboSearch" id="cboSearch"  style="width:350px" >
          <option value=""></option>
          <?php   $sql = "SELECT
							fin_other_receivable_payments_header.strReferenceNo,
							fin_other_receivable_payments_header.intReceiptNo,
							mst_finance_service_customer.strName
							FROM
							fin_other_receivable_payments_header
							Inner Join mst_finance_service_customer ON fin_other_receivable_payments_header.intCustomerId = mst_finance_service_customer.intId
							WHERE
							fin_other_receivable_payments_header.intCompanyId =  '$companyId' AND
							fin_other_receivable_payments_header.intDeleteStatus = '0'
							ORDER BY intReceiptNo DESC
							";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['strReferenceNo']."\">".$row['strReferenceNo']." - ".$row['strName']."</option>";
						}
          ?>
        </select>
      </span></td>
    </tr>
    <tr>
    <td colspan="2" height="22"></td>
    </tr>
    <tr>
    <td colspan="2" align="left"><table width="770" height="25" align="right">
    </table></td>  
    </tr>
    <tr>
      <td colspan="2">
      <table width="100%" class="tableBorder_allRound">
    <tr>
      <td class="normalfnt">&nbsp;</td>
      <td class="normalfnt">Receipt Number</td>
      <td><span class="normalfnt">
        <input name="txtNo" type="text" readonly="readonly"class="normalfntRight" id="txtNo" style="width:180px; background-color:#F4FFFF;text-align:center; border:dotted; border-color:#F00" onBlur="numberExisting(this,'Received Payments');" />
        <input checked="checked" type="checkbox" name="chkAutoManual" id="chkAutoManual" />
        <input name="amStatus" type="text" class="normalfntBlue" id="amStatus" style="width:40px; background-color:#FFF; text-align:center; border:thin" disabled="disabled" value="(Auto)"/>
      </span></td>
      <td>&nbsp;</td>
      <td bgcolor="#FFFFFF" class="">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
   <!-- ========Add by dulakshi 2013.03.20============== -->   
      <tr>
      <td width="19" class="normalfnt">&nbsp;</td>
      <td width="125" class="normalfnt">Ledger Accounts </td>
      <td width="273"><span class="normalfntMid">
        <select name="cboLedgerAcc" id="cboLedgerAcc"  style="width:211px" onchange="getSupplierList();">
          <option value=""></option>
          <?php  $sql = "SELECT DISTINCT
								mst_financechartofaccounts.intId,
								mst_financechartofaccounts.strCode,
								mst_financechartofaccounts.strName
						 FROM
								mst_financechartofaccounts
								Inner Join mst_finance_service_customer_activate ON mst_financechartofaccounts.intId = mst_finance_service_customer_activate.intChartOfAccountId
						 WHERE
								mst_finance_service_customer_activate.intCompanyId =  '$companyId'
						 GROUP BY
								mst_financechartofaccounts.intId
						 ORDER BY
								mst_financechartofaccounts.strCode ASC";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
						}
          ?>
        </select>
      </span></td>      
    </tr>
        <!-- ================================ -->  
    <tr>
      <td width="19" class="normalfnt">&nbsp;</td>
      <td width="125" class="normalfnt">Service Customer <span class="compulsoryRed">*</span></td>
          
  <!-- ========Edited by dulakshi 2013.03.20============== --> 
       <td width="273"><span class="normalfntMid">
           <select name="cboCustomer" id="cboCustomer"  style="width:211px" class="validate[required]" >          
           </select>   
       </span></td>
  <!-- ================================== --> 
  
      <td width="137"><span class="normalfnt">Date <span class="compulsoryRed">*</span></span></td>
      <td width="313" bgcolor="#FFFFFF" class=""><input name="txtDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="validate[required]" id="txtDate" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" onBlur="backDateExisting(this,'Other Receivable Payments');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
      <td width="13">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="normalfnt">Currency <span class="compulsoryRed">*</span></span></td>
      <td><select name="cboCurrency" id="cboCurrency" style="width:140px" class="validate[required]">
        <option value=""></option>
        <?php  $sql = "SELECT
						mst_financecurrency.intId,
						mst_financecurrency.strCode,
						mst_financecurrencyactivate.intCompanyId
						FROM
						mst_financecurrency
						Inner Join mst_financecurrencyactivate ON mst_financecurrency.intId = mst_financecurrencyactivate.intCurrencyId
						WHERE
						mst_financecurrency.intStatus =  1 AND
						mst_financecurrencyactivate.intCompanyId = '$companyId'
						order by strCode
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strCode']."</option>";
						}
        				?>
      </select></td>
      <td><span class="normalfnt">Rate <span class="compulsoryRed">*</span></span></td>
      <td><span class="normalfntMid">
        <input class="rdoRate" type="radio" name="radio" id="rdoSelling" value="" />
Selling
<input class="rdoRate" type="radio" name="radio" id="rdoBuying" value="" />
Buying 
<input class="rdoRate" type="radio" name="radio" id="rdoAverage" value="" />
Average
<input type="text" name="txtRate" id="txtRate" style="width:75px; background-color:#9F9; border:thin; text-align:center" readonly="readonly" class="validate[custom[number],required] normalfntBlue"/>
<input type="checkbox" name="chkEdit" id="chkEdit" />
      </span></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="normalfnt">Received Amount</span></td>
      <td class="normalfnt">
      <input type="text" class="validate[custom[number]] normalfntMid" name="txtRecAmount" readonly="readonly" id="txtRecAmount" style="width:100px;background-color:#FCF;border:none;text-align:center" />
      <span class="normalfntMid">
      <input type="checkbox" name="chkCurrency" id="chkCurrency" />
      </span></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong class="normalfnt">Payment Method</strong></td>
      <td><select name="cboPaymentsMethods" id="cboPaymentsMethods" style="width:158px" >
        <option value=""></option>
        <?php  $sql = "SELECT
						intId,
						strName
						FROM mst_financepaymentsmethods
						WHERE
							intStatus = 1
						order by strName
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
						}
        				?>
      </select></td>
      <td><strong class="normalfnt">Reference No.</strong></td>
      <td><span class="normalfntMid">
        <input type="text" name="txtRefNo" id="txtRefNo" style="width:105px" />
      </span></td>
      <td>&nbsp;</td>
    </tr>
    <tr id="rwChequeDetails" style="display:none;background-color:#CCF">
      <td>&nbsp;</td>
      <td><strong class="normalfnt">Reference</strong> <strong class="normalfnt">Date</strong></td>
      <td><input name="txtRefDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="validate[required]" id="txtRefDate" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" />
        <input type="checkbox" name="chkPosted" id="chkPosted" />
        <span class="normalfnt">Posted</span></td>
      <td><strong class="normalfnt">Reference Organization</strong></td>
      <td><span class="normalfntMid">
        <input type="text" name="txtRefOrg" id="txtRefOrg" style="width:195px" />
      </span></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="normalfnt">Memo</span></td>
      <td colspan="2"><textarea name="txtRemarks" id="txtRemarks" cols="45" rows="2"></textarea></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
      </table>
      </td>
      </tr>
    <tr>
      <td colspan="2">
      <!--<div style="overflow:scroll;width:900px;height:250px;" id="divGrid">-->
      <table width="100%">
      <tr>
      <td align="right">
		<table width="100%" id="tblMainGrid1" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
        <thead>
        <tr class="">
          <td width="19" bgcolor="#FAD163" class="normalfntMid">&nbsp;</td>
          <td width="170"  height="26" bgcolor="#FAD163" class="normalfntMid"><strong>Job No.</strong></td>
          <td width="170"  bgcolor="#FAD163" class="normalfntMid"><strong>Document No.</strong></td>
          <td width="75"  bgcolor="#FAD163" class="normalfntMid"><strong>Date</strong></td>
          <td width="83" bgcolor="#FAD163" class="normalfntMid"  ><strong>Amount</strong></td>
          <td width="89" bgcolor="#FAD163" class="normalfntMid"  ><strong>To be Paid<a href="#" class="normalfntMid"></a></strong></td>
          <td width="82" bgcolor="#FAD163" class="normalfntMid"  ><strong>Currency</strong></td>
          <td width="65" bgcolor="#FAD163" class="normalfntMid"  ><strong>Rate</strong></td>
          <td width="80" bgcolor="#FAD163" class="normalfntMid"  ><strong>Paying Amount</strong></td>
          <td width="50" bgcolor="#FAD163" class="normalfntMid"  ><strong>Type</strong></td>
          </tr>
         </thead>
         <tbody id="allInvoice" class="normalfnt" bgcolor="#FFFFFF">

         </tbody>
        <tfoot>
        <tr class="normalfnt">
          <td bgcolor="#FFFFFF" class="normalfntMid">&nbsp;</td>
          <td bgcolor="#FFFFFF" class="normalfntMid">&nbsp;</td>
          <td align="center" bgcolor="#FFFFFF">&nbsp;</td>
          <td align="right" bgcolor="#FFFFFF">Total</td>
          <td align="right" bgcolor="#FFFFFF" class=""><span class="normalfntMid">
            <input name="txtTotalAmount" type="text" class="normalfntMid" readonly="readonly" id="txtTotalAmount" style="width:100%;background-color:#F3F3F3;border:none;text-align:right;font-weight:500" />
          </span></td>
          <td align="right"  bgcolor="#FFFFFF"><span class="normalfntMid">
            <input name="txttoBePaid" type="text" class="normalfntMid" readonly="readonly" id="txttoBePaid" style="width:100%;background-color:#F3F3F3;border:none;text-align:right;font-weight:500" />
          </span></td>
          <td align="center"  bgcolor="#FFFFFF">&nbsp;</td>
          <td align="center"  bgcolor="#FFFFFF">&nbsp;</td>
          <td  bgcolor="#FFFFFF"><span class="normalfntMid">
          <input name="txtTotAmount" type="text" class="normalfntMid" readonly="readonly" id="txtTotAmount" style="width:100%;background-color:#F3F3F3;border:none;text-align:right;font-weight:500" />
            </span></td>
          <td align="center"  bgcolor="#FFFFFF">&nbsp;</td>
        </tr>
        </tfoot>
        </table>
      </td>
      </tr>
      </table>
      <!--</div>-->
      </td>
      <tr>
      <td colspan="2" align="center"></td>
      </tr>
      <tr>
      <td colspan="2" align="center">
      <table width="100%" class="tableBorder_allRound">
      <tr>
      <td align="center" bgcolor="#FFFFFF" class="normalfntMid"><strong>Debit Account</strong></td>
      </tr>
      </table>
      </td>
      </tr>
      </tr>
            <tr>
      <td colspan="2" align="right"><img src="../../../../images/Tadd.jpg" width="92" height="24" onClick="insertRow();" /></td>
      </tr>
    <tr>
      <td colspan="2"><table width="100%">
        <tr>
          <td>
          <div style="overflow:scroll;width:900px;height:150px;" id="divGrid"><table width="100%" id="tblMainGrid2" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
        <tr class="">
          <td width="20" bgcolor="#FAD163" class="normalfntMid">Del</td>
          <td width="211" height="24" bgcolor="#FAD163" class="normalfntMid"><strong>Account <span class="compulsoryRed">*</span></strong></td>
          <td width="136" bgcolor="#FAD163" class="normalfntMid"><strong>Amount <span class="compulsoryRed">*</span></strong></td>
          <td width="422" bgcolor="#FAD163" class="normalfntMid"><strong>Memo</strong></td>
          <td width="105" bgcolor="#FAD163" class="normalfntMid"><strong><span class="compulsoryRed">*</span> Cost Center</strong></td>
        </tr>
        <tr class="normalfnt">
          <td bgcolor="#FFFFFF" class="normalfntMid">
          <img src="../../../../images/del.png" width="15" height="15" class="delImg"/></td>
          <td bgcolor="#FFFFFF" class="normalfntMid">
            <select name="cboChartOfAcc" id="cboChartOfAcc" style="width:100%" class="validate[required] cusAccount" >
              <option value=""></option>
              <?php  $sql2 = "SELECT
							mst_financechartofaccounts.intId,
							mst_financechartofaccounts.strCode,
							mst_financechartofaccounts.strName,
							mst_financechartofaccounts_companies.intCompanyId,
							mst_financechartofaccounts_companies.intChartOfAccountId
							FROM
							mst_financechartofaccounts
							Inner Join mst_financechartofaccounts_companies ON mst_financechartofaccounts_companies.intChartOfAccountId = mst_financechartofaccounts.intId
							WHERE
							intStatus = '1' AND  (intFinancialTypeId = '24'  OR  intFinancialTypeId = '23' OR  intFinancialTypeId = '11' OR  intFinancialTypeId = '6') AND strType = 'Posting' AND intCompanyId = '$companyId'
							order by strCode

						";
						$result2 = $db->RunQuery($sql2);
						while($row2=mysqli_fetch_array($result2))
						{
						   echo "<option value=\"".$row2['intId']."\">".$row2['strCode']."-".$row2['strName']."</option>";
						}
        			?>
            </select>
          </td>
          <td bgcolor="#FFFFFF">
          <input name="txtPayAccAmount" class="validate[required,custom[number]] calTotAmt" type="text" id="txtPayAccAmount" style="width:135px;text-align:right" /></td>
          <td  bgcolor="#FFFFFF">
          <input type="text" name="txtMemo" id="txtMemo" style="width:100%" class="memo" /></td>
          <td  bgcolor="#FFFFFF">
          <select name="cboDimension" class="validate[required] dimension" id="cboDimension"  style="width:100%;">
            <option value=""></option>
            <?php  $sql = "SELECT
									intId,
									strName
								FROM mst_financedimension
								WHERE
									intStatus = 1
								order by strName
								";
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
								}
                   ?>
          </select></td>
        </tr>
      </table>
      </div>
            </td>
          </tr>
          <tr>
          <td colspan="2">
          <table width="476">
                    <tr>
                      <td width="227" align="right" class="normalfntRight">Total </td>
                      <td width="135">
                      <input name="txtFinalAmount" type="text" disabled="disabled" id="txtFinalAmount" style="width:132px;text-align:right" /></td>
                      <td width="98" class="normalfnt">&nbsp;</td>
                    </tr>
                </table>
          </td>
          </tr>
        </table></td>
    </tr>
      <tr>
        <td colspan="2">
          <table width="100%">
            <tr>
              <td width="100%" height="34" class="tableBorder_allRound"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                <tr>
<td width="100%" align="center" bgcolor=""><img style="display:none" border="0" src="../../../../images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:none" border="0" src="../../../../images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/><img style="display:none" border="0" src="../../../../images/Tprint.jpg" alt="Print" name="butPrint" width="92" height="24" class="mouseover" id="butPrint" tabindex="25"/><img style="display:none" border="0" src="../../../../images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="25"/><a href="../../../../main.php"><img  src="../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                </tr>
              </table></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
</div>
</div>
</form>
</body>
</html>
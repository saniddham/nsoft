<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	$companyId	= $_SESSION["headCompanyId"];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	///
	/////////// type of print load part /////////////////////
	if($requestType=='loadCombo')
	{
		$sql = "SELECT
				fin_other_receivable_advancereceived_header.strReceiptNo,
				mst_finance_service_customer.strName,
				IFNULL(CONCAT(' - ',fin_other_receivable_advancereceived_header.strPerfInvoiceNo),'') AS refNo
				FROM
				fin_other_receivable_advancereceived_header
				Inner Join mst_finance_service_customer ON fin_other_receivable_advancereceived_header.intCustomer = mst_finance_service_customer.intId
				WHERE
				fin_other_receivable_advancereceived_header.intStatus =  '1'
				AND fin_other_receivable_advancereceived_header.intCompanyId =  '$companyId'
				ORDER BY
				fin_other_receivable_advancereceived_header.strReceiptNo DESC
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['strReceiptNo']."\">".$row['strReceiptNo'].$row['refNo']." (".$row['strName'].") "."</option>";
		}
		echo $html;
	}
	
	//===========Add by dulakshi 2013.03.20=========
	else if($requestType=='loadCustomer')
	{
		$ledAcc  = $_REQUEST['ledgerAcc'];		
							
		$condition == "";
			if($ledAcc != "")
			{	
				$condition .= "AND mst_finance_service_customer_activate.intChartOfAccountId = '$ledAcc'";			
			}
				
		$sql = "SELECT
					mst_finance_service_customer.intId,
					mst_finance_service_customer.strName,
					mst_finance_service_customer_activate.intCompanyId
				FROM
					mst_finance_service_customer
					Inner Join mst_finance_service_customer_activate ON mst_finance_service_customer.intId = mst_finance_service_customer_activate.intCustomerId
				WHERE
					mst_finance_service_customer.intStatus =  '1' AND
					mst_finance_service_customer_activate.intCompanyId =  '$companyId' " .$condition . "					
				order by mst_finance_service_customer.strName ASC";		
				
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
	}
	//==============================================
	
	else if($requestType=='loadExchangeRates')
	{
		$currency  = $_REQUEST['currency'];
		$date  = $_REQUEST['date'];
		$sql = "SELECT
				mst_financeexchangerate.dblBuying, 
				mst_financeexchangerate.dblSellingRate  
				FROM mst_financeexchangerate
				WHERE
				mst_financeexchangerate.intCurrencyId =  '$currency' and mst_financeexchangerate.dtmDate =  '$date'";
		$result = $db->RunQuery($sql);
		
		$response['sellingRate'] = '';
		$response['buyingRate'] = '';
		while($row=mysqli_fetch_array($result))
		{
			$response['sellingRate'] = $row['dblSellingRate'];
			$response['buyingRate'] = $row['dblBuying'];
		}
		echo json_encode($response);
	}
	
	
?>
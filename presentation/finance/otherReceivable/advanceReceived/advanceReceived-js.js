// JavaScript Document
var amStatus = "Auto";
function functionList()
{
	//---------------------------------------------------
		amStatus = "Auto";
		//document.getElementById("chkAutoManual").style.display='none';
		document.getElementById("amStatus").style.display='none';
		$("#frmAdvanceReceived #txtReceiptNo").attr("readonly","readonly");
	 //---------------------------------------------------
	if($('#frmAdvanceReceived #cboSearch').val()=='')
	{
	//---------------------------------------------------
		existingMsgDate = "";
		amStatus = "Auto";
		//document.getElementById("chkAutoManual").style.display='';
		document.getElementById("amStatus").style.display='';
		$('#frmAdvanceReceived #txtReceiptNo').removeClass('validate[required]');
		$("#frmAdvanceReceived #txtReceiptNo").attr("readonly","readonly");
	//---------------------------------------------------
	}
}
$(document).ready(function() {
	
	// TODO: ===========Add by dulaskshi 2013.03.20===========
	getSupplierList();
	
  		$("#frmAdvanceReceived").validationEngine();
		$('#frmAdvanceReceived #cboCustomer').focus();
		
		$("#insertRow").click(function(){
			insertRow();
		});
		//-----------------------------------
		$('.accounts').change(function(){
			loadRecievedFrom(this);
		});
		//-------------------------------
		$('.delImg').click(function(){
			var rowCount = document.getElementById('tblDebitAccount').rows.length;
			if(rowCount>2)
			$(this).parent().parent().remove();
			calculateTotalVal();
			
		});
		//--------------------------------------- 
		$('.ammount').keyup(function(){
			calculateTotalVal();
		});
		//-------------------------------------- 
		
	//--------------------------------------------
		
  //permision for add 
  if(intAddx)
  {
 	$('#frmAdvanceReceived #butNew').show();
	$('#frmAdvanceReceived #butSave').show();
	$('#frmAdvanceReceived #butPrint').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmAdvanceReceived #butSave').show();
	//$('#frmAdvanceReceived #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
	$('#frmAdvanceReceived #butPrint').show();
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmAdvanceReceived #butDelete').show();
	//$('#frmAdvanceReceived #cboSearch').removeAttr('disabled');
  }
  
  //permision for view
  if(intViewx)
  {
	//$('#frmAdvanceReceived #cboSearch').removeAttr('disabled');
  }
   //===================================================================
 	$('#frmAdvanceReceived #chkAutoManual').click(function(){
	  if($('#frmAdvanceReceived #chkAutoManual').attr('checked'))
	  {
		  amStatus = "Auto";
		  $('#frmAdvanceReceived #amStatus').val('Auto');
		  $('#frmAdvanceReceived #txtReceiptNo').val('');
		  $("#frmAdvanceReceived #txtReceiptNo").attr("readonly","readonly");
		  $('#frmAdvanceReceived #txtReceiptNo').removeClass('validate[required]');
	  }
	  else
	  {
		  amStatus = "Manual";
		  $('#frmAdvanceReceived #amStatus').val('Manual');
		  $('#frmAdvanceReceived #txtReceiptNo').val('');
		  $("#frmAdvanceReceived #txtReceiptNo").attr("readonly","");
		  $('#frmAdvanceReceived #txtReceiptNo').focus();
		  $('#frmAdvanceReceived #txtReceiptNo').addClass('validate[required]');
	  }
  });
 //===================================================================
 //-------------------------------------------- 
  $('#frmAdvanceReceived #cboCurrency').change(function(){
	    var currency = $('#cboCurrency').val();
	    var date = $('#txtDate').val();
		var url 		= "advanceReceived-db-get.php?requestType=loadExchangeRates";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"currency="+currency+'&date='+date,
			async:false,
			success:function(json){

					document.getElementById("txtRate").value=json.sellingRate;
					document.getElementById("exchSelling").value=json.sellingRate;
					document.getElementById("exchBuying").value=json.buyingRate;
					document.getElementById("rdoAverage").value=((eval(json.buyingRate) + eval(json.sellingRate))/2).toFixed(4);
					//document.getElementById("txtRate").innerHTML=json.buyingRate;
			}
		});
		
  });
  //-------------------------------------------------------
  $('#frmAdvanceReceived #exchSelling').click(function(){
					document.getElementById("txtRate").value=document.getElementById("exchSelling").value;
					//document.getElementById("txtRate").innerHTML=json.buyingRate;
  });
  //-------------------------------------------------------
  $('#frmAdvanceReceived #exchBuying').click(function(){
		document.getElementById("txtRate").value=document.getElementById("exchBuying").value;
  });
  //-------------------------------------------------------------
  //-------------------------------------------------------
  $('#frmAdvanceReceived #rdoAverage').click(function(){
		document.getElementById("txtRate").value=document.getElementById("rdoAverage").value;
  });
  //-------------------------------------------------------------
  $('#frmAdvanceReceived #chkEdit').click(function(){
	  if(document.getElementById("chkEdit").checked==true)
		$('#frmAdvanceReceived #txtRate').removeAttr('disabled');
		else
		$('#frmAdvanceReceived #txtRate').attr("disabled",true);

  });
  //------------------------------------------------------------
   $('#frmAdvanceReceived #cboPaymentMethod').change(function(){
	    var payMethod = $('#cboPaymentMethod').val();
		if(payMethod==2){
		document.getElementById("rwChequeDetails").style.display='';
		}
		else{
		document.getElementById("rwChequeDetails").style.display='none';
		}
  });
	//----------------------------------------------------------
	$('.calTot').keyup(function(){
		calculateTotalVal();
	});
	//-----------------------------------------------------------
    $('#frmAdvanceReceived #butDelete').click(function(){
		var serial=URLEncode($('#frmAdvanceReceived #txtReceiptNo').val());
		if($('#frmAdvanceReceived #txtReceiptNo').val()=='')
		{
			//$('#frmBankDeposit #butDelete').validationEngine('showPrompt', 'Please select Deposit No.', 'fail');
		//	var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
                        var customerId=$('#frmAdvanceReceived #cboCustomer').val()
			var val = $.prompt('Are you sure you want to delete "'+serial+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = "advanceReceived-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											data:'requestType=delete&serialNo='+serial+"&customer="+customerId,
											async:false,
											success:function(json){
												
												$('#frmAdvanceReceived #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												if(json.type=='pass')
												{
													$('#frmAdvanceReceived').get(0).reset();
													
													clearForm();
													loadCombo_search();
													var t=setTimeout("alertDelete()",1000);return;
												}
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
		
	});
	//-----------------------------------------------------------
 
  $('#frmAdvanceReceived #butSave').click(function(){
	if(existingMsgDate == "")
	{
		  calculateTotalVal();
		  var RecvedAmmount=parseFloat(document.getElementById("txtRecvAmmount").value).toFixed(2);
		  var totAccQty=parseFloat(document.getElementById("txtTotal").value).toFixed(2);
		  
		var requestType = '';
		if ($('#frmAdvanceReceived').validationEngine('validate'))   
		{
			  if(checkForDuplicate()==false){
				return false;  
			  }
			  else if(RecvedAmmount!=totAccQty){
				alert('Recieved Ammount does not tally with total Account Ammount');
				return false;  
			  }
			  if(document.getElementById('chkPosted').checked==true)
				var posted = 1;
			  else
				var posted = 0;
	
			var data = "requestType=save";
			
				data+="&serialNo="		+	URLEncode($('#cboSearch').val());
				data+="&customer="	+	$('#cboCustomer').val();
				data+="&currency="	+	$('#cboCurrency').val();
				data+="&recvAmmount="			+	$('#txtRecvAmmount').val();
				data+="&invoiceNo="	+	URLEncode($('#txtPerfInvoiceNo').val());
				data+="&paymentMethod="		+	$('#cboPaymentMethod').val();
				data+="&txtpaymentMethod="		+	$('#cboPaymentMethod').text();
				data+="&posted="	+	posted;
				data+="&remarks="			+	URLEncode($('#txtRemarks').val());
				data+="&date="		+	$('#txtDate').val();
				data+="&refDate="		+	$('#txtRefDate').val();
				data+="&rate="		+	$('#txtRate').val();
				data+="&refNo="	+	$('#txtRafNo').val();
				data+="&refOrganization=" +	$('#txtRefOrganization').val();
				data+="&amStatus="		+	amStatus;
				data+="&manualNo="		+	$('#txtReceiptNo').val();
	
	
				var rowCount = document.getElementById('tblDebitAccount').rows.length;
				if(rowCount==1){
					alert("No Accounts selected");
					return false;				
				}
				var row = 0;
				
				var arr="[";
				
				for(var i=1;i<rowCount;i++)
				{
						 
						var account = 	document.getElementById('tblDebitAccount').rows[i].cells[1].childNodes[0].value;
						var ammount = 	document.getElementById('tblDebitAccount').rows[i].cells[2].childNodes[0].value;
						var memo = 	document.getElementById('tblDebitAccount').rows[i].cells[3].childNodes[0].value;
						var dimention = 	document.getElementById('tblDebitAccount').rows[i].cells[4].childNodes[0].value;
						
							arr += "{";
							arr += '"account":"'+		account +'",' ;
							arr += '"ammount":"'+		ammount +'",' ;
							arr += '"memo":"'+		URLEncode(memo) +'",' ;
							arr += '"dimention":"'+		dimention  +'"' ;
							arr +=  '},';
							
				}
				arr = arr.substr(0,arr.length-1);
				arr += " ]";
				
				data+="&arr="	+	arr;
			///////////////////////////// save main infomations /////////////////////////////////////////
			var url = "advanceReceived-db-set.php";
			var obj = $.ajax({
				url:url,
				
				dataType: "json",  
				data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
				//data:'{"requestType":"addsampleInfomations"}',
				async:false,
				success:function(json){
	
						$('#frmAdvanceReceived #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
						if(json.type=='pass')
						{
							loadCombo_search();
							$('#cboSearch').val(json.serialNo);
							var t=setTimeout("alertx()",1000);
							$('#txtReceiptNo').val(json.serialNo);
						//	$('#txtRetSupYear').val(json.year);
							return;
						}
					},
				error:function(xhr,status){
						
						$('#frmAdvanceReceived #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						var t=setTimeout("alertx()",3000);
						//function (xhr, status){errormsg(status)}
					}		
				});
		}
	}
	else
	{
		$('#frmAdvanceReceived #butSave').validationEngine('showPrompt', existingMsgDate,'fail');
		var t=setTimeout("alertx()",5000);
	}
   });
   

	//--------------refresh the form----------
	$('#frmAdvanceReceived #butNew').click(function(){
		//---------------------------------------------------
			existingMsgDate = "";
			amStatus = "Auto";
			$("#frmAdvanceReceived #txtReceiptNo").attr("readonly","readonly");
			$('#frmAdvanceReceived #chkAutoManual').attr('checked')
			$("#frmAdvanceReceived #chkAutoManual").attr("disabled","");
			//document.getElementById("chkAutoManual").style.display='';
			document.getElementById("amStatus").style.display='';
			$('#frmAdvanceReceived #txtReceiptNo').removeClass('validate[required]');
		//---------------------------------------------------
		$('#frmAdvanceReceived').get(0).reset();
		clearRows();
		$('#frmAdvanceReceived #txtReceiptNo').val('');
		$('#frmAdvanceReceived #cboSearch').val('');
		$('#frmAdvanceReceived #cboCustomer').val('');
		$('#frmAdvanceReceived #cboCurrency').val('');
		$('#frmAdvanceReceived #txtRecvAmmount').val('');
		$('#frmAdvanceReceived #txtPerfInvoiceNo').val('');
		$('#frmAdvanceReceived #cboPaymentMethod').val('');
		$('#frmAdvanceReceived #chkPosted').val('');
		$('#frmAdvanceReceived #txtRemarks').val('');
		$('#frmAdvanceReceived #txtDate').val('');
		$('#frmAdvanceReceived #txtRefDate').val('');
		$('#frmAdvanceReceived #txtRate').val('');
		$('#frmAdvanceReceived #txtRafNo').val('');
		$('#frmAdvanceReceived #txtRefOrganization').val('');
		$('#frmAdvanceReceived #txtTotal').val(0);
		//$('#frmAdvanceReceived #cboCustomer').focus();
		$('#frmAdvanceReceived #txtPerfInvoiceNo').focus();
		
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmAdvanceReceived #txtDate').val(d);
		$('#frmAdvanceReceived #txtRefDate').val(d);
		
		document.getElementById("rwChequeDetails").style.display='none';
	});
//----------------------------	
			$('.delImg').click(function(){
			    var rowCount = document.getElementById('tblDebitAccount').rows.length;
				if(rowCount>2)
				$(this).parent().parent().remove();
			});


//-----------------------------------
$('#butReport').click(function(){
	if($('#txtRetSupNo').val()!=''){
		window.open('../listing/rptReturnToSupplier.php?retSupNo='+$('#txtRetSupNo').val()+'&year='+$('#txtRetSupYear').val());	
	}
	else{
		alert("There is no Return Note to view");
	}
});
//----------------------------------	
$('#butConfirm').click(function(){
	if($('#txtRetSupNo').val()!=''){
		window.open('../listing/rptReturnToSupplier.php?retSupNo='+$('#txtRetSupNo').val()+'&year='+$('#txtRetSupYear').val()+'&approveMode=1');	
	}
	else{
		alert("There is no Return Note to confirm");
	}
});
//-----------------------------------------------------
$('#butClose').click(function(){
		//load('main.php');	
});
//--------------refresh the form-----------------------
	$('#frmAdvanceReceived #butNew').click(function(){
		$('#frmAdvanceReceived').get(0).reset();
		clearRows();
		$('#frmAdvanceReceived #txtReceiptNo').val('');
		$('#frmAdvanceReceived #cboSearch').val('');
		$('#frmAdvanceReceived #cboCustomer').val('');
		$('#frmAdvanceReceived #cboCurrency').val('');
		$('#frmAdvanceReceived #txtRecvAmmount').val('');
		$('#frmAdvanceReceived #txtPerfInvoiceNo').val('');
		$('#frmAdvanceReceived #cboPaymentMethod').val('');
		$('#frmAdvanceReceived #chkPosted').val('');
		$('#frmAdvanceReceived #txtRemarks').val('');
		$('#frmAdvanceReceived #txtDate').val('');
		$('#frmAdvanceReceived #txtRefDate').val('');
		$('#frmAdvanceReceived #txtRate').val('');
		$('#frmAdvanceReceived #txtRafNo').val('');
		$('#frmAdvanceReceived #txtRefOrganization').val('');
		$('#frmAdvanceReceived #txtTotal').val(0);
		//$('#frmAdvanceReceived #cboCustomer').focus();
		$('#frmAdvanceReceived #txtPerfInvoiceNo').focus();
		
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmAdvanceReceived #txtDate').val(d);
		$('#frmAdvanceReceived #txtRefDate').val(d);
		
		document.getElementById("rwChequeDetails").style.display='none';
	});
//-----------------------------------------------------

$('#frmAdvanceReceived #butPrint').click(function(){
		if($('#frmAdvanceReceived #txtReceiptNo').val()=='')
		{
			$('#frmAdvanceReceived #butPrint').validationEngine('showPrompt', 'Please select Receipt.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var myurl = 'listing/advanceReceivedDetails.php?id='+URLEncode($('#frmAdvanceReceived #txtReceiptNo').val());
    		window.open(myurl); 
		}
	});

});

//----------end of ready -------------------------------

function alertx()
{
	$('#frmAdvanceReceived #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmAdvanceReceived #butDelete').validationEngine('hide')	;
	$('#frmAdvanceReceived #butPrint').validationEngine('hide') ;
}

//----------------------------------------------
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
//----------------------------------------------- 
function clearRows()
{
	var rowCount = document.getElementById('tblDebitAccount').rows.length;

	for(var i=2;i<rowCount;i++)
	{
			document.getElementById('tblDebitAccount').deleteRow(1);
	}
	document.getElementById('tblDebitAccount').rows[1].cells[1].childNodes[0].value='';
	document.getElementById('tblDebitAccount').rows[1].cells[2].childNodes[0].value='';
	document.getElementById('tblDebitAccount').rows[1].cells[3].childNodes[0].value='';
	document.getElementById('tblDebitAccount').rows[1].cells[4].childNodes[0].value='';
}
//-----------------------------------------------------------------
function insertRow()
{
	var tbl = document.getElementById('tblDebitAccount');	
	var rows = tbl.rows.length;
	//tbl.rows[1].cells[5].childNodes[0].nodeValue;
	tbl.insertRow(rows);
	tbl.rows[rows].innerHTML = tbl.rows[rows-1].innerHTML;
	document.getElementById('tblDebitAccount').rows[rows].cells[2].childNodes[0].value='';
	document.getElementById('tblDebitAccount').rows[rows].cells[3].childNodes[0].value='';
	
		//-----------------------------------
		$('.accounts').change(function(){
			loadRecievedFrom(this);
		});
		//-------------------------------
		$('.delImg').click(function(){
			var rowCount = document.getElementById('tblDebitAccount').rows.length;
			if(rowCount>2)
			$(this).parent().parent().remove();
			calculateTotalVal();
			
		});
		//--------------------------------------- 
		$('.ammount').keyup(function(){
			calculateTotalVal();
		});
		
		//-------------------------------------- 
}			
//------------------------------------------------------------------------------
function calculateTotalVal(){
	var rowCount = document.getElementById('tblDebitAccount').rows.length;
	var row = 0;
	var totQty=0;
	
	for(var i=1;i<rowCount;i++)
	{
		if(document.getElementById('tblDebitAccount').rows[i].cells[2].childNodes[0].value==''){
		var qty=0;
		}
		else{
		var qty= 	document.getElementById('tblDebitAccount').rows[i].cells[2].childNodes[0].value;
		}

	//	document.getElementById('tblDebitAccount').rows[i].cells[2].childNodes[0].value=
		if(isNaN(qty)==true){
			qty=0; 
		}
		totQty+=parseFloat(qty);
	}
		totQty=totQty.toFixed(2);

	$('#txtTotal').val(totQty);
	$('#txtRecvAmmount').val(totQty);
	if(isNaN(totQty)==true){
		$('#txtTotal').val('0');
		$('#txtRecvAmmount').val('0');
	}

}
//--------------------------------------------------------------------------------
function checkForDuplicate(){
	var rowCount = document.getElementById('tblDebitAccount').rows.length;
	//var row=this.parentNode.parentNode.rowIndex;
	var k=0;
	for(var row=1;row<rowCount;row++){
		for(var i=1;i<row;i++){
			if(document.getElementById('tblDebitAccount').rows[i].cells[1].childNodes[0].value==document.getElementById('tblDebitAccount').rows[row].cells[1].childNodes[0].value)	{
				k=1;		
			}
		}
	}
	if(k==0){
		return true
	}
	else{
		alert("duplicate Accounts existing");
		return false
	}
}
//-----------------------------------------------------------------------------
function loadCombo_search()
{
	var url 	= "advanceReceived-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmAdvanceReceived #cboSearch').html(httpobj.responseText);
}
//------------------------------------------------------------------------------
function clearForm(){
		$('#frmAdvanceReceived').get(0).reset();
		clearRows();
		$('#frmAdvanceReceived #txtReceiptNo').val('');
		$('#frmAdvanceReceived #cboSearch').val('');
		$('#frmAdvanceReceived #cboCustomer').val('');
		$('#frmAdvanceReceived #cboCurrency').val('');
		$('#frmAdvanceReceived #txtRecvAmmount').val('');
		$('#frmAdvanceReceived #txtPerfInvoiceNo').val('');
		$('#frmAdvanceReceived #cboPaymentMethod').val('');
		$('#frmAdvanceReceived #chkPosted').val('');
		$('#frmAdvanceReceived #txtRemarks').val('');
		$('#frmAdvanceReceived #txtDate').val('');
		$('#frmAdvanceReceived #txtRefDate').val('');
		$('#frmAdvanceReceived #txtRate').val('');
		$('#frmAdvanceReceived #txtRafNo').val('');
		$('#frmAdvanceReceived #txtRefOrganization').val('');
		$('#frmAdvanceReceived #txtTotal').val(0);
		//$('#frmAdvanceReceived #cboCustomer').focus();
		$('#frmAdvanceReceived #txtPerfInvoiceNo').focus();
		
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmAdvanceReceived #txtDate').val(d);
		$('#frmAdvanceReceived #txtRefDate').val(d);
		
		document.getElementById("rwChequeDetails").style.display='none';
}

//=========Add by dulaskshi 2013.03.20===========
//====================Get Supplier List==============================
function getSupplierList()
{	
	$ledgerAccId = $('#frmAdvanceReceived #cboLedgerAcc').val();
	$searchId = $('#frmAdvanceReceived #cboSearch').val();
	
	var url = "advanceReceived-db-get.php?requestType=loadCustomer&ledgerAcc="+$ledgerAccId;
	var httpobj = $.ajax({url:url,async:false})
	if($searchId == '')
		{
			$('#frmAdvanceReceived #cboCustomer').html(httpobj.responseText);
		}	
}
//------------------------------------------------------------------------------
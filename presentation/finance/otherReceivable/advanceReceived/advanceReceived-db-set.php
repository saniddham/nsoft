<?php 
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$locationId 	= $_SESSION['CompanyID'];
	$companyId = $_SESSION['headCompanyId'];
	include "{$backwardseperator}dataAccess/Connector.php";
        include "../../commanFunctions/CommanEditAndDelete.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	
	$serialNo = $_REQUEST['serialNo'];
	$customer = $_REQUEST['customer'];
	$currency = $_REQUEST['currency'];
	$recvAmmount = $_REQUEST['recvAmmount'];
	$invoiceNo = $_REQUEST['invoiceNo'];
	$paymentMethod = $_REQUEST['paymentMethod'];
	$txtpaymentMethod = $_REQUEST['txtpaymentMethod'];
	$remarks = $_REQUEST['remarks'];
	$date = $_REQUEST['date'];
	$rate = $_REQUEST['rate'];
	$refNo = $_REQUEST['refNo'];
	$amStatus 	= $_REQUEST['amStatus'];
	$manualNo 	= $_REQUEST['manualNo'];
	
	if($paymentMethod=="2"){//cheque
	  $posted   = $_REQUEST['posted'];
	if($posted=='on'){
		$posted=1;
	}
	else{
		$posted=0;
	}
	$refDate 		 = $_REQUEST['refDate'];
	$refOrganization = $_REQUEST['refOrganization'];
	}
	else{
		$posted='0';
		$refDate='0000-00-00';
		$refOrganization='';
	}
	
        
	$arr 		= json_decode($_REQUEST['arr'], true);
	$accountPeriod 		= getLatestAccPeriod($companyId);
//------------save---------------------------	
	if($requestType=='save')
	{
            $chk=checkEditDeleteForUnrealize("ocus",$customer,'O.AReceived',$serialNo);            
            if($chk){
                $response['type'] = 'fail';
                $response['msg']  = "You cannot allow this process! Credit Note has some Unrealize Gain Or Loss";
                //echo json_encode($response);
            }
            else{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		if($serialNo==''){
			if($amStatus == "Auto")
			{
				$serialNo 	= getnextAdvPayNo($companyId);
			}
			else if($amStatus == "Manual")
			{
				$serialNo	= $manualNo;
			}
			$year = date('Y');
			$editMode=0;
		}
		else{
			$editMode=1;
		}
		//-----------delete and insert to header table---------------------------
		if($editMode==1){
                    $sql = "UPDATE `fin_other_receivable_advancereceived_header` SET intCustomer ='$customer', 
                            dtDate ='$date', 
                            dblRate ='$rate', 
                            dblReceivedAmount ='$recvAmmount', 
                            intPaymentMethod ='$paymentMethod', 
                            intCurrency ='$currency', 
                            strPerfInvoiceNo ='$invoiceNo', 
                            strReferenceNo ='$refNo', 
                            dtReferenceDate ='$refDate', 
                            intCheckPosted ='$posted', 
                            strReferenceOrganization ='$refOrganization', 
                            strRemarks ='$remarks', 
                                intModifyer ='$userId', 
                                intStatus ='1',
                                dtmModifyDate =now() 
                            WHERE (`strReceiptNo`='$serialNo')";
                    $result = $db->RunQuery2($sql);
                    //========update the transaction deader====================
                    $sql="SELECT fin_other_receivable_advancereceived_header.entryId FROM fin_other_receivable_advancereceived_header WHERE (`strReceiptNo`='$serialNo')";
                    $result = $db->RunQuery2($sql);
                    $row = mysqli_fetch_array($result);
                    $entryId=$row['entryId'];
                    
                    $sql="UPDATE fin_transactions SET 
                                entryDate='$date',                                                    
                                currencyId=$currency,
                                currencyRate='$rate',
                                transDetails='$remarks',                    
                                accPeriod=$accountPeriod,
                                payMethodId=$paymentMethod,
                                paymentNumber='$refNo',
                        WHERE entryId=$entryId";
                    $db->RunQuery2($sql);

                    $sqld = "DELETE FROM `fin_transactions_details` WHERE entryId=$entryId";
                    $resultd = $db->RunQuery2($sqld);
		}
		else{
			$sql = "DELETE FROM `fin_other_receivable_advancereceived_header` WHERE (`strReceiptNo`='$serialNo')";
			$result1 = $db->RunQuery2($sql);
                        
                        //Add data to transaction header*******************************************
                        $sql="INSERT INTO fin_transactions (entryDate, strProgramType, documentNo, currencyId, currencyRate, transDetails, payMethodId, paymentNumber, accPeriod, userId, companyId, createdOn) VALUES
                                ('$date','Other Advance Received','$serialNo',$currency,$rate,'$remarks',$paymentMethod,'$refNo',$accountPeriod,$userId,$companyId,now())";
                
                        $db->RunQuery2($sql);
                        $entryId=$db->insertId;
//echo $sql;
			$sql = "INSERT INTO `fin_other_receivable_advancereceived_header` (`strReceiptNo`,`intCustomer`,`dtDate`,dblRate,dblReceivedAmount,strReferenceNo,dtReferenceDate,intCheckPosted,strReferenceOrganization,strRemarks,intCreator,dtmCreateDate,intCompanyId,intPost,intPaymentMethod,intCurrency,strPerfInvoiceNo,entryId) 
					VALUES ('$serialNo','$customer','$date','$rate','$recvAmmount','$refNo','$refDate','$posted','$refOrganization','$remarks','$userId',now(),'$companyId','0','$paymentMethod','$currency','$invoiceNo',$entryId)";
			$result = $db->RunQuery2($sql);
		}
		//-----------delete and insert to detail table-----------------------
		if($result){
                    $sql = "DELETE FROM `fin_other_receivable_advancereceived_details` WHERE (`strReceiptNo`='$serialNo')";
                    $result2 = $db->RunQuery2($sql);		
					
			$toSave=0;
			$saved=0;
			$rollBackFlag=0;
			$totAmnt=0;
			foreach($arr as $arrVal)
			{
                            $account 	= $arrVal['account'];
                            $ammount 	= $arrVal['ammount'];
                            $memo 	= $arrVal['memo'];
                            $dimention  = $arrVal['dimention'];
                            if($dimention==''){
                                    $dimention=0;	
                            }
				
                            if($rollBackFlag!=1){
                               $sql = "INSERT INTO `fin_other_receivable_advancereceived_details` (`strReceiptNo`,`intAccountId`,`dblAmount`,`strMemo`,`intDimension`) 
                                VALUES ('$serialNo','$account','$ammount','$memo','$dimention')";
                                $result3 = $db->RunQuery2($sql);
	
                                $sql="INSERT INTO fin_transactions_details (`entryId`,`credit/debit`,`accountId`,`amount`,`details`,`dimensionId`) VALUES 
                                        ($entryId,'D',$account,$ammount,'$memo','$dimention')";
                                $result2 = $db->RunQuery2($sql);
                                $totAmnt+=$ammount;
                                if($result3==1){
                                    $saved++;
                                }
                                else{
                                    $rollBackFlag=1	;
                                }
				$toSave++;
                            }
			}//end of foreach
                        if($rollBackFlag!=1){
                            $sql = "SELECT
                                    mst_finance_service_customer_activate.intChartOfAccountId
                                    FROM mst_finance_service_customer_activate
                                    WHERE
                                    mst_finance_service_customer_activate.intCustomerId =  '$customer' AND
                                    mst_finance_service_customer_activate.intCompanyId =  '$companyId'";
                            $result = $db->RunQuery2($sql);
                            $row=mysqli_fetch_array($result);
                            $custAccount = $row['intChartOfAccountId'];
                            $totAmnt=$totAmnt;
                                                       
                            $sql="INSERT INTO fin_transactions_details (`entryId`,`credit/debit`,`accountId`,`amount`,`details`,`dimensionId`,personType, personId) VALUES 
                                    ($entryId,'C',$custAccount,$totAmnt,'$remarks',null,'ocus',$customer)";
                                                
                            $result4 = $db->RunQuery2($sql);
                            if(!$result4){
                                $rollBackFlag=1;
                            }
                        }
		}
		//echo $rollBackFlag;
		
		if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		else if(($result) && ($toSave==$saved)){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			if($editMode==1)
			$response['msg'] 		= 'Updated successfully.';
			else
			$response['msg'] 		= 'Saved successfully.';
			
			$response['serialNo'] 		= $serialNo;
			$response['year'] 		= $year;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		
		$db->CloseConnection();
            }
	}
//--------------------------------------------------------------------------------------	
	else if($requestType=='delete')
	{
            // ckeck Unrealize gain/loss for entry and if exist block edit and delete
            $chk=checkEditDeleteForUnrealize("ocus",$customer,'O.AReceived',$serialNo);            
            if($chk){
                $response['type'] = 'fail';
                $response['msg']  = "You cannot allow this process! Advance Received has some Unrealize Gain Or Loss";
                //echo json_encode($response);
            }
            else{
		try
		{
			$serialNo = $_REQUEST['serialNo'];
			$db->begin();
			//----------------added by lasantha @ CAIT on 9/8/2012----------------------
			$sql = "SELECT
					fin_other_receivable_payments_main_details.strDocNo,
					fin_other_receivable_payments_main_details.intCompanyId,
					fin_other_receivable_advancereceived_header.strReceiptNo,
					fin_other_receivable_payments_main_details.strDocType
					FROM
					fin_other_receivable_advancereceived_header
					Inner Join fin_other_receivable_payments_main_details ON fin_other_receivable_advancereceived_header.strReceiptNo = fin_other_receivable_payments_main_details.strDocNo
					Inner Join fin_other_receivable_payments_header ON fin_other_receivable_payments_main_details.intReceiptNo = fin_other_receivable_payments_header.intReceiptNo AND fin_other_receivable_payments_main_details.intAccPeriodId = fin_other_receivable_payments_header.intAccPeriodId AND fin_other_receivable_payments_main_details.intLocationId = fin_other_receivable_payments_header.intLocationId AND fin_other_receivable_payments_main_details.intCompanyId = fin_other_receivable_payments_header.intCompanyId AND fin_other_receivable_payments_main_details.strReferenceNo = fin_other_receivable_payments_header.strReferenceNo
					WHERE
					fin_other_receivable_payments_main_details.strDocNo =  '$serialNo' AND
					fin_other_receivable_payments_main_details.strDocType =  'O.AReceived' AND
					fin_other_receivable_payments_main_details.intCompanyId =  '$companyId' AND
					fin_other_receivable_payments_header.intDeleteStatus =  '0'
					";
		$result = $db->RunQuery2($sql);
		if(!mysqli_num_rows($result))
		{
			$sql = "UPDATE `fin_other_receivable_advancereceived_header` SET intStatus ='0', intModifyer ='$userId'  WHERE (`strReceiptNo`='$serialNo')  ";
			$result = $db->RunQuery2($sql);
			//==========UPDATE TRANS ACTION delete STATUS
			$sql="SELECT fin_other_receivable_advancereceived_header.entryId FROM fin_other_receivable_advancereceived_header WHERE (`strReceiptNo`='$serialNo')";
			$result = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($result);
			$entryId=$row['entryId'];                        
			$sqld = "UPDATE `fin_transactions` SET delStatus=1 WHERE entryId=$entryId";
			$resultd = $db->RunQuery2($sqld);	
		
			if(($result))
			{
				$db->commit();
				$response['type'] 		= 'pass';
				$response['msg'] 		= 'Deleted successfully.';
			}
			else
			{
				$response['type'] 		= 'fail';
				$response['msg'] 		= $db->errormsg;
				$response['q'] 			=$sql;
				$db->rollback();//roalback
			}
		}
		else
		{
			$db->rollback();
			$response['type'] 		= 'fail';
			$response['msg'] 		= "You cannot allow this process! Advance Received has some payements";
		}
		//------------------------------------------------------------------------------------------------------
            }catch(Exception $e){               
                $db->rollback();//roalback
                $response['type'] 		= 'fail';
                $response['msg'] 		= $e->getMessage();
                $response['q'] 			= $sql;                
            }
        }
    }
//-----------------------------------------------------------------------------------
	echo json_encode($response);
//-----------------------------------------------------------------------------------	
	function getnextAdvPayNo($companyId)
	{
		global $db;
		global $locationId;
	//---------------------	
		$sql = "SELECT
				Max(mst_financeaccountingperiod.dtmCreateDate),
				mst_financeaccountingperiod.dtmStartingDate, 
				substring(mst_financeaccountingperiod.dtmStartingDate,1,4) as fromY,
				substring(mst_financeaccountingperiod.dtmClosingDate,1,4) as toY 
				FROM mst_financeaccountingperiod ";	
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$accPeriod = $row['fromY']."-".$row['toY'];
	//--------------	
		$sql = "SELECT
				sys_finance_no.intOtherCusAdvancePaymentNo 
				FROM
				sys_finance_no
			WHERE
			sys_finance_no.intLocationId =  '$locationId' and sys_finance_no.intCompanyId = '$companyId'
				";	
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$advPayNo = $row['intOtherCusAdvancePaymentNo'];

	//------------------
	 $sql = "SELECT
			mst_companies.strCode AS company,
			mst_companies.intId,
			mst_locations.intCompanyId,
			mst_locations.strCode AS location,
			mst_locations.intId
			FROM
			mst_companies
			Inner Join mst_locations ON mst_locations.intCompanyId = mst_companies.intId
			WHERE
			mst_locations.intId =  '$locationId' AND
			mst_companies.intId =  '$companyId'
			";
	$result = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($result);
	$companyCode = $row['company'];
	$locationCode = $row['location'];
	//---------------------
		
		$sql = "UPDATE `sys_finance_no` SET intOtherCusAdvancePaymentNo=intOtherCusAdvancePaymentNo+1 WHERE (`intLocationId`='$locationId' AND  sys_finance_no.intCompanyId = '$companyId')  ";
		$db->RunQuery2($sql);
		
		return $companyCode."/".$locationCode."/".$accPeriod."/".$advPayNo;
	}
//--------------------------------------------------------------------------------------------
function getLatestAccPeriod($companyId)
{
        global $db;
        $sql = "SELECT
                        MAX(mst_financeaccountingperiod.intId) AS accId,
                        mst_financeaccountingperiod.dtmStartingDate,
                        mst_financeaccountingperiod.dtmClosingDate,
                        mst_financeaccountingperiod.intStatus,
                        mst_financeaccountingperiod_companies.intCompanyId,
                        mst_financeaccountingperiod_companies.intPeriodId
                        FROM
                        mst_financeaccountingperiod
                        Inner Join mst_financeaccountingperiod_companies ON mst_financeaccountingperiod_companies.intPeriodId = mst_financeaccountingperiod.intId
                        WHERE
                        mst_financeaccountingperiod_companies.intCompanyId =  '$companyId' AND mst_financeaccountingperiod.intStatus = '1'
                        ORDER BY
                        mst_financeaccountingperiod.intId DESC
                        ";	
        $result = $db->RunQuery($sql);
        $row = mysqli_fetch_array($result);
        $latestAccPeriodId = $row['accId'];	
        return $latestAccPeriodId;
}
//--------------------------------------------------------------------------------------------
	
?>
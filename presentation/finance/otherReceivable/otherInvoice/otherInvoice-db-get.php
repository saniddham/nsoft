<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$companyId = $row['intCompanyId']; 
	}
	/////////// sales invoice load part /////////////////////
	if($requestType=='loadCombo')
	{
		$sql = "SELECT
				fin_other_receivable_invoice_header.strReferenceNo,
				fin_other_receivable_invoice_header.intInvoiceNo,
				mst_finance_service_customer.strName
				FROM
				fin_other_receivable_invoice_header
				Inner Join mst_finance_service_customer ON fin_other_receivable_invoice_header.intCustomerId = mst_finance_service_customer.intId
				WHERE
				fin_other_receivable_invoice_header.intCompanyId =  '$companyId' AND
				fin_other_receivable_invoice_header.intDeleteStatus =  '0'
				ORDER BY intInvoiceNo DESC
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['strReferenceNo']."\">".$row['strReferenceNo']." - ".$row['strName']."</option>";
		}
		echo $html;
	}
	
	//===========Add by dulakshi 2013.03.20=========
	else if($requestType=='loadCustomer')
	{
		$ledAcc  = $_REQUEST['ledgerAcc'];		
							
		$condition == "";
			if($ledAcc != "")
			{	
				$condition .= "AND mst_finance_service_customer_activate.intChartOfAccountId = '$ledAcc'";			
			}
				
		$sql = "SELECT
					mst_finance_service_customer.intId,
					mst_finance_service_customer.strName,
					mst_finance_service_customer_activate.intCompanyId
				FROM
					mst_finance_service_customer
					Inner Join mst_finance_service_customer_activate ON mst_finance_service_customer.intId = mst_finance_service_customer_activate.intCustomerId
				WHERE
					mst_finance_service_customer.intStatus =  '1' AND
					mst_finance_service_customer_activate.intCompanyId =  '$companyId' " .$condition . "					
				order by mst_finance_service_customer.strName ASC";		
				
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
	}
	//==============================================
	
	else if($requestType=='loadDetails')
	{
		$id  = $_REQUEST['id'];
		
		//-------------------------------------------------------------
		$sql = "SELECT
				fin_other_receivable_invoice_details.intInvoiceNo,
				fin_other_receivable_invoice_details.intAccPeriodId,
				fin_other_receivable_invoice_details.intLocationId,
				fin_other_receivable_invoice_details.intCompanyId,
				fin_other_receivable_invoice_details.strReferenceNo,
				fin_other_receivable_invoice_details.intItem,
				fin_other_receivable_invoice_details.strStyleNo,
				fin_other_receivable_invoice_details.strGraphicNo,
				fin_other_receivable_invoice_details.strOrderNo,
				fin_other_receivable_invoice_details.strLineItem,
				fin_other_receivable_invoice_details.strCusLocation,
				fin_other_receivable_invoice_details.strItemDesc,
				fin_other_receivable_invoice_details.intUom,
				fin_other_receivable_invoice_details.dblQty,
				fin_other_receivable_invoice_details.dblUnitPrice,
				fin_other_receivable_invoice_details.dblDiscount,
				fin_other_receivable_invoice_details.dblTaxAmount,
				fin_other_receivable_invoice_details.intTaxGroupId,
				fin_other_receivable_invoice_details.intDimensionId,
				fin_other_receivable_invoice_header.intDeleteStatus
				FROM
				fin_other_receivable_invoice_details
				Inner Join fin_other_receivable_invoice_header ON fin_other_receivable_invoice_details.strReferenceNo = fin_other_receivable_invoice_header.strReferenceNo
				WHERE
				fin_other_receivable_invoice_details.strReferenceNo =  '$id' AND
				fin_other_receivable_invoice_header.intDeleteStatus =  '0'
				";
		$result = $db->RunQuery($sql);
		$arrDetail;
		while($row=mysqli_fetch_array($result))
		{
			$val['itemId'] 		= $row['intItem'];
			
			$val['styleNo'] 	= $row['strStyleNo'];
			$val['graphicNo'] 	= $row['strGraphicNo'];
			$val['orderNo'] 	= $row['strOrderNo'];
			$val['lineItem'] 	= $row['strLineItem'];
			$val['cusLocate'] 	= $row['strCusLocation'];
			$val['itemDesc'] 	= $row['strItemDesc'];
			
			$val['uom'] 		= $row['intUom'];
			$val['qty'] 		= $row['dblQty'];
			$val['unitPrice'] 	= $row['dblUnitPrice'];
			$val['discount'] 	= $row['dblDiscount'];
			$val['taxAmount'] 	= $row['dblTaxAmount'];
			$val['taxGroup'] 	= $row['intTaxGroupId'];
			$val['dimension'] 	= $row['intDimensionId'];
			$arrDetail[] = $val;
		}
		$response['detailVal'] = $arrDetail;
		//-----------------------------------------------------------
		
		$sql   = "SELECT
				fin_other_receivable_invoice_header.intInvoiceNo,
				fin_other_receivable_invoice_header.intAccPeriodId,
				fin_other_receivable_invoice_header.intLocationId,
				fin_other_receivable_invoice_header.intCompanyId,
				fin_other_receivable_invoice_header.strReferenceNo,
				fin_other_receivable_invoice_header.intCustomerId,
				fin_other_receivable_invoice_header.dtmDate,
				fin_other_receivable_invoice_header.strInvoiceType,
				fin_other_receivable_invoice_header.strAddress,
				fin_other_receivable_invoice_header.strShipTo,
				fin_other_receivable_invoice_header.intCurrencyId,
				fin_other_receivable_invoice_header.dblRate,
				fin_other_receivable_invoice_header.strRemark,
				fin_other_receivable_invoice_header.strPoNo,
				fin_other_receivable_invoice_header.intPaymentsTermsId,
				fin_other_receivable_invoice_header.intMarketerId,
				fin_other_receivable_invoice_header.dtmShipDate,
				fin_other_receivable_invoice_header.intShipmentId,
				fin_other_receivable_invoice_header.strMessage
				FROM
				fin_other_receivable_invoice_header
				WHERE
				fin_other_receivable_invoice_header.strReferenceNo =  '$id' AND
				fin_other_receivable_invoice_header.intDeleteStatus =  '0'
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['customer'] 	= $row['intCustomerId'];
			$response['date'] 		= $row['dtmDate'];
			$response['invoType'] 	= $row['strInvoiceType'];
			$response['address'] 	= $row['strAddress'];
			$response['shipTo'] 	= $row['strShipTo'];
			$response['currency']	= $row['intCurrencyId'];
			$response['rate'] 		= $row['dblRate'];
			$response['remark'] 	= $row['strRemark'];
			$response['poNo'] 		= $row['strPoNo'];
			$response['payTerms'] 	= $row['intPaymentsTermsId'];
			$response['marketer'] 	= $row['intMarketerId'];
			$response['shipDate'] 	= $row['dtmShipDate'];
			$response['shipment'] 	= $row['intShipmentId'];
			$response['message'] 	= $row['strMessage'];
		}
		//--------------------------------------------------
		$sql = "SELECT
				fin_other_receivable_invoice_header.strReferenceNo,
				fin_other_receivable_invoice_header.intCustomerId,
				INV.dblQty,
				INV.dblUnitPrice,
				INV.dblDiscount,
				INV.dblTaxAmount,
				sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) AS amount,
				(
				sum(((INV.dblQty*INV.dblUnitPrice) *(100-INV.dblDiscount)/100)+ IFNULL(INV.dblTaxAmount,0)) 
				-
				IFNULL ((SELECT
				Sum(fin_other_receivable_payments_main_details.dblPayAmount )AS paidAmount
				FROM fin_other_receivable_payments_main_details
				Inner Join fin_other_receivable_payments_header ON fin_other_receivable_payments_main_details.strReferenceNo = fin_other_receivable_payments_header.strReferenceNo
				WHERE
				fin_other_receivable_payments_main_details.strJobNo =  INV.strReferenceNo AND
				fin_other_receivable_payments_header.intDeleteStatus =  '0'  AND fin_other_receivable_payments_main_details.strDocType = 'O.SInvoice'
				GROUP BY
				fin_other_receivable_payments_main_details.strJobNo),0)
				) AS balAmount
				FROM
				fin_other_receivable_invoice_details AS INV
				Inner Join fin_other_receivable_invoice_header ON fin_other_receivable_invoice_header.strReferenceNo = INV.strReferenceNo AND INV.intInvoiceNo = fin_other_receivable_invoice_header.intInvoiceNo AND INV.intAccPeriodId = fin_other_receivable_invoice_header.intAccPeriodId AND INV.intCompanyId = fin_other_receivable_invoice_header.intCompanyId
				Inner Join mst_finance_service_customer ON mst_finance_service_customer.intId = fin_other_receivable_invoice_header.intCustomerId
				Inner Join mst_financecurrency ON mst_financecurrency.intId = fin_other_receivable_invoice_header.intCurrencyId
				WHERE
				fin_other_receivable_invoice_header.intDeleteStatus =  '0' AND fin_other_receivable_invoice_header.strReferenceNo = '$id'
				GROUP BY
				fin_other_receivable_invoice_header.strReferenceNo
				";//  AND INV.intLocationId = fin_other_receivable_invoice_header.intLocationId
			$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['totAmount'] 	= $row['amount'];
			$response['balAmount'] 	= $row['balAmount'];
		}
		//--------------------------------------------------
		echo json_encode($response);
	}
	else if($requestType=='getExchangeRate')
	{
		$currencyId  	= $_REQUEST['currencyId'];
		$exchangeDate	= $_REQUEST['exchangeDate'];
		
		$sql = "SELECT
					mst_financeexchangerate.dblSellingRate,
					mst_financeexchangerate.dblBuying
				FROM mst_financeexchangerate
				WHERE
					mst_financeexchangerate.intCurrencyId 	=  '$currencyId' AND
					mst_financeexchangerate.dtmDate 		=  '$exchangeDate'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		
		if(mysqli_num_rows($result)>0)
		{
			$arrValue['sellingRate'] 	= $row['dblSellingRate'];
			$arrValue['buyingRate'] 	= $row['dblBuying'];
		}
		else
		{
			$arrValue['sellingRate'] 	= "";
			$arrValue['buyingRate'] 	= "";
		}
		echo json_encode($arrValue);
	}
	else if($requestType=='getCustomerAddress') // with invoice type, currency and payment terms
	{
		$customerId  = $_REQUEST['customerId'];
		
		$sql = "SELECT 	mst_finance_service_customer.strAddress, mst_finance_service_customer.strInvoiceType,
				mst_finance_service_customer.intCurrencyId,
				mst_finance_service_customer.intPaymentsTermsId
				FROM mst_finance_service_customer	WHERE mst_finance_service_customer.intId =  '$customerId'";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$response['address'] = $row['strAddress'];
		$response['invoType']= $row['strInvoiceType'];
		$response['currency']= $row['intCurrencyId'];
		$response['payTerms']= $row['intPaymentsTermsId'];
		echo json_encode($response);
	}
	else if($requestType=='getItemDescription')
	{
		$itemId  = $_REQUEST['itemId'];
		
		$sql = "SELECT
				mst_financecustomeritem.intId,
				mst_financecustomeritem.strRemark
				FROM
				mst_financecustomeritem
				WHERE
				mst_financecustomeritem.intId =  '$itemId'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		echo $row['strRemark'];
	}
	else if($requestType=='getTaxValue')
	{
		$operation  = $_REQUEST['opType'];
		$amount  = $_REQUEST['valAmount'];
		$taxCodes = json_decode($_REQUEST['arrTaxCode'], true);
		
		if(count($taxCodes) != 0)
		{
			foreach($taxCodes as $taxCode)
			{
				$codeValues[] = callTaxValue($taxCode['taxId']);
			}
		}
		if(count($codeValues) > 1)
		{
			if($operation == 'Inclusive')
			{
				$firstVal = ($amount*$codeValues[0])/100;
				$withTaxVal = $firstVal + ((($amount+$firstVal)*$codeValues[1])/100);
				$val1 = ($amount*$codeValues[0])/100;
				$val2 = ((($amount+$firstVal)*$codeValues[1])/100);
			}
			else if($operation == 'Exclusive')
			{
				$withTaxVal = ($amount*($codeValues[0] + $codeValues[1]))/100;
				$val1 = ($amount*$codeValues[0])/100;
				$val2 = ($amount*$codeValues[1])/100;
			}
		}
		else if(count($codeValues) == 1 && $operation == 'Isolated')
		{
			$withTaxVal = ($amount*$codeValues[0])/100;
			$val1 = ($amount*$codeValues[0])/100;
		}
		echo $withTaxVal."/".$val1."/".$val2;
	}
	else if($requestType=='getTaxProcess')
	{
		$taxGrpId  = $_REQUEST['taxGroupId'];
		
		$sql = "SELECT
				mst_financetaxgroup.intId,
				mst_financetaxgroup.strProcess
				FROM
				mst_financetaxgroup
				WHERE
				mst_financetaxgroup.intId =  '$taxGrpId'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		echo $row['strProcess'];
	}
	function callTaxValue($taxId)
	{
		global $db;
		$sql = "SELECT
				mst_financetaxisolated.intId,
				mst_financetaxisolated.strCode,
				mst_financetaxisolated.dblRate
				FROM
				mst_financetaxisolated
				WHERE
				mst_financetaxisolated.intId = '$taxId'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$taxVal = $row['dblRate'];	
		return $taxVal;
	}
?>
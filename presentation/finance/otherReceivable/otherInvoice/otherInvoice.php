<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];

$thisFilePath =  $_SERVER['PHP_SELF'];
include  "{$backwardseperator}dataAccess/permisionCheck.inc";
$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$companyId = $row['intCompanyId']; 
}
$invoiceRefNo = $_REQUEST['id'];
// ======================Check Exchange Rate Updates========================
if($invoiceRefNo == "")
{
	$status = "Adding";
}
else
{
	$status = "Changing";
}
$currentDate = date("Y-m-d");

$sql = "SELECT COUNT(*) AS 'no'
		FROM
		mst_financeexchangerate
		WHERE
		mst_financeexchangerate.dtmDate =  '$currentDate'
		";
$result = $db->RunQuery($sql);
$row= mysqli_fetch_array($result);
if($row['no']==0)
	{
		$str =  "Please Update Exchange Rates Before ".$status." Other Invoice .";
		$str .= $row['NameList'];
		$maskClass="maskShow";
	}
	else
	{
		$maskClass="maskHide";
	}
// =========================================================================
?>
<script type="application/javascript" >
var invoRefNo = '<?php echo $invoiceRefNo ?>';
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Other Receivable / Service Customer - Invoice</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script src="otherInvoice-js.js" type="text/javascript"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
<script src="../../commanFunctions/numberExisting-js.js" type="text/javascript"></script>

<link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css">

<style type="text/css">
.apDiv1 {
	position:absolute;
	left:179px;
	top:116px;
	width:auto;
	height:auto;
	z-index:0;
}
</style>

</head>

<body onLoad="functionList();">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<div id="divMask" class="<?php echo $maskClass?> mask"> <?php echo $str; ?></div>

<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<div id="partPay" class= "apDiv1 maskHide" ><img src="../../../../images/partPayment.png"  /></div>
<div id="fullPay" class= "apDiv1 maskHide" ><img src="../../../../images/paymentCompleted.png"  /></div>
<div id="overPay" class= "apDiv1 maskHide" ><img src="../../../../images/overPayment.png"  /></div>
<div id="pending" class= "apDiv1 maskHide" ><img src="../../../../images/pending2.png"  /></div>

<form id="frmOtherInvoice" name="frmOtherInvoice" method="post" action="otherInvoice-db-set.php" autocomplete="off">
<div align="center">
  <div class="trans_layoutL">
    <div class="trans_text">Other Receivable / Service Customer - Invoice</div>
    <table width="100%">
      <tr>
      <td class="normalfnt" width="34%"><img src="../../../../images/fb.png" width="18" height="19" id="imgFb" /></td>
      <td align="right" width="66%"><img src="../../../../images/ff.png" width="18" height="19" id="imgFf" /></td>
    </tr>
      <tr> <!--style="display:none"-->
        <td align="right"><span class="normalfnt">Invoice: </span></td>
        <td align="left"><span class="normalfntMid">
          <select name="cboSearch" id="cboSearch"  style="width:350px" >
            <option value=""></option>
            <?php   $sql = "SELECT
							fin_other_receivable_invoice_header.strReferenceNo,
							fin_other_receivable_invoice_header.intInvoiceNo,
							mst_finance_service_customer.strName
							FROM
							fin_other_receivable_invoice_header
							Inner Join mst_finance_service_customer ON fin_other_receivable_invoice_header.intCustomerId = mst_finance_service_customer.intId
							WHERE
							fin_other_receivable_invoice_header.intCompanyId =  '$companyId' AND
							fin_other_receivable_invoice_header.intDeleteStatus =  '0'
							ORDER BY intInvoiceNo DESC
							";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['strReferenceNo']."\">".$row['strReferenceNo']." - ".$row['strName']."</option>";
						}
          ?>
          </select>
        </span></td>
      </tr>
     <tr>
    	<td colspan="2" align="left" height="25"></td>
     </tr>
      <!--<tr>
    <td colspan="2" align="left">
    <table width="770" height="25" align="right">
    <tr>
      <td width="524" align="right">&nbsp;</td>
    <td width="10" align="center">&nbsp;</td>
    <td width="194" align="right">&nbsp;</td>
      <td width="28" align="right">&nbsp;</td>
    </tr>
    </table>
    </td>  
    </tr>-->
    <tr>
      <td colspan="2">
      <table width="100%" class="tableBorder_allRound">
    <tr>
      <td class="normalfnt">&nbsp;</td>
      <td class="normalfnt"><span class="name">Invoice Number</span></td>
      <td><span class="normalfnt">
        <input name="txtNo" type="text" readonly="readonly"class="normalfntRight" id="txtNo" style="width:180px; background-color:#F4FFFF;text-align:center; border:dotted; border-color:#F00" onBlur="numberExisting(this,'Other Invoice');" /> <input checked="checked" type="checkbox" name="chkAutoManual" id="chkAutoManual" /> <input name="amStatus" type="text" class="normalfntBlue" id="amStatus" style="width:40px; background-color:#FFF; text-align:center; border:thin" disabled="disabled" value="(Auto)"/>
</td>
      <td>&nbsp;</td>
      <td bgcolor="#FFFFFF" class="">&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
  <!-- ========Add by dulakshi 2013.03.20============== -->      
    <tr>
      <td width="13" class="normalfnt">&nbsp;</td>
      <td width="127" class="normalfnt">Ledger Accounts </td>
      <td width="335"><span class="normalfntMid">
        <select name="cboLedgerAcc" id="cboLedgerAcc"  style="width:211px" onchange="getSupplierList();" >
        <option value=""></option>
          <?php  $sql = "SELECT DISTINCT
								mst_financechartofaccounts.intId,
								mst_financechartofaccounts.strCode,
								mst_financechartofaccounts.strName
						 FROM
								mst_financechartofaccounts
								Inner Join mst_finance_service_customer_activate ON mst_financechartofaccounts.intId = mst_finance_service_customer_activate.intChartOfAccountId
						 WHERE
								mst_finance_service_customer_activate.intCompanyId =  '$companyId'
						 GROUP BY
								mst_financechartofaccounts.intId
						 ORDER BY
								mst_financechartofaccounts.strCode ASC";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
						}
          ?>
        </select>
          
        </span></td>     
    </tr>
      <!-- ================================ -->    
    <tr>
      <td width="13" class="normalfnt">&nbsp;</td>
      <td width="127" class="normalfnt">Service Customer <span class="compulsoryRed">*</span></td>
             
    <!-- ========Edited by dulakshi 2013.03.20============== --> 
       <td width="335"><span class="normalfntMid">
           <select name="cboCustomer" id="cboCustomer"  style="width:211px" class="validate[required]" >          
           </select>
              <img id="butcustomer" src="../../../../images/add_new.png" width="15" height="15" style="display:none" /> 
       </span></td>
  <!-- ================================== --> 
        
      <td width="83"><span class="normalfnt">Date <span class="compulsoryRed">*</span></span></td>
      <td width="308" bgcolor="#FFFFFF" class=""><input name="txtDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="validate[required]" id="txtDate" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" onBlur="backDateExisting(this,'Other Invoice');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
      <td width="14">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="normalfnt">Invoice Type</span></td>
      <td><span class="normalfnt">
        <input type="radio" class="invTpe" name="rdInvoType" id="Commercial" value="Commercial" />
Commercial
<input type="radio"  class="invTpe" name="rdInvoType" id="Tax" value="Tax" />
Tax</span> <span class="normalfnt">
<input type="radio"  class="invTpe" name="rdInvoType" id="SVAT" value="SVAT" />
SVAT 
<input type="radio"  class="invTpe" name="rdInvoType" id="NFE" value="NFE" />
NFE-Suspended</span></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="normalfnt">Invoice Address</span></td>
      <td><textarea name="txtAddress" id="txtAddress" cols="30" rows="2"></textarea></td>
      <td><span class="normalfnt">Ship To</span></td>
      <td><textarea name="txtShipTo" id="txtShipTo" cols="30" rows="2"></textarea></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="normalfnt">Currency <span class="compulsoryRed">*</span></span></td>
      <td><select name="cboCurrency" id="cboCurrency" style="width:140px" class="validate[required]">
        <option value=""></option>
        <?php  $sql = "SELECT
						mst_financecurrency.intId,
						mst_financecurrency.strCode,
						mst_financecurrencyactivate.intCompanyId
						FROM
						mst_financecurrency
						Inner Join mst_financecurrencyactivate ON mst_financecurrency.intId = mst_financecurrencyactivate.intCurrencyId
						WHERE
						mst_financecurrency.intStatus =  1 AND
						mst_financecurrencyactivate.intCompanyId = '$companyId'
						order by strCode
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strCode']."</option>";
						}
        				?>
      </select></td>
      <td><span class="normalfnt">Rate <span class="compulsoryRed">*</span></span></td>
      <td align="left"><span class="normalfnt"><span class="normalfntMid">
        <input class="rdoRate" type="radio" name="radio" id="rdoSelling" value="" />
        Selling
        <input class="rdoRate" type="radio" name="radio" id="rdoBuying" value="" />
        Buying 
        <input class="rdoRate" type="radio" name="radio" id="rdoAverage" value="" />
Average
<input type="text" name="txtRate" id="txtRate" style="width:75px; background-color:#9F9; border:thin; text-align:center" readonly="readonly" class="validate[custom[number],required] normalfntBlue"/>
<input type="checkbox" name="chkEdit" id="chkEdit" />
      </span></span></td>
      <td>&nbsp;</td>
      </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="normalfnt">Memo</span></td>
      <td colspan="2"><textarea name="txtRemarks" id="txtRemarks" cols="45" rows="2"></textarea></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
      </table>
      </td>
      </tr>
    <tr>
      <td colspan="2">
      <table width="100%">
      <tr>
      <td>
		<table width="100%" id="tblMainGrid1" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
        <tr class="">
          <td width="130"  height="26" bgcolor="#FAD163" class="normalfntMid"><strong>P.O No.</strong></td>
          <td width="182"  bgcolor="#FAD163" class="normalfntMid"><strong><span class="compulsoryRed">*</span> Terms
          <img id="butTerms" src="../../../../images/add_new.png" width="15" height="15" style="display:none" /></strong></td>
          <td width="250"  bgcolor="#FAD163" class="normalfntMid"><strong> Marketer</strong></td>
          <td width="123" bgcolor="#FAD163" class="normalfntMid"  ><strong>Ship Date</strong></td>
          <td width="209" bgcolor="#FAD163" class="normalfntMid"  ><strong>Ship Via <img id="butShipment" src="../../../../images/add_new.png" width="15" height="15" style="display:none"/></strong></td>
          </tr>
        <tr class="normalfnt">
          <td bgcolor="#FFFFFF" class="normalfntMid">
          <input type="text" name="txtPoNo" id="txtPoNo" style="width:130px" /></td>
          <td bgcolor="#FFFFFF">
          <select name="cboPaymentsTerms" id="cboPaymentsTerms" style="width:182px" class="validate[required]">
            <option value=""></option>
            <?php  $sql = "SELECT
						intId,
						strName
						FROM mst_financepaymentsterms
						WHERE
							intStatus = 0
						order by strName
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strName']." days"."</option>";
						}
        				?>
          </select></td>
          <td bgcolor="#FFFFFF">
          <select name="cboMarketer" id="cboMarketer" style="width:250px">
            <option value=""></option>
            <?php
					$sql = "SELECT
							mst_marketer.intUserId,
							sys_users.strUserName
							FROM
							mst_marketer
							Inner Join sys_users ON mst_marketer.intUserId = sys_users.intUserID
							WHERE
								sys_users.intStatus = 1
								Order By strUserName";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intUserId']."\">".$row['strUserName']."</option>";	
					}
				?>
          </select></td>
          <td width="123" align="center" bgcolor="#FFFFFF" class=""><input name="txtShipDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="txtbox" id="txtShipDate" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
          <td  bgcolor="#FFFFFF">
          <select name="cboShipmentMethod" id="cboShipmentMethod" style="width:206px" >
            <option value=""></option>
            <?php  $sql = "SELECT
						intId,
						strName
						FROM mst_shipmentmethod
						WHERE
							intStatus = 1
						order by strName
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
						}
        				?>
          </select></td>
          </tr>
      </table>
      </td>
      </tr>
      </table>
      </td>
      </tr>
            <tr>
      <td colspan="2" align="right">
 <img src="../../../../images/Tadd.jpg" width="92" height="24" onClick="insertRow();" id="btnAddRow" /></td>
      </tr>
    <tr>
      <td colspan="2"><table width="99%">
        <tr>
          <td>
          <div style="overflow:scroll;width:900px;height:285px;" id="divGrid">
          <table width="100%" id="tblMainGrid2" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
            <tr class="">
              <td width="2%" bgcolor="#FAD163" class="normalfntMid">Del</td>
              <td width="12%"   height="27" bgcolor="#FAD163" class="normalfntMid"><strong><span class="compulsoryRed">*</span> Item</strong> <strong><img id="butItem" src="../../../../images/add_new.png" width="15" height="15" style="display:none"/></strong></td>
              <td width="30%"    bgcolor="#FAD163" class="normalfntMid"><strong>Description</strong></td>
              <td width="9%"    bgcolor="#FAD163" class="normalfntMid"><strong><span class="compulsoryRed">*</span> UOM</strong></td>
              <td width="8%"    bgcolor="#FAD163" class="normalfntMid"><strong>Qty</strong></td>
              <td  bgcolor="#FAD163" class="normalfntMid"  ><strong>Unit Price</strong></td>
              <td style="display:none" width="8%"   bgcolor="#FAD163" class="normalfntMid" ><strong>Discount %</strong></td>
              <td width="12%"   bgcolor="#FAD163" class="normalfntMid"  ><strong>Amount</strong></td>
              <td width="11%"   bgcolor="#FAD163" class="normalfntMid"  ><strong>Tax</strong> <strong>
              <img id="butTaxGroup" src="../../../../images/add_new.png" width="15" height="15" style="display:none"/></strong></td>
              <td width="11%"   bgcolor="#FAD163" class="normalfntMid"  ><strong><span class="compulsoryRed">*</span> Cost Center<img id="butDimension" src="../../../../images/add_new.png" width="15" height="15" style="display:none" /></strong></td>
              <td width="11%" style="display:none"  bgcolor="#FAD163" class="normalfntMid"><strong>Tax With</strong></td>
            </tr>
            <tr class="normalfnt mainRow">
              <td bgcolor="#FFFFFF" class="normalfntMid">
              <img src="../../../../images/del.png" width="15" height="15" class="delImg"/></td>
              <td bgcolor="#FFFFFF" class="normalfntMid">
              <select name="cboItem" id="cboItem" style="width:110px" class="validate[required] item">
                <option value=""></option>
                <?php  $sql = "SELECT
							mst_financecustomeritem.intId,
							mst_financecustomeritem.strName,
							mst_financecustomeritemactivate.intCompanyId
							FROM
							mst_financecustomeritem
							Inner Join mst_financecustomeritemactivate ON mst_financecustomeritem.intId = mst_financecustomeritemactivate.intCustomerItemId
							WHERE
							mst_financecustomeritem.intStatus =  1 AND
							mst_financecustomeritemactivate.intCompanyId = '$companyId'
							order by strName
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
						}
        				?>
              </select></td>
              <td bgcolor="#FFFFFF">
              <table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900" id="tblSubGrid">
              <tr class="normalfnt" style="display:none">
              <td width="55%" bgcolor="#FFFFFF">Style No</td>
              <td width="45%" bgcolor="#FFFFFF">
              <input type="text" name="txtStyleNo" id="txtStyleNo" style="width:100%;text-align:center" class="styleNo" /></td>
              </tr>
              <tr class="normalfnt" style="display:none">
              <td bgcolor="#FFFFFF">Graphic No</td>
              <td width="45%" bgcolor="#FFFFFF">
              <input type="text" name="txtGraphicNo" id="txtGraphicNo" style="width:100%;text-align:center" class="graphicNo"/></td>
              </tr>
              <tr class="normalfnt" style="display:none">
              <td bgcolor="#FFFFFF">Sales Order No / Schedule No</td>
              <td width="45%" bgcolor="#FFFFFF">
              <input type="text" name="txtSalesNo" id="txtSalesNo" style="width:100%;text-align:center" class="orderNo"/></td>
              </tr>
              <tr class="normalfnt" style="display:none">
              <td bgcolor="#FFFFFF">Line Item</td>
              <td width="45%" bgcolor="#FFFFFF">
              <input type="text" name="txtLineItem" id="txtLineItem" style="width:100%;text-align:center" class="lineItem"/></td>
              </tr>
              <tr class="normalfnt" style="display:none">
              <td bgcolor="#FFFFFF">Customer Location</td>
              <td width="45%" bgcolor="#FFFFFF">
              <input type="text" name="txtLocationNo" id="txtLocationNo" style="width:100%;text-align:center" class="cusLocate"/></td>
              </tr>
              <tr class="normalfnt">
                <td bgcolor="#FFFFFF" style="display:none">Item Desc.</td>
                <td bgcolor="#FFFFFF">
                <input type="text" name="txtItemMemo" id="txtItemMemo" style="width:100%;text-align:center" class="itemDesc" /></td>
              </tr>
              </table>
              </td>
              <td bgcolor="#FFFFFF">
              <select name="cboUOM" class="validate[required] uom" id="cboUOM" style="width:100%" >
                <option value=""></option>
                <?php  $sql = "SELECT
								intId,
								strName
								FROM mst_units
									WHERE intStatus=1
										order by strName";
										
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
								}
                   		?>
              </select></td>
              <td bgcolor="#FFFFFF">
              <input type="text" name="txtQty" id="txtQty" style="width:100%;text-align:center" class="validate[custom[number] qty"/>
              </td>
              <td width="11%" align="center" bgcolor="#FFFFFF" class="">
              <input type="text" name="txtUnitPrice" id="txtUnitPrice" style="width:100%;text-align:center" class="validate[custom[number]] unitPrice" />
              </td>
              <td  bgcolor="#FFFFFF" style="display:none">
              <input name="txtDiscount" type="text" class="validate[custom[number]] discount" id="txtDiscount" style="width:75px;text-align:center" />
             </td>
              <td  bgcolor="#FFFFFF">
                <input name="txtAmount" type="text" class="validate[custom[number]] amount normalfntBlue" id="txtAmount" style="width:100%; background-color:#DFDFDF; text-align:right; border:thin" readonly="readonly"/>
              </td>
              <td  bgcolor="#FFFFFF">
              <select name="cboTaxGroup" class="taxGroup" id="cboTaxGroup" style="width:100%;">
                <option value=""></option>
                <?php  $sql = "SELECT
								mst_financetaxgroup.intId,
								mst_financetaxgroup.strCode,
								mst_financetaxgroup.intStatus
								FROM
								mst_financetaxgroup
								WHERE
									intStatus = 1
								order by strCode
								";
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intId']."\">".$row['strCode']."</option>";
								}
                   ?>
              </select></td>
              <td  bgcolor="#FFFFFF">
              <select name="cboDimension" class="validate[required] dimension" id="cboDimension"  style="width:95px;">
                <option value=""></option>
                <?php  $sql = "SELECT
								intId,
								strName
								FROM mst_financedimension
								WHERE
									intStatus = 1
								order by strName
								";
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
								}
                   ?>
              </select></td>
              <td  bgcolor="#FFFFFF" class="taxVal" style="display:none">
              <input type="text" name="txtTaxWith" id="txtTaxWith" style="width:80px; background-color:#F4FFFF; text-align:right" class="validate[custom[number]] taxWith" disabled="disabled"/></td>
            </tr>
          </table>
                </div>
          </td>
        </tr>
      </table></td>
      </tr>
      <tr>
    	<td colspan="2">
    	<table width="100%">
        <tr>
        <td width="9%"><span class="normalfnt">Message</span></td>
        <td width="68%"><textarea name="txtMessage" cols="50" id="txtMessage"></textarea></td>
        <td width="23%">
        <table width="100%">
        <tr>
        <td width="74" align="right"><span class="normalfnt">Sub-Total</span></td>
        <td width="153" align="right"><span class="normalfntMid">
          <input name="txtSubTotal" type="text" disabled="disabled" id="txtSubTotal" style="width:100%;background-color:#F4FFFF; text-align:right" value="0.0000" />
        </span></td>
        </tr>
        <tr>
          <td align="right"><span class="normalfnt">Tax</span></td>
          <td align="right"><span class="normalfntMid">
            <input name="txtTotalTax" type="text" disabled="disabled" id="txtTotalTax" style="width:100%;background-color:#F4FFFF; text-align:right" value="0.0000" />
          </span></td>
        </tr>
        <tr>
          <td align="right"><span class="normalfnt">Total</span></td>
          <td align="right"><span class="normalfntMid">
            <input name="txtTotal" class="normalfntBlue" type="text" id="txtTotal" style="width:100%;background-color:#F4FFFF; text-align:right; border:solid" value="0.0000" readonly="readonly" />
          </span></td>
        </tr>
        </table>
        </td>
        </tr>
        </table>
    	</td>
      </tr>
    <tr>
      <td colspan="2">
        <table width="100%">
          <tr>
            <td width="100%" height="34" class="tableBorder_allRound"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
<td width="100%" align="center" bgcolor=""><img style="display:none" border="0" src="../../../../images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:none" border="0" src="../../../../images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/><img style="display:none" border="0" src="../../../../images/Tprint.jpg" alt="Print" name="butPrint" width="92" height="24" class="mouseover" id="butPrint" tabindex="25"/><img style="display:none" border="0" src="../../../../images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="25"/><a href="../../../../main.php"><img  src="../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                </tr>
              </table></td>
            </tr>
          </table>
        </td>
    </tr>
    </table>
</div>
</div>
</form>
</body>
</html>
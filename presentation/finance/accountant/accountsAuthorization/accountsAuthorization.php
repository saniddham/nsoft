<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$companyId = $_SESSION['headCompanyId'];
$location = $_SESSION['CompanyID'];
$thisFilePath = $_SERVER['PHP_SELF'];
include "{$backwardseperator}dataAccess/permisionCheck.inc";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Accounts Authorization (Ledger Posting)</title>
        <link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/tblstyle.css" rel="stylesheet" type="text/css" />

        <script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
        <script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
        <script src="accountsAuthorization.js" type="text/javascript"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

        <link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
        <script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

        <link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css"/>
        <link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css"/> 


    </head>

    <body>
        <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
                <td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator . 'Header.php'; ?></td>
            </tr> 
        </table>
        <script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
        <script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
        <script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>
        <div align="center">
            <div class="trans_layoutD">
                <div class="trans_text">Authorized Book Entrees</div>
                <form name="fmtAuhoBookEntries" id="fmtAuhoBookEntries" autocomplete="off" action="" method="post">
                    <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="">
                        <tr>
                            <td width="18%" bgcolor="#FFFFFF" class="normalfntMid">&nbsp;</td>
                            <td width="28%" bgcolor="#FFFFFF" class="normalfntMid">&nbsp;</td>
                            <td width="54%" bgcolor="#FFFFFF" class="normalfntb"></td>
                        </tr>
                        <tr>
                            <td bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
                            <td bgcolor="#FFFFFF" class="normalfnt">Book Entries Types</td>
                            <td bgcolor="#FFFFFF">
                                <select name="cmbEntryType" id="cmbEntryType" style="width:115px" class="validate[required]">
                                    <option value=""></option>
                                    <?php
                                    $sql2 = "SELECT
                                                DISTINCT (fin_transactions.strProgramType) as transType
                                                FROM
                                                fin_transactions
                                                ORDER BY
                                                fin_transactions.strProgramType ASC";
                                    $result2 = $db->RunQuery($sql2);
                                    while ($row2 = mysqli_fetch_array($result2)) {
                                        echo "<option value=\"" . $row2['transType'] . "\">" . $row2['transType'] . "</option>";
                                    }
                                    ?>
                                </select>
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="#FFFFFF" class="normalfnt" colspan="3">
                                <div id="divGrid">
                                    <table width="100%">                                                     
                                        <tbody id="allentrys" class="normalfnt" bgcolor="#FFFFFF">
                                        </tbody>
                                    </table>
                                </div>
                            </td>

                        </tr>
                    </table>
                    <table width="100%">
                        <tr>
                            <td width="100%" height="34"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                                    <tr>
                                        <td width="100%" align="center" bgcolor=""><img style="display:none" border="0" src="../../../../images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew"/><img  style="display:none" border="0" src="../../../../images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave"/><img  src="../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose"/></td>
                                    </tr>
                                    <tr>
                                        <td align="center" bgcolor="">&nbsp;</td>
                                    </tr>
                                </table></td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>  
    </body>
</html>
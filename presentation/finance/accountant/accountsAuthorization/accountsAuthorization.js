//permision for add 
if(intAddx)
{
    $('#frmPurchaseDayBook #butNew').show();
    $('#frmPurchaseDayBook #butSave').show();
}
//permision for edit 
if(intEditx)
{
    $('#frmPurchaseDayBook #butSave').show();
    $('#frmPurchaseDayBook #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
}
//permision for delete
if(intDeletex)
{
    $('#frmPurchaseDayBook #butDelete').show();
    $('#frmPurchaseDayBook #cboSearch').removeAttr('disabled');
}
//permision for view
if(intViewx)
{
    $('#frmPurchaseDayBook #cboSearch').removeAttr('disabled');
}
function alertx()
{
    $('#frmPurchaseDayBook #butSave').validationEngine('hide') ;
}

$(document).ready(function(){
    $("#fmtAuhoBookEntries").validationEngine();
    
    //permision for add 
    if(intAddx){
        $('#fmtAuhoBookEntries #butSave').show();
    }
    //get Grn from PO-------------------------------
    $('#fmtAuhoBookEntries #cmbEntryType').change(function(){
        if($('#fmtAuhoBookEntries #cmbEntryType').val()!=""){
            var entryType=$('#fmtAuhoBookEntries #cmbEntryType').val();
            
            var url ='accountsAuthorization-get-db.php?requestType=loadEntrys&entryType='+entryType;            
            var obj=$.ajax({
                url:url,
                dataType:'json',
                success:function(json){                    
                    $('#fmtAuhoBookEntries #allentrys').html(json.entryList);                
                },
                async:false
            });  
               
        }
    });
    //save the selected entris as authorized
    $('#fmtAuhoBookEntries #butSave').click(function(){
        if ($('#fmtAuhoBookEntries').validationEngine('validate')) {
            var entryType=$('#fmtAuhoBookEntries #cmbEntryType').val();
            var entries=document.getElementsByName("chkEnt");
            var params='&entryType='+entryType;
            var x=0;
            for(var i=0; i< entries.length;++i){
                if(entries[i].checked){                    
                    params+="&entries["+x+"][entId]="+entries[i].value;
                    ++x;                    
                }                
            }
            
            var url = "accountsAuthorization-set-db.php";
            var obj = $.ajax({
                url:url,
                dataType: "json",  
                data:$("#fmtAuhoBookEntries").serialize()+'&requestType=add'+params,
                async:false,
			
                success:function(json){
                    $('#fmtAuhoBookEntries #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
                    if(json.type=='pass')
                    {
                        $('#fmtAuhoBookEntries').get(0).reset();						
                        var t=setTimeout("alertx()",1000);                        
                        $('#fmtAuhoBookEntries #allentrys').html('');
                        return;
                    }
                    var t=setTimeout("alertx()",3000);
                },
                error:function(xhr,status){					
                    $('#fmtAuhoBookEntries #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
                    var t=setTimeout("alertx()",3000);
                }		
            }); 
        }
        
    });
    
});




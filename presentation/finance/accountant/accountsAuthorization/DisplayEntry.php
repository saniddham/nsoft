<?php
session_start();
$backwardseperator = "../../../../";
$mainPath 	= $_SESSION['mainPath'];
$userId 	= $_SESSION['userId'];
$companyId = $_SESSION['headCompanyId'];
$location = $_SESSION['CompanyID'];
include "{$backwardseperator}dataAccess/Connector.php";

$entryNo=$_REQUEST['entryNo'];
$entryType=$_REQUEST['entType'];

$sql="SELECT
        fin_transactions.dtDate,
        fin_transactions.`strCredit/Debit`,
        fin_transactions.strDocumentNo,
        fin_transactions.dblAmount,
        fin_transactions.intCurrencyRate,
        fin_transactions.srtTransDetails,
        fin_transactions.strChequeNo,
        mst_financechartofaccounts.strCode AS accCode,
        mst_financechartofaccounts.strName AS accName,
        mst_financecurrency.strCode AS currency,
        mst_financedimension.strCode AS dimension
        FROM
        fin_transactions
        INNER JOIN mst_locations ON fin_transactions.intLocationId = mst_locations.intId
        INNER JOIN mst_financechartofaccounts ON fin_transactions.intAccountId = mst_financechartofaccounts.intId
        INNER JOIN mst_financecurrency ON fin_transactions.intCurrencyId = mst_financecurrency.intId
        LEFT OUTER JOIN mst_financedimension ON fin_transactions.intDimensionId = mst_financedimension.intId
        WHERE
        mst_locations.intCompanyId = $companyId AND
        fin_transactions.strProgramType = '$entryType' AND
        fin_transactions.strDocumentNo = '$entryNo'";
$result = $db->RunQuery($sql);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Book Entry - <?php echo $entryNo."--".$entryType ?></title>
        <link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/tblstyle.css" rel="stylesheet" type="text/css" />
        <style>
            table.rptTable{
                width: 100%;
                border-collapse:collapse;
            }
            table.rptTable tr td{
                border:1px solid black;
                font-size: 11px;
            }
            .rptTblHeader{
                font-size: 12px;
                text-align: center;
                font-weight:bold;
            }
            .rptTblBodyRight  {
                font-size: 11px;
                text-align: right;
            }
            .rptTblBodyMid {
                font-size: 11px;
            }
            .rptTblBodyFoot {
                font-size: 11px;
                font-weight:bold;
                text-align: right;
            }
            .drillLink{
                cursor:pointer;
            }
            a:hover
            { 
                font-weight: bold;
                text-decoration:underline;
                color: #0000FF;
            }
            table.rptTable2{
                width:90%;
                border-collapse:collapse;
                font-size: 12px;
            }
            table.rptTable2 tr td{
                border-bottom: 1px solid black;                
                font-size: 11px;
            }
            .rptTblBodyRight2  {
                font-size: 12px;
                text-align: right;
                background-color:#c3c3c3;
                 width: 12%;
            }
            .rptTblBody {
                font-size: 12px;
                text-align: left;
                background-color:#d6dce7;
                 width: 12%;
                
            }
            .rptTblBodyMid {
                font-size: 12px;
                text-align: center;
                /*background-color:thistle;*/
                width: 1%;
            }
            
        </style>
    </head>
    <body>
        <div align="center">   
            <div class="trans_layoutL">
                <div class="trans_text">Display A Book Entry</div>
            <table  class="rptTable">
                <thead>
                <tr class="rptTblHeader">
                    <td>Account code</td>
                    <td>Account name</td>
                    <td>Cost Center</td>
                    <td>Trans. Details</td>
                    <td>Debit</td>
                    <td>Credit</td>
                </tr>
                </thead>
                <tbody>
                <?php
                    $debitTotal=0; $creditTotal=0;
                    while($row=mysqli_fetch_array($result)){
                        $debitAmount=0;
                        $crditAmount=0;
                        if($row['strCredit/Debit']=='C') $crditAmount=$row['dblAmount'];
                        else if($row['strCredit/Debit']=='D') $debitAmount=$row['dblAmount'];
                        $debitTotal+=$debitAmount; $creditTotal+=$crditAmount;
                        
                        //comman details
                        $currency=$row['currency'];
                        $currencyRate=$row['intCurrencyRate'];
                        $chequeNo=$row['strChequeNo'];
                ?>
                        <tr>
                            <td><?php echo $row['accCode'] ?></td>
                            <td><?php echo $row['accName'] ?></td>
                            <td><?php echo $row['dimension']==null?"N/A":$row['dimension'] ?></td>
                            <td><?php echo  $row['srtTransDetails'] ?></td>
                            <td class="rptTblBodyRight"><?php echo $debitAmount ?></td>
                            <td class="rptTblBodyRight"><?php echo $crditAmount ?></td>
                        </tr>
                <?php
                    }
                ?>
                <tr class="rptTblBodyFoot">
                    <td colspan="4">Total</td>
                    <td><?php echo $debitTotal ?></td>
                    <td><?php echo $creditTotal ?></td>
                </tr>
                    </tbody>
            </table>
                <br/>
            <table class="rptTable2">
                <tr>
                    <td class="rptTblBodyRight2">Entry Number</td>
                    <td class="rptTblBodyMid">:</td>
                    <td class="rptTblBody"><?php echo $entryNo ?></td>
                    <td class="rptTblBodyRight2">Entry Type</td>
                    <td class="rptTblBodyMid">:</td>
                    <td class="rptTblBody"><?php echo $entryType ?></td>
                    <td class="rptTblBodyRight2">Currency</td>
                    <td class="rptTblBodyMid">:</td>
                    <td class="rptTblBody"><?php echo $currency ?></td>
                    <td class="rptTblBodyRight2">Rate</td>
                    <td class="rptTblBodyMid">:</td>
                    <td class="rptTblBody"><?php echo $currencyRate ?></td>
                </tr>               
                <tr>
                    <td class="rptTblBodyRight2">Cheque No</td>
                    <td class="rptTblBodyMid">:</td>
                    <td class="rptTblBody"><?php echo $chequeNo ?></td>
                    <td class="rptTblBodyRight2">&nbsp;</td>
                    <td class="rptTblBodyMid"> &nbsp;</td>
                    <td class="rptTblBody">&nbsp;</td>
                    <td class="rptTblBodyRight2">&nbsp;</td>
                    <td class="rptTblBodyMid">&nbsp;</td>
                    <td class="rptTblBody">&nbsp;</td>
                    <td class="rptTblBodyRight2">&nbsp;</td>
                    <td class="rptTblBodyMid">&nbsp;</td>
                    <td class="rptTblBody">&nbsp;</td>
                </tr>
            </table>
            </div>
        </div>
    </body>
</html>

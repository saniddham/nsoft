<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];

$thisFilePath =  $_SERVER['PHP_SELF'];
include  "{$backwardseperator}dataAccess/permisionCheck.inc";
$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$companyId = $row['intCompanyId']; 
}
$receiptRefNo = $_REQUEST['id'];
$strReferenceNo = $_REQUEST['strReferenceNo'];
// ======================Check Exchange Rate Updates========================
if($invoiceRefNo == "")
{
	$status = "Adding";
}
else
{
	$status = "Changing";
}
$currentDate = date("Y-m-d");

$sql = "SELECT COUNT(*) AS 'no'
		FROM
		mst_financeexchangerate
		WHERE
		mst_financeexchangerate.dtmDate =  '$currentDate'
		";
$result = $db->RunQuery($sql);
$row= mysqli_fetch_array($result);
if($row['no']==0)
	{
		$str =  "Please Update Exchange Rates Before ".$status." Journal Entry .";
		$str .= $row['NameList'];
		$maskClass="maskShow";
	}
	else
	{
		$maskClass="maskHide";
	}
// =========================================================================
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Journal Entry</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/tblstyle.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script src="journalEntry-js.js" type="text/javascript"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>
<script src="../../commanFunctions/numberExisting-js.js" type="text/javascript"></script>

<link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

<link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css">

<style type="text/css">
.setProperty 
{
  width:80px;
  text-align:righ; 
  background-color:#FFF;
  border:none;
}
</style>

</head>

    <body onload="functionList('<?php echo $strReferenceNo ?>')">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<div id="divMask" class="<?php echo $maskClass?> mask"> <?php echo $str; ?></div>

<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<form id="frmJournalEntry" name="frmJournalEntry" method="post" action="journalEntry-db-set.php" autocomplete="off">
<div align="center">
  <div class="trans_layoutL">
    <div class="trans_text">Journal Entry</div>
    <table width="100%">
    <tr><td width="100%"><table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="">
      <tr class="even">
        <td width="7%" bgcolor="#FFFFFF"><img src="../../../../images/fb.png" width="18" height="19" /></td>
        <td width="7%" bgcolor="#FFFFFF">&nbsp;</td>
        <td width="15%" bgcolor="#FFFFFF">&nbsp;</td>
        <td width="13%" bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
        <td width="29%" bgcolor="#FFFFFF">&nbsp;</td>
        <td width="29%" align="right" bgcolor="#FFFFFF"><img src="../../../../images/ff.png" width="18" height="19" /></td>
      </tr>
      <tr>
      <td class="normalfntRight" colspan="4"><span class="normalfnt">Entry</span> No:</td>
       <td align="left"  colspan="4"><span class="normalfntMid">
         <select name="cboSearch" id="cboSearch"  style="width:240px" >
           <option value=""></option>
           <?php   $sql = "SELECT
							fin_accountant_journal_entry_header.strReferenceNo,
							IFNULL(CONCAT(' - ',fin_accountant_journal_entry_header.strFnRefNo),'') AS refNo
							FROM
							fin_accountant_journal_entry_header
							WHERE
							fin_accountant_journal_entry_header.intCompanyId =  '$companyId' AND
							fin_accountant_journal_entry_header.intDeleteStatus = '0'
							ORDER BY intEntryNo DESC
							";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['strReferenceNo']."\">".$row['strReferenceNo'].$row['refNo']."</option>";
						}
          ?>
         </select>
       </span></td>
      </tr>
      <tr>
        <td class="normalfntRight" colspan="4">&nbsp;</td>
        <td align="left"  colspan="4">&nbsp;</td>
      </tr>
    </table></td></tr>
    <tr>
      <td><table width="100%" border="0" align="center" bgcolor="#FFFFFF" class="tableBorder_allRound">
        <tr>
          <td width="2%" class="normalfnt">&nbsp;</td>
          <td width="16%" class="normalfnt">Entry No</td>
          <td class="normalfnt"><input name="txtNo" type="text" readonly="readonly"class="normalfntRight" id="txtNo" style="width:230px; background-color:#F4FFFF;text-align:center; border:dotted; border-color:#F00" onBlur="numberExisting(this,'Journal Entry');" />
            <input checked="checked" type="checkbox" name="chkAutoManual" id="chkAutoManual" style="display:none"/>
            <input name="amStatus" type="text" class="normalfntBlue" id="amStatus" style="width:40px; background-color:#FFF; text-align:center; border:thin" disabled="disabled" value="(Auto)"/></td>
          <td class="normalfnt">&nbsp;</td>
          <td class="normalfnt">&nbsp;</td>
          <td width="24%" class="normalfnt">&nbsp;</td>
        </tr>
        <tr>
          <td class="normalfnt">&nbsp;</td>
          <td class="normalfnt">Reference Number</td>
          <td class="normalfnt"><input type="text" name="txtFnRefNo" id="txtFnRefNo" style="border-bottom-color:#00F; width:230px; text-align:center; border:double"/></td>
          <td class="normalfnt"><span class="normalfntMid">Date <span class="compulsoryRed">*</span></span></td>
          <td width="15%" class="normalfnt"><input name="txtDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="validate[required]" id="txtDate" style="width:98px;" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" onblur="backDateExisting(this,'Journal Entry');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
          <td class="normalfnt">&nbsp;</td>
        </tr>
        <tr>
          <td class="normalfnt">&nbsp;</td>
          <td class="normalfnt">Currency <span class="compulsoryRed">*</span></td>
          <td width="33%" class="normalfnt"><select name="cboCurrency" id="cboCurrency" style="width:90px" class="validate[required]">
            <option value=""></option>
            <?php  $sql = "SELECT
						mst_financecurrency.intId,
						mst_financecurrency.strCode,
						mst_financecurrencyactivate.intCompanyId
						FROM
						mst_financecurrency
						Inner Join mst_financecurrencyactivate ON mst_financecurrency.intId = mst_financecurrencyactivate.intCurrencyId
						WHERE
						mst_financecurrency.intStatus =  1 AND
						mst_financecurrencyactivate.intCompanyId = '$companyId'
						order by strCode
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strCode']."</option>";
						}
        				?>
          </select></td>
          <td width="10%" class="normalfnt"><span class="normalfntRight">Rate</span></td>
          <td colspan="2" class="normalfnt"><span class="normalfntMid">
            <input class="rdoRate" type="radio" name="radio" id="rdoSelling" value="" />
            Selling
            <input class="rdoRate" type="radio" name="radio" id="rdoBuying" value="" />
            Buying
            <input class="rdoRate" type="radio" name="radio" id="rdoAverage" value="" />
            Average
            <input type="text" name="txtRate" id="txtRate" style="width:75px; background-color:#9F9; border:thin; text-align:center" readonly="readonly" class="validate[custom[number],required] normalfntBlue"/>
            <input type="checkbox" name="chkEdit" id="chkEdit" />
          </span></td>
          </tr>
      </table></td>
    </tr>
    <tr>
      <td align="right"><img src="../../../../images/Tadd.jpg" width="92" height="24" onclick="insertRow();" /></td>
    </tr>
    <tr>
      <td>
      <div style="overflow:scroll;width:900px;height:250px;" id="divGrid">
      <table width="100%" id="tblMainGrid1" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
        <tr class="">
          <td width="19" bgcolor="#FAD163" class="normalfntMid">Del</td>
          <td width="178"  height="24" bgcolor="#FAD163" class="normalfntMid"><strong><span class="compulsoryRed">*</span> Account</strong></td>
          <td width="90"  bgcolor="#FAD163" class="normalfntMid"><strong>Debit</strong></td>
          <td width="90"  bgcolor="#FAD163" class="normalfntMid"><strong>Credit</strong></td>
          <td width="230" bgcolor="#FAD163" class="normalfntMid"  ><strong>Memo</strong></td>
          <td width="153" bgcolor="#FAD163" class="normalfntMid"  ><strong><span class="compulsoryRed">*</span> Name</strong></td>
          <td width="105" bgcolor="#FAD163" class="normalfntMid"  ><strong><span class="compulsoryRed">*</span> Cost Center</strong></td>
        </tr>
        <tr class="normalfnt mainRow">
          <td bgcolor="#FFFFFF" class="normalfntMid"><img src="../../../../images/del.png" width="15" height="15" class="delImg"/></td>
          <td bgcolor="#FFFFFF" class="normalfntMid">
          <select name="cboAccounts" id="cboAccounts" style="width:100%" class="validate[required]  accounts">
            <option value="">&nbsp;</option>
            <?php
				$sql = "SELECT
						mst_financechartofaccounts.intId,
						mst_financechartofaccounts.strCode,
						mst_financechartofaccounts.strName
						FROM mst_financechartofaccounts 
						Inner Join mst_financechartofaccounts_companies ON mst_financechartofaccounts.intId = mst_financechartofaccounts_companies.intChartOfAccountId
						WHERE
						mst_financechartofaccounts_companies.intCompanyId =  '$companyId' 
						AND
						mst_financechartofaccounts.intStatus =  '1' AND strType = 'Posting'
						ORDER BY strCode
						";
					$result = $db->RunQuery($sql);
					
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$account)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strCode']."-".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strCode']."-".$row['strName']."</option>";	
					}
			?>
          </select></td>
          <td bgcolor="#FFFFFF">
          <input name="txtDebit" type="text" disabled="disabled" class="validate[custom[number]] debit" id="txtDebit" style="width:100%; text-align:right" /></td>
          <td bgcolor="#FFFFFF">
          <input name="txtCredit" type="text" disabled="disabled" class="validate[custom[number]] credit" id="txtCredit" style="width:100%; text-align:right" /></td>
          <td  bgcolor="#FFFFFF">
          <input type="text" name="txtMemo" id="txtMemo" class="memo" style="width:100%" /></td>
          <td  bgcolor="#FFFFFF">
            <select name="cboCustomer" id="cboCustomer" class="validate[required] customer" style="width:100%"><?php echo $combo; ?></select></td>
          <td  bgcolor="#FFFFFF">
          	<select name="cboDimension" class="validate[required] dimension" id="cboDimension"  style="width:100%;">
            <option value=""></option>
            <?php  $sql = "SELECT
							intId,
							strName
							FROM mst_financedimension
							WHERE
								intStatus = 1
							order by strName
							";
							$result = $db->RunQuery($sql);
							while($row=mysqli_fetch_array($result))
							{
								echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
							}
                   ?>
          </select></td>
        </tr>
      </table>
      </div>
      </td>
    </tr>
    <tr>
      <td>
      <table width="44%">
      <tr>
        <td width="180" align="right" class="normalfntMid">Total </td>
        <td width="90">
        <input name="txtTotDebit" type="text" id="txtTotDebit" style="width:100%;background-color:#F4E8EE; border:thin; border-style:dashed; border-color:#30F; text-align:right" readonly="readonly" class="normalfntMid"/></td>
        <td width="90" class="normalfnt">
        <input name="txtTotCredit" type="text" id="txtTotCredit" style="width:100%;background-color:#FCDEDA; border:thin; border-style:dashed; border-color:#30F; text-align:right" readonly="readonly" class="normalfntMid"/></td>
        <td width="90" class="normalfnt">
        <input name="txtTotDiff" type="text" id="txtTotDiff" style="width:100%;background-color:#9F9; border:thin; border-style:dotted; border-color:#F00; text-align:right" readonly="readonly" class="normalfntMid"/></td>
      </tr>
      </table>
      </td>
    </tr>
    <tr>
      <td>
       <table width="100%">
      <tr>
            <td width="100%" height="34"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                <tr>
<td width="100%" align="center" class="tableBorder_allRound" bgcolor=""><img style="display:none" border="0" src="../../../../images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:none" border="0" src="../../../../images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/><img style="display:none" border="0" src="../../../../images/Tprint.jpg" alt="Print" name="butPrint" width="92" height="24" class="mouseover" id="butPrint" tabindex="25"/><img style="display:none" border="0" src="../../../../images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="25"/><a href="../../../../main.php"><img  src="../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                </tr>
              <tr>
                <td align="center" bgcolor="">&nbsp;</td>
              </tr>
              </table></td>
      </tr>
    </table>
      </td>
    </tr>
    </table>
  </div>
</div>
</form>
</body>
</html>
// JavaScript Document
var toBePaidAmount = '';
var amStatus = "Auto";
function functionList(recRefNo)
{
	if(recRefNo!='')
	{
		$('#frmJournalEntry #cboSearch').val(recRefNo);
		$('#frmJournalEntry #cboSearch').change();
	}
}
var rows = 1;
function insertRow()
{
	var tbl = document.getElementById('tblMainGrid1');	
	rows = tbl.rows.length;
	tbl.insertRow(rows);
	tbl.rows[rows].innerHTML = tbl.rows[rows-1].innerHTML;
//	loadJs();
}
$(document).ready(function() {
	
	var id = '';
	$("#frmJournalEntry").validationEngine();
	$('#frmJournalEntry #cboCurrency').focus();

$('.delImg').live('click',function(){
	var rowId = $(this).parent().parent().parent().find('tr').length;
	if(rowId!=2)
	$(this).parent().parent().remove();
	calculateDebit();
	calculateCredit();
});

$('.delImg').css('cursor', 'pointer');

  //permision for add 
  if(intAddx)
  {
 	$('#frmJournalEntry #butNew').show();
	$('#frmJournalEntry #butSave').show();
	$('#frmJournalEntry #butPrint').show();
  }
  //permision for edit 
  if(intEditx)
  {
  	$('#frmJournalEntry #butSave').show();
	$('#frmJournalEntry #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
	$('#frmJournalEntry #butPrint').show();
  }
  //permision for delete
  if(intDeletex)
  {
  	$('#frmJournalEntry #butDelete').show();
	$('#frmJournalEntry #cboSearch').removeAttr('disabled');
  }
  //permision for view
  if(intViewx)
  {
	$('#frmJournalEntry #cboSearch').removeAttr('disabled');
  }
  
  $('#frmJournalEntry #chkEdit').click(function(){
	  if($('#frmJournalEntry #chkEdit').attr('checked'))
	  {
		  $("#frmJournalEntry #txtRate").attr("readonly","");
		  $('#frmJournalEntry #txtRate').focus();
	  }
	  else
	  {
		  $('#frmJournalEntry #txtRate').val('');
		  $("#frmJournalEntry #txtRate").attr("readonly","readonly");
		  $('#frmJournalEntry #cboCurrency').change();
	  }
  });
///////////////////////////get customer/supplier////////////////////
$('#cboAccounts').live('change',function(){
	var row=this.parentNode.parentNode.rowIndex;
	var url = "journalEntry-db-get.php?requestType=getName&accId="+$(this).val();
	var obj = $.ajax({url:url,async:false});
	$(this).parent().parent().find('.customer').html(obj.responseText);
	$(this).parent().parent().find('.debit').attr("disabled","");
	$(this).parent().parent().find('.credit').attr("disabled","");
	if(obj.responseText == '<option value=""></option>')
	{
		 $(this).parent().parent().find('#cboCustomer').removeAttr('class')
		 $(this).parent().parent().find('#cboCustomer').attr('class','customer');
	}
	else
	{
		 $(this).parent().parent().find('#cboCustomer').attr('class','validate[required] customer');
	}
});
////////////////////////////////////////////////////////////////////

$('.debit').live('keyup',function(){
	if($(this).parent().parent().find('.debit').val() != "")
	{
		$(this).parent().parent().find('.credit').attr("disabled","disabled");
	}
	else
	{
		$(this).parent().parent().find('.credit').attr("disabled","");
	}
});

$('.credit').live('keyup',function(){
	if($(this).parent().parent().find('.credit').val() != "")
	{
		$(this).parent().parent().find('.debit').attr("disabled","disabled");
	}
	else
	{
		$(this).parent().parent().find('.debit').attr("disabled","");
	}
});

//------------------------------------------------------------
  $("input[name^=txtDebit]").live("keyup ", calculateDebit);
  $("input[name^=txtCredit]").live("keyup ", calculateCredit);
//----------------------------------------------------------

 //===================================================================
 	$('#frmJournalEntry #chkAutoManual').click(function(){
	  if($('#frmJournalEntry #chkAutoManual').attr('checked'))
	  {
		  amStatus = "Auto";
		  $('#frmJournalEntry #amStatus').val('Auto');
		  $('#frmJournalEntry #txtNo').val('');
		  $("#frmJournalEntry #txtNo").attr("readonly","readonly");
		  $('#frmJournalEntry #txtNo').removeClass('validate[required]');
	  }
	  else
	  {
		  amStatus = "Manual";
		  $('#frmJournalEntry #amStatus').val('Manual');
		  $('#frmJournalEntry #txtNo').val('');
		  $("#frmJournalEntry #txtNo").attr("readonly","");
		  $('#frmJournalEntry #txtNo').focus();
		  $('#frmJournalEntry #txtNo').addClass('validate[required]');
	  }
  });
 //===================================================================

///////////////////////////get G/L amount//////////////////////////
//$('.glAccount').live('change',function(){
//	$('#txtAccAmount').val($('#txtRecAmount').val());
//});
//////////////////////////////////////////////////////////////////

////////////////////////get exchange rate//////////////////////////
$('#cboCurrency').change(function(){
	var url = "journalEntry-db-get.php?requestType=getExchangeRate&currencyId="+$(this).val()+'&exchangeDate='+$('#txtDate').val();
	var obj = $.ajax({url:url,dataType:'json',success:function(json){
		
		$('#rdoBuying').val(json.buyingRate);
		$('#rdoSelling').val(json.sellingRate);
		$('#rdoAverage').val(((eval(json.buyingRate) + eval(json.sellingRate))/2).toFixed(4));
		$('#rdoSelling').click();
		},async:false});
});
///////////////////////////////////////////////////////////////////
$('.rdoRate').click(function(){
  $('#txtRate').val($(this).val());
});
//save button click event
$('#frmJournalEntry #butSave').click(function(){
if(existingMsgDate == "")
{
//-------------------------------------------------------------------
	var accId = "";
	var debitAmount = "";
	var creditAmount = "";
	var memo = "";
	var dimension = "";
			
 value="[ ";
	$('#tblMainGrid1 tr:not(:first)').each(function(){
		
		accId		= $(this).find(".accounts").val();
		debitAmount = $(this).find(".debit").val();
		creditAmount = $(this).find(".credit").val();
		memo 		= $(this).find(".memo").val();
		name 		= $(this).find(".customer").val();
		dimension 	= $(this).find(".dimension").val();
		
		value += '{ "accId":"'+accId+'", "debitAmount": "'+debitAmount+'", "creditAmount": "'+creditAmount+'", "memo": "'+URLEncode(memo)+'", "name": "'+name+'", "dimension": "'+dimension+'"},';

	});
	
	value = value.substr(0,value.length-1);
	value += " ]";
//---------------------------------------------------------------------------
	var requestType = '';
	if (($('#frmJournalEntry').validationEngine('validate')) && (getAvailability == "" || amStatus == "Auto"))
    {
		//showWaiting();
		if(value != '[ ]')
		{
			if((eval($('#txtTotDebit').val()) - eval($('#txtTotCredit').val()))==0)
			{
				if(($('#txtNo').val()=='' && amStatus == "Auto") || ($('#txtNo').val()!='' && amStatus == "Manual"))
					requestType = 'add';
				else
					requestType = 'edit';
				
				var url = "journalEntry-db-set.php";
				var obj = $.ajax({
					url:url,
					dataType: "json",
					type:'post',
					data:$("#frmJournalEntry").serialize()+'&requestType='+requestType+'&cboSearch='+id+'&detail='+value+'&amStatus='+amStatus,
					async:false,
					
					success:function(json){
							$('#frmJournalEntry #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
							if(json.type=='pass')
							{
								var t=setTimeout("alertx()",1000);
								$('#txtNo').val(json.entryNo);
								amStatus = "Auto";
								loadCombo_frmJournalEntry();
								return;
							}
							var t=setTimeout("alertx()",3000);
						},
					error:function(xhr,status){
							$('#frmJournalEntry #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
							var t=setTimeout("alertx()",3000);
						}		
					});
			}
			else
			{
				$('#frmJournalEntry #butSave').validationEngine('showPrompt', 'You cannot allow this process untill debit  amount and credit amount are same','fail');
				var t=setTimeout("alertx()",5000);
			}
		}
		else
		{
			$('#frmJournalEntry #butSave').validationEngine('showPrompt', 'You cannot allow this process!','fail');
			var t=setTimeout("alertx()",5000);
		}
	}
}
else
{
	$('#frmJournalEntry #butSave').validationEngine('showPrompt', existingMsgDate,'fail');
	var t=setTimeout("alertx()",5000);
}
});
/////////////////////////////////////////////////////
//// load invoice details //////////////////////////
/////////////////////////////////////////////////////
$('#frmJournalEntry #cboSearch').click(function(){
   $('#frmJournalEntry').validationEngine('hide');
});
$('#frmJournalEntry #cboSearch').change(function(){
//---------------------------------------------------
	existingMsgDate = "";
	amStatus = "Auto";
	//document.getElementById("chkAutoManual").style.display='none';
	document.getElementById("amStatus").style.display='none';
	$("#frmJournalEntry #txtNo").attr("readonly","readonly");
//---------------------------------------------------
$('#frmJournalEntry').validationEngine('hide');
var url = "journalEntry-db-get.php";
if($('#frmJournalEntry #cboSearch').val()=='')
{
		//---------------------------------------------------
			amStatus = "Auto";
			//document.getElementById("chkAutoManual").style.display='';
			document.getElementById("amStatus").style.display='';
			$('#frmJournalEntry #txtNo').removeClass('validate[required]');
			$("#frmJournalEntry #txtNo").attr("readonly","readonly");
		//---------------------------------------------------
		$('#tblMainGrid1 >tbody >tr').each(function(){
			if($(this).index()!=0 && $(this).index()!=1 )
			{
				$(this).remove();
			}
		});
	$('#frmJournalEntry').get(0).reset();return;	
}
$('#txtNo').val($(this).val());
var httpobj = $.ajax({
	url:url,
	dataType:'json',
	type:'post',
	data:'requestType=loadDetails&id='+URLEncode($(this).val()),
	async:false,
	success:function(json)
	{
		$('#frmJournalEntry #cboCurrency').val(json.currency);
		$('#frmJournalEntry #txtRate').val(json.rate);
		$('#frmJournalEntry #txtDate').val(json.date);
		$('#frmJournalEntry #txtFnRefNo').val(json.fnRefNo);
		
		//--------------------------------------------------
		$('#tblMainGrid1 >tbody >tr').each(function(){
			if($(this).index()!=0 && $(this).index()!=1 )
			{
				$(this).remove();
			}
		});

		var accId 			= "";
		var debitAmount 	= "";
		var creditAmount 	= "";
		var memo 			= "";
		var date 			= "";
		var dimension 		= "";

		if(json.detailVal!=null)
		{
			var rowId = $('#tblMainGrid1').find('tr').length;
			var tbl = document.getElementById('tblMainGrid1');
			rows = $('#tblMainGrid1').find('.mainRow').length + 1;
			for(var j=0;j<=json.detailVal.length-1;j++)
			{
				accId			= json.detailVal[j].accId;
				debitAmount		= json.detailVal[j].debitAmount;
				creditAmount	= json.detailVal[j].creditAmount;
				memo			= json.detailVal[j].memo;
				name			= json.detailVal[j].name;
				dimension		= json.detailVal[j].dimension;
				if(j==0)
				{
					tbl.rows[1].cells[5].childNodes[1].value = "";
					tbl.rows[1].cells[1].childNodes[1].value = accId;
					if(name != 0)
					{
						var url = "journalEntry-db-get.php?requestType=getName&accId="+accId;
						var obj = $.ajax({url:url,async:false});
						$('#tblMainGrid1 >tbody >tr:eq(1)>td:eq(5)').find('.customer').html(obj.responseText);
					}
					else
					{
						$('#tblMainGrid1 >tbody >tr:eq(1)>td:eq(5)').find('.customer').html("");
					}
					tbl.rows[1].cells[2].childNodes[1].value = debitAmount;
					tbl.rows[1].cells[3].childNodes[1].value = creditAmount;
					tbl.rows[1].cells[4].childNodes[1].value = memo;
					tbl.rows[1].cells[5].childNodes[1].value = name;
					tbl.rows[1].cells[6].childNodes[1].value = dimension;
				}
				else
				{
					tbl.insertRow(rows);
					tbl.rows[rows].className='mainRow';
					tbl.rows[rows].innerHTML = tbl.rows[rows-1].innerHTML;
					tbl.rows[rows].cells[5].childNodes[1].value = "";
					tbl.rows[rows].cells[1].childNodes[1].value = accId;
					if(name != 0)
					{
						var url = "journalEntry-db-get.php?requestType=getName&accId="+accId;
						var obj = $.ajax({url:url,async:false});
						$('#tblMainGrid1 >tbody >tr:eq('+rows+')>td:eq(5)').find('.customer').html(obj.responseText);
					}
					else
					{
						$('#tblMainGrid1 >tbody >tr:eq('+rows+')>td:eq(5)').find('.customer').html("");
					}
					tbl.rows[rows].cells[2].childNodes[1].value = debitAmount;
					tbl.rows[rows].cells[3].childNodes[1].value = creditAmount;
					tbl.rows[rows].cells[4].childNodes[1].value = memo;
					tbl.rows[rows].cells[5].childNodes[1].value = name;
					tbl.rows[rows].cells[6].childNodes[1].value = dimension;
				}
			}
			calculateDebit();
			calculateCredit();
		}
		else
		{
			
		}
	   //--------------------------------------------------
	}
});
});
//////////// end of load details /////////////////

  	$('#frmJournalEntry #butNew').click(function(){
		//---------------------------------------------------
			existingMsgDate = "";
			amStatus = "Auto";
			$("#frmJournalEntry #txtNo").attr("readonly","readonly");
			$('#frmJournalEntry #chkAutoManual').attr('checked')
			$("#frmJournalEntry #chkAutoManual").attr("disabled","");
			//document.getElementById("chkAutoManual").style.display='';
			document.getElementById("amStatus").style.display='';
			$('#frmJournalEntry #txtNo').removeClass('validate[required]');
		//---------------------------------------------------
		$('#frmJournalEntry').get(0).reset();
		$('#tblMainGrid1 >tbody >tr').each(function(){
			if($(this).index()!=0 && $(this).index()!=1 )
			{
				$(this).remove();
			}
		});
		$('#tblMainGrid2 >tbody >tr').each(function(){
			if($(this).index()!=0 && $(this).index()!=1 )
			{
				$(this).remove();
			}
		});
		$('#frmJournalEntry #cboPaymentsMethods').change();//--->
		loadCombo_frmJournalEntry();
	});
	$('#frmJournalEntry #butDelete').click(function(){
		if($('#frmJournalEntry #cboSearch').val()=='')
		{
			$('#frmJournalEntry #butDelete').validationEngine('showPrompt', 'Please select G/L Number.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmJournalEntry #cboSearch option:selected').text()+'" ?',{
			buttons: { Ok: true, Cancel: false },
			callback: function(v,m,f){
			if(v)
			{
					var url = "journalEntry-db-set.php";
					var httpobj = $.ajax({
						url:url,
						dataType:'json',
						type:'post',
						data:'requestType=delete&cboSearch='+URLEncode($('#frmJournalEntry #cboSearch').val()),
						async:false,
						success:function(json){
							
							$('#frmJournalEntry #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
							
							if(json.type=='pass')
							{
								$('#frmJournalEntry').get(0).reset();
								$('#tblMainGrid2 >tbody >tr').each(function(){
									if($(this).index()!=0 && $(this).index()!=1 )
									{
										$(this).remove();
									}
								});
								$('#frmJournalEntry #cboPaymentsMethods').change();//--->
								loadCombo_frmJournalEntry();
								var t=setTimeout("alertDelete()",1000);return;
							}	
							var t=setTimeout("alertDelete()",3000);
						}	 
					});
			}
		}
			});	
		}
	});
	
	$('#frmJournalEntry #butPrint').click(function(){
		if($('#frmJournalEntry #txtNo').val()=='')
		{
			$('#frmJournalEntry #butPrint').validationEngine('showPrompt', 'Please select Entry.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var myurl = 'journalEntryDetails.php?id='+URLEncode($('#frmJournalEntry #txtNo').val());
    		window.open(myurl); 
		}
	});
	
});
////////////////////// calculation ////////////////////////////////
function calculateDebit()
{
	var debitTotal = 0.0000;
	var totDiff = 0.0000;
	$(".debit").each( function()
	{
		debitTotal += eval($(this).val()==''?0.0000:$(this).val());
	});
	$('#txtTotDebit').val(debitTotal.toFixed(4));
	totDiff = $('#txtTotDebit').val() - $('#txtTotCredit').val();
	$('#txtTotDiff').val(totDiff.toFixed(4));
}

function calculateCredit()
{
	var creditTotal = 0.0000;
	var totDiff = 0.0000;
	$(".credit").each( function()
	{
		creditTotal += eval($(this).val()==''?0.0000:$(this).val());
	});
	$('#txtTotCredit').val(creditTotal.toFixed(4));
	totDiff = $('#txtTotDebit').val() - $('#txtTotCredit').val();
	$('#txtTotDiff').val(totDiff.toFixed(4));
}
///////////////////////////////////////////////////////////////////
function loadCombo_frmJournalEntry()
{
	var url 	= "journalEntry-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmJournalEntry #cboSearch').html(httpobj.responseText);
}
function alertx()
{
	$('#frmJournalEntry #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmJournalEntry #butDelete').validationEngine('hide') ;
	$('#frmJournalEntry #butPrint').validationEngine('hide') ;
}

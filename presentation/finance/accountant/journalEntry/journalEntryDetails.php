<?php
session_start();
$backwardseperator = "../../../../";
$locationId = $_SESSION['CompanyID'];
$intUser  = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
include  	"{$backwardseperator}dataAccess/Connector.php";

include_once "../../commanFunctions/Converter.php";

$sql = "SELECT DISTINCT
		mst_locations.intCompanyId,
		mst_locations.strName,
		mst_companies.strVatNo,
		mst_companies.strSVatNo
		FROM
		mst_locations
		Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
		WHERE
		mst_locations.intId =  ".$_SESSION["CompanyID"]."";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$companyId = $row['intCompanyId']; 
}
$entryRefNo = $_REQUEST['id'];

$sql = "SELECT
		fin_accountant_journal_entry_header.intEntryNo,
		mst_financecurrency.strCode AS currency,
		fin_accountant_journal_entry_header.dblRate,
		fin_accountant_journal_entry_header.dtmDate,
		fin_accountant_journal_entry_header.strReferenceNo AS entryNo,
		fin_accountant_journal_entry_header.intCreator,
		fin_accountant_journal_entry_header.strFnRefNo,
		user1.intUserId,
		user1.strUserName AS creater,
		user2.strUserName AS modifyer
		FROM
		fin_accountant_journal_entry_header
		Inner Join mst_financecurrency ON fin_accountant_journal_entry_header.intCurrencyId = mst_financecurrency.intId
		Left Outer Join sys_users AS user1 ON fin_accountant_journal_entry_header.intCreator = user1.intUserId
		Left Outer Join sys_users AS user2 ON fin_accountant_journal_entry_header.intModifyer = user2.intUserId
		WHERE
		fin_accountant_journal_entry_header.strReferenceNo =  '$entryRefNo'";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$ourRef		= $row['intEntryNo'];
	$entryNo 	= $row['entryNo'];
	$currency	= $row['currency'];
	$entryDate 	= $row['dtmDate'];
	$rate 		= $row['dblRate'];
	$creater	= $row['creater'];
	$modifyer	= $row['modifyer'];
	$financeRefNo = $row['strFnRefNo'];
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Journal Entry Report</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:239px;
	top:172px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
.borderLine
{
	border: solid 1px black;
}
.borderLineIn
{
	border: solid 0.5px black;
	border-color:#999
}
</style>

<style>
    /*@media screen{thead{display:none;}}*/
    @media print{thead{display:table-header-group; margin-bottom:2px;}}
    @page{margin-top:1cm;margin-left:1cm;margin-right:1cm;margin-bottom:1.5cm;}}
</style>

</head>
<body>

<form id="frmSalesInvoiceDetails" name="frmSalesInvoiceDetails" method="post" action="salesInvoiceDetails.php">
<div align="center">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
<thead>
<th colspan="6">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<th colspan="3"></th>
</tr>
<tr>
<td width="10%"></td>
<td width="80%" height="80" valign="top"><strong>Journal Entry</strong><br /><?php include '../../../../reportHeader.php'?></td>
<td width="10%"></td>
</tr>
<tr>
<td colspan="3"></td>
</tr>
</table>
</th>
</tr>
<tr>
  <th>
  <table width="100%">
  <tr>
    <td colspan="6" align="center" bgcolor="#DBDBDB">
  </tr>
  <tr>
    <td height="21" colspan="2" align="left">
      <span class="normalfnt"><strong>Entry No :</strong> </span><span class="normalfnt"><?php echo $entryNo ?><br />
      </span></td>
    <td width="14%" align="left">&nbsp;</td>
    <td colspan="3" class="normalfnt" align="left"><strong>Date. :</strong> <?php echo $entryDate ?></td>
   </tr>
  <tr>
    <td colspan="2" class="normalfnt"><strong>Currency :</strong> <?php echo $currency ?></td>
    <td>&nbsp;</td>
    <td colspan="3" align="left"><span class="normalfnt"><strong>Rate :</strong> <?php echo $rate ?></span></td>
  </tr> 
   <tr>
     <td height="21" colspan="2" class="normalfnt"><strong>Reference Number :</strong> <?php echo $financeRefNo ?></td>
     <td>&nbsp;</td>
     <td>&nbsp;</td>
     <td align="center" valign="middle">&nbsp;</td>
     <td>&nbsp;</td>
   </tr>
   <tr>
     <td width="34%" height="21" class="normalfnt">&nbsp;</td>
     <td width="23%" align="center" valign="middle">&nbsp;</td>
     <td>&nbsp;</td>
     <td width="16%">&nbsp;</td>
     <td width="6%" align="center" valign="middle">&nbsp;</td>
     <td width="7%">&nbsp;</td>
   </tr>
  </table>
  </th>
</tr>
</thead>
<tbody>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td colspan="7" class="normalfnt">
        <table width="100%">
        <tr>
        <td width="100%" style="vertical-align:top" >
          <table width="100%" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr>
              <td width="382"  height="22" class="normalfnt borderLine"><strong>Account</strong></td>
              <td width="122" class="normalfntRight borderLine"><strong>Debit</strong></td>
              <td width="122" class="normalfntRight borderLine"><strong>Credit</strong></td>
              <td width="281" class="normalfnt borderLine"><strong>Memo</strong></td>
              <td width="187" class="normalfnt borderLine"><strong>Cost Center</strong></td>
              <td width="187" class="normalfnt borderLine"><strong>Name</strong></td>
              <!--<td width="7%" >Discount(%)</td>-->
             <!-- <td width="7%" >Tax Code</td>-->              </tr>
            <?php 
	  	  	$sql1 = "SELECT
					fin_accountant_journal_entry_details.strReferenceNo,
					CONCAT(mst_financechartofaccounts.strCode,'-',mst_financechartofaccounts.strName ) AS accounts,
					fin_accountant_journal_entry_details.dblDebitAmount,
					fin_accountant_journal_entry_details.dbCreditAmount,
					fin_accountant_journal_entry_details.strMemo,
					fin_accountant_journal_entry_details.strPersonType,
					fin_accountant_journal_entry_details.intNameId,
					mst_financedimension.strName AS dimension
					FROM
					fin_accountant_journal_entry_details
					Inner Join mst_financechartofaccounts ON fin_accountant_journal_entry_details.intChartOfAccountId = mst_financechartofaccounts.intId
					Inner Join mst_financedimension ON fin_accountant_journal_entry_details.intDimensionId = mst_financedimension.intId
					WHERE
					fin_accountant_journal_entry_details.strReferenceNo =  '$entryRefNo'";
			$result1 = $db->RunQuery($sql1);

		while($row=mysqli_fetch_array($result1))
		{
			$name = "";
			$nameId = $row['intNameId'];
			$subAmount = (($row['dblUnitPrice'])*((100-$row['dblDiscount'])/100))*$row['dblQty'];
			$totalTax = $totalTax + $row['dblTaxAmount'];
			if($row['strPersonType']=='cus')
			{
				$sqlName = "SELECT
							mst_customer.strName,
							mst_customer.intId
							FROM
							mst_customer
							WHERE
							mst_customer.intId =  '$nameId'";
				$resultName = $db->RunQuery($sqlName);

				while($rowName=mysqli_fetch_array($resultName))
				{
					$name = $rowName['strName'];
				}
			}
			else if($row['strPersonType']=='sup')
			{
				$sqlName = "SELECT
							mst_supplier.strName,
							mst_supplier.intId
							FROM
							mst_supplier
							WHERE
							mst_supplier.intId =  '$nameId'";
				$resultName = $db->RunQuery($sqlName);

				while($rowName=mysqli_fetch_array($resultName))
				{
					$name = $rowName['strName'];
				}
			}
			else if($row['strPersonType']=='ocus')
			{
				$sqlName = "SELECT
							mst_finance_service_customer.strName,
							mst_finance_service_customer.intId
							FROM
							mst_finance_service_customer
							WHERE
							mst_finance_service_customer.intId =  '$nameId'";
				$resultName = $db->RunQuery($sqlName);

				while($rowName=mysqli_fetch_array($resultName))
				{
					$name = $rowName['strName'];
				}
			}
			else if($row['strPersonType']=='osup')
			{
				$sqlName = "SELECT
							mst_finance_service_supplier.strName,
							mst_finance_service_supplier.intId
							FROM
							mst_finance_service_supplier
							WHERE
							mst_finance_service_supplier.intId =  '$nameId'";
				$resultName = $db->RunQuery($sqlName);

				while($rowName=mysqli_fetch_array($resultName))
				{
					$name = $rowName['strName'];
				}
			}
			else
			{
				$name = "";
			}
	  ?>
	  <tr class="normalfnt borderLineIn"  bgcolor="#FFFFFF">
   	  <td class="normalfnt borderLineIn" height="50"><?php echo $row['accounts']; ?>&nbsp;</td>
      <td class="normalfntRight borderLineIn"><?php echo $row['dblDebitAmount']; ?></td>
      <td class="normalfntRight borderLineIn" >&nbsp;<?php echo $row['dbCreditAmount'] ?></td>
      <td class="normalfnt borderLineIn" ><?php echo $row['strMemo'] ?></td>
      <td class="normalfnt borderLineIn" ><?php echo $row['dimension']; ?></td>
      <td class="normalfnt borderLineIn" ><?php echo $name ?></td>
      <?php 
			$totDebit+= $row['dblDebitAmount'];
			$totCredit+= $row['dbCreditAmount'];
			}
	  ?>
            </table>
          </td>
        </tr>
      </table>
      </td>
      </tr>
      </table>
    </td>
</tr>
<!--<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>-->
<tr height="40">
  <td align="center" class="normalfntMid">&nbsp;</td>
</tr>
</tbody>
<tr height="40">
  <td align="center" class="normalfntMid">
  <table width="100%">
  <tr>
    <td width="2%" align="left" class="normalfnt">&nbsp;</td>
    <td width="26%" align="center" class="normalfntMid">&nbsp;</td>
    <td colspan="2" align="left" class="normalfnt">
      <strong>
        <?php 
		echo "Total Debit/Credit Amount: ".number_format($totDebit, 2);
	  ?>
      <br />
      </strong><span class="normalfntGrey">(Amount in Word :-</span>
      <?php
		$val = $totDebit;
        $inWord= convert_number($val);   
        $sence=explode(".",number_format($val,2));
        $sent=convert_number($sence[1]);
        if(strlen($sence[1])>0)
		{
        	$inWord=$inWord." and ".$sent." cents";
        }
       echo $inWord;
    ?>)</td>
    </tr>
  <tr>
    <td align="left" class="normalfnt">&nbsp;</td>
    <td align="center" class="normalfntMid">&nbsp;</td>
    <td colspan="2" align="left" class="normalfnt"><span class="normalfntGrey">
      <?php
		echo "Exchange Rate: <strong>Rs. ".number_format($rate,4).""."/".$symbol." '".number_format($totDebit*$rate, 2)."'</strong>";
	  ?>
      <br />
      (<?php
		$val = $totDebit*$rate;
        $inWord= convert_number($val);   
        $sence=explode(".",number_format($val,2));
        $sent=convert_number($sence[1]);
        if(strlen($sence[1])>0)
		{
        	$inWord=$inWord." and ".$sent." cents";
        }
       echo $inWord;
    ?>) </span></td>
    </tr>
  <tr>
    <td align="left" class="normalfnt">&nbsp;</td>
    <td align="center" class="normalfntMid">&nbsp;</td>
    <td colspan="2" align="center" class="normalfntMid">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" class="normalfnt">&nbsp;</td>
    <td align="center" class="normalfntMid">
    <?php
	if($modifyer == NULL)
	{
		echo $creater;
	}
	else
	{
		echo $modifyer;
	}
	?>
    </td>
    <td width="46%" align="center" class="normalfntMid">&nbsp;</td>
    <td width="26%" align="right" class="normalfntRight">&nbsp;</td>
  </tr>
  <tr>
    <td align="left" class="normalfnt">&nbsp;</td>
    <td align="center" class="normalfntMid">........................................</td>
    <td align="center" class="normalfntMid">....................................................</td>
    <td align="right" class="normalfntMid">............................</td>
  </tr>
  <tr>
    <td align="left" class="normalfnt">&nbsp;</td>
    <td align="center" class="normalfntMid">Prepared By</td>
    <td align="center" class="normalfntMid">Authorized by Accountant</td>
    <td align="right" class="normalfntMid">Authorized By</td>
  </tr>
  </table>
  </td>
</tr>
</table>
</div>        
</form>
</body>
</html>
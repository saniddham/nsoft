<?php

session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$userId = $_SESSION['userId'];
include "{$backwardseperator}dataAccess/Connector.php";
$response = array('type' => '', 'msg' => '');
$companyId = $_SESSION['headCompanyId'];
$locationId = $_SESSION["CompanyID"];
include "../../commanFunctions/CommanEditAndDelete.php";
////////////////////////// main parameters ////////////////////////////////
$entryNo = trim($_REQUEST['txtNo']);
$requestType = $_REQUEST['requestType'];
$amStatus 	= $_REQUEST['amStatus'];
$id = $_REQUEST['cboSearch'];
///////////////////// journal entry header parameters ////////////////////
$currency = null(trim($_REQUEST['cboCurrency']));
$rate = trim($_REQUEST['txtRate']);
$date = trim($_REQUEST['txtDate']);

$fnRefNo 	= $_REQUEST['txtFnRefNo'];
///////////////////// journal entry detail parameters /////////////////////
$mainDetails = json_decode($_REQUEST['detail'], true);
///////////////////////////////////////////////////////////////////////////
//////////////////////// journal entry insert part ///////////////////////
if ($requestType == 'add') {
    try {
        $entryNumber = getNextEntryNo($companyId, $locationId);
        $accountPeriod = getLatestAccPeriod($companyId);
		if($amStatus == "Auto")
		{
			$entryReference = trim(encodeEntryNo($entryNumber, $accountPeriod, $companyId, $locationId,$date));
		}
		else if($amStatus == "Manual")
		{
			$entryReference	= $entryNo;
		}
        $db->begin();
        //Add data to transaction header*******************************************
        $sql = "INSERT INTO fin_transactions (entryDate, strProgramType,  documentNo, currencyId, currencyRate, transDetails, payMethodId, paymentNumber, accPeriod, userId, companyId, createdOn) VALUES
                    ('$date','Journal Entry','$entryReference',$currency,$rate,'J/A',null,null,$accountPeriod,$userId,$companyId,now())";

        $db->RunQuery2($sql);
        $entryId = $db->insertId;
        //********************************************************************************

        $sql = "INSERT INTO `fin_accountant_journal_entry_header`
		(`intEntryNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strReferenceNo`,`intCurrencyId`,`dblRate`,`dtmDate`,`intCreator`,dtmCreateDate,`intDeleteStatus`,entryId,strFnRefNo)
		VALUES ('$entryNumber','$accountPeriod','$locationId','$companyId','$entryReference',$currency,'$rate','$date','$userId',now(), '0',$entryId,'$fnRefNo')";

        $firstResult = $db->RunQuery2($sql);

        if (count($mainDetails) != 0 && $firstResult) {
            foreach ($mainDetails as $detail) {
                $accId = trim($detail['accId']);
                $debitAmount = val($detail['debitAmount']);
                $creditAmount = val($detail['creditAmount']);
                $memo = $detail['memo'];
                $name = val($detail['name']);
                $dimension = $detail['dimension'];
                //>>>>>>>>>>get Person type if available
                $sql = "SELECT   
				mst_financechartofaccounts.intId, 
				mst_financialsubtype.strName
				FROM
				mst_financialsubtype
				Inner Join mst_financechartofaccounts ON mst_financialsubtype.intId = mst_financechartofaccounts.intFinancialTypeId
				WHERE
				mst_financechartofaccounts.intId =  '$accId'
				";
                $result = $db->RunQuery2($sql);
                $row = mysqli_fetch_array($result);
                $chartOfAcc = $row['intId'];

                if ($row['strName'] == 'Accounts Receivable') {
                    $personType = "cus";
                } else if ($row['strName'] == 'Accounts Payable') {
                    $personType = "sup";
                } 
				else if($row['strName']=='Lease' || $row['strName']=='Loan' || $row['strName']=='Other Non Current Liabilities' || $row['strName']=='Other Current Liabilities'){
					$personType = "osup";
				}
				else if($row['strName']=='Interest In suspenses' || $row['strName']=='Prepayments' || $row['strName']=='Inter Company' || $row['strName']=='Other Current Assets' || $row['strName']=='Other Non - Current Assets'){
					$personType = "ocus";
				}
				else {
                    $personType = "non";
                }
                //>>>>>>>>>>>>>>>>>>>               
                if ($detail['debitAmount'] == '' && $detail['creditAmount'] != '') {
                    $accStatus = "C";
                    $accAmount = $creditAmount;
                } else if ($detail['creditAmount'] == '' && $detail['debitAmount'] != '') {
                    $accStatus = "D";
                    $accAmount = $debitAmount;
                }
				
				$sql = "SELECT
						COUNT(intItemSerial) AS 'no',
						MAX(intItemSerial) AS val,
						fin_accountant_journal_entry_details.intItemSerial
						FROM
						fin_accountant_journal_entry_details
						WHERE
						fin_accountant_journal_entry_details.strReferenceNo =  '$entryReference'
						ORDER BY intItemSerial DESC
						";
				$result = $db->RunQuery2($sql);
				$row= mysqli_fetch_array($result);
				if($row['no']==0)
				{
					$itemSerial = 1;
				}
				else
				{
					$itemSerial = $row['val'] + 1;
				}

                $sql = "INSERT INTO `fin_accountant_journal_entry_details` (`intEntryNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strReferenceNo`,`intChartOfAccountId`,`dblDebitAmount`,`dbCreditAmount`,`strMemo`,`intNameId`,`intDimensionId`,`intCreator`,dtmCreateDate,strPersonType,`intItemSerial`) 
				VALUES ('$entryNumber','$accountPeriod','$locationId','$companyId','$entryReference','$accId','$debitAmount','$creditAmount','$memo','$name','$dimension','$userId',now(),'$personType','$itemSerial')";

                $mainDetailResult = $db->RunQuery2($sql);

                //>>>>>>>>>>>>>>>>>>>>>>transaction table process - account>>>>>>>>>>>>>>>>>>>>>>>>>	
                if ($mainDetailResult && $firstResult) {
                    $sql = "INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId,personType, personId) VALUES 
                                        ($entryId,'$accStatus',$accId,$accAmount,'$memo',$dimension,'$personType',$name)";
                    $trnAccResult = $db->RunQuery2($sql);
                }
                //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            }
        }

        if ($mainDetailResult && $firstResult && $trnAccResult) {
            $response['type'] = 'pass';
            $response['msg'] = 'Saved successfully.';
            $response['entryNo'] = $entryReference;
            $db->commit();
        } else {
            $response['type'] = 'fail';
            $response['msg'] = $db->errormsg;
            $response['q'] = $sql;
            $db->rollback(); //roalback
        }
        echo json_encode($response);
    } catch (Exception $e) {
        $db->rollback(); //roalback
        $response['type'] = 'fail';
        $response['msg'] = $e->getMessage();
        $response['q'] = $sql;
        echo json_encode($response);
    }
}
////////////////////// journal entry update part ////////////////////////
else if ($requestType == 'edit') {
    //get Account Details
    $sql1 = "SELECT intChartOfAccountId,intNameId FROM fin_accountant_journal_entry_details WHERE strReferenceNo='$entryNo'";
    $result1 = $db->RunQuery($sql1);
    $x = 0;
    while ($row1 = mysqli_fetch_array($result1)) {
        $arr1[$x]['account'] = $row1['intChartOfAccountId'];
        $arr1[$x]['recvFrom'] = $row1['intNameId'];
        ++$x;
    }
    // ckeck Unrealize gain/loss for entry and if exist block edit and delete
    $chk = checkUnrealizeEntry('JN', $entryNo, $arr1);
    if ($chk) {
        $response['type'] = 'fail';
        $response['msg'] = "You cannot allow this process! Journal Entry has some Unrealize Gain Or Loss for Customer or Supplier";
        echo json_encode($response);
    } else {
        try {
            $db->begin();

            if (count($mainDetails) != 0) {

                $sql = "UPDATE `fin_accountant_journal_entry_header` SET 		intCurrencyId		= $currency,
							dblRate				='$rate',
                                                        dtmDate				='$date',
                                                        intModifyer			='$userId',
                                                        intDeleteStatus		='0',
														strFnRefNo			='$fnRefNo'
                        WHERE (`strReferenceNo`='$entryNo')";
                $firstResult = $db->RunQuery2($sql);
            }
            if (count($mainDetails) != 0 && $firstResult) {
                $sql = "SELECT
					fin_accountant_journal_entry_header.intEntryNo,
					fin_accountant_journal_entry_header.intAccPeriodId,
					fin_accountant_journal_entry_header.strReferenceNo,
                                        fin_accountant_journal_entry_header.entryId
					FROM
					fin_accountant_journal_entry_header
					WHERE
					fin_accountant_journal_entry_header.strReferenceNo =  '$entryNo'";
                $result = $db->RunQuery2($sql);
                while ($row = mysqli_fetch_array($result)) {
                    $entryNumber = $row['intEntryNo'];
                    $accountPeriod = $row['intAccPeriodId'];
                    $entryId = $row['entryId'];
                }
                $sql = "UPDATE fin_transactions SET 
                                    entryDate='$date',                                                        
                                    currencyId=$currency,
                                    currencyRate='$rate',
                                    accPeriod=$accountPeriod
                            WHERE entryId=$entryId";
                $db->RunQuery2($sql);

                $sqld = "DELETE FROM `fin_transactions_details` WHERE entryId=$entryId";
                $resultd = $db->RunQuery2($sqld);
                //=========================================================

                $sql = "DELETE FROM `fin_accountant_journal_entry_details` WHERE (`strReferenceNo`='$entryNo')";
                $db->RunQuery2($sql);


                foreach ($mainDetails as $detail) {
                    $accId = trim($detail['accId']);
                    $debitAmount = val($detail['debitAmount']);
                    $creditAmount = val($detail['creditAmount']);
                    $memo = $detail['memo'];
                    $name = val($detail['name']);
                    $dimension = $detail['dimension'];
                    //>>>>>>>>>>get Person type if available
                    $sql = "SELECT   
				mst_financechartofaccounts.intId, 
				mst_financialsubtype.strName
				FROM
				mst_financialsubtype
				Inner Join mst_financechartofaccounts ON mst_financialsubtype.intId = mst_financechartofaccounts.intFinancialTypeId
				WHERE
				mst_financechartofaccounts.intId =  '$accId'
				";
                    $result = $db->RunQuery2($sql);
                    $row = mysqli_fetch_array($result);
                    $chartOfAcc = $row['intId'];

                    if ($row['strName'] == 'Accounts Receivable') {
                        $personType = "cus";
                    } else if ($row['strName'] == 'Accounts Payable') {
                        $personType = "sup";
                    } 
					else if($row['strName']=='Lease' || $row['strName']=='Loan' || $row['strName']=='Other Non Current Liabilities' || $row['strName']=='Other Current Liabilities'){
					$personType = "osup";
					}
					else if($row['strName']=='Interest In suspenses' || $row['strName']=='Prepayments' || $row['strName']=='Inter Company' || $row['strName']=='Other Current Assets' || $row['strName']=='Other Non - Current Assets'){
					$personType = "ocus";
					}
					else {
                        $personType = "non";
                    }
                    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                    if (($detail['debitAmount'] == '0.0000' || $detail['debitAmount'] == '') && ($detail['creditAmount'] != '0.0000' || $detail['creditAmount'] != '')) {
                        $accStatus = "C";
                        $accAmount = $creditAmount;
                    } else if (($detail['creditAmount'] == '0.0000' || $detail['creditAmount'] == '') && ($detail['debitAmount'] != '0.0000' || $detail['debitAmount'] != '')) {
                        $accStatus = "D";
                        $accAmount = $debitAmount;
                    }
					
				$sql = "SELECT
						COUNT(intItemSerial) AS 'no',
						MAX(intItemSerial) AS val,
						fin_accountant_journal_entry_details.intItemSerial
						FROM
						fin_accountant_journal_entry_details
						WHERE
						fin_accountant_journal_entry_details.strReferenceNo =  '$entryNo'
						ORDER BY intItemSerial DESC
						";
				$result = $db->RunQuery2($sql);
				$row= mysqli_fetch_array($result);
				if($row['no']==0)
				{
					$itemSerial = 1;
				}
				else
				{
					$itemSerial = $row['val'] + 1;
				}

                    $sql = "INSERT INTO `fin_accountant_journal_entry_details` (`intEntryNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strReferenceNo`,`intChartOfAccountId`,`dblDebitAmount`,`dbCreditAmount`,`strMemo`,`intNameId`,`intDimensionId`,`intCreator`,dtmCreateDate,strPersonType,`intItemSerial`) 
				VALUES ('$entryNumber','$accountPeriod','$locationId','$companyId','$entryNo','$accId','$debitAmount','$creditAmount','$memo','$name','$dimension','$userId',now(),'$personType','$itemSerial')";

                    $mainDetailResult = $db->RunQuery2($sql);

                    //>>>>>>>>>>>>>>>>>>>>>>transaction table process - account>>>>>>>>>>>>>>>>>>>>>>>>>	
                    if ($mainDetailResult && $firstResult) {
                        $sql = "INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId,personType, personId) VALUES 
                                    ($entryId,'$accStatus',$accId,$accAmount,'$memo',$dimension,'$personType',$name)";
                        $trnAccResult = $db->RunQuery2($sql);
                    }
                    //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                }
            }
            if ($mainDetailResult && $firstResult && $trnAccResult) {
                $response['type'] = 'pass';
                $response['msg'] = 'Updated successfully.';
                $response['entryNo'] = $entryNo;
                $db->commit();
            } else {
                $response['type'] = 'fail';
                $response['msg'] = $db->errormsg;
                $response['q'] = $sql;
                $db->rollback();
            }
            echo json_encode($response);
        } catch (Exception $e) {
            $db->rollback(); //roalback
            $response['type'] = 'fail';
            $response['msg'] = $e->getMessage();
            $response['q'] = $sql;
            echo json_encode($response);
        }
    }
}
/////////// journal entry delete part /////////////////////
else if ($requestType == 'delete') {
    //get Account Details
    $sql1 = "SELECT intChartOfAccountId,intNameId FROM fin_accountant_journal_entry_details WHERE strReferenceNo='$id'";
    $result1 = $db->RunQuery($sql1);
    $x = 0;
    while ($row1 = mysqli_fetch_array($result1)) {
        $arr1[$x]['account'] = $row1['intChartOfAccountId'];
        $arr1[$x]['recvFrom'] = $row1['intNameId'];
        ++$x;
    }
    // ckeck Unrealize gain/loss for entry and if exist block edit and delete
    $chk = checkUnrealizeEntry('JN', $id, $arr1);
    if ($chk) {
        $response['type'] = 'fail';
        $response['msg'] = "You cannot allow this process! Journal Entry has some Unrealize Gain Or Loss for Customer or Supplier";
        echo json_encode($response);
    } else {
        try {
            $db->begin();
            $sqlRp = "SELECT
						fin_customer_receivedpayments_main_details.strDocNo,
						fin_customer_receivedpayments_main_details.intCompanyId,
						fin_customer_receivedpayments_main_details.strDocType
						FROM
						fin_customer_receivedpayments_main_details
						Inner Join fin_customer_receivedpayments_header ON fin_customer_receivedpayments_main_details.intReceiptNo = fin_customer_receivedpayments_header.intReceiptNo AND fin_customer_receivedpayments_main_details.intAccPeriodId = fin_customer_receivedpayments_header.intAccPeriodId AND fin_customer_receivedpayments_main_details.intLocationId = fin_customer_receivedpayments_header.intLocationId AND fin_customer_receivedpayments_main_details.intCompanyId = fin_customer_receivedpayments_header.intCompanyId AND fin_customer_receivedpayments_main_details.strReferenceNo = fin_customer_receivedpayments_header.strReferenceNo
						Inner Join fin_accountant_journal_entry_header ON fin_accountant_journal_entry_header.intAccPeriodId = fin_customer_receivedpayments_main_details.intAccPeriodId AND fin_accountant_journal_entry_header.intLocationId = fin_customer_receivedpayments_main_details.intLocationId AND fin_accountant_journal_entry_header.intCompanyId = fin_customer_receivedpayments_main_details.intCompanyId AND fin_accountant_journal_entry_header.strReferenceNo = fin_customer_receivedpayments_main_details.strDocNo
						WHERE
						fin_customer_receivedpayments_main_details.intCompanyId =  '$companyId' AND
						fin_customer_receivedpayments_main_details.strDocNo =  '$id' AND
						fin_customer_receivedpayments_main_details.strDocType =  'JN' AND
						fin_customer_receivedpayments_header.intDeleteStatus =  '0'";
            $resultRp = $db->RunQuery2($sqlRp);
            $sqlSp = "SELECT
						fin_supplier_payments_main_details.strDocNo,
						fin_supplier_payments_main_details.intCompanyId,
						fin_supplier_payments_main_details.strDocType
						FROM
						fin_supplier_payments_main_details
						Inner Join fin_supplier_payments_header ON fin_supplier_payments_main_details.intReceiptNo = fin_supplier_payments_header.intReceiptNo AND fin_supplier_payments_main_details.intAccPeriodId = fin_supplier_payments_header.intAccPeriodId AND fin_supplier_payments_main_details.intLocationId = fin_supplier_payments_header.intLocationId AND fin_supplier_payments_main_details.intCompanyId = fin_supplier_payments_header.intCompanyId AND fin_supplier_payments_main_details.strReferenceNo = fin_supplier_payments_header.strReferenceNo
						Inner Join fin_accountant_journal_entry_header ON fin_accountant_journal_entry_header.intAccPeriodId = fin_supplier_payments_main_details.intAccPeriodId AND fin_accountant_journal_entry_header.intLocationId = fin_supplier_payments_main_details.intLocationId AND fin_accountant_journal_entry_header.intCompanyId = fin_supplier_payments_main_details.intCompanyId AND fin_accountant_journal_entry_header.strReferenceNo = fin_supplier_payments_main_details.strDocNo
						WHERE
						fin_supplier_payments_header.intDeleteStatus =  '0' AND
						fin_supplier_payments_main_details.strDocType =  'JN' AND
						fin_supplier_payments_main_details.intCompanyId =  '$companyId' AND
						fin_supplier_payments_main_details.strDocNo =  '$id'";
            $resultSp = $db->RunQuery2($sqlSp);
			$sqlOsp = "SELECT
						fin_other_payable_payments_main_details.strDocNo,
						fin_other_payable_payments_main_details.intCompanyId,
						fin_other_payable_payments_main_details.strDocType
						FROM
						fin_other_payable_payments_main_details
						Inner Join fin_other_payable_payments_header ON fin_other_payable_payments_main_details.intReceiptNo = fin_other_payable_payments_header.intReceiptNo AND fin_other_payable_payments_main_details.intAccPeriodId = fin_other_payable_payments_header.intAccPeriodId AND fin_other_payable_payments_main_details.intLocationId = fin_other_payable_payments_header.intLocationId AND fin_other_payable_payments_main_details.intCompanyId = fin_other_payable_payments_header.intCompanyId AND fin_other_payable_payments_main_details.strReferenceNo = fin_other_payable_payments_header.strReferenceNo
						Inner Join fin_accountant_journal_entry_header ON fin_accountant_journal_entry_header.intAccPeriodId = fin_other_payable_payments_main_details.intAccPeriodId AND fin_accountant_journal_entry_header.intLocationId = fin_other_payable_payments_main_details.intLocationId AND fin_accountant_journal_entry_header.intCompanyId = fin_other_payable_payments_main_details.intCompanyId AND fin_accountant_journal_entry_header.strReferenceNo = fin_other_payable_payments_main_details.strDocNo
						WHERE
						fin_other_payable_payments_header.intDeleteStatus =  '0' AND
						fin_other_payable_payments_main_details.strDocType =  'JN' AND
						fin_other_payable_payments_main_details.intCompanyId =  '$companyId' AND
						fin_other_payable_payments_main_details.strDocNo =  '$id'";
            $resultOsp = $db->RunQuery2($sqlOsp);

            if (!mysqli_num_rows($resultRp) && !mysqli_num_rows($resultSp) && !mysqli_num_rows($resultOsp)) {
                $sql = "UPDATE `fin_accountant_journal_entry_header` SET intDeleteStatus ='1', intModifyer ='$userId'
							WHERE (`strReferenceNo`='$id')";
                $result = $db->RunQuery2($sql);

                //==========UPDATE TRANS ACTION delete STATUS
                $sql = "SELECT fin_accountant_journal_entry_header.entryId FROM fin_accountant_journal_entry_header WHERE (`strReferenceNo`='$id')";
                $result = $db->RunQuery2($sql);
                $row = mysqli_fetch_array($result);
                $entryId = $row['entryId'];
                $sqld = "UPDATE `fin_transactions` SET delStatus=1 WHERE entryId=$entryId";
                $resultd = $db->RunQuery2($sqld);
                //============================
                if (($result)) {
                    $response['type'] = 'pass';
                    $response['msg'] = 'Deleted successfully.';
                    $db->commit();
                } else {
                    $response['type'] = 'fail';
                    $response['msg'] = $db->errormsg;
                    $response['q'] = $sql;
                    $db->rollback(); //roalback
                }
            } else {
                $db->rollback();
                $response['type'] = 'fail';
                $response['msg'] = "You cannot allow this process! Journal Entry has some payements";
            }
            echo json_encode($response);
        } catch (Exception $e) {
            $db->rollback(); //roalback
            $response['type'] = 'fail';
            $response['msg'] = $e->getMessage();
            $response['q'] = $sql;
            echo json_encode($response);
        }
    }
}

//--------------------------------------------------------------------------------------------
function getNextEntryNo($companyId, $locationId) {
    global $db;
    $sql = "SELECT
				intJournalEntryNo
				FROM sys_finance_no
				WHERE
				intCompanyId = '$companyId' AND intLocationId = '$locationId'
				";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $nextEntryNo = $row['intJournalEntryNo'];

    $sql = "UPDATE `sys_finance_no` SET intJournalEntryNo=intJournalEntryNo+1 WHERE (intCompanyId = '$companyId' AND intLocationId = '$locationId')";

    $db->RunQuery($sql);
    return $nextEntryNo;
}

//--------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------
function getLatestAccPeriod($companyId) {
    global $db;
    $sql = "SELECT
				MAX(mst_financeaccountingperiod.intId) AS accId,
				mst_financeaccountingperiod.dtmStartingDate,
				mst_financeaccountingperiod.dtmClosingDate,
				mst_financeaccountingperiod.intStatus,
				mst_financeaccountingperiod_companies.intCompanyId,
				mst_financeaccountingperiod_companies.intPeriodId
				FROM
				mst_financeaccountingperiod
				Inner Join mst_financeaccountingperiod_companies ON mst_financeaccountingperiod_companies.intPeriodId = mst_financeaccountingperiod.intId
				WHERE
				mst_financeaccountingperiod_companies.intCompanyId =  '$companyId' AND mst_financeaccountingperiod.intStatus = '1'
				ORDER BY
				mst_financeaccountingperiod.intId DESC
				";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $latestAccPeriodId = $row['accId'];
    return $latestAccPeriodId;
}

//--------------------------------------------------------------------------------------------
//============================================================================================
function encodeEntryNo($entryNo, $accountPeriod, $companyId, $locationId, $date) {
    global $db;
    $sql = "SELECT
				mst_financeaccountingperiod.intId,
				mst_financeaccountingperiod.dtmStartingDate,
				mst_financeaccountingperiod.dtmClosingDate,
				mst_financeaccountingperiod.intStatus
				FROM
				mst_financeaccountingperiod
				WHERE
				mst_financeaccountingperiod.intId =  '$accountPeriod'
				";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    if($row['dtmStartingDate'] <= $date && $date <= $row['dtmClosingDate'])
	{
		$startDate = substr($row['dtmStartingDate'],0,4);
		$closeDate = substr($row['dtmClosingDate'],0,4);
	}
	else
	{
			$sql = "SELECT
				mst_financeaccountingperiod.intId,
				mst_financeaccountingperiod.dtmStartingDate,
				mst_financeaccountingperiod.dtmClosingDate,
				mst_financeaccountingperiod.intStatus
				FROM
				mst_financeaccountingperiod
				WHERE
				mst_financeaccountingperiod.dtmStartingDate <= '$date' AND
				mst_financeaccountingperiod.dtmClosingDate >=  '$date'
				";	
			$result = $db->RunQuery($sql);
			$row = mysqli_fetch_array($result);
			$startDate = substr($row['dtmStartingDate'],0,4);
			$closeDate = substr($row['dtmClosingDate'],0,4);
	}
    $sql = "SELECT
				mst_companies.strCode AS company,
				mst_companies.intId,
				mst_locations.intCompanyId,
				mst_locations.strCode AS location,
				mst_locations.intId
				FROM
				mst_companies
				Inner Join mst_locations ON mst_locations.intCompanyId = mst_companies.intId
				WHERE
				mst_locations.intId =  '$locationId' AND
				mst_companies.intId =  '$companyId'
				";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $companyCode = $row['company'];
    $locationCode = $row['location'];
    $entryFormat = $companyCode . "/" . $locationCode . "/" . $startDate . "-" . $closeDate . "/" . $entryNo;
    return $entryFormat;
}

//============================================================================================
?>
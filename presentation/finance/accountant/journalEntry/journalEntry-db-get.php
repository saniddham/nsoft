<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$companyId = $row['intCompanyId']; 
	}
	///////////  journal entry load part /////////////////////
	if($requestType=='loadCombo')
	{
		$sql = "SELECT
				fin_accountant_journal_entry_header.strReferenceNo,
				IFNULL(CONCAT(' - ',fin_accountant_journal_entry_header.strFnRefNo),'') AS refNo
				FROM
				fin_accountant_journal_entry_header
				WHERE
				fin_accountant_journal_entry_header.intCompanyId =  '$companyId' AND
				fin_accountant_journal_entry_header.intDeleteStatus = '0'
				ORDER BY intEntryNo DESC
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['strReferenceNo']."\">".$row['strReferenceNo'].$row['refNo']."</option>";
		}
		echo $html;
	}
	else if($requestType=='loadDetails')
	{
		$id  = $_REQUEST['id'];		
		
		//-------------------------------------------------------------
		$sql = "SELECT
				fin_accountant_journal_entry_details.intEntryNo,
				fin_accountant_journal_entry_details.intChartOfAccountId,
				fin_accountant_journal_entry_details.dblDebitAmount,
				fin_accountant_journal_entry_details.dbCreditAmount,
				fin_accountant_journal_entry_details.strMemo,
				fin_accountant_journal_entry_details.intNameId,
				fin_accountant_journal_entry_details.intDimensionId,
				fin_accountant_journal_entry_header.intDeleteStatus
				FROM
				fin_accountant_journal_entry_header
				Inner Join fin_accountant_journal_entry_details ON fin_accountant_journal_entry_header.strReferenceNo = fin_accountant_journal_entry_details.strReferenceNo
				WHERE
				fin_accountant_journal_entry_header.intDeleteStatus =  '0' AND
				fin_accountant_journal_entry_details.strReferenceNo =  '$id'
				ORDER BY intEntryNo
				";
		$result = $db->RunQuery($sql);
		$arrDetail;
		while($row=mysqli_fetch_array($result))
		{
			$val['accId'] 			= $row['intChartOfAccountId'];
			$val['debitAmount'] 	= number_format($row['dblDebitAmount'],4,'.','');
			$val['creditAmount'] 	= number_format($row['dbCreditAmount'],4,'.','');
			$val['memo'] 			= $row['strMemo'];
			$val['name'] 			= $row['intNameId'];
			$val['dimension'] 		= $row['intDimensionId'];
			$arrDetail[] = $val;
		}
		$response['detailVal'] = $arrDetail;
		//-----------------------------------------------------------
		$sql   = "SELECT
					fin_accountant_journal_entry_header.intCurrencyId,
					fin_accountant_journal_entry_header.dblRate,
					fin_accountant_journal_entry_header.dtmDate,
					fin_accountant_journal_entry_header.intDeleteStatus,
					fin_accountant_journal_entry_header.strFnRefNo
					FROM
					fin_accountant_journal_entry_header
					WHERE
					fin_accountant_journal_entry_header.intDeleteStatus =  '0' AND
					fin_accountant_journal_entry_header.strReferenceNo =  '$id'
					";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['currency'] 	= $row['intCurrencyId'];
			$response['rate'] 		= $row['dblRate'];
			$response['date'] 		= $row['dtmDate'];
			$response['fnRefNo']	= $row['strFnRefNo'];
		}
		//-------------------------------------------------------------
		echo json_encode($response);
	}
	else if($requestType=='getExchangeRate')
	{
		$currencyId  	= $_REQUEST['currencyId'];
		$exchangeDate	= $_REQUEST['exchangeDate'];
		
		$sql = "SELECT
					mst_financeexchangerate.dblSellingRate,
					mst_financeexchangerate.dblBuying
				FROM mst_financeexchangerate
				WHERE
					mst_financeexchangerate.intCurrencyId 	=  '$currencyId' AND
					mst_financeexchangerate.dtmDate 		=  '$exchangeDate'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		
		if(mysqli_num_rows($result)>0)
		{
			$arrValue['sellingRate'] 	= $row['dblSellingRate'];
			$arrValue['buyingRate'] 	= $row['dblBuying'];
		}
		else
		{
			$arrValue['sellingRate'] 	= "";
			$arrValue['buyingRate'] 	= "";
		}
		echo json_encode($arrValue);
	}
	else if($requestType=='getName')
	{
		$accountsId  = $_REQUEST['accId'];
		
		//////////////////////////accounts//////////////////////////////////
		$sql = "SELECT   
				mst_financechartofaccounts.intId, 
				mst_financialsubtype.strName
				FROM
				mst_financialsubtype
				Inner Join mst_financechartofaccounts ON mst_financialsubtype.intId = mst_financechartofaccounts.intFinancialTypeId
				WHERE
				mst_financechartofaccounts.intId =  '$accountsId'
				";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$chartOfAcc=$row['intId'];
		
		if($row['strName']=='Accounts Receivable')
		{
		$sql = "SELECT
				mst_financecustomeractivate.intCustomerId as id,
				mst_customer.strName
				FROM
				mst_financecustomeractivate
				Inner Join mst_customer ON mst_financecustomeractivate.intCustomerId = mst_customer.intId
				WHERE
				mst_financecustomeractivate.intCompanyId =  '$companyId' AND
				mst_financecustomeractivate.intChartOfAccountId =  '$chartOfAcc'
				ORDER BY strName";
		}
		else if($row['strName']=='Accounts Payable')
		{
		$sql = "SELECT
				mst_financesupplieractivate.intSupplierId as id,
				mst_supplier.strName
				FROM
				mst_financesupplieractivate
				Inner Join mst_supplier ON mst_financesupplieractivate.intSupplierId = mst_supplier.intId
				WHERE
				mst_financesupplieractivate.intCompanyId =  '$companyId' AND
				mst_financesupplieractivate.intChartOfAccountId =  '$chartOfAcc'
				ORDER BY strName";
		}
		else if($row['strName']=='Lease' || $row['strName']=='Loan' || $row['strName']=='Other Non Current Liabilities' || $row['strName']=='Other Current Liabilities'){
		$sql = "SELECT
				mst_finance_service_supplier_activate.intSupplierId as id,
				mst_finance_service_supplier.strName
				FROM
				mst_finance_service_supplier_activate
				Inner Join mst_finance_service_supplier ON mst_finance_service_supplier_activate.intSupplierId = mst_finance_service_supplier.intId
				WHERE
				mst_finance_service_supplier_activate.intCompanyId =  '$companyId' AND
				mst_finance_service_supplier_activate.intChartOfAccountId =  '$chartOfAcc'
				ORDER BY strName";
		}
		else if($row['strName']=='Interest In suspenses' || $row['strName']=='Prepayments' || $row['strName']=='Inter Company' || $row['strName']=='Other Current Assets' || $row['strName']=='Other Non - Current Assets'){
		$sql = "SELECT
				mst_finance_service_customer_activate.intCustomerId as id,
				mst_finance_service_customer.strName
				FROM
				mst_finance_service_customer_activate
				Inner Join mst_finance_service_customer ON mst_finance_service_customer_activate.intCustomerId = mst_finance_service_customer.intId
				WHERE
				mst_finance_service_customer_activate.intCompanyId =  '$companyId' AND
				mst_finance_service_customer_activate.intChartOfAccountId =  '$chartOfAcc'
				ORDER BY strName";
		}
		else
		{
			$sql = "";	
		}
		
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['id']."\">".$row['strName']."</option>";
		}
		echo $html;
		//////////////////////////////////////////////////////////////////////
	}
?>
<?php
session_start();
$backwardseperator = "../../../../";
$thisFilePath =  $_SERVER['PHP_SELF'];

include  "{$backwardseperator}dataAccess/permisionCheck2.inc";

$companyId 	= $_SESSION['headCompanyId'];

//---------------------------------------------New Grid-----------------------------------------------------
include_once "../../../../libraries/jqdrid/inc/jqgrid_dist.php";

$sql = "select * from(SELECT
		fin_accountant_journal_entry_header.intEntryNo, 'More' AS More,
		fin_accountant_journal_entry_details.strMemo AS memo,
		fin_accountant_journal_entry_details.dblDebitAmount,
		fin_accountant_journal_entry_details.dbCreditAmount AS credit,
		fin_accountant_journal_entry_details.intChartOfAccountId,
		fin_accountant_journal_entry_header.strReferenceNo AS docNo,
		fin_accountant_journal_entry_details.dblDebitAmount AS debit,
		CONCAT(mst_financechartofaccounts.strCode,'-',mst_financechartofaccounts.strName ) AS accounts,
		mst_financecurrency.strCode AS currency,
		mst_financechartofaccounts.intId,
		fin_accountant_journal_entry_header.dtmDate AS date,
		fin_accountant_journal_entry_header.strFnRefNo
		FROM
		fin_accountant_journal_entry_header
		Inner Join fin_accountant_journal_entry_details ON fin_accountant_journal_entry_header.intEntryNo = fin_accountant_journal_entry_details.intEntryNo AND fin_accountant_journal_entry_header.intAccPeriodId = fin_accountant_journal_entry_details.intAccPeriodId AND fin_accountant_journal_entry_header.intLocationId = fin_accountant_journal_entry_details.intLocationId AND fin_accountant_journal_entry_header.intCompanyId = fin_accountant_journal_entry_details.intCompanyId AND fin_accountant_journal_entry_header.strReferenceNo = fin_accountant_journal_entry_details.strReferenceNo
		Inner Join mst_financecurrency ON fin_accountant_journal_entry_header.intCurrencyId = mst_financecurrency.intId
		Inner Join mst_financechartofaccounts ON fin_accountant_journal_entry_details.intChartOfAccountId = mst_financechartofaccounts.intId
		WHERE
fin_accountant_journal_entry_header.intDeleteStatus =  '0'  AND
fin_accountant_journal_entry_header.intCompanyId =  '$companyId'
		ORDER BY
		fin_accountant_journal_entry_header.intEntryNo DESC
		) as t where 1=1";
		
$entryLink = "journalEntry.php?strReferenceNo={docNo}";
$entryReport = "journalEntryDetails.php?id={docNo}";

//Accounts
$col["title"] 	= "Accounts"; // caption of column
$col["name"] 	= "accounts"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "8";
$col["align"] 	= "left";
$cols[] = $col;	$col=NULL;

//Date
$col["title"] = "Date"; // caption of column
$col["name"] = "date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Currency
$col["title"] 	= "Currency"; // caption of column
$col["name"] 	= "currency"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "2";
$col["align"] 	= "center";
$cols[] = $col;	$col=NULL;

//Debit
$col["title"] 	= "Debit"; // caption of column
$col["name"] 	= "debit"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "4";
$col["align"] 	= "right";
$cols[] = $col;	$col=NULL;

//Credit
$col["title"] 	= "Credit"; // caption of column
$col["name"] 	= "credit"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "4";
$col["align"] 	= "right";
$cols[] = $col;	$col=NULL;

//Memo
$col["title"] 	= "Memo"; // caption of column
$col["name"] 	= "memo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "7";
$col["align"] 	= "left";
$cols[] = $col;	$col=NULL;

////Doc No
//$col["title"] 	= "Documents No."; // caption of column
//$col["name"] 	= "docNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
//$col["width"] 	= "6";
//$col["align"] 	= "center";
//$cols[] = $col;	$col=NULL;

//Reference Number
$col["title"] = "Reference Number"; // caption of column
$col["name"] = "strFnRefNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "center";
$col['link']	= $entryLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

//Doc No
$col["title"] = "Entry Number"; // caption of column
$col["name"] = "docNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "7";
$col["align"] = "center";
$col['link']	= $entryLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

//View
$col["title"] = "View"; // caption of column
$col["name"] = "More"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "1";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $entryReport;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;


$jq = new jqgrid('',$db);

$grid["caption"] 		= "Journal Entry Listing";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'intEntryNo'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes

//<><><><><><>======================Compulsory Code==============================<><><><><><>
	$jq->set_options($grid);
	$jq->select_command = $sql;
	$jq->set_columns($cols);
	$jq->set_actions(array(	
		"add"=>false, // allow/disallow add
		"edit"=>false, // allow/disallow edit
		"delete"=>false, // allow/disallow delete
		"rowactions"=>false, // show/hide row wise edit/del/save option
		"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
		"export"=>true
	) 
	);

$out = $jq->render("list1");
//<><><><><><>======================Compulsory Code==============================<><><><><><>

//---------------------------------------------New Grid-----------------------------------------------------

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Journal Entry Listing</title>
</head>

<body>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
<div align="center">
<!-------------------------------------------New Grid---------------------------------------------------->
<link rel="stylesheet" type="text/css" media="screen" href="../../../../libraries/jqdrid/js/themes/smoothness/jquery-ui.custom.css"></link>	
<link rel="stylesheet" type="text/css" media="screen" href="../../../../libraries/jqdrid/js/jqgrid/css/ui.jqgrid.css"></link>	

<script src="../../../../libraries/jqdrid/js/jquery.min.js" type="text/javascript"></script>
<script src="../../../../libraries/jqdrid/js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
<script src="../../../../libraries/jqdrid/js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>	
<script src="../../../../libraries/jqdrid/js/themes/jquery-ui.custom.min.js" type="text/javascript"></script>
<script src="../../../../libraries/javascript/script.js" type="text/javascript"></script>
<!-------------------------------------------New Grid---------------------------------------------------->

<table  width="100%" border="0" align="center" bgcolor="#FFFFFF">
<tr>
 <td>
   <div align="center" style="margin:10px">
     <?php echo $out?>
   </div>
  </td>
 </tr>
</table>
</div>
</body>
</html>
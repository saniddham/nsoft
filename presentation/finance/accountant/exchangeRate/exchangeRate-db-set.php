<?php 
	session_start();
	ini_set('max_execution_time', 10000000);
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$curr_date = date('Y-m-d');
	$savedStatus	= true;
	$savedMsg		= '';
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$companyId = $row['intCompanyId']; 
	}
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	$id 			= $_REQUEST['cboSearch'];
	$date			= trim($_REQUEST['txtDate']);
	//$sellRate		= val(trim($_REQUEST['txtSellRate']));
	//$buyRate		= val(trim($_REQUEST['txtBuyRate']));
	$status			= (trim($_REQUEST['chkAll']));	
	
	$serverDate		= (trim($_REQUEST['sdate']));
	
	$details	 	= json_decode($_REQUEST['mainDetails'], true);
	
	//=============================================================
		$bsCurr 	= trim($_REQUEST['bsCurr']);	
		if($bsCurr != '')
		{
			$sql = "SELECT
					mst_companies.intId,
					mst_companies.intBaseCurrencyId
					FROM
					mst_companies
					WHERE
					mst_companies.intBaseCurrencyId =  '$bsCurr'";
			$result = $db->RunQuery($sql);
			while($row=mysqli_fetch_array($result))
			{
				$arrCompany[] = $row['intId'];
			}
		}
		//print_r($arrCompany);
		
	//---------exchange rate entering time margin--------------
	$sql_t 		= "SELECT EXCHANGE_SAVING_DEAD_LINE From sys_config WHERE intCompanyId=".$companyId;
	$result_t 	= $db->RunQuery($sql_t);
	$row_t		=mysqli_fetch_array($result_t);
	$dead_line 	= $row_t['EXCHANGE_SAVING_DEAD_LINE'];
	$sql_today_records = "SELECT * FROM `mst_financeexchangerate` WHERE `dtmDate` = '$curr_date' AND `intCompanyId` IN ($companyId)";
	$result_records = $db->RunQuery($sql_today_records);
	$alreadyUpdated = (mysqli_num_rows($result_records)>0)?true:false;
	//--------------------------------------------------------
		
	//=============================================================
	/////////// exchange rate insert part /////////////////////
	if($requestType=='add')
	{
		 $sql = "SELECT intCurrencyId, dtmDate FROM mst_financeexchangerate
		 		 WHERE
				 dtmDate= '$date'"; // intCurrencyId = '$id' AND
		 $result = $db->RunQuery($sql);
		 if($savedStatus && mysqli_num_rows($result)>0)
		 {
			$savedStatus 		= false;
			$savedMsg	 		= 'You can\'t change back-date exchange rate';
		 }
		 if($savedStatus && (date('H:i') > $dead_line))
		 {
			$savedStatus 		= false;
			$savedMsg			= 'You can\'t enter exchange rates after 9.00 A.M';
		 }

		 if($savedStatus)
		 {$db->begin();
			for($i=0; $i < count($arrCompany); $i++)
			{
				$sqlMaxDt 	= "SELECT MAX(dtmDate) AS maxDate FROM mst_financeexchangerate WHERE intCompanyId='$arrCompany[$i]' "; 
				$resultMxDt = $db->RunQuery2($sqlMaxDt);
				$rowMxDt 	= mysqli_fetch_array($resultMxDt);
				$maxDate 	= $rowMxDt['maxDate'];
				
				if($maxDate!='')
				{
					$start 		= strtotime($date);
					$end 		= strtotime($maxDate);
				
					if($start>$end)
					{
						$days_between 	= ceil(abs($end - $start) / 86400);
						$newFrmDate 	= $maxDate;
						
						if($days_between>1)
						{
							setOldDateExchange($days_between,$newFrmDate,$maxDate,$userId,$arrCompany[$i]);	
						}
					}
				}
			}
			if($savedStatus && $status)
			{
				for($i=0; $i < count($arrCompany); $i++)
				{
					$company = $arrCompany[$i];
					foreach($details as $detail)
					{
						$currency 	= $detail['currency'];
						$sellRate 	= $detail['sellRate'];
						$buyRate 	= $detail['buyRate'];
						$avgRate	= round((($sellRate+$buyRate)/2),2);
						
						$sql = "INSERT INTO `mst_financeexchangerate` (`intCurrencyId`,`dtmDate`,`intCompanyId`,`dblSellingRate`,`dblBuying`,dblExcAvgRate,`intCreator`,dtmCreateDate) 
						VALUES ('$currency','$date','$company','$sellRate','$buyRate','$avgRate','$userId',now())";
						
						$result = $db->RunQuery2($sql);
						if($savedStatus && !$result)
						{
							$savedStatus	= false;
							$savedMsg		= $db->errormsg;		
						}
					}
				}
			}
			else if($savedStatus && !$status)
			{
				foreach($details as $detail)
					{
						$currency 	= $detail['currency'];
						$sellRate 	= $detail['sellRate'];
						$buyRate 	= $detail['buyRate'];
						$avgRate	= round((($sellRate+$buyRate)/2),2);
						
						$sql = "INSERT INTO `mst_financeexchangerate` (`intCurrencyId`,`dtmDate`,`intCompanyId`,`dblSellingRate`,`dblBuying`,dblExcAvgRate,`intCreator`,dtmCreateDate) 
						VALUES ('$currency','$date','$companyId','$sellRate','$buyRate','$avgRate','$userId',now())";
						
						$result = $db->RunQuery2($sql);
						if($savedStatus && !$result)
						{
							$savedStatus	= false;
							$savedMsg		= $db->errormsg;		
						}
					}
			}
			}
		if($savedStatus) {
            if ($_SESSION['headCompanyId'] == 1) {
                foreach ($details as $detail) {
                    $currency = $detail['currency'];
                    $currencyCode = $detail['code'];
                    $sellRate = $detail['sellRate'];
                    $buyRate = $detail['buyRate'];
                    $avgRate = round((($sellRate + $buyRate) / 2), 2);
                    sendDetailsToFinanceModule($date, $currencyCode, $currency, $avgRate, $userId, $bsCurr);
                }
                $db->commit();
                $response['type'] = 'pass';
                $response['msg'] = 'Saved successfully.';
            }
        }
		else{
			$db->rollback();
			$response['type'] 		= 'fail';
			$response['msg'] 		= $savedMsg;
			$response['q'] 			= $sql;
		}
		
		echo json_encode($response);
	}
	/////////// exchange rate update part /////////////////////
	else if($requestType=='edit')
	{
		 if(date('H:i') > $dead_line)
		 {
			$savedStatus 		= false;
			$savedMsg		 	= 'You can\'t enter exchange rates after 9.00 A.M';
		 }

        if ($savedStatus && $alreadyUpdated) {
            $savedStatus = false;
            $savedMsg = 'exchange rates are already updated';
        }

		 if($savedStatus && $status)
		 {$db->begin();
			for($i=0; $i < count($arrCompany); $i++)
			{
				$sqlMaxDt 	= "SELECT MAX(dtmDate) AS maxDate FROM mst_financeexchangerate WHERE intCompanyId='$arrCompany[$i]' "; 
				$resultMxDt = $db->RunQuery2($sqlMaxDt);
				$rowMxDt 	= mysqli_fetch_array($resultMxDt);
				$maxDate 	= $rowMxDt['maxDate'];
				
				if($maxDate!='')
				{
					$start 		= strtotime($date);
					$end 		= strtotime($maxDate);
				
					if($start>$end)
					{
						$days_between 	= ceil(abs($end - $start) / 86400);
						$newFrmDate 	= $maxDate;
						
						if($days_between>1)
						{
							setOldDateExchange($days_between,$newFrmDate,$maxDate,$userId,$arrCompany[$i]);	
						}
					}
				}
			}	
			
			 $sql = "SELECT intCurrencyId, dtmDate FROM mst_financeexchangerate
					WHERE
					dtmDate= '$serverDate' AND intCompanyId= '$companyId'";
			 $result = $db->RunQuery2($sql);
			 if(mysqli_num_rows($result)==0)
			 {
				for($i=0; $i < count($arrCompany); $i++)
					{
						$company = $arrCompany[$i];
						
						foreach($details as $detail)
						{
							$currency 	= $detail['currency'];
							$sellRate 	= $detail['sellRate'];
							$buyRate 	= $detail['buyRate'];
							$avgRate	= round((($sellRate+$buyRate)/2),2);
							
							$sql = "INSERT INTO `mst_financeexchangerate` (`intCurrencyId`,`dtmDate`,`intCompanyId`,`dblSellingRate`,`dblBuying`,dblExcAvgRate,`intCreator`,dtmCreateDate) 
							VALUES ('$currency','$date','$company','$sellRate','$buyRate','$avgRate','$userId',now())";
							$result = $db->RunQuery2($sql);
							if($savedStatus && !$result)
							{
								$savedStatus	= false;
								$savedMsg		= $db->errormsg;		
							}
						}
					}
			}
			else
			{	
				for($i=0; $i < count($arrCompany); $i++)
				{
					$company = $arrCompany[$i];
							
				foreach($details as $detail)
				{
					$currency 	= $detail['currency'];
					$sellRate 	= $detail['sellRate'];
					$buyRate 	= $detail['buyRate'];
					$avgRate	= round((($sellRate+$buyRate)/2),2);
					
					$sql = "UPDATE `mst_financeexchangerate` SET 	dblSellingRate	='$sellRate',
																dblBuying		='$buyRate',
																dblExcAvgRate	='$avgRate',
																intModifyer		='$userId'						
							WHERE (`intCurrencyId`='$currency' AND `dtmDate`='$date' AND `intCompanyId`='$company')";
					
					$result = $db->RunQuery2($sql);
					if($savedStatus && !$result)
					{
						$savedStatus	= false;
						$savedMsg		= $db->errormsg;		
					}
				}
			}
			}
		 }
	else if($savedStatus && !$status)
		{	
			$db->begin();		
			foreach($details as $detail)
				{
					$currency 	= $detail['currency'];
					$sellRate 	= $detail['sellRate'];
					$buyRate 	= $detail['buyRate'];
					$avgRate	= round((($sellRate+$buyRate)/2),2);
					$sqlNewAdd	= "SELECT intCurrencyId, dtmDate FROM mst_financeexchangerate
							WHERE
							`intCurrencyId`='$currency' AND `dtmDate`='$serverDate' AND `intCompanyId`='$companyId'";
					 $resultNew = $db->RunQuery2($sqlNewAdd);
					 
					 if(mysqli_num_rows($resultNew)==0)
					 {
						 $sqlNew = "INSERT INTO `mst_financeexchangerate` (`intCurrencyId`,`dtmDate`,`intCompanyId`,`dblSellingRate`,`dblBuying`,dblExcAvgRate,`intCreator`,dtmCreateDate) 
						VALUES ('$currency','$date','$companyId','$sellRate','$buyRate','$avgRate','$userId',now())";
						
						$result = $db->RunQuery2($sqlNew);
						if($savedStatus && !$result)
						{
							$savedStatus	= false;
							$savedMsg		= $db->errormsg;		
						}
					 }
					else
					{
							$sql = "UPDATE `mst_financeexchangerate` SET 	dblSellingRate	='$sellRate',
																dblBuying		='$buyRate',
																dblExcAvgRate	='$avgRate',
																intModifyer		='$userId'						
							WHERE (`intCurrencyId`='$currency' AND `dtmDate`='$date' AND `intCompanyId`='$companyId')";
						$result = $db->RunQuery2($sql);
						if($savedStatus && !$result)
						{
							$savedStatus	= false;
							$savedMsg		= $db->errormsg;		
						}
				}
			}
		}
		
		if($savedStatus) {
            if ($_SESSION['CompanyID'] == 2) { // details send if logged in screenline holdings only
                foreach ($details as $detail) {
                    $currency = $detail['currency'];
                    $currencyCode = $detail['code'];
                    $sellRate = $detail['sellRate'];
                    $buyRate = $detail['buyRate'];
                    $avgRate = round((($sellRate + $buyRate) / 2), 2);
                    sendDetailsToFinanceModule($date, $currencyCode, $currency, $avgRate, $userId, $bsCurr);
                }
            }
            $db->commit();
            $response['type'] = 'pass';
            $response['msg'] = 'Saved successfully.';

        }
		else{
			$db->rollback;
			$response['type'] 		= 'fail';
			$response['msg'] 		= $savedMsg;
			$response['q'] 			=$sql;
		}
		echo json_encode($response);
		}
		
	
	/////////// exchange rate delete part /////////////////////
/*	else if($requestType=='delete')
	{
		// no needed for this process
	}*/
function setOldDateExchange($days_between,$newFrmDate,$maxDate,$userId,$companyId)
{
	global $db;
	global $savedStatus;
	global $savedMsg;
	
	$savedStatus = true;
	for($t=1;$t<$days_between;$t++)
	{
		$date1 = date_create($newFrmDate);
		date_add($date1, date_interval_create_from_date_string('1 days'));
		$nextDt = date_format($date1, 'Y-m-d');
		
		$sqlIns = "INSERT INTO mst_financeexchangerate
					(
					intCurrencyId,dtmDate,
					intCompanyId,dblSellingRate,
					dblBuying,dblExcAvgRate,intCreator,
					dtmCreateDate
					)
					(SELECT intCurrencyId, '".$nextDt."', 
					intCompanyId, dblSellingRate, 
					dblBuying,ROUND(((dblSellingRate+dblBuying)/2),2),$userId, 
					NOW()
					FROM 
					mst_financeexchangerate 
					WHERE dtmDate='$maxDate' and
					intCompanyId='$companyId'
					)";
		
		$result = $db->RunQuery2($sqlIns);
		if($savedStatus && !$result)
		{
			$savedStatus	= false;
			$savedMsg		= $db->errormsg;		
		}
		
		$newFrmDate = $nextDt;	
	}
	
}

function sendDetailsToFinanceModule($date,$currencyCode,$currencyId,$exchangeRate,$userId,$baseCurrency){

    $configs = include('../../../../config/zillionConfig.php');
    $url = $configs['URL'].'IntExRate';
    $allowed_types = $configs['ALLOWED_TYPES'];
    if(!in_array($currencyCode,$allowed_types)){
        return;
    }
    $code = $currencyCode;
    if($currencyCode == 'EURO'){
        $code = 'Eur';
    }
    else if($currencyCode == 'LKR'){
        $code = '';
    }
    $data = array('Starting_Date' => $date, 'Currency_Code' => $code, 'Relational_Exch_Rate_Amount' => strval($exchangeRate));
    $data_json = json_encode($data);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    $auth_header = "Authorization: ".$configs["TYPE"].' '.base64_encode($configs['AUTH_USERNAME'].':'.$configs['AUTH_PASSWORD']);
    $header_arr = array("Content-Type: application/json", $auth_header);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header_arr);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $successStatus = 0;
    $response = curl_exec($ch);
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    if($httpcode == '201'){
        $successStatus = 1; //delivered
    }
    curl_close($ch);
    insertAPIRecordsToTable($currencyId,$date,$exchangeRate,$successStatus,$userId,$baseCurrency);

}

function insertAPIRecordsToTable($currencyId,$date,$exchangeRate,$status,$userId,$baseCurrency){

    global $db;
    $sql = "SELECT intCurrencyId, date, exchange_rate FROM trn_financemodule_exrate
							WHERE
							`intCurrencyId`='$currencyId' AND `date`='$date' LIMIT 1";

    $resultNew = $db->RunQuery2($sql);
    while($row=mysqli_fetch_array($resultNew))
    {
        $ex_rate = $row['exchange_rate'];
    }

    if (mysqli_num_rows($resultNew) == 0) {
        $sql_insert = " INSERT INTO `trn_financemodule_exrate` (
                    `intCurrencyId`,
                    `date`,
                    `exchange_rate`,
                    `intStatus`,
                    `createdDate`,
                    `createdBy`,
                    `baseCurrency`
                     )
                     VALUES
                    (
                        '$currencyId',
                        '$date',
                        '$exchangeRate',
                        '$status',
                         NOW(),
                        '$userId',
                        '$baseCurrency'
                    )";
    } else if($ex_rate != $exchangeRate && $status){
        $sql_insert = "UPDATE `trn_financemodule_exrate` SET 	exchange_rate	='$exchangeRate',
																intStatus		='$status',
																modifiedDate    = NOW(),
																modifiedBy		= '$userId'																				
							WHERE (`intCurrencyId`='$currencyId' AND `date`='$date')";
    }
    $result = $db->RunQuery2($sql_insert);
}
?>
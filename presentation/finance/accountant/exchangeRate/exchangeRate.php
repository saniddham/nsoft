<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$companyId = $row['intCompanyId']; 
}
?>
<script type="text/javascript">
var serverDate = '<?php echo date("Y-m-d") ?>';
</script>

<title>Exchange Rate</title>
<?php
	//include 		"include/javascript.html";
?>
<!--<script type="text/javascript" src="presentation/finance/accountant/exchangeRate/exchangeRate-js.js"></script>-->

<form id="frmExchangeRate" name="frmExchangeRate" method="post">
<div align="center">
  <div class="trans_layoutD">
    <div class="trans_text">New Exchange Rate</div>
    <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
    	<td>
        	<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
              <td colspan="2" class="normalfnt">Date <span class="compulsoryRed">*</span></td>
              <td width="24%" class="normalfnt"><input name="txtDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="validate[required]" id="txtDate" style="width:98px;" onClick="return showCalendar(this.id, '%Y-%m-%d');" /><input type="reset" value=""  class="txtbox" style="visibility:hidden;" onClick="return showCalendar(this.id, '%Y-%m-%');" /></td>
              <td width="63%" class="normalfnt" style="text-align:left"><a class="button white medium search" id="butSearch" name="butSearch">&nbsp;Search&nbsp;</a></td>
            </tr>
            <tr>
              <td colspan="4" class="normalfnt">
                <table width="100%" class="tableBorder" id="tblMainGrid1" cellpadding="1">
                  <tr>
                    <td colspan="4" class="normalfnt"><table width="100%" bgcolor="#E4E4E4" class="tableBorder">
                      <tr class="normalfnt">
                        <td width="25%"><strong>&nbsp;Currency Name</strong></td>
                        <td width="35%"><strong>&nbsp;Exchange Rate - Selling</strong> <span class="compulsoryRed">*</span></td>
                        <td width="35%"><strong>&nbsp;Exchange Rate - Buying</strong> <span class="compulsoryRed">*</span></td>
                        </tr>
                      </table></td>
                    </tr>
                  <?php
	 	 $sql = "SELECT
				mst_financecurrency.intId AS currId,
				mst_financecurrency.strCode,
				mst_companies.intBaseCurrencyId,
				mst_companies.intId,
				mst_financecurrency.intStatus
				FROM
				mst_financecurrency
				Left Join mst_companies ON mst_companies.intBaseCurrencyId = mst_financecurrency.intId and mst_companies.intId = $companyId
				WHERE
				mst_financecurrency.intStatus =  '1'
				ORDER BY
				mst_financecurrency.intId ASC";
				
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
	  			 ?>
                  <tr class="mainRow normalfnt">
                    <td width="4%" class="currency normalfnt" id=<?php echo $row['currId'];?>>&nbsp;</td>
                    <td width="23%" class="code normalfnt" style="text-align:left"><?php echo $row['strCode'];?></td>
                    <td width="36%" class="sellCode normalfnt" id="<?php echo $row['intBaseCurrencyId']; ?>" style="text-align:left"><input type="text" value="<?php echo($row['intBaseCurrencyId']==''?'':'1.0000'); ?>" name="txtSellRate<?php echo $row['currId'];?>" id="txtSellRate<?php echo $row['currId'];?>" class="sellRate validate[required,custom[number]]" style="width:170px;text-align:right" <?php echo ($row['intBaseCurrencyId']==''?'':'disabled=disabled') ?>/></td>
                    <td width="37%" class="buyCode normalfnt" id=<?php echo $row['intBaseCurrencyId'];?> style="text-align:left"><input type="text" value="<?php echo($row['intBaseCurrencyId']==''?'':'1.0000'); ?>" name="txtBuyRate<?php echo $row['currId'];?>" id="txtBuyRate<?php echo $row['currId'];?>" class="buyRate validate[required,custom[number]]" style="width:170px;text-align:right"  <?php echo ($row['intBaseCurrencyId']==''?'':'disabled=disabled') ?>/></td>
                    </tr>
                  <?php 
		 	} 
		 ?>
                  </table>
                </td>
            </tr>
              <tr>
              <td width="5%" class="normalfnt" style="text-align:left"><input type="checkbox" name="chkAll" id="chkAll" checked="checked" tabindex="24"/></td>
              <td colspan="3" class="normalfnt">Apply all company with same base currency</td>
              </tr>
              <tr>
                
              </tr>
             <tr>
    	<td height="32" colspan="4" >
    		<table width="100%" height="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td align="center" class="tableBorder_allRound"><a class="button white medium" id="butNew" name="butNew">New</a><?php echo($form_permision['edit']==1?'<a class="button white medium" id="butSave" name="butSave">Save</a>':'');?><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
              </tr>
            </table>
        </td>
    </tr>
    <tr>            
    </tr>
            <tr>
              <td colspan="4" class="normalfnt"><div style="overflow:scroll;width:100%;height:250px;" id="divGrid">
              <table width="100%" id="tblMainGrid" class="bordered">
                  <tr>
                    <th width="27%">Currency</th>
        			<th width="25%">Date</th>
        			<th width="24%">Selling Rate</th>
        			<th width="24%">Buying Rate</th>
                    </tr>
					<?php
                    $sql = "SELECT 
							c.intId,
							c.strCode,
							c.strSymbol,
							e.dtmDate,
							e.dblSellingRate,
							e.intCompanyId,
							e.dblBuying
							FROM mst_financecurrency c, mst_financeexchangerate e
							WHERE
							c.intId = e.intCurrencyId AND e.intCompanyId = '$companyId'
							ORDER BY dtmDate DESC, strCode ASC";
                    $result = $db->RunQuery($sql);
                    while($row=mysqli_fetch_array($result))
                    {
                    ?>
                        <tr class="normalfnt">
                        <td id=<?php echo $row['intId'];?> class="loadId" align="center" ><?php echo $row['strCode'];?> (<?php echo $row['strSymbol'];?>)</td>
                        <td align="center" ><?php echo $row['dtmDate'];?></td>
                        <td align="right" ><?php echo $row['dblSellingRate'];?></td>
                        <td align="right" ><?php echo $row['dblBuying'];?></td>
                        </tr>
                    <?php 
                    } 
                    ?>
                </table>
                </div>
                </td>
            </tr>
            </table>
        </td>
    </tr>
</table>      
</div>
</div>
</form>
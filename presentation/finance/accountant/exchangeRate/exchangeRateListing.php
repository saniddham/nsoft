<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$companyId = $_SESSION['CompanyID'];

if($_SERVER['REQUEST_METHOD'] == "POST")
{
	$optionValue = trim($_REQUEST['cboSearchOption']);
	$searchValue = trim($_REQUEST['txtSearchValue']);
}
$searchValue	 = (!isset($searchValue)?'':$searchValue);   
$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$companyId = $row['intCompanyId']; 
}
?>
<title>Exchange Rate Listing</title>

<script type="text/javascript" language="javascript">
function pageSubmit()
{
	document.getElementById('frmExchangeListing').submit();	
}
function clearValue()
{
	document.getElementById('txtSearchValue').value = '';
	document.getElementById('txtSearchValue').focus();
}
</script>
<form id="frmExchangeListing" name="frmExchangeListing" method="post">
<div align="center">
		<div class="trans_layoutS" style="width:600px">
		  <div class="trans_text">Exchange Rate Listing</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0" class="tableBorder_allRound">
          <tr>
            <td width="9%" height="22" class="normalfnt">&nbsp;</td>
            <td width="24%" class="normalfntMid">Searching Option</td>
            <td width="26%" class="normalfnt">
            <select name="cboSearchOption" id="cboSearchOption" onChange="clearValue();" style="width:150px">
              <option value="">&nbsp;</option>
<option value="c.strCode" <?php echo($optionValue=='c.strCode'?'selected':'') ?>>Currency</option>
<option value="e.dtmDate" <?php echo($optionValue=='e.dtmDate'?'selected':'') ?>>Date Range</option>
            </select></td>
            <td width="25%"><input value="<?php echo $searchValue; ?>" type="text" name="txtSearchValue" id="txtSearchValue" /></td>
            <td width="16%" class="normalfnt"  style="cursor:pointer">
              <img src="images/search.png" width="20" height="20" onClick="pageSubmit();" /></td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td>
          <!--<div id="_table_wrap" style="overflow: hidden; display: inline-block; border: 1px solid rgb(136, 136, 136);">
          <div id="_head_wrap" style="width: 800px; overflow: hidden;">-->
          <div style="overflow:hidden;width:600px;" id="divGrid">
          <table width="100%" class="bordered" id="tblMain" >
          <thead>
            <tr>
              <th height="22" style="width: 90px;" ><strong>Currency</strong></th>
              <th style="width: 80px;" ><strong>Date</strong></th>
              <th style="width: 75px;" ><strong>Selling Rate</strong></th>
              <th style="width: 75px;" ><strong>Buying Rate</strong></th>
              </tr>
           </thead>
           <tbody>
              <?php

				if($searchValue!='')
			  	$wherePart = "AND $optionValue like '%$searchValue%'";
				
				$wherePart = (!isset($wherePart)?'':$wherePart);
				
	 	 		 $sql = "SELECT 
					c.intId,
					c.strCode,
					c.strSymbol,
					e.dtmDate,
					e.dblSellingRate,
					e.dblBuying
					FROM mst_financecurrency c, mst_financeexchangerate e
					WHERE
					c.intId = e.intCurrencyId  AND e.intCompanyId = '$companyId'
					$wherePart
					ORDER BY dtmDate DESC, strCode ASC";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$id 		=	 $row['intId'];
	  			 ?>
           		<tr class="normalfnt">
           		<td class="exCurrency" height="16" align="left" bgcolor="#FFFFFF">
		   		<?php echo $row['strCode'];?> (<?php echo $row['strSymbol'];?>)</td>
              	<td class="exDate"  align="center" bgcolor="#FFFFFF"><?php echo $row['dtmDate'];?></td>
              	<td class="exSellRate" bgcolor="#FFFFFF" align="right"><?php echo $row['dblSellingRate'];?></td>
              	<td class="exBuyRate" align="right" bgcolor="#FFFFFF"><?php echo $row['dblBuying'];?></td>
              	</tr>
               <?php 
        	    } 
       		   ?>
              </tbody>
             </table>
           	</div>
              </td>
              </tr>
            </table>
			</td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound"><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>

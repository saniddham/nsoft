<?php
	$backwardseperator = "../../../";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Accountant Reports</title>
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../../libraries/calendar/theme.css" />
<script src="../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
<script src="journalEntry-js.js" type="text/javascript"></script>

</head>

<body>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<div align="center">
  <div class="trans_layoutD">
    <div class="trans_text">Accountant Reports</div>
    <table width="100%" cellspacing="0" cellpadding="5">
    <tr>
    <td width="50%" align="center" class="tableBorder_bottomRound"><a href="../reports/companyReports/reportMenu.php" class="normalfntMid">Company &amp; Financial</a></td>
    <td width="50%" align="center" class="tableBorder_topRound"><a href="../reports/customerReports/reportsMenu.php" class="normalfntMid">Customer Center</a></td>
    </tr>
    <tr>
      <td align="center" class="tableBorder_bottomRound"><a href="#" class="normalfntMid">Supplier Center</a></td>
      <td align="center" class="tableBorder_topRound"><a href="#" class="normalfntMid">Bank</a></td>
    </tr>
    <tr>
      <td align="center" class="tableBorder_bottomRound"><a href="#" class="normalfntMid">Inventory</a></td>
      <td align="center" class="tableBorder_topRound"><a href="../reports/accountReports/reportMenu.php" class="normalfntMid">Accounting</a></td>
    </tr>
    <tr>
      <td align="center" class="tableBorder_bottomRound"><a href="#" class="normalfntMid">Budget</a></td>
      <td align="center" class="tableBorder_topRound"><a href="#" class="normalfntMid">List</a></td>
    </tr>
    <tr>
      <td align="center">&nbsp;</td>
      <td align="center"></td>
    </tr>
    </table>

</div>
</div>    
</body>
</html>
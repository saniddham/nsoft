<?php
	$backwardseperator = "../../../";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Accountant Search</title>
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../../libraries/calendar/theme.css" />
<script src="../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
<!--<script src="journalEntry-js.js" type="text/javascript"></script>
-->
</head>

<body>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<div align="center">
  <div class="trans_layoutL">
    <div class="trans_text">Search All</div>
    <table width="100%" cellspacing="0" cellpadding="5">
    <tr class="tableBorder_allRound">
      <td width="30%" align="center" class="normalfntMid tableBorder_allRound" bgcolor="#E4F9DF"><strong>Customer</strong></td>
      <td width="30%" align="center" class="normalfntMid tableBorder_allRound" bgcolor="#F5D3C2"><strong>Supplier</strong></td>
      <td width="30%" align="center" class="normalfntMid tableBorder_allRound" bgcolor="#F8F7DE"><strong>Bank</strong></td>
	</tr>
    <tr>
      <td align="center" class="tableBorder_bottomRound"><a href="../customer/salesInvoice/salesInvoiceListing.php" target="_blank" class="normalfntMid">Sales Invoice</a></td>
      <td align="center" class="tableBorder_topRound"><a href="../supplier/purchaseInvoice/purchaseInvoiceListing.php" target="_blank" class="normalfntMid">Purchase Invoice</a></td>
      <td align="center" class="tableBorder_bottomRound"><a href="../bank/deposit/listing/depositListing.php" target="_blank" class="normalfntMid">Deposit</a></td>
    </tr>
    <tr>
      <td align="center" class="tableBorder_bottomRound"><a href="../customer/debitNoteInvoice/debitNoteInvoiceListing.php" target="_blank" class="normalfntMid">Debit Note Invoice</a></td>
      <td align="center" class="tableBorder_topRound"><a href="../supplier/debitNote/listing/debitNoteListing.php" target="_blank" class="normalfntMid">Debit Note</a></td>
      <td align="center" class="tableBorder_bottomRound"><a href="../bank/payments/listing/paymentsListing.php" target="_blank" class="normalfntMid">Payments</a></td>
    </tr>
    <tr>
      <td align="center" class="tableBorder_bottomRound"><a href="../customer/creditNote/listing/creditNoteListing.php" target="_blank" class="normalfntMid">Credit Note</a></td>
      <td align="center" class="tableBorder_topRound"><a href="../supplier/advancePayments/listing/advancePaymentsListing.php" target="_blank" class="normalfntMid">Advance Payments</a></td>
      <td align="center" class="tableBorder_bottomRound"><a href="../bank/pettyCash/listing/pettyCashListing.php" target="_blank" class="normalfntMid">Petty Cash</a></td>
    </tr>
    <tr>
      <td align="center" class="tableBorder_bottomRound"><a href="../customer/advanceReceived/listing/advanceReceivedListing.php" target="_blank" class="normalfntMid">Advance Received</a></td>
      <td align="center" class="tableBorder_topRound"><a href="../supplier/supplierPayments/supplierPaymentsListing.php" target="_blank" class="normalfntMid">Supplier Payments</a></td>
      <td align="center" class="tableBorder_bottomRound"><a href="../bank/unDepositFunds/unDepositFunds.php" target="_blank" class="normalfntMid">Un deposit Funds</a></td>
    </tr>
    <tr>
      <td align="center" class="tableBorder_bottomRound"><a href="../customer/receivedPayments/receivedPaymentsListing.php" target="_blank" class="normalfntMid">Received Payments</a></td>
      <td align="center" class="tableBorder_topRound"><a href="../supplier/supplierGainLoss/supplierGainLoss.php" target="_blank" class="normalfntMid">Supplier Gain/Loss</a></td>
      <td align="center" class="tableBorder_bottomRound"><a href="../accountant/journalEntry/journalEntry.php" target="_blank" class="normalfntMid">Journal Entry</a></td>
    </tr>
    <tr>
      <td align="center" class="tableBorder_bottomRound"><a href="../customer/customerGainLoss/customerGainLoss.php" target="_blank" class="normalfntMid">Customer Gain/Loss</a></td>
      <td align="center" class="tableBorder_topRound">&nbsp;</td>
      <td align="center" class="tableBorder_bottomRound">&nbsp;</td>
    </tr>
    <tr>
      <td height="26" align="center" class="tableBorder_bottomRound" bgcolor="#F8F7DE"></td>
      <td align="center" class="tableBorder_topRound" bgcolor="#F5D3C2"></td>
      <td align="center" class="tableBorder_bottomRound" bgcolor="#E4F9DF"></td>
    </tr>
    <tr>
      <td height="31" align="center">&nbsp;</td>
      <td align="center">&nbsp;</td>
      <td align="center">&nbsp;</td>
    </tr>
    </table>
</div>
</div>
</body>
</html>
<?php
	$backwardseperator = "../../../";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Higher Authorization</title>
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../../libraries/calendar/theme.css" />
<script src="../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

</head>

<body>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<div align="center">
  <div class="trans_layoutD">
  <div class="trans_text">Higher Authorization</div>
      <table width="100%" cellpadding="0" cellspacing="0" border="0" bgcolor="">
        <tr>
          <td width="8%" bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
          <td bgcolor="#FFFFFF"><span class="normalfnt">Amount</span></td>
          <td bgcolor="#FFFFFF"><span class="normalfnt">
            <input type="text" name="txtNumber2" id="txtAmount" />
          </span></td>
          <td bgcolor="#FFFFFF"><img src="../../../images/search.png"/></td>
          <td colspan="2" bgcolor="#FFFFFF">&nbsp;</td>
          <tr>
            <td bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
            <td colspan="5" bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
          <tr>
            <td bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
            <td bgcolor="#FFFFFF"><span class="normalfnt">Number</span></td>
            <td bgcolor="#FFFFFF"><span class="normalfnt">
              <input type="text" name="txtNumber" id="txtNumber" />
            </span></td>
            <td bgcolor="#FFFFFF"><img src="../../../images/search.png"/></td>
            <td colspan="2" bgcolor="#FFFFFF">&nbsp;</td>
          </tr>
        <tr>
            <td colspan="6" bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
        <tr>
        <td height="30" bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
   	    <td colspan="5" bgcolor="#FFFFFF"><span class="normalfnt">Date Range</span></td>
        </tr>
        <tr>
          <td bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
          <td width="18%" bgcolor="#FFFFFF" class="normalfnt"><div align="center">From:</div></td>
          <td width="24%" bgcolor="#FFFFFF"><input name="adviceDateFrom" type="text" value="<?php echo date("Y-m-d"); ?>" class="txtbox" id="adviceDate" style="width:98px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
          <td width="7%" bgcolor="#FFFFFF" class="normalfnt"><div align="center">To:</div></td>
          <td width="20%" bgcolor="#FFFFFF"> <input name="adviceDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="txtbox" id="adviceDateTo" style="width:98px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
          <td width="23%" bgcolor="#FFFFFF"><img src="../../../images/search.png"/></td>
        <tr>
          <td bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
          <td bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
          <td bgcolor="#FFFFFF">&nbsp;</td>
          <td bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
          <td bgcolor="#FFFFFF">&nbsp;</td>
          <td bgcolor="#FFFFFF">&nbsp;</td>
        </table>
</div>
</div> 
</body>
</html>
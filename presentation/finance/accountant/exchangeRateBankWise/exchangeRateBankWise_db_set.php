<?php 
session_start();
$backwardseperator 	= "../../../../";

$userId 			= $_SESSION['userId'];
$locationId	  		= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];

$savedMasseged		= "";
$error_sql			= "";
$savedStatus		= true;

include "{$backwardseperator}dataAccess/Connector.php";

$requestType 		= $_REQUEST['requestType'];
$id 				= $_REQUEST['cboSearch'];
$date				= trim($_REQUEST['txtDate']);
$bankId				= $_REQUEST['cboBank'];
$status				= (trim($_REQUEST['chkAll']));	
$serverDate			= (trim($_REQUEST['sdate']));
$details	 		= json_decode($_REQUEST['mainDetails'], true);

//=============================================================
$bsCurr 	= trim($_REQUEST['bsCurr']);	
if($bsCurr != '')
{
	$sql = "SELECT
			mst_companies.intId,
			mst_companies.intBaseCurrencyId
			FROM
			mst_companies
			WHERE
			mst_companies.intBaseCurrencyId =  '$bsCurr'";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$arrCompany[] = $row['intId'];
	}
}
	//print_r($arrCompany);
//=============================================================
/////////// exchange rate insert part /////////////////////
if($requestType=='add')
{
	$db->begin();
	
	 $sql = "SELECT CURRENCY_ID, DATE FROM mst_financeexchangerate_bankwise
			 WHERE
			 DATE= '$date' "; // intCurrencyId = '$id' AND
	 $result = $db->RunQuery2($sql);
	 if(mysqli_num_rows($result)>0 && ($savedStatus))
	 {
		$savedStatus			= false;
		$savedMasseged 			= 'You can\'t change back-date exchange rate';
	 }
	 else
	 {
		for($i=0; $i < count($arrCompany); $i++)
		{
			$sqlMaxDt 		= " SELECT MAX(DATE) AS maxDate 
								FROM mst_financeexchangerate_bankwise 
								WHERE COMPANY_ID = '$arrCompany[$i]' AND 
								BANK_ID = '$bankId' ";
			
			$resultMxDt 	= $db->RunQuery2($sqlMaxDt); 
			$rowMxDt 		= mysqli_fetch_array($resultMxDt);
			$maxDate 		= $rowMxDt['maxDate'];
			$start 			= strtotime($date);
			$end 			= strtotime($maxDate);
			if($start>$end)
			{
				$days_between 	= ceil(abs($end - $start) / 86400);
				$newFrmDate 	= $maxDate;
			
				if($days_between>1)
				{
					setOldDateExchange($days_between,$newFrmDate,$userId,$arrCompany[$i],$bankId);	
				}
			}
		}
		
		if($status)
		{
			for($i=0; $i < count($arrCompany); $i++)
			{
				$company = $arrCompany[$i];
				foreach($details as $detail)
				{
					$currency 	= $detail['currency'];
					$sellRate 	= $detail['sellRate'];
					$buyRate 	= $detail['buyRate'];
					$avgRate	= round((($sellRate+$buyRate)/2),2);
					
					$sql = "INSERT INTO mst_financeexchangerate_bankwise 
							(
							BANK_ID, 
							CURRENCY_ID, 
							DATE, 
							COMPANY_ID, 
							SELLING_RATE, 
							BUYING_RATE, 
							AVERAGE_RATE, 
							CREATED_BY, 
							CREATED_DATE
							)
							VALUES
							(
							'$bankId', 
							'$currency', 
							'$date', 
							'$company', 
							'$sellRate', 
							'$buyRate', 
							'$avgRate', 
							'$userId', 
							NOW()
							)";
					
					$result = $db->RunQuery2($sql);
					if(!$result && ($savedStatus))
					{
						$savedStatus 	= false;
						$savedMasseged 	= $db->errormsg;
						$error_sql 		= $sqlIns;
					}
				}
			}
		}
		else
		{
			foreach($details as $detail)
			{
				$currency 	= $detail['currency'];
				$sellRate 	= $detail['sellRate'];
				$buyRate 	= $detail['buyRate'];
				$avgRate	= round((($sellRate+$buyRate)/2),2);
				
				$sql = "INSERT INTO mst_financeexchangerate_bankwise 
						(
						BANK_ID, 
						CURRENCY_ID, 
						DATE, 
						COMPANY_ID, 
						SELLING_RATE, 
						BUYING_RATE, 
						AVERAGE_RATE, 
						CREATED_BY, 
						CREATED_DATE
						)
						VALUES
						(
						'$bankId', 
						'$currency', 
						'$date', 
						'$companyId', 
						'$sellRate', 
						'$buyRate', 
						'$avgRate', 
						'$userId', 
						NOW()
						)";
				
				$result = $db->RunQuery2($sql);
				if(!$result && ($savedStatus))
				{
					$savedStatus 	= false;
					$savedMasseged 	= $db->errormsg;
					$error_sql 		= $sqlIns;
				}
			}
		}
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg']		= "Saved Successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql'] 		= $error_sql;
	}
	echo json_encode($response);
}
/////////// exchange rate update part /////////////////////
else if($requestType=='edit')
{
	$db->begin();
	
	for($i=0; $i < count($arrCompany); $i++)
	{
		$sqlMaxDt 		= " SELECT IFNULL(MAX(DATE),0) AS maxDate 
							FROM mst_financeexchangerate_bankwise 
							WHERE COMPANY_ID = '$arrCompany[$i]' AND 
							BANK_ID = '$bankId' ";
			
		$resultMxDt 	= $db->RunQuery2($sqlMaxDt); 
		$rowMxDt 		= mysqli_fetch_array($resultMxDt);
		$maxDate 		= $rowMxDt['maxDate'];
		if($maxDate!=0)
		{
			$start 			= strtotime($date);
			$end 			= strtotime($maxDate);
			if($start>$end)
			{
				$days_between 	= ceil(abs($end - $start) / 86400);
				$newFrmDate 	= $maxDate;
			
				if($days_between>1)
				{
					setOldDateExchange($days_between,$newFrmDate,$userId,$arrCompany[$i],$bankId);	
				}
			}
		}
	}

	$sql = "SELECT CURRENCY_ID, DATE FROM mst_financeexchangerate_bankwise
			WHERE
			DATE= '$serverDate' AND 
			COMPANY_ID= '$companyId' AND
			BANK_ID = '$bankId'";
	
	 $result = $db->RunQuery2($sql);
	 if(mysqli_num_rows($result)==0)
	 {
		if($status)
		{
			for($i=0; $i < count($arrCompany); $i++)
			{
				$company = $arrCompany[$i];
				
				foreach($details as $detail)
				{
					$currency 	= $detail['currency'];
					$sellRate 	= $detail['sellRate'];
					$buyRate 	= $detail['buyRate'];
					$avgRate	= round((($sellRate+$buyRate)/2),2);
					
					$sql = "INSERT INTO mst_financeexchangerate_bankwise 
							(
							BANK_ID, 
							CURRENCY_ID, 
							DATE, 
							COMPANY_ID, 
							SELLING_RATE, 
							BUYING_RATE, 
							AVERAGE_RATE, 
							CREATED_BY, 
							CREATED_DATE
							)
							VALUES
							(
							'$bankId', 
							'$currency', 
							'$date', 
							'$company', 
							'$sellRate', 
							'$buyRate', 
							'$avgRate', 
							'$userId', 
							NOW()
							)";
					
					$result = $db->RunQuery2($sql);
					if(!$result && ($savedStatus))
					{
						$savedStatus 	= false;
						$savedMasseged 	= $db->errormsg;
						$error_sql 		= $sqlIns;
					}
				}
			}
		}
		else
		{			
			foreach($details as $detail)
			{
				$currency 	= $detail['currency'];
				$sellRate 	= $detail['sellRate'];
				$buyRate 	= $detail['buyRate'];
				$avgRate	= round((($sellRate+$buyRate)/2),2);
				
				$sql = "INSERT INTO mst_financeexchangerate_bankwise 
						(
						BANK_ID, 
						CURRENCY_ID, 
						DATE, 
						COMPANY_ID, 
						SELLING_RATE, 
						BUYING_RATE, 
						AVERAGE_RATE, 
						CREATED_BY, 
						CREATED_DATE
						)
						VALUES
						(
						'$bankId', 
						'$currency', 
						'$date', 
						'$companyId', 
						'$sellRate', 
						'$buyRate', 
						'$avgRate', 
						'$userId', 
						NOW()
						)";
				
				$result = $db->RunQuery2($sql);
				if(!$result && ($savedStatus))
				{
					$savedStatus 	= false;
					$savedMasseged 	= $db->errormsg;
					$error_sql 		= $sqlIns;
				}
			}
		}
	}
	else
	{
		if($status)
		{
			for($i=0; $i < count($arrCompany); $i++)
			{
				$company = $arrCompany[$i];
				foreach($details as $detail)
				{
					$currency 	= $detail['currency'];
					$sellRate 	= $detail['sellRate'];
					$buyRate 	= $detail['buyRate'];
					$avgRate	= round((($sellRate+$buyRate)/2),2);
					
					$sqlNewAdd	= "SELECT CURRENCY_ID, DATE FROM mst_financeexchangerate_bankwise
									WHERE
									DATE= '$serverDate' AND 
									COMPANY_ID= '$company' AND
									BANK_ID = '$bankId'";
					
					$resultNew = $db->RunQuery2($sqlNewAdd);
					if(mysqli_num_rows($resultNew)==0)
					{
						 $sql = "INSERT INTO mst_financeexchangerate_bankwise 
								(
								BANK_ID, 
								CURRENCY_ID, 
								DATE, 
								COMPANY_ID, 
								SELLING_RATE, 
								BUYING_RATE, 
								AVERAGE_RATE, 
								CREATED_BY, 
								CREATED_DATE
								)
								VALUES
								(
								'$bankId', 
								'$currency', 
								'$date', 
								'$company', 
								'$sellRate', 
								'$buyRate', 
								'$avgRate', 
								'$userId', 
								NOW()
								)";
					
						$result = $db->RunQuery2($sql);
						if(!$result && ($savedStatus))
						{
							$savedStatus 	= false;
							$savedMasseged 	= $db->errormsg;
							$error_sql 		= $sqlIns;
						}
					}
					else
					{
						$sql = "UPDATE mst_financeexchangerate_bankwise 
								SET
								SELLING_RATE = '$sellRate' , 
								BUYING_RATE = '$buyRate' , 
								AVERAGE_RATE = '$avgRate' , 
								MODIFIED_BY = '$userId' , 
								MODIFIED_DATE = NOW()
								WHERE
								BANK_ID = '$bankId' AND 
								CURRENCY_ID = '$currency' AND 
								DATE = '$date' AND 
								COMPANY_ID = '$company' ";
						
						$result = $db->RunQuery2($sql);
						if(!$result && ($savedStatus))
						{
							$savedStatus 	= false;
							$savedMasseged 	= $db->errormsg;
							$error_sql 		= $sqlIns;
						}
					}
				}
			}
		}
		else
		{
			foreach($details as $detail)
			{
				$currency 	= $detail['currency'];
				$sellRate 	= $detail['sellRate'];
				$buyRate 	= $detail['buyRate'];
				$avgRate	= round((($sellRate+$buyRate)/2),2);
				
				$sql = "UPDATE mst_financeexchangerate_bankwise 
						SET
						SELLING_RATE = '$sellRate' , 
						BUYING_RATE = '$buyRate' , 
						AVERAGE_RATE = '$avgRate' , 
						MODIFIED_BY = '$userId' , 
						MODIFIED_DATE = NOW()
						WHERE
						BANK_ID = '$bankId' AND 
						CURRENCY_ID = '$currency' AND 
						DATE = '$date' AND 
						COMPANY_ID = '$companyId' ";
						
						$result = $db->RunQuery2($sql);
						if(!$result && ($savedStatus))
						{
							$savedStatus 	= false;
							$savedMasseged 	= $db->errormsg;
							$error_sql 		= $sqlIns;
						}
			}
		}
	}
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg']		= "Updated Successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql'] 		= $error_sql;
	}
	echo json_encode($response);
}
function setOldDateExchange($days_between,$Date,$userId,$company,$bankId)
{
	global $db;
	global $savedMasseged;
	global $error_sql;
	global $savedStatus;
	
	$newDate = $Date; 
	for($t=1;$t<$days_between;$t++)
	{
		$date1 = date_create($newDate);
		date_add($date1, date_interval_create_from_date_string('1 days'));
		$nextDt = date_format($date1, 'Y-m-d');
		
		$sqlIns = " INSERT INTO mst_financeexchangerate_bankwise 
					(
					BANK_ID, 
					CURRENCY_ID, 
					DATE, 
					COMPANY_ID, 
					SELLING_RATE, 
					BUYING_RATE, 
					AVERAGE_RATE, 
					CREATED_BY, 
					CREATED_DATE
					)
					(
					SELECT $bankId,
					CURRENCY_ID, 
					'".$nextDt."',
					$company,
					SELLING_RATE, 
					BUYING_RATE, 
					AVERAGE_RATE,
					$userId,
					NOW()
					FROM 
					mst_financeexchangerate_bankwise 
					WHERE DATE = '$Date' AND
					BANK_ID = '$bankId' AND
					COMPANY_ID = '$company'
					)
				";
		
		$result = $db->RunQuery2($sqlIns);
		if(!$result && ($savedStatus))
		{
			$savedStatus 	= false;
			$savedMasseged 	= $db->errormsg;
			$error_sql 		= $sqlIns;
		}
		$newDate = $nextDt;	
	}
}
?>
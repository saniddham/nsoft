var basePath	= "presentation/finance/accountant/exchangeRateBankWise/";			
$(document).ready(function() {
  	
	$("#frmExchangeRate").validationEngine();
	
	//permision for add 
	/*if(intAddx)
	{
	$('#frmExchangeRate #butNew').show();
	$('#frmExchangeRate #butSave').show();
	}
	
	//permision for edit 
	if(intEditx)
	{
	$('#frmExchangeRate #butSave').show();
	$('#frmExchangeRate #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
	}
	
	//permision for delete
	if(intDeletex)
	{
	$('#frmExchangeRate #butDelete').show();
	$('#frmExchangeRate #cboSearch').removeAttr('disabled');
	}
	
	//permision for view
	if(intViewx)
	{
	$('#frmExchangeRate #cboSearch').removeAttr('disabled');
	}*/
  
  ///save button click event
  $('#frmExchangeRate #butSave').off('click').on('click',function(){
	var requestType = '';
	var bsCurr = '';
	showWaiting();
	if ($('#frmExchangeRate').validationEngine('validate'))   
    { 
		if($("#frmExchangeRate #txtDate").val() < serverDate)
			requestType = 'add';
		else if ($("#frmExchangeRate #txtDate").val() == serverDate)
			requestType = 'edit';
		else if ($("#frmExchangeRate #txtDate").val() > serverDate)
		{
			$('#frmExchangeRate #txtDate').validationEngine('showPrompt', 'You can\'t allow future date','fail');
			var t=setTimeout("alertx()",2000);
			hideWaiting();
			return;
		}
		value="[";
		
		$('#frmExchangeRate #tblMainGrid1 .mainRow').each(function(){
				
		currency	= $(this).find(".currency").attr('id');
		code		= $(this).find(".code").html();
		sellRate	= $(this).find(".sellRate").val();
		buyRate		= $(this).find(".buyRate").val();
		baseCurrency	= $(this).find(".sellCode").attr('id');
		if(baseCurrency != '')
		{
			bsCurr = baseCurrency;
		}
		
		value += '{"currency":"'+currency+'", "code":"'+code+'", "sellRate":"'+sellRate+'", "buyRate":"'+buyRate+'"},';
	});
	value = value.substr(0,value.length-1);
	value += "]";
	
		var url = basePath+"exchangeRateBankWise_db_set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json",
			type:'POST',  
			data:$("#frmExchangeRate").serialize()+'&requestType='+requestType+'&sdate='+serverDate+'&mainDetails='+value+'&bsCurr='+bsCurr,
			async:false,
			
			success:function(json){
					$('#frmExchangeRate #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmExchangeRate').get(0).reset();
						var t=setTimeout("alertx()",1000);
						setTimeout("loadCombo_frmExchangeRate()",1000);
						hideWaiting();
						return;
					}
					hideWaiting();
				},
			error:function(xhr,status){
					
					$('#frmExchangeRate #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					hideWaiting();
				}		
			});
	}
	else
	{
		hideWaiting();
	}
   });
   
   $('#frmExchangeRate #txtDate').off('click').on('click',function(){
	  $('#frmExchangeRate #cboSearch').val('');
	  $('#frmExchangeRate #txtSellRate').val('');
	  $('#frmExchangeRate #txtBuyRate').val('');
	  $('#frmExchangeRate #cboSearch').focus();
   });
   
    /////////////////////////////////////////////////////
   ////////load exchange details - new process //////////
   /////////////////////////////////////////////////////
   $('#frmExchangeRate .search').off('click').on('click',function(){
	   $('#frmExchangeRate').validationEngine('hide');
   });
    $('#frmExchangeRate .search').off('click').on('click',function(){
		$('#frmExchangeRate').validationEngine('hide');
		clearDetails();
		var url = basePath+"exchangeRateBankWise_db_get.php";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:'requestType=loadDetails&date='+$("#frmExchangeRate #txtDate").val()+'&bank='+$("#frmExchangeRate #cboBank").val(),
			async:false,
			success:function(json){
				var currencyId = "";
				var selRate = "";
				var buyRate = "";
				if(eval(json)!=null)
				{
					for(var i=0;i<=json.length;i++)
					{
						currencyId = json[i].currencyId;
						selRate = json[i].selRate;
						buyRate = json[i].buyRate;
						$('#frmExchangeRate #txtSellRate'+currencyId).val(selRate);
						$('#frmExchangeRate #txtBuyRate'+currencyId).val(buyRate);
					}
				}
				else
				{
					//$('.sellRate:text').val('');
					//$('.buyRate:text').val('');
				}
			}
	});
	//////////// end of load details /////////////////
	});
   
	$('#frmExchangeRate #butNew').off('click').on('click',function(){
		$('#frmExchangeRate').get(0).reset();
		//loadCombo_frmExchangeRate();
		$('#frmExchangeRate #cboSearch').focus();
	});
    $('#frmExchangeRate #butDelete').off('click').on('click',function(){
				
	
	});
});
function clearDetails()
{
	$('#frmExchangeRate #tblMainGrid1 .mainRow').each(function() {
        
		if(!$(this).find('.sellRate').attr('disabled'))
			$(this).find('.sellRate').val('');
		if(!$(this).find('.buyRate').attr('disabled'))
			$(this).find('.buyRate').val('');
    });
}
function loadCombo_frmExchangeRate()
{
	location.reload();
}
function alertx()
{
	$('#frmExchangeRate #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmExchangeRate #butDelete').validationEngine('hide')	;
}

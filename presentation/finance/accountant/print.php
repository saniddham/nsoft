<?php
	$backwardseperator = "../../../";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Accountant Print</title>
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../../libraries/calendar/theme.css" />
<script src="../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
<script src="journalEntry-js.js" type="text/javascript"></script>

</head>

<body>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<div align="center">
  <div class="trans_layoutD">
    <div class="trans_text">Print All</div>
    <table width="100%" cellspacing="0" cellpadding="5">
    <tr>
      <td width="14%" align="center" bgcolor="#FFFFFF" class="tableBorder_allRound"><span class="normalfnt"><strong>From</strong></span>
        <input name="adviceDateFrom" type="text" value="<?php echo date("Y-m-d"); ?>" class="txtbox" id="adviceDate" style="width:98px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
      <td width="14%" align="center" bgcolor="#FFFFFF" class="tableBorder_allRound"><span class="normalfntMid"><strong>To</strong></span>
<input name="adviceDateFrom" type="text" value="<?php echo date("Y-m-d"); ?>" class="txtbox" id="adviceDate2" style="width:98px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
      </tr>
    <tr>
    <td width="50%" align="center" class="tableBorder_bottomRound"><a href="#" class="normalfntMid">Cheque</a></td>
    <td width="50%" align="center" class="tableBorder_topRound"><a href="#" class="normalfntMid">Petty Cash</a></td>
    </tr>
    <tr>
      <td align="center" class="tableBorder_bottomRound"><a href="#" class="normalfntMid">Cash Payments</a></td>
      <td align="center" class="tableBorder_topRound"><a href="#" class="normalfntMid">Cheque Payment Voucher</a></td>
    </tr>
    <tr>
      <td align="center" class="tableBorder_bottomRound"><a href="#" class="normalfntMid">Receipt</a></td>
      <td align="center" class="tableBorder_topRound"><a href="#" class="normalfntMid">Journal Entry</a></td>
    </tr>
    <tr>
      <td align="center" class="tableBorder_bottomRound"><a href="#" class="normalfntMid">Debit Note</a></td>
      <td align="center" class="tableBorder_topRound"><a href="#" class="normalfntMid">Credit Note</a></td>
    </tr>
    <tr>
      <td align="center" class="tableBorder_bottomRound"><a href="#" class="normalfntMid">Bill Payment Voucher</a></td>
      <td align="center" class="tableBorder_topRound"><a href="#" class="normalfntMid">Deposit</a></td>
    </tr>
    <tr>
      <td align="center" class="tableBorder_bottomRound"><a href="#" class="normalfntMid">Cash Sale</a></td>
      <td align="center" class="tableBorder_topRound"><a href="#" class="normalfntMid">Credit Sale</a></td>
    </tr>
    <tr>
      <td align="center" class="tableBorder_bottomRound"><a href="#" class="normalfntMid">Credit Purchase</a></td>
      <td align="center" class="tableBorder_topRound"><a href="#" class="normalfntMid">Funds Transfer</a></td>
    </tr>
    <tr>
      <td align="center" class="tableBorder_bottomRound"><a href="#" class="normalfntMid">Advanced Received</a></td>
      <td align="center" class="tableBorder_topRound"><a href="#" class="normalfntMid">Advanced Payments</a></td>
    </tr>
    <tr>
      <td align="center" class="tableBorder_bottomRound"><a href="#" class="normalfntMid">Supplier Payments</a></td>
      <td align="center" class="tableBorder_topRound"><a href="#" class="normalfntMid">Accural Basis Bill</a></td>
    </tr>
    <tr>
      <td align="center">&nbsp;</td>
      <td align="center">&nbsp;</td>
    </tr>
    </table>
</div>
</div>    
</body>
</html>
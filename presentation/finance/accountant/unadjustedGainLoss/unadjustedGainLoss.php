<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$companyId = $_SESSION['headCompanyId'];
$location = $_SESSION['CompanyID'];
$thisFilePath = $_SERVER['PHP_SELF'];
include "{$backwardseperator}dataAccess/permisionCheck.inc";
// ======================Check Exchange Rate Updates========================

$currentDate = date("Y-m-d");
//
$sql = "SELECT COUNT(*) AS 'no'
		FROM
		mst_financeexchangerate
		WHERE
		mst_financeexchangerate.dtmDate =  '$currentDate'
		";
$result = $db->RunQuery($sql);
$row = mysqli_fetch_array($result);
if ($row['no'] == 0) {
    $str = "Please Update Exchange Rates Before " . $status . " Received Payments .";
    $str .= $row['NameList'];
    $maskClass = "maskShow";
} else {
    $maskClass = "maskHide";
}
// =========================================================================
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Unrealize Gain/Loss</title>

        <link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
        <link href="../../../../css/tblstyle.css" rel="stylesheet" type="text/css" />

        <script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
        <script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
        <script src="unadjustedGainLoss.js" type="text/javascript"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

        <link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
        <script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
        <script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

        <link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css"/>
        <link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css"/>   
        <style>
            .txtStyle{
                width:75px; background-color:#9F9; border:thin; text-align:right;                    
            }
            .show{
                display: ''
            }
            .hide{
                display: none;
            }


            * { padding: 0; margin: 0; }
            table.cruises { 
                font-family: Verdana;
                font-size: 10px;
                cellspacing: 0; 
                border-collapse: collapse; 
                width: 100%;    
            }
            table.cruises th, table.cruises td { 
                border-right: 1px solid #999; 
                border-bottom: 1px solid #999; 
            }
            table.cruises th { background: #aab; }
            table.cruises td { background: #eee; }

            div.scrollableContainer { 
                position: relative; 
                width: 100%; 
                padding-top: 1.7em; 
                margin: 0px;    
                border: 1px solid #999;
                background: #ffffff;
            }
            div.scrollingArea { 
                height: 450px; 
                overflow: auto; 
            }

            table.scrollable thead tr {
                left: 0; top: 0;
                position: absolute;
            }

            table.cruises .Account {text-align:center;width: 200px; }
            table.cruises .Account1 {text-align:left;}            
            table.cruises .Currency    { text-align:center;width: 200px; }
            table.cruises .Currency1    { text-align:left;}           
            table.cruises .Number   { text-align:center;width: 200px; }
            table.cruises .Number1   { text-align:right;width: 200px; }
            
        </style>
    </head>
    <body>
        <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
                <td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator . 'Header.php'; ?></td>
            </tr> 
        </table>
        <div id="divMask" class="<?php echo $maskClass ?> mask"> <?php echo $str; ?></div>
        <div id="divMaskProces" class="maskHide mask"><img src="loading.gif" width="319" height="305" alt="loading"/>
        </div>

        <script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
        <script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
        <script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
        <script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>
        <form id="frmUnadjustedGL" name="frmUnadjustedGL" method="post" autocomplete="off">
            <div align="center">
                <div class="trans_layoutL">
                    <div class="trans_text">Unrealize Gain/Loss</div>
                    <table>
                        <tr>
                            <td></td>
                            <td class="normalfnt">Date :</td>
                            <td class="normalfnt"><input name="txtDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="validate[required]" id="txtDate" style="width:98px;" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');"  /></td>
                            <td></td>
                        </tr>
                        <tr class="normalfntMid">
                            <td>Currency</td>
                            <td>Buying</td>
                            <td>Selling</td>
                            <td></td>
                        </tr>
                        <tbody id="curList"></tbody> 
                        <tr class="normalfntMid">                            
                            <td colspan="4"><input type="button" value="Display" name="btnDisplay" id="btnDisplay" class="hide"/></td>
                        </tr>
                    </table>
                    <br/>
                    <div class="scrollableContainer">
                        <div class="scrollingArea">
                            <table class="cruises scrollable">
                                <thead>
                                    <tr>
                                        <th class="Account">Account</th>                                        
                                        <th class="Currency">Currency</th>                               
                                        <th class="Number">Amount</th>
                                         <th class="Number">Rate</th>
                                        <th class="Number">Base Currency Amount</th>                                       
                                        <th class="Number">Gain/Loss</th>
                                    </tr>
                                </thead>                        
                                <tbody id="valueList"></tbody> 
                            </table>                            
                        </div>
                    </div>
                </div>
                <div align="center"><input type="button" value="Generate" name="btnGenerate" id="btnGenerate" class="hide"/></div>
            </div>

        </form>
    </body>
</html>
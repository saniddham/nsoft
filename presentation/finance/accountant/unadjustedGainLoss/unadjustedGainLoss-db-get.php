<?php
session_start();
$backwardseperator = "../../../../";
$mainPath 	= $_SESSION['mainPath'];
$userId 	= $_SESSION['userId'];
$requestType 	= $_REQUEST['requestType'];
$companyId = $_SESSION['headCompanyId'];
$location = $_SESSION['CompanyID'];
include "{$backwardseperator}dataAccess/Connector.php";

//==get base currency=============
$result = $db->RunQuery("SELECT com.intBaseCurrencyId FROM mst_companies AS com WHERE com.intId =  '$companyId'");
$row=mysqli_fetch_array($result);
$baseCurrencyId=$row['intBaseCurrencyId'];

if($requestType=='loadCurrency'){
    $theDate = $_REQUEST['theDate'];
    $sql="SELECT
            CUR.intId,
            CUR.strCode,
            ER.dblSellingRate,
            ER.dblBuying
            FROM
            mst_financecurrency AS CUR
            Inner Join mst_financecurrencyactivate AS CA ON CUR.intId = CA.intCurrencyId
            Inner Join mst_financeexchangerate AS ER ON CUR.intId = ER.intCurrencyId AND CA.intCompanyId = ER.intCompanyId
            WHERE
            CUR.intStatus =  1 AND
            CA.intCompanyId =  '$companyId' AND
            CUR.intId <>  '$baseCurrencyId' AND
            ER.dtmDate =  '$theDate'
            ORDER BY
            CUR.strCode ASC";
        $result = $db->RunQuery($sql);
        $curList="";
        while($row=mysqli_fetch_array($result)){
            $curId=$row['intId'];
            $buing=$row['dblBuying'];            
            $selling=$row['dblSellingRate'];
           $curList.= "<tr>";
            $curList.="<td class=\"normalfnt\">".$row['strCode']."
                            <input type=\"hidden\" name=\"txtCurId\" value=\"$curId\" />
                        </td>";
            $curList.="<td class=\"normalfntRight\">
                        <input class=\"rdoRate\" type=\"radio\" name=\"radio$curId\" id=\"radio$curId\" value=\"bui\" checked=\"true\"/>
                        <input type=\"text\" name=\"txtbuing$curId\" id=\"txtbuing$curId\" readonly=\"readonly\" class=\"validate[custom[number],required] normalfntBlue txtStyle\" value=\"$buing\"/> 
                       </td>";
            $curList.="<td class=\"normalfntRight\">
                            <input class=\"rdoRate\" type=\"radio\" name=\"radio$curId\" id=\"radio$curId\" value=\"sel\" />
                            <input type=\"text\" name=\"txtseling$curId\" id=\"txtseling$curId\" readonly=\"readonly\" class=\"validate[custom[number],required] normalfntBlue txtStyle\" value=\"$selling\"/> 
                       </td>";
            $curList.="<td class=\"normalfntRight\">
                            <input class=\"rdoRate\" type=\"radio\" name=\"radio$curId\" id=\"radio$curId\" value=\"oth\" />
                            <input type=\"text\" name=\"txtother$curId\" id=\"txtother$curId\" class=\"validate[custom[number],required] normalfntBlue txtStyle\" value=\"0\"/>
                       </td>";
           $curList.="</tr>";
        }
        $response['curList']=$curList;               
        echo json_encode($response);
}
else if($requestType=='loadValues'){
    $theDate = $_REQUEST['theDate'];
    $curRates = $_REQUEST['curIds'];
    $accPeriod=getLatestAccPeriod($companyId);
    //get ledger accounts of type Account Receiveble
    $sqlAcc="SELECT
                CA.intId,CA.strCode,CA.strName, mst_financialsubtype.intFinancialMainTypeID as mainType, mst_financialsubtype.strType
            FROM
                mst_financechartofaccounts AS CA
                Inner Join mst_financialsubtype ON CA.intFinancialTypeId = mst_financialsubtype.intId
                Inner Join mst_financechartofaccounts_companies ON CA.intId = mst_financechartofaccounts_companies.intChartOfAccountId
            WHERE
                CA.strExchangeRateAdj =  'Adjustments' AND
                CA.strType =  'Posting' AND
                mst_financialsubtype.intId NOT IN  (10, 18) AND
                mst_financechartofaccounts_companies.intCompanyId = '$companyId'";
    $resultAcc = $db->RunQuery($sqlAcc);
    $valueList="";
    while($rowAcc=mysqli_fetch_array($resultAcc)){
        $accId=$rowAcc['intId'];
        $mainTypeId=$rowAcc['mainType'];
        $strType=$rowAcc['strType'];
        $valueList.= "<tr>";
            $valueList.="<td class=\"Account1\" colspan=\"6\">".$rowAcc['strName']."-".$rowAcc['strCode']."</td>";          
        //get Currency List
        $sqlCur="SELECT
                CUR.intId, CUR.strCode            
            FROM
                mst_financecurrency AS CUR
                Inner Join mst_financecurrencyactivate AS CA ON CUR.intId = CA.intCurrencyId
            WHERE
                CUR.intStatus =  1 AND CA.intCompanyId =  '$companyId' AND CUR.intId <>  '$baseCurrencyId'";
        $resultCur= $db->RunQuery($sqlCur);
        while($rowCur=mysqli_fetch_array($resultCur)){
            $curId=$rowCur['intId'];
            $valueList.= "<tr>";
            $valueList.="<td class=\"Account1\"></td>";            
            $valueList.="<td class=\"Currency1\" colspan=\"5\">".$rowCur['strCode']."</td>";                            
            //get Account Balance
            $bal=getAcccountBalance($accId,$mainTypeId,$theDate,$curId,$companyId,$accPeriod,$strType);           
            $gainLoss=($bal['closingBal'] * $curRates[$curId])-($bal['baseCurcencyBal']);
            $valueList.= "<tr>";
                $valueList.="<td class=\"Account\"></td>";                
                $valueList.="<td class=\"Currency\"></td>";
                $valueList.="<td class=\"Number1\">".$bal['closingBal']."</td>";
                $valueList.="<td class=\"Number1\">".$curRates[$curId]."</td>";
                $valueList.="<td class=\"Number1\">".$bal['baseCurcencyBal']."</td>";    
                $valueList.="<td class=\"Number1\">".$gainLoss."</td>";
            $valueList.= "</tr>";
                            
                            
                        
        }//Curency                
                $valueList.= "</tr>";
            
            
            
            
            
            $valueList.= "</tr>";
        }//account
        $response['valueList']=$valueList;               
        echo json_encode($response);
}

//Get account Balance to date
function getAcccountBalance($accountId,$mainTypeId,$theDate,$curencyId,$companyId,$accPeriod,$strType){      
    global $db;
    $BalanceDetails;
    //get Curreency balancy
    $accPeriodSql="";
    //get account period value for ' Manufacturing ' and 'Income Statements'
    if($mainTypeId==1 | $mainTypeId==2){
        $accPeriodSql=" AND fin_transactions.accPeriod='$accPeriod'";
    }
    else{// skip accounting period for 'Balance Sheet '
        $accPeriodSql=" ";
    }
    
    //get Data And calculate opening Balance
    $resultDebitTot = $db->RunQuery("SELECT
                                            SUM(fin_transactions_details.amount) AS debitAmount
                                        FROM
                                            fin_transactions
                                            INNER JOIN fin_transactions_details ON fin_transactions.entryId = fin_transactions_details.entryId
                                        WHERE
                                            fin_transactions_details.accountId = $accountId AND
                                            fin_transactions_details.`credit/debit` = 'D' AND
                                            fin_transactions.entryDate <= '$theDate' AND
                                            fin_transactions.authorized = 1 AND
                                            fin_transactions.delStatus = 0 AND
                                            fin_transactions.currencyId = $curencyId AND
                                            fin_transactions.companyId = $companyId ".$accPeriodSql);    
    
    $rowDebitTot=mysqli_fetch_array($resultDebitTot);
    
    $resultCreditTot = $db->RunQuery("SELECT
                                        SUM(fin_transactions_details.amount) AS creditAmount
                                    FROM
                                        fin_transactions
                                        INNER JOIN fin_transactions_details ON fin_transactions.entryId = fin_transactions_details.entryId
                                    WHERE
                                        fin_transactions_details.accountId = $accountId AND
                                        fin_transactions_details.`credit/debit` = 'C' AND
                                        fin_transactions.entryDate <='$theDate' AND
                                        fin_transactions.authorized = 1 AND
                                        fin_transactions.delStatus = 0 AND
                                        fin_transactions.currencyId = $curencyId AND
                                        fin_transactions.companyId = $companyId ".$accPeriodSql);
    $rowCreditTot=mysqli_fetch_array($resultCreditTot);
    $openingBalance=0;
    if ($strType=='D'){
        $openingBalance=($rowDebitTot['debitAmount']-$rowCreditTot['creditAmount']);
    }else{
        $openingBalance=($rowCreditTot['creditAmount'] - $rowDebitTot['debitAmount']);
    }
    $BalanceDetails['closingBal']=$openingBalance;
    
    //get Base currency Balance=======================
    //get Data And calculate opening Balance
    $resultDebitTot = $db->RunQuery("SELECT
                                            SUM(fin_transactions_details.amount * fin_transactions.currencyRate) AS debitAmount
                                        FROM
                                            fin_transactions
                                            INNER JOIN fin_transactions_details ON fin_transactions.entryId = fin_transactions_details.entryId
                                        WHERE
                                            fin_transactions_details.accountId = $accountId AND
                                            fin_transactions_details.`credit/debit` = 'D' AND
                                            fin_transactions.entryDate <= '$theDate' AND
                                            fin_transactions.authorized = 1 AND
                                            fin_transactions.delStatus = 0 AND
                                            fin_transactions.currencyId = $curencyId AND
                                            fin_transactions.companyId = $companyId ".$accPeriodSql);
    $rowDebitTot=mysqli_fetch_array($resultDebitTot);
    
    $resultCreditTot = $db->RunQuery("SELECT
                                        SUM(fin_transactions_details.amount * fin_transactions.currencyRate) AS creditAmount
                                    FROM
                                        fin_transactions
                                        INNER JOIN fin_transactions_details ON fin_transactions.entryId = fin_transactions_details.entryId
                                    WHERE
                                        fin_transactions_details.accountId = $accountId AND
                                        fin_transactions_details.`credit/debit` = 'C' AND
                                        fin_transactions.entryDate <= '$theDate' AND
                                        fin_transactions.authorized = 1 AND
                                        fin_transactions.delStatus = 0 AND
                                        fin_transactions.currencyId = $curencyId AND
                                        fin_transactions.companyId = $companyId ".$accPeriodSql);
    $rowCreditTot=mysqli_fetch_array($resultCreditTot);
    $openingBalance=0;
    if ($strType=='D'){
        $openingBalance=($rowDebitTot['debitAmount']-$rowCreditTot['creditAmount']);
    }else{
        $openingBalance=($rowCreditTot['creditAmount'] - $rowDebitTot['debitAmount']);
    }
    $BalanceDetails['baseCurcencyBal']=$openingBalance;
    
    return $BalanceDetails;                      
}

//--------------------------------------------------------------------------------------------
function getLatestAccPeriod($companyId)
{
        global $db;
        $sql = "SELECT
                        MAX(mst_financeaccountingperiod.intId) AS accId,
                        mst_financeaccountingperiod.dtmStartingDate,
                        mst_financeaccountingperiod.dtmClosingDate,
                        mst_financeaccountingperiod.intStatus,
                        mst_financeaccountingperiod_companies.intCompanyId,
                        mst_financeaccountingperiod_companies.intPeriodId
                        FROM
                        mst_financeaccountingperiod
                        Inner Join mst_financeaccountingperiod_companies ON mst_financeaccountingperiod_companies.intPeriodId = mst_financeaccountingperiod.intId
                        WHERE
                        mst_financeaccountingperiod_companies.intCompanyId =  '$companyId' AND mst_financeaccountingperiod.intStatus = '1'
                        ORDER BY
                        mst_financeaccountingperiod.intId DESC
                        ";	
        $result = $db->RunQuery($sql);
        $row = mysqli_fetch_array($result);
        $latestAccPeriodId = $row['accId'];	
        return $latestAccPeriodId;
}
//--------------------------------------------------------------------------------------------
?>

<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$userId = $_SESSION['userId'];
$companyId = $_SESSION['headCompanyId'];
$locationId = $_SESSION['CompanyID'];
$thisFilePath = $_SERVER['PHP_SELF'];
include "{$backwardseperator}dataAccess/Connector.php";
$response = array('type' => '', 'msg' => '');
$requestType = $_REQUEST['requestType'];
if ($requestType == 'genEntres') {
    try {
        $recCount=0;
        $db->begin();
        $theDate = $_REQUEST['theDate'];
        $curSpotRates = $_REQUEST['curIds'];
        
        //==get base currency=============
        $result = $db->RunQuery2("SELECT com.intBaseCurrencyId FROM mst_companies AS com WHERE com.intId =  '$companyId'");
        $row=mysqli_fetch_array($result);
        $baseCurrencyId=$row['intBaseCurrencyId'];
        
        //add record to fin_unadjusted_gl_header
        $sqlHeader="INSERT INTO fin_unadjusted_gl_header (prosDate,type) VALUES('$theDate','Other')";
        $db->RunQuery2($sqlHeader);
        $prossId=$db->insertId;
        
        //get ledger accounts of type Account Receiveble
        $sqlAcc="SELECT
                CA.intId,CA.strCode,CA.strName, mst_financialsubtype.intFinancialMainTypeID as mainType, mst_financialsubtype.strType
            FROM
                mst_financechartofaccounts AS CA
                Inner Join mst_financialsubtype ON CA.intFinancialTypeId = mst_financialsubtype.intId
                Inner Join mst_financechartofaccounts_companies ON CA.intId = mst_financechartofaccounts_companies.intChartOfAccountId
            WHERE
                CA.strExchangeRateAdj =  'Adjustments' AND
                CA.strType =  'Posting' AND
                mst_financialsubtype.intId NOT IN  (10, 18) AND
                mst_financechartofaccounts_companies.intCompanyId = '$companyId'";
        $resultAcc = $db->RunQuery2($sqlAcc);        
        while($rowAcc=mysqli_fetch_array($resultAcc)){
            $accId=$rowAcc['intId'];
            $mainTypeId=$rowAcc['mainType'];
            $strType=$rowAcc['strType'];
            //get Currency List
            $sqlCur="SELECT
                    CUR.intId, CUR.strCode            
                FROM
                    mst_financecurrency AS CUR
                    Inner Join mst_financecurrencyactivate AS CA ON CUR.intId = CA.intCurrencyId
                WHERE
                    CUR.intStatus =  1 AND CA.intCompanyId =  '$companyId' AND CUR.intId <>  '$baseCurrencyId'";
            $resultCur= $db->RunQuery2($sqlCur);
            while($rowCur=mysqli_fetch_array($resultCur)){
                $curId=$rowCur['intId'];
                
                //get unrealized account for currency ===================================
                $sqlCurAc="SELECT CA.intUnrealizeGainId,CA.intUnrealizeLostId FROM mst_financecurrencyactivate AS CA WHERE CA.intCurrencyId =  '$curId' AND CA.intCompanyId =  '$companyId'";
                $resultCurAc= $db->RunQuery2($sqlCurAc);
                $rowCurAc=mysqli_fetch_array($resultCurAc);
                $unGainAc=$rowCurAc['intUnrealizeGainId'];
                $unLossAc=$rowCurAc['intUnrealizeLostId'];
                //=======================================
                //get Account Balance
                $accountPeriod = getLatestAccPeriod($companyId);
                $bal=getAcccountBalance($accId,$mainTypeId,$theDate,$curId,$companyId,$accountPeriod,$strType);                 
                $gainLoss=($bal['closingBal'] * $curSpotRates[$curId])-($bal['baseCurcencyBal']);
                
                $uglNumber=getUnadjustedGL($companyId,$locationId);                
                $uglReference	= trim(encodeReceiptNo($uglNumber,$accountPeriod,$companyId,$locationId)); 
                
                //add trnasactions
                if($gainLoss > 0){//Gain
                    //Add data to transaction header
                    $remarks='Unrealize Gain';
                    $sql="INSERT INTO fin_transactions (entryDate, strProgramType, documentNo, currencyId, currencyRate, transDetails, payMethodId, paymentNumber, accPeriod, userId, companyId, createdOn) VALUES
                        ('$theDate','Unrealize Gain-Loss','$uglReference',$baseCurrencyId,1,'$remarks',null,null,$accountPeriod,$userId,$companyId,now())";                
                    $db->RunQuery2($sql);
                    $entryId=$db->insertId;
                    //trna action details
                    $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                            ($entryId,'D',$accId,$gainLoss,'$remarks',null)";
                    $db->RunQuery2($sql);

                    $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                            ($entryId,'C',$unGainAc,$gainLoss,'$remarks',null)";
                    $db->RunQuery2($sql);

                    // insert in to fin_unadjusted_gl Table
                    $sql="INSERT INTO fin_unadjusted_gl (porcessId,RefNumber, entDate, personType, personId, invoiceNumber, invoiceType, entryId) VALUES
                            ($prossId,'$uglReference','$theDate','non',null,'non','non',$entryId)";
                    $db->RunQuery2($sql);
                    ++$recCount;
                }//Gain
                else if($gainLoss < 0){//Loss
                    //Add data to transaction header
                    $gainLoss=$gainLoss *(-1);
                    $remarks='Unrealize Loss ';
                    $sql="INSERT INTO fin_transactions (entryDate, strProgramType, documentNo, currencyId, currencyRate, transDetails, payMethodId, paymentNumber, accPeriod, userId, companyId, createdOn) VALUES
                        ('$theDate','Unrealize Gain-Loss','$uglReference',$baseCurrencyId,1,'$remarks',null,null,$accountPeriod,$userId,$companyId,now())";                
                    $db->RunQuery2($sql);
                    $entryId=$db->insertId;
                    //trna action details
                    $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                            ($entryId,'C',$accId,$gainLoss,'$remarks',null)";
                    $db->RunQuery2($sql);

                    $sql="INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                            ($entryId,'D',$unLossAc,$gainLoss,'$remarks',null)";
                    $db->RunQuery2($sql);

                    // insert in to fin_unadjusted_gl Table
                    $sql="INSERT INTO fin_unadjusted_gl (porcessId,RefNumber, entDate, personType, personId, invoiceNumber, invoiceType, entryId) VALUES
                            ($prossId,'$uglReference','$theDate','non',null,'non','non',$entryId)";
                    $db->RunQuery2($sql);
                    ++$recCount;
                }//Loss  
            }//Curency           
        }//account
        
        $response['type'] = 'pass';
        $response['msg'] = 'Saved successfully.';
        $response['reCcount'] = $recCount;
        $db->commit();
        echo json_encode($response);
    }catch (Exception $e){
        $db->rollback();//roalback        
        $response['type'] = 'fail';
        $response['msg'] = 'Error - '.$db->errormsg;
        $response['q'] = $sql;
        echo json_encode($response);
    }    
}

//Get account Balance to date
function getAcccountBalance($accountId,$mainTypeId,$theDate,$curencyId,$companyId,$accPeriod,$strType){      
    global $db;
    $BalanceDetails;
    //get Curreency balancy
    $accPeriodSql="";
    //get account period value for ' Manufacturing ' and 'Income Statements'
    if($mainTypeId==1 | $mainTypeId==2){
        $accPeriodSql=" AND fin_transactions.accPeriod='$accPeriod'";
    }
    else{// skip accounting period for 'Balance Sheet '
        $accPeriodSql=" ";
    }
    
    //get Data And calculate opening Balance
    $resultDebitTot = $db->RunQuery2("SELECT
                                            SUM(fin_transactions_details.amount) AS debitAmount
                                        FROM
                                            fin_transactions
                                            INNER JOIN fin_transactions_details ON fin_transactions.entryId = fin_transactions_details.entryId
                                        WHERE
                                            fin_transactions_details.accountId = $accountId AND
                                            fin_transactions_details.`credit/debit` = 'D' AND
                                            fin_transactions.entryDate <= '$theDate' AND
                                            fin_transactions.authorized = 1 AND
                                            fin_transactions.delStatus = 0 AND
                                            fin_transactions.currencyId = $curencyId AND
                                            fin_transactions.companyId = $companyId ".$accPeriodSql);    
    
    $rowDebitTot=mysqli_fetch_array($resultDebitTot);
    
    $resultCreditTot = $db->RunQuery2("SELECT
                                        SUM(fin_transactions_details.amount) AS creditAmount
                                    FROM
                                        fin_transactions
                                        INNER JOIN fin_transactions_details ON fin_transactions.entryId = fin_transactions_details.entryId
                                    WHERE
                                        fin_transactions_details.accountId = $accountId AND
                                        fin_transactions_details.`credit/debit` = 'C' AND
                                        fin_transactions.entryDate <='$theDate' AND
                                        fin_transactions.authorized = 1 AND
                                        fin_transactions.delStatus = 0 AND
                                        fin_transactions.currencyId = $curencyId AND
                                        fin_transactions.companyId = $companyId ".$accPeriodSql);
    $rowCreditTot=mysqli_fetch_array($resultCreditTot);
    $openingBalance=0;
    if ($strType=='D'){
        $openingBalance=($rowDebitTot['debitAmount']-$rowCreditTot['creditAmount']);
    }else{
        $openingBalance=($rowCreditTot['creditAmount'] - $rowDebitTot['debitAmount']);
    }
    $BalanceDetails['closingBal']=$openingBalance;
    
    //get Base currency Balance=======================
    //get Data And calculate opening Balance
    $resultDebitTot = $db->RunQuery2("SELECT
                                            SUM(fin_transactions_details.amount * fin_transactions.currencyRate) AS debitAmount
                                        FROM
                                            fin_transactions
                                            INNER JOIN fin_transactions_details ON fin_transactions.entryId = fin_transactions_details.entryId
                                        WHERE
                                            fin_transactions_details.accountId = $accountId AND
                                            fin_transactions_details.`credit/debit` = 'D' AND
                                            fin_transactions.entryDate <= '$theDate' AND
                                            fin_transactions.authorized = 1 AND
                                            fin_transactions.delStatus = 0 AND
                                            fin_transactions.currencyId = $curencyId AND
                                            fin_transactions.companyId = $companyId ".$accPeriodSql);
    $rowDebitTot=mysqli_fetch_array($resultDebitTot);
    
    $resultCreditTot = $db->RunQuery2("SELECT
                                        SUM(fin_transactions_details.amount * fin_transactions.currencyRate) AS creditAmount
                                    FROM
                                        fin_transactions
                                        INNER JOIN fin_transactions_details ON fin_transactions.entryId = fin_transactions_details.entryId
                                    WHERE
                                        fin_transactions_details.accountId = $accountId AND
                                        fin_transactions_details.`credit/debit` = 'C' AND
                                        fin_transactions.entryDate <= '$theDate' AND
                                        fin_transactions.authorized = 1 AND
                                        fin_transactions.delStatus = 0 AND
                                        fin_transactions.currencyId = $curencyId AND
                                        fin_transactions.companyId = $companyId ".$accPeriodSql);
    $rowCreditTot=mysqli_fetch_array($resultCreditTot);
    $openingBalance=0;
    if ($strType=='D'){
        $openingBalance=($rowDebitTot['debitAmount']-$rowCreditTot['creditAmount']);
    }else{
        $openingBalance=($rowCreditTot['creditAmount'] - $rowDebitTot['debitAmount']);
    }
    $BalanceDetails['baseCurcencyBal']=$openingBalance;
    
    return $BalanceDetails;                      
}
//--------------------------------------------------------------------------------------------
function getUnadjustedGL($companyId,$locationId)
{
    global $db;
    $sql = "SELECT intUnadjustedGL FROM sys_finance_no WHERE intCompanyId = '$companyId' AND intLocationId = '$locationId'";	
    $result = $db->RunQuery2($sql);    
    $row = mysqli_fetch_array($result);
    $nextReceiptNo = $row['intUnadjustedGL'];
    if(mysqli_num_rows($result)<=0 || $nextReceiptNo==null || $nextReceiptNo==0){
        $nextReceiptNo=1000001;
    }
    $sql = "UPDATE `sys_finance_no` SET intUnadjustedGL=($nextReceiptNo+1) WHERE (intCompanyId = '$companyId' AND intLocationId = '$locationId')";
    $db->RunQuery2($sql);	
    return $nextReceiptNo;
}
//--------------------------------------------------------------------------------------------
function getLatestAccPeriod($companyId)
{
        global $db;
        $sql = "SELECT
                        MAX(mst_financeaccountingperiod.intId) AS accId,
                        mst_financeaccountingperiod.dtmStartingDate,
                        mst_financeaccountingperiod.dtmClosingDate,
                        mst_financeaccountingperiod.intStatus,
                        mst_financeaccountingperiod_companies.intCompanyId,
                        mst_financeaccountingperiod_companies.intPeriodId
                        FROM
                        mst_financeaccountingperiod
                        Inner Join mst_financeaccountingperiod_companies ON mst_financeaccountingperiod_companies.intPeriodId = mst_financeaccountingperiod.intId
                        WHERE
                        mst_financeaccountingperiod_companies.intCompanyId =  '$companyId' AND mst_financeaccountingperiod.intStatus = '1'
                        ORDER BY
                        mst_financeaccountingperiod.intId DESC
                        ";	
        $result = $db->RunQuery2($sql);
        $row = mysqli_fetch_array($result);
        $latestAccPeriodId = $row['accId'];	
        return $latestAccPeriodId;
}
//--------------------------------------------------------------------------------------------
//=========================================================================================
function encodeReceiptNo($receiptNumber,$accountPeriod,$companyId,$locationId)
{
        global $db;
        $sql = "SELECT
                        mst_financeaccountingperiod.intId,
                        mst_financeaccountingperiod.dtmStartingDate,
                        mst_financeaccountingperiod.dtmClosingDate,
                        mst_financeaccountingperiod.intStatus
                        FROM
                        mst_financeaccountingperiod
                        WHERE
                        mst_financeaccountingperiod.intId =  '$accountPeriod'
                        ";	
        $result = $db->RunQuery2($sql);
        $row = mysqli_fetch_array($result);
        $startDate = substr($row['dtmStartingDate'],0,4);
        $closeDate = substr($row['dtmClosingDate'],0,4);
        $sql = "SELECT
                        mst_companies.strCode AS company,
                        mst_companies.intId,
                        mst_locations.intCompanyId,
                        mst_locations.strCode AS location,
                        mst_locations.intId
                        FROM
                        mst_companies
                        Inner Join mst_locations ON mst_locations.intCompanyId = mst_companies.intId
                        WHERE
                        mst_locations.intId =  '$locationId' AND
                        mst_companies.intId =  '$companyId'
                        ";
        $result = $db->RunQuery2($sql);
        $row = mysqli_fetch_array($result);
        $companyCode = $row['company'];
        $locationCode = $row['location'];
        $receiptFormat = $companyCode."/".$locationCode."/".$startDate."-".$closeDate."/".$receiptNumber;
        //$receiptFormat = $companyCode."/".$startDate."-".$closeDate."/".$receiptNumber;
        return $receiptFormat;
}
//============================================================================================
?>

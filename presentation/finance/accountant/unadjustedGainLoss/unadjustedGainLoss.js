$(document).ready(function() {     
     $('#frmUnadjustedGL #txtDate').blur(function(){         
         var theDate=$('#frmUnadjustedGL #txtDate').val();
         var url ='unadjustedGainLoss-db-get.php?requestType=loadCurrency&theDate='+theDate;
            var obj=$.ajax({
                url:url,
                dataType:'json',
                success:function(json){
                    $('#frmUnadjustedGL #curList').html(json.curList);
                    $('#frmUnadjustedGL #btnDisplay').attr("class","show");
                    
                },
                async:false
            });          
     });
     $('#frmUnadjustedGL #btnDisplay').click(function(){ 
         $('#divMaskProces').attr("class","maskShow mask");
         var theDate=$('#frmUnadjustedGL #txtDate').val();
         var data="&theDate="+theDate;
         var curIds=document.getElementsByName("txtCurId");
         for(var i=0;curIds.length > i;++i){
             var curId=curIds[i].value;
             var radios=document.getElementsByName("radio"+curId);
             for(var j=0;radios.length > j;++j){
                if(radios[j].checked){
                    var val=radios[j].value;                 
                    if(val=="bui"){data+="&curIds["+curId+"]="+  $('#frmUnadjustedGL #txtbuing'+curId).val();}
                    else if (val=="sel"){data+="&curIds["+curId+"]="+  $('#frmUnadjustedGL #txtseling'+curId).val();}   
                    else if (val=="oth"){data+="&curIds["+curId+"]="+  $('#frmUnadjustedGL #txtother'+curId).val();}  
                }
             }
         }
         var url ='unadjustedGainLoss-db-get.php?requestType=loadValues'+data;
            var obj=$.ajax({
                url:url,
                dataType:'json',
                success:function(json){                    
                    $('#frmUnadjustedGL #valueList').html(json.valueList);
                    $('#frmUnadjustedGL #btnGenerate').attr("class","show");
                    $('#divMaskProces').attr("class","maskHide mask"); 
                    $('#frmUnadjustedGL #btnGenerate').attr("disabled",false);
                },
                async:false
            });          
     });  
     
     //generate value
     $('#frmUnadjustedGL #btnGenerate').click(function(){ 
         
         $('#divMaskProces').attr("class","maskShow mask");
         
         var theDate=$('#frmUnadjustedGL #txtDate').val();
         var data="&theDate="+theDate;
         var curIds=document.getElementsByName("txtCurId");
         for(var i=0;curIds.length > i;++i){
             var curId=curIds[i].value;
             var radios=document.getElementsByName("radio"+curId);
             for(var j=0;radios.length > j;++j){
                if(radios[j].checked){
                    var val=radios[j].value;                 
                    if(val=="bui"){data+="&curIds["+curId+"]="+  $('#frmUnadjustedGL #txtbuing'+curId).val();}
                    else if (val=="sel"){data+="&curIds["+curId+"]="+  $('#frmUnadjustedGL #txtseling'+curId).val();}   
                    else if (val=="oth"){data+="&curIds["+curId+"]="+  $('#frmUnadjustedGL #txtother'+curId).val();}  
                }
             }
         }
         var url ='unadjustedGainLoss-db-set.php?requestType=genEntres'+data;
            var obj=$.ajax({
                url:url,
                dataType:'json',
                success:function(json){
                    $('#divMaskProces').attr("class","maskHide mask");
                    $('#frmUnadjustedGL #btnGenerate').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
                    if(json.type=='pass')
                    {
                        $('#frmUnadjustedGL #btnGenerate').attr("disabled",true);
                        var count=json.reCcount;
                        var dis="<tr class='normalfntMid'><td colspan="+11+">"+ count +" Records Added</td></tr>";
                        $('#frmUnadjustedGL #valueList').html(dis);
                    }
                    var t=setTimeout("alertx()",3000);
                    
                },            
                error:function(xhr,status){
                    $('#divMaskProces').attr("class","maskHide mask");
                    $('#frmUnadjustedGL #btnGenerate').validationEngine('showPrompt', errormsg(xhr.status),'fail');
                    var t=setTimeout("alertx()",3000);
                }
            });          
     });        
     
    
});
function alertx()
{
	$('#frmUnadjustedGL #btnGenerate').validationEngine('hide')	;
}


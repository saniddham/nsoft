<?php	
	session_start();
	$backwardseperator = "../../../../../";
	$companyId = $_SESSION['CompanyID'];
	$thisFilePath =  $_SERVER['PHP_SELF'];
	
	include  "{$backwardseperator}dataAccess/permisionCheck.inc";
	$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$companyId = $row['intCompanyId']; 
	}
	if($_SERVER['REQUEST_METHOD'] == "POST")
	{
		$optionValue = trim($_REQUEST['cboServicePro']);
		//$searchValue = trim($_REQUEST['txtSearchValue']);
	}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Service Provider Listing</title>

<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../../css/tblstyle.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../../libraries/jquery/jquery-1.3.2.min.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/scrolltable.js"></script>

<script type="application/javascript" src="../../../../../libraries/javascript/zebraStripe.js"></script>

<script type="text/javascript" language="javascript">

function pageSubmit()
{
	document.getElementById('frmSerProviderListing').submit();	
}
function clearValue()
{
	document.getElementById('txtSearchValue').value = '';
	document.getElementById('txtSearchValue').focus();
}
</script>

</head>

<body>
<form id="frmSerProviderListing" name="frmSerProviderListing" method="post" autocomplete="off" action="serviceProviderListing.php">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>


<div align="center">
<div class="trans_layoutL">
<div class="trans_text">Service Provider Listing</div>
<table width="804">
    <tr>
      <td width="796"><table width="100%" border="0" class="tableBorder_allRound">
      <tr>
        <td width="16%" height="22" class="normalfnt">&nbsp;</td>
    	<td width="18%" class="normalfntMid">Service Provider</td>
    	 
         <td bgcolor="#FFFFFF" class=""><span class="normalfntMid">      
     		 <select name="cboServicePro" id="cboServicePro" style="width:180px">
       			<option value=""></option>
       			 <?php  $sql = "SELECT intProviderId, strProviderName
							FROM mst_financeserviceprovider
								ORDER BY strProviderName ASC";
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intProviderId']."\">".$row['strProviderName']."</option>";
								}
       			 ?>
      		</select>
    </span></td>
           
            <td width="18%" class="normalfnt">
            <a href="#">
            <img src="../../../../../images/search.png" width="20" height="20" onclick="pageSubmit();" /></a></td>
            <td width="15%">&nbsp;</td>
            </tr>
          	
            <td height="22" class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            
     </table></td>
          </tr>
          
      <tr>
          <td>
          
            <div id="_table_wrap" style="overflow: hidden; display: inline-block; border: 1px solid rgb(136, 136, 136);">
            
            <div id="_head_wrap" style="width: 800px; overflow: hidden;">
              <table class="tbl1 scroll" id="_head" style="table-layout: fixed;">
              <thead>
                <tr class="gridHeader">
                  <th height="22" style="width: 136px;" ><strong>Provider Name</strong></th>
                  <th style="width: 100px;" ><strong>Address</strong></th>
                  <th style="width: 100px;" ><strong>Telephone</strong></th>
                  <th style="width: 98px;" ><strong>Contact Person</strong></th>
                  <th style="width: 40px;" ><strong>Blocked</strong></th>
                  <th style="width: 40px;" >View</th>
                  <th style="width: 10px;" ></th>
                  </tr>
               </table>
           	</div>            
            
             <div class="normalfnt" id="_body_wrap" style="width: 800px; height: 310px; overflow: scroll;">      
          
           <table class="tbl1 scroll"> 
           <tbody>
              <?php		  	
				if($optionValue!='')
				$wherePart = "WHERE mst_financeserviceprovider.intProviderId =  $optionValue";
				
	 	 		 $sql = "SELECT
							mst_financeserviceprovider.intProviderId AS proId,
							mst_financeserviceprovider.strProviderName,
							mst_financeserviceprovider.strAddress,
							mst_financeserviceprovider.intTelNo,
							mst_financeserviceprovider.strContactPerson,
							mst_financeserviceprovider.intBlocked
						FROM
							mst_financeserviceprovider
							
							$wherePart";
				 $result = $db->RunQuery($sql);
				 
				 while($row=mysqli_fetch_array($result))
				 {
					$pid 		=	 $row['proId'];
	  			 ?>
           		<tr class="normalfnt">
           		<td class="proName" height="16" align="center" bgcolor="#FFFFFF">
		   		<?php echo $row['strProviderName'];?></td>
              	<td class="address"  align="center" bgcolor="#FFFFFF"><?php echo $row['strAddress'];?></td>
              	<td class="telNo" bgcolor="#FFFFFF"><?php echo $row['intTelNo'];?></td>
              	<td class="contactPer" align="center" bgcolor="#FFFFFF"><?php echo $row['strContactPerson'];?></td>
              	<td class="blocked" align="center" bgcolor="#FFFFFF">
              	  <input disabled="disabled" <?php echo($row['intBlocked']?'checked':''); ?> type="checkbox"/></td>
              	<td class="view" align="center" bgcolor="#FFFFFF"><a target="_blank" href="<?php echo "../addNew/serviceProvider.php?id=$pid";?>">More</a></td>
              </tr>
               <?php 
        	    } 
       		   ?>
              </tbody>
             </table>
            </div>      
          
          </div>
          </td></tr>
          </table>
          
          
   </td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound"><a href="../../../../../main.php"><img  src="../../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
      </tr>
    </table></td>
    </tr>
  </table>
</div>
</div>
</form>
</body>
</html>
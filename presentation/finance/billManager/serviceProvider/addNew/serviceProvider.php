<?php	
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath = $_SESSION['mainPath'];
	
	$thisFilePath =  $_SERVER['PHP_SELF'];
	
	//echo realpath('./../../etc/passwd');
	include  "{$backwardseperator}dataAccess/permisionCheck.inc";
	
	$proId = $_REQUEST['id']; 
?>

<script type="application/javascript" >
var pId = <?php echo $proId ?>;
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Service Provider</title>
<link href="../../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../../css/promt.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="serviceProvider-js.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" href="../../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../../libraries/validate/template.css" type="text/css">

</head>

<body onLoad="functionList();">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<script src="../../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<form id="frmServiceProvider" name="frmServiceProvider" method="post" action="customer-db-set.php" autocomplete="off"  >
<div align="center">
  <div class="trans_layoutD">
	<div class="trans_text">Service Provider</div>
<table width="100%">
<tr>
  <td width="100%"><table width="100%" class="tableBorder">
    <tr>
      <td bgcolor="#F7F7F7" class="normalfnt">&nbsp;</td>
      <td bgcolor="#F7F7F7" class="normalfnt">Provider</td>
      <td colspan="2" bgcolor="#F7F7F7" class="normalfnt"
      ><select  disabled="disabled" name="cboSearch" class="txtbox" id="cboSearch"  style="width:250px;"  >
        <option value=""></option>
        <?php  $sql = "SELECT intProviderId, strProviderName
							FROM mst_financeserviceprovider
								ORDER BY strProviderName ASC";
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intProviderId']."\">".$row['strProviderName']."</option>";
								}
                   ?>
      </select></td>
      <td bgcolor="#F7F7F7" class="">&nbsp;</td>
      <td bgcolor="#F7F7F7" class="">&nbsp;</td>
    </tr>
    <tr>
      <td width="21" bgcolor="#F7F7F7" class="normalfnt">&nbsp;</td>
      <td width="113" bgcolor="#F7F7F7" class="normalfnt">Provider  Name <span class="compulsoryRed">*</span></td>
      <td width="137" bgcolor="#F7F7F7" class="normalfnt"><input  class="validate[required]" type="text" name="proName" id="proName" style="width:125px" /></td>
      <td width="95" bgcolor="#F7F7F7" class=""><span class="normalfnt">Address</span></td>
      <td width="180" bgcolor="#F7F7F7" class=""><textarea name="address" id="address" style="width:180px"></textarea></td>
      <td width="18" bgcolor="#F7F7F7" class="">&nbsp;</td>
      </tr>
    <tr>
      <td class="normalfnt">&nbsp;</td>
      <td height="21" class="normalfnt">Telephone</td>
      <td class="normalfnt"><input type="text" name="telNo" id="telNo" style="width:125px" /></td>
      <td bgcolor="#FFFFFF" class=""><span class="normalfnt">Email</span></td>
      <td bgcolor="#FFFFFF" class=""><span class="normalfnt">
        <input type="text" name="email" id="email" style="width:175px" />
      </span></td>
      <td bgcolor="#FFFFFF" class="">&nbsp;</td>
      </tr>
    <tr>
      <td bgcolor="#F7F7F7" class="normalfnt">&nbsp;</td>
      <td height="21" bgcolor="#F7F7F7" class="normalfnt">Fax</td>
      <td bgcolor="#F7F7F7" class="normalfnt"><input type="text" name="fax" id="fax" style="width:125px" /></td>
      <td bgcolor="#F7F7F7" class="normalfnt">Contact Person</td>
      <td bgcolor="#F7F7F7" class=""><span class="normalfnt">
        <input type="text" name="contPerson" id="contPerson" style="width:175px" />
      </span></td>
      <td bgcolor="#F7F7F7" class="">&nbsp;</td>
    </tr>
    <tr>
      <td class="normalfnt">&nbsp;</td>
      <td height="21" class="normalfnt">Credit Limit</td>
      <td class="normalfnt"><input type="text" name="crLimit" id="crLimit" style="width:100px" /></td>
      <td bgcolor="#FFFFFF" class=""><span class="normalfnt">VAT Number</span></td>
      <td bgcolor="#FFFFFF" class=""><span class="normalfnt">
        <input type="text" name="vatNo" id="vatNo" style="width:125px" />
      </span></td>
      <td bgcolor="#FFFFFF" class="">&nbsp;</td>
    </tr>
    <tr>
      <td bgcolor="#F7F7F7" class="normalfnt">&nbsp;</td>
      <td height="21" bgcolor="#F7F7F7" class="normalfnt">SVAT Number</td>
      <td bgcolor="#F7F7F7" class="normalfnt"><input type="text" name="svatNo" id="svatNo" style="width:125px" /></td>
      <td bgcolor="#F7F7F7" class="normalfnt">Blocked</td>
      <td bgcolor="#F7F7F7" class=""><span class="normalfnt">
      <input name="cbBlocked" type="checkbox" id="cbBlocked"/>     
       
      </span></td>
      <td bgcolor="#F7F7F7" class="">&nbsp;</td>
    </tr>
    <tr>
      <td bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
      <td height="21" bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
      <td bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
      <td bgcolor="#FFFFFF" class="normalfnt">&nbsp;</td>
      <td bgcolor="#FFFFFF" class="">&nbsp;</td>
      <td bgcolor="#FFFFFF" class="">&nbsp;</td>
    </tr>
  </table></td>
</tr>
<tr>
  <td align="center">
  <table width="100%" class="tableBorder_allRound">
  <tr>
  <td align="center">
  <td width="100%" align="center" bgcolor=""><img style="display:none" border="0" src="../../../../../images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:none" border="0" src="../../../../../images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/><img style="display:none" border="0" src="../../../../../images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="25"/><a href="../../../../../main.php"><img  src="../../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
  </td>
  </tr>
  </table>
  </td>
</tr>
<tr>
  <td align="center">&nbsp;</td>
</tr>
    </table>
</div>
</div>
</form>   
</body>
</html>
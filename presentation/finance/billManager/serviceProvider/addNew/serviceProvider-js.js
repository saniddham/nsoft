function functionList()
{
	if(pId!='')
	{
		$('#frmServiceProvider #cboSearch').val(pId);
		$('#frmServiceProvider #cboSearch').change();
	}
}

$(document).ready(function() {
  		$("#frmServiceProvider").validationEngine();
		$('#frmServiceProvider #proName').focus();
  //permision for add 
  if(intAddx)
  {
 	$('#frmServiceProvider #butNew').show();
	$('#frmServiceProvider #butSave').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmServiceProvider #butSave').show();
	$('#frmServiceProvider #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmServiceProvider #butDelete').show();
	$('#frmServiceProvider #cboSearch').removeAttr('disabled');
  }
  
  //permision for view
  if(intViewx)
  {
	$('#frmServiceProvider #cboSearch').removeAttr('disabled');
  }
  
  $('#butSave').click(function(){
	  if ($('#frmServiceProvider').validationEngine('validate'))   
	  {
		if($("#frmServiceProvider #cboSearch").val()=='')
			requestType = 'add';
		else
			requestType = 'edit';
		
		var url = "serviceProvider-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json",  
			data:$("#frmServiceProvider").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addLocation"}',
			async:false,

			success:function(json){
					$('#frmServiceProvider #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmServiceProvider').get(0).reset();
						loadCombo_frmServiceProvider();						
						var t=setTimeout("alertx()",1000);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmServiceProvider #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			 });
	  }
  });
  
/*  $(':input').bind('keyup mouseup', function(){
      $(this).val($.trim($(this).val())) ;
    });*/

  ///save button click event
 /* $('#frmServiceProvider #butSave').click(function(){
	//$('#frmServiceProvider').submit();
	var requestType = '';
	if ($('#frmServiceProvider').validationEngine('validate'))   
    { 
		//$('#txtLocationCode').validationEngine('showPrompt', 'This is an example', 'pass'); //hide all alerts//
		if($("#frmServiceProvider #cboSearch").val()=='')
			requestType = 'add';
		else
			requestType = 'edit';
		
		var url = "serviceProvider-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json",  
			data:$("#frmServiceProvider").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addLocation"}',
			async:false,
			
			success:function(json){
					$('#frmServiceProvider #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*///);
					/*if(json.type=='pass')
					{
						$('#frmServiceProvider').get(0).reset();
						loadCombo_frmServiceProvider();
						var t=setTimeout("alertx()",1000);
						location.reload(true);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmServiceProvider #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
   */
   /////////////////////////////////////////////////////
   //// load Service Provider details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmServiceProvider #cboSearch').click(function(){
	   $('#frmServiceProvider').validationEngine('hide');
   });
    $('#frmServiceProvider #cboSearch').change(function(){
		$('#frmServiceProvider').validationEngine('hide');
		var url = "serviceProvider-db-get.php";
		if($('#frmServiceProvider #cboSearch').val()=='')
		{
			$('#frmServiceProvider').get(0).reset();return;	
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:'requestType=loadDetails&id='+$(this).val(),
			async:false,
			success:function(json){
					//json  = eval('('+json+')');
					$('#frmServiceProvider #proName').val(json.proName);
					$('#frmServiceProvider #address').val(json.address);
					$('#frmServiceProvider #telNo').val(json.telNo);
					$('#frmServiceProvider #email').val(json.eMail);
					$('#frmServiceProvider #fax').val(json.fax);
					$('#frmServiceProvider #contPerson').val(json.conPerson);
					$('#frmServiceProvider #crLimit').val(json.crLimit);
					$('#frmServiceProvider #vatNo').val(json.vatNo);
					$('#frmServiceProvider #svatNo').val(json.svatNo);				
					$('#frmServiceProvider #cbBlocked').attr('checked',json.blocked);				
			}
	});
	//////////// end of load details /////////////////
	
	});
	
	$('#frmServiceProvider #butNew').click(function(){
		$('#frmServiceProvider').get(0).reset();
		location.reload(true);
		loadCombo_frmServiceProvider();
		$('#frmServiceProvider #txtCode').focus();
	});
    $('#frmServiceProvider #butDelete').click(function(){
		if($('#frmServiceProvider #cboSearch').val()=='')
		{
			$('#frmServiceProvider #butDelete').validationEngine('showPrompt', 'Please select Service Provider.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmServiceProvider #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = "serviceProvider-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											data:'requestType=delete&cboSearch='+$('#frmServiceProvider #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmServiceProvider #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													$('#frmServiceProvider').get(0).reset();
													loadCombo_frmServiceProvider();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
});


function loadCombo_frmServiceProvider()
{
	var url 	= "serviceProvider-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmServiceProvider #cboSearch').html(httpobj.responseText);
}

function alertx()
{
	$('#frmServiceProvider #butSave').validationEngine('hide');
}
function alertDelete()
{
	$('#frmServiceProvider #butDelete').validationEngine('hide');
}

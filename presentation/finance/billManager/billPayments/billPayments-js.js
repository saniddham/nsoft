// JavaScript Document
var toBePaidAmount = '';

$(document).ready(function() {	
		var id = '';
		var taxCode = '';
  		$("#frmBillPayment").validationEngine();
		$('#frmBillPayment #txtPayAmt').val('0.00');
		//$('#frmBillPayment #txtCatName').focus();
		  //permision for add 
	  if(intAddx)
	  {		 
		$('#frmBillPayment #butNew').show();
		$('#frmBillPayment #butSave').show();
	  }
  
	  //permision for edit 
	  if(intEditx)
	  {
		$('#frmBillPayment #butSave').show();
		//$('#frmBillPayment #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
	  }
  
  //permision for delete
	  if(intDeletex)
	  {		  
		$('#frmBillPayment #butDelete').show();
		//$('#frmBillPayment #cboSearch').removeAttr('disabled');
	  }
  
  //permision for view
	  if(intViewx)
	  {
		//$('#frmBillPayment #cboSearch').removeAttr('disabled');
	  } 
	    
	  // Load grid when click checkbox
	   $('.checkCategory').click(function(){
		  
		 LoadCategoryDetails();
	  });

// ---------------Edit Rate -------------------
	  $('#frmBillPayment #chkEdit').click(function(){
		  if($('#frmBillPayment #chkEdit').attr('checked'))
		  {
			  $("#frmBillPayment #txtRate").attr("readonly","");
			  $('#frmBillPayment #txtRate').focus();
		  }
		  else
		  {
			  $('#frmBillPayment #txtRate').val('');
			  $("#frmBillPayment #txtRate").attr("readonly","readonly");
			  $('#frmBillPayment #cboCurrency').change();
		  }
	  }); 
	  
// -------------Button New  ------------------
		$('#frmBillPayment #butNew').click(function(){
		$('#frmBillPayment').get(0).reset();
		clearGridRowContent();
		loadCombo_frmBillPayment();
		
		$('#frmBillPayment #cboSearch').focus();
		document.getElementById("rwChequeDetails").style.display='none';
		});
	
//---------------Change pay amount--------------------------

/*$('.payAmount').live('change',function(){
	alert("aaaaaaa");
	 var amount=0;
	 
	amount = $('#frmBillPayment #txtTotPaying').val();
	alert(amount);
	$('#frmBillPayment #txtPayAmt').val(amount);
	
	}); */	


//---------------- delete button click vent-----------

 $('#frmBillPayment #butDelete').click(function(){
	 
	var VouchNo=document.getElementById("cboSearchVoucher").value; 	
	//var serviceProNo=document.getElementById("txtServicePro").value; 
	//alert(serviceProNo);
    var requestType = 'delete';
	 //------------------------------------
	 
		if($('#frmBillPayment #cboSearchVoucher').val()=='')
		{
			$('#frmBillPayment #butDelete').validationEngine('showPrompt', 'Please select Voucher.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmBillPayment #cboSearchVoucher option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = "billPayments-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											//data:'requestType=delete&cboSearch='+$('#frmBillPayment #cboSearchVoucher').val(),
											data:$("#frmBillPayment").serialize()+'&requestType='+requestType+'&cboSearch='+VouchNo,											
											
											async:false,
											success:function(json){
												$('#frmBillPayment #butDelete').validationEngine('showPrompt', json.msg,json.type);
												if(json.type=='pass')
												{
												//	alert(json.msg)
													$('#frmBillPayment').get(0).reset();
												    clearGridRowContent();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
								}
		 	});
			
		}
	});

//---------------- save button click vent-----------
//save button click event
$('#frmBillPayment #butSave').click(function(){
//-------------------------------------------------------------------
	var docuNo = "";
	var invoiceNo = "";
	var lastPayDate = "";	
	var amount = "";
	var toBePaid = "";	
	var payAmount = "";
value="[ ";
	$('#tblMainGrid3 tr:not(:first)').each(function(){
			
			docuNo		= $(this).find(".DocNo").attr('id'); // id in js grid				
			invoiceNo	= $(this).find(".invoiceNo").html();						
			lastPayDate = $(this).find(".lPDate").html();					
			amount 		= $(this).find(".amount").val();					
			toBePaid 	= $(this).find(".payingAmount").val();				
			payAmount 	= $(this).find(".paidAmount").val();	
					
								
		value += '{"docuNo":"'+docuNo+'","invoiceNo":"'+invoiceNo+'", "lastPayDate": "'+lastPayDate+'", "amount": "'+amount+'", "toBePaid": "'+toBePaid+'", "payAmount": "'+payAmount+'"},';
	
	});
	
	value = value.substr(0,value.length-1);
	value += " ]";
	
//---------------------------------------------------------------------------
	var requestType = '';
	if ($('#frmBillPayment').validationEngine('validate'))
    {		
		if((eval($('#txtPayAmt').val()) - eval($('#txtTotPaying').val()))==0)
		{
			if($('#cboSearchVoucher').val()=='')
				requestType = 'add';
				
			else
				requestType = 'edit';
			
			var url = "billPayments-db-set.php";
			var obj = $.ajax({
				url:url,
				dataType: "json",  
				data:$("#frmBillPayment").serialize()+'&requestType='+requestType+'&recPayDetail='+value,
				async:false,
				
				success:function(json){
						$('#frmBillPayment #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
						if(json.type=='pass')
						{
							//$('#frmReceivedPayments').get(0).reset(); hideWaitng();							
							var t=setTimeout("alertx()",1000);
							$('#frmBillPayment').get(0).reset(); // Clear values
							clearGridRowContent();
							$('#txtVoucherNo').val(json.voucherNo);//Dispaly Voucher No
							
							return;
						}
						var t=setTimeout("alertx()",3000);
					},
				error:function(xhr,status){
						
						$('#frmBillPayment #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						var t=setTimeout("alertx()",3000);
					}		
				});
		}
		else
		{
			$('#frmBillPayment #butSave').validationEngine('showPrompt', 'You cannot allow this process untill pay amount and total paying amount are same','fail');
			var t=setTimeout("alertx()",5000);
		}
	}
});

  
//---------get  To be paid amount--------------- 

	$('.paidAmount').live('keyup',function(){
		calToBePaid(this);
		
		callTotalAmount();		
		
		amount = $('#frmBillPayment #txtTotPaying').val();
		
		$('#frmBillPayment #txtPayAmt').val(amount);
	});	
	
//--------------hide reference details-----------------------
 
 $('#frmBillPayment #cboPaymentMethod').change(function(){
	    var payMethod = $('#cboPaymentMethod').val();
		if(payMethod==2){
		document.getElementById("rwChequeDetails").style.display='';
		}
		else{
		document.getElementById("rwChequeDetails").style.display='none';
		}
  });
  
  // load voucher details according to reference no 
   
   $('#frmBillPayment #cboSearchVoucher').click(function(){
	   $('#frmBillPayment').validationEngine('hide');
   });

});

 //=====================end of ready================================


function functionVoucherDetails()
{	
	//var billRefNo=document.getElementById("cboSearchVoucher").value;
	//var billRefNo = $('#frmBillPayment #cboSearchVoucher').val();	
	
	var voucherRefNo = $('#frmBillPayment #cboSearchVoucher').val();	
	if(voucherRefNo!='')
	{
		loadVouchergDetails(voucherRefNo);
	}
}
//-------------------------------------------------------------------------------

 function loadVouchergDetails(voucherRefNo){
		$('#frmBillPayment').validationEngine('hide');
		var url = "billPayments-db-get.php"; 
		if($('#frmBillPayment #cboSearchVoucher').val()=='')
		{
			$('#frmBillPayment').get(0).reset();return;	
		}
	
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:'requestType=loadDetails&voucherRefNo='+voucherRefNo,
			async:false,
			success:function(json){
				
				$('#frmBillPayment #txtVoucherNo').val(json.referenceNo);
				$('#frmBillPayment #txtDate').val(json.date);
				$('#frmBillPayment #cboCurrency').val(json.currencyId);
				$('#frmBillPayment #txtRate').val(json.rate);
				$('#frmBillPayment #txtPayAmt').val(json.payAmount);
				$('#frmBillPayment #txtPayTo').val(json.payTo);
				$('#frmBillPayment #cboPaymentMethod').val(json.payMethodId);					
				$('#frmBillPayment #txtRefNo').val(json.payRefNo);
				//$('#frmBillPayment #txtRefDate').val(json.refDate);
				//$('#frmBillPayment #chbPosted').attr('checked',json.isPosted);	
				//$('#frmBillPayment #txtRefOrg').val(json.refOrg);
				$('#frmBillPayment #txtRemarks').val(json.remarks);	
				
				$('#frmBillPayment #cmbCreditAcc').val(json.crAcc);	
				$('#frmBillPayment #cmbDebitAcc').val(json.drAcc);	
				$('#frmBillPayment #txtTotPaid').val(json.toBePaid);	
				$('#frmBillPayment #txtTotPaying').val(json.PayAmt);							
		//---------------------------------------------------------	
		if(json.payMethodId==2)
		{
			document.getElementById("rwChequeDetails").style.display='';
			
			$('#frmBillPayment #txtRefDate').val(json.refDate);
			$('#frmBillPayment #chbPosted').attr('checked',json.isPosted);	
			$('#frmBillPayment #txtRefOrg').val(json.refOrg);
		}
		else
		{
			document.getElementById("rwChequeDetails").style.display='none';
		}
			
		//-------------------------------------------------------
		$('#tblMainGrid3 >tbody >tr').each(function(){
			if($(this).index()!=0 )
			{
				$(this).remove();
			}
		});
		var docuNo 		= "";
		var invoiceNo 	= "";
		var lastPDate 	= "";
		var amount		= "";		
		var toBePaid 	= "";
		var payingAmt 	= "";
		
		if(json.detailVal!=null)
		{
			var rowId = $('#tblMainGrid3').find('tr').length;
			var tbl = document.getElementById('tblMainGrid3');
			rows = $('#tblMainGrid3').find('tr').length;
			for(var j=0;j<=json.detailVal.length-1;j++)
			{
				docuNo		= json.detailVal[j].docNo;				
				invoiceNo	= json.detailVal[j].invoiceNo;				
				lastPDate	= json.detailVal[j].lastPdate;
				amount		= json.detailVal[j].amount;				
				toBePaid	= json.detailVal[j].toBePaid;
				payingAmt	= json.detailVal[j].payAmt;				
				//alert(docuNo);	
/*				if(j != 0)
				{				
*//*					tbl.insertRow(rows);
					tbl.rows[rows].innerHTML = tbl.rows[rows-1].innerHTML;
					tbl.rows[rows].cells[0].childNodes[1].value = invoiceNo;					
					tbl.rows[rows].cells[1].childNodes[1].value = lastPDate;
					tbl.rows[rows].cells[2].childNodes[0].value = amount;
					tbl.rows[rows].cells[3].childNodes[1].value = toBePaid;
					tbl.rows[rows].cells[4].childNodes[0].value = payingAmt;			
					
*/					
					var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF" name="txtDocNo" type="text" id="'+docuNo+'" class="validate[custom[number]] docuNo"><a target="_blank" href="../bills/bills.php?cboSearch='+docuNo+'">'+docuNo+'</a></td>';			
					content +='<td align="center" bgcolor="#FFFFFF" name="txtInvoiceNo" type="text" id="txtInvoiceNo" class="validate[custom[number]] invoiceNo">'+invoiceNo+'</td>';
					//var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF" name="txtInvoiceNo" type="text" id="txtInvoiceNo" class="validate[custom[number]] invoiceNo">'+invoiceNo+'</td>';		
					content +='<td align="center" bgcolor="#FFFFFF" name="txtLastPDate" type="text" id="txtLastPDate" class="lPDate">'+lastPDate+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF"><input name="txtAmount" type="text" readonly="readonly" id="txtAmount" style="width:100%;background-color:#F3F3F3;border:none;text-align:right" class="validate[custom[number]] amount normalfntMid" value='+amount+'></td>';	 					
					content +='<td align="center" bgcolor="#FFFFFF"><input name="txttoBePaid" type="text" id="txttoBePaid" readonly="readonly" style="width:100%;background-color:#F3F3F3;border:none;text-align:right" class="validate[custom[number]] payingAmount normalfntMid" value='+toBePaid+' ></td>';							
					content +='<td align="center" bgcolor="#FFFFFF"><span class="normalfntMid"><input name="txtPaidAmt" type="text" id="txtPaidAmt" class="validate[custom[number]] paidAmount"  style="width:100%;background-color:#F3F3F3;border:none;text-align:right" value='+payingAmt+' ></span></td>';
					content +='</tr>';		
				
					add_new_row('#frmBillPayment #tblMainGrid3',content);
			}	
					
		}				
		
					
								
			}
		});
 }
 //============================
//---------------------------------------------------------

function changeRate()
{	
	//var billRefNo=document.getElementById("cboSearch").value;
	var currencyType = $('#frmBillPayment #cboCurrency').val();
	
	if(currencyType!='')
	{
		loadCurrencyRate(currencyType);
	}
}

function loadCurrencyRate(currencyType){	
	var url = "billPayments-db-get.php?requestType=getExchangeRate&currencyId="+currencyType+'&exchangeDate='+$('#txtDate').val();
	var obj = $.ajax({url:url,dataType:'json',success:function(json){
		
		$('#rdbBuying').val(json.buyingRate);
		$('#rdbSelling').val(json.sellingRate);
		document.getElementById("txtRate").value=json.sellingRate;
		
	//--------------------------------------------------------	
	$('#frmBillPayment #rdbSelling').click(function(){
					document.getElementById("txtRate").value=json.sellingRate;
					//document.getElementById("txtRate").innerHTML=json.buyingRate;
  	});
 	 //-------------------------------------------------------
  	$('#frmBillPayment #rdbBuying').click(function(){
		//$('#frmBillPayment #rdbBuying').checked == false;
		document.getElementById("txtRate").value=json.buyingRate;
 	 });
	 
	 //-------------------------------------------------------
	 
	  $('#frmBillPayment #chkEdit').click(function(){
	  if(document.getElementById("chkEdit").checked==true)
		$('#frmBillPayment #txtRate').removeAttr('disabled');
		else
		$('#frmBillPayment #txtRate').attr("disabled",true);

  });			
		},async:false});
	
}

// =========================Load Grig when click checkbox=====================================

function LoadCategoryDetails()
{	
	//var rowCount = $('#tblDispatchPopup >tr').length;
	
	var rowCount = document.getElementById('tblMainGrid1').rows.length;
	clearRows();
	for(var i=1;i<rowCount;i++)
	{		
	
		if((document.getElementById('tblMainGrid1').rows[i].cells[0].childNodes[0].checked==true) && (document.getElementById('tblMainGrid1').rows[i].cells[0].childNodes[0].disabled==false)){
			
			var DocNo=document.getElementById('tblMainGrid1').rows[i].cells[1].id;			
			var invoiceNo=document.getElementById('tblMainGrid1').rows[i].cells[3].innerHTML;
			var lastPayDate=document.getElementById('tblMainGrid1').rows[i].cells[4].innerHTML;		
			var amount=parseFloat(document.getElementById('tblMainGrid1').rows[i].cells[7].innerHTML);		
			var toBePaidAmt=parseFloat(document.getElementById('tblMainGrid1').rows[i].cells[8].innerHTML);		
			
			var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF" name="txtDocNo" type="text" id="'+DocNo+'" class="validate[custom[number]] DocNo"><a target="_blank" href="../bills/bills.php?cboSearch='+DocNo+'">'+DocNo+'</a></td>';																																										
			content +='<td align="center" bgcolor="#FFFFFF" name="txtInvoiceNo" type="text" id="txtInvoiceNo" class="validate[custom[number]] invoiceNo">'+invoiceNo+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" name="txtLastPDate" type="text" id="txtLastPDate" class="lPDate">'+lastPayDate+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF"><input name="txtAmount" type="text" readonly="readonly" id="txtAmount" style="width:100%;background-color:#F3F3F3;border:none;text-align:right" class="validate[custom[number]] amount normalfntMid" value='+amount+'></td>';	 					
			content +='<td align="center" bgcolor="#FFFFFF" id="'+toBePaidAmt+'"><input name="txttoBePaid" type="text" id="txttoBePaid" readonly="readonly" style="width:100%;background-color:#F3F3F3;border:none;text-align:right" class="validate[custom[number]] payingAmount normalfntMid" value='+toBePaidAmt+'></td>';							
			content +='<td align="center" bgcolor="#FFFFFF"><span class="normalfntMid"><input name="txtPaidAmt" type="text" id="txtPaidAmt" class="validate[custom[number]] paidAmount"  style="width:100%;background-color:#F3F3F3;border:none;text-align:right"/></span></td>';
			content +='</tr>';		
		
			add_new_row('#frmBillPayment #tblMainGrid3',content);
		}		
	}	
	
}
//var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF" name="txtInvoiceNo" type="text" id="txtInvoiceNo" class="validate[custom[number]] invoiceNo">'+invoiceNo+'</td>';		
//<td align="center" bgcolor="#FFFFFF"> <input name="txtDocNo" type="text" id="txtDocNo" style="width:100%" class="validate[custom[number]] DocNo" /></td>
            
// Add new row for grid

function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
	
function clearRows()
{
	//var rowCount = $('#tblDispatchPopup >tbody >tr').length;
	var rowCount = document.getElementById('tblMainGrid3').rows.length;
	//alert(rowCount);
	//var cellCount = document.getElementById('tblMain').rows[1].cells.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblMainGrid3').deleteRow(1);			
	}
}

// Remove tblMainGrid3 content after save-----------------
function clearGridRowContent()
{
	//var rowCount = $('#tblDispatchPopup >tbody >tr').length;
	var rowCount = document.getElementById('tblMainGrid3').rows.length;
	//alert(rowCount);
	//var cellCount = document.getElementById('tblMain').rows[1].cells.length;
	for(var i=2;i<rowCount;i++)
	{
			document.getElementById('tblMainGrid3').deleteRow(1);			
	}	
		document.getElementById('tblMainGrid3').rows[1].cells[0].innerHTML = "";	
		document.getElementById('tblMainGrid3').rows[1].cells[1].innerHTML = "";
		document.getElementById('tblMainGrid3').rows[1].cells[2].innerHTML = "";
		document.getElementById('tblMainGrid3').rows[1].cells[3].innerHTML = "";
		document.getElementById('tblMainGrid3').rows[1].cells[4].innerHTML = "";
		document.getElementById('tblMainGrid3').rows[1].cells[5].innerHTML = "";			
}

// -----------cal tobepaid amount------------------

function calToBePaid(obje){
	
	var row=obje.parentNode.parentNode.parentNode.rowIndex;
	var paying=$(obje).parent().find(".paidAmount").val();	
	//var amount=$(obje).parent().parent().find(".Amount").val();
	//var paying=document.getElementById('tblMainGrid3').rows[row].cells[4].childNodes[0].value;
	//var amount=document.getElementById('tblMainGrid3').rows[row].cells[3].childNodes[0].value;	
	
	//========================6.13 Change===============================
	//var amount=document.getElementById('tblMainGrid3').rows[row].cells[3].id;	//---------get 'tobepaid' amount befor change (when load to value to grid)
	//==================================================================
	var amount=document.getElementById('tblMainGrid3').rows[row].cells[4].id;
	var toBePaid= amount- paying;

	$(obje).parent().parent().parent().find(".payingAmount").val(toBePaid);
}

//------------cal total amounts-----------------

function callTotalAmount()
{		
	var amountTobePaid=0;
	$('#tblMainGrid3 tr:not(:first)').each(function(){				
		amountTobePaid+= parseFloat($(this).find(".payingAmount").val());				
	});	
		$('#frmBillPayment #txtTotPaid').val(amountTobePaid);
			
//------------------------------------------------------
	var amountPaying =0;
	$('#tblMainGrid3 tr:not(:first)').each(function(){				
		amountPaying+= parseFloat($(this).find(".paidAmount").val());				
	});
	
	$('#frmBillPayment #txtTotPaying').val(amountPaying);
}

function loadCombo_frmBillPayment()
{
	var url 	= "billPayments-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmBillPayment #cboSearchVoucher').html(httpobj.responseText);
}
//-------------------------------------
function alertx()
{
	$('#frmBillPayment #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	//alert("ll");
	//$('#frmBillPayment #butDelete').validationEngine('hide')	;
	$('#frmBillPayment #butDelete').validationEngine('hide')	;
}







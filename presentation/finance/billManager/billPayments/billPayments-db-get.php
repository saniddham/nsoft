<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$companyId = $row['intCompanyId']; 
	}
	/////////// sales invoice load part /////////////////////
	if($requestType=='loadCombo')
	{
		$sql = "SELECT
					fin_billmanager_billpayments_header.strReferenceNo
				FROM
					fin_billmanager_billpayments_header
				WHERE
					fin_billmanager_billpayments_header.intCompanyId =  '$companyId' AND fin_billmanager_billpayments_header.intDeleteStatus =  '0'";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['strReferenceNo']."\">".$row['strReferenceNo']."</option>";
		}
		echo $html;
	}
	else if($requestType=='loadDetails')
	{
		$voucherID  = $_REQUEST['voucherRefNo'];		
		
		 $sql = "SELECT
		 			fin_billmanager_billpayments_main_details.intVoucherNo,
					fin_billmanager_billpayments_main_details.intAccPeriodId,
					fin_billmanager_billpayments_main_details.intLocationId,
					fin_billmanager_billpayments_main_details.intCompanyId,
					fin_billmanager_billpayments_main_details.strDocumentNo,
					fin_billmanager_billpayments_main_details.strInvoiceNo,
					fin_billmanager_billpayments_main_details.dtmLastPayDate,
					fin_billmanager_billpayments_main_details.dblAmount,
					fin_billmanager_billpayments_main_details.dblToBePaid,
					fin_billmanager_billpayments_main_details.dblPayAmount
		 
				FROM
					fin_billmanager_billpayments_main_details
				WHERE
					fin_billmanager_billpayments_main_details.strReferenceNo =  '$voucherID'";
	
		$result = $db->RunQuery($sql);
		$arrDetail;
		while($row=mysqli_fetch_array($result))
		{
			$val['docNo'] 		= $row['strDocumentNo'];
			$val['invoiceNo'] 	= $row['strInvoiceNo'];
			$val['lastPdate'] 	= $row['dtmLastPayDate'];
			$val['amount'] 		= $row['dblAmount'];
			$val['toBePaid'] 	= $row['dblToBePaid'];
			$val['payAmt'] 		= $row['dblPayAmount'];
			
			$arrDetail[] = $val;
		}
		$response['detailVal'] = $arrDetail;
								
		$sql   = "SELECT
					fin_billmanager_billpayments_header.intVoucherNo,
					fin_billmanager_billpayments_header.intAccPeriodId,
					fin_billmanager_billpayments_header.intLocationId,
					fin_billmanager_billpayments_header.intCompanyId,
					fin_billmanager_billpayments_header.strReferenceNo,
					fin_billmanager_billpayments_header.dtmDate,
					fin_billmanager_billpayments_header.intCurrencyId,
					fin_billmanager_billpayments_header.dblRate,
					fin_billmanager_billpayments_header.dblPayAmount,
					fin_billmanager_billpayments_header.strPayTo,
					fin_billmanager_billpayments_header.intPayMethodId,
					fin_billmanager_billpayments_header.strPaymentRefNo,
					fin_billmanager_billpayments_header.dtmRefDate,
					fin_billmanager_billpayments_header.intIsPosted,
					fin_billmanager_billpayments_header.strRefOrg,
					fin_billmanager_billpayments_header.strRemark,
					fin_billmanager_billpayments_account_details.intCrAccountId,
					fin_billmanager_billpayments_account_details.intDrAccountId,
					fin_billmanager_billpayments_account_details.dblToBePaidAmt,
					fin_billmanager_billpayments_account_details.dblPayAmt
				FROM
					fin_billmanager_billpayments_header Inner Join fin_billmanager_billpayments_account_details ON fin_billmanager_billpayments_header.strReferenceNo = fin_billmanager_billpayments_account_details.strReferenceNo
				WHERE
					fin_billmanager_billpayments_header.strReferenceNo =  '$voucherID'";
				
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['referenceNo'] = $row['strReferenceNo'];
			$response['date'] 		 = $row['dtmDate'];
			$response['currencyId']  = $row['intCurrencyId'];			
			$response['rate'] 		 = $row['dblRate'];
			$response['payAmount'] 	 = $row['dblPayAmount'];
			$response['payTo'] 		 = $row['strPayTo'];
			$response['payMethodId'] = $row['intPayMethodId'];
			$response['payRefNo'] 	 = $row['strPaymentRefNo'];
			$response['refDate'] 	 = $row['dtmRefDate'];
			$response['isPosted'] 	 =($row['intIsPosted']?true:false);
			$response['refOrg'] 	 = $row['strRefOrg'];
			$response['remarks'] 	 = $row['strRemark'];
			
			$response['crAcc'] 	 = $row['intCrAccountId'];
			$response['drAcc'] 	 = $row['intDrAccountId'];
			$response['toBePaid'] 	 = $row['dblToBePaidAmt'];
			$response['PayAmt'] 	 = $row['dblPayAmt'];
			
		}		
		
		echo json_encode($response);
		
		//====================================================================
	}
	else if($requestType=='getExchangeRate')
	{
		$currencyId  	= $_REQUEST['currencyId'];
		$exchangeDate	= $_REQUEST['exchangeDate'];
		
		$sql = "SELECT
					mst_financeexchangerate.dblSellingRate,
					mst_financeexchangerate.dblBuying
				FROM mst_financeexchangerate
				WHERE
					mst_financeexchangerate.intCurrencyId 	=  '$currencyId' AND
					mst_financeexchangerate.dtmDate 		=  '$exchangeDate'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		
		if(mysqli_num_rows($result)>0)
		{
			$arrValue['sellingRate'] 	= $row['dblSellingRate'];
			$arrValue['buyingRate'] 	= $row['dblBuying'];
		}
		else
		{
			$arrValue['sellingRate'] 	= "";
			$arrValue['buyingRate'] 	= "";
		}
		echo json_encode($arrValue);
	}	
	
	else if($requestType=='getInvoice')
	{
		$customerId  = $_REQUEST['customerId'];
		
		$sql = "SELECT
				fin_customer_salesinvoice_header.intInvoiceNo,
				fin_customer_salesinvoice_details.intInvoiceNo,
				fin_customer_salesinvoice_header.intAccPeriodId,
				fin_customer_salesinvoice_details.intAccPeriodId,
				fin_customer_salesinvoice_header.intLocationId,
				fin_customer_salesinvoice_details.intLocationId,
				fin_customer_salesinvoice_header.intCompanyId,
				fin_customer_salesinvoice_details.intCompanyId,
				fin_customer_salesinvoice_header.strReferenceNo,
				fin_customer_salesinvoice_details.strReferenceNo,
				fin_customer_salesinvoice_header.intCustomerId,
				fin_customer_salesinvoice_header.dtmDate,
				fin_customer_salesinvoice_header.dblRate,
				mst_customer.intId,
				mst_customer.strName,
				fin_customer_salesinvoice_header.intCurrencyId,
				mst_financecurrency.intId,
				mst_financecurrency.strCode,
				fin_customer_salesinvoice_details.dblQty,
				fin_customer_salesinvoice_details.dblUnitPrice,
				fin_customer_salesinvoice_details.dblDiscount,
				fin_customer_salesinvoice_details.dblTaxAmount,
				sum(fin_customer_salesinvoice_details.dblQty*(fin_customer_salesinvoice_details.dblUnitPrice *(100-fin_customer_salesinvoice_details.dblDiscount)/100)+IFNULL(fin_customer_salesinvoice_details.dblTaxAmount,0)) AS amount
				FROM
				fin_customer_salesinvoice_header
				left outer Join fin_customer_salesinvoice_details ON fin_customer_salesinvoice_header.intInvoiceNo = fin_customer_salesinvoice_details.intInvoiceNo AND fin_customer_salesinvoice_header.intAccPeriodId = fin_customer_salesinvoice_details.intAccPeriodId AND fin_customer_salesinvoice_header.intLocationId = fin_customer_salesinvoice_details.intLocationId AND fin_customer_salesinvoice_header.intCompanyId = fin_customer_salesinvoice_details.intCompanyId AND fin_customer_salesinvoice_header.strReferenceNo = fin_customer_salesinvoice_details.strReferenceNo
				left outer Join mst_financecurrency ON fin_customer_salesinvoice_header.intCurrencyId = mst_financecurrency.intId
				left outer Join mst_customer ON fin_customer_salesinvoice_header.intCustomerId = mst_customer.intId
				WHERE
				fin_customer_salesinvoice_header.intCustomerId =  '$customerId'
				GROUP BY
				fin_customer_salesinvoice_header.strReferenceNo
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$invoice = $row['strReferenceNo'];
			$amount = number_format($row['amount'],2,'.','');
			$date = $row['dtmDate'];
			$currency = $row['strCode'];
			$rate = $row['dblRate'];
			echo 
				"
				<tr class=normalfnt bgcolor=#FFFFFF>
				<td align=center><input class=checkRow type=checkbox id=$invoice /></td>
				<td width=170 align=center class=jobNo>$invoice</td>
				<td width=170 align=center class=docNo>$invoice</td>
				<td align=center class=docDate>$date</td>
				<td align=right class=amount>$amount</td>
				<td align=right class=toBePaid>$amount</td>
				<td align=center class=docCurrency>$currency</td>
				<td align=right class=docRate>$rate</td>
				<td align=center><input disabled=disabled style=width:80px;text-align:right;background-color:#FFF;border:none name=txtPayAmount  class=payAmount type=text id=txt$invoice /></td>
				<td align=center>more</td>
				</tr>
				";
		} 
		echo json_encode($response);
	}
	

		
		
		//====================================================================
	

?>
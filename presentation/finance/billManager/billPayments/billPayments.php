<?php
	$backwardseperator = "../../../../";
	
	$mainPath = $_SESSION['mainPath'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	
	$thisFilePath =  $_SERVER['PHP_SELF'];
	include  "{$backwardseperator}dataAccess/permisionCheck.inc";
	
	$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$companyId = $row['intCompanyId']; 
	}	
	  $serviceproId = $_POST['cboServicePro'];
	  $paging=$_POST['cboPaging'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bill Payments</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $backwardseperator; ?>css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">


<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="billPayments-js.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
</head>

<body>

 	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator.'Header.php'; ?></td>
	</tr> 
 
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>
<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>


<form id="frmBillPayment" name="frmBillPayment" method="post" autocomplete="off" >
<div align="center">
  <div class="trans_layoutL">
    <div class="trans_text">Bill Payments</div>
<table width="100%">
<tr>
      <td width="50%" colspan="2" align="center" bgcolor="#EAF8FB" class="tableBorder"><span class="normalfnt"><strong>Bill Progress</strong></span></td>
      </tr>
    <tr>
      <td width="50%" colspan="2" align="left"><table width="100%">
      <tr>
      <td align="right"><span class="normalfntMid">
        <em>Records
        </em>       
        <select name="cboPaging" id="cboPaging" style="width:80px" onchange = "submit()"> 
		    		
          <option value="10" <?php if($paging==10){ ?> selected="selected" <?php } ?>>0 to 10 </option>
          <option value="25" <?php if($paging==25){ ?> selected="selected" <?php } ?>>0 to 25 </option>
          <option value="50" <?php if($paging==50){ ?> selected="selected" <?php } ?>>0 to 50</option>
          <option value="100" <?php if($paging==100){ ?> selected="selected" <?php } ?>>0 to 100</option>
          <option value= "*" <?php if($paging=='*'){ ?> selected="selected" <?php } ?>>0 to Max</option>
        </select>
      </span></td>
      </tr>
     <tr>
      <td align="right"><span class="normalfntMid">
        <em>Service Provider
        </em>       
       <select name="cboServicePro" id="cboServicePro"  style="width:195px" onchange = "submit()" >
            <option value=""></option>
            <?php   $sql = "SELECT intProviderId, strProviderName
							FROM mst_financeserviceprovider
							Inner Join mst_financeserviceprovideractivate ON mst_financeserviceprovider.intProviderId = mst_financeserviceprovideractivate.intServiceProviderId
							WHERE 	mst_financeserviceprovider.intCompanyId =  '$companyId'	
							ORDER BY strProviderName ASC";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							if($serviceproId==$row['intProviderId']){
							echo "<option value=\"".$row['intProviderId']."\" selected=\"selected\">".$row['strProviderName']."</option>";
							}
							else{
							echo "<option value=\"".$row['intProviderId']."\">".$row['strProviderName']."</option>";
							}
						}
          ?>
          </select>
      </span></td>
      </tr>      
        <tr>
          <td align="right"><table width="100%" id="tblMainGrid1" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
            <tr class="">
              <td width="25" bgcolor="#FAD163" class="normalfntMid">&nbsp;</td>
              <td width="143" height = "25" align="center" bgcolor="#FAD163" class="normalfntMid"><strong>Document No.</strong></td>
              <td width="143" align="center" bgcolor="#FAD163" class="normalfntMid"><strong>Bill Category</strong></td>
              <td width="135" align="center"  bgcolor="#FAD163" class="normalfntMid"><strong>Invoice No.</strong></td>
              <td width="206" align="center"  bgcolor="#FAD163" class="normalfntMid"><strong>Last Date for Payments</strong></td>
              <td width="135" align="center" bgcolor="#FAD163" class="normalfntMid"  ><span class="normalfnt"><strong>Bill From</strong></span></td>
              <td width="128" align="center" bgcolor="#FAD163" class="normalfntMid"  ><span class="normalfnt"><strong>Bill To</strong></span><strong><a href="#" class="normalfntMid"></a></strong></td>
              <td width="120" align="center" bgcolor="#FAD163" class="normalfntMid"  ><strong> Amount</strong></td>
              <td width="120" align="center" bgcolor="#FAD163" class="normalfntMid"  ><strong> To Be Paid</strong></td>
              </tr>
           
                        <?php
	
		    $sql = "SELECT
						fin_billmanager_bills_header.intBillNo,
						fin_billmanager_bills_header.strReferenceNo,
						fin_billmanager_bills_header.strBillType,
						fin_billmanager_bills_header.invoiceNo,
						fin_billmanager_bills_header.dtmBillFrom,
						fin_billmanager_bills_header.dtmBillTo,
						fin_billmanager_bills_header.dtmLastPayDate,
						fin_billmanager_bills_header.intTotAmtMonth,
						fin_billmanager_bills_header.dblToBePaid
					FROM
						fin_billmanager_bills_header
					WHERE
						fin_billmanager_bills_header.intServiceProId =  '$serviceproId' AND fin_billmanager_bills_header.intDeleteStatus =  '0'";
				
				if($paging==''){
				$sql .= " limit 10";
				}
				else if($paging!='*'){
				$sql .= " limit ".$paging;
				}

					
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
	  ?>
      <tr class="normalfnt">        
     	 <td align="center" bgcolor="#FFFFFF" class="normalfntMid" ><input type="checkbox" name="chbCatId" id="chbCatId" class="checkCategory"/></td>
         <td align="center" bgcolor="#FFFFFF" id="<?php echo $row['strReferenceNo'];?>" class="loadId" ><a target="_blank" href="../bills/bills.php?cboSearch=<?php echo $row['strReferenceNo'];?>" class="normalfntMid"><?php echo $row['strReferenceNo'];?></a></td>
         <td align="center" bgcolor="#FFFFFF"><?php echo $row['strBillType'];?></td> 
         <td align="center" bgcolor="#FFFFFF"><?php echo $row['invoiceNo'];?></td> 
         <td align="center" bgcolor="#FFFFFF"><?php echo $row['dtmBillFrom'];?></td>  
         <td align="center" bgcolor="#FFFFFF"><?php echo $row['dtmBillTo'];?></td>           
         <td align="center" bgcolor="#FFFFFF"><?php echo $row['dtmLastPayDate'];?></td>  
         <td align="center" bgcolor="#FFFFFF"><?php echo $row['intTotAmtMonth'];?></td> 
         <td align="center" bgcolor="#FFFFFF"><?php echo $row['dblToBePaid'];?></td>       
      </tr>
      <?php 
        } 
       ?>
              
            
            </table></td>
          </tr>
        </table>
        <table align="right">
          <tr>
            <td align="right"><span class="normalfnt">Voucher Number</span></td>
            <td align="right"><span class="normalfnt">
              <input name="txtVoucherNo" type="text" disabled="disabled" class="normalfntRight" id="txtVoucherNo" value="<?php echo $voucherNo ?>" style="width:175px" />
              </span>
              </td>
            </tr>
          </table>
        </td>  
    </tr>
    <tr>
    <td colspan="2" align="left">
    <table width="100%">
   <tr>
	<td align="right" width="350px"><span class="normalfntMid">Voucher No</span></td>
        <td align="left"><span class="normalfntMid">
          <select name="cboSearchVoucher" id="cboSearchVoucher"  style="width:195px"  onChange="functionVoucherDetails()">
            <option value=""></option>
            <?php   $sql = "SELECT
								fin_billmanager_billpayments_header.strReferenceNo
							FROM
								fin_billmanager_billpayments_header
							WHERE
								fin_billmanager_billpayments_header.intCompanyId  =  '$companyId' AND fin_billmanager_billpayments_header.intDeleteStatus =  '0'";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['strReferenceNo']."\">".$row['strReferenceNo']."</option>";
							$docNo 		=	 $row['strReferenceNo'];
						}
          ?>
          </select>
        </span></td>
      </tr>
    
    </table>
    </td>
    </tr>
    <tr>
      <td colspan="2">
      <table width="100%" class="tableBorder_allRound">
    <tr>
      <td width="58" class="normalfnt">&nbsp;</td>
      <td width="158"><span class="normalfnt">Date <span class="compulsoryRed">*</span></span></td>
       <td width="240" bgcolor="#FFFFFF" class=""><input name="txtDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="txtbox" id="txtDate" style="width:98px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
      <td width="158"></td>
      <td width="240" bgcolor="#FFFFFF" class=""></td>
      <td width="39">&nbsp;</td>
    </tr>
    
    <tr>
      <td>&nbsp;</td>
      <td><span class="normalfnt">Currency <span class="compulsoryRed">*</span></span></td>
      <td><select name="cboCurrency" id="cboCurrency" style="width:140px" class="validate[required]" onChange="changeRate()">
        <option value=""></option>
        <?php  $sql = "SELECT
						intId,
						strCode
						FROM mst_financecurrency
						WHERE
							intStatus = 1
						order by strCode
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strCode']."</option>";
						}
        				?>
      </select></td>
      <td><span class="normalfnt">Rate</span> <span class="compulsoryRed">*</span></td>
      <td><span class="normalfnt"><span class="normalfntMid">
        <input class="rdoRate" type="radio" name="rdbSelect" id="rdbSelling" value="" checked="checked"/>
        Selling
        <input class="rdoRate" type="radio" name="rdbSelect" id="rdbBuying" value="" />
        Buying
        
        <input type="text" name="txtRate" id="txtRate" style="width:75px; background-color:#9F9; border:thin; text-align:center" readonly="readonly" class="validate[custom[number],required] normalfntBlue"/>
		<input type="checkbox" name="chkEdit" id="chkEdit" />       
       
      </span></span></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="normalfnt">Pay Amount</span></td>
      <td><span class="normalfnt">        
        <input type="text" class="normalfntMid" name="txtPayAmt" readonly="readonly" id="txtPayAmt" style="width:100px;background-color:#FCF;border:none;text-align:center" />    
      </span></td>
      <td class="normalfnt">Pay To</td>
      <td><span class="normalfntMid">
        <input name="txtPayTo" type="text" id="txtPayTo" style="width:150px" />
      </span></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong class="normalfnt">Payment Method</strong></td>
     <td><span class="normalfntMid">
        <select name="cboPaymentMethod" id="cboPaymentMethod" style="width:150px"  class="validate[required]">
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_financepaymentsmethods.intId,
							mst_financepaymentsmethods.strName
							FROM mst_financepaymentsmethods
							WHERE
							mst_financepaymentsmethods.intStatus =  '1'";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$payMethod)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
					?>
        </select>
      </span></td>
      <td><strong class="normalfnt">Reference No.</strong></td>
      <td><span class="normalfntMid">
        <input type="text" name="txtRefNo" id="txtRefNo" style="width:105px" />
      </span></td>
      <td>&nbsp;</td>
    </tr>
    <tr id="rwChequeDetails" <?php if($payMethod!=2){?> style="display:none" <?php } ?>>
      <td>&nbsp;</td>
      <td><strong class="normalfnt">Reference</strong> <strong class="normalfnt">Date</strong></td>
      <td><input name="txtRefDate" type="text" value="<?php echo date("Y-m-d"); ?>"  class="validate[required] txtbox" id="txtRefDate" style="width:98px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /> 
        <input type="checkbox" name="chbPosted" id="chbPosted" <?php if($chkPost=='1'){ ?> checked="checked" <?php } ?>  />
        <span class="normalfnt">Posted</span></td>
      <td><strong class="normalfnt">Reference Organization</strong></td>
      <td><span class="normalfntMid">
      <input type="text" name="txtRefOrg" id="txtRefOrg" style="width:195px" value="<?php echo $refOrganization ?>" />
      </span></td>
      <td>&nbsp;</td>
    </tr>   
    
    <tr>
      <td>&nbsp;</td>
      <td><span class="normalfnt">Memo</span></td>
      <td colspan="2"><textarea name="txtRemarks" id="txtRemarks" cols="45" rows="2"></textarea></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
      </table>
      </td>
      </tr>
      <tr>
      <td colspan="2" align="center">
      </td>
      </tr>
      <tr>
      <td colspan="2" align="center">
       <div style="overflow:scroll;width:900px;height:150px;" id="divGrid">
         <table width="100%" id="tblMainGrid3" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
           <tr class="">
             <td width="168" align="center"  bgcolor="#FAD163" class="normalfntMid"><strong>Document No.</strong></td>
             <td width="168" height="26" align="center"  bgcolor="#FAD163" class="normalfntMid"><strong>Invoice No.</strong></td>
             <td width="247" align="center"  bgcolor="#FAD163" class="normalfntMid"><strong>Last Date for Payments</strong></td>
             <td width="179" align="center" bgcolor="#FAD163" class="normalfntMid"  ><strong>Amount</strong></td>
             <td width="150" align="center" bgcolor="#FAD163" class="normalfntMid"  ><strong>To Be Paid</strong></td>
             <td width="150" align="center" bgcolor="#FAD163" class="normalfntMid"  ><strong>Paying Amount</strong></td>
           </tr>
           <tr class="normalfnt">
             <td align="center" bgcolor="#FFFFFF"> <input name="txtDocNo" type="text" id="txtDocNo" style="width:100%" class="validate[custom[number]] DocNo" /></td>          																													                    
             <td align="center" bgcolor="#FFFFFF"> <input name="txtInvoiceNo" type="text" id="txtInvoiceNo" style="width:100%" class="validate[custom[number]] invoiceNo"  /></td>      
             <td align="center" bgcolor="#FFFFFF"> <input name="txtLastPDate" type="text" id="txtLastPDate" style="width:100%" class="lPDate"  /></td>
             <td align="right"  bgcolor="#FFFFFF"><input name="txtAmount" type="text" readonly="readonly" id="txtAmount" style="width:100%;background-color:#F3F3F3;border:none;text-align:right" class="validate[custom[number]] amount normalfntMid "/> <?php echo $amount ?>
             </td>     
        
             <td align="right"  bgcolor="#FFFFFF">
               <input name="txttoBePaid" type="text" id="txttoBePaid" readonly="readonly" style="width:100%;background-color:#F3F3F3;border:none;text-align:right" class="validate[custom[number]] payingAmount normalfntMid"  />
             </td>
             <td align="right"  bgcolor="#FFFFFF"><input name="txtPaidAmt" type="text" id="txtPaidAmt" class="validate[custom[number]] normalfntMid paidAmount" style="width:100%;background-color:#F3F3F3;border:none;text-align:right" />
              </td>
           </tr>
         </table>
       
       </div>
      </td>
      </tr>
      <tr>
      <td colspan="2" align="Right">
      <table width="100%">
      <tr>
        <td width="65%" align="right" class="normalfntRight">Total Amount</td>
        <td width="17%" align="right" class="normalfntRight"><span class="normalfntMid">
          <input name="txtTotPaid" type="text" readonly="readonly" id="txtTotPaid" style="width:150px" class="normalfntMid"/>
        </span></td>
        <td width="17%"><span class="normalfntMid">
          <input name="txtTotPaying" type="text" readonly="readonly" id="txtTotPaying" style="width:150px" class="normalfntMid" />
        </span></td>
      <td width="1%">&nbsp;</td>
      </tr>
      </table>
      </td>
      </tr>
      <tr>
      <td colspan="2" align="center">
          <table width="100%">
      <tr>
      <td align="center" bgcolor="#E3F5F9" class="tableBorder_topRound"><span class="normalfntMid"><!--<strong>Debit Account</strong></span>--></td>
      <td align="center" bgcolor="#E3F5F9" class="tableBorder_topRound"><span class="normalfntMid"><strong>Credit Account</strong></span></td>
      </tr>
      <tr class="tableBorder_allRound">
      <td align="center" bgcolor="#F2F2F2"><span class="normalfntMid"><!--
        <select name="cmbDebitAcc" id="cmbDebitAcc" style="width:210px" class="validate[required]">
        <option value=""></option>
        <?php /* $sql = "SELECT
							mst_financechartofaccounts.intId,
							mst_financechartofaccounts.strCode,
							mst_financechartofaccounts.strName,
							mst_financechartofaccounts_companies.intCompanyId,
							mst_financechartofaccounts_companies.intChartOfAccountId
							FROM
							mst_financechartofaccounts
							Inner Join mst_financechartofaccounts_companies ON mst_financechartofaccounts_companies.intChartOfAccountId = mst_financechartofaccounts.intId
							WHERE
							intStatus = '1' AND   ( intFinancialTypeId = '28' Or  intFinancialTypeId = '18') AND strType = 'Posting' AND intCompanyId = '$companyId'
							order by strCode";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
						}
        				*/?>
      </select>   
      --></span></td>
      <td align="center" bgcolor="#F2F2F2"><span class="normalfntMid">
        <select name="cmbCreditAcc" id="cmbCreditAcc" style="width:210px" class="validate[required]">
        <option value=""></option>
        <?php  $sql = "SELECT
							mst_financechartofaccounts.intId,
							mst_financechartofaccounts.strCode,
							mst_financechartofaccounts.strName,
							mst_financechartofaccounts_companies.intCompanyId,
							mst_financechartofaccounts_companies.intChartOfAccountId
							FROM
							mst_financechartofaccounts
							Inner Join mst_financechartofaccounts_companies ON mst_financechartofaccounts_companies.intChartOfAccountId = mst_financechartofaccounts.intId
							WHERE
							intStatus = '1' AND ( intFinancialTypeId = '24' Or  intFinancialTypeId = '13') AND strType = 'Posting' AND intCompanyId = '$companyId'
							order by strCode";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
						}
        				?>
      </select>   
      </span></td>
      </tr>
      <tr class="tableBorder_allRound">
        <td align="center" bgcolor="#E3F5F9" class="tableBorder_bottomRound">&nbsp;</td>
        <td align="center" bgcolor="#E3F5F9" class="tableBorder_bottomRound">&nbsp;</td>
      </tr>
          </table>
      </td>
      </tr>
      <tr>
        <td colspan="2">
          <table width="100%">
            <tr>
              <td width="100%" height="34" class="tableBorder_allRound"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                <tr>
                 	<td width="100%" align="center" bgcolor=""><img style="display:none" border="0" src="../../../../images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:none" border="0" src="../../../../images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/><img style="display:none" border="0" src="../../../../images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="25"/><a href="../../../../main.php"><img  src="../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                </tr>
              </table></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
</div>
</div>
</form>    
</body>
</html>
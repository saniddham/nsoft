<?php 
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	$companyId = $_SESSION['headCompanyId'];
	$locationId = $_SESSION["CompanyID"];
	////////////////////////// main parameters ////////////////////////////////
	$voucherNo		= trim($_REQUEST['txtVoucherNo']);
	$requestType 	= $_REQUEST['requestType'];
	//$id 			= $_REQUEST['cboSearch'];
	///////////////////// received payments header parameters ////////////////////
	//$customer		= null(trim($_REQUEST['cboCustomer']));
	$date			= trim($_REQUEST['txtDate']);
	$currency		= null(trim($_REQUEST['cboCurrency']));
	$rate			= val(trim($_REQUEST['txtRate']));
	$payAmount		= val(trim($_REQUEST['txtPayAmt']));
	$payTo			= $_REQUEST['txtPayTo'];	
	$payMethod		= null($_REQUEST['cboPaymentMethod']);
	$refNo 			= $_REQUEST['txtRefNo'];
	$servicePro		= $_REQUEST['cboServicePro'];
	//$refDate		= trim($_REQUEST['txtRefDate']);
	//$isPosted		= ($_REQUEST['chbPosted']?1:0);
	//$refOrg			= trim($_REQUEST['txtRefOrg']);
	$remarks 		= $_REQUEST['txtRemarks'];
	
	if($payMethod=="2"){//cheque
	  	$isPosted  = ($_REQUEST['chbPosted']?1:0);
		$refDate   = $_REQUEST['txtRefDate'];
		$refOrg    = $_REQUEST['txtRefOrg'];
	}
	else{
		$isPosted  ='0';
		$refDate   ='0000-00-00';
		$refOrg    ='';
	}	
	
	$toBePaidAmt = val($_REQUEST['txtTotPaid']);
	$payAmt = val($_REQUEST['txtTotPaying']);
	
	$crAcc = null($_REQUEST['cmbCreditAcc']);
	//$drAcc = null($_REQUEST['cmbDebitAcc']);
	$drAcc = 0;
	
	///////////////////// js parameters /////////////////////
	$mainDetails 	= json_decode($_REQUEST['recPayDetail'], true); 
	$rollbackBillPay = json_decode($_REQUEST['recDelPay'], true); 

	//////////////////////// received payments insert part ///////////////////////
	if($requestType=='add')
	{
            try{
		$voucherNumber 		= getNextVoucherNo($companyId,$locationId);
		$accountPeriod 		= getLatestAccPeriod($companyId);
		$voucherReference	= trim(encodeVoucherNo($voucherNumber,$accountPeriod,$companyId,$locationId));
		$db->begin();
                 //Add data to transaction header*******************************************
                $sql="INSERT INTO fin_transactions (entryDate, strProgramType,documentNo, currencyId, currencyRate, transDetails, payMethodId, paymentNumber, accPeriod, userId, companyId, createdOn) VALUES
                    ('$date','Bill Payment','$voucherReference',$currency,$rate,'$remarks',$payMethod,'$refNo',$accountPeriod,$userId,$companyId,now())";
                
                $db->RunQuery2($sql);
                $entryId=$db->insertId;                
                //********************************************************************************
		$sql = "INSERT INTO fin_billmanager_billpayments_header 
		(`intVoucherNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strReferenceNo`,`dtmDate`,`intCurrencyId`,`dblRate`,`dblPayAmount`,`strPayTo`,`intPayMethodId`, `strPaymentRefNo`,`dtmRefDate`,`intIsPosted`,`strRefOrg`,`strRemark`,`intServiceProId`,`intCreator`,`dtmCreateDate`,`intDeleteStatus`,entryId)
 		VALUES ('$voucherNumber','$accountPeriod','$locationId','$companyId','$voucherReference','$date','$currency','$rate','$payAmount','$payTo','$payMethod', '$refNo','$refDate','$isPosted','$refOrg','$remarks', '$servicePro','$userId',now(),'0',$entryId)";
				
		$firstResult = $db->RunQuery2($sql);
				
		//echo($mainDetails);
		if(count($mainDetails) != 0 && $firstResult)
		{
			
			foreach($mainDetails as $detail)
			{
				$DocNo 			= trim($detail['docuNo']);				
				$invoiceNo 		= trim($detail['invoiceNo']);
				$lastPayDate	= trim($detail['lastPayDate']);				
				$amount 		= val($detail['amount']);
				$toBePaid		= val($detail['toBePaid']);				
				$payAmount 		= val($detail['payAmount']);
				
				$sql = "INSERT INTO fin_billmanager_billpayments_main_details 
				(`intVoucherNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strDocumentNo`,`strReferenceNo`,`strInvoiceNo`,`dtmLastPayDate`,`dblAmount`,`dblToBePaid`,`dblPayAmount`,`intCreator`,`dtmCreateDate`,`intDeleteStatus`) 
				VALUES ('$voucherNumber','$accountPeriod','$locationId','$companyId' ,'$DocNo','$voucherReference','$invoiceNo','$lastPayDate','$amount','$toBePaid','$payAmount','$userId',now(),'0')";
			
				$mainDetailResult = $db->RunQuery2($sql);
			}
		}
		
		//-----------To be Paid-------------------
		if(count($mainDetailResult) != 0 && $firstResult)
		{			
			foreach($mainDetails as $detailToBePaid)
			{
				$invoiceNoToBePaid		= trim($detailToBePaid['invoiceNo']);
				$lastPayDateToBePaid	= trim($detailToBePaid['lastPayDate']);				
				$amountToBePaid 		= val($detailToBePaid['amount']);
				$toBePaidToBePaid		= val($detailToBePaid['toBePaid']);				
				$payAmountToBePaid 		= val($detailToBePaid['payAmount']);
				
				$sql = "UPDATE `fin_billmanager_bills_header` 
						SET `dblToBePaid`='$toBePaidToBePaid',`intModifyer`='$userId',`dtmModifyDate`=now() 
						WHERE `invoiceNo`='$invoiceNoToBePaid'";
				
				$mainDetailResultToBePaid = $db->RunQuery2($sql);
			}
		}
		//-----------
		
		//if($firstResult && $mainDetailResult)
		if($mainDetailResult && $mainDetailResultToBePaid)
		{				
				$sql = "INSERT INTO `fin_billmanager_billpayments_account_details` 
				(`intVoucherNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strReferenceNo`,`intCrAccountId`,`intDrAccountId`,`dblToBePaidAmt`,`dblPayAmt`,`intCreator`,`dtmCreateDate`,`intDeleteStatus`) 
				VALUES ('$voucherNumber','$accountPeriod','$locationId','$companyId','$voucherReference','$crAcc','$drAcc','$toBePaidAmt','$payAmt','$userId',now(),'0')";
				
				$accDetailResult = $db->RunQuery2($sql);
			
		}
		
		//-----------Transaction Credit-------------------------
		if(count($mainDetailResult) != 0 && $accDetailResult)
		{			
			foreach($mainDetails as $detailTransact)
			{
				$invoiceNoTransact		= trim($detailTransact['invoiceNo']);
				$lastPayDateTransact	= trim($detailTransact['lastPayDate']);				
				$amountTransact 		= val($detailTransact['amount']);
				$toBePaidTransact		= val($detailTransact['toBePaid']);				
				$payAmountTransact 		= val($detailTransact['payAmount']);
				
                 $sql = "INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                            ($entryId,'C',$crAcc,$payAmountTransact,'$remarks',null)";
                $mainDetailTransact = $db->RunQuery2($sql);
			}
		}
		
		//-----------Transaction Debit-------------------------
		if(count($mainDetailTransact) != 0 && $accDetailResult)
		{			
			foreach($mainDetails as $detailTransactDr)
			{
				$invoiceNoTransactDr	= trim($detailTransactDr['invoiceNo']);
				$lastPayDateTransactDr	= trim($detailTransactDr['lastPayDate']);				
				$amountTransactDr 		= val($detailTransactDr['amount']);
				$toBePaidTransactDr		= val($detailTransactDr['toBePaid']);				
				$payAmountTransactDr 	= val($detailTransactDr['payAmount']);
				
		               
             //trans action services provider as credit
            //get servoces provider account
            $res = $db->RunQuery2("SELECT intChartOfAccountId FROM mst_financeserviceprovideractivate WHERE intServiceProviderId=$servicePro AND intCompanyId=$companyId");
            $row = mysqli_fetch_array($res);
            $providerAcc = $row['intChartOfAccountId'];
            $sql = "INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId,personType, personId) VALUES 
                        ($entryId,'D',$providerAcc,$payAmountTransactDr,'$refNo',null,'ser',$servicePro)";
              		
		

				$mainDetailTransactDebit = $db->RunQuery2($sql);
			}
		}
		//-----------
		//if($mainDetailResult && $accDetailResult)
	    //if($mainDetailResultToBePaid && $accDetailResult)
		if($mainDetailTransactDebit && $accDetailResult)
		{
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
			$response['voucherNo'] 	= $voucherReference;
                        $db->commit();
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
                        $db->rollback(); //roalback
		}
                echo json_encode($response);
                }catch(Exception $e){               
                $db->rollback();//roalback
                $response['type'] 		= 'fail';
                $response['msg'] 		= $e->getMessage();
                $response['q'] 			= $sql;                
               echo json_encode($response);                 
            }
	}
	////////////////////// received payments update part ////////////////////////
	else if($requestType=='edit')
	{
            try {
                
	$voucherNumber 	= $_REQUEST['cboSearchVoucher'];	
	$voucherReference	= trim(encodeVoucherNo($voucherNumber,$accountPeriod,$companyId,$locationId));
	$db->begin();
		  $sql = "UPDATE `fin_billmanager_billpayments_header` SET 				 
					 dtmDate 		='$date',
					 intCurrencyId 	='$currency',
					 dblRate 		='$rate',
					 dblPayAmount 	='$payAmount',
					 strPayTo 		='$payTo',
					 intPayMethodId ='$payMethod',
					 strPaymentRefNo ='$refNo',
					 dtmRefDate 	='$refDate',
					 intIsPosted 	='$isPosted',
					 strRefOrg 		='$refOrg',
					 strRemark 		='$remarks',
					 intServiceProId='$servicePro',
					 intModifyer 	='$userId',
					 dtmModifyDate 	= now(),
					 intDeleteStatus ='0' 
				WHERE strReferenceNo ='$voucherNumber' ";
		$firstResult = $db->RunQuery2($sql);
		
		if(count($mainDetails) != 0 && $firstResult)
		{
			$sql = "SELECT
						fin_billmanager_billpayments_header.intVoucherNo,
						fin_billmanager_billpayments_header.intAccPeriodId,						
						fin_billmanager_billpayments_header.strReferenceNo,
                                                fin_billmanager_billpayments_header.entryId
						FROM
						fin_billmanager_billpayments_header
						WHERE
						fin_billmanager_billpayments_header.strReferenceNo = '$voucherNumber'";
	
			$result = $db->RunQuery2($sql);
			while($row=mysqli_fetch_array($result))
			{
				$VoucherNum 	= $row['intVoucherNo'];
				$accoPeriod 	= $row['intAccPeriodId'];
                                $entryId= $row['entryId']; 
			}
                        //========update the transaction deader====================
                        $sql="UPDATE fin_transactions SET 
                                    entryDate='$date',                                                        
                                    currencyId=$currency,
                                    currencyRate='$rate',
                                    payMethodId=$payMethod, 
                                    paymentNumber='$refNo',
                                    transDetails='$remarks',                    
                                    accPeriod=$accountPeriod
                            WHERE entryId=$entryId";
                        $db->RunQuery2($sql);

                        $sqld = "DELETE FROM `fin_transactions_details` WHERE entryId=$entryId";
                        $resultd = $db->RunQuery2($sqld);
                        //=========================================================
			
			$sql = "DELETE FROM
						fin_billmanager_billpayments_main_details
					WHERE
						fin_billmanager_billpayments_main_details.strReferenceNo =  '$voucherNumber'";
			$db->RunQuery2($sql);		
			
			foreach($mainDetails as $detail)
			{
				$DocNo 			= trim($detail['docuNo']);				
				$invoiceNo 		= trim($detail['invoiceNo']);
				$lastPayDate	= trim($detail['lastPayDate']);				
				$amount 		= val($detail['amount']);
				$toBePaid		= val($detail['toBePaid']);				
				$payAmount 		= val($detail['payAmount']);
				
				$sql = "INSERT INTO fin_billmanager_billpayments_main_details 
				(`intVoucherNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId` ,`strDocumentNo`,`strReferenceNo`,`strInvoiceNo`,`dtmLastPayDate`,`dblAmount`,`dblToBePaid`,`dblPayAmount`,`intCreator`,`dtmCreateDate`,`intDeleteStatus`) 
				VALUES ('$VoucherNum','$accoPeriod','$locationId','$companyId' ,'$DocNo','$voucherNumber','$invoiceNo','$lastPayDate','$amount','$toBePaid','$payAmount','$userId',now(),'0')";
		
				$mainDetailResult = $db->RunQuery2($sql);
			}
		}
		
		//------------------		
		if(count($mainDetailResult) != 0 && $firstResult)
		{
			$sql = "SELECT
						fin_billmanager_billpayments_main_details.strInvoiceNo
					FROM
						fin_billmanager_billpayments_main_details
					WHERE
						fin_billmanager_billpayments_main_details.intVoucherNo = '$voucherNumber'";
	
			$result = $db->RunQuery2($sql);
			while($row=mysqli_fetch_array($result))
			{
				$invoiceNo 	= $row['strInvoiceNo'];				
			}
			
			/*$sql = "DELETE FROM
						fin_billmanager_billpayments_main_details
					WHERE
						fin_billmanager_billpayments_main_details.strReferenceNo =  '$voucherNumber'";
			$db->RunQuery2($sql);*/
			
			foreach($mainDetails as $detailToBePaid)
			{
				$invoiceNoToBePaid		= trim($detailToBePaid['invoiceNo']);
				$lastPayDateToBePaid	= trim($detailToBePaid['lastPayDate']);				
				$amountToBePaid 		= val($detailToBePaid['amount']);
				$toBePaidToBePaid		= val($detailToBePaid['toBePaid']);				
				$payAmountToBePaid 		= val($detailToBePaid['payAmount']);
				
				$sql = "UPDATE `fin_billmanager_bills_header` 
						SET `dblToBePaid`='$toBePaidToBePaid',`intModifyer`='$userId',`dtmModifyDate`=now() 
						WHERE `invoiceNo`='$invoiceNoToBePaid'";
				
				$mainDetailResultToBePaid = $db->RunQuery2($sql);
			}					
		}
		//------------------
		
		//if($firstResult && $mainDetailResult)
		if($mainDetailResult && $mainDetailResultToBePaid)
		{				
				$sql = "UPDATE fin_billmanager_billpayments_account_details SET							
							 intCrAccountId ='$crAcc',
							 intDrAccountId ='$drAcc',
							 dblToBePaidAmt ='$toBePaidAmt',
							 dblPayAmt ='$payAmt',
							 intModifyer ='$userId',
							 dtmModifyDate =now(),
							 intDeleteStatus ='0' 
						WHERE strReferenceNo ='$voucherNumber'";
				
				$accDetailResult = $db->RunQuery2($sql);
			
		}
		//======================
		//-----------Transaction Credit-------------------------
		if(count($mainDetailResult) != 0 && $accDetailResult)		{
				
			foreach($mainDetails as $detailTransact)
			{
				$invoiceNoTransact		= trim($detailTransact['invoiceNo']);
				$lastPayDateTransact	= trim($detailTransact['lastPayDate']);				
				$amountTransact 		= val($detailTransact['amount']);
				$toBePaidTransact		= val($detailTransact['toBePaid']);				
				$payAmountTransact 		= val($detailTransact['payAmount']);
				
		 $sql = "INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                            ($entryId,'C',$crAcc,$payAmountTransact,'$remarks',null)";
				
				$mainDetailTransact = $db->RunQuery2($sql);
			}
		}
		
		//-----------Transaction Debit-------------------------
		if(count($mainDetailTransact) != 0 && $accDetailResult)
		{
			foreach($mainDetails as $detailTransactDr)
			{
				$invoiceNoTransactDr	= trim($detailTransactDr['invoiceNo']);
				$lastPayDateTransactDr	= trim($detailTransactDr['lastPayDate']);				
				$amountTransactDr 		= val($detailTransactDr['amount']);
				$toBePaidTransactDr		= val($detailTransactDr['toBePaid']);				
				$payAmountTransactDr 	= val($detailTransactDr['payAmount']);
				
		//trans action services provider as credit
            //get servoces provider account
            $res = $db->RunQuery2("SELECT intChartOfAccountId FROM mst_financeserviceprovideractivate WHERE intServiceProviderId=$servicePro AND intCompanyId=$companyId");
            $row = mysqli_fetch_array($res);
            $providerAcc = $row['intChartOfAccountId'];
            $sql = "INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId,personType, personId) VALUES 
                        ($entryId,'D',$providerAcc,$payAmountTransactDr,'$refNo',null,'ser',$servicePro)";
				
				$mainDetailTransactDebit = $db->RunQuery2($sql);
			}
		}
		//-----------
		//====================================
		
		
	
		//if($mainDetailResult && $accDetailResult)
		 if($mainDetailTransactDebit && $accDetailResult)
		{
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Updated successfully.';
			$response['receiptNo'] 	= $receipt;
                        $db->commit();
		}
		else
		{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
                        $db->rollback(); //roalback
		}
                echo json_encode($response);
            } catch (Exception $e) {
                $db->rollback(); //roalback
                $response['type'] = 'fail';
                $response['msg'] = $e->getMessage();
                $response['q'] = $sql;
                echo json_encode($response);
        }
	}	
	
	/////////// delete part ////////////
	else if($requestType=='delete')
	{
            try{
                $db->begin();
		$voucherNumber 	= $_REQUEST['cboSearch'];	
		//$sevicePId =  $_REQUEST['serviceProviderId'];		
		
	//------Rolback Payment Amount--------------------------
		
			$sql = "SELECT
						fin_billmanager_billpayments_main_details.strInvoiceNo,
						fin_billmanager_billpayments_main_details.dblAmount,
						fin_billmanager_billpayments_header.intServiceProId
					FROM
						fin_billmanager_billpayments_main_details
						Inner Join fin_billmanager_billpayments_header ON fin_billmanager_billpayments_main_details.intVoucherNo = fin_billmanager_billpayments_header.intVoucherNo
					WHERE
						fin_billmanager_billpayments_main_details.strReferenceNo = '$voucherNumber'";
	
			$result = $db->RunQuery2($sql);
			
			while($row=mysqli_fetch_array($result))
			{
				$invoNum 	= $row['strInvoiceNo'];
				$amount 	= $row['dblAmount'];				
				$serProNo 	= $row['intServiceProId'];
							
			$sql = "UPDATE `fin_billmanager_bills_header` 
					SET dblToBePaid='$amount',
				   		intModifyer='$userId',
				   		dtmModifyDate=now()
			 WHERE fin_billmanager_bills_header.invoiceNo =  '$invoNum' AND
				   fin_billmanager_bills_header.intServiceProId = '$serProNo'";
				
				$rollbackResult = $db->RunQuery2($sql);		
			}			
			
	//---------------------------------------------	
		$sql = "UPDATE `fin_billmanager_billpayments_header` SET	
		 			 intModifyer ='$userId',
					 dtmModifyDate = now(),				
					 intDeleteStatus ='1' 
				WHERE strReferenceNo ='$voucherNumber' ";
		
		$result = $db->RunQuery2($sql);
                //==========UPDATE TRANS ACTION delete STATUS
                $sql="SELECT fin_billmanager_billpayments_header.entryId FROM fin_billmanager_billpayments_header WHERE (`strReferenceNo`='$voucherNumber')";
                $result = $db->RunQuery2($sql);
                $row = mysqli_fetch_array($result);
                $entryId=$row['entryId'];                        
                $sqld = "UPDATE `fin_transactions` SET delStatus=1 WHERE entryId=$entryId";
                $resultd = $db->RunQuery2($sqld);
                //============================
		
	/*	if(count($mainDetails) != 0 && $firstResult)
		{			
			foreach($mainDetails as $detail)
			{				
				$sql = "UPDATE `fin_billmanager_billpayments_main_details`
						SET `intModifyer`='$userId',
							`dtmModifyDate`=now(),
							`intDeleteStatus`='1' 
						WHERE `strReferenceNo`='$voucherNumber' ";
										
				$mainDetailResult = $db->RunQuery($sql);
			}
		}
		
		if($firstResult && $mainDetailResult)
		{				
				$sql = "UPDATE fin_billmanager_billpayments_account_details SET							
							 intModifyer ='$userId',
							 dtmModifyDate =now(),
							 intDeleteStatus ='1' 
						WHERE strReferenceNo ='$voucherNumber'";
				
				$accDetailResult = $db->RunQuery($sql);
		}*/
		
		//if($mainDetailResult && $accDetailResult)
			
		if(($result)){
			$response['type'] 		= 'pass';			
			$response['msg'] 		= 'Deleted successfully.';
                        $db->commit();
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
                        $db->rollback(); //roalback
		}
                echo json_encode($response);
            } catch (Exception $e) {
                $db->rollback(); //roalback
                $response['type'] = 'fail';
                $response['msg'] = $e->getMessage();
                $response['q'] = $sql;
                echo json_encode($response);
            } 
	
	}
	
//--------------------------------------------------------------------------------------------
	function getNextVoucherNo($companyId,$locationId)
	{
		global $db;
		$sql = "SELECT
					sys_finance_no.intVoucherNo
				FROM sys_finance_no
				WHERE intCompanyId = '$companyId' AND intLocationId = '$locationId'";	
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$nextVoucherNo = $row['intVoucherNo'];
		
		$sql = "UPDATE `sys_finance_no` SET intVoucherNo=intVoucherNo+1 WHERE (intCompanyId = '$companyId' AND intLocationId = '$locationId')";
		$db->RunQuery($sql);	
		return $nextVoucherNo;
	}

//--------------------------------------------------------------
	function getLatestAccPeriod($companyId)
	{
		global $db;
		$sql = "SELECT
				MAX(mst_financeaccountingperiod.intId) AS accId,
				mst_financeaccountingperiod.dtmStartingDate,
				mst_financeaccountingperiod.dtmClosingDate,
				mst_financeaccountingperiod.intStatus,
				mst_financeaccountingperiod_companies.intCompanyId,
				mst_financeaccountingperiod_companies.intPeriodId
				FROM
				mst_financeaccountingperiod
				Inner Join mst_financeaccountingperiod_companies ON mst_financeaccountingperiod_companies.intPeriodId = mst_financeaccountingperiod.intId
				WHERE
				mst_financeaccountingperiod_companies.intCompanyId =  '$companyId' AND mst_financeaccountingperiod.intStatus = '1'
				ORDER BY
				mst_financeaccountingperiod.intId DESC
				";	
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$latestAccPeriodId = $row['accId'];	
		return $latestAccPeriodId;
	}
//-----------------------------------------------------------------

	function encodeVoucherNo($voucherNumber,$accountPeriod,$companyId,$locationId)
	{
		global $db;
		$sql = "SELECT
				mst_financeaccountingperiod.intId,
				mst_financeaccountingperiod.dtmStartingDate,
				mst_financeaccountingperiod.dtmClosingDate,
				mst_financeaccountingperiod.intStatus
				FROM
				mst_financeaccountingperiod
				WHERE
				mst_financeaccountingperiod.intId =  '$accountPeriod'
				";	
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$startDate = substr($row['dtmStartingDate'],0,4);
		$closeDate = substr($row['dtmClosingDate'],0,4);
		$sql = "SELECT
				mst_companies.strCode AS company,
				mst_companies.intId,
				mst_locations.intCompanyId,
				mst_locations.strCode AS location,
				mst_locations.intId
				FROM
				mst_companies
				Inner Join mst_locations ON mst_locations.intCompanyId = mst_companies.intId
				WHERE
				mst_locations.intId =  '$locationId' AND
				mst_companies.intId =  '$companyId'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$companyCode = $row['company'];
		$locationCode = $row['location'];
		//$voucherFormat = $companyCode."/".$locationCode."/".$startDate."-".$closeDate."/".$voucherNumber;
		$voucherFormat = $companyCode."/".$startDate."-".$closeDate."/".$voucherNumber;
		return $voucherFormat;
	}
//============================================================================================

?>
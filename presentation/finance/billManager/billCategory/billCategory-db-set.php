<?php 
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 		= $_REQUEST['requestType'];
	$id 				= $_REQUEST['cboSearch'];
	$categoryName		= $_REQUEST['txtCatName'];
	$serviceProvider	= null($_REQUEST['cboServicePro']);
	$number				= $_REQUEST['txtNumber'];
	
		
	/////////// bill category insert part /////////////////////
	if($requestType=='add')
	{
		 $sql = "INSERT INTO `mst_financebillcategory` 
				(`strCategoryName`,`intServiceProId`,`strNumber`,`intCreator`,`dtmCreateDate`) 
				VALUES ('$categoryName',$serviceProvider,'$number','$userId',now()) ";
		$result = $db->RunQuery($sql);
		if($result){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
	}
	/////////// tax isolated update part /////////////////////  dtmModifyDate   = now()
	else if($requestType=='edit')
	{
		$sql = "UPDATE `mst_financebillcategory` SET
					strCategoryName ='$categoryName',
					intServiceProId =$serviceProvider,
					strNumber ='$number',
					intModifier ='$userId'				
 				WHERE (`intCategoryId`='$id')";
		
		$result = $db->RunQuery($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Updated successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	/////////// tax isolated delete part /////////////////////
	else if($requestType=='delete')
	{
		$sql = "DELETE FROM `mst_financebillcategory` WHERE (`intCategoryId`='$id')  ";
		$result = $db->RunQuery($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Deleted successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	echo json_encode($response);
?>
<?php
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	
	/////////// bill category load part /////////////////////
	if($requestType=='loadCombo')
	{
		$sql =  		"SELECT
						mst_financebillcategory.strCategoryName,
						mst_financeserviceprovider.strProviderName,
						mst_financebillcategory.strNumber,
						mst_financebillcategory.intServiceProId,
						mst_financeserviceprovider.intProviderId,
						mst_financebillcategory.intCategoryId
						FROM
						mst_financeserviceprovider
						right outer Join mst_financebillcategory ON mst_financebillcategory.intServiceProId = mst_financeserviceprovider.intProviderId";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{		
			$response['catName'] 		= $row['strCategoryName'];
			$response['providerName'] 	= $row['strProviderName'];
			$response['number'] 		= $row['strNumber'];			
		}
		echo json_encode($response);		
	}
	
	else if($requestType=='loadDetails')
	{
		$id  = $_REQUEST['id'];
		$sql = "SELECT
						mst_financebillcategory.strCategoryName,
						mst_financeserviceprovider.strProviderName,
						mst_financebillcategory.strNumber,
						mst_financebillcategory.intServiceProId,
						mst_financeserviceprovider.intProviderId,
						mst_financebillcategory.intCategoryId
						FROM
						mst_financeserviceprovider
						right outer Join mst_financebillcategory ON mst_financebillcategory.intServiceProId = mst_financeserviceprovider.intProviderId
						WHERE mst_financebillcategory.intCategoryId =  '$id' ";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['catName'] 			= $row['strCategoryName'];
			$response['providerName'] 	= $row['strProviderName'];
			$response['number'] 		= $row['strNumber'];			
		}
		echo json_encode($response);
	}
	
?>
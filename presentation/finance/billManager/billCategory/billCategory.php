<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];	
$thisFilePath =  $_SERVER['PHP_SELF'];
include  "{$backwardseperator}dataAccess/permisionCheck.inc";
$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$companyId = $row['intCompanyId']; 
}
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bill Category</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="billCategory-js.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

<link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css">
</head>

<body>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<form id="frmBillCategory" name="frmBillCategory" method="post" action="billCategory-db-set.php" autocomplete="off" >
<div align="center">
  <div class="trans_layoutD">
	<div class="trans_text">Bill Category</div>
 <table width="100%">
<tr>
<td width="100%"><table width="100%">
  <tr>
    <td width="25" class="normalfnt">&nbsp;</td>
    <td width="96" bgcolor="#FFFFFF" class="">&nbsp;</td>
    <td width="154" bgcolor="#FFFFFF" class=""><span class="normalfnt">Category Name / Code <span class="compulsoryRed">*</span></span></td>
    <td width="186" bgcolor="#FFFFFF" class=""><input type="text" name="txtCatName" id="txtCatName" style="width:120px" /></td>
    <td width="87" bgcolor="#FFFFFF" class="">&nbsp;</td>
    <td width="46" bgcolor="#FFFFFF" class="">&nbsp;</td>
  </tr>
  <tr>
    <td class="normalfnt">&nbsp;</td>
    <td bgcolor="#FFFFFF" class="">&nbsp;</td>
    <td bgcolor="#FFFFFF" class="normalfnt">Service Provider</td>
    <td bgcolor="#FFFFFF" class=""><span class="normalfntMid">
      <select name="cboServicePro" id="cboServicePro" style="width:180px">
       	<option value=""></option>
        <?php  $sql = "SELECT intProviderId, strProviderName
					   FROM mst_financeserviceprovider
					   Inner Join mst_financeserviceprovideractivate ON mst_financeserviceprovider.intProviderId = mst_financeserviceprovideractivate.intServiceProviderId
					   ORDER BY strProviderName ASC";
							$result = $db->RunQuery($sql);
							while($row=mysqli_fetch_array($result))
							{
								echo "<option value=\"".$row['intProviderId']."\">".$row['strProviderName']."</option>";
							}
        ?>
      </select>
    </span></td>
    <td bgcolor="#FFFFFF" class="">&nbsp;</td>
    <td bgcolor="#FFFFFF" class="">&nbsp;</td>
  </tr>
  <tr>
    <td class="normalfnt">&nbsp;</td>
    <td bgcolor="#FFFFFF" class="">&nbsp;</td>
    <td bgcolor="#FFFFFF" class="normalfnt">Number</td>
    <td bgcolor="#FFFFFF" class=""><input type="text" name="txtNumber" id="txtNumber" style="width:120px" /></td>
    <td bgcolor="#FFFFFF" class="">&nbsp;</td>
    <td bgcolor="#FFFFFF" class="">&nbsp;</td>
  </tr>
</table></td>
</tr>
<tr>
  <td><div style="overflow:scroll;width:100%;height:200px;" id="divGrid">
    <table width="100%" id="tblMainGrid" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
      <tr class="">
        <td width="20%" height="27"    bgcolor="#FAD163" class="normalfntMid"><span class="normalfnt"><strong>Category Name</strong></span></td>
        <td width="56%"    bgcolor="#FAD163" class="normalfntMid"><span class="normalfnt"><strong>Service Provider</strong></span></td>
        <td  bgcolor="#FAD163" class="normalfntMid"  ><span class="normalfnt"><strong>Number</strong></span></td>
      </tr>
      
        <?php
	  $sql = "SELECT
						mst_financebillcategory.strCategoryName,
						mst_financeserviceprovider.strProviderName,
						mst_financebillcategory.strNumber,
						mst_financebillcategory.intServiceProId,
						mst_financeserviceprovider.intProviderId,
						mst_financebillcategory.intCategoryId
						FROM
						mst_financeserviceprovider
						right outer Join mst_financebillcategory ON mst_financebillcategory.intServiceProId = mst_financeserviceprovider.intProviderId ";	
					
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
	  ?>
      <tr class="normalfnt">                
        
      <td id=<?php echo $row['intCategoryId'];?> class="loadId" align="center" bgcolor="#FFFFFF"><u class="mouseOver butName"><?php echo $row['strCategoryName'];?></u></td>
        <td align="center" id="<?php echo $row['intProviderId']; ?>" bgcolor="#FFFFFF"><?php echo $row['strProviderName'];?></td>
        <td align="center" bgcolor="#FFFFFF"><?php echo $row['strNumber'];?></td>       
       
      </tr>
      <?php 
        } 
       ?>
      
      
      
     
    </table>
  </div></td>
</tr>
<tr>
  <td width="100%" align="center" bgcolor=""><img style="display:none" border="0" src="../../../../images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:none" border="0" src="../../../../images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/><img style="display:none" border="0" src="../../../../images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="25"/><a href="../../../../main.php"><img  src="../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
</tr>
</table>
</div>
</div>    
</form>   
</body>
</html>
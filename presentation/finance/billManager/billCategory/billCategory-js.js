	/*    	id = $(this).attr('id');
		$('#frmBillCategory').validationEngine('hide');
		var url = "billCategory-db-get.php";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:'requestType=loadDetails&id='+id,
			async:false,
			
			success:function(json){
					$('#frmBillCategory #txtCatName').val(json.catName);
					$('#frmBillCategory #cboServicePro').val(json.providerName);
					$('#frmBillCategory #txtNumber').val(json.number);					
			}*/		
			
$(document).ready(function() {
		var id = '';
		var taxCode = '';
  		$("#frmBillCategory").validationEngine();
		$('#frmBillCategory #txtCatName').focus();
		
  //permision for add 
  if(intAddx)
  {
 	$('#frmBillCategory #butNew').show();
	$('#frmBillCategory #butSave').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmBillCategory #butSave').show();
	$('#frmBillCategory #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmBillCategory #butDelete').show();
	$('#frmBillCategory #cboSearch').removeAttr('disabled');
  }
  
  //permision for view
  if(intViewx)
  {
	$('#frmBillCategory #cboSearch').removeAttr('disabled');
  }
  
  $("#tblMainGrid").each(function () {
  $('.loadId').css('cursor', 'pointer');
  $('.loadId').mouseover(function () {
  $(this).css('color', 'red');
  });
  $('.loadId').mouseout(function () {
  $(this).css('color', 'black');
  });
  });
  
  ///save button click event
  //$('.butName').live();
  $('#frmBillCategory #butSave').click(function(){
	var requestType = '';
	if ($('#frmBillCategory').validationEngine('validate'))   
    { 
		if(taxCode=='')
			requestType = 'add';
			
		else
			requestType = 'edit';
		
		var url = "billCategory-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json",  
			data:$("#frmBillCategory").serialize()+'&requestType='+requestType+'&cboSearch='+id,
			async:false,
			
			success:function(json){
					$('#frmBillCategory #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						id = '';
						taxCode = '';
						$('#frmBillCategory').get(0).reset();
						var t=setTimeout("alertx()",1000);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmBillCategory #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
				}		
			});
	}
   });
   
   /////////////////////////////////////////////////////
   //// load bill category details -  //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmBillCategory #tblMainGridRow').click(function(){
	   $('#frmBillCategory').validationEngine('hide');
   });
   $('#tblMainGrid .loadId').click(function() {
		var categoryName = $(this).find('.butName').html();
		var providerId	 = $(this).parent().find('td:eq(1)').attr('id');
		var number = $(this).parent().find('td:eq(2)').html();
		//alert(number);
		$('#txtCatName').val(categoryName);
		$('#cboServicePro').val(providerId);
		$('#txtNumber').val(number);
		//alert(categoryName);
		id = $(this).attr('id');
		taxCode = $('#frmBillCategory #txtCatName').val();
	});
	
	
	
	
	$('#frmBillCategory #butNew').click(function(){
		$('#frmBillCategory').get(0).reset();
		id = '';
		taxCode = '';
		$('#frmBillCategory #txtCatName').focus();
		loadCombo_frmBillCategory();
	});
    $('#frmBillCategory #butDelete').click(function(){
		if(taxCode =='')
		//if('#frmBillCategory #cboSearch').val()=='')
		{			
			$('#frmBillCategory #butDelete').validationEngine('showPrompt', 'Please select code.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+taxCode+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = "billCategory-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											data:'requestType=delete&cboSearch='+id,
											async:false,
											success:function(json){
												$('#frmBillCategory #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													var t=setTimeout("alertDelete()",1000);
													return;
												}	
												//var t=setTimeout("alertDelete()",3000);
											}	 
										});
					}
				}
		 	});
		}
	});
});
function loadCombo_frmBillCategory()
{
	location.reload();
}
function alertx()
{
	$('#frmBillCategory #butSave').validationEngine('hide')	;
	loadCombo_frmBillCategory();
}
function alertDelete()
{
	$('#frmBillCategory #butDelete').validationEngine('hide')	;
	loadCombo_frmBillCategory();
}

<?php

session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$userId = $_SESSION['userId'];
include "{$backwardseperator}dataAccess/Connector.php";
$response = array('type' => '', 'msg' => '');
$companyId = $_SESSION['headCompanyId'];
$locationId = $_SESSION["CompanyID"];
////////////////////////// main parameters ////////////////////////////////
$billNo = trim($_REQUEST['txtbillNo']);
$requestType = $_REQUEST['requestType'];
$id = $_REQUEST['cboSearch'];
$searchId = $_request['&cboSearchBill'];
///////////////////// bill header parameters ////////////////////
$billType = $_REQUEST['txtbillType'];
$date = trim($_REQUEST['txtDate']);
$servicePro = null($_REQUEST['txtServicePro']);
$number = $_REQUEST['txtNumber'];
$billFrom = $_REQUEST['txtBillFrom'];
$billTo = $_REQUEST['txtBillTo'];
$lstDayPay = $_REQUEST['txtLastDayPay'];
$transact = $_REQUEST['txtTransaction'];
$invoiceNo = $_REQUEST['txtInvoiceNo'];
$currency = $_REQUEST['cboCurrency'];
$rate = val(trim($_REQUEST['txtRate']));
$totAmt = val(trim($_REQUEST['txtTotAmtMonth']));
$remarks = $_REQUEST['txtRemarks'];
//$crAcc = $_REQUEST['cmbCreditAcc'];
$crAcc = 0;

///////////////////// bill detail parameters /////////////////////
$details = json_decode($_REQUEST['billDetails'], true);
///////////////////////////////////////////////////////////////////////////
//////////////////////// bill insert part ///////////////////////
if ($requestType == 'add') {
    try {
        $billNumber = getNextBillNo($companyId, $locationId);
        $accountPeriod = getLatestAccPeriod($companyId);
        $billReference = trim(encodeBillNo($billNumber, $accountPeriod, $companyId, $locationId));
        //$billN = trim(BillNo($billNumber));

        $db->begin();
        $sql = "SELECT
			mst_financeserviceprovider.intProviderId
		  FROM
			mst_financeserviceprovider
		  WHERE
			mst_financeserviceprovider.strProviderName =  '$servicePro'";
        $result = $db->RunQuery2($sql);
        $row = mysqli_fetch_array($result);
        //$serviceProId =  $row['intProviderId'];


        $serviceProId = $row['intProviderId'];
        if ($serviceProId == '') {
            $serviceProId = 0;
        }
        //---------------Validate Duplicate invoice No for same service provider-----------------------
        $sql = "SELECT
			 fin_billmanager_bills_header.intServiceProId
		  FROM
			 fin_billmanager_bills_header
		  WHERE
			 fin_billmanager_bills_header.invoiceNo =  '$invoiceNo' AND fin_billmanager_bills_header.intServiceProId =  '$serviceProId'";
        $checkDuplicate = $db->RunQuery2($sql);
        $row = mysqli_fetch_array($checkDuplicate);

        $duplicate = $row['intServiceProId'];
        if ($duplicate != '') {
            $response['msg'] = 'Duplicate Invoice Number for same service provider.';
        } else {
            //---------------------------------------
            //Add data to transaction header*******************************************
            $sql = "INSERT INTO fin_transactions (entryDate, strProgramType, documentNo, currencyId, currencyRate, transDetails, payMethodId, paymentNumber, accPeriod, userId, companyId, createdOn) VALUES
                    ('$date','Bill','$billReference',$currency,$rate,'$transact',null,null,$accountPeriod,$userId,$companyId,now())";

            $db->RunQuery2($sql);
            $entryId = $db->insertId;
            //********************************************************************************

            $sql = "INSERT INTO `fin_billmanager_bills_header` (`intBillNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strReferenceNo`,`strBillType`,`intCatNo`,`intServiceProId`,`dtmDate`,`dtmBillFrom`,`dtmBillTo`,
                        `dtmLastPayDate`,`transaction`,`invoiceNo`,`currencyId`,`dblRate`,`intTotAmtMonth`,`strRemark`,`intcreditAccId`,`dblToBePaid`,`intCreator`,`dtmCreateDate`,`intDeleteStatus`,entryId)
                        VALUES ('$billNumber','$accountPeriod','$locationId','$companyId','$billReference','$billType','$number','$serviceProId','$date','$billFrom','$billTo',
                            '$lstDayPay','$transact','$invoiceNo','$currency','$rate','$totAmt','$remarks','$crAcc','$totAmt','$userId',now(),'0',$entryId)";

            $firstResult = $db->RunQuery2($sql);
            $totalAmouunt = 0;
            if (count($details) != 0 && $firstResult) {
                foreach ($details as $detail) {
                    $drAcc = $detail['drAccId'];
                    $amount = val($detail['amount']);
                    $memo = $detail['memo'];
                    $dimension = null($detail['dimensionId']);


                    $sql = "INSERT INTO `fin_billmanager_bills_details` 
                            (`intBillNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strReferenceNo`,`intAcountId`,`dblAmount`,`strMemo`,`intDimensionId`,`intCreator`,`dtmCreateDate`,`intDeleteStatus`) 
                            VALUES ('$billNumber','$accountPeriod','$locationId','$companyId','$billReference','$drAcc','$amount','$memo','$dimension','$userId',now(),'0')	";

                    $finalResult = $db->RunQuery2($sql);

                    //add to Trans action details as debit
                    $sql = "INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                            ($entryId,'D',$drAcc,$amount,'$memo',$dimension)";
                    $db->RunQuery2($sql);
                    $totalAmouunt+=$amount;
                }
            }
            //trans action services provider as credit
            //get servoces provider account
            $res = $db->RunQuery2("SELECT intChartOfAccountId FROM mst_financeserviceprovideractivate WHERE intServiceProviderId=$serviceProId AND intCompanyId=$companyId");
            $row = mysqli_fetch_array($res);
            $providerAcc = $row['intChartOfAccountId'];
            $sql = "INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId,personType, personId) VALUES 
                            ($entryId,'C',$providerAcc,$totalAmouunt,'$transact',null,'ser',$serviceProId)";
            $db->RunQuery2($sql);

            if ($finalResult) {
                $db->commit();
                $response['type'] = 'pass';
                $response['msg'] = 'Saved successfully.';
                $response['billNo'] = $billReference;
            } else {
                $db->rollback(); //roalback
                $response['type'] = 'fail';
                $response['msg'] = $db->errormsg;
                $response['q'] = $sql;
            }
        }
        echo json_encode($response);
    } catch (Exception $e) {
        $db->rollback(); //roalback
        $response['type'] = 'fail';
        $response['msg'] = $e->getMessage();
        $response['q'] = $sql;
        echo json_encode($response);
    }
}

//======================================================================================
////////////////////// bill update part ////////////////////////
else if ($requestType == 'edit') {
    try {
        $db->begin();
        $refNo = $_REQUEST['cboSearchBill'];

        $sql = "SELECT
			fin_billmanager_bills_details.intBillNo
		FROM
			fin_billmanager_bills_details
		WHERE
			fin_billmanager_bills_details.strReferenceNo =  '$refNo'";
        $result = $db->RunQuery($sql);
        $row = mysqli_fetch_array($result);
        //$serviceProId =  $row['intProviderId'];


        $billNum = $row['intBillNo'];

        //-------------------------------------	
        $sql = "SELECT
			mst_financeserviceprovider.intProviderId
		  FROM
			mst_financeserviceprovider
		  WHERE
			mst_financeserviceprovider.strProviderName =  '$servicePro'";
        $result = $db->RunQuery($sql);
        $row = mysqli_fetch_array($result);
        //$serviceProId =  $row['intProviderId'];


        $serviceProId = $row['intProviderId'];
        if ($serviceProId == '') {
            $serviceProId = 0;
        }

        $sql = "UPDATE `fin_billmanager_bills_header` 
					SET 
					`strBillType`='$billType',
					`intCatNo`='$number',
					`intServiceProId`='$serviceProId',
					`dtmDate`='$date',
					`dtmBillFrom`='$billFrom',
					`dtmBillTo`='$billTo',
					`dtmLastPayDate`='$lstDayPay',
					`transaction`='$transact',
					`invoiceNo`='$invoiceNo',
					`currencyId`='$currency',
					`dblRate`='$rate',
					`intTotAmtMonth`='$totAmt',
					`strRemark`='$remarks',
					`intcreditAccId`='$crAcc',
					`dblToBePaid`='$totAmt',					
					`intModifyer`='$userId',
					`dtmModifyDate`=now(),
					`intDeleteStatus` ='0' 
					WHERE(`strReferenceNo`='$refNo')";
        $firstResult = $db->RunQuery($sql);
        if (count($details) != 0 && $firstResult) {
            $sql = "SELECT
						fin_billmanager_bills_header.intBillNo,
						fin_billmanager_bills_header.intAccPeriodId,
						fin_billmanager_bills_header.strReferenceNo,
                                                fin_billmanager_bills_header.entryId
					FROM
						fin_billmanager_bills_header
					WHERE
						fin_billmanager_bills_header.strReferenceNo	=  '$refNo'";
            $result = $db->RunQuery($sql);
            while ($row = mysqli_fetch_array($result)) {
                $billNumber = $row['intBillNo'];
                $accountPeriod = $row['intAccPeriodId'];
                $entryId = $row['entryId'];
            }
            //========update the transaction deader====================
            $sql = "UPDATE fin_transactions SET 
                    entryDate='$date',                                        
                    currencyId=$currency,
                    currencyRate='$rate',
                    transDetails='$remarks',                    
                    accPeriod=$accountPeriod
            WHERE entryId=$entryId";
            $db->RunQuery2($sql);

            $sqld = "DELETE FROM `fin_transactions_details` WHERE entryId=$entryId";
            $resultd = $db->RunQuery2($sqld);
            //=========================================================

            $sql = "DELETE FROM fin_billmanager_bills_details
						WHERE fin_billmanager_bills_details.strReferenceNo =  '$refNo' ";
            $db->RunQuery($sql);
            $totalAmouunt = 0;
            foreach ($details as $detail) {
                $drAcc = $detail['drAccId'];
                $amount = val($detail['amount']);
                $memo = $detail['memo'];
                $dimension = null($detail['dimensionId']);

                $sql = "INSERT INTO `fin_billmanager_bills_details` (`intBillNo`,`intAccPeriodId`,`intLocationId`,`intCompanyId`,`strReferenceNo`,`intAcountId`,`dblAmount`,`strMemo`,`intDimensionId`,`intCreator`,`dtmCreateDate`,`intDeleteStatus`) VALUES ('$billNum','$accountPeriod','$locationId','$companyId','$refNo','$drAcc','$amount','$memo','$dimension','$userId',now(),'0')";

                $finalResult = $db->RunQuery($sql);

                //add to Trans action details as debit
                $sql = "INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId) VALUES 
                            ($entryId,'D',$drAcc,$amount,'$memo',$dimension)";
                $db->RunQuery2($sql);
                $totalAmouunt+=$amount;
            }
            //trans action services provider as credit
            //get servoces provider account
            $res = $db->RunQuery2("SELECT intChartOfAccountId FROM mst_financeserviceprovideractivate WHERE intServiceProviderId=$serviceProId AND intCompanyId=$companyId");
            $row = mysqli_fetch_array($res);
            $providerAcc = $row['intChartOfAccountId'];
            $sql = "INSERT INTO fin_transactions_details (entryId,`credit/debit`,accountId,amount,details,dimensionId,personType, personId) VALUES 
                        ($entryId,'C',$providerAcc,$totalAmouunt,'$transact',null,'ser',$serviceProId)";
            $db->RunQuery2($sql);
        }
        if ($finalResult) {
            $db->commit();
            $response['type'] = 'pass';
            $response['msg'] = 'Updated successfully.';
            $response['billNo'] = $billReference;
        } else {
            $response['type'] = 'fail';
            $response['msg'] = $db->errormsg;
            $response['q'] = $sql;
            $db->rollback(); //roalback
        }
        echo json_encode($response);
    } catch (Exception $e) {
        $db->rollback(); //roalback
        $response['type'] = 'fail';
        $response['msg'] = $e->getMessage();
        $response['q'] = $sql;
        echo json_encode($response);
    }
}
//========================================================================
/////////// exchange rate delete part /////////////////////
else if ($requestType == 'delete') {
    try {
        $db->begin();
        $refNum = $_REQUEST['cboSearch'];
        $serProName = $_REQUEST['serviceProviderId'];
        $invNum = $_REQUEST['invoiceNo'];

//-----Get Service provider id----------------

        $sql = "SELECT
				mst_financeserviceprovider.intProviderId
		  FROM
				mst_financeserviceprovider
		  WHERE
				mst_financeserviceprovider.strProviderName =  '$serProName'";
        $result = $db->RunQuery($sql);
        $row = mysqli_fetch_array($result);

        $serviceProId = $row['intProviderId'];

//---------------Check payment bill in bill before delete-----------------------
        $sql = "SELECT
			fin_billmanager_billpayments_header.intServiceProId,
			fin_billmanager_billpayments_main_details.strInvoiceNo
		  FROM
			fin_billmanager_billpayments_header
			Inner Join fin_billmanager_billpayments_main_details ON fin_billmanager_billpayments_header.intVoucherNo = fin_billmanager_billpayments_main_details.intVoucherNo
			Inner Join fin_billmanager_bills_header ON fin_billmanager_billpayments_main_details.strInvoiceNo = fin_billmanager_bills_header.invoiceNo
		  WHERE
			fin_billmanager_bills_header.intServiceProId =  '$serviceProId' AND
			fin_billmanager_bills_header.invoiceNo =  '$invNum'";

        $checkPaid = $db->RunQuery($sql);
        $row = mysqli_fetch_array($checkPaid);

        $paidSerProId = $row['intServiceProId'];
        $paidInvoiceNo = $row['strInvoiceNo'];

        if ($paidSerProId != '' && $paidInvoiceNo != '') {
            $response['msg'] = 'Payment this Invoice No.';
        } else {
            //---------------------------------------				

            /* 	$sql = "DELETE FROM fin_billmanager_bills_header 
              WHERE fin_billmanager_bills_header.strReferenceNo = '$refNum'";
              $result = $db->RunQuery($sql);

              $sql = "DELETE FROM fin_billmanager_bills_details
              WHERE fin_billmanager_bills_details.strReferenceNo =  '$refNum' ";

              $result = $db->RunQuery($sql); */

            $sql = "UPDATE fin_billmanager_bills_header 
					SET intModifyer='$userId',
						dtmModifyDate= now(),
						intDeleteStatus='1' 
					WHERE strReferenceNo='$refNum' ";

            $result = $db->RunQuery($sql);

            $sql = "UPDATE fin_billmanager_bills_details
 					SET intModifyer='$userId',
					dtmModifyDate= now(),
					intDeleteStatus='1' 
					WHERE strReferenceNo='$refNum'";

            $result = $db->RunQuery($sql);
            //==========UPDATE TRANS ACTION delete STATUS
            $sql = "SElect fin_billmanager_bills_header.entryId FROM fin_billmanager_bills_header WHERE (`strReferenceNo`='$refNum')";
            $result = $db->RunQuery2($sql);
            $row = mysqli_fetch_array($result);
            $entryId = $row['entryId'];
            $sqld = "UPDATE `fin_transactions` SET delStatus=1 WHERE entryId=$entryId";
            $resultd = $db->RunQuery2($sqld);
            //============================

            if (($result)) {
                $db->commit();
                $response['type'] = 'pass';
                $response['msg'] = 'Deleted successfully.';
            } else {
                $db->rollback(); //roalback
                $response['type'] = 'fail';
                $response['msg'] = $db->errormsg;
                $response['q'] = $sql;
            }
        }
        echo json_encode($response);
    } catch (Exception $e) {
        $db->rollback(); //roalback
        $response['type'] = 'fail';
        $response['msg'] = $e->getMessage();
        $response['q'] = $sql;
        echo json_encode($response);
    }
}

//--------------------------------------------------------------------------------------------
function getNextBillNo($companyId, $locationId) {
    global $db;
    $sql = "SELECT
					sys_finance_no.intBillNo
				FROM
					sys_finance_no
				WHERE
					intCompanyId = '$companyId' AND intLocationId = '$locationId'
				";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $nextInvoiceNo = $row['intBillNo'];

    $sql = "UPDATE `sys_finance_no` SET intBillNo=intBillNo+1 WHERE (intCompanyId = '$companyId' AND intLocationId = '$locationId')";
    $db->RunQuery($sql);
    return $nextInvoiceNo;
}

//--------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------
function getLatestAccPeriod($companyId) {
    global $db;
    $sql = "SELECT
				MAX(mst_financeaccountingperiod.intId) AS accId,
				mst_financeaccountingperiod.dtmStartingDate,
				mst_financeaccountingperiod.dtmClosingDate,
				mst_financeaccountingperiod.intStatus,
				mst_financeaccountingperiod_companies.intCompanyId,
				mst_financeaccountingperiod_companies.intPeriodId
				FROM
				mst_financeaccountingperiod
				Inner Join mst_financeaccountingperiod_companies ON mst_financeaccountingperiod_companies.intPeriodId = mst_financeaccountingperiod.intId
				WHERE
				mst_financeaccountingperiod_companies.intCompanyId =  '$companyId' AND mst_financeaccountingperiod.intStatus = '1'
				ORDER BY
				mst_financeaccountingperiod.intId DESC
				";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $latestAccPeriodId = $row['accId'];
    return $latestAccPeriodId;
}

//--------------------------------------------------------------------------------------------
//============================================================================================
function encodeBillNo($billNumber, $accountPeriod, $companyId, $locationId) {
    global $db;
    $sql = "SELECT
				mst_financeaccountingperiod.intId,
				mst_financeaccountingperiod.dtmStartingDate,
				mst_financeaccountingperiod.dtmClosingDate,
				mst_financeaccountingperiod.intStatus
				FROM
				mst_financeaccountingperiod
				WHERE
				mst_financeaccountingperiod.intId =  '$accountPeriod'
				";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $startDate = substr($row['dtmStartingDate'], 0, 4);
    $closeDate = substr($row['dtmClosingDate'], 0, 4);
    $sql = "SELECT
				mst_companies.strCode AS company,
				mst_companies.intId,
				mst_locations.intCompanyId,
				mst_locations.strCode AS location,
				mst_locations.intId
				FROM
				mst_companies
				Inner Join mst_locations ON mst_locations.intCompanyId = mst_companies.intId
				WHERE
				mst_locations.intId =  '$locationId' AND
				mst_companies.intId =  '$companyId'
				";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $companyCode = $row['company'];
    $locationCode = $row['location'];
    //$billFormat = $companyCode."/".$locationCode."/".$startDate."-".$closeDate."/".$billNumber;
    $billFormat = $companyCode . "/" . $startDate . "-" . $closeDate . "/" . $billNumber;
    return $billFormat;


    /* global $db;	

      $billFormat = $companyCode."/".$locationCode."/".$startDate."-".$closeDate."/".$billNumber;
      return $billFormat; */
}

/* function BillNo($billNumber)
  {
  global $db;

  $billNum = $billNumber;
  return $billNum;
  } */
//============================================================================================
?>
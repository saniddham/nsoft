// JavaScript Document

$(document).ready(function() {
		var id = '';
		var taxCode = '';
  		$("#frmBills").validationEngine();
		$('#frmBills #txtTotAmtMonth').val('0.00');
		//$('#frmBills #txtCatName').focus();
		  //permision for add 
		  
$('.delImg').live('click',function(){	
	var rowId = $(this).parent().parent().parent().find('tr').length;
	if(rowId!=2)
	$(this).parent().parent().remove();
	finalAmount();
});

$('.delImg').css('cursor', 'pointer');

	  if(intAddx)
	  {
		$('#frmBills #butNew').show();
		$('#frmBills #butSave').show();
	  }
  
	  //permision for edit 
	  if(intEditx)
	  {
		$('#frmBills #butSave').show();
		$('#frmBills #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
	  }
  
  //permision for delete
	  if(intDeletex)
	  {		  
		$('#frmBills #butDelete').show();
		$('#frmBills #cboSearch').removeAttr('disabled');
	  }
  
  //permision for view
	  if(intViewx)
	  {
		$('#frmBills #cboSearch').removeAttr('disabled');
	  } 
  
  
	  $('#frmBills #chkEdit').click(function(){
		  if($('#frmBills #chkEdit').attr('checked'))
		  {
			  $("#frmBills #txtRate").attr("readonly","");
			  $('#frmBills #txtRate').focus();
		  }
		  else
		  {
			  $('#frmBills #txtRate').val('');
			  $("#frmBills #txtRate").attr("readonly","readonly");
			  $('#frmBills #cboCurrency').change();
		  }
	  }); 
// Button New --------------------------- 
		$('#frmBills #butNew').click(function(){
		$('#frmBills').get(0).reset();
		
		$('#tblMainGrid2 >tbody >tr').each(function(){
			if($(this).index()!=0 && $(this).index()!=1 )
			{
				$(this).remove();
			}
		});
		$('#partPay').hide();
		$('#fullPay').hide();
		$('#overPay').hide();
		$('#pending').hide();
		
		loadCombo_frmBills();
		
		$('#frmBills #cboSearch').focus();
		});
		
//======================		
		
	 /*	$('#frmReceivedPayments #butNew').click(function(){		
		document.getElementById('allInvoice').innerHTML = "";		
		$('#frmReceivedPayments #cboPaymentsMethods').change();
		$('#frmReceivedPayments #cboCustomer').attr("disabled","");		
	});	
		*/

//---------Load Total Amount for the Month to grid--------------- 

	$('.debitacc').live('change',function(){		
		//amount = $('#frmBills #txtTotAmtMonth').val();		
		//$('#frmBills #txtAmount').val(amount);
		
		$(this).parent().parent().find(".amount").val((eval($('#txtTotAmtMonth').val()==''?0:$('#txtTotAmtMonth').val()) - eval($('#txtTotal').val()==''?0:$('#txtTotal').val())).toFixed(2));
		finalAmount();
	});
	
// delete button click vent

 $('#frmBills #butDelete').click(function(){
	 
	 var billRefNo=document.getElementById("cboSearchBill").value; 	
	 var requestType = 'delete';
	 var serviceProNo=document.getElementById("txtServicePro").value; 
	 var invoiceNo=document.getElementById("txtInvoiceNo").value; 
	 
	
	
		if($('#frmBills #cboSearchBill').val()=='')
		{
			$('#frmBills #butDelete').validationEngine('showPrompt', 'Please select Bill.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}		
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmBills #cboSearchBill option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = "bills-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											//data:'requestType=delete&cboSearch='+$('#frmBills #cboSearchBill').val(),
											//check???????????
											data:$("#frmBills").serialize()+'&requestType='+requestType+'&cboSearch='+billRefNo+'&serviceProviderId='+serviceProNo+'&invoiceNo='+invoiceNo,
											async:false,
											success:function(json){
												
												$('#frmBills #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													$('#frmBills').get(0).reset();
													loadCombo_frmBills();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
								}
		 	});
			
		}
	});

//save button click event

$('#frmBills #butSave').click(function(){
	var drAccId = "";
	var amount = "";
	var memo = "";
	var dimensionId = "";	
			
 value="[ ";
	$('#tblMainGrid2 tr:not(:first)').each(function(){
		
		drAccId		= $(this).find(".debitacc").val();   // debitacc load from bills.php class
		amount 		= $(this).find(".amount").val();
		memo 		= $(this).find(".memo").val();	
		dimensionId = $(this).find(".dimension").val();
		
	value += '{ "drAccId":"'+drAccId+'", "amount": "'+amount+'", "memo": "'+memo+'", "dimensionId": "'+dimensionId+'" },';
	});
	
	value = value.substr(0,value.length-1);
	value += " ]";
  
//-------------------------------------------------
 	
	var billRefNo=document.getElementById("cboSearchBill").value; 
	//alert(billRefNo); 
	var requestType = '';
	if ($('#frmBills').validationEngine('validate')) 
    { 
		if((eval($('#txtTotal').val()) - eval($('#txtTotAmtMonth').val()))==0)
		{
			if($('#frmBills #cboSearchBill').val()=='')		
				requestType = 'add';	  		
			else		
				requestType = 'edit';
				
			
			var url = "bills-db-set.php";
			var obj = $.ajax({
				url:url,
				dataType: "json",  
				data:$("#frmBills").serialize()+'&requestType='+requestType+'&cboSearchBill='+billRefNo+'&billDetails='+value,
				async:false,
							
				success:function(json){
						$('#frmBills #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
						if(json.type=='pass')
						{
							id = '';
							taxCode = '';
							$('#frmBills').get(0).reset();
							var t=setTimeout("alertx()",1000);
							$('#txtbillNo').val(json.billNo);
							//loadCombo_frmSalesInvoice();
							return;
						}
						var t=setTimeout("alertx()",3000);
					},				
					
				error:function(xhr,status){
						
						$('#frmBills #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						var t=setTimeout("alertx()",3000);
					}	
						
				});
		}
		else
		{
			$('#frmBills #butSave').validationEngine('showPrompt', 'You cannot allow this process untill total amount for the month and total amount are same','fail');
				var t=setTimeout("alertx()",5000);
		}
	}
  }); 
 //---------------------- // check amount ------------------

 $('.amount').live('change',function(){
	 //alert("aaaaaaa");
	 var amount=0;
	$('#tblMainGrid2 tr:not(:first)').each(function(){		
		amount+= parseFloat($(this).find(".amount").val());		
	});
	
	$('#frmBills #txtTotal').val(amount);
	
	
	//fgfdgfd=amount;
	
	}); 	
//----------------------------
  
});  //end of ready
//------------------------------------------------------------------------------

function functionList()
{
	//var billRefNo=document.getElementById("cboSearch").value;
	var billRefNo = $('#frmBills #cboSearch').val();

	if(billRefNo!='')
	{
		loadExistingDetails(billRefNo);
	}
}


//-------------------------------------------------------------------------------
   //// load Bill type details according to reference no //////////////////////////
   
   $('#frmBills #cboSearch').click(function(){
	   $('#frmBills').validationEngine('hide');
   });
   //-------------------------------------------------
   function loadExistingDetails(billRefNo){
		$('#frmBills').validationEngine('hide');
		var url = "bills-db-get.php"; 
		if($('#frmBills #cboSearch').val()=='')
		{
			$('#frmBills').get(0).reset();return;	
		}
	
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:'requestType=loadDetails&billRefNo='+billRefNo,
			async:false,
			success:function(json){
					//json  = eval('('+json+')');
					//$('#frmBills #cboSearch').val(json.refNo);
					$('#frmBills #txtNumber').val(json.billNo);
					$('#frmBills #txtbillType').val(json.billType);
					$('#frmBills #txtServicePro').val(json.proName);
					$('#frmBills #cmbCreditAcc').val(json.chartOfAccId);								
			}
		});
   }
   
// Load details according to bill reference----------------
function functionBillDetails()
{
	//var billRefNo=document.getElementById("cboSearch").value;
	var billRefNumber = $('#frmBills #cboSearchBill').val();
	//alert(billRefNumber);
	if(billRefNumber!='')
	{
		loadBillDetails(billRefNumber);
	}
}
//----------------------------

 function loadBillDetails(billRefNumber){
		$('#frmBills').validationEngine('hide');
		var url = "bills-db-get.php"; 
		if($('#frmBills #cboSearchBill').val()=='')
		{
			$('#partPay').hide();
			$('#fullPay').hide();
			
			$('#frmBills').get(0).reset();return;	
		}
	
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:'requestType=loadBillDetails&billReferenceNo='+billRefNumber,
			async:false,
			success:function(json){
				
					$('#frmBills #txtbillNo').val(json.billNoD);
					$('#frmBills #txtbillType').val(json.billTypeD);
					$('#frmBills #txtServicePro').val(json.serProD);
					$('#frmBills #txtNumber').val(json.catNoD);
					$('#frmBills #txtDate').val(json.dateD);
					$('#frmBills #txtBillFrom').val(json.billFromD);
					$('#frmBills #txtBillTo').val(json.billToD);
					$('#frmBills #txtLastDayPay').val(json.lastPDateD);
					$('#frmBills #txtTransaction').val(json.transactD);
					$('#frmBills #txtInvoiceNo').val(json.invoiceNoD);
					$('#frmBills #cboCurrency').val(json.curIdD);
					$('#frmBills #txtRate').val(json.rateD);
					$('#frmBills #txtTotAmtMonth').val(json.totAmtMonD);
					$('#frmBills #txtRemarks').val(json.remarksD);
					$('#frmBills #cmbCreditAcc').val(json.crAccD);
					
		//-----------------------------------6.12------------------------------------------------
		//toBePaidAmt
		/*if(json.totAmount != json.balAmount)
		{
			if(json.balAmount > 0)
			{
				$('#partPay').show();
				$('#fullPay').hide();
				$('#overPay').hide();
				$('#pending').hide();
			}
			else if(json.balAmount == 0)
			{
				$('#fullPay').show();
				$('#partPay').hide();
				$('#overPay').hide();
				$('#pending').hide();
			}
			if(json.balAmount < 0)
			{
				$('#partPay').hide();
				$('#fullPay').hide();
				$('#overPay').show();
				$('#pending').hide();
			}
		}
		else
		{
			$('#partPay').hide();
			$('#fullPay').hide();
			$('#overPay').hide();
			$('#pending').show();
		}		*/		
		
		if(json.toBePaidAmt == 0)
		{
			$('#fullPay').show();
			$('#partPay').hide();			
		}		
		else
		{
			$('#partPay').show();
			$('#fullPay').hide();
		}
		//------------------------
			$('#tblMainGrid2 >tbody >tr').each(function(){
			if($(this).index()!=0 && $(this).index()!=1 )
			{
				$(this).remove();
			}
		});
		var debitacc 	= "";
		var amount 		= "";
		var memo		= "";		
		var dimension 	= "";
				
		if(json.detailVal!=null)
		{
			var rowId = $('#tblMainGrid2').find('tr').length;
			var tbl = document.getElementById('tblMainGrid2');
			rows = $('#tblMainGrid2').find('tr').length;
			for(var j=0;j<=json.detailVal.length-1;j++)
			{
				debitacc	= json.detailVal[j].accId;
				amount		= json.detailVal[j].amount;
				memo		= json.detailVal[j].memo;
				dimension	= json.detailVal[j].dimensionId;
										
				if(j != json.detailVal.length-1)
				{					
					tbl.insertRow(rows);
					tbl.rows[rows].innerHTML = tbl.rows[rows-1].innerHTML;
					tbl.rows[rows].cells[1].childNodes[1].value = debitacc;					
					tbl.rows[rows].cells[2].childNodes[0].value = amount;
					tbl.rows[rows].cells[3].childNodes[0].value = memo;
					tbl.rows[rows].cells[4].childNodes[0].value = dimension;					
				}
				else
				{		
							
					tbl.rows[1].cells[1].childNodes[1].value = debitacc;									
					tbl.rows[1].cells[2].childNodes[0].value = amount;					
					tbl.rows[1].cells[3].childNodes[0].value = memo;
					tbl.rows[1].cells[4].childNodes[0].value = dimension;					
				}				
			}			
			finalAmount();
		}
		else
		{
			
		}
					
								
			}
		});
   }
 
//---------------------------------------------------------
function changeRate()
{
	//var billRefNo=document.getElementById("cboSearch").value;
	var currencyType = $('#frmBills #cboCurrency').val();
	
	if(currencyType!='')
	{
		loadCurrencyRate(currencyType);
	}
}
//-----------------------------------------------------------------------------

function loadCurrencyRate(currencyType){
	var url = "bills-db-get.php?requestType=getExchangeRate&currencyId="+currencyType+'&exchangeDate='+$('#txtDate').val();
	var obj = $.ajax({url:url,dataType:'json',success:function(json){
		//alert(json.buyingRate);
		$('#rdbBuying').val(json.buyingRate);
		$('#rdbSelling').val(json.sellingRate);
		document.getElementById("txtRate").value=json.sellingRate;
		
	//--------------------------------------------------------	
	$('#frmBills #rdbSelling').click(function(){
					document.getElementById("txtRate").value=json.sellingRate;
					//document.getElementById("txtRate").innerHTML=json.buyingRate;
  	});
 	 //-------------------------------------------------------
  	$('#frmBills #rdbBuying').click(function(){
		//$('#frmBills #rdbBuying').checked == false;
		document.getElementById("txtRate").value=json.buyingRate;
 	 });
	 
	 //-------------------------------------------------------
	 
	  $('#frmBills #chkEdit').click(function(){
	  if(document.getElementById("chkEdit").checked==true)
		$('#frmBills #txtRate').removeAttr('disabled');
		else
		$('#frmBills #txtRate').attr("disabled",true);

  });			
		},async:false});
	
}

//---------------------------------------------------
function insertRow()
{
	var tbl = document.getElementById('tblMainGrid2');	
	var rows = tbl.rows.length;
	//tbl.rows[1].cells[5].childNodes[0].nodeValue;
	tbl.insertRow(rows);
	tbl.rows[rows].innerHTML = tbl.rows[rows-1].innerHTML;
}

function loadCombo_frmBills()
{
	var url 	= "bills-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmBills #cboSearchBill').html(httpobj.responseText);
}

function finalAmount()
{
	var finalTotal = 0.00;
	$(".amount").each( function(){
          finalTotal += eval($(this).val()==''?0.00:$(this).val());
	});
	$('#txtTotal').val(finalTotal.toFixed(2));
}
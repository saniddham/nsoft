<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$companyId = $row['intCompanyId']; 
	}
	/////////// Bill load part /////////////////////
	if($requestType=='loadCombo')
	{
		$sql = "SELECT
					fin_billmanager_bills_header.strReferenceNo
				FROM
					fin_billmanager_bills_header
				WHERE
					fin_billmanager_bills_header.intCompanyId  =  '$companyId'";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['strReferenceNo']."\">".$row['strReferenceNo']."</option>";
		}
		echo $html;
	}	
	
	else if($requestType=='loadDetails')
	{
		$id  = $_REQUEST['billRefNo'];	
		
		$sql   = "SELECT
					mst_financebillcategory.strCategoryName,
					mst_financeserviceprovider.strProviderName,
					mst_financebillcategory.strNumber,
					mst_financeserviceprovideractivate.intChartOfAccountId
				FROM
					mst_financebillcategory
					Left Join mst_financeserviceprovider ON mst_financebillcategory.intServiceProId = mst_financeserviceprovider.intProviderId
					Inner Join mst_financeserviceprovideractivate ON mst_financeserviceprovideractivate.intServiceProviderId = mst_financeserviceprovider.intProviderId
				WHERE
					mst_financebillcategory.intCategoryId =  '$id'";
	
		
		/*$sql   = "SELECT						
						mst_financebillcategory.strCategoryName,
						mst_financeserviceprovider.strProviderName,
						mst_financebillcategory.strNumber
				   FROM
						mst_financebillcategory
						left Join mst_financeserviceprovider ON mst_financebillcategory.intServiceProId = 			mst_financeserviceprovider.intProviderId
				   WHERE
						mst_financebillcategory.intCategoryId =  '$id'"; */
		 				
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			//$response['refNo'] 		= $row['strReferenceNo'];
			$response['billNo'] 	= $row['strNumber'];
			$response['billType'] 	= $row['strCategoryName'];
			$response['proName'] 	= $row['strProviderName'];
			$response['chartOfAccId'] 	= $row['intChartOfAccountId'];			
			
		}
		echo json_encode($response);
	}		 
		 
	else if($requestType=='loadBillDetails')
	{		
		 $refN  = $_REQUEST['billReferenceNo'];
	
		//-------------------------------------------------------------
		$sql = "SELECT
					fin_billmanager_bills_details.intBillNo,
					fin_billmanager_bills_details.intAccPeriodId,
					fin_billmanager_bills_details.intLocationId,
					fin_billmanager_bills_details.intCompanyId,
					fin_billmanager_bills_details.strReferenceNo,
					fin_billmanager_bills_details.intAcountId,
					fin_billmanager_bills_details.dblAmount,
					fin_billmanager_bills_details.strMemo,
					fin_billmanager_bills_details.intDimensionId,
					fin_billmanager_bills_details.intCreator,
					fin_billmanager_bills_details.dtmCreateDate,
					fin_billmanager_bills_details.intModifyer,
					fin_billmanager_bills_details.dtmModifyDate
				FROM
					fin_billmanager_bills_details
				WHERE
					fin_billmanager_bills_details.strReferenceNo =  '$refN'";
				
		$result = $db->RunQuery($sql);
		$arrDetail;
		while($row=mysqli_fetch_array($result))
		{
			$val['accId'] 		= $row['intAcountId'];
			$val['amount'] 	= $row['dblAmount'];
			$val['memo'] 		= $row['strMemo'];
			$val['dimensionId'] 	= $row['intDimensionId'];	
			
			$arrDetail[] = $val;
		}
		$response['detailVal'] = $arrDetail; 
		//-----------------------------------------------------------
		
		 $sql   = "SELECT
						fin_billmanager_bills_header.intBillNo,
						fin_billmanager_bills_header.intAccPeriodId,
						fin_billmanager_bills_header.intLocationId,
						fin_billmanager_bills_header.intCompanyId,
						fin_billmanager_bills_header.strBillType,
						mst_financeserviceprovider.strProviderName,
						fin_billmanager_bills_header.intCatNo,
						fin_billmanager_bills_header.dtmDate,
						fin_billmanager_bills_header.dtmBillFrom,
						fin_billmanager_bills_header.dtmBillTo,
						fin_billmanager_bills_header.dtmLastPayDate,
						fin_billmanager_bills_header.transaction,
						fin_billmanager_bills_header.invoiceNo,
						fin_billmanager_bills_header.currencyId,
						fin_billmanager_bills_header.dblRate,
						fin_billmanager_bills_header.intTotAmtMonth,
						fin_billmanager_bills_header.strRemark,
						fin_billmanager_bills_header.intcreditAccId,
						fin_billmanager_bills_header.strReferenceNo
					FROM
						fin_billmanager_bills_header
						left Join mst_financeserviceprovider ON fin_billmanager_bills_header.intServiceProId = mst_financeserviceprovider.intProviderId
					WHERE
						fin_billmanager_bills_header.strReferenceNo =  '$refN'"; 				
												
		$result = $db->RunQuery($sql);
		
		if($result == '')
		{
			echo ("aaaaa");
		}
			
	
		while($row=mysqli_fetch_array($result))
		{			
			$response['billNoD'] 	= $row['strReferenceNo'];			
			$response['billTypeD'] 	= $row['strBillType'];
			$response['serProD'] 	= $row['strProviderName'];
			//$response['serProD'] 	= $serviceProId;
			$response['catNoD'] 	= $row['intCatNo'];
			$response['dateD'] 	= $row['dtmDate'];
			$response['billFromD'] 	= $row['dtmBillFrom'];
			$response['billToD'] 	= $row['dtmBillTo'];
			$response['lastPDateD'] 	= $row['dtmLastPayDate'];
			$response['transactD'] 	= $row['transaction'];
			$response['invoiceNoD'] 	= $row['invoiceNo'];
			$response['curIdD'] 	= $row['currencyId'];
			$response['rateD'] 	= $row['dblRate'];
			$response['totAmtMonD'] 	= $row['intTotAmtMonth'];
			$response['remarksD'] 	= $row['strRemark'];
			$response['crAccD'] 	= $row['intcreditAccId'];
			
		}
		
		//--------------------------------------
		$sql = "SELECT
						fin_billmanager_bills_header.intBillNo,
						fin_billmanager_bills_header.strReferenceNo,						
						fin_billmanager_bills_header.invoiceNo,						
						fin_billmanager_bills_header.intTotAmtMonth,
						fin_billmanager_bills_header.dblToBePaid
					FROM
						fin_billmanager_bills_header
					WHERE
						fin_billmanager_bills_header.strReferenceNo = '$refN' AND fin_billmanager_bills_header.intDeleteStatus =  '0'";
			$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['toBePaidAmt'] 	= $row['dblToBePaid'];
			//$response['totAmountMon'] 	= $row['intTotAmtMonth'];
		}
		//--------------------------------------
		
		
		echo json_encode($response);
	}
	
	else if($requestType=='getExchangeRate')
	{
		$currencyId  	= $_REQUEST['currencyId'];
		$exchangeDate	= $_REQUEST['exchangeDate'];
		
		$sql = "SELECT
					mst_financeexchangerate.dblSellingRate,
					mst_financeexchangerate.dblBuying
				FROM mst_financeexchangerate
				WHERE
					mst_financeexchangerate.intCurrencyId 	=  '$currencyId' AND
					mst_financeexchangerate.dtmDate 		=  '$exchangeDate'
				";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		
		if(mysqli_num_rows($result)>0)
		{
			$arrValue['sellingRate'] 	= $row['dblSellingRate'];
			$arrValue['buyingRate'] 	= $row['dblBuying'];
		}
		else
		{
			$arrValue['sellingRate'] 	= "";
			$arrValue['buyingRate'] 	= "";
		}
		echo json_encode($arrValue);
	}

?>
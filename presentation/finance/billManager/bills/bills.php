<?php	
	session_start();
	$backwardseperator = "../../../../";
	$mainPath = $_SESSION['mainPath'];
	
	$thisFilePath =  $_SERVER['PHP_SELF'];
	include  "{$backwardseperator}dataAccess/permisionCheck.inc";
	$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$companyId = $row['intCompanyId']; 
	}
	
 $categoryNo = $_GET['cboSearch'];
?>
<script type="application/javascript" >
var billRefNo = '<?php echo $categoryNo ?>';
</script>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bills</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script src="bills-js.js" type="text/javascript"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css">


</head>

<body onLoad="functionBillDetails()">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator.'Header.php'; ?></td>
	</tr> 
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>
<style type="text/css">
.apDiv1 {
	position:absolute;
	left:179px;
	top:116px;
	width:auto;
	height:auto;
	z-index:0;
}
</style>

<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<div id="partPay" class= "apDiv1 maskHide" ><img src="../../../../images/partPayment.png"  /></div>
<div id="fullPay" class= "apDiv1 maskHide" ><img src="../../../../images/paymentCompleted.png"  /></div>
<div id="overPay" class= "apDiv1 maskHide" ><img src="../../../../images/overPayment.png"  /></div>
<div id="pending" class= "apDiv1 maskHide" ><img src="../../../../images/pending2.png"  /></div>

<form id="frmBills" name="frmBills"  method="post" action="">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
</table>


<div align="center">
  <div class="trans_layoutL">
    <div class="trans_text">Bills</div>
<table width="100%">
<tr>
	<td align="right"><span class="normalfntMid">Bill</span></td>
        <td align="left"><span class="normalfntMid">
          <select name="cboSearchBill" id="cboSearchBill"  style="width:195px" onChange="functionBillDetails()">
            <option value=""></option>
            <?php   $sql = "SELECT
								fin_billmanager_bills_header.strReferenceNo
							FROM
								fin_billmanager_bills_header
							WHERE
								fin_billmanager_bills_header.intCompanyId  =  '$companyId' AND fin_billmanager_bills_header.intDeleteStatus =  '0'";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							if($row['strReferenceNo']==$categoryNo){
							echo "<option value=\"".$row['strReferenceNo']."\" selected=\"selected\">".$row['strReferenceNo']."</option>";
							}
							else{
							echo "<option value=\"".$row['strReferenceNo']."\">".$row['strReferenceNo']."</option>";
							}
						}
          ?>
          </select>
        </span></td>
      </tr>
    
      <td width="50%" colspan="2" align="left">
        <table align="right">
          <tr>
            <td align="right"><span class="normalfnt">Bill Number </span></td>
            <td align="right"><span class="normalfnt">
              <input name="txtbillNo" type="text" disabled="disabled" class="normalfntRight" id="txtbillNo" value="<?php echo $billNo ?>" style="width:175px" />
              </span>
              </td>
            </tr>
          </table>
        </td>  
    </tr>
    <tr>
      <td colspan="2">
      <table width="100%" class="tableBorder_allRound">
    <tr>
      <td bgcolor="#F2F2F2" class="normalfnt">&nbsp;</td>
      <td bgcolor="#F2F2F2" class="normalfnt">&nbsp;</td>
      <td align="center" bgcolor="#F2F2F2"><span class="normalfnt">Bill Type </span></td>
      <td colspan="2" bgcolor="#F2F2F2"><span class="normalfntMid">
                 
          <select name="cboSearch" id="cboSearch"  style="width:180px" onChange="functionList()"  >
            <option value=""></option>
            <?php   $sql = "SELECT 
								mst_financebillcategory.intCategoryId,
								CONCAT(mst_financebillcategory.strCategoryName, ' - ', mst_financebillcategory.strNumber) As billType
							FROM
								mst_financebillcategory ";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intCategoryId']."\">".$row['billType']."</option>";
						}
          ?>
          </select>       
          <!--SELECT fin_billmanager_bills_header.strReferenceNo 
							FROM fin_billmanager_bills_header 
							WHERE fin_billmanager_bills_header.intCompanyId =  '$companyId'-->
      </span></td>
      <td bgcolor="#F2F2F2">&nbsp;</td>
    </tr>
    <tr>
      <td width="61" class="normalfnt">&nbsp;</td>
      <td width="181" class="normalfnt">Bill Type <span class="compulsoryRed">*</span></td>
      <td width="214"><span class="normalfntMid">
        <input name="txtbillType" type="text" readonly="readonly" id="txtbillType" style="width:195px" class="normalfnt"/>
      </span></td>
      <td width="96"><span class="normalfnt">Date <span class="compulsoryRed">*</span></span></td>
      <td width="288" bgcolor="#FFFFFF" class=""><input name="txtDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="txtbox" id="txtDate" style="width:98px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
      <td width="40">&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="normalfnt">Service Provider</span></td>
      <td><span class="normalfntMid">
        <input name="txtServicePro" type="text" readonly="readonly" id="txtServicePro" style="width:195px" class="normalfnt" />
      </span></td>
      <td><span class="normalfnt">Number <span class="compulsoryRed">*</span></span><span class="compulsoryRed"></span></td>
      <td><span class="normalfntMid">
        <input name="txtNumber" type="text" readonly="readonly" id="txtNumber" style="width:195px" class="normalfnt" />
      </span></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="normalfnt">Bill From</span></td>
      <td><input name="txtBillFrom" type="text" value="<?php echo date("Y-m-d"); ?>" class="txtbox" id="txtBillFrom" style="width:98px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
      <td class="normalfnt">Bill To</td>
      <td><input name="txtBillTo" type="text" value="<?php echo date("Y-m-d"); ?>" class="txtbox" id="txtBillTo" style="width:98px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong class="normalfnt">Last Date for Payments</strong></td>
      <td><input name="txtLastDayPay" type="text" value="<?php echo date("Y-m-d"); ?>" class="txtbox" id="txtLastDayPay" style="width:98px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
      <td><strong class="normalfnt">Transaction</strong></td>
      <td><span class="normalfntMid">
        <textarea name="txtTransaction" id="txtTransaction" style="width:195px"></textarea>
      </span></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong class="normalfnt">Invoice Number</strong><strong class="normalfnt"></strong></td>
      <td><span class="normalfntMid">
        <input type="text" name="txtInvoiceNo" id="txtInvoiceNo" style="width:150px" />
      </span></td>
      <td><strong class="normalfnt"></strong></td>
      <td><span class="normalfntMid">
       
      </span></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="normalfnt">Currency <span class="compulsoryRed">*</span></span></td>
      <td><select name="cboCurrency" id="cboCurrency" style="width:140px" class="validate[required]" onChange="changeRate()">
        <option value=""></option>
        <?php  $sql = "SELECT
						intId,
						strCode
						FROM mst_financecurrency
						WHERE
							intStatus = 1
						order by strCode
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strCode']."</option>";
						}
        				?>
      </select></td>
      <td><span class="normalfnt">Rate</span> <span class="compulsoryRed">*</span></td>
      <td><span class="normalfnt"><span class="normalfntMid">
        <input class="rdoRate" type="radio" name="rdbSelect" id="rdbSelling" value="" checked="checked"/>
        Selling
        <input class="rdoRate" type="radio" name="rdbSelect" id="rdbBuying" value="" />
        Buying
        
        <input type="text" name="txtRate" id="txtRate" style="width:75px; background-color:#9F9; border:thin; text-align:center" readonly="readonly" class="validate[custom[number],required] normalfntBlue"/>
		<input type="checkbox" name="chkEdit" id="chkEdit" />       
       
      </span></span></td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><strong class="normalfnt">Total Amount for the Month</strong><strong class="normalfnt"></strong></td>
      <td colspan="2"><span class="normalfntMid">
        <input type="text" name="txtTotAmtMonth" id="txtTotAmtMonth" style="width:125px;background-color:#FCF;border:none;text-align:center" class="validate[required] TotalAmount" />
      </span></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><span class="normalfnt">Memo</span></td>
      <td colspan="2"><textarea name="txtRemarks" id="txtRemarks" cols="45" rows="2"></textarea></td>
      <td>&nbsp;</td>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td bgcolor="#F2F2F2">&nbsp;</td>
      <td bgcolor="#F2F2F2" class="normalfnt"><!--<strong>Credit Account</strong> --></td>
      <td colspan="2" bgcolor="#F2F2F2"><span class="normalfntMid"><!--
       <select name="cmbCreditAcc" id="cmbCreditAcc" style="width:210px" class="validate[required]">
        <option value=""></option>
        <?php /* $sql = "SELECT
							mst_financechartofaccounts.intId,
							mst_financechartofaccounts.strCode,
							mst_financechartofaccounts.strName,
							mst_financechartofaccounts_companies.intCompanyId,
							mst_financechartofaccounts_companies.intChartOfAccountId
							FROM
							mst_financechartofaccounts
							Inner Join mst_financechartofaccounts_companies ON mst_financechartofaccounts_companies.intChartOfAccountId = mst_financechartofaccounts.intId
							WHERE
							intStatus = '1' AND ( intFinancialTypeId = '28' Or  intFinancialTypeId = '18') AND strType = 'Posting' AND intCompanyId = '$companyId'
							order by strCode";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
						}
        				*/?>
      </select>      
       
      --></span></td>
      <td bgcolor="#F2F2F2">&nbsp;</td>
      <td bgcolor="#F2F2F2">&nbsp;</td>
    </tr>
      </table>
      </td>
      </tr>
      <tr>
      <td colspan="2" align="center">
      </td>
      </tr>
      <tr>
        <td colspan="2" align="center">
          
        </td>
      </tr>
      <tr>
        <td colspan="2" align="center">
          <table width="100%" class="tableBorder_allRound">
            <tr>
              <td align="center" bgcolor="#FFFFFF" class="normalfntMid"><strong>Debit Account</strong></td>
            </tr>
          </table>
        </td>
      </tr>
      <td width="50%"></tr>
            <tr>
      <td colspan="2" align="right"><img src="../../../../images/Tadd.jpg" width="92" height="24" onClick="insertRow();" /></td>
      </tr>
    <tr>
      <td colspan="2"><table width="100%">
        <tr>
          <td>
                  <div style="overflow:scroll;width:900px;height:150px;" id="divGrid"><table width="97%" id="tblMainGrid2" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
        <tr class="">
          <td width="22" bgcolor="#FAD163" class="normalfntMid">Del</td>
          <td width="211"  height="24" bgcolor="#FAD163" class="normalfntMid"><strong>Account</strong></td>
          <td width="135"  bgcolor="#FAD163" class="normalfntMid"><strong>Amount</strong></td>
          <td width="399" bgcolor="#FAD163" class="normalfntMid"  ><strong>Memo</strong></td>
          <td width="100" bgcolor="#FAD163" class="normalfntMid"  ><strong>Cost Center</strong></td>
        </tr>
        <tr class="normalfnt">
          <td bgcolor="#FFFFFF" class="normalfntMid"><img src="../../../../images/del.png" width="15" height="15" class="delImg"/></td>
          <td bgcolor="#FFFFFF" class="normalfntMid">
           <select name="cmbDebitAcc" id="cmbDebitAcc" style="width:210px" class="validate[required] debitacc">
        		<option value=""></option>
       				 <?php  $sql = "SELECT
										mst_financechartofaccounts.intId,
										CONCAT(mst_financechartofaccounts.strCode, ' - ', mst_financechartofaccounts.strName) As debitAcc
									FROM
										mst_financechartofaccounts
									WHERE
										mst_financechartofaccounts.strType =  'Posting'";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['debitAcc']."</option>";
						}
        			?>
      </select>          
        </td>
          <td bgcolor="#FFFFFF"><input name="txtAmount" type="text" id="txtAmount" style="width:135px" class="validate[custom[number]] amount"  /></td>
          <td  bgcolor="#FFFFFF"><input type="text" name="txtMemo" id="txtMemo" style="width:400px" class="memo"/></td>
          <td  bgcolor="#FFFFFF"><select name="cmbDimention" id="cmbDimention" style="width:100px" class="validate[required] dimension normalfntMid">
        		<option value=""></option>
       				 <?php  $sql = "SELECT
										mst_financedimension.intId,
										mst_financedimension.strName
									FROM
										mst_financedimension ORDER BY strName ASC";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
						}
        			?>
      		</select>   
          </td>
        </tr>
      </table>
      </div>
            </td>
          </tr>
          <tr>
          <td colspan="2">
          <table width="476">
                    <tr>
                      <td width="227" align="right" class="normalfntRight">Total </td>
                      <td width="135"><input name="txtTotal" type="text" disabled="disabled" id="txtTotal" style="width:135px" /></td>
                      <td width="98" class="normalfnt">&nbsp;</td>
                    </tr>
                  </table>
          </td>
          </tr>
        </table></td>
    </tr>
      <tr>
        <td colspan="2">
          <table width="100%">
            <tr>
              <td width="100%" height="34" class="tableBorder_allRound"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                <tr>
          <td width="100%" align="center" bgcolor=""><img style="display:none" border="0" src="../../../../images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img  style="display:none" border="0" src="../../../../images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/><img style="display:none" border="0" src="../../../../images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="25"/><a href="../../../../main.php"><img  src="../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                </tr>
              </table></td>
            </tr>
          </table>
        </td>
      </tr>
    </table>
</div>
</div>

</form>
</body>
</html>
<?php
function convert_number($number)
{
	if ($number >= 0)
	{
		$Gn = floor($number / 1000000);  /* Millions (giga) */
		$number -= $Gn * 1000000;
		$kn = floor($number / 1000);     /* Thousands (kilo) */
		$number -= $kn * 1000;
		$Hn = floor($number / 100);      /* Hundreds (hecto) */
		$number -= $Hn * 100;
		$Dn = floor($number / 10);       /* Tens (deca) */
		$n = $number % 10;               /* Ones */
	
		$res = "";
	
		if ($Gn)
		{
			$res .= convert_number($Gn) . " Million";
		}
	
		if ($kn)
		{
			$res .= (empty($res) ? "" : " ") .
				convert_number($kn) . " Thousand";
		}
	
		if ($Hn)
		{
			$res .= (empty($res) ? "" : " ") .
				convert_number($Hn) . " Hundred";
		}
	
		$ones = array("", "One", "Two", "Three", "Four", "Five", "Six",
			"Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen",
			"Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen",
			"Nineteen");
		$tens = array("", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty",
			"Seventy", "Eigthy", "Ninety");
	
		if ($Dn || $n)
		{
			if (!empty($res))
			{
				$res .= " ";
			}
	
			if ($Dn < 2)
			{
				$res .= $ones[$Dn * 10 + $n];
			}
			else
			{
				$res .= $tens[$Dn];
	
				if ($n)
				{
					$res .= " " . $ones[$n];
				}
			}
		}
	
		if (empty($res))
		{
			$res = "zero";
		}
	
		return $res;
    }
	else if($number < 0)
	{
		$number = (-1)*($number);
		
		$Gn = floor($number / 1000000);  /* Millions (giga) */
		$number -= $Gn * 1000000;
		$kn = floor($number / 1000);     /* Thousands (kilo) */
		$number -= $kn * 1000;
		$Hn = floor($number / 100);      /* Hundreds (hecto) */
		$number -= $Hn * 100;
		$Dn = floor($number / 10);       /* Tens (deca) */
		$n = $number % 10;               /* Ones */
	
		$res = "";
	
		if ($Gn)
		{
			$res .= convert_number($Gn) . " Million";
		}
	
		if ($kn)
		{
			$res .= (empty($res) ? "" : " ") .
				convert_number($kn) . " Thousand";
		}
	
		if ($Hn)
		{
			$res .= (empty($res) ? "" : " ") .
				convert_number($Hn) . " Hundred";
		}
	
		$ones = array("", "One", "Two", "Three", "Four", "Five", "Six",
			"Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen",
			"Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eightteen",
			"Nineteen");
		$tens = array("", "", "Twenty", "Thirty", "Fourty", "Fifty", "Sixty",
			"Seventy", "Eigthy", "Ninety");
	
		if ($Dn || $n)
		{
			if (!empty($res))
			{
				$res .= " ";
			}
	
			if ($Dn < 2)
			{
				$res .= $ones[$Dn * 10 + $n];
			}
			else
			{
				$res .= $tens[$Dn];
	
				if ($n)
				{
					$res .= " " . $ones[$n];
				}
			}
		}
	
		if (empty($res))
		{
			$res = "zero";
		}
	
		return "Minus ".$res;
	}
}		
?>
<?php
function getName($account, $pId, $companyId)
{
	global $db;
		
	$sqlOne = "SELECT   
			mst_financechartofaccounts.intId, 
			mst_financialsubtype.strName
			FROM
			mst_financialsubtype
			Inner Join mst_financechartofaccounts ON mst_financialsubtype.intId = mst_financechartofaccounts.intFinancialTypeId
			WHERE
			mst_financechartofaccounts.intId =  '$account'";
		$resultOne = $db->RunQuery($sqlOne);
		$rowOne=mysqli_fetch_array($resultOne);
		
		$chartOfAcc=$rowOne['intId'];
		if($rowOne['strName']=='Accounts Receivable'){
		$sqlTwo = "SELECT
				mst_financecustomeractivate.intCustomerId as id,
				mst_customer.strName
				FROM
				mst_financecustomeractivate
				Inner Join mst_customer ON mst_financecustomeractivate.intCustomerId = mst_customer.intId
				WHERE
				mst_financecustomeractivate.intCompanyId =  '$companyId' AND
				mst_financecustomeractivate.intChartOfAccountId =  '$chartOfAcc' AND
				mst_customer.intId =  '$pId'";
			 	$resultTwo = $db->RunQuery($sqlTwo);
			 	$rowTwo=mysqli_fetch_array($resultTwo);
			 	return $rowTwo['strName'];
		}
		else if($rowOne['strName']=='Accounts Payable'){
		$sqlTwo = "SELECT
				mst_financesupplieractivate.intSupplierId as id,
				mst_supplier.strName
				FROM
				mst_financesupplieractivate
				Inner Join mst_supplier ON mst_financesupplieractivate.intSupplierId = mst_supplier.intId
				WHERE
				mst_financesupplieractivate.intCompanyId =  '$companyId' AND
				mst_financesupplieractivate.intChartOfAccountId =  '$chartOfAcc' AND
				mst_supplier.intId =  '$pId'";
				$resultTwo = $db->RunQuery($sqlTwo);
			 	$rowTwo=mysqli_fetch_array($resultTwo);
			 	return $rowTwo['strName'];
		}
		else if($rowOne['strName']=='Lease' || $rowOne['strName']=='Loan' || $rowOne['strName']=='Other Non Current Liabilities' || $rowOne['strName']=='Other Current Liabilities'){
		$sqlTwo = "SELECT
				mst_finance_service_supplier_activate.intSupplierId as id,
				mst_finance_service_supplier.strName
				FROM
				mst_finance_service_supplier_activate
				Inner Join mst_finance_service_supplier ON mst_finance_service_supplier_activate.intSupplierId = mst_finance_service_supplier.intId
				WHERE
				mst_finance_service_supplier_activate.intCompanyId =  '$companyId' AND
				mst_finance_service_supplier_activate.intChartOfAccountId =  '$chartOfAcc' AND
				mst_finance_service_supplier.intId =  '$pId'";
				$resultTwo = $db->RunQuery($sqlTwo);
			 	$rowTwo=mysqli_fetch_array($resultTwo);
			 	return $rowTwo['strName'];
		}
		else if($rowOne['strName']=='Interest In suspenses' || $rowOne['strName']=='Prepayments' || $rowOne['strName']=='Inter Company' || $rowOne['strName']=='Other Current Assets' || $rowOne['strName']=='Other Non - Current Assets'){
		$sqlTwo = "SELECT
				mst_finance_service_customer_activate.intCustomerId as id,
				mst_finance_service_customer.strName
				FROM
				mst_finance_service_customer_activate
				Inner Join mst_finance_service_customer ON mst_finance_service_customer_activate.intCustomerId = mst_finance_service_customer.intId
				WHERE
				mst_finance_service_customer_activate.intCompanyId =  '$companyId' AND
				mst_finance_service_customer_activate.intChartOfAccountId =  '$chartOfAcc' AND
				mst_finance_service_customer.intId =  '$pId'";
				$resultTwo = $db->RunQuery($sqlTwo);
			 	$rowTwo=mysqli_fetch_array($resultTwo);
			 	return $rowTwo['strName'];
		}
		else
		{
			return "";
		}
}
?>

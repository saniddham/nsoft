<?php
function checkEditDeleteForUnrealize($personType,$personId,$invoiceType,$invoiceNumber){    
    global $db;
    $sqlCheck=" SELECT count(InvoiceNumber) as cc 
                FROM fin_unadjusted_gl 
                WHERE
                personType='$personType' AND personId=$personId AND 
                invoiceType='$invoiceType' AND InvoiceNumber='$invoiceNumber' AND status=1";
    $resultCheck = $db->RunQuery($sqlCheck);
    $rowCheck=mysqli_fetch_array($resultCheck);
    $recordCount=$rowCheck['cc'];
    if($recordCount>0) $res=true;
    else $res=false; 
    
    return $res;
}

function checkUnrealizeEntry($invoiceType,$invoiceNumber,$detailArr){    
    global $db;
    $chk=false;
    foreach($detailArr as $arrVal){
        $accountId = $arrVal['account'];
        $sql = "SELECT
                        mst_financechartofaccounts.intId,
                        mst_financechartofaccounts.strName,
                        mst_financialsubtype.intId as finType,
                        mst_financialsubtype.strName
                    FROM
                        mst_financechartofaccounts
                        Inner Join mst_financialsubtype ON mst_financechartofaccounts.intFinancialTypeId = mst_financialsubtype.intId
                    WHERE
                        mst_financechartofaccounts.intId =  '$accountId'";
        $result = $db->RunQuery($sql);
        $row=mysqli_fetch_array($result);
        $finType=$row['finType'];
        if($finType==10 || $finType==18 || $finType==26 || $finType==16 || $finType==17 || $finType==28){
            if($finType==10) $personType="cus";
            else if($finType==18) $personType="sup";
			else if($finType==26 || $finType==16 || $finType==17 || $finType==28) $personType="osup";
			else if($finType==22 || $finType==21 || $finType==11 || $finType==12 || $finType==9) $personType="ocus";         
            $personId = $arrVal['recvFrom'];
            $chk=checkEditDeleteForUnrealize($personType,$personId,$invoiceType,$invoiceNumber);
            if( $chk==true) break;
        }
    }
    return $chk;
}

function checkCustomerUnrealizeProcessDetate($prosesId) {//check For pmayments
    global $db;
    $chk = false;
    $sql = "SELECT UD.invoiceNumber,UD.invoiceType,H.prosDate
                FROM
                fin_unadjusted_gl_header AS H
                Inner Join fin_unadjusted_gl AS UD ON H.processId = UD.porcessId
                WHERE
                H.processId =  '$prosesId' AND UD.status=1";
    $result = $db->RunQuery($sql);
    while ($row = mysqli_fetch_array($result)) {
        // set transaction as delete
        $invoiceNumber = $row['invoiceNumber'];
        $invoiceType = $row['invoiceType'];
        $prosDate = $row['prosDate'];

        $sql1 = "  SELECT 
                            COUNT(D.strJobNo) as cc 
                        From 
                            fin_customer_receivedpayments_main_details AS D 
                            Inner Join fin_customer_receivedpayments_header AS H ON H.strReferenceNo=D.strReferenceNo
                        WHERE 
                            D.strJobNo='$invoiceNumber' AND 
                            D.strDocType='$invoiceType' AND 
                            H.dtmDate >='$prosDate' AND
                            H.intDeleteStatus=0";
        $resultCheck = $db->RunQuery($sql1);
        $rowCheck = mysqli_fetch_array($resultCheck);
        $recordCount = $rowCheck['cc'];
        if ($recordCount > 0) {
            $chk = true;
            break;
        }
    }
    return $chk;
}

function checkOtherCustomerUnrealizeProcessDetate($prosesId) {//check For pmayments
    global $db;
    $chk = false;
    $sql = "SELECT UD.invoiceNumber,UD.invoiceType,H.prosDate
                FROM
                fin_unadjusted_gl_header AS H
                Inner Join fin_unadjusted_gl AS UD ON H.processId = UD.porcessId
                WHERE
                H.processId =  '$prosesId' AND UD.status=1";
    $result = $db->RunQuery($sql);
    while ($row = mysqli_fetch_array($result)) {
        // set transaction as delete
        $invoiceNumber = $row['invoiceNumber'];
        $invoiceType = $row['invoiceType'];
        $prosDate = $row['prosDate'];

        $sql1 = "  SELECT 
                            COUNT(D.strJobNo) as cc 
                        From 
                            fin_other_receivable_payments_main_details AS D 
                            Inner Join fin_other_receivable_payments_header AS H ON H.strReferenceNo=D.strReferenceNo
                        WHERE 
                            D.strJobNo='$invoiceNumber' AND 
                            D.strDocType='$invoiceType' AND 
                            H.dtmDate >='$prosDate' AND
                            H.intDeleteStatus=0";
        $resultCheck = $db->RunQuery($sql1);
        $rowCheck = mysqli_fetch_array($resultCheck);
        $recordCount = $rowCheck['cc'];
        if ($recordCount > 0) {
            $chk = true;
            break;
        }
    }
    return $chk;
}

function checkSupplierUnrealizeProcessDetate($prosesId) {//check For pmayments
    global $db;
    $chk = false;
    $sql = "SELECT UD.invoiceNumber,UD.invoiceType,H.prosDate
                FROM
                fin_unadjusted_gl_header AS H
                Inner Join fin_unadjusted_gl AS UD ON H.processId = UD.porcessId
                WHERE
                H.processId =  '$prosesId' AND UD.status=1";
    $result = $db->RunQuery($sql);
    while ($row = mysqli_fetch_array($result)) {
        // set transaction as delete
        $invoiceNumber = $row['invoiceNumber'];
        $invoiceType = $row['invoiceType'];
        $prosDate = $row['prosDate'];

        $sql1 = "  SELECT 
                            COUNT(D.strDocNo) as cc 
                        From 
                            fin_supplier_payments_main_details AS D 
                            Inner Join fin_supplier_payments_header AS H ON H.strReferenceNo=D.strReferenceNo
                        WHERE 
                            D.strDocNo='$invoiceNumber' AND 
                            D.strDocType='$invoiceType' AND 
                            H.dtmDate >='$prosDate' AND
                            H.intDeleteStatus=0";
        $resultCheck = $db->RunQuery($sql1);
        $rowCheck = mysqli_fetch_array($resultCheck);
        $recordCount = $rowCheck['cc'];
        if ($recordCount > 0) {
            $chk = true;
            break;
        }
    }
    return $chk;
}

function checkOtherSupplierUnrealizeProcessDetate($prosesId) {//check For pmayments
    global $db;
    $chk = false;
    $sql = "SELECT UD.invoiceNumber,UD.invoiceType,H.prosDate
                FROM
                fin_unadjusted_gl_header AS H
                Inner Join fin_unadjusted_gl AS UD ON H.processId = UD.porcessId
                WHERE
                H.processId =  '$prosesId' AND UD.status=1";
    $result = $db->RunQuery($sql);
    while ($row = mysqli_fetch_array($result)) 
	{
        // set transaction as delete
        $invoiceNumber = $row['invoiceNumber'];
        $invoiceType = $row['invoiceType'];
        $prosDate = $row['prosDate'];

        $sql1 = "SELECT 
                            COUNT(D.strDocNo) as cc 
                        From 
                            fin_other_payable_payments_main_details AS D 
                            Inner Join fin_other_payable_payments_header AS H ON H.strReferenceNo=D.strReferenceNo
                        WHERE 
                            D.strDocNo='$invoiceNumber' AND 
                            D.strDocType='$invoiceType' AND 
                            H.dtmDate >='$prosDate' AND
                            H.intDeleteStatus=0";
        $resultCheck = $db->RunQuery($sql1);
        $rowCheck = mysqli_fetch_array($resultCheck);
        $recordCount = $rowCheck['cc'];
        if ($recordCount > 0) 
		{
            $chk = true;
            break;
        }
    }
    return $chk;
}
?>

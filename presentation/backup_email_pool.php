<?php 
//2017-09-20

	//backup---------------------------------------------
	$sql_to		= " insert into sys_emailpool_backup 
					(
					mailId,
					intLocationId,
					strFromName,
					strFromEmail,
					strToEmail,
					strCCEmail,
					strBCCEmail,
					strMailHeader,
					strEmailBody,
					intEnterUserId,
					dtmEnterDate,
					intDelivered
					)
					select  
					sys_emailpool.mailId,
					sys_emailpool.intLocationId,
					sys_emailpool.strFromName,
					sys_emailpool.strFromEmail,
					sys_emailpool.strToEmail,
					sys_emailpool.strCCEmail,
					sys_emailpool.strBCCEmail,
					sys_emailpool.strMailHeader,
					sys_emailpool.strEmailBody,
					sys_emailpool.intEnterUserId,
					sys_emailpool.dtmEnterDate,
					sys_emailpool.intDelivered
					from sys_emailpool where ((TIMESTAMPDIFF(HOUR, dtmEnterDate, now()))>12) or (sys_emailpool.intDelivered=1 and ((TIMESTAMPDIFF(HOUR, dtmEnterDate, now()))>1))";
	$result_to	= $db->RunQuery($sql_to);

	//delete------------------------------------------------------------------------------
	$sql_del		= " delete from sys_emailpool where ((TIMESTAMPDIFF(HOUR, dtmEnterDate, now()))>12) or (sys_emailpool.intDelivered=1 and ((TIMESTAMPDIFF(HOUR, dtmEnterDate, now()))>1))";
	$result_del	= $db->RunQuery($sql_del);
	//-------------------------------------------------------------------------------------
?>
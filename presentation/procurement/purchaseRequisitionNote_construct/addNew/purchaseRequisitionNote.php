<?php
	session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];

$thisFilePath =  $_SERVER['PHP_SELF'];
$intUser  = $_SESSION["userId"];
$company 	= $_SESSION['headCompanyId'];

include  	"{$backwardseperator}dataAccess/permisionCheck.inc";
//include  	"{$backwardseperator}dataAccess/Connector.php";
require_once "{$backwardseperator}class/cls_permisions.php";

//---------check Permission to save recive qty more than PO qty.------------
$objpermisionget= new cls_permisions($db);
$permisionEnableProject 	= $objpermisionget->getPermisionCompanyEnableProject($company);
//------------------------------------------------------------------------

$prnNo = $_REQUEST['prnNo'];
$year = $_REQUEST['year'];
$programName='Purchase Request Note';
$programCode='P0222';

if(($prnNo=='')&&($year=='')){
	$savedStat = (int)getApproveLevel($programName);
	$intStatus=$savedStat+1;
	$date='';
}
else{
   $sql = "SELECT
			trn_prnheader.intApproveLevels,
			trn_prnheader.intStatus 
			FROM
			trn_prnheader 
			WHERE
			trn_prnheader.intPrnNo =  '$prnNo' AND
			trn_prnheader.intYear =  '$year' ";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$savedStat=$row['intApproveLevels'];
		$intStatus=$row['intStatus'];
	}
}

//url-presentation/procurement/purchaseRequisitionNote/addNew/purchaseRequisitionNote.php
$editMode=loadEditMode($programCode,$intStatus,$savedStat,$intUser);
$confirmatonMode=loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser);
	if($prnNo==''){
		$confirmatonMode=0;	
	}

$mainPage=$backwardseperator."main.php";
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Purchase Requisition Note</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $backwardseperator; ?>css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">


<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="purchaseRequisitionNote-js.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

</head>

<body>
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>
<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<form id="frmPurchaseRequisitionNote" name="frmPurchaseRequisitionNote" method="post" action="">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">

</table>

      <?php 
	   $sql = "SELECT
				trn_prnheader.dtmRequiredDate,
				trn_prnheader.dtmPrnDate,
				trn_prnheader.intInternalUsage,
				trn_prnheader.intDepartment,
				trn_prnheader.intStatus,
				trn_prnheader.strRemarks
				FROM
				trn_prnheader 
				WHERE
				trn_prnheader.intPrnNo =  '$prnNo' AND
				trn_prnheader.intYear =  '$year'
";
				
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						$reqdat=$row['dtmRequiredDate'];
						$department=$row['intDepartment'];
						$prndat=$row['dtmPrnDate'];
						$interUsg=$row['intInternalUsage'];
						$remarks=$row['strRemarks'];
						$intStatus=$row['intStatus'];
					}
					
					//default user department
					if($prnNo==''){
					
					 $sql1 = "SELECT
								sys_users.intDepartmentId
								FROM
								sys_users
								WHERE
								sys_users.intUserId =  '$intUser'";	
					$result1 = $db->RunQuery($sql1);
					$row1=mysqli_fetch_array($result1);
					$department=$row1['intDepartmentId'];
					}
					
?>

<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">Purchase Requisition Note</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="14%" height="22" class="normalfnt">PRN No</td>
            <td width="24%"><input name="txtPrnNo" type="text" disabled="disabled" class="txtText" id="txtPrnNo" style="width:60px" value="<?php echo $prnNo ?>"/><input name="txtYear" type="text" disabled="disabled" class="txtText" id="txtYear" style="width:40px" value="<?php echo $year ?>"/></td>
            <td width="11%">&nbsp;</td>
            <td width="23%">&nbsp;</td>
            <td width="14%" class="normalfnt">Date</td>
            <td width="14%" align="left"><input name="dtPrnDate" type="text" value="<?php if($prnNo){ echo $prndat; }else { echo date("Y-m-d"); }?>" class="txtbox" id="dtPrnDate" style="width:98px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" disabled="disabled"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
          </tr>
          <tr>
            <td height="22" class="normalfnt">Department</td>
            <td><select name="cboDepartment" id="cboDepartment" style="width:250px"  class="validate[required]" >
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_department.intId,
							mst_department.strName
							FROM
							mst_department
							WHERE
							mst_department.intStatus =  '1'";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$department)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
            </select></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">Required Date</td>
            <td align="left"><input name="dtRequiredDate" type="text" value="<?php if($prnNo){ echo $reqdat; }else { echo date("Y-m-d"); }?>" class="txtbox" id="dtRequiredDate" style="width:98px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
          </tr>
          <tr>
            <td height="22" class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt"><div style="display:none">Internal Use</div></td>
            <td align="left"><input type="checkbox" name="chkInternal" id="chkInternal" <?php if($interUsg==1){ ?>checked="checked" <?php } ?> style="display:none"/></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="14%" height="27" rowspan="2" class="normalfnt">Note</td>
            <td width="34%" rowspan="2"><textarea name="txtRemarks" id="txtRemarks" cols="35" rows="3"><?php echo $remarks ?></textarea></td>
            <td width="26%" rowspan="2">&nbsp;</td>
            <td width="10%" rowspan="2" valign="top" class="normalfnt">&nbsp;</td>
            <td width="16%" align="right" valign="top">&nbsp;</td>
            </tr>
          <tr>
            <td align="right" valign="bottom"><img src="../../../../images/Tadd.jpg" width="92" height="24" id="butAddItems" name="butAddItems" class="mouseover"  /></td>
            </tr>
          </table></td>
      </tr>
      <?php
	$viewProject=0;
	
	if($permisionEnableProject==1)
	$viewProject=1;
	  
 	$sql = "	SELECT
						trn_prndetails.intProject
						FROM
						trn_prndetails 
						WHERE
						trn_prndetails.intPrnNo =  '$prnNo' AND
						trn_prndetails.intYear =  '$year'  and 
						trn_prndetails.intProject>0 
						";
	$result = $db->RunQuery($sql);
	$rows = mysqli_num_rows($result);
	if($rows>0)
	{
	  $viewProject=1;
	}
	  
	  ?>
      <tr>
        <td><div style="width:900px;height:300px;overflow:scroll" >
          <table width="100%" class="grid" id="tblItems" >
            <tr class="gridHeader">
              <td width="3%" height="22" >Del</td>
              <td width="12%" >Main Category</td>
              <td width="11%" >Sub Category</td>
              <td width="9%" >Item Code</td>
              <td width="15%" >Item Description</td>
              <td width="20%"  <?php if($viewProject!=1){ ?>style="display:none" <?php } ?>>Project</td>
              <td width="19%"  <?php if($viewProject!=1){ ?>style="display:none" <?php } ?>>Sub Project</td>
              <td width="11%" >Qty</td>
              </tr>
              <?php
					  $sqlP = "SELECT
mst_item.strName item,
trn_prndetails.dblPrnQty,
trn_prndetails.dblPoQty,
mst_maincategory.strName category,
mst_subcategory.strName subCategory,
trn_prndetails.intItem,
mst_item.intMainCategory,
mst_item.intSubCategory ,
trn_prndetails.intProject ,
trn_prndetails.intSubProject ,
mst_item.strCode 
FROM
trn_prndetails
Inner Join mst_item ON trn_prndetails.intItem = mst_item.intId
Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
WHERE
trn_prndetails.intPrnNo =  '$prnNo' AND
trn_prndetails.intYear =  '$year' 
Order by mst_maincategory.strName ASC, mst_subcategory.strName ASC, mst_item.strName ASC";

	$resultP = $db->RunQuery($sqlP);
	while($rowP=mysqli_fetch_array($resultP))
	{
		$categoryID=$rowP['intMainCategory'];
		$subCatID=$rowP['intSubCategory'];
		$category=$rowP['category'];
		$subCat=$rowP['subCategory'];
		$itemID=$rowP['intItem'];
		$itemCode=$rowP['strCode'];
		$idemDesc=$rowP['item'];
		$qty=$rowP['dblPrnQty'];
		$project=$rowP['intProject'];
		$subProject=$rowP['intSubProject'];
		
	?>
			<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF"><img class="delImg" src="../../../../images/del.png" width="15" height="15" /></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $categoryID ?>'"><?php echo $category ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $subCatID?> "><?php echo $subCat ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $itemID ?>"><?php echo $itemCode ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $itemID ?>" class="item"><?php echo $idemDesc ?></td>
			<td align="center" bgcolor="#FFFFFF" <?php if($viewProject!=1){ ?>style="display:none" <?php } ?>><select name="cboProject" id="cboProject" style="width:150px"  class="clsProject" <?php if($permisionEnableProject!=1){ ?> disabled="disabled"  <?php } ?>>
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							trn_orderheader.intOrderNo,
							trn_orderheader.strOrderCode
							FROM
							trn_orderheader
							WHERE
							trn_orderheader.intStatus =  '1' and intType='1'";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intOrderNo']==$project)
						echo "<option value=\"".$row['intOrderNo']."\" selected=\"selected\">".$row['strOrderCode']."</option>";	
						else
						echo "<option value=\"".$row['intOrderNo']."\">".$row['strOrderCode']."</option>";	
					}
				?>
            </select></td>
			<td align="center" bgcolor="#FFFFFF" <?php if($viewProject!=1){ ?>style="display:none" <?php } ?>><select name="cboSubProject" id="cboSubProject" style="width:150px"  class="subProject" <?php if($permisionEnableProject!=1){ ?> disabled="disabled"  <?php } ?>>
                  <option value=""></option>
                  <?php
					$sql = "SELECT
				trn_orderdetails.intSalesOrderId,
				trn_orderdetails.strSalesOrderNo,
				trn_orderdetails.strSubProjectDesc,
				trn_orderdetails.dblPrice
				FROM trn_orderdetails
				WHERE
				trn_orderdetails.intOrderNo =  '$project'
				ORDER BY
				trn_orderdetails.intSalesOrderId ASC";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intSalesOrderId']==$subProject)
						echo "<option value=\"".$row['intSalesOrderId']."\" selected=\"selected\">".$row['strSalesOrderNo']."</option>";	
						else
						echo "<option value=\"".$row['intSalesOrderId']."\">".$row['strSalesOrderNo']."</option>";	
					}
				?>
            </select></td>
			<td align="center" bgcolor="#FFFFFF" ><input id="txtQty" style="width:80px;text-align:center" type="text" value="<?php echo $qty ?>"  class="validate[required,custom[number]] Qty"/></td></tr>
	<?php
	}
	?>
            </table>
          </div></td>
      </tr>
<?php
/*	        $intStatus;
			$savedStat = (int)getApproveLevel($programName);
			$editMode=0;
			if($intStatus==0)//rejected
			$editMode=1;
			else if($savedStat+1==$intStatus)//saved(not cnfirmed
			$editMode=1;
			
			
			 $k=$savedStat+2-$intStatus;
			   $sqlp = "SELECT
					menupermision.int".$k."Approval 
					FROM menupermision 
					Inner Join menus ON menupermision.intMenuId = menus.intId
					WHERE
					menus.strCode =  '$programCode' AND
					menupermision.intUserId =  '$intUser'";	
							
					 $resultp = $db->RunQuery($sqlp);
					 $rowp=mysqli_fetch_array($resultp);
					 if($rowp['int'.$k.'Approval']==1){
					 $confirmatonMode=1;
					 }
					 else{
					 $confirmatonMode=0;//no user permission to confirm
					 }
					// echo $intStatus;
					// echo $confirmatonMode;
*/	
//echo $editMode; 
 ?>  
      <tr>    
        <td align="center" class="tableBorder_allRound"><img src="../../../../images/Tnew.jpg" width="92" height="24" class="mouseover" id="butNew" name="butNew" /><img src="../../../../images/Tsave.jpg" width="92" height="24" class="mouseover" id="butSave" name="butSave" <?php if($editMode==1){} else {?>style="display:none"<?php } ?>  /><img src="../../../../images/Tconfirm.jpg" width="92" height="24" class="mouseover" id="butConfirm" name="butConfirm" <?php if(($intStatus>1) and ($confirmatonMode==1)){ } else {?>  style="display:none" <?php } ?>/><img src="../../../../images/Treport.jpg" width="92" height="24" class="mouseover" id="butReport" name="butReport" /><img src="../../../../planning/img/Tclose.jpg" width="92" height="24" id="butClose" name="butClose" class="mouseover"  /></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>

	<!-- items to prn -->
	<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1">
   <!-- <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php" style="width:800;height:800;border:0;overflow:hidden">
    </iframe>-->
    </div>

	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>

</body>
</html>

<?php
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
			 
	return $editMode;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	 $sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
?>



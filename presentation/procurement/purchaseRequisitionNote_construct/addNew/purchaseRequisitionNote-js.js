			
$(document).ready(function() {
	
  		$("#frmPurchaseRequisitionNote").validationEngine();
		$('#frmPurchaseRequisitionNote #cboDepartment').focus();
		
	$('#frmPurchaseRequisitionNote .clsProject').live('change',function(){
//var row=this.parentNode.parentNode.rowIndex;
//alert(row);
			loadSubProjectOptions(this);
		});
		
/*		$("#cboProject").change(function(){
			alert("ppppp");
		});
*/		
		
		$("#butAddItems").click(function(){
			closePopUp();
		popupWindow3('1');
		$('#popupContact1').load('purchaseRequisitionNotePopup.php',function(){
				checkAlreadySelected();
				$('#butAdd').click(addClickedRows);
				$('#butClose1').click(disablePopup);
				 //-------------------------------------------- 
				  $('#frmPurchaseRequisitionNotePopup #cboMainCategory').change(function(){
						var mainCategory = $('#cboMainCategory').val();
						var url 		= "purchaseRequisitionNote-db-get.php?requestType=loadSubCategory&mainCategory="+mainCategory;
						var httpobj 	= $.ajax({url:url,async:false})
						document.getElementById('cboSubCategory').innerHTML=httpobj.responseText;
						
				  });
				 //-------------------------------------------- 
				  $('#frmPurchaseRequisitionNotePopup #imgSearchItems').click(function(){
						var rowCount = document.getElementById('tblItemsPopup').rows.length;
						for(var i=1;i<rowCount;i++)
						{
							document.getElementById('tblItemsPopup').deleteRow(1);
						}
						var mainCategory = $('#cboMainCategory').val();
						var subCategory = $('#cboSubCategory').val();
						var description = $('#txtItmDesc').val();
						var url 		= "purchaseRequisitionNote-db-get.php?requestType=loadItems";
						var httpobj = $.ajax({
							url:url,
							dataType:'json',
							data:"mainCategory="+mainCategory+"&subCategory="+subCategory+"&description="+description,
							async:false,
							success:function(json){

								var length = json.arrCombo.length;
								var arrCombo = json.arrCombo;


								for(var i=0;i<length;i++)
								{
									var itemId=arrCombo[i]['itemId'];	
									var maincatId=arrCombo[i]['maincatId'];	
									var subCatId=arrCombo[i]['subCatId'];	
									var code=arrCombo[i]['code'];	
									var itemName=arrCombo[i]['itemName'];	
									var mainCatName=arrCombo[i]['mainCatName'];
									var subCatName=arrCombo[i]['subCatName'];	
										
									var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF" id=""><input type="checkbox" id="chkDisp" /></td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'">'+mainCatName+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'">'+subCatName+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'">'+code+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'">'+itemName+'</td>';
									add_new_row('#frmPurchaseRequisitionNotePopup #tblItemsPopup',content);
								}
									checkAlreadySelected();

							}
						});
						
				  });
				  //-------------------------------------------------------
			});	
		});
	
	//--------------------------------------------
		
  //permision for add 
  if(intAddx)
  {
 	$('#frmPurchaseRequisitionNote #butNew').show();
	//$('#frmPurchaseRequisitionNote #butSave').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	//$('#frmPurchaseRequisitionNote #butSave').show();
	//$('#frmPurchaseRequisitionNote #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmPurchaseRequisitionNote #butDelete').show();
	//$('#frmPurchaseRequisitionNote #cboSearch').removeAttr('disabled');
  }
  
  //permision for view
  if(intViewx)
  {
	//$('#frmPurchaseRequisitionNote #cboSearch').removeAttr('disabled');
  }
  
  $('#frmPurchaseRequisitionNote #butSave').click(function(){
	var requestType = '';
	if ($('#frmPurchaseRequisitionNote').validationEngine('validate'))   
    { 
		showWaiting();
		var data = "requestType=save";
		
			data+="&serialNo="		+	$('#txtPrnNo').val();
			data+="&Year="	+	$('#txtYear').val();
			data+="&date="			+	$('#dtPrnDate').val();
			data+="&reqDate="	+	$('#dtRequiredDate').val();
			data+="&department="	+	$('#cboDepartment').val();
			if(document.getElementById('chkInternal').checked==true){
				var chkInternal=1;
			}
			else{
				var chkInternal=0;
			}
			data+="&internalUse=" +chkInternal;
			data+="&remarks="	+	$('#txtRemarks').val();
			
			//////////////////// saving main grid part //////////////////////////////////////////
			//var rowCount = $('#tblMain >tbody >tr').length;
			var rowCount = document.getElementById('tblItems').rows.length;
			if(rowCount==1){
				alert("Items not selected to PRN");hideWaiting();
				return false;				
			}
			var row = 0;
			var errorQtyFlag=0;
			var errorSubProFlag=0;
			
			var arr="[";
			
			$('#tblItems .item').each(function(){
	
	
				var itemID	= $(this).attr('id');
				var itemName	= $(this).html();
				var quantity	= $(this).parent().find(".Qty").val();
				var project	= $(this).parent().find(".clsProject").val();
				var subProject	= $(this).parent().find(".subProject").val();
					
					if(quantity>0){
						arr += "{";
						arr += '"itemID":"'+ itemID +'",' ;
						arr += '"itemName":"'+ itemName +'",' ;
						arr += '"project":"'+ project +'",' ;
						arr += '"subProject":"'+ subProject +'",' ;
						arr += '"quantity":"'+ quantity +'"' ;
						arr +=  '},';
					}
						if(quantity<=0){
							errorQtyFlag=1;
						}
						if((project!='') && (subProject=='')){
							errorSubProFlag=1;
						}
						
			});
			
			if(rowCount<=1){
				alert("No items selected to Gate Pass");hideWaiting();
				return false;
			}
			else if(errorQtyFlag==1){
				alert("Please Enter Quantities");hideWaiting();
				return false;
			}
			else if(errorSubProFlag==1){
				alert("Please Select sub projects for selected projects");hideWaiting();
				return false;
			}
			
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;

		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = "purchaseRequisitionNote-db-set.php";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmPurchaseRequisitionNote #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						//$('#frmSampleInfomations').get(0).reset();
						//saveImage(json.sampleNo,json.sampleYear,json.revisionNo);
						//loadCombo_frmSampleInfomations();
						var t=setTimeout("alertx()",1000);
						$('#txtPrnNo').val(json.serialNo);
						$('#txtYear').val(json.year);
						return;
					}
				},
			error:function(xhr,status){
					
					$('#frmPurchaseRequisitionNote #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
			 hideWaiting();
	}
   });
   
	
	//--------------refresh the form----------
	$('#frmPurchaseRequisitionNote #butNew').click(function(){
		$('#frmPurchaseRequisitionNote').get(0).reset();
		clearRows();
		$('#frmPurchaseRequisitionNote #cboDispatchTo').removeAttr('disabled');
		$('#frmPurchaseRequisitionNote #cboDispatchTo').val('');
		$('#frmPurchaseRequisitionNote #cboDispatchTo').focus();
		$('#frmPurchaseRequisitionNote #txtDispNo').val('');
		$('#frmPurchaseRequisitionNote #txtDispYear').val('');
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmPurchaseRequisitionNote #dtDate').val(d);
	});
	//----------------------------------------
	
/*    $('#frmPurchaseRequisitionNote #butDelete').click(function(){
		if($('#frmPurchaseRequisitionNote #cboSearch').val()=='')
		{
			$('#frmPurchaseRequisitionNote #butDelete').validationEngine('showPrompt', 'Please select Type.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmPurchaseRequisitionNote #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = "typeOfPrint-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											data:'requestType=delete&cboSearch='+$('#frmPurchaseRequisitionNote #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmPurchaseRequisitionNote #butDelete').validationEngine('showPrompt', json.msg,json.type );
												
												if(json.type=='pass')
												{
													$('#frmPurchaseRequisitionNote').get(0).reset();
													loadCombo_frmPurchaseRequisitionNote();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
	
*/	

//-----------------------------------
$('#butReport').click(function(){
	if($('#txtPrnNo').val()!=''){
		window.open('../listing/rptPurchaseRequisitionNote.php?prnNo='+$('#txtPrnNo').val()+'&year='+$('#txtYear').val());	
	}
	else{
		alert("There is no PRN No to view");
	}
});
//----------------------------------	
$('#butConfirm').click(function(){
	if($('#txtPrnNo').val()!=''){
		window.open('../listing/rptPurchaseRequisitionNote.php?prnNo='+$('#txtPrnNo').val()+'&year='+$('#txtYear').val()+'&approveMode=1');		
	}
	else{
		alert("There is no PRN No to confirm");
	}
});
//-----------------------------------------------------
$('#butClose').click(function(){
		//load('main.php');	
});
//--------------refresh the form-----------------------
	$('#frmPurchaseRequisitionNote #butNew').click(function(){
		window.location.href = "purchaseRequisitionNote.php";
		$('#frmPurchaseRequisitionNote').get(0).reset();
		clearRows();
		$('#frmPurchaseRequisitionNote #cboDepartment').val('');
		$('#frmPurchaseRequisitionNote #cboDepartment').focus();
		$('#frmPurchaseRequisitionNote #txtPrnNo').val('');
		$('#frmPurchaseRequisitionNote #txtYear').val('');
		$('#frmPurchaseRequisitionNote #txtRemarks').val('');
		document.getElementById('chkInternal').checked=false;
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmPurchaseRequisitionNote #dtPrnDate').val(d);
		$('#frmPurchaseRequisitionNote #dtRequiredDate').val(d);
		document.getElementById('butConfirm').style.display=true;
		document.getElementById('butSave').style.display=true;
	});
//-----------------------------------------------------
});

//----------end of ready -------------------------------
function loadMain()
{
	//alert(1);
/*	$("#butAddPendingDispatch").click(function(){
		popupWindow3('1');
		//$('#iframeMain1').attr('src','http://localhost/qpet/presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php?id='+$('#cboDispatchTo').val());
		$('#popupContact1').load('sampleDispatchPopup.php');	
		
	});*/
	//$("#iframeMain1").contents().find("#butAdd").click(abc);
	//$("#frmPurchaseRequisitionNotePopup").contents().find("#butAdd").click(abc);
//	$('#frmPurchaseRequisitionNotePopup #butClose').click(abc);
}


function addClickedRows()
{
	//var rowCount = $('#tblDispatchPopup >tr').length;
	var rowCount = document.getElementById('tblItemsPopup').rows.length;
	var projectCombo=loadProjectCombo();
	var subProjectCombo=loadSubProjectCombo();
	var viewPermission=getViewPermission();

	for(var i=1;i<rowCount;i++)
	{
		if((document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].checked==true) && (document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].disabled==false)){
			var categoryID=document.getElementById('tblItemsPopup').rows[i].cells[1].id;
			var category=document.getElementById('tblItemsPopup').rows[i].cells[1].innerHTML;
			var subCatID=document.getElementById('tblItemsPopup').rows[i].cells[2].id;
			var subCat=document.getElementById('tblItemsPopup').rows[i].cells[2].innerHTML;
			var itemID=document.getElementById('tblItemsPopup').rows[i].cells[3].id;
			var itemCode=document.getElementById('tblItemsPopup').rows[i].cells[3].innerHTML;
			var idemDesc=document.getElementById('tblItemsPopup').rows[i].cells[4].innerHTML;
			
			//alert($('#frmPurchaseRequisitionNote #cboDispatchTo').val());
			var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF"><img class="delImg" src="../../../../images/del.png" width="15" height="15" /></td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+categoryID+'">'+category+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatID+'">'+subCat+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemID+'">'+itemCode+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemID+'" class="item">'+idemDesc+'</td>';	
			if(viewPermission==1){
			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemID+'" >'+projectCombo+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemID+'" >'+subProjectCombo+'</td>';
			}
			else{
			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemID+'" style="display:none">'+projectCombo+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemID+'" style="display:none">'+subProjectCombo+'</td>';
			}
			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemID+'" ><input  id="'+itemID+'" class="validate[required,custom[number]] Qty" style="width:80px;text-align:center" type="text" value="0"/></td>';
		//	content +='<td align="center" bgcolor="#FFFFFF" id="" class=""><input style="width:80px;text-align:center" type="text" value="0"/></td>';
		
			add_new_row('#frmPurchaseRequisitionNote #tblItems',content);
			//$('.')
			//$("#frmPurchaseRequisitionNote").validationEngine();
		//----------------------------	
		$('.delImg').click(function(){
			$(this).parent().parent().remove();
		});
		
		//----------------------------	
		$('.clsValidateBalQty').keyup(function(){
			var input=$(this).val();
			var balQty=$(this).closest('td').attr('id');
			if((input>balQty) || (input<0)){
			alert("Invalid Qty");
				$(this).val(balQty);
			}

		});
		
/*	  //--------------change customer------------
		$('#frmPurchaseRequisitionNote #cboDispatchTo').change(function(){
				var val = $.prompt('Are you sure you want to delete existing samples  ?',{
							buttons: { Ok: true, Cancel: false },
							callback: function(v,m,f){
								if(v)
								{
									clearRows();
								}
					}
				});
		});
	//--------------------------------------------	
*/		}
	}
	
	disablePopup();
}
//-------------------------------------
function alertx()
{
	$('#frmPurchaseRequisitionNote #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmPurchaseRequisitionNote #butDelete').validationEngine('hide')	;
}

function closePopUp(){
	
}
//----------------------------------------------
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
//----------------------------------------------- 
function clearRows()
{
	//var rowCount = $('#tblDispatchPopup >tbody >tr').length;
	var rowCount = document.getElementById('tblItems').rows.length;
	//alert(rowCount);
	//var cellCount = document.getElementById('tblMain').rows[1].cells.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblItems').deleteRow(1);
			
	}
}
//------------------------------------------------------------
function checkAlreadySelected(){
	var rowCount = document.getElementById('tblItems').rows.length;
	for(var i=1;i<rowCount;i++)
	{
			var itemNo = 	document.getElementById('tblItems').rows[i].cells[4].id;
		
			var rowCount1 = document.getElementById('tblItemsPopup').rows.length;
			for(var j=1;j<rowCount1;j++)
			{
				var itemNoP = 	document.getElementById('tblItemsPopup').rows[j].cells[4].id;
				
				if(itemNo==itemNoP){
					document.getElementById('tblItemsPopup').rows[j].cells[0].childNodes[0].checked=true;
					document.getElementById('tblItemsPopup').rows[j].cells[0].childNodes[0].disabled=true;
				}
			}
	}
}
//----------------------------------------------------
function loadProjectCombo(){
	
	var combo='';
	var url 		= "purchaseRequisitionNote-db-get.php?requestType=loadProjectCombo";
	var httpobj 	= $.ajax({url:url,async:false})
	combo=httpobj.responseText;
		
	return combo;
}
//----------------------------------------------------
function loadSubProjectCombo(){
	
	var combo='';
	var url 		= "purchaseRequisitionNote-db-get.php?requestType=loadSubProjectCombo";
	var httpobj 	= $.ajax({url:url,async:false})
	combo=httpobj.responseText;
		
	return combo;
}

function getViewPermission(){
	
	var permision='';
	var serialNo=$('#frmPurchaseRequisitionNote #txtPrnNo').val();
	var Year=$('#frmPurchaseRequisitionNote #txtYear').val();
	
	var url 		= "purchaseRequisitionNote-db-get.php?requestType=getViewPermission&serialNo="+serialNo+"&Year="+Year;
	var httpobj 	= $.ajax({url:url,async:false})
	permision=httpobj.responseText;
		
	return permision;
}

//----------------------------------------------------
function loadSubProjectOptions(obj){
		var project =  $(obj).parent().parent().find(".clsProject").val();
		//alert(project);
		
	var combo='';
		var url 		= "purchaseRequisitionNote-db-get.php?requestType=loadSubProjectOptions&project="+project;
	var httpobj 	= $.ajax({url:url,async:false})
	combo=httpobj.responseText;
		
				 $(obj).parent().parent().find(".subProject").html(combo);
	
}

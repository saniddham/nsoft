<?php 
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$companyId 	= $_SESSION['CompanyID'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	
	$serialNo 		= $_REQUEST['serialNo'];
	$year 			= $_REQUEST['Year'];
	$date 			= $_REQUEST['date'];
	$reqDate 		= $_REQUEST['reqDate'];
	$internalUse 	= $_REQUEST['internalUse'];
	$remarks 		= $_REQUEST['remarks'];
	$department 			= $_REQUEST['department'];
	
	$arr 		= json_decode($_REQUEST['arr'], true);
	
	$ApproveLevels=(int)getApproveLevel('Purchase Request Note');
	$prnApproveLevel = $ApproveLevels+1;
	
//------------save---------------------------	
	if($requestType=='save')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		if($serialNo==''){
			$serialNo 	= getNextPrnNo();
			$year = date('Y');
			$editMode=0;
		}
		else{
			$editMode=1;
		}
		//-----------delete and insert to header table-----------------------
		if($editMode==1){
			$sql = "UPDATE `trn_prnheader` SET intStatus ='$prnApproveLevel', 
												intApproveLevels ='$ApproveLevels', 
												intModifiedBy ='$userId', 
												dtmModifiedDate =now(), 
												dtmRequiredDate ='$reqDate', 
												intInternalUsage ='$internalUse', 
												strRemarks ='$remarks', 
												dtmPrnDate ='$date'
					                            WHERE (`intPrnNo`='$serialNo') AND (`intYear`='$year')";
			$result = $db->RunQuery2($sql);
		}
		else{
			$sql = "DELETE FROM `trn_prnheader` WHERE  (`intPrnNo`='$serialNo') AND (`intYear`='$year')";
			$result1 = $db->RunQuery2($sql);
			
			$sql = "INSERT INTO `trn_prnheader` (`intPrnNo`,`intYear`,`intDepartment`,intCompany,intInternalUsage,dtmRequiredDate,dtmPrnDate,strRemarks,intUser,intStatus,intApproveLevels) 
					VALUES ('$serialNo','$year','$department','$companyId','$internalUse','$reqDate','$date','$remarks','$userId','$prnApproveLevel','$ApproveLevels')";
			$result = $db->RunQuery2($sql);
		}
		//-----------delete and insert to detail table-----------------------
		if($result){
		$sql = "DELETE FROM `trn_prndetails` WHERE (`intPrnNo`='$serialNo') AND (`intYear`='$year')";
		$result2 = $db->RunQuery2($sql);
		;
			$toSave=0;
			$saved=0;
			$rollBackFlag=0;
			$rollBackMsg ="Invalid PRN Qtys for items are..."; 
			foreach($arr as $arrVal)
			{
				$itemID 		= $arrVal['itemID'];
				$itemName 		= $arrVal['itemName'];
				$quantity 		= $arrVal['quantity'];
				$project 		= $arrVal['project'];
				$subProject 		= $arrVal['subProject'];
				
				if($project==''){
					$project=0;
				}
				if($subProject==''){
					$subProject=0;
				}
				
				if($quantity<=0){
					$rollBackFlag=1;
					$rollBackMsg .="\n ".$itemName." =".$quantity;
					$result3=0;
				}
				else if(($project!=0) && ($subProject==0)){
					$rollBackFlag=1;
					$rollBackMsg ="Please select sub project";
					$result3=0;
				}
				else{
				$sql = "INSERT INTO `trn_prndetails` (`intPrnNo`,`intYear`,`intItem`,`intProject`,`intSubProject`,`dblPrnQty`,`dblPoQty`) 
				VALUES ('$serialNo','$year','$itemID','$project','$subProject','$quantity','0')";
				$result3 = $db->RunQuery2($sql);
				}
				if($result3==1){
				$saved++;
				}
			  $toSave++;
			}
		}
		
		
		if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= '';
		}
		else if(($result) && ($toSave==$saved)){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			if($editMode==1)
			$response['msg'] 		= 'Updated successfully.';
			else
			$response['msg'] 		= 'Saved successfully.';
			
			$response['serialNo'] 		= $serialNo;
			$response['year'] 		= $year;
		}
		else{
			$db->RunQuery2('Rollback');
			
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		$db->CloseConnection();		
	}
//----------------------------------------
	echo json_encode($response);
//-----------------------------------------	


	function getNextPrnNo()
	{
		global $db;
		global $companyId;
		$sql = "SELECT
				sys_no.intPRNno
				FROM sys_no
				WHERE
				sys_no.intCompanyId =  '$companyId'
				";	
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$nextDispatchNo = $row['intPRNno'];
		
		$sql = "UPDATE `sys_no` SET intPRNno=intPRNno+1 WHERE (`intCompanyId`='$companyId')  ";
		$db->RunQuery2($sql);
		
		return $nextDispatchNo;
	}
//---------------------------------------	
?>
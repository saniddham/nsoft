<?php
session_start();
$backwardseperator = "../../../../";
$companyId = $_SESSION['CompanyID'];
$intUser  = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
include  	"{$backwardseperator}dataAccess/Connector.php";

$prnNo = $_REQUEST['prnNo'];
$year = $_REQUEST['year'];
$approveMode = $_REQUEST['approveMode'];
$programName='Purchase Request Note';
$programCode='P0222';//url-presentation/procurement/purchaseRequisitionNote/addNew/purchaseRequisitionNote.php

		$sql = "SELECT
trn_prnheader.intPrnNo,
trn_prnheader.intYear,
trn_prnheader.intDepartment,
trn_prnheader.intInternalUsage,
trn_prnheader.dtmRequiredDate,
trn_prnheader.dtmPrnDate,
trn_prnheader.strRemarks,
mst_department.intId,
trn_prnheader.intStatus, 
trn_prnheader.intApproveLevels,
trn_prnheader.intUser,
trn_prnheader.intCompany as PRNRaisedLocationId, 
mst_department.strName,
sys_users.strUserName as prnBy 
FROM
trn_prnheader
Inner Join mst_department ON trn_prnheader.intDepartment = mst_department.intId 
Inner Join sys_users ON trn_prnheader.intUser = sys_users.intUserId 
WHERE
trn_prnheader.intPrnNo =  '$prnNo' AND
trn_prnheader.intYear =  '$year'

";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$department = $row['strName'];
					$prnDate = $row['dtmPrnDate'];
					$requiredDate = $row['dtmRequiredDate'];
					$intInternalUsage = $row['intInternalUsage'];
					if($intInternalUsage==1)
					$InternalUsage="Yes";
					else
					$InternalUsage="No";
					
					$intStatus = $row['intStatus'];
					$savedLevels = $row['intApproveLevels'];
					$prnBy = $row['prnBy'];
					$confirmBy = $row['intConfirmedBy'];
					$confirmDate = $row['intConfirmedDate'];
					$locationId = $row['PRNRaisedLocationId'];//this is use in report header(reportHeader.php)--------------------
				 }
				 

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Purchase Request Report</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../customerAndOperation/libraries/jquery/jquery-ui.js"></script>

<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<script type="application/javascript" src="rptPurchaseRequisitionNote-js.js"></script>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:242px;
	top:169px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<?php
 if($intStatus>1)//pending
{
?>
<div id="apDiv1"><img src="../../../../images/pending.png"  /></div>
<?php
}
?>
<form id="frmPurchaseRequisitionNoteReport" name="frmPurchaseRequisitionNoteReport" method="post" action="rptPurchaseRequisitionNote.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include '../../../../reportHeader.php'?></td>
<td width="20%"></td>
</tr>
<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>PURCHASE REQUEST NOTE</strong><strong></strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
  <tr>
    <td colspan="9" align="center" bgcolor="#FFDFCB">
    <?php
	if($intStatus>1)
	{
	
	//------------
	$k=$savedLevels+2-$intStatus;
	$sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
	
	 $resultp = $db->RunQuery($sqlp);
	 $rowp=mysqli_fetch_array($resultp);
	 $userPermission=0;
	 if($rowp['int'.$k.'Approval']==1){
	 $userPermission=1;
	 }
	//--------------	
	?>
    <?php if(($approveMode==1) and ($userPermission==1)) { ?>
    <img src="../../../../images/approve.png" align="middle" class="noPrint" id="imgApprove" />
    <img src="../../../../images/reject.png" align="middle" class="noPrint" id="imgReject" />
    <?php
	}
	}
	?>
    </td>
  </tr>
  <tr>
  <?php
 	if($intStatus==1)
	{
	?>
   <td colspan="9" class="APPROVE">CONFIRMED</td>
   <?PHP
	}
	else if($intStatus==0)
	{
   ?>
   <td colspan="9" class="APPROVE" style="color:#F00">REJECTED</td>
   <?php
	}
	else
	{
   ?>
   <td width="10%" colspan="9" class="APPROVE">PENDING</td>
   <?php
	}
   ?>
  </tr>
  <tr>
    <td width="1%">&nbsp;</td>
    <td width="12%"><span class="normalfnt"><strong>PRN No</strong></span></td>
    <td width="2%" align="center" valign="middle"><strong>:</strong></td>
    <td width="25%"><span class="normalfnt"><?php echo $prnNo ?>/<?php echo $year ?></span></td>
    <td width="13%" class="normalfnt"><strong>Required Date</strong></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="18%"><span class="normalfnt"><?php echo $requiredDate ?></span></td>
    <td width="1%"></td>
  <td width="5%"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Department</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $department ?></span></td>
    <td><span class="normalfnt"><strong>PRN Date</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $prnDate ?></span></td>
    <td class="normalfnt">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Internal Use</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $InternalUsage ?></span></td>
    <td><span class="normalfnt"><strong> PRN By</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $prnBy ?></span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  
  
  
  <tr>
    <td colspan="9">&nbsp;</td>
    <td width="4%">&nbsp;</td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td width="5%">&nbsp;</td>
        <td colspan="7" class="normalfnt">
          <table width="100%" class="grid" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr class="gridHeader">
              <td width="15%"  height="22">Main Category</td>
              <td width="13%" >Sub Category</td>
              <td width="9%" >Item Code</td>
              <td width="27%" >Item</td>
              <td width="16%" >Project</td>
              <td width="13%" >Sub Project</td>
              <td width="7%" >Qty</td>
              </tr>
              <?php 
	  	 $sql1 = "SELECT
trn_prndetails.intItem,
mst_item.strCode,
mst_item.strName,
mst_maincategory.strName as mainCategory,
mst_subcategory.strName as subCategory, 
trn_prndetails.dblPrnQty, 
trn_orderheader.strOrderCode as project , 
trn_orderdetails.strSalesOrderNo as subProject  
FROM
trn_prndetails
Inner Join mst_item ON trn_prndetails.intItem = mst_item.intId
Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
left Join trn_orderheader ON trn_prndetails.intProject = trn_orderheader.intOrderNo 
left Join trn_orderdetails ON trn_prndetails.intProject = trn_orderdetails.intOrderNo AND trn_prndetails.intSubProject = trn_orderdetails.intSalesOrderId
WHERE
trn_prndetails.intPrnNo =  '$prnNo' AND
trn_prndetails.intYear =  '$year' 
Order by mst_maincategory.strName ASC, mst_subcategory.strName ASC, mst_item.strName ASC";
			$result1 = $db->RunQuery($sql1);
			while($row=mysqli_fetch_array($result1))
			{
	  ?>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfnt" >&nbsp;<?php echo $row['mainCategory'] ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['subCategory']?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['strCode'] ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['strName'] ?>&nbsp;</td>
              <td class="normalfnt" ><?php echo $row['project'] ?></td>
              <td class="normalfnt" ><?php echo $row['subProject'] ?></td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['dblPrnQty'] ?>&nbsp;</td>
              </tr>
      <?php        
			}
	  ?>
            
            </table>
          </td>
        <td width="6%">&nbsp;</td>
        </tr>
      
      </table>
    </td>
</tr>
<?php  
 	if($intStatus!=0)
	{

			  $prnApproveLevel = (int)getApproveLevel($programName);
				for($i=1; $i<=$savedLevels; $i++)
				{
					 $sqlc = "SELECT
							trn_prnheader_approvedby.intApproveUser,
							trn_prnheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName,
							trn_prnheader_approvedby.intApproveLevelNo
							FROM
							trn_prnheader_approvedby
							Inner Join sys_users ON trn_prnheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							trn_prnheader_approvedby.intPrnNo =  '$prnNo' AND
							trn_prnheader_approvedby.intYear =  '$year' AND
							trn_prnheader_approvedby.intApproveLevelNo =  '$i' order by intApproveLevelNo asc
";
					 $resultc = $db->RunQuery($sqlc);
					 $rowc=mysqli_fetch_array($resultc);
						if($i==1)
						$desc="1st ";
						else if($i==2)
						$desc="2nd ";
						else if($i==3)
						$desc="3rd ";
						else
						$desc=$i."th ";
					 //  $desc=$ap.$desc;
					 $desc2=$rowc['UserName']."(".$rowc['dtApprovedDate'].")";
					 if($rowc['UserName']=='')
					 $desc2='---------------------------------';
				?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> Approved By - </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
            </tr>
<?php
			}
	}
	else{
					 $sqlc = "SELECT
							trn_prnheader_approvedby.intApproveUser,
							trn_prnheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName,
							trn_prnheader_approvedby.intApproveLevelNo
							FROM
							trn_prnheader_approvedby
							Inner Join sys_users ON trn_prnheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							trn_prnheader_approvedby.intPrnNo =  '$prnNo' AND
							trn_prnheader_approvedby.intYear =  '$year' AND
							trn_prnheader_approvedby.intApproveLevelNo =  '0'  order by intApproveLevelNo asc 
";
					 $resultc = $db->RunQuery($sqlc);
					 $rowc=mysqli_fetch_array($resultc);
					  ?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong> Rejected By - </strong></span><span class="normalfnt"><?php echo $rowc['UserName']."(".$rowc['dtApprovedDate'].")";?></span></td>
            </tr>
<?php
	}
?>
<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
</body>
</html>
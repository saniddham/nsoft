<?php
session_start();
$backwardseperator = "../../../../";
$location = $_SESSION['CompanyID'];
$company 	= $_SESSION['headCompanyId'];
$mainPath = $_SESSION['mainPath'];
$intUser  = $_SESSION["userId"];
$thisFilePath =  $_SERVER['PHP_SELF'];
$programName='Purchase Request Note';
$programCode='P0222';//url-presentation/procurement/purchaseRequisitionNote/addNew/purchaseRequisitionNote.php
//$formName		  	= 'frmItem';
include  	"{$backwardseperator}dataAccess/permisionCheck.inc";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PRN Listing</title>

<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="rptPurchaseRequisitionNote-js.js"></script>
<script type="application/javascript" src="grid-js.js"></script>

</head>

<body>
<form id="frmPRNlisting" name="frmPRNlisting" method="get" action="purchaseRequisitionNoteListing.php">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
<?php
$stat = $_GET['cboPrnStatus'];
if($stat==''){
$stat='2';
}
$prnApproveLevel = (int)getApproveLevel($programName);

$sql = "SELECT
Max(trn_prnheader.intApproveLevels) AS appLevels
FROM trn_prnheader";
 $result = $db->RunQuery($sql);
 $row=mysqli_fetch_array($result);
 if($row['appLevels']>$prnApproveLevel){
	$prnApproveLevel= $row['appLevels'];
 }
					 
?>
<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">PRN
		    <select style="width:100px" name="cboPrnStatus" id="cboPrnStatus" onchange="submit();">
             <option <?php if($stat==2){ ?> selected="selected" <?php } ?>value="2">Pending</option>
              <option <?php if($stat==1){ ?> selected="selected" <?php } ?>value="1">Confirmed</option>
              <option <?php if($stat==0){ ?> selected="selected" <?php } ?>value="0">Rejected</option>
            <option  <?php if($stat==3){ ?> selected="selected" <?php } ?> value="3">All</option>
	        </select>
LISTING</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableBorder_allRound">
          <tr>
            <td width="10%" height="22" class="normalfnt">&nbsp;</td>
            <td width="22%">&nbsp;</td>
            <td width="17%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="11%" class="normalfnt">&nbsp;</td>
            <td width="15%">&nbsp;</td>
            </tr>
          <tr>
            <td height="22" class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td><div style="width:900px;height:300px;overflow:scroll" >
          <table width="100%" class="grid" >
            <tr class="gridHeader">
              <td width="10%" height="21" >PRN No</td>
              <td width="11%" >Request Date</td>
              <td width="11%" >Required Date</td>
              <td width="11%">Department</td>
              <td width="9%">PRN Qty</td>
              <td width="7%">PO Qty</td>
              <td width="8%">Progress(%)</td>
              <td width="11%">User</td>
              <?php
				for($i=1; $i<=$prnApproveLevel; $i++)
				{
					if($i==1)
					$ap="1st Approval";
					else if($i==2)
					$ap="2nd Approval";
					else if($i==3)
					$ap="3rd Approval";
					else
					$ap=$i."th Approval";
			  ?>
              <td width="9%"><?php echo $ap ?></td>
              <?php
				}
			  ?>
              <td width="13%"> Report</td>
              </tr>
                 <?php
				 if($stat=='3')//all
				 $sqlCon="";
				 else if($stat==0)//rej
				 $sqlCon="and trn_prnheader.intStatus=0";
				 else if($stat==1)//conf
				 $sqlCon="and trn_prnheader.intStatus=1";
				 else if($stat==2)//pending
				 $sqlCon="and trn_prnheader.intStatus<=trn_prnheader.intApproveLevels+1 and trn_prnheader.intStatus>1";
				 
	 	 		 $sql = "SELECT
trn_prnheader.intPrnNo,
trn_prnheader.intYear,
trn_prnheader.dtmPrnDate,
trn_prnheader.dtmRequiredDate,
mst_department.strName as department ,
sys_users.strUserName as UserName,  
trn_prnheader.intStatus,
trn_prnheader.intApproveLevels,
sum(trn_prndetails.dblPrnQty) as dblPrnQty,
sum(trn_prndetails.dblPoQty) as dblPoQty  
FROM
trn_prnheader 
Inner Join trn_prndetails ON trn_prnheader.intPrnNo = trn_prndetails.intPrnNo AND trn_prnheader.intYear = trn_prndetails.intYear 
Inner Join mst_department ON trn_prnheader.intDepartment = mst_department.intId
Inner Join sys_users ON trn_prnheader.intUser = sys_users.intUserId 
Inner Join mst_locations ON trn_prnheader.intCompany = mst_locations.intId WHERE mst_locations.intCompanyId = '$company' and  trn_prnheader.intCompany = '$location' ".$sqlCon." 
GROUP BY trn_prnheader.intPrnNo,trn_prnheader.intYear 
ORDER BY
trn_prnheader.intPrnNo DESC,
trn_prnheader.intYear DESC";
	//echo $sql;
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					 $prnNo=$row['intPrnNo'];
					 $year=$row['intYear'];
					 $status=$row['intStatus'];
					 $savedLevels=$row['intApproveLevels'];
	  			 ?>
            <tr class="normalfnt">
              <?php
				$editMode=0;
				if($row['intStatus']==0)
				$editMode=1;
				else if($row['intApproveLevels']+1==$row['intStatus'])
				$editMode=1;
				//user can edit only saved and rejected prns ...... 
			  ?>
              <td align="center" bgcolor="#FFFFFF"><?php if($editMode==1){ ?><a href="../addNew/purchaseRequisitionNote.php?prnNo=<?php echo $row['intPrnNo']?>&year=<?php echo $row['intYear']?>" target="_blank"> <?php } ?><?php echo $row['intPrnNo']."/".$row['intYear'];?><?php if($editMode==1){ ?></a><?php } ?></td>
              <td bgcolor="#FFFFFF"><?php echo $row['dtmPrnDate']?></td>
              <td bgcolor="#FFFFFF"><?php echo $row['dtmRequiredDate']?></td>
              <td bgcolor="#FFFFFF"><?php echo $row['department']?></td>
              <td bgcolor="#FFFFFF" class="normalfntRight"><?php echo $row['dblPrnQty']?></td>
              <td bgcolor="#FFFFFF" class="normalfntRight"><?php echo $row['dblPoQty']?></td>
              <td bgcolor="#FFFFFF" class="normalfntRight"><?php echo round($row['dblPoQty']/$row['dblPrnQty']*100)."%"; ?></td>
              <td bgcolor="#FFFFFF"><?php echo $row['UserName']?></td>
              <?php
			  $tempUser="";
				for($i=1; $i<=$prnApproveLevel; $i++)
				{
					 $sqlc = "SELECT
							trn_prnheader_approvedby.intApproveUser,
							trn_prnheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName,
							trn_prnheader_approvedby.intApproveLevelNo
							FROM
							trn_prnheader_approvedby
							Inner Join sys_users ON trn_prnheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							trn_prnheader_approvedby.intPrnNo =  '$prnNo' AND
							trn_prnheader_approvedby.intYear =  '$year' AND
							trn_prnheader_approvedby.intApproveLevelNo =  '$i'";
					$resultc = $db->RunQuery($sqlc);
					$rowc=mysqli_fetch_array($resultc);

					$k=$savedLevels+2-$status;//next approve level
					$sqlp = "SELECT
							menupermision.int".$k."Approval 
							FROM menupermision 
							Inner Join menus ON menupermision.intMenuId = menus.intId
							WHERE
							menus.strCode =  '$programCode' AND
							menupermision.intUserId =  '$intUser'";	
							
					 $resultp = $db->RunQuery($sqlp);
					 $rowp=mysqli_fetch_array($resultp);
					 
					$app=0; 
					if($status==0)
						$app=0; 
					else if($rowp['int'.$i.'Approval']==1){
					 if($i==1)
						$app=1; 
					 else if($tempUser!='')
						$app=1; 
					}

				?>
              <td bgcolor="#FFFFFF"><?php echo $rowc['UserName']?>
			  <?php if($rowc['UserName']==''){ ?>
			  <?php if(($app==1) && ($savedLevels>=$i)){?>
              <a href="rptPurchaseRequisitionNote.php?prnNo=<?php echo $row['intPrnNo']?>&year=<?php echo $row['intYear']?>&approveMode=1" target="_blank"><span class="normalfntBlue"><strong >Approve</strong></span></a>
			  <?php } ?>
              <?php }
			  else{
				   echo "(".$rowc['dtApprovedDate'].")"; 
				   } ?>
                   <?php
				   if($i>$savedLevels){
				   		echo "&nbsp;&nbsp;&nbsp;-------";
				   }else if(($status==0) && ($i==$savedLevels)){
					  echo "<strong class=\"compulsoryRed\">Rejected</strong>" ;
				   }
				   ?>
               </td>
              <?php
$tempUser=	$rowc['UserName'];		  
				}
				?>

              <td bgcolor="#FFFFFF"><a href="rptPurchaseRequisitionNote.php?prnNo=<?php echo $row['intPrnNo']?>&year=<?php echo $row['intYear']?>&approveMode=0" target="_blank"><img src="../../../../images/view.png" width="91" height="19" /></a></td>
              </tr>
              	<?php 
        		 } 
       			 ?>
            </table>
          </div></td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound"><img src="../../../../planning/img/Tclose.jpg" width="92" height="24" /></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
</body>
</html>

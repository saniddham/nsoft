<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
$programName='Purchase Request Note';
$programCode='P0222'; 


 if($requestType=='getValidation')
	{
		$errorFlg=0;
		$serial  = $_REQUEST['serial'];
		$serialArray=explode("/",$serial);
		$no=$serialArray[0];
		$year=$serialArray[1];
		
		$sql = "SELECT
		trn_prnheader.intStatus, 
		trn_prnheader.intApproveLevels   
		FROM 
		trn_prnheader 
		WHERE
		trn_prnheader.intPrnNo =  '$no' AND
		trn_prnheader.intYear =  '$year'";

		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$approveLevels=$row['intApproveLevels'];
		$status=$row['intStatus'];
		

		$confirmatonMode=loadConfirmatonMode($programCode,$status,$approveLevels,$userId);
		
	if($status==1){
		$errorFlg=1;
		$msg ="Final confirmation of this PRN is already raised"; 
	}	
	else if($status==0){// 
		$errorFlg = 1;
		$msg ="This PRN is rejected"; 
	}
	else if($confirmatonMode==0){// 
		$errorFlg = 1;
		$msg ="No Permission to Approve"; 
	}
	
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);
	}
	
else if($requestType=='validateRejecton')
	{
		$errorFlg=0;
		$serial  = $_REQUEST['serial'];
		$serialArray=explode("/",$serial);
		$no=$serialArray[0];
		$year=$serialArray[1];
		
		$sql = "SELECT
		trn_prnheader.intStatus, 
		trn_prnheader.intApproveLevels   
		FROM 
		trn_prnheader 
		WHERE
		trn_prnheader.intPrnNo =  '$no' AND
		trn_prnheader.intYear =  '$year'";

		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$approveLevels=$row['intApproveLevels'];
		$status=$row['intStatus'];
		
		$msg =""; 
		
		$rejectionMode=loadRejectionMode($programCode,$row['intStatus'],$row['intApproveLevels'],$userId);

		if($row['intStatus']==1){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="Final confirmation of this PRN is already raised.So cant reject"; 
		}
		else if($row['intStatus']==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="This PRN is already Rejected"; 
		}
		else if($rejectionMode==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="No Permission to reject this PRN"; 
		}
		else{//confirmation has been raised
			$errorFlg = 0;
			
		}

	
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);

	}
	
//------------------------------function loadRejectionMode-------------------
function loadRejectionMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$rejectMode=0;
	$sqlp = "SELECT
		menupermision.intReject 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intReject']==1){
	 if($intStatus!=0){
	 $rejectMode=1;
	 }
	}
	 
	return $rejectMode;
}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
			 
	return $editMode;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	 $sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
//--------------------------------------------------------

?>

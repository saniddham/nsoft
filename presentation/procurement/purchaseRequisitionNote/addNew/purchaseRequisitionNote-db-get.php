<?php
// ini_set('display_errors',1);//$_SESSION['ROOT_PATH'].
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$company 	= $_SESSION['headCompanyId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";


	require_once "{$backwardseperator}class/cls_permisions.php";
	
	//---------check Permission to save recive qty more than PO qty.------------
	$objpermisionget= new cls_permisions($db);
	$permisionEnableProject 	= $objpermisionget->getPermisionCompanyEnableProject($company);
	//------------------------------------------------------------------------
	
	//------------------------------------------------------------------------
	/////////// type of print load part /////////////////////
	if($requestType=='loadCombo')
	{
		$sql = "SELECT
					intId,
					strName
				FROM mst_techniques
				order by strName
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
	}
	if($requestType=='loadSubProjectOptions')
	{
		$project  = $_REQUEST['project'];
		$year  = $_REQUEST['year'];
		$sql = "SELECT
				trn_orderdetails.strSalesOrderNo,intSalesOrderId
				FROM trn_orderdetails
				WHERE
				trn_orderdetails.intOrderNo =  '$project' AND 
				trn_orderdetails.intOrderYear =  '$year' 
				ORDER BY
				trn_orderdetails.strSalesOrderNo ASC

				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['strSalesOrderNo']."\">".$row['strSalesOrderNo']."</option>";
		}
		echo $html;
	}
	if($requestType=='loadProjectCombo')
	{
		$sql = "SELECT DISTINCT
				trn_orderheader.intOrderNo
				FROM trn_orderheader
				WHERE
				trn_orderheader.intStatus =  '1'
				ORDER BY
				trn_orderheader.intOrderNo ASC
				";
		$result = $db->RunQuery($sql);
		if($permisionEnableProject==1){
		$html = "<select name=\"cboOrdrNo\" id=\"cboOrdrNo\" style=\"width:100px\"  class=\"clsOrderNo\" ><option value=\"\"></option>";
		}
		else{
		$html = "<select name=\"cboOrdrNo\" id=\"cboOrdrNo\" style=\"width:100px\"  class=\"clsOrderNo\" disabled=\"disabled\"><option value=\"\"></option>";
		}
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";
		}
		$html .= "</select>";
		echo $html;
	}
	if($requestType=='loadOrderYearCombo')
	{
		$sql = "SELECT DISTINCT
				trn_orderheader.intOrderYear
				FROM trn_orderheader
				WHERE
				trn_orderheader.intStatus =  '1'
				ORDER BY
				trn_orderheader.intOrderYear ASC
				";
		$result = $db->RunQuery($sql);
		if($permisionEnableProject==1){
		$html = "<select name=\"cboOrderYear\" id=\"cboOrderYear\" style=\"width:60px\"  class=\"clsOrderYear\" ><option value=\"\"></option>";
		}
		else{
		$html = "<select name=\"cboOrderYear\" id=\"cboOrderYear\" style=\"width:60px\"  class=\"clsOrderYear\" disabled=\"disabled\"><option value=\"\"></option>";
		}
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intOrderYear']."\">".$row['intOrderYear']."</option>";
		}
		$html .= "</select>";
		echo $html;
	}
	if($requestType=='loadSubProjectCombo')
	{
		$html = "";
		if($permisionEnableProject==1){
		$html = "<select name=\"cboSubProject\" id=\"cboSubProject\" style=\"width:80px\"  class=\"clsSalesOrderNo\" ><option value=\"\"></option>";
		}
		else{
		$html = "<select name=\"cboSubProject\" id=\"cboSubProject\" style=\"width:80px\"  class=\"clsSalesOrderNo\" disabled=\"disabled\"><option value=\"\"></option>";
		}
		$html .= "</select>";
		echo $html;
	}
	
	else if($requestType=='loadDetails')
	{
		$id  = $_REQUEST['id'];
		$sql = "SELECT 	strName,
						strRemark,
						intStatus
				FROM mst_techniques
				WHERE
					intId =  '$id'
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['name'] 	= $row['strName'];
			$response['remark'] = $row['strRemark'];
			$response['status'] = ($row['intStatus']?true:false);
		}
		echo json_encode($response);
	}
	else if($requestType=='loadSubCategory')
	{
		
		$mainCategory  = $_REQUEST['mainCategory'];
		$sql = "SELECT
				mst_subcategory.intId,
				mst_subcategory.strCode,
				mst_subcategory.strName
				FROM mst_subcategory
				WHERE
				mst_subcategory.intMainCategory =  '$mainCategory'";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	else if($requestType=='loadItems')
	{
		$mainCategory  = $_REQUEST['mainCategory'];
		$subCategory  = $_REQUEST['subCategory'];
		$description  = $_REQUEST['description'];
			$sql="SELECT
				mst_item.intId,
				mst_item.intMainCategory,
				mst_item.intSubCategory,
				mst_item.strCode as ItemCode,
				mst_item.strName as itemName,
				mst_maincategory.strName as mainCatName,
				mst_subcategory.strName as subCatName,
                                mst_item.strCode as supItemCode,
                                mst_item.ITEM_HIDE as itemHide
				FROM
				mst_item
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
                                LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
				WHERE 
				mst_item.intStatus = '1' AND 
				mst_item.intMainCategory =  '$mainCategory' ";
				if($subCategory!=''){
				$sql.=" AND
				mst_item.intSubCategory =  '$subCategory'";
				}
				if($description!=''){
				$sql.=" AND
				mst_item.strName LIKE  '%$description%'";
				}
				$sql.=" Order by mst_maincategory.strName asc, mst_subcategory.strName asc, mst_item.strName asc";
		//echo $sql;
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$prnQty = $row['dblPrnQty'];
			if(!$prnQty)
			$prnQty=0;
			$poQty = $row['dblPoQty'];
			if(!$poQty)
			$poQty=0;
			
			$data['itemId'] 	= $row['intId'];
			$data['maincatId'] = $row['intMainCategory'];
			$data['subCatId'] 	= $row['intSubCategory'];
			$data['code'] = $row['ItemCode'];
                        $data['supItemCode'] = $row['supItemCode'];
			$data['itemName'] = $row['itemName'];
			$data['mainCatName'] = $row['mainCatName'];
			$data['subCatName'] = $row['subCatName'];
			$data['itemHide']=$row['itemHide'];
			
			$arrCombo[] = $data;
		}
		$response['arrCombo'] 	= $arrCombo;
		echo json_encode($response);

	}
	//---------------------
	else if($requestType=='getExistingPRN'){
		
	$orderNo = $_REQUEST['orderNo'];
	$orderYear 	= $_REQUEST['orderYear'];
	$serialNo = $_REQUEST['serialNo'];
	$Year 	= $_REQUEST['Year'];
	
	$sql = "SELECT
			trn_prnheader.intPrnNo,
			trn_prnheader.intYear
			FROM trn_prnheader
			WHERE
			trn_prnheader.intOrderNo =  '$orderNo' AND
			trn_prnheader.intOrderYear =  '$orderYear' AND
			trn_prnheader.intStatus <>  '0' AND
			trn_prnheader.intPrnNo <>  '$serialNo' AND
			trn_prnheader.intYear <>  '$year'
			";	
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$prnNo = $row['intPRNno'];
		return $prnNo;
		
	}
	//---------------------------------
	else if($requestType=='getViewPermission'){
		
	$serialNo = $_REQUEST['serialNo'];
	$Year 	= $_REQUEST['Year'];
	
	$viewProject=0;
	
	if($permisionEnableProject==1)
	$viewProject=1;
	  
 	 $sql = "	SELECT
						trn_prndetails.intProject
						FROM
						trn_prndetails 
						WHERE
						trn_prndetails.intPrnNo =  '$serialNo' AND
						trn_prndetails.intYear =  '$Year'  and 
						trn_prndetails.intProject>0 
						";
	$result = $db->RunQuery($sql);
	$rows = mysqli_num_rows($result);
	if($rows>0)
	{
	  $viewProject=1;
	}
	
	echo $viewProject;
		
	}
	
	
?>

<?php 
	//ini_set('display_errors',0);
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$companyId 	= $_SESSION['CompanyID'];
	include "{$backwardseperator}dataAccess/Connector.php";
	include "{$backwardseperator}class/cls_commonFunctions_get.php"; $cls_commonFunctions_get = new cls_commonFunctions_get($db);
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	
	$serialNo 		= $_REQUEST['serialNo'];
	$year 			= $_REQUEST['Year'];
	$date 			= $_REQUEST['date'];
	$reqDate 		= $_REQUEST['reqDate'];
	$internalUse 	= $_REQUEST['internalUse'];
	$remarks 		= $_REQUEST['remarks'];
	$department 	= $_REQUEST['department'];
	$orderNo 		= $_REQUEST['orderNo'];
	$orderYear 		= $_REQUEST['orderYear'];
	$menuId 		= $_REQUEST['menuId'];
	
	if($menuId==222){
		$title		= "Raw Materials";
		$program	= 'Purchase Request Note';
		$programCode='P0222';
		$poType		= 1;
	}
	else if($menuId==1142){
		$title		= "Non - Raw Materials";
		$program	= 'Purchase Request Note - None Raw Materials';
		$programCode='P1142';
		$poType		= 2;
	}
	else if($menuId==1146){
		$title		= "All";
		$program	= 'Purchase Request Note - All';
		$programCode='P1146';
		$poType		= 3;
	}
	
	$arr 		= json_decode($_REQUEST['arr'], true);
	
	
//------------save---------------------------	
	if($requestType=='save')
	{
		$rollBackFlag=0;
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		//$sys_approvelevels->set($programCode);
		//$ApproveLevels		= $sys_approvelevels->getintApprovalLevel();
		$ApproveLevels=(int)getApproveLevel2($program);
		$prnApproveLevel = $ApproveLevels+1;
	
		if($serialNo==''){
			$serialNo 	= getNextPrnNo();
			$year = date('Y');
			$editMode=0;
		}
		else{
			$editMode=1;
		}
		
		//-----------delete and insert to header table-----------------------
		if($editMode==1){
			$sql = "UPDATE `trn_prnheader` SET intStatus ='$prnApproveLevel', 
												intApproveLevels ='$ApproveLevels', 
												intModifiedBy ='$userId', 
												dtmModifiedDate =now(), 
												dtmRequiredDate ='$reqDate', 
												intInternalUsage ='$internalUse', 
												strRemarks ='$remarks', 
												dtmPrnDate ='$date'
					                            WHERE (`intPrnNo`='$serialNo') AND (`intYear`='$year')";
			$result = $db->RunQuery2($sql);
		}
		else{
			$sql = "DELETE FROM `trn_prnheader` WHERE  (`intPrnNo`='$serialNo') AND (`intYear`='$year')";
			$result1 = $db->RunQuery2($sql);
			
			if(($orderNo!='') && ($orderYear!='')){
			$sql = "INSERT INTO `trn_prnheader` (`intPrnNo`,`intYear`,`intDepartment`,intCompany,intInternalUsage,dtmRequiredDate,dtmPrnDate,strRemarks,intUser,intStatus,intApproveLevels,intOrderNo,intOrderYear,PO_TYPE) 
					VALUES ('$serialNo','$year','$department','$companyId','$internalUse','$reqDate','$date','$remarks','$userId','$prnApproveLevel','$ApproveLevels','$orderNo','$orderYear','$poType')";
			}
			else{
			$sql = "INSERT INTO `trn_prnheader` (`intPrnNo`,`intYear`,`intDepartment`,intCompany,intInternalUsage,dtmRequiredDate,dtmPrnDate,strRemarks,intUser,intStatus,intApproveLevels,PO_TYPE) 
					VALUES ('$serialNo','$year','$department','$companyId','$internalUse','$reqDate','$date','$remarks','$userId','$prnApproveLevel','$ApproveLevels','$poType')";
			}
					
			$result = $db->RunQuery2($sql);
		}
		//-----------delete and insert to detail table-----------------------
		if(($result) && ($rollBackFlag!=1)){
		$sql = "DELETE FROM `trn_prndetails` WHERE (`intPrnNo`='$serialNo') AND (`intYear`='$year')";
		$result2 = $db->RunQuery2($sql);
		;
			$toSave=0;
			$saved=0;
			$rollBackFlag=0;
			$rollBackMsg ="Invalid PRN Qtys for items are..."; 
			foreach($arr as $arrVal)
			{
				$itemID 		= $arrVal['itemID'];
				$itemName 		= $cls_commonFunctions_get->replace($arrVal['itemName']);
				$quantity 		= $arrVal['quantity'];
				$orderNo 		= $arrVal['orderNo'];
				$orderYear 		= $arrVal['orderYear'];
				$salesOrderNo 	= $cls_commonFunctions_get->replace($arrVal['salesOrderNo']);
				
				
				if($orderNo==''){
					$orderNo=0;
				}
				if($orderYear==''){
					$orderYear=0;
				}
				
				if($quantity<=0){
					$rollBackFlag=1;
					$rollBackMsg .="\n ".$itemName." =".$quantity;
					$result3=0;
				}
				else if(($project!=0) && ($subProject==0)){
					$rollBackFlag=1;
					$rollBackMsg ="Please select Sales Order No";
					$result3=0;
				}
				else{
				$sql = "INSERT INTO `trn_prndetails` (`intPrnNo`,`intYear`,`intItem`,`intOrderNo`,`intOrderYear`,`strSalesOrderNo`,`dblPrnQty`,`dblPoQty`) 
				VALUES ('$serialNo','$year','$itemID','$orderNo','$orderYear','$salesOrderNo','$quantity','0')";
				$result3 = $db->RunQuery2($sql);
				}
				if($result3==1){
				$saved++;
				}
			  $toSave++;
			}
		}
		
		
		if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= '';
		}
		else if(($result) && ($toSave==$saved)){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			if($editMode==1)
			$response['msg'] 		= 'Updated successfully.';
			else
			$response['msg'] 		= 'Saved successfully.';
			
			$response['serialNo'] 		= $serialNo;
			$response['year'] 		= $year;
		}
		else{
			$db->RunQuery2('Rollback');
			
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		$db->CloseConnection();		
	}
//----------------------------------------
	echo json_encode($response);
//-----------------------------------------	


	function getNextPrnNo()
	{
		global $db;
		global $companyId;
		$sql = "SELECT
				sys_no.intPRNno
				FROM sys_no
				WHERE
				sys_no.intCompanyId =  '$companyId'
				";	
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$nextDispatchNo = $row['intPRNno'];
		
		$sql = "UPDATE `sys_no` SET intPRNno=intPRNno+1 WHERE (`intCompanyId`='$companyId')  ";
		$db->RunQuery2($sql);
		
		return $nextDispatchNo;
	}
//---------------------------------------	
?>
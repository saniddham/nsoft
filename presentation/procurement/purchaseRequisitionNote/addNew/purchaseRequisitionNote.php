<?php
//ini_set('display_errors',1);

$intUser  	= $_SESSION["userId"];
$company 	= $_SESSION['headCompanyId'];

//include "include/javascript.html";

require_once "{$backwardseperator}class/cls_permisions.php";
include_once "class/tables/sys_approvelevels.php";$sys_approvelevels = new sys_approvelevels($db);

//---------check Permission to save recive qty more than PO qty.------------
$objpermisionget= new cls_permisions($db);
$permisionEnableProject 	= $objpermisionget->getPermisionCompanyEnableProject($company);
//------------------------------------------------------------------------

$prnNo = $_REQUEST['prnNo'];
$year = $_REQUEST['year'];
$programName='Purchase Request Note';

$arrItem = json_decode($_REQUEST['arrItem'],true);

//////////////// NOTE DETAILS ////////////////////////////////////
$arrDesc = json_decode($_REQUEST['arrDesc'],true);
if($_REQUEST['q']==222){
	$note	= "(Style No:".$arrDesc[0]['styleNo'].") / (Graphic No:".$arrDesc[0]['graphicNo'].") / (Combo :".$arrDesc[0]['combo'].") /(Sales Order No :".$arrDesc[0]['salesOrderNo'].") /(Order Qty :".$arrDesc[0]['orderQty'].") / (Extra Qty % :".$arrDesc[0]['ExtraQty'].")";
}
////////////////END NOTE DETAILS ////////////////////////////////
$q	= $_REQUEST['q'];

if($_REQUEST['q']==222){
	$title	= "Raw Materials";
	$programCode='P0222';
}
else if($_REQUEST['q']==1142){
	$title	= "Non-Raw Materials";
	$programCode='P1142';
}
else if($_REQUEST['q']==1146){
	$title	= "RM and Non-RM";
	$programCode='P1146';
}



$orderNo 	= 	$arrDesc[0]['orderNo'];
$orderYear 	= 	$arrDesc[0]['orderYear'];

$extraQtyPercentage 	= 	$arrDesc[0]['ExtraQty'];
if($extraQtyPercentage==''){
	$extraQtyPercentage =0;	
}

$prnType = $_REQUEST['type'];
//print_r($arrItem);
////////////////////////

if(($prnNo=='')&&($year=='')){
	$sys_approvelevels->set($programCode);
	$intStatus		= $sys_approvelevels->getintApprovalLevel();
	$savedStat 		= $intStatus;
	$intStatus		= $intStatus+1;
	$date='';
}
else{
   $sql = "SELECT
			trn_prnheader.intApproveLevels,
			trn_prnheader.intStatus 
			FROM
			trn_prnheader 
			WHERE
			trn_prnheader.intPrnNo =  '$prnNo' AND
			trn_prnheader.intYear =  '$year' ";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$savedStat=$row['intApproveLevels'];
		$intStatus=$row['intStatus'];
	}
}

//url-presentation/procurement/purchaseRequisitionNote/addNew/purchaseRequisitionNote.php
$editMode=loadEditMode($programCode,$intStatus,$savedStat,$intUser);
$confirmatonMode=loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser);
	if($prnNo==''){
		$confirmatonMode=0;	
	}

$mainPage=$backwardseperator."main.php";
if($programCode=='P1146'){
	$confirmatonMode=0;	
	$editMode=0;
} 
 ?>

<title>Purchase Requisition Note - <?php echo $title; ?></title>
<style type="text/css">
.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>
<form id="frmPurchaseRequisitionNote" name="frmPurchaseRequisitionNote" method="post" >
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">

</table>

      <?php 
	   $sql = "SELECT
				trn_prnheader.dtmRequiredDate,
				trn_prnheader.dtmPrnDate,
				trn_prnheader.intInternalUsage,
				trn_prnheader.intDepartment,
				trn_prnheader.intStatus,
				trn_prnheader.strRemarks, 
				trn_prnheader.intOrderNo, 
				trn_prnheader.intOrderYear  
				FROM
				trn_prnheader 
				WHERE
				trn_prnheader.intPrnNo =  '$prnNo' AND
				trn_prnheader.intYear =  '$year'
";
				
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						$reqdat=$row['dtmRequiredDate'];
						$department=$row['intDepartment'];
						$prndat=$row['dtmPrnDate'];
						$interUsg=$row['intInternalUsage'];
						$remarks=$row['strRemarks'];
						$intStatus=$row['intStatus'];
						if($prnType!='AutoPRN'){
						$orderNo=$row['intOrderNo'];
						$orderYear=$row['intOrderYear'];
						}
					}
					
					//default user department
					if($prnNo==''){
					
					 $sql1 = "SELECT
								sys_users.intDepartmentId
								FROM
								sys_users
								WHERE
								sys_users.intUserId =  '$intUser'";	
					$result1 = $db->RunQuery($sql1);
					$row1=mysqli_fetch_array($result1);
					$department=$row1['intDepartmentId'];
					}
					
?>

<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">Purchase Requisition Note - <?php echo $title; ?><div id="menuId" style="display:none"><?php echo $q;?></div></div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF"><tr>
    <td><table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="14%" height="22" class="normalfnt">PRN No</td>
            <td width="24%"><input name="txtPrnNo" type="text" disabled="disabled" class="txtText" id="txtPrnNo" style="width:60px" value="<?php echo $prnNo ?>"/><input name="txtYear" type="text" disabled="disabled" class="txtText" id="txtYear" style="width:40px" value="<?php echo $year ?>"/></td>
            <td width="11%">&nbsp;</td>
            <td width="23%">&nbsp;</td>
            <td width="14%" class="normalfnt">Date</td>
            <td width="14%" align="left"><input name="dtPrnDate" type="text" value="<?php if($prnNo){ echo $prndat; }else { echo date("Y-m-d"); }?>" class="txtbox" id="dtPrnDate" style="width:98px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" disabled="disabled"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
          </tr>
          <tr>
            <td height="22" class="normalfnt">Department</td>
            <td><select name="cboDepartment" id="cboDepartment" style="width:250px"  class="validate[required]" >
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_department.intId,
							mst_department.strName
							FROM
							mst_department
							WHERE
							mst_department.intStatus =  '1'";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$department)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
            </select></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">Required Date</td>
            <td align="left"><input name="dtRequiredDate" type="text" value="<?php if($prnNo){ echo $reqdat; }else { echo date("Y-m-d"); }?>" class="txtbox" id="dtRequiredDate" style="width:98px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
          </tr>
          <tr>
            <td height="22" class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt"><div style="display:none">Internal Use</div></td>
            <td align="left"><input type="checkbox" name="chkInternal" id="chkInternal" <?php if($interUsg==1){ ?>checked="checked" <?php } ?> style="display:none"/></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="14%" height="27" rowspan="2" class="normalfnt">Note</td>
            <td width="34%" rowspan="2"><textarea name="txtRemarks" id="txtRemarks" cols="35" rows="3"><?php
			 if($prnNo=='')
			 	echo $note;
			 else
			 	echo $remarks 
			 
			 ?></textarea></td>
            <td width="26%" rowspan="2"><input name="txtOrderNo" id="txtOrderNo" type="text" value="<?php echo $orderNo ?>" style="display:none" /><input name="txtOrderYear" id="txtOrderYear" type="text" value="<?php echo $orderYear ?>"  style="display:none"  /></td>
            <td width="10%" rowspan="2" valign="top" class="normalfnt">&nbsp;</td>
            <td width="16%" align="right" valign="top">&nbsp;</td>
            </tr>
          <tr>
            <td align="right" valign="bottom"><img src="images/Tadd.jpg" width="92" height="24" id="butAddItems" name="butAddItems" class="mouseover"  /></td>
            </tr>
           
           <?php
     $sql = "SELECT
			trn_prnheader.intPrnNo,
			trn_prnheader.intYear
			FROM trn_prnheader
			WHERE
			trn_prnheader.intOrderNo =  '$orderNo' AND
			trn_prnheader.intOrderYear =  '$orderYear'  
			order by trn_prnheader.intPrnNo asc ,trn_prnheader.intYear asc
";
			
			$result = $db->RunQuery($sql);
			$i=0;
			$prnString="";
			while($row=mysqli_fetch_array($result))
			{
				$i++;
				
				$prnNo1=$row['intPrnNo'];
				$year1=$row['intYear'];
				$prnString .="($prnNo1/$year1),";
			}
			$prnString=rtrim($prnString, ",");
			
			if($i>0){
		   ?> 
          <tr bgcolor="#FFC4C4">
            <td class="normalfnt" colspan="5">Already Raised PRNs for Order (<?php echo $orderNo."/".$orderYear?>) are: <?php echo $prnString ?></td>
            </tr>
           <?php
			}
		   ?> 
            
          </table></td>
      </tr>
      <?php
	$viewProject=0;
	
	if($permisionEnableProject==1)
	$viewProject=1;
	  
 	$sql = "	SELECT
						trn_prndetails.intProject
						FROM
						trn_prndetails 
						WHERE
						trn_prndetails.intPrnNo =  '$prnNo' AND
						trn_prndetails.intYear =  '$year'  and 
						trn_prndetails.intProject>0 
						";
	$result = $db->RunQuery($sql);
	$rows = mysqli_num_rows($result);
	if($rows>0)
	{
	  $viewProject=1;
	}
	  
	  ?>
      <tr>
        <td><div style="width:900px;height:300px;overflow:scroll" >
          <table width="100%" class="grid" id="tblItems" >
            <tr class="gridHeader">
              <td width="3%" height="22" >Del</td>
              <td width="12%" >Main Category</td>
              <td width="11%" >Sub Category</td>
              <td width="12%" >Item Code</td>
              <td width="23%" >Item Description</td>
              <td width="23%"  <?php if($q!=222){ ?>style="display:none" <?php } ?>>Order No</td>
              <td width="9%"  <?php if($q!=222){ ?>style="display:none" <?php } ?>>Sales Order</td>
              <td width="11%" >Qty</td>
              </tr>
              <?php
			  if($prnType=='')
		{
					$sqlP = "SELECT
mst_item.strName item,
trn_prndetails.dblPrnQty,
trn_prndetails.dblPoQty,
mst_maincategory.strName category,
mst_subcategory.strName subCategory,
trn_prndetails.intItem,
mst_item.intMainCategory,
mst_item.intSubCategory ,
trn_prndetails.intOrderNo ,
trn_prndetails.intOrderYear ,
trn_prndetails.strSalesOrderNo ,
mst_item.strCode as supItemCode,
mst_item.strCode as ItemCode,
mst_item.ITEM_HIDE
FROM
trn_prndetails
Inner Join mst_item ON trn_prndetails.intItem = mst_item.intId
Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
WHERE 
/*mst_item.intStatus = '1' AND */
trn_prndetails.intPrnNo =  '$prnNo' AND
trn_prndetails.intYear =  '$year' 
Order by mst_maincategory.strName ASC, mst_subcategory.strName ASC, mst_item.strName ASC";
                                        
	$resultP = $db->RunQuery($sqlP);
	while($rowP=mysqli_fetch_array($resultP))
	{
		$categoryID=$rowP['intMainCategory'];
		$subCatID=$rowP['intSubCategory'];
		$category=$rowP['category'];
		$subCat=$rowP['subCategory'];
		$itemID=$rowP['intItem'];
		$itemCode=$rowP['ItemCode'];
                $supItemCode=$rowP['supItemCode'];
                $idemDesc=$rowP['item'];
		$qty=$rowP['dblPrnQty'];
		$orderNo 		= $rowP['intOrderNo'];
		$orderYear 		= $rowP['intOrderYear'];
		$salesOrderNo 	= $rowP['strSalesOrderNo'];
		$itemHide       = $rowP['ITEM_HIDE'];

		
	?>
			<tr class="normalfnt">
                            <td align="center" bgcolor="#FFFFFF"><img class="delImg" src="images/del.png" width="20" height="15" /></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $categoryID ?>'"><?php echo $category ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $subCatID?> "><?php echo $subCat ?></td>
            <td align="center" bgcolor="#FFFFFF" id="<?php echo $itemID ?>"><?php if($supItemCode!=null){echo $supItemCode;} else { echo $itemCode;  } ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $itemID?>" class="item"><?php if($itemHide==1 && $q==222 || $itemHide==1 && $q==1142  || $itemHide==1 && $q==1146){ echo '******';}else{ echo $idemDesc; } ?></td>
			<td align="center" bgcolor="#FFFFFF" <?php if($q!=222){ ?>style="display:none" <?php } ?>><select name="cboOrderYear" id="cboOrderYear" style="width:60px"  class="clsOrderYear" <?php if($q!=222){ ?> disabled="disabled"  <?php } ?>>
                  <?php
					$sql = "SELECT DISTINCT
                                                    trn_orderheader.intOrderYear
                                                    FROM trn_orderheader
                                                    WHERE
                                                    ((trn_orderheader.intStatus-1) <intApproveLevelStart)
                                                    ORDER BY
                                                    trn_orderheader.intOrderYear DESC";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intOrderYear']==$orderYear)
						echo "<option value=\"".$row['intOrderYear']."\" selected=\"selected\">".$row['intOrderYear']."</option>";	
						else
						echo "<option value=\"".$row['intOrderYear']."\">".$row['intOrderYear']."</option>";	
					}
				?>
            </select><select name="cboOrdrNo" id="cboOrdrNo" style="width:100px"  class="clsOrderNo" <?php if($permisionEnableProject!=1){ ?> disabled="disabled"  <?php } ?>>
                  <option value=""></option>
                  <?php
							$sql = "SELECT DISTINCT
								trn_orderheader.intOrderNo,
								mst_locations.intCompanyId
								FROM
								trn_orderheader
								Inner Join mst_locations ON trn_orderheader.intLocationId = mst_locations.intId
								WHERE
								((trn_orderheader.intStatus-1) <  trn_orderheader.intApproveLevelStart) 
								
								AND
								mst_locations.intCompanyId = $company
								ORDER BY
								trn_orderheader.intOrderNo DESC

								";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intOrderNo']==$orderNo)
						echo "<option value=\"".$row['intOrderNo']."\" selected=\"selected\">".$row['intOrderNo']."</option>";	
						else
						echo "<option value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";	
					}
				?>
            </select></td>
			<td align="center" bgcolor="#FFFFFF" <?php if($q!=222){ ?>style="display:none" <?php } ?>><select name="cboSubProject" id="cboSubProject" style="width:80px"  class="clsSalesOrderNo" <?php if($permisionEnableProject!=1){ ?> disabled="disabled"  <?php } ?>>
                  <option value=""></option>
                  <?php
					$sql = "SELECT
trn_orderdetails.strSalesOrderNo,intSalesOrderId
FROM trn_orderdetails
WHERE
trn_orderdetails.intOrderNo =  '$orderNo' AND 
trn_orderdetails.intOrderYear =  '$orderYear' 
ORDER BY
trn_orderdetails.strSalesOrderNo ASC";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['strSalesOrderNo']==$salesOrderNo)
						echo "<option value=\"".$row['strSalesOrderNo']."\" selected=\"selected\">".$row['strSalesOrderNo']."</option>";	
						else
						echo "<option value=\"".$row['strSalesOrderNo']."\">".$row['strSalesOrderNo']."</option>";	
					}
				?>
            </select></td>
			<td align="center" bgcolor="#FFFFFF" ><input id="txtQty" style="width:80px;text-align:center" type="text" value="<?php echo number_format($qty+round($extraQtyPercentage*$qty/100,4),6); ?>"  class="validate[required,custom[number]] Qty"/></td></tr>
	<?php
	}
	//echo 'testtttttttttttttttttttttttt';
	}
	else if($prnType=='AutoPRN')
	{
		
		foreach($arrItem as $xItem)
		{
			//print_r($arrItem);
			 $sql 	= "SELECT
							mst_item.intId,
							mst_maincategory.intId AS mianId,
							mst_subcategory.intId AS subId,
							mst_maincategory.strName AS mainName,
							mst_subcategory.strName AS subName,
							mst_units.strName AS unitName,
							mst_item.strCode AS itemCode,
							mst_item.strName AS itemName,
                                                        CONCAT(mst_supplier.strCode,mst_item.strCode) as supItemCode
						FROM
						mst_item
							Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
							Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
							Inner Join mst_units ON mst_units.intId = mst_item.intUOM
                                                        LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
						WHERE
							mst_item.intId =  '".$xItem['itemId']."'
						";
                         echo $sql;
			$result = $db->RunQuery($sql);
			while($row=mysqli_fetch_array($result))
			{
				?>
                	<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF"><img class="delImg" src="images/del.png" width="20" height="15" /></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $row['mianId'] ?>'"><?php echo $row['mainName']; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $row['subId']?> "><?php   echo $row['subName'] ?></td>
                        <td align="center" bgcolor="#FFFFFF" id="<?php echo $row['intId'] ?>"><?php   if($row['supItemCode']!=null){echo $row['supItemCode'];} else{ echo $row['itemCode'];}?></td>
			<td align="center" class="item" bgcolor="#FFFFFF" id="<?php echo $row['intId'] ?>"><?php   if($q==222 && $itemHide==1 || $q=1142 && $itemHide==1 || $q=1146 && $itemHide==1){echo '********';}else{ echo $row['itemName'];} ?></td>
            <td align="center" bgcolor="#FFFFFF" <?php if($q!=222){ ?>style="display:none" <?php } ?>><select name="cboOrderYear" id="cboOrderYear" style="width:60px"  class="clsOrderYear" <?php if($permisionEnableProject!=1){ ?> disabled="disabled"  <?php } ?>>
                  <?php
					$sql = "SELECT DISTINCT
trn_orderheader.intOrderYear
FROM trn_orderheader
WHERE
((trn_orderheader.intStatus-1) <intApproveLevelStart)  and intOrderYear = ".$xItem['orderYear']."
ORDER BY
trn_orderheader.intOrderYear DESC";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intOrderYear']==$orderYear)
						echo "<option value=\"".$row['intOrderYear']."\" selected=\"selected\">".$row['intOrderYear']."</option>";	
						else
						echo "<option value=\"".$row['intOrderYear']."\">".$row['intOrderYear']."</option>";	
					}
				?>
            </select><select name="cboOrdrNo" id="cboOrdrNo" style="width:100px"  class="clsOrderNo" <?php if($permisionEnableProject!=1){ ?> disabled="disabled"  <?php } ?>>
                  <?php
							$sql = "SELECT DISTINCT
trn_orderheader.intOrderNo
FROM trn_orderheader
WHERE
((trn_orderheader.intStatus-1) < intApproveLevelStart)  and (intOrderNo='".$xItem['orderNo']."') 
ORDER BY
trn_orderheader.intOrderNo ASC
";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intOrderNo']==$orderNo)
						echo "<option value=\"".$row['intOrderNo']."\" selected=\"selected\">".$row['intOrderNo']."</option>";	
						else
						echo "<option value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";	
					}
				?>
            </select></td>
			<td align="center" bgcolor="#FFFFFF" <?php if($viewProject!=1){ ?>style="display:none" <?php } ?>><select name="cboSubProject" id="cboSubProject" style="width:80px"  class="clsSalesOrderNo" <?php if($permisionEnableProject!=1){ ?> disabled="disabled"  <?php } ?>>
                  <option value=""></option>
                  <?php
					echo $sql = "SELECT
trn_orderdetails.strSalesOrderNo,intSalesOrderId
FROM trn_orderdetails
WHERE
trn_orderdetails.intOrderNo =  '".$xItem['orderNo']."' AND 
trn_orderdetails.intOrderYear =  '".$xItem['orderYear']."' 
ORDER BY
trn_orderdetails.strSalesOrderNo ASC";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['strSalesOrderNo']==$xItem['salesId'])
							echo "<option value=\"".$row['strSalesOrderNo']."\" selected=\"selected\">".$row['strSalesOrderNo']."</option>";	
						else
							echo "<option value=\"".$row['strSalesOrderNo']."\">".$row['strSalesOrderNo']."</option>";	
					}
				?>
            </select></td>
			<td align="center" bgcolor="#FFFFFF" ><input id="txtQty" style="width:80px;text-align:center" type="text" value="<?php echo $xItem['qty'] ?>"  class="validate[required,custom[number]] Qty"/></td></tr>
                <?php
			}	
		}	
	}

	?>
            </table>
          </div></td>
      </tr>
<?php
/*	        $intStatus;
			$savedStat = (int)getApproveLevel($programName);
			$editMode=0;
			if($intStatus==0)//rejected
			$editMode=1;
			else if($savedStat+1==$intStatus)//saved(not cnfirmed
			$editMode=1;
			
			
			 $k=$savedStat+2-$intStatus;
			   $sqlp = "SELECT
					menupermision.int".$k."Approval 
					FROM menupermision 
					Inner Join menus ON menupermision.intMenuId = menus.intId
					WHERE
					menus.strCode =  '$programCode' AND
					menupermision.intUserId =  '$intUser'";	
							
					 $resultp = $db->RunQuery($sqlp);
					 $rowp=mysqli_fetch_array($resultp);
					 if($rowp['int'.$k.'Approval']==1){
					 $confirmatonMode=1;
					 }
					 else{
					 $confirmatonMode=0;//no user permission to confirm
					 }
					// echo $intStatus;
					// echo $confirmatonMode;
*/	
//echo $editMode; 
 ?>  
      <tr>
        <td align="center" class="tableBorder_allRound"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td width="100%" align="center" bgcolor=""><img style="display:" border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/>
                  <?php
                if(($form_permision['add'] || $form_permision['edit']) && $editMode==1)
				{
				?>
                  <img  style="display:" border="0" src="images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/>
                  <?php
				}
                if($intStatus>1 && $confirmatonMode==1)
				{
				?>
                  <img src="images/Tconfirm.jpg" width="92" height="24" class="mouseover" id="butConfirm" name="butConfirm"/>
                  <?Php 
				}
				?><img src="images/Treport.jpg" width="92" height="24" class="mouseover" id="butReport" name="butReport" />
                  <a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
              </tr>
            </table></td>
    </tr>
  </table></td></tr></table>
  

  </div>
  </div>
</form>

	<!-- items to prn -->
	<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1">
   <!-- <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php" style="width:800;height:800;border:0;overflow:hidden">
    </iframe>-->
    </div>

	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>


<?php
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
	return $editMode;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	 $sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
?>



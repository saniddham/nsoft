<?php 
	ini_set('display_errors',0);
	
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$companyId 	= $_SESSION['CompanyID'];

	include "{$backwardseperator}dataAccess/Connector.php";
	include "{$backwardseperator}libraries/mail/mail.php";
	require_once "{$backwardseperator}class/cls_mail.php";
	
	$objMail = new cls_create_mail($db);
	//$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	//$requestType 			= $_REQUEST['requestType'];
	
	$prnNo = $_REQUEST['prnNo'];
	$year = $_REQUEST['year'];

	$sql = "SELECT trn_prnheader.intUser, trn_prnheader.intStatus, trn_prnheader.intApproveLevels,PO_TYPE FROM trn_prnheader WHERE (`intPrnNo`='$prnNo') AND (`intYear`='$year')";
	$results = $db->RunQuery($sql);
	$row = mysqli_fetch_array($results);
	$po_type = $row['PO_TYPE'];
	if($po_type==1){
		$title		= "Raw Materials";
		$programCode='P0222';
		$programName= 'Purchase Request Note';
	}
	else if($po_type==2){
		$title		= "None Raw Materials";
		$programCode='P1142';
		$programName= 'Purchase Request Note - None Raw Materials';
	}
	else if($po_type==3){
		$title		= "All";
		$programCode='P1146';
		$programName= 'Purchase Request Note - All';
	}
	
	
	
	if($_REQUEST['status']=='approve')
	{
		$sql = "UPDATE `trn_prnheader` SET `intStatus`=intStatus-1 WHERE (`intPrnNo`='$prnNo') AND (`intYear`='$year') and intStatus not in(0,1)";
		$result = $db->RunQuery($sql);
		
		if($result){
			$sql = "SELECT trn_prnheader.intUser, trn_prnheader.intStatus, trn_prnheader.intApproveLevels FROM trn_prnheader WHERE (`intPrnNo`='$prnNo') AND (`intYear`='$year')";
			$results = $db->RunQuery($sql);
			$row = mysqli_fetch_array($results);
		    $createdUser = $row['intUser'];
			$status = $row['intStatus'];
			$savedLevels = $row['intApproveLevels'];
			
			$approveLevel=$savedLevels+1-$status;
			$sqlI = "INSERT INTO `trn_prnheader_approvedby` (`intPrnNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
				VALUES ('$prnNo','$year','$approveLevel','$userId',now(),0)";
			$resultI = $db->RunQuery($sqlI);
			
			if($status==1)
			{
				if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
				//send mail to creater
				sendFinlConfirmedEmailToUser($prnNo,$year,$objMail,$mainPath,$root_path);
				}
				sendFinlConfirmedEmailToEmailConfig($createdUser,$userId,$prnNo,$year,$objMail,$mainPath,$root_path);
			}
		}
		
	}
	else if($_REQUEST['status']=='reject')
	{
		$sql = "SELECT trn_prnheader.intStatus, trn_prnheader.intApproveLevels, trn_prnheader.intUser  FROM trn_prnheader WHERE (`intPrnNo`='$prnNo') AND (`intYear`='$year')";
		$results = $db->RunQuery($sql);
		$row = mysqli_fetch_array($results);
		$createdUser = $row['intUser'];
		$status = $row['intStatus'];
		$savedLevels = $row['intApproveLevels'];
		
		$sql = "UPDATE `trn_prnheader` SET `intStatus`=0 WHERE (`intPrnNo`='$prnNo') AND (`intYear`='$year') ";
		$result = $db->RunQuery($sql);
		
		//$sql = "DELETE FROM `trn_prnheader_approvedby` WHERE (`intPrnNo`='$prnNo') AND (`intYear`='$year')";
		//$result2 = $db->RunQuery($sql);
		
		$sqlI = "INSERT INTO `trn_prnheader_approvedby` (`intPrnNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) VALUES ('$prnNo','$year','0','$userId',now(),0)";
		$resultI = $db->RunQuery($sqlI);
		
		if($resultI)
		{
			if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
			sendRejecedEmailToUser($prnNo,$year,$objMail,$mainPath,$root_path);
			}
		}
	}
	
	
	
	
	
	
	
	
	
//--------------------------------------------------------
function sendFinlConfirmedEmailToUser($serialNo,$year,$objMail,$mainPat,$root_path){
	global $db;
	global $mainPath;
		
		    $sql = "SELECT
			sys_users.strFullName,
			sys_users.strEmail 
			FROM 
			trn_prnheader
			Inner Join sys_users ON trn_prnheader.intUser = sys_users.intUserId
			WHERE
			trn_prnheader.intPrnNo =  '$serialNo' AND
			trn_prnheader.intYear =  '$year'";

				
		 $result = $db->RunQuery($sql);
		 
		$row=mysqli_fetch_array($result);
			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="CONFIRMED PURCHASE REQUEST NOTE('$serialNo'/'$year')"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST = NULL;
			
			$_REQUEST['program']='PURCHASE REQUEST NOTE';
			$_REQUEST['field1']='PRN No';
			$_REQUEST['field2']='PRN Year';
			$_REQUEST['field3']='';
			$_REQUEST['field4']='';
			$_REQUEST['field5']='';
			$_REQUEST['value1']=$serialNo;
			$_REQUEST['value2']=$year;
			$_REQUEST['value3']='';
			$_REQUEST['value4']='';
			$_REQUEST['value5']='';
			
			$_REQUEST['subject']="CONFIRMED PURCHASE REQUEST NOTE('$serialNo'/'$year')";
			
			$_REQUEST['statement1']="Approved this";
			$_REQUEST['statement2']="to view this";
			$_REQUEST['link']=base64_encode($_SESSION['MAIN_URL']."?q=917&prnNo=$serialNo&year=$year&approveMode=0"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
			$objMail->send_Response_Mail($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}
//--------------------------------------------------------
function sendRejecedEmailToUser($serialNo,$year,$objMail,$mainPat,$root_path){
	global $db;
	global $mainPath;
			
		    $sql = "SELECT
			sys_users.strFullName,
			sys_users.strEmail 
			FROM 
			trn_prnheader
			Inner Join sys_users ON trn_prnheader.intUser = sys_users.intUserId
			WHERE
			trn_prnheader.intPrnNo =  '$serialNo' AND
			trn_prnheader.intYear =  '$year'";

				
		 $result = $db->RunQuery($sql);
		 
		$row=mysqli_fetch_array($result);
			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="REJECTED PURCHASE REQUEST NOTE ('$serialNo'/'$year')"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST = NULL;
			
			$_REQUEST['program']='PURCHASE REQUEST NOTE';
			$_REQUEST['field1']='PRN No';
			$_REQUEST['field2']='PRN Year';
			$_REQUEST['field3']='';
			$_REQUEST['field4']='';
			$_REQUEST['field5']='';
			$_REQUEST['value1']=$serialNo;
			$_REQUEST['value2']=$year;
			$_REQUEST['value3']='';
			$_REQUEST['value4']='';
			$_REQUEST['value5']='';
			
			$_REQUEST['subject']="REJECTED PURCHASE REQUEST NOTE ('$serialNo'/'$year')";
			
			$_REQUEST['statement1']="Rejected this";
			$_REQUEST['statement2']="to view this";
			$_REQUEST['link']=base64_encode($_SESSION['MAIN_URL']."?q=917&prnNo=$serialNo&year=$year&approveMode=0"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
			$objMail->send_Response_Mail($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}
//--------------------------------------------------------
function sendFinlConfirmedEmailToEmailConfig($createdUser,$userId,$serialNo,$year,$objMail,$mainPat,$root_path){
	global $db;
	global $mainPath;
	global $companyId;
	$event=	'When PRN approved';	
		    $sql = "SELECT
					sys_mail_eventusers.intUserId,
					sys_users.strFullName,
					sys_users.strEmail
					FROM
					sys_mail_events
					Inner Join sys_mail_eventusers ON sys_mail_events.intMailIventId = sys_mail_eventusers.intMailEventId
					Inner Join sys_users ON sys_mail_eventusers.intUserId = sys_users.intUserId
					WHERE
					sys_mail_events.strMailEventName =  '$event' AND 
					sys_mail_eventusers.intCompanyId = '$companyId'";
				
		 $result = $db->RunQuery($sql);
		 
		while($row=mysqli_fetch_array($result)){
			if(($userId!=$row['intUserId']) && ($createdUser!=$row['intUserId'])){
			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="CONFIRMED PURCHASE REQUEST NOTE('$serialNo'/'$year')"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST = NULL;
			
			$_REQUEST['program']='PURCHASE REQUEST NOTE';
			$_REQUEST['field1']='PRN No';
			$_REQUEST['field2']='PRN Year';
			$_REQUEST['field3']='';
			$_REQUEST['field4']='';
			$_REQUEST['field5']='';
			$_REQUEST['value1']=$serialNo;
			$_REQUEST['value2']=$year;
			$_REQUEST['value3']='';
			$_REQUEST['value4']='';
			$_REQUEST['value5']='';
			
			$_REQUEST['subject']="CONFIRMED PURCHASE REQUEST NOTE('$serialNo'/'$year')";
			
			$_REQUEST['statement1']="Approved this";
			$_REQUEST['statement2']="to view this";
			$_REQUEST['link']=base64_encode($_SESSION['MAIN_URL']."?q=917&prnNo=$serialNo&year=$year&approveMode=0"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
			$objMail->send_Response_Mail($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);
			}
		}

}
//--------------------------------------------------------

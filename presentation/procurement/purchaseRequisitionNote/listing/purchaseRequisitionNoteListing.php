<?php
(define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');
include_once "libraries/jqgrid2/inc/jqgrid_dist.php";
//$menuId 		= '222';
//$reportMenuId 	= '917';

$q	= $_REQUEST['q'];

if($_REQUEST['q']==223){
	$menuId 		= '222';
	$title			= "Raw Materials";
	$programCode	='P0222';
	$reportMenuId 	= '917';
	$poType			='1';
}
else if($_REQUEST['q']==1143){
	$menuId 		= '1142';
	$title			= "Non-Raw Materials";
	$programCode	='P1142';
	$reportMenuId 	= '1144';
	$poType			='2';
}
else if($_REQUEST['q']==1147){
	$menuId 		= '1146';
	$title	= "RM and Non-RM";
	$programCode	='P1146';
	$reportMenuId 	= '1148';
	$poType			='3';
}

$company 		= $_SESSION['headCompanyId'];
$location 		= $_SESSION['CompanyID'];

$programName	='Purchase Request Note - '.$title;
$intUser  		= $_SESSION["userId"];

$approveLevel 	= (int)getMaxApproveLevel();


################################################################
################## new part ####################################
################################################################
///// create where part ///////////////////
$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];
//print_r($arr);
$where_string = '';
$where_array = array(
				'Status'=>'tb1.intStatus',
				'LOCATION'=>'mst_locations.strName',
				'PRN_No'=>'tb1.intPrnNo',
				'PRN_Year'=>'tb1.intYear',
				'Order_No'=>"(select  group_concat(intOrderNo separator ', ') from  (select distinct strStyleNo, intOrderNo,intOrderYear from trn_orderdetails ) as tb WHERE
							tb.intOrderNo =  tb1.intOrderNo AND
							tb.intOrderYear =  tb1.intOrderYear

							) ",
				'Graphic'=>"(select  group_concat(strGraphicNo separator ', ') from  (select distinct strGraphicNo, intOrderNo,intOrderYear from trn_orderdetails ) as tb WHERE
							tb.intOrderNo =  tb1.intOrderNo AND
							tb.intOrderYear =  tb1.intOrderYear

							)",
				'Style'=>"(select  group_concat(strStyleNo separator ', ') from  (select distinct strStyleNo, intOrderNo,intOrderYear from trn_orderdetails ) as tb WHERE
							tb.intOrderNo =  tb1.intOrderNo AND
							tb.intOrderYear =  tb1.intOrderYear

							)",
				'Request_Date'=>'tb1.dtmPrnDate',
				'Required_Date'=>'tb1.dtmRequiredDate',
				'Department'=>'mst_department.strName',
				'User'=>'sys_users.strUserName'
				);
$arr_status = array('Approved'=>'1','Rejected'=>'0','Pending'=>'2','Cancelled'=>'-10');
foreach($arr as $k=>$v)
{
	if($v['field']=='Status')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}
if(!count($arr)>0)					 
	$where_string .= "AND tb1.dtmPrnDate = '".date('Y-m-d')."'";
	
################## end code ####################################

//---------------------------------------
   $sql1 = "SELECT DISTINCT 
			L.intId,
			L.strName
			FROM mst_locations L 
			WHERE intStatus='1'			  
			ORDER BY
			L.strName ASC

	";
	$result1 = $db->RunQuery($sql1);
	$strCustomers='';
	$j=0;
	$strLocations = ":All;" ;
	while($row=mysqli_fetch_array($result1))
	{
		$strLocations .= $row['strName'].":".$row['strName'].";" ;
		$j++;
	}
	$strLocations = substr($strLocations,0,-1);
//---------------------------------------

$sql = "select * from(SELECT DISTINCT 
							if(tb1.intStatus=1,'Approved',if(tb1.intStatus=0,'Rejected',if(tb1.intStatus=-10,'Cancelled','Pending'))) as Status,
							tb1.intPrnNo as `PRN_No`, 
							tb1.intYear as `PRN_Year`,
							mst_locations.strName as LOCATION,
							
							(select  group_concat(distinct intOrderNo separator ', ') from  (select distinct strStyleNo, intOrderNo,intOrderYear from trn_orderdetails ) as tb WHERE
							tb.intOrderNo =  tb1.intOrderNo AND
							tb.intOrderYear =  tb1.intOrderYear

							) as `Order_No`,

							(select  group_concat(distinct DUMMY_PO_NO separator ', ') from  (select distinct  DUMMY_PO_NO,DUMMY_PO_YEAR,intOrderNo,intOrderYear from trn_orderheader ) as tb WHERE
							tb.intOrderNo =  tb1.intOrderNo AND
							tb.intOrderYear =  tb1.intOrderYear

							) as `Order_No_Dummy`,

							tb1.BULK_REVISION_NO as bulkRevisionNo,

							(select  group_concat(strStyleNo separator ', ') from  (select distinct strStyleNo, intOrderNo,intOrderYear from trn_orderdetails ) as tb WHERE
							tb.intOrderNo =  tb1.intOrderNo AND
							tb.intOrderYear =  tb1.intOrderYear

							) as `Style`,
							
							(select  group_concat(strGraphicNo separator ', ') from  (select distinct strGraphicNo, intOrderNo,intOrderYear from trn_orderdetails ) as tb WHERE
							tb.intOrderNo =  tb1.intOrderNo AND
							tb.intOrderYear =  tb1.intOrderYear

							) as `Graphic`, 
							
							IFNULL((SELECT
								Sum(trn_orderdetails.intQty) 
								FROM trn_orderdetails
								WHERE
								trn_orderdetails.intOrderNo = tb1.intOrderNo AND
								trn_orderdetails.intOrderYear =  tb1.intOrderYear AND trn_orderdetails.SO_TYPE > -1
							),0) as Order_Qty, 
							
							tb1.dtmPrnDate as `Request_Date`,
							tb1.dtmRequiredDate as `Required_Date`,
							mst_department.strName as `Department`,
					
							IFNULL((SELECT
							Sum(trn_prndetails.dblPrnQty)
							FROM trn_prndetails
							WHERE
							trn_prndetails.intPrnNo =  tb1.intPrnNo AND
							trn_prndetails.intYear =  tb1.intYear),0) as `PRN_Qty`,
							
						/*	IFNULL((SELECT
							Sum(trn_podetails.dblQty)
							FROM
							trn_podetails
							Inner Join trn_poheader ON trn_podetails.intPONo = trn_poheader.intPONo AND trn_podetails.intPOYear = trn_poheader.intPOYear
							WHERE
							trn_podetails.intPrnNo =  tb1.intPrnNo AND
							trn_podetails.intPrnYear =  tb1.intYear AND 
							trn_poheader.intStatus =  '1'),0) as `PO_Qty`,
							
						
							
							(IFNULL((SELECT
							Sum(trn_podetails.dblQty)
							FROM
							trn_podetails
							Inner Join trn_poheader ON trn_podetails.intPONo = trn_poheader.intPONo AND trn_podetails.intPOYear = trn_poheader.intPOYear
							WHERE
							trn_podetails.intPrnNo =  tb1.intPrnNo AND
							trn_podetails.intPrnYear =  tb1.intYear AND 
							trn_poheader.intStatus =  '1'),0)/IFNULL((SELECT
							Sum(trn_prndetails.dblPrnQty)
							FROM trn_prndetails
							WHERE
							trn_prndetails.intPrnNo =  tb1.intPrnNo AND
							trn_prndetails.intYear =  tb1.intYear),0))*100 as Progress, */
							
							sys_users.strUserName as `User`, 
							  
							IFNULL((
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(trn_prnheader_approvedby.dtApprovedDate),')' )
								FROM
								trn_prnheader_approvedby
								Inner Join sys_users ON trn_prnheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								trn_prnheader_approvedby.intPrnNo  = tb1.intPrnNo AND
								trn_prnheader_approvedby.intYear =  tb1.intYear AND
								trn_prnheader_approvedby.intApproveLevelNo =  '1' AND 
							    trn_prnheader_approvedby.intStatus='0'
							),IF(((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.intStatus>1),'Approve', '')) as `1st_Approval`,  ";
							
						for($i=2; $i<=$approveLevel; $i++){
							
							if($i==2){
							$approval="2nd_Approval";
							}
							else if($i==3){
							$approval="3rd_Approval";
							}
							else {
							$approval=$i."th_Approval";
							}
							
							
						$sql .= "IFNULL(
						/*condition*/
(
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(trn_prnheader_approvedby.dtApprovedDate),')' )
								FROM
								trn_prnheader_approvedby
								Inner Join sys_users ON trn_prnheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								trn_prnheader_approvedby.intPrnNo  = tb1.intPrnNo AND
								trn_prnheader_approvedby.intYear =  tb1.intYear AND
								trn_prnheader_approvedby.intApproveLevelNo =  '$i' AND 
							    trn_prnheader_approvedby.intStatus='0'
							),
							/*end condition*/
							
							/*false part*/

							IF(
							/*if condition */
							((SELECT
								menupermision.int".$i."Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND (tb1.intStatus>1) AND (tb1.intStatus<=tb1.intApproveLevels) AND ((SELECT
								max(trn_prnheader_approvedby.dtApprovedDate) 
								FROM
								trn_prnheader_approvedby
								Inner Join sys_users ON trn_prnheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								trn_prnheader_approvedby.intPrnNo  = tb1.intPrnNo AND
								trn_prnheader_approvedby.intYear =  tb1.intYear AND
								trn_prnheader_approvedby.intApproveLevelNo =  ($i-1) AND 
							    trn_prnheader_approvedby.intStatus='0')<>'')),
								
								/*end if condition*/
								/*if true part*/
								'Approve',
								/*fase part*/
								 if($i>tb1.intApproveLevels,'-----',''))
								
								/*end IFNULL false part*/
								) as `".$approval."`, "; 
									
								}
								
						$sql .= "'View' as `View`   
						FROM
							trn_prnheader as tb1
							Inner Join mst_department ON tb1.intDepartment = mst_department.intId 
							Inner Join sys_users ON tb1.intUser = sys_users.intUserId 
							Inner Join mst_locations ON tb1.intCompany = mst_locations.intId  
							WHERE 
							1=1 
							$where_string 
							/* AND tb1.PO_TYPE	='$poType' */
							AND FIND_IN_SET(tb1.PO_TYPE,'$poType')   
							/*mst_locations.intCompanyId =  '$company' AND 
							tb1.intCompany =  '$location'*/
							)  as t where 1=1
						";
					  	//echo $sql;
					   //	 echo $sql;
						 
$col = array();


$col["title"] 			= "Location";
$col["name"] 			= "LOCATION";
$col["width"] 			= "7";
$col["stype"] 			= "select";
$str 					= $strLocations ;
$col["editoptions"] 	= array("value"=> $str);
$cols[] 				= $col;	
$col					= NULL;

//STATUS
$col["title"] 	= "Status"; // caption of column
$col["name"] 	= "Status"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "2";
//edittype
$col["stype"] 	= "select";
$str = ":All;Pending:Pending;Approved:Approved;Rejected:Rejected;Cancelled:Cancelled" ;
$col["editoptions"] 	=  array("value"=> $str);
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$cols[] = $col;	$col=NULL;

//PO No
$col["title"] 	= "PRN No"; // caption of column
$col["name"] 	= "PRN_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "2";
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$col['link']	= "?q=$menuId&requestType=URLGetHeader&MenuId=$menuId&prnNo={PRN_No}&year={PRN_Year}";	 
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag

$reportLink  = "?q=$reportMenuId&prnNo={PRN_No}&year={PRN_Year}";
$reportLinkApprove  = "?q=$reportMenuId&prnNo={PRN_No}&year={PRN_Year}&approveMode=1";

$cols[] = $col;	$col=NULL;


//PO Year
$col["title"] = "PRN Year"; // caption of column
$col["name"] = "PRN_Year"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//PO Year
$col["title"] 	= "Order No"; // caption of column
$col["name"] 	= "Order_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "2";
$col["align"]	= "center";
$cols[] = $col;	$col=NULL;

//PO Year
$col["title"] 	= "Dummy Order No"; // caption of column
$col["name"] 	= "Order_No_Dummy"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "2";
$col["align"]	= "center";
$cols[] = $col;	$col=NULL;

//Bulk Revisiion Number
$col["title"] 	= "Bulk Revision No"; // caption of column
$col["name"] 	= "bulkRevisionNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] 	= "2";
$col["align"]	= "center";
$cols[] = $col;	$col=NULL;

//PO Year
$col["title"] = "Graphic"; // caption of column
$col["name"] = "Graphic"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//PO Year
$col["title"] = "Style"; // caption of column
$col["name"] = "Style"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Delivery Date
$col["title"] = "Order Qty"; // caption of column
$col["name"] = "Order_Qty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Delivery Date
$col["title"] = "PRN Date"; // caption of column
$col["name"] = "Request_Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Delivery Date
$col["title"] = "Required Date"; // caption of column
$col["name"] = "Required_Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;



//Delivery Date
$col["title"] = "Department"; // caption of column
$col["name"] = "Department"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//PO Qty
$col["title"] = "PRN Qty"; // caption of column
$col["name"] = "PRN_Qty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;


/*//GRN Qty
$col["title"] = "PO Qty"; // caption of column
$col["name"] = "PO_Qty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;*/


//GRN Qty
$col["title"] = "Progress"; // caption of column
$col["name"] = "Progress"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;


//User
$col["title"] = "User"; // caption of column
$col["name"] = "User"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;



//FIRST APPROVAL
$col["title"] = "1st Approval"; // caption of column
$col["name"] = "1st_Approval"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

for($i=2; $i<=$approveLevel; $i++){
	if($i==2){
	$ap="2nd Approval";
	$ap1="2nd_Approval";
	}
	else if($i==3){
	$ap="3rd Approval";
	$ap1="3rd_Approval";
	}
	else {
	$ap=$i."th Approval";
	$ap1=$i."th_Approval";
	}
//SECOND APPROVAL
$col["title"] = $ap; // caption of column
$col["name"] = $ap1; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;
}



//VIEW
$col["title"] = "Report"; // caption of column
$col["name"] = "View"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

$jq = new jqgrid('',$db);

$grid["caption"] 		= "Purchase Request Note Listing - ".$title;
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'PRN_No'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes


$grid["search"] = true; 

// export XLS file
// export to excel parameters - range could be "all" or "filtered"
$grid["export"] = array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);




$out = $jq->render("list1");

//include "include/listing.html";
?>
<head>
<title>Purchase Request Note Listing - <?php echo $title; ?></title>
</head>
<body>	
<form id="frmlisting" name="frmlisting" method="post" action="">
    <td><table width="100%" border="0">
      <tr>
        <td>
        <div align="center" style="margin:10px">
			<?php echo $out?>
        </div>
        </td>
      </tr>

    </table></td>
    </tr>


</form>
</body>
</html>
<?php

//------------------------------function load Default Department---------------------
function getMaxApproveLevel(){
	global $db;
	
	//echo $savedStat;
	$appLevel=0;
	$sqlp = "SELECT
			Max(trn_prnheader.intApproveLevels) AS appLevel
			FROM trn_prnheader";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['appLevel'];
}
?>

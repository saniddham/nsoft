<?php
(define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');
$companyId 	= $_SESSION['CompanyID'];
$locationId = $companyId;//this is use in report header(reportHeader.php)--------------------
$intUser  	= $_SESSION["userId"];

$orderNo		= $_REQUEST['orderNo'];
$orderYear		= $_REQUEST['orderYear'];
$graphic		= $_REQUEST['graphic'];
$approveMode 	= $_REQUEST['approveMode'];
$programName	='Production Required Note';

 	
 ?>
<head>
<title>Production Required Report  </title>
<script type="application/javascript" src="presentation/procurement/purchaseRequisitionNote/listing/rptPurchaseRequisitionNote-js.js"></script>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position: absolute;
	left: 234px;
	top: 301px;
	width: 650px;
	height: 322px;
	z-index: 1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<?php
 if($intStatus>1)//pending
{
?>
<div id="apDiv1">	</div>
<?php
}
?>
<form id="frmPurchaseRequisitionNoteReport" name="frmPurchaseRequisitionNoteReport" method="post" >
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include 'reportHeader.php'?></td>
<td width="20%"></td>
</tr>
<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>PRODUCTION REQUIRED ITEMS   <?php echo strtoupper ($title); ?></strong><strong></strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
 <tr>
  <td width="1%">&nbsp;</td>
  <td width="12%" class="normalfnt"><strong>Order No</strong></td>
  <td width="2%" align="center" valign="middle"><strong>:</strong></td>
  <td width="25%"><span class="normalfnt"><?php echo $orderYear.'/'.$orderNo ?></span></td>
  <td width="13%"><span class="normalfnt"><strong> Graphic No</strong></span></td>
  <td width="1%" align="center" valign="middle"><strong>:</strong></td>
  <td width="18%"><span class="normalfnt"><?php echo $graphic ?></span></td>
  <td width="20%" align="center" valign="middle"><span class="normalfnt"><strong>Order Qty for Graphic</strong></span></td>
  <td width="1%" align="center" valign="middle"><strong>:</strong></td>
  <td width="5%" align="center" valign="middle"><span class="normalfnt">
  			<?php
					$sql="SELECT
							sum(trn_orderdetails.intQty) as qty
							FROM
							trn_orderdetails
						  WHERE
							trn_orderdetails.intOrderNo = '$orderNo' AND
							trn_orderdetails.intOrderYear = '$orderYear' AND
							trn_orderdetails.strGraphicNo = '$graphic' AND  trn_orderdetails.SO_TYPE > -1
  					";
					$result 	= $db->RunQuery($sql);
					while($row	=mysqli_fetch_array($result))
					
					echo $row['qty'];
			?></span>
              
  </td>
</tr>
 </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td>
          <table width="100%" class="grid" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr class="gridHeader">
              <td width="10%">Order Qty</td>
              <td width="10%">Sales Order</td>
              <td width="10%">Combo</td>
              <td width="10%">Part</td>
              <td width="10%">Print</td>
              <td width="10%" >Main Category</td>
              <td width="10%" >Sub Category</td>
              <td width="7%" >Code</td>
              <td width="20%" >Item</td>
              <td width="5%" >UOM</td>
              <td width="8%" >Required</td>
              </tr>
            <?php 
				$sql1		="  SELECT
									Sum(trn_orderdetails.intQty) As order_qty,
									trn_po_prn_details_sales_order.ORDER_NO,
									trn_po_prn_details_sales_order.ORDER_YEAR,
									trn_orderdetails.strGraphicNo,
									Sum(trn_po_prn_details_sales_order.REQUIRED) AS required_qty,
									trn_po_prn_details_sales_order.ITEM,
									mst_maincategory.strName AS mainCat,
                                                                        mst_maincategory.intId AS mainCatId,
									mst_subcategory.strName AS subCat,
									mst_item.strCode AS itemCode,
									mst_units.strCode AS unit,
									mst_item.strName AS item_name,
                                                                        mst_item.ITEM_HIDE,
                                                                        mst_item.strCode as SUP_ITEM_CODE,
									trn_orderdetails.strSalesOrderNo,
									trn_orderdetails.strCombo,
									trn_orderdetails.strPrintName,
									mst_part.strName as part
								FROM
									trn_po_prn_details_sales_order
									INNER JOIN trn_orderdetails ON trn_po_prn_details_sales_order.ORDER_NO = trn_orderdetails.intOrderNo AND trn_po_prn_details_sales_order.ORDER_YEAR = trn_orderdetails.intOrderYear AND trn_po_prn_details_sales_order.SALES_ORDER = trn_orderdetails.intSalesOrderId
									INNER JOIN mst_item ON trn_po_prn_details_sales_order.ITEM = mst_item.intId
									INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
									INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
									INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
									INNER JOIN mst_part ON trn_orderdetails.intPart = mst_part.intId
                                                                        LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier =  
                                                                        mst_supplier.intId
								WHERE	
									trn_po_prn_details_sales_order.ORDER_NO = '$orderNo' AND
									trn_po_prn_details_sales_order.ORDER_YEAR = '$orderYear' AND
									trn_orderdetails.strGraphicNo = '$graphic' AND
									trn_po_prn_details_sales_order.REQUIRED > 0 
								GROUP BY
									trn_po_prn_details_sales_order.ORDER_NO,
									trn_po_prn_details_sales_order.ORDER_YEAR,
									trn_orderdetails.strGraphicNo,
									trn_po_prn_details_sales_order.ITEM,
									trn_orderdetails.intSalesOrderId 
			
			    				ORDER BY
									trn_po_prn_details_sales_order.ORDER_NO ASC,
									trn_po_prn_details_sales_order.ORDER_YEAR ASC,
									trn_orderdetails.strSalesOrderNo ASC,
									trn_orderdetails.strGraphicNo ASC,
									trn_orderdetails.strCombo ASC,
									trn_orderdetails.strPrintName ASC ,
									mst_item.strName ASC 

						 ";
 			$result1 	= $db->RunQuery($sql1);
			while($row	=mysqli_fetch_array($result1))
			{
	  ?>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfntRight" nowrap ><?php echo $row['order_qty'] ?>&nbsp;</td>
              <td class="normalfnt" nowrap ><?php echo $row['strSalesOrderNo'] ?></td>
              <td class="normalfnt" nowrap ><?php echo $row['strCombo'] ?></td>
              <td class="normalfnt" nowrap ><?php echo $row['part'] ?></td>
              <td class="normalfnt" nowrap ><?php echo $row['strPrintName']  ?></td>
              <td class="normalfnt" nowrap >&nbsp;<?php echo $row['mainCat'] ?>&nbsp;</td>
              <td class="normalfnt" nowrap >&nbsp;<?php echo $row['subCat']?>&nbsp;</td>
              <?php
              if($row['SUP_ITEM_CODE']!=null)
              {
              ?>
              <td class="normalfnt" nowrap >&nbsp;<?php echo $row['SUP_ITEM_CODE'] ?>&nbsp;</td>
              <?php
              }else
              {
              ?>
              <td class="normalfnt" nowrap >&nbsp;<?php echo $row['itemCode'] ?>&nbsp;</td>
              <?php
              }
              if($row['ITEM_HIDE']==1)
              {
                  ?>
               <td class="normalfnt" nowrap >&nbsp;*****&nbsp;</td>
              <?php
              }else{
                  
              ?>
              <td class="normalfnt" nowrap >&nbsp;<?php echo $row['item_name'] ?>&nbsp;</td>
              <?php
              }
              ?>
              <td class="normalfnt" nowrap >&nbsp;<?php echo $row['unit'] ?>&nbsp;</td>
              <td class="normalfntRight" nowrap>&nbsp;<?php echo number_format($row['required_qty'],8) ?>&nbsp;</td>
              </tr>
            <?php        
			}
	  ?>
            
          </table>        </td>
        </tr>
      
      </table>
    </td>
</tr>
 
<tr height="110">
            <td bgcolor="#FFFFFF"></td>
      </tr>
<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
</body>
</html>
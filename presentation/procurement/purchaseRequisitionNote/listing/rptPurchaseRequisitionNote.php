<?php
(define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');
$companyId 	= $_SESSION['CompanyID'];
$intUser  	= $_SESSION["userId"];

$prnNo 			= $_REQUEST['prnNo'];
$year 			= $_REQUEST['year'];
$q	 			= $_REQUEST['q'];
$approveMode 	= $_REQUEST['approveMode'];
$programName	='Purchase Request Note';



$sql = "SELECT
trn_prnheader.intPrnNo,
trn_prnheader.intYear,
trn_prnheader.intDepartment,
trn_prnheader.intInternalUsage,
trn_prnheader.dtmRequiredDate,
trn_prnheader.dtmPrnDate,
trn_prnheader.strRemarks,
mst_department.intId,
trn_prnheader.intStatus, 
trn_prnheader.intApproveLevels,
trn_prnheader.intUser,
trn_prnheader.intCompany as PRNRaisedLocationId, 
mst_department.strName,
sys_users.strUserName as prnBy ,
trn_prnheader.PO_TYPE ,
trn_prnheader.SYSTEM_CANCELLATION ,
trn_prnheader.SYSTEM_GENERATED ,
trn_prnheader.intOrderNo, 
trn_prnheader.intOrderYear ,
trn_prnheader.BULK_PO_QTY ,
trn_prnheader.BULK_PO_QTY_RCV_ABLE 
FROM
trn_prnheader
Inner Join mst_department ON trn_prnheader.intDepartment = mst_department.intId 
Inner Join sys_users ON trn_prnheader.intUser = sys_users.intUserId 
WHERE
trn_prnheader.intPrnNo =  '$prnNo' AND
trn_prnheader.intYear =  '$year'

";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$department = $row['strName'];
					$prnDate = $row['dtmPrnDate'];
					$requiredDate = $row['dtmRequiredDate'];
					$intInternalUsage = $row['intInternalUsage'];
					if($intInternalUsage==1)
					$InternalUsage="Yes";
					else
					$InternalUsage="No";
					$remarks1=	$row['strRemarks'];				
					
					$intStatus = $row['intStatus'];
					$savedLevels = $row['intApproveLevels'];
					$prnBy = $row['prnBy'];
					$intCreatedUser = $row['intUser'];
					$confirmBy = $row['intConfirmedBy'];
					$confirmDate = $row['intConfirmedDate'];
					$locationId = $row['PRNRaisedLocationId'];//this is use in report header(reportHeader.php)--------------------
					$q=$row['PO_TYPE'];
					if($q==1){
						$title	= "Raw Materials";
						$programCode='P0222';
					}
					else if($q==2){
						$title	= "Non-Raw Materials";
						$programCode='P1142';
					}
					else if($q==3){
						$title	= "RM and Non-RM";
						$programCode='P1146';
					}
					
					$system_cancel=$row['SYSTEM_CANCELLATION'];
					
					$flag_sys_generated	= $row['SYSTEM_GENERATED'];
					if($flag_sys_generated==1)
						$sys_generated	= ' - SYSTEM GENERATED PRN';
					else
						$sys_generated	= '';
						
					$orderNo	=$row['intOrderNo'];
					$orderYear	=$row['intOrderYear'];
					$order_no_str	= $row['intOrderNo'].'/'.$row['intOrderYear'];
			
					$bulk_qty			= $row['BULK_PO_QTY'];
					$bulk_po_rcv_able	= $row['BULK_PO_QTY_RCV_ABLE'];
					
					$cancelledPRNs		= getCancelledPRNList($orderNo,$orderYear);
					$approvedPRNs		= getApprovedPRNList($orderNo,$orderYear);
				 }

//----------------------------------------------------------------------------------
	 $sql = "SELECT
			GROUP_CONCAT(trn_orderdetails.strGraphicNo) as str_strGraphicNo ,
			trn_orderheader.PO_TYPE,
			trn_orderheader.strRemark 
			FROM
			trn_orderdetails
			INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
			WHERE
			trn_orderdetails.intOrderNo = '$orderNo' AND
			trn_orderdetails.intOrderYear = '$orderYear'
			";
	$result = $db->RunQuery($sql);
	$row=mysqli_fetch_array($result);
	$graphic		= $row['str_strGraphicNo'];
	$bulkRemarks	= $row['strRemark'];
//-----------------------------------------------------------------------------------
	
	
	
	
				 
$confirmationMode=loadConfirmatonMode($programCode,$intStatus,$savedLevels,$intUser);
$rejectionMode=loadRejectionMode($programCode,$intStatus,$savedLevels,$intUser);
?>
<head>
<title>Purchase Request Report - <?php echo $title; ?></title>
<script type="application/javascript" src="presentation/procurement/purchaseRequisitionNote/listing/rptPurchaseRequisitionNote-js.js"></script>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:219px;
	top:173px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<?php
 if($intStatus>1)//pending
{
?>
<div id="apDiv1"><img src="images/pending.png"  /></div>
<?php
}
?>
<form id="frmPurchaseRequisitionNoteReport" name="frmPurchaseRequisitionNoteReport" method="post" >
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include 'reportHeader.php'?></td>
<td width="20%"></td>
</tr>
<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>PURCHASE REQUEST NOTE  - <?php echo strtoupper ($title); ?></strong><strong></strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
  <tr>
    <td colspan="9" align="center" bgcolor="#FFDFCB">
    <?php
	if($intStatus>1)
	{
	?>
    <?php if($approveMode==1){
			if($confirmationMode==1) { 
			?>
			<img src="images/approve.png" align="middle" class="mouseover noPrint" id="imgApprove" />
			<?php
			}
			if($rejectionMode==1) { 
			?>
            <img src="images/reject.png" align="middle" class="mouseover noPrint" id="imgReject" />
                <?php
                }
            }
	}
	?>
    </td>
  </tr>
  <tr>
  <?php
 	if($intStatus==1)
	{
	?>
   <td colspan="9" class="APPROVE">CONFIRMED <div style="color:#F00"><?php echo $sys_generated; ?></div></td>
   <?PHP
	}
	else if($intStatus==0)
	{
   ?>
   <td colspan="9" class="APPROVE" style="color:#F00">REJECTED</td>
   <?php
	}
	else if($intStatus==-10)
	{
   ?>
   <td colspan="9" class="CANCELLED" style="color:#F00">CANCELLED <?php if($system_cancel==1){ echo "BY SYSTEM" ;}?></td>
   <?php
	}
	else
	{
   ?>
   <td width="10%" colspan="9" class="APPROVE">PENDING</td>
   <?php
	}
   ?>
  </tr>
  <tr>
    <td width="1%">&nbsp;</td>
    <td width="12%"><span class="normalfnt"><strong>PRN No</strong></span></td>
    <td width="2%" align="center" valign="middle"><strong>:</strong></td>
    <td width="25%"><span class="normalfnt"><?php echo $prnNo ?>/<?php echo $year ?></span></td>
    <td width="13%" class="normalfnt"><strong>Required Date</strong></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="18%"><span class="normalfnt"><?php echo $requiredDate ?></span></td>
    <td width="1%"></td>
  <td width="5%"><div id="divNo" style="display:none"><?php echo $prnNo ?>/<?php echo $year ?></div></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Department</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $department ?></span></td>
    <td><span class="normalfnt"><strong>PRN Date</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $prnDate ?></span></td>
    <td class="normalfnt">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Internal Use</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $InternalUsage ?></span></td>
    <td><span class="normalfnt"><strong> PRN By</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $prnBy ?></span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <?php
  if($flag_sys_generated==1){
  ?>
<tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Order No</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $order_no_str ?></span></td>
    <td><span class="normalfnt"><strong> Graphic Nos</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $graphic ?></span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
<tr>
    <td>&nbsp;</td>
    <td class="normalfnt" valign="top"><strong>Cancelled/ Approved PRNs</strong></td>
    <td align="center"  valign="top"><strong>:</strong></td>
    <td colspan="6" valign="top"><table width="100%">
    <tr>
    <td bgcolor="#FFFFFF" valign="top"><textarea cols="90" rows="1" class="textarea" style="width::300px; font-weight: bold;" disabled="disabled">Cancelled :  (<?php echo $cancelledPRNs  ?>)  / Approved :  (<?php echo $approvedPRNs  ?>)</textarea></td>
    </tr>
                </table></td>
            </tr>
     <tr>
    <td>&nbsp;</td>
    <td class="normalfnt" valign="top"><strong>Bulk Order Remarks</strong></td>
    <td align="center"  valign="top"><strong>:</strong></td>
    <td colspan="6" valign="top"><table width="100%">
    <tr>
    <td bgcolor="#FFFFFF" valign="top"><span class="normalfnt"><textarea cols="90" rows="3" class="textarea" style="width::300px" disabled="disabled"><?php echo $bulkRemarks  ?> / (Customer Order Qty/Customer Order Receivable Qty : <?php echo $bulk_qty."/".$bulk_po_rcv_able ; ?>)</textarea></span></td>
    </tr>
                </table></td>
            </tr>            
  <?php
  }
  ?>
  
  
  
  
<tr>
<td>&nbsp;</td>
<td class="normalfnt" valign="top"><strong>Remarks</strong></td>
<td align="center"  valign="top"><strong>:</strong></td>
<td colspan="6" valign="top"><table width="100%">
  <tr>
    <td bgcolor="#FFFFFF" valign="top"><span class="normalfnt"><textarea cols="90" rows="3" class="textarea" style="width::300px" disabled="disabled"><?php echo $remarks1  ?></textarea></span></td>
    </tr>
                </table></td>
            </tr>            
  </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td>
          <table width="100%" class="grid" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr class="gridHeader">
            <?php
			if($q==1){
			?>
              <td width="10%" height="22">Order No</td>
              <td width="12%" height="22" style="display:none">Sales Order No</td>
			  <td width="8%" height="22">Bulk Revision No</td>
            <?php
			}
			?>
              <td width="16%" height="22">Main Category</td>
              <td width="14%" >Sub Category</td>
              <td width="8%" >UOM</td>
              <td width="22%" >Item Code</td>
              <td width="22%" >Item</td>
              <td width="10%" >PRN Qty</td>
              <td width="8%" >PO Qty</td>
              </tr>
            <?php 
	   	 $sql1 =/* "
				SELECT
					trn_prndetails.intOrderNo,
					trn_prndetails.intOrderYear,
					trn_prndetails.intSalesOrderId,
					trn_prndetails.intItem,
					mst_item.strCode,
					mst_item.strName,
					mst_maincategory.strName AS mainCategory,
					mst_subcategory.strName AS subCategory,
					trn_prndetails.dblPrnQty,
					trn_orderdetails.strSalesOrderNo
				FROM
				trn_prndetails
					Inner Join mst_item ON trn_prndetails.intItem = mst_item.intId
					Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
					Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
					Inner Join trn_orderdetails ON trn_orderdetails.intOrderNo = trn_prndetails.intOrderNo AND trn_orderdetails.intOrderYear = trn_prndetails.intOrderYear AND trn_orderdetails.intSalesOrderId = trn_prndetails.intSalesOrderId
				WHERE trn_prndetails.intPrnNo = '$prnNo' AND trn_prndetails.intYear = '$year'
					Order by mst_maincategory.strName ASC, mst_subcategory.strName ASC, mst_item.strName ASC


";*/
"SELECT
trn_prndetails.intOrderNo,
trn_prndetails.intOrderYear,
trn_prndetails.strSalesOrderNo,
trn_prnheader.BULK_REVISION_NO as bulkRevisionNo,
mst_item.strName AS itemName,
mst_maincategory.strName AS mainCat,
mst_subcategory.strName AS subCat,
trn_prndetails.dblPrnQty,
trn_prndetails.dblPoQty,
mst_units.strName AS unitName,
mst_item.strCode as supItemCode,
mst_item.strCode as ItemCode,
mst_item.ITEM_HIDE,
/*(
	SELECT DISTINCT 
trn_orderdetails.strSalesOrderNo
FROM
trn_orderdetails
WHERE
trn_orderdetails.intOrderNo =  trn_prndetails.intOrderNo AND
trn_orderdetails.intOrderYear =  trn_prndetails.intOrderYear AND
trn_orderdetails.strSalesOrderNo =  trn_prndetails.strSalesOrderNo

) as salesNo1,*/

trn_prndetails.strSalesOrderNo as salesNo 
FROM
trn_prndetails
Inner Join trn_prnheader ON trn_prndetails.intPrnNo = trn_prnheader.intPrnNo AND trn_prndetails.intYear = trn_prnheader.intYear
Inner Join mst_item ON mst_item.intId = trn_prndetails.intItem
Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
Inner Join mst_units ON mst_units.intId = mst_item.intUOM
LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
WHERE
trn_prndetails.intPrnNo =  '$prnNo' AND
trn_prndetails.intYear =  '$year' 
Order by  
mst_maincategory.strName asc, 
mst_subcategory.strName asc, 
mst_item.strName asc
";
			$result1 = $db->RunQuery($sql1);
			while($row=mysqli_fetch_array($result1))
			{
                            $itemCode=$row['ItemCode'];
                            $supItemCode=$row['supItemCode'];
                           
	  ?>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
            <?php
			if($q==1){
			?>
              <td class="normalfnt" >&nbsp;<?php echo ($row['intOrderNo']>0?$row['intOrderNo']."/".$row['intOrderYear']:''); ?>&nbsp;</td>
              <td class="normalfnt"  style="display:none" >&nbsp;<?php echo $row['salesNo'] ?>&nbsp;</td>
			  <td class="normalfnt" >&nbsp;<?php echo $row['bulkRevisionNo'] ?>&nbsp;</td>
            <?php
			}
			?>
              <td class="normalfnt" >&nbsp;<?php echo $row['mainCat'] ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['subCat']?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['unitName'] ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php if($supItemCode!=null){echo $supItemCode;}else{echo $itemCode;}  ?>&nbsp;</td>
              <?php
                  if($row['ITEM_HIDE']==1 && $q==1 || $row['ITEM_HIDE']==1 && $q==2 || $row['ITEM_HIDE']==1 && $q==3)
                  {
                  ?>
                <td class="normalfnt" nowrap >&nbsp;*****&nbsp;</td>
            <?php
            }else{

                ?>
                <td class="normalfnt" nowrap >&nbsp;<?php echo $row['itemName'] ?>&nbsp;</td>
                <?php
            }
            ?>
                <td class="normalfntRight" >&nbsp;<?php echo number_format($row['dblPrnQty'],6); ?>&nbsp;</td>
                <td class="normalfntRight" >&nbsp;<?php echo $row['dblPoQty'] ?>&nbsp;</td>
            </tr>
              <?php

              }
	  ?>
            
          </table>        </td>
        </tr>
      
      </table>
    </td>
</tr>
<?php 
if($flag_sys_generated!=1){ 
 //	if($intStatus!=0)
//	{
					 $flag=0;
					 $sqlc = "SELECT
							trn_prnheader_approvedby.intApproveUser,
							trn_prnheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName,
							trn_prnheader_approvedby.intApproveLevelNo
							FROM
							trn_prnheader_approvedby
							Inner Join sys_users ON trn_prnheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							trn_prnheader_approvedby.intPrnNo =  '$prnNo' AND
							trn_prnheader_approvedby.intYear =  '$year'   order by dtApprovedDate asc
";
					 $resultc = $db->RunQuery($sqlc);
					while($rowc=mysqli_fetch_array($resultc)){
						if($rowc['intApproveLevelNo']==1)
						$desc="1st ";
						else if($rowc['intApproveLevelNo']==2)
						$desc="2nd ";
						else if($rowc['intApproveLevelNo']==3)
						$desc="3rd ";
						else
						$desc=$rowc['intApproveLevelNo']."th ";
					 //  $desc=$ap.$desc;
					 $desc2=$rowc['UserName']."(".$rowc['dtApprovedDate'].")";
					 if($rowc['UserName']=='')
					 $desc2='---------------------------------';
					 
					 if($rowc['intApproveLevelNo']>0){
						 $flag=1;
				?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> Approved By - </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
            </tr>
<?php
	//}
	//else{			
					}
					 if($rowc['intApproveLevelNo']==0){
						 $flag=2;
					  ?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong> Rejected By - </strong></span><span class="normalfnt"><?php echo $rowc['UserName']."(".$rowc['dtApprovedDate'].")";?></span></td>
            </tr>
<?php
					 }
					 if($rowc['intApproveLevelNo']==-1){
						 $flag=3;
					  ?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong> Revised By - </strong></span><span class="normalfnt"><?php echo $rowc['UserName']."(".$rowc['dtApprovedDate'].")";?></span></td>
            </tr>
<?php
					 }
					 
		 if($rowc['intApproveLevelNo']>0){
			 $finalSavedLevel=$rowc['intApproveLevelNo'];
		 }
	 }//end of while
	 
	 
	if($flag>1){
		$finalSavedLevel=0;
	}
	
	if(($flag<=1) || ($intStatus>0)){
	for($j=$finalSavedLevel+1; $j<=$savedLevels; $j++)
	{ 
		if($j==1)
		$desc="1st ";
		else if($j==2)
		$desc="2nd ";
		else if($j==3)
		$desc="3rd ";
		else
		$desc=$j."th ";
		$desc2='---------------------------------';
	?>
        <tr>
            <td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> Approved By - </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
        </tr>
<?php
	}
	}
	
}
?>

<tr height="110">
            <td bgcolor="#FFFFFF"></td>
      </tr>
<tr height="40">
<?php
	//$locationId = '';
	$sql = "SELECT mst_locations.intCompanyId FROM mst_locations WHERE mst_locations.intId =  '$locationId'";
	$result 	= $db->RunQuery($sql);
	$row		= mysqli_fetch_array($result);
	$createCompanyId = $row['intCompanyId'];
?>
<?Php 
	$url  = "{$backwardseperator}presentation/sendToApproval.php";		// * file name
	$url .= "?status=$intStatus";										// * set recent status
	$url .= "&approveLevels=$savedLevels";								// * set approve levels in order header
	$url .= "&programCode=$programCode";								// * program code (ex:P10001)
	$url .= "&program=PURCHASE REQUEST NOTE";									// * program name (ex:Purchase Order)
	$url .= "&companyId=$createCompanyId";									// * created company id
	$url .= "&createUserId=$intCreatedUser";	
	
	$url .= "&field1=PRN No";												 
	$url .= "&field2=PRN Year";	
	$url .= "&value1=$prnNo";												 
	$url .= "&value2=$year";	
	
	$url .= "&subject=PURCHASE REQUEST NOTE FOR APPROVAL ('$prnNo'/'$year')";	
	
	$url .= "&statement1=Please Approve this";	
	$url .= "&statement2=to Approve this";	
							// * doc year
	$url .= "&link=".urlencode(base64_encode($_SESSION['MAIN_URL']."?q=917&prnNo=$prnNo&year=$year&approveMode=1"));
?>
  <td align="center" class="normalfntMid"><iframe id="iframeFiles2" src="<?php echo $url;?>" name="iframeFiles" style="width:500px;height:150px;border:none"  ></iframe></td>
</tr>
<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
</body>
</html>
<?php
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	$sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
//------------------------------function loadRejectionMode-------------------
function loadRejectionMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$rejectMode=0;
	$sqlp = "SELECT
		menupermision.intReject 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intReject']==1){
	 if($intStatus!=0){
	 $rejectMode=1;
	 }
	}
	 
	return $rejectMode;
}

function getCancelledPRNList($orderNo,$orderYear){
	global $db;
	
	$sql	="SELECT
				GROUP_CONCAT(trn_prnheader.intPrnNo,'/',trn_prnheader.intYear) as prn_list 
				FROM `trn_prnheader`
				WHERE
				trn_prnheader.SYSTEM_CANCELLATION = 1 AND
				trn_prnheader.intOrderNo = '$orderNo' AND
				trn_prnheader.intOrderYear = '$orderYear' AND
				trn_prnheader.intStatus = -10";
	$result = $db->RunQuery($sql);
	$row=mysqli_fetch_array($result);
	return $row['prn_list'];	
}

function getApprovedPRNList($orderNo,$orderYear){
	global $db;
	
	$sql	="SELECT
				GROUP_CONCAT(trn_prnheader.intPrnNo,'/',trn_prnheader.intYear) as prn_list 
				FROM `trn_prnheader`
				WHERE
 				trn_prnheader.intOrderNo = '$orderNo' AND
				trn_prnheader.intOrderYear = '$orderYear' AND
				trn_prnheader.intStatus = 1";
	$result = $db->RunQuery($sql);
	$row=mysqli_fetch_array($result);
	return $row['prn_list'];	
}

//-------------------------------------------------------------------------
?>	

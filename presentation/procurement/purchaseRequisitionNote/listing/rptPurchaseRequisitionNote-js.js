// JavaScript Document
var basePath = "presentation/procurement/purchaseRequisitionNote/listing/";
$(document).ready(function() {
	$('#frmPurchaseRequisitionNoteReport #imgApprove').click(function(){
	var val = $.prompt('Are you sure you want to approve this PRN ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										if(validateQuantities()==0){
										var url = basePath +"rptPurchaseRequisitionNote-db-set.php"+window.location.search+'&status=approve';
										var obj = $.ajax({url:url,async:false});
										window.location.href = window.location.href;
										//window.opener.location.reload();//reload listing page
										//$('txtApproveStatus').val('1');
										//document.getElementById('txtApproveStatus').value = 1;
										//document.getElementById('frmSampleReport').submit();
									 }
									}
								}});
	});
	
	$('#frmPurchaseRequisitionNoteReport #imgReject').click(function(){
	var val = $.prompt('Are you sure you want to reject this PRN ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										if(validateRejecton()==0){
										var url = basePath +"rptPurchaseRequisitionNote-db-set.php"+window.location.search+'&status=reject';
										var obj = $.ajax({url:url,async:false});
										window.location.href = window.location.href;
										window.opener.location.reload();//reload listing page
										//document.getElementById('txtApproveStatus').value = 0;
										//document.getElementById('frmSampleReport').submit();
										}
									}
								}});
	});
	
/*	$('#frmPRNlisting #cboPrnStatus').change(function(){
					//	var url = "rptPurchaseRequisitionNote-db-set.php"+window.location.search+'&status=reject';
					//	var url = "purchaseRequisitionNoteListing.php"+window.location.search+'?cboPrnStatus='+$('#cboPrnStatus').val();
					//		var obj = $.ajax({url:url,async:false});
						//	var val=$('#cboPrnStatus').val();
							//alert(val);
						//	window.location.href = window.location.href;
						//	$('#cboPrnStatus').val(val);
						//	alert($('#cboPrnStatus').val());
						$("frmPRNlisting").submit();
	});
*/	
});

//---------------------------------------------------
function validateQuantities(){
		var ret=0;	
	    var serial = document.getElementById('divNo').innerHTML;
		var url 		= basePath +"rptPurchaseRequisitionNote-db-get.php?requestType=getValidation";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:"serial="+serial,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#imgApprove').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertx()",1000);
					  ret= 1;
					}
				}
			});
			return ret;
}
//---------------------------------------------------
function validateRejecton(){
		var ret=0;	
	    var serial = document.getElementById('divNo').innerHTML;
		var url 		= basePath +"rptPurchaseRequisitionNote-db-get.php?requestType=validateRejecton";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:"serial="+serial,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#imgApprove').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertx()",1000);
					  ret= 1;
					}
				}
			});
			return ret;
}



<?php
$backwardseperator = "../../../";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>PRN Pogress</title>

<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />


</head>

<body>
<form id="frmCompanyDetails" name="frmCompanyDetails" method="post" action="">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">PRN
		    
Progress</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableBorder_allRound">
          <tr>
            <td width="10%" height="22" class="normalfnt">&nbsp;</td>
            <td width="22%">&nbsp;</td>
            <td width="17%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="11%" class="normalfnt">&nbsp;</td>
            <td width="15%">&nbsp;</td>
            </tr>
          <tr>
            <td height="22" class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td><div style="width:900px;height:300px;overflow:scroll" >
          <table width="100%" class="grid" >
            <tr class="gridHeader">
              <td width="5%" height="21" >PRN No</td>
              <td width="10%" >PRN Type</td>
              <td width="12%" >Requested By</td>
              <td width="12%">Department</td>
              <td width="9%">Request Date</td>
              <td width="11%">Approval Status</td>
              <td width="8%">Qty</td>
              <td width="9%"> Received Qty</td>
              <td width="9%">Pogress(%)</td>
              <td width="7%"> Report</td>
              </tr>
              <?php  
			  $sql1="SELECT
					trn_prnheader.intPrnNo,
					trn_prnheader.intYear,
					trn_prnheader.intUser,
					trn_prnheader.intDepartment,
					trn_prnheader.dtmPrnDate,
					trn_prnheader.intStatus,
					sum(trn_prndetails.dblPrnQty) as dblPrnQty,
					sum(trn_prndetails.dblPoQty) as dblPoQty,
					mst_department.strName,
					sys_users.strUserName
					FROM
					trn_prnheader
					Inner Join trn_prndetails ON trn_prnheader.intPrnNo = trn_prndetails.intPrnNo AND trn_prnheader.intYear = trn_prndetails.intYear
					Inner Join mst_department ON trn_prnheader.intDepartment = mst_department.intId
					Inner Join sys_users ON trn_prnheader.intUser = sys_users.intUserId 
					group by trn_prnheader.intPrnNo,trn_prnheader.intYear 
					";
			$result1 = $db->RunQuery($sql1);
			$totQty=0;
			$totAmmount=0;
			while($row=mysqli_fetch_array($result1))
			{
			  
			  ?>
            <tr class="normalfnt">
              <td align="center" bgcolor="#FFFFFF"><?php echo $row['intPrnNo']."/".$row['intYear']?></td>
              <td bgcolor="#FFFFFF">&nbsp;</td>
              <td bgcolor="#FFFFFF"><?php echo $row['strUserName']?></td>
              <td bgcolor="#FFFFFF"><?php echo $row['strName']?></td>
              <td bgcolor="#FFFFFF"><?php echo $row['dtmPrnDate']?></td>
              <td bgcolor="#FFFFFF"><?php echo $row['intStatus']?></td>
              <td bgcolor="#FFFFFF"><?php echo $row['dblPrnQty']?></td>
              <td bgcolor="#FFFFFF"><?php echo $row['dblPoQty']?></td>
              <td bgcolor="#FFFFFF"><?php echo $row['dblPoQty']/$row['dblPrnQty']*100?></td>
              <td bgcolor="#FFFFFF"><a href="listing/rptPurchaseRequisitionNote.php?prnNo=<?php echo $row['intPrnNo']?>&year=<?php echo $row['intYear']?>&approveMode=0" target="_blank"><img src="../../../images/view.png" width="91" height="19" /></a></td>
              </tr>
              <?php
			}
			  ?>
            </table>
          </div></td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound"><img src="../../../../planning/img/Tclose.jpg" width="92" height="24" /></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
</body>
</html>

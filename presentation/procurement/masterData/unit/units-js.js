var basePath = 'presentation/procurement/masterData/unit/';		
$(document).ready(function() {
	
	$("#frmUOM").validationEngine();
	$('#frmUOM #txtCode').focus();
  //permision for add 
  /*if(intAddx)
  {
 	$('#frmUOM #butNew').show();
	$('#frmUOM #butSave').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmUOM #butSave').show();
	$('#frmUOM #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmUOM #butDelete').show();
	$('#frmUOM #cboSearch').removeAttr('disabled');
  }
  
  //permision for view
  if(intViewx)
  {
	$('#frmUOM #cboSearch').removeAttr('disabled');
  }*/
  
/*  $(':input').bind('keyup mouseup', function(){
      $(this).val($.trim($(this).val())) ;
    });*/

  ///save button click event
  $('#frmUOM #butSave').die('click').live('click',function(){
	//$('#frmUOM').submit();
	var requestType = '';
	if ($('#frmUOM').validationEngine('validate'))   
    { 
		//$('#txtLocationCode').validationEngine('showPrompt', 'This is an example', 'pass'); //hide all alerts//
		if($("#frmUOM #cboSearch").val()=='')
			requestType = 'add';
		else
			requestType = 'edit';
		
		var url = basePath+"units-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json",
			type:'POST',  
			data:$("#frmUOM").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addLocation"}',
			async:false,
			
			success:function(json){
					$('#frmUOM #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmUOM').get(0).reset();
						loadCombo_frmUOM();
						var t=setTimeout("alertx()",1000);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					
					$('#frmUOM #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
   
   /////////////////////////////////////////////////////
   //// load location details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmUOM #cboSearch').die('click').live('click',function(){
	   $('#frmUOM').validationEngine('hide');
   });
    $('#frmUOM #cboSearch').die('change').live('change',function(){
		$('#frmUOM').validationEngine('hide');
		var url = basePath+"units-db-get.php";
		if($('#frmUOM #cboSearch').val()=='')
		{
			$('#frmUOM').get(0).reset();return;	
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:'requestType=loadDetails&id='+$(this).val(),
			async:false,
			success:function(json){
					//json  = eval('('+json+')');
					$('#frmUOM #txtCode').val(json.code);
					$('#frmUOM #txtName').val(json.name);
					$('#frmUOM #txtRemark').val(json.remark);
					$('#frmUOM #chkActive').attr('checked',json.status);
					$('#frmUOM #txtPcs').val(json.pcs);
			}
	});
	//////////// end of load details /////////////////
	
	});
	
	$('#frmUOM #butNew').die('click').live('click',function(){
		$('#frmUOM').get(0).reset();
		loadCombo_frmUOM();
		$('#frmUOM #txtCode').focus();
	});
    $('#frmUOM #butDelete').die('click').live('click',function(){
		if($('#frmUOM #cboSearch').val()=='')
		{
			$('#frmUOM #butDelete').validationEngine('showPrompt', 'Please select UOM.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmUOM #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = basePath+"units-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											type:'POST',
											data:'requestType=delete&cboSearch='+$('#frmUOM #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmUOM #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												
												if(json.type=='pass')
												{
													$('#frmUOM').get(0).reset();
													loadCombo_frmUOM();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
});


function loadCombo_frmUOM()
{
	var url 	= basePath+"units-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#frmUOM #cboSearch').html(httpobj.responseText);
}

function alertx()
{
	$('#frmUOM #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmUOM #butDelete').validationEngine('hide')	;
}

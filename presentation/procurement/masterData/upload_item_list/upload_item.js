var basePath = 'presentation/procurement/masterData/upload_item_list/';		
$(document).ready(function(e) {
    $('.butApprove').die('click').live('click',Approve);
	
	$("#td1").die('click').live('click',function(){
		$("#tr2").hide('slow');
		$("#tr1").toggle('slow');
	});
	
	$("#td2").die('click').live('click',function () {
		$("#tr1").hide('slow');
		$("#tr2").toggle('slow');
	});
});

function Approve()
{
	var x = confirm("Are you sure want to 'Approve'");
	if(!x)return;
	
	var url 	 = basePath+'upload_item_db.php?RequestType=Approve';
		url 	+= '&SerialNo='+$(this).parent().parent().find('td:eq(0)').html();
	$.ajax({
		url:url,
		async:false,
		dataType:'json',
		success:function(json){
			if(json.type=='pass')
				document.location.href  = document.location.href;
		}
	});
}
<?php
require_once	"libraries/jqgrid2/inc/jqgrid_dist.php";
$intUser		=$_SESSION["userId"];
$programCode	= 'P0667';	
$approveLevel 	= GetMaxApproveLevel();

if($_SERVER["REQUEST_METHOD"] == "POST")
{
	require_once("reader.php");

	//if (!empty($_FILES['file1'])) 
	if($_FILES["file1"]["size"]!=0)
	{	
		if(!is_dir("documents/supplierBulkItem"))
		{
			mkdir("documents/supplierBulkItem", 0700);
		}
		$upload_path = 'documents/supplierBulkItem/';
		if ($_FILES['file1']['error'] == 0) 
		{
			$file = explode(".", $_FILES['file1']['name']);
			move_uploaded_file($_FILES['file1']['tmp_name'], $upload_path.'/'.$_FILES['file1']['name']);
		} 
		header("Location:presentation/procurement/masterData/upload_item_list/upload_item_write.php?name=".$_FILES['file1']['name']."&Type=S");	
	}
	
	//if (!empty($_FILES['file2'])) 
	if($_FILES["file2"]["size"]!=0)
	{	
		if(!is_dir("documents/companyBulkItem"))
		{
			mkdir("documents/companyBulkItem", 0700);
		}
		$upload_path = 'documents/companyBulkItem/';
		
		if ($_FILES['file2']['error'] == 0) 
		{
			$file = explode(".", $_FILES['file2']['name']);
			move_uploaded_file($_FILES['file2']['tmp_name'], $upload_path.'/'.$_FILES['file2']['name']);
		} 
		header("Location:presentation/procurement/masterData/upload_item_list/upload_item_write.php?name=".$_FILES['file2']['name']."&Type=C");
	}	
}

$sql = "SELECT SUB_1.* FROM
		(SELECT
			IUH.SERIAL_ID		AS SERIAL_ID,
			IUH.UPLOAD_DATE		AS UPLOAD_DATE,
			SU.strUserName		AS UPLOAD_USER_NAME,
			IF(IUH.TYPE='S','Supplier Wise','Company Wise')	AS TYPE,		
			'View'				AS VIEW,
			IFNULL((
			SELECT
			concat(U.strUserName,'(',max(IUA.dtApprovedDate),')' )
			FROM
			trn_item_upload_approvedby IUA
			Inner Join sys_users U 
				ON IUA.intApproveUser = U.intUserId
			WHERE
				IUA.intSerialNo  = IUH.SERIAL_ID AND
				IUA.intApproveLevelNo =  '1' AND 
				IUA.intStatus = '0'
				),IF(((SELECT
				MP.int1Approval 
			FROM menupermision MP
			Inner Join menus M 
				ON MP.intMenuId = M.intId
			WHERE
				M.strCode = '$programCode' AND
				MP.intUserId =  '$intUser') = 1 
				AND IUH.STATUS > 1),'Approve', '')) AS `1st_Approval`, ";
		
for($i=2; $i<=$approveLevel; $i++)
{							
	if($i==2){
		$approval	= "2nd_Approval";
	}
	else if($i==3){
		$approval	= "3rd_Approval";
	}
	else {
		$approval	= $i."th_Approval";
	}
	
	
$sql .= "IFNULL(
		(
		SELECT
		concat(U.strUserName,'(',max(IUA.dtApprovedDate),')' )
		FROM
		trn_item_upload_approvedby IUA
		Inner Join sys_users U
			ON IUA.intApproveUser = U.intUserId
		WHERE
		IUA.intSerialNo  = IUH.SERIAL_ID AND
		IUA.intApproveLevelNo =  '$i' AND 
		IUA.intStatus='0'
		),
		IF(
		((SELECT
			MP.int".$i."Approval 
			FROM menupermision MP
			Inner Join menus M
				ON MP.intMenuId = M.intId
			WHERE
				M.strCode = '$programCode' 
				AND MP.intUserId =  '$intUser')=1 
				AND (IUH.STATUS > 1) 
				AND (IUH.STATUS <= IUH.APPROVE_LEVEL) 
				AND ((SELECT
					max(IUA.dtApprovedDate) 
					FROM
					trn_item_upload_approvedby IUA
					Inner Join sys_users U
						ON IUA.intApproveUser = U.intUserId
					WHERE
					IUA.intSerialNo  = IUH.SERIAL_ID AND
					IUA.intApproveLevelNo =  ($i-1) AND 
					IUA.intStatus='0')<>'')),		
			'Approve',
			 if($i>IUH.APPROVE_LEVEL,'-----',''))		
			) as `".$approval."`, "; 
	}
			
$sql .= "IUH.FOLDER AS FOLDER,
		IUH.URL AS URL
		 FROM trn_item_upload_header IUH
		INNER JOIN sys_users SU 
			ON SU.intUserId = IUH.UPLOAD_USER_ID)  AS SUB_1 WHERE 1=1 ";
//die($sql);

//echo $sql;
$jq 	= new jqgrid('',$db);	
$col 	= array();

$col["title"] 			= "Serial No";
$col["name"] 			= "SERIAL_ID";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Type";
$col["name"] 			= "TYPE";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Upload Date";
$col["name"] 			= "UPLOAD_DATE";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= true;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Uploaded User";
$col["name"] 			= "UPLOAD_USER_NAME";
$col["width"] 			= "4";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Address";
$col["name"] 			= "UPLOAD_DATE";
$col["width"] 			= "4";
$col["align"] 			= "left";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "1st Approval";
$col["name"] 			= "1st_Approval";
$col["width"] 			= "2";
$col["align"] 			= "center";
/*$col['link']			= "rptupload_item.php?SERIAL_ID={SERIAL_ID}";
$col["linkoptions"] 	= "target='rptupload_item.php'";*/
$col['link']			= "#";
$col["linkoptions"] 	= " class='butApprove'";
$col["linkName"] 		= "Approve";
$col["sortable"] 		= false; 					
$col["search"] 			= false; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

for($i=2;$i<=$approveLevel;$i++)
{
if($i==2){
	$ap		= "2nd Approval";
	$ap1	= "2nd_Approval";
}
else if($i==3){
	$ap		= "3rd Approval";
	$ap1	= "3rd_Approval";
}
else{
	$ap		= $i."th Approval";
	$ap1	= $i."th_Approval";
}

$col["title"] 			= $ap;
$col["name"] 			= $ap1;
$col["width"] 			= "2";
$col["align"] 			= "center";
$col['link']			= "#";
$col["linkoptions"] 	= " class='butApprove'";
$col["linkName"] 		= "Approve";
$col["sortable"] 		= false; 					
$col["search"] 			= false; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;
}

$col["title"] 			= "Download";
$col["name"] 			= "URL";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col['link']			= "{URL}";
$col["linkoptions"] 	= "target='_blank'";
$col["sortable"] 		= false; 					
$col["hidden"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Download";
$col["name"] 			= "FOLDER";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col['link']			= "{FOLDER}";
$col["linkoptions"] 	= "target='_blank'";
$col["sortable"] 		= false; 					
$col["hidden"] 			= true; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Download";
$col["name"] 			= "VIEW";
$col["width"] 			= "1";
$col["align"] 			= "center";
$col["link"] 			= "documents/{FOLDER}/{URL}";
$col["linkoptions"] 	= "target='_blank'";
$col["sortable"] 		= false; 					
$col["search"] 			= false; 					
$col["editable"] 		= false;
$cols[] 				= $col;	
$col					= NULL;

$grid["caption"] 		= "Bulk Items Upload Listing";
$grid["multiselect"] 	= false;
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'UPLOAD_DATE'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes
$grid["search"] 		= true; 
$grid["postData"] 		= array("filters" => $sarr ); 
$grid["export"] 		= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");

$jq->set_options($grid);
$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, 				// allow/disallow add
	"edit"=>false, 				// allow/disallow edit
	"delete"=>false, 			// allow/disallow delete
	"rowactions"=>false, 		// show/hide row wise edit/del/save option
	"search" => "advance", 		// show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>

<title>Bulk Items Upload Listing</title>
<?php 
	//	include "include/listing.html";
	?>
	
<form id="frmBulkItemsUploadListing" name="frmBulkItemsUploadListing" method="post" enctype="multipart/form-data">

<!--<script src="presentation/procurement/masterData/upload_item_list/upload_item.js" type="text/javascript"></script>
-->	
<table width="100%" border="0">
   <tr>
    <td><table width="493" border="0" >
    <tr>
    	<td colspan="3" class="normalfnt mouseover" style="text-decoration:underline;color:#00F" id="td1">Supplier Wise Item Upload</td>
    </tr>
      <tr style="display:none" id="tr1" >
        <td width="218"><input type="file" width="150px" name="file1" id="file1"/></td>
        <td width="55"><input type="submit" name="button" class="button green medium" id="button" value="Upload" /></td>
        <td width="206" class="normalfnt"><a href="presentation/procurement/masterData/upload_item_list/sample_supplier.xls">Download sample excel sheet</a></td>
   </tr>
       
   	<tr>
    	<td colspan="3" class="normalfnt mouseover" style="text-decoration:underline;color:#00F" id="td2">Supplier & Company Wise Item Upload</td>
    </tr>
      <tr style="display:none" id="tr2" >
        <td width="218"><input type="file" width="150px" name="file2" id="file2"/></td>
        <td width="55"><input type="submit" name="button" class="button green medium" id="button" value="Upload" /></td>
        <td width="206" class="normalfnt"><a href="presentation/procurement/masterData/upload_item_list/sample_supplier_company.xls">Download sample excel sheet</a></td>
       </tr>
    </table></td>
  </tr>
  <div align="center" style="margin:10px"><?php echo $out?></div></td>
 
</table>
</form>

<?php
function GetMaxApproveLevel()
{
	global $db;	
	$appLevel	= 0;
	
	$sql = "SELECT
			COALESCE(Max(trn_item_upload_header.APPROVE_LEVEL),0) AS appLevel
			FROM trn_item_upload_header";	
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	return $row['appLevel'];
}
?>
<?php
session_start();
date_default_timezone_set('Asia/Kolkata');
$backwardseperator 	= "../../../../";
$session_userId		= $_SESSION["userId"];
require_once("../../../../dataAccess/Connector.php");

$programCode	= 'P0667';	
$requestType		= $_REQUEST["RequestType"];
$savedStatus		= true;

if($requestType=='Approve')
{
	$serialNo	= $_REQUEST["SerialNo"];
	$status		= CheckStatus($serialNo);
	if($status!='1')
		InsertToApprove($serialNo);
	
	if($savedStatus){
		$response['msg'] = "Approved successfully.";	
		$response['type'] = 'pass';
	}
	else{
		$response['msg'] = "Approved failed.";	
		$response['type'] = 'fail';
	}
	echo json_encode($response);
}

function CheckStatus($serialNo)
{
	global $db;
	
	$sql = "SELECT STATUS FROM trn_item_upload_header WHERE SERIAL_ID = $serialNo";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	return $row["STATUS"];
}

function InsertToApprove($serialNo)
{
	global $db;
	global $session_userId;
	global $savedStatus;
	
	$approve_level	= GetMaxApproveLevel($serialNo);
	
	$sql = "INSERT INTO trn_item_upload_approvedby 
			(intSerialNo, 
			intApproveLevelNo, 
			intApproveUser, 
			dtApprovedDate, 
			intStatus)
			VALUES
			('$serialNo', 
			'$approve_level', 
			'$session_userId', 
			now(), 
			'0');";
	$result = $db->RunQuery($sql);
	if(!$result)
		$savedStatus = false;
	
	$sql = "UPDATE trn_item_upload_header 
			SET STATUS = STATUS - 1
			WHERE SERIAL_ID = $serialNo ;";
	$result = $db->RunQuery($sql);
	if(!$result)
		$savedStatus = false;
		
	$sql = "DELETE FROM mst_item_supplier";
	$result = $db->RunQuery($sql);
	if(!$result)
		$savedStatus = false;
		
	$sql = "INSERT INTO mst_item_supplier 
				(intItemId, 
				intSupplierId, 
				dblPrice
				)
			SELECT 	ITEM_ID, 
				SUPPLIER_ID,	
				UNIT_PRICE	 
				FROM 
				trn_item_upload_details D
				INNER JOIN trn_item_upload_header H 
					ON H.SERIAL_ID = D.SERIAL_ID
				WHERE H.SERIAL_ID = $serialNo AND STATUS = 1 AND H.TYPE = 'S'";
	$result = $db->RunQuery($sql);
	if(!$result)
		$savedStatus = false;
		
		$sql = "INSERT INTO mst_item_supplier_company 
				(intItemId, 
				intSupplierId, 
				intCompanyId,
				dblPrice
				)
			SELECT 	ITEM_ID, 
				SUPPLIER_ID,
				COMPANY_ID,	
				UNIT_PRICE	 
				FROM 
				trn_item_upload_details D
				INNER JOIN trn_item_upload_header H 
					ON H.SERIAL_ID = D.SERIAL_ID
				WHERE H.SERIAL_ID = $serialNo AND STATUS = 1 AND H.TYPE = 'C'";
	$result = $db->RunQuery($sql);
	if(!$result)
		$savedStatus = false;
}

function GetMaxApproveLevel($serialNo)
{
	global $db;	
	$appLevel	= 0;
	
	$sql = "SELECT COALESCE(MAX(intApproveLevelNo),0) AS  MAX_NO
			FROM trn_item_upload_approvedby 
			WHERE intSerialNo = $serialNo AND intStatus = 0";	
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);	
	return $row['MAX_NO']+1;
}
?>

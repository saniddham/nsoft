<?php
session_start();
$session_userId	= $_SESSION["userId"];
ini_set('max_execution_time',60000);
require_once("../../../../dataAccess/Connector.php");
require_once("reader.php");

	if($_REQUEST['Type']=='S'){
		$upload_path 	= '../../../../documents/supplierBulkItem/'.$_REQUEST['name']; //same directory
		$folder			= "supplierBulkItem";
	}else{
		$upload_path 	= '../../../../documents/companyBulkItem/'.$_REQUEST['name']; //same directory
		$folder			= "companyBulkItem";
	}
	$booSaveStatus	= true;
	$message		= "";
	$db->Begin();	
	
	$excel = new Spreadsheet_Excel_Reader();
	$excel->read($upload_path);   

	$approveLevel	= GetApproveLevel1('P0667');
	$serialNo		= Insert_to_status_header($approveLevel,$_REQUEST['name'],$_REQUEST['Type'],$folder);
	
	if($_REQUEST['Type']=='S')
	{
		for ($i=2;$i<$excel->sheets[0]['numRows']+1;$i++)
		{
			$companyId	= 0;
			$supplierId	= GetSupplierId($excel->sheets[0]['cells'][$i][1]);		
			$itemId		= GetItemId($excel->sheets[0]['cells'][$i][2]);
			$price		= $excel->sheets[0]['cells'][$i][3];

			if($supplierId=='' || $itemId=='' || $price==''){
				$booSaveStatus = false;
				$message		= "Codes which available in the excel sheeet not match with the system.<br/>Please check the 'Line No :$i'<br/><br/><a href=\"upload_item_list.php\">Click here to go back<a/>";
				continue;
			}
			Insert_to_status_details_S($serialNo,$supplierId,$itemId,$price,$companyId);
		}
	}
	
	if($_REQUEST['Type']=='C')
	{
		for ($i=2;$i<$excel->sheets[0]['numRows']+1;$i++)
		{
			$companyId	= GetCompanyId($excel->sheets[0]['cells'][$i][1]);
			$supplierId	= GetSupplierId($excel->sheets[0]['cells'][$i][2]);
			$itemId		= GetItemId($excel->sheets[0]['cells'][$i][3]);
			$price		= $excel->sheets[0]['cells'][$i][4];
			
			if($supplierId=='' || $itemId=='' || $price==''){
				$booSaveStatus = false;
				$message		= "Codes which available in the excel sheeet not match with the system.<br/>Please check the 'Line No :$i'<br/><br/><a href=\"upload_item_list.php\">Click here to go back<a/>";
				continue;
			}
			Insert_to_status_details_C($serialNo,$supplierId,$itemId,$price,$companyId);
		}
	}
	
	if($booSaveStatus)
	{
		$db->Commit();
		header("Location:upload_item_list.php");
	}
	else{
		$db->Rollback();
		die($message);
	}
	
	
function Insert_to_status_header($approveLevel,$upload_path,$type,$folder)
{
	global $db;
	global $session_userId;
	global $booSaveStatus;
	global $message;
	
	$sql = "INSERT INTO trn_item_upload_header 
			(UPLOAD_DATE, 
			UPLOAD_USER_ID,
			FOLDER,
			URL,
			STATUS, 
			APPROVE_LEVEL,
			TYPE
			)
			VALUES
			(now(), 
			'$session_userId',
			'$folder',
			'$upload_path',
			$approveLevel + 1, 
			$approveLevel,
			'$type');";echo $sql;
	$result = $db->RunQuery2($sql);
	if(!$result){
		$booSaveStatus = false;
		$message		=  $db->mysqli_error();
	}
	return  $db->insertId;
}

function Insert_to_status_details_S($serialNo,$supplierId,$itemId,$price,$companyId)
{
	global $db;
	global $booSaveStatus;
	global $message;
	
	$sql = "INSERT INTO trn_item_upload_details 
			(SERIAL_ID,
			COMPANY_ID,
			SUPPLIER_ID, 
			ITEM_ID, 
			UNIT_PRICE
			)
			VALUES
			('$serialNo',
			'$companyId',
			'$supplierId', 
			'$itemId', 
			'$price');";
			
	$result = $db->RunQuery2($sql);
	if(!$result){
		$booSaveStatus = false;
		$message		=  $db->errormsg();
	}
		
}

function Insert_to_status_details_C($serialNo,$supplierId,$itemId,$price,$companyId)
{
	global $db;
	global $booSaveStatus;
	global $message;
	
	$sql = "INSERT INTO trn_item_upload_details 
			(SERIAL_ID,
			COMPANY_ID,
			SUPPLIER_ID, 
			ITEM_ID, 
			UNIT_PRICE
			)
			VALUES
			('$serialNo',
			'$companyId',
			'$supplierId', 
			'$itemId', 
			'$price');";
	$result = $db->RunQuery2($sql);
	if(!$result){
		$booSaveStatus 	= false;
		$message		=  $db->mysqli_error();
	}
}

function GetApproveLevel1($pro_code)
{
	global $db;
	$approveLevel	= 0 ;
	
	$sql 	= "SELECT intApprovalLevel FROM sys_approvelevels WHERE strCode = '$pro_code'";
	$result = $db->RunQuery2($sql);
	$row 	= mysqli_fetch_array($result);
	$approveLevel = $row["intApprovalLevel"];
	return $approveLevel;
}

function GetSupplierId($code)
{
	global $db;
	
	$sql = "SELECT S.intId AS SUPPLIER_ID FROM mst_supplier S WHERE S.strCode = '$code'";
	$result = $db->RunQuery2($sql);
	$row 	= mysqli_fetch_array($result);
	return $row["SUPPLIER_ID"];
}

function GetItemId($code)
{
	global $db;
	
	$sql = "SELECT I.intId AS ITEM_ID FROM mst_item I WHERE I.strCode = '$code'";
	$result = $db->RunQuery2($sql);
	$row 	= mysqli_fetch_array($result);
	return $row["ITEM_ID"];
}

function GetCompanyId($code)
{
	global $db;
	
	$sql = "SELECT C.intId AS COMPANY_ID FROM mst_companies C WHERE C.strCode = '$code'";
	$result = $db->RunQuery2($sql);
	$row 	= mysqli_fetch_array($result);
	return $row["COMPANY_ID"];
}
?>
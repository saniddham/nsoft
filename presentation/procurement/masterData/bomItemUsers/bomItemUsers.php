<?php
session_start();
$backwardseperator = "../../../../";
$thisFilePath =  $_SERVER['PHP_SELF'];
include  "{$backwardseperator}dataAccess/permisionCheck.inc";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Bom Item Users</title>

<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="bomItemUsers-js.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" href="../../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../../libraries/validate/template.css" type="text/css">

</head>

<body>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<script src="../../../../libraries/validate//jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<form id="frmBomItemUsers" name="frmBomItemUsers"   autocomplete="off">
<div align="center">
<div class="trans_layoutS">
		  <div class="trans_text">Bom Item User</div>
		  <table width="400" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0" align="center">
      <tr>
        <td width="62%"><table width="100%" border="0" > 
          <tr>
            <td height="47" ><table width="112%" border="0" class="">
              <tr>
                <td  class="normalfnt">&nbsp;</td>
                <td wclass="normalfnt">&nbsp;</td>
                <td >&nbsp;</td>
              </tr>
              <tr>
                <td colspan="3" class="normalfnt"><table id="tblMain" width="100%" border="0" class="grid">
                  
                  <tr class="gridHeader">
                    <td colspan="3" bgcolor="#FFFFFF" class="normalfntRight"><img src="../../../../images/insert.png" name="butAdd" width="78" height="24" id="butAdd" class="mouseover" /></td>
                    </tr>
                  <tr class="gridHeader">
                    <td width="21%" >Delete</td>
                    <td width="79%" >User</td>
                    </tr>
                  <?php
				  
				  $sql = " SELECT sys_bomitem_users.intUserId,sys_users.strUserName
							FROM sys_bomitem_users
							INNER JOIN sys_users WHERE sys_users.intUserId=sys_bomitem_users.intUserId
							order by sys_users.strUserName";
				  $result = $db->RunQuery($sql);
				  while($row=mysqli_fetch_array($result))
				  {
				 ?>
					<tr id="<?php echo $row['intUserId'] ?>" class="normalfnt">
                    <td style="text-align:center" bgcolor="#FFFFFF" class="normalfnt clsBomUser"><img class="mouseover removeRow" src="../../../../images/del.png" /></td>
                    <td bgcolor="#FFFFFF" class="normalfnt" align="left"><?php echo $row['strUserName']; ?></td>
                    </tr>				 
				 <?php
				  }
				  
				  ?>  
                    
                    
                </table></td>
                </tr>
              </table></td>
            </tr>
          <tr>
            <td height="34"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td width="100%" align="center" bgcolor=""><img style="display:none" border="0" src="../../../../images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><img style="display:none" border="0" src="../../../../images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="25"/><img border="0" src="../../../../images/Tsave.jpg" id="butSave" name="butSave" width="92" class="mouseover" height="24" /><a href="../../../../main.php"><img  src="../../../../images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                </tr>
              </table></td>
            </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
  </table>
	</div>
  </div>
</form>
</body>
 <div    style="width:500px; position: absolute;display:none;z-index:100"  id="popupContact1"></div>
 <div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
</html>

// JavaScript Document
$(document).ready(function() {
	
	setRemoveRow();
	$('#butAdd').live('click',function(){
		
		popupWindow3('1');
		$('#popupContact1').html('');
		$('#popupContact1').load('popup.php',function(){
			
			$('#frmPopUp #popButAdd').click(function(){
				//addEmailsToGrid(mainRowId,typeRowId,cellId);
				$('.chkUsers:checked').each(function(){
					var fromId = $(this).val();
					var found = false;
					$('.clsBomUser').each(function(){
						if(fromId ==$(this).parent().attr('id'))
						{
							found = true;
						}	
					});
					if(found==false)
					{
						document.getElementById('tblMain').insertRow(document.getElementById('tblMain').rows.length);
						document.getElementById('tblMain').rows[document.getElementById('tblMain').rows.length-1].innerHTML = "<td style=\"text-align:center\" bgcolor=\"#FFFFFF\" class=\"normalfnt clsBomUser\">"+
						"<img class=\"mouseover removeRow\" src=\"../../../../images/del.png\" /></td>"+
						"<td bgcolor=\"#FFFFFF\" class=\"normalfnt\">"+$(this).parent().parent().find('td').eq(1).html()+"</td>";
						document.getElementById('tblMain').rows[document.getElementById('tblMain').rows.length-1].id  = fromId;	
						setRemoveRow();	
					}
				});
				disablePopup();
				return;
				
			});
			
			$('#frmPopUp #butClose1').click(disablePopup);
			
		});	
		
	});
	$('#butSave').live('click',function(){
		
		var value="[ ";
		$('.clsBomUser').each(function(){
			
			var userId = $(this).parent().attr('id');
			value +='{"userId":"'+userId+'"},' ;	
		})
		value = value.substr(0,value.length-1);
		value += " ]";
		
		var url = "bomItemUsers-db-set.php?requestType=saveData";
					$.ajax({
							url:url,
							async:false,
							dataType:'json',
							type:'post',
							data:'&userDetails='+value,
						success:function(json){
							$('#frmBomItemUsers #butSave').validationEngine('showPrompt', json.msg,json.type );
							if(json.type=='pass')
							{
								var t=setTimeout("alertx()",1000);
								setTimeout("window.location.reload();",1000)
								return;
							}
						},
						error:function(xhr,status){
							
							$('#frmBomItemUsers #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
							var t=setTimeout("alertx()",1000);
							return;
						}
					});
	});
	
});
function setRemoveRow()
{
	$('.removeRow').click(function(){
		 $(this).parent().parent().remove();
	});	
}
function alertx()
{
	$('#frmBomItemUsers #butSave').validationEngine('hide')	;
}
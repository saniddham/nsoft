<?php
(define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');
if($_REQUEST['iframe'])
{
include "include/javascript.html";
include "include/css.html";
?>
<script type="text/javascript" src="../../subCategory/addNew/subCategory-js.js"></script>
<?php } 
include "class/finance/masterData/chartOfAccount/cls_chartofaccount_get.php";
$obj_chartofaccount_get	= new Cls_ChartOfAccount_Get($db);
echo ($popup);
?>
<title>Sub Category</title>
<form id="frmSubCategory" name="frmSubCategory" method="post" autocomplete="off"  >

<div align="center">
		<div class="trans_layoutD" style="width:550px">
		  <div class="trans_text">Sub Category</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="550" border="0" align="center">
      <tr>
        <td width="62%"><table width="100%" border="0" > 
          <tr>
            <td height="47" ><table width="100%" border="0" class="">
              <tr>
                <td width="24" class="normalfnt">&nbsp;</td>
                <td width="159" class="normalfnt">Sub Category</td>
                <td width="323">
                  <select  <?Php if(!($form_permision['edit'] || $form_permision['delete'])){echo 'disabled';}  ?> name="cboSearch" class="txtbox" id="cboSearch"  style="width:250px;"  tabindex="1" >
                  <option value=""></option>
                 <?php  $sql = "SELECT
									intId,
									strName
								FROM mst_subcategory
								order by strName
								";
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
								}
                   ?> </select>				</td>
                <td width="14">&nbsp;</td>                
                </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">&nbsp;</td>
                <td>&nbsp;</td>
                <td class="normalfnt">&nbsp;</td>
              </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td colspan="2" class="normalfnt">
                <fieldset class="tableBorder">
                <legend class="normalfntBlue"><b>&nbsp;General&nbsp;</b></legend>
                <table width="100%" border="0" cellpadding="3">
                  <tr>
                    <td><table width="100%" border="0">
                      <tr class="normalfnt">
                        <td>Sub Category Code&nbsp;<span class="compulsoryRed">*</span></td>
                        <td><input  name="txtCode" type="text" class="validate[required,maxSize[10]] cls_clear" id="txtCode" style="width:140px"  tabindex="2" maxlength="10"/></td>
                      </tr>
                      <tr class="normalfnt">
                        <td>Sub Category Name&nbsp;<span class="compulsoryRed">*</span></td>
                        <td><input name="txtName" type="text" class="validate[required,maxSize[50]] cls_clear" id="txtName" style="width:250px" maxlength="50"  tabindex="3"/></td>
                      </tr>
                      <tr class="normalfnt">
                        <td width="32%">Main Category <span class="compulsoryRed">*</span></td>
                        <td width="68%"><select  name="cboMainCategory" class="validate[required] cls_clear" id="cboMainCategory"  style="width:250px;"  tabindex="4" >
                          <option value=""></option>
                          <?php  $sql = "SELECT
									intId,
									strName
								FROM mst_maincategory
								WHERE 
									intStatus=1
								order by strName
								";
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
								}
                   ?>
                        </select></td>
                      </tr>
                      <tr class="normalfnt">
                        <td valign="top">Remarks</td>
                        <td valign="top"><textarea name="txtRemark" style="width:250px" class="validate[maxSize[250]] cls_clear"  rows="2" id="txtRemark" tabindex="5" ></textarea></td>
                      </tr>
                    </table></td>
                  </tr>
                </table>
                </fieldset></td>
                <td class="normalfnt">&nbsp;</td>
                </tr>
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td colspan="2" class="normalfnt">
                <fieldset class="tableBorder">
                <legend class="normalfntBlue"><b>&nbsp;Finance&nbsp;</b></legend>
                <table width="100%" border="0" cellpadding="3">
                  <tr>
                    <td><table width="100%" border="0">
                      <tr class="normalfnt">
                        <td>Finance Type <span class="compulsoryRed">*</span></td>
                        <td>
                        <select  name="cboFinanceType" id="cboFinanceType"  style="width:140px;"  tabindex="6" class="cls_normal cls_clear">
						<?php
                        	echo $obj_chartofaccount_get->loadFinanceTypeCombo();
                        ?>
                        </select></td>
                      </tr>
                      <tr class="normalfnt">
                        <td>Main Type <span class="compulsoryRed">*</span></td>
                        <td><select  name="cboMainType" id="cboMainType"  style="width:250px;"  tabindex="7" class="cls_normal cls_clear">
                          
                        </select></td>
                      </tr>
                      <tr class="normalfnt">
                        <td width="32%">Sub Type <span class="compulsoryRed">*</span></td>
                        <td width="68%"><select  name="cboSubType" id="cboSubType"  style="width:250px;"  tabindex="8" class="cls_normal cls_clear">
                          
                        </select></td>
                      </tr>
                      <tr class="normalfnt">
                        <td>Chart Of Account <span class="compulsoryRed">*</span></td>
                        <td><select  name="cboChartOfAccount" class="validate[required] cls_normal cls_clear" id="cboChartOfAccount"  style="width:250px;"  tabindex="9" >
                          
                        </select></td>
                      </tr>
                      <tr class="normalfnt">
                        <td>Depreciation Rate <span class="compulsoryRed">*</span></td>
                        <td><input  name="txtDepreciationRate" type="text" class="validate[required,maxSize[4]]" id="txtDepreciationRate" style="width:140px;text-align:right"  tabindex="10" maxlength="4"/> 
                          %</td>
                      </tr>
                    </table></td>
                  </tr>
                </table>
                </fieldset></td>
                <td class="normalfnt">&nbsp;</td>
              </tr>
              
              <tr id="fDebit" style="display:none">
                <td class="normalfnt">&nbsp;</td>
                <td colspan="2" class="normalfnt">
                <fieldset class="tableBorder" id="fDebit1">
                <legend class="normalfntBlue"><b>&nbsp;Depreciation  Debit Account&nbsp;</b></legend>
                <table width="100%" border="0" cellpadding="3">
                  <tr>
                    <td><table width="100%" border="0">
                      <tr class="normalfnt">
                        <td>Finance Type <span class="compulsoryRed">*</span></td>
                        <td>
                        <select  name="cboFinanceType_depDr" id="cboFinanceType_depDr"  style="width:140px;"  tabindex="6" class="cls_clear" >
						<?php
                        	echo $obj_chartofaccount_get->loadFinanceTypeCombo();
                        ?>
                        </select></td>
                      </tr>
                      <tr class="normalfnt">
                        <td>Main Type <span class="compulsoryRed">*</span></td>
                        <td><select  name="cboMainType_depDr" id="cboMainType_depDr"  style="width:250px;"  tabindex="7" class="cls_debit cls_clear" >
                          
                        </select></td>
                      </tr>
                      <tr class="normalfnt">
                        <td width="32%">Sub Type <span class="compulsoryRed">*</span></td>
                        <td width="68%"><select  name="cboSubType_depDr" id="cboSubType_depDr"  style="width:250px;"  tabindex="8" class="cls_debit cls_clear" >
                          
                        </select></td>
                      </tr>
                      <tr class="normalfnt">
                        <td>Chart Of Account <span class="compulsoryRed">*</span></td>
                        <td><select  name="cboChartOfAccount_depDr" class="validate[required] cls_debit cls_clear" id="cboChartOfAccount_depDr"  style="width:250px;"  tabindex="9" >
                          
                          </select></td>
                      </tr>
                      </table></td>
                  </tr>
                </table>
                </fieldset></td>
                <td class="normalfnt">&nbsp;</td>
              </tr>
              <tr id="fCredit" style="display:none">
                <td class="normalfnt">&nbsp;</td>
                <td colspan="2" class="normalfnt">
                <fieldset class="tableBorder" id="fCredit1">
                <legend class="normalfntBlue"><b>&nbsp;Depreciation  Credit Account&nbsp;</b></legend>
                <table width="100%" border="0" cellpadding="3">
                  <tr>
                    <td><table width="100%" border="0">
                      <tr class="normalfnt">
                        <td>Finance Type <span class="compulsoryRed">*</span></td>
                        <td>
                        <select  name="cboFinanceType_depCr" id="cboFinanceType_depCr"  style="width:140px;"  tabindex="6" class="cls_clear">
						<?php
                        	echo $obj_chartofaccount_get->loadFinanceTypeCombo();
                        ?>
                        </select></td>
                      </tr>
                      <tr class="normalfnt">
                        <td>Main Type <span class="compulsoryRed">*</span></td>
                        <td><select  name="cboMainType_depCr" id="cboMainType_depCr"  style="width:250px;"  tabindex="7" class="cls_credit cls_clear">
                          
                        </select></td>
                      </tr>
                      <tr class="normalfnt">
                        <td width="32%">Sub Type <span class="compulsoryRed">*</span></td>
                        <td width="68%"><select  name="cboSubType_depCr" id="cboSubType_depCr"  style="width:250px;"  tabindex="8" class="cls_credit cls_clear">
                          
                        </select></td>
                      </tr>
                      <tr class="normalfnt">
                        <td>Chart Of Account <span class="compulsoryRed">*</span></td>
                        <td><select  name="cboChartOfAccount_depCr" class="validate[required] cls_credit cls_clear" id="cboChartOfAccount_depCr"  style="width:250px;"  tabindex="9" >
                          
                          </select></td>
                      </tr>
                      </table></td>
                  </tr>
                </table>
                </fieldset></td>
                <td class="normalfnt">&nbsp;</td>
              </tr>
              
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">&nbsp;&nbsp;&nbsp;&nbsp;Active</td>
                <td><input type="checkbox" name="chkActive" id="chkActive" checked="checked" tabindex="10"/></td>
                <td class="normalfnt">&nbsp;</td>
                </tr>
            <tr>
                <td class="normalfnt">&nbsp;</td>
                <td class="normalfnt">Send details to finance module</td>
                <td><input type="checkbox"name="chkSendDetails" id="chkSendDetails" checked="checked" tabindex="10"/></td>
                <td><img style="display:none"  src="images/completed.png" alt="Change" name="butChange" width="24" height="24"  class="mouseover" id="butChange" tabindex="10"></td>
            </tr>
              </table></td>
            </tr>
          <tr>
            <td height="34"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td width="100%" align="center" bgcolor=""><img style="display:" border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><?php
                if($form_permision['add']||$form_permision['edit'])
				{
				?><img  style="display:" border="0" src="images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/><?php
				}
                if($form_permision['delete'])
				{
				?><img style="display:" border="0" src="images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="25"/><?Php 
				}
				?><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                </tr>
              </table></td>
            </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
  </table>
	</div>
  </div>
</form>


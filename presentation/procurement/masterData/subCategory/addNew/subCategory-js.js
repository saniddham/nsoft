var basePath = "presentation/procurement/masterData/subCategory/addNew/";
$(document).ready(function() {
	$("#frmSubCategory").validationEngine();
	$('#frmSubCategory #txtCode').focus();
	
	//BEGIN - INTERFACE BUTTON VALIDATION {
	/*if(intAddx)		//permision for add 
	{
		$('#frmSubCategory #butNew').show();
		$('#frmSubCategory #butSave').show();
	}	
	
	if(intEditx)	//permision for edit 
	{
		$('#frmSubCategory #butSave').show();
		$('#frmSubCategory #cboSearch').removeAttr('disabled');
	}	
	
	if(intDeletex)	//permision for delete
	{
		$('#frmSubCategory #butDelete').show();
		$('#frmSubCategory #cboSearch').removeAttr('disabled');
	}	
	
	if(intViewx)	//permision for view
	{
		$('#frmSubCategory #cboSearch').removeAttr('disabled');
	}*/	
	//END - INTERFACE BUTTON VALIDATION }
	
   $('#frmSubCategory #butSave').die('click').live('click',Save);

   $('#frmSubCategory #butChange').die('click').live('click',changeSendDetails);

   $('#frmSubCategory').die('click').live('click',function(){
	   $('#frmSubCategory #butDelete').validationEngine('hide');
   });
   
    $('#frmSubCategory #cboSearch').die('change').live('change',function(){
		$('#frmSubCategory').validationEngine('hide');
		var url = basePath+"subCategory-db-get.php";
		clearCombo('All');
		if($('#frmSubCategory #cboSearch').val()=='')
		{
			$('#frmSubCategory').get(0).reset();
            $('#frmSubCategory #butChange').css('display','none');return;
		}
		else{
			$('#frmSubCategory #butChange').css('display','block');
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:'requestType=loadDetails&id='+$(this).val(),
			async:false,
			success:function(json){
					$('#frmSubCategory #txtCode').val(json.code);
					$('#frmSubCategory #txtName').val(json.name);
					$('#frmSubCategory #txtRemark').val(json.remark);
					$('#frmSubCategory #txtDepreciationRate').val(json.depreciationRate);
					$('#frmSubCategory #cboMainCategory').val(json.intMainCategory);
					if(json.status)
						$('#frmSubCategory #chkActive').attr('checked',true);
					else
						$('#frmSubCategory #chkActive').removeAttr('checked');
					if(json.sendDetails)
						$('#frmSubCategory #chkSendDetails').attr('checked',true);
					else
						$('#frmSubCategory #chkSendDetails').removeAttr('checked');
					
					if(parseFloat($('#frmSubCategory #txtDepreciationRate').val()) >0)
					{
						$('#fDebit').show('slow');
						$('#fCredit').show('slow');
					}
					else
					{
						$('#fDebit').hide('slow');
						$('#fCredit').hide('slow');
					}
					
					if(json.ledger)
					{
						$('#frmSubCategory #cboFinanceType').val(json.financeType);
						$('#frmSubCategory #cboFinanceType').change();
						
						$('#frmSubCategory #cboMainType').val(json.mainType);
						$('#frmSubCategory #cboMainType').change();
						
						$('#frmSubCategory #cboSubType').val(json.subType);
						$('#frmSubCategory #cboSubType').change();
						
						$('#frmSubCategory #cboChartOfAccount').val(json.chartOfAccId);
					}
					
					if(json.ledger_dr)
					{
						$('#frmSubCategory #cboFinanceType_depDr').val(json.financeType_dr);
						$('#frmSubCategory #cboFinanceType_depDr').change();
						
						$('#frmSubCategory #cboMainType_depDr').val(json.mainType_dr);
						$('#frmSubCategory #cboMainType_depDr').change();
						
						$('#frmSubCategory #cboSubType_depDr').val(json.subType_dr);
						$('#frmSubCategory #cboSubType_depDr').change();
						
						$('#frmSubCategory #cboChartOfAccount_depDr').val(json.chartOfAccId_dr);
					}
					
					if(json.ledger_cr)
					{
						$('#frmSubCategory #cboFinanceType_depCr').val(json.financeType_cr);
						$('#frmSubCategory #cboFinanceType_depCr').change();
						
						$('#frmSubCategory #cboMainType_depCr').val(json.mainType_cr);
						$('#frmSubCategory #cboMainType_depCr').change();
						
						$('#frmSubCategory #cboSubType_depCr').val(json.subType_cr);
						$('#frmSubCategory #cboSubType_depCr').change();
						
						$('#frmSubCategory #cboChartOfAccount_depCr').val(json.chartOfAccId_cr);
					}
			}
	});
	//////////// end of load details /////////////////
	
	});
	
	$('#frmSubCategory #butNew').die('click').live('click',function(){
		$('#frmSubCategory').get(0).reset();
		loadCombo_frmSubCategory();
		$('#frmSubCategory #txtCode').focus();
		$('#fDebit').hide('slow');
		$('#fCredit').hide('slow');
	});
	
    $('#frmSubCategory #butDelete').die('click').live('click',function(){
		if($('#frmSubCategory #cboSearch').val()=='')
		{
			$('#frmSubCategory #butDelete').validationEngine('showPrompt', 'Please select Sub Category.', 'fail');
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmSubCategory #txtName').val()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = basePath+"subCategory-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											type:'POST',
											data:'requestType=delete&cboSearch='+$('#frmSubCategory #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmSubCategory #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												//var t=setTimeout("alertDelete()",1000);
												if(json.type=='pass')
												{
													$('#frmSubCategory').get(0).reset();
													loadCombo_frmSubCategory();
												}	
											}	 
										});
									}
				}
		 	});
			
		}
	});
//---------------------- Add By Lahiru -----------------------------------

$('#frmSubCategory #cboFinanceType').die('change').live('change',function(){loadMainType(this,'#cboMainType','financeType')});
$('#frmSubCategory #cboFinanceType_depDr').die('change').live('change',function(){loadMainType(this,'#cboMainType_depDr','financeType_debit')});
$('#frmSubCategory #cboFinanceType_depCr').die('change').live('change',function(){loadMainType(this,'#cboMainType_depCr','financeType_credit')});

$('#frmSubCategory #cboMainType').die('change').live('change',function(){loadSubType(this,'#cboSubType','mainType')});
$('#frmSubCategory #cboMainType_depDr').die('change').live('change',function(){loadSubType(this,'#cboSubType_depDr','mainType_debit')});
$('#frmSubCategory #cboMainType_depCr').die('change').live('change',function(){loadSubType(this,'#cboSubType_depCr','mainType_credit')});

$('#frmSubCategory #cboSubType').die('change').live('change',function(){loadChartOfAccount(this,'#cboChartOfAccount','subType')});
$('#frmSubCategory #cboSubType_depDr').die('change').live('change',function(){loadChartOfAccount(this,'#cboChartOfAccount_depDr','subType_depDr')});
$('#frmSubCategory #cboSubType_depCr').die('change').live('change',function(){loadChartOfAccount(this,'#cboChartOfAccount_depCr','subType_depCr')});

//-------------------------------------------------------------------------

$('#frmSubCategory #txtDepreciationRate').die('keyup').live('keyup',TEST);
});

$('#frmSubCategory').die('click').live('click',function(){
		$('#frmSubCategory #butDelete').validationEngine('hide')	;
});

function Save()
{
	if(!$('#frmSubCategory').validationEngine('validate'))   
    	return ;
		
	showWaiting();
	if($("#frmSubCategory #cboSearch").val()=='')
		var requestType = 'add';
	else
		var requestType = 'edit';
	
	var url = basePath+"subCategory-db-set.php";
	var obj = $.ajax({
		url:url,
		dataType: "json",
		type:'POST',  
		data:$("#frmSubCategory").serialize()+'&requestType='+requestType,
		async:false,			
		success:function(json){
				$('#frmSubCategory #butSave').validationEngine('showPrompt', json.msg,json.type);
				if(json.type=='pass')
				{
					setTimeout("alertx()",1000);
					loadCombo_frmSubCategory();	
					$('#frmSubCategory').get(0).reset();					
					$('#fDebit').hide('slow');
					$('#fCredit').hide('slow');
					return;
				}
				setTimeout("alertx()",1000);
			},
		error:function(xhr,status){
				$('#frmSubCategory #butSave').validationEngine('showPrompt', errormsg(xhr.status), 'fail');
				setTimeout("alertx()",1000);
			}		
		});
	hideWaiting();
}

function changeSendDetails()
{
    if($('#frmSubCategory #cboSearch').val() == '')
        return ;

    var url = basePath+"subCategory-db-set.php";
    var obj = $.ajax({
        url:url,
        dataType: "json",
        type:'POST',
        data:$("#frmSubCategory").serialize()+'&requestType=sendDetails',
        async:false,
        success:function(json){
            $('#frmSubCategory #butChange').validationEngine('showPrompt', json.msg,json.type);
            if(json.type=='pass')
            {
                setTimeout("alertChange()",1000);
                return;
            }
            setTimeout("alertChange()",1000);
        },
        error:function(xhr,status){
            $('#frmSubCategory #butChange').validationEngine('showPrompt', errormsg(xhr.status), 'fail');
            setTimeout("alertChange()",1000);
        }
    });

}



function loadCombo_frmSubCategory()
{
	var url 	= basePath+"subCategory-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#cboSearch').html(httpobj.responseText);
}

function alertx()
{
	$('#frmSubCategory #butSave').validationEngine('hide')	;
}

function alertChange()
{
    $('#frmSubCategory #butChange').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmSubCategory #butDelete').validationEngine('hide')	;
}
//---------------------- Add By Lahiru -----------------------------------
function loadMainType(obj,cbo,type)
{
	clearCombo(type,cbo);
	if($(obj).val()=='')
	{
		return;
	}
	var url 	= basePath+"subCategory-db-get.php?requestType=loadMainType";
	var data 	= "financeType="+$(obj).val();
	$.ajax({
		url:url,
		dataType:'json', 
		type:'POST', 
		data:data,
		async:false,
		success:function(json){				
				$(cbo).html(json.mainTypeCombo);
		},
		error:function(xhr,status){
			
		}		
	});
}
function loadSubType(obj,cbo,type)
{
	clearCombo(type,cbo);
	if($(obj).val()=='')
	{
		return;
	}
	var url 	= basePath+"subCategory-db-get.php?requestType=loadSubType";
	var data 	= "mainType="+$(obj).val();
	$.ajax({
		url:url,
		dataType:'json',  
		type:'POST',
		data:data,
		async:false,
		success:function(json){
			
			$(cbo).html(json.subTypeCombo);
		},
		error:function(xhr,status){	
		}		
	});
}
function loadChartOfAccount(obj,cbo,type)
{
	clearCombo(type,cbo);
	if($(obj).val()=='')
	{
		return;
	}
	var url 	= basePath+"subCategory-db-get.php?requestType=loadChartOfAccount";
	var data 	= "subType="+$(obj).val();
	$.ajax({
		url:url,
		dataType:'json',
		type:'POST',  
		data:data,
		async:false,
		success:function(json){
				
				$(cbo).html(json.chrtOfAccCombo);
		},
		error:function(xhr,status){
			
		}		
	});
}
function clearCombo(type,cbo)
{
	switch(type)
	{
		case 'All' :
			$('.cls_clear').val('');
			break;		
		case 'financeType' :
			$('#cboMainType').html('');
			$('#cboSubType').html('');
			$('#cboChartOfAccount').html('');
			break;		
		case 'financeType_debit':
			$('#cboMainType_debit').html('');
			$('#cboSubType_debit').html('');
			$('#cboChartOfAccount_debit').html('');
			break;
		case 'financeType_credit':
			$('#cboMainType_credit').html('');
			$('#cboSubType_credit').html('');
			$('#cboChartOfAccount_credit').html('');
			break;	
		case 'mainType' :
			$('#cboSubType').html('');
			$('#cboChartOfAccount').html('');
			break;
		case 'mainType_debit' :
			$('#cboSubType_debit').html('');
			$('#cboChartOfAccount_debit').html('');
			break;
		case 'mainType_credit' :
			$('#cboSubType_credit').html('');
			$('#cboChartOfAccount_credit').html('');
			break;			
		case 'subType' :
			$('#cboChartOfAccount').html('');	
			break;
		case 'mainType_debit' :
			$('#cboChartOfAccount_debit').html('');
			break;
		case 'mainType_credit' :
			$('#cboChartOfAccount_credit').html('');
			break;
	}
}

function TEST()
{
	if($(this).val()=="" || $(this).val()=="0"){
		$('.cls_debit').html('');
		$('.cls_credit').html('');		
		$('#fDebit').hide('slow');
		$('#fCredit').hide('slow');
	}else{
		$('#fDebit').show('fast');
		$('#fCredit').show('fast');
	}
}

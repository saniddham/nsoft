<?php 
session_start();
$backwardseperator = "../../../../../";
include "{$backwardseperator}dataAccess/Connector.php";
$mainPath 	= $_SESSION['mainPath'];
$userId 	= $_SESSION['userId'];

$response 	= array('type'=>'', 'msg'=>'');

/////////// parameters /////////////////////////////
$requestType 			= $_REQUEST['requestType'];
$id 					= $_REQUEST['cboSearch'];
$mainCatId				= $_REQUEST['cboMainCategory'];
$code					= trim($_REQUEST['txtCode']);
$name					= trim($_REQUEST['txtName']);
$remark					= trim($_REQUEST['txtRemark']);	
$chartOfAccount 		= $_REQUEST['cboChartOfAccount'];
$chartOfAccount_depDr 	= ($_REQUEST['cboChartOfAccount_depDr']==""?'null':$_REQUEST['cboChartOfAccount_depDr']);
$chartOfAccount_depCr 	= ($_REQUEST['cboChartOfAccount_depCr']==""?'null':$_REQUEST['cboChartOfAccount_depCr']);
$depRate		= $_REQUEST["txtDepreciationRate"];
$intStatus				= ($_REQUEST['chkActive']?1:0);
$sendDetails            =  ($_REQUEST['chkSendDetails']?1:0);

if($requestType=='add')
{
	global $booSaved;
	global $errorMsg;
	global $errorSql;
	 
	$booSaved	= true;	
	$db->begin();
	
	$insertId = Insert($code,$name,$mainCatId,$remark,$depRate,$sendDetails);

	saveSubCatChartOfAccount($insertId,$chartOfAccount,$chartOfAccount_depDr,$chartOfAccount_depCr);
	
	if($booSaved){
		$db->commit();
		$response['type'] 		= 'pass';
		$response['msg'] 		= 'Saved successfully.';
	}
	else{
		$db->rollback();
		$response['type'] 		= 'fail';
		$response['msg'] 		= $errorMsg;
		$response['errorSql']	= $errorSql;
	}
	echo json_encode($response);
}
else if($requestType=='edit')
{
	global $booSaved;
	global $errorMsg;
	global $errorSql;
	 
	$booSaved	= true;	
	$db->begin();
	
	Update($id,$code,$name,$mainCatId,$remark,$depRate,$intStatus,$sendDetails);
	
	$result = saveSubCatChartOfAccount($id,$chartOfAccount,$chartOfAccount_depDr,$chartOfAccount_depCr);
	
	if(($booSaved)){
		$db->commit();
		$response['type'] 		= 'pass';
		$response['msg'] 		= 'Updated successfully.';
	}
	else{
		$db->rollback();
		$response['type'] 		= 'fail';
		$response['msg'] 		= $errorMsg;
		$response['errorSql']	= $errorSql;
	}
	echo json_encode($response);
}
else if($requestType=='sendDetails'){
    global $booSaved;
    $db->begin();

    $sql = "Update mst_subcategory set intSendDetails = '$sendDetails' WHERE (`intId`='$id')";
    $result = $db->RunQuery2($sql);

    if(($result)){
        $db->commit();
        $response['type'] 		= 'pass';
        $response['msg'] 		= 'Changed successfully';
    }
    else{
        $db->rollback();
        $response['type'] 		= 'fail';
        $response['msg'] 		= $db->errormsg;
        $response['q'] 			=$sql;
    }
    echo json_encode($response);

}
else if($requestType=='delete')
{
	$db->begin();

	$sql = "DELETE FROM `mst_subcategory` WHERE (`intId`='$id')  ";
	$result = $db->RunQuery2($sql);
	
	$sql = "DELETE FROM finance_mst_chartofaccount_subcategory 
			WHERE SUBCATEGORY_ID = '$id' ";
	$result = $db->RunQuery2($sql);
	
	if(($result)){
		$db->commit();
		$response['type'] 		= 'pass';
		$response['msg'] 		= 'Deleted successfully.';
	}
	else{
		$db->rollback();
		$response['type'] 		= 'fail';
		$response['msg'] 		= $db->errormsg;
		$response['q'] 			=$sql;
	}
	echo json_encode($response);
}
	
function Insert($code,$name,$mainCatId,$remarks,$depRate,$sendDetails)
{
	global $db;
	global $userId;
	global $booSaved;
	global $errorMsg;
	global $errorSql;
	
	$sql = "INSERT INTO `mst_subcategory` (`strCode`,`strName`,intMainCategory,`strRemark`,`dblDepreciationRate`,`intCreator`,dtmCreateDate, intSendDetails) 
				VALUES ('$code','$name','$mainCatId','$remarks',$depRate,'$userId',now(),$sendDetails)";
	$result = $db->RunQuery2($sql);
	if(!$result && $booSaved)
	{
		$booSaved	= false;
		$errorMsg	= $db->errormsg;
		$errorSql	= $sql;
	}
	else
		return $db->insertId;
}

function Update($id,$code,$name,$mainCatId,$remark,$depRate,$intStatus,$sendDetails)
{
	global $db;
	global $userId;
	global $booSaved;
	global $errorMsg;
	global $errorSql;
	
	$sql = "UPDATE `mst_subcategory` SET 	strCode = '$code',
										strName = '$name',
										intMainCategory = '$mainCatId',
										strRemark = '$remark',
										dblDepreciationRate = $depRate,
										intStatus = '$intStatus',
										intModifyer	= '$userId',
										intSendDetails = '$sendDetails'
			WHERE (`intId`='$id')";

	$result = $db->RunQuery2($sql);
	if(!$result && $booSaved)
	{
		$booSaved	= false;
		$errorMsg	= $db->errormsg;
		$errorSql	= $sql;
	}
}

function saveSubCatChartOfAccount($subCatId,$chartOfAccount,$chartOfAccount_depDr,$chartOfAccount_depCr)
{
	global $db;
	global $booSaved;
	global $errorMsg;
	global $errorSql;
	
	$sql = "DELETE FROM finance_mst_chartofaccount_subcategory 
			WHERE SUBCATEGORY_ID = '$subCatId' ";
	$result = $db->RunQuery2($sql);	
	if(!$result && $booSaved)
	{
		$booSaved	= false;
		$errorMsg	= $db->errormsg;
		$errorSql	= $sql;
	}	
	
	$sql = "INSERT INTO finance_mst_chartofaccount_subcategory 
			(
			SUBCATEGORY_ID, 
			CHART_OF_ACCOUNT_ID,
			DEPRECIATION_DEBIT_ID,
			DEPRECIATION_CREDIT_ID
			)
			VALUES
			(
			'$subCatId', 
			'$chartOfAccount',
			$chartOfAccount_depDr,
			$chartOfAccount_depCr) ";
	$result = $db->RunQuery2($sql);
	if(!$result && $booSaved)
	{
		$booSaved	= false;
		$errorMsg	= $db->errormsg;
		$errorSql	= $sql;
	}
}
?>
<?php
session_start();
$backwardseperator = "../../../../../";
include "{$backwardseperator}dataAccess/Connector.php";
include  "../../../../../class/finance/masterData/chartOfAccount/cls_chartofaccount_get.php";
$mainPath 		= $_SESSION['mainPath'];
$userId 		= $_SESSION['userId'];
$requestType 	= $_REQUEST['requestType'];

$obj_chartofaccount_get	= new Cls_ChartOfAccount_Get($db);
	
/////////// location load part /////////////////////
if($requestType=='loadCombo')
{
	$sql = "SELECT
				intId,
				strName
			FROM mst_subcategory
			order by strName
			";
	$result = $db->RunQuery($sql);
	$html = "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($result))
	{
		$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
	}
	echo $html;
}
else if($requestType=='loadDetails')
{
	$id  = $_REQUEST['id'];
	$sql = "SELECT
				strCode,
				strName,
				intMainCategory,
				strRemark,
				dblDepreciationRate,
				intStatus,
				intSendDetails
			FROM mst_subcategory
			WHERE
				intId =  '$id'";
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$response['code'] 				= $row['strCode'];
		$response['name'] 				= $row['strName'];
		$response['intMainCategory'] 	= $row['intMainCategory'];
		$response['remark'] 			= $row['strRemark'];
		$response['depreciationRate'] 	= $row['dblDepreciationRate'];
		$response['status']				= ($row['intStatus']?true:false);
        $response['sendDetails']		= ($row['intSendDetails']?true:false);

	}
	
	$response['ledger']				= false;
	$result = GetCOA($id,'CHART_OF_ACCOUNT_ID');
	while($rowCOA = mysqli_fetch_array($result))
	{
		$response['chartOfAccId'] 		= $rowCOA['CHART_OF_ACCOUNT_ID'];
		$response['subType'] 			= $rowCOA['SUB_TYPE_ID'];
		$response['mainType'] 			= $rowCOA['MAIN_TYPE_ID'];
		$response['financeType'] 		= $rowCOA['FINANCE_TYPE_ID'];
		$response['ledger']				= true;
	}
	
	$response['ledger_dr']			= false;
	$result = GetCOA($id,'DEPRECIATION_DEBIT_ID');
	while($rowCOA = mysqli_fetch_array($result))
	{
		$response['chartOfAccId_dr'] 	= $rowCOA['CHART_OF_ACCOUNT_ID'];
		$response['subType_dr'] 		= $rowCOA['SUB_TYPE_ID'];
		$response['mainType_dr'] 		= $rowCOA['MAIN_TYPE_ID'];
		$response['financeType_dr'] 	= $rowCOA['FINANCE_TYPE_ID'];
		$response['ledger_dr']			= true;
	}
	
	$response['ledger_cr']			= false;
	$result = GetCOA($id,'DEPRECIATION_CREDIT_ID');
	while($rowCOA = mysqli_fetch_array($result))
	{
		$response['chartOfAccId_cr'] 	= $rowCOA['CHART_OF_ACCOUNT_ID'];
		$response['subType_cr'] 		= $rowCOA['SUB_TYPE_ID'];
		$response['mainType_cr'] 		= $rowCOA['MAIN_TYPE_ID'];
		$response['financeType_cr'] 	= $rowCOA['FINANCE_TYPE_ID'];
		$response['ledger_cr']			= true;		
	}	
	echo json_encode($response);
}
//------------------------------------- Add by lahiru ---------------------------
else if($requestType=='loadMainType')
{
	$financeType	= $_REQUEST['financeType'];

	$response['mainTypeCombo']	= $obj_chartofaccount_get->loadMainTypeCombo($financeType);
	
	echo json_encode($response);
}
else if($requestType=='loadSubType')
{
	$mainType	= $_REQUEST['mainType'];
	
	$response['subTypeCombo']	= $obj_chartofaccount_get->loadSubTypeCombo($mainType);
	
	echo json_encode($response);
}
else if($requestType=='loadChartOfAccount')
{
	$subType	= $_REQUEST['subType'];
	
	$response['chrtOfAccCombo'] = $obj_chartofaccount_get->loadSearchCombo($subType);
	
	echo json_encode($response);
}

function GetCOA($id,$coa)
{
	global $db;
	
	$sql = "SELECT 
				COA.CHART_OF_ACCOUNT_ID,
				COA.SUB_TYPE_ID,
				MT.MAIN_TYPE_ID,
				FT.FINANCE_TYPE_ID
			FROM finance_mst_chartofaccount COA
			INNER JOIN  finance_mst_chartofaccount_subcategory COASC ON COASC.$coa = COA.CHART_OF_ACCOUNT_ID
			INNER JOIN finance_mst_account_sub_type ST ON ST.SUB_TYPE_ID = COA.SUB_TYPE_ID
			INNER JOIN finance_mst_account_main_type MT ON MT.MAIN_TYPE_ID = ST.MAIN_TYPE_ID
			INNER JOIN finance_mst_account_main_type FT ON FT.FINANCE_TYPE_ID = MT.FINANCE_TYPE_ID
			WHERE COASC.SUBCATEGORY_ID = '$id'
			GROUP BY COA.CHART_OF_ACCOUNT_ID";
			//echo $sql;
	return $db->RunQuery($sql);
}	
?>
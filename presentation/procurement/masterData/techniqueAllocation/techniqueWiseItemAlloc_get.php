<?php
session_start();
$backwardseperator 	= "../../../../";
$mainPath 			= $_SESSION['mainPath'];
$userId 			= $_SESSION['userId'];
$requestType 		= $_REQUEST['requestType'];

include "{$backwardseperator}dataAccess/Connector.php";

if($requestType=='loadSubCat')
{
	$mainCat = $_REQUEST['mainCat'];
	
	$sql = "SELECT
			mst_subcategory.intId,
			mst_subcategory.strCode,
			mst_subcategory.strName
			FROM mst_subcategory
			WHERE
			mst_subcategory.intMainCategory =  '$mainCat' 
			ORDER BY mst_subcategory.strName ASC";
	
	$result = $db->RunQuery($sql);
	$html = "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($result))
	{
		$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
	}
	$response['html'] = $html ;
	echo json_encode($response);	
}
else if($requestType=='loadItems')
{
	$mainCategory 	= $_REQUEST['mainCategory'];
	$subCategory 	= ($_REQUEST['subCategory']=='null'?'':$_REQUEST['subCategory']);
	$description 	= $_REQUEST['description'];
	
	$sql = "SELECT DISTINCT
			mst_item.intId,
			mst_item.intMainCategory,
			mst_item.intSubCategory,
			mst_item.strCode,
			mst_item.strName AS itemName,
			mst_maincategory.strName AS mainCatName,
			mst_subcategory.strName AS subCatName 
			FROM
				mst_item
			INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
			INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
			WHERE  
			mst_item.intStatus = '1' "; 
	
	if($mainCategory!='')
		$sql.="AND mst_item.intMainCategory='$mainCategory' ";
	if($subCategory!='')
		$sql.="AND mst_item.intSubCategory='$subCategory' ";
	if($description!='')
		$sql.="AND mst_item.strName LIKE '%$description%' ";
	
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$data['itemId'] 		= $row['intId'];
		$data['maincatId'] 		= $row['intMainCategory'];
		$data['subCatId'] 		= $row['intSubCategory'];
		$data['code'] 			= $row['strCode'];
		$data['itemName'] 		= $row['itemName'];
		$data['mainCatName'] 	= $row['mainCatName'];
		$data['subCatName'] 	= $row['subCatName'];
		
		$arrDetailData[] 		= $data;
	}
	$response['arrDetailData'] 	= $arrDetailData;
	echo json_encode($response);
}
else if($requestType=='loadSavedData')
{
	$technique 	= $_REQUEST['technique'];
	
	$sql = "SELECT ITEM_ID,strName
			FROM mst_techniques_wise_item
			INNER JOIN mst_item ON mst_item.intId=mst_techniques_wise_item.ITEM_ID
			WHERE mst_techniques_wise_item.TECHNIQUE_ID='$technique' ";
	
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$data['itemId'] 		= $row['ITEM_ID'];
		$data['itemName'] 		= $row['strName'];
		
		$arrDetailData[] 		= $data;
	}
	$response['arrDetailData'] 	= $arrDetailData;
	echo json_encode($response);
}
?>
<?php
session_start();
$backwardseperator 	= "../../../../";
$mainPath 			= $_SESSION['mainPath'];
$userId 			= $_SESSION['userId'];
$requestType 		= $_REQUEST['requestType'];

include "{$backwardseperator}dataAccess/Connector.php";

$savedStatus			= true;
$savedMasseged 			= '';
$error_sql				= '';

if($requestType=='saveData')
{
	$technique	= $_REQUEST['technique'];
	$arr 		= json_decode($_REQUEST['itemDetails'], true);
	
	$db->begin();
	
	//$deleteTeqArr	= deleteData($technique);
	
	
/*	if($deleteTeqArr['savedStatus']=='fail' && $savedStatus)
	{
		$savedStatus	= false;
		$savedMasseged 	= $deleteTeqArr['savedMassege'];
		$error_sql		= $deleteTeqArr['error_sql'];	
	}
*/	$to_save		=0;
	$saved_count	=0;
	foreach($arr as $array_loop)
	{
		$itemId			= $array_loop['itemId'];
		$exist	=checkExistSaved($technique,$itemId);
		if($exist==0){
			$to_save++;
			
			$saveResultArr 	= saveData($technique,$itemId);
			if($saveResultArr['savedStatus']=='fail' && $savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged 	= $saveResultArr['savedMassege'];
				$error_sql		= $saveResultArr['error_sql'];	
			}
			else{
				$saved_count++;
			}
		}
	}
	if($saved_count != $to_save){
		$db->rollback();
		$response['type'] 			= "fail";
		$response['msg'] 			= "Details saving error".$saved_count .'!='. $to_save.'/'.$error_sql;
		$response['sql']			= '';
	}
	else if($savedStatus)
	{
		$db->commit();
		$response['type'] 			= "pass";
		$response['msg'] 			= "Saved Successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 			= "fail";
		$response['msg'] 			= $savedMasseged;
		$response['sql']			= $error_sql;
	}
	echo json_encode($response);
}
else if($requestType=='deleteData')
{
	$savedStatus	= true;
	$technique		= $_REQUEST['technique'];
	$itemId			= $_REQUEST['itemId'];
	$flag			=0;
	
	$db->begin();
	
	$item_name		= getItemName($itemId);
	$exists_saved	= checkExistSaved($technique,$itemId);
	$deleteTeqArr	= deleteData($technique,$itemId);
	if($deleteTeqArr['savedStatus']=='fail' && $exists_saved==1)
	{
		$flag			=1;
		$savedStatus	= false;
		$savedMasseged 	= $deleteTeqArr['savedMassege'];
		$error_sql		= $deleteTeqArr['error_sql'];	
	}
	else if($deleteTeqArr['savedStatus']!='fail' && $exists_saved==1){
		$flag			=2;
		$savedStatus	= true;
		$savedMasseged 	= '"'.$item_name.'" deleted Successfully.';
	}
	else if($exists_saved==0)
	{
		$flag			=3;
		$savedStatus	= true;
		$savedMasseged 	= '"'.$item_name.'" deleted Successfully.';
	}
	

	if($savedStatus == false){
		$db->rollback();
		$response['type'] 			= "fail";
		$response['msg'] 			= "error";
		$response['sql']			= '';
	}
	else if($savedStatus && $flag > 0)
	{
		$db->commit();
		$response['type'] 			= "pass";
		$response['msg'] 			= $savedMasseged;
	}
	else
	{
		$db->rollback();
		$response['type'] 			= "fail";
		$response['msg'] 			= $savedMasseged/*.'-'. $deleteTeqArr['savedStatus'].'-'.$exists_saved*/;
		$response['sql']			= $error_sql;
	}
	echo json_encode($response);
}


function deleteData($technique,$itemId)
{
	global $db;
	
	$sql = "DELETE FROM mst_techniques_wise_item 
			WHERE
			TECHNIQUE_ID = '$technique'  and 
			ITEM_ID = '$itemId' ";
	
	$result = $db->RunQuery2($sql);
	if(!$result)
	{
		$data['savedStatus']	= 'fail';
		$data['savedMassege']	= $db->errormsg;
		$data['error_sql']		= $sql;
	}
	return $data;
}
function saveData($technique,$itemId)
{
	global $db;
	
	$sql = "INSERT INTO mst_techniques_wise_item 
			(
			TECHNIQUE_ID, 
			ITEM_ID
			)
			VALUES
			(
			'$technique', 
			'$itemId'
			)";
	
	$result = $db->RunQuery2($sql);
	if(!$result)
	{
		$data['savedStatus']	= 'fail';
		$data['savedMassege']	= $db->errormsg;
		$data['error_sql']		= $sql;
	}
	return $data;
}

function checkExistSaved($technique,$itemId)
{
	global $db;
	
	$sql 		= "select * from mst_techniques_wise_item 
					WHERE
					TECHNIQUE_ID = '$technique' and 
					ITEM_ID = '$itemId' ";
	
	$result 	= $db->RunQuery2($sql);
	$rowCount	=mysqli_num_rows($result);
	if($rowCount > 0)
	{
		return 1;
 	}
	else
		return 0;
}

 function getItemName($itemId)
{
	global $db;
	
	$sql 		= "SELECT
					mst_item.strName
					FROM `mst_item`
					WHERE
					mst_item.intId = '$itemId' ";
	
	$result 	= $db->RunQuery2($sql);
	$row=mysqli_fetch_array($result);
	return $row['strName'];
}


?>
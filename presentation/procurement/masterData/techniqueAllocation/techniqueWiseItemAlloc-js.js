// JavaScript Document
var basePath = "presentation/procurement/masterData/techniqueAllocation/";
$(document).ready(function() {
	$("#frmTechniqueWiseItemAlloc #butNew").die('click').live('click',clearForm);
	$("#frmTechniqueWiseItemAlloc").validationEngine();
	$('#frmTechniqueWiseItemAlloc #butAdd').die('click').live('click',addNewItem);
	$('#frmTechniqueAllocPopUp #cboMainCategory').die('change').live('change',loadSubCat);
	$('#frmTechniqueAllocPopUp #btnSearch').die('click').live('click',loadItems);
	$('#frmTechniqueAllocPopUp #butAdd').die('click').live('click',addClickedRows);
	$('#frmTechniqueWiseItemAlloc #butSave').die('click').live('click',saveData);
	$('#frmTechniqueWiseItemAlloc #cboTechnique').die('change').live('change',loadData);
	$('#frmTechniqueWiseItemAlloc .removeRow').die('click').live('click',removeRow);
	$('#frmTechniqueAllocPopUp #chkAll').die('click').live('click',checkAll);
	$('#frmTechniqueWiseItemAlloc .removeRow').die('click').live('click',deleteRow);
	
});

function clearForm()
{
	$('#frmTechniqueWiseItemAlloc #cboTechnique').val('');
	$("#frmTechniqueWiseItemAlloc #tblMain tr:gt(1)").remove();
}

function addNewItem()
{
	if($("#frmTechniqueWiseItemAlloc").validationEngine('validate'))
	{
		$('#popupContact1').html('');
		popupWindow3('1');
		$('#popupContact1').load(basePath+'popUp.php',function(){
			
				$('#butPopClose').die('click').live('click',disablePopup);
		});
	}
}
function loadSubCat()
{
	var mainCat	= $(this).val();
	if(mainCat=='')
	{
		$('#frmTechniqueAllocPopUp #cboSubCategory').html('');
		return;
	}
	
	var url = basePath+"techniqueWiseItemAlloc_get.php?requestType=loadSubCat";
		$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				data:'&mainCat='+mainCat,
				async:false,
				success:function(json){
					
					$('#frmTechniqueAllocPopUp #cboSubCategory').html(json.html);
				},
				error:function(xhr,status){
						
				}		
		});
}
function loadItems()
{
	$("#tblItemsPopup tr:gt(0)").remove();
	
	var mainCategory 	= $('#cboMainCategory').val();
	var subCategory 	= $('#cboSubCategory').val();
	var description 	= $('#txtItmDesc').val();
	
	var url 			= basePath+"techniqueWiseItemAlloc_get.php?requestType=loadItems";
	
	$.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:"mainCategory="+mainCategory+"&subCategory="+subCategory+"&description="+description,
			async:false,
			success:function(json){
	
				var length 			= json.arrDetailData.length;
				var arrDetailData 	= json.arrDetailData;
	
				for(var i=0;i<length;i++)
				{
					var itemId		= arrDetailData[i]['itemId'];	
					var maincatId	= arrDetailData[i]['maincatId'];	
					var subCatId	= arrDetailData[i]['subCatId'];	
					var code		= arrDetailData[i]['code'];	
					var itemName	= arrDetailData[i]['itemName'];	
					var mainCatName	= arrDetailData[i]['mainCatName'];	
					var subCatName	= arrDetailData[i]['subCatName'];	
						
					var content='<tr class="normalfnt" id="'+itemId+'"><td align="center" bgcolor="#FFFFFF"><input type="checkbox" id="chkDisp" class="clsChkDisp" /></td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'" class="mainCatNameP">'+mainCatName+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'" class="subCatNameP">'+subCatId+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+code+'" class="codeP">'+code+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="itemP">'+itemName+'</td></tr>';
					add_new_row('#frmTechniqueAllocPopUp #tblItemsPopup',content);
				}
					//checkAlreadySelected();
	
			}
		});
}
function add_new_row(table,rowcontent)
{
	if ($(table).length>0)
	{
		if ($(table+' > tbody').length==0) $(table).append('<tbody />');
		($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
	}
}
function checkAlreadySelected()
{
	$('#tblMain .item').each(function()
	{
		var itemNo	= $(this).parent().attr('id');
		
		$('#tblItemsPopup .itemP').each(function()
		{
			var itemNoP = $(this).attr('id');
			if(itemNo==itemNoP)
			{
				$(this).parent().parent().find('.clsChkDisp').attr('checked',true);
				$(this).parent().parent().find('.clsChkDisp').attr('disabled',true);
			}
		});
	});
}
function addClickedRows()
{
	$('.clsChkDisp:checked').each(function(){
		
		var fromId 	= $(this).parent().parent().attr('id') ;
		var found 	= false ;
		$('#tblMain .removeRow').each(function(){
			
			if(fromId == $(this).parent().parent().attr('id'))
			{
				found = true;
			}	
		});
		if(found==false)
		{
			document.getElementById('tblMain').insertRow(document.getElementById('tblMain').rows.length);
			document.getElementById('tblMain').rows[document.getElementById('tblMain').rows.length-1].innerHTML = "<td align=\"center\" bgcolor=\"#FFFFFF\">"+
			"<img class=\"mouseover removeRow\" src=\"images/del.png\" /></td>"+
			"<td bgcolor=\"#FFFFFF\" class=\"normalfnt item\">"+$(this).parent().parent().find('.itemP').html()+"</td>";
			document.getElementById('tblMain').rows[document.getElementById('tblMain').rows.length-1].id  = fromId;
			//setRemoveRow();
		}
	});
	disablePopup();
}
function removeRow()
{
	$(this).parent().parent().remove();
}
function saveData()
{
	showWaiting();
	var technique = $('#cboTechnique').val();
	
	if($('#frmTechniqueWiseItemAlloc').validationEngine('validate'))
	{
		value = "[ ";
		$('#tblMain .item').each(function(){
			
			var itemId	= $(this).parent().attr('id');
			value += '{"itemId": "'+itemId+'"},';
		});
		
		value = value.substr(0,value.length-1);
		value += " ]";
		
		var url = basePath+"techniqueWiseItemAlloc_set.php?requestType=saveData";
			var obj = $.ajax({
				url:url,
				dataType: "json",  
				data:'&technique='+technique+'&itemDetails='+value,
				async:false,
				type:'POST',
				success:function(json){
						$('#frmTechniqueWiseItemAlloc #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
						if(json.type=='pass')
						{
							var t = setTimeout("alertx()",3000);
							hideWaiting();
							return;
						}
						else
						{
							hideWaiting();
						}
					},
				error:function(xhr,status){
						
						$('#frmTechniqueWiseItemAlloc #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
						hideWaiting();
						return;
					}		
				});
	}
	hideWaiting();
}

function deleteRow(){
	var obj_r 		=this;
	var flag		=0;
	var alertMsg	= '';
	
	showWaiting();
	var technique = $('#cboTechnique').val();
	
	if($('#frmTechniqueWiseItemAlloc').validationEngine('validate'))
	{
		var itemId	= $(obj_r).parent().parent().attr('id');
 		
 		
		var url = basePath+"techniqueWiseItemAlloc_set.php?requestType=deleteData";
			var obj = $.ajax({
				url:url,
				dataType: "json",  
				data:'&technique='+technique+'&itemId='+itemId,
				async:false,
				type:'POST',
				success:function(json){
						$('#frmTechniqueWiseItemAlloc '+$(obj_r).attr('id')).validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
						if(json.type=='pass')
						{
							flag	=1;
							var t = setTimeout("alertx()",3000);
							hideWaiting();
							alertMsg	=json.msg;
							return;
						}
						else
						{
							hideWaiting();
						}
					},
				error:function(xhr,status){
						
						$('#frmTechniqueWiseItemAlloc '+$(obj_r).attr('id')).validationEngine('showPrompt', errormsg(xhr.status),'fail');
						hideWaiting();
						return;
					}		
				});
	}
	hideWaiting();
	
	if(flag==1){
	alert(alertMsg);
	$(obj_r).parent().parent().remove();
	}
	else{
	alert('Error');
	}
	

}


function loadData()
{
	showWaiting();
	
	$('#tblMain .removeRow').each(function(){
		var delRowCount = parseInt(document.getElementById('tblMain').rows.length);
		if(delRowCount>2)
			$(this).parent().parent().remove();
		
	});
		
	var technique = $('#cboTechnique').val();
	if(technique=='')
	{
		return;
	}
	
	var url = basePath+"techniqueWiseItemAlloc_get.php?requestType=loadSavedData";
	$.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:"technique="+technique,
			async:false,
			success:function(json){
				if(arrDetailData== null)
					hideWaiting();
	
				var length 			= json.arrDetailData.length;
				var arrDetailData 	= json.arrDetailData;
			
				for(var i=0;i<length;i++)
				{
					var itemId		= arrDetailData[i]['itemId'];	
					var itemName	= arrDetailData[i]['itemName'];	
					
					var content='<tr class="normalfnt" id="'+itemId+'"><td align=\"center\" bgcolor=\"#FFFFFF\"><img class=\"mouseover removeRow\" src=\"images/del.png\" /></td>';
					content +='<td bgcolor=\"#FFFFFF\" class=\"normalfnt item\">'+itemName+'</td>';
					add_new_row('#frmTechniqueWiseItemAlloc #tblMain',content);
				}
			}
			
		});
		
			hideWaiting();

}
function checkAll()
{
	if($(this).attr('checked'))
		$('.clsChkDisp').attr('checked',true);
	else
		$('.clsChkDisp').attr('checked',false);
}
function alertx()
{
	$('#frmTechniqueWiseItemAlloc #butSave').validationEngine('hide')	;
}
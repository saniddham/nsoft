<?php
session_start();
$backwardseperator 	= "../../../../";
$mainPath 			= $_SESSION['mainPath'];
$location 			= $_SESSION['CompanyID'];
$company 			= $_SESSION['headCompanyId'];
$thisFilePath 		=  $_SERVER['PHP_SELF'];

include $backwardseperator."dataAccess/Connector.php";

?>
<title>Items</title>
<!--<script type="text/javascript" src="presentation/procurement/masterData/techniqueAllocation/techniqueWiseItemAlloc-js.js"></script>
--><style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>
   
<form id="frmTechniqueAllocPopUp" name="frmTechniqueAllocPopUp" method="post" action="">
<div align="center">
	<div class="trans_layoutD">
	<div class="trans_text"> Item List</div>
	<table width="550" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#FFFFFF">
    <tr>
    	<td>
        <table width="550" border="0" cellpadding="0" cellspacing="0">
      	<tr>
        	<td>
            <table width="100%" border="0">
         	 <tr>
           	 	<td width="25%" height="22" class="normalfnt">Main Category</td>
            	<td width="49%"><select name="cboMainCategory" style="width:200px" id="cboMainCategory">
            	  <option value=""></option>
            	  <?php
					$sql = "SELECT
							mst_maincategory.intId,
							mst_maincategory.strName
							FROM mst_maincategory";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
          	  </select></td>
            	<td width="2%" class="normalfnt"><input type="text" id="txtPONo" value="<?php echo $poNo; ?>" style="display:none" /></td>
            	<td width="24%" align="right">&nbsp;</td>
            </tr>
            <tr>
            	<td height="22" class="normalfnt">Sub Category</td>
            	<td width="49%"><select name="cboSubCategory" style="width:200px" id="cboSubCategory">
          	  </select></td>
            	<td width="2%" class="normalfnt">&nbsp;</td>
            	<td width="24%" align="right">&nbsp;</td>
            </tr>
            <tr>
                <td height="22" class="normalfnt">Description Like</td>
                <td width="49%">
                  <input type="text" name="txtItmDesc" id="txtItmDesc"  style="width:100%" /></td>
                <td width="2%" class="normalfnt">&nbsp;</td>
                <td width="24%" align="left"><a class="button green small" id="btnSearch">Search</a></td>
            </tr>          
            </table>
         </td>
      </tr>
      <tr>
        <td><div style="width:600px;height:300px;overflow:scroll" >
          <table width="580" class="bordered" id="tblItemsPopup" >
          <thead>
            <tr>
              <th width="5%" height="22" ><input type="checkbox" name="chkAll" id="chkAll" /></th>
              <th width="23%" >Main Catogery</th>
              <th width="20%" >Sub Catogery</th>
              <th width="14%" >Item Code</th>
              <th width="38%" >Description</th>
              </tr>
         </thead>
         </table>
          </div></td>
      </tr>
      <tr>
        <td align="center" class="">
        <a  alt="add" id="butAdd" name="butAdd" class="button white medium">Add</a><a id="butPopClose" name="butClose1" class="button white medium">Close</a>
        </td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>

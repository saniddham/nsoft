<?php
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	
	/////////// location load part /////////////////////
	if($requestType=='loadCombo')
	{//$db->connect();
		$sql = "SELECT
					intId,
					strName
				FROM mst_maincategory
				order by strName
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
		//$db->disconnect();
	}
	else if($requestType=='loadDetails')
	{//$db->connect();
		$id  = $_REQUEST['id'];
		
		//---------------------------------------------------------------------------------------------------------------
		//Check for saved as a fixed asset (2014-12-04)------
		$sql_f 		= "SELECT ID FROM fixed_assets_registry INNER JOIN mst_item ON fixed_assets_registry.ITEM_ID = mst_item.intId
		WHERE mst_item.intMainCategory =  '$id' limit 1 ";
		$result_f 	= $db->RunQuery($sql_f);
		$row_f=mysqli_fetch_array($result_f);
		//---------------------------------------------------------------------------------------------------------------
		
		$sql = "SELECT
					strCode,
					strName,
					strRemark,
					intStatus,
					intSendDetails,
					FIXED_ASSET,
					intFinanceClass
				FROM mst_maincategory
				WHERE
					intId =  '$id'
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['code'] 	= $row['strCode'];
			$response['name'] 	= $row['strName'];
			$response['financeClass'] 	= $row['intFinanceClass'];
			$response['remark'] = $row['strRemark'];
			$response['status'] = ($row['intStatus']?true:false);
			$response['sendDetails'] = ($row['intSendDetails']?true:false);
			$response['statusFixedAsset'] = ($row['FIXED_ASSET']?true:false);
			if($row_f['ID'] != '' && $row['FIXED_ASSET']==true )
			$response['disable_fd'] 	= true;
			else
			$response['disable_fd'] 	= false;
			
		}
		echo json_encode($response);
		//$db->disconnect();
	}
	
?>
var basePath	= "presentation/procurement/masterData/mainCategory/addNew/";
			
$(document).ready(function() { 

  		$("#frmMainCategory").validationEngine();
		$('#frmMainCategory #butNew').die('click').live('click',clearForm);
		$('#frmMainCategory #txtCode').focus();
		
  //permision for add 
  /*if(intAddx)
  {
 	$('#frmMainCategory #butNew').show();
	$('#frmMainCategory #butSave').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmMainCategory #butSave').show();
	$('#frmMainCategory #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmMainCategory #butDelete').show();
	$('#frmMainCategory #cboSearch').removeAttr('disabled');
  }
  
  //permision for view
  if(intViewx)
  {
	$('#frmMainCategory #cboSearch').removeAttr('disabled');
  }*/
  
/*  $(':input').bind('keyup mouseup', function(){
      $(this).val($.trim($(this).val())) ;
    });*/

  ///save button click event
  $('#frmMainCategory #butSave').die('click').live('click',function(){
	//$('#frmMainCategory').submit();
	var requestType = '';
	if ($('#frmMainCategory').validationEngine('validate'))   
    { 
		showWaiting();
		//$('#txtLocationCode').validationEngine('showPrompt', 'This is an example', 'pass'); //hide all alerts//
		if($("#frmMainCategory #cboSearch").val()=='')
			requestType = 'add';
		else
			requestType = 'edit';
		var fa	=$('#frmMainCategory #chkFixedAsset').is(':checked');
		var url = basePath+"mainCategory-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json",  
			type:'POST',
			data:$("#frmMainCategory").serialize()+'&fixedAsset='+fa+'&requestType='+requestType,
			//data:'{"requestType":"addLocation"}',
			async:false,
			
			success:function(json){
					$('#frmMainCategory #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#frmMainCategory').get(0).reset();
						loadCombo_frmMainCategory();
						$('#frmMainCategory #chkFixedAsset').attr('disabled',false);
						var t=setTimeout("alertx()",1000);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					$('#frmMainCategory #butSave').validationEngine('showPrompt', errormsg(xhr.status), 'fail');
					var t=setTimeout("alertx()",4000);
					//function (xhr, status){errormsg(status)}
				}		
			});
		hideWaiting();
	}
   });
   
   /////////////////////////////////////////////////////
   //// load location details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmMainCategory #cboSearch').die('click').live('click',function(){
	   $('#frmMainCategory').validationEngine('hide');
   });
    $('#frmMainCategory #cboSearch').die('change').live('change',function(){
		$('#frmMainCategory').validationEngine('hide');
		var url = basePath+"mainCategory-db-get.php";
		if($('#frmMainCategory #cboSearch').val()=='')
		{
			$('#frmMainCategory').get(0).reset();return;	
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:'requestType=loadDetails&id='+$(this).val(),
			async:false,
			success:function(json){
					//json  = eval('('+json+')');
					$('#frmMainCategory #txtCode').val(json.code);
					$('#frmMainCategory #txtName').val(json.name);
					$('#frmMainCategory #txtRemark').val(json.remark);
					$('#frmMainCategory #cboFinanceClass').val(json.financeClass);
					if(json.status)
						$('#frmMainCategory #chkActive').attr('checked',true);
					else
						$('#frmMainCategory #chkActive').removeAttr('checked');
					if(json.sendDetails)
						$('#frmMainCategory #chkSendDetails').attr('checked',true);
					else
						$('#frmMainCategory #chkSendDetails').removeAttr('checked');
					if(json.statusFixedAsset){
						$('#frmMainCategory #chkFixedAsset').attr('checked',true);
						if(json.disable_fd)
							$('#frmMainCategory #chkFixedAsset').attr('disabled',true);
						else
							$('#frmMainCategory #chkFixedAsset').attr('disabled',false);
					}
					else{
						$('#frmMainCategory #chkFixedAsset').removeAttr('checked');
						$('#frmMainCategory #chkFixedAsset').attr('disabled',false);
					}
						
			}
	});
	//////////// end of load details /////////////////
	
	});
	
	$('#frmMainCategory #butNew').die('click').live('click',function(){
		$('#frmMainCategory').get(0).reset();
		loadCombo_frmMainCategory();
		$('#frmMainCategory #txtCode').focus();
	});
    $('#frmMainCategory #butDelete').die('click').live('click',function(){
		if($('#frmMainCategory #cboSearch').val()=='')
		{
			$('#frmMainCategory #butDelete').validationEngine('showPrompt', 'Please select Main Category.', 'fail');
			//var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmMainCategory #txtName').val()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = basePath+"mainCategory-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											type:'POST',
											data:'requestType=delete&cboSearch='+$('#frmMainCategory #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmMainCategory #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												//var t=setTimeout("alertDelete()",1000);
												if(json.type=='pass')
												{
													$('#frmMainCategory').get(0).reset();
													loadCombo_frmMainCategory();
												}	
											}	 
										});
									}
				}
		 	});
			
		}
	});
});

$('#frmMainCategory').die('click').live('click',function(){
		$('#frmMainCategory #butDelete').validationEngine('hide')	;
});

function loadCombo_frmMainCategory()
{
	var url 	= basePath+"mainCategory-db-get.php?requestType=loadCombo";
	var httpobj = $.ajax({url:url,async:false})
	$('#cboSearch').html(httpobj.responseText);
}

function alertx()
{
	$('#frmMainCategory #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmMainCategory #butDelete').validationEngine('hide')	;
}
function clearForm()
{
	$('#frmMainCategory').get(0).reset();
}
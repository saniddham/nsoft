<?php 
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	$id 			= $_REQUEST['cboSearch'];
	$code			= trim($_REQUEST['txtCode']);
	$name			= trim($_REQUEST['txtName']);
	$intFinanceClass= ($_REQUEST['cboFinanceClass']);
	$remarks		= trim($_REQUEST['txtRemark']);
	$intStatus		= ($_REQUEST['chkActive']?1:0);
	$sendDetails    = ($_REQUEST['chkSendDetails']?1:0);
	//$statusFixedAsset		= ($_REQUEST['chkFixedAsset']?1:0);	
	//echo $_REQUEST['fixedAsset'];
	if($_REQUEST['fixedAsset']=='false')
		$statusFixedAsset	=0;
	else
		$statusFixedAsset	=1;
	
 	//$db->connect();
	/////////// location insert part /////////////////////
	if($requestType=='add')
	{
		$sql = "INSERT INTO `mst_maincategory` (`strCode`,`strName`,intFinanceClass,`strRemark`,`intCreator`,dtmCreateDate,FIXED_ASSET,`intSendDetails`) 
				VALUES ('$code','$name',$intFinanceClass,'$remarks','$userId',now(),$statusFixedAsset,$sendDetails)";
		$result = $db->RunQuery($sql);
		if($result){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		
	}
	/////////// location update part /////////////////////
	else if($requestType=='edit')
	{
		$sql = "UPDATE `mst_maincategory` SET 	strCode		='$code',
											strName		='$name',
											intFinanceClass = $intFinanceClass,
											strRemark	='$remarks',
											intStatus	='$intStatus',
											FIXED_ASSET	='$statusFixedAsset',
											intModifyer	='$userId',
											intSendDetails = '$sendDetails'
				WHERE (`intId`='$id')";
		$result = $db->RunQuery($sql);
		
		//Check for saved as a fixed asset (2014-12-04)------
		$sql_f 		= "SELECT ID FROM fixed_assets_registry INNER JOIN mst_item ON fixed_assets_registry.ITEM_ID = mst_item.intId
		WHERE mst_item.intMainCategory =  '$id' limit 1 ";
		$result_f 	= $db->RunQuery($sql_f);
		$row_f=mysqli_fetch_array($result_f);
		//---------------------------------------------------
		//echo $row_f['ID']." && ".$statusFixedAsset." !=1";
//		if(($row_f['ID']) && $statusFixedAsset !=1){
//			$response['type'] 		= 'fail';
//			$response['msg'] 		= 'This should be mark as fixed asset since there are saved fixed assets under this category.';
//		}
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Updated successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	/////////// location delete part /////////////////////
	else if($requestType=='delete')
	{
		$sql = "DELETE FROM `mst_maincategory` WHERE (`intId`='$id')  ";
		$result = $db->RunQuery($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Deleted successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	echo json_encode($response);
	//$db->connect();
?>
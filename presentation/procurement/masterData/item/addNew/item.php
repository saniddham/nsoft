<?php
(define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');
?>

<title>Item Creation</title>
<form id="frmItem" name="frmItem" method="post" >
<div align="center">
		<div class="trans_layoutD">
		  <div class="trans_text">Item Creation</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="585" border="0" align="center">
      <tr>
        <td width="62%"><table width="100%" border="0" > 
          <tr>
            <td height="47" ><table width="100%" border="0" class="">
              <tr>
                <td width="1" class="normalfnt">&nbsp;</td>
                <td width="925" colspan="3" align="left" valign="middle" ><table width="100%" border="0" class="tableBorder">
                  <tr>
                    <td width="12%">&nbsp;</td>
                    <td width="19%" class="normalfnt">Main Category<span class="compulsoryRed"> *</span></td>
                    <td width="69%" colspan="3" class="normalfnt"><select class="validate[required]" name="cboMainCategory"  id="cboMainCategory"   style="width:300px" >
                      <option value=""></option>
                      <?php  $sql = "SELECT
										intId,
										strName
									FROM mst_maincategory
									WHERE intStatus=1
										order by strName";
										
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
								}
                   ?>
                      </select>
                      <img src="images/add_new.png" name="butViewMainCategory" class="mouseover" id="butViewMainCategory"  /></td>
                  </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td class="normalfnt">Sub Category <span class="compulsoryRed">*</span></td>
                    <td colspan="3" class="normalfnt"><select class="validate[required]" name="cboSubCategory"  id="cboSubCategory"   style="width:300px" tabindex="1">
                      </select>
                      <img src="images/add_new.png" name="butViewSubCategory" class="mouseover" id="butViewSubCategory"  /></td>
                    </tr>
                  <tr>
                    <td>&nbsp;</td>
                    <td class="normalfnt">Item</td>
                    <td colspan="3" class="normalfnt"><select name="cboSearch" class="txtbox" id="cboSearch"   style="width:300px" tabindex="1">
                    <option value=""></option>
                      </select></td>
                  </tr>
                  </table>
                 
                  </td>
              </tr>
             
              <tr>
                <td class="normalfnt">&nbsp;</td>
                <td colspan="4" class="normalfnt"><table width="100%" border="0" class="tableBorder">
                  <tr>
                    <td colspan="6" align="left" bgcolor="#E4E4E4" class="normalfnt"><strong>General</strong></td>
                    </tr>
                  <tr class="">
                    <td class="normalfnt"><span class="normalfnt">Item Code&nbsp;</span><span class="compulsoryRed">*</span></td>
                    <td colspan="4"><span class="normalfnt">
                      <input  name="txtCode" type="text" class="validate[required,maxSize[50]]" id="txtCode" style="width:240px" />
                    </span></td>
                    <td ></td>
                    </tr>
                  <tr class="">
                    <td class="normalfnt"><span class="normalfnt">Item Description&nbsp;</span><span class="compulsoryRed">*</span></td>
                    <td colspan="4"><span class="normalfnt">
                      <input name="txtName" class="validate[required,maxSize[100]]" type="text"  id="txtName" style="width:360px" tabindex="3"/>
                      </span></td>
                      <td></td>
                    </tr>
                  <tr class="">
                    <td class="normalfnt"><span class="normalfnt"> UOM&nbsp;<span class="compulsoryRed">*</span></span></td>
                    <td ><span class="normalfnt">
                      <select name="cboUOM" class="validate[required]" tabindex="7" id="cboUOM" style="width:110px" >
                        <option value=""></option>
                         <?php  $sql = "SELECT
										intId,
										strName
									FROM mst_units
									WHERE intStatus=1
										order by strName";
										
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
								}
                   		?>
                        </select>
                      </span></td>
                    <td class="normalfnt">&nbsp;</td>
                    <td >&nbsp;</td>
                    <td >&nbsp;</td>
                    <td >&nbsp;</td>
                    </tr>
                  <tr class="">
                    <td width="26%" class="normalfnt"><span class="normalfnt">BOM Item</span></td>
                    <td width="7%"><input onFocus="this.blur();" type="checkbox" name="chkBomItem" id="chkBomItem" tabindex="24"/></td>
                    <td width="23%" class="normalfnt">Stores Trans Item</td>
                    <td width="12%"><input type="checkbox" name="chkStoresTrnsItm" id="chkStoresTrnsItm" tabindex="24"/></td>
                    <td width="14%" class="normalfnt">Special RM</td>
                    <td width="18%"><input type="checkbox" name="chkSpecialRm" id="chkSpecialRm" tabindex="24"/></td>
                    </tr>
                  </table></td>
              </tr>
			<tr id="tblFoil" <?php /*?>style="display:none"<?php */?>>
              	<td>&nbsp;</td>
                <td colspan="5"><table  width="100%" border="0" class="tableBorder">
                  <tr>
                    <td colspan="4" align="left" bgcolor="#E4E4E4" class="normalfnt"><strong>FOIL</strong></td>
                  </tr>
                  <tr>
                    <td width="22%"><span class="normalfnt">Item Width (Meters)<?php /*?><span class="compulsoryRed">*</span><?php */?><?php /*?><?php */?></span></td>
                    <td width="28%"><span class="normalfnt">
                      <input name="txtFoilWidth" tabindex="8" type="text" class="validate[custom[number]]" id="txtFoilWidth" style="width:100px" maxlength="30" />
                    </span></td>
                    <td width="28%" class="normalfnt">Item Height(Inch)<?php /*?><span class="compulsoryRed">*</span><?php */?></td>
                    <td width="22%"><span class="normalfnt">
                      <input name="txtFoilHeight" tabindex="8" type="text" class="validate[custom[number]]" id="txtFoilHeight" style="width:100px" maxlength="30" />
                    </span></td>
                  </tr>
                </table></td>
              </tr>
              <tr>
              	<td>&nbsp;</td>
                <td colspan="5"><table width="100%" border="0" class="tableBorder">
                  <tr>
                    <td colspan="4" align="left" bgcolor="#E4E4E4" class="normalfnt"><strong>Invoice</strong></td>
                  </tr>
                  <tr>
                    <td width="22%"><span class="normalfnt">Stock Valuation</span></td>
                    <td width="34%"><select name="cboStockValuation" tabindex="7" class="txtbox" id="cboStockValuation" style="width:110px" >
                      <option value="1" selected="selected">NONE</option>
                      <option value="2">FIFO</option>
                      <option value="3">Weight Average </option>
                    </select></td>
                    <td width="17%">&nbsp;</td>
                    <td width="27%">&nbsp;</td>
                  </tr>
                </table></td>
              </tr>
              <tr>
                <td rowspan="3" class="normalfnt">&nbsp;</td>
                <td colspan="4" class="normalfnt"><table width="100%" border="0" class="tableBorder">
                  <tr>
                    <td colspan="4" align="left" bgcolor="#E4E4E4" class="normalfnt"><strong>Purchase</strong></td>
                    </tr>
                  <tr>
                      <td width="21%"><span class="normalfnt">Perfect Supplier</span><span class="compulsoryRed">*</span></td>
                    <td width="79%" colspan="3"><span class="normalfnt">
                      <select name="cboPerfectSupplier" class="validate[required]" id="cboPerfectSupplier"  style="width:300px" tabindex="1">
                        <option value=""></option>
                      <?php  $sql = "SELECT
										intId,
										strName
									FROM mst_supplier
									WHERE intStatus=1
										order by strName";
										
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
								}
                   		?>
                      </select>
                    </span></td>
                    </tr>
                  <tr>
                    <td><span class="normalfnt">Alternative Supplier</span></td>
                    <td colspan="3"><span class="normalfnt">
                      <select name="cboAlternativeSupplier" class="txtbox" id="cboAlternativeSupplier"  style="width:300px" tabindex="1">
                      <option value=""></option>
                       <?php  $sql = "SELECT
										intId,
										strName
									FROM mst_supplier
									WHERE intStatus=1
										order by strName";
										
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
								}
                   		?>
                        </select>
                      </span></td>
                  </tr>
                  </table></td>
              </tr>
                            <tr>
                <td colspan="5" class="normalfnt"><table width="100%" border="0" class="tableBorder">
                  <tr>
                    <td colspan="4" align="left" bgcolor="#E4E4E4" class="normalfnt"><strong>Costing</strong></td>
                    </tr>
                  <tr>
                    <td width="21%"><span class="normalfnt">Default Item</span></td>
                    <td width="30%"><input type="checkbox" name="chkDefaultCostItem" id="chkDefaultCostItem" tabindex="24"/></td>
                    <td width="22%"><span class="normalfnt">Consumption Per Inch</span></td>
                    <td width="27%">                      <span class="normalfnt">
                      <input name="txtConsuptionPerInch" tabindex="8" type="text" class="validate[custom[number]]" id="txtConsuptionPerInch" style="width:140px" maxlength="30" />                    
                      </span></td>
                  </tr>
                  <tr>
                    <td><span class="normalfnt">Cost Per Inch</span></td>
                    <td>                      <span class="normalfnt">
                      <input name="txtCostPerInch" tabindex="8" type="text" class="validate[custom[number]]" id="txtCostPerInch" style="width:140px" maxlength="30" />                    
                      </span></td>
                    <td><span class="normalfnt">Last Price</span></td>
                    <td>                      <span class="normalfnt">
                      <input name="txtLastPrice" tabindex="8" type="text" class="validate[custom[number]]" id="txtLastPrice" style="width:140px" maxlength="30" />
                    </span></td>
                  </tr>
                  <tr>
                    <td class="normalfnt">Currency <span class="compulsoryRed">*</span></td>
                    <td><select  name="cboCurrency" class="validate[required]" id="cboCurrency"  style="width:140px;"  >
                      <option value=""></option>
                      <?php  $sql = "SELECT
						intId,
						strCode
						FROM mst_financecurrency
						order by strCode
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							echo "<option value=\"".$row['intId']."\">".$row['strCode']."</option>";
						}
        ?>
                    </select></td>
                    <td class="normalfnt">Technique <span class="compulsoryRed">*</span></td>
                    <td><select name="cboTechnique" class="validate[required]" id="cboTechnique" style="width:140px" tabindex="1">
                      <option value=""></option>
                      <?php  $sql = "SELECT
									T.intId,
									T.strName
								FROM mst_techniques T
								WHERE T.intStatus = 1
								ORDER BY T.strName";
								$result = $db->RunQuery($sql);
								while($row=mysqli_fetch_array($result))
								{
									echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
								}
                   ?>
                    </select></td>
                  </tr>
<tr>
                    <td class="normalfnt" id="depreciation">Depreciation Rate </td>
                    <td><input name="txtDepreciateRate" tabindex="8" type="text" class="validate[custom[number]]" id="txtDepreciateRate" style="width:140px" maxlength="30" /></td>
                    <td class="normalfnt">&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>                </table></td>
              </tr>
              <tr>
                <td colspan="5" class="normalfnt"><table width="100%" border="0" class="tableBorder">
                  <tr>
                    <td colspan="4" align="left" bgcolor="#E4E4E4" class="normalfnt"><strong>Material Planning Manager</strong></td>
                    </tr>
                  <tr>
                    <td width="21%"><span class="normalfnt">Re-Order Policy   </span></td>
                    <td width="30%"><span class="normalfnt">
                      <select name="cboReOrderPolicy" tabindex="7" class="txtbox" id="cboReOrderPolicy" style="width:140px" >
                        <option value="0"></option>
                        <option value="1">Fixed Reorder Qty</option>
                        <option value="2">Maximum Qty </option>
                        <option value="3">Order </option>
                        <option value="4">Lot for Lot</option>
</select>
                    </span></td>
                    <td width="22%"><span class="normalfnt">Reserve Policy</span></td>
                    <td width="27%"><span class="normalfnt">
                      <select name="cboReservePolicy" tabindex="7" class="txtbox" id="cboReservePolicy" style="width:140px" >
                        <option value="1">Optional</option>
                        <option value="2">Never</option>
                        <option value="3">Always</option>
                      </select>
                    </span></td>
                    </tr>
                  <tr>

                    <td><span class="normalfnt">Safety Lead Time</span></td>
                    <td><span class="normalfnt">
                      <input name="txtSafetyLeadTime" type="text" disabled="disabled" class="validate[custom[number]]" id="txtSafetyLeadTime" style="width:140px" tabindex="8" maxlength="30" />
                    </span></td>
                    </tr>
                  <tr>
                    <td><span class="normalfnt">Safety Stock Qty</span></td>
                    <td><span class="normalfnt">
                      <input name="txtSafetyStockQty" type="text" disabled="disabled" class="validate[custom[number]]" id="txtSafetyStockQty" style="width:140px" tabindex="8" maxlength="30" />
                    </span></td>
                    <td><span class="normalfnt">Re order Points</span></td>
                    <td><span class="normalfnt">
                      <input name="txtReOrderPoints" type="text" disabled="disabled" class="validate[custom[number]]" id="txtReOrderPoints" style="width:140px" tabindex="8" maxlength="30" />
                    </span></td>
                    </tr>
                  <tr>
                    <td style="display:none;"><span class="normalfnt">Re order Qty</span></td>
                    <td style="display:none;"><span class="normalfnt">
                      <input name="txtReOrderQty" type="text" disabled="disabled" class="validate[custom[number]]" id="txtReOrderQty" style="width:140px" tabindex="8" maxlength="30" />
                    </span></td>
                    <td><span class="normalfnt">Maximum Inventory</span></td>
                    <td><span class="normalfnt">
                      <input name="txtMaximumInventory" type="text" disabled="disabled" class="validate[custom[number]]" id="txtMaximumInventory" style="width:140px" tabindex="8" maxlength="30" />
                    </span></td>
                     <td><span class="normalfnt">Maximum Order Qty</span></td>
                    <td><span class="normalfnt">
                      <input name="txtMaximumOrderQty" type="text" disabled="disabled" class="validate[custom[number]]" id="txtMaximumOrderQty" style="width:140px" tabindex="8" maxlength="30" />
                    </span></td>
                    </tr>
                  <tr>
                   
                    <td><span class="normalfnt">Minimum Order Qty</span></td>
                    <td><span class="normalfnt">
                      <input name="txtMinimumOrderQty" type="text" disabled="disabled" class="validate[custom[number]]" id="txtMinimumOrderQty" style="width:140px" tabindex="8" maxlength="30" />
                    </span></td>
                    </tr>
                  </table>
                  <table width="100%" border="0" class="tableBorder">
                    <tr>
                      <td colspan="4" align="left" bgcolor="#E4E4E4" class="normalfnt"><strong>Other</strong></td>
                      </tr>
                    <tr>
                      <td width="21%"><span class="normalfnt">Blocked Purchase</span></td>
                      <td width="30%"><input type="checkbox" name="chkBlockedPurchase" id="chkBlockedPurchase" tabindex="24"/></td>
                      <td width="22%"><span class="normalfnt">Blocked GRN</span></td>
                      <td width="27%"><input type="checkbox" name="chkBlockGrn" id="chkBlockGrn" tabindex="24"/></td>
                      </tr>
                    <tr class="">
                      <td class="normalfnt"><span class="normalfnt">Active</span></td>
                      <td><input type="checkbox" name="chkActive" id="chkActive" tabindex="24"/></td>
                      <td class="normalfnt">Inspection</td>
                      <td><input type="checkbox" name="chkInspection" id="chkInspection" tabindex="24"/></td>
                      </tr>
                    <tr class="">
                      <td class="normalfnt">Expire Item</td>
                      <td><input type="checkbox" name="chkExpireItem" id="chkExpireItem" tabindex="24"/></td>
                      <td class="normalfnt">Auto Consumption</td>
                      <td><input type="checkbox" name="chkAutoCons" id="chkAutoCons" tabindex="24"/></td>
                    </tr>
                    <tr class="">
                      <td colspan="3" class="normalfnt">Order wise Production Issuing Item</td>
                      <td><input type="checkbox" name="chkNonOrder" id="chkNonOrder" tabindex="24"/></td>
                    </tr>
                    <tr class="">
                      <td colspan="3" class="normalfnt">Roll Form</td>
                      <td><input type="checkbox" name="chkRoll" id="chkRoll" tabindex="24"/></td>
                    </tr>
                    <tr class="">
                      <td colspan="3" class="normalfnt">Item Hide</td>
                      <td><input type="checkbox" name="chkItemHide" id="chkItemHide" tabindex="24"/></td>
                    </tr>
                    </table></td>
              </tr>
              </table></td>
            </tr>
          <tr>
            <td height="34"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
              <tr>
                <td width="100%" align="center" bgcolor=""><img style="display:" border="0" src="images/Tnew.jpg" alt="New" name="butNew" width="92" height="24"  class="mouseover" id="butNew" tabindex="28"/><?php
                if($form_permision['add']|| $form_permision['edit'])
				{
				?><img  style="display:" border="0" src="images/Tsave.jpg" alt="Save" name="butSave"width="92" height="24"  class="mouseover" id="butSave" tabindex="24"/><?php
				}
                if($form_permision['delete'])
				{
				?><img style="display:" border="0" src="images/Tdelete.jpg" alt="Delete" name="butDelete" width="92" height="24" class="mouseover" id="butDelete" tabindex="25"/><?Php 
				}
				?><a href="main.php"><img  src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0"  class="mouseover" id="butClose" tabindex="27"/></a></td>
                </tr>
              </table></td>
            </tr>
          </table></td>
        </tr>
      </table></td>
    </tr>
  </table>
	</div>
  </div>
	<!-- this is main category window -->
	<div  style="position: absolute;display:none;z-index:100"  id="popupContact1">
    <?php 
	
	 ?>
    <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="header_db.php?q=331&iframe=1&clearCash=1" style="width:800px;height:800px;border:0;overflow:hidden">
    
    </iframe>
    </div>

    	<!-- this is main category window -->
	<div style="position: absolute;display:none;z-index:100"  id="popupContact2">
    <iframe onload="loadSub();"   id="iframeMain2" name="iframeMain2" src="header_db.php?q=332&iframe=1&clearCash=1" style="width:800px;height:800px;border:0;overflow:scroll">
    </iframe>
    </div>
    
	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>

</form>

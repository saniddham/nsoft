<?php
	ini_set('display_errors',0);
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	
	/////////// location load part ////////////////////////
	if($requestType=='loadCombo')
	{
		$mainId  	= $_REQUEST['mainId'];
		$subId  	= $_REQUEST['subId'];
		
		$sql = "SELECT
					mst_item.intId,
					mst_item.strName
				FROM mst_item
				WHERE
					mst_item.intMainCategory 	=  '$mainId' AND
					mst_item.intSubCategory 	=  '$subId'
				ORDER BY
					mst_item.strName ASC
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
	}
	////////////////////////// load sub category //////////////////////
	if($requestType=='loadSubCategory')
	{
		$id  = $_REQUEST['id'];
		$sql = "SELECT
					intId,
					strName
				FROM mst_subcategory
				WHERE
					intMainCategory =  '$id'
				";
	//die ($sql);
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
	}
	else if($requestType=='loadDetails')
	{
		$id  = $_REQUEST['id'];
		$sql = "SELECT mst_item.* ,mst_maincategory.FIXED_ASSET,mst_supplier.strCode AS supplier_code  FROM mst_item 
		INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
		LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
 WHERE mst_item.intId =  '$id'";
               // echo $sql;
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			//mysqli_field_len(
			$response['code'] 						= $row['strCode'];
			$response['name'] 						= $row['strName'];
			$response['remark'] 					= $row['strRemark'];
            if(strpos($response['code'], $row['supplier_code']) !== false){
                $response['code'] = str_replace($row['supplier_code'],'',$response['code']);
            }
			$response['intUOM'] 					= $row['intUOM'];
			$response['intBomItem'] 				= $row['intBomItem'];
			$response['intStoresTrnsItem'] 				= $row['intStoresTrnsItem'];
			$response['intSpecialRm'] 				= $row['intSpecialRm'];
			$response['intStockValuationType'] 		= $row['intStockValuationType'];
			$response['intPerfectSupplier'] 		= $row['intPerfectSupplier'];
			$response['intAlternativeSupplier'] 	= $row['intAlternativeSupplier'];
			$response['intDefaultCostItem'] 		= $row['intDefaultCostItem'];
			$response['dblConsumptionPerInch'] 		= $row['dblConsumptionPerInch'];
			
			$response['dblCostPerInch'] 			= $row['dblCostPerInch'];
			$response['dblLastPrice'] 				= $row['dblLastPrice'];
			$response['intReorderPolicy'] 			= $row['intReorderPolicy'];
			$response['intReservePolicy'] 			= $row['intReservePolicy'];
			$response['dblReorderCycle'] 			= $row['dblReorderCycle'];
			$response['dblSaftyLeadTime'] 			= $row['dblSaftyLeadTime'];
			$response['dblSaftyStockQty'] 			= $row['dblSaftyStockQty'];
			$response['dblReorderPoints'] 			= $row['dblReorderPoints'];
			$response['dblReorderQty'] 				= $row['dblReorderQty'];
			
			$response['dblMaximumInventory'] 		= $row['dblMaximumInventory'];
			$response['dblMaximumOrderQty'] 		= $row['dblMaximumOrderQty'];
			$response['dblMinimumOrderQty'] 		= $row['dblMinimumOrderQty'];
			$response['intBlockPurchase'] 			= $row['intBlockPurchase'];
			$response['intBlockGrn'] 				= $row['intBlockGrn'];
			
			$response['status'] 					= $row['intStatus'];
			$response['intCurrency'] 				= $row['intCurrency'];
			$response['foilWidth'] 					= $row['foil_width'];
			$response['foilHeight'] 				= $row['foil_height'];
			$response['inspection'] 				= $row['intInspectionItem'];
			$response['TechniqueId'] 				= $row['intTechniqueId'];
			$response['ExpireItem'] 				= $row['EXPIRERY_ITEM'];
		    $response['AutoCons'] 				    = $row['CONSUMPTION_AUTO'];
			$response['nonOrder'] 			    	= $row['DIRECT_NON_DIRECT_RM_TYPE'];
			$response['roll_form'] 			    	= $row['ROLL_FORM'];
                        $response['item_hide'] 			    	= $row['ITEM_HIDE'];	
                        $response['fixedAsset'] 				= $row['FIXED_ASSET'];
 			$response['depriciation'] 				= $row['DEPRECIATION_RATE'];
		}
		echo json_encode($response);
	}
	else if($requestType=='loadFixedType')
	{
		$mainCat  = $_REQUEST['mainCat'];
		$sql = "SELECT mst_maincategory.FIXED_ASSET from mst_maincategory  
 WHERE mst_maincategory.intId =  '$mainCat'";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
		
			$response['fixedAsset'] 				= $row['FIXED_ASSET'];
		}
		echo json_encode($response);
	}
	
	
?>
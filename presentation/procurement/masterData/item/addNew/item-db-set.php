<?php 
	ini_set('display_errors',0);
	session_start();
	$backwardseperator = "../../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	
	$id							= $_REQUEST['cboSearch'];
	$intMainCategory			= $_REQUEST['cboMainCategory'];
	$intSubCategory				= $_REQUEST['cboSubCategory'];
	$strCode					= $_REQUEST['txtCode'];
	$strName					= $_REQUEST['txtName'];
	$intUOM						= $_REQUEST['cboUOM'];
	$intBomItem					= chk($_REQUEST['chkBomItem']);
	$intSpecialRm				= chk($_REQUEST['chkSpecialRm']);
	$intStoresTrnsItem				= chk($_REQUEST['chkStoresTrnsItm']);
	$intStockValuationType		= null($_REQUEST['cboStockValuation']);
	$intPerfectSupplier			= null($_REQUEST['cboPerfectSupplier']);
	$intAlternativeSupplier		= null($_REQUEST['cboAlternativeSupplier']);
	$intCurrency				= null($_REQUEST['cboCurrency']);
	$intDefaultCostItem			= chk($_REQUEST['chkDefaultCostItem']);
	$dblConsumptionPerInch		= val($_REQUEST['txtConsuptionPerInch']);	
	$dblCostPerInch				= val($_REQUEST['txtCostPerInch']);	
	$dblLastPrice				= val($_REQUEST['txtLastPrice']);
	$intReorderPolicy			= null($_REQUEST['cboReOrderPolicy']);	
	$intReservePolicy			= null($_REQUEST['cboReservePolicy']);
	$dblReorderCycle			= val($_REQUEST['txtReOrderCycle']);	
	$dblSaftyLeadTime			= val($_REQUEST['txtSafetyLeadTime']);
	$dblSaftyStockQty			= val($_REQUEST['txtSafetyStockQty']);
	$dblReorderPoints			= val($_REQUEST['txtReOrderPoints']);
	$dblReorderQty				= val($_REQUEST['txtReOrderQty']);
	$dblMaximumInventory		= val($_REQUEST['txtMaximumInventory']);	
	$dblMaximumOrderQty			= val($_REQUEST['txtMaximumOrderQty']);
	$dblMinimumOrderQty			= val($_REQUEST['txtMinimumOrderQty']);
	$intBlockPurchase			= chk($_REQUEST['chkBlockedPurchase']);
	$intBlockGrn				= chk($_REQUEST['chkBlockGrn']);
	$intStatus					= chk($_REQUEST['chkActive']);
	
	$foilWidth					= val($_REQUEST['txtFoilWidth']);
	$foilHeight					= val($_REQUEST['txtFoilHeight']);
	$inspection					= chk($_REQUEST['chkInspection']);
	$techniqueId				= val($_REQUEST['cboTechnique']);
	$expireItem					= chk($_REQUEST['chkExpireItem']);
	$autoCons					= chk($_REQUEST['chkAutoCons']);
	$nonOrder					= chk($_REQUEST['chkNonOrder']);
	$roll_form					= chk($_REQUEST['chkRoll']);
	$itemHide					= chk($_REQUEST['chkItemHide']);
	
	$depriciation				= null($_REQUEST['txtDepreciateRate']);
    if($intPerfectSupplier != null){
        $sql_supplier = "SELECT mst_supplier.strCode FROM mst_supplier WHERE mst_supplier.intId = '$intPerfectSupplier'";
        $result_supplier = $db->RunQuery($sql_supplier);
        $row = mysqli_fetch_array($result_supplier);
        $strCode = $row['strCode'].$strCode;
    }
	/////////// location insert part /////////////////////
	if($requestType=='add')
	{
        $sql = "INSERT INTO `mst_item` (intMainCategory,intSubCategory,strCode,strName,intUOM,intBomItem,intStoresTrnsItem,intSpecialRm,intStockValuationType,
intPerfectSupplier,intAlternativeSupplier,intDefaultCostItem,dblConsumptionPerInch,dblCostPerInch,dblLastPrice,
intReorderPolicy,intReservePolicy,
dblReorderCycle,dblSaftyLeadTime,dblSaftyStockQty,dblReorderPoints,dblReorderQty,dblMaximumInventory,dblMaximumOrderQty,dblMinimumOrderQty,
intBlockPurchase,intBlockGrn,intStatus,intCreator,dtmCreateDate,intCurrency,foil_width,foil_height,intInspectionItem,intTechniqueId,EXPIRERY_ITEM,CONSUMPTION_AUTO,DEPRECIATION_RATE,DIRECT_NON_DIRECT_RM_TYPE,ROLL_FORM,ITEM_HIDE)
 VALUES ($intMainCategory,$intSubCategory,'$strCode','$strName',$intUOM,$intBomItem,$intStoresTrnsItem,$intSpecialRm,$intStockValuationType,
$intPerfectSupplier,$intAlternativeSupplier,$intDefaultCostItem,$dblConsumptionPerInch,$dblCostPerInch,$dblLastPrice,
$intReorderPolicy,$intReservePolicy,
$dblReorderCycle,$dblSaftyLeadTime,$dblSaftyStockQty,$dblReorderPoints,$dblReorderQty,$dblMaximumInventory,$dblMaximumOrderQty,$dblMinimumOrderQty,
$intBlockPurchase,$intBlockGrn,$intStatus,$userId,now(),$intCurrency,$foilWidth,$foilHeight,$inspection,$techniqueId,$expireItem,$autoCons,$depriciation,$nonOrder,$roll_form,$itemHide)";
//die($sql);
		$result = $db->RunQuery($sql);
		if($result){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
	}
	/////////// location update part /////////////////////
	else if($requestType=='edit')
	{
			$sql = "UPDATE `mst_item` SET `intMainCategory`='$intMainCategory',
			`intSubCategory`='$intSubCategory',
			`strCode`='$strCode',
			`strName`='$strName',
			`intUOM`=$intUOM,
			`intBomItem`='$intBomItem',
			`intStoresTrnsItem`='$intStoresTrnsItem',
			`intSpecialRm`='$intSpecialRm',
			`intStockValuationType`=$intStockValuationType,
			`intPerfectSupplier`=$intPerfectSupplier,
			`intAlternativeSupplier`=$intAlternativeSupplier,
			`intDefaultCostItem`='$intDefaultCostItem',
			`dblConsumptionPerInch`='$intDefaultCostItem',
			`dblCostPerInch`='$intDefaultCostItem',
			`dblLastPrice`='$intDefaultCostItem',
			`intReorderPolicy`=$intDefaultCostItem,
			
			`dblConsumptionPerInch`='$dblConsumptionPerInch',
			`dblCostPerInch`='$dblCostPerInch',
			`dblLastPrice`='$dblLastPrice',
			
			`intReorderPolicy`=$intReorderPolicy,
			`intReservePolicy`=$intReservePolicy,
			`dblReorderCycle`='$dblReorderCycle',
			`dblSaftyLeadTime`='$dblSaftyLeadTime',
			`dblSaftyStockQty`='$dblSaftyStockQty',
			`dblReorderPoints`='$dblReorderPoints',
			`dblReorderQty`='$dblReorderQty',
			`dblMaximumInventory`='$dblMaximumInventory',
			`dblMaximumOrderQty`='$dblMaximumOrderQty',
			`dblMinimumOrderQty`='$dblMinimumOrderQty',
			`intBlockPurchase`='$intBlockPurchase',
			`intBlockGrn`='$intBlockGrn',
			`intStatus`='$intStatus',
			`intModifyer`='$userId',
			 intCurrency=$intCurrency,
			 foil_width='$foilWidth',
			 intInspectionItem = '$inspection',
			 foil_height = '$foilHeight',
			 intTechniqueId = $techniqueId,
			 EXPIRERY_ITEM = '$expireItem',
			 CONSUMPTION_AUTO = '$autoCons',
			 DIRECT_NON_DIRECT_RM_TYPE = '$nonOrder',
			 DEPRECIATION_RATE = $depriciation,
			 ROLL_FORM = $roll_form,
			 ITEM_HIDE = $itemHide
	
			WHERE (`intId`='$id')  ";
                        //echo $sql;
		$result = $db->RunQuery($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Updated successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	/////////// location delete part /////////////////////
	else if($requestType=='delete')
	{
		$sql = "DELETE FROM `mst_item` WHERE (`intId`='$id')  ";
		$result = $db->RunQuery($sql);
		if(($result)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Deleted successfully.';
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			=$sql;
		}
	}
	echo json_encode($response);
	
?>
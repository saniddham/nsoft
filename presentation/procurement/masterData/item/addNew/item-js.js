
var basePath	= "presentation/procurement/masterData/item/addNew/";			
$(document).ready(function() {
		$("#frmItem").validationEngine();
		$('#frmItem #cboUOM').die('change').live('change',UOMChange);
		
  //permision for add 
  /*if(intAddx)
  {
 	$('#frmItem #butNew').show();
	$('#frmItem #butSave').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmItem #butSave').show();
	$('#frmItem #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmItem #butDelete').show();
	$('#frmItem #cboSearch').removeAttr('disabled');
  }
  
  //permision for view
  if(intViewx)
  {
	$('#frmItem #cboSearch').removeAttr('disabled');
  }*/
  
/*  $(':input').bind('keyup mouseup', function(){
      $(this).val($.trim($(this).val())) ;
    });*/

  ///save button click event
  $('#frmItem #butSave').die('click').live('click',function(){
	//$('#frmItem').submit();
	var requestType = '';
	if ($('#frmItem').validationEngine('validate'))   
    { 
		//$('#txtLocationCode').validationEngine('showPrompt', 'This is an example', 'pass'); //hide all alerts//
		if($("#frmItem #cboSearch").val()=='')
			requestType = 'add';
		else
			requestType = 'edit';
                   
		
		var url = basePath+"item-db-set.php";
     	var obj = $.ajax({
			url:url,
			dataType: "json",  
			type:'POST',
			data:$("#frmItem").serialize()+'&requestType='+requestType,
                       
			//data:'{"requestType":"addLocation"}',
			async:false,
			
			success:function(json){
					$('#frmItem #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
							var mainId = $('#frmItem #cboMainCategory').val();
							var subId  = $('#frmItem #cboSubCategory').val();
							$('#frmItem').get(0).reset();
							$('#frmItem #cboSubCategory').val(subId);
							$('#frmItem #cboMainCategory').val(mainId);
						loadCombo_frmItem();
						var t=setTimeout("alertx()",1000);
						return;
					}
					var t=setTimeout("alertx()",3000);
				},
			error:function(xhr,status){
					$('#frmItem #butSave').validationEngine('showPrompt', errormsg(xhr.status), 'fail');
					var t=setTimeout("alertx()",1000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
   
   /////////////////////////////////////////////////////
   //// load location details //////////////////////////
   /////////////////////////////////////////////////////
   $('#frmItem #cboSearch').die('click').live('click',function(){
	   $('#frmItem').validationEngine('hide');
   });
    $('#frmItem #cboSearch').die('change').live('change',function(){
		$('#frmItem').validationEngine('hide');
		var url = basePath+"item-db-get.php";
		if($('#frmItem #cboSearch').val()=='')
		{
			var mainId = $('#frmItem #cboMainCategory').val();
			var subId  = $('#frmItem #cboSubCategory').val();
			$('#frmItem').get(0).reset();
			$('#frmItem #cboSubCategory').val(subId);
			$('#frmItem #cboMainCategory').val(mainId);
			return;	
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:'requestType=loadDetails&id='+$(this).val(),
			async:false,
			success:function(json){
					$('#frmItem #txtCode').val(json.code);
					$('#frmItem #txtName').val(json.name);
					$('#frmItem #txtRemark').val(json.remark);
					
					$('#frmItem #cboUOM').val(json.intUOM);
					//uom validation
					//validateFoil(json.intUOM);
					/////////////////
					
					$('#frmItem #chkBomItem').attr('checked',chk(json.intBomItem));
					$('#frmItem #chkSpecialRm').attr('checked',chk(json.intSpecialRm));
					$('#frmItem #chkStoresTrnsItm').attr('checked',chk(json.intStoresTrnsItem));
					$('#frmItem #cboStockValuation').val(json.intStockValuationType);
					$('#frmItem #cboPerfectSupplier').val(json.intPerfectSupplier);
					$('#frmItem #cboAlternativeSupplier').val(json.intAlternativeSupplier);
					$('#frmItem #chkDefaultCostItem').attr('checked',chk(json.intDefaultCostItem));
					$('#frmItem #txtConsuptionPerInch').val(json.dblConsumptionPerInch);
					
					$('#frmItem #txtCostPerInch').val(json.dblCostPerInch);
					$('#frmItem #txtLastPrice').val(json.dblLastPrice);
					$('#frmItem #cboReOrderPolicy').val(json.intReorderPolicy);
					$('#frmItem #cboReservePolicy').val(json.intReservePolicy);
					$('#frmItem #txtReOrderCycle').val(json.dblReorderCycle);
					$('#frmItem #txtSafetyLeadTime').val(json.dblSaftyLeadTime);
					$('#frmItem #txtSafetyStockQty').val(json.dblSaftyStockQty);
					
					$('#frmItem #txtReOrderPoints').val(json.dblReorderPoints);
					$('#frmItem #txtReOrderQty').val(json.dblReorderQty);
					$('#frmItem #txtMaximumInventory').val(json.dblMaximumInventory);
					$('#frmItem #txtMaximumOrderQty').val(json.dblMaximumOrderQty);
					$('#frmItem #txtMinimumOrderQty').val(json.dblMinimumOrderQty);
					$('#frmItem #chkBlockedPurchase').attr('checked',chk(json.intBlockPurchase));
					$('#frmItem #chkInspection').attr('checked',chk(json.inspection));
					$('#frmItem #chkBlockGrn').attr('checked',chk(json.intBlockGrn));
					$('#frmItem #chkActive').attr('checked',chk(json.status));
					$('#frmItem #cboCurrency').val(json.intCurrency);
					
					$('#frmItem #txtFoilWidth').val((json.foilWidth==0?'':json.foilWidth));
					$('#frmItem #txtFoilHeight').val((json.foilHeight==0?'':json.foilHeight));
					$('#frmItem #cboTechnique').val(json.TechniqueId);
					$('#frmItem #chkExpireItem').attr('checked',chk(json.ExpireItem));
				    $('#frmItem #chkAutoCons').attr('checked',chk(json.AutoCons));
				    $('#frmItem #chkNonOrder').attr('checked',chk(json.nonOrder));
				    $('#frmItem #chkRoll').attr('checked',chk(json.roll_form));
				      $('#frmItem #chkItemHide').attr('checked',chk(json.item_hide));	
					if(json.fixedAsset==1){
						$('#frmItem #txtDepreciateRate').val((json.depriciation==0?'':json.depriciation));
						$('#frmItem #txtDepreciateRate').show();
						$('#frmItem #depreciation').show();
					}
					else{
 						$('#frmItem #txtDepreciateRate').val(0);
						$('#frmItem #txtDepreciateRate').hide();
						$('#frmItem #depreciation').hide();
					}

			}
	});
	//////////// end of load details /////////////////
	
	});
	
	$('#frmItem #butNew').die('click').live('click',function(){
		var mainId = $('#frmItem #cboMainCategory').val();
		var subId  = $('#frmItem #cboSubCategory').val();
		$('#frmItem').get(0).reset();
		$('#frmItem #cboSubCategory').val(subId);
		$('#frmItem #cboMainCategory').val(mainId);
		loadCombo_frmItem();
		$('#frmItem #txtCode').focus();
	});
    $('#frmItem #butDelete').die('click').live('click',function(){
		if($('#frmItem #cboSearch').val()=='')
		{
			$('#frmItem #butDelete').validationEngine('showPrompt', 'Please select Item.', 'fail');
			//var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmItem #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = basePath+"item-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											type:'POST',
											data:'requestType=delete&cboSearch='+$('#frmItem #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmItem #butDelete').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
												//var t=setTimeout("alertDelete()",1000);
												if(json.type=='pass')
												{
													var mainId = $('#frmItem #cboMainCategory').val();
													var subId  = $('#frmItem #cboSubCategory').val();
													$('#frmItem').get(0).reset();
													$('#frmItem #cboSubCategory').val(subId);
													$('#frmItem #cboMainCategory').val(mainId);
													loadCombo_frmItem();
												}	
											}	 
										});
									}
				}
		 	});
			
		}
	});
	
	//////////// load sub category /////////////////////////
	$('#cboMainCategory').die('change').live('change',loadSubCategory_frmItem);
	/////////// load items ////////////////////////////////
	$('#cboSubCategory').die('change').live('change',loadCombo_frmItem);
	////////////////////////////////////////////////////////
	
	////////////////// load main category popup ////////////
	$('#frmItem #cboReOrderPolicy').die('change').live('change',function(){
		
		switch ($(this).val()){
			case '1':
				$('#txtReOrderCycle').removeAttr("disabled");
				$('#txtSafetyStockQty').removeAttr("disabled");
				$('#txtReOrderQty').removeAttr("disabled");
				$('#txtMaximumOrderQty').removeAttr("disabled");
				$('#cboReservePolicy').removeAttr("disabled");
				$('#txtSafetyLeadTime').removeAttr("disabled");
				$('#txtReOrderPoints').removeAttr("disabled");
			$('#txtMaximumInventory').attr("disabled", "disabled");
				$('#txtMinimumOrderQty').removeAttr("disabled");
			break;
			case '2':
				$('#txtReOrderCycle').removeAttr("disabled");
				$('#txtSafetyStockQty').removeAttr("disabled");
			$('#txtReOrderQty').attr("disabled", "disabled");
				$('#txtMaximumOrderQty').removeAttr("disabled");
				$('#cboReservePolicy').removeAttr("disabled");
				$('#txtSafetyLeadTime').removeAttr("disabled");
				$('#txtReOrderPoints').removeAttr("disabled");
				$('#txtMaximumInventory').removeAttr("disabled");
				$('#txtMinimumOrderQty').removeAttr("disabled");
			break;
			case '3':
				$('#txtReOrderCycle').attr("disabled", "disabled");
				$('#txtSafetyStockQty').attr("disabled", "disabled");
				$('#txtReOrderQty').attr("disabled", "disabled");
				$('#txtMaximumOrderQty').attr("disabled", "disabled");
					$('#cboReservePolicy').removeAttr("disabled");
					$('#txtSafetyLeadTime').removeAttr("disabled");
				$('#txtReOrderPoints').attr("disabled", "disabled");
				$('#txtMaximumInventory').attr("disabled", "disabled");
				$('#txtMinimumOrderQty').attr("disabled", "disabled");
			break;
			case '4':
					$('#txtReOrderCycle').removeAttr("disabled");
				$('#txtSafetyStockQty').attr("disabled", "disabled");
				$('#txtReOrderQty').attr("disabled", "disabled");
					$('#txtMaximumOrderQty').removeAttr("disabled");
					$('#cboReservePolicy').removeAttr("disabled");
					$('#txtSafetyLeadTime').removeAttr("disabled");
				$('#txtReOrderPoints').attr("disabled", "disabled");
				$('#txtMaximumInventory').attr("disabled", "disabled");
					$('#txtMinimumOrderQty').removeAttr("disabled");
			break;
		}
});
	
				
});
function loadMain()
{
	$("#butViewMainCategory").die('click').live('click',popupMainCategory);
}

function loadSub()
{
	$("#butViewSubCategory").die('click').live('click',popupSubCategory);
}

function popupMainCategory()
{
	popupWindow2('1','#cboSearch','#cboMainCategory','#frmItem');
}

function popupSubCategory()
{
	popupWindow2('2','#cboSearch','#cboSubCategory','#frmItem');
}
function loadSubCategory_frmItem()
{
	var url 		= basePath+"item-db-get.php?requestType=loadSubCategory&id="+$('#cboMainCategory').val();
	var httpobj 	= $.ajax({url:url,async:false})
	$('#frmItem #cboSubCategory').html(httpobj.responseText);
	$('#frmItem #cboSearch').html('');
	hideDisplayDepreciation();
}

function loadCombo_frmItem()
{
	var url 	= basePath+"item-db-get.php?requestType=loadCombo&mainId="+$('#cboMainCategory').val()+'&subId='+$('#cboSubCategory').val();
	var httpobj = $.ajax({url:url,async:false})
	$('#frmItem #cboSearch').html(httpobj.responseText);
}

function alertx()
{
	$('#frmItem #butSave').validationEngine('hide');
}
function alertDelete()
{
	$('#frmItem #butDelete').validationEngine('hide');
}

function UOMChange()
{
	var id = $(this).val();
	//validateFoil(id);
}

function validateFoil(id)
{
	if(id==8 || id==10)
	{
		$('#tblFoil').show();
		$('#txtFoilWidth').addClass('validate[custom[number],required]');
		$('#txtFoilHeight').addClass('validate[custom[number],required]');
	}
	else
	{
		$('#tblFoil').hide();
		$('#txtFoilWidth').addClass('validate[custom[number]]');
		$('#txtFoilHeight').addClass('validate[custom[number]]');	
	}	
}




function hideDisplayDepreciation() {
	
		$('#frmItem').validationEngine('hide');
		var url = basePath+"item-db-get.php";
		if($('#frmItem #cboMainCategory').val()=='')
		{
			return;	
		}
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:'requestType=loadFixedType&mainCat='+$('#cboMainCategory').val(),
			async:false,
			success:function(json){
					if(json.fixedAsset==1){
						$('#frmItem #txtDepreciateRate').val('');
						$('#frmItem #txtDepreciateRate').show();
						$('#frmItem #depreciation').show();
					}
					else{
 						$('#frmItem #txtDepreciateRate').val('');
						$('#frmItem #txtDepreciateRate').hide();
						$('#frmItem #depreciation').hide();
					}

			}
		});
}
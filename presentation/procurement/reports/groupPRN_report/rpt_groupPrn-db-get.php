<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	require_once "{$backwardseperator}class/cls_permisions.php";
	$objpermisionget= new cls_permisions($db);
	
	$flagAllowDefaultExcRate= $objpermisionget->allowDefaultExcRate($company);
	$flagRemarks= $objpermisionget->allowRemarks($company);
	///
	/////////// type of print load part /////////////////////
	if($requestType=='loadCombo')
	{
		$sql = "SELECT
					intId,
					strName
				FROM mst_techniques
				order by strName
				";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
	}
	else if($requestType=='loadSupplierDetails')
	{
		$supplier  = $_REQUEST['supplier'];
		$date  = $_REQUEST['date'];
		 $sql = "SELECT
				mst_supplier.intCurrencyId,
				mst_financeexchangerate.dblBuying,
				mst_supplier.intShipmentId,
				mst_supplier.intPaymentsTermsId,
				mst_supplier.intPaymentsMethodsId
				FROM
				mst_supplier
				left Join mst_financeexchangerate ON mst_supplier.intCurrencyId = mst_financeexchangerate.intCurrencyId
				WHERE
				mst_supplier.intId =  '$supplier' /* and intCompanyId='$company'*/";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['currency'] 	= $row['intCurrencyId'];
			$response['shipmentMode'] 	= $row['intShipmentId'];
			$response['payTerm'] = $row['intPaymentsTermsId'];
			$response['payMode'] = $row['intPaymentsMethodsId'];
		}
		
		echo json_encode($response);
	}
	else if($requestType=='loadExchangeRate')
	{
		$currency  = $_REQUEST['currency'];
		$date  = $_REQUEST['date'];
		 $sql = "SELECT
				mst_financeexchangerate.dblBuying
				FROM mst_financeexchangerate
				WHERE
				mst_financeexchangerate.intCurrencyId =  '$currency' and mst_financeexchangerate.dtmDate LIKE '%$date%' and intCompanyId='$company'";
		$result = $db->RunQuery($sql);
		
		$response['excRate'] = '';
		while($row=mysqli_fetch_array($result))
		{
			$response['flagAllowDefaultExcRate'] = $flagAllowDefaultExcRate;
			$response['excRate'] = $row['dblBuying'];
		}
		
		echo json_encode($response);
	}
	else if($requestType=='loadDefaultCurrencyAndExchRate')
	{
		$currency  = $_REQUEST['currency'];
		$response['flag'] = 0;
		
		 $sql = "SELECT 
				 mst_financeexchangerate.intCurrencyId, 
				mst_financeexchangerate.dblBuying
				FROM mst_financeexchangerate
				WHERE
				mst_financeexchangerate.dblBuying =  1 order by dtmDate desc limit 1";
		$result = $db->RunQuery($sql);
		
		$response['flag'] = 0;
		$response['excRate'] = '';
		while($row=mysqli_fetch_array($result))
		{
			if(($row['intCurrencyId']==$currency) || ($currency=='')){
				$response['flag'] = 1;
			}
			if($flagAllowDefaultExcRate==0){
				$response['flag'] = 0;
			}
			$response['currency'] = $row['intCurrencyId'];
			$response['excRate'] = 1.00;
		}
		
		echo json_encode($response);
	}
	else if($requestType=='loadSubCategory')
	{
		$mainCategory  = $_REQUEST['mainCategory'];
		$sql = "SELECT
				mst_subcategory.intId,
				mst_subcategory.strCode,
				mst_subcategory.strName
				FROM
				mst_subcategory
				INNER JOIN mst_financeitemactivate ON mst_financeitemactivate.subCategoryId = mst_subcategory.intId
				WHERE
				concat(IFNULL(otherAccountId,''),IFNULL(stockAccountId,''),IFNULL(localAccId,''),IFNULL(impAccId,'')) <> '' AND
				mst_financeitemactivate.intCompanyId = '$company' AND 
				mst_subcategory.intMainCategory =  '$mainCategory'";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
		//echo json_encode($response);
	}
	else if($requestType=='loadItems')
	{
		$prnSelection  = $_REQUEST['prnSelection'];
		$prnNo  = $_REQUEST['prnNo'];
		$prnNoArray 	 = explode('/',$prnNo);
		$mainCategory  = $_REQUEST['mainCategory'];
		$subCategory  = $_REQUEST['subCategory'];
		$description  = $_REQUEST['description'];
		$currency  = $_REQUEST['currency'];
		$poDate  = $_REQUEST['poDate'];
		
		$sql = "SELECT
				menus_special.intStatus
				FROM
				menus_special
				Inner Join menus_special_permision ON menus_special.intId = menus_special_permision.intSpMenuId
				WHERE
				menus_special_permision.intUser =  '$userId' AND
				menus_special.strPermisionType =  'Permision to price edit'";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$data['priceEdit'] 	= $row['intStatus'];
		
		if($prnSelection==1){
		$sql="SELECT
				mst_item.intId,
				mst_item.intMainCategory,
				mst_item.intSubCategory,
				mst_item.strCode,
				mst_item.strName as itemName,
				mst_item.intCurrency,
				mst_item.intUOM,
				mst_units.strCode as uom,
				mst_item.dblLastPrice,
				mst_maincategory.strName as mainCatName,
				mst_subcategory.strName as subCatName  ,
				mst_financecurrency.strCode as currency ,  
				trn_prndetails.intPrnNo,
				trn_prndetails.intYear,
				trn_prndetails.dblPrnQty,
				trn_prndetails.dblPoQty 
				FROM
				trn_prndetails 
				INNER JOIN trn_prnheader ON trn_prndetails.intPrnNo = trn_prnheader.intPrnNo AND trn_prndetails.intYear = trn_prnheader.intYear
				Inner Join mst_item ON trn_prndetails.intItem = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
				INNER JOIN mst_financeitemactivate ON mst_financeitemactivate.subCategoryId = mst_subcategory.intId
				Left Join mst_financecurrency ON mst_item.intCurrency = mst_financecurrency.intId 
				Left Join mst_units ON mst_item.intUOM = mst_units.intId 
				WHERE 
				trn_prnheader.intStatus='1' AND 
				concat(IFNULL(otherAccountId,''),IFNULL(stockAccountId,''),IFNULL(localAccId,''),IFNULL(impAccId,'')) <> '' AND
				mst_financeitemactivate.intCompanyId = $company AND 
				mst_item.intStatus = '1' AND 
				trn_prndetails.intPrnNo =  '$prnNoArray[0]' AND
				trn_prndetails.intYear =  '$prnNoArray[1]' AND  
				trn_prndetails.dblPrnQty-trn_prndetails.dblPoQty>0";	
		}
		else{
			$sql="SELECT
				mst_item.intId,
				mst_item.intMainCategory,
				mst_item.intSubCategory,
				mst_item.strCode,
				mst_item.strName as itemName,
				mst_item.intCurrency,
				mst_item.intUOM,
				mst_units.strCode as uom,
				mst_item.dblLastPrice,
				mst_maincategory.strName as mainCatName,
				mst_subcategory.strName as subCatName  ,
				mst_financecurrency.strCode as currency  
				FROM
				mst_item
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
				INNER JOIN mst_financeitemactivate ON mst_financeitemactivate.subCategoryId = mst_subcategory.intId
				Left Join mst_financecurrency ON mst_item.intCurrency = mst_financecurrency.intId 
				Left Join mst_units ON mst_item.intUOM = mst_units.intId 
				WHERE 
				concat(IFNULL(otherAccountId,''),IFNULL(stockAccountId,''),IFNULL(localAccId,''),IFNULL(impAccId,'')) <> '' AND
				mst_financeitemactivate.intCompanyId = $company AND 
				mst_item.intStatus = '1' AND 
				mst_item.intMainCategory =  '$mainCategory' ";
				if($subCategory!=''){
				$sql.=" AND
				mst_item.intSubCategory =  '$subCategory'";
				}
				if($description!=''){
				$sql.=" AND
				mst_item.strName LIKE  '%$description%'";
				}
				$sql.=" Order by mst_maincategory.strName asc, mst_subcategory.strName asc, mst_item.strName asc";
		}
		// echo $sql;
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$prnQty = $row['dblPrnQty'];
			if(!$prnQty)
			$prnQty=0;
			$poQty = $row['dblPoQty'];
			if(!$poQty)
			$poQty=0;
			
			$data['itemId'] 	= $row['intId'];
			$data['maincatId'] = $row['intMainCategory'];
			$data['subCatId'] 	= $row['intSubCategory'];
			$data['code'] = $row['strCode'];
			$data['itemName'] = $row['itemName'];
			if($row['currency']==''){
			$data['currency'] 	= '';
			}
			else{
			$data['currency'] 	= $row['currency'];
			}
			//$data['uom'] 	= $row['intUOM'];
			$data['uom'] 	= $row['uom'];
			//$data['unitPrice'] = $row['dblLastPrice'];
			$data['unitPrice']=loadUnitPrice($row['dblLastPrice'],$row['intCurrency'],$currency,$company,$poDate);
			$data['stockBal']=round(getStockBalance_bulk($location,$row['intId']),4);
			$data['mainCatName'] = $row['mainCatName'];
			$data['subCatName'] = $row['subCatName'];
			$data['prnQty'] = $prnQty;
			$data['poQty'] = $poQty;
			$data['flagRemarks'] = $flagRemarks;
			$arrCombo[] = $data;
		}
		$response['arrCombo'] 	= $arrCombo;
		echo json_encode($response);

	}
	else if($requestType=='loadGRPPRNItems'){
		
		$arr 		= json_decode($_REQUEST['arr'], true);
		$prnNos ='';
		foreach($arr as $arrVal)
		{
			$prnNo 			= $arrVal['prnNo'];
			//$prnNoArray=explode("/",$prnNo);
			//$prnNo=$prnNoArray[0];
			//$prnNoYear=$prnNoArray[1];
			$prnNos .="'".$prnNo."',";
		}
		$prnNos .="'*'";
		
		
		$sql = "SELECT
				menus_special.intStatus
				FROM
				menus_special
				Inner Join menus_special_permision ON menus_special.intId = menus_special_permision.intSpMenuId
				WHERE
				menus_special_permision.intUser =  '$userId' AND
				menus_special.strPermisionType =  'Permision to price edit'";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$data['priceEdit'] 	= $row['intStatus'];
		
		$sql="SELECT
				mst_item.intId,
				mst_item.intMainCategory,
				mst_item.intSubCategory,
				mst_item.strCode,
				mst_item.strName as itemName,
				mst_item.intCurrency,
				mst_item.intUOM,
				mst_units.strCode as uom,
				mst_item.dblLastPrice,
				mst_maincategory.strName as mainCatName,
				mst_subcategory.strName as subCatName  ,
				mst_financecurrency.strCode as currency ,
				sum(trn_prndetails.dblPrnQty) as dblPrnQty 
				FROM
				trn_prndetails 
				INNER JOIN trn_prnheader ON trn_prndetails.intPrnNo = trn_prnheader.intPrnNo AND trn_prndetails.intYear = trn_prnheader.intYear
				Inner Join mst_item ON trn_prndetails.intItem = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
				INNER JOIN mst_financeitemactivate ON mst_financeitemactivate.subCategoryId = mst_subcategory.intId
				Left Join mst_financecurrency ON mst_item.intCurrency = mst_financecurrency.intId 
				Left Join mst_units ON mst_item.intUOM = mst_units.intId 
				WHERE 
				trn_prnheader.intStatus='1' AND 
				concat(IFNULL(otherAccountId,''),IFNULL(stockAccountId,''),IFNULL(localAccId,''),IFNULL(impAccId,'')) <> '' AND
				mst_financeitemactivate.intCompanyId = $company AND 
				mst_item.intStatus = '1' AND 
				concat(trn_prndetails.intPrnNo,'/',trn_prndetails.intYear) IN  ($prnNos) 
				group by  mst_item.intId
				";	

		// echo $sql;
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$prnQty = $row['dblPrnQty'];
			if(!$prnQty)
			$prnQty=0;
			
			$data['itemId'] 	= $row['intId'];
			$data['maincatId'] = $row['intMainCategory'];
			$data['subCatId'] 	= $row['intSubCategory'];
			$data['code'] = $row['strCode'];
			$data['itemName'] = $row['itemName'];
			if($row['currency']==''){
			$data['currency'] 	= '';
			}
			else{
			$data['currency'] 	= $row['currency'];
			}
			//$data['uom'] 	= $row['intUOM'];
			$data['uom'] 	= $row['uom'];
			$data['unitPrice'] = $row['dblLastPrice'];
			$data['mainCatName'] = $row['mainCatName'];
			$data['subCatName'] = $row['subCatName'];
			$data['prnQty'] = round($prnQty,4);
			$arrCombo[] = $data;
		}
		$response['arrCombo'] 	= $arrCombo;
		echo json_encode($response);

	}
	else if($requestType=='loadPRNnos')
	{
		$poType  = $_REQUEST['poType'];
		
		$sql="SELECT DISTINCT 
							trn_prnheader.intPrnNo,
							trn_prnheader.intYear
							FROM trn_prnheader 
							Inner Join trn_prndetails ON trn_prnheader.intPrnNo = trn_prndetails.intPrnNo AND trn_prnheader.intYear = trn_prndetails.intYear
							Inner Join mst_locations ON trn_prnheader.intCompany = mst_locations.intId
							WHERE
							mst_locations.intCompanyId =  '$company' and trn_prnheader.intStatus='1' AND (trn_prndetails.dblPrnQty-trn_prndetails.dblPoQty) >  '0'";	
							
		if($poType==1)
			$sql.="  AND IFNULL(intPORaised,0) = '1'";
		else
			$sql.="  AND IFNULL(intPORaised,0) != '1'";
							
		// echo $sql;
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$data['prnNo'] = $row['intPrnNo'];
			$data['prnYear'] = $row['intYear'];
			$arrCombo[] = $data;
		}
		$response['arrCombo'] 	= $arrCombo;
		echo json_encode($response);

	}
	//----------------------------------------------------------
	function loadUnitPrice($itemUnitPrice,$itemCurrency,$poCurrency,$company,$poDate){
		global $db;
		
		if($flagAllowDefaultExcRate==1){
	  	$sql = "SELECT
				mst_financeexchangerate.dblBuying
				FROM mst_financeexchangerate
				WHERE
				mst_financeexchangerate.intCurrencyId =  '$itemCurrency' and mst_financeexchangerate.dtmDate <=  '$poDate' and intCompanyId='$company' order by mst_financeexchangerate.dtmDate desc limit 1";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$excRateItem = $row['dblBuying'];
		
		$sql = "SELECT
				mst_financeexchangerate.dblBuying
				FROM mst_financeexchangerate
				WHERE
				mst_financeexchangerate.intCurrencyId =  '$poCurrency' and mst_financeexchangerate.dtmDate <=  '$poDate' and intCompanyId='$company' order by mst_financeexchangerate.dtmDate desc limit 1";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$excRatePO = $row['dblBuying'];
		}
		else{
	  	$sql = "SELECT
				mst_financeexchangerate.dblBuying
				FROM mst_financeexchangerate
				WHERE
				mst_financeexchangerate.intCurrencyId =  '$itemCurrency' and mst_financeexchangerate.dtmDate =  '$poDate' and intCompanyId='$company' order by mst_financeexchangerate.dtmDate desc limit 1";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$excRateItem = $row['dblBuying'];
		
		$sql = "SELECT
				mst_financeexchangerate.dblBuying
				FROM mst_financeexchangerate
				WHERE
				mst_financeexchangerate.intCurrencyId =  '$poCurrency' and mst_financeexchangerate.dtmDate =  '$poDate' and intCompanyId='$company' order by mst_financeexchangerate.dtmDate desc limit 1";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$excRatePO = $row['dblBuying'];
		}
		
		return $itemUnitPrice*$excRateItem/$excRatePO;
	}
	//--------------------------------------------------------------
	function getStockBalance_bulk($location,$item)
	{
		global $db;
		   $sql = "SELECT
				Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$location'
				GROUP BY
				ware_stocktransactions_bulk.intItemId";

		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
	
?>
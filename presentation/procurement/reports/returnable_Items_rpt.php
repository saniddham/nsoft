<?php

ini_set('max_execution_time', 1111111111111); ;
session_start();

$dateFrom  = $_REQUEST['dateFrom'];
$dateTo  = $_REQUEST['dateTo'];

$companyId = $sessions->getCompanyId();
$userId = $sessions->getUserId();
$locationId = $sessions->getLocationId();

$programCode = 'P1223';

require_once "class/cls_permisions.php";
$objpermisionget = new cls_permisions($db);
require_once "class/tables/sys_approvelevels.php";
$obj_sys_approvelevels = new sys_approvelevels($db);

$permi_price_view = $objpermisionget->boolSPermision(8);
$str_price_view = 'hidden';

if ($permi_price_view)
    $str_price_view = '';

$dateFrom = $_REQUEST['dateFrom'];
$dateTo = $_REQUEST['dateTo'];
$head_office_loc	= get_ho_loc($companyId);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Returnable Items Report</title>
    <script type="text/javascript"
            src="presentation/customerAndOperation/bulk/placeOrder/listing/rptBulkOrder-js.js?n=1"></script>
    <style>
        .break {
            page-break-before: always;
        }

        @media print {
            .noPrint {
                display: none;
            }
        }

        #apDiv1 {
            position: absolute;
            left: 237px;
            top: 175px;
            width: 619px;
            height: 235px;
            z-index: 1;
        }

        .APPROVE {
            font-size: 16px;
            font-weight: bold;
            color: #00C;
        }
    </style>
</head>

<body>
<form id="frmOrderStockMovemntReport" name="frmOrderStockMovemntReport" method="post" action="returnable_Items_rpt.php">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="3"></td>
        </tr>
        <tr>
            <td width="20%"></td>
            <td width="60%" height="80"
                valign="top"><?php include 'presentation/procurement/reports/reportHeader.php' ?></td>
            <td width="20%"></td>
        </tr>
        <tr>
            <td colspan="3">
                <div id="divPoNo" style="display:none"><?php echo $orderNo ?>/<?php echo $orderYear ?></div>
            </td>
        </tr>
    </table>
    <div align="center">
        <div style="background-color:#FFF"><strong>Returnable Items Report</strong><strong></strong></div>
        <table width="900" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td colspan="10" align="center" bgcolor="#FFFFFF">
                            </td>
                        </tr>
                        <tr>
                            <td width="1%"><span class="normalfnt">Date Range:</span></td>
                            <td width="9%" class="normalfnt"><?php echo $dateFrom; ?> To <?php echo $dateTo; ?></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td colspan="20">
                                <table width="100%" class="bordered" id="tblMainGrid" cellspacing="0"
                                       cellpadding="0">
                                    <tr>
                                        <th width="2%">Date</th>
                                        <th width="30%">Order</th>
                                        <th width="8">Location</th>
                                        <th width="8%">Main Category</th>
                                        <th width="8%">Sub Category</th>
                                        <th width="4%"> Code</th>
                                        <th width="16%">Item</th>
                                        <th width="16%">UOM</th>
                                        <th width="10%" height="22">PRI</th>
                                        <th width="10%" height="22">Issued To HO</th>
                                        <th width="10%">GP From HO</th>
                                        <th width="10%">Issued To Non HO</th>
                                        <th width="10%">Returnable</th>
                                    </tr>
                                    <?php
                                   $sql4 = "SELECT
	TB3.ORDER_COMPANY,
	TB3.location AS location,
	TB3.date,
	TB3.ORDER_NO,
	TB3.ORDER_YEAR,
	TB3.SALES_ORDER,
	TB3.SALES_ORDER_String AS salesOrderStr,
	TB3.ITEM,
	TB3.REQUIRED,
	TB3.TRANSACTION_LOCATION,
	TB3.PO_QTY,
	TB3.PRN_QTY,
	TB3.mainCatName,
	TB3.subCatName,
	TB3.strCode,
        TB3.supItemCode,
	TB3.itemName,
	TB3.UOM,
	TB3.type,
	SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.MRN_QTY,0)) as MRN_QTY,
	SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.MRN_EXTRA_QTY,0)) as MRN_EXTRA_QTY,
	SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.ISSUE_QTY,0)) as ISSUE_QTY,
	SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.GP_QTY,0)) as GP_QTY,
	SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.GP_IN_QTY,0)) as GP_IN_QTY,
	SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.RET_TO_STORE_QTY,0)) as RET_TO_STORE_QTY,
	SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.MRN_QTY,0)) as MRN_QTY_OTHER_LOC,
	SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.MRN_EXTRA_QTY,0)) as MRN_EXTRA_QTY_OTHER_LOC,
	SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.ISSUE_QTY,0)) as ISSUE_QTY_OTHER_LOC,
	SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.GP_QTY,0)) as GP_QTY_OTHER_LOC,
	SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.GP_IN_QTY,0)) as GP_IN_QTY_OTHER_LOC,
	SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.RET_TO_STORE_QTY,0)) as RET_TO_STORE_QTY_OTHER_LOC
FROM
	(
		SELECT
			trn_orderheader.intLocationId AS ORDER_COMPANY,
			TB2.ORDER_NO,
			TB2.ORDER_YEAR,
			TB2.location,
			TB2.ITEM,
			TB2.SALES_ORDER,
			TB2.type,
			CONCAT (trn_orderdetails.strSalesOrderNo,'/',trn_orderdetails.strCombo,'/',trn_orderdetails.strPrintName) AS SALES_ORDER_String,
            GROUP_CONCAT(DISTINCT date(trn_orderheader_approvedby.dtApprovedDate ))AS date,
            mst_maincategory.strName AS mainCatName,
			mst_subcategory.strName AS subCatName,
			mst_item.strCode,
                        CONCAT(mst_supplier.strCode,mst_item.strCode) as supItemCode,
			mst_item.intId,
			mst_item.strName AS itemName,
			mst_units.strName AS UOM,
			TB2.REQUIRED,
			trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID AS TRANSACTION_LOCATION,
			
			(
				SELECT
					trn_po_prn_details.PURCHASED_QTY
				FROM
					trn_po_prn_details
				WHERE
					trn_po_prn_details.ORDER_NO = TB2.ORDER_NO
				AND trn_po_prn_details.ORDER_YEAR = TB2.ORDER_YEAR
				AND trn_po_prn_details.ITEM = TB2.ITEM
			) AS PO_QTY,
			(
				SELECT
					trn_po_prn_details.PURCHASED_QTY
				FROM
					trn_po_prn_details
				WHERE
					trn_po_prn_details.ORDER_NO = TB2.ORDER_NO
				AND trn_po_prn_details.ORDER_YEAR = TB2.ORDER_YEAR
				AND trn_po_prn_details.ITEM = TB2.ITEM
			) AS PRN_QTY,
			SUM(

				IF (
					trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'MRN',
					trn_po_prn_details_sales_order_stock_transactions.QTY,
					0
				)
			) AS MRN_QTY,
			SUM(

				IF (
					trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'MRN_EXTRA',
					trn_po_prn_details_sales_order_stock_transactions.QTY,
					0
				)
			) AS MRN_EXTRA_QTY,
			SUM(

				IF (
					trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'ISSUE',
					trn_po_prn_details_sales_order_stock_transactions.QTY,
					0
				)
			) AS ISSUE_QTY,
			SUM(

				IF (
					trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'GATE_PASS',
					trn_po_prn_details_sales_order_stock_transactions.QTY,
					0
				)
			) AS GP_QTY,
			SUM(

				IF (
					trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'GATE_PASS_IN',
					trn_po_prn_details_sales_order_stock_transactions.QTY,
					0
				)
			) AS GP_IN_QTY,
			SUM(

				IF (
					trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'RETURNED_TO_STORES',
					trn_po_prn_details_sales_order_stock_transactions.QTY,
					0
				)
			) AS RET_TO_STORE_QTY
		FROM
			(
				SELECT
					*
				FROM
					(
						SELECT
							trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
							trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
							trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER,
							trn_po_prn_details_sales_order_stock_transactions.ITEM,
							trn_po_prn_details_sales_order.REQUIRED,
							mst_locations.intCompanyId AS ORDER_COMPANY,
							mst_locations.strName AS location,
							(
								SELECT
									GROUP_CONCAT(mst_locations.intCompanyId)
								FROM
									ware_fabricreceivedheader
								INNER JOIN mst_locations ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId
								WHERE
									ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO
								AND ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
							) AS FABRIC_IN_COMPANIES,
							1 AS type
						FROM
							trn_po_prn_details_sales_order_stock_transactions
						INNER JOIN trn_po_prn_details_sales_order ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = trn_po_prn_details_sales_order.ORDER_NO
						AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR
						AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER
						AND trn_po_prn_details_sales_order_stock_transactions.ITEM = trn_po_prn_details_sales_order.ITEM
						INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO
						AND trn_orderheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
						INNER JOIN mst_locations ON mst_locations.intId = trn_orderheader.intLocationId
						INNER JOIN trn_orderheader_approvedby AS AP ON AP.intOrderNo = trn_orderheader.intOrderNo
                        AND AP.intYear = trn_orderheader.intOrderYear
                        AND AP.intStatus = 0
                        AND AP.intApproveLevelNo = (
                            trn_orderheader.intApproveLevelStart + 1 - trn_orderheader.intStatus
                        )
                        WHERE 
                         date(AP.dtApprovedDate) >= '$dateFrom'
				         AND date(AP.dtApprovedDate) <= '$dateTo'
						GROUP BY
							trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
							trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
							trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER,
							trn_po_prn_details_sales_order_stock_transactions.ITEM
						HAVING
							FIND_IN_SET('$companyId', FABRIC_IN_COMPANIES)
						OR ORDER_COMPANY = '$companyId'
						UNION ALL
							SELECT
								trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
								trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
								trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER,
								trn_po_prn_details_sales_order_stock_transactions.ITEM,
								trn_po_prn_details_sales_order.REQUIRED,
								mst_locations.intCompanyId AS ORDER_COMPANY,
								mst_locations.strName AS location,
							  
								(
									SELECT
										GROUP_CONCAT(
											DISTINCT mst_locations.intCompanyId
										)
									FROM
										ware_fabricreceivedheader
									INNER JOIN mst_locations ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId
									WHERE
										ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO
									AND ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
								) AS FABRIC_IN_COMPANIES,
								2 AS type
							FROM
								trn_po_prn_details_sales_order_stock_transactions
							LEFT JOIN trn_po_prn_details_sales_order ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = trn_po_prn_details_sales_order.ORDER_NO
							AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR
							AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER
							AND trn_po_prn_details_sales_order_stock_transactions.ITEM = trn_po_prn_details_sales_order.ITEM
							INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO
							AND trn_orderheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
							INNER JOIN mst_locations ON mst_locations.intId = trn_orderheader.intLocationId
							INNER JOIN trn_orderheader_approvedby AS AP ON AP.intOrderNo = trn_orderheader.intOrderNo
                            AND AP.intYear = trn_orderheader.intOrderYear
                            AND AP.intStatus = 0
                            AND AP.intApproveLevelNo = (
                                trn_orderheader.intApproveLevelStart + 1 - trn_orderheader.intStatus
                            )
                            WHERE 
                             date(AP.dtApprovedDate) >= '$dateFrom'
                             AND date(AP.dtApprovedDate) <= '$dateTo'
							GROUP BY
								trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
								trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
								trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER,
								trn_po_prn_details_sales_order_stock_transactions.ITEM
							HAVING
								(
									FIND_IN_SET('$companyId', FABRIC_IN_COMPANIES)
									OR ORDER_COMPANY = '$companyId'
								)
							UNION ALL
								SELECT
									trn_po_prn_details_sales_order.ORDER_NO,
									trn_po_prn_details_sales_order.ORDER_YEAR,
									trn_po_prn_details_sales_order.SALES_ORDER,
									trn_po_prn_details_sales_order.ITEM,
									trn_po_prn_details_sales_order.REQUIRED,
									mst_locations.intCompanyId AS ORDER_COMPANY,
									mst_locations.strName AS location,
									(
										SELECT
											GROUP_CONCAT(
												DISTINCT mst_locations.intCompanyId
											)
										FROM
											ware_fabricreceivedheader
										INNER JOIN mst_locations ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId
										WHERE
											ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order.ORDER_NO
										AND ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order.ORDER_YEAR
									) AS FABRIC_IN_COMPANIES,
									1 AS type
								FROM
									trn_po_prn_details_sales_order_stock_transactions
								RIGHT JOIN trn_po_prn_details_sales_order ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = trn_po_prn_details_sales_order.ORDER_NO
								AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR
								AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER
								AND trn_po_prn_details_sales_order_stock_transactions.ITEM = trn_po_prn_details_sales_order.ITEM
								INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = trn_po_prn_details_sales_order.ORDER_NO
								AND trn_orderheader.intOrderYear = trn_po_prn_details_sales_order.ORDER_YEAR
								INNER JOIN mst_locations ON mst_locations.intId = trn_orderheader.intLocationId
								INNER JOIN trn_orderheader_approvedby AS AP ON AP.intOrderNo = trn_orderheader.intOrderNo
                                AND AP.intYear = trn_orderheader.intOrderYear
                                AND AP.intStatus = 0
                                AND AP.intApproveLevelNo = (
                                    trn_orderheader.intApproveLevelStart + 1 - trn_orderheader.intStatus
                                )
                                WHERE 
                                 date(AP.dtApprovedDate) >= '$dateFrom'
                                 AND date(AP.dtApprovedDate) <= '$dateTo'
								GROUP BY
									trn_po_prn_details_sales_order.ORDER_NO,
									trn_po_prn_details_sales_order.ORDER_YEAR,
									trn_po_prn_details_sales_order.SALES_ORDER,
									trn_po_prn_details_sales_order.ITEM
								HAVING
									(
										FIND_IN_SET('1', FABRIC_IN_COMPANIES)
										OR ORDER_COMPANY = '1'
									)
					) AS TB1
				GROUP BY
					TB1.ORDER_NO,
					TB1.ORDER_YEAR,
					TB1.SALES_ORDER,
					TB1.ITEM
			) AS TB2
		LEFT JOIN trn_po_prn_details_sales_order_stock_transactions ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = TB2.ORDER_NO
		AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = TB2.ORDER_YEAR
		AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = TB2.SALES_ORDER
		AND trn_po_prn_details_sales_order_stock_transactions.ITEM = TB2.ITEM
		INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = TB2.ORDER_NO
		AND trn_orderheader.intOrderYear = TB2.ORDER_YEAR
        INNER JOIN trn_orderheader_approvedby ON trn_orderheader_approvedby.intOrderNo = TB2.ORDER_NO
											AND trn_orderheader_approvedby.intYear = TB2.ORDER_YEAR
											AND trn_orderheader_approvedby.intStatus = 0
											AND trn_orderheader_approvedby.intApproveLevelNo = (
												trn_orderheader.intApproveLevelStart + 1 - trn_orderheader.intStatus
											)
		LEFT JOIN trn_po_prn_details_sales_order ON trn_po_prn_details_sales_order.ORDER_NO = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO
		AND trn_po_prn_details_sales_order.ORDER_YEAR = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
		AND trn_po_prn_details_sales_order.SALES_ORDER = trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER
		AND trn_po_prn_details_sales_order.ITEM = trn_po_prn_details_sales_order_stock_transactions.ITEM
		INNER JOIN trn_orderdetails ON TB2.ORDER_NO = trn_orderdetails.intOrderNo
			AND TB2.ORDER_YEAR = trn_orderdetails.intOrderYear
			AND TB2.SALES_ORDER = trn_orderdetails.intSalesOrderId
		INNER JOIN mst_item ON TB2.ITEM = mst_item.intId
                LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
		INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
		INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
		INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId	
		WHERE 
		((trn_orderheader.intStatus > 0 AND trn_orderheader.intStatus < (trn_orderheader.intApproveLevelStart + 1)) OR trn_orderheader.intStatus = -10)  
		AND mst_item.intStatus = '1'
		GROUP BY
			TB2.ORDER_NO,
			TB2.ORDER_YEAR,
			TB2.SALES_ORDER,
			TB2.ITEM,
			trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID
	) AS TB3
GROUP BY
	TB3.ORDER_NO,
	TB3.ORDER_YEAR,
	TB3.SALES_ORDER,
	TB3.ITEM";

                                    $result4 = $db->RunQuery($sql4);
                                    while ($row = mysqli_fetch_array($result4)) {
                                        $issuedTo_HO = round($row['ISSUE_QTY'], 4);
                                        $GP_From_HO = round($row['GP_QTY'], 4);
                                        $IssuedToNon_HO = round($row['ISSUE_QTY_OTHER_LOC'], 4);
                                        $PRI = round($row['REQUIRED'], 4);
                                        $returnable = $issuedTo_HO + max($GP_From_HO,$IssuedToNon_HO) - $PRI;
                                        if($returnable < 0){
                                            $returnable = 0;
                                        }
                                        $type = $row['type'];
                                        if($type == 2){
                                            $backColor = "background-color: #00CCFF";
                                        }else{
                                            $backColor = "";
                                        }
                                        ?>
                                        <tr style="<?php echo $backColor; ?>">
                                            <td class="normalfnt" style="text-align: center">
                                                &nbsp;<?php echo $row['date']; ?>&nbsp;</td>
                                            <td class="normalfnt" style="text-align: center">
                                                &nbsp;<?php echo $row['ORDER_NO'].'/'.$row['ORDER_YEAR']; ?>&nbsp;</td>
                                            <td class="normalfnt" style="text-align: center">
                                                &nbsp;<?php echo $row['location']; ?>&nbsp;</td>
                                            <td class="normalfnt" style="text-align: center">
                                                &nbsp;<?php echo $row['mainCatName']; ?>&nbsp;</td>
                                            <td class="normalfnt" style="text-align: center">
                                                &nbsp;<?php echo $row['subCatName']; ?>&nbsp;</td>
                                            
                                            <td class="normalfnt" style="text-align: center">
                                                &nbsp;<?php if($row['supItemCode']!=null) echo $row['supItemCode']; else echo $row['strCode']; ?>&nbsp;</td>
                                            <td class="normalfnt" style="text-align: center">
                                                &nbsp;<?php echo $row['itemName']; ?>&nbsp;</td>
                                            <td class="normalfnt" style="text-align: center">
                                                &nbsp;<?php echo $row['UOM']; ?>&nbsp;</td>
                                            <td class="normalfnt" style="text-align: right">
                                                &nbsp;<?php echo $PRI; ?>&nbsp;</td>
                                            <td class="normalfnt" style="text-align: right">
                                                &nbsp;<?php echo $issuedTo_HO; ?>&nbsp;</td>
                                            <td class="normalfnt" style="text-align: right">
                                                &nbsp;<?php echo $GP_From_HO;  ?>&nbsp;</td>
                                            <td class="normalfnt" style="text-align: right">
                                                &nbsp;<?php echo $IssuedToNon_HO; ?>&nbsp;</td>
                                            <td class="normalfnt" style="text-align: right">
                                                &nbsp;<?php echo round($returnable,4);//$issuedTo_HO + max($GP_From_HO,$IssuedToNon_HO)-$PRI; round($row['returnable'],4);  ?>&nbsp;</td>
                                        </tr>
                                    <?php } ?>

                                </table>
                            </td>
                        </tr>

                    </table>
                </td>
                <td width="6%">&nbsp;</td>
            </tr>
        </table>
        </td>
        </tr>
        <tr>
    </div>
</form>
</body>
</html>
<?php

function getHOStock($item)
{

    global $db;

    $sql = "SELECT
			Sum(ware_stocktransactions_bulk.dblQty) as qty 
			FROM `ware_stocktransactions_bulk`
			WHERE
			ware_stocktransactions_bulk.intItemId = '$item' AND
			ware_stocktransactions_bulk.intLocationId = 2
			";

    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    return $row['qty'];

}

function get_approved_non_grn_pos($item, $location)
{

    global $db;

    $sql = "SELECT
			sum(trn_podetails.dblQty-trn_podetails.dblGRNQty) as qty
			FROM
			trn_podetails
			INNER JOIN trn_poheader ON trn_podetails.intPONo = trn_poheader.intPONo AND trn_podetails.intPOYear = trn_poheader.intPOYear
			WHERE
			trn_poheader.intStatus = 1 AND
			trn_poheader.intDeliveryTo = '$location' AND
			trn_podetails.intItem = '$item'
			";

    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    return $row['qty'];


}
function get_ho_loc($companyId){
    global $db;
    $sql="SELECT
			mst_locations.intId
			FROM `mst_locations`
			WHERE
			mst_locations.COMPANY_MAIN_STORES_LOCATION = 1 AND
			mst_locations.intCompanyId = '$companyId'
			";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    return $row['intId'];

}
?>

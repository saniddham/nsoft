<?php
session_start();
$backwardseperator = "../../../../";
$mainPath          = $_SESSION['mainPath'];
$location 	       = $_SESSION['CompanyID'];
$intUser 		   = $_SESSION["userId"];
$company 	       = $_SESSION['headCompanyId'];
$thisFilePath      =  $_SERVER['PHP_SELF'];
$resultMrn = $_REQUEST['mrn'];
$resultIssue = $_REQUEST['issue'];
$resultPri = $_REQUEST['pri'];
$resultPrn = $_REQUEST['prn'];
$rowIndex = 0;
$item = $_REQUEST['item'];

$arrMrn = json_decode($resultMrn,true);
$arrIssue = json_decode($resultIssue,true);
$arrPri = json_decode($resultPri,true);
$arrPrn = json_decode($resultPrn,true);


//include $backwardseperator."dataAccess/Connector.php";



?>
<head>
    <title>Details about items</title>
</head>

<body>
<style type="text/css">


</style>


<form id="frmDetailsPopup" name="frmDetailsPopup" method="post" >
    <div id="item" value="<?php echo $item;?>" style="display: none;">
    </div>
    <div align="center">
        <div class="trans_layoutD" style="width:1000px">
           <div style="width:1000px;height:300px;overflow:scroll" >
               <div align="right">
                       <img src="images/Tclose.jpg" width="92" height="24" id="butClose1" name="butClose1" class="mouseover"  /></td>

               </div>
                <table width="1000" class="bordered" id="tblItemsPopup" >
                    <thead>
                    <tr >
                        <th width="20%" >Order Number / Order Year</th>
                        <th width="8%" >PRI Qty</th>
                        <th width="8%" >PRN Qty</th>
                        <th width="8%">Mrn Qty</th>
                        <th width="8%" >Issue Qty</th>
                        <th width="8%">Issue Details</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php

                    $mrnOrderNos = array_keys($arrMrn);
                    for($i=0;$i<sizeof($mrnOrderNos);$i++) {
                        ?>
                        <tr>
                            <td id="orderString<?php echo $rowIndex;?>" value="<?php echo $mrnOrderNos[$i]; ?>"><?php echo $mrnOrderNos[$i]; ?></td>
                            <td><?php if(array_key_exists($mrnOrderNos[$i],$arrPri))
                                echo round($arrPri[$mrnOrderNos[$i]],4);
                                else echo "0"; ?>
                            </td>
                            <td>
                            <?php if(array_key_exists($mrnOrderNos[$i],$arrPrn))
                                echo round($arrPrn[$mrnOrderNos[$i]],4);
                            else echo "0"; ?></td>
                            <td><?php if(array_key_exists($mrnOrderNos[$i],$arrMrn)){
                                echo round($arrMrn[$mrnOrderNos[$i]],4);
                                }
                            else{
                                echo "0";

                            }?></td>
                            <td><?php if(array_key_exists($mrnOrderNos[$i],$arrIssue)) echo $arrIssue[$mrnOrderNos[$i]]; else echo "0";?></td>
                            <td><?php if(array_key_exists($mrnOrderNos[$i],$arrIssue)){ ?><img id="bb" name="bb" src="images/aad.png" width="30" height="24" align="middle" ><?php }?></td>
                        </tr>
                        <?php
                        $rowIndex ++;
                    }
                    $issueOrderNos = array_keys($arrIssue);
                    for($i=0;$i<sizeof($issueOrderNos);$i++) {
                    if(in_array($issueOrderNos[$i],$mrnOrderNos)) {
                    }
                    else{
                        ?>
                        <tr>

                            <td id="orderString<?php echo $rowIndex;?>" value="<?php echo $issueOrderNos[$i]; ?>"><?php echo $issueOrderNos[$i]; ?></td>
                            <td><?php if(array_key_exists($issueOrderNos[$i],$arrPri))
                                    echo round($arrPri[$issueOrderNos[$i]],4);
                                else echo "0";; ?></td>
                            <td><?php if(array_key_exists($issueOrderNos[$i],$arrPrn))
                                    echo round($arrPrn[$issueOrderNos[$i]],4);
                                else echo "0"; ?>
                            </td>
                            <td><?php if(array_key_exists($issueOrderNos[$i],$arrMrn)){
                            echo round($arrMrn[$issueOrderNos[$i]],4);
                        }
                        else{
                            echo "0";}?></td>
                            <td><?php if(array_key_exists($issueOrderNos[$i],$arrIssue)) echo $arrIssue[$issueOrderNos[$i]]; else echo "0"; ?></td>
                            <td><?php if(array_key_exists($issueOrderNos[$i],$arrIssue)){ ?><img id="bb" name="bb" src="images/aad.png" width="30" height="24" align="middle" ><?php }?></td>


                        </tr>
                        <?php
                         $rowIndex ++;
                    }
                    }
                    $priOrderNos = array_keys($arrPri);
                     for($i=0;$i<sizeof($priOrderNos);$i++) {
                        if(in_array($priOrderNos[$i],$mrnOrderNos) || in_array($priOrderNos[$i],$issueOrderNos)) {
                        }
                        else{

                        ?>
                    <tr>
                        <td id="orderString<?php echo $rowIndex;?>" value="<?php echo $priOrderNos[$i]; ?>"><?php echo $priOrderNos[$i]; ?></td>
                        <td><?php echo round($arrPri[$priOrderNos[$i]],4); ?>
                        </td>
                        <td><?php echo round($arrPrn[$priOrderNos[$i]],4);?></td>
                        <td><?php echo "0"; ?></td>
                        <td><?php echo "0"; ?></td>
                        <td></td>

                    </tr>
                    <?php
                            $rowIndex ++;
                         }
                         }
                    ?>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</form>



</body>

<script type="text/javascript">
    $("#frmDetailsPopup #tblItemsPopup #bb").click(function(){
        var rowindex = $(this).closest('tr').index();
        var key = "orderString".concat(rowindex);
        var order = $('#'+key).attr('value');
        var item = $("#frmDetailsPopup #item").attr('value');
        loadSecondPopUp(order, item);

    });

    function loadSecondPopUp(orderString, item)
    {
       window.open(basePath+'rptItemWiseStockSecondPopUp.php?orderString='+orderString+'&item='+item);

    }

</script>
<?php
ini_set('max_execution_time', 1111111111111);
ini_set('display_errors', 0);
session_start();

$companyId 	= $sessions->getCompanyId();
$userId 	= $sessions->getUserId();
$locationId = $sessions->getLocationId();

$programCode = 'P1124';

require_once "class/cls_permisions.php";
$objpermisionget = new cls_permisions($db);
require_once "class/tables/sys_approvelevels.php";
$obj_sys_approvelevels = new sys_approvelevels($db);


$permi_price_view = $objpermisionget->boolSPermision(8);
$str_price_view = 'hidden';

if ($permi_price_view)
    $str_price_view = '';
$year                   = $_REQUEST['year'];
$orderNo				= $_REQUEST['order'];
$orderArray				= explode(',',$orderNo);
$report_type		 	= $_REQUEST['report_type'];
$head_office_loc 		= get_ho_loc($companyId);
?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Order Wise Stock movement Report</title>
        <script type="text/javascript" src="presentation/procurement/reports/reports.js"></script>
        <script type="text/javascript"
                src="presentation/customerAndOperation/bulk/placeOrder/listing/rptBulkOrder-js.js?n=1"></script>
        <style>
            .break {
                page-break-before: always;
            }

            @media print {
                .noPrint {
                    display: none;
                }
            }

            #apDiv1 {
                position: absolute;
                left: 237px;
                top: 175px;
                width: 619px;
                height: 235px;
                z-index: 1;
            }

            .APPROVE {
                font-size: 16px;
                font-weight: bold;
                color: #00C;
            }
        </style>
    </head>

    <body>
<form id="frmOrderStockMovementReport" name="frmOrderStockMovementReport" method="post" action="orderwise_stock_movement_rpt.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80"
valign="top"><?php include 'presentation/procurement/reports/reportHeader.php' ?></td>
<td width="20%"></td>
</tr>
<tr>
<td colspan="3">
<div id="divPoNo" style="display:none"><?php echo $year ?>/<?php echo $orderNo ?></div>
</td>
</tr>
</table>
<div align="center">
<?php 
if($report_type ==1) //suvini
{?>
<div style="background-color:#FFF"><strong>Order Wise Stock Movement Report - Quantity </strong><strong></strong>
</div>
<?php
}
elseif($report_type ==2)//suvini
{
?>
<div style="background-color:#FFF"><strong>Order Wise Stock Movement Report - Value</strong><strong></strong>
</div>	
<?php
}
?>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
<td>
<table width="100%">
<tr>
<td colspan="10" align="center" bgcolor="#FFFFFF">
</td>
</tr>
<tr>
<td width="1%"><span class="normalfnt"><strong>Order No:</strong></span></td>
<td width="9%" class="normalfnt"><strong><?php  echo $year. "/" ?></strong>
	
<strong><?php  echo $orderNo; ?>
</strong></td>
</tr>
<tr height="50">
<td class="normalfnt"><div style="background-color: #FFE6E6; position: absolute; top: 150px; left: 250px; width: 40px; height: 40px; "></div> Order wise Additional Items</td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<table width="100%">
<tr>
<td colspan="20">
<?php 
?>
<table width="100%" class="bordered" id="tblMainGrid" cellspacing="0"
cellpadding="0">
<?php 
if($report_type ==1)
{ ?>
<tr>
<th width="2%">Date</th>
<th width="10%">Order</th>
<th width="6">Graphic No</th>
<th width="6">Location</th>
<th width="8%">Main Category</th>
<th width="8%">Sub Category</th>
<th width="4%"> Code</th>
<th width="10%">Item</th>
<th width="10%">Sales Order</th>
<th width="8%">UOM</th>
<th width="3%" height="22">PRI</th>
<th width="3%" height="22">PRN</th>
<th width="3%">Purchased (All Loc)</th>
<th width="3%">MRN (All Loc)</th>
<th width="3%">Issued (All Loc)</th>
<th width="3%">Returned (All Loc)</th>
<th width="3%">GatePass Out (From HO)</th>
<th width="3%">Bal To Issue</th>
</tr>
<?php
}
else
{
?>	
<tr>
<th width="2%">Date</th>
<th width="10%">Order</th>
<th width="6">Graphic No</th>
<th width="6">Location</th>
<th width="8%">Main Category</th>
<th width="8%">Sub Category</th>
<th width="4%"> Code</th>
<th width="10%">Item</th>
<th width="10%">Sales Order</th>
<th width="8%">UOM</th>
<th width="3%" height="22">PRI(USD)(All Loc)</th>
<th width="3%" height="22">PRN (USD)(All Loc)</th>
<th width="3%">Purchased (USD)(All Loc)</th>
<th width="3%">MRN (USD) (All Loc)</th>
<th width="3%">Issued (USD)(All Loc)</th>
<th width="3%">Returned (USD)(All Loc)</th>
<th width="3%">GatePass Out (From HO)(USD)</th>
<th width="3%">Bal To Issue (USD)</th>
</tr>
<?php 
}
 ?>
<?php

$sql4 = "SELECT
TB3.ORDER_COMPANY,
TB3.location AS location,
TB3.date,
TB3.ORDER_NO,
TB3.ORDER_YEAR,
TB3.SALES_ORDER,
TB3.SALES_ORDER_String AS salesOrderStr,
TB3.graphic,
TB3.ITEM,
TB3.REQUIRED,
TB3.TRANSACTION_LOCATION,
TB3.PO_QTY,
TB3.PRN_QTY,
TB3.mainCatName,
TB3.subCatName,
TB3.strCode,
TB3.supItemCode,
TB3.itemName,
TB3.itemId AS itemId,
TB3.UOM,
TB3.type,
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.MRN_QTY,0)) as MRN_QTY,
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.MRN_EXTRA_QTY,0)) as MRN_EXTRA_QTY,
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.ISSUE_QTY,0)) as ISSUE_QTY,
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.GP_QTY,0)) as GP_QTY,
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.GP_IN_QTY,0)) as GP_IN_QTY,
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.RET_TO_STORE_QTY,0)) as RET_TO_STORE_QTY,
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.MRN_QTY,0)) as MRN_QTY_OTHER_LOC,
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.MRN_EXTRA_QTY,0)) as MRN_EXTRA_QTY_OTHER_LOC,
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.ISSUE_QTY,0)) as ISSUE_QTY_OTHER_LOC,
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.GP_QTY,0)) as GP_QTY_OTHER_LOC,
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.GP_IN_QTY,0)) as GP_IN_QTY_OTHER_LOC,
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.RET_TO_STORE_QTY,0)) as RET_TO_STORE_QTY_OTHER_LOC
FROM
(
SELECT
TB2.ORDER_COMPANY,
TB2.ORDER_NO,
TB2.ORDER_YEAR,
TB2.location,
TB2.ITEM,
TB2.SALES_ORDER,
TB2.type,
CONCAT (trn_orderdetails.strSalesOrderNo,'/',trn_orderdetails.strCombo,'/',trn_orderdetails.strPrintName) AS SALES_ORDER_String,
trn_sampleinfomations.strGraphicRefNo as graphic,
GROUP_CONCAT(DISTINCT date(trn_orderheader_approvedby.dtApprovedDate ))AS date,
mst_maincategory.strName AS mainCatName,
mst_subcategory.strName AS subCatName,
mst_item.strCode,
 CONCAT(mst_supplier.strCode,mst_item.strCode) as supItemCode,
mst_item.intId AS itemId,
mst_item.strName AS itemName,
mst_units.strName AS UOM,
TB2.REQUIRED,
trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID AS TRANSACTION_LOCATION,

(
SELECT
trn_po_prn_details.PURCHASED_QTY
FROM
trn_po_prn_details
WHERE
trn_po_prn_details.ORDER_NO = TB2.ORDER_NO
AND trn_po_prn_details.ORDER_YEAR = TB2.ORDER_YEAR
AND trn_po_prn_details.ITEM = TB2.ITEM
) AS PO_QTY,
(
SELECT
trn_po_prn_details.PRN_QTY
FROM
trn_po_prn_details
WHERE
trn_po_prn_details.ORDER_NO = TB2.ORDER_NO
AND trn_po_prn_details.ORDER_YEAR = TB2.ORDER_YEAR
AND trn_po_prn_details.ITEM = TB2.ITEM
) AS PRN_QTY,
SUM(

IF (
trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'MRN',
trn_po_prn_details_sales_order_stock_transactions.QTY,
0
)
) AS MRN_QTY,
SUM(

IF (
trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'MRN_EXTRA',
trn_po_prn_details_sales_order_stock_transactions.QTY,
0
)
) AS MRN_EXTRA_QTY,
SUM(

IF (
trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'ISSUE',
trn_po_prn_details_sales_order_stock_transactions.QTY,
0
)
) AS ISSUE_QTY,
SUM(

IF (
trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'GATE_PASS',
trn_po_prn_details_sales_order_stock_transactions.QTY,
0
)
) AS GP_QTY,
SUM(

IF (
trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'GATE_PASS_IN',
trn_po_prn_details_sales_order_stock_transactions.QTY,
0
)
) AS GP_IN_QTY,
SUM(

IF (
trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'RETURNED_TO_STORES',
trn_po_prn_details_sales_order_stock_transactions.QTY,
0
)
) AS RET_TO_STORE_QTY
FROM
(
SELECT
*
FROM
(
SELECT
trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER,
trn_po_prn_details_sales_order_stock_transactions.ITEM,
trn_po_prn_details_sales_order.REQUIRED,
mst_locations.intCompanyId AS ORDER_COMPANY,
mst_locations.strName AS location,
(
SELECT
GROUP_CONCAT(mst_locations.intCompanyId)
FROM
ware_fabricreceivedheader
INNER JOIN mst_locations ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId
WHERE
ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO
AND ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
) AS FABRIC_IN_COMPANIES,
1 AS type
FROM
trn_po_prn_details_sales_order_stock_transactions
INNER JOIN trn_po_prn_details_sales_order ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = trn_po_prn_details_sales_order.ORDER_NO
AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR
AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER
AND trn_po_prn_details_sales_order_stock_transactions.ITEM = trn_po_prn_details_sales_order.ITEM
INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO
AND trn_orderheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
INNER JOIN mst_locations ON mst_locations.intId = trn_orderheader.intLocationId
WHERE trn_orderheader.intOrderNo In ($orderNo)
AND trn_orderheader.intOrderYear = '$year'
GROUP BY
trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER,
trn_po_prn_details_sales_order_stock_transactions.ITEM
HAVING
FIND_IN_SET('$companyId', FABRIC_IN_COMPANIES)
OR ORDER_COMPANY = '$companyId'
UNION ALL
SELECT
trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER,
trn_po_prn_details_sales_order_stock_transactions.ITEM,
trn_po_prn_details_sales_order.REQUIRED,
mst_locations.intCompanyId AS ORDER_COMPANY,
mst_locations.strName AS location,

(
SELECT
	GROUP_CONCAT(
		DISTINCT mst_locations.intCompanyId
	)
FROM
	ware_fabricreceivedheader
INNER JOIN mst_locations ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId
WHERE
	ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO
AND ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
) AS FABRIC_IN_COMPANIES,
2 AS type
FROM
trn_po_prn_details_sales_order_stock_transactions
LEFT JOIN trn_po_prn_details_sales_order ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = trn_po_prn_details_sales_order.ORDER_NO
AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR
AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER
AND trn_po_prn_details_sales_order_stock_transactions.ITEM = trn_po_prn_details_sales_order.ITEM
INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO
AND trn_orderheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
INNER JOIN mst_locations ON mst_locations.intId = trn_orderheader.intLocationId
WHERE
trn_po_prn_details_sales_order.ORDER_NO IS NULL
AND trn_orderheader.intOrderNo In ($orderNo)
AND trn_orderheader.intOrderYear = '$year'
GROUP BY
trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER,
trn_po_prn_details_sales_order_stock_transactions.ITEM
HAVING
(
FIND_IN_SET('$companyId', FABRIC_IN_COMPANIES)
OR ORDER_COMPANY = '$companyId'
)
UNION ALL
SELECT
trn_po_prn_details_sales_order.ORDER_NO,
trn_po_prn_details_sales_order.ORDER_YEAR,
trn_po_prn_details_sales_order.SALES_ORDER,
trn_po_prn_details_sales_order.ITEM,
trn_po_prn_details_sales_order.REQUIRED,
mst_locations.intCompanyId AS ORDER_COMPANY,
mst_locations.strName AS location,
(
SELECT
GROUP_CONCAT(
DISTINCT mst_locations.intCompanyId
)
FROM
ware_fabricreceivedheader
INNER JOIN mst_locations ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId
WHERE
ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order.ORDER_NO
AND ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order.ORDER_YEAR
) AS FABRIC_IN_COMPANIES,
1 AS type
FROM
trn_po_prn_details_sales_order_stock_transactions

RIGHT JOIN 

trn_po_prn_details_sales_order ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = trn_po_prn_details_sales_order.ORDER_NO
AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR
AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER
AND trn_po_prn_details_sales_order_stock_transactions.ITEM = trn_po_prn_details_sales_order.ITEM
INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = trn_po_prn_details_sales_order.ORDER_NO
AND trn_orderheader.intOrderYear = trn_po_prn_details_sales_order.ORDER_YEAR
INNER JOIN mst_locations ON mst_locations.intId = trn_orderheader.intLocationId
WHERE
trn_po_prn_details_sales_order_stock_transactions.ORDER_NO IS NULL
AND trn_orderheader.intOrderNo In ($orderNo)
AND trn_orderheader.intOrderYear = '$year'
GROUP BY
trn_po_prn_details_sales_order.ORDER_NO,
trn_po_prn_details_sales_order.ORDER_YEAR,
trn_po_prn_details_sales_order.SALES_ORDER,
trn_po_prn_details_sales_order.ITEM
HAVING
(
FIND_IN_SET('$companyId', FABRIC_IN_COMPANIES)
OR ORDER_COMPANY = '$companyId'
)
) AS TB1
GROUP BY
TB1.ORDER_NO,
TB1.ORDER_YEAR,
TB1.SALES_ORDER,
TB1.ITEM
) AS TB2
LEFT JOIN trn_po_prn_details_sales_order_stock_transactions ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = TB2.ORDER_NO
AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = TB2.ORDER_YEAR
AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = TB2.SALES_ORDER
AND trn_po_prn_details_sales_order_stock_transactions.ITEM = TB2.ITEM
INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = TB2.ORDER_NO
AND trn_orderheader.intOrderYear = TB2.ORDER_YEAR
INNER JOIN trn_orderheader_approvedby ON trn_orderheader_approvedby.intOrderNo = TB2.ORDER_NO
AND trn_orderheader_approvedby.intYear = TB2.ORDER_YEAR 
AND trn_orderheader_approvedby.intStatus = 0
AND trn_orderheader_approvedby.intApproveLevelNo = 1
LEFT JOIN trn_po_prn_details_sales_order ON trn_po_prn_details_sales_order.ORDER_NO = TB2.ORDER_NO
AND trn_po_prn_details_sales_order.ORDER_YEAR = TB2.ORDER_YEAR
AND trn_po_prn_details_sales_order.SALES_ORDER = TB2.SALES_ORDER
AND trn_po_prn_details_sales_order.ITEM = TB2.ITEM
INNER JOIN trn_orderdetails ON TB2.ORDER_NO = trn_orderdetails.intOrderNo
AND TB2.ORDER_YEAR = trn_orderdetails.intOrderYear
AND TB2.SALES_ORDER = trn_orderdetails.intSalesOrderId
INNER JOIN trn_sampleinfomations ON trn_orderdetails.intSampleNo = trn_sampleinfomations.intSampleNo AND trn_orderdetails.intSampleYear = trn_sampleinfomations.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
INNER JOIN mst_item ON TB2.ITEM = mst_item.intId
LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId	
GROUP BY
TB2.ORDER_NO,
TB2.ORDER_YEAR,
TB2.SALES_ORDER,
TB2.ITEM,
trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID
) AS TB3
GROUP BY
TB3.ORDER_NO,
TB3.ORDER_YEAR,
TB3.SALES_ORDER,
TB3.ITEM 
ORDER BY 
TB3.ORDER_NO,
TB3.ORDER_YEAR,
TB3.ITEM ,
TB3.SALES_ORDER 
";
//echo $sql4;
$result4 		= $db->RunQuery($sql4);
$orderNo_temp 	='';
$orderYear_temp ='';
$item_temp 		='';
$rowspanPRn = 0;
if($report_type ==1)
{

while ($row 	= mysqli_fetch_array($result4)) 
{

$so_count	  = get_number_of_sos($row['ORDER_NO'],$row['ORDER_YEAR'],$row['ITEM']);
$MRN_QTY      = $row['MRN_QTY'] + $row['MRN_QTY_OTHER_LOC']; //Total
if($row['type'] == '2') 
{
$bal_to_issue = round((($MRN_QTY) - ($row['ISSUE_QTY'] + MAX($row['ISSUE_QTY_OTHER_LOC'], ($row['GP_QTY_OTHER_LOC'])))), 4);
}
else
{
$bal_to_issue = round((($row['REQUIRED']) - ($row['ISSUE_QTY'] + MAX($row['ISSUE_QTY_OTHER_LOC'], ($row['GP_QTY_OTHER_LOC'])))), 4);
}

if ($bal_to_issue < 0)
$bal_to_issue = 0;
$ISSUE_QTY = $row['ISSUE_QTY'] + $row['ISSUE_QTY_OTHER_LOC']; // Total
$returnToStores = $row['RET_TO_STORE_QTY'] +  $row['RET_TO_STORE_QTY_OTHER_LOC']; //Get the return to head office qty not the all the return Qty - - 2017.11.15 required by sashika to take returned qty total
?>
<tr style="background-color:<?php 
if($row['type'] == '2')
{$color = '#FFE6E6'; 
echo $color; 
}else
{ $color = '#FFFFFF';
echo $color;} ?>">
<td title="Date" class="normalfnt" style="text-align: center">
&nbsp;<?php echo $row['date'];?>&nbsp;</td>
<td title="Order" class="normalfnt" style="text-align: center">
&nbsp;<?php echo $row['ORDER_NO'].'/'.$row['ORDER_YEAR']; ?>&nbsp;</td>
<td title="graphic" class="normalfnt" style="text-align: center">
&nbsp;<?php echo $row['graphic']; ?>&nbsp;</td>
<td title="Location" class="normalfnt" style="text-align: center">
&nbsp;<?php echo $row['location']; ?>&nbsp;</td>
<td title="Main Category" class="normalfnt" style="text-align: center">
&nbsp;<?php echo $row['mainCatName']; ?>&nbsp;</td>
<td title="Sub Category" class="normalfnt" style="text-align: center">
&nbsp;<?php echo $row['subCatName']; ?>&nbsp;</td>
<td title="Code" class="normalfnt" style="text-align: center">
&nbsp;<?php if($row['supItemCode']!=null) echo $row['supItemCode']; else echo $row['strCode']; ?>&nbsp;</td>
<td title="Item" class="normalfnt" style="text-align: center">
&nbsp;<?php echo $row['itemName']; ?>&nbsp;</td>
<td title="Sales Order" class="normalfnt" style="text-align: center">
&nbsp;<?php echo $row['salesOrderStr']; ?>&nbsp;</td>
<td title="UOM" class="normalfnt" style="text-align: center">
&nbsp;<?php echo $row['UOM']; ?>&nbsp;</td>
<td valign="top" title="PRI" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round($row['REQUIRED'], 4); ?>&nbsp;</td>
<?php if($orderNo_temp != $row['ORDER_NO'] || $orderYear_temp != $row['ORDER_YEAR'] || $item_temp != $row['ITEM']  )
{  ?>
    <td valign="top" title="PRN" class="normalfnt" 
    <?php if($so_count > 0)
    { $rowspanPRn = $so_count; ?> rowspan="<?php echo $so_count; ?>" <?php 
    } ?> style="text-align: right; ">
    &nbsp;<?php echo round($row['PRN_QTY'], 4); ?>&nbsp;</td>
    
    <td valign="top" title="PURCHASED" class="normalfnt" <?php if($so_count > 0)
    { ?> rowspan="<?php echo $so_count; ?>" 
    <?php } ?> style="text-align: right; ">
    &nbsp;<?php echo round($row['PO_QTY'], 4); ?>&nbsp;</td>

<?php 
} 
else 
{   ?>
	<?php if($rowspanPRn < 0) { ?>
    	<!--<td title="PRN"></td>-->
    <?php } 
    if($rowspanPRn < 0 ) { ?>
    	<!--<td title="PURCHASED"></td>-->
    <?php } ?>

<?php
 }  
 //$rowspanPRn--; // substracting data from rowspanPrn  ?>
<td valign="top" title="MRN" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round($MRN_QTY, 4); ?>&nbsp;</td>
<td valign="top" title="ISSUED" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round($ISSUE_QTY, 4); ?>&nbsp;</td>
<td valign="top" title="RETURNED" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round($returnToStores, 4); ?>&nbsp;</td>
<td valign="top" title="Gp out (From HO)" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round($row['GP_QTY'], 4); ?>&nbsp;</td>
<td valign="top" title="Bal To Issue" class="normalfnt" style="text-align: right">
&nbsp;<?php echo $bal_to_issue ?>&nbsp;</td>

</tr>
<?php 
$orderNo_temp 	=$row['ORDER_NO'];
$orderYear_temp =$row['ORDER_YEAR'];
$item_temp 		=$row['ITEM'];

}
}
else if($report_type ==2)
{
	
while ($row = mysqli_fetch_array($result4))
{
$sql_currency = "SELECT 
mst_item.dblLastPrice, 
mst_item.intCurrency,
mst_financecurrency.strCode,
mst_financeexchangerate.dblBuying as rate,
(
		SELECT
			round(
				sum(
					ware_grnheader.dblExchangeRate * ware_stocktransactions_bulk.dblQty * ware_stocktransactions_bulk.dblGRNRate
				) / sum(
					ware_stocktransactions_bulk.dblQty
				),4
			)
		FROM
			ware_stocktransactions_bulk
		LEFT JOIN ware_grnheader ON ware_grnheader.intGrnNo = ware_stocktransactions_bulk.intGRNNo
		AND ware_grnheader.intGrnYear = ware_stocktransactions_bulk.intGRNYear
		WHERE
			ware_stocktransactions_bulk.intCompanyId = '$companyId'
        AND ware_stocktransactions_bulk.intItemId = '".$row['itemId']."'
	) AS price
FROM mst_item
INNER JOIN mst_financecurrency 
on mst_item.intCurrency = mst_financecurrency.intId
INNER JOIN mst_financeexchangerate on  mst_item.intCurrency = mst_financeexchangerate.intCurrencyId
WHERE 
mst_item.intId ='".$row['itemId']."' 
AND mst_financeexchangerate.intCompanyId = '$companyId' 
AND mst_financeexchangerate.dtmDate = (select MAX(mst_financeexchangerate.dtmDate) 
FROM mst_financeexchangerate 
WHERE mst_financeexchangerate.intCompanyId = '$companyId') ";


$result5 = $db->RunQuery($sql_currency);

while ($row_1 = mysqli_fetch_array($result5))
{
if($row_1['price'] > 0){
    $unit_price = $row_1['price'];
}
else{
    $unit_price     = $row_1['dblLastPrice'];
}
$currency_id    = $row_1['intCurrency'];                          
$exchange_rate  = $row_1['rate'];


$so_count	     = get_number_of_sos($row['ORDER_NO'],$row['ORDER_YEAR'],$row['ITEM']);
$MRN_QTY         = $row['MRN_QTY'] + $row['MRN_QTY_OTHER_LOC']; //Total

if($row['type'] == '2')
{
$bal_to_issue    = round((($MRN_QTY) - ($row['ISSUE_QTY'] + MAX($row['ISSUE_QTY_OTHER_LOC'], ($row['GP_QTY_OTHER_LOC'])))), 4);
 }
 else
 {
$bal_to_issue    = round((($row['REQUIRED']) - ($row['ISSUE_QTY'] + MAX($row['ISSUE_QTY_OTHER_LOC'], ($row['GP_QTY_OTHER_LOC'])))), 4);
 }

if ($bal_to_issue < 0)
$bal_to_issue = 0;
$ISSUE_QTY 		= $row['ISSUE_QTY'] + $row['ISSUE_QTY_OTHER_LOC']; // Total
$returnToStores = $row['RET_TO_STORE_QTY'] +  $row['RET_TO_STORE_QTY_OTHER_LOC'];
   ?>
<tr style="background-color:
<?php if($row['type'] == '2'){$color = '#FFE6E6'; echo $color; }else { $color = '#FFFFFF'; echo $color;} ?>">

<td title="Date" class="normalfnt" style="text-align: center">
&nbsp;<?php echo $row['date'];?>&nbsp;</td>

<td title="Order" class="normalfnt" style="text-align: center">
&nbsp;<?php echo $row['ORDER_NO'].'/'.$row['ORDER_YEAR']; ?>&nbsp;</td>

<td title="graphic" class="normalfnt" style="text-align: center">
&nbsp;<?php echo $row['graphic']; ?>&nbsp;</td>

<td title="Location" class="normalfnt" style="text-align: center">
&nbsp;<?php echo $row['location']; ?>&nbsp;</td>

<td title="Main Category" class="normalfnt" style="text-align: center">
&nbsp;<?php echo $row['mainCatName']; ?>&nbsp;</td>

<td title="Sub Category" class="normalfnt" style="text-align: center">
&nbsp;<?php echo $row['subCatName']; ?>&nbsp;</td>

<td title="Code" class="normalfnt" style="text-align: center">
&nbsp;<?php if($row['supItemCode']!=null) echo $row['supItemCode']; else echo $row['strCode']; ?>&nbsp;</td>

<td title="Item" class="normalfnt" style="text-align: center">
&nbsp;<?php echo $row['itemName']; ?>&nbsp;</td>

<td title="Sales Order" class="normalfnt" style="text-align: center">
&nbsp;<?php echo $row['salesOrderStr']; ?>&nbsp;</td>

<td title="UOM" class="normalfnt" style="text-align: center">
&nbsp;<?php echo $row['UOM']; ?>&nbsp;</td>

<?php
if($currency_id!=1)
{                                            
$sql_rate_usd = "SELECT 
mst_financeexchangerate.dblBuying
FROM
mst_financeexchangerate
WHERE 
mst_financeexchangerate.intCompanyId 	  = '$companyId' 
AND mst_financeexchangerate.intCurrencyId = '1'
AND
mst_financeexchangerate.dtmDate = (select MAX(mst_financeexchangerate.dtmDate)
FROM
mst_financeexchangerate
WHERE
mst_financeexchangerate.intCompanyId = '$companyId') ";

$result_rateUSD = $db->RunQuery($sql_rate_usd);
while($row_rateUSD=mysqli_fetch_array($result_rateUSD))
{
$rate_usd = $row_rateUSD['dblBuying'];

?>
<td valign="top" title="PRI" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round( (round(($row['REQUIRED']),4)*(($unit_price)* $exchange_rate /$rate_usd)), 4); ?>&nbsp;</td>

<?php 
if($orderNo_temp != $row['ORDER_NO'] || $orderYear_temp != $row['ORDER_YEAR'] || $item_temp != $row['ITEM']  )
{  ?>
<td valign="top" title="PRN" class="normalfnt" <?php if($so_count > 0) { $rowspanPRn = $so_count; ?>     rowspan="<?php echo $so_count; ?>" <?php } ?> style="text-align: right; ">
&nbsp;<?php echo round( (round($row['PRN_QTY'],4)*(($unit_price)* $exchange_rate /$rate_usd)), 4); ?>&nbsp;</td>

<td valign="top" title="PURCHASED" class="normalfnt" <?php if($so_count > 0) { ?> rowspan="<?php echo $so_count; ?>"  <?php } ?> style="text-align: right; ">
&nbsp;<?php echo round( (round($row['PO_QTY'],4) *(($unit_price)* $exchange_rate /$rate_usd)), 4); ?>&nbsp;</td>

<?php } 
else {   ?>
<?php 
if($rowspanPRn < 0) { ?>
<!--<td title="PRN"></td>-->
<?php } 
if($rowspanPRn < 0 ) { ?>

<!--<td title="PURCHASED"></td>-->
<?php } ?>
<?php }  $rowspanPRn--; // substracting data from rowspanPrn  ?>

  <td valign="top" title="MRN" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round( ( $MRN_QTY*(($unit_price)* $exchange_rate/$rate_usd)), 4); ?>&nbsp;</td>
    
<td valign="top" title="ISSUED" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round(( $ISSUE_QTY*(($unit_price)* $exchange_rate/$rate_usd)), 4); ?>&nbsp;</td>
    
 
<td valign="top" title="RETURNED" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round(($returnToStores*(($unit_price)* $exchange_rate/$rate_usd)), 4); ?>&nbsp;</td>


<td valign="top" title="Gp out (From HO)" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round(($row['GP_QTY']*(($unit_price)* $exchange_rate/$rate_usd)), 4); ?>&nbsp;</td>

 <td valign="top" title="Bal To Issue" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round(($bal_to_issue*(($unit_price)* $exchange_rate/$rate_usd)), 4); ?>&nbsp;</td>


<?php 
$orderNo_temp 	=$row['ORDER_NO'];
$orderYear_temp =$row['ORDER_YEAR'];
$item_temp 		=$row['ITEM'];
?>
<?php 

}
}//currencu id = another

elseif($currency_id==1)
{
										 
?>			
  
<td valign="top" title="PRI" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round((round(($row['REQUIRED']), 4)*$unit_price),4); ?>&nbsp;</td>

<?php 
if($orderNo_temp != $row['ORDER_NO'] || $orderYear_temp != $row['ORDER_YEAR'] || $item_temp != $row['ITEM']  )
{  ?>
<td valign="top" title="PRN" class="normalfnt" <?php if($so_count > 0) { $rowspanPRn = $so_count; ?> rowspan="<?php echo $so_count; ?>" 
<?php 
} 
?> style="text-align: right; ">
&nbsp;<?php echo round((round($row['PRN_QTY'],4)*$unit_price), 4); ?>&nbsp;</td>

<td valign="top" title="PURCHASED" class="normalfnt" <?php if($so_count > 0) { ?> rowspan="<?php echo $so_count; ?>"  <?php } ?> style="text-align: right; ">
&nbsp;<?php echo round((round($row['PO_QTY'],4)*$unit_price), 4); ?>&nbsp;</td>

<?php } 
else {   ?>
<?php 
if($rowspanPRn < 0)
 { ?>
<!--<td title="PRN"></td>-->
<?php 
} 
if($rowspanPRn < 0 )
 { ?>
<!--<td title="PURCHASED"></td>-->
<?php } ?>
<?php }  $rowspanPRn--; // substracting data from rowspanPrn  ?>

<td valign="top" title="MRN" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round(($MRN_QTY*$unit_price),4); ?>&nbsp;</td>

<td valign="top" title="ISSUED" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round(($ISSUE_QTY*$unit_price),4); ?>&nbsp;</td>

<td valign="top" title="RETURNED" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round(($returnToStores*$unit_price),4); ?>&nbsp;</td>

<td valign="top" title="Gp out (From HO)" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round(($row['GP_QTY']*$unit_price),4); ?>&nbsp;</td>

<td valign="top" title="Bal To Issue" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round(($bal_to_issue *$unit_price),4); ?>&nbsp;</td>


<?php 
$orderNo_temp 	=$row['ORDER_NO'];
$orderYear_temp =$row['ORDER_YEAR'];
$item_temp 		=$row['ITEM'];

} //currency usd
}
}
}
?>

</table>
<a id="link" onclick="toggleTable();" href="#">Summary</a>

<table width="100%" class="bordered" id="tblSummary" cellspacing="0"
cellpadding="0" style="display:none">
<caption><strong>Summary</strong></caption>
<tr>
<th width="10%">Item</th>
<th width="8%">UOM</th>
<th width="3%" height="22">PRI</th>
<th width="3%" height="22">PRN</th>
<th width="3%">Purchased</th>
<th width="3%">MRN</th>
<th width="3%">Issued</th>
<th width="3%">Returned</th>
<th width="5%">GatePass Out (From HO)</th>
<th width="3%">Bal To Issue</th>
</tr>
<?php

$sql_summary = "
select 

TB_ORDER_WISE.ITEM,
sum(TB_ORDER_WISE.REQUIRED) as REQUIRED,
TB_ORDER_WISE.TRANSACTION_LOCATION,
sum(
		TB_ORDER_WISE.PRN_QTY
) AS PRN_QTY,
sum(
    TB_ORDER_WISE.PO_QTY
) AS PO_QTY,
TB_ORDER_WISE.mainCatName,
TB_ORDER_WISE.subCatName,
TB_ORDER_WISE.strCode,
TB_ORDER_WISE.itemName,
TB_ORDER_WISE.itemId AS itemId,
TB_ORDER_WISE.UOM,
TB_ORDER_WISE.type,
SUM(TB_ORDER_WISE.MRN_QTY) as MRN_QTY,
SUM(TB_ORDER_WISE.MRN_EXTRA_QTY) as MRN_EXTRA_QTY,
SUM(TB_ORDER_WISE.ISSUE_QTY) as ISSUE_QTY,
SUM(TB_ORDER_WISE.GP_QTY) as GP_QTY,
SUM(TB_ORDER_WISE.GP_IN_QTY) as GP_IN_QTY,
SUM(TB_ORDER_WISE.RET_TO_STORE_QTY) as RET_TO_STORE_QTY,
SUM(TB_ORDER_WISE.MRN_QTY_OTHER_LOC) as MRN_QTY_OTHER_LOC,
SUM(TB_ORDER_WISE.MRN_EXTRA_QTY_OTHER_LOC) as MRN_EXTRA_QTY_OTHER_LOC,
SUM(TB_ORDER_WISE.ISSUE_QTY_OTHER_LOC) as ISSUE_QTY_OTHER_LOC,
SUM(TB_ORDER_WISE.GP_QTY_OTHER_LOC) as GP_QTY_OTHER_LOC,
SUM(TB_ORDER_WISE.GP_IN_QTY_OTHER_LOC) as GP_IN_QTY_OTHER_LOC,
SUM(TB_ORDER_WISE.RET_TO_STORE_QTY_OTHER_LOC) as RET_TO_STORE_QTY_OTHER_LOC

 from (


SELECT
TB3.ORDER_COMPANY,
TB3.location AS location,
TB3.date,
TB3.ORDER_NO,
TB3.ORDER_YEAR,
TB3.SALES_ORDER,
TB3.SALES_ORDER_String AS salesOrderStr,
TB3.graphic,
TB3.ITEM,
TB3.REQUIRED,
TB3.TRANSACTION_LOCATION,
TB3.PO_QTY,
TB3.PRN_QTY,
TB3.mainCatName,
TB3.subCatName,
TB3.strCode,
TB3.itemName,
TB3.itemId AS itemId,
TB3.UOM,
TB3.type,
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.MRN_QTY,0)) as MRN_QTY,
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.MRN_EXTRA_QTY,0)) as MRN_EXTRA_QTY,
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.ISSUE_QTY,0)) as ISSUE_QTY,
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.GP_QTY,0)) as GP_QTY,
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.GP_IN_QTY,0)) as GP_IN_QTY,
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.RET_TO_STORE_QTY,0)) as RET_TO_STORE_QTY,
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.MRN_QTY,0)) as MRN_QTY_OTHER_LOC,
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.MRN_EXTRA_QTY,0)) as MRN_EXTRA_QTY_OTHER_LOC,
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.ISSUE_QTY,0)) as ISSUE_QTY_OTHER_LOC,
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.GP_QTY,0)) as GP_QTY_OTHER_LOC,
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.GP_IN_QTY,0)) as GP_IN_QTY_OTHER_LOC,
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.RET_TO_STORE_QTY,0)) as RET_TO_STORE_QTY_OTHER_LOC
FROM
(
SELECT
TB2.ORDER_COMPANY,
TB2.ORDER_NO,
TB2.ORDER_YEAR,
TB2.location,
TB2.ITEM,
TB2.SALES_ORDER,
TB2.type,
CONCAT (trn_orderdetails.strSalesOrderNo,'/',trn_orderdetails.strCombo,'/',trn_orderdetails.strPrintName) AS SALES_ORDER_String,
trn_sampleinfomations.strGraphicRefNo as graphic,
GROUP_CONCAT(DISTINCT date(trn_orderheader_approvedby.dtApprovedDate ))AS date,
mst_maincategory.strName AS mainCatName,
mst_subcategory.strName AS subCatName,
mst_item.strCode,
mst_item.intId AS itemId,
mst_item.strName AS itemName,
mst_units.strName AS UOM,
TB2.REQUIRED,
trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID AS TRANSACTION_LOCATION,
(
    SELECT
        trn_po_prn_details.PURCHASED_QTY
    FROM
        trn_po_prn_details
    WHERE
        trn_po_prn_details.ORDER_NO = TB2.ORDER_NO
    AND trn_po_prn_details.ORDER_YEAR = TB2.ORDER_YEAR
    AND trn_po_prn_details.ITEM = TB2.ITEM
) AS PO_QTY,
(
    SELECT
        trn_po_prn_details.PRN_QTY
    FROM
        trn_po_prn_details
    WHERE
        trn_po_prn_details.ORDER_NO = TB2.ORDER_NO
    AND trn_po_prn_details.ORDER_YEAR = TB2.ORDER_YEAR
    AND trn_po_prn_details.ITEM = TB2.ITEM
) AS PRN_QTY,
SUM(

IF (
trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'MRN',
trn_po_prn_details_sales_order_stock_transactions.QTY,
0
)
) AS MRN_QTY,
SUM(

IF (
trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'MRN_EXTRA',
trn_po_prn_details_sales_order_stock_transactions.QTY,
0
)
) AS MRN_EXTRA_QTY,
SUM(

IF (
trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'ISSUE',
trn_po_prn_details_sales_order_stock_transactions.QTY,
0
)
) AS ISSUE_QTY,
SUM(

IF (
trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'GATE_PASS',
trn_po_prn_details_sales_order_stock_transactions.QTY,
0
)
) AS GP_QTY,
SUM(

IF (
trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'GATE_PASS_IN',
trn_po_prn_details_sales_order_stock_transactions.QTY,
0
)
) AS GP_IN_QTY,
SUM(

IF (
trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'RETURNED_TO_STORES',
trn_po_prn_details_sales_order_stock_transactions.QTY,
0
)
) AS RET_TO_STORE_QTY
FROM
(
SELECT
*
FROM
(
SELECT
trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER,
trn_po_prn_details_sales_order_stock_transactions.ITEM,
trn_po_prn_details_sales_order.REQUIRED,
mst_locations.intCompanyId AS ORDER_COMPANY,
mst_locations.strName AS location,
(
SELECT
GROUP_CONCAT(mst_locations.intCompanyId)
FROM
ware_fabricreceivedheader
INNER JOIN mst_locations ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId
WHERE
ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO
AND ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
) AS FABRIC_IN_COMPANIES,
1 AS type
FROM
trn_po_prn_details_sales_order_stock_transactions
INNER JOIN trn_po_prn_details_sales_order ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = trn_po_prn_details_sales_order.ORDER_NO
AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR
AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER
AND trn_po_prn_details_sales_order_stock_transactions.ITEM = trn_po_prn_details_sales_order.ITEM
INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO
AND trn_orderheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
INNER JOIN mst_locations ON mst_locations.intId = trn_orderheader.intLocationId
WHERE trn_orderheader.intOrderNo In ($orderNo)
AND trn_orderheader.intOrderYear = '$year'
GROUP BY
trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER,
trn_po_prn_details_sales_order_stock_transactions.ITEM
HAVING
FIND_IN_SET('$companyId', FABRIC_IN_COMPANIES)
OR ORDER_COMPANY = '$companyId'
UNION ALL
SELECT
trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER,
trn_po_prn_details_sales_order_stock_transactions.ITEM,
trn_po_prn_details_sales_order.REQUIRED,
mst_locations.intCompanyId AS ORDER_COMPANY,
mst_locations.strName AS location,

(
SELECT
	GROUP_CONCAT(
		DISTINCT mst_locations.intCompanyId
	)
FROM
	ware_fabricreceivedheader
INNER JOIN mst_locations ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId
WHERE
	ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO
AND ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
) AS FABRIC_IN_COMPANIES,
2 AS type
FROM
trn_po_prn_details_sales_order_stock_transactions
LEFT JOIN trn_po_prn_details_sales_order ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = trn_po_prn_details_sales_order.ORDER_NO
AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR
AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER
AND trn_po_prn_details_sales_order_stock_transactions.ITEM = trn_po_prn_details_sales_order.ITEM
INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO
AND trn_orderheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
INNER JOIN mst_locations ON mst_locations.intId = trn_orderheader.intLocationId
WHERE
trn_po_prn_details_sales_order.ORDER_NO IS NULL
AND trn_orderheader.intOrderNo In ($orderNo)
AND trn_orderheader.intOrderYear = '$year'
GROUP BY
trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER,
trn_po_prn_details_sales_order_stock_transactions.ITEM
HAVING
(
FIND_IN_SET('$companyId', FABRIC_IN_COMPANIES)
OR ORDER_COMPANY = '$companyId'
)
UNION ALL
SELECT
trn_po_prn_details_sales_order.ORDER_NO,
trn_po_prn_details_sales_order.ORDER_YEAR,
trn_po_prn_details_sales_order.SALES_ORDER,
trn_po_prn_details_sales_order.ITEM,
trn_po_prn_details_sales_order.REQUIRED,
mst_locations.intCompanyId AS ORDER_COMPANY,
mst_locations.strName AS location,
(
SELECT
GROUP_CONCAT(
DISTINCT mst_locations.intCompanyId
)
FROM
ware_fabricreceivedheader
INNER JOIN mst_locations ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId
WHERE
ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order.ORDER_NO
AND ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order.ORDER_YEAR
) AS FABRIC_IN_COMPANIES,
1 AS type
FROM
trn_po_prn_details_sales_order_stock_transactions

RIGHT JOIN 

trn_po_prn_details_sales_order ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = trn_po_prn_details_sales_order.ORDER_NO
AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR
AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER
AND trn_po_prn_details_sales_order_stock_transactions.ITEM = trn_po_prn_details_sales_order.ITEM
INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = trn_po_prn_details_sales_order.ORDER_NO
AND trn_orderheader.intOrderYear = trn_po_prn_details_sales_order.ORDER_YEAR
INNER JOIN mst_locations ON mst_locations.intId = trn_orderheader.intLocationId
WHERE
trn_po_prn_details_sales_order_stock_transactions.ORDER_NO IS NULL
AND trn_orderheader.intOrderNo In ($orderNo)
AND trn_orderheader.intOrderYear = '$year'
GROUP BY
trn_po_prn_details_sales_order.ORDER_NO,
trn_po_prn_details_sales_order.ORDER_YEAR,
trn_po_prn_details_sales_order.SALES_ORDER,
trn_po_prn_details_sales_order.ITEM
HAVING
(
FIND_IN_SET('$companyId', FABRIC_IN_COMPANIES)
OR ORDER_COMPANY = '$companyId'
)
) AS TB1
GROUP BY
TB1.ORDER_NO,
TB1.ORDER_YEAR,
TB1.SALES_ORDER,
TB1.ITEM
) AS TB2
LEFT JOIN trn_po_prn_details_sales_order_stock_transactions ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = TB2.ORDER_NO
AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = TB2.ORDER_YEAR
AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = TB2.SALES_ORDER
AND trn_po_prn_details_sales_order_stock_transactions.ITEM = TB2.ITEM
INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = TB2.ORDER_NO
AND trn_orderheader.intOrderYear = TB2.ORDER_YEAR
INNER JOIN trn_orderheader_approvedby ON trn_orderheader_approvedby.intOrderNo = TB2.ORDER_NO
AND trn_orderheader_approvedby.intYear = TB2.ORDER_YEAR 
AND trn_orderheader_approvedby.intStatus = 0
AND trn_orderheader_approvedby.intApproveLevelNo = 1
LEFT JOIN trn_po_prn_details_sales_order ON trn_po_prn_details_sales_order.ORDER_NO = TB2.ORDER_NO
AND trn_po_prn_details_sales_order.ORDER_YEAR = TB2.ORDER_YEAR
AND trn_po_prn_details_sales_order.SALES_ORDER = TB2.SALES_ORDER
AND trn_po_prn_details_sales_order.ITEM = TB2.ITEM
INNER JOIN trn_orderdetails ON TB2.ORDER_NO = trn_orderdetails.intOrderNo
AND TB2.ORDER_YEAR = trn_orderdetails.intOrderYear
AND TB2.SALES_ORDER = trn_orderdetails.intSalesOrderId
INNER JOIN trn_sampleinfomations ON trn_orderdetails.intSampleNo = trn_sampleinfomations.intSampleNo AND trn_orderdetails.intSampleYear = trn_sampleinfomations.intSampleYear AND trn_orderdetails.intRevisionNo = trn_sampleinfomations.intRevisionNo
INNER JOIN mst_item ON TB2.ITEM = mst_item.intId
INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId	
GROUP BY
TB2.ORDER_NO,
TB2.ORDER_YEAR,
TB2.SALES_ORDER,
TB2.ITEM,
trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID
) AS TB3
GROUP BY
TB3.ORDER_NO,
TB3.ORDER_YEAR,
TB3.SALES_ORDER,
TB3.ITEM 
ORDER BY 
TB3.ORDER_NO,
TB3.ORDER_YEAR,
TB3.ITEM ,
TB3.SALES_ORDER 
) as TB_ORDER_WISE
group by 
TB_ORDER_WISE.ITEM 
";


//echo $sql_summary;
$result_summary = $db->RunQuery($sql_summary);

$orderNo_temp 	='';
$orderYear_temp ='';
$item_temp 		='';
$rowspanPRn     = 0;
if($report_type ==1)
{
while ($row = mysqli_fetch_array($result_summary)) 
{
	

$so_count	  = get_number_of_sos($row['ORDER_NO'],$row['ORDER_YEAR'],$row['ITEM']);
$MRN_QTY      = $row['MRN_QTY'] + $row['MRN_QTY_OTHER_LOC']; //Total
if($row['type'] == '2')
 {
$bal_to_issue = round((($MRN_QTY) - ($row['ISSUE_QTY'] + MAX($row['ISSUE_QTY_OTHER_LOC'], ($row['GP_QTY_OTHER_LOC'])))), 4);
}
else
{
$bal_to_issue = round((($row['REQUIRED']) - ($row['ISSUE_QTY'] + MAX($row['ISSUE_QTY_OTHER_LOC'], ($row['GP_QTY_OTHER_LOC'])))), 4);
}

if ($bal_to_issue < 0)
$bal_to_issue = 0;
$ISSUE_QTY = $row['ISSUE_QTY'] + $row['ISSUE_QTY_OTHER_LOC']; // Total
//$returnToStores = $row['RET_TO_STORE_QTY']; //Get the return to head office qty not the all the return Qty

$returnToStores = $row['RET_TO_STORE_QTY'] +  $row['RET_TO_STORE_QTY_OTHER_LOC'];
?>
<tr style="background-color:<?php if($row['type'] == '2'){$color = '#FFE6E6'; echo $color; }else { $color = '#FFFFFF'; echo $color;} ?>">

<td title="Item" class="normalfnt" style="text-align: center">
&nbsp;<?php echo $row['itemName']; ?>&nbsp;</td>

<td title="UOM" class="normalfnt" style="text-align: center">
&nbsp;<?php echo $row['UOM']; ?>&nbsp;</td>
<td valign="top" title="PRI" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round($row['REQUIRED'], 4); ?>&nbsp;</td>
<?php if($orderNo_temp != $row['ORDER_NO'] || $orderYear_temp != $row['ORDER_YEAR'] || $item_temp != $row['ITEM']  ){  ?>
<td valign="top" title="PRN" class="normalfnt" <?php if($so_count > 0) { $rowspanPRn = $so_count; ?>     rowspan="<?php echo $so_count; ?>" <?php } ?> style="text-align: right; ">
&nbsp;<?php echo round($row['PRN_QTY'], 4); ?>&nbsp;</td>
<td valign="top" title="PURCHASED" class="normalfnt" <?php if($so_count > 0) { ?> rowspan="<?php echo $so_count; ?>"  <?php } ?> style="text-align: right; ">
&nbsp;<?php echo round($row['PO_QTY'], 4); ?>&nbsp;</td>

<?php } else {   ?>
<?php if($rowspanPRn < 0) { ?>
<!--<td title="PRN"></td>-->
<?php } if($rowspanPRn < 0 ) { ?>

<!--<td title="PURCHASED"></td>-->
<?php } ?>
<?php }  $rowspanPRn--; // substracting data from rowspanPrn  ?>
<td valign="top" title="MRN" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round($MRN_QTY, 4); ?>&nbsp;</td>
<td valign="top" title="ISSUED" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round($ISSUE_QTY, 4); ?>&nbsp;</td>
<td valign="top" title="RETURNED" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round($returnToStores, 4); ?>&nbsp;</td>
<td valign="top" title="Gp out (From HO)" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round($row['GP_QTY'], 4); ?>&nbsp;</td>
<td valign="top" title="Bal To Issue" class="normalfnt" style="text-align: right">
&nbsp;<?php echo $bal_to_issue ?>&nbsp;</td>
</tr>
<?php 
}
}
else if($report_type ==2)
{
while ($row = mysqli_fetch_array($result_summary))
{
$sql_currency = "SELECT 
mst_item.dblLastPrice, 
mst_item.intCurrency,
mst_financecurrency.strCode,
mst_financeexchangerate.dblBuying as rate,
(
		SELECT
			round(
				sum(
					ware_grnheader.dblExchangeRate * ware_stocktransactions_bulk.dblQty * ware_stocktransactions_bulk.dblGRNRate
				) / sum(
					ware_stocktransactions_bulk.dblQty
				),4
			)
		FROM
			ware_stocktransactions_bulk
		LEFT JOIN ware_grnheader ON ware_grnheader.intGrnNo = ware_stocktransactions_bulk.intGRNNo
		AND ware_grnheader.intGrnYear = ware_stocktransactions_bulk.intGRNYear
		WHERE
			ware_stocktransactions_bulk.intCompanyId = '$companyId'
		AND ware_stocktransactions_bulk.intItemId = '".$row['itemId']."'
	) AS price
FROM mst_item
INNER JOIN mst_financecurrency 
on mst_item.intCurrency = mst_financecurrency.intId
INNER JOIN mst_financeexchangerate on  mst_item.intCurrency = mst_financeexchangerate.intCurrencyId
WHERE 
mst_item.intId ='".$row['itemId']."' 
AND mst_financeexchangerate.intCompanyId = '$companyId' 
AND mst_financeexchangerate.dtmDate = (select MAX(mst_financeexchangerate.dtmDate) 
FROM mst_financeexchangerate 
WHERE mst_financeexchangerate.intCompanyId = '$companyId') ";


$result5 = $db->RunQuery($sql_currency);

while ($row_1 = mysqli_fetch_array($result5))
{
    if($row_1['price'] > 0){
        $unit_price = $row_1['price'];
    }
    else{
        $unit_price     = $row_1['dblLastPrice'];
    }

$currency_id    = $row_1['intCurrency'];                          
$exchange_rate  = $row_1['rate'];


$so_count	     = get_number_of_sos($row['ORDER_NO'],$row['ORDER_YEAR'],$row['ITEM']);
$MRN_QTY         = $row['MRN_QTY'] + $row['MRN_QTY_OTHER_LOC']; //Total

if($row['type'] == '2')
{
$bal_to_issue    = round((($MRN_QTY) - ($row['ISSUE_QTY'] + MAX($row['ISSUE_QTY_OTHER_LOC'], ($row['GP_QTY_OTHER_LOC'])))), 4);
 }
 else
 {
$bal_to_issue    = round((($row['REQUIRED']) - ($row['ISSUE_QTY'] + MAX($row['ISSUE_QTY_OTHER_LOC'], ($row['GP_QTY_OTHER_LOC'])))), 4);
 }

if ($bal_to_issue < 0)
$bal_to_issue = 0;
$ISSUE_QTY = $row['ISSUE_QTY'] + $row['ISSUE_QTY_OTHER_LOC']; // Total
//$returnToStores = $row['RET_TO_STORE_QTY']; //Get the return to head office qty not the all the return Qty
$returnToStores = $row['RET_TO_STORE_QTY'] +  $row['RET_TO_STORE_QTY_OTHER_LOC'];
   ?>
<tr style="background-color:<?php if($row['type'] == '2'){$color = '#FFE6E6'; echo $color; }else { $color = '#FFFFFF'; echo $color;} ?>">



<td title="Item" class="normalfnt" style="text-align: center">
&nbsp;<?php echo $row['itemName']; ?>&nbsp;</td>

<td title="UOM" class="normalfnt" style="text-align: center">
&nbsp;<?php echo $row['UOM']; ?>&nbsp;</td>

<?php
if($currency_id!=1)
{                                            
$sql_rate_usd = "SELECT 
mst_financeexchangerate.dblBuying
FROM
mst_financeexchangerate
WHERE 
mst_financeexchangerate.intCompanyId = '$companyId' 
AND mst_financeexchangerate.intCurrencyId = '1'
AND
mst_financeexchangerate.dtmDate = (select MAX(mst_financeexchangerate.dtmDate)
FROM
mst_financeexchangerate
WHERE
mst_financeexchangerate.intCompanyId = '$companyId') ";

$result_rateUSD = $db->RunQuery($sql_rate_usd);
while($row_rateUSD=mysqli_fetch_array($result_rateUSD))
{
$rate_usd = $row_rateUSD['dblBuying'];

?>
<td valign="top" title="PRI" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round( (round($row['REQUIRED'],4)*(($unit_price)* $exchange_rate /$rate_usd)), 4); ?>&nbsp;</td>

<?php 
if($orderNo_temp != $row['ORDER_NO'] || $orderYear_temp != $row['ORDER_YEAR'] || $item_temp != $row['ITEM']  )
{  ?>
<td valign="top" title="PRN" class="normalfnt" <?php if($so_count > 0) { $rowspanPRn = $so_count; ?>     rowspan="<?php echo $so_count; ?>" <?php } ?> style="text-align: right; ">
&nbsp;<?php echo round( (round( $row['PRN_QTY'],4)*(($unit_price)* $exchange_rate /$rate_usd)), 4); ?>&nbsp;</td>

<td valign="top" title="PURCHASED" class="normalfnt" <?php if($so_count > 0) { ?> rowspan="<?php echo $so_count; ?>"  <?php } ?> style="text-align: right; ">
&nbsp;<?php echo round( (round( $row['PO_QTY'],4) *(($unit_price)* $exchange_rate /$rate_usd)), 4); ?>&nbsp;</td>

<?php } 
else {   ?>
<?php 
if($rowspanPRn < 0) { ?>
<!--<td title="PRN"></td>-->
<?php } 
if($rowspanPRn < 0 ) { ?>
<!--<td title="PURCHASED"></td>-->
<?php } ?>
<?php }  $rowspanPRn--; // substracting data from rowspanPrn  ?>

  <td valign="top" title="MRN" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round( ( $MRN_QTY*(($unit_price)* $exchange_rate/$rate_usd)), 4); ?>&nbsp;</td>
    
<td valign="top" title="ISSUED" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round(( $ISSUE_QTY*(($unit_price)* $exchange_rate/$rate_usd)), 4); ?>&nbsp;</td>
    
 
<td valign="top" title="RETURNED" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round(($returnToStores*(($unit_price)* $exchange_rate/$rate_usd)), 4); ?>&nbsp;</td> 
    
   
<td valign="top" title="Gp out (From HO)" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round(($row['GP_QTY']*(($unit_price)* $exchange_rate/$rate_usd)), 4); ?>&nbsp;</td>
  
<td valign="top" title="Bal To Issue" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round(($bal_to_issue*(($unit_price)* $exchange_rate/$rate_usd)), 4); ?>&nbsp;</td>

   
<?php
$orderNo_temp 	=$row['ORDER_NO'];
$orderYear_temp =$row['ORDER_YEAR'];
$item_temp 		=$row['ITEM'];
?>
<?php 

}
}//currencu id = another

elseif($currency_id==1)
{
										 
?>			
  
<td valign="top" title="PRI" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round(round($row['REQUIRED'],4)*$unit_price, 4); ?>&nbsp;</td>

<?php 
if($orderNo_temp != $row['ORDER_NO'] || $orderYear_temp != $row['ORDER_YEAR'] || $item_temp != $row['ITEM']  )
{  ?>
<td valign="top" title="PRN" class="normalfnt" <?php if($so_count > 0) { $rowspanPRn = $so_count; ?> rowspan="<?php echo $so_count; ?>" 
<?php 
} 
?> style="text-align: right; ">
&nbsp;<?php echo round(round($row['PRN_QTY'],4)*$unit_price, 4); ?>&nbsp;</td>

<td valign="top" title="PURCHASED" class="normalfnt" <?php if($so_count > 0) { ?> rowspan="<?php echo $so_count; ?>"  <?php } ?> style="text-align: right; ">
&nbsp;<?php echo round((round($row['PO_QTY'],4)*$unit_price), 4); ?>&nbsp;</td>

<?php } 
else {   ?>
<?php 
if($rowspanPRn < 0)
 { ?>
<td title="PRN"></td>
<?php 
} 
if($rowspanPRn < 0 )
 { ?>
<td title="PURCHASED"></td>
<?php } ?>
<?php }  $rowspanPRn--; // substracting data from rowspanPrn  ?>

<td valign="top" title="MRN" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round(($MRN_QTY*$unit_price),4); ?>&nbsp;</td>

<td valign="top" title="ISSUED" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round(($ISSUE_QTY*$unit_price),4); ?>&nbsp;</td>

<td valign="top" title="RETURNED" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round(($returnToStores*$unit_price),4); ?>&nbsp;</td>

<td valign="top" title="Gp out (From HO)" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round(($row['GP_QTY']*$unit_price),4); ?>&nbsp;</td>

<td valign="top" title="Bal To Issue" class="normalfnt" style="text-align: right">
&nbsp;<?php echo round(($bal_to_issue *$unit_price),4); ?>&nbsp;</td>

<?php


} //currency usd
}

?>

                                    				 
<?php 
// }//usd_currency
}//dblprice	
}

?>
	
	
	


</table>



</tr></table>		




		
</td>
</tr>

</table>

</div>
</form>
</body>
</html>
<?php

function getHOStock($item)
{

    global $db;

    $sql = "SELECT
			Sum(ware_stocktransactions_bulk.dblQty) as qty 
			FROM `ware_stocktransactions_bulk`
			WHERE
			ware_stocktransactions_bulk.intItemId = '$item' AND
			ware_stocktransactions_bulk.intLocationId = 2 ";
          
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    return $row['qty'];

}

function get_approved_non_grn_pos($item, $location)
{

    global $db;

    $sql = "SELECT
			sum(trn_podetails.dblQty-trn_podetails.dblGRNQty) as qty
			FROM
			trn_podetails
			INNER JOIN trn_poheader ON trn_podetails.intPONo = trn_poheader.intPONo AND trn_podetails.intPOYear = trn_poheader.intPOYear
			WHERE
			trn_poheader.intStatus = 1 AND
			trn_poheader.intDeliveryTo = '$location' AND
			trn_podetails.intItem = '$item'
			";

    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    return $row['qty'];


}

function get_ho_loc($companyId){
    global $db;
    $sql="SELECT
			mst_locations.intId
			FROM `mst_locations`
			WHERE
			mst_locations.COMPANY_MAIN_STORES_LOCATION = 1 AND
			mst_locations.intCompanyId = '$companyId'
			";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    return $row['intId'];

}

function get_number_of_sos($no,$year,$item){
    global $db;
	global $userId;
    $sql="
		SELECT COUNT(distinct TB.SALES_ORDER) AS so_count  FROM(
		SELECT
		trn_po_prn_details_sales_order.SALES_ORDER
			FROM `trn_po_prn_details_sales_order`
			INNER JOIN trn_po_prn_details_sales_order_stock_transactions AS ST 
			ON ST.ORDER_NO = trn_po_prn_details_sales_order.ORDER_NO
			AND ST.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR
			AND ST.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER
			AND ST.ITEM = trn_po_prn_details_sales_order.ITEM
			 
INNER JOIN trn_orderdetails ON trn_po_prn_details_sales_order.ORDER_NO = trn_orderdetails.intOrderNo
AND trn_po_prn_details_sales_order.ORDER_YEAR = trn_orderdetails.intOrderYear
AND trn_po_prn_details_sales_order.SALES_ORDER = trn_orderdetails.intSalesOrderId
			WHERE
			trn_po_prn_details_sales_order.ORDER_NO = '$no' AND
			trn_po_prn_details_sales_order.ORDER_YEAR = '$year' AND
			trn_po_prn_details_sales_order.ITEM = '$item'
			GROUP BY 
			trn_po_prn_details_sales_order.ORDER_NO,
			trn_po_prn_details_sales_order.ORDER_YEAR,
			trn_po_prn_details_sales_order.ITEM,
			trn_po_prn_details_sales_order.SALES_ORDER
			
			union 

		SELECT
		trn_po_prn_details_sales_order.SALES_ORDER
			FROM `trn_po_prn_details_sales_order`
			left JOIN trn_po_prn_details_sales_order_stock_transactions AS ST 
			ON ST.ORDER_NO = trn_po_prn_details_sales_order.ORDER_NO
			AND ST.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR
			AND ST.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER
			AND ST.ITEM = trn_po_prn_details_sales_order.ITEM
INNER JOIN trn_orderdetails ON trn_po_prn_details_sales_order.ORDER_NO = trn_orderdetails.intOrderNo
AND trn_po_prn_details_sales_order.ORDER_YEAR = trn_orderdetails.intOrderYear
AND trn_po_prn_details_sales_order.SALES_ORDER = trn_orderdetails.intSalesOrderId
			WHERE
			trn_po_prn_details_sales_order.ORDER_NO = '$no' AND
			trn_po_prn_details_sales_order.ORDER_YEAR = '$year' AND
			trn_po_prn_details_sales_order.ITEM = '$item'
			GROUP BY 
			trn_po_prn_details_sales_order.ORDER_NO,
			trn_po_prn_details_sales_order.ORDER_YEAR,
			trn_po_prn_details_sales_order.ITEM	,
			trn_po_prn_details_sales_order.SALES_ORDER
			union
			
 
		SELECT
		ST.SALES_ORDER
			FROM `trn_po_prn_details_sales_order_stock_transactions`  AS ST 
			left JOIN trn_po_prn_details_sales_order
			ON ST.ORDER_NO = trn_po_prn_details_sales_order.ORDER_NO
			AND ST.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR
			AND ST.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER
			AND ST.ITEM = trn_po_prn_details_sales_order.ITEM
			 
INNER JOIN trn_orderdetails ON ST.ORDER_NO = trn_orderdetails.intOrderNo
AND ST.ORDER_YEAR = trn_orderdetails.intOrderYear
AND ST.SALES_ORDER = trn_orderdetails.intSalesOrderId
			WHERE
			ST.ORDER_NO = '$no' AND
			ST.ORDER_YEAR = '$year' AND
			ST.ITEM = '$item'
			GROUP BY 
			ST.ORDER_NO,
			ST.ORDER_YEAR,
			ST.SALES_ORDER,
			ST.ITEM			
			) AS TB
			";
	
	if($userId==2){
		//echo $sql;
	}
	
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    return $row['so_count'];
	
}


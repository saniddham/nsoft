<?php (define('UNLOCKPAGE', true) ? die('<<< Access denied >>>') : ''); ?>
<?php
$mainPath = $_SESSION['mainPath'];

$thisFilePath = $_SERVER['PHP_SELF'];
//include  "{$backwardseperator}dataAccess/permisionCheck.inc";
//include 	"reports.js";

//BEGIN - CLASS OBJECTS	{
//$obj_getData	= new clsGetData;
//END - CLASS OBJECTS	}
$userId = $_SESSION['userId'];
$year = $_SESSION["Year"];
$month = $_SESSION["Month"];
?>
<title>nsoft | Procument - Reports</title>
<script type="text/javascript" src="presentation/procurement/reports/reports.js"></script>
<!--<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/3.4.3/select2.js"></script>-->
<!--<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/3.4.3/select2.css" rel="stylesheet" />-->
<script src="libraries/select2/select2.min.js"></script>
<link href="libraries/select2/select2.min.css" rel="stylesheet" />
<div align="center">
    <div class="trans_layoutL" style="width:600px">
        <div class="trans_text">Procument Reports</div>
        <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            <!--BEGIN - PO Summery-->
            <tr>
                <td>
                    <table align="left" width="600" class="main_header_table" id="tblPOSummeryHead" cellpadding="2"
                           cellspacing="0">
                        <tr>
                            <td width="544"><img src="images/report_go.png" class="mouseover"/>&nbsp;<u>Order Wise PRN
                                    Report</u></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <form name="frmPOSummery" id="frmPOSummery" autocomplete="off">
                        <table width="600" align="left" border="0" id="tblPOSummeryBody" class="main_table"
                               cellspacing="0" style="display:none;">
                            <thead>
                            <tr>
                                <td colspan="4" style="text-align:left">Order wise PRN Report<img
                                        src="images/close_small.png" align="right" class="mouseover"/></td>
                            </tr>
                            </thead>
                            <tbody align="center">
                            <tr>
                                <td width="200" align="left"><span class="normalfnt">Order No</span></td>
                                <td width="443" align="left"><input type="text" id="orderNo"></td>
                            </tr>
                            <tr>
                                <td width="200" align="left"><span class="normalfnt">Order Year</span></td>
                                <td width="443" align="left"><input type="text" id="orderYear"></td>
                            </tr>
                            <tr>
                                <td class="normalfnt">&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="4" align="center" class="normalfntMid">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                                        <tr>
                                            <td width="100%" align="center" bgcolor=""><img style="display:inline"
                                                                                            border="0"
                                                                                            src="images/Tnew.jpg"
                                                                                            alt="New" name="butNew"
                                                                                            width="92" height="24"
                                                                                            class="mouseover"
                                                                                            id="butNew"
                                                                                            tabindex="28"/><img
                                                    style="display:inline" border="0" src="images/Treport.jpg"
                                                    alt="Save" name="butReports" class="mouseover" id="butReports"
                                                    tabindex="24" onClick="ViewReport('POSummery','frmPOSummery')"/><a
                                                    href="main.php"><img src="images/Tclose.jpg" alt="Close"
                                                                         name="butClose" width="92" height="24"
                                                                         border="0" class="mouseover" id="butClose"
                                                                         tabindex="27"/></a></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </form>
                </td>
            </tr>

            <tr>
                <td>
                    <table align="left" width="600" class="main_header_table" id="tblStockPredictionHead"
                           cellpadding="2" cellspacing="0">
                        <tr>
                            <td width="544"><img src="images/report_go.png" class="mouseover"/>&nbsp;<u>Order Wise Stock
                                    Order Forecast Report</u></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <form name="frmStockPrediction" id="frmStockPrediction" method="post" action="?q=1216" autocomplete="off" onsubmit="return ViewStockOrderForecastReport('StockPrediction','frmStockPrediction')" target="_blank">
                        <table width="600" align="left" border="0" id="tblStockPredictionBody" class="main_table"
                               cellspacing="0" style="display:none;">
                            <thead>
                            <tr>
                                <td colspan="4" style="text-align:left">Order Wise Stock Order Forecast Report<img
                                        src="images/close_small.png" align="right" class="mouseover"/></td>
                            </tr>
                            </thead>
                            <tbody align="center">
                            <tr>

                                <td width="15%" height="27" class="normalfnt">Company</td>
                                <td width="30%"><select name="cboCompany_r"  id="cboCompany_r" style="width:250px"  class="txtText" >
                                        <option value=""></option>
                                        <?php
                                        $sql = "SELECT
							mst_companies.intId,
							mst_companies.strName
							FROM mst_companies
							WHERE
							mst_companies.intStatus =  '1'";
                                        $result = $db->RunQuery($sql);
                                        while($row=mysqli_fetch_array($result))
                                        {
                                            echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
                                        }
                                        ?>
                                    </select></td>
                                <td width="14%"><span id="validate_company" name="validate_company"></span></td>
                                <td width="24%">&nbsp;</td>
                            </tr>
                            <tr>

                                <td width="15%" height="27" class="normalfnt">Order No</td>
                                <td width="30%"><select multiple name="order_no[]" id="order_no" style="width:250px"  class="txtText" >
                                        <option value=""></option>
                                        <?php
                                       /* $sql = "SELECT
                                          trn_orderheader.intOrderNo As OrderNo
                                          FROM
                                          trn_orderheader
                                          ORDER BY trn_orderheader.intOrderNo ASC";
                                        $result = $db->RunQuery($sql);
                                        while($row=mysqli_fetch_array($result))
                                        {

                                            echo "<option value=\"".$row['OrderNo']."\">".$row['OrderNo']."</option>";
                                        }*/
                                        ?>
                                    </select></td>
                                <td width="14%">&nbsp;</td>
                                <td width="24%">&nbsp;</td>
                            </tr>
                            <tr>

                                <td width="15%" height="27" class="normalfnt">Main Category</td>
                                <td width="30%"><select multiple name="cboMainCategory[]" id="cboMainCategory" style="width:250px"  class="txtText ms_select1" >
                                        <?php
                                        //$maincat	= '1,2,3,4,5,7'; //All
                                        //                                        $maincat	= '1,3';
                                        //                                        $sql = "SELECT
                                        //							mst_maincategory.intId,
                                        //							mst_maincategory.strName
                                        //							FROM mst_maincategory
                                        //							WHERE FIND_IN_SET(mst_maincategory.intId,'$maincat')";
                                        $sql ='select mst_maincategory.intId,
						                              mst_maincategory.strName
						                              FROM
						                              mst_maincategory';
                                        $result = $db->RunQuery($sql);
                                        $k=0;
                                        while($row=mysqli_fetch_array($result))
                                        {
                                            if($k=0)
                                                $main=$row['intId'];
                                            echo "<option value=\"" . $row['intId'] . "\">" . $row['strName'] . "</option>";
                                            $k++;
                                        }
                                        ?>
                                    </select></td>
<!--                                <td width="14%"><input type="checkbox" id="checkbox_maincat" >&nbsp;Select All</td>-->
                                <td width="24%">&nbsp;<span id="validate_maincategory" class="mouseover"></span></td>
                            </tr>
                            <tr>

                                <td width="15%" height="27" class="normalfnt">Sub Category</td>
                                <td width="30%"><select multiple name="cboSubCategory[]" id="cboSubCategory" style="width:250px"  class="txtText ms_select2" >
                                        <?php
//                                        $sql = "SELECT
//							mst_subcategory.intId,
//							mst_subcategory.strCode,
//							mst_subcategory.strName
//							FROM mst_subcategory
//							";
//                                        $result = $db->RunQuery($sql);
//                                        while($row=mysqli_fetch_array($result))
//                                        {
//                                            echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
//                                        }
                                        ?>
                                    </select></td>
<!--                                <td width="14%"><input type="checkbox" id="checkbox_subncat" >&nbsp;Select All</td>-->
                                <td width="24%">&nbsp;<span id="validate_subcategory" class="mouseover"></span></td>
                            </tr>
                            <tr>

                                <td width="15%" height="27" class="normalfnt">Item</td>
                                <td width="30%"><select multiple name="cboItem[]" id="cboItem" style="width:250px"  class="txtText" >
                                        <option value=""></option>
                                        <?php
//                                        $sql = "SELECT
//                                                mst_item.intMainCategory,
//                                                mst_item.intSubCategory,
//                                                mst_item.strCode,
//                                                mst_item.strName,
//                                                mst_item.intId
//                                                FROM
//                                                mst_item";
//                                        $result = $db->RunQuery($sql);
//                                        while($row=mysqli_fetch_array($result))
//                                        {
//                                            echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
//                                        }
                                        ?>
                                    </select></td>
                                <td width="14%">&nbsp;</td>
                                <td width="24%">&nbsp;</td>
                            </tr>
                            <tr>

                                <td width="22.5%" height="27" class="normalfnt"><input type="checkbox" name="balance_mrn" id="balance_mrn" value="1"> Balance To MRN</td>
                                <td width="22.5%">
                                    <input type="checkbox" name="balance_issues" id="balance_issues" value="1"> Balance To Issues (Based On Pending and Approved)
                                </td>
                                <td width="14%">&nbsp;</td>
                                <td width="24%">&nbsp;</td>
                            </tr>
                            <tr>

                                <td width="15%" height="27" class="normalfnt">Sales Order wise</td>
                                <td width="30%"> <input type="checkbox" name="report_so_wise" id="report_so_wise" value="1" ></td>
                                <td width="14%">&nbsp;</td>
                                <td width="24%">&nbsp;</td>
                            </tr>
                            <tr>
                                <td width="15%" height="27" class="normalfnt">Include Pending MRN</td>
                                <td width="30%"> <input type="checkbox" name="pending_mrn" id="pending_mrn" value="1" ></td>
                                <td width="14%">&nbsp;</td>
                                <td width="24%">&nbsp;</td>
                            </tr>

                            <tr style="display: none">
                                <td width="200" align="left"><span class="normalfnt">From</span></td>
                                <td width="500" align="left"><input name="txtFromDate" type="text"
                                                                    class="#validate[required] txtbox"
                                                                    id="prnwiseDateFrom" style="width:98px;"
                                                                    onmousedown="DisableRightClickEvent();"
                                                                    onmouseout="EnableRightClickEvent();"
                                                                    onkeypress="return ControlableKeyAccess(event);"
                                                                    onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input
                                        type="reset" value="" class="txtbox" style="visibility:hidden;width:2px;"
                                        onclick="return showCalendar(this.id, '%Y-%m-%');"/><span id="validate_from" name="validate_from"></span></td>
                            </tr>
                            <tr style="display: none">
                                <td width="200" align="left"><span class="normalfnt">To</span></td>
                                <td width="500" align="left"><input name="txtToDate" type="text"
                                                                    class="#validate[required] txtbox" id="prnwiseDateTo"
                                                                    style="width:98px;"
                                                                    onmousedown="DisableRightClickEvent();"
                                                                    onmouseout="EnableRightClickEvent();"
                                                                    onkeypress="return ControlableKeyAccess(event);"
                                                                    onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input
                                        type="reset" value="" class="txtbox" style="visibility:hidden;width:2px;"
                                        onclick="return showCalendar(this.id, '%Y-%m-%');"/><span id="validate_to" name="validate_to"></span></td>
                            </tr>

                            <tr>
                                <td class="normalfnt">&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="4" align="center" class="normalfntMid">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                                        <tr>
                                            <td width="100%" align="center" bgcolor="">
                                                <img style="display:inline"
                                                     border="0"
                                                     src="images/Tnew.jpg"
                                                     alt="New" name="butNew"
                                                     width="92" height="24"
                                                     class="mouseover"
                                                     id="butNew"
                                                     tabindex="28"/>
                                                <img  style="display:inline" border="0" src="images/Treport.jpg" alt="Save" name="butReports" class="mouseover"
                                                      id="butReports" tabindex="24" href="javascript:{}" onclick="submit_Report();"/>
<!--                                                <img  style="display:inline" border="0" src="images/Treport_exel.png" alt="excel" class="mouseover"-->
<!--                                                      id="butReports" tabindex="24" href="javascript:{}" onclick="exportRepot();" value="excel" name="Report" />-->
                                                <a href="main.php">
                                                    <img src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0" class="mouseover" id="butClose"
                                                         tabindex="27"/></a></td>

                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </form>
                </td>
            </tr>

            <tr>
                <td>
                    <table align="left" width="600" class="main_header_table" id="tblItemStockPredictionHead"
                           cellpadding="2" cellspacing="0">
                        <tr>
                            <td width="544"><img src="images/report_go.png" class="mouseover"/>&nbsp;<u>Item Wise Stock
                                    Order Forecast Report</u></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <form name="frmItemStockPrediction" id="frmItemStockPrediction" autocomplete="off">
                        <table width="600" align="left" border="0" id="tblItemStockPredictionBody" class="main_table"
                               cellspacing="0" style="display:none;">
                            <thead>
                            <tr>
                                <td colspan="4" style="text-align:left">Item Wise Stock Order Forecast Report<img
                                        src="images/close_small.png" align="right" class="mouseover"/></td>
                            </tr>
                            </thead>
                            <tbody align="center">
                       
                            <tr>
                            <td class="normalfnt">Company</td>
                            <td width="443"  align="left">

                                <select  class="chosen-select clscompany"  data-placeholder="Select Your Option"
                                        style="width:400px; height:50px" tabindex="4" id="cbocompany" >

                                    <?php
                                    $html   		= "<option value=\"\"></option>";
                                    $result_company = "select mst_companies.strName,
                                                        mst_companies.intId
                                                        FROM
                                                        mst_companies
                                                        where
                                                        mst_companies.intStatus ='1'
                                                        ";
                                    $result = $db->RunQuery($result_company);

                                    while($row=mysqli_fetch_array($result))
                                    {
                                        $html	.=  "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
                                    }
                                    echo $html;
                                    ?>
                                </select>
                                </td>
                            <td width="443"  align="left"><span id="validate_company1" name="validate_company1"></span></td>
                            
                            </tr>
                            

                            <tr>
                                <td class="normalfnt">Main Category</td>
                                <td  width="443" >

                                    <select class="chosen-select clscategory" multiple="multiple" data-placeholder="Select Your Option"
                                            style="width:400px; height:50px" tabindex="4" id="cbomaincategory">

                                        <?php
                                        $html   		        = "<option value=\"\"></option>";
                                        $result_maincategory    = "select mst_maincategory.intId,
						mst_maincategory.strName
						FROM
						mst_maincategory
				";
                                        $query = $db->RunQuery($result_maincategory);
                                        while($row=mysqli_fetch_array($query))
                                        {
                                            $html	.=  "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
                                        }
                                        echo $html;
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="normalfnt">Sub Category</td>
                                <td  width="443" >

                                    <select class="chosen-select clssubcategory" multiple="multiple" data-placeholder="Select Your Option"
                                            style="width:400px; height:50px" tabindex="4" id="cbosubcategory">
                                        <?php
                                        $html   = "<option value=\"\"></option>";
                                        echo $html;
                                        ?>

                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td class="normalfnt">Item</td>
                                <td  width="443" >

                                    <select class="chosen-select clsitem" multiple="multiple" data-placeholder="Select Your Option"
                                            style="width:400px; height:50px" tabindex="4" id="cboitem">
                                        <?php
                                        $html   = "<option value=\"\"></option>";
                                        echo $html;
                                        ?>

                                    </select>
                                </td>
                            </tr>
                            <tr><td  width="443" align="left"> Balance to purchase  </td>
                                <td><input type="checkbox" name="balance" id="balance" value="1" checked></td></tr>
                            <tr><td  width="443" align="left"> Include Pending MRN to this report  </td>
                                <td><input type="checkbox" name="mrn" id="mrn" value="1" checked></td></tr>


                            <tr>
                                <td class="normalfnt">&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="4" align="center" class="normalfntMid">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                                        <tr>
                                            <td width="100%" align="center" bgcolor=""><img style="display:inline"
                                                                                            border="0"
                                                                                            src="images/Tnew.jpg"
                                                                                            alt="New" name="butNew"
                                                                                            width="92" height="24"
                                                                                            class="mouseover"
                                                                                            id="butNew"
                                                                                            tabindex="28"/><img
                                                    style="display:inline" border="0" src="images/Treport.jpg"
                                                    alt="Save" name="butReports" class="mouseover" id="butReports"
                                                    tabindex="24"
                                                    onClick="ViewIetmWiseStockOrderForecastReport('ItemStockPrediction','frmItemStockPrediction')"/><a
                                                    href="main.php">
                                                    <img  style="display:inline" border="0" src="images/Treport_exel.png" alt="Save" name="butReports" class="mouseover"
                                                          id="butReports" tabindex="24" onClick="ViewReport_excel_new2('itemForcast','frmItemStockPrediction','excel')"/>
                                                    <a href="main.php">
                                                        <img src="images/Tclose.jpg" alt="Close" name="butClose" width="92" height="24" border="0" class="mouseover" id="butClose"
                                                             tabindex="27"/></a></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </form>
                </td>
            </tr>

            <tr>
                <td>
                    <table align="left" width="600" class="main_header_table" id="tblItemReturnableHead"
                           cellpadding="2" cellspacing="0">
                        <tr>
                            <td width="544"><img src="images/report_go.png" class="mouseover"/>&nbsp;<u>Returnable Items Report</u></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <form name="frmItemReturnable" id="frmItemReturnable" autocomplete="off">
                        <table width="600" align="left" border="0" id="tblItemReturnableBody" class="main_table"
                               cellspacing="0" style="display:none;">
                            <thead>
                            <tr>
                                <td colspan="4" style="text-align:left">Returnable Items Report<img
                                        src="images/close_small.png" align="right" class="mouseover"/></td>
                            </tr>
                            </thead>
                            <tbody align="center">
                            <tr>
                                <td width="200" align="left"><span class="normalfnt">From</span></td>
                                <td width="443" align="left"><input name="txtItemFromDate" type="text"
                                                                    class="validate[required] txtbox"
                                                                    id="returnableItemDateFrom" style="width:98px;"
                                                                    onmousedown="DisableRightClickEvent();"
                                                                    onmouseout="EnableRightClickEvent();"
                                                                    onkeypress="return ControlableKeyAccess(event);"
                                                                    onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input
                                        type="reset" value="" class="txtbox" style="visibility:hidden;width:2px;"
                                        onclick="return showCalendar(this.id, '%Y-%m-%');"/></td>
                            </tr>
                            <tr>
                                <td width="200" align="left"><span class="normalfnt">To</span></td>
                                <td width="443" align="left"><input name="txtItemToDate" type="text"
                                                                    class="validate[required] txtbox"
                                                                    id="returnableItemDateTo" style="width:98px;"
                                                                    onmousedown="DisableRightClickEvent();"
                                                                    onmouseout="EnableRightClickEvent();"
                                                                    onkeypress="return ControlableKeyAccess(event);"
                                                                    onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input
                                        type="reset" value="" class="txtbox" style="visibility:hidden;width:2px;"
                                        onclick="return showCalendar(this.id, '%Y-%m-%');"/></td>
                            </tr>
                            <tr>
                                <td class="normalfnt">&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="4" align="center" class="normalfntMid">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                                        <tr>
                                            <td width="100%" align="center" bgcolor=""><img style="display:inline"
                                                                                            border="0"
                                                                                            src="images/Tnew.jpg"
                                                                                            alt="New" name="butNew"
                                                                                            width="92" height="24"
                                                                                            class="mouseover"
                                                                                            id="butNew"
                                                                                            tabindex="28"/><img
                                                    style="display:inline" border="0" src="images/Treport.jpg"
                                                    alt="Save" name="butReports" class="mouseover" id="butReports"
                                                    tabindex="24"
                                                    onClick="ViewReturnableIetmReport('returnItems','frmItemReturnable')"/><a
                                                    href="main.php"><img src="images/Tclose.jpg" alt="Close"
                                                                         name="butClose" width="92" height="24"
                                                                         border="0" class="mouseover" id="butClose"
                                                                         tabindex="27"/></a></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </form>
                </td>
            </tr>

            <tr>
                <td>
                    <table align="left" width="600" class="main_header_table" id="tblOrderwiseStockMovementHead"
                           cellpadding="2" cellspacing="0">
                        <tr>
                            <td width="544"><img src="images/report_go.png" class="mouseover"/>&nbsp;<u>Orderwise Stock Movement Report</u></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <form name="frmOrderwiseStockMovement" id="frmOrderwiseStockMovement" autocomplete="off">
                        <table width="600" align="left" border="0" id="tblOrderwiseStockMovementBody" class="main_table"
                               cellspacing="0" style="display:none;">
                            <thead>
                            <tr>
                                <td colspan="4" style="text-align:left">Orderwise Stock Movement Report<img
                                        src="images/close_small.png" align="right" class="mouseover"/></td>
                            </tr>
                            </thead>
                            <tbody align="center">


                            <tr>
                                <td width="443"  align="left">Year</td>
                                <td width="443"  align="left">
                                    <select name="Year" id="cboYear" style="width:70px">
                                        <option value=\"\"></option>
                                        <?php
                                        $d = date('Y');
                                        for($d;$d>=2012;$d--)
                                        {
                                            echo "<option id=\"$d\" >$d</option>";
                                        }
                                        ?>
                                    </select></td>
                            </tr>

                            <tr><td width="443" align ="left">Order No</td> <td style="display:none;"> <input id ="company" value = "<?php echo $companyId;?>"</input></td>
                                <td width="443"  align="left">
                                    <select class="chosen-select clsorder" multiple="multiple" data-placeholder="Select Your Option"  style="width:400px; height:50px"tabindex="4" id="cboorder">

                                        <?php
                                        $db->connect();
                                        $db->begin();
                                        $html     = "<option value=\"\"></option>";
                                        echo $html;
                                        ?>
                                    </select>

                                </td>
                            </tr>
                            <tr>
                                <td align="left"><input align="left" type="radio" name="radio"  id="r1" value="1"> quantity Report</td>
                                <td align="left"><input align="left" type="radio" name="radio"  id="r2" value="2"> Value Report</td>
                            </tr>
                            <tr>
                                <td class="normalfnt">&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="4" align="center" class="normalfntMid">
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
                                        <tr>
                                            <td width="100%" align="center" bgcolor=""><img style="display:inline"
                                                                                            border="0"
                                                                                            src="images/Tnew.jpg"
                                                                                            alt="New" name="butNew"
                                                                                            width="92" height="24"
                                                                                            class="mouseover"
                                                                                            id="butNew"
                                                                                            tabindex="28"/>

                                                <img
                                                    style="display:inline" border="0" src="images/Treport.jpg"
                                                    alt="Save" name="butReports" class="mouseover" id="butReports"
                                                    tabindex="24"
                                                    onClick="ViewOrderwiseStockMovementReport('returnItems','frmOrderwiseStockMovement')"/><a
                                                    href="main.php">

                                                    <img  style="display:inline" border="0" src="images/Treport_exel.png" alt="Save" name="butReports" class="mouseover" id="butReports" tabindex="24" onClick="ViewReport_excel_new1('returnItems','frmOrderwiseStockMovement','excel')"/><a href="main.php">

                                                        <img src="images/Tclose.jpg" alt="Close"
                                                             name="butClose" width="92" height="24"
                                                             border="0" class="mouseover" id="butClose"
                                                             tabindex="27"/></a></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </form>
                </td>
            </tr>
        </table>
    </div>
</div>
<!-- using below style and javascript for order wise stock order forcaast report -->
<!-- Author : Hasitha Charka -->
<style>

    .select2-drop{
        top:350px !important;
    }

    .button2 {
        background-color: white;
        color: black;
        border: 2px solid #008CBA;
    }

    .button2:hover {
        background-color: #008CBA;
        color: white;
    }
</style>
<script type="text/javascript">


    $('#frmStockPrediction #cboCompany_r').change(function() {

        $('#validate_company').html('<span id="error" style=\"display: none\">* This Field is Required</span>');
    });

    $("#cboCompany_r").select2({
        //placeholder: "Search for a repository",

    });

    $("#order_no").select2({
        //placeholder: "Search for a repository",
        minimumInputLength: 3
    });

    $("#cboMainCategory").select2({});
//    $("#checkbox_maincat").click(function(){
//        if($("#checkbox_maincat").is(':checked') ){
//            $("#cboMainCategory > option").prop("selected","selected");
//            $("#cboMainCategory").trigger("change");
//        }else{
////            $('#validate_maincategory').html('<span id="error" style=\"color:red\">* Please Load SubCategory</span>');
//           $("#cboMainCategory > option").removeAttr("selected");
//            $("#cboMainCategory").trigger("change");
//
//        }
//    });
//
//
//
//    $('#cboMainCategory').on("removed", function(e) {
//        $('#checkbox_maincat').attr('checked', false); // Unchecks it
//        if (e.val != ''){
//            $('#validate_maincategory').html('<span id="error" style=\"color:red\">* Please Load SubCategory</span>');
//
//        }
//    });
//
//
    $("#cboSubCategory").select2({

    });
//    $("#checkbox_subncat").click(function(){
//        if($("#checkbox_subncat").is(':checked') ){
//            $("#cboSubCategory > option").prop("selected","selected");
//            $("#cboSubCategory").trigger("change");
//        }else{
//            $('#validate_subcategory').html('<span id="error" style=\"color:red\">* Please Load Item</span>');
//            $("#cboSubCategory > option").removeAttr("selected");
//            $("#cboSubCategory").trigger("change");
//        }
//    });
//
//    $('#cboSubCategory').on("removed", function(e) {
//        $('#checkbox_subncat').attr('checked', false); // Unchecks it
//        if (e.val != ''){
//            $('#validate_subcategory').html('<span id="error" style=\"color:red\">* Please Load Item</span>');
//
//        }
//    });


    $("#cboItem").select2({
        //placeholder: "Search for a repository",
    });

    $("#button").click(function(){
        alert($("#order_no").val());
    });

    function exportRepot() {
        var cboCompany_r = $('#cboCompany_r').val();
        var cboMainCategory = $('#cboMainCategory').val();
        var order_no = $('#order_no').val();
        var cboSubCategory = $('#cboSubCategory').val();
        var cboItem = $('#cboItem').val();

        if(!cboMainCategory) {
            cboMainCategory = 'null';
        }
        if(!order_no){
            order_no ='null'

        }
        if(!cboSubCategory){
            cboSubCategory='null'
        }
        if(!cboItem){
            cboItem='null'
        }


        if(cboCompany_r == '') {
            $('#validate_company').html('<span id="error" style=\"color:red\">* This Field is Required</span>');
            return false;
        }else if(cboCompany_r != ''){
            $('#validate_company').html('<span id="error" style=\"display: none\">* This Field is Required</span>');
        }
        if($('#pending_mrn').is(":checked")) {
            pending_mrn = 1;

        }else {
            pending_mrn = 0;
        }

        if($('#report_so_wise').is(":checked")) {
            report_so_wise = 1;

        }else {
            report_so_wise = 0;
        }
        if($('#balance_issues').is(":checked")) {
            balance_issues = 1;

        }else {
            balance_issues = 0;
        }
        if($('#balance_mrn').is(":checked")) {
            balance_mrn = 1;

        }else {
            balance_mrn = 0;
        }
        //presentation/procurement/reports/test.php
        
        var url2 ="presentation/procurement/reports/order_wise_stock_forcast_excel.php?cboCompany_r="+cboCompany_r+"&cboMainCategory="+cboMainCategory+"&cboSubCategory="+cboSubCategory+"&cboItem ="+cboItem+"&order_no="+order_no+"&balance_mrn="+balance_mrn+"&balance_issues="+balance_issues+"&report_so_wise="+report_so_wise+"&pending_mrn="+pending_mrn+"report_type=excel";
       //new way
       // var url2 ="presentation/warehouse/reports/report.php?type=order_wise_stock_order_forcast&cboCompany_r="+cboCompany_r+"&cboMainCategory="+cboMainCategory+"&cboSubCategory="+cboSubCategory+"&cboItem="+cboItem+"&order_no="+order_no+"&balance_mrn="+balance_mrn+"&balance_issues="+balance_issues+"&report_so_wise="+report_so_wise+"&pending_mrn="+pending_mrn+"&report_type=excel_order";

        window.open(url2,'_blank' );
    }

</script>
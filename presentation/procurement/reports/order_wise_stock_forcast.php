<?php
/**
 * Created by PhpStorm.
 * AUTHOR: Hasitha
 * Date: 9/22/2017
 * Time: 9:38 AM
 */



ini_set('display_errors', 'off');
set_time_limit(600);
session_start();
//echo 'Time Limit = ' . ini_get('max_execution_time') .

$companyId = $sessions->getCompanyId();
$head_office_loc =get_ho_loc($companyId);
$userId = $sessions->getUserId();
$locationId = $sessions->getLocationId();

$programCode = 'P1216';

require_once "class/cls_permisions.php";
$objpermisionget = new cls_permisions($db);
require_once "class/tables/sys_approvelevels.php";
$obj_sys_approvelevels = new sys_approvelevels($db);

$permi_price_view = $objpermisionget->boolSPermision(8);
$str_price_view = 'hidden';

if ($permi_price_view)
    $str_price_view = '';
/** get post data to filter option  */
if ($_SERVER['REQUEST_METHOD'] == 'POST'){
    if(isset($_POST['order_no'])){
        //$order_number = array();
        $order_number = implode(",",$_POST['order_no']);
//echo '<br>'.$order_number.'<br>'; die;
    }
    if(($_POST['cboMainCategory'])){

        $main_category= implode(",",$_POST['cboMainCategory']);
//        echo $main_category.'<br>';
    }
    if(isset($_POST['cboSubCategory'])){
        $sub_category = implode(",", $_POST['cboSubCategory']);
//        echo $sub_category.'<br>';
    }
    if(isset($_POST['cboItem'])){

        $item = implode(",",$_POST['cboItem']);
//        echo $item.'<br>';
    }
    if(isset($_POST['balance_mrn'])){
        $balance_mrn = $_POST['balance_mrn'];
//        echo  $balance_mrn .'<br>';
    }
    if(isset($_POST['balance_issues'])){
        $balance_issues = $_POST['balance_issues'];

//        echo $balance_issues.'<br>';
    }
    if(isset($_POST['report_so_wise'])){
        $so_wise =$_POST['report_so_wise'];
//        echo $so_wise.'<br>';
    }
    if(isset($_POST['pending_mrn'])== 1){
        $pending_mrn = $_POST['pending_mrn'];
        // var_dump($pending_mrn); die;
        //$pending_mrn= '1,-2';

    }else {
        $pending_mrn= '';
    }

    if(isset($_POST['cboCompany_r'])){
        $company =$_POST['cboCompany_r'];



        //$company= implode(",",$_POST['cboCompany_r']);


    }
    if(isset($_POST['Report'])){
        $export_excel= $_POST['Report'];
    }
    if($export_excel== 'excel')
    {

    }

    if(isset($_POST['txtFromDate']) && ($_POST['txtToDate'])){
        $fromDate = $_POST['txtFromDate'];
        $toDate = $_POST['txtToDate'];
    }
}

//$companyId = 1;
//$userId = 2;
//$locationId = 2;



?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Order Wise Stock Order Forecast Report</title>
    <script type="text/javascript" src="presentation/customerAndOperation/bulk/placeOrder/listing/rptBulkOrder-js.js?n=1"></script>
<!--    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/excellentexport@2.1.0/dist/excellentexport.min.js"></script>-->
<!--    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
<!--    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" type="text/css"/>-->
    <style type="text/css">
        .break {
            page-break-before: always;
        }

        @media print {
            .noPrint {
                display: none;
            }
        }

        #apDiv1 {
            position: absolute;
            left: 237px;
            top: 175px;
            width: 619px;
            height: 235px;
            z-index: 1;
        }

        .APPROVE {
            font-size: 16px;
            font-weight: bold;
            color: #00C;
        }
        .foo {
            float: left;
            width: 20px;
            height: 20px;
            margin: 1px;
            border: 1px solid rgba(0, 0, 0, .2);
        }

        .blue {
            background: #71ede4;
        }
        .red {
            background: #FF99CC;
        }

        /*div.dataTables_wrapper {*/
        /*width: 800px;*/
        /*margin: 0 auto;*/
        /*white-space: nowrap;*/
        /*}*/


        div.dataTables_length {
            padding-left: 2em;
        }
        div.dataTables_length,
        div.dataTables_filter {
            padding-top: 0.55em;
        }
        div.dt-buttons{
            position:relative !important;
            float:left !important;
        }
        #tblMainGrid_paginate{
            position: absolute;
            top: -87px;
            right: 837px;
            width: 436px;
            height: 100px;
        }

        .type_2 {
            background-color:  #FF99CC !important;
        }
        .pending_mrn{
            background-color: #2affe4 !important;
        }

    </style>
    <script src="libraries/dataTables/jquery.dataTables.min.js"></script>
    <script src="libraries/dataTables/dataTables.buttons.min.js"></script>
    <script src="libraries/dataTables/buttons.flash.min.js"></script>
    <script src="libraries/dataTables/jszip.min.js"></script>
    <script src="libraries/dataTables/pdfmake.min.js"></script>
    <script src="libraries/dataTables/vfs_fonts.js"></script>
    <script src="libraries/dataTables/buttons.html5.min.js"></script>
    <script src="libraries/dataTables/buttons.print.min.js "></script>
    <link rel="stylesheet" type="text/css" href="libraries/dataTables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="libraries/dataTables/css/buttons.dataTables.min.css">
    <script type="text/javascript">
        $(document).ready(function() {
            var table =$('#tblMainGrid').DataTable( {
                "bFilter": false,
                pageLength: 50,
                responsive: true,
                "scrollY": 600,
                "scrollX": true,
                dom: 'Bfrtip',
                buttons: [{
                    extend: 'print',
                    text: 'Print',
                    exportOptions: {
                        columns: ':not(.no-print)'
                    },
                    footer: true,
                    autoPrint: false
                }, {

                    extend: 'pdfHtml5',
                    messageTop: 'ORDER WISE STOCK ORDER FORECAST',
                    orientation: 'landscape',
                    pageSize: 'LEGAL',
                    text: 'PDF',


                },{

                    extend: 'excel',
                    messageTop: 'ORDER WISE STOCK ORDER FORECAST',
                    text: 'Excel',

                },
                    {

                        extend: 'csv',
                        text: 'CSV',
                        customize: function (csv) {

                            return '\t ORDER WISE STOCK ORDER FORECAST\n\n'+  csv;
                        },
                        exportOptions: {
                            columns: ':not(.no-print)'
                        }


                    }],
                "createdRow": function( row, data, dataIndex ) {
                    if ( data[0] == 2 ) {
                        $(row).addClass('type_2');
                    }else if(data[0]==45){
                        $(row).addClass('pending_mrn');
                    }

                },
                //invisible type column on table
                "columnDefs": [
                    {
                        "targets": [0],
                        "visible": false,
                        "searchable": false
                    }
                ]

            });
         


        } );
    </script>


</head>

<body>
<form id="frmSamplePlaceOrderReport" name="frmSamplePlaceOrderReport" method="post" action="rptStock_Forecast.php">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="3"></td>
        </tr>
        <tr>
            <td width="20%"></td>
            <td width="60%" height="80"
                valign="top"><?php include 'presentation/procurement/reports/reportHeader.php' ?></td>
            <td width="20%"></td>
        </tr>
        <tr>
            <td colspan="3">
                <div id="divPoNo" style="display:none"><?php echo $orderNo ?>/<?php echo $orderYear ?></div>
            </td>
        </tr>
    </table>
    <div align="center">
       
        <div style="background-color:#FFF"><strong>Order Wise Stock Order Forecast Report</strong><strong></strong></div>
        <table width="900" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td colspan="10" align="center" bgcolor="#FFFFFF">
                            </td>
                        </tr>
                        <!--                        <tr>-->
                        <!--                            <td width="1%"><span class="normalfnt">Date Range:</span></td>-->
                        <!--                            <td width="9%" class="normalfnt">--><?php //echo $fromDate; ?><!-- To --><?php //echo $toDate; ?><!--</td>-->
                        <!--                        </tr>-->
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                        <?php if($pending_mrn==1){?>
                        <tr><td><div class="foo blue"></div> <span>: Pending MRN</span></td>
                        <td>&nbsp;</td>
                        </tr>
                        <?php } ?>
                        <tr>
                            <td><div class="foo red"></div> <span>: Additional Item</span></td>
                        </tr>

                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <br/>
                    <table width="100%">
                        <tr>
                            <td colspan="20">
                                <div>
                                    <!--                                            <table width="100%" class="display nowrap bordered " id="tblMainGrid" cellspacing="0"-->
                                    <!--                                                   cellpadding="0">-->
                                    <table width="100%" class="table table-striped table-bordered bordered" id="tblMainGrid" cellspacing="0"
                                           cellpadding="0">
                                        <thead>
                                        <tr class="">
                                            <th class="tbheader"><div>Type</div></th>
                                            <th class="tbheader"><div>Order No</div></th>
                                            <th class="tbheader"><div>Order Type</div></th>
                                            <th class="tbheader"><div>Order Status</div></th>
                                            <th  class="tbheader"><div>Order Created Date</div></th>
                                            <?php if($so_wise !='') { ?>
                                                <th class="tbheader"><div>Sales Order</div></th>
                                            <?php } ?>
                                            <th class="tbheader"><div>Production Location</div></th>
                                            <th class="tbheader"><div>Main Category</div></th>
                                            <th class="tbheader"><div>Sub Category</div></th>
                                            <th class="tbheader"><div>Code</div>                </th>
                                            <th class="tbheader"><div>Item</div></th>
                                            <th class="tbheader"><div>UOM</div></th>
                                            <th  class="tbheader"><div>PRI</div></th>
                                            <?php if($so_wise !=1) { ?>
                                            <th class="tbheader"><div>PRN</div></th>
                                            <?php }  ?>
                                            <th class="tbheader"><div>ORDER BASED PO</div></th>
                                            <th  class="tbheader"><div>MRN</div></th>
                                            <th class="tbheader"><div>ISSUED</div></th>
                                            <th class="tbheader"><div>RETURNED</div></th>
                                            <th class="tbheader" ><div>GP OUT(From HO)</div></th>
                                            <th class="tbheader"><div>GP IN(TO HO)</div></th>
                                            <?php if($balance_mrn == 1) {  ?>
                                                <th class="tbheader"><div>BAL MRN</div></th>
                                            <?php } if($balance_issues == 1){ ?>
                                                <th class="tbheader"><div>BAL TO ISSUE</div></th>
                                            <?php } ?>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        $sql4 = get_forecastDetails($fromDate,$toDate,$head_office_loc,$companyId,$order_number,$main_category,$sub_category,$item,$pending_mrn,$so_wise,$company);
                                        $result4 = $db->RunQuery($sql4);

                                    $i=0;
                                    while ($row = mysqli_fetch_array($result4)) {
                                        $i++;

                                            $MRN_Qty 	=  round($row['MRN_QTY_TODATE'],4);
                                            if($balance_mrn == 1) {
                                                $bal_to_MRN = (round(($row['REQUIRED_TODATE'] - $MRN_Qty), 4));
                                            }else {
                                                $bal_to_MRN = 0;
                                            }
                                            if($balance_issues == 1)
                                            {
                                                $bal_to_issue_for_all = round((max($row['REQUIRED_TODATE'],$MRN_Qty)- $row['ISSUE_QTY_TODATE']),4);

                                                if ($bal_to_issue_for_all < 0)
                                                {
                                                    $bal_to_issue_for_all = 0;
                                                }
                                            }
//
//
                                        if($row['ORDER_STATUS'] ==1){
                                            $order_status ='Approved';
                                        }else if($row['ORDER_STATUS']==0) {
                                            $order_status ='Rejected';

                                        }else if($row['ORDER_STATUS']== -10) {
                                            $order_status ='Completed';

                                        }else if($row['ORDER_STATUS']== -2){
                                            $order_status ='Cancel';
                                        }else if($row['ORDER_STATUS']== -1) {
                                            $order_status ='Revised';
                                        }else{
                                            $order_status ='Pending';
                                        }

                                        if($row['ORDER_TYPE'] == 1) {
                                            $order_type= 'Dummy';
                                        }else if($row['ORDER_TYPE'] == 2){
                                            $order_type= 'Acctual';
                                        }else {
                                            $order_type= 'Other';
                                        }

                                            if($row['type']==1 && $i%2 == 0)
                                                $col	='#FFFFFF';
                                            else if($row['type']==1 && $i%2 != 0)
                                                $col	='#B7B7B7';
                                            if($row['type']!=1)
                                                $col	='#FF99CC';
                                            ?>
                                            <tr>
                                                <td><?php echo $row['type']; ?></td>
                                                <td><?php echo $row['ORDER_NO']; ?></td>
                                                <td><?php echo $order_type; ?></td>
                                                <td><?php echo $order_status; ?></td>
                                                <td><?php echo $row['CREATE_DATE']; ?></td>
                                                <?php if($so_wise !='') { ?>
                                                    <td><?php echo $row['SO']; ?></td>
                                                <?php } ?>
                                                <td><?php echo $row['location']; ?></td>
                                                <td><?php echo $row['mainCatName']; ?></td>
                                                <td><?php echo $row['subCatName']; ?></td>
                                                <?php
                                                if($row['supItemCode']!=null)
                                                {
                                                ?>
                                                <td><?php echo $row['supItemCode']; ?></td>
                                                <?php
                                                }else{
                                                ?>
                                                <td><?php echo $row['strCode']; ?></td>
                                                <?php
                                                }
                                                ?>
                                                <td><?php echo $row['itemName']; ?></td>
                                                <td><?php echo $row['UOM']; ?></td>
                                                <td><?php echo round($row['REQUIRED_TODATE'],4); ?></td>
                                                <?php if($so_wise !=1) { ?>
                                                <td><?php echo round($row['PRN_QTY_TODATE'],4); ?></td>
                                                <?php } ?>
                                                <td><?php echo round($row['PO_QTY_TODATE'],4); ?></td>
                                                <td>&nbsp;<?php echo $MRN_Qty; ?></td>
                                                <td><?php echo round($row['ISSUE_QTY_TODATE'],4); ?></td>
                                                <td><?php echo round($row['RET_TO_STORE_QTY_TODATE'],4); ?></td>
                                                <td><?php echo round($row['GP_FROM_HO_TODATE'],4); ?></td>
                                                <td><?php echo round($row['GP_IN_QTY_TO_HO_TODATE'],4); ?></td>
                                                <?php if($balance_mrn == 1) {
                                                    if($bal_to_MRN>0) {   ?>
                                                        <td><?php echo $bal_to_MRN; ?></td>
                                                    <?php }else {
                                                        echo '<td>0</td>';
                                                    } ?>
                                                <?php } if($balance_issues == 1){ ?>
                                                    <td><?php echo $bal_to_issue_for_all; ?></td>
                                                <?php } ?>

                                            </tr>
                                        <?php } ?>

                                        </tbody>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</form>
</body>
</html>
<?php
function get_ho_loc($companyId){

    global $db;
    $sql="SELECT
			mst_locations.intId
			FROM `mst_locations`
			WHERE
			mst_locations.COMPANY_MAIN_STORES_LOCATION = 1 AND
			mst_locations.intCompanyId = '$companyId'
			";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    return $row['intId'];

}
/*
 * Pending MRN
 * 2017/10/31
 */
function get_pending_mrn_details($companyId,$head_office_loc,$locationId,$company,$maincategory,$subcategory,$item,$order_number,$so_wise)
{

    $sql = "SELECT
TB_ALL.intCompanyId,
TB_ALL.intItemId,
TB_ALL.MRN_QTY,
TB_ALL.item,
TB_ALL.strCode,
TB_ALL.supItemCode,
TB_ALL.re_order_policy,
TB_ALL.mainCat,
TB_ALL.mainCat_id,
TB_ALL.subCat,
TB_ALL.subCat_id,
TB_ALL.unit,
TB_ALL.GP_IN,
TB_ALL.GP_OUT,
TB_ALL.ISSUE_QTY,
TB_ALL.PO_QTY,
TB_ALL.PRI_QTY,
TB_ALL.PRN_QTY,
TB_ALL.RET_TO_STORES,
TB_ALL.ORDER_NO,
TB_ALL.ORDER_TYPE,
TB_ALL.ORDER_STATUS,
TB_ALL.CREATE_DATE,
TB_ALL.SALES_ORDER,
TB_ALL.location AS location
FROM 
(SELECT
TB2.intCompanyId,
TB2.intItemId,
TB2.item,
TB2.strCode,
TB2.supItemCode,
TB2.re_order_policy,
TB2.mainCat,
TB2.mainCat_id,
TB2.subCat,
TB2.subCat_id,
TB2.unit,
SUM(TB2.MRN_QTY) AS MRN_QTY,
SUM(TB2.ISSUE_QTY) AS ISSUE_QTY,
SUM(TB2.PRI_QTY) AS PRI_QTY,
SUM(TB2.RET_TO_STORES) AS RET_TO_STORES,
SUM(TB2.GP_OUT) AS GP_OUT,
SUM(TB2.GP_IN) AS GP_IN,
SUM(TB2.PRN_QTY) AS PRN_QTY,
SUM(TB2.PO_QTY) AS PO_QTY,
TB2.ORDER_NO,
TB2.ORDER_TYPE,
TB2.ORDER_STATUS,
TB2.CREATE_DATE,
TB2.SALES_ORDER,
TB2.location AS location
FROM(select 
TB1.intOrderNo  AS ORDER_NO,
TB1.intOrderYear,
TB1.intItemId,
TB1.intCompanyId,
TB1.item,
TB1.strCode,
TB1.supItemCode,
TB1.re_order_policy,
TB1.mainCat,
TB1.mainCat_id,
TB1.subCat,
TB1.subCat_id,
TB1.unit,
SUM(TB1.mrn_qty) AS MRN_QTY,
SUM(TB1.issue_qty)as ISSUE_QTY,
SUM(TB1.PRI_QTY) as PRI_QTY,
SUM(TB1.ret_to_stores) AS RET_TO_STORES,
SUM(TB1.GP_Out) AS GP_OUT,
SUM(TB1.GP_IN_QTY) AS GP_IN,



( SELECT SUM(trn_po_prn_details.PRN_QTY) as prn_qty 
FROM trn_po_prn_details 
WHERE trn_po_prn_details.ORDER_NO = TB1.intOrderNo AND 
trn_po_prn_details.ORDER_YEAR = TB1.intOrderYear AND 
trn_po_prn_details.ITEM = TB1.intItemId) AS PRN_QTY,

(SELECT SUM(trn_po_prn_details.PURCHASED_QTY) as po_qty 
FROM trn_po_prn_details WHERE 
trn_po_prn_details.ORDER_NO = TB1.intOrderNo AND 
trn_po_prn_details.ORDER_YEAR = TB1.intOrderYear AND 
trn_po_prn_details.ITEM = TB1.intItemId ) AS PO_QTY,
TB1.ORDER_TYPE,
TB1.ORDER_STATUS,
TB1.CREATE_DATE,
TB1.SALES_ORDER,
TB1.location AS location

FROM(

select  

ware_mrndetails.intOrderNo,  

ware_mrndetails.intOrderYear,
ware_mrndetails.intItemId,
mst_locations.intCompanyId,
mst_maincategory.intId AS mainCat_id,
mst_maincategory.strName AS mainCat,
mst_subcategory.intId AS subCat_id,
mst_subcategory.strName As subCat,
mst_item.strName AS item,
mst_item.strCode,
CONCAT(mst_supplier.strCode,mst_item.strCode) as supItemCode,
mst_item.intReorderPolicy as re_order_policy,
mst_units.strName AS unit,
	
sum(ware_mrndetails.dblQty) as mrn_qty, 
sum(ware_mrndetails.dblIssudQty) as issue_qty, 

(select SUM(trn_po_prn_details_sales_order.REQUIRED) 
from trn_po_prn_details_sales_order
WHERE 
trn_po_prn_details_sales_order.ORDER_NO = ware_mrndetails.intOrderNo AND 
trn_po_prn_details_sales_order.ORDER_YEAR = ware_mrndetails.intOrderYear AND
trn_po_prn_details_sales_order.SALES_ORDER = ware_mrndetails.strStyleNo AND  
trn_po_prn_details_sales_order.ITEM = ware_mrndetails.intItemId ) AS PRI_QTY, 


(select SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'RETURNED_TO_STORES',
 trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS RET_TO_STORE_QTY 
FROM trn_po_prn_details_sales_order_stock_transactions 
WHERE trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = ware_mrndetails.intOrderNo 
AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = ware_mrndetails.intOrderYear 
AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = ware_mrndetails.strStyleNo
AND trn_po_prn_details_sales_order_stock_transactions.ITEM = ware_mrndetails.intItemId 
AND trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID = '$locationId' ) as ret_to_stores,

 (select SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'GATE_PASS',
 trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS GP_QTY 
FROM trn_po_prn_details_sales_order_stock_transactions 
WHERE trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = ware_mrndetails.intOrderNo AND 
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = ware_mrndetails.intOrderYear AND 
 trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = ware_mrndetails.strStyleNo AND 
trn_po_prn_details_sales_order_stock_transactions.ITEM = ware_mrndetails.intItemId AND 
trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID = '$locationId' )as GP_Out, 

(select SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'GATE_PASS_IN', 
trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS GP_IN_QTY 
FROM trn_po_prn_details_sales_order_stock_transactions 
WHERE trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = ware_mrndetails.intOrderNo AND 
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = ware_mrndetails.intOrderYear AND 
 trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = ware_mrndetails.strStyleNo AND
trn_po_prn_details_sales_order_stock_transactions.ITEM = ware_mrndetails.intItemId AND 
trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID = '$locationId' )GP_IN_QTY,
trn_orderheader.PO_TYPE AS ORDER_TYPE,
trn_orderheader.intStatus AS ORDER_STATUS,
trn_orderheader.dtDate AS CREATE_DATE,
(
trn_orderdetails.strSalesOrderNo
) AS SALES_ORDER,
mst_locations.strName AS location

from ware_mrndetails 
INNER JOIN ware_mrnheader on ware_mrndetails.intMrnNo = ware_mrnheader.intMrnNo AND 
ware_mrndetails.intMrnYear= ware_mrnheader.intMrnYear 
INNER JOIN mst_locations on ware_mrnheader.intCompanyId = mst_locations.intId
INNER JOIN mst_item ON ware_mrndetails.intItemId = mst_item.intId 
INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId 
INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId 
INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
INNER JOIN trn_orderdetails ON ware_mrndetails.intOrderNo = trn_orderdetails.intOrderNo
AND ware_mrndetails.intOrderYear = trn_orderdetails.intOrderYear
AND ware_mrndetails.strStyleNo = trn_orderdetails.intSalesOrderId
LEFT JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
WHERE 
((ware_mrnheader.intStatus <= (ware_mrnheader.intApproveLevels+1) && ware_mrnheader.intStatus >1))
AND     /*change 2017/09/11*/
mst_locations.intCompanyId IN ($company) ";

    if($so_wise == '1') {
        $sql .=" GROUP BY 
ware_mrndetails.intOrderNo,
ware_mrndetails.intOrderYear,
ware_mrndetails.strStyleNo,
ware_mrndetails.intItemId,
trn_orderdetails.strSalesOrderNo
) as  TB1

GROUP BY 
TB1.intOrderNo,
TB1.intOrderYear,
TB1.intItemId,
TB1.SALES_ORDER
)AS TB2
GROUP BY 
TB2.ORDER_NO,
TB2.intOrderYear,
TB2.intItemId,
TB2.SALES_ORDER
) 
AS TB_ALL
where
1=1 ";
}else {
        $sql .=" GROUP BY 
ware_mrndetails.intOrderNo,
ware_mrndetails.intOrderYear,
ware_mrndetails.strStyleNo,
ware_mrndetails.intItemId
) as  TB1
GROUP BY 
TB1.intOrderNo,
TB1.intOrderYear,
TB1.intItemId
)AS TB2

GROUP BY 
TB2.ORDER_NO,
TB2.intOrderYear,
TB2.intItemId
) 

AS TB_ALL

where
1=1 ";
    }


    $sql .=  "  AND TB_ALL.intCompanyId IN ($company)";
//    $sql .=  "  AND TB_ALL.intCompanyId = 2";

    //TB_ALL.ORDER_NO
    if($order_number !='' || $order_number != NULL ){
        $sql .= "AND TB_ALL.ORDER_NO  IN ($order_number)";
    }

    if($maincategory !=''){
        $sql .=  "  AND TB_ALL.mainCat_id IN ($maincategory)";
    }

    if($subcategory !='') {
        $sql .=  "  AND TB_ALL.subCat_id IN ($subcategory)";
    }
    if($item !='')
    {
        $sql .=  "  AND TB_ALL.intItemId IN ($item)";
    }

    return $sql;
}

function get_forecastDetails($fromDate,$toDate,$head_office_loc,$companyId,$order_number,$main_category,$sub_category,$item,$pending_mrn,$so_wise,$company){

    $sql = "SELECT 
TB_ALL .type,
TB_ALL .date as DATES_TODATE, 
TB_ALL .date as DATES_OTHER, 
TB_ALL .ORDER_COMPANY,
TB_ALL .location AS location,
TB_ALL .mainCatId,
TB_ALL .subCatId, 
TB_ALL .ITEM, 
TB_ALL .re_order_policy,
TB_ALL .mainCatName, 
TB_ALL .subCatName,
TB_ALL .strCode,
TB_ALL.supItemCode,
TB_ALL .itemName, 
TB_ALL .otherOrders AS TODATE_ORDERS,
TB_ALL .REQUIRED AS REQUIRED_TODATE,
TB_ALL .PRN_QTY AS PRN_QTY_TODATE,
TB_ALL .PO_QTY AS PO_QTY_TODATE, 
(
ifnull(TB_ALL.MRN_QTY,0) + ifnull(TB_ALL.MRN_QTY_OTHER_LOC,0)
) AS MRN_QTY_TODATE,

(
ifnull(TB_ALL.ISSUE_QTY,0) + ifnull(TB_ALL.ISSUE_QTY_OTHER_LOC,0) 
) AS ISSUE_QTY_TODATE,

(
ifnull(TB_ALL.RET_TO_STORE_QTY,0) 
+ ifnull(TB_ALL.RET_TO_STORE_QTY_OTHER_LOC,0) 
) AS RET_TO_STORE_QTY_TODATE,

TB_ALL.GP_QTY AS GP_FROM_HO_TODATE,
TB_ALL.GP_IN_QTY AS GP_IN_QTY_TO_HO_TODATE,
TB_ALL.UOM,
TB_ALL.otherOrders AS OTHER_ORDERS, 

(
ifnull(TB_ALL.ISSUE_QTY,0) 
) AS ISSUE_QTY_ALL_HO,

(
ifnull(TB_ALL.GP_QTY,0)  
) AS GP_FROM_HO_ALL,

(
ifnull(TB_ALL.ISSUE_QTY_OTHER_LOC,0)  
) AS ISSUE_QTY_ALL_NON_HO,

TB_ALL.PO_QTY AS PO_QTY_OTHER,
TB_ALL.ORDER_NO,
TB_ALL.ORDER_STATUS,
TB_ALL.ORDER_TYPE,
TB_ALL.CREATE_DATE,
TB_ALL.SO

from 
/*tb_all*/	
(select tb.type,
tb.ORDER_COMPANY, 
tb.location AS location, 
tb.date,
tb.ITEM, 
GROUP_CONCAT(DISTINCT tb.ORDER_NO,'/',tb.ORDER_YEAR) as otherOrders,
 
SUM(tb.REQUIRED) as REQUIRED,
tb.TRANSACTION_LOCATION, 
sum(tb.PO_QTY) as PO_QTY, 
sum(tb.PRN_QTY) as PRN_QTY, 
tb.mainCatId,
tb.subCatId,
tb.mainCatName,
tb.subCatName, 
tb.strCode,
tb.supItemCode, 
tb.itemName, 
tb.re_order_policy,
tb.UOM,
(SUM(tb.MRN_QTY) + SUM(tb.mrn_pending)) AS MRN_QTY,
SUM(tb.MRN_EXTRA_QTY) as MRN_EXTRA_QTY,
SUM(tb.ISSUE_QTY) as ISSUE_QTY,
SUM(tb.GP_QTY) as GP_QTY,
SUM(tb.GP_IN_QTY) as GP_IN_QTY,
SUM(tb.RET_TO_STORE_QTY) as RET_TO_STORE_QTY,
SUM(tb.MRN_QTY_OTHER_LOC) as MRN_QTY_OTHER_LOC,
SUM(tb.MRN_EXTRA_QTY_OTHER_LOC) as MRN_EXTRA_QTY_OTHER_LOC,
SUM(tb.ISSUE_QTY_OTHER_LOC) as ISSUE_QTY_OTHER_LOC,
SUM(tb.GP_QTY_OTHER_LOC) as GP_QTY_OTHER_LOC,
SUM(tb.GP_IN_QTY_OTHER_LOC) as GP_IN_QTY_OTHER_LOC,
SUM(tb.RET_TO_STORE_QTY_OTHER_LOC) as RET_TO_STORE_QTY_OTHER_LOC ,
tb.ORDER_NO,
tb.ORDER_STATUS,
tb.ORDER_TYPE,
tb.CREATE_DATE,
tb.SO
/*tb*/
from(
SELECT TB3.type,
TB3.ORDER_COMPANY,
TB3.location AS location,
TB3.date,
TB3.ITEM,
TB3.ORDER_NO,
TB3.ORDER_YEAR,";

    if ($so_wise == '1') {
        $sql .= "(TB3.REQUIRED) AS REQUIRED,";
    }
    if ($so_wise == '') {
        $sql .=" (
SELECT
sum(
trn_po_prn_details.REQUIRED
)
FROM
trn_po_prn_details
WHERE
trn_po_prn_details.ORDER_NO = TB3.ORDER_NO
AND trn_po_prn_details.ORDER_YEAR = TB3.ORDER_YEAR
AND trn_po_prn_details.ITEM = TB3.ITEM
) AS REQUIRED, ";

    }

        $sql .= "
(
SELECT
sum(
trn_po_prn_details.PURCHASED_QTY) 
FROM trn_po_prn_details 
WHERE trn_po_prn_details.ORDER_NO = TB3.ORDER_NO 
AND trn_po_prn_details.ORDER_YEAR = TB3.ORDER_YEAR 
AND trn_po_prn_details.ITEM = TB3.ITEM ) AS PO_QTY, 

(SELECT SUM(trn_po_prn_details.PRN_QTY)
FROM trn_po_prn_details 
WHERE trn_po_prn_details.ORDER_NO = TB3.ORDER_NO AND
trn_po_prn_details.ORDER_YEAR = TB3.ORDER_YEAR AND
trn_po_prn_details.ITEM = TB3.ITEM ) AS PRN_QTY,";

    $sql .= "

TB3.TRANSACTION_LOCATION,
SUM(TB3.mrn_pending) AS mrn_pending,
TB3.mainCatId,
TB3.subCatId,
TB3.mainCatName, 
TB3.subCatName, 
TB3.strCode,
TB3.supItemCode,
TB3.itemName, 
TB3.re_order_policy,
TB3.UOM, 
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.MRN_QTY,0)) as MRN_QTY, 
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.MRN_EXTRA_QTY,0)) as MRN_EXTRA_QTY, 
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.ISSUE_QTY,0)) as ISSUE_QTY, 
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.GP_QTY,0)) as GP_QTY, 
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.GP_IN_QTY,0)) as GP_IN_QTY, 
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.RET_TO_STORE_QTY,0)) as RET_TO_STORE_QTY, 
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.MRN_QTY,0)) as MRN_QTY_OTHER_LOC, 
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.MRN_EXTRA_QTY,0)) as MRN_EXTRA_QTY_OTHER_LOC, 
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.ISSUE_QTY,0)) as ISSUE_QTY_OTHER_LOC, 
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.GP_QTY,0)) as GP_QTY_OTHER_LOC, 
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.GP_IN_QTY,0)) as GP_IN_QTY_OTHER_LOC, 
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.RET_TO_STORE_QTY,0)) as RET_TO_STORE_QTY_OTHER_LOC,
TB3.ORDER_STATUS,
TB3.ORDER_TYPE,
TB3.CREATE_DATE,
TB3.SO
FROM 
/*tb3*/	(SELECT TB2.ORDER_COMPANY, 
TB2.ORDER_NO, 
TB2.ORDER_YEAR, 
TB2.location, 
TB2.ITEM,
TB2.SALES_ORDER,
TB2.type,
(TB2.pending_mrn_qty_new) AS mrn_pending,
CONCAT(trn_orderdetails.strSalesOrderNo,'/',trn_orderdetails.strCombo,'/',trn_orderdetails.strPrintName) AS SALES_ORDER_String,
'' AS date,
mst_maincategory.intId as mainCatId,
mst_subcategory.intId AS subCatId,  
mst_maincategory.strName AS mainCatName, 
mst_subcategory.strName AS subCatName, 
mst_item.strCode,
CONCAT(mst_supplier.strCode,mst_item.strCode) as supItemCode,
mst_item.intId, 
mst_item.strName AS itemName, 
mst_item.intReorderPolicy AS re_order_policy,
mst_units.strName AS UOM,";

    if ($so_wise == '1') {
        $sql .= "trn_po_prn_details_sales_order.REQUIRED,";
    }
    $sql .= "trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID AS TRANSACTION_LOCATION,



SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'MRN', 
trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS MRN_QTY, 
SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'MRN_EXTRA', 
trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS MRN_EXTRA_QTY, 
SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'ISSUE', 
trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS ISSUE_QTY, 
SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'GATE_PASS', 
trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS GP_QTY, 
SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'GATE_PASS_IN',
trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS GP_IN_QTY, 
SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'RETURNED_TO_STORES', 
trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS RET_TO_STORE_QTY,
TB2.ORDER_STATUS,
TB2.ORDER_TYPE,
TB2.CREATE_DATE,
(
trn_orderdetails.strSalesOrderNo
) AS SO
FROM
/*TB2*/( SELECT TB1.ORDER_NO,
									TB1.ORDER_YEAR,
									TB1.SALES_ORDER,
									TB1.ITEM,
									TB1.ORDER_COMPANY,
									TB1.location,
									sum(TB1.pending_mrn_qty) AS pending_mrn_qty_new,
									TB1.type,
									TB1.ORDER_STATUS,
									TB1.ORDER_TYPE,
									TB1.CREATE_DATE FROM
/*tb1*/ (
/* PRI + MOVED */ 
SELECT
trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER,
trn_po_prn_details_sales_order_stock_transactions.ITEM,
mst_locations.intCompanyId AS ORDER_COMPANY,
mst_locations.strName AS location,
0 as pending_mrn_qty,
( SELECT GROUP_CONCAT(mst_locations.intCompanyId)
FROM ware_fabricreceivedheader
INNER JOIN mst_locations
ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId
WHERE
ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO
AND ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
)AS FABRIC_IN_COMPANIES,
1 AS type,
trn_orderheader.intStatus AS ORDER_STATUS,
trn_orderheader.PO_TYPE AS ORDER_TYPE,
trn_orderheader.dtDate AS CREATE_DATE
FROM trn_po_prn_details_sales_order_stock_transactions 
INNER JOIN trn_po_prn_details_sales_order 
ON 
trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = trn_po_prn_details_sales_order.ORDER_NO 
AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR 
AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER 
AND trn_po_prn_details_sales_order_stock_transactions.ITEM = trn_po_prn_details_sales_order.ITEM 
INNER JOIN trn_orderheader 
ON trn_orderheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO AND 
trn_orderheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR 
AND trn_orderheader.intStatus = 1
INNER JOIN mst_locations 
ON mst_locations.intId = trn_orderheader.intLocationId 

GROUP BY trn_po_prn_details_sales_order_stock_transactions.ORDER_NO, 
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR, 
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER, 
trn_po_prn_details_sales_order_stock_transactions.ITEM 
HAVING FIND_IN_SET('$company', FABRIC_IN_COMPANIES) OR ORDER_COMPANY = '$company' 

UNION ALL
 /* MOVED + NOT IN PRI */
SELECT
trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER,
trn_po_prn_details_sales_order_stock_transactions.ITEM,
mst_locations.intCompanyId AS ORDER_COMPANY,
mst_locations.strName AS location,
0 as pending_mrn_qty,
( SELECT GROUP_CONCAT( DISTINCT mst_locations.intCompanyId )
FROM ware_fabricreceivedheader
INNER JOIN mst_locations
ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId
WHERE
ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO AND
ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
) AS FABRIC_IN_COMPANIES,
2 AS type,
trn_orderheader.intStatus AS ORDER_STATUS,
trn_orderheader.PO_TYPE AS ORDER_TYPE,
trn_orderheader.dtDate AS CREATE_DATE
FROM trn_po_prn_details_sales_order_stock_transactions 

LEFT JOIN trn_po_prn_details_sales_order 
ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = trn_po_prn_details_sales_order.ORDER_NO AND 
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR AND 
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER AND 
trn_po_prn_details_sales_order_stock_transactions.ITEM = trn_po_prn_details_sales_order.ITEM 
INNER JOIN trn_orderheader ON 
trn_orderheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO AND 
trn_orderheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR 
AND trn_orderheader.intStatus = 1
INNER JOIN mst_locations ON mst_locations.intId = trn_orderheader.intLocationId 

WHERE trn_po_prn_details_sales_order.ORDER_NO IS NULL 

GROUP BY 
trn_po_prn_details_sales_order_stock_transactions.ORDER_NO, 
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR, 
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER, 
trn_po_prn_details_sales_order_stock_transactions.ITEM 
HAVING ( FIND_IN_SET('$company', FABRIC_IN_COMPANIES) OR ORDER_COMPANY = '$company' ) 


UNION ALL
 /* IN PRI + NOT MOVED */ 
SELECT
trn_po_prn_details_sales_order.ORDER_NO,
trn_po_prn_details_sales_order.ORDER_YEAR,
trn_po_prn_details_sales_order.SALES_ORDER,
trn_po_prn_details_sales_order.ITEM,
mst_locations.intCompanyId AS ORDER_COMPANY,
mst_locations.strName AS location,
0 as pending_mrn_qty,
( SELECT GROUP_CONCAT( DISTINCT mst_locations.intCompanyId )
FROM ware_fabricreceivedheader
INNER JOIN mst_locations
ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId
WHERE ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order.ORDER_NO
AND ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order.ORDER_YEAR
) AS FABRIC_IN_COMPANIES,
1 AS type,
trn_orderheader.intStatus AS ORDER_STATUS,
trn_orderheader.PO_TYPE AS ORDER_TYPE,
trn_orderheader.dtDate AS CREATE_DATE
FROM trn_po_prn_details_sales_order_stock_transactions

RIGHT JOIN 
trn_po_prn_details_sales_order 
ON 
trn_po_prn_details_sales_order_stock_transactions.ORDER_NO    = trn_po_prn_details_sales_order.ORDER_NO AND 
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR  = trn_po_prn_details_sales_order.ORDER_YEAR AND 
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER AND 
trn_po_prn_details_sales_order_stock_transactions.ITEM        = trn_po_prn_details_sales_order.ITEM 
INNER JOIN trn_orderheader 
ON trn_orderheader.intOrderNo = trn_po_prn_details_sales_order.ORDER_NO AND 
trn_orderheader.intOrderYear  = trn_po_prn_details_sales_order.ORDER_YEAR 
AND trn_orderheader.intStatus = 1
INNER JOIN mst_locations 
ON mst_locations.intId = trn_orderheader.intLocationId 

WHERE trn_po_prn_details_sales_order_stock_transactions.ORDER_NO IS NULL 

GROUP BY 
trn_po_prn_details_sales_order.ORDER_NO, 
trn_po_prn_details_sales_order.ORDER_YEAR, 
trn_po_prn_details_sales_order.SALES_ORDER, 
trn_po_prn_details_sales_order.ITEM 

HAVING ( FIND_IN_SET('$company', FABRIC_IN_COMPANIES) OR ORDER_COMPANY = '$company' )";

if($pending_mrn==1) {
    $sql .= " UNION ALL 
	
 /* PENDING MRN - (PENDINGS TO MOVE BUT MAY BE NOT IN PRI) */	
SELECT
ware_mrndetails.intOrderNo AS ORDER_NO,
ware_mrndetails.intOrderYear AS ORDER_YEAR,
ware_mrndetails.strStyleNo AS SALES_ORDER,
ware_mrndetails.intItemId AS ITEM,
mst_locations.intCompanyId AS ORDER_COMPANY,
mst_locations.strName AS location,
ware_mrndetails.dblQty as pending_mrn_qty,

(
SELECT
GROUP_CONCAT(
DISTINCT mst_locations.intCompanyId
)
FROM
ware_fabricreceivedheader
INNER JOIN mst_locations ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId
WHERE
ware_fabricreceivedheader.intOrderNo = ware_mrndetails.intOrderNo
AND ware_fabricreceivedheader.intOrderYear = ware_mrndetails.intOrderYear
) AS FABRIC_IN_COMPANIES,
2 AS type,

0 AS ORDER_STATUS,
0 AS ORDER_TYPE,
0 AS CREATE_DATE
-- ware_mrndetails.dblQty
FROM
trn_po_prn_details_sales_order_stock_transactions
RIGHT JOIN ware_mrndetails ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = ware_mrndetails.intOrderNo
AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = ware_mrndetails.intOrderYear
AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = ware_mrndetails.strStyleNo
AND trn_po_prn_details_sales_order_stock_transactions.ITEM = ware_mrndetails.intItemId
LEFT JOIN trn_po_prn_details_sales_order ON ware_mrndetails.intOrderNo = trn_po_prn_details_sales_order.ORDER_NO
AND ware_mrndetails.intOrderYear = trn_po_prn_details_sales_order.ORDER_YEAR
AND ware_mrndetails.strStyleNo = trn_po_prn_details_sales_order.SALES_ORDER
AND ware_mrndetails.intItemId = trn_po_prn_details_sales_order.ITEM
INNER JOIN ware_mrnheader ON ware_mrndetails.intMrnNo = ware_mrnheader.intMrnNo
AND ware_mrndetails.intMrnYear = ware_mrnheader.intMrnYear
INNER JOIN mst_locations ON ware_mrnheader.intCompanyId = mst_locations.intId

WHERE
(
ware_mrndetails.intOrderNo > 0
AND (
ware_mrnheader.intStatus <= (
ware_mrnheader.intApproveLevels + 1
) && ware_mrnheader.intStatus > 1
) -- || ware_mrnheader.intStatus=0 
)
AND mst_locations.intCompanyId IN ($company)
GROUP BY
ware_mrndetails.intOrderNo,
ware_mrndetails.intOrderYear,
ware_mrndetails.strStyleNo,
ware_mrndetails.intItemId
HAVING
(
FIND_IN_SET('$company', FABRIC_IN_COMPANIES)
OR ORDER_COMPANY = '$company'
) ";

}

 

$sql.=" ) AS TB1  /* TB1 - all order wise rm  => moved+in pri, moved+not in pri, not moved+in pri, pending to move+not in pri    */ 
GROUP BY
TB1.ORDER_NO,
TB1.ORDER_YEAR,
TB1.SALES_ORDER,
TB1.ITEM ) AS TB2 


LEFT JOIN 
trn_po_prn_details_sales_order_stock_transactions 
ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = TB2.ORDER_NO AND 
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = TB2.ORDER_YEAR AND 
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = TB2.SALES_ORDER AND 
trn_po_prn_details_sales_order_stock_transactions.ITEM = TB2.ITEM 
INNER JOIN trn_orderheader 
ON trn_orderheader.intOrderNo = TB2.ORDER_NO AND 
trn_orderheader.intOrderYear = TB2.ORDER_YEAR AND
trn_orderheader.intStatus = 1
LEFT JOIN trn_po_prn_details_sales_order ON trn_po_prn_details_sales_order.ORDER_NO = TB2.ORDER_NO
AND trn_po_prn_details_sales_order.ORDER_YEAR = TB2.ORDER_YEAR
AND trn_po_prn_details_sales_order.SALES_ORDER = TB2.SALES_ORDER
AND trn_po_prn_details_sales_order.ITEM = TB2.ITEM
INNER JOIN trn_orderdetails 
ON TB2.ORDER_NO     = trn_orderdetails.intOrderNo 
AND TB2.ORDER_YEAR  = trn_orderdetails.intOrderYear 
AND TB2.SALES_ORDER = trn_orderdetails.intSalesOrderId 
INNER JOIN mst_item 
ON TB2.ITEM = mst_item.intId
LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
INNER JOIN mst_units 
ON mst_item.intUOM = mst_units.intId 
INNER JOIN mst_maincategory 
ON mst_item.intMainCategory = mst_maincategory.intId 
INNER JOIN mst_subcategory 
ON mst_item.intSubCategory = mst_subcategory.intId 
GROUP BY 
TB2.ORDER_NO, 
TB2.ORDER_YEAR, 
TB2.SALES_ORDER, 
TB2.ITEM,
TB2.ORDER_COMPANY
) AS TB3 ";
if($so_wise == '1') {

    $sql .=" GROUP BY
TB3.ORDER_NO,
TB3.ORDER_YEAR,
TB3.ITEM,
TB3.SO ) 
as 
tb 
GROUP BY 
tb.ORDER_NO,
tb.ORDER_YEAR,
tb.ITEM,
tb.SO
 ) as TB_ALL 
WHERE
1 = 1 ";

}else {
    $sql .= " GROUP BY
TB3.ORDER_NO,
TB3.ORDER_YEAR,
TB3.ITEM ) 
as 
tb 
GROUP BY 
tb.ORDER_NO,
tb.ORDER_YEAR,
tb.ITEM
 ) as TB_ALL 
WHERE
1 = 1 ";
}
    //$sql .=  " AND TB_ALL .ORDER_COMPANY IN (2)";
    $sql .=  " AND TB_ALL .ORDER_COMPANY  = $company ";

    if($order_number !='' || $order_number != NULL ){
        $sql .= "AND TB_ALL.ORDER_NO IN ($order_number)";
    }

    if($main_category !='' || $main_category != NULL) {

        $sql .= " AND TB_ALL.mainCatId IN ($main_category)";

    }

    if($sub_category !='' || $sub_category != NULL) {
        $sql .= "AND TB_ALL.subCatId IN ($sub_category)";
    }
    if($item !='' || $item != NULL) {
        $sql .= "AND TB_ALL.ITEM IN ($item)";

    }
    $sql .=   " ORDER BY TB_ALL.date ASC";

    return $sql;
}

?>


<?php
/**
 * Created by PhpStorm.
 * User: Hasitha Charaka
 * Date: 10/26/2017
 * Time: 12:31 PM
 */
ini_set('display_errors', 'off');
ini_set('memory_limit', '1024M');
set_time_limit(0);
session_start();

$servername = $_SESSION['Server'];
$username = $_SESSION['UserName'];
$password = $_SESSION['Password'];
$dbname = $_SESSION['Database'];

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}
$companyId = $_SESSION['headCompanyId'];
$head_office_loc =get_ho_loc($companyId,$conn);
$userId = $_SESSION['userId'];
$locationId = $_SESSION['CompanyID'];

if ($_REQUEST == $_GET) {

    if(isset($_GET['order_no'])){
        //$order_number = array();
        $order_number = $_GET['order_no'];
//       echo $order_number.'<br>'; die;
    }
    if(($_GET['cboMainCategory'])){

        $main_category= $_GET['cboMainCategory'];
//        echo $main_category.'<br>';
    }
    if(isset($_GET['cboSubCategory'])){
        $sub_category = $_GET['cboSubCategory'];
//        echo $sub_category.'<br>';
    }
    if(isset($_GET['cboItem'])){

        $item = $_GET['cboItem'];
//        echo $item.'<br>';
    }else {
        $item = '';
    }
    if(isset($_GET['balance_mrn'])){
        $balance_mrn = $_GET['balance_mrn'];
//        echo  $balance_mrn .'<br>';
    }
    if(isset($_GET['balance_issues'])){
        $balance_issues = $_GET['balance_issues'];

//        echo $balance_issues.'<br>';
    }
    if(isset($_GET['report_so_wise'])){
        $so_wise =$_GET['report_so_wise'];
//        echo $so_wise.'<br>';
    }
    if(isset($_GET['pending_mrn'])== 1){
        $pending_mrn = $_GET['pending_mrn'];
    }else {
        $pending_mrn= '';
    }

//     echo $pending_mrn.'<br>';

    if(isset($_GET['cboCompany_r'])){
        $company= $_GET['cboCompany_r'];
        //$head_office_loc =get_ho_loc($company,$conn);
//        echo $company.'<br>';
    }
    if(isset($_GET['Report'])){
        $export_excel= $_POST['Report'];


    }
//     if($export_excel== 'excel')
//     {
//
//     }

//     if(isset($_POST['txtFromDate']) && ($_POST['txtToDate'])){
//         $fromDate = $_POST['txtFromDate'];
//         $toDate = $_POST['txtToDate'];
//     }


}

$fromDate = 0;
$toDate = 0;


/** Error reporting */
error_reporting(E_ALL);

define("DIR_PATH", "libraries/excel/Classes/PHPExcel.php");


require_once ($_SESSION['ROOT_PATH'].DIR_PATH);

// Create new PHPExcel
//create alphabet A-Z  and Number range 1-9
$charcters = range( 'A', 'Z');
$numbers = range(1,9);
$objPHPExcel = new PHPExcel();
// Set properties
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
    ->setLastModifiedBy("Maarten Balliauw")
    ->setTitle("Office 2007 XLSX Test Document")
    ->setSubject("Office 2007 XLSX Test Document")
    ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
    ->setKeywords("office 2007 openxml php")
    ->setCategory("Test result file");
// Create a first sheet, representing sales data


$reportTitle = 'ORDER WISE STOCK ORDER FORECAST';

$objPHPExcel->getActiveSheet()->mergeCells('A2:U2');
$objPHPExcel->setActiveSheetIndex(0)
//$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Something');
    ->setCellValue('A2', $reportTitle)
    ->setCellValue('A4', 'ORDER NO')
    ->setCellValue('B4', 'Order Type')
    ->setCellValue('C4', 'Order Status')
    ->setCellValue('D4', 'Order Created Date')
    ->setCellValue('E4', 'Sales Order')
    ->setCellValue('F4', 'Production Location')
    ->setCellValue('G4', 'Main Category')
    ->setCellValue('H4', 'Sub Category')
    ->setCellValue('I4', 'Code')
    ->setCellValue('J4', 'Item')
    ->setCellValue('K4', 'UOM')
    ->setCellValue('L4', 'PRI')
    ->setCellValue('M4', 'PRN')
    ->setCellValue('N4', 'PURCHASED')
    ->setCellValue('O4', 'MRN')
    ->setCellValue('P4', 'ISSUED')
    ->setCellValue('Q4', 'RETURNED')
    ->setCellValue('R4', 'GP OUT(From HO)')
    ->setCellValue('S4', 'GP IN(TO HO)')
    ->setCellValue('T4', 'BAL MRN')
    ->setCellValue('U4', 'BAL TO ISSUE');
PHPExcel_Shared_Font::setAutoSizeMethod(PHPExcel_Shared_Font::AUTOSIZE_METHOD_EXACT);
$objPHPExcel->getActiveSheet()->getStyle('A2')->getFont()->setBold(true);

//Set Bold Titles
for($i=0;$i<24;$i++) {
    //echo $i.'-'.$charcters[$i].'<br>';
    $header =$charcters[$i].'4';
    $objPHPExcel->getActiveSheet()->getStyle($header)->getFont()->setBold(true);
}
//add color to row title
$objPHPExcel->getActiveSheet()
    ->getStyle('A2:O2')
    ->applyFromArray(
        array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'cccccc')
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'font' => array(
                'bold' => false,
                'color' => array('rgb' => '000000'),
                'size' => 12,
            )
        )
    );

$sql = get_forecastDetails($fromDate,$toDate,$head_office_loc,$companyId,$order_number,$main_category,$sub_category,$item,$pending_mrn,$so_wise,$company);

$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
    $count=5;
    while($row = $result->fetch_assoc()) {

        $MRN_Qty 	=  round($row['MRN_QTY_TODATE'],4);

        $bal_to_MRN = (round(($row['REQUIRED_TODATE'] - $MRN_Qty), 4));
        if($bal_to_MRN > 0) {

        }else {
            $bal_to_MRN = 0;
        }

        if($balance_issues == 1) {
            if ($row['type'] == 2) {
                $bal_to_issue_for_all = round((($MRN_Qty) - ($row['ISSUE_QTY_ALL_HO'] + MAX($row['ISSUE_QTY_ALL_NON_HO'], ($row['GP_FROM_HO_ALL'])))), 4);
            } else {
                $bal_to_issue_for_all = round((($row['REQUIRED_TODATE']) - ($row['ISSUE_QTY_ALL_HO'] + MAX($row['ISSUE_QTY_ALL_NON_HO'], ($row['GP_FROM_HO_ALL'])))), 4);
            }
            if ($bal_to_issue_for_all < 0) {
                $bal_to_issue_for_all = 0;
            }
        }

//

        if($row['ORDER_STATUS'] ==1){
            $order_status ='Approved';
        }else if($row['ORDER_STATUS']==0) {
            $order_status ='Rejected';

        }else if($row['ORDER_STATUS']== -10) {
            $order_status ='Completed';

        }else if($row['ORDER_STATUS']== -2){
            $order_status ='Cancel';
        }else if($row['ORDER_STATUS']== -1) {
            $order_status ='Revised';
        }else{
            $order_status ='Pending';
        }

        if($row['ORDER_TYPE'] == 1) {
            $order_type= 'Dummy';
        }else if($row['ORDER_TYPE'] == 2){
            $order_type= 'Acctual';
        }else {
            $order_type= 'Other';
        }

        $objPHPExcel->getActiveSheet()->SetCellValue('A'.$count, $row['ORDER_NO']);
        $objPHPExcel->getActiveSheet()->SetCellValue('B'.$count,$order_type);
        $objPHPExcel->getActiveSheet()->SetCellValue('C'.$count,$order_status);
        $objPHPExcel->getActiveSheet()->SetCellValue('D'.$count,$row['CREATE_DATE']);
        if($so_wise !='') {
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $count, $row['SO']);
        }
        $objPHPExcel->getActiveSheet()->SetCellValue('F'.$count,$row['location']);
        $objPHPExcel->getActiveSheet()->SetCellValue('G'.$count,$row['mainCatName']);
        $objPHPExcel->getActiveSheet()->SetCellValue('H'.$count,$row['subCatName']);
        $objPHPExcel->getActiveSheet()->SetCellValue('I'.$count,$row['strCode']);
        $objPHPExcel->getActiveSheet()->SetCellValue('J'.$count,$row['itemName']);
        $objPHPExcel->getActiveSheet()->SetCellValue('K'.$count,$row['UOM']);
        $objPHPExcel->getActiveSheet()->SetCellValue('L'.$count,round($row['REQUIRED_TODATE'],4));
        $objPHPExcel->getActiveSheet()->SetCellValue('M'.$count,round($row['PRN_QTY_TODATE'],4));
        $objPHPExcel->getActiveSheet()->SetCellValue('N'.$count,round($row['PO_QTY_TODATE'],4));
        $objPHPExcel->getActiveSheet()->SetCellValue('O'.$count,$MRN_Qty);
        $objPHPExcel->getActiveSheet()->SetCellValue('P'.$count,round($row['ISSUE_QTY_TODATE'],4));
        $objPHPExcel->getActiveSheet()->SetCellValue('Q'.$count,round($row['RET_TO_STORE_QTY_TODATE'],4));
        $objPHPExcel->getActiveSheet()->SetCellValue('R'.$count,round($row['GP_FROM_HO_TODATE'],4));
        $objPHPExcel->getActiveSheet()->SetCellValue('S'.$count,round($row['GP_IN_QTY_TO_HO_TODATE'],4));
        if($balance_mrn == 1) {
            $objPHPExcel->getActiveSheet()->SetCellValue('T' . $count, $bal_to_MRN);
        }
        if($balance_issues == 1) {
            $objPHPExcel->getActiveSheet()->SetCellValue('U' . $count, $bal_to_issue_for_all);
        }

        $count++;

    }

}
/*
 * pending mrn
 * 2017/10/31
 */


if($pending_mrn==1)//include pending MRn to thr report.
{
    $count= $count+1;
    $pending_heading = 'A'.$count.':U'.$count;

    //$objPHPExcel->getActiveSheet()->setCellValue('A', 'Something');
    $objPHPExcel->getActiveSheet()->SetCellValue('A' . $count, 'PENDING MRN');
    $bold= 'A'.$count;
    $objPHPExcel->getActiveSheet()->getStyle($bold)->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->mergeCells($pending_heading);
    $sql6 = get_pending_mrn_details($companyId,$head_office_loc,$locationId,$company,$main_category,$sub_category,$item,$order_number,$so_wise);

    //$result6 = $db->RunQuery($sql6);
    $result = $conn->query($sql6);

    //$result = $conn->query($sql);
    if ($result->num_rows > 0) {
        $count_2 = $count+1;
        while($row = $result->fetch_assoc()) {
//

            //$MRN_Qty = round($row['MRN_QTY_TODATE'], 4);
            $bal_to_MRN = round(($row['PRI_QTY'] - $row['MRN_QTY']),4);
            //$bal_to_MRN = round(($row['PRI_QTY'] - $row['MRN_QTY']), 4);
            if($bal_to_MRN < 0){
                $bal_to_MRN = 0;
            }
            $bal_to_issue_for_all = (($row['MRN_QTY']) - ($row['ISSUE_QTY']));
            if($bal_to_issue_for_all < 0){
                $bal_to_issue_for_all = 0;
            }

            if ($row['ORDER_STATUS'] == 1) {
                $order_status = 'Approved';
            } else if ($row['ORDER_STATUS'] == 0) {
                $order_status = 'Rejected';

            } else if ($row['ORDER_STATUS'] == -10) {
                $order_status = 'Completed';

            } else if ($row['ORDER_STATUS'] == -2) {
                $order_status = 'Cancel';
            } else if ($row['ORDER_STATUS'] == -1) {
                $order_status = 'Revised';
            } else {
                $order_status = 'Pending';
            }

            if ($row['ORDER_TYPE'] == 1) {
                $order_type = 'Dummy';
            } else if ($row['ORDER_TYPE'] == 2) {
                $order_type = 'Acctual';
            } else {
                $order_type = 'Other';
            }

            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $count_2, $row['ORDER_NO']);
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $count_2, $order_type);
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $count_2, $order_status);
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $count_2, $row['CREATE_DATE']);
            if ($so_wise != '') {
                $objPHPExcel->getActiveSheet()->SetCellValue('E' . $count_2, $row['SALES_ORDER']);
            }
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $count_2, $row['location']);
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $count_2, $row['mainCat']);
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $count_2, $row['subCat']);
            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $count_2, $row['strCode']);
            $objPHPExcel->getActiveSheet()->SetCellValue('J' . $count_2, $row['item']);
            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $count_2, $row['unit']);
            $objPHPExcel->getActiveSheet()->SetCellValue('L' . $count_2, round($row['PRI_QTY'], 4));
            $objPHPExcel->getActiveSheet()->SetCellValue('M' . $count_2, round($row['PRN_QTY'], 4));
            $objPHPExcel->getActiveSheet()->SetCellValue('N' . $count_2, round($row['PO_QTY'], 4));
            $objPHPExcel->getActiveSheet()->SetCellValue('O' . $count_2, round($row['MRN_QTY'],4));
            $objPHPExcel->getActiveSheet()->SetCellValue('P' . $count_2, round($row['ISSUE_QTY'], 4));
            $objPHPExcel->getActiveSheet()->SetCellValue('Q' . $count_2, round($row['RET_TO_STORES'], 4));
            $objPHPExcel->getActiveSheet()->SetCellValue('R' . $count_2, round($row['GP_IN'], 4));
            $objPHPExcel->getActiveSheet()->SetCellValue('S' . $count_2, round($row['GP_OUT'], 4));
            if ($balance_mrn == 1) {
                $objPHPExcel->getActiveSheet()->SetCellValue('T' . $count_2, $bal_to_MRN);
            }
            if ($balance_issues == 1) {

                $objPHPExcel->getActiveSheet()->SetCellValue('U' . $count_2, $bal_to_issue_for_all);
            }

            $count_2++;
        }
    }
}


// Rename 5th sheet
$objPHPExcel->getActiveSheet()->setTitle('STOCK ORDER FORCAST');
// Redirect output to a client’s web browser (Excel5)

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
ob_end_clean();
$date= 'STOCK_ORDER_FORCAST.xlsx';
header('Content-type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename="'.$date.'"');
//header("Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
//header("Content-Disposition: attachment;filename=\"filename.xlsx\"");
//header("Cache-Control: max-age=0");
$objWriter->save('php://output');
exit;



function get_ho_loc($companyId,$conn){
    $sql="SELECT
			mst_locations.intId
			FROM `mst_locations`
			WHERE
			mst_locations.COMPANY_MAIN_STORES_LOCATION = 1 AND
			mst_locations.intCompanyId = '$companyId' ";

    $result = $conn->query($sql);

    while($row = $result->fetch_assoc()) {
        $intId=(int)$row['intId'];
    }
    return $intId;

}
/*
 * pending mrn
 * 2017/10/31
 */
function get_pending_mrn_details($companyId,$head_office_loc,$locationId,$company,$maincategory,$subcategory,$item,$order_number,$so_wise)
{

    $sql = "SELECT
TB_ALL.intCompanyId,
TB_ALL.intItemId,
TB_ALL.MRN_QTY,
TB_ALL.item,
TB_ALL.strCode,
TB_ALL.re_order_policy,
TB_ALL.mainCat,
TB_ALL.mainCat_id,
TB_ALL.subCat,
TB_ALL.subCat_id,
TB_ALL.unit,
TB_ALL.GP_IN,
TB_ALL.GP_OUT,
TB_ALL.ISSUE_QTY,
TB_ALL.PO_QTY,
TB_ALL.PRI_QTY,
TB_ALL.PRN_QTY,
TB_ALL.RET_TO_STORES,
TB_ALL.ORDER_NO,
TB_ALL.ORDER_TYPE,
TB_ALL.ORDER_STATUS,
TB_ALL.CREATE_DATE,
TB_ALL.SALES_ORDER,
TB_ALL.location AS location
FROM 
(SELECT
TB2.intCompanyId,
TB2.intItemId,
TB2.item,
TB2.strCode,
TB2.re_order_policy,
TB2.mainCat,
TB2.mainCat_id,
TB2.subCat,
TB2.subCat_id,
TB2.unit,
SUM(TB2.MRN_QTY) AS MRN_QTY,
SUM(TB2.ISSUE_QTY) AS ISSUE_QTY,
SUM(TB2.PRI_QTY) AS PRI_QTY,
SUM(TB2.RET_TO_STORES) AS RET_TO_STORES,
SUM(TB2.GP_OUT) AS GP_OUT,
SUM(TB2.GP_IN) AS GP_IN,
SUM(TB2.PRN_QTY) AS PRN_QTY,
SUM(TB2.PO_QTY) AS PO_QTY,
TB2.ORDER_NO,
TB2.ORDER_TYPE,
TB2.ORDER_STATUS,
TB2.CREATE_DATE,
TB2.SALES_ORDER,
TB2.location AS location
FROM(select 
TB1.intOrderNo  AS ORDER_NO,
TB1.intOrderYear,
TB1.intItemId,
TB1.intCompanyId,
TB1.item,
TB1.strCode,
TB1.re_order_policy,
TB1.mainCat,
TB1.mainCat_id,
TB1.subCat,
TB1.subCat_id,
TB1.unit,
SUM(TB1.mrn_qty) AS MRN_QTY,
SUM(TB1.issue_qty)as ISSUE_QTY,
SUM(TB1.PRI_QTY) as PRI_QTY,
SUM(TB1.ret_to_stores) AS RET_TO_STORES,
SUM(TB1.GP_Out) AS GP_OUT,
SUM(TB1.GP_IN_QTY) AS GP_IN,



( SELECT SUM(trn_po_prn_details.PRN_QTY) as prn_qty 
FROM trn_po_prn_details 
WHERE trn_po_prn_details.ORDER_NO = TB1.intOrderNo AND 
trn_po_prn_details.ORDER_YEAR = TB1.intOrderYear AND 
trn_po_prn_details.ITEM = TB1.intItemId) AS PRN_QTY,

(SELECT SUM(trn_po_prn_details.PURCHASED_QTY) as po_qty 
FROM trn_po_prn_details WHERE 
trn_po_prn_details.ORDER_NO = TB1.intOrderNo AND 
trn_po_prn_details.ORDER_YEAR = TB1.intOrderYear AND 
trn_po_prn_details.ITEM = TB1.intItemId ) AS PO_QTY,
TB1.ORDER_TYPE,
TB1.ORDER_STATUS,
TB1.CREATE_DATE,
TB1.SALES_ORDER,
TB1.location AS location

FROM(

select  

ware_mrndetails.intOrderNo,  

ware_mrndetails.intOrderYear,
ware_mrndetails.intItemId,
mst_locations.intCompanyId,
mst_maincategory.intId AS mainCat_id,
mst_maincategory.strName AS mainCat,
mst_subcategory.intId AS subCat_id,
mst_subcategory.strName As subCat,
mst_item.strName AS item,
mst_item.strCode,
mst_item.intReorderPolicy as re_order_policy,
mst_units.strName AS unit,
	
sum(ware_mrndetails.dblQty) as mrn_qty, 
sum(ware_mrndetails.dblIssudQty) as issue_qty, 

(select SUM(trn_po_prn_details_sales_order.REQUIRED) 
from trn_po_prn_details_sales_order
WHERE 
trn_po_prn_details_sales_order.ORDER_NO = ware_mrndetails.intOrderNo AND 
trn_po_prn_details_sales_order.ORDER_YEAR = ware_mrndetails.intOrderYear AND
trn_po_prn_details_sales_order.SALES_ORDER = ware_mrndetails.strStyleNo AND  
trn_po_prn_details_sales_order.ITEM = ware_mrndetails.intItemId ) AS PRI_QTY, 


(select SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'RETURNED_TO_STORES',
 trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS RET_TO_STORE_QTY 
FROM trn_po_prn_details_sales_order_stock_transactions 
WHERE trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = ware_mrndetails.intOrderNo 
AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = ware_mrndetails.intOrderYear 
AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = ware_mrndetails.strStyleNo
AND trn_po_prn_details_sales_order_stock_transactions.ITEM = ware_mrndetails.intItemId 
AND trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID = '$locationId' ) as ret_to_stores,

 (select SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'GATE_PASS',
 trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS GP_QTY 
FROM trn_po_prn_details_sales_order_stock_transactions 
WHERE trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = ware_mrndetails.intOrderNo AND 
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = ware_mrndetails.intOrderYear AND 
 trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = ware_mrndetails.strStyleNo AND 
trn_po_prn_details_sales_order_stock_transactions.ITEM = ware_mrndetails.intItemId AND 
trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID = '$locationId' )as GP_Out, 

(select SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'GATE_PASS_IN', 
trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS GP_IN_QTY 
FROM trn_po_prn_details_sales_order_stock_transactions 
WHERE trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = ware_mrndetails.intOrderNo AND 
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = ware_mrndetails.intOrderYear AND 
 trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = ware_mrndetails.strStyleNo AND
trn_po_prn_details_sales_order_stock_transactions.ITEM = ware_mrndetails.intItemId AND 
trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID = '$locationId' )GP_IN_QTY,
trn_orderheader.PO_TYPE AS ORDER_TYPE,
trn_orderheader.intStatus AS ORDER_STATUS,
trn_orderheader.dtDate AS CREATE_DATE,
(
trn_orderdetails.strSalesOrderNo
) AS SALES_ORDER,
mst_locations.strName AS location

from ware_mrndetails 
INNER JOIN ware_mrnheader on ware_mrndetails.intMrnNo = ware_mrnheader.intMrnNo AND 
ware_mrndetails.intMrnYear= ware_mrnheader.intMrnYear 
INNER JOIN mst_locations on ware_mrnheader.intCompanyId = mst_locations.intId
INNER JOIN mst_item ON ware_mrndetails.intItemId = mst_item.intId 
INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId 
INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId 
INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
-- INNER JOIN trn_orderheader ON ware_mrndetails.intOrderNo = trn_orderheader.intOrderNo
-- AND ware_mrndetails.intOrderYear = trn_orderheader.intOrderYear
INNER JOIN trn_orderdetails ON ware_mrndetails.intOrderNo = trn_orderdetails.intOrderNo
AND ware_mrndetails.intOrderYear = trn_orderdetails.intOrderYear
AND ware_mrndetails.strStyleNo = trn_orderdetails.strSalesOrderNo
INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo
AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
WHERE 
((ware_mrnheader.intStatus <= (ware_mrnheader.intApproveLevels+1) && ware_mrnheader.intStatus >1) || ware_mrnheader.intStatus=0)
AND     /*change 2017/09/11*/
mst_locations.intCompanyId IN ($company) ";

    if($so_wise == '1') {
        $sql .=" GROUP BY 
ware_mrndetails.intOrderNo,
ware_mrndetails.intOrderYear,
ware_mrndetails.strStyleNo,
ware_mrndetails.intItemId,
trn_orderdetails.strSalesOrderNo
) as  TB1
GROUP BY 
TB1.intOrderNo,
TB1.intOrderYear,
TB1.intItemId,
TB1.SALES_ORDER
)AS TB2
GROUP BY 
TB2.ORDER_NO,
TB2.intOrderYear,
TB2.intItemId,
TB2.SALES_ORDER
) 
AS TB_ALL
where
1=1 ";
    }else {
        $sql .=" GROUP BY 
ware_mrndetails.intOrderNo,
ware_mrndetails.intOrderYear,
ware_mrndetails.strStyleNo,
ware_mrndetails.intItemId
) as  TB1
GROUP BY 
TB1.intOrderNo,
TB1.intOrderYear,
TB1.intItemId
)AS TB2

GROUP BY 
TB2.ORDER_NO,
TB2.intOrderYear,
TB2.intItemId
) 

AS TB_ALL

where
1=1 ";
    }

    $sql .=  "  AND TB_ALL.intCompanyId IN ($company)";
   // $sql .=  "  AND TB_ALL.intCompanyId = 2";

    //TB_ALL.ORDER_NO
    if($order_number !='null'){
        $sql .= "AND TB_ALL.ORDER_NO  IN ($order_number)";
    }

    if($maincategory !='null')
    {
        $sql .=  "  AND TB_ALL.mainCat_id IN ($maincategory)";
    }

    if($subcategory !='null')
    {
        $sql .=  "  AND TB_ALL.subCat_id IN ($subcategory)";
    }
    if($item !='')
    {
        $sql .=  "  AND TB_ALL.intItemId IN ($item)";
    }

    return $sql;
}

function get_forecastDetails($fromDate,$toDate,$head_office_loc,$companyId,$order_number,$main_category,$sub_category,$item,$pending_mrn,$so_wise,$company){
//var_dump($sub_category); die;
//echo $sub_category .'<br>'.$main_category.'<br>'.$item.'<br>'.$order_number ; die;

    $sql = "SELECT 
TB_ALL .type,
TB_ALL .date as DATES_TODATE, 
TB_ALL .date as DATES_OTHER, 
TB_ALL .ORDER_COMPANY,
TB_ALL .location AS location,
TB_ALL .mainCatId,
TB_ALL .subCatId, 
TB_ALL .ITEM, 
TB_ALL .re_order_policy,
TB_ALL .mainCatName, 
TB_ALL .subCatName,
TB_ALL .strCode,
TB_ALL .itemName, 
TB_ALL .otherOrders AS TODATE_ORDERS,
TB_ALL .REQUIRED AS REQUIRED_TODATE,
TB_ALL .PRN_QTY AS PRN_QTY_TODATE,
TB_ALL .PO_QTY AS PO_QTY_TODATE, 
(
ifnull(TB_ALL.MRN_QTY,0) + ifnull(TB_ALL.MRN_QTY_OTHER_LOC,0)
) AS MRN_QTY_TODATE,

(
ifnull(TB_ALL.ISSUE_QTY,0) + ifnull(TB_ALL.ISSUE_QTY_OTHER_LOC,0) 
) AS ISSUE_QTY_TODATE,

(
ifnull(TB_ALL.RET_TO_STORE_QTY,0) 
+ ifnull(TB_ALL.RET_TO_STORE_QTY_OTHER_LOC,0) 
) AS RET_TO_STORE_QTY_TODATE,

TB_ALL.GP_QTY AS GP_FROM_HO_TODATE,
TB_ALL.GP_IN_QTY AS GP_IN_QTY_TO_HO_TODATE,
TB_ALL.UOM,
TB_ALL.otherOrders AS OTHER_ORDERS, 

(
ifnull(TB_ALL.ISSUE_QTY,0) 
) AS ISSUE_QTY_ALL_HO,

(
ifnull(TB_ALL.GP_QTY,0)  
) AS GP_FROM_HO_ALL,

(
ifnull(TB_ALL.ISSUE_QTY_OTHER_LOC,0)  
) AS ISSUE_QTY_ALL_NON_HO,

TB_ALL.PO_QTY AS PO_QTY_OTHER,
TB_ALL.ORDER_NO,
TB_ALL.ORDER_STATUS,
TB_ALL.ORDER_TYPE,
TB_ALL.CREATE_DATE,
TB_ALL.SO

from 
/*tb_all*/	
(select tb.type,
tb.ORDER_COMPANY, 
tb.location AS location, 
tb.date,
tb.ITEM, 
GROUP_CONCAT(DISTINCT tb.ORDER_NO,'/',tb.ORDER_YEAR) as otherOrders,
 
SUM(tb.REQUIRED) as REQUIRED,
tb.TRANSACTION_LOCATION, 
sum(tb.PO_QTY) as PO_QTY, 
sum(tb.PRN_QTY) as PRN_QTY, 
tb.mainCatId,
tb.subCatId,
tb.mainCatName,
tb.subCatName, 
tb.strCode, 
tb.itemName, 
tb.re_order_policy,
tb.UOM, 
SUM(tb.MRN_QTY) as MRN_QTY, 
SUM(tb.MRN_EXTRA_QTY) as MRN_EXTRA_QTY, 
SUM(tb.ISSUE_QTY) as ISSUE_QTY, 
SUM(tb.GP_QTY) as GP_QTY, 
SUM(tb.GP_IN_QTY) as GP_IN_QTY, 
SUM(tb.RET_TO_STORE_QTY) as RET_TO_STORE_QTY, 
SUM(tb.MRN_QTY_OTHER_LOC) as MRN_QTY_OTHER_LOC, 
SUM(tb.MRN_EXTRA_QTY_OTHER_LOC) as MRN_EXTRA_QTY_OTHER_LOC, 
SUM(tb.ISSUE_QTY_OTHER_LOC) as ISSUE_QTY_OTHER_LOC, 
SUM(tb.GP_QTY_OTHER_LOC) as GP_QTY_OTHER_LOC, 
SUM(tb.GP_IN_QTY_OTHER_LOC) as GP_IN_QTY_OTHER_LOC, 
SUM(tb.RET_TO_STORE_QTY_OTHER_LOC) as RET_TO_STORE_QTY_OTHER_LOC ,
tb.ORDER_NO,
tb.ORDER_STATUS,
tb.ORDER_TYPE,
tb.CREATE_DATE,
tb.SO 
/*tb*/	
from( 
SELECT 	TB3.type, 
TB3.ORDER_COMPANY, 
TB3.location AS location, 
TB3.date, 
TB3.ITEM, 
TB3.ORDER_NO, 
TB3.ORDER_YEAR, 
-- SUM(TB3.REQUIRED) AS REQUIRED,

(
SELECT
sum(
trn_po_prn_details.REQUIRED
)
FROM
trn_po_prn_details
WHERE
trn_po_prn_details.ORDER_NO = TB3.ORDER_NO
AND trn_po_prn_details.ORDER_YEAR = TB3.ORDER_YEAR
AND trn_po_prn_details.ITEM = TB3.ITEM
) AS REQUIRED,
(
SELECT
sum(
trn_po_prn_details.PURCHASED_QTY) 
FROM trn_po_prn_details 
WHERE trn_po_prn_details.ORDER_NO = TB3.ORDER_NO 
AND trn_po_prn_details.ORDER_YEAR = TB3.ORDER_YEAR 
AND trn_po_prn_details.ITEM = TB3.ITEM ) AS PO_QTY, 

(SELECT SUM(trn_po_prn_details.PRN_QTY)
FROM trn_po_prn_details 
WHERE trn_po_prn_details.ORDER_NO = TB3.ORDER_NO AND
 trn_po_prn_details.ORDER_YEAR = TB3.ORDER_YEAR AND 
trn_po_prn_details.ITEM = TB3.ITEM ) AS PRN_QTY, 

TB3.TRANSACTION_LOCATION, 
 
TB3.mainCatId,
TB3.subCatId,
TB3.mainCatName, 
TB3.subCatName, 
TB3.strCode, 
TB3.itemName, 
TB3.re_order_policy,
TB3.UOM, 
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.MRN_QTY,0)) as MRN_QTY, 
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.MRN_EXTRA_QTY,0)) as MRN_EXTRA_QTY, 
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.ISSUE_QTY,0)) as ISSUE_QTY, 
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.GP_QTY,0)) as GP_QTY, 
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.GP_IN_QTY,0)) as GP_IN_QTY, 
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.RET_TO_STORE_QTY,0)) as RET_TO_STORE_QTY, 
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.MRN_QTY,0)) as MRN_QTY_OTHER_LOC, 
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.MRN_EXTRA_QTY,0)) as MRN_EXTRA_QTY_OTHER_LOC, 
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.ISSUE_QTY,0)) as ISSUE_QTY_OTHER_LOC, 
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.GP_QTY,0)) as GP_QTY_OTHER_LOC, 
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.GP_IN_QTY,0)) as GP_IN_QTY_OTHER_LOC, 
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.RET_TO_STORE_QTY,0)) as RET_TO_STORE_QTY_OTHER_LOC,
TB3.ORDER_STATUS,
TB3.ORDER_TYPE,
TB3.CREATE_DATE,
TB3.SO
FROM 
/*tb3*/	(SELECT TB2.ORDER_COMPANY, 
TB2.ORDER_NO, 
TB2.ORDER_YEAR, 
TB2.location, 
TB2.ITEM,
TB2.SALES_ORDER, 
TB2.type, 
CONCAT(trn_orderdetails.strSalesOrderNo,'/',trn_orderdetails.strCombo,'/',trn_orderdetails.strPrintName) AS SALES_ORDER_String, 
'' AS date,
mst_maincategory.intId as mainCatId,
mst_subcategory.intId AS subCatId,  
mst_maincategory.strName AS mainCatName, 
mst_subcategory.strName AS subCatName, 
mst_item.strCode, 
mst_item.intId, 
mst_item.strName AS itemName, 
mst_item.intReorderPolicy AS re_order_policy,
mst_units.strName AS UOM, 
trn_po_prn_details_sales_order.REQUIRED, 
trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID AS TRANSACTION_LOCATION, 



SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'MRN', 
trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS MRN_QTY, 
SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'MRN_EXTRA', 
trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS MRN_EXTRA_QTY, 
SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'ISSUE', 
trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS ISSUE_QTY, 
SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'GATE_PASS', 
trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS GP_QTY, 
SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'GATE_PASS_IN',
trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS GP_IN_QTY, 
SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'RETURNED_TO_STORES', 
trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS RET_TO_STORE_QTY,
TB2.ORDER_STATUS,
TB2.ORDER_TYPE,
TB2.CREATE_DATE,
(
trn_orderdetails.strSalesOrderNo
) AS SO
FROM 
/*TB2*/( SELECT * FROM 
/*tb1*/	( SELECT 
trn_po_prn_details_sales_order_stock_transactions.ORDER_NO, 
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR, 
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER, 
trn_po_prn_details_sales_order_stock_transactions.ITEM, 
mst_locations.intCompanyId AS ORDER_COMPANY, 
mst_locations.strName AS location, 
( SELECT GROUP_CONCAT(mst_locations.intCompanyId) 
FROM ware_fabricreceivedheader 
INNER JOIN mst_locations 
ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId 
WHERE 
ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO 
AND ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR 
)AS FABRIC_IN_COMPANIES, 
1 AS type,
trn_orderheader.intStatus AS ORDER_STATUS,
trn_orderheader.PO_TYPE AS ORDER_TYPE,
trn_orderheader.dtDate AS CREATE_DATE
FROM trn_po_prn_details_sales_order_stock_transactions 
INNER JOIN trn_po_prn_details_sales_order 
ON 
trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = trn_po_prn_details_sales_order.ORDER_NO 
AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR 
AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER 
AND trn_po_prn_details_sales_order_stock_transactions.ITEM = trn_po_prn_details_sales_order.ITEM 
INNER JOIN trn_orderheader 
ON trn_orderheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO AND 
trn_orderheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR 
AND trn_orderheader.intStatus = 1
INNER JOIN mst_locations 
ON mst_locations.intId = trn_orderheader.intLocationId 

GROUP BY trn_po_prn_details_sales_order_stock_transactions.ORDER_NO, 
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR, 
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER, 
trn_po_prn_details_sales_order_stock_transactions.ITEM 
HAVING FIND_IN_SET('$company', FABRIC_IN_COMPANIES) OR ORDER_COMPANY = '$company' 

UNION ALL 

SELECT 
trn_po_prn_details_sales_order_stock_transactions.ORDER_NO, 
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR, 
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER, 
trn_po_prn_details_sales_order_stock_transactions.ITEM, 
mst_locations.intCompanyId AS ORDER_COMPANY, 
mst_locations.strName AS location, 
( SELECT GROUP_CONCAT( DISTINCT mst_locations.intCompanyId ) 
FROM ware_fabricreceivedheader 
INNER JOIN mst_locations 
ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId 
WHERE 
ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO AND 
ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR 
) AS FABRIC_IN_COMPANIES, 
2 AS type,
trn_orderheader.intStatus AS ORDER_STATUS,
trn_orderheader.PO_TYPE AS ORDER_TYPE,
trn_orderheader.dtDate AS CREATE_DATE
FROM trn_po_prn_details_sales_order_stock_transactions 

LEFT JOIN trn_po_prn_details_sales_order 
ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = trn_po_prn_details_sales_order.ORDER_NO AND 
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR AND 
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER AND 
trn_po_prn_details_sales_order_stock_transactions.ITEM = trn_po_prn_details_sales_order.ITEM 
INNER JOIN trn_orderheader ON 
trn_orderheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO AND 
trn_orderheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR 
AND trn_orderheader.intStatus = 1
INNER JOIN mst_locations ON mst_locations.intId = trn_orderheader.intLocationId 

WHERE trn_po_prn_details_sales_order.ORDER_NO IS NULL 

GROUP BY 
trn_po_prn_details_sales_order_stock_transactions.ORDER_NO, 
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR, 
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER, 
trn_po_prn_details_sales_order_stock_transactions.ITEM 
HAVING ( FIND_IN_SET('$company', FABRIC_IN_COMPANIES) OR ORDER_COMPANY = '$company' ) 


UNION ALL 

SELECT 
trn_po_prn_details_sales_order.ORDER_NO, 
trn_po_prn_details_sales_order.ORDER_YEAR, 
trn_po_prn_details_sales_order.SALES_ORDER, 
trn_po_prn_details_sales_order.ITEM, 
mst_locations.intCompanyId AS ORDER_COMPANY, 
mst_locations.strName AS location, 
( SELECT GROUP_CONCAT( DISTINCT mst_locations.intCompanyId ) 
FROM ware_fabricreceivedheader 
INNER JOIN mst_locations 
ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId 
WHERE ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order.ORDER_NO 
AND ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order.ORDER_YEAR 
) AS FABRIC_IN_COMPANIES, 
1 AS type,
trn_orderheader.intStatus AS ORDER_STATUS,
trn_orderheader.PO_TYPE AS ORDER_TYPE,
trn_orderheader.dtDate AS CREATE_DATE
FROM trn_po_prn_details_sales_order_stock_transactions

RIGHT JOIN 
trn_po_prn_details_sales_order 
ON 
trn_po_prn_details_sales_order_stock_transactions.ORDER_NO    = trn_po_prn_details_sales_order.ORDER_NO AND 
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR  = trn_po_prn_details_sales_order.ORDER_YEAR AND 
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER AND 
trn_po_prn_details_sales_order_stock_transactions.ITEM        = trn_po_prn_details_sales_order.ITEM 
INNER JOIN trn_orderheader 
ON trn_orderheader.intOrderNo = trn_po_prn_details_sales_order.ORDER_NO AND 
trn_orderheader.intOrderYear  = trn_po_prn_details_sales_order.ORDER_YEAR 
AND trn_orderheader.intStatus = 1
INNER JOIN mst_locations 
ON mst_locations.intId = trn_orderheader.intLocationId 

WHERE trn_po_prn_details_sales_order_stock_transactions.ORDER_NO IS NULL 

GROUP BY 
trn_po_prn_details_sales_order.ORDER_NO, 
trn_po_prn_details_sales_order.ORDER_YEAR, 
trn_po_prn_details_sales_order.SALES_ORDER, 
trn_po_prn_details_sales_order.ITEM 

HAVING ( FIND_IN_SET('$company', FABRIC_IN_COMPANIES) OR ORDER_COMPANY = '$company' ) ) AS TB1 
GROUP BY 
TB1.ORDER_NO, 
TB1.ORDER_YEAR, 
TB1.SALES_ORDER,
TB1.ITEM ) AS TB2 


LEFT JOIN 
trn_po_prn_details_sales_order_stock_transactions 
ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = TB2.ORDER_NO AND 
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = TB2.ORDER_YEAR AND 
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = TB2.SALES_ORDER AND 
trn_po_prn_details_sales_order_stock_transactions.ITEM = TB2.ITEM 
INNER JOIN trn_orderheader 
ON trn_orderheader.intOrderNo = TB2.ORDER_NO AND 
trn_orderheader.intOrderYear = TB2.ORDER_YEAR AND
trn_orderheader.intStatus = 1
LEFT JOIN trn_po_prn_details_sales_order ON trn_po_prn_details_sales_order.ORDER_NO = TB2.ORDER_NO
AND trn_po_prn_details_sales_order.ORDER_YEAR = TB2.ORDER_YEAR
AND trn_po_prn_details_sales_order.SALES_ORDER = TB2.SALES_ORDER
AND trn_po_prn_details_sales_order.ITEM = TB2.ITEM
INNER JOIN trn_orderdetails 
ON TB2.ORDER_NO     = trn_orderdetails.intOrderNo 
AND TB2.ORDER_YEAR  = trn_orderdetails.intOrderYear 
AND TB2.SALES_ORDER = trn_orderdetails.intSalesOrderId 
INNER JOIN mst_item 
ON TB2.ITEM = mst_item.intId 
INNER JOIN mst_units 
ON mst_item.intUOM = mst_units.intId 
INNER JOIN mst_maincategory 
ON mst_item.intMainCategory = mst_maincategory.intId 
INNER JOIN mst_subcategory 
ON mst_item.intSubCategory = mst_subcategory.intId 
GROUP BY 
TB2.ORDER_NO, 
TB2.ORDER_YEAR, 
TB2.SALES_ORDER, 
TB2.ITEM,
trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID
) AS TB3 ";

    if($so_wise == '1') {

        $sql .=" GROUP BY
TB3.ORDER_NO,
TB3.ORDER_YEAR,
TB3.ITEM,
TB3.SO) 
as 
tb 
GROUP BY 
tb.ORDER_NO,
tb.ORDER_YEAR,
tb.ITEM,
tb.SO
 ) as TB_ALL 
WHERE
1 = 1 ";

    }else {
        $sql .= " GROUP BY
TB3.ORDER_NO,
TB3.ORDER_YEAR,
TB3.ITEM ) 
as 
tb 
GROUP BY 
tb.ORDER_NO,
tb.ORDER_YEAR,
tb.ITEM
 ) as TB_ALL 
WHERE
1 = 1 ";
    }



    // $sql .=  " AND TB_ALL .ORDER_COMPANY IN (2)";
   $sql .=  " AND TB_ALL .ORDER_COMPANY  = $company ";
    if($order_number !='null'){
        $sql .= "AND TB_ALL.ORDER_NO IN ($order_number)";
    }
    if($main_category !='null') {

        $sql .= " AND TB_ALL.mainCatId IN ($main_category)";

    }

    if($sub_category !='null') {
        $sql .= "AND TB_ALL.subCatId IN ($sub_category)";
    }

    if($item !='') {

        $sql .= "AND TB_ALL.ITEM IN ($item)";

    }
    $sql .=   " ORDER BY TB_ALL.date ASC";

    return $sql;
}

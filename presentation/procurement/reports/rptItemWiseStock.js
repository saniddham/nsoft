var basePath	="presentation/procurement/reports/";
$(document).ready(function() {
    $("#frmSamplePlaceOrderReport #tblMainGrid #butDetails").click(function(){
        var rowindex = $(this).closest('tr').index();
        var mrn = "mrnQtyArray".concat(rowindex);
        var issue = "issueQtyArray".concat(rowindex);
        var pri = "priQtyArray".concat(rowindex)
        var prn = "prnQtyArray".concat(rowindex);
        var itemName = "item".concat(rowindex);

        var mrnQty = $('#'+mrn).attr('value');
        var issueQty = $('#'+issue).attr('value');
        var priQty = $('#'+pri).attr('value');
        var prnQty = $('#'+prn).attr('value');
        var item = $('#'+itemName).attr('value');

        loadPopup(mrnQty,issueQty,priQty,prnQty,item);

    });



});

function loadPopup(mrnQty,issueQty,priQty,prnQty,item)
{
    popupWindow3('1');
    $('#popupContact1').load(basePath+'rptItemWiseStockPopUp.php?mrn='+mrnQty+'&issue='+issueQty+'&pri='+priQty+'&prn='+prnQty+'&item='+item,function(){
        $('#frmDetailsPopup #butClose1').click(disablePopup);
    });
}


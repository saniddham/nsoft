var basePath	="presentation/warehouse/reports/";

$(document).ready(function(){
	//------------------------
	$('#tblPOSummeryHead').click(function(){

		headShowHide('tblPOSummeryHead','tblPOSummeryBody');
	});

	$('#frmPOSummery thead').click(function(){

		bodyShowHide('tblPOSummeryHead','tblPOSummeryBody');

	});

	$('#tblPOSummeryBody thead').click(function(){

		bodyShowHide('tblPOSummeryHead','tblPOSummeryBody');

	});
	
	$('#frmPOSummery #butNew').click(function(){
		$('#frmPOSummery')[0].reset();
	});
	
	$('#tblStockPredictionHead').click(function(){

		headShowHide('tblStockPredictionHead','tblStockPredictionBody');
	});
	
	$('#frmStockPrediction thead').click(function(){

		bodyShowHide('tblStockPredictionHead','tblStockPredictionBody');

	});
	
	$('#tblStockPredictionBody thead').click(function(){

		bodyShowHide('tblStockPredictionHead','tblStockPredictionBody');

	});
	
	//
	$('#tblItemStockPredictionHead').click(function(){

		headShowHide('tblItemStockPredictionHead','tblItemStockPredictionBody');
	});
	
	$('#frmItemStockPrediction thead').click(function(){

		bodyShowHide('tblItemStockPredictionHead','tblItemStockPredictionBody');

	});
	
	$('#tblItemStockPredictionBody thead').click(function(){

		bodyShowHide('tblItemStockPredictionHead','tblItemStockPredictionBody');

	});

	// --------------
	$('#tblItemReturnableHead').click(function(){

		headShowHide('tblItemReturnableHead','tblItemReturnableBody');
	});

	$('#frmItemReturnable thead').click(function(){

		bodyShowHide('tblItemReturnableHead','tblItemReturnableBody');

	});

	$('#tblItemReturnableBody thead').click(function(){

		bodyShowHide('tblItemReturnableHead','tblItemReturnableBody');

	});
	// --------------

	// --------------
	$('#tblOrderwiseStockMovementHead').click(function(){

		headShowHide('tblOrderwiseStockMovementHead','tblOrderwiseStockMovementBody');
	});

	$('#frmItemReturnable thead').click(function(){

		bodyShowHide('tblOrderwiseStockMovementHead','tblOrderwiseStockMovementBody');

	});

	$('#tblOrderwiseStockMovementBody thead').click(function(){

		bodyShowHide('tblOrderwiseStockMovementHead','tblOrderwiseStockMovementBody');

	});
	// --------------

	$("#frmItemStockPrediction .chosen-select").chosen({allow_single_deselect: false , no_results_text: "Oops, nothing found!",width:"100%"});
	
	$("#frmOrderwiseStockMovement .chosen-select").chosen({allow_single_deselect: false , no_results_text: "Oops, nothing found!",width:"100%"});
	
	$('#frmItemStockPrediction #cbomaincategory').change(function()
	{
	var mainCategory 	= $('#cbomaincategory').val();
	var url 		 	= basePath+"report-db-get.php?requestType=loadSubCategory_multi&mainCategory="+mainCategory;
	var httpobj 		= $.ajax({url:url,async:false})
	
	$('#frmItemStockPrediction #cbosubcategory').html(httpobj.responseText);
	$("#frmItemStockPrediction .clssubcategory").trigger("chosen:updated");
	
	});
	
	$('#frmItemStockPrediction #cbosubcategory').change(function()
	{
	var mainCategory 	= $('#cbomaincategory').val();	
	var subcategory 	= $('#cbosubcategory').val();
	var url 		 	= basePath+"report-db-get.php?requestType=loadItem_multiple&mainCategory="+mainCategory+"&subCategory="+subcategory;
	var httpobj 		=  $.ajax({url:url,async:false})
	$('#frmItemStockPrediction #cboitem').html(httpobj.responseText);
	$("#frmItemStockPrediction .clsitem").trigger("chosen:updated");
	
	});
	
	$("#cboYear").die('change').live('change',function()
	{
	 loadOrder($(this).val(),this);
	}); 

		
});


function loadOrder(year,obj)
{
	
	var url      =  basePath+"report-db-get.php?requestType=loadOrder&year="+year;
	
	$.ajax({
			url:url,
			dataType:'json',
			async:false,
			success:function(json)
			{
				
				$('#frmOrderwiseStockMovement #cboorder').html(json.orderHTML);
				$("#frmOrderwiseStockMovement .clsorder").trigger("chosen:updated");
			}
	});
	
}


function ViewReport(type,formId)
{
	var reportId	="927";
	if ($('#'+formId).validationEngine('validate'))  {
		var orderNo    = $('#frmPOSummery #orderNo').val();
		var orderYear  = $('#frmPOSummery #orderYear').val();
	var url = "?q=1179&orderNo="+orderNo+"&orderYear="+orderYear;
	if(orderNo && orderYear != ""){
	window.open(url)	
		}
	}
}

function ViewStockOrderForecastReport(type,formId)
{   


	var company = $('#frmStockPrediction #cboCompany_r').val();
	var dateFrom = $('#frmStockPrediction #prnwiseDateFrom').val();
	var dateTo = $('#frmStockPrediction #prnwiseDateTo').val();

	if(company == '') {
		$('#validate_company').html('<span id="error" style=\"color:red\">* This Field is Required</span>');
		return false;
	}else if(company != ''){
		$('#validate_company').html('<span id="error" style=\"display: none\">* This Field is Required</span>');
	}


	
	// if(dateFrom == ''){
	// 	$('#validate_from').html('<span id="error" style=\"color:red\">* This Field is Required</span>');
	// 	return false;
	// }else if(dateFrom != ''){
	// 	$('#validate_from').html('<span id="error" style=\"display:none\">* This Field is Required</span>');
	//
	// }
	//
	// if(dateTo ==''){
	// 	$('#validate_to').html('<span id="error" style=\"color:red\">* This Field is Required</span>');
	// 	return false;
	// }else if(dateTo !=''){
	// 	$('#validate_to').html('<span id="error" style=\"display:none\">* This Field is Required</span>');
	//
	// }

	// var reportId	="928";
	// if ($('#'+formId).validationEngine('validate')) {
	// 	var company = $('#frmStockPrediction #cboCompany').val();
	// 	var dateFrom = $('#frmStockPrediction #prnwiseDateFrom').val();
	// 	var dateTo = $('#frmStockPrediction #prnwiseDateTo').val();
	// 	if (dateFrom > dateTo) {
	// 		alert("Invalid date range.");
	// 	}
	//	
	//
	// 	else{
	//
	// 		var url = "?q=1216&dateFrom="+dateFrom+"&dateTo="+dateTo;
	//
	// 		if(dateFrom != "" && dateTo != ""){
	// 			var path = window.location.href.split('?')[0];
	// 			//window.open(url, 'TheWindow');
	// 		// window.open(url)
	// 		}
	// 	}
	// }

}


function ViewIetmWiseStockOrderForecastReport(type,formId)
{
	var reportId	="929";
	if ($('#'+formId).validationEngine('validate')) 
	 {
		var company 		= $('#frmItemStockPrediction #cbocompany').val();
		var maincategory    = $('#frmItemStockPrediction #cbomaincategory').val();
		var subcategory    	= $('#frmItemStockPrediction #cbosubcategory').val();
		var item           	= $('#frmItemStockPrediction #cboitem').val();
		var BalToPurchase   = ($("#frmItemStockPrediction #balance").is(':checked') ) ? 1 : 0;
		var mrn             = ($("#frmItemStockPrediction #mrn").is(':checked') ) ? 1 : 0;
		
			
		var url = "?q=1218&company="+company+"&maincategory="+maincategory+"&subcategory="+subcategory+"&item="+item+"&BalToPurchase="+BalToPurchase+"&mrn="+mrn;
		if(company == '')
		{
			
		$('#validate_company1').html('<span id="error" style=\"color:red\">* This Field is Required</span>');
		return false;
		}
		else
		{
			
		window.open(url)
		}
		
	}
}

function ViewReturnableIetmReport(type,formId)
{
	var reportId	="930";
	if ($('#'+formId).validationEngine('validate'))  {
		var dateFrom = $('#frmItemReturnable #returnableItemDateFrom').val();
		var dateTo   = $('#frmItemReturnable #returnableItemDateTo').val();
		if(dateFrom > dateTo){
			alert("Invalid date range.");
		}
		else{
			var url = "?q=1223&dateFrom="+dateFrom+"&dateTo="+dateTo;
			if(dateFrom != "" && dateTo != ""){
				window.open(url)
			}
		}
	}
}

function ViewOrderwiseStockMovementReport(type,formId)
{
    var reportId			= "931";
    if ($('#'+formId).validationEngine('validate')) 
	 {
        var year           = $('#frmOrderwiseStockMovement #cboYear').val(); 
        var order		   = $('#frmOrderwiseStockMovement #cboorder').val();
        var report_type    = $('input[name=radio]:checked').val();
		
        if(order != "")
		{
            var url = "?q=1224&order="+order+"&year="+year+"&report_type="+report_type;
            window.open(url)
        }
    }
}

function ViewReport_excel_new1(type,formId,format)//excel report suvini
{
	
	    var year           = $('#frmOrderwiseStockMovement #cboYear').val(); 
        var order		   = $('#frmOrderwiseStockMovement #cboorder').val();
		var company        = $('#frmOrderwiseStockMovement #company').val();
        var report_cat     = $('input[name=radio]:checked').val(); 
	 
		var url = "presentation/warehouse/reports/report.php?type="+type+"&report_type="+format+"&formId="+formId+"&year="+year+"&order="+order+"&company="+company+"&report_cat="+report_cat+"&report_type=excel";
	
		window.open(url)

}

 function ViewReport_excel_new2(type,formId,format)//excel report suvini
{
	
	    var company          = $('#frmItemStockPrediction #cbocompany').val();
        var maincategory	 = $('#frmItemStockPrediction #cbomaincategory').val();
		var subcategory      = $('#frmItemStockPrediction #cbosubcategory').val();
		var item             = $('#frmItemStockPrediction #cboitem').val();
		var BalToPurchase    = ($("#frmItemStockPrediction #balance").is(':checked') ) ? 1 : 0;
		var mrn              = ($("#frmItemStockPrediction #mrn").is(':checked') ) ? 1 : 0;
        
	 
		var url = "presentation/warehouse/reports/report.php?type="+type+"&report_type="+format+"&formId="+formId+"&company="+company+"&maincategory="+maincategory+"&subcategory="+subcategory+"&item="+item+"&BalToPurchase="+BalToPurchase+"&mrn="+mrn+"&report_type=excel";
	  
		window.open(url)

}

function headShowHide(headId,bodyId){

	$('#'+headId).hide('slow');
	$('#'+bodyId).toggle();

}

function bodyShowHide(headId,bodyId){

	$('#'+headId).show('slow');
	$('#'+bodyId).hide('slow');
}


function toggleTable() 
{
	
    var lTable = document.getElementById("tblSummary");
    lTable.style.display = (lTable.style.display == "table") ? "none" : "table";
}

//Author Hasitha Charaka

function submit_Report () {
	var cboCompany_r = $('#cboCompany_r').val();
	if(cboCompany_r == '') {
		$('#validate_company').html('<span id="error" style=\"color:red\">* This Field is Required</span>');
		return false ;
	}
	document.getElementById('frmStockPrediction').submit();

}
$("#cboMainCategory").change(function () {
	var mainCategory = $('#cboMainCategory').val();
	if(mainCategory != null) {
		var url = basePath + "quick_reports/report-db-get.php?requestType=loadSubCategoryMultiple&mainCategory=" + mainCategory;

	}else if(mainCategory == null) {
		var url = basePath + "quick_reports/report-db-get.php?requestType=loadSubCategoryMultiple&mainCategory=" + mainCategory;
	}
	var httpobj = $.ajax({url: url, async: false})
	document.getElementById('cboSubCategory').innerHTML=httpobj.responseText;

});

/*get orden numbers where selected company */
$("#cboCompany_r").change(function () {
	var cboCompany_r = $('#cboCompany_r').val();
	var url 		= basePath+"quick_reports/report-db-get.php?requestType=loadOrderNumberToCompany&cboCompany_r="+cboCompany_r;
	var httpobj 	= $.ajax({url:url,async:false});
	document.getElementById('order_no').innerHTML = httpobj.responseText;

} );

$("#cboSubCategory").change(function () {
	var mainCategory = $('#cboMainCategory').val();
	var subCategory = $('#cboSubCategory').val();
	var url 		= basePath+"quick_reports/report-db-get.php?requestType=loadItemsMultiple&mainCategory="+mainCategory+"&subCategory="+subCategory;
	var httpobj 	= $.ajax({url:url,async:false});

	document.getElementById('cboItem').innerHTML = httpobj.responseText;

});


/*$("#btn_load_subcat").click(function(){
	// alert($("#cboMainCategory").val());
	$('#validate_maincategory').html('<span id="error" style=\"display:none\">* Please Load SubCategory</span>');
	var mainCategory = $('#cboMainCategory').val();
	if(mainCategory != null) {
		var url = basePath + "quick_reports/report-db-get.php?requestType=loadSubCategoryMultiple&mainCategory=" + mainCategory;
		
	}else if(mainCategory == null) {
		var url = basePath + "quick_reports/report-db-get.php?requestType=loadSubCategoryMultiple&mainCategory=" + mainCategory;
	}
	var httpobj = $.ajax({url: url, async: false})
	document.getElementById('cboSubCategory').innerHTML=httpobj.responseText;


	var url1 		= basePath+"quick_reports/report-db-get.php?requestType=loadItems&mainCategory="+ +"&subCategory=";
	var httpo 	= $.ajax({url:url1,async:false})
	document.getElementById('cboItem').innerHTML = httpo.responseText;
});*/

/*$("#btn_load_item").click(function(){
	$('#validate_subcategory').html('<span id="error" style=\"display:none\">* Please Load SubCategory</span>');
	var mainCategory = $('#cboMainCategory').val();
	var subCategory = $('#cboSubCategory').val();

	var url 		= basePath+"quick_reports/report-db-get.php?requestType=loadItemsMultiple&mainCategory="+mainCategory+"&subCategory="+subCategory;
	var httpobj 	= $.ajax({url:url,async:false});

	document.getElementById('cboItem').innerHTML = httpobj.responseText;

});*/


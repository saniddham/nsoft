<?php
session_start();
$backwardseperator = "../../../";
$mainPath = $_SESSION['mainPath'];

	$locationId = $_SESSION['CompanyID'];
	
$thisFilePath 		=  $_SERVER['PHP_SELF'];
include  "{$backwardseperator}dataAccess/permisionCheck.inc";
?>
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<title>Item List</title>
<table width="900" border="0" cellspacing="0" cellpadding="0" align="center">
  <tr>
    <td colspan="3"><?php include "../../../reportHeader.php"; ?></td>
  </tr>
  <tr>
    <td width="33">&nbsp;</td>
    <td colspan="2" class="reportHeader">&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td colspan="2" align="center" class="reportHeader">Item List</td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3">
        <?php
  	$sql = "SELECT
				mst_maincategory.strName,intId
			FROM
				mst_maincategory
			WHERE
				mst_maincategory.intStatus =  '1'
			ORDER BY
				mst_maincategory.strName ASC
			";
			
	$result = $db->RunQuery($sql);
	while($row=mysqli_fetch_array($result))
	{
		$mainId = $row['intId'];
	?>
     <table class="pageBreak" width="100%" border="0" cellspacing="0" cellpadding="0">
     <tr>
        <td colspan="3" class="reportSubHeader"><?php echo $row['strName']; ?></td>
        </tr>
      <?php
     // $companyId = $row['intId'];
		$sql2 = "SELECT
					mst_subcategory.strName,intId
				FROM
					mst_subcategory
				WHERE
					mst_subcategory.intMainCategory =  '".$row['intId']."' AND
					mst_subcategory.intStatus =  '1'
				ORDER BY
					mst_subcategory.strName ASC
				";
			?>
            <?php
			$result2 = $db->RunQuery($sql2);
			while($row2=mysqli_fetch_array($result2))
			{
				$subId = $row2['intId'];
			?>
            <tr>
    <td width="33">&nbsp;</td>
    <td colspan="2" bgcolor="#E1F8FF" class="normalfnt"><strong><?php echo $row2['strCode'].' - '.$row2['strName']; ?></strong></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td class="normalfnt"><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr class="normalfnt">
        <td width="36%" bgcolor="#CCCCCC"><strong>Name</strong></td>
        <td width="16%" align="center" bgcolor="#CCCCCC"><strong>Special RM</strong></td>
        <td width="14%" bgcolor="#CCCCCC"><strong>UOM</strong></td>
        <td width="18%" bgcolor="#CCCCCC"><strong>Active</strong></td>
        </tr>
        <?php
			$sql3 = "SELECT
						mst_item.strCode,
						mst_item.strName as itemName,
						mst_item.intStatus,
						mst_units.strName as unitName,intSpecialRm
						FROM
						mst_item
						Inner Join mst_units ON mst_units.intId = mst_item.intUOM
						WHERE
						mst_item.intMainCategory =  '$mainId' AND
						mst_item.intSubCategory =  '$subId'
						";
			$result3 = $db->RunQuery($sql3);
			while($row3=mysqli_fetch_array($result3))
			{
		?>
      <tr class="normalfnt">
        <td><?php echo $row3['itemName']; ?></td>
        <td class="normalfntMid"><?php echo ($row3['intSpecialRm']==1?'Y':''); ?></td>
        <td><?php echo $row3['unitName']; ?></td>
        <td><?php echo ($row3['intStatus']==1?'Active':'Inactive'); ?></td>
        </tr>
        <?php
			}
		?>
    </table></td>
    </tr>
   <tr>
     <td>&nbsp;</td>
     <td width="64" class="normalfnt">&nbsp;</td>
     <td width="803">&nbsp;</td>
   </tr>
	<?php
			}
	?>
     </table>
    <?php
	}
	?>
    </td>
  </tr>

  <tr>
    <td>&nbsp;</td>
    <td width="200">&nbsp;</td>
    <td width="667">&nbsp;</td>
  </tr>
</table>

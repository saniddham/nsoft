<?php

//ini_set('max_execution_time', 1111111111111111) ;
set_time_limit(600);
session_start();
//echo 'Time Limit = ' . ini_get('max_execution_time') .

$companyId = $sessions->getCompanyId();

$userId = $sessions->getUserId();
$locationId = $sessions->getLocationId();
$head_office_loc	= get_ho_loc($companyId);
$programCode = 'P0929';

require_once "class/cls_permisions.php";
$objpermisionget = new cls_permisions($db);
require_once "class/tables/sys_approvelevels.php";
$obj_sys_approvelevels = new sys_approvelevels($db);

$permi_price_view = $objpermisionget->boolSPermision(8);
$str_price_view = 'hidden';

if ($permi_price_view)
    $str_price_view = '';

$dateFrom  = $_REQUEST['dateFrom'];
$dateTo  = $_REQUEST['dateTo'];

//Get orderNo, Order year and revision No
/*$orderNo = $_REQUEST['orderNo'];
$orderYear = $_REQUEST['orderYear'];

$approveMode = (!isset($_REQUEST["approveMode"]) ? '' : $_REQUEST["approveMode"]);
$revise = (!isset($_REQUEST["revise"]) ? '' : $_REQUEST["revise"]);


                $sql = "SELECT
					trn_orderheader.intOrderNo AS orderNo,
					trn_orderheader.intOrderYear AS orderYear, 
					trn_orderheader.strCustomerPoNo AS customerPO,
					mst_customer.strName AS strCustomer,
					trn_orderheader.dtDate AS dtDate,
					trn_orderheader.intReviseNo AS RevisionNO,
					C.strUserName AS creator,
					trn_orderheader.dtDeliveryDate AS dtDeliveryDate,
					A.strUserName AS marketer
				FROM
					trn_orderheader
					Inner Join mst_customer ON mst_customer.intId = trn_orderheader.intCustomer
					Inner Join sys_users AS A ON A.intUserId = trn_orderheader.intMarketer
					Inner Join sys_users AS C ON C.intUserId = trn_orderheader.intCreator		
				WHERE
					trn_orderheader.intOrderNo 		=  '$orderNo' AND
					trn_orderheader.intOrderYear 	=  '$orderYear' 
					";

$result = $db->RunQuery($sql);

$row = mysqli_fetch_array($result);
$orderNo = $row['orderNo'];
$orderYear = $row['orderYear'];
$strCustomerPoNo = $row['customerPO'];
$strCustomer = $row['strCustomer'];
$dtDate = $row['dtDate'];
$dtDeliveryDate = $row['dtDeliveryDate'];
$marketer = $row['marketer'];
$creator = $row['creator'];
$reviseNo = $row['RevisionNO'];
*/
?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Order Wise Stock Order Forecast Report</title>
        <script type="text/javascript"
                src="presentation/customerAndOperation/bulk/placeOrder/listing/rptBulkOrder-js.js?n=1"></script>
        <style>
            .break {
                page-break-before: always;
            }

            @media print {
                .noPrint {
                    display: none;
                }
            }

            #apDiv1 {
                position: absolute;
                left: 237px;
                top: 175px;
                width: 619px;
                height: 235px;
                z-index: 1;
            }

            .APPROVE {
                font-size: 16px;
                font-weight: bold;
                color: #00C;
            }
        </style>
    </head>

    <body>
    <form id="frmSamplePlaceOrderReport" name="frmSamplePlaceOrderReport" method="post" action="rptStock_Forecast.php">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="3"></td>
            </tr>
            <tr>
                <td width="20%"></td>
                <td width="60%" height="80"
                    valign="top"><?php include 'presentation/procurement/reports/reportHeader.php' ?></td>
                <td width="20%"></td>
            </tr>
            <tr>
                <td colspan="3">
                    <div id="divPoNo" style="display:none"><?php echo $orderNo ?>/<?php echo $orderYear ?></div>
                </td>
            </tr>
        </table>
        <div align="center">
            <div style="background-color:#FFF"><strong>Order Wise Stock Order Forecast Report</strong><strong></strong></div>
            <table width="900" border="0" align="center" bgcolor="#FFFFFF">
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td colspan="10" align="center" bgcolor="#FFFFFF">
                                </td>
                            </tr>
                            <tr>
                                <td width="1%"><span class="normalfnt">Date Range:</span></td>
                                <td width="9%" class="normalfnt"><?php echo $dateFrom; ?> To <?php echo $dateTo; ?></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td colspan="20">
                                    <table width="100%" class="bordered" id="tblMainGrid" cellspacing="0"
                                           cellpadding="0">
                                        <tr>
                                            <th width="2%">Date</th>
                                            <th width="10%">To date Orders</th>
                                            <th width="10%">Other Order Dates</th>
                                            <th width="10%" nowrap>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Other Orders&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </th>
                                            <th width="6">Location</th>
                                            <th width="8%">Main Category</th>
                                            <th width="8%">Sub Category</th>
                                            <th width="4%"> Code</th>
                                            <th width="10%">Item </th>
                                            <th width="10%">UOM </th>
                                            <th width="3%" height="22">PRI </th>
                                            <th width="3%" height="22">PRN </th>
                                            <th width="3%"> PURCHASED </th>
                                            <th width="3%">MRN </th>
                                            <th width="3%">ISSUED </th>
                                            <th width="3%">RETURNED</th>
                                            <th width="3%">GP OUT</th>
                                            <th width="3%">GP IN</th>
                                            <th width="3%">PRI - Other Orders</th>
                                            <th width="3%">Issued to HO - All Orders</th>
                                            <th width="3%">GP fromHO- All Orders</th>
                                            <th width="3%">Issued From non-HO- All Orders</th>
                                            <th width="3%">PURCHASED - Other Orders</th>
                                            <th width="3%">Bal To Issue For All</th>
                                            <th width="3%">HO  Stock</th>
                                            <th width="3%">GRN Required</th>
                                            <th width="3%">Forecasted Pending Balance Stock for Production </th>
                                        </tr>
                                        <?php
                                        $sql4 = get_forecastDetails($dateFrom,$dateTo,$head_office_loc,$companyId);

                                        $result4 = $db->RunQuery($sql4);


                                        $i=0;
                                        while ($row = mysqli_fetch_array($result4)) {
                                            $i++;

//                                            echo '(('.$row['REQUIRED'].'+'.$row['PRI_OTHER_ORDERS'].')-('.$row['TOT_HO_ISSUED'].'+MAX('.$row['TOT_NON_HO_ISSUED'].',('.$row['GP_OUT_FROM_HO'].'+'.$row['GP_OTHER_ORDERS'].'))))';
//                                            echo '<br>';

                                            $MRN_Qty = round($row['MRN_QTY_TODATE'],4);
                                            if($row['type'] == 2){
                                                $bal_to_issue_for_all = round((($MRN_Qty) - ($row['ISSUE_QTY_ALL_HO'] + MAX($row['ISSUE_QTY_ALL_NON_HO'], ($row['GP_FROM_HO_ALL'])))), 4);
                                            }else {
                                                $bal_to_issue_for_all = round((($row['REQUIRED_TODATE'] + $row['PRI_OTHER']) - ($row['ISSUE_QTY_ALL_HO'] + MAX($row['ISSUE_QTY_ALL_NON_HO'], ($row['GP_FROM_HO_ALL'])))), 4);
                                            }

                                            if($bal_to_issue_for_all < 0)
                                                $bal_to_issue_for_all=0;

                                            $bal_to_grn				=((round((($row['REQUIRED_TODATE']+$row['PRI_OTHER'])-($row['ISSUE_QTY_ALL_HO']+MAX($row['ISSUE_QTY_ALL_NON_HO'],($row['GP_FROM_HO_ALL'])))),4))-(round(getHOStock($row['ITEM'],$head_office_loc),4))) ;
                                            if($bal_to_grn < 0)
                                                $bal_to_grn=0;
                                            $bal_to_purchase		=$bal_to_grn-get_approved_non_grn_pos($row['ITEM'],2);
                                            if($bal_to_purchase < 0)
                                                $bal_to_purchase=0;

                                            if($row['type']==1 && $i%2 == 0)
                                                $col	='#FFFFFF';
                                            else if($row['type']==1 && $i%2 != 0)
                                                $col	='#B7B7B7';
                                            if($row['type']!=1)
                                                $col	='#FF99CC';
                                            ?>
                                            <tr bgcolor="<?php echo $col; ?>">
                                                <td class="normalfnt" style="text-align: center">&nbsp;<?php echo $row['DATES_TODATE']; ?>&nbsp;</td>
                                                <td class="normalfnt" style="text-align: center" title="To date Orders"><span class="normalfnt" style="text-align: center"><?php echo $row['TODATE_ORDERS']; ?></span></td>
                                                <td class="normalfnt" style="text-align: center" title="Other Order Dates"><span class="normalfnt" style="text-align: center"><?php echo $row['DATES_OTHER']; ?></span></td>
                                                <td class="normalfnt" style="text-align: left" title="Other Orders"><?php echo $row['OTHER_ORDERS']; ?>&nbsp;</td>
                                                <td class="normalfnt" style="text-align: center" title="Location">&nbsp;<?php echo $row['location']; ?>&nbsp;</td>
                                                <td class="normalfnt" style="text-align: center" title="Main Category">&nbsp;<?php echo $row['mainCatName']; ?>&nbsp;</td>
                                                <td class="normalfnt" style="text-align: center" title="Sub Category">&nbsp;<?php echo $row['subCatName']; ?>&nbsp;</td>
                                                <td class="normalfnt" style="text-align: center" title="Code">&nbsp;<?php echo $row['strCode']; ?>&nbsp;</td>
                                                <td class="normalfnt" style="text-align: center" title="Item">&nbsp;<?php echo $row['itemName']; ?>&nbsp;</td>
                                                <td class="normalfnt" style="text-align: center" title="UOM">&nbsp;<?php echo $row['UOM']; ?>&nbsp;</td>
                                                <td class="normalfnt" style="text-align: right" title="PRI">&nbsp;<?php echo round($row['REQUIRED_TODATE'],4); ?>&nbsp;</td>
                                                <td class="normalfnt" style="text-align: right" title="PRN">&nbsp;<?php echo round($row['PRN_QTY_TODATE'],4); ?>&nbsp;</td>
                                                <td class="normalfnt" style="text-align: right" title="PURCHASED">&nbsp;<?php echo round($row['PO_QTY_TODATE'],4); ?>&nbsp;</td>
                                                <td class="normalfnt" style="text-align: right" title="MRN">&nbsp;<?php echo $MRN_Qty; ?>&nbsp;</td>
                                                <td class="normalfnt" style="text-align: right" title="ISSUED">&nbsp;<?php echo round($row['ISSUE_QTY_TODATE'],4); ?>&nbsp;</td>
                                                <td class="normalfnt" style="text-align: right" title="RETURNED">&nbsp;<?php echo round($row['RET_TO_STORE_QTY_TODATE'],4); ?>&nbsp;</td>
                                                <td class="normalfnt" style="text-align: right" title="GP OUT">&nbsp;<?php echo round($row['GP_FROM_HO_TODATE'],4); ?>&nbsp;</td>
                                                <td class="normalfnt" style="text-align: right" title="GP IN"><?php echo round($row['GP_IN_QTY_TO_HO_TODATE'],4); ?></td>
                                                <td class="normalfnt" title="PRI - Other Orders" style="text-align: right">&nbsp;<?php echo round($row['PRI_OTHER'],4); ?>&nbsp;</td>
                                                <td class="normalfnt" title="Issued to HO - All Orders" style="text-align: right">&nbsp;<?php echo round($row['ISSUE_QTY_ALL_HO'],4); ?>&nbsp;</td>
                                                <td class="normalfnt" title="GP fromHO- All Orders" style="text-align: right">&nbsp;<?php echo round($row['GP_FROM_HO_ALL'],4); ?>&nbsp;</td>
                                                <td class="normalfnt" style="text-align: right" title="Issued From non-HO- All Orders"><span class="normalfnt" style="text-align: right"><?php echo round($row['ISSUE_QTY_ALL_NON_HO'],4); ?></span></td>
                                                <td class="normalfnt" title="PURCHASED - Other Orders" style="text-align: right">&nbsp;<?php echo round($row['PO_QTY_OTHER'],4); ?>&nbsp;</td>
                                                <td class="normalfnt" title="Bal To Issue For All" style="text-align: right">&nbsp;<?php echo $bal_to_issue_for_all ?>&nbsp;</td>
                                                <td class="normalfnt" title="HO Stock" style="text-align: right">&nbsp;<?php echo round(getHOStock($row['ITEM'],$head_office_loc),4); ?>&nbsp;</td>
                                                <td class="normalfnt" title="GRN Required" style="text-align: right">&nbsp;<?php echo $bal_to_grn ?>&nbsp;</td>
                                                <td class="normalfnt" title="Forecasted Pending Balance Stock for Production" style="text-align: right">&nbsp;<?php echo $bal_to_purchase ?>&nbsp;</td>
                                            </tr>
                                        <?php } ?>

                                    </table>
                                </td>
                            </tr>

                        </table>
                    </td>
                    <td width="6%">&nbsp;</td>
                </tr>
            </table>
            </td>
            </tr>
            <tr></tr></div>
    </form>
    </body>
    </html>
<?php

function getHOStock($item,$head_office_loc){

    global $db;

    $sql = "SELECT
			Sum(ware_stocktransactions_bulk.dblQty) as qty 
			FROM `ware_stocktransactions_bulk`
			WHERE
			ware_stocktransactions_bulk.intItemId = '$item' AND
			ware_stocktransactions_bulk.intLocationId = '$head_office_loc'
			";

    $result = $db->RunQuery($sql);
    $row=mysqli_fetch_array($result);
    return $row['qty'];

}
function get_approved_non_grn_pos($item,$location){

    global $db;

    $sql = "SELECT
			sum(trn_podetails.dblQty-trn_podetails.dblGRNQty) as qty
			FROM
			trn_podetails
			INNER JOIN trn_poheader ON trn_podetails.intPONo = trn_poheader.intPONo AND trn_podetails.intPOYear = trn_poheader.intPOYear
			WHERE
			trn_poheader.intStatus = 1 AND
			trn_poheader.intDeliveryTo = '$location' AND
			trn_podetails.intItem = '$item'
			";

    $result = $db->RunQuery($sql);
    $row=mysqli_fetch_array($result);
    return $row['qty'];


}
function get_ho_loc($companyId){
    global $db;
    $sql="SELECT
			mst_locations.intId
			FROM `mst_locations`
			WHERE
			mst_locations.COMPANY_MAIN_STORES_LOCATION = 1 AND
			mst_locations.intCompanyId = '$companyId'
			";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    return $row['intId'];

}
function get_forecastDetails($dateFrom,$dateTo,$head_office_loc,$companyId){
    $sql	= "SELECT  
	TB_TODATE.type,
    TB_TODATE.date as DATES_TODATE,
	TB_ALL.date as DATES_OTHER,
	TB_TODATE.ORDER_COMPANY,  
	TB_TODATE.location AS location,
	TB_TODATE.ITEM,
	TB_TODATE.mainCatName,
	TB_TODATE.subCatName,
	TB_TODATE.strCode,
	TB_TODATE.itemName,
	TB_TODATE.otherOrders AS TODATE_ORDERS,
	TB_TODATE.REQUIRED AS REQUIRED_TODATE,
	TB_TODATE.PRN_QTY AS PRN_QTY_TODATE,
	TB_TODATE.PO_QTY AS PO_QTY_TODATE,
	(
		ifnull(TB_TODATE.MRN_QTY,0) + ifnull(TB_TODATE.MRN_QTY_OTHER_LOC,0)
	) AS MRN_QTY_TODATE,
	(
		ifnull(TB_TODATE.ISSUE_QTY,0) + ifnull(TB_TODATE.ISSUE_QTY_OTHER_LOC,0)
	) AS ISSUE_QTY_TODATE,
	(
		ifnull(TB_TODATE.RET_TO_STORE_QTY,0) + ifnull(TB_TODATE.RET_TO_STORE_QTY_OTHER_LOC,0)
	) AS RET_TO_STORE_QTY_TODATE,
	TB_TODATE.GP_QTY AS GP_FROM_HO_TODATE,
	TB_TODATE.GP_IN_QTY AS GP_IN_QTY_TO_HO_TODATE,
	TB_TODATE.UOM,
	TB_ALL.otherOrders AS OTHER_ORDERS,
	TB_ALL.REQUIRED AS PRI_OTHER,
	(
		ifnull(TB_TODATE.ISSUE_QTY,0) + ifnull(TB_ALL.ISSUE_QTY,0)
	) AS ISSUE_QTY_ALL_HO,
	(
		ifnull(TB_TODATE.GP_QTY,0) + ifnull(TB_ALL.GP_QTY,0)
	) AS GP_FROM_HO_ALL,
	(
		ifnull(TB_TODATE.ISSUE_QTY_OTHER_LOC,0) + ifnull(TB_ALL.ISSUE_QTY_OTHER_LOC,0)
	) AS ISSUE_QTY_ALL_NON_HO,
	TB_ALL.PO_QTY AS PO_QTY_OTHER
	
 from (select 
tb.type,
	tb.ORDER_COMPANY,
	tb.location AS location,
    tb.date,
	tb.ITEM,
  GROUP_CONCAT(DISTINCT tb.ORDER_NO,'/',tb.ORDER_YEAR SEPARATOR ' ') as otherOrders,
  -- tb.otherOrders,
	SUM(tb.REQUIRED) as REQUIRED,
	tb.TRANSACTION_LOCATION,
	sum(tb.PO_QTY) as PO_QTY,
	sum(tb.PRN_QTY) as PRN_QTY,
	tb.mainCatName,
	tb.subCatName,
	tb.strCode,
	tb.itemName,
	tb.UOM,
	SUM(tb.MRN_QTY) as MRN_QTY,
	SUM(tb.MRN_EXTRA_QTY) as MRN_EXTRA_QTY,
	SUM(tb.ISSUE_QTY) as ISSUE_QTY,
	SUM(tb.GP_QTY) as GP_QTY,
	SUM(tb.GP_IN_QTY) as GP_IN_QTY,
	SUM(tb.RET_TO_STORE_QTY) as RET_TO_STORE_QTY,
	SUM(tb.MRN_QTY_OTHER_LOC) as MRN_QTY_OTHER_LOC,
	SUM(tb.MRN_EXTRA_QTY_OTHER_LOC) as MRN_EXTRA_QTY_OTHER_LOC,
	SUM(tb.ISSUE_QTY_OTHER_LOC) as ISSUE_QTY_OTHER_LOC,
	SUM(tb.GP_QTY_OTHER_LOC) as GP_QTY_OTHER_LOC,
	SUM(tb.GP_IN_QTY_OTHER_LOC) as GP_IN_QTY_OTHER_LOC,
	SUM(tb.RET_TO_STORE_QTY_OTHER_LOC) as RET_TO_STORE_QTY_OTHER_LOC
 from(
SELECT 
 	TB3.type,
	TB3.ORDER_COMPANY,
	TB3.location AS location,
    TB3.date,
	TB3.ITEM,
  
	TB3.ORDER_NO,
	TB3.ORDER_YEAR,
	(select sum(trn_po_prn_details.REQUIRED) from trn_po_prn_details 
								where trn_po_prn_details.ORDER_NO=TB3.ORDER_NO 
								and trn_po_prn_details.ORDER_YEAR=TB3.ORDER_YEAR 
								and trn_po_prn_details.ITEM=TB3.ITEM) AS REQUIRED, 
	TB3.TRANSACTION_LOCATION,
	(TB3.PO_QTY) as PO_QTY,
	(TB3.PRN_QTY) as PRN_QTY,
	TB3.mainCatName,
	TB3.subCatName,
	TB3.strCode,
	TB3.itemName,
	TB3.UOM,
	SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.MRN_QTY,0)) as MRN_QTY,
	SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.MRN_EXTRA_QTY,0)) as MRN_EXTRA_QTY,
	SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.ISSUE_QTY,0)) as ISSUE_QTY,
	SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.GP_QTY,0)) as GP_QTY,
	SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.GP_IN_QTY,0)) as GP_IN_QTY,
	SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.RET_TO_STORE_QTY,0)) as RET_TO_STORE_QTY,
	SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.MRN_QTY,0)) as MRN_QTY_OTHER_LOC,
	SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.MRN_EXTRA_QTY,0)) as MRN_EXTRA_QTY_OTHER_LOC,
	SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.ISSUE_QTY,0)) as ISSUE_QTY_OTHER_LOC,
	SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.GP_QTY,0)) as GP_QTY_OTHER_LOC,
	SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.GP_IN_QTY,0)) as GP_IN_QTY_OTHER_LOC,
	SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.RET_TO_STORE_QTY,0)) as RET_TO_STORE_QTY_OTHER_LOC
FROM
	(
		SELECT
			trn_orderheader.intLocationId AS ORDER_COMPANY,
			TB2.ORDER_NO,
			TB2.ORDER_YEAR,
			TB2.location,
			TB2.ITEM,
			TB2.SALES_ORDER,
			TB2.type,
			CONCAT (trn_orderdetails.strSalesOrderNo,'/',trn_orderdetails.strCombo,'/',trn_orderdetails.strPrintName) AS SALES_ORDER_String,
            GROUP_CONCAT(DISTINCT date(trn_orderheader_approvedby.dtApprovedDate ))AS date,
            mst_maincategory.strName AS mainCatName,
			mst_subcategory.strName AS subCatName,
			mst_item.strCode,
			mst_item.intId,
			mst_item.strName AS itemName,
			mst_units.strName AS UOM,
			(trn_po_prn_details_sales_order.REQUIRED) as REQUIRED,
			trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID AS TRANSACTION_LOCATION,
			
			(
				SELECT
					trn_po_prn_details.PURCHASED_QTY
				FROM
					trn_po_prn_details
				WHERE
					trn_po_prn_details.ORDER_NO = TB2.ORDER_NO
				AND trn_po_prn_details.ORDER_YEAR = TB2.ORDER_YEAR
				AND trn_po_prn_details.ITEM = TB2.ITEM
			) AS PO_QTY,
			(
				SELECT
					trn_po_prn_details.PRN_QTY
				FROM
					trn_po_prn_details
				WHERE
					trn_po_prn_details.ORDER_NO = TB2.ORDER_NO
				AND trn_po_prn_details.ORDER_YEAR = TB2.ORDER_YEAR
				AND trn_po_prn_details.ITEM = TB2.ITEM
			) AS PRN_QTY,
			SUM(

				IF (
					trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'MRN',
					trn_po_prn_details_sales_order_stock_transactions.QTY,
					0
				)
			) AS MRN_QTY,
			SUM(

				IF (
					trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'MRN_EXTRA',
					trn_po_prn_details_sales_order_stock_transactions.QTY,
					0
				)
			) AS MRN_EXTRA_QTY,
			SUM(

				IF (
					trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'ISSUE',
					trn_po_prn_details_sales_order_stock_transactions.QTY,
					0
				)
			) AS ISSUE_QTY,
			SUM(

				IF (
					trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'GATE_PASS',
					trn_po_prn_details_sales_order_stock_transactions.QTY,
					0
				)
			) AS GP_QTY,
			SUM(

				IF (
					trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'GATE_PASS_IN',
					trn_po_prn_details_sales_order_stock_transactions.QTY,
					0
				)
			) AS GP_IN_QTY,
			SUM(

				IF (
					trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'RETURNED_TO_STORES',
					trn_po_prn_details_sales_order_stock_transactions.QTY,
					0
				)
			) AS RET_TO_STORE_QTY
		FROM
			(
				SELECT
					*
				FROM
					(
						SELECT
							trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
							trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
							trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER,
							trn_po_prn_details_sales_order_stock_transactions.ITEM,
							mst_locations.intCompanyId AS ORDER_COMPANY,
							mst_locations.strName AS location,
							(
								SELECT
									GROUP_CONCAT(mst_locations.intCompanyId)
								FROM
									ware_fabricreceivedheader
								INNER JOIN mst_locations ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId
								WHERE
									ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO
								AND ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
							) AS FABRIC_IN_COMPANIES,
							1 AS type
						FROM
							trn_po_prn_details_sales_order_stock_transactions
						INNER JOIN trn_po_prn_details_sales_order ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = trn_po_prn_details_sales_order.ORDER_NO
						AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR
						AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER
						AND trn_po_prn_details_sales_order_stock_transactions.ITEM = trn_po_prn_details_sales_order.ITEM
						INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO
						AND trn_orderheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
						INNER JOIN mst_locations ON mst_locations.intId = trn_orderheader.intLocationId
								INNER JOIN trn_orderheader_approvedby ON trn_orderheader_approvedby.intOrderNo = trn_orderheader.intOrderNo
								AND trn_orderheader_approvedby.intYear = trn_orderheader.intOrderYear
								AND trn_orderheader_approvedby.intStatus = 0
								AND trn_orderheader_approvedby.intApproveLevelNo = 1
								AND date(trn_orderheader_approvedby.dtApprovedDate) >= '$dateFrom'
								AND date(trn_orderheader_approvedby.dtApprovedDate) < '$dateTo'
						GROUP BY
							trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
							trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
							trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER,
							trn_po_prn_details_sales_order_stock_transactions.ITEM
						HAVING
							FIND_IN_SET('$companyId', FABRIC_IN_COMPANIES)
						OR ORDER_COMPANY = '$companyId'
						UNION ALL
							SELECT
								trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
								trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
								trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER,
								trn_po_prn_details_sales_order_stock_transactions.ITEM,
								mst_locations.intCompanyId AS ORDER_COMPANY,
								mst_locations.strName AS location,
								
								(
									SELECT
										GROUP_CONCAT(
											DISTINCT mst_locations.intCompanyId
										)
									FROM
										ware_fabricreceivedheader
									INNER JOIN mst_locations ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId
									WHERE
										ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO
									AND ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
								) AS FABRIC_IN_COMPANIES,
								2 AS type
							FROM
								trn_po_prn_details_sales_order_stock_transactions
							LEFT JOIN trn_po_prn_details_sales_order ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = trn_po_prn_details_sales_order.ORDER_NO
							AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR
							AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER
							AND trn_po_prn_details_sales_order_stock_transactions.ITEM = trn_po_prn_details_sales_order.ITEM
							INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO
							AND trn_orderheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
							INNER JOIN mst_locations ON mst_locations.intId = trn_orderheader.intLocationId
								INNER JOIN trn_orderheader_approvedby ON trn_orderheader_approvedby.intOrderNo = trn_orderheader.intOrderNo
								AND trn_orderheader_approvedby.intYear = trn_orderheader.intOrderYear
								AND trn_orderheader_approvedby.intStatus = 0
								AND trn_orderheader_approvedby.intApproveLevelNo = 1
										
							WHERE
								trn_po_prn_details_sales_order.ORDER_NO IS NULL
								AND date(trn_orderheader_approvedby.dtApprovedDate) >= '$dateFrom'
								AND date(trn_orderheader_approvedby.dtApprovedDate) < '$dateTo'
							GROUP BY
								trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
								trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
								trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER,
								trn_po_prn_details_sales_order_stock_transactions.ITEM
							HAVING
								(
									FIND_IN_SET('$companyId', FABRIC_IN_COMPANIES)
									OR ORDER_COMPANY = '$companyId'
								)
							UNION ALL
								SELECT
									trn_po_prn_details_sales_order.ORDER_NO,
									trn_po_prn_details_sales_order.ORDER_YEAR,
									trn_po_prn_details_sales_order.SALES_ORDER,
									trn_po_prn_details_sales_order.ITEM,
									mst_locations.intCompanyId AS ORDER_COMPANY,
									mst_locations.strName AS location,
									(
										SELECT
											GROUP_CONCAT(
												DISTINCT mst_locations.intCompanyId
											)
										FROM
											ware_fabricreceivedheader
										INNER JOIN mst_locations ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId
										WHERE
											ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order.ORDER_NO
										AND ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order.ORDER_YEAR
									) AS FABRIC_IN_COMPANIES,
									1 AS type
								FROM
									trn_po_prn_details_sales_order_stock_transactions
								RIGHT JOIN trn_po_prn_details_sales_order ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = trn_po_prn_details_sales_order.ORDER_NO
								AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR
								AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER
								AND trn_po_prn_details_sales_order_stock_transactions.ITEM = trn_po_prn_details_sales_order.ITEM
								INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = trn_po_prn_details_sales_order.ORDER_NO
								AND trn_orderheader.intOrderYear = trn_po_prn_details_sales_order.ORDER_YEAR
								INNER JOIN mst_locations ON mst_locations.intId = trn_orderheader.intLocationId
								INNER JOIN trn_orderheader_approvedby ON trn_orderheader_approvedby.intOrderNo = trn_orderheader.intOrderNo
								AND trn_orderheader_approvedby.intYear = trn_orderheader.intOrderYear
								AND trn_orderheader_approvedby.intStatus = 0
								AND trn_orderheader_approvedby.intApproveLevelNo = 1
								WHERE
								 trn_po_prn_details_sales_order_stock_transactions.ORDER_NO IS NULL
								AND date(trn_orderheader_approvedby.dtApprovedDate) >= '$dateFrom'
								AND date(trn_orderheader_approvedby.dtApprovedDate) < '$dateTo'
								GROUP BY
									trn_po_prn_details_sales_order.ORDER_NO,
									trn_po_prn_details_sales_order.ORDER_YEAR,
									trn_po_prn_details_sales_order.SALES_ORDER,
									trn_po_prn_details_sales_order.ITEM
								HAVING
									(
										FIND_IN_SET('$companyId', FABRIC_IN_COMPANIES)
										OR ORDER_COMPANY = '$companyId'
									)
					) AS TB1
				GROUP BY
					TB1.ORDER_NO,
					TB1.ORDER_YEAR,
					TB1.SALES_ORDER,
					TB1.ITEM
			) AS TB2
		LEFT JOIN trn_po_prn_details_sales_order_stock_transactions ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = TB2.ORDER_NO
		AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = TB2.ORDER_YEAR
		AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = TB2.SALES_ORDER
		AND trn_po_prn_details_sales_order_stock_transactions.ITEM = TB2.ITEM
		INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = TB2.ORDER_NO
		AND trn_orderheader.intOrderYear = TB2.ORDER_YEAR
        INNER JOIN trn_orderheader_approvedby ON trn_orderheader_approvedby.intOrderNo = TB2.ORDER_NO
											AND trn_orderheader_approvedby.intYear = TB2.ORDER_YEAR
											AND trn_orderheader_approvedby.intStatus = 0
											AND trn_orderheader_approvedby.intApproveLevelNo = (
												trn_orderheader.intApproveLevelStart + 1 - trn_orderheader.intStatus
											)
		LEFT JOIN trn_po_prn_details_sales_order ON trn_po_prn_details_sales_order.ORDER_NO = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO
		AND trn_po_prn_details_sales_order.ORDER_YEAR = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
		AND trn_po_prn_details_sales_order.SALES_ORDER = trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER
		AND trn_po_prn_details_sales_order.ITEM = trn_po_prn_details_sales_order_stock_transactions.ITEM
		INNER JOIN trn_orderdetails ON TB2.ORDER_NO = trn_orderdetails.intOrderNo
			AND TB2.ORDER_YEAR = trn_orderdetails.intOrderYear
			AND TB2.SALES_ORDER = trn_orderdetails.intSalesOrderId
		INNER JOIN mst_item ON TB2.ITEM = mst_item.intId
		INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
		INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
		INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId	
		GROUP BY
			TB2.ORDER_NO,
			TB2.ORDER_YEAR,
			TB2.SALES_ORDER,
			TB2.ITEM,
			trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID
	) AS TB3
	GROUP BY  
	TB3.ORDER_NO,
	TB3.ORDER_YEAR, 
	TB3.ITEM 
 ) as tb
GROUP BY  
tb.ITEM 
) as TB_ALL   ";

    $sql .=" RIGHT JOIN 
(
SELECT 
	TB3.type,
	TB3.ORDER_COMPANY,
	TB3.location AS location,
    TB3.date,
	TB3.ITEM,
    GROUP_CONCAT(DISTINCT TB3.ORDER_NO,'/',TB3.ORDER_YEAR) as otherOrders,
	(select sum(trn_po_prn_details.REQUIRED) from trn_po_prn_details 
								where trn_po_prn_details.ORDER_NO=TB3.ORDER_NO 
								and trn_po_prn_details.ORDER_YEAR=TB3.ORDER_YEAR 
								and trn_po_prn_details.ITEM=TB3.ITEM) AS REQUIRED,
	TB3.TRANSACTION_LOCATION,
	(TB3.PO_QTY) as PO_QTY,
	(TB3.PRN_QTY) as PRN_QTY,
	TB3.mainCatName,
	TB3.subCatName,
	TB3.strCode,
	TB3.itemName,
	TB3.UOM,
	SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.MRN_QTY,0)) as MRN_QTY,
	SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.MRN_EXTRA_QTY,0)) as MRN_EXTRA_QTY,
	SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.ISSUE_QTY,0)) as ISSUE_QTY,
	SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.GP_QTY,0)) as GP_QTY,
	SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.GP_IN_QTY,0)) as GP_IN_QTY,
	SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.RET_TO_STORE_QTY,0)) as RET_TO_STORE_QTY,
	SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.MRN_QTY,0)) as MRN_QTY_OTHER_LOC,
	SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.MRN_EXTRA_QTY,0)) as MRN_EXTRA_QTY_OTHER_LOC,
	SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.ISSUE_QTY,0)) as ISSUE_QTY_OTHER_LOC,
	SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.GP_QTY,0)) as GP_QTY_OTHER_LOC,
	SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.GP_IN_QTY,0)) as GP_IN_QTY_OTHER_LOC,
	SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.RET_TO_STORE_QTY,0)) as RET_TO_STORE_QTY_OTHER_LOC
FROM
	(
		SELECT
			trn_orderheader.intLocationId AS ORDER_COMPANY,
			TB2.ORDER_NO,
			TB2.ORDER_YEAR,
			TB2.location,
			TB2.ITEM,
			TB2.SALES_ORDER,
			TB2.type,
			CONCAT (trn_orderdetails.strSalesOrderNo,'/',trn_orderdetails.strCombo,'/',trn_orderdetails.strPrintName) AS SALES_ORDER_String,
            GROUP_CONCAT(DISTINCT date(trn_orderheader_approvedby.dtApprovedDate ))AS date,
            mst_maincategory.strName AS mainCatName,
			mst_subcategory.strName AS subCatName,
			mst_item.strCode,
			mst_item.intId,
			mst_item.strName AS itemName,
			mst_units.strName AS UOM,
			(trn_po_prn_details_sales_order.REQUIRED) as REQUIRED,
			trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID AS TRANSACTION_LOCATION,
			
			(
				SELECT
					trn_po_prn_details.PURCHASED_QTY
				FROM
					trn_po_prn_details
				WHERE
					trn_po_prn_details.ORDER_NO = TB2.ORDER_NO
				AND trn_po_prn_details.ORDER_YEAR = TB2.ORDER_YEAR
				AND trn_po_prn_details.ITEM = TB2.ITEM
			) AS PO_QTY,
			(
				SELECT
					trn_po_prn_details.PRN_QTY
				FROM
					trn_po_prn_details
				WHERE
					trn_po_prn_details.ORDER_NO = TB2.ORDER_NO
				AND trn_po_prn_details.ORDER_YEAR = TB2.ORDER_YEAR
				AND trn_po_prn_details.ITEM = TB2.ITEM
			) AS PRN_QTY,
			SUM(

				IF (
					trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'MRN',
					trn_po_prn_details_sales_order_stock_transactions.QTY,
					0
				)
			) AS MRN_QTY,
			SUM(

				IF (
					trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'MRN_EXTRA',
					trn_po_prn_details_sales_order_stock_transactions.QTY,
					0
				)
			) AS MRN_EXTRA_QTY,
			SUM(

				IF (
					trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'ISSUE',
					trn_po_prn_details_sales_order_stock_transactions.QTY,
					0
				)
			) AS ISSUE_QTY,
			SUM(

				IF (
					trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'GATE_PASS',
					trn_po_prn_details_sales_order_stock_transactions.QTY,
					0
				)
			) AS GP_QTY,
			SUM(

				IF (
					trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'GATE_PASS_IN',
					trn_po_prn_details_sales_order_stock_transactions.QTY,
					0
				)
			) AS GP_IN_QTY,
			SUM(

				IF (
					trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'RETURNED_TO_STORES',
					trn_po_prn_details_sales_order_stock_transactions.QTY,
					0
				)
			) AS RET_TO_STORE_QTY
		FROM
			(
				SELECT
					*
				FROM
					(
						SELECT
							trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
							trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
							trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER,
							trn_po_prn_details_sales_order_stock_transactions.ITEM,
							mst_locations.intCompanyId AS ORDER_COMPANY,
							mst_locations.strName AS location,
							(
								SELECT
									GROUP_CONCAT(mst_locations.intCompanyId)
								FROM
									ware_fabricreceivedheader
								INNER JOIN mst_locations ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId
								WHERE
									ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO
								AND ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
							) AS FABRIC_IN_COMPANIES,
							1 AS type
						FROM
							trn_po_prn_details_sales_order_stock_transactions
						INNER JOIN trn_po_prn_details_sales_order ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = trn_po_prn_details_sales_order.ORDER_NO
						AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR
						AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER
						AND trn_po_prn_details_sales_order_stock_transactions.ITEM = trn_po_prn_details_sales_order.ITEM
						INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO
						AND trn_orderheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
						INNER JOIN mst_locations ON mst_locations.intId = trn_orderheader.intLocationId
								INNER JOIN trn_orderheader_approvedby ON trn_orderheader_approvedby.intOrderNo = trn_orderheader.intOrderNo
								AND trn_orderheader_approvedby.intYear = trn_orderheader.intOrderYear
								AND trn_orderheader_approvedby.intStatus = 0
								AND trn_orderheader_approvedby.intApproveLevelNo = 1
								AND date(trn_orderheader_approvedby.dtApprovedDate) = '$dateTo' 
						GROUP BY
							trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
							trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
							trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER,
							trn_po_prn_details_sales_order_stock_transactions.ITEM
						HAVING
							FIND_IN_SET('$companyId', FABRIC_IN_COMPANIES)
						OR ORDER_COMPANY = '$companyId'
						UNION ALL
							SELECT
								trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
								trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
								trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER,
								trn_po_prn_details_sales_order_stock_transactions.ITEM,
								mst_locations.intCompanyId AS ORDER_COMPANY,
								mst_locations.strName AS location,
								
								(
									SELECT
										GROUP_CONCAT(
											DISTINCT mst_locations.intCompanyId
										)
									FROM
										ware_fabricreceivedheader
									INNER JOIN mst_locations ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId
									WHERE
										ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO
									AND ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
								) AS FABRIC_IN_COMPANIES,
								2 AS type
							FROM
								trn_po_prn_details_sales_order_stock_transactions
							LEFT JOIN trn_po_prn_details_sales_order ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = trn_po_prn_details_sales_order.ORDER_NO
							AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR
							AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER
							AND trn_po_prn_details_sales_order_stock_transactions.ITEM = trn_po_prn_details_sales_order.ITEM
							INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO
							AND trn_orderheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
							INNER JOIN mst_locations ON mst_locations.intId = trn_orderheader.intLocationId
								INNER JOIN trn_orderheader_approvedby ON trn_orderheader_approvedby.intOrderNo = trn_orderheader.intOrderNo
								AND trn_orderheader_approvedby.intYear = trn_orderheader.intOrderYear
								AND trn_orderheader_approvedby.intStatus = 0
								AND trn_orderheader_approvedby.intApproveLevelNo = 1
										
							WHERE
								trn_po_prn_details_sales_order.ORDER_NO IS NULL
								AND date(trn_orderheader_approvedby.dtApprovedDate) = '$dateTo' 
							GROUP BY
								trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
								trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
								trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER,
								trn_po_prn_details_sales_order_stock_transactions.ITEM
							HAVING
								(
									FIND_IN_SET('$companyId', FABRIC_IN_COMPANIES)
									OR ORDER_COMPANY = '$companyId'
								)
							UNION ALL
								SELECT
									trn_po_prn_details_sales_order.ORDER_NO,
									trn_po_prn_details_sales_order.ORDER_YEAR,
									trn_po_prn_details_sales_order.SALES_ORDER,
									trn_po_prn_details_sales_order.ITEM,
									mst_locations.intCompanyId AS ORDER_COMPANY,
									mst_locations.strName AS location,
									(
										SELECT
											GROUP_CONCAT(
												DISTINCT mst_locations.intCompanyId
											)
										FROM
											ware_fabricreceivedheader
										INNER JOIN mst_locations ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId
										WHERE
											ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order.ORDER_NO
										AND ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order.ORDER_YEAR
									) AS FABRIC_IN_COMPANIES,
									1 AS type
								FROM
									trn_po_prn_details_sales_order_stock_transactions
								RIGHT JOIN trn_po_prn_details_sales_order ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = trn_po_prn_details_sales_order.ORDER_NO
								AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR
								AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER
								AND trn_po_prn_details_sales_order_stock_transactions.ITEM = trn_po_prn_details_sales_order.ITEM
								INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = trn_po_prn_details_sales_order.ORDER_NO
								AND trn_orderheader.intOrderYear = trn_po_prn_details_sales_order.ORDER_YEAR
								INNER JOIN mst_locations ON mst_locations.intId = trn_orderheader.intLocationId
								INNER JOIN trn_orderheader_approvedby ON trn_orderheader_approvedby.intOrderNo = trn_orderheader.intOrderNo
								AND trn_orderheader_approvedby.intYear = trn_orderheader.intOrderYear
								AND trn_orderheader_approvedby.intStatus = 0
								AND trn_orderheader_approvedby.intApproveLevelNo = 1
								WHERE
								 trn_po_prn_details_sales_order_stock_transactions.ORDER_NO IS NULL
								AND date(trn_orderheader_approvedby.dtApprovedDate) = '$dateTo' 
								GROUP BY
									trn_po_prn_details_sales_order.ORDER_NO,
									trn_po_prn_details_sales_order.ORDER_YEAR,
									trn_po_prn_details_sales_order.SALES_ORDER,
									trn_po_prn_details_sales_order.ITEM
								HAVING
									(
										FIND_IN_SET('$companyId', FABRIC_IN_COMPANIES)
										OR ORDER_COMPANY = '$companyId'
									)
					) AS TB1
				GROUP BY
					TB1.ORDER_NO,
					TB1.ORDER_YEAR,
					TB1.SALES_ORDER,
					TB1.ITEM
			) AS TB2
		LEFT JOIN trn_po_prn_details_sales_order_stock_transactions ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = TB2.ORDER_NO
		AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = TB2.ORDER_YEAR
		AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = TB2.SALES_ORDER
		AND trn_po_prn_details_sales_order_stock_transactions.ITEM = TB2.ITEM
		INNER JOIN trn_orderheader ON trn_orderheader.intOrderNo = TB2.ORDER_NO
		AND trn_orderheader.intOrderYear = TB2.ORDER_YEAR
        INNER JOIN trn_orderheader_approvedby ON trn_orderheader_approvedby.intOrderNo = TB2.ORDER_NO
											AND trn_orderheader_approvedby.intYear = TB2.ORDER_YEAR
											AND trn_orderheader_approvedby.intStatus = 0
											AND trn_orderheader_approvedby.intApproveLevelNo = (
												trn_orderheader.intApproveLevelStart + 1 - trn_orderheader.intStatus
											)
		LEFT JOIN trn_po_prn_details_sales_order ON trn_po_prn_details_sales_order.ORDER_NO = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO
		AND trn_po_prn_details_sales_order.ORDER_YEAR = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
		AND trn_po_prn_details_sales_order.SALES_ORDER = trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER
		AND trn_po_prn_details_sales_order.ITEM = trn_po_prn_details_sales_order_stock_transactions.ITEM
		LEFT JOIN trn_orderdetails ON TB2.ORDER_NO = trn_orderdetails.intOrderNo
			AND TB2.ORDER_YEAR = trn_orderdetails.intOrderYear
			AND TB2.SALES_ORDER = trn_orderdetails.intSalesOrderId
		INNER JOIN mst_item ON TB2.ITEM = mst_item.intId
		INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
		INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
		INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId	
		GROUP BY
			TB2.ORDER_NO,
			TB2.ORDER_YEAR,
			TB2.SALES_ORDER,
			TB2.ITEM,
			trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID
	) AS TB3
GROUP BY 
	TB3.ORDER_NO,
	TB3.ORDER_YEAR,
	TB3.ITEM 
)as TB_TODATE 
ON 
TB_TODATE.ITEM = TB_ALL.ITEM";
//echo $sql; die;
    return $sql;
}



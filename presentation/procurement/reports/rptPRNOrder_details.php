<?php (define('UNLOCKPAGE', true) ? die('<<< Access denied >>>') : ''); ?>
<?php
$companyId = $sessions->getCompanyId();
$userId = $sessions->getUserId();
$locationId = $sessions->getLocationId();

$programCode = 'P0427';

require_once "class/cls_permisions.php";
$objpermisionget = new cls_permisions($db);
require_once "class/tables/sys_approvelevels.php";
$obj_sys_approvelevels = new sys_approvelevels($db);

$permi_price_view = $objpermisionget->boolSPermision(8);
$str_price_view = 'hidden';

if ($permi_price_view)
    $str_price_view = '';

//Get orderNo, Order year and revision No
$orderNo = $_REQUEST['orderNo'];
$orderYear = $_REQUEST['orderYear'];

$approveMode = (!isset($_REQUEST["approveMode"]) ? '' : $_REQUEST["approveMode"]);
$revise = (!isset($_REQUEST["revise"]) ? '' : $_REQUEST["revise"]);


                $sql = "SELECT
					trn_orderheader.intOrderNo AS orderNo,
					trn_orderheader.intOrderYear AS orderYear, 
					trn_orderheader.strCustomerPoNo AS customerPO,
					mst_customer.strName AS strCustomer,
					trn_orderheader.dtDate AS dtDate,
					trn_orderheader.intReviseNo AS RevisionNO,
					C.strUserName AS creator,
					trn_orderheader.dtDeliveryDate AS dtDeliveryDate,
					A.strUserName AS marketer,
trn_orderheader.DUMMY_PO_NO,
trn_orderheader.DUMMY_PO_YEAR
				FROM
					trn_orderheader
					Inner Join mst_customer ON mst_customer.intId = trn_orderheader.intCustomer
					Inner Join sys_users AS A ON A.intUserId = trn_orderheader.intMarketer
					Inner Join sys_users AS C ON C.intUserId = trn_orderheader.intCreator		
				WHERE
					trn_orderheader.intOrderNo 		=  '$orderNo' AND
					trn_orderheader.intOrderYear 	=  '$orderYear' 
					";

$result = $db->RunQuery($sql);

$row = mysqli_fetch_array($result);
$orderNo = $row['orderNo'];
$orderYear = $row['orderYear'];
$strCustomerPoNo = $row['customerPO'];
$strCustomer = $row['strCustomer'];
$dtDate = $row['dtDate'];
$dtDeliveryDate = $row['dtDeliveryDate'];
$marketer = $row['marketer'];
$creator = $row['creator'];
$reviseNo = $row['RevisionNO'];
$search_no = $row['DUMMY_PO_NO'];
$search_year = $row['DUMMY_PO_YEAR'];
if($search_no==''){
$search_no = $row['orderNo'];
$search_year = $row['orderYear'];	
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Orderwise PRN Report</title>
    <script type="text/javascript"
            src="presentation/customerAndOperation/bulk/placeOrder/listing/rptBulkOrder-js.js?n=1"></script>
    <style>
        .break {
            page-break-before: always;
        }

        @media print {
            .noPrint {
                display: none;
            }
        }

        #apDiv1 {
            position: absolute;
            left: 237px;
            top: 175px;
            width: 619px;
            height: 235px;
            z-index: 1;
        }

        .APPROVE {
            font-size: 16px;
            font-weight: bold;
            color: #00C;
        }
    </style>
</head>

<body>
<form id="frmSamplePlaceOrderReport" name="frmSamplePlaceOrderReport" method="post" action="rptBulkOrder.php">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="3"></td>
        </tr>
        <tr>
            <td width="20%"></td>
            <td width="60%" height="80"
                valign="top"><?php include 'presentation/procurement/reports/reportHeader.php' ?></td>
            <td width="20%"></td>
        </tr>
        <tr>
            <td colspan="3">
                <div id="divPoNo" style="display:none"><?php echo $orderNo ?>/<?php echo $orderYear ?></div>
            </td>
        </tr>
    </table>
    <div align="center">
        <div style="background-color:#FFF"><strong>Orderwise PRN Report</strong><strong></strong></div>
        <table width="900" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td colspan="10" align="center" bgcolor="#FFFFFF">
                            </td>
                        </tr>
                        <tr>
                            <td width="1%">&nbsp;</td>
                            <td width="9%" class="normalfnt">Order No</td>
                            <td width="1%" align="center" valign="middle">:</td>
                            <td width="17%"><span class="normalfnt"><?php echo $orderNo; ?>/<?php echo $orderYear; ?></span></td>
                            <td width="10%" class="normalfnt">Date</td>
                            <td width="1%" align="center" valign="middle">:</td>
                            <td width="18%"><span class="normalfnt"><?php echo $dtDate; ?></span></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td class="normalfnt">Customer</td>
                            <td align="center" valign="middle">:</td>
                            <td><span class="normalfnt"><?php echo $strCustomer; ?></span></td>
                            <td width="10%" class="normalfnt">Enter BY</td>
                            <td width="1%" align="center" valign="middle">:</td>
                            <td width="18%"><span class="normalfnt"><?php echo $creator; ?></span></td>
                        </tr>

                        <tr>
                            <td>&nbsp;</td>
                            <td class="normalfnt">Cus PO No</td>
                            <td align="center" valign="middle">:</td>
                            <td class="normalfnt"><?php echo $strCustomerPoNo; ?></td>
                            <td><span class="normalfnt">Marketer</span></td>
                            <td align="center" valign="middle">:</td>
                            <td><span class="normalfnt"><?php echo $marketer; ?></span></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td class="normalfnt">Bulk Revise No</td>
                            <td align="center" valign="middle">:</td>
                            <td class="normalfnt"><?php echo $reviseNo; ?></td>
                            <td colspan="2" class="normalfnt">&nbsp;</td>
                            <td>&nbsp;</td>

                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr><td valign="bottom" height="30"><b>CONSUMPTION DETAILS - S/O WISE</b></td></tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td colspan="7" class="normalfnt">
                                <table width="806" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
                                    <tr class="">
                                      <th width="8%">Order Type</th>
                                      <th width="8%">Order No</th>
                                      <th width="8%">Sales Order</th>
                                      <th width="8%">Main category</th>
                                      <th width="8%">Sub Category</th>
                                      <th width="8%">UOM</th>
                                        <th width="8%" height="22">Item</th>
                                        <th width="13%">Consumption</th>
                                        <th width="13%">Order Qty+excess</th>
                                        <th width="18%">Production Required</th>
                                    </tr>
                                  <?php
                                  $sql1 = "SELECT
if(trn_orderheader.PO_TYPE=1,'DUMMY',if(trn_orderheader.PO_TYPE=2,'ACTUAL','OTHER')) AS type,
trn_po_prn_details_sales_order.ORDER_NO,
trn_po_prn_details_sales_order.ORDER_YEAR,
trn_po_prn_details_sales_order.SALES_ORDER,
mst_maincategory.strName as mc,
mst_subcategory.strName as sc,
mst_units.strName as uom,
mst_item.strName as item,
trn_po_prn_details_sales_order.CONS_PC,
trn_po_prn_details_sales_order.PRODUCTION_QTY,
trn_po_prn_details_sales_order.REQUIRED,
trn_orderheader.DUMMY_PO_NO,
trn_orderheader.DUMMY_PO_YEAR,
trn_orderdetails.strSalesOrderNo
FROM
trn_po_prn_details_sales_order
INNER JOIN mst_item ON trn_po_prn_details_sales_order.ITEM = mst_item.intId
INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
INNER JOIN trn_orderheader ON trn_po_prn_details_sales_order.ORDER_NO = trn_orderheader.intOrderNo AND trn_po_prn_details_sales_order.ORDER_YEAR = trn_orderheader.intOrderYear
INNER JOIN trn_orderdetails ON trn_po_prn_details_sales_order.ORDER_NO = trn_orderdetails.intOrderNo AND trn_po_prn_details_sales_order.ORDER_YEAR = trn_orderdetails.intOrderYear AND trn_po_prn_details_sales_order.SALES_ORDER = trn_orderdetails.intSalesOrderId
where (trn_orderheader.intOrderNo = '$search_no' and trn_orderheader.intOrderYear='$search_year') or (trn_orderheader.DUMMY_PO_NO ='$search_no' and trn_orderheader.DUMMY_PO_YEAR='$search_year')

 
	        		  	";
                                    $result1 = $db->RunQuery($sql1);
                                    $Previouse = "";
                                    $current = "";
                                    $i = 0;
                                    while ($row = mysqli_fetch_array($result1)) {
										$current = $row['ORDER_NO'].'/'.$row['ORDER_YEAR'].'/'.$row['SALES_ORDER'];
                                        ?>
                                        	<tr>
                                            <?php
											if($current !=$Previouse){
											?>
                                            <td class="normalfnt" NOWRAP style="text-align: center">&nbsp;<?php echo $row['type']; ?>&nbsp;</td>
                                            <td class="normalfnt" style="text-align: center">&nbsp;<?php echo $row['ORDER_NO']; ?>/<?php echo $row['ORDER_YEAR']; ?>&nbsp;</td>
                                            <td class="normalfnt" style="text-align: center">&nbsp;<?php echo $row['strSalesOrderNo']; ?>&nbsp;</td>
											<?php
											}
											else{
											?>
                                            <td class="normalfnt" NOWRAP style="text-align: center">&nbsp;</td>
                                            <td class="normalfnt" style="text-align: center">&nbsp;</td>
                                            <td class="normalfnt" style="text-align: center">&nbsp;</td>
											<?php
											}
 											?>
                                            <td class="normalfnt" NOWRAP style="text-align: center">&nbsp;<?php echo $row['mc']; ?>&nbsp;</td>
                                            <td class="normalfnt" NOWRAP style="text-align: center">&nbsp;<?php echo $row['sc']; ?>&nbsp;</td>
                                            <td class="normalfnt" NOWRAP style="text-align: center">&nbsp;<?php echo $row['uom']; ?>&nbsp;</td>
                                            <td class="normalfnt" NOWRAP style="text-align: center">&nbsp;<?php echo $row['item']; ?>&nbsp;</td>
                                            <td class="normalfnt" NOWRAP style="text-align: right">&nbsp;<?php echo $row['CONS_PC']; ?>&nbsp;</td>
                                            <td class="normalfnt" NOWRAP style="text-align: right">&nbsp;<?php echo $row['PRODUCTION_QTY']; ?>&nbsp;</td>
                                            <td class="normalfnt" NOWRAP style="text-align: right">&nbsp;<?php echo $row['REQUIRED']; ?>&nbsp;</td>
                                          </tr>  
                                     <?php
									 $Previouse = $row['ORDER_NO'].'/'.$row['ORDER_YEAR'].'/'.$row['SALES_ORDER'];
									  }
                                    ?>
                                    <tr style="height: 40px">
                                        <td colspan="20"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="20">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                            <td width="6%">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
              <td valign="bottom" height="30"><b>PRN SUMMARY - ORDER WISE</b></td></tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td colspan="7" class="normalfnt">
                                <table width="806" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
                                    <tr class="">
                                      <th width="8%">Order Type</th>
                                      <th width="8%">Order No</th>
                                      <th width="8%">Main category</th>
                                      <th width="8%">Sub Category</th>
                                      <th width="8%">UOM</th>
                                        <th width="8%" height="22">Item</th>
                                        <th width="18%">Stock</th>
                                        <th width="18%">Production Required</th>
                                        <th width="18%">Balanced From Dummy</th>
                                        <th width="18%">Used by Other orders</th>
                                        <th width="18%">Active PRN Qty</th>
                                        <th width="18%">PO Qty</th>
                                        <th width="18%">Active PRN</th>
                                        <th width="18%">Cancelled PRN</th>
                                        <th width="18%">Cancelled PRN Qty</th>
                                    </tr>
                                  <?php
                                  $sql1 = "SELECT
if(trn_orderheader.PO_TYPE=1,'DUMMY',if(trn_orderheader.PO_TYPE=2,'ACTUAL','OTHER')) AS type,
trn_po_prn_details.ORDER_NO,
trn_po_prn_details.ORDER_YEAR,
mst_maincategory.strName AS mc,
mst_subcategory.strName AS sc,
mst_units.strName AS uom,
mst_item.intId as item_id,
mst_item.strName AS item,
trn_po_prn_details.REQUIRED,
trn_po_prn_details.PRN_QTY,
trn_po_prn_details.PURCHASED_QTY,
trn_orderheader.DUMMY_PO_NO,
trn_orderheader.DUMMY_PO_YEAR,
trn_po_prn_details.PRN_QTY_USED_OTHERS,
trn_po_prn_details.PRN_QTY_BALANCED_FROM_DUMMY,
(select GROUP_CONCAT(trn_prnheader.intPrnNo,'/',trn_prnheader.intYear) from trn_prnheader where trn_prnheader.intOrderNo=trn_po_prn_details.ORDER_NO and trn_po_prn_details.ORDER_YEAR=trn_prnheader.intOrderYear and trn_prnheader.intStatus=1) as prn_app, 
(select GROUP_CONCAT(trn_prnheader.intPrnNo,'/',trn_prnheader.intYear) from trn_prnheader where trn_prnheader.intOrderNo=trn_po_prn_details.ORDER_NO and trn_po_prn_details.ORDER_YEAR=trn_prnheader.intOrderYear and trn_prnheader.intStatus=-10) as prn_canc,  
(select sum(trn_prndetails.dblPrnQty) from trn_prnheader
INNER JOIN trn_prndetails on trn_prnheader.intPrnNo = trn_prndetails.intPrnNo and trn_prnheader.intYear = trn_prndetails.intYear 
 where trn_prnheader.intOrderNo=trn_po_prn_details.ORDER_NO 
and trn_po_prn_details.ORDER_YEAR=trn_prnheader.intOrderYear 
and trn_prndetails.intItem = trn_po_prn_details.ITEM
and trn_prnheader.intStatus=-10) as prn_canc_qty 
FROM
trn_po_prn_details
INNER JOIN mst_item ON trn_po_prn_details.ITEM = mst_item.intId
INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
INNER JOIN trn_orderheader ON trn_po_prn_details.ORDER_NO = trn_orderheader.intOrderNo 
AND trn_po_prn_details.ORDER_YEAR = trn_orderheader.intOrderYear
where (trn_orderheader.intOrderNo = '$search_no' and trn_orderheader.intOrderYear='$search_year') or (trn_orderheader.DUMMY_PO_NO ='$search_no' and trn_orderheader.DUMMY_PO_YEAR='$search_year')

 
	        		  	";
                                    $result1 = $db->RunQuery($sql1);
                                    $Previouse = "";
                                    $current = "";
                                    $i = 0;
                                    while ($row = mysqli_fetch_array($result1)) {
										$current = $row['ORDER_NO'].'/'.$row['ORDER_YEAR'];
											$sql3 = "SELECT SUM(ROUND(ware_stocktransactions_bulk.dblQty,6)) AS stockQuantity
                                            FROM ware_stocktransactions_bulk
                                            WHERE ware_stocktransactions_bulk.intItemId = ".$row['item_id']." 
                                            AND ware_stocktransactions_bulk.intLocationId = $locationId";
                                            $result3 = $db->RunQuery($sql3);
											$row3 = mysqli_fetch_array($result3)
									    ?>
                                        	<tr>
                                            <?php
											if($current !=$Previouse){
											?>
                                            <td class="normalfnt" NOWRAP style="text-align: center">&nbsp;<?php echo $row['type']; ?>&nbsp;</td>
                                            <td class="normalfnt" style="text-align: center">&nbsp;<?php echo $row['ORDER_NO']; ?>/<?php echo $row['ORDER_YEAR']; ?>&nbsp;</td>
											<?php
											}
											else{
											?>
                                            <td class="normalfnt" style="text-align: center">&nbsp;</td>
                                            <td class="normalfnt" style="text-align: center">&nbsp;</td>
											<?php
											}
 											?>
                                             <td class="normalfnt" NOWRAP style="text-align: center">&nbsp;<?php echo $row['mc']; ?>&nbsp;</td>
                                            <td class="normalfnt" NOWRAP style="text-align: center">&nbsp;<?php echo $row['sc']; ?>&nbsp;</td>
                                            <td class="normalfnt" NOWRAP style="text-align: right">&nbsp;<?php echo $row['uom']; ?>&nbsp;</td>
                                            <td class="normalfnt" NOWRAP style="text-align: center">&nbsp;<?php echo $row['item']; ?>&nbsp;</td>
                                            <td class="normalfnt" NOWRAP style="text-align: right">&nbsp;<?php echo $row3['stockQuantity']; ?></td>
                                            <td class="normalfnt" NOWRAP style="text-align: right">&nbsp;<?php echo $row['REQUIRED']; ?>&nbsp;</td>
                                            <td class="normalfnt" NOWRAP style="text-align: right">&nbsp;<?php echo $row['PRN_QTY_BALANCED_FROM_DUMMY']; ?>&nbsp;</td>
                                            <td class="normalfnt" NOWRAP style="text-align: right">&nbsp;<?php echo $row['PRN_QTY_USED_OTHERS']; ?>&nbsp;</td>
                                            <td class="normalfnt" NOWRAP style="text-align: right">&nbsp;<?php echo $row['PRN_QTY']; ?>&nbsp;</td>
                                            <td class="normalfnt" NOWRAP style="text-align: right">&nbsp;<?php echo $row['PURCHASED_QTY']; ?>&nbsp;</td>
                                            <td class="normalfnt" NOWRAP style="text-align: center">&nbsp;<?php echo $row['prn_app']; ?>&nbsp;</td>
                                            <td class="normalfnt" NOWRAP style="text-align: center">&nbsp;<?php echo $row['prn_canc']; ?>&nbsp;</td>
                                            <td class="normalfnt" NOWRAP style="text-align: right">&nbsp;<?php echo $row['prn_canc_qty']; ?>&nbsp;</td>
                                          </tr>  
                                     <?php
									 $Previouse = $row['ORDER_NO'].'/'.$row['ORDER_YEAR'];
									  }
                                    ?>
                                    <tr style="height: 40px">
                                        <td colspan="20"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="20">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                            <td width="6%">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
  
            <tr>
               <td valign="bottom" height="30">					<b>PRN SUMMARY - TOTAL ORDER</b></td></tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td colspan="7" class="normalfnt">
                                <table width="806" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
                                    <tr class="">
                                      <th width="7%">Main category</th>
                                      <th width="8%">Sub Category</th>
                                      <th width="5%">UOM</th>
                                        <th width="4%" height="22">Item</th>
                                        <th width="9%">Stock</th>
                                        <th width="9%">Production Required</th>
                                        <th width="9%">Active PRN Qty</th>
                                        <th width="10%">Active PRN Qty-must be Cleared</th>
                                        <th width="8%">PO Qty</th>
                                        <th width="9%">Active PRN</th>
                                        <th width="9%">Cancelled PRN</th>
                                        <th width="13%">Cancelled PRN Qty</th>
                                    </tr>
                                  <?php
                                  $sql1 = "SELECT
mst_maincategory.strName AS mc,
mst_subcategory.strName AS sc,
mst_units.strName AS uom,
mst_item.intId as item_id,
mst_item.strName AS item,
SUM(trn_po_prn_details.REQUIRED) AS REQUIRED,
SUM(trn_po_prn_details.PRN_QTY) AS PRN_QTY,
SUM(trn_po_prn_details.PURCHASED_QTY) AS PURCHASED_QTY,
trn_orderheader.DUMMY_PO_NO,
trn_orderheader.DUMMY_PO_YEAR,
SUM(trn_po_prn_details.PRN_QTY_USED_OTHERS) AS PRN_QTY_USED_OTHERS ,
SUM(trn_po_prn_details.PRN_QTY_BALANCED_FROM_DUMMY) AS PRN_QTY_BALANCED_FROM_DUMMY,
(select GROUP_CONCAT(trn_prnheader.intPrnNo,'/',trn_prnheader.intYear) from trn_prnheader where trn_prnheader.intOrderNo=trn_po_prn_details.ORDER_NO and trn_po_prn_details.ORDER_YEAR=trn_prnheader.intOrderYear and trn_prnheader.intStatus=1) as prn_app, 
(select GROUP_CONCAT(trn_prnheader.intPrnNo,'/',trn_prnheader.intYear) from trn_prnheader where trn_prnheader.intOrderNo=trn_po_prn_details.ORDER_NO and trn_po_prn_details.ORDER_YEAR=trn_prnheader.intOrderYear and trn_prnheader.intStatus=-10) as prn_canc,  
(select sum(trn_prndetails.dblPrnQty) from trn_prnheader
INNER JOIN trn_prndetails on trn_prnheader.intPrnNo = trn_prndetails.intPrnNo and trn_prnheader.intYear = trn_prndetails.intYear 
 where trn_prnheader.intOrderNo=trn_po_prn_details.ORDER_NO 
and trn_po_prn_details.ORDER_YEAR=trn_prnheader.intOrderYear 
and trn_prndetails.intItem = trn_po_prn_details.ITEM
and trn_prnheader.intStatus=-10) as prn_canc_qty 
FROM
trn_po_prn_details
INNER JOIN mst_item ON trn_po_prn_details.ITEM = mst_item.intId
INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
INNER JOIN trn_orderheader ON trn_po_prn_details.ORDER_NO = trn_orderheader.intOrderNo 
AND trn_po_prn_details.ORDER_YEAR = trn_orderheader.intOrderYear
where (trn_orderheader.intOrderNo = '$search_no' and trn_orderheader.intOrderYear='$search_year') or (trn_orderheader.DUMMY_PO_NO ='$search_no' and trn_orderheader.DUMMY_PO_YEAR='$search_year')
GROUP BY trn_po_prn_details.ITEM

 
	        		  	";
                                    $result1 = $db->RunQuery($sql1);
                                    $Previouse = "";
                                    $current = "";
                                    $i = 0;
                                    while ($row = mysqli_fetch_array($result1)) {
										$current = $row['ORDER_NO'].'/'.$row['ORDER_YEAR'];
											$sql3 = "SELECT SUM(ROUND(ware_stocktransactions_bulk.dblQty,6)) AS stockQuantity
                                            FROM ware_stocktransactions_bulk
                                            WHERE ware_stocktransactions_bulk.intItemId = ".$row['item_id']." 
                                            AND ware_stocktransactions_bulk.intLocationId = $locationId";
                                            $result3 = $db->RunQuery($sql3);
											$row3 = mysqli_fetch_array($result3)
									    ?>
                                        	<tr>
                                             <td class="normalfnt" NOWRAP style="text-align: center">&nbsp;<?php echo $row['mc']; ?>&nbsp;</td>
                                            <td class="normalfnt" NOWRAP style="text-align: center">&nbsp;<?php echo $row['sc']; ?>&nbsp;</td>
                                            <td class="normalfnt" NOWRAP style="text-align: right">&nbsp;<?php echo $row['uom']; ?>&nbsp;</td>
                                            <td class="normalfnt" NOWRAP style="text-align: center">&nbsp;<?php echo $row['item']; ?>&nbsp;</td>
                                            <td class="normalfnt" NOWRAP style="text-align: right">&nbsp;<?php echo $row3['stockQuantity']; ?></td>
                                            <td class="normalfnt" NOWRAP style="text-align: right" bgcolor="#5DD087">&nbsp;<?php echo $row['REQUIRED']; ?>&nbsp;</td>
                                            <td class="normalfnt" NOWRAP style="text-align: right" bgcolor="<?php if(round(($row['PRN_QTY']-$row['REQUIRED']),6) <=0){ echo '#5DD087'; } else{ echo '#F2898B';}?>">&nbsp;<?php echo $row['PRN_QTY']; ?>&nbsp;</td>
                                            <td class="normalfnt" NOWRAP style="text-align: right" bgcolor="<?php if(round(($row['PRN_QTY']-$row['REQUIRED']),6) <=0){ echo '#5DD087'; } else{ echo '#F2898B';}?>"><?php echo round(($row['PRN_QTY']-$row['REQUIRED']),6); ?></td>
                                            <td class="normalfnt" NOWRAP style="text-align: right" bgcolor="#5DD087">&nbsp;<?php echo $row['PURCHASED_QTY']; ?>&nbsp;</td>
                                            <td class="normalfnt" NOWRAP style="text-align: center">&nbsp;<?php echo $row['prn_app']; ?>&nbsp;</td>
                                            <td class="normalfnt" NOWRAP style="text-align: center">&nbsp;<?php echo $row['prn_canc']; ?>&nbsp;</td>
                                            <td class="normalfnt" NOWRAP style="text-align: right">&nbsp;<?php echo $row['prn_canc_qty']; ?>&nbsp;</td>
                                          </tr>  
                                     <?php
									 $Previouse = $row['ORDER_NO'].'/'.$row['ORDER_YEAR'];
									  }
                                    ?>
                                    <tr style="height: 40px">
                                        <td colspan="17"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="17">&nbsp;</td>
                                    </tr>
                                </table>
                            </td>
                            <td width="6%">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
  
            <tr>
            
</form>
</body>
</html>


<?php
ini_set('display_errors',0) ;
set_time_limit(600);
session_start();
//echo 'Time Limit = ' . ini_get('max_execution_time') .

$companyId 			= $sessions->getCompanyId();
$userId 			= $sessions->getUserId();
$locationId	 		= $sessions->getLocationId();
//$head_office_loc	= get_ho_loc($companyId);
$programCode 		= 'P0929';

require_once "class/cls_permisions.php";
$objpermisionget = new cls_permisions($db);
require_once "class/tables/sys_approvelevels.php";
$obj_sys_approvelevels = new sys_approvelevels($db);

$permi_price_view = $objpermisionget->boolSPermision(8);
$str_price_view = 'hidden';

if ($permi_price_view)
    $str_price_view = '';
	
$company 			= $_REQUEST['company'];
$maincategory  		= $_REQUEST['maincategory'];
$subcategory  		= $_REQUEST['subcategory'];
$item   			= $_REQUEST['item'];
$BalToPurchase   	= $_REQUEST['BalToPurchase'];
$mrn  				= $_REQUEST['mrn'];
$curYear 			= date('Y');
$head_office_loc	= get_ho_loc($company);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Item Wise Stock Order Forecast Report</title>
    <script type="text/javascript"
            src="presentation/customerAndOperation/bulk/placeOrder/listing/rptBulkOrder-js.js?n=1"></script>
    <style>
        .break {
            page-break-before: always;
        }

        @media print {
            .noPrint {
                display: none;
            }
        }

        #apDiv1 {
            position: absolute;
            left: 237px;
            top: 175px;
            width: 619px;
            height: 235px;
            z-index: 1;
        }

        .APPROVE {
            font-size: 16px;
            font-weight: bold;
            color: #00C;
        }
		
		.foo {
            float: left;
            width: 20px;
            height: 20px;
            margin: 1px;
            border: 1px solid rgba(0, 0, 0, .2);
        }

        .blue {
            background: #71ede4;
        }
        .red {
            background: #FF99CC;
        }
		 div.dataTables_length {
            padding-left: 2em;
        }
        div.dataTables_length,
        div.dataTables_filter {
            padding-top: 0.55em;
        }
        div.dt-buttons{
            position:relative !important;
            float:left !important;
        }
        #tblMainGrid_paginate{
            position: absolute;
            top: -87px;
            right: 837px;
            width: 436px;
            height: 100px;
        }

        .type_2 {
            background-color:  #FF99CC !important;
        }
       
		 .stock {
            background-color:  #71ede4 !important;
        }
		
		.dataTables_paginate {
   	 		margin-top: 15px;
    		position: absolute;
    		text-align: right;
    		left: 30%;
		}
    </style>
    
    
	 <script src="libraries/dataTables/jquery.dataTables.min.js"></script>
    <script src="libraries/dataTables/dataTables.buttons.min.js"></script>
    <script src="libraries/dataTables/buttons.flash.min.js"></script>
    <script src="libraries/dataTables/jszip.min.js"></script>
    <script src="libraries/dataTables/pdfmake.min.js"></script>
    <script src="libraries/dataTables/vfs_fonts.js"></script>
    <script src="libraries/dataTables/buttons.html5.min.js"></script>
    <script src="libraries/dataTables/buttons.print.min.js "></script>
    <script type="text/javascript" src="presentation/procurement/reports/rptItemWiseStock.js"></script>
    <link rel="stylesheet" type="text/css" href="libraries/dataTables/css/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="libraries/dataTables/css/buttons.dataTables.min.css">
     
    <script type="text/javascript">
	
	$(document).ready(function() {
		  
          var table =$('#tblMainGrid').DataTable( {
			  
			  
              "bFilter": false,
              pageLength: 50,
              responsive: true,
              "scrollY": 600,
              "scrollX": true,
              dom: 'Bfrtip',
              buttons: [{
                  extend: 'print',
                  text: 'Print',
                  exportOptions: {
                      columns: ':not(.no-print)'
                  },
                  footer: true,
                  autoPrint: false
              }, {

                  extend: 'pdfHtml5',
                  messageTop: 'ITEM WISE STOCK ORDER FORECAST REPORT',
                  orientation: 'landscape',
                  pageSize: 'LEGAL',
                  text: 'PDF',


              },{

                  extend: 'excel',
                  messageTop: 'ITEM WISE STOCK ORDER FORECAST REPORT',
                  text: 'Excel',

              },
                  {

                      extend: 'csv',
                      text: 'CSV',
                      customize: function (csv) {

                          return '\t ITEM WISE STOCK ORDER FORECAST REPORT\n'+  csv;
                      },
                      exportOptions: {
                          columns: ':not(.no-print)'
                      }


                  }],
              "createdRow": function( row, data, dataIndex ) {
				 // alert(data[0]);
                  
				  if ( data[12].replace(/[\$,]/g, '') * 1  < data[15] ) {
                     
					 $(row).find('td:eq(12)').addClass('stock');
                  }
				   

              },
                //invisible type column on table
              "columnDefs": [
                  {
                      "targets": 0,
                      //"visible": false,
                      "searchable": false
                  }
              ]
           // table.column( 0 ).visible( false );
          });
      } );
  </script>
   
    
    
   
</head>

<body>
<form id="frmSamplePlaceOrderReport" name="frmSamplePlaceOrderReport" method="post" action="rptStock_Forecast.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80"
valign="top"><?php include 'presentation/procurement/reports/reportHeader.php' ?></td>
<td width="20%"></td>
</tr>
<tr>
<td colspan="3">
<div id="divPoNo" style="display:none"><?php echo $orderNo ?>/<?php echo $orderYear ?></div>
</td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF"><strong>Item Wise Stock Order Forecast Report</strong></div>

<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
<td>
<table width="100%">
<tr>
<td colspan="10" align="center" bgcolor="#FFFFFF">
</td>
</tr>
   
<tr>
<td>&nbsp;</td>
</tr>
</table>
</td>
</tr>
<tr>
<td>
<table width="100%">
<tr>
<td colspan="20">
</td>
</tr>
<tr height="50">
<td><div class="foo blue"></div><span> Available stock less than rolling stock</span></td>
</tr> 

</table>
</td>
</tr>
<tr>
<td>
<br/>
<table width="100%">
 <tr>
 <td colspan="20">


<div>
<table width="100%" class="table table-striped table-bordered bordered" id="tblMainGrid"  cellspacing="0"
cellpadding="0">
<thead>
<tr class="">

<th class="tbheader"><div>Main Category</div></th>
<th class="tbheader"><div>Sub Category</div></th>
<th class="tbheader"><div>Code</div></th>
<th class="tbheader"><div>Item </div></th>
<th class="tbheader"><div>UOM </div></th>
<th class="tbheader"><div>PRI </div></th>
<th class="tbheader"><div>PRN </div></th>
<th class="tbheader"><div>MRN </div></th>
<th class="tbheader"><div>ISSUED </div></th>
<th class="tbheader"><div>RETURNED</div></th>
<th class="tbheader"><div>GP OUT (FROM HO)</div></th>
<th class="tbheader"><div>GP IN (TO HO)</div></th> 
<th class="tbheader"><div>Available stock</div></th> 
<th class="tbheader"><div>Bal To MRN</div></th>
<th class="tbheader"><div>Bal To Issue<div></th>
<th class="tbheader"><div>Rolling stock</div></th>
<th class="tbheader"><div>Details</div></th>

<?php 
if($BalToPurchase==1)
{
?>
<th class="tbheader"><div>Bal to purchase<div></th>
<?php
}
?>
<th class="tbheader"><div>Re-order Policy<div></th>
                                                
</tr>

</thead>
<tbody>
 <?php
												 
$sql4 		= get_forecastDetails($companyId,$head_office_loc,$company,$maincategory,$subcategory,$item,$mrn); // suvini
$result4 	= $db->RunQuery($sql4);
$a=0;
while ($row = mysqli_fetch_array($result4))
{

$MRN_Qty 	=  round($row['MRN_QTY_TODATE'],4);
$bal_to_MRN = (round(($row['REQUIRED_TODATE'] - $MRN_Qty),4));
$transMrnQtyArray = explode(',', $row['MRNids']);
$mrnQty = array_filter($transMrnQtyArray);
$mrnQtyNew = array();
for($i=0;$i<sizeof($mrnQty);$i++){
    $temp = explode('/',$mrnQty[$i]);
    $mrnQtyNew[$temp[0]] += $temp[1];

}
 $transIssueQtyArray = explode(',', $row['ISSUEids']);
 $issueQty = array_filter($transIssueQtyArray);
 $issueQtyNew = array();
 for($i=0;$i<sizeof($issueQty);$i++){
     $temp = explode('/',$issueQty[$i]);
     $issueQtyNew[$temp[0]] += $temp[1];

 }

 $priQtyArray = explode(',', $row['REQUIRED_NO']);
 $priQtyArrayNew = array();
 for($i=0;$i<sizeof($priQtyArray);$i++){
     $temp = explode('/',$priQtyArray[$i]);
     $priQtyArrayNew[$temp[0]] = $temp[1];

 }

 $prnQtyArray = explode(',', $row['PRN_NO']);
 $prnQtyArrayNew = array();
 for($i=0;$i<sizeof($prnQtyArray);$i++){
     $tempArr = explode('/',$prnQtyArray[$i]);
     $prnQtyArrayNew[$tempArr[0]] = $tempArr[1];

 }


 ?>

 <div id="mrnQtyArray<?php echo $a;?>" value=<?php echo json_encode($mrnQtyNew);?> style="display: none;"></div>
 <div id="issueQtyArray<?php echo $a;?>" value=<?php echo json_encode($issueQtyNew);?> style="display: none;"></div>
 <div id="priQtyArray<?php echo $a;?>" value=<?php echo json_encode($priQtyArrayNew);?> style="display: none;"></div>
 <div id="prnQtyArray<?php echo $a;?>" value=<?php echo json_encode($prnQtyArrayNew);?> style="display: none;"></div>
 <div id="item<?php echo $a;?>" value=<?php echo $row['ITEM'];?> style="display: none;"></div>
 <?php
 $a++;

//if($row['type'] == 2)
//{
//$bal_to_issue_for_all = round((($MRN_Qty)-($row['ISSUE_QTY_ALL_HO']+MAX($row['ISSUE_QTY_ALL_NON_HO'],($row['GP_FROM_HO_ALL'])))),4);
//}
//else
//{
//$bal_to_issue_for_all = round((($row['REQUIRED_TODATE'])-($row['ISSUE_QTY_ALL_HO']+MAX($row['ISSUE_QTY_ALL_NON_HO'],($row['GP_FROM_HO_ALL'])))),4);
//}

$bal_to_issue_for_all = round((max($row['REQUIRED_TODATE'],$MRN_Qty)- $row['ISSUE_QTY_TODATE']),4);

if($bal_to_issue_for_all < 0)
$bal_to_issue_for_all = 0;

$bal_to_grn	= ((round((($row['REQUIRED_TODATE'])-($row['ISSUE_QTY_ALL_HO']+MAX($row['ISSUE_QTY_ALL_NON_HO'],($row['GP_FROM_HO_ALL'])))),4))-(round(getHOStock($row['ITEM'],$head_office_loc),4))) ;
if($bal_to_grn < 0)
$bal_to_grn = 0;
$bal_to_purchase = $bal_to_grn-get_approved_non_grn_pos($row['ITEM'],$head_office_loc);
if($bal_to_purchase < 0)
$bal_to_purchase = 0;



$stockbal = get_stock_balance($row['ITEM'],$head_office_loc);
$rolling  = getRollingStock($row['ITEM'],$curYear,$company);


?>

<tr bgcolor="$col">

<td><?php echo $row['mainCatName']; ?></td>
<td><?php echo $row['subCatName']; ?></td>
<?php
if($row['supItemCode']!=null)
{
?>
<td><?php echo $row['supItemCode']; ?></td>
<?php
}else{
 ?>   

<td><?php echo $row['strCode']; ?></td>
<?php
}
?>
<td><?php echo $row['itemName']; ?></td>
<td><?php echo $row['UOM']; ?></td>
<td><?php echo round($row['REQUIRED_TODATE'],4); ?></td>
<td><?php echo round($row['PRN_QTY_TODATE'],4); ?></td>
<td><?php echo $MRN_Qty; ?></td>
<td><?php echo round($row['ISSUE_QTY_TODATE'],4); ?></td>
<td><?php echo round($row['RET_TO_STORE_QTY_TODATE'],4); ?></td>
<td><?php echo round($row['GP_FROM_HO_TODATE'],4); ?></td>
<td><?php echo round($row['GP_IN_QTY_TO_HO_TODATE'],4); ?></td>
<td><?php echo round(get_stock_balance($row['ITEM'],$head_office_loc),4);?></td>
<td><?php echo $row['pending_mrn_qry']?></td>
<td><?php echo $bal_to_issue_for_all; ?></td>
<td><?php
    $rol = getRollingStock($row['ITEM'],$curYear,$company);
    if($rol != null || $rol != ''){
        echo $rol;
    }
    else echo "not specified"; ?> </td>
<td><img id="butDetails" name="butDetails" src="images/dw.png" width="30" height="24" align="middle" ></td>

<?php
if($BalToPurchase == 1)
{ 
?>
<td><?php echo $bal_to_purchase; ?></td>
<?php

}
?>
<td>
<?php
 if($row['re_order_policy'] == 1)
 {
	 echo "Fixed Reorder Qty";
 } 
 else if($row['re_order_policy'] == 2)
 {
	 echo "Maximum Qty";
 }
 else if($row['re_order_policy'] == 3)
 {
	 echo "Order";
 }
 else if($row['re_order_policy'] == 4)
 {
	 echo "Lot for Lot";
 }
 
 ?>

</td>

<?php
}
?>
</tr>
</tbody>
</table>
</div>
</td>
</tr>    
</td>
<td width="6%"></td>
</tr>
</table>
</div>
</form>
<div class="popup-overlay" id="popupContact1" style="width:900px; position: absolute;display:none;z-index:100">

</div>
</body>
</html>
<?php

function getRollingStock($item,$curYear,$company)
{
	global $db;
	$sql_roll = "select (item_rolling_stock.qty) AS qty
		FROM item_rolling_stock
		WHERE
		item_rolling_stock.locationId = '$company'
		AND
		item_rolling_stock.intItemId = '$item'
		AND
		item_rolling_stock.`year`   = '$curYear'";
        $result_roll  = $db->RunQuery($sql_roll);
	    $row_roll     = mysqli_fetch_array($result_roll);
	    return $row_roll['qty'];
		
}

function get_stock_balance($item,$head_office_loc)
{
	
	global $db;
	
	$sql = "SELECT
			Sum(ware_stocktransactions_bulk.dblQty) as qty 
			FROM `ware_stocktransactions_bulk`
			WHERE
			ware_stocktransactions_bulk.intItemId = '$item' AND
			ware_stocktransactions_bulk.intLocationId = '$head_office_loc'
			";
	
	$result_stock = $db->RunQuery($sql);
	$row_stock    = mysqli_fetch_array($result_stock);
	$stock_bal 	  = round (($row_stock['qty']),4);
	return $stock_bal;

}

function getPOqty($item)
{
	
		global $db;
	$sql_po = "SELECT sum(trn_podetails.dblQty) as po_qty
	from trn_podetails
	INNER JOIN
	trn_poheader on trn_podetails.intPONo = trn_poheader.intPONo AND
	trn_podetails.intPOYear = trn_poheader.intPOYear
	WHERE
	trn_poheader.intStatus = 1 AND
	trn_podetails.intItem = ".$item."";

	$result  = $db->RunQuery($sql_po);
	$row     = mysqli_fetch_array($result);
	return $row['po_qty'];	
	
}

function getHOStock($item,$head_office_loc)
{

	global $db;

	$sql = "SELECT
			Sum(ware_stocktransactions_bulk.dblQty) as qty 
			FROM `ware_stocktransactions_bulk`
			WHERE
			ware_stocktransactions_bulk.intItemId = '$item' AND
			ware_stocktransactions_bulk.intLocationId = '$head_office_loc' 
			";

	$result = $db->RunQuery($sql);
	$row=mysqli_fetch_array($result);
	return $row['qty'];

}
function get_approved_non_grn_pos($item,$head_office_loc)  //head office location sashika
{
	
	global $db;
	
	$sql = "SELECT
			sum(trn_podetails.dblQty-trn_podetails.dblGRNQty) as qty
			FROM
			trn_podetails
			INNER JOIN trn_poheader ON trn_podetails.intPONo = trn_poheader.intPONo AND trn_podetails.intPOYear = 					trn_poheader.intPOYear
			WHERE
			trn_poheader.intStatus = 1 AND
			trn_poheader.intDeliveryTo = '$head_office_loc' AND
			trn_podetails.intItem = '$item'
			";
	
	$result = $db->RunQuery($sql);
	$row=mysqli_fetch_array($result);
	return $row['qty'];
	
	
}
function get_ho_loc($company)
{ 
    global $db;
    $sql="SELECT
			mst_locations.intId
			FROM `mst_locations`
			WHERE
			mst_locations.COMPANY_MAIN_STORES_LOCATION = 1 AND
			mst_locations.intCompanyId = '$company'
			";
	
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    return $row['intId'];

}
/*function get_pending_mrn_details($companyId,$head_office_loc,$locationId,$company,$maincategory,$subcategory,$item)
{
$sql = "
SELECT
TB_ALL.intCompanyId,
TB_ALL.intItemId,
TB_ALL.MRN_QTY,
TB_ALL.item,
TB_ALL.strCode,
TB_ALL.re_order_policy,
TB_ALL.mainCat,
TB_ALL.mainCat_id,
TB_ALL.subCat,
TB_ALL.subCat_id,
TB_ALL.unit,
TB_ALL.GP_IN,
TB_ALL.GP_OUT,
TB_ALL.ISSUE_QTY,
TB_ALL.PO_QTY,
TB_ALL.PRI_QTY,
TB_ALL.PRN_QTY,
TB_ALL.RET_TO_STORES

FROM 
(SELECT
TB2.intCompanyId,
TB2.intItemId,
TB2.item,
TB2.strCode,
TB2.re_order_policy,
TB2.mainCat,
TB2.mainCat_id,
TB2.subCat,
TB2.subCat_id,
TB2.unit,
(TB2.MRN_QTY) AS MRN_QTY,
(TB2.ISSUE_QTY) AS ISSUE_QTY,
(TB2.PRI_QTY) AS PRI_QTY,
(TB2.RET_TO_STORES) AS RET_TO_STORES,
(TB2.GP_OUT) AS GP_OUT,
(TB2.GP_IN) AS GP_IN,

(TB2.PRN_QTY) AS PRN_QTY,
(TB2.PO_QTY) AS PO_QTY
FROM(select 
TB1.intOrderNo,
TB1.intOrderYear,
TB1.intItemId,
TB1.intCompanyId,
TB1.item,
TB1.strCode,
TB1.re_order_policy,
TB1.mainCat,
TB1.mainCat_id,
TB1.subCat,
TB1.subCat_id,
TB1.unit,
SUM(TB1.mrn_qty) AS MRN_QTY,
SUM(TB1.issue_qty)as ISSUE_QTY,
SUM(TB1.PRI_QTY) as PRI_QTY,
SUM(TB1.ret_to_stores) AS RET_TO_STORES,
SUM(TB1.GP_Out) AS GP_OUT,
SUM(TB1.GP_IN_QTY) AS GP_IN,



( SELECT SUM(trn_po_prn_details.PRN_QTY) as prn_qty 
FROM trn_po_prn_details 
WHERE trn_po_prn_details.ORDER_NO = TB1.intOrderNo AND 
trn_po_prn_details.ORDER_YEAR = TB1.intOrderYear AND 
trn_po_prn_details.ITEM = TB1.intItemId) AS PRN_QTY,

(SELECT SUM(trn_po_prn_details.PURCHASED_QTY) as po_qty 
FROM trn_po_prn_details WHERE 
trn_po_prn_details.ORDER_NO = TB1.intOrderNo AND 
trn_po_prn_details.ORDER_YEAR = TB1.intOrderYear AND 
trn_po_prn_details.ITEM = TB1.intItemId ) AS PO_QTY

FROM(

select  

ware_mrndetails.intOrderNo,  

ware_mrndetails.intOrderYear,
ware_mrndetails.intItemId,
mst_locations.intCompanyId,
mst_maincategory.intId AS mainCat_id,
mst_maincategory.strName AS mainCat,
mst_subcategory.intId AS subCat_id,
mst_subcategory.strName As subCat,
mst_item.strName AS item,
mst_item.strCode,
mst_item.intReorderPolicy as re_order_policy,
mst_units.strName AS unit,
	
sum(ware_mrndetails.dblQty) as mrn_qty, 
sum(ware_mrndetails.dblIssudQty) as issue_qty, 

(select SUM(trn_po_prn_details_sales_order.REQUIRED) 
from trn_po_prn_details_sales_order
WHERE 
trn_po_prn_details_sales_order.ORDER_NO = ware_mrndetails.intOrderNo AND 
trn_po_prn_details_sales_order.ORDER_YEAR = ware_mrndetails.intOrderYear AND
trn_po_prn_details_sales_order.SALES_ORDER = ware_mrndetails.strStyleNo AND  
trn_po_prn_details_sales_order.ITEM = ware_mrndetails.intItemId ) AS PRI_QTY, 


(select SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'RETURNED_TO_STORES',
 trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS RET_TO_STORE_QTY 
FROM trn_po_prn_details_sales_order_stock_transactions 
WHERE trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = ware_mrndetails.intOrderNo 
AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = ware_mrndetails.intOrderYear 
AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = ware_mrndetails.strStyleNo
AND trn_po_prn_details_sales_order_stock_transactions.ITEM = ware_mrndetails.intItemId 
AND trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID = '$locationId' ) as ret_to_stores,

 (select SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'GATE_PASS',
 trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS GP_QTY 
FROM trn_po_prn_details_sales_order_stock_transactions 
WHERE trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = ware_mrndetails.intOrderNo AND 
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = ware_mrndetails.intOrderYear AND 
 trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = ware_mrndetails.strStyleNo AND 
trn_po_prn_details_sales_order_stock_transactions.ITEM = ware_mrndetails.intItemId AND 
trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID = '$locationId' )as GP_Out, 

(select SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'GATE_PASS_IN', 
trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS GP_IN_QTY 
FROM trn_po_prn_details_sales_order_stock_transactions 
WHERE trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = ware_mrndetails.intOrderNo AND 
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = ware_mrndetails.intOrderYear AND 
 trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = ware_mrndetails.strStyleNo AND
trn_po_prn_details_sales_order_stock_transactions.ITEM = ware_mrndetails.intItemId AND 
trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID = '$locationId' )GP_IN_QTY 

from ware_mrndetails 
INNER JOIN ware_mrnheader on ware_mrndetails.intMrnNo = ware_mrnheader.intMrnNo AND 
ware_mrndetails.intMrnYear= ware_mrnheader.intMrnYear 
INNER JOIN mst_locations on ware_mrnheader.intCompanyId = mst_locations.intId
INNER JOIN mst_item ON ware_mrndetails.intItemId = mst_item.intId 
INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId 
INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId 
INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
WHERE 
((ware_mrnheader.intStatus <= (ware_mrnheader.intApproveLevels+1) && ware_mrnheader.intStatus >1) 
-- || ware_mrnheader.intStatus=0
)
AND 
mst_locations.intCompanyId IN ($company)

GROUP BY 
ware_mrndetails.intOrderNo,
ware_mrndetails.intOrderYear,
ware_mrndetails.strStyleNo,
ware_mrndetails.intItemId
) as  TB1



GROUP BY 
TB1.intOrderNo,
TB1.intOrderYear,
TB1.intItemId 
)AS TB2

GROUP BY 
TB2.intItemId 
) 

AS TB_ALL

where
1=1
";

$sql .=  "  AND TB_ALL.intCompanyId IN ($company)";


if($maincategory !='null')
{
$sql .=  "  AND TB_ALL.mainCat_id IN ($maincategory)";
}



if($subcategory !='null')
{
$sql .=  "  AND TB_ALL.subCat_id IN ($subcategory)";
}
if($item !='null')
{
$sql .=  "  AND TB_ALL.intItemId IN ($item)";
}
$sql .=   "ORDER BY
		   TB_ALL.mainCat_id,
	       TB_ALL.subCat_id,
		   TB_ALL.intItemId
		   ASC";
//echo $sql;	    
return $sql;	
}*/

function get_forecastDetails($companyId,$head_office_loc, $company,$maincategory,$subcategory,$item,$mrn)
{

 $sql = 
 
 "
SELECT
TB_ALL_1.type as type,
TB_ALL_1.ORDER_COMPANY as ORDER_COMPANY,
TB_ALL_1.location as location,
TB_ALL_1.mainCatId as mainCatId,
TB_ALL_1.subCatId as subCatId,
TB_ALL_1.ITEM as ITEM,
TB_ALL_1.re_order_policy as re_order_policy,
TB_ALL_1.mainCatName as mainCatName,
TB_ALL_1.subCatName as subCatName,
TB_ALL_1.strCode as strCode,
TB_ALL_1.supItemCode,
TB_ALL_1.itemName as itemName,
TB_ALL_1.MOQ as MOQ,
TB_ALL_1.TODATE_ORDERS as TODATE_ORDERS,
TB_ALL_1.REQUIRED_TODATE as REQUIRED_TODATE,
TB_ALL_1.REQUIRED_NO as REQUIRED_NO,
TB_ALL_1.PRN_NO as PRN_NO,
TB_ALL_1.PRN_QTY_TODATE as PRN_QTY_TODATE,
TB_ALL_1.PO_QTY_TODATE as PO_QTY_TODATE,
TB_ALL_1.MRN_QTY_TODATE as MRN_QTY_TODATE,
TB_ALL_1.pending_mrn_qry as pending_mrn_qry ,
TB_ALL_1.ISSUE_QTY_TODATE as ISSUE_QTY_TODATE,
TB_ALL_1.RET_TO_STORE_QTY_TODATE as RET_TO_STORE_QTY_TODATE,
TB_ALL_1.GP_FROM_HO_TODATE as GP_FROM_HO_TODATE,
TB_ALL_1.GP_IN_QTY_TO_HO_TODATE as GP_IN_QTY_TO_HO_TODATE,
TB_ALL_1.UOM as UOM,
TB_ALL_1.ISSUE_QTY_ALL_HO as ISSUE_QTY_ALL_HO,
TB_ALL_1.GP_FROM_HO_ALL as GP_FROM_HO_ALL,
TB_ALL_1.ISSUE_QTY_ALL_NON_HO as ISSUE_QTY_ALL_NON_HO,
(TB_ALL_1.MRNids) AS MRNids,
(TB_ALL_1.ISSUEids) AS ISSUEids

from
 (SELECT
	TB_ALL.type,
	TB_ALL.date AS DATES_TODATE,
	TB_ALL.date AS DATES_OTHER,
	TB_ALL.ORDER_COMPANY,
	TB_ALL.location AS location,
	TB_ALL.mainCatId,
	TB_ALL.subCatId,
	TB_ALL.ITEM,
	TB_ALL.re_order_policy,
	TB_ALL.mainCatName,
	TB_ALL.subCatName,
	TB_ALL.strCode,
        TB_ALL.supItemCode,
	TB_ALL.itemName,
	TB_ALL.MOQ,
	TB_ALL.otherOrders AS TODATE_ORDERS,
	sum(TB_ALL.REQUIRED) AS REQUIRED_TODATE,
	GROUP_CONCAT(TB_ALL.REQUIRED_NO) AS REQUIRED_NO,
	GROUP_CONCAT(TB_ALL.PRN_NO) AS PRN_NO,
	sum(TB_ALL.PRN_QTY) AS PRN_QTY_TODATE,
	sum(TB_ALL.PO_QTY) AS PO_QTY_TODATE,
	sum((
		ifnull(TB_ALL.MRN_QTY, 0) + ifnull(TB_ALL.MRN_QTY_OTHER_LOC, 0)
	)) AS MRN_QTY_TODATE,

    round((sum(TB_ALL.REQUIRED)-sum(ifnull(TB_ALL.MRN_QTY, 0) + ifnull(TB_ALL.MRN_QTY_OTHER_LOC, 0))), 4) as pending_mrn_qry,

	sum((
		ifnull(TB_ALL.ISSUE_QTY, 0) + ifnull(
			TB_ALL.ISSUE_QTY_OTHER_LOC,
			0
		)
	)) AS ISSUE_QTY_TODATE,
	sum((
		ifnull(TB_ALL.RET_TO_STORE_QTY, 0) + ifnull(
			TB_ALL.RET_TO_STORE_QTY_OTHER_LOC,
			0
		)
	) )AS RET_TO_STORE_QTY_TODATE,
	sum(TB_ALL.GP_QTY) AS GP_FROM_HO_TODATE,
	sum(TB_ALL.GP_IN_QTY) AS GP_IN_QTY_TO_HO_TODATE,
	TB_ALL.UOM,
	TB_ALL.otherOrders AS OTHER_ORDERS,
	sum((ifnull(TB_ALL.ISSUE_QTY, 0))) AS ISSUE_QTY_ALL_HO,
	sum((ifnull(TB_ALL.GP_QTY, 0))) AS GP_FROM_HO_ALL,
	sum((
		ifnull(
			TB_ALL.ISSUE_QTY_OTHER_LOC,
			0
		)
	) )AS ISSUE_QTY_ALL_NON_HO,
	TB_ALL.PO_QTY AS PO_QTY_OTHER,
	TB_ALL.ORDER_NO,
	TB_ALL.ORDER_STATUS,
	TB_ALL.ORDER_TYPE,
	TB_ALL.CREATE_DATE,
	TB_ALL.SO,
	GROUP_CONCAT(TB_ALL.MRNids) as MRNids,
	GROUP_CONCAT(TB_ALL.ISSUEids) as ISSUEids	

from
(select tb.type,
tb.ORDER_COMPANY,
tb.location AS location,
tb.date,
tb.ITEM,
GROUP_CONCAT(DISTINCT tb.ORDER_NO,'/',tb.ORDER_YEAR) as otherOrders,

SUM(tb.REQUIRED) as REQUIRED,
GROUP_CONCAT(tb.REQUIRED_NO) as REQUIRED_NO,
GROUP_CONCAT(tb.PRN_NO) as PRN_NO,
tb.TRANSACTION_LOCATION,
sum(tb.PO_QTY) as PO_QTY,
sum(tb.PRN_QTY) as PRN_QTY,
tb.mainCatId,
tb.subCatId,
tb.mainCatName,
tb.subCatName,
tb.strCode,
tb.supItemCode,
tb.itemName,
tb.MOQ,
tb.re_order_policy,
tb.UOM,
(SUM(tb.MRN_QTY) + SUM(tb.mrn_pending)) AS MRN_QTY,
SUM(tb.MRN_EXTRA_QTY) as MRN_EXTRA_QTY,
SUM(tb.ISSUE_QTY) as ISSUE_QTY,
SUM(tb.GP_QTY) as GP_QTY,
SUM(tb.GP_IN_QTY) as GP_IN_QTY,
SUM(tb.RET_TO_STORE_QTY) as RET_TO_STORE_QTY,
SUM(tb.MRN_QTY_OTHER_LOC) as MRN_QTY_OTHER_LOC,
SUM(tb.MRN_EXTRA_QTY_OTHER_LOC) as MRN_EXTRA_QTY_OTHER_LOC,
SUM(tb.ISSUE_QTY_OTHER_LOC) as ISSUE_QTY_OTHER_LOC,
SUM(tb.GP_QTY_OTHER_LOC) as GP_QTY_OTHER_LOC,
SUM(tb.GP_IN_QTY_OTHER_LOC) as GP_IN_QTY_OTHER_LOC,
SUM(tb.RET_TO_STORE_QTY_OTHER_LOC) as RET_TO_STORE_QTY_OTHER_LOC ,
tb.ORDER_NO,
tb.ORDER_STATUS,
tb.ORDER_TYPE,
tb.CREATE_DATE,
tb.SO,
GROUP_CONCAT(tb.MRNids) as MRNids,
GROUP_CONCAT(tb.ISSUEids) as ISSUEids
from(
SELECT TB3.type,
TB3.ORDER_COMPANY,
TB3.location AS location,
TB3.date,
TB3.ITEM,
TB3.ORDER_NO,
TB3.ORDER_YEAR,
(
SELECT
sum(
trn_po_prn_details.REQUIRED
)
FROM
trn_po_prn_details
WHERE
trn_po_prn_details.ORDER_NO = TB3.ORDER_NO
AND trn_po_prn_details.ORDER_YEAR = TB3.ORDER_YEAR
AND trn_po_prn_details.ITEM = TB3.ITEM
) AS REQUIRED, 
(SELECT GROUP_CONCAT(CONCAT(trn_po_prn_details.ORDER_NO,'-',trn_po_prn_details.ORDER_YEAR,'/',trn_po_prn_details.REQUIRED))
FROM
trn_po_prn_details
WHERE
trn_po_prn_details.ORDER_NO = TB3.ORDER_NO
AND trn_po_prn_details.ORDER_YEAR = TB3.ORDER_YEAR
AND trn_po_prn_details.ITEM = TB3.ITEM
) AS REQUIRED_NO,
(SELECT GROUP_CONCAT(CONCAT(trn_po_prn_details.ORDER_NO,'-',trn_po_prn_details.ORDER_YEAR,'/',trn_po_prn_details.PRN_QTY))
FROM
trn_po_prn_details
WHERE
trn_po_prn_details.ORDER_NO = TB3.ORDER_NO
AND trn_po_prn_details.ORDER_YEAR = TB3.ORDER_YEAR
AND trn_po_prn_details.ITEM = TB3.ITEM
) AS PRN_NO, 

(
SELECT
sum(
trn_po_prn_details.PURCHASED_QTY)
FROM trn_po_prn_details
WHERE trn_po_prn_details.ORDER_NO = TB3.ORDER_NO
AND trn_po_prn_details.ORDER_YEAR = TB3.ORDER_YEAR
AND trn_po_prn_details.ITEM = TB3.ITEM ) AS PO_QTY,

(SELECT SUM(trn_po_prn_details.PRN_QTY)
FROM trn_po_prn_details
WHERE trn_po_prn_details.ORDER_NO = TB3.ORDER_NO AND
trn_po_prn_details.ORDER_YEAR = TB3.ORDER_YEAR AND
trn_po_prn_details.ITEM = TB3.ITEM ) AS PRN_QTY,";

    $sql .= "

TB3.TRANSACTION_LOCATION,
SUM(TB3.mrn_pending) AS mrn_pending,
TB3.mainCatId,
TB3.subCatId,
TB3.mainCatName,
TB3.subCatName,
TB3.strCode,
TB3.supItemCode,
TB3.itemName,
TB3.MOQ,
TB3.re_order_policy,
TB3.UOM,
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.MRN_QTY,0)) as MRN_QTY,
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.MRN_EXTRA_QTY,0)) as MRN_EXTRA_QTY,
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.ISSUE_QTY,0)) as ISSUE_QTY,
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.GP_QTY,0)) as GP_QTY,
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.GP_IN_QTY,0)) as GP_IN_QTY,
SUM(IF(TB3.`TRANSACTION_LOCATION`='$head_office_loc',TB3.RET_TO_STORE_QTY,0)) as RET_TO_STORE_QTY,
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.MRN_QTY,0)) as MRN_QTY_OTHER_LOC,
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.MRN_EXTRA_QTY,0)) as MRN_EXTRA_QTY_OTHER_LOC,
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.ISSUE_QTY,0)) as ISSUE_QTY_OTHER_LOC,
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.GP_QTY,0)) as GP_QTY_OTHER_LOC,
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.GP_IN_QTY,0)) as GP_IN_QTY_OTHER_LOC,
SUM(IF(TB3.`TRANSACTION_LOCATION`<>'$head_office_loc',TB3.RET_TO_STORE_QTY,0)) as RET_TO_STORE_QTY_OTHER_LOC,
TB3.ORDER_STATUS,
TB3.ORDER_TYPE,
TB3.CREATE_DATE,
TB3.SO,
GROUP_CONCAT(TB3.MRNids) as MRNids,
GROUP_CONCAT(TB3.ISSUEids) as ISSUEids
FROM
 (SELECT TB2.ORDER_COMPANY,
TB2.ORDER_NO,
TB2.ORDER_YEAR,
TB2.location,
TB2.ITEM,
TB2.SALES_ORDER,
TB2.type,
(TB2.pending_mrn_qty_new) AS mrn_pending,
CONCAT(trn_orderdetails.strSalesOrderNo,'/',trn_orderdetails.strCombo,'/',trn_orderdetails.strPrintName) AS SALES_ORDER_String,
'' AS date,
mst_maincategory.intId as mainCatId,
mst_subcategory.intId AS subCatId,
mst_maincategory.strName AS mainCatName,
mst_subcategory.strName AS subCatName,
mst_item.strCode,
CONCAT(mst_supplier.strCode,mst_item.strCode) as supItemCode,
mst_item.intId,
mst_item.strName AS itemName,
mst_item.dblMinimumOrderQty AS MOQ,
mst_item.intReorderPolicy AS re_order_policy,
mst_units.strName AS UOM,";

    $sql .= "trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID AS TRANSACTION_LOCATION,
(SELECT GROUP_CONCAT(
											CONCAT(
												trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
												'-',
												trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
												'/',
												(
													trn_po_prn_details_sales_order_stock_transactions.QTY
												)
											))
										FROM
											trn_po_prn_details_sales_order_stock_transactions
										WHERE
											trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = TB2.ORDER_NO
										AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = TB2.ORDER_YEAR
										AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = TB2.SALES_ORDER
										AND trn_po_prn_details_sales_order_stock_transactions.ITEM = TB2.ITEM
										AND trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'MRN'
									) AS MRNids,
(SELECT GROUP_CONCAT(
											CONCAT(
												trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
												'-',
												trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
												'/',
												(
													trn_po_prn_details_sales_order_stock_transactions.QTY
												)
											))
										FROM
											trn_po_prn_details_sales_order_stock_transactions
										WHERE
											trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = TB2.ORDER_NO
										AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = TB2.ORDER_YEAR
										AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = TB2.SALES_ORDER
										AND trn_po_prn_details_sales_order_stock_transactions.ITEM = TB2.ITEM
										AND trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'ISSUE'
									) AS ISSUEids,
SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'MRN',
trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS MRN_QTY,
SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'MRN_EXTRA',
trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS MRN_EXTRA_QTY,
SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'ISSUE',
trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS ISSUE_QTY,
SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'GATE_PASS',
trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS GP_QTY,
SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'GATE_PASS_IN',
trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS GP_IN_QTY,
SUM( IF ( trn_po_prn_details_sales_order_stock_transactions.`TRANSACTION` = 'RETURNED_TO_STORES',
trn_po_prn_details_sales_order_stock_transactions.QTY, 0 ) ) AS RET_TO_STORE_QTY,
TB2.ORDER_STATUS,
TB2.ORDER_TYPE,
TB2.CREATE_DATE,
(
trn_orderdetails.strSalesOrderNo
) AS SO
FROM
( SELECT TB1.ORDER_NO,
									TB1.ORDER_YEAR,
									TB1.SALES_ORDER,
									TB1.ITEM,
									TB1.ORDER_COMPANY,
									TB1.location,
									sum(TB1.pending_mrn_qty) AS pending_mrn_qty_new,
									TB1.type,
									TB1.ORDER_STATUS,
									TB1.ORDER_TYPE,
									TB1.CREATE_DATE FROM 
(
SELECT
trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER,
trn_po_prn_details_sales_order_stock_transactions.ITEM,
mst_locations.intCompanyId AS ORDER_COMPANY,
mst_locations.strName AS location,
0 as pending_mrn_qty,
( SELECT GROUP_CONCAT(mst_locations.intCompanyId)
FROM ware_fabricreceivedheader
INNER JOIN mst_locations
ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId
WHERE
ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO
AND ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
)AS FABRIC_IN_COMPANIES,
1 AS type,
trn_orderheader.intStatus AS ORDER_STATUS,
trn_orderheader.PO_TYPE AS ORDER_TYPE,
trn_orderheader.dtDate AS CREATE_DATE
FROM trn_po_prn_details_sales_order_stock_transactions
INNER JOIN trn_po_prn_details_sales_order
ON
trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = trn_po_prn_details_sales_order.ORDER_NO
AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR
AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER
AND trn_po_prn_details_sales_order_stock_transactions.ITEM = trn_po_prn_details_sales_order.ITEM
INNER JOIN trn_orderheader
ON trn_orderheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO AND
trn_orderheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
AND trn_orderheader.intStatus = 1
INNER JOIN mst_locations
ON mst_locations.intId = trn_orderheader.intLocationId

GROUP BY trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER,
trn_po_prn_details_sales_order_stock_transactions.ITEM
HAVING FIND_IN_SET('$company', FABRIC_IN_COMPANIES) OR ORDER_COMPANY = '$company'

UNION ALL
SELECT
trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER,
trn_po_prn_details_sales_order_stock_transactions.ITEM,
mst_locations.intCompanyId AS ORDER_COMPANY,
mst_locations.strName AS location,
0 as pending_mrn_qty,
( SELECT GROUP_CONCAT( DISTINCT mst_locations.intCompanyId )
FROM ware_fabricreceivedheader
INNER JOIN mst_locations
ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId
WHERE
ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO AND
ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
) AS FABRIC_IN_COMPANIES,
2 AS type,
trn_orderheader.intStatus AS ORDER_STATUS,
trn_orderheader.PO_TYPE AS ORDER_TYPE,
trn_orderheader.dtDate AS CREATE_DATE
FROM trn_po_prn_details_sales_order_stock_transactions

LEFT JOIN trn_po_prn_details_sales_order
ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = trn_po_prn_details_sales_order.ORDER_NO AND
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR AND
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER AND
trn_po_prn_details_sales_order_stock_transactions.ITEM = trn_po_prn_details_sales_order.ITEM
INNER JOIN trn_orderheader ON
trn_orderheader.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO AND
trn_orderheader.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR
AND trn_orderheader.intStatus = 1
INNER JOIN mst_locations ON mst_locations.intId = trn_orderheader.intLocationId

WHERE trn_po_prn_details_sales_order.ORDER_NO IS NULL

GROUP BY
trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER,
trn_po_prn_details_sales_order_stock_transactions.ITEM
HAVING ( FIND_IN_SET('$company', FABRIC_IN_COMPANIES) OR ORDER_COMPANY = '$company' )


UNION ALL
SELECT
trn_po_prn_details_sales_order.ORDER_NO,
trn_po_prn_details_sales_order.ORDER_YEAR,
trn_po_prn_details_sales_order.SALES_ORDER,
trn_po_prn_details_sales_order.ITEM,
mst_locations.intCompanyId AS ORDER_COMPANY,
mst_locations.strName AS location,
0 as pending_mrn_qty,
( SELECT GROUP_CONCAT( DISTINCT mst_locations.intCompanyId )
FROM ware_fabricreceivedheader
INNER JOIN mst_locations
ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId
WHERE ware_fabricreceivedheader.intOrderNo = trn_po_prn_details_sales_order.ORDER_NO
AND ware_fabricreceivedheader.intOrderYear = trn_po_prn_details_sales_order.ORDER_YEAR
) AS FABRIC_IN_COMPANIES,
1 AS type,
trn_orderheader.intStatus AS ORDER_STATUS,
trn_orderheader.PO_TYPE AS ORDER_TYPE,
trn_orderheader.dtDate AS CREATE_DATE
FROM trn_po_prn_details_sales_order_stock_transactions

RIGHT JOIN
trn_po_prn_details_sales_order
ON
trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = trn_po_prn_details_sales_order.ORDER_NO AND
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = trn_po_prn_details_sales_order.ORDER_YEAR AND
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = trn_po_prn_details_sales_order.SALES_ORDER AND
trn_po_prn_details_sales_order_stock_transactions.ITEM = trn_po_prn_details_sales_order.ITEM
INNER JOIN trn_orderheader
ON trn_orderheader.intOrderNo = trn_po_prn_details_sales_order.ORDER_NO AND
trn_orderheader.intOrderYear = trn_po_prn_details_sales_order.ORDER_YEAR
AND trn_orderheader.intStatus = 1
INNER JOIN mst_locations
ON mst_locations.intId = trn_orderheader.intLocationId

WHERE trn_po_prn_details_sales_order_stock_transactions.ORDER_NO IS NULL

GROUP BY
trn_po_prn_details_sales_order.ORDER_NO,
trn_po_prn_details_sales_order.ORDER_YEAR,
trn_po_prn_details_sales_order.SALES_ORDER,
trn_po_prn_details_sales_order.ITEM

HAVING ( FIND_IN_SET('$company', FABRIC_IN_COMPANIES) OR ORDER_COMPANY = '$company' )";

if($mrn ==1) {
    $sql .= " UNION ALL 
		
SELECT
ware_mrndetails.intOrderNo AS ORDER_NO,
ware_mrndetails.intOrderYear AS ORDER_YEAR,
ware_mrndetails.strStyleNo AS SALES_ORDER,
ware_mrndetails.intItemId AS ITEM,
mst_locations.intCompanyId AS ORDER_COMPANY,
mst_locations.strName AS location,
ware_mrndetails.dblQty as pending_mrn_qty,

(
SELECT
GROUP_CONCAT(
DISTINCT mst_locations.intCompanyId
)
FROM
ware_fabricreceivedheader
INNER JOIN mst_locations ON mst_locations.intId = ware_fabricreceivedheader.intCompanyId
WHERE
ware_fabricreceivedheader.intOrderNo = ware_mrndetails.intOrderNo
AND ware_fabricreceivedheader.intOrderYear = ware_mrndetails.intOrderYear
) AS FABRIC_IN_COMPANIES,
2 AS type,

0 AS ORDER_STATUS,
0 AS ORDER_TYPE,
0 AS CREATE_DATE
FROM
trn_po_prn_details_sales_order_stock_transactions
RIGHT JOIN ware_mrndetails ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = ware_mrndetails.intOrderNo
AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = ware_mrndetails.intOrderYear
AND trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = ware_mrndetails.strStyleNo
AND trn_po_prn_details_sales_order_stock_transactions.ITEM = ware_mrndetails.intItemId
LEFT JOIN trn_po_prn_details_sales_order ON ware_mrndetails.intOrderNo = trn_po_prn_details_sales_order.ORDER_NO
AND ware_mrndetails.intOrderYear = trn_po_prn_details_sales_order.ORDER_YEAR
AND ware_mrndetails.strStyleNo = trn_po_prn_details_sales_order.SALES_ORDER
AND ware_mrndetails.intItemId = trn_po_prn_details_sales_order.ITEM
INNER JOIN ware_mrnheader ON ware_mrndetails.intMrnNo = ware_mrnheader.intMrnNo
AND ware_mrndetails.intMrnYear = ware_mrnheader.intMrnYear
INNER JOIN mst_locations ON ware_mrnheader.intCompanyId = mst_locations.intId

WHERE
(
ware_mrndetails.intOrderNo > 0
AND (
ware_mrnheader.intStatus <= (
ware_mrnheader.intApproveLevels + 1
) && ware_mrnheader.intStatus > 1
) 
)
AND mst_locations.intCompanyId IN ($company)
GROUP BY
ware_mrndetails.intOrderNo,
ware_mrndetails.intOrderYear,
ware_mrndetails.strStyleNo,
ware_mrndetails.intItemId
HAVING
(
FIND_IN_SET('$company', FABRIC_IN_COMPANIES)
OR ORDER_COMPANY = '$company'
) ";

}

$sql.=" ) AS TB1 
GROUP BY
TB1.ORDER_NO,
TB1.ORDER_YEAR,
TB1.SALES_ORDER,
TB1.ITEM ) AS TB2


LEFT JOIN
trn_po_prn_details_sales_order_stock_transactions
ON trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = TB2.ORDER_NO AND
trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = TB2.ORDER_YEAR AND
trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER = TB2.SALES_ORDER AND
trn_po_prn_details_sales_order_stock_transactions.ITEM = TB2.ITEM
INNER JOIN trn_orderheader
ON trn_orderheader.intOrderNo = TB2.ORDER_NO AND
trn_orderheader.intOrderYear = TB2.ORDER_YEAR AND
trn_orderheader.intStatus = 1
LEFT JOIN trn_po_prn_details_sales_order ON trn_po_prn_details_sales_order.ORDER_NO = TB2.ORDER_NO
AND trn_po_prn_details_sales_order.ORDER_YEAR = TB2.ORDER_YEAR
AND trn_po_prn_details_sales_order.SALES_ORDER = TB2.SALES_ORDER
AND trn_po_prn_details_sales_order.ITEM = TB2.ITEM
INNER JOIN trn_orderdetails
ON TB2.ORDER_NO = trn_orderdetails.intOrderNo
AND TB2.ORDER_YEAR = trn_orderdetails.intOrderYear
AND TB2.SALES_ORDER = trn_orderdetails.intSalesOrderId
INNER JOIN mst_item
ON TB2.ITEM = mst_item.intId
LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier =  
                                                        mst_supplier.intId
INNER JOIN mst_units
ON mst_item.intUOM = mst_units.intId
INNER JOIN mst_maincategory
ON mst_item.intMainCategory = mst_maincategory.intId
INNER JOIN mst_subcategory
ON mst_item.intSubCategory = mst_subcategory.intId
GROUP BY
TB2.ORDER_NO,
TB2.ORDER_YEAR,
TB2.SALES_ORDER,
TB2.ITEM,
TB2.ORDER_COMPANY
) AS TB3 ";
     
 $sql .= " GROUP BY
TB3.ORDER_NO,
TB3.ORDER_YEAR,
TB3.ITEM )
as
tb
GROUP BY
tb.ORDER_NO,
tb.ORDER_YEAR,
tb.ITEM
) as TB_ALL
GROUP BY
TB_ALL.ITEM) as 	TB_ALL_1
WHERE
1 = 1 ";
   
//$sql .= " AND TB_ALL .ORDER_COMPANY IN (2)";
    $sql .= " AND TB_ALL_1.ORDER_COMPANY = $company ";

    if ($maincategory != '' || $maincategory != NULL) {

        $sql .= " AND TB_ALL_1.mainCatId IN ($maincategory)";

    }

    if ($subcategory != '' || $subcategory != NULL) {
        $sql .= "AND TB_ALL_1.subCatId IN ($subcategory)";
    }
    if ($item !='null') {
        $sql .= "AND TB_ALL_1.ITEM IN ($item)";

    }
	$sql .=   "ORDER BY  
		   TB_ALL_1.mainCatId,
		   TB_ALL_1.subCatId,
		   TB_ALL_1.ITEM
		   ASC";


    return $sql;
}




<?php

$location 	= $_SESSION['CompanyID'];
$company 	= $_SESSION['headCompanyId'];
$locationId = $location;//this locationId use in report header(reportHeader.php)--------------------

$userId 	= $_SESSION['userId'];
?>
<title>Selected PRN Group Progress Report</title>
<!--/*<script type="text/javascript" src="presentation/procurement/reports/groupPRN_report/rpt_groupPrn-js.js"></script>
*/--><style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:281px;
	top:171px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>

<form id="frmFabGPReport" name="frmFabGPReport" method="post">
<div align="center">
<table width="1100" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  	<table width="100%">
		<tr>
            <td width="5%">&nbsp;</td>
            <td colspan="7" height="80" align="center" valign="top"><?php include 'presentation/procurement/reports/reportHeader.php' ?></td>
            <td width="4%">&nbsp;</td>
      	</tr> 
		<tr>
        	<td>&nbsp;</td>
            <td colspan="7"><div align="center"><strong>SELECTED PRN GROUP PROGRESS REPORT</strong></div></td>
            <td>&nbsp;</td>   
  		</tr>    
  		<tr>
    		<td>&nbsp;</td>
            <td width="12%" valign="top" class="normalfnt"><strong>PRN NOs</strong></td>
            <td width="2%" valign="top"><strong>:</strong></td>
            <td width="74%" ><textarea id="txtPRN" cols="50" rows="6" class="textarea" style="width::300px" disabled="disabled"><?php echo $remarks  ?></textarea></td>
    	</tr>
	</table>
  </td>
</tr>

<tr>
<td align="right" valign="bottom"><img src="images/Tadd.jpg" width="92" height="24" id="butAddItems" /></td>
</tr>

<tr>
  <td>
    <table width="100%">
      <tr>
        <td width="5%">&nbsp;</td>
        <td colspan="7" class="normalfnt">
          <table width="100%" class="grid" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr class="gridHeader">
              <td width="5%" >PRN NO</td>
              <td width="10%" >Main Category</td>
              <td width="10%" >Sub Category</td>
              <td width="5%" >Item Code</td>
              <td width="15%" >Item Description</td>
              <td width="5%" >PRN QTY</td>
              <td width="15%" >MRN NOs</td>
              <td width="5%" >MRN QTY</td>
              <td width="15%" >MIN NOs</td>
              <td width="5%">Issued QTY</td>
              <td width="5%" >Central Warehouse Stock</td>
              <td width="5%" >Total Stock</td>
             </tr>
            </table>
          </td>
        <td width="6%">&nbsp;</td>
        </tr>
      </table>
    </td>
</tr>
<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
	<!-- items to prn -->
	<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1">
   <!-- <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php" style="width:800;height:800;border:0;overflow:hidden">
    </iframe>-->
    </div>

	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>



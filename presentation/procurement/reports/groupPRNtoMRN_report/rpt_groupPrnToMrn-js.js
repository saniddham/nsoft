var basePath = "presentation/procurement/reports/groupPRNtoMRN_report/";	
var flagAllowDefaultExcRate= 0;
var flagRemarks= 0;
var selectedPRNstring='';
			
$(document).ready(function() {
	
		$("#butAddItems").die('click').live('click',function(){
		showWaiting();
		closePopUp();
		popupWindow3('1');
		$('#popupContact1').load(basePath+'rpt_groupPrnToMrn_popup.php',function(){
				//checkAlreadySelected();
				$('#frmGrpPRNPopup #butAdd').die('click').live('click',addClickedRows);
				$('#frmGrpPRNPopup #butClose1').die('click').live('click',disablePopup);
			});	hideWaiting();
			
		});
	
 
//------------------------------------------
  $('#frmGrpPRNPopup  #chkAll').die('click').live('click',function(){
		if(document.getElementById('chkAll').checked==true)
		 var chk=true;
		else
		 var chk=false;
		
		var rowCount = document.getElementById('tblItemsPopup').rows.length;
		for(var i=1;i<rowCount;i++)
		{
			document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].checked=chk;
		}
	});
//-----------------------------------------------------
  $('#frmGrpPRNPopup  .delImg').die('click').live('click',function(){
		var prnNo=$(this).parent().parent().find('.prnNo').html();
		$(this).parent().parent().remove();
		updatePoHideStatus(prnNo,1);
	});
//-----------------------------------------------------
$('#butClose').die('click').live('click',function(){
		//load('main.php');	
});
//--------------refresh the form-----------------------
});

//----------end of ready -------------------------------
function loadMain()
{
	//alert(1);
/*	$("#butAddPendingDispatch").click(function(){
		popupWindow3('1');
		//$('#iframeMain1').attr('src','http://localhost/qpet/presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php?id='+$('#cboDispatchTo').val());
		$('#popupContact1').load('sampleDispatchPopup.php');	
		
	});*/
	//$("#iframeMain1").contents().find("#butAdd").click(abc);
	//$("#frmGrpPRNPopup").contents().find("#butAdd").click(abc);
//	$('#frmGrpPRNPopup #butClose').click(abc);
}

//----------------------------------------------------
function addClickedRows()
{
		deleteAll();
			
		var rowCount = document.getElementById('tblPRNsPopup').rows.length;
		var data='';
		showWaiting();
		var url 		= basePath+"rpt_groupPrnToMrn-db-get.php?requestType=loadGRPPRNItems";
		var arr="[";
		
		selectedPRNstring='';
		for(var i=1;i<rowCount;i++)
		{
			if(document.getElementById('tblPRNsPopup').rows[i].cells[1].childNodes[0].checked==true){
				arr += "{";
				var prnNo = document.getElementById('tblPRNsPopup').rows[i].cells[2].innerHTML;
				arr += '"prnNo":"'+		prnNo +'"' ;
				arr +=  '},';
				
				selectedPRNstring +=prnNo+'  ';
			}
		}
			
			selectedPRNstring = selectedPRNstring.substr(0,selectedPRNstring.length-1);
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;
		
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:data,
			async:false,
			success:function(json){

				var length = json.arrCombo.length;
				var arrCombo = json.arrCombo;
				
				if(arrCombo[0]=='none'){
				alert("Items not found.");
				return;
				//hideWaiting();
				}

				for(var i=0;i<length;i++)
				{
					var prnNo=arrCombo[i]['prnNo'];
					var itemId=arrCombo[i]['itemId'];	
					var maincatId=arrCombo[i]['maincatId'];	
					var mainCatName=arrCombo[i]['mainCatName'];
					var subCatId=arrCombo[i]['subCatId'];
					var subCatName=arrCombo[i]['subCatName'];	
					var code=arrCombo[i]['code'];	
					var itemName=arrCombo[i]['itemName'];	
					var prnQty=parseFloat(arrCombo[i]['prnQty']);	
					var mrnNos=arrCombo[i]['mrnNos'];	
					var dblMrnQty=parseFloat(arrCombo[i]['dblMrnQty']);	
					var minNos=arrCombo[i]['minNos'];	
					var dblIssuedQty=parseFloat(arrCombo[i]['dblIssuedQty']);
					var HOStockQty=parseFloat(arrCombo[i]['HOStockQty']);
					var AllStockQty=parseFloat(arrCombo[i]['AllStockQty']);
					
			var content='<tr class="normalfnt">';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+prnNo+'">'+prnNo+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'">'+mainCatName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'">'+subCatName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'">'+code+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'">'+itemName+'</td>';
			content +='<td align="right" bgcolor="#FFFFFF" id="'+prnQty+'">'+(prnQty)+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+mrnNos+'">'+mrnNos+'</td>';
			content +='<td align="right" bgcolor="#FFFFFF" id="'+dblMrnQty+'">'+(dblMrnQty)+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+minNos+'">'+minNos+'</td>';
			content +='<td align="right" bgcolor="#FFFFFF" id="'+dblIssuedQty+'" >'+(dblIssuedQty)+'</td>';
			content +='<td align="right" bgcolor="#FFFFFF" id="'+HOStockQty+'" >'+(HOStockQty)+'</td>';
			content +='<td align="right" bgcolor="#FFFFFF" id="'+AllStockQty+'" >'+(AllStockQty)+'</td>';
			content +='</tr>';
		
			add_new_row('#frmFabGPReport #tblMainGrid',content);
				}

			}
		});hideWaiting();
		
		$('#frmFabGPReport #txtPRN').val(selectedPRNstring)
}

function addSelectedPRNs()
{
	addClickedPRNRows();
	hideWaiting();
	hideWaiting();
}

//-------------------------------------
function alertx()
{
	$('#frmGrpPRN #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmGrpPRN #butDelete').validationEngine('hide')	;
}

function closePopUp(){
		hideWaiting();

}
//----------------------------------------------
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
//----------------------------------------------- 
function clearRows()
{
	//var rowCount = $('#tblDispatchPopup >tbody >tr').length;
	var rowCount = document.getElementById('tblPOItems').rows.length;
	//alert(rowCount);
	//var cellCount = document.getElementById('tblMain').rows[1].cells.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblPOItems').deleteRow(1);
			
	}
}
//------------------------------------------------------------
function checkAlreadySelected(){
	var rowCount = document.getElementById('tblPOItems').rows.length;
	for(var i=1;i<rowCount;i++)
	{
			var itemNo = 	document.getElementById('tblPOItems').rows[i].cells[4].id;
			var prnNo = 	document.getElementById('tblPOItems').rows[i].cells[1].innerHTML;
		
			var rowCount1 = document.getElementById('tblItemsPopup').rows.length;
			for(var j=1;j<rowCount1;j++)
			{
				var itemNoP = 	document.getElementById('tblItemsPopup').rows[j].cells[3].id;
				var prnNoP = 	document.getElementById('cboPRN').value;
				if((itemNo==itemNoP) && (prnNo==prnNoP)){
					document.getElementById('tblItemsPopup').rows[j].cells[0].childNodes[0].checked=true;
					document.getElementById('tblItemsPopup').rows[j].cells[0].childNodes[0].disabled=true;
				}
			}
	}
}
//---------------------------
function calTotals(){
	
	var rowCount = document.getElementById('tblPOItems').rows.length;
	var total=0;
	var totalDiscount=0;
	var grandTotal=0;
	for(var i=1;i<rowCount;i++)
	{
			var unitPrice = 	parseFloat(document.getElementById('tblPOItems').rows[i].cells[7].childNodes[0].value);
			var discount = 	parseFloat(document.getElementById('tblPOItems').rows[i].cells[8].childNodes[0].value);
			var Qty = 	parseFloat(document.getElementById('tblPOItems').rows[i].cells[10].childNodes[0].value);
			total+=Qty*(unitPrice);
			totalDiscount+=Qty*(discount/100)*unitPrice;
			
	 }
	grandTotal=total-totalDiscount;
	document.getElementById('divTotal').innerHTML=total.toFixed(4);;
	document.getElementById('divTotalDiscount').innerHTML=totalDiscount.toFixed(4);;
	document.getElementById('divGrandTotal').innerHTML=grandTotal.toFixed(4);;
}
//----------------------------------
function deleteAll(){
	//var rowCount = $('#tblDispatchPopup >tbody >tr').length;
	var rowCount = document.getElementById('tblMainGrid').rows.length;
	//alert(rowCount);
	//var cellCount = document.getElementById('tblMain').rows[1].cells.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblMainGrid').deleteRow(1);
			
	}
}
//---------------------------------------------
function updatePoHideStatus(prnNo,status){
	
		var data ='prnNo='+prnNo+'&status='+status;
		var url 		= basePath+"rpt_groupPrn-db-set.php?requestType=updatePoHideStatus";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:data,
			async:false,
			success:function(json){
				
			}
		});
	
}
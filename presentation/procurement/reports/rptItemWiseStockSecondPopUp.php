<?php

session_start();
$backwardseperator = "../../../";
include "{$backwardseperator}dataAccess/Connector.php";


$mainPath = $_SESSION['mainPath'];
$locationId = $_SESSION['CompanyID'];
$intUser = $_SESSION["userId"];
$company = $_SESSION['headCompanyId'];
$thisFilePath = $_SERVER['PHP_SELF'];

$item = $_REQUEST['item'];
$orderString = $_REQUEST['orderString'];
$orderArray = explode('-', $orderString);
$orderNo = $orderArray[0];
$orderYear = $orderArray[1];


?>
<head>
    <title>Details about items</title>
</head>

<body>
<style type="text/css"></style>
<form id="frmOrderDetailsPopup" name="frmOrderDetailsPopup" method="post">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="3"></td>
        </tr>
        <tr>
            <td width="20%"></td>
            <td width="60%" height="80"
                valign="top"><?php include '../../../presentation/procurement/reports/reportHeader.php'; ?></td>
            <td width="20%"></td>
        </tr>
        <tr height ="40">
            <td colspan="3">
                <div id="divPoNo" style="display:none"><?php echo $orderNo ?>/<?php echo $orderYear ?></div>
            </td>
        </tr>
    </table>


    <div align="center">
        <div style="background-color:#FFF"><strong>Item issue details for a selected order</strong></div>
        <div align="center">
            <table width="100%" border="0">
                <tr height="40">
                <td colspan="4"></td>
                </tr>
                <tr>
                    <td width="15%"></td>
                    <td width="15%">Order No / Order Year :</td>
                    <td width="15%"><?php echo $orderString;?></td>
                    <td></td>
                </tr>
                <tr height="40">
                    <td colspan="4"></td>
                </tr>
            </table>
        </div>


        <table width="1000" class="table-bordered" id="tblItemsPopup" cellpadding="0" cellspacing="0" border="1">
            <thead>
            <tr height="50" bgcolor="#7fffd4">
                <th width="8%" class="tbheader">Issue No</th>
                <th width="8%" class="tbheader">Issue Year</th>
                <th width="20%" class="tbheader">MRN No/Year</th>
                <th width="8%" class="tbheader">Issue Qty</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $sql = getIssueDetailsForOrder($orderNo, $orderYear, $item);
            $result = $db->RunQuery($sql);
            while ($row = mysqli_fetch_array($result)) {
                $qty = round($row['qty'],4);
                ?>
                <tr height="30" bgcolor="#f0ffff">
                    <td width="8%" align="center"><?php echo $row['issueNo']; ?></td>
                    <td width="8%" align="center"><?php echo $row['issueYear']; ?></td>
                    <td width="20%" align="center"><?php echo $row['mrn_no']; ?></td>
                    <td width="8%" align="center"><?php echo $qty; ?></td>
                </tr>
                <?php
            }
            ?>
            </tbody>
        </table>
    </div>
</form>
</body>


<?php
function getIssueDetailsForOrder($orderNo, $orderYear, $item)
{

    $sql = "SELECT
	ware_issuedetails.intIssueNo as issueNo,
    ware_issuedetails.intIssueYear as issueYear,
	GROUP_CONCAT(DISTINCT ware_issuedetails.intMrnNo, '/' ,ware_issuedetails.intMrnYear) as mrn_no,	
	SUM(ware_issuedetails.dblQty) as qty
FROM
	ware_issuedetails
INNER JOIN ware_issueheader ON ware_issueheader.intIssueNo = ware_issuedetails.intIssueNo
AND ware_issueheader.intIssueYear = ware_issuedetails.intIssueYear
WHERE
 	ware_issuedetails.strOrderNo = '$orderNo'
AND 
 ware_issuedetails.intOrderYear = '$orderYear'
AND 
 ware_issuedetails.intItemId = '$item'
AND 
ware_issueheader.intStatus = '1'
GROUP BY
ware_issuedetails.intIssueNo,
ware_issuedetails.intIssueYear;
			";

    return $sql;


}

?>

<?php
(define('UNLOCKPAGE', true) ? die('<<< Access denied >>>') : '');
include_once "libraries/jqgrid2/inc/jqgrid_dist.php";

ini_set('max_execution_time', 11111111);
//$menuId = '220';
//$reportMenuId = '934';

if ($_REQUEST['q'] == 221) {
    $menuId = '220';
    $title = "Raw Materials";
    $programCode = 'P0220';
    $reportMenuId = '934';
    $programName = 'Purchase Order - ' . $title;
    $menuSpecialId = '20';
    $poType = '1';
} else if ($_REQUEST['q'] == 1152) {
    $menuId = '1151';
    $title = "Non-Raw Materials";
    $programCode = 'P1151';
    $reportMenuId = '1156';
    $programName = 'Purchase Order - ' . $title;
    $menuSpecialId = '20';
    $poType = '2';
} else if ($_REQUEST['q'] == 1154) {
    $menuId = '1153';
    $title = "RM and Non-RM";
    $programCode = 'P1153';
    $reportMenuId = '1157';
    $programName = 'Purchase Order - ' . $title;
    $menuSpecialId = '20';
    $poType = '3';
}


################################################################
################## new part ####################################
################################################################
///// create where part ///////////////////
$arr = json_decode($_REQUEST['filters'], true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];
//print_r($arr);
$where_string = '';
$where_item_code = '';
$where_array = array(
    'Status' => 'tb1.intStatus',
    'Order_No' => 'tb1.intPONo',
    'Order_Year' => 'tb1.intPOYear',
    'PO_Date' => 'tb1.dtmPODate',
    'Shipment_Term' => 'mst_shipmentterms.strName',
    'Delivery_Date' => 'tb1.dtmDeliveryDate',
    'Supplier' => 'mst_supplier.strName',
    'User' => 'sys_users.strUserName'
);
$arr_status = array('Approved' => '1', 'Rejected' => '0', 'Pending' => '2', 'Cancelled' => '-10');
foreach ($arr as $k => $v) {
    if ($v['field'] == 'Status') {
        if ($arr_status[$v['data']] == 2)
            $where_string .= "AND  " . $where_array[$v['field']] . " >1 ";
        else
            $where_string .= "AND  " . $where_array[$v['field']] . " = '" . $arr_status[$v['data']] . "' ";
    } else if ($where_array[$v['field']])
        $where_string .= "AND  " . $where_array[$v['field']] . " like '%" . $v['data'] . "%' ";
}
if (!count($arr) > 0)
    $where_string .= "AND date(tb1.dtmPODate) = '" . date('Y-m-d') . "'";

################## end code ####################################
//include_once "libraries/jqgrid2/inc/jqgrid_dist2.php";

$company = $_SESSION['headCompanyId'];
$location = $_SESSION['CompanyID'];

$intUser = $_SESSION["userId"];

$userDepartment = getUserDepartment($intUser);
$approveLevel = (int)getMaxApproveLevel();

//---------------------------------------
$strShipments = ":All";
$sql = "SELECT
		mst_shipmentterms.strName 
		FROM mst_shipmentterms 
		Order by mst_shipmentterms.strName asc";
$result = $db->RunQuery($sql);
while ($row = mysqli_fetch_array($result)) {
    $term = $row['strName'];
    $strShipments .= ";$term:$term";
}
//---------------------------------------

$sql = "select  * from(SELECT DISTINCT 
							if(tb1.intStatus=1,'Approved',if(tb1.intStatus=0,'Rejected',if(tb1.intStatus=-10,'Cancelled','Pending'))) as Status,
							tb1.intPONo as `Order_No`, 
							mst_shipmentterms.strName as `Shipment_Term`,
							tb1.intPOYear as `Order_Year`,
							
							(SELECT group_concat( DISTINCT strCode SEPARATOR ' / ')	FROM trn_podetails AS po INNER JOIN mst_item ON po.intItem = mst_item.intId WHERE po.intPONo = tb1.intPONo AND po.intPOYear = tb1.intPOYear ) AS item_code,
							tb1.dtmPODate as `PO_Date`,
							tb1.dtmDeliveryDate as `Delivery_Date`,
							mst_supplier.strName as `Supplier`,
							
							IFNULL((SELECT
							Sum(trn_podetails.dblQty)
							FROM trn_podetails
							WHERE
							trn_podetails.intPONo =  tb1.intPONo AND
							trn_podetails.intPOYear =  tb1.intPOYear),0) as `PO_Qty`,
							
							IFNULL((SELECT
							Sum(ware_grndetails.dblGrnQty-ware_grndetails.dblRetunSupplierQty)
							FROM
							ware_grnheader
							Inner Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear
							WHERE
							ware_grnheader.intStatus =  '1' AND
							ware_grnheader.intPoNo =  tb1.intPONo AND
							ware_grnheader.intPoYear =  tb1.intPOYear),0) as `GRN_Qty`,
							
							concat(((IFNULL((SELECT
							Sum(ware_grndetails.dblGrnQty-ware_grndetails.dblRetunSupplierQty)
							FROM
							ware_grnheader
							Inner Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear
							WHERE
							ware_grnheader.intStatus =  '1' AND
							ware_grnheader.intPoNo =  tb1.intPONo AND
							ware_grnheader.intPoYear =  tb1.intPOYear),0))/((SELECT
							Sum(trn_podetails.dblQty)
							FROM trn_podetails
							WHERE
							trn_podetails.intPONo =  tb1.intPONo AND
							trn_podetails.intPOYear =  tb1.intPOYear)))*100,'%') as `Progress`,
							
							sys_users.strUserName as `User`, 
							  
							IFNULL((
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(trn_poheader_approvedby.dtApprovedDate),')' )
								FROM
								trn_poheader_approvedby
								Inner Join sys_users ON trn_poheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								trn_poheader_approvedby.intPONo  = tb1.intPONo AND
								trn_poheader_approvedby.intYear =  tb1.intPOYear AND
								trn_poheader_approvedby.intApproveLevelNo =  '1' AND 
							    trn_poheader_approvedby.intStatus='0'
							),IF(((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.intStatus>1),'Approve', '')) as `1st_Approval`,  ";

for ($i = 2; $i <= $approveLevel; $i++) {

    if ($i == 2) {
        $approval = "2nd_Approval";
    } else if ($i == 3) {
        $approval = "3rd_Approval";
    } else {
        $approval = $i . "th_Approval";
    }


    $sql .= "IFNULL(
						/*condition*/
(
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(trn_poheader_approvedby.dtApprovedDate),')' )
								FROM
								trn_poheader_approvedby
								Inner Join sys_users ON trn_poheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								trn_poheader_approvedby.intPONo  = tb1.intPONo AND
								trn_poheader_approvedby.intYear =  tb1.intPOYear AND
								trn_poheader_approvedby.intApproveLevelNo =  '$i' AND 
							    trn_poheader_approvedby.intStatus='0'
							),
							/*end condition*/
							
							/*false part*/

							IF(
							/*if condition */
							((SELECT
								menupermision.int" . $i . "Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND (tb1.intStatus>1) AND (tb1.intStatus<=tb1.intApproveLevels) AND ((SELECT
								max(trn_poheader_approvedby.dtApprovedDate) 
								FROM
								trn_poheader_approvedby
								Inner Join sys_users ON trn_poheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								trn_poheader_approvedby.intPONo  = tb1.intPONo AND
								trn_poheader_approvedby.intYear =  tb1.intPOYear AND
								trn_poheader_approvedby.intApproveLevelNo =  ($i-1) AND 
							    trn_poheader_approvedby.intStatus='0')<>'')),
								
								/*end if condition*/
								/*if true part*/
								'Approve',
								/*fase part*/
								 if($i>tb1.intApproveLevels,'-----',''))
								
								/*end IFNULL false part*/
								) as `" . $approval . "`, ";

}

//existing next approve permission or (last app user= login user && last app user have permission to reject)

$sql .= "IF(((SELECT
								menupermision.int2Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.intStatus>1) OR (((select tbl.intApproveUser from  (SELECT
								Max(trn_poheader_approvedby.intApproveLevelNo),
								trn_poheader_approvedby.intPONo,
								trn_poheader_approvedby.intYear,
								trn_poheader_approvedby.intApproveUser,
								trn_poheader_approvedby.dtApprovedDate
								FROM trn_poheader_approvedby
								WHERE
								trn_poheader_approvedby.intStatus =  '0') as tbl)=$intUser) AND (tb1.intStatus>1)),'Reject','') as Reject, ";


$sql .= "IF(((SELECT
								menupermision.intRevise 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode =  '$programCode' AND
								menupermision.intUserId =  '$intUser' AND  
								tb1.intStatus<=tb1.intApproveLevels AND 
								tb1.intStatus=1) =1),
								
								/*if true part*/
								  IF(((SELECT
	IFNULL(SUM(trn_podetails.dblQty),0) -(SELECT
IFNULL(SUM(ware_grndetails.dblGrnQty-ware_grndetails.dblRetunSupplierQty),0)
FROM
ware_grnheader
INNER JOIN ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear
WHERE
ware_grnheader.intPoNo = trn_poheader.intPONo AND
ware_grnheader.intPoYear = trn_poheader.intPOYear 
AND ware_grnheader.intStatus = 1) 
							FROM
							trn_poheader
							INNER JOIN trn_podetails ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear
         WHERE trn_podetails.intPONo = tb1.intPONo AND trn_podetails.intPOYear = tb1.intPOYear)>0),'Revise',''),
								 
								 /*fase part*/
								  ''
								  
								  )  as `Revise`, ";

$sql .= "IF(((SELECT
									menus_special_permision.intSpMenuId
									FROM
									menus_special
									INNER JOIN menus_special_permision ON menus_special.intId = menus_special_permision.intSpMenuId
									WHERE
									menus_special_permision.intUser = '$intUser' AND
									menus_special.intId = '$menuSpecialId') ='') || (tb1.intStatus !=1),
								
								/*if true part*/
								   '', 
								 
								/*fase part*/
								   'Print'
								  
								  )  as `PRINT`,  ";

$sql .= "'View' as `View`,
						    
						      (
                                SELECT
                                GROUP_CONCAT( DISTINCT mst_department.strName SEPARATOR ' / ')
                                FROM
                                trn_podetails
                                LEFT JOIN mst_department ON trn_podetails.CostCenter = mst_department.intId
                                INNER JOIN mst_department_heads ON mst_department_heads.DEPARTMENT_ID = mst_department.intId
                                INNER JOIN mst_locations ON mst_department_heads.LOCATION_ID = mst_locations.intId
                                where trn_podetails.intPONo =tb1.intPONo  AND trn_podetails.intPOYear =tb1.intPOYear
                              
                                )AS CostCenter  
						FROM
							trn_poheader as tb1
							Inner Join mst_supplier ON tb1.intSupplier = mst_supplier.intId
							Inner Join sys_users ON tb1.intUser = sys_users.intUserId 
							Inner Join mst_locations ON tb1.intCompany = mst_locations.intId  
							Inner Join mst_shipmentterms ON tb1.intShipmentTerm = mst_shipmentterms.intId
							WHERE
							/*tb1.PO_TYPE	='$poType' AND */
							FIND_IN_SET(tb1.PO_TYPE,'$poType') AND  
							mst_locations.intCompanyId =  '$company' AND 
							tb1.intCompany =  '$location' 
							$where_string
							)  as t where 1=1
						";
	//echo $sql;

$col = array();

//STATUS
$col["title"] = "Status"; // caption of column
$col["name"] = "Status"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "2";
//edittype
$col["stype"] = "select";
$str = ":All;Pending:Pending;Approved:Approved;Rejected:Rejected;Cancelled:Cancelled";
$col["editoptions"] = array("value" => $str);
//searchOper
$col["align"] = "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$cols[] = $col;
$col = NULL;

//PO No
$col["title"] = "PO No"; // caption of column
$col["name"] = "Order_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "2";
//searchOper
$col["align"] = "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$col['link'] = "?q=$menuId&requestType=URLGetHeader&poNo={Order_No}&year={Order_Year}";
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag

$reportLink = "?q=$reportMenuId&poNo={Order_No}&year={Order_Year}&mode=view";
$reportLinkApprove = "?q=$reportMenuId&poNo={Order_No}&year={Order_Year}&approveMode=1";
$reportLinkPrint = "?q=$reportMenuId&poNo={Order_No}&year={Order_Year}&mode=print";

$cols[] = $col;
$col = NULL;


//PO Year
$col["title"] = "PO Year"; // caption of column
$col["name"] = "Order_Year"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;
$col = NULL;


$col["title"] = "PRN NOs"; // caption of column
$col["name"] = "PRN_nos"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;
$col = NULL;


//Delivery Date
$col["title"] = "Purchase Date"; // caption of column
$col["name"] = "PO_Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;
$col = NULL;


//SHIPMENT TERM
$col["title"] = "Shipment Term"; // caption of column
$col["name"] = "Shipment_Term"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "4";
//edittype
$col["stype"] = "select";
$str = $strShipments;
$col["editoptions"] = array("value" => $str);
//searchOper
$col["align"] = "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$cols[] = $col;
$col = NULL;


//Delivery Date
$col["title"] = "Delivery Date"; // caption of column
$col["name"] = "Delivery_Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;
$col = NULL;


//Supplier
$col["title"] = "Supplier"; // caption of column
$col["name"] = "Supplier"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "left";
$cols[] = $col;
$col = NULL;


//PO Qty
$col["title"] = "PO Qty"; // caption of column
$col["name"] = "PO_Qty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "right";
$cols[] = $col;
$col = NULL;


//GRN Qty
$col["title"] = "GRN Qty"; // caption of column
$col["name"] = "GRN_Qty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "right";
$cols[] = $col;
$col = NULL;


//Pogress
$col["title"] = "Progress"; // caption of column
$col["name"] = "Progress"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "right";
$cols[] = $col;
$col = NULL;

//CostCenter  //ticket :-2260  //Hasitha
$col["title"] = "Cost Center"; // caption of column
$col["name"] = "CostCenter"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "5";
$col["align"] = "center";
$cols[] = $col;
$col = NULL;

//User
$col["title"] = "User"; // caption of column
$col["name"] = "User"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;
$col = NULL;

//Material code
$col["title"] = "Item Code "; // caption of column
$col["name"] = "item_code"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;
$col = NULL;

//FIRST APPROVAL
$col["title"] = "1st Approval"; // caption of column
$col["name"] = "1st_Approval"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link'] = $reportLinkApprove;
$col['linkName'] = 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;
$col = NULL;

for ($i = 2; $i <= $approveLevel; $i++) {
    if ($i == 2) {
        $ap = "2nd Approval";
        $ap1 = "2nd_Approval";
    } else if ($i == 3) {
        $ap = "3rd Approval";
        $ap1 = "3rd_Approval";
    } else {
        $ap = $i . "th Approval";
        $ap1 = $i . "th_Approval";
    }
//SECOND APPROVAL
    $col["title"] = $ap; // caption of column
    $col["name"] = $ap1; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
    $col["width"] = "3";
    $col["search"] = false;
    $col["align"] = "center";
    $col['link'] = $reportLinkApprove;
    $col['linkName'] = 'Approve';
    $col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
    $cols[] = $col;
    $col = NULL;
}

//FIRST APPROVAL
$col["title"] = "Reject"; // caption of column
$col["name"] = "Reject"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["search"] = false;
$col["align"] = "center";
$col['link'] = $reportLinkApprove;
$col['linkName'] = 'Reject';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;
$col = NULL;

//REVISE
$col["title"] = "Revise"; // caption of column
$col["name"] = "Revise"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col['link'] = "#";
$col["linkoptions"] = " class='butRevise'"; // extra params with <a> tag
$col["search"] = false;
$col["align"] = "center";
$cols[] = $col;
$col = NULL;

//PRINT
$col["title"] = "Print"; // caption of column
$col["name"] = "PRINT"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["search"] = false;
$col["align"] = "center";
$col['link'] = $reportLinkPrint;
$col['linkName'] = 'Print';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;
$col = NULL;


//VIEW
$col["title"] = "Report"; // caption of column
$col["name"] = "View"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["search"] = false;
$col["align"] = "center";
$col['link'] = $reportLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;
$col = NULL;

$jq = new jqgrid('', $db);

$grid["caption"] = "Purchase Order Listing - " . $title;
$grid["multiselect"] = false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] = 20; // by default 20
$grid["sortname"] = 'Order_No'; // by default sort grid by this field
$grid["sortorder"] = "DESC"; // ASC or DESC
$grid["autowidth"] = true; // expand grid to screen width
$grid["multiselect"] = false; // allow you to multi-select through checkboxes


// export XLS file
// export to excel parameters - range could be "all" or "filtered"
//$grid["export"] = array("format"=>"xlsx", "filename"=>"my-file", "sheetname"=>"test");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

$jq->select_command = $sql;
$jq->set_columns($cols);
$jq->set_actions(array(
        "add" => false, // allow/disallow add
        "edit" => false, // allow/disallow edit
        "delete" => false, // allow/disallow delete
        "rowactions" => false, // show/hide row wise edit/del/save option
        "search" => "advance", // show single/multi field search condition (e.g. simple or advance)
        "export" => true
    )
);

$out = $jq->render("list1");
//include "include/listing.html";

?>
<title>Purchase Order Listing - <?php echo $title; ?></title>
<script type="text/javascript" src="presentation/procurement/purchaseOrder/listing/rptPurchaseOrder-js.js"></script>
<?php echo $out; ?>

<?php

//------------------------------function load Default Department---------------------
function getMaxApproveLevel()
{
    global $db;

    //echo $savedStat;
    $appLevel = 0;
    $sqlp = "SELECT
			Max(trn_poheader.intApproveLevels) AS appLevel
			FROM trn_poheader";

    $resultp = $db->RunQuery($sqlp);
    $rowp = mysqli_fetch_array($resultp);

    return $rowp['appLevel'];
}

//------------------------------function load User Department---------------------
function getUserDepartment($intUser)
{
    global $db;

    $sqlp = "SELECT
			sys_users.intDepartmentId
			FROM
			sys_users
			WHERE
			sys_users.intUserId =  '$intUser'";

    $resultp = $db->RunQuery($sqlp);
    $rowp = mysqli_fetch_array($resultp);

    return $rowp['intDepartmentId'];

}

?>

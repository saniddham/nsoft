<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 		= $_SESSION['mainPath'];
	$userId 		= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	$location 		= $_SESSION['CompanyID'];
	$company 		= $_SESSION['headCompanyId'];
	
	require_once "{$backwardseperator}dataAccess/Connector.php";
	require_once "../../../../class/procurement/purchaseOrder/cls_purchase_order_get.php";
 	require_once "../../../../class/cls_commonFunctions_get.php";
	require_once "../../../../class/finance/budget/cls_common_function_budget_get.php";
	require_once "../../../../class/finance/budget/entering/cls_budget_get.php";
	require_once "../../../../class/finance/budget/entering/cls_budget_set.php";
 
 
 	$obj_poGet				= new Cls_purchase_order_Get($db); 
	$obj_common				= new cls_commonFunctions_get($db);
	$obj_comm_budget_get	= new cls_common_function_budget_get($db);
	$obj_budget_get			= new cls_budget_get($db);
	$obj_budget_set			= new cls_budget_set($db);


	$programName='Purchase Order';
	$programCode='P0220';

	$poNo1  = $_REQUEST['poNo'];
	$poNoArray1=explode("/",$poNo1);
	$poNo1=$poNoArray1[0];
	$poYear1=$poNoArray1[1];


	$sql = "SELECT trn_poheader.intStatus, trn_poheader.intUser, trn_poheader.intSupplier, trn_poheader.intApproveLevels , PO_TYPE FROM trn_poheader WHERE (`intPONo`='$poNo1') AND (`intPOYear`='$poYear1')";
	$results = $db->RunQuery($sql);
	$row = mysqli_fetch_array($results);
	$po_type = $row['PO_TYPE'];
	if($po_type==1){
		$title		= "Raw Materials";
		$programCode='P0220';
		$programName= 'Purchase Order';
	}
	else if($po_type==2){
		$title		= "None Raw Materials";
		$programCode='P1151';
		$programName= 'Purchase Order - None Raw Materials';
	}
	else if($po_type==3){
		$title		= "All";
		$programCode='P1153';
		$programName= 'Purchase Order - All';
	}


 if($requestType=='getValidation')
	{
		$db->begin();
		$poNo  = $_REQUEST['poNo'];
		$poNoArray=explode("/",$poNo);
		$poNo=$poNoArray[0];
		$poYear=$poNoArray[1];
		
		$sql = "SELECT
		trn_poheader.intStatus, 
		trn_poheader.intApproveLevels   ,
		trn_poheader.intShipmentTerm ,
		trn_poheader.intDeliveryTo ,
		date(trn_poheader.dtmPODate) as POdate  
		FROM 
		trn_poheader 
		WHERE
		trn_poheader.intPONo =  '$poNo' AND
		trn_poheader.intPOYear =  '$poYear'";

		$result 		= $db->RunQuery2($sql);
		$row			= mysqli_fetch_array($result);
		$approveLevels	= $row['intApproveLevels'];
		$status			= $row['intStatus'];
		$shipmentTerm	= $row['intShipmentTerm'];
		$deliveryToLoc	= $row['intDeliveryTo'];
		$POdate			= $row['POdate']; 
		
		//echo 'ccc';
		 $sql = "SELECT
				trn_podetails.dblQty,
				trn_podetails.intItem,
				sum(trn_prndetails.dblPrnQty) as dblPrnQty,
				(
				SELECT
				Sum(pod.dblQty)
				FROM
				trn_podetails AS pod
				INNER JOIN trn_poheader AS po ON pod.intPONo = po.intPONo AND pod.intPOYear = po.intPOYear
				INNER JOIN trn_prndetails AS prn ON pod.intPrnNo = prn.intPrnNo AND pod.intPrnYear = prn.intYear   and prn.intItem= pod.intItem
				WHERE
				pod.intPrnNo = trn_podetails.intPrnNo AND
				pod.intPrnYear = trn_podetails.intPrnYear AND
				pod.intItem = trn_prndetails.intItem AND
				po.intStatus <= po.intApproveLevels AND
				po.intStatus >0) as dblPoQty,
				trn_podetails.intPrnNo,
				trn_podetails.intPrnYear,
				mst_item.strName, 
				trn_poheader.intApproveLevels,
				trn_poheader.intStatus ,
				trn_poheader.intShipmentTerm  
				FROM
				trn_podetails
				left Join trn_prndetails ON trn_podetails.intPrnNo = trn_prndetails.intPrnNo AND trn_podetails.intPrnYear = trn_prndetails.intYear 
				Inner Join mst_item ON trn_prndetails.intItem = mst_item.intId
				WHERE 
				mst_item.intStatus = '1' AND 
				trn_podetails.intPONo =  '$poNo' AND
				trn_podetails.intPOYear =  '$poYear' 
				GROUP BY 
				trn_podetails.intPONo  ,
				trn_podetails.intPOYear,
				trn_podetails.intItem 
				";
		
		$result = $db->RunQuery2($sql);
		$errorFlg=0;
		$msg ="Maximum Qtys for items"; 
		while($row=mysqli_fetch_array($result))
		{
			$poQty			=$row['dblQty'];
			$poItem			= $row['intItem'];
			if($row['intApproveLevels']>=$row['intStatus']){
			$maxPoQty		= $row['dblPrnQty']-$row['dblPoQty']+$poQty;
			$maxPoQty1		=$row['dblPrnQty']."s".$row['dblPoQty']."s".$row['dblQty'];
			}
			else{
			$maxPoQty		= $row['dblPrnQty']-$row['dblPoQty'];
			$maxPoQty1="t";
			}
			$prnNo= $row['intPrnNo'];
			$prnYear= $row['intYear'];
			if($poQty>$maxPoQty)
				$errorFlg=1;
				$msg .="\n ".$prnNo."/".$prnYear."-".$row['strName']." =".$maxPoQty;
		}
	$confirmatonMode=loadConfirmatonMode($programCode,$status,$approveLevels,$userId,'RunQuery2');
 	//$finalApprovalPermision=$obj_poGet->get_finalApprovalPermision($poYear,$poNo,$shipmentTerm,$userId);
 	$finalApprovalPermision=$obj_poGet->get_finalApprovalPermision_2($poYear,$poNo,$shipmentTerm,$userId,'RunQuery2');
	$finalApprovalPermision=1;		 
	if($finalApprovalPermision!=1 && $status==2){
		$confirmatonMode=0;
	}
		
	if($status==1 && $errorFlg==0){
		$errorFlg=1;
		$msg ="Final confirmation of this PO is already raised"; 
	}	
	else if($status==0 && $errorFlg==0){// 
		$errorFlg = 1;
		$msg ="This PO is rejected"; 
	}
	else if($confirmatonMode==0 && $errorFlg==0){// 
		$errorFlg = 1;
		$msg ="No Permission to Approve"; 
	}
	
	

		//--2014-05-12-------Budget Validation-------------------
		$resp_save_array					= NULL;
		$arr_item_u							= NULL;
		$arr_item_a							= NULL;
		$arr_sub_cat_u						= NULL;
		$arr_sub_cat_a						= NULL; 
		
		$resp_save_array['arr_item_u']		= $arr_item_u;
		$resp_save_array['arr_item_a']		= $arr_item_a;
		$resp_save_array['arr_sub_cat_u']	= $arr_sub_cat_u;
		$resp_save_array['arr_sub_cat_a']	= $arr_sub_cat_a;
		
		$results_details	=$obj_poGet->get_details_service_po_results($poNo,$poYear,'RunQuery2');
		
		$resp_save_array	= $obj_comm_budget_get->load_save_to_array($results_details,$location,$resp_save_array,'SERVICE_PO',$poNo,$poYear,'RunQuery2');
		$results_fin_year	= $obj_comm_budget_get->loadFinanceYear_with_selected_date($company,$POdate,'RunQuery2');
		$row_fin_year		= mysqli_fetch_array($results_fin_year);
		$budg_year			= $row_fin_year['intId'];
		$fields 			= explode('-', $POdate);
		$budg_month 		= $fields['1'];
		$resp_budg_validate	= $obj_comm_budget_get->get_validate_budget($location,'',$budg_year,$budg_month,$resp_save_array,'SERVICE_PO',$obj_common,'RunQuery2');
		//print_r($resp_budg_validate);
		
		if($resp_budg_validate['type'] == 'fail' && $errorFlg==0){
			 $errorFlg					=1;
			 $msg						= $resp_budg_validate['msg'];
			 $sqlM						= $resp_budg_validate['sql'];
		}
		//--End of 2014-05-12-------Budget Validation-------------------
	//$errorFlg = 1;
	
	//---------------------- budget PO Validation (Lahiru : 2014/06/18) ------------------------------
	$checkBudgetLoc		= $obj_poGet->checkBudgetValidatedLocation($deliveryToLoc,'RunQuery2');
	if($checkBudgetLoc && $errorFlg==0)
	{
		$detail_result	= $obj_poGet->get_details_results($poNo,$poYear,'RunQuery2');
		while($row = mysqli_fetch_array($detail_result))
		{
			$validateArr	= validateBudgetBalance($poNo,$poYear,$deliveryToLoc,$company,$location,$row['ITEM_ID'],$row['QTY']);
			if($validateArr['Flag']==1 && $errorFlg==0)
			{
				$errorFlg		= 1;
				$msg			= $validateArr['msg'];
			}
		}	
	}
	//---------------------------------------------------------------------------------------------
	
	if($errorFlg==1){
		$db->rollback();
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
		$response['sql'] 	= $sqlM;
	}
	else{
		$db->commit();
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		$db->CloseConnection();		
		echo json_encode($response);
	}
	
else if($requestType=='validateRejecton')
	{
		$errorFlg=0;
		$poNo  = $_REQUEST['poNo'];
		$poNoArray=explode("/",$poNo);
		$poNo=$poNoArray[0];
		$poYear=$poNoArray[1];
		
		$sql = "SELECT
		trn_poheader.intStatus, 
		trn_poheader.intApproveLevels  
		FROM 
		trn_poheader 
		WHERE
		trn_poheader.intPONo =  '$poNo' AND
		trn_poheader.intPOYear =  '$poYear'";


		$result = $db->RunQuery($sql);
		$errorFlg=0;
		$msg =""; 
		$row=mysqli_fetch_array($result);
		
		//$rejectionMode=loadRejectionMode($programCode,$row['intStatus'],$row['intApproveLevels'],$userId);--10/01/2013
		$rejectMode=rejectMode($userId,$row['intApproveLevels'],$poNo,$poYear,$row['intStatus']);			 
		$confirmMode=loadConfirmatonMode($programCode,$row['intStatus'],$row['intApproveLevels'],$userId,'RunQuery');
		
		if($row['intStatus']==1){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="Final confirmation of this PO is already raised.So cant reject"; 
		}
		else if($row['intStatus']==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="This PO is already Rejected"; 
		}
		/*else if($rejectionMode==0){//no confirmation has been raised  //commented on 10/01/2013
			$errorFlg = 1;
			$msg ="No Permission to reject this PO"; 
		}*/
		else if(($rejectMode==0) && ($confirmMode!=1)){//no confirmation has been raised  //added on on 10/01/2013   (1)
			$errorFlg = 1;
			$msg ="Approved user should reject this PO and you have no permission to Approve this also."; 
		}
		else if(($rejectMode==0) &&($confirmMode==0)){//no confirmation has been raised  //added on on 10/01/2013  (2)
			$errorFlg = 1;
			$msg ="No Permission to reject this PO"; 
		}
		
		else{//confirmation has been raised
			$errorFlg = 0;
			
		}

	
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);

	}
	
//------------------------------function loadRejectionMode-------------------
function loadRejectionMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$rejectMode=0;
	$sqlp = "SELECT
		menupermision.intReject 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intReject']==1){
	 if($intStatus!=0){
	 $rejectMode=1;
	 }
	}
	 
	return $rejectMode;
}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
			 
	return $editMode;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser,$executeType){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	 $sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->$executeType($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
//------------------------------function load loadrejMode---------------------
function rejectMode($intUser,$savedLevels,$poNo,$year,$intStatus){
	global $db;
	
	$approveLevel=$savedLevels+1-$intStatus;
	
	//echo $savedStat;
	$rejMode=0;
	  	$sql = "SELECT
				menus_special.intStatus
FROM
				menus_special
				Inner Join menus_special_permision ON menus_special.intId = menus_special_permision.intSpMenuId
WHERE
menus_special_permision.intUser =  '$intUser' AND
menus_special.strPermisionType =  'Permission to reject PO for user who approved the PO'";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$rejMode 	= $row['intStatus'];
	
	
	$sqlp = "SELECT
				trn_poheader_approvedby.dtApprovedDate
				FROM trn_poheader_approvedby
				WHERE
				trn_poheader_approvedby.intPONo =  '$poNo' AND
				trn_poheader_approvedby.intYear =  '$year' AND
				trn_poheader_approvedby.intApproveUser =  '$intUser' AND
				trn_poheader_approvedby.intApproveLevelNo =  '$approveLevel'";	
	
	 $resultp = $db->RunQuery($sqlp);
	 $rowp=mysqli_fetch_array($resultp);
	 $lastApprovalFromUser=0;
	 if($rowp['dtApprovedDate']!=''){
	 $lastApprovalFromUser=1;
	 }
	 
	 $val=0;
	 
	 if(($intStatus>1) && ($approveLevel>0) && ($rejMode==1) && ($lastApprovalFromUser==1)){
		 $val=1;
	 }
			 
	return $val;
}
////---------------------------------------------------------
function validateBudgetBalance($poNo,$poYear,$deliveryToLoc,$company,$location,$poItem,$poQty)
{
	global $obj_poGet;
	global $obj_comm_budget_get;
	global $obj_common;
	
	$PO_item_tot_U			= 0;
	$PO_sub_cat_tot_U		= 0;
	$PO_item_tot_A			= 0;
	$PO_sub_cat_tot_A		= 0;
	
	$saveStatus 			= true;	
	$saveQty				= 0;
	
	$subCatId				= $obj_poGet->getSubCategory($poItem,'RunQuery2');
	$results_fin_year		= $obj_comm_budget_get->loadFinanceYear_with_selected_date($company,date('Y-m-d'),'RunQuery2');
	$row_fin_year			= mysqli_fetch_array($results_fin_year);
	$budg_year				= $row_fin_year['intId'];
	$sub_cat_budget_flag	= $obj_comm_budget_get->check_sub_category_budgeted_or_not($deliveryToLoc,$subCatId,'RunQuery2');
	
	$check					= $obj_comm_budget_get->checkBudgetApprovalDepWise($deliveryToLoc,'',$budg_year,'RunQuery2');
	if(!$check && $saveStatus)
	{
		$saveStatus			= false;
		$data['Flag']		= 1;
		$data['msg']		= "No approve budget for all department which belong to delivery to location and current finance year ";
	}
	if($sub_cat_budget_flag==0)
	{
		$budget_type		= $obj_comm_budget_get->load_budget_type($deliveryToLoc,$subCatId,$poItem,'RunQuery2');
		$budget_bal			= $obj_comm_budget_get->load_budget_balance($deliveryToLoc,'',$budg_year,'',$poItem,date('m'),'RunQuery2');
		$result_itemPrice	= $obj_comm_budget_get->get_price_for_item($location,$poItem,'RunQuery2');
		
		while($row = mysqli_fetch_array($result_itemPrice))
		{
			if($row['stockBal']>=$poQty)
			{
				$saveQty 	= $poQty;
				$poQty		= 0;
			}
			else if($poQty>$row['stockBal'])
			{
				$saveQty	= $row['stockBal'];
				$poQty		= $poQty-$saveQty;
			}
			if($saveQty>0)
			{
				if($budget_type == 'U')
					$PO_item_tot_U += $saveQty;
				else
					$PO_item_tot_A += $saveQty*$row['price'];
				
				$budget_bal_A 	= $budget_bal['balAmount'];
				
				if(($PO_item_tot_A > $budget_bal_A) && $saveStatus)
				{
					$saveStatus		= false;
					$data['Flag']	= 1;
					$data['msg']	= "There is no budget balance for this month for item '".$obj_common->get_item_name($poItem,'RunQuery2')."' - ".$budget_bal_A;
				}
				$budget_bal_U 	= $budget_bal['balUnits'];
				
				if(($PO_item_tot_U > $budget_bal_U) && $saveStatus)
				{
					$saveStatus		= false;
					$data['Flag']	= 1;
					$data['msg']	= "There is no budget balance for this month for item '".$obj_common->get_item_name($poItem,'RunQuery2')."' - ".$budget_bal_U;
				}	
			}
		}
	}
	else if($sub_cat_budget_flag==1)
	{
		$budget_type		= $obj_comm_budget_get->load_budget_type($deliveryToLoc,$subCatId,0,'RunQuery2');
		$budget_bal			= $obj_comm_budget_get->load_budget_balance($deliveryToLoc,'',$budg_year,$subCatId,0,date('m'),'RunQuery2');
		$result_itemPrice	= $obj_comm_budget_get->get_price_for_item($location,$poItem,'RunQuery2');
		
		while($row = mysqli_fetch_array($result_itemPrice))
		{
			if($row['stockBal']>=$poQty)
			{
				$saveQty 	= $poQty;
				$poQty		= 0;
			}
			else if($poQty>$row['stockBal'])
			{
				$saveQty	= $row['stockBal'];
				$poQty		= $poQty-$saveQty;
			}
			if($saveQty>0)
			{
				if($budget_type == 'U')
					$PO_sub_cat_tot_U += $saveQty;
				else
					$PO_sub_cat_tot_A += $saveQty*$row['price'];
				
				$budget_bal_A 	= $budget_bal['balAmount'];
				if(($PO_item_tot_A > $budget_bal_A) && $saveStatus)
				{
					$saveStatus		= false;
					$data['Flag']	= 1;
					$data['msg']	= "There is no budget balance for this month for item '".$obj_common->get_sub_category_name($poItem,'RunQuery2')."' - ".$budget_bal_A;
				}
				$budget_bal_U 	= $budget_bal['balUnits'];
				if(($PO_item_tot_U > $budget_bal_U) && $saveStatus)
				{
					$saveStatus		= false;
					$data['Flag']	= 1;
					$data['msg']	= "There is no budget balance for this month for item '".$obj_common->get_item_name($poItem,'RunQuery2')."' - ".$budget_bal_U;
				}
			}
		}
	}
	return $data;
}
?>
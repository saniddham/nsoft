<?php
	// ini_set('display_errors',1);
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$companyId 	= $_SESSION['CompanyID'];

	include "{$backwardseperator}dataAccess/Connector.php";
	//require_once "{$backwardseperator}libraries/mail/mail.php";
	require_once $_SESSION['ROOT_PATH']."class/cls_mail.php";
	require_once $_SESSION['ROOT_PATH']."class/common/azureDBconnection.php";

	$objMail = new cls_create_mail($db);
	$objAzure = new cls_azureDBconnection();


	/////////// parameters /////////////////////////////

	$poNo = $_REQUEST['poNo'];
	$year = $_REQUEST['year'];

	$sql = "SELECT trn_poheader.intStatus, trn_poheader.intUser, trn_poheader.intSupplier, trn_poheader.intApproveLevels, PO_TYPE FROM trn_poheader WHERE (`intPONo`='$poNo') AND (`intPOYear`='$year')";
	$results = $db->RunQuery($sql);
	$row = mysqli_fetch_array($results);
	$po_type = $row['PO_TYPE'];
	if($po_type==1){
        $title		= "Raw Materials";
        $programCode='P0220';
        $programName= 'Purchase Order';
    }
    else if($po_type==2){
        $title		= "None Raw Materials";
        $programCode='P1151';
        $programName= 'Purchase Order - None Raw Materials';
    }
    else if($po_type==3){
        $title		= "All";
        $programCode='P1153';
        $programName= 'Purchase Order - All';
    }

	$poApproveLevel = (int)getApproveLevel($programName);


	if($_REQUEST['status']=='approve')
    {
        $sendMail	= $_REQUEST['sendMail'];

        $sql 		= "UPDATE `trn_poheader` SET `intStatus`=intStatus-1 WHERE (`intPONo`='$poNo') AND (`intPOYear`='$year') and intStatus not in(0,1)";
        $result 	= $db->RunQuery($sql);

        if($result){
            $sql = "SELECT trn_poheader.intStatus, trn_poheader.intReviseNo, trn_poheader.intUser, trn_poheader.intSupplier, trn_poheader.intApproveLevels FROM trn_poheader WHERE (`intPONo`='$poNo') AND (`intPOYear`='$year')";
            $results = $db->RunQuery($sql);
            $row = mysqli_fetch_array($results);
            $status = $row['intStatus'];
            $intSupplier = $row['intSupplier'];
            $createdUser = $row['intUser'];
            $savedLevels = $row['intApproveLevels'];
            $rev_no = $row['intReviseNo'];

            $approveLevel=$savedLevels+1-$status;
            $sqlI = "INSERT INTO `trn_poheader_approvedby` (`intPONo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
					VALUES ('$poNo','$year','$approveLevel','$userId',now(),0)";
            $resultI = $db->RunQuery($sqlI);

            $sqlItm = "SELECT
					trn_podetails.intPONo,
					trn_podetails.intPOYear,
					trn_podetails.intPrnNo,
					trn_podetails.intPrnYear,
					trn_podetails.dblQty,
					trn_podetails.intItem  
					FROM trn_podetails
					WHERE
					trn_podetails.intPONo =  '$poNo' AND
					trn_podetails.intPOYear =  '$year'";
            $resultItm = $db->RunQuery($sqlItm);
            while($rowItm=mysqli_fetch_array($resultItm))
            {
                $itemId			= $rowItm['intItem'];
                $sql_poBalQty	=  "SELECT IFNULL(trn_podetails.dblQty,0)  -(SELECT
								IFNULL((ware_grndetails.dblGrnQty-ware_grndetails.dblRetunSupplierQty),0) 
								FROM
								ware_grnheader
								INNER JOIN ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear
								WHERE
								ware_grnheader.intPoNo = trn_podetails.intPONo AND
								ware_grnheader.intPoYear = trn_podetails.intPOYear AND 
								ware_grndetails.intItemId =  trn_podetails.intItem) AS balQty
															FROM
															trn_poheader
															INNER JOIN trn_podetails ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear
															WHERE
								trn_podetails.intPONo =  '$poNo' AND
								trn_podetails.intPOYear =  '$year' AND 
								trn_podetails.intItem =  '$itemId'";	//echo $sql_poBalQty;
                $result_poBalQty  = $db->RunQuery($sql_poBalQty);
                $rows_poBalQty 	  = mysqli_fetch_array($result_poBalQty);
                $poBalQty		  = $rows_poBalQty['balQty']	;
            }
            //------------------------------
            //if($status==1){//if final confirmation raised
            //if($status==$savedLevels){
            //if first confirmation raised
            $sql1 = "SELECT
						trn_podetails.intPONo,
						trn_podetails.intPOYear,
						trn_podetails.intPrnNo,
						trn_podetails.intPrnYear,
						trn_podetails.dblQty,
						trn_podetails.intItem ,
						trn_prndetails.intOrderNo,
						trn_prndetails.intOrderYear
						FROM trn_podetails
						INNER JOIN trn_prndetails ON trn_podetails.intPrnNo = trn_prndetails.intPrnNo AND trn_podetails.intPrnYear = trn_prndetails.intYear AND trn_podetails.intItem = trn_prndetails.intItem
						WHERE
						trn_podetails.intPONo =  '$poNo' AND
						trn_podetails.intPOYear =  '$year'";
            $result1 = $db->RunQuery($sql1);
            while($row=mysqli_fetch_array($result1))
            {
                $order_no	=$row['intOrderNo'];
                $order_year	=$row['intOrderYear'];
                $prnNo		=$row['intPrnNo'];
                $prnYear	=$row['intPrnYear'];
                $item		=$row['intItem'];
                $Qty		=$row['dblQty'];


                if($status==$savedLevels){//if first confirmation raised
                    $sql = "UPDATE `trn_prndetails` SET 	dblPoQty = dblPoQty+'$Qty'
							WHERE (`intPrnNo`='$prnNo') AND (`intYear`='$prnYear') AND (`intItem`='$item') ";
                    $result = $db->RunQuery($sql);
                }

                if($status==1){
                    $sql = "UPDATE `trn_po_prn_details` SET `PURCHASED_QTY`= `PURCHASED_QTY`+'$Qty' WHERE (`ORDER_NO`='$order_no') AND (`ORDER_YEAR`='$order_year') AND (`ITEM`='$item') ";
                    $result = $db->RunQuery($sql);
                }
            }

            if($status == 1 && $_SESSION['headCompanyId'] == 1){
                  $revised_date = getRevisedDate($poNo,$year,$rev_no,$db);
                  $transaction_string = ($revised_date != '' || $rev_no > 0)?"PO_EditFinish":"PO_New";
                  updateAzureDBTables($poNo,$year,$db,$transaction_string,$revised_date);

            }

            //	}
            //------------------------------
        }


        if($status==1)
        {
             ////////////// send mail to creator //////////////////
            //	if($createdUser!=$userId){//if confiremed user and created user same, email is not generate.

            //sendFinlConfirmedEmailToUser($poNo,$year,$objMail,$mainPath,$root_path);

            $creatorEmail = getCreatorEmail($poNo,$year);
            $sendMail 		= $_REQUEST['sendMail'];
            $supplierEmail  = getSupplierEmail($intSupplier);

            $emailHeaderS 	= "CONFIRMED SUPPLIER PO ('$poNo'/'$year')";
            $emailHeaderC 	= "CONFIRMED SUPPLIER PO ('$poNo'/'$year')";
            $enterUserEmail = $_SESSION['email'];
            $enterUserName  = $_SESSION['systemUserName'];
            $mainPath 		= $_SESSION['mainPath'];
            //$poNo 			= $serialNo;
            $companyId 		= $_SESSION['CompanyID'];
            $locationId 	= $location;
            require_once('emailPOReport_toSupplier.php');




            //	}
            ////////////// send mail to supplier //////////////////
            $sendMail 		= $_REQUEST['sendMail'];
            $supplierEmail  = getSupplierEmail($intSupplier);

            $emailHeader 	= " PURCHASE ORDER ";
            $enterUserEmail = $_SESSION['email'];
            $enterUserName  = $_SESSION['systemUserName'];
            $mainPath 		= $_SESSION['mainPath'];
            //$poNo 			= $serialNo;
            $companyId 		= $_SESSION['CompanyID'];
            $locationId 	= $location;
            //require_once('emailPOReport_toSupplier.php');

        }

    }
    else if($_REQUEST['status']=='reject')
    {
        $sql = "SELECT trn_poheader.intStatus,trn_poheader.intApproveLevels, trn_poheader.intUser  FROM trn_poheader WHERE (`intPONo`='$poNo') AND (`intPOYear`='$year')";
        $results = $db->RunQuery($sql);
        $row = mysqli_fetch_array($results);
        $createdUser = $row['intUser'];
        $status = $row['intStatus'];
        $savedLevels = $row['intApproveLevels'];

        $sql = "UPDATE `trn_poheader` SET `intStatus`=0 WHERE (`intPONo`='$poNo') AND (`intPOYear`='$year') ";
        $result = $db->RunQuery($sql);

        //$sql = "DELETE FROM `trn_poheader_approvedby` WHERE (`intPONo`='$poNo') AND (`intYear`='$year')";
        //$result2 = $db->RunQuery($sql);

        $sqlI = "INSERT INTO `trn_poheader_approvedby` (`intPONo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
			VALUES ('$poNo','$year','0','$userId',now(),0)";
        $resultI = $db->RunQuery($sqlI);

        if($resultI)
        {
            if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
                sendRejecedEmailToUser($poNo,$year,$objMail,$mainPath,$root_path);
            }
        }
        if ($_SESSION['headCompanyId'] == 1) {
            updateAzureDBTables($poNo, $year, $db, "PO_Cancel",'');
        }


        //---------roll back po qty from prn table------------------------
        if($savedLevels>=$status){//atleast one confirmation has been raised....
            $sql1 = "SELECT
						trn_podetails.intPONo,
						trn_podetails.intPOYear,
						trn_podetails.intPrnNo,
						trn_podetails.intPrnYear,
						trn_podetails.dblQty,
						trn_podetails.intItem  
						FROM trn_podetails
						WHERE
						trn_podetails.intPONo =  '$poNo' AND
						trn_podetails.intPOYear =  '$year'";
            $result1 = $db->RunQuery($sql1);
            while($row=mysqli_fetch_array($result1))
            {
                $prnNo=$row['intPrnNo'];
                $prnYear=$row['intPrnYear'];
                $item=$row['intItem'];
                $Qty=$row['dblQty'];

                $sql = "UPDATE `trn_prndetails` SET 	dblPoQty = dblPoQty-'$Qty'
							WHERE (`intPrnNo`='$prnNo') AND (`intYear`='$prnYear') AND (`intItem`='$item') ";
                $result = $db->RunQuery($sql);
            }
        }
        //------------------------------------
    }
    else if($_REQUEST['status']=='revisePO')
    {
        $value 	= explode('/',$_REQUEST['poNO']);
        $poNo 	= $value[0];
        $poYear = $value[1];

        $sql = "SELECT trn_poheader.intStatus,trn_poheader.intApproveLevels, trn_poheader.intUser  FROM trn_poheader WHERE (`intPONo`='$poNo') AND (`intPOYear`='$poYear')";
        $results = $db->RunQuery($sql);
        $row = mysqli_fetch_array($results);
        $createdUser = $row['intUser'];
        $status = $row['intStatus'];
        $savedLevels = $row['intApproveLevels'];

        /////////////validate grn is raised or not /////////////

        $sql_poBalQty	=  "SELECT IFNULL(SUM(trn_podetails.dblQty),0)  -(SELECT
IFNULL(SUM(ware_grndetails.dblGrnQty-ware_grndetails.dblRetunSupplierQty),0) 
FROM
ware_grnheader
INNER JOIN ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear
WHERE
ware_grnheader.intPoNo = trn_podetails.intPONo AND
ware_grnheader.intPoYear = trn_podetails.intPOYear
AND ware_grnheader.intStatus = 1) AS balQty
							FROM
							trn_poheader
							INNER JOIN trn_podetails ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear
							WHERE
									trn_podetails.intPONo =  '$poNo' AND
									trn_podetails.intPOYear =  '$poYear'";
        $result_poBalQty  = $db->RunQuery($sql_poBalQty);
        $rows_poBalQty 	  = mysqli_fetch_array($result_poBalQty);
        $poBalQty		  = $rows_poBalQty['balQty']	;

        $sql_poStatus   = "	SELECT
							trn_poheader.intStatus
							FROM trn_poheader
							WHERE
							intPONo ='$poNo' AND intPOYear ='$poYear'";
        $result_poStatus = $db->RunQuery($sql_poStatus);
        $rows = mysqli_fetch_array($result_poStatus);
        if($rows['intStatus']>1)
        {
            $response['msg'] = "You can't revise this PO. Because final approval not raised";
            $response['type'] = 'fail';
            echo json_encode($response);
            return;
        }

        else if($poBalQty == 0)
        {
            $response['msg'] = "You can't revise this PO. Because already raised GRN for full amount of this PO";
            $response['type'] = 'fail';
            echo json_encode($response);
            return;
        }

        else if(advancePayment($poNo,$poYear) != '')
        {
            $response['msg'] = "You can't revise this PO, There are active advanced payments : ".advancePayment($poNo,$poYear);
            $response['type'] = 'fail';
            echo json_encode($response);
            return;
        }

        else
        {
            $sql = "UPDATE `trn_poheader` SET `intStatus`=`intApproveLevels`+1, `intReviseNo`=`intReviseNo`+1 WHERE (`intPONo`='$poNo') AND (`intPOYear`='$poYear')  ";
            $db->RunQuery($sql);

            $sql = "UPDATE `trn_poheader` SET `PRINT_COUNT`=0 WHERE (`intPONo`='$poNo') AND (`intPOYear`='$year')";
            $db->RunQuery($sql);
            //$sql = "DELETE FROM `trn_poheader_approvedby` WHERE (`intPONo`='$poNo') AND (`intYear`='$poYear')";
            //$db->RunQuery($sql);
            //inactive previous recordrs in approvedby table

            $maxAppByStatus=(int)getMaxAppByStatus($poNo,$poYear)+1;
            $sql = "UPDATE `trn_poheader_approvedby` SET intStatus ='$maxAppByStatus' 
					WHERE (`intPONo`='$poNo') AND (`intYear`='$poYear') AND (`intStatus`='0')";
            $result1 = $db->RunQuery($sql);

            $sqlI = "INSERT INTO `trn_poheader_approvedby` (`intPONo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
				VALUES ('$poNo','$poYear','-1','$userId',now(),0)";
            $resultI = $db->RunQuery($sqlI);


            //---------roll back po qty from prn table------------------------
            if($savedLevels>=$status){//atleast one confirmation has been raised....
                $sql1 = "SELECT
						trn_podetails.intPONo,
						trn_podetails.intPOYear,
						trn_podetails.intPrnNo,
						trn_podetails.intPrnYear,
						trn_podetails.dblQty,
						trn_podetails.intItem ,
						trn_prndetails.intOrderNo,
						trn_prndetails.intOrderYear 
						FROM trn_podetails
						INNER JOIN trn_prndetails ON trn_podetails.intPrnNo = trn_prndetails.intPrnNo AND trn_podetails.intPrnYear = trn_prndetails.intYear AND trn_podetails.intItem = trn_prndetails.intItem
						WHERE
						trn_podetails.intPONo =  '$poNo' AND
						trn_podetails.intPOYear =  '$poYear'";
                $result1 = $db->RunQuery($sql1);
                while($row=mysqli_fetch_array($result1))
                {
                    $order_no	=$row['intOrderNo'];
                    $order_year	=$row['intOrderYear'];
                    $prnNo=$row['intPrnNo'];
                    $prnYear=$row['intPrnYear'];
                    $item=$row['intItem'];
                    $Qty=$row['dblQty'];

                    $sql = "UPDATE `trn_prndetails` SET 	dblPoQty = dblPoQty-'$Qty'
							WHERE (`intPrnNo`='$prnNo') AND (`intYear`='$prnYear') AND (`intItem`='$item') ";
                    $result = $db->RunQuery($sql);

                    if($status==1){
                        $sql = "UPDATE `trn_po_prn_details` SET `PURCHASED_QTY`= `PURCHASED_QTY`-'$Qty' WHERE (`ORDER_NO`='$order_no') AND (`ORDER_YEAR`='$order_year') AND (`ITEM`='$item') ";
                        $result = $db->RunQuery($sql);
                    }
                }
            }

            //generatePRNnotesForChildCustomer_pos($poNo,$poYear);
            //------------------------------------

            if($resultI){
                if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
                    sendRevisedEmailToUser($poNo,$poYear,$objMail,$mainPath,$root_path);
                }
                if ($_SESSION['headCompanyId'] == 1) {
                    updateAzureDBTables($poNo, $poYear, $db, "PO_EditStart",'');
                }
                $response['msg'] = "PO revised successfully.";
                $response['type'] = 'pass';

            }
            else{
                $response['msg'] = "PO revised Fialed.";
                $response['type'] = 'fail';
            }
            echo json_encode($response);
        }


        ////////////////////////////////////////////////////////
    }






////////////////////////////////////////////////////////////////////////////

	function getSupplierEmail($supplierId)
    {
        global $db;
        $sql = "SELECT mst_supplier.strEmail FROM mst_supplier WHERE mst_supplier.intId =  '$supplierId'";
        $result = $db->RunQuery($sql);
        $row    = mysqli_fetch_array($result);
        return $row['strEmail'];
    }
////////////////////////////////////////////////////////////////////////////

	function getCreatorEmail($poNo,$year)
    {
        global $db;
        $sql = "SELECT
		sys_users.strFullName,
		sys_users.strEmail 
		FROM 
		trn_poheader
		Inner Join sys_users ON trn_poheader.intUser = sys_users.intUserId
		WHERE
		trn_poheader.intPONo =  '$poNo' AND
		trn_poheader.intPOYear =  '$year'";
        $result = $db->RunQuery($sql);
        $row    = mysqli_fetch_array($result);
        return $row['strEmail'];
    }

//--------------------------------------------------------
function sendFinlConfirmedEmailToUser($serialNo,$year,$objMail,$mainPat,$root_path){
    global $db;

    $sql = "SELECT
			sys_users.strFullName,
			sys_users.strEmail 
			FROM 
			trn_poheader
			Inner Join sys_users ON trn_poheader.intUser = sys_users.intUserId
			WHERE
			trn_poheader.intPONo =  '$serialNo' AND
			trn_poheader.intPOYear =  '$year'";


    $result = $db->RunQuery($sql);

    $row=mysqli_fetch_array($result);
    $enterUserName = $row['strFullName'];
    $enterUserEmail = $row['strEmail'];

    $header="CONFIRMED SUPPLIER PO ('$serialNo'/'$year')";

    //send mails ////
    $_REQUEST = NULL;
    $_REQUEST = NULL;

    $_REQUEST['program']='CUSTOMER PO';
    $_REQUEST['field1']='PO No';
    $_REQUEST['field2']='PO Year';
    $_REQUEST['field3']='';
    $_REQUEST['field4']='';
    $_REQUEST['field5']='';
    $_REQUEST['value1']=$serialNo;
    $_REQUEST['value2']=$year;
    $_REQUEST['value3']='';
    $_REQUEST['value4']='';
    $_REQUEST['value5']='';

    $_REQUEST['subject']="CONFIRMED SUPPLIER PO ('$serialNo'/'$year')";

    $_REQUEST['statement1']="Approved this";
    $_REQUEST['statement2']="to view this";
    $_REQUEST['link']=base64_encode("http://nsoft.screenlineholdings.com/?q=934&poNo=$serialNo&year=$year&approveMode=0");
    //$path=$mainPath.'presentation/mail_approval_template.php';
    $path=$root_path."presentation/mail_approval_template.php";
    $objMail->send_Response_Mail($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}
//--------------------------------------------------------
function sendRejecedEmailToUser($serialNo,$year,$objMail,$mainPat,$root_path){
    global $db;

    $sql = "SELECT 
			sys_users.strFullName,
			sys_users.strEmail 
			FROM 
			trn_poheader
			Inner Join sys_users ON trn_poheader.intUser = sys_users.intUserId
			WHERE
			trn_poheader.intPONo =  '$serialNo' AND
			trn_poheader.intPOYear =  '$year'";


    $result = $db->RunQuery($sql);

    $row=mysqli_fetch_array($result);
    $enterUserName = $row['strFullName'];
    $enterUserEmail = $row['strEmail'];

    $header="REJECTED SUPPLIER PO ('$serialNo'/'$year')";

    //send mails ////
    $_REQUEST = NULL;
    $_REQUEST = NULL;

    $_REQUEST['program']='SUPPLIER PO';
    $_REQUEST['field1']='PO No';
    $_REQUEST['field2']='PO Year';
    $_REQUEST['field3']='';
    $_REQUEST['field4']='';
    $_REQUEST['field5']='';
    $_REQUEST['value1']=$serialNo;
    $_REQUEST['value2']=$year;
    $_REQUEST['value3']='';
    $_REQUEST['value4']='';
    $_REQUEST['value5']='';

    $_REQUEST['subject']="REJECTED SUPPLIER PO ('$serialNo'/'$year')";

    $_REQUEST['statement1']="Rejected this";
    $_REQUEST['statement2']="to view this";
    $_REQUEST['link']=base64_encode("http://nsoft.screenlineholdings.com/?q=934&poNo=$serialNo&year=$year&approveMode=0");
    //$path=$mainPath.'presentation/mail_approval_template.php';
    $path=$root_path."presentation/mail_approval_template.php";
    $objMail->send_Response_Mail($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}
//--------------------------------------------------------
function sendRevisedEmailToUser($serialNo,$year,$objMail,$mainPat,$root_path){
    global $db;

    $sql = "SELECT
			sys_users.strFullName,
			sys_users.strEmail 
			FROM 
			trn_poheader
			Inner Join sys_users ON trn_poheader.intUser = sys_users.intUserId
			WHERE
			trn_poheader.intPONo =  '$serialNo' AND
			trn_poheader.intPOYear =  '$year'";


    $result = $db->RunQuery($sql);

    $row=mysqli_fetch_array($result);
    $enterUserName = $row['strFullName'];
    $enterUserEmail = $row['strEmail'];

    $header="REVISED SUPPLIER PO ('$serialNo'/'$year')";

    //send mails ////
    $_REQUEST = NULL;
    $_REQUEST = NULL;

    $_REQUEST['program']='SUPPLIER PO';
    $_REQUEST['field1']='Order No';
    $_REQUEST['field2']='Order Year';
    $_REQUEST['field3']='';
    $_REQUEST['field4']='';
    $_REQUEST['field5']='';
    $_REQUEST['value1']=$serialNo;
    $_REQUEST['value2']=$year;
    $_REQUEST['value3']='';
    $_REQUEST['value4']='';
    $_REQUEST['value5']='';

    $_REQUEST['subject']="REVISED SUPPLIER PO ('$serialNo'/'$year')";

    $_REQUEST['statement1']="Revised this";
    $_REQUEST['statement2']="to view this";
    $_REQUEST['link']=base64_encode("http://nsoft.screenlineholdings.com/?q=934&poNo=$serialNo&year=$year&approveMode=0");
    //$path=$mainPath.'presentation/mail_approval_template.php';
    $path=$root_path."presentation/mail_approval_template.php";
    $objMail->send_Response_Mail($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}
//------------------------------function get MaxAppByStatus---------------------
function getMaxAppByStatus($serialNo,$year){
    global $db;

    //echo $savedStat;
    $editMode=0;
    $sql = "SELECT
				max(trn_poheader_approvedby.intStatus) as status 
				FROM
				trn_poheader_approvedby
				WHERE
				trn_poheader_approvedby.intPONo =  '$serialNo' AND
				trn_poheader_approvedby.intYear =  '$year'";

    $resultm = $db->RunQuery($sql);
    $rowm=mysqli_fetch_array($resultm);

    return $rowm['status'];

}
//--------------------------------------------------------
function advancePayment($poNo,$poYear){

    global $db;

    //echo $savedStat;
    $editMode=0;
    $sql = "SELECT
				GROUP_CONCAT(ADVANCE_PAYMENT_NO,'/',ADVANCE_PAYMENT_YEAR) as payNos
				FROM `finance_supplier_advancepayment_header`
				WHERE
				finance_supplier_advancepayment_header.`STATUS` = 1 AND
				finance_supplier_advancepayment_header.PO_NO = '$poNo' AND
				finance_supplier_advancepayment_header.PO_YEAR = '$poYear'
				";

    $resultm = $db->RunQuery($sql);
    $rowm=mysqli_fetch_array($resultm);

    return $rowm['payNos'];

}



function updateAzureDBTables($poNo, $poYear, $db, $transactionType, $revisedDate)
{
    global $objAzure;
    $NBT_taxcodes = array(2,3,22);
    $environment = $objAzure->getEnvironment();
    $headerTable = 'PurchaseHeader'.$environment;
    $lineTable = 'PurchaseLine'.$environment;

    $azure_connection = $objAzure->connectAzureDB();
        $sql_select = "SELECT
                    TH.intSupplier AS supplierId,
                    mst_supplier.strName AS supplier,
                    mst_supplier.strCode AS supplier_code,
                    mst_financecurrency.strCode AS currency_code,
                    TH.intPaymentTerm AS payTerm,
                    TH.intPaymentMode AS payMode,
                  (
                        SELECT
                            GROUP_CONCAT(
                                DISTINCT ware_grnheader.strInvoiceNo
                            )
                        FROM
                            ware_grnheader
                        WHERE
                            ware_grnheader.intPoNo = TH.intPONo
                        AND ware_grnheader.intPoYear = TH.intPOYear
                        AND ware_grnheader.intStatus = 1
                    ) AS invoices,
                    TH.intShipmentTerm AS shipmentTerm,
                    TH.intShipmentMode AS shipmentMode,
                    TH.dtmDeliveryDate AS deliveryDate,
                    TH.strRemarks AS narration,
                    mst_companies.strName AS delToCompany,
                    mst_locations.intId AS delToLocation,
                    TH.dtmPODate AS orderDate,
                    TH.intStatus,
                    TH.PRINT_COUNT,
                    TH.intApproveLevels,
                    TH.intUser,
                    TH.intCompany AS poRaisedLocationId,
                    sys_users.strUserName,
                    TH.intReviseNo,
                    TH.PO_TYPE	
                FROM
                    trn_poheader TH
                INNER JOIN mst_supplier ON TH.intSupplier = mst_supplier.intId
                INNER JOIN trn_poheader_approvedby ON TH.intPONo = trn_poheader_approvedby.intPONo                
                AND TH.intPOyear = trn_poheader_approvedby.intYear
                AND trn_poheader_approvedby.intApproveLevelNo = 5
                INNER JOIN mst_locations ON TH.intDeliveryTo = mst_locations.intId
                INNER JOIN mst_companies ON mst_locations.intCompanyId = mst_companies.intId
                INNER JOIN sys_users ON TH.intUser = sys_users.intUserId
                INNER JOIN mst_financecurrency ON mst_financecurrency.intId = TH.intCurrency
                WHERE
                    TH.intPONo = '$poNo'
                AND TH.intPOYear = '$poYear'";

        $result = $db->RunQuery($sql_select);
        while ($row = mysqli_fetch_array($result)) {
            $supplierId = $row['supplierId'];
            $supplier = $row['supplier'];
            $supplier_code = $row['supplier_code'];
            $currency = ($row['currency_code'] == 'EURO')?"Eur":($row['currency_code'] == 'LKR'?"":$row['currency_code']);
            $payTerm = $row['payTerm'];
            $payMode = $row['payMode'];
            $shipmentMode = $row['shipmentMode'];
            $deliveryDate = $row['deliveryDate'];
            $locationId = $row['poRaisedLocationId'];
            $orderDate = $row['orderDate'];
            $pOBy = $row['strUserName'];
            $remarks = $row['strRemarks'];
            $intStatus = $row['intStatus'];
            $savedLevels = $row['intApproveLevels'];
            $user = $row['intUser'];
            $intCreatedUser = $row['intUser'];
            $reviseNo = $row['intReviseNo'];
            $PRINT_COUNT = $row['PRINT_COUNT'];
            $po_type = $row['shipmentTerm'];
            $old_poString = $poNo . '/' . $poYear;
            $response = getLatestPODetails($old_poString,$db);
            $poString = $response['new_poString'];
            $version_no = $response['version_no'];
            $narration = trim($row['narration']);
            $narration_sql = $db->escapeString($narration);
            $narration = str_replace("'", '', $narration);
            $invoice_no = $row['invoices'];
            $invoice_no_arr = explode(',',$invoice_no);
            $successHeader = '0';
            if($transactionType == 'PO_EditStart'){
                $revisedDate = date('Y-m-d H:i:s');
            }
            $postingDate = date('Y-m-d H:i:s');


            $sql = "SELECT
                    trn_podetails.intPONo,
                    trn_podetails.intPOYear,
                    trn_podetails.intItem,
                    mst_item.strCode AS itemCode,
                    mst_item.strName AS itemName,
                    mst_maincategory.strName AS mainCategory,
                    mst_subcategory.strName AS subCategory,
                    round(
                        trn_podetails.dblUnitPrice,
                        6
                    ) AS dblUnitPrice,
                    trn_podetails.dblDiscount,
                    trn_podetails.intTaxCode,
                    sum(
                        trn_podetails.dblTaxAmmount
                    ) AS dblTaxAmmount,
                    mst_financetaxgroup.strCode,
                    trn_podetails.SVAT_ITEM,
                    sum(trn_podetails.dblQty) AS dblQty,
                    mst_item.intUOM,
                    mst_units.strName AS uom,
                    trn_podetails.CostCenter
                FROM
                    trn_podetails
                INNER JOIN mst_item ON trn_podetails.intItem = mst_item.intId
                INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
                INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
                LEFT JOIN mst_financetaxgroup ON trn_podetails.intTaxCode = mst_financetaxgroup.intId
                INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
                WHERE
                    trn_podetails.intPONo = '$poNo'
                AND trn_podetails.intPOYear = '$poYear'
                GROUP BY
                    trn_podetails.intItem                    
                HAVING
                    dblQty > 0";

            $result_data = $db->RunQuery($sql);
            $i = 0;
            while ($row_data = mysqli_fetch_array($result_data)){
                $line_no = $row_data['intItem'];
                $item_code = $row_data['itemCode'];
                $description = $row_data['itemName'];
                $description = str_replace("'","''",$description);
                $item_category = $row_data['mainCategory'];
                $item_subcategory = $row_data['subCategory'];
                $qty = $row_data['dblQty'];
                $invoiced_qty = getInvoicedQty($supplierId,$invoice_no_arr,$line_no,$db);
                $qty = $qty - $invoiced_qty;
                if($qty < 0){
                    $qty = 0;
                }
                $tax_code = $row_data['intTaxCode'];
                $discount = $row_data['dblDiscount'];
                $unit_price = round($row_data['dblUnitPrice'] * (100-$discount)/100 ,5);
                $cost_center = $row_data['CostCenter'];
                $successDetails = '0';
                $i++;
                if(in_array($tax_code,$NBT_taxcodes)){
                    $tax_amount = $row_data['dblTaxAmmount'];
                }
                else if($item_code == 'SERFNBT'  && $tax_code != 0){
                    $tax_amount = $row_data['dblTaxAmmount'];
                }
                else{
                    $tax_amount = 0;
                }
                $vat_group = ($tax_code != 0)?"VAT":"NO_VAT";

                $sql_azure_details = "INSERT into $lineTable (Transaction_type, Document_Type, Purchase_Order_No, Revised_Count, Line_No, Item_Code, Narration, Item_Category, Item_SubCategory, Quantity, Unit_Cost, VAT_Prod_Posting_Group, NBT_Amount) VALUES ('$transactionType','Order','$poString','$reviseNo','$line_no','$item_code','$description','$item_category','$item_subcategory','$qty','$unit_price','$vat_group','$tax_amount')";
                if($azure_connection) {
                    $getResults = $objAzure->runQuery($azure_connection, $sql_azure_details);
                    if($getResults != FALSE){
                        $successDetails = '1';
                    }
                }
                $sql_db_details = "INSERT into trn_financemodule_purchaseline (Transaction_type, Purchase_Order_No, Revised_Count, Line_No, Item_Code, Narration, Item_Category, Item_SubCategory, Quantity, Unit_Cost, VAT_Prod_Posting_Group, NBT_Amount, deliveryStatus, deliveryDate) VALUES ('$transactionType','$poString','$reviseNo','$line_no','$item_code','$description','$item_category','$item_subcategory','$qty','$unit_price','$vat_group', '$tax_amount','$successDetails', NOW())";
                $result_db_details = $db->RunQuery($sql_db_details);
            }
            $sql_azure_header = "INSERT INTO $headerTable (Transaction_type, Document_Type, Purchase_Order_No, Revised_Count, Vendor_Code, Vendor_Name, Order_Date, Posting_Date, Revised_Date, Currency_Code, Payment_Term, Payment_Method, Shipment_Mode, Delivery_Date, Location_Code, Narration, PO_Type, No_of_Lines) VALUES ('$transactionType', 'Order', '$poString','$reviseNo', '$supplier_code', '$supplier','$orderDate', '$postingDate', '$revisedDate','$currency', '$payTerm', '$payMode', '$shipmentMode','$deliveryDate','$locationId',CONVERT(varchar(250),'$narration'), '$po_type', '$i')";
            if($azure_connection) {
                $getResultsHeader = $objAzure->runQuery($azure_connection, $sql_azure_header);
                if($getResultsHeader != FALSE){
                    $successHeader = '1';
                }
            }
            $sql_db_header = "INSERT INTO trn_financemodule_purchaseheader (Transaction_type, Document_Type, Purchase_Order_No, Revised_Count, Vendor_Code, Vendor_Name, Order_Date, Posting_Date, Revised_Date, Currency_Code, Payment_Term, Payment_Method, Shipment_Mode, Delivery_Date, Location_Code, Narration, PO_Type, No_of_Lines, version_no, deliveryStatus, deliveryDate) VALUES ('$transactionType', 'Order', '$poString', '$reviseNo','$supplier_code', '$supplier', '$orderDate', '$postingDate','$revisedDate','$currency', '$payTerm', '$payMode', '$shipmentMode','$deliveryDate','$locationId','$narration_sql', '$po_type', '$i', '$version_no','$successHeader', NOW())";
            $result_db_header = $db->RunQuery($sql_db_header);
        }

}

function getRevisedDate($poNo, $poYear, $revision_No, $db){
	    $poString = $poNo.'/'.$poYear;
	    $revised_date = '';
	    $sql = "SELECT Revised_Date FROM `trn_financemodule_purchaseheader` WHERE `Transaction_Type` = 'PO_EditStart' AND `Purchase_Order_No` LIKE '%$poString%' AND `Revised_Count` = '$revision_No'";
        $result = $db->RunQuery($sql);
        while($row=mysqli_fetch_array($result)){
            $revised_date =$row['Revised_Date'];
        }
        return $revised_date;

}

function getLatestPODetails($poString,$db){
    $version_no = 0;
    $new_poString = $poString;
    $sql = "SELECT MAX(version_no) AS version_no, Purchase_Order_No FROM `trn_financemodule_purchaseheader` WHERE `Purchase_Order_No` LIKE '%$poString%' GROUP BY Purchase_Order_No ORDER BY version_no desc limit 1";
    $result = $db->RunQuery($sql);
    while($row = mysqli_fetch_array($result)){
        $version_no = $row['version_no'];
        $new_poString = $row['Purchase_Order_No'];
    }
    $response = array();
    $response['version_no'] = $version_no;
    $response['new_poString'] = $new_poString;
    return $response;
}

function getInvoicedQty($supplier,$arr_invoice,$item,$db)
{
    $sql_select = "SELECT
                      IFNULL(SUM(SPID.QTY),0) AS invoiced_qty
                    FROM
                        finance_supplier_purchaseinvoice_details SPID
                    INNER JOIN finance_supplier_purchaseinvoice_header SPIH ON SPID.PURCHASE_INVOICE_NO = SPIH.PURCHASE_INVOICE_NO
                    AND SPIH.PURCHASE_INVOICE_YEAR = SPID.PURCHASE_INVOICE_YEAR
                    WHERE
                    SPIH.INVOICE_NO IN ('" . implode("','", $arr_invoice) . "')
                    AND SPIH.SUPPLIER_ID = '$supplier'
                    AND SPID.ITEM_ID = '$item'
                    AND SPIH.`STATUS` = 1";

    $result = $db->RunQuery($sql_select);
    $row = mysqli_fetch_array($result);
    return $row['invoiced_qty'];
}
?>




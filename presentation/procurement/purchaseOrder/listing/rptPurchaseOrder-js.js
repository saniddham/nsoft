// JavaScript Document
var basePath = "presentation/procurement/purchaseOrder/listing/";
var menuId = '220';
var reportMenuId = '30';

$(document).ready(function() {
	
	$('.butRevise').die('click').live('click',revisePO);
	
	
	$('#frmPrchaseOrderReport #imgApprove').die('click').live('click',function(){
	var val = $.prompt('Are you sure you want to approve this PO ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
						if(validateQuantities()==0){
						var sendMail  = ($('#chkSendEmail').attr('checked')?1:0);
						var url = basePath+"rptPurchaseOrder-db-set.php"+window.location.search+'&status=approve&sendMail='+sendMail;
						var obj = $.ajax({url:url,
							async:false});
							//alert(1);
						window.location.href = window.location.href;
						window.opener.location.reload();//reload listing page
						//$('txtApproveStatus').val('1');
						//document.getElementById('txtApproveStatus').value = 1;
						//document.getElementById('frmSampleReport').submit();
						}
					}
				}});
	});
	
	$('#frmPrchaseOrderReport #imgReject').click(function(){
	var val = $.prompt('Are you sure you want to reject this PO ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										if(validateRejecton()==0){
										var url = basePath+"rptPurchaseOrder-db-set.php"+window.location.search+'&status=reject';
										var obj = $.ajax({url:url,async:false});
										window.location.href = window.location.href;
										window.opener.location.reload();//reload listing page
										//document.getElementById('txtApproveStatus').value = 0;
										//document.getElementById('frmSampleReport').submit();
										}
									}
								}});
	});
	
/*	$('#frmPRNlisting #cboPrnStatus').change(function(){
					//	var url = "rptPurchaseRequisitionNote-db-set.php"+window.location.search+'&status=reject';
					//	var url = "purchaseRequisitionNoteListing.php"+window.location.search+'?cboPrnStatus='+$('#cboPrnStatus').val();
					//		var obj = $.ajax({url:url,async:false});
						//	var val=$('#cboPrnStatus').val();
							//alert(val);
						//	window.location.href = window.location.href;
						//	$('#cboPrnStatus').val(val);
						//	alert($('#cboPrnStatus').val());
						$("frmPRNlisting").submit();
	});
*/	
});
//---------------------------------------------------
function validateQuantities(){
		var ret=0;	
	    var poNo 		= document.getElementById('divPoNo').innerHTML;
		var url 		= basePath+"rptPurchaseOrder-db-get.php?requestType=getValidation";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:"poNo="+poNo,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#imgApprove').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertx()",10000);
					  ret= 1;
					}
				}
			});
			return ret;
}
//---------------------------------------------------
function validateRejecton(){
		var ret=0;	
	    var poNo 		= document.getElementById('divPoNo').innerHTML;
		var url 		= basePath+"rptPurchaseOrder-db-get.php?requestType=validateRejecton";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:"poNo="+poNo,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#imgReject').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertx()",1000);
					  ret= 1;
					}
				}
			});
			return ret;
}


function revisePO()
{
	
	var poNO = $(this).parent().parent().find('td:eq(1) a').html()+'/'+$(this).parent().parent().find('td:eq(2)').html();
	var x = confirm('Are you sure want to revise this PO:'+poNO);
	if(!x)return;
	
	var url = basePath+'rptPurchaseOrder-db-set.php?status=revisePO&poNO='+poNO;
	$.ajax({
		url:url,
		async:false,
		dataType:'json',
		type:'POST',
		success:function(json){
			alert(json.msg);
			document.location.href  = document.location.href;
		}
		});
}


//------------------------------------------------
function alertx()
{
	$('#frmPrchaseOrderReport #imgApprove').validationEngine('hide')	;
}
//--------------------------------------------
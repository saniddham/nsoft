<?php
(define('UNLOCKPAGE', true) ? die('<<< Access denied >>>') : '');
ini_set('display_errors', 0);
require_once "class/finance/cls_common_get.php";
require_once "class/procurement/purchaseOrder/cls_purchase_order_get.php";

$obj_fin_com = new Cls_Common_Get($db);
$obj_poGet = new Cls_purchase_order_Get($db);

$companyId = $_SESSION['CompanyID'];
$headCompanyId = $_SESSION['headCompanyId'];
$intUser = $_SESSION["userId"];

$mode = $_REQUEST['mode'];
$poNo = $_REQUEST['poNo'];
$year = $_REQUEST['year'];
$approveMode = $_REQUEST['approveMode'];
$q = $_REQUEST['q'];
/*$poNo = '100003';
$year = '2012';
$approveMode=1;
*/
$sql = "SELECT
mst_supplier.strName as supplier, 
mst_financecurrency.strCode,
mst_financepaymentsterms.strName as payTerm,
mst_financepaymentsmethods.strName as payMode,
mst_shipmentterms.strName as shipmentTerm,
mst_shipmentmethod.strName as shipmentMode,
trn_poheader.intShipmentTerm as shipmentTermId, 
trn_poheader.dtmDeliveryDate,
mst_companies.strName as delToCompany,
mst_locations.strName as delToLocation ,
trn_poheader.dtmPODate,
trn_poheader.strRemarks,
trn_poheader.intStatus,
trn_poheader.PRINT_COUNT,
trn_poheader.intApproveLevels,
trn_poheader.intUser,
trn_poheader.intCompany as poRaisedLocationId, 
sys_users.strUserName , 
trn_poheader.intReviseNo ,
trn_poheader.PO_TYPE ,
trn_poheader.SYSTEM_CANCELLATION 

FROM
trn_poheader
Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
left Join mst_financecurrency ON trn_poheader.intCurrency = mst_financecurrency.intId
left Join mst_financepaymentsterms ON trn_poheader.intPaymentTerm = mst_financepaymentsterms.intId
left Join mst_financepaymentsmethods ON trn_poheader.intPaymentMode = mst_financepaymentsmethods.intId
left Join mst_shipmentterms ON trn_poheader.intShipmentTerm = mst_shipmentterms.intId
left Join mst_shipmentmethod ON trn_poheader.intShipmentMode = mst_shipmentmethod.intId
Inner Join mst_locations ON trn_poheader.intDeliveryTo = mst_locations.intId
Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId 
Inner Join sys_users ON trn_poheader.intUser = sys_users.intUserId
WHERE
trn_poheader.intPONo =  '$poNo' AND
trn_poheader.intPOYear =  '$year' ";
$result = $db->RunQuery($sql);
while ($row = mysqli_fetch_array($result)) {
    $supplier = $row['supplier'];
    $currency = $row['strCode'];
    $payTerm = $row['payTerm'];
    $payMode = $row['payMode'];
    $shipmentTerm = $row['shipmentTerm'];
    $shipmentMode = $row['shipmentMode'];
    $shipmentTermId = $row['shipmentTermId'];
    $deliveryDate = $row['dtmDeliveryDate'];
    $company = $row['delToCompany'];
    $location = $row['delToLocation'];
    $locationId = $row['poRaisedLocationId'];//this is use in report header(reportHeader.php)--------------------
    $pODate = $row['dtmPODate'];
    $pOBy = $row['strUserName'];
    $remarks = $row['strRemarks'];
    $intStatus = $row['intStatus'];
    $savedLevels = $row['intApproveLevels'];
    $user = $row['intUser'];
    $intCreatedUser = $row['intUser'];
    $reviseNo = $row['intReviseNo'];
    $PRINT_COUNT = $row['PRINT_COUNT'];
    $po_type = $row['PO_TYPE'];

    if ($po_type == 1) {
        $title = "Raw Materials";
        $programCode = 'P0220';
    } else if ($po_type == 2) {
        $title = "Non-Raw Materials";
        $programCode = 'P1151';
    } else if ($po_type == 3) {
        $title = "RM and Non-RM";
        $programCode = 'P1153';
    }

    $system_cancel = $row['SYSTEM_CANCELLATION'];
}

$finalApprovalPermision = $obj_poGet->get_finalApprovalPermision($year, $poNo, $shipmentTermId, $intUser);
$finalApprovalPermision = 1;
?>
    <head>
        <title>Purchase Order Report - <?php echo $title; ?></title>
        <script type="text/javascript"
                src="presentation/procurement/purchaseOrder/listing/rptPurchaseOrder-js.js"></script>
        <style>
            .break {
                page-break-before: always;
            }

            @media print {
                .noPrint {
                    display: none;
                }
            }

            #apDiv1 {
                position: absolute;
                left: 329px;
                top: 199px;
                width: 554px;
                height: 222px;
                z-index: 1;
            }

            #apDiv2 {
                position: absolute;
                left: 449px;
                top: 179px;
                width: 357px;
                height: 183px;
                z-index: 1;
            }

            .APPROVE {
                font-size: 18px;
                font-weight: bold;
            }
        </style>
    </head>

    <body>
    <?php
    if ($intStatus > 1)//pending
    {

        ?>
        <div id="apDiv1"><img width="413" height="219" src="images/pending.png"/></div>
        <?php
    }

    if ((($PRINT_COUNT > 0) && ($mode != 'print')) && ($intStatus == 1)) {
        ?>
        <div id="apDiv2"><img width="161" height="152" src="images/duplicate.png"/></div>
        <?php
    }
    if (($intStatus == 1) && ($mode == 'view')) {

        $sql = "UPDATE `trn_poheader` SET `PRINT_COUNT`=PRINT_COUNT+1 WHERE (`intPONo`='$poNo') AND (`intPOYear`='$year')";
        $db->RunQuery($sql);

    }
    ?>
    <form id="frmPrchaseOrderReport" name="frmPrchaseOrderReport" method="post" action="rptPurchaseOrder.php">
        <table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td colspan="3"></td>
            </tr>
            <tr>
                <td width="20%"></td>
                <td width="60%" height="80" valign="top"><?php include 'reportHeader.php' ?></td>
                <td width="20%"></td>
            </tr>

            <tr>
                <td colspan="3"></td>
            </tr>
        </table>
        <div align="center">
            <div style="background-color:#FFF"><strong>PURCHASE ORDER REPORT
                    - <?php echo strtoupper($title); ?></strong><strong></strong></div>
            <table width="1140" border="0" align="center" bgcolor="#FFFFFF">
                <tr>
                    <td width="1134">
                        <table width="105%">
                            <tr>
                                <td colspan="9" align="center" bgcolor=""><?php
                                    if ($intStatus > 1) {

                                        //------------
                                        $k = $savedLevels + 2 - $intStatus;
                                        $sqlp = "SELECT
		menupermision.int" . $k . "Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";

                                        $resultp = $db->RunQuery($sqlp);
                                        $rowp = mysqli_fetch_array($resultp);
                                        $userPermission = 0;
                                        if ($rowp['int' . $k . 'Approval'] == 1) {
                                            $userPermission = 1;
                                        }
                                        //--------------

                                        if ($finalApprovalPermision != 1 && $intStatus == 2) {
                                            $userPermission = 0;
                                        }

                                        ?>
                                        <?php if (($approveMode == 1) and ($userPermission == 1)) {
                                            $flag = 1;
                                            ?>
                                            <img src="images/approve.png" align="middle" class="noPrint mouseover"
                                                 id="imgApprove"/>
                                            <img src="images/reject.png" align="middle" class="noPrint mouseover"
                                                 id="imgReject"/>
                                            <?php
                                        }
                                    }


                                    //------------
                                    $rejectMode = rejectMode($intUser, $savedLevels, $poNo, $year, $intStatus);

                                    //--------------
                                    //echo $intStatus."-".$rejectMode."-".$lastApprovalFromUser."-".$flag;
                                    if (($rejectMode == 1) && ($flag != 1)) {
                                        ?>
                                        <img src="images/reject.png" align="middle" class="noPrint mouseover"
                                             id="imgReject"/>
                                        <?php

                                    }
                                    ?></td>
                            </tr>
                            <tr>
                                <td colspan="9" class="APPROVE"><?php
                                    if ($intStatus == 2) {
                                        ?>
                                        <div align="left" style="color:#00C">
                                            <input id="chkSendEmail" type="checkbox"/>
                                            Send a copy of email to supplier.
                                        </div><?php
                                    }

                                    ?></td>
                            </tr>
                            <tr>
                                <?php
                                if ($intStatus == 1) {
                                    ?>
                                    <td colspan="9" class="APPROVE" align="center" style="color:#0C0">CONFIRMED</td>
                                    <?PHP
                                } else if ($intStatus == 0) {
                                    ?>
                                    <td colspan="9" class="APPROVE" style="color:#F00">REJECTED</td>
                                    <?php
                                } else if ($intStatus == -10) {
                                    ?>
                                    <td colspan="9" class="CANCELLED" style="color:#F00">
                                        CANCELLED <?php if ($system_cancel == 1) {
                                            echo "BY SYSTEM";
                                        } ?></td>
                                    <?php
                                } else {
                                    ?>
                                    <td width="10%" colspan="9" class="APPROVE" style="color:#00C">PENDING</td>
                                    <?php
                                }
                                ?>
                            </tr>
                            <tr>
                                <td width="1%">&nbsp;</td>
                                <td width="10%"><span class="normalfnt">PO No</span></td>
                                <td width="2%" align="center" valign="middle"><strong>:</strong></td>
                                <td width="23%"><span class="normalfnt"><?php echo $poNo ?>/<?php echo $year ?></span>
                                </td>
                                <td width="12%" class="normalfnt">Delivery Date</td>
                                <td width="1%" align="center" valign="middle"><strong>:</strong></td>
                                <td width="10%"><span class="normalfnt"><?php echo $deliveryDate ?></span></td>
                                <td width="11%">
                                    <div id="divPoNo" style="display:none"><?php echo $poNo ?>/<?php echo $year ?></div>
                                    <strong><span class="normalfnt">Payment Term</span></strong></td>
                                <td width="11%"><strong>:</strong>&nbsp;<span
                                            class="normalfnt"><?php echo $payTerm ?> </span></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="normalfnt">Deliver To</td>
                                <td align="center" valign="middle"><strong>:</strong></td>
                                <td><span class="normalfnt"><?php echo $company . "-" . $location ?></span></td>
                                <td><span class="normalfnt">PO Date</span></td>
                                <td align="center" valign="middle"><strong>:</strong></td>
                                <td><span class="normalfnt"><?php echo $pODate ?></span></td>
                                <td class="normalfnt">Shipment Term</td>
                                <td><strong>:</strong>&nbsp;<span class="normalfnt"><?php echo $shipmentTerm ?> </span>
                                </td>
                            </tr>

                            <tr>
                                <td>&nbsp;</td>
                                <td class="normalfnt">Supplier</td>
                                <td align="center" valign="middle"><strong>:</strong></td>
                                <td><span class="normalfnt"><?php echo $supplier ?></span></td>
                                <td><span class="normalfnt"> PO By</span></td>
                                <td align="center" valign="middle"><strong>:</strong></td>
                                <td><span class="normalfnt"><?php echo $pOBy ?></span></td>
                                <td><strong><span class="normalfnt">Shipment Mode</span></strong></td>
                                <td><span class="normalfnt"><strong>: </strong><?php echo $shipmentMode ?></span></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="normalfnt">Currency</td>
                                <td align="center" valign="middle"><strong>:</strong></td>
                                <td><span class="normalfnt"><?php echo $currency ?></span></td>
                                <td><span class="normalfnt"> Payment Mode</span></td>
                                <td align="center" valign="middle"><strong>:</strong></td>
                                <td><span class="normalfnt"><?php echo $payMode ?></span></td>
                                <td class="normalfnt">PO Revise No</td>
                                <td class="normalfnt"><strong>: </strong><?php echo $reviseNo; ?></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td class="normalfnt">Remarks</td>
                                <td align="center" valign="middle"><strong>:</strong></td>
                                <td colspan="6" align="left"><span class="normalfnt"><?php echo $remarks ?></span></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table width="100%">
                            <tr>
                                <td width="1%">&nbsp;</td>
                                <td colspan="7" class="normalfnt" align="left">
                                    <table width="100%" class="rptBordered" id="tblMainGrid" cellspacing="0"
                                           cellpadding="0">
                                        <tr class="">
                                            <th width="5%" height="22" style="display:none">PRN No</th>
                                            <th width="10%">Main Category</th>
                                            <th width="13%">Sub Category</th>
                                            <th width="16%">Item</th>
                                            <th width="3%">UOM</th>
                                            <th width="5%"> CostCenter</th>
                                            <th width="7%">Unit Price</th>
                                            <th width="6%">Discount</th>
                                            <th width="7%">Tax Code</th>
                                            <th width="4%">Qty</th>
                                            <th width="6%">Amount</th>
                                            <th width="9%">SVAT Amount</th>
                                            <th width="8%">Tax Amount</th>
                                            <th width="6%">Total</th>
                                        </tr>
                                        <?php
                                        $sql1 = "SELECT
trn_podetails.intPONo,
trn_podetails.intPOYear,
trn_podetails.intPrnNo,
trn_podetails.intPrnYear,
trn_podetails.intItem,
mst_item.strCode as itemCode, 
mst_item.strName,
mst_maincategory.strName as mainCategory,
mst_subcategory.strName as subCategory,
round(trn_podetails.dblUnitPrice,6) AS dblUnitPrice,
trn_podetails.dblDiscount,
trn_podetails.intTaxCode,
sum(trn_podetails.dblTaxAmmount) as dblTaxAmmount,
mst_financetaxgroup.strCode, 
trn_podetails.SVAT_ITEM,
sum(trn_podetails.dblQty) as dblQty,
-- ROUND(sum((round((round(dblUnitPrice,6)*(100-dblDiscount)/100*dblQty),2))),2) as totAmmount, 
-- ROUND(sum((round((round(dblUnitPrice,6)*(100-dblDiscount)/100*dblQty+dblTaxAmmount),2))),2) as taxAmmount, 
-- ROUND(sum(((round(dblUnitPrice,6)*(100-dblDiscount)/100)*dblQty+dblTaxAmmount)),2) as taxAmmount, 
mst_item.intUOM, 
mst_units.strName as uom,
trn_podetails.CostCenter
FROM
trn_podetails
Inner Join mst_item ON trn_podetails.intItem = mst_item.intId
Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
left Join mst_financetaxgroup ON trn_podetails.intTaxCode = mst_financetaxgroup.intId 
Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
WHERE
trn_podetails.intPONo =  '$poNo' AND
trn_podetails.intPOYear =  '$year' 
group by trn_podetails.intItem,trn_podetails.intTaxCode,dblUnitPrice,dblDiscount  
having dblQty > 0 
Order by trn_podetails.intPrnNo DESC,trn_podetails.intPrnYear DESC,mst_maincategory.strName ASC, mst_subcategory.strName ASC, mst_item.strName ASC   
";
                                        $result1 = $db->RunQuery($sql1);
                                        $totQty = 0;
                                        $totAmmount = 0;
                                        $totTax = 0;
                                        $totTaxAndAmmount = 0;
                                        $t = 0;
                                        while ($row = mysqli_fetch_array($result1)) {
                                            //hasitha  //ticker:-2260  //2018-02-27
                                            $amount					= round($row['dblUnitPrice']*(100-$row['dblDiscount'])/100*$row['dblQty'],2);
                                            //ROUND(sum((round((round(dblUnitPrice,6)*(100-dblDiscount)/100*dblQty+dblTaxAmmount),2))),2) as taxAmmount,
                                            $taxammount             = round($row['dblUnitPrice']*(100-$row['dblDiscount'])/100*$row['dblQty']+$row['dblTaxAmmount'],2);
                                            if ($row['CostCenter'] != '') {
                                                $costCenter = getCostCenter($row['CostCenter'], $row['intPONo'], $row['intPOYear'], $row['intItem']);

			    }
				if($row['SVAT_ITEM']==1){
					//$svatAmount	=	$obj_fin_com->getSvatValue($row['totAmmount']+$row['dblTaxAmmount'],'RunQuery');
					$svatAmount	=	$obj_fin_com->getSvatValue_poSaved($poNo,$year,$amount+$row['dblTaxAmmount'],'RunQuery');
					if($svatAmount=='' || $svatAmount==0){
						$svatAmount	=	$obj_fin_com->getSvatValue($amount+$row['dblTaxAmmount'],'RunQuery');
					}
				}
				else{
					$svatAmount	=   0;
				}
				if(($row['intTaxCode']!='') && (($row['intTaxCode']!=0))){
					$t++;
					$arrSum[$row['strCode']] +=$row['dblTaxAmmount'];
				}
	  ?>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfnt" style="display:none" >&nbsp;<?php echo $row['intPrnNo']."/".$row['intPrnNo']; ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['mainCategory']?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['subCategory'] ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['strName'] ?>&nbsp;</td>
              <td class="normalfntMid" >&nbsp;<?php echo $row['uom'] ?>&nbsp;</td>
              <td class="normalfntMid"><?php echo $costCenter; ?></td>
              <td class="normalfntRight" >&nbsp;<?php echo number_format($row['dblUnitPrice'],6) ?></td>
              <td class="normalfntRight" >&nbsp;<?php echo number_format($row['dblDiscount'],2) ?></td>
              <td class="normalfnt" >&nbsp;<?php echo $row['strCode'] ?></td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['dblQty'] ?></td>
              <td class="normalfntRight" >&nbsp;<?php echo number_format($amount,2) ?></td>
              <td class="normalfntRight" ><?php echo number_format($svatAmount,2) ?></td>
              <td class="normalfntRight" >&nbsp;<?php echo number_format($row['dblTaxAmmount'],2) ?></td>
              <td class="normalfntRight" >&nbsp;<?php echo number_format($taxammount,2) ?></td>
              </tr>
      <?php
			$totQty				+= $row['dblQty'];
			$totAmmount			+= $amount;
			$totTax				+= $row['dblTaxAmmount'];
			$totSvat			+= $svatAmount;
			$totTaxAndAmmount	+= $taxammount;
			}

	  ?>
            <tr class="normalfnt"  bgcolor="#CCCCCC">
              <td class="normalfnt" style="display:none" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $totQty ?></td>
              <td class="normalfntRight" >&nbsp;<?php echo number_format($totAmmount,2) ?></td>
              <td class="normalfntRight" ><?php echo number_format($totSvat,2) ?></td>
              <td class="normalfntRight" >&nbsp;<?php echo number_format($totTax,2) ?></td>
              <td class="normalfntRight" >&nbsp;<?php echo number_format($totTaxAndAmmount,2) ?></td>
              </tr>
            </table>
          </td>
        <td width="1%">&nbsp;</td>
        </tr>

      </table>
    </td>
</tr>
    <?php
    $sql_headCompany        = "SELECT strName FROM mst_companies WHERE intId = '$headCompanyId'";
    $result_headCompany     = $db->RunQuery($sql_headCompany);
    $headCompany            = mysqli_fetch_array($result_headCompany);
    $headCompnyName         = $headCompany['strName'];
    ?><tr>
           <td bgcolor="#FFFFFF"><table width="100%" cellspacing="0" cellpadding="0">
       			<tr>
                <td width="75%">
                    <table>
                    <tr>
                      <td class="normalfnt"><p>&nbsp;</p> <p><strong><em>Terms &amp;  Conditions </em></strong></p>
                        <ul>
                          <li>This is an automated purchase order therefore manual  signature is not required.&nbsp;</li>
                          <li>No metal pins or clips should be included in packing /  packaging as  <?php echo $headCompnyName; ?><!-- Screenline (Pvt) Ltd--> plants are <strong>METAL FREE</strong>.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </li>
                          <li>Please issue purchase invoice within 2 days upon the  receipt of the purchase order in favor of <?php echo $headCompnyName; ?> <!-- Screenline (Pvt) Ltd-->. </li>
                          <li>Purchase order no&rsquo;s must be clearly quoted on all  shipping documents &amp; if not supplier will be liable for a charge of US$ 100  per shipment. </li>
                          <li>Tolerance +/- 3% on quantity will be accepted. </li>
                          <li>Goods delivered exceeding the stipulated tolerance  level, will not be return or paid. </li>
                          <li>All the samples requirement to be couriered on FOC  basis &amp; courier pre-paid basis. </li>
                          <li>All the samples/bulk stones/studs/nail heads/convex  etc.. Should be Lead/Nickel free. </li>
                          <li>Except for color all the other criteria&rsquo;s of the  product has to 100% match to the item given on catalogues or items supplied at  the development stage. Only 0-5% color variance against given catalogues will  be accepted in bulk goods. .&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </li>
                          <li>In house/Independent wash durability test report to be  submitted for each &amp; every consignment based on the requirement of the  respective brand. </li>
                          <li>Falling to meet agreed delivery/split deliveries  supplier will be held responsible for all additional cost incurred by <?php echo $headCompnyName; ?><!-- Screenline (Pvt) Ltd-->. </li>
                          <li>SGS test report is required for the ITEMS which do not have OEKO TEX certification. </li>
                      </ul></td>
                      </tr><tr><td>&nbsp;</td></tr>
<?php
 //	if($intStatus!=0)
//	{
					   $flag=0;
					 $sqlc = "SELECT
							trn_poheader_approvedby.intApproveUser,
							trn_poheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName,
							trn_poheader_approvedby.intApproveLevelNo
							FROM
							trn_poheader_approvedby
							Inner Join sys_users ON trn_poheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							trn_poheader_approvedby.intPONo =  '$poNo' AND
							trn_poheader_approvedby.intYear =  '$year'   order by dtApprovedDate asc
";
                                        $resultc = $db->RunQuery($sqlc);
                                        while ($rowc = mysqli_fetch_array($resultc)) {
                                            if ($rowc['intApproveLevelNo'] == 1)
                                                $desc = "1st ";
                                            else if ($rowc['intApproveLevelNo'] == 2)
                                                $desc = "2nd ";
                                            else if ($rowc['intApproveLevelNo'] == 3)
                                                $desc = "3rd ";
                                            else
                                                $desc = $rowc['intApproveLevelNo'] . "th ";
                                            //  $desc=$ap.$desc;
                                            $desc2 = $rowc['UserName'] . "(" . $rowc['dtApprovedDate'] . ")";
                                            if ($rowc['UserName'] == '')
                                                $desc2 = '---------------------------------';

                                            if ($rowc['intApproveLevelNo'] > 0) {
                                                $flag = 1;
                                                ?>

                                                <tr>
                                                    <td width="839" bgcolor="#FFFFFF"><span
                                                                class="normalfnt"><strong><?php echo $desc; ?> Approved By - </strong></span><span
                                                                class="normalfnt"><?php echo $desc2; ?></span></td>
                                                </tr>
                                                <?php
                                                //}
                                                //else{
                                            }
                                            if ($rowc['intApproveLevelNo'] == 0) {
                                                $flag = 2;
                                                ?>
                                                <tr>
                                                    <td bgcolor="#FFFFFF"><span
                                                                class="normalfnt"><strong> Rejected By - </strong></span><span
                                                                class="normalfnt"><?php echo $rowc['UserName'] . "(" . $rowc['dtApprovedDate'] . ")"; ?></span>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            if ($rowc['intApproveLevelNo'] == -1) {
                                                $flag = 3;
                                                ?>
                                                <tr>
                                                    <td bgcolor="#FFFFFF"><span
                                                                class="normalfnt"><strong> Revised By - </strong></span><span
                                                                class="normalfnt"><?php echo $rowc['UserName'] . "(" . $rowc['dtApprovedDate'] . ")"; ?></span>
                                                    </td>
                                                </tr>
                                                <?php
                                            }

                                            if ($rowc['intApproveLevelNo'] > 0) {
                                                $finalSavedLevel = $rowc['intApproveLevelNo'];
                                            }
                                        }//end of while

                                        if ($flag > 1) {
                                            $finalSavedLevel = 0;
                                        }

                                        if (($flag <= 1) || ($intStatus > 0)) {
                                            for ($j = $finalSavedLevel + 1; $j <= $savedLevels; $j++) {
                                                if ($j == 1)
                                                    $desc = "1st ";
                                                else if ($j == 2)
                                                    $desc = "2nd ";
                                                else if ($j == 3)
                                                    $desc = "3rd ";
                                                else
                                                    $desc = $j . "th ";
                                                $desc2 = '---------------------------------';
                                                ?>
                                                <tr>
                                                    <td bgcolor="#FFFFFF"><span
                                                                class="normalfnt"><strong><?php echo $desc; ?> Approved By - </strong></span><span
                                                                class="normalfnt"><?php echo $desc2; ?></span></td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </table>
                                </td>
                                <td width="23%" bgcolor="#FFFFFF" valign="top" align="right">
                                    <table width="86%" cellspacing="0" cellpadding="0">
                                        <?php
                                        if ($totSvat > 0) {
                                            ?>
                                            <tr>
                                                <td width="63%" class="normalfnt">SVAT Amount</td>
                                                <td width="3%" class="normalfnt">=</td>
                                                <td width="34%"
                                                    class="normalfntRight"><?php echo number_format($totSvat, 2) ?></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                        if ($t > 0) {
                                            foreach ($arrSum as $key => $value) {

                                                ?>
                                                <tr>
                                                    <td class="normalfnt"><?php echo $key; ?></td>
                                                    <td class="normalfnt">=</td>
                                                    <td class="normalfntRight"><?php echo number_format($value, 2); ?></td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </table>
                                </td>
                                <td width="2%">&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>


                <tr height="40">
                    <?php
                    //$locationId = '';
                    $sql = "SELECT mst_locations.intCompanyId FROM mst_locations WHERE mst_locations.intId =  '$locationId'";
                    $result = $db->RunQuery($sql);
                    $row = mysqli_fetch_array($result);
                    $createCompanyId = $row['intCompanyId'];
                    ?>
                    <?Php
                    if ($intStatus == 2) {
                        $url = "{$backwardseperator}presentation/sendToApproval_final_PO.php";
                    } else {
                        $url = "{$backwardseperator}presentation/sendToApproval.php";
                    }// * file name
                    $url .= "?status=$intStatus";                                        // * set recent status
                    $url .= "&approveLevels=$savedLevels";                                // * set approve levels in order header
                    $url .= "&programCode=$programCode";                                // * program code (ex:P10001)
                    $url .= "&program=SUPPLIER PO";                                    // * program name (ex:Purchase Order)
                    $url .= "&companyId=$createCompanyId";                                    // * created company id
                    $url .= "&createUserId=$intCreatedUser";
                    $url .= "&shipmentTermId=$shipmentTermId";

                    $url .= "&field1=PO No";
                    $url .= "&field2=PO Year";
                    $url .= "&value1=$poNo";
                    $url .= "&value2=$year";

                    $url .= "&subject=SUPPLIER PO FOR APPROVAL ('$poNo'/'$year')";

                    $url .= "&statement1=Please Approve this";
                    $url .= "&statement2=to Approve this";
                    // * doc year
                    $url .= "&link=" . urlencode(base64_encode($_SESSION['mainPath'] . "?q=934&poNo=$poNo&year=$year&approveMode=1"));
                    ?>
                    <td align="center" class="normalfntMid">
                        <iframe id="iframeFiles2" src="<?php echo $url; ?>" name="iframeFiles"
                                style="width:500px;height:150px;border:none"></iframe>
                    </td>
                </tr>
                <tr height="40">
                    <td align="center" class="normalfntMid"><span
                                class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span>
                    </td>
                </tr>
            </table>
        </div>
    </form>
    </body>
    </html>

<?php

//------------------------------function load loadrejMode---------------------
function rejectMode($intUser, $savedLevels, $poNo, $year, $intStatus)
{
    global $db;

    $approveLevel = $savedLevels + 1 - $intStatus;

    //echo $savedStat;
    $rejMode = 0;
    $sql = "SELECT
				menus_special.intStatus
FROM
				menus_special
				Inner Join menus_special_permision ON menus_special.intId = menus_special_permision.intSpMenuId
WHERE
menus_special_permision.intUser =  '$intUser' AND
menus_special.strPermisionType =  'Permission to reject PO for user who approved the PO'";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $rejMode = $row['intStatus'];


    $sqlp = "SELECT
				trn_poheader_approvedby.dtApprovedDate
				FROM trn_poheader_approvedby
				WHERE
				trn_poheader_approvedby.intPONo =  '$poNo' AND
				trn_poheader_approvedby.intYear =  '$year' AND
				trn_poheader_approvedby.intApproveUser =  '$intUser' AND
				trn_poheader_approvedby.intApproveLevelNo =  '$approveLevel'";

    $resultp = $db->RunQuery($sqlp);
    $rowp = mysqli_fetch_array($resultp);
    $lastApprovalFromUser = 0;
    if ($rowp['dtApprovedDate'] != '') {
        $lastApprovalFromUser = 1;
    }

    $val = 0;

    if (($intStatus > 1) && ($approveLevel > 0) && ($rejMode == 1) && ($lastApprovalFromUser == 1)) {
        $val = 1;
    }

    return $val;
}

/*function get_finalApprovalPermision($poNo,$year,$intUser,$shipmentTerm){
	
	global $db;
 	$sql="
		select 
		max(amount) as amount,
		intMainCategory
		from (
		SELECT mst_item.intMainCategory,
		Sum(trn_podetails.dblQty) as amount FROM trn_podetails 
		INNER JOIN mst_item ON mst_item.intId = trn_podetails.intItem WHERE
		trn_podetails.intPONo = '$poNo' AND
		trn_podetails.intPOYear = '$year'
		GROUP BY
		mst_item.intMainCategory 
		) as tb1";
	
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$maxAmount 	= $row['amount'];
		$category 	= $row['intMainCategory'];
		
  	$sql="
		SELECT
		IFNULL(GROUP_CONCAT(trn_poheader_approve_level_chart.USER_ID),0) as userList 
		FROM `trn_poheader_approve_level_chart`
		WHERE
		trn_poheader_approve_level_chart.MAIN_CATEGORY_ID = '$category' AND
		trn_poheader_approve_level_chart.SHIPMENT_TERM_ID = '$shipmentTerm' AND
		trn_poheader_approve_level_chart.MAX_AMOUNT >= '$maxAmount' AND 
		trn_poheader_approve_level_chart.USER_ID = '$intUser' 
		";
	
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$val 	= $row['userList'];
		if($val!=0)
		$val=1;
		
		return $val;
}
*/////---------------------------------------------------------
function getCostCenter($CostCenter, $intPONo, $intPOYear, $itemId)
{
    global $db;
    $sql = "SELECT
trn_podetails.CostCenter,
trn_podetails.intPONo,
trn_podetails.intPOYear,
mst_department.strName,
trn_podetails.intItem
FROM
trn_podetails
INNER JOIN mst_department ON trn_podetails.CostCenter = mst_department.intId
where 
trn_podetails.intPONo = '$intPONo' AND trn_podetails.intPOYear= '$intPOYear' AND trn_podetails.intItem ='$itemId' AND trn_podetails.CostCenter=$CostCenter";

    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    return $row['strName'];
}

?>
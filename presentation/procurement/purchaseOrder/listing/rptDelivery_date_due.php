<?php
$thisFilePath =  $_SERVER['PHP_SELF'];
include_once '../../../../dataAccess/DBManager2.php';
include_once "../../../../libraries/mail/mail.php";		
$db =  new DBManager2();

$db->connect();//open connection.

ini_set('max_execution_time', 11111111) ;
$current_date	= date('Y-m-d');
ob_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Delivery Date Due Report</title>
</head>
<style type="text/css">
.reportTitle{
	text-align:center;
	font-weight:bold;
	font-size:12;
	font-family:Verdana, Geneva, sans-serif;
	height:50px;
	}
.normalfnt {
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
#tblMain{
	border-right: 1px solid #ccc;
	border-bottom: 1px solid #ccc;
	border-spacing: 0;
}

#tblMain thead {
    background-color: #dce9f9;
	border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
	text-align:center;
	padding: 2px;
}
#tblMain td {
	border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
	padding: 2px;
}

#tblMain tfoot{
	font-family: Verdana;
	font-size: 10px;
	color: #000000;
	margin: 0px;
	font-weight: bold;
}

.tblBorder{
	border-top:solid;
	border-top-width:thin;
	border-bottom:solid;
	border-bottom-width:thin;
	border-left:solid;
	border-left-width:thin;
	border-right:solid;
	border-right-width:thin;
	border-color:#dce9f9;
}

.reportFooter{
	text-align:center;
	font-weight:bold;
	font-size:10px;
	font-family:Verdana, Geneva, sans-serif;
	}
</style>
<body>
  <form id="frmDeliveryDateDueReport" name="frmDeliveryDateDueReport" method="post" >
  <div align="center">
   <div align="left"><img style="vertical-align:" src="http://accsee.com/nsoft_logo.png"/></div>  
<table width="900" align="center" bgcolor="#FFFFFF" class="tblBorder">
<tr>
  <td class="reportTitle">Delivery Date Due Report AS AT <?php echo $current_date?></td>
</tr>
<tr>
  <td><table width="100%%" border="0" class="normalfnt" id="tblMain">
    <thead>
      <tr>
        <td width="8%" style="text-align:center">PO No</td>
        <td width="34%" style="text-align:center">Supplier Name</td>
        <td width="6%" style="text-align:center">Currency</td>
        <td width="9%" style="text-align:center">PO Date</td>
        <td width="11%" style="text-align:center">Delivery Date</td>
        <td width="10%" style="text-align:center">PO QTY</td>
        <td width="11%" style="text-align:center">GRN QTY</td>
        <td width="11%" style="text-align:center">Balance To GRN</td>
        </tr>
      </thead>
  <?php
$tot_po_qty		= 0;
$tot_grn_qty	= 0;
$tot_bal_to_grn	= 0;
$result = GetMainDetails();
while($row = mysqli_fetch_array($result))
{
?>
    <tr>
      <td><?php echo $row["PONO"]?></td>
      <td><?php echo $row["SUPPLIER_NAME"]?></td>
      <td><?php echo $row["CURRENCY_CODE"]?></td>
      <td style="text-align:center"><?php echo $row["PO_DATE"]?></td>
      <td style="text-align:center"><?php echo $row["DELIVERY_DATE"]?></td>
      <td style="text-align:right"><?php echo number_format($row["PO_QTY"],2)?></td>
      <td style="text-align:right"><?php echo number_format($row["GRN_QTY"],2)?></td>
      <td style="text-align:right"><?php echo number_format($row["BALANCE_TO_GRN"],2)?></td>
      </tr>
  <?php
	$tot_po_qty		+= round($row["PO_QTY"],2);
	$tot_grn_qty	+= round($row["GRN_QTY"],2);
	$tot_bal_to_grn	+= round($row["BALANCE_TO_GRN"],2);
}
?>
    <tfoot>
      <tr>
        <td>&nbsp;</td>
        <td style="text-align:center">TOTAL</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td style="text-align:right"><?php echo number_format($tot_po_qty,2)?></td>
        <td style="text-align:right"><?php echo number_format($tot_grn_qty,2)?></td>
        <td style="text-align:right"><?php echo number_format($tot_bal_to_grn,2)?></td>
        </tr>
      </tfoot> 
    </table></td>
</tr>
<tr>
    <td>&nbsp;</td>
</tr>
<tr >
  <td class="reportFooter">Report Generated Date <?php echo $current_date?></td>
</tr>
</table>
</div>        
</form>
</body>
</html>
<?php
function GetMainDetails()
{
	global $db;
	global $current_date;
	
	$sql = "SELECT
			  CONCAT(PH.intPONo,'/',PH.intPOYear) 							AS PONO,
			  SU.strName         											AS SUPPLIER_NAME,
			  CU.strCode         											AS CURRENCY_CODE,
			  PH.dtmPODate       											AS PO_DATE,
			  PH.dtmDeliveryDate 											AS DELIVERY_DATE,
			  ROUND(SUM(PD.dblQty),2) 										AS PO_QTY,
			  ROUND(SUM(PD.dblGRNQty),2) 									AS GRN_QTY,
			  ROUND(ROUND(SUM(PD.dblQty),2) - ROUND(SUM(PD.dblGRNQty),2),2)	AS BALANCE_TO_GRN
			FROM trn_poheader PH
			  INNER JOIN trn_podetails PD
				ON PD.intPONo = PH.intPONo
				  AND PD.intPOYear = PH.intPOYear
			  INNER JOIN mst_supplier SU
				ON SU.intId = PH.intSupplier
			  INNER JOIN mst_financecurrency CU
				ON CU.intId = PH.intCurrency
			WHERE DATE(PH.dtmDeliveryDate) < '$current_date'
				AND PD.dblGRNQty < PD.dblQty
				AND PH.intStatus = 1
			GROUP BY PH.intPOYear,PH.intPONo
			ORDER BY PH.dtmDeliveryDate ";
	return $db->RunQuery($sql);
}

function GetEmailSendList()
{
	global $db;
	
	$sql = "SELECT
			  U.strEmail		AS USER_EMAIL
			FROM sys_mail_events ME
			  INNER JOIN sys_mail_eventusers MEU
				ON MEU.intMailEventId = ME.intMailIventId
			  INNER JOIN sys_users U
				ON MEU.intUserId = U.intUserId
			WHERE ME.strMailEventName = 'Delivery Date Due Report'";
	return $db->RunQuery($sql);
}

$body 		= ob_get_clean();
$mailHeader	= "Delivery Date Due Report AS AT $current_date";
$result = GetEmailSendList();
while($row = mysqli_fetch_array($result))
{
	sendMessage('nsoft@screenlineholdings.com','NSOFT - SCREENLINE HOLDINGS.',$row['USER_EMAIL'],$mailHeader,$body);
}
echo $body;	
echo 'Delivery Date Due Report -> OK <br>';

$db->commit();		
$db->disconnect();

?>
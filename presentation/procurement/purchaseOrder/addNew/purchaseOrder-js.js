var basePath 		= "presentation/procurement/purchaseOrder/addNew/";	
var bal	=0;
//var reportMenuId 	= 934;

var menuId			= $('#menuId').html();
if(menuId==220){
var reportMenuId	= 934;
}
else if(menuId==1151){
var reportMenuId	= 1156;
}
else if(menuId==1153){
var reportMenuId	= 1157;
}

var flagAllowDefaultExcRate= 0;
var flagRemarks= 0;
var selectedPRNstring='';
			
$(document).ready(function() {
	
  		$("#frmPurchaseOrder").validationEngine();
		$('#frmPurchaseOrder #cboSupplier').focus();
		$('.delImg').die('click').live('click',function(){ 
			var obj	= $(this);
			if( $('#frmPurchaseOrder #txtPoNo').val()== '' && $('#frmPurchaseOrder #txtPoYear').val() == '')
			{
					$(this).parent().parent().remove();
			}
			else 
			{
				var val = $.prompt('Are you sure you want to delete this item?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
						var itemId	= obj.parent().attr('id');
						deleteItems(obj);
													
					}
				}
				});
			}
				disableEnableCurrency();
				calfunction();

		});
		$("#frmPurchaseOrder #butAddItems").die('click').live('click',function(){
			$('#frmPurchaseOrder #cboCurrency').die('change').live('change');
			if($('#cboCurrency').val()==''){
				alert("Please select the currency.");
				return false;
			}
			else if($('#txtExcRate').val()==''){
				alert("Please add the ecxhange rate.");
				return false;
			}
			else if($('#dtDate').val()==''){
				alert("Please select the date.");
				return false;
			}
			
			
		closePopUp();
		popupWindow3('1');
		var delivertTo = $('#cboDeliverTo').val();
		$('#popupContact1').load(basePath+'purchaseOrderPopup.php?q='+$('#menuId').html()+'&delivertTo='+delivertTo,function(){
				//checkAlreadySelected();
				$('#frmPurchaseOrderPopup #butAdd').die('click').live('click',addClickedRows);
				$('#frmPurchaseOrderPopup #butClose1').die('click').live('click',disablePopup);
				 //-------------------------------------------- 
				  $('#frmPurchaseOrderPopup #cboMainCategory').die('change').live('change',function(){
						var mainCategory = $('#cboMainCategory').val();
						var url 		= basePath+"purchaseOrder-db-get.php?requestType=loadSubCategory&mainCategory="+mainCategory;
						var httpobj 	= $.ajax({url:url,async:false})
						document.getElementById('cboSubCategory').innerHTML=httpobj.responseText;
						
				  });
				 //-------------------------------------------- 
				  $('#frmPurchaseOrderPopup #chkPRN').die('click').live('click',function(){
						if(document.getElementById('chkPRN').checked==true){
						 document.getElementById('cboPRN').disabled=false;
						 document.getElementById('chkGrpPRN').checked=false;
						 $('#butAddPrnGroup').hide();
						 }
						else{
						 $('#cboPRN').val('');
						 document.getElementById('cboPRN').disabled=true;
						// document.getElementById('chkGrpPRN').checked=true;
						}
				  });
				 //-------------------------------------------- 
				  $('#frmPurchaseOrderPopup #chkGrpPRN').die('click').live('click',function(){
						if(document.getElementById('chkGrpPRN').checked==true){
						 $('#cboPRN').val('');
						 document.getElementById('cboPRN').disabled=true;
						 document.getElementById('chkPRN').checked=false;
						 $('#butAddPrnGroup').show();
						 }
						else{
						 $('#butAddPrnGroup').hide();
						// document.getElementById('chkGrpPRN').disabled=true;
						// document.getElementById('cboPRN').checked=true;
						}
				  });
				  //-------------------------------------------
				  $('#frmPurchaseOrderPopup #butAddPrnGroup').die('click').live('click',function(){
						showWaiting();
						$('#divBackgroundImg').css('left','300px');
						$('#divBackgroundImg').css('top','100px');
						$('#divBackgroundImg').load(basePath+'purchaseOrderPRNgroupPopup.php',function(){
						$('#frmPurchaseOrderPRNgroupPopup #butAddP').die('click').live('click', addSelectedPRNs);
						$('#frmPurchaseOrderPRNgroupPopup #butClose2').die('click').live('click', hideWaiting);
						});
				});
				 //-------------------------------------------- 
				  $('#frmPurchaseOrderPopup #imgSearchItems').die('click').live('click',function(){
						var rowCount = document.getElementById('tblItemsPopup').rows.length;
						for(var i=1;i<rowCount;i++)
						{
							document.getElementById('tblItemsPopup').deleteRow(1);
						}
						var prnNo = $('#cboPRN').val();
						if(document.getElementById('chkPRN').checked==true)
							var prnSelection	=1;
						else
							var prnSelection	=0;
							var currency		=$('#cboCurrency').val();
							var poDate			=$('#dtDate').val();
							var supplierId		=$('#cboSupplier').val();
							var mainCategory 	= $('#cboMainCategory').val();
							var subCategory 	= $('#cboSubCategory').val();
							var description 	= $('#txtItmDesc').val();
							var itemCode 		= $('#txtItmCode').val();
							var menuId			=$('#menuId').html();

						var url 		= basePath+"purchaseOrder-db-get.php?requestType=loadItems";
						var httpobj = $.ajax({
							url:url,
							dataType:'json',
							type:'POST',
							data:"prnSelection="+prnSelection+"&prnNo="+prnNo+"&mainCategory="+mainCategory+"&subCategory="+subCategory+"&description="+URLEncode(description)+"&itemCode="+URLEncode(itemCode)+"&currency="+currency+"&poDate="+poDate+"&supplierId="+supplierId+"&menuId="+menuId,
							async:false,
							success:function(json){

								var length = json.arrCombo.length;
								var arrCombo = json.arrCombo;

								for(var i=0;i<length;i++)
								{
									var itemId=arrCombo[i]['itemId'];	
									var maincatId=arrCombo[i]['maincatId'];	
									var subCatId=arrCombo[i]['subCatId'];	
									var code=arrCombo[i]['code'];
                                                                        var supItemCode=arrCombo[i]['supItemCode'];
									var itemName=arrCombo[i]['itemName'];	
									var uom=arrCombo[i]['uom'];	
									var currency=arrCombo[i]['currency'];	
									var unitPrice=arrCombo[i]['unitPrice'];	
									var mainCatName=arrCombo[i]['mainCatName'];
									var subCatName=arrCombo[i]['subCatName'];	
									var prnQty=arrCombo[i]['prnQty'];
									var poQty=arrCombo[i]['poQty'];
									var balQty=parseFloat(prnQty)-parseFloat(poQty);
									flagRemarks=arrCombo[i]['flagRemarks'];
									var stockBal 		= parseFloat(arrCombo[i]['stockBal']);
									var reOrderLevel 	= parseFloat(arrCombo[i]['ReOrderLevel']);
										
									var content='<tr class="normalfnt">';
									
/*
BEGIN - THIS CONDITION COMMENT FOR TEMPORARY  - 2013-06-27
									if((currency=='') || (stockBal>=reOrderLevel && permission_reorder==false && maincatId=='1')){
										content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'"><input type="checkbox" id="chkDisp" disabled="disabled"/></td>';
									}
									else{
										content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'"><input type="checkbox" id="chkDisp" /></td>';
									}
END 	- THIS CONDITION COMMENT FOR TEMPORARY - 2013-06-27
*/
								
									if(currency==''){
										content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'"><input type="checkbox" id="chkDisp" disabled="disabled"/></td>';
									}
									else{
										content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'"><input type="checkbox" id="chkDisp" /></td>';
									}
									
									content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'">'+mainCatName+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'">'+subCatName+'</td>';
                                                                        if(supItemCode!=null)
                                                                        {
									content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'">'+supItemCode+'</td>';
                                                                        }else{
                                                                        content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'">'+code+'</td>';   
                                                                        }
									content +='<td align="center" bgcolor="#FFFFFF" id="'+uom+'">'+itemName+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+unitPrice+'" style="display:none" >'+prnNo+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+poQty+'" >'+prnQty+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+poQty+'">'+currency+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+stockBal+'">'+stockBal+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+reOrderLevel+'">'+reOrderLevel+'</td>';
									add_new_row('#frmPurchaseOrderPopup #tblItemsPopup',content);
								}
									checkAlreadySelected();

							}
						});
						
				  });
				  //-------------------------------------------------------
					//$('#frmPurchaseOrderPopup #butAdd').die('click').live('click',addClickedRows);
					//$('#frmPurchaseOrderPopup #butClose1').die('click').live('click',disablePopup);
				  //-------------------------------------------------------
			});	
			
		});
	
	//--------------------------------------------
   
  //-------------------------------------------- 
  $('#frmPurchaseOrder #cboCurrency').die('change').live('change',function(){
	  loadExchangeRateDetails();
	  clearRows();
  });
  
  $('#frmPurchaseOrder .balQty').die('keyup').live('keyup',function(){
	  bal	=$(this).parent().parent().find('.balQty').val();
  });

  $('#frmPurchaseOrder .calMax').die('keyup').live('keyup',function(){
	  calculateMaxPO(this);
  });
  $('#frmPurchaseOrder .calculateValue').die('keyup').live('keyup',function(){
	  calfunction();
  });
 
  //-------------------------------------------------------
   $('#frmPurchaseOrder #cboSupplier').die('change').live('change',function(){
	   
	    var supplier = $('#cboSupplier').val();
	    var date = $('#dtDate').val();
	    var currencyId = $('#cboCurrency').val();
	    var currency = $('#cboCurrency').text();
		var url 		= basePath+"purchaseOrder-db-get.php?requestType=loadSupplierDetails";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:"supplier="+supplier+"&date="+date,
			async:false,
			success:function(json){
					var supplierEmailFlag	=	json.supplierEmailFlag;
					var supplierName		=	json.supplierName;
					if(supplierEmailFlag==0){
						alert("There is no Supplier Email for supplier '"+supplierName+"'");
						$('#cboSupplier').val('');
						return false;
					}
					$('#frmPurchaseOrder #cboCurrency').val(json.currency);
					$('#frmPurchaseOrder #cboPayTerm').val(json.payTerm);
					$('#frmPurchaseOrder #cboPayMode').val(json.payMode);
					$('#frmPurchaseOrder #cboShipmentMode').val(json.shipmentMode);
					//document.getElementById("txtExcRate").value=json.excRate;
			}
		});
		//$('#frmPurchaseOrder #cboCurrency').die('change').live('change');	
		//if($('#frmPurchaseOrder #txtExcRate').val()=='' || document.getElementById('#frmPurchaseOrder #txtExcRate').value==0)
		//alert("There is no saved exchange rate for this 'Date' and 'Currency'.");
	  	loadExchangeRateDetails();
	    loadExchangeRate(basePath);
  });
  //-------------------------------------------------------
 
  $('#frmPurchaseOrder #butSave').die('click').live('click',function(){
	var requestType = '';
	if ($('#frmPurchaseOrder').validationEngine('validate'))   
    { 
		showWaiting();
		var data = "requestType=save";
		
			data+="&serialNo="		+	$('#txtPoNo').val();
			data+="&Year="	+	$('#txtPoYear').val();
			data+="&supplier="	+	$('#cboSupplier').val();
			data+="&currency="	+	$('#cboCurrency').val();
			data+="&payTerms="	+	$('#cboPayTerm').val();
			data+="&payMode="			+	$('#cboPayMode').val();
			data+="&shipmentTerm="	+	$('#cboShipmentTerm').val();
			data+="&shipmentMode="	+	$('#cboShipmentMode').val();
			data+="&deliveryTo="	+	$('#cboDeliverTo').val();
			data+="&deliveryDate="	+	$('#dtDeliveryDate').val();
			data+="&remarks="	+	URLEncode($('#txtRemarks').val());
			data+="&date="			+	$('#dtDate').val();
			data+="&menuId="	+	$('#menuId').html();



			var rowCount = document.getElementById('tblPOItems').rows.length;
			if(rowCount==1){
				alert("items not selected to PO");hideWaiting();
				return false;				
			}
			else if($('#dtDeliveryDate').val()<$('#dtDate').val()) {
				alert("Invalid Delivery date");hideWaiting();
				return false;				
			}
			var row = 0;
			
			var arr="[";
			
			var error=0;
			for(var i=1;i<rowCount;i++)
			{
					 arr += "{";
					 
					var prnNo 		= 	document.getElementById('tblPOItems').rows[i].cells[1].innerHTML;
					var itemId 		= 	document.getElementById('tblPOItems').rows[i].cells[4].id;
					var unitPrice 	= 	document.getElementById('tblPOItems').rows[i].cells[7].childNodes[0].value;
					var discount 	= 	document.getElementById('tblPOItems').rows[i].cells[8].childNodes[0].value;
					var taxCode 	= 	document.getElementById('tblPOItems').rows[i].cells[9].childNodes[0].value;
					var Qty 		= 	document.getElementById('tblPOItems').rows[i].cells[11].childNodes[0].value;
					var MOQ 		= 	document.getElementById('tblPOItems').rows[i].cells[12].childNodes[0].value;
					var rolling		= 	document.getElementById('tblPOItems').rows[i].cells[13].childNodes[0].value;
					var svatItem 	= 	(document.getElementById('tblPOItems').rows[i].cells[10].childNodes[0].checked?1:0);
					var remarks		= 	document.getElementById('tblPOItems').rows[i].cells[15].childNodes[0].value;
                	var costcenter	= 	document.getElementById('tblPOItems').rows[i].cells[16].childNodes[0].value;

                	//alert(svatItem);
					//return;
								//($('#chkSvatItem').attr('checked')?1:0);
					
				//	var remarks = 	document.getElementById('tblPOItems').rows[i].cells[12].childNodes[0].value;
					if(prnNo=='' && remarks==''){
							alert("Remark is compulsory for non-prn based purchase notes");hideWaiting();
							return false;				
					}
				 
				 
				 	if((Qty>0) && (unitPrice>0)){

						arr += '"prnNo":"'+		prnNo +'",' ;
						arr += '"itemId":"'+	itemId +'",' ;
						arr += '"unitPrice":"'+	unitPrice +'",' ;
						arr += '"discount":"'+	discount +'",' ;
						arr += '"taxCode":"'+	taxCode +'",' ;
						arr += '"Qty":"'+		Qty +'",' ;
						arr += '"MOQ":"'+		MOQ +'",' ;
						arr += '"rolling":"'+	rolling +'",' ;
						arr += '"svatItem":"'+	svatItem +'",' ;
						arr += '"remarks":"'+		remarks +'",' ;
                        arr += '"costcenter":"'+		costcenter +'"' ;
						arr +=  '},';
						
				 	}
					else{
						error++;
					}
			}
			if(error>0){
				alert("Invalid Qty or Unit Price");hideWaiting();
				return false;				
			}
			
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;

		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = basePath+"purchaseOrder-db-set.php";
     	var obj = $.ajax({
			url:url,
			type:'post',
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmPurchaseOrder #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						//$('#frmSampleInfomations').get(0).reset();
						//saveImage(json.sampleNo,json.sampleYear,json.revisionNo);
						//loadCombo_frmSampleInfomations();
						var t=setTimeout("alertx()",1000);
						$('#txtPoNo').val(json.serialNo);
						$('#txtPoYear').val(json.year);
						if(json.confirmationMode==1){
							$('#butConfirm').show();
						}
						else{
							$('#butConfirm').hide();
						}
						hideWaiting();
						return;
					}
					hideWaiting();
				},
			error:function(xhr,status){
					hideWaiting();
					$('#frmPurchaseOrder #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
   
   
	$('#frmPurchaseOrderPRNgroupPopup #cboPOType').die('change').live('change',function(){

			deleteAllPRNs();
			var poType=$('#cboPOType').val();

			var tbl  = document.getElementById('tblPRNsPopup');
			var rows = tbl.getElementsByTagName('tr');
			
			if(poType==1){
				  var cels = rows[0].getElementsByTagName('td')
				  cels[1].style.display='block';
				  cels[0].style.display='none';
				  $('#butAddP').hide();
			}
			else{
				  var cels = rows[0].getElementsByTagName('td')
				  cels[0].style.display='block';
				  cels[1].style.display='none';
				  $('#butAddP').show();
			}
			var url 		= basePath+"purchaseOrder-db-get.php?requestType=loadPRNnos";
			var httpobj = $.ajax({
				url:url,
				dataType:'json',
				type:'POST',
				data:"poType="+poType+"&menuId="+$('#menuId').html(),
				async:false,
				success:function(json){

				var length = json.arrCombo.length;
				var arrCombo = json.arrCombo;
				var content='';
				for(var i=0;i<length;i++)
				{
					var prnNo=arrCombo[i]['prnNo'];	
					var prnYear=arrCombo[i]['prnYear'];	
			
					content='<tr class="normalfnt">';
					if(poType==1){
					content +='<td align="center" bgcolor="#FFFFFF" id="'+prnNo+'" style="display:none"><input id="chkPRN" type="checkbox"></td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+prnYear+'"><input id="chkNotPO" type="checkbox" class="chkNotPO"></td>';
					}
					else{
					content +='<td align="center" bgcolor="#FFFFFF" id="'+prnNo+'"><input id="chkPRN" type="checkbox"></td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+prnYear+'" style="display:none"><input id="chkNotPO"  class="chkNotPO" type="checkbox"></td>';
					}
					content +='<td align="center" bgcolor="#FFFFFF" id="'+prnNo+'" class="prnNo">'+prnNo+'/'+prnYear+'</td>';
					//content='</tr>';
					add_new_row('#frmPurchaseOrderPRNgroupPopup #tblPRNsPopup',content);
				}
	 	  } 
  	 });
   });
	
	//--------------refresh the form----------
	$('#frmPurchaseOrder #butNew').die('click').live('click',function(){
		$('#frmPurchaseOrder').get(0).reset();
		clearRows();
		$('#frmPurchaseOrder #txtPoNo').val('');
		$('#frmPurchaseOrder #txtPoYear').val('');
		$('#frmPurchaseOrder #cboSupplier').val('');
		$('#frmPurchaseOrder #cboCurrency').val('');
		document.getElementById('txtExcRate').value='';
		$('#frmPurchaseOrder #cboPayTerm').val('');
		$('#frmPurchaseOrder #cboPayMode').val('');
		$('#frmPurchaseOrder #cboShipmentTerm').val('');
		$('#frmPurchaseOrder #cboShipmentMode').val('');
		$('#frmPurchaseOrder #cboDeliverTo').val('');
		$('#frmPurchaseOrder #txtRemarks').val('');
		$('#frmPurchaseOrder #cboCurrency').removeAttr('disabled',false);
		
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmPurchaseOrder #dtDate').val(d);
		$('#frmPurchaseOrder #dtDeliveryDate').val(d);
	});
	//----------------------------------------
	
/*    $('#frmPurchaseOrder #butDelete').click(function(){
		if($('#frmPurchaseOrder #cboSearch').val()=='')
		{
			$('#frmPurchaseOrder #butDelete').validationEngine('showPrompt', 'Please select Type.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmPurchaseOrder #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = "typeOfPrint-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											data:'requestType=delete&cboSearch='+$('#frmPurchaseOrder #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmPurchaseOrder #butDelete').validationEngine('showPrompt', json.msg,json.type );
												
												if(json.type=='pass')
												{
													$('#frmPurchaseOrder').get(0).reset();
													loadCombo_frmPurchaseOrder();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
	
*/	

//---------------------------------
 $('#frmPurchaseOrderPRNgroupPopup #chkNotPO').die('click').live('click',function(){
	var data='';
	var status =0;
	var prnNo	= $(this).parent().parent().find(".prnNo").html();
	var arr="[";
	if($(this).is(':checked')==true){
		arr += "{";
		arr += '"prnNo":"'+		prnNo +'"' ;
		arr +=  '},';
	}

	arr = arr.substr(0,arr.length-1);
	arr += " ]";
	
	data+="arr="	+	arr;
	//updatePOraisedStatus(data,status);
	$('#frmPurchaseOrderPRNgroupPopup #cboPOType').die('change').live('change');
	});
//------------------------------------------
  $('#frmPurchaseOrderPopup  #chkAll').die('click').live('click',function(){
		if(document.getElementById('chkAll').checked==true)
		 var chk=true;
		else
		 var chk=false;
		
		var rowCount = document.getElementById('tblItemsPopup').rows.length;
		for(var i=1;i<rowCount;i++)
		{
			if(document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].disabled==false)
				document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].checked=chk;
		}
	});
//-----------------------------------
$('#frmPurchaseOrder #butReport').die('click').live('click',function(){
	if($('#txtPoNo').val()!=''){
		window.open('?q='+reportMenuId+'&poNo='+$('#txtPoNo').val()+'&year='+$('#txtPoYear').val());	
	}
	else{
		alert("There is no PO No to view");
	}
});
//----------------------------------	
$('#frmPurchaseOrder #butConfirm').die('click').live('click',function(){
	if($('#txtPoNo').val()!=''){
		window.open('?q='+reportMenuId+'&poNo='+$('#txtPoNo').val()+'&year='+$('#txtPoYear').val()+'&approveMode=1');	
	}
	else{
		alert("There is no PO No to confirm");
	}
});
//-----------------------------------------------------
$('#frmPurchaseOrder #butClose').die('click').live('click',function(){
		//load('main.php');	
});
//--------------refresh the form-----------------------
	$('#frmPurchaseOrder #butNew').die('click').live('click',function(){
		$('#frmPurchaseOrder').get(0).reset();
		clearRows();
		$('#frmPurchaseOrder #txtPoNo').val('');
		$('#frmPurchaseOrder #txtPoYear').val('');
		$('#frmPurchaseOrder #cboSupplier').val('');
		$('#frmPurchaseOrder #cboCurrency').val('');
		document.getElementById('txtExcRate').value='';
		$('#frmPurchaseOrder #cboPayTerm').val('');
		$('#frmPurchaseOrder #cboPayMode').val('');
		$('#frmPurchaseOrder #cboShipmentTerm').val('');
		$('#frmPurchaseOrder #cboShipmentMode').val('');
		$('#frmPurchaseOrder #cboDeliverTo').val('');
		$('#frmPurchaseOrder #txtRemarks').val('');
		$('#frmPurchaseOrder #cboCurrency').removeAttr('disabled',false);
		
		
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmPurchaseOrder #dtDate').val(d);
		$('#frmPurchaseOrder #dtDeliveryDate').val(d);
	});
//-----------------------------------------------------
});

//----------end of ready -------------------------------
function loadMain()
{
	//alert(1);
/*	$("#butAddPendingDispatch").click(function(){
		popupWindow3('1');
		//$('#iframeMain1').attr('src','http://localhost/qpet/presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php?id='+$('#cboDispatchTo').val());
		$('#popupContact1').load('sampleDispatchPopup.php');	
		
	});*/
	//$("#iframeMain1").contents().find("#butAdd").click(abc);
	//$("#frmPurchaseOrderPopup").contents().find("#butAdd").click(abc);
//	$('#frmPurchaseOrderPopup #butClose').click(abc);
}

//----------------------------------------------------
function addClickedRows()
{
	var rowCount = document.getElementById('tblItemsPopup').rows.length;
	var priceEdit = document.getElementById('divPrice').innerHTML;
	
	for(var i=1;i<rowCount;i++)
	{
		if((document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].checked==true) && (document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].disabled==false)){
			var itemId      =document.getElementById('tblItemsPopup').rows[i].cells[3].id;
			var maincatId    =document.getElementById('tblItemsPopup').rows[i].cells[1].id;
			var subCatId     =document.getElementById('tblItemsPopup').rows[i].cells[2].id;
			var code         =document.getElementById('tblItemsPopup').rows[i].cells[3].innerHTML;
			var uom          =document.getElementById('tblItemsPopup').rows[i].cells[4].id;
			var itemName     =document.getElementById('tblItemsPopup').rows[i].cells[4].innerHTML;
			var unitPrice    =parseFloat(document.getElementById('tblItemsPopup').rows[i].cells[5].id);
			unitPrice        = unitPrice
			var mainCatName  =document.getElementById('tblItemsPopup').rows[i].cells[1].innerHTML;
			var subCatName   =document.getElementById('tblItemsPopup').rows[i].cells[2].innerHTML;
			var prn       	 =document.getElementById('tblItemsPopup').rows[i].cells[5].innerHTML;
			
			var prnQty   	 = parseFloat(document.getElementById('tblItemsPopup').rows[i].cells[6].innerHTML);
			var poQty        = parseFloat(document.getElementById('tblItemsPopup').rows[i].cells[6].id);
			var taxGrpCombo  = document.getElementById('divTaxCobo').innerHTML;
            var divCostcenterCombo  = document.getElementById('divCostcenterCombo').innerHTML;
			var ammount      = unitPrice*(prnQty-poQty);
			//var remarks      = selectedPRNstring;
			var remarks      = "";
			if(prnQty>0){
				var POQty    = (prnQty-poQty).toFixed(6);
				var maxPOQty = (prnQty-poQty).toFixed(6);
			}
			else{
				var POQty=0;
				var maxPOQty=0;
			}
			var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF"><img class="delImg" src="images/del.png" width="15" height="15" /></td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+prn+'">'+prn+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'">'+mainCatName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'">'+subCatName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'">'+code+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'">'+itemName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'">'+uom+'</td>';
			if(priceEdit==1){
			content +='<td align="center" bgcolor="#FFFFFF" id="'+unitPrice+'"><input  id="'+(unitPrice)+'" class="validate[required,custom[number]] calculateValue" style="width:80px;text-align:right" type="text" value="'+unitPrice+'" onKeyPress=\"return validateDecimalPlaces(this.value,6,event);\"/></td>';
			}
			else{
			content +='<td align="center" bgcolor="#FFFFFF" id="'+unitPrice+'"><input  id="'+(unitPrice)+'" class="validate[required,custom[number]] calculateValue" style="width:80px;text-align:right" disabled="disabled" type="text" value="'+unitPrice+'" onKeyPress=\"return validateDecimalPlaces(this.value,6,event);\"/></td>';
			}
			content +='<td align="center" bgcolor="#FFFFFF" id=""><input  id="'+itemId+'" class="validate[required,custom[number]]  calculateValue" style="width:80px;text-align:right" type="text" value="0"/></td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" >'+taxGrpCombo+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" ><input type="checkbox" name="chkSvatItem" id="chkSvatItem" /></td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'"><input  id="'+(maxPOQty)+'" class="validate[required,custom[number]] calculateValue balQty calMax" style="width:80px;text-align:right" type="text" value="'+(POQty)+'"/></td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'"><input  id="" class="validate[required,custom[number]] calculateValue moq calMax" style="width:80px;text-align:right" type="text" value="0"/></td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'"><input  id="" class="validate[required,custom[number]] calculateValue rolling calMax" style="width:80px;text-align:right" type="text" value="0"/></td>';
			content +='<td align="right" bgcolor="#FFFFFF" id="'+itemId+'" >'+Math.round(ammount)+'</td>';
		//	if(flagRemarks==1){
			content +='<td align="right" bgcolor="#FFFFFF"><input  id="'+remarks+'" class="remarks" style="width:120px;text-align:left" type="text" value="'+remarks+'"/></td>';

			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" >'+divCostcenterCombo+'</td>';
			/*	}
			else{
			content +='<td align="right" bgcolor="#FFFFFF" style="display:none"><input  id="'+remarks+'" class="remarks" style="width:120px;text-align:left" type="text" value="'+remarks+'"/></td>';
			}*/
			content +='</tr>';
		
			add_new_row('#frmPurchaseOrder #tblPOItems',content);
			//----------------------------	

			//----------------------------	
			$('.clsValidateBalQty').keyup(function(){
				var input=$(this).val();
				var balQty=$(this).closest('td').attr('id');
				if((input>balQty) || (input<0)){
				alert("Invalid Qty");
					$(this).val(balQty);
				}
			});
			//----------------------------
			calfunction();
			}
	}
	
	calTotals();
	disableEnableCurrency();
	
	disablePopup();
	hideWaiting();
}

//----------------------------------------------------
function addClickedPRNRows()
{
	
		var rowCount = document.getElementById('tblPRNsPopup').rows.length;
		var currency=$('#cboCurrency').val();
		var poDate=$('#dtDate').val();
		var data='';
		data+="currency="		+	currency;
		data+="&poDate="		+	poDate;
		data+="&menuId="	+	$('#menuId').html();

		
		var url 		= basePath+"purchaseOrder-db-get.php?requestType=loadGRPPRNItems";
		var arr="[";
		
		for(var i=1;i<rowCount;i++)
		{
			if(document.getElementById('tblPRNsPopup').rows[i].cells[0].childNodes[0].checked==true){
				arr += "{";
				var prnNo = 	document.getElementById('tblPRNsPopup').rows[i].cells[2].innerHTML;
				arr += '"prnNo":"'+		prnNo +'"' ;
				arr +=  '},';
				
				selectedPRNstring +=prnNo+',';
			}
		}
			
			selectedPRNstring = selectedPRNstring.substr(0,selectedPRNstring.length-1);
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;
		
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:data,
			async:false,
			success:function(json){

				var length = json.arrCombo.length;
				var arrCombo = json.arrCombo;

								for(var i=0;i<length;i++)
								{
									var prnNo = arrCombo[i]['prn'];
									var itemId=arrCombo[i]['itemId'];	
									var maincatId=arrCombo[i]['maincatId'];	
									var subCatId=arrCombo[i]['subCatId'];	
									var code=arrCombo[i]['code'];	
									var itemName=arrCombo[i]['itemName'];	
									var uom=arrCombo[i]['uom'];	
									var currency=arrCombo[i]['currency'];	
									var unitPrice=arrCombo[i]['unitPrice'];	
									var mainCatName=arrCombo[i]['mainCatName'];
									var subCatName=arrCombo[i]['subCatName'];	
									var prnQty=arrCombo[i]['prnQty'];
									var poQty=arrCombo[i]['poQty'];
									var balQty=parseFloat(prnQty)-parseFloat(poQty);
									flagRemarks=arrCombo[i]['flagRemarks'];
									var stockBal 		= parseFloat(arrCombo[i]['stockBal']);
									var reOrderLevel 	= parseFloat(arrCombo[i]['ReOrderLevel']);
									var poRaisedStatus=1;

										
									var content='<tr class="normalfnt">';
									
/*
BEGIN - THIS CONDITION COMMENT FOR TEMPORARY  - 2013-06-27
									if((currency=='') || (stockBal>=reOrderLevel && permission_reorder==false && maincatId=='1')){
										content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'"><input type="checkbox" id="chkDisp" disabled="disabled"/></td>';
									}
									else{
										content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'"><input type="checkbox" id="chkDisp" /></td>';
									}
END 	- THIS CONDITION COMMENT FOR TEMPORARY - 2013-06-27
*/
								
									if(currency==''){
										content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'"><input type="checkbox" id="chkDisp" disabled="disabled"/></td>';
									}
									else{
										content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'"><input type="checkbox" id="chkDisp" /></td>';
									}
									
									content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'">'+mainCatName+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'">'+subCatName+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'">'+code+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+uom+'">'+itemName+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+unitPrice+'" style="display:none" >'+prnNo+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+poQty+'" >'+prnQty+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+poQty+'">'+currency+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+stockBal+'">'+stockBal+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+reOrderLevel+'">'+reOrderLevel+'</td>';
									add_new_row('#frmPurchaseOrderPopup #tblItemsPopup',content);
								}
					checkAlreadySelected();
					//updatePOraisedStatus(data,poRaisedStatus);

			}
		});
			hideWaiting();
			//	hideWaiting();


}

function deleteItems(obj)
{
	if ($('#frmPurchaseOrder').validationEngine('validate'))   
    { 
		var poNo		= $('#frmPurchaseOrder #txtPoNo').val();
		var poYear		= $('#frmPurchaseOrder #txtPoYear').val();
		var itemId		= obj.parent().attr('id');
		var url 		= basePath+"purchaseOrder-db-set.php?requestType=deleteItems";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:"itemId="+itemId+"&poNo="+poNo+"&poYear="+poYear,
			async:false,
			success:function(json){	
			
			if(json.type == 'pass')
			{
				obj.validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
				obj.parent().parent().remove();
			}
			else
				obj.validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					
			}
		})
	}
}

function addSelectedPRNs()
{
	addClickedPRNRows();
	hideWaiting();
	hideWaiting();
}

function calfunction()
{
	//$('.calculateValue').keyup(function(){
		$('.calculateValue').each(function(){
		var row	= this.parentNode.parentNode.rowIndex;
		
		if(document.getElementById('tblPOItems').rows[row].cells[this.parentNode.cellIndex].childNodes[0].value=='')
			document.getElementById('tblPOItems').rows[row].cells[this.parentNode.cellIndex].childNodes[0].value=0;

		calTotals();	
		
		var qty 	= parseFloat(document.getElementById('tblPOItems').rows[row].cells[11].childNodes[0].value);
		var price 	= parseFloat(document.getElementById('tblPOItems').rows[row].cells[7].childNodes[0].value)*(100-parseFloat(document.getElementById('tblPOItems').rows[row].cells[8].childNodes[0].value))/100;
		document.getElementById('tblPOItems').rows[row].cells[14].innerHTML	= RoundNumber(qty*price,2).toFixed(2);
	});	
}

function calTotals()
{	
	var rowCount 			= document.getElementById('tblPOItems').rows.length;
	var total				= 0;
	var totalDiscount		= 0;
	var grandTotal			= 0;
	
	for(var i=1;i<rowCount;i++)
	{
			var unitPrice 	= parseFloat(document.getElementById('tblPOItems').rows[i].cells[7].childNodes[0].value);
			var discount 	= parseFloat(document.getElementById('tblPOItems').rows[i].cells[8].childNodes[0].value);
			var Qty 		= parseFloat(document.getElementById('tblPOItems').rows[i].cells[11].childNodes[0].value);
			
			var moq 		= parseFloat(document.getElementById('tblPOItems').rows[i].cells[12].childNodes[0].value);
			var Qtrollingy 		= parseFloat(document.getElementById('tblPOItems').rows[i].cells[13].childNodes[0].value);
			total		   += RoundNumber(Qty*unitPrice,2);
			totalDiscount  += Qty*(discount/100)*unitPrice;			
	 }
	 
	grandTotal		= total-totalDiscount;
	document.getElementById('divTotal').innerHTML				= RoundNumber(total,2).toFixed(2);
	document.getElementById('divTotalDiscount').innerHTML		= RoundNumber(totalDiscount,2).toFixed(2);
	document.getElementById('divGrandTotal').innerHTML			= RoundNumber(grandTotal,2).toFixed(2);
}

function alertx()
{
	$('#frmPurchaseOrder #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmPurchaseOrder #butDelete').validationEngine('hide')	;
}

function closePopUp(){
		hideWaiting();

}
//----------------------------------------------
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
//----------------------------------------------- 
function clearRows()
{
	//var rowCount = $('#tblDispatchPopup >tbody >tr').length;
	var rowCount = document.getElementById('tblPOItems').rows.length;
	//alert(rowCount);
	//var cellCount = document.getElementById('tblMain').rows[1].cells.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblPOItems').deleteRow(1);
			
	}
}
//------------------------------------------------------------
function checkAlreadySelected(){
	var rowCount = document.getElementById('tblPOItems').rows.length;
	for(var i=1;i<rowCount;i++)
	{
			var itemNo = 	document.getElementById('tblPOItems').rows[i].cells[4].id;
			var prnNo = 	document.getElementById('tblPOItems').rows[i].cells[1].innerHTML;
		
			var rowCount1 = document.getElementById('tblItemsPopup').rows.length;
			for(var j=1;j<rowCount1;j++)
			{
				var itemNoP = 	document.getElementById('tblItemsPopup').rows[j].cells[3].id;
				var prnNoP = 	document.getElementById('cboPRN').value;
				if((itemNo==itemNoP) && (prnNo==prnNoP)){
					document.getElementById('tblItemsPopup').rows[j].cells[0].childNodes[0].checked=true;
					document.getElementById('tblItemsPopup').rows[j].cells[0].childNodes[0].disabled=true;
				}
			}
	}
}

function disableEnableCurrency(){
	var rowCount = document.getElementById('tblPOItems').rows.length;
	if(rowCount>1){
		$('#cboCurrency').attr("disabled", "disabled");
		$('#txtExcRate').attr("disabled", "disabled");
	}
	else{
		$('#cboCurrency').removeAttr("disabled");
		$('#txtExcRate').removeAttr("disabled");
	}
}
//--------------------------------------------
function loadDefaultCurrencyAndExchRate(){
	var url 		= basePath+"purchaseOrder-db-get.php?requestType=loadDefaultCurrencyAndExchRate";
	var currency = $('#frmPurchaseOrder #cboCurrency').val();

	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		type:'POST',
		data:"currency="+currency,
		async:false,
		success:function(json){
				if(json.flag==1){
				$('#frmPurchaseOrder #cboCurrency').val(json.currency);
				document.getElementById("txtExcRate").value=json.excRate;
				}
		}
	});
}
//--------------------------------------------
function deleteAllPRNs(){
	//var rowCount = $('#tblDispatchPopup >tbody >tr').length;
	var rowCount = document.getElementById('tblPRNsPopup').rows.length;
	//alert(rowCount);
	//var cellCount = document.getElementById('tblMain').rows[1].cells.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblPRNsPopup').deleteRow(1);
			
	}
}
//---------------------------------------------
function updatePOraisedStatus(data,status){
	
		data +='&status='+status;
		var url 		= basePath+"purchaseOrder-db-set.php?requestType=updatePOraisedStatus";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:data,
			async:false,
			success:function(json){
				
			}
		});
}

function loadExchangeRate(basePath){
	    var currency = $('#cboCurrency').val();
	    var date = $('#dtDate').val();
		var url 		= basePath+"purchaseOrder-db-get.php?requestType=loadExchangeRate";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:"currency="+currency+"&date="+date,
			async:false,
			success:function(json){
					document.getElementById("txtExcRate").value=json.excRate;
			}
		});
}

function loadExchangeRateDetails()
{
	    var currency = $('#cboCurrency').val();
	    var date = $('#dtDate').val();
		var url 		= basePath+"purchaseOrder-db-get.php?requestType=loadExchangeRate";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			type:'POST',
			data:"currency="+currency+"&date="+date,
			async:false,
			success:function(json){
					document.getElementById("txtExcRate").value=json.excRate;
					flagAllowDefaultExcRate=json.flagAllowDefaultExcRate
			}
		});
		
 		//if(($('#cboCurrency').val()=='') || ($("#cboCurrency option:selected").text()=='LKR')){
			if(flagAllowDefaultExcRate==1)
			loadDefaultCurrencyAndExchRate($('#cboCurrency').val());
		//}
	
}
function calculateMaxPO(obj){

	if($(obj).attr('class')=='validate[required,custom[number]] calculateValue balQty calMax'){
		$(obj).parent().parent().find('.moq').val(0);
		$(obj).parent().parent().find('.rolling').val(0);
	}
	
	if(bal=='' || bal ==0 || isNaN(bal))//page loading
	bal			=	parseFloat($(obj).parent().parent().find('.balQty').val())/*-parseFloat($(obj).parent().parent().find('.rolling').val())*/;
	var qty		=	parseFloat($(obj).parent().parent().find('.balQty').attr('id'));

	var moq		=	parseFloat($(obj).parent().parent().find('.moq').val());
	var rolling	=	parseFloat($(obj).parent().parent().find('.rolling').val());

	if(qty=='')
	qty	=0;

	if(isNaN(moq))
	 moq=0;
	if(isNaN(rolling))
	 rolling=0;
	if(isNaN(bal))
	 bal=0;
	
	//alert(qty+'/'+bal+'/'+moq+'/'+rolling);
	var req	= 0;
	
	//alert(bal+"/"+qty);
	var bal_s = $(obj).parent().parent().find('.balQty').val().toString();;
	//alert(bal_s.substr(bal_s.length - 1));
	//alert(bal_s.substr(bal_s.length - 1));
	if(bal_s.substr(bal_s.length - 1)=='.'){
		//do nothing
		//alert('ok');
	}
	else{
	
	if(parseFloat(bal) < parseFloat(qty) && parseFloat(qty) >0){
		if(bal==0)
		bal=qty;
		
 		req	= parseFloat(bal)+parseFloat(rolling);
		//alert(req);
	}
	else{
		//alert($(obj).parent().parent().find('.balQty').val());
		
		if(qty=='' && $(obj).parent().parent().find('.balQty').val()==0){
		qty	=0;
		}
		//else if(qty==0)
		//qty	=$(obj).parent().parent().find('.balQty').val();
		
		//alert(qty+'/'+rolling);
		//alert(qty+'/'+rolling);
		req	= parseFloat(qty)+parseFloat(rolling);
	}
		
		//alert(rolling);
	
	//alert(qty+'/'+rolling+'/'+moq);
	//alert(qty+'/'+rolling+'/'+moq);
	//alert(req+' >= '+moq);
	if(req==0 && moq==0)
	$(obj).parent().parent().find('.balQty').val(bal);
	else if(req >= moq)
	$(obj).parent().parent().find('.balQty').val(req);
	else
	$(obj).parent().parent().find('.balQty').val(moq);
	
	//alert(rolling+'/'+moq+'/'+$(obj).parent().parent().find('.balQty').val());
	if((rolling==0 || rolling=='')  && (moq==0 || moq=='') && (parseFloat($(obj).parent().parent().find('.balQty').val())==0 || $(obj).parent().parent().find('.balQty').val()==''))
	$(obj).parent().parent().find('.balQty').val(parseFloat($(obj).parent().parent().find('.balQty').attr('id')));
	
	
	calfunction();
	}
}
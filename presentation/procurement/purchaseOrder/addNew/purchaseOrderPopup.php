<?php
session_start();
$backwardseperator = "../../../../";
$mainPath = $_SESSION['mainPath'];
$location 	= $_SESSION['CompanyID'];
$company 	= $_SESSION['headCompanyId'];
$thisFilePath =  $_SERVER['PHP_SELF'];

//$formName		  	= 'frmItem';
//include  	"../../../customerAndOperation/sample/sampleDispatch/addNew/{$backwardseperator}dataAccess/permisionCheck.inc";
include $backwardseperator."dataAccess/Connector.php";
$delivertTo  = $_REQUEST['delivertTo'];

$q	=$_GET['q'];
if($q==220){
	$po_type	= '1';
	$maincat	= '1,3';
}
else if($q==1151){
	$po_type	= '2';
	$maincat	= '2,3,4,5,7';
}
else if($q==1153){//All
	$po_type	= '3';
	$maincat	= '1,2,3,4,5,7';
}

?>
<head>
<title>Items</title>
</head>

<body>
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>

    
<form id="frmPurchaseOrderPopup" name="frmPurchaseOrderPopup" method="post" >
<table width="450" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutL" style="width:650">
		  <div class="trans_text"> Item List</div>
		  <table width="660" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableBorder">
<tr>
            <td width="20%" height="22" class="normalfnt">&nbsp;</td>
            <td><input type="checkbox" name="chkPRN" id="chkPRN" />&nbsp;<span class="normalfnt">PRN wise</span>
              </td>
            <td width="28%"><select name="cboPRN" style="width:120px" id="cboPRN" disabled="disabled">
                  <option value=""></option>
                  <?php
					$sql = "
select tb1.intPrnNo,tb1.intYear  from (
SELECT DISTINCT 
							trn_prnheader.intPrnNo,
							trn_prnheader.intYear,
							sum(trn_prndetails.dblPrnQty) as prn_qty,
							(select sum(trn_podetails.dblQty) from trn_podetails where trn_podetails.intPrnNo =trn_prndetails.intPrnNo 
							and trn_podetails.intPrnYear =trn_prndetails.intYear 
							and trn_podetails.intItem =trn_prndetails.intItem) as po_qty 
							FROM trn_prnheader 
							Inner Join trn_prndetails ON trn_prnheader.intPrnNo = trn_prndetails.intPrnNo AND trn_prnheader.intYear = trn_prndetails.intYear
							Inner Join mst_locations ON trn_prnheader.intCompany = mst_locations.intId
							WHERE   
							date(trn_prnheader.dtmPrnDate) >= ADDDATE(date(now()),INTERVAL -3 month) 
							and 1=1 AND trn_prnheader.PO_TYPE = '$po_type' 
							AND mst_locations.intCompanyId =  '$company' and trn_prnheader.intStatus='1' /*AND (trn_prndetails.dblPrnQty-trn_prndetails.dblPoQty) >  '0' AND IFNULL(intPORaised,0) != '1'
					having (dblPrnQty-po_qty)  > 0  */
							group by 
							trn_prndetails.intPrnNo, trn_prndetails.intYear,trn_prndetails.intItem 
							having (prn_qty-ifnull(po_qty,0)) > 0
) as tb1
					 
";

					//echo $sql;
							$pp=$sql;
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intPrnNo']."/".$row['intYear']."\">".$row['intPrnNo']."/".$row['intYear']."</option>";	
					}
				?>
            </select></td>
            <td class="normalfnt"><input type="checkbox" name="chkGrpPRN" id="chkGrpPRN" />&nbsp;<span class="normalfnt">Group PRN wise</span></td>
              <td class="normalfnt"><img src="images/aad.png" id="butAddPrnGroup" width="19" height="19" alt="add prn group" style="display:none" /></td>
            </tr>
            <tr>
            <td width="20%" height="22" class="normalfnt">&nbsp;</td>
            <td width="18%" class="normalfnt">Main Category</td>
            <td width="28%"><select name="cboMainCategory" style="width:120px" id="cboMainCategory">
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_maincategory.intId,
							mst_maincategory.strName
							FROM mst_maincategory 
							WHERE FIND_IN_SET(mst_maincategory.intId,'$maincat')";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
            </select></td>
            <td width="24%" class="normalfnt">&nbsp;</td>
            <td width="10%" align="right">&nbsp;</td>
            </tr>
          <tr>
            <td width="20%" height="22" class="normalfnt">&nbsp;</td>
            <td class="normalfnt">Sub Category</td>
            <td width="28%"><select name="cboSubCategory" style="width:120px" id="cboSubCategory">
                  <option value=""></option>
            </select></td>
            <td width="24%" class="normalfnt">&nbsp;</td>
            <td width="10%" align="right">&nbsp;</td>
            </tr>
          <tr>
            <td height="22" class="normalfnt">&nbsp;</td>
            <td class="normalfnt">Code Like</td>
            <td><input type="text" name="txtItmCode" id="txtItmCode"  style="width:120px" /></td>
            <td class="normalfnt">&nbsp;</td>
            <td align="right">&nbsp;</td>
          </tr>
          <tr>
            <td width="20%" height="22" class="normalfnt">&nbsp;</td>
            <td class="normalfnt">Description Like</td>
            <td width="28%">
              <input type="text" name="txtItmDesc" id="txtItmDesc"  style="width:120px" /></td>
            <td width="24%" class="normalfnt"><img src="images/smallSearch.png" width="24" height="24" class="mouseover" id="imgSearchItems" /></td>
            <td width="10%" align="right">&nbsp;</td>
            </tr>          </table></td>
      </tr>
      <?php //echo $pp
	  ?>
      
      <tr>
        <td><div style="height:300px;overflow:scroll" >
          <table width="640" class="bordered" id="tblItemsPopup" >
          <thead>
            <tr>
              <th width="2%" height="22" ><input type="checkbox" name="chkAll" id="chkAll" class="chkAll" /></th>
              <th width="11%" >Main Catogery</th>
              <th width="11%" >Sub Catogery</th>
              <th width="9%" >Item Code</th>
              <th width="20%" >Description</th>
              <th width="9%" style="display:none" >PRN</th>
              <th width="8%" >PRN Qty</th>
              <th width="12%" >Currency</th>
              <th width="9%" >Stock Bal</th>
              <th width="9%" >Re-Order Level</th>
              </tr>
            </thead>
            </table>
          </div></td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound"><img src="images/Tadd.jpg" width="92" height="24"  alt="add" id="butAdd" name="butAdd" class="mouseover"/><img src="images/Tclose.jpg" width="92" height="24" id="butClose1" name="butClose1" class="mouseover"  /></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>

</body>
</html>

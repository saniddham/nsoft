<?php 
ini_set('display_errors',0);
//error_reporting(E_ALL);
//ini_set('display_errors','On');

/*date_default_timezone_set('America/Los_Angeles');

$script_tz = date_default_timezone_get();

if (strcmp($script_tz, ini_get('date.timezone'))){
    echo 'Script timezone differs from ini-set timezone.';
} else {
    echo 'Script timezone and ini-set timezone match.';
}
*/
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$companyId 	= $_SESSION['CompanyID'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	include_once "{$backwardseperator}dataAccess/Connector.php";
 	require_once "../../../../class/cls_commonFunctions_get.php";
	require_once "../../../../class/finance/budget/cls_common_function_budget_get.php";
	require_once "../../../../class/finance/budget/entering/cls_budget_get.php";
	require_once "../../../../class/finance/budget/entering/cls_budget_set.php";
	require_once "../../../../class/procurement/purchaseOrder/cls_purchase_order_get.php";
	
	//require_once "../../../../class/warehouse/returnToStores/cls_returnToStores_get.php";
	
	//$obj_returnToStores_get = new cls_returnToStores_get($db);
	$obj_common				= new cls_commonFunctions_get($db);
	$obj_comm_budget_get	= new cls_common_function_budget_get($db);
	$obj_budget_get			= new cls_budget_get($db);
	$obj_budget_set			= new cls_budget_set($db);
	$obj_po_get				= new Cls_purchase_order_Get($db);
	
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	
	$serialNo 		= $_REQUEST['serialNo'];
	$year 			= $_REQUEST['Year'];
	$supplier 		= $_REQUEST['supplier'];
	$currency 		= $_REQUEST['currency'];
	$payTerms 		= $_REQUEST['payTerms'];
	$payMode 		= $_REQUEST['payMode'];
	$shipmentTerm 	= $_REQUEST['shipmentTerm'];
	$shipmentMode 	= $_REQUEST['shipmentMode'];
	$deliveryTo 	= $_REQUEST['deliveryTo'];
	$deliveryDate 	= $_REQUEST['deliveryDate'];
	$remarks 		= $_REQUEST['remarks'];
	$date       	= $_REQUEST['date'];
	$menuId 		= $_REQUEST['menuId'];




	$arr 		= json_decode($_REQUEST['arr'], true);

	if($menuId==220){
		$title		= "Raw Materials";
		$programName= 'Purchase Order';
		$programCode='P0220';
		$poType		= 1;
	}
	else if($menuId==1151){
		$title		= "None Raw Materials";
		$programName= 'Purchase Order - None Raw Materials';
		$programCode='P1151';
		$poType		= 2;
	}
	else if($menuId==1153){
		$title		= "All";
		$programName= 'Purchase Order - All';
		$programCode='P1153';
		$poType		= 3;
	}

	$ApproveLevels = (int)getApproveLevel($programName);
	$poApproveLevel = $ApproveLevels+1;

	//$programName='Purchase Order';
	//$programCode='P0220';

//------------save---------------------------	
	if($requestType=='save')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		$savableFlag= 1;
		$rollBackFlag=0;
		
		$swatSql	= " SELECT
							mst_financetaxisolated.dblRate
							FROM
							mst_financetaxisolated
							WHERE
							mst_financetaxisolated.intId = 3";
		$result_swat = $db->RunQuery2($swatSql);		
		$row_swat    = mysqli_fetch_array($result_swat);
		$swatRate	 = $row_swat['dblRate'];
	
		if($serialNo==''){
			$serialNo 	= getNextPoNo();
			$year = date('Y');
			$editMode=0;
			$savedStatus=$poApproveLevel;
			$savedLevels=$ApproveLevels;
		}
		else{
			$editMode=1;
			$savableFlag=getSaveStatus($serialNo,$year);//check wether already confirmed
			$sql = "SELECT
			trn_poheader.intStatus, 
			trn_poheader.intApproveLevels 
			FROM trn_poheader 
			WHERE
			trn_poheader.intPONo =  '$serialNo' AND
			trn_poheader.intPOYear =  '$year'";

			$result = $db->RunQuery2($sql);
			$row=mysqli_fetch_array($result);
			$savedStatus=$row['intStatus'];
			$savedLevels=$row['intApproveLevels'];
		}
		
		$editPermission=loadEditMode($programCode,$savedStatus,$savedLevels,$userId);
		
		//--------------------------
		$locationEditFlag=getEditLocationValidation('Supplier',$location,$serialNo,$year);
		//--------------------------
		 $sql_s = "SELECT
				mst_supplier.strEmail,
				mst_supplier.strName as supplierName   
				FROM
				mst_supplier
				WHERE
				mst_supplier.intId =  '$supplier' ";
		$result_s = $db->RunQuery2($sql_s);
		$row_s=mysqli_fetch_array($result_s);
		//-----------delete and insert to header table-----------------------
		 if($row_s['strEmail']==''){
			 $rollBackFlag=1;
			 $rollBackMsg="There is no Email Address for supplier '".$row_s['supplierName']."'";
		 }
		 else if(( $locationEditFlag==1) && ($editMode==1)){
			 $rollBackFlag=1;
			 $rollBackMsg="Invalid Edit Location";
		 }
		 else if($savableFlag==0){
			 $rollBackFlag=1;
			 $rollBackMsg="This PO No is already confirmed.cant edit.";
		 }
		 else if($editPermission==0){
			 $rollBackFlag=1;
			 $rollBackMsg="No Permission to save.";
		 }
		 else if($editMode==1){
			$sql_grn	= " SELECT
							ware_grnheader.intGrnNo,
							ware_grnheader.intGrnYear
							FROM
							ware_grnheader
							WHERE
							ware_grnheader.intPoNo = '$serialNo' AND
							ware_grnheader.intPoYear = '$year'";
			$result_grn	= $db->RunQuery2($sql_grn);
			$grnRaised	= (mysqli_num_rows($result_grn)>0?1:0);
			$sql_crncy		= "SELECT intCurrency FROM trn_poheader WHERE (`intPONo`='$serialNo') AND (`intPOYear`='$year')";
			$result_crncy	= $db->RunQuery2($sql_crncy);
			$row_crncy		= mysqli_fetch_array($result_crncy);
			$currency_old	= $row_crncy['intCurrency'];
			if($grnRaised && $currency != $currency_old)
			{
				$rollBackFlag=1;
				$rollBackMsg="GRN Already Raised for this PO.Can not change currency";
			}
			else
			{
				$sql = "UPDATE `trn_poheader` SET intStatus ='$poApproveLevel', 
															  dtmPODate ='$date', 
															  intApproveLevels ='$ApproveLevels', 
															  intModifiedBy ='$userId', 
															  dtmModifiedDate =now(), 
															  strRemarks ='$remarks', 
															  intSupplier ='$supplier', 
															  intCurrency ='$currency', 
															  intPaymentTerm ='$payTerms', 
															  intPaymentMode ='$payMode', 
															  dtmDeliveryDate ='$deliveryDate', 
															  intShipmentTerm ='$shipmentTerm', 
															  intShipmentMode ='$shipmentMode',
															  intDeliveryTo='$deliveryTo',
															  SVAT_RATE = '$swatRate'
															  
						WHERE (`intPONo`='$serialNo') AND (`intPOYear`='$year')";
				$result = $db->RunQuery2($sql);
			}
			//inactive previous recordrs in approvedby table
			$maxAppByStatus=(int)getMaxAppByStatus($serialNo,$year)+1;
			$sql = "UPDATE `trn_poheader_approvedby` SET intStatus ='$maxAppByStatus' 
					WHERE (`intPONo`='$serialNo') AND (`intYear`='$year') AND (`intStatus`='0')";
			$result1 = $db->RunQuery2($sql);
							
			
		}
		else{
			//commented on 22/05/2013
/*			$sql = "DELETE FROM `trn_poheader` WHERE 
					(`intPONo`='$serialNo') AND (`intPOYear`='$year')";
			$result1 = $db->RunQuery2($sql);
*/			
			$sql = "INSERT INTO `trn_poheader` (`intPONo`,`intPOYear`,`intSupplier`,intCurrency,intPaymentTerm,intPaymentMode,intShipmentTerm,intShipmentMode,intDeliveryTo,dtmDeliveryDate,dtmPODate,strRemarks,intStatus,intApproveLevels,dtmCreateDate,intUser,intCompany, SVAT_RATE,PO_TYPE) 
					VALUES ('$serialNo','$year','$supplier','$currency','$payTerms','$payMode','$shipmentTerm','$shipmentMode','$deliveryTo','$deliveryDate','$date','$remarks','$poApproveLevel','$ApproveLevels',now(),'$userId','$location','$swatRate','$poType')";
			$result = $db->RunQuery2($sql);
		}
		//-----------delete and insert to detail table-----------------------
		if(($result) && ($rollBackFlag!=1)){
		//$sql = "DELETE FROM `trn_podetails` WHERE (`intPONo`='$serialNo') AND (`intPOYear`='$year')";
		//$result2 = $db->RunQuery2($sql);
		
			$toSave=0;
			$saved=0;
			$rollBackFlag=0;
			$rollBackFlag1=0;
			$rollBackFlag2=0;
			$rollBackMsg ="Maximum PO Qtys for items: "; 
			$rollBackMsg2 ="PO Qty should greater than 0 for ..."; 
			$rollBackMsg3= "Minimum PO Qtys for items: ";

			foreach($arr as $arrVal)
			{
				$prnNo 			= $arrVal['prnNo'];
				$prnNoArray=explode("/",$prnNo);
				$prnNo=$prnNoArray[0];
				$prnNoYear=$prnNoArray[1];
				if($prnNo=='')
					$prnNo=0;
				
				if($prnNoYear=='')
					$prnNoYear=0;
				$remarks			= $arrVal['remarks'];
				$itemId 			= $arrVal['itemId'];
				$unitPrice 		= round($arrVal['unitPrice'],6);
				$svatItem 		= $arrVal['svatItem'];
				$discount 		= $arrVal['discount'];
				$taxCode 			= $arrVal['taxCode'];
				if($taxCode==''){
					$taxCode=0;
				}
				$Qty 		= round($arrVal['Qty'],4);
				$moq 		= $arrVal['MOQ'];
				$rolling	= $arrVal['rolling'];

									$Costcenter 			= $arrVal['costcenter'];

                if($Costcenter == ''){
                    $Costcenter = "NULL";
				}


//                if ($interval['to_h'] == ''|| $interval['to_m'] == '') {
//                    $todate = "NULL";
//
//                }else {
//                    $to = $interval['to_h'].':'.$interval['to_m'];
//                    $to = "'$to'";
//                    $todate = str_replace('"', '', $to );
//                }

				//////
				/*if($Qty<=0){
					$rollBackFlag1=1;
					$rollBackMsg2 .="\n ".$itemName;
				}
				*/
				//if po is raised for a prn
				if($prnNo!=0){
				//------check maximum PO Qty--------------------
			 	 $sqlc = "SELECT
						sum(trn_prndetails.dblPrnQty) as dblPrnQty,
						ifnull((
						SELECT
						Sum(pod.dblQty)
						FROM
						trn_podetails AS pod
						INNER JOIN trn_poheader AS po ON pod.intPONo = po.intPONo AND pod.intPOYear = po.intPOYear
						INNER JOIN trn_prndetails AS prn ON pod.intPrnNo = prn.intPrnNo AND pod.intPrnYear = prn.intYear   and prn.intItem= pod.intItem
						WHERE
						pod.intPrnNo = '$prnNoArray[0]' AND
						pod.intPrnYear = '$prnNoArray[1]' AND
						pod.intItem = trn_prndetails.intItem AND
						po.intStatus = 1),0) as dblPoQty,
						mst_item.strName
						FROM
						trn_prndetails
						Inner Join mst_item ON trn_prndetails.intItem = mst_item.intId
						WHERE
						trn_prndetails.intPrnNo =  '$prnNo' AND
						trn_prndetails.intYear =  '$prnNoYear' AND
						trn_prndetails.intItem =  '$itemId' 
						group by 
						trn_prndetails.intPrnNo,
						trn_prndetails.intYear,
						trn_prndetails.intItem ";
				$resultsc   = $db->RunQuery2($sqlc);
				$rowc       = mysqli_fetch_array($resultsc);
				$balQtyToPO = $obj_common->ceil_to_decimal_places($rowc['dblPrnQty'],4)- round($rowc['dblPoQty'],4);
				if($Qty > max(($balQtyToPO+$rolling),$moq)){
				//	call roll back--------****************
					$rollBackFlag=1;
					$rollBackMsg .="\n ".$prnNo."/".$prnNoYear."-".$rowc['strName']." =".max($balQtyToPO,$moq,$rolling);
					//exit();
				}
				}
				//end --if po is raised for a prn
				
				//check for maximum po qty
				
				$sql_qty	= " SELECT 
							(SELECT
							IFNULL(sum(ware_grndetails.dblGrnQty-ware_grndetails.dblRetunSupplierQty),0) 
							FROM
							ware_grnheader
							INNER JOIN ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear
							WHERE
							ware_grnheader.intPoNo = trn_podetails.intPONo AND
							ware_grnheader.intPoYear = trn_podetails.intPOYear  
							AND ware_grnheader.intStatus = 1 AND 
							ware_grndetails.intItemId =  trn_podetails.intItem
							
							) AS actualGrnQty
							FROM
							trn_poheader
							INNER JOIN trn_podetails ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear
							WHERE
									trn_podetails.intPONo =  '$serialNo' AND
									trn_podetails.intPOYear =  '$year' AND
									trn_podetails.intItem =  '$itemId'
									";
				$result_qty	= $db->RunQuery2($sql_qty);
				$row_qty	= mysqli_fetch_array($result_qty);
				$actualGrnQty	= $row_qty['actualGrnQty'];
												
				if($Qty<$actualGrnQty){
				//	call roll back--------****************
				
					$sql_item	= " SELECT
									mst_item.strName
									FROM
									mst_item
									WHERE
									mst_item.intId = '$itemId'";
					$result_item= $db->RunQuery2($sql_item);
					$row_item	= mysqli_fetch_array($result_item);
					$itemName	= $row_item['strName'];
												
					$rollBackFlag2=1;
					//$rollBackMsg = '';
					$rollBackMsg3 .="\n ".$itemName.'='.$actualGrnQty."\n";
				}
				//----------------------------
				if($rollBackFlag!=1){
					$ammount=$Qty*$unitPrice;
					$taxAmmount=calTaxAmmount($taxCode,$ammount,$discount);
					$taxAmmount=round($taxAmmount,4);
					if($taxAmmount==''){
						$taxAmmount=0;
					}
					///////////////////////////////////////////////
					$sqlItm	= " SELECT
									trn_podetails.intItem,
									trn_podetails.intPONo
									FROM
									trn_podetails
									WHERE
									trn_podetails.intPONo = '$serialNo' AND
									trn_podetails.intPOYear = '$year' AND 
									trn_podetails.intItem = '$itemId' ";
					if($prnNo>0) 
						$sqlItm	.= "AND intPrnNo = '$prnNo' AND intPrnYear = '$prnNoYear' ";
									
						$result_itm = $db->RunQuery2($sqlItm);
						$rows		= mysqli_num_rows($result_itm);
						//echo($rows);
					if($editMode != 1 || $rows==0)
					{
						$sql = "INSERT INTO `trn_podetails` (`intPONo`,`intPOYear`,`intPrnNo`,`intPrnYear`,`intItem`,`dblUnitPrice`,`dblDiscount`,`intTaxCode`,`dblQty`,`dblGRNQty`,dblTaxAmmount,strRemarks,SVAT_ITEM,ROLLING_QTY,MO_QTY,`CostCenter`) 
						VALUES ('$serialNo','$year','$prnNo','$prnNoYear','$itemId','$unitPrice','$discount','$taxCode','$Qty','0','$taxAmmount','$remarks','$svatItem','$rolling','$moq',$Costcenter)";
						$result3 = $db->RunQuery2($sql);
					}
					else
					{
						
						$sql	= " UPDATE trn_podetails 
									SET
									dblQty = '$Qty' ,
									ROLLING_QTY = '$rolling' ,
									MO_QTY = '$moq' ,
									
									dblUnitPrice = '$unitPrice',
									dblDiscount = '$discount',
									intTaxCode = '$taxCode',
									dblTaxAmmount = '$taxAmmount',
									strRemarks = '$remarks',
									SVAT_ITEM = '$svatItem',
									CostCenter =$Costcenter
									
									WHERE
									intPONo = '$serialNo' AND intPOYear = '$year' AND intItem = '$itemId' and 
									intPrnNo = '$prnNo' and 
									intPrnYear = '$prnNoYear' 
									 ";	
						$result3 = $db->RunQuery2($sql);
					$c .=$sql;
					}
					//////////////////////////////////////////////////
					if($result3==1){
					$saved++;
					if($itemId=='806')
						$artRoomItemFound = true;
					}
				}
				$toSave++;
			}
		}
		//die('ok');
		//--2014-05-12-------Budget Validation-------------------
		$resp_save_array					= NULL;
		$arr_item_u							= NULL;
		$arr_item_a							= NULL;
		$arr_sub_cat_u						= NULL;
		$arr_sub_cat_a						= NULL; 
		
		$resp_save_array['arr_item_u']		= $arr_item_u;
		$resp_save_array['arr_item_a']		= $arr_item_a;
		$resp_save_array['arr_sub_cat_u']	= $arr_sub_cat_u;
		$resp_save_array['arr_sub_cat_a']	= $arr_sub_cat_a;
		
		$results_details	=$obj_po_get->get_details_service_po_results($serialNo,$year,'RunQuery2');
		
		$resp_save_array	= $obj_comm_budget_get->load_save_to_array($results_details,$location,$resp_save_array,'SERVICE_PO',$serialNo,$year,'RunQuery2');
		$results_fin_year	= $obj_comm_budget_get->loadFinanceYear_with_selected_date($company,$date,'RunQuery2');
		$row_fin_year		= mysqli_fetch_array($results_fin_year);
		$budg_year			= $row_fin_year['intId'];
		$fields 			= explode('-', $date);
		$budg_month 		= $fields['1'];
		$resp_budg_validate	= $obj_comm_budget_get->get_validate_budget($location,'',$budg_year,$budg_month,$resp_save_array,'SERVICE_PO',$obj_common,'RunQuery2');
		//print_r($resp_budg_validate);
		
		if($resp_budg_validate['type'] == 'fail'){
			 $rollBackFlag					=1;
			 $rollBackMsg					= $resp_budg_validate['msg'];
			 $sqlM							= $resp_budg_validate['sql'];
		}
		//--End of 2014-05-12-------Budget Validation-------------------
		//$rollBackFlag = 1;
		
		if($rollBackFlag1==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg2;
			$response['q'] 			= '';
		}
		if($rollBackFlag2==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg3;
			$response['q'] 			= '';
		}
		else if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= '';
		}
		else if($toSave!=$saved or $saved==0 ){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= 'Details saving error'.$toSave.'!='.$saved;
			$response['q'] 			= '';
			$response['c'] 			= $c;
		}
		else if(($result) && ($toSave==$saved)){
			$confirmationMode=getConfirmationMode($programCode,$userId,$ApproveLevels,$poApproveLevel);
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			if($editMode==1)
			$response['msg'] 		= 'Updated successfully.';
			else
			$response['msg'] 		= 'Saved successfully.';
			
			$response['serialNo'] 		= $serialNo;
			$response['year'] 		= $year;
			$response['confirmationMode'] = $confirmationMode;
			//if($editMode==0 && $artRoomItemFound)
			//{
				//$url ="http://localhost/qpet/presentation/procurement/purchaseOrder/addNew/emailPOReport.php?poNo=$serialNo&year=$year&locationId=$location&userId=$userId&mainPath=".urlencode($_SESSION['mainPath'])."&enterUserEmail=".urlencode($_SESSION['email'])."&enterUserName=".urlencode($_SESSION['systemUserName']);			
				//require_once( $url ="emailPOReport.php?poNo=$serialNo&year=$year&locationId=$location&userId=$userId&mainPath=".urlencode($_SESSION['mainPath'])."&enterUserEmail=".urlencode($_SESSION['email'])."&enterUserName=".urlencode($_SESSION['systemUserName']));	
				if($artRoomItemFound)
				{
					if($editMode==0)
						$emailHeader 	= " NEW PURCHASE ORDER ";
					else
						$emailHeader 	= " UPDATE PURCHASE ORDER ";
						
					$enterUserEmail = $_SESSION['email'];
					$enterUserName  = $_SESSION['systemUserName'];
					$mainPath 		= $_SESSION['mainPath'];
					$poNo 			= $serialNo;
					$companyId 		= $_SESSION['CompanyID'];
					$locationId 	= $location;
					
					#This line comment on 2014-11-21 because server error found when include this file.
					#require_once('emailPOReport.php');
				}
				//$email_status = file_get_contents($url);
				//include $url;	
				//$url = "emailPOReport.php?poNo=$serialNo&year=$year&locationId=$location&userId=$userId&mainPath=".urlencode($_SESSION['mainPath'])."&enterUserEmail=".urlencode($_SESSION['email'])."&enterUserName=".urlencode($_SESSION['systemUserName']);			
				
				//echo $email_status;
			//}
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		
		$db->CloseConnection();		
	}
	else if($requestType=='updatePOraisedStatus'){
	
		$db->OpenConnection();
		$status=$_REQUEST['status'];		
		$arr 		= json_decode($_REQUEST['arr'], true);
		$prnNos ='';
		foreach($arr as $arrVal)
		{
			$prnNo 			= $arrVal['prnNo'];
			$prnNoArray=explode("/",$prnNo);
			$prnNo=$prnNoArray[0];
			$prnNoYear=$prnNoArray[1];
			$sql = "UPDATE `trn_prnheader` SET intPORaised ='$status' 
					WHERE (`intPrnNo`='$prnNo') AND (`intYear`='$prnNoYear')";
			$result1 = $db->RunQuery2($sql);
		}
		$db->RunQuery2('Commit');
		$db->CloseConnection();		
		
		$response['type'] 		= 'pass';
		$response['msg'] 		= 'updated sucessfully';
		$response['q'] 			= $sql;
	}
	else if($requestType == 'deleteItems')
	{
		$poNo	= $_REQUEST['poNo']	;
		$poYear = $_REQUEST['poYear'];
		$itemId	= $_REQUEST['itemId'];
		
		$sql_S = "SELECT
				trn_poheader.intStatus, 
				trn_poheader.intApproveLevels 
				FROM trn_poheader 
				WHERE
				trn_poheader.intPONo =  '$poNo' AND
				trn_poheader.intPOYear =  '$poYear'";
		
		$result_S 	= $db->RunQuery($sql_S);
		$row_S		=mysqli_fetch_array($result_S);
		$savedStatus=$row_S['intStatus'];
		$apLevels	=$row_S['intApproveLevels'];
		
		$sql	= " SELECT
					ware_grnheader.intGrnNo
					FROM
					ware_grnheader
					INNER JOIN ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear
					WHERE
					ware_grnheader.intStatus = '1' AND
					ware_grnheader.intPoNo = '$poNo' AND
					ware_grnheader.intPoYear = '$poYear' AND
					ware_grndetails.intItemId = '$itemId'

					"	;
		$sql_result	= $db->RunQuery($sql);
		$rows		= mysqli_num_rows($sql_result);
		if($rows>0)
		{
			$response['type'] 		= 'fail';
			$response['msg'] 		= 'Can not delete item.Already raised GRN for this po';
			$response['q'] 			= $sql;	
		}
		else if(($apLevels+1) != $savedStatus && $savedStatus!=0){
			$response['type'] 		= 'fail';
			$response['msg'] 		= 'Can not delete item.PO is not pending';
			$response['q'] 			= $sql;	
		}
		else
		{
			$sql_del	=  "DELETE FROM trn_podetails 
							WHERE
							intPONo = '$poNo' AND intPOYear = '$poYear' AND intItem = '$itemId'";//echo $sq
			$sql_result	= $db->RunQuery($sql_del);
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Deleted sucessfully';
			$response['q'] 			= $sql;	
		}
	}
//----------------------------------------
	echo json_encode($response);
//-----------------------------------------	
	function getNextPoNo()
	{
		global $db;
		global $location;
		$sql = "SELECT
				sys_no.intPONo
				FROM sys_no
				WHERE
				sys_no.intCompanyId =  '$location'
				";	
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$nextPONo = $row['intPONo'];
		
		$sql = "UPDATE `sys_no` SET intPONo=intPONo+1 WHERE (`intCompanyId`='$location')  ";
		$db->RunQuery2($sql);
		
		return $nextPONo;
	}
//-----------------------------------------	
	function calTaxAmmount($taxGrpId,$amount,$discount)
    {
        $amount = $amount * (100 - $discount) / 100;
        global $db;
        global $location;
        //(1)Get the tax string which is selected by the user at the time PO create
        $sql = "SELECT
				mst_financetaxgroup.intId,
				mst_financetaxgroup.strProcess
				FROM
				mst_financetaxgroup
				WHERE
				mst_financetaxgroup.intId =  '$taxGrpId'
				";
        $result = $db->RunQuery2($sql);
        $row = mysqli_fetch_array($result);
        $taxId = $row['strProcess'];

        //(2)separate the strProcess (this array contains tax ids and calculation method)
        $arrTax = explode("/", $taxId);
        $operation = '';

        //(3)capture calculation method(isolated/inclusive/exclusive) and the array 'jsonTaxCode' contains only tax ids
        $jsonTaxCode = "[ ";
        if (count($arrTax) == 1)//If strProcess contain only one value (Example $row['strProcess'] = 5)
        {
            $operation = 'Isolated';
            $jsonTaxCode .= '{ "taxId":"' . $taxId . '"}';
        } else if (count($arrTax) > 1) //IF there are multiple tax ids in the tax group(example strProcess = {4/Inclusive/7})
        {
            $operation = $arrTax[1];//this should be inclusive/exclusive
            for ($i = 0; $i < count($arrTax); $i = $i + 2) {
                $jsonTaxCode .= '{ "taxId":"' . $arrTax[$i] . '"},'; //create a json array geting 0 and 2 value from the array
            }

            $jsonTaxCode = $jsonTaxCode . substr(0, count($jsonTaxCode) - 1);
        }
        $jsonTaxCode .= " ]";
        $taxCodes = json_decode($jsonTaxCode, true);

        //(4)get tax rates for all tax ids in the tax group ( tax array)
        if (count($taxCodes) != 0) {
            foreach ($taxCodes as $taxCode) {
                //get tax rates from the mst_financetaxisolated table sending taxId to the callTaxValue function which got by json array
                $codeValues[] = callTaxValue($taxCode['taxId']);
            }
        }


        if (count($codeValues) > 1) // if there are more than one tax types in the tax group (this can be identified from mst_financetaxgroup.strProcess field)
        {
            if ($operation == 'Inclusive') {
                //step 1: po amount will be multiplied by the first tax rate
                //step 2 : result of the step 1(po amount+tax ammount), will be multiplied by the second tax rate.
                $firstVal = ($amount * $codeValues[0]) / 100;
                $withTaxVal = $firstVal + ((($amount + $firstVal) * $codeValues[1]) / 100);
                $val1 = ($amount * $codeValues[0]) / 100;
                $val2 = ((($amount + $firstVal) * $codeValues[1]) / 100);
            } else if ($operation == 'Exclusive') {
                //get the summation of the two tax rates and multiply it from the amount
                $withTaxVal = ($amount * ($codeValues[0] + $codeValues[1])) / 100;
                $val1 = ($amount * $codeValues[0]) / 100;
                $val2 = ($amount * $codeValues[1]) / 100;
            }
        }
		else if(count($codeValues) == 1 && $operation == 'Isolated')//there is only one tax type for the tax group
		{
			$withTaxVal = ($amount*$codeValues[0])/100;
			$val1 = ($amount*$codeValues[0])/100;
		}
		
		return $withTaxVal;
	}
//-----------------------------------------	
	function callTaxValue($taxId)
	{
		global $db;
	 	$sql = "SELECT
				mst_financetaxisolated.intId,
				mst_financetaxisolated.strCode,
				mst_financetaxisolated.dblRate
				FROM
				mst_financetaxisolated
				WHERE
				mst_financetaxisolated.intId = '$taxId'
				";
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$taxVal = $row['dblRate'];	
		return $taxVal;
	}
//---------------------------------------	
	function getSaveStatus($serialNo,$year)
	{
		global $db;
		$sql = "SELECT trn_poheader.intStatus, trn_poheader.intApproveLevels FROM trn_poheader WHERE (intPONo='$serialNo') AND (`intPOYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
			
			if(($status==0) || ($row['intStatus']==($row['intApproveLevels']+1))){
				$editableFlag=1	;
			}
			else{
				$editableFlag=0;	
			}
			
		return $editableFlag;
	}
//-----------------------------------------------------------
	function getEditLocationValidation($type,$location,$serialNo,$year)
	{
		$flag= 1;
		
		global $db;
		$sql = "SELECT
					trn_poheader.intCompany
					FROM
					trn_poheader
					Inner Join mst_locations ON trn_poheader.intCompany = mst_locations.intId
					WHERE
					trn_poheader.intPONo =  '$serialNo' AND
					trn_poheader.intPOYear =  '$year'";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			if($location == $row['intCompany'])
			$flag=0;
 			
		return $flag;

}
//------------------------------function loadConfirmatonMode-------------------
function getConfirmationMode($programCode,$intUser,$approveLevels,$status){
		global $db;
		$k=$approveLevels+2-$status;
		$sqlp = "SELECT 
		menupermision.intEdit, 
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		if($rowp['int'.$k.'Approval']==1) 
		 $confirmatonMode=1; 
		else
		 $confirmatonMode=0; 
	 
	return $confirmatonMode;
}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		$resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
		 
		// $editMode;
		 
	return $editMode;
}
//------------------------------function get MaxAppByStatus---------------------
function getMaxAppByStatus($serialNo,$year){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	  $sql = "SELECT
				max(trn_poheader_approvedby.intStatus) as status 
				FROM
				trn_poheader_approvedby
				WHERE
				trn_poheader_approvedby.intPONo =  '$serialNo' AND
				trn_poheader_approvedby.intYear =  '$year'";
				
		$resultm = $db->RunQuery2($sql);
		 $rowm=mysqli_fetch_array($resultm);
		 
		 return $rowm['status'];

}

//--------------------------------------------------------getMaxAppByStatus

?>


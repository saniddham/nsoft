<?php
(define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');
$location 	= $_SESSION['CompanyID'];
$company 	= $_SESSION['headCompanyId'];
$userId 	= $_SESSION['userId'];
include_once "class/tables/sys_approvelevels.php";$sys_approvelevels = new sys_approvelevels($db);

$poNo = $_REQUEST['poNo'];
$year = $_REQUEST['year'];
$programName='Purchase Order';
$not_allowed_taxcode_arr = array('15','19','20','25');
$obsoleteCurrency = array("BDT","Franc","INR");

$q	= $_REQUEST['q'];

if($_REQUEST['q']==220){
	$title				= "Raw Materials";
	$programCode		='P0220';
	$menuID				='220';
	$priceEdit_string	='Permision to price edit';
}
else if($_REQUEST['q']==1151){
	$title				= "Non-Raw Materials";
	$programCode		='P1151';
	$menuID				='1151';
	$priceEdit_string	='Permision to price edit non-rm';
}
else if($_REQUEST['q']==1153){
	$title	= "RM and Non-RM";
	$programCode='P1153';
	$menuID='1153';
}

$permission_reorder 	= 'false';

if(($poNo=='')&&($year=='')){
	/*$savedStat = (int)getApproveLevel($programName);
	$intStatus=$savedStat+1;*/
	
	$sys_approvelevels->set($programCode);
	$intStatus=$sys_approvelevels->getintApprovalLevel()+1;
	
}
else{
		 $sql = "SELECT
trn_poheader.intSupplier, 
trn_poheader.intCurrency,
trn_poheader.intPaymentTerm as payTerm,
trn_poheader.intPaymentMode as payMode,
trn_poheader.intShipmentTerm as shipmentTerm,
trn_poheader.intShipmentMode as shipmentMode,
trn_poheader.dtmDeliveryDate,
trn_poheader.intDeliveryTo, 
trn_poheader.dtmPODate,
trn_poheader.strRemarks,
trn_poheader.intStatus,
trn_poheader.intApproveLevels,
trn_poheader.intUser
FROM
trn_poheader
WHERE
trn_poheader.intPONo =  '$poNo' AND
trn_poheader.intPOYear =  '$year'

";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$supplier = $row['intSupplier'];
					$currency = $row['intCurrency'];
					$payTerm = $row['payTerm'];
					$payMode = $row['payMode'];
					$shipmentTerm = $row['shipmentTerm'];
					$shipmentMode = $row['shipmentMode'];
					$deliveryDate = $row['dtmDeliveryDate'];
					$DelTo = $row['intDeliveryTo'];
					$pODate = $row['dtmPODate'];
					$remarks = $row['strRemarks'];
					$intStatus = $row['intStatus'];
					$savedStat=$row['intApproveLevels'];
					$user = $row['intUser'];
					//$costCenter ='';
				 }
}
				 
		$sql = "SELECT
				menus_special.intStatus
FROM
				menus_special
				Inner Join menus_special_permision ON menus_special.intId = menus_special_permision.intSpMenuId
WHERE
menus_special_permision.intUser =  '$userId' AND
menus_special.strPermisionType =  '$priceEdit_string'";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$priceEdit 	= $row['intStatus'];
		
		$sql = "SELECT
				menus_special.intStatus
FROM
				menus_special
				Inner Join menus_special_permision ON menus_special.intId = menus_special_permision.intSpMenuId
WHERE
menus_special_permision.intUser =  '$userId' AND
menus_special.strPermisionType =  'Permision to purchase if stock less than the reorder level'";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$permission_reorder 	= $row['intStatus']=='1'?'true':'false';
		
		
////////////////////////	
$editMode=loadEditMode($programCode,$intStatus,$savedStat,$userId);
$confirmatonMode=loadConfirmatonMode($programCode,$intStatus,$savedStat,$userId);
if($poNo==''){
	$confirmatonMode=0;	
}
if($menuID=='1153'){
	$confirmatonMode=0;	
	$editMode=0;
}
//////////////////////////		
?>
<head>
<title>Purchase Order - <?php echo $title; ?></title>
<!--<script type="text/javascript" src="presentation/procurement/purchaseOrder/addNew/purchaseOrder-js.js"></script>
--><script type="text/javascript">
var permission_reorder = <?php echo $permission_reorder ?> ;
</script>
</head>

<body>
  <?php  
	$sql = "SELECT
			mst_locations.intBlocked
			FROM mst_locations
			WHERE
			mst_locations.intId =  '$location' ";
	
	$result = $db->RunQuery($sql);
	$row=mysqli_fetch_array($result);
 	if($row['intBlocked']=='1'){ 
	$str =  "This Location has been Blocked.Can't raise POs.";
	$maskClass="maskShow";
  ?>
    <div id="divMask" class="<?php echo $maskClass?> mask"> <?php echo $str; ?></div>
 <?php 	return false;
  }
  ?>  
    
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>
<form id="frmPurchaseOrder" name="frmPurchaseOrder" method="post" >
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">Purchase Order - <?php echo $title; ?><div id="menuId" style="display:none"><?php echo $q;?></div></div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="14%" height="22" class="normalfnt">PO No</td>
            <td width="15%"><input name="txtPoNo" type="text" disabled="disabled" class="txtText" id="txtPoNo" style="width:60px" value="<?php echo $poNo ?>"/><input name="txtPoYear" type="text" disabled="disabled" class="txtText" id="txtPoYear" style="width:40px" value="<?php echo $year ?>" /></td>
            <td width="13%">&nbsp;</td>
            <td width="20%">&nbsp;</td>
            <td width="10%" class="normalfnt">&nbsp;</td>
            <td width="28%" align="left"></td>
          </tr>
          <tr>
            <td height="22" class="normalfnt">Supplier</td>
            <td colspan="3"><select name="cboSupplier" style="width:250px" id="cboSupplier" class="validate[required]" >
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_supplier.intId,
							mst_supplier.strCode,
							mst_supplier.strName,
							mst_supplier.strColorCode
							FROM mst_supplier
							WHERE
							mst_supplier.intStatus =  '1' order by strName ASC";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$supplier)
							echo "<option value=\"".$row['intId']."\" selected=\"selected\" style=\"background-color:".$row["strColorCode"]."\">".$row['strName']."</option>";	
						else
							echo "<option value=\"".$row['intId']."\" style=\"background-color:".$row["strColorCode"]."\">".$row['strName']."</option>";	
					}
				?>
            </select></td>
            <td class="normalfnt">Date</td>
            <td align="left"><input name="dtDate" type="text" value="<?php if($poNo){ echo substr($pODate,0,10); }else { echo date("Y-m-d"); }?>" class="txtbox" id="dtDate" style="width:120px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
          </tr>
          <tr>
            <td height="22" class="normalfnt">Currency</td>
            <td><select name="cboCurrency" style="width:103px" id="cboCurrency" class="validate[required]" >
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_financecurrency.intId,
							mst_financecurrency.strCode
							FROM mst_financecurrency";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$currency)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strCode']."</option>";	
						else{
                            if ($_SESSION["headCompanyId"] == '1') {
                                if (!in_array($row['strCode'], $obsoleteCurrency)) {
                                    echo "<option value=\"" . $row['intId'] . "\">" . $row['strCode'] . "</option>";
                                }
                            } else {
                                echo "<option value=\"" . $row['intId'] . "\">" . $row['strCode'] . "</option>";
                            }
                        }
					}
				?>
            </select></td>
            <td class="normalfnt">Exchange Rate</td>
   <?php

		$sql = "SELECT
				mst_financeexchangerate.dblBuying
				FROM mst_financeexchangerate
				WHERE
				mst_financeexchangerate.intCurrencyId =  '$currency' and mst_financeexchangerate.dtmDate =  '$pODate' and intCompanyId='$company'";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$excRate = $row['dblBuying'];
   
   ?>         
            <td class="normalfnt" style="color:#EA820F"><div id="divExchRate" style="display:none"><?php echo $excRate;  ?></div><input type="text" value="<?php echo $excRate ; ?>"  class="validate[required] txtText" id="txtExcRate" name="txtExcRate" style="width:103px" readonly/></td>
            <td class="normalfnt">Deliver Date</td>
            <td align="left"><input name="dtDeliveryDate" type="text" value="<?php if($poNo){ echo $deliveryDate; }else { echo date("Y-m-d"); }?>" class="txtbox <?php echo "validate[required,min[".date("Y-m-d")."]]" ?>" id="dtDeliveryDate" style="width:120px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
          </tr>
          <tr>
            <td height="22" class="normalfnt">Payment Terms</td>
            <td><select name="cboPayTerm" style="width:103px" id="cboPayTerm" class="validate[required]" >
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_financepaymentsterms.intId,
							mst_financepaymentsterms.strName,
							mst_financepaymentsterms.strDescription
							FROM mst_financepaymentsterms";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$payTerm)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']." days"."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strName']." days"."</option>";	
					}
				?>
            </select></td>
            <td class="normalfnt">Payment Mode</td>
            <td><select name="cboPayMode" style="width:103px" id="cboPayMode" class="validate[required]" >
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_financepaymentsmethods.intId,
							mst_financepaymentsmethods.strName,
							mst_financepaymentsmethods.strDescription
							FROM mst_financepaymentsmethods 
							ORDER BY
							mst_financepaymentsmethods.strName ASC";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$payMode)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
            </select></td>
            <td class="normalfnt">Delivered To</td>
            <td align="left"><select name="cboDeliverTo" style="width:181px" id="cboDeliverTo" class="validate[required]" >
              <option value=""></option>
              <?php
					$sql = "SELECT
							mst_locations.intId,
							mst_locations.strName as locName,
							mst_companies.strName as compName 
							FROM
							mst_locations
							Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId 
							AND mst_locations.intCompanyId='$company' AND 
							mst_locations.intBlocked != 1";
							$pp=$sql;
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$DelTo)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['compName']."-".$row['locName']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['compName']."-".$row['locName']."</option>";	
					}
				?>
            </select></td>
          </tr>
          <tr>
            <td height="22" class="normalfnt">Shipment Terms</td>
            <td><select name="cboShipmentTerm" style="width:103px" id="cboShipmentTerm" class="validate[required]" >
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_shipmentterms.intId,
							mst_shipmentterms.strName,
							mst_shipmentterms.strRemark
							FROM mst_shipmentterms 
							ORDER BY
							mst_shipmentterms.strName ASC
							";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$shipmentTerm)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";
                        else {
                            if ($_SESSION['headCompanyId'] == 1) {
                                if ($row['intId'] != 3) {
                                    // for screenline companies this term is not valid
                                    echo "<option value=\"" . $row['intId'] . "\">" . $row['strName'] . "</option>";
                                }
                            }
                            else{
                                echo "<option value=\"" . $row['intId'] . "\">" . $row['strName'] . "</option>";
                            }
                        }

					}
				?>
            </select></td>
            <td class="normalfnt">Shipment Mode</td>
            <td><select name="cboShipmentMode" style="width:103px" id="cboShipmentMode" class="validate[required]" >
              <option value=""></option>
              <?php
					$sql = "SELECT
							mst_shipmentmethod.intId,
							mst_shipmentmethod.strName
							FROM mst_shipmentmethod";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$shipmentMode)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
            </select></td>
            <td class="normalfnt"></td>
            <td align="left">&nbsp;</td>
          </tr>
          <tr>
            <td colspan="4" class="normalfnt">&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td align="left"><div id="divTaxCobo" style="display:none"><select name="cboTaxCode" id="cboTaxCode" style="width:100px;" >
              <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_financetaxgroup.intId,
							mst_financetaxgroup.strCode
							FROM mst_financetaxgroup
							WHERE
							mst_financetaxgroup.intStatus =  '1'
							";
							$pp=$sql;
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
                        if ($_SESSION["headCompanyId"] == '1') {

                            if (!in_array($row['intId'], $not_allowed_taxcode_arr)) {
                                echo "<option value=\"" . $row['intId'] . "\">" . $row['strCode'] . "</option>";
                            }
                        } else {
                            echo "<option value=\"" . $row['intId'] . "\">" . $row['strCode'] . "</option>";
                        }

					}
				?>
                    </select></div></td>
          </tr>
                <tr>
                    <td colspan="4" class="normalfnt">&nbsp;</td>
                    <td class="normalfnt">&nbsp;</td>
                    <td align="left"><div id="divCostcenterCombo" style="display:none"><select name="cboCostcenter" id="cboCostcenter" style="width:100px;">
                                <option value=""></option>
                                <?php
                                $sql2 = "SELECT DISTINCT
                              mst_department.strName,
                              mst_department.intId
                              FROM
                              mst_department
                              INNER JOIN mst_department_heads ON mst_department_heads.DEPARTMENT_ID = mst_department.intId
                              INNER JOIN mst_locations ON mst_department_heads.LOCATION_ID = mst_locations.intId
                              WHERE mst_department.intStatus ='1' AND mst_locations.intCompanyId='$company'
                              GROUP BY mst_department.intId";
                                $pp=$sql;
                                $result = $db->RunQuery($sql2);
                                while($row=mysqli_fetch_array($result))
                                {
                                    echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
                                }
                                ?>
                            </select></div></td>
                </tr>
            </table></td>
      </tr>
            <tr>
                <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
            <td width="14%" height="27" rowspan="2" class="normalfnt">Note</td>
            <td width="34%" rowspan="2"><textarea name="txtRemarks" id="txtRemarks" cols="35" rows="3" maxlength="250"><?php echo $remarks; ?></textarea></td>
            <td width="26%" rowspan="2"><div id="divPrice" style="display:none"><?php echo $priceEdit; ?></div></td>
            <td width="10%" rowspan="2" valign="top" class="normalfnt">&nbsp;</td>
            <td width="16%" align="right" valign="top">&nbsp;</td>
            </tr>
          <tr>
            <td align="right" valign="bottom"><img src="images/Tadd.jpg" width="92" height="24" id="butAddItems" /></td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td><div style="width:900px;height:300px;overflow:scroll" >
          <table width="100%" class="bordered" id="tblPOItems" >
            <tr>
              <th width="3%" height="22" >Del</th>
              <th width="6%" >PRN No</th>
              <th width="12%" >Main Category</th>
              <th width="10%" >Sub Category</th>
              <th width="8%" >Item Code</th>
              <th width="12%" >Item Description</th>
              <th width="5%">UOM</th>
              <th width="9%">Unit Price</th>
              <th width="8%">Discount %</th>
              <th width="8%">Tax Code</th>
              <th width="4%">SVAT</th>
              <th width="4%">Qty</th>
              <th width="4%">MOQ</th>
              <th width="4%">Rolling</th>
              <th width="11%">Amount</th>
              <th width="11%">Remarks</th>
                <th width="8%">Cost center</th>
              </tr>
              <?php
					   $sql = "SELECT
trn_podetails.intPONo,
trn_podetails.intPOYear,
trn_podetails.intPrnNo,
trn_podetails.intPrnYear,
trn_podetails.intItem,
trn_podetails.dblUnitPrice,
trn_podetails.dblDiscount,
trn_podetails.intTaxCode,
trn_podetails.dblQty,
trn_podetails.ROLLING_QTY,
trn_podetails.MO_QTY,
trn_prndetails.dblPrnQty,
trn_prndetails.dblPoQty, 
trn_podetails.SVAT_ITEM, 
mst_item.strName as itemDesc,
mst_item.intMainCategory,
mst_maincategory.strName as mainCategory,
mst_item.intSubCategory,
mst_subcategory.strName as subCategory,
mst_item.strCode,
mst_item.strCode as SUP_ITEM_CODE,
mst_item.intUOM , 
mst_units.strCode as uom,
trn_podetails.strRemarks,
trn_podetails.CostCenter
FROM
trn_podetails 
left Join trn_prndetails ON trn_podetails.intPrnNo = trn_prndetails.intPrnNo AND trn_podetails.intPrnYear = trn_prndetails.intYear AND trn_podetails.intItem = trn_prndetails.intItem
Inner Join mst_item ON trn_podetails.intItem = mst_item.intId
Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
Inner Join mst_units ON mst_item.intUOM = mst_units.intId
LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
WHERE 
/* mst_item.intStatus = '1' AND */
trn_podetails.intPONo =  '$poNo' AND
trn_podetails.intPOYear =  '$year' 
Order by trn_podetails.intPrnNo DESC,trn_podetails.intPrnYear DESC,mst_maincategory.strName ASC, mst_subcategory.strName ASC, mst_item.strName ASC";

	$result = $db->RunQuery($sql);
	$total						= 0;
	$totalDiscount				= 0;
	$grandTotal					= 0;
	while($row=mysqli_fetch_array($result))
	{
		$prnNo					= $row['intPrnNo'];
		$prnYear				= $row['intPrnYear'];
		if($prnNo==0){
			$prnNoYear			= '';	
		}
		else{
			$prnNoYear			= $prnNo."/".$prnYear;	
		}
		$MainCatID				= $row['intMainCategory'];
		$mainCategory			= $row['mainCategory'];
		$subCatID				= $row['intSubCategory'];
		$subCategory			= $row['subCategory'];
		$itemID					= $row['intItem'];
		$itemCode				= $row['strCode'];
                $supItemCode				= $row['SUP_ITEM_CODE'];
                $itemDesc				= $row['itemDesc'];
		$uom					= $row['uom'];
		$unitPrice				= round($row['dblUnitPrice'],6);
		$discount				= $row['dblDiscount'];
		$taxCode				= $row['intTaxCode'];
		$qty					= $row['dblQty'];
		$SVAT_ITEM				= $row['SVAT_ITEM'];
		$balToPoQty				= $row['dblPrnQty']-$row['dblPoQty'];
		if($balToPoQty==''){
			$balToPoQty			= 0;	
		}
		$rolling				= $row['ROLLING_QTY'];
		if($rolling=='')
		$rolling	=0;
		$moq					= $row['MO_QTY'];
		if($moq=='')
		$moq	=0;
	
        $remarks				= $row['strRemarks'];
        $costcenter         =  $row['CostCenter'];

        $amount					= round($unitPrice*(100-$discount)/100*$qty,2);


	?>
			<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF" id="<?php echo $itemID ?>"><?php if($intStatus !=1){ ?><img class="delImg" id="<?php echo $itemID ?>" src="images/del.png" width="15" height="15" /><?php } ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $prnNo."/".$prnYear; ?>"><?php echo $prnNoYear; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $MainCatID ?>"><?php echo $mainCategory ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $subCatID ?>"><?php echo $subCategory ?></td>
                        <?php
                        if($supItemCode!=null)
                        {
                        ?>
                        <td align="center" bgcolor="#FFFFFF" id="<?php echo $itemID ?>"><?php echo $supItemCode ?></td>
                        <?php
                        }else{
                        ?>
                        <td align="center" bgcolor="#FFFFFF" id="<?php echo $itemID ?>"><?php echo $itemCode ?></td>
                        <?php
                        }
                        ?>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $itemID ?>"><?php echo $itemDesc ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $itemID ?>"><?php echo $uom ?></td>

				<td align="center" bgcolor="#FFFFFF" id="<?php echo $unitPrice ?>"><input  id="<?php echo $unitPrice ?>" class="validate[required,custom[number]] calculateValue" style="width:80px;text-align:right" <?php if($priceEdit!=1){ ?> disabled="disabled" <?php }?> type="text" value="<?php echo $unitPrice ?>" onKeyPress="return validateDecimalPlaces(this.value,6,event);"/></td>
                
			<td align="center" bgcolor="#FFFFFF" id=""><input  id="<?php echo $itemID ?>" class="<?php echo "validate[required,custom[number],max[100]] calculateValue" ?>" style="width:50px;text-align:right" type="text" value="<?php echo $discount ?>"/></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $itemID ?>" ><select name="cboTaxCode" id="cboTaxCode" style="width:100px" class="">
              <option value=""></option>
                  <?php
					$sql2 ="SELECT
							mst_financetaxgroup.intId,
							mst_financetaxgroup.strCode
							FROM mst_financetaxgroup
							WHERE
							mst_financetaxgroup.intStatus =  '1'
							";
							$pp=$sql;
					$result2 = $db->RunQuery($sql2);
					while($row2=mysqli_fetch_array($result2)) {
                        if ($taxCode == $row2['intId'])
                            echo "<option value=\"" . $row2['intId'] . "\" selected=\"selected\">" . $row2['strCode'] . "</option>";
                        else {
                            if ($_SESSION["headCompanyId"] == '1') {
                                if (!in_array($row2['intId'], $not_allowed_taxcode_arr)) {
                                    echo "<option value=\"" . $row2['intId'] . "\">" . $row2['strCode'] . "</option>";
                                }
                            } else {
                                echo "<option value=\"" . $row2['intId'] . "\">" . $row2['strCode'] . "</option>";
                            }
                        }

                    }
                  ?>
                </select></td>
                <td align="center" bgcolor="#FFFFFF" id="<?php echo $itemID ?>"><input <?php if($SVAT_ITEM){ ?>checked="checked" <?php }?> type="checkbox" name="chkSvatItem" id="chkSvatItem" /></td>
                <td align="center" bgcolor="#FFFFFF" id="<?php echo $itemID ?>"><input  id="<?php echo $balToPoQty ?>" class="<?php echo "validate[required,custom[number]] calculateValue balQty calMax" ?>" style="width:80px;text-align:right" type="text" value="<?php echo $qty ?>"/></td>
                <td align="center" bgcolor="#FFFFFF" id="<?php echo $itemID ?>"><input  id="<?php echo $moq ?>" class="<?php echo "validate[required,custom[number]] calculateValue moq calMax" ?>" style="width:80px;text-align:right" type="text" value="<?php echo $moq ?>"/></td>
                <td align="center" bgcolor="#FFFFFF" id="<?php echo $itemID ?>"><input  id="<?php echo $rolling ?>" class="<?php echo "validate[required,custom[number]] calculateValue rolling calMax" ?>" style="width:80px;text-align:right" type="text" value="<?php echo $rolling ?>"/></td>
                <td align="right" bgcolor="#FFFFFF" id="<?php echo $itemID ?>" ><?php echo number_format($amount,2,'.','') ?></td>
                <td align="right" bgcolor="#FFFFFF" id="<?php echo $itemID ?>" ><input  id="<?php echo $remarks ?>" class="<?php echo "calculateValue" ?>" style="width:200px;text-align:right" type="text" value="<?php echo $remarks ?>"/></td>
                <td align="right" bgcolor="#FFFFFF" id="<?php echo $itemID ?>" ><select name="cboCostcenter" id="cboCostcenter" style="width:100px" class="">
                        <option value=""></option>
                        <?php
                        $sql3 = "SELECT DISTINCT
                              mst_department.strName,
                              mst_department.intId
                              FROM
                              mst_department
                              INNER JOIN mst_department_heads ON mst_department_heads.DEPARTMENT_ID = mst_department.intId
                              INNER JOIN mst_locations ON mst_department_heads.LOCATION_ID = mst_locations.intId
                              WHERE mst_department.intStatus ='1' AND mst_locations.intCompanyId='$company'
                              GROUP BY mst_department.intId";
                        $pp=$sql;
                        $result3 = $db->RunQuery($sql3);
                        while($row=mysqli_fetch_array($result3))
                        {
                            if($row['intId']==$costcenter)
                                echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";
                            else
                                echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
                        }
                        ?>
                    </select>
                </td>

            </tr>
        <?php
        $total				+= round($qty*$unitPrice,2);
        $totalDiscount		+= round($qty*($unitPrice)*($discount)/100,2);
    }
              ?>
            
          </table>
            </div></td>
      </tr>
      <tr>
        <td align="center" class=""><table width="100%" border="0">
          <tr>
            <td class="normalfnt">&nbsp;</td>
            <td colspan="3" rowspan="3">&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">Total</td>
            <td width="9%" align="right" class="tableBorder"><span id="divTotal" class="normalfnt"><?php echo number_format($total,2,'.','') ?></span>&nbsp;</td>
            </tr>
          <tr>
            <td width="17%" class="normalfnt">&nbsp;</td>
            <td width="38%">&nbsp;</td>
            <td width="12%" class="normalfnt">Total Discount</td>
            <td class="tableBorder" align="right"><span id="divTotalDiscount" class="normalfnt"><?php echo number_format($totalDiscount,2,'.','') ?></span>&nbsp;</td>
            </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">Grand Total</td>
            <?php
				$grandTotal=($total-$totalDiscount);
			?>
            <td class="tableBorder" align="right"><span id="divGrandTotal" class="normalfnt"><?php echo number_format($grandTotal,2,'.','') ?></span>&nbsp;</td>
            </tr>
          
          </table></td>
      </tr>
      <tr>    
        <td align="center" class="tableBorder_allRound"><img src="images/Tnew.jpg" width="92" height="24" id="butNew" name="butNew"  class="mouseover"/>
          <?php if(($form_permision['add']||$form_permision['edit']) /*&& $editMode==1*/){ ?><img src="images/Tsave.jpg" width="92" height="24" id="butSave" name="butSave"  class="mouseover"/><?php } ?><img class="mouseover" src="images/Tconfirm.jpg" width="92" height="24" id="butConfirm" name="butConfirm" style=" <?php if(($intStatus>1) and ($confirmatonMode==1)){  echo 'display:yes'; } else { ?>  <?php echo 'display:none';  } ?>  " /><img src="images/Treport.jpg" width="92" height="24" id="butReport" name="butReport"  class="mouseover"/><a href="main.php"><img src="images/Tclose.jpg" width="92" height="24"  class="mouseover"/></a></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>

	<!-- items to prn -->
	<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1">
   <!-- <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php" style="width:800;height:800;border:0;overflow:hidden">
    </iframe>-->
    </div>

	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>

</body>
</html>
<?php 
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
			 
	return $editMode;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	 $sqlp = "SELECT

		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
?>


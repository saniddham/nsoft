<?php
//error_reporting(E_ALL);
//ini_set('display_errors','On');

/*date_default_timezone_set('America/Los_Angeles');

$script_tz = date_default_timezone_get();

if (strcmp($script_tz, ini_get('date.timezone'))){
    echo 'Script timezone differs from ini-set timezone.';
} else {
    echo 'Script timezone and ini-set timezone match.';
}*/
//echo $poNo 			= $_REQUEST['poNo'];
//$year 			= $_REQUEST['year'];
//$companyId 		= $_REQUEST['locationId'];
//$intUser 		= $_REQUEST['userId'];
/*$enterUserEmail = urldecode($_REQUEST['enterUserEmail']);
$enterUserName  = urldecode($_REQUEST['enterUserName']);

$mainPath 		= urldecode($_REQUEST['mainPath']);*/
//$locationId 	= $companyId;

$thisFilePath =  $_SERVER['PHP_SELF'];
include '../../../../dataAccess/DBManager2.php';		
$db =  new DBManager2(1);

$db->connect();//open connection.

$programName='Purchase Order';
$programCode='P0220';
/*$poNo = '100003';
$year = '2012';
$approveMode=1;
*/
//echo $companyId;
 $sql = "SELECT
mst_supplier.strName as supplier, 
mst_financecurrency.strCode,
mst_financepaymentsterms.strName as payTerm,
mst_financepaymentsmethods.strName as payMode,
mst_shipmentterms.strName as shipmentTerm,
mst_shipmentmethod.strName as shipmentMode,
trn_poheader.dtmDeliveryDate,
mst_companies.strName as delToCompany,
mst_locations.strName as delToLocation ,
trn_poheader.dtmPODate,
trn_poheader.strRemarks,
trn_poheader.intStatus,
trn_poheader.intApproveLevels,
trn_poheader.intUser,
trn_poheader.intCompany as poRaisedLocationId, 
sys_users.strUserName 
FROM
trn_poheader
Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
left Join mst_financecurrency ON trn_poheader.intCurrency = mst_financecurrency.intId
left Join mst_financepaymentsterms ON trn_poheader.intPaymentTerm = mst_financepaymentsterms.intId
left Join mst_financepaymentsmethods ON trn_poheader.intPaymentMode = mst_financepaymentsmethods.intId
left Join mst_shipmentterms ON trn_poheader.intShipmentTerm = mst_shipmentterms.intId
left Join mst_shipmentmethod ON trn_poheader.intShipmentMode = mst_shipmentmethod.intId
Inner Join mst_locations ON trn_poheader.intDeliveryTo = mst_locations.intId
Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId 
Inner Join sys_users ON trn_poheader.intUser = sys_users.intUserId
WHERE
trn_poheader.intPONo =  '$poNo' AND
trn_poheader.intPOYear =  '$year' ";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$supplier = $row['supplier'];
					$currency = $row['strCode'];
					$payTerm = $row['payTerm'];
					$payMode = $row['payMode'];
					$shipmentTerm = $row['shipmentTerm'];
					$shipmentMode = $row['shipmentMode'];
					$deliveryDate = $row['dtmDeliveryDate'];
					$company = $row['delToCompany'];
					$location = $row['delToLocation'];
					$locationId = $row['poRaisedLocationId'];//this is use in report header(reportHeader.php)--------------------
					$pODate = $row['dtmPODate'];
					$pOBy = $row['strUserName'];
					$remarks = $row['strRemarks'];
					$intStatus = $row['intStatus'];
					$savedLevels = $row['intApproveLevels'];
					$user = $row['intUser'];
				 }
			 
include "../../../../libraries/mail/mail.php";
//echo $companyId;	
$sql_email ="SELECT
				sys_users.strUserName,
				sys_users.strEmail
			FROM
				sys_mail_eventusers
				Inner Join sys_users ON sys_users.intUserId = sys_mail_eventusers.intUserId
			WHERE
				sys_mail_eventusers.intMailEventId 	=  '1006' AND
				sys_mail_eventusers.intCompanyId 	=  '$companyId'
		";
$result_email = $db->RunQuery($sql_email);
while($row_email=mysqli_fetch_array($result_email))
{
	ob_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Purchase Order Report</title>


<style>
.gridHeader{
	padding: 1px 0px 4px 8px;
	width:auto;
	border:0;
	background-color: #FAD163;
		
	text-align:center;
	font-size: 11px;
	font-family: Verdana;
	text-align:center;
	color: #751609;
}
.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:232px;
	top:168px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
.normalfntGrey {
	font-family: Verdana;
	font-size: 11px;
	color: #999;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntMid {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:center;
}
.normalfntRight {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:right;
}
.grid{
	background-color:#999;
	border-spacing:1px;
}
</style>
</head>

<body>
<?php
 if($intStatus>1)//pending
{
?>
<?php
}
?>
<form id="frmPrchaseOrderReport" name="frmPrchaseOrderReport" method="post" action="rptPurchaseOrder.php">
  <div align="center">
  <div style="background-color:#FFF" ><strong></strong></div>
<table width="900" style="border-top:double;border-bottom:double;border-left:double;border-right:double;border-color:#06F" align="center" bgcolor="#FFFFFF" >
<tr>
  <td>
  <table width="100%">
  <tr>
    <td colspan="9" align="center" ><table width="100%" cellpadding="0" cellspacing="0">
      <tr>
        <td class="tophead"><table width="100%" cellpadding="0" cellspacing="0">
          <tr>
            <td width="15%"><img src="<?php echo $mainPath?>images/qpet_logo_sm.png" alt="" class="mainImage" /></td>
            <td width="1%" class="normalfnt">&nbsp;</td>
            <td align="center" valign="top" width="68%" class="topheadBLACK"><?php
	 $SQL = "SELECT
mst_companies.strName,
mst_locations.strName as locationName,
mst_locations.strAddress,
mst_locations.strStreet,
mst_locations.strCity,
mst_country.strCountryName,
mst_locations.strPhoneNo,
mst_locations.strFaxNo,
mst_locations.strEmail,
mst_companies.strWebSite,
mst_locations.strZip
FROM
mst_locations
Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
Inner Join mst_country ON mst_companies.intCountryId = mst_country.intCountryID
WHERE
mst_locations.intId =  '$locationId'
;";
	
	$result = $db->RunQuery($SQL);
	$row = mysqli_fetch_array($result);
	$companyName		= $row["strName"];
	$locationName		= $row["locationName"];
	$companyAddress1	= $row["strAddress"];
	$companyStreet		= $row["strStreet"];
	$companyCity		= $row["strCity"];
	$companyCountry		= $row["strCountryName"];
	$companyZipCode		= $row["strZip"];
	$companyPhone		= "(".$companyZipCode.")".$row["strPhoneNo"];
	$companyFax		 	= "(".$companyZipCode.")".$row["strFaxNo"];
	$companyEmail		= $row["strEMail"];
	$companyWeb		 	= $row["strWebSite"];
	
	?>
              <b><?php echo $companyName.""; ?></b>
              <p class="normalfntMid">
                <?php // (".$locationName.") --> Edited By Lasantha 14th of May 2012?>
                <?php echo $companyAddress1." ".$companyStreet." ".$companyCity." ".$companyCountry."."."<br><b>Tel : </b>".$companyPhone." <b>Fax : </b>".$companyFax." <br><b>E-Mail : </b>".$companyEmail." <b>Web : </b>".$companyWeb;?></p></td>
            <td width="16%" class="tophead">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  <tr>
    <td colspan="9" align="center" ><strong>PURCHASE ORDER REPORT</strong></td>
  </tr>
  <tr>
    <td colspan="9" align="center" >&nbsp;</td>
  </tr>
  <tr>
  <?php
 	if($intStatus==1)
	{
	?>
   <td colspan="9" class="APPROVE" >CONFIRMED</td>
   <?PHP
	}
	else if($intStatus==0)
	{
   ?>
   <td colspan="9" class="APPROVE" style="color:#F00">REJECTED</td>
   <?php
	}
	else
	{
   ?>
   <td width="10%" colspan="9" class="APPROVE">PENDING</td>
   <?php
	}
   ?>
  </tr>
  <tr>
    <td width="1%">&nbsp;</td>
    <td width="10%"><span class="normalfnt"><strong>PO No</strong></span></td>
    <td width="2%" align="center" valign="middle"><strong>:</strong></td>
    <td width="15%"><span class="normalfnt"><?php echo $poNo ?>/<?php echo $year ?></span></td>
    <td width="15%" class="normalfnt"><strong>Delivery Date</strong></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="13%"><span class="normalfnt"><?php echo $deliveryDate ?></span></td>
    <td width="12%"><div id="divPoNo" style="display:none"><?php echo $poNo ?>/<?php echo $year ?></div></td>
  <td width="12%"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Deliver To</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $company."-".$location   ?></span></td>
    <td><span class="normalfnt"><strong>PO Date</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $pODate  ?></span></td>
    <td class="normalfnt">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Supplier</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $supplier  ?></span></td>
    <td><span class="normalfnt"><strong> PO By</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $pOBy  ?></span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
<tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Currency</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $currency  ?></span></td>
    <td><span class="normalfnt"><strong> Payment Mode</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $payMode  ?></span></td>
    <td><span class="normalfnt"><strong>Payment Term</strong></span></td>
    <td><strong>:</strong>&nbsp;<span class="normalfnt"><?php echo $payTerm  ?>
    </span></td>
  </tr>  
 <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Supplier</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $supplier ?></span></td>
    <td><span class="normalfnt"><strong> Shipment Mode</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $shipmentMode ?></span></td>
    <td><span class="normalfnt"><strong>Shipment Term</strong></span></td>
    <td><strong>:</strong>&nbsp;<span class="normalfnt"><?php echo $shipmentTerm  ?>
    </span></td>
  </tr> 
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Remarks</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td colspan="6" align="left"><span class="normalfnt"><?php echo $remarks ?></span></td>
  </tr>
  </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td width="-1%">&nbsp;</td>
        <td colspan="7" class="normalfnt" align="left">
          <table width="100%" class="grid" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr class="gridHeader">
              <td width="6%"  height="22" style="display:none">PRN No</td>
              <td width="8%" >Main Category</td>
              <td width="9%" >Sub Category</td>
              <td width="8%" >Item Code</td>
              <td width="16%" >Item</td>
              <td width="6%" >UOM</td>
              <td width="7%" >Unit Price</td>
              <td width="7%" >Discount</td>
              <td width="7%" >Tax Code</td>
              <td width="3%" >Qty</td>
              <td width="6%" >Amount</td>
              <td width="10%" >Tax Ammount</td>
              <td width="7%" >Total</td>
              </tr>
              <?php 
	  	   $sql1 = "SELECT
trn_podetails.intPONo,
trn_podetails.intPOYear,
trn_podetails.intPrnNo,
trn_podetails.intPrnYear,
trn_podetails.intItem,
mst_item.strCode as itemCode, 
mst_item.strName,
mst_maincategory.strName as mainCategory,
mst_subcategory.strName as subCategory,
trn_podetails.dblUnitPrice,
trn_podetails.dblDiscount,
trn_podetails.intTaxCode,
trn_podetails.dblTaxAmmount,
mst_financetaxgroup.strCode, 
(trn_podetails.dblQty) as dblQty,
mst_item.intUOM, 
mst_units.strName as uom 
FROM
trn_podetails
Inner Join mst_item ON trn_podetails.intItem = mst_item.intId
Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
left Join mst_financetaxgroup ON trn_podetails.intTaxCode = mst_financetaxgroup.intId 
Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
WHERE
trn_podetails.intPONo =  '$poNo' AND
trn_podetails.intPOYear =  '$year' 
group by trn_podetails.intItem, trn_podetails.dblDiscount, trn_podetails.intTaxCode  
Order by trn_podetails.intPrnNo DESC,trn_podetails.intPrnYear DESC,mst_maincategory.strName ASC, mst_subcategory.strName ASC, mst_item.strName ASC   
";
			$result1 = $db->RunQuery($sql1);
			$totQty=0;
			$totAmmount=0;
			$totTax=0;
			$totTaxAndAmmount=0;
			while($row=mysqli_fetch_array($result1))
			{
	  ?>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfnt" style="display:none" >&nbsp;<?php echo $row['intPrnNo']."/".$row['intPrnNo']; ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['mainCategory']?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['subCategory'] ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['itemCode'] ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['strName'] ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['uom'] ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['dblUnitPrice'] ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['dblDiscount'] ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['strCode'] ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['dblQty'] ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo number_format(($row['dblUnitPrice']*(100-$row['dblDiscount'])/100)*$row['dblQty'],2) ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo number_format($row['dblTaxAmmount'],2) ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo number_format(($row['dblUnitPrice']*(100-$row['dblDiscount'])/100)*$row['dblQty']+$row['dblTaxAmmount'],2) ?>&nbsp;</td>
              </tr>
      <?php 
			$totQty+=$row['dblQty'];
			$totAmmount+=($row['dblUnitPrice']*(100-$row['dblDiscount'])/100*$row['dblQty']);
			$totTax+=$row['dblTaxAmmount'];
			$totTaxAndAmmount+=($row['dblUnitPrice']*(100-$row['dblDiscount'])/100*$row['dblQty'])+$row['dblTaxAmmount'];
			}
	  ?>
            <tr class="normalfnt"  bgcolor="#CCCCCC">
              <td class="normalfnt" style="display:none" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $totQty ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $totAmmount ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo number_format($totTax,2) ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo number_format($totTaxAndAmmount,2) ?>&nbsp;</td>
              </tr>
            </table>
          </td>
        <td width="6%">&nbsp;</td>
        </tr>
      
      </table>
    </td>
</tr>
<?php  
 	if($intStatus!=0)
	{
				for($i=1; $i<=$savedLevels; $i++)
				{
					 $sqlc = "SELECT
							trn_poheader_approvedby.intApproveUser,
							trn_poheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName,
							trn_poheader_approvedby.intApproveLevelNo
							FROM
							trn_poheader_approvedby
							Inner Join sys_users ON trn_poheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							trn_poheader_approvedby.intPONo =  '$poNo' AND
							trn_poheader_approvedby.intYear =  '$year' AND
							trn_poheader_approvedby.intApproveLevelNo =  '$i' order by intApproveLevelNo asc
";
					 $resultc = $db->RunQuery($sqlc);
					 $rowc=mysqli_fetch_array($resultc);
						if($i==1)
						$desc="1st ";
						else if($i==2)
						$desc="2nd ";
						else if($i==3)
						$desc="3rd ";
						else
						$desc=$i."th ";
					 //  $desc=$ap.$desc;
					 $desc2=$rowc['UserName']."(".$rowc['dtApprovedDate'].")";
					 if($rowc['UserName']=='')
					 $desc2='---------------------------------';
				?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> Appoved By - </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
            </tr>
<?php
			}
	}
	else{
					 $sqlc = "SELECT
							trn_poheader_approvedby.intApproveUser,
							trn_poheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName,
							trn_poheader_approvedby.intApproveLevelNo
							FROM
							trn_poheader_approvedby
							Inner Join sys_users ON trn_poheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							trn_poheader_approvedby.intPONo =  '$poNo' AND
							trn_poheader_approvedby.intYear =  '$year' AND
							trn_poheader_approvedby.intApproveLevelNo =  '0'  order by intApproveLevelNo asc 
";
					 $resultc = $db->RunQuery($sqlc);
					 $rowc=mysqli_fetch_array($resultc);
					  ?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong> Rejected By - </strong></span><span class="normalfnt"><?php echo $rowc['UserName']."(".$rowc['dtApprovedDate'].")";?></span></td>
            </tr>
<?php
	}
?>
<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
</body>
</html>

<?php
$body = ob_get_clean();

		$mailHeader = "$emailHeader ($poNo/$year)";
		echo sendMessage($enterUserEmail,$enterUserName,$row_email['strEmail'],$mailHeader,$body);
/*		$sql_final = "INSERT INTO  sys_emailpool (`intLocationId`,`strFromEmail`,`strToEmail`,`strMailHeader`,`strEmailBody`,`intEnterUserId`,`dtmEnterDate`) 
		VALUES ('$x_locationId','$enterUserEmail','".$row_email['strEmail']."','$mailHeader','$body','$userId',now())";
		$db->RunQuery($sql_final);*/
}
//header('Location:purchaseOrder.php');
	$db->commit();		
	$db->disconnect();

?>
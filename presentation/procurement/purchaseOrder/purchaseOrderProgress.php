<?php
$backwardseperator = "../../../";

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Purchase Order Pogress</title>

<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />


</head>

<body>
<form id="frmCompanyDetails" name="frmCompanyDetails" method="post" action="">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">Purchase Order 
		    
Progress</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableBorder_allRound">
          <tr>
            <td width="10%" height="22" class="normalfnt">&nbsp;</td>
            <td width="22%">&nbsp;</td>
            <td width="17%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="11%" class="normalfnt">&nbsp;</td>
            <td width="15%">&nbsp;</td>
            </tr>
          <tr>
            <td height="22" class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td><div style="width:900px;height:300px;overflow:scroll" >
          <table width="100%" class="grid" >
            <tr class="gridHeader">
              <td width="6%" height="21" >PO No</td>
              <td width="9%" >PO Type</td>
              <td width="12%" >Supplier</td>
              <td width="12%" >PO Date</td>
              <td width="9%">Delivery Date</td>
              <td width="11%">Approval Status</td>
              <td width="8%">Qty</td>
              <td width="9%"> Received Qty</td>
              <td width="9%">Pogress(%)</td>
              <td width="7%"> Report</td>
              </tr>
              <?php
			  $sql1="SELECT
					trn_poheader.intPONo,
					trn_poheader.intPOYear,
					mst_supplier.strName,
					trn_poheader.dtmPODate,
					trn_poheader.dtmDeliveryDate,
					trn_poheader.intStatus,
					Sum(trn_podetails.dblQty) AS dblQty,
					Sum(trn_podetails.dblGRNQty) AS dblGRNQty
					FROM
					trn_poheader
					Inner Join trn_podetails ON trn_poheader.intPONo = trn_podetails.intPONo AND trn_poheader.intPOYear = trn_podetails.intPOYear
					Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
					GROUP BY
					trn_poheader.intPONo,
					trn_poheader.intPOYear,
					mst_supplier.strName,
					trn_poheader.dtmPODate,
					trn_poheader.dtmDeliveryDate,
					trn_poheader.intStatus";
					$result1 = $db->RunQuery($sql1);
					$totQty=0;
					$totAmmount=0;
					while($row=mysqli_fetch_array($result1))
					{
			  ?>
            <tr class="normalfnt">
              <td align="center" bgcolor="#FFFFFF"><?php echo $row['intPONo']."/".$row['intPOYear']?></td>
              <td bgcolor="#FFFFFF">&nbsp;</td>
              <td bgcolor="#FFFFFF"><?php echo $row['strName']?></td>
              <td bgcolor="#FFFFFF"><?php echo $row['dtmPODate']?></td>
              <td bgcolor="#FFFFFF"><?php echo $row['dtmDeliveryDate']?></td>
              <td bgcolor="#FFFFFF"><?php echo $row['intStatus']?></td>
              <td bgcolor="#FFFFFF"><?php echo $row['dblQty']?></td>
              <td bgcolor="#FFFFFF"><?php echo $row['dblGRNQty']?></td>
              <td bgcolor="#FFFFFF"><?php echo $row['dblGRNQty']/$row['dblQty']*100?></td>
              <td bgcolor="#FFFFFF"><a href="listing/rptPurchaseOrder.php?poNo=<?php echo $row['intPONo']?>&year=<?php echo $row['intPOYear']?>&approveMode=0" target="_blank"><img src="../../../images/view.png" width="91" height="19" /></a></td>
              </tr>
              <?php
					}
			  ?>
            </table>
          </div></td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound"><img src="../../../images/Tclose.jpg" width="92" height="24" /></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
</body>
</html>

var basePath	="presentation/warehouse/itemInspection/addNew/";
var reportId	="921";
	
			
$(document).ready(function() {
	
	$("#frmItemsInspection").validationEngine();
		loadGridData();
	
	$('#frmItemsInspection #cboGRNNo').die('change').live('change',function(){
		loadGridData();
	});
	//----------------------------------------------------
	$('#frmItemsInspection #cboGRNYear').die('change').live('change',function(){
		clearRows()
		loadGRNNos();
	});
   //-------------------------------------------------------
	$('#frmItemsInspection .checkAll').die('click').live('click',function(){
		checkAll();
	});
	//-------------------------------------------------------
	$('#frmItemsInspection #butSave').die('click').live('click',function(){
		save();
	});
  //-------------------------------------------------------
	$('#frmItemsInspection .confirm').die('click').live('click',function(){
		approveInspections(this);
	});
  //-------------------------------------------------------
	$('#frmItemsInspection #butReport').die('click').live('click',function(){
		viewReport();
	});
	//-----------------------------------
	$('#frmItemsInspection #butConfirm').die('click').live('click',function(){
		confirmRpt();
	});
	//-----------------------------------------------------
	$('#frmItemsInspection #butClose').die('click').live('click',function(){
			//load('main.php');	
	});
	//--------------refresh the form-----------------------
	$('#frmItemsInspection #butNew').die('click').live('click',function(){
		newPage();
	});
	//-----------------------------------------------------
});

//----------end of ready -------------------------------

//-------------------------------------
function alertx()
{
	//var grnNo=$('#cboGRNNo').val();
	//var grnYear=$('#cboGRNYear').val();
	$('#frmItemsInspection #butSave').validationEngine('hide')	;
	//window.location.href = "itemInspection.php";
	window.location.href = window.location.href;
	//$('#frmItemsInspection #cboGRNNo').val(grnNo);
	//$('#frmItemsInspection #cboGRNYear').val(grnYear);
	//$('#frmItemsInspection #cboGRNNo').change();
}
function alertxC()
{
	var grnNo=$('#cboGRNNo').val();
	var grnYear=$('#cboGRNYear').val();
	$('#frmItemsInspection #butConfirm').validationEngine('hide')	;
//	window.location.href = "itemInspection.php";
	$('#frmItemsInspection #cboGRNNo').val(grnNo);
	$('#frmItemsInspection #cboGRNYear').val(grnYear);
	$('#frmItemsInspection #cboGRNNo').die('change').live('change');
}
function alertDelete()
{
	$('#frmItemsInspection #butDelete').validationEngine('hide')	;
}

//----------------------------------------------
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
//----------------------------------------------- 
function clearRows()
{
	//var rowCount = $('#tblDispatchPopup >tbody >tr').length;
	var rowCount = document.getElementById('tblMain').rows.length;
	//alert(rowCount);
	//var cellCount = document.getElementById('tblMain').rows[1].cells.length;
	for(var i=2;i<rowCount;i++)
	{
			document.getElementById('tblMain').deleteRow(2);
			
	}
}
//----------------------------------------------------
function loadGRNYears(){
		var url 		= basePath+"itemInspection-db.php?requestType=loadGRNYears";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"",
			async:false,
			success:function(json){
					document.getElementById("cboGRNYear").innerHTML=json.arrCombo;
			}
		});
}
//----------------------------------------------------
function loadGRNNos(){
	    var grnYear = $('#cboGRNYear').val();
		
		var url 		= basePath+"itemInspection-db.php?requestType=loadGRNNos";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"grnYear="+grnYear,
			async:false,
			success:function(json){
					document.getElementById("cboGRNNo").innerHTML=json.arrCombo;
			}
		});
}
//----------------------------------------------------
function selectCurrentDate(){
	    var grnYear = $('#cboGRNYear').val();
		var url 		= basePath+"itemInspection-db.php?requestType=selectCurrentDate";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"grnYear="+grnYear,
			async:false,
			success:function(json){
					document.getElementById("cboGRNNo").innerHTML=json.arrCombo;
			}
		});
}
//----------------------------------------------------
function loadSerialNoAndYear(){
	    var grnYear = $('#cboGRNYear').val();
		var url 		= basePath+"itemInspection-db.php?requestType=loadSerialNoAndYear";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"grnYear="+grnYear,
			async:false,
			success:function(json){
					document.getElementById("txtSerialNo").innerHTML=json.serialNo;
					document.getElementById("txtSerialYear").innerHTML=json.serialYear;
			}
		});
}
//----------------------------------------------------
function loadGridData(){
	
	 clearRows();
	if($('#frmItemsInspection #cboGRNNo').val() != ''){
	 
	    var grnNo = $('#cboGRNNo').val();
	    var grnYear = $('#cboGRNYear').val();
		var url 		= basePath+"itemInspection-db.php?requestType=loadGridData";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"grnNo="+grnNo+"&grnYear="+grnYear,
			async:false,
			success:function(json){
				
				var length = json.arrData.length;
				var arrData = json.arrData;
				for(var i=0;i<length;i++)
				{
					var maincatId=arrData[i]['intMainCategory'];	
					var mainCatName=arrData[i]['mainCatName'];
					var subCatId=arrData[i]['intSubCategory'];
					var subCatName=arrData[i]['subCatName'];	
					var code=arrData[i]['strCode'];
                                        var supItemCode=arrData[i]['SUP_ITEM_CODE'];
					var uom=arrData[i]['uom'];	
					var itemId=arrData[i]['intItemId'];	
					var itemName=arrData[i]['itemName'];
					var goodQty=parseFloat(arrData[i]['goodQty']);
					var dammagedQty=parseFloat(arrData[i]['dammagedQty']);
					var inspectionStatus=parseFloat(arrData[i]['inspectionStatus']);
					var approveLevels=parseFloat(arrData[i]['approveLevels']);
					var remarks=arrData[i]['remarks'];
					var grnQty=parseFloat(arrData[i]['grnQty']);
					var confQty=parseFloat(arrData[i]['confQty']);
					var stockQty=parseFloat(arrData[i]['stockQty']);
					var confDmgQty=(confQty-stockQty);
					if(confDmgQty<0){
						confDmgQty=0;
					}
					var confirmPermission=getMenuPermission(approveLevels,inspectionStatus);
					
					var editFlag=0;
					var confirmFlag=0;
					var strConfirmation='';
					if(inspectionStatus==(approveLevels+1) || (inspectionStatus==0)){
						editFlag=1;
					}
					if((inspectionStatus>1) && (confirmPermission==1)){
						confirmFlag=1;
					}
					
					if(inspectionStatus==1){
						//var bgColor='#008484'
						var bgColor='#6AB5B5';
						strConfirmation='Confirmed';
					}
					else if(inspectionStatus>1){
					//	var bgColor='#FFA6A6'
						var bgColor='#FFFFFF';
						strConfirmation='Pending';
					}
					else{
						var bgColor='#FFFFFF'
						strConfirmation='Not Saved';
					}
					
					editFlag=1;

					var content='<tr class="normalfnt">';
					if(grnQty==confQty){
						var bgColor='#6AB5B5';
					content +='<td align="center" bgcolor="'+bgColor+'" id=""><input type="checkbox" id="chkItem" class="checkItem" disabled="disabled"/>';
					}
					else{
						var bgColor='#FFFFFF';
					content +='<td align="center" bgcolor="'+bgColor+'" id=""><input type="checkbox" id="chkItem" class="checkItem" />';
					}
					content +='<td align="center" bgcolor="'+bgColor+'"  id="'+maincatId+'">'+mainCatName+'</td>';
					content +='<td align="center" bgcolor="'+bgColor+'" id="'+subCatId+'">'+subCatName+'</td>';
                                        if(supItemCode!=null)
                                        {
                                        content +='<td align="center" bgcolor="'+bgColor+'" id="'+itemId+'">'+supItemCode+'</td>';   
                                        }else{
					content +='<td align="center" bgcolor="'+bgColor+'" id="'+itemId+'">'+code+'</td>';
                                        }
					content +='<td align="center" bgcolor="'+bgColor+'" id="'+itemId+'" class="item">'+itemName+'</td>';
					content +='<td align="center" bgcolor="'+bgColor+'" id="'+uom+'" class="uom">'+uom+'</td>';
					content +='<td align="center" bgcolor="'+bgColor+'" id="'+grnQty+'" class="grnQty">'+grnQty+'</td>';
					content +='<td align="center" bgcolor="'+bgColor+'" id="'+confQty+'" class="confQty">'+stockQty+'</td>';
					content +='<td align="center" bgcolor="'+bgColor+'" id="'+confDmgQty+'" class="confDmgQty">'+confDmgQty+'</td>';
					
					
					if(editFlag==0){
					content +='<td align="center" bgcolor="'+bgColor+'" id="" ><input  id="txtGoodQty" class="validate[required,custom[number],max['+(grnQty-confQty)+']] goodQty validate" style="width:80px;text-align:right" type="text" value="'+goodQty+'" disabled="disabled"/></td>';
					content +='<td align="center" bgcolor="'+bgColor+'" id="" ><input  id="txtDmgQty" class="validate[required,custom[number],max['+(grnQty-confQty)+']] dammagedQty validate" style="width:80px;text-align:right" type="text" value="'+dammagedQty+'" disabled="disabled"/></td>';
					content +='<td align="center" bgcolor="'+bgColor+'" id="" ><input  id="txtRemarks" class="remarks" style="width:80px;text-align:right" type="text" value="'+remarks+'" disabled="disabled"/></td>';
					}
					else{
					content +='<td align="center" bgcolor="'+bgColor+'" id="" ><input  id="'+goodQty+'" class="validate[required,custom[number],max['+(grnQty-confQty)+']] goodQty validate" style="width:80px;text-align:right" type="text" value="'+goodQty+'"/></td>';
					content +='<td align="center" bgcolor="'+bgColor+'" id="" ><input  id="'+dammagedQty+'" class="validate[required,custom[number],max['+(grnQty-confQty)+']] dammagedQty validate" style="width:80px;text-align:right" type="text" value="'+dammagedQty+'"/></td>';
					content +='<td align="center" bgcolor="'+bgColor+'" id="" ><input  id="txtRemarks" class="remarks" style="width:80px;text-align:right" type="text" value="'+remarks+'"/></td>';
					}
					
					
					content +='</tr>';
					
					add_new_row('#frmItemsInspection #tblMain',content);
					
				}
			}
		});
	}
}
//----------------------------------------------------

function validatee(){
	var flag=0;
	var chkFlag=1;
	var disableFlag=1;
		$('#tblMain .item').each(function(){
			var checked=$(this).parent().find(".checkItem").prop('checked');
			var disabled=$(this).parent().find(".checkItem").prop('disabled');
			var grnQty=parseFloat($(this).parent().parent().find('.grnQty').html());
			var confQty=parseFloat($(this).parent().parent().find('.confQty').id);
			confQty
			var goodQty=parseFloat($(this).parent().parent().find('.goodQty').val());
			var dammagedQty=parseFloat($(this).parent().parent().find('.dammagedQty').val());
			//alert(grnQty+'-'+goodQty+'-'+dammagedQty);
			if((grnQty-confQty)<(goodQty+dammagedQty)){
				$(this).parent().parent().find('.goodQty').focus();
				flag++;
			}
			if(checked){
				chkFlag=0;
			}
			if(disabled==false){
				disableFlag=0;
			}
			
		});
		
		if(chkFlag==1){
			alert('No items selected');
			return false;
		}
		else if(disableFlag==1){
			alert('No pending items selected');
			return false;
		}
		else if(flag>0){
			alert('Invalid Qty');
			return false;
		}
		else{
			return true;
		}
}
//-------------------------------------------------------------
function getMenuPermission(approveLevels,inspectionStatus){
		var level=approveLevels+2-inspectionStatus;
		var url 		= basePath+"itemInspection-db.php?requestType=loadPermission";
		var httpobj1 = $.ajax({
			url:url,
			dataType:'json',
			data:"level="+level+"&approveLevels="+approveLevels+"&inspectionStatus="+inspectionStatus,
			async:false,
			success:function(json){
					var permission=json.permission;
					$('#divPermission').html(permission);
			}
		});
		//alert($('#divPermission').html());
	return $('#divPermission').html();	
}
//----------------------------------------------------

function validateQuantities(obj){
	
	var flag=0;
		$('#tblMain .item').each(function(){

			var goodQty=parseFloat($(obj).parent().parent().find('.goodQty').val());
			var dammagedQty=parseFloat($(obj).parent().parent().find('.dammagedQty').val());
			var goodQtySaved=parseFloat($(obj).parent().parent().find('.goodQty').prop('id'));
			var dammagedQtySaved=parseFloat($(obj).parent().parent().find('.dammagedQty').prop('id'));

			if((goodQty!=goodQtySaved) || (dammagedQty!=dammagedQtySaved)){
				flag++;
			}
		});
		
		if(flag>0){
			alert('Save before confirm');
			return false;
		}
		else{
			return true;
		}
}
//-------------------------------------------------------------
function selectGrnNo(grnNo,grnYear){
	loadGRNYears();
	$('#cboGRNYear').val(grnYear);
	loadGRNNos();
	$('#cboGRNNo').val(grnNo);
	$('#frmItemsInspection #cboGRNNo').die('change').live('change');
}
//------------------------------------------------------------
function checkAll(){
	if(document.getElementById('chkAll').checked==true)
	 var chk=true;
	else
	 var chk=false;
	
	var rowCount = document.getElementById('tblMain').rows.length;
	for(var i=2;i<rowCount;i++)
	{
		document.getElementById('tblMain').rows[i].cells[0].childNodes[0].checked=chk;
	}
}

function save(){
	
	$("#frmItemsInspection").validationEngine();

	var requestType = '';
	if ($('#frmItemsInspection').validationEngine('validate'))   
    {  
		if(validatee()){
		showWaiting();

		var data = "requestType=save";
		
			data+="&grnNo="	+	$('#cboGRNNo').val();
			data+="&grnYear="	+	$('#cboGRNYear').val();


			var rowCount = document.getElementById('tblMain').rows.length;
			if(rowCount==2){
				alert("items not selected to Inspect");hideWaiting();
				return false;				
			}
			var row = 0;
			
			var arr="[";
			
			$('#tblMain .item').each(function(){
				
				var checked=$(this).parent().find(".checkItem").prop('checked');
				var disabled=$(this).parent().find(".checkItem").prop('disabled');
 				var itemId	= $(this).prop('id');
				var itemName	= $(this).html();
				var goodQty	= $(this).parent().find(".goodQty").val();
				var dammagedQty	= $(this).parent().find(".dammagedQty ").val();
				var remarks	= $(this).parent().find(".remarks ").val();
					
					if((checked==true) && (disabled==false)){
						if((goodQty>0) || (dammagedQty>0)){
							arr += "{";
							arr += '"itemId":"'+	itemId +'",' ;
							arr += '"itemName":"'+	itemName +'",' ;
							arr += '"goodQty":"'+	goodQty +'",' ;
							arr += '"dammagedQty":"'+	dammagedQty +'",' ;
							arr += '"remarks":"'+  remarks  +'"' ;
							arr +=  '},';
							
						}
					}
			});
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;

		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = basePath+"itemInspection-db.php";
     	var obj = $.ajax({
			url:url,
			type:'post',
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmItemsInspection #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						//var t=setTimeout("alertx()",1000);
						return;
					}
				},
			error:function(xhr,status){
					
					$('#frmItemsInspection #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
			hideWaiting();
	}
	}
	
}

function approveInspections(object){
		
	if(validateQuantities(object)==true){
		var grnNo=$('#cboGRNNo').val();
		var grnYear=$('#cboGRNYear').val();
		var itemId=$(object).parent().parent().find(".item").prop('id');
		var itemName=$(object).parent().parent().find(".item").html();
		var goodQty	= $(object).parent().parent().find(".goodQty").val();
		var dammagedQty	= $(object).parent().parent().find(".dammagedQty ").val();
		
		var val = $.prompt('Are you sure you want to confirm this inspection ?',{
			buttons: { Ok: true, Cancel: false },
			callback: function(v,m,f){
				if(v)
				{
					var data = "grnNo="+grnNo+"&grnYear="+grnYear+"&itemId="+itemId+"&itemName="+itemName+"&goodQty="+goodQty+"&dammagedQty="+dammagedQty+"&requestType=approve";
					var url = basePath+"itemInspection-db.php";
					var obj = $.ajax({
						url:url,
	
						dataType: "json",  
						data:data,
						async:false,
						
						success:function(json){
							$('#frmItemsInspection #butConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertxC()",1000);
									return;
								}
							},
						error:function(xhr,status){
							$(object).parent().parent().find("#butConfirm").validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertxC()",1000);
							}		
						});
				}
			}});
	}
	
}

function viewReport(){
	
	if($('#cboGRNNo').val()!=''){
		window.open('?q='+reportId+'&grnNo='+$('#cboGRNNo').val()+'&year='+$('#cboGRNYear').val());	
	}
	else{
		alert("There is no GRN No to view");
	}
}
	
function confirmRpt(){
	if($('#cboGRNNo').val()!=''){
		window.open('?q='+reportId+'&grnNo='+$('#cboGRNNo').val()+'&year='+$('#cboGRNYear').val()+'&approveMode=1');		
	}
	else{
		alert("There is no GRN No to view");
	}
}

function newPage(){
		window.location.href = "?q=487";
		$('#frmItemsInspection').get(0).reset();
		clearRows();
		$('#frmItemsInspection #txtGrnNo').val('');
		$('#frmItemsInspection #txtGrnYear').val('');
		$('#frmItemsInspection #txtInvioceNo').val('');
		$('#frmItemsInspection #cboPO').val('');
		$('#frmItemsInspection #cboPO').focus();
		$('#frmItemsInspection #txtDeliveryNo').val('');
		$('#frmItemsInspection #txtTotGrnAmmount').val('');
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmItemsInspection #dtDate').val(d);
}
	
  
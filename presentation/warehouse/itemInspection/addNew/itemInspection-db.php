<?php
///////////////////////////////
// H B G KORALA   ////////////
//////////////////////////////
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	$requestType 	= $_REQUEST['requestType'];
	$programName='Item Inspection';
	$programCode='P0487';
	
 	include_once "{$backwardseperator}dataAccess/Connector.php";
	
	require_once $backwardseperator."class/cls_commonFunctions_get.php";
	//include "../../../../class/warehouse/cls_all.php";
	require_once $backwardseperator."class/warehouse/itemInspection/cls_itemInspection_get.php";
	require_once $backwardseperator."class/warehouse/itemInspection/cls_itemInspection_set.php";
	//include "../../../../class/warehouse/cls_warehouse_set.php";
	require_once $backwardseperator."class/warehouse/cls_warehouse_get.php";
	require_once $backwardseperator."class/warehouse/cls_warehouse_set.php";
	
	$objcomfunget= new cls_commonFunctions_get($db);
	$objiteminspectget= new cls_itemInspection_get($db);
	$objiteminspectset= new cls_itemInspection_set($db);
	$objwhouseget= new cls_warehouse_get($db);
	
	//$objwhouseset= new cls_warehouse_set($db);
	
	
	$db->begin();
	if($requestType=='loadGRNYears')
	{ 
		$response['arrCombo'] 	= $objwhouseget->getGRNYears_options();
		echo json_encode($response);
	}
	else if($requestType=='loadGRNNos')
	{ 
		$response['arrCombo'] 	= $objwhouseget->getGRNNosWithInpections_options($_REQUEST['grnYear']);
		echo json_encode($response);
	}
	/*else if($requestType=='loadGRNNos')
	{ 
		$response['arrCombo'] 	= $objwhouseget->getGRNNos_options($_REQUEST['grnYear']);
		echo json_encode($response);
	}*/
	else if($requestType=='loadPermission')
	{
		$response['permission'] 	= $objcomfunget->getPermission($programCode,$userId,$_REQUEST['level'],$_REQUEST['inspectionStatus']);
		echo json_encode($response);
	}
	else if($requestType=='loadGridData')
	{
		$response['arrData'] 	= $objiteminspectget->getDetails($_REQUEST['grnNo'],$_REQUEST['grnYear']);
		echo json_encode($response);
	}
	else if($requestType=='save')
	{
		$grnNo 	= $_REQUEST['grnNo'];
		$grnYear = $_REQUEST['grnYear'];
		
		$rollBackFlag=0;
		
		$grnStatus=$objwhouseget->getGRNStatus($grnNo,$grnYear);
		
		$response['arrData'] = $objwhouseget->getLocationValidation('INSPECT',$location,$grnNo,$grnYear);//check for grn location
		
		if($response['arrData']['type']=='fail'){
			 $rollBackFlag=1;
			 $rollBackMsg=$response['arrData']['msg'];
		 } 
		else if($grnStatus!=1){
			$rollBackFlag=1; 
			$rollBackMsg ='This GRN is not confirmed.So cant Inspect';
		}
		
			if($rollBackFlag!=1){
			$arr 	 = json_decode($_REQUEST['arr'], true);
			$toSave=0;
			$saved=0;
			$rollBackMsg ='Invalid Good Qty or Dammaged Qty for following Items'.'<br>'; 
			$sqlString='';
			foreach($arr as $arrVal)
			{
				$location 	= $_SESSION['CompanyID'];
				$company 	= $_SESSION['headCompanyId'];
				$userId 	= $_SESSION['userId'];
				
				$itemId 	 = $arrVal['itemId'];
				$itemName 	 = $arrVal['itemName'];
				$goodQty 	 = $arrVal['goodQty'];
				$dammagedQty 	 = $arrVal['dammagedQty'];
				$remarks 	 = $arrVal['remarks'];
			 	
			   $grnQty=$objwhouseget->getGRNQty($grnNo,$grnYear,$itemId);
				
					if($grnQty<($goodQty+$dammagedQty)){//change only qty
						$rollBackFlag++;
						$rollBackMsg.=$itemName.'<br>';
					}
					
					if($rollBackFlag==0){
						$toSave++;
						$response['arrData'] = $objiteminspectset->update_ware_grndetails($company,$location,$grnNo,$grnYear,$itemId,$goodQty,$dammagedQty,$remarks,$userId);
						$saved+=$response['arrData']['result'];
						$sqlString.=$response['arrData']['q'];
						$msg.=$response['arrData']['msg'];
					}
				
		
			}//end of foreach
			}
			
			
			if($rollBackFlag==1){
				$db->rollback();
				$response['type'] 		= 'fail';
				$response['msg'] 		= $rollBackMsg;
				$response['q'] 			= '';
			}			
			else if(($toSave==$saved)){
				$db->commit();
				$response['type'] 		= 'pass';
				$response['msg'] 		= 'Saved successfully.';
			}
			else{
				$db->rollback();	
				$response['type'] 		= 'fail';
				//$response['msg'] 		= $db->errormsg;
				$response['msg'] 		= $msg;
				$response['q'] 			= $sqlString;
			}
			echo  json_encode($response);
	}
	else if($requestType=='approve')
	{
		
			$toSave=0;
			$saved=0;
			$rollBackFlag=0;
			$rollBackMsg ='Invalid Good Qty or Dammaged Qty for this Item'.'<br>'; 
			$sqlString='';
			
			$grnNo 	= $_REQUEST['grnNo'];
			$grnYear = $_REQUEST['year'];
			
		 	$response['arrData'] 	= $objiteminspectget->getDetails($grnNo,$grnYear);	
			  $response['arrData']['dammagedQty'];
			
		 $len= count($response['arrData']);
			
			$grnStatus=$objwhouseget->getGRNStatus($grnNo,$grnYear);
			
			 if($grnStatus!=1){
				$rollBackFlag=1; 
				$rollBackMsg ='This GRN is not confirmed.So cant Inspect';
			}
			
			if($rollBackFlag!=1){
				
			//$toSavedQty=0;
			//$transSavedQty=0;
		 	for($i=0; $i<$len; $i++){		

			$itemId 	 = $response['arrData'][$i]['intItemId'];
			$itemName 	 = $response['arrData'][$i]['itemName'];
			$goodQty 	 = $response['arrData'][$i]['goodQty'];
			$dammagedQty 	 = $response['arrData'][$i]['dammagedQty'];
			$grnQty 	 = $response['arrData'][$i]['grnQty'];
			
			//$toSavedQty+=$goodQty+$dammagedQty;
			
			$inspectQty =$objwhouseget->getGRNinspectedGoodQty($grnNo,$grnYear,$itemId);
			$trnsQty =$objwhouseget->getStockInspectedGoodQty($grnNo,$grnYear,$itemId);
			$saveQty =$inspectQty;
			

			//$grnQty=$objwhouseget->getGRNQty($grnNo,$grnYear,$itemId);	
			if($grnQty<($goodQty+$dammagedQty)){//change only qty
				$rollBackFlag++;
				$rollBackMsg.="Item-".$itemName." grnQty-".$grnQty." good Qty-".$goodQty." Dammage Qty-".$dammagedQty.'<br>';
			}
			
			
			if($saveQty!=0){
			//	$transSavedQty+=$saveQty;
			if($rollBackFlag==0){
				
				$toSave++;
				$response['arrData'] 	= $objiteminspectset->confirmItemInspection($company,$location,$grnNo,$grnYear,$itemId,$userId);
				$saved+=$response['arrData']['result'];
				$sqlString.=$response['arrData']['q'];
				$msg.=$response['arrData']['msg'];
				
			}
			}
			
		 	}//end of for 
			}
						
			if($rollBackFlag==1){
				$db->rollback();
				$response['type'] 		= 'fail';
				$response['msg'] 		= $rollBackMsg;
				$response['q'] 			= '';
			}			
			else if(($toSave==$saved)){
				$db->commit();
				$response['type'] 		= 'pass';
				$response['msg'] 		= 'Confirmed successfully.';
			}
			else{
				$db->rollback();
				$response['type'] 		= 'fail';
				//$response['msg'] 		= $db->errormsg;
				$response['msg'] 		= $msg;
				$response['q'] 			= $sqlString;
			}
			echo  json_encode($response);
	}
	
	
	$db->commit();
	
?>
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
// JavaScript Document
var basePath = "presentation/warehouse/itemInspection/listing/";

$(document).ready(function() {
	$('#frmItemInspectionReport').validationEngine();
	
	$('#frmItemInspectionReport #imgApprove').click(function(){
	var val = $.prompt('Are you sure you want to approve this Inspection ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					//	if(validateQuantities()==0){
						var url = basePath+"../addNew/itemInspection-db.php"+window.location.search+'&requestType=approve';
						var obj = $.ajax({url:url,async:false});
						window.location.href = window.location.href;
						window.opener.location.reload();//reload listing page
						//}
					}
				}});
	});
	
	
});
//---------------------------------------------------
function validateQuantities(){
		var ret=0;	
	    var issueNo = document.getElementById('divIssueNo').innerHTML;
		var url 		= basePath+"rptIssue-db-get.php?requestType=getValidation";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"issueNo="+issueNo,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#imgApprove').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertx()",3000);
					  ret= 1;
					}
				}
			});
			return ret;
}
//------------------------------------------------
function alertx()
{
	$('#frmIssueReport #imgApprove').validationEngine('hide')	;
}
//--------------------------------------------
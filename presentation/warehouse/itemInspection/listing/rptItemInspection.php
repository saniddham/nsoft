<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
//////////////////////////////////////////
//status=-1  cancelled
//status=1  confirmed
//status = 0 not inspected
//status>1   pending
//////////////////////////////////////////
 $companyId = $_SESSION['CompanyID'];
$intUser  = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
 
$grnNo = $_REQUEST['grnNo'];
$year = $_REQUEST['year'];
$approveMode = $_REQUEST['approveMode'];

$programName='Item Inspection';
$programCode='P0487';

$confirmationMode=loadConfirmatonMode($grnNo,$year,$programCode,$intUser);

	$sql = "SELECT
			ware_grndetails.intInspected, 
			ware_grndetails.intInspectedAppLevels , 
			ware_grnheader.intPoNo,
			ware_grnheader.intPoYear,
			ware_grnheader.datdate as grnDate   
			FROM
			ware_grndetails 
			Inner Join ware_grnheader ON ware_grndetails.intGrnNo = ware_grnheader.intGrnNo AND ware_grndetails.intGrnYear = ware_grnheader.intGrnYear 
			WHERE
			ware_grndetails.intGrnNo =  '$grnNo' AND
			ware_grndetails.intGrnYear =  '$year' AND
			ware_grndetails.intInspectionItem =  '1'";	
	
	 $result = $db->RunQuery($sql);
	 $row=mysqli_fetch_array($result);
	 $intStatus=$row['intInspected'];
	 $savedLevels=$row['intInspectedAppLevels'];
	 $grnDate=$row['grnDate'];
	 $locationId=$_SESSION['CompanyID'];
	 $poNo = $row['intPoNo'];
	 $poYear = $row['intPoYear'];
?>
 <head>
 <title>Item Inspection Report</title>
 
<script type="application/javascript" src="presentation/warehouse/itemInspection/listing/rptItemInspection-js.js"></script>
 <style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:260px;
	top:169px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<form id="frmItemInspectionReport" name="frmItemInspectionReport" method="post" action="frmItemInspectionReport.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include 'reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>ITEM INSPECTION REPORT</strong><strong></strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
    <?php
			if(($confirmationMode==2) && ($approveMode==1)) { 
			?>
              <tr>
                <td colspan="9" align="center" bgcolor="#FFDFCB">
			<img src="images/approve.png" align="middle" class="mouseover noPrint" id="imgApprove" />
                </td>
              </tr>
			<?php
			}
			else if($confirmationMode==1) { 
	?>
          <tr>
           <td colspan="9" class="APPROVE" >CONFIRMED</td>
          </tr>
			<?php
			}
			else 
			{
	?>
          <tr>
           <td colspan="9" class="APPROVE" >PENDING</td>
          </tr>
			<?php
			}
	?>
<tr>
    <td width="56" ><span class="normalfnt"><strong>GRN No</strong></span></td>
    <td width="5"  align="center" valign="middle"><strong>:</strong></td>
    <td width="122" ><span class="normalfnt"><div id="divGrnNo"><?php echo $grnNo ?>/<?php echo $year ?></div></span></td>
    <td width="43" class="normalfnt"><strong>PO NO</strong></td>
    <td width="5" align="center" valign="middle"><strong>:</strong></td>
    <td width="539" ><span class="normalfnt"><?php echo $poNo ?>/<?php echo $poYear ?></span></td>
    <td width="30" ><div id="divPoNo" style="display:none"><?php echo $poNo ?>/<?php echo $year ?></div></td>
  <td width="30" ></td>
    <td width="32" >&nbsp;</td>
  </tr>    
<tr>
  <td colspan="9">
    <table width="100%">
      <tr>
        <td width="5%">&nbsp;</td>
        <td colspan="7" class="normalfnt">
          <table width="100%" class="grid" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr class="gridHeader">
              <td width="13%" rowspan="2" >Main Category</td>
              <td width="11%" rowspan="2" >Sub Category</td>
              <td width="7%" rowspan="2" >Item Code</td>
              <td width="18%" rowspan="2" >Item</td>
              <td width="5%" rowspan="2" >UOM</td>
              <td width="6%" rowspan="2" >GRN Qty</td>
              <td width="6%" rowspan="2" >Pending To Inspect</td>
              <td colspan="2" >Pending To Confirm</td>
              <td colspan="2" >Inspection Confirmed</td>
              <td width="12%" rowspan="2" >Inspect Status</td>
              <td width="12%" rowspan="2" >Remarks</td>
              </tr>
            <tr class="gridHeader">
              <td >Good</td>
              <td >Damaged</td>
              <td width="6%" >Good</td>
              <td width="8%" >Damaged</td>
            </tr>
            <?php 
		
		     $sql="SELECT DISTINCT
				mst_item.intMainCategory,
				mst_maincategory.strName as mainCatName,
				mst_item.intSubCategory,
				mst_subcategory.strName as subCatName,
				tb1.intItemId,
				mst_item.strCode,
                mst_item.strCode as SUP_ITEM_CODE,
				mst_item.strName as itemName,
				tb1.dblInspectedGoodQty as goodQty,
				tb1.dblInspectedDammagedQty as dammagedQty,
				tb1.dblGrnQty as grnQty, 
				tb1.dblRetunSupplierQty as retQty, 
				tb1.dblConfirmedInspectQty as confQty, 
				IFNULL((select sum(dblQty) from ware_stocktransactions_bulk where ware_stocktransactions_bulk.strType='GRN' AND ware_stocktransactions_bulk.intDocumentNo=tb1.intGrnNo AND ware_stocktransactions_bulk.intDocumntYear=tb1.intGrnYear AND ware_stocktransactions_bulk.intItemId=tb1.intItemId ),0) as stockQty, 
				tb1.intInspected,  
				tb1.intInspectionItem, 
				tb1.strInspectionRemarks , 
				mst_units.strCode as uom 
				FROM
				ware_grndetails as tb1 
				Inner Join mst_item ON tb1.intItemId = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
                                LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
				WHERE 
				tb1.intGrnNo =  '$grnNo' AND
				tb1.intGrnYear =  '$year'";
			$result = $db->RunQuery($sql);
			$totQty=0;
			$totAmmount=0;
			$TPI=0;
			$TPCG=0;
			$TPCD=0;
			$TCG=0;
			$TCD=0;
			while($row=mysqli_fetch_array($result))
			{	
			
				$confGood=$row['stockQty'];
				$confDmg=$row['confQty']-$row['stockQty'];
				
				$pendingGoodQty=$row['goodQty'];
				$pendingDammagedQty=$row['dammagedQty'];
				
				$pendingInsp=$row['grnQty']-$row['confQty'];
				
				$inspectStatus="";
				if($row['intInspectionItem']==1){
					if(($row['grnQty']-$row['retQty'])==($row['confQty'])){
						$inspectStatus="confirmed";
						$color="#00A6A6";
					}
					else{
						$inspectStatus="pending";
						$color="#9D0000";
					}
					
					$TCG+=$confGood;
					$TCD+=$confDmg;
					$TPCG+=$pendingGoodQty;
					$TPCD+=$pendingDammagedQty;
					$TPI +=$pendingInsp;
				}
				else{
						$goodQty='';
						$dammagedQty='';
						$pendingInsp='';
						$pendingGoodQty='';
						$pendingDammagedQty='';
						$confGood='';
						$confDmg='';
				} 
				$remarks	=$row['strInspectionRemarks'];	
	  ?>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfnt" >&nbsp;<?php echo $row['mainCatName']?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['subCatName'] ?>&nbsp;</td>
              <?php
              if($row['SUP_ITEM_CODE']!=null)
              {
               ?>
              <td class="normalfnt" >&nbsp;<?php echo $row['SUP_ITEM_CODE'] ?>&nbsp;</td>
              <?php
              }else{
                  
              ?>
              <td class="normalfnt" >&nbsp;<?php echo $row['strCode'] ?>&nbsp;</td>
              <?php
              }
              ?>
              <td class="normalfnt" >&nbsp;<?php echo $row['itemName'] ?>&nbsp;</td>
              <td class="normalfntMid" ><?php echo $row['uom'] ?></td>
              <td class="normalfntRight" ><?php echo $row['grnQty'] ?></td>
              <td class="normalfntRight" ><?php echo $pendingInsp ?></td>
              <td width="6%" class="normalfntRight" ><?php echo $pendingGoodQty ?></td>
              <td width="8%" class="normalfntRight" >&nbsp;<?php echo $pendingDammagedQty ?>&nbsp;</td>
              <td class="normalfntRight"><?php echo $confGood ?></td>
              <td class="normalfntRight"><?php echo $confDmg ?></td>
              <td class="normalfnt" align="center"><div style="color:<?php echo $color;?>; text-align:center"><?php echo $inspectStatus ?></div></td>
              <td class="normalfnt" align="center"><?php echo $remarks ?></td>
              </tr>
            <?php 
			$totGrnQty+=$row['grnQty'];
			$totGoodQty+=$row['goodQty'];
			$toDmgQty+=$row['dammagedQty'];
			
			}
	  ?>
            <tr class="normalfnt"  bgcolor="#CCCCCC">
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfntRight" >&nbsp;</td>
              <td class="normalfntRight" ><?php echo $totGrnQty; ?></td>
              <td class="normalfntRight" ><?php echo $TPI; ?></td>
              <td class="normalfntRight" ><?php echo $TPCG; ?></td>
              <td class="normalfntRight" ><?php echo $TPCD; ?></td>
              <td class="normalfntRight" ><?php echo $TCG ?></td>
              <td class="normalfntRight" ><?php echo $TCD ?></td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              </tr>
            </table>
          </td>
        <td width="6%">&nbsp;</td>
        </tr>
      
      </table>
    </td>
</tr>
<?php  
 	if($intStatus!=0)
	{

				for($i=1; $i<=$savedLevels; $i++)
				{
					  $sqlc = "SELECT
							ware_iteminspection_approvedby.intApproveUser,
							ware_iteminspection_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName,
							ware_iteminspection_approvedby.intApproveLevelNo
							FROM
							ware_iteminspection_approvedby
							Inner Join sys_users ON ware_iteminspection_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							ware_iteminspection_approvedby.intGrnNo =  '$grnNo' AND
							ware_iteminspection_approvedby.intYear =  '$year'  AND
							ware_iteminspection_approvedby.intApproveLevelNo =  '$i'  order by intApproveLevelNo asc
";
					 $resultc = $db->RunQuery($sqlc);
					 $rowc=mysqli_fetch_array($resultc);
						if($i==1)
						$desc="1st ";
						else if($i==2)
						$desc="2nd ";
						else if($i==3)
						$desc="3rd ";
						else
						$desc=$i."th ";
					 //  $desc=$ap.$desc;
					 $desc2=$rowc['UserName']."(".$rowc['dtApprovedDate'].")";
					 if($rowc['UserName']=='')
					 $desc2='---------------------------------';
				?>
      <?php
			}
	}
	else{
					 $sqlc = "SELECT
							ware_iteminspection_approvedby.intApproveUser,
							ware_iteminspection_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName,
							ware_iteminspection_approvedby.intApproveLevelNo
							FROM
							ware_iteminspection_approvedby
							Inner Join sys_users ON ware_iteminspection_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							ware_iteminspection_approvedby.intGrnNo =  '$grnNo' AND
							ware_iteminspection_approvedby.intYear =  '$year'  AND
							ware_iteminspection_approvedby.intApproveLevelNo =  '-1'";
					 $resultc = $db->RunQuery($sqlc);
					 $rowc=mysqli_fetch_array($resultc);
					  ?>
      <?php
	}
?>
<tr height="40">
  <td align="center" class="normalfntMid" colspan="9"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
</body>
</html>
<?php
function loadConfirmatonMode($grnNo,$year,$programCode,$intUser){
	global $db;

$sql = "SELECT DISTINCT 
							if(((IFNULL((SELECT
							Sum(ware_grndetails.dblGrnQty-ware_grndetails.dblRetunSupplierQty)
							FROM
							ware_grndetails
							WHERE ware_grndetails.intGrnNo = tb2.intGrnNo AND tb2.intGrnYear = ware_grndetails.intGrnYear AND ware_grndetails.intInspectionItem = 1),0))<=(IFNULL((SELECT
							Sum(ware_grndetails.dblConfirmedInspectQty)
							FROM
							ware_grndetails
							WHERE ware_grndetails.intGrnNo = tb2.intGrnNo AND tb2.intGrnYear = ware_grndetails.intGrnYear AND ware_grndetails.intInspectionItem = 1),0))),'1','0') as Status1, 
			
							if(((IFNULL((SELECT
							Sum(ware_grndetails.dblInspectedGoodQty+ware_grndetails.dblInspectedDammagedQty)
							FROM
							ware_grndetails
							WHERE ware_grndetails.intGrnNo = tb2.intGrnNo AND tb2.intGrnYear = ware_grndetails.intGrnYear AND ware_grndetails.intInspectionItem = 1),0))=0),'1','0') as Status 
							
													FROM
							ware_grnheader as tb1  
							Inner Join ware_grndetails as tb2 ON tb1.intGrnNo = tb2.intGrnNo AND tb1.intGrnYear = tb2.intGrnYear
							Inner Join trn_poheader ON tb1.intPoNo = trn_poheader.intPONo AND tb1.intPoYear = trn_poheader.intPOYear
							Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
							Inner Join sys_users ON tb1.intUser = sys_users.intUserId 
							Inner Join mst_locations ON tb1.intCompanyId = mst_locations.intId 
							WHERE 
							tb1.intStatus =  '1' AND  
							tb2.intInspectionItem =  '1' AND 
							tb1.intGrnNo= '$grnNo' AND 
							tb1.intGrnYear= '$year' group by  
							tb1.intGrnNo, 
							tb1.intGrnYear
							";

		 $result = $db->RunQuery($sql);
		 $row=mysqli_fetch_array($result);
			 
	
	
	$sqlp = "SELECT
		menupermision.int1Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	
	if(($row['Status']==1) && ($row['Status1']==1)){
	 $confirmatonMode=1;
	}
	else if(($rowp['int1Approval']==1) and ($row['Status']==0)){
	 $confirmatonMode=2;
	}
	else{
	 $confirmatonMode=3;
	}
	
	return $confirmatonMode;
}

	
?>
<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
date_default_timezone_set('Asia/Kolkata');
//ini_set('allow_url_include',1);
// ini_set('display_errors',1);
 
require_once	"libraries/jqgrid2/inc/jqgrid_dist.php";

$company 	= $_SESSION['headCompanyId'];
$location = $_SESSION['CompanyID'];

$programName='Item Inspection';
$programCode='P0487';

$reportMenuId	='921';
$menuId			='487';



$intUser  = $_SESSION["userId"];

$userDepartment=getUserDepartment($intUser);
$approveLevel = (int)getMaxApproveLevel();

$sql = "select * from(SELECT DISTINCT 
							if(((IFNULL((SELECT
							Sum(ware_grndetails.dblGrnQty-ware_grndetails.dblRetunSupplierQty)
							FROM
							ware_grndetails
							WHERE ware_grndetails.intGrnNo = tb2.intGrnNo AND tb2.intGrnYear = ware_grndetails.intGrnYear AND tb2.intItemId = ware_grndetails.intItemId),0))<=(IFNULL((SELECT
							Sum(ware_grndetails.dblConfirmedInspectQty)
							FROM
							ware_grndetails
							WHERE ware_grndetails.intGrnNo = tb2.intGrnNo AND tb2.intGrnYear = ware_grndetails.intGrnYear AND tb2.intItemId = ware_grndetails.intItemId),0))),'Approved','Pending') as Status,
							
							IFNULL((SELECT
							Sum(ware_grndetails.intInspectionItem)
							FROM
							ware_grndetails
							WHERE ware_grndetails.intGrnNo = tb2.intGrnNo AND tb2.intGrnYear = ware_grndetails.intGrnYear),0) as `itemsToInspect`,
							
							IFNULL((SELECT
							Sum(ware_grndetails.intInspected)
							FROM
							ware_grndetails
							WHERE ware_grndetails.intGrnNo = tb2.intGrnNo AND tb2.intGrnYear = ware_grndetails.intGrnYear),0) as `itemsInspected`,
							
							IFNULL((SELECT
							Sum(ware_grndetails.dblGrnQty-ware_grndetails.dblRetunSupplierQty)
							FROM
							ware_grndetails
							WHERE ware_grndetails.intGrnNo = tb2.intGrnNo AND tb2.intGrnYear = ware_grndetails.intGrnYear AND ware_grndetails.intInspectionItem=1 ),0) as `GRN_Qty`,
							
							IFNULL((SELECT
							Sum(ware_grndetails.dblInspectedGoodQty+ware_grndetails.dblInspectedDammagedQty)
							FROM
							ware_grndetails
							WHERE ware_grndetails.intGrnNo = tb2.intGrnNo AND tb2.intGrnYear = ware_grndetails.intGrnYear AND ware_grndetails.intInspectionItem=1 ),0) as `inspectedQty`,
							
							tb1.intGrnNo as `GRN_No`,
							tb1.intGrnYear as `GRN_Year`,
							tb1.intPoNo as `PO_No`,
							tb1.intPoYear as `PO_Year`,
							tb1.strInvoiceNo as `Invoice_No`,
							mst_supplier.strName as `Supplier`,
							tb1.datdate as `Date`,
							
							  
							sys_users.strUserName as `User`, 
							  
							if(((IFNULL((SELECT
							Sum(ware_grndetails.intInspectionItem)
							FROM
							ware_grndetails
							WHERE ware_grndetails.intGrnNo = tb2.intGrnNo AND tb2.intGrnYear = ware_grndetails.intGrnYear),0))<=(IFNULL((SELECT
							Sum(ware_grndetails.intInspected)
							FROM
							ware_grndetails
							WHERE ware_grndetails.intGrnNo = tb2.intGrnNo AND tb2.intGrnYear = ware_grndetails.intGrnYear),0))) AND ((IFNULL((SELECT
							Sum(ware_grndetails.dblGrnQty-ware_grndetails.dblRetunSupplierQty)
							FROM
							ware_grndetails
							WHERE ware_grndetails.intGrnNo = tb2.intGrnNo AND tb2.intGrnYear = ware_grndetails.intGrnYear AND tb2.intItemId = ware_grndetails.intItemId),0))<=(IFNULL((SELECT
							Sum(ware_grndetails.dblInspectedGoodQty+ware_grndetails.dblInspectedDammagedQty)
							FROM
							ware_grndetails
							WHERE ware_grndetails.intGrnNo = tb2.intGrnNo AND tb2.intGrnYear = ware_grndetails.intGrnYear AND tb2.intItemId = ware_grndetails.intItemId),0))),'Approved',if((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1,'Approve','Pending')) as 1st_Approval,  ";
							
								
							$sql .= "'View' as `View`   
						FROM
							ware_grnheader as tb1  
							Inner Join ware_grndetails as tb2 ON tb1.intGrnNo = tb2.intGrnNo AND tb1.intGrnYear = tb2.intGrnYear
							Inner Join trn_poheader ON tb1.intPoNo = trn_poheader.intPONo AND tb1.intPoYear = trn_poheader.intPOYear
							Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
							Inner Join sys_users ON tb1.intUser = sys_users.intUserId 
							Inner Join mst_locations ON tb1.intCompanyId = mst_locations.intId 
							WHERE 
							tb1.intStatus =  '1' AND  
							tb2.intInspectionItem =  '1' AND 
							mst_locations.intCompanyId =  '$company' AND 
							tb1.intCompanyId =  '$location'
							)  as t where 1=1
						";
					  	// echo $sql;
						
$formLink			= "?q=$menuId&grnNo={GRN_No}&year={GRN_Year}";	 
$reportLink  		= "?q=$reportMenuId&grnNo={GRN_No}&year={GRN_Year}";
$reportLinkApprove  = "?q=$reportMenuId&grnNo={GRN_No}&year={GRN_Year}&approveMode=1";
$reportLinkPrint  	= "?q=$reportMenuId&grnNo={GRN_No}&year={GRN_Year}&mode=print";
						 
$col = array();

//STATUS
$col["title"] 	= "Status"; // caption of column
$col["name"] 	= "Status"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
//edittype
$col["stype"] 	= "select";
$str = ":All;Pending:Pending;Approved:Approved;Rejected:Rejected" ;
$col["editoptions"] 	=  array("value"=> $str);
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$cols[] = $col;	$col=NULL;

//PO No
$col["title"] 	= "GRN No"; // caption of column
$col["name"] 	= "GRN_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "2";
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$col['link']	= $formLink;	 
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
 
$cols[] = $col;	$col=NULL;



//PO Year
$col["title"] = "GRN Year"; // caption of column
$col["name"] = "GRN_Year"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//PO Year
$col["title"] = "PO No"; // caption of column
$col["name"] = "PO_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;



//PO Year
$col["title"] = "PO Year"; // caption of column
$col["name"] = "PO_Year"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//Delivery Date
$col["title"] = "Invoice No"; // caption of column
$col["name"] = "Invoice_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "left";

$cols[] = $col;	$col=NULL;



//Delivery Date
$col["title"] = "Supplier"; // caption of column
$col["name"] = "Supplier"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;


//Supplier
$col["title"] = "Date"; // caption of column
$col["name"] = "Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//GRN Qty
$col["title"] = "GRN Qty"; // caption of column
$col["name"] = "GRN_Qty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//GRN Qty
$col["title"] = "Inspected Qty"; // caption of column
$col["name"] = "inspectedQty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;


//User
$col["title"] = "User"; // caption of column
$col["name"] = "User"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;



//FIRST APPROVAL
$col["title"] = "Approval"; // caption of column
$col["name"] = "1st_Approval"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;



//VIEW
$col["title"] = "Report"; // caption of column
$col["name"] = "View"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

$jq = new jqgrid('',$db);

$grid["caption"] 		= "Item Inspection Listing";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'GRN_No'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes


// export XLS file
// export to excel parameters - range could be "all" or "filtered"
//$grid["export"] = array("format"=>"xlsx", "filename"=>"my-file", "sheetname"=>"test");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);




$out = $jq->render("list1");
?>
<title>Item Inspection Listing</title>
<form id="frmlisting" name="frmlisting" method="post" action="">

        <div align="center" style="margin:10px">        
			<?php echo $out?>
        </div>
</form>

<?php

//------------------------------function load Default Department---------------------
function getMaxApproveLevel(){
	global $db;
	
	//echo $savedStat;
	$appLevel=0;
	$sqlp = "SELECT
			Max(ware_grnheader.intApproveLevels) AS appLevel
			FROM ware_grnheader";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['appLevel'];
}
//------------------------------function load User Department---------------------
function getUserDepartment($intUser){
	global $db;

	$sqlp = "SELECT
			sys_users.intDepartmentId
			FROM
			sys_users
			WHERE
			sys_users.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['intDepartmentId'];

}
?>


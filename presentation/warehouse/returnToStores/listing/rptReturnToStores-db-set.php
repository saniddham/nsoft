<?php 
//error_reporting(E_ALL);
ini_set('display_errors', 0);
//////////////////////////////////////////////
//Create By:H.B.G Korala
//note://if first confirmation raised,dblIssudQty qty field in ware_mrndetails table shld update.
//note://if final confirmation raised,insert into stocktransaction table.
//note://if final confirmation raised,update budget plan details.(2014-05-08)
//////////////////////////////////////////////
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];

	include "{$backwardseperator}dataAccess/Connector.php";
	include "{$backwardseperator}libraries/mail/mail.php";
	include "../../../../class/finance/supplier/cls_issuing_chartofaccount_set.php";
	require_once $backwardseperator."class/cls_mail.php";
 	require_once "../../../../class/cls_commonFunctions_get.php";
	require_once "../../../../class/finance/budget/cls_common_function_budget_get.php";
	require_once "../../../../class/finance/budget/entering/cls_budget_get.php";
	require_once "../../../../class/finance/budget/entering/cls_budget_set.php";
	require_once "../../../../class/warehouse/returnToStores/cls_returnToStores_get.php";
	require_once "../../../../class/warehouse/cls_warehouse_get.php";
    require_once $_SESSION['ROOT_PATH']."class/common/azureDBconnection.php";
	
	$obj_chartOfAccount_set	= new Cls_Issuing_ChartofAccount_Set($db);
	$objMail 				= new cls_create_mail($db);
	$obj_returnToStores_get = new cls_returnToStores_get($db);
	$obj_common				= new cls_commonFunctions_get($db);
	$obj_comm_budget_get	= new cls_common_function_budget_get($db);
	$obj_budget_get			= new cls_budget_get($db);
	$obj_budget_set			= new cls_budget_set($db);
	$objwhouseget = new cls_warehouse_get($db);
    $objAzure = new cls_azureDBconnection();
	
	//GatePass Approve Levels for not stock maintain locations
	
	$programNameGate='Gate Pass';
    $programCodeGate='P0250';
   $ApproveLevels = (int)getApproveLevel($programNameGate);

   $gatePassApproveLevel = $ApproveLevels+1;

   //GatePass TransferIN Levels for not stock maintain locations
	
       $programNameTransfer='GatePass Transfer In';
		$programCode='P0252';

		$ApproveLevelsTransfer = (int)getApproveLevel($programNameTransfer);
		$transfApproveLevel = $ApproveLevelsTransfer+1;

	//-----------------------------
	
	
	$programName='Return to Stores';
	$programCode='P0246';
	
	$returnNo = $_REQUEST['returnNo'];
	$year = $_REQUEST['year'];
	
	if($_REQUEST['status']=='approve')
	{
		$currentLocation = $location;
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		$rollBackFlag=0;
		$rollBackMsg = '';
		$mrnType = 2;
		$sql = "UPDATE `ware_returntostoresheader` SET `intStatus`=intStatus-1 WHERE (intReturnNo='$returnNo') AND (`intReturnYear`='$year') and intStatus not in(0,1)";
		$result = $db->RunQuery2($sql);
		if((!$result) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sql;
			$rollBackMsg = "Approval error!";
		}

		if($result){
			$sql = "SELECT ware_returntostoresheader.intUser, 
			ware_returntostoresheader.intStatus, 
			ware_returntostoresheader.intApproveLevels, 
			ware_returntostoresheader.intCompanyId AS location, 
			mst_locations.intCompanyId AS companyId
			 FROM ware_returntostoresheader 
			Inner Join mst_locations ON ware_returntostoresheader.intCompanyId = mst_locations.intId
			 WHERE (intReturnNo='$returnNo') AND (`intReturnYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$createdUser = $row['intUser'];
			$status = $row['intStatus'];
			$savedLevels = $row['intApproveLevels'];
			$location 	= $row['location'];
			$company = $row['companyId'];
			
			$approveLevel=$savedLevels+1-$status;
			$sqlI = "INSERT INTO `ware_returntostoresheader_approvedby` (`intReturnNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
				VALUES ('$returnNo','$year','$approveLevel','$userId',now(),0)";
			$resultI = $db->RunQuery2($sqlI);
					
			if((!$resultI) && ($rollBackFlag!=1)){
				$rollBackFlag=1;
				$sqlM=$sqlI;
				$rollBackMsg = "Approval error!";
			}
			
			if($status==1)
			{
				if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
				////////////// send mail to creater //////////////////
				sendFinlConfirmedEmailToUser($returnNo,$year,$objMail,$mainPath,$root_path);
				}
			}

		//--------------------------------------------------------------------
		if($status==1){//if final confirmation raised,insert to stock
				$sql1 = "SELECT 
						ware_returntostoresdetails.strOrderNo,
						ware_returntostoresdetails.intOrderYear, 
						ware_returntostoresdetails.strStyleNo,
						trn_orderdetails.strSalesOrderNo,
						trn_orderdetails.intSalesOrderId,
 						ware_returntostoresdetails.dblQty,
						ware_returntostoresdetails.intItemId,   
						ware_returntostoresheader.intIssueNo,
						ware_returntostoresheader.intIssueYear, 
						mst_item.intBomItem,
						ware_issueheader.intCompanyId						
						FROM 
						ware_returntostoresdetails 
						left Join trn_orderdetails ON ware_returntostoresdetails.strOrderNo = trn_orderdetails.intOrderNo AND ware_returntostoresdetails.intOrderYear = trn_orderdetails.intOrderYear AND ware_returntostoresdetails.strStyleNo = trn_orderdetails.intSalesOrderId
						Inner Join mst_item ON ware_returntostoresdetails.intItemId = mst_item.intId 
						Inner Join ware_returntostoresheader ON ware_returntostoresdetails.intReturnNo = ware_returntostoresheader.intReturnNo AND ware_returntostoresdetails.intReturnYear = ware_returntostoresheader.intReturnYear
						Inner Join ware_issueheader ON ware_returntostoresheader.intIssueNo = ware_issueheader.intIssueNo AND ware_returntostoresheader.intIssueYear = ware_issueheader.intIssueYear
						WHERE
						ware_returntostoresdetails.intReturnNo =  '$returnNo' AND
						ware_returntostoresdetails.intReturnYear =  '$year'";
				$result1 = $db->RunQuery2($sql1);
				$toSavedQty=0;
				$transSavedQty=0;
				while($row=mysqli_fetch_array($result1))
				{
					$toSavedQty+=round($row['dblQty'],4);
					$issueNo=$row['intIssueNo'];
					$issueYear=$row['intIssueYear'];
					$item=$row['intItemId'];
					$Qty=$row['dblQty'];
					$orderNo=$row['strOrderNo'];
					$orderYear=$row['intOrderYear'];
					$salesOrderNo=$row['strSalesOrderNo'];
					$salesOrderId=$row['intSalesOrderId'];
					$styleNo=$row['strStyleNo'];
					$toIssueLocation=$row['intCompanyId'];
					$gpFromLocation=$row['intCompanyId'];
					$gpToLocation=$_SESSION['CompanyID'];
			
					if($orderNo!=0){//bom item--get grn number wise issue ammounts
						//$resultG = getGrnWiseStockBalance($location,$issueNo,$issueYear,$orderNo,$orderYear,$salesOrderId,$item);
						$resultG = getGrnWiseStockBalance_bulk($toIssueLocation,$issueNo,$issueYear,$item,'ISSUE');
						while($rowG=mysqli_fetch_array($resultG)){
							
								if(($Qty>0) && (($rowG['issueQty']*(-1))>0)){
									if($Qty<=$rowG['issueQty']*(-1)){
									$saveQty=$Qty;
									$Qty=0;
									}
									else if($Qty>$rowG['issueQty']*(-1)){
									$saveQty=$rowG['issueQty']*(-1);
									$Qty=$Qty-$saveQty;
									}
									$grnNo=$rowG['intGRNNo'];
									$grnYear=$rowG['intGRNYear'];
									$grnDate=$rowG['dtGRNDate'];
									$grnRate=$rowG['dblGRNRate'];
									$currency=loadCurrency($grnNo,$grnYear,$item);
									$saveQty=round($saveQty,4);	
									if($saveQty>0){
									
									$sqlI = "INSERT INTO `ware_stocktransactions` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intOrderNo,intOrderYear,intSalesOrderId,intItemId,dblQty,strType,intUser,dtDate) 
										VALUES ('$company','$location','$returnNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$orderNo','$orderYear','$styleNo','$item','$saveQty','RETSTORES','$userId',now())";
									$sqlII = "INSERT INTO `ware_stocktransactions` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intOrderNo,intOrderYear,intSalesOrderId,intItemId,dblQty,strType,intUser,dtDate) 
									VALUES ('$company','$location','$returnNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$orderNo','$orderYear','$styleNo','$item','-$saveQty','RETSTORES','$userId',now())";
									$sqlIII = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intOrderNo,intOrderYear,intSalesOrderId,intUser,dtDate) 
										VALUES ('$company','$toIssueLocation','$returnNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$item','$saveQty','RETSTORES','$orderNo','$orderYear','$styleNo','$userId',now())";	
										
									$resultI = $db->RunQuery2($sqlI);
									$resultII = $db->RunQuery2($sqlII);
									$resultIII = $db->RunQuery2($sqlIII);
									
									if($resultI&&$resultII&&$resultIII){
										$transSavedQty+=$saveQty;
									}
								
									if((!$resultI) && (!$resultII) && (!$resultIII) && ($rollBackFlag!=1)){
										$rollBackFlag=1;
										$sqlM=$sqlI;
										$rollBackMsg = "Approval error!";
									}
									
									}
								}
						}
					}
					else{//general item--get grn number wise issue ammounts
						$resultG = getGrnWiseStockBalance_bulk($toIssueLocation,$issueNo,$issueYear,$item,'ISSUE');

						while($rowG=mysqli_fetch_array($resultG)){
								if(($Qty>0) && (($rowG['issueQty']*(-1))>0)){
									if($Qty<=$rowG['issueQty']*(-1)){
									$saveQty=$Qty;
									$Qty=0;
									}
									else if($Qty>$rowG['issueQty']*(-1)){
									$saveQty=$rowG['issueQty']*(-1);
									$Qty=$Qty-$saveQty;
									}
									$grnNo=$rowG['intGRNNo'];
									$grnYear=$rowG['intGRNYear'];
									$grnDate=$rowG['dtGRNDate'];
									$grnRate=$rowG['dblGRNRate'];	
									$currency=loadCurrency($grnNo,$grnYear,$item);
									$saveQty=round($saveQty,4);	
									if($saveQty>0){
									$sqlI = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intUser,dtDate) 
										VALUES ('$company','$toIssueLocation','$returnNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$item','$saveQty','RETSTORES','$userId',now())";
									$resultI = $db->RunQuery2($sqlI);
									
									if($resultI){
										$transSavedQty+=$saveQty;
									}
									if((!$resultI) && ($rollBackFlag!=1)){
										$rollBackFlag=1;
										$sqlM=$sqlI;
										$rollBackMsg = "Approval error!";
									}
									
									}
								}
						}
					}
				}
			}
		//--------------------------------------------------------------------
			if($status==$savedLevels){//if first confirmation raised,grn qty field in po table shld update
				$sql1 = "SELECT
						ware_returntostoresdetails.intMrnNo,
						ware_returntostoresdetails.intMrnYear,
						ware_returntostoresdetails.strOrderNo,
						ware_returntostoresdetails.intOrderYear,
						ware_returntostoresdetails.strStyleNo,
						ware_returntostoresdetails.intItemId,
						ware_returntostoresheader.intIssueNo,
						ware_returntostoresheader.intIssueYear,
						ware_mrnheader.mrnType,
						ware_returntostoresdetails.dblQty
						FROM
						ware_returntostoresdetails 
						Inner Join ware_returntostoresheader ON ware_returntostoresdetails.intReturnNo = ware_returntostoresheader.intReturnNo AND ware_returntostoresdetails.intReturnYear = ware_returntostoresheader.intReturnYear
						INNER JOIN ware_mrnheader ON ware_mrnheader.intMrnNo = ware_returntostoresdetails.intMrnNo AND ware_mrnheader.intMrnYear = ware_returntostoresdetails.intMrnYear
						WHERE
						ware_returntostoresdetails.intReturnNo =  '$returnNo' AND
						ware_returntostoresdetails.intReturnYear =  '$year'";
				$result1 = $db->RunQuery2($sql1);
				while($row=mysqli_fetch_array($result1))
				{
					$mrnNo=$row['intMrnNo'];
					$mrnYear=$row['intMrnYear'];
					$orderNo=$row['strOrderNo'];
					$orderYear=$row['intOrderYear'];
					$styleNo=$row['strStyleNo'];
					$issueNo=$row['intIssueNo'];
					$issueYear=$row['intIssueYear'];
					$item=$row['intItemId'];
					$Qty=$row['dblQty'];
					$mrnType= $row['mrnType'];
					
					$sql = "UPDATE `ware_issuedetails` SET 	dblReturnQty = dblReturnQty+'$Qty' 
							WHERE 
							ware_issuedetails.intMrnNo =  '$mrnNo' AND
							ware_issuedetails.intMrnYear =  '$mrnYear' AND
							ware_issuedetails.strOrderNo =  '$orderNo' AND
							ware_issuedetails.intOrderYear =  '$orderYear' AND
							ware_issuedetails.strStyleNo =  '$styleNo' AND
							ware_issuedetails.intIssueNo =  '$issueNo' AND
							ware_issuedetails.intIssueYear =  '$issueYear' AND
							ware_issuedetails.intItemId =  '$item' ";
					//echo $sql;
					$result = $db->RunQuery2($sql);
					if((!$result) && ($rollBackFlag!=1)){
						$rollBackFlag=1;
						$sqlM=$sql;
						$rollBackMsg = "Approval error!";
					}
				}
			}
			
		//---------------------------------------------------------------------------
		}
		
		//-BUDGET BALANCE UPDATION------------------
		if($rollBackFlag != 1){
			$header_array		= $obj_returnToStores_get->get_header($returnNo,$year,'RunQuery2');
			
			if($header_array['STATUS'] == 1){	
				$results_details					=$obj_returnToStores_get->get_details_results($returnNo,$year,'RunQuery2');
				$results_fin_year					=$obj_comm_budget_get->loadFinanceYear_with_selected_date($company,$header_array['DATE'],'RunQuery2');
				$row_fin_year						=mysqli_fetch_array($results_fin_year);
				$budg_year							=$row_fin_year['intId'];
				
				$arr_item_u							= NULL;
				$arr_item_a							= NULL;
				$arr_sub_cat_u						= NULL;
				$arr_sub_cat_a						= NULL; 
				
				$resp_save_array['arr_item_u']		= $arr_item_u;
				$resp_save_array['arr_item_a']		= $arr_item_a;
				$resp_save_array['arr_sub_cat_u']	= $arr_sub_cat_u;
				$resp_save_array['arr_sub_cat_a']	= $arr_sub_cat_a;
				
				$resp_save_array					= $obj_comm_budget_get->load_save_to_array($results_details,$header_array['LOCATION_ID'],$resp_save_array,'RET_STORES',$header_array['ISSUE_NO'],$header_array['ISSUE_YEAR'],'RunQuery2');
 				
				$resp_update						= $obj_budget_set->update_array_PlanDetails_units_or_amount($header_array['LOCATION_ID'],$header_array['DEPARTMENT_ID'],$budg_year,$header_array['BUDG_MONTH'],'ISSUE','-1',$resp_save_array);
				 
				
				if($resp_update['type'] == 'fail'){
					 $rollBackFlag					=1;
					 $rollBackMsg					= $resp_update['msg'];
					 $sqlM							= $resp_update['sql'];
				}
			}
			
			
			//Gate Pass creations ,if the Issue location is not maintaing the stores
			$sql1 = "SELECT ware_issueheader.intCompanyId,issueFrom FROM `ware_issueheader` JOIN mst_locations ON ware_issueheader.intCompanyId = mst_locations.intId WHERE `intIssueNo` = '$issueNo' and intIssueYear = '$issueYear' AND mst_locations.int_stock_locationin = '0'";				
			$result1 = $db->RunQuery2($sql1);
			$issueData = mysqli_fetch_array($result1);
			if(isset($issueData['intCompanyId']) && ($issueData['intCompanyId'] == $currentLocation)){
				$rollBackFlag=1;
				$sqlM='';
				$rollBackMsg = "Return note cannot approve from Substore";
			}
			
			if(isset($issueData['intCompanyId']) && ($issueData['issueFrom'] == $currentLocation) && $rollBackMsg != 1){
				$fromLocation = $issueData['intCompanyId'];
				$tolocation = $location;
				 $gatePassCreated = createGatepass($gpFromLocation,$gpToLocation,$returnNo,$year,$mrnType);
							
				if($gatePassCreated['type'] == 'pass')
				{
					//Approving the created gatepass
					$gatePassApprove = gatePassApprove($gpFromLocation,$gatePassCreated['serialNo'],$gatePassCreated['year'],$returnNo,$year);
					
					if($gatePassApprove['type'] == 'pass')
				{
                    if($company == '1'){
                        updateAzureDBTablesOnGP($gatePassCreated['serialNo'],$gatePassCreated['year'],$db, $gpFromLocation);
                    }
					//gatepass Transfer IN
					$gatePassTransferin  = gatepassTrnsferin($gpFromLocation,$gpToLocation,$gatePassCreated['serialNo'],$gatePassCreated['year'],$returnNo,$year);

					if($gatePassTransferin['type'] == 'pass'){
						$gatepassTransferInApprove = gatepassTransferInApprove($gatePassTransferin['serialNo'],$gatePassTransferin['year']);
							if($gatepassTransferInApprove['type'] == 'fail') {
								$rollBackFlag=1;
								$sqlM='';
								$rollBackMsg = "Unable To Approve Transfer In  Gatepass";
							}
							else{
                                if($company == '1'){
                                    updateAzureDBTablesOnGPIN($gatePassTransferin['serialNo'],$gatePassTransferin['year'],$db, $gpToLocation);
                                }
                            }
					}	
					else{
					$rollBackFlag=1;
					$sqlM='';
					$rollBackMsg = "Unable To Create gatepass Transfer In  Gatepass";
					}
				}
				
					else{
						$rollBackFlag=1;
						$sqlM='';
						$rollBackMsg = "Unable To Approve Gatepass ".$gatePassApprove['msg'];
					}
				}
				
				else{
					
					$rollBackFlag=1;
					$sqlM=$sqlI;
					$rollBackMsg = "Unable To create Gatepass ".$gatePassCreated['msg'];
				}
				
			}
			
		}
		//$rollBackFlag =1;
		//----------------------------
		if($status==1){
			//update_po_prn_details_for_returned_qry($returnNo,$year);
			update_po_prn_details_transactions($returnNo,$year);
		}
	
		$toSavedQty=round($toSavedQty,4);
		$transSavedQty=round($transSavedQty,4);
		
		if(($toSavedQty!=$transSavedQty) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$rollBackMsg = "Approval Error";
			$sqlM=$toSavedQty."-".$transSavedQty;
		}
		
		if(($rollBackFlag==0) &&($status==1)){
		    if($company == '1'){
                updateAzureDBTables($returnNo,$year,$db);
            }
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= "Final Approval raised sucessfully";
			$response['q'] 			= $sql;
			
			$obj_chartOfAccount_set->saveFinanceTransaction($returnNo,$year,'RETSTORES');
		}
		else if($rollBackFlag==0){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= "Approval raised sucessfully";
			$response['q'] 			= '';
		}
		else if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= $sqlM;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sqlM;
		}
		$db->CloseConnection();		
		
		//----------------------------------------
			echo json_encode($response);
		//-----------------------------------------	
		
	}
	else if($_REQUEST['status']=='reject')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		$rollBackFlag=0;
		$rollBackMsg = '';
		
		//$grnApproveLevel = (int)getApproveLevel($programName);
		$sql = "SELECT ware_returntostoresheader.intUser,ware_returntostoresheader.intStatus,ware_returntostoresheader.intApproveLevels FROM ware_returntostoresheader WHERE  (intReturnNo='$returnNo') AND (`intReturnYear`='$year')";
		$results = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($results);
		$createdUser = $row['intUser'];
		$status = $row['intStatus'];
		$savedLevels = $row['intApproveLevels'];
		
		$sql = "UPDATE `ware_returntostoresheader` SET `intStatus`=0 WHERE (intReturnNo='$returnNo') AND (`intReturnYear`='$year')";
		$result = $db->RunQuery2($sql);
		if((!$result) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sql;
			$rollBackMsg = "Rejection error!";
		}
		
		//$sql = "DELETE FROM `ware_returntostoresheader_approvedby` WHERE (`intReturnNo`='$returnNo') AND (`intYear`='$year')";
		//$result2 = $db->RunQuery($sql);
		
		$sqlI = "INSERT INTO `ware_returntostoresheader_approvedby` (`intReturnNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
			VALUES ('$returnNo','$year','0','$userId',now(),0)";
		$resultI = $db->RunQuery2($sqlI);
		if((!$resultI) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sqlI;
			$rollBackMsg = "Rejection error!";
		}
		
		if($resultI)
		{
			if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
			sendRejecedEmailToUser($returnNo,$year,$objMail,$mainPath,$root_path);
			}
		}
		//---------------------------------------------------------------------------------
/*		if($status==1){//if final confirmation raised,insert to stock
				$sql1 = "SELECT 
						ware_returntostoresdetails.strOrderNo,
						ware_returntostoresdetails.intOrderYear,
						trn_orderdetails.strSalesOrderNo,
						trn_orderdetails.intSalesOrderId,
						ware_returntostoresdetails.strStyleNo,
 						ware_returntostoresdetails.dblQty,
						ware_returntostoresdetails.intItemId,   
						mst_item.intBomItem 
						FROM 
						ware_returntostoresdetails  
						left Join trn_orderdetails ON ware_returntostoresdetails.strOrderNo = trn_orderdetails.intOrderNo AND ware_returntostoresdetails.intOrderYear = trn_orderdetails.intOrderYear AND ware_returntostoresdetails.strStyleNo = trn_orderdetails.intSalesOrderId
						Inner Join mst_item ON ware_returntostoresdetails.intItemId = mst_item.intId
						WHERE
						ware_returntostoresdetails.intReturnNo =  '$returnNo' AND
						ware_returntostoresdetails.intReturnYear =  '$year'";
				$result1 = $db->RunQuery($sql1);
				while($row=mysqli_fetch_array($result1))
				{
					$item=$row['intItemId'];
					$Qty=$row['dblQty'];
					$orderNo=$row['strOrderNo'];
					$orderYear=$row['intOrderYear'];
					$salesOrderNo=$row['strSalesOrderNo'];
					$salesOrderId=$row['intSalesOrderId'];
					$styleNo=$row['strStyleNo'];
					$Qty=round($Qty,4);
					if($orderNo!=0){//bom item
					$sqlI = "INSERT INTO `ware_stocktransactions` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intOrderNo,intOrderYear,intSalesOrderId,intItemId,dblQty,strType,intUser,dtDate) 
						VALUES ('$company','$location','$returnNo','$year','$orderNo','$orderYear','$salesOrderId','$item','-$Qty','CRETSTORES','$userId',now())";
					}
					else{//general item
					$sqlI = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intItemId,dblQty,strType,intUser,dtDate) 
						VALUES ('$company','$location','$returnNo','$year','$item','-$Qty','CRETSTORES','$userId',now())";
					}
					$resultI = $db->RunQuery($sqlI);
				}
			}
*/		//---------------------------------------------------------------------------------
			if($savedLevels>=$status){//if atleast one confirmation processed, roll back return to saupplier qty from grn table-
				$sql1 = "SELECT
						ware_returntostoresdetails.intMrnNo,
						ware_returntostoresdetails.intMrnYear,
						ware_returntostoresdetails.strOrderNo,
						ware_returntostoresdetails.intOrderYear,
						ware_returntostoresdetails.strStyleNo,
						ware_returntostoresdetails.intItemId,
						ware_returntostoresheader.intIssueNo,
						ware_returntostoresheader.intIssueYear,
						ware_returntostoresdetails.dblQty
						FROM
						ware_returntostoresdetails 
						Inner Join ware_returntostoresheader ON ware_returntostoresdetails.intReturnNo = ware_returntostoresheader.intReturnNo AND ware_returntostoresdetails.intReturnYear = ware_returntostoresheader.intReturnYear
						WHERE
						ware_returntostoresdetails.intReturnNo =  '$returnNo' AND
						ware_returntostoresdetails.intReturnYear =  '$year'";
				$result1 = $db->RunQuery2($sql1);
				while($row=mysqli_fetch_array($result1))
				{
					$intMrnNo=$row['intMrnNo'];
					$intMrnYear=$row['intMrnYear'];
					$orderNo=$row['strOrderNo'];
					$orderYear=$row['intOrderYear'];
					$salesOrderNo=$row['strStyleNo'];
					$issueNo=$row['intIssueNo'];
					$issueYear=$row['intIssueYear'];
					$item=$row['intItemId'];
					$Qty=$row['dblQty'];
					
					$sql = "UPDATE `ware_issuedetails` SET 	dblReturnQty = dblReturnQty-'$Qty' 
							WHERE 
							ware_issuedetails.intMrnNo =  '$mrnNo' AND
							ware_issuedetails.intMrnYear =  '$mrnYear' AND
							ware_issuedetails.strOrderNo =  '$orderNo' AND
							ware_issuedetails.intOrderYear =  '$orderYear' AND
							ware_issuedetails.strStyleNo =  '$salesOrderNo' AND
							ware_issuedetails.intIssueNo =  '$issueNo' AND
							ware_issuedetails.intIssueYear =  '$issueYear' AND
							ware_issuedetails.intItemId =  '$item' ";
					$result = $db->RunQuery2($sql);
					if((!$result) && ($rollBackFlag!=1)){
						$rollBackFlag=1;
						$sqlM=$sql;
						$rollBackMsg = "Rejection error!";
					}
				}
			}
			
		//------------------------------------
		if($rollBackFlag==0){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= "Rejection raised sucessfully";
			$response['q'] 			= '';
		}
		else if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= $sqlM;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sqlM;
		}
		$db->CloseConnection();		
		
		//----------------------------------------
			echo json_encode($response);
		//-----------------------------------------	

	}
//--------------------------------------------------------------
function getGrnWiseStockBalance_bulk($location,$issueNo,$issueYear,$item,$type=null)
{
if($type == null){
$type = 'ISSUE';
}
	global $db;
	 $sql = "SELECT
			Sum(ware_stocktransactions_bulk.dblQty) AS issueQty,
			ware_stocktransactions_bulk.intGRNNo,
			ware_stocktransactions_bulk.intGRNYear,
			ware_stocktransactions_bulk.dtGRNDate,
			ware_stocktransactions_bulk.dblGRNRate
			FROM ware_stocktransactions_bulk
			WHERE
			ware_stocktransactions_bulk.intItemId =  '$item' AND
			ware_stocktransactions_bulk.strType =  '$type' AND
			ware_stocktransactions_bulk.intDocumentNo =  '$issueNo' AND 
			ware_stocktransactions_bulk.intDocumntYear =  '$issueYear' AND 
			ware_stocktransactions_bulk.intLocationId =  '$location'
			GROUP BY
			ware_stocktransactions_bulk.intItemId,
			ware_stocktransactions_bulk.intGRNNo,
			ware_stocktransactions_bulk.intGRNYear, 
			ware_stocktransactions_bulk.dblGRNRate 
			ORDER BY
			ware_stocktransactions_bulk.dtGRNDate ASC";
	$result = $db->RunQuery2($sql);
	return $result;	
}
//--------------------------------------------------------------
function getGrnWiseStockBalance($location,$issueNo,$issueYear,$orderNo,$orderYear,$salesOrderId,$item)
{
	global $db;
	 $sql = "SELECT
			Sum(ware_stocktransactions.dblQty) AS issueQty,
			ware_stocktransactions.intGRNNo,
			ware_stocktransactions.intGRNYear,
			ware_stocktransactions.dtGRNDate,
			ware_stocktransactions.dblGRNRate
			FROM ware_stocktransactions 
			WHERE
			ware_stocktransactions.intItemId =  '$item' AND 
			ware_stocktransactions.strType =  'ISSUE' AND
			ware_stocktransactions.intDocumentNo =  '$issueNo' AND 
			ware_stocktransactions.intDocumntYear =  '$issueYear' AND 
			ware_stocktransactions.intLocationId =  '$location' AND 
			ware_stocktransactions.intOrderNo='$orderNo' AND 
			ware_stocktransactions.intOrderYear='$orderYear' AND 
			ware_stocktransactions.intSalesOrderId='$salesOrderId' 
			GROUP BY
			ware_stocktransactions.intItemId,
			ware_stocktransactions.intGRNNo,
			ware_stocktransactions.intGRNYear, 
			ware_stocktransactions.dblGRNRate 
			ORDER BY
			ware_stocktransactions.dtGRNDate ASC";

	$result = $db->RunQuery2($sql);
	return $result;	
}
//--------------------------------------------------------------
function loadCurrency($grnNo,$grnYear,$item)
{
	global $db;
	
	if(($grnNo!=0) && ($grnYear!=0)){
	  $sql = "SELECT
			trn_poheader.intCurrency as currency 
			FROM
			ware_grnheader
			Inner Join trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
			WHERE
			ware_grnheader.intGrnNo =  '$grnNo' AND
			ware_grnheader.intGrnYear =  '$grnYear'";
	}
	else{
	$sql = "SELECT
			mst_item.intCurrency as currency 
			FROM mst_item
			WHERE
			mst_item.intId =  '$item'";
	}

	$result = $db->RunQuery2($sql);
	$row=mysqli_fetch_array($result);	
	return $row['currency'];;	
}
//--------------------------------------------------------
function sendFinlConfirmedEmailToUser($serialNo,$year,$objMail,$mainPat,$root_path){
	global $db;
				
		    $sql = "SELECT
			sys_users.strFullName,
			sys_users.strEmail 
			FROM 
			ware_returntostoresheader
			Inner Join sys_users ON ware_returntostoresheader.intUser = sys_users.intUserId
			WHERE
			ware_returntostoresheader.intReturnNo =  '$serialNo' AND
			ware_returntostoresheader.intReturnYear =  '$year'";

				
		 $result = $db->RunQuery2($sql);
		 
		$row=mysqli_fetch_array($result);
			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="CONFIRMED RETURN TO STORES NOTE ('$serialNo'/'$year')"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST = NULL;
			
			$_REQUEST['program']='RETURN TO STORES NOTE';
			$_REQUEST['field1']='Return No';
			$_REQUEST['field2']='Return Year';
			$_REQUEST['field3']='';
			$_REQUEST['field4']='';
			$_REQUEST['field5']='';
			$_REQUEST['value1']=$serialNo;
			$_REQUEST['value2']=$year;
			$_REQUEST['value3']='';
			$_REQUEST['value4']='';
			$_REQUEST['value5']='';
			
			$_REQUEST['subject']="CONFIRMED RETURN TO STORES NOTE ('$serialNo'/'$year')";
			
			$_REQUEST['statement1']="Approved this";
			$_REQUEST['statement2']="to view this";
			$_REQUEST['link']=base64_encode($mainPath."presentation/warehouse/returnToStores/listing/rptReturnToStores.php?returnNo=$serialNo&year=$year&approveMode=0"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}
//--------------------------------------------------------
function sendRejecedEmailToUser($serialNo,$year,$objMail,$mainPat,$root_path){
	global $db;
				
		    $sql = "SELECT
			sys_users.strFullName,
			sys_users.strEmail 
			FROM 
			ware_returntostoresheader
			Inner Join sys_users ON ware_returntostoresheader.intUser = sys_users.intUserId
			WHERE
			ware_returntostoresheader.intReturnNo =  '$serialNo' AND
			ware_returntostoresheader.intReturnYear =  '$year'";

				
		 $result = $db->RunQuery2($sql);
		 
		$row=mysqli_fetch_array($result);
			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="REJECTED RETURN TO STORES NOTE('$serialNo'/'$year')"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST = NULL;
			
			$_REQUEST['program']='RETURN TO STORES NOTE';
			$_REQUEST['field1']='Return No';
			$_REQUEST['field2']='Return Year';
			$_REQUEST['field3']='';
			$_REQUEST['field4']='';
			$_REQUEST['field5']='';
			$_REQUEST['value1']=$serialNo;
			$_REQUEST['value2']=$year;
			$_REQUEST['value3']='';
			$_REQUEST['value4']='';
			$_REQUEST['value5']='';
			
			$_REQUEST['subject']="REJECTED RETURN TO STORES NOTE ('$serialNo'/'$year')";
			
			$_REQUEST['statement1']="Rejected this";
			$_REQUEST['statement2']="to view this";
			$_REQUEST['link']=base64_encode($mainPath."presentation/warehouse/returnToStores/listing/rptReturnToStores.php?returnNo=$serialNo&year=$year&approveMode=0"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}

function update_po_prn_details_for_returned_qry($returnNo,$year){
	global $db;
	      $sql="SELECT
				ware_issuedetails.strOrderNo,
				ware_issuedetails.intOrderYear,
				ware_issuedetails.strStyleNo,
				ware_issuedetails.intItemId,
				ware_returntostoresdetails.dblQty,
				trn_po_prn_details_sales_order.ORDER_NO AS saved_pri
				FROM
				ware_returntostoresdetails
				INNER JOIN ware_returntostoresheader ON ware_returntostoresdetails.intReturnNo = ware_returntostoresheader.intReturnNo AND ware_returntostoresdetails.intReturnYear = ware_returntostoresheader.intReturnYear
				INNER JOIN ware_issuedetails ON ware_returntostoresheader.intIssueNo = ware_issuedetails.intIssueNo AND ware_returntostoresheader.intIssueYear = ware_issuedetails.intIssueYear AND ware_returntostoresdetails.intItemId = ware_issuedetails.intItemId AND ware_returntostoresdetails.intMrnNo = ware_issuedetails.intMrnNo AND ware_returntostoresdetails.intMrnYear = ware_issuedetails.intMrnYear AND ware_returntostoresdetails.strOrderNo = ware_issuedetails.strOrderNo AND ware_returntostoresdetails.intOrderYear = ware_issuedetails.intOrderYear AND ware_returntostoresdetails.strStyleNo = ware_issuedetails.strStyleNo
				LEFT JOIN trn_po_prn_details_sales_order ON ware_issuedetails.strOrderNo = trn_po_prn_details_sales_order.ORDER_NO AND ware_issuedetails.intOrderYear = trn_po_prn_details_sales_order.ORDER_YEAR AND ware_issuedetails.strStyleNo = trn_po_prn_details_sales_order.SALES_ORDER AND ware_issuedetails.intItemId = trn_po_prn_details_sales_order.ITEM
				WHERE
			ware_returntostoresdetails.intReturnNo = '$returnNo' AND
			ware_returntostoresdetails.intReturnYear = '$year'
			";
	$result = $db->RunQuery2($sql);
	while($row	=mysqli_fetch_array($result)){
		$orderNo			= $row['strOrderNo'];
		$orderYear			= $row['intOrderYear'];
		$so					= $row['strStyleNo'];
		$item				= $row['intItemId'];
		$qty				= $row['dblQty'];
		$saved_pri			= $row['saved_pri'];
	
		if($saved_pri>0){
		$sql_u	="UPDATE `trn_po_prn_details_sales_order` SET `RETURNED_QTY`=IFNULL(`RETURNED_QTY`,0)+'$qty' 
		WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`SALES_ORDER`='$so') AND (`ITEM`='$item')";
		$result_u = $db->RunQuery2($sql_u);
		}
		else{ //NON PRI ITEM
		  $sql_u	="UPDATE `trn_po_prn_details_sales_order_additional_mrn_items` SET `RETURNED_QTY`=IFNULL(`RETURNED_QTY`,0)+'$qty' 
		WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`SALES_ORDER`='$so') AND (`ITEM`='$item')";
		$result_u = $db->RunQuery2($sql_u);
		}
	}
}
function getOrderStatus($orderNo,$year){
    global $db;
    $sql = "SELECT
			trn_orderheader.intStatus,
			trn_orderheader.intApproveLevelStart ,
			PO_TYPE 
			FROM
			trn_orderheader
			WHERE
			trn_orderheader.intOrderNo = '$orderNo' AND
			trn_orderheader.intOrderYear = '$year'

			";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
	$arr[0]	= $row['intStatus'];
	$arr[1]	= $row['intApproveLevelStart'];
	$arr[2]	= $row['PO_TYPE'];
    return $arr;
	
}

function update_po_prn_details_transactions($returnNo,$year){
	global $db;
	      $sql="SELECT
				ware_returntostoresdetails.strOrderNo,
				ware_returntostoresdetails.intOrderYear,
				ware_returntostoresdetails.strStyleNo,
				ware_returntostoresdetails.intItemId,
				sum(ware_returntostoresdetails.dblQty) as dblQty,
				ware_returntostoresheader.intCompanyId,
				trn_po_prn_details_sales_order_stock_transactions.ORDER_NO AS saved_pri
				FROM
				ware_returntostoresdetails
				INNER JOIN ware_returntostoresheader ON ware_returntostoresdetails.intReturnNo = ware_returntostoresheader.intReturnNo 
AND ware_returntostoresdetails.intReturnYear = ware_returntostoresheader.intReturnYear
			LEFT JOIN trn_po_prn_details_sales_order_stock_transactions 
ON ware_returntostoresdetails.strOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO 
AND ware_returntostoresdetails.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR 
AND ware_returntostoresdetails.strStyleNo = trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER 
AND ware_returntostoresdetails.intItemId = trn_po_prn_details_sales_order_stock_transactions.ITEM  
			AND (trn_po_prn_details_sales_order_stock_transactions.TRANSACTION='RETURNED_TO_STORES' ) 
			AND trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID =ware_returntostoresheader.intCompanyId
				WHERE
			ware_returntostoresdetails.intReturnNo = '$returnNo' AND
			ware_returntostoresdetails.intReturnYear = '$year' 
			group by 
			ware_returntostoresdetails.strOrderNo,
			ware_returntostoresdetails.intOrderYear,
			ware_returntostoresdetails.strStyleNo,
			ware_returntostoresdetails.intItemId 
			";
	$result = $db->RunQuery2($sql);
	while($row	=mysqli_fetch_array($result)){
		$orderNo			= $row['strOrderNo'];
		$orderYear			= $row['intOrderYear'];
		$so					= $row['strStyleNo'];
		$item				= $row['intItemId'];
		$qty				= $row['dblQty'];
		$location			= $row['intCompanyId'];
		$saved_pri			= $row['saved_pri'];
	
		if($saved_pri > 0){
        //PRI ITEM
            $sql_u	="UPDATE `trn_po_prn_details_sales_order_stock_transactions` SET `QTY`=IFNULL(`QTY`,0)+'$qty' 
		  WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`SALES_ORDER`='$so') AND (`ITEM`='$item') AND (`TRANSACTION`='RETURNED_TO_STORES') AND (`LOCATION_ID`='$location')";
            $result_u = $db->RunQuery2($sql_u);
        }
		else{
			$sql_u2	="INSERT INTO `trn_po_prn_details_sales_order_stock_transactions` (`ORDER_NO`, `ORDER_YEAR`, `SALES_ORDER`, `ITEM`, `LOCATION_ID`, `TRANSACTION`, `QTY`) VALUES ('$orderNo', '$orderYear', '$so', '$item', '$location', 'RETURNED_TO_STORES', '$qty')";
			$result_u2 	= $db->RunQuery2($sql_u2);
		}
	}
}
//--------------------------------------------------------
//Gate Paas adding

function createGatepass($fromLocation,$tolocation,$returnNo,$returnyear,$mrnType){
	
global $gatePassApproveLevel;
global $db;
global $ApproveLevels;
global $userId;
//------------save---------------------------	

	$savableFlag= 1;

	
		$serialNo 	= getNextGatepassNo($fromLocation);
		$year = date('Y');
		$editMode=0;
		$savedStatus=$gatePassApproveLevel;
		$savedLevels=$ApproveLevels;
		$note = 'This Gate Pass is Generated by System';
	 //Here Gate pass type 1 is orderwise and 0 is General.but 2 is special gatepass and it is
	//generating by system
		if($mrnType != null && $mrnType == 3){
		$type = 1;
	}else{
		$type = 0;
	}

		$sql = "INSERT INTO `ware_gatepassheader` (`intGatePassNo`,`intGatePassYear`,intGPToLocation,strNote,intStatus,intApproveLevels,datdate,dtmCreateDate,intUser,intCompanyId,gpType,isSystemGenerated) 
					VALUES ('$serialNo','$year','$tolocation','$note','$gatePassApproveLevel','$ApproveLevels',now(),now(),'$userId','$fromLocation','$type',1)";
		$result = $db->RunQuery2($sql);
		$maxAppByStatus=(int)getMaxAppByStatus($serialNo,$year)+1;
		$sql = "UPDATE `ware_gatepassheader_approvedby` SET intStatus ='$maxAppByStatus' 
					WHERE (`intGatePassNo`='$serialNo') AND (`intYear`='$year') AND (`intStatus`='0')";
		$result1 = $db->RunQuery2($sql);
		
		//Updating return header with gatepass number
		$returnHeaderUpdateSql = "UPDATE `ware_returntostoresheader` SET intGatePassNo ='$serialNo' 
					WHERE (`intReturnNo`='$returnNo') AND (`intReturnYear`='$returnyear')";
		$RHUresult = $db->RunQuery2($returnHeaderUpdateSql);
	
	//-----------delete and insert to detail table-----------------------
	if($result){
		$sql = "DELETE FROM `ware_gatepassdetails` WHERE (`intGatePassNo`='$serialNo') AND (`intGatePassYear`='$year')";
		$result2 = $db->RunQuery2($sql);
		$toSave=0;
		$saved=0;
		$rollBackFlag=0;
		$rollBackMsg ="Maximum Gate Pass Qtys for items are...";
		$budgetFlag	 = 0;

		// Getting Return Note Details
		
		$sql = "SELECT
ware_returntostoresdetails.intReturnNo,
ware_returntostoresdetails.intReturnYear,
ware_returntostoresdetails.strOrderNo,
ware_returntostoresdetails.intOrderYear,
ware_returntostoresdetails.strStyleNo,
ware_returntostoresdetails.intItemId,
sum(ware_returntostoresdetails.dblQty) as dblQty
FROM `ware_returntostoresdetails`
WHERE intReturnNo = '$returnNo' AND intReturnYear = '$returnyear'
GROUP BY
ware_returntostoresdetails.strOrderNo,
ware_returntostoresdetails.intOrderYear,
ware_returntostoresdetails.strStyleNo,
ware_returntostoresdetails.intItemId";
		$ReturnNoteDetails = $db->RunQuery2($sql);

		while($arrVal = mysqli_fetch_array($ReturnNoteDetails))
		{
			
			$location 	= $_SESSION['CompanyID'];
			$company 	= $_SESSION['headCompanyId'];
			$orderNo 	 = $arrVal['strOrderNo'];
			$intOrderYear 	 = $arrVal['intOrderYear'];
			//$orderNoArray   = explode('/',$orderNo);
			$allocatedFlag=1;

			// if($orderNoArray[0]==''){
				// $orderNoArray[0]=0;
				// $allocatedFlag=0;
			// }
			// else{
				// $allocatedFlag=1;
			// }

			// if($orderNoArray[1]==''){
				// $orderNoArray[1]=0;
			// }
			
			// in this table strStyleNo is sales ID (this is mistake for earlier Developer :) )
			$salesOrderId 		 = $arrVal['strStyleNo'];
			$itemId 	 = $arrVal['intItemId'];
			$Qty 		 = $arrVal['dblQty'];

			$itemName=getItemName($itemId);

			if($allocatedFlag==1){
				$stockBalQty=getStockBalance_bulk($fromLocation,$itemId);
			}
			

			$confirmedGPqty=get_gpQty($fromLocation,$orderNo,$intOrderYear,$salesOrderId,$itemId);//but not raised final confirmation

			//------check maximum ISSUE Qty--------------------
			$Qty = round($Qty,4);
			$balQtyToGP = ($stockBalQty-$confirmedGPqty);
			$balQtyToGP = round($balQtyToGP,4);
			if($Qty>$balQtyToGP){
				//	call roll back--------****************
				$rollBackFlag=1;
				if($allocatedFlag==1){
					$rollBackMsg .="<br> ".$orderNo."/".$intOrderYear."-".$salesOrderId."-".$itemName." =".$balQtyToGP;
					$msg .=$Qty." =".$balQtyToGP;
				}
				else{
					$rollBackMsg .="<br> ".$itemName." =".$balQtyToGP;
				}
				//exit();
			}

			//----------------------------
			if($rollBackFlag!=1){
				
				$Qty=round($Qty,4);
				
				$sql = "INSERT INTO `ware_gatepassdetails` (`intGatePassNo`,`intGatePassYear`,`strOrderNo`,`intOrderYear`,`strStyleNo`,`intItemId`,`dblQty`,dblTransferQty) 
					VALUES ('$serialNo','$year','$orderNo','$intOrderYear','$salesOrderId','$itemId','$Qty','0')";
				$result3 = $db->RunQuery2($sql);
				
				if($result3==1){
					$saved++;
				}
			}
			$toSave++;
		}
	}
	//die($rollBackMsg);
	if($rollBackFlag==1){
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $rollBackMsg;
		$response['q'] 			= $msg;
	}
	else if(($savableFlag==1) &&(!$result)){
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= "Header saving error";
		$response['q'] 			= $sql;
	}
	else if(($savableFlag==1) &&($toSave==0) &&($saved==0)){
		$response['type'] 		= 'fail';
		$response['msg'] 		= "Details saving error";
		$response['q'] 			= '';
	}
	else if(($result) && ($toSave==$saved)){
		

		$response['type'] 		= 'pass';
		if($editMode==1)
			$response['msg'] 		= 'Updated successfully.';
		else
			$response['msg'] 		= 'Saved successfully.';

		$response['serialNo'] 		= $serialNo;
		$response['year'] 		= $year;

	}
	else{
		$response['type'] 		= 'fail';
		$response['msg'] 		= $db->errormsg;
		$response['q'] 			= $sql;
	}

//----------------------------------------

return $response;

	
	
	
	
}

function getNextGatepassNo($companyId)
{
	global $db;
	$sql = "SELECT
				sys_no.intGatePassNo
				FROM sys_no
				WHERE
				sys_no.intCompanyId =  '$companyId'
				";
	$result = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($result);
	$nextGatePassNo = $row['intGatePassNo'];

	$sql = "UPDATE `sys_no` SET intGatePassNo=intGatePassNo+1 WHERE (`intCompanyId`='$companyId')  ";
	$db->RunQuery2($sql);

	return $nextGatePassNo;
}

//------------------------------function get MaxAppByStatus---------------------
function getMaxAppByStatus($serialNo,$year){
	global $db;

	//echo $savedStat;
	$editMode=0;
	$sql = "SELECT
				max(ware_gatepassheader_approvedby.intStatus) as status 
				FROM
				ware_gatepassheader_approvedby
				WHERE
				ware_gatepassheader_approvedby.intGatePassNo =  '$serialNo' AND
				ware_gatepassheader_approvedby.intYear =  '$year'";

	$resultm = $db->RunQuery2($sql);
	$rowm=mysqli_fetch_array($resultm);

	return $rowm['status'];

}

function getItemName($itemId){
	global $db;
	$sql = "SELECT
					mst_item.strName
					FROM mst_item
					WHERE
					mst_item.intId =  '$itemId'";

	$result = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($result);
	return $row['strName'];
}
//--------------------------------------------------------------
function getStockBalance_bulk($location,$item)
{
	global $db;
	$sql = "SELECT
				Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$location'
				GROUP BY
				ware_stocktransactions_bulk.intItemId";

	$result = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($result);
	return val($row['stockBal']);
}

function get_gpQty($location,$orderNo,$orderYear,$styleNo,$itemId){
	global $db;
	$sql1 = "SELECT
				Sum(ware_gatepassdetails.dblQty) as gpQty
				FROM
				ware_gatepassheader
				Inner Join ware_gatepassdetails ON ware_gatepassheader.intGatePassNo = ware_gatepassdetails.intGatePassNo AND ware_gatepassheader.intGatePassYear = ware_gatepassdetails.intGatePassYear
				WHERE
				ware_gatepassdetails.strOrderNo =  '$orderNo' AND
				ware_gatepassdetails.intOrderYear =  '$orderYear' AND
				ware_gatepassdetails.strStyleNo =  '$styleNo' AND
				ware_gatepassdetails.intItemId =  '$itemId' AND
				ware_gatepassheader.intStatus >  '1' AND
				ware_gatepassheader.intStatus <= ware_gatepassheader.intApproveLevels AND
				ware_gatepassheader.intCompanyId =  '$location'";
	$result1 = $db->RunQuery2($sql1);
	$row1 = mysqli_fetch_array($result1);
	$gpQty = $row1['gpQty'];
	return $gpQty;
}

function gatePassApprove($Tolocation,$gatePassNo,$year,$returnNo,$Returnyear){
		global $userId;
		global $db;
		global $company;
		global $location;
		$currentLocation = $location;
		$rollBackFlag=0;
		$rollBackMsg = '';
		
			$sql_1 = "SELECT 
						trn_orderheader.intOrderNo,
						trn_orderheader.intOrderYear,
						trn_orderheader.intStatus,
						trn_orderheader.intApproveLevelStart
					FROM trn_orderheader 
					INNER JOIN ware_gatepassdetails ON trn_orderheader.intOrderNo = ware_gatepassdetails.strOrderNo AND
						trn_orderheader.intOrderYear = ware_gatepassdetails.intOrderYear
					WHERE
						ware_gatepassdetails.intGatePassNo = '$gatePassNo'
					AND ware_gatepassdetails.intGatePassYear = '$year'";
			$result_1 = $db->RunQuery2($sql_1);
			
			while($row=mysqli_fetch_array($result_1))
			{
		
				$intOrderNo   = $row['intOrderNo'];
				$intOrderYear = $row['intOrderYear'];
				$intStatus = $row['intStatus'];
				$intApproveLevelStart = $row['intApproveLevelStart'];
				
				if($intStatus <= 0 || ($intStatus>=$intApproveLevelStart+1))
				{
				//$rollBackFlag=1;
				//$sqlM=$sql;
				//$rollBackMsg = "Approval error! Unapproved Order.";
				}
				
			}
	
		
		$sql = "UPDATE `ware_gatepassheader` SET `intStatus`=intStatus-1 WHERE (intGatePassNo='$gatePassNo') AND (`intGatePassYear`='$year') and intStatus not in(0,1)";
		$result = $db->RunQuery2($sql);
		if((!$result) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sql;
			$rollBackMsg = "GatePass Approval error!";
		}
				
		if($result){
			$sql = "SELECT ware_gatepassheader.intUser,
					ware_gatepassheader.intStatus,
					ware_gatepassheader.intApproveLevels, 
					ware_gatepassheader.intCompanyId AS location,
					mst_locations.intCompanyId AS companyId,
					ware_gatepassheader.intGPToLocation AS gatepassToLoc
					FROM
					ware_gatepassheader
					Inner Join mst_locations ON ware_gatepassheader.intCompanyId = mst_locations.intId
					WHERE (intGatePassNo='$gatePassNo') AND (`intGatePassYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$createdUser = $row['intUser'];
			$status = $row['intStatus'];
			$savedLevels = $row['intApproveLevels'];
			$company = $row['companyId'];
			$location = $row['location'];
			$gatepassToLoc = $row['gatepassToLoc'];
			
			$approveLevel=$savedLevels+1-$status;
			$sqlI = "INSERT INTO `ware_gatepassheader_approvedby` (`intGatePassNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
				VALUES ('$gatePassNo','$year','$approveLevel','$userId',now(),0)";
			$resultI = $db->RunQuery2($sqlI);
			if((!$resultI) && ($rollBackFlag!=1)){
				$rollBackFlag=1;
				$sqlM=$sqlI;
				$rollBackMsg = "Approval error!";
			}
			
			
		//--------------------------------------------------------------------
		if($status==1){//if final confirmation raised,insert to stock
				$sql1 = "SELECT 
						ware_gatepassdetails.strOrderNo,
						ware_gatepassdetails.intOrderYear,
						trn_orderdetails.strSalesOrderNo,
						trn_orderdetails.intSalesOrderId,
						ware_gatepassdetails.strStyleNo,
 						ware_gatepassdetails.dblQty,
						ware_gatepassdetails.intItemId,   
						mst_item.intBomItem 
						FROM 
						ware_gatepassdetails  
						left Join trn_orderdetails ON ware_gatepassdetails.strOrderNo = trn_orderdetails.intOrderNo AND ware_gatepassdetails.intOrderYear = trn_orderdetails.intOrderYear AND ware_gatepassdetails.strStyleNo = trn_orderdetails.intSalesOrderId
						Inner Join mst_item ON ware_gatepassdetails.intItemId = mst_item.intId
						WHERE
						ware_gatepassdetails.intGatePassNo =  '$gatePassNo' AND
						ware_gatepassdetails.intGatePassYear =  '$year'";
				$result1 = $db->RunQuery2($sql1);
				$toSavedQty=0;
				$transSavedQty=0;
				while($row=mysqli_fetch_array($result1))
				{
					$toSavedQty+=round($row['dblQty'],4);
					$item=$row['intItemId'];
					$Qty=$row['dblQty'];
					$orderNo=$row['strOrderNo'];
					$orderYear=$row['intOrderYear'];
					$salesOrderNo=$row['strSalesOrderNo'];
					$salesOrderId=$row['intSalesOrderId'];
					if($orderNo==0){
						$bomItemFlag=0;
					}
					else{
						$bomItemFlag=1;
					}
					

					if($bomItemFlag==1){//bom item--get grn wise stock balance
						//$resultB = getGrnWiseStockBalance($location,$orderNo,$orderYear,$salesOrderId,$item);
						$resultB = getGrnWiseStockBalance_bulk_gatepass($Tolocation,$item);
						
						while($rowB=mysqli_fetch_array($resultB)){
								if(($Qty>0) && ($rowB['stockBal']>0)){
									if(($rowB['stockBal']>=$Qty)){
									$saveQty=$Qty;
									$Qty=0;
									}
									else if($Qty>$rowB['stockBal']){
									$saveQty=$rowB['stockBal'];
									$Qty=$Qty-$saveQty;
									}
									$grnNo=$rowB['intGRNNo'];
									$grnYear=$rowB['intGRNYear'];
									$grnDate=$rowB['dtGRNDate'];
									$grnRate=$rowB['dblGRNRate'];
									$currency=loadCurrency($grnNo,$grnYear,$item);
									$saveQty=round($saveQty,4);		
										
									if($saveQty>0){	
									/*$sqlI = "INSERT INTO `ware_stocktransactions` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intOrderNo,intOrderYear,intSalesOrderId,intItemId,dblQty,strType,intUser,dtDate) 
										VALUES ('$company','$location','$gatePassNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$orderNo','$orderYear','$salesOrderId','$item','-$saveQty','GATEPASS','$userId',now())";*/
									$sqlI = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intOrderNo,intOrderYear,intSalesOrderId,intUser,dtDate) 
										VALUES ('$company','$Tolocation','$gatePassNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$item','-$saveQty','GATEPASS','$orderNo','$orderYear','$salesOrderId','$userId',now())";
									$resultI = $db->RunQuery2($sqlI);
									
									if($resultI){
										$transSavedQty+=$saveQty;
									}
									if((!$resultI) && ($rollBackFlag!=1)){
										$rollBackFlag=1;
										$sqlM=$sqlI;
										$rollBackMsg = "Approval error!";
									}
									
									}
								}
						}
					}
				
					else{//general item--get grn wise stock balance
						$resultG = getGrnWiseStockBalance_bulk_gatepass($Tolocation,$item);
						while($rowG=mysqli_fetch_array($resultG)){
								if(($Qty>0) && ($rowG['stockBal']>0)){
									if($Qty<=$rowG['stockBal']){
									$saveQty=$Qty;
									$Qty=0;
									}
									else if($Qty>$rowG['stockBal']){
									$saveQty=$rowG['stockBal'];
									$Qty=$Qty-$saveQty;
									}
									$grnNo=$rowG['intGRNNo'];
									$grnYear=$rowG['intGRNYear'];
									$grnDate=$rowG['dtGRNDate'];
									$grnRate=$rowG['dblGRNRate'];	
									$currency=loadCurrency($grnNo,$grnYear,$item);
									$saveQty=round($saveQty,4);		
									if($saveQty>0){	
									$sqlI = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intUser,dtDate) 
										VALUES ('$company','$Tolocation','$gatePassNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$item','-$saveQty','GATEPASS','$userId',now())";
									$resultI = $db->RunQuery2($sqlI);
									if($resultI){
										$transSavedQty+=$saveQty;
									}
									if((!$resultI) && ($rollBackFlag!=1)){
										$rollBackFlag=1;
										$sqlM=$sqlI;
										$rollBackMsg = "Approval error!";
									}
									
									}
								}
						}
					}
				}
			}
	
		
		}

		if($status==1 ){
			//echo "sdfdsfdwf";
			//update_po_prn_details_for_gp_qty($gatePassNo,$year);
			update_po_prn_details_transactionsGatePass($gatePassNo,$year);
		}
		$toSavedQty=round($toSavedQty,4);
		$transSavedQty=round($transSavedQty,4);
	
		if(($toSavedQty!=$transSavedQty) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$rollBackMsg = "Approval Error";
			$sqlM=$toSavedQty."-".$transSavedQty;
		}
		
		if(($rollBackFlag==0) &&($status==1)){
			$response['type'] 		= 'pass';
			$response['msg'] 		= "Final Approval raised sucessfully";
			$response['q'] 			= $sql;
		}
		else if($rollBackFlag==0){
			$response['type'] 		= 'pass';
			$response['msg'] 		= "Approval raised sucessfully";
			$response['q'] 			= '';
		}
		else if($rollBackFlag==1){
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= $sqlM;
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sqlM;
		}
		//----------------------------------------
			return $response;
		//-----------------------------------------	
		
		
		
	}
	
	function update_po_prn_details_transactionsGatePass($gatePassNo,$year){
	global $db;
	$sql="SELECT
			ware_gatepassdetails.strOrderNo,
			ware_gatepassdetails.intOrderYear,
			ware_gatepassdetails.strStyleNo,
			ware_gatepassdetails.intItemId,
			ware_gatepassdetails.dblQty,
			ware_gatepassheader.intCompanyId AS `from`,
			ware_gatepassheader.intGPToLocation AS `to`,
			trn_po_prn_details_sales_order_stock_transactions.ORDER_NO AS saved_pri
			FROM
			ware_gatepassdetails
			INNER JOIN ware_gatepassheader ON ware_gatepassdetails.intGatePassNo = ware_gatepassheader.intGatePassNo AND ware_gatepassdetails.intGatePassYear = ware_gatepassheader.intGatePassYear
			LEFT JOIN trn_po_prn_details_sales_order_stock_transactions ON ware_gatepassdetails.strOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO AND ware_gatepassdetails.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR AND ware_gatepassdetails.strStyleNo = trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER AND ware_gatepassdetails.intItemId = trn_po_prn_details_sales_order_stock_transactions.ITEM  
			AND (trn_po_prn_details_sales_order_stock_transactions.TRANSACTION='GATE_PASS' ) 
			AND trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID =ware_gatepassheader.intCompanyId
			WHERE
			ware_gatepassdetails.intGatePassNo = '$gatePassNo' AND
			ware_gatepassdetails.intGatePassYear = '$year'
			";
	$result = $db->RunQuery2($sql);
	while($row	=mysqli_fetch_array($result)){
		$orderNo			= $row['strOrderNo'];
		$orderYear			= $row['intOrderYear'];
		$so					= $row['strStyleNo'];
		$item				= $row['intItemId'];
		$totQty				= $row['dblQty'];
		$saved_pri				= $row['saved_pri'];
		$location			= $row['from'];
		if($saved_pri > 0){
        //PRI ITEM
            $sql_u	="UPDATE `trn_po_prn_details_sales_order_stock_transactions` SET `QTY`=IFNULL(`QTY`,0)+'$totQty' 
		  WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`SALES_ORDER`='$so') AND (`ITEM`='$item') AND (`TRANSACTION`='GATE_PASS') AND (`LOCATION_ID`='$location')";
            $result_u = $db->RunQuery2($sql_u);
        }
		else{
			$sql_u2	="INSERT INTO `trn_po_prn_details_sales_order_stock_transactions` (`ORDER_NO`, `ORDER_YEAR`, `SALES_ORDER`, `ITEM`, `LOCATION_ID`, `TRANSACTION`, `QTY`) VALUES ('$orderNo', '$orderYear', '$so', '$item', '$location', 'GATE_PASS', '$totQty')";
			$result_u2 	= $db->RunQuery2($sql_u2);
		}
		
 	}
}

function getGrnWiseStockBalance_bulk_gatepass($location,$item)
{
	global $db;
	  $sql = "SELECT
			Sum(ware_stocktransactions_bulk.dblQty) AS stockBal,
			ware_stocktransactions_bulk.intGRNNo,
			ware_stocktransactions_bulk.intGRNYear,
			ware_stocktransactions_bulk.dtGRNDate,
			ware_stocktransactions_bulk.dblGRNRate
			FROM ware_stocktransactions_bulk
			WHERE
			ware_stocktransactions_bulk.intItemId =  '$item' AND
			ware_stocktransactions_bulk.intLocationId =  '$location'
			GROUP BY
			ware_stocktransactions_bulk.intItemId,
			ware_stocktransactions_bulk.intGRNNo,
			ware_stocktransactions_bulk.intGRNYear,
			ware_stocktransactions_bulk.dtGRNDate, 
			ware_stocktransactions_bulk.dblGRNRate 
			ORDER BY
			ware_stocktransactions_bulk.dtGRNDate ASC";
			
			//echo $sql;

	$result = $db->RunQuery2($sql);
	return $result;	
}


//------------Gate Pass Transferin---------------------------	
	function gatepassTrnsferin($fromLocation,$tolocation,$gpNo,$gpYear,$returnNo,$returnedYear)
	{
		global $userId;
		global $db;
		global $ApproveLevelsTransfer;
		global $transfApproveLevel;
		global $objwhouseget;
		
		$savableFlag= 1;
	

			$serialNo 	= getNextTransfNo($tolocation);
			$year = date('Y');
			$editMode=0;
			$savedStatus=$transfApproveLevel;
			$savedLevels=$transfApproveLevel;
			$ApproveLevels=$transfApproveLevel;
		
		//--------------------------
		//$response['arrData'] = $objwhouseget->getLocationValidation('TRANSFERIN',$fromLocation,$gpNo,$gpYear);//check for grn location
		//--------------------------
		
				$sql="SELECT
					ware_gatepassheader.intGPToLocation as location 
					FROM
					ware_gatepassheader
					WHERE
					ware_gatepassheader.intGatePassNo =  '$gpNo' AND
					ware_gatepassheader.intGatePassYear =  '$gpYear'";
					$result1 = $db->RunQuery2($sql);
				$row = mysqli_fetch_array($result1);
				if($row['location']==$tolocation){
				$data['type'] = 'pass';
				$data['msg'] = '';
				}
				else{
				$rollBackFlag=1;
				$rollBackMsg = 'Invalid Save Location';
				}

		 if($savableFlag==0){
			 $rollBackFlag=1;
			 $rollBackMsg="This Gate Pass Transfer In No is already confirmed.cant edit";
		 }
		 
		else if($editMode==1){//edit existing grn
		$note = 'This Gate Pass Transferin is Generated by System';
		$sql = "UPDATE `ware_gatepasstransferinheader` SET intStatus ='$transfApproveLevel', 
												       intApproveLevels ='$ApproveLevels', 
												       strNote ='$note', 
													  intModifiedBy ='$userId', 
													  dtmModifiedDate =now() 
				WHERE (`intGpTransfNo`='$serialNo') AND (`intGpTransfYear`='$year')";
		$result = $db->RunQuery2($sql);
		}
		else{
			$note = 'This Gate Pass Transferin is Generated by System';
			//$sql = "DELETE FROM `ware_gatepasstransferinheader` WHERE (`intGpTransfNo`='$serialNo') AND (`intGpTransfYear`='$year')";
			//$result1 = $db->RunQuery2($sql);

			$sql = "INSERT INTO `ware_gatepasstransferinheader` (`intGpTransfNo`,`intGpTransfYear`,intGatePassNo,intGatePassYear,strNote,intStatus,intApproveLevels,datdate,dtmCreateDate,intUser,intCompanyId,isSystemGenerated) 
					VALUES ('$serialNo','$year','$gpNo','$gpYear','$note','$transfApproveLevel','$ApproveLevels',now(),now(),'$userId','$tolocation',1)";
			$result = $db->RunQuery2($sql);
			$maxAppByStatus=(int)getMaxAppByStatus($serialNo,$year)+1;
			$sql = "UPDATE `ware_gatepasstransferinheader_approvedby` SET intStatus ='$maxAppByStatus' 
					WHERE (`intGpTransfNo`='$serialNo') AND (`intYear`='$year') AND (`intStatus`='0')";
			$result1 = $db->RunQuery2($sql);
		}
		//-----------delete and insert to detail table-----------------------
		if(($result) && ($rollBackFlag!=1)){
		$sql = "DELETE FROM `ware_gatepasstransferindetails` WHERE (`intGpTransfNo`='$serialNo') AND (`intGpTransfYear`='$year')";
		$result2 = $db->RunQuery2($sql);
			$toSave=0;
			$saved=0;
			$rollBackFlag=0;
			$rollBackMsg ="Maximum Qtys for items are..."; 
			
				// Getting Returned Note Details
		
		$sql = "SELECT
ware_returntostoresdetails.intReturnNo,
ware_returntostoresdetails.intReturnYear,
ware_returntostoresdetails.strOrderNo,
ware_returntostoresdetails.intOrderYear,
ware_returntostoresdetails.strStyleNo,
ware_returntostoresdetails.intItemId,
sum(ware_returntostoresdetails.dblQty) as dblQty
FROM `ware_returntostoresdetails`
WHERE intReturnNo = '$returnNo' AND intReturnYear = '$returnedYear'
GROUP BY
ware_returntostoresdetails.strOrderNo,
ware_returntostoresdetails.intOrderYear,
ware_returntostoresdetails.strStyleNo,
ware_returntostoresdetails.intItemId";
		$IssueNoteDetails = $db->RunQuery2($sql);
			
			
				while($arrVal = mysqli_fetch_array($IssueNoteDetails))
			{
				$location 	= $_SESSION['CompanyID'];
				$company 	= $_SESSION['headCompanyId'];
				$orderNo 	 = $arrVal['strOrderNo'];
				$orderYear 	 = $arrVal['intOrderYear'];
	
				$salesOrderId 		 = $arrVal['strStyleNo'];
				$itemId 	 = $arrVal['intItemId'];
				$Qty 		 = $arrVal['dblQty'];
		

				//------check maximum ISSUE Qty--------------------
				 $sqlc = "SELECT ware_gatepassheader.intStatus,
				mst_item.strName as itemName,
				tb1.intGatePassNo,
				tb1.intGatePassYear,
				tb1.strOrderNo,
				tb1.intOrderYear,
				tb1.strStyleNo,
				tb1.dblQty,
				IFNULL((SELECT
				Sum(ware_gatepasstransferindetails.dblQty)
				FROM
				ware_gatepasstransferinheader
				Inner Join ware_gatepasstransferindetails ON ware_gatepasstransferinheader.intGpTransfNo = ware_gatepasstransferindetails.intGpTransfNo AND ware_gatepasstransferinheader.intGpTransfYear = ware_gatepasstransferindetails.intGpTransfYear
				WHERE
				ware_gatepasstransferinheader.intGatePassNo =  tb1.intGatePassNo AND
				ware_gatepasstransferinheader.intGatePassYear =  tb1.intGatePassYear AND
				ware_gatepasstransferindetails.strOrderNo =  tb1.strOrderNo AND
				ware_gatepasstransferindetails.intOrderYear =  tb1.intOrderYear AND
				ware_gatepasstransferindetails.strStyleNo =  tb1.strStyleNo AND
				ware_gatepasstransferindetails.intItemId =  tb1.intItemId AND
				ware_gatepasstransferinheader.intStatus > '0' AND 
				ware_gatepasstransferinheader.intStatus <= ware_gatepasstransferinheader.intApproveLevels),0) as dblTransferQty
				FROM
				ware_gatepassdetails as tb1 
				Inner Join ware_gatepassheader ON tb1.intGatePassNo = ware_gatepassheader.intGatePassNo AND tb1.intGatePassYear = ware_gatepassheader.intGatePassYear
				Inner Join mst_item ON tb1.intItemId = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
				WHERE 
				ware_gatepassheader.intStatus = '1' AND 
				ware_gatepassheader.intGatePassNo =  '$gpNo' AND 
				ware_gatepassheader.intGatePassYear =  '$gpYear' AND 
				tb1.intItemId =  '$itemId' ";
				
				if($orderNo!='')
				$sqlc.=" AND tb1.strOrderNo =  '$orderNo'";
				if($orderYear!='')
				$sqlc.=" AND tb1.intOrderYear =  '$orderYear'";
				if($salesOrderId!='')
				$sqlc.=" AND tb1.strStyleNo =  '$salesOrderId'";		
				$sql_gp	=$sqlc;			
				//echo $sqlc;
				$resultsc = $db->RunQuery2($sqlc);
				$rowc = mysqli_fetch_array($resultsc);
				$item=$rowc['itemName'];

				//if($rowc['dblQty']-$rowc['dblIssudQty']<$stockBalQty)
				$balQtyToTransf = $rowc['dblQty']-$rowc['dblTransferQty'];
				if($editMode==1){
					 $balQtyToTransf=$balQtyToTransf+$Qty;
				//	$balQtyToTransf +=	$Qty;
				}
			//	else
				//$balQtyToIssue = $stockBalQty;
				if($Qty>$balQtyToTransf){
				//	call roll back--------****************
					$rollBackFlag=1;
					if($rowc['strOrderNo']!=0){
					$rollBackMsg .="\n ".$orderNo."/".$orderYear."-".$salesOrderId."-".$item." =".$balQtyToTransf;
					}
					else{
					$rollBackMsg .="\n "."-".$item." =".$balQtyToTransf;
					}
					//exit();
				}

				//----------------------------
				if($rollBackFlag!=1){
				if($orderNoArray[0]==''){
					$orderNoArray[0]=0;
				}
				if($orderNoArray[1]==''){
					$orderNoArray[1]=0;
				}
					$Qty=round($Qty,4);
					$sql = "INSERT INTO `ware_gatepasstransferindetails` (`intGpTransfNo`,`intGpTransfYear`,`strOrderNo`,`intOrderYear`,`strStyleNo`,`intItemId`,`dblQty`) 
					VALUES ('$serialNo','$year','$orderNo','$orderYear','$salesOrderId','$itemId','$Qty')";
					$result3 = $db->RunQuery2($sql);
					if($result3==1){
					$saved++;
					}
				}
				$toSave++;
			}
		}
		//echo $rollBackFlag;
		
		//$rollBackFlag=1;
		
		if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= $sql_gp;
		}
		else if(($savableFlag==1) &&(!$result)){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= "Header saving error";
			$response['q'] 			= $sql;
		}
		else if(($savableFlag==1) &&($toSave==0) &&($saved==0)){
			$response['type'] 		= 'fail';
			$response['msg'] 		= "Details saving error";
			$response['q'] 			= '';
		}
		else if(($result) && ($toSave==$saved)){
			
			$response['type'] 		= 'pass';
			if($editMode==1)
			$response['msg'] 		= 'Updated successfully.';
			else
			$response['msg'] 		= 'Saved successfully.';
			
			$response['serialNo'] 		= $serialNo;
			$response['year'] 		= $year;
		}
		else{
		
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}

			return $response;
	}
	
	//-----------------------------------------	
	function getNextTransfNo($toCompany)
	{
		global $db;
		 $sql = "SELECT
				sys_no.intGpTransfNo
				FROM sys_no
				WHERE
				sys_no.intCompanyId =  '$toCompany'
				";	
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$nextTransfNo = $row['intGpTransfNo'];
		
		$sql = "UPDATE `sys_no` SET intGpTransfNo=intGpTransfNo+1 WHERE (`intCompanyId`='$toCompany')  ";
		$db->RunQuery2($sql);
		
		return $nextTransfNo;
	}
	
	function gatepassTransferInApprove($transfNo,$year){
   global $db;
   global $userId;
		
		$rollBackFlag=0;
		$rollBackMsg = '';
		
		$sql = "UPDATE `ware_gatepasstransferinheader` SET `intStatus`=intStatus-1 WHERE (intGpTransfNo='$transfNo') AND (`intGpTransfYear`='$year') and intStatus not in(0,1)";
		$result = $db->RunQuery2($sql);
		if((!$result) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sql;
			$rollBackMsg = "Approval error!";
		}
		
		if($result){
			  $sql = "SELECT  ware_gatepasstransferinheader.intUser, 
			  		ware_gatepasstransferinheader.intStatus, 
					ware_gatepasstransferinheader.intApproveLevels, 
					ware_gatepasstransferinheader.intCompanyId as location,
					mst_locations.intCompanyId as companyId 
					FROM
					ware_gatepasstransferinheader 
					Inner Join mst_locations ON ware_gatepasstransferinheader.intCompanyId = mst_locations.intId 
					WHERE (intGpTransfNo='$transfNo') AND (`intGpTransfYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$createdUser = $row['intUser'];
			$status = $row['intStatus'];
			$savedLevels = $row['intApproveLevels'];
			$company = $row['companyId'];
			$location = $row['location'];
			
			$approveLevel=$savedLevels+1-$status;
			$FirstapproveLevel = $approveLevel-1;
			$sqlI = "INSERT INTO `ware_gatepasstransferinheader_approvedby` (`intGpTransfNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
				VALUES ('$transfNo','$year','$FirstapproveLevel','$userId',now(),0)";
			$sqlII = "INSERT INTO `ware_gatepasstransferinheader_approvedby` (`intGpTransfNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
				VALUES ('$transfNo','$year','$approveLevel','$userId',now(),0)";
			$resultI = $db->RunQuery2($sqlI);
			$resultII = $db->RunQuery2($sqlII);
			if((!$resultI) && (!$resultII) && ($rollBackFlag!=1)){
				$rollBackFlag=1;
				$sqlM=$sqlI;
				$rollBackMsg = "Approval error!";
			}
			
		
		//--------------------------------------------------------------------
		if($status==1){//if final confirmation raised,insert to stock
				$sql1 = "SELECT 
						 ware_gatepasstransferindetails.strOrderNo,
						 ware_gatepasstransferindetails.intOrderYear, 
						 ware_gatepasstransferindetails.strStyleNo,
 						 ware_gatepasstransferindetails.dblQty,
						 ware_gatepasstransferindetails.intItemId,  
						 ware_gatepasstransferinheader.intGatePassNo,
						 ware_gatepasstransferinheader.intGatePassYear,
						 ware_gatepassheader.intCompanyId,  
						 ware_gatepassheader.intGPToLocation,
						 mst_locations.intCompanyId AS MasterCompany,						 
						 mst_item.intBomItem 
						FROM 
						 ware_gatepasstransferindetails  
						Inner Join ware_gatepasstransferinheader ON ware_gatepasstransferindetails.intGpTransfNo = ware_gatepasstransferinheader.intGpTransfNo AND ware_gatepasstransferindetails.intGpTransfYear = ware_gatepasstransferinheader.intGpTransfYear
						Inner Join ware_gatepassheader ON ware_gatepasstransferinheader.intGatePassNo = ware_gatepassheader.intGatePassNo AND ware_gatepasstransferinheader.intGatePassYear = ware_gatepassheader.intGatePassYear
						Inner Join mst_item ON  ware_gatepasstransferindetails.intItemId = mst_item.intId
						Inner Join mst_locations ON  ware_gatepassheader.intGPToLocation = mst_locations.intId
						WHERE
						 ware_gatepasstransferindetails.intGpTransfNo =  '$transfNo' AND
						 ware_gatepasstransferindetails.intGpTransfYear =  '$year'";
				$result1 = $db->RunQuery2($sql1);
				$toSavedQty=0;
				$transSavedQty=0;
				
				while($row=mysqli_fetch_array($result1))
				{
					$toSavedQty+=round($row['dblQty'],4);
					$gpNo=$row['intGatePassNo'];
					$gpYear=$row['intGatePassYear'];
					$item=$row['intItemId'];
					$Qty=$row['dblQty'];
					$orderNo=$row['strOrderNo'];
					$orderYear=$row['intOrderYear'];
					$styleNo=$row['strStyleNo'];
					$locationId=$row['intCompanyId'];
					$company=$row['MasterCompany'];
					
					if($row['strOrderNo']!=0){//bom item
						//$resultG = getGrnWiseStockBalance($locationId,$gpNo,$gpYear,$orderNo,$orderYear,$styleNo,$item);
						$resultG = getGrnWiseStockBalance_bulk_GatePass_transferIN($locationId,$gpNo,$gpYear,$item);
						while($rowG=mysqli_fetch_array($resultG)){
								if(($Qty>0) && (($rowG['gpQty']*(-1))>0)){
									if($Qty<=$rowG['gpQty']*(-1)){
									$saveQty=$Qty;
									$Qty=0;
									}
									else if($Qty>$rowG['gpQty']*(-1)){
									$saveQty=$rowG['gpQty']*(-1);
									$Qty=$Qty-$saveQty;
									}
									$grnNo=$rowG['intGRNNo'];
									$grnYear=$rowG['intGRNYear'];
									$grnDate=$rowG['dtGRNDate'];
									$grnRate=$rowG['dblGRNRate'];	
									$currency=loadCurrency($grnNo,$grnYear,$item);
									$saveQty=round($saveQty,4);
									if($saveQty>0){ 
									/*$sqlI = "INSERT INTO `ware_stocktransactions` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intOrderNo,intOrderYear,intSalesOrderId,intItemId,dblQty,strType,intUser,dtDate) 
										VALUES ('$company','$location','$transfNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$orderNo','$orderYear','$styleNo','$item','$saveQty','TRANSFERIN','$userId',now())";*/
									$sqlI = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intOrderNo,intOrderYear,intSalesOrderId,intUser,dtDate) 
										VALUES ('$company','$location','$transfNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$item','$saveQty','TRANSFERIN',$orderNo,'$orderYear','$styleNo','$userId',now())";
									$resultI = $db->RunQuery2($sqlI);
									if($resultI){
										$transSavedQty+=$saveQty;
									}
									if((!$resultI) && ($rollBackFlag!=1)){
										$rollBackFlag=1;
										$sqlM=$sqlI;
										$rollBackMsg = "Approval error!";
									}
									
									}
								}
						}
					}
					else{//general item
						$resultG = getGrnWiseStockBalance_bulk_GatePass_transferIN($locationId,$gpNo,$gpYear,$item);
						while($rowG=mysqli_fetch_array($resultG)){
								if(($Qty>0) && (($rowG['gpQty']*(-1))>0)){
									if($Qty<=$rowG['gpQty']*(-1)){
									$saveQty=$Qty;
									$Qty=0;
									}
									else if($Qty>$rowG['gpQty']*(-1)){
									$saveQty=$rowG['gpQty']*(-1);
									$Qty=$Qty-$saveQty;
									}
									$grnNo=$rowG['intGRNNo'];
									$grnYear=$rowG['intGRNYear'];
									$grnDate=$rowG['dtGRNDate'];
									$grnRate=$rowG['dblGRNRate'];	
									$currency=loadCurrency($grnNo,$grnYear,$item);
									$saveQty=round($saveQty,4);
									if($saveQty>0){
									$sqlI = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intUser,dtDate) 
										VALUES ('$company','$location','$transfNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$item','$saveQty','TRANSFERIN','$userId',now())";
									$resultI = $db->RunQuery2($sqlI);
									if($resultI){
										$transSavedQty+=$saveQty;
									}
									if((!$resultI) && ($rollBackFlag!=1)){
										$rollBackFlag=1;
										$sqlM=$sqlI;
										$rollBackMsg = "Approval error!";
									}
									
									}
								}
						}
					}
				}
			}
		//--------------------------------------------------------------------
			if($status==$savedLevels){//if first confirmation raised,gp qty field in po table shld update
			 	$sql1 = "SELECT
						 ware_gatepasstransferindetails.intItemId,
						 ware_gatepasstransferindetails.dblQty,
						 ware_gatepasstransferindetails.strOrderNo,
						 ware_gatepasstransferindetails.intOrderYear, 
						 ware_gatepasstransferindetails.strStyleNo,
						 ware_gatepasstransferinheader.intGatePassNo,
						 ware_gatepasstransferinheader.intGatePassYear
						FROM
						ware_gatepasstransferindetails
						Inner Join ware_gatepasstransferinheader ON ware_gatepasstransferindetails.intGpTransfNo = ware_gatepasstransferinheader.intGpTransfNo AND ware_gatepasstransferindetails.intGpTransfYear = ware_gatepasstransferinheader.intGpTransfYear
						WHERE
						 ware_gatepasstransferinheader.intGpTransfNo =  '$transfNo' AND
						 ware_gatepasstransferinheader.intGpTransfYear =  '$year'";
				$result1 = $db->RunQuery2($sql1);
				while($row=mysqli_fetch_array($result1))
				{
					$orderNo=$row['strOrderNo'];
					$orderYear=$row['intOrderYear'];
					$styleNo=$row['strStyleNo'];
					$gpNo=$row['intGatePassNo'];
					$gpYear=$row['intGatePassYear'];
					$item=$row['intItemId'];
					$Qty=$row['dblQty'];
					
					$sql = "UPDATE `ware_gatepassdetails` SET 	dblTransferQty = dblTransferQty+'$Qty' 
							WHERE 
							 ware_gatepassdetails.strOrderNo =  '$orderNo' AND
							 ware_gatepassdetails.intOrderYear =  '$orderYear' AND
							 ware_gatepassdetails.strStyleNo =  '$styleNo' AND
							 ware_gatepassdetails.intGatePassNo =  '$gpNo' AND
							 ware_gatepassdetails.intGatePassYear =  '$gpYear' AND
							 ware_gatepassdetails.intItemId =  '$item' ";
					$result = $db->RunQuery2($sql);
					if((!$result) && ($rollBackFlag!=1)){
						$rollBackFlag=1;
						$sqlM=$sql;
						$rollBackMsg = "Approval error!";
					}
				}
			}
		//---------------------------------------------------------------------------
		}
		
		if($status==1 ){
			//echo "sdfdsfdwf";
			//update_po_prn_details_for_gp_qty($gatePassNo,$year);
			update_po_prn_details_transactions_gatepass_transferIN($transfNo,$year);
		}
		
		$toSavedQty=round($toSavedQty,4);
		$transSavedQty=round($transSavedQty,4);
		
		if(($toSavedQty!=$transSavedQty) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$rollBackMsg = "Approval Error";
			$sqlM=$toSavedQty."-".$transSavedQty;
		}
		
		if(($rollBackFlag==0) &&($status==1)){

			$response['type'] 		= 'pass';
			$response['msg'] 		= "Final Approval raised sucessfully";
			$response['q'] 			= $sql;
		}
		else if($rollBackFlag==0){

			$response['type'] 		= 'pass';
			$response['msg'] 		= "Approval raised sucessfully";
			$response['q'] 			= '';
		}
		else if($rollBackFlag==1){

			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= $sqlM;
		}
		else{
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sqlM;
		}
		
		//----------------------------------------
			return $response;
		//-----------------------------------------	
		
	
	
	
}

function getGrnWiseStockBalance_bulk_GatePass_transferIN($location,$gpNo,$gpYear,$item)
{
	global $db;
	 $sql = "SELECT
			Sum(ware_stocktransactions_bulk.dblQty) AS gpQty,
			ware_stocktransactions_bulk.intGRNNo,
			ware_stocktransactions_bulk.intGRNYear,
			ware_stocktransactions_bulk.dtGRNDate,
			ware_stocktransactions_bulk.dblGRNRate
			FROM ware_stocktransactions_bulk
			WHERE
			ware_stocktransactions_bulk.intItemId =  '$item' AND
			ware_stocktransactions_bulk.strType =  'GATEPASS' AND
			ware_stocktransactions_bulk.intDocumentNo =  '$gpNo' AND 
			ware_stocktransactions_bulk.intDocumntYear =  '$gpYear' AND 
			ware_stocktransactions_bulk.intLocationId =  '$location'
			GROUP BY
			ware_stocktransactions_bulk.intItemId,
			ware_stocktransactions_bulk.intGRNNo,
			ware_stocktransactions_bulk.intGRNYear, 
			ware_stocktransactions_bulk.dblGRNRate 
			ORDER BY
			ware_stocktransactions_bulk.dtGRNDate ASC";

	$result = $db->RunQuery2($sql);
	return $result;	
}

function update_po_prn_details_transactions_gatepass_transferIN($transfNo,$year){
	global $db;
	$sql="SELECT
			ware_gatepasstransferindetails.strOrderNo,
			ware_gatepasstransferindetails.intOrderYear,
			ware_gatepasstransferindetails.strStyleNo,
			ware_gatepasstransferindetails.intItemId,
			ware_gatepasstransferindetails.dblQty,
			ware_gatepasstransferinheader.intCompanyId AS `from`,
			trn_po_prn_details_sales_order_stock_transactions.ORDER_NO AS saved_pri
			FROM
			ware_gatepasstransferindetails
			INNER JOIN ware_gatepasstransferinheader ON ware_gatepasstransferindetails.intGpTransfNo = ware_gatepasstransferinheader.intGpTransfNo AND ware_gatepasstransferindetails.intGpTransfYear = ware_gatepasstransferinheader.intGpTransfYear
			LEFT JOIN trn_po_prn_details_sales_order_stock_transactions ON ware_gatepasstransferindetails.strOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO AND ware_gatepasstransferindetails.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR AND ware_gatepasstransferindetails.strStyleNo = trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER AND ware_gatepasstransferindetails.intItemId = trn_po_prn_details_sales_order_stock_transactions.ITEM  
			AND (trn_po_prn_details_sales_order_stock_transactions.TRANSACTION='GATE_PASS_IN' ) 
			AND trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID =ware_gatepasstransferinheader.intCompanyId
			WHERE
			ware_gatepasstransferindetails.intGpTransfNo = '$transfNo' AND
			ware_gatepasstransferindetails.intGpTransfYear = '$year'
			";
	$result = $db->RunQuery2($sql);
	while($row	=mysqli_fetch_array($result)){
		$orderNo			= $row['strOrderNo'];
		$orderYear			= $row['intOrderYear'];
		$so					= $row['strStyleNo'];
		$item				= $row['intItemId'];
		$totQty				= $row['dblQty'];
		$saved_pri				= $row['saved_pri'];
		$location			= $row['from'];
		if($saved_pri > 0){
        //PRI ITEM
            $sql_u	="UPDATE `trn_po_prn_details_sales_order_stock_transactions` SET `QTY`=IFNULL(`QTY`,0)+'$totQty' 
		  WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`SALES_ORDER`='$so') AND (`ITEM`='$item') AND (`TRANSACTION`='GATE_PASS_IN') AND (`LOCATION_ID`='$location')";
            $result_u = $db->RunQuery2($sql_u);
        }
		else{
			$sql_u2	="INSERT INTO `trn_po_prn_details_sales_order_stock_transactions` (`ORDER_NO`, `ORDER_YEAR`, `SALES_ORDER`, `ITEM`, `LOCATION_ID`, `TRANSACTION`, `QTY`) VALUES ('$orderNo', '$orderYear', '$so', '$item', '$location', 'GATE_PASS_IN', '$totQty')";
			$result_u2 	= $db->RunQuery2($sql_u2);
		}
		
 	}
}

function updateAzureDBTables($retStoreNo, $retStoreYear, $db)
{
    global $objAzure;
    $environment = $objAzure->getEnvironment();
    $table = 'Inventory'.$environment;

    $azure_connection = $objAzure->connectAzureDB();
                $sql_select = "SELECT
                CONCAT(
                    ret_store_details.intReturnNo,
                    '/',
                    ret_store_details.intReturnYear
                ) AS ret_no,
                CONCAT(
                    ret_store_header.intIssueNo,
                    '/',
                    ret_store_header.intIssueYear
                ) AS issue_no,
                ret_store_header.intCompanyId AS location,
                ret_store_details.strOrderNo,
                ret_store_details.intOrderYear,
                ret_store_details.strStyleNo,
                trn_orderdetails.strSalesOrderNo,
                trn_orderdetails.intSalesOrderId,
                ret_store_details.intItemId,
                Sum(ret_store_details.dblQty) AS dblQty,
                mst_item.strCode AS item_code,
                mst_item.strName AS description,
                mst_maincategory.strName AS mainCategory,
                mst_subcategory.strName AS subCategory,
                mst_maincategory.intSendDetails AS sendMainCat,
                mst_subcategory.intSendDetails AS sendSubCat,
                ware_issueheader.intDepartment AS dept_code,
                mst_supplier.strCode AS vendor_no,
                ware_mrnheader.mrnType,
                (
                    SELECT
                        sum(
                            (
                                ware_grnheader.dblExchangeRate * ware_stocktransactions_bulk.dblQty * ware_stocktransactions_bulk.dblGRNRate
                            ) / 1.0000
                        ) / sum(
                            ware_stocktransactions_bulk.dblQty
                        )
                    FROM
                        ware_stocktransactions_bulk
                    LEFT JOIN ware_grnheader ON ware_grnheader.intGrnNo = ware_stocktransactions_bulk.intGRNNo
                    AND ware_grnheader.intGrnYear = ware_stocktransactions_bulk.intGRNYear
                    WHERE
                        ware_stocktransactions_bulk.intDocumentNo = ret_store_details.intReturnNo
                    AND ware_stocktransactions_bulk.intDocumntYear = ret_store_details.intReturnYear
                    AND ware_stocktransactions_bulk.intItemId = ret_store_details.intItemId
                    AND ware_stocktransactions_bulk.strType = 'RETSTORES'
                    GROUP BY
                        ware_stocktransactions_bulk.intDocumentNo,
                        ware_stocktransactions_bulk.intDocumntYear,
                        ware_stocktransactions_bulk.intItemId
                ) AS grn_rate
            FROM
                ware_returntostoresdetails ret_store_details
            LEFT JOIN trn_orderdetails ON ret_store_details.strOrderNo = trn_orderdetails.intOrderNo
            AND ret_store_details.intOrderYear = trn_orderdetails.intOrderYear
            AND ret_store_details.strStyleNo = trn_orderdetails.intSalesOrderId
            INNER JOIN mst_item ON ret_store_details.intItemId = mst_item.intId
            INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
            INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
            INNER JOIN ware_mrnheader ON ware_mrnheader.intMrnNo = ret_store_details.intMrnNo
            AND ware_mrnheader.intMrnYear = ret_store_details.intMrnYear
            INNER JOIN ware_returntostoresheader ret_store_header ON ret_store_header.intReturnNo = ret_store_details.intReturnNo
            AND ret_store_header.intReturnYear = ret_store_details.intReturnYear
            INNER JOIN ware_issueheader ON ware_issueheader.intIssueNo = ret_store_header.intIssueNo
            AND ware_issueheader.intIssueYear = ret_store_header.intIssueYear
            LEFT JOIN mst_supplier ON ware_issueheader.intSupplier = mst_supplier.intId
            INNER JOIN mst_department ON ware_issueheader.intDepartment = mst_department.intId
            WHERE
                ret_store_details.intReturnNo = '$retStoreNo'
            AND ret_store_details.intReturnYear = '$retStoreYear'
            GROUP BY
                ret_store_details.strOrderNo,
                ret_store_details.intOrderYear,
                ret_store_details.strStyleNo,
                ret_store_details.intItemId";

    $result = $db->RunQuery2($sql_select);
    $line_no = rand(100,1000);
    while ($row = mysqli_fetch_array($result)) {
        $ret_no = $row['ret_no'];
        $mrn_no = $row['MRN_NO'];
        $item_category = $row['mainCategory'];
        $item_subcategory = $row['subCategory'];
        $sendDetails = ($row['sendMainCat'] == 1 && $row['sendSubCat'] == 1)? true:false;
        $item_code = $row['item_code'];
        $description = str_replace("'","''",$row['description']);
        $amount = round($row['grn_rate'] * $row['dblQty'],2) ;
        $location = $row['location'];
        $mrn_type = $row['mrnType'];
        $line_no++;
        $ex_docNo = $row['issue_no'];
        $dept_no = $row['dept_code'];
        $supplier = $row['vendor_no'];
        $successHeader = '0';
        $postingDate = date('Y-m-d H:i:s');

        if($sendDetails) {
            $sql_azure_header = "INSERT INTO $table (Document_Category, Document_No, Line_No, External_Doc_No, Posting_Date, Item_Category, Item_Sub_Category, Item_Code, Description, Amount, Location, Department_Code, MRN_Type, Vendor_No) VALUES ('ReturnToStockNote', '$ret_no', '$line_no', '$ex_docNo', '$postingDate', '$item_category', '$item_subcategory', '$item_code', '$description', '$amount','$location', '$dept_no','$mrn_type', '$supplier')";
            if ($azure_connection) {
                $getResultsHeader = $objAzure->runQuery($azure_connection, $sql_azure_header);
                if ($getResultsHeader != FALSE) {
                    $successHeader = '1';
                }
            }
            $sql_db_header = "INSERT INTO trn_financemodule_inventory (Document_Category, Document_No, Line_No, External_Doc_No, Posting_Date, Item_Category, Item_Sub_Category, Item_Code, Description, Amount, Location, Department_Code, MRN_Type, Vendor_No, deliveryStatus, deliveryDate) VALUES ('ReturnToStockNote', '$ret_no', '$line_no', '$ex_docNo', '$postingDate', '$item_category', '$item_subcategory', '$item_code', '$description', '$amount','$location', '$dept_no', '$mrn_type', '$supplier','$successHeader', NOW())";
            $result_db_header = $db->RunQuery2($sql_db_header);
        }

    }


}

function getNextSysNo($db,$companyId, $transaction_type){

    $current_serial_no = $companyId.'00000';
    $sql = "SELECT `$transaction_type` FROM `trn_financemodule_sysNo` WHERE `COMPANY_ID` = '$companyId'";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    if($row[$transaction_type] != NULL || $row[$transaction_type] != ''){
        $current_serial_no = $row[$transaction_type];
    }
    //update next serial no
    $sql_update = "UPDATE `trn_financemodule_sysNo` SET `$transaction_type`='$current_serial_no'+1 WHERE `COMPANY_ID`='$companyId'";
    $result = $db->RunQuery2($sql_update);
    return $current_serial_no;
}

function updateAzureDBTablesOnGP($gatePassNo, $gatePassYear, $db, $intCompanyId)
{
    global $objAzure;
    $environment = $objAzure->getEnvironment();
    $table = 'Inventory'.$environment;

    $azure_connection = $objAzure->connectAzureDB();
    $sql_select = "SELECT
                    CONCAT(
                        GPD.intGatePassNo,
                        '/',
                        GPD.intGatePassYear
                    ) AS GP_NO,
                    GPD.intItemId,
                    Sum(GPD.dblQty) AS dblQty,
                    mst_item.strCode AS item_code,
                    mst_item.strName AS description,
                    mst_item.dblLastPrice AS price,
                    mst_maincategory.strName AS mainCategory,
                    mst_subcategory.strName AS subCategory,
                    mst_maincategory.intSendDetails AS sendMainCat,
                    mst_subcategory.intSendDetails AS sendSubCat,
                    GPH.intCompanyId AS location,
                    (
                        SELECT
                            round(
                                sum(
                                    (
                                        ware_grnheader.dblExchangeRate * ware_stocktransactions_bulk.dblQty * ware_stocktransactions_bulk.dblGRNRate
                                    ) / 1.0000
                                ),
                                2
                            )
                        FROM
                            ware_stocktransactions_bulk
                        LEFT JOIN ware_grnheader ON ware_grnheader.intGrnNo = ware_stocktransactions_bulk.intGRNNo
                        AND ware_grnheader.intGrnYear = ware_stocktransactions_bulk.intGRNYear
                        WHERE
                            ware_stocktransactions_bulk.intDocumentNo = GPD.intGatePassNo
                        AND ware_stocktransactions_bulk.intDocumntYear = GPD.intGatePassYear
                        AND ware_stocktransactions_bulk.intItemId = GPD.intItemId
                        AND ware_stocktransactions_bulk.strType = 'GATEPASS'
                        GROUP BY
                            ware_stocktransactions_bulk.intDocumentNo,
                            ware_stocktransactions_bulk.intDocumntYear,
                            ware_stocktransactions_bulk.intItemId
                    ) AS amount
                FROM
                    ware_gatepassdetails GPD
                INNER JOIN ware_gatepassheader AS GPH ON GPD.intGatePassNo = GPH.intGatePassNo
                AND GPD.intGatePassYear = GPH.intGatePassYear
                INNER JOIN mst_item ON GPD.intItemId = mst_item.intId
                INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
                INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
                WHERE
                    GPD.intGatePassNo = '$gatePassNo'
                AND GPD.intGatePassYear = '$gatePassYear'
                GROUP BY
                    GPD.intItemId";

    $result = $db->RunQuery2($sql_select);
    $line_no = rand(100,1000);
    while ($row = mysqli_fetch_array($result)) {
        $gp_no = $row['GP_NO'];
        $item_category = $row['mainCategory'];
        $item_subcategory = $row['subCategory'];
        $sendDetails = ($row['sendMainCat'] == 1 && $row['sendSubCat'] == 1)? true:false;
        $item_code = $row['item_code'];
        $description = str_replace("'","''",$row['description']);
        $unit_price = $row['price'];
        $qty = $row['dblQty'];
        // GP records should be sent as negative
        $amount = $row['amount'];
        $location = $row['location'];
        $line_no++;
        $successHeader = '0';
        $postingDate = date('Y-m-d H:i:s');

        if($sendDetails) {
            $sql_azure_header = "INSERT INTO $table (Document_Category, Document_No, Line_No, External_Doc_No, Posting_Date, Item_Category, Item_Sub_Category, Item_Code, Description, Amount, Location) VALUES ('GatePass', '$gp_no', '$line_no', '$gp_no', '$postingDate', '$item_category', '$item_subcategory', '$item_code', '$description', '$amount','$location')";
            if ($azure_connection) {
                $getResultsHeader = $objAzure->runQuery($azure_connection, $sql_azure_header);
                if ($getResultsHeader != FALSE) {
                    $successHeader = '1';
                }
            }
            $sql_db_header = "INSERT INTO trn_financemodule_inventory (Document_Category, Document_No, Line_No, External_Doc_No, Posting_Date, Item_Category, Item_Sub_Category, Item_Code, Description, Amount, Location, deliveryStatus, deliveryDate) VALUES ('GatePass', '$gp_no', '$line_no','$gp_no', '$postingDate', '$item_category', '$item_subcategory', '$item_code', '$description', '$amount','$location','$successHeader', NOW())";
            $result_db_header = $db->RunQuery2($sql_db_header);
        }

    }

}

function updateAzureDBTablesOnGPIN($gpTrInNo, $gpTrInYear, $db, $intCompanyId)
{
    global $objAzure;
    $environment = $objAzure->getEnvironment();
    $table = 'Inventory'.$environment;

    $azure_connection = $objAzure->connectAzureDB();
    $sql_select = "SELECT
                CONCAT(
                    GPIND.intGpTransfNo,
                    '/',
                    GPIND.intGpTransfYear
                ) AS GP_IN_NO,
                CONCAT(
                    GPH.intGatePassNo,
                    '/',
                    GPH.intGatePassYear
                ) AS GP_NO,
                ware_gatepasstransferinheader.intCompanyId AS location,
                GPIND.intItemId,
                Sum(GPIND.dblQty) AS dblQty,
                mst_item.strCode AS item_code,
                mst_item.dblLastPrice AS price,
                mst_item.strName AS description,
                mst_maincategory.strName AS mainCategory,
                mst_subcategory.strName AS subCategory,
                mst_maincategory.intSendDetails AS sendMainCat,
                mst_subcategory.intSendDetails AS sendSubCat,
                (
                    SELECT
                        round(
                            sum(
                                (
                                    (- 1) * ware_grnheader.dblExchangeRate * ware_stocktransactions_bulk.dblQty * ware_stocktransactions_bulk.dblGRNRate
                                ) / 1.0000
                            ),
                            2
                        )
                    FROM
                        ware_stocktransactions_bulk
                    LEFT JOIN ware_grnheader ON ware_grnheader.intGrnNo = ware_stocktransactions_bulk.intGRNNo
                    AND ware_grnheader.intGrnYear = ware_stocktransactions_bulk.intGRNYear
                    WHERE
                        ware_stocktransactions_bulk.intDocumentNo = GPH.intGatePassNo
                    AND ware_stocktransactions_bulk.intDocumntYear = GPH.intGatePassYear
                    AND ware_stocktransactions_bulk.intItemId = GPIND.intItemId
                    AND ware_stocktransactions_bulk.strType = 'GATEPASS'
                    GROUP BY
                        ware_stocktransactions_bulk.intDocumentNo,
                        ware_stocktransactions_bulk.intDocumntYear,
                        ware_stocktransactions_bulk.intItemId
                ) AS amount
            FROM
                ware_gatepasstransferindetails GPIND
            INNER JOIN ware_gatepasstransferinheader ON GPIND.intGpTransfNo = ware_gatepasstransferinheader.intGpTransfNo
            AND GPIND.intGpTransfYear = ware_gatepasstransferinheader.intGpTransfYear
            INNER JOIN ware_gatepassheader GPH ON ware_gatepasstransferinheader.intGatePassNo = GPH.intGatePassNo
            AND ware_gatepasstransferinheader.intGatePassYear = GPH.intGatePassYear
            INNER JOIN mst_item ON GPIND.intItemId = mst_item.intId
            INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
            INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
            WHERE
                GPIND.intGpTransfNo = '$gpTrInNo'
            AND GPIND.intGpTransfYear = '$gpTrInYear'
            GROUP BY
                GPIND.intItemId";

    $result = $db->RunQuery2($sql_select);
    $line_no = rand(100,1000);
    while ($row = mysqli_fetch_array($result)) {
        $gp_in_no = $row['GP_IN_NO'];
        $gp_no = $row['GP_NO'];
        $line_no++;
        $item_category = $row['mainCategory'];
        $item_subcategory = $row['subCategory'];
        $item_code = $row['item_code'];
        $description = str_replace("'","''",$row['description']);
        $unit_price = $row['price'];
        $qty = $row['dblQty'];
        $amount = $row['amount'];
        $location = $row['location'];
        $successHeader = '0';
        $postingDate = date('Y-m-d H:i:s');
        $sendDetails = ($row['sendMainCat'] == 1 && $row['sendSubCat'] == 1)? true:false;

        if($sendDetails) {
            $sql_azure_header = "INSERT INTO $table (Document_Category, Document_No, Line_No, External_Doc_No, Posting_Date, Item_Category, Item_Sub_Category, Item_Code, Description, Amount, Location) VALUES ('TransferIn', '$gp_in_no', '$line_no','$gp_no', '$postingDate', '$item_category', '$item_subcategory', '$item_code', '$description', '$amount','$location')";
            if ($azure_connection) {
                $getResultsHeader = $objAzure->runQuery($azure_connection, $sql_azure_header);
                if ($getResultsHeader != FALSE) {
                    $successHeader = '1';
                }
            }
            $sql_db_header = "INSERT INTO trn_financemodule_inventory (Document_Category, Document_No, Line_No, External_Doc_No, Posting_Date, Item_Category, Item_Sub_Category, Item_Code, Description, Amount, Location, deliveryStatus, deliveryDate) VALUES ('TransferIn', '$gp_in_no', '$line_no', '$gp_no', '$postingDate', '$item_category', '$item_subcategory', '$item_code', '$description', '$amount','$location','$successHeader', NOW())";
            $result_db_header = $db->RunQuery2($sql_db_header);
        }

    }

}

?>
<?php
//////////////////////////////////////
//Created By: H.B.G Korala
/////////////////////////////////////
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$companyId 	= $_SESSION['CompanyID'];
	$requestType 	= $_REQUEST['requestType'];
	
	include "{$backwardseperator}dataAccess/Connector.php";
	require_once $backwardseperator."class/warehouse/cls_warehouse_get.php";
	$objwhouseget= new cls_warehouse_get($db);

	$programName='Return to Stores';
	$programCode='P0246';

		$db->OpenConnection();
		$db->RunQuery2('Begin');

 if($requestType=='getValidation')
	{
		$returnNo  = $_REQUEST['returnNo'];
		$returnNoArray=explode("/",$returnNo);
		
		///////////////////////
	  	$sql = "SELECT
				ware_returntostoresheader.intIssueNo,
				ware_returntostoresheader.intIssueYear,
				ware_returntostoresheader.intStatus,
				ware_returntostoresheader.intApproveLevels
				FROM ware_returntostoresheader
				WHERE
				ware_returntostoresheader.intReturnNo =  '$returnNoArray[0]' AND
				ware_returntostoresheader.intReturnYear =  '$returnNoArray[1]'";

		$result = $db->RunQuery2($sql);
		$errorFlg=0;
		$msg =""; 
		$msg1 ="";
		$row=mysqli_fetch_array($result);
		$status=$row['intStatus'];
		$approveLevels=$row['intApproveLevels'];	
		$issueNo=$row['intIssueNo'];
		$issueYear=$row['intIssueYear'];
		//--------------------------
		// check location of issueNote
		$sql="SELECT
					ware_issueheader.intCompanyId as location 
					FROM ware_issueheader
					JOIN mst_locations ON ware_issueheader.intCompanyId = mst_locations.intId
					WHERE
					ware_issueheader.intIssueNo =  '$issueNo' AND
					ware_issueheader.intIssueYear =  '$issueYear' AND 
					mst_locations.int_stock_locationin = '0' ";
	    $result = $db->RunQuery2($sql);
		$issuedLocation = mysqli_fetch_array($result);
		if($issuedLocation['location'] != null){
			$companyId = $issuedLocation['location'];
		}
		$response['arrData'] = $objwhouseget->getLocationValidation('RETSTORES',$companyId,$issueNo,$issueYear);//check for return location
		//--------------------------
		
		$confirmatonMode=loadConfirmatonMode($programCode,$status,$approveLevels,$userId);
		
		if($row['intStatus']==1){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="Final confirmation of this Return Note is already raised."; 
		}
		else if($row['intStatus']==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="This Return Note is Rejected"; 
		}
		else if($confirmatonMode==0){// 
			$errorFlg = 1;
			$msg ="No Permission to Approve"; 
		}
	
		if($errorFlg==0){
		
		        $sql = "SELECT 
		 		ware_returntostoresdetails.strOrderNo,
				ware_returntostoresdetails.intOrderYear,
				trn_orderdetails.strSalesOrderNo,
				trn_orderdetails.intSalesOrderId,
				ware_returntostoresdetails.strStyleNo,
				ware_returntostoresdetails.intItemId, 
				mst_item.intBomItem,
				mst_item.strName as itemName,
				ware_returntostoresheader.intIssueNo,
				sum(ware_issuedetails.dblQty) as dblIssue,
				ware_returntostoresheader.intIssueYear,
				sum(ware_issuedetails.dblReturnQty) as dblReturnQty , 
				ware_returntostoresdetails.dblQty as dblQty,
				ware_returntostoresheader.intApproveLevels,  
				ware_returntostoresheader.intStatus 
				FROM
				ware_returntostoresheader
				Inner Join ware_returntostoresdetails ON ware_returntostoresheader.intReturnNo = ware_returntostoresdetails.intReturnNo AND ware_returntostoresheader.intReturnYear = ware_returntostoresdetails.intReturnYear
				left Join trn_orderdetails ON ware_returntostoresdetails.strOrderNo = trn_orderdetails.intOrderNo AND ware_returntostoresdetails.intOrderYear = trn_orderdetails.intOrderYear AND ware_returntostoresdetails.strStyleNo = trn_orderdetails.intSalesOrderId
				Inner Join ware_issuedetails ON ware_returntostoresheader.intIssueNo = ware_issuedetails.intIssueNo AND ware_returntostoresheader.intIssueYear = ware_issuedetails.intIssueYear AND ware_returntostoresdetails.strOrderNo = ware_issuedetails.strOrderNo AND ware_returntostoresdetails.intOrderYear = ware_issuedetails.intOrderYear AND ware_returntostoresdetails.strStyleNo = ware_issuedetails.strStyleNo AND ware_returntostoresdetails.intItemId = ware_issuedetails.intItemId
				Inner Join ware_issueheader ON ware_issuedetails.intIssueNo = ware_issueheader.intIssueNo AND ware_issuedetails.intIssueYear = ware_issueheader.intIssueYear
				Inner Join mst_item ON ware_issuedetails.intItemId = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId  
				WHERE 
				ware_issueheader.intStatus =  '1' AND
				ware_returntostoresheader.intReturnNo =  '$returnNoArray[0]' AND
				ware_returntostoresheader.intReturnYear =  '$returnNoArray[1]' 
				group by 
			 	ware_returntostoresdetails.strOrderNo,
				ware_returntostoresdetails.intOrderYear,
				trn_orderdetails.strSalesOrderNo,
				trn_orderdetails.intSalesOrderId,
				ware_returntostoresdetails.strStyleNo,
				ware_returntostoresdetails.intItemId, 
				ware_returntostoresheader.intIssueNo ,
				ware_returntostoresheader.intIssueYear 
				";
				
		$result = $db->RunQuery2($sql);
		$errorFlg=0;
		$msg ="Maximum Return Qtys for items are..."; 
		
		$totTosaveQty=0;
		$totSavableQty=0;
		while($row=mysqli_fetch_array($result))
		{	
		
			 $issueQty=$row['dblIssue'];
			 $issueNo=$row['intIssueNo'];
			 $issueYear=$row['intIssueYear'];
			 $orderNo=$row['strOrderNo'];
			 $orderYear=$row['intOrderYear'];
			 $salesOrderId=$row['intSalesOrderId'];
			 $itemId=$row['intItemId'];
			 $status=$row['intStatus'];
			 
			 $Qty=$row['dblQty'];
			 
			$location 	= $_SESSION['CompanyID'];
			$company 	= $_SESSION['headCompanyId'];
			
			  $totReturnedQty=geTotConfirmedStockRetStoresQtys($issueNo,$issueYear,$orderNo,$orderYear,$salesOrderId,$itemId);
		//	echo $issueNo." =".$issueYear." =".$orderNo." =".$orderYear." =".$salesOrderId." =".$itemId;
			
			if($row['intStatus']==($row['intApproveLevels']+1)){//no confirmation has been raised
			 	$balQtyToReturn=$issueQty-$totReturnedQty;
			//	echo $issueQty."--".$totReturnedQty;
			}
			else{//confirmation has been raised
				$balQtyToReturn = $issueQty-$totReturnedQty+$Qty;
			//	echo $issueQty."--".$totReturnedQty."--".$Qty;
			}
			

			if($Qty>$balQtyToReturn){
				$errorFlg=1;
			if($row['strOrderNo']!=0){//BOM item
					$msg .="<br>".$row['strOrderNo']."/".$row['intOrderYear']."-".$row['strSalesOrderNo']."-".$row['itemName']." =".$balQtyToReturn;
				}
				else{
					$msg .="<br>".$row['itemName']." =".$balQtyToReturn;
				}
			}
			$totTosaveQty +=$Qty;
			$totSavableQty +=$balQtyToReturn;
		}//end of while
		}
	
	if($msg1!=''){
		$msg=$msg1;
	}
//	echo $totTosaveQty ."--". $totSavableQty;
	if(($totTosaveQty > $totSavableQty) && ($errorFlg!=1)){
		$errorFlg=1;
		$msg ="Balance to Return to stores Qty is not tally with This Qtys.";
	}
	
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);
	}
//---------------------------
else if($requestType=='validateRejecton')
	{
		$returnNo  = $_REQUEST['returnNo'];
		$returnNoArray=explode("/",$returnNo);
		
		///////////////////////
	 	 $sql = "SELECT
		ware_returntostoresheader.intStatus, 
		ware_returntostoresheader.intApproveLevels 
		FROM ware_returntostoresheader
		WHERE
		ware_returntostoresheader.intReturnNo =  '$returnNoArray[0]' AND 
		ware_returntostoresheader.intReturnYear =  '$returnNoArray[1]'";

		$result = $db->RunQuery2($sql);
		$errorFlg=0;
		$msg =""; 
		$row=mysqli_fetch_array($result);
		
		$rejectionMode=loadRejectionMode($programCode,$row['intStatus'],$row['intApproveLevels'],$userId);
		
		if($row['intStatus']==1){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="Final confirmation of this Return Note is already raised.So cant reject"; 
		}
		else if($row['intStatus']==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="This Return Note is already Rejected"; 
		}
		else if($rejectionMode==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="No Permission to reject this Return Note"; 
		}
		else{//confirmation has been raised
			$errorFlg = 0;
			
		}
	
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);
	}

		$db->RunQuery2('Commit');
		$db->CloseConnection();		
		
		
//---------------------------
		
		
	//-----------------------------------------------------------
	function getStockBalance($company,$location,$orderNo,$orderYear,$salesOrderId,$item)
	{
		global $db;
		  $sql = "SELECT
				Sum(ware_stocktransactions.dblQty) as stockBal 
				FROM ware_stocktransactions
				WHERE
				ware_stocktransactions.intCompanyId =  '$company' AND
				ware_stocktransactions.intLocationId =  '$location' AND
				ware_stocktransactions.intOrderNo =  '$orderNo' AND
				ware_stocktransactions.intOrderYear =  '$orderYear' AND
				ware_stocktransactions.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions.intItemId =  '$item'";

		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['stockBal']);	
	}
	//--------------------------------------------------------------
	function getStockBalance_bulk($location,$item)
	{
		global $db;
		   $sql = "SELECT
				Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$location'
				GROUP BY
				ware_stocktransactions_bulk.intItemId";

		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//-----------------------------------------------------------------
	function geTotConfirmedStockRetStoresQtys($issueNo,$issueYear,$orderNo,$orderYear,$salesOrderId,$item){
		global $db;
		 $sql="SELECT
				Sum(ware_returntostoresdetails.dblQty) as returnQty 
				FROM
				ware_returntostoresheader
				Inner Join ware_returntostoresdetails ON ware_returntostoresheader.intReturnNo = ware_returntostoresdetails.intReturnNo AND ware_returntostoresheader.intReturnYear = ware_returntostoresdetails.intReturnYear
				WHERE
				ware_returntostoresheader.intIssueNo =   '$issueNo' AND
				ware_returntostoresheader.intIssueYear =   '$issueYear' AND
				ware_returntostoresdetails.intItemId =   '$item' AND
				ware_returntostoresheader.intStatus <=  ware_returntostoresheader.intApproveLevels AND
				ware_returntostoresheader.intStatus >  0 AND
				ware_returntostoresdetails.strOrderNo =  '$orderNo' AND
				ware_returntostoresdetails.intOrderYear =  '$orderYear' AND
				ware_returntostoresdetails.strStyleNo =  '$salesOrderId'
			";
		
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$issuedQty = val($row['returnQty']);
			return $issuedQty;
	}
//------------------------------function loadRejectionMode-------------------
function loadRejectionMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$rejectMode=0;
	$sqlp = "SELECT
		menupermision.intReject 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery2($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intReject']==1){
	 if($intStatus!=0){
	 $rejectMode=1;
	 }
	}
	 
	return $rejectMode;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	 $sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery2($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
//--------------------------------------------------------

?>

<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
//////////////////////////////////////
//Created By: H.B.G Korala
/////////////////////////////////////
session_start();
 $companyId = $_SESSION['CompanyID'];
$intUser  = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
 
$returnNo = $_REQUEST['returnNo'];
$year = $_REQUEST['year'];
$approveMode = $_REQUEST['approveMode'];

$programName='Return to Stores';
$programCode='P0246';

/*$issueNo = '100000';
$year = '2012';
$approveMode=1;
*/
   $sql = "SELECT
mst_companies.strName as delToCompany,
mst_locations.strName as delToLocation,
ware_returntostoresheader.datdate,
ware_returntostoresheader.intStatus,
ware_returntostoresheader.intApproveLevels,
ware_returntostoresheader.intUser,
ware_returntostoresheader.intIssueNo,
ware_returntostoresheader.intIssueYear,
ware_returntostoresheader.intCompanyId as poRaisedLocationId, 
ware_returntostoresheader.intGatePassNo as GatepassNo,
sys_users.strUserName ,
(
	SELECT
		ware_mrnheader.mrnType
	FROM
		ware_mrnheader
	INNER JOIN ware_issuedetails ON ware_mrnheader.intMrnNo = ware_issuedetails.intMrnNo
	AND ware_mrnheader.intMrnYear = ware_issuedetails.intMrnYear
	where ware_issuedetails.intIssueNo = ware_returntostoresheader.intIssueNo
	AND ware_issuedetails.intIssueYear = ware_returntostoresheader.intIssueYear
	LIMIT 1
) mrnType  
FROM
ware_returntostoresheader 
Inner Join mst_locations ON ware_returntostoresheader.intCompanyId = mst_locations.intId
Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
Inner Join sys_users ON ware_returntostoresheader.intUser = sys_users.intUserId
WHERE
ware_returntostoresheader.intReturnNo =  '$returnNo' AND
ware_returntostoresheader.intReturnYear =  '$year' 
GROUP BY
ware_returntostoresheader.intReturnNo,
ware_returntostoresheader.intReturnYear
";

				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$company = $row['delToCompany'];
					$location = $row['delToLocation'];
					$locationId = $row['poRaisedLocationId'];//this locationId use in report header(reportHeader.php)--------------------
					$issueNo=$row['intIssueNo'];
					$issueYear=$row['intIssueYear'];
					$returnDate = $row['datdate'];
					$returnBy = $row['strUserName'];
					$intStatus = $row['intStatus'];
					$savedLevels = $row['intApproveLevels'];
					$user = $row['strUserName'];
					$createUserId = $row['intUser'];
					$GatepassNo = $row['GatepassNo'];
					if($row['mrnType']==0)
					$mrnType="Non RM";
					else if($row['mrnType']==1)
					$mrnType="Sample";
					else if($row['mrnType']==2)
					$mrnType="Technical R&D";
					else if($row['mrnType']==3)
					$mrnType="Production Orderwise";
					else if($row['mrnType']==5)
					$mrnType="Production Non Orderwise";
					else if($row['mrnType']==6)
					$mrnType="Production Orderwise Additional";
					else if($row['mrnType']==7)
					$mrnType="Loan settlement to Supplier";
					else if($row['mrnType']==8)
					$mrnType="Loan out to Supplier";
				 }
				 
$confirmationMode=loadConfirmatonMode($programCode,$intStatus,$savedLevels,$intUser);
$rejectionMode=loadRejectionMode($programCode,$intStatus,$savedLevels,$intUser);
?>
 <head>
 <title>Return To Stores Report</title>
 <script type="application/javascript" src="presentation/warehouse/returnToStores/listing/rptReturnToStores-js.js"></script>
 <style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:266px;
	top:170px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<?php
 if($intStatus>1)//pending
{
?>
<div id="apDiv1"><img src="images/pending.png"  /></div>
<?php
}
?>
<form id="frmReturnToStoresReport" name="frmReturnToStoresReport" method="post" action="rptReturnToStores.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include 'reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>RETURN TO STORES REPORT</strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
  <tr>
   	<td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
	<td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td colspan="9" align="center" bgcolor="#FFDFCB">
    <?php
	if($intStatus>1)
	{
	?>
    <?php if($approveMode==1){
			if($confirmationMode==1) { 
			?>
			<img src="images/approve.png" align="middle" class="mouseover noPrint" id="imgApprove" />
			<?php
			}
			if($rejectionMode==1) { 
			?>
            <img src="images/reject.png" align="middle" class="mouseover noPrint" id="imgReject" />
                <?php
                }
            }
	}
	?>
    </td>
  </tr>
  <tr>
  <?php
 	if($intStatus==1)
	{
	?>
   <td colspan="9" class="APPROVE" >CONFIRMED</td>
   <?PHP
	}
	else if($intStatus==0)
	{
   ?>
   <td colspan="9" class="APPROVE" style="color:#F00">REJECTED</td>
   <?php
	}
	else
	{
   ?>
   <td width="10%" colspan="9" class="APPROVE">PENDING</td>
   <?php
	}
   ?>
  </tr>
  <tr>
    <td width="1%">&nbsp;</td>
    <td width="11%"><span class="normalfnt"><strong>Return No</strong></span></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="28%"><span class="normalfnt"><?php echo $returnNo ?>/<?php echo $year ?></span></td>
    <td width="12%" class="normalfnt"><strong><strong>Issue No</strong></strong></td>
    <td width="2%" align="center" valign="middle"><strong>:</strong></td>
    <td width="22%"><span class="normalfnt"><?php echo $issueNo ?>/<?php echo $year ?></span></td>
    <td width="1%"><div id="divReturnNo" style="display:none"><?php echo $returnNo ?>/<?php echo $year ?></div></td>
  <td width="3%"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Return By</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $returnBy  ?></span></td>
    <td align="normalfnt" valign="middle"><strong><?php if($GatepassNo != null) { echo 'Gatepass No';} ?><strong></td>
    <td align="center" valign="middle"><strong><?php if($GatepassNo != null) { echo ':';} ?></strong></td>
    <td class="normalfnt"><?php if($GatepassNo != null) { echo $GatepassNo;} ?></td>
    <td>&nbsp;</td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Date</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $returnDate  ?></span></td>
    <td><strong>MRN Type</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td class="normalfnt"><span><?php echo $mrnType ?></span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td colspan="9" class="normalfnt">
          <table width="100%" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr  >
              <th width="10%" >MRN No</th>
              <th width="10%" >Order No</th>
              <th width="10%" >Sales Order No</th>
              <th width="10%" >Main Category</th>
              <th width="16%" >Sub Category</th>
              <th width="10%" >Item Code</th>
              <th width="20%" >Item</th>
              <th width="7%" >UOM</th>
              <th width="9%" >Qty</th>
              </tr>
              <?php 
	  	    $sql1 = "SELECT
ware_returntostoresdetails.intReturnNo,
ware_returntostoresdetails.intReturnYear,
ware_returntostoresdetails.intMrnNo,
ware_returntostoresdetails.intMrnYear,
ware_returntostoresdetails.strOrderNo,
ware_returntostoresdetails.intOrderYear,
ware_returntostoresdetails.strStyleNo,
trn_orderdetails.strSalesOrderNo,
trn_orderdetails.intSalesOrderId,
ware_returntostoresdetails.intItemId,
Sum(ware_returntostoresdetails.dblQty) as dblQty,
mst_item.strCode,
mst_item.strName,
mst_item.strCode as SUP_ITEM_CODE,
mst_item.intUOM, 
mst_item.ITEM_HIDE,
mst_maincategory.strName AS mainCategory,
mst_maincategory.intId AS mainCatId,
mst_subcategory.strName AS subCategory, 
mst_part.strName as part , 
mst_units.strCode as uom  
FROM ware_returntostoresdetails 
left Join trn_orderdetails ON ware_returntostoresdetails.strOrderNo = trn_orderdetails.intOrderNo AND ware_returntostoresdetails.intOrderYear = trn_orderdetails.intOrderYear AND ware_returntostoresdetails.strStyleNo = trn_orderdetails.intSalesOrderId
Inner Join mst_item ON ware_returntostoresdetails.intItemId = mst_item.intId
Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
left Join mst_part ON trn_orderdetails.intPart = mst_part.intId  
Inner Join mst_units ON mst_item.intUOM = mst_units.intId
LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
WHERE
ware_returntostoresdetails.intReturnNo =  '$returnNo' AND
ware_returntostoresdetails.intReturnYear =  '$year' 
GROUP BY
ware_returntostoresdetails.intMrnNo,
ware_returntostoresdetails.intMrnYear,
ware_returntostoresdetails.strOrderNo,
ware_returntostoresdetails.intOrderYear,
ware_returntostoresdetails.strStyleNo,
ware_returntostoresdetails.intItemId 
";
			$result1 = $db->RunQuery($sql1);
			$totQty=0;
			$totAmmount=0;
			while($row=mysqli_fetch_array($result1))
			{
				$mrnNo=$row['intMrnNo']."/".$row['intMrnYear'];
				$orderNo=$row['strOrderNo']."/".$row['intOrderYear'];
				$salesOrderNo=$row['strSalesOrderNo']."/".$row['part'];
                                $mainCatId=$row['mainCatId'];
                                $ITEM_HIDE=$row['ITEM_HIDE'];
                                
				if($row['strOrderNo']==0){
					$orderNo='';
					$salesOrderNo='';	
				}
	  ?>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfnt" >&nbsp;<?php echo $mrnNo;?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $orderNo;?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $salesOrderNo?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['mainCategory']?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['subCategory'] ?>&nbsp;</td>
              <?php
                if($row['SUP_ITEM_CODE']!=null)
                {
                    ?>
                
                 <td class="normalfnt" >&nbsp;<?php echo $row['SUP_ITEM_CODE'] ?>&nbsp;</td>  
                 <?php
                }else{
                ?>
              <td class="normalfnt" >&nbsp;<?php echo $row['strCode'] ?>&nbsp;</td>
              
              <?php
                }
              if($ITEM_HIDE==1)
              {
                  ?>
                <td class="normalfnt" >&nbsp;*****&nbsp;</td>
              <?php
              }else{
                  
              ?>
              <td class="normalfnt" >&nbsp;<?php echo $row['strName'] ?>&nbsp;</td>
              <?php
              }
              ?>
              <td class="normalfntMid" ><?php echo $row['uom'] ?></td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['dblQty'] ?>&nbsp;</td>
              </tr>
      <?php 
			$totQty+=$row['dblQty'];
			}
	  ?>
            <tr class="normalfnt"  bgcolor="#CCCCCC">
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfntRight" >&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $totQty ?>&nbsp;</td>
              </tr>
            </table>
          </td>
        </tr>
      
      </table>
    </td>
</tr>
<?php  
$flag=0;
					  $sqlc = "SELECT
							ware_returntostoresheader_approvedby.intApproveUser,
							ware_returntostoresheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName,
							ware_returntostoresheader_approvedby.intApproveLevelNo
							FROM
							ware_returntostoresheader_approvedby
							Inner Join sys_users ON ware_returntostoresheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							ware_returntostoresheader_approvedby.intReturnNo =  '$returnNo' AND
							ware_returntostoresheader_approvedby.intYear =  '$year'  order by dtApprovedDate asc
";
					 $resultc = $db->RunQuery($sqlc);
					while($rowc=mysqli_fetch_array($resultc)){
						if($rowc['intApproveLevelNo']==1)
						$desc="1st ";
						else if($rowc['intApproveLevelNo']==2)
						$desc="2nd ";
						else if($rowc['intApproveLevelNo']==3)
						$desc="3rd ";
						else
						$desc=$rowc['intApproveLevelNo']."th ";
					 //  $desc=$ap.$desc;
					 $desc2=$rowc['UserName']."(".$rowc['dtApprovedDate'].")";
					 if($rowc['UserName']=='')
					 $desc2='---------------------------------';
					 
					 if($rowc['intApproveLevelNo']>0){
						 $flag=1;
				?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> Approved By - </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
            </tr>
<?php
	//}
	//else{			
					}
					 if($rowc['intApproveLevelNo']==0){
						 $flag=2;
					  ?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong> Rejected By - </strong></span><span class="normalfnt"><?php echo $rowc['UserName']."(".$rowc['dtApprovedDate'].")";?></span></td>
            </tr>
<?php
					 }
					 if($rowc['intApproveLevelNo']==-1){
						 $flag=3;
					  ?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong> Revised By - </strong></span><span class="normalfnt"><?php echo $rowc['UserName']."(".$rowc['dtApprovedDate'].")";?></span></td>
            </tr>
<?php
					 }
					 
		 if($rowc['intApproveLevelNo']>0){
			 $finalSavedLevel=$rowc['intApproveLevelNo'];
		 }
	 }//end of while
	
	
	//echo $flag;
	if($flag>1){
		$finalSavedLevel=0;
	}
	
	if(($flag<=1) || ($intStatus>0)){
	for($j=$finalSavedLevel+1; $j<=$savedLevels; $j++)
	{ 
		if($j==1)
		$desc="1st ";
		else if($j==2)
		$desc="2nd ";
		else if($j==3)
		$desc="3rd ";
		else
		$desc=$j."th ";
		$desc2='---------------------------------';
	?>
        <tr>
            <td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> Approved By - </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
        </tr>
<?php
	}
	}
?>
<tr height="90" >
  <td align="center" class="normalfntMid"></td>
</tr>
<tr height="40">
<?php
	//$locationId = '';
	$sql = "SELECT mst_locations.intCompanyId FROM mst_locations WHERE mst_locations.intId =  '$locationId'";
	$result 	= $db->RunQuery($sql);
	$row		= mysqli_fetch_array($result);
	$createCompanyId = $row['intCompanyId'];
?>
<?Php 
	$url  = "{$backwardseperator}presentation/sendToApproval.php";		// * file name
	$url .= "?status=$intStatus";										// * set recent status
	$url .= "&approveLevels=$savedLevels";								// * set approve levels in order header
	$url .= "&programCode=$programCode";								// * program code (ex:P10001)
	$url .= "&program=RETURN TO STORES NOTE";									// * program name (ex:Purchase Order)
	$url .= "&companyId=$createCompanyId";									// * created company id
	$url .= "&createUserId=$createUserId";	
	
	$url .= "&field1=Return No";												 
	$url .= "&field2=Return Year";	
	$url .= "&value1=$returnNo";												 
	$url .= "&value2=$year";	
	
	$url .= "&subject=RETURN TO STORES NOTE FOR APPROVAL ('$returnNo'/'$year')";	
	
	$url .= "&statement1=Please Approve this";	
	$url .= "&statement2=to Approve this";	
							// * doc year
	$url .= "&link=".urlencode(base64_encode($mainPath."presentation/warehouse/returnToStores/listing/rptReturnToStores.php?returnNo=$returnNo&year=$year&approveMode=1"));
?>
  <td align="center" class="normalfntMid"><iframe id="iframeFiles2" src="<?php echo $url;?>" name="iframeFiles" style="width:500px;height:150px;border:none"  ></iframe></td>
</tr>
<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
</body>
</html>
<?php
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	$sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
//------------------------------function loadRejectionMode-------------------
function loadRejectionMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$rejectMode=0;
	$sqlp = "SELECT
		menupermision.intReject 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intReject']==1){
	 if($intStatus!=0){
	 $rejectMode=1;
	 }
	}
	 
	return $rejectMode;
}
//-------------------------------------------------------------------------
?>	

//////////////////////////////////////
//Created By: H.B.G Korala
/////////////////////////////////////
var basePath = "presentation/warehouse/returnToStores/listing/";
// JavaScript Document

$(document).ready(function() {
	$('#frmReturnToStoresReport').validationEngine();
	
	$('#frmReturnToStoresReport #imgApprove').click(function(){
	//	alert("pp");
	var val = $.prompt('Are you sure you want to approve this Return Note ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					if(validateQuantities()==0){
					///////////
					var url = basePath+"rptReturnToStores-db-set.php"+window.location.search+'&status=approve';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmReturnToStoresReport #imgApprove').validationEngine('showPrompt', json.msg,json.type );
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmReturnToStoresReport #imgApprove').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						}
 					   hideWaiting();
					}
				}});
	});
	
	$('#frmReturnToStoresReport #imgReject').click(function(){
	var val = $.prompt('Are you sure you want to reject this Return Note ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
					showWaiting();
					if(validateRejecton()==0){
					///////////
					var url = basePath+"rptReturnToStores-db-set.php"+window.location.search+'&status=reject';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmReturnToStoresReport #imgReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmReturnToStoresReport #imgReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						}
 					   hideWaiting();
					}
				}});
	});
	
});
//---------------------------------------------------
function validateQuantities(){
		var ret=0;	
	    var returnNo = document.getElementById('divReturnNo').innerHTML;
		var url 		= basePath+"rptReturnToStores-db-get.php?requestType=getValidation";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"returnNo="+returnNo,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#imgApprove').validationEngine('showPrompt',json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertx()",1000);
					  ret= 1;
					}
				}
			});
			return ret;
}
//---------------------------------------------------
function validateRejecton(){
		var ret=0;	
	    var returnNo = document.getElementById('divReturnNo').innerHTML;
		var url 		= basePath+"rptReturnToStores-db-get.php?requestType=validateRejecton";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"returnNo="+returnNo,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#imgApprove').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertx()",1000);
					  ret= 1;
					}
				}
			});
			return ret;
}
//------------------------------------------------
function alertx()
{
	$('#frmReturnToStoresReport #imgApprove').validationEngine('hide')	;
}
//--------------------------------------------
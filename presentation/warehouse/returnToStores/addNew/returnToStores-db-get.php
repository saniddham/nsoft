<?php
//////////////////////////////////////
//Created By: H.B.G Korala
/////////////////////////////////////
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	///
	/////////// type of print load part /////////////////////
if($requestType=='loadIssueNos')
	{
		$issueYear  = $_REQUEST['issueYear'];
		$type  = $_REQUEST['type'];
		$sql = "SELECT DISTINCT 
				ware_issueheader.intIssueNo ,
				(SELECT
				ware_mrnheader.mrnType
				FROM
				ware_issuedetails
				INNER JOIN ware_mrnheader ON ware_issuedetails.intMrnNo = ware_mrnheader.intMrnNo AND ware_issuedetails.intMrnYear = ware_mrnheader.intMrnYear
				WHERE
				ware_issuedetails.intIssueNo = ware_issueheader.intIssueNo AND
				ware_issuedetails.intIssueYear = ware_issueheader.intIssueYear 
				limit 1) as mrnType 
				FROM ware_issueheader
				WHERE
				ware_issueheader.intStatus =  '1' AND 
				ware_issueheader.intCompanyId =  '$location' ";
		if($issueYear!=''){		
		$sql .= " AND ware_issueheader.intIssueYear =  '$issueYear' ";	
		}
		if($type==1 || $type=='')
		$sql	.= " HAVING (mrnType<>7 AND mrnType<>8 or mrnType is null) ";
		else
		$sql	.= " HAVING (mrnType=7 OR mrnType=8) ";

		$sql .= " order by intIssueNo desc, intIssueYear desc ";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intIssueNo']."\">".$row['intIssueNo']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
else if($requestType=='loadSubCategory')
	{
		$mainCategory  = $_REQUEST['mainCategory'];
		$sql = "SELECT
				mst_subcategory.intId,
				mst_subcategory.strCode,
				mst_subcategory.strName
				FROM mst_subcategory
				WHERE
				mst_subcategory.intMainCategory =  '$mainCategory' 
				ORDER BY mst_subcategory.strName ASC ";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	else if($requestType=='loadStyleNo')
	{
		$orderNo  = $_REQUEST['orderNo'];
		$orderNoArray 	 = explode('/',$orderNo);
		$issueNo  = $_REQUEST['issueNo'];
		$year  = $_REQUEST['year'];
		
		 /*$sql = "SELECT DISTINCT
				ware_stocktransactions.intSalesOrderId,
				trn_orderdetails.strSalesOrderNo, 
				mst_part.strName
				FROM
				ware_stocktransactions
				Inner Join trn_orderdetails ON ware_stocktransactions.intOrderNo = trn_orderdetails.intOrderNo AND ware_stocktransactions.intOrderYear = trn_orderdetails.intOrderYear AND ware_stocktransactions.intSalesOrderId = trn_orderdetails.intSalesOrderId 
				Inner Join mst_part ON trn_orderdetails.intPart = mst_part.intId 
				WHERE 
				ware_stocktransactions.intOrderNo =  '$orderNoArray[0]' AND
				ware_stocktransactions.intOrderYear =  '$orderNoArray[1]'";*/
				
				$sql = "SELECT DISTINCT
				ware_stocktransactions_bulk.intSalesOrderId,
				trn_orderdetails.strSalesOrderNo, 
				mst_part.strName
				FROM
				ware_stocktransactions_bulk
				Inner Join trn_orderdetails ON ware_stocktransactions_bulk.intOrderNo = trn_orderdetails.intOrderNo AND ware_stocktransactions_bulk.intOrderYear = trn_orderdetails.intOrderYear AND ware_stocktransactions_bulk.intSalesOrderId = trn_orderdetails.intSalesOrderId 
				Inner Join mst_part ON trn_orderdetails.intPart = mst_part.intId 
				WHERE 
				ware_stocktransactions_bulk.intOrderNo =  '$orderNoArray[0]' AND
				ware_stocktransactions_bulk.intOrderYear =  '$orderNoArray[1]'";
				
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intSalesOrderId']."\">".$row['strSalesOrderNo']."/".$row['strName']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	else if($requestType=='loadItems')
	{
		$issueNo  = $_REQUEST['issueNo'];
		$year  = $_REQUEST['year'];
		$orderNo  = $_REQUEST['orderNo'];
		$orderNoArray 	 = explode('/',$orderNo);
		$salesOrderId  = $_REQUEST['salesOrderId'];
		$mainCategory  = $_REQUEST['mainCategory'];
		$subCategory  = $_REQUEST['subCategory'];
		$description  = $_REQUEST['description'];
		

		if($salesOrderNo=='null'){
			$salesOrderNo='';
		}
		if($orderNoArray[0]==''){
			$orderNoArray[0]=0;
		}
		if($orderNoArray[1]==''){
			$orderNoArray[1]=0;
		}
		
		  $sql="select * from (SELECT
				mst_item.intId,
				mst_item.intMainCategory,
				mst_item.intSubCategory,
				mst_item.strCode,
				mst_item.strName as itemName,
				mst_item.intBomItem,
				mst_item.intUOM,
				mst_item.dblLastPrice,
                                mst_item.ITEM_HIDE,
                                mst_item.strCode as SUP_ITEM_CODE,
				mst_maincategory.strName as mainCatName,
				mst_subcategory.strName as subCatName,   
				tb1.intIssueNo,
				tb1.intIssueYear,
				tb1.intMrnNo,
				tb1.intMrnYear,
				tb1.strOrderNo,
				tb1.intOrderYear,
				trn_orderdetails.strSalesOrderNo,
				trn_orderdetails.intSalesOrderId,
				sum(tb1.dblQty) AS dblQty,
				IFNULL((SELECT
				Sum(ware_returntostoresdetails.dblQty)
				FROM
				ware_returntostoresheader
				Inner Join ware_returntostoresdetails ON ware_returntostoresheader.intReturnNo = ware_returntostoresdetails.intReturnNo AND ware_returntostoresheader.intReturnYear = ware_returntostoresdetails.intReturnYear
				WHERE
				ware_returntostoresheader.intIssueNo =  tb1.intIssueNo AND
				ware_returntostoresheader.intIssueYear =  tb1.intIssueYear AND 
				ware_returntostoresdetails.strOrderNo =  tb1.strOrderNo AND
				ware_returntostoresdetails.intOrderYear =  tb1.intOrderYear AND
				ware_returntostoresdetails.strStyleNo =  tb1.strStyleNo  AND
				ware_returntostoresdetails.intItemId =  tb1.intItemId AND
				ware_returntostoresheader.intStatus <=ware_returntostoresheader.intApproveLevels AND
				ware_returntostoresheader.intStatus > 0 ),0) as dblReturnQty, 
				mst_part.strName as part  , 
				mst_units.strCode as uom 
				FROM
				ware_issuedetails as tb1   
				Inner Join ware_issueheader ON tb1.intIssueNo = ware_issueheader.intIssueNo AND tb1.intIssueYear = ware_issueheader.intIssueYear
				left Join trn_orderdetails ON tb1.strOrderNo = trn_orderdetails.intOrderNo AND tb1.intOrderYear = trn_orderdetails.intOrderYear AND tb1.strStyleNo = trn_orderdetails.intSalesOrderId 
				left Join mst_part ON trn_orderdetails.intPart = mst_part.intId 
				Inner Join mst_item ON tb1.intItemId = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId
                                LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
				WHERE 
				ware_issueheader.intStatus = '1' AND 
				tb1.intIssueNo =  '$issueNo' AND 
				tb1.intIssueYear =  '$year'";
				
			///	if($orderNoArray[0]!='')
				$sql.=" AND tb1.strOrderNo =  '$orderNoArray[0]' ";
			//	if($orderNoArray[1]!='')
				$sql.=" AND tb1.intOrderYear =  '$orderNoArray[1]' ";
				if($salesOrderId!='')
				$sql.=" AND tb1.strStyleNo =  '$salesOrderId' ";
				if($mainCategory!='')
				$sql.=" AND mst_item.intMainCategory =  '$mainCategory' ";
				if($subCategory!='')
				$sql.=" AND mst_item.intSubCategory =  '$subCategory'";
				if($description!=''){
				$sql.=" AND
				(mst_item.strName LIKE  '%$description%' OR mst_item.strCode LIKE  '%$description%')";
				}
				$sql.=" group by 
				tb1.intMrnNo,
				tb1.intMrnYear,
				tb1.strOrderNo,
				tb1.intOrderYear,
				tb1.strStyleNo,
				mst_item.intId
				) as tb1 where tb1.dblQty-tb1.dblReturnQty>0";	

		  //echo $sql;
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$issueQty = $row['dblQty'];
			if(!$issueQty)
			$issueQty=0;
			$returnQty = $row['dblReturnQty'];
			if(!$returnQty)
			$returnQty=0;
			
			if($row['strOrderNo']==0){//general item
			$stockBalQty=getStockBalance_bulk($location,$row['intId']);
			}
			else{//bom item
			//$stockBalQty=getStockBalance($company,$location,$orderNoArray[0],$orderNoArray[1],$row['intSalesOrderId'],$row['intId']);
			$stockBalQty=getStockBalance_bulk($location,$row['intId']);
			}
			$data['issueNum'] 	= $row['intIssueNo'];
			$data['issueYear'] 	= $row['intIssueYear'];
			$data['itemId'] 	= $row['intId'];
			$data['maincatId'] = $row['intMainCategory'];
			$data['subCatId'] 	= $row['intSubCategory'];
			$data['code'] = $row['strCode'];
			$data['itemName'] = $row['itemName'];
                        $data['supItemCode'] = $row['SUP_ITEM_CODE'];
                        $data['ITEM_HIDE'] = $row['ITEM_HIDE'];
			$data['uom'] 	= $row['uom'];
			$data['unitPrice'] = $row['dblLastPrice'];
			$data['mainCatName'] = $row['mainCatName'];
			$data['subCatName'] = $row['subCatName'];
			$balToreturnQty = $issueQty - $returnQty;
			$data['issueQty'] = round($issueQty,4);
			$data['returnQty'] = round($returnQty,4);
			$data['balToreturnQty'] = round($balToreturnQty,4);			
			$data['stockBalQty'] = round($stockBalQty,4);
			$data['intMrnNo'] 	= $row['intMrnNo'];
			$data['intMrnYear'] 	= $row['intMrnYear'];
			
		if($orderNoArray[0]==''){
			$data['intOrderNo'] 	= '0';
			$data['intOrderYear'] 	= '0';
			$data['salesOrderId'] 	= '';
			$data['salesOrderNo'] 	= '';
			$data['strStyleNo'] 	= '';
		}
		else{
			$data['intOrderNo'] 	= $row['strOrderNo'];
			$data['intOrderYear'] 	= $row['intOrderYear'];
			$data['salesOrderId'] 	= $row['intSalesOrderId'];
			$data['salesOrderNo'] 	= $row['strSalesOrderNo']."/".$row['part'];
			$data['strStyleNo'] 	= $row['strStyleNo'];
		}
			
			
			$arrCombo[] = $data;
		}
		
		if($arrCombo==null){
			$arrCombo[0]="none";
		}
		
		$response['arrCombo'] 	= $arrCombo;
		echo json_encode($response);

	}
	
	//-----------------------------------------------------------
	function getStockBalance($company,$location,$orderNo,$orderYear,$salesOrderId,$item)
	{
		global $db;
		  $sql = "SELECT
				Sum(ware_stocktransactions.dblQty) as stockBal 
				FROM ware_stocktransactions
				WHERE
				ware_stocktransactions.intCompanyId =  '$company' AND
				ware_stocktransactions.intLocationId =  '$location' AND
				ware_stocktransactions.intOrderNo =  '$orderNo' AND
				ware_stocktransactions.intOrderYear =  '$orderYear' AND
				ware_stocktransactions.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions.intItemId =  '$item'";

		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
	function getStockBalance_bulk($location,$item)
	{
		global $db;
		   $sql = "SELECT
				Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$location'
				GROUP BY
				ware_stocktransactions_bulk.intItemId";

		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
?>
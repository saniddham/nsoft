<?php 
//////////////////////////////////////
//Created By: H.B.G Korala
/////////////////////////////////////
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$companyId 	= $_SESSION['CompanyID'];
	$headCompany 	= $_SESSION['headCompanyId'];
	include_once "{$backwardseperator}dataAccess/Connector.php";
	require_once $backwardseperator."class/warehouse/cls_warehouse_get.php";
 	require_once "../../../../class/cls_commonFunctions_get.php";
	require_once "../../../../class/finance/budget/cls_common_function_budget_get.php";
	require_once "../../../../class/finance/budget/entering/cls_budget_get.php";
	require_once "../../../../class/finance/budget/entering/cls_budget_set.php";
	require_once "../../../../class/warehouse/returnToStores/cls_returnToStores_get.php";
	
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$objwhouseget			= new cls_warehouse_get($db);
	$obj_returnToStores_get = new cls_returnToStores_get($db);
	$obj_common				= new cls_commonFunctions_get($db);
	$obj_comm_budget_get	= new cls_common_function_budget_get($db);
	$obj_budget_get			= new cls_budget_get($db);
	$obj_budget_set			= new cls_budget_set($db);
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	
	$serialNo 	 = $_REQUEST['serialNo'];
	$year 		 = $_REQUEST['Year'];
	$issueNo 	 = $_REQUEST['issueNo'];
	$issueYear 	 = $_REQUEST['issueYear'];
	$date 		 = $_REQUEST['date'];
	
	$arr 		= json_decode($_REQUEST['arr'], true);
	
	$programName='Return to Stores';
	$programCode='P0246';
	
	$ApproveLevels = (int)getApproveLevel('Return to Stores');
	$storesReturnApproveLevel = $ApproveLevels+1;
	
//------------save---------------------------	
	if($requestType=='save')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		$savableFlag=1;
		
		if($serialNo==''){
			$serialNo 	= getNextReturnNo();
			$year = date('Y');
			$editMode=0;
			$savedStatus=$storesReturnApproveLevel;
			$savedLevels=$ApproveLevels;
		}
		else{
			$editMode=1;
			$savableFlag=getSaveStatus($serialNo,$year);
			$sql = "SELECT
			ware_returntostoresheader.intStatus, 
			ware_returntostoresheader.intApproveLevels 
			FROM ware_returntostoresheader 
			WHERE
			ware_returntostoresheader.intReturnNo =  '$serialNo' AND
			ware_returntostoresheader.intReturnYear =  '$year'";

			$result = $db->RunQuery2($sql);
			$row=mysqli_fetch_array($result);
			$savedStatus=$row['intStatus'];
			$savedLevels=$row['intApproveLevels'];
		}

		//--------------------------
		$response['arrData'] = $objwhouseget->getLocationValidation('RETSTORES',$companyId,$issueNo,$issueYear);//check for return location
		//--------------------------
		$editPermission		= loadEditMode($programCode,$savedStatus,$savedLevels,$userId);
		
		//
		$sql = "SELECT int_stock_locationin,intCompanyId from mst_locations where intId = '$companyId'";

			$result = $db->RunQuery2($sql);
			$checkStockMaintain = mysqli_fetch_array($result);
			
			if($checkStockMaintain['int_stock_locationin'] == 1){
				$locationEditFlag	= getEditLocationValidation('RETSTORES',$companyId,$serialNo,$year);
			}
			else{			
				// getting head office location as return note location
			$mainComapny = $checkStockMaintain['intCompanyId'];
			$sql = "SELECT intId from mst_locations where intCompanyId = '$mainComapny' AND COMPANY_MAIN_STORES_LOCATION = '1'";
			$result = $db->RunQuery2($sql);
			$MainComanyResult = mysqli_fetch_array($result);
			$MainComanyID = $MainComanyResult['intId'];
			$locationEditFlag	= getEditLocationValidation('RETSTORES',$MainComanyID,$serialNo,$year);
				
			}

		//
		$return_header		= $obj_returnToStores_get->get_header($serialNo,$year,'RunQuery2');
		/*$results_fin_year	=$obj_comm_budget_get->loadFinanceYear_with_selected_date($company,$header_array['MRN_DATE'],'RunQuery2');
		$row_fin_year		=mysqli_fetch_array($results_fin_year);
		$budg_year			=$row_fin_year['intId'];
		$budget_header		= $obj_budget_get->get_header($return_header['LOCATION_ID'],$return_header['DEPARTMENT_ID'],$budg_year,'RunQuery2');
		*/
		//$response_budget_validation	= get_budget_validation();
		//--------------------------
		
		if($response['arrData']['type']=='fail'){
			 $rollBackFlag=1;
			 $rollBackMsg=$response['arrData']['msg'];
		 }
		 else if(( $locationEditFlag==1) && ($editMode==1)){
			 $rollBackFlag=1;
			 $rollBackMsg="Invalid Edit Location";
		 }
		else if($savableFlag==0){
			 $rollBackFlag=1;
			 $rollBackMsg="This Return No is already confirmed.cant edit";
		 }
		 else if($editPermission==0){
			 $rollBackFlag=1;
			 $rollBackMsg="No Permission to save.";
		 }
		/* else if($budget_header['STATUS'] != 1){
			 $rollBackFlag=1;
			 $rollBackMsg="There is no Approved Budget for mrn Month.";
		 }*/
		else if($editMode==1){//edit existing grn
			$sql = "UPDATE `ware_returntostoresheader` SET intStatus ='$storesReturnApproveLevel', 
														   intApproveLevels ='$ApproveLevels', 
														  intModifiedBy ='$userId', 
														  dtmModifiedDate =now() 
					WHERE (`intReturnNo`='$serialNo') AND (`intReturnYear`='$year')";
			$result = $db->RunQuery2($sql);
			
			//inactive previous recordrs in approvedby table
			$maxAppByStatus=(int)getMaxAppByStatus($serialNo,$year)+1;
			$sql = "UPDATE `ware_returntostoresheader_approvedby` SET intStatus ='$maxAppByStatus' 
					WHERE (`intReturnNo`='$serialNo') AND (`intYear`='$year') AND (`intStatus`='0')";
			$result1 = $db->RunQuery2($sql);
			
		}
		else{
			// check current company is stock maintaining company 
			
			$sql = "SELECT int_stock_locationin,intCompanyId from mst_locations where intId = '$companyId'";

			$result = $db->RunQuery2($sql);
			$checkStockMaintain = mysqli_fetch_array($result);
			
			if($checkStockMaintain['int_stock_locationin'] == 1){
				$sql = "INSERT INTO `ware_returntostoresheader` (`intReturnNo`,`intReturnYear`,`intIssueNo`,intIssueYear,intStatus,intApproveLevels,datdate,dtmCreateDate,intUser,intCompanyId) 
					VALUES ('$serialNo','$year','$issueNo','$issueYear','$storesReturnApproveLevel','$ApproveLevels',now(),now(),'$userId','$companyId')";
			$result = $db->RunQuery2($sql);
			}
			else{
				
				// getting head office location as return note location
			$mainComapny = $checkStockMaintain['intCompanyId'];
			$sql = "SELECT intId from mst_locations where intCompanyId = '$mainComapny' AND COMPANY_MAIN_STORES_LOCATION = '1'";
			$result = $db->RunQuery2($sql);
			$MainComanyResult = mysqli_fetch_array($result);
			$MainComanyID = $MainComanyResult['intId'];
			$sql = "INSERT INTO `ware_returntostoresheader` (`intReturnNo`,`intReturnYear`,`intIssueNo`,intIssueYear,intStatus,intApproveLevels,datdate,dtmCreateDate,intUser,intCompanyId) 
					VALUES ('$serialNo','$year','$issueNo','$issueYear','$storesReturnApproveLevel','$ApproveLevels',now(),now(),'$userId','$companyId')";
			$result = $db->RunQuery2($sql);
				
			}
			

			
		}
		//-----------delete and insert to detail table-----------------------
		if(($result) && ($rollBackFlag!=1)){
		$sql = "DELETE FROM `ware_returntostoresdetails` WHERE (`intReturnNo`='$serialNo') AND (`intReturnYear`='$year')";
		$result2 = $db->RunQuery2($sql);
			$toSave=0;
			$saved=0;
			$rollBackFlag=0;
			$rollBackMsg ="Maximum Return Qtys for items are..."; 
			$allocatedFlag=1;
			foreach($arr as $arrVal)
			{
				$location 	= $_SESSION['CompanyID'];
				$company 	= $_SESSION['headCompanyId'];
				$orderNo 	 = $arrVal['orderNo'];
				$orderNoArray   = explode('/',$orderNo);
				$mrnNo 	 = $arrVal['mrnNo'];
				$mrnNoArray   = explode('/',$mrnNo);
				if($orderNoArray[0]==''){
					$orderNoArray[0]=0;
					$allocatedFlag=0;
				}
				else{
					$allocatedFlag=1;
				}
				
				if($orderNoArray[1]==''){
					$orderNoArray[1]=0;
				}
				$salesOrderId = $arrVal['salesOrderId'];
				$itemId 	 = $arrVal['itemId'];
				$Qty 		 = $arrVal['Qty'];
		
				//------check maximum ISSUE Qty--------------------
			  $sqlc = "SELECT 
				mst_item.strName as itemName,
				ware_issuedetails.intIssueNo,
				ware_issuedetails.intIssueYear,
				ware_issuedetails.strOrderNo,
				ware_issuedetails.intOrderYear,
				ware_issuedetails.strStyleNo,
				sum(ware_issuedetails.dblQty) as dblQty,
				sum(ware_issuedetails.dblReturnQty ) as dblReturnQty
				FROM
				ware_issuedetails 
				Inner Join ware_issueheader ON ware_issuedetails.intIssueNo = ware_issueheader.intIssueNo AND ware_issuedetails.intIssueYear = ware_issueheader.intIssueYear
				Inner Join mst_item ON ware_issuedetails.intItemId = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
				WHERE 
				ware_issueheader.intStatus = '1' AND 
				ware_issuedetails.intIssueNo =  '$issueNo' AND 
				ware_issuedetails.intIssueYear =  '$issueYear' AND 
				ware_issuedetails.intItemId =  '$itemId' ";
				
				if($mrnNoArray[0]!='')
				$sqlc.=" AND ware_issuedetails.intMrnNo =  '$mrnNoArray[0]'";
				if($mrnNoArray[1]!='')
				$sqlc.=" AND ware_issuedetails.intMrnYear =  '$mrnNoArray[1]'";
				if($orderNoArray[0]!='')
				$sqlc.=" AND ware_issuedetails.strOrderNo =  '$orderNoArray[0]'";
				if($orderNoArray[1]!='')
				$sqlc.=" AND ware_issuedetails.intOrderYear =  '$orderNoArray[1]'";
				if($salesOrderId!='')
				$sqlc.=" AND ware_issuedetails.strStyleNo =  '$salesOrderId'";		
				
				$resultsc = $db->RunQuery2($sqlc);
				$rowc = mysqli_fetch_array($resultsc);
				$item=$rowc['itemName'];
				 $totReturnedQty=geTotConfirmedReturnedQtys($issueNo,$issueYear,$orderNoArray[0],$orderNoArray[1],$salesOrderId,$itemId,$mrnNoArray[0],$mrnNoArray[1]);
                 
				$balQtyToReturn=$rowc['dblQty']-$totReturnedQty;
				
				if($Qty>$balQtyToReturn){
				//	call roll back--------****************
					$rollBackFlag=1;
					if($allocatedFlag==1){
					$rollBackMsg .="<br>".$orderNoArray[0]."/".$orderNoArray[1]."-".$salesOrderId."-".$issueNo."/".$issueYear."-".$item." =".$balQtyToReturn;
					}
					else{
					$rollBackMsg .="<br>".$issueNo."/".$issueYear."-".$item." =".$balQtyToReturn;
					}
					//exit();
				}

				//----------------------------
				if($rollBackFlag!=1){
				if($orderNoArray[0]==''){
					$orderNoArray[0]=0;
				}
				if($orderNoArray[1]==''){
					$orderNoArray[1]=0;
				}
				$Qty=round($Qty,4);
					$sql = "INSERT INTO `ware_returntostoresdetails` (`intReturnNo`,`intReturnYear`,`strOrderNo`,`intOrderYear`,`strStyleNo`,`intItemId`,`dblQty`,`intMrnNo`,`intMrnYear`) 
					VALUES ('$serialNo','$year','$orderNoArray[0]','$orderNoArray[1]','$salesOrderId','$itemId','$Qty','$mrnNoArray[0]','$mrnNoArray[1]')";
					$result3 = $db->RunQuery2($sql);
					if($result3==1){
					$saved++;
					}
				}
				$toSave++;
			}
		}
		//echo $rollBackFlag;
		
		if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= '';
		}
		else if(($savableFlag==1) &&(!$result)){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= "Header saving error";
			$response['q'] 			= $sql;
		}
		else if(($savableFlag==1) &&($toSave==0) &&($saved==0)){
			$response['type'] 		= 'fail';
			$response['msg'] 		= "Details saving error";
			$response['q'] 			= '';
		}
		else if(($result) && ($toSave==$saved)){
			$confirmationMode=getConfirmationMode($programCode,$userId,$ApproveLevels,$storesReturnApproveLevel);
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			if($editMode==1)
			$response['msg'] 		= 'Updated successfully.';
			else
			$response['msg'] 		= 'Saved successfully.';
			
			$response['serialNo'] 		= $serialNo;
			$response['year'] 		= $year;
			$response['confirmationMode'] = $confirmationMode;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		
		$db->CloseConnection();		
	}
//----------------------------------------
	echo json_encode($response);
//-----------------------------------------	
	function getNextReturnNo()
	{
		global $db;
		global $companyId;
		$sql = "SELECT
				sys_no.intReturnToStores
				FROM sys_no
				WHERE
				sys_no.intCompanyId =  '$companyId'
				";	
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$nextReturnNo = $row['intReturnToStores'];
		
		$sql = "UPDATE `sys_no` SET intReturnToStores=intReturnToStores+1 WHERE (`intCompanyId`='$companyId')  ";
		$db->RunQuery2($sql);
		
		return $nextReturnNo;
	}
	//-----------------------------------------------------------
	function getStockBalance($company,$location,$orderNo,$orderYear,$salesOrderId,$item)
	{
		global $db;
		 $sql = "SELECT
				Sum(ware_stocktransactions.dblQty) as stockBal 
				FROM ware_stocktransactions
				WHERE
				ware_stocktransactions.intCompanyId =  '$company' AND
				ware_stocktransactions.intLocationId =  '$location' AND
				ware_stocktransactions.intOrderNo =  '$orderNo' AND
				ware_stocktransactions.intOrderYear =  '$orderYear' AND
				ware_stocktransactions.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions.intItemId =  '$item'";

		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
	function getStockBalance_bulk($location,$item)
	{
		global $db;
		   $sql = "SELECT
				Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$location'
				GROUP BY
				ware_stocktransactions_bulk.intItemId";

		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
	function getSaveStatus($serialNo,$year)
	{
		global $db;
		$sql = "SELECT ware_returntostoresheader.intStatus, ware_returntostoresheader.intApproveLevels FROM ware_returntostoresheader WHERE (intReturnNo='$serialNo') AND (`intReturnYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
			
			if(($status==0) || ($row['intStatus']==($row['intApproveLevels']+1))){
				$editableFlag=1	;
			}
			else{
				$editableFlag=0;	
			}
			
		return $editableFlag;
	}
	//-----------------------------------------------------------------
	function geTotConfirmedReturnedQtys($issueNo,$issueYear,$orderNo,$orderYear,$salesOrderId,$item,$mrnNo,$mrnYear){
		global $db;
		 $sql="SELECT
	Sum(ware_returntostoresdetails.dblQty)as returnQty
FROM
	ware_returntostoresheader
	Inner Join ware_returntostoresdetails ON ware_returntostoresheader.intReturnNo = ware_returntostoresdetails.intReturnNo AND ware_returntostoresheader.intReturnYear = ware_returntostoresdetails.intReturnYear
WHERE
ware_returntostoresheader.intIssueNo =  '$issueNo' AND
ware_returntostoresheader.intIssueYear =  '$issueYear' AND
ware_returntostoresdetails.intItemId =  '$item' AND
ware_returntostoresheader.intStatus <=  ware_returntostoresheader.intApproveLevels AND
ware_returntostoresheader.intStatus >  0 AND
ware_returntostoresdetails.intMrnNo =  '$mrnNo' AND
ware_returntostoresdetails.intMrnYear =  '$mrnYear' AND
ware_returntostoresdetails.strOrderNo =  '$orderNo' AND
ware_returntostoresdetails.intOrderYear =  '$orderYear' AND
ware_returntostoresdetails.strStyleNo =  '$salesOrderId'
			";
		
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$issuedQty = val($row['returnQty']);
			return $issuedQty;
	}
	//-----------------------------------------------------------------
//-----------------------------------------------------------
	function getEditLocationValidation($type,$location,$serialNo,$year)
	{
		$flag= 1;
		
		global $db;
		  $sql = "SELECT
					ware_returntostoresheader.intCompanyId
					FROM
					ware_returntostoresheader
					Inner Join mst_locations ON ware_returntostoresheader.intCompanyId = mst_locations.intId
					WHERE
					ware_returntostoresheader.intReturnNo =  '$serialNo' AND
					ware_returntostoresheader.intReturnYear =  '$year'";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			if($location == $row['intCompanyId'])
			$flag=0;
 			
		return $flag;

}
//------------------------------function loadConfirmatonMode-------------------
function getConfirmationMode($programCode,$intUser,$approveLevels,$status){
		global $db;
		$k=$approveLevels+2-$status;
		$sqlp = "SELECT 
		menupermision.intEdit, 
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		if($rowp['int'.$k.'Approval']==1) 
		 $confirmatonMode=1; 
		else
		 $confirmatonMode=0; 
	 
	return $confirmatonMode;
}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		$resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
		 
		// $editMode;
		 
	return $editMode;
}
//------------------------------function get MaxAppByStatus---------------------
function getMaxAppByStatus($serialNo,$year){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	  $sql = "SELECT
				max(ware_returntostoresheader_approvedby.intStatus) as status 
				FROM
				ware_returntostoresheader_approvedby
				WHERE
				ware_returntostoresheader_approvedby.intReturnNo =  '$serialNo' AND
				ware_returntostoresheader_approvedby.intYear =  '$year'";
				
		$resultm = $db->RunQuery2($sql);
		 $rowm=mysqli_fetch_array($resultm);
		 
		 return $rowm['status'];

}
//--------------------------------------------------------
?>


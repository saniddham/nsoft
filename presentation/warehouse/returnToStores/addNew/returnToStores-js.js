//////////////////////////////////////
//Created By: H.B.G Korala
/////////////////////////////////////
var basePath	="presentation/warehouse/returnToStores/addNew/";
var reportId	="900";


$(document).ready(function() {
	
  		$("#frmReturnToStores").validationEngine();
		$('#frmReturnToStores #cboIssueNo').focus();
		
		$("#frmReturnToStores #butAddItems").click(function(){
			if(!$('#frmReturnToStores #cboIssueNo').val()){
				alert("Please select the Return No");
				return false;				
			}
			else if(!$('#frmReturnToStores #cboIssueYear').val()){
				alert("Please select the Year");
				return false;				
			}
			//clearRows();
			closePopUp();
			loadPopup();
		});
		
		$("#frmReturnToStores #cboIssueNo").change(function(){
			clearRows();
		});
		$("#frmReturnToStores #cboIssueYear").change(function(){
			loadIssueNos();
			clearRows();
		});
		$("#frmReturnToStores .type").change(function(){
			loadIssueNos();
			clearRows();
		});
	//--------------------------------------------
  //-------------------------------------------------------
 
  $('#frmReturnToStores #butSave').click(function(){
	var requestType = '';
	if ($('#frmReturnToStores').validationEngine('validate'))   
    { 

		var data = "requestType=save";
		
			data+="&serialNo="		+	$('#txtReturnNo').val();
			data+="&Year="	+	$('#txtReturnYear').val();
			data+="&issueNo="		+	$('#cboIssueNo').val();
			data+="&issueYear="	+	$('#cboIssueYear').val();
			data+="&date="			+	$('#dtDate').val();


			var rowCount = document.getElementById('tblRetstores').rows.length;
			if(rowCount==1){
				alert("items not selected to return");
				return false;				
			}
			var row = 0;
				errorQtyFlag=0;
			
			var arr="[";
			
			$('#tblRetstores .item').each(function(){
	
				var mrnNo	= $(this).parent().find(".mrnNo").html();
				var orderNo	= $(this).parent().find(".orderNo").html();
				var salesOrderId	= $(this).parent().find(".salesOrderNo").attr('id');
				var itemId	= $(this).attr('id');
				var Qty	= $(this).parent().find(".Qty").val();
					
					if(Qty>0){
						
						 arr += "{";
						arr += '"mrnNo":"'+	mrnNo +'",' ;
						arr += '"orderNo":"'+	orderNo +'",' ;
						arr += '"salesOrderId":"'+		salesOrderId +'",' ;
						arr += '"itemId":"'+	itemId +'",' ;
						arr += '"Qty":"'+		Qty +'"' ;
						arr +=  '},';
						if(Qty<=0){
							errorQtyFlag=1;
						}
					}
			});
			if(errorQtyFlag==1){
				alert("Please Enter Quantities");
				return false;
			}
			
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;

		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = basePath+"returnToStores-db-set.php";
     	var obj = $.ajax({
			url:url,
			type:'post',
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmReturnToStores #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t=setTimeout("alertx()",1000);
						$('#txtReturnNo').val(json.serialNo);
						$('#txtReturnYear').val(json.year);
						if(json.confirmationMode==1){
							$('#butConfirm').show();
						}
						else{
							$('#butConfirm').hide();
						}
						return;
					}
				},
			error:function(xhr,status){
					
					$('#frmReturnToStores #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
				}		
			});
	}
   });
	
//----------------------------	
$('#frmReturnToStores .delImg').click(function(){
	$(this).parent().parent().remove();
});

//----------------------------------- 
$('#frmReturnToStores #butReport').click(function(){
	if($('#txtReturnNo').val()!=''){
		window.open('?q='+reportId+'&returnNo='+$('#txtReturnNo').val()+'&year='+$('#txtReturnYear').val());	
	}
	else{
		alert("There is no Return No to view");
	}
});
//----------------------------------	
$('#frmReturnToStores #butConfirm').click(function(){
	if($('#txtReturnNo').val()!=''){
		window.open('?q='+reportId+'&returnNo='+$('#txtReturnNo').val()+'&year='+$('#txtReturnYear').val()+'&approveMode=1');	
	}
	else{
		alert("There is no Return No to confirm");
	}
});
//-----------------------------------------------------
$('#frmReturnToStores #butClose').click(function(){
		//load('main.php');	
});
//--------------refresh the form-----------------------
	$('#frmReturnToStores #butNew').click(function(){
		window.location.href = "?q=246";
		$('#frmReturnToStores').get(0).reset();
		clearRows();
		$('#frmReturnToStores #txtGrnNo').val('');
		$('#frmReturnToStores #txtGrnYear').val('');
		$('#frmReturnToStores #txtInvioceNo').val('');
		$('#frmReturnToStores #cboPO').val('');
		$('#frmReturnToStores #cboPO').focus();
		$('#frmReturnToStores #txtDeliveryNo').val('');
		$('#frmReturnToStores #txtTotGrnAmmount').val('');
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmReturnToStores #dtDate').val(d);
	});
//-----------------------------------------------------
});

//----------end of ready -------------------------------

//-------------------------------------------------------
function loadPopup()
{
			closePopUp();
		popupWindow3('1');
		var issueNo = $('#cboIssueNo').val();
		var year = $('#cboIssueYear').val();
		$('#popupContact1').load(basePath+'returnToStoresPopup.php?issueNo='+issueNo+"&year="+year+'&type='+$('input[name=type]:checked', '#frmReturnToStores').val(),function(){
				//checkAlreadySelected();
				$('#butAdd').click(addClickedRows);
				$('#butClose1').click(disablePopup);
				 //-------------------------------------------- 
				  $('#frmReturnToStoresPopup #cboOrderNo').change(function(){
						var orderNo = $('#cboOrderNo').val();
						
						var url 		= basePath+"returnToStores-db-get.php?requestType=loadStyleNo&orderNo="+orderNo+"&issueNo="+issueNo+"&year="+year;
						var httpobj 	= $.ajax({url:url,async:false})
						document.getElementById('cbStyleNo').innerHTML=httpobj.responseText;
						
				  });
				 //-------------------------------------------- 
				  $('#frmReturnToStoresPopup #cboMainCategory').change(function(){
						var mainCategory = $('#cboMainCategory').val();
						var url 		= basePath+"returnToStores-db-get.php?requestType=loadSubCategory&mainCategory="+mainCategory;
						var httpobj 	= $.ajax({url:url,async:false})
						document.getElementById('cboSubCategory').innerHTML=httpobj.responseText;
						
				  });
				 //-------------------------------------------- 
				 $('#frmReturnToStoresPopup  #chkAll').click(function(){
						if(document.getElementById('chkAll').checked==true)
						 var chk=true;
						else
						 var chk=false;
						
						var rowCount = document.getElementById('tblItemsPopup').rows.length;
						for(var i=1;i<rowCount;i++)
						{
							document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].checked=chk;
						}
					});
				 //-------------------------------------------- 
				  $('#frmReturnToStoresPopup #imgSearchItems').click(function(){
						var rowCount = document.getElementById('tblItemsPopup').rows.length;
						for(var i=1;i<rowCount;i++)
						{
							document.getElementById('tblItemsPopup').deleteRow(1);
						}
						var issueNo = $('#cboIssueNo').val();
						var year = $('#cboIssueYear').val();
						var orderNo = $('#cboOrderNo').val();
						var salesOrderId = $('#cbStyleNo').val();
						var salesOrderNo = $('#cbStyleNo option:selected').text();
						var mainCategory = $('#cboMainCategory').val();
						var subCategory = $('#cboSubCategory').val();
						var description = $('#txtItmDesc').val();
							/*if(orderNo==''){
								alert("Please select Order No");
								return false;
							}	
							if(mainCategory==''){
								alert("Please select Main Category");
								return false;
							}*/
						showWaiting();	
						var url 		= basePath+"returnToStores-db-get.php?requestType=loadItems";
						var httpobj = $.ajax({
							url:url,
							dataType:'json',
							data:"issueNo="+issueNo+"&year="+year+"&orderNo="+orderNo+"&salesOrderId="+salesOrderId+"&mainCategory="+mainCategory+"&subCategory="+subCategory+"&description="+description,
							async:false,
							success:function(json){

								var length = json.arrCombo.length;
								var arrCombo = json.arrCombo;

								if(arrCombo[0]=='none'){
									alert("Items not found.");
									return;
									//hideWaiting();
								}

								for(var i=0;i<length;i++)
								{
									var issueNum=arrCombo[i]['issueNum'];	
									var issueYear=arrCombo[i]['issueYear'];	
									var intOrderNo=arrCombo[i]['intOrderNo'];
									var intOrderYear=arrCombo[i]['intOrderYear'];
									var intMrnNo=arrCombo[i]['intMrnNo'];
									var intMrnYear=arrCombo[i]['intMrnYear'];
									var salesOrderNo=arrCombo[i]['salesOrderNo'];
									var salesOrderId=arrCombo[i]['salesOrderId'];
									var itemId=arrCombo[i]['itemId'];	
									var maincatId=arrCombo[i]['maincatId'];	
									var subCatId=arrCombo[i]['subCatId'];	
									var code=arrCombo[i]['code'];
                                                                        var supItemCode=arrCombo[i]['supItemCode'];
									var itemName=arrCombo[i]['itemName'];
                                                                        var ITEM_HIDE=arrCombo[i]['ITEM_HIDE'];
									var uom=arrCombo[i]['uom'];	
									var unitPrice=arrCombo[i]['unitPrice'];	
									var mainCatName=arrCombo[i]['mainCatName'];
									var subCatName=arrCombo[i]['subCatName'];	
									var issueQty=arrCombo[i]['issueQty'];
									var returnQty=arrCombo[i]['returnQty'];
									//var balToreturnQty=parseFloat(issueQty)-parseFloat(returnQty);
									var balToreturnQty=arrCombo[i]['balToreturnQty'];
									var stockBalQty=arrCombo[i]['stockBalQty'];
									var mrnNo=intMrnNo+"/"+intMrnYear;
									
									if((intOrderNo==0) && (intOrderYear==0)){
										var orderNo='';
										salesOrderNo='';
										salesOrderId='';
									}
									else{
										var orderNo=intOrderNo+"/"+intOrderYear;
									}
									
									if(!salesOrderNo){
										salesOrderNo = '';
									}
									
									if(!salesOrderId){
										salesOrderId = '';
									}	

										
									var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF" id=""><input type="checkbox" id="chkDisp" /></td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+mrnNo+'" class="mrnNoP">'+mrnNo+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+orderNo+'" class="orderNoPP">'+orderNo+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'" class="mainCatNameP">'+mainCatName+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'" class="subCatNameP">'+subCatName+'</td>';
                                                                        if(supItemCode!= null)
                                                                        {
                                                                        content +='<td align="center" bgcolor="#FFFFFF" id="'+code+'" class="codeP">'+supItemCode+'</td>';  
                                                                        }else{
                                                                        content +='<td align="center" bgcolor="#FFFFFF" id="'+code+'" class="codeP">'+code+'</td>';
                                                                            }
									
                                                                        if(ITEM_HIDE==1)
                                                                        {
                                                                        content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="itemP">*****</td>';    
                                                                        }else{
									content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="itemP">'+itemName+'</td>';
                                                                            }
									content +='<td align="center" bgcolor="#FFFFFF" id="'+uom+'" class="uomP">'+uom+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+orderNo+'" style="display:none" class="orderNoP">'+orderNo+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+salesOrderId+'" style="display:none" class="salesOrderNoP">'+salesOrderNo+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+issueNum+"/"+issueYear+'" style="display:none" class="issueNumP">'+issueNum+"/"+issueYear+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+issueQty+'"  class="issueQtyP">'+issueQty+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+returnQty+'" class="returnQtyP" >'+returnQty+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+balToreturnQty+'" class="balToreturnQtyP">'+balToreturnQty+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+stockBalQty+'"  class="stockBalQtyP" >'+stockBalQty+'</td></tr>';

									add_new_row('#frmReturnToStoresPopup #tblItemsPopup',content);
								}
									checkAlreadySelected();

							}
						});hideWaiting();
						
				  });
				  //-------------------------------------------------------
			});	
}
//-------------------------------------------------------
function addClickedRows()
{
	var rowCount = document.getElementById('tblItemsPopup').rows.length;

	var i=0;
	$('#tblItemsPopup .itemP').each(function(){
		i++;
		if((document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].checked==true) && (document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].disabled==false)){
			
			var intMrnNo=$(this).parent().find(".mrnNoP").attr('id');
			var intOrderNo=$(this).parent().find(".orderNoP").attr('id');
			var salesOrderNo=$(this).parent().find(".salesOrderNoP").html();
			var salesOrderId=$(this).parent().find(".salesOrderNoP").attr('id');
			var itemId=$(this).attr('id');
			var maincatId=$(this).parent().find(".mainCatNameP").attr('id');
			var subCatId=$(this).parent().find(".subCatNameP").attr('id');
			var code=$(this).parent().find(".codeP").html();
			var uom=$(this).parent().find(".uomP").html();
			var itemName=$(this).parent().find(".itemP").html();
			var mainCatName=$(this).parent().find(".mainCatNameP").html();
			var subCatName=$(this).parent().find(".subCatNameP").html();
			var issueQty=parseFloat($(this).parent().find(".issueQtyP").html());
			var returnQty=parseFloat($(this).parent().find(".returnQtyP").html());
			var stockBalance=parseFloat($(this).parent().find(".stockBalQtyP").html());
			var balToReturnQty=parseFloat($(this).parent().find(".balToreturnQtyP").html());
			//var balToReturnQty=issueQty-returnQty;
			//if(stockBalance<balToReturnQty){
			var maxRetQty=balToReturnQty;

			var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF"><img class="delImg" src="images/del.png" width="15" height="15" /></td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+intMrnNo+'" class="mrnNo">'+intMrnNo+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+intOrderNo+'" class="orderNo">'+intOrderNo+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+salesOrderId+'" class="salesOrderNo">'+salesOrderNo+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'" class="mainCatName">'+mainCatName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'" class="subCatName">'+subCatName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="item">'+itemName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+uom+'" class="uom">'+uom+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+issueQty+'" class="issueQty">'+issueQty+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+returnQty+'" class="returnQty">'+returnQty+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+balToReturnQty+'" class="balToReturnQty">'+balToReturnQty+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id=""><input  id="'+maxRetQty+'" class="validate[required,custom[number],max['+maxRetQty+']] calculateValue Qty" style="width:80px;text-align:center" type="text" value="'+maxRetQty+'"/></td>';
			content +='</tr>';
		
			add_new_row('#frmReturnToStores #tblRetstores',content);
		//----------------------------	
		$('.delImg').click(function(){
			$(this).parent().parent().remove();
		});
		
		}
	});
	disablePopup();
	closePopUp();
}
//-------------------------------------
function alertx()
{
	$('#frmReturnToStores #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmReturnToStores #butDelete').validationEngine('hide')	;
}

function closePopUp(){
	
}
//----------------------------------------------
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
//----------------------------------------------- 
function clearRows()
{
	//var rowCount = $('#tblDispatchPopup >tbody >tr').length;
	var rowCount = document.getElementById('tblRetstores').rows.length;
	//alert(rowCount);
	//var cellCount = document.getElementById('tblMain').rows[1].cells.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblRetstores').deleteRow(1);
			
	}
}
//------------------------------------------------------------
/*function checkAlreadySelected_Duplicate(){
	var rowCount = document.getElementById('tblRetstores').rows.length;
	//alert(rowCount);
	for(var i=1;i<rowCount;i++)
	{
			var orderNo = 	document.getElementById('tblRetstores').rows[i].cells[1].innerHTML;
			var salesOrderNo = 	document.getElementById('tblRetstores').rows[i].cells[2].id;
			var itemId = 	document.getElementById('tblRetstores').rows[i].cells[5].id;
			
			
		
			var rowCount1 = document.getElementById('tblItemsPopup').rows.length;
			for(var j=1;j<rowCount1;j++)
			{
				var orderNoP = 	document.getElementById('tblItemsPopup').rows[j].cells[5].innerHTML;
				var salesOrderNoP = 	document.getElementById('tblItemsPopup').rows[j].cells[6].id;
				var itemIdP = 	document.getElementById('tblItemsPopup').rows[j].cells[4].id;
				
			//alert(orderNo+"=="+orderNoP+"***"+salesOrderNo+"=="+salesOrderNoP+"***"+itemId+"=="+itemIdP+"***");
				if((orderNo==orderNoP) && (salesOrderNo==salesOrderNoP) && (itemId==itemIdP)){
					document.getElementById('tblItemsPopup').rows[j].cells[0].childNodes[0].checked=true;
					document.getElementById('tblItemsPopup').rows[j].cells[0].childNodes[0].disabled=true;
				}
			}
	}
}*/
//------------------------------------------------------------
function checkAlreadySelected(){
	$('#tblRetstores .item').each(function(){

		var orderNo	= $(this).parent().find(".orderNo").html();
		var salesOrderNo	= $(this).parent().find(".salesOrderNo").attr('id');
		var itemId	= $(this).attr('id');

			var j=0;
			$('#tblItemsPopup .itemP').each(function(){
				j++;
				var orderNoP=$(this).parent().find(".orderNoP").attr('id');
				var salesOrderNoP=$(this).parent().find(".salesOrderNoP").attr('id');
				var itemIdP=$(this).attr('id');
				
			//alert(orderNo+"=="+orderNoP+"***"+salesOrderNo+"=="+salesOrderNoP+"***"+itemId+"=="+itemIdP+"***")
				if((orderNo==orderNoP) && (salesOrderNo==salesOrderNoP) && (itemId==itemIdP)){
					document.getElementById('tblItemsPopup').rows[j].cells[0].childNodes[0].checked=true;
					document.getElementById('tblItemsPopup').rows[j].cells[0].childNodes[0].disabled=true;
				}
			});
	});
}
//------------------------------------------------------------
function loadIssueNos(){
	var issueYear = $('#cboIssueYear').val();
	var type	  = $('input[name=type]:checked', '#frmReturnToStores').val()
	showWaiting();
	var url 		= basePath+"returnToStores-db-get.php?requestType=loadIssueNos&issueYear="+issueYear+"&type="+type;
	var httpobj 	= $.ajax({url:url,async:false})
	hideWaiting();
	document.getElementById('cboIssueNo').innerHTML=httpobj.responseText;
}
//------------------------------------------------------------

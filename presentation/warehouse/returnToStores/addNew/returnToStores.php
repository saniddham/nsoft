<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
//////////////////////////////////////
//Created By: H.B.G Korala
/////////////////////////////////////
$mainPath = $_SESSION['mainPath'];
$location 	= $_SESSION['CompanyID'];
$company 	= $_SESSION['headCompanyId'];
$intUser  = $_SESSION["userId"];
$thisFilePath =  $_SERVER['PHP_SELF'];
 
$returnNo = $_REQUEST['returnNo'];
$year = $_REQUEST['year'];
$programName='Return to Stores';
$programCode='P0246';

if(($returnNo=='')&&($year=='')){
	$savedStat = '';
	$intStatus=$savedStat+1;
	$date='';
	$issueNo='';
	$issueYear='';
}
else{
	$result=loadHeader($returnNo,$year);
	while($row=mysqli_fetch_array($result))
	{
		$date = $row['datdate'];
		$savedStat=$row['intApproveLevels'];
		$intStatus=$row['intStatus'];
		$issueNo=$row['intIssueNo'];
		$issueYear=$row['intIssueYear'];
		$mrnType=$row['mrnType'];
		if($mrnType==7 || $mrnType==8)
		$type	= 2;
		else 
		$type =1;
	}
}

$editMode=loadEditMode($programCode,$intStatus,$savedStat,$intUser);
$confirmatonMode=loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser);
if($returnNo==''){
	$confirmatonMode=0;	
}
$mainPage=$backwardseperator."main.php";

?>
<head>
<title>Return To Stores</title>
<?php
/*include "include/javascript.html";
*/?>
<!--<script type="text/javascript" src="presentation/warehouse/returnToStores/addNew/returnToStores-js.js"></script>
--></head>

<body>
 <form id="frmReturnToStores" name="frmReturnToStores" method="post" action="">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">Return To Stores</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="10%" height="22" class="normalfnt">Return No</td>
            <td width="22%"><input name="txtReturnNo" type="text" disabled="disabled" class="txtText" id="txtReturnNo" style="width:60px" value="<?php echo $returnNo ; ?>" /><input name="txtReturnYear" type="text" disabled="disabled" class="txtText" id="txtReturnYear" style="width:40px"value="<?php echo $year ; ?>" /></td>
            <td width="17%">&nbsp;</td>
            <td width="25%">&nbsp;</td>
            <td width="11%" class="normalfnt">Date</td>
            <td width="15%"><input name="dtDate" type="text" value="<?php if($issueNo){ echo substr($date,0,10); }else { echo date("Y-m-d"); }?>" class="txtbox" id="dtDate" style="width:120px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php if($editMode==0){  ?> disabled="disabled"<?php } ?>/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="10%" height="27" class="normalfnt">Issue Note No</td>
            <td width="39%">
              <select name="cboIssueYear" id="cboIssueYear" style="width:55px"   class="validate[required]" <?php if($editMode==0){  ?> disabled="disabled"<?php } ?>>
                  <?php
					$sql = "SELECT distinct 
							ware_issueheader.intIssueYear
							FROM ware_issueheader
							WHERE
							ware_issueheader.intStatus =  '1' AND
							ware_issueheader.intCompanyId =  '$location' 
							order by ware_issueheader.intIssueYear desc";
					$result = $db->RunQuery($sql);
					$i=0;
					while($row=mysqli_fetch_array($result))
					{
						if($i==0)
							$issueYear=$row['intIssueYear'];
						
						if($row['intIssueYear']==$issueYear)
						echo "<option value=\"".$row['intIssueYear']."\" selected=\"selected\">".$row['intIssueYear']."</option>";	
						else
						echo "<option value=\"".$row['intIssueYear']."\">".$row['intIssueYear']."</option>";	
						$i++;
					}
				?>
              <option value=""></option>
              </select>
			  
<select name="cboIssueNo" id="cboIssueNo" style="width:103px"   class="validate[required]" <?php if($editMode==0){  ?> disabled="disabled"<?php } ?>>
					<?php if($editMode == 0){
						 echo '<option value="">'.$issueNo.'</option>';
					} ?>
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							ware_issueheader.intIssueNo,
							ware_issueheader.intIssueYear,
							(SELECT
							ware_mrnheader.mrnType
							FROM
							ware_issuedetails
							INNER JOIN ware_mrnheader ON ware_issuedetails.intMrnNo = ware_mrnheader.intMrnNo AND ware_issuedetails.intMrnYear = ware_mrnheader.intMrnYear
							WHERE
							ware_issuedetails.intIssueNo = ware_issueheader.intIssueNo AND
							ware_issuedetails.intIssueYear = ware_issueheader.intIssueYear 
							limit 1) as mrnType 
							FROM ware_issueheader
							WHERE
							ware_issueheader.intStatus =  '1' AND
							ware_issueheader.intCompanyId =  '$location' 
							";
							if($issueYear)
							$sql	.= " AND ware_issueheader.intIssueYear=$issueYear";
							
							if($type==1 || $type=='')
							$sql	.= " HAVING (mrnType<>7 AND mrnType<>8) ";
							else
							$sql	.= " HAVING (mrnType=7 OR mrnType=8) ";
							 $sql.=" 
							order by intIssueNo desc, intIssueYear desc
							";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intIssueNo']==$issueNo)
						echo "<option value=\"".$row['intIssueNo']."\" selected=\"selected\">".$row['intIssueNo']."</option>";	
						else
						echo "<option value=\"".$row['intIssueNo']."\">".$row['intIssueNo']."</option>";	
					}
				?>
            </select>              </td>
            <td width="25%">&nbsp;</td>
            <td width="11%" class="normalfnt">Other
              <input type="radio" name="type" value="1" <?php if($type=='' || $type ==1){ ?>checked <?php } ?> class="type"></td>
            <td width="15%"><span class="normalfnt">Loan In/Out
                <input type="radio" name="type" value="2"<?php if($type ==2){ ?>checked <?php } ?> class="type">
            </span></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="92%">&nbsp;</td>
            <td width="8%"><?php if($editMode!=0){  ?> <img src="images/Tadd.jpg" width="92" height="24" id="butAddItems"  /><?php } ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><div style="width:900px;height:300px;overflow:scroll" >
          <table width="100%" class="bordered" id="tblRetstores" >
            <tr>
              <th width="3%" height="22" >Del</th>
              <th width="7%" height="22" >MRN No</th>
              <th width="7%" height="22" >Order No</th>
              <th width="11%" >Sales Order No</th>
              <th width="10%" >Main Category</th>
              <th width="10%" >Sub Category</th>
              <th width="23%" >Item Description</th>
              <th width="7%">UOM</th>
              <th width="7%">Issue Qty</th>
              <th width="8%">Return Qty</th>
              <th width="12%">Bal to Return Qty</th>
              <th width="9%"> Qty</th>
              </tr>
  <?php
  
	  $sql = "SELECT
	mst_item.strName as item,
	mst_item.intMainCategory,
	mst_maincategory.strName as maincat,
	mst_item.intSubCategory,
	mst_subcategory.strName as subcat,
	mst_item.strCode,
	mst_item.intBomItem,
        mst_item.ITEM_HIDE,
	ware_returntostoresdetails.intMrnNo,
	ware_returntostoresdetails.intMrnYear,
	ware_returntostoresdetails.strOrderNo,
	ware_returntostoresdetails.intOrderYear,
	trn_orderdetails.strSalesOrderNo,
	trn_orderdetails.intSalesOrderId,
	ware_returntostoresdetails.strStyleNo,
	ware_returntostoresdetails.intItemId, 
	ware_returntostoresdetails.dblQty,
	ware_returntostoresheader.intStatus,
	ware_returntostoresheader.intApproveLevels,
	tb1.dblQty as dblIssueQty,
	IFNULL((SELECT
	Sum(ware_returntostoresdetails.dblQty)
	FROM
	ware_returntostoresheader
	Inner Join ware_returntostoresdetails ON ware_returntostoresheader.intReturnNo = ware_returntostoresdetails.intReturnNo AND ware_returntostoresheader.intReturnYear = ware_returntostoresdetails.intReturnYear
	WHERE
	ware_returntostoresheader.intIssueNo =  tb1.intIssueNo AND
	ware_returntostoresheader.intIssueYear =  tb1.intIssueYear AND 
	ware_returntostoresdetails.strOrderNo =  tb1.strOrderNo AND
	ware_returntostoresdetails.intOrderYear =  tb1.intOrderYear AND
	ware_returntostoresdetails.strStyleNo =  tb1.strStyleNo  AND
	ware_returntostoresdetails.intItemId =  tb1.intItemId AND
	ware_returntostoresheader.intStatus <=ware_returntostoresheader.intApproveLevels AND
	ware_returntostoresheader.intStatus > 0 ),0) as dblReturnQty, 
	mst_units.strCode as uom , 
	mst_part.strName as part
	FROM
	ware_returntostoresdetails
	Inner Join ware_returntostoresheader ON ware_returntostoresdetails.intReturnNo = ware_returntostoresheader.intReturnNo AND ware_returntostoresdetails.intReturnYear = ware_returntostoresheader.intReturnYear
	Inner Join ware_issuedetails as tb1 ON ware_returntostoresheader.intIssueNo = tb1.intIssueNo AND ware_returntostoresheader.intIssueYear = tb1.intIssueYear AND ware_returntostoresdetails.strOrderNo = tb1.strOrderNo AND ware_returntostoresdetails.intOrderYear = tb1.intOrderYear AND ware_returntostoresdetails.strStyleNo = tb1.strStyleNo AND ware_returntostoresdetails.intItemId = tb1.intItemId
	left Join trn_orderdetails ON tb1.strOrderNo = trn_orderdetails.intOrderNo AND tb1.intOrderYear = trn_orderdetails.intOrderYear AND tb1.strStyleNo = trn_orderdetails.intSalesOrderId
	Inner Join mst_item ON tb1.intItemId = mst_item.intId
	Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
	Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
	Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
	left Join mst_part ON trn_orderdetails.intPart = mst_part.intId 
	WHERE
	ware_returntostoresdetails.intReturnNo =  '$returnNo' AND
	ware_returntostoresdetails.intReturnYear =  '$year' 
	ORDER BY mst_maincategory.strName asc, mst_subcategory.strName asc, mst_item.strName asc 
	";
	$result = $db->RunQuery($sql);
	$totAmm=0;
	while($row=mysqli_fetch_array($result))
	{
			$intMrnNo=$row['intMrnNo'];
			$intMrnYear=$row['intMrnYear'];
			$mrnNo = $row['intMrnNo']."/".$row['intMrnYear'];
			$intOrderNo=$row['strOrderNo'];
			$orderYear=$row['intOrderYear'];
			$orderNo=$row['strOrderNo']."/".$row['intOrderYear'];
			$salesOrderNo=$row['strSalesOrderNo']."/".$row['part'];
			$salesOrderId=$row['intSalesOrderId'];
			$strStyleNo=$row['strStyleNo'];
			$mainCatName=$row['maincat'];
			$subCatName=$row['subcat'];
			$mainCatId=$row['intMainCategory'];
			$subCatId=$row['intSubCategory'];
			$itemId=$row['intItemId'];
			$itemName=$row['item'];
			$uom=$row['uom'];
			$Qty=$row['dblQty'];
                        $ITEM_HIDE=$row['ITEM_HIDE'];
			$issueQty=$row['dblIssueQty'];
			$returnQty=$row['dblReturnQty'];
			$balToReturnQty=$issueQty-$returnQty;
			$maxRetQty=$balToReturnQty;
			
			if($row['strOrderNo']==0){
				$orderNo='';	
				$salesOrderNo='';
				$salesOrderId='';
				$strStyleNo='';
			}
		
	?>
<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF"><?php if($editMode==1){  ?><img class="delImg" src="images/del.png" width="15" height="15" /><?php } ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $mrnNo; ?>" class="mrnNo"><?php echo $mrnNo; ?></td>
            <td align="center" bgcolor="#FFFFFF" id="<?php echo $orderNo; ?>" class="orderNo"><?php echo $orderNo; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $salesOrderId; ?>" class="salesOrderNo"><?php echo $salesOrderNo; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $mainCatId; ?>" class="mainCatName"><?php echo $mainCatName; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $subCatId; ?>" class="subCatName"><?php echo $subCatName; ?></td>
                        <?php
                        if($ITEM_HIDE==1)
                        {
                            ?>
                        <td align="center" bgcolor="#FFFFFF" id="<?php echo $itemId; ?>" class="item">***** </td>
                        <?php
                        }else{
                            ?>
                        
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $itemId; ?>" class="item"><?php echo $itemName; ?></td>
                        <?php
                        }?>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $uom; ?>" class="uom"><?php echo $uom; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $issueQty; ?>" class="issueQty"><?php echo $issueQty; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $returnQty; ?>" class="returnQty"><?php echo $returnQty; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $balToReturnQty; ?>" class="balToReturnQty"><?php echo $balToReturnQty; ?></td>
			<td align="center" bgcolor="#FFFFFF" id=""><input  id="<?php echo $maxRetQty; ?>" class="validate[required,custom[number],max[<?php echo $maxRetQty; ?>]] calculateValue Qty" style="width:80px;text-align:center" type="text" value="<?php echo $Qty; ?>" <?php if($editMode==0){  ?> disabled="disabled"<?php } ?>/></td>
			</tr>	<?php
    }
    ?>

          </table>
        </div></td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound">
        <a name="butNew"  class="button white medium" id="butNew">New</a>
         <?php if(($form_permision['add']||$form_permision['edit']) && $editMode==1){ ?>
         <a id="butSave" name="butSave"  class="button white medium">Save</a>
		 <?php } ?>
         <a id="butConfirm" name="butConfirm" style=" <?php if(($intStatus>1) and ($confirmatonMode==1) && $confirmatonMode==1){  echo 'display:yes'; } else { ?>  <?php echo 'display:none';  } ?>" class="button white medium">Approve</a>
         <a id="butReport" name="butReport"  class="button white medium">Report</a>
         <a href="main.php"  class="button white medium">Close</a></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
	<!-- items to prn -->
	<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1">
   <!-- <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php" style="width:800;height:800;border:0;overflow:hidden">
    </iframe>-->
    </div>

	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>


</body>
</html>
<?php
	//--------------------------------------------------------------
	function getStockBalance($company,$location,$orderNo,$orderYear,$style,$item)
	{
		global $db;
		  $sql = "SELECT
				Sum(ware_stocktransactions.dblQty) as stockBal 
				FROM ware_stocktransactions
				WHERE
				ware_stocktransactions.intCompanyId =  '$company' AND
				ware_stocktransactions.intLocationId =  '$location' AND
				ware_stocktransactions.intOrderNo =  '$orderNo' AND
				ware_stocktransactions.intOrderYear =  '$orderYear' AND
				ware_stocktransactions.strStyleNo =  '$style' AND
				ware_stocktransactions.intItemId =  '$item'";

		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['stockBal']);	
	}
	//--------------------------------------------------------------
	function getStockBalance_bulk($location,$item)
	{
		global $db;
		   $sql = "SELECT
				Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$location'
				GROUP BY
				ware_stocktransactions_bulk.intItemId";

		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
//------------------------------function load details---------------------
function loadDetails($serialNo,$year)
{
	global $db;
			    $sql = "SELECT
mst_item.strName as item,
mst_item.intMainCategory,
mst_maincategory.strName as maincat,
mst_item.intSubCategory,
mst_subcategory.strName as subcat,
mst_item.strCode,
mst_item.intBomItem,
ware_issuedetails.strOrderNo,
ware_issuedetails.intOrderYear,
ware_issuedetails.strStyleNo,
trn_orderdetails.strSalesOrderNo,
trn_orderdetails.intSalesOrderId,
ware_issuedetails.intMrnNo,
ware_issuedetails.intMrnYear,
ware_issuedetails.intItemId, 
ware_issuedetails.dblQty,
ware_issueheader.intStatus,
ware_issueheader.intApproveLevels,
ware_mrndetails.dblQty as dbMrnQty,
ware_mrndetails.dblIssudQty
FROM
ware_issuedetails 
left Join trn_orderdetails ON ware_issuedetails.strOrderNo = trn_orderdetails.intOrderNo AND ware_issuedetails.intOrderYear = trn_orderdetails.intOrderYear AND ware_issuedetails.strStyleNo = trn_orderdetails.intSalesOrderId
Inner Join ware_issueheader ON ware_issuedetails.intIssueNo = ware_issueheader.intIssueNo AND ware_issuedetails.intIssueYear = ware_issueheader.intIssueYear
Inner Join ware_mrndetails ON ware_issuedetails.intMrnNo = ware_mrndetails.intMrnNo AND ware_issuedetails.intMrnYear = ware_mrndetails.intMrnYear AND ware_issuedetails.strOrderNo = ware_mrndetails.intOrderNo AND ware_issuedetails.intOrderYear = ware_mrndetails.intOrderYear AND ware_issuedetails.strStyleNo = ware_mrndetails.strStyleNo AND ware_issuedetails.intItemId = ware_mrndetails.intItemId
Inner Join mst_item ON ware_mrndetails.intItemId = mst_item.intId
Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
WHERE
ware_issuedetails.intIssueNo =  '$serialNo' AND
ware_issuedetails.intIssueYear =  '$year' 
ORDER BY mst_maincategory.strName asc, mst_subcategory.strName asc, mst_item.strName asc 
";			 $result = $db->RunQuery($sql);
			 return $result;
}
//------------------------------function load Header---------------------
function loadHeader($serialNo,$year)
{
	global $db;
		 $sql = "SELECT
					ware_returntostoresheader.datdate,
					ware_returntostoresheader.intStatus,
					ware_returntostoresheader.intApproveLevels,
					ware_returntostoresheader.intUser,
					ware_returntostoresheader.intIssueNo,  
					ware_returntostoresheader.intIssueYear,
					(SELECT
					ware_mrnheader.mrnType
					FROM
					ware_issuedetails
					INNER JOIN ware_mrnheader ON ware_issuedetails.intMrnNo = ware_mrnheader.intMrnNo AND ware_issuedetails.intMrnYear = ware_mrnheader.intMrnYear
					WHERE
					ware_issuedetails.intIssueNo = ware_returntostoresheader.intIssueNo AND
					ware_issuedetails.intIssueYear = ware_returntostoresheader.intIssueYear  
					limit 1) AS mrnType 
					FROM
					ware_returntostoresheader 
					WHERE
					ware_returntostoresheader.intReturnNo =  '$serialNo' AND
					ware_returntostoresheader.intReturnYear =  '$year' ";
			 $result = $db->RunQuery($sql);
			 return $result;
}
//------------------------------function load Default Department---------------------
function loadDefaultDepartment($intUser){
	global $db;
	 $sql1 = "SELECT
				sys_users.intDepartmentId
				FROM
				sys_users
				WHERE
				sys_users.intUserId =  '$intUser'";	
	$result1 = $db->RunQuery($sql1);
	$row1=mysqli_fetch_array($result1);
	$department=$row1['intDepartmentId'];
	return $department;
}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
			 
	return $editMode;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	$sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
	
?>

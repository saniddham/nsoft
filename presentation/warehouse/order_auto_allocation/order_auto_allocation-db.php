<?php 
//ini_set('display_errors',1);
ini_set('max_execution_time', 11111111) ;

session_start();
$backwardseperator 	= "../../../";
$mainPath 			= $_SESSION['mainPath'];
$userId 			= $_SESSION['userId'];
$location 			= $_SESSION['CompanyID'];
$company 			= $_SESSION['headCompanyId'];

$requestType 		= $_REQUEST['requestType'];
$programCode		= 'P0794';

$savedStatus		= true;
$savedMasseged 		= '';
$error_sql			= '';

include_once 	"{$backwardseperator}dataAccess/Connector.php";
include_once 	"../../../class/warehouse/order_auto_allocation/cls_order_auto_allocation_get.php";
include_once 	"../../../class/warehouse/order_auto_allocation/cls_order_auto_allocation_set.php";
include_once 	"../../../class/warehouse/cls_warehouse_get.php";
include_once 	"../../../class/cls_commonFunctions_get.php";


$obj_auto_allo_get	= new Cls_order_auto_allocation_get($db);
$obj_auto_allo_set	= new Cls_order_auto_allocation_set($db);
$obj_warehouse_get	= new cls_warehouse_get($db);
$obj_common			= new cls_commonFunctions_get($db);

$db->begin();

$AP_arr				= $obj_auto_allo_get->getAllocationPercentages($company,'RunQuery2');

$sampleAP			= $AP_arr['SAMPLE_ALLOCATION_%'];
$fullAP				= $AP_arr['FULL_ALLOCATION_%'];
$extraAP1			= $AP_arr['INK_ITEM_EXTRA_ALLOCATION_%'];// ink Extra allocation percentage
$extraAP2			= $AP_arr['SP_RM_EXTRA_ALLOCATION_%'];// special RM extra allocation percentage
$extraAP3			= $AP_arr['FOIL_EXTRA_ALLOCATION_%'];// foil extra allocation percentage

if($requestType=='loadAllComboes')
{
	$slected 			= $_REQUEST['slected'];
	$orderYear 			= $_REQUEST['orderYear'];
	$orderNo 			= $_REQUEST['orderNo'];

	$html				= $obj_auto_allo_get->loadAllComboes($slected,$orderYear,$orderNo);
	
	$response['combo']	= $html;
	echo json_encode($response);
}
else if($requestType=='getOrderQty')
{
	$orderNo 			= $_REQUEST['orderNo'];
	$orderYear 			= $_REQUEST['orderYear'];
	$salesOrder 		= $_REQUEST['salesOrder'];
	
	//$response['orderQty']	= $obj_auto_allo_get->getOrderQty($orderNo,$orderYear,$salesOrder);
	$response['orderQty']	= $obj_auto_allo_get->getOrderQty_with_dammage($orderNo,$orderYear,$salesOrder);
	$response['sampleAP']	= $sampleAP;
	$response['fullAP']		= $fullAP;
	echo json_encode($response);
}
else if($requestType=='loadDetails')
{

	$orderNo 			= $_REQUEST['orderNo'];
	$orderYear 			= $_REQUEST['orderYear'];
	$salesOrder 		= $_REQUEST['salesOrder'];
	
	$result				= $obj_warehouse_get->get_order_item_styleAllocation_auto($orderNo,$orderYear,$salesOrder,'','RunQuery2');
	while($row = mysqli_fetch_array($result))
	{
		$data['type'] 	 		= $row['type'];
		$data['type_id'] 	 	= $row['allocationType'];
		$data['itemName'] 	 	= $row['itemName'];
		$data['itemId'] 	 	= $row['intId'];
		$data['unit'] 			= $row['uom'];
		$data['orderQty']		= $row['orderQty'];
		$data['sampConPC']		= $obj_warehouse_get->get_order_item_sample_consumption($orderNo,$orderYear,$salesOrder,$row['intId'],'RunQuery2');
		$data['balToAllc25']    ='';
	
	
		switch($row['allocationType'])
		{
			case 1 :
				$extraAP = $extraAP1;
			break;
			
			case 2 :
				$extraAP = $extraAP2;
			break;
			
			case 3 :
				$extraAP = $extraAP3;
			break;
		}
		
		if($row['allocationType']==1)//ink item allocation
		{
			$day_cons	= $obj_warehouse_get->get_production_cumulateve_ink_item_consumption($orderNo,$orderYear,$salesOrder,$row['intId'],'RunQuery2');
		}
		else// foil/special RM allocation
		{	
			$day_cons	= $obj_warehouse_get->get_production_cumulateve_special_foil_item_consumption($orderNo,$orderYear,$salesOrder,$row['intId'],'RunQuery2');
		}
				
		//get already allocated Qty
		$allocatedQty	= $obj_warehouse_get->get_already_allocated_qty($orderYear,$orderNo,$salesOrder,$row['intId'],'RunQuery2');
 				
		$data['1stDayConsm'] 	= round($day_cons,6);
		$data['allocatedQty'] 	= $obj_common->ceil_to_two_decimal_places($allocatedQty, 2);
		
		if($day_cons==''){
			//$data['balToAllc25'] = $row['consum'].'*'.$row['orderQty'].'*'.($sampleAP/100).'*'.($allocatedQty*100);
		  $val=0;
		  $val1=0;
		  $bal_to_allocate	= 0;
		  $result_S	= $obj_warehouse_get->get_sales_order_nos_result($orderNo,$orderYear,$salesOrder,'RunQuery2');
			while($row_S 	= mysqli_fetch_array($result_S)){
				$salesOrder_tmp = $row_S['intSalesOrderId'];
				$result_T		= $obj_warehouse_get->get_order_item_styleAllocation_auto($orderNo,$orderYear,$salesOrder_tmp,$row['intId'],'RunQuery2');
				while($row_T 	= mysqli_fetch_array($result_T)){
					$val   		= $row_T['consum']*$row_T['orderQty']*$sampleAP/100;
					$all_tmp	= $obj_warehouse_get->get_already_allocated_qty($orderYear,$orderNo,$salesOrder_tmp,$row['intId'],'RunQuery2');
					//$all_tmp	= $obj_common->ceil_to_two_decimal_places($all_tmp, 2);
					$val1		+=   ($obj_common->ceil_to_two_decimal_places(($val*(1+$extraAP/100)), 2)) -($obj_common->ceil_to_two_decimal_places($all_tmp, 2));
					
					//////////////////////////////////
						$sample_cons	= $obj_warehouse_get->get_order_item_sample_consumption($orderNo,$orderYear,$salesOrder_tmp,$row['intId'],'RunQuery2');
						$production_qty	= $obj_warehouse_get->get_production_order_qty($orderNo,$orderYear,$salesOrder_tmp,'RunQuery2');
						
						if($itemType==1)//ink item allocation
						{
							$day_cons	= $obj_warehouse_get->get_production_cumulateve_ink_item_consumption($orderNo,$orderYear,$salesOrder_tmp,$row['intId'],'RunQuery2');
						}
						else// foil/special RM allocation
						{	
							$day_cons	= $obj_warehouse_get->get_production_cumulateve_special_foil_item_consumption($orderNo,$orderYear,$salesOrder_tmp,$row['intId'],'RunQuery2');
						}
								
						//get already allocated Qty
						$allocatedQty	= $obj_warehouse_get->get_already_allocated_qty($orderYear,$orderNo,$salesOrder_tmp,$row['intId'],'RunQuery2');
								
						$day_cons 		= round($day_cons,6);
						$allocatedQty	= $obj_common->ceil_to_two_decimal_places($allocatedQty, 2);
						
						if($allocType=='alloc100'){
							$val   					=   $day_cons*$production_qty*$fullAP/100;
							$bal_to_allocate		+=   $obj_common->ceil_to_two_decimal_places(($val*(1+$extraAP/100)), 2) -$obj_common->ceil_to_two_decimal_places($allocatedQty, 2);
						}
						else{
							$val   					=   $sample_cons*$production_qty*$sampleAP/100;
							$bal_to_allocate		+=   $obj_common->ceil_to_two_decimal_places(($val*(1+$extraAP/100)), 2) -$obj_common->ceil_to_two_decimal_places($allocatedQty, 2);
							//$bal_to_allocate		.=   (($val*(1+$extraAP/100)), 2) .'-';
						}
					////////////////////////////////
				}
			}
			
			$data['balToAllc25']	=    $bal_to_allocate;
		}
		else
		{
			$data['balToAllc25']	= '';
		}
		
		
		if($day_cons!=''){
		    $val   						=   $day_cons*$row['orderQty']*$fullAP/100;
			$data['balToAllc1stDayCon']	= 	$obj_common->ceil_to_two_decimal_places(($val*(1+$extraAP)), 2) -$obj_common->ceil_to_two_decimal_places($allocatedQty, 2);
			$data['balToAllc_tot']		=   $obj_common->ceil_to_two_decimal_places(($val*(1+$extraAP)), 2) -$obj_common->ceil_to_two_decimal_places($allocatedQty, 2);
		}
		else{
			$data['balToAllc1stDayCon']	= '';
			$data['balToAllc_tot']		=   $obj_common->ceil_to_two_decimal_places((($sample_cons*$row['orderQty']*$fullAP/100)*(1+$extraAP)), 2) -$obj_common->ceil_to_two_decimal_places($allocatedQty, 2);
		}
		
				$data['bulkStockBal'] 		= $obj_auto_allo_get->getBulkStockBalance($row['intId'],'RunQuery2');
				$data['colorRoomStock'] 	= $obj_common->ceil_to_two_decimal_places($obj_auto_allo_get->getColorRoomStockBalance($row['intId'],$orderNo,$orderYear,$salesOrder,'RunQuery2'), 2);
				$data['issued'] 			= $obj_common->ceil_to_two_decimal_places($obj_auto_allo_get->getIssuedQty($row['intId'],$orderNo,$orderYear,$salesOrder,'RunQuery2'), 2);
				$data['returned'] 			= $obj_common->ceil_to_two_decimal_places($obj_auto_allo_get->getReturnedQty($row['intId'],$orderNo,$orderYear,$salesOrder,'RunQuery2'), 2);

		$arrDetailData[] 			= $data;
	}
	$response['arrOrderData']		= $arrDetailData;
	echo json_encode($response);
}
else if($requestType=='allocateData')
{
	$arrHeader 		= json_decode($_REQUEST['arrHeader'],true);	
	$arrDetails 	= json_decode($_REQUEST['arrDetails'],true);
	
	$orderYear		= $arrHeader['orderYear'];
	$orderNo		= $arrHeader['orderNo'];
	$salesOrderId	= $arrHeader['salesOrderId'];
	$allocType		= $arrHeader['allocType'];
	
	$db->begin();
	if($allocType=='alloc25')
		$saveMode		= $obj_common->ValidateSpecialPermission(52,$userId,'RunQuery2');
	else
		$saveMode		= $obj_common->ValidateSpecialPermission(53,$userId,'RunQuery2');
	
	if($saveMode==0 && ($savedStatus))
	{
		$savedStatus 	= false;
		$savedMasseged 	= 'No permission to allocate.';
	}
	foreach($arrDetails as $array_loop)
	{
		$itemType		= $array_loop['itemType'];
		$itemId			= $array_loop['itemId'];
		$balAllocSC		= $array_loop['balAllocSC'];
		$balAllocDC		= $array_loop['balAllocDC'];
		
		if($allocType=='alloc100')
		{
			$allocateQty	= $balAllocDC*$sampleAP/100;
			$allocateQty	=$obj_common->ceil_to_two_decimal_places($allocateQty, 2);
			$type			= '100_ITEM_ALLOC';
		}
		else
		{
			$allocateQty	= $balAllocSC;
			$allocateQty	=$obj_common->ceil_to_two_decimal_places($allocateQty, 2);
			$type			= '25_ITEM_ALLOC';
		}
	
 	//------------------
		switch($itemType)
		{
			case 1 :
				$extraAP = $extraAP1;
			break;
			
			case 2 :
				$extraAP = $extraAP2;
			break;
			
			case 3 :
				$extraAP = $extraAP3;
			break;
		}
		
		
		//////////////////////////////////////////////////////////////////////////////////////////////
		$result_M	= $obj_warehouse_get->get_sales_order_no_wise_result($orderNo,$orderYear,$itemId,$allocateQty,'RunQuery2');
		while($row_M=mysqli_fetch_array($result_M))
		{
			$salesOrderId		= $row_M['intSalesOrderId'];
			$currunt_allocated	= 0;
			$currunt_save_data	= 0;
			$bal_to_allocate	= 0;
		//////////////////////////////////////////////////////////////////////////////////////////////
			$sample_cons	= $obj_warehouse_get->get_order_item_sample_consumption($orderNo,$orderYear,$salesOrderId,$itemId,'RunQuery2');
			$production_qty	= $obj_warehouse_get->get_production_order_qty($orderNo,$orderYear,$salesOrderId,'RunQuery2');
			
			if($itemType==1)//ink item allocation
			{
				$day_cons	= $obj_warehouse_get->get_production_cumulateve_ink_item_consumption($orderNo,$orderYear,$salesOrderId,$itemId,'RunQuery2');
			}
			else// foil/special RM allocation
			{	
				$day_cons	= $obj_warehouse_get->get_production_cumulateve_special_foil_item_consumption($orderNo,$orderYear,$salesOrderId,$itemId,'RunQuery2');
			}
					
			//get already allocated Qty
			$allocatedQty	= $obj_warehouse_get->get_already_allocated_qty($orderYear,$orderNo,$salesOrderId,$itemId,'RunQuery2');
					
			$day_cons 		= round($day_cons,6);
			$allocatedQty	= $obj_common->ceil_to_two_decimal_places($allocatedQty, 2);
			
			if($allocType=='alloc100'){
				$val   					=   $day_cons*$production_qty*$fullAP/100;
				$bal_to_allocate		=   $obj_common->ceil_to_two_decimal_places(($val*(1+$extraAP/100)), 2) -$obj_common->ceil_to_two_decimal_places($allocatedQty, 2);
			}
			else{
				$val   					=   $sample_cons*$production_qty*$sampleAP/100;
				$bal_to_allocate		=   $obj_common->ceil_to_two_decimal_places(($val*(1+$extraAP/100)), 2) -$obj_common->ceil_to_two_decimal_places($allocatedQty, 2);
			}
			$stockBal			= $obj_warehouse_get->get_bulk_stock_balance($location,$itemId,'RunQuery2');		
			
			//echo $salesOrderId.'--'.$itemId."--".$production_qty."--".$allocatedQty."--".$sample_cons."|";
			
			if($bal_to_allocate > $stockBal){
				$currunt_save_data	= $stockBal;
			}
			else{
				$currunt_save_data	= $bal_to_allocate;
			}
			
			
			if($currunt_save_data > 0){
			$alloc_arr			= allocatedata($orderYear,$orderNo,$salesOrderId,$itemId,$currunt_save_data,$type);
			}
			else{
			$alloc_arr			= unAllocatedata($orderYear,$orderNo,$salesOrderId,$itemId,$currunt_save_data);
			}
			if($alloc_arr['savedStatus']=='fail' && $savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged 	= $alloc_arr['savedMassege'];
				$error_sql		= $alloc_arr['error_sql'];
			}
		}
		
	/////////////////////////////////////////////////////////////////
	}
	///////////////////////////////////////////////////////////////
	if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= "Allocated successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql'] 		= $error_sql;
	}
	echo json_encode($response);
}
else if($requestType=='allocateDataManually')
{
	$arrHeader 		= json_decode($_REQUEST['arrHeader'],true);	
	
	$orderYear		= $arrHeader['orderYear'];
	$orderNo		= $arrHeader['orderNo'];
	$salesOrderId	= $arrHeader['salesOrderId'];
	$itemId			= $arrHeader['itemId'];
	$itemType		= $arrHeader['itemType'];
	$balAllocSC		= $arrHeader['balAllocSC'];
	$balAllocDC		= $arrHeader['balAllocDC'];
	$allocateQty	= $arrHeader['allocateQty'];
	$allocateQty	=$obj_common->ceil_to_two_decimal_places($allocateQty, 2);
	$ini_alloc_qty	= $allocateQty;
	
	if($balAllocSC!='' && $balAllocSC!=0 )
		$type		= '25_ITEM_ALLOC';
	else if($balAllocDC!='' && $balAllocDC!=0)
		$type		= '100_ITEM_ALLOC';
	
	$db->begin();
	
	$saveMode		= $obj_common->ValidateSpecialPermission(54,$userId,'RunQuery2');
	if($saveMode==0 && ($savedStatus))
	{
		$savedStatus 	= false;
		$savedMasseged 	= 'No permission to allocate.';
	}
	
		//////////////////////////////////////////////////////////////////////////////////////////////
		$tot_saved	=	0;
		$result_M	= $obj_warehouse_get->get_sales_order_no_wise_result($orderNo,$orderYear,$itemId,$allocateQty,'RunQuery2');
		while($row_M=mysqli_fetch_array($result_M))
		{
			$salesOrderId		= $row_M['intSalesOrderId'];
			$currunt_allocated	= 0;
			$currunt_save_data	= 0;
			$bal_to_allocate	= 0;
		//////////////////////////////////////////////////////////////////////////////////////////////
			$sample_cons	= $obj_warehouse_get->get_order_item_sample_consumption($orderNo,$orderYear,$salesOrderId,$itemId,'RunQuery2');
			$production_qty	= $obj_warehouse_get->get_production_order_qty($orderNo,$orderYear,$salesOrderId,'RunQuery2');
			
			if($itemType==1)//ink item allocation
			{
				$day_cons	= $obj_warehouse_get->get_production_cumulateve_ink_item_consumption($orderNo,$orderYear,$salesOrderId,$itemId,'RunQuery2');
			}
			else// foil/special RM allocation
			{	
				$day_cons	= $obj_warehouse_get->get_production_cumulateve_special_foil_item_consumption($orderNo,$orderYear,$salesOrderId,$itemId,'RunQuery2');
			}
					
			//get already allocated Qty
			$allocatedQty	= $obj_warehouse_get->get_already_allocated_qty($orderYear,$orderNo,$salesOrderId,$itemId,'RunQuery2');
					
			$day_cons 		= round($day_cons,6);
			$allocatedQty	= $obj_common->ceil_to_two_decimal_places($allocatedQty, 2);
			
			
			if($type=='100_ITEM_ALLOC'){
				$val   					=   $day_cons*$production_qty*$fullAP/100;
				$bal_to_allocate		=   $obj_common->ceil_to_two_decimal_places(($val*(1+$extraAP/100)), 2) -$obj_common->ceil_to_two_decimal_places($allocatedQty, 2);
			}
			else{
				$val   					=   $sample_cons*$production_qty*$sampleAP/100;
				$bal_to_allocate		=   $obj_common->ceil_to_two_decimal_places(($val*(1+$extraAP/100)), 2) -$obj_common->ceil_to_two_decimal_places($allocatedQty, 2);
			}
			$stockBal			= $obj_warehouse_get->get_bulk_stock_balance($location,$itemId,'RunQuery2');		
			
			$currunt_save_data	= $obj_common->ceil_to_two_decimal_places($allocateQty, 2);
			
			if($currunt_save_data > $bal_to_allocate){
				$currunt_save_data	= $bal_to_allocate;
			}
			if($currunt_save_data > $stockBal){
				$currunt_save_data	= $stockBal;
			}
			
			$allocateQty	= $allocateQty-$currunt_save_data;
			if($currunt_save_data > 0){
			$tot_saved			+= 	$currunt_save_data;
			$alloc_arr			= allocatedata($orderYear,$orderNo,$salesOrderId,$itemId,$currunt_save_data,$type);
			}

			if($alloc_arr['savedStatus']=='fail' && $savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged 	= $alloc_arr['savedMassege'];
				$error_sql		= $alloc_arr['error_sql'];
			}
 		
	/////////////////////////////////////////////////////////////////
	}
	///////////////////////////////////////////////////////////////
	if(round($ini_alloc_qty,2) != round($tot_saved,2)){
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= 'Maximum Qty is '.$tot_saved;
		$response['sql'] 		= $error_sql;
	}
 	else if($savedStatus)
	{
		$db->commit();
		$response['type'] 		= "pass";
		$response['msg'] 		= "Allocated successfully.";
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql'] 		= $error_sql;
	}
	echo json_encode($response);
}
else if($requestType=='unAllocateDataManually')
{
	$arrHeader 		= json_decode($_REQUEST['arrHeader'],true);	
	
	$orderYear		= $arrHeader['orderYear'];
	$orderNo		= $arrHeader['orderNo'];
	$salesOrderId	= $arrHeader['salesOrderId'];
	$itemId			= $arrHeader['itemId'];
	$itemType		= $arrHeader['itemType'];
	$unallocateQty	= $arrHeader['unallocateQty'];
	$unallocateQty	=$obj_common->ceil_to_two_decimal_places($unallocateQty, 2);
	$ini_unalloc	= $unallocateQty;
	$db->begin();
	
	$saveMode		= $obj_common->ValidateSpecialPermission(55,$userId,'RunQuery2');
	$stockBal		= $obj_warehouse_get->get_order_item_stock_bal($location,$orderNo,$orderYear,$salesOrderId,$itemId,'RunQuery2');		
	
	if($saveMode==0 && ($savedStatus))
	{
		$savedStatus 	= false;
		$savedMasseged 	= 'No permission to unallocate.';
	}
	else if($unallocateQty > $stockBal){
		$savedStatus	= false;
		$savedMasseged 	= 'This Qty greater than stock balance : '. $stockBal;
		$error_sql		= '';
	}
		//////////////////////////////////////////////////////////////////////////////////////////////
		$tot_saved	= 0;
		$result_M	= $obj_warehouse_get->get_sales_order_no_wise_result($orderNo,$orderYear,$itemId,$unallocateQty,'RunQuery2');
		while($row_M=mysqli_fetch_array($result_M))
		{
			$salesOrderId		= $row_M['intSalesOrderId'];
			$currunt_allocated	= 0;
			$currunt_save_data	= 0;
			$bal_to_allocate	= 0;
		//////////////////////////////////////////////////////////////////////////////////////////////
			$sample_cons	= $obj_warehouse_get->get_order_item_sample_consumption($orderNo,$orderYear,$salesOrderId,$itemId,'RunQuery2');
			$production_qty	= $obj_warehouse_get->get_production_order_qty($orderNo,$orderYear,$salesOrderId,'RunQuery2');
			
			if($itemType==1)//ink item allocation
			{
				$day_cons	= $obj_warehouse_get->get_production_cumulateve_ink_item_consumption($orderNo,$orderYear,$salesOrderId,$itemId,'RunQuery2');
			}
			else// foil/special RM allocation
			{	
				$day_cons	= $obj_warehouse_get->get_production_cumulateve_special_foil_item_consumption($orderNo,$orderYear,$salesOrderId,$itemId,'RunQuery2');
			}
					
			//get already allocated Qty
			$allocatedQty	= $obj_warehouse_get->get_already_allocated_qty($orderYear,$orderNo,$salesOrderId,$itemId,'RunQuery2');
					
			$day_cons 		= round($day_cons,6);
			$allocatedQty	= $obj_common->ceil_to_two_decimal_places($allocatedQty, 2);
			
			
			if($day_cons >0){
				$val   					=   $day_cons*$production_qty*$fullAP/100;
				$bal_to_allocate		=   $obj_common->ceil_to_two_decimal_places(($val*(1+$extraAP/100)), 2) -$obj_common->ceil_to_two_decimal_places($allocatedQty, 2);
			
			}else{
				$val   					=   $sample_cons*$production_qty*$sampleAP/100;
				$bal_to_allocate		=   $obj_common->ceil_to_two_decimal_places(($val*(1+$extraAP/100)), 2) -$obj_common->ceil_to_two_decimal_places($allocatedQty, 2);
			}
			$stockBal			= $obj_warehouse_get->get_order_item_stock_bal($location,$orderNo,$orderYear,$salesOrderId,$itemId,'RunQuery2');		
			
			$unallocateQty	=	$obj_common->ceil_to_two_decimal_places($unallocateQty, 2);
 
			$currunt_save_data	= $unallocateQty;
			if($stockBal < $unallocateQty){
				$currunt_save_data = $stockBal;
			}
			$unallocateQty	= $unallocateQty - $currunt_save_data;
	
			
			if($currunt_save_data > 0){
			$tot_saved		+= $currunt_save_data;
			$alloc_arr			= unAllocatedata($orderYear,$orderNo,$salesOrderId,$itemId,$currunt_save_data*(-1),$type);
			}

			if($alloc_arr['savedStatus']=='fail' && $savedStatus)
			{
				$savedStatus	= false;
				$savedMasseged 	= $alloc_arr['savedMassege'];
				$error_sql		= $alloc_arr['error_sql'];
			}
 		
	/////////////////////////////////////////////////////////////////
	}
	///////////////////////////////////////////////////////////////
	if(round($ini_unalloc,2) != round($tot_saved,2)){
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= 'Maximum Qty is '.$tot_saved;
		$response['sql'] 		= $error_sql;
	}
 	else if($savedStatus)
	{
		$db->commit();
		$response['type'] 		 = "pass";
		$response['msg'] 		 = "Un allocated successfully.";
		$response['qty25Allc'] 	 = $qty25Allc;
		$response['qty100Alloc'] = $qty100Alloc;
	}
	else
	{
		$db->rollback();
		$response['type'] 		= "fail";
		$response['msg'] 		= $savedMasseged;
		$response['sql'] 		= $error_sql;
	}
	echo json_encode($response);
}

else if($requestType=='OrderWiseUnallocate')
{
	
	
	$saveQty		= 0;
	$saveStatus		= true;
	$saveMasseged 	= '';
	$errorSql		= '';
	$r				=0;
	
	$db->begin();
	
  	$arr 	 		= json_decode($_REQUEST['arr'], true);
	foreach($arr as $arrVal)
	{
		$r++;
		$orderNo 				= $arrVal['orderNo'];
		$arr_Order				= explode('/',$orderNo);
		$orderNo				= $arr_Order[0];
		$orderYear				= $arr_Order[1];
		$salesOrder 			= $arrVal['saleOrder'];
		$item		 			= $arrVal['item'];
		$itemType	 			= $arrVal['itemType'];
		$Qty			 	 	= $arrVal['qty'];
		$Qty 	  				= $obj_common->ceil_to_two_decimal_places($Qty, 2);
		$saveQty				=0;
		$resultG = $obj_warehouse_get->get_grn_wise_style_stock($location,$orderNo,$orderYear,$salesOrder,$item,'RunQuery2');
		while($rowG=mysqli_fetch_array($resultG)){
				$stockBal = $obj_common->ceil_to_two_decimal_places($rowG['stockBal'], 2);
				$Qty 	  = $Qty-$saveQty;
				if(($Qty>0) && ($stockBal>0)){
				if($stockBal>=$Qty){
					$saveQty=$Qty;
					//$Qty=0;
					}
					else if($Qty>$stockBal){
					$saveQty=$stockBal;
					//$Qty=$Qty-$saveQty;
					}
					$grnNo=$rowG['intGRNNo'];
					$grnYear=$rowG['intGRNYear'];
					$grnDate=$rowG['dtGRNDate'];
					$grnRate=$rowG['dblGRNRate'];	
					$currency=$obj_common->get_currency_name_of_grn_item($grnNo,$grnYear,$item,'RunQuery2');
		
					if($saveQty>0)
					{
						//$Qty =  $Qty - $saveQty;
						$resultBulk_arr	= $obj_auto_allo_set->saveBulkTransaction($company,$location,0,0,$grnNo,$grnYear,$grnDate,$currency,$grnRate,$item,$saveQty,'UNALLOCATE',$orderNo,$orderYear,$salesOrder,$userId);
						if($resultBulk_arr['savedStatus']=='fail' && $saveStatus)
						{
							$saveStatus		= false;
							$saveMasseged 	= $resultBulk_arr['savedMassege'];
							$errorSql		= $resultBulk_arr['error_sql'];
						}
						
						$resultstyle_arr	= $obj_auto_allo_set->saveStyleTransaction($company,$location,0,0,$grnNo,$grnYear,$grnDate,$currency,$grnRate,$item,$saveQty*-1,'UNALLOCATE',$orderNo,$orderYear,$salesOrder,$userId);
						if($resultBulk_arr['savedStatus']=='fail' && $saveStatus)
						{
							$saveStatus		= false;
							$saveMasseged 	= $resultBulk_arr['savedMassege'];
							$errorSql		= $resultBulk_arr['error_sql'];
						}
					}
				}
		}
	}

				if($r==0){
					$db->rollback();
					$data['type']	= 'fail';
					$data['msg']	= 'No recoreds to save';
				}
				else if(!$saveStatus)
				{
					$db->rollback();
					$data['type']	= 'fail';
					$data['msg']	= $saveMasseged;
					$data['error_sql']		= $errorSql;
				}
				else
				{
					$db->commit();
					$data['type']	= 'pass';
					$data['msg']	= 'Un Allocated Successfully';
					$data['error_sql']		= '';
				}
		
		$db->CloseConnection();		
	echo json_encode($data);
}
		$db->CloseConnection();		

function allocatedata($orderYear,$orderNo,$salesOrderId,$itemId,$allocateQty,$type)
{
	global $userId;
	global $location;
	global $company;
	global $obj_auto_allo_get;
	global $obj_auto_allo_set;
	global $obj_common;
	
	$saveQty		= 0;
	$saveStatus		= true;
	$saveMasseged 	= '';
	$errorSql		= '';
	$allocateQty    = $obj_common->ceil_to_two_decimal_places($allocateQty, 2);
	//$allocatedQty	= $obj_warehouse_get->get_already_allocated_qty($orderYear,$orderNo,$salesOrderId,$itemId,'RunQuery2');;
	
	$result_grnWise		= $obj_auto_allo_get->getGrnWiseStockBalance_bulk($location,$itemId);
	while($row = mysqli_fetch_array($result_grnWise))
	{
		if(($allocateQty>0) && ($row['stockBal']>0))
		{
			if($row['stockBal']>=$allocateQty)
			{
				$saveQty 		= $allocateQty;
				$allocateQty	= 0;
			}
			else if($allocateQty>$row['stockBal'])
			{
				$saveQty		= $row['stockBal'];
				$allocateQty	= $allocateQty-$saveQty;
			}
			$grnNo			= $row['intGRNNo'];
			$grnYear		= $row['intGRNYear'];
			$grnDate		= $row['dtGRNDate'];
			$grnRate		= $row['dblGRNRate'];	
			$currency		= $obj_auto_allo_get->loadCurrency($grnNo,$grnYear,$itemId);
			
			if($saveQty>0)
			{
				$resultBulk_arr	= $obj_auto_allo_set->saveBulkTransaction($company,$location,0,0,$grnNo,$grnYear,$grnDate,$currency,$grnRate,$itemId,$saveQty*-1,$type,$orderNo,$orderYear,$salesOrderId,$userId);
				if($resultBulk_arr['savedStatus']=='fail' && $saveStatus)
				{
					$saveStatus		= false;
					$saveMasseged 	= $resultBulk_arr['savedMassege'];
					$errorSql		= $resultBulk_arr['error_sql'];
				}
				
				$resultstyle_arr	= $obj_auto_allo_set->saveStyleTransaction($company,$location,0,0,$grnNo,$grnYear,$grnDate,$currency,$grnRate,$itemId,$saveQty,$type,$orderNo,$orderYear,$salesOrderId,$userId);
				if($resultBulk_arr['savedStatus']=='fail' && $saveStatus)
				{
					$saveStatus		= false;
					$saveMasseged 	= $resultBulk_arr['savedMassege'];
					$errorSql		= $resultBulk_arr['error_sql'];
				}
			}
		}
	}
	if(!$saveStatus)
	{
		$data['savedStatus']	= 'fail';
		$data['savedMassege']	= $saveMasseged;
		$data['error_sql']		= $errorSql;
	}
	return $data;
}

function allocatedata_none_sales($orderYear,$orderNo,$salesOrderId,$itemId,$allocateQty,$type)
{
	global $userId;
	global $location;
	global $company;
	global $obj_auto_allo_get;
	global $obj_auto_allo_set;
	global $obj_warehouse_get;
	global $obj_common;
	
	$saveQty		= 0;
	$saveStatus		= true;
	$saveMasseged 	= '';
	$errorSql		= '';
	$allocateQty	= $obj_common->ceil_to_two_decimal_places($allocateQty, 2);
	
	$result_s			=	$obj_warehouse_get->get_sales_order_no_wise_result($orderNo,$orderYear,$itemId,$allocateQty,'RunQuery2');
	while($row_s=mysqli_fetch_array($result_s)){
		$salesOrderId 	= $row_s['intSalesOrderId'];
 		//$allocatedQty	= $obj_warehouse_get->get_already_allocated_qty($orderYear,$orderNo,$salesOrderId,$itemId,'RunQuery2');;
 	//----------------------------------------------------------


	$result_grnWise		= $obj_auto_allo_get->getGrnWiseStockBalance_bulk($location,$itemId);
	while($row = mysqli_fetch_array($result_grnWise))
	{
		if(($allocateQty>0) && ($row['stockBal']>0))
		{
			if($row['stockBal']>=$allocateQty)
			{
				$saveQty 		= $allocateQty;
				$allocateQty	= 0;
			}
			else if($allocateQty>$row['stockBal'])
			{
				$saveQty		= $row['stockBal'];
				$allocateQty	= $allocateQty-$saveQty;
			}
			$grnNo			= $row['intGRNNo'];
			$grnYear		= $row['intGRNYear'];
			$grnDate		= $row['dtGRNDate'];
			$grnRate		= $row['dblGRNRate'];	
			$toSaveQty		= $saveQty;
			$currency		= $obj_auto_allo_get->loadCurrency($grnNo,$grnYear,$item);
			
				if($saveQty>0)
				{
					$resultBulk_arr	= $obj_auto_allo_set->saveBulkTransaction($company,$location,0,0,$grnNo,$grnYear,$grnDate,$currency,$grnRate,$itemId,$saveQty*-1,$type,$orderNo,$orderYear,$salesOrderId,$userId);
					if($resultBulk_arr['savedStatus']=='fail' && $saveStatus)
					{
						$saveStatus		= false;
						$saveMasseged 	= $resultBulk_arr['savedMassege'];
						$errorSql		= $resultBulk_arr['error_sql'];
					}
					
					$resultstyle_arr	= $obj_auto_allo_set->saveStyleTransaction($company,$location,0,0,$grnNo,$grnYear,$grnDate,$currency,$grnRate,$itemId,$saveQty,$type,$orderNo,$orderYear,$salesOrderId,$userId);
					if($resultBulk_arr['savedStatus']=='fail' && $saveStatus)
					{
						$saveStatus		= false;
						$saveMasseged 	= $resultBulk_arr['savedMassege'];
						$errorSql		= $resultBulk_arr['error_sql'];
					}
				}
		}
	}
		$allocateQty = $allocateQty-$saveQty;
	//--------------------------------
	}
	if(!$saveStatus)
	{
		$data['savedStatus']	= 'fail';
		$data['savedMassege']	= $saveMasseged;
		$data['error_sql']		= $errorSql;
	}
	return $data;
}

function unAllocatedata($orderYear,$orderNo,$salesOrderId,$itemId,$unallocateQty)
{
	global $userId;
	global $location;
	global $company;
	global $obj_auto_allo_get;
	global $obj_auto_allo_set;
	global $obj_warehouse_get;
	global $obj_common;
	
	$saveQty		= 0;
	$qty25Allc		= 0;
	$qty100Alloc	= 0;
	$saveStatus		= true;
	$saveMasseged 	= '';
	$errorSql		= '';
	
	$type	 = 'UNALLOCATE';
	$Qty	 = $unallocateQty*(-1);
	$resultG = $obj_warehouse_get->get_grn_wise_style_stock($location,$orderNo,$orderYear,$salesOrderId,$itemId,'RunQuery2');
	while($rowG=mysqli_fetch_array($resultG)){
			$stockBal = $rowG['stockBal'];
			$Qty 	  = $obj_common->ceil_to_two_decimal_places($Qty, 2);
			if(($Qty>0) && ($stockBal>0)){
				if($stockBal>=$Qty){
				$saveQty=$Qty;
				//$Qty=0;
				}
				else if($Qty>$stockBal){
				$saveQty=$stockBal;
				//$Qty=$Qty-$saveQty;
				}
				$grnNo=$rowG['intGRNNo'];
				$grnYear=$rowG['intGRNYear'];
				$grnDate=$rowG['dtGRNDate'];
				$grnRate=$rowG['dblGRNRate'];	
				$currency		= $obj_auto_allo_get->loadCurrency($grnNo,$grnYear,$itemId);
				//$saveQty=round($saveQty+0.04,1);
				$Qty=$Qty-$saveQty;
							
			if($saveQty>0){	
 				$resultBulk_arr	= $obj_auto_allo_set->saveBulkTransaction($company,$location,0,0,$grnNo,$grnYear,$grnDate,$currency,$grnRate,$itemId,$saveQty,'UNALLOCATE',$orderNo,$orderYear,$salesOrderId,$userId);
				if($resultBulk_arr['savedStatus']=='fail' && $saveStatus)
				{
					$saveStatus		= false;
					$saveMasseged 	= $resultBulk_arr['savedMassege'];
					$errorSql		= $resultBulk_arr['error_sql'];
				}
				
				$resultstyle_arr	= $obj_auto_allo_set->saveStyleTransaction($company,$location,0,0,$grnNo,$grnYear,$grnDate,$currency,$grnRate,$itemId,$saveQty*-1,'UNALLOCATE',$orderNo,$orderYear,$salesOrderId,$userId);
				if($resultBulk_arr['savedStatus']=='fail' && $saveStatus)
				{
					$saveStatus		= false;
					$saveMasseged 	= $resultBulk_arr['savedMassege'];
					$errorSql		= $resultBulk_arr['error_sql'];
				}
			}
		}
	}
	if(!$saveStatus)
	{
		$data['savedStatus']	= 'fail';
		$data['savedMassege']	= $saveMasseged;
		$data['error_sql']		= $errorSql;
	}
	else
	{
		$data['savedStatus']	= 'pass';
		$data['qty100Alloc']	= $qty100Alloc;
		$data['qty25Allc']		= $qty25Allc;
	}
	return $data;
}

function unAllocatedata_none_sales($orderYear,$orderNo,$salesOrderId,$itemId,$unallocateQty)
{
	global $userId;
	global $location;
	global $company;
	global $obj_auto_allo_get;
	global $obj_auto_allo_set;
	global $obj_warehouse_get;
	global $obj_common;
	
	$saveQty		= 0;
	$qty25Allc		= 0;
	$qty100Alloc	= 0;
	$saveStatus		= true;
	$saveMasseged 	= '';
	$errorSql		= '';
	
	$result_allocateData	= $obj_auto_allo_get->getAllocatedData($orderYear,$orderNo,$salesOrderId,$itemId,$location,'RunQuery2');
	
	while($row = mysqli_fetch_array($result_allocateData))
	{
		$intSalesOrderId= $row['intSalesOrderId'];
		if($unallocateQty>0)
		{
			if($row['strType']=='100_ITEM_ALLOC')
			{
				if($unallocateQty>abs($row['dblQty']))
				{
					$saveQty 		= abs($row['dblQty']);
					$unallocateQty	= $unallocateQty-$saveQty;
					$qty100Alloc	+= $saveQty;
				}
				else if($unallocateQty<=abs($row['dblQty']))
				{
					$saveQty 		= $unallocateQty;
					$unallocateQty	= 0;
					$qty100Alloc	+= $saveQty;
				}
			}
			else if($row['strType']=='25_ITEM_ALLOC')
			{
				if($unallocateQty>abs($row['dblQty']))
				{
					$saveQty 		= abs($row['dblQty']);
					$unallocateQty	= $unallocateQty-$saveQty;
					$qty25Allc		+= $saveQty;
				}
				else if($unallocateQty<=abs($row['dblQty']))
				{
					$saveQty 		= $unallocateQty;
					$unallocateQty	= 0;
					$qty25Allc		+= $saveQty;
				}
			}
			
			$grnNo			= $row['intGRNNo'];
			$grnYear		= $row['intGRNYear'];
			$grnDate		= $row['dtGRNDate'];
			$grnRate		= $row['dblGRNRate'];	
			$currency		= $row['intCurrencyId'];
			
			if($saveQty>0)
			{
				$resultBulk_arr	= $obj_auto_allo_set->saveBulkTransaction($company,$location,0,0,$grnNo,$grnYear,$grnDate,$currency,$grnRate,$itemId,$saveQty,'UNALLOCATE',$orderNo,$orderYear,$salesOrderId,$userId);
				if($resultBulk_arr['savedStatus']=='fail' && $saveStatus)
				{
					$saveStatus		= false;
					$saveMasseged 	= $resultBulk_arr['savedMassege'];
					$errorSql		= $resultBulk_arr['error_sql'];
				}
				
				$resultstyle_arr	= $obj_auto_allo_set->saveStyleTransaction($company,$location,0,0,$grnNo,$grnYear,$grnDate,$currency,$grnRate,$itemId,$saveQty*-1,'UNALLOCATE',$orderNo,$orderYear,$salesOrderId,$userId);
				if($resultBulk_arr['savedStatus']=='fail' && $saveStatus)
				{
					$saveStatus		= false;
					$saveMasseged 	= $resultBulk_arr['savedMassege'];
					$errorSql		= $resultBulk_arr['error_sql'];
				}
			}
		}
	}
	if(!$saveStatus)
	{
		$data['savedStatus']	= 'fail';
		$data['savedMassege']	= $saveMasseged;
		$data['error_sql']		= $errorSql;
	}
	else
	{
		$data['savedStatus']	= 'pass';
		$data['qty100Alloc']	= $qty100Alloc;
		$data['qty25Allc']		= $qty25Allc;
	}
	return $data;
}

function checkAllocatedQty($orderYear,$orderNo,$salesOrder,$itemId,$allocateQty,$type)
{
	global $location;
	global $obj_auto_allo_get;
	global $obj_warehouse_get;
	global $obj_common;
	global $sampleAP;
	global $fullAP;
	global $extraAP1;
	global $extraAP2;
	global $extraAP3;
	
	$allocatedQty			= $obj_auto_allo_get->getAllocatedQty($orderYear,$orderNo,$salesOrder,$itemId,'RunQuery2');
	
	$result		= $obj_warehouse_get->get_order_item_styleAllocation($orderNo,$orderYear,$salesOrder,$itemId,'RunQuery2');
	while($row = mysqli_fetch_array($result))
	{
		switch($row['allocationType'])
		{
			case 1 :
				$extraAP = $extraAP1;
			break;
			
			case 2 :
				$extraAP = $extraAP2;
			break;
			
			case 3 :
				$extraAP = $extraAP3;
			break;
		}
		if($row['allocationType']==1)
		{
			$day_cons	= $obj_warehouse_get->get_production_ink_first_day_consumptions_salesOrder_wise($orderNo,$orderYear,$salesOrder,$row['intId'],'RunQuery2');
		}
		else
		{		
			$day_cons	= $obj_warehouse_get->get_order_item_first_second_consumption($orderNo,$orderYear,$salesOrder,$row['intId'],'RunQuery2');
		}		
		
		if($type=='25_ITEM_ALLOC'){
			$val   					=   $row['consum']*$row['orderQty']*$sampleAP/100;
			$balToAllocate		=   $obj_common->ceil_to_two_decimal_places(($val*(1+$extraAP/100)), 2) -$obj_common->ceil_to_two_decimal_places($allocatedQty, 2);
		
		}else if($type=='100_ITEM_ALLOC'){
			$val   					=   $day_cons*$row['orderQty']*$fullAP/100;
			$balToAllocate		=   $obj_common->ceil_to_two_decimal_places(($val*(1+$extraAP/100)), 2) -$obj_common->ceil_to_two_decimal_places($allocatedQty, 2);
		
		}
		if($allocateQty>$balToAllocate)
		{
			$data['savedStatus']	= 'fail';
			$data['savedMassege']	= 'Allocated qty exceed balance to allocate qty.';
		}
	}
	return $data;
}
function checkUnAllocatedQty($orderYear,$orderNo,$salesOrderId,$itemId,$unallocateQty)
{

	global $location;
	global $obj_auto_allo_get;
	global $obj_warehouse_get;
	global $obj_common;

	$allocatedQty	= $obj_auto_allo_get->getAllocatedQty($orderNo,$orderYear,$salesOrderId,$itemId,'RunQuery2');
	
	if($unallocateQty>$allocatedQty)
	{
		$data['savedStatus']	= 'fail';
		$data['savedMassege']	= 'No Allocated Qty Balane to Unallocate.';
	} 
	return $data;
}
?>
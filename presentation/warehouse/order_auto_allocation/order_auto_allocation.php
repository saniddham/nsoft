<?php
// H B G KORALA //
session_start();
$backwardseperator 	= "../../../";
$mainPath			= $_SESSION['mainPath'];
$location 			= $_SESSION['CompanyID'];
$company 			= $_SESSION['headCompanyId'];
$thisFilePath 		=  $_SERVER['PHP_SELF'];
$intUser  			= $_SESSION["userId"];
include_once 	"../../../class/cls_commonFunctions_get.php";

include_once  	"{$backwardseperator}dataAccess/permisionCheck.inc";
include_once 	"../../../class/warehouse/order_auto_allocation/cls_order_auto_allocation_get.php";

$obj_common			= new cls_commonFunctions_get($db);
$obj_auto_allo_get	= new Cls_order_auto_allocation_get($db);

$programName		= 'Order Auto Allocation';
$programCode		= 'P0794';

$AP_arr				= $obj_auto_allo_get->getAllocationPercentages($company,'RunQuery');
$sampleAP			= $AP_arr['SAMPLE_ALLOCATION_%'];
$fullAP				= $AP_arr['FULL_ALLOCATION_%'];
//echo $val1	=	 ceil(8.461475*100)/100;
//echo '<br>'.$val2	=	 ($val1*100)/100;

//echo $val1 	  = $obj_common->ceil_to_two_decimal_places(8.461475,2);
//echo '<br>'.$val2 	  = $obj_common->ceil_to_two_decimal_places(8.47,2);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Order Allocation</title>

<link rel="stylesheet" type="text/css" href="../../../css/mainstyle.css"/>
<link rel="stylesheet" type="text/css" href="../../../css/button.css"/>
<link rel="stylesheet" type="text/css" href="../../../css/promt.css"/>


<script src="../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
</head>
<body>
<form id="frmAutoAllocation" name="frmAutoAllocation" method="post" action="">
<table width="100%" border="0" align="center">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
        <script type="application/javascript" src="order_auto_allocation-js.js"></script>
	</tr> 
</table>	
    <div align="center">
        <div class="trans_layoutL" style="width:1180px" >
        <div class="trans_text">Order Allocation</div> 
        <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
        <tr>
            <td>
            <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
            <tr>
                <td align="left">
                    <table width="100%" border="0" align="center" bgcolor="#FFFFFF"   class="tableBorder_allRound">
                    <tr class="normalfnt">
                    	<td width="9%">Order No</td>
                        <td width="16%"><select name="cboOrderYear" id="cboOrderYear" style="width:55px"  class="validate[required] clsOrderYear">
                        <?php 
							echo $obj_auto_allo_get->getOrderYearCombo('RunQuery',date('Y')); 
						?>
						</select>
						<select name="cboOrderNo" id="cboOrderNo" style="width:85px"  class="validate[required] clsOrderNo">
                        </select></td>
                    	<td width="17%">Sales Order No</td>
                    	<td width="18%"><select name="cboSalesOrder" id="cboSalesOrder" style="width:165px"  class="clsSalesOrder">
                        </select></td>
                        <td width="15%">Date</td>
                        <td width="13%"><input name="txtDate" type="text" value="<?php echo date('Y-m-d'); ?>" class="validate[required]" id="txtDate" style="width:98px;" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                        <td width="12%" align="right"><a class="button green small" id="butAllocate25" style="display:none">&nbsp;<?php echo $sampleAP; ?>% Allocate&nbsp;</a></td>
                      </tr>
                    <tr class="normalfnt">
                    	<td height="24">Order Qty<span class="compulsoryRed"></span></td>
                   	  <td ><input type="text" name="txtOrderQty" id="txtOrderQty" style="width:145px; text-align:right" disabled="disabled"/></td>
                        <td >Sample ConPC based Qty (<?php echo $sampleAP; ?>%)</td>
                        <td><input type="text" name="txtSampleQty25" id="txtSampleQty25" style="width:98px; text-align:right" disabled="disabled"/></td>
                        <td>Day ConPC based Qty (<?php echo ($fullAP-$sampleAP); ?>%)</td>
                        <td><input type="text" name="txtBulkQty75" id="txtBulkQty75" style="width:98px; text-align:right" disabled="disabled"/></td>
                        <td align="right"><a class="button green small" id="butAllocate100" style="display:none"><?php echo $fullAP; ?>% Allocate</a></td>
                    </tr>                        </table>
                </td>
            </tr>
            <tr>
            <td align="left">
            <table width="100%" border="0"  bgcolor="#FFE8E8"   class="tableBorder_allRound">
            <tr class="normalfnt">
            <td width="14%">1st Stage Allocation %</td>
            <td width="6%">: <?php echo $AP_arr['SAMPLE_ALLOCATION_%']; ?>%</td>
            <td width="16%">2nd Stage Allocation %</td>
            <td width="5%">: <?php echo $AP_arr['FULL_ALLOCATION_%']; ?>%</td>
            <td width="14%"></td>
            <td width="43%"></td>
            </tr>
            <tr class="normalfnt">
            <td>Foil item Extra Allocation %</td>
            <td>: <?php echo $AP_arr['FOIL_EXTRA_ALLOCATION_%']; ?>%</td>
            <td>Special RM Extra Allocation %</td>
            <td>: <?php echo $AP_arr['SP_RM_EXTRA_ALLOCATION_%']; ?>%</td>
            <td>Ink Item Extra Allocation %</td>
            <td>: <?php echo $AP_arr['INK_ITEM_EXTRA_ALLOCATION_%']; ?>%</td>
            <td width="2%"></td>
            </tr>
            </table>
            </td>
            </tr>
            <tr>
                <td class="normalfnt"><div style="height:350px;overflow:scroll;width:1180px" >
                <table style="width:1725px" class="bordered" id="tblMain">
                    <thead>
                        <tr>
                          <th style="width:20px" >Allocated</th>
                            <th style="width:100px" >Item Type</th>
                            <th style="width:310px" >Item</th>
                            <th style="width:60px" >Unit </th>
                            <th style="width:80px" >Samp ConPC</th>
                            <th style="width:80px" >Cumulative ConPC</th>
                          	<th style="width:70px" >Allocated Qty</th>
                            <th style="width:100px" >Bal to allocate from<?php echo $sampleAP; ?>%</th>
                            <th style="width:100px" >Bal to allocate from Cumulative ConPC </th>
                            <th style="width:100px" >Unallocatable </th>
                            <th style="width:100px" >Bulk stock balance</th>
                            <th style="width:100px" >Style Stock balance</th>
                            <th style="width:70px" >Issued</th>
                            <th style="width:10px" >Returned</th>
                            <th style="width:195px" >Allocate</th>
                            <th style="width:210px" >Unallocate</th>
                        </tr>
                    </thead>
                    <tbody id="tblMain_tbody">
                    		
                    </tbody>
                </table>
                </div></td>
            </tr>
            <tr>
                <td align="left" >
                    <table width="100%" >
                    <tr>
                        <td colspan="2" align="center"><a href="../../../main.php" class="button white medium" id="butSave">Close</a></td>
                    </tr>
                    </table>
                </td>
            </tr>
            <!--end of footer-->
            
            </table>
            </td></tr></table>
        
        </div>
    </div>
</form>

	<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1">
   <!-- <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php" style="width:800;height:800;border:0;overflow:hidden">
    </iframe>-->
    </div>

	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>


</body>
</html>

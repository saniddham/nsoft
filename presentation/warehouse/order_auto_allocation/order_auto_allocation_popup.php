<?php
//ini_set('display_errors',1);
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
session_start();
$backwardseperator = "../../../";
$mainPath 		= $_SESSION['mainPath'];
$location 		= $_SESSION['CompanyID'];
$company 		= $_SESSION['headCompanyId'];
$thisFilePath 	=  $_SERVER['PHP_SELF'];

$itemId 			= $_REQUEST['itemId'];

include_once $backwardseperator."dataAccess/Connector.php";
include_once 	"../../../class/warehouse/order_auto_allocation/cls_order_auto_allocation_get.php";
require_once 	"../../../class/cls_commonFunctions_get.php";
  
$obj_common	= new cls_commonFunctions_get($db);
$obj_auto_allo_get	= new Cls_order_auto_allocation_get($db);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Allocated Details</title>
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />

<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $backwardseperator; ?>css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">


<script type="application/javascript" src="../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/script.js"></script>


</head>

<body>
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>

    
<form id="frmAutoAllocationPopup" name="frmAutoAllocationPopup" method="post" action="">
<table width="750" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutD" style="width:610px">
		  <div class="trans_text">Allocated List</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      
      <tr>
        <td><div style="width:620px;height:300px;overflow:scroll" >
          <table width="100%" class="bordered" id="tblItemsPopup" >
            <tr>
                <th width="8%" height="22" >Select</th>
                <th width="31%" >Description</th>
                <th width="12%">Order No</th>
                <th width="21%">Sales Order</th>
                <th width="12%">Stock Bal</th>
                <th width="16%">Un Alloc Qty</th>
              </tr>
             <?php
			$result		=$obj_auto_allo_get->get_order_wise_stock_balance_result($location,$itemId,'RunQuery');
			while($row 	= mysqli_fetch_array($result)){
				
				$result_s			= $obj_common->load_sales_order_result($row['intOrderNo'],$row['intOrderYear'],$row['intSalesOrderId'],'RunQuery');
				$row_s 				= mysqli_fetch_array($result_s);
				$salesOrder_desc	= $row_s['salesOrder'];
				
			?>
              <tr>
                <td align="center" id=""><input type="checkbox" id="chkUnAllOrder" class="clsUnAllOrder" /></td>
                <td align="center" id="<?php echo $itemId; ?>" class="itemP"><?php echo $row['strName'] ?></td>
                <td align="center" id="" class="orderP"><?php echo $row['intOrderNo'].'/'.$row['intOrderYear'] ?></td>
                <td align="center" id="<?php echo $row['intSalesOrderId']; ?>" class="salesOrderP"><?php echo $salesOrder_desc ?></td>
                <td align="center" id="" class="stkBalP"><?php echo number_format(ceil($row['stk_bal']*100)/100,6); ?></td>
                <td align="center" id=""><input  id="<?php echo $itemId; ?>" value="0" class="clsAlloc validate[custom[number],max[<?php echo ceil($row['stk_bal']*100)/100; ?>]] calculateValue unAllQtyP" style="width:80px;text-align:center" type="text"  /></td>
              </tr>
              <?php
			}
			  ?>
            </table>
          </div></td>
      </tr>
        <td align="center" class="tableBorder_allRound">
        <a class="button white medium" id="butSavePopup" name="butSavePopup">Save</a>
        <a class="button white medium" id="butClose1" name="butClose1">Close</a>
        </td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>

</body>
</html>

$(document).ready(function(){
	
	$("#frmAutoAllocation").validationEngine();
	
	$('#cboSalesOrder').live('change',getOrderQty);
	$('#cboSalesOrder').live('change',loadDetails);
	$('#butAllocate25').live('click',function (){allocateData(this,'alloc25')});
	$('#butAllocate100').live('click',function (){allocateData(this,'alloc100')});
	$('.clsMAllocate').live('click',manualyAllocate);
	$('.clsMUnallocate').live('click',manualyUNAllocate);
	$('#butViewAllocated').live('click',loadPopup);
	$('#butSavePopup').live('click',unAllocateOtherOrders);
	$('#butClose1').live('click',function(){
		//$('#cboSalesOrder').change();
		 		disablePopup();

	});
	$('.clsAlloc').live('keyup',validateDecimals);

	
	$("#cboOrderYear").live('change',function(){
		
		loadComboes('orderYear',1,1,this);
	});
	$("#cboOrderNo").live('change',function(){
		
		loadComboes('orderNo',0,1,this);
		getOrderQty();
		loadDetails();
	});
	
 	
	$('#cboOrderYear').change();
	
});
function loadComboes(slected,flag1,flag2,obj)
{
	$('#txtOrderQty').val('');
	$('#txtSampleQty25').val('');
	$('#txtBulkQty75').val('');
	$('#butAllocate25').hide();
	$('#butAllocate100').hide();
	$("#tblMain tr:gt(0)").remove();	
	
	if($(obj).val()=="")
	{
		if(flag1==1)
			$(obj).parent().parent().find(".clsOrderNo").html('');
		if(flag2==1)
			$(obj).parent().parent().find(".clsSalesOrder").html('');
		return;
	}	
	if(flag1==1)
		$(obj).parent().parent().find(".clsOrderNo").html('');
	if(flag2==1)
		$(obj).parent().parent().find(".clsSalesOrder").html('');
	
	var orderYear	= $(obj).parent().parent().find(".clsOrderYear").val();	
	var orderNo		= $(obj).parent().parent().find(".clsOrderNo").val();
	
	if(orderNo==null)
		orderNo='';
		
	var url 	= "order_auto_allocation-db.php?requestType=loadAllComboes";
	var httpobj = $.ajax({
		url		:url,
		dataType:'json',
		data	:"&orderYear="+orderYear+"&orderNo="+orderNo+"&slected="+slected,
		async	:false,
		success	:function(json)
		{
			if(slected=='orderYear')
				$(obj).parent().parent().find(".clsOrderNo").html(json.combo)
			if(slected=='orderNo')
				$(obj).parent().parent().find(".clsSalesOrder").html(json.combo)		
		}
	});
}
function getOrderQty()
{
	
	if($('#cboOrderNo')=='')
	{
		$('#txtOrderQty').val('');
		$('#txtSampleQty25').val('');
		$('#txtBulkQty75').val('');
		$('#butAllocate25').hide();
		$('#butAllocate100').hide();
		$("#tblMain tr:gt(0)").remove();	
		return;
	}
	var orderNo		= $('#cboOrderNo').val();
	var orderYear	= $('#cboOrderYear').val();
	var salesOrder	= $(this).val();
	
	var url 	= "order_auto_allocation-db.php?requestType=getOrderQty";
	var httpobj = $.ajax({
		url		:url,
		dataType:'json',
		data	:"&orderNo="+orderNo+"&orderYear="+orderYear+"&salesOrder="+salesOrder,
		async	:false,
		success	:function(json)
		{
			var orderQty 	= parseFloat(json.orderQty);
			var sampleAP 	= parseFloat(json.sampleAP);
			var fullAP 		= parseFloat(json.fullAP);
			var sampleQty	= orderQty*(sampleAP/100);
			var bulkQty		= orderQty*((fullAP-sampleAP)/100);
			
			$('#txtOrderQty').val(json.orderQty);
			$('#txtSampleQty25').val(RoundNumber(sampleQty,2));
			$('#txtBulkQty75').val(RoundNumber(bulkQty,2));	
		}
	});
}
function loadDetails()
{
	showWaiting();
	$('#butAllocate25').hide();
	$('#butAllocate100').hide();
	$("#tblMain tr:gt(0)").remove();
	
	var orderNo			= $('#cboOrderNo').val();
	var orderYear		= $('#cboOrderYear').val();
	var salesOrder		= $('#cboSalesOrder').val();
	
	//if(salesOrder=='' || orderNo=='' || orderYear=='')
	if(orderNo=='' || orderYear=='')
	{
		hideWaiting();
		return;	
	}
	var url 	= "order_auto_allocation-db.php?requestType=loadDetails";
	var httpobj = $.ajax({
		url		:url,
		dataType:'json',
		data	:"&orderNo="+orderNo+"&orderYear="+orderYear+"&salesOrder="+salesOrder,
		async	:false,
		success	:function(json)
		{
			if(json.arrOrderData!=null)
			{
				var dayConStatus	= true;
				var lengthDetail   	= json.arrOrderData.length;
				var arrOrderData  	= json.arrOrderData;
				
				for(var i=0;i<lengthDetail;i++)
				{
					var maxAllocation		= 0;
					var type				= arrOrderData[i]['type'];
					var type_id				= arrOrderData[i]['type_id'];
					var itemName			= arrOrderData[i]['itemName'];
					var itemId				= arrOrderData[i]['itemId'];	
					var UOM					= arrOrderData[i]['unit'];
					var sampConPC			= arrOrderData[i]['sampConPC'];
					var firstDayConsm		= arrOrderData[i]['1stDayConsm'];
					var allocatedQty		= arrOrderData[i]['allocatedQty'];
					var balToAllc25			= arrOrderData[i]['balToAllc25'];
					var balToAllc1stDayCon	= arrOrderData[i]['balToAllc1stDayCon'];
					var bulkStockBal		= arrOrderData[i]['bulkStockBal'];
					var colorRoomStock		= arrOrderData[i]['colorRoomStock'];
					var issued				= arrOrderData[i]['issued'];
					var returned			= arrOrderData[i]['returned'];
					var balToAllc_tot		= arrOrderData[i]['balToAllc_tot'];
					
					if(firstDayConsm=='')
						dayConStatus = false;
					
					if(balToAllc25!='')
						var maxAllocation	= balToAllc25;
					if(balToAllc1stDayCon!='')
						var maxAllocation	= balToAllc1stDayCon;
					
					createGrid(type,type_id,itemName,itemId,UOM,sampConPC,firstDayConsm,allocatedQty,balToAllc25,balToAllc1stDayCon,bulkStockBal,colorRoomStock,issued,returned,maxAllocation,balToAllc_tot);
				}
				if(dayConStatus)
				{
					$('#butAllocate100').show();
					$('#butAllocate25').hide();
				}
				else
				{
					$('#butAllocate25').show();
					$('#butAllocate100').hide();
				}
			}
			else
			{
				hideWaiting();
			}
		}
	});
	hideWaiting();
}
function createGrid(type,type_id,itemName,itemId,UOM,sampConPC,firstDayConsm,allocatedQty,balToAllc25,balToAllc1stDayCon,bulkStockBal,colorRoomStock,issued,returned,maxAllocation,balToAllc_tot)
{
	bulkStockBal	= parseFloat(bulkStockBal);
	bulkStockBal	= bulkStockBal.toFixed(2);
	
	balToAllc_tot	= parseFloat(balToAllc_tot);
	var bgColor	= '';
	if(balToAllc_tot > 0){
		bgColor	= '#FFEAEA';
	}
	
	var unallocatable_qty = 0;
	if(balToAllc1stDayCon <0 ){
		unallocatable_qty = balToAllc1stDayCon*-1;
		balToAllc1stDayCon =0;
	}
	if(balToAllc25 <0 ){
		unallocatable_qty += balToAllc25*-1;
		balToAllc25 =0;
	}

	var tbl 		= document.getElementById('tblMain_tbody');
	var lastRow		= tbl.rows.length;	
	var row 		= tbl.insertRow(lastRow);
	row.id			= itemId;
	row.style.backgroundColor = bgColor;
	
	var cell		= row.insertCell(0);
	cell.setAttribute("style",'text-align:left');
	cell.innerHTML	= '<a id="butViewAllocated" class="button gray small">View</a>';
	
	var cell		= row.insertCell(1);
	cell.setAttribute("style",'text-align:left');
	cell.className	= 'clsType';
	cell.innerHTML  = type;
	cell.id			= type_id;

	var cell		= row.insertCell(2);
	cell.setAttribute("style",'text-align:left');
	cell.className	= 'clsItemName';
	cell.innerHTML  = itemName;
	
	var cell		= row.insertCell(3);
	cell.setAttribute("style",'text-align:center');
	cell.className	= 'clsUnit';
	cell.innerHTML  = UOM;
	
	var cell		= row.insertCell(4);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'clsSampConPC';
	cell.innerHTML  = (sampConPC==''?'&nbsp;':sampConPC.toFixed(6));
	
	var cell		= row.insertCell(5);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'clsFirstDayConPC';
	cell.innerHTML  = (firstDayConsm==''?'&nbsp;':firstDayConsm.toFixed(6));
	
	var cell		= row.insertCell(6);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'clsAllocatedQty';
	cell.innerHTML  = (allocatedQty==''?'&nbsp;':allocatedQty);
	
	var cell		= row.insertCell(7);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'clsBalToAllc25';
	cell.innerHTML  = (balToAllc25==''?'&nbsp;':balToAllc25);
	
	var cell		= row.insertCell(8);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'clsBalToAllc1stDayCon';
	cell.innerHTML  = (balToAllc1stDayCon==''?'&nbsp;':balToAllc1stDayCon);
	
	
	cell			= row.insertCell(9);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'clsUnallocatable';
	cell.innerHTML  = (unallocatable_qty==''?'&nbsp;':unallocatable_qty);
	
	var cell		= row.insertCell(10);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'clsBulkStockBal';
	cell.className	= 'clsBulkStockBal';
	cell.bgColor	= '#FFDDDD';
	cell.innerHTML  = (bulkStockBal==''?'&nbsp;':bulkStockBal);
	
	var cell		= row.insertCell(11);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'clsColorRoomStock';
	cell.bgColor	= '#FFFFB7';
	cell.innerHTML  = (colorRoomStock==''?'&nbsp;':colorRoomStock);
	
	var cell		= row.insertCell(12);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'clsIssued';
	cell.innerHTML  = (issued==''?'&nbsp;':issued);
	
	var cell		= row.insertCell(13);
	cell.setAttribute("style",'text-align:right');
	cell.className	= 'clsReturned';
	cell.innerHTML  = (returned==''?'&nbsp;':returned);
	
	var cell		= row.insertCell(14);
	cell.setAttribute("style",'text-align:right');
	cell.innerHTML  = "<input name=\"txtMAllocateQty\" id=\"txtMAllocateQty\" type=\"text\" style=\"width:80px\" class=\"clsMAllocateQty clsAlloc validate[required, custom[number]]\" /><a class=\"button gray small clsMAllocate\" id=\"butMAllocate\">Allocate</a>"
	
	var cell		= row.insertCell(15);
	cell.setAttribute("style",'text-align:right');
	cell.innerHTML  = "<input name=\"txtMUnallocateQty\" id=\"txtMUnallocateQty\" type=\"text\" style=\"width:80px\" class=\"clsMUnallocateQty clsAlloc validate[required, custom[number]]\" /><a class=\"button gray small clsMUnallocate\" id=\"butMUnallocate\">Unallocate</a>"
}
function allocateData(obj,allocType)
{
	showWaiting();
	
	var orderYear		= $('#cboOrderYear').val();
	var orderNo			= $('#cboOrderNo').val();
	var salesOrderId	= $('#cboSalesOrder').val();
	
	var chkStatus	= false;
	var arrDetails	= "";
	
	var arrHeader = "{";
						arrHeader += '"orderYear":"'+orderYear+'",' ;
						arrHeader += '"orderNo":"'+orderNo+'",' ;
						arrHeader += '"salesOrderId":"'+salesOrderId+'",' ;
						arrHeader += '"allocType":"'+allocType+'"' ;
		arrHeader  += "}";
	
	var data = "requestType=allocateData";
	$('.clsBalToAllc25').each(function(index, element) {
        
			var itemType	= $(this).parent().find('.clsType').attr('id');
			var itemId		= $(this).parent().attr('id');
			var balAllocSC	= ($(this).html()=='&nbsp;'?'':$(this).html()); // balance to allocate sample consumption
			var balAllocDC	= ($(this).parent().find('.clsBalToAllc1stDayCon').html()=='&nbsp;'?'':$(this).parent().find('.clsBalToAllc1stDayCon').html()); // balance to allocate first day consumption
			
			if(balAllocSC!='' || balAllocDC!='')
			{
				chkStatus	= true;				
				arrDetails += "{";
				arrDetails += '"itemType":"'+ itemType +'",' ;
				arrDetails += '"itemId":"'+ itemId +'",' ;
				arrDetails += '"balAllocSC":"'+ balAllocSC +'",' ;
				arrDetails += '"balAllocDC":"'+ balAllocDC +'"' ;
				arrDetails += "},";
			}
    });
	if(!chkStatus)
	{
		$(obj).validationEngine('showPrompt','No records to Allocate.','fail');
		hideWaiting();
		return;
	}
	arrDetails 		= arrDetails.substr(0,arrDetails.length-1);
		
	var arrHeader	= arrHeader;
	var arrDetails	= '['+arrDetails+']';
	data+="&arrHeader="+arrHeader+"&arrDetails="+arrDetails;
	
	var url = "order_auto_allocation-db.php";
	$.ajax({
			url:url,
			dataType:'json',
			type:'post',
			data:data,
			async:false,
			success:function(json){
				$(obj).validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
				if(json.type=='pass')
				{
					var t = setTimeout("alertx()",3000);
					hideWaiting();
					loadDetails()
					return;
				}
				else
				{
					hideWaiting();
				}
			},
			error:function(xhr,status){
					
				$(obj).validationEngine('showPrompt', errormsg(xhr.status),'fail');
				hideWaiting();
				return;
			}		
	});
}
function manualyAllocate()
{
	var obj	= this;
	if($(this).parent().validationEngine('validate'))
	{
		var orderYear		= $('#cboOrderYear').val();
		var orderNo			= $('#cboOrderNo').val();
		var salesOrderId	= $('#cboSalesOrder').val();
		var itemId			= $(this).parent().parent().attr('id');
		var itemType		= $(this).parent().parent().find('.clsType').attr('id');
		var balAllocSC		= ($(this).parent().parent().find('.clsBalToAllc25').html()=='&nbsp;'?0:$(this).parent().parent().find('.clsBalToAllc25').html());
		var balAllocDC		= ($(this).parent().parent().find('.clsBalToAllc1stDayCon').html()=='&nbsp;'?0:$(this).parent().parent().find('.clsBalToAllc1stDayCon').html());
		var allocateQty		= parseFloat($(this).parent().parent().find('.clsMAllocateQty').val());
		var allocatedQty	= $(this).parent().parent().find('.clsAllocatedQty').html();
		var stockBalance	= $(this).parent().parent().find('.clsBulkStockBal').html();
		
		if(balAllocSC!='')
		{
			if(allocateQty>parseFloat(balAllocSC==''?0:balAllocSC))
			{
				$(this).parent().parent().find('.clsMAllocateQty').validationEngine('showPrompt','Maximum value is '+balAllocSC,'fail');
				return;
			}
		}
		else if(balAllocDC!='')
		{
			if(allocateQty>parseFloat(balAllocDC==''?0:balAllocDC))
			{
				$(this).parent().parent().find('.clsMAllocateQty').validationEngine('showPrompt','Maximum value is '+balAllocDC,'fail');
				return;
			}
		}
		var arrHeader = "{";
							arrHeader += '"orderYear":"'+orderYear+'",' ;
							arrHeader += '"orderNo":"'+orderNo+'",' ;
							arrHeader += '"salesOrderId":"'+salesOrderId+'",' ;
							arrHeader += '"itemId":"'+itemId+'",' ;
							arrHeader += '"itemType":"'+itemType+'",' ;
							arrHeader += '"balAllocSC":'+parseFloat(balAllocSC)+',' ;
							arrHeader += '"allocateQty":"'+allocateQty+'",' ;
							arrHeader += '"balAllocDC":'+parseFloat(balAllocDC)+'' ;
			arrHeader  += "}";
		
		if(allocateQty<=0)
		{
			$(this).validationEngine('showPrompt','value must greater than 0.','fail');
			return;
		}
		var data = "requestType=allocateDataManually";
		data+="&arrHeader="+arrHeader;
	
		var url = "order_auto_allocation-db.php";
		$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				data:data,
				async:false,
				success:function(json){
					$(obj).validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t = setTimeout("alertxM()",3000);
						$(obj).parent().parent().find('.clsAllocatedQty').html(parseFloat(allocatedQty=='&nbsp;'?0:allocatedQty)+allocateQty);
						if(parseFloat(balAllocSC==''?0:balAllocSC)!=0)
							$(obj).parent().parent().find('.clsBalToAllc25').html((parseFloat(balAllocSC==''?0:balAllocSC)-allocateQty).toFixed(6));
						if(parseFloat(balAllocDC==''?0:balAllocDC)!=0)
							$(obj).parent().parent().find('.clsBalToAllc1stDayCon').html((parseFloat(balAllocDC==''?0:balAllocDC)-allocateQty).toFixed(6));
						
						$(obj).parent().parent().find('.clsBulkStockBal').html(parseFloat(stockBalance)-allocateQty);
						$(obj).parent().parent().find('.clsMAllocateQty').val('');
						return;
					}
				},
				error:function(xhr,status){
						
					$(obj).validationEngine('showPrompt', errormsg(xhr.status),'fail');
					return;
				}		
		});
	}
}
function manualyUNAllocate()
{
	var obj	= this;
	if($(this).parent().validationEngine('validate'))
	{
		var orderYear		= $('#cboOrderYear').val();
		var orderNo			= $('#cboOrderNo').val();
		var salesOrderId	= $('#cboSalesOrder').val();
		var itemId			= $(this).parent().parent().attr('id');
		var itemType		= $(this).parent().parent().find('.clsType').attr('id');
		var balAllocSC		= ($(this).parent().parent().find('.clsBalToAllc25').html()=='&nbsp;'?'':$(this).parent().parent().find('.clsBalToAllc25').html());
		var balAllocDC		= ($(this).parent().parent().find('.clsBalToAllc1stDayCon').html()=='&nbsp;'?'':$(this).parent().parent().find('.clsBalToAllc1stDayCon').html());
		var firstDayConPC	= ($(this).parent().parent().find('.clsFirstDayConPC').html()=='&nbsp;'?'':$(this).parent().parent().find('.clsFirstDayConPC').html());
		var unallocateQty	= parseFloat($(this).parent().parent().find('.clsMUnallocateQty').val());
		var allocatedQty	= $(this).parent().parent().find('.clsAllocatedQty').html();
		var stockBalance	= $(this).parent().parent().find('.clsBulkStockBal').html();
		var col_room_kBalance	= $(this).parent().parent().find('.clsColorRoomStock').html();
		
		
		if(unallocateQty>parseFloat(allocatedQty=='&nbsp;'?0:allocatedQty))
		{
			$(this).parent().parent().find('.clsMUnallocateQty').validationEngine('showPrompt','Maximum value is '+allocatedQty,'fail');
			return;
		}
		
		var arrHeader = "{";
							arrHeader += '"orderYear":"'+orderYear+'",' ;
							arrHeader += '"orderNo":"'+orderNo+'",' ;
							arrHeader += '"salesOrderId":"'+salesOrderId+'",' ;
							arrHeader += '"itemId":"'+itemId+'",' ;
							arrHeader += '"itemType":"'+itemType+'",' ;
							arrHeader += '"unallocateQty":"'+unallocateQty+'"' ;
			arrHeader  += "}";
		
		if(parseFloat(unallocateQty)<=0)
		{
			$(this).validationEngine('showPrompt','value must greater than 0.','fail');
			return;
		}
		var data = "requestType=unAllocateDataManually";
		data+="&arrHeader="+arrHeader;
	
		var url = "order_auto_allocation-db.php";
		$.ajax({
				url:url,
				dataType:'json',
				type:'post',
				data:data,
				async:false,
				success:function(json){
					$(obj).validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t = setTimeout("alertxUM()",3000);
						$(obj).parent().parent().find('.clsAllocatedQty').html(parseFloat(allocatedQty=='&nbsp;'?0:allocatedQty)-unallocateQty);
						$(obj).parent().parent().find('.clsBalToAllc25').html((parseFloat(balAllocSC==''?0:balAllocSC)+json.qty25Allc).toFixed(6));
						$(obj).parent().parent().find('.clsBalToAllc1stDayCon').html((parseFloat(balAllocDC==''?0:balAllocDC)+json.qty100Alloc).toFixed(6));
						
						$(obj).parent().parent().find('.clsBulkStockBal').html(parseFloat(stockBalance)+unallocateQty);
						$(obj).parent().parent().find('.clsColorRoomStock').html(parseFloat(col_room_kBalance)-unallocateQty);
						
						$(obj).parent().parent().find('.clsMUnallocateQty').val('');
						$('#cboSalesOrder').change();
						return;
					}
				},
				error:function(xhr,status){
						
					$(obj).validationEngine('showPrompt', errormsg(xhr.status),'fail');
					return;
				}		
		});
	}
}
function alertx()
{
	$('#butAllocate100').validationEngine('hide');
	$('#butAllocate25').validationEngine('hide');
}
function alertxM()
{
	$('.clsMAllocate').validationEngine('hide');
}
function alertxUM()
{
	$('.clsMUnallocateQty').validationEngine('hide');
}

function loadPopup()
{
	var itemId 	= $(this).parent().parent().attr('id');
	var url		=	"order_auto_allocation_popup.php?itemId="+itemId;
		popupWindow3('1');
		$('#popupContact1').load(url);	
}//-------------------------------------------------------

function unAllocateOtherOrders(){

	var requestType = '';
	var data = "";
	if ($('#frmAutoAllocationPopup').validationEngine('validate'))   
    { 
		showWaiting();
			var r=0;
			var arr="[";
			var salesIds='';
			$('#tblItemsPopup .clsUnAllOrder').each(function(){
					if(($(this).is(':checked')) && ($(this).parent().parent().find('.unAllQtyP').val() > 0)){ 
						arr += "{";
						arr += '"orderNo":"'+		$(this).parent().parent().find('.orderP').html() +'",' 	;
						arr += '"saleOrder":"'+		$(this).parent().parent().find('.salesOrderP').attr('id') +'",' 	;
						arr += '"item":"'+			$(this).parent().parent().find('.itemP').attr('id') +'",' 			;
						arr += '"itemType":"'+		$(this).parent().parent().find('.clsType').attr('id') +'",' 			;
						arr += '"qty":"'+			$(this).parent().parent().find('.unAllQtyP').val() +'"' 		;
						arr +=  '},';
						r++;
					}
			});
			arr = arr.substr(0,arr.length-1);
			
			arr += " ]";
			
		if(r==0){
			alert('No selected items to allocate');
			hideWaiting();
			return false;
		}
	
			data+="&arr="	+	arr;

		var url = "order_auto_allocation-db.php?requestType=OrderWiseUnallocate";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json", 
			type:'POST', 
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmAutoAllocationPopup #butSavePopup').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						$('#butSavePopup').hide();
						hideWaiting();

						var t=setTimeout("alertx()",1000);
 					}
				},
			error:function(xhr,status){
					hideWaiting();
					$('#frmAutoAllocationPopup #butSavePopup').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
 				}		
			});
	}
		
 		
 	
		$('#cboSalesOrder').change();
		disablePopup();
	
}
function validateDecimals(){
	var num			=	$(this).val();
	var decimals	=	decimalPlaces(num);
 	if(decimals >2){
		var val1 = num.substring(0, num.length - (decimals-2));
 		$(this).val(val1);
		return false;
	}
	else 
	return true;
	
}

function decimalPlaces(num) {
  var match = (''+num).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
  if (!match) { return 0; }
  return Math.max(
       0,
       // Number of digits right of decimal point.
       (match[1] ? match[1].length : 0)
       // Adjust for scientific notation.
       - (match[2] ? +match[2] : 0));
}

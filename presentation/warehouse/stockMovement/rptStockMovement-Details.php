<?php
$companyId = $_SESSION['CompanyID'];
$locationId 	= $_SESSION['CompanyID'];//this locationId use in report header(reportHeader.php)-------
$intUser  = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
 
$company = $_REQUEST['company'];
$location = $_REQUEST['location'];

$mainCat = $_REQUEST['mainCat'];
$subCat = $_REQUEST['subCat'];
$item = $_REQUEST['item'];
$fromDate 	= $_REQUEST['fromDate'];
$toDate 	= $_REQUEST['toDate'];

//////////////date range //////////////
if($fromDate!='')
	$paraDate = " AND date(dtDate)>='$fromDate' ";

if($toDate!='')
	$paraDate .= " AND date(dtDate)<='$toDate' ";
	


///////////////////////////////////////

 $sql = "SELECT
			mst_companies.strName
			FROM mst_companies
		WHERE
			mst_companies.intId =  '$company'
		";
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
$companyName = $row['strName'];
	
	
				 
 $sql = "SELECT
			mst_locations.strName
		FROM
			mst_locations
		WHERE
			mst_locations.intId =  '$location'
		";
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
$locationName = $row['strName'];

$sql = "SELECT
			mst_subcategory.strName AS subName,
			mst_maincategory.strName AS mainName,
			mst_item.strName AS itemName
		FROM
		mst_maincategory
			Inner Join mst_subcategory ON mst_maincategory.intId = mst_subcategory.intMainCategory
			Inner Join mst_item ON mst_subcategory.intId = mst_item.intSubCategory
		WHERE
			mst_item.intId =  '$item'
		";
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
$subName = $row['subName'];
$mainName = $row['mainName'];
$itemName = $row['itemName'];
?>
 <head>
 <title>General Stock Movement Report</title>
  <style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:264px;
	top:247px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<form id="frmMrnReport" name="frmMrnReport" method="post" action="">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include 'reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>GENERAL STOCK MOVEMENT REPORT</strong><strong></strong></div>
<table width="1200" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
  <tr>
    <td width="7%">&nbsp;</td>
    <td width="19%" class="normalfntMid" align="center"><strong>Main Category</strong></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="28%" align="left"><span class="normalfnt"><?php echo $mainName  ?></span></td>
    <td width="11%"><span class="normalfnt"><strong style="text-align:right">Company</strong></span></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="20%"><span class="normalfnt"><strong style="text-align:right"><span class="normalfnt" style="text-align:left"><?php echo $companyName  ?></span></strong></span></td>
    <td width="6%" class="normalfntMid">&nbsp;</td>
    <td width="7%">&nbsp;</td>
  </tr>
 
  <tr>
    <td>&nbsp;</td>
    <td class="normalfntMid"><strong>Sub Category</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $subName  ?></span></td>
    <td><span class="normalfnt"><strong>Location</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><strong style="text-align:right"><span class="normalfnt" style="text-align:left"><?php echo $locationName;  ?></span></strong></span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfntMid"><strong>Item</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $itemName;  ?></span></td>
    <td>&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>

  
  </table>
  </td>
</tr>
<tr>
  <td><table width="1132"  cellspacing="1" cellpadding="0"  >

      <?PHP
/*	  if($fromDate!='')
		 {
			$datePara = " AND  MAINSTOCK.dtGRNDate>='$fromDate' AND MAINSTOCK.dtGRNDate<='$toDate' ";	 
		 }
	  	 $sql = "SELECT
					MAINSTOCK.intItemId,
					MAINSTOCK.dtGRNDate,
					trn_podetails.intPONo,
					trn_podetails.intPOYear,
					Sum(trn_podetails.dblQty) AS poQty,
					MAINSTOCK.intGRNNo,
					MAINSTOCK.intGRNYear,
					Sum(MAINSTOCK.dblQty) AS grnQty,
					IFNULL(mst_supplier.strName,'OPEN STOCK') AS supplierName
					FROM
					ware_stocktransactions_bulk as MAINSTOCK
					left Join ware_grnheader ON ware_grnheader.intGrnNo = MAINSTOCK.intGRNNo AND ware_grnheader.intGrnYear = MAINSTOCK.intGRNYear
					left Join trn_podetails ON trn_podetails.intPONo = ware_grnheader.intPoNo AND trn_podetails.intPOYear = ware_grnheader.intPoYear AND MAINSTOCK.intItemId = trn_podetails.intItem
					left Join trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
					left Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
				
					WHERE
					(MAINSTOCK.strType =  'GRN' OR MAINSTOCK.strType =  'OPENSTOCK')  AND
					MAINSTOCK.intLocationId =  '$location' AND
					MAINSTOCK.intItemId =  '$item' 
					
					$datePara
					
					GROUP BY
					MAINSTOCK.strType,
					MAINSTOCK.dtGRNDate,
					MAINSTOCK.intLocationId,
					MAINSTOCK.intItemId,
					MAINSTOCK.intGRNNo,
					MAINSTOCK.intGRNYear,
					trn_podetails.intPONo,
					trn_podetails.intPOYear
					";
*/	

	  if($fromDate!='')
		 {
			$datePara = " AND  MAINSTOCK.dtGRNDate>='$fromDate' AND MAINSTOCK.dtGRNDate<='$toDate' ";	 
		 }
	  	 $sql = "SELECT DISTINCT 
					MAINSTOCK.intItemId,
					MAINSTOCK.dtGRNDate,
					trn_podetails.intPONo,
					trn_podetails.intPOYear,
					trn_podetails.dblQty AS poQty,
					MAINSTOCK.intGRNNo,
					MAINSTOCK.intGRNYear,
					MAINSTOCK.dblGRNRate,
					mst_financecurrency.strCode as currency,
					(select sum(ware_stocktransactions_bulk.dblQty) FROM ware_stocktransactions_bulk WHERE ware_stocktransactions_bulk.strType =  'GRN' AND ware_stocktransactions_bulk.intDocumentNo=MAINSTOCK.intGRNNo AND ware_stocktransactions_bulk.intDocumntYear=MAINSTOCK.intGRNYear AND ware_stocktransactions_bulk.intItemId=MAINSTOCK.intItemId) as grnQty, 
					
					IF((select distinct ware_grnheader.intCompanyId 
					FROM ware_stocktransactions_bulk 
					Inner Join ware_grnheader ON ware_stocktransactions_bulk.intDocumentNo = ware_grnheader.intGrnNo AND ware_stocktransactions_bulk.intDocumntYear = ware_grnheader.intGrnYear 
					WHERE ware_stocktransactions_bulk.strType =  'GRN' AND ware_stocktransactions_bulk.intDocumentNo=MAINSTOCK.intGRNNo AND ware_stocktransactions_bulk.intDocumntYear=MAINSTOCK.intGRNYear AND ware_stocktransactions_bulk.intItemId=MAINSTOCK.intItemId)=$location,1,0) as flagLocation,
					
					IFNULL(mst_supplier.strName,'OPEN STOCK') AS supplierName
					FROM
					ware_stocktransactions_bulk as MAINSTOCK
					left Join ware_grnheader ON ware_grnheader.intGrnNo = MAINSTOCK.intGRNNo AND ware_grnheader.intGrnYear = MAINSTOCK.intGRNYear
					left Join trn_podetails ON trn_podetails.intPONo = ware_grnheader.intPoNo AND trn_podetails.intPOYear = ware_grnheader.intPoYear AND MAINSTOCK.intItemId = trn_podetails.intItem
					left Join trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
					left Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
					left Join mst_financecurrency ON MAINSTOCK.intCurrencyId= mst_financecurrency.intId
					WHERE 
					MAINSTOCK.intGRNNo <> 0 AND 
					/*(MAINSTOCK.strType =  'GRN' OR MAINSTOCK.strType LIKE  '%OPENSTOCK%')  AND*/
					MAINSTOCK.intLocationId =  '$location' AND
					MAINSTOCK.intItemId =  '$item' 
					
					$datePara
					
					GROUP BY
					MAINSTOCK.strType,
					MAINSTOCK.dtGRNDate,
					MAINSTOCK.intLocationId,
					MAINSTOCK.intItemId,
					MAINSTOCK.intGRNNo,
					MAINSTOCK.intGRNYear,
					MAINSTOCK.dblGRNRate,
					trn_podetails.intPONo,
					trn_podetails.intPOYear 
					order by  
					ware_grnheader.datdate
					asc
					";
				 	//echo $sql;

		$result = $db->RunQuery($sql);
			$i=0;//RTSUP
			while($row=mysqli_fetch_array($result))
			{
				$grnNo 		= $row['intGRNNo'];
				$grnYear	= $row['intGRNYear'];
				$itemNo 	= $item;
				 
				 $grn=$row['intGRNYear'].'/'.$row['intGRNNo'];
				 if($row['flagLocation']==0){
					$grn="( ".$row['intGRNYear'].'/'.$row['intGRNNo']." )"; 
				 }
	  ?>
      		    <tr>
      <td width="78" height="20" align="center" bgcolor="#469EE6" style="color:#FFF;font-size:11px"><strong>GRN DATE</strong></td>
      <td width="99" align="center" bgcolor="#469EE6" style="color:#FFF;font-size:11px"><strong>PO NO</strong></td>
      <td width="371" align="center" bgcolor="#469EE6" style="color:#FFF;font-size:11px"><strong>SUPPLIER</strong></td>
      <td width="117" align="center" bgcolor="#469EE6" style="color:#FFF;font-size:11px"><strong>PO QTY</strong></td>
      <td width="158" align="center" bgcolor="#469EE6" style="color:#FFF;font-size:11px"><strong>GRN NO</strong></td>
      <td width="105" align="center" bgcolor="#469EE6" style="color:#FFF;font-size:11px"><strong>CURRENCY</strong></td>
      <td width="51" align="center" bgcolor="#469EE6" style="color:#FFF;font-size:11px"><strong>RATE</strong></td>
      <td width="55" align="center" bgcolor="#469EE6" style="color:#FFF;font-size:11px"><strong>GRN QTY</strong></td>
      <td width="86" align="center" bgcolor="#469EE6" style="color:#FFF;font-size:11px"><strong>AMOUNT</strong></td>
            </tr>
            <tr bgcolor="#FFE1C4" >
              <td class="normalfntMid"><?php echo $row['dtGRNDate']; ?></td>
              <td class="normalfntMid"><?php echo $row['intPOYear'].'/'.$row['intPONo']; ?></td>
              <td class="normalfntMid"><?php echo $row['supplierName']; ?></td>
              <td class="normalfntMid"><?php echo $row['poQty']; ?></td>
              <td class="normalfntMid"><?php echo $grn; ?></td>
              <td class="normalfntMid" style="font-size:14px;color:#C63"><b><?php echo $row['currency']; ?></b></td>
              <td class="normalfntMid" style="font-size:14px;color:#C63"><span class="normalfntRight"><?php echo $row['dblGRNRate']; ?></span></td>
              <td class="normalfntMid" style="font-size:14px;color:#C63"><b><?php echo $row['grnQty']; ?></b></td>
              <td class="normalfntMid" style="font-size:14px;color:#C63"><span class="normalfntRight"><?php echo $row['grnQty']*$row['dblGRNRate']; ?></span></td>
            </tr>
            <tr>
            	<td colspan="9">
                
                	<table width="100%">
                    <tr>
                    	<td width="52%" valign="top">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="35%" style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;GRN</td>
                              <td width="10%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"> Date</td>
                              <td width="20%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">&nbsp;</td>
                              <td width="10%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                              <td width="10%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">&nbsp;PRICE&nbsp;</td>
                              <td width="15%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">AMOUNT</td>
                            </tr>
                            <?PHP 
								$sql1 = "SELECT
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear,
											ware_stocktransactions_bulk.dblQty, 
											ware_stocktransactions_bulk.dblGRNRate,
											date(ware_stocktransactions_bulk.dtDate) as date 
										FROM ware_stocktransactions_bulk
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType =  'GRN' AND
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
											$paraDate
										";
										$i=0;
								$result1 = $db->RunQuery($sql1);
								while($row1=mysqli_fetch_array($result1))
								{
									$i++;
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                              <td class="normalfntMid"><?php echo $row1['date']; ?></td>
                              <td>&nbsp;</td>
                              <td class="normalfntRight"><?php echo $row1['dblQty']; ?>&nbsp;</td>
                              <td class="normalfntRight"><?php echo $row1['dblGRNRate']; ?></td>
                              <td class="normalfntRight"><?php echo round($row1['dblQty']*$row1['dblGRNRate'],4); ?></td>
                            </tr>
                            <?php
								}	
							?>
                            <?php
							if($i==0){
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid"></td>
                              <td>&nbsp;</td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                            </tr>
                            <?php
							}
							?>
                          </table>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="35%" style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;OPEN STOCK</td>
                              <td width="10%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"> Date</td>
                              <td width="20%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">&nbsp;</td>
                              <td width="10%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                              <td width="10%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">&nbsp;PRICE&nbsp;</td>
                              <td width="15%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">AMOUNT</td>
                            </tr>
                            <?PHP 
								$sql1 = "SELECT
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear,
											ware_stocktransactions_bulk.dblQty, 
											ware_stocktransactions_bulk.dblGRNRate,
											date(ware_stocktransactions_bulk.dtDate) as date 
										FROM ware_stocktransactions_bulk
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType LIKE  '%OPENSTOCK%' AND
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
											$paraDate
										";
										$i=0;
								$result1 = $db->RunQuery($sql1);
								while($row1=mysqli_fetch_array($result1))
								{
									$i++;
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                              <td class="normalfntMid"><?php echo $row1['date']; ?></td>
                              <td>&nbsp;</td>
                              <td class="normalfntRight"><?php echo $row1['dblQty']; ?>&nbsp;</td>
                              <td class="normalfntRight"><?php echo $row1['dblGRNRate']; ?></td>
                              <td class="normalfntRight"><?php echo round($row1['dblQty']*$row1['dblGRNRate'],4); ?></td>
                            </tr>
                            <?php
								}	
							?>
                            <?php
							if($i==0){
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid"></td>
                              <td>&nbsp;</td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                            </tr>
                            <?php
							}
							?>
                          </table>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="35%" style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;RETURN STORES NO</td>
                              <td width="10%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"> Date</td>
                              <td width="20%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">&nbsp;</td>
                              <td width="10%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                              <td width="10%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">&nbsp;PRICE&nbsp;</td>
                              <td width="15%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">AMOUNT</td>
                            </tr>
                            <?PHP 
								$sql1 = "SELECT
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear,
											ware_stocktransactions_bulk.dblQty, 
											ware_stocktransactions_bulk.dblGRNRate,
											date(ware_stocktransactions_bulk.dtDate) as date 
										FROM ware_stocktransactions_bulk
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType =  'RETSTORES' AND
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
											$paraDate
										";
										$i=0;
								$result1 = $db->RunQuery($sql1);
								while($row1=mysqli_fetch_array($result1))
								{
									$i++;
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                              <td class="normalfntMid"><?php echo $row1['date']; ?></td>
                              <td>&nbsp;</td>
                              <td class="normalfntRight"><?php echo $row1['dblQty']; ?>&nbsp;</td>
                              <td class="normalfntRight"><?php echo $row1['dblGRNRate']; ?></td>
                              <td class="normalfntRight"><?php echo round($row1['dblQty']*$row1['dblGRNRate'],4); ?></td>
                            </tr>
                            <?php
								}	
							?>
                            <?php
							if($i==0){
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid"></td>
                              <td>&nbsp;</td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                            </tr>
                            <?php
							}
							?>
                          </table>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">

                            <tr>
                              <td width="35%" style="color:#666" height="16" bgcolor="#EBFAC7" class="normalfnt">&nbsp;TRANSFER IN NO</td>
                              <td width="10%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"> Date</td>
                              <td width="20%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">FROM COMPANY</td>
                              <td width="10%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                              <td width="10%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">&nbsp;PRICE&nbsp;</td>
                              <td width="15%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">AMOUNT</td>
                            </tr>
                                  <?PHP 
							 	$sql1 = "SELECT
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear,
											ware_stocktransactions_bulk.dblQty, 
											ware_stocktransactions_bulk.dblGRNRate,
											date(ware_stocktransactions_bulk.dtDate) as date , 
											mst_locations.strName as location 
										FROM ware_stocktransactions_bulk 
										Inner Join ware_gatepasstransferinheader ON ware_stocktransactions_bulk.intDocumentNo = ware_gatepasstransferinheader.intGpTransfNo AND ware_stocktransactions_bulk.intDocumntYear = ware_gatepasstransferinheader.intGpTransfYear
										Inner Join ware_gatepassheader ON ware_gatepasstransferinheader.intGatePassNo = ware_gatepassheader.intGatePassNo AND ware_gatepasstransferinheader.intGatePassYear = ware_gatepassheader.intGatePassYear
										Inner Join mst_locations ON ware_gatepassheader.intCompanyId = mst_locations.intId
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType =  'TRANSFERIN' AND
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
											$paraDate
										";
								$result1 = $db->RunQuery($sql1);
								$i=0;
								while($row1=mysqli_fetch_array($result1))
								{
									$i++;
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                              <td class="normalfntMid"><?php echo $row1['date']; ?></td>
                              <td class="normalfntMid"><?php echo $row1['location']; ?></td>
                              <td class="normalfntRight"><?php echo $row1['dblQty']; ?>&nbsp;</td>
                              <td class="normalfntRight"><?php echo $row1['dblGRNRate']; ?></td>
                              <td class="normalfntRight"><?php echo round($row1['dblQty']*$row1['dblGRNRate'],4); ?></td>
                            </tr>
                            <?php
								}	
							?>
                            <?php
							if($i==0){
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid"></td>
                              <td>&nbsp;</td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                            </tr>
                            <?php
							}
							?>
                          </table>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
          
                            <tr>
                              <td width="35%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;UN-ALLOCATION NO</td>
                              <td width="10%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"> Date</td>
                              <td width="20%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">&nbsp;</td>
                              <td width="10%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                              <td width="10%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">&nbsp;PRICE&nbsp;</td>
                              <td width="15%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">AMOUNT</td>
                            </tr>
                                  <?PHP 
								$sql1 = "SELECT
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear,
											round(sum(ware_stocktransactions_bulk.dblQty),2) as dblQty, 
											ware_stocktransactions_bulk.dblGRNRate,
											date(ware_stocktransactions_bulk.dtDate) as date 
										FROM ware_stocktransactions_bulk
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType =  'UNALLOCATE' AND
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
											$paraDate
											group by ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear 
										";
								$result1 = $db->RunQuery($sql1);
								$i=0;
								while($row1=mysqli_fetch_array($result1))
								{
									$i++;
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                              <td class="normalfntMid"><?php echo $row1['date']; ?></td>
                              <td>&nbsp;</td>
                              <td class="normalfntRight"><?php echo $row1['dblQty']; ?>&nbsp;</td>
                              <td class="normalfntRight"><?php echo $row1['dblGRNRate']; ?></td>
                              <td class="normalfntRight"><?php echo round($row1['dblQty']*$row1['dblGRNRate'],4); ?></td>
                            </tr>
                            <?php
								}	
							?>
                            <?php
							if($i==0){
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid"></td>
                              <td>&nbsp;</td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                            </tr>
                            <?php
							}
							?>
                          </table>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        
                            <tr>
                              <td width="35%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;STOCK ADJUSTMENTS +</td>
                              <td width="10%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"> Date</td>
                              <td width="20%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">&nbsp;</td>
                              <td width="10%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                              <td width="10%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">&nbsp;PRICE&nbsp;</td>
                              <td width="15%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">AMOUNT</td>
                              
                            </tr>
                                  <?PHP 
							  	$sql1 = "select 
								sum(tb1.dblQty)  as dblQty,
								tb1.dblGRNRate, 
								tb1.date 
								From(SELECT
											abs(sum(ware_stocktransactions_bulk.dblQty )) as dblQty, 
											date(ware_stocktransactions_bulk.dtDate) as date , 
											ware_stocktransactions_bulk.dblGRNRate 
										FROM ware_stocktransactions_bulk  
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType IN  ('Adjust Q+','Adjust R+','Adjust RQ+','Adjust Q-') AND 
											ware_stocktransactions_bulk.dblQty >0 AND 
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
											$paraDate
										  Group by ware_stocktransactions_bulk.dtDate,ware_stocktransactions_bulk.dblGRNRate) as tb1 
										  Group by tb1.date, tb1.dblGRNRate ";
								$result1 = $db->RunQuery($sql1);
								$i=0;
								while($row1=mysqli_fetch_array($result1))
								{
									$i++;
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid">&nbsp;</td>
                              <td class="normalfntMid"><?php echo $row1['date']; ?></td>
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid"><span class="normalfntRight"><?php echo $row1['dblQty']; ?>&nbsp;</span></td>
                              <td class="normalfntMid"><span class="normalfntRight"><?php echo $row1['dblGRNRate']; ?></span></td>
                              <td class="normalfntMid"><span class="normalfntRight"><?php echo round($row1['dblQty']*$row1['dblGRNRate'],4); ?></span></td>
                            </tr>
                            <?php
								}	
							?>
                            <?php
							if($i==0){
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid"></td>
                              <td>&nbsp;</td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                            </tr>
                            <?php
							}
							?>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        
                            <tr>
                              <td width="35%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">RETURN TO SUPP RCV</td>
                              <td width="10%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"> Date</td>
                              <td width="20%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">&nbsp;</td>
                              <td width="10%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                              <td width="10%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">&nbsp;PRICE&nbsp;</td>
                              <td width="15%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">AMOUNT</td>
                              
                            </tr>
                                  <?PHP 
							  	$sql1 = "select 
								tb1.intDocumentNo,
								tb1.intDocumntYear,
								 (tb1.dblQty)  as dblQty,
								 tb1.dblGRNRate,
								tb1.date 
								From(SELECT 
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear ,
											ware_stocktransactions_bulk.dblGRNRate,
											abs(sum(ware_stocktransactions_bulk.dblQty )) as dblQty, 
											date(ware_stocktransactions_bulk.dtDate) as date 
										FROM ware_stocktransactions_bulk  
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType IN  ('RTSUP_RCV') AND 
											ware_stocktransactions_bulk.dblQty >0 AND 
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
											$paraDate
										  group by ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear ) as tb1 
										  group by tb1.intDocumentNo,
											tb1.intDocumntYear";
								$result1 = $db->RunQuery($sql1);
								$i=0;
								while($row1=mysqli_fetch_array($result1))
								{
									$i++;
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                              <td class="normalfntMid"><?php echo $row1['date']; ?></td>
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid"><span class="normalfntRight"><?php echo $row1['dblQty']; ?>&nbsp;</span></td>
                              <td class="normalfntMid"><span class="normalfntRight"><?php echo $row1['dblGRNRate']; ?></span></td>
                              <td class="normalfntMid"><span class="normalfntRight"><?php echo round($row1['dblQty']*$row1['dblGRNRate'],4); ?></span></td>
                            </tr>
                            <?php
								}	
							?>
                            <?php
							if($i==0){
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid"></td>
                              <td>&nbsp;</td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                            </tr>
                            <?php
							}
							?>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        
                            <tr>
                              <td width="35%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">STORES TRANSFER+</td>
                              <td width="10%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"> Date</td>
                              <td width="20%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">&nbsp;</td>
                              <td width="10%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                              <td width="10%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">&nbsp;PRICE&nbsp;</td>
                              <td width="15%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">AMOUNT</td>
                              
                            </tr>
                                  <?PHP 
							  	$sql1 = "select  
								tb1.intDocumentNo,
								tb1.intDocumntYear,
								sum(tb1.dblQty)  as dblQty,
								tb1.dblGRNRate,
								tb1.date 
								From(SELECT 
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear,
											abs(sum(ware_stocktransactions_bulk.dblQty )) as dblQty, 
											ware_stocktransactions_bulk.dblGRNRate,
											date(ware_stocktransactions_bulk.dtDate) as date 
										FROM ware_stocktransactions_bulk  
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType IN  ('STORESTRANSFER') AND 
											ware_stocktransactions_bulk.dblQty >0 AND 
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
											$paraDate
										  Group by ware_stocktransactions_bulk.intDocumentNo,
								ware_stocktransactions_bulk.intDocumntYear,ware_stocktransactions_bulk.dblGRNRate) as tb1 
								Group by tb1.intDocumentNo,
								tb1.intDocumntYear,tb1.dblGRNRate";
								$result1 = $db->RunQuery($sql1);
								$i=0;
								while($row1=mysqli_fetch_array($result1))
								{
									$i++;
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                              <td class="normalfntMid"><?php echo $row1['date']; ?></td>
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid"><span class="normalfntRight"><?php echo $row1['dblQty']; ?>&nbsp;</span></td>
                              <td class="normalfntMid"><span class="normalfntRight"><?php echo $row1['dblGRNRate']; ?></span></td>
                              <td class="normalfntMid"><span class="normalfntRight"><?php echo round($row1['dblQty']*$row1['dblGRNRate'],4); ?></span></td>
                            </tr>
                            <?php
								}	
							?>
                            <?php
							if($i==0){
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid"></td>
                              <td>&nbsp;</td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                            </tr>
                            <?php
							}
							?>
                        </table>                        </td>
                        <td width="48%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  
                          <tr>
                            <td width="33%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;ISSUE NO</td>
                            <td width="11%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">Date</td>
                            <td width="2%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;</td>
                            <td width="18%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">ISSUE TO </td>
                            <td width="11%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                            <td width="11%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">&nbsp;PRICE&nbsp;</span></td>
                            <td width="14%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">AMOUNT</span></td>
                          </tr>
                                <?PHP 
								 $sql1 = "SELECT
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear,
											ware_stocktransactions_bulk.dblQty*-1 AS dblQty,
											ware_stocktransactions_bulk.dblGRNRate,
											date(ware_stocktransactions_bulk.dtDate) as date , 
											mst_department.strName
										FROM ware_stocktransactions_bulk
										inner join ware_issueheader on ware_issueheader.intIssueNo=ware_stocktransactions_bulk.intDocumentNo and ware_issueheader.intIssueYear=ware_stocktransactions_bulk.intDocumntYear
										inner join mst_department on mst_department.intId=ware_issueheader.intDepartment
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType =  'ISSUE' AND
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
											$paraDate
										";
								$result1 = $db->RunQuery($sql1);
								$i=0;
								while($row1=mysqli_fetch_array($result1))
								{
									$i++;
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                              <td class="normalfntMid"><?php echo $row1['date']; ?></td>
                              <td class="normalfntMid">&nbsp;</td>
                              <td class="normalfntMid"><?php echo $row1['strName'] ?></td>
                              <td class="normalfntRight"><?php echo $row1['dblQty']; ?>&nbsp;</td>
                              <td class="normalfntRight"><?php echo $row1['dblGRNRate']; ?></td>
                              <td class="normalfntRight"><?php echo round($row1['dblQty']*$row1['dblGRNRate'],4); ?></td>
                            </tr>
                            <?php
								}	
							?>
                            <?php
							if($i==0){
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid"></td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                            </tr>
                            <?php
							}
							?>
                        </table>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
              
                            <tr>
                              <td width="33%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;RETURN SUP NO</td>
                           	 <td width="11%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">Date</td>
                              <td width="2%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;</td>
                              <td width="18%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">SUPPLIER</td>
                              <td width="11%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                              <td width="11%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">&nbsp;PRICE&nbsp;</span></td>
                              <td width="13%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">AMOUNT</span></td>
                            </tr>
                                  <?PHP 
								$sql1 = "SELECT
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear,
											ware_stocktransactions_bulk.dblQty * -1 as dblQty, 
											ware_stocktransactions_bulk.dblGRNRate,
											date(ware_stocktransactions_bulk.dtDate) as date 
										FROM ware_stocktransactions_bulk
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType =  'RTSUP' AND
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
											$paraDate
										";
								$result1 = $db->RunQuery($sql1);
								$i=0;
								while($row1=mysqli_fetch_array($result1))
								{
									$i++;
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                              <td class="normalfntMid"><?php echo $row1['date']; ?></td>
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid"><?php 
							  	  //get supplier name
							  $sql_sup = "	SELECT
											mst_supplier.strName
											FROM
											ware_returntosupplierheader
											Inner Join ware_grnheader ON ware_grnheader.intGrnNo = ware_returntosupplierheader.intGrnNo AND ware_grnheader.intGrnYear = ware_returntosupplierheader.intGrnYear
											Inner Join trn_poheader ON trn_poheader.intPONo = ware_grnheader.intPoNo AND trn_poheader.intPOYear = ware_grnheader.intPoYear
											Inner Join mst_supplier ON mst_supplier.intId = trn_poheader.intSupplier
											WHERE
											ware_returntosupplierheader.intReturnYear =  '".$row1['intDocumntYear']."' AND
											ware_returntosupplierheader.intReturnNo =  '".$row1['intDocumentNo']."'
											";
								$result_sup = $db->RunQuery($sql_sup);
								$row_sup	= mysqli_fetch_array($result_sup);
							  	echo $row_sup['strName']; 
							  
							   ?></td>
                              <td class="normalfntRight"><?php echo $row1['dblQty']; ?>&nbsp;</td>
                              <td class="normalfntRight"><?php echo $row1['dblGRNRate']; ?></td>
                              <td class="normalfntRight"><?php echo round($row1['dblQty']*$row1['dblGRNRate'],4); ?></td>
                            </tr>
                            <?php
								}	
							?>
                            <?php
							if($i==0){
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid"></td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td width="1%" class="normalfntRight"></td>
                            </tr>
                            <?php
							}
							?>
                        </table>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                     
                            <tr>
                              <td width="33%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;GATEPASS NO</td>
                            <td width="11%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">Date</td>
                              <td width="2%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;</td>
                              <td width="18%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">TO LOCATION</td>
                              <td width="11%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                              <td width="11%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">&nbsp;PRICE&nbsp;</span></td>
                              <td width="14%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">AMOUNT</span></td>
                            </tr>
                                  <?PHP 
								$sql1 = "SELECT
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear,
											ware_stocktransactions_bulk.dblQty * -1 as dblQty, 
											ware_stocktransactions_bulk.dblGRNRate,
											date(ware_stocktransactions_bulk.dtDate) as date, 
											mst_locations.strName as location 
										FROM ware_stocktransactions_bulk 
										Inner Join ware_gatepassheader ON ware_stocktransactions_bulk.intDocumentNo = ware_gatepassheader.intGatePassNo AND ware_stocktransactions_bulk.intDocumntYear = ware_gatepassheader.intGatePassYear
										Inner Join mst_locations ON ware_gatepassheader.intGPToLocation = mst_locations.intId
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType =  'GATEPASS' AND
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
											$paraDate
										";
								$result1 = $db->RunQuery($sql1);
								$i=0;
								while($row1=mysqli_fetch_array($result1))
								{
									$i++;
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                              <td class="normalfntMid"><?php echo $row1['date']; ?></td>
                              <td class="normalfntMid">&nbsp;</td>
                              <td class="normalfntMid"><?php echo $row1['location']; ?></td>
                              <td class="normalfntRight"><?php echo $row1['dblQty']; ?>&nbsp;</td>
                              <td class="normalfntRight"><?php echo $row1['dblGRNRate']; ?></td>
                              <td class="normalfntRight"><?php echo round($row1['dblQty']*$row1['dblGRNRate'],4); ?></td>
                            </tr>
                            <?php
								}	
							?>
                            <?php
							if($i==0){
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid"></td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td width="11%" class="normalfntRight"></td>
                              <td width="11%" class="normalfntRight"></td>
                              <td width="14%" class="normalfntRight"></td>
                            </tr>
                            <?php
							}
							?>
                        </table><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        
                            <tr>
                              <td width="33%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;ALLOCATION NO</td>
                            <td width="11%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">Date</td>
                              <td width="2%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;</td>
                              <td width="18%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">STYLE NO</td>
                              <td width="11%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                              <td width="11%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">&nbsp;PRICE&nbsp;</span></td>
                              <td width="14%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">AMOUNT</span></td>
                            </tr>
                                  <?PHP 
								  $sql1 = "SELECT
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear,
											sum(ware_stocktransactions_bulk.dblQty * -1) as dblQty, 
											ware_stocktransactions_bulk.dblGRNRate,
											date(ware_stocktransactions_bulk.dtDate) as date , 
											trn_orderdetails.strStyleNo 
										FROM ware_stocktransactions_bulk 
											left Join trn_orderdetails ON ware_stocktransactions_bulk.intOrderNo = trn_orderdetails.intOrderNo AND ware_stocktransactions_bulk.intOrderYear = trn_orderdetails.intOrderYear AND ware_stocktransactions_bulk.intSalesOrderId = trn_orderdetails.intSalesOrderId
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType =  'ALLOCATE' AND
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location' 	
											$paraDate
											group by ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear ,
											ware_stocktransactions_bulk.dblGRNRate 
										";
								$result1 = $db->RunQuery($sql1);
								$i=0;
								while($row1=mysqli_fetch_array($result1))
								{
									$i++;
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                              <td class="normalfntMid"><?php echo $row1['date']; ?></td>
                              <td class="normalfntMid">&nbsp;</td>
                              <td class="normalfntMid"><?php echo $row1['strStyleNo']; ?></td>
                              <td class="normalfntRight"><?php echo $row1['dblQty']; ?>&nbsp;</td>
                              <td class="normalfntRight"><?php echo $row1['dblGRNRate']; ?></td>
                              <td class="normalfntRight"><?php echo round($row1['dblQty']*$row1['dblGRNRate'],4); ?></td>
                            </tr>
                            <?php
								}	
							?>
                            <?php
							if($i==0){
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid"></td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td width="11%" class="normalfntRight"></td>
                              <td width="11%" class="normalfntRight"></td>
                              <td width="14%" class="normalfntRight"></td>
                            </tr>
                            <?php
							}
							?>
                        </table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
                        
                            <tr>
                              <td width="33%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;INVOICE NO</td>
                            <td width="11%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">Date</td>
                              <td width="2%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;</td>
                              <td width="18%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">INVOICE TO</td>
                              <td width="11%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                              <td width="11%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">&nbsp;PRICE&nbsp;</span></td>
                              <td width="14%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">AMOUNT</span></td>
                            </tr>
                                  <?PHP 
							  	$sql1 = "SELECT
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear,
											ware_stocktransactions_bulk.dblQty * -1 as dblQty, 
											ware_stocktransactions_bulk.dblGRNRate,
											date(ware_stocktransactions_bulk.dtDate) as date , 
											mst_locations.strName AS location 
										FROM ware_stocktransactions_bulk  
										Inner Join ware_invoiceheader ON ware_stocktransactions_bulk.intDocumentNo = ware_invoiceheader.intInvoiceNo AND ware_stocktransactions_bulk.intDocumntYear = ware_invoiceheader.intInvoiceYear
											Inner Join trn_poheader ON ware_invoiceheader.intPONo = trn_poheader.intPONo AND ware_invoiceheader.intPOYear = trn_poheader.intPOYear
											Inner Join mst_locations ON trn_poheader.intDeliveryTo = mst_locations.intId
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType =  'INVOICE' AND
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
											$paraDate
										";
								$result1 = $db->RunQuery($sql1);
								$i=0;
								while($row1=mysqli_fetch_array($result1))
								{
									$i++;
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                              <td class="normalfntMid"><?php echo $row1['date']; ?></td>
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid"><?php echo $row1['location']; ?></td>
                              <td class="normalfntRight"><?php echo $row1['dblQty']; ?>&nbsp;</td>
                              <td class="normalfntRight"><?php echo $row1['dblGRNRate']; ?></td>
                              <td class="normalfntRight"><?php echo round($row1['dblQty']*$row1['dblGRNRate'],4); ?></td>
                            </tr>
                            <?php
								}	
							?>
                            <?php
							if($i==0){
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid"></td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td width="11%" class="normalfntRight"></td>
                              <td width="11%" class="normalfntRight"></td>
                              <td width="14%" class="normalfntRight"></td>
                            </tr>
                            <?php
							}
							?>
                        </table>  
<table width="100%" border="0" cellspacing="0" cellpadding="0">
                        
                            <tr>
                              <td width="33%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;STOCK ADJUSTMENTS -</td>
                            <td width="11%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">Date</td>
                              <td width="2%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;</td>
                              <td width="18%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">&nbsp;</td>
                              <td width="11%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                              <td width="11%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">&nbsp;PRICE&nbsp;</span></td>
                              <td width="14%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">AMOUNT</span></td>
                            </tr>
                                  <?PHP 
							  	$sql1 = "select 
								sum(tb1.dblQty)  as dblQty,
								tb1.dblGRNRate, 
								tb1.date 
								From(SELECT 
											ware_stocktransactions_bulk.dblGRNRate, 
											abs(sum(ware_stocktransactions_bulk.dblQty )) as dblQty, 
											date(ware_stocktransactions_bulk.dtDate) as date 
										FROM ware_stocktransactions_bulk  
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType IN  ('Adjust Q+','Adjust R+','Adjust RQ+','Adjust Q-') AND 
											ware_stocktransactions_bulk.dblQty <0 AND 
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
											$paraDate
										  Group by ware_stocktransactions_bulk.dtDate, ware_stocktransactions_bulk.dblGRNRate) as tb1 
										  Group by tb1.date,tb1.dblGRNRate";
								$result1 = $db->RunQuery($sql1);
								$i=0;
								while($row1=mysqli_fetch_array($result1))
								{
									$i++;
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid">&nbsp;</td>
                              <td class="normalfntMid"><?php echo $row1['date']; ?></td>
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid">&nbsp;</td>
                              <td class="normalfntRight"><?php echo $row1['dblQty']; ?>&nbsp;</td>
                              <td class="normalfntRight"><?php echo $row1['dblGRNRate']; ?></td>
                              <td class="normalfntRight"><?php echo round($row1['dblQty']*$row1['dblGRNRate'],4); ?></td>
                            </tr>
                            <?php
								}	
							?>
                            <?php
							if($i==0){
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid"></td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td width="11%" class="normalfntRight"></td>
                              <td width="11%" class="normalfntRight"></td>
                              <td width="14%" class="normalfntRight"></td>
                            </tr>
                            <?php
							}
							?>
                        </table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
                        
                            <tr>
                              <td width="33%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;ITEM CREATION NO -</td>
                            <td width="11%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">Date</td>
                              <td width="2%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;</td>
                              <td width="18%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">&nbsp;</td>
                              <td width="11%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                              <td width="11%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">&nbsp;PRICE&nbsp;</span></td>
                              <td width="14%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">AMOUNT</span></td>
                          </tr>
                                  <?php 
							  	  $sql2 = "SELECT 
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear,
											abs(ware_stocktransactions_bulk.dblQty ) as dblQty, 
											ware_stocktransactions_bulk.dblGRNRate,
											date(ware_stocktransactions_bulk.dtDate) as date 
										FROM ware_stocktransactions_bulk  
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType =  'ITEMCREATION-' AND 
											ware_stocktransactions_bulk.dblQty <0 AND 
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
											$paraDate
										  ";
								$result2 = $db->RunQuery($sql2);
								$i=0;
								while($row1=mysqli_fetch_array($result2))
								{
									//echo $row1['intDocumntYear'];
									$i++;
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                              <td class="normalfntMid"><?php echo $row1['date']; ?></td>
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid"></td>
                              <td class="normalfntRight"><?php echo $row1['dblQty']; ?>&nbsp;</td>
                              <td class="normalfntRight"><?php echo $row1['dblGRNRate']; ?></td>
                              <td class="normalfntRight"><?php echo round($row1['dblQty']*$row1['dblGRNRate'],4); ?></td>
                            </tr>
                            <?php
								}	
							?>
                            <?php
							if($i==0){
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid"></td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td width="11%" class="normalfntRight"></td>
                              <td width="11%" class="normalfntRight"></td>
                              <td width="14%" class="normalfntRight"></td>
                            </tr>
                            <?php
							}
							?>
                      </table> 
<table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="33%" style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;CLOSE STOCK</td>
                           	 <td width="11%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">Date</td>
                              <td width="2%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;</td>
                              <td width="18%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">&nbsp;</td>
                              <td width="11%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                              <td width="11%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">&nbsp;PRICE&nbsp;</span></td>
                              <td width="14%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">AMOUNT</span></td>
                            </tr>
                            <?PHP 
								$sql1 = "SELECT
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear,
											ware_stocktransactions_bulk.dblQty*(-1) AS dblQty, 
											ware_stocktransactions_bulk.dblGRNRate,
											date(ware_stocktransactions_bulk.dtDate) as date 
										FROM ware_stocktransactions_bulk
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType LIKE  '%CLOSESTOCK%' AND
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
											$paraDate
										";
										$i=0;
								$result1 = $db->RunQuery($sql1);
								while($row1=mysqli_fetch_array($result1))
								{
									$i++;
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                              <td class="normalfntMid"><?php echo $row1['date']; ?></td>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              <td class="normalfntRight"><?php echo $row1['dblQty']; ?>&nbsp;</td>
                              <td class="normalfntRight"><?php echo $row1['dblGRNRate']; ?></td>
                              <td class="normalfntRight"><?php echo round($row1['dblQty']*$row1['dblGRNRate'],4); ?></td>
                            </tr>
                            <?php
								}	
							?>
                            <?php
							if($i==0){
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid"></td>
                              <td>&nbsp;</td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                            </tr>
                            <?php
							}
							?>
                          </table>  
<table width="100%" border="0" cellspacing="0" cellpadding="0">
                        
                            <tr>
                              <td width="33%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">STORES TRANSFER-</td>
                            <td width="11%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">Date</td>
                              <td width="2%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;</td>
                              <td width="18%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">&nbsp;</td>
                              <td width="11%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                              <td width="11%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">&nbsp;PRICE&nbsp;</span></td>
                              <td width="14%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">AMOUNT</span></td>
                              
                            </tr>
                                  <?PHP 
							  	$sql1 = "select 
								tb1.intDocumentNo,
								tb1.intDocumntYear,
								sum(tb1.dblQty)  as dblQty,
								tb1.dblGRNRate, 
								tb1.date 
								From(SELECT 
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear, 
											abs(sum(ware_stocktransactions_bulk.dblQty*(-1) )) as dblQty, 
											ware_stocktransactions_bulk.dblGRNRate, 
											date(ware_stocktransactions_bulk.dtDate) as date 
										FROM ware_stocktransactions_bulk  
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType IN  ('STORESTRANSFER') AND 
											ware_stocktransactions_bulk.dblQty <0 AND 
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
											$paraDate
										  Group by ware_stocktransactions_bulk.intDocumentNo,
								ware_stocktransactions_bulk.intDocumntYear,ware_stocktransactions_bulk.dblGRNRate) as tb1 
										  Group by tb1.intDocumentNo,
										tb1.intDocumntYear, tb1.dblGRNRate";
								$result1 = $db->RunQuery($sql1);
								$i=0;
								while($row1=mysqli_fetch_array($result1))
								{
									$i++;
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                              <td class="normalfntMid"><?php echo $row1['date']; ?></td>
                              <td class="normalfntMid"></td>
                              <td>&nbsp;</td>
                              <td class="normalfntMid"><span class="normalfntRight"><?php echo $row1['dblQty']; ?>&nbsp;</span></td>
                              <td class="normalfntMid"><span class="normalfntRight"><?php echo $row1['dblGRNRate']; ?></span></td>
                              <td class="normalfntMid"><span class="normalfntRight"><?php echo round($row1['dblQty']*$row1['dblGRNRate'],4); ?></span></td>
                            </tr>
                            <?php
								}	
							?>
                            <?php
							if($i==0){
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid"></td>
                              <td>&nbsp;</td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                            </tr>
                            <?php
							}
							?>
                        </table> 
<table width="100%" border="0" cellspacing="0" cellpadding="0">
                        
                            <tr>
                              <td width="33%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">DISPOSAL</td>
                            <td width="11%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">Date</td>
                              <td width="2%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;</td>
                              <td width="18%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">&nbsp;</td>
                              <td width="11%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                              <td width="11%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">&nbsp;PRICE&nbsp;</span></td>
                              <td width="14%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">AMOUNT</span></td>
                              
                            </tr>
                                  <?PHP 
							  	$sql1 = "select 
								tb1.intDocumentNo,
								tb1.intDocumntYear,
								sum(tb1.dblQty)  as dblQty,
								tb1.dblGRNRate, 
								tb1.date 
								From(SELECT 
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear, 
											abs(sum(ware_stocktransactions_bulk.dblQty*(-1) )) as dblQty, 
											ware_stocktransactions_bulk.dblGRNRate,
											date(ware_stocktransactions_bulk.dtDate) as date 
										FROM ware_stocktransactions_bulk  
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType IN  ('Dispose') AND 
											ware_stocktransactions_bulk.dblQty <0 AND 
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
											$paraDate
										  Group by ware_stocktransactions_bulk.intDocumentNo,
								ware_stocktransactions_bulk.intDocumntYear,ware_stocktransactions_bulk.dblGRNRate) as tb1 
										  Group by tb1.intDocumentNo,
										tb1.intDocumntYear,tb1.dblGRNRate";
								$result1 = $db->RunQuery($sql1);
								$i=0;
								while($row1=mysqli_fetch_array($result1))
								{
									$i++;
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                              <td class="normalfntMid"><?php echo $row1['date']; ?></td>
                              <td class="normalfntMid"></td>
                              <td>&nbsp;</td>
                              <td class="normalfntMid"><span class="normalfntRight"><?php echo $row1['dblQty']; ?>&nbsp;</span></td>
                              <td class="normalfntMid"><span class="normalfntRight"><?php echo $row1['dblGRNRate']; ?></span></td>
                              <td class="normalfntMid"><span class="normalfntRight"><?php echo round($row1['dblQty']*$row1['dblGRNRate'],4); ?></span></td>
                            </tr>
                            <?php
								}	
							?>
                            <?php
							if($i==0){
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid"></td>
                              <td>&nbsp;</td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                            </tr>
                            <?php
							}
							?>
                        </table></tr>
                    </table>
                </td>
            </tr>
            <tr >
              <td colspan="9" class="normalfntMid"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="52%">&nbsp;</td>
                  <td width="11%" bgcolor="#E9E6E0">Stock Balance </td>
                  <?PHP 
								 $sql1 = "SELECT
											round(sum(ware_stocktransactions_bulk.dblQty),8) as balQty,
											round(sum(ware_stocktransactions_bulk.dblQty*dblGRNRate),8) as balAmntQty 
										FROM ware_stocktransactions_bulk
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
											$paraDate
										";
								$result1 = $db->RunQuery($sql1);
								$row1=mysqli_fetch_array($result1);
				?>
                  <td width="25%" bgcolor="#E9E6E0" style="font-size:14px;text-align:right;color:#06F"><?php echo round($row1['balQty'],4); ?></td>
                  <td width="5%" bgcolor="#E9E6E0" style="font-size:14px;text-align:right;color:#06F"><?php echo round($row1['balAmntQty']/$row1['balQty'],4); ?></td>
                  <td width="7%" bgcolor="#E9E6E0" style="font-size:14px;text-align:right;color:#06F"><?php echo round($row1['balAmntQty'],4); ?></td>
                </tr>
              </table></td>
            </tr>
            <tr >
              <td colspan="9" bgcolor="#CCCCCC" class="normalfntMid"></td>
            </tr>
    <?php
			}
			if($grnNo=='')
			{
				echo "<tr><td align=\"middle\" colspan=\"6\">&nbsp;NO RECORD FOUND</td></tr>";	
			}
	?>

    		<tr >
              <td colspan="9" class="normalfntMid"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="52%">&nbsp;</td>
                  <td width="48%">&nbsp;</td>
                </tr>
              </table></td>
            </tr>
  </table>
 <table width="1132"  cellspacing="1" cellpadding="0"  >

      <?PHP
	  if($fromDate!='')
		 {
			$datePara = " AND  MAINSTOCK.dtDate>='$fromDate' ";	 
			$datePara2 = " AND  ware_stocktransactions_bulk.dtDate>='$fromDate'";	 
		 }
	  if($toDate!='')
		 {
			$datePara = " AND MAINSTOCK.dtDate<='$toDate' ";	 
			$datePara2 = " AND ware_stocktransactions_bulk.dtDate<='$toDate' ";	 
		 }
		 
	  	 $sql = "SELECT
					MAINSTOCK.intItemId,
					Sum(MAINSTOCK.dblQty) AS grnQty 
					FROM
					ware_stocktransactions_bulk as MAINSTOCK
				
					WHERE
					(MAINSTOCK.strType =  'ITEMCREATION+')  AND
					MAINSTOCK.intLocationId =  '$location' AND
					MAINSTOCK.intItemId =  '$item' 
					
					$datePara
					
					GROUP BY
					MAINSTOCK.strType,
					MAINSTOCK.intLocationId,
					MAINSTOCK.intItemId 
					";
				// echo $sql;
					
			$result = $db->RunQuery($sql);
			$i=0;//RTSUP
			while($row=mysqli_fetch_array($result))
			{
				$grnNo 		= 0;
				$grnYear	= 0;
				$itemNo 	= $item;
				 
	  ?>
      		    <tr>
      <td height="20" colspan="6" align="center" bgcolor="#469EE6" style="color:#FFF;font-size:11px"><strong>NEW ITEM ALLOCATIONS</strong></td>
      </tr>
            <tr bgcolor="#FFE1C4" >
              <td width="104" class="normalfntMid">GRN NO :0</td>
              <td width="140" class="normalfntMid"><?php //	echo $row['intPOYear'].'/'.$row['intPONo']; ?></td>
              <td width="370" class="normalfntMid">&nbsp;</td>
              <td width="148" class="normalfntMid">&nbsp;</td>
              <td width="119" class="normalfntMid">&nbsp;</td>
              <td width="168" class="normalfntMid" style="font-size:14px;color:#C63"><b></b></td>
            </tr>
            <tr>
            	<td colspan="6">
                	<table width="100%">
                    <tr>
                    	<td width="52%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        
                            <tr>
                              <td width="29%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt"><span class="normalfnt" style="color:#666">&nbsp;ITEM CREATION NO</span> +</td>
                            <td width="23%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">Date</td>
                              <td width="23%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">&nbsp;</td>
                              <td width="25%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                              <td width="25%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">&nbsp;PRICE&nbsp;</span></td>
                              <td width="25%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">AMOUNT</span></td>
                          </tr>
                                  <?PHP 
							    	$sql1 = "SELECT 
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear,
											ware_stocktransactions_bulk.intGRNNo,
											ware_stocktransactions_bulk.intGRNYear,
											ware_stocktransactions_bulk.dblGRNRate,
											sum(ware_stocktransactions_bulk.dblQty ) as dblQty, 
											date(ware_stocktransactions_bulk.dtDate) as date 
										FROM ware_stocktransactions_bulk  
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType =  'ITEMCREATION+' AND 
											ware_stocktransactions_bulk.dblQty >0 AND 
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
											$datePara2 
											group by 
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear,
											ware_stocktransactions_bulk.dblGRNRate 
											";
								$result1 = $db->RunQuery($sql1);
								$m=0;
								while($row1=mysqli_fetch_array($result1))
								{
									//$grnNo 		= $row['intGRNNo'];
									//$grnYear	= $row['intGRNYear'];
									$m++;
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                              <td class="normalfntMid"><?php echo $row1['date']; ?></td>
                              <td class="normalfntMid"></td>
                              <td class="normalfntRight"><span class="normalfntRight"><?php echo $row1['dblQty']; ?>&nbsp;</span></td>
                              <td class="normalfntRight"><?php echo $row1['dblGRNRate']; ?></td>
                              <td class="normalfntRight"><?php echo round($row1['dblQty']*$row1['dblGRNRate'],4); ?></td>
                            </tr>
                            <?php
								}	
							?>
                    <?php
							if($m==0){
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid"></td>
                              <td>&nbsp;</td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                            </tr>
                            <?php
							}
							?>
                      </table> 
                      <?php
					  if($m>0){
					  ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="32%" style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;RETURN STORES NO</td>
                              <td width="18%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"> Date</td>
                              <td width="26%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">&nbsp;</td>
                              <td width="24%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                              <td width="24%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">&nbsp;PRICE&nbsp;</span></td>
                              <td width="24%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">AMOUNT</span></td>
                            </tr>
                            <?PHP 
								$sql1 = "SELECT
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear,
											ware_stocktransactions_bulk.dblQty, 
											ware_stocktransactions_bulk.dblGRNRate,
											date(ware_stocktransactions_bulk.dtDate) as date 
										FROM ware_stocktransactions_bulk
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType =  'RETSTORES' AND
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
											$paraDate
										";
										$i=0;
								$result1 = $db->RunQuery($sql1);
								while($row1=mysqli_fetch_array($result1))
								{
									$i++;
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                              <td class="normalfntMid"><?php echo $row1['date']; ?></td>
                              <td>&nbsp;</td>
                              <td class="normalfntRight"><?php echo $row1['dblQty']; ?>&nbsp;</td>
                              <td class="normalfntRight"><?php echo $row1['dblGRNRate']; ?>&nbsp;</td>
                              <td class="normalfntRight"><?php echo round($row1['dblQty']*$row1['dblGRNRate'],4); ?></td>
                            </tr>
                            <?php
								}	
							?>
                            <?php
							if($i==0){
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid"></td>
                              <td>&nbsp;</td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                            </tr>
                            <?php
							}
							?>
                          </table>                      
                      
                      
                      
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="32%" style="color:#666" height="16" bgcolor="#EBFAC7" class="normalfnt">&nbsp;TRANSFER IN NO</td>
                          <td width="18%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"> Date</td>
                          <td width="26%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">FROM COMPANY</td>
                          <td width="24%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                          <td width="24%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">&nbsp;PRICE&nbsp;</span></td>
                          <td width="24%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">AMOUNT</span></td>
                        </tr>
                        <?PHP 
							 	$sql1 = "SELECT
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear,
											ware_stocktransactions_bulk.dblQty, 
											ware_stocktransactions_bulk.dblGRNRate,
											date(ware_stocktransactions_bulk.dtDate) as date , 
											mst_locations.strName as location 
										FROM ware_stocktransactions_bulk 
										Inner Join ware_gatepasstransferinheader ON ware_stocktransactions_bulk.intDocumentNo = ware_gatepasstransferinheader.intGpTransfNo AND ware_stocktransactions_bulk.intDocumntYear = ware_gatepasstransferinheader.intGpTransfYear
										Inner Join ware_gatepassheader ON ware_gatepasstransferinheader.intGatePassNo = ware_gatepassheader.intGatePassNo AND ware_gatepasstransferinheader.intGatePassYear = ware_gatepassheader.intGatePassYear
										Inner Join mst_locations ON ware_gatepassheader.intCompanyId = mst_locations.intId
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType =  'TRANSFERIN' AND
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
											$paraDate
										";
								$result1 = $db->RunQuery($sql1);
								$i=0;
								while($row1=mysqli_fetch_array($result1))
								{
									$i++;
							?>
                        <tr class="normalfnt">
                          <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                          <td class="normalfntMid"><?php echo $row1['date']; ?></td>
                          <td class="normalfntMid"><?php echo $row1['location']; ?></td>
                          <td class="normalfntRight"><?php echo $row1['dblQty']; ?>&nbsp;</td>
                          <td class="normalfntRight"><?php echo $row1['dblGRNRate']; ?>11</td>
                          <td class="normalfntRight"><?php echo round($row1['dblQty']*$row1['dblGRNRate'],4); ?></td>
                        </tr>
                        <?php
								}	
							?>
                        <?php
							if($i==0){
							?>
                        <tr class="normalfnt">
                          <td class="normalfntMid"></td>
                          <td class="normalfntMid"></td>
                          <td>&nbsp;</td>
                          <td class="normalfntRight"></td>
                          <td class="normalfntRight"></td>
                          <td class="normalfntRight"></td>
                        </tr>
                        <?php
							}
							?>
                      </table>
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="32%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;UN-ALLOCATION NO</td>
                          <td width="18%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"> Date</td>
                          <td width="26%"  style="color:#666" bgcolor="#EBFAC7">&nbsp;</td>
                          <td width="24%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">QTY</span></td>
                          <td width="24%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">&nbsp;PRICE&nbsp;</span></td>
                          <td width="24%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">AMOUNT</span></td>
                        </tr>
                        <?PHP 
								$sql1 = "SELECT
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear,
											ware_stocktransactions_bulk.dblGRNRate,
											round(sum(ware_stocktransactions_bulk.dblQty),2) as dblQty, 
											date(ware_stocktransactions_bulk.dtDate) as date 
										FROM ware_stocktransactions_bulk
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType =  'UNALLOCATE' AND
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location' 	
											$paraDate										group by ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear, ware_stocktransactions_bulk.dblGRNRate
										";
								$result1 = $db->RunQuery($sql1);
								$i=0;
								while($row1=mysqli_fetch_array($result1))
								{
									$i++;
							?>
                        <tr class="normalfnt">
                          <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                          <td class="normalfntMid"><?php echo $row1['date']; ?></td>
                          <td>&nbsp;</td>
                          <td class="normalfntRight"><?php echo $row1['dblQty']; ?>&nbsp;</td>
                          <td class="normalfntRight"><?php echo $row1['dblGRNRate']; ?></td>
                          <td class="normalfntRight"><?php echo round($row1['dblQty']*$row1['dblGRNRate'],4); ?></td>
                        </tr>
                        <?php
								}	
							?>
                        <?php
							if($i==0){
							?>
                        <tr class="normalfnt">
                          <td class="normalfntMid"></td>
                          <td class="normalfntMid"></td>
                          <td>&nbsp;</td>
                          <td class="normalfntRight"></td>
                          <td class="normalfntRight"></td>
                          <td class="normalfntRight"></td>
                        </tr>
                        <?php
							}
							?>
                      </table>
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td width="32%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;STOCK ADJUSTMENTS +</td>
                          <td width="18%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">Date</td>
                          <td width="26%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">&nbsp;</td>
                          <td width="24%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                          <td width="24%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">&nbsp;PRICE&nbsp;</span></td>
                          <td width="24%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">AMOUNT</span></td>
                        </tr>
                        <?PHP 
							  	$sql1 = "select 
								sum(tb1.dblQty)  as dblQty,
								tb1.dblGRNRate,
								tb1.date 
								From(SELECT
											ware_stocktransactions_bulk.dblGRNRate,
											abs(sum(ware_stocktransactions_bulk.dblQty )) as dblQty, 
											date(ware_stocktransactions_bulk.dtDate) as date 
										FROM ware_stocktransactions_bulk  
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType IN  ('Adjust Q+','Adjust R+','Adjust RQ+','Adjust Q-') AND 
											ware_stocktransactions_bulk.dblQty >0 AND 
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
											$paraDate
										  Group by ware_stocktransactions_bulk.dtDate,ware_stocktransactions_bulk.dblGRNRate) as tb1 
										  Group by tb1.date, tb1.dblGRNRate";
								$result1 = $db->RunQuery($sql1);
								$i=0;
								while($row1=mysqli_fetch_array($result1))
								{
									$i++;
							?>
                        <tr class="normalfnt">
                          <td class="normalfntMid">&nbsp;</td>
                          <td class="normalfntMid"><?php echo $row1['date']; ?></td>
                          <td class="normalfntMid"></td>
                          <td class="normalfntMid"><span class="normalfntRight"><?php echo $row1['dblQty']; ?>&nbsp;</span></td>
                          <td class="normalfntMid"><span class="normalfntRight"><?php echo $row1['dblGRNRate']; ?></span></td>
                          <td class="normalfntRight"><?php echo round($row1['dblQty']*$row1['dblGRNRate'],4); ?></td>
                        </tr>
                        <?php
								}	
							?>
                        <?php
							if($i==0){
							?>
                        <tr class="normalfnt">
                          <td class="normalfntMid"></td>
                          <td class="normalfntMid"></td>
                          <td>&nbsp;</td>
                          <td class="normalfntRight"></td>
                          <td class="normalfntRight"></td>
                          <td class="normalfntRight"></td>
                        </tr>
                        <?php
							}
							?>
                      </table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
                        
                            <tr>
                              <td width="32%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">RETURN TO SUPP RCV</td>
                            <td width="18%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">Date</td>
                              <td width="26%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">&nbsp;</td>
                              <td width="24%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                              <td width="24%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">&nbsp;PRICE&nbsp;</span></td>
                              <td width="24%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">AMOUNT</span></td>
                              
                            </tr>
                                  <?PHP 
							  	$sql1 = "select 
								tb1.intDocumentNo,
								tb1.intDocumntYear,
								sum(tb1.dblQty)  as dblQty,
								tb1.dblGRNRate, 
								tb1.date 
								From(SELECT 
								ware_stocktransactions_bulk.intDocumentNo,
								ware_stocktransactions_bulk.intDocumntYear,
											abs(sum(ware_stocktransactions_bulk.dblQty )) as dblQty, 
											ware_stocktransactions_bulk.dblGRNRate, 
											date(ware_stocktransactions_bulk.dtDate) as date 
										FROM ware_stocktransactions_bulk  
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType IN  ('RTSUP_RCV') AND 
											ware_stocktransactions_bulk.dblQty >0 AND 
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
											$paraDate
										  Group by ware_stocktransactions_bulk.dtDate, ware_stocktransactions_bulk.dblGRNRate) as tb1 
										  Group by tb1.date, tb1.dblGRNRate";
								$result1 = $db->RunQuery($sql1);
								$i=0;
								while($row1=mysqli_fetch_array($result1))
								{
									$i++;
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                              <td class="normalfntMid"><?php echo $row1['date']; ?></td>
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid"><span class="normalfntRight"><?php echo $row1['dblQty']; ?>&nbsp;</span></td>
                              <td class="normalfntMid"><span class="normalfntRight"><?php echo $row1['dblGRNRate']; ?></span></td>
                              <td class="normalfntMid"><span class="normalfntRight"><?php echo round($row1['dblQty']*$row1['dblGRNRate'],4); ?></span></td>
                            </tr>
                            <?php
								}	
							?>
                            <?php
							if($i==0){
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid"></td>
                              <td>&nbsp;</td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                            </tr>
                            <?php
							}
							?>
                        </table>      
<table width="100%" border="0" cellspacing="0" cellpadding="0">
                        
                            <tr>
                              <td width="32%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">STORES TRANSFER+</td>
                            <td width="18%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">Date</td>
                              <td width="26%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">&nbsp;</td>
                              <td width="24%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                              <td width="24%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">&nbsp;PRICE&nbsp;</span></td>
                              <td width="24%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">AMOUNT</span></td>
                              
                            </tr>
                                  <?PHP 
							  	$sql1 = "select  
								tb1.intDocumentNo,
								tb1.intDocumntYear,
								tb1.dblGRNRate,
								sum(tb1.dblQty)  as dblQty,
								tb1.date 
								From(SELECT 
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear,
											abs(sum(ware_stocktransactions_bulk.dblQty )) as dblQty, 
											ware_stocktransactions_bulk.dblGRNRate , 
											date(ware_stocktransactions_bulk.dtDate) as date 
										FROM ware_stocktransactions_bulk  
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType IN  ('STORESTRANSFER') AND 
											ware_stocktransactions_bulk.dblQty >0 AND 
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
											$paraDate
										  Group by ware_stocktransactions_bulk.dtDate,ware_stocktransactions_bulk.dblGRNRate) as tb1 
										  Group by tb1.date,tb1.dblGRNRate ";
								$result1 = $db->RunQuery($sql1);
								$i=0;
								while($row1=mysqli_fetch_array($result1))
								{
									$i++;
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                              <td class="normalfntMid"><?php echo $row1['date']; ?></td>
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid"><span class="normalfntRight"><?php echo $row1['dblQty']; ?>&nbsp;</span></td>
                              <td class="normalfntMid"><span class="normalfntRight"><?php echo $row1['dblGRNRate']; ?></span></td>
                              <td class="normalfntMid"><span class="normalfntRight"><?php echo round($row1['dblQty']*$row1['dblGRNRate'],4); ?></span></td>
                            </tr>
                            <?php
								}	
							?>
                            <?php
							if($i==0){
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid"></td>
                              <td>&nbsp;</td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                            </tr>
                            <?php
							}
							?>
                        </table>                      
                      <?php
					  }
					  ?>
                      </td>
                        <td width="48%" valign="top">
                        <?php
						if($m>0){
						?>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="31%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;ISSUE NO</td>
                            <td width="29%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">Date</td>
                            <td width="1%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;</td>
                            <td width="24%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">ISSUE TO </td>
                            <td width="15%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                            <td width="15%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">&nbsp;PRICE&nbsp;</span></td>
                            <td width="15%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">AMOUNT</span></td>
                          </tr>
                          <?PHP 
								   $sql1 = "SELECT
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear,
											ware_stocktransactions_bulk.dblQty*-1 AS dblQty,
											ware_stocktransactions_bulk.dblGRNRate,
											date(ware_stocktransactions_bulk.dtDate) as date , 
											mst_department.strName
										FROM ware_stocktransactions_bulk
										inner join ware_issueheader on ware_issueheader.intIssueNo=ware_stocktransactions_bulk.intDocumentNo and ware_issueheader.intIssueYear=ware_stocktransactions_bulk.intDocumntYear
										inner join mst_department on mst_department.intId=ware_issueheader.intDepartment
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType =  'ISSUE' AND
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
											$paraDate
										";
								$result1 = $db->RunQuery($sql1);
								$i=0;
								while($row1=mysqli_fetch_array($result1))
								{
									$i++;
							?>
                          <tr class="normalfnt">
                            <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                            <td class="normalfntMid"><?php echo $row1['date']; ?></td>
                            <td class="normalfntMid">&nbsp;</td>
                            <td class="normalfntMid"><?php echo $row1['strName'] ?></td>
                            <td class="normalfntRight"><?php echo $row1['dblQty']; ?>&nbsp;</td>
                            <td class="normalfntRight"><?php echo $row1['dblGRNRate']; ?></td>
                            <td class="normalfntRight"><?php echo round($row1['dblQty']*$row1['dblGRNRate'],4); ?></td>
                          </tr>
                          <?php
								}	
							?>
                          <?php
							if($i==0){
							?>
                          <tr class="normalfnt">
                            <td class="normalfntMid"></td>
                            <td class="normalfntMid"></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td class="normalfntRight"></td>
                            <td class="normalfntRight"></td>
                            <td class="normalfntRight"></td>
                          </tr>
                          <?php
							}
							?>
                        </table>
                        
                        
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="31%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;RETURN SUP NO</td>
                            <td width="29%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">Date</td>
                            <td width="1%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;</td>
                            <td width="24%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">SUPPLIER</td>
                            <td width="15%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                            <td width="15%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">&nbsp;PRICE&nbsp;</span></td>
                            <td width="15%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">AMOUNT</span></td>
                          </tr>
                          <?PHP 
								$sql1 = "SELECT
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear,
											ware_stocktransactions_bulk.dblQty * -1 as dblQty, 
											ware_stocktransactions_bulk.dblGRNRate,
											date(ware_stocktransactions_bulk.dtDate) as date 
										FROM ware_stocktransactions_bulk
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType =  'RTSUP' AND
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
											$paraDate
										";
								$result1 = $db->RunQuery($sql1);
								$i=0;
								while($row1=mysqli_fetch_array($result1))
								{
									$i++;
							?>
                          <tr class="normalfnt">
                            <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                            <td class="normalfntMid"><?php echo $row1['date']; ?></td>
                            <td class="normalfntMid"></td>
                            <td class="normalfntMid"><?php echo $row['supplierName']; ?></td>
                            <td class="normalfntRight"><?php echo $row1['dblQty']; ?>&nbsp;</td>
                            <td class="normalfntRight"><?php echo $row1['dblGRNRate']; ?></td>
                            <td class="normalfntRight"><?php echo round($row1['dblQty']*$row1['dblGRNRate'],4); ?></td>
                          </tr>
                          <?php
								}	
							?>
                          <?php
							if($i==0){
							?>
                          <tr class="normalfnt">
                            <td class="normalfntMid"></td>
                            <td class="normalfntMid"></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td width="0%" class="normalfntRight"></td>
                          </tr>
                          <?php
							}
							?>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="31%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;GATEPASS NO</td>
                            <td width="29%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">Date</td>
                            <td width="1%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;</td>
                            <td width="24%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">TO LOCATION</td>
                            <td width="15%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                            <td width="15%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">&nbsp;PRICE&nbsp;</span></td>
                            <td width="15%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">AMOUNT</span></td>
                          </tr>
                          <?PHP 
								$sql1 = "SELECT
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear,
											ware_stocktransactions_bulk.dblQty * -1 as dblQty, 
											ware_stocktransactions_bulk.dblGRNRate,
											date(ware_stocktransactions_bulk.dtDate) as date, 
											mst_locations.strName as location 
										FROM ware_stocktransactions_bulk 
										Inner Join ware_gatepassheader ON ware_stocktransactions_bulk.intDocumentNo = ware_gatepassheader.intGatePassNo AND ware_stocktransactions_bulk.intDocumntYear = ware_gatepassheader.intGatePassYear
										Inner Join mst_locations ON ware_gatepassheader.intGPToLocation = mst_locations.intId
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND

											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType =  'GATEPASS' AND
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
											$paraDate
										";
								$result1 = $db->RunQuery($sql1);
								$i=0;
								while($row1=mysqli_fetch_array($result1))
								{
									$i++;
							?>
                          <tr class="normalfnt">
                            <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                            <td class="normalfntMid"><?php echo $row1['date']; ?></td>
                            <td class="normalfntMid">&nbsp;</td>
                            <td class="normalfntMid"><?php echo $row1['location']; ?></td>
                            <td class="normalfntRight"><?php echo $row1['dblQty']; ?>&nbsp;</td>
                            <td class="normalfntRight"><?php echo $row1['dblGRNRate']; ?></td>
                            <td class="normalfntRight"><?php echo round($row1['dblQty']*$row1['dblGRNRate'],4); ?></td>
                          </tr>
                          <?php
								}	
							?>
                          <?php
							if($i==0){
							?>
                          <tr class="normalfnt">
                            <td class="normalfntMid"></td>
                            <td class="normalfntMid"></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td width="15%" class="normalfntRight"></td>
                            <td width="15%" class="normalfntRight"></td>
                            <td width="15%" class="normalfntRight"></td>
                          </tr>
                          <?php
							}
							?>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="31%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;ALLOCATION NO</td>
                            <td width="29%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">Date</td>
                            <td width="1%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;</td>
                            <td width="24%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">STYLE NO</td>
                            <td width="15%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                            <td width="15%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">&nbsp;PRICE&nbsp;</span></td>
                            <td width="15%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">AMOUNT</span></td>
                          </tr>
                          <?PHP 
								  $sql1 = "SELECT
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear,
											sum(ware_stocktransactions_bulk.dblQty * -1) as dblQty, 
											ware_stocktransactions_bulk.dblGRNRate, 
											date(ware_stocktransactions_bulk.dtDate) as date , 
											trn_orderdetails.strStyleNo 
										FROM ware_stocktransactions_bulk 
											left Join trn_orderdetails ON ware_stocktransactions_bulk.intOrderNo = trn_orderdetails.intOrderNo AND ware_stocktransactions_bulk.intOrderYear = trn_orderdetails.intOrderYear AND ware_stocktransactions_bulk.intSalesOrderId = trn_orderdetails.intSalesOrderId
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType =  'ALLOCATE' AND
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location' 
											$paraDate
											group by ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear, ware_stocktransactions_bulk.dblGRNRate 
										";
								$result1 = $db->RunQuery($sql1);
								$i=0;
								while($row1=mysqli_fetch_array($result1))
								{
									$i++;
							?>
                          <tr class="normalfnt">
                            <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                            <td class="normalfntMid"><?php echo $row1['date']; ?></td>
                            <td class="normalfntMid">&nbsp;</td>
                            <td class="normalfntMid"><?php echo $row1['strStyleNo']; ?></td>
                            <td class="normalfntRight"><?php echo $row1['dblQty']; ?>&nbsp;</td>
                            <td class="normalfntRight"><?php echo $row1['dblGRNRate']; ?></td>
                            <td class="normalfntRight"><?php echo round($row1['dblQty']*$row1['dblGRNRate'],4); ?></td>
                          </tr>
                          <?php
								}	
							?>
                          <?php
							if($i==0){
							?>
                          <tr class="normalfnt">
                            <td class="normalfntMid"></td>
                            <td class="normalfntMid"></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td width="15%" class="normalfntRight"></td>
                            <td width="15%" class="normalfntRight"></td>
                            <td width="15%" class="normalfntRight"></td>
                          </tr>
                          <?php
							}
							?>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="31%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;INVOICE NO</td>
                            <td width="29%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">Date</td>
                            <td width="1%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;</td>
                            <td width="24%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">INVOICE TO</td>
                            <td width="15%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                            <td width="15%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">&nbsp;PRICE&nbsp;</span></td>
                            <td width="15%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">AMOUNT</span></td>
                          </tr>
                          <?PHP 
							  	$sql1 = "SELECT
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear,
											ware_stocktransactions_bulk.dblQty * -1 as dblQty, 
											ware_stocktransactions_bulk.dblGRNRate,
											date(ware_stocktransactions_bulk.dtDate) as date , 
											mst_locations.strName AS location 
										FROM ware_stocktransactions_bulk  
										Inner Join ware_invoiceheader ON ware_stocktransactions_bulk.intDocumentNo = ware_invoiceheader.intInvoiceNo AND ware_stocktransactions_bulk.intDocumntYear = ware_invoiceheader.intInvoiceYear
											Inner Join trn_poheader ON ware_invoiceheader.intPONo = trn_poheader.intPONo AND ware_invoiceheader.intPOYear = trn_poheader.intPOYear
											Inner Join mst_locations ON trn_poheader.intDeliveryTo = mst_locations.intId
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType =  'INVOICE' AND
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
											$paraDate
										";
								$result1 = $db->RunQuery($sql1);
								$i=0;
								while($row1=mysqli_fetch_array($result1))
								{
									$i++;
							?>
                          <tr class="normalfnt">
                            <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                            <td class="normalfntMid"><?php echo $row1['date']; ?></td>
                            <td class="normalfntMid"></td>
                            <td class="normalfntMid"><?php echo $row1['location']; ?></td>
                            <td class="normalfntRight"><?php echo $row1['dblQty']; ?>&nbsp;</td>
                            <td class="normalfntRight"><?php echo $row1['dblGRNRate']; ?></td>
                            <td class="normalfntRight"><?php echo round($row1['dblQty']*$row1['dblGRNRate'],4); ?></td>
                          </tr>
                          <?php
								}	
							?>
                          <?php
							if($i==0){
							?>
                          <tr class="normalfnt">
                            <td class="normalfntMid"></td>
                            <td class="normalfntMid"></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td width="15%" class="normalfntRight"></td>
                            <td width="15%" class="normalfntRight"></td>
                            <td width="15%" class="normalfntRight"></td>
                          </tr>
                          <?php
							}
							?>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="31%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;STOCK ADJUSTMENTS -</td>
                            <td width="29%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">Date</td>
                            <td width="1%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;</td>
                            <td width="24%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">&nbsp;</td>
                            <td width="15%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                            <td width="15%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">&nbsp;PRICE&nbsp;</span></td>
                            <td width="15%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">AMOUNT</span></td>
                          </tr>
                          <?PHP 
							  	$sql1 = "select 
								sum(tb1.dblQty)  as dblQty,
								tb1.dblGRNRate ,
								tb1.date 
								From(SELECT
											abs(sum(ware_stocktransactions_bulk.dblQty )) as dblQty, 
											ware_stocktransactions_bulk.dblGRNRate, 
											date(ware_stocktransactions_bulk.dtDate) as date 
										FROM ware_stocktransactions_bulk  
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType IN  ('Adjust Q+','Adjust R+','Adjust RQ+','Adjust Q-') AND 
											ware_stocktransactions_bulk.dblQty <0 AND 
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
											$paraDate
										  Group by ware_stocktransactions_bulk.dtDate, ware_stocktransactions_bulk.dblGRNRate) as tb1 
										  Group by tb1.date, tb1.dblGRNRate";
								$result1 = $db->RunQuery($sql1);
								$i=0;
								while($row1=mysqli_fetch_array($result1))
								{
									$i++;
							?>
                          <tr class="normalfnt">
                            <td class="normalfntMid">&nbsp;</td>
                            <td class="normalfntMid"><?php echo $row1['date']; ?></td>
                            <td class="normalfntMid"></td>
                            <td class="normalfntMid">&nbsp;</td>
                            <td class="normalfntRight"><?php echo $row1['dblQty']; ?>&nbsp;</td>
                            <td class="normalfntRight"><?php echo $row1['dblGRNRate']; ?></td>
                            <td class="normalfntRight"><?php echo round($row1['dblQty']*$row1['dblGRNRate'],4); ?></td>
                          </tr>
                          <?php
								}	
							?>
                          <?php
							if($i==0){
							?>
                          <tr class="normalfnt">
                            <td class="normalfntMid"></td>
                            <td class="normalfntMid"></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td width="15%" class="normalfntRight"></td>
                            <td width="15%" class="normalfntRight"></td>
                            <td width="15%" class="normalfntRight"></td>
                          </tr>
                          <?php
							}
							?>
                        </table>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                          <tr>
                            <td width="31%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;ITEM CREATION NO -</td>
                            <td width="29%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">Date</td>
                            <td width="1%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;</td>
                            <td width="24%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">&nbsp;</td>
                            <td width="15%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                            <td width="15%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">&nbsp;PRICE&nbsp;</span></td>
                            <td width="15%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">AMOUNT</span></td>
                          </tr>
                          <?php 
							  	  $sql2 = "SELECT 
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear,
											abs(ware_stocktransactions_bulk.dblQty ) as dblQty, 
											ware_stocktransactions_bulk.dblGRNRate,
											date(ware_stocktransactions_bulk.dtDate) as date 
										FROM ware_stocktransactions_bulk  
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType =  'ITEMCREATION-' AND 
											ware_stocktransactions_bulk.dblQty <0 AND 
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
											$paraDate
										  ";
								$result2 = $db->RunQuery($sql2);
								$i=0;
								while($row1=mysqli_fetch_array($result2))
								{
									//echo $row1['intDocumntYear'];
									$i++;
							?>
                          <tr class="normalfnt">
                            <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                            <td class="normalfntMid"><?php echo $row1['date']; ?></td>
                            <td class="normalfntMid"></td>
                            <td class="normalfntMid"></td>
                            <td class="normalfntRight"><?php echo $row1['dblQty']; ?>&nbsp;</td>
                            <td class="normalfntRight"><?php echo $row1['dblGRNRate']; ?></td>
                            <td class="normalfntRight"><?php echo round($row1['dblQty']*$row1['dblGRNRate'],4); ?></td>
                          </tr>
                          <?php
								}	
							?>
                          <?php
							if($i==0){
							?>
                          <tr class="normalfnt">
                            <td class="normalfntMid"></td>
                            <td class="normalfntMid"></td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td width="15%" class="normalfntRight"></td>
                            <td width="15%" class="normalfntRight"></td>
                            <td width="15%" class="normalfntRight"></td>
                          </tr>
                          <?php
							}
							?>
                        </table>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="32%" style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;CLOSE STOCK</td>
                              <td width="18%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"> Date</td>
                              <td width="26%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">&nbsp;</td>
                              <td width="24%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                              <td width="24%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">&nbsp;PRICE&nbsp;</td>
                              <td width="24%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">AMOUNT</td>
                            </tr>
                            <?PHP 
								$sql1 = "SELECT
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear,
											ware_stocktransactions_bulk.dblQty*(-1) AS dblQty, 
											ware_stocktransactions_bulk.dblGRNRate,
											date(ware_stocktransactions_bulk.dtDate) as date 
										FROM ware_stocktransactions_bulk
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType LIKE  '%CLOSESTOCK%' AND
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
											$paraDate
										";
										$i=0;
								$result1 = $db->RunQuery($sql1);
								while($row1=mysqli_fetch_array($result1))
								{
									$i++;
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                              <td class="normalfntMid"><?php echo $row1['date']; ?></td>
                              <td>&nbsp;</td>
                              <td class="normalfntRight"><?php echo $row1['dblQty']; ?>&nbsp;</td>
                              <td class="normalfntRight"><?php echo $row1['dblGRNRate']; ?></td>
                              <td class="normalfntRight"><?php echo round($row1['dblQty']*$row1['dblGRNRate'],4); ?></td>
                            </tr>
                            <?php
								}	
							?>
                            <?php
							if($i==0){
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid"></td>
                              <td>&nbsp;</td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                            </tr>
                            <?php
							}
							?>
                          </table>                        
<table width="100%" border="0" cellspacing="0" cellpadding="0">
                        
                            <tr>
                              <td width="32%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">STORES TRANSFER-</td>
                            <td width="18%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">Date</td>
                              <td width="26%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">&nbsp;</td>
                              <td width="24%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                              <td width="24%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">&nbsp;PRICE&nbsp;</span></td>
                              <td width="24%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">AMOUNT</span></td>
                              
                            </tr>
                                  <?PHP 
							  	$sql1 = "select 
								tb1.intDocumentNo,
								tb1.intDocumntYear,
								sum(tb1.dblQty)  as dblQty,
								tb1.dblGRNRate , 
								tb1.date 
								From(SELECT 
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear,
											abs(sum(ware_stocktransactions_bulk.dblQty*(-1) )) as dblQty, 
											ware_stocktransactions_bulk.dblGRNRate,
											date(ware_stocktransactions_bulk.dtDate) as date 
										FROM ware_stocktransactions_bulk  
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType IN  ('STORESTRANSFER') AND 
											ware_stocktransactions_bulk.dblQty <0 AND 
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
											$paraDate
										  Group by ware_stocktransactions_bulk.dtDate, ware_stocktransactions_bulk.dblGRNRate) as tb1 
										  Group by tb1.date, tb1.dblGRNRate";
								$result1 = $db->RunQuery($sql1);
								$i=0;
								while($row1=mysqli_fetch_array($result1))
								{
									$i++;
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                              <td class="normalfntMid"><?php echo $row1['date']; ?></td>
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid"><span class="normalfntRight"><?php echo $row1['dblQty']; ?>&nbsp;</span></td>
                              <td class="normalfntMid"><span class="normalfntRight"><?php echo $row1['dblGRNRate']; ?></span></td>
                              <td class="normalfntMid"><span class="normalfntRight"><?php echo round($row1['dblQty']*$row1['dblGRNRate'],4); ?></span></td>
                            </tr>
                            <?php
								}	
							?>
                            <?php
							if($i==0){
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid"></td>
                              <td>&nbsp;</td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                            </tr>
                            <?php
							}
							?>
                        </table>   
<table width="100%" border="0" cellspacing="0" cellpadding="0">
                        
                            <tr>
                              <td width="32%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">DISPOSAL</td>
                            <td width="18%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">Date</td>
                              <td width="26%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">&nbsp;</td>
                              <td width="24%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                              <td width="24%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">&nbsp;PRICE&nbsp;</span></td>
                              <td width="24%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid"><span class="normalfntMid" style="color:#666">AMOUNT</span></td>
                              
                            </tr>
                                  <?PHP 
							  	$sql1 = "select 
								tb1.intDocumentNo,
								tb1.intDocumntYear,
								sum(tb1.dblQty)  as dblQty,
								tb1.dblGRNRate, 
								tb1.date 
								From(SELECT 
								ware_stocktransactions_bulk.intDocumentNo,
								ware_stocktransactions_bulk.intDocumntYear,
											abs(sum(ware_stocktransactions_bulk.dblQty*(-1) )) as dblQty, 
											ware_stocktransactions_bulk.dblGRNRate, 
											date(ware_stocktransactions_bulk.dtDate) as date 
										FROM ware_stocktransactions_bulk  
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType IN  ('Dispose') AND 
											ware_stocktransactions_bulk.dblQty <0 AND 
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
											$paraDate
										  Group by ware_stocktransactions_bulk.dtDate, ware_stocktransactions_bulk.dblGRNRate) as tb1 
										  Group by tb1.date, tb1.dblGRNRate";
								$result1 = $db->RunQuery($sql1);
								$i=0;
								while($row1=mysqli_fetch_array($result1))
								{
									$i++;
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                              <td class="normalfntMid"><?php echo $row1['date']; ?></td>
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid"><span class="normalfntRight"><?php echo $row1['dblQty']; ?>&nbsp;</span></td>
                              <td class="normalfntMid"><span class="normalfntRight"><?php echo $row1['dblGRNRate']; ?></span></td>
                              <td class="normalfntMid"><span class="normalfntRight"><?php echo round($row1['dblQty']*$row1['dblGRNRate'],4); ?></span></td>
                            </tr>
                            <?php
								}	
							?>
                            <?php
							if($i==0){
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"></td>
                              <td class="normalfntMid"></td>
                              <td>&nbsp;</td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                              <td class="normalfntRight"></td>
                            </tr>
                            <?php
							}
							?>
                        </table>                        
                                             
                        <?php
						}
						?>
                        
                      </td>
                    </tr>
                    </table>
                </td>
            </tr>
            <tr >
              <td colspan="6" class="normalfntMid"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="37%">&nbsp;</td>
                  <td width="46%" bgcolor="#E9E6E0">Stock Balance </td>
                  <?PHP 
								 $sql1 = "SELECT 
											sum(ware_stocktransactions_bulk.dblQty ) as dblQty  ,
											sum(ware_stocktransactions_bulk.dblQty*dblGRNRate) as balAmntQty  
											
										FROM ware_stocktransactions_bulk  
										WHERE
											ware_stocktransactions_bulk.strType =  'ITEMCREATION+' AND 
											ware_stocktransactions_bulk.dblQty >0 AND 
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
											
											$paraDate
										";
								$result1 = $db->RunQuery($sql1);
								$row1=mysqli_fetch_array($result1);
				?>
                  <td width="5%" bgcolor="#E9E6E0" style="font-size:14px;text-align:right;color:#06F"><?php echo round($row1['balQty'],4); ?></td>
                  <td width="5%" bgcolor="#E9E6E0" style="font-size:14px;text-align:right;color:#06F"><?php echo round($row1['balAmntQty']/$row1['balQty'],4); ?></td>
                  <td width="7%" bgcolor="#E9E6E0" style="font-size:14px;text-align:right;color:#06F"><?php echo round($row1['balAmntQty'],4); ?></td>
               
                </tr>
              </table></td>
            </tr>
            <tr >
              <td colspan="6" bgcolor="#CCCCCC" class="normalfntMid"></td>
            </tr>
    <?php
			}
			if($grnNo=='')
			{
			//	echo "<tr><td align=\"middle\" colspan=\"6\">&nbsp;NO RECORD FOUND</td></tr>";	
			}
	?>
    
            <tr >
              <td colspan="8" bgcolor="#CCCCCC" class="normalfntMid">
              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                
                  <td width="41%">&nbsp;</td>
                  <td width="47%"><strong>Total Item Balance</strong></td>
                  <?PHP 
								  $sql1 = "SELECT
											sum(ware_stocktransactions_bulk.dblQty) as balQty
										FROM ware_stocktransactions_bulk
										WHERE
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
											$paraDate
										";
								$result1 = $db->RunQuery($sql1);
								$row1=mysqli_fetch_array($result1);
				?>
                  <td width="2%" style="font-size:18px;text-align:right;color:#06F;border-top:groove;border-bottom:double"><?php echo round($row1['balQty'],4); ?></td>
                  <td width="3%" style="font-size:18px;text-align:right;color:#06F;border-top:groove;border-bottom:double">&nbsp;</td>
                  <td width="7%" style="font-size:18px;text-align:right;color:#06F;border-top:groove;border-bottom:double">&nbsp;</td>
                </tr>
              </table></td>
            </tr>
    		<tr >
              <td colspan="6" class="normalfntMid"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="52%">&nbsp;</td>
                  <td width="48%">&nbsp;</td>
                </tr>
              </table></td>
            </tr>
  </table> 
  </td>
</tr>

<tr height="40">
  <td align="center" class="normalfntMid">&nbsp;</td>
</tr>
</table>
</div>        
</form>
</body>
</html>
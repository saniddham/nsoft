			
$(document).ready(function() {
	
	$('#frmStockBalance #cboCompany').change(function(){
	var company = $('#frmStockBalance #cboCompany').val();
	var url 		= "stockMovement-db-get.php?requestType=loadLocations&company="+company;
	var httpobj 	= $.ajax({url:url,async:false})
	document.getElementById('cboLocation').innerHTML=httpobj.responseText;
	
	});

//--------------------------------------------------------
/*$('#frmStockBalance #cboCompany').change(function(){
	var company = $('#frmStockBalance #cboCompany').val();
	var url 		= "stockMovement-db-get.php?requestType=loadCurrency&company="+company;
	var httpobj 	= $.ajax({url:url,async:false});
	var curr=httpobj.responseText.trim();
	document.getElementById('cboCurrency').value=curr;
	
});*/
//--------------------------------------------------------
  $('#frmStockBalance #cboOrderNo').change(function(){
		var orderNo = $('#frmStockBalance #cboOrderNo').val();
		var url 		= "stockMovement-db-get.php?requestType=loadStyleNo&orderNo="+orderNo;
		var httpobj 	= $.ajax({url:url,async:false})
		document.getElementById('cbStyleNo').innerHTML=httpobj.responseText;
		
  });
 //-------------------------------------------- 
  $('#frmStockBalance #cboMainCategory').change(function(){
		var mainCategory = $('#frmStockBalance #cboMainCategory').val();
		var url 		= "stockMovement-db-get.php?requestType=loadSubCategory&mainCategory="+mainCategory;
		var httpobj 	= $.ajax({url:url,async:false})
		document.getElementById('cboSubCategory').innerHTML=httpobj.responseText;
  });
 //-------------------------------------------- 
  $('#frmStockBalance #cboSubCategory').change(function(){
		var mainCategory = $('#frmStockBalance #cboMainCategory').val();
		var subCategory = $('#frmStockBalance #cboSubCategory').val();
		var url 		= "stockMovement-db-get.php?requestType=loadItems&mainCategory="+mainCategory+"&subCategory="+subCategory;
		var httpobj 	= $.ajax({url:url,async:false})
		document.getElementById('cboItems').innerHTML=httpobj.responseText;
  });
 //-------------------------------------------- 
  $('#frmStockBalance #chkStyle').click(function(){
		document.getElementById('rwOrder').style.display='';
		document.getElementById('rwStyle').style.display='';
  });
 //-------------------------------------------- 
  $('#frmStockBalance #chkGeneral').click(function(){
		document.getElementById('rwOrder').style.display='none';
		document.getElementById('rwStyle').style.display='none';
  });
 //-------------------------------------------- 
  $('#frmStockBalance #imgSearchItems').click(function(){
	  
	if ($('#frmStockBalance').validationEngine('validate'))   
    { 
	//alert($('#chkNormal').attr('checked'));
	if($('#chkNormal').attr('checked'))
		var url='rptStockMovement.php?';
	else
		var url='rptStockMovement-Details.php?';
		
		url+='company='+	$('#frmStockBalance #cboCompany').val();
		url+='&location='+	$('#frmStockBalance #cboLocation').val();
		url+='&mainCat='+	$('#frmStockBalance #cboMainCategory').val();
		url+='&subCat='+	$('#frmStockBalance #cboSubCategory').val();
		url+='&item='+		$('#frmStockBalance #cboItems').val();
		url+='&fromDate='+	$('#frmStockBalance #txtFromDate').val();
		url+='&toDate='+	$('#frmStockBalance #txtToDate').val();
		window.open(url);
	}
  });
  
  $('#chkDate').click(function(){
	  var val = $(this).attr('checked');
	  if(!val)
	  {
		$('#txtFromDate').val('');  
		$('#txtFromDate').attr('disabled','true');
		$('#txtToDate').val('');  
		$('#txtToDate').attr('disabled','true');
	  }
	  else
	  {
		//$('#txtFromDate').val('');  
		$('#txtFromDate').removeAttr('disabled');
		//$('#txtToDate').val('');  
		$('#txtToDate').removeAttr('disabled');
	  }
  });

});

function loadItems(){
	var data = "requestType=loadItems";
	var string="";
	
	$('.subcheck:checked').each(function(){
		var subCatId=$(this).val();	
			//	arr2 += "{";
			//	arr2 += '"subCatId":"'+		subCatId  +'"' ;
				string += subCatId  +',' ;
			//	arr2 +=  '},';
	});

				string = string.substr(0,string.length-1);
				data+="&string="	+	string;
	var url 		= "stockMovement-db-get.php?"+data;
	var httpobj 	= $.ajax({url:url,async:false})
	document.getElementById('cboItems').innerHTML=httpobj.responseText;
}
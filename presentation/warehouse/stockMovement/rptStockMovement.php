<?php
$companyId = $_SESSION['CompanyID'];
$locationId 	= $_SESSION['CompanyID'];//this locationId use in report header(reportHeader.php)-------
$intUser  = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
 
$company = $_REQUEST['company'];
$location = $_REQUEST['location'];

$mainCat 	= $_REQUEST['mainCat'];
$subCat 	= $_REQUEST['subCat'];
$item 		= $_REQUEST['item'];
$fromDate 	= $_REQUEST['fromDate'];
$toDate 	= $_REQUEST['toDate'];


//////////////date range //////////////
if($fromDate!='')
	$paraDate = " AND date(dtDate)>='$fromDate' ";

if($toDate!='')
	$paraDate .= " AND date(dtDate)<='$toDate' ";
///////////////////////////////////////

 $sql = "SELECT
			mst_companies.strName
			FROM mst_companies
		WHERE
			mst_companies.intId =  '$company'
		";
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
$companyName1 = $row['strName'];
	
	
				 
 $sql = "SELECT
			mst_locations.strName
		FROM
			mst_locations
		WHERE
			mst_locations.intId =  '$location'
		";
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
$locationName1 = $row['strName'];

$sql = "SELECT
			mst_subcategory.strName AS subName,
			mst_maincategory.strName AS mainName,
			mst_item.strName AS itemName
		FROM
		mst_maincategory
			Inner Join mst_subcategory ON mst_maincategory.intId = mst_subcategory.intMainCategory
			Inner Join mst_item ON mst_subcategory.intId = mst_item.intSubCategory
		WHERE
			mst_item.intId =  '$item'
		";
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
$subName = $row['subName'];
$mainName = $row['mainName'];
$itemName = $row['itemName'];
?>
<head>
<title>Stock Balance Report</title>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:264px;
	top:247px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<form id="frmMrnReport" name="frmMrnReport" method="post" action="">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include 'reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>STOCK MOVEMENT REPORT</strong><strong></strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
  <tr>
    <td width="7%">&nbsp;</td>
    <td width="19%" class="normalfntMid" align="center"><strong>Main Category</strong></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="28%" align="left"><span class="normalfnt"><?php echo $mainName  ?></span></td>
    <td width="11%"><span class="normalfnt"><strong style="text-align:right">Company</strong></span></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="20%"><span class="normalfnt"><strong style="text-align:right"><span class="normalfnt" style="text-align:left"><?php echo $companyName1  ?></span></strong></span></td>
    <td width="6%" class="normalfntMid">&nbsp;</td>
    <td width="7%">&nbsp;</td>
  </tr>
 
  <tr>
    <td>&nbsp;</td>
    <td class="normalfntMid"><strong>Sub Category</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $subName  ?></span></td>
    <td><span class="normalfnt"><strong>Location</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><strong style="text-align:right"><span class="normalfnt" style="text-align:left"><?php echo $locationName1;  ?></span></strong></span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfntMid"><strong>Item</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $itemName;  ?></span></td>
    <td>&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>

  
  </table>
  </td>
</tr>
<tr>
  <td><table width="1169"  cellspacing="1" cellpadding="0" style="border-style:groove" >
    <tr>
      <td width="78" height="20" align="center" bgcolor="#469EE6" style="color:#FFF;font-size:11px"><strong>DATE</strong></td>
      <td width="88" align="center" bgcolor="#469EE6" style="color:#FFF;font-size:11px"><strong>PO NO</strong></td>
      <td width="88" align="center" bgcolor="#469EE6" style="color:#FFF;font-size:11px"><strong>PO QTY</strong></td>
      <td width="95" align="center" bgcolor="#469EE6" style="color:#FFF;font-size:11px"><strong>GRN NO</strong></td>
      <td width="81" align="center" bgcolor="#469EE6" style="color:#FFF;font-size:11px"><strong>GRN QTY</strong></td>
      <td width="89" align="center" bgcolor="#469EE6" style="color:#FFF;font-size:11px"><strong>ISSUE QTY</strong></td>
      <td width="88" align="center" bgcolor="#469EE6" style="color:#FFF;font-size:11px"><strong>RETURN TO STORES QTY</strong></td>
      <td width="89" align="center" bgcolor="#469EE6" style="color:#FFF;font-size:11px"><strong>RETURN TO SUPPLIER QTY</strong></td>
      <td width="89" align="center" bgcolor="#469EE6" style="color:#FFF;font-size:11px"><strong>GATE PASS QTY</strong></td>
      <td width="88" align="center" bgcolor="#469EE6" style="color:#FFF;font-size:11px"><strong>TRANSFER IN QTY</strong></td>
      <td width="91" align="center" bgcolor="#469EE6" style="color:#FFF;font-size:11px"><strong>STYLE ALLOCATED QTY</strong></td>
      <td width="89" align="center" bgcolor="#469EE6" style="color:#FFF;font-size:11px"><strong>STYLE UN ALLOCATED QTY</strong></td>
      <td width="89" align="center" bgcolor="#469EE6" style="color:#FFF;font-size:11px">INVOICE QTY</td>
      <td width="89" align="center" bgcolor="#469EE6" style="color:#FFF;font-size:11px">RETURN INVOICE QTY</td>
      <td width="89" align="center" bgcolor="#469EE6" style="color:#FFF;font-size:11px">STOCK ADJUSTMENT+ QTY</td>
      <td width="89" align="center" bgcolor="#469EE6" style="color:#FFF;font-size:11px">STOCK ADJUSTMENT- QTY</td>
      <td width="89" align="center" bgcolor="#469EE6" style="color:#FFF;font-size:11px">ALLOCATE TO CREATE ITEM QTY</td>
      <td width="89" align="center" bgcolor="#469EE6" style="color:#FFF;font-size:11px">DISPOSE QTY</td>
      <td width="94" align="center" bgcolor="#469EE6" style="color:#FC0;font-size:11px"><strong>BALANCE QTY</strong></td>
      </tr>
      <?PHP
	  if($fromDate!='')
		 {
			$datePara = " AND  MAINSTOCK.dtGRNDate>='$fromDate' ";	 
			$datePara2 = " AND  MAINSTOCK.dtDate>='$fromDate'";	 
		 }
	  if($toDate!='')
		 {
			$datePara = " AND MAINSTOCK.dtGRNDate<='$toDate' ";	 
			$datePara2 = " AND MAINSTOCK.dtDate<='$toDate' ";	 
		 }
		 
	  	 $sql = "(SELECT DISTINCT 
					MAINSTOCK.intItemId,
					MAINSTOCK.dtGRNDate,
					trn_podetails.intPONo,
					trn_podetails.intPOYear,
					trn_podetails.dblQty AS poQty,
					MAINSTOCK.intGRNNo,
					MAINSTOCK.intGRNYear,
					1 as grnFlag,
					
					round((select sum(ware_stocktransactions_bulk.dblQty) FROM ware_stocktransactions_bulk WHERE ware_stocktransactions_bulk.strType =  'GRN' AND ware_stocktransactions_bulk.intDocumentNo=MAINSTOCK.intGRNNo AND ware_stocktransactions_bulk.intDocumntYear=MAINSTOCK.intGRNYear AND ware_stocktransactions_bulk.intItemId=MAINSTOCK.intItemId),2) as grnQty, 
					
					IF((select distinct ware_grnheader.intCompanyId 
					FROM ware_stocktransactions_bulk 
					Inner Join ware_grnheader ON ware_stocktransactions_bulk.intDocumentNo = ware_grnheader.intGrnNo AND ware_stocktransactions_bulk.intDocumntYear = ware_grnheader.intGrnYear 
					WHERE ware_stocktransactions_bulk.strType =  'GRN' AND ware_stocktransactions_bulk.intDocumentNo=MAINSTOCK.intGRNNo AND ware_stocktransactions_bulk.intDocumntYear=MAINSTOCK.intGRNYear AND ware_stocktransactions_bulk.intItemId=MAINSTOCK.intItemId)=$location,1,0) as flagLocation,
					
					round((select abs(sum(ware_stocktransactions_bulk.dblQty)) FROM ware_stocktransactions_bulk WHERE ware_stocktransactions_bulk.strType =  'ISSUE' AND ware_stocktransactions_bulk.intGRNNo=MAINSTOCK.intGRNNo AND ware_stocktransactions_bulk.intGRNYear=MAINSTOCK.intGRNYear AND ware_stocktransactions_bulk.intItemId=MAINSTOCK.intItemId AND MAINSTOCK.intLocationId=ware_stocktransactions_bulk.intLocationId),2) as issueQty, 
					
					round((select abs(sum(ware_stocktransactions_bulk.dblQty)) FROM ware_stocktransactions_bulk WHERE ware_stocktransactions_bulk.strType =  'RETSTORES' AND ware_stocktransactions_bulk.intGRNNo=MAINSTOCK.intGRNNo AND ware_stocktransactions_bulk.intGRNYear=MAINSTOCK.intGRNYear AND ware_stocktransactions_bulk.intItemId=MAINSTOCK.intItemId AND MAINSTOCK.intLocationId=ware_stocktransactions_bulk.intLocationId),2) as returnStoresQty, 
					
					round((select abs(sum(ware_stocktransactions_bulk.dblQty)) FROM ware_stocktransactions_bulk WHERE ware_stocktransactions_bulk.strType =  'RTSUP' AND ware_stocktransactions_bulk.intGRNNo=MAINSTOCK.intGRNNo AND ware_stocktransactions_bulk.intGRNYear=MAINSTOCK.intGRNYear AND ware_stocktransactions_bulk.intItemId=MAINSTOCK.intItemId AND MAINSTOCK.intLocationId=ware_stocktransactions_bulk.intLocationId),2) as returnToSupQty, 
					
					round((select abs(sum(ware_stocktransactions_bulk.dblQty)) FROM ware_stocktransactions_bulk WHERE ware_stocktransactions_bulk.strType =  'GATEPASS' AND ware_stocktransactions_bulk.intGRNNo=MAINSTOCK.intGRNNo AND ware_stocktransactions_bulk.intGRNYear=MAINSTOCK.intGRNYear AND ware_stocktransactions_bulk.intItemId=MAINSTOCK.intItemId AND MAINSTOCK.intLocationId=ware_stocktransactions_bulk.intLocationId),2) as gatepassQty, 
					
					round((select abs(sum(ware_stocktransactions_bulk.dblQty)) FROM ware_stocktransactions_bulk WHERE ware_stocktransactions_bulk.strType =  'TRANSFERIN' AND ware_stocktransactions_bulk.intGRNNo=MAINSTOCK.intGRNNo AND ware_stocktransactions_bulk.intGRNYear=MAINSTOCK.intGRNYear AND ware_stocktransactions_bulk.intItemId=MAINSTOCK.intItemId AND MAINSTOCK.intLocationId=ware_stocktransactions_bulk.intLocationId),2) as transferInQty, 
					
					round((select abs(sum(ware_stocktransactions_bulk.dblQty)) FROM ware_stocktransactions_bulk WHERE ware_stocktransactions_bulk.strType =  'ALLOCATE' AND ware_stocktransactions_bulk.intGRNNo=MAINSTOCK.intGRNNo AND ware_stocktransactions_bulk.intGRNYear=MAINSTOCK.intGRNYear AND ware_stocktransactions_bulk.intItemId=MAINSTOCK.intItemId AND MAINSTOCK.intLocationId=ware_stocktransactions_bulk.intLocationId),2) as allocatedQty, 
					
					round((select abs(sum(ware_stocktransactions_bulk.dblQty)) FROM ware_stocktransactions_bulk WHERE ware_stocktransactions_bulk.strType =  'UNALLOCATE' AND ware_stocktransactions_bulk.intGRNNo=MAINSTOCK.intGRNNo AND ware_stocktransactions_bulk.intGRNYear=MAINSTOCK.intGRNYear AND ware_stocktransactions_bulk.intItemId=MAINSTOCK.intItemId AND MAINSTOCK.intLocationId=ware_stocktransactions_bulk.intLocationId),2) as unAllocatedQty, 
					
					round((select abs(sum(ware_stocktransactions_bulk.dblQty)) FROM ware_stocktransactions_bulk WHERE ware_stocktransactions_bulk.strType =  'INVOICE' AND ware_stocktransactions_bulk.intGRNNo=MAINSTOCK.intGRNNo AND ware_stocktransactions_bulk.intGRNYear=MAINSTOCK.intGRNYear AND ware_stocktransactions_bulk.intItemId=MAINSTOCK.intItemId AND MAINSTOCK.intLocationId=ware_stocktransactions_bulk.intLocationId),2) as invoiceQty, 

					round((select abs(sum(ware_stocktransactions_bulk.dblQty)) FROM ware_stocktransactions_bulk WHERE ware_stocktransactions_bulk.strType =  'RTSUP_RCV' AND ware_stocktransactions_bulk.intGRNNo=MAINSTOCK.intGRNNo AND ware_stocktransactions_bulk.intGRNYear=MAINSTOCK.intGRNYear AND ware_stocktransactions_bulk.intItemId=MAINSTOCK.intItemId AND MAINSTOCK.intLocationId=ware_stocktransactions_bulk.intLocationId),2) as retSupRcvQty, 

					round((select abs(sum(ware_stocktransactions_bulk.dblQty)) FROM ware_stocktransactions_bulk WHERE ware_stocktransactions_bulk.strType IN  ('Adjust Q+','Adjust R+','Adjust RQ+') AND ware_stocktransactions_bulk.intGRNNo=MAINSTOCK.intGRNNo AND ware_stocktransactions_bulk.intGRNYear=MAINSTOCK.intGRNYear AND ware_stocktransactions_bulk.intItemId=MAINSTOCK.intItemId AND MAINSTOCK.intLocationId=ware_stocktransactions_bulk.intLocationId),2) as adjustPlusQty, 
					
					round((select abs(sum(ware_stocktransactions_bulk.dblQty)) FROM ware_stocktransactions_bulk WHERE ware_stocktransactions_bulk.strType =  'Adjust Q-' AND ware_stocktransactions_bulk.intGRNNo=MAINSTOCK.intGRNNo AND ware_stocktransactions_bulk.intGRNYear=MAINSTOCK.intGRNYear AND ware_stocktransactions_bulk.intItemId=MAINSTOCK.intItemId AND MAINSTOCK.intLocationId=ware_stocktransactions_bulk.intLocationId),2) as adjustMinusQty, 

					round((select abs(sum(ware_stocktransactions_bulk.dblQty)) FROM ware_stocktransactions_bulk WHERE ware_stocktransactions_bulk.strType =  'ITEMCREATION-' AND ware_stocktransactions_bulk.intGRNNo=MAINSTOCK.intGRNNo AND ware_stocktransactions_bulk.intGRNYear=MAINSTOCK.intGRNYear AND ware_stocktransactions_bulk.intItemId=MAINSTOCK.intItemId AND MAINSTOCK.intLocationId=ware_stocktransactions_bulk.intLocationId),2) as allocToNewQty, 
				
					round((select abs(sum(ware_stocktransactions_bulk.dblQty)) FROM ware_stocktransactions_bulk WHERE ware_stocktransactions_bulk.strType =  'Dispose' AND ware_stocktransactions_bulk.intGRNNo=MAINSTOCK.intGRNNo AND ware_stocktransactions_bulk.intGRNYear=MAINSTOCK.intGRNYear AND ware_stocktransactions_bulk.intItemId=MAINSTOCK.intItemId AND MAINSTOCK.intLocationId=ware_stocktransactions_bulk.intLocationId),2) as disposedQty, 
					
					round((select sum(ware_stocktransactions_bulk.dblQty) FROM ware_stocktransactions_bulk WHERE   ware_stocktransactions_bulk.intGRNNo=MAINSTOCK.intGRNNo AND ware_stocktransactions_bulk.intGRNYear=MAINSTOCK.intGRNYear AND ware_stocktransactions_bulk.intItemId=MAINSTOCK.intItemId AND MAINSTOCK.intLocationId=ware_stocktransactions_bulk.intLocationId),2) as stockBalance, 



					
					IFNULL(mst_supplier.strName,'OPEN STOCK') AS supplierName
					FROM
					ware_stocktransactions_bulk as MAINSTOCK
					left Join ware_grnheader ON ware_grnheader.intGrnNo = MAINSTOCK.intGRNNo AND ware_grnheader.intGrnYear = MAINSTOCK.intGRNYear
					left Join trn_podetails ON trn_podetails.intPONo = ware_grnheader.intPoNo AND trn_podetails.intPOYear = ware_grnheader.intPoYear AND MAINSTOCK.intItemId = trn_podetails.intItem
					left Join trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
					left Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
				
					WHERE 
					MAINSTOCK.intGRNNo <> 0 AND 
					/*(MAINSTOCK.strType =  'GRN' OR MAINSTOCK.strType =  'OPENSTOCK')  AND*/
					MAINSTOCK.intLocationId =  '$location' AND
					MAINSTOCK.intItemId =  '$item' 
					
					$datePara
					
					GROUP BY
					MAINSTOCK.strType,
					MAINSTOCK.dtGRNDate,
					MAINSTOCK.intLocationId,
					MAINSTOCK.intItemId,
					MAINSTOCK.intGRNNo,
					MAINSTOCK.intGRNYear,
					trn_podetails.intPONo,
					trn_podetails.intPOYear 
					order by  
					trn_podetails.intPONo,
					trn_podetails.intPOYear , 
					MAINSTOCK.intGRNNo,
					MAINSTOCK.intGRNYear 
					asc
					)
					UNION 
					(
SELECT DISTINCT 
					MAINSTOCK.intItemId,
					date(MAINSTOCK.dtDate) as dtGRNDate,
					trn_podetails.intPONo,
					trn_podetails.intPOYear,
					trn_podetails.dblQty AS poQty,
					'' as intGRNNo,
					'' as intGRNYear,

					0 as grnFlag,
					round((select abs(sum(ware_stocktransactions_bulk.dblQty)) FROM ware_stocktransactions_bulk WHERE ware_stocktransactions_bulk.strType =  'ITEMCREATION+' AND ware_stocktransactions_bulk.intGRNNo=MAINSTOCK.intGRNNo AND ware_stocktransactions_bulk.intGRNYear=MAINSTOCK.intGRNYear AND ware_stocktransactions_bulk.intItemId=MAINSTOCK.intItemId AND MAINSTOCK.intLocationId=ware_stocktransactions_bulk.intLocationId),2) as grnQty, 
					1 as flagLocation,
					
					round((select abs(sum(ware_stocktransactions_bulk.dblQty)) FROM ware_stocktransactions_bulk WHERE ware_stocktransactions_bulk.strType =  'ISSUE' AND ware_stocktransactions_bulk.intGRNNo=MAINSTOCK.intGRNNo AND ware_stocktransactions_bulk.intGRNYear=MAINSTOCK.intGRNYear AND ware_stocktransactions_bulk.intItemId=MAINSTOCK.intItemId AND MAINSTOCK.intLocationId=ware_stocktransactions_bulk.intLocationId),2) as issueQty, 
					
					round((select abs(sum(ware_stocktransactions_bulk.dblQty)) FROM ware_stocktransactions_bulk WHERE ware_stocktransactions_bulk.strType =  'RETSTORES' AND ware_stocktransactions_bulk.intGRNNo=MAINSTOCK.intGRNNo AND ware_stocktransactions_bulk.intGRNYear=MAINSTOCK.intGRNYear AND ware_stocktransactions_bulk.intItemId=MAINSTOCK.intItemId AND MAINSTOCK.intLocationId=ware_stocktransactions_bulk.intLocationId),2) as returnStoresQty, 
					
					round((select abs(sum(ware_stocktransactions_bulk.dblQty)) FROM ware_stocktransactions_bulk WHERE ware_stocktransactions_bulk.strType =  'RTSUP' AND ware_stocktransactions_bulk.intGRNNo=MAINSTOCK.intGRNNo AND ware_stocktransactions_bulk.intGRNYear=MAINSTOCK.intGRNYear AND ware_stocktransactions_bulk.intItemId=MAINSTOCK.intItemId AND MAINSTOCK.intLocationId=ware_stocktransactions_bulk.intLocationId),2) as returnToSupQty, 
					
					round((select abs(sum(ware_stocktransactions_bulk.dblQty)) FROM ware_stocktransactions_bulk WHERE ware_stocktransactions_bulk.strType =  'GATEPASS' AND ware_stocktransactions_bulk.intGRNNo=MAINSTOCK.intGRNNo AND ware_stocktransactions_bulk.intGRNYear=MAINSTOCK.intGRNYear AND ware_stocktransactions_bulk.intItemId=MAINSTOCK.intItemId AND MAINSTOCK.intLocationId=ware_stocktransactions_bulk.intLocationId),2) as gatepassQty, 
					
					round((select abs(sum(ware_stocktransactions_bulk.dblQty)) FROM ware_stocktransactions_bulk WHERE ware_stocktransactions_bulk.strType =  'TRANSFERIN' AND ware_stocktransactions_bulk.intGRNNo=MAINSTOCK.intGRNNo AND ware_stocktransactions_bulk.intGRNYear=MAINSTOCK.intGRNYear AND ware_stocktransactions_bulk.intItemId=MAINSTOCK.intItemId AND MAINSTOCK.intLocationId=ware_stocktransactions_bulk.intLocationId),2) as transferInQty, 
					
					round((select abs(sum(ware_stocktransactions_bulk.dblQty)) FROM ware_stocktransactions_bulk WHERE ware_stocktransactions_bulk.strType =  'ALLOCATE' AND ware_stocktransactions_bulk.intGRNNo=MAINSTOCK.intGRNNo AND ware_stocktransactions_bulk.intGRNYear=MAINSTOCK.intGRNYear AND ware_stocktransactions_bulk.intItemId=MAINSTOCK.intItemId AND MAINSTOCK.intLocationId=ware_stocktransactions_bulk.intLocationId),2) as allocatedQty, 
					
					round((select abs(sum(ware_stocktransactions_bulk.dblQty)) FROM ware_stocktransactions_bulk WHERE ware_stocktransactions_bulk.strType =  'UNALLOCATE' AND ware_stocktransactions_bulk.intGRNNo=MAINSTOCK.intGRNNo AND ware_stocktransactions_bulk.intGRNYear=MAINSTOCK.intGRNYear AND ware_stocktransactions_bulk.intItemId=MAINSTOCK.intItemId AND MAINSTOCK.intLocationId=ware_stocktransactions_bulk.intLocationId),2) as unAllocatedQty, 
					
					round((select abs(sum(ware_stocktransactions_bulk.dblQty)) FROM ware_stocktransactions_bulk WHERE ware_stocktransactions_bulk.strType =  'INVOICE' AND ware_stocktransactions_bulk.intGRNNo=MAINSTOCK.intGRNNo AND ware_stocktransactions_bulk.intGRNYear=MAINSTOCK.intGRNYear AND ware_stocktransactions_bulk.intItemId=MAINSTOCK.intItemId AND MAINSTOCK.intLocationId=ware_stocktransactions_bulk.intLocationId),2) as invoiceQty, 

					round((select abs(sum(ware_stocktransactions_bulk.dblQty)) FROM ware_stocktransactions_bulk WHERE ware_stocktransactions_bulk.strType =  'RTSUP_RCV' AND ware_stocktransactions_bulk.intGRNNo=MAINSTOCK.intGRNNo AND ware_stocktransactions_bulk.intGRNYear=MAINSTOCK.intGRNYear AND ware_stocktransactions_bulk.intItemId=MAINSTOCK.intItemId AND MAINSTOCK.intLocationId=ware_stocktransactions_bulk.intLocationId),2) as retSupRcvQty, 

					round((select abs(sum(ware_stocktransactions_bulk.dblQty)) FROM ware_stocktransactions_bulk WHERE ware_stocktransactions_bulk.strType IN  ('Adjust Q+','Adjust R+','Adjust RQ+') AND ware_stocktransactions_bulk.intGRNNo=MAINSTOCK.intGRNNo AND ware_stocktransactions_bulk.intGRNYear=MAINSTOCK.intGRNYear AND ware_stocktransactions_bulk.intItemId=MAINSTOCK.intItemId AND MAINSTOCK.intLocationId=ware_stocktransactions_bulk.intLocationId),2) as adjustPlusQty, 
					
					round((select abs(sum(ware_stocktransactions_bulk.dblQty)) FROM ware_stocktransactions_bulk WHERE ware_stocktransactions_bulk.strType =  'Adjust Q-' AND ware_stocktransactions_bulk.intGRNNo=MAINSTOCK.intGRNNo AND ware_stocktransactions_bulk.intGRNYear=MAINSTOCK.intGRNYear AND ware_stocktransactions_bulk.intItemId=MAINSTOCK.intItemId AND MAINSTOCK.intLocationId=ware_stocktransactions_bulk.intLocationId),2) as adjustMinusQty, 

					round((select abs(sum(ware_stocktransactions_bulk.dblQty)) FROM ware_stocktransactions_bulk WHERE ware_stocktransactions_bulk.strType =  'ITEMCREATION-' AND ware_stocktransactions_bulk.intGRNNo=MAINSTOCK.intGRNNo AND ware_stocktransactions_bulk.intGRNYear=MAINSTOCK.intGRNYear AND ware_stocktransactions_bulk.intItemId=MAINSTOCK.intItemId AND MAINSTOCK.intLocationId=ware_stocktransactions_bulk.intLocationId),2) as allocToNewQty, 
					
					round((select abs(sum(ware_stocktransactions_bulk.dblQty)) FROM ware_stocktransactions_bulk WHERE ware_stocktransactions_bulk.strType =  'Dispose' AND ware_stocktransactions_bulk.intGRNNo=MAINSTOCK.intGRNNo AND ware_stocktransactions_bulk.intGRNYear=MAINSTOCK.intGRNYear AND ware_stocktransactions_bulk.intItemId=MAINSTOCK.intItemId AND MAINSTOCK.intLocationId=ware_stocktransactions_bulk.intLocationId),2) as disposedQty, 
			
					round((select sum(ware_stocktransactions_bulk.dblQty) FROM ware_stocktransactions_bulk WHERE   ware_stocktransactions_bulk.intGRNNo=MAINSTOCK.intGRNNo AND ware_stocktransactions_bulk.intGRNYear=MAINSTOCK.intGRNYear AND ware_stocktransactions_bulk.intItemId=MAINSTOCK.intItemId AND MAINSTOCK.intLocationId=ware_stocktransactions_bulk.intLocationId),2) as stockBalance, 



					
					IFNULL(mst_supplier.strName,'OPEN STOCK') AS supplierName
					FROM
					ware_stocktransactions_bulk as MAINSTOCK
					left Join ware_grnheader ON ware_grnheader.intGrnNo = MAINSTOCK.intGRNNo AND ware_grnheader.intGrnYear = MAINSTOCK.intGRNYear
					left Join trn_podetails ON trn_podetails.intPONo = ware_grnheader.intPoNo AND trn_podetails.intPOYear = ware_grnheader.intPoYear AND MAINSTOCK.intItemId = trn_podetails.intItem
					left Join trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
					left Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
				
					WHERE 
					MAINSTOCK.intGRNNo IN (0) AND 
					/*(MAINSTOCK.strType =  'GRN' OR MAINSTOCK.strType =  'OPENSTOCK')  AND*/
					MAINSTOCK.intLocationId =  '$location' AND
					MAINSTOCK.intItemId =  '$item' 
					
					$datePara2
					
					GROUP BY
					MAINSTOCK.intGRNNo,
					MAINSTOCK.intGRNYear 
					order by  
					trn_podetails.intPONo,
					trn_podetails.intPOYear , 
					MAINSTOCK.intGRNNo,
					MAINSTOCK.intGRNNo 
					asc					)
					
					";
				 	// echo $sql;
			$result = $db->RunQuery($sql);
			$i=0;//RTSUP
			$totalQty=0;
			while($row=mysqli_fetch_array($result))
			{
				$grn=$row['intGRNNo'].'/'.$row['intGRNYear'];
				if($row['flagLocation']==0) 
				$grn="(".$row['intGRNNo'].'/'.$row['intGRNYear'].")";
	  ?>
            <tr bgcolor="<?php if(($i++ % 2)==1){echo '#FCEDCD';} ?>">
              <?php
			  if($row['grnFlag']==0){
				$grn='';  
			  ?>
              <td colspan="3" class="normalfntMid">Item Creations</td>
              <?php
			  }
			  else{
			  ?>
              <td class="normalfntMid"><?php echo $row['dtGRNDate']; ?></td>
              <td class="normalfntMid"><?php echo $row['intPOYear'].'/'.$row['intPONo']; ?></td>
              <td class="normalfntMid"><?php echo $row['poQty']; ?></td>
              <?php
			  }
			  
				
			  
			  ?>
              
              <td class="normalfntMid"><?php echo $grn; ?></td>
              <td class="normalfntMid" style="color:#060"><?php echo  $row['grnQty']; ?></td>
              <td class="normalfntMid" style="color:#06F"><?php echo  $row['issueQty']; ?></td>
              <td class="normalfntMid" style="color:#060"><?php echo  $row['returnStoresQty']; ?></td>
              <td class="normalfntMid" style="color:#06F"><?php  echo $row['returnToSupQty']; ?></td>
              <td class="normalfntMid" style="color:#06F"><?php  echo $row['gatepassQty']; ?></td>
              <td class="normalfntMid" style="color:#060"><?php echo  $row['transferInQty']; ?></td>
              <td class="normalfntMid" style="color:#06F"><?php  echo $row['allocatedQty']; ?></td>
              <td class="normalfntMid" style="color:#060"><?php echo  $row['unAllocatedQty']; ?></td>
              <td class="normalfntMid" style="color:#060"><span class="normalfntMid" style="color:#060"><?php echo  $row['invoiceQty']; ?></span></td>
              <td class="normalfntMid" style="color:#060"><span class="normalfntMid" style="color:#060"><?php echo  $row['retSupRcvQty']; ?></span></td>
              <td class="normalfntMid" style="color:#060"><span class="normalfntMid" style="color:#060"><?php echo  $row['adjustPlusQty']; ?></span></td>
              <td class="normalfntMid" style="color:#060"><span class="normalfntMid" style="color:#060"><?php echo  $row['adjustMinusQty']; ?></span></td>
              <td class="normalfntMid" style="color:#060"><span class="normalfntMid" style="color:#060"><?php echo  $row['allocToNewQty']; ?></span></td>
              <td class="normalfntMid" style="color:#060"><span class="normalfntMid" style="color:#060"><?php echo  $row['disposedQty']; ?></span></td>
            
              <td class="normalfntMid" style="color:#060"><strong><?php echo  $row['stockBalance'];?></strong></td>
            </tr>
            
    <?php
	$totalQty+=$row['stockBalance']; 
			}
			
	?>
    
  <?php
$sql1 = "SELECT
											sum(ware_stocktransactions_bulk.dblQty) as balQty
										FROM ware_stocktransactions_bulk
										WHERE
											ware_stocktransactions_bulk.intItemId =  '$item' and intLocationId='$location'
											$paraDate
										";
								$result1 = $db->RunQuery($sql1);
								$row1=mysqli_fetch_array($result1);	  ?>  
    
    
    			<tr bgcolor="#FCEDCD">
              <td colspan="11" bgcolor="#E3F8D1" class="normalfntMid">&nbsp;</td>
              <td bgcolor="#E3F8D1" class="normalfntMid" style="color:#060">&nbsp;</td>
              <td bgcolor="#E3F8D1" class="normalfntMid" style="color:#060">&nbsp;</td>
              <td bgcolor="#E3F8D1" class="normalfntMid" style="color:#060">&nbsp;</td>
              <td bgcolor="#E3F8D1" class="normalfntMid" style="color:#060">&nbsp;</td>
              <td bgcolor="#E3F8D1" class="normalfntMid" style="color:#060">&nbsp;</td>
              <td bgcolor="#E3F8D1" class="normalfntMid" style="color:#060">&nbsp;</td>
              <td bgcolor="#E3F8D1" class="normalfntMid" style="color:#060"><span class="normalfntMid" style="color:#060">Total</span></td>
              <td bgcolor="#E3F8D1" class="normalfntMid" style="color:#060;font-size:16px;border-top:groove;border-bottom:double"><strong><?php echo round($row1['balQty'],4); ?></strong></td>
            </tr>
  </table></td>
</tr>

<tr height="40">
  <td align="center" class="normalfntMid">&nbsp;</td>
</tr>
</table>
</div>        
</form>
</body>
</html>
	
var basePath	="presentation/warehouse/gatepassTransferIn/addNew/";
var reportId	="902";
			
$(document).ready(function() {
	
  		$("#frmGatePassTransferin").validationEngine();
		$('#frmGatePassTransferin #cboDepartment').focus();
		$('#frmGatePassTransferinPopup #butAdd').die('click').live('click',addClickedRows);
		$('#frmGatePassTransferinPopup #butClose1').die('click').live('click',disablePopup);
		$('#frmGatePassTransferin #cboGPNo').die('change').live('change',loadData);
		
		$("#frmGatePassTransferin #butAddItems").click(function(){
			if(!$('#frmGatePassTransferin #cboGPNo').val()){
				alert("Please select the Gatepass No");
				return false;				
			}
			else if(!$('#frmGatePassTransferin #cboGPYear').val()){
				alert("Please select the Year");
				return false;				
			}
			//clearRows();
			closePopUp();
			loadPopup();
		});

		$("#frmGatePassTransferin #cboGPYear").change(function(){
			loadGPNos();
		});
    //-------------------------------------------------------
	
	//-------------------------------------------- 
				 $('#frmGatePassTransferin  #chkAll').click(function(){
						if(document.getElementById('chkAll').checked==true)
						 var chk=true;
						else
						 var chk=false;
						
						var rowCount = document.getElementById('tblGatepassTransferInItems').rows.length;
						for(var i=1;i<rowCount;i++)
						{
							document.getElementById('tblGatepassTransferInItems').rows[i].cells[0].childNodes[0].checked=chk;
						}
					});
				//-------------------------------------------- 
 
  $('#frmGatePassTransferin #butSave').click(function(){
	var requestType = '';
	if ($('#frmGatePassTransferin').validationEngine('validate'))   
    { 

		var data = "requestType=save";
		
			data+="&serialNo="+$('#txtGpTrnsfNo').val();
			data+="&Year="+$('#txtGpTrnsfYear').val();
			data+="&gpNo="	+	$('#cboGPNo').val();
			data+="&gpYear="	+	$('#cboGPYear').val();
			data+="&note="	+	URLEncode_json($('#txtNote').val());
			data+="&date="			+	$('#dtDate').val();


			var rowCount = document.getElementById('tblGatepassTransferInItems').rows.length;
			if(rowCount==1){
				alert("items not selected to Transferin");
				return false;				
			}
			var row = 0;
			var errorQtyFlag=0;
			var flagQty	=0;
			
			var arr="[";
			
			$('#tblGatepassTransferInItems .item').each(function(){
	
				var orderNo	= $(this).parent().find(".orderNo").html();
				var salesOrderId	= $(this).parent().find(".salesOrderNo").attr('id');
				var itemId	= $(this).attr('id');
				var Qty	= $(this).parent().find(".Qty").val();
					
					if(Qty>0 && $(this).parent().find(".chkAll").attr('checked')){
						flagQty	=1;
						arr += "{";
						arr += '"orderNo":"'+	orderNo +'",' ;
						arr += '"salesOrderId":"'+		salesOrderId +'",' ;
						arr += '"itemId":"'+	itemId +'",' ;
						arr += '"Qty":"'+		Qty +'"' ;
						arr +=  '},';
						
					}
					if(Qty<=0  && $(this).parent().find(".chkAll").attr('checked')){
						errorQtyFlag=1;
					}
			});
			if(flagQty==0){
				alert("Please select atleast one record");
				return false;
			}
			if(errorQtyFlag==1){
				alert("Please Enter Quantities");
				return false;
			}
			
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;

		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = basePath+"gatepassTransferIn-db-set.php";
     	var obj = $.ajax({
			url:url,
			type:'post',
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmGatePassTransferin #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						//$('#frmSampleInfomations').get(0).reset();
						//saveImage(json.sampleNo,json.sampleYear,json.revisionNo);
						//loadCombo_frmSampleInfomations();
						var t=setTimeout("alertx()",1000);
						$('#txtGpTrnsfNo').val(json.serialNo);
						$('#txtGpTrnsfYear').val(json.year);
						if(json.confirmationMode==1){
							$('#butConfirm').show();
						}
						else{
							$('#butConfirm').hide();
						}
						return;
					}
				},
			error:function(xhr,status){
					
					$('#frmGatePassTransferin #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
   
	
	//--------------refresh the form----------
	$('#frmGatePassTransferin #butNew').click(function(){
		window.location.href = "?q=252";
		$('#frmGatePassTransferin').get(0).reset();
		clearRows();
		$('#frmGatePassTransferin #txtGrnNo').val('');
		$('#frmGatePassTransferin #txtGrnYear').val('');
		$('#frmGatePassTransferin #txtInvioceNo').val('');
		$('#frmGatePassTransferin #cboPO').val('');
		$('#frmGatePassTransferin #txtDeliveryNo').val('');
		$('#frmGatePassTransferin #txtTotGrnAmmount').val('');
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmGatePassTransferin #dtDate').val(d);
	});
	//----------------------------------------
	
//----------------------------	
$('#frmGatePassTransferin .delImg').click(function(){
	$(this).parent().parent().remove();
});

//----------------------------	
//-----------------------------------
$('#frmGatePassTransferin #butReport').click(function(){
	if($('#txtGpTrnsfNo').val()!=''){
		window.open('?q='+reportId+'&transfNo='+$('#txtGpTrnsfNo').val()+'&year='+$('#txtGpTrnsfYear').val());	
	}
	else{
		alert("There is no Transfer IN No to view");
	}
});
//----------------------------------	
$('#frmGatePassTransferin #butConfirm').click(function(){
	if($('#txtGpTrnsfNo').val()!=''){
		window.open('?q='+reportId+'&transfNo='+$('#txtGpTrnsfNo').val()+'&year='+$('#txtGpTrnsfYear').val()+'&approveMode=1');	
	}
	else{
		alert("There is no Transfer IN No to confirm");
	}
});
//-----------------------------------------------------
$('#frmGatePassTransferin #butClose').click(function(){
		//load('main.php');	
});
});

//----------end of ready -------------------------------

//-------------------------------------------------------
function loadPopup()
{
			closePopUp();
		popupWindow3('1');
		var gpNo = $('#cboGPNo').val();
		var year = $('#cboGPYear').val();
		$('#popupContact1').load(basePath+'gatepassTransferInPopup.php?gpNo='+gpNo+"&year="+year,function(){
				 //-------------------------------------------- 
				  $('#frmGatePassTransferinPopup #cboOrderNo').change(function(){
						var orderNo = $('#cboOrderNo').val();
						var url 		= basePath+"gatepassTransferIn-db-get.php?requestType=loadStyleNo&orderNo="+orderNo;
						var httpobj 	= $.ajax({url:url,async:false})
						document.getElementById('cbStyleNo').innerHTML=httpobj.responseText;
						
				  });
				 //-------------------------------------------- 
				  $('#frmGatePassTransferinPopup #cboMainCategory').change(function(){
						var mainCategory = $('#cboMainCategory').val();
						var url 		= basePath+"gatepassTransferIn-db-get.php?requestType=loadSubCategory&mainCategory="+mainCategory;
						var httpobj 	= $.ajax({url:url,async:false})
						document.getElementById('cboSubCategory').innerHTML=httpobj.responseText;
						
				  });
				 //-------------------------------------------- 
				 $('#frmGatePassTransferinPopup  #chkAll').click(function(){
						if(document.getElementById('chkAll').checked==true)
						 var chk=true;
						else
						 var chk=false;
						
						var rowCount = document.getElementById('tblItemsPopup').rows.length;
						for(var i=1;i<rowCount;i++)
						{
							document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].checked=chk;
						}
					});
				//-------------------------------------------- 
				  $('#frmGatePassTransferinPopup #imgSearchItems').click(function(){
						var rowCount = document.getElementById('tblItemsPopup').rows.length;
						for(var i=1;i<rowCount;i++)
						{
							document.getElementById('tblItemsPopup').deleteRow(1);
						}
						var orderNo = $('#cboOrderNo').val();
						var salesOrderId = $('#cbStyleNo').val();
						var salesOrderNo = $('#cbStyleNo option:selected').text();
						var mainCategory = $('#cboMainCategory').val();
						var subCategory = $('#cboSubCategory').val();
						var description = $('#txtItmDesc').val();
						
						/*if(orderNo==''){
								alert("Please select Order No");
								return false;
						}*/
						
						if(mainCategory==''){
								alert("Please select Main Category");
								return false;
						}
							
						showWaiting();			
							
						var url 		= basePath+"gatepassTransferIn-db-get.php?requestType=loadItems";
						var httpobj = $.ajax({
							url:url,
							dataType:'json',
							data:"orderNo="+orderNo+"&salesOrderId="+salesOrderId+"&mainCategory="+mainCategory+"&subCategory="+subCategory+"&description="+description+"&gpNo="+gpNo+"&year="+year,
							async:false,
							success:function(json){

								var length = json.arrCombo.length;
								var arrCombo = json.arrCombo;

								if(arrCombo[0]=='none'){
									alert("Items not found");
									return;
									//hideWaiting();
								}
								
								for(var i=0;i<length;i++)
								{
									var intOrderNo=arrCombo[i]['intOrderNo'];
									var intOrderYear=arrCombo[i]['intOrderYear'];
									var salesOrderNo=arrCombo[i]['salesOrderNo'];
									var salesOrderId=arrCombo[i]['salesOrderId'];
									var itemId=arrCombo[i]['itemId'];	
									var maincatId=arrCombo[i]['maincatId'];	
									var subCatId=arrCombo[i]['subCatId'];	
									var code=arrCombo[i]['code'];
                                                                        var supItemCode=arrCombo[i]['supItemCode'];
									var itemName=arrCombo[i]['itemName'];
                                                                        var itemHide=arrCombo[i]['itemHide'];
									var uom=arrCombo[i]['uom'];	
									var unitPrice=arrCombo[i]['unitPrice'];	
									var mainCatName=arrCombo[i]['mainCatName'];
									var subCatName=arrCombo[i]['subCatName'];
									var gpNo=arrCombo[i]['gpNo'];
									var gpQty=arrCombo[i]['gpQty'];
									var transfQty=arrCombo[i]['transfQty'];
									var balToTransfQty=parseFloat(gpQty)-parseFloat(transfQty);
									var stockBalQty=arrCombo[i]['stockBalQty'];
									
									if((intOrderNo==0) && (intOrderYear==0)){
										var orderNo='';
										salesOrderNo='';
										salesOrderId='';
									}
									else{
										var orderNo=intOrderNo+"/"+intOrderYear;
									}
									
									if(!salesOrderNo){
										salesOrderNo = '';
									}
									
									if(!salesOrderId){
										salesOrderId = '';
									}	
										
									var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF" id=""><input type="checkbox" id="chkDisp" /></td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'" class="mainCatNameP">'+mainCatName+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'" class="subCatNameP">'+subCatName+'</td>';
									if(supItemCode!=null)
                                                                        {
                                                                        content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="codeP">'+supItemCode+'</td>';   
                                                                        }else{
                                                                        content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="codeP">'+code+'</td>';
                                                                         }
                                                                        if(itemHide==1)
                                                                        {
                                                                        content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="itemP">*****</td>';    
                                                                        }else
                                                                        {
									content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="itemP">'+itemName+'</td>';
                                                                        }
									content +='<td align="center" bgcolor="#FFFFFF" id="'+uom+'" class="uomP">'+uom+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+orderNo+'" style="display:none"  class="orderNoP">'+orderNo+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+salesOrderId+'" style="display:none"  class="salesOrderNoP">'+salesOrderNo+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+gpNo+'" style="display:none"  class="gpNoP">'+gpNo+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+gpQty+'" class="gpQtyP" >'+gpQty+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+transfQty+'"  class="transfQtyeP">'+transfQty+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+balToTransfQty+'" class="balToTransfQtyP" >'+balToTransfQty+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+stockBalQty+'"   class="stockBalQtyP">'+stockBalQty+'</td></tr>';

									add_new_row('#frmGatePassTransferinPopup #tblItemsPopup',content);
								}
									checkAlreadySelected();

							}
						});hideWaiting();
						
				  });
			});	
}
//----------------------------------------------------
function addClickedRows()
{
	var i=0;
	$('#tblItemsPopup .itemP').each(function(){
		i++;
		if((document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].checked==true) && (document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].disabled==false)){
			

			var intOrderNo=$(this).parent().find(".orderNoP").attr('id');
			var salesOrderNo=$(this).parent().find(".salesOrderNoP").html();
			var salesOrderId=$(this).parent().find(".salesOrderNoP").attr('id');
			var itemId=$(this).attr('id');
			var maincatId=$(this).parent().find(".mainCatNameP").attr('id');
			var subCatId=$(this).parent().find(".subCatNameP").attr('id');
			var code=$(this).parent().find(".codeP").html();
			var uom=$(this).parent().find(".uomP").html();
			var itemName=$(this).parent().find(".itemP").html();
			var mainCatName=$(this).parent().find(".mainCatNameP").html();
			var subCatName=$(this).parent().find(".subCatNameP").html();
			
			var gpQty=$(this).parent().find(".gpQtyP").html();
			var transfQty=$(this).parent().find(".transfQtyeP").html();
			var stockBalance=$(this).parent().find(".stockBalQtyP").html();
			
			var balToTransfQty=gpQty-transfQty;
			var maxTrnsfQty=balToTransfQty;

			var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF"><img class="delImg" src="images/del.png" width="15" height="15" /></td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+intOrderNo+'" class="orderNo">'+intOrderNo+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+salesOrderId+'" class="salesOrderNo">'+salesOrderNo+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'" class="mainCatName">'+mainCatName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'" class="subCatName">'+subCatName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="item">'+itemName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+uom+'" class="uom">'+uom+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+gpQty+'" class="gpQty">'+gpQty+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+balToTransfQty+'" class="balToTransfQty">'+balToTransfQty+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id=""><input  id="'+maxTrnsfQty+'" class="validate[required,custom[number],max['+maxTrnsfQty+']] calculateValue Qty" style="width:80px;text-align:center" type="text" value="'+maxTrnsfQty+'"/></td>';
			content +='</tr>';
			
		
			add_new_row('#frmGatePassTransferin #tblGatepassTransferInItems',content);
		//----------------------------	
		$('.delImg').click(function(){
			$(this).parent().parent().remove();
		});
		
		//----------------------------
	}
	
	});
	disablePopup();
	closePopUp();
}
//-------------------------------------
function alertx()
{
	$('#frmGatePassTransferin #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmGatePassTransferin #butDelete').validationEngine('hide')	;
}

function closePopUp(){
	
}
//----------------------------------------------
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
//----------------------------------------------- 
function clearRows()
{
	//var rowCount = $('#tblDispatchPopup >tbody >tr').length;
	var rowCount = document.getElementById('tblGatepassTransferInItems').rows.length;
	//alert(rowCount);
	//var cellCount = document.getElementById('tblMain').rows[1].cells.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblGatepassTransferInItems').deleteRow(1);
			
	}
}
//------------------------------------------------------------
function checkAlreadySelected(){
	$('#tblGatepassTransferInItems .item').each(function(){

		var orderNo	= $(this).parent().find(".orderNo").html();
		var styleNo	= $(this).parent().find(".salesOrderNo").attr('id');
		var itemId	= $(this).attr('id');

			var j=0;
			$('#tblItemsPopup .itemP').each(function(){
				j++;
				var orderNoP=$(this).parent().find(".orderNoP").attr('id');
				var styleNoP=$(this).parent().find(".salesOrderNoP").attr('id');
				var itemIdP=$(this).attr('id');
				
			//alert(orderNo+"=="+orderNoP+"***"+styleNo+"=="+styleNoP+"***"+mrnNo+"=="+mrnNoP+"***"+itemId+"=="+itemIdP+"***")
				if((orderNo==orderNoP) && (styleNo==styleNoP) && (itemId==itemIdP)){
					document.getElementById('tblItemsPopup').rows[j].cells[0].childNodes[0].checked=true;
					document.getElementById('tblItemsPopup').rows[j].cells[0].childNodes[0].disabled=true;
				}
			});
	});
}
//------------------------------------------------------------
function loadGPNos(){
	var gpYear = $('#cboGPYear').val();
	var url 		= basePath+"gatepassTransferIn-db-get.php?requestType=loadGPNos&gpYear="+gpYear;
	var httpobj 	= $.ajax({url:url,async:false})
	document.getElementById('cboGPNo').innerHTML=httpobj.responseText;
}
//------------------------------------------------------------
function loadData(){
			window.location.href ='?q=252&gpNo='+$('#cboGPNo').val()+'&gpYear='+$('#cboGPYear').val();

}

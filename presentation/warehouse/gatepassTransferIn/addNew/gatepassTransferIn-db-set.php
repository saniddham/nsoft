<?php 
	ini_set('display_errors',0);
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$companyId 	= $_SESSION['CompanyID'];
	include "{$backwardseperator}dataAccess/Connector.php";
	require_once $backwardseperator."class/warehouse/cls_warehouse_get.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$objwhouseget = new cls_warehouse_get($db);
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	
	$serialNo 	 = $_REQUEST['serialNo'];
	$year 		 = $_REQUEST['Year'];
	$gpNo 	 = $_REQUEST['gpNo'];
	$gpYear 	 = $_REQUEST['gpYear'];
	$note 	 = replace1($_REQUEST['note']);
	$date 		 = $_REQUEST['date'];
	
	$arr 		= json_decode($_REQUEST['arr'], true);
	
	$programName='GatePass Transfer In';
	$programCode='P0252';

	$ApproveLevels = (int)getApproveLevel($programName);
	$transfApproveLevel = $ApproveLevels+1;
	
//------------save---------------------------	
	if($requestType=='save')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		$savableFlag= 1;
		
		if($serialNo==''){
			$serialNo 	= getNextTransfNo();
			$year = date('Y');
			$editMode=0;
			$savedStatus=$transfApproveLevel;
			$savedLevels=$ApproveLevels;
		}
		else{
			$savableFlag=getSaveStatus($serialNo,$year);
			$editMode=1;
			$sql = "SELECT
			ware_gatepasstransferinheader.intStatus, 
			ware_gatepasstransferinheader.intApproveLevels 
			FROM ware_gatepasstransferinheader 
			WHERE
			ware_gatepasstransferinheader.intGpTransfNo =  '$serialNo' AND
			ware_gatepasstransferinheader.intGpTransfYear =  '$year'";

			$result = $db->RunQuery2($sql);
			$row=mysqli_fetch_array($result);
			$savedStatus=$row['intStatus'];
			$savedLevels=$row['intApproveLevels'];
		}
		
		//--------------------------
		$response['arrData'] = $objwhouseget->getLocationValidation('TRANSFERIN',$companyId,$gpNo,$gpYear);//check for grn location
		//--------------------------
		$editPermission=loadEditMode($programCode,$savedStatus,$savedLevels,$userId);
		$locationEditFlag=getEditLocationValidation('TRANSFERIN',$companyId,$serialNo,$year);
		//--------------------------
		
		if($response['arrData']['type']=='fail'){
			 $rollBackFlag=1;
			 $rollBackMsg=$response['arrData']['msg'];
		 }
		else if($savableFlag==0){
			 $rollBackFlag=1;
			 $rollBackMsg="This Gate Pass Transfer In No is already confirmed.cant edit";
		 }
		 else if($editPermission==0){
			 $rollBackFlag=1;
			 $rollBackMsg="No Permission to save.";
		 }
		else if($editMode==1){//edit existing grn
		$sql = "UPDATE `ware_gatepasstransferinheader` SET intStatus ='$transfApproveLevel', 
												       intApproveLevels ='$ApproveLevels', 
												       strNote ='$note', 
													  intModifiedBy ='$userId', 
													  dtmModifiedDate =now() 
				WHERE (`intGpTransfNo`='$serialNo') AND (`intGpTransfYear`='$year')";
		$result = $db->RunQuery2($sql);
		}
		else{
			//$sql = "DELETE FROM `ware_gatepasstransferinheader` WHERE (`intGpTransfNo`='$serialNo') AND (`intGpTransfYear`='$year')";
			//$result1 = $db->RunQuery2($sql);

			$sql = "INSERT INTO `ware_gatepasstransferinheader` (`intGpTransfNo`,`intGpTransfYear`,intGatePassNo,intGatePassYear,strNote,intStatus,intApproveLevels,datdate,dtmCreateDate,intUser,intCompanyId) 
					VALUES ('$serialNo','$year','$gpNo','$gpYear','$note','$transfApproveLevel','$ApproveLevels',now(),now(),'$userId','$companyId')";
			$result = $db->RunQuery2($sql);
			$maxAppByStatus=(int)getMaxAppByStatus($serialNo,$year)+1;
			$sql = "UPDATE `ware_gatepasstransferinheader_approvedby` SET intStatus ='$maxAppByStatus' 
					WHERE (`intGpTransfNo`='$serialNo') AND (`intYear`='$year') AND (`intStatus`='0')";
			$result1 = $db->RunQuery2($sql);
		}
		//-----------delete and insert to detail table-----------------------
		if(($result) && ($rollBackFlag!=1)){
		$sql = "DELETE FROM `ware_gatepasstransferindetails` WHERE (`intGpTransfNo`='$serialNo') AND (`intGpTransfYear`='$year')";
		$result2 = $db->RunQuery2($sql);
			$toSave=0;
			$saved=0;
			$rollBackFlag=0;
			$rollBackMsg ="Maximum Qtys for items are..."; 
			foreach($arr as $arrVal)
			{
				$location 	= $_SESSION['CompanyID'];
				$company 	= $_SESSION['headCompanyId'];
				$orderNo 	 = $arrVal['orderNo'];
				$orderNoArray   = explode('/',$orderNo);
				if($orderNoArray[0]==''){
					$orderNoArray[0]=0;
				}
				if($orderNoArray[1]==''){
					$orderNoArray[1]=0;
				}
				$salesOrderId 		 = $arrVal['salesOrderId'];
				$itemId 	 = $arrVal['itemId'];
				$Qty 		 = $arrVal['Qty'];
		

				//------check maximum ISSUE Qty--------------------
				 $sqlc = "SELECT 
				mst_item.strName as itemName,
				tb1.intGatePassNo,
				tb1.intGatePassYear,
				tb1.strOrderNo,
				tb1.intOrderYear,
				tb1.strStyleNo,
				tb1.dblQty,
				IFNULL((SELECT
				Sum(ware_gatepasstransferindetails.dblQty)
				FROM
				ware_gatepasstransferinheader
				Inner Join ware_gatepasstransferindetails ON ware_gatepasstransferinheader.intGpTransfNo = ware_gatepasstransferindetails.intGpTransfNo AND ware_gatepasstransferinheader.intGpTransfYear = ware_gatepasstransferindetails.intGpTransfYear
				WHERE
				ware_gatepasstransferinheader.intGatePassNo =  tb1.intGatePassNo AND
				ware_gatepasstransferinheader.intGatePassYear =  tb1.intGatePassYear AND
				ware_gatepasstransferindetails.strOrderNo =  tb1.strOrderNo AND
				ware_gatepasstransferindetails.intOrderYear =  tb1.intOrderYear AND
				ware_gatepasstransferindetails.strStyleNo =  tb1.strStyleNo AND
				ware_gatepasstransferindetails.intItemId =  tb1.intItemId AND
				ware_gatepasstransferinheader.intStatus > '0' AND 
				ware_gatepasstransferinheader.intStatus <= ware_gatepasstransferinheader.intApproveLevels),0) as dblTransferQty
				FROM
				ware_gatepassdetails as tb1 
				Inner Join ware_gatepassheader ON tb1.intGatePassNo = ware_gatepassheader.intGatePassNo AND tb1.intGatePassYear = ware_gatepassheader.intGatePassYear
				Inner Join mst_item ON tb1.intItemId = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
				WHERE 
				ware_gatepassheader.intStatus = '1' AND 
				ware_gatepassheader.intGatePassNo =  '$gpNo' AND 
				ware_gatepassheader.intGatePassYear =  '$gpYear' AND 
				tb1.intItemId =  '$itemId' ";
				
				if($orderNoArray[0]!='')
				$sqlc.=" AND tb1.strOrderNo =  '$orderNoArray[0]'";
				if($orderNoArray[1]!='')
				$sqlc.=" AND tb1.intOrderYear =  '$orderNoArray[1]'";
				if($salesOrderId!='')
				$sqlc.=" AND tb1.strStyleNo =  '$salesOrderId'";		
				$sql_gp	=$sqlc;			
				//echo $sqlc;
				$resultsc = $db->RunQuery2($sqlc);
				$rowc = mysqli_fetch_array($resultsc);
				$item=$rowc['itemName'];

				//if($rowc['dblQty']-$rowc['dblIssudQty']<$stockBalQty)
				$balQtyToTransf = $rowc['dblQty']-$rowc['dblTransferQty'];
				if($editMode==1){
					 $balQtyToTransf=$balQtyToTransf+$Qty;
				//	$balQtyToTransf +=	$Qty;
				}
			//	else
				//$balQtyToIssue = $stockBalQty;
				if($Qty>$balQtyToTransf){
				//	call roll back--------****************
					$rollBackFlag=1;
					if($rowc['strOrderNo']!=0){
					$rollBackMsg .="\n ".$orderNoArray[0]."/".$orderNoArray[1]."-".$salesOrderId."-".$item." =".$balQtyToTransf;
					}
					else{
					$rollBackMsg .="\n "."-".$item." =".$balQtyToTransf;
					}
					//exit();
				}

				//----------------------------
				if($rollBackFlag!=1){
				if($orderNoArray[0]==''){
					$orderNoArray[0]=0;
				}
				if($orderNoArray[1]==''){
					$orderNoArray[1]=0;
				}
					$Qty=round($Qty,4);
					$sql = "INSERT INTO `ware_gatepasstransferindetails` (`intGpTransfNo`,`intGpTransfYear`,`strOrderNo`,`intOrderYear`,`strStyleNo`,`intItemId`,`dblQty`) 
					VALUES ('$serialNo','$year','$orderNoArray[0]','$orderNoArray[1]','$salesOrderId','$itemId','$Qty')";
					$result3 = $db->RunQuery2($sql);
					if($result3==1){
					$saved++;
					}
				}
				$toSave++;
			}
		}
		//echo $rollBackFlag;
		
		//$rollBackFlag=1;
		
		if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= $sql_gp;
		}
		else if(($savableFlag==1) &&(!$result)){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= "Header saving error";
			$response['q'] 			= $sql;
		}
		else if(($savableFlag==1) &&($toSave==0) &&($saved==0)){
			$response['type'] 		= 'fail';
			$response['msg'] 		= "Details saving error";
			$response['q'] 			= '';
		}
		else if(($result) && ($toSave==$saved)){
			$confirmationMode=getConfirmationMode($programCode,$userId,$ApproveLevels,$transfApproveLevel);
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			if($editMode==1)
			$response['msg'] 		= 'Updated successfully.';
			else
			$response['msg'] 		= 'Saved successfully.';
			
			$response['serialNo'] 		= $serialNo;
			$response['year'] 		= $year;
			$response['confirmationMode'] = $confirmationMode;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		
		$db->CloseConnection();		
	}
//----------------------------------------
	echo json_encode($response);
//-----------------------------------------	
	function getNextTransfNo()
	{
		global $db;
		global $companyId;
		 $sql = "SELECT
				sys_no.intGpTransfNo
				FROM sys_no
				WHERE
				sys_no.intCompanyId =  '$companyId'
				";	
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$nextTransfNo = $row['intGpTransfNo'];
		
		$sql = "UPDATE `sys_no` SET intGpTransfNo=intGpTransfNo+1 WHERE (`intCompanyId`='$companyId')  ";
		$db->RunQuery2($sql);
		
		return $nextTransfNo;
	}
	//-----------------------------------------------------------
	function getStockBalance($company,$location,$orderNo,$orderYear,$salesOrderId,$item)
	{
		global $db;
		 $sql = "SELECT
				Sum(ware_stocktransactions.dblQty) as stockBal 
				FROM ware_stocktransactions
				WHERE
				ware_stocktransactions.intCompanyId =  '$company' AND
				ware_stocktransactions.intLocationId =  '$location' AND
				ware_stocktransactions.strOrderNo =  '$orderNo' AND
				ware_stocktransactions.intOrderYear =  '$orderYear' AND
				ware_stocktransactions.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions.intItemId =  '$item'";

		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
	function getStockBalance_bulk($location,$item)
	{
		global $db;
		   $sql = "SELECT
				Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$location'
				GROUP BY
				ware_stocktransactions_bulk.intItemId";

		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
	function getSaveStatus($serialNo,$year)
	{
		global $db;
		$sql = "SELECT ware_gatepasstransferinheader.intStatus, ware_gatepasstransferinheader.intApproveLevels FROM ware_gatepasstransferinheader WHERE (intGpTransfNo='$serialNo') AND (`intGpTransfYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
			
			if(($status==0) || ($row['intStatus']==($row['intApproveLevels']+1))){
				$editableFlag=1	;
			}
			else{
				$editableFlag=0;	
			}
			
		return $editableFlag;
	}
//-----------------------------------------------------------
	function getEditLocationValidation($type,$location,$serialNo,$year)
	{
		$flag= 1;
		
		global $db;
		  $sql = "SELECT
					ware_gatepasstransferinheader.intCompanyId
					FROM
					ware_gatepasstransferinheader
					Inner Join mst_locations ON ware_gatepasstransferinheader.intCompanyId = mst_locations.intId
					WHERE
					ware_gatepasstransferinheader.intGpTransfNo =  '$serialNo' AND
					ware_gatepasstransferinheader.intGpTransfNo =  '$year'";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			if($location == $row['intCompanyId'])
			$flag=0;
 			
		return $flag;

}
//------------------------------function loadConfirmatonMode-------------------
function getConfirmationMode($programCode,$intUser,$approveLevels,$status){
		global $db;
		$k=$approveLevels+2-$status;
		$sqlp = "SELECT 
		menupermision.intEdit, 
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		if($rowp['int'.$k.'Approval']==1) 
		 $confirmatonMode=1; 
		else
		 $confirmatonMode=0; 
	 
	return $confirmatonMode;
}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
 	 	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		$resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
		 
	//	echo $editMode;
		 
	return $editMode;
}
//------------------------------function get MaxAppByStatus---------------------
function getMaxAppByStatus($serialNo,$year){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	  $sql = "SELECT
				max(ware_gatepasstransferinheader_approvedby.intStatus) as status 
				FROM
				ware_gatepasstransferinheader_approvedby
				WHERE
				ware_gatepasstransferinheader_approvedby.intGpTransfNo =  '$serialNo' AND
				ware_gatepasstransferinheader_approvedby.intYear =  '$year'";
				
		$resultm = $db->RunQuery2($sql);
		 $rowm=mysqli_fetch_array($resultm);
		 
		 return $rowm['status'];

}
//--------------------------------------------------------getMaxAppByStatus
    function replace1($str)
{
	  $str 	= 	str_replace('\\', "\\\\",	$str);
	  $str 	= 	str_replace("'","\'",	$str);
	  $str	=	str_replace("\n", "<br>",$str);  
	  
	  return $str;
}	


?>



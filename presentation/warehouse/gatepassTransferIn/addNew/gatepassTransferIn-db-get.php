<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	///
	/////////// type of print load part /////////////////////
if($requestType=='loadGPNos')
	{
		$gpYear  = $_REQUEST['gpYear'];
		
		
		$sql = "select * from (SELECT
							ware_gatepassheader.intGatePassNo, 
sum(ware_gatepassdetails.dblQty) as gpQty, 
sum(ware_gatepassdetails.dblTransferQty) as transfQty 

							FROM ware_gatepassheader 
							Inner Join ware_gatepassdetails ON ware_gatepassheader.intGatePassNo = ware_gatepassdetails.intGatePassNo AND ware_gatepassheader.intGatePassYear = ware_gatepassdetails.intGatePassYear
							WHERE
				ware_gatepassheader.intStatus =  '1' AND 
				ware_gatepassheader.intGPToLocation =  '$location' ";
		if($gpYear!=''){		
		$sql .= " AND ware_gatepassheader.intGatePassYear =  '$gpYear' ";	
		}
		$sql .= " GROUP BY 
							ware_gatepassdetails.intGatePassNo,
							ware_gatepassdetails.intGatePassYear
							order by intGatePassNo desc ) as tb1 where  tb1.gpQty-tb1.transfQty >0";
		
		
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intGatePassNo']."\">".$row['intGatePassNo']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
else if($requestType=='loadSubCategory')
	{
		$mainCategory  = $_REQUEST['mainCategory'];
		$sql = "SELECT
				mst_subcategory.intId,
				mst_subcategory.strCode,
				mst_subcategory.strName
				FROM mst_subcategory
				WHERE
				mst_subcategory.intMainCategory =  '$mainCategory' 
				Order by mst_subcategory.strName ASC";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	else if($requestType=='loadStyleNo')
	{
		$orderNo  = $_REQUEST['orderNo'];
		$orderNoArray 	 = explode('/',$orderNo);
		 /*$sql = "SELECT DISTINCT
				ware_stocktransactions.intSalesOrderId,
				trn_orderdetails.strSalesOrderNo, 
				mst_part.strName
				FROM
				ware_stocktransactions
				Inner Join trn_orderdetails ON ware_stocktransactions.intOrderNo = trn_orderdetails.intOrderNo AND ware_stocktransactions.intOrderYear = trn_orderdetails.intOrderYear AND ware_stocktransactions.intSalesOrderId = trn_orderdetails.intSalesOrderId 
				Inner Join mst_part ON trn_orderdetails.intPart = mst_part.intId 
				WHERE 
				ware_stocktransactions.intOrderNo =  '$orderNoArray[0]' AND
				ware_stocktransactions.intOrderYear =  '$orderNoArray[1]'";*/
		$sql = "SELECT DISTINCT
				trn_orderdetails.intSalesOrderId,
				trn_orderdetails.strSalesOrderNo, 
				mst_part.strName
				FROM
				trn_orderdetails
				Inner Join trn_orderheader ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear  
				Inner Join mst_part ON trn_orderdetails.intPart = mst_part.intId 
				WHERE 
				trn_orderheader.intOrderNo =  '$orderNoArray[0]' AND
				trn_orderheader.intOrderYear =  '$orderNoArray[1]'";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intSalesOrderId']."\">".$row['strSalesOrderNo']."/".$row['strName']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	else if($requestType=='loadItems')
	{
		$gpNo  = $_REQUEST['gpNo'];
		$year  = $_REQUEST['year'];
		$orderNo  = $_REQUEST['orderNo'];
		$orderNoArray 	 = explode('/',$orderNo);
		$styleNo  = $_REQUEST['salesOrderId'];
		$mrnNo  = $_REQUEST['mrnNo'];
		$mrnNoArray 	 = explode('/',$mrnNo);
		$mainCategory  = $_REQUEST['mainCategory'];
		$subCategory  = $_REQUEST['subCategory'];
		$description  = $_REQUEST['description'];
		

		if($styleNo=='null'){
			$styleNo='';
		}
		if($orderNoArray[0]==''){
			$orderNoArray[0]=0;
		}
		if($orderNoArray[1]==''){
			$orderNoArray[1]=0;
		}
		
		  $sql="select * from (SELECT
				mst_item.intId,
				mst_item.intMainCategory,
				mst_item.intSubCategory,
				mst_item.strCode,
				mst_item.strName as itemName,
				mst_item.intBomItem,
				mst_item.intUOM,
                                mst_item.ITEM_HIDE,
                                mst_item.strCode as SUP_ITEM_CODE,
				mst_units.strCode as uom , 
				mst_item.dblLastPrice,
				mst_maincategory.strName as mainCatName,
				mst_subcategory.strName as subCatName,   
				tb1.strOrderNo,
				tb1.intOrderYear,
				tb1.strStyleNo,
				tb1.dblQty,
				tb1.intGatePassNo,
				tb1.intGatePassYear, 
				IFNULL((SELECT
				Sum(ware_gatepasstransferindetails.dblQty)
				FROM
				ware_gatepasstransferinheader
				Inner Join ware_gatepasstransferindetails ON ware_gatepasstransferinheader.intGpTransfNo = ware_gatepasstransferindetails.intGpTransfNo AND ware_gatepasstransferinheader.intGpTransfYear = ware_gatepasstransferindetails.intGpTransfYear
				WHERE
				ware_gatepasstransferinheader.intGatePassNo =  tb1.intGatePassNo AND
				ware_gatepasstransferinheader.intGatePassYear =  tb1.intGatePassYear AND
				ware_gatepasstransferindetails.strOrderNo =  tb1.strOrderNo AND
				ware_gatepasstransferindetails.intOrderYear =  tb1.intOrderYear AND
				ware_gatepasstransferindetails.strStyleNo =  tb1.strStyleNo AND
				ware_gatepasstransferindetails.intItemId =  tb1.intItemId AND
				ware_gatepasstransferinheader.intStatus > '0' AND 
				ware_gatepasstransferinheader.intStatus <= ware_gatepasstransferinheader.intApproveLevels),0) as dblTransferQty, 
				trn_orderdetails.strSalesOrderNo,
				mst_part.strName as part 
				FROM
				ware_gatepassdetails as tb1   
				Inner Join ware_gatepassheader ON tb1.intGatePassNo = ware_gatepassheader.intGatePassNo AND tb1.intGatePassYear = ware_gatepassheader.intGatePassYear
				Inner Join mst_item ON tb1.intItemId = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
				left Join trn_orderdetails ON tb1.strOrderNo = trn_orderdetails.intOrderNo AND tb1.intOrderYear = trn_orderdetails.intOrderYear AND tb1.strStyleNo = trn_orderdetails.intSalesOrderId 
				left Join mst_part ON trn_orderdetails.intPart = mst_part.intId 
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
                                LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier =  
                                mst_supplier.intId
				WHERE 
				ware_gatepassheader.intStatus = '1' AND  
				ware_gatepassheader.intGPToLocation =  '$location' ";
				$sql.=" AND tb1.intGatePassNo =  '$gpNo' ";
				$sql.=" AND tb1.intGatePassYear =  '$year' ";
				
				if($orderNoArray[0]!=0)
				$sql.=" AND tb1.strOrderNo =  '$orderNoArray[0]' ";
				if($orderNoArray[1]!=0)
				$sql.=" AND tb1.intOrderYear =  '$orderNoArray[1]' ";
				if($styleNo!='')
				$sql.=" AND tb1.strStyleNo =  '$styleNo' ";
				if($mainCategory!='')
				$sql.=" AND mst_item.intMainCategory =  '$mainCategory' ";
				if($subCategory!='')
				$sql.=" AND mst_item.intSubCategory =  '$subCategory'";
				if($description!=''){
				$sql.=" AND
				(mst_item.strName LIKE  '%$description%' OR mst_item.strCode LIKE  '%$description%')";
				}
				$sql.=" group by mst_item.intId) as tb1 where tb1.dblQty-tb1.dblTransferQty>0";	
				$sql.=" Order By tb1.mainCatName asc, 
						tb1.subCatName asc, 
						tb1.itemName asc 
						";	

	 	//echo $sql;
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$gpQty = $row['dblQty'];
			if(!$gpQty)
			$gpQty=0;
			$transfQty = $row['dblTransferQty'];
			if(!$transfQty)
			$transfQty=0;
			
			if($row['strOrderNo']==0){
			$stockBalQty=getStockBalance_bulk($location,$row['intId']);
			}
			else{
			//$stockBalQty=getStockBalance($company,$location,$orderNoArray[0],$orderNoArray[1],$row['strStyleNo'],$row['intId']);
			$stockBalQty=getStockBalance_bulk($location,$row['intId']);
			}
			$data['intOrderNo'] 	= $row['strOrderNo'];
			$data['intOrderYear'] 	= $row['intOrderYear'];
			$data['salesOrderId'] 	= $row['strStyleNo'];
			$data['salesOrderNo'] 	= $row['strSalesOrderNo']."/".$row['part'];
			$data['gpNo'] 	= $row['intGatePassNo']."/".$row['intGatePassYear'];
			$data['itemId'] 	= $row['intId'];
			$data['maincatId'] = $row['intMainCategory'];
			$data['subCatId'] 	= $row['intSubCategory'];
			$data['code'] = $row['strCode'];
                        $data['supItemCode'] = $row['SUP_ITEM_CODE'];
			$data['itemName'] = $row['itemName'];
                        $data['itemHide'] = $row['ITEM_HIDE'];
			$data['uom'] 	= $row['uom'];
			$data['unitPrice'] = $row['dblLastPrice'];
			$data['mainCatName'] = $row['mainCatName'];
			$data['subCatName'] = $row['subCatName'];
			$data['gpQty'] = $gpQty;
			$data['transfQty'] = $transfQty;
			$data['stockBalQty'] = $stockBalQty;
			
			$arrCombo[] = $data;
		}
		
		if($arrCombo==null){
			$arrCombo[0]="none";
		}
		
		$response['arrCombo'] 	= $arrCombo;
		echo json_encode($response);

	}
	
	//-----------------------------------------------------------
	function getStockBalance($company,$location,$orderNo,$orderYear,$style,$item)
	{
		global $db;
		  $sql = "SELECT
				Sum(ware_stocktransactions.dblQty) as stockBal 
				FROM ware_stocktransactions
				WHERE
				ware_stocktransactions.intCompanyId =  '$company' AND
				ware_stocktransactions.intLocationId =  '$location' AND
				ware_stocktransactions.intOrderNo =  '$orderNo' AND
				ware_stocktransactions.intOrderYear =  '$orderYear' AND
				ware_stocktransactions.strStyleNo =  '$style' AND
				ware_stocktransactions.intItemId =  '$item'";

		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
	function getStockBalance_bulk($location,$item)
	{
		global $db;
		   $sql = "SELECT
				Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$location'
				GROUP BY
				ware_stocktransactions_bulk.intItemId";
				
			//echo $sql;

		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
?>
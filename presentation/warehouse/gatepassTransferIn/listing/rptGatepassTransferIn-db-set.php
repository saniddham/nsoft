<?php 

	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];

	include "{$backwardseperator}dataAccess/Connector.php";
	include "{$backwardseperator}libraries/mail/mail.php";
	require_once $backwardseperator."class/cls_mail.php";
    require_once $_SESSION['ROOT_PATH']."class/common/azureDBconnection.php";

    $objAzure = new cls_azureDBconnection();
	$objMail = new cls_create_mail($db);
	
	$programName='GatePass Transfer In';
	$programCode='P0252';
	$transfApproveLevel = (int)getApproveLevel($programName);
	
	/////////// parameters /////////////////////////////

	
	$transfNo = $_REQUEST['transfNo'];
	$year = $_REQUEST['year'];
	
	if($_REQUEST['status']=='approve')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		$rollBackFlag=0;
		$rollBackMsg = '';
		
		$sql = "UPDATE `ware_gatepasstransferinheader` SET `intStatus`=intStatus-1 WHERE (intGpTransfNo='$transfNo') AND (`intGpTransfYear`='$year') and intStatus not in(0,1)";
		$result = $db->RunQuery2($sql);
		if((!$result) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sql;
			$rollBackMsg = "Approval error!";
		}
		
		if($result){
			  $sql = "SELECT  ware_gatepasstransferinheader.intUser, 
			  		ware_gatepasstransferinheader.intStatus, 
					ware_gatepasstransferinheader.intApproveLevels, 
					ware_gatepasstransferinheader.intCompanyId as location,
					mst_locations.intCompanyId as companyId 
					FROM
					ware_gatepasstransferinheader 
					Inner Join mst_locations ON ware_gatepasstransferinheader.intCompanyId = mst_locations.intId 
					WHERE (intGpTransfNo='$transfNo') AND (`intGpTransfYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$createdUser = $row['intUser'];
			$status = $row['intStatus'];
			$savedLevels = $row['intApproveLevels'];
			$company = $row['companyId'];
			$location = $row['location'];
			
			$approveLevel=$savedLevels+1-$status;
			$sqlI = "INSERT INTO `ware_gatepasstransferinheader_approvedby` (`intGpTransfNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
				VALUES ('$transfNo','$year','$approveLevel','$userId',now(),0)";
			$resultI = $db->RunQuery2($sqlI);
			if((!$resultI) && ($rollBackFlag!=1)){
				$rollBackFlag=1;
				$sqlM=$sqlI;
				$rollBackMsg = "Approval error!";
			}
			
			if($status==1)
			{
				if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
				////////////// send mail to creater //////////////////
				sendFinlConfirmedEmailToUser($transfNo,$year,$objMail,$mainPath,$root_path);
				}
			}
		//--------------------------------------------------------------------
		if($status==1){//if final confirmation raised,insert to stock
				$sql1 = "SELECT 
						 ware_gatepasstransferindetails.strOrderNo,
						 ware_gatepasstransferindetails.intOrderYear, 
						 ware_gatepasstransferindetails.strStyleNo,
 						 ware_gatepasstransferindetails.dblQty,
						 ware_gatepasstransferindetails.intItemId,  
						 ware_gatepasstransferinheader.intGatePassNo,
						 ware_gatepasstransferinheader.intGatePassYear,
						 ware_gatepassheader.intCompanyId,  
						 mst_item.intBomItem 
						FROM 
						 ware_gatepasstransferindetails  
						Inner Join ware_gatepasstransferinheader ON ware_gatepasstransferindetails.intGpTransfNo = ware_gatepasstransferinheader.intGpTransfNo AND ware_gatepasstransferindetails.intGpTransfYear = ware_gatepasstransferinheader.intGpTransfYear
						Inner Join ware_gatepassheader ON ware_gatepasstransferinheader.intGatePassNo = ware_gatepassheader.intGatePassNo AND ware_gatepasstransferinheader.intGatePassYear = ware_gatepassheader.intGatePassYear
						Inner Join mst_item ON  ware_gatepasstransferindetails.intItemId = mst_item.intId
						WHERE
						 ware_gatepasstransferindetails.intGpTransfNo =  '$transfNo' AND
						 ware_gatepasstransferindetails.intGpTransfYear =  '$year'";
				$result1 = $db->RunQuery2($sql1);
				$toSavedQty=0;
				$transSavedQty=0;
				while($row=mysqli_fetch_array($result1))
				{
					$toSavedQty+=round($row['dblQty'],4);
					$gpNo=$row['intGatePassNo'];
					$gpYear=$row['intGatePassYear'];
					$item=$row['intItemId'];
					$Qty=$row['dblQty'];
					$orderNo=$row['strOrderNo'];
					$orderYear=$row['intOrderYear'];
					$styleNo=$row['strStyleNo'];
					$locationId=$row['intCompanyId'];
					
					if($row['strOrderNo']!=0){//bom item
						//$resultG = getGrnWiseStockBalance($locationId,$gpNo,$gpYear,$orderNo,$orderYear,$styleNo,$item);
						$resultG = getGrnWiseStockBalance_bulk($locationId,$gpNo,$gpYear,$item);
						while($rowG=mysqli_fetch_array($resultG)){
								if(($Qty>0) && (($rowG['gpQty']*(-1))>0)){
									if($Qty<=$rowG['gpQty']*(-1)){
									$saveQty=$Qty;
									$Qty=0;
									}
									else if($Qty>$rowG['gpQty']*(-1)){
									$saveQty=$rowG['gpQty']*(-1);
									$Qty=$Qty-$saveQty;
									}
									$grnNo=$rowG['intGRNNo'];
									$grnYear=$rowG['intGRNYear'];
									$grnDate=$rowG['dtGRNDate'];
									$grnRate=$rowG['dblGRNRate'];	
									$currency=loadCurrency($grnNo,$grnYear,$item);
									$saveQty=round($saveQty,4);
									if($saveQty>0){ 
									/*$sqlI = "INSERT INTO `ware_stocktransactions` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intOrderNo,intOrderYear,intSalesOrderId,intItemId,dblQty,strType,intUser,dtDate) 
										VALUES ('$company','$location','$transfNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$orderNo','$orderYear','$styleNo','$item','$saveQty','TRANSFERIN','$userId',now())";*/
									$sqlI = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intOrderNo,intOrderYear,intSalesOrderId,intUser,dtDate) 
										VALUES ('$company','$location','$transfNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$item','$saveQty','TRANSFERIN',$orderNo,'$orderYear','$styleNo','$userId',now())";
									$resultI = $db->RunQuery2($sqlI);
									if($resultI){
										$transSavedQty+=$saveQty;
									}
									if((!$resultI) && ($rollBackFlag!=1)){
										$rollBackFlag=1;
										$sqlM=$sqlI;
										$rollBackMsg = "Approval error!";
									}
									
									}
								}
						}
					}
					else{//general item
						$resultG = getGrnWiseStockBalance_bulk($locationId,$gpNo,$gpYear,$item);
						while($rowG=mysqli_fetch_array($resultG)){
								if(($Qty>0) && (($rowG['gpQty']*(-1))>0)){
									if($Qty<=$rowG['gpQty']*(-1)){
									$saveQty=$Qty;
									$Qty=0;
									}
									else if($Qty>$rowG['gpQty']*(-1)){
									$saveQty=$rowG['gpQty']*(-1);
									$Qty=$Qty-$saveQty;
									}
									$grnNo=$rowG['intGRNNo'];
									$grnYear=$rowG['intGRNYear'];
									$grnDate=$rowG['dtGRNDate'];
									$grnRate=$rowG['dblGRNRate'];	
									$currency=loadCurrency($grnNo,$grnYear,$item);
									$saveQty=round($saveQty,4);
									if($saveQty>0){
									$sqlI = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intUser,dtDate) 
										VALUES ('$company','$location','$transfNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$item','$saveQty','TRANSFERIN','$userId',now())";
									$resultI = $db->RunQuery2($sqlI);
									if($resultI){
										$transSavedQty+=$saveQty;
									}
									if((!$resultI) && ($rollBackFlag!=1)){
										$rollBackFlag=1;
										$sqlM=$sqlI;
										$rollBackMsg = "Approval error!";
									}
									
									}
								}
						}
					}
				}
			}
		//--------------------------------------------------------------------
			if($status==$savedLevels){//if first confirmation raised,gp qty field in po table shld update
			 	$sql1 = "SELECT
						 ware_gatepasstransferindetails.intItemId,
						 ware_gatepasstransferindetails.dblQty,
						 ware_gatepasstransferindetails.strOrderNo,
						 ware_gatepasstransferindetails.intOrderYear, 
						 ware_gatepasstransferindetails.strStyleNo,
						 ware_gatepasstransferinheader.intGatePassNo,
						 ware_gatepasstransferinheader.intGatePassYear
						FROM
						ware_gatepasstransferindetails
						Inner Join ware_gatepasstransferinheader ON ware_gatepasstransferindetails.intGpTransfNo = ware_gatepasstransferinheader.intGpTransfNo AND ware_gatepasstransferindetails.intGpTransfYear = ware_gatepasstransferinheader.intGpTransfYear
						WHERE
						 ware_gatepasstransferinheader.intGpTransfNo =  '$transfNo' AND
						 ware_gatepasstransferinheader.intGpTransfYear =  '$year'";
				$result1 = $db->RunQuery2($sql1);
				while($row=mysqli_fetch_array($result1))
				{
					$orderNo=$row['strOrderNo'];
					$orderYear=$row['intOrderYear'];
					$styleNo=$row['strStyleNo'];
					$gpNo=$row['intGatePassNo'];
					$gpYear=$row['intGatePassYear'];
					$item=$row['intItemId'];
					$Qty=$row['dblQty'];
					
					$sql = "UPDATE `ware_gatepassdetails` SET 	dblTransferQty = dblTransferQty+'$Qty' 
							WHERE 
							 ware_gatepassdetails.strOrderNo =  '$orderNo' AND
							 ware_gatepassdetails.intOrderYear =  '$orderYear' AND
							 ware_gatepassdetails.strStyleNo =  '$styleNo' AND
							 ware_gatepassdetails.intGatePassNo =  '$gpNo' AND
							 ware_gatepassdetails.intGatePassYear =  '$gpYear' AND
							 ware_gatepassdetails.intItemId =  '$item' ";
					$result = $db->RunQuery2($sql);
					if((!$result) && ($rollBackFlag!=1)){
						$rollBackFlag=1;
						$sqlM=$sql;
						$rollBackMsg = "Approval error!";
					}
				}
			}
		//---------------------------------------------------------------------------
		}
		
		if($status==1 ){
			//echo "sdfdsfdwf";
			//update_po_prn_details_for_gp_qty($gatePassNo,$year);
			update_po_prn_details_transactions($transfNo,$year);
		}
		
		$toSavedQty=round($toSavedQty,4);
		$transSavedQty=round($transSavedQty,4);
		
		if(($toSavedQty!=$transSavedQty) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$rollBackMsg = "Approval Error";
			$sqlM=$toSavedQty."-".$transSavedQty;
		}
		
		if(($rollBackFlag==0) &&($status==1)){
		    if($company == '1'){
		        updateAzureDBTablesOnGPIN($transfNo,$year,$db);
            }
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= "Final Approval raised sucessfully";
			$response['q'] 			= $sql;
		}
		else if($rollBackFlag==0){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= "Approval raised sucessfully";
			$response['q'] 			= '';
		}
		else if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= $sqlM;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sqlM;
		}
		$db->CloseConnection();		
		
		//----------------------------------------
			echo json_encode($response);
		//-----------------------------------------	
		
	}
	else if($_REQUEST['status']=='reject')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		$rollBackFlag=0;
		$rollBackMsg = '';
		
		//$grnApproveLevel = (int)getApproveLevel($programName);
		$sql = "SELECT  ware_gatepasstransferinheader.intUser, ware_gatepasstransferinheader.intStatus,ware_gatepasstransferinheader.intApproveLevels FROM ware_gatepasstransferinheader WHERE  (intGpTransfNo='$transfNo') AND (`intGpTransfYear`='$year')";
		$results = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($results);
		$createdUser = $row['intUser'];
		$status = $row['intStatus'];
		$savedLevels = $row['intApproveLevels'];
		
		$sql = "UPDATE `ware_gatepasstransferinheader` SET `intStatus`=0 WHERE (intGpTransfNo='$transfNo') AND (`intGpTransfYear`='$year')";
		$result = $db->RunQuery2($sql);
		if((!$result) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sql;
			$rollBackMsg = "Rejection error!";
		}
		
		//$sql = "DELETE FROM `ware_gatepasstransferinheader_approvedby` WHERE (`intGpTransfNo`='$transfNo') AND (`intYear`='$year')";
		//$result2 = $db->RunQuery($sql);
		
		$sqlI = "INSERT INTO `ware_gatepasstransferinheader_approvedby` (`intGpTransfNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
			VALUES ('$transfNo','$year','0','$userId',now(),0)";
		$resultI = $db->RunQuery2($sqlI);
		if((!$resultI) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sqlI;
			$rollBackMsg = "Rejection error!";
		}
		
			if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
			sendRejecedEmailToUser($transfNo,$year,$objMail,$mainPath,$root_path);
			}
		
		//---------------------------------------------------------------------------------
/*		if($status==1){//if final confirmation raised,insert to stock
				$sql1 = "SELECT
						 ware_gatepasstransferindetails.intItemId,
						 ware_gatepasstransferindetails.dblQty,
						 ware_gatepasstransferindetails.strOrderNo,
						 ware_gatepasstransferindetails.intOrderYear, 
						 ware_gatepasstransferindetails.strStyleNo, 
						mst_item.intBomItem 
						FROM 
						 ware_gatepasstransferindetails 
						Inner Join mst_item ON  ware_gatepasstransferindetails.intItemId = mst_item.intId
						WHERE
						 ware_gatepasstransferindetails.intGpTransfNo =  '$transfNo' AND
						 ware_gatepasstransferindetails.intGpTransfYear =  '$year'";
				$result1 = $db->RunQuery($sql1);
				while($row=mysqli_fetch_array($result1))
				{
					$orderNo=$row['strOrderNo'];
					$orderYear=$row['intOrderYear'];
					$styleNo=$row['strStyleNo'];
					$item=$row['intItemId'];
					$Qty=$row['dblQty'];
					$Qty=round($Qty,4);
		
					if($row['strOrderNo']!=0){//bom item
					$sqlI = "INSERT INTO `ware_stocktransactions` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intOrderNo,intOrderYear,intSalesOrderId,intItemId,dblQty,strType,intUser,dtDate) 
						VALUES ('$company','$location','$transfNo','$year','$orderNo','$orderYear','$styleNo','$item','-$Qty','CTRANSFERIN','$userId',now())";
					}
					else{
					$sqlI = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intItemId,dblQty,strType,intUser,dtDate) 
						VALUES ('$company','$location','$transfNo','$year','$item','-$Qty','CTRANSFERIN','$userId',now())";
					}
					$resultI = $db->RunQuery($sqlI);
				}
			}
*/		//---------------------------------------------------------------------------------
			if($savedLevels>=$status){//if atleast one confirmation processed, roll back return to saupplier qty from grn table-
				$sql1 = "SELECT
						 ware_gatepasstransferindetails.intItemId,
						 ware_gatepasstransferindetails.dblQty,
						 ware_gatepasstransferindetails.strOrderNo,
						 ware_gatepasstransferindetails.intOrderYear, 
						 ware_gatepasstransferindetails.strStyleNo,
						 ware_gatepasstransferinheader.intGatePassNo,
						 ware_gatepasstransferinheader.intGatePassYear
						FROM
						ware_gatepasstransferindetails
						Inner Join ware_gatepasstransferinheader ON ware_gatepasstransferindetails.intGpTransfNo = ware_gatepasstransferinheader.intGpTransfNo AND ware_gatepasstransferindetails.intGpTransfYear = ware_gatepasstransferinheader.intGpTransfYear
						WHERE
						 ware_gatepasstransferinheader.intGpTransfNo =  '$transfNo' AND
						 ware_gatepasstransferinheader.intGpTransfYear =  '$year'";
				$result1 = $db->RunQuery2($sql1);
				while($row=mysqli_fetch_array($result1))
				{
					$orderNo=$row['strOrderNo'];
					$orderYear=$row['intOrderYear'];
					$styleNo=$row['strStyleNo'];
					$gpNo=$row['intGatePassNo'];
					$gpYear=$row['intGatePassYear'];
					$item=$row['intItemId'];
					$Qty=$row['dblQty'];
					
					$sql = "UPDATE `ware_gatepassdetails` SET 	dblTransferQty = dblTransferQty-'$Qty' 
							WHERE 
							 ware_gatepassdetails.strOrderNo =  '$orderNo' AND
							 ware_gatepassdetails.intOrderYear =  '$orderYear' AND
							 ware_gatepassdetails.strStyleNo =  '$styleNo' AND
							 ware_gatepassdetails.intGatePassNo =  '$gpNo' AND
							 ware_gatepassdetails.intGatePassYear =  '$gpYear' AND
							 ware_gatepassdetails.intItemId =  '$item' ";
					$result = $db->RunQuery2($sql);
					if((!$result) && ($rollBackFlag!=1)){
						$rollBackFlag=1;
						$sqlM=$sql;
						$rollBackMsg = "Rejection error!";
					}
				}
			}
		//------------------------------------
		if($rollBackFlag==0){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= "Rejection raised sucessfully";
			$response['q'] 			= '';
		}
		else if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= $sqlM;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sqlM;
		}
		$db->CloseConnection();		
		
		//----------------------------------------
			echo json_encode($response);
		//-----------------------------------------	
		
	}
//--------------------------------------------------------------
function getGrnWiseStockBalance_bulk($location,$gpNo,$gpYear,$item)
{
	global $db;
	 $sql = "SELECT
			Sum(ware_stocktransactions_bulk.dblQty) AS gpQty,
			ware_stocktransactions_bulk.intGRNNo,
			ware_stocktransactions_bulk.intGRNYear,
			ware_stocktransactions_bulk.dtGRNDate,
			ware_stocktransactions_bulk.dblGRNRate
			FROM ware_stocktransactions_bulk
			WHERE
			ware_stocktransactions_bulk.intItemId =  '$item' AND
			ware_stocktransactions_bulk.strType =  'GATEPASS' AND
			ware_stocktransactions_bulk.intDocumentNo =  '$gpNo' AND 
			ware_stocktransactions_bulk.intDocumntYear =  '$gpYear' AND 
			ware_stocktransactions_bulk.intLocationId =  '$location'
			GROUP BY
			ware_stocktransactions_bulk.intItemId,
			ware_stocktransactions_bulk.intGRNNo,
			ware_stocktransactions_bulk.intGRNYear, 
			ware_stocktransactions_bulk.dblGRNRate 
			ORDER BY
			ware_stocktransactions_bulk.dtGRNDate ASC";

	$result = $db->RunQuery2($sql);
	return $result;	
}
//--------------------------------------------------------------
function getGrnWiseStockBalance($location,$gpNo,$gpYear,$orderNo,$orderYear,$salesOrderNo,$item)
{
	global $db;
	 $sql = "SELECT
			Sum(ware_stocktransactions.dblQty) AS gpQty,
			ware_stocktransactions.intGRNNo,
			ware_stocktransactions.intGRNYear,
			ware_stocktransactions.dtGRNDate,
			ware_stocktransactions.dblGRNRate
			FROM ware_stocktransactions 
			WHERE
			ware_stocktransactions.intItemId =  '$item' AND 
			ware_stocktransactions.strType =  'GATEPASS' AND
			ware_stocktransactions.intDocumentNo =  '$gpNo' AND 
			ware_stocktransactions.intDocumntYear =  '$gpYear' AND 
			ware_stocktransactions.intLocationId =  '$location' AND 
			ware_stocktransactions.intOrderNo='$orderNo' AND 
			ware_stocktransactions.intOrderYear='$orderYear' AND 
			ware_stocktransactions.intSalesOrderId='$salesOrderNo' 
			GROUP BY
			ware_stocktransactions.intItemId,
			ware_stocktransactions.intGRNNo,
			ware_stocktransactions.intGRNYear, 
			ware_stocktransactions.dblGRNRate
 			ORDER BY
			ware_stocktransactions.dtGRNDate ASC";
			//echo $sql;

	$result = $db->RunQuery2($sql);
	return $result;	
}
//--------------------------------------------------------------
function loadCurrency($grnNo,$grnYear,$item)
{
	global $db;
	
	if(($grnNo!=0) && ($grnYear!=0)){
	  $sql = "SELECT
			trn_poheader.intCurrency as currency 
			FROM
			ware_grnheader
			Inner Join trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
			WHERE
			ware_grnheader.intGrnNo =  '$grnNo' AND
			ware_grnheader.intGrnYear =  '$grnYear'";
	}
	else{
	$sql = "SELECT
			mst_item.intCurrency as currency 
			FROM mst_item
			WHERE
			mst_item.intId =  '$item'";
	}

	$result = $db->RunQuery2($sql);
	$row=mysqli_fetch_array($result);	
	return $row['currency'];;	
}
//--------------------------------------------------------
function sendFinlConfirmedEmailToUser($serialNo,$year,$objMail,$mainPat,$root_path){
	global $db;
				
		    $sql = "SELECT
			sys_users.strFullName,
			sys_users.strEmail 
			FROM 
			ware_gatepasstransferinheader
			Inner Join sys_users ON ware_gatepasstransferinheader.intUser = sys_users.intUserId
			WHERE
			ware_gatepasstransferinheader.intGpTransfNo =  '$serialNo' AND
			ware_gatepasstransferinheader.intGpTransfYear =  '$year'";

				
		 $result = $db->RunQuery2($sql);
		 
		$row=mysqli_fetch_array($result);
			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="CONFIRMED GATE PASS TRANSFER IN ('$serialNo'/'$year')"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST = NULL;
			
			$_REQUEST['program']='GATE PASS TRANSFER IN';
			$_REQUEST['field1']='Tranfer In No';
			$_REQUEST['field2']='Transfer In Year';
			$_REQUEST['field3']='';
			$_REQUEST['field4']='';
			$_REQUEST['field5']='';
			$_REQUEST['value1']=$serialNo;
			$_REQUEST['value2']=$year;
			$_REQUEST['value3']='';
			$_REQUEST['value4']='';
			$_REQUEST['value5']='';
			
			$_REQUEST['subject']="CONFIRMED GATE PASS TRANSFER IN ('$serialNo'/'$year')";
			
			$_REQUEST['statement1']="Approved this";
			$_REQUEST['statement2']="to view this";
			$_REQUEST['link']=base64_encode($mainPath."presentation/warehouse/gatepassTransferIn/listing/rptGatepassTransferIn.php?transfNo=$transfNo&year=$year&approveMode=0"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}
//--------------------------------------------------------
function sendRejecedEmailToUser($serialNo,$year,$objMail,$mainPat,$root_path){
	global $db;
				
		    $sql = "SELECT
			sys_users.strFullName,
			sys_users.strEmail 
			FROM 
			ware_gatepasstransferinheader
			Inner Join sys_users ON ware_gatepasstransferinheader.intUser = sys_users.intUserId
			WHERE
			ware_gatepasstransferinheader.intGpTransfNo =  '$serialNo' AND
			ware_gatepasstransferinheader.intGpTransfYear =  '$year'";

				
		 $result = $db->RunQuery2($sql);
		 
		$row=mysqli_fetch_array($result);
			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="REJECTED GATE PASS TRANSFER IN('$serialNo'/'$year')"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST = NULL;
			
			$_REQUEST['program']='GATE PASS TRANSFER IN';
			$_REQUEST['field1']='Tranfer In No';
			$_REQUEST['field2']='Transfer In Year';
			$_REQUEST['field3']='';
			$_REQUEST['field4']='';
			$_REQUEST['field5']='';
			$_REQUEST['value1']=$serialNo;
			$_REQUEST['value2']=$year;
			$_REQUEST['value3']='';
			$_REQUEST['value4']='';
			$_REQUEST['value5']='';
			
			$_REQUEST['subject']="REJECTED GATE PASS TRANSFER IN ('$serialNo'/'$year')";
			
			$_REQUEST['statement1']="Rejected this";
			$_REQUEST['statement2']="to view this";
			$_REQUEST['link']=base64_encode($mainPath."presentation/warehouse/gatepassTransferIn/listing/rptGatepassTransferIn.php?transfNo=$transfNo&year=$year&approveMode=0"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}
function update_po_prn_details_transactions($transfNo,$year){
	global $db;
	$sql="SELECT
			ware_gatepasstransferindetails.strOrderNo,
			ware_gatepasstransferindetails.intOrderYear,
			ware_gatepasstransferindetails.strStyleNo,
			ware_gatepasstransferindetails.intItemId,
			ware_gatepasstransferindetails.dblQty,
			ware_gatepasstransferinheader.intCompanyId AS `from`,
			trn_po_prn_details_sales_order_stock_transactions.ORDER_NO AS saved_pri
			FROM
			ware_gatepasstransferindetails
			INNER JOIN ware_gatepasstransferinheader ON ware_gatepasstransferindetails.intGpTransfNo = ware_gatepasstransferinheader.intGpTransfNo AND ware_gatepasstransferindetails.intGpTransfYear = ware_gatepasstransferinheader.intGpTransfYear
			LEFT JOIN trn_po_prn_details_sales_order_stock_transactions ON ware_gatepasstransferindetails.strOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO AND ware_gatepasstransferindetails.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR AND ware_gatepasstransferindetails.strStyleNo = trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER AND ware_gatepasstransferindetails.intItemId = trn_po_prn_details_sales_order_stock_transactions.ITEM  
			AND (trn_po_prn_details_sales_order_stock_transactions.TRANSACTION='GATE_PASS_IN' ) 
			AND trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID =ware_gatepasstransferinheader.intCompanyId
			WHERE
			ware_gatepasstransferindetails.intGpTransfNo = '$transfNo' AND
			ware_gatepasstransferindetails.intGpTransfYear = '$year'
			";
	$result = $db->RunQuery2($sql);
	while($row	=mysqli_fetch_array($result)){
		$orderNo			= $row['strOrderNo'];
		$orderYear			= $row['intOrderYear'];
		$so					= $row['strStyleNo'];
		$item				= $row['intItemId'];
		$totQty				= $row['dblQty'];
		$saved_pri				= $row['saved_pri'];
		$location			= $row['from'];
		if($saved_pri > 0){
        //PRI ITEM
            $sql_u	="UPDATE `trn_po_prn_details_sales_order_stock_transactions` SET `QTY`=IFNULL(`QTY`,0)+'$totQty' 
		  WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`SALES_ORDER`='$so') AND (`ITEM`='$item') AND (`TRANSACTION`='GATE_PASS_IN') AND (`LOCATION_ID`='$location')";
            $result_u = $db->RunQuery2($sql_u);
        }
		else{
			$sql_u2	="INSERT INTO `trn_po_prn_details_sales_order_stock_transactions` (`ORDER_NO`, `ORDER_YEAR`, `SALES_ORDER`, `ITEM`, `LOCATION_ID`, `TRANSACTION`, `QTY`) VALUES ('$orderNo', '$orderYear', '$so', '$item', '$location', 'GATE_PASS_IN', '$totQty')";
			$result_u2 	= $db->RunQuery2($sql_u2);
		}
		
 	}
}

function updateAzureDBTablesOnGPIN($gpTrInNo, $gpTrInYear, $db)
{
    global $objAzure;
    $environment = $objAzure->getEnvironment();
    $table = 'Inventory'.$environment;

    $azure_connection = $objAzure->connectAzureDB();
    $sql_select = "SELECT
                    CONCAT(
                        GPIND.intGpTransfNo,
                        '/',
                        GPIND.intGpTransfYear
                    ) AS GP_IN_NO,
                    CONCAT(
                        GPH.intGatePassNo,
                        '/',
                        GPH.intGatePassYear
                    ) AS GP_NO,
                    ware_gatepasstransferinheader.intCompanyId AS location,
                    GPIND.intItemId,
                    Sum(GPIND.dblQty) AS dblQty,
                    mst_item.strCode AS item_code,
                    mst_item.dblLastPrice AS price,
                    mst_item.strName AS description,
                    mst_maincategory.strName AS mainCategory,
                    mst_subcategory.strName AS subCategory,
                    mst_maincategory.intSendDetails AS sendMainCat,
                    mst_subcategory.intSendDetails AS sendSubCat,
                    (
                        SELECT
                            round(
                                sum(
                                    (
                                        (- 1) * ware_grnheader.dblExchangeRate * ware_stocktransactions_bulk.dblQty * ware_stocktransactions_bulk.dblGRNRate
                                    ) / 1.0000
                                ),
                                2
                            )
                        FROM
                            ware_stocktransactions_bulk
                        LEFT JOIN ware_grnheader ON ware_grnheader.intGrnNo = ware_stocktransactions_bulk.intGRNNo
                        AND ware_grnheader.intGrnYear = ware_stocktransactions_bulk.intGRNYear
                        WHERE
                            ware_stocktransactions_bulk.intDocumentNo = GPH.intGatePassNo
                        AND ware_stocktransactions_bulk.intDocumntYear = GPH.intGatePassYear
                        AND ware_stocktransactions_bulk.intItemId = GPIND.intItemId
                        AND ware_stocktransactions_bulk.strType = 'GATEPASS'
                        GROUP BY
                            ware_stocktransactions_bulk.intDocumentNo,
                            ware_stocktransactions_bulk.intDocumntYear,
                            ware_stocktransactions_bulk.intItemId
                    ) AS amount
                FROM
                    ware_gatepasstransferindetails GPIND
                INNER JOIN ware_gatepasstransferinheader ON GPIND.intGpTransfNo = ware_gatepasstransferinheader.intGpTransfNo
                AND GPIND.intGpTransfYear = ware_gatepasstransferinheader.intGpTransfYear
                INNER JOIN ware_gatepassheader GPH ON ware_gatepasstransferinheader.intGatePassNo = GPH.intGatePassNo
                AND ware_gatepasstransferinheader.intGatePassYear = GPH.intGatePassYear
                INNER JOIN mst_item ON GPIND.intItemId = mst_item.intId
                INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
                INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
                WHERE
                    GPIND.intGpTransfNo = '$gpTrInNo'
                AND GPIND.intGpTransfYear = '$gpTrInYear'
                GROUP BY
                    GPIND.intItemId";

    $result = $db->RunQuery2($sql_select);
    $line_no = rand(100,1000);
    while ($row = mysqli_fetch_array($result)) {
        $gp_in_no = $row['GP_IN_NO'];
        $gp_no = $row['GP_NO'];
        $item_category = $row['mainCategory'];
        $item_subcategory = $row['subCategory'];
        $item_code = $row['item_code'];
        $description = str_replace("'","''",$row['description']);
        $unit_price = $row['price'];
        $qty = $row['dblQty'];
        $amount = $row['amount'];
        $location = $row['location'];
        $line_no++;
        $successHeader = '0';
        $postingDate = date('Y-m-d H:i:s');
        $sendDetails = ($row['sendMainCat'] == 1 && $row['sendSubCat'] == 1)? true:false;

        if($sendDetails) {
            $sql_azure_header = "INSERT INTO $table (Document_Category, Document_No, Line_No, External_Doc_No, Posting_Date, Item_Category, Item_Sub_Category, Item_Code, Description, Amount, Location) VALUES ('TransferIn', '$gp_in_no', '$line_no', '$gp_no', '$postingDate', '$item_category', '$item_subcategory', '$item_code', '$description', '$amount','$location')";
            if ($azure_connection) {
                $getResultsHeader = $objAzure->runQuery($azure_connection, $sql_azure_header);
                if ($getResultsHeader != FALSE) {
                    $successHeader = '1';
                }
            }
            $sql_db_header = "INSERT INTO trn_financemodule_inventory (Document_Category, Document_No, Line_No, External_Doc_No, Posting_Date, Item_Category, Item_Sub_Category, Item_Code, Description, Amount, Location, deliveryStatus, deliveryDate) VALUES ('TransferIn', '$gp_in_no', '$line_no', '$gp_no', '$postingDate', '$item_category', '$item_subcategory', '$item_code', '$description', '$amount','$location','$successHeader', NOW())";
            $result_db_header = $db->RunQuery2($sql_db_header);
        }

    }

}

function getNextSysNo($db, $companyId, $transaction_type){
    $current_serial_no = $companyId.'00000';
    $sql = "SELECT `$transaction_type` FROM `trn_financemodule_sysNo` WHERE `COMPANY_ID` = '$companyId'";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    if($row[$transaction_type] != ''){
        $current_serial_no = $row[$transaction_type];
    }
    //update next serial no
    $sql_update = "UPDATE `trn_financemodule_sysNo` SET `$transaction_type`='$current_serial_no'+1 WHERE `COMPANY_ID`='$companyId'";
    $result = $db->RunQuery2($sql_update);
    return $current_serial_no;
}
?>
<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$companyId 	= $_SESSION['CompanyID'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	require_once $backwardseperator."class/warehouse/cls_warehouse_get.php";

	$objwhouseget= new cls_warehouse_get($db);

	$programName='GatePass Transfer In';
	$programCode='P0252';
	$issueApproveLevel = (int)getApproveLevel($programName);
	
		$db->OpenConnection();
		$db->RunQuery2('Begin');

 if($requestType=='getValidation')
	{
		$transfNo  = $_REQUEST['transfNo'];
		$transfNoArray=explode("/",$transfNo);
	
		///////////////////////
		$sql = "SELECT
		ware_gatepasstransferinheader.intStatus, 
		ware_gatepasstransferinheader.intApproveLevels, 
		ware_gatepasstransferinheader.intGatePassNo,
		ware_gatepasstransferinheader.intGatePassYear 
		FROM ware_gatepasstransferinheader
		WHERE
		ware_gatepasstransferinheader.intGpTransfNo =  '$transfNoArray[0]' AND
		ware_gatepasstransferinheader.intGpTransfYear =  '$transfNoArray[1]'";

		$result = $db->RunQuery2($sql);
		$errorFlg=0;
		$msg =""; 
		$msg1 ="";
		$row=mysqli_fetch_array($result);
		$status=$row['intStatus'];
		$approveLevels=$row['intApproveLevels'];	
		$gpNo=$row['intGatePassNo'];
		$gpYear=$row['intGatePassYear'];
		//--------------------------
		$response['arrData'] = $objwhouseget->getLocationValidation('TRANSFERIN',$companyId,$gpNo,$gpYear);//check for grn location
		//--------------------------
		$confirmatonMode=loadConfirmatonMode($programCode,$status,$approveLevels,$userId);
		
/*		 if($response['arrData']['type']=='fail'){
			$errorFlg = 1;
			$msg=$response['arrData']['msg'];
		 }
		 else */
		if($row['intStatus']==1){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="Final confirmation of this Gate Pass Trnasfer IN Note is already raised."; 
		}
		else if($row['intStatus']==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="This Gate Pass No is Rejected"; 
		}
		else if($confirmatonMode==0){// 
			$errorFlg = 1;
			$msg ="No Permission to Approve"; 
		}
	
		if($errorFlg==0){
		           $sql = "SELECT
ware_gatepasstransferinheader.intStatus,
ware_gatepasstransferinheader.intApproveLevels,
ware_gatepasstransferinheader.intGatePassNo,
ware_gatepasstransferinheader.intGatePassYear,
ware_gatepasstransferindetails.strOrderNo,
ware_gatepasstransferindetails.intOrderYear,
ware_gatepasstransferindetails.strStyleNo,
ware_gatepasstransferindetails.intItemId,
ware_gatepasstransferindetails.dblQty,
tb1.dblQty as gpQty,
IFNULL((SELECT
Sum(ware_gatepasstransferindetails.dblQty)
FROM
ware_gatepasstransferinheader
Inner Join ware_gatepasstransferindetails ON ware_gatepasstransferinheader.intGpTransfNo = ware_gatepasstransferindetails.intGpTransfNo AND ware_gatepasstransferinheader.intGpTransfYear = ware_gatepasstransferindetails.intGpTransfYear
WHERE
ware_gatepasstransferinheader.intGatePassNo =  tb1.intGatePassNo AND
ware_gatepasstransferinheader.intGatePassYear =  tb1.intGatePassYear AND
ware_gatepasstransferindetails.strOrderNo =  tb1.strOrderNo AND
ware_gatepasstransferindetails.intOrderYear =  tb1.intOrderYear AND
ware_gatepasstransferindetails.strStyleNo =  tb1.strStyleNo AND
ware_gatepasstransferindetails.intItemId =  tb1.intItemId AND
ware_gatepasstransferinheader.intStatus > '0' AND 
ware_gatepasstransferinheader.intStatus <= ware_gatepasstransferinheader.intApproveLevels),0) as dblTransferQty,
mst_item.strName as itemName 
FROM
ware_gatepasstransferinheader
Inner Join ware_gatepasstransferindetails ON ware_gatepasstransferinheader.intGpTransfNo = ware_gatepasstransferindetails.intGpTransfNo AND ware_gatepasstransferinheader.intGpTransfYear = ware_gatepasstransferindetails.intGpTransfYear
Inner Join ware_gatepassdetails as tb1 ON ware_gatepasstransferinheader.intGatePassNo = tb1.intGatePassNo AND ware_gatepasstransferinheader.intGatePassYear = tb1.intGatePassYear AND ware_gatepasstransferindetails.strOrderNo = tb1.strOrderNo AND ware_gatepasstransferindetails.intOrderYear = tb1.intOrderYear AND ware_gatepasstransferindetails.strStyleNo = tb1.strStyleNo AND ware_gatepasstransferindetails.intItemId = tb1.intItemId
Inner Join mst_item ON tb1.intItemId = mst_item.intId
Inner Join ware_gatepassheader ON tb1.intGatePassNo = ware_gatepassheader.intGatePassNo AND tb1.intGatePassYear = ware_gatepassheader.intGatePassYear 
Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId  
WHERE 
ware_gatepassheader.intStatus =  '1' AND
ware_gatepasstransferinheader.intGpTransfNo =  '$transfNoArray[0]' AND
ware_gatepasstransferinheader.intGpTransfYear =  '$transfNoArray[1]'";
				
		$result = $db->RunQuery2($sql);
		$errorFlg=0;
		$msg ="Maximum Qtys for items"; 
		
		$totTosaveQty=0;
		$totSavableQty=0;
		while($row=mysqli_fetch_array($result))
		{
			 $status=$row['intStatus'];
			 
			 $Qty=$row['dblQty'];
			 
			$location 	= $_SESSION['CompanyID'];
			$company 	= $_SESSION['headCompanyId'];
			
			if($row['intStatus']==($row['intApproveLevels']+1)){//no confirmation has been raised
				$balQtyTotrnsferin = $row['gpQty']-$row['dblTransferQty'];
			}
			else{//confirmation has been raised
				$balQtyTotrnsferin = $row['gpQty']-$row['dblTransferQty']+$Qty;
			}

			if($Qty>$balQtyTotrnsferin){
				$errorFlg=1;
				if($row['strOrderNo']!=0){
					$msg .="\n ".$row['strOrderNo']."/".$row['intOrderYear']."-".$row['strStyleNo']."-".$row['itemName']." =".$balQtyToIssue;
				}
				else{
					$msg .="\n "."-".$row['itemName']." =".$balQtyTotrnsferin;
				}
			}
			
			$totTosaveQty +=$Qty;
			$totSavableQty +=$balQtyTotrnsferin;
		}//end of while
		}//end if ($errorFlg==0)
		
		
	if(($totTosaveQty > $totSavableQty) && ($errorFlg!=1)){
		$errorFlg=1;
		$msg ="Balance to transfer Qty is not tally with This Qtys.";
	}
	
	if($msg1!=''){
		$msg=$msg1;
	}
	
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);
	}
	
//---------------------------
else if($requestType=='validateRejecton')
	{
		$transfNo  = $_REQUEST['transfNo'];
		$transfNoArray=explode("/",$transfNo);
		
		///////////////////////
		 $sql = "SELECT
		ware_gatepasstransferinheader.intStatus, 
		ware_gatepasstransferinheader.intApproveLevels 
		FROM ware_gatepasstransferinheader
		WHERE
		ware_gatepasstransferinheader.intGpTransfNo =  '$transfNoArray[0]' AND 
		ware_gatepasstransferinheader.intGpTransfYear =  '$transfNoArray[1]'";

		$result = $db->RunQuery2($sql);
		$errorFlg=0;
		$msg =""; 
		$row=mysqli_fetch_array($result);
		
		$rejectionMode=loadRejectionMode($programCode,$row['intStatus'],$row['intApproveLevels'],$userId);
		
		if($row['intStatus']==1){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="Final confirmation of this Gate Pass Trnasfer IN Note is already raised.So cant reject"; 
		}
		else if($row['intStatus']==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="This Gate Pass Trnasfer IN Note is already Rejected"; 
		}
		else if($rejectionMode==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="No Permission to reject this Gate Pass Trnasfer IN Note"; 
		}
		else{//confirmation has been raised
			$errorFlg = 0;
			
		}
	
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);
	}
	
	
		$db->RunQuery2('Commit');
		$db->CloseConnection();		
	

//---------------------------
	//-----------------------------------------------------------
	function getStockBalance($company,$location,$orderNo,$orderYear,$style,$item)
	{
		global $db;
		  $sql = "SELECT
				Sum(ware_stocktransactions.dblQty) as stockBal 
				FROM ware_stocktransactions
				WHERE
				ware_stocktransactions.intCompanyId =  '$company' AND
				ware_stocktransactions.intLocationId =  '$location' AND
				ware_stocktransactions.intOrderNo =  '$orderNo' AND
				ware_stocktransactions.intOrderYear =  '$orderYear' AND
				ware_stocktransactions.intSalesOrderId =  '$style' AND
				ware_stocktransactions.intItemId =  '$item'";

		$result = $db->RunQuery2($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['stockBal']);	
	}
	//--------------------------------------------------------------
	function getStockBalance_bulk($location,$item)
	{
		global $db;
		   $sql = "SELECT
				Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$location'
				GROUP BY
				ware_stocktransactions_bulk.intItemId";

		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
//------------------------------function loadRejectionMode-------------------
function loadRejectionMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$rejectMode=0;
	$sqlp = "SELECT
		menupermision.intReject 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery2($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intReject']==1){
	 if($intStatus!=0){
	 $rejectMode=1;
	 }
	}
	 
	return $rejectMode;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	 $sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery2($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
//--------------------------------------------------------

?>

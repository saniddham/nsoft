// JavaScript Document
var basePath = "presentation/warehouse/gatepassTransferIn/listing/";

$(document).ready(function() {
	$('#frmGPtransferinReport').validationEngine();
	
	$('#frmGPtransferinReport #imgApprove').click(function(){
	var val = $.prompt('Are you sure you want to approve this Transferin Note ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					if(validateQuantities()==0){
					///////////
					var url = basePath+"rptGatepassTransferIn-db-set.php"+window.location.search+'&status=approve';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmGPtransferinReport #imgApprove').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmGPtransferinReport #imgApprove').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						}
 					   hideWaiting();
					}
				}});
	});
	
	$('#frmGPtransferinReport #imgReject').click(function(){
	var val = $.prompt('Are you sure you want to reject this Transferin Note ?',{
			buttons: { Ok: true, Cancel: false },
			callback: function(v,m,f){
				if(v)
				{
					showWaiting();
					if(validateRejecton()==0){
					///////////
					var url = basePath+"rptGatepassTransferIn-db-set.php"+window.location.search+'&status=reject';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmGPtransferinReport #imgReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmGPtransferinReport #imgReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						}
						hideWaiting();
				}
			}});
	});
	
/*	$('#frmPRNlisting #cboPrnStatus').change(function(){
					//	var url = "rptPurchaseRequisitionNote-db-set.php"+window.location.search+'&status=reject';
					//	var url = "purchaseRequisitionNoteListing.php"+window.location.search+'?cboPrnStatus='+$('#cboPrnStatus').val();
					//		var obj = $.ajax({url:url,async:false});
						//	var val=$('#cboPrnStatus').val();
							//alert(val);
						//	window.location.href = window.location.href;
						//	$('#cboPrnStatus').val(val);
						//	alert($('#cboPrnStatus').val());
						$("frmPRNlisting").submit();
	});
*/	
});
//---------------------------------------------------
function validateQuantities(){
		var ret=0;	
	    var transfNo = document.getElementById('divTransfNo').innerHTML;
		var url 		= basePath+"rptGatepassTransferIn-db-get.php?requestType=getValidation";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"transfNo="+transfNo,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#imgApprove').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertx()",1000);
					  ret= 1;
					}
				}
			});
			return ret;
}
//---------------------------------------------------
function validateRejecton(){
		var ret=0;	
	    var transfNo = document.getElementById('divTransfNo').innerHTML;
		var url 		= basePath+"rptGatepassTransferIn-db-get.php?requestType=validateRejecton";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"transfNo="+transfNo,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#imgApprove').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertx()",1000);
					  ret= 1;
					}
				}
			});
			return ret;
}
//------------------------------------------------
function alertx()
{
	$('#frmGPtransferinReport #imgApprove').validationEngine('hide')	;
}
//--------------------------------------------
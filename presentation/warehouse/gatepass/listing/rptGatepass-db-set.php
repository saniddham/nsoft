<?php 
//////////////////////////////////////////////
//Create By:H.B.G Korala
//note://if final confirmation raised,insert into stocktransaction table.
/////////////////////////////////////////////

	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];

	include "{$backwardseperator}dataAccess/Connector.php";
	include "{$backwardseperator}libraries/mail/mail.php";
	require_once $backwardseperator."class/cls_mail.php";
	require_once 	"../../../../class/warehouse/gatepass/cls_gatepass_get.php";
	require_once 	"../../../../class/finance/budget/cls_common_function_budget_get.php";
	require_once 	"../../../../class/finance/budget/entering/cls_budget_set.php";
	require_once 	"../../../../class/cls_commonFunctions_get.php";
    require_once $_SESSION['ROOT_PATH']."class/common/azureDBconnection.php";
	
	$objMail 				= new cls_create_mail($db);
	$obj_gatepass_get		= new cls_gatepass_get($db);
	$obj_comm_budget_get	= new cls_common_function_budget_get($db);
	$obj_budget_set			= new cls_budget_set($db);
	$obj_common				= new cls_commonFunctions_get($db);
    $objAzure = new cls_azureDBconnection();
	
	$programName='Gate Pass';
	$programCode='P0250';
	$issueApproveLevel = (int)getApproveLevel($programName);
	
	/////////// parameters /////////////////////////////

	
	$gatePassNo = $_REQUEST['gatePassNo'];
	$year 		= $_REQUEST['year'];
	$grnType 	= $_REQUEST['grnType'];
	
	if($_REQUEST['status']=='approve')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		$rollBackFlag=0;
		$rollBackMsg = '';
		
		if($grnType == 'Orderwise' || $grnType == 1){
			$sql_1 = "SELECT 
						trn_orderheader.intOrderNo,
						trn_orderheader.intOrderYear,
						trn_orderheader.intStatus,
						trn_orderheader.intApproveLevelStart
					FROM trn_orderheader 
					INNER JOIN ware_gatepassdetails ON trn_orderheader.intOrderNo = ware_gatepassdetails.strOrderNo AND
						trn_orderheader.intOrderYear = ware_gatepassdetails.intOrderYear
					WHERE
						ware_gatepassdetails.intGatePassNo = '$gatePassNo'
					AND ware_gatepassdetails.intGatePassYear = '$year'";
			$result_1 = $db->RunQuery2($sql_1);
			while($row=mysqli_fetch_array($result_1))
			{
				$intOrderNo   = $row['intOrderNo'];
				$intOrderYear = $row['intOrderYear'];
				$intStatus = $row['intStatus'];
				$intApproveLevelStart = $row['intApproveLevelStart'];
				
				if($intStatus <= 0 || ($intStatus>=$intApproveLevelStart+1))
				{
				$rollBackFlag=1;
				$sqlM=$sql;
				$rollBackMsg = "Approval error! Unapproved Order.";
				}
				
			}
		}
		
		$sql = "UPDATE `ware_gatepassheader` SET `intStatus`=intStatus-1 WHERE (intGatePassNo='$gatePassNo') AND (`intGatePassYear`='$year') and intStatus not in(0,1)";
		$result = $db->RunQuery2($sql);
		if((!$result) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sql;
			$rollBackMsg = "Approval error!";
		}
		
		if($result){
			$sql = "SELECT ware_gatepassheader.intUser,
					ware_gatepassheader.intStatus,
					ware_gatepassheader.intApproveLevels, 
					ware_gatepassheader.intCompanyId AS location,
					mst_locations.intCompanyId AS companyId,
					ware_gatepassheader.intGPToLocation AS gatepassToLoc
					FROM
					ware_gatepassheader
					Inner Join mst_locations ON ware_gatepassheader.intCompanyId = mst_locations.intId
					WHERE (intGatePassNo='$gatePassNo') AND (`intGatePassYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$createdUser = $row['intUser'];
			$status = $row['intStatus'];
			$savedLevels = $row['intApproveLevels'];
			$company = $row['companyId'];
			$location = $row['location'];
			$gatepassToLoc = $row['gatepassToLoc'];
			
			$approveLevel=$savedLevels+1-$status;
			$sqlI = "INSERT INTO `ware_gatepassheader_approvedby` (`intGatePassNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
				VALUES ('$gatePassNo','$year','$approveLevel','$userId',now(),0)";
			$resultI = $db->RunQuery2($sqlI);
			if((!$resultI) && ($rollBackFlag!=1)){
				$rollBackFlag=1;
				$sqlM=$sqlI;
				$rollBackMsg = "Approval error!";
			}
			
			if($status==1)
			{
				if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
				////////////// send mail to creater //////////////////
				sendFinlConfirmedEmailToUser($gatePassNo,$year,$objMail,$mainPath,$root_path);
				}
			}
		//--------------------------------------------------------------------
		if($status==1){//if final confirmation raised,insert to stock
				$sql1 = "SELECT 
						ware_gatepassdetails.strOrderNo,
						ware_gatepassdetails.intOrderYear,
						trn_orderdetails.strSalesOrderNo,
						trn_orderdetails.intSalesOrderId,
						ware_gatepassdetails.strStyleNo,
 						ware_gatepassdetails.dblQty,
						ware_gatepassdetails.intItemId,   
						mst_item.intBomItem 
						FROM 
						ware_gatepassdetails  
						left Join trn_orderdetails ON ware_gatepassdetails.strOrderNo = trn_orderdetails.intOrderNo AND ware_gatepassdetails.intOrderYear = trn_orderdetails.intOrderYear AND ware_gatepassdetails.strStyleNo = trn_orderdetails.intSalesOrderId
						Inner Join mst_item ON ware_gatepassdetails.intItemId = mst_item.intId
						WHERE
						ware_gatepassdetails.intGatePassNo =  '$gatePassNo' AND
						ware_gatepassdetails.intGatePassYear =  '$year'";
				$result1 = $db->RunQuery2($sql1);
				$toSavedQty=0;
				$transSavedQty=0;
				while($row=mysqli_fetch_array($result1))
				{
					$toSavedQty+=round($row['dblQty'],4);
					$item=$row['intItemId'];
					$Qty=$row['dblQty'];
					$orderNo=$row['strOrderNo'];
					$orderYear=$row['intOrderYear'];
					$salesOrderNo=$row['strSalesOrderNo'];
					$salesOrderId=$row['intSalesOrderId'];
					if($orderNo==0){
						$bomItemFlag=0;
					}
					else{
						$bomItemFlag=1;
					}
					
					//---------------------- budget Validation ----------------------------------------------------				
					if($rollBackFlag==0)
					{
						$validateArr	= validateBudgetBalance($gatePassNo,$year,$gatepassToLoc,$company,$location,$item,$Qty);
						if($validateArr['Flag']==1 && $rollBackFlag==0)
						{
							$rollBackFlag	= 1;
							$rollBackMsg	= $validateArr['msg'];
						}
					}
					//---------------------------------------------------------------------------------------------
					
					if($bomItemFlag==1){//bom item--get grn wise stock balance
						//$resultB = getGrnWiseStockBalance($location,$orderNo,$orderYear,$salesOrderId,$item);
						$resultB = getGrnWiseStockBalance_bulk($location,$item);
						while($rowB=mysqli_fetch_array($resultB)){
								if(($Qty>0) && ($rowB['stockBal']>0)){
									if(($rowB['stockBal']>=$Qty)){
									$saveQty=$Qty;
									$Qty=0;
									}
									else if($Qty>$rowB['stockBal']){
									$saveQty=$rowB['stockBal'];
									$Qty=$Qty-$saveQty;
									}
									$grnNo=$rowB['intGRNNo'];
									$grnYear=$rowB['intGRNYear'];
									$grnDate=$rowB['dtGRNDate'];
									$grnRate=$rowB['dblGRNRate'];
									$currency=loadCurrency($grnNo,$grnYear,$item);
									$saveQty=round($saveQty,4);		
									if($saveQty>0){	
									/*$sqlI = "INSERT INTO `ware_stocktransactions` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intOrderNo,intOrderYear,intSalesOrderId,intItemId,dblQty,strType,intUser,dtDate) 
										VALUES ('$company','$location','$gatePassNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$orderNo','$orderYear','$salesOrderId','$item','-$saveQty','GATEPASS','$userId',now())";*/
									$sqlI = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intOrderNo,intOrderYear,intSalesOrderId,intUser,dtDate) 
										VALUES ('$company','$location','$gatePassNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$item','-$saveQty','GATEPASS','$orderNo','$orderYear','$salesOrderId','$userId',now())";
									$resultI = $db->RunQuery2($sqlI);
									if($resultI){
										$transSavedQty+=$saveQty;
									}
									if((!$resultI) && ($rollBackFlag!=1)){
										$rollBackFlag=1;
										$sqlM=$sqlI;
										$rollBackMsg = "Approval error!";
									}
									
									}
								}
						}
					}
					else{//general item--get grn wise stock balance
						$resultG = getGrnWiseStockBalance_bulk($location,$item);
						while($rowG=mysqli_fetch_array($resultG)){
								if(($Qty>0) && ($rowG['stockBal']>0)){
									if($Qty<=$rowG['stockBal']){
									$saveQty=$Qty;
									$Qty=0;
									}
									else if($Qty>$rowG['stockBal']){
									$saveQty=$rowG['stockBal'];
									$Qty=$Qty-$saveQty;
									}
									$grnNo=$rowG['intGRNNo'];
									$grnYear=$rowG['intGRNYear'];
									$grnDate=$rowG['dtGRNDate'];
									$grnRate=$rowG['dblGRNRate'];	
									$currency=loadCurrency($grnNo,$grnYear,$item);
									$saveQty=round($saveQty,4);		
									if($saveQty>0){	
									$sqlI = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intUser,dtDate) 
										VALUES ('$company','$location','$gatePassNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$item','-$saveQty','GATEPASS','$userId',now())";
									$resultI = $db->RunQuery2($sqlI);
									if($resultI){
										$transSavedQty+=$saveQty;
									}
									if((!$resultI) && ($rollBackFlag!=1)){
										$rollBackFlag=1;
										$sqlM=$sqlI;
										$rollBackMsg = "Approval error!";
									}
									
									}
								}
						}
					}
				}
			}
		//--------------------------------------------------------------------
			if($status==$savedLevels){//if first confirmation raised,requisition table shld update
			}
		//---------------------------------------------------------------------------
		
		}
		
		if($status==1 ){
			//echo "sdfdsfdwf";
			//update_po_prn_details_for_gp_qty($gatePassNo,$year);
			update_po_prn_details_transactions($gatePassNo,$year);
		}
		
		$toSavedQty=round($toSavedQty,4);
		$transSavedQty=round($transSavedQty,4);
		
		if(($toSavedQty!=$transSavedQty) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$rollBackMsg = "Approval Error";
			$sqlM=$toSavedQty."-".$transSavedQty;
		}
		
		if(($rollBackFlag==0) &&($status==1)){
		    if($company == '1'){
		        updateAzureDBTablesOnGP($gatePassNo,$year,$db);
            }
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= "Final Approval raised sucessfully";
			$response['q'] 			= $sql;
		}
		else if($rollBackFlag==0){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= "Approval raised sucessfully";
			$response['q'] 			= '';
		}
		else if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= $sqlM;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sqlM;
		}
		$db->CloseConnection();		
		
		//----------------------------------------
			echo json_encode($response);
		//-----------------------------------------	
		
	}
	else if($_REQUEST['status']=='reject')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		$rollBackFlag=0;
		$rollBackMsg = '';
		
		//$grnApproveLevel = (int)getApproveLevel($programName);
		$sql = "SELECT  ware_gatepassheader.intUser,ware_gatepassheader.intStatus,ware_gatepassheader.intApproveLevels FROM ware_gatepassheader WHERE  (intGatePassNo='$gatePassNo') AND (`intGatePassYear`='$year')";
		$results = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($results);
		$createdUser = $row['intUser'];
		$status = $row['intStatus'];
		$savedLevels = $row['intApproveLevels'];
		
		$sql = "UPDATE `ware_gatepassheader` SET `intStatus`=0 WHERE (intGatePassNo='$gatePassNo') AND (`intGatePassYear`='$year')";
		$result = $db->RunQuery2($sql);
		if((!$result) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sql;
			$rollBackMsg = "Rejection error!";
		}
		
		//$sql = "DELETE FROM `ware_gatepassheader_approvedby` WHERE (`intGatePassNo`='$gatePassNo') AND (`intYear`='$year')";
		//$result2 = $db->RunQuery($sql);
		
		$sqlI = "INSERT INTO `ware_gatepassheader_approvedby` (`intGatePassNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
			VALUES ('$gatePassNo','$year','0','$userId',now(),0)";
		$resultI = $db->RunQuery2($sqlI);
		if((!$resultI) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sqlI;
			$rollBackMsg = "Rejection error!";
		}
		
		if($resultI)
		{
			if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
			sendRejecedEmailToUser($gatePassNo,$year,$objMail,$mainPath,$root_path);
			}
		}
		//---------------------------------------------------------------------------------
/*		if($status==1){//if final confirmation raised,insert to stock
				$sql1 = "SELECT
						ware_gatepassdetails.intItemId,
						ware_gatepassdetails.dblQty,
						ware_gatepassdetails.strOrderNo,
						ware_gatepassdetails.intOrderYear, 
						trn_orderdetails.strSalesOrderNo,
						trn_orderdetails.intSalesOrderId,
						ware_gatepassdetails.strStyleNo, 
						mst_item.intBomItem 
						FROM 
						ware_gatepassdetails  
						left Join trn_orderdetails ON ware_gatepassdetails.strOrderNo = trn_orderdetails.intOrderNo AND ware_gatepassdetails.intOrderYear = trn_orderdetails.intOrderYear AND ware_gatepassdetails.strStyleNo = trn_orderdetails.intSalesOrderId
						Inner Join mst_item ON ware_gatepassdetails.intItemId = mst_item.intId
						WHERE
						ware_gatepassdetails.intGatePassNo =  '$gatePassNo' AND
						ware_gatepassdetails.intGatePassYear =  '$year'";
				$result1 = $db->RunQuery($sql1);
				while($row=mysqli_fetch_array($result1))
				{
					$orderNo=$row['strOrderNo'];
					$orderYear=$row['intOrderYear'];
					$salesOrderNo=$row['strSalesOrderNo'];
					$salesOrderId=$row['intSalesOrderId'];
					$styleNo=$row['strStyleNo'];
					$item=$row['intItemId'];
					$Qty=$row['dblQty'];
		
					if($orderNo==0){
						$bomItemFlag=0;
					}
					else{
						$bomItemFlag=1;
					}
					
					if($bomItemFlag==1){//bom item 
					$sqlI = "INSERT INTO `ware_stocktransactions` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intOrderNo,intOrderYear,intSalesOrderId,intItemId,dblQty,strType,intUser,dtDate) 
						VALUES ('$company','$location','$gatePassNo','$year','$orderNo','$orderYear','$salesOrderId','$item','$Qty','CGATEPASS','$userId',now())";
					}
					else{
					$sqlI = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intItemId,dblQty,strType,intUser,dtDate) 
						VALUES ('$company','$location','$gatePassNo','$year','$item','$Qty','CGATEPASS','$userId',now())";
					}
					$resultI = $db->RunQuery($sqlI);
				}
			}
*/		//---------------------------------------------------------------------------------
			if($savedLevels>=$status){//if atleast one confirmation processed, roll back requisition table-
			}
		//-----------------------------------------------------------------------------
		//------------------------------------
		if($rollBackFlag==0){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= "Rejection raised sucessfully";
			$response['q'] 			= '';
		}
		else if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= $sqlM;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sqlM;
		}
		$db->CloseConnection();		
		
		//----------------------------------------
			echo json_encode($response);
		//-----------------------------------------	
	}
//--------------------------------------------------------------
function getGrnWiseStockBalance_bulk($location,$item)
{
	global $db;
	  $sql = "SELECT
			Sum(ware_stocktransactions_bulk.dblQty) AS stockBal,
			ware_stocktransactions_bulk.intGRNNo,
			ware_stocktransactions_bulk.intGRNYear,
			ware_stocktransactions_bulk.dtGRNDate,
			ware_stocktransactions_bulk.dblGRNRate
			FROM ware_stocktransactions_bulk
			WHERE
			ware_stocktransactions_bulk.intItemId =  '$item' AND
			ware_stocktransactions_bulk.intLocationId =  '$location'
			GROUP BY
			ware_stocktransactions_bulk.intItemId,
			ware_stocktransactions_bulk.intGRNNo,
			ware_stocktransactions_bulk.intGRNYear,
			ware_stocktransactions_bulk.dtGRNDate, 
			ware_stocktransactions_bulk.dblGRNRate 
			ORDER BY
			ware_stocktransactions_bulk.dtGRNDate ASC";
			
			//echo $sql;

	$result = $db->RunQuery2($sql);
	return $result;	
}
//--------------------------------------------------------------
function getGrnWiseStockBalance($location,$orderNo,$orderYear,$salesOrderId,$item)
{
	global $db;
	 $sql = "SELECT
			Sum(ware_stocktransactions.dblQty) AS stockBal,
			ware_stocktransactions.intGRNNo,
			ware_stocktransactions.intGRNYear,
			ware_stocktransactions.dtGRNDate,
			ware_stocktransactions.dblGRNRate
			FROM ware_stocktransactions 
			WHERE
			ware_stocktransactions.intItemId =  '$item' AND
			ware_stocktransactions.intLocationId =  '$location' AND 
			ware_stocktransactions.intOrderNo='$orderNo' AND 
			ware_stocktransactions.intOrderYear='$orderYear' AND 
			ware_stocktransactions.intSalesOrderId='$salesOrderId' 
			GROUP BY
			ware_stocktransactions.intItemId,
			ware_stocktransactions.intGRNNo,
			ware_stocktransactions.intGRNYear, 
			ware_stocktransactions.dtGRNDate,
			ware_stocktransactions.dblGRNRate 
			ORDER BY
			ware_stocktransactions.dtGRNDate ASC";

	$result = $db->RunQuery2($sql);
	return $result;	
}
//--------------------------------------------------------------
function loadCurrency($grnNo,$grnYear,$item)
{
	global $db;
	
	if(($grnNo!=0) && ($grnYear!=0)){
	  $sql = "SELECT
			trn_poheader.intCurrency as currency 
			FROM
			ware_grnheader
			Inner Join trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
			WHERE
			ware_grnheader.intGrnNo =  '$grnNo' AND
			ware_grnheader.intGrnYear =  '$grnYear'";
	}
	else{
	$sql = "SELECT
			mst_item.intCurrency as currency 
			FROM mst_item
			WHERE
			mst_item.intId =  '$item'";
	}

	$result = $db->RunQuery2($sql);
	$row=mysqli_fetch_array($result);
	return $row['currency'];;	
}
//--------------------------------------------------------
function sendFinlConfirmedEmailToUser($serialNo,$year,$objMail,$mainPat,$root_path){
	global $db;
				
		    $sql = "SELECT
			sys_users.strFullName,
			sys_users.strEmail 
			FROM 
			ware_gatepassheader
			Inner Join sys_users ON ware_gatepassheader.intUser = sys_users.intUserId
			WHERE
			ware_gatepassheader.intGatePassNo =  '$serialNo' AND
			ware_gatepassheader.intGatePassYear =  '$year'";

				
		 $result = $db->RunQuery2($sql);
		 
		$row=mysqli_fetch_array($result);
			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="CONFIRMED GATE PASS ('$serialNo'/'$year')"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST = NULL;
			
			$_REQUEST['program']='GATE PASS';
			$_REQUEST['field1']='Gate Pass No';
			$_REQUEST['field2']='Gate Pass Year';
			$_REQUEST['field3']='';
			$_REQUEST['field4']='';
			$_REQUEST['field5']='';
			$_REQUEST['value1']=$serialNo;
			$_REQUEST['value2']=$year;
			$_REQUEST['value3']='';
			$_REQUEST['value4']='';
			$_REQUEST['value5']='';
			
			$_REQUEST['subject']="CONFIRMED GATE PASS ('$serialNo'/'$year')";
			
			$_REQUEST['statement1']="Approved this";
			$_REQUEST['statement2']="to view this";
			$_REQUEST['link']=base64_encode($mainPath."presentation/warehouse/gatepass/listing/rptGatepass.php?gatePassNo=$serialNo&year=$year&approveMode=0"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}
//--------------------------------------------------------
function sendRejecedEmailToUser($serialNo,$year,$objMail,$mainPat,$root_path){
	global $db;
				
		    $sql = "SELECT
			sys_users.strFullName,
			sys_users.strEmail 
			FROM 
			ware_gatepassheader
			Inner Join sys_users ON ware_gatepassheader.intUser = sys_users.intUserId
			WHERE
			ware_gatepassheader.intGatePassNo =  '$serialNo' AND
			ware_gatepassheader.intGatePassYear =  '$year'";

				
		 $result = $db->RunQuery2($sql);
		 
		$row=mysqli_fetch_array($result);
			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="REJECTED GATE PASS('$serialNo'/'$year')"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST = NULL;
			
			$_REQUEST['program']='GATE PASS';
			$_REQUEST['field1']='Gate Pass No';
			$_REQUEST['field2']='Gate Pass Year';
			$_REQUEST['field3']='';
			$_REQUEST['field4']='';
			$_REQUEST['field5']='';
			$_REQUEST['value1']=$serialNo;
			$_REQUEST['value2']=$year;
			$_REQUEST['value3']='';
			$_REQUEST['value4']='';
			$_REQUEST['value5']='';
			
			$_REQUEST['subject']="REJECTED GATE PASS ('$serialNo'/'$year')";
			
			$_REQUEST['statement1']="Rejected this";
			$_REQUEST['statement2']="to view this";
			$_REQUEST['link']=base64_encode($mainPath."presentation/warehouse/gatepass/listing/rptGatepass.php?gatePassNo=$serialNo&year=$year&approveMode=0"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}
//--------------------------------------------------------
function validateBudgetBalance($serialNo,$year,$gatePassTo,$company,$location,$itemId,$Qty)
{
	global $obj_gatepass_get;
	global $obj_comm_budget_get;
	global $obj_common;
	
	$gp_item_tot_U			= 0;
	$gp_sub_cat_tot_U		= 0;
	$gp_item_tot_A			= 0;
	$gp_sub_cat_tot_A		= 0;
	
	$saveStatus 			= true;	
	$saveQty				= 0;
	$header_array			= $obj_gatepass_get->get_header($serialNo,$year,'RunQuery2');
	$subCatId				= $obj_gatepass_get->getSubCategory($itemId,'RunQuery2');
	$results_fin_year		= $obj_comm_budget_get->loadFinanceYear_with_selected_date($company,date('Y-m-d'),'RunQuery2');
	$row_fin_year			= mysqli_fetch_array($results_fin_year);
	$budg_year				= $row_fin_year['intId'];
	$sub_cat_budget_flag	= $obj_comm_budget_get->check_sub_category_budgeted_or_not($gatePassTo,$subCatId,'RunQuery2');
	
	$check					= $obj_comm_budget_get->checkBudgetApprovalDepWise($gatePassTo,'',$budg_year,'RunQuery2');
	
	if(!$check && $saveStatus && $sub_cat_budget_flag!='2')
	{
		$saveStatus			= false;
		$data['Flag']		= 1;
		$data['msg']		= "No approve budget for all department which belong to gate pass to location and current finance year ";
	}
	if($sub_cat_budget_flag==0)
	{
		$budget_type		= $obj_comm_budget_get->load_budget_type($gatePassTo,$subCatId,$itemId,'RunQuery2');
		$budget_bal			= $obj_comm_budget_get->load_budget_balance($gatePassTo,'',$budg_year,'',$itemId,date('m'),'RunQuery2');
		$result_itemPrice	= $obj_comm_budget_get->get_price_for_item($location,$itemId,'RunQuery2');
		
		while($row = mysqli_fetch_array($result_itemPrice))
		{
			if($row['stockBal']>=$Qty)
			{
				$saveQty 	= $Qty;
				$Qty		= 0;
			}
			else if($Qty>$row['stockBal'])
			{
				$saveQty	= $row['stockBal'];
				$Qty		= $Qty-$saveQty;
			}
			if($saveQty>0)
			{
				if($budget_type == 'U')
					$gp_item_tot_U += $saveQty;
				else
					$gp_item_tot_A += $saveQty*$row['price'];
				
				$budget_bal_A 	= $budget_bal['balAmount'];
				if(($gp_item_tot_A > $budget_bal_A) && $saveStatus)
				{
					$saveStatus		= false;
					$data['Flag']	= 1;
					$data['msg']	= "There is no budget balance for this month for item '".$obj_common->get_item_name($itemId,'RunQuery2')."' - ".$budget_bal_A;
				}
				$budget_bal_U 	= $budget_bal['balUnits'];
				
				if(($gp_item_tot_U > $budget_bal_U) && $saveStatus)
				{
					$saveStatus		= false;
					$data['Flag']	= 1;
					$data['msg']	= "There is no budget balance for this month for item '".$obj_common->get_item_name($itemId,'RunQuery2')."' - ".$budget_bal_U;
				}	
			}
		}
		
	}
	else if($sub_cat_budget_flag==1)
	{
		$budget_type		= $obj_comm_budget_get->load_budget_type($gatePassTo,$subCatId,0,'RunQuery2');
		$budget_bal			= $obj_comm_budget_get->load_budget_balance($gatePassTo,'',$budg_year,$subCatId,0,date('m'),'RunQuery2');
		$result_itemPrice	= $obj_comm_budget_get->get_price_for_item($location,$itemId,'RunQuery2');	
		
		while($row = mysqli_fetch_array($result_itemPrice))
		{
			if($row['stockBal']>=$Qty)
			{
				$saveQty 	= $Qty;
				$Qty		= 0;
			}
			else if($Qty>$row['stockBal'])
			{
				$saveQty	= $row['stockBal'];
				$Qty		= $Qty-$saveQty;
			}
			if($saveQty>0)
			{
				if($budget_type == 'U')
					$gp_sub_cat_tot_U += $saveQty;
				else
					$gp_sub_cat_tot_A += $saveQty*$row['price'];
				
				$budget_bal_A 	= $budget_bal['balAmount'];
				if(($gp_item_tot_A > $budget_bal_A) && $saveStatus)
				{
					$saveStatus		= false;
					$data['Flag']	= 1;
					$data['msg']	= "There is no budget balance for this month for item '".$obj_common->get_sub_category_name($itemId,'RunQuery2')."' - ".$budget_bal_A;
				}
				$budget_bal_U 	= $budget_bal['balUnits'];
				if(($gp_item_tot_U > $budget_bal_U) && $saveStatus)
				{
					$saveStatus		= false;
					$data['Flag']	= 1;
					$data['msg']	= "There is no budget balance for this month for item '".$obj_common->get_item_name($itemId,'RunQuery2')."' - ".$budget_bal_U;
				}
			}			
				
		}	
	}
	return $data;
}

function update_po_prn_details_for_gp_qty($gatePassNo,$year){
	global $db;
	$sql="SELECT
			ware_gatepassdetails.strOrderNo,
			ware_gatepassdetails.intOrderYear,
			ware_gatepassdetails.strStyleNo,
			ware_gatepassdetails.intItemId,
			ware_gatepassdetails.dblQty,
			ware_gatepassheader.intCompanyId AS `from`,
			ware_gatepassheader.intGPToLocation AS `to`,
			trn_po_prn_details_sales_order.ORDER_NO AS saved_pri
			FROM
			ware_gatepassdetails
			INNER JOIN ware_gatepassheader ON ware_gatepassdetails.intGatePassNo = ware_gatepassheader.intGatePassNo AND ware_gatepassdetails.intGatePassYear = ware_gatepassheader.intGatePassYear
			left JOIN trn_po_prn_details_sales_order ON ware_gatepassdetails.strOrderNo = trn_po_prn_details_sales_order.ORDER_NO AND ware_gatepassdetails.intOrderYear = trn_po_prn_details_sales_order.ORDER_YEAR AND ware_gatepassdetails.strStyleNo = trn_po_prn_details_sales_order.SALES_ORDER AND ware_gatepassdetails.intItemId = trn_po_prn_details_sales_order.ITEM
			WHERE
			ware_gatepassdetails.intGatePassNo = '$gatePassNo' AND
			ware_gatepassdetails.intGatePassYear = '$year'
			";
	$result = $db->RunQuery2($sql);
	while($row	=mysqli_fetch_array($result)){
		$orderNo			= $row['strOrderNo'];
		$orderYear			= $row['intOrderYear'];
		$so					= $row['strStyleNo'];
		$item				= $row['intItemId'];
		$totQty				= $row['dblQty'];
		$saved_pri				= $row['saved_pri'];
		
		if($saved_pri>0){
		//PRI ITEMS
 		if($row['from']==2){
		$sql_u	="UPDATE `trn_po_prn_details_sales_order` SET `GP_OUT_FROM_HO`=IFNULL(`GP_OUT_FROM_HO`,0)+'$totQty' WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`SALES_ORDER`='$so') AND (`ITEM`='$item')";
		}
		else if($row['to']==2){
		$sql_u	="UPDATE `trn_po_prn_details_sales_order` SET `GP_IN_TO_HO`=IFNULL(`GP_IN_TO_HO`,0)+'$totQty' WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`SALES_ORDER`='$so') AND (`ITEM`='$item')";
		}
		
		$result_u = $db->RunQuery2($sql_u);
		}
		else{
		//if(!$result_u){ //NON PRI ITEM
		  $sql_u	="UPDATE `trn_po_prn_details_sales_order_additional_mrn_items` SET `GP_IN_TO_HO`=IFNULL(`GP_IN_TO_HO`,0)+'$totQty' WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`SALES_ORDER`='$so') AND (`ITEM`='$item')";
		$result_u = $db->RunQuery2($sql_u);
		}
		
 	}
}

function update_po_prn_details_transactions($gatePassNo,$year){
	global $db;
	$sql="SELECT
			ware_gatepassdetails.strOrderNo,
			ware_gatepassdetails.intOrderYear,
			ware_gatepassdetails.strStyleNo,
			ware_gatepassdetails.intItemId,
			ware_gatepassdetails.dblQty,
			ware_gatepassheader.intCompanyId AS `from`,
			ware_gatepassheader.intGPToLocation AS `to`,
			trn_po_prn_details_sales_order_stock_transactions.ORDER_NO AS saved_pri
			FROM
			ware_gatepassdetails
			INNER JOIN ware_gatepassheader ON ware_gatepassdetails.intGatePassNo = ware_gatepassheader.intGatePassNo AND ware_gatepassdetails.intGatePassYear = ware_gatepassheader.intGatePassYear
			LEFT JOIN trn_po_prn_details_sales_order_stock_transactions ON ware_gatepassdetails.strOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO AND ware_gatepassdetails.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR AND ware_gatepassdetails.strStyleNo = trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER AND ware_gatepassdetails.intItemId = trn_po_prn_details_sales_order_stock_transactions.ITEM  
			AND (trn_po_prn_details_sales_order_stock_transactions.TRANSACTION='GATE_PASS' ) 
			AND trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID =ware_gatepassheader.intCompanyId
			WHERE
			ware_gatepassdetails.intGatePassNo = '$gatePassNo' AND
			ware_gatepassdetails.intGatePassYear = '$year'
			";
	$result = $db->RunQuery2($sql);
	while($row	=mysqli_fetch_array($result)){
		$orderNo			= $row['strOrderNo'];
		$orderYear			= $row['intOrderYear'];
		$so					= $row['strStyleNo'];
		$item				= $row['intItemId'];
		$totQty				= $row['dblQty'];
		$saved_pri				= $row['saved_pri'];
		$location			= $row['from'];
		if($saved_pri > 0){
        //PRI ITEM
            $sql_u	="UPDATE `trn_po_prn_details_sales_order_stock_transactions` SET `QTY`=IFNULL(`QTY`,0)+'$totQty' 
		  WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`SALES_ORDER`='$so') AND (`ITEM`='$item') AND (`TRANSACTION`='GATE_PASS') AND (`LOCATION_ID`='$location')";
            $result_u = $db->RunQuery2($sql_u);
        }
		else{
			$sql_u2	="INSERT INTO `trn_po_prn_details_sales_order_stock_transactions` (`ORDER_NO`, `ORDER_YEAR`, `SALES_ORDER`, `ITEM`, `LOCATION_ID`, `TRANSACTION`, `QTY`) VALUES ('$orderNo', '$orderYear', '$so', '$item', '$location', 'GATE_PASS', '$totQty')";
			$result_u2 	= $db->RunQuery2($sql_u2);
		}
		
 	}
}

function updateAzureDBTablesOnGP($gatePassNo, $gatePassYear, $db)
{
    global $objAzure;
    $environment = $objAzure->getEnvironment();
    $table = 'Inventory'.$environment;

    $azure_connection = $objAzure->connectAzureDB();
    $sql_select = "SELECT
                    CONCAT(
                        GPD.intGatePassNo,
                        '/',
                        GPD.intGatePassYear
                    ) AS GP_NO,
                    GPD.intItemId,
                    Sum(GPD.dblQty) AS dblQty,
                    mst_item.strCode AS item_code,
                    mst_item.strName AS description,
                    mst_item.dblLastPrice AS price,
                    mst_maincategory.strName AS mainCategory,
                    mst_subcategory.strName AS subCategory,
                    mst_maincategory.intSendDetails AS sendMainCat,
                    mst_subcategory.intSendDetails AS sendSubCat,
                    GPH.intCompanyId AS location,
                    (
                        SELECT
                            round(
                                sum(
                                    (
                                        ware_grnheader.dblExchangeRate * ware_stocktransactions_bulk.dblQty * ware_stocktransactions_bulk.dblGRNRate
                                    ) / 1.0000
                                ),
                                2
                            )
                        FROM
                            ware_stocktransactions_bulk
                        LEFT JOIN ware_grnheader ON ware_grnheader.intGrnNo = ware_stocktransactions_bulk.intGRNNo
                        AND ware_grnheader.intGrnYear = ware_stocktransactions_bulk.intGRNYear
                        WHERE
                            ware_stocktransactions_bulk.intDocumentNo = GPD.intGatePassNo
                        AND ware_stocktransactions_bulk.intDocumntYear = GPD.intGatePassYear
                        AND ware_stocktransactions_bulk.intItemId = GPD.intItemId
                        AND ware_stocktransactions_bulk.strType = 'GATEPASS'
                        GROUP BY
                            ware_stocktransactions_bulk.intDocumentNo,
                            ware_stocktransactions_bulk.intDocumntYear,
                            ware_stocktransactions_bulk.intItemId
                    ) AS amount
                FROM
                    ware_gatepassdetails GPD
                INNER JOIN ware_gatepassheader AS GPH ON GPD.intGatePassNo = GPH.intGatePassNo
                AND GPD.intGatePassYear = GPH.intGatePassYear
                INNER JOIN mst_item ON GPD.intItemId = mst_item.intId
                INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
                INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
                WHERE
                    GPD.intGatePassNo = '$gatePassNo'
                AND GPD.intGatePassYear = '$gatePassYear'
                GROUP BY
                    GPD.intItemId";

    $result = $db->RunQuery2($sql_select);
    $line_no = rand(100,1000);
    while ($row = mysqli_fetch_array($result)) {
        $gp_no = $row['GP_NO'];
        $item_category = $row['mainCategory'];
        $item_subcategory = $row['subCategory'];
        $item_code = $row['item_code'];
        $description_sql = $db->escapeString($row['description']);
        $description = str_replace("'","''",$row['description']);
        $unit_price = $row['price'];
        $qty = $row['dblQty'];
        $amount = $row['amount'];
        $location = $row['location'];
        $line_no++;
        $successHeader = '0';
        $postingDate = date('Y-m-d H:i:s');
        $sendDetails = ($row['sendMainCat'] == 1 && $row['sendSubCat'] == 1)? true:false;

        if($sendDetails) {
            $sql_azure_header = "INSERT INTO $table (Document_Category, Document_No, Line_No, External_Doc_No, Posting_Date, Item_Category, Item_Sub_Category, Item_Code, Description, Amount, Location) VALUES ('GatePass', '$gp_no', '$line_no', '$gp_no', '$postingDate', '$item_category', '$item_subcategory', '$item_code',STRING_ESCAPE('$description','json'), '$amount','$location')";
            if ($azure_connection) {
                $getResultsHeader = $objAzure->runQuery($azure_connection, $sql_azure_header);
                if ($getResultsHeader != FALSE) {
                    $successHeader = '1';
                }
            }
            $sql_db_header = "INSERT INTO trn_financemodule_inventory (Document_Category, Document_No, Line_No, External_Doc_No, Posting_Date, Item_Category, Item_Sub_Category, Item_Code, Description, Amount, Location, deliveryStatus, deliveryDate) VALUES ('GatePass', '$gp_no', '$line_no', '$gp_no', '$postingDate', '$item_category', '$item_subcategory', '$item_code', '$description_sql', '$amount','$location','$successHeader', NOW())";
            $result_db_header = $db->RunQuery2($sql_db_header);
        }

    }

}

function getNextSysNo($db, $companyId, $transaction_type){
    $current_serial_no = $companyId.'00000';
    $sql = "SELECT `$transaction_type` FROM `trn_financemodule_sysNo` WHERE `COMPANY_ID` = '$companyId'";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    if($row[$transaction_type] != ''){
        $current_serial_no = $row[$transaction_type];
    }
    //update next serial no
    $sql_update = "UPDATE `trn_financemodule_sysNo` SET `$transaction_type`='$current_serial_no'+1 WHERE `COMPANY_ID`='$companyId'";
    $result = $db->RunQuery2($sql_update);
    return $current_serial_no;
}

?>
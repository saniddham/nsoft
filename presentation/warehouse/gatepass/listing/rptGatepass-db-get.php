<?php
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";

	$programName='Gate Pass';
	$programCode='P0250';
	$issueApproveLevel = (int)getApproveLevel($programName);
	

 if($requestType=='getValidation')
	{
		$gpNo  = $_REQUEST['gpNo'];
		$gpNoArray=explode("/",$gpNo);
		
		///////////////////////
$sql = "SELECT ware_gatepassheader.intStatus,
		ware_gatepassheader.intApproveLevels, 
		ware_gatepassheader.intCompanyId AS location,
		mst_locations.intCompanyId AS companyId 
		FROM
		ware_gatepassheader
		Inner Join mst_locations ON ware_gatepassheader.intCompanyId = mst_locations.intId
		WHERE
		ware_gatepassheader.intGatePassNo =  '$gpNoArray[0]' AND 
		ware_gatepassheader.intGatePassYear =  '$gpNoArray[1]'";

		$result = $db->RunQuery($sql);
		$errorFlg=0;
		$row=mysqli_fetch_array($result);
		$status = $row['intStatus'];
		$approveLevels = $row['intApproveLevels'];
		$company = $row['companyId'];
		$location = $row['location'];
		
		$confirmatonMode=loadConfirmatonMode($programCode,$status,$approveLevels,$userId);
		
		if($row['intStatus']==1){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="Final confirmation of this Gate Pass No is already raised"; 
		}
		else if($row['intStatus']==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="This Gate Pass No is Rejected"; 
		}
		else if($confirmatonMode==0){// 
			$errorFlg = 1;
			$msg ="No Permission to Approve"; 
		}
		else{//confirmation has been raised
			$errorFlg = 0;
		}
		////////////////////////
	
		if($errorFlg == 0){
		
		$result = loadGatePassDetails($gpNoArray[0],$gpNoArray[1]);
		$errorFlg=0;
		$msg ="Gate Pass Qty is greater than Stock Balance for items"; 
		
		$totTosaveQty=0;
		$totSavableQty=0;
		while($row=mysqli_fetch_array($result))
		{	
			 $status=$row['intStatus'];
			 $Qty=$row['dblQty'];
			 
			//$location 	= $_SESSION['CompanyID'];
			//$company 	= $_SESSION['headCompanyId'];
			
			if($row['strOrderNo']==0){
				$bomItemFlag=0;
			}
			else{
				$bomItemFlag=1;
			}
			
			if($bomItemFlag==0){//general item
			$stockBalQty=getStockBalance_bulk($location,$row['intItemId']);
			}
			else{//bom item
			//$stockBalQty=getStockBalance($company,$location,$row['strOrderNo'],$row['intOrderYear'],$row['intSalesOrderId'],$row['intItemId']);
			$stockBalQty=getStockBalance_bulk($location,$row['intItemId']);
			}
			$confirmedGPqty=get_gpQty($location,$row['strOrderNo'],$row['intOrderYear'],$row['intSalesOrderId'],$row['intItemId']);//but not raised final confirmation
			
				if($status==($approveLevels+1))
				$balQtyToGP = $stockBalQty-$confirmedGPqty;
				else
				$balQtyToGP = $stockBalQty-$confirmedGPqty+$Qty;

			if(round($Qty,4)>round($balQtyToGP,4)){
				$errorFlg=1;
			if($bomItemFlag==1){//bom item
					$msg .="<br>".$row['strOrderNo']."/".$row['intOrderYear']."-".$row['strSalesOrderNo']."-".$row['itemName']." =".$balQtyToGP;
				}	
				else{
					$msg .="<br>"."-".$row['itemName']." =".$balQtyToGP;
				}
			}
			
			
			$totTosaveQty +=$Qty;
			$totSavableQty +=$balQtyToGP;
		}//end of while
		}
	
	$totTosaveQty = round($totTosaveQty);
	$totSavableQty = round($totSavableQty);
	if(($totTosaveQty > $totSavableQty) && ($errorFlg!=1)){
		$errorFlg=1;
		$msg ="No Stock balance to confirm.";
	}

	
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);
	}
//---------------------------
else if($requestType=='validateRejecton')
	{
		$gpNo  = $_REQUEST['gpNo'];
		$gpNoArray=explode("/",$gpNo);
		
		///////////////////////
		 $sql = "SELECT
		ware_gatepassheader.intStatus, 
		ware_gatepassheader.intApproveLevels 
		FROM ware_gatepassheader
		WHERE
		ware_gatepassheader.intGatePassNo =  '$gpNoArray[0]' AND 
		ware_gatepassheader.intGatePassYear =  '$gpNoArray[1]'";

		$result = $db->RunQuery($sql);
		$errorFlg=0;
		$msg =""; 
		$row=mysqli_fetch_array($result);
		
		$rejectionMode=loadRejectionMode($programCode,$row['intStatus'],$row['intApproveLevels'],$userId);
		
		if($row['intStatus']==1){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="Final confirmation of this Gate Pass Note is already raised.So cant reject"; 
		}
		else if($row['intStatus']==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="This  Gate Pass Note is already Rejected"; 
		}
		else if($rejectionMode==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="No Permission to reject this Gate Pass Note"; 
		}
		else{//confirmation has been raised
			$errorFlg = 0;
			
		}

	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);
	}

//---------------------------

	//-----------------------------------------------------------
	function getStockBalance($company,$location,$orderNo,$orderYear,$salesOrderNo,$item)
	{
		global $db;
		  $sql = "SELECT
				Sum(ware_stocktransactions.dblQty) as stockBal 
				FROM ware_stocktransactions
				WHERE
				ware_stocktransactions.intCompanyId =  '$company' AND
				ware_stocktransactions.intLocationId =  '$location' AND
				ware_stocktransactions.intOrderNo =  '$orderNo' AND
				ware_stocktransactions.intOrderYear =  '$orderYear' AND
				ware_stocktransactions.intSalesOrderId =  '$salesOrderNo' AND
				ware_stocktransactions.intItemId =  '$item'";

		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return val($rows['stockBal']);	
	}
	//--------------------------------------------------------------
	function getStockBalance_bulk($location,$item)
	{
		global $db;
		   $sql = "SELECT
				Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$location'
				GROUP BY
				ware_stocktransactions_bulk.intItemId";

		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
	function loadGatePassDetails($serialNo,$year){
		global $db;
	   $sql = "SELECT
			ware_gatepassdetails.strOrderNo,
			ware_gatepassdetails.intOrderYear,
			trn_orderdetails.strSalesOrderNo,
			trn_orderdetails.intSalesOrderId,
			ware_gatepassdetails.strStyleNo,
			ware_gatepassdetails.intItemId,
			ware_gatepassdetails.dblQty,
			ware_gatepassheader.intStatus,
			ware_gatepassheader.intApproveLevels,
			mst_item.intBomItem,
			mst_item.strName AS itemName
			FROM
			ware_gatepassdetails 
			left Join trn_orderdetails ON ware_gatepassdetails.strOrderNo = trn_orderdetails.intOrderNo AND ware_gatepassdetails.intOrderYear = trn_orderdetails.intOrderYear AND ware_gatepassdetails.strStyleNo = trn_orderdetails.intSalesOrderId
			Inner Join ware_gatepassheader ON ware_gatepassdetails.intGatePassNo = ware_gatepassheader.intGatePassNo AND ware_gatepassdetails.intGatePassYear = ware_gatepassheader.intGatePassYear
			Inner Join mst_item ON ware_gatepassdetails.intItemId = mst_item.intId
			WHERE
			ware_gatepassdetails.intGatePassNo =  '$serialNo' AND
			ware_gatepassdetails.intGatePassYear =  '$year'";
		
			return $results = $db->RunQuery($sql);
	}

//------------------------------function loadRejectionMode-------------------
function loadRejectionMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$rejectMode=0;
	 $sqlp = "SELECT
		menupermision.intReject 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intReject']==1){
	 if($intStatus!=0){
	 $rejectMode=1;
	 }
	}
	 
	return $rejectMode;
}
//------------------------------------------------------------
function get_gpQty($location,$orderNo,$orderYear,$styleNo,$itemId){
	global $db;
	 $sql1 = "SELECT
				Sum(ware_gatepassdetails.dblQty) as gpQty
				FROM
				ware_gatepassheader
				Inner Join ware_gatepassdetails ON ware_gatepassheader.intGatePassNo = ware_gatepassdetails.intGatePassNo AND ware_gatepassheader.intGatePassYear = ware_gatepassdetails.intGatePassYear
				WHERE
				ware_gatepassdetails.strOrderNo =  '$orderNo' AND
				ware_gatepassdetails.intOrderYear =  '$orderYear' AND
				ware_gatepassdetails.strStyleNo =  '$styleNo' AND
				ware_gatepassdetails.intItemId =  '$itemId' AND
				ware_gatepassheader.intStatus >  '1' AND
				ware_gatepassheader.intStatus <= ware_gatepassheader.intApproveLevels AND
				ware_gatepassheader.intCompanyId =  '$location'";	
	$result1 = $db->RunQuery($sql1);
	$row1=mysqli_fetch_array($result1);
	$gpQty=$row1['gpQty'];
	return $gpQty;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	 $sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
//--------------------------------------------------------

?>

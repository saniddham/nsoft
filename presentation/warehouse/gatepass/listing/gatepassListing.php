<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
date_default_timezone_set('Asia/Kolkata');

require_once	"libraries/jqgrid2/inc/jqgrid_dist.php";

$company 		= $_SESSION['headCompanyId'];
$location		= $_SESSION['CompanyID'];

$programName	='Gate Pass';
$programCode	='P0250';
$intUser  		= $_SESSION["userId"];

$reportMenuId	='901';
$menuId			='250';


$userDepartment=getUserDepartment($intUser);
$approveLevel = (int)getMaxApproveLevel();

################################################################
################## new part ####################################
################################################################
///// create where part ///////////////////
$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];
//print_r($arr);
$where_string = '';
$where_array = array(
				'Status'=>'tb1.intStatus',
				'Gate_Pass_Type'=>'tb1.gpType',
				'Gate_Pass_No'=>'tb1.intGatePassNo',
				'Gate_Pass_Year'=>'tb1.intGatePassYear',
				'Gate_Pass_To'=>'(SELECT
							mst_locations.strName
							FROM
							ware_gatepassheader
							Inner Join mst_locations ON ware_gatepassheader.intGPToLocation = mst_locations.intId
							WHERE
							ware_gatepassheader.intGatePassNo =  tb1.intGatePassNo AND
							ware_gatepassheader.intGatePassYear =  tb1.intGatePassYear)',
				'Date'=>'tb1.datdate',
				'User'=>'sys_users.strUserName'
				);
$arr_status = array('Approved'=>'1','Rejected'=>'0','Pending'=>'2');
$arr_type = array('Orderwise'=>'1','General'=>'0');
foreach($arr as $k=>$v)
{
	if($v['field']=='Status')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($v['field']=='Gate_Pass_Type')
	{
		//if($arr_type[$v['data']]==3)
			//$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		//else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_type[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}
if(!count($arr)>0)					 
	$where_string .= "AND substr(tb1.datdate,1,10) = '".date('Y-m-d')."'";
	
################## end code ####################################


$sql = "select * from(SELECT DISTINCT 
							if(tb1.intStatus=1,'Approved',if(tb1.intStatus=0,'Rejected','Pending')) as Status,
							if(tb1.gpType=1,'Orderwise','General') as `Gate_Pass_Type`,
							tb1.intGatePassNo as `Gate_Pass_No`,
							tb1.intGatePassYear as `Gate_Pass_Year`,
							
							
                           (select GROUP_CONCAT(DISTINCT                CONCAT(ware_gatepassdetails.strOrderNo,'/',ware_gatepassdetails.intOrderYear))
FROM ware_gatepassheader 
INNER JOIN ware_gatepassdetails 
on ware_gatepassheader.intGatePassNo = ware_gatepassdetails.intGatePassNo
AND ware_gatepassheader.intGatePassYear = ware_gatepassdetails.intGatePassYear WHERE ware_gatepassdetails.intGatePassNo = tb1.intGatePassNo 
AND ware_gatepassdetails.intGatePassYear = tb1.intGatePassYear )as orderNO, 


							(SELECT
							mst_locations.strName
							FROM
							ware_gatepassheader
							Inner Join mst_locations ON ware_gatepassheader.intGPToLocation = mst_locations.intId
							WHERE
							ware_gatepassheader.intGatePassNo =  tb1.intGatePassNo AND
							ware_gatepassheader.intGatePassYear =  tb1.intGatePassYear) as Gate_Pass_To, 
							tb1.datdate as `Date`,
							sys_users.strUserName as `User`,
							
							(SELECT dtApprovedDate FROM `ware_gatepassheader_approvedby` WHERE `intGatePassNo` = tb1.intGatePassNo AND `intYear` = tb1.intGatePassYear AND `intStatus` = '0' AND `intApproveLevelNo` = tb1.intApproveLevels limit 1) as app_Date,

							IFNULL((SELECT
								concat(sys_users.strUserName,'(',max(ware_gatepassheader_approvedby.dtApprovedDate),')' )
								FROM
								ware_gatepassheader_approvedby
								Inner Join sys_users ON ware_gatepassheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_gatepassheader_approvedby.intGatePassNo  = tb1.intGatePassNo AND
								ware_gatepassheader_approvedby.intYear =  tb1.intGatePassYear AND
								ware_gatepassheader_approvedby.intApproveLevelNo =  '1' AND
								ware_gatepassheader_approvedby.intStatus =  '0'
							),IF(((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.intStatus>1),'Approve', '')) as `1st_Approval`,  ";
							
						for($i=2; $i<=$approveLevel; $i++){
							
							if($i==2){
							$approval="2nd_Approval";
							}
							else if($i==3){
							$approval="3rd_Approval";
							}
							else {
							$approval=$i."th_Approval";
							}
							
							
						$sql .= "IFNULL(
						/*condition*/
(
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(ware_gatepassheader_approvedby.dtApprovedDate),')' )
								FROM
								ware_gatepassheader_approvedby
								Inner Join sys_users ON ware_gatepassheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_gatepassheader_approvedby.intGatePassNo  = tb1.intGatePassNo AND
								ware_gatepassheader_approvedby.intYear =  tb1.intGatePassYear AND
								ware_gatepassheader_approvedby.intApproveLevelNo =  '$i' AND
								ware_gatepassheader_approvedby.intStatus =  '0'
							),
							/*end condition*/
							
							/*false part*/

							IF(
							/*if condition */
							((SELECT
								menupermision.int".$i."Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND (tb1.intStatus>1) AND (tb1.intStatus<=tb1.intApproveLevels) AND ((SELECT
								max(ware_gatepassheader_approvedby.dtApprovedDate) 
								FROM
								ware_gatepassheader_approvedby
								Inner Join sys_users ON ware_gatepassheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_gatepassheader_approvedby.intGatePassNo  = tb1.intGatePassNo AND
								ware_gatepassheader_approvedby.intYear =  tb1.intGatePassYear AND
								ware_gatepassheader_approvedby.intApproveLevelNo =  ($i-1) AND
								ware_gatepassheader_approvedby.intStatus =  '0')<>'')),
								
								/*end if condition*/
								/*if true part*/
								'Approve',
								/*fase part*/
								 if($i>tb1.intApproveLevels,'-----',''))
								
								/*end IFNULL false part*/
								) as `".$approval."`, "; 
									
								}
								
							$sql .= "'View' as `View`,
                                  ( SELECT
 SUM(ware_gatepassdetails.dblQty)
FROM
ware_gatepassdetails
INNER JOIN ware_gatepassheader ON ware_gatepassdetails.intGatePassNo = ware_gatepassheader.intGatePassNo
	AND ware_gatepassdetails.intGatePassYear = ware_gatepassheader.intGatePassYear
WHERE 
ware_gatepassdetails.intGatePassNo = tb1.intGatePassNo AND
ware_gatepassdetails.intGatePassYear = tb1.intGatePassYear

	) AS `GatePassQtySUM`,
                                   (SELECT 
	SUM(ware_gatepasstransferindetails.dblQty)
FROM
	ware_gatepasstransferindetails
INNER JOIN ware_gatepasstransferinheader ON ware_gatepasstransferindetails.intGpTransfNo = ware_gatepasstransferinheader.intGpTransfNo
AND ware_gatepasstransferindetails.intGpTransfYear = ware_gatepasstransferinheader.intGpTransfYear
WHERE
ware_gatepasstransferinheader.intGatePassNo = tb1.intGatePassNo AND
ware_gatepasstransferinheader.intGatePassYear = tb1.intGatePassYear
) AS `TransferQty`,
                                   
( SELECT
	GROUP_CONCAT(
	DISTINCT CONCAT(
		ware_gatepasstransferinheader.intGpTransfNo,
		'/',
		ware_gatepasstransferinheader.intGpTransfYear
	)
)
FROM
ware_gatepasstransferinheader
WHERE 
ware_gatepasstransferinheader.intGatePassNo = tb1.intGatePassNo AND 
ware_gatepasstransferinheader.intGatePassYear = tb1.intGatePassYear
) AS `TranseferNumber`,

(
SELECT
app.dtApprovedDate
from 
ware_gatepasstransferinheader
INNER JOIN ware_gatepasstransferinheader_approvedby as app
on ware_gatepasstransferinheader.intGpTransfNo = app.intGpTransfNo 
and ware_gatepasstransferinheader.intGatePassYear = app.intYear
WHERE 
ware_gatepasstransferinheader.intGatePassNo = tb1.intGatePassNo AND 
ware_gatepasstransferinheader.intGatePassYear = tb1.intGatePassYear and 
app.intStatus = 0 AND
app.intApproveLevelNo = ware_gatepasstransferinheader.intApproveLevels limit 1
) as trns_app_Date 

						FROM
							ware_gatepassheader as tb1 
							Inner Join sys_users ON tb1.intUser = sys_users.intUserId 
							Inner Join mst_locations ON tb1.intCompanyId = mst_locations.intId 
							WHERE
							mst_locations.intCompanyId =  '$company' AND 
							tb1.intCompanyId =  '$location'
							$where_string
							)  as t where 1=1
						";
					//   echo $sql;
						 
$col = array();

$formLink			= "?q=$menuId&gatePassNo={Gate_Pass_No}&year={Gate_Pass_Year}";	 
$reportLink  		= "?q=$reportMenuId&gatePassNo={Gate_Pass_No}&year={Gate_Pass_Year}";
$reportLinkApprove  = "?q=$reportMenuId&gatePassNo={Gate_Pass_No}&year={Gate_Pass_Year}&grnType={Gate_Pass_Type}&approveMode=1";
$reportLinkPrint  	= "?q=$reportMenuId&gatePassNo={Gate_Pass_No}&year={Gate_Pass_Year}&mode=print";

//STATUS
$col["title"] 	= "Status"; // caption of column
$col["name"] 	= "Status"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
//edittype
$col["stype"] 	= "select";
$str = ":All;Pending:Pending;Approved:Approved;Rejected:Rejected" ;
$col["editoptions"] 	=  array("value"=> $str);
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$cols[] = $col;	$col=NULL;

//MRN Type
//STATUS
$col["title"] 	= "Gate Pass Type"; // caption of column
$col["name"] 	= "Gate_Pass_Type"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
//edittype
$col["stype"] 	= "select";
$str = ":All;Orderwise:Orderwise;General:General" ;
$col["editoptions"] 	=  array("value"=> $str);
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$cols[] = $col;	$col=NULL;

//PO No
$col["title"] 	= "Gate Pass No"; // caption of column
$col["name"] 	= "Gate_Pass_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "2";
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$col['link']	= $formLink;	 
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag


$cols[] = $col;	$col=NULL;



//PO Year
$col["title"] = "Gate Pass Year"; // caption of column
$col["name"] = "Gate_Pass_Year"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//PO Year
$col["title"] = "Gate Pass To"; // caption of column
$col["name"] = "Gate_Pass_To"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


$col["title"]  = "Order No"; // caption of column
$col["name"]   = "orderNO"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"]  = "3";
$col["align"]  = "center";
$cols[] = $col;	$col=NULL;

//Supplier
$col["title"] = "Date"; // caption of column
$col["name"] = "Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

$col["title"] = "Approved Date"; // caption of column
$col["name"] = "app_Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//User
$col["title"] = "User"; // caption of column
$col["name"] = "User"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;



//FIRST APPROVAL
$col["title"] = "1st Approval"; // caption of column
$col["name"] = "1st_Approval"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

for($i=2; $i<=$approveLevel; $i++){
	if($i==2){
	$ap="2nd Approval";
	$ap1="2nd_Approval";
	}
	else if($i==3){
	$ap="3rd Approval";
	$ap1="3rd_Approval";
	}
	else {
	$ap=$i."th Approval";
	$ap1=$i."th_Approval";
	}
//SECOND APPROVAL
$col["title"] = $ap; // caption of column
$col["name"] = $ap1; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;
}



//VIEW
$col["title"] = "Report"; // caption of column
$col["name"] = "View"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;


$col["title"] = "GP Qty"; // caption of column
$col["name"] = "GatePassQtySUM"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

$col["title"] = "Tranf Number"; // caption of column
$col["name"] = "TranseferNumber"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $TrnsfLink;	 
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag

$cols[] = $col;	$col=NULL;

$col["title"] = "Transf Qty"; // caption of column
$col["name"] = "TransferQty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

$col["title"] = "Transf Approved Date"; // caption of column
$col["name"]  = "trns_app_Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


$jq = new jqgrid('',$db);

$grid["caption"] 		= "Gate Pass Listing";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'Gate_Pass_No'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes


// export XLS file
// export to excel parameters - range could be "all" or "filtered"
//$grid["export"] = array("format"=>"xlsx", "filename"=>"my-file", "sheetname"=>"test");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>
<title>Gate Pass Listing</title>
<form id="frmlisting" name="frmlisting" method="post" action="">

        <div align="center" style="margin:10px">        
			<?php echo $out?>
        </div>
</form>
</body>

</html>
<?php

//------------------------------function load Default Department---------------------
function getMaxApproveLevel(){
	global $db;
	
	//echo $savedStat;
	$appLevel=0;
	$sqlp = "SELECT
			Max(ware_gatepassheader.intApproveLevels) AS appLevel
			FROM ware_gatepassheader";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['appLevel'];
}
//------------------------------function load User Department---------------------
function getUserDepartment($intUser){
	global $db;

	$sqlp = "SELECT
			sys_users.intDepartmentId
			FROM
			sys_users
			WHERE
			sys_users.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['intDepartmentId'];

}
?>


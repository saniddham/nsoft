<?php
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
	ini_set('max_execution_time', 1111111111) ;
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	$requestType = $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	///
	/////////// type of print load part /////////////////////
if($requestType=='loadSubCategory')
	{
		$mainCategory  = $_REQUEST['mainCategory'];
		$sql = "SELECT
				mst_subcategory.intId,
				mst_subcategory.strCode,
				mst_subcategory.strName
				FROM mst_subcategory
				WHERE
				mst_subcategory.intMainCategory =  '$mainCategory' and intStatus='1'  
				order by mst_subcategory.strName asc";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
			//------LOAD ORDER NOS--------------------------------------
	else if($requestType=='loadOrderNo')
	{
		$orderYear  = $_REQUEST['orderYear'];
		$sql = "SELECT DISTINCT
							trn_orderheader.intOrderNo,
							trn_orderheader.intOrderYear
							FROM trn_orderheader 
							WHERE
							trn_orderheader.intOrderYear = '$orderYear' AND
							trn_orderheader.intStatus >  '0' and trn_orderheader.intStatus < (trn_orderheader.intApproveLevelStart+1)   
							AND trn_orderheader.PO_TYPE<>1 
							order by intOrderYear desc,intOrderNo desc";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intOrderNo']."/".$row['intOrderYear']."\">".$row['intOrderNo']."/".$row['intOrderYear']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	else if($requestType=='loadStyleNo')
	{
		$orderNo  = $_REQUEST['orderNo'];
		$orderNoArray 	 = explode('/',$orderNo);
		 $sql = "SELECT DISTINCT 
		 		trn_orderdetails.intSalesOrderId, 
				trn_orderdetails.strSalesOrderNo,
				trn_orderdetails.strCombo,
				mst_part.strName
				FROM
				trn_orderdetails
				Inner Join mst_part ON trn_orderdetails.intPart = mst_part.intId 
				WHERE 
				trn_orderdetails.intOrderNo =  '$orderNoArray[0]' AND
				trn_orderdetails.intOrderYear =  '$orderNoArray[1]'";
		$result = $db->RunQuery($sql);
 	
			$html .= "<tr id=\"\"><th><input name=\"chkSoAll\" id=\"chkSoAll\" class=\"chkSoAll\" type=\"checkbox\"></th><th><strong>Sales Orders</strong></th></tr>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<tr id=\"".$row['intSalesOrderId']."\"><td align=\"center\"><input name=\"chkSo\" id=\"chkSo\" class=\"chkSo\" type=\"checkbox\"></td><td id=\"".($orderNoArray[0].'/'.$orderNoArray[1].'/'.$row['intSalesOrderId'])."\" class=\"normalfnt so\" nowrap>".$row['strSalesOrderNo']."/".$row['strName']."/".$row['strCombo']."</td></tr>";
		}
		echo $html;
		//echo json_encode($response);
	}
	else if($requestType=='loadMrnNo')
	{
		$orderNo  = $_REQUEST['orderNo'];
		$orderNoArray 	 = explode('/',$orderNo);
		$styleNo  = $_REQUEST['styleNo'];
		
		  $sql = "SELECT DISTINCT
				ware_mrndetails.intMrnNo, 
				ware_mrndetails.intMrnYear  
				FROM ware_mrndetails 
				Inner Join ware_mrnheader ON ware_mrndetails.intMrnNo = ware_mrnheader.intMrnNo AND ware_mrndetails.intMrnYear = ware_mrnheader.intMrnYear
				WHERE 
				ware_mrnheader.intStatus=1 AND 
				ware_mrnheader.intCompanyId =  '$location' AND 
				ware_mrndetails.intOrderNo =  '$orderNoArray[0]' AND
				ware_mrndetails.intOrderYear =  '$orderNoArray[1]' AND 
				ware_mrndetails.strStyleNo =  '$styleNo' AND 
				(ware_mrndetails.dblQty-ware_mrndetails.dblIssudQty)>0";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intMrnNo']."/".$row['intMrnYear']."\">".$row['intMrnNo']."/".$row['intMrnYear']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	//--------------------------------------------------------
	else if($requestType=='loadGeneralItems')
	{
		$mainCategory  = $_REQUEST['mainCategory'];
		$subCategory  = $_REQUEST['subCategory'];
		$description  = $_REQUEST['description'];
		
		   $sql="SELECT
				mst_item.strName as itemName,
				mst_item.intId, 
				mst_item.intMainCategory,
				mst_item.intSubCategory,
				mst_maincategory.strName as mainCatName,
				mst_subcategory.strName as subCatName,
				mst_units.strCode as uom , 
				mst_item.strCode,
                                mst_item.ITEM_HIDE,
                                mst_item.strCode as SUP_ITEM_CODE
				FROM  
				mst_item  
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId
                                LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
				WHERE 
				mst_item.intMainCategory =  '$mainCategory' AND 
				 
				mst_item.intStatus = '1' ";
				if($subCategory!='')
				$sql.=" AND mst_item.intSubCategory =  '$subCategory'";
				if($description!=''){
				$sql.=" AND
				(mst_item.strName LIKE  '%$description%' OR mst_item.strCode LIKE  '%$description%')";
				}
				$sql.=" Order by mst_maincategory.strName asc, mst_subcategory.strName asc, mst_item.strName asc";
		//echo $sql;
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			 $stockBalQty=getStockBalance_bulk($location,$row['intId']);
			$confirmedGPqty=get_gpQtyGen($location,$row['intId']);//but not raised final confirmation

			$data['itemId'] 	= $row['intId'];
			$data['maincatId'] = $row['intMainCategory'];
			$data['subCatId'] 	= $row['intSubCategory'];
			$data['mainCatName'] = $row['mainCatName'];
			$data['subCatName'] = $row['subCatName'];
			$data['code'] = $row['strCode'];
			$data['uom'] = $row['uom'];
			$data['itemName'] = $row['itemName'];
                        $data['ITEM_HIDE'] = $row['ITEM_HIDE'];
			$data['supItemCode'] = $row['SUP_ITEM_CODE'];
			$data['stockBal'] = round($stockBalQty,4);
			$data['confirmedGPqty'] = round($confirmedGPqty,4);
			$data['mrnNo']= '';
			$data['actualPriQty']= '';
			
			$arrCombo[] = $data;
		}
		
		if($arrCombo==null){
			$arrCombo[0]="none";
		}
		
		$response['arrCombo'] 	= $arrCombo;
		echo json_encode($response);
	}
	//--------------------------------------------------------
	else if($requestType=='loadBomItems')
	{  
		$itemType  = $_REQUEST['itemType'];
		$orderNo  = $_REQUEST['orderNo'];
		$orderNoArray 	 = explode('/',$orderNo);
		//$salesOrderNo  = $_REQUEST['salesOrderNo'];
		$mainCategory  = $_REQUEST['mainCategory'];
		$subCategory  = $_REQUEST['subCategory'];
		$description  = $_REQUEST['description'];
		
		$salesOrderNo  = $_REQUEST['salesOrderNo'];
		$salesOrderNo_list='';
		$salesOrderNo 		 = json_decode($_REQUEST['salesOrderNo'], true);
		foreach($salesOrderNo as $arrVal)
		{
			if($salesOrderNo_list!=''){
			$salesOrderNo_list      .= ",'".$arrVal['so']."'";
			$salesOrderNo_list1      .= ",".$arrVal['so'];
			}
			else{
			$salesOrderNo_list      .= "'".$arrVal['so']."'";
			$salesOrderNo_list1      .= $arrVal['so'];
			}
		}
		//---------------------
		if($orderNoArray[0]!=''){  //if(($orderNoArray[0]!='') && ($salesOrderNo!=''))
			
 				
				$sql = "SELECT 
						GROUP_CONCAT(distinct TB1.mrnNo) AS mrnNo,
								sum(TB1.priQty) as priQty,
  								sum(TB1.mrnQty) as mrnQty,
								TB1.itemName,
                                                                TB1.ITEM_HIDE,
								TB1.intOrderNo,
								TB1.intOrderYear,
								TB1.intId,
								TB1.intMainCategory,
								TB1.intSubCategory,
								TB1.mainCatName,
								TB1.subCatName,
								TB1.strCode,
								TB1.strName,
                                                                TB1.SUP_ITEM_CODE,
								TB1.strSalesOrderNo,
								TB1.intSalesOrderId,
								TB1.uom,
								TB1.TYPE 
						 FROM (
						(
							SELECT
								'' AS mrnNo,
								sum(MAIN.REQUIRED) AS priQty,
								0 as mrnQty,
								mst_item.strName AS itemName,
                                                                mst_item.ITEM_HIDE,
								trn_orderdetails.intOrderNo,
								trn_orderdetails.intOrderYear,
								-- trn_orderdetails.intSalesOrderId,
								mst_item.intId,
								mst_item.intMainCategory,
								mst_item.intSubCategory,
								mst_maincategory.strName AS mainCatName,
								mst_subcategory.strName AS subCatName,
								mst_item.strCode,
                                mst_item.strCode as SUP_ITEM_CODE,
								mst_part.strName,
								trn_orderdetails.strSalesOrderNo,
								trn_orderdetails.intSalesOrderId,
								mst_units.strCode AS uom,
								3 AS TYPE 
							FROM
								(
									SELECT
										*
									FROM
										trn_po_prn_details_sales_order
									WHERE
										trn_po_prn_details_sales_order.ORDER_NO = '$orderNoArray[0]'
									AND trn_po_prn_details_sales_order.ORDER_YEAR = '$orderNoArray[1]'
						 ";
						
						
						
						
						if($salesOrderNo_list!='')
						$sql.=" AND concat(trn_po_prn_details_sales_order.ORDER_NO,'/',trn_po_prn_details_sales_order.ORDER_YEAR,'/',trn_po_prn_details_sales_order.SALES_ORDER) IN ($salesOrderNo_list)) AS MAIN "; 
						
						$sql.=" INNER JOIN trn_orderdetails ON trn_orderdetails.intOrderNo = MAIN.ORDER_NO
						AND trn_orderdetails.intOrderYear = MAIN.ORDER_YEAR
						AND trn_orderdetails.intSalesOrderId = MAIN.SALES_ORDER
						INNER JOIN mst_item ON mst_item.intId = MAIN.ITEM
						INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
						INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
						INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
						INNER JOIN mst_part ON trn_orderdetails.intPart = mst_part.intId
                                                LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
						WHERE
							trn_orderdetails.intOrderNo = '$orderNoArray[0]'
						AND trn_orderdetails.intOrderYear = '$orderNoArray[1]'
						AND mst_item.intStatus = '1'";
						
						if($salesOrderNo_list!='')
						$sql.=" AND concat(trn_orderdetails.intOrderNo,'/',trn_orderdetails.intOrderYear,'/',trn_orderdetails.intSalesOrderId) IN  ($salesOrderNo_list)";
						if($mainCategory!='')
						$sql.=" AND mst_item.intMainCategory = '$mainCategory'";
						if($subCategory!='')
						$sql.=" AND mst_item.intSubCategory =  '$subCategory'";
						if($description!='')
						$sql.=" AND (mst_item.strName LIKE  '%$description%' OR mst_item.strCode LIKE  '%$description%')";
											
						$sql.=" 
							GROUP BY
							trn_orderdetails.intOrderNo,
							trn_orderdetails.intOrderYear,
							trn_orderdetails.intSalesOrderId,
							mst_item.intId
						ORDER BY
							mst_maincategory.strName ASC,
							mst_subcategory.strName ASC,
							mst_item.strName ASC
					)
					UNION ALL
						(
							SELECT DISTINCT
								GROUP_CONCAT(
									MAIN.intMrnNo,
									'/',
									MAIN.intMrnYear
								) AS mrnNo,
								0 AS priQty,
								sum(MAIN.dblQty) as mrnQty,
								mst_item.strName AS itemName,
								trn_orderdetails.intOrderNo,
								trn_orderdetails.intOrderYear,
								mst_item.intId,
								mst_item.intMainCategory,
								mst_item.intSubCategory,
								mst_maincategory.strName AS mainCatName,
								mst_subcategory.strName AS subCatName,
								mst_item.strCode,
                                                                mst_item.ITEM_HIDE,
                                                                mst_item.strCode as SUP_ITEM_CODE,
								mst_part.strName,
								trn_orderdetails.strSalesOrderNo,
								trn_orderdetails.intSalesOrderId,
								mst_units.strCode AS uom,
								MAIN.MRN_TYPE as TYPE 
							FROM
								(
									SELECT
										*
									FROM
										ware_mrndetails
									WHERE
										ware_mrndetails.intOrderNo = '$orderNoArray[0]'
									AND ware_mrndetails.intOrderYear = '$orderNoArray[1]'
									";
						
						if($salesOrderNo_list!='')
						$sql.=" AND concat(ware_mrndetails.intOrderNo,'/',ware_mrndetails.intOrderYear,'/',ware_mrndetails.strStyleNo) IN  ($salesOrderNo_list)) AS MAIN "; 
						
						$sql.=" INNER JOIN trn_orderdetails ON trn_orderdetails.intOrderNo = MAIN.intOrderNo
		AND trn_orderdetails.intOrderYear = MAIN.intOrderYear
		AND trn_orderdetails.intSalesOrderId = MAIN.strStyleNo
		INNER JOIN ware_mrnheader ON MAIN.intMrnNo = ware_mrnheader.intMrnNo
		AND MAIN.intMrnYear = ware_mrnheader.intMrnYear
		INNER JOIN mst_item ON mst_item.intId = MAIN.intItemId
		INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
		INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
		INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
		INNER JOIN mst_part ON trn_orderdetails.intPart = mst_part.intId
                LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
		WHERE
			ware_mrnheader.intStatus = '1'
		AND trn_orderdetails.intOrderNo = '$orderNoArray[0]'
		AND trn_orderdetails.intOrderYear = '$orderNoArray[1]'
		AND mst_item.intStatus = '1' 
		AND MAIN.MRN_TYPE <>3";
						
						if($salesOrderNo_list!='')
						$sql.=" AND concat(trn_orderdetails.intOrderNo,'/',trn_orderdetails.intOrderYear,'/',trn_orderdetails.intSalesOrderId) IN ($salesOrderNo_list)";
						if($mainCategory!='')
						$sql.=" AND mst_item.intMainCategory = '$mainCategory'";
						if($subCategory!='')
						$sql.=" AND mst_item.intSubCategory =  '$subCategory'";
						if($description!='')
						$sql.=" AND mst_item.strName LIKE  '%$description%'";						
						$sql.="
						GROUP BY 
						trn_orderdetails.intOrderNo,
						trn_orderdetails.intOrderYear,
						trn_orderdetails.intSalesOrderId ,
									MAIN.intItemId
								ORDER BY
									MAIN.intMrnYear DESC,
									MAIN.intMrnNo DESC,
									mst_maincategory.strName DESC,
									mst_subcategory.strName ASC,
									mst_item.strName ASC
						)
						) AS TB1
						GROUP BY 
								/*TB1.intOrderNo,
								TB1.intOrderYear,
								TB1.intSalesOrderId, */
								TB1.intId"; 
                                                
						//HAVING mrnNo NOT LIKE '%PRI,%'";		
 
		}
                 //echo $sql;
		//-----------------
		//-----------------
		//echo $sql;
		$confirmedGPqty	=0;
		$gpQty			=0;
		$actualPriQty	=0;
		$actualMrnQty	=0;
		$actualPriQty_ini	=0;
		$actualMrnQty_ini	=0;
		$result_1 = $db->RunQuery($sql);
		while($row_1=mysqli_fetch_array($result_1))
		{
			$salesOrderQty_list='';
			$salesOrderNo1 		 = json_decode($_REQUEST['salesOrderNo'], true);
			$k=0;
			//to get pri qtys
			$salesOrderQty_list='';
			$salesOrderNo1 		 = json_decode($_REQUEST['salesOrderNo'], true);
			$k=0;
			foreach($salesOrderNo1 as $arrVal1)
			{
				if($salesOrderQty_list!=''){
				$salesOrderQty_list      .= ",".getRequiredQty($orderNoArray[0],$orderNoArray[1],$arrVal1['so'],$row_1['intId']);
				}
				else{
				$salesOrderQty_list      .= getRequiredQty($orderNoArray[0],$orderNoArray[1],$arrVal1['so'],$row_1['intId']);
				}
				$k++;
			}
			//
			//to get pri qtys
			$salesOrderQty_ini_list='';
			$salesOrderNo1 		 = json_decode($_REQUEST['salesOrderNo'], true);
			$k=0;
			foreach($salesOrderNo1 as $arrVal1)
			{
				if($salesOrderQty_ini_list!=''){
				$salesOrderQty_ini_list      .= ",".get_pri_qty($orderNoArray[0],$orderNoArray[1],$arrVal1['so'],$row_1['intId']);
				}
				else{
				$salesOrderQty_ini_list      .= get_pri_qty($orderNoArray[0],$orderNoArray[1],$arrVal1['so'],$row_1['intId']);
				}
				$k++;
			}
			//
			$salesOrderBalQty_list	='';
			$salesOrderBalQty_based_mrn_list='';
			$actualPriQty			=0;
			$confirmedGPqty			=0;
			$actualMrnQty			=0;
			$actualPriQty_ini			=0;
			$actualMrnQty_ini			=0;
			foreach($salesOrderNo1 as $arrVal1)
			{
			
				$stockBalQty=getStockBalance_bulk($location,$row_1['intId']);
				$confirmedGPqty +=get_gpQty($location,$orderNoArray[0],$orderNoArray[1],$arrVal1['so'],$row_1['intId']);//but not raised final confirmation
				
				$priQty	= 	round(get_pri_qty($orderNoArray[0],$orderNoArray[1],$arrVal1['so'],$row_1['intId']),4);
				$mrnQty =	round(get_mrn_qty($orderNoArray[0],$orderNoArray[1],$arrVal1['so'],$row_1['intId']),4);
				$mrnNos =	get_mrn_nos($orderNoArray[0],$orderNoArray[1],$row_1['intId']);
				if(!$mrnNos)
					$mrnNos='';
				$gpQty	=	round(getGPQty($orderNoArray[0],$orderNoArray[1],$arrVal1['so'],$row_1['intId']),4);
				/////
				$balToIssueQty=$priQty-$gpQty;
				if($balToIssueQty<0)
					$balToIssueQty=0;
				if($salesOrderBalQty_list!=''){
					$salesOrderBalQty_list      .= ",".$balToIssueQty;
				}
				else{
					$salesOrderBalQty_list      .= $balToIssueQty;
				}
				$bal_based_mrn=$mrnQty-$gpQty;
				if($bal_based_mrn=='' || $bal_based_mrn<0)
					$bal_based_mrn=0;
				if($salesOrderBalQty_based_mrn_list!=''){
					$salesOrderBalQty_based_mrn_list.= ",".$bal_based_mrn;
				}
				else{
					$salesOrderBalQty_based_mrn_list.= $bal_based_mrn;
				}
				////
				$actualPriQty	+=$priQty-$gpQty;
				$actualPriQty_ini	+=$priQty;
				if($actualPriQty<0){
					$actualPriQty=0;
				}
				
				
 				$actualMrnQty	+=$mrnQty-$gpQty;
 				$actualMrnQty_ini	+=$mrnQty;
				if($actualMrnQty<0){
					$actualMrnQty=0;
				}
			}
			////////////////////////////////////////////////////
			
			$data_1['salesOrderNo_list'] 				= $salesOrderNo_list;
			
			$data_1['salesOrderPRIQty_ini_list'] 		= $salesOrderQty_ini_list;//only pri qty
			$data_1['salesOrderPRIQty_list'] 			= $salesOrderQty_list;//if in pri, required qty.else if additional item, mrn qty
			$data_1['salesOrderBalQty_list'] 			= $salesOrderBalQty_list;//pri qty - gp qty
			$data_1['salesOrderBalQty_based_mrn_list'] 	= $salesOrderBalQty_based_mrn_list;//mrn qty - gp qty
			$data_1['actualPriQty']						=round($actualPriQty,4);//tot salesOrderPRIQty_list
			$data_1['actualMrnQty']						=round($actualMrnQty,4);//tot salesOrderBalQty_based_mrn_list				
			$data_1['actualPriQty_ini']					=round($actualPriQty_ini,4);//if =0, this is an additional item
			$data_1['itemId'] 							= $row_1['intId'];
			$data_1['maincatId'] 						= $row_1['intMainCategory'];
			$data_1['subCatId'] 						= $row_1['intSubCategory'];
			$data_1['mainCatName'] 						= $row_1['mainCatName'];
			$data_1['subCatName'] 						= $row_1['subCatName'];
			$data_1['code'] 							= $row_1['strCode'];
			$data_1['uom'] 								= $row_1['uom'];
			$data_1['itemName'] 						= $row_1['itemName'];
                        $data_1['supItemCode']                                          = $row_1['SUP_ITEM_CODE'];
                        $data_1['ITEM_HIDE'] 						= $row_1['ITEM_HIDE'];
			$data_1['type'] = $row_1['TYPE'];
			/*if($row['intSalesOrderId']==''){
				$data_1['stockBal'] = 0;
			}
			else{
				$data_1['stockBal'] = round($stockBalQty,4);
			}*/
			
			$data_1['stockBal'] = round($stockBalQty,4);
			$data_1['confirmedGPqty'] = round($confirmedGPqty,4);
			
			$data_1['strSalesOrderNo'] = $row_1['strSalesOrderNo'];
			$data_1['strSalesOrderName'] = $row_1['strName'];
			$data_1['intSalesOrderId'] = $row_1['intSalesOrderId'];
			
			//$data_1['intMrnNo'] = $row['intMrnNo'];
			//$data_1['intMrnYear'] = $row['intMrnYear'];
			$f=substr($row_1['mrnNo'], 0,1);
			if($f==',')
			$mrn =substr($row_1['mrnNo'], 1);

			$data_1['mrnNo'] = $mrnNos;
			
			$arrCombo[] = $data_1;
		}
		
		if($arrCombo==null){
			$arrCombo[0]="none";
		}
		
		$response['arrCombo'] 	= $arrCombo;
		echo json_encode($response);

	}
	//-----------------------------------------------------------






	//-----------------------------------------------------------
	function getStockBalance($company,$location,$orderNo,$orderYear,$salesOrderNo,$item)
	{
		global $db;
		  $sql = "SELECT
				Sum(ware_stocktransactions.dblQty) as stockBal 
				FROM ware_stocktransactions
				WHERE
				ware_stocktransactions.intCompanyId =  '$company' AND
				ware_stocktransactions.intLocationId =  '$location' AND
				ware_stocktransactions.intOrderNo =  '$orderNo' AND
				ware_stocktransactions.intOrderYear =  '$orderYear' AND
				ware_stocktransactions.intSalesOrderId =  '$salesOrderNo' AND
				ware_stocktransactions.intItemId =  '$item'";

		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
	function getStockBalance_bulk($location,$item)
	{
		global $db;
		   $sql = "SELECT
				Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$location'
				GROUP BY
				ware_stocktransactions_bulk.intItemId";
				
				//echo $sql;

		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
//------------------------------------------------------------
function get_gpQtyGen($location,$itemId){
	global $db;
	 $sql1 = "SELECT
				Sum(ware_gatepassdetails.dblQty) as gpQty
				FROM
				ware_gatepassheader
				Inner Join ware_gatepassdetails ON ware_gatepassheader.intGatePassNo = ware_gatepassdetails.intGatePassNo AND ware_gatepassheader.intGatePassYear = ware_gatepassdetails.intGatePassYear
				WHERE
				ware_gatepassdetails.intItemId =  '$itemId' AND
				ware_gatepassheader.intStatus >  '1' AND
				ware_gatepassheader.intStatus <= ware_gatepassheader.intApproveLevels AND
				ware_gatepassheader.intCompanyId =  '$location'";	
	$result1 = $db->RunQuery($sql1);
	$row1=mysqli_fetch_array($result1);
	$gpQty=$row1['gpQty'];
	return $gpQty;
}
//------------------------------------------------------------
function get_gpQty($location,$orderNo,$orderYear,$styleNo,$itemId){
	global $db;
	 $sql1 = "SELECT
				Sum(ware_gatepassdetails.dblQty) as gpQty
				FROM
				ware_gatepassheader
				Inner Join ware_gatepassdetails ON ware_gatepassheader.intGatePassNo = ware_gatepassdetails.intGatePassNo AND ware_gatepassheader.intGatePassYear = ware_gatepassdetails.intGatePassYear
				WHERE
				ware_gatepassdetails.strOrderNo =  '$orderNo' AND
				ware_gatepassdetails.intOrderYear =  '$orderYear' AND
				ware_gatepassdetails.strStyleNo =  '$styleNo' AND
				ware_gatepassdetails.intItemId =  '$itemId' AND
				ware_gatepassheader.intStatus >  '1' AND
				ware_gatepassheader.intStatus <= ware_gatepassheader.intApproveLevels AND
				ware_gatepassheader.intCompanyId =  '$location'";	
	$result1 = $db->RunQuery($sql1);
	$row1=mysqli_fetch_array($result1);
	$gpQty=$row1['gpQty'];
	return $gpQty;
}
function getMRNQty($orderNo,$orderYear,$salesOrderNo,$item){
	global $db;
	 $sql1 = "SELECT
			sum(ware_mrndetails.dblQty) as mrnQty
		FROM
			ware_mrndetails
		WHERE
			ware_mrndetails.intOrderNo = '$orderNo'
		AND ware_mrndetails.intOrderYear = '$orderYear'
		AND ware_mrndetails.strStyleNo = '$salesOrderNo'
		AND ware_mrndetails.intItemId = '$item'";	
	$result1 = $db->RunQuery($sql1);
	$row1=mysqli_fetch_array($result1);
	$mrnQty=$row1['mrnQty'];
	return $mrnQty;
}
//------------------------------------------------------------

	function getPRIQty($orderNo,$orderYear,$salesOrderNo,$item)
	{
		global $db;
		   $sql = "	SELECT
						IFNULL(trn_po_prn_details_sales_order.REQUIRED,'0') AS priQty
					FROM
						trn_po_prn_details_sales_order
					WHERE
						trn_po_prn_details_sales_order.ORDER_NO = '$orderNo'
					AND trn_po_prn_details_sales_order.ORDER_YEAR = '$orderYear'
					AND trn_po_prn_details_sales_order.SALES_ORDER = '$salesOrderNo'
					AND trn_po_prn_details_sales_order.ITEM = '$item'";
		//echo $sql;
				
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return val($row['priQty']);	
	}
//--------------------------------------------------------------
function getGPQty($orderNo,$orderYear,$salesOrderNo,$item)
	{
		global $db;
		   $sql = "SELECT
				IFNULL(sum(ware_gatepassdetails.dblQty),'0') AS gpQty
			FROM
				ware_gatepassdetails
			INNER JOIN ware_gatepassheader ON ware_gatepassheader.intGatePassNo = ware_gatepassdetails.intGatePassNo
			AND ware_gatepassheader.intGatePassYear = ware_gatepassdetails.intGatePassYear
			WHERE
				ware_gatepassdetails.strOrderNo = '$orderNo'
			AND ware_gatepassdetails.intOrderYear = '$orderYear'
			/*AND ware_gatepassdetails.strStyleNo = '$salesOrderNo'*/
			AND concat(ware_gatepassdetails.strOrderNo,'/',ware_gatepassdetails.intOrderYear,'/',ware_gatepassdetails.strStyleNo) = '$salesOrderNo'
			AND ware_gatepassdetails.intItemId = '$item'
			AND ware_gatepassheader.intStatus = '1'";
		//echo $sql;
				
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return val($row['gpQty']);	
	}
//--------------------------------------------------------------
function getRequiredQty($orderNo,$year,$so,$item){
	global $db;
      $sql = "SELECT
			trn_po_prn_details_sales_order.REQUIRED
			FROM `trn_po_prn_details_sales_order`
			WHERE 
			trn_po_prn_details_sales_order.ORDER_NO = '$orderNo' AND
			trn_po_prn_details_sales_order.ORDER_YEAR = '$year' AND
			concat(trn_po_prn_details_sales_order.ORDER_NO,'/',trn_po_prn_details_sales_order.ORDER_YEAR,'/',trn_po_prn_details_sales_order.SALES_ORDER) = '$so' AND
			trn_po_prn_details_sales_order.ITEM = '$item'
			GROUP BY
			trn_po_prn_details_sales_order.ORDER_NO,
			trn_po_prn_details_sales_order.ORDER_YEAR,
			trn_po_prn_details_sales_order.SALES_ORDER,
			trn_po_prn_details_sales_order.ITEM

			";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
	//$actualPriQty_s	= getRoundedWeight($row['REQUIRED'],$item);
	$actualPriQty_s	= $row['REQUIRED'];
	if($actualPriQty_s=='')
		$actualPriQty_s=0;
	
	if($actualPriQty_s=='' || $actualPriQty_s==0){
		
      $sql = "SELECT
			SUM(trn_po_prn_details_sales_order_stock_transactions.QTY) AS QTY  
			FROM `trn_po_prn_details_sales_order_stock_transactions`
			WHERE 
			trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = '$orderNo' AND
			trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = '$year' AND
			concat(trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,'/',trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,'/',trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER) = '$so' AND
			trn_po_prn_details_sales_order_stock_transactions.ITEM = '$item' AND
			trn_po_prn_details_sales_order_stock_transactions.TRANSACTION = 'MRN'
			GROUP BY
			trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,
			trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,
			trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER,
			trn_po_prn_details_sales_order_stock_transactions.ITEM

			";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
	//$actualPriQty_s	= getRoundedWeight($row['REQUIRED'],$item);
	$actualPriQty_s	= $row['QTY'];
	if($actualPriQty_s=='')
		$actualPriQty_s=0;
 	}
	
	//if($row['REQUIRED']=='')
   // return 0;
	//else
	return $actualPriQty_s;
}

function get_pri_qty($orderNo,$orderYear,$salesOrder,$item){
	global $db;
    $sql	= "SELECT
					*
				FROM
					trn_po_prn_details_sales_order
				WHERE
					trn_po_prn_details_sales_order.ORDER_NO = '$orderNo'
				AND trn_po_prn_details_sales_order.ORDER_YEAR = '$orderYear'
				AND concat(trn_po_prn_details_sales_order.ORDER_NO,'/',trn_po_prn_details_sales_order.ORDER_YEAR,'/',trn_po_prn_details_sales_order.SALES_ORDER) = '$salesOrder'
				AND trn_po_prn_details_sales_order.ITEM = '$item'";
	$result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
	return $row['REQUIRED'];
	
}
function get_mrn_qty($orderNo,$orderYear,$salesOrder,$item){

	global $db;
    $sql	= "SELECT
					sum(QTY) as mrnQty 
				FROM
					trn_po_prn_details_sales_order_stock_transactions
				WHERE
					trn_po_prn_details_sales_order_stock_transactions.ORDER_NO = '$orderNo'
				AND trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR = '$orderYear'
				AND concat(trn_po_prn_details_sales_order_stock_transactions.ORDER_NO,'/',trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR,'/',trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER) = '$salesOrder'
				AND trn_po_prn_details_sales_order_stock_transactions.ITEM = '$item'
				AND trn_po_prn_details_sales_order_stock_transactions.TRANSACTION = 'MRN'";
	$result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
	return $row['mrnQty'];
	
}
function get_mrn_nos($orderNo,$orderYear,$item){

	global $db;
    $sql	= "SELECT
					group_concat(distinct intMrnNo,'/',intMrnYear) as mrnNos 
				FROM
					ware_mrndetails
				WHERE
					ware_mrndetails.intOrderNo = '$orderNo'
				AND ware_mrndetails.intOrderYear = '$orderYear'
				AND ware_mrndetails.intItemId = '$item' 
				AND intMrnNo > 0";
	$result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
	return $row['mrnNos'];
	
}

?>

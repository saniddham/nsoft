//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
var basePath	="presentation/warehouse/gatepass/addNew/";
var reportId	="901";
var so_selected_flag=0;
	
$(document).ready(function() {
	
  		$("#frmGatePass").validationEngine();
		$('#frmGatePass #cboDepartment').focus();
		
		$("#frmGatePass #butAddItems").click(function(){
			if(!$('#frmGatePass #cboGatePassTo').val()){
				alert("Please select the Gate Pass to location");
				return false;				
			}
			//clearRows();
			closePopUp();
			loadPopup();
		});
		
 	//--------------------------------------------
		
   
 //-------------------------------------------- 
  $('#frmGatePass #cboCurrency').change(function(){
	    var currency = $('#cboCurrency').val();
		var url 		= basePath+"gatepass-db-get.php?requestType=loadExchangeRate";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"currency="+currency,
			async:false,
			success:function(json){

					document.getElementById("divExchRate").innerHTML=json.excRate;
			}
		});
		
  });
  //-------------------------------------------------------
   $('#frmGatePass #cboSupplier').change(function(){
	    var supplier = $('#cboSupplier').val();
		var url 		= basePath+"gatepass-db-get.php?requestType=loadSupplierDetails";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"supplier="+supplier,
			async:false,
			success:function(json){

					$('#frmGatePass #cboCurrency').val(json.currency);
					$('#frmGatePass #cboPayTerm').val(json.payTerm);
					$('#frmGatePass #cboPayMode').val(json.payMode);
					$('#frmGatePass #cboShipmentMode').val(json.shipmentMode);
					document.getElementById("divExchRate").innerHTML=json.excRate;
			}
		});
		
  });
  //-------------------------------------------------------
 
  $('#frmGatePass #butSave').click(function(){
	var requestType = '';
	if ($('#frmGatePass').validationEngine('validate'))   
    { 
		showWaiting();
		var data = "requestType=save";
		
			data+="&serialNo="		+	$('#txtGatePassNo').val();
			data+="&Year="	+	$('#txtGPYear').val();
			data+="&gatePassTo="	+	$('#cboGatePassTo').val();
			data+="&note="	+	URLEncode_json($('#txtNote').val());
			data+="&date="			+	$('#dtDate').val();
			data+="&type="	+	$('#txtItemType').val();


			var rowCount = document.getElementById('tblGatePass').rows.length;
			if(rowCount==1){
				alert("items not selected to Gate Pass");hideWaiting();
				return false;				
			}
			var row = 0;
			var errorQtyFlag=0;
			
			var arr="[";
			
			$('#tblGatePass .item').each(function(){
	
				var orderNo	= $(this).parent().find(".orderNo").html();
				var salesOrderId	= $(this).parent().find(".salesOrderNo").attr('id');
				var itemId	= $(this).attr('id');
				var Qty	= $(this).parent().find(".Qty").val();
					
					if(Qty>0){
						arr += "{";
						arr += '"orderNo":"'+	orderNo +'",' ;
						arr += '"salesOrderId":"'+		salesOrderId +'",' ;
						arr += '"itemId":"'+	itemId +'",' ;
						arr += '"Qty":"'+		Qty +'"' ;
						arr +=  '},';
					}
						if(Qty<=0){
							errorQtyFlag=1;
						}
			});
			if(rowCount<=1){
				alert("No items selected to Gate Pass");hideWaiting();
				return false;
			}
			else if(errorQtyFlag==1){
				alert("Please Enter Quantities");hideWaiting();
				return false;
			}
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;

		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = basePath+"gatepass-db-set.php";
     	var obj = $.ajax({
			url:url,
			type:'post',
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmGatePass #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						//$('#frmSampleInfomations').get(0).reset();
						//saveImage(json.sampleNo,json.sampleYear,json.revisionNo);
						//loadCombo_frmSampleInfomations();
						var t=setTimeout("alertx()",1000);
						$('#txtGatePassNo').val(json.serialNo);
						$('#txtGPYear').val(json.year);
						if(json.confirmationMode==1){
							$('#butConfirm').show();
						}
						else{
							$('#butConfirm').hide();
						}
						return;
					}
				},
			error:function(xhr,status){
					
					$('#frmGatePass #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
			;hideWaiting();
	}
   });
   
	
	//--------------refresh the form----------
	$('#frmGatePass #butNew').click(function(){
		window.location.href = "?q=250";
		$('#frmGatePass').get(0).reset();
		clearRows();
		$('#frmGatePass #txtGrnNo').val('');
		$('#frmGatePass #txtGrnYear').val('');
		$('#frmGatePass #txtInvioceNo').val('');
		$('#frmGatePass #cboPO').val('');
		$('#frmGatePass #txtDeliveryNo').val('');
		$('#frmGatePass #txtTotGrnAmmount').val('');
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmGatePass #dtDate').val(d);
	});
	//----------------------------------------
	
/*    $('#frmGatePass #butDelete').click(function(){
		if($('#frmGatePass #cboSearch').val()=='')
		{
			$('#frmGatePass #butDelete').validationEngine('showPrompt', 'Please select Type.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmGatePass #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = "typeOfPrint-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											data:'requestType=delete&cboSearch='+$('#frmGatePass #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmGatePass #butDelete').validationEngine('showPrompt', json.msg,json.type );
												
												if(json.type=='pass')
												{
													$('#frmGatePass').get(0).reset();
													loadCombo_frmGatePass();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
	
*/	
//----------------------------	
$('#frmGatePass  .delImg').click(function(){
	$(this).parent().parent().remove();
});

//----------------------------	
//-----------------------------------
$('#frmGatePass  #butReport').click(function(){
	if($('#txtGatePassNo').val()!=''){
		window.open('?q='+reportId+'&gatePassNo='+$('#txtGatePassNo').val()+'&year='+$('#txtGPYear').val());	
	}
	else{
		alert("There is no Gate Pass No to view");
	}
});
//----------------------------------	
$('#frmGatePass  #butConfirm').click(function(){
	if($('#txtGatePassNo').val()!=''){
		window.open('?q='+reportId+'&gatePassNo='+$('#txtGatePassNo').val()+'&year='+$('#txtGPYear').val()+'&grnType='+$('#txtItemType').val()+'&approveMode=1');	
	}
	else{
		alert("There is no Gate Pass No to confirm");
	}
});
//-----------------------------------------------------
$('#frmGatePass #butClose').click(function(){
		//load('main.php');	
});
//--------------refresh the form-----------------------
	$('#frmGatePass #butNew').click(function(){
		$('#frmGatePass').get(0).reset();
		clearRows();
		$('#frmGatePass #txtGrnNo').val('');
		$('#frmGatePass #txtGrnYear').val('');
		$('#frmGatePass #txtInvioceNo').val('');
		$('#frmGatePass #cboPO').val('');
		$('#frmGatePass #cboPO').focus();
		$('#frmGatePass #txtDeliveryNo').val('');
		$('#frmGatePass #txtTotGrnAmmount').val('');
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmGatePass #dtDate').val(d);
	});
//-----------------------------------------------------
});

//----------end of ready -------------------------------

function loadPopup()
{
	
		popupWindow3('1');
		
		var currentItemType = $('#frmGatePass #txtItemType').val();
		var rowCount = document.getElementById('tblGatePass').rows.length;
		
		var grnNo = $('#txtGrnNo').val();
		$('#popupContact1').load(basePath+'gatepassPopup.php?',function(){
				//checkAlreadySelected();
				$('#butAdd').click(addClickedRows);
				$('#butClose1').click(disablePopup);
				
				if(currentItemType==1 && rowCount>1){	
				$('#frmGatePassPopup #cboItemType').val('1').prop('selected', true);			
				$('#frmGatePassPopup #cboItemType').prop('disabled', true);
				}
				if(currentItemType==0 && rowCount>1){
				$('#frmGatePassPopup #cboItemType').val('0').prop('selected', true);
				$('#frmGatePassPopup #cboItemType').prop('disabled', true);
				$('#frmGatePassPopup #cboOrderYear').val('');
				$('#frmGatePassPopup #cboOrderNo').val('');
				$('#frmGatePassPopup #cbStyleNo').val('');
				document.getElementById('rw1').style.display='none';
				document.getElementById('rw2').style.display='none';
				}
				 //-------------------------------------------- 
				  $('#frmGatePassPopup #cboItemType').change(function(){
					    clearPopupRows();
						var itemType = $('#cboItemType').val();
						if(itemType==0){
							//document.getElementById('#divBom').style.display='none';
							$('#frmGatePassPopup #cboOrderYear').val('');
							$('#frmGatePassPopup #cboOrderNo').val('');
							$('#frmGatePassPopup #cbStyleNo').val('');
							$('#frmGatePassPopup #salesOrderTbl').html('');
							
							document.getElementById('rw1').style.display='none';
							document.getElementById('rw2').style.display='none';
							document.getElementById('rw3').style.display='none';
						}
						else{
							document.getElementById('rw1').style.display='';
							document.getElementById('rw2').style.display='';
						}
						
				  });
				 //-------------------------------------------- 
				  $('#frmGatePassPopup #cboMainCategory').change(function(){
						var mainCategory = $('#cboMainCategory').val();
						var url 		= basePath+"gatepass-db-get.php?requestType=loadSubCategory&mainCategory="+mainCategory;
						var httpobj 	= $.ajax({url:url,async:false})
						document.getElementById('cboSubCategory').innerHTML=httpobj.responseText;
						
				  });
				  //------------------------------------------
				  $('#frmGatePassPopup  #chkAll').click(function(){
						if(document.getElementById('chkAll').checked==true)
						 var chk=true;
						else
						 var chk=false;
						
						var rowCount = document.getElementById('tblItemsPopup').rows.length;
						for(var i=1;i<rowCount;i++)
						{
							document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].checked=chk;
						}
					});
				 //-------------------------------------------- 				 	
				  $('#frmGatePassPopup #cboOrderYear').change(function(){
						var orderYear = $('#cboOrderYear').val();
						var url 		= basePath+"gatepass-db-get.php?requestType=loadOrderNo&orderYear="+orderYear;
						var httpobj 	= $.ajax({url:url,async:false})
						document.getElementById('cboOrderNo').innerHTML=httpobj.responseText;
						
				  });
				  //-------------------------------------------- 
				 /* $('#frmGatePassPopup #cboOrderNo').change(function(){
						var orderNo = $('#cboOrderNo').val();
						var url 		= basePath+"gatepass-db-get.php?requestType=loadStyleNo&orderNo="+orderNo;
						var httpobj 	= $.ajax({url:url,async:false})
						document.getElementById('cbStyleNo').innerHTML=httpobj.responseText;
						
				  });*/
				   //-------------------------------------------- 
				  $('#frmGatePassPopup #cboOrderNo').change(function(){
						var orderNo = $('#cboOrderNo').val();
						var url 		= basePath+"gatepass-db-get.php?requestType=loadStyleNo&orderNo="+orderNo;
						var httpobj 	= $.ajax({url:url,async:false})
						//alert(httpobj.responseText);
						document.getElementById('salesOrderTbl').innerHTML=httpobj.responseText;
						
				  });

				 //-------------------------------------------- 
				  $('#frmGatePassPopup #imgSearchItems').click(function(){
					  	
						
					  	var rowCount = document.getElementById('tblItemsPopup').rows.length;
						for(var i=1;i<rowCount;i++)
						{
							document.getElementById('tblItemsPopup').deleteRow(1);
						}
						var itemType = $('#cboItemType').val();
						var orderYear = $('#cboOrderYear').val();
						var orderNo = $('#cboOrderNo').val();
						var salesOrderId = '';
						var salesOrderNo = '';
						var mainCategory = $('#cboMainCategory').val();
						var subCategory = $('#cboSubCategory').val();
						var description = $('#txtItmDesc').val();
						
						//salesOrders
						var arr="[";
						var i=0;
						var salesOrderId_list	='';
						var salesOrderNo_list	='';
						$('#salesOrderTbl .chkSo').each(function(){
							i++;
 							var salesOrderId = 	$(this).parent().parent().find('.so').attr('id');
 							var salesOrderNo = 	$(this).parent().parent().find('.so').html();
							//alert(salesOrderNo);
								if(document.getElementById('salesOrderTbl').rows[i].cells[0].childNodes[0].checked==true){
									so_selected_flag=1;
									arr += "{";
									arr += '"so":"'+		salesOrderId  +'"' ;
									arr +=  '},';
									//alert(salesOrderNo);
									if(salesOrderId_list==''){
									salesOrderId_list += salesOrderId;
									salesOrderNo_list += salesOrderNo;
									}
									else {
									salesOrderId_list += ','+salesOrderId;
									salesOrderNo_list += ','+salesOrderNo;
									}
								}
 						});
						arr = arr.substr(0,arr.length-1);
						arr += " ]";
						//						
						//alert(salesOrderNo_list);
						if(itemType==1){//bom items
							if(orderYear==''){
								alert("Please select Order Year");
								return false;
							}
							else if(orderNo==''){
								alert("Please select Order No");
								return false;
							}
							else if(so_selected_flag==0){
								alert("Please select atleast one Sales Order");
								return false;
							} 
							/*else if(mainCategory==''){
								alert("Please select the main category");
								return false;
							}*/
						}
						else if(itemType==0){//bom items
							if(mainCategory==''){
								alert("Please select main category");
								return;
							}
							else if(subCategory==''){
								alert("Please select sub category");
								return;
							}
						}
						showWaiting();
						
						if(itemType==0){//genaral items
						var url 		= basePath+"gatepass-db-get.php?requestType=loadGeneralItems";
						}
						else if(itemType==1){//bom items
						var url 		= basePath+"gatepass-db-get.php?requestType=loadBomItems";
						}
						
						var httpobj = $.ajax({
							url:url,
							dataType:'json',
							data:"orderNo="+orderNo+"&mainCategory="+mainCategory+"&subCategory="+subCategory+"&salesOrderNo="+arr+"&description="+description, 
							async:false,
							success:function(json){

								var length = json.arrCombo.length;
								var arrCombo = json.arrCombo;
								
								if(arrCombo[0]=='none'){
									alert("MRN items not found.");
									return;
									//hideWaiting();
								}

								if(!orderNo){
										orderNo = '';
								}
								if(!salesOrderId){
									salesOrderId = '';
								}
									
								if(!salesOrderNo){
										salesOrderNo = '';
								}
									
 
								var salesOrderId_sel=salesOrderId_list;
								var salesOrderNo_sel=salesOrderNo_list;
							
								for(var i=0;i<length;i++)
								{
									var itemId=arrCombo[i]['itemId'];	
									var maincatId=arrCombo[i]['maincatId'];	
									var subCatId=arrCombo[i]['subCatId'];	
									var code=arrCombo[i]['code'];
									var uom=arrCombo[i]['uom'];
									var itemName=arrCombo[i]['itemName'];
                                                                        var itemHide=arrCombo[i]['ITEM_HIDE'];
                                                                        var supItemCode=arrCombo[i]['supItemCode'];
									var stockBal=arrCombo[i]['stockBal'];
									var confirmedGPqty=arrCombo[i]['confirmedGPqty'];
									var mainCatName=arrCombo[i]['mainCatName'];
									var subCatName=arrCombo[i]['subCatName'];	
									var strSalesOrderNo=arrCombo[i]['strSalesOrderNo'];
									var strSalesOrderName=arrCombo[i]['strSalesOrderName'];
									var intSalesOrderId=arrCombo[i]['intSalesOrderId'];									
									var actualPriQty=arrCombo[i]['actualPriQty'];
									//var intMrnNo=arrCombo[i]['intMrnNo'];
									//var intMrnYear=arrCombo[i]['intMrnYear'];
									var mrnNo=arrCombo[i]['mrnNo'];
									var actualMrnQty=arrCombo[i]['actualMrnQty'];
									var type=arrCombo[i]['type'];
									
									var salesOrderPRIQty_ini_list=arrCombo[i]['salesOrderPRIQty_ini_list'];
									var salesOrderPRIQty_list=arrCombo[i]['salesOrderPRIQty_list'];	
									var salesOrderBalQty_list=arrCombo[i]['salesOrderBalQty_list'];	
									var salesOrderBalQty_based_mrn_list=arrCombo[i]['salesOrderBalQty_based_mrn_list'];	
									
									
									var actualPriQty=arrCombo[i]['actualPriQty'];
									var actualMrnQty=arrCombo[i]['actualMrnQty'];
									if(!actualPriQty)
										actualPriQty = 0;
									//if(!actualPriQty_ini)
									//	actualPriQty_ini = 0;
									if(!actualMrnQty)
										actualMrnQty = 0;
										
									var content='<tr class="normalfnt">';
									if(stockBal<=0){
									content +='<td align="center" bgcolor="#FFFFFF" id=""><input type="checkbox" id="chkDisp" disabled="disabled"/></td>';
									}
									else{
									content +='<td align="center" bgcolor="#FFFFFF" id=""><input type="checkbox" id="chkDisp" /></td>';
									}
									content +='<td align="center" bgcolor="#FFFFFF" id="'+mrnNo+'" class="intMrnNoP">'+mrnNo+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'" class="mainCatNameP">'+mainCatName+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'" class="subCatNameP">'+subCatName+'</td>';
									if(supItemCode!=null){
                                                                        content +='<td align="center" bgcolor="#FFFFFF" id="'+code+'" class="codeP">'+supItemCode+'</td>';    
                                                                        }else{
                                                                
                                                                        content +='<td align="center" bgcolor="#FFFFFF" id="'+code+'" class="codeP">'+code+'</td>';
                                                                        }
                                                                        if(itemHide==1)
                                                                        {
									content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="itemP">*****</td>';
                                                                        }else{
                                                                        content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="itemP">'+itemName+'</td>';   
                                                                        }
									content +='<td align="center" bgcolor="#FFFFFF" id="'+uom+'" class="uomP">'+uom+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+orderNo+'" style="display:none"  class="orderNoP">'+orderNo+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+salesOrderId_list+'" style="display:none"  class="salesOrderNoP">'+salesOrderNo_list+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+salesOrderBalQty_list+'" style="display:none"  class="productionQtyP">'+salesOrderPRIQty_list+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+salesOrderPRIQty_ini_list+'" class="actualPriQtyP">'+actualPriQty+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+salesOrderBalQty_based_mrn_list+'" class="QtyMrn">'+actualMrnQty+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="" class="Qt">'+confirmedGPqty+'</td>';
								if(type==6){
									content +='<td align="center" bgcolor="#FFFFFF" id="" class="">'+'<input onkeyup="checkQty(this)" id="form-validation-field-0" class="validate[required,custom[number]] calculateValue totQtyP" style="width:80px;text-align:center" value="'+actualMrnQty+'" type="text"></td>';
								}
								else{
									content +='<td align="center" bgcolor="#FFFFFF" id="" class="">'+'<input onkeyup="checkQty(this)" id="form-validation-field-0" class="validate[required,custom[number]] calculateValue totQtyP" style="width:80px;text-align:center" value="'+actualPriQty+'" type="text"></td>';
								}
									content +='<td align="center" bgcolor="#FFFFFF" id="'+stockBal+'" class="stockBalP">'+stockBal+'</td></tr>';

									add_new_row('#frmGatePassPopup #tblItemsPopup',content);
									
								}
									checkAlreadySelected();

							}
						});hideWaiting();
						
				  });
				  //-------------------------------------------------------
			});	
			
 $('#frmGatePassPopup .chkSoAll').die('click').live('click',function(){
 		if(document.getElementById('chkSoAll').checked==true)
		 var chk=true;
		else
		 var chk=false;
 		var rowCount = document.getElementById('salesOrderTbl').rows.length;
		for(var i=1;i<rowCount;i++)
		{
			//alert(document.getElementById('salesOrderTbl').rows[i].cells[0].childNodes[0].html());
			document.getElementById('salesOrderTbl').rows[i].cells[0].childNodes[0].checked=chk;
		}
	});
			
}//-------------------------------------------------------
function addClickedRows()
{
	var itemType = $('#cboItemType').val();
	$('#frmGatePass #txtItemType').val(itemType);
	
	//var rowCount = $('#tblDispatchPopup >tr').length;
	var rowCount = document.getElementById('tblItemsPopup').rows.length;

	var i=0;
	$('#tblItemsPopup .itemP').each(function(){
			i++;
 			var itemId=$(this).attr('id');
			var orderNo=$(this).parent().find(".orderNoP").html();
			//alert(orderNo+'/1');
			var itemName=$(this).html();
			if((document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].checked==true) && (document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].disabled==false)){
				checkAndDeleteExisting(orderNo,itemId,itemName);
			}
	});
	
	
	var i=0;
	$('#tblItemsPopup .itemP').each(function(){
		i++;
		if((document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].checked==true) && (document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].disabled==false)){
			
			var salesOrderNo=$(this).parent().find(".salesOrderNoP").html();
			var salesOrderId=$(this).parent().find(".salesOrderNoP").attr('id');
			var PRI_initial_list=$(this).parent().find(".productionQtyP").html();//initial pri qtys or if additional item, mrn qty
			var actualBalQty=$(this).parent().find(".productionQtyP").attr('id');//bal to gp (pri-gp) list
			var actualBalQty_mrn=$(this).parent().find(".QtyMrn").attr('id');//bal to gp (mrn-gp)
			var pri_ini			=$(this).parent().find(".actualPriQtyP").attr('id');//bal to gp (pri-gp) list
			
			var itemId=$(this).attr('id');
			var maincatId=$(this).parent().find(".mainCatNameP").attr('id');
			var subCatId=$(this).parent().find(".subCatNameP").attr('id');
			var code=$(this).parent().find(".codeP").html();
			var uom=$(this).parent().find(".uomP").html();
			var itemName=$(this).parent().find(".itemP").html();
			var mainCatName=$(this).parent().find(".mainCatNameP").html();
			var subCatName=$(this).parent().find(".subCatNameP").html();
			var mrnQty=parseFloat($(this).parent().find(".mrnQtyP").html());
			var issueQty=parseFloat($(this).parent().find(".issueQtyP").html());
			var stockBalance=parseFloat($(this).parent().find(".stockBalQtyP").html());
			var qty=$(this).parent().find(".totQtyP").val();// 

			var arr_so = salesOrderId.split(",");
			var arr_so_no = salesOrderNo.split(",");

			var arr_pri_ini = pri_ini.split(",");//pri or mrn
			var arr_pri = PRI_initial_list.split(",");//pri or mrn
			var arr_bal = actualBalQty.split(",");
			var arr_bal_mrn = actualBalQty_mrn.split(",");
			

			var index 		=0;
			var sum   		=0;
			var sum_pri_act	=0;//pri ir mrn
			var sum_pri   	=0;//pri ir mrn
			var sum_act_bal	=0;
			var sum_mrn_bal	=0;
			var flag		=0;
			
			for (index = 0; index < arr_pri.length; ++index) {
				sum_pri_act		+=parseFloat(arr_pri_ini[index]);
				sum_pri		+=parseFloat(arr_pri[index]);
  				sum_act_bal	+=parseFloat(arr_bal[index]);
  				sum_mrn_bal	+=parseFloat(arr_bal_mrn[index]);
				if(arr_so[index].split("/")[0]>0)
					flag=1;//order wise gp
					//alert(sum_pri_act+'/'+sum_pri+'/'+sum_act_bal+'/'+sum_mrn_bal);
			}
   		
			var index =0;
			var actualQty_prapo		= 0;
			var actualExQty_prapo	= 0;
			var actualQtyBal		= 0;
			var pri					= 0;
			if(flag>0){
				for (index = 0; index < arr_pri.length; ++index) {

						
						if(sum_mrn_bal>0){//additional item
							actualQty_prapo		= (parseFloat(arr_bal_mrn[index])/sum_mrn_bal*parseFloat(qty)).toFixed(4);
						}
						else if(sum_act_bal>0){ //pri item.but no pri/mrn balance
							actualQty_prapo		= (parseFloat(arr_bal[index])/sum_act_bal*parseFloat(qty)).toFixed(4);
						}
						else{ //order wise ,no balance
							actualQty_prapo		= (parseFloat(arr_pri[index])/sum_pri*parseFloat(qty)).toFixed(4);
						}
						
 						pri				= arr_pri[index];
						var salesOrder_1 = arr_so[index];
						var arr_so_1 = salesOrder_1.split("/");
						var salesOrderNo_1 = arr_so_no[index];

				var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF"><img class="delImg" src="images/del.png" width="15" height="15" /></td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="'+arr_so_1[0]+'/'+arr_so_1[1]+'" class="orderNo">'+arr_so_1[0]+'/'+arr_so_1[1]+'</td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="'+arr_so_1[2]+'" class="salesOrderNo">'+salesOrderNo_1+'</td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'" class="mainCatName">'+mainCatName+'</td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'" class="subCatName">'+subCatName+'</td>';
                                
				content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="item">'+itemName+'</td>';
                                  
				content +='<td align="center" bgcolor="#FFFFFF" id="'+uom+'" class="uom">'+uom+'</td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="'+arr_bal[index]+'" class="balToIssueQty">'+arr_bal[index]+'</td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="'+arr_bal_mrn[index]+'" class="balToIssueQty">'+arr_bal_mrn[index]+'</td>';
				if(flag==1){
				content +='<td align="center" bgcolor="#FFFFFF" id="" class="orderNo"><input  id="'+arr_bal[index]+'" class="validate[required,custom[number]] calculateValue Qty" style="width:80px;text-align:center" type="text" value="'+parseFloat(actualQty_prapo)+'"/></td>';
				}
				else{
				content +='<td align="center" bgcolor="#FFFFFF" id="" class="orderNo"><input  id="'+arr_bal_mrn[index]+'" class="validate[required,custom[number]] calculateValue Qty" style="width:80px;text-align:center" type="text" value="'+parseFloat(actualQty_prapo)+'"/></td>';
				}
				content +='</tr>';
				
				
			//	content +='<td align="center" bgcolor="#FFFFFF" id="" class=""><input style="width:80px;text-align:center" type="text" value="0"/></td>';
				if((actualQty_prapo>0))//exists in pri
				add_new_row('#frmGatePass #tblGatePass',content);
			//	totGRNAmmount+=ammount;
				//$('.')
				//$("#frmIssue").validationEngine();
				}
			}
			else{
				var actualQty_prapo		= 0;
				var actualExQty_prapo	= 0;
				var actualQtyBal		= 0;
				var pri					= 0;
				var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF"><img class="delImg" src="images/del.png" width="15" height="15" /></td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="" class="orderNo"></td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="" class="salesOrderNo"></td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'" class="mainCatName">'+mainCatName+'</td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'" class="subCatName">'+subCatName+'</td>';
                                if(maincatId==1)
                                {
				content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="item"></td>';
                                }else{
                                content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="item">'+itemName+'</td>';  
                                }
				content +='<td align="center" bgcolor="#FFFFFF" id="'+uom+'" class="uom">'+uom+'</td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="" class="balToIssueQty"></td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="" class="balToIssueQty"></td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="" class="orderNo"><input  id="" class="validate[required,custom[number]] calculateValue Qty" style="width:80px;text-align:center" type="text" value="'+parseFloat(qty)+'"/></td>';
				content +='</tr>';

				add_new_row('#frmGatePass #tblGatePass',content);
		
			}
		//----------------------------	
		$('.delImg').click(function(){
			$(this).parent().parent().remove();
		});
		
		}
	});
	disablePopup();
	closePopUp();
}
//-------------------------------------------------------
function addClickedRows_2017_04_21()
{
	var itemType = $('#cboItemType').val();
	$('#frmGatePass #txtItemType').val(itemType);
	var i=0;
	$('#tblItemsPopup .itemP').each(function(){
		i++;
		
		if((document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].checked==true) && (document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].disabled==false)){
			
			var intOrderNo=$(this).parent().find(".orderNoP").attr('id');
			var salesOrderNo=$(this).parent().find(".salesOrderNoP").html();
			var salesOrderId=$(this).parent().find(".salesOrderNoP").attr('id');
			var itemId=$(this).attr('id');
			var maincatId=$(this).parent().find(".mainCatNameP").attr('id');
			var subCatId=$(this).parent().find(".subCatNameP").attr('id');
			var code=$(this).parent().find(".codeP").html();
			var uom=$(this).parent().find(".uomP").html();
			var itemName=$(this).parent().find(".itemP").html();
			var mainCatName=$(this).parent().find(".mainCatNameP").html();
			var subCatName=$(this).parent().find(".subCatNameP").html();
			var stockBalance=$(this).parent().find(".stockBalP").html();
			var confirmedGPqty=$(this).parent().find(".stockBalP").attr('id');
			//var maxQty=stockBalance-confirmedGPqty;
			var maxQty=stockBalance;
			
			var intSalesOrderId	=$(this).parent().find(".salesOrderP").attr('id');
			var salesOrderNo	=$(this).parent().find(".salesOrderP").html();
			var actualPriQty	=$(this).parent().find(".actualPriQtyP").html();
			var actualMrnQty	=$(this).parent().find(".actualMrnQtyP").html();
			var actualPriQty_ini=$(this).parent().find(".actualPriQtyP").attr('id');

			//alert($('#frmGatePass #cboDispatchTo').val());
			var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF"><img class="delImg" src="images/del.png" width="15" height="15" /></td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+intOrderNo+'" class="orderNo">'+intOrderNo+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+intSalesOrderId+'" class="salesOrderNo">'+salesOrderNo+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'" class="mainCatName">'+mainCatName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'" class="subCatName">'+subCatName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="item">'+itemName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+uom+'" class="uom">'+uom+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+actualPriQty+'" class="actualPriQty">'+actualPriQty+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+actualMrnQty+'" class="actualMrnQty">'+actualMrnQty+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id=""><input  id="'+maxQty+'" class="validate[required,custom[number],max['+maxQty+']] calculateValue Qty" style="width:80px;text-align:center" type="text" value="0"/></td>';
			content +='</tr>';
			
			add_new_row('#frmGatePass #tblGatePass',content);
		//----------------------------	
		$('.delImg').click(function(){
			$(this).parent().parent().remove();
		});
		
		//----------------------------
	}
	});
	disablePopup();
	closePopUp();
}
//-------------------------------------
function alertx()
{
	$('#frmGatePass #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmGatePass #butDelete').validationEngine('hide')	;
}

function closePopUp(){
	
}
//----------------------------------------------
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
//----------------------------------------------- 
function clearRows()
{
	//var rowCount = $('#tblDispatchPopup >tbody >tr').length;
	var rowCount = document.getElementById('tblGatePass').rows.length;
	//alert(rowCount);
	//var cellCount = document.getElementById('tblMain').rows[1].cells.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblGatePass').deleteRow(1);
			
	}
}
//----------------------------------------------- 
function clearPopupRows()
{
	//var rowCount = $('#tblDispatchPopup >tbody >tr').length;
	var rowCount = document.getElementById('tblItemsPopup').rows.length;
	//alert(rowCount);
	//var cellCount = document.getElementById('tblMain').rows[1].cells.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblItemsPopup').deleteRow(1);
			
	}
}
//------------------------------------------------------------
function checkAlreadySelected(){
	var rowCount = document.getElementById('tblGatePass').rows.length;
	for(var i=1;i<rowCount;i++)
	{
			var orderNo = 	document.getElementById('tblGatePass').rows[i].cells[1].innerHTML;
			var salesOrderNo = 	document.getElementById('tblGatePass').rows[i].cells[2].id;
			var itemId = 	document.getElementById('tblGatePass').rows[i].cells[5].id;
		
			var rowCount1 = document.getElementById('tblItemsPopup').rows.length;
			for(var j=1;j<rowCount1;j++)
			{
				var orderNoP = 	document.getElementById('tblItemsPopup').rows[j].cells[5].innerHTML;
				var salesOrderNoP = 	document.getElementById('tblItemsPopup').rows[j].cells[6].id;
				var itemIdP = 	document.getElementById('tblItemsPopup').rows[j].cells[4].id;
				
			//alert(orderNo+"=="+orderNoP+"***"+salesOrderNo+"=="+salesOrderNoP+"***"+mrnNo+"=="+mrnNoP+"***"+itemId+"=="+itemIdP+"***")
				if((orderNo==orderNoP) && (salesOrderNo==salesOrderNoP) && (itemId==itemIdP)){
					document.getElementById('tblItemsPopup').rows[j].cells[0].childNodes[0].checked=true;
					document.getElementById('tblItemsPopup').rows[j].cells[0].childNodes[0].disabled=true;
				}
			}
	}
}
function checkAndDeleteExisting(orderNoP,itemIdP,name){
		
		var orderNo ='';
		var salesOrderId ='';
			var r=0;
			$('#tblGatePass .item').each(function(){
	
				orderNo = 	$(this).parent().find(".orderNo").html();
				salesOrderId = 	$(this).parent().find(".salesOrderNo").attr('id');
				var itemId	= $(this).attr('id');
				
				if(orderNoP==orderNo /*&& so==salesOrderId */&& itemIdP==itemId){
					r++;
					$(this).parent().remove();
				}
			});
	if(r>0)
		alert("Removed parent form item :"+orderNo+'/'+name);

	
}

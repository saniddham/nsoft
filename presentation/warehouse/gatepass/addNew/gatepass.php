<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
$mainPath 			= $_SESSION['mainPath'];
$location 			= $_SESSION['CompanyID'];
$company 			= $_SESSION['headCompanyId'];
$thisFilePath	 	=  $_SERVER['PHP_SELF'];
$intUser  			= $_SESSION["userId"];

$gatePassNo 		= $_REQUEST['gatePassNo'];
$year 				= $_REQUEST['year'];

$programName		='Gate Pass';
$programCode		='P0250';

if(($gatePassNo=='')&&($year=='')){
	$savedStat 		= '';
	$department		=loadDefaultDepartment($intUser);
	$intStatus		=$savedStat+1;
	$date			='';
	$type			='2';  //general - 0, orderwise - 1
}
else{
	$result=loadHeader($gatePassNo,$year);
	while($row=mysqli_fetch_array($result))
	{
		$locId 			= $row['intGPToLocation'];
		$note			= $row['strNote'];
		$date		 	= $row['datdate'];
		$savedStat		=$row['intApproveLevels'];
		$intStatus		=$row['intStatus'];
		$type			=$row['gpType'];
	}
}

$editMode			=loadEditMode($programCode,$intStatus,$savedStat,$intUser);
$confirmatonMode	=loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser);
if($gatePassNo==''){
	$confirmatonMode=0;	
}
$mainPage			=$backwardseperator."main.php";
			 
?>
<head>
<title>Gate Pass</title>
<?php
/*include "include/javascript.html";
*/?>
<!--<script type="text/javascript" src="presentation/warehouse/gatepass/addNew/gatepass-js.js"></script>
--></head>

<body>
 
<form id="frmGatePass" name="frmGatePass" method="post" action="">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text"> Gate Pass</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="10%" height="22" class="normalfnt">Gate Pass No</td>
            <td width="22%"><input name="txtGatePassNo" type="text" disabled="disabled" class="txtText" id="txtGatePassNo" style="width:60px" value="<?php echo $gatePassNo ?>" /><input name="txtGPYear" type="text" disabled="disabled" class="txtText" id="txtGPYear" style="width:40px"  value="<?php echo $year ?>"/></td>
            <td width="17%"><div id="divItem" style="display:none"><input name="txtItemType" type="text" disabled="disabled" class="txtText" id="txtItemType" style="width:40px" value="<?php echo $type ?>"  /></div></td>
            <td width="25%">&nbsp;</td>
            <td width="9%" class="normalfnt">Date</td>
            <td width="17%" align="left"><input name="dtDate" type="text" value="<?php if($gatePassNo){ echo substr($date,0,10); }else { echo date("Y-m-d"); }?>" class="txtbox" id="dtDate" style="width:130px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"  <?php if($editMode==0){  ?> disabled="disabled"<?php } ?>/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="10%" height="27" rowspan="2" class="normalfnt">Note</td>
            <td width="38%" rowspan="2"><textarea name="txtNote" id="txtNote" cols="35" rows="3" class="validate[required]" <?php if($editMode==0){  ?> disabled="disabled"<?php } ?>><?php echo $note; ?></textarea></td>
            <td width="20%">&nbsp;</td>
            <td width="9%" class="normalfnt">Gate Pass To</td>
            <td width="23%" align="left"><select style="width:191px" name="cboGatePassTo" id="cboGatePassTo"   class="validate[required]"  <?php if($editMode==0){  ?> disabled="disabled"<?php } ?>>
              <option value=""></option>
              <?php
					$sql = "SELECT
							mst_locations.intId,
							mst_locations.strName as locName,
							mst_companies.strName as compName 
							FROM
							mst_locations
							Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId AND mst_locations.intCompanyId='$company' 
							AND mst_locations.intId!='$location'";
							$pp=$sql;
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$locId)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['compName']."-".$row['locName']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['compName']."-".$row['locName']."</option>";	
					}
				?>
              </select></td>
            </tr>
          <tr>
          <td></td>
          <td></td>
            <td align="right" valign="bottom"> <?php if($editMode!=0){  ?><img src="images/Tadd.jpg" width="92" height="24" id="butAddItems" name="butAddItems" /><?php } ?></td>
            </tr>
          </table></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="92%">&nbsp;</td>
            <td width="8%"></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><div style="width:900px;height:300px;overflow:scroll" >
          <table width="100%" class="bordered" id="tblGatePass" >
            <tr  >
              <th width="5%" height="22" >Del</th>
              <th width="10%" >Order No</th>
              <th width="11%" >Sales Order No</th>
              <th width="11%" >Main Category</th>
              <th width="11%" >Sub Category</th>
              <th width="30%" >Item Description</th>
              <th width="5%">UOM</th>
              <th width="7%">Bal to GP based on(PRI)</th>
              <th width="7%">Bal to GP based on(MRN)</th>
              <th width="10%">Qty</th>
              </tr>
              <?php
			     $sql = "SELECT
ware_gatepassdetails.intItemId,
mst_item.intMainCategory,
mst_item.strName AS item,
mst_item.intSubCategory,
mst_item.intBomItem,
mst_units.strCode as uom , 
mst_part.strName as part , 
mst_subcategory.strName AS subcat,
mst_maincategory.strName AS maincat,
ware_gatepassdetails.strOrderNo,
ware_gatepassdetails.intOrderYear,
trn_orderdetails.strSalesOrderNo,
trn_orderdetails.intSalesOrderId,
ware_gatepassdetails.strStyleNo,
ware_gatepassdetails.intItemId,
ware_gatepassdetails.dblQty,
mst_item.ITEM_HIDE
FROM
ware_gatepassdetails 
left Join trn_orderdetails ON ware_gatepassdetails.strOrderNo = trn_orderdetails.intOrderNo AND ware_gatepassdetails.intOrderYear = trn_orderdetails.intOrderYear AND ware_gatepassdetails.strStyleNo = trn_orderdetails.intSalesOrderId
Inner Join mst_item ON ware_gatepassdetails.intItemId = mst_item.intId
Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
Inner Join mst_units ON mst_item.intUOM = mst_units.intId  
left Join mst_part ON trn_orderdetails.intPart = mst_part.intId 
WHERE 
mst_item.intStatus = '1' AND 
ware_gatepassdetails.intGatePassNo =  '$gatePassNo' AND
ware_gatepassdetails.intGatePassYear =  '$year'  
Order by mst_maincategory.strName asc, mst_subcategory.strName asc, mst_item.strName asc";
				$result = $db->RunQuery($sql);
				$totAmm=0;
				while($row=mysqli_fetch_array($result))
				{
					$confirmedGPqty=get_gpQty($location,$row['strOrderNo'],$row['intOrderYear'],$row['strStyleNo'],$row['intItemId']);//but not raised final confirmation
					
					if($row['intBomItem']==0){
					$stockBalQty=getStockBalance_bulk($location,$row['intItemId']);
					}
					else{
					$stockBalQty=getStockBalance($company,$location,$row['strOrderNo'],$row['intOrderYear'],$row['strStyleNo'],$row['intItemId']);
					}
					
					$priQty=round(getPRIQty($row['strOrderNo'],$row['intOrderYear'],$row['strStyleNo'],$row['intItemId']),4);
					$gpQty=round(getGPQty($row['strOrderNo'],$row['intOrderYear'],$row['strStyleNo'],$row['intItemId']),4);
					$mrnQty=round(getMRNQty($row['strOrderNo'],$row['intOrderYear'],$row['strStyleNo'],$row['intItemId']),4);
					
					$actualPriQty=$priQty-$gpQty;
					
					if($actualPriQty<0){
						$actualPriQty=0;
					}
					
					$actualMrnQty=$mrnQty-$gpQty;
					
					if($actualMrnQty<0){
						$actualMrnQty=0;
					}
					
					$balToGp=$stockBalQty-$confirmedGPqty;
					$intOrderNo=$row['strOrderNo'];
					$intOrderYear=$row['intOrderYear'];
					$salesOrderNo=$row['strSalesOrderNo']."/".$row['part'];
					$salesOrderId=$row['intSalesOrderId'];
					$orderNo=$intOrderNo."/".$intOrderYear;
					if($row['strOrderNo']==0){
						$bomItemFlag=0;
					}
					else{
						$bomItemFlag=1;
					}
					
					$strStyleNo=$row['strStyleNo'];
					$maincatId=$row['intMainCategory'];
					$subCatId=$row['intSubCategory'];
					$mainCatName=$row['maincat'];
					$subCatName=$row['subcat'];
					$itemId=$row['intItemId'];
					$itemName=$row['item'];
                                        $itemHide=$row['ITEM_HIDE'];
					$uom=$row['uom'];
					$Qty=$row['dblQty'];
					
					if($bomItemFlag==0){
					$orderNo='';
					$salesOrderNo='';
					$salesOrderId='';
					$strStyleNo='';
					$priQty='';
					$mrnQty='';
					$actualPriQty='';
					}
					
					$totAmm+=$Qty;
			  ?>
<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF"><?php if($editMode==1){  ?><img class="delImg" src="images/del.png" width="15" height="15" /><?php } ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $orderNo; ?>" class="orderNo"><?php echo $orderNo; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $salesOrderId; ?>" class="salesOrderNo"><?php echo $salesOrderNo; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $maincatId; ?>" class="mainCatName"><?php echo $mainCatName; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $subCatId; ?>" class="subCatName"><?php echo $subCatName; ?></td>
                        <?php
                        if($itemHide==1)
                        {
                            ?>
                         <td align="center" bgcolor="#FFFFFF" id="<?php echo $itemId; ?>" class="item">*****</td> 
                         <?php
                        }else{
                            
                        
                        ?>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $itemId; ?>" class="item"><?php echo $itemName; ?></td>
                        <?php
                        }
                        ?>
			<td align="center" bgcolor="#FFFFFF" id="" class="uom"><?php echo $uom; ?></td>
            <td align="center" bgcolor="#FFFFFF" id="" class="actualPriQty"><?php echo $actualPriQty; ?></td>
            <td align="center" bgcolor="#FFFFFF" id="" class="actualMrnQty"><?php echo $actualMrnQty; ?></td>
			<td align="center" bgcolor="#FFFFFF" id=""><input  id="<?php echo $balToGp ?>" class="validate[required,custom[number],max[<?php echo $balToGp ?>]] calculateValue Qty" style="width:80px;text-align:right" type="text" value="<?php echo $Qty ?>" <?php if($editMode==0){  ?> disabled="disabled"<?php } ?>/></td>
			</tr>             <?php
				}
			  ?>            </table>
        </div></td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound">
        <a name="butNew"  class="button white medium" id="butNew">New</a>
         <?php if(($form_permision['add']||$form_permision['edit']) && $editMode==1){ ?>
         <a id="butSave" name="butSave"  class="button white medium">Save</a>
		 <?php } ?>
         <a id="butConfirm" name="butConfirm" style=" <?php if(($intStatus>1) and ($confirmatonMode==1) && $confirmatonMode==1){  echo 'display:yes'; } else { ?>  <?php echo 'display:none';  } ?>" class="button white medium">Approve</a>
         <a id="butReport" name="butReport"  class="button white medium">Report</a>
         <a href="main.php"  class="button white medium">Close</a></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
	<!-- items to prn -->
	<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1">
   <!-- <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php" style="width:800;height:800;border:0;overflow:hidden">
    </iframe>-->
    </div>

	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>


</body>
</html>
<?php
	//--------------------------------------------------------------
	function getStockBalance($company,$location,$orderNo,$orderYear,$style,$item)
	{
		global $db;
		   $sql = "SELECT
				Sum(ware_stocktransactions.dblQty) as stockBal 
				FROM ware_stocktransactions
				WHERE
				ware_stocktransactions.intCompanyId =  '$company' AND
				ware_stocktransactions.intLocationId =  '$location' AND
				ware_stocktransactions.intOrderNo =  '$orderNo' AND
				ware_stocktransactions.intOrderYear =  '$orderYear' AND
				ware_stocktransactions.strStyleNo =  '$style' AND
				ware_stocktransactions.intItemId =  '$item'";

		$result = $db->RunQuery($sql);
		$rows = mysqli_fetch_array($result);
		return (float)($rows['stockBal']);	
	}
	//--------------------------------------------------------------
	function getStockBalance_bulk($location,$item)
	{
		global $db;
		   $sql = "SELECT
				Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$location'
				GROUP BY
				ware_stocktransactions_bulk.intItemId";

		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return (float)($row['stockBal']);	
	}
//------------------------------function load Header---------------------
function loadHeader($serialNo,$year)
{
	global $db;
	   $sql = "SELECT
				ware_gatepassheader.datdate,
				ware_gatepassheader.intStatus,
				ware_gatepassheader.intApproveLevels,
				ware_gatepassheader.intUser,
				ware_gatepassheader.intGPToLocation,
				ware_gatepassheader.strNote,
				ware_gatepassheader.gpType,
				mst_companies.strName as delToCompany,
				mst_locations.strName as delToLocation,
				sys_users.strUserName  
				FROM
				ware_gatepassheader 
				Inner Join mst_locations ON ware_gatepassheader.intGPToLocation = mst_locations.intId
				Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
				Inner Join sys_users ON ware_gatepassheader.intUser = sys_users.intUserId
				WHERE
				ware_gatepassheader.intGatePassNo =  '$serialNo' AND
				ware_gatepassheader.intGatePassYear =  '$year' 
				GROUP BY
				ware_gatepassheader.intGatePassNo,
				ware_gatepassheader.intGatePassYear"; 
			 $result = $db->RunQuery($sql);
			 return $result;
}
//------------------------------function load Default Department---------------------
function loadDefaultDepartment($intUser){
	global $db;
	 $sql1 = "SELECT
				sys_users.intDepartmentId
				FROM
				sys_users
				WHERE
				sys_users.intUserId =  '$intUser'";	
	$result1 = $db->RunQuery($sql1);
	$row1=mysqli_fetch_array($result1);
	$department=$row1['intDepartmentId'];
	return $department;
}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
			 
	return $editMode;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	$sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
//------------------------------------------------------------
function get_gpQty($location,$orderNo,$orderYear,$styleNo,$itemId){
	global $db;
	 $sql1 = "SELECT
				Sum(ware_gatepassdetails.dblQty) as gpQty
				FROM
				ware_gatepassheader
				Inner Join ware_gatepassdetails ON ware_gatepassheader.intGatePassNo = ware_gatepassdetails.intGatePassNo AND ware_gatepassheader.intGatePassYear = ware_gatepassdetails.intGatePassYear
				WHERE
				ware_gatepassdetails.strOrderNo =  '$orderNo' AND
				ware_gatepassdetails.intOrderYear =  '$orderYear' AND
				ware_gatepassdetails.strStyleNo =  '$styleNo' AND
				ware_gatepassdetails.intItemId =  '$itemId' AND
				ware_gatepassheader.intStatus >  '1' AND
				ware_gatepassheader.intStatus <= ware_gatepassheader.intApproveLevels AND
				ware_gatepassheader.intCompanyId =  '$location'";	
	$result1 = $db->RunQuery($sql1);
	$row1=mysqli_fetch_array($result1);
	$gpQty=$row1['gpQty'];
	return $gpQty;
}

function getGPQty($orderNo,$orderYear,$salesOrderNo,$item)
{
		global $db;
		   $sql = "SELECT
				IFNULL(sum(ware_gatepassdetails.dblQty),'0') AS gpQty
			FROM
				ware_gatepassdetails
			INNER JOIN ware_gatepassheader ON ware_gatepassheader.intGatePassNo = ware_gatepassdetails.intGatePassNo
			AND ware_gatepassheader.intGatePassYear = ware_gatepassdetails.intGatePassYear
			WHERE
				ware_gatepassdetails.strOrderNo = '$orderNo'
			AND ware_gatepassdetails.intOrderYear = '$orderYear'
			AND ware_gatepassdetails.strStyleNo = '$salesOrderNo'
			AND ware_gatepassdetails.intItemId = '$item'
			AND ware_gatepassheader.intStatus = '1'";
		//echo $sql;
				
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$gpQty=$row['gpQty'];
		return $gpQty;
}
//------------------------------------------------------------

	function getPRIQty($orderNo,$orderYear,$salesOrderNo,$item)
	{
		global $db;
		   $sql = "	SELECT
						IFNULL(trn_po_prn_details_sales_order.REQUIRED,'0') AS priQty
					FROM
						trn_po_prn_details_sales_order
					WHERE
						trn_po_prn_details_sales_order.ORDER_NO = '$orderNo'
					AND trn_po_prn_details_sales_order.ORDER_YEAR = '$orderYear'
					AND trn_po_prn_details_sales_order.SALES_ORDER = '$salesOrderNo'
					AND trn_po_prn_details_sales_order.ITEM = '$item'";
		//echo $sql;
				
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$priQty=$row['priQty'];
		//return val($row['priQty']);	
		return $priQty;
	}
//--------------------------------------------------------------
function getMRNQty($orderNo,$orderYear,$salesOrderNo,$item)
	{
		global $db;
		   $sql = "SELECT
				IFNULL(sum(ware_mrndetails.dblQty),'0') AS mrnQty
			FROM
				ware_mrndetails
			INNER JOIN ware_mrnheader ON ware_mrndetails.intMrnNo = ware_mrnheader.intMrnNo
			AND ware_mrndetails.intMrnYear = ware_mrnheader.intMrnYear
			WHERE
				ware_mrndetails.intOrderNo = '$orderNo'
			AND ware_mrndetails.intOrderYear = '$orderYear'
			AND ware_mrndetails.strStyleNo = '$salesOrderNo'
			AND ware_mrndetails.intItemId = '$item'
			AND ware_mrnheader.intStatus = '1'";
		//echo $sql;
				
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$mrnQty = $row['mrnQty'];
		//return val($row['mrnQty']);	
		return $mrnQty;
	}
//--------------------------------------------------------------
?>

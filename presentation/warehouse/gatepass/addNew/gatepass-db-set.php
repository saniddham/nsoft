<?php
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////

session_start();
$backwardseperator = "../../../../";
$mainPath 	= $_SESSION['mainPath'];
$userId 	= $_SESSION['userId'];
$companyId 	= $_SESSION['CompanyID'];
$company 	= $_SESSION['headCompanyId'];
$location 	= $_SESSION['CompanyID'];
//ini_set('display_errors',1);

include "{$backwardseperator}dataAccess/Connector.php";
require_once 	"../../../../class/warehouse/gatepass/cls_gatepass_get.php";
require_once 	"../../../../class/finance/budget/cls_common_function_budget_get.php";
require_once 	"../../../../class/finance/budget/entering/cls_budget_set.php";
require_once 	"../../../../class/cls_commonFunctions_get.php";

$obj_gatepass_get		= new cls_gatepass_get($db);
$obj_comm_budget_get	= new cls_common_function_budget_get($db);
$obj_budget_set			= new cls_budget_set($db);
$obj_common				= new cls_commonFunctions_get($db);

$response = array('type'=>'', 'msg'=>'');

/////////// parameters /////////////////////////////
/////////// parameters /////////////////////////////
$requestType 	= $_REQUEST['requestType'];

$serialNo 	 = $_REQUEST['serialNo'];
$year 		 = $_REQUEST['Year'];
$gatePassTo  = $_REQUEST['gatePassTo'];
$note 	 	 = replace1($_REQUEST['note']);
$date 		 = $_REQUEST['date'];
$type 		 = $_REQUEST['type'];

$arr 		= json_decode($_REQUEST['arr'], true);

$programName='Gate Pass';
$programCode='P0250';

$ApproveLevels = (int)getApproveLevel($programName);
$gatePassApproveLevel = $ApproveLevels+1;

//------------save---------------------------	
if($requestType=='save')
{
	$db->OpenConnection();
	$db->RunQuery2('Begin');
	$savableFlag= 1;

	if($serialNo==''){
		$serialNo 	= getNextGatepassNo();
		$year = date('Y');
		$editMode=0;
		$savedStatus=$gatePassApproveLevel;
		$savedLevels=$ApproveLevels;
	}
	else{
		$savableFlag = getSaveStatus($serialNo,$year);
		$editMode=1;
		$sql = "SELECT
			ware_gatepassheader.intStatus, 
			ware_gatepassheader.intApproveLevels 
			FROM ware_gatepassheader 
			WHERE
			ware_gatepassheader.intGatePassNo =  '$serialNo' AND
			ware_gatepassheader.intGatePassYear =  '$year'";

		$result = $db->RunQuery2($sql);
		$row=mysqli_fetch_array($result);
		$savedStatus=$row['intStatus'];
		$savedLevels=$row['intApproveLevels'];
	}
	//-----------delete and insert to header table-----------------------
	//--------------------------
	$editPermission=loadEditMode($programCode,$savedStatus,$savedLevels,$userId);
	$locationEditFlag=getEditLocationValidation('GATEPASS',$companyId,$serialNo,$year);
	//--------------------------

	if(( $locationEditFlag==1) && ($editMode==1)){
		$rollBackFlag=1;
		$rollBackMsg="Invalid Edit Location";
	}
	else if($savableFlag==0){
		$rollBackFlag=1;
		$rollBackMsg="This Gate Pass No is already confirmed.cant edit";
	}
	else if($editPermission==0){
		$rollBackFlag=1;
		$rollBackMsg="No Permission to save.";
	}
	else if($editMode==1){//edit existing grn
		$sql = "UPDATE `ware_gatepassheader` SET intStatus ='$gatePassApproveLevel', 
												       intApproveLevels ='$ApproveLevels', 
													  intGPToLocation ='$gatePassTo', 
													  strNote ='$note', 
													  intModifiedBy ='$userId', 
													  gpType ='$type', 
													  dtmModifiedDate =now() 
				WHERE (`intGatePassNo`='$serialNo') AND (`intGatePassYear`='$year')";
		$result = $db->RunQuery2($sql);
	}
	else{
		//$sql = "DELETE FROM `ware_gatepassheader` WHERE (`intGatePassNo`='$serialNo') AND (`intGatePassYear`='$year')";
		//$result1 = $db->RunQuery2($sql);

		$sql = "INSERT INTO `ware_gatepassheader` (`intGatePassNo`,`intGatePassYear`,intGPToLocation,strNote,intStatus,intApproveLevels,datdate,dtmCreateDate,intUser,intCompanyId,gpType) 
					VALUES ('$serialNo','$year','$gatePassTo','$note','$gatePassApproveLevel','$ApproveLevels',now(),now(),'$userId','$companyId','$type')";
		$result = $db->RunQuery2($sql);
		$maxAppByStatus=(int)getMaxAppByStatus($serialNo,$year)+1;
		$sql = "UPDATE `ware_gatepassheader_approvedby` SET intStatus ='$maxAppByStatus' 
					WHERE (`intGatePassNo`='$serialNo') AND (`intYear`='$year') AND (`intStatus`='0')";
		$result1 = $db->RunQuery2($sql);
	}
	//-----------delete and insert to detail table-----------------------
	if(($result) && ($rollBackFlag!=1)){
		$sql = "DELETE FROM `ware_gatepassdetails` WHERE (`intGatePassNo`='$serialNo') AND (`intGatePassYear`='$year')";
		$result2 = $db->RunQuery2($sql);
		$toSave=0;
		$saved=0;
		$rollBackFlag=0;
		$rollBackMsg ="Maximum Gate Pass Qtys for items are...";
		$budgetFlag	 = 0;

		foreach($arr as $arrVal)
		{
			$location 	= $_SESSION['CompanyID'];
			$company 	= $_SESSION['headCompanyId'];
			$orderNo 	 = $arrVal['orderNo'];
			$orderNoArray   = explode('/',$orderNo);
			$allocatedFlag=1;

			if($orderNoArray[0]==''){
				$orderNoArray[0]=0;
				$allocatedFlag=0;
			}
			else{
				$allocatedFlag=1;
			}

			if($orderNoArray[1]==''){
				$orderNoArray[1]=0;
			}
			$salesOrderId 		 = $arrVal['salesOrderId'];
			$itemId 	 = $arrVal['itemId'];
			$Qty 		 = $arrVal['Qty'];

			$itemName=getItemName($itemId);

			if($allocatedFlag==0){
				$stockBalQty=getStockBalance_bulk($location,$itemId);
			}
			else{
				$stockBalQty=getStockBalance_bulk($location,$itemId);
				//$stockBalQty=getStockBalance($company,$location,$orderNoArray[0],$orderNoArray[1],$salesOrderId,$itemId);
			}

			$confirmedGPqty=get_gpQty($location,$orderNoArray[0],$orderNoArray[1],$salesOrderId,$itemId);//but not raised final confirmation

			//------check maximum ISSUE Qty--------------------
			$Qty = round($Qty,4);
			$balQtyToGP = ($stockBalQty-$confirmedGPqty);
			$balQtyToGP = round($balQtyToGP,4);
			if($Qty>$balQtyToGP){
				//	call roll back--------****************
				$rollBackFlag=1;
				if($allocatedFlag==1){
					$rollBackMsg .="<br> ".$orderNoArray[0]."/".$orderNoArray[1]."-".$salesOrderId."-".$itemName." =".$balQtyToGP;
					$msg .=$Qty." =".$balQtyToGP;
				}
				else{
					$rollBackMsg .="<br> ".$itemName." =".$balQtyToGP;
				}
				//exit();
			}

			//----------------------------
			if($rollBackFlag!=1){
				if($orderNoArray[0]==''){
					$orderNoArray[0]=0;
				}
				if($orderNoArray[1]==''){
					$orderNoArray[1]=0;
				}
				$Qty=round($Qty,4);
				$sql = "INSERT INTO `ware_gatepassdetails` (`intGatePassNo`,`intGatePassYear`,`strOrderNo`,`intOrderYear`,`strStyleNo`,`intItemId`,`dblQty`,dblTransferQty) 
					VALUES ('$serialNo','$year','$orderNoArray[0]','$orderNoArray[1]','$salesOrderId','$itemId','$Qty','0')";
				$result3 = $db->RunQuery2($sql);
				if($result3==1){
					$saved++;
				}
			}
			$toSave++;

			//---------------------- budget Validation ----------------------------------------------------
			if($rollBackFlag==0)
			{
				$validateArr	= validateBudgetBalance($serialNo,$year,$gatePassTo,$itemId,$Qty);
				if($validateArr['Flag']==1 && $rollBackFlag==0)
				{
					$rollBackFlag	= 1;
					$rollBackMsg	= $validateArr['msg'];
				}
			}
			//---------------------------------------------------------------------------------------------

		}
	}
	//die($rollBackMsg);
	if($rollBackFlag==1){
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $rollBackMsg;
		$response['q'] 			= $msg;
	}
	else if(($savableFlag==1) &&(!$result)){
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= "Header saving error";
		$response['q'] 			= $sql;
	}
	else if(($savableFlag==1) &&($toSave==0) &&($saved==0)){
		$response['type'] 		= 'fail';
		$response['msg'] 		= "Details saving error";
		$response['q'] 			= '';
	}
	else if(($result) && ($toSave==$saved)){
		$confirmationMode=getConfirmationMode($programCode,$userId,$ApproveLevels,$gatePassApproveLevel);
		$db->RunQuery2('Commit');
		$response['type'] 		= 'pass';
		if($editMode==1)
			$response['msg'] 		= 'Updated successfully.';
		else
			$response['msg'] 		= 'Saved successfully.';

		$response['serialNo'] 		= $serialNo;
		$response['year'] 		= $year;
		$response['confirmationMode'] = $confirmationMode;
	}
	else{
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $db->errormsg;
		$response['q'] 			= $sql;
	}

	$db->CloseConnection();
}
//----------------------------------------
echo json_encode($response);
//-----------------------------------------	
function getNextGatepassNo()
{
	global $db;
	global $companyId;
	$sql = "SELECT
				sys_no.intGatePassNo
				FROM sys_no
				WHERE
				sys_no.intCompanyId =  '$companyId'
				";
	$result = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($result);
	$nextGatePassNo = $row['intGatePassNo'];

	$sql = "UPDATE `sys_no` SET intGatePassNo=intGatePassNo+1 WHERE (`intCompanyId`='$companyId')  ";
	$db->RunQuery2($sql);

	return $nextGatePassNo;
}
//-----------------------------------------------------------
function getStockBalance($company,$location,$orderNo,$orderYear,$salesOrderId,$item)
{
	global $db;
	$sql = "SELECT
				Sum(ware_stocktransactions.dblQty) as stockBal 
				FROM ware_stocktransactions
				WHERE
				ware_stocktransactions.intCompanyId =  '$company' AND
				ware_stocktransactions.intLocationId =  '$location' AND
				ware_stocktransactions.intOrderNo =  '$orderNo' AND
				ware_stocktransactions.intOrderYear =  '$orderYear' AND
				ware_stocktransactions.intSalesOrderId =  '$salesOrderId' AND
				ware_stocktransactions.intItemId =  '$item'";

	$result = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($result);
	return val($row['stockBal']);
}
//--------------------------------------------------------------
function getStockBalance_bulk($location,$item)
{
	global $db;
	$sql = "SELECT
				Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$location'
				GROUP BY
				ware_stocktransactions_bulk.intItemId";

	$result = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($result);
	return val($row['stockBal']);
}
//--------------------------------------------------------------
function getItemName($itemId){
	global $db;
	$sql = "SELECT
					mst_item.strName
					FROM mst_item
					WHERE
					mst_item.intId =  '$itemId'";

	$result = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($result);
	return $row['strName'];
}
//--------------------------------------------------------------
function getSaveStatus($serialNo,$year)
{
	global $db;
	$sql = "SELECT ware_gatepassheader.intStatus, ware_gatepassheader.intApproveLevels FROM ware_gatepassheader WHERE (intIssueNo='$serialNo') AND (`intIssueYear`='$year')";
	$results = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($results);
	$status = $row['intStatus'];

	if(($status==0) || ($row['intStatus']==($row['intApproveLevels']+1))){
		$editableFlag=1	;
	}
	else{
		$editableFlag=0;
	}

	return $editableFlag;
}
//------------------------------------------------------------
function get_gpQty($location,$orderNo,$orderYear,$styleNo,$itemId){
	global $db;
	$sql1 = "SELECT
				Sum(ware_gatepassdetails.dblQty) as gpQty
				FROM
				ware_gatepassheader
				Inner Join ware_gatepassdetails ON ware_gatepassheader.intGatePassNo = ware_gatepassdetails.intGatePassNo AND ware_gatepassheader.intGatePassYear = ware_gatepassdetails.intGatePassYear
				WHERE
				ware_gatepassdetails.strOrderNo =  '$orderNo' AND
				ware_gatepassdetails.intOrderYear =  '$orderYear' AND
				ware_gatepassdetails.strStyleNo =  '$styleNo' AND
				ware_gatepassdetails.intItemId =  '$itemId' AND
				ware_gatepassheader.intStatus >  '1' AND
				ware_gatepassheader.intStatus <= ware_gatepassheader.intApproveLevels AND
				ware_gatepassheader.intCompanyId =  '$location'";
	$result1 = $db->RunQuery2($sql1);
	$row1=mysqli_fetch_array($result1);
	$gpQty=$row1['gpQty'];
	return $gpQty;
}
//-----------------------------------------------------------
function getEditLocationValidation($type,$location,$serialNo,$year)
{
	$flag= 1;

	global $db;
	$sql = "SELECT
					ware_gatepassheader.intCompanyId
					FROM
					ware_gatepassheader
					Inner Join mst_locations ON ware_gatepassheader.intCompanyId = mst_locations.intId
					WHERE
					ware_gatepassheader.intGatePassNo =  '$serialNo' AND
					ware_gatepassheader.intGatePassYear =  '$year'";
	$results = $db->RunQuery2($sql);
	$row = mysqli_fetch_array($results);
	if($location == $row['intCompanyId'])
		$flag=0;

	return $flag;

}
//------------------------------function loadConfirmatonMode-------------------
function getConfirmationMode($programCode,$intUser,$approveLevels,$status){
	global $db;
	$k=$approveLevels+2-$status;
	$sqlp = "SELECT 
		menupermision.intEdit, 
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";

	$resultp = $db->RunQuery2($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	if($rowp['int'.$k.'Approval']==1)
		$confirmatonMode=1;
	else
		$confirmatonMode=0;

	return $confirmatonMode;
}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;

	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";

	$resultp = $db->RunQuery2($sqlp);
	$rowp=mysqli_fetch_array($resultp);

	if($rowp['intEdit']==1){
		if($intStatus==($savedStat+1) || ($intStatus==0)){
			$editMode=1;
		}
	}

	// $editMode;

	return $editMode;
}
//------------------------------function get MaxAppByStatus---------------------
function getMaxAppByStatus($serialNo,$year){
	global $db;

	//echo $savedStat;
	$editMode=0;
	$sql = "SELECT
				max(ware_gatepassheader_approvedby.intStatus) as status 
				FROM
				ware_gatepassheader_approvedby
				WHERE
				ware_gatepassheader_approvedby.intGatePassNo =  '$serialNo' AND
				ware_gatepassheader_approvedby.intYear =  '$year'";

	$resultm = $db->RunQuery2($sql);
	$rowm=mysqli_fetch_array($resultm);

	return $rowm['status'];

}
//--------------------------------------------------------getMaxAppByStatus
function validateBudgetBalance($serialNo,$year,$gatePassTo,$itemId,$Qty)
{
	global $obj_gatepass_get;
	global $obj_comm_budget_get;
	global $company;
	global $location;
	global $obj_common;

	$gp_item_tot_U			= 0;
	$gp_sub_cat_tot_U		= 0;
	$gp_item_tot_A			= 0;
	$gp_sub_cat_tot_A		= 0;

	$saveStatus 			= true;
	$saveQty				= 0;
	$header_array			= $obj_gatepass_get->get_header($serialNo,$year,'RunQuery2');
	$subCatId				= $obj_gatepass_get->getSubCategory($itemId,'RunQuery2');
	$results_fin_year		= $obj_comm_budget_get->loadFinanceYear_with_selected_date($company,date('Y-m-d'),'RunQuery2');
	$row_fin_year			= mysqli_fetch_array($results_fin_year);
	$budg_year				= $row_fin_year['intId'];
	$sub_cat_budget_flag	= $obj_comm_budget_get->check_sub_category_budgeted_or_not($gatePassTo,$subCatId,'RunQuery2');

	$check					= $obj_comm_budget_get->checkBudgetApprovalDepWise($gatePassTo,'',$budg_year,'RunQuery2');

	if(!$check && $saveStatus && $sub_cat_budget_flag!='2')
	{
		$saveStatus			= false;
		$data['Flag']		= 1;
		$data['msg']		= "No approve budget for all department which belong to gate pass to location and current finance year ";
	}

	if($sub_cat_budget_flag==0)
	{
		$budget_type		= $obj_comm_budget_get->load_budget_type($gatePassTo,$subCatId,$itemId,'RunQuery2');
		$budget_bal			= $obj_comm_budget_get->load_budget_balance($gatePassTo,'',$budg_year,'',$itemId,date('m'),'RunQuery2');
		$result_itemPrice	= $obj_comm_budget_get->get_price_for_item($location,$itemId,'RunQuery2');

		while($row = mysqli_fetch_array($result_itemPrice))
		{
			if($row['stockBal']>=$Qty)
			{
				$saveQty 	= $Qty;
				$Qty		= 0;
			}
			else if($Qty>$row['stockBal'])
			{
				$saveQty	= $row['stockBal'];
				$Qty		= $Qty-$saveQty;
			}
			if($saveQty>0)
			{
				if($budget_type == 'U')
					$gp_item_tot_U += $saveQty;
				else if($budget_type == 'A')
					$gp_item_tot_A += $saveQty*$row['price'];

				$budget_bal_A 	= $budget_bal['balAmount'];
				if(($gp_item_tot_A > $budget_bal_A) && $saveStatus)
				{
					$saveStatus		= false;
					$data['Flag']	= 1;
					$data['msg']	= "There is no budget balance for this month for item '".$obj_common->get_item_name($itemId,'RunQuery2')."' - ".$budget_bal_A;
				}
				$budget_bal_U 	= $budget_bal['balUnits'];

				if(($gp_item_tot_U > $budget_bal_U) && $saveStatus)
				{
					$saveStatus		= false;
					$data['Flag']	= 1;
					$data['msg']	= "There is no budget balance for this month for item '".$obj_common->get_item_name($itemId,'RunQuery2')."' - ".$budget_bal_U;
				}
			}
		}

	}
	else if($sub_cat_budget_flag==1)
	{
		$budget_type		= $obj_comm_budget_get->load_budget_type($gatePassTo,$subCatId,0,'RunQuery2');
		$budget_bal			= $obj_comm_budget_get->load_budget_balance($gatePassTo,'',$budg_year,$subCatId,0,date('m'),'RunQuery2');
		$result_itemPrice	= $obj_comm_budget_get->get_price_for_item($location,$itemId,'RunQuery2');

		while($row = mysqli_fetch_array($result_itemPrice))
		{
			if($row['stockBal']>=$Qty)
			{
				$saveQty 	= $Qty;
				$Qty		= 0;
			}
			else if($Qty>$row['stockBal'])
			{
				$saveQty	= $row['stockBal'];
				$Qty		= $Qty-$saveQty;
			}
			if($saveQty>0)
			{
				if($budget_type == 'U')
					$gp_sub_cat_tot_U += $saveQty;
				else if($budget_type == 'A')
					$gp_sub_cat_tot_A += $saveQty*$row['price'];

				$budget_bal_A 	= $budget_bal['balAmount'];
				if(($gp_sub_cat_tot_A > $budget_bal_A) && $saveStatus)
				{
					$saveStatus		= false;
					$data['Flag']	= 1;
					$data['msg']	= "There is no budget balance for this month for item '".$obj_common->get_sub_category_name($itemId,'RunQuery2')."' - ".$budget_bal_A;
				}
				$budget_bal_U 	= $budget_bal['balUnits'];
				if(($gp_sub_cat_tot_U > $budget_bal_U) && $saveStatus)
				{
					$saveStatus		= false;
					$data['Flag']	= 1;
					$data['msg']	= "There is no budget balance for this month for item '".$obj_common->get_item_name($itemId,'RunQuery2')."' - ".$budget_bal_U;
				}
			}

		}
	}
	return $data;
}

    function replace1($str)
{
	  $str 	= 	str_replace('\\', "\\\\",	$str);
	  $str 	= 	str_replace("'","\'",	$str);
	  $str	=	str_replace("\n", "<br>",$str);  
	  
	  return $str;
}	

?>



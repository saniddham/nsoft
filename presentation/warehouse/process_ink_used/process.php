<?php
session_start();
$backwardseperator = "../../../";
$mainPath = $_SESSION['mainPath'];

$thisFilePath =  $_SERVER['PHP_SELF'];
//include  "../reports/{$backwardseperator}dataAccess/permisionCheck.inc";
include  "../reports/{$backwardseperator}dataAccess/Connector.php";
//include 	"../../../class/cls_getData.php";

//BEGIN - CLASS OBJECTS	{
//$obj_getData	= new clsGetData;
//END - CLASS OBJECTS	}

$year			= $_SESSION["Year"];
$month			= $_SESSION["Month"];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Process - Used Ink</title>

<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../reports/reports.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" href="../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../libraries/validate/template.css" type="text/css">
</head>
<body>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
<link rel="stylesheet" type="text/css" href="../../../libraries/calendar/theme.css" />
<script src="../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

<script src="../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.min.js"></script>

<!--<form id="frmPayrollReport" name="frmPayrollReport" autocomplete="off">
--><div align="center">
		<div class="trans_layoutL" style="width:700px">
		  <div class="trans_text">Process - Ink Used</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
          
<!--BEGIN - PO Summery-->
		    <tr>
		      <td><form name="frmStockBalance" id="frmStockBalance" autocomplete="off">
              <table width="700" align="left" border="0" id="tblStockBalanceBody" class="main_table" cellspacing="0" >
		        <thead>
	            </thead>
		        <tbody>
<tr>
  <td width="1%"></td>
  <td width="5%" height="27" class="normalfnt">Date</td>
  <td width="18%"><input name="dtDateFrom7" type="text"  disabled="disabled" class="txtbox" id="dtDateFrom7" style="width:100px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" value="<?php echo(($dtDate)?$dtDate:date('Y-m-d')); ?>"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
  <td width="76%"><input type="text" name="txtNoOfTables" id="txtNoOfTables" value="<?php echo $i?>" style="display:none" />
    <a id="butProcess" class="button green medium" style="" name="butProcess"> Process </a>
  </td>
  </tr>  
        <tr>
          <td height="34" colspan="4"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
            <tr>
              <td width="100%" align="center" bgcolor=""><a id="butClose" class="button white medium" name="butClose" href="../../../../main.php">Close</a></td>
            </tr>
          </table></td>
        </tr>
        	            </tbody>
	          </table></form></td>
	        </tr>
<!--END - Allocated items STOCK BALANCE--> 
    
          </table>
    </div>
  </div>
<!--</form>
-->
</body>
</html>

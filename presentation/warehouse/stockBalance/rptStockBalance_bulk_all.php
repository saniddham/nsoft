<?php
session_start();
$backwardseperator 	= "../../../";
$companyId 			= $_SESSION['CompanyID'];
$locationId 		= $_SESSION['CompanyID'];//this locationId use in report header(reportHeader.php)-------
$intUser  			= $_SESSION["userId"];
$mainPath 			= $_SESSION['mainPath'];
$thisFilePath 		=  $_SERVER['PHP_SELF'];
include  			"{$backwardseperator}dataAccess/Connector.php";

$company 			= $_REQUEST['company'];
//$locationId $_REQUEST['company'];//this locationId use in report header(reportHeader.php)----
//$searchCurrency=2;
$searchCurrency 	= $_REQUEST['currency'];
$location 			= $_REQUEST['location'];
$chkStyle 			= $_REQUEST['chkStyle'];
$reptType 			= $_REQUEST['reptType'];
$orderNo 			= $_REQUEST['orderNo'];
$orderNoArray		=explode("/",$orderNo);
$orderNo			=$orderNoArray[0];
$orderYear			=$orderNoArray[1];
$styleNo 			= $_REQUEST['styleNo'];
$mainCat 			= $_REQUEST['mainCat'];
$subCat 			= $_REQUEST['subCat'];
$item 				= $_REQUEST['item'];
$dateFrom 			= $_REQUEST['dateFrom'];
$dateTo 			= $_REQUEST['dateTo'];

//----------------------------------	
$sql 				= "SELECT
						mst_financecurrency.strCode
						FROM mst_financecurrency
						WHERE
						mst_financecurrency.intId =  '$searchCurrency'";
$result 			= $db->RunQuery($sql);
$row				= mysqli_fetch_array($result);
$curren 			= $row['strCode'];	
//----------------------------------
$sql 				= "SELECT
						mst_companies.intBaseCurrencyId
						FROM mst_companies
						WHERE
						mst_companies.intId =  '$companyId'";
$result 			= $db->RunQuery($sql);
$row				= mysqli_fetch_array($result);
$baseCurrency 		= $row['intBaseCurrencyId'];
//----------------------------------
//get latest exchange rate
$sql 				= "SELECT
						mst_financeexchangerate.dblBuying,
						Max(mst_financeexchangerate.dtmDate)
						FROM mst_financeexchangerate
						WHERE
						mst_financeexchangerate.intCurrencyId =  '$searchCurrency' 
						GROUP BY
						mst_financeexchangerate.dblBuying";
$result 			= $db->RunQuery($sql);
$row				= mysqli_fetch_array($result);
$latestExchRate 	= $row['dblBuying'];
//----------------------------------
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Stock Balance Report</title>
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />


<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../mrn/listing/rptMrn-js.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/script.js"></script>

<script src="../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.min.js"></script>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:264px;
	top:247px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<form id="frmMrnReport" name="frmMrnReport" method="post" action="">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include "reportHeader.php"?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>STOCK BALANCE <?php if($reptType==2){ ?> VALUE <?php } ?>REPORT</strong><strong></strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
  <?php
  if($curren!=''){
  ?>
  <tr>
    <td width="1%">&nbsp;</td>
    <td width="9%" align="right" class="normalfnt"><strong style="text-align:right">Currency</strong></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="58%"><span class="normalfnt" align="left" style="text-align:left"><?php echo $curren  ?></span></td>
    <td width="6%">&nbsp;</td>
    <td width="6%" align="center" valign="middle">&nbsp;</td>
    <td width="6%">&nbsp;</td>
    <td width="6%" class="normalfnt">&nbsp;</td>
    <td width="7%">&nbsp;</td>
  </tr>
  <?php
  }
  ?>
  <?php
  if(($dateFrom!='') ||($dateTo!='')) {
  ?>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Date Range</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $dateFrom."  To  ".$dateTo  ?></span></td>
    <td>&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <?php
  }
  ?>
  
  </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td width="5%">&nbsp;</td>
        <td colspan="7" class="normalfnt">
           <table width="100%"  class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <thead>
              <tr class=""  bgcolor="#dce9f9">
                <th colspan="10" >&nbsp;</th>
           <?php
		   $result_loc	= locations_result($location,$company);
 			while($row_loc=mysqli_fetch_array($result_loc)){
				$company_id			= $row_loc['company_id'];
				$location_id		= $row_loc['location_id'];
				$company_name		= $row_loc['company_name'];
				$location_name		= $row_loc['location_name'];
		   ?>
                <th colspan="2" align="left" ><?php echo $company_name."  :  ".$location_name; ?></th>
           <?php
			}
 		   ?>
                <th >&nbsp;</th>
                <th >&nbsp;</th>
                </tr>
              
              <tr class=""  bgcolor="#dce9f9">
              <th width="15%" >Main Category</th>
              <th width="15%" >Sub Category</th>
              <th width="9%" >Finance Type</th>
              <th width="9%" >Main Type</th>
              <th width="9%" >Sub Type </th>
              <th width="9%" >Chart Of Account</th>
              <th width="9%" >Depreciation Rate</th>
              <th width="9%" >Item Code</th>
              <th width="20%" >Item</th>
              <th width="5%">UOM</th>
           <?php
		   $result_loc2	= locations_result($location,$company);
 			while($row_loc2=mysqli_fetch_array($result_loc2)){
				$company_id			= $row_loc3['company_id'];
				$location_id		= $row_loc3['location_id'];
		   ?>
              <th width="7%" >Qty</th>
              <th width="7%" >Value(<?php echo $curren;  ?>)</th>
           <?php
			}
		   ?>
              <th width="8%" >Tot Qty</th>
              <th width="8%" >Tot Value</th>
              </tr>
              <?php 
 	  	   $sql1 = "select 
					tb1.mainCategory,
					tb1.subCategoryId,
					tb1.subCategory,
					tb1.dblDepreciationRate,
					tb1.strCode,
                                        tb1.SUP_ITEM_CODE,
					tb1.intItemId,
					tb1.itemName,
					tb1.UOM 
					from(SELECT 
					mst_maincategory.strName as mainCategory,
					mst_subcategory.strName as subCategory,
					mst_subcategory.intId as subCategoryId,
					mst_subcategory.dblDepreciationRate,
					mst_item.strCode,
                    mst_item.strCode as SUP_ITEM_CODE,
					mst_item.intId as intItemId, 
					mst_item.strName as itemName,
					mst_units.strCode as UOM 
					
					FROM
					mst_item 
					Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
					Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
					Inner Join mst_units ON mst_item.intUOM = mst_units.intId
                                        LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
					WHERE
					1=1  ";
			if($subCat!='')		
			$sql1 .="AND mst_item.intSubCategory IN  ($subCat)  " ;
			if($item!='')		
			$sql1 .="AND mst_item.intId =  '$item' " ;
			
			$sql1 .=" ) as tb1 ";
					  
 			$sql1 .="GROUP BY
					tb1.intItemId
					ORDER BY
					tb1.itemName ASC,
					tb1.mainCategory ASC,
					tb1.subCategory ASC";
 	     
			$result1 = $db->RunQuery($sql1);
			while($row=mysqli_fetch_array($result1))
			{
				$rowCOA			= NULL;
				$response 		= get_finance_details($row['subCategoryId']);
	  ?>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfntleft" >&nbsp;<?php echo $row['mainCategory']?>&nbsp;</td>
              <td class="normalfntleft" nowrap="nowrap" >&nbsp;<?php echo $row['subCategory'] ?>&nbsp;</td>
              <td class="normalfntleft" nowrap="nowrap" ><?php echo $response['financeType'] ?></td>
              <td class="normalfntleft" nowrap="nowrap" ><?php echo $response['mainType'] ?></td>
              <td class="normalfntleft" nowrap="nowrap" ><?php echo $response['financeType'] ?></td>
              <td class="normalfntleft" nowrap="nowrap" ><?php echo $response['chartOfAccId'] ?></td>
              <td class="normalfntRight" nowrap="nowrap" ><?php echo $row['dblDepreciationRate'] ?></td>
              <?php
              if($row['SUP_ITEM_CODE']!=null)
              {
              ?>
               <td class="normalfntleft" nowrap="nowrap" >&nbsp;<?php echo $row['SUP_ITEM_CODE'] ?>&nbsp;</td>
              <?php
              }else{
               ?>
              <td class="normalfntleft" nowrap="nowrap" >&nbsp;<?php echo $row['strCode'] ?>&nbsp;</td>
              <?php
              }
              ?>
              <td class="normalfntleft" nowrap="nowrap" >&nbsp;<?php echo $row['itemName'] ?>&nbsp;</td>
              <td class="normalfntRight">&nbsp;<?php echo $row['UOM'] ?>&nbsp;</td>
			   <?php
				$tot_qty	=0;
				$tot_val	=0;

				$result_loc3	= locations_result($location,$company);
				while($row_loc3=mysqli_fetch_array($result_loc3)){
				
				$company_id			= $row_loc3['company_id'];
				$location_id		= $row_loc3['location_id'];
				$qty_arr			= get_location_qty_and_value($company_id,$location_id,$row['intItemId'],$dateFrom,$dateTo,$searchCurrency,$latestExchRate);
				if($searchCurrency==$baseCurrency){	//if filter from base currency  
					$value=$qty_arr['Val'];
				}
				else{
					$value=$qty_arr['Val1'];
				}
			  
			   ?>
              <td class="normalfntRight">&nbsp;<?php echo number_format($qty_arr['Qty'],4) ?></td>
              <td class="normalfntRight">&nbsp;<?php echo number_format($value,4) ?></td>
             	<?php
				$tot_qty	+=round($qty_arr['Qty'],4);
				$tot_val	+=round($value,4);
				
				}
				?>
              <td class="normalfntRight">&nbsp;<?php echo number_format(round($tot_qty,4),4) ?></td>
              <td class="normalfntRight">&nbsp;<?php echo number_format(round($tot_val,4),4) ?></td>
              </tr>
      <?php 
			$totQty+=$row['Qty'];
			$totVal+=$row['Val'];
			}
	  ?>
            </table>
          </td>
        <td width="6%">&nbsp;</td>
        </tr>
      
      </table>
    </td>
</tr>

<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
</body>
</html>

<?php
function locations_result($location,$company){
	global $db;
	$sql_loc		="SELECT
						mst_companies.strName as company_name ,
						mst_locations.strName as location_name,
						mst_locations.intCompanyId as company_id,
						mst_locations.intId			as location_id 
						FROM
						mst_locations
						INNER JOIN mst_companies ON mst_locations.intCompanyId = mst_companies.intId
						WHERE 
						1=1  ";
	if($location !='') 
	$sql_loc		.=" AND mst_locations.intId = '$location' ";
	if($company !='')
	$sql_loc		.=" AND mst_locations.intCompanyId = '$company'";
	
	$sql_loc		.=" AND mst_companies.intStatus = 1 
						AND mst_locations.intStatus = 1
						ORDER BY
						mst_companies.strName ASC,
						mst_locations.strName ASC
						";
	$result_loc 	= $db->RunQuery($sql_loc);
	
	return $result_loc;
	
}

function get_location_qty_and_value($company,$location,$item,$dateFrom,$dateTo,$searchCurrency,$latestExchRate){
	
	global $db;
	
	  	   $sql1 = "select 
					tb1.dtmDate, 
					tb1.dblBuying as poCurrencyRate , 
					tb1.mainCategory,
					tb1.subCategory,
					tb1.strCode,
					tb1.intItemId,
					tb1.itemName,
					tb1.UOM,
					Sum(tb1.Qty) as Qty, 
					sum(((tb1.value*tb1.dblBuying)/$latestExchRate)) as Val1, 
					sum(((tb1.value*tb1.dblBuying)/mst_financeexchangerate.dblBuying)) as Val, 
					mst_financeexchangerate.dblBuying as searchCurrencyRate 
					from(SELECT 
					ware_grnheader.intCompanyId,  
					mst_maincategory.strName as mainCategory,
					mst_subcategory.strName as subCategory,
					mst_item.strCode,
					ware_stocktransactions_bulk.intItemId, 
					mst_item.strName as itemName,
					mst_units.strCode as UOM,
					mst_financeexchangerate.dblBuying, 
					mst_financeexchangerate.dtmDate, 
					(ware_stocktransactions_bulk.dblQty*dblGRNRate) AS value, 
					(ware_stocktransactions_bulk.dblQty) AS Qty
					FROM
					ware_stocktransactions_bulk
					Inner Join ware_grnheader ON ware_stocktransactions_bulk.intGRNNo = ware_grnheader.intGrnNo AND ware_stocktransactions_bulk.intGRNYear = ware_grnheader.intGrnYear
					Inner Join trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
					Inner Join mst_locations ON ware_grnheader.intCompanyId = mst_locations.intId 
					left Join mst_financeexchangerate ON trn_poheader.intCurrency = mst_financeexchangerate.intCurrencyId AND trn_poheader.dtmPODate = mst_financeexchangerate.dtmDate AND mst_locations.intCompanyId = mst_financeexchangerate.intCompanyId 
					Inner Join mst_item ON ware_stocktransactions_bulk.intItemId = mst_item.intId
					Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
					Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
					Inner Join mst_units ON mst_item.intUOM = mst_units.intId  
					WHERE
					ware_stocktransactions_bulk.intItemId !=  '*'  ";
			if($company!='')		
			$sql1 .="AND ware_stocktransactions_bulk.intCompanyId =  '$company'  " ;
			if($location!='')		
			$sql1 .="AND ware_stocktransactions_bulk.intLocationId =  '$location' " ;
/*			if($mainCat!='')		
			$sql1 .="AND mst_item.intMainCategory =  '$mainCat'  " ;
*/			if($item!='')		
			$sql1 .="AND mst_item.intId =  '$item' " ;
			if($dateFrom!='')		
			$sql1 .="AND ware_stocktransactions_bulk.dtGRNDate >=  '$dateFrom' " ;
			if($dateTo!='')		
			$sql1 .="AND ware_stocktransactions_bulk.dtGRNDate <=  '$dateTo' " ;
			
			$sql1 .=" ) as tb1 ";
					  
			$sql1 .="Inner Join mst_locations ON tb1.intCompanyId = mst_locations.intId 
			        left Join mst_financeexchangerate ON tb1.dtmDate   = mst_financeexchangerate.dtmDate  AND mst_locations.intCompanyId = mst_financeexchangerate.intCompanyId 
AND  mst_financeexchangerate.intCurrencyId='$searchCurrency' ";

			$sql1 .="GROUP BY
					tb1.intItemId
					ORDER BY
					tb1.itemName ASC,
					tb1.mainCategory ASC,
					tb1.subCategory ASC";
					
					//echo $sql1;
					//die();
	$result_loc 	= $db->RunQuery($sql1);
	$row_loc		=mysqli_fetch_array($result_loc);
	
	return $row_loc;
	
	
}

function get_finance_details($id){
	
	$response['ledger']					= false;
	$result = GetCOA($id,'CHART_OF_ACCOUNT_ID');
	while($rowCOA = mysqli_fetch_array($result))
	{
		$response['chartOfAccId'] 		= load_COA_desc($rowCOA['CHART_OF_ACCOUNT_ID']);
		$response['subType'] 			= load_ST_desc($rowCOA['SUB_TYPE_ID']);
		$response['mainType'] 			= load_MT_desc($rowCOA['MAIN_TYPE_ID']);
		$response['financeType'] 		= load_FT_desc($rowCOA['FINANCE_TYPE_ID']);
		$response['ledger']				= true;
	}
	
	$response['ledger_dr']				= false;
	$result = GetCOA($id,'DEPRECIATION_DEBIT_ID');
	while($rowCOA = mysqli_fetch_array($result))
	{
		$response['chartOfAccId'] 	= load_COA_desc($rowCOA['CHART_OF_ACCOUNT_ID']);
		$response['subType'] 		= load_ST_desc($rowCOA['SUB_TYPE_ID']);
		$response['mainType'] 		= load_MT_desc($rowCOA['MAIN_TYPE_ID']);
		$response['financeType'] 	= load_FT_desc($rowCOA['FINANCE_TYPE_ID']);
		$response['ledger']			= true;
	}
	
	$response['ledger_cr']			= false;
	$result = GetCOA($id,'DEPRECIATION_CREDIT_ID');
	while($rowCOA = mysqli_fetch_array($result))
	{
		$response['chartOfAccId'] 	= load_COA_desc($rowCOA['CHART_OF_ACCOUNT_ID']);
		$response['subType'] 		= load_ST_desc($rowCOA['SUB_TYPE_ID']);
		$response['mainType'] 		= load_MT_desc($rowCOA['MAIN_TYPE_ID']);
		$response['financeType'] 	= load_FT_desc($rowCOA['FINANCE_TYPE_ID']);
		$response['ledger']			= true;
	}
 	return ($response);
}


function GetCOA($id,$coa)
{
	global $db;
	
	$sql = "SELECT 
				COA.CHART_OF_ACCOUNT_ID,
				COA.SUB_TYPE_ID,
				MT.MAIN_TYPE_ID,
				FT.FINANCE_TYPE_ID 
			FROM finance_mst_chartofaccount COA
			INNER JOIN  finance_mst_chartofaccount_subcategory COASC ON COASC.$coa = COA.CHART_OF_ACCOUNT_ID
			INNER JOIN finance_mst_account_sub_type ST ON ST.SUB_TYPE_ID = COA.SUB_TYPE_ID
			INNER JOIN finance_mst_account_main_type MT ON MT.MAIN_TYPE_ID = ST.MAIN_TYPE_ID
			INNER JOIN finance_mst_account_main_type FT ON FT.FINANCE_TYPE_ID = MT.FINANCE_TYPE_ID 
			INNER JOIN finance_mst_account_type FMAT ON FMAT.FINANCE_TYPE_ID = MT.FINANCE_TYPE_ID 
			WHERE COASC.SUBCATEGORY_ID = '$id'
			GROUP BY COA.CHART_OF_ACCOUNT_ID";
			//echo $sql;
	return $db->RunQuery($sql);
}

function load_COA_desc($coa){
global $db;
	$sql	= "SELECT CHART_OF_ACCOUNT_ID,CHART_OF_ACCOUNT_NAME
				FROM finance_mst_chartofaccount
				WHERE CHART_OF_ACCOUNT_ID = '$coa'
				ORDER BY CHART_OF_ACCOUNT_NAME 
";
	$result = $db->RunQuery($sql);
	$row=mysqli_fetch_array($result);
	return $row['CHART_OF_ACCOUNT_NAME'];

	
}
function load_ST_desc($st){
global $db;
	$sql = "SELECT SUB_TYPE_ID,SUB_TYPE_NAME
				FROM finance_mst_account_sub_type
				WHERE SUB_TYPE_ID='$st'
				ORDER BY SUB_TYPE_NAME ";

	$result = $db->RunQuery($sql);
	$row=mysqli_fetch_array($result);
	return $row['SUB_TYPE_NAME'];
	
}
function load_MT_desc($mt){
global $db;
	$sql = "SELECT MAIN_TYPE_ID,MAIN_TYPE_NAME
				FROM finance_mst_account_main_type
				WHERE MAIN_TYPE_ID='$mt' AND STATUS=1
				ORDER BY MAIN_TYPE_NAME ";

	$result = $db->RunQuery($sql);
	$row=mysqli_fetch_array($result);
	return $row['MAIN_TYPE_NAME'];
	
}
function load_FT_desc($ft){
global $db;
$sql = "SELECT FINANCE_TYPE_ID,FINANCE_TYPE_NAME
				FROM finance_mst_account_type
				WHERE FINANCE_TYPE_ID='$ft'
				ORDER BY FINANCE_TYPE_NAME ";
				
	$result = $db->RunQuery($sql);
	$row=mysqli_fetch_array($result);
	return $row['FINANCE_TYPE_NAME'];
	
}

	


?>
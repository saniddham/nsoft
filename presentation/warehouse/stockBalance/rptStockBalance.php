<?php
session_start();
 $companyId = $_SESSION['CompanyID'];
$locationId 	= $_SESSION['CompanyID'];//this locationId use in report header(reportHeader.php)-------
$intUser  = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
 
$company = $_REQUEST['company'];
//$locationId $_REQUEST['company'];//this locationId use in report header(reportHeader.php)----
//$searchCurrency=2;
$searchCurrency = $_REQUEST['currency'];
$location = $_REQUEST['location'];
$chkStyle = $_REQUEST['chkStyle'];
$reptType = $_REQUEST['reptType'];
$orderNo = $_REQUEST['orderNo'];
$orderNoArray=explode("/",$orderNo);
$orderNo=$orderNoArray[0];
$orderYear=$orderNoArray[1];
$styleNo = $_REQUEST['styleNo'];
$mainCat = $_REQUEST['mainCat'];
$subCat = $_REQUEST['subCat'];
$item = $_REQUEST['item'];
$dateTo = $_REQUEST['dateTo'];
$itemStatus = $_REQUEST['itemType'];
 $sql = "SELECT
mst_companies.strName
FROM mst_companies
WHERE
mst_companies.intId =  '$company'
";
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
$comp = $row['strName'];
//----------------------------------	
 $sql = "SELECT
mst_financecurrency.strCode
FROM mst_financecurrency
WHERE
mst_financecurrency.intId =  '$searchCurrency'";
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
$curren = $row['strCode'];	
//----------------------------------
 $sql = "SELECT
mst_locations.strName
FROM
mst_locations
WHERE
mst_locations.intId =  '$location'";
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
$loca = $row['strName'];
//----------------------------------
 $sql = "SELECT
mst_companies.intBaseCurrencyId
FROM mst_companies
WHERE
mst_companies.intId =  '$companyId'";
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
$baseCurrency = $row['intBaseCurrencyId'];
//----------------------------------
//get latest exchange rate
 $sql = "SELECT
mst_financeexchangerate.dblBuying,
Max(mst_financeexchangerate.dtmDate)
FROM mst_financeexchangerate
WHERE
mst_financeexchangerate.intCurrencyId =  '$searchCurrency' 
GROUP BY
mst_financeexchangerate.dblBuying";
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
$latestExchRate = $row['dblBuying'];
//----------------------------------
?>
 <head>
 <title>Stock Balance Report</title>
 <style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:264px;
	top:247px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<form id="frmMrnReport" name="frmMrnReport" method="post" >
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include 'reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>BULK STOCK BALANCE <?php if($reptType==2){ ?> VALUE <?php } ?>REPORT</strong><strong></strong></div>
<table width="868" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td width="905">
  <table width="100%">
  <?php
  if($comp!=''){
  ?>
  <tr>
    <td width="9%"></td>
    <td width="9%" class="normalfnt" align="right"><strong style="text-align:right">Company</strong></td>
    <td width="2%" align="center" valign="middle"><strong>:</strong></td>
    <td width="49%" align="left"><span class="normalfnt" style="text-align:left"><?php echo $comp  ?></span></td>
    <td width="6%"></td>
    <td width="6%" align="center" valign="middle"></td>
    <td width="6%"></td>
    <td width="6%" class="normalfnt"></td>
    <td width="7%"></td>
  </tr>
  <?php
  }
  ?>
  <?php
  if($curren!=''){
  ?>
  <tr>
    <td></td>
    <td class="normalfnt" align="right"><strong style="text-align:right">Currency</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt" align="left" style="text-align:left"><?php echo $curren  ?></span></td>
    <td></td>
    <td align="center" valign="middle"></td>
    <td></td>
    <td class="normalfnt"></td>
    <td></td>
  </tr>
  <?php
  }
  ?>
  <?php
  if($loca!=''){
  ?>
  <tr>
    <td></td>
    <td class="normalfnt"><strong>Location</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $loca  ?></span></td>
    <td></td>
    <td align="center" valign="middle"></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <?php
  }
  ?>
  
  <?php
  if($dateTo!='') {
  ?>
  <tr>
    <td></td>
    <td class="normalfnt"><strong>Stock Balance To Date </strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $dateTo  ?></span></td>
    <td></td>
    <td align="center" valign="middle"></td>
    <td></td>
    <td></td>
    <td></td>
  </tr>
  <?php
  }
  ?>
  
  </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td colspan="7" class="normalfnt">
          <table width="100%" class="grid" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr class="gridHeader">
              <td width="13%" >Company</td>
              <td width="13%" >Location</td>
              <td width="13%" >Main Category</td>
              <td width="13%" >Sub Category</td>
              <td width="13%" >Item Code</td>
              <td width="20%" >Item</td>
               <td width="8%" >Item Type</td>
              <td width="7%" <?php if($reptType=='1'){ ?> style="display:"<?php } ?>>UOM</td>
              <td width="12%" <?php if($reptType=='1'){ ?> style="display:none"<?php } ?> >Unit Price(<?php echo $curren;  ?>)</td>
              <td width="9%" >Qty</td>
              <td width="9%" >Rolling Qty</td>
              <td width="9%" <?php if($reptType=='1'){ ?> style="display:none"<?php } ?> >Value(<?php echo $curren;  ?>)</td>
              </tr>
            <?php 
			
			$sql = "SELECT
						mst_financeexchangerate.dblBuying
					FROM
						mst_financeexchangerate
					WHERE
						mst_financeexchangerate.intCurrencyId =  '$searchCurrency' AND
						mst_financeexchangerate.intCompanyId =  '$company'
					ORDER BY
						mst_financeexchangerate.dtmDate DESC
					LIMIT 1
					";
			$result = $db->RunQuery($sql);
			$row=mysqli_fetch_array($result);
			$toCurrencyRate = $row['dblBuying'];
			if($toCurrencyRate<=0)
				$toCurrencyRate=1;
			
					if($company!='')		
							$para .="AND ware_stocktransactions_bulk.intCompanyId =  '$company'  " ;
						if($location!='')		
							$para .="AND ware_stocktransactions_bulk.intLocationId =  '$location' " ;	
						if($subCat!='')		
							$para .="AND mst_item.intSubCategory IN  ($subCat)  " ;
						if($item!='')		
							$para .="AND ware_stocktransactions_bulk.intItemId =  '$item' " ;
						if($dateTo!='')		
							$para .="AND date(ware_stocktransactions_bulk.dtDate) <=  '$dateTo' " ;
						if ($itemStatus  == 1 || $itemStatus == 0)
                            $para .="AND mst_item.intStatus = '$itemStatus'  " ;
			
	 			$sql = "SELECT
							mst_maincategory.strName AS mainName,
							mst_subcategory.strName AS subName,
							mst_item.strCode AS itemCode,
                            mst_item.strCode as SUP_ITEM_CODE,
							mst_item.intSpecialRm AS itemType,
							mst_item.strName AS itemName,
                                                        mst_item.ITEM_HIDE,
							round(Sum(ware_stocktransactions_bulk.dblQty),4) AS balQty,
							mst_units.strName AS unitName,
							ware_stocktransactions_bulk.intCurrencyId,
							round(  sum( (ware_grnheader.dblExchangeRate *ware_stocktransactions_bulk.dblQty * ware_stocktransactions_bulk.dblGRNRate)/$toCurrencyRate),4   ) AS value,
							mst_locations.strName as location,
							mst_companies.strName as company,
							(SELECT qty FROM `item_rolling_stock` as rs
							WHERE rs.`year` = year(now()) 
							AND rs.`intItemId` = mst_item.intId 
							AND rs.`locationId` = ware_stocktransactions_bulk.intCompanyId) as rollingQty 
						FROM
							ware_stocktransactions_bulk
							Inner Join mst_item ON mst_item.intId = ware_stocktransactions_bulk.intItemId
							Inner Join mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
							Inner Join mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
							Inner Join mst_units ON mst_units.intId = mst_item.intUOM
							left Join ware_grnheader ON ware_grnheader.intGrnNo = ware_stocktransactions_bulk.intGRNNo AND ware_grnheader.intGrnYear = ware_stocktransactions_bulk.intGRNYear 
							INNER JOIN mst_locations ON ware_stocktransactions_bulk.intLocationId = mst_locations.intId
							INNER JOIN mst_companies ON mst_locations.intCompanyId = mst_companies.intId
                                                        LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
                                                where 1=1 
							$para
						GROUP BY
							ware_stocktransactions_bulk.intLocationId,
							ware_stocktransactions_bulk.intItemId
							ORDER BY
							ware_stocktransactions_bulk.intLocationId asc, 
							mst_maincategory.intId ASC,
							subName ASC,
							itemName ASC
						";
                                

			 if($intUser==2 && $item == 1306)
				 echo $sql;

			$result = $db->RunQuery($sql);
			$totQty=0;
			$totAmmount=0;
			while($row=mysqli_fetch_array($result))
			{
			    $itemType = $row['itemType']==1?"Special RM":"General Item";
				$bgCol="#FFFFFF";
				if(round($row['balQty'],4)< round($row['rollingQty'],4))
				$bgCol="#C88182";
	  ?>
            <tr class="normalfnt"  bgcolor="<?php echo $bgCol; ?>">
              <td class="normalfnt" nowrap="nowrap" ><?php echo $row['company']?></td>
              <td class="normalfnt" nowrap="nowrap" ><?php echo $row['location']?></td>
              <td class="normalfnt" nowrap="nowrap" ><?php echo $row['mainName']?></td>
              <td class="normalfnt" nowrap="nowrap" ><?php echo $row['subName'] ?></td>
              <?php
              if($row['SUP_ITEM_CODE']!=null)
              {
              ?>
               <td class="normalfnt" nowrap="nowrap" ><?php echo $row['SUP_ITEM_CODE'] ?></td>
              <?php
              }else{
              ?>    
              <td class="normalfnt" nowrap="nowrap" ><?php echo $row['itemCode'] ?></td>
              <?php
              }
              ?>
              <td class="normalfnt" nowrap="nowrap" ><?php if($row['ITEM_HIDE']==1) echo "*****"; else echo $row['itemName'] ?></td>
              <td class="normalfnt" nowrap="nowrap" ><?php echo $itemType;?></td>
              <td class="normalfntRight" <?php if($reptType=='1'){ ?> style="display:"<?php } ?> ><?php echo $row['unitName'] ?></td>
              <td class="normalfntRight" <?php if($reptType=='1'){ ?> style="display:none"<?php } ?> ><?php echo number_format($row['value']/$row['balQty'],4) ?></td>
              <td class="normalfntRight" ><?php echo number_format($row['balQty'],4) ?></td>
              <td class="normalfntRight" ><?php echo number_format($row['rollingQty'],4) ?></td>
              <td class="normalfntRight" <?php if($reptType=='1'){ ?> style="display:none"<?php } ?> ><?php echo number_format($row['value'],4) ?></td>
              </tr>
            <?php 
			$totQty+=$row['balQty'];
			$totVal+=$row['value'];
			}
	  ?>
            <tr class="normalfnt"  bgcolor="#CCCCCC">
              <td class="normalfnt" ></td>
              <td class="normalfnt" ></td>
              <td class="normalfnt" ></td>
              <td class="normalfnt" ></td>
              <td class="normalfnt" ></td>
              <td class="normalfnt" ></td>
              <td class="normalfntRight" ></td>
              <td class="normalfntRight" <?php if($reptType=='1'){ ?> style="display:none"<?php } ?> ></td>
              <td class="normalfntRight" <?php if($reptType=='1'){ ?> style="display:none"<?php } ?> ><?php echo number_format($totQty,2) ?></td>
              <td class="normalfntRight" <?php if($reptType=='1'){ ?> style="display:none"<?php } ?> >&nbsp;</td>
              <td class="normalfntRight" <?php if($reptType=='1'){ ?> style="display:none"<?php } ?> ><?php echo number_format($totVal,4) ?></td>
              </tr>
            </table>
        </td>
        </tr>
      
      </table>
    </td>
</tr>

<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
</body>
</html>
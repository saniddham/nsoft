<?php
	session_start();
	$backwardseperator = "../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	///
	/////////// type of print load part /////////////////////
	if($requestType=='loadLocations')
	{
		$company  = $_REQUEST['company'];
		$sql = "SELECT
				mst_locations.intId,
				mst_locations.strName
				FROM mst_locations
				WHERE
				mst_locations.intCompanyId =  '$company'";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	else if($requestType=='loadStyleNo')
	{
		$orderNo  = $_REQUEST['orderNo'];
		$orderNoArray 	 = explode('/',$orderNo);
		 $sql = "SELECT DISTINCT
				trn_materialratio.strStyleNo
				FROM trn_materialratio
				WHERE
				trn_materialratio.intOrderNo =  '$orderNoArray[0]' AND
				trn_materialratio.intOrderYear =  '$orderNoArray[1]'";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['strStyleNo']."\">".$row['strStyleNo']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	else if($requestType=='loadSubCategory')
	{
		$mainCategory  = $_REQUEST['mainCategory'];
		$sql = "SELECT
				mst_subcategory.intId,
				mst_subcategory.strCode,
				mst_subcategory.strName
				FROM mst_subcategory
				WHERE
				mst_subcategory.intMainCategory =  '$mainCategory'";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	else if($requestType=='loadItems')
	{
		 $subCategoriesString 		= $_REQUEST['string'];
	
		 $sql = "SELECT
				mst_item.intId,
				mst_item.strName
				FROM mst_item
				WHERE
				mst_item.intSubCategory  IN (".$subCategoriesString.") order by strName ASC";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	else if($requestType=='loadCurrency')
	{
		$company  = $_REQUEST['company'];
		$sql = "SELECT
				mst_companies.intBaseCurrencyId
				FROM mst_companies
				WHERE
				mst_companies.intId =  '$company'";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
			$currency = $row['intBaseCurrencyId'];
		echo $currency;
		//echo json_encode($response);
	}
?>	
	

			
$(document).ready(function() {
	
  		$("#frmStockBalance").validationEngine();
		$('#frmStockBalance #cboSupplier').focus();
		
		$("#butSearch").click(function(){
			loadHeaderData();
		});
		
		$("#butAddItems").click(function(){
			if($('#cboDepartment').val()==''){
				alert("Please select the deparment");
				return false;
			}
			//clearRows();
			loadPopup();

		});
		
	//--------------------------------------------
		
  //permision for add 
  if(intAddx)
  {
 	$('#frmStockBalance #butNew').show();
	$('#frmStockBalance #butSave').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmStockBalance #butSave').show();
	//$('#frmStockBalance #cboSearch').removeAttr('disabled');// to enable $('#cboSearch').attr('disabled');
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmStockBalance #butDelete').show();
	//$('#frmStockBalance #cboSearch').removeAttr('disabled');
  }
  
  //permision for view
  if(intViewx)
  {
	//$('#frmStockBalance #cboSearch').removeAttr('disabled');
  }
  

  //-------------------------------------------------------
 
  $('#frmStockBalance #butSave').click(function(){
	var requestType = '';
	if ($('#frmStockBalance').validationEngine('validate'))   
    { 

		var data = "requestType=save";
			data+="&serialNo="		+	$('#txtMrnNo').val();
			data+="&Year="	+	$('#txtYear').val();
			data+="&department="		+	$('#cboDepartment').val();
			data+="&date="			+	$('#dtDate').val();


			var rowCount = document.getElementById('tblMrn').rows.length;
			if(rowCount==1){
				alert("items not selected to MRN");
				return false;				
			}
			var row = 0;
			
			var arr="[";
			
			for(var i=1;i<rowCount;i++)
			{
					 arr += "{";
					var orderNo = 	document.getElementById('tblMrn').rows[i].cells[1].innerHTML;
					var styleNo = 	document.getElementById('tblMrn').rows[i].cells[2].innerHTML;
					//var delivNo = 	document.getElementById('tblMrn').rows[i].cells[3].innerHTML;
					var itemId = 	document.getElementById('tblMrn').rows[i].cells[5].id;
					var Qty = 	document.getElementById('tblMrn').rows[i].cells[6].childNodes[0].value;
					
					if(Qty>0){
						arr += '"orderNo":"'+		orderNo +'",' ;
						arr += '"styleNo":"'+		styleNo +'",' ;
						arr += '"itemId":"'+		itemId +'",' ;
						arr += '"Qty":"'+		Qty  +'"' ;
						arr +=  '},';
						
					}
			}
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;

		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = "mrn-db-set.php";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmStockBalance #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						//$('#frmSampleInfomations').get(0).reset();
						//saveImage(json.sampleNo,json.sampleYear,json.revisionNo);
						//loadCombo_frmSampleInfomations();
						var t=setTimeout("alertx()",1000);
						$('#txtMrnNo').val(json.serialNo);
						$('#txtYear').val(json.year);
						return;
					}
				},
			error:function(xhr,status){
					
					$('#frmStockBalance #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
						window.opener.location.reload();//reload listing page
						window.opener.opener.location.reload();//reload listing page
   });
   
  //-------------------------------------------------------
 
  $('#frmStockBalanceClearance #butClear').click(function(){
	if ($('#frmStockBalanceClearance').validationEngine('validate'))   
    { 
		var requestType = '';
		var data = "requestType=clear";
			data+="&serialNo="		+	$('#cboMrnNo').val();
			data+="&Year="	+	$('#cboMrnYear').val();


			var rowCount = document.getElementById('tblMrn').rows.length;
			if(rowCount==1){
				alert("No items to clear");
				return false;				
			}
			var row = 0;
			
			var arr="[";
			
			for(var i=1;i<rowCount;i++)
			{
					 arr += "{";
					var orderNo = 	document.getElementById('tblMrn').rows[i].cells[1].innerHTML;
					var styleNo = 	document.getElementById('tblMrn').rows[i].cells[2].innerHTML;
					var itemId = 	document.getElementById('tblMrn').rows[i].cells[5].id;
					var mrnQty = 	parseFloat(document.getElementById('tblMrn').rows[i].cells[6].innerHTML)-parseFloat(document.getElementById('tblMrn').rows[i].cells[8].childNodes[0].value);
					var clearQty = 	parseFloat(document.getElementById('tblMrn').rows[i].cells[8].childNodes[0].value);
					
				//	if(Qty>0){
						arr += '"orderNo":"'+		orderNo +'",' ;
						arr += '"styleNo":"'+		styleNo +'",' ;
						arr += '"itemId":"'+		itemId +'",' ;
						arr += '"mrnQty":"'+		mrnQty +'",' ;
						arr += '"clearQty":"'+		clearQty  +'"' ;
						arr +=  '},';
						
				//	}
			}
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;

		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = "mrn-db-set.php";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmStockBalanceClearance #butClear').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
				},
			error:function(xhr,status){
					
					$('#frmStockBalanceClearance #butClear').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
				}		
			});
	}
   });
   
	
	//--------------refresh the form----------
	$('#frmStockBalance #butNew').click(function(){
		$('#frmStockBalance').get(0).reset();
		clearRows();
		$('#frmStockBalance #txtMrnNo').val('');
		$('#frmStockBalance #txtYear').val('');
		$('#frmStockBalance #cboDepartment').val('');
		$('#frmStockBalance #cboDepartment').focus();
		
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmStockBalance #dtDate').val(d);
	});
	//----------------------------------------
	
/*    $('#frmStockBalance #butDelete').click(function(){
		if($('#frmStockBalance #cboSearch').val()=='')
		{
			$('#frmStockBalance #butDelete').validationEngine('showPrompt', 'Please select Type.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmStockBalance #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = "typeOfPrint-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											data:'requestType=delete&cboSearch='+$('#frmStockBalance #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmStockBalance #butDelete').validationEngine('showPrompt', json.msg,json.type );
												
												if(json.type=='pass')
												{
													$('#frmStockBalance').get(0).reset();
													loadCombo_frmStockBalance();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
	
*/	
$('#frmStockBalance #cboCompany').change(function(){
	var company = $('#frmStockBalance #cboCompany').val();
	var url 		= "stockBalance-db-get.php?requestType=loadLocations&company="+company;
	var httpobj 	= $.ajax({url:url,async:false})
	document.getElementById('cboLocation').innerHTML=httpobj.responseText;
	
});
//--------------------------------------------------------
$('#frmStockBalance #cboCompany').change(function(){
	var company = $('#frmStockBalance #cboCompany').val();
	var url 		= "stockBalance-db-get.php?requestType=loadCurrency&company="+company;
	var httpobj 	= $.ajax({url:url,async:false});
	var curr=httpobj.responseText.trim();
	document.getElementById('cboCurrency').value=curr;
	
});
//--------------------------------------------------------
  $('#frmStockBalance #cboOrderNo').change(function(){
		var orderNo = $('#frmStockBalance #cboOrderNo').val();
		var url 		= "stockBalance-db-get.php?requestType=loadStyleNo&orderNo="+orderNo;
		var httpobj 	= $.ajax({url:url,async:false})
		document.getElementById('cbStyleNo').innerHTML=httpobj.responseText;
		
  });
 //-------------------------------------------- 
  $('#frmStockBalance #cboMainCategory').change(function(){
		var mainCategory = $('#frmStockBalance #cboMainCategory').val();
		var url 		= "stockBalance-db-get.php?requestType=loadSubCategory&mainCategory="+mainCategory;
		var httpobj 	= $.ajax({url:url,async:false})
		document.getElementById('cboSubCategory').innerHTML=httpobj.responseText;
  });
 //-------------------------------------------- 
  $('#frmStockBalance #cboSubCategory').change(function(){
		var mainCategory = $('#frmStockBalance #cboMainCategory').val();
		var subCategory = $('#frmStockBalance #cboSubCategory').val();
		var url 		= "stockBalance-db-get.php?requestType=loadItems&mainCategory="+mainCategory+"&subCategory="+subCategory;
		var httpobj 	= $.ajax({url:url,async:false})
		document.getElementById('cboItems').innerHTML=httpobj.responseText;
  });
 //-------------------------------------------- 
  $('#frmStockBalance #chkStyle').click(function(){
		document.getElementById('rwOrder').style.display='';
		document.getElementById('rwStyle').style.display='';
  });
 //-------------------------------------------- 
  $('#frmStockBalance #chkGeneral').click(function(){
		document.getElementById('rwOrder').style.display='none';
		document.getElementById('rwStyle').style.display='none';
  });
 //-------------------------------------------- 
  $('#chkDate').click(function(){
	  var val = $(this).attr('checked');
	  if(!val)
	  {
		$('#dtDateFrom').val('');  
		$('#dtDateFrom').attr('disabled','true');
		$('#dtDateTo').val('');  
		$('#dtDateTo').attr('disabled','true');
	  }
	  else
	  {
		$('#dtDateFrom').removeAttr('disabled');
		$('#dtDateTo').removeAttr('disabled');
	  }
  });
 //-------------------------------------------- 
 $('#frmStockBalance .subcheck').live('click',function(){
	loadItems();	 
 });
 
  
 
  $('#frmStockBalance #imgSearchItems').click(function(){
	  
	if ($('#frmStockBalance').validationEngine('validate'))   
    { 
	//	document.frmStockBalance.submit();
	//alert($('#frmStockBalance #cboCompany').val());
	//alert($('#cboCompany').val());
	
	var dateFrom=$('#dtDateFrom').val();
	var dateTo=$('#dtDateTo').val();
	if(dateFrom>dateTo){
		alert("Invalid Date Range");
		return false;
	}
	
	var string="";
	$('#frmStockBalance .subcheck:checked').each(function(){
		var subCatId=$(this).val();	
				string += subCatId  +',' ;
	});
	string = string.substr(0,string.length-1);
	
	
	
	var url='rptStockBalance.php?';
		url+='company='+$('#frmStockBalance #cboCompany').val();
		url+='&currency='+$('#frmStockBalance #cboCurrency').val();
		url+='&location='+$('#frmStockBalance #cboLocation').val();
		url+='&chkStyle='+$('input[name=radio]:checked').val();
		url+='&reptType='+$('input[name=radioReptType]:checked').val();
		url+='&orderNo='+$('#frmStockBalance #cboOrderNo').val();
		url+='&styleNo='+$('#frmStockBalance #cbStyleNo').val();
		url+='&mainCat='+$('#frmStockBalance #cboMainCategory').val();
		url+='&subCat='+string;
		url+='&item='+$('#frmStockBalance #cboItems').val();
		url+='&dateFrom='+dateFrom;
		url+='&dateTo='+dateTo;
		window.open(url);
	}
  });
 //---------------------------------------------
$('.delImg').click(function(){
	$(this).parent().parent().remove();
});
//-----------------------------------
$('#butReport').click(function(){
	if($('#txtRetSupNo').val()!=''){
		window.open('../listing/rptMrn.php?mrnNo='+$('#txtMrnNo').val()+'&year='+$('#txtYear').val());	
	}
	else{
		alert("There is no MRN to view");
	}
});
//----------------------------------	
$('#butConfirm').click(function(){
	if($('#txtRetSupNo').val()!=''){
		window.open('../listing/rptMrn.php?mrnNo='+$('#txtMrnNo').val()+'&year='+$('#txtYear').val()+'&approveMode=1');	
	}
	else{
		alert("There is no MRN to confirm");
	}
});
//-----------------------------------------------------
$('#butClose').click(function(){
		//load('main.php');	
});
//--------------refresh the form-----------------------
	$('#frmStockBalance #butNew').click(function(){
		$('#frmStockBalance').get(0).reset();
		clearRows();
		$('#frmStockBalance #txtMrnNo').val('');
		$('#frmStockBalance #txtYear').val('');
		$('#frmStockBalance #cboDepartment').val('');
		$('#frmStockBalance #cboDepartment').focus();
		
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmStockBalance #dtDate').val(d);
	});
	
	
 //---------------------------------
  $('#frmStockBalance #chkAll2').click(function(){
		if(document.getElementById('chkAll2').checked==true)
		 var chk=true;
		else
		 var chk=false;
		
		var rowCount = document.getElementById('tblMainCategory').rows.length;
		for(var i=1;i<rowCount;i++)
		{
	    	document.getElementById('tblMainCategory').rows[i].cells[0].childNodes[0].checked=chk;
			
			if(chk==true){
				$('#div'+i).show();
				$('#div'+i+' input:checkbox').attr('checked','checked');
			}
			else{
				$('#div'+i).hide();
				$('#div'+i+' input:checkbox').removeAttr('checked');
			}
		}
		
		loadItems();
		
    });

 //---------------------------------
  //---------------------------------
  	$('#frmStockBalance .mainCateg').click(function(){
	 	var id = $(this).val();
		if($(this).attr('checked'))
		{
			$('#div'+id).show();
			$('#div'+id+' input:checkbox').attr('checked','checked');
		}
		else
		{
			$('#div'+$(this).val()).hide();
			$('#div'+id+' input:checkbox').removeAttr('checked');
		}
		loadItems();
});

 //-----------------------------------------------------


});//----------end of ready -------------------------------



//-------------------------------------------------------
function loadPopup()
{
		popupWindow3('1');
		var grnNo = $('#txtGrnNo').val();
		$('#popupContact1').load('mrnPopup.php?grnNo='+grnNo,function(){
				//checkAlreadySelected();
				$('#butAdd').click(addClickedRows);
				$('#butClose1').click(disablePopup);
				 //-------------------------------------------- 
				  $('#frmStockBalancePopup #cboItemType').change(function(){
						var itemType = $('#cboItemType').val();
						if(itemType==0){
						$('#frmStockBalancePopup #cboOrderNo').val('');
						$('#frmStockBalancePopup #cbStyleNo').val('');
						document.getElementById('rw1').style.display='none';
						document.getElementById('rw2').style.display='none';
						document.getElementById('rwh1').style.display='none';
						document.getElementById('rwh2').style.display='none';
						document.getElementById('rwh3').style.display='none';
						document.getElementById('rwh4').style.display='none';
						document.getElementById('rwh5').style.display='none';
						document.getElementById('rwh6').style.display='none';
						document.getElementById('rwh7').style.display='none';
						}
						else{
						document.getElementById('rw1').style.display='';
						document.getElementById('rw2').style.display='';
						document.getElementById('rwh1').style.display='';
						document.getElementById('rwh2').style.display='';
						document.getElementById('rwh3').style.display='';
						document.getElementById('rwh4').style.display='';
						document.getElementById('rwh5').style.display='';
						document.getElementById('rwh6').style.display='';
						document.getElementById('rwh7').style.display='';
						}
						
				  });
				 //-------------------------------------------- 
				  $('#frmStockBalancePopup #cboMainCategory').change(function(){
						var mainCategory = $('#cboMainCategory').val();
						var url 		= "mrn-db-get.php?requestType=loadSubCategory&mainCategory="+mainCategory;
						var httpobj 	= $.ajax({url:url,async:false})
						document.getElementById('cboSubCategory').innerHTML=httpobj.responseText;
						
				  });
				 //-------------------------------------------- 
				  $('#frmStockBalancePopup #cboOrderNo').change(function(){
						var orderNo = $('#cboOrderNo').val();
						var url 		= "mrn-db-get.php?requestType=loadStyleNo&orderNo="+orderNo;
						var httpobj 	= $.ajax({url:url,async:false})
						document.getElementById('cbStyleNo').innerHTML=httpobj.responseText;
						
				  });
				 //-------------------------------------------- 
				  $('#frmStockBalancePopup #imgSearchItems').click(function(){
						var rowCount = document.getElementById('tblItemsPopup').rows.length;
						for(var i=1;i<rowCount;i++)
						{
							document.getElementById('tblItemsPopup').deleteRow(1);
						}
						var itemType = $('#cboItemType').val();
						var orderNo = $('#cboOrderNo').val();
						var styleNo = $('#cbStyleNo').val();
						var mainCategory = $('#cboMainCategory').val();
						var subCategory = $('#cboSubCategory').val();
						var description = $('#txtItmDesc').val();
						var url 		= "mrn-db-get.php?requestType=loadItems";
						var httpobj = $.ajax({
							url:url,
							dataType:'json',
							data:"orderNo="+orderNo+"&mainCategory="+mainCategory+"&subCategory="+subCategory+"&styleNo="+styleNo+"&description="+description+"&itemType="+itemType, 
							async:false,
							success:function(json){

								var length = json.arrCombo.length;
								var arrCombo = json.arrCombo;


								for(var i=0;i<length;i++)
								{
									var itemId=arrCombo[i]['itemId'];	
									var maincatId=arrCombo[i]['maincatId'];	
									var subCatId=arrCombo[i]['subCatId'];	
									var code=arrCombo[i]['code'];	
									var itemName=arrCombo[i]['itemName'];	
									var requiredQty=arrCombo[i]['requiredQty'];	
									var allocatedQty=arrCombo[i]['allocatedQty'];	
									var mrnQty=arrCombo[i]['mrnQty'];	
									var issueQty=arrCombo[i]['issueQty'];	
									var stockBal=arrCombo[i]['stockBal'];	
									var mainCatName=arrCombo[i]['mainCatName'];
									var subCatName=arrCombo[i]['subCatName'];	
									var balToMrnqty=parseFloat(allocatedQty)-parseFloat(mrnQty);

										
									var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF" id=""><input type="checkbox" id="chkDisp" /></td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'">'+mainCatName+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'">'+subCatName+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'">'+code+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'">'+itemName+'</td>';
									
									if(itemType==0){
									content +='<td align="center" bgcolor="#FFFFFF" style="display:none" id="'+requiredQty+'"  >'+requiredQty+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" style="display:none" id="'+allocatedQty+'"  >'+allocatedQty+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" style="display:none" id="'+mrnQty+'" >'+mrnQty+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" style="display:none" id="'+issueQty+'" >'+issueQty+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" style="display:none" id="'+balToMrnqty+'" >'+balToMrnqty+'</td>';
									}
									else{
									content +='<td align="center" bgcolor="#FFFFFF"  id="'+requiredQty+'"  >'+requiredQty+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+allocatedQty+'"  >'+allocatedQty+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+mrnQty+'" >'+mrnQty+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+issueQty+'" >'+issueQty+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+balToMrnqty+'" >'+balToMrnqty+'</td>';
									}
									
									
									content +='<td align="center" bgcolor="#FFFFFF" id="'+orderNo+'" style="display:none" >'+orderNo+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+styleNo+'" style="display:none" >'+styleNo+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+stockBal+'">'+stockBal+'</td></tr>';

									add_new_row('#frmStockBalancePopup #tblItemsPopup',content);
									
								}
									checkAlreadySelected();

							}
						});
						
				  });
				  //-------------------------------------------------------
					$('#butAdd').click(addClickedRows);
					$('#butClose1').click(disablePopup);
				  //-------------------------------------------------------
			});	
}//-------------------------------------------------------
function loadMain()
{
	//alert(1);
/*	$("#butAddPendingDispatch").click(function(){
		popupWindow3('1');
		//$('#iframeMain1').attr('src','http://localhost/qpet/presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php?id='+$('#cboDispatchTo').val());
		$('#popupContact1').load('sampleDispatchPopup.php');	
		
	});*/
	//$("#iframeMain1").contents().find("#butAdd").click(abc);
	//$("#frmStockBalancePopup").contents().find("#butAdd").click(abc);
//	$('#frmStockBalancePopup #butClose').click(abc);
}

//----------------------------------------------------
function addClickedRows()
{
	//var rowCount = $('#tblDispatchPopup >tr').length;
	var rowCount = document.getElementById('tblItemsPopup').rows.length;
	var totRetAmmount=0;
	var itemType = $('#cboItemType').val();

	for(var i=1;i<rowCount;i++)
	{
		if((document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].checked==true) && (document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].disabled==false)){
			
			var itemId=document.getElementById('tblItemsPopup').rows[i].cells[4].id;
			var maincatId=document.getElementById('tblItemsPopup').rows[i].cells[1].id;
			var subCatId=document.getElementById('tblItemsPopup').rows[i].cells[2].id;
			var code=document.getElementById('tblItemsPopup').rows[i].cells[3].innerHTML;
			var itemName=document.getElementById('tblItemsPopup').rows[i].cells[4].innerHTML;
			var mainCatName=document.getElementById('tblItemsPopup').rows[i].cells[1].innerHTML;
			var subCatName=document.getElementById('tblItemsPopup').rows[i].cells[2].innerHTML;
			var balToMrnQty=parseFloat(document.getElementById('tblItemsPopup').rows[i].cells[9].innerHTML);
			var maxMrn=balToMrnQty;
			var orderNo=document.getElementById('tblItemsPopup').rows[i].cells[10].innerHTML;
			var StyleNo=document.getElementById('tblItemsPopup').rows[i].cells[11].innerHTML;
				if(itemType==0){
				balToMrnQty=0;
				maxMrn=10000;
				}

		//	var stockBalQty=parseFloat(document.getElementById('tblItemsPopup').rows[i].cells[6].innerHTML);
		//	var Qty=parseFloat(document.getElementById('tblItemsPopup').rows[i].cells[6].id);

			//alert($('#frmStockBalance #cboDispatchTo').val());
			var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF"><img class="delImg" src="../../../../images/del.png" width="15" height="15" /></td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+orderNo+'">'+orderNo+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+StyleNo+'">'+StyleNo+'</td>';
			//content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'">'+itemName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'">'+mainCatName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'">'+subCatName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'">'+itemName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id=""><input  id="'+balToMrnQty+'" class="validate[required,custom[integer],max['+maxMrn +']] calculateValue" style="width:80px;text-align:center" type="text" value="'+balToMrnQty+'"/></td>';
			content +='</tr>';
			
			
		//	content +='<td align="center" bgcolor="#FFFFFF" id="" class=""><input style="width:80px;text-align:center" type="text" value="0"/></td>';
		
			add_new_row('#frmStockBalance #tblMrn',content);
		//	totRetAmmount+=Qty;
			//$('.')
			//$("#frmStockBalance").validationEngine();
		//----------------------------	
		$('.delImg').click(function(){
			$(this).parent().parent().remove();
		});
		
		//----------------------------	
		$('.clsValidateBalQty').keyup(function(){
			var input=$(this).val();
			var balQty=$(this).closest('td').attr('id');
			if((input>balQty) || (input<0)){
			alert("Invalid Qty");
				$(this).val(balQty);
			}

		});
		//----------------------------
		$('.calculateValue').keyup(function(){
			//alert(this.parentNode.parentNode.rowIndex);
			var row=this.parentNode.parentNode.rowIndex;
			if(document.getElementById('tblMrn').rows[row].cells[this.parentNode.cellIndex].childNodes[0].value==''){
				document.getElementById('tblMrn').rows[row].cells[this.parentNode.cellIndex].childNodes[0].value=0;
			}
			
			
			var qty = parseFloat(document.getElementById('tblMrn').rows[row].cells[10].childNodes[0].value);
			var price = parseFloat(document.getElementById('tblMrn').rows[row].cells[7].innerHTML)-parseFloat(document.getElementById('tblMrn').rows[row].cells[8].childNodes[0].value);
			document.getElementById('tblMrn').rows[row].cells[11].innerHTML=qty*price;
			//calculateTotalVal();
		});
		//----------------------------
/*	  //--------------change customer------------
		$('#frmStockBalance #cboDispatchTo').change(function(){
				var val = $.prompt('Are you sure you want to delete existing samples  ?',{
							buttons: { Ok: true, Cancel: false },
							callback: function(v,m,f){
								if(v)
								{
									clearRows();
								}
					}
				});
		});
	//--------------------------------------------	
*/		}
	}
	$('#txtTotAmnt').val(totRetAmmount)
	disablePopup();
}
//-------------------------------------
function alertx()
{
	$('#frmStockBalance #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmStockBalance #butDelete').validationEngine('hide')	;
}

function closePopUp(){
	
}
//----------------------------------------------
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
//----------------------------------------------- 
function clearRows()
{
	//var rowCount = $('#tblDispatchPopup >tbody >tr').length;
	var rowCount = document.getElementById('tblMrn').rows.length;
	//alert(rowCount);
	//var cellCount = document.getElementById('tblMain').rows[1].cells.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblMrn').deleteRow(1);
			
	}
}
//------------------------------------------------------------
function checkAlreadySelected(){
	
	var rowCount = document.getElementById('tblMrn').rows.length;
	for(var i=1;i<rowCount;i++)
	{
			var itemId = 	document.getElementById('tblMrn').rows[i].cells[5].id;
			var orderNo = 	document.getElementById('tblMrn').rows[i].cells[1].innerHTML;
			var styleNo = 	document.getElementById('tblMrn').rows[i].cells[2].innerHTML;
		
			var rowCount1 = document.getElementById('tblItemsPopup').rows.length;

			for(var j=1;j<rowCount1;j++)
			{
				var itemIdP = 	document.getElementById('tblItemsPopup').rows[j].cells[4].id;
				var orderNoP = 	document.getElementById('tblItemsPopup').rows[j].cells[10].innerHTML;
				var styleNoP = 	document.getElementById('tblItemsPopup').rows[j].cells[11].innerHTML;
				
			//alert(orderNo+"=="+orderNoP+"***"+styleNo+"=="+styleNoP+"***"+itemId+"=="+itemIdP+"***")
				if((orderNo==orderNoP) && (styleNo==styleNoP) && (itemId==itemIdP)){
					document.getElementById('tblItemsPopup').rows[j].cells[0].childNodes[0].checked=true;
					document.getElementById('tblItemsPopup').rows[j].cells[0].childNodes[0].disabled=true;
				}
			}
	}
}
//------------------------------------------------------------------
function loadHeaderData(){
	    var grnNo = $('#txtGrnNo').val();
		var url 		= "mrn-db-get.php?requestType=loadHeaderData";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"grnNo="+grnNo,
			async:false,
			success:function(json){

					$('#frmStockBalance #txtSupplier').val(json.supplier);
					$('#frmStockBalance #txtInvoiceNo').val(json.invoiceNo);
			}
		});
}
//---------------------------------------------------
function loadItems(){
/*	var noOfTbls = document.getElementById('txtNoOfTables').value;
	for(var i=1;i<=noOfTbls;i++)
	{
		var node='#tbl'+i+' .chkRow';
		$('#tbl'+i+' .chkRow').each(function(){
			
		}
	}*/
	var data = "requestType=loadItems";
	var string="";
	
	$('#frmStockBalance .subcheck:checked').each(function(){
		var subCatId=$(this).val();	
			//	arr2 += "{";
			//	arr2 += '"subCatId":"'+		subCatId  +'"' ;
				string += subCatId  +',' ;
			//	arr2 +=  '},';
	});

				string = string.substr(0,string.length-1);
				data+="&string="	+	string;
	var url 		= "stockBalance-db-get.php?"+data;
	var httpobj 	= $.ajax({url:url,async:false})
	//document.getElementById('cboItems').innerHTML=httpobj.responseText;
	$('#frmStockBalance #cboItems').html(httpobj.responseText);
}
//---------------------------------------------------
 
<?php
session_start();
$backwardseperator = "../../../";
$companyId = $_SESSION['CompanyID'];
$locationId 	= $_SESSION['CompanyID'];//this locationId use in report header(reportHeader.php)-------
$intUser  = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
include  	"{$backwardseperator}dataAccess/Connector.php";

$company = $_REQUEST['company'];
//$locationId $_REQUEST['company'];//this locationId use in report header(reportHeader.php)----
//$searchCurrency=2;
$searchCurrency = $_REQUEST['currency'];
$location = $_REQUEST['location'];
$chkStyle = $_REQUEST['chkStyle'];
$reptType = $_REQUEST['reptType'];
$orderNo = $_REQUEST['orderNo'];
$orderNoArray=explode("/",$orderNo);
$orderNo=$orderNoArray[0];
$orderYear=$orderNoArray[1];
$styleNo = $_REQUEST['styleNo'];
$mainCat = $_REQUEST['mainCat'];
$subCat = $_REQUEST['subCat'];
$item = $_REQUEST['item'];
$dateFrom = $_REQUEST['dateFrom'];
$dateTo = $_REQUEST['dateTo'];

 $sql = "SELECT
mst_companies.strName
FROM mst_companies
WHERE
mst_companies.intId =  '$company'
";
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
$comp = $row['strName'];
//----------------------------------	
 $sql = "SELECT
mst_financecurrency.strCode
FROM mst_financecurrency
WHERE
mst_financecurrency.intId =  '$searchCurrency'";
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
$curren = $row['strCode'];	
//----------------------------------
 $sql = "SELECT
mst_locations.strName
FROM
mst_locations
WHERE
mst_locations.intId =  '$location'";
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
$loca = $row['strName'];
//----------------------------------
 $sql = "SELECT
mst_companies.intBaseCurrencyId
FROM mst_companies
WHERE
mst_companies.intId =  '$companyId'";
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
$baseCurrency = $row['intBaseCurrencyId'];
//----------------------------------
//get latest exchange rate
 $sql = "SELECT
mst_financeexchangerate.dblBuying,
Max(mst_financeexchangerate.dtmDate)
FROM mst_financeexchangerate
WHERE
mst_financeexchangerate.intCurrencyId =  '$searchCurrency' 
GROUP BY
mst_financeexchangerate.dblBuying";
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
$latestExchRate = $row['dblBuying'];
//----------------------------------
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Stock Balance Report</title>
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />


<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../mrn/listing/rptMrn-js.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/script.js"></script>

<script src="../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.min.js"></script>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:264px;
	top:247px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<form id="frmMrnReport" name="frmMrnReport" method="post" action="../mrn/listing/rptMrn.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include '../../../reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>STOCK BALANCE <?php if($reptType==2){ ?> VALUE <?php } ?>REPORT</strong><strong></strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
  <?php
  if($comp!=''){
  ?>
  <tr>
    <td width="7%">&nbsp;</td>
    <td width="9%" class="normalfnt" align="right"><strong style="text-align:right">Company</strong></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="52%" align="left"><span class="normalfnt" style="text-align:left"><?php echo $comp  ?></span></td>
    <td width="6%">&nbsp;</td>
    <td width="6%" align="center" valign="middle">&nbsp;</td>
    <td width="6%">&nbsp;</td>
    <td width="6%" class="normalfnt">&nbsp;</td>
    <td width="7%">&nbsp;</td>
  </tr>
  <?php
  }
  ?>
  <?php
  if($curren!=''){
  ?>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt" align="right"><strong style="text-align:right">Currency</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt" align="left" style="text-align:left"><?php echo $curren  ?></span></td>
    <td>&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <?php
  }
  ?>
  <?php
  if($loca!=''){
  ?>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Location</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $loca  ?></span></td>
    <td>&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <?php
  }
  ?>
  
  <?php
  if(($dateFrom!='') ||($dateTo!='')) {
  ?>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Date Range</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $dateFrom."  To  ".$dateTo  ?></span></td>
    <td>&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <?php
  }
  ?>
  
  </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td width="5%">&nbsp;</td>
        <td colspan="7" class="normalfnt">
          <table width="100%" class="grid" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr class="gridHeader">
              <td width="7%" >Order No</td>
              <td width="7%" >Style No</td>
              <td width="15%" >Main Category</td>
              <td width="15%" >Sub Category</td>
              <td width="9%" >Item Code</td>
              <td width="20%" >Item</td>
              <td width="7%" >Qty</td>
              <td width="5%" <?php if($reptType=='1'){ ?> style="display:none"<?php } ?> >UOM</td>
              <td width="8%" <?php if($reptType=='1'){ ?> style="display:none"<?php } ?> >Unit Price</td>
              <td width="7%" <?php if($reptType=='1'){ ?> style="display:none"<?php } ?> >Value(<?php echo $curren;  ?>)</td>
              </tr>
              <?php 
	  if($chkStyle==1){
	  	   $sql1 = "select 
					tb1.dtmDate, 
					tb1.dblBuying as poCurrencyRate , 
		            tb1.intOrderNo,
					tb1.intOrderYear,
					tb1.strStyleNo,
					tb1.mainCategory,
					tb1.subCategory,
					tb1.strCode,
					tb1.intItemId,
					tb1.itemName,
					tb1.UOM,
					Sum(tb1.Qty) as Qty, 
					sum(((tb1.value*tb1.dblBuying)/$latestExchRate)) as Val1, 
					sum(((tb1.value*tb1.dblBuying)/mst_financeexchangerate.dblBuying)) as Val, 
					mst_financeexchangerate.dblBuying as  searchCurrencyRate 
					from(SELECT  
					ware_grnheader.intCompanyId,  
		            ware_stocktransactions.intOrderNo,
					ware_stocktransactions.intOrderYear,
					ware_stocktransactions.strStyleNo,
					mst_maincategory.strName as mainCategory,
					mst_subcategory.strName as subCategory,
					mst_item.strCode,
					ware_stocktransactions.intItemId, 
					mst_item.strName as itemName,
					mst_units.strCode as UOM, 
					mst_financeexchangerate.dblBuying, 
					mst_financeexchangerate.dtmDate,
					(ware_stocktransactions.dblQty*dblGRNRate) AS value, 
					(ware_stocktransactions.dblQty) AS Qty
					FROM
					ware_stocktransactions 
					Inner Join ware_grnheader ON ware_stocktransactions.intGRNNo = ware_grnheader.intGrnNo AND ware_stocktransactions.intGRNYear = ware_grnheader.intGrnYear
					Inner Join trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
					Inner Join mst_locations ON ware_grnheader.intCompanyId = mst_locations.intId 
					left Join mst_financeexchangerate ON trn_poheader.intCurrency = mst_financeexchangerate.intCurrencyId AND trn_poheader.dtmPODate = mst_financeexchangerate.dtmDate AND mst_locations.intCompanyId = mst_financeexchangerate.intCompanyId 
					Inner Join mst_item ON ware_stocktransactions.intItemId = mst_item.intId
					Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
					Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
					Inner Join mst_units ON mst_item.intUOM = mst_units.intId  
					WHERE
					ware_stocktransactions.intItemId !=  '*'  ";
			if($company!='')		
			$sql1 .="AND ware_stocktransactions.intCompanyId =  '$company'  " ;
			if($location!='')		
			$sql1 .="AND ware_stocktransactions.intLocationId =  '$location' " ;
			if($orderNo!='')		
			$sql1 .="AND ware_stocktransactions.intOrderNo =  '$orderNo' " ;
			if($orderYear!='')		
			$sql1 .="AND ware_stocktransactions.intOrderYear =  '$orderYear' " ;
			if($styleNo!='')		
			$sql1 .="AND ware_stocktransactions.strStyleNo =  '$styleNo' " ; 
/*			if($mainCat!='')		
			$sql1 .="AND mst_item.intMainCategory =  '$mainCat'  " ;
*/			if($subCat!='')		
			$sql1 .="AND mst_item.intSubCategory IN  ($subCat)  " ;
			if($item!='')		
			$sql1 .="AND mst_item.intId =  '$item' " ;
			if($dateFrom!='')		
			$sql1 .="AND ware_stocktransactions.dtGRNDate >=  '$dateFrom' " ;
			if($dateTo!='')		
			$sql1 .="AND ware_stocktransactions.dtGRNDate <=  '$dateTo' " ;
			
			$sql1 .=" ) as tb1 ";
				
			$sql1 .="Inner Join mst_locations ON tb1.intCompanyId = mst_locations.intId 
			        left Join mst_financeexchangerate ON tb1.dtmDate   = mst_financeexchangerate.dtmDate  AND mst_locations.intCompanyId = mst_financeexchangerate.intCompanyId 
AND  mst_financeexchangerate.intCurrencyId='$searchCurrency' ";

			$sql1 .="GROUP BY
					tb1.intOrderNo,
					tb1.intOrderYear,
					tb1.strStyleNo,
					tb1.intItemId
					ORDER BY
					tb1.intOrderNo ASC,
					tb1.intOrderYear ASC,
					tb1.strStyleNo ASC,
					tb1.itemName ASC,
					tb1.mainCategory ASC,
					tb1.subCategory ASC";

 
	  }
	  else if($chkStyle==2){
	  	   $sql1 = "select 
					tb1.dtmDate, 
					tb1.dblBuying as poCurrencyRate , 
					tb1.mainCategory,
					tb1.subCategory,
					tb1.strCode,
					tb1.intItemId,
					tb1.itemName,
					tb1.UOM,
					Sum(tb1.Qty) as Qty, 
					sum(((tb1.value*tb1.dblBuying)/$latestExchRate)) as Val1, 
					sum(((tb1.value*tb1.dblBuying)/mst_financeexchangerate.dblBuying)) as Val, 
					mst_financeexchangerate.dblBuying as searchCurrencyRate 
					from(SELECT 
					ware_grnheader.intCompanyId,  
					mst_maincategory.strName as mainCategory,
					mst_subcategory.strName as subCategory,
					mst_item.strCode,
					ware_stocktransactions_bulk.intItemId, 
					mst_item.strName as itemName,
					mst_units.strCode as UOM,
					mst_financeexchangerate.dblBuying, 
					mst_financeexchangerate.dtmDate, 
					(ware_stocktransactions_bulk.dblQty*dblGRNRate) AS value, 
					(ware_stocktransactions_bulk.dblQty) AS Qty
					FROM
					ware_stocktransactions_bulk
					Inner Join ware_grnheader ON ware_stocktransactions_bulk.intGRNNo = ware_grnheader.intGrnNo AND ware_stocktransactions_bulk.intGRNYear = ware_grnheader.intGrnYear
					Inner Join trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
					Inner Join mst_locations ON ware_grnheader.intCompanyId = mst_locations.intId 
					left Join mst_financeexchangerate ON trn_poheader.intCurrency = mst_financeexchangerate.intCurrencyId AND trn_poheader.dtmPODate = mst_financeexchangerate.dtmDate AND mst_locations.intCompanyId = mst_financeexchangerate.intCompanyId 
					Inner Join mst_item ON ware_stocktransactions_bulk.intItemId = mst_item.intId
					Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
					Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
					Inner Join mst_units ON mst_item.intUOM = mst_units.intId  
					WHERE
					ware_stocktransactions_bulk.intItemId !=  '*'  ";
			if($company!='')		
			$sql1 .="AND ware_stocktransactions_bulk.intCompanyId =  '$company'  " ;
			if($location!='')		
			$sql1 .="AND ware_stocktransactions_bulk.intLocationId =  '$location' " ;
/*			if($mainCat!='')		
			$sql1 .="AND mst_item.intMainCategory =  '$mainCat'  " ;
*/			if($subCat!='')		
			$sql1 .="AND mst_item.intSubCategory IN  ($subCat)  " ;
			if($item!='')		
			$sql1 .="AND mst_item.intId =  '$item' " ;
			if($dateFrom!='')		
			$sql1 .="AND ware_stocktransactions_bulk.dtGRNDate >=  '$dateFrom' " ;
			if($dateTo!='')		
			$sql1 .="AND ware_stocktransactions_bulk.dtGRNDate <=  '$dateTo' " ;
			
			$sql1 .=" ) as tb1 ";
					  
			$sql1 .="Inner Join mst_locations ON tb1.intCompanyId = mst_locations.intId 
			        left Join mst_financeexchangerate ON tb1.dtmDate   = mst_financeexchangerate.dtmDate  AND mst_locations.intCompanyId = mst_financeexchangerate.intCompanyId 
AND  mst_financeexchangerate.intCurrencyId='$searchCurrency' ";

			$sql1 .="GROUP BY
					tb1.intItemId
					ORDER BY
					tb1.itemName ASC,
					tb1.mainCategory ASC,
					tb1.subCategory ASC";
	  }
	     // echo $sql1;
			$result1 = $db->RunQuery($sql1);
			$totQty=0;
			$totAmmount=0;
			while($row=mysqli_fetch_array($result1))
			{
				$orderNo=$row['intOrderNo']."/".$row['intOrderYear'];
				if($row['intOrderNo']==''){
				 $orderNo='';	
				}
				if($searchCurrency==$baseCurrency){	//if filter from base currency  
					$value=$row['Val'];
				}
				else{
					$value=$row['Val1'];
				}
	  ?>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfnt" >&nbsp;<?php echo $orderNo;?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['strStyleNo']?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['mainCategory']?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['subCategory'] ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['strCode'] ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['itemName'] ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo number_format($row['Qty'],2) ?>&nbsp;</td>
              <td class="normalfntRight" <?php if($reptType=='1'){ ?> style="display:none"<?php } ?> >&nbsp;<?php echo $row['UOM'] ?>&nbsp;</td>
              <td class="normalfntRight" <?php if($reptType=='1'){ ?> style="display:none"<?php } ?> >&nbsp;<?php echo number_format($value/$row['Qty'],4) ?>&nbsp;</td>
              <td class="normalfntRight" <?php if($reptType=='1'){ ?> style="display:none"<?php } ?> >&nbsp;<?php echo number_format($value,4) ?>&nbsp;</td>
              </tr>
      <?php 
			$totQty+=$row['Qty'];
			$totVal+=$row['Val'];
			}
	  ?>
            <tr class="normalfnt"  bgcolor="#CCCCCC">
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo number_format($totQty,2) ?>&nbsp;</td>
              <td class="normalfntRight" <?php if($reptType=='1'){ ?> style="display:none"<?php } ?> >&nbsp;<?php //echo $totVal ?>&nbsp;</td>
              <td class="normalfntRight" <?php if($reptType=='1'){ ?> style="display:none"<?php } ?> >&nbsp;<?php //echo $totVal ?>&nbsp;</td>
              <td class="normalfntRight" <?php if($reptType=='1'){ ?> style="display:none"<?php } ?> >&nbsp;<?php echo number_format($totVal,4) ?>&nbsp;</td>
              </tr>
            </table>
          </td>
        <td width="6%">&nbsp;</td>
        </tr>
      
      </table>
    </td>
</tr>

<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
</body>
</html>
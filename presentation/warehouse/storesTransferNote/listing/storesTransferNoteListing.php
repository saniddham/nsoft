<?php
date_default_timezone_set('Asia/Kolkata');
//ini_set('allow_url_include',1);
// ini_set('display_errors',1);
session_start();

$backwardseperator 	= "../../../../";
$thisFilePath 		=  $_SERVER['PHP_SELF'];
include_once $backwardseperator."dataAccess/permisionCheck2.inc";
include_once "../../../../libraries/jqdrid/inc/jqgrid_dist.php";

$company 			= $_SESSION['headCompanyId'];
$location   		= $_SESSION['CompanyID'];

$programName		= 'Stores Transfer Note';
$programCode		= 'P0258';
$intUser  			= $_SESSION["userId"];
$approveLevel 		= (int)getMaxApproveLevel();

$sql = "select * from(SELECT DISTINCT 
							if(tb1.intStatus=1,'Approved',if(tb1.intStatus=0,'Rejected','Pending')) as Status,
							tb1.intTransfNo as `STNNo`,
							tb1.intTransfYear as `STNYear`,
							tb1.intRequisitionNo as `SRNNo`,
							tb1.intRequisitionYear as `SRNYear`,
							SSRT.strName AS srnTo,
							SSRF.strName AS srnFrom,
							tb1.strNote as `Note`,
							date(tb1.datdate) as `Date`,
							
							IFNULL((SELECT
							Sum(ware_storestransferdetails.dblQty)
							FROM
							ware_storestransferheader
							Inner Join ware_storestransferdetails ON ware_storestransferheader.intTransfNo = ware_storestransferdetails.intTransfNo AND ware_storestransferheader.intTransfYear = ware_storestransferdetails.intTransfYear
							WHERE
							ware_storestransferheader.intTransfNo =  tb1.intTransfNo AND
							ware_storestransferheader.intTransfYear =  tb1.intTransfYear),0) as `STN_Qty`,
							  
							sys_users.strUserName as `User`, 
							  
							IFNULL((
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(ware_storestransferheader_approvedby.dtApprovedDate),')' )
								FROM
								ware_storestransferheader_approvedby
								Inner Join sys_users ON ware_storestransferheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_storestransferheader_approvedby.intTransfNo  = tb1.intTransfNo AND
								ware_storestransferheader_approvedby.intYear =  tb1.intTransfYear AND
								ware_storestransferheader_approvedby.intApproveLevelNo = '1' AND 
								ware_storestransferheader_approvedby.intStatus = '0' 
							),IF(((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.intStatus>1),'Approve', '')) as `1st_Approval`,  ";
							
						for($i=2; $i<=$approveLevel; $i++){
							
							if($i==2){
							$approval="2nd_Approval";
							}
							else if($i==3){
							$approval="3rd_Approval";
							}
							else {
							$approval=$i."th_Approval";
							}
							
							
						$sql .= "IFNULL(
						/*condition*/
(
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(ware_storestransferheader_approvedby.dtApprovedDate),')' )
								FROM
								ware_storestransferheader_approvedby
								Inner Join sys_users ON ware_storestransferheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_storestransferheader_approvedby.intTransfNo  = tb1.intTransfNo AND
								ware_storestransferheader_approvedby.intYear =  tb1.intTransfYear AND
								ware_storestransferheader_approvedby.intApproveLevelNo =  '$i' AND 
								ware_storestransferheader_approvedby.intStatus='0' 
							),
							/*end condition*/
							
							/*false part*/

							IF(
							/*if condition */
							((SELECT
								menupermision.int".$i."Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND (tb1.intStatus>1) AND (tb1.intStatus<=tb1.intApproveLevels) AND ((SELECT
								max(ware_storestransferheader_approvedby.dtApprovedDate) 
								FROM
								ware_storestransferheader_approvedby
								Inner Join sys_users ON ware_storestransferheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_storestransferheader_approvedby.intTransfNo = tb1.intTransfNo AND
								ware_storestransferheader_approvedby.intYear =  tb1.intTransfYear AND
								ware_storestransferheader_approvedby.intApproveLevelNo =  ($i-1) AND 
								ware_storestransferheader_approvedby.intStatus='0' )<>'')),
								
								/*end if condition*/
								/*if true part*/
								'Approve',
								/*fase part*/
								 if($i>tb1.intApproveLevels,'-----',''))
								
								/*end IFNULL false part*/
								) as `".$approval."`, "; 
									
								}
								
							$sql .= "'View' as `View`   
						FROM
							ware_storestransferheader as tb1
							INNER JOIN ware_storesrequesitionheader SRH ON SRH.intRequisitionNo=tb1.intRequisitionNo AND SRH.intRequisitionYear=tb1.intRequisitionYear
							INNER JOIN mst_substores SSRT ON SSRT.intId=SRH.intReqToStores 	
							INNER JOIN mst_substores SSRF ON SSRF.intId=SRH.intReqFromStores
							Inner Join sys_users ON tb1.intUser = sys_users.intUserId 
							Inner Join mst_locations ON tb1.intCompanyId = mst_locations.intId 
							WHERE
							mst_locations.intCompanyId =  '$company' AND 
							tb1.intCompanyId =  '$location'
							)  as t where 1=1
						";
						 
$col = array();

//STATUS
$col["title"] 	= "Status"; // caption of column
$col["name"] 	= "Status"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
//edittype
$col["stype"] 	= "select";
$str = ":All;Pending:Pending;Approved:Approved;Rejected:Rejected" ;
$col["editoptions"] =  array("value"=> $str);
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$cols[] = $col;	$col=NULL;

//PO No
$col["title"] 	= "STN No"; // caption of column
$col["name"] 	= "STNNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "2";
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$col['link']	= "../addNew/storesTransferNote.php?transfNo={STNNo}&year={STNYear}";	 
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

$reportLink  = "rptStoresTransferNote.php?transfNo={STNNo}&year={STNYear}";
$reportLinkApprove  = "rptStoresTransferNote.php?transfNo={STNNo}&year={STNYear}&approveMode=1";
//PO Year
$col["title"] = "STN Year"; // caption of column
$col["name"] = "STNYear"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

$col["title"] 	= "SRN No"; // caption of column
$col["name"] 	= "SRNNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "2";
$col["align"] 	= "center";
$cols[] = $col;	$col=NULL;

//PO Year
$col["title"] = "SRN Year"; // caption of column
$col["name"] = "SRNYear"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//PO Year
$col["title"] = "SRN From"; // caption of column
$col["name"] = "srnFrom"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;



//PO Year
$col["title"] = "SRN To"; // caption of column
$col["name"] = "srnTo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Delivery Date
$col["title"] = "Note"; // caption of column
$col["name"] = "Note"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "7";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;


//Supplier
$col["title"] = "Date"; // caption of column
$col["name"] = "Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//GRN Qty
$col["title"] = "STN Qty"; // caption of column
$col["name"] = "STN_Qty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;


//User
$col["title"] = "User"; // caption of column
$col["name"] = "User"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;



//FIRST APPROVAL
$col["title"] = "1st Approval"; // caption of column
$col["name"] = "1st_Approval"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

for($i=2; $i<=$approveLevel; $i++){
	if($i==2){
	$ap="2nd Approval";
	$ap1="2nd_Approval";
	}
	else if($i==3){
	$ap="3rd Approval";
	$ap1="3rd_Approval";
	}
	else {
	$ap=$i."th Approval";
	$ap1=$i."th_Approval";
	}
//SECOND APPROVAL
$col["title"] = $ap; // caption of column
$col["name"] = $ap1; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;
}



//VIEW
$col["title"] = "Report"; // caption of column
$col["name"] = "View"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

$jq = new jqgrid('',$db);

$grid["caption"] 		= "STN Listing";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'SRNNo'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes


// export XLS file
// export to excel parameters - range could be "all" or "filtered"
//$grid["export"] = array("format"=>"xlsx", "filename"=>"my-file", "sheetname"=>"test");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);




$out = $jq->render("list1");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>STN Listing</title>

<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />


</head>

<body>	
<form id="frmlisting" name="frmlisting" method="post" action="">
<table width="100%" border="0" align="center">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
	<link rel="stylesheet" type="text/css" media="screen" href="../../../../libraries/jqdrid/js/themes/smoothness/jquery-ui.custom.css"></link>	
	<link rel="stylesheet" type="text/css" media="screen" href="../../../../libraries/jqdrid/js/jqgrid/css/ui.jqgrid.css"></link>	
	
	<script src="../../../../libraries/jqdrid/js/jquery.min.js" type="text/javascript"></script>
	<script src="../../../../libraries/jqdrid/js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
	<script src="../../../../libraries/jqdrid/js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>	
	<script src="../../../../libraries/jqdrid/js/themes/jquery-ui.custom.min.js" type="text/javascript"></script>
	<script src="../../../../libraries/javascript/script.js" type="text/javascript"></script>
    
    <td><table width="100%" border="0">
      <tr>
        <td>
        <div align="center" style="margin:10px">
        
			<?php echo $out?>
        </div>
        </td>
      </tr>

    </table></td>
    </tr>


</form>
</body>
</html>
<?php

//------------------------------function load Default Department---------------------
function getMaxApproveLevel(){
	global $db;	
	//echo $savedStat;
	$appLevel=0;
	$sqlp = "SELECT
			Max(ware_storestransferheader.intApproveLevels) AS appLevel
			FROM ware_storestransferheader";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['appLevel'];
}
?>


<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	///
	/////////// type of print load part /////////////////////
if($requestType=='loadsrnNos')
	{
		$srnYear  = $_REQUEST['srnYear'];
		
		
		$sql = "select * from (SELECT
							ware_storesrequesitionheader.intRequisitionNo, 
sum(ware_storesrequesitiondetails.dblQty) as reqQty, 
sum(ware_storesrequesitiondetails.dblTransferQty) as transfQty 

							FROM ware_storesrequesitionheader 
							Inner Join ware_storesrequesitiondetails ON 
							ware_storesrequesitionheader.intRequisitionNo = ware_storesrequesitiondetails.intRequisitionNo 
							AND ware_storesrequesitionheader.intRequisitionYear = ware_storesrequesitiondetails.intRequisitionYear
							WHERE
				ware_storesrequesitionheader.intStatus =  '1' AND 
				ware_storesrequesitionheader.intLocationId =  '$location' ";
		//if($srnYear!=''){		
		$sql .= " AND ware_storesrequesitionheader.intRequisitionYear =  '$srnYear' ";	
		//}
		$sql .= " GROUP BY 
							ware_storesrequesitiondetails.intRequisitionNo,
							ware_storesrequesitiondetails.intRequisitionYear
							order by intRequisitionNo desc ) as tb1 where  tb1.reqQty-tb1.transfQty >0";
		
	 	//echo $sql;
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intRequisitionNo']."\">".$row['intRequisitionNo']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
else if($requestType=='loadSubCategory')
	{
		$mainCategory  = $_REQUEST['mainCategory'];
		$sql = "SELECT
				mst_subcategory.intId,
				mst_subcategory.strCode,
				mst_subcategory.strName
				FROM mst_subcategory
				WHERE
				mst_subcategory.intMainCategory =  '$mainCategory' 
				Order by mst_subcategory.strName ASC";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	else if($requestType=='loadFromToStores')
	{
		$srnYear  = $_REQUEST['srnYear'];
		$srnNo  = $_REQUEST['srnNo'];

		 $sql = "SELECT
				ware_storesrequesitionheader.intReqToStores,
				ware_storesrequesitionheader.intReqFromStores
				FROM `ware_storesrequesitionheader`
				WHERE
				ware_storesrequesitionheader.intRequisitionNo = $srnNo AND
				ware_storesrequesitionheader.intRequisitionYear = $srnYear";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$response['srnFrom'] 	= $row['intReqFromStores'];
		$response['srnTo'] = $row['intReqToStores'];
		echo json_encode($response);
	}
	else if($requestType=='loadItems')
	{
		$srnNo  = $_REQUEST['srnNo'];
		$year  = $_REQUEST['year'];
		$mainCategory  = $_REQUEST['mainCategory'];
		$subCategory  = $_REQUEST['subCategory'];
		$description  = $_REQUEST['description'];
		

		
		  $sql="select * from (SELECT
				mst_item.intId,
				mst_item.intMainCategory,
				mst_item.intSubCategory,
				mst_item.strCode,
				mst_item.strName as itemName,
				mst_item.intBomItem,
				mst_item.intUOM,
				mst_units.strCode as uom , 
				mst_item.dblLastPrice,
				mst_maincategory.strName as mainCatName,
				mst_subcategory.strName as subCatName,   
				tb1.dblQty,
				ware_storesrequesitionheader.intReqFromStores, 
				IFNULL((SELECT
				Sum(ware_storestransferdetails.dblQty)
				FROM
				ware_storestransferheader
				Inner Join ware_storestransferdetails ON ware_storestransferheader.intTransfNo = ware_storestransferdetails.intTransfNo AND ware_storestransferheader.intTransfYear = ware_storestransferdetails.intTransfYear
				WHERE
				ware_storestransferheader.intRequisitionNo =  ware_storesrequesitionheader.intRequisitionNo AND
				ware_storestransferheader.intRequisitionYear =  ware_storesrequesitionheader.intRequisitionYear AND
				ware_storestransferdetails.intItemId =  tb1.intItemId AND
				ware_storestransferheader.intStatus > '0' AND 
				ware_storestransferheader.intStatus <= ware_storestransferheader.intApproveLevels),0) as dblTransferQty 
				FROM
				ware_storesrequesitiondetails as tb1   
				Inner Join ware_storesrequesitionheader ON tb1.intRequisitionNo = ware_storesrequesitionheader.intRequisitionNo 
				AND tb1.intRequisitionYear = ware_storesrequesitionheader.intRequisitionYear
				Inner Join mst_item ON tb1.intItemId = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
				WHERE 
				ware_storesrequesitionheader.intStatus = '1' AND  
				ware_storesrequesitionheader.intLocationId =  '$location' ";
				$sql.=" AND tb1.intRequisitionNo =  '$srnNo' ";
				$sql.=" AND tb1.intRequisitionYear =  '$year' ";
				
				if($mainCategory!='')
				$sql.=" AND mst_item.intMainCategory =  '$mainCategory' ";
				if($subCategory!='')
				$sql.=" AND mst_item.intSubCategory =  '$subCategory'";
				if($description!=''){
				$sql.=" AND
				mst_item.strName LIKE  '%$description%'";
				}
				$sql.=" group by mst_item.intId) as tb1 where tb1.dblQty-tb1.dblTransferQty>0";	
				$sql.=" Order By tb1.mainCatName asc, 
						tb1.subCatName asc, 
						tb1.itemName asc 
						";	

	 	 //  echo $sql;
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			
			$reqFromStores = $row['intReqFromStores'];
			$reqQty = $row['dblQty'];
			if(!$gpQty)
			$gpQty=0;
			$transfQty = $row['dblTransferQty'];
			if(!$transfQty)
			$transfQty=0;
			
			$stockBalQty=getStockBalance_bulk($reqFromStores,$row['intId']);
			$data['itemId'] 	= $row['intId'];
			$data['maincatId'] = $row['intMainCategory'];
			$data['subCatId'] 	= $row['intSubCategory'];
			$data['code'] = $row['strCode'];
			$data['itemName'] = $row['itemName'];
			$data['uom'] 	= $row['uom'];
			$data['unitPrice'] = $row['dblLastPrice'];
			$data['mainCatName'] = $row['mainCatName'];
			$data['subCatName'] = $row['subCatName'];
			$data['reqQty'] = $reqQty;
			$data['transfQty'] = $transfQty;
			$data['stockBalQty'] = $stockBalQty;
			
			$arrCombo[] = $data;
		}
		$response['arrCombo'] 	= $arrCombo;
		echo json_encode($response);

	}
	
	//-----------------------------------------------------------
	function getStockBalance($company,$location,$orderNo,$orderYear,$style,$item)
	{
		global $db;
		  $sql = "SELECT
				Sum(ware_stocktransactions.dblQty) as stockBal 
				FROM ware_stocktransactions
				WHERE
				ware_stocktransactions.intCompanyId =  '$company' AND
				ware_stocktransactions.intLocationId =  '$location' AND
				ware_stocktransactions.intOrderNo =  '$orderNo' AND
				ware_stocktransactions.intOrderYear =  '$orderYear' AND
				ware_stocktransactions.strStyleNo =  '$style' AND
				ware_stocktransactions.intItemId =  '$item'";

		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
	function getStockBalance_bulk($reqFromStores,$item)
	{
		global $db;
		   $sql = "SELECT
				ROUND(IFNULL(SUM(ware_stocktransactions_bulk.dblQty),0),2) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND 
				ware_stocktransactions_bulk.intLocationId = (SELECT intLocation FROM mst_substores WHERE intId='$reqFromStores')
				GROUP BY
				ware_stocktransactions_bulk.intItemId";

		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return ($row['stockBal']==''?0:$row['stockBal']);	
	}
	//--------------------------------------------------------------
?>
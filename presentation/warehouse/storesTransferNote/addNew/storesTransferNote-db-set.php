<?php 
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$companyId 	= $_SESSION['CompanyID'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	
	$serialNo 	 = $_REQUEST['serialNo'];
	$year 		 = $_REQUEST['Year'];
	$srnNo 	 = $_REQUEST['srnNo'];
	$srnYear 	 = $_REQUEST['srnYear'];
	$note 	 = $_REQUEST['note'];
	$date 		 = $_REQUEST['date'];
	
	$arr 		= json_decode($_REQUEST['arr'], true);
	
	$programName='Stores Transfer Note';
	$programCode='P0258';

	$ApproveLevels = (int)getApproveLevel($programName);
	$transfApproveLevel = $ApproveLevels+1;
	
//------------save---------------------------	
	if($requestType=='save')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		$savableFlag= 1;
		
		if($serialNo==''){
			$serialNo 	= getNextTransfNo();
			$year = date('Y');
			$editMode=0;
			$savedStatus=$transfApproveLevel;
			$savedLevels=$ApproveLevels;
		}
		else{
			$savableFlag=getSaveStatus($serialNo,$year);
			$editMode=1;
			$sql = "SELECT
			ware_storestransferheader.intStatus, 
			ware_storestransferheader.intApproveLevels , 
			FROM ware_storestransferheader 
			WHERE
			ware_storestransferheader.intTransfNo =  '$serialNo' AND
			ware_storestransferheader.intTransfYear =  '$year'";

			$result = $db->RunQuery2($sql);
			$row=mysqli_fetch_array($result);
			$savedStatus=$row['intStatus'];
			$savedLevels=$row['intApproveLevels'];
		}
		
		//--------------------------
		//$response['arrData'] = $objwhouseget->getLocationValidation('TRANSFERIN',$companyId,$srnNo,$srnYear);//check for grn location
		//--------------------------
		$editPermission=loadEditMode($programCode,$savedStatus,$savedLevels,$userId);
		$locationEditFlag=getEditLocationValidation('TRANSFERIN',$companyId,$serialNo,$year);
		//--------------------------
		
		if($response['arrData']['type']=='fail'){
			 $rollBackFlag = 1;
			 $rollBackMsg = $response['arrData']['msg'];
		 }
		else if($savableFlag==0){
			 $rollBackFlag=1;
			 $rollBackMsg="This Stores Transfer In No is already confirmed.cant edit";
		 }
		 else if($editPermission==0){
			 $rollBackFlag=1;
			 $rollBackMsg="No Permission to save.";
		 }
		else if($editMode==1){//edit existing grn
		$sql = "UPDATE `ware_storestransferheader` SET intStatus ='$transfApproveLevel', 
												       intApproveLevels ='$ApproveLevels', 
												       strNote ='$note', 
													  intModifiedBy ='$userId', 
													  dtmModifiedDate =now() 
				WHERE (`intTransfNo`='$serialNo') AND (`intTransfYear`='$year')";
		$result = $db->RunQuery2($sql);
		}
		else{
			$sql = "INSERT INTO `ware_storestransferheader` (`intTransfNo`,`intTransfYear`,intRequisitionNo,intRequisitionYear,strNote,intStatus,intApproveLevels,datdate,dtmCreateDate,intUser,intCompanyId) 
					VALUES ('$serialNo','$year','$srnNo','$srnYear','$note','$transfApproveLevel','$ApproveLevels',now(),now(),'$userId','$companyId')";
			$result = $db->RunQuery2($sql);
			$maxAppByStatus=(int)getMaxAppByStatus($serialNo,$year)+1;
			$sql = "UPDATE `ware_storestransferheader_approvedby` SET intStatus ='$maxAppByStatus' 
					WHERE (`intTransfNo`='$serialNo') AND (`intYear`='$year') AND (`intStatus`='0')";
			$result1 = $db->RunQuery2($sql);
		}
		//-----------delete and insert to detail table-----------------------
		if(($result) && ($rollBackFlag!=1)){
		$sql = "DELETE FROM `ware_storestransferdetails` WHERE (`intTransfNo`='$serialNo') AND (`intTransfYear`='$year')";
		$result2 = $db->RunQuery2($sql);
			$toSave=0;
			$saved=0;
			$rollBackFlag=0;
			$rollBackMsg ="Maximum Qtys for items are..."; 
			foreach($arr as $arrVal)
			{
				$location 	= $_SESSION['CompanyID'];
				$company 	= $_SESSION['headCompanyId'];
				$itemId 	 = $arrVal['itemId'];
				$Qty 		 = $arrVal['Qty'];
		

				//------check maximum ISSUE Qty--------------------
				 $sqlc = "SELECT 
				mst_item.strName as itemName,
				tb1.intRequisitionNo,
				tb1.intRequisitionYear,
				ware_storesrequesitionheader.intReqFromStores as stores,
				tb1.dblQty,
				tb1.intItemId,
				IFNULL((SELECT
				Sum(ware_storestransferdetails.dblQty)
				FROM
				ware_storestransferheader
				Inner Join ware_storestransferdetails ON ware_storestransferheader.intTransfNo = ware_storestransferdetails.intTransfNo AND ware_storestransferheader.intTransfYear = ware_storestransferdetails.intTransfYear
				WHERE
				ware_storestransferheader.intRequisitionNo =  tb1.intRequisitionNo AND
				ware_storestransferheader.intRequisitionYear =  tb1.intRequisitionYear AND
				ware_storestransferdetails.intItemId =  tb1.intItemId AND
				ware_storestransferheader.intStatus > '0' AND 
				ware_storestransferheader.intStatus <= ware_storestransferheader.intApproveLevels),0) as dblTransferQty
				FROM
				ware_storesrequesitiondetails as tb1 
				Inner Join ware_storesrequesitionheader ON tb1.intRequisitionNo = ware_storesrequesitionheader.intRequisitionNo 
				AND tb1.intRequisitionYear = ware_storesrequesitionheader.intRequisitionYear
				Inner Join mst_item ON tb1.intItemId = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
				WHERE 
				ware_storesrequesitionheader.intStatus = '1' AND 
				ware_storesrequesitionheader.intRequisitionNo =  '$srnNo' AND 
				ware_storesrequesitionheader.intRequisitionYear =  '$srnYear' AND 
				tb1.intItemId =  '$itemId' ";
				$resultsc = $db->RunQuery2($sqlc);
				$rowc = mysqli_fetch_array($resultsc);
				$item=$rowc['itemName'];
				$stores=$rowc['stores'];
				$itemId = $rowc['intItemId'];

				//if($rowc['dblQty']-$rowc['dblIssudQty']<$stockBalQty)
				$balQtyToTransf = $rowc['dblQty']-$rowc['dblTransferQty'];
				if($editMode==1){
					 $balQtyToTransf=$balQtyToTransf+$Qty;
				//	$balQtyToTransf +=	$Qty;
				}
			//	else
				//$balQtyToIssue = $stockBalQty;
				$stockBalQty = getStockBalance_bulk($stores,$itemId);
				if($balQtyToTransf>$stockBalQty){
					$balQtyToTransf=$stockBalQty;
				}
				
				//$balQtyToTransf=200;
				if($Qty>$balQtyToTransf){
				//	call roll back--------****************
					$rollBackFlag=1;
					$rollBackMsg .="\n "."-".$item." =".$balQtyToTransf;
				}
				//----------------------------
				if($rollBackFlag!=1){
				if($orderNoArray[0]==''){
					$orderNoArray[0]=0;
				}
				if($orderNoArray[1]==''){
					$orderNoArray[1]=0;
				}
					$Qty=round($Qty,4);
					$sql = "INSERT INTO `ware_storestransferdetails` (`intTransfNo`,`intTransfYear`,`intItemId`,`dblQty`) 
					VALUES ('$serialNo','$year','$itemId','$Qty')";
					$result3 = $db->RunQuery2($sql);
					if($result3==1){
					$saved++;
					}
				}
				$toSave++;
			}
		}
		//echo $rollBackFlag;
		
		//$rollBackFlag=1;
		
		if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= '';
		}
		else if(($savableFlag==1) &&(!$result)){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= "Header saving error";
			$response['q'] 			= $sql;
		}
		else if(($savableFlag==1) &&($toSave==0) &&($saved==0)){
			$response['type'] 		= 'fail';
			$response['msg'] 		= "Details saving error";
			$response['q'] 			= '';
		}
		else if(($result) && ($toSave==$saved)){
			$confirmationMode=getConfirmationMode($programCode,$userId,$ApproveLevels,$transfApproveLevel);
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			if($editMode==1)
			$response['msg'] 		= 'Updated successfully.';
			else
			$response['msg'] 		= 'Saved successfully.';
			
			$response['serialNo'] 		= $serialNo;
			$response['year'] 		= $year;
			$response['confirmationMode'] = $confirmationMode;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		
		$db->CloseConnection();		
	}
//----------------------------------------
	echo json_encode($response);
//-----------------------------------------	
	function getNextTransfNo()
	{
		global $db;
		global $companyId;
		 $sql = "SELECT
				sys_no.intSTNNo
				FROM sys_no
				WHERE
				sys_no.intCompanyId =  '$companyId'
				";	
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$nextTransfNo = $row['intSTNNo'];
		
		$sqlUpd = "UPDATE `sys_no` SET intSTNNo=intSTNNo+1 WHERE (`intCompanyId`='$companyId') ";
		$db->RunQuery2($sqlUpd);
		
		return $nextTransfNo;
	}
	//-----------------------------------------------------------
	function getStockBalance($company,$location,$orderNo,$orderYear,$salesOrderId,$item)
	{
		global $db;
		 $sql = "SELECT
				Sum(ware_stocktransactions.dblQty) as stockBal 
				FROM ware_stocktransactions
				WHERE
				ware_stocktransactions.intCompanyId =  '$company' AND 
				ware_stocktransactions.intLocationId =  '$location' AND
				ware_stocktransactions.intItemId =  '$item'";

		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
	function getStockBalance_bulk($stores,$item)
	{
		global $db;
		   $sql = "SELECT
					SUM(ware_stocktransactions_bulk.dblQty) AS stockBal
					FROM ware_stocktransactions_bulk
					WHERE
					ware_stocktransactions_bulk.intItemId =  '$item' AND 
					ware_stocktransactions_bulk.intLocationId = (SELECT intLocation FROM mst_substores WHERE intId='$stores')
					GROUP BY
					ware_stocktransactions_bulk.intItemId";
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
	function getSaveStatus($serialNo,$year)
	{
		global $db;
		$sql = "SELECT ware_storestransferheader.intStatus, ware_storestransferheader.intApproveLevels FROM ware_storestransferheader WHERE (intTransfNo='$serialNo') AND (`intTransfYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
			
			if(($status==0) || ($row['intStatus']==($row['intApproveLevels']+1))){
				$editableFlag=1	;
			}
			else{
				$editableFlag=0;	
			}
			
		return $editableFlag;
	}
//-----------------------------------------------------------
	function getEditLocationValidation($type,$location,$serialNo,$year)
	{
		$flag= 1;
		
		global $db;
		  $sql = "SELECT
					ware_storesrequesitionheader.intLocationId
					FROM
					ware_storesrequesitionheader
					Inner Join mst_locations ON ware_storesrequesitionheader.intLocationId = mst_locations.intId
					WHERE
					ware_storesrequesitionheader.intTransfNo =  '$serialNo' AND
					ware_storesrequesitionheader.intTransfYear =  '$year'";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			if($location == $row['intLocationId'])
			$flag=0;
 			
		return $flag;

}
//------------------------------function loadConfirmatonMode-------------------
function getConfirmationMode($programCode,$intUser,$approveLevels,$status){
		global $db;
		$k=$approveLevels+2-$status;
		$sqlp = "SELECT 
		menupermision.intEdit, 
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		if($rowp['int'.$k.'Approval']==1) 
		 $confirmatonMode=1; 
		else
		 $confirmatonMode=0; 
	 
	return $confirmatonMode;
}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
 	 	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		$resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1)
		 {
			 if($intStatus==($savedStat+1) || ($intStatus==0))
			 { 
				 $editMode=1;
			 }
		 }
		 
	//	echo $editMode;
		 
	return $editMode;
}
//------------------------------function get MaxAppByStatus---------------------
function getMaxAppByStatus($serialNo,$year){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	  $sql = "SELECT
				max(ware_storestransferheader_approvedby.intStatus) as status 
				FROM
				ware_storestransferheader_approvedby
				WHERE
				ware_storestransferheader_approvedby.intTransfNo =  '$serialNo' AND
				ware_storestransferheader_approvedby.intYear =  '$year'";
				
		$resultm = $db->RunQuery2($sql);
		 $rowm=mysqli_fetch_array($resultm);
		 
		 return $rowm['status'];

}
//--------------------------------------------------------getMaxAppByStatus

?>



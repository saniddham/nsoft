			
$(document).ready(function() {
	
  		$("#frmStoresTransferNote").validationEngine();
		$('#butAdd').live('click',addClickedRows);
		$('#butClose1').live('click',disablePopup);
		
		$("#butAddItems").click(function(){
			if(!$('#frmStoresTransferNote #cboSRNno').val()){
				alert("Please select the SRN No");
				return false;				
			}
			else if(!$('#frmStoresTransferNote #cboSRNYear').val()){
				alert("Please select the Year");
				return false;				
			}
			//clearRows();
			closePopUp();
			loadPopup();
		});

		$("#cboSRNYear").change(function(){
			loadSRNos();
		});
		$("#cboSRNno").change(function(){
			loadFromToStores();
		});
		
	//--------------------------------------------
		
  //permision for add 
  if(intAddx)
	{	
		if(intStatus!='' && approvalLevels!='')
		{
			if((intStatus>approvalLevels) || intStatus==0)
			{
				$('#frmStoresTransferNote #butNew').show();
				$('#frmStoresTransferNote #butSave').show();
			}
		}
		else
		{
			$('#frmStoresTransferNote #butNew').show();
			$('#frmStoresTransferNote #butSave').show();
		}
	}
  
  //permision for edit 
  if(intEditx)
	{	
		if(intStatus!='' && approvalLevels!='')
		{
			if((intStatus>approvalLevels) || intStatus==0)
			{
				$('#frmStoresTransferNote #butSave').show();
			}
		}
		else
		{
			$('#frmStoresTransferNote #butSave').show();
		}
	}
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmStoresTransferNote #butDelete').show();
  }
  
  //permision for view
  if(intViewx)
  {
	//$('#frmStoresTransferNote #cboSearch').removeAttr('disabled');
  }
  
  //-------------------------------------------------------
 
  $('#frmStoresTransferNote #butSave').click(function(){
	var requestType = '';
	if ($('#frmStoresTransferNote').validationEngine('validate'))   
    { 

		var data = "requestType=save";
		
			data+="&serialNo="+$('#txtTransNo').val();
			data+="&Year="+$('#txtTransYear').val();
			data+="&srnNo="+$('#cboSRNno').val();
			data+="&srnYear="+$('#cboSRNYear').val();
			data+="&note="+$('#txtNote').val();
			data+="&date="+$('#dtDate').val();


			var rowCount = document.getElementById('tblMain').rows.length;
			if(rowCount==1){
				alert("items not selected to Transferin");
				return false;				
			}
			var row = 0;
			var errorQtyFlag=0;
			
			var arr="[";
			
			$('#tblMain .item').each(function(){
	
				var itemId	= $(this).attr('id');
				var Qty	= $(this).parent().find(".Qty").val();
					
					if(Qty>0){
						
						arr += "{";
						arr += '"itemId":"'+	itemId +'",' ;
						arr += '"Qty":"'+		Qty +'"' ;
						arr +=  '},';
						
					}
						if(Qty<=0){
							errorQtyFlag=1;
						}
			});
			if(errorQtyFlag==1){
				alert("Please Enter Quantities");
				return false;
			}
			
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;

		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = "storesTransferNote-db-set.php";
     	var obj = $.ajax({
			url:url,
			type:'post',
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmStoresTransferNote #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						//$('#frmSampleInfomations').get(0).reset();
						//saveImage(json.sampleNo,json.sampleYear,json.revisionNo);
						//loadCombo_frmSampleInfomations();
						var t=setTimeout("alertx()",1000);
						$('#txtTransNo').val(json.serialNo);
						$('#txtTransYear').val(json.year);
						if(json.confirmationMode==1){
							$('#butConfirm').show();
						}
						else{
							$('#butConfirm').hide();
						}
						return;
					}
				},
			error:function(xhr,status){
					
					$('#frmStoresTransferNote #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
   
	
	//--------------refresh the form----------
	$('#frmStoresTransferNote #butNew').click(function()
	{
		window.location.href = "storesTransferNote.php";
	});
	//----------------------------------------
	
//----------------------------	
$('.delImg').click(function(){
	$(this).parent().parent().remove();
});

//----------------------------	
//-----------------------------------
$('#butReport').click(function(){
	if($('#txtTransNo').val()!=''){
		window.open('../listing/rptStoresTransferNote.php?transfNo='+$('#txtTransNo').val()+'&year='+$('#txtTransYear').val());	
	}
	else{
		alert("There is no Transfer IN No to view");
	}
});
//----------------------------------	
$('#butConfirm').click(function(){
	if($('#txtTransNo').val()!=''){
		window.open('../listing/rptStoresTransferNote.php?transfNo='+$('#txtTransNo').val()+'&year='+$('#txtTransYear').val()+'&approveMode=1');	
	}
	else{
		alert("There is no Transfer IN No to confirm");
	}
});
//-----------------------------------------------------
$('#butClose').click(function(){
		//load('main.php');	
});
});

//----------end of ready -------------------------------

//-------------------------------------------------------
function loadPopup()
{
		closePopUp();
		popupWindow3('1');
		var srnNo = $('#cboSRNno').val();
		var year = $('#cboSRNYear').val();
		$('#popupContact1').load('storesTransferNotePopup.php?srnNo='+srnNo+"&year="+year,function(){
				 //-------------------------------------------- 
				  $('#frmStoresTransferNotePopup #cboOrderNo').change(function(){
						var orderNo = $('#cboOrderNo').val();
						var url 		= "gatepassTransferIn-db-get.php?requestType=loadStyleNo&orderNo="+orderNo;
						var httpobj 	= $.ajax({url:url,async:false})
						document.getElementById('cbStyleNo').innerHTML=httpobj.responseText;
						
				  });
				 //-------------------------------------------- 
				  $('#frmStoresTransferNotePopup #cboMainCategory').change(function(){
						var mainCategory = $('#cboMainCategory').val();
						var url 		= "storesTransferNote-db-get.php?requestType=loadSubCategory&mainCategory="+mainCategory;
						var httpobj 	= $.ajax({url:url,async:false})
						document.getElementById('cboSubCategory').innerHTML=httpobj.responseText;
						
				  });
				 //-------------------------------------------- 
				  $('#frmStoresTransferNotePopup #imgSearchItems').click(function(){
						var rowCount = document.getElementById('tblItemsPopup').rows.length;
						for(var i=1;i<rowCount;i++)
						{
							document.getElementById('tblItemsPopup').deleteRow(1);
						}
						var srnNo = $('#cboSRNno').val();
						var year = $('#cboSRNYear').val();
						var mainCategory = $('#cboMainCategory').val();
						var subCategory = $('#cboSubCategory').val();
						var description = $('#txtItmDesc').val();
						var url 		= "storesTransferNote-db-get.php?requestType=loadItems";
						var httpobj = $.ajax({
							url:url,
							dataType:'json',
							data:"mainCategory="+mainCategory+"&subCategory="+subCategory+"&description="+description+"&srnNo="+srnNo+"&year="+year,
							async:false,
							success:function(json){

								var length = json.arrCombo.length;
								var arrCombo = json.arrCombo;


								for(var i=0;i<length;i++)
								{
									var itemId=arrCombo[i]['itemId'];	
									var maincatId=arrCombo[i]['maincatId'];	
									var subCatId=arrCombo[i]['subCatId'];	
									var code=arrCombo[i]['code'];	
									var itemName=arrCombo[i]['itemName'];	
									var uom=arrCombo[i]['uom'];	
									var unitPrice=arrCombo[i]['unitPrice'];	
									var mainCatName=arrCombo[i]['mainCatName'];
									var subCatName=arrCombo[i]['subCatName'];
									var reqQty=arrCombo[i]['reqQty'];
									var transfQty=arrCombo[i]['transfQty'];
									var balToTransfQty=parseFloat(reqQty)-parseFloat(transfQty);
									var stockBalQty=arrCombo[i]['stockBalQty'];
										
									var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF" id=""><input type="checkbox" id="chkDisp" class="clschkItem" /></td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'" class="mainCatNameP">'+mainCatName+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'" class="subCatNameP">'+subCatName+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="codeP">'+code+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="itemP">'+itemName+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+uom+'" class="uomP">'+uom+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+reqQty+'" class="reqQtyP"  style="display:none">'+reqQty+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+transfQty+'"  class="transfQtyeP" style="display:none">'+transfQty+'</td>';
									content +='<td style="text-align:right" bgcolor="#FFFFFF" id="'+stockBalQty+'"   class="stockBalQtyP" >'+stockBalQty+'</td></tr>';

									add_new_row('#frmStoresTransferNotePopup #tblItemsPopup',content);
								}
									checkAlreadySelected();

							}
						});
						
				  });
					  $('#tblItemsPopup #chkAll').live('click',function(){
						
							var status = false;
							if($('#chkAll').attr('checked'))
							{
								status = true;
							}
							else
							{
								status = false;
							}
							$('#tblItemsPopup .clschkItem').attr("checked",status);
					});
			});	
}
//----------------------------------------------------
function addClickedRows()
{
	var i=0;
	$('#tblItemsPopup .itemP').each(function(){
		i++;
		if((document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].checked==true) && (document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].disabled==false)){
			

			var itemId=$(this).attr('id');
			var maincatId=$(this).parent().find(".mainCatNameP").attr('id');
			var subCatId=$(this).parent().find(".subCatNameP").attr('id');
			var code=$(this).parent().find(".codeP").html();
			var uom=$(this).parent().find(".uomP").html();
			var itemName=$(this).parent().find(".itemP").html();
			var mainCatName=$(this).parent().find(".mainCatNameP").html();
			var subCatName=$(this).parent().find(".subCatNameP").html();
			
			var reqQty=parseFloat($(this).parent().find(".reqQtyP").html());
			var transfQty=parseFloat($(this).parent().find(".transfQtyeP").html());
			var stockBalance=parseFloat($(this).parent().find(".stockBalQtyP").html());
			
			var balToTransfQty=reqQty-transfQty;
			var maxTrnsfQty=balToTransfQty;
			if(stockBalance<maxTrnsfQty){
				maxTrnsfQty=stockBalance;
			}

			var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF"><img class="delImg" src="../../../../images/del.png" width="15" height="15" /></td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'" class="mainCatName">'+mainCatName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'" class="subCatName">'+subCatName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="item">'+itemName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+uom+'" class="uom">'+uom+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+reqQty+'" class="srnQty">'+reqQty+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+balToTransfQty+'" class="balToTransfQty">'+balToTransfQty+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id=""><input  id="'+maxTrnsfQty+'" class="validate[required,custom[number],max['+maxTrnsfQty+']] calculateValue Qty" style="width:80px;text-align:center" type="text" value="'+maxTrnsfQty+'" onkeyup="chekQty(this);"/></td>';
			content +='</tr>';
			
		
			add_new_row('#frmStoresTransferNote #tblMain',content);
		//----------------------------	
		$('.delImg').click(function(){
			$(this).parent().parent().remove();
		});
		
		//----------------------------
	}
	
	});
	disablePopup();
	closePopUp();
}
//-------------------------------------
function alertx()
{
	$('#frmStoresTransferNote #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmStoresTransferNote #butDelete').validationEngine('hide')	;
}

function closePopUp(){
	
}
//----------------------------------------------
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
//----------------------------------------------- 
function clearRows()
{
	//var rowCount = $('#tblDispatchPopup >tbody >tr').length;
	var rowCount = document.getElementById('tblGatepassTransferInItems').rows.length;
	//alert(rowCount);
	//var cellCount = document.getElementById('tblMain').rows[1].cells.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblGatepassTransferInItems').deleteRow(1);
			
	}
}
//------------------------------------------------------------
/*function checkAlreadySelected(){
	$('#tblGatepassTransferInItems .item').each(function(){

		var orderNo	= $(this).parent().find(".orderNo").html();
		var styleNo	= $(this).parent().find(".salesOrderNo").html();
		var itemId	= $(this).attr('id');

			var j=0;
			$('#tblItemsPopup .itemP').each(function(){
				j++;
				var orderNoP=$(this).parent().find(".orderNoP").attr('id');
				var styleNoP=$(this).parent().find(".salesOrderNoP").html();
				var itemIdP=$(this).attr('id');
				
			//alert(orderNo+"=="+orderNoP+"***"+styleNo+"=="+styleNoP+"***"+mrnNo+"=="+mrnNoP+"***"+itemId+"=="+itemIdP+"***")
				if((orderNo==orderNoP) && (styleNo==styleNoP) && (itemId==itemIdP)){
					document.getElementById('tblItemsPopup').rows[j].cells[0].childNodes[0].checked=true;
					document.getElementById('tblItemsPopup').rows[j].cells[0].childNodes[0].disabled=true;
				}
			});
	});
}*/
function checkAlreadySelected()
{	
	$('#tblMain .item').each(function(){
		var itemNo	= $(this).attr('id');
		
		$('#tblItemsPopup .itemP').each(function(){

			var itemNoP = $(this).attr('id');
			if((itemNo==itemNoP))
			{	
				$(this).parent().find('.clschkItem').attr("checked",true);
				$(this).parent().find('.clschkItem').attr("disabled",true);
			}
		});
	});
}
//------------------------------------------------------------
function loadSRNos(){
	var srnYear = $('#cboSRNYear').val();
	if(srnYear=='')
	{
		$('#cboTrnFrom').val('');
		$('#cboTrnTo').val('');
		$('#cboSRNno').html('');
	}
	else
	{
		var url 		= "storesTransferNote-db-get.php?requestType=loadsrnNos&srnYear="+srnYear;
		var httpobj 	= $.ajax({url:url,async:false})
		document.getElementById('cboSRNno').innerHTML=httpobj.responseText;
	}
}
 //-------------------------------------------- 
function loadFromToStores(){
	
	    var srnYear = $('#cboSRNYear').val();
	    var srnNo = $('#cboSRNno').val();
		var url 		= "storesTransferNote-db-get.php?requestType=loadFromToStores";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"srnYear="+srnYear+"&srnNo="+srnNo,
			async:false,
			success:function(json){
					$('#cboTrnFrom').val(json.srnFrom);
					$('#cboTrnTo').val(json.srnTo);
			}
		});
		
}
 //-------------------------------------------- 
function chekQty(obj)
{
	var srnQty   = parseFloat($(obj).parent().parent().find('.srnQty').html());
	
	if(obj.value>srnQty)
	{
		obj.value = parseFloat(srnQty);
	}
}
//-------------------------------------------- 
<?php 
session_start();
$backwardseperator 	= "../../../";
$mainPath 			= $_SESSION['mainPath'];
$userId 	 		= $_SESSION['userId'];
$locationId 		= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];
$requestType 		= $_REQUEST['requestType'];

include "{$backwardseperator}dataAccess/Connector.php";

$consumpDetails 	= json_decode($_REQUEST['consumpDetails'], true);

if($requestType=='saveData')
{
	$db->OpenConnection();
	$db->RunQuery2('Begin');
	
	$rollBackFlag 	= 0;
	$editMode		= 0;
	$saved			= 0;
	$toSave			= 0;
	$option 		= $_REQUEST['option'];
	$orderNo 		= $_REQUEST['orderNo'];
	$orderYear 		= $_REQUEST['orderYear'];
	$salesOrderId 		= $_REQUEST['salesOrderId'];
	$dtDate 		= $_REQUEST['dtDate'];
	$orderQty 		= $_REQUEST['orderQty'];
	$bucketWeight 	= $_REQUEST['bucketWeight'];
	
	foreach($consumpDetails as $arrDetails)
	{
		$graphicNo 		= $arrDetails['graphicNo'];
		$colorId 		= $arrDetails['colorId'];
		$technicId 		= $arrDetails['technicId'];
		$inkTypeId 		= $arrDetails['inkTypeId'];
		//$consumption 	= $arrDetails['consumption'];
		//$initialQty 	= $arrDetails['initialQty'];
		//$currQty 		= $arrDetails['currQty'];
		$Qty 		= $arrDetails['Qty'];
		$qty 		= $arrDetails['Qty'];
		$wastage 		= $arrDetails['wastage'];
		if($option==1){
			$qty=-1*$qty;
			$type='ISSUE';
		}
		if($option==2){
			$type='RETURN';
			$type2='WASTAGE';
		}
		
		$sqlChk  = "SELECT * FROM trn_inktype_production_allocation
					WHERE dtmDate='$dtDate' AND
					intOrderYear='$orderYear' AND 
					intOrderNo='$orderNo' AND 
					intSalesOrderId='$salesOrderId' AND 
					strGraphicNo='$graphicNo' AND
					intColorId='$colorId' AND
					intTechniqueId='$technicId' AND
					intInkTypeId='$inkTypeId' ";
		$resultChk = $db->RunQuery2($sqlChk);
		$count  = mysqli_num_rows($resultChk);
	 	$initialQty = stockBalance($colorId,$technicId,$inkTypeId,$orderNo,$orderYear,$salesOrderId);
		if(($option==1) && ($initialQty<$Qty))
		{
			$rollBackFlag = 1;
			$rollBackMsg  = "There is no Weight to issue.";
			break;
		}
		
		
		if($count>0)
		{
			if($option==1){
			$sql = "UPDATE trn_inktype_production_allocation 
					SET
					dblOrderQty = $orderQty ,
					dblIssueQty = dblIssueQty+$Qty,
					dtmModifiedDate = now() , 
					intModifiedBy = '$userId'
					
					WHERE
					dtmDate = '$dtDate' AND 
					intOrderYear = '$orderYear' AND 
					intOrderNo = '$orderNo' AND  
					intSalesOrderId='$salesOrderId' AND 
					strGraphicNo = '$graphicNo' AND 
					intColorId = '$colorId' AND 
					intTechniqueId = '$technicId' AND 
					intInkTypeId = '$inkTypeId' ";
			}
			else if($option==2){
			$sql = "UPDATE trn_inktype_production_allocation 
					SET
					dblOrderQty = $orderQty ,
					dblReturnedQty = dblReturnedQty+$Qty, 
					dblWastageQty = dblWastageQty+'$wastage' , 
					dtmModifiedDate = now() , 
					intModifiedBy = '$userId'
					
					WHERE
					dtmDate = '$dtDate' AND 
					intOrderYear = '$orderYear' AND 
					intOrderNo = '$orderNo' AND  
					intSalesOrderId='$salesOrderId' AND 
					strGraphicNo = '$graphicNo' AND 
					intColorId = '$colorId' AND 
					intTechniqueId = '$technicId' AND 
					intInkTypeId = '$inkTypeId' ";
			}
			$result = $db->RunQuery2($sql);
/*			if($result)
			{
				$editMode 	  = 1;
				
				$sqlStock = " UPDATE ware_sub_color_stocktransactions_bulk 
								SET
								dblQty = (-1*$consumption) ,  
								intUser = '$userId' , 
								dtDate = now()
								WHERE
								intCompanyId = '$companyId' AND
								intLocationId = '$locationId' AND
								intColorId = '$colorId' AND
								intTechnique = '$technicId' AND
								intInkType = '$inkTypeId' AND
								intOrderNo = '$orderNo' AND
								intOrderYear = '$orderYear' AND 
								intSalesOrderId='$salesOrderId' AND 
								strType = 'PROD_ALLOC' ";
				$resultStock = $db->RunQuery2($sqlStock);
				
				
				if($resultStock)
				{
					$saved++;
				}
				else
				{
					$rollBackFlag = 1;
					$rollBackMsg  = $db->errormsg;
				}
				
			}
			else
			{
				$rollBackFlag = 1;
				$rollBackMsg  = $db->errormsg;
			}
*/	

				if($result)
				{
					$saved++;
				}
				else
				{
					$rollBackFlag = 1;
					$rollBackMsg  = $db->errormsg;
				}
		}
		else
		{
			$chkStatus = chkStockValidate($initialQty,$colorId,$technicId,$inkTypeId,$orderNo,$orderYear,$salesOrderId);
		//	if($chkStatus)
		//	{
				if($option==1){
				$sql = "INSERT INTO trn_inktype_production_allocation 
						(dtmDate, intOrderYear, intOrderNo, intSalesOrderId,
						strGraphicNo, intColorId, intTechniqueId, 
						intInkTypeId, dblOrderQty, dblIssueQty, 
						dblReturnedQty, dblWastageQty, dtmCreateDate, 
						intCreateBy
						)
						VALUES
						('$dtDate', '$orderYear', '$orderNo', '$salesOrderId', 
						'$graphicNo', '$colorId', '$technicId', 
						'$inkTypeId', '$orderQty', '$Qty',
						'0', '0', now(), 
						'$userId'
						);";
					}
				if($option==2){
				$sql = "INSERT INTO trn_inktype_production_allocation 
						(dtmDate, intOrderYear, intOrderNo, intSalesOrderId,
						strGraphicNo, intColorId, intTechniqueId, 
						intInkTypeId, dblOrderQty, dblIssueQty, 
						dblReturnedQty, dblWastageQty, dtmCreateDate, 
						intCreateBy
						)
						VALUES
						('$dtDate', '$orderYear', '$orderNo', '$salesOrderId', 
						'$graphicNo', '$colorId', '$technicId', 
						'$inkTypeId', '$orderQty', '0',
						'$Qty', '$wastage', now(), 
						'$userId'
						);";
					}
						
						
				$result = $db->RunQuery2($sql);
					if($result)
					{
						$saved++;
					}
					else
					{
						$rollBackFlag = 1;
						$rollBackMsg  = $db->errormsg;
					}
				}
		//	}
/*			else
			{
				$rollBackFlag = 1;
				$rollBackMsg  = "Initial value must not exceed the Stock Qty.";
			}
*/
	if($saved>0){
					$editMode 	  = 0;
					
					if(abs($qty)>0){
					$sqlStock = "INSERT INTO ware_sub_color_stocktransactions_bulk 
								( 
								intCompanyId, intLocationId, intColorId, 
								intTechnique, intInkType, dblQty, 
								strType, intOrderNo, intOrderYear, intSalesOrderId,
								intUser, dtDate
								)
								VALUES
								(
								'$companyId', '$locationId', '$colorId', 
								'$technicId', '$inkTypeId', '$qty', 
								'$type', '$orderNo', '$orderYear','$salesOrderId', 
								'$userId', NOW()
								) ";
					$resultStock = $db->RunQuery2($sqlStock);
					}
					if($option==2){
						if($wastage>0){
						$sqlStock = "INSERT INTO ware_sub_color_stocktransactions_bulk 
									( 
									intCompanyId, intLocationId, intColorId, 
									intTechnique, intInkType, dblQty, 
									strType, intOrderNo, intOrderYear, intSalesOrderId,
									intUser, dtDate
									)
									VALUES
									(
									'$companyId', '$locationId', '$colorId', 
									'$technicId', '$inkTypeId', '-1*$wastage', 
									'$type2', '$orderNo', '$orderYear','$salesOrderId', 
									'$userId', NOW()
									) ";
						$resultStock = $db->RunQuery2($sqlStock);
						}
						}
								
								
}

	$toSave++; 
	}
	if($rollBackFlag==1)
	{
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $rollBackMsg;
	}
	else if(($rollBackFlag==0) && ($saved==$toSave))
	{
		$db->RunQuery2('Commit');
		$response['type'] 		= 'pass';
		if($editMode==1)
			$response['msg'] 	= 'Updated successfully.';
		else
			$response['msg'] 	= 'Saved successfully.';
	}
	else
	{
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $db->errormsg;
	}
	$db->CloseConnection();	
	echo json_encode($response);
}
function chkStockValidate($initialQty,$colorId,$technicId,$inkTypeId,$orderNo,$orderYear,$salesOrderId)
{
	global $locationId;
	global $db;
	
	$sqlStock ="SELECT IFNULL(SUM(dblQty),0) AS totQty FROM ware_sub_color_stocktransactions_bulk SCSB 
				WHERE SCSB.intColorId='$colorId' AND 
				SCSB.intTechnique='$technicId' AND 
				SCSB.intInkType='$inkTypeId' AND
				SCSB.intOrderNo='$orderNo' AND
				SCSB.intOrderYear='$orderYear' AND 
				SCSB.intSalesOrderId='$salesOrderId' AND 
				SCSB.intLocationId='$locationId' ";
	$resultStock = $db->RunQuery2($sqlStock);
	$rowStock 	 = mysqli_fetch_array($resultStock);
	
	if($initialQty<=$rowStock['totQty'])
		return true;
	else
		return false;
}
function stockBalance($colorId,$technicId,$inkTypeId,$orderNo,$orderYear,$salesOrderId)
{
	global $locationId;
	global $db;
	
	$sqlStock ="SELECT IFNULL(SUM(dblQty),0) AS totQty FROM ware_sub_color_stocktransactions_bulk SCSB 
				WHERE SCSB.intColorId='$colorId' AND 
				SCSB.intTechnique='$technicId' AND 
				SCSB.intInkType='$inkTypeId' AND
				SCSB.intOrderNo='$orderNo' AND
				SCSB.intOrderYear='$orderYear' AND 
				SCSB.intSalesOrderId='$salesOrderId' AND 
				SCSB.intLocationId='$locationId' ";
	$resultStock = $db->RunQuery2($sqlStock);
	$rowStock 	 = mysqli_fetch_array($resultStock);
	
		return $rowStock['totQty'];
}

?>
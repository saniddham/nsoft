<?php
session_start();
$backwardseperator = "../../../";
$mainPath = $_SESSION['mainPath'];
$location 	= $_SESSION['CompanyID'];
$company 	= $_SESSION['headCompanyId'];
$thisFilePath =  $_SERVER['PHP_SELF'];

//$formName		  	= 'frmItem';
//include  	"../../../customerAndOperation/sample/sampleDispatch/addNew/{$backwardseperator}dataAccess/permisionCheck.inc";
include $backwardseperator."dataAccess/Connector.php";
$poNo  = $_REQUEST['poNo'];
$poNArray = explode("/",$poNo);
//$poNo=$poNArray[0];
//$poYear=$poNArray[1];


$cboYear 		= $_REQUEST['orderYear'];
$cboOrderNo 	= $_REQUEST['orderNo'];
$cboGraphicNo 	= $_REQUEST['graphic'];
$cboSalesOrderNo = $_REQUEST['salesOrder'];
$dtDate			= $_REQUEST['date'];


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Items</title>
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />

<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $backwardseperator; ?>css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">


<script type="application/javascript" src="../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/script.js"></script>


</head>

<body>
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>

    
<form id="frmIssuePopup" name="frmIssuePopup" method="post" action="">
<table width="450" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutD">
		  <div class="trans_text">Issue</div>
		  <table width="550" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="550" border="0">
      <tr>
        <td>&nbsp;</td>
      </tr>
      <?php //echo $pp
	  ?>
      
      <tr>
        <td><div style="width:600px;height:300px;overflow:scroll" >
          <table width="100%" class="bordered" id="tblMain" >
                    <thead>
                     <tr>
                       <th width="15%" height="19" >Graphic No</th>
                       <th width="16%" height="19" >Color</th>
                       <th width="18%" height="19" >Technic Type</th>
                       <th width="28%" height="19" >Ink Type</th>
                       <th width="11%" height="19" >Issue</th>
                       <th width="12%" height="19" >Stock Bal</th>
                       
                       </tr>
					<?php
                    $sql = "SELECT OD.intOrderNo,
							SDT.intColorId,
							SID.intTechniqueId,
							SDT.intInkTypeId,
							OD.intOrderYear,
							OD.strGraphicNo,
							IFNULL(SDT.dblColorWeight,0) as dblColorWeight,
							MC.strName AS color,
							MT.strName AS technic,
							MI.strName AS inkType,
							OH.strCustomerPoNo,
							IFNULL((SELECT SUM(PAD.dblQty) FROM plan_actual_details PAD
							INNER JOIN plan_upload_header PUH ON PUH.intId=PAD.intPlanDetailId
							WHERE OH.strCustomerPoNo=PUH.strCustomerPONo AND PAD.intLocationId='$locationId'),0) AS actualQty,
							IFNULL((SELECT SUM(dblQty) FROM ware_sub_color_stocktransactions_bulk SCSB
							WHERE SCSB.intColorId=SDT.intColorId AND
							SCSB.intTechnique=SID.intTechniqueId AND
							SCSB.intInkType=SDT.intInkTypeId AND
							SCSB.intOrderNo=OD.intOrderNo AND
							SCSB.intOrderYear=OD.intOrderYear AND 
							SCSB.intSalesOrderId=OD.intSalesOrderId AND 
							SCSB.intLocationId='$locationId'),0) AS consumStock,
							IFNULL((SELECT (IPA.dblIssueQty-IPA.dblReturnedQty-IPA.dblWastageQty) FROM trn_inktype_production_allocation IPA
							WHERE IPA.intOrderNo=OD.intOrderNo AND 
							IPA.intOrderYear=OD.intOrderYear AND 
							IPA.intSalesOrderId =OD.intSalesOrderId AND 
							IPA.dtmDate='$dtDate' AND
							IPA.strGraphicNo=OD.strGraphicNo AND
							IPA.intColorId=SDT.intColorId AND
							IPA.intTechniqueId=SID.intTechniqueId AND
							IPA.intInkTypeId=SDT.intInkTypeId),0) AS consumption,
							IFNULL((SELECT sum((IPA.dblIssueQty-IPA.dblReturnedQty-IPA.dblWastageQty)) FROM trn_inktype_production_allocation IPA
							WHERE IPA.intOrderNo=OD.intOrderNo AND 
							IPA.intOrderYear=OD.intOrderYear AND 
							IPA.intSalesOrderId =OD.intSalesOrderId AND 
							IPA.strGraphicNo=OD.strGraphicNo AND
							IPA.intColorId=SDT.intColorId AND
							IPA.intTechniqueId=SID.intTechniqueId AND
							IPA.intInkTypeId=SDT.intInkTypeId),0) AS consumptionEnterd 
							/*(SELECT IFNULL(IPA.dblCurrentQty,0) FROM trn_inktype_production_allocation IPA WHERE 
							IPA.intOrderNo=OD.intOrderNo AND 
							IPA.intOrderYear=OD.intOrderYear AND 
							IPA.intSalesOrderId =OD.intSalesOrderId AND 
							IPA.strGraphicNo=OD.strGraphicNo AND 
							IPA.intColorId=SDT.intColorId AND 
							IPA.intTechniqueId=SID.intTechniqueId AND 
							IPA.intInkTypeId=SDT.intInkTypeId AND
							IPA.dtmDate='$dtDate') as currQty, 
							IF((SELECT IFNULL(SUM((IPA.dblIssueQty-IPA.dblReturnedQty-IPA.dblWastageQty)),0) AS totQty FROM trn_inktype_production_allocation IPA WHERE 
							IPA.intOrderNo=OD.intOrderNo AND 
							IPA.intOrderYear=OD.intOrderYear AND 
							IPA.intSalesOrderId =OD.intSalesOrderId AND 
							IPA.strGraphicNo=OD.strGraphicNo AND 
							IPA.intColorId=SDT.intColorId AND 
							IPA.intTechniqueId=SID.intTechniqueId AND 
							IPA.intInkTypeId=SDT.intInkTypeId AND
							IPA.dtmDate='$dtDate'
							HAVING(totQty)>0),
							 (SELECT IFNULL(IPA.dblInitialQty,0) FROM trn_inktype_production_allocation IPA WHERE 
							IPA.intOrderNo=OD.intOrderNo AND 
							IPA.intOrderYear=OD.intOrderYear AND 
							IPA.intSalesOrderId =OD.intSalesOrderId AND 
							IPA.strGraphicNo=OD.strGraphicNo AND 
							IPA.intColorId=SDT.intColorId AND 
							IPA.intTechniqueId=SID.intTechniqueId AND 
							IPA.intInkTypeId=SDT.intInkTypeId AND
							IPA.dtmDate='$dtDate'),
							(IFNULL((SELECT SUM(dblQty) FROM ware_sub_color_stocktransactions_bulk SCSB 
							WHERE SCSB.intColorId=SDT.intColorId AND 
							SCSB.intTechnique=SID.intTechniqueId AND 
							SCSB.intInkType=SDT.intInkTypeId AND
							SCSB.intOrderNo=OD.intOrderNo AND
							SCSB.intOrderYear=OD.intOrderYear AND 
							SCSB.intSalesOrderId =OD.intSalesOrderId AND 
							SCSB.intLocationId='$locationId'),0))) AS initialQty*/
							FROM trn_orderdetails OD
							INNER JOIN trn_orderheader OH ON OH.intOrderNo=OD.intOrderNo AND OH.intOrderYear=OD.intOrderYear
							INNER JOIN trn_sampleinfomations_details_technical SDT ON SDT.intSampleNo=OD.intSampleNo AND
							SDT.intSampleYear=OD.intSampleYear
							INNER JOIN trn_sampleinfomations_details SID ON SID.intSampleNo=SDT.intSampleNo AND
							SID.intSampleYear=SDT.intSampleYear AND
							SID.intRevNo=SDT.intRevNo AND
							SID.strPrintName=SDT.strPrintName AND
							SID.strComboName=SDT.strComboName AND
							SID.intColorId=SDT.intColorId
							INNER JOIN mst_colors MC ON MC.intId=SDT.intColorId
							INNER JOIN mst_techniques MT ON MT.intId=SID.intTechniqueId
							INNER JOIN mst_inktypes MI ON MI.intId=SDT.intInkTypeId
							WHERE OD.intOrderYear='$cboYear' AND OD.intOrderNo='$cboOrderNo' AND OD.intSalesOrderId='$cboSalesOrderNo' AND OH.intStatus=1 ";
					if($cboGraphicNo!="")
						$sql.="AND OD.strGraphicNo='$cboGraphicNo' ";
					if($cboStyleNo!="")
						$sql.="AND OD.strStyleNo='$cboStyleNo' ";
							
					$sql.="GROUP BY OD.intOrderNo,OD.intOrderYear,OD.intSalesOrderId,OD.strGraphicNo,SDT.intColorId,SID.intTechniqueId,SDT.intInkTypeId 
						   ORDER BY OD.intOrderNo";
						  // echo $sql;
					
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						$stockBal=getStockBal($row['intColorId'],$row['intTechniqueId'],$row['intInkTypeId'],$cboOrderNo,$cboYear,$cboSalesOrderNo,$location);
					?>
                    <tr>
                        <td class="normalfnt clsGrapicNo" id="<?php echo $row['strGraphicNo']; ?>"><?php echo $row['strGraphicNo']; ?></td>
                        <td class="normalfnt clsColor" id="<?php echo $row['intColorId']; ?>"><?php echo $row['color']; ?></td>
                        <td class="normalfnt clsTechnicType" id="<?php echo $row['intTechniqueId']; ?>"><?php echo $row['technic']; ?></td>
                        <td class="normalfnt clsInkType" id="<?php echo $row['intInkTypeId']; ?>"><?php echo $row['inkType']; ?></td>
                        <td style="text-align:center" class="normalfnt"><input type="text" name="txtQty" id="txtQty" style="width:40px; text-align:right" class="clsQty validate[custom[number],max[<?php echo ($stockBal); ?>]]"/></td>
                          <td style="text-align:center" class="normalfnt"><span class="normalfnt clsQtyEnterd" style="text-align:right"><?php echo round($stockBal,4); ?></span></td>
                        </tr>
                    <?php
					}
					?>
                    </thead>
                    <tbody>
                    </tbody>
                    </table>
          </div></td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound"><img src="../../../images/Tsave.jpg" width="92" height="24"  alt="save" id="butSave" name="butSave" class="mouseover"/><img src="../../../images/Tclose.jpg" width="92" height="24" id="butClose1" name="butClose1" class="mouseover"  /></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>

</body>
</html>
<?php
function getStockBal($colorId,$technicId,$inkTypeId,$orderNo,$orderYear,$salesOrderId,$locationId){
	global $db;
	$sqlStock ="SELECT IFNULL(SUM(dblQty),0) AS totQty FROM ware_sub_color_stocktransactions_bulk SCSB 
				WHERE SCSB.intColorId='$colorId' AND 
				SCSB.intTechnique='$technicId' AND 
				SCSB.intInkType='$inkTypeId' AND
				SCSB.intOrderNo='$orderNo' AND
				SCSB.intOrderYear='$orderYear' AND 
				SCSB.intSalesOrderId='$salesOrderId' AND 
				SCSB.intLocationId='$locationId' ";
	$resultStock = $db->RunQuery($sqlStock);
	$rowStock 	 = mysqli_fetch_array($resultStock);
	
	return $rowStock['totQty'];
	
}

?>
<?php
	//ini_set('display_errors',1);
	session_start();
	$backwardseperator = "../../../";
	$thisFilePath 	=  $_SERVER['PHP_SELF'];
	$locationId 	= $_SESSION['CompanyID'];
	$intUser  		= $_SESSION["userId"];
	$company 	= $_SESSION['headCompanyId'];

	//include  		"{$backwardseperator}dataAccess/permisionCheck.inc";
	include_once  		"{$backwardseperator}dataAccess/connector.php";
	require_once $_SESSION['ROOT_PATH']."class/warehouse/stores_requesition_note/cls_stores_requesition_note_get.php";
	require_once $_SESSION['ROOT_PATH']."class/cls_commonErrorHandeling_get.php";
	require_once $_SESSION['ROOT_PATH']."class/warehouse/cls_warehouse_get.php";
  
  	$obj_st_req_get	= new cls_stores_requesition_note_get($db);
	$obj_commonErr	= new cls_commonErrorHandeling_get($db);
	$obj_ware_get	= new cls_warehouse_get($db);
	
	$programName	= 'Stores Requisition Note';
	$programCode	= 'P0256';

	$serialNo		=$_REQUEST['SRNNo'];
	$serialYear		=$_REQUEST['SRNYear'];
	$curr_date		= date('Y-m-d');
   	
 	$row 			=	$obj_st_req_get->other_stores_requisition_header_select($serialNo,$serialYear,'RunQuery');
	$status			=	$row['intStatus'];
	$levels			=	$row['intApproveLevels'];
	$year_selected	=	$row['intOrderYear'];
	if($year_selected==''){
		$year_selected = date('Y');
	}
	
 	$reult_d		=	$obj_st_req_get->other_stores_requisition_details_result($serialNo,$serialYear,'RunQuery');
 	
	$order_selected1=	$row['strOrderNos'];
	$order_selected = explode(",", $order_selected1);

	
 	
 	$editPermition		=$obj_commonErr->get_permision_withApproval_save($status,$levels,$intUser,$programCode,'RunQuery');
	$editMode			=$editPermition['permision'];
 	$confirnPermition	=$obj_commonErr->get_permision_withApproval_confirm($status,$levels,$intUser,$programCode,'RunQuery');
	$confirmMode		=$confirnPermition['permision'];
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Production Issue (Ink) Note</title>
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/button.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $backwardseperator; ?>css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">
<script type="application/javascript" src="../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="productionAllocation-js.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/script.js"></script>
<!--<script type="application/javascript" src="../../../libraries/jquery/multi_select_combo.js"></script>
--><link rel="stylesheet" type="text/css" href="../../../libraries/calendar/theme.css" />
<script src="../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
</head>

<body>
<table width="100%" border="0" align="center">
  <tr>
    <td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
  </tr>
</table>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.js"></script> 
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.min.js"></script>
<link rel="stylesheet" href="../../../libraries/chosen/chosen/chosen.css" />

<!--<script type="text/javascript" src="../../../libraries/jquery/jquery-1.8.3-min.js"></script>
--> 
<script src="../../../libraries/chosen/chosen/prototype.js" type="text/javascript"></script> 
<script src="../../../libraries/chosen/chosen/chosen.proto.js" type="text/javascript"></script> 
<script src="../../../libraries/validate/jquery-1.js" type="text/javascript"></script> 
<script src="../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script> 
<script src="../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script> 
<script>
jQuery.noConflict();
</script>
<form id="frmStoresRequisition" name="frmStoresRequisition" autocomplete="off" action="stores_requesition_note.php" method="post">
  <div align="center">
    <div class="trans_layoutXL" style="width:950px"  align="center">
      <div class="trans_text">Ink - Production Issue Note </div>
      <table width="900" border="0" class="">
        <tr>
          <td  class="normalfnt" colspan="3" align="center"><fieldset class="tableBorder_allRound">
            <table width="100%" border="0" class="">
            
      <tr>
        <td colspan="4"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="tableBorder_allRound">
          <tr>
            <td width="1%" class="normalfnt">&nbsp;</td>
            <td width="6%" height="22" class="normalfnt">Issue No</td>
            <td width="22%"><input name="txtTransNo" type="text" disabled="disabled" class="txtText" id="txtTransNo" style="width:60px" value="<?php echo $transfNo; ?>" />&nbsp;<input name="txtTransYear" type="text" disabled="disabled" class="txtText" id="txtTransYear" style="width:40px" value="<?php echo $year; ?>" /></td>
            <td width="8%">&nbsp;</td>
            <td width="40%">&nbsp;</td>
            <td width="5%" class="normalfnt">Date</td>
            <td width="18%" align="left">&nbsp;<input name="dtDate" type="text" value="<?php echo(($Date)?$Date:date('Y-m-d')); ?>" class="txtbox" id="dtDate" style="width:120px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php if($editMode==0){  ?> disabled="disabled"<?php } ?>/></td>
            </tr>
          
        </table></td>
      </tr>
            
              <tr>
                <td colspan="4"><table width="100%" border="0" cellpadding="0" cellspacing="0"  class="tableBorder_allRound">
                  <tr>
                    <td width="1%" class="normalfnt">&nbsp;</td>
                    <td width="6%" height="22" class="normalfnt">Year</td>
                    <td width="12%"><select style="width:60px" name="cboOrderYear2" id="cboOrderYear2" class="validate[required]">
                      <?php echo $options = $obj_st_req_get->get_order_years_options($year_selected,'RunQuery') ?>
                      </select></td>
                    <td width="6%">Graphic</td>
                    <td width="53%"><select name="select2" class="txtCalled normalfnt multiSelect graphics validate[required]" style="width:200px; height:20px" tabindex="4" data-placeholder="" >
                      <?php
					$options = $obj_st_req_get->get_graphics_options($year_selected,$graphic_selected,'RunQuery');
					echo $options;
				?>
                      </select></td>
                    <td width="22%" rowspan="2"><div id="divPicture" class="tableBorder_allRound divPicture" align="center" contenteditable="true" style="width:184px;height:108px;overflow:hidden" >
                      <?php
			if($cboYear!='' && $cboOrderNo!='' && $cboSalesOrderNo!='')
			{
				echo "<img id=\"saveimg\" style=\"width:264px;height:148px;\" src=\"../../../documents/sampleinfo/samplePictures/$SampleNo-$SampleYear-$RevisionNo-".substr($PrintName,6,1).".png\" />";	
			}
			 ?>
                      </div></td>
                    </tr>
                  <tr>
                    <td class="normalfnt">&nbsp;</td>
                    <td height="22" class="normalfnt">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td width="53%">&nbsp;</td>
                    </tr>
                  </table></td>
              </tr>  
           <tr>
           <td width="8%" align="left"><a id="butIssue" class="button green medium" style="" name="butIssue"> Issue </a></td>
           <td width="29%" align="left"><a id="butReturn" class="button green medium" style="" name="butReturn"> Return </a></td>
         <td width="63%" colspan="2"></td>
           </tr>
           <tr>
             <td colspan="4" class="normalfnt"><div style="width:900px;height:300px;overflow:scroll" >
               <table width="100%" class="bordered" id="tblMain" >
                 <thead>
                   <tr>
                     <th width="8%" height="19" >Color</th>
                     <th width="12%" height="19" >Technic Type</th>
                     <th width="13%" height="19" >Ink Type</th>
                     <th width="7%" height="19" >Conpc</th>
                     <th width="17%" height="19" >Expected Consump</th>
                     <th width="19%" height="19" >Tot.Consump Entered</th>
                     <th width="7%" height="19" >Issued</th>
                     <th width="9%" >Returned</th>
                     <th width="8%" height="19" >Wastage</th>
                     
                     </tr>
                   <?php
                    $sql = "SELECT OD.intOrderNo,
							SDT.intColorId,
							SID.intTechniqueId,
							SDT.intInkTypeId,
							OD.intOrderYear,
							OD.strGraphicNo,
							IFNULL(SDT.dblColorWeight,0) as dblColorWeight,
							MC.strName AS color,
							MT.strName AS technic,
							MI.strName AS inkType,
							OH.strCustomerPoNo,
							IFNULL((SELECT SUM(PAD.dblQty) FROM plan_actual_details PAD
							INNER JOIN plan_upload_header PUH ON PUH.intId=PAD.intPlanDetailId
							WHERE OH.strCustomerPoNo=PUH.strCustomerPONo AND PAD.intLocationId='$locationId'),0) AS actualQty,
							IFNULL((SELECT SUM(dblQty) FROM ware_sub_color_stocktransactions_bulk SCSB
							WHERE SCSB.intColorId=SDT.intColorId AND
							SCSB.intTechnique=SID.intTechniqueId AND
							SCSB.intInkType=SDT.intInkTypeId AND
							SCSB.intOrderNo=OD.intOrderNo AND
							SCSB.intOrderYear=OD.intOrderYear AND 
							SCSB.intSalesOrderId=OD.intSalesOrderId AND 
							SCSB.intLocationId='$locationId'),0) AS consumStock,
							IFNULL((SELECT (IPA.dblIssueQty-IPA.dblReturnedQty-IPA.dblWastageQty) FROM trn_inktype_production_allocation IPA
							WHERE IPA.intOrderNo=OD.intOrderNo AND 
							IPA.intOrderYear=OD.intOrderYear AND 
							IPA.intSalesOrderId =OD.intSalesOrderId AND 
							IPA.dtmDate='$dtDate' AND
							IPA.strGraphicNo=OD.strGraphicNo AND
							IPA.intColorId=SDT.intColorId AND
							IPA.intTechniqueId=SID.intTechniqueId AND
							IPA.intInkTypeId=SDT.intInkTypeId),0) AS consumption,
							IFNULL((SELECT sum((IPA.dblIssueQty-IPA.dblReturnedQty-IPA.dblWastageQty)) FROM trn_inktype_production_allocation IPA
							WHERE IPA.intOrderNo=OD.intOrderNo AND 
							IPA.intOrderYear=OD.intOrderYear AND 
							IPA.intSalesOrderId =OD.intSalesOrderId AND 
							IPA.strGraphicNo=OD.strGraphicNo AND
							IPA.intColorId=SDT.intColorId AND
							IPA.intTechniqueId=SID.intTechniqueId AND
							IPA.intInkTypeId=SDT.intInkTypeId),0) AS consumptionEnterd,
							
							IFNULL((SELECT sum((IPA.dblIssueQty)) FROM trn_inktype_production_allocation IPA
							WHERE IPA.intOrderNo=OD.intOrderNo AND 
							IPA.intOrderYear=OD.intOrderYear AND 
							IPA.intSalesOrderId =OD.intSalesOrderId AND 
							IPA.strGraphicNo=OD.strGraphicNo AND
							IPA.intColorId=SDT.intColorId AND
							IPA.intTechniqueId=SID.intTechniqueId AND
							IPA.intInkTypeId=SDT.intInkTypeId),0) AS  dblIssueQty,
							
							IFNULL((SELECT sum((IPA.dblReturnedQty)) FROM trn_inktype_production_allocation IPA
							WHERE IPA.intOrderNo=OD.intOrderNo AND 
							IPA.intOrderYear=OD.intOrderYear AND 
							IPA.intSalesOrderId =OD.intSalesOrderId AND 
							IPA.strGraphicNo=OD.strGraphicNo AND
							IPA.intColorId=SDT.intColorId AND
							IPA.intTechniqueId=SID.intTechniqueId AND
							IPA.intInkTypeId=SDT.intInkTypeId),0) AS  dblReturnedQty,
							
							IFNULL((SELECT sum((IPA.dblWastageQty)) FROM trn_inktype_production_allocation IPA
							WHERE IPA.intOrderNo=OD.intOrderNo AND 
							IPA.intOrderYear=OD.intOrderYear AND 
							IPA.intSalesOrderId =OD.intSalesOrderId AND 
							IPA.strGraphicNo=OD.strGraphicNo AND
							IPA.intColorId=SDT.intColorId AND
							IPA.intTechniqueId=SID.intTechniqueId AND
							IPA.intInkTypeId=SDT.intInkTypeId),0) AS  dblWastageQty 
							
							 
							/*(SELECT IFNULL(IPA.dblCurrentQty,0) FROM trn_inktype_production_allocation IPA WHERE 
							IPA.intOrderNo=OD.intOrderNo AND 
							IPA.intOrderYear=OD.intOrderYear AND 
							IPA.intSalesOrderId =OD.intSalesOrderId AND 
							IPA.strGraphicNo=OD.strGraphicNo AND 
							IPA.intColorId=SDT.intColorId AND 
							IPA.intTechniqueId=SID.intTechniqueId AND 
							IPA.intInkTypeId=SDT.intInkTypeId AND
							IPA.dtmDate='$dtDate') as currQty, 
							IF((SELECT IFNULL(SUM((IPA.dblIssueQty-IPA.dblReturnedQty-IPA.dblWastageQty)),0) AS totQty FROM trn_inktype_production_allocation IPA WHERE 
							IPA.intOrderNo=OD.intOrderNo AND 
							IPA.intOrderYear=OD.intOrderYear AND 
							IPA.intSalesOrderId =OD.intSalesOrderId AND 
							IPA.strGraphicNo=OD.strGraphicNo AND 
							IPA.intColorId=SDT.intColorId AND 
							IPA.intTechniqueId=SID.intTechniqueId AND 
							IPA.intInkTypeId=SDT.intInkTypeId AND
							IPA.dtmDate='$dtDate'
							HAVING(totQty)>0),
							 (SELECT IFNULL(IPA.dblInitialQty,0) FROM trn_inktype_production_allocation IPA WHERE 
							IPA.intOrderNo=OD.intOrderNo AND 
							IPA.intOrderYear=OD.intOrderYear AND 
							IPA.intSalesOrderId =OD.intSalesOrderId AND 
							IPA.strGraphicNo=OD.strGraphicNo AND 
							IPA.intColorId=SDT.intColorId AND 
							IPA.intTechniqueId=SID.intTechniqueId AND 
							IPA.intInkTypeId=SDT.intInkTypeId AND
							IPA.dtmDate='$dtDate'),
							(IFNULL((SELECT SUM(dblQty) FROM ware_sub_color_stocktransactions_bulk SCSB 
							WHERE SCSB.intColorId=SDT.intColorId AND 
							SCSB.intTechnique=SID.intTechniqueId AND 
							SCSB.intInkType=SDT.intInkTypeId AND
							SCSB.intOrderNo=OD.intOrderNo AND
							SCSB.intOrderYear=OD.intOrderYear AND 
							SCSB.intSalesOrderId =OD.intSalesOrderId AND 
							SCSB.intLocationId='$locationId'),0))) AS initialQty*/
							FROM trn_orderdetails OD
							INNER JOIN trn_orderheader OH ON OH.intOrderNo=OD.intOrderNo AND OH.intOrderYear=OD.intOrderYear
							INNER JOIN trn_sampleinfomations_details_technical SDT ON SDT.intSampleNo=OD.intSampleNo AND
							SDT.intSampleYear=OD.intSampleYear
							INNER JOIN trn_sampleinfomations_details SID ON SID.intSampleNo=SDT.intSampleNo AND
							SID.intSampleYear=SDT.intSampleYear AND
							SID.intRevNo=SDT.intRevNo AND
							SID.strPrintName=SDT.strPrintName AND
							SID.strComboName=SDT.strComboName AND
							SID.intColorId=SDT.intColorId
							INNER JOIN mst_colors MC ON MC.intId=SDT.intColorId
							INNER JOIN mst_techniques MT ON MT.intId=SID.intTechniqueId
							INNER JOIN mst_inktypes MI ON MI.intId=SDT.intInkTypeId
							WHERE OD.intOrderYear='$cboYear' AND OD.intOrderNo='$cboOrderNo' AND OD.intSalesOrderId='$cboSalesOrderNo' AND OH.intStatus=1 ";
					if($cboGraphicNo!="")
						$sql.="AND OD.strGraphicNo='$cboGraphicNo' ";
					if($cboStyleNo!="")
						$sql.="AND OD.strStyleNo='$cboStyleNo' ";
							
					$sql.="GROUP BY OD.intOrderNo,OD.intOrderYear,OD.intSalesOrderId,OD.strGraphicNo,SDT.intColorId,SID.intTechniqueId,SDT.intInkTypeId 
						   ORDER BY OD.intOrderNo";
						  //  echo $sql;
					
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
					?>
                   <tr>
                     <td class="normalfnt clsColor" id="<?php echo $row['intColorId']; ?>"><?php echo $row['color']; ?></td>
                     <td class="normalfnt clsTechnicType" id="<?php echo $row['intTechniqueId']; ?>"><?php echo $row['technic']; ?></td>
                     <td class="normalfnt clsInkType" id="<?php echo $row['intInkTypeId']; ?>"><?php echo $row['inkType']; ?></td>
                     <td style="text-align:right" class="normalfnt clsConpc"><?php echo round($row['dblColorWeight'],4); ?></td>
                     <td style="text-align:right" class="normalfnt clsExpectedQty"><?php echo round(($row['dblColorWeight']*$orderQty),4); ?></td>
                     <td style="text-align:right" class="normalfnt clsQtyEnterd"><?php echo round($row['dblIssueQty']-$row['dblReturnedQty']-$row['dblWastageQty'],4); ?></td>
                     <td style="text-align:center" class="normalfnt"><span class="normalfnt clsQtyEnterd" style="text-align:right"><?php echo round($row['dblIssueQty'],4); ?></span></td>
                     <td style="text-align:center" class="normalfnt"><span class="normalfnt clsQtyEnterd" style="text-align:right"><?php echo round($row['dblReturnedQty'],4); ?></span></td>
                     <td style="text-align:center" class="normalfnt"><span class="normalfnt clsQtyEnterd" style="text-align:right"><?php echo round($row['dblWastageQty'],4); ?></span></td>
                     </tr>
                   <?php
					}
					?>
                   </thead>
                 <tbody>
                   </tbody>
                 </table>
               </div></td>
</tr>
              
              </table>
          </fieldset></td>
        </tr>
        <tr>
          <td colspan="4" align="center">
                  </td>
        </tr>
        <tr>
          <td height="34" colspan="4"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
            <tr>
              <td width="100%" align="center" bgcolor=""><a id="butNew"  class="button white medium" style="" name="butAddNewTask"> New </a>
                <?php if($editMode==1){ ?>
                <a id="butSave" class="button white medium" style="" name="butSave"> Save </a>
                <?php } ?>
                 <a id="butConfirm" class="button white medium"  <?php if($confirmMode!=1){ ?>style="display:none"<?php } ?> name="butConfirm"> Confirm </a>
                 <a id="butReport" class="button white medium" <?php if($serialNo==''){ ?>style="display:none"<?php } ?> name="butReport"  > Report </a>
                <a id="butClose" class="button white medium" name="butClose" href="../../../main.php">Close</a></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td colspan="4" class="normalfnt"> 
   
          </tr>
      </table>
    </div>
  </div>
</form>
<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1"> 
  <!-- <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php" style="width:800;height:800;border:0;overflow:hidden">
    </iframe>--> 
</div>
<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
</body>
</html>

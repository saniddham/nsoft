<?php 
session_start();
$backwardseperator 	= "../../../";
$mainPath 			= $_SESSION['mainPath'];
$userId 	 		= $_SESSION['userId'];
$locationId 		= $_SESSION["CompanyID"];
$companyId			= $_SESSION["headCompanyId"];
$requestType 		= $_REQUEST['requestType'];

include "{$backwardseperator}dataAccess/Connector.php";

if($requestType=="loadOrderStylGrap")
{
	$year 		= $_REQUEST['year'];
	$order 		= $_REQUEST['order'];
	$graphic 	= $_REQUEST['graphic'];
	$style 		= $_REQUEST['style'];
	$salesOrder = $_REQUEST['salesOrder'];
	$imageStatus = false;
	
	$result 	= loadOrderCombo($year,$graphic,$style,$salesOrder);
	$htmlOrder 	= "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($result))
	{
		$htmlOrder.= "<option value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";
	}
	
	$result1 	= loadGraphicCombo($year,$order,$style,$salesOrder);
	$htmlGraphic= "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($result1))
	{
		$htmlGraphic.= "<option value=\"".$row['strGraphicNo']."\">".$row['strGraphicNo']."</option>";
	}
	
	$result2 	= loadStyleCombo($year,$order,$graphic,$salesOrder);
	$htmlStyle 	= "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($result2))
	{
		$htmlStyle.= "<option value=\"".$row['strStyleNo']."\">".$row['strStyleNo']."</option>";
	}
	
	$result3 	= loadSalesOrderCombo($year,$order,$graphic,$style);
	$htmlSales 	= "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($result3))
	{
		$htmlSales.= "<option value=\"".$row['intSalesOrderId']."\">".$row['strSalesOrderNo']."/".$row['strGraphicNo']."/".$row['strCombo']."/".$row['part']."</option>";
	}
	
	if($year!='' && $order!='' && $salesOrder!='')
	{
		$result = loadSampleDetails($year,$order,$salesOrder);
		while($row=mysqli_fetch_array($result))
		{
			$response['SampleNo'] 	= $row['intSampleNo'];
			$response['SampleYear'] = $row['intSampleYear'];
			$response['RevisionNo'] = $row['intRevisionNo'];
			$response['PrintName'] 	= substr($row['strPrintName'],6,1);
			$imageStatus			= true;
		}
	}
	
	$response['orderCombo'] 	= $htmlOrder;
	$response['graphicCombo'] 	= $htmlGraphic;
	$response['styleCombo'] 	= $htmlStyle;
	$response['salesCombo'] 	= $htmlSales;
	$response['imageStatus'] 	= $imageStatus;
	echo json_encode($response);
}
else if($requestType=="loadStylGrap")
{
	$year 		= $_REQUEST['year'];
	$order 		= $_REQUEST['order'];
	$graphic 	= $_REQUEST['graphic'];
	$style 		= $_REQUEST['style'];
	$salesOrder = $_REQUEST['salesOrder'];
	$imageStatus = false;
	
	if($year!='' && $order!='' && $salesOrder!='')
	{
		$result = loadSampleDetails($year,$order,$salesOrder);
		while($row=mysqli_fetch_array($result))
		{
			$response['SampleNo'] 	= $row['intSampleNo'];
			$response['SampleYear'] = $row['intSampleYear'];
			$response['RevisionNo'] = $row['intRevisionNo'];
			$response['PrintName'] 	= substr($row['strPrintName'],6,1);
			$imageStatus			= true;
		}
	}
	
	$result 	= loadGraphicCombo($year,$order,$style,$salesOrder);
	$htmlGraphic= "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($result))
	{
		$htmlGraphic.= "<option value=\"".$row['strGraphicNo']."\">".$row['strGraphicNo']."</option>";
	}
	
	$result1 	= loadStyleCombo($year,$order,$graphic,$salesOrder);
	$htmlStyle 	= "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($result1))
	{
		$htmlStyle.= "<option value=\"".$row['strStyleNo']."\">".$row['strStyleNo']."</option>";
	}
	
	$result3 	= loadSalesOrderCombo($year,$order,$graphic,$style);
	$htmlSales 	= "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($result3))
	{
		$htmlSales.= "<option value=\"".$row['intSalesOrderId']."\">".$row['strSalesOrderNo']."/".$row['strGraphicNo']."/".$row['strCombo']."/".$row['part']."</option>";
	}
	
		$sqlAct = " SELECT IFNULL(SUM(PAD.dblQty),0) AS actualQty FROM plan_actual_details PAD
					INNER JOIN plan_upload_header PUH ON PUH.intId=PAD.intPlanDetailId
					INNER JOIN trn_orderheader OH ON OH.strCustomerPoNo=PUH.strCustomerPONo
					WHERE OH.intOrderNo='$order' AND OH.intOrderYear='$year' AND PAD.intLocationId='$locationId' ";
		$resultAct = $db->RunQuery($sqlAct);
		$rowAct = mysqli_fetch_array($resultAct);
		$actQty = $rowAct['actualQty'];

	$response['graphicCombo'] 	= $htmlGraphic;
	$response['styleCombo'] 	= $htmlStyle;
	$response['salesCombo'] 	= $htmlSales;
	$response['actualQty'] 		= $actQty;
	$response['imageStatus'] 	= $imageStatus;
	echo json_encode($response);
}
else if($requestType=="loadStylOrder")
{
	$year 		= $_REQUEST['year'];
	$order 		= $_REQUEST['order'];
	$graphic 	= $_REQUEST['graphic'];
	$style 		= $_REQUEST['style'];
	$salesOrder = $_REQUEST['salesOrder'];
	
	$result 	= loadOrderCombo($year,$graphic,$style,$salesOrder);
	$htmlOrder 	= "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($result))
	{
		$htmlOrder.= "<option value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";
	}
	
	$result1 	= loadStyleCombo($year,$order,$graphic,$salesOrder);
	$htmlStyle 	= "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($result1))
	{
		$htmlStyle.= "<option value=\"".$row['strStyleNo']."\">".$row['strStyleNo']."</option>";
	}
	
	$result3 	= loadSalesOrderCombo($year,$order,$graphic,$style);
	$htmlSales 	= "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($result3))
	{
		$htmlSales.= "<option value=\"".$row['intSalesOrderId']."\">".$row['strSalesOrderNo']."/".$row['strGraphicNo']."/".$row['strCombo']."/".$row['part']."</option>";
	}
	
	$response['orderCombo'] 	= $htmlOrder;
	$response['styleCombo'] 	= $htmlStyle;
	$response['salesCombo'] 	= $htmlSales;
	
	echo json_encode($response);
}
else if($requestType=="loadOrderGrap")
{
	$year 		= $_REQUEST['year'];
	$order 		= $_REQUEST['order'];
	$graphic 	= $_REQUEST['graphic'];
	$style 		= $_REQUEST['style'];
	$salesOrder = $_REQUEST['salesOrder'];
	
	$result 	= loadOrderCombo($year,$graphic,$style,$salesOrder);
	$htmlOrder 	= "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($result))
	{
		$htmlOrder.= "<option value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";
	}
	
	$result1 	= loadGraphicCombo($year,$order,$style,$salesOrder);
	$htmlGraphic= "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($result1))
	{
		$htmlGraphic.= "<option value=\"".$row['strGraphicNo']."\">".$row['strGraphicNo']."</option>";
	}
	
	$result3 	= loadSalesOrderCombo($year,$order,$graphic,$style);
	$htmlSales 	= "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($result3))
	{
		$htmlSales.= "<option value=\"".$row['intSalesOrderId']."\">".$row['strSalesOrderNo']."/".$row['part']."</option>";
	}

	$response['orderCombo'] 	= $htmlOrder;
	$response['graphicCombo'] 	= $htmlGraphic;
	$response['salesCombo'] 	= $htmlSales;
	echo json_encode($response);
}
else if($requestType=="loadOrderGrapStyle")
{
	$year 		= $_REQUEST['year'];
	$order 		= $_REQUEST['order'];
	$graphic 	= $_REQUEST['graphic'];
	$style 		= $_REQUEST['style'];
	$salesOrder = $_REQUEST['salesOrder'];
	$imageStatus = false;
	
	$result 	= loadOrderCombo($year,$graphic,$style,$salesOrder);
	$htmlOrder 	= "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($result))
	{
		$htmlOrder.= "<option value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";
	}
	
	$result1 	= loadGraphicCombo($year,$order,$style,$salesOrder);
	$htmlGraphic= "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($result1))
	{
		$htmlGraphic.= "<option value=\"".$row['strGraphicNo']."\">".$row['strGraphicNo']."</option>";
	}
	
	$result3 	= loadStyleCombo($year,$order,$graphic,$salesOrder);
	$htmlStyle 	= "<option value=\"\"></option>";
	while($row=mysqli_fetch_array($result3))
	{
		$htmlStyle.= "<option value=\"".$row['strStyleNo']."\">".$row['strStyleNo']."</option>";
	}
	
	if($year!='' && $order!='' && $salesOrder!='')
	{
		$result = loadSampleDetails($year,$order,$salesOrder);
		while($row=mysqli_fetch_array($result))
		{
			$response['SampleNo'] 	= $row['intSampleNo'];
			$response['SampleYear'] = $row['intSampleYear'];
			$response['RevisionNo'] = $row['intRevisionNo'];
			$response['PrintName'] 	= substr($row['strPrintName'],6,1);
			$imageStatus			= true;
		}
	}

	$response['orderCombo'] 	= $htmlOrder;
	$response['graphicCombo'] 	= $htmlGraphic;
	$response['styleCombo'] 	= $htmlStyle;
	$response['imageStatus'] 	= $imageStatus;
	echo json_encode($response);
}
function loadOrderCombo($year,$graphic,$style,$salesOrder)
{
	global $db;
	global $locationId;
	
	$sql = "SELECT DISTINCT OH.intOrderNo
			FROM trn_orderheader OH
			INNER JOIN trn_orderdetails OD ON OH.intOrderNo=OD.intOrderNo AND OH.intOrderYear=OD.intOrderYear
			WHERE OH.intStatus=1 ";
	if($year!='')
		$sql.="AND OH.intOrderYear = '$year' ";		
	if($graphic!='')
		$sql.="AND OD.strGraphicNo = '$graphic' ";
	if($style!='')
		$sql.="AND OD.strStyleNo = '$style' ";
	if($salesOrder!='')
		$sql.="AND OD.intSalesOrderId = '$salesOrder' ";
	$sql.="ORDER BY OH.intOrderNo ";
	
	$result = $db->RunQuery($sql);
	return $result;
}
function loadGraphicCombo($year,$order,$style,$salesOrder)
{
	global $db;
	global $locationId;
	
	$sql = "SELECT DISTINCT OD.strGraphicNo
			FROM trn_orderdetails OD
			INNER JOIN trn_orderheader OH ON OH.intOrderNo=OD.intOrderNo AND OH.intOrderYear=OD.intOrderYear
			WHERE OH.intStatus=1 ";
	if($year!='')
		$sql.="AND OD.intOrderYear = '$year' ";	
	if($order!='')
		$sql.="AND OD.intOrderNo = '$order' ";
	if($style!='')
		$sql.="AND OD.strStyleNo = '$style' ";
	if($salesOrder!='')
		$sql.="AND OD.intSalesOrderId = '$salesOrder' ";
	
	$sql.="ORDER BY OD.strGraphicNo ";
	
	$result = $db->RunQuery($sql);
	return $result;
}
function loadStyleCombo($year,$order,$graphic,$salesOrder)
{
	global $db;
	global $locationId;
	
	$sql = "SELECT DISTINCT OD.strStyleNo
			FROM trn_orderdetails OD
			INNER JOIN trn_orderheader OH ON OH.intOrderNo=OD.intOrderNo AND OH.intOrderYear=OD.intOrderYear
			WHERE OH.intStatus=1 ";
	if($year!='')
		$sql.="AND OD.intOrderYear = '$year' ";	
	if($order!='')
		$sql.="AND OD.intOrderNo = '$order' ";
	if($graphic!='')
		$sql.="AND OD.strGraphicNo = '$graphic' ";
	if($salesOrder!='')
		$sql.="AND OD.intSalesOrderId = '$salesOrder' ";
	
	
	$sql.="ORDER BY OD.strStyleNo ";
	
	$result = $db->RunQuery($sql);
	return $result;
}
function loadSalesOrderCombo($year,$order,$graphic,$style)
{
	global $db;
	global $locationId;
	
	$sql = "SELECT DISTINCT OD.intSalesOrderId,OD.strSalesOrderNo,OD.intPart,OD.strGraphicNo,OD.strCombo,
			mst_part.strName AS part
			FROM trn_orderdetails OD
			INNER JOIN trn_orderheader OH ON OH.intOrderNo=OD.intOrderNo AND OH.intOrderYear=OD.intOrderYear
			INNER JOIN mst_part ON OD.intPart = mst_part.intId 
			WHERE OH.intStatus=1 ";
	if($year!='')
		$sql.="AND OD.intOrderYear = '$year' ";	
	if($order!='')
		$sql.="AND OD.intOrderNo = '$order' ";
	if($graphic!='')
		$sql.="AND OD.strGraphicNo = '$graphic' ";
	if($style!='')
		$sql.="AND OD.strStyleNo = '$style' ";
	
	
	$sql.="ORDER BY OD.strStyleNo ";
	
	$result = $db->RunQuery($sql);
	return $result;
}
function loadSampleDetails($year,$order,$salesOrder)
{
	global $db;
	$sql = "SELECT
			trn_orderdetails.intSampleNo,
			trn_orderdetails.intSampleYear,
			trn_orderdetails.strCombo,
			trn_orderdetails.strPrintName,
			trn_orderdetails.intRevisionNo
			FROM `trn_orderdetails`
			WHERE
			trn_orderdetails.intOrderNo = '$order' AND
			trn_orderdetails.intOrderYear = '$year' ";
			if($salesOrder!='')
				$sql.=" AND trn_orderdetails.intSalesOrderId = '$salesOrder' ";
	
	$result = $db->RunQuery($sql);
	return $result;
}
?>
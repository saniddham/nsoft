<?php
///////////////////////////////
// H B G KORALA   ////////////
//////////////////////////////
//ini_set('display_errors',1);
session_start();
$backwardseperator	= "../../../";
$mainPath 			= $_SESSION['mainPath'];
$location 			= $_SESSION['CompanyID'];
$company 			= $_SESSION['headCompanyId'];
$thisFilePath 		=  $_SERVER['PHP_SELF'];
$intUser  			= $_SESSION["userId"];
$session_userId		= $_SESSION["userId"];


include  	"{$backwardseperator}dataAccess/permisionCheck.inc";
include_once ("../../../class/cls_commonErrorHandeling_get.php");
include_once ("../../../class/warehouse/cls_warehouse_get.php");
include_once ("../../../class/warehouse/first_second_day_consumptions/cls_first_second_day_consumptions_get.php");


 
 
$order_year 		= $_REQUEST['order_year'];
$order_no 			= $_REQUEST['order_no'];
$sales_order 		= $_REQUEST['sales_order'];
$type 				= $_REQUEST['type'];

$editMode			=0;
$confirmatonMode	=0;

$programName		='Day Consumptions';
$programCode		='P0829';

$obj_commonErr		= new cls_commonErrorHandeling_get($db);
$obj_warehouse_get	= new cls_warehouse_get($db);
$obj_day_get		= new cls_first_second_day_consumptions_get($db);

$header_array 		= $obj_day_get->get_header_array($order_year,$order_no,$sales_order,$type,'RunQuery');
//$detail_result		= $obj_day_get->get_details_result($order_year,$order_no,$sales_order,$type,'RunQuery');

$permition_arr		= $obj_commonErr->get_permision_withApproval_save($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery');
$permision_save		= $permition_arr['permision'];
$permition_arr		= $obj_commonErr->get_permision_withApproval_cancel($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery');
$permision_cancel	= $permition_arr['permision'];
$permition_arr		= $obj_commonErr->get_permision_withApproval_reject($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery');
$permision_reject	= $permition_arr['permision'];
$permition_arr		= $obj_commonErr->get_permision_withApproval_confirm($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery');
$permision_confirm	= $permition_arr['permision'];

 if(!isset($header_array["MRN_NO"]))
	$MRNDate	= date("Y-m-d");
else
	$MRNDate	= $header_array["MRN_DATE"];
	
?> 		
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Day Consumptions</title>
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/button.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $backwardseperator; ?>css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">


<script type="application/javascript" src="../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="first_second_day_consumptions-js.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" type="text/css" href="../../../libraries/calendar/theme.css" />
<script src="../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
</head>

<body onload="<?php if($order_no !=''){ ?> loadData('<?php echo $order_no ?>','<?php echo $order_year ?>','<?php echo $sales_order ?>','<?php echo $type ?>') <?php } ?>">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>
<script src="../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.min.js"></script>


<form id="frmDayConsump" name="frmDayConsump" method="post" action="">
<table width="1200" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutL" style="width:950px">
		  <div class="trans_text">Day Consumptions</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="100%" height="22" colspan="8" class="normalfnt"><table width="950" border="0" cellpadding="0" cellspacing="0" class="tableBorder_allRound">
              <tr height="25">
                <td width="79" class="normalfnt"> Order No <span class="compulsoryRed">*</span></td>
                <td width="224" >
                <select name="cboOrderYear" id="cboOrderYear" style="width:60px" class="validate[required]">
                <?php
                echo $obj_warehouse_get->get_order_years__options($location,'RunQuery');
                ?>
                </select><?php /*?><select name="cboOrderNo" id="cboOrderNo" style="width:100px" class="validate[required]">
                <?php
                echo $obj_warehouse_get->get_order_nos_options($location,$order_year,'RunQuery');
                ?>
                </select><?php */?><?php  //echo $sql ?><input type="text"  name="cboOrderNo" id="cboOrderNo" style="width:100px" class="validate[required]"/>
                <img id="loadSalesOrders" src="../../../images/down1.png" width="20" height="20" title="download sales orders" class="mouseover" /></td>
                <td width="133" class="normalfnt">Sales Order No <span class="compulsoryRed">*</span></td>
                <td width="150" ><select name="cboSalesOrderNo" id="cboSalesOrderNo" style="width:130px" class="validate[required]">
               <?php
                echo $obj_warehouse_get->get_sales_order_nos_options($order_no,$order_year,'RunQuery');
                ?>
				  </select><?php // echo $sql ?></td>
                <td width="110" class="normalfnt">&nbsp;</td>
            <td width="41" class="normalfnt">Date <span class="compulsoryRed">*</span></td>
            <td width="211" ><input name="dtDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="txtbox" id="dtDate" style="width:120px;" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php if($editMode==0){  ?> disabled="disabled"<?php } ?>/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                </tr>
              <tr>
                <td class="normalfnt">Type <span class="compulsoryRed">*</span></td>
                <td><select name="cboType" id="cboType" style="width:160px" class="validate[required]">
                <?php
                echo $obj_day_get->get_type_combo($type,'RunQuery');
                ?>
                   </select>
                </td>
                <td class="normalfnt">&nbsp;</td>
                <td align="right">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td align="center"><img src="../../../images/smallSearch.png" width="24" height="24" id="butSearch" name="butSearch" class="mouseover"/></td>
                </tr>  
              </table></td>
          </tr>
          </table></td>
      </tr>
      <tr>
        <td>
          <table width="100%" class="bordered" id="tblMain"  >
            <thead>
             <tr class="">
                <th align="center" bgcolor="#FFFFFF" width="8%" >Sample No</th> 
                <th align="center" bgcolor="#FFFFFF" width="8%" >Graphic</th> 
                <th align="center" bgcolor="#FFFFFF" width="4%" >Revision</th> 
                <th align="center" bgcolor="#FFFFFF" width="8%" >Combo</th> 
                <th align="center" bgcolor="#FFFFFF" width="8%"   nowrap="nowrap" >Part</th> 
                <th align="center" bgcolor="#FFFFFF" width="10%"  nowrap="nowrap">Main Category</th> 
                <th align="center" bgcolor="#FFFFFF" width="10%"  nowrap="nowrap">Sub Category</th> 
                <th align="center" bgcolor="#FFFFFF" width="20%"  nowrap="nowrap">Item</th> 
                <th align="center" bgcolor="#FFFFFF" width="5%">UOM</th> 
                <th align="center" bgcolor="#FFFFFF" width="5%">conPC</th>
              </tr>
              </thead>
  </table>
           </td>
</tr>
<tr>
        <td align="center" class="tableBorder_allRound">
                <a class="button white medium" id="butNew">New</a>
                <a class="button white medium" id="butSave" <?php if($permision_save!=1){ ?>style="display:none"<?php } ?>>Save</a>
                <a class="button white medium" id="butConfirm"  <?php if($permision_confirm!=1){ ?> style="display:none"<?php } ?>>Approve</a>
                <a class="button white medium" id="butReport">Report</a>
                <a href="../../../main.php" class="button white medium" id="butSave">Close</a>
        </td>
</tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
	<!-- items to prn -->
	<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1">
   <!-- <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php" style="width:800;height:800;border:0;overflow:hidden">
    </iframe>-->
    </div>

	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>


</body>
</html>
 
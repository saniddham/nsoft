<?php
session_start();
$backwardseperator 	= "../../../";
include  "{$backwardseperator}dataAccess/Connector.php";
include_once ("../../../class/warehouse/first_second_day_consumptions/cls_first_second_day_consumptions_get.php");
include_once  ("../../../class/cls_commonErrorHandeling_get.php");
include_once ("../../../class/warehouse/cls_warehouse_get.php");
 
$programName		='Day Consumptions';
$programCode		='P0829';

$session_userId 			= $_SESSION["userId"];
$order_year 				= $_REQUEST['order_year'];
$order_no 					= $_REQUEST['order_no'];
$sales_order 				= $_REQUEST['sales_order'];
$type 						= $_REQUEST['type'];
$mode						= $_REQUEST["mode"];

$obj_commonErr				= new cls_commonErrorHandeling_get($db);
$obj_day_get				= new cls_first_second_day_consumptions_get($db);
$obj_warehouse_get			= new cls_warehouse_get($db);

$header_array 				= $obj_day_get->get_header_array($order_year,$order_no,$sales_order,$type,'RunQuery');
//$detail_result			= $obj_day_get->get_details_result($order_year,$order_no,$sales_order,$type,'RunQuery');

$permition_arr				= $obj_commonErr->get_permision_withApproval_save($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery');
$permision_save				= $permition_arr['permision'];
$permition_arr				= $obj_commonErr->get_permision_withApproval_cancel($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery');
$permision_cancel			= $permition_arr['permision'];
$permition_arr				= $obj_commonErr->get_permision_withApproval_reject($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery');
$permision_reject			= $permition_arr['permision'];
$permition_arr				= $obj_commonErr->get_permision_withApproval_confirm($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery');
$permision_confirm			= $permition_arr['permision'];

$locationId					= $_SESSION['CompanyID'];

 	
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Day Consumptions Report</title>
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../../../css/button.css"/>

<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="first_second_day_consumptions-js.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/script.js"></script>

<script src="../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.min.js"></script>
 </head>
<body>
<style type="text/css">
.apDiv1 {
	position:absolute;
	left:380px;
	top:100px;
	width:auto;
	height:auto;
	z-index:0;
	opacity:0.1;
}
</style>
<?php
//$header_array["STATUS"]=3;

?>

 <form id="frmRptConsumptions" name="frmRptConsumptions" method="post" action="rpt_first_second_day_consumptions.php">
  <table width="900" align="center">
    <tr>
      <td><?php include '../../../reportHeader.php'?></td>
    </tr>
    <tr>
      <td class="reportHeader" align="center">Day Consumptions</td>
    </tr>
	<?php
		include "../../../presentation/report_approve_status_and_buttons.php"
     ?>
<tr>
      <td><table width="100%" border="0" class="normalfnt">
          <tr>
            <td width="19%">Order No</td>
            <td width="2%">:</td>
            <td width="38%"><?php echo $header_array["ORDER_NO"]."/".$header_array["ORDER_YEAR"]; ?></td>
            <td width="18%"> Date</td>
            <td width="2%">:</td>
            <td width="21%"><?php echo $header_array["SAVED_DATE"]?></td>
          </tr>
          <tr>
            <td>Sales Order</td>
            <td>:</td>
            <td><?php echo $header_array["strSalesOrderNo"]?></td>
            <td>Type</td>
            <td>:</td>
            <td><?php echo $header_array["COMSUMPTION_TYPE"]?></td>
          </tr>
</table></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <?php
		$sizes					= $obj_warehouse_get->get_sizes_array($order_no,$order_year,$sales_order,'RunQuery');
	?>
    
    
    <tr>
      <td ><table width="100%%" border="0" class="rptBordered" id="tblMain">
          <thead> <tr class="normalfnt">
          <th align="center" bgcolor="#FFFFFF" width="10%" >Sample No</th>
          <th align="center" bgcolor="#FFFFFF" width="10%" >Graphic</th>
          <th align="center" bgcolor="#FFFFFF" width="5%" >Revision</th>
          <th align="center" bgcolor="#FFFFFF" width="8%" >Combo</th>
          <th align="center" bgcolor="#FFFFFF" width="8%"   nowrap="nowrap" >Part</th>
          <th align="center" bgcolor="#FFFFFF" width="10%"  nowrap="nowrap">Main Category</th>
          <th align="center" bgcolor="#FFFFFF" width="10%"  nowrap="nowrap">Sub Category</th>
          <th align="center" bgcolor="#FFFFFF" width="20%"  nowrap="nowrap">Item</th>
          <th align="center" bgcolor="#FFFFFF" width="5%">UOM</th> 
			<?php
			foreach($sizes as $x){
 				$cons	= $obj_warehouse_get->get_day_consumptions($order_no,$order_year,$sales_order,$row['intId'],$x,$type,'RunQuery');
			?>
                <th class="" style="text-align:right" nowrap="nowrap"><?php echo $x?></th>
            <?php
			}
			?>
          </thead>
          <tbody>
          <?php
		$result = $obj_warehouse_get->get_foil_special_items($order_no,$order_year,$sales_order,$mainCategory,$subCategory,$description,'RunQuery');
		while($row=mysqli_fetch_array($result))
		{
			$savePermision	= $permision_save;
			$confPermision	= $permision_confirm;
			
			$intSampleNo	= $row['intSampleNo']; 
			$intSampleYear	= $row['intSampleYear']; 
			$intRevisionNo	= $row['intRevisionNo'];
			$strCombo		= $row['strCombo']; 
			$strPrintName	= $row['strPrintName'];
			$strGraphicRefNo= $row['strGraphicRefNo'];
			$partId			= $row['partId'];
			$partName		= $row['partName'];

			$itemId			= $row['intId'];
			$maincatId		= $row['intMainCategory'];
			$subCatId		= $row['intSubCategory'];
			$mainCatName	= $row['mainCatName'];
			$subCatName		= $row['subCatName'];
			$code 			= $row['strCode'];
			$uom 			= $row['uom'];
			$itemName 		= $row['itemName'];
  			$consum 		= $row['consum'];
  			
			?>
            	<tr>
 				<td align="center" bgcolor="#FFFFFF" id="<?php echo $maincatId?>" class="sample"><?php echo $intSampleNo?>/<?php echo $intSampleYear?>
 				<td align="center" bgcolor="#FFFFFF" id="<?php echo $maincatId?>" class=""><?php echo $strGraphicRefNo?>
 				<td align="center" bgcolor="#FFFFFF" id="<?php echo $maincatId?>" class=""><?php echo $intRevisionNo?>
 				<td align="center" bgcolor="#FFFFFF" id="<?php echo $maincatId?>" class=""><?php echo $strCombo?>
 				<td align="center" bgcolor="#FFFFFF" id="<?php echo $maincatId?>" class="" nowrap="nowrap"><?php echo $partName?>
				<td align="center" bgcolor="#FFFFFF" id="<?php echo $maincatId?>" class="mainCatName"  nowrap="nowrap"><?php echo $mainCatName?>
				<td align="center" bgcolor="#FFFFFF" id="<?php echo $subCatId?>" class="subCatName"  nowrap="nowrap"><?php echo $subCatName?>
				<td align="center" bgcolor="#FFFFFF" id="<?php echo $itemId?>" class="item"  nowrap="nowrap"><?php echo $itemName?>
				<td align="center" bgcolor="#FFFFFF" id="<?php echo $uom?>" class="uom"><?php echo $uom?>
            
            
			<?php
			foreach($sizes as $x){
 				$cons	= $obj_warehouse_get->get_day_consumptions($order_no,$order_year,$sales_order,$row['intId'],$x,$type,'RunQuery');
			?>
                <td class="" style="text-align:right" nowrap="nowrap"><?php echo $cons?></td>
            <?php
			}
			?>
         </tr>
            <?php
		}
			?>
 </tbody>
        </table></td>
    </tr>
     <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
    <td>
    <?php
			$creator		= $header_array['strFullName'];
			$createdDate	= $header_array['SAVED_DATE'];
 			$resultA 		= $obj_day_get->get_Report_approval_details_result($order_no,$order_year,$sales_order,$type);
			include "../../../presentation/report_approvedBy_details.php"
 	?>
    </td>
    </tr>
  </table>
</form>
</body>
</html>
<?php
 ?>
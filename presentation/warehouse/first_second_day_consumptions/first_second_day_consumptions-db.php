<?php
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
 // ini_set('display_errors',1);
	session_start();
	$backwardseperator 	= "../../../";
	$mainPath 			= $_SESSION['mainPath'];
	$userId 			= $_SESSION['userId'];
	$session_userId		= $_SESSION["userId"];
	$requestType 		= $_REQUEST['requestType'];
	$location 			= $_SESSION['CompanyID'];
	$company 			= $_SESSION['headCompanyId'];
	
	$programCode		='P0829';

	include_once "{$backwardseperator}dataAccess/Connector.php";
	require_once $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
	include_once ("../../../class/cls_commonErrorHandeling_get.php");
	include_once ("../../../class/warehouse/first_second_day_consumptions/cls_first_second_day_consumptions_set.php");
	include_once ("../../../class/warehouse/first_second_day_consumptions/cls_first_second_day_consumptions_get.php");
	include_once ("../../../class/warehouse/cls_warehouse_get.php");
	
	//$obj_mrn_get			= new Cls_color_room_mrn_Get($db);
	//$obj_mrn_set			= new Cls_color_room_mrn_Set($db);
	$obj_warehouse_get		= new cls_warehouse_get($db);
	$obj_comfunc_get		= new cls_commonFunctions_get($db);
	$obj_commonErr			= new cls_commonErrorHandeling_get($db);
	$obj_day_con_set		= new cls_first_second_day_consumptions_set($db);
	$obj_day_con_get		= new cls_first_second_day_consumptions_get($db);
	

	if($requestType=='loadOrders')
	{
		$orderYear		= $_REQUEST['year'];
 		$html			= $obj_warehouse_get->get_order_nos_options($location,$orderYear,'RunQuery');
		echo $html;
	}
	else if($requestType=='loadSalesOrder')
	{
		$orderNo		= $_REQUEST['orderNo'];
		$year			= $_REQUEST['year'];
 		$html			= $obj_warehouse_get->get_sales_order_nos_options($orderNo,$year,'RunQuery');
		echo $html;
	}
	else if($requestType=='loadSizes')
	{
		$orderNo		= $_REQUEST['orderNo'];
		$year			= $_REQUEST['year'];
		$salesOrderNo	= $_REQUEST['salesOrderNo'];
		$html			= $obj_warehouse_get->get_sizes_array($orderNo,$year,$salesOrderNo,'RunQuery');
 		$response['arrData'] 		= $html;
		echo json_encode($response);
		
	}
	else if($requestType=='loadItems')
	{
		$orderNo		= $_REQUEST['orderNo'];
		$year			= $_REQUEST['year'];
		$salesOrderNo	= $_REQUEST['salesOrderNo'];
		$type  	= $_REQUEST['type'];
 		
		$header_array 		= $obj_day_con_get->get_header_array($year,$orderNo,$salesOrderNo,$type,'RunQuery');
		$permition_arr		= $obj_commonErr->get_permision_withApproval_save($header_array['STATUS'],$header_array['LEVELS'],$userId,$progrmCode,'RunQuery');
		$permision_save		= $permition_arr['permision'];
		$permition_arr		= $obj_commonErr->get_permision_withApproval_confirm($header_array['STATUS'],$header_array['LEVELS'],$userId,$progrmCode,'RunQuery');
		$permision_confirm	= $permition_arr['permision'];
		
		$result = $obj_warehouse_get->get_foil_special_items($orderNo,$year,$salesOrderNo,$mainCategory,$subCategory,$description,'RunQuery');
		while($row=mysqli_fetch_array($result))
		{
			$data['savePermision']	= $permision_save;
			$data['confPermision']	= $permision_confirm;
			
			$data['intSampleNo']	= $row['intSampleNo']; 
			$data['intSampleYear']	= $row['intSampleYear']; 
			$data['intRevisionNo']	= $row['intRevisionNo'];
			$data['strCombo']		= $row['strCombo']; 
			$data['strPrintName']	= $row['strPrintName'];
			$data['strGraphicRefNo']= $row['strGraphicRefNo'];
			$data['partId']			= $row['partId'];
			$data['partName']		= $row['partName'];

			$data['itemId']			= $row['intId'];
			$data['maincatId']		= $row['intMainCategory'];
			$data['subCatId']		= $row['intSubCategory'];
			$data['mainCatName']	= $row['mainCatName'];
			$data['subCatName']		= $row['subCatName'];
			$data['code'] 			= $row['strCode'];
			$data['uom'] 			= $row['uom'];
			$data['itemName'] 		= $row['itemName'];
  			$data['consum'] 		= round($row['consum'],4);
			$saved_flag				= $obj_day_con_get->get_saved_flag($year,$orderNo,$salesOrderNo,$type,'RunQuery');
  			$data['saved_flag'] 	= $saved_flag;
		
			echo $cc				= $obj_warehouse_get->get_cumulateve_ink_item_consumption($orderNo,$year,$salesOrderNo,'723','RunQuery');
			$sizes					= $obj_warehouse_get->get_sizes_array($orderNo,$year,$salesOrderNo,'RunQuery');
			foreach($sizes as $x){
 				$data['savedCons'][$x]		= $obj_warehouse_get->get_day_consumptions($orderNo,$year,$salesOrderNo,$row['intId'],$x,$type,'RunQuery');
			}
			
			
			
			$arrCombo[] = $data;
		}
 		$response['arrCombo'] 		= $arrCombo;
		echo json_encode($response);

	}
	else if($requestType=='save')
	{
		
		$db->begin();
		
		$rollBackFlag	=0;
		$saved_records	=0;
	
		$orderNo		= $_REQUEST['order_no'];
		$year			= $_REQUEST['year'];
		$salesOrderNo	= $_REQUEST['sales_order'];
		$type  			= $_REQUEST['type'];
		$date			=$_REQUEST["date"];
  		$arr			= json_decode($_REQUEST["arr"],true);
 
		$header_array 	= $obj_day_con_get->get_header_array($year,$orderNo,$salesOrderNo,$type,'RunQuery2');
		$approveLevels 	= $obj_comfunc_get->getApproveLevels('Day Consumptions');
 		$status			=$approveLevels+1;
		if($rollBackFlag != 1){
			if($header_array['STATUS']==''){
				$response_h=$obj_day_con_set->save_header($year,$orderNo,$salesOrderNo,$type,$approveLevels,$status,$userId,'RunQuery2');
 			}
			else{
				$response_h=$obj_day_con_set->update_header($year,$orderNo,$salesOrderNo,$type,$approveLevels,$status,$userId,'RunQuery2');
			}
			if($response_h['type']=='fail'){
				$rollBackFlag	=1;
				$response		=$response_h;
				$sqlErr			=$response_h['sql'];
			}

			$maxAppByStatus=$obj_day_con_get->getMaxAppByStatus($year,$orderNo,$salesOrderNo,$type,'RunQuery2');
			$maxAppByStatus=(int)$maxAppByStatus+1;
			$response=$obj_day_con_set->approved_by_update($year,$orderNo,$salesOrderNo,$type,$maxAppByStatus,$userId,'RunQuery2');
			if($response['type']=='fail'){
				$rollBackFlag	=1;
				$response		=$response;
				$sqlErr			=$response['sql'];
			}
			
		}
		
		if($rollBackFlag==0){
			$response_dt	= $obj_day_con_set->delete_details($orderNo,$year,$salesOrderNo,$type,'RunQuery2');
			if($response_dt['type']=='fail'){
				$rollBackFlag	=1;
				$response		=$response_dt;
				$sqlErr			=$response_dt['sql'];
			}
		}
		foreach($arr as $array_loop)
		{
 			$item	= $array_loop["item"];
			$qty	= $array_loop["qty"];
			$size	= $array_loop["size"];
			
 
			if($rollBackFlag==0){
				$response_dt	= $obj_day_con_set->save_details($orderNo,$year,$salesOrderNo,$type,$item,$qty,$size,'RunQuery2');
				if($response_dt['type']=='fail'){
					$rollBackFlag	=1;
					$response		=$response_dt;
					$sqlErr			=$response_dt['sql'];
				}
				else{
					$saved_records++;	
				}
			}
		}
		//print_r($response);
	
		$permition_arr		= $obj_commonErr->get_permision_withApproval_save($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery2');
		$permision_save		= $permition_arr['permision'];
		$permition_arr		= $obj_commonErr->get_permision_withApproval_confirm($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery2');
		$permision_confirm	= $permition_arr['permision'];
	
		
		if($saved_records==0){
			$db->rollback();
			$response['type'] 		= 'fail';
			$response['msg'] 		= 'Detials Not Saved'.$response['msg'];
			$response['q'] 			= $sqlErr;
		}
		else if($rollBackFlag==0){
			$db->commit();
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Saved Successfully';
			$response['q'] 			= '';
			$response['savePermision']	= $permision_save;
			$response['confPermision']	= $permision_confirm;
		}
		else
		{
			$db->rollback();
 		}
	
	$arrCombo[] = $response;
  	echo json_encode($response);
		
	}
	
	else if($requestType=='approve')
	{
		$order_year 				= $_REQUEST['order_year'];
		$order_no 					= $_REQUEST['order_no'];
		$sales_order 				= $_REQUEST['sales_order'];
		$type 						= $_REQUEST['type'];
		
		$rollBack	= 0;
		$db->begin();
		
 		
		$response = $obj_day_con_set->updateHeaderStatus($order_year,$order_no,$sales_order,$type,'','RunQuery2');
		if($response['type']=='fail'){
			$messageErr	= $response['msg']; 
			$sqlErr		= $response['sql'];
			$rollBack=1;
		}
		
		if($rollBack!=1){
			$header_array = $obj_day_con_get->get_header_array($order_year,$order_no,$sales_order,$type,'RunQuery2');
			$status=$header_array['STATUS'];
			$savedLevels=$header_array['LEVELS'];
			$approval=$savedLevels+1-$status;
			
			$response = $obj_day_con_set->approved_by_insert($order_year,$order_no,$sales_order,$type,$session_userId,$approval,'RunQuery2');
			if($response['type']=='fail'){
				$messageErr	= $response['msg']; 
				$sqlErr		= $response['sql'];
				$rollBack=1;
			}
		}
		
    		
		if($rollBack==1){
			$db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= $messageErr;
			$response['sql']		= $sqlErr;
		}
		else if($rollBack==0){
			$db->commit();
			$response['type'] 		= "pass";
			$header_array = $obj_day_con_get->get_header_array($order_year,$order_no,$sales_order,$type,'RunQuery2');
			if($header_array['STATUS']==1) 
			$response['msg'] 		= "Final Approval Raised successfully.";
 			else 
			$response['msg'] 		= "Approved successfully.";
 		}else{
			$db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= $messageErr;
			$response['sql']		= $sqlErr;
		}
		
 		echo json_encode($response);
	}

	else if($requestType=='reject')
	{
		$order_year 				= $_REQUEST['order_year'];
		$order_no 					= $_REQUEST['order_no'];
		$sales_order 				= $_REQUEST['sales_order'];
		$type 						= $_REQUEST['type'];
		
		$rollBack	= 0;
		$db->begin();
		
 		
		$response = $obj_day_con_set->updateHeaderStatus($order_year,$order_no,$sales_order,$type,0,'RunQuery2');
		if($response['type']=='fail'){
			$messageErr	= $response['msg']; 
			$sqlErr		= $response['sql'];
			$rollBack=1;
		}
		if($rollBack!=1){
			$response = $obj_day_con_set->approved_by_insert($order_year,$order_no,$sales_order,$type,$session_userId,0,'RunQuery2');
			if($response['type']=='fail'){
				$messageErr	= $response['msg']; 
				$sqlErr		= $response['sql'];
				$rollBack=1;
			}
		}
   		
		if($rollBack==1){
			$db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= $messageErr;
			$response['sql']		= $sqlErr;
		}
		else if($savedStatus){
			$db->commit();
			$response['type'] 		= "pass";
 			$response['msg'] 		= "Rejected successfully.";
 		}else{
			$db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= $messageErr;
			$response['sql']		= $sqlErr;
		}
		echo json_encode($response);
	}
	
	
//--------------------------------------------------------getMaxAppByStatus
function replace($str)
{
	  $str 	= 	str_replace('\\', "\\\\",	$str);
	  $str 	= 	str_replace("'","\'",	$str);
	  $str	=	str_replace("\n", "<br>",$str);  
	  
	  return $str;
}	

//--------------------------------------------------------getMaxAppByStatus
 
	?>
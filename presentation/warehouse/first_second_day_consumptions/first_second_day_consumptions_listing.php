<?php
date_default_timezone_set('Asia/Kolkata');
//ini_set('allow_url_include',1);
// ini_set('display_errors',1);
session_start();

$backwardseperator 	= "../../../";
$thisFilePath 		=  $_SERVER['PHP_SELF'];
//include_once $backwardseperator."dataAccess/permisionCheck2.inc";
include_once  "{$backwardseperator}dataAccess/Connector.php";

include_once "../../../libraries/jqdrid/inc/jqgrid_dist.php";

$company 	= $_SESSION['headCompanyId'];
$location = $_SESSION['CompanyID'];
$programCode		='P0829';
 
$intUser  = $_SESSION["userId"];

 $approveLevel = (int)getMaxApproveLevel();

$sql = "select * from(SELECT DISTINCT 
							if(tb1.STATUS=1,'Approved',if(tb1.STATUS=0,'Rejected','Pending')) as Status,
							tb1.ORDER_NO,
							tb1.ORDER_YEAR,
							tb1.SALES_ORDER_ID,
							trn_orderdetails.strSalesOrderNo,
 							tb1.CONSUMPTION_TYPE_ID,
							mst_sample_consumption_types.DESCRIPTION,
							sys_users.strFullName as `User`,
							tb1.SAVED_DATE as `Date` ,
							  
 							  
							IFNULL((
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(trn_sample_size_wise_item_consumptions_approve_by.SAVED_DATE),')' )
								FROM
								trn_sample_size_wise_item_consumptions_approve_by
								Inner Join sys_users ON trn_sample_size_wise_item_consumptions_approve_by.SAVED_BY = sys_users.intUserId
								WHERE
								trn_sample_size_wise_item_consumptions_approve_by.ORDER_NO  = tb1.ORDER_NO AND
								trn_sample_size_wise_item_consumptions_approve_by.ORDER_YEAR =  tb1.ORDER_YEAR AND
								trn_sample_size_wise_item_consumptions_approve_by.SALES_ORDER_ID  = tb1.SALES_ORDER_ID AND
								trn_sample_size_wise_item_consumptions_approve_by.CONSUMPTION_TYPE_ID =  tb1.CONSUMPTION_TYPE_ID AND 
								trn_sample_size_wise_item_consumptions_approve_by.LEVELS =  '1' AND 
								trn_sample_size_wise_item_consumptions_approve_by.STATUS='0' 
							),IF(((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.STATUS>1),'Approve', '')) as `1st_Approval`,  ";
							
						for($i=2; $i<=$approveLevel; $i++){
							
							if($i==2){
							$approval="2nd_Approval";
							}
							else if($i==3){
							$approval="3rd_Approval";
							}
							else {
							$approval=$i."th_Approval";
							}
							
							
						$sql .= "IFNULL(
						/*condition*/
(
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(trn_sample_size_wise_item_consumptions_approve_by.SAVED_DATE),')' )
								FROM
								trn_sample_size_wise_item_consumptions_approve_by
								Inner Join sys_users ON trn_sample_size_wise_item_consumptions_approve_by.SAVED_BY = sys_users.intUserId
								WHERE
								trn_sample_size_wise_item_consumptions_approve_by.ORDER_NO  = tb1.ORDER_NO AND
								trn_sample_size_wise_item_consumptions_approve_by.ORDER_YEAR =  tb1.ORDER_YEAR AND
								trn_sample_size_wise_item_consumptions_approve_by.SALES_ORDER_ID  = tb1.SALES_ORDER_ID AND
								trn_sample_size_wise_item_consumptions_approve_by.CONSUMPTION_TYPE_ID =  tb1.CONSUMPTION_TYPE_ID AND 
								trn_sample_size_wise_item_consumptions_approve_by.LEVELS =  '$i' AND 
								trn_sample_size_wise_item_consumptions_approve_by.STATUS='0' 
							),
							/*end condition*/
							
							/*false part*/

							IF(
							/*if condition */
							((SELECT
								menupermision.int".$i."Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND (tb1.STATUS>1) AND (tb1.STATUS<=tb1.LEVELS) AND ((SELECT
								max(trn_sample_size_wise_item_consumptions_approve_by.SAVED_DATE) 
								FROM
								trn_sample_size_wise_item_consumptions_approve_by
								Inner Join sys_users ON trn_sample_size_wise_item_consumptions_approve_by.SAVED_BY = sys_users.intUserId
								WHERE
								trn_sample_size_wise_item_consumptions_approve_by.ORDER_NO  = tb1.ORDER_NO AND
								trn_sample_size_wise_item_consumptions_approve_by.ORDER_YEAR =  tb1.ORDER_YEAR AND 
								trn_sample_size_wise_item_consumptions_approve_by.SALES_ORDER_ID  = tb1.SALES_ORDER_ID AND
								trn_sample_size_wise_item_consumptions_approve_by.CONSUMPTION_TYPE_ID =  tb1.CONSUMPTION_TYPE_ID AND 
								trn_sample_size_wise_item_consumptions_approve_by.LEVELS =  ($i-1) AND 
								trn_sample_size_wise_item_consumptions_approve_by.STATUS='0' )<>'')),
								
								/*end if condition*/
								/*if true part*/
								'Approve',
								/*fase part*/
								 if($i>tb1.LEVELS,'-----',''))
								
								/*end IFNULL false part*/
								) as `".$approval."`, "; 
									
								}
								
 								
							$sql .= "'View' as `View`   
							FROM
							trn_sample_size_wise_item_consumptions_header as tb1 
							INNER JOIN mst_sample_consumption_types ON tb1.CONSUMPTION_TYPE_ID = mst_sample_consumption_types.ID
							INNER JOIN sys_users ON tb1.SAVED_BY = sys_users.intUserId
							INNER JOIN trn_orderdetails ON tb1.ORDER_NO = trn_orderdetails.intOrderNo AND tb1.ORDER_YEAR = trn_orderdetails.intOrderYear AND tb1.SALES_ORDER_ID = trn_orderdetails.intSalesOrderId
							)  as t where 1=1
						";
					  	//echo $sql;
						 
$col = array();

//STATUS
$col["title"] 	= "Status"; // caption of column
$col["name"] 	= "Status"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
//edittype
$col["stype"] 	= "select";
$str = ":All;Pending:Pending;Approved:Approved;Rejected:Rejected" ;
$col["editoptions"] 	=  array("value"=> $str);
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$cols[] = $col;	$col=NULL;

//PO No
$col["title"] 	= "Order No"; // caption of column
$col["name"] 	= "ORDER_NO"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "2";
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$col['link']	= "first_second_day_consumptions.php?order_year={ORDER_YEAR}&order_no={ORDER_NO}&sales_order={SALES_ORDER_ID}&type={CONSUMPTION_TYPE_ID}";	 
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag

$reportLink  = "rpt_first_second_day_consumptions.php?order_year={ORDER_YEAR}&order_no={ORDER_NO}&sales_order={SALES_ORDER_ID}&type={CONSUMPTION_TYPE_ID}";
$reportLinkApprove  = "rpt_first_second_day_consumptions.php?order_year={ORDER_YEAR}&order_no={ORDER_NO}&sales_order={SALES_ORDER_ID}&type={CONSUMPTION_TYPE_ID}&approveMode=1&mode=Confirm";
$reportLinkPrint  = "rpt_first_second_day_consumptions.php?order_year={ORDER_YEAR}&order_no={ORDER_NO}&sales_order={SALES_ORDER_ID}&type={CONSUMPTION_TYPE_ID}&mode=print";

$cols[] = $col;	$col=NULL;
 


//PO Year
$col["title"] = "Order Year"; // caption of column
$col["name"] = "ORDER_YEAR"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//PO Year
$col["title"] = "Sales Order  No"; // caption of column
$col["name"] = "strSalesOrderNo"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

 //PO Year
$col["title"] = "Day Type"; // caption of column
$col["name"] = "DESCRIPTION"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//PO Year
$col["title"] = "Day Type Id"; // caption of column
$col["name"] = "CONSUMPTION_TYPE_ID"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$col["hidden"] = true;   
$cols[] = $col;	$col=NULL;

//PO Year
$col["title"] = "Sales Order No Id"; // caption of column
$col["name"] = "SALES_ORDER_ID"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$col["hidden"] = true;   
$cols[] = $col;	$col=NULL;


 //Supplier
$col["title"] = "Date"; // caption of column
$col["name"] = "Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

 
//User
$col["title"] = "User"; // caption of column
$col["name"] = "User"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;



//FIRST APPROVAL
$col["title"] = "1st Approval"; // caption of column
$col["name"] = "1st_Approval"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

for($i=2; $i<=$approveLevel; $i++){
	if($i==2){
	$ap="2nd Approval";
	$ap1="2nd_Approval";
	}
	else if($i==3){
	$ap="3rd Approval";
	$ap1="3rd_Approval";
	}
	else {
	$ap=$i."th Approval";
	$ap1=$i."th_Approval";
	}
//SECOND APPROVAL
$col["title"] = $ap; // caption of column
$col["name"] = $ap1; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;
}

 //VIEW
$col["title"] = "Report"; // caption of column
$col["name"] = "View"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

$jq = new jqgrid('',$db);

$grid["caption"] 		= "Day Consumption Listing";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'ORDER_NO'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes


// export XLS file
// export to excel parameters - range could be "all" or "filtered"
//$grid["export"] = array("format"=>"xlsx", "filename"=>"my-file", "sheetname"=>"test");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);




$out = $jq->render("list1");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Day Consumption Listing</title>

<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />


</head>

<body>	
<form id="frmlisting" name="frmlisting" method="post" action="">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
	<link rel="stylesheet" type="text/css" media="screen" href="../../../libraries/jqdrid/js/themes/smoothness/jquery-ui.custom.css"></link>	
	<link rel="stylesheet" type="text/css" media="screen" href="../../../libraries/jqdrid/js/jqgrid/css/ui.jqgrid.css"></link>	
	
	<script src="../../../libraries/jqdrid/js/jquery.min.js" type="text/javascript"></script>
	<script src="../../../libraries/jqdrid/js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
	<script src="../../../libraries/jqdrid/js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>	
	<script src="../../../libraries/jqdrid/js/themes/jquery-ui.custom.min.js" type="text/javascript"></script>
	<script src="../../../libraries/javascript/script.js" type="text/javascript"></script>
    
    <td><table width="100%" border="0">
      <tr>
        <td>
        <div align="center" style="margin:10px">
        
			<?php echo $out?>
        </div>
        </td>
      </tr>

    </table></td>
    </tr>


</form>
</body>
</html>
<?php

//------------------------------function load Default Department---------------------
function getMaxApproveLevel(){
	global $db;
	
	//echo $savedStat;
	$appLevel=0;
	$sqlp = "SELECT
			Max(trn_sample_size_wise_item_consumptions_header.LEVELS) AS appLevel
			FROM trn_sample_size_wise_item_consumptions_header";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['appLevel'];
}
//------------------------------function load User Department---------------------
 ?>


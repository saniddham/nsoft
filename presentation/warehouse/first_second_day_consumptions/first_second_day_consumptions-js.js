var arrSizes;



			
$(document).ready(function() {
   //-------------------------------------------- 
 	//$('#frmDayConsump #cboOrderYear').live('change',load_orders);
  //-------------------------------------------- 
 	//$('#frmDayConsump #cboOrderNo').live('change',load_salesOrders);
 
  //-------------------------------------------- 
 	$('#frmDayConsump #loadSalesOrders').live('click',load_salesOrders);
 //-------------------------------------------- 
 	$('#frmDayConsump #cboSalesOrderNo').live('change',load_sizes);
 //-------------------------------------------- 
 	$('#frmDayConsump #butSearch').live('click',load_items);
 //-------------------------------------------- 
 	$('#frmDayConsump #cboType').live('change',load_items_type);
 //-------------------------------------------- 
   	$('#frmDayConsump #butSave').live('click',save_data);
 //-------------------------------------------- 
   	$('#frmDayConsump #butConfirm').live('click', viewConfirm);
 //-------------------------------------------- 
   	$('#frmDayConsump #butReport').live('click', viewReport);
 //-------------------------------------------- 
 	$('#frmRptConsumptions #butRptConfirm').live('click',ConfirmRpt);
 //-------------------------------------------- 
	$('#frmRptConsumptions #butRptReject').live('click',RejectRpt);
 //-------------------------------------------- 
 

	});
	
	
function load_orders()
{
  		var year = $('#cboOrderYear').val();
		var url 		= "first_second_day_consumptions-db.php?requestType=loadOrders&year="+year;
		var httpobj 	= $.ajax({url:url,async:false})
		$('#frmDayConsump #cboOrderNo').html(httpobj.responseText)
}
function load_salesOrders()
{
 		var orderNo = $('#cboOrderNo').val();
 		var year = $('#cboOrderYear').val();
		var url 		= "first_second_day_consumptions-db.php?requestType=loadSalesOrder&orderNo="+orderNo+"&year="+year;
		var httpobj 	= $.ajax({url:url,async:false})
		$('#frmDayConsump #cboSalesOrderNo').html(httpobj.responseText)
}
function load_sizes()
{
	var orderNo 		= $('#cboOrderNo').val();
	var year 			= $('#cboOrderYear').val();
	var salesOrderId 	= $('#cboSalesOrderNo').val();
	var type		 	= $('#cboType').val();
	
	var url 		= "first_second_day_consumptions-db.php?requestType=loadSizes";
	
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		data:"orderNo="+orderNo+"&year="+year+"&salesOrderNo="+salesOrderId, 
		async:false,
		success:function(json){
  		  arrSizes = json.arrData;
 		}
		
 	});
	
	var content='<tr class="normalfnt">';
	content +='<th align="center" bgcolor="#FFFFFF" width="8%" >Sample No</th>';
	content +='<th align="center" bgcolor="#FFFFFF" width="8%" >Graphic</th>';
	content +='<th align="center" bgcolor="#FFFFFF" width="4%" >Revision</th>';
	content +='<th align="center" bgcolor="#FFFFFF" width="8%" >Combo</th>';
	content +='<th align="center" bgcolor="#FFFFFF" width="8%"   nowrap="nowrap" >Part</th>';
	content +='<th align="center" bgcolor="#FFFFFF" width="10%"  nowrap="nowrap">Main Category</th>';
	content +='<th align="center" bgcolor="#FFFFFF" width="10%"  nowrap="nowrap">Sub Category</th>';
	content +='<th align="center" bgcolor="#FFFFFF" width="20%"  nowrap="nowrap">Item</th>';
	content +='<th align="center" bgcolor="#FFFFFF" width="5%">UOM</th>';
	content +='<th align="center" bgcolor="#FFFFFF" width="5%">conPC</th>';
	
	if(arrSizes ){
		for(var j=0;j<arrSizes.length;j++){
			var width= (22/arrSizes.length)+'%';
			
			content +='<th align="center" bgcolor="#FFFFFF" width="'+width+'">'+arrSizes[j]+'</th>';
		}
		content +='</tr>';
	}

	clearRows();
	document.getElementById('tblMain').deleteRow(0);
	add_new_row('#frmDayConsump #tblMain',content);
	if(orderNo != '' && year != '' && salesOrderId != '' && type !='' )
	load_items();	
}
function load_items()
{
	clearRows();
	
	var orderNo 		= $('#cboOrderNo').val();
	var year 			= $('#cboOrderYear').val();
	var salesOrderId 	= $('#cboSalesOrderNo').val();
	var salesOrderNo 	= $('#cboSalesOrderNo option:selected').text();
	var type		 	= $('#cboType').val();
	var saved_flag		=0;
	
	if(orderNo==''){
		alert("Please select the Order No");
		return false;
	}
	else if(year==''){
		alert("Please select the Order Year");
		return false;
	}
	else if(salesOrderId==''){
		alert("Please select the Sales Order No");
		return false;
	}
	else if(type==''){
		alert("Please select the type");
		return false;
	}

	var url 		= "first_second_day_consumptions-db.php?requestType=loadItems";
	
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		data:"orderNo="+orderNo+"&year="+year+"&salesOrderNo="+salesOrderId+"&type="+type, 
		async:false,
		success:function(json){

			var length = json.arrCombo.length;
			var arrCombo = json.arrCombo;

 			var perm_save		=arrCombo[0].savePermision
			var perm_app    	=arrCombo[0].confPermision;
			
			if(perm_save==1)
				$('#butSave').show();
			else
				$('#butSave').hide();
				
 
			if(perm_app==1) 
				$('#butConfirm').show();
 			else
				$('#butConfirm').hide();

		
			for(var i=0;i<length;i++)
			{
				
				var intSampleNo		=arrCombo[i]['intSampleNo'];	
				var intSampleYear	=arrCombo[i]['intSampleYear'];	
				var intRevisionNo	=arrCombo[i]['intRevisionNo'];
				var strCombo		= arrCombo[i]['strCombo'];
				var strPrintName	= arrCombo[i]['strPrintName'];
				var strGraphicRefNo	= arrCombo[i]['strGraphicRefNo'];
				var partId			= arrCombo[i]['partId'];
				var partName		= arrCombo[i]['partName'];
				
				var itemId			=arrCombo[i]['itemId'];	
				var maincatId		=arrCombo[i]['maincatId'];	
				var subCatId		=arrCombo[i]['subCatId'];
				var mainCatName		= arrCombo[i]['mainCatName'];
				var subCatName		= arrCombo[i]['subCatName'];
			
 				var code			=arrCombo[i]['code'];	
 				var itemName		=arrCombo[i]['itemName'];
 				var uom				=arrCombo[i]['uom'];
				var conPC			=arrCombo[i]['consum'];
				var savedCons		=arrCombo[i]['savedCons'];	
					saved_flag		+=arrCombo[i]['saved_flag'];	
 				
 				var content='<tr class="normalfnt">';
 				content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'" class="sample">'+intSampleNo+'/'+intSampleYear+'</td>';
 				content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'" class="">'+strGraphicRefNo+'</td>';
 				content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'" class="">'+intRevisionNo+'</td>';
 				content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'" class="">'+strCombo+'</td>';
 				content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'" class="" nowrap="nowrap">'+partName+'</td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'" class="mainCatName"  nowrap="nowrap">'+mainCatName+'</td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'" class="subCatName"  nowrap="nowrap">'+subCatName+'</td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="item"  nowrap="nowrap">'+itemName+'</td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="'+uom+'" class="uom">'+uom+'</td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="'+conPC+'" class="uom">'+conPC+'</td>';
 		
				if(arrSizes ){
					for(var j=0;j<arrSizes.length;j++){
						content +='<td align="center" bgcolor="#FFFFFF" id=""><input  id="'+savedCons[arrSizes[j]]+'" class="validate[required,custom[number]] '+arrSizes[j]+' calculateValue Qty" style="width:80px;text-align:center" type="text" value="'+savedCons[arrSizes[j]]+'"/></td>';
					}
					content +='</tr>';
				}


 				add_new_row('#frmDayConsump #tblMain',content);
				
			}
				//checkAlreadySelected();

		}
	});
	
	if(saved_flag ==1){
		$("#butReport").show();  
	}
	else{
		$("#butReport").hide();  
	}
	

}

 //----------------------------------------------
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
//----------------------------------------------- 
function clearRows()
{
	//var rowCount = $('#tblDispatchPopup >tbody >tr').length;
	var rowCount = document.getElementById('tblMain').rows.length;
	//alert(rowCount);
	//var cellCount = document.getElementById('tblMain').rows[1].cells.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblMain').deleteRow(1);
			
	}
}
 //---------------------------------------------------
function save_data(){
 	showWaiting();

	if(!Validate_save()){
		hideWaiting();
		setTimeout("alertx('')",4000);
		return;
	}
	
	var data 	= "order_no="+$('#cboOrderNo').val();
		data   += "&year="+$('#cboOrderYear').val();
		data   += "&sales_order="+$('#cboSalesOrderNo').val();
		data   += "&type="+$('#cboType').val();
		data   += "&date="+$('#dtDate').val();
		//data   += "&note="	+	URLEncode_json($('#txtRemarks').val());
 
	var arr		= "[";
	$('#tblMain .item').each(function(){
		for(var j=0;j<arrSizes.length;j++){
			
			arr += "{";
 			var classNm= '.'+arrSizes[j];
			arr += '"size":"'+arrSizes[j]+'",';
			arr += '"qty":"'+($(this).parent().find(classNm).val())+'",';
			arr += '"item":"'+($(this).parent().find('.item').attr('id'))+'"';
 			arr +=  '},';
		}
	});

	arr 		 = arr.substr(0,arr.length-1);
	arr 		+= " ]"; 
 
 	var url 	= "first_second_day_consumptions-db.php?requestType=save";
		data 	+= "&arr="+arr;
   		
	var httpobj = $.ajax({
	url:url,
	data:data,
	dataType:'json',
	async:false,
	success:function(json)
		{
			$('#frmDayConsump #butSave').validationEngine('showPrompt', json.msg,json.type );
			if(json.type=='pass')
			{
			//var length 			= json.arrCombo.length;
			//var arrCombo 		= json.arrCombo;
 			var perm_save		=json.savePermision
			var perm_app    	=json.confPermision;
			
			if(perm_save==1)
				$('#butSave').show();
			else
				$('#butSave').hide();
				
 
			if(perm_app==1)
				$('#butConfirm').show();
 			else
				$('#butConfirm').hide();
			}
			
		},
	error:function(xhr,status)
		{
			$('#frmDayConsump #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
		}
	});
	var t = setTimeout("alertx('#butSave')",4000);
	hideWaiting();
}
	
function Validate_save()
{

	var orderNo 	 = $('#cboOrderNo').val();
	var year 		 = $('#cboOrderYear').val();
	var salesOrderId = $('#cboSalesOrderNo').val();
	var salesOrderNo = $('#cboSalesOrderNo option:selected').text();
	var type		 = $('#cboType').val();

	if(year==''){
		alert("Please select the Order Year");
		return false;
	}
	else if(orderNo==''){
		alert("Please select the Order No");
		return false;
	}
	else if(salesOrderId==''){
		alert("Please select the Sales Order No");
		return false;
	}
	else if(type==''){
		alert("Please select the type");
		return false;
	}
	else
	return true;
}
function alertx(but)
{
	$('#frmDayConsump '+but).validationEngine('hide')	;
}

 function loadData(orderNo,year,salesOrder,type){
	 
	$('#cboOrderNo').val(orderNo);
	$('#cboOrderYear').val(year);
	
	 load_salesOrders();
	 $('#cboSalesOrderNo').val(salesOrder)
	 $('#cboType').val(type)
	 load_sizes();
 	 load_items();
 }
 function viewReport()
{
  	var url  = "rpt_first_second_day_consumptions.php?order_year="+$('#frmDayConsump #cboOrderYear').val();
	    url += "&order_no="+$('#frmDayConsump #cboOrderNo').val();
	    url += "&sales_order="+$('#frmDayConsump #cboSalesOrderNo').val();
	    url += "&type="+$('#frmDayConsump #cboType').val();
	    url += "&mode=";
	window.open(url,'rpt_first_second_day_consumptions.php');
}
function viewConfirm()
{
  	var url  = "rpt_first_second_day_consumptions.php?order_year="+$('#frmDayConsump #cboOrderYear').val();
	    url += "&order_no="+$('#frmDayConsump #cboOrderNo').val();
	    url += "&sales_order="+$('#frmDayConsump #cboSalesOrderNo').val();
	    url += "&type="+$('#frmDayConsump #cboType').val();
		
		
	    url += "&mode=Confirm";
	window.open(url,'rpt_first_second_day_consumptions.php');
}

 function ConfirmRpt(){
	 
	var val = $.prompt('Are you sure you want to approve this Consumptions ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					var url = "first_second_day_consumptions-db.php"+window.location.search+'&requestType=approve';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmRptConsumptions #butRptConfirm').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmRptConsumptions #butRptConfirm').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						}
					   hideWaiting();
				}});
  }
  
  
  function RejectRpt(){
	  
	var val = $.prompt('Are you sure you want to reject this Consumptions ?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{
				showWaiting();
				var url = "first_second_day_consumptions-db.php"+window.location.search+'&requestType=reject';
				var obj = $.ajax({
					url:url,
					type:'post',
					dataType: "json",  
					data:'',
					async:false,
					
					success:function(json){
							$('#frmRptConsumptions #butRptReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
							if(json.type=='pass')
							{
								var t=setTimeout("alertx()",1000);
								window.location.href = window.location.href;
								window.opener.location.reload();//reload listing page
								return;
							}
						},
					error:function(xhr,status){
							
							$('#frmRptConsumptions #butRptReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
							var t=setTimeout("alertx()",3000);
						}		
					});
				////////////
					}
					hideWaiting();
			}});
	  
  }
  
  function load_items_type(){
	clearRows();

	var orderNo 		= $('#cboOrderNo').val();
	var year 			= $('#cboOrderYear').val();
	var salesOrderId 	= $('#cboSalesOrderNo').val();
	var type		 	= $('#cboType').val();
	
	if(orderNo != '' && year != '' && salesOrderId != '' && type !='' )
	load_items();	
	  
  }


var basePath	=	"presentation/warehouse/styleStockMovement/";			
$(document).ready(function() {

	$('#styleMovement #cboCompany').change(function(){
	var company = $('#styleMovement #cboCompany').val();
	var url 		= basePath+"styleStockMovement-db-get.php?requestType=loadLocations&company="+company;
	var httpobj 	= $.ajax({url:url,async:false})
	document.getElementById('cboLocation').innerHTML=httpobj.responseText;
	
	});

	$(".search").live('change',loadAllCombo);
	
	$(".searchAll").live('click',submitForm);

});//------------------------------------------------------------------------

function loadAllCombo()
{
	//######################
	//####### get values ###
	//######################
	var year 		= $('#cboOrderYear').val();
	var graphicNo 	= $('#cboGraphicNo').val();
	var styleId 	= $('#cboStyleNo').val();
	var combo 	= $('#cboCombo').val();
	var revNo 	= $('#cboRevNo').val();
	var printName 	= $('#cboPrintName').val();
	var orderNo		= $('#cboOrderNo').val();
	var company		= $('#cboCompany').val();
	var location		= $('#cboLocation').val();
	var Item		= $('#cboItem').val();
	var sampNo		= $('#cboSampNo').val();
	var sampYear		= $('#cboSampYear').val();
	var salesOrderNo		= $('#cboSalesOrderNo').val();
	
	//######################
	//####### create url ###
	//######################
	var url		=basePath+"styleStockMovement-db-get.php?requestType=loadAllComboDetails";
		url	   +="&year="+			year;	
		url	   +="&graphicNo="+		graphicNo;
		url	   +="&styleId="+		styleId;
		url	   +="&combo="+	combo;
		url	   +="&revNo="+		revNo;
		url	   +="&printName="+	printName;
		url	   +="&orderNo="+	orderNo;
		url	   +="&company="+		company;
		url	   +="&location="+	location;
		url	   +="&Item="+	Item;
		url	   +="&sampNo="+	sampNo;
		url	   +="&sampYear="+	sampYear;
		url	   +="&salesOrderNo="+	salesOrderNo;
	//######################
	//####### send data  ###
	//######################
	$.ajax({url:url,
			async:false,
			dataType:'json',
			success:function(json){
				//alert(json.styleNo);
					$('#cboStyleNo').html(json.styleNo);
					$('#cboSampNo').html(json.sampNo);
					$('#cboSampYear').html(json.sampYear);
					$('#cboGraphicNo').html(json.graphicNo);
					$('#cboPrintName').html(json.printName);
					$('#cboCombo').html(json.combo);
					$('#cboRevNo').html(json.revNo);
					$('#cboSalesOrderNo').html(json.salesOrderNo);
					
/*					$('#cboGraphicNo').html(json.graphicNo);
					$('#cboPrintName').html(json.printName);
					$('#cboCombo').html(json.combo);
					$('#cboRevNo').html(json.revNo);
					$('#cboOrderNo').html(json.orderNo);
					$('#cboOrderYear').html(json.year);
					$('#cboItem').html(json.Item);
*/			}
			});
}

function submitForm()
{
	var year 		= $('#cboOrderYear').val();
	var graphicNo 	= $('#cboGraphicNo').val();
	var styleId 	= $('#cboStyleNo').val();
	var combo 	= $('#cboCombo').val();
	var revNo 	= $('#cboRevNo').val();
	var printName 	= $('#cboPrintName').val();
	var orderNo		= $('#cboOrderNo').val();
	var company		= $('#cboCompany').val();
	var location		= $('#cboLocation').val();
	var Item		= $('#cboItem').val();
	var sampNo		= $('#cboSampNo').val();
	var sampYear		= $('#cboSampYear').val();
	var salesOrderNo		= $('#cboSalesOrderNo').val();
	var dateFrom		= $('#txtFromDate').val();
	var dateTo		= $('#txtToDate').val();
				var path='?q='+965;
	document.location.href = path+"&orderNo="+orderNo+"&orderYear="+year+"&styleNo="+styleId+"&combo="+combo+"&revNo="+revNo+"&printName="+printName+"&graphicNo="+graphicNo+"&company="+company+"&location="+location+"&Item="+Item+"&sampNo="+sampNo+"&sampYear="+sampYear+"&salesOrderNo="+salesOrderNo+"&dateFrom="+dateFrom+"&dateTo="+dateTo;
}


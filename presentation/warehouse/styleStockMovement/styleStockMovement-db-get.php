<?php
	session_start();
	$backwardseperator = "../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	///
	/////////// type of print load part /////////////////////
	if($requestType=='loadLocations')
	{
		$company  = $_REQUEST['company'];
		$sql = "SELECT
				mst_locations.intId,
				mst_locations.strName
				FROM mst_locations
				WHERE
				mst_locations.intCompanyId =  '$company' order by strName";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	else if($requestType=='loadStyleNo')
	{
		$location  = $_REQUEST['location'];

		 $sql = "SELECT DISTINCT 
				trn_orderdetails.strStyleNo
				FROM
				trn_orderdetails
				Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
				WHERE
				trn_orderheader.intLocationId =  '$location' 
				ORDER BY trn_orderdetails.strStyleNo ASC";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['strStyleNo']."\">".$row['strStyleNo']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	else if($requestType=='loadSubCategory')
	{
		$mainCategory  = $_REQUEST['mainCategory'];
		$sql = "SELECT
				mst_subcategory.intId,
				mst_subcategory.strCode,
				mst_subcategory.strName
				FROM mst_subcategory
				WHERE
				mst_subcategory.intMainCategory =  '$mainCategory' order by strName";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	else if($requestType=='loadItems')
	{
		 $mainCategory  = $_REQUEST['mainCategory'];
		$subCategory  = $_REQUEST['subCategory'];
		$sql = "SELECT
				mst_item.intId,
				mst_item.strName
				FROM mst_item
				WHERE
				mst_item.intMainCategory =  '$mainCategory' AND
				mst_item.intSubCategory =  '$subCategory' order by strName";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	else if($requestType=='loadCurrency')
	{
		$company  = $_REQUEST['company'];
		$sql = "SELECT
				mst_companies.intBaseCurrencyId
				FROM mst_companies
				WHERE
				mst_companies.intId =  '$company'";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
			$currency = $row['intBaseCurrencyId'];
		echo $currency;
		//echo json_encode($response);
	}
	else if($requestType=='loadAllComboDetails')
	{
		$year				= $_REQUEST['year'];
		$graphicNo			= $_REQUEST['graphicNo'];
		$styleId			= $_REQUEST['styleId'];
		$sampNo			= $_REQUEST['sampNo'];
		$combo		= $_REQUEST['combo'];
		$printName			= $_REQUEST['printName'];
		$revNo			= $_REQUEST['revNo'];
		$orderNo			= $_REQUEST['orderNo'];
		$company		= $_REQUEST['company'];
		$location			= $_REQUEST['location'];
		$Item			= $_REQUEST['Item'];
		$sampYear			= $_REQUEST['sampYear'];
		$salesOrderNo			= $_REQUEST['salesOrderNo'];
		
		echo loadAllComboDetails($year,$graphicNo,$styleId,$sampNo,$sampYear,$combo,$printName,$revNo,$orderNo,$company,$location,$Item,$salesOrderNo);	
	}
	
	
	
	
	
	
function loadAllComboDetails($year,$graphicNo,$styleId,$sampNo,$sampYear,$combo,$printName,$revNo,$orderNo,$company,$location,$Item,$salesOrderNo)
{
		global $db;
		
		//#########################
		//######### STYLE NO ######
		//#########################
			$para='';
		if($orderNo!='')
			$para.=" AND trn_orderheader.intOrderNo =  '$orderNo' ";
		if($year!='')
			$para.=" AND trn_orderheader.intOrderYear =  '$year'  ";
		if($styleId!='')
			$para.=" AND trn_orderdetails.strStyleNo =  '$styleId'   ";
		if($graphicNo!='')
			$para.=" AND trn_orderdetails.strGraphicNo =  '$graphicNo' ";
		if($sampNo!='')
			$para.=" AND trn_orderdetails.intSampleNo =  '$sampNo'   ";	
		if($sampYear!='')
			$para.=" AND trn_orderdetails.intSampleYear =  '$sampYear'   ";	
		if($combo!='')
			$para.=" AND trn_orderdetails.strCombo =  '$combo'    ";	
		if($printName!='')
			$para.=" AND trn_orderdetails.strPrintName =  '$printName'   ";	
		if($revNo!='')
			$para.=" AND trn_orderdetails.intRevisionNo =  '$revNo'   ";	
		if($location!='')
			$para.=" AND trn_orderheader.intLocationId =  '$location'   ";	
		if($salesOrderNo!='')
			$para.=" AND trn_orderdetails.intSalesOrderId =  '$salesOrderNo'   ";	

		  $sql = "SELECT DISTINCT 
					trn_orderdetails.strStyleNo
					FROM
					trn_orderheader
					Inner Join trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
					WHERE
					trn_orderheader.intStatus=1
					$para
				ORDER BY trn_orderdetails.strStyleNo ASC
				";
		$result = $db->RunQuery($sql);
		$options = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$options .= "<option ".($styleId==$row['strStyleNo']?'selected':'')." value=\"".$row['strStyleNo']."\">".$row['strStyleNo']."</option>";
		}
		$response['styleNo'] = $options;
	 	$response['styleNo_sql'] = $sql;
		
		
		//######### SAMPLE NO ####
		//#########################
			$para='';
		if($orderNo!='')
			$para.=" AND trn_orderheader.intOrderNo =  '$orderNo' ";
		if($year!='')
			$para.=" AND trn_orderheader.intOrderYear =  '$year'  ";
		if($styleId!='')
			$para.=" AND trn_orderdetails.strStyleNo =  '$styleId'   ";
		if($graphicNo!='')
			$para.=" AND trn_orderdetails.strGraphicNo =  '$graphicNo' ";
		if($sampNo!='')
			$para.=" AND trn_orderdetails.intSampleNo =  '$sampNo'   ";	
		if($sampYear!='')
			$para.=" AND trn_orderdetails.intSampleYear =  '$sampYear'   ";	
		if($combo!='')
			$para.=" AND trn_orderdetails.strCombo =  '$combo'    ";	
		if($printName!='')
			$para.=" AND trn_orderdetails.strPrintName =  '$printName'   ";	
		if($revNo!='')
			$para.=" AND trn_orderdetails.intRevisionNo =  '$revNo'   ";	
		if($location!='')
			$para.=" AND trn_orderheader.intLocationId =  '$location'   ";	
		if($salesOrderNo!='')
			$para.=" AND trn_orderdetails.intSalesOrderId =  '$salesOrderNo'   ";	

		  $sql = "SELECT DISTINCT 
					trn_orderdetails.intSampleNo
					FROM
					trn_orderheader
					Inner Join trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
					WHERE
					trn_orderheader.intStatus=1
					$para
				ORDER BY trn_orderdetails.intSampleNo DESC
				";
		$result = $db->RunQuery($sql);
		$options = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$options .= "<option ".($sampNo==$row['intSampleNo']?'selected':'')." value=\"".$row['intSampleNo']."\">".$row['intSampleNo']."</option>";
		}
		$response['sampNo'] = $options;
	 	$response['sampNo_sql'] = $sql;
		
		//######### SAMPLE YEAR ####
		//#########################
			$para='';
		if($orderNo!='')
			$para.=" AND trn_orderheader.intOrderNo =  '$orderNo' ";
		if($year!='')
			$para.=" AND trn_orderheader.intOrderYear =  '$year'  ";
		if($styleId!='')
			$para.=" AND trn_orderdetails.strStyleNo =  '$styleId'   ";
		if($graphicNo!='')
			$para.=" AND trn_orderdetails.strGraphicNo =  '$graphicNo' ";
		if($sampNo!='')
			$para.=" AND trn_orderdetails.intSampleNo =  '$sampNo'   ";	
		if($sampYear!='')
			$para.=" AND trn_orderdetails.intSampleYear =  '$sampYear'   ";	
		if($combo!='')
			$para.=" AND trn_orderdetails.strCombo =  '$combo'    ";	
		if($printName!='')
			$para.=" AND trn_orderdetails.strPrintName =  '$printName'   ";	
		if($revNo!='')
			$para.=" AND trn_orderdetails.intRevisionNo =  '$revNo'   ";	
		if($location!='')
			$para.=" AND trn_orderheader.intLocationId =  '$location'   ";	
		if($salesOrderNo!='')
			$para.=" AND trn_orderdetails.intSalesOrderId =  '$salesOrderNo'   ";	

		  $sql = "SELECT DISTINCT 
					trn_orderdetails.intSampleYear
					FROM
					trn_orderheader
					Inner Join trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
					WHERE
					trn_orderheader.intStatus=1
					$para
				ORDER BY trn_orderdetails.intSampleYear DESC
				";
		$result = $db->RunQuery($sql);
		$options = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$options .= "<option ".($sampYear==$row['intSampleYear']?'selected':'')." value=\"".$row['intSampleYear']."\">".$row['intSampleYear']."</option>";
		}
		$response['sampYear'] = $options;
	 	$response['sampYear_sql'] = $sql;
		
		//######### GRAPHIC NO ####
		//#########################
			$para='';
		if($orderNo!='')
			$para.=" AND trn_orderheader.intOrderNo =  '$orderNo' ";
		if($year!='')
			$para.=" AND trn_orderheader.intOrderYear =  '$year'  ";
		if($styleId!='')
			$para.=" AND trn_orderdetails.strStyleNo =  '$styleId'   ";
		if($graphicNo!='')
			$para.=" AND trn_orderdetails.strGraphicNo =  '$graphicNo' ";
		if($sampNo!='')
			$para.=" AND trn_orderdetails.intSampleNo =  '$sampNo'   ";	
		if($sampYear!='')
			$para.=" AND trn_orderdetails.intSampleYear =  '$sampYear'   ";	
		if($combo!='')
			$para.=" AND trn_orderdetails.strCombo =  '$combo'    ";	
		if($printName!='')
			$para.=" AND trn_orderdetails.strPrintName =  '$printName'   ";	
		if($revNo!='')
			$para.=" AND trn_orderdetails.intRevisionNo =  '$revNo'   ";	
		if($location!='')
			$para.=" AND trn_orderheader.intLocationId =  '$location'   ";	
		if($salesOrderNo!='')
			$para.=" AND trn_orderdetails.intSalesOrderId =  '$salesOrderNo'   ";	

		  $sql = "SELECT DISTINCT 
					trn_orderdetails.strGraphicNo
					FROM
					trn_orderheader
					Inner Join trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
					WHERE
					trn_orderheader.intStatus=1
					$para
				ORDER BY trn_orderdetails.strGraphicNo ASC
				";
		$result = $db->RunQuery($sql);
		$options = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$options .= "<option ".($graphicNo==$row['strGraphicNo']?'selected':'')." value=\"".$row['strGraphicNo']."\">".$row['strGraphicNo']."</option>";
		}
		$response['graphicNo'] = $options;
	 	$response['graphicNo_sql'] = $sql;
		
		//#########PRINT NAME ####
		//#########################
			$para='';
		if($orderNo!='')
			$para.=" AND trn_orderheader.intOrderNo =  '$orderNo' ";
		if($year!='')
			$para.=" AND trn_orderheader.intOrderYear =  '$year'  ";
		if($styleId!='')
			$para.=" AND trn_orderdetails.strStyleNo =  '$styleId'   ";
		if($graphicNo!='')
			$para.=" AND trn_orderdetails.strGraphicNo =  '$graphicNo' ";
		if($sampNo!='')
			$para.=" AND trn_orderdetails.intSampleNo =  '$sampNo'   ";	
		if($sampYear!='')
			$para.=" AND trn_orderdetails.intSampleYear =  '$sampYear'   ";	
		if($combo!='')
			$para.=" AND trn_orderdetails.strCombo =  '$combo'    ";	
		if($printName!='')
			$para.=" AND trn_orderdetails.strPrintName =  '$printName'   ";	
		if($revNo!='')
			$para.=" AND trn_orderdetails.intRevisionNo =  '$revNo'   ";	
		if($location!='')
			$para.=" AND trn_orderheader.intLocationId =  '$location'   ";	
		if($salesOrderNo!='')
			$para.=" AND trn_orderdetails.intSalesOrderId =  '$salesOrderNo'   ";	

		  $sql = "SELECT DISTINCT 
					trn_orderdetails.strPrintName
					FROM
					trn_orderheader
					Inner Join trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
					WHERE
					trn_orderheader.intStatus=1
					$para
				ORDER BY trn_orderdetails.strPrintName ASC
				";
		$result = $db->RunQuery($sql);
		$options = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$options .= "<option ".($printName==$row['strPrintName']?'selected':'')." value=\"".$row['strPrintName']."\">".$row['strPrintName']."</option>";
		}
		$response['printName'] = $options;
	 	$response['printName_sql'] = $sql;
		
		//#########Revision No ####
		//#########################
			$para='';
		if($orderNo!='')
			$para.=" AND trn_orderheader.intOrderNo =  '$orderNo' ";
		if($year!='')
			$para.=" AND trn_orderheader.intOrderYear =  '$year'  ";
		if($styleId!='')
			$para.=" AND trn_orderdetails.strStyleNo =  '$styleId'   ";
		if($graphicNo!='')
			$para.=" AND trn_orderdetails.strGraphicNo =  '$graphicNo' ";
		if($sampNo!='')
			$para.=" AND trn_orderdetails.intSampleNo =  '$sampNo'   ";	
		if($sampYear!='')
			$para.=" AND trn_orderdetails.intSampleYear =  '$sampYear'   ";	
		if($combo!='')
			$para.=" AND trn_orderdetails.strCombo =  '$combo'    ";	
		if($printName!='')
			$para.=" AND trn_orderdetails.strPrintName =  '$printName'   ";	
		if($revNo!='')
			$para.=" AND trn_orderdetails.intRevisionNo =  '$revNo'   ";	
		if($location!='')
			$para.=" AND trn_orderheader.intLocationId =  '$location'   ";	
		if($salesOrderNo!='')
			$para.=" AND trn_orderdetails.intSalesOrderId =  '$salesOrderNo'   ";	

		  $sql = "SELECT DISTINCT 
					trn_orderdetails.intRevisionNo
					FROM
					trn_orderheader
					Inner Join trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
					WHERE
					trn_orderheader.intStatus=1
					$para
				ORDER BY trn_orderdetails.intRevisionNo ASC
				";
		$result = $db->RunQuery($sql);
		$options = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$options .= "<option ".($revNo==$row['intRevisionNo']?'selected':'')." value=\"".$row['intRevisionNo']."\">".$row['intRevisionNo']."</option>";
		}
		$response['revNo'] = $options;
	 	$response['revNo_sql'] = $sql;
		
		//#########COMBO ####
		//#########################
			$para='';
		if($orderNo!='')
			$para.=" AND trn_orderheader.intOrderNo =  '$orderNo' ";
		if($year!='')
			$para.=" AND trn_orderheader.intOrderYear =  '$year'  ";
		if($styleId!='')
			$para.=" AND trn_orderdetails.strStyleNo =  '$styleId'   ";
		if($graphicNo!='')
			$para.=" AND trn_orderdetails.strGraphicNo =  '$graphicNo' ";
		if($sampNo!='')
			$para.=" AND trn_orderdetails.intSampleNo =  '$sampNo'   ";	
		if($sampYear!='')
			$para.=" AND trn_orderdetails.intSampleYear =  '$sampYear'   ";	
		if($combo!='')
			$para.=" AND trn_orderdetails.strCombo =  '$combo'    ";	
		if($printName!='')
			$para.=" AND trn_orderdetails.strPrintName =  '$printName'   ";	
		if($revNo!='')
			$para.=" AND trn_orderdetails.intRevisionNo =  '$revNo'   ";	
		if($location!='')
			$para.=" AND trn_orderheader.intLocationId =  '$location'   ";	
		if($salesOrderNo!='')
			$para.=" AND trn_orderdetails.intSalesOrderId =  '$salesOrderNo'   ";	

		  $sql = "SELECT DISTINCT 
					trn_orderdetails.strCombo
					FROM
					trn_orderheader
					Inner Join trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear
					WHERE
					trn_orderheader.intStatus=1
					$para
				ORDER BY trn_orderdetails.strCombo ASC
				";
		$result = $db->RunQuery($sql);
		$options = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$options .= "<option ".($combo==$row['strCombo']?'selected':'')." value=\"".$row['strCombo']."\">".$row['strCombo']."</option>";
		}
		$response['combo'] = $options;
	 	$response['combo_sql'] = $sql;

		
		//#########SALES ORDER NO ##########
		//#########################
			$para='';
		if($orderNo!='')
			$para.=" AND trn_orderheader.intOrderNo =  '$orderNo' ";
		if($year!='')
			$para.=" AND trn_orderheader.intOrderYear =  '$year'  ";
		if($styleId!='')
			$para.=" AND trn_orderdetails.strStyleNo =  '$styleId'   ";
		if($graphicNo!='')
			$para.=" AND trn_orderdetails.strGraphicNo =  '$graphicNo' ";
		if($sampNo!='')
			$para.=" AND trn_orderdetails.intSampleNo =  '$sampNo'   ";	
		if($sampYear!='')
			$para.=" AND trn_orderdetails.intSampleYear =  '$sampYear'   ";	
		if($combo!='')
			$para.=" AND trn_orderdetails.strCombo =  '$combo'    ";	
		if($printName!='')
			$para.=" AND trn_orderdetails.strPrintName =  '$printName'   ";	
		if($revNo!='')
			$para.=" AND trn_orderdetails.intRevisionNo =  '$revNo'   ";	
		if($location!='')
			$para.=" AND trn_orderheader.intLocationId =  '$location'   ";	
		if($salesOrderNo!='')
			$para.=" AND trn_orderdetails.intSalesOrderId =  '$salesOrderNo'   ";	

		  $sql = "SELECT DISTINCT  
					trn_orderdetails.intSalesOrderId,
					trn_orderdetails.strSalesOrderNo, 
					mst_part.strName 
					FROM
					trn_orderheader
					Inner Join trn_orderdetails ON trn_orderheader.intOrderNo = trn_orderdetails.intOrderNo AND trn_orderheader.intOrderYear = trn_orderdetails.intOrderYear 
					Inner Join mst_part ON trn_orderdetails.intPart = mst_part.intId
					WHERE
					trn_orderheader.intStatus=1
					$para
				ORDER BY trn_orderdetails.strCombo ASC
				";
		$result = $db->RunQuery($sql);
		$options = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$options .= "<option ".($salesOrderId==$row['intSalesOrderId']?'selected':'')." value=\"".$row['intSalesOrderId']."\">".$row['strSalesOrderNo']."/".$row['strName']."</option>";
		}
		$response['salesOrderNo'] = $options;
	 	$response['salesOrderNo_sql'] = $sql;
		
		return json_encode($response);
}
	
?>	
	

<?php
session_start();
$backwardseperator = "../../../";
$companyId = $_SESSION['CompanyID'];
$locationId 	= $_SESSION['CompanyID'];//this locationId use in report header(reportHeader.php)-------
$intUser  = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
include  	"{$backwardseperator}dataAccess/Connector.php";

$company = $_REQUEST['company'];
$location = $_REQUEST['location'];

$mainCat = $_REQUEST['mainCat'];
$subCat = $_REQUEST['subCat'];
$item = $_REQUEST['item'];
$fromDate 	= $_REQUEST['fromDate'];
$toDate 	= $_REQUEST['toDate'];

 $sql = "SELECT
			mst_companies.strName
			FROM mst_companies
		WHERE
			mst_companies.intId =  '$company'
		";
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
$companyName = $row['strName'];
	
	
				 
 $sql = "SELECT
			mst_locations.strName
		FROM
			mst_locations
		WHERE
			mst_locations.intId =  '$location'
		";
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
$locationName = $row['strName'];

$sql = "SELECT
			mst_subcategory.strName AS subName,
			mst_maincategory.strName AS mainName,
			mst_item.strName AS itemName
		FROM
		mst_maincategory
			Inner Join mst_subcategory ON mst_maincategory.intId = mst_subcategory.intMainCategory
			Inner Join mst_item ON mst_subcategory.intId = mst_item.intSubCategory
		WHERE
			mst_item.intId =  '$item'
		";
$result = $db->RunQuery($sql);
$row=mysqli_fetch_array($result);
$subName = $row['subName'];
$mainName = $row['mainName'];
$itemName = $row['itemName'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Stock Balance Report</title>
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />


<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../mrn/listing/rptMrn-js.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/script.js"></script>

<script src="../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.min.js"></script>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:264px;
	top:247px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<form id="frmMrnReport" name="frmMrnReport" method="post" action="../mrn/listing/rptMrn.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include '../../../reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>STOCK MOVEMENT REPORT</strong><strong></strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
  <tr>
    <td width="7%">&nbsp;</td>
    <td width="19%" class="normalfntMid" align="center"><strong>Main Category</strong></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="28%" align="left"><span class="normalfnt"><?php echo $mainName  ?></span></td>
    <td width="11%"><span class="normalfnt"><strong style="text-align:right">Company</strong></span></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="20%"><span class="normalfnt"><strong style="text-align:right"><span class="normalfnt" style="text-align:left"><?php echo $companyName  ?></span></strong></span></td>
    <td width="6%" class="normalfntMid">&nbsp;</td>
    <td width="7%">&nbsp;</td>
  </tr>
 
  <tr>
    <td>&nbsp;</td>
    <td class="normalfntMid"><strong>Sub Category</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $subName  ?></span></td>
    <td><span class="normalfnt"><strong>Location</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><strong style="text-align:right"><span class="normalfnt" style="text-align:left"><?php echo $locationName;  ?></span></strong></span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfntMid"><strong>Item</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $itemName;  ?></span></td>
    <td>&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>

  
  </table>
  </td>
</tr>
<tr>
  <td><table width="1058"  cellspacing="1" cellpadding="0"  >

      <?PHP
	  if($fromDate!='')
		 {
			$datePara = " AND  MAINSTOCK.dtGRNDate>='$fromDate' AND MAINSTOCK.dtGRNDate<='$toDate' ";	 
		 }
	  	 $sql = "SELECT
					MAINSTOCK.intItemId,
					MAINSTOCK.dtGRNDate,
					trn_podetails.intPONo,
					trn_podetails.intPOYear,
					Sum(trn_podetails.dblQty) AS poQty,
					MAINSTOCK.intGRNNo,
					MAINSTOCK.intGRNYear,
					Sum(MAINSTOCK.dblQty) AS grnQty,
					IFNULL(mst_supplier.strName,'OPEN STOCK') AS supplierName
					FROM
					ware_stocktransactions_bulk as MAINSTOCK
					left Join ware_grnheader ON ware_grnheader.intGrnNo = MAINSTOCK.intGRNNo AND ware_grnheader.intGrnYear = MAINSTOCK.intGRNYear
					left Join trn_podetails ON trn_podetails.intPONo = ware_grnheader.intPoNo AND trn_podetails.intPOYear = ware_grnheader.intPoYear AND MAINSTOCK.intItemId = trn_podetails.intItem
					left Join trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
					left Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
				
					WHERE
					(MAINSTOCK.strType =  'GRN' OR MAINSTOCK.strType =  'OPENSTOCK')  AND
					MAINSTOCK.intLocationId =  '$location' AND
					MAINSTOCK.intItemId =  '$item' 
					
					$datePara
					
					GROUP BY
					MAINSTOCK.strType,
					MAINSTOCK.dtGRNDate,
					MAINSTOCK.intLocationId,
					MAINSTOCK.intItemId,
					MAINSTOCK.intGRNNo,
					MAINSTOCK.intGRNYear,
					trn_podetails.intPONo,
					trn_podetails.intPOYear
					";
			$result = $db->RunQuery($sql);
			$i=0;//RTSUP
			while($row=mysqli_fetch_array($result))
			{
				$grnNo 		= $row['intGRNNo'];
				$grnYear	= $row['intGRNYear'];
				$itemNo 	= $item;
				 
	  ?>
      		    <tr>
      <td width="104" height="20" align="center" bgcolor="#469EE6" style="color:#FFF;font-size:11px"><strong>GRN DATE</strong></td>
      <td width="140" align="center" bgcolor="#469EE6" style="color:#FFF;font-size:11px"><strong>PO NO</strong></td>
      <td width="370" align="center" bgcolor="#469EE6" style="color:#FFF;font-size:11px"><strong>SUPPLIER</strong></td>
      <td width="148" align="center" bgcolor="#469EE6" style="color:#FFF;font-size:11px"><strong>PO QTY</strong></td>
      <td width="119" align="center" bgcolor="#469EE6" style="color:#FFF;font-size:11px"><strong>GRN NO</strong></td>
      <td width="168" align="center" bgcolor="#469EE6" style="color:#FFF;font-size:11px"><strong>GRN QTY</strong></td>
      </tr>
            <tr bgcolor="#FFE1C4" >
              <td class="normalfntMid"><?php echo $row['dtGRNDate']; ?></td>
              <td class="normalfntMid"><?php echo $row['intPOYear'].'/'.$row['intPONo']; ?></td>
              <td class="normalfntMid"><?php echo $row['supplierName']; ?></td>
              <td class="normalfntMid"><?php echo $row['poQty']; ?></td>
              <td class="normalfntMid"><?php echo $row['intGRNYear'].'/'.$row['intGRNNo']; ?></td>
              <td class="normalfntMid" style="font-size:14px;color:#C63"><b><?php echo $row['grnQty']; ?></b></td>
            </tr>
            <tr>
            	<td colspan="6">
                	<table width="100%">
                    <tr>
                    	<td width="52%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="23%" style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;RETURN STORES NO</td>
                              <td width="44%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">&nbsp;</td>
                              <td width="33%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                            </tr>
                            <?PHP 
								$sql1 = "SELECT
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear,
											ware_stocktransactions_bulk.dblQty
										FROM ware_stocktransactions_bulk
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType =  'RETSTORES' AND
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
										";
								$result1 = $db->RunQuery($sql1);
								while($row1=mysqli_fetch_array($result1))
								{
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                              <td>&nbsp;</td>
                              <td class="normalfntRight"><?php echo $row1['dblQty']; ?></td>
                            </tr>
                            <?php
								}	
							?>
                          </table>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">

                            <tr>
                              <td width="23%" style="color:#666" height="16" bgcolor="#EBFAC7" class="normalfnt">&nbsp;TRANSFER IN NO</td>
                              <td width="44%" style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">FROM COMPANY</td>
                              <td width="33%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                            </tr>
                                  <?PHP 
								$sql1 = "SELECT
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear,
											ware_stocktransactions_bulk.dblQty
										FROM ware_stocktransactions_bulk
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType =  'TRANSFERIN' AND
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
										";
								$result1 = $db->RunQuery($sql1);
								while($row1=mysqli_fetch_array($result1))
								{
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                              <td>&nbsp;</td>
                              <td class="normalfntRight"><?php echo $row1['dblQty']; ?></td>
                            </tr>
                            <?php
								}	
							?>
                          </table>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
          
                            <tr>
                              <td width="23%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;UN-ALLOCATION NO</td>
                              <td width="44%"  style="color:#666" bgcolor="#EBFAC7">&nbsp;</td>
                              <td width="33%"  style="color:#666" bgcolor="#EBFAC7">&nbsp;</td>
                            </tr>
                                  <?PHP 
								$sql1 = "SELECT
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear,
											ware_stocktransactions_bulk.dblQty
										FROM ware_stocktransactions_bulk
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType =  'UNALLOCATE' AND
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
										";
								$result1 = $db->RunQuery($sql1);
								while($row1=mysqli_fetch_array($result1))
								{
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                              <td>&nbsp;</td>
                              <td class="normalfntRight"><?php echo $row1['dblQty']; ?></td>
                            </tr>
                            <?php
								}	
							?>
                          </table>
                        </td>
                        <td width="48%"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  
                          <tr>
                            <td width="23%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;ISSUE NO</td>
                            <td width="44%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">ISSUE TO </td>
                            <td width="33%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                          </tr>
                                <?PHP 
								 $sql1 = "SELECT
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear,
											ware_stocktransactions_bulk.dblQty*-1 AS dblQty,
											mst_department.strName
										FROM ware_stocktransactions_bulk
										inner join ware_issueheader on ware_issueheader.intIssueNo=ware_stocktransactions_bulk.intDocumentNo and ware_issueheader.intIssueYear=ware_stocktransactions_bulk.intDocumntYear
										inner join mst_department on mst_department.intId=ware_issueheader.intDepartment
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType =  'ISSUE' AND
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
										";
								$result1 = $db->RunQuery($sql1);
								while($row1=mysqli_fetch_array($result1))
								{
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                              <td><?php echo $row1['strName'] ?></td>
                              <td class="normalfntRight"><?php echo $row1['dblQty']; ?></td>
                            </tr>
                            <?php
								}	
							?>
                        </table>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
              
                            <tr>
                              <td width="23%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;RETURN SUP NO</td>
                              <td width="44%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">SUPPLIER</td>
                              <td width="33%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                            </tr>
                                  <?PHP 
								$sql1 = "SELECT
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear,
											ware_stocktransactions_bulk.dblQty * -1 as dblQty
										FROM ware_stocktransactions_bulk
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType =  'RTSUP' AND
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
										";
								$result1 = $db->RunQuery($sql1);
								while($row1=mysqli_fetch_array($result1))
								{
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                              <td><span class="normalfntMid"><?php echo $row['supplierName']; ?></span></td>
                              <td class="normalfntRight"><?php echo $row1['dblQty']; ?></td>
                            </tr>
                            <?php
								}	
							?>
                        </table>
                          <table width="100%" border="0" cellspacing="0" cellpadding="0">
                     
                            <tr>
                              <td width="23%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;GATEPASS NO</td>
                              <td width="44%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">TO COMPANY</td>
                              <td width="33%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                            </tr>
                                  <?PHP 
								$sql1 = "SELECT
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear,
											ware_stocktransactions_bulk.dblQty * -1 as dblQty
										FROM ware_stocktransactions_bulk
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType =  'GATEPASS' AND
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
										";
								$result1 = $db->RunQuery($sql1);
								while($row1=mysqli_fetch_array($result1))
								{
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                              <td>&nbsp;</td>
                              <td class="normalfntRight"><?php echo $row1['dblQty']; ?></td>
                            </tr>
                            <?php
								}	
							?>
                        </table><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        
                            <tr>
                              <td width="23%"  style="color:#666" bgcolor="#EBFAC7" class="normalfnt">&nbsp;ALLOCATION NO</td>
                              <td width="44%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">STYLE NO</td>
                              <td width="33%"  style="color:#666" bgcolor="#EBFAC7" class="normalfntMid">QTY</td>
                            </tr>
                                  <?PHP 
								$sql1 = "SELECT
											ware_stocktransactions_bulk.intDocumentNo,
											ware_stocktransactions_bulk.intDocumntYear,
											ware_stocktransactions_bulk.dblQty * -1 as dblQty
										FROM ware_stocktransactions_bulk
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.strType =  'ALLOCATE' AND
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
										";
								$result1 = $db->RunQuery($sql1);
								while($row1=mysqli_fetch_array($result1))
								{
							?>
                            <tr class="normalfnt">
                              <td class="normalfntMid"><?php echo $row1['intDocumntYear'].'/'.$row1['intDocumentNo'] ?></td>
                              <td>&nbsp;</td>
                              <td class="normalfntRight"><?php echo $row1['dblQty']; ?></td>
                            </tr>
                            <?php
								}	
							?>
                        </table></td>
                    </tr>
                    </table>
                </td>
            </tr>
            <tr >
              <td colspan="6" class="normalfntMid"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="52%">&nbsp;</td>
                  <td width="32%" bgcolor="#E9E6E0">Stock Balance </td>
                  <?PHP 
								 $sql1 = "SELECT
											sum(ware_stocktransactions_bulk.dblQty) as balQty
										FROM ware_stocktransactions_bulk
										WHERE
											ware_stocktransactions_bulk.intGRNNo =  '$grnNo' AND
											ware_stocktransactions_bulk.intGRNYear =  '$grnYear' AND
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
										";
								$result1 = $db->RunQuery($sql1);
								$row1=mysqli_fetch_array($result1);
				?>
                  <td width="16%" bgcolor="#E9E6E0" style="font-size:14px;text-align:right;color:#06F"><?php echo $row1['balQty']; ?></td>
                </tr>
              </table></td>
            </tr>
            <tr >
              <td colspan="6" bgcolor="#CCCCCC" class="normalfntMid"></td>
            </tr>
    <?php
			}
			if($grnNo=='')
			{
				echo "<tr><td align=\"middle\" colspan=\"6\">&nbsp;NO RECORD FOUND</td></tr>";	
			}
	?>

    		<table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="52%">&nbsp;</td>
                  <td width="32%" bgcolor="#E9E6E0"><strong>Total Item Balance</strong></td>
                  <?PHP 
								  $sql1 = "SELECT
											sum(ware_stocktransactions_bulk.dblQty) as balQty
										FROM ware_stocktransactions_bulk
										WHERE
											ware_stocktransactions_bulk.intItemId =  '$itemNo' and intLocationId='$location'
										";
								$result1 = $db->RunQuery($sql1);
								$row1=mysqli_fetch_array($result1);
				?>
                  <td width="16%" bgcolor="#E9E6E0" style="font-size:18px;text-align:right;color:#06F;border-top:groove;border-bottom:double"><?php echo $row1['balQty']; ?></td>
                </tr>
              </table>
    		<tr >
              <td colspan="6" class="normalfntMid"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                  <td width="52%">&nbsp;</td>
                  <td width="48%">&nbsp;</td>
                </tr>
              </table></td>
            </tr>
  </table></td>
</tr>

<tr height="40">
  <td align="center" class="normalfntMid">&nbsp;</td>
</tr>
</table>
</div>        
</form>
</body>
</html>
<?php
session_start();
$backwardseperator = "../../../";
$mainPath = $_SESSION['mainPath'];
$location 	= $_SESSION['CompanyID'];
$company 	= $_SESSION['headCompanyId'];
$thisFilePath =  $_SERVER['PHP_SELF'];
$intUser  = $_SESSION["userId"];

//include  	"{$backwardseperator}dataAccess/permisionCheck.inc";
include  	"{$backwardseperator}dataAccess/Connector.php";


?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Stock Movement</title>
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $backwardseperator; ?>css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">


<script type="application/javascript" src="../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="styleStockMovement-js.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" type="text/css" href="../../../libraries/calendar/theme.css" />
<script src="../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
</head>

<body>
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
<style type="text/css">

.fixHeader thead tr { display: block; }
.fixHeader tbody { display: block;  overflow: auto; }
</style>
<script src="../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.min.js"></script>


<form id="styleMovement" name="styleMovement" method="get" action="styleStockMovement.php">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutS">
		  <div class="trans_text">Style Stock Movement</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
          <td width="34%" height="27" class="normalfnt">Company</td>
            <td width="51%"><select name="cboCompany" id="cboCompany" style="width:250px"  class="validate[required] txtText" >
              <option value=""></option>
              <?php
					$sql = "SELECT
							mst_companies.intId,
							mst_companies.strName
							FROM mst_companies
							WHERE
							mst_companies.intStatus =  '1'";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
            </select></td>
            <td width="5%">&nbsp;</td>
            <td width="10%">&nbsp;</td>
          </tr>
          <tr>
            <td height="27" class="normalfnt">Location</td>
            <td width="51%"><select name="cboLocation" id="cboLocation" style="width:250px"  class="validate[required] txtText" >
              <option value=""></option>
              </select></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td height="27" class="normalfnt">Order No</td>
            <td><select name="cboOrderNo" id="cboOrderNo" style="width:250px"  class="validate[required] txtText" >
              <option value=""></option>
              <?php
					$sql = "SELECT DISTINCT
							trn_orderdetails.intOrderNo,
							trn_orderdetails.intOrderYear
							FROM
							trn_orderdetails
							Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
							WHERE
							trn_orderheader.intStatus =  '1'";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intOrderNo']."\">".$row['intOrderNo']."</option>";	
					}
				?>
            </select></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td height="27" class="normalfnt">Style No</td>
            <td><select name="cboStyleNo" id="cboStyleNo" style="width:250px"  class="validate[required] txtText" >
              <option value=""></option>
              <?php
					$sql = "SELECT DISTINCT
trn_orderdetails.intOrderNo,
trn_orderdetails.intOrderYear
FROM
trn_orderdetails
Inner Join trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
WHERE
trn_orderheader.intStatus =  '1'";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
            </select></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td height="27" class="normalfnt">Sales Order No</td>
            <td><select name="cboSalesOrderNo" id="cboSalesOrderNo" style="width:180px"  class="validate[required] txtText" >
              <option value=""></option>
            </select><select style="width:70px" name="cboOrderYear" id="cboOrderYear" class="validate[required] txtText">
      <option value="2012">2012</option>
    </select></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td height="27" class="normalfnt">Main Category</td>
            <td width="51%"><select name="cboMainCategory" style="width:250px" id="cboMainCategory">
              <option value=""></option>
              <?php
					$sql = "SELECT
							mst_maincategory.intId,
							mst_maincategory.strName
							FROM mst_maincategory";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
              </select></td>
            <td></td>
            <td></td>
          </tr>          <tr>
          <td height="27" class="normalfnt">Sub Category</td>
            <td width="51%"><select name="cboSubCategory" id="cboSubCategory" style="width:250px"  class="validate[required] txtText" >
              <option value=""></option>
            </select></td>
            <td></td>
            <td></td>
          </tr>
          <tr>
            <td height="27" class="normalfnt">Item </td>
            <td><select name="cboItems" id="cboItems" style="width:250px"  class="validate[required] txtText" >
              <option value=""></option>
            </select></td>
            <td>&nbsp;</td>
            <td></td>
          </tr>
          <tr>
            <td height="27" class="normalfnt">Date</td>
            <td colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="10%" class="normalfnt"><input name="chkDate" type="checkbox" id="chkDate"  /></td>
                <td width="12%" class="normalfnt">From</td>
                <td width="35%"><input name="dtDate" type="text" disabled="disabled"  class="txtbox" id="txtFromDate" style="width:80px;"  onclick="return showCalendar(this.id, '%Y-%m-%d');" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                <td width="8%" class="normalfnt">To</td>
                <td width="35%"><input name="dtDate2" type="text" disabled="disabled"  class="txtbox" id="txtToDate" style="width:80px;"  onclick="return showCalendar(this.id, '%Y-%m-%d');" onmousedown="DisableRightClickEvent();" onmouseout="EnableRightClickEvent();" onkeypress="return ControlableKeyAccess(event);"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
              </tr>
            </table></td>
          </tr>
          <tr>
          <td height="27" class="normalfnt">&nbsp;</td>
            <td colspan="3"><table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="31%" class="normalfnt">Normal Report</td>
                <td width="18%"><input name="radio" type="radio" class="mouseover" id="chkNormal" value="1" checked="checked" /></td>
                <td width="29%" class="normalfnt">Details Report</td>
                <td width="7%"><input name="radio" type="radio" class="mouseover" id="chkDetails" value="radio" /></td>
                <td width="7%">&nbsp;</td>
                <td width="8%">&nbsp;</td>
              </tr>
            </table></td>
        </tr>
          <tr>
          <td colspan="4"><table width="100%" border="0" cellpadding="0" cellspacing="0"><tr>
          <td width="30%" height="27" class="normalfnt"></td>
          <td height="27" align="center" ><img src="../../../images/Treport.jpg" width="92" height="24" class="mouseover" id="imgSearchItems" name="imgSearchItems"  /></td>
          <td width="26%" height="27" class="normalfnt">&nbsp;</td>
          <td width="15%" height="27" class="normalfnt">&nbsp;</td>
          </tr></table></td>
          </tr>
        </table></td>
      </tr>
<tr>
      </tr>      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="92%">&nbsp;</td>
            <td width="8%">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td></td>
      </tr>
<?php
	       $intStatus;
			if($intStatus==''){
			$savedStat = (int)getApproveLevel($programName);
			}
			else{
			    $sqla = "SELECT
						ware_mrnheader.intStatus, 
						ware_mrnheader.intApproveLevels 
						FROM ware_mrnheader
						WHERE
						ware_mrnheader.intMrnNo =  '$mrnNo' AND
						ware_mrnheader.intMrnYear =  '$year'";	
							
					 $resulta = $db->RunQuery($sqla);
					 $rowa=mysqli_fetch_array($resulta);
					 $savedStat=$rowa['intApproveLevels'];
					 $intStatus=$rowa['intStatus'];
			}
			$editMode=0;
			if($mrnNo=='')
			$intStatus=$savedStat+1;
			if($intStatus==0)//rejected
			$editMode=1;
			else if($savedStat+1==$intStatus)//saved(not confirmed
			$editMode=1;
			
			 $k=$savedStat+2-$intStatus;
			    $sqlp = "SELECT
					menupermision.int".$k."Approval 
					FROM menupermision 
					Inner Join menus ON menupermision.intMenuId = menus.intId
					WHERE
					menus.strCode =  '$programCode' AND
					menupermision.intUserId =  '$intUser'";	
							
					 $resultp = $db->RunQuery($sqlp);
					 $rowp=mysqli_fetch_array($resultp);
					 if($rowp['int'.$k.'Approval']==1){
					 $confirmatonMode=1;
					 }
					 else{
					 $confirmatonMode=0;//no user permission to confirm
					 }
			
		//	echo "1".$intStatus;
		//	echo "2".$confirmatonMode;
	  ?>      
  </table></td></table></div></div>
</form>
	<!-- items to prn -->
	<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1">
   <!-- <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php" style="width:800;height:800;border:0;overflow:hidden">
    </iframe>-->
    </div>

	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>


</body>
</html>

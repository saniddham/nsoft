<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
date_default_timezone_set('Asia/Kolkata');
//ini_set('allow_url_include',1);
// ini_set('display_errors',1);

$thisFilePath 	=  $_SERVER['PHP_SELF'];

require_once	"libraries/jqgrid2/inc/jqgrid_dist.php";

$company 		= $_SESSION['headCompanyId'];
$location 		= $_SESSION['CompanyID'];

$programName	='Supplier Return Note';
$programCode	='P0226';
 
$reportMenuId	='897';
$menuId			='226';

$intUser  		= $_SESSION["userId"];

$userDepartment	=getUserDepartment($intUser);
$approveLevel 	= (int)getMaxApproveLevel();

################################################################
################## new part ####################################
################################################################
///// create where part ///////////////////
$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];
//print_r($arr);
$where_string = '';
$where_array = array(
				'Status'=>'tb1.intStatus',
				'Return_No'=>'tb1.intReturnNo',
				'Return_Year'=>'tb1.intReturnYear',
				'PO_No'=>'trn_poheader.intPONo',
				'PO_Year'=>'trn_poheader.intPOYear',
				'GRN_No'=>'tb1.intGrnNo',
				'GRN_Year'=>'tb1.intGrnYear',
				'Invoice_No'=>'ware_grnheader.strInvoiceNo',
				'Supplier'=>'mst_supplier.strName',
				'Date'=>'tb1.datdate',
				'User'=>'sys_users.strUserName'
				);
$arr_status = array('Approved'=>'1','Rejected'=>'0','Pending'=>'2');
foreach($arr as $k=>$v)
{
	if($v['field']=='Status')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}
if(!count($arr)>0)					 
	$where_string .= "AND date(tb1.datdate) = '".date('Y-m-d')."'";
	
################## end code ####################################


$sql = "select * from(SELECT DISTINCT 
							if(tb1.intStatus=1,'Approved',if(tb1.intStatus=0,'Rejected','Pending')) as Status,
							tb1.intReturnNo as `Return_No`,
							tb1.intReturnYear as `Return_Year`,
							trn_poheader.intPONo as `PO_No`,
							trn_poheader.intPOYear as `PO_Year`,
							tb1.intGrnNo as `GRN_No`, 
							tb1.intGrnYear as `GRN_Year`, 
							ware_grnheader.strInvoiceNo as `Invoice_No`,
							mst_supplier.strName as `Supplier`,
							tb1.datdate as `Date`,
							sys_users.strUserName as `User`,


							IFNULL((SELECT
								concat(sys_users.strUserName,'(',max(ware_returntosupplierheader_approvedby.dtApprovedDate),')' )
								FROM
								ware_returntosupplierheader_approvedby
								Inner Join sys_users ON ware_returntosupplierheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_returntosupplierheader_approvedby.intReturnNo  = tb1.intReturnNo AND
								ware_returntosupplierheader_approvedby.intYear =  tb1.intReturnYear AND
								ware_returntosupplierheader_approvedby.intApproveLevelNo =  '1' AND 							                                ware_returntosupplierheader_approvedby.intStatus = '0' 
							),IF(((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.intStatus>1),'Approve', '')) as `1st_Approval`,  ";
							
						for($i=2; $i<=$approveLevel; $i++){
							
							if($i==2){
							$approval="2nd_Approval";
							}
							else if($i==3){
							$approval="3rd_Approval";
							}
							else {
							$approval=$i."th_Approval";
							}
							
							
						$sql .= "IFNULL(
						/*condition*/
(
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(ware_returntosupplierheader_approvedby.dtApprovedDate),')' )
								FROM
								ware_returntosupplierheader_approvedby
								Inner Join sys_users ON ware_returntosupplierheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_returntosupplierheader_approvedby.intReturnNo  = tb1.intReturnNo AND
								ware_returntosupplierheader_approvedby.intYear =  tb1.intReturnYear AND
								ware_returntosupplierheader_approvedby.intApproveLevelNo =  '$i' AND 							                                ware_returntosupplierheader_approvedby.intStatus = '0' 
							),
							/*end condition*/
							
							/*false part*/

							IF(
							/*if condition */
							((SELECT
								menupermision.int".$i."Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND (tb1.intStatus>1) AND (tb1.intStatus<=tb1.intApproveLevels) AND ((SELECT
								max(ware_returntosupplierheader_approvedby.dtApprovedDate) 
								FROM
								ware_returntosupplierheader_approvedby
								Inner Join sys_users ON ware_returntosupplierheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_returntosupplierheader_approvedby.intReturnNo  = tb1.intReturnNo AND
								ware_returntosupplierheader_approvedby.intYear =  tb1.intReturnYear AND
								ware_returntosupplierheader_approvedby.intApproveLevelNo =  ($i-1) AND 							                                ware_returntosupplierheader_approvedby.intStatus = '0')<>'')),
								
								/*end if condition*/
								/*if true part*/
								'Approve',
								/*fase part*/
								 if($i>tb1.intApproveLevels,'-----',''))
								
								/*end IFNULL false part*/
								) as `".$approval."`, "; 
									
								}
								
							$sql .= "'View' as `View`   
						FROM
							ware_returntosupplierheader as tb1 
							Inner Join ware_grnheader ON tb1.intGrnNo = ware_grnheader.intGrnNo AND tb1.intGrnYear = ware_grnheader.intGrnYear
							Inner Join trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear 
							Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
							Inner Join sys_users ON tb1.intUser = sys_users.intUserId 
							Inner Join mst_locations ON tb1.intCompanyId = mst_locations.intId 
							WHERE
							mst_locations.intCompanyId =  '$company' AND 
							tb1.intCompanyId =  '$location'
							$where_string
							)  as t where 1=1
						";
					  	//  echo $sql;
	
$formLink			= "?q=$menuId&retSupNo={Return_No}&year={Return_Year}";	 
$reportLink  		= "?q=$reportMenuId&retSupNo={Return_No}&year={Return_Year}";
$reportLinkApprove  = "?q=$reportMenuId&retSupNo={Return_No}&year={Return_Year}&approveMode=1";
$reportLinkPrint  	= "?q=$reportMenuId&retSupNo={Return_No}&year={Return_Year}&mode=print";
						 
$col = array();

//STATUS
$col["title"] 		= "Status"; // caption of column
$col["name"] 		= "Status"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 		= "3";
//edittype
$col["stype"] 		= "select";
$str = ":All;Pending:Pending;Approved:Approved;Rejected:Rejected" ;
$col["editoptions"]	=  array("value"=> $str);
//searchOper
$col["align"] 		= "center";
//$col["link"] 		= "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$cols[] = $col;	$col=NULL;

//PO No
$col["title"] 		= "Return No"; // caption of column
$col["name"] 		= "Return_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 		= "2";
//searchOper
$col["align"] 		= "center";
//$col["link"] 		= "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$col['link']		= $formLink;	 
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag

 
$cols[] = $col;	$col=NULL;



//PO Year
$col["title"] = "Return Year"; // caption of column
$col["name"] = "Return_Year"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//PO Year
$col["title"] = "PO No"; // caption of column
$col["name"] = "PO_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;



//PO Year
$col["title"] = "PO Year"; // caption of column
$col["name"] = "PO_Year"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//grn Year
$col["title"] = "GRN No"; // caption of column
$col["name"] = "GRN_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;



//PO Year
$col["title"] = "GRN Year"; // caption of column
$col["name"] = "GRN_Year"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//Delivery Date
$col["title"] = "Invoice No"; // caption of column
$col["name"] = "Invoice_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "left";

$cols[] = $col;	$col=NULL;



//Delivery Date
$col["title"] = "Supplier"; // caption of column
$col["name"] = "Supplier"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "left";
$cols[] = $col;	$col=NULL;


//Supplier
$col["title"] = "Date"; // caption of column
$col["name"] = "Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//User
$col["title"] = "User"; // caption of column
$col["name"] = "User"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;



//FIRST APPROVAL
$col["title"] = "1st Approval"; // caption of column
$col["name"] = "1st_Approval"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

for($i=2; $i<=$approveLevel; $i++){
	if($i==2){
	$ap="2nd Approval";
	$ap1="2nd_Approval";
	}
	else if($i==3){
	$ap="3rd Approval";
	$ap1="3rd_Approval";
	}
	else {
	$ap=$i."th Approval";
	$ap1=$i."th_Approval";
	}
//SECOND APPROVAL
$col["title"] = $ap; // caption of column
$col["name"] = $ap1; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;
}



//VIEW
$col["title"] = "Report"; // caption of column
$col["name"] = "View"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

$jq = new jqgrid('',$db);

$grid["caption"] 		= "Return To Supplier Note Listing";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'Return_No'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes


// export XLS file
// export to excel parameters - range could be "all" or "filtered"
//$grid["export"] = array("format"=>"xlsx", "filename"=>"my-file", "sheetname"=>"test");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);


$out = $jq->render("list1");
?>
<title>Return to Supplier Note Listing</title>

<form id="frmlisting" name="frmlisting" method="post" action="">

        <div align="center" style="margin:10px">        
			<?php echo $out?>
        </div>
</form>
 <?php

//------------------------------function load Default Department---------------------
function getMaxApproveLevel(){
	global $db;
	
	//echo $savedStat;
	$appLevel=0;
	$sqlp = "SELECT
			Max(ware_returntosupplierheader.intApproveLevels) AS appLevel
			FROM ware_returntosupplierheader";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['appLevel'];
}
//------------------------------function load User Department---------------------
function getUserDepartment($intUser){
	global $db;

	$sqlp = "SELECT
			sys_users.intDepartmentId
			FROM
			sys_users
			WHERE
			sys_users.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['intDepartmentId'];

}
?>


// JavaScript Document
var basePath = "presentation/warehouse/returnToSupplier/listing/";

$(document).ready(function() {
	$('#frmReterunToSupplierReport').validationEngine();
	
	$('#frmReterunToSupplierReport #imgApprove').click(function(){
	var val = $.prompt('Are you sure you want to approve this Return Note ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
						if(validateQuantities()==0){
							var url = basePath+"rptReturnToSupplier-db-set.php"+window.location.search+'&status=approve';
							var obj = $.ajax({url:url,async:false});
							window.location.href = window.location.href;
							window.opener.location.reload();//reload listing page
						}
					}
				}});
	});
	
	$('#frmReterunToSupplierReport #imgReject').click(function(){
	var val = $.prompt('Are you sure you want to reject this Return Note ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										if(validateRejecton()==0){
											var url = basePath+"rptReturnToSupplier-db-set.php"+window.location.search+'&status=reject';
											var obj = $.ajax({url:url,async:false});
											window.location.href = window.location.href;
											window.opener.location.reload();//reload listing page
										}
									}
								}});
	});
	
});
//---------------------------------------------------
function validateQuantities(){
		var ret=0;	
	    var grnNo = document.getElementById('divGrnNo').innerHTML;
	    var returnNo = document.getElementById('divReturnNo').innerHTML;
		var url 		= basePath+"rptReturnToSupplier-db-get.php?requestType=getValidation";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"grnNo="+grnNo+"&returnNo="+returnNo,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#imgApprove').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertx()",1000);
					  ret= 1;
					}
				}
			});
			return ret;
}
//---------------------------------------------------
function validateRejecton(){
		var ret=0;	
	    var grnNo = document.getElementById('divGrnNo').innerHTML;
	    var returnNo = document.getElementById('divReturnNo').innerHTML;
		var url 		= basePath+"rptReturnToSupplier-db-get.php?requestType=validateRejecton";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"grnNo="+grnNo+"&returnNo="+returnNo,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#imgApprove').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertx()",3000);
					  ret= 1;
					}
				}
			});
			return ret;

}
//------------------------------------------------
function alertx()
{
	$('#frmReterunToSupplierReport #imgApprove').validationEngine('hide')	;
}
//--------------------------------------------
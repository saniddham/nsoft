<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$companyId = $_SESSION['CompanyID'];
$intUser  = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
 
$retSupNo = $_REQUEST['retSupNo'];
$year = $_REQUEST['year'];
$approveMode = $_REQUEST['approveMode'];

$programName='Supplier Return Note';
$programCode='P0226';
 
/*$retSupNo = '100000';
$year = '2012';
$approveMode=1;*/

 $sql = "SELECT
mst_companies.strName as delToCompany,
mst_locations.strName as delToLocation,
ware_returntosupplierheader.datdate,
ware_returntosupplierheader.intStatus,
ware_returntosupplierheader.intApproveLevels,
ware_returntosupplierheader.intUser,
ware_returntosupplierheader.intCompanyId as poRaisedLocationId, 
sys_users.strUserName, 
mst_supplier.strName as supplier,
ware_grnheader.strInvoiceNo,
ware_returntosupplierheader.intGrnNo,
ware_returntosupplierheader.intGrnYear,
ware_returntosupplierheader.REMARKS 
FROM
ware_returntosupplierheader
Inner Join mst_locations ON ware_returntosupplierheader.intCompanyId = mst_locations.intId
Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
Inner Join sys_users ON ware_returntosupplierheader.intUser = sys_users.intUserId
Inner Join ware_grnheader ON ware_returntosupplierheader.intGrnNo = ware_grnheader.intGrnNo AND ware_returntosupplierheader.intGrnYear = ware_grnheader.intGrnYear
Inner Join trn_podetails ON ware_grnheader.intPoNo = trn_podetails.intPONo AND ware_grnheader.intPoYear = trn_podetails.intPOYear
Inner Join trn_poheader ON trn_podetails.intPONo = trn_poheader.intPONo AND trn_podetails.intPOYear = trn_poheader.intPOYear
Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
WHERE
ware_returntosupplierheader.intReturnNo =  '$retSupNo' AND
ware_returntosupplierheader.intReturnYear =  '$year' 
GROUP BY
ware_returntosupplierheader.intReturnNo,
ware_returntosupplierheader.intReturnYear
";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$company = $row['delToCompany'];
					$location = $row['delToLocation'];
					$locationId = $row['poRaisedLocationId'];//this locationId use in report header(reportHeader.php)--------------------
					$supplier=$row['supplier'];
					$invoiceNo=$row['strInvoiceNo'];
					$grnNo=$row['intGrnNo'];
					$grnYear=$row['intGrnYear'];
					$supReturnDate = $row['datdate'];
					$supReturnBy = $row['strUserName'];
					$intStatus = $row['intStatus'];
					$savedLevels = $row['intApproveLevels'];
					$user = $row['strUserName'];
					$createUserId = $row['intUser'];
					$remarks = $row['REMARKS'];
				 }
				 
$confirmationMode=loadConfirmatonMode($programCode,$intStatus,$savedLevels,$intUser);
$rejectionMode=loadRejectionMode($programCode,$intStatus,$savedLevels,$intUser);
?>
 <head>
 <title>SUPPLIER RETURN NOTE</title>
 <script type="application/javascript" src="presentation/warehouse/returnToSupplier/listing/rptReturnToSupplier-js.js"></script>
 <style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:274px;
	top:172px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<?php
 if($intStatus>1)//pending
{
?>
<div id="apDiv1"><img src="images/pending.png"  /></div>
<?php
}
?>
<form id="frmReterunToSupplierReport" name="frmReterunToSupplierReport" method="post">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include 'reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>SUPPLIER RETURN NOTE</strong><strong></strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
  <tr>
    <td colspan="9" align="center" bgcolor="#FFDFCB">
    <?php
	if($intStatus>1)
	{
	?>
    <?php if($approveMode==1){
			if($confirmationMode==1) { 
			?>
			<img src="images/approve.png" align="middle" class="mouseover noPrint" id="imgApprove" />
			<?php
			}
			if($rejectionMode==1) { 
			?>
            <img src="images/reject.png" align="middle" class="mouseover noPrint" id="imgReject" />
                <?php
                }
            }
	}
	?>
    </td>
  </tr>
  <tr>
  <?php
 	if($intStatus==1)
	{
	?>
   <td colspan="9" class="APPROVE" >CONFIRMED</td>
   <?PHP
	}
	else if($intStatus==0)
	{
   ?>
   <td colspan="9" class="APPROVE" style="color:#F00">REJECTED</td>
   <?php
	}
	else
	{
   ?>
   <td width="10%" colspan="9" class="APPROVE">PENDING</td>
   <?php
	}
   ?>
  </tr>
  <tr>
    <td width="1%">&nbsp;</td>
    <td width="11%"><span class="normalfnt"><strong>Return No</strong></span></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="28%"><span class="normalfnt"><?php echo $retSupNo ?>/<?php echo $year ?></span></td>
    <td width="12%" class="normalfnt"><strong>GRN NO</strong></td>
    <td width="2%" align="center" valign="middle"><strong>:</strong></td>
    <td width="22%"><span class="normalfnt"><?php echo $grnNo ?>/<?php echo $grnYear ?></span></td>
    <td width="1%"><div id="divGrnNo" style="display:none"><?php echo $grnNo ?>/<?php echo $grnYear ?></div><div id="divReturnNo" style="display:none"><?php echo $retSupNo ?>/<?php echo $year ?></div></td>
  <td width="3%"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Return By</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $user  ?></span></td>
    <td><span class="normalfnt"><strong>Invoice No</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $invoiceNo  ?></span></td>
    <td class="normalfnt">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Date</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $supReturnDate  ?></span></td>
    <td>&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  
<tr>
    <td>&nbsp;</td>
    <td class="normalfnt" valign="top"><strong>Remarks</strong></td>
    <td align="center" valign="top"><strong>:</strong></td>
    <td colspan="3"><textarea cols="50" rows="6" class="textarea" style="width:300px" disabled="disabled"><?php echo $remarks  ?></textarea></td>
    <td class="normalfnt">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    </tr>  
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td width="5%">&nbsp;</td>
        <td colspan="7" class="normalfnt">
          <table width="100%" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr >
              <th width="16%" >Main Category</th>
              <th width="16%" >Sub Category</th>
              <th width="12%" >Item Code</th>
              <th width="41%" >Item</th>
              <th width="5%" >UOM</th>
              <th width="10%" >Qty</th>
              </tr>
              <?php 
	  	   $sql1 = "SELECT
ware_returntosupplierdetails.intReturnNo,
ware_returntosupplierdetails.intReturnYear,
ware_returntosupplierdetails.intItemId,
Sum(ware_returntosupplierdetails.dblQty) as dblQty,
mst_item.strCode,
mst_item.strCode as SUP_ITEM_CODE,
mst_item.strName,
mst_item.intUOM, 
mst_maincategory.strName AS mainCategory,
mst_subcategory.strName AS subCategory, 
mst_units.strCode as uom 
FROM ware_returntosupplierdetails
Inner Join mst_item ON ware_returntosupplierdetails.intItemId = mst_item.intId
Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
Inner Join mst_units ON mst_item.intUOM = mst_units.intId
LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
WHERE
ware_returntosupplierdetails.intReturnNo =  '$retSupNo' AND
ware_returntosupplierdetails.intReturnYear =  '$year' 
GROUP BY
ware_returntosupplierdetails.intItemId  
Order by mst_maincategory.strName asc, mst_subcategory.strName asc, mst_item.strName asc  
";
			$result1 = $db->RunQuery($sql1);
			$totQty=0;
			$totAmmount=0;
			while($row=mysqli_fetch_array($result1))
			{
	  ?>
            <tr class="normalfnt"  bgcolor="#FFFFFF">
              <td class="normalfnt" >&nbsp;<?php echo $row['mainCategory']?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['subCategory'] ?>&nbsp;</td>
              <?php
              if($row['SUP_ITEM_CODE']!=null){
              ?>
               <td class="normalfnt" >&nbsp;<?php echo $row['SUP_ITEM_CODE'] ?>&nbsp;</td>
              <?php
              }else{
                  ?>
             <td class="normalfnt" >&nbsp;<?php echo $row['strCode'] ?>&nbsp;</td>
             <?php
              }
              ?>
              <td class="normalfnt" >&nbsp;<?php echo $row['strName'] ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['uom'] ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['dblQty'] ?>&nbsp;</td>
              </tr>
      <?php 
			$totQty+=$row['dblQty'];
			}
	  ?>
            <tr class="normalfnt"  bgcolor="#CCCCCC">
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfntRight" >&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $totQty ?>&nbsp;</td>
              </tr>
            </table>
          </td>
        <td width="6%">&nbsp;</td>
        </tr>
      
      </table>
    </td>
</tr>
<?php  
					 $flag=0;

					  $sqlc = "SELECT
							ware_returntosupplierheader_approvedby.intApproveUser,
							ware_returntosupplierheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName,
							ware_returntosupplierheader_approvedby.intApproveLevelNo
							FROM
							ware_returntosupplierheader_approvedby
							Inner Join sys_users ON ware_returntosupplierheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							ware_returntosupplierheader_approvedby.intReturnNo =  '$retSupNo' AND
							ware_returntosupplierheader_approvedby.intYear =  '$year'   order by dtApprovedDate asc
";
					 $resultc = $db->RunQuery($sqlc);
					while($rowc=mysqli_fetch_array($resultc)){
						if($rowc['intApproveLevelNo']==1)
						$desc="1st ";
						else if($rowc['intApproveLevelNo']==2)
						$desc="2nd ";
						else if($rowc['intApproveLevelNo']==3)
						$desc="3rd ";
						else
						$desc=$rowc['intApproveLevelNo']."th ";
					 //  $desc=$ap.$desc;
					 $desc2=$rowc['UserName']."(".$rowc['dtApprovedDate'].")";
					 if($rowc['UserName']=='')
					 $desc2='---------------------------------';
					 
					 if($rowc['intApproveLevelNo']>0){
						 $flag=1;
				?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> Approved By - </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
            </tr>
<?php
	//}
	//else{			
					}
					 if($rowc['intApproveLevelNo']==0){
						 $flag=2;
					  ?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong> Rejected By - </strong></span><span class="normalfnt"><?php echo $rowc['UserName']."(".$rowc['dtApprovedDate'].")";?></span></td>
            </tr>
<?php
					 }
					 if($rowc['intApproveLevelNo']==-1){
						 $flag=3;
					  ?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong> Revised By - </strong></span><span class="normalfnt"><?php echo $rowc['UserName']."(".$rowc['dtApprovedDate'].")";?></span></td>
            </tr>
<?php
					 }
					 
		 if($rowc['intApproveLevelNo']>0){
			 $finalSavedLevel=$rowc['intApproveLevelNo'];
		 }
	 }//end of while
	
	
	//echo $flag;
	if($flag>1){
		$finalSavedLevel=0;
	}
	
	if(($flag<=1) || ($intStatus>0)){
	for($j=$finalSavedLevel+1; $j<=$savedLevels; $j++)
	{ 
		if($j==1)
		$desc="1st ";
		else if($j==2)
		$desc="2nd ";
		else if($j==3)
		$desc="3rd ";
		else
		$desc=$j."th ";
		$desc2='---------------------------------';
	?>
        <tr>
            <td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> Approved By - </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
        </tr>
<?php
	}
	}
?>
<tr height="130" >
  <td align="center" class="normalfntMid"></td>
</tr>
<tr height="40">
<?php
	//$locationId = '';
	$sql = "SELECT mst_locations.intCompanyId FROM mst_locations WHERE mst_locations.intId =  '$locationId'";
	$result 	= $db->RunQuery($sql);
	$row		= mysqli_fetch_array($result);
	$createCompanyId = $row['intCompanyId'];
?>
<?Php 
	$url  = "presentation/send_to_approval_latest.php";		// * file name
	$url .= "?status=$intStatus";										// * set recent status
	$url .= "&approveLevels=$savedLevels";								// * set approve levels in order header
	$url .= "&programCode=$programCode";								// * program code (ex:P10001)
	$url .= "&program=RETURN TO SUPPLIER NOTE";									// * program name (ex:Purchase Order)
	$url .= "&companyId=$createCompanyId";									// * created company id
	$url .= "&createUserId=$createUserId";	
	
	$url .= "&field1=Return No";												 
	$url .= "&field2=Return Year";	
	$url .= "&value1=$retSupNo";												 
	$url .= "&value2=$year";	
 	
	$url .= "&remarksField=Remarks";	
	$url .= "&remarksValue=$remarks";	

	$url .= "&subject=RETURN TO SUPPLIER NOTE FOR APPROVAL ('$retSupNo'/'$year')";	
	
	$url .= "&statement1=Please Approve this";	
	$url .= "&statement2=to Approve this";	
							// * doc year
	$url .= "&link=".urlencode(base64_encode($mainPath."presentation/warehouse/returnToSupplier/listing/rptReturnToSupplier.php?retSupNo=$retSupNo&year=$year&approveMode=1"));
?>
  <td align="center" class="normalfntMid"><iframe id="iframeFiles2" src="<?php echo $url;?>" name="iframeFiles" style="width:500px;height:150px;border:none"  ></iframe></td>
</tr>
<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
</body>
</html>
<?php
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	$sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
//------------------------------function loadRejectionMode-------------------
function loadRejectionMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$rejectMode=0;
	$sqlp = "SELECT
		menupermision.intReject 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intReject']==1){
	 if($intStatus!=0){
	 $rejectMode=1;
	 }
	}
	 
	return $rejectMode;
}
//-------------------------------------------------------------------------
?>	

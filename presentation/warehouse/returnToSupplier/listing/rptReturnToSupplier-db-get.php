<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$companyId 	= $_SESSION['CompanyID'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	require_once $backwardseperator."class/warehouse/cls_warehouse_get.php";

	$objwhouseget= new cls_warehouse_get($db);

	$programName='Supplier Return Note';
	$programCode='P0226';
	$supReturnApproveLevel = (int)getApproveLevel($programName);
	
		$db->OpenConnection();
		$db->RunQuery2('Begin');

 if($requestType=='getValidation')
	{
		$grnNo  = $_REQUEST['grnNo'];
		$grnNoArray=explode("/",$grnNo);
		$grnNo=$grnNoArray[0];
		$grnNoYear=$grnNoArray[1];
		
		$returnNo = $_REQUEST['returnNo'];
		$returnNoArray=explode("/",$returnNo);
		$returnNo=$returnNoArray[0];
		$returnNoYear=$returnNoArray[1];
		
		///////////////////////
		$sql = "SELECT
		ware_returntosupplierheader.intStatus, 
		ware_returntosupplierheader.intApproveLevels 
		FROM ware_returntosupplierheader
		WHERE
		ware_returntosupplierheader.intReturnNo =  '$returnNo' AND
		ware_returntosupplierheader.intReturnYear =  '$returnNoYear'";

		$result = $db->RunQuery2($sql);
		$errorFlg=0;
		$row=mysqli_fetch_array($result);
		$status=$row['intStatus'];
		$approveLevels=$row['intApproveLevels'];	
		//--------------------------
		$response['arrData'] = $objwhouseget->getLocationValidation('RTSUP',$companyId,$grnNo,$grnNoYear);//check for grn location
		//--------------------------
		$confirmatonMode=loadConfirmatonMode($programCode,$status,$approveLevels,$userId);
		
		if($row['intStatus']==1){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="Final confirmation of this Return Note is already raised."; 
		}
		else if($row['intStatus']==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="This Return Note is Rejected"; 
		}
		else if($confirmatonMode==0){// 
			$errorFlg = 1;
			$msg ="No Permission to Approve"; 
		}

		////////////////////////
		if($errorFlg == 0){
		         $sql = "SELECT 
		 		mst_item.intId, 
				mst_item.strName as itemName,
				tb1.dblGrnQty,
				tb1.dblFocQty,
				ware_grnheader.intCompanyId,  
				IFNULL((SELECT
					Sum(ware_returntosupplierdetails.dblQty)
					FROM
					ware_returntosupplierdetails
					Inner Join ware_returntosupplierheader ON ware_returntosupplierheader.intReturnNo = ware_returntosupplierdetails.intReturnNo AND ware_returntosupplierheader.intReturnYear = ware_returntosupplierdetails.intReturnYear
					WHERE 
					ware_returntosupplierheader.intStatus > 0 AND 
					ware_returntosupplierheader.intStatus <=  ware_returntosupplierheader.intApproveLevels AND
					ware_returntosupplierheader.intGrnNo =  tb1.intGrnNo AND
					ware_returntosupplierheader.intGrnYear =  tb1.intGrnYear AND
					ware_returntosupplierdetails.intItemId =  tb1.intItemId),0) as dblRetunSupplierQty , 
				ware_returntosupplierdetails.dblQty ,
				ware_returntosupplierheader.intApproveLevels,  
				ware_returntosupplierheader.intStatus 
				FROM 
				ware_returntosupplierheader
				Inner Join ware_returntosupplierdetails ON ware_returntosupplierheader.intReturnNo = ware_returntosupplierdetails.intReturnNo AND ware_returntosupplierheader.intReturnYear = ware_returntosupplierdetails.intReturnYear
				Inner Join ware_grndetails as tb1 ON ware_returntosupplierheader.intGrnNo = tb1.intGrnNo AND ware_returntosupplierheader.intGrnYear = tb1.intGrnYear AND ware_returntosupplierdetails.intItemId = tb1.intItemId
				Inner Join mst_item ON tb1.intItemId = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId  
				Inner Join ware_grnheader ON tb1.intGrnNo = ware_grnheader.intGrnNo AND tb1.intGrnYear = ware_grnheader.intGrnYear
				WHERE 
				mst_item.intStatus ='1' AND 
				ware_grnheader.intStatus =  '1' AND
				ware_returntosupplierheader.intReturnNo =  '$returnNo' AND
				ware_returntosupplierheader.intReturnYear =  '$returnNoYear' AND
				ware_returntosupplierheader.intGrnNo =  '$grnNo' AND
				ware_returntosupplierheader.intGrnYear =  '$grnNoYear'
";
		$result = $db->RunQuery2($sql);
		$errorFlg=0;
		$msg ="Maximum Qtys for items"; 
		$totTosaveQty=0;
		$totSavableQty=0;
		while($row=mysqli_fetch_array($result))
		{	
			 $Qty=$row['dblQty'];
			 $stockBalQty=getStockBalance($row['intId'],$row['intCompanyId']);
				
				if($row['intStatus']==($row['intApproveLevels']+1)){//no confirmation has been raised
					if($row['dblGrnQty']-$row['dblRetunSupplierQty']<$stockBalQty)
					$balQtyToReturn = $row['dblGrnQty']-$row['dblRetunSupplierQty'];
					else
					$balQtyToReturn = $stockBalQty;
				}
				else{//confirmation has been raised
					if(($row['dblGrnQty']-($row['dblRetunSupplierQty']-$Qty))<$stockBalQty)
					$balQtyToReturn = $row['dblGrnQty']-$row['dblRetunSupplierQty']+$Qty;
					else
					$balQtyToReturn = $stockBalQty;
				}

				if($Qty>$balQtyToReturn){
					$errorFlg=1;
					$msg .="\n ".$grnNoArray[0]."/".$grnNoArray[1]."-".$row['itemName']." =".$balQtyToReturn;
				}
				
			$totTosaveQty +=$Qty;
			$totSavableQty +=$balQtyToReturn;
		}//end of while
		}//end of if($errorFlg == 0)
	
	if(($totTosaveQty > $totSavableQty) && ($errorFlg!=1)){
		$errorFlg=1;
		$msg ="Balance to Return to stores Qty is not tally with This Qtys.";
	}
	
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);
	}
//----------------------------------------------------
else if($requestType=='validateRejecton')
	{
		$grnNo  = $_REQUEST['grnNo'];
		$grnNoArray=explode("/",$grnNo);
		$grnNo=$grnNoArray[0];
		$grnNoYear=$grnNoArray[1];
		
		$returnNo = $_REQUEST['returnNo'];
		$returnNoArray=explode("/",$returnNo);
		$returnNo=$returnNoArray[0];
		$returnNoYear=$returnNoArray[1];
		
		$sql = "SELECT
		ware_returntosupplierheader.intStatus, 
		ware_returntosupplierheader.intApproveLevels 
		FROM ware_returntosupplierheader
		WHERE
		ware_returntosupplierheader.intReturnNo =  '$returnNo' AND
		ware_returntosupplierheader.intReturnYear =  '$returnNoYear' AND
		ware_returntosupplierheader.intGrnNo =  '$grnNo' AND
		ware_returntosupplierheader.intGrnYear =  '$grnNoYear' ";

		$result = $db->RunQuery2($sql);
		$errorFlg=0;
		$msg ="";
		$row=mysqli_fetch_array($result);
		
		$rejectionMode=loadRejectionMode($programCode,$row['intStatus'],$row['intApproveLevels'],$userId);

		if($row['intStatus']==1){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="Final confirmation of this Return Note is already raised.So cant reject"; 
		}
		else if($row['intStatus']==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="This Return Note is already Rejected"; 
		}
		else if($rejectionMode==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="No Permission to reject this Return Note"; 
		}
		else{//confirmation has been raised
			$errorFlg = 0;
			
		}
	
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);
	}

		$db->RunQuery2('Commit');
		$db->CloseConnection();		

//---------------------------
	function getStockBalance($item,$location)
	{
		global $db;
		 $sql = "SELECT
				Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$location' AND 
				ware_stocktransactions_bulk.strType <>  'ITEMCREATION+' 
				GROUP BY
				ware_stocktransactions_bulk.intItemId";

		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	
//------------------------------function loadRejectionMode-------------------
function loadRejectionMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$rejectMode=0;
	$sqlp = "SELECT
		menupermision.intReject 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery2($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intReject']==1){
	 if($intStatus!=0){
	 $rejectMode=1;
	 }
	}
	 
	return $rejectMode;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	 $sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery2($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
//--------------------------------------------------------

?>

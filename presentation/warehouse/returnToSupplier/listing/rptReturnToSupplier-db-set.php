<?php 

	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	$programName='Supplier Return Note';
	$programCode='P0226';

	include "{$backwardseperator}dataAccess/Connector.php";
	include "{$backwardseperator}libraries/mail/mail.php";
	require_once $backwardseperator."class/cls_mail.php";
    require_once $_SESSION['ROOT_PATH']."class/common/azureDBconnection.php";
	
	$objMail = new cls_create_mail($db);
    $objAzure = new cls_azureDBconnection();
	$supReturnApproveLevel = (int)getApproveLevel($programName);
	//$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////

	
	$retSupNo = $_REQUEST['retSupNo'];
	$year = $_REQUEST['year'];
	
	if($_REQUEST['status']=='approve')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		$rollBackFlag=0;
		$rollBackMsg = '';
		
		$sql = "UPDATE `ware_returntosupplierheader` SET `intStatus`=intStatus-1 WHERE (intReturnNo='$retSupNo') AND (`intReturnYear`='$year') and intStatus not in(0,1)";
		$result = $db->RunQuery2($sql);
		if((!$result) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sql;
			$rollBackMsg = "Approval error!";
		}
		
		if($result){
			$sql = "SELECT ware_returntosupplierheader.intUser, 
					ware_returntosupplierheader.intStatus,
					ware_returntosupplierheader.intApproveLevels, 
					ware_returntosupplierheader.intCompanyId AS location,
					mst_locations.intCompanyId AS companyId 
					FROM
					ware_returntosupplierheader
					Inner Join mst_locations ON ware_returntosupplierheader.intCompanyId = mst_locations.intId WHERE (intReturnNo='$retSupNo') AND (`intReturnYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$createdUser = $row['intUser'];
			$status = $row['intStatus'];
			$savedLevels = $row['intApproveLevels'];
			$location 	= $row['location'];
			$company = $row['companyId'];
			
			$approveLevel=$savedLevels+1-$status;
			$sqlI = "INSERT INTO `ware_returntosupplierheader_approvedby` (`intReturnNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
				VALUES ('$retSupNo','$year','$approveLevel','$userId',now(),0)";
			$resultI = $db->RunQuery2($sqlI);
			if((!$resultI) && ($rollBackFlag!=1)){
				$rollBackFlag=1;
				$sqlM=$sqlI;
				$rollBackMsg = "Approval error!";
			}
			
			if($status==1)
			{

				if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
				////////////// send mail to creater //////////////////
				sendFinlConfirmedEmailToUser($retSupNo,$year,$objMail,$mainPath,$root_path);
				}
			}
            if($status == 1 && $_SESSION['headCompanyId'] == '1'){
                sendAzureReturnToSupplierDetails($retSupNo,$year,$db,"PO_New");
			}

		//--------------------------------------------------------------------
		if($status==1){//if final confirmation raised,insert to stock
				$sql1 = "SELECT
 						ware_returntosupplierdetails.dblQty,
						ware_returntosupplierdetails.intItemId,   
						ware_returntosupplierheader.intGrnNo,
						ware_returntosupplierheader.intGrnYear,
						ware_grnheader.datdate,
						ware_grndetails.dblGrnRate  
						FROM
						ware_returntosupplierdetails
						Inner Join ware_returntosupplierheader ON ware_returntosupplierdetails.intReturnNo = ware_returntosupplierheader.intReturnNo AND ware_returntosupplierdetails.intReturnYear = ware_returntosupplierheader.intReturnYear
						Inner Join ware_grndetails ON ware_returntosupplierheader.intGrnNo = ware_grndetails.intGrnNo AND ware_returntosupplierheader.intGrnYear = ware_grndetails.intGrnYear AND ware_returntosupplierdetails.intItemId = ware_grndetails.intItemId
						Inner Join ware_grnheader ON ware_grndetails.intGrnNo = ware_grnheader.intGrnNo AND ware_grndetails.intGrnYear = ware_grnheader.intGrnYear
						WHERE
						ware_returntosupplierdetails.intReturnNo =  '$retSupNo' AND
						ware_returntosupplierdetails.intReturnYear =  '$year'";
				$result1 = $db->RunQuery2($sql1);
				$toSavedQty=0;
				$transSavedQty=0;
				while($row=mysqli_fetch_array($result1))
				{
					$toSavedQty+=round($row['dblQty'],4);
 					$grnNo=$row['intGrnNo'];
					$grnYear=$row['intGrnYear'];
/*					$grnDate=$row['datdate'];
					$grnRate=$row['dblGrnRate'];								
*/					$item=$row['intItemId'];
					$Qty=$row['dblQty'];
//					$currency=loadCurrency($grnNo,$grnYear,$item);
					$rowI=loadInterCompany($grnNo,$grnYear);
					$interCompany=$rowI['intInterCompany'];
					$interCompanyId=$rowI['intCompanyId'];
					$interLocation=$rowI['intInterLocation'];
					
					/////////////////////////////////////////there may be nox existing stock in relevent
					// grn No.but shd return from existing grns 
		$resultG = getGrnWiseStockBalance_bulk($location,$item);
		while($rowG=mysqli_fetch_array($resultG)){
				if(($Qty>0) && ($rowG['stockBal']>0)){
				if($rowG['stockBal']>=$Qty){
					$saveQty=$Qty;
					$Qty=0;
					}
					else if($Qty>$rowG['stockBal']){
					$saveQty=$rowG['stockBal'];
					$Qty=$Qty-$saveQty;
					}
					$grnNo=$rowG['intGRNNo'];
					$grnYear=$rowG['intGRNYear'];
					$grnDate=$rowG['dtGRNDate'];
					$grnRate=$rowG['dblGRNRate'];	
					$currency=loadCurrency($grnNo,$grnYear,$item);
					$saveQty=round($saveQty,4);
					if($saveQty>0){	
					
					$sqlI = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intUser,dtDate) 
						VALUES ('$company','$location','$retSupNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$item','-$saveQty','RTSUP','$userId',now())";
					$resultI = $db->RunQuery2($sqlI);
					if($resultI){
						$transSavedQty+=$saveQty;
					}
					if((!$resultI) && ($rollBackFlag!=1)){
						$rollBackFlag=1;
						$sqlM=$sqlI;
						$rollBackMsg = "Approval error!";
					}
					
					if(($interCompany!=0) && ($interCompanyId!=0) && ($interLocation!=0)){
						$sqlS = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intUser,dtDate) 
							VALUES ('$interCompanyId','$interLocation','$retSupNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$item','$saveQty','RTSUP_RCV','$userId',now())";
						$resultI = $db->RunQuery2($sqlS);
						if((!$resultI) && ($rollBackFlag!=1)){
							$rollBackFlag=1;
							$sqlM=$sqlS;
							$rollBackMsg = "Approval error!";
						}
					}
					
					}
				}
			}
		/////////////////////////////////////////
				}
			}
		//--------------------------------------------------------------------
			if($status==$savedLevels){//if first confirmation raised,grn qty field in po table shld update
				$sql1 = "SELECT
						ware_returntosupplierdetails.intItemId,
						ware_returntosupplierdetails.dblQty,
						ware_returntosupplierheader.intGrnNo,
						ware_returntosupplierheader.intGrnYear
						FROM 
						ware_returntosupplierdetails
						Inner Join ware_returntosupplierheader ON ware_returntosupplierdetails.intReturnNo = ware_returntosupplierheader.intReturnNo AND ware_returntosupplierdetails.intReturnYear = ware_returntosupplierheader.intReturnYear
						WHERE
						ware_returntosupplierdetails.intReturnNo =  '$retSupNo' AND
						ware_returntosupplierdetails.intReturnYear =  '$year'";
				$result1 = $db->RunQuery2($sql1);
				while($row=mysqli_fetch_array($result1))
				{
					$grnNo=$row['intGrnNo'];
					$grnYear=$row['intGrnYear'];
					$item=$row['intItemId'];
					$Qty=$row['dblQty'];
					
					$sql = "UPDATE `ware_grndetails` SET 	dblRetunSupplierQty = dblRetunSupplierQty+'$Qty' 
							WHERE
							ware_grndetails.intGrnNo =  '$grnNo' AND
							ware_grndetails.intGrnYear =  '$grnYear' AND
							ware_grndetails.intItemId =  '$item' ";
					$result = $db->RunQuery2($sql);
					if((!$result) && ($rollBackFlag!=1)){
						$rollBackFlag=1;
						$sqlM=$sql;
						$rollBackMsg = "Approval error!";
					}
				}
			}
		//---------------------------------------------------------------------------
		}
		
		$toSavedQty=round($toSavedQty,4);
		$transSavedQty=round($transSavedQty,4);
		
		if(($toSavedQty!=$transSavedQty) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$rollBackMsg = "Approval Error";
			$sqlM=$toSavedQty."-".$transSavedQty;
		}
		
		if(($rollBackFlag==0) &&($status==1)){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= "Final Approval raised sucessfully";
			$response['q'] 			= $sql;
		}
		else if($rollBackFlag==0){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= "Approval raised sucessfully";
			$response['q'] 			= '';
		}
		else if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= $sqlM;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sqlM;
		}
		$db->CloseConnection();		
		
		//----------------------------------------
			echo json_encode($response);
		//-----------------------------------------	
		
	}
	else if($_REQUEST['status']=='reject')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		$rollBackFlag=0;
		$rollBackMsg = '';
		
		//$grnApproveLevel = (int)getApproveLevel($programName);
			$sql = "SELECT ware_returntosupplierheader.intUser, 
					ware_returntosupplierheader.intStatus,
					ware_returntosupplierheader.intApproveLevels, 
					ware_returntosupplierheader.intCompanyId AS location,
					mst_locations.intCompanyId AS companyId 
					FROM
					ware_returntosupplierheader
					Inner Join mst_locations ON ware_returntosupplierheader.intCompanyId = mst_locations.intId WHERE (intReturnNo='$retSupNo') AND (`intReturnYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$createdUser = $row['intUser'];
			$status = $row['intStatus'];
			$savedLevels = $row['intApproveLevels'];
			$location 	= $row['location'];
			$company = $row['companyId'];
		
		$sql = "UPDATE `ware_returntosupplierheader` SET `intStatus`=0 WHERE (intReturnNo='$retSupNo') AND (`intReturnYear`='$year')";
		$result = $db->RunQuery2($sql);
		if((!$result) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sql;
			$rollBackMsg = "Rejection error!";
		}
		
		//$sql = "DELETE FROM `ware_returntosupplierheader_approvedby` WHERE (`intReturnNo`='$retSupNo') AND (`intYear`='$year')";
		//$result2 = $db->RunQuery($sql);
		
		$sqlI = "INSERT INTO `ware_returntosupplierheader_approvedby` (`intReturnNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
			VALUES ('$retSupNo','$year','0','$userId',now(),0)";
		$resultI = $db->RunQuery2($sqlI);
		if((!$resultI) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sqlI;
			$rollBackMsg = "Rejection error!";
		}
		if($resultI)
		{
			if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
			sendRejecedEmailToUser($retSupNo,$year,$objMail,$mainPath,$root_path);
			}
		}

		
		//---------------------------------------------------------------------------------
		if($status==1){//if final confirmation raised,insert to stock
				$sql1 = "SELECT
 						ware_returntosupplierdetails.dblQty,
						ware_returntosupplierdetails.intItemId,   
						ware_returntosupplierheader.intGrnNo,
						ware_returntosupplierheader.intGrnYear,
						ware_grnheader.datdate,
						ware_grndetails.dblGrnRate  
						FROM
						ware_returntosupplierdetails
						Inner Join ware_returntosupplierheader ON ware_returntosupplierdetails.intReturnNo = ware_returntosupplierheader.intReturnNo AND ware_returntosupplierdetails.intReturnYear = ware_returntosupplierheader.intReturnYear
						Inner Join ware_grndetails ON ware_returntosupplierheader.intGrnNo = ware_grndetails.intGrnNo AND ware_returntosupplierheader.intGrnYear = ware_grndetails.intGrnYear AND ware_returntosupplierdetails.intItemId = ware_grndetails.intItemId
						Inner Join ware_grnheader ON ware_grndetails.intGrnNo = ware_grnheader.intGrnNo AND ware_grndetails.intGrnYear = ware_grnheader.intGrnYear
						WHERE
						ware_returntosupplierdetails.intReturnNo =  '$retSupNo' AND
						ware_returntosupplierdetails.intReturnYear =  '$year'";
				$result1 = $db->RunQuery2($sql1);
				while($row=mysqli_fetch_array($result1))
				{
					$grnNo=$row['intGrnNo'];
					$grnYear=$row['intGrnYear'];
					$grnDate=$row['datdate'];
					$grnRate=$row['dblGrnRate'];								
					$item=$row['intItemId'];
					$Qty=$row['dblQty'];
					$currency=loadCurrency($grnNo,$grnYear,$item);
					$rowI=loadInterCompany($grnNo,$grnYear);
					$interCompany=$rowI['intInterCompany'];
					$interLocation=$rowI['intInterLocation'];
					
					$sqlI = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intUser,dtDate) 
						VALUES ('$company','$location','$retSupNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$item','$Qty','CRTSUP','$userId',now())";
					$resultI = $db->RunQuery2($sqlI);
					if((!$resultI) && ($rollBackFlag!=1)){
						$rollBackFlag=1;
						$sqlM=$sqlI;
						$rollBackMsg = "Rejection error!";
					}
					
					if(($interCompany!=0) && ($interLocation!=0)){
						$sqlS = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intUser,dtDate) 
							VALUES ('$interCompany','$interLocation','$retSupNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$item','-$Qty','CRTSUP_RCV','$userId',now())";
						$resultI = $db->RunQuery2($sqlS);
						if((!$resultI) && ($rollBackFlag!=1)){
							$rollBackFlag=1;
							$sqlM=$sqlS;
							$rollBackMsg = "Rejection error!";
						}
				}
			}
		}
		//---------------------------------------------------------------------------------
			if($savedLevels>=$status){//if atleast one confirmation processed, roll back return to saupplier qty from grn table-
				$sql1 = "SELECT
						ware_returntosupplierdetails.intItemId,
						ware_returntosupplierdetails.dblQty,
						ware_returntosupplierheader.intGrnNo,
						ware_returntosupplierheader.intGrnYear
						FROM
						ware_returntosupplierdetails
						Inner Join ware_returntosupplierheader ON ware_returntosupplierdetails.intReturnNo = ware_returntosupplierheader.intReturnNo AND ware_returntosupplierdetails.intReturnYear = ware_returntosupplierheader.intReturnYear
						WHERE
						ware_returntosupplierdetails.intReturnNo =  '$retSupNo' AND
						ware_returntosupplierdetails.intReturnYear =  '$year'";
				$result1 = $db->RunQuery2($sql1);
				while($row=mysqli_fetch_array($result1))
				{
					$grnNo=$row['intGrnNo'];
					$grnYear=$row['intGrnYear'];
					$item=$row['intItemId'];
					$Qty=$row['dblQty'];
					
					$sql = "UPDATE `ware_grndetails` SET 	dblRetunSupplierQty = dblRetunSupplierQty-'$Qty' 
							WHERE
							ware_grndetails.intGrnNo =  '$grnNo' AND
							ware_grndetails.intGrnYear =  '$grnYear' AND
							ware_grndetails.intItemId =  '$item' ";
					$result = $db->RunQuery2($sql);
					if((!$result) && ($rollBackFlag!=1)){
						$rollBackFlag=1;
						$sqlM=$sql;
						$rollBackMsg = "Rejection error!";
					}
				}
			}
 		
		if($rollBackFlag==0){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= "Rejection raised sucessfully";
			$response['q'] 			= '';
		}
		else if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= $sqlM;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sqlM;
		}
		$db->CloseConnection();		
		
		//----------------------------------------
			echo json_encode($response);
		//-----------------------------------------	
		
	}
	
	
//--------------------------------------------------------------
function loadCurrency($grnNo,$grnYear,$item)
{
	global $db;
	
	if(($grnNo!=0) && ($grnYear!=0)){
	  $sql = "SELECT
			trn_poheader.intCurrency as currency 
			FROM
			ware_grnheader
			Inner Join trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
			WHERE
			ware_grnheader.intGrnNo =  '$grnNo' AND
			ware_grnheader.intGrnYear =  '$grnYear'";
	}
	else{
	$sql = "SELECT
			mst_item.intCurrency as currency 
			FROM mst_item
			WHERE
			mst_item.intId =  '$item'";
	}

	$result = $db->RunQuery2($sql);
	$row=mysqli_fetch_array($result);	
	return $row['currency'];;	
}
//--------------------------------------------------------------
function loadInterCompany($grnNo,$grnYear)
{
	global $db;
	 $sql = "SELECT 
			mst_supplier.intInterCompany, 
			mst_supplier.intCompanyId, 
			mst_supplier.intInterLocation 
			FROM 
			ware_returntosupplierheader
			Inner Join ware_grnheader ON ware_returntosupplierheader.intGrnNo = ware_grnheader.intGrnNo AND ware_returntosupplierheader.intGrnYear = ware_grnheader.intGrnYear
			Inner Join trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
			Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
			WHERE
			ware_returntosupplierheader.intGrnNo =  '$grnNo' AND
			ware_returntosupplierheader.intGrnYear =  '$grnYear'";

	$result = $db->RunQuery2($sql);
	$row=mysqli_fetch_array($result);	
	return $row;	
}
//--------------------------------------------------------------
function getGrnWiseStockBalance_bulk($location,$item)
{
	global $db;
	 $sql = "SELECT
			Sum(ware_stocktransactions_bulk.dblQty) AS stockBal,
			ware_stocktransactions_bulk.intGRNNo,
			ware_stocktransactions_bulk.intGRNYear,
			ware_stocktransactions_bulk.dtGRNDate,
			ware_stocktransactions_bulk.dblGRNRate 
			FROM ware_stocktransactions_bulk 
			WHERE
			ware_stocktransactions_bulk.intItemId =  '$item' AND
			ware_stocktransactions_bulk.intLocationId =  '$location'
			GROUP BY
			ware_stocktransactions_bulk.intItemId,
			ware_stocktransactions_bulk.intGRNNo,
			ware_stocktransactions_bulk.intGRNYear,
			ware_stocktransactions_bulk.dtGRNDate, 
			ware_stocktransactions_bulk.dblGRNRate 
			ORDER BY
			ware_stocktransactions_bulk.dtGRNDate ASC";

	$result = $db->RunQuery2($sql);
	return $result;	
}
	
//--------------------------------------------------------
function sendFinlConfirmedEmailToUser($serialNo,$year,$objMail,$mainPat,$root_path){
	global $db;
				
		    $sql = "SELECT
			sys_users.strFullName,
			sys_users.strEmail 
			FROM 
			ware_returntosupplierheader
			Inner Join sys_users ON ware_returntosupplierheader.intUser = sys_users.intUserId
			WHERE
			ware_returntosupplierheader.intReturnNo =  '$serialNo' AND
			ware_returntosupplierheader.intReturnYear =  '$year'";

				
		 $result = $db->RunQuery2($sql);
		 
		$row=mysqli_fetch_array($result);
			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="CONFIRMED RETURN TO SUPPLIER NOTE ('$serialNo'/'$year')"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST = NULL;
			
			$_REQUEST['program']='RETURN TO SUPPLIER NOTE';
			$_REQUEST['field1']='Return No';
			$_REQUEST['field2']='Return Year';
			$_REQUEST['field3']='';
			$_REQUEST['field4']='';
			$_REQUEST['field5']='';
			$_REQUEST['value1']=$serialNo;
			$_REQUEST['value2']=$year;
			$_REQUEST['value3']='';
			$_REQUEST['value4']='';
			$_REQUEST['value5']='';
			
			$_REQUEST['subject']="CONFIRMED RETURN TO SUPPLIER NOTE ('$serialNo'/'$year')";
			
			$_REQUEST['statement1']="Approved this";
			$_REQUEST['statement2']="to view this";
			$_REQUEST['link']=base64_encode($mainPath."presentation/warehouse/returnToSupplier/listing/rptReturnToSupplier.php?retSupNo=$serialNo&year=$year&approveMode=0"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}
//--------------------------------------------------------
function sendRejecedEmailToUser($serialNo,$year,$objMail,$mainPat,$root_path){
	global $db;
				
		    $sql = "SELECT
			sys_users.strFullName,
			sys_users.strEmail 
			FROM 
			ware_returntosupplierheader
			Inner Join sys_users ON ware_returntosupplierheader.intUser = sys_users.intUserId
			WHERE
			ware_returntosupplierheader.intReturnNo =  '$serialNo' AND
			ware_returntosupplierheader.intReturnYear =  '$year'";

				
		 $result = $db->RunQuery2($sql);
		 
		$row=mysqli_fetch_array($result);
			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="REJECTED RETURN TO SUPPLIER NOTE ('$serialNo'/'$year')"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST = NULL;
			
			$_REQUEST['program']='RETURN TO SUPPLIER NOTE';
			$_REQUEST['field1']='Return No';
			$_REQUEST['field2']='Return Year';
			$_REQUEST['field3']='';
			$_REQUEST['field4']='';
			$_REQUEST['field5']='';
			$_REQUEST['value1']=$serialNo;
			$_REQUEST['value2']=$year;
			$_REQUEST['value3']='';
			$_REQUEST['value4']='';
			$_REQUEST['value5']='';
			
			$_REQUEST['subject']="REJECTED RETURN TO SUPPLIER NOTE ('$serialNo'/'$year')";
			
			$_REQUEST['statement1']="Rejected this";
			$_REQUEST['statement2']="to view this";
			$_REQUEST['link']=base64_encode($mainPath."presentation/warehouse/returnToSupplier/listing/rptReturnToSupplier.php?retSupNo=$serialNo&year=$year&approveMode=0"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}

function sendAzureReturnToSupplierDetails($retSupNo, $retSupYear, $db, $transactionType)
{

    global $objAzure;
    $NBT_taxcodes = array(2,3,22);
    $environment = $objAzure->getEnvironment();
    $headerTable = 'PurchaseHeader'.$environment;
    $lineTable = 'PurchaseLine'.$environment;

    $azure_connection = $objAzure->connectAzureDB();
     $sql_header = "SELECT
                        ware_returntosupplierheader.intCompanyId AS locationCode,
                        mst_supplier.strName as supplier,
                        mst_supplier.strCode AS supplier_code,
                        ware_grnheader.strInvoiceNo,
                        CONCAT(ware_returntosupplierheader.intGrnNo,'/',ware_returntosupplierheader.intGrnYear) AS receipt_no,
                        CONCAT(trn_poheader.intPONo,'/',trn_poheader.intPOYear) AS po_string,
                        mst_financecurrency.strCode AS currency_code,
                        trn_poheader.intPaymentTerm AS payTerm,
                        trn_poheader.intPaymentMode AS payMode,
                        trn_poheader.intShipmentTerm AS shipmentTerm,
                        trn_poheader.intShipmentMode AS shipmentMode,
                        trn_poheader.dtmDeliveryDate AS deliveryDate,
                        trn_poheader.strRemarks AS narration,
                        trn_poheader.dtmPODate AS orderDate,
                        trn_poheader.intReviseNo
                        FROM
                        ware_returntosupplierheader
                        Inner Join mst_locations ON ware_returntosupplierheader.intCompanyId = mst_locations.intId
                        Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
                        Inner Join sys_users ON ware_returntosupplierheader.intUser = sys_users.intUserId
                        Inner Join ware_grnheader ON ware_returntosupplierheader.intGrnNo = ware_grnheader.intGrnNo AND ware_returntosupplierheader.intGrnYear = ware_grnheader.intGrnYear
                        Inner Join trn_podetails ON ware_grnheader.intPoNo = trn_podetails.intPONo AND ware_grnheader.intPoYear = trn_podetails.intPOYear
                        Inner Join trn_poheader ON trn_podetails.intPONo = trn_poheader.intPONo AND trn_podetails.intPOYear = trn_poheader.intPOYear
                        Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
                        INNER JOIN mst_financecurrency ON mst_financecurrency.intId = trn_poheader.intCurrency
                        WHERE
                        ware_returntosupplierheader.intReturnNo =  '$retSupNo' AND
                        ware_returntosupplierheader.intReturnYear =  '$retSupYear' 
                        GROUP BY
                        ware_returntosupplierheader.intReturnNo,
                        ware_returntosupplierheader.intReturnYear";

       $result_header = $db->RunQuery2($sql_header);
        while ($row_header = mysqli_fetch_array($result_header)) {
            $old_poString = $row_header['po_string'];
            $receiptNo = $row_header['receipt_no'];
            $supplier = $row_header['supplier'];
            $supplier_code = $row_header['supplier_code'];
            $currency = ($row_header['currency_code'] == 'EURO')?"Eur":($row_header['currency_code'] == 'LKR'?"":$row_header['currency_code']);
            $payTerm = $row_header['payTerm'];
            $payMode = $row_header['payMode'];
            $shipmentMode = $row_header['shipmentMode'];
            $locationId = $row_header['locationCode'];
            $orderDate = $row_header['orderDate'];
            $reviseNo = $row_header['intReviseNo'];
            $narration = trim($row_header['narration']);
            $narration_sql = $db->escapeString($narration);
            $narration = str_replace("'", '', $narration);
            $deliveryDate = $row_header['deliveryDate'];
            $po_type = $row_header['shipmentTerm'];

            $response = getLatestPODetails($old_poString,$db);
            $poString = $response['new_poString'];
            $version_no = $response['version_no'];
            $postingDate = date('Y-m-d H:i:s');


        $sql_detail = "SELECT
                    ware_returntosupplierdetails.intReturnNo,
                    ware_returntosupplierdetails.intReturnYear,
                    ware_returntosupplierdetails.intItemId,
                    CONCAT(ware_grnheader.intGrnNo,'/',ware_grnheader.intGrnYear) as receipt_no,
                    Sum(
                        ware_returntosupplierdetails.dblQty
                    ) AS dblQty,
                    mst_item.strCode AS itemCode,
                    mst_item.strName AS itemName,
                    mst_item.intUOM,
                    mst_maincategory.strName AS mainCategory,
                    mst_subcategory.strName AS subCategory,
                    mst_units.strCode AS uom,
                    trn_podetails.dblUnitPrice AS unitPrice,
                    trn_podetails.dblDiscount,
                    trn_podetails.intTaxCode                    
                FROM
                    ware_returntosupplierdetails
                INNER JOIN mst_item ON ware_returntosupplierdetails.intItemId = mst_item.intId
                INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
                INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
                INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
                INNER JOIN ware_returntosupplierheader ON ware_returntosupplierdetails.intReturnNo = ware_returntosupplierheader.intReturnNo AND ware_returntosupplierdetails.intReturnYear = ware_returntosupplierheader.intReturnYear
                INNER JOIN  ware_grnheader ON ware_returntosupplierheader.intGrnNo = ware_grnheader.intGrnNo AND ware_returntosupplierheader.intGrnYear = ware_grnheader.intGrnYear
                Inner Join ware_grndetails ON ware_grndetails.intGrnNo = ware_grnheader.intGrnNo AND ware_grndetails.intGrnYear = ware_grnheader.intGrnYear AND ware_grndetails.intItemId = ware_returntosupplierdetails.intItemId
                Inner Join trn_podetails ON ware_grnheader.intPoNo = trn_podetails.intPONo AND ware_grnheader.intPoYear = trn_podetails.intPOYear AND ware_grndetails.intItemId = trn_podetails.intItem
                WHERE
                    ware_returntosupplierdetails.intReturnNo = '$retSupNo'
                AND ware_returntosupplierdetails.intReturnYear = '$retSupYear'
                GROUP BY
                    ware_returntosupplierdetails.intItemId
                ORDER BY
                    mst_maincategory.strName ASC,
                    mst_subcategory.strName ASC,
                    mst_item.strName ASC";

                $result_data = $db->RunQuery2($sql_detail);
                $i = 0;
                while ($row_data = mysqli_fetch_array($result_data)) {
                    $line_no = $row_data['intItemId'];
                    $item_code = $row_data['itemCode'];
                    $description = $row_data['itemName'];
                    $description = str_replace("'","''",$description);
                    $item_category = $row_data['mainCategory'];
                    $item_subcategory = $row_data['subCategory'];
                    $qty = $row_data['dblQty'];
                    $qty = round($qty,5);
                    $discount = $row_data['dblDiscount'];
                    $unit_price = round($row_data['unitPrice'] * (100-$discount)/100 ,5);
                    $successDetails = '0';
                    $i++;
                    $tax_code = $row_data['intTaxCode'];
                    if(in_array($tax_code,$NBT_taxcodes) ||  ($item_code == 'SERFNBT' && $tax_code != 0)){
                        $tax_amount = calculateNewTaxAmount($tax_code,$qty,$unit_price,$discount,$db);
                        $tax_amount = round($tax_amount,2);
                    }
                    else{
                        $tax_amount = 0;
                    }
                    $vat_group = ($tax_code != 0)?"VAT":"NO_VAT";

                    $sql_azure_details = "INSERT into $lineTable (Transaction_type, Document_Type, Purchase_Order_No, Revised_Count, Line_No, Reciept_No, Item_Code, Narration, Item_Category, Item_SubCategory, Quantity, Unit_Cost, VAT_Prod_Posting_Group, NBT_Amount) VALUES ('$transactionType', 'CreditMemo', '$poString','$reviseNo','$line_no', '$receiptNo', '$item_code','$description','$item_category','$item_subcategory','$qty','$unit_price','$vat_group','$tax_amount')";
                    if ($azure_connection) {
                        $getResults = $objAzure->runQuery($azure_connection, $sql_azure_details);
                        if ($getResults != FALSE) {
                            $successDetails = '1';
                        }
                    }
                    $sql_db_details = "INSERT into trn_financemodule_purchaseline (Transaction_type, Purchase_Order_No, Revised_Count, Line_No, Receipt_No,  Item_Code, Narration, Item_Category, Item_SubCategory, Quantity, Unit_Cost, VAT_Prod_Posting_Group, NBT_Amount, deliveryStatus, deliveryDate) VALUES ('$transactionType','$poString','$reviseNo','$line_no', '$receiptNo', '$item_code','$description','$item_category','$item_subcategory','$qty','$unit_price','$vat_group', '$tax_amount','$successDetails', NOW())";
                    $result_db_details = $db->RunQuery2($sql_db_details);
                }
        $sql_azure_header = "INSERT INTO $headerTable (Transaction_type, Document_Type, Receipt_No, Purchase_Order_No, Revised_Count, Vendor_Code, Vendor_Name, Order_Date, Posting_Date, Revised_Date, Currency_Code, Payment_Term, Payment_Method, Shipment_Mode, Delivery_Date, Location_Code, Narration, PO_Type, No_of_Lines) VALUES ('$transactionType', 'CreditMemo', '$receiptNo', '$poString', '$reviseNo', '$supplier_code', '$supplier','$orderDate', '$postingDate', '', '$currency', '$payTerm', '$payMode', '$shipmentMode','$deliveryDate','$locationId',CONVERT(varchar(250),'$narration'), '$po_type', '$i')";
       if($azure_connection) {
           $getResultsHeader = $objAzure->runQuery($azure_connection, $sql_azure_header);
           if($getResultsHeader != FALSE){
                $successHeader = '1';
           }
      }
        $sql_db_header = "INSERT INTO trn_financemodule_purchaseheader (Transaction_type, Document_Type, Receipt_No, Purchase_Order_No, Revised_Count, Vendor_Code, Vendor_Name, Order_Date, Posting_Date, Revised_Date, Currency_Code, Payment_Term, Payment_Method, Shipment_Mode, Delivery_Date, Location_Code, PO_Type, No_of_Lines, version_no, deliveryStatus, deliveryDate, Narration) VALUES ('$transactionType', 'CreditMemo','$receiptNo', '$poString', '$reviseNo','$supplier_code', '$supplier', '$orderDate', '$postingDate', '', '$currency', '$payTerm', '$payMode', '$shipmentMode','$deliveryDate','$locationId', '$po_type', '$i', '$version_no','$successHeader', NOW(),'$narration_sql')";
        $result_db_header = $db->RunQuery2($sql_db_header);
        createNewPurchaseOrder($old_poString,$db, $azure_connection);

    }
}

function createNewPurchaseOrder ($poString,$db,$azure_connection){
	    global $objAzure;
        $NBT_taxcodes = array(2,3,22);
        $environment = $objAzure->getEnvironment();
        $headerTable = 'PurchaseHeader'.$environment;
        $lineTable = 'PurchaseLine'.$environment;

        $sql = "SELECT COALESCE(MAX(version_no),0) AS version_no from trn_financemodule_purchaseheader WHERE Purchase_Order_No LIKE '%$poString%'";
	    $result = $db->RunQuery2($sql);
	    $row = mysqli_fetch_array($result);
	    $version_no = $row['version_no']+1;
        $new_po_string = $poString.'-D'.$version_no;
	    $poArray = explode('/',$poString);
	    $poNo = $poArray[0];
	    $poYear = $poArray[1];

	    // qty needed to be re-calculated

    $sql_select = "SELECT
                    trn_poheader.intSupplier AS supplierId,
                    mst_supplier.strName AS supplier,
                    mst_supplier.strCode AS supplier_code,
                    mst_financecurrency.strCode AS currency_code,
                    trn_poheader.intPaymentTerm AS payTerm,
                    trn_poheader.intPaymentMode AS payMode,
                    trn_poheader.intShipmentTerm AS shipmentTerm,
                    trn_poheader.intShipmentMode AS shipmentMode,
                    trn_poheader.dtmDeliveryDate AS deliveryDate,
                    trn_poheader.strRemarks AS narration,
                    mst_companies.strName AS delToCompany,
                    mst_locations.intId AS delToLocation,
                    trn_poheader.dtmPODate AS orderDate,
                    trn_poheader.intStatus,
                    trn_poheader.PRINT_COUNT,
                    trn_poheader.intApproveLevels,
                    trn_poheader.intUser,
                    trn_poheader.intCompany AS poRaisedLocationId,
                    sys_users.strUserName,
                    trn_poheader.intReviseNo,
                    trn_poheader.PO_TYPE	
                FROM
                    trn_poheader
                INNER JOIN mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
                INNER JOIN mst_locations ON trn_poheader.intDeliveryTo = mst_locations.intId
                INNER JOIN mst_companies ON mst_locations.intCompanyId = mst_companies.intId
                INNER JOIN sys_users ON trn_poheader.intUser = sys_users.intUserId
                INNER JOIN mst_financecurrency ON mst_financecurrency.intId = trn_poheader.intCurrency
                WHERE
                    trn_poheader.intPONo = '$poNo'
                AND trn_poheader.intPOYear = '$poYear'";

    $result = $db->RunQuery2($sql_select);
    while ($row = mysqli_fetch_array($result)) {
        $supplierId = $row['supplierId'];
        $supplier = $row['supplier'];
        $supplier_code = $row['supplier_code'];
        $currency = ($row['currency_code'] == 'EURO')?"Eur":($row['currency_code'] == 'LKR'?"":$row['currency_code']);
        $payTerm = $row['payTerm'];
        $payMode = $row['payMode'];
        $shipmentMode = $row['shipmentMode'];
        $deliveryDate = $row['deliveryDate'];
        $locationId = $row['poRaisedLocationId'];
        $orderDate = $row['orderDate'];
        $reviseNo = $row['intReviseNo'];
        $po_type = $row['shipmentTerm'];
        $narration = trim($row['narration']);
        $narration_sql = $db->escapeString($narration);
        $narration = str_replace("'", '', $narration);
        $successHeader = '0';
        $postingDate = date('Y-m-d H:i:s');

        $sql = "SELECT
                trn_podetails.intPONo,
                trn_podetails.intPOYear,
                trn_podetails.intItem,
                (
                    SELECT
                        GROUP_CONCAT(ware_grnheader.strInvoiceNo)
                    FROM
                        ware_grnheader
                    WHERE
                        ware_grnheader.intPoNo = TPH.intPONo
                    AND ware_grnheader.intPoYear = TPH.intPOYear
                    AND ware_grnheader.intStatus = 1
                ) AS invoices,
                COALESCE (
                    SUM(trn_podetails.dblGRNQty),
                    0
                ) AS tot_grn_qty,
                mst_item.strCode AS itemCode,
                mst_item.strName AS itemName,
                mst_maincategory.strName AS mainCategory,
                mst_subcategory.strName AS subCategory,
                round(
                    trn_podetails.dblUnitPrice,
                    6
                ) AS dblUnitPrice,
                trn_podetails.dblDiscount,
                trn_podetails.intTaxCode,
                sum(
                    trn_podetails.dblTaxAmmount
                ) AS dblTaxAmmount,
                mst_financetaxgroup.strCode,
                trn_podetails.SVAT_ITEM,
                sum(trn_podetails.dblQty) AS dblQty,
                mst_item.intUOM,
                mst_units.strName AS uom,
                trn_podetails.CostCenter
            FROM
                trn_podetails
            INNER JOIN mst_item ON trn_podetails.intItem = mst_item.intId
            INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
            INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
            LEFT JOIN mst_financetaxgroup ON trn_podetails.intTaxCode = mst_financetaxgroup.intId
            INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
            INNER JOIN trn_poheader TPH ON TPH.intPONo = trn_podetails.intPONo
            AND TPH.intPOYear = trn_podetails.intPOYear
            WHERE
                trn_podetails.intPONo = '$poNo'
            AND trn_podetails.intPOYear = '$poYear'
            GROUP BY
                trn_podetails.intItem
            HAVING
                dblQty > 0";

        $result_data = $db->RunQuery2($sql);
        $i = 0;
        while ($row_data = mysqli_fetch_array($result_data)){
            $invoiced_qty = 0;
            $line_no = $row_data['intItem'];
            $item_code = $row_data['itemCode'];
            $description = $row_data['itemName'];
            $description = str_replace("'","''",$description);
            $item_category = $row_data['mainCategory'];
            $item_subcategory = $row_data['subCategory'];
            $tax_code = $row_data['intTaxCode'];
            $po_qty = $row_data['dblQty'];
            $discount = $row_data['dblDiscount'];
            $tot_grn_qty = $row_data['tot_grn_qty'];
            if($tot_grn_qty>0){
                $invoice_no = $row_data['invoices'];
                $invoice_array = explode(',',$invoice_no);
                $invoiced_qty = getInvoicedQty($supplierId,$invoice_array,$line_no);
            }

            $qty = round($po_qty - $invoiced_qty,4);
            if($qty < 0){
                $qty = 0;
            }
            $unit_price = round($row_data['dblUnitPrice'] * (100-$discount)/100 ,5);
            $successDetails = '0';
            $i++;
            if(in_array($tax_code,$NBT_taxcodes) ||  ($item_code == 'SERFNBT' && $tax_code != 0)){
                $tax_amount = calculateNewTaxAmount($tax_code,$qty,$unit_price,$discount,$db);
                $tax_amount = round($tax_amount,2);
            }
            else{
                $tax_amount = 0;
            }
            $vat_group = ($tax_code != 0)?"VAT":"NO_VAT";

            // should calculate the tax amount;

            $sql_azure_details = "INSERT into $lineTable (Transaction_type, Document_Type, Purchase_Order_No, Revised_Count, Line_No, Item_Code, Narration, Item_Category, Item_SubCategory, Quantity, Unit_Cost, VAT_Prod_Posting_Group, NBT_Amount) VALUES ('PO_New','Order','$new_po_string', '$reviseNo', '$line_no','$item_code','$description','$item_category','$item_subcategory','$qty','$unit_price','$vat_group', '$tax_amount')";
            if($azure_connection) {
                $getResults = $objAzure->runQuery($azure_connection, $sql_azure_details);
                if($getResults != FALSE){
                    $successDetails = '1';
                }
            }
                $sql_db_details = "INSERT into trn_financemodule_purchaseline (Transaction_type, Purchase_Order_No, Revised_Count, Line_No, Item_Code, Narration, Item_Category, Item_SubCategory, Quantity, Unit_Cost, VAT_Prod_Posting_Group, NBT_Amount, deliveryStatus, deliveryDate) VALUES ('PO_New','$new_po_string', '$reviseNo', '$line_no','$item_code','$description','$item_category','$item_subcategory','$qty','$unit_price','$vat_group', '$tax_amount', '$successDetails', NOW())";
            $result_db_details = $db->RunQuery2($sql_db_details);
        }
        $sql_azure_header = "INSERT INTO $headerTable (Transaction_type, Document_Type, Purchase_Order_No, Revised_Count, Vendor_Code, Vendor_Name, Order_Date, Posting_Date, Revised_Date, Currency_Code, Payment_Term, Payment_Method, Shipment_Mode, Delivery_Date, Location_Code, Narration, PO_Type, No_of_Lines) VALUES ('PO_New', 'Order', '$new_po_string', '$reviseNo', '$supplier_code', '$supplier','$orderDate', '$postingDate', '', '$currency', '$payTerm', '$payMode', '$shipmentMode','$deliveryDate','$locationId',CONVERT(varchar(250),'$narration'), '$po_type', '$i')";
        if($azure_connection) {
            $getResultsHeader = $objAzure->runQuery($azure_connection, $sql_azure_header);
            if($getResultsHeader != FALSE){
                $successHeader = '1';
            }
        }
        $sql_db_header = "INSERT INTO trn_financemodule_purchaseheader (Transaction_type, Document_Type, Purchase_Order_No, Revised_Count, Vendor_Code, Vendor_Name, Order_Date, Posting_Date, Revised_Date, Currency_Code, Payment_Term, Payment_Method, Shipment_Mode, Delivery_Date, Location_Code, PO_Type, No_of_Lines, version_no, deliveryStatus, deliveryDate, Narration) VALUES ('PO_New', 'Order', '$new_po_string', '$reviseNo','$supplier_code', '$supplier', '$orderDate', '$postingDate', '', '$currency', '$payTerm', '$payMode', '$shipmentMode','$deliveryDate','$locationId', '$po_type', '$i', '$version_no', '$successHeader', NOW(),'$narration_sql')";
        $result_db_header = $db->RunQuery2($sql_db_header);
    }
}

function getLatestPODetails($poString,$db){
    $version_no = 0;
    $new_poString = $poString;
    $sql = "SELECT MAX(version_no) AS version_no, Purchase_Order_No FROM `trn_financemodule_purchaseheader` WHERE `Purchase_Order_No` LIKE '%$poString%' GROUP BY Purchase_Order_No ORDER BY version_no desc limit 1";
    $result = $db->RunQuery2($sql);
    while($row = mysqli_fetch_array($result)){
        $version_no = $row['version_no'];
        $new_poString = $row['Purchase_Order_No'];
    }
    $response = array();
    $response['version_no'] = $version_no;
    $response['new_poString'] = $new_poString;
    return $response;
}

function getInvoicedQty($supplier,$invoice_nos,$item){
	    global $db;
       $sql_select = "SELECT
                      IFNULL(SUM(SPID.QTY),0) AS invoiced_qty
                    FROM
                        finance_supplier_purchaseinvoice_details SPID
                    INNER JOIN finance_supplier_purchaseinvoice_header SPIH ON SPID.PURCHASE_INVOICE_NO = SPIH.PURCHASE_INVOICE_NO
                    AND SPIH.PURCHASE_INVOICE_YEAR = SPID.PURCHASE_INVOICE_YEAR
                    WHERE
                    SPIH.INVOICE_NO IN ('".implode("','", $invoice_nos)."')
                    AND SPIH.SUPPLIER_ID = '$supplier'
                    AND SPID.ITEM_ID = '$item'
                    AND SPIH.`STATUS` = 1";

    $result1 = $db->RunQuery2($sql_select);
    $row = mysqli_fetch_array($result1);
    return $row['invoiced_qty'];
}

function calculateNewTaxAmount($taxId, $quantity, $unitPrice, $discount, $db){
    $amount=$quantity*$unitPrice;
    $amount = $amount * (100 - $discount) / 100;
    $sql = "SELECT
				mst_financetaxgroup.intId,
				mst_financetaxgroup.strProcess
				FROM
				mst_financetaxgroup
				WHERE
				mst_financetaxgroup.intId =  '$taxId'";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $taxProcess = $row['strProcess'];
    $arrTax = explode("/", $taxProcess);
    $operation = '';

    //(3)capture calculation method(isolated/inclusive/exclusive) and the array 'jsonTaxCode' contains only tax ids
    $jsonTaxCode = "[ ";
    if (count($arrTax) == 1)//If strProcess contain only one value (Example $row['strProcess'] = 5)
    {
        $operation = 'Isolated';
        $jsonTaxCode .= '{ "taxId":"' . $taxProcess . '"}';
    } else if (count($arrTax) > 1) //IF there are multiple tax ids in the tax group(example strProcess = {4/Inclusive/7})
    {
        $operation = $arrTax[1];//this should be inclusive/exclusive
        for ($i = 0; $i < count($arrTax); $i = $i + 2) {
            $jsonTaxCode .= '{ "taxId":"' . $arrTax[$i] . '"},'; //create a json array geting 0 and 2 value from the array
        }

        $jsonTaxCode = $jsonTaxCode . substr(0, count($jsonTaxCode) - 1);
    }
    $jsonTaxCode .= " ]";
    $taxCodes = json_decode($jsonTaxCode, true);

    //(4)get tax rates for all tax ids in the tax group ( tax array)
    if (count($taxCodes) != 0) {
        foreach ($taxCodes as $taxCode) {
            //get tax rates from the mst_financetaxisolated table sending taxId to the callTaxValue function which got by json array
            $codeValues[] = callTaxValue($taxCode['taxId'],$db);
        }
    }
    if (count($codeValues) > 1) // if there are more than one tax types in the tax group (this can be identified from mst_financetaxgroup.strProcess field)
    {
        if ($operation == 'Inclusive') {
            //step 1: po amount will be multiplied by the first tax rate
            //step 2 : result of the step 1(po amount+tax ammount), will be multiplied by the second tax rate.
            $firstVal = ($amount * $codeValues[0]) / 100;
            $withTaxVal = $firstVal + ((($amount + $firstVal) * $codeValues[1]) / 100);
            $val1 = ($amount * $codeValues[0]) / 100;
            $val2 = ((($amount + $firstVal) * $codeValues[1]) / 100);
        } else if ($operation == 'Exclusive') {
            //get the summation of the two tax rates and multiply it from the amount
            $withTaxVal = ($amount * ($codeValues[0] + $codeValues[1])) / 100;
            $val1 = ($amount * $codeValues[0]) / 100;
            $val2 = ($amount * $codeValues[1]) / 100;
        }
    }
    else if(count($codeValues) == 1 && $operation == 'Isolated')//there is only one tax type for the tax group
    {
        $withTaxVal = ($amount*$codeValues[0])/100;
        $val1 = ($amount*$codeValues[0])/100;
    }

    return $withTaxVal;
}

function callTaxValue($taxId, $db)
{
    $sql = "SELECT
				mst_financetaxisolated.intId,
				mst_financetaxisolated.strCode,
				mst_financetaxisolated.dblRate
				FROM
				mst_financetaxisolated
				WHERE
				mst_financetaxisolated.intId = '$taxId'
				";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $taxVal = $row['dblRate'];
    return $taxVal;
}



?>
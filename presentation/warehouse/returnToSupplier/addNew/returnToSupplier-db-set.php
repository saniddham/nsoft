<?php 
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$companyId 	= $_SESSION['CompanyID'];
	include "{$backwardseperator}dataAccess/Connector.php";
	require_once $backwardseperator."class/warehouse/cls_warehouse_get.php";

	$objwhouseget= new cls_warehouse_get($db);
	
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	
	$serialNo 	 = $_REQUEST['serialNo'];
	$year 		 = $_REQUEST['Year'];
	$invoiceNo 	 = $_REQUEST['invoiceNo'];
	$grnNo 		 = $_REQUEST['grnNo'];
	$grnNoArray=explode("/",$grnNo);
	$grnNo=$grnNoArray[0];
	$grnNoYear=$grnNoArray[1];
	$date 		 = $_REQUEST['date'];
	$remarks 	 = $_REQUEST['remarks'];
	
	$arr 		= json_decode($_REQUEST['arr'], true);
	
	$programName='Supplier Return Note';
	$programCode='P0226';
	
	$ApproveLevels = (int)getApproveLevel('Supplier Return Note');
	$supReturnApproveLevel = $ApproveLevels+1;
	
	
//------------save---------------------------	
	if($requestType=='save')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		$savableFlag= 1;
		
		if($serialNo==''){
			$serialNo 	= getNextReturnNo();
			$year = date('Y');
			$editMode=0;
			$savedStatus=$supReturnApproveLevel;
			$savedLevels=$ApproveLevels;
		}
		else{
			$editMode=1;
			$savableFlag=getSaveStatus($serialNo,$year);
			$sql = "SELECT
			ware_returntosupplierheader.intStatus, 
			ware_returntosupplierheader.intApproveLevels 
			FROM ware_returntosupplierheader 
			WHERE
			ware_returntosupplierheader.intReturnNo =  '$serialNo' AND
			ware_returntosupplierheader.intReturnYear =  '$year'";

			$result = $db->RunQuery2($sql);
			$row=mysqli_fetch_array($result);
			$savedStatus=$row['intStatus'];
			$savedLevels=$row['intApproveLevels'];
		}
		//--------------------------
		$response['arrData'] = $objwhouseget->getLocationValidation('RTSUP',$companyId,$grnNo,$grnNoYear);//check for grn location
		//--------------------------
		$editPermission=loadEditMode($programCode,$savedStatus,$savedLevels,$userId);
		$locationEditFlag=getEditLocationValidation('RTSUP',$companyId,$serialNo,$year);
		//--------------------------
		
		if($response['arrData']['type']=='fail'){
			 $rollBackFlag=1;
			 $rollBackMsg=$response['arrData']['msg'];
		 }
		 else if(( $locationEditFlag==1) && ($editMode==1)){
			 $rollBackFlag=1;
			 $rollBackMsg="Invalid Edit Location";
		 }
		else if($savableFlag==0){
			 $rollBackFlag=1;
			 $rollBackMsg="This Supplier Return Note is already confirmed.cant edit";
		 }
		 else if($editPermission==0){
			 $rollBackFlag=1;
			 $rollBackMsg="No Permission to save.";
		 }
		else if($editMode==1){
		$sql = "UPDATE `ware_returntosupplierheader` SET REMARKS ='$remarks',
														intStatus ='$supReturnApproveLevel', 
												       intApproveLevels ='$ApproveLevels', 
													  intModifiedBy ='$userId', 
													  dtmModifiedDate =now(), 
													  intCompanyId ='$companyId'  
				WHERE (`intReturnNo`='$serialNo') AND (`intReturnYear`='$year')";
		$result = $db->RunQuery2($sql);
		
		//inactive previous recordrs in approvedby table
		$maxAppByStatus=(int)getMaxAppByStatus($serialNo,$year)+1;
		$sql = "UPDATE `ware_returntosupplierheader_approvedby` SET intStatus ='$maxAppByStatus' 
				WHERE (`intReturnNo`='$serialNo') AND (`intYear`='$year') AND (`intStatus`='0')";
		$result1 = $db->RunQuery2($sql);
		
		}
		else{
			//$sql = "DELETE FROM `ware_returntosupplierheader` WHERE (`intReturnNo`='$serialNo') AND (`intReturnYear`='$year')";
			//$result1 = $db->RunQuery2($sql);

			$sql = "INSERT INTO `ware_returntosupplierheader` (`intReturnNo`,`intReturnYear`,`intGrnNo`,intGrnYear,REMARKS,intStatus,intApproveLevels,datdate,dtmCreateDate,intUser,intCompanyId) 
					VALUES ('$serialNo','$year','$grnNo','$grnNoYear','$remarks','$supReturnApproveLevel','$ApproveLevels',now(),now(),'$userId','$companyId')";
			$result = $db->RunQuery2($sql);
		}
		//-----------delete and insert to detail table-----------------------
		if(($result) && ($rollBackFlag!=1)){
		$sql = "DELETE FROM `ware_returntosupplierdetails` WHERE (`intReturnNo`='$serialNo') AND (`intReturnYear`='$year')";
		$result2 = $db->RunQuery2($sql);
			$toSave=0;
			$saved=0;
			$rollBackFlag=0;
			$rollBackMsg ="Maximum Return Qtys for items are..."; 
			foreach($arr as $arrVal)
			{
				$itemId 	 = $arrVal['itemId'];
				$itemName 	 = $arrVal['itemName'];
				$Qty 		 = round($arrVal['Qty'],4);
				
			    $stockBalQty=round(getStockBalance($itemId,$companyId),4);

				//------check maximum Return Qty--------------------
				  $sqlc = "SELECT
				mst_item.strName as itemName,
				tb1.dblGrnQty,
				tb1.dblFocQty, 
				IFNULL((SELECT
				Sum(ware_returntosupplierdetails.dblQty)
				FROM
				ware_returntosupplierdetails
				Inner Join ware_returntosupplierheader ON ware_returntosupplierheader.intReturnNo = ware_returntosupplierdetails.intReturnNo AND ware_returntosupplierheader.intReturnYear = ware_returntosupplierdetails.intReturnYear
				WHERE 
				ware_returntosupplierheader.intStatus > 0 AND 
				ware_returntosupplierheader.intStatus <=  ware_returntosupplierheader.intApproveLevels AND
				ware_returntosupplierheader.intGrnNo =  tb1.intGrnNo AND
				ware_returntosupplierheader.intGrnYear =  tb1.intGrnYear AND
				ware_returntosupplierdetails.intItemId =  tb1.intItemId),0) as dblRetunSupplierQty 
				FROM
				ware_grndetails as tb1  
				Inner Join mst_item ON tb1.intItemId = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId  
				Inner Join ware_grnheader ON tb1.intGrnNo = ware_grnheader.intGrnNo AND tb1.intGrnYear = ware_grnheader.intGrnYear
				WHERE 
				ware_grnheader.intStatus =  '1' AND 
				tb1.intGrnNo =  '$grnNoArray[0]' AND
				tb1.intGrnYear =  '$grnNoArray[1]' AND
				tb1.intItemId =  '$itemId'";
				$resultsc = $db->RunQuery2($sqlc);
				$rowc = mysqli_fetch_array($resultsc);

				if(round(($rowc['dblGrnQty']- round($rowc['dblRetunSupplierQty'],4)),4)<$stockBalQty)
				$balQtyToReturn = round(($rowc['dblGrnQty']-round($rowc['dblRetunSupplierQty'], 4)),4);
				else
				$balQtyToReturn = $stockBalQty;
				if($Qty>$balQtyToReturn){
				//	call roll back--------****************
					$rollBackFlag=1;
					$rollBackMsg .="</br> ".$itemName." =".$balQtyToReturn;
					//exit();
				}

				//----------------------------
				if($rollBackFlag!=1){
					$sql = "INSERT INTO `ware_returntosupplierdetails` (`intReturnNo`,`intReturnYear`,`intItemId`,`dblQty`) 
					VALUES ('$serialNo','$year','$itemId','$Qty')";
					$result3 = $db->RunQuery2($sql);
					if($result3==1){
					$saved++;
					}
				}
				$toSave++;
			}
		}
		//echo $rollBackFlag;
		
		if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= '';
		}
		else if(($result) && ($toSave==$saved)){
			$confirmationMode=getConfirmationMode($programCode,$userId,$ApproveLevels,$supReturnApproveLevel);
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			if($editMode==1)
			$response['msg'] 		= 'Updated successfully.';
			else
			$response['msg'] 		= 'Saved successfully.';
			
			$response['serialNo'] 		= $serialNo;
			$response['year'] 		= $year;
			$response['confirmationMode'] = $confirmationMode;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		
		$db->CloseConnection();		
	}
//----------------------------------------
	echo json_encode($response);
//-----------------------------------------	
	function getNextReturnNo()
	{
		global $db;
		global $companyId;
		$sql = "SELECT
				sys_no.intSupReturnNo
				FROM sys_no
				WHERE
				sys_no.intCompanyId =  '$companyId'
				";	
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$nextReturnNo = $row['intSupReturnNo'];
		
		$sql = "UPDATE `sys_no` SET intSupReturnNo=intSupReturnNo+1 WHERE (`intCompanyId`='$companyId')  ";
		$db->RunQuery2($sql);
		
		return $nextReturnNo;
	}
//---------------------------------------------------	
	function getStockBalance($item,$location)
	{
		global $db;
		 $sql = "SELECT
				Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$location' AND 
				ware_stocktransactions_bulk.strType <>  'ITEMCREATION+' 
				GROUP BY
				ware_stocktransactions_bulk.intItemId";

		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	
	//--------------------------------------------------------------
	function getSaveStatus($serialNo,$year)
	{
		global $db;
		$sql = "SELECT ware_returntosupplierheader.intStatus, ware_returntosupplierheader.intApproveLevels FROM ware_returntosupplierheader WHERE (intReturnNo='$serialNo') AND (`intReturnYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
			
			if(($status==0) || ($row['intStatus']==($row['intApproveLevels']+1))){
				$editableFlag=1	;
			}
			else{
				$editableFlag=0;	
			}
			
		return $editableFlag;
	}
//-----------------------------------------------------------
	function getEditLocationValidation($type,$location,$serialNo,$year)
	{
		$flag= 1;
		
		global $db;
		  $sql = "SELECT
					ware_returntosupplierheader.intCompanyId
					FROM
					ware_returntosupplierheader
					Inner Join mst_locations ON ware_returntosupplierheader.intCompanyId = mst_locations.intId
					WHERE
					ware_returntosupplierheader.intReturnNo =  '$serialNo' AND
					ware_returntosupplierheader.intReturnYear =  '$year'";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			if($location == $row['intCompanyId'])
			$flag=0;
 			
		return $flag;

}
//------------------------------function loadConfirmatonMode-------------------
function getConfirmationMode($programCode,$intUser,$approveLevels,$status){
		global $db;
		$k=$approveLevels+2-$status;
		$sqlp = "SELECT 
		menupermision.intEdit, 
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		if($rowp['int'.$k.'Approval']==1) 
		 $confirmatonMode=1; 
		else
		 $confirmatonMode=0; 
	 
	return $confirmatonMode;
}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		$resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
		 
		// $editMode;
		 
	return $editMode;
}
//------------------------------function get MaxAppByStatus---------------------
function getMaxAppByStatus($serialNo,$year){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	  $sql = "SELECT
				max(ware_returntosupplierheader_approvedby.intStatus) as status 
				FROM
				ware_returntosupplierheader_approvedby
				WHERE
				ware_returntosupplierheader_approvedby.intReturnNo =  '$serialNo' AND
				ware_returntosupplierheader_approvedby.intYear =  '$year'";
				
		$resultm = $db->RunQuery2($sql);
		 $rowm=mysqli_fetch_array($resultm);
		 
		 return $rowm['status'];

}
//--------------------------------------------------------getMaxAppByStatus
?>



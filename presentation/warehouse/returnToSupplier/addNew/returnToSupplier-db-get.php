<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	///
	/////////// type of print load part /////////////////////
	if($requestType=='loadHeaderData')
	{
		$grnNo  = $_REQUEST['grnNo'];
		$grnNoArray = explode('/',$grnNo);
		$grnNo 	 = $grnNoArray[0];
		$grnYear = $grnNoArray[1];
		
		$sql = "SELECT
				ware_grnheader.strInvoiceNo,
				trn_poheader.intSupplier,
				mst_supplier.strName
				FROM
				ware_grnheader
				Inner Join trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
				Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
				WHERE
				ware_grnheader.intGrnNo =  '$grnNo' AND
				ware_grnheader.intGrnYear =  '$grnYear'";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['invoiceNo'] 	= $row['strInvoiceNo'];
			$response['supplier'] = $row['strName'];
		}
		echo json_encode($response);
	}
	else if($requestType=='loadSubCategory')
	{
		$mainCategory  = $_REQUEST['mainCategory'];
		$sql = "SELECT
				mst_subcategory.intId,
				mst_subcategory.strCode,
				mst_subcategory.strName
				FROM mst_subcategory
				WHERE
				mst_subcategory.intMainCategory =  '$mainCategory' 
				ORDER BY mst_subcategory.strName ASC";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	else if($requestType=='loadItems')
	{
		$grnNo  = $_REQUEST['grnNo'];
		$grnNoArray 	 = explode('/',$grnNo);
		$mainCategory  = $_REQUEST['mainCategory'];
		$subCategory  = $_REQUEST['subCategory'];
		$description  = $_REQUEST['description'];
		
		  $sql="SELECT
				mst_item.intMainCategory,
				mst_item.intSubCategory,
				mst_maincategory.strName as mainCatName,
				mst_subcategory.strName as subCatName,
				mst_item.strCode,
                mst_item.strCode as SUP_ITEM_CODE,
				mst_item.strName as itemName,
				tb1.intItemId,
				tb1.dblGrnQty,
				tb1.dblFocQty, 
				IFNULL((SELECT
				Sum(ware_returntosupplierdetails.dblQty)
				FROM
				ware_returntosupplierdetails
				Inner Join ware_returntosupplierheader ON ware_returntosupplierheader.intReturnNo = ware_returntosupplierdetails.intReturnNo AND ware_returntosupplierheader.intReturnYear = ware_returntosupplierdetails.intReturnYear
				WHERE 
				ware_returntosupplierheader.intStatus > 0 AND 
				ware_returntosupplierheader.intStatus <=  ware_returntosupplierheader.intApproveLevels AND
				ware_returntosupplierheader.intGrnNo =  tb1.intGrnNo AND
				ware_returntosupplierheader.intGrnYear =  tb1.intGrnYear AND
				ware_returntosupplierdetails.intItemId =  tb1.intItemId),0) as dblRetunSupplierQty , 
				mst_units.strCode as uom , 
				tb2.intCompanyId 
				FROM
				ware_grndetails as tb1 
				Inner Join mst_item ON tb1.intItemId = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId  
				Inner Join ware_grnheader as tb2 ON tb1.intGrnNo = tb2.intGrnNo AND tb1.intGrnYear = tb2.intGrnYear 
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId
                                LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
				WHERE 
				mst_item.intStatus = '1' AND 
				tb2.intStatus =  '1' AND 
				tb1.intGrnNo =  '$grnNoArray[0]' AND
				tb1.intGrnYear =  '$grnNoArray[1]' ";
				if($mainCategory!='')
				$sql.=" AND mst_item.intMainCategory =  '$mainCategory' ";
				if($subCategory!='')
				$sql.=" AND mst_item.intSubCategory =  '$subCategory'";
				if($description!=''){
				$sql.=" AND
				mst_item.strName LIKE  '%$description%'";
				}
				$sql.=" AND tb1.dblGrnQty-tb1.dblRetunSupplierQty>0";
				$sql.=" Order by mst_maincategory.strName asc, mst_subcategory.strName asc, mst_item.strName asc";

		//  echo $sql;
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$stockBalQty=getStockBalance($row['intItemId'],$row['intCompanyId']);
			$allocatedQty=allocatedQty($row['intItemId'],$row['intCompanyId'],$grnNoArray[0],$grnNoArray[1]);

			$data['itemId'] 	= $row['intItemId'];
			$data['maincatId'] = $row['intMainCategory'];
			$data['subCatId'] 	= $row['intSubCategory'];
			$data['code'] = $row['strCode'];
                        $data['supItemCode'] = $row['SUP_ITEM_CODE'];
			$data['uom'] = $row['uom'];
			$data['itemName'] = $row['itemName'];
			$data['grnQty'] 	= $row['dblGrnQty'];
			$data['nonAllocQty'] 	= $row['dblGrnQty']-$allocatedQty;
			$data['returnedQty'] 	= $row['dblRetunSupplierQty'];
			$data['stockBal'] = $stockBalQty;
			
			if($row['dblGrnQty']-$row['dblRetunSupplierQty']<$stockBalQty)
			$data['Qty'] = $row['dblGrnQty']-$row['dblRetunSupplierQty'];
			else
			$data['Qty'] = $stockBalQty;
			
			$data['mainCatName'] = $row['mainCatName'];
			$data['subCatName'] = $row['subCatName'];
			
			$arrCombo[] = $data;
		}
		$response['arrCombo'] 	= $arrCombo;
		echo json_encode($response);

	}
	
	
	function getStockBalance($item,$location)
	{
		global $db;
		 $sql = "SELECT
				Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$location' AND 
				ware_stocktransactions_bulk.strType <>  'ITEMCREATION+' 
				GROUP BY
				ware_stocktransactions_bulk.intItemId";

		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	
	
	function allocatedQty($item,$location,$grnNo,$grnYear)
	{
		global $db;
		 $sql = "SELECT
				Sum(ware_stocktransactions.dblQty) AS stockBal
				FROM ware_stocktransactions
				WHERE
				ware_stocktransactions.intItemId =  '$item' AND
				ware_stocktransactions.intLocationId =  '$location' AND 
				ware_stocktransactions.strType =  'ALLOCATE' AND
				ware_stocktransactions.intGRNNo =  '$grnNo' AND
				ware_stocktransactions.intGRNYear =  '$grnYear'
				GROUP BY
				ware_stocktransactions.intItemId";

		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	
?>
	
var basePath	="presentation/warehouse/returnToSupplier/addNew/";
var reportId	="897";
	
			
$(document).ready(function() {
	
  		$("#frmReturnToSupplier").validationEngine();
		$('#frmReturnToSupplier #cboSupplier').focus();
		
		
		$("#frmReturnToSupplier #butSearch").die('click').live('click',function(){
			//alert('ok');
			clearRows();
			loadHeaderData();
		});
		
				calfunction()

		
		$("#frmReturnToSupplier #butAddItems").die('click').live('click',function(){
			//clearRows();
			closePopUp();
			loadPopup();
		});
		
	//-------------------------------------------
	//checkAlreadySelected();
	$('#frmReturnToSupplierPopup #butAdd').die('click').live('click',addClickedRows);
	$('#frmReturnToSupplierPopup #butClose1').die('click').live('click',disablePopup);
	//-------------------------------------------- 
	$('#frmReturnToSupplierPopup #cboMainCategory').die('change').live('change',function(){
		var mainCategory = $('#cboMainCategory').val();
		var url 		= basePath+"returnToSupplier-db-get.php?requestType=loadSubCategory&mainCategory="+mainCategory;
		var httpobj 	= $.ajax({url:url,async:false})
		document.getElementById('cboSubCategory').innerHTML=httpobj.responseText;
		
	});
	//-------------------------------------------- 
	$('#frmReturnToSupplierPopup #imgSearchItems').die('click').live('click',function(){
		var rowCount = document.getElementById('tblItemsPopup').rows.length;
		for(var i=1;i<rowCount;i++)
		{
			document.getElementById('tblItemsPopup').deleteRow(1);
		}
		var grnNo = $('#txtPopGrnNo').val();
		var mainCategory = $('#cboMainCategory').val();
		var subCategory = $('#cboSubCategory').val();
		var description = $('#txtItmDesc').val();
		var url 		= basePath+"returnToSupplier-db-get.php?requestType=loadItems";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"grnNo="+grnNo+"&mainCategory="+mainCategory+"&subCategory="+subCategory+"&description="+description,
			async:false,
			success:function(json){
	
				var length = json.arrCombo.length;
				var arrCombo = json.arrCombo;
	
	
				for(var i=0;i<length;i++)
				{
					var itemId=arrCombo[i]['itemId'];	
					var maincatId=arrCombo[i]['maincatId'];	
					var subCatId=arrCombo[i]['subCatId'];	
					var code=arrCombo[i]['code'];
                                        var supItemCode=arrCombo[i]['supItemCode'];
					var uom=arrCombo[i]['uom'];	
					var itemName=arrCombo[i]['itemName'];	
					var grnQty=arrCombo[i]['grnQty'];	
					var nonAllocQty=arrCombo[i]['nonAllocQty'];	
					var returnedQty=arrCombo[i]['returnedQty'];	
					var balToReturn=parseFloat(grnQty)-parseFloat(returnedQty)
					var stockBal=arrCombo[i]['stockBal'];	
					var mainCatName=arrCombo[i]['mainCatName'];
					var subCatName=arrCombo[i]['subCatName'];	
					var Qty=arrCombo[i]['Qty'];
						
					var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF" id=""><input type="checkbox" id="chkDisp" /></td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'" class="mainCatNameP">'+mainCatName+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'" class="subCatNameP">'+subCatName+'</td>';
                                        if(supItemCode!=null)
                                        {
                                        content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="codeP">'+supItemCode+'</td>';   
                                        }else{    
					content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="codeP">'+code+'</td>';
                                        }
					content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="itemP">'+itemName+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+uom+'" class="uomP">'+uom+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" style="display:none"  class="grnQtyP">'+grnQty+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+Qty+'" style="display:none"  class="QtyP">'+stockBal+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+grnQty+'" class="grnQty1P">'+grnQty+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+nonAllocQty+'" class="nonAllocQtyP">'+nonAllocQty+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+returnedQty+'" class="returnedQtyP">'+returnedQty+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+balToReturn+'" class="balToReturnP">'+balToReturn+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+stockBal+'" class="stockBalP">'+stockBal+'</td></tr>';
	
					add_new_row('#frmReturnToSupplierPopup #tblItemsPopup',content);
				}
					checkAlreadySelected();
	
			}
		});
		
	});
   //-------------------------------------------- 
  $('#frmReturnToSupplier #cboCurrency').die('change').live('change',function(){
	    var currency = $('#cboCurrency').val();
		var url 		= basePath+"returnToSupplier-db-get.php?requestType=loadExchangeRate";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"currency="+currency,
			async:false,
			success:function(json){

					document.getElementById("divExchRate").innerHTML=json.excRate;
			}
		});
		
  });
  //-------------------------------------------------------
   $('#frmReturnToSupplier #cboSupplier').die('change').live('change',function(){
	    var supplier = $('#cboSupplier').val();
		var url 		= basePath+"returnToSupplier-db-get.php?requestType=loadSupplierDetails";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"supplier="+supplier,
			async:false,
			success:function(json){

					$('#frmReturnToSupplier #cboCurrency').val(json.currency);
					$('#frmReturnToSupplier #cboPayTerm').val(json.payTerm);
					$('#frmReturnToSupplier #cboPayMode').val(json.payMode);
					$('#frmReturnToSupplier #cboShipmentMode').val(json.shipmentMode);
					document.getElementById("divExchRate").innerHTML=json.excRate;
			}
		});
		
  });
  //-------------------------------------------------------
  $('#frmReturnToSupplier .clsValidateBalQty').die('keyup').live('keyup',function(){
 //$(document.body).die('keyup').live('keyup', '.clsValidateBalQty' ,function(){			
 			var input=$(this).val();
			var balQty=$(this).closest('td').attr('id');
			if((input>balQty) || (input<0)){
			alert("Invalid Qty");
				$(this).val(balQty);
			}

		});
	
  $('#frmReturnToSupplier .calculateValue').die('keyup').live('keyup',function(){
 	//$(document.body).die('keyup').live('keyup', '.calculateValue' ,function(){			
		calfunction();
	});
//---------------------------------------------

  $('#frmReturnToSupplier #butSave').die('click').live('click',function(){
	$("#frmReturnToSupplier").validationEngine();
	var requestType = '';
	if ($('#frmReturnToSupplier').validationEngine('validate'))   
    { 
		showWaiting();
		var data = "requestType=save";
		
			data+="&serialNo="		+	$('#txtRetSupNo').val();
			data+="&Year="	+	$('#txtRetSupYear').val();
			data+="&invoiceNo="	+	$('#txtInvoiceNo').val();
			data+="&grnNo="	+	$('#txtGrnNo').val();
			data+="&date="			+	$('#dtDate').val();
			data+="&remarks="	+	URLEncode($('#txtRemarks').val());


			var rowCount = document.getElementById('tblRetSupItems').rows.length;
			if(rowCount==1){
				alert("items not selected to return");hideWaiting();
				return false;				
			}
			var row = 0;
			var errorFlag=0;
			
			var arr="[";
			
			$('#tblRetSupItems .item').each(function(){
	
				var itemId	= $(this).attr('id');
				var itemName	= URLEncode($(this).html());
				var Qty	= $(this).parent().find(".Qty").val();
				
					if(Qty>0){
						arr += "{";
						arr += '"itemId":"'+		itemId +'",' ;
						arr += '"itemName":"'+		itemName +'",' ;
						arr += '"Qty":"'+		Qty  +'"' ;
						arr +=  '},';
						
					}
					else{
						errorFlag=1;
					}
			});
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;
			
			
			if(errorFlag==1){
				alert("Please enter Quantities");hideWaiting();
				return false;
			}

		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = basePath+"returnToSupplier-db-set.php";
     	var obj = $.ajax({
			url:url,
			type:'post',
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmReturnToSupplier #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						//$('#frmSampleInfomations').get(0).reset();
						//saveImage(json.sampleNo,json.sampleYear,json.revisionNo);
						//loadCombo_frmSampleInfomations();
						var t=setTimeout("alertx()",1000);
						$('#txtRetSupNo').val(json.serialNo);
						$('#txtRetSupYear').val(json.year);
						if(json.confirmationMode==1){
							$('#butConfirm').show();
						}
						else{
							$('#butConfirm').hide();
						}
						return;
					}
				},
			error:function(xhr,status){
					
					$('#frmReturnToSupplier #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
			hideWaiting();
	}
   });
   
	
	//--------------refresh the form----------
	$('#frmReturnToSupplier #butNew').die('click').live('click',function(){
		window.location.href = "?q=226";
		$('#frmReturnToSupplier').get(0).reset();
		clearRows();
		$('#frmReturnToSupplier #txtRetSupNo').val('');
		$('#frmReturnToSupplier #txtRetSupYear').val('');
		$('#frmReturnToSupplier #txtTotAmnt').val('');
		$('#frmReturnToSupplier #txtGrnNo').val('');
		$('#frmReturnToSupplier #txtSupplier').val('');
		$('#frmReturnToSupplier #txtInvoiceNo').val('');
		$('#frmReturnToSupplier #cboDispatchTo').focus();
		document.getElementById('butSearch').style.display='';
		document.getElementById('divFormat').style.display='';
		document.getElementById('butAddItems').style.display='none';
		
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmReturnToSupplier #dtDate').val(d);
	});
	//----------------------------------------
	
/*    $('#frmReturnToSupplier #butDelete').click(function(){
		if($('#frmReturnToSupplier #cboSearch').val()=='')
		{
			$('#frmReturnToSupplier #butDelete').validationEngine('showPrompt', 'Please select Type.', 'fail');
			var t=setTimeout("alertDelete()",1000);	
		}
		else
		{
			var val = $.prompt('Are you sure you want to delete "'+$('#frmReturnToSupplier #cboSearch option:selected').text()+'" ?',{
								buttons: { Ok: true, Cancel: false },
								callback: function(v,m,f){
									if(v)
									{
										var url = "typeOfPrint-db-set.php";
										var httpobj = $.ajax({
											url:url,
											dataType:'json',
											data:'requestType=delete&cboSearch='+$('#frmReturnToSupplier #cboSearch').val(),
											async:false,
											success:function(json){
												
												$('#frmReturnToSupplier #butDelete').validationEngine('showPrompt', json.msg,json.type );
												
												if(json.type=='pass')
												{
													$('#frmReturnToSupplier').get(0).reset();
													loadCombo_frmReturnToSupplier();
													var t=setTimeout("alertDelete()",1000);return;
												}	
												var t=setTimeout("alertDelete()",3000);
											}	 
										});
									}
				}
		 	});
			
		}
	});
	
*/	
//----------------------------	
// $(document).die('click').live('click', '.delImg' ,function(){			
$('#frmReturnToSupplier .delImg').die('click').live('click',function(){
	$(this).parent().parent().remove();
	calfunction();

});

//----------------------------	

//-----------------------------------
$('#frmReturnToSupplier #butReport').die('click').live('click',function(){
	if($('#txtRetSupNo').val()!=''){
		window.open('?q='+reportId+'&retSupNo='+$('#txtRetSupNo').val()+'&year='+$('#txtRetSupYear').val());	
	}
	else{
		alert("There is no Return Note to view");
	}
});
//----------------------------------	
$('#frmReturnToSupplier #butConfirm').die('click').live('click',function(){
	if($('#txtRetSupNo').val()!=''){
		window.open('?q='+reportId+'&retSupNo='+$('#txtRetSupNo').val()+'&year='+$('#txtRetSupYear').val()+'&approveMode=1');	
	}
	else{
		alert("There is no Return Note to confirm");
	}
});
//-----------------------------------------------------
$('#frmReturnToSupplier #butClose').die('click').live('click',function(){
		//load('main.php');	
});
//--------------refresh the form-----------------------
	$('#frmReturnToSupplier #butNew').die('click').live('click',function(){
		window.location.href = "?q=226";
		$('#frmReturnToSupplier').get(0).reset();
		clearRows();
		$('#frmReturnToSupplier #txtRetSupNo').val('');
		$('#frmReturnToSupplier #txtRetSupYear').val('');
		$('#frmReturnToSupplier #txtTotAmnt').val('');
		$('#frmReturnToSupplier #txtGrnNo').val('');
		$('#frmReturnToSupplier #txtSupplier').val('');
		$('#frmReturnToSupplier #txtInvoiceNo').val('');
		$('#frmReturnToSupplier #cboDispatchTo').focus();
		document.getElementById('butSearch').style.display='';
		document.getElementById('divFormat').style.display='';
		document.getElementById('butAddItems').style.display='none';
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmReturnToSupplier #dtDate').val(d);
	});
//-----------------------------------------------------
});

//----------end of ready -------------------------------

//-------------------------------------------------------
function loadPopup()
{
		popupWindow3('1');
		var grnNo = $('#txtGrnNo').val();
		$('#popupContact1').load(basePath+'returnToSupplierPopup.php?grnNo='+grnNo,function(){
			});	
}//-------------------------------------------------------
function loadMain()
{
	//alert(1);
/*	$("#butAddPendingDispatch").die('click').live('click',function(){
		popupWindow3('1');
		//$('#iframeMain1').attr('src','http://localhost/qpet/presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php?id='+$('#cboDispatchTo').val());
		$('#popupContact1').load('sampleDispatchPopup.php');	
		
	});*/
	//$("#iframeMain1").contents().find("#butAdd").die('click').live('click',abc);
	//$("#frmReturnToSupplierPopup").contents().find("#butAdd").die('click').live('click',abc);
//	$('#frmReturnToSupplierPopup #butClose').die('click').live('click',abc);
}

//----------------------------------------------------
function addClickedRows()
{
	//var rowCount = $('#tblDispatchPopup >tr').length;
	var totRetAmmount=0;
	var i=0;
	$('#tblItemsPopup .itemP').each(function(){
		i++;
		if((document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].checked==true) && (document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].disabled==false)){
			
/*			var itemId=document.getElementById('tblItemsPopup').rows[i].cells[3].id;
			var maincatId=document.getElementById('tblItemsPopup').rows[i].cells[1].id;
			var subCatId=document.getElementById('tblItemsPopup').rows[i].cells[2].id;
			var code=document.getElementById('tblItemsPopup').rows[i].cells[3].innerHTML;
			var itemName=document.getElementById('tblItemsPopup').rows[i].cells[4].innerHTML;
			var unitPrice=parseFloat(document.getElementById('tblItemsPopup').rows[i].cells[5].id);
			var mainCatName=document.getElementById('tblItemsPopup').rows[i].cells[1].innerHTML;
			var subCatName=document.getElementById('tblItemsPopup').rows[i].cells[2].innerHTML;
			var grnQty=parseFloat(document.getElementById('tblItemsPopup').rows[i].cells[5].innerHTML);
			var nonAllocQty=parseFloat(document.getElementById('tblItemsPopup').rows[i].cells[8].innerHTML);
			var returnQty=parseFloat(document.getElementById('tblItemsPopup').rows[i].cells[9].innerHTML);
			var stockBalQty=parseFloat(document.getElementById('tblItemsPopup').rows[i].cells[6].innerHTML);
			var Qty=parseFloat(document.getElementById('tblItemsPopup').rows[i].cells[6].id);
*/
			var itemId=$(this).attr('id');
			var maincatId=$(this).parent().find(".mainCatNameP").attr('id');
			var subCatId=$(this).parent().find(".subCatNameP").attr('id');
			var code=$(this).parent().find(".codeP").html();
			var uom=$(this).parent().find(".uomP").html();
			var itemName=$(this).parent().find(".itemP").html();
			var mainCatName=$(this).parent().find(".mainCatNameP").html();
			var subCatName=$(this).parent().find(".subCatNameP").html();
			var grnQty=parseFloat($(this).parent().find(".grnQtyP").html());
			var nonAllocQty=parseFloat($(this).parent().find(".nonAllocQtyP").html());
			var returnQty=parseFloat($(this).parent().find(".returnedQtyP").html());
			var stockBalQty=parseFloat($(this).parent().find(".stockBalP").html());
			var Qty=parseFloat($(this).parent().find(".QtyP").attr('id'));

			//alert($('#frmReturnToSupplier #cboDispatchTo').val());
			var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF"><img class="delImg" src="images/del.png" width="15" height="15" /></td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'">'+mainCatName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'">'+subCatName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="item">'+itemName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+uom+'" class="uom">'+uom+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'">'+grnQty+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'">'+nonAllocQty+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+returnQty+'">'+returnQty+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+stockBalQty+'">'+stockBalQty+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id=""><input  id="'+itemId+'" class="validate[required,custom[number],max['+Qty+']] calculateValue Qty" style="width:80px;text-align:center" type="text" value="'+Qty+'"/></td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="" style="display:none"><input  id="'+itemId+'" class="validate[required,custom[integer],max['+Qty+']] calculateValue" style="width:80px;text-align:center" type="text" value="0"/></td>';
			content +='</tr>';
			
			
		//	content +='<td align="center" bgcolor="#FFFFFF" id="" class=""><input style="width:80px;text-align:center" type="text" value="0"/></td>';
		
			add_new_row('#frmReturnToSupplier #tblRetSupItems',content);
			totRetAmmount+=Qty;
			//$('.')
			//$("#frmReturnToSupplier").validationEngine();
		//----------------------------	
/*		 $(document).die('click').live('click', '.delImg' ,function(){			
			$(this).parent().parent().remove();
			calfunction();

		});
*/		
		//----------------------------	
		
		calfunction();
		}
	});
	$('#txtTotAmnt').val(totRetAmmount)
	disablePopup();
	closePopUp();
}

function calfunction()
{
	var total=0;
	$('#tblRetSupItems .item').each(function(){
	
		var Qty	= parseFloat($(this).parent().find(".Qty").val());
		total+=Qty;
			
	 });
	document.getElementById('txtTotAmnt').value=total;
 }
//-------------------------------------
function alertx()
{
	$('#frmReturnToSupplier #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmReturnToSupplier #butDelete').validationEngine('hide')	;
}

function closePopUp(){
	
}
//----------------------------------------------
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
//----------------------------------------------- 
function clearRows()
{
	//var rowCount = $('#tblDispatchPopup >tbody >tr').length;
	var rowCount = document.getElementById('tblRetSupItems').rows.length;
	//alert(rowCount);
	//var cellCount = document.getElementById('tblMain').rows[1].cells.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblRetSupItems').deleteRow(1);
			
	}
}
//------------------------------------------------------------
function checkAlreadySelected(){
	
	var rowCount = document.getElementById('tblRetSupItems').rows.length;
	for(var i=1;i<rowCount;i++)
	{
			var itemNo = 	document.getElementById('tblRetSupItems').rows[i].cells[4].id;
			var prnNo = 	document.getElementById('tblRetSupItems').rows[i].cells[1].innerHTML;
		
			var rowCount1 = document.getElementById('tblItemsPopup').rows.length;
			for(var j=1;j<rowCount1;j++)
			{
				var itemNoP = 	document.getElementById('tblItemsPopup').rows[j].cells[3].id;
			//	var prnNoP = 	document.getElementById('cboPRN').value;
				if((itemNo==itemNoP)){
					document.getElementById('tblItemsPopup').rows[j].cells[0].childNodes[0].checked=true;
					document.getElementById('tblItemsPopup').rows[j].cells[0].childNodes[0].disabled=true;
				}
			}
	}
}
//------------------------------------------------------------------
function loadHeaderData(){
	    var grnNo = $('#txtGrnNo').val();
		var url 		= basePath+"returnToSupplier-db-get.php?requestType=loadHeaderData";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"grnNo="+grnNo,
			async:false,
			success:function(json){

					$('#frmReturnToSupplier #txtSupplier').val(json.supplier);
					$('#frmReturnToSupplier #txtInvoiceNo').val(json.invoiceNo);
					if(json.supplier!=''){
					document.getElementById('butAddItems').style.display='';
					}

			}
		});
}


<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
///////////////////////////////
// H B G KORALA   ////////////
//////////////////////////////
$mainPath 		= $_SESSION['mainPath'];
$location 		= $_SESSION['CompanyID'];
$company 		= $_SESSION['headCompanyId'];
$intUser  		= $_SESSION["userId"];
$thisFilePath 	=  $_SERVER['PHP_SELF'];
 
$retSupNo 		= $_REQUEST['retSupNo'];
$year 			= $_REQUEST['year'];
$programName	='Supplier Return Note';
$programCode	='P0226';
$menuID			='226';

if(($retSupNo=='')&&($year=='')){
	$savedStat 	= '';
	$invoiceNo 	= '';
	$grnNo 		=  '';
	$grnYear 	= '';
	$date 		= '';
	$supplier 	=  '';
	$intStatus	= $savedStat+1;
}
else{
	$result=loadHeader($retSupNo,$year);
	while($row=mysqli_fetch_array($result))
	{
		$invoiceNo = $row['strInvoiceNo'];
		$grnNo = $row['intGrnNo'];
		$grnYear = $row['intGrnYear'];
		$date = $row['datdate'];
		$supplier = $row['supplier'];
		$savedStat=$row['intApproveLevels'];
		$intStatus=$row['intStatus'];
		$remarks=$row['REMARKS'];
	}
}
			 

$editMode=loadEditMode($programCode,$intStatus,$savedStat,$intUser);
$confirmatonMode=loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser);

if($retSupNo==''){
	$confirmatonMode=0;	
}
?>
<script>
	//global variables


</script>
 <title>Return To Supplier</title>
<?php
//include "include/javascript.html";
?>
<!--<script type="text/javascript" src="presentation/warehouse/returnToSupplier/addNew/returnToSupplier-js.js"></script>
--> 
 
<form id="frmReturnToSupplier" name="frmReturnToSupplier" method="post" action="">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">Return To Supplier</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="10%" height="22" class="normalfnt">Return No</td>
            <td width="22%"><input name="txtRetSupNo" type="text" disabled="disabled" class="txtText" id="txtRetSupNo" style="width:60px" value="<?php echo $retSupNo ?>" /><input name="txtRetSupYear" type="text" disabled="disabled" class="txtText" id="txtRetSupYear" style="width:40px"value="<?php echo $year ?>" /></td>
            <td width="17%">&nbsp;</td>
            <td width="23%">&nbsp;</td>
            <td width="12%" class="normalfnt">Return Amount</td>
            <td width="16%"><input name="txtTotAmnt" type="text" disabled="disabled" class="txtNumber" id="txtTotAmnt" style="width:120px; text-align:right" /></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="10%" height="22" class="normalfnt">GRN No</td>
            <td width="21%"><input name="txtGrnNo" type="text" class="txtText" id="txtGrnNo" style="width:153px" value="<?php if($grnNo){ echo $grnNo."/".$grnYear;}?>" <?php if($editMode==0){  ?> disabled="disabled"<?php } ?> />&nbsp;&nbsp;<img src="images/search.png"  class="mouseover" id="butSearch" name="butSearch"  <?php if($retSupNo){?> style="display:none"<?php } ?> /></td>
            <td width="41%"><div id="divFormat" class="normalfnt" <?php if($retSupNo){?> style="display:none"<?php } ?>>(GRN Number/Year)</div></td>
            <td width="12%" class="normalfnt">Date</td>
            <td width="16%"><input name="dtDate" type="text" value="<?php if($retSupNo){ echo substr($date,0,10); }else { echo date("Y-m-d"); }?>" class="txtbox" id="dtDate" style="width:120px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php if($editMode==0){  ?> disabled="disabled"<?php } ?>/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
          </tr>
          <tr>
            <td height="27" class="normalfnt">Supplier</td>
            <td><input name="txtSupplier" type="text" disabled="disabled" class="txtText" id="txtSupplier" style="width:180px" value="<?php echo $supplier ?>" />&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">Invoice NO</td>
            <td><input name="txtInvoiceNo" type="text" disabled="disabled" class="txtText" id="txtInvoiceNo" style="width:120px" value="<?php echo $invoiceNo ?>" /></td>
          </tr>
     
<tr>
            <td height="27" class="normalfnt">Remarks</td>
            <td colspan="2"><textarea name="txtRemarks" id="txtRemarks" cols="46" rows="2" class="validate[maxSize[500]] normalfnt"><?php echo htmlentities($remarks); ?></textarea></td>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>     
        </table></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="92%">&nbsp;</td>
            <td width="8%"><?php if($editMode!=0){  ?><img src="images/Tadd.jpg" width="92" height="24" id="butAddItems" name="butAddItems" <?php if($retSupNo==''){  ?>style="display:none" <?php } ?>><?php } ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><div style="width:900px;height:300px;overflow:scroll" >
          <table width="100%" class="bordered" id="tblRetSupItems" >
            <tr >
              <th width="3%" height="22" >Del</th>
              <th width="13%" >Main Category</th>
              <th width="16%" >Sub Category</th>
              <th width="23%" >Item Description</th>
              <th width="6%">UOM</th>
              <th width="6%">GRN Qty</th>
              <th width="7%">Non<br />Allocated<br /> Qty</th>
              <th width="7%">Returned<br />Qty</th>
              <th width="7%">Stock<br />Balance</th>
              <th width="9%"> Qty</th>
              <th width="9%" style="display:none">Bin</th>
              </tr>
            <?php
			    $sql = "SELECT
ware_returntosupplierdetails.intItemId,
mst_item.strName as item,
mst_item.intMainCategory,
mst_maincategory.strName as maincat,
mst_item.intSubCategory,
mst_subcategory.strName as subcat,
mst_item.strCode,
mst_units.strCode as uom , 
tb1.dblGrnQty,
tb1.intGrnNo, 
tb1.intGrnYear, 
IFNULL((SELECT
	Sum(ware_returntosupplierdetails.dblQty)
	FROM
	ware_grndetails
	Inner Join ware_returntosupplierheader ON ware_grndetails.intGrnNo = ware_returntosupplierheader.intGrnNo AND ware_grndetails.intGrnYear = ware_returntosupplierheader.intGrnYear
	Inner Join ware_returntosupplierdetails ON ware_returntosupplierheader.intReturnNo = ware_returntosupplierdetails.intReturnNo AND ware_returntosupplierheader.intReturnYear = ware_returntosupplierdetails.intReturnYear
	 AND ware_grndetails.intItemId = ware_returntosupplierdetails.intItemId 
	WHERE 
	ware_returntosupplierheader.intStatus > 0 AND 
	ware_returntosupplierheader.intStatus <= ware_returntosupplierheader.intApproveLevels AND
	ware_returntosupplierheader.intGrnNo =  tb1.intGrnNo AND
	ware_returntosupplierheader.intGrnYear =  tb1.intGrnYear AND
	ware_returntosupplierdetails.intItemId =  tb1.intItemId),0) as dblRetunSupplierQty , 
ware_returntosupplierdetails.dblQty,
ware_returntosupplierheader.intCompanyId, 
ware_grnheader.intCompanyId as grnCompany, 
ware_returntosupplierheader.intStatus, 
ware_returntosupplierheader.intApproveLevels  
FROM
ware_returntosupplierdetails
Inner Join ware_returntosupplierheader ON ware_returntosupplierdetails.intReturnNo = ware_returntosupplierheader.intReturnNo AND ware_returntosupplierdetails.intReturnYear = ware_returntosupplierheader.intReturnYear
Inner Join mst_item ON ware_returntosupplierdetails.intItemId = mst_item.intId
Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
Inner Join ware_grndetails as tb1 ON ware_returntosupplierheader.intGrnNo = tb1.intGrnNo AND ware_returntosupplierheader.intGrnYear = tb1.intGrnYear AND ware_returntosupplierdetails.intItemId = tb1.intItemId  
				Inner Join ware_grnheader ON tb1.intGrnNo = ware_grnheader.intGrnNo AND tb1.intGrnYear = ware_grnheader.intGrnYear 
Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
WHERE 
/*mst_item.intStatus = '1' AND */
ware_returntosupplierdetails.intReturnNo =  '$retSupNo' AND
ware_returntosupplierdetails.intReturnYear =  '$year' 
Order by mst_maincategory.strName asc, mst_subcategory.strName asc, mst_item.strName asc";
				$result = $db->RunQuery($sql);
				$totAmm=0;
				while($row=mysqli_fetch_array($result))
				{
					$stockBalQty=round(getStockBalance($row['intItemId'],$row['intCompanyId']),4);
					$grnNo=$row['intGrnNo'];
					$grnYear=$row['intGrnYear'];
					$mainCatName=$row['maincat'];
					$subCatName=$row['subcat'];
					$mainCatId=$row['intMainCategory'];
					$subCatId=$row['intSubCategory'];
					$itemId=$row['intItemId'];
					$uom=$row['uom'];
					$itemName=$row['item'];
					$grnQty=round($row['dblGrnQty'], 4);
					$Qty=round($row['dblQty'],4);
					$returnedQty=round($row['dblRetunSupplierQty'],4);
					$balToReturn=round($grnQty-$returnedQty,4);
					if($stockBalQty>$balToReturn){
						$maxReturnQty=$balToReturn;
					}
					else{
						$maxReturnQty=$stockBalQty;
					}
					$allocatedQty=allocatedQty($itemId,$row['grnCompany'],$grnNo,$grnYear);
					$nonAllocatedQty=$row['dblGrnQty']-$allocatedQty;
					
					$totAmm+=$Qty;
			?>
<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF"><?php if($editMode==1){ ?><img class="delImg" src="images/del.png" width="15" height="15" /><?php } ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $maincatId; ?>"><?php echo $mainCatName; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $subCatId; ?>"><?php echo $subCatName; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $itemId; ?>" class="item"><?php echo $itemName; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $uom; ?>"><?php echo $uom; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $itemId; ?>"><?php echo $grnQty; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $nonAllocatedQty; ?>"><?php echo $nonAllocatedQty; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $returnedQty; ?>"><?php echo $returnedQty; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $stockBalQty; ?>"><?php echo $stockBalQty; ?></td>
			<td align="center" bgcolor="#FFFFFF" id=""><input  id="<?php echo $itemId; ?>" class="validate[required,custom[number],max[<?php echo $maxReturnQty; ?>]] calculateValue Qty" style="width:80px;text-align:center" type="text" value="<?php echo $Qty; ?>"<?php if($editMode==0){  ?> disabled="disabled"<?php } ?>/></td>
			<td align="center" bgcolor="#FFFFFF" id=""  style="display:none"><input  id="<?php echo $itemId; ?>" class="validate[required,custom[integer],max[<?php echo $Qty; ?>]] calculateValue" style="width:80px;text-align:center" type="text" value="0"/></td>
			</tr>            
            <?php
				}
			?>
          </table>
        </div></td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound">
        <a name="butNew"  class="button white medium" id="butNew">New</a>
         <?php if(($form_permision['add']||$form_permision['edit']) && $editMode==1){ ?>
         <a id="butSave" name="butSave"  class="button white medium">Save</a>
		 <?php } ?>
         <a id="butConfirm" name="butConfirm" style=" <?php if(($intStatus>1) and ($confirmatonMode==1)){  echo 'display:yes'; } else { ?>  <?php echo 'display:none';  } ?>" class="button white medium">Approve</a>
         <a id="butReport" name="butReport"  class="button white medium">Report</a>
         <a href="main.php"  class="button white medium">Close</a></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
  <script type="text/javascript" language="javascript">
  $('#txtTotAmnt').val('<?php echo $totAmm ?> ');
  </script>
</form>
	<!-- items to prn -->
	<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1">
   <!-- <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php" style="width:800;height:800;border:0;overflow:hidden">
    </iframe>-->
    </div>

	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>

 <?php
//------------------------------function load Header---------------------
function loadHeader($retSupNo,$year)
{
	global $db;
	 $sql = "SELECT
			ware_grnheader.strInvoiceNo,
			ware_returntosupplierheader.intGrnNo,
			ware_returntosupplierheader.intGrnYear,
			trn_poheader.intSupplier, 
			ware_returntosupplierheader.datdate ,
			ware_returntosupplierheader.intStatus, 
			ware_returntosupplierheader.intApproveLevels, 
			ware_returntosupplierheader.REMARKS, 
			mst_supplier.strName as supplier 
			FROM
			ware_returntosupplierheader
			Inner Join ware_grnheader ON ware_returntosupplierheader.intGrnNo = ware_grnheader.intGrnNo AND ware_returntosupplierheader.intGrnYear = ware_grnheader.intGrnYear
			Inner Join trn_podetails ON ware_grnheader.intPoNo = trn_podetails.intPONo AND ware_grnheader.intPoYear = trn_podetails.intPOYear
			Inner Join trn_poheader ON trn_podetails.intPONo = trn_poheader.intPONo AND trn_podetails.intPOYear = trn_poheader.intPOYear
			Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
			WHERE
			ware_returntosupplierheader.intReturnNo =  '$retSupNo' AND
			ware_returntosupplierheader.intReturnYear =  '$year' "; 
			 $result = $db->RunQuery($sql);
			 return $result;
}
//------------------------------function load Default Department---------------------
	function getStockBalance($item,$location)
	{
		global $db;
		 $sql = "SELECT
				Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$location' AND 
				ware_stocktransactions_bulk.strType <>  'ITEMCREATION+' 
				GROUP BY
				ware_stocktransactions_bulk.intItemId";

		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return (float)($row['stockBal']);	
	}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$editMode=0;
	  $sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
			 
	return $editMode;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	$sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
//----------------------------------------------------
	function allocatedQty($item,$location,$grnNo,$grnYear)
	{
		global $db;
		 $sql = "SELECT
				Sum(ware_stocktransactions.dblQty) AS stockBal
				FROM ware_stocktransactions
				WHERE
				ware_stocktransactions.intItemId =  '$item' AND
				ware_stocktransactions.intLocationId =  '$location' AND 
				ware_stocktransactions.strType =  'ALLOCATE' AND
				ware_stocktransactions.intGRNNo =  '$grnNo' AND
				ware_stocktransactions.intGRNYear =  '$grnYear'
				GROUP BY
				ware_stocktransactions.intItemId";

		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return (float)($row['stockBal']);	
	}
//----------------------------------------------------
?>

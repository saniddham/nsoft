<title>Color Room Wastage</title>
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../../css/button.css" rel="stylesheet" type="text/css" />

<form id="frmPopUp" name="frmPopUp" method="post">
  <div align="center">
    <div class="trans_layoutD">
      <div class="trans_text">Color Room Wastage</div>
      <table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0">
        <tr>
          <td width="16%" class="normalfnt">Serial No</td>
<td width="40%"><input id="txtSerialNo" name="txtSerialNo" type="text" style="width:100px;" disabled >
                  <input id="txtSerialYear" name="txtSerialYear" type="text" disabled style="width:50px;""></td>                   
            <td width="6%" class="normalfnt">Date</td>
                        <td width="23%"><input name="dtDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="txtbox" id="dtDate" style="width:120px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>

          </tr>
          <tr>
               <td width="16%" class="normalfnt">Ink</td>
            <td colspan="3"><select name="cboInk" id="cboInk" style="width:300px" class="chosen-select">
              <option>Waterbase/Pitch Black/Water Base</option>
            </select></td>
            </tr>
		</table>
</td>
      </tr>
      <tr>
        <td><table width="100%" class="bordered" id="tblItems">
  <tr>
    <th width="6%" align="center"><input id="chkAll" name="chkAll" type="checkbox" value=""></th>
  	<th width="27%">Order No</th>
    <th width="27%">Sales Order No</th>
    <th width="35%">Stock Bal(KG)</th>
    <th width="35%">Wastage(KG)</th>
  </tr>
  <tr>
    <td align="center"><input name="chkItem" id="chkItem" type="checkbox" value=""></td>
    <td align="center">200025/2014</td>
    <td align="center">S1</td>
    <td align="center">12</td>
    <td align="center"><input id="txtWastage" name="txtWastage" type="text" value="0.56" style="text-align:right"></td>
  </tr>
  <tr>
    <td align="center"><input name="chkItem" id="chkItem" type="checkbox" ></td>
     <td align="center">200025/2014</td>
     <td align="center">S2</td>
    <td align="center">11</td>
    <td align="center"><input id="txtWastage" name="txtWastage" type="text" value="0.23" style="text-align:right"></td>
  </tr>
  <tr>
    <td align="center"><input name="chkItem" id="chkItem" type="checkbox" ></td>
     <td align="center">200026/2014</td>
     <td align="center">S3</td>
    <td align="center">8</td>
    <td align="center"><input id="txtWastage" name="txtWastage" type="text" value="0.15" style="text-align:right"></td>
  </tr>
</table>
</td>
      </tr>
      <tr height="32">
          <td align="center"><a class="button white medium" id="butNew" name="butNew">New</a>
            <a class="button white medium" id="butSave" name="butSave">Save</a>
            <a class="button white medium" id="butConfirm" name="butConfirm">Approve</a><a class="button white medium" id="butReport" name="butReport">Report</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
      </tr>
    </table>
    </div>
  </div>
</form>


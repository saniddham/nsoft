<style type="text/css">
.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: left;
}
.tableBorder_allRound {
	border: 1px solid #CCCCCC;
	border-radius: 10px 10px 10px 10px;
}
.orderNo {
	font-family: Verdana;
	font-size: 14px;
	color: #000040;
	margin: 0px;
	font-weight: bold;
	text-align: center;
}
/*BEGIN - MAIN INTERFACE GRID DESIGN {*/
 table .bordered{
    *border-collapse: collapse; /* IE7 and lower */
    border-spacing: 0;
    width: 100%;    
}
.bordered {
	border: solid #328648 1px;
	border-radius: 6px;
	box-shadow: 0 1px 1px #4ABB67;
	font-size:11px;
	font-family: Verdana;
}  
.bordered td{
    border-left: 1px solid #328648;
    border-top: 1px solid #328648;
    padding: 2px;
}

.bordered th {
    border-left: 1px solid #328648;
    border-top: 1px solid #328648;
    padding: 4px;
    text-align: center;    
}

.bordered th {
    background-color: #4ABB67;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#72E38F), to(#4ABB67));
    background-image: -webkit-linear-gradient(top, #72E38F, #4ABB67);
    background-image:    -moz-linear-gradient(top, #72E38F, #4ABB67);
    background-image:     -ms-linear-gradient(top, #72E38F, #4ABB67);
    background-image:      -o-linear-gradient(top, #72E38F, #4ABB67);
    background-image:         linear-gradient(top, #72E38F, #4ABB67);

    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered td:first-child, .bordered th:first-child {
    border-left: none;
}

.bordered th:first-child {

    border-radius: 6px 0 0 0;
}

.bordered th:last-child {

    border-radius: 0 6px 0 0;
}

.bordered th:only-child{

    border-radius: 6px 6px 0 0;
}

.bordered tr:last-child td:first-child {

    border-radius: 0 0 0 6px;
}

.bordered tr:last-child td:last-child {

    border-radius: 0 0 6px 0;
}
/*END - MAIN INTERFACE GRID DESIGN }*/
/*BEGIN - MAIN INTERFACE GRID DESIGN2 {*/
 table .bordered2{
    *border-collapse: collapse; /* IE7 and lower */
    border-spacing: 0;
    width: 100%;    
}
.bordered2 {
	border: solid #316083 1px;
	border-radius: 6px;
	box-shadow: 0 1px 1px #4788BA;
	font-size:11px;
	font-family: Verdana;
}  
.bordered2 td{
    border-left: 1px solid #316083;
    border-top: 1px solid #316083;
    padding: 2px;
}

.bordered2 th {
    border-left: 1px solid #316083;
    border-top: 1px solid #316083;
    padding: 4px;
    text-align: center;    
}

.bordered2 th {
    background-color: #4788BA;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#79BAEC), to(#4788BA));
    background-image: -webkit-linear-gradient(top, #79BAEC, #4788BA);
    background-image:    -moz-linear-gradient(top, #79BAEC, #4788BA);
    background-image:     -ms-linear-gradient(top, #79BAEC, #4788BA);
    background-image:      -o-linear-gradient(top, #79BAEC, #4788BA);
    background-image:         linear-gradient(top, #79BAEC, #4788BA);

    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered2 td:first-child, .bordered2 th:first-child {
    border-left: none;
}

.bordered2 th:first-child {

    border-radius: 6px 0 0 0;
}

.bordered2 th:last-child {

    border-radius: 0 6px 0 0;
}

.bordered2 th:only-child{

    border-radius: 6px 6px 0 0;
}

.bordered2 tr:last-child td:first-child {

    border-radius: 0 0 0 6px;
}

.bordered2 tr:last-child td:last-child {

    border-radius: 0 0 6px 0;
}
/*END - MAIN INTERFACE GRID DESIGN }*/
</style>
<table  width="600" border="0" cellspacing="0" cellpadding="0" class="tableBorder_allRound" style="border-color:#025077">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0">
        <tr>
          <td width="150" class="normalfnt">Dear Nilu,</td>
          <td width="450">&nbsp;</td>
        </tr>
        <tr>
          <td width="150">&nbsp;</td>
          <td width="450" class="normalfnt">Following Items allocated to this order from bulk stock to style stock.</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0">
        <tr>
          <td width="50%"><table width="100%" border="0" class="tableBorder_allRound">
              <tr>
                <td height="32" width="33%" class="normalfnt">Order No</td>
                <td width="2%" class="normalfnt">:</td>
                <td width="65%" class="orderNo">200025</td>
              </tr>
            </table></td>
          <td width="50%"><table width="100%" border="0" class="tableBorder_allRound">
              <tr>
                <td height="32" width="42%" class="normalfnt">Customer PO No</td>
                <td width="1%" class="normalfnt">:</td>
                <td width="57%" class="orderNo">PO098987</td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr>
  	<td>
    <table width="100%" border="0">
    <tr>
    	<td>
        <table width="100%" border="0" class="bordered">
        <tr class="normalfnt">
            <th colspan="5" align="center"><b>Allocate</b></th>
          </tr>
        <tr class="normalfnt">
          <th width="17%" style="text-align:center">Sales Order</th>
            <th width="19%" style="text-align:center">Item Code</th>
            <th width="33%" style="text-align:center">Item Name</th>
            <th width="16%" style="text-align:center">Unit</th>
            <th width="15%" style="text-align:center">Weight/Qty</th>
        </tr>
        <tr class="normalfnt">
          <td width="17%" height="22" style="text-align:center">s1</td>
            <td width="19%" style="text-align:center">01540</td>
            <td width="33%" style="text-align:left">57-P BRILIANT REGAL RED</td>
            <td width="16%" style="text-align:center">KG</td>
            <td width="15%" style="text-align:right">10</td>
        </tr>
        <tr class="normalfnt">
          <td width="17%" style="text-align:center">s2</td>
            <td width="19%" style="text-align:center">098789</td>
            <td width="33%" style="text-align:left">57-P BRILIANT REGAL RED</td>
            <td width="16%" style="text-align:center">KG</td>
            <td width="15%" style="text-align:right">5</td>
        </tr>
        <tr class="normalfnt">
          <td width="17%" style="text-align:center">s3</td>
            <td width="19%" style="text-align:center">098789</td>
            <td width="33%" style="text-align:left">D-1-1 S (CLEAR)</td>
            <td width="16%" style="text-align:center">KG</td>
            <td width="15%" style="text-align:right">5</td>
        </tr>
        <tr class="normalfnt">
          <td width="17%" style="text-align:center">s4</td>
          <td width="19%" style="text-align:center">098789</td>
          <td width="33%" style="text-align:left">ART-20-2MM (ROSE)</td>
          <td width="16%" style="text-align:center">KG</td>
          <td width="15%" style="text-align:right">5</td>
        </tr>
        </table>
        </td>
    </tr>
    </table>
    </td>
  </tr>
  <tr>
  	<td>
    <table width="100%" border="0">
    <tr>
    	<td>
        	<table width="100%" border="0" class="bordered2">
        <tr class="normalfnt">
            <th colspan="4" align="center"><b>Unallocate</b></th>
          </tr>
        <tr class="normalfnt">
            <th width="22%" style="text-align:center">Item Code</th>
            <th width="41%" style="text-align:center">Item Name</th>
            <th width="20%" style="text-align:center">Unit</th>
            <th width="17%" style="text-align:center">Weight/Qty</th>
        </tr>
        <tr class="normalfnt">
            <td width="22%" style="text-align:center">01540</td>
            <td width="33%" style="text-align:left">57-P BRILIANT REGAL RED</td>
            <td width="20%" style="text-align:center">KG</td>
            <td width="17%" style="text-align:right">10</td>
        </tr>
        <tr class="normalfnt">
            <td width="22%" style="text-align:center">098789</td>
            <td width="33%" style="text-align:left">57-P BRILIANT REGAL RED</td>
            <td width="20%" style="text-align:center">KG</td>
            <td width="17%" style="text-align:right">5</td>
        </tr>
        <tr class="normalfnt">
            <td width="22%" style="text-align:center">098789</td>
            <td width="33%" style="text-align:left">D-1-1 S (CLEAR)</td>
            <td width="20%" style="text-align:center">KG</td>
            <td width="17%" style="text-align:right">5</td>
        </tr>
        <tr class="normalfnt">
          <td width="22%" height="18" style="text-align:center">098789</td>
          <td width="33%" style="text-align:left">ART-20-2MM (ROSE)</td>
          <td width="20%" style="text-align:center">KG</td>
          <td width="17%" style="text-align:right">5</td>
        </tr>
        </table>
        </td>
    </tr>
    </table>
    </td>
  </tr>
  <tr>
  	<td>
    <table width="100%" border="0">
        <tr>
          <td width="150" class="normalfnt">Thanks,</td>
          <td width="450">&nbsp;</td>
        </tr>
        <tr class="normalfnt">
          <td width="150"><strong>bbbb</strong><br />
    ...................</td>
          <td width="450" class="normalfnt">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2" class="normalfnt">(This is a <strong><span style="color:#025077">NSOFT</span> </strong>system generated email.)</td>
        </tr>
      </table>
    </td>
  </tr>
</table>

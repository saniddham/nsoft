<title>Actual Production</title>
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../../css/button.css" rel="stylesheet" type="text/css" />

<form id="frmPopUp" name="frmPopUp" method="post">
  <div align="center">
    <div class="trans_layoutD">
      <div class="trans_text">Actual Production</div>
      <table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0">
         <tr>
            <td width="17%"><a class="button red medium" id="butDayEnd" name="butDayEnd">Day End</a></td>
            <td width="38%" ></td>
            <td width="7%"></td>
            <td width="38%" align="center"></td>
          </tr>
        <tr class="normalfnt">
                <td width="17%">Serial No</td>
                <td><input name="txtReturnYear" type="text" disabled="disabled" id="txtReturnYear" style="width:60px" value="" />&nbsp;<input name="txtReturnNo" type="text" disabled="disabled" id="txtReturnNo" style="width:90px"value="" /></td>
                <td class="normalfnt">Date</td>
                <td width="38%"><input name="txtReturnNo2" type="text" id="txtReturnNo2" style="width:90px"value="" /></td>
              </tr>
          <tr>
            <td width="17%" class="normalfnt">Graphic</td>
            <td width="38%"><select name="cboGraphicNo" id="cboGraphicNo" style="width:200px" class="chosen-select">
                 <option>Graphic 1</option>
</select></td>
            <td width="7%" class="normalfnt">Group</td>
            <td width="38%"><select name="cboGroup" id="cboGroup" style="width:200px" class="chosen-select">
                 <option>Group A</option>
                  </select></td>
          </tr>
          <tr>
            <td width="17%">&nbsp;</td>
            <td width="38%" ></td>
            <td width="7%"></td>
            <td width="38%" align="right"><a class="button green medium" id="butAddItem" name="butAddItem">Add</a></td>
          </tr>
		</table>
</td>
      </tr>
      <tr>
        <td><table width="100%" class="bordered" id="tblItems">
  <tr>
    <th width="6%" align="center"><input id="chkAll" name="chkAll" type="checkbox" value=""></th>
  	<th width="27%">Order No</th>
    <th width="35%">Sales Order No</th>
    <th width="32%">Actual Production</th>
  </tr>
  <tr>
    <td align="center"><input name="chkItem" id="chkItem" type="checkbox" value=""></td>
    <td align="center">200025/2014</td>
    <td align="center">S1</td>
    <td align="center"><input id="txtActual" name="txtActual" type="text" value="850" style="text-align:right"></td>
  </tr>
  <tr>
    <td align="center"><input name="chkItem" id="chkItem" type="checkbox" ></td>
     <td align="center">200026/2014</td>
    <td align="center">S2</td>
    <td align="center"><input name="txtActual" type="text" value="600" style="text-align:right"></td>
  </tr>
  <tr>
    <td align="center"><input name="chkItem" id="chkItem" type="checkbox" ></td>
     <td align="center">200026/2014</td>
    <td align="center">S3</td>
    <td align="center"><input name="txtActual" type="text" value="930" style="text-align:right"></td>
  </tr>
</table>
</td>
      </tr>
      <tr>
        <td align="center"><a class="button white medium" id="butNew" name="butNew">New</a>
            <a class="button white medium" id="butSave" name="butSave">Save</a>
<a class="button white medium" id="butConfirm" name="butConfirm">Approve</a><a class="button white medium" id="butClose" name="butClose">Close</a></td>
      </tr>
    </table>
    </div>
  </div>
</form>


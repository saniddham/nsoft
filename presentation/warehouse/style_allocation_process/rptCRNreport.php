<?php //(define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$locationId					= $_SESSION["CompanyID"];
$companyId					= $_SESSION["headCompanyId"];
$session_userId 			= $_SESSION["userId"];
$thisFilePath 				= $_SERVER['PHP_SELF'];
?>
<head>
<title>Color Room Requisition Report</title>
<link rel="stylesheet" type="text/css" href="css/button.css"/>
<link rel="stylesheet" type="text/css" href="css/promt.css"/>
<script type="text/javascript" src="presentation/general/general_gate_pass_return/rptGatepassReturned-js.js"></script>
<style>
#apDiv1 {
	position: absolute;
	left: 468px;
	top: 181px;
	width: 650px;
	height: 322px;
	z-index: 1;
	visibility: hidden;
}
.APPROVE {
	font-size: 16px;
	font-weight:bold;
	font-family: "Arial Black", Gadget, sans-serif;
	color: #36F;
}
#apDiv2 {
	position: absolute;
	left: 407px;
	top: 240px;
	width: 294px;
	height: 113px;
	z-index: 1;
}
</style>
</head>

<body>
<form id="frmRptGatepassReturn" name="frmRptGatepassReturn" method="post" >
<table width="100%" align="center">
	<tr>
    	<td colspan="3"><?php include 'reportHeader.php'?></td>
    </tr>
   	<tr>
   	  <td colspan="3" style="text-align:center">&nbsp;</td></tr>
    <tr>
    <td colspan="3" style="text-align:center"><strong>Color Room Requisition Report</strong></td>
    </tr>
     <tr>
   <td bgcolor="#EAEAFF" align="center">
		            <a id="butRptConfirm" class="button white medium">Approve</a>
                     <a id="butRptReject" class="button white medium">Reject</a>
            </td></tr>
    <tr>
    	<td colspan="3">
            <table width="100%" border="0">
            	<tr class="normalfnt">
                	<td width="18%">CRN No</td>
                    <td width="2%">:</td>
                    <td width="29%">12122/2014</td>
                    <td width="20%">GatePass Return Date</td>
                    <td width="2%">:</td>
                    <td width="29%">2014-08-01</td>
                </tr>
                <tr class="normalfnt">
                	<td width="16%">Graphic No</td>
                    <td width="2%">:</td>
                    <td width="31%"><?php echo $header_array["GATEPASS_NO"]?></td>
                    <td width="18%">Order Nos</td>
                    <td width="2%">:</td>
                    <td width="31%">213232/2014,232231/2014</td>
                </tr>
            	
              </table>
              </td></tr>
        <tr>
          <td colspan="3">&nbsp;</td>
    	</tr>
   	    <tr>
    		<td colspan="3">
        	<table width="100%" border="0" class="rptBordered" id="tblMain">
                <thead> 
                <tr class="normalfnt">
                    <th width="5%">No.</th>
                    <th width="36%">Item</th>
                    <th width="14%">Size</th>
                    <th width="15%">Serial No</th>
                    <th width="8%">Unit</th>
                    <th width="10%">Qty</th>
                    <th width="12%">Returned Date</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $total = 0;
				$i	= 0;
                while($row = mysqli_fetch_array($detail_result))
                {
					$total += $row['QTY'];
                ?>
                <tr class="normalfnt">
                	<td style="text-align:center"><?php echo $i++; ?></td>
                    <td style="text-align:left"><?php echo $row['DESCRIPTION']; ?></td>
                    <td style="text-align:left"><?php echo ($row['SIZE']==''?'&nbsp;':$row['SIZE']); ?></td>
                    <td style="text-align:left"><?php echo ($row['SERIAL_NO']==''?'&nbsp;':$row['SERIAL_NO']); ?></td>
                    <td style="text-align:left"><?php echo $row['strCode']; ?></td>
                    <td style="text-align:right"><?php echo ($row['QTY']==''?'&nbsp;':$row['QTY']); ?></td>
                    <td style="text-align:center"><?php echo ($row['RETURN_DATE']==''?'&nbsp;':$row['RETURN_DATE']); ?></td>
                <?php
				}
				?>
                </tbody>
                </table>
             </table>
        </td>
    </tr>
    <tr>
    <td>
    <?php
			$creator		= $header_array['CREATOR'];
			$createdDate	= $header_array['CREATED_DATE'];
 			$resultA 		= $obj_gatePassReturn_get->get_Report_approval_details_result($gatePassReturnNo,$gatePassReturnYear,'RunQuery');
			include "presentation/report_approvedBy_details.php"
 	?>
    </td>
    </tr>
</table>
</form> 
</body>

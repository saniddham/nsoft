<?php //(define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<title>Ink Issue to Production</title>
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../../css/button.css" rel="stylesheet" type="text/css" />
<form id="frmCRN" name="frmCRN" method="post">
  <div align="center">
    <div class="trans_layoutD" style="width:750px">
      <div class="trans_text">Ink Issue to Production</div>
      <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
        <tr>
          <td><table width="100%" border="0" align="center" bgcolor="#FFFFFF">
              <tr>
                <td width="14%" class="normalfnt">Issue No</td>
                <td width="36%"><input id="txtSerialNo" name="txtSerialNo" type="text" style="width:100px;" disabled >
                  <input id="txtSerialYear" name="txtSerialYear" type="text" disabled style="width:50px;""></td>
                <td width="10%" class="normalfnt">Date</td>
                <td width="32%"><input name="dtDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="txtbox" id="dtDate" style="width:120px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/>
                  <input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
              </tr>
              <tr>
                <td class="normalfnt">Graphic No</td>
                <td><select name="cboGraphicNo" id="cboGraphicNo" style="width:200px" class="chosen-select">
               <option>Graphic 1</option>
                  </select></td>
                <td class="normalfnt">Orders</td>
                <td width="32%"><select name="cboOrders" id="cboOrders" style="width:200px" class="chosen-select">
               <option>200135/2014/ S 1</option>
                  </select></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td width="32%" style="text-align:right"><a class="button green medium" id="butAddItem" name="butAddItem">Search</a></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td><table  width="100%" class="bordered" id="tblInkItems" >
              <tr>
                <th width="9%">Technique</th>
                <th width="9%">Color</th>
                <th width="13%">Ink Type</th>
                <th width="13%">Sample Consumption</th>
                <th width="13%">Actual Consumption</th>
                <th width="10%">Issuable Weight(KG)</th>
                <th width="10%">Stock Bal(KG)</th>
                <th width="12%">Issued Weight(KG)</th>
                <th width="11%">Issuing Weight(KG)</th>
              </tr>
              <tr>
                <td style="text-align:center">Waterbase</td>
                <td align="center">Pitch Black</td>
                <td align="center">Water Base</td>
                <td align="center">0.100</td>
                <td align="center">0.108</td>
                <td align="center">10</td>
                <td align="center">2</td>
                <td align="center">3</td>
                <td align="center"><input type="text" name="txtExtra" id="txtExtra" value="2" style="text-align:right;width:70px"" /></td>
              </tr>
              <tr>
                <td style="text-align:center">Rhine stone</td>
                <td align="center">Black 1</td>
                <td align="center">Rhine stone transfer</td>
                <td align="center">0.112</td>
                <td align="center">0.118</td>
                <td align="center">9</td>
                <td align="center">1</td>
                <td align="center">2</td>
                <td align="center"><input type="text" name="txtExtra" id="txtExtra" value="3" style="text-align:right;width:70px"" /></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td align="center"><a class="button white medium" id="butNew" name="butNew">New</a>
            <?php if($form_permision['add']||$form_permision['edit']){ ?>
            <a class="button white medium" id="butSave" name="butSave">Save</a>
            <?php } ?>
            <a class="button white medium" id="butConfirm" name="butConfirm">Approve</a><a class="button white medium" id="butReport" name="butReport">Report</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
        </tr>
      </table>
    </div>
  </div>
</form>

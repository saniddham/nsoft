<title>Untitled Document</title>

<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../../css/button.css" rel="stylesheet" type="text/css" />

<form id="frmStockAllocate" name="frmStockAllocate" method="post">
<div align="center" style="width:1500px">
	<div class="trans_layoutXL">
    <div class="trans_text">Stock Allocate/Unallocate</div>
    <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <tr>
    	<td>
            <table width="100%" border="0">
            <tr>
            	<td width="10%" class="normalfnt">Order no</td>
                <td width="76%" class="normalfnt"><select name="cboOrderno" style="width:200px">
                <option>200045/2014</option>
                </select></td>
                <td width="5%" class="normalfnt">Date</td>
                <td width="9%" class="normalfnt" style="text-align:right"><input type="text" name="textfield" id="textfield" style="width:100px" value="2014-08-03" /></td>
            </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td>
            <table width="100%" border="0">
            <tr>
            	<td>
                <div style="overflow:scroll;overflow-x:hidden;width:100%;height:250px;">
                <table width="100%" border="0" class="bordered" id="tblMain">
                <thead>
                    <tr>
                        <th width="3%" rowspan="2"><input name="" type="checkbox" value="" /></th>
                        <th width="27%" rowspan="2">Item</th>
                        <th width="4%" rowspan="2">UOM</th>
                        <th width="7%" rowspan="2">Sample Con.</th>
                        <th width="9%" rowspan="2">Actual Con.</th>
                        <th width="5%" rowspan="2">25%</th>
                        <th width="5%" rowspan="2">100%</th>
                        <th width="5%" rowspan="2">Allocated </th>
                        <th width="5%" rowspan="2">Bulk Stock</th>
                        <th width="5%" rowspan="2">Style Stock</th>
                        <th colspan="2">Color Room Stock</th>
                        <th width="15%" rowspan="2">Adjust Qty/weight</th>
                    </tr>
                    <tr>
                        <th width="5%">None</th>
                        <th width="5%">Order</th>
                      </tr>
                </thead>
                <tbody>
                	<tr class="normalfnt">
                    	<td style="text-align:center"><input name="" type="checkbox" value="" /></td>
                    	<td width="27%" style="text-align:left">57-P BRILIANT REGAL RED</td>
                    	<td width="4%" style="text-align:center">KG</td>
                        <td style="text-align:right">0.1</td>
                        <td style="text-align:right">0.25</td>
                        <td style="text-align:right">250</td>
                        <td style="text-align:right">1000</td>
                        <td style="text-align:right">250</td>
                        <td style="text-align:right">10000</td>
                        <td style="text-align:right">5000</td>
                        <td style="text-align:right">4000</td>
                        <td style="text-align:right">3000</td>
                        <td style="text-align:center"><input name="txtAdjustQty" type="text" style="width:100%" /></td>
                    </tr>
                    <tr class="normalfnt">
                    	<td style="text-align:center"><input name="" type="checkbox" value="" /></td>
                    	<td width="27%" style="text-align:left">57-P BRILIANT REGAL RED</td>
                    	<td width="4%" style="text-align:center">KG</td>
                        <td style="text-align:right">0.1</td>
                        <td style="text-align:right">0.25</td>
                        <td style="text-align:right">25</td>
                        <td style="text-align:right">100</td>
                        <td style="text-align:right">25</td>
                        <td style="text-align:right">9000</td>
                        <td style="text-align:right">7000</td>
                        <td style="text-align:right">5000</td>
                        <td style="text-align:right">2000</td>
                        <td style="text-align:center"><input name="txtAdjustQty" type="text" style="width:100%" /></td>
                    </tr>
               <tr class="normalfnt">
                    	<td style="text-align:center"><input name="" type="checkbox" value="" /></td>
                    	<td width="27%" style="text-align:left">D-1-1 S (CLEAR)</td>
                    	<td width="4%" style="text-align:center">KG</td>
                        <td style="text-align:right">0.1</td>
                        <td style="text-align:right">0.25</td>
                        <td style="text-align:right">25</td>
                        <td style="text-align:right">100</td>
                        <td style="text-align:right">25</td>
                        <td style="text-align:right">9000</td>
                        <td style="text-align:right">7000</td>
                        <td style="text-align:right">5000</td>
                        <td style="text-align:right">2000</td>
                        <td style="text-align:center"><input name="txtAdjustQty" type="text" style="width:100%" /></td>
                    </tr>
                    <tr class="normalfnt">
                    	<td style="text-align:center"><input name="" type="checkbox" value="" /></td>
                    	<td width="27%" style="text-align:left">ART-20-2MM (ROSE)</td>
                    	<td width="4%" style="text-align:center">KG</td>
                        <td style="text-align:right">0.1</td>
                        <td style="text-align:right">0.25</td>
                        <td style="text-align:right">25</td>
                        <td style="text-align:right">100</td>
                        <td style="text-align:right">25</td>
                        <td style="text-align:right">9000</td>
                        <td style="text-align:right">7000</td>
                        <td style="text-align:right">5000</td>
                        <td style="text-align:right">2000</td>
                        <td style="text-align:center"><input name="txtAdjustQty" type="text" style="width:100%" /></td>
                    </tr> </tbody>
                </table>
                </div>
                </td>
            </tr>
            </table>
        </td>
    </tr>
    <tr height="32">
    	<td align="center"><a class="button white medium" id="butNew" name="butNew">New</a><a class="button red medium" id="butAllocate" name="butAllocate">Allocate</a><a class="button green medium" id="butUnAllocate" name="butUnAllocate">Unallocate</a><a class="button white medium" id="butReport" name="butReport">Report</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>  
    </tr>
    </table>
    </div>
</div>
</form>

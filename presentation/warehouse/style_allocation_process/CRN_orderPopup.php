<title>Order Selection</title>
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../../css/button.css" rel="stylesheet" type="text/css" />
<form id="frmPopUp" name="frmPopUp" method="post">
  <div align="center">
    <div class="trans_layoutS">
      <div class="trans_text">Order Selection</div>
      <table width="100%" border="0">
        <tr>
          <td><table width="100%" class="bordered" id="tblItems">
              <tr>
                <th width="8%" align="center"><input id="chkAll" name="chkAll" type="checkbox" value=""></th>
                <th width="29%">Order No</th>
                <th width="28%">Sales Order No</th>
                <th width="17%">Stock(KG)</th>
                <th width="18%">Weight(KG)</th>
              </tr>
              <tr>
                <td align="center"><input id="chkOrder" name="chkOrder" type="checkbox" value=""></td>
                <td align="center">200025/2014</td>
                <td align="center">S1</td>
                <td align="center">0.1</td>
                <td><input id="txtItemWeight" name="txtItemWeight" type="text" value="1.150" style="text-align:right" /></td>
              </tr>
               <tr>
                <td align="center"><input id="chkOrder" name="chkOrder" type="checkbox" value=""></td>
                <td align="center">200025/2014</td>
                <td align="center">S2</td>
                <td align="center">0.1</td>
                <td><input id="txtItemWeight" name="txtItemWeight" type="text" value="2.130" style="text-align:right" /></td>
              </tr>
               <tr>
                <td align="center"><input id="chkOrder" name="chkOrder" type="checkbox" value=""></td>
                <td align="center">200026/2014</td>
                <td align="center">S3</td>
                <td align="center">0.75</td>
                <td><input id="txtItemWeight" name="txtItemWeight" type="text" value="3.50" style="text-align:right" /></td>
              </tr>
               <tr>
                <td align="center"><input id="chkOrder" name="chkOrder" type="checkbox" value=""></td>
                <td align="center">200026/2014</td>
                <td align="center">S1</td>
                <td align="center">1.75</td>
                <td><input id="txtItemWeight" name="txtItemWeight" type="text" value="2.250" style="text-align:right" /></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td align="center"><a class="button white medium" id="butAdd" name="butAdd">Add</a><a class="button white medium" id="butClose" name="butClose">Close</a></td>
        </tr>
      </table>
    </div>
  </div>
</form>

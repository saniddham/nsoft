<?php //(define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<title>Ink Return from Production</title>

<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../../css/button.css" rel="stylesheet" type="text/css" />
<form id="frmCRN" name="frmCRN" method="post">
  <div align="center">
    <div class="trans_layoutD" style="width:700px">
      <div class="trans_text">Ink Return from Production</div>
      <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
        <tr>
          <td><table width="100%" border="0" align="center" bgcolor="#FFFFFF">
              
              <tr>
                <td class="normalfnt">Serial No</td>
                <td><input id="txtSerialNo" name="txtSerialNo" type="text" style="width:100px;" disabled >
                  <input id="txtSerialYear" name="txtSerialYear" type="text" disabled style="width:50px;""></td>
                <td class="normalfnt">Date</td>
                <td width="32%"><input name="dtDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="txtbox" id="dtDate" style="width:120px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/>
                  <input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
              </tr>
              <tr>
                <td class="normalfnt">Graphic No</td>
                <td><select name="cboGraphicNo" id="cboGraphicNo" style="width:200px" class="chosen-select">
                 <option>Graphic 1</option>
                  </select></td>
                <td class="normalfnt">Orders</td>
                <td width="32%"><select name="cboOrders" id="cboOrders" style="width:200px" class="chosen-select">
               <option>200135/2014/ S 1</option>
                  </select></td>
              </tr>
              <tr>
                <td width="14%" class="normalfnt">Issue No</td>
                <td width="36%"> <select name="cboIssueYear" id="cboIssueYear" style="width:70px" >
                 <option>2014</option>
                  </select><select name="cbossueNo" id="cbossueNo" style="width:130px" class="chosen-select">
                  <option>200005</option>              	
                   </select></td>
                <td width="10%" class="normalfnt"></td>
                <td width="32%"></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            
                <td width="32%" style="text-align:right"><a class="button green medium" id="butAddItem" name="butAddItem">Search</a></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td><table  width="100%" class="bordered" id="tblInkItems" >
              <tr>
              	<th width="4%">&nbsp;</th>
                <th width="11%">Technique</th>
                <th width="15%">Color</th>
                <th width="15%">Ink Type</th>
                <th width="5%">Issued (KG)</th>
                <th width="11%">Return Qty(KG)</th>
                <th width="12%">Wastage(KG)</th>
                <th width="24%">Remark</th>
              </tr>
              <tr>
                <td align="center"><input id="chkReturn" name="chkReturn" type="checkbox" value=""></td>
				<td style="text-align:center">Waterbase</td>
                <td align="center">Pitch Black</td>
                <td align="center">Water Base</td>                <td align="center">30</td>
                <td align="center"><input type="text" name="txtReturn" id="txtReturn" value="8" style="text-align:right;width:70px"" /></td>
                <td align="center"><input type="text" name="txtWastage" id="txtWastage" value="4" style="text-align:right;width:70px"" /></td>
                <td align="center"><textarea name="txtRemark" id="txtRemark" ></textarea></td>
              </tr>
              <tr>
              	<td align="center"><input id="chkReturn" name="chkReturn" type="checkbox" value=""></td>
                <td style="text-align:center">Rhine stone</td>
                <td align="center">Black 1</td>
                <td align="center">Rhine stone transfer</td>
                <td align="center">45</td>
                <td align="center"><input type="text" name="txtReturn" id="txtReturn" value="12" style="text-align:right;width:70px"" /></td>
                <td align="center"><input type="text" name="txtWastage" id="txtWastage" value="7" style="text-align:right;width:70px"" /></td>
                <td align="center"><textarea name="txtRemark" id="txtRemark" ></textarea></td>

              </tr>
            </table></td>
        </tr>
        <tr>
          <td align="center"><a class="button white medium" id="butNew" name="butNew">New</a>
            <a class="button white medium" id="butSave" name="butSave">Save</a>
            <a class="button white medium" id="butConfirm" name="butConfirm">Approve</a><a class="button white medium" id="butReport" name="butReport">Report</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
        </tr>
      </table>
    </div>
  </div>
</form>

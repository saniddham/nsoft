<?php
	session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ink Return From Production</title>

	<link href="css.css" rel="stylesheet" />
	<!-- jQuery & jQuery UI + theme (required) -->
	<link href="other/jquery-ui.css" rel="stylesheet">
	<script src="other/jquery.js"></script>
	<script src="other/jquery-ui.min.js"></script>

	<!-- keyboard widget css & script (required) -->
	<link href="css/keyboard.css" rel="stylesheet">
	<script src="js/jquery.keyboard.js"></script>

	<!-- keyboard extensions (optional) -->
	<script src="js/jquery.mousewheel.js"></script>
	<script src="js/jquery.keyboard.extension-typing.js"></script>
	<script src="js/jquery.keyboard.extension-autocomplete.js"></script>

	<!-- demo only -->
	<link href="demo/demo.css" rel="stylesheet">
	<script src="demo/demo.js"></script>
	<script src="other/jquery.jatt.min.js"></script> <!-- tooltips -->

	<!-- theme switcher -->
	<script src="other/themeswitchertool.htm"></script>

	<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>
    <script type="application/javascript" src="index.js"></script>
	<script>
		$(function(){
			$('#keyboard').keyboard();
		});
		var projectName = '/<?php echo $_SESSION["projectName"]; ?>' ;
	</script>
  
    <script type="application/javascript" src="colorRecipes-js.js"></script>
</head>

<body>
<table width="1386" height="199" border="0" cellpadding="0" cellspacing="0">

  <tr>
    <td><table width="1300">
    <!--home-->
  <tr>
    <td height="20"  class="label" >Return Ink From Production</td>
    <td width="102" class="label">&nbsp;</td>
    <td width="142" class="label">&nbsp;</td>
    <td width="116" class="label">&nbsp;</td>
    <td width="162">&nbsp;</td>
    <td width="108">&nbsp;</td>
    <td  width="91" align="right"><a id="butHome" href="logout.php" class="pink button logout" style="height:20px;margin-top:5px; width:65px">HOME</a></td>
    <td width="152" align="left"><a id="butSearch" href="logout.php" class="red button logout" style="height:20px;margin-top:5px; width:65px">LOG OUT</a></td>
    </tr>
  </table></td></tr>
  
  <tr>
    <td><table width="1300" class="tableBorder_allRound divPicture">
    <!--search-->
    <tr>
    <td width="1" class="label" >&nbsp;</td>
      <td width="154" height="48" class="label" >&nbsp;ORDER YEAR</td>
      <td width="183"><select name="cboOrderYear" class="down"   id="cboOrderYear" >
      	<?php
				    $d = date('Y');
					if($orderYear=='')
						$orderYear = $d;
				  	for($d;$d>=2012;$d--)
					{
						if($d==$orderYear)
							echo "<option selected=\"selected\" id=\"$d\" >$d</option>";	
						else
							echo "<option id=\"$d\" >$d</option>";	
					}
		?>
      </select></td>
      <td width="115" class="label">ORDER NO</td>
      <td width="149" class="label"><input type="text" style="height:30px;margin-top:5px; width:85px" class="txtGraphicNo" id="num2"   role="textbox" value="<?php echo $graphicNo; ?>" /></td>
      <td width="137" class="label">GRAPHIC NO</td>
      <td width="375"><input type="text" style="height:30px;margin-top:5px; width:155px" class="txtGraphicNo" id="num"   role="textbox" value="<?php echo $graphicNo; ?>" /></td>
      <td width="11">&nbsp;</td>
      <td width="107" align="left"><a id="butSearch" class="green button search" style="height:30px; width:65px;margin-top:5px">Search</a></td> 
     <td width="24"></td>
      </tr> 
  </table></td></tr>
     
  <tr>
    <td><table width="1300"  class="tableBorder_allRound divPicture">
    <!--graphic-->
    <tr height="20">
      <td colspan="11"></td>
    </tr>
     <!--end of graphic-->    
    <!--orders-->
    <tr height="20">
    <td colspan="11"></td>
    </tr>
     <tr>
     <td></td>
     <td colspan="9">
     <table width="100%">
         <tr>
         <td width="26%"><span class="label">GRAPHICS LIST</span></td>
         <td width="26%" align="left" class="label"><input name="" type="checkbox" value=""   style="width:10px" />ORDERS LIST</td>
         <td align="left" width="48%"><span class="label">
           <input name="input" type="checkbox" value=""   style="width:10px" />
ISSUE NUMBERS LIST</span></td>
         </tr>
         <tr>
         <td width="26%"><select name="cboGraphics" size="1" multiple="multiple" id="cboGraphics" style="height:210px;width:300px">
           <option>panda</option>
           <option>VV - 3858</option>
           <option>FN 2316</option>
           <option>Ready to roll sk 8r tee</option>
           <option>Pro SK 8R</option>
           <option>GLF12-192</option>
           <option>LT AW 12 019</option>
           <option>Cmal Stake Tee</option>
         </select></td>
         <td><table id="cboOrders" class="label" style="height:210px;width:300px" bgcolor="#999999">
                 <tr><td><input name="" type="checkbox" value=""   style="width:10px" /></td><td><font color="#CCFF33">200999/2014/S 1</font></td></tr>
                <tr><td><input name="" type="checkbox" value=""  style="width:10px" /></td><td><font color="#CCFF33">200999/2014/S 2</font></td></tr>
                <tr><td><input name="" type="checkbox" value=""  style="width:10px" /></td><td><font color="#CCFF33">200998/2014/S 1</font></td></tr>
                <tr><td><input name="" type="checkbox" value=""   style="width:10px"/></td><td><font color="#CCFF33">200997/2014/S 1</font></td></tr>
                <tr><td><input name="" type="checkbox" value=""   style="width:10px"/></td><td><font color="#CCFF33">200997/2014/S 2</font></td></tr>
                <tr><td><input name="" type="checkbox" value=""   style="width:10px"/></td><td><font color="#CCFF33">200997/2014/S 3</font></td></tr>
                <tr><td><input name="" type="checkbox" value=""   style="width:10px"/></td><td><font color="#CCFF33">200996/2014/S 1</font></td></tr>
                <tr><td><input name="" type="checkbox" value=""   style="width:10px"/></td><td><font color="#CCFF33">200995/2014/S 1</font></td></tr>
          </table>
         <td align="left" width="48%"><select name="cboGraphics2" size="1" multiple="multiple" id="cboGraphics2" style="height:210px;width:300px">
           <option>200034/2014</option>
           <option>200035/2014</option>
           <option>200036/2014</option>
           <option>200037/2014</option>
           <option>200038/2014</option>
           <option>200039/2014</option>
           <option>200040/2014</option>
           <option>200041/2014</option>
         </select></td>
         </tr>
     </table>
     </td>
     <td></td>
     </tr>
 <!--end of graphic-->    
 
     <!--issue numbers-->
    <tr height="20">
      <td colspan="11"></td>
    </tr>
     <!--end of issue numbers-->   
 <tr height="20"><td colspan="11"></td></tr>
 <tr>
 <td></td>
 <td colspan="9"><table id="tblInk" class="tableBorder_allRound" width="100%" cellspacing="1" cellpadding="0" border="0" style="background:#FFF;margin-top:5px">
<tbody>
<tr style="color:#FF0">
<td class="tableBorder_allRound" width="17%" bgcolor="#333333" align="center">COLOR</td>
<td class="tableBorder_allRound" width="15%" bgcolor="#333333" align="center">TECHNIQUE</td>
<td class="tableBorder_allRound" width="18%" bgcolor="#333333" align="center">INK TYPE</td>
<td class="tableBorder_allRound" width="9%" bgcolor="#333333" align="center">SAMPLE cONSUMPTION</td>
<td class="tableBorder_allRound" width="10%" bgcolor="#333333" align="center">ACTUAL CONSUMPTION</td>
<td class="tableBorder_allRound" width="9%" bgcolor="#333333" align="center">ISSUED</td>
<td class="tableBorder_allRound" width="7%" bgcolor="#333333" align="center">RETURNED</td>
<td class="tableBorder_allRound" width="8%" bgcolor="#333333" align="center">WEIGHT</td>
<td class="tableBorder_allRound" width="7%" bgcolor="#333333" align="center">WASTAGE</td>
<td class="tableBorder_allRound" width="7%" bgcolor="#333333" align="center">ORDERS</td>
</tr>
<tr>
<td id="6" class="color" bgcolor="#333333">Black</td>
<td id="43" class="technique" bgcolor="#333333">SHINY PLASTISOL</td>
<td id="15" class="inkType" bgcolor="#333333">Rubber Print</td>
<td class="normalfntMid" bgcolor="#333333" align="center">0.3</td>
<td class="normalfntMid" bgcolor="#333333" align="center">0.4</td>
<td class="normalfntMid" bgcolor="#333333" align="center">10</td>
<td class="normalfntMid" bgcolor="#333333" align="center">2</td>
<td class="normalfntMid" bgcolor="#333333" align="center"><input type="text" style="height:50px;margin-top:5px" class="hex txtShots" id="txtShots" aria-haspopup="true" role="textbox" value="<?php echo$row['intNoOfShots']; ?>" /></td>
<td class="normalfntMid" bgcolor="#333333" style="height:20	px"><input type="text" style="height:50px;margin-top:5px" class="hex txtShots" id="txtShots" aria-haspopup="true" role="textbox" value="<?php echo$row['intNoOfShots']; ?>" /></td>
<td class="normalfntMid" bgcolor="#333333" align="center"><a id="butSearch" class="green button search" style="height:30px; width:65px;margin-top:5px">ORDERS</a></td>
</tr>
<tr>
<td id="6" class="color" bgcolor="#333333">Black</td>
<td id="43" class="technique" bgcolor="#333333">SHINY PLASTISOL</td>
<td id="15" class="inkType" bgcolor="#333333">Hayleys shiny clear</td>
<td class="normalfntMid" bgcolor="#333333" align="center">0.32</td>
<td class="normalfntMid" bgcolor="#333333" align="center">0.41</td>
<td class="normalfntMid" bgcolor="#333333" align="center">10</td>
<td class="normalfntMid" bgcolor="#333333" align="center">1</td>
<td class="normalfntMid" bgcolor="#333333" align="center"><input type="text" style="height:50px;margin-top:5px" class="hex txtShots" id="txtShots" aria-haspopup="true" role="textbox" value="<?php echo$row['intNoOfShots']; ?>" /></td>
<td class="normalfntMid" bgcolor="#333333" style="height:20px"><input type="text" style="height:50px;margin-top:5px" class="hex txtShots" id="txtShots" aria-haspopup="true" role="textbox" value="<?php echo$row['intNoOfShots']; ?>" /></td>
<td class="normalfntMid" bgcolor="#333333" align="center"><a id="butSearch" class="green button search" style="height:30px; width:65px;margin-top:5px">ORDERS</a></td>
</tr>
<tr>
<td id="6" class="color" bgcolor="#333333">Black 1</td>
<td id="43" class="technique" bgcolor="#333333">Rhine stone</td>
<td id="15" class="inkType" bgcolor="#333333">Rhine stone transfer</td>
<td class="normalfntMid" bgcolor="#333333" align="center">0.38</td>
<td class="normalfntMid" bgcolor="#333333" align="center">0.45</td>
<td class="normalfntMid" bgcolor="#333333" align="center">12</td>
<td class="normalfntMid" bgcolor="#333333" align="center">0.5</td>
<td class="normalfntMid" bgcolor="#333333" align="center"><input type="text" style="height:50px;margin-top:5px" class="hex txtShots" id="txtShots" aria-haspopup="true" role="textbox" value="<?php echo$row['intNoOfShots']; ?>" /></td>
<td class="normalfntMid" bgcolor="#333333" style="height:20px"><input type="text" style="height:50px;margin-top:5px" class="hex txtShots" id="txtShots" aria-haspopup="true" role="textbox" value="<?php echo$row['intNoOfShots']; ?>" /></td>
<td class="normalfntMid" bgcolor="#333333" align="center"><a id="butSearch" class="green button search" style="height:30px; width:65px;margin-top:5px">ORDERS</a></td>
</tr>

</tbody>
</table></td>
 
 <td></td>
 </tr>
 <tr height="20"><td colspan="11"></td></tr>
  </table></td></tr>
 
  <tr>
    <td><table width="1300" class="tableBorder_allRound divPicture">
    <!--search-->
    <tr>
    <td align="center"><a id="butSearch" class="green button search" style="height:30px; width:65px;margin-top:5px">SAVE</a></td>
      </tr> 
  </table></td></tr>
 
   </table>
   
</body>
</html>

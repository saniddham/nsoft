// JavaScript Document
$(document).ready(function() {

$('.technique').change();
$('#butSearch').live('click',searchDetails);	
$('#cboStyleNo').change(function(){
		
	loadSampleNos();
	
	var intSampleNo = $('#cboSampleNo').val();
	//$('#frmSampleInfomations').get(0).reset();
	$('#cboSampleNo').val(intSampleNo);
	$('#divPicture').html('');
	return;
});
$('#cboSampleNo').change(function(){
		
		loadRevisionNo();
		
		var intSampleNo = $('#cboSampleNo').val();
		//$('#frmSampleInfomations').get(0).reset();
		$('#cboSampleNo').val(intSampleNo);
		$('#divPicture').html('');
		return;
});

$('#cboRevisionNo').change(function(){
	loadCombo();
		
});
$('#cboCombo').change(function(){
	loadPrint();
		
});

		//----------------------------
		$('.costPerInch').keyup(function(){
			calTotCostPrice(this);
		});
		//----------------------------
		$('.usage').keyup(function(){
			calTotCostPrice(this);
		});
		//----------------------------
		$('.technique').change(function(){
			loadItem(this);
		});
		//----------------------------
		$('.foil').change(function(){
			techniqueCalculations(this);
		});
		//----------------------------
		$('.printWidth').keyup(function(){
			techniqueCalculations(this);
		});
		//----------------------------
		$('.printHeight').keyup(function(){
			techniqueCalculations(this);
		});
		//----------------------------
		$('.qty').keyup(function(){
			techniqueCalculations(this);
		});
		//----------------------------
		$('.delImg').live('click',function(){
			$(this).parent().parent().remove();
		});
		//----------------------------

function loadRevisionNo()
{
	var url = "colorRecipes-db-get.php?requestType=loadRevisionNo&sampleNo="+$('#cboSampleNo').val()+"&sampleYear="+$('#cboYear').val();
	var obj = $.ajax({url:url,async:false});	
	$('#cboRevisionNo').html(obj.responseText);
	$('#cboCombo').html('');	
	$('#cboPrint').html('');	
}
function loadSampleNos()
{
	var url = "colorRecipes-db-get.php?requestType=loadSampleNo&styleNo="+URLEncode($('#cboStyleNo').val())+"&sampleYear="+$('#cboYear').val();
	var obj = $.ajax({url:url,async:false});	
	$('#cboSampleNo').html(obj.responseText);	
	$('#cboRevisionNo').html('');	
	$('#cboCombo').html('');	
	$('#cboPrint').html('');	
	
}
function loadCombo()
{
	var url = "colorRecipes-db-get.php?requestType=loadCombo&sampleNo="+($('.txtSampleNo').val())+"&sampleYear="+$('#cboSampleYear').val()+"&revNo="+$('#cboRevisionNo').val();
	var obj = $.ajax({url:url,async:false});	
	$('#cboCombo').html(obj.responseText);	
	$('#cboPrint').html('');
}
function loadPrint()
{
	var url = "colorRecipes-db-get.php?requestType=loadPrint&sampleNo="+($('.txtSampleNo').val())+"&sampleYear="+$('#cboSampleYear').val()+"&revNo="+$('#cboRevisionNo').val()+"&combo="+encodeURIComponent($('#cboCombo').val());
	var obj = $.ajax({url:url,async:false});	
	$('#cboPrint').html(obj.responseText);	
}
	
	
/*data load part*/
$('#cboPrint').change(function(){
	window.location.href = "index.php?sampleNo="+$('.txtSampleNo').val()		
						+'&sampleYear='+$('#cboSampleYear').val()	
						+'&revNo='+$('#cboRevisionNo').val()	
						+'&combo='+encodeURIComponent($('#cboCombo').val())	
						+'&printName='+$('#cboPrint').val()	
});

$('#butAddRow').live('click',function(){
	$(this).parent().parent().before('<tr>'+$(this).parent().parent().parent().find('tr:eq(0)').html()+'</tr>');
});
	
//----------------------------
	
//if ($('#frmSampleInfomations').validationEngine('validate'))   
//---------------------------------------------------------------------------
$('#butSave').live('click',function(){
	var arrRecipe = '';
	var arrFoil = '';
	var arrSpecial = '';
	var data='';
	var i =0;
	
	var sampleNo 		= 	$('#cboSampleNo').val();
	var sampleYear 		= 	$('#cboYear').val();
	var revisionNo 		= 	$('#cboRevisionNo').val();
	var combo 			=	$('#cboCombo').val();
	var printName 		=	$('#cboPrint').val();
	
	$('.cboItem').each(function(){
		var itemId 		= $(this).val();
		
		if(itemId!='')
		{
			var colorId 	= $(this).parent().parent().parent().parent().parent().parent().find('td:eq(1)').attr('id');
			var techniqueId = $(this).parent().parent().parent().parent().parent().parent().find('td:eq(2)').attr('id');
			var inkTypeId 	= $(this).parent().parent().parent().parent().parent().parent().find('td:eq(3)').attr('id');
			var weight 		= $(this).parent().parent().find('#txtWeight').val();
			arrRecipe +=(i++ == 0?'':',' )+'{"color":"'+colorId+'","techId":"'+techniqueId+'","inkTypeId":"'+inkTypeId+'","weight":"'+weight+'","sampleNo":"'+sampleNo+'","sampleYear":"'+sampleYear+'","revisionNo":"'+revisionNo+'","combo":"'+combo+'","printName":"'+printName+'","itemId":"'+itemId+'"}';
		}
	});
	
	//alert(arrRecipe);
		// SAVE AJAX PART //
		data  = 'arrRecipe=['+ arrRecipe +']';
		
		var url 	= "colorRecipes-db-set.php?requestType=saveDetails";
		$.ajax({
			url:url,
			data:data,
			async:false,
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmColorRecipies #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t=setTimeout("alertx()",1000);
						return;
					}
				},
			error:function(xhr,status){
					
					$('#frmColorRecipies #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
				}		
			});
   });
   //-----------------------------------------------------
$('#butApprove').live('click',function(){
	var arrInk = '';
	var data='';
	var i =0;
	
	var sampleNo 		= 	$('.txtSampleNo').val();
	var sampleYear 		= 	$('#cboSampleYear').val();
	var revisionNo 		= 	$('#txtRevNo').text();
	var combo 			=	$('#txtCombo').text();
	var printName 		=	$('#txtPrintName').text();
	
	data += 'sampleNo='+ sampleNo;
	data += '&sampleYear='+ sampleYear;
	data += '&revisionNo='+ revisionNo;
	data += '&combo='+ combo;
	data += '&printName='+ printName;

	$('.color').each(function(){
		
			var colorId 	= $(this).parent().find('.color').attr('id');
			var techniqueId = $(this).parent().find('.technique').attr('id');
			var inkTypeId 	= $(this).parent().find('.inkType').attr('id');
			arrInk +=(i++ == 0?'':',' )+'{"color":"'+colorId+'","techId":"'+techniqueId+'","inkTypeId":"'+inkTypeId+'","sampleNo":"'+sampleNo+'","sampleYear":"'+sampleYear+'","revisionNo":"'+revisionNo+'","combo":"'+combo+'","printName":"'+printName+'"}';
	});
	
	//alert(arrRecipe);
		// SAVE AJAX PART //
		data  += '&arrInk=['+ arrInk +']';
		
		var url 	= "colorRecipes-db-set.php?requestType=approveRecipForBulk";
		$.ajax({
			url:url,
			data:data,
			async:false,
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#msg_parent').html(json.msg);
					if(msg.type=='pass'){
						$('#but_parent').html('<a id="butCancel" class="green button " style="height:16px;margin:2px 2px 2px 2px;">Cancel Approval of Bulk recipies</a>');
					}
				},
			error:function(xhr,status){
					
					
				}		
			});
   });
   //-----------------------------------------------------
$('#butCancel').live('click',function(){
	var arrInk = '';
	var data='';
	var i =0;
	
	var sampleNo 		= 	$('.txtSampleNo').val();
	var sampleYear 		= 	$('#cboSampleYear').val();
	var revisionNo 		= 	$('#txtRevNo').text();
	var combo 			=	$('#txtCombo').text();
	var printName 		=	$('#txtPrintName').text();
	
	data += 'sampleNo='+ sampleNo;
	data += '&sampleYear='+ sampleYear;
	data += '&revisionNo='+ revisionNo;
	data += '&combo='+ combo;
	data += '&printName='+ printName;

		
		var url 	= "colorRecipes-db-set.php?requestType=cancelApprovedRecipForBulk";
		$.ajax({
			url:url,
			data:data,
			async:false,
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#msg_parent').html(json.msg);
					if(msg.type=='pass'){
						$('#but_parent').html('<a id="butApprove" class="green button " style="height:16px;margin:2px 2px 2px 2px;">Approval For Bulk</a>');
					}
				},
			error:function(xhr,status){
					
					
				}		
			});
   });
   
   //---------------------------------------------------
	$('#butNew').click(function(){
		window.location.href = 'colorRecipes.php';
	});

});

//----------------------------------------------------------------------
function calTotCostPrice(obj)
{
		var printWidth = document.getElementById('txtPrintWidth').value;
		var printHeight = document.getElementById('txtPrintHeight').value;
	 	var costPerInch = $(obj).parent().parent().find('.costPerInch').val();
	 	var usage = $(obj).parent().parent().find('.usage').val();
	//	var totCost=qty*costPerInch*usage/100;
		var totCost=printWidth*printHeight*costPerInch*usage/100;
		//alert(totCost);
		$(obj).parent().parent().find('.totCost').text(totCost);
	
}
//----------------------------------------------------------------------
function loadItem(obj)
{
	 	var technique = $(obj).parent().parent().find('.technique').val();
	 	var sampleNo =$('#cboSampleNo').val();
	 	var sampleYear = $('#cboYear').val();
	 	var revNo = $('#cboRevisionNo').val();
	 	var combo = $('#cboCombo').val();
	 	var printName = $('#cboPrint').val();
		var url 		= "colorRecipes-db-get.php?requestType=loadItem";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"technique="+technique+"&sampleNo="+sampleNo+"&revNo="+revNo+"&combo="+combo+"&sampleYear="+sampleYear+"&printName="+printName,
			async:false,
			success:function(json){
				var itemId=json.itemId;
				 $(obj).parent().parent().find('.foil').val(json.itemId)
				 //	if(itemId){	
					  $(obj).parent().parent().find('.foil').change();
				//	}
			}
		});
}
//----------------------------------------------------------------------
function techniqueCalculations(obj)
{
	 	var foil = $(obj).parent().parent().find('.foil').val();
		var url 		= "colorRecipes-db-get.php?requestType=loadMeasurements";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"foil="+foil,
			async:false,
			success:function(json){
				foilWidth=json.width;
				foilHeight=json.height;
				foilcostPerMtr=json.price;
			}
		});
	
		var printWidth = $(obj).parent().parent().find('.printWidth').val();
		var printHeight = $(obj).parent().parent().find('.printHeight').val();
	 	var qty = $(obj).parent().parent().find('.qty').val();
		
		//------------------------------
		var pcs=Math.floor((foilHeight/printWidth))
		if(printWidth==0){
			pcs=0;
		}
		//alert(printWidth);
		$(obj).parent().parent().find('.pcsFrmHeight').text((foilHeight/printWidth).toFixed(4));
		$(obj).parent().parent().find('.pcs').text(pcs);
		var mtrs=((printHeight/pcs)*qty)/39.5;
		if(pcs==0){
			mtrs=0;
		}
		$(obj).parent().parent().find('.meters').text(mtrs.toFixed(4));
		$(obj).parent().parent().find('.cost').text((mtrs*foilcostPerMtr).toFixed(4));
		
		//-------------------
		var pcs=Math.floor((foilHeight/printHeight));
		if(printWidth==0){
			pcs=0;
		}
		$(obj).parent().parent().next().find('.pcsFrmHeight').text((foilHeight/printHeight).toFixed(4));
		$(obj).parent().parent().next().find('.pcs').text(pcs);
		var mtrs=((printWidth/pcs)*qty)/39.5;
		if(pcs==0){
			mtrs=0;
		}
		$(obj).parent().parent().next().find('.meters').text(mtrs.toFixed(4));
		$(obj).parent().parent().next().find('.cost').text((mtrs*foilcostPerMtr).toFixed(4));
}
//----------------------------------------------------------------------
function alertx()
{
	$('#frmColorRecipies #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmColorRecipies #butDelete').validationEngine('hide')	;
}

function searchDetails()
{
	var sampleYear 	= $('#cboSampleYear').val();
	var sampleNo 	= $('#num').val();
	document.location.href = 'index.php?sampleYear='+sampleYear+'&sampleNo='+sampleNo;
}
//----------------------------------------------

<?php
	session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Ink Return From Production</title>

	<link href="css.css" rel="stylesheet" />
	<!-- jQuery & jQuery UI + theme (required) -->
	<link href="other/jquery-ui.css" rel="stylesheet">
	<script src="other/jquery.js"></script>
	<script src="other/jquery-ui.min.js"></script>

	<!-- keyboard widget css & script (required) -->
	<link href="css/keyboard.css" rel="stylesheet">
	<script src="js/jquery.keyboard.js"></script>

	<!-- keyboard extensions (optional) -->
	<script src="js/jquery.mousewheel.js"></script>
	<script src="js/jquery.keyboard.extension-typing.js"></script>
	<script src="js/jquery.keyboard.extension-autocomplete.js"></script>

	<!-- demo only -->
	<link href="demo/demo.css" rel="stylesheet">
	<script src="demo/demo.js"></script>
	<script src="other/jquery.jatt.min.js"></script> <!-- tooltips -->

	<!-- theme switcher -->
	<script src="other/themeswitchertool.htm"></script>

	<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>
    <script type="application/javascript" src="index.js"></script>
	<script>
		$(function(){
			$('#keyboard').keyboard();
		});
		var projectName = '/<?php echo $_SESSION["projectName"]; ?>' ;
	</script>
  
    <script type="application/javascript" src="colorRecipes-js.js"></script>
</head>

<body>
<table width="900" height="199" border="0" cellpadding="0" cellspacing="0">

     <!--end of issue numbers-->   
 <tr height="20"><td colspan="11"></td></tr>
 <tr>
 <td></td>
 <td colspan="9"><table id="tblInk" class="tableBorder_allRound" width="100%" cellspacing="1" cellpadding="0" border="0" style="background:#FFF;margin-top:5px">
<tbody>
<tr style="color:#FF0">
<td class="tableBorder_allRound" width="22%" bgcolor="#333333" align="center">ORDER</td>
<td class="tableBorder_allRound" width="17%" bgcolor="#333333" align="center">SALES ORDER</td>
<td class="tableBorder_allRound" width="31%" bgcolor="#333333" align="center">WEIGHT</td>
<td class="tableBorder_allRound" width="30%" bgcolor="#333333" align="center">WASTAGE</td>
</tr>
<tr>
<td id="43" class="technique" bgcolor="#333333">200045/2014</td>
<td id="15" class="inkType" bgcolor="#333333">S 1</td>
<td class="normalfntMid" bgcolor="#333333" align="center"><input type="text" style="height:50px;margin-top:5px" class="hex txtShots" id="txtShots" aria-haspopup="true" role="textbox" value="<?php echo$row['intNoOfShots']; ?>" /></td>
<td class="normalfntMid" bgcolor="#333333" style="height:20	px"><input type="text" style="height:50px;margin-top:5px" class="hex txtShots" id="txtShots" aria-haspopup="true" role="textbox" value="<?php echo$row['intNoOfShots']; ?>" /></td>
</tr>
<tr>
<td id="43" class="technique" bgcolor="#333333">200045/2014</td>
<td id="15" class="inkType" bgcolor="#333333">S 2</td>
<td class="normalfntMid" bgcolor="#333333" align="center"><input type="text" style="height:50px;margin-top:5px" class="hex txtShots" id="txtShots" aria-haspopup="true" role="textbox" value="<?php echo$row['intNoOfShots']; ?>" /></td>
<td class="normalfntMid" bgcolor="#333333" style="height:20px"><input type="text" style="height:50px;margin-top:5px" class="hex txtShots" id="txtShots" aria-haspopup="true" role="textbox" value="<?php echo$row['intNoOfShots']; ?>" /></td>
</tr>
<tr>
<td id="43" class="technique" bgcolor="#333333">200046/2014</td>
<td id="15" class="inkType" bgcolor="#333333">S 1</td>
<td class="normalfntMid" bgcolor="#333333" align="center"><input type="text" style="height:50px;margin-top:5px" class="hex txtShots" id="txtShots" aria-haspopup="true" role="textbox" value="<?php echo$row['intNoOfShots']; ?>" /></td>
<td class="normalfntMid" bgcolor="#333333" style="height:20px"><input type="text" style="height:50px;margin-top:5px" class="hex txtShots" id="txtShots" aria-haspopup="true" role="textbox" value="<?php echo$row['intNoOfShots']; ?>" /></td>
</tr>

</tbody>
</table></td>
 
 <td></td>
 </tr>
 <tr height="20"><td colspan="11"></td></tr>
  </table></td></tr>
 
 
     <td colspan="9"><table width="900" class="tableBorder_allRound divPicture">
    <!--search-->
    <tr>
    <td align="center"><a id="butSearch" class="green button search" style="height:30px; width:65px;margin-top:5px">ADD</a><a id="butSearch" class="pink button search" style="height:30px; width:65px;margin-top:5px">CLOSE</a></td>
      </tr> 
  </table></td></tr>

 
   </table>
   
</body>
</html>

<title>Item Selection</title>
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../../css/button.css" rel="stylesheet" type="text/css" />

<form id="frmPopUp" name="frmPopUp" method="post">
  <div align="center">
    <div class="trans_layoutS">
      <div class="trans_text">Item Selection</div>
      <table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0">
          <tr>
            <td width="22%">&nbsp;</td>
            <td width="11%" class="normalfnt">Item</td>
            <td width="63%"><select name="cboGraphicNo" id="cboGraphicNo" style="width:200px" class="chosen-select">
                  </select></td>
            <td width="4%">&nbsp;</td>
          </tr>
		</table>
</td>
      </tr>
      <tr>
        <td><table width="100%" class="bordered" id="tblItems">
  <tr>
    <th width="9%" align="center"><input id="chkAll" name="chkAll" type="checkbox" value=""></th>
  	<th width="40%">Item Code</th>
    <th width="51%">Item</th>
  </tr>
  <tr>
    <td align="center"><input name="chkItem" id="chkItem" type="checkbox" value=""></td>
    <td align="center">A-1 MC 1100 O/RED</td>
    <td align="center">A-1 MC 1100 O/RED</td>
  </tr>
  <tr>
    <td align="center"><input name="chkItem" id="chkItem" type="checkbox" value=""></td>
    <td align="center">MAGNA ACTIVATOR M</td>
    <td align="center">A-1 MC 1100 O/RED</td>
  </tr>
  <tr>
    <td align="center"><input name="chkItem" id="chkItem" type="checkbox" value=""></td>
    <td align="center">MAGNAPRINT ECO BLUE HG</td>
    <td align="center">A-1 MC 1100 O/RED</td>
  </tr>
</table>
</td>
      </tr>
      <tr>
        <td align="center"><a class="button white medium" id="butAdd" name="butAdd">Add</a><a class="button white medium" id="butClose" name="butClose">Close</a></td>
      </tr>
    </table>
    </div>
  </div>
</form>


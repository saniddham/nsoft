<title>Return to Stock</title>

<link href="css/css.css" rel="stylesheet" />
<link href="../../../../css/mainstyle.css" rel="stylesheet" />
<!-- jQuery & jQuery UI + theme (required) -->
<link href="other/jquery-ui.css" rel="stylesheet">
<script src="other/jquery.js"></script>
<script src="other/jquery-ui.min.js"></script>

<!-- keyboard widget css & script (required) -->
<link href="css/keyboard.css" rel="stylesheet">
<script src="js/jquery.keyboard.js"></script>

<!-- keyboard extensions (optional) -->
<script src="js/jquery.mousewheel.js"></script>
<script src="js/jquery.keyboard.extension-typing.js"></script>
<script src="js/jquery.keyboard.extension-autocomplete.js"></script>

<!-- demo only -->
<link href="demo/demo.css" rel="stylesheet">
<script src="demo/demo.js"></script>
<script src="other/jquery.jatt.min.js"></script> <!-- tooltips -->

<!-- theme switcher -->
<script src="other/themeswitchertool.htm"></script>

<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>
<script>
    $(function(){
        $('#keyboard').keyboard();
    });
</script>
<body>
<table width="1024" height="110" border="0" cellpadding="0" cellspacing="0">
    <tr>
    	<td>
            <table width="100%" border="0">
            <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
            <tr>
         <td width="50%" height="30" class="normalfnt" style="background:#A40;color:#FFF;font-size:18px;border-radius:10px 10px 10px 10px;text-align:center">ITEM RETURN TO STOCK</td>
                <td width="38%" align="right" ><a id="butSearch" href="../touch_main_form.php" class="white button logout" style="height:20px;margin-top:5px; width:65px">HOME</a>
                <a id="butSearch" href="logout.php" class="red button logout" style="height:20px;margin-top:5px; width:65px">LOG OUT</a></td>
            </tr>
            </table>
        </td>
  </tr>
  <tr><td>&nbsp;</td></tr>
  <tr style="display:none">
  	<td>
    <table width="100%" border="0" style="background-color:#000;border-radius:8px 8px 8px 8px;">
        <tr class="normalfnt" style="color:#FFF">
            <td width="13%" style="text-align:right">2014-08-05 10:00:00</td>
            <td width="74%" class="label" style="text-align:center">Return to Stock</td>
            <td width="13%" style="text-align:left">Admin</td>
        </tr>
    </table>
    </td>
  </tr>
    <tr>
        <td>
            <table width="100%" border="0">
            <tr>
            	<td width="15%" height="48" class="label">ITEM NAME</td>
            	<td width="23%" class="label"><input type="text" style="height:50px;margin-top:5px;width:200px" class="txtGraphicNo" id="num" role="textbox" value="" /></td>
            	<td width="62%" style="text-align:left"><a id="butSearch" class="orange button search" style="height:35px;margin-top:5px">Search</a></td>
            </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td>&nbsp;</td>
    </tr>
    <tr>
        <td>
            <table width="100%" border="0">
            <tr>
            	<td width="24%">&nbsp;</td>
                <td width="52%" align="center">
                <div style="overflow:auto;width:520px;height:250px;overflow-x:hidden">
                <table align="center" style="background:#FFF;margin-top:5px;width:500px" border="0" cellspacing="1" cellpadding="0">
                <thead>
                	<tr style="color:#FF0">
                    	<td align="center" bgcolor="#333333"><input style="height:30px;width:30px" name="" type="checkbox" value=""></td>
                        <td align="center" bgcolor="#333333" style="width:180px">ITEM CODE</td>
                        <td align="center" bgcolor="#333333" style="width:290px">ITEM NAME</td>
                    </tr>
                </thead>
                <tbody>
                	<tr>
                    	<td style="text-align:center" bgcolor="#333333"><input style="height:30px;width:30px" name="" type="checkbox" value=""></td>
                        <td style="text-align:left" bgcolor="#333333">Item Code 1</td>
                        <td style="text-align:left" bgcolor="#333333">Item Name 1</td>
                    </tr>
                    <tr>
                    	<td style="text-align:center" bgcolor="#333333"><input style="height:30px;width:30px" name="" type="checkbox" value=""></td>
                        <td style="text-align:left" bgcolor="#333333">Item Code 2</td>
                        <td style="text-align:left" bgcolor="#333333">Item Name 2</td>
                    </tr>
                    <tr>
                    	<td style="text-align:center" bgcolor="#333333"><input style="height:30px;width:30px" name="" type="checkbox" value=""></td>
                        <td style="text-align:left" bgcolor="#333333">Item Code 3</td>
                        <td style="text-align:left" bgcolor="#333333">Item Name 3</td>
                    </tr>
                </tbody>
                </table>
                </div>
                </td>
                <td width="24%">&nbsp;</td>
            </tr>
            </table>
        </td>
    </tr>
    <tr>
    	<td>&nbsp;</td>
    </tr>
    <tr>
    	<td>
        	<table border="0" align="left" cellpadding="0" cellspacing="1" style="background:#FFF;margin-top:5px;width:900px">
            <thead>
            	<tr style="color:#FF0">
                	<td width="119" align="center" bgcolor="#333333">REMOVE</td>
                    <td width="366" align="center" bgcolor="#333333">ITEM NAME</td>
                    <td width="118" align="center" bgcolor="#333333">UOM</td>
                    <td width="164" align="center" bgcolor="#333333">STOCK BALANCE</td>
                    <td width="127" align="center" bgcolor="#333333">RETURN QTY</td>
                </tr>
            </thead>
            <tbody>
            	<tr>
                	<td align="center" bgcolor="#333333"><a id="butSearch" class="red button logout" style="height:20px;margin-top:3px;margin-bottom:3px;width:50px">REMOVE</a></td>
                    <td style="text-align:left" bgcolor="#333333">Item Name 1</td>
                    <td style="text-align:center" bgcolor="#333333">KG</td>
                    <td style="text-align:right" bgcolor="#333333">200</td>
                    <td style="text-align:center" bgcolor="#333333"><input type="text" style="height:30px;margin-top:3px;margin-bottom:3px;width:110px;text-align:right" class="txtGraphicNo" id="num" role="textbox" value="" /></td>
                </tr>
                <tr>
                	<td align="center" bgcolor="#333333"><a id="butSearch" class="red button logout" style="height:20px;margin-top:3px;margin-bottom:3px;width:50px">REMOVE</a></td>
                    <td style="text-align:left" bgcolor="#333333">Item Name 2</td>
                    <td style="text-align:center" bgcolor="#333333">KG</td>
                    <td style="text-align:right" bgcolor="#333333">100</td>
                    <td style="text-align:center" bgcolor="#333333"><input type="text" style="height:30px;margin-top:3px;margin-bottom:3px;width:110px" class="txtGraphicNo" id="num" role="textbox" value="" /></td>
                </tr>
                <tr>
                	<td align="center" bgcolor="#333333"><a id="butSearch" class="red button logout" style="height:20px;margin-top:3px;margin-bottom:3px;width:50px">REMOVE</a></td>
                    <td style="text-align:left" bgcolor="#333333">Item Name 3</td>
                    <td style="text-align:center" bgcolor="#333333">KG</td>
                    <td style="text-align:right" bgcolor="#333333">150</td>
                    <td style="text-align:center" bgcolor="#333333"><input type="text" style="height:30px;margin-top:3px;margin-bottom:3px;width:110px" class="txtGraphicNo" id="num" role="textbox" value="" /></td>
                </tr>
            </tbody>
            </table>
        </td>
    </tr>
    <tr>
    	<td style="text-align:center"><a id="butSearch" class="green button logout" style="height:20px;margin-top:10px;width:50px">SAVE</a></td>
    </tr>
</table>
</body>

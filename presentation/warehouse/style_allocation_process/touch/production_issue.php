
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Color Room Item Allocations</title>

	<link href="css/css.css" rel="stylesheet" />
	<!-- jQuery & jQuery UI + theme (required) -->
	<link href="other/jquery-ui.css" rel="stylesheet">
	<script src="other/jquery.js"></script>
	<script src="other/jquery-ui.min.js"></script>

	<!-- keyboard widget css & script (required) -->
	<link href="css/keyboard.css" rel="stylesheet">
	<script src="js/jquery.keyboard.js"></script>

	<!-- keyboard extensions (optional) -->
	<script src="js/jquery.mousewheel.js"></script>
	<script src="js/jquery.keyboard.extension-typing.js"></script>
	<script src="js/jquery.keyboard.extension-autocomplete.js"></script>

	<!-- demo only -->
	<link href="demo/demo.css" rel="stylesheet">
	<script src="demo/demo.js"></script>
	<script src="other/jquery.jatt.min.js"></script> <!-- tooltips -->

	<!-- theme switcher -->
	<script src="other/themeswitchertool.htm"></script>

	<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>
    <script type="application/javascript" src="index.js"></script>
	<script>
		$(function(){
			$('#keyboard').keyboard();
		});
		var projectName = '/<?php echo $_SESSION["projectName"]; ?>' ;
	</script>
  
    <script type="application/javascript" src="bulkStockAllocation_inkType-js.js"></script>
</head>

<body>
<table width="1024" height="111" border="0" cellpadding="0" cellspacing="0">
    <tr>
      <td height="24"><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="25%">Ink Issuing to Production</td>
          <td width="49%">&nbsp;</td>
          <td width="13%" style="text-align:right"><a id="butSearch" href="logout.php" class="white button logout" style="height:25px;margin-top:5px; width:65px">Home</a></td>
          <td width="13%" style="text-align:right"><a id="butSearch" href="logout.php" class="red button logout" style="height:25px;margin-top:5px; width:65px">Log Out</a></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="13%"><span class="label">ORDER YEAR</span></td>
          <td width="13%"><select name="cboOrderYear" class="down"   id="cboOrderYear" >
            <?php
				    $d = date('Y');
					if($orderYear=='')
						$orderYear = $d;
				  	for($d;$d>=2012;$d--)
					{
						if($d==$orderYear)
							echo "<option selected=\"selected\" id=\"$d\" >$d</option>";	
						else
							echo "<option id=\"$d\" >$d</option>";	
					}
		?>
          </select></td>
          <td width="12%"><span class="label">GRAPHIC NO</span></td>
          <td width="17%"><input type="text" style="height:50px;margin-top:5px; width:155px" class="txtGraphicNo" id="num"   role="textbox" value="<?php echo $graphicNo; ?>" /></td>
          <td width="11%"><span class="label">ORDER NO</span></td>
          <td width="20%"><input type="text" style="height:50px;margin-top:5px; width:155px" class="txtGraphicNo" id="num2"   role="textbox" value="<?php echo $graphicNo; ?>" /></td>
          <td width="14%" style="text-align:right"><a id="butSearch" class="orange button search" style="height:35px;margin-top:5px">Search</a></td>
        </tr>
      </table></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td width="33%" valign="top"><table width="100%" border="0" cellspacing="1" cellpadding="0" style="background:#FFF">
            <tr style="color:#FF0">
              <td bgcolor="#333333" style="text-align:center" height="30" >ORDER NO / SALES ORDER NO</td>
              </tr>
            <tr>
              <td height="30" bgcolor="#333333" ><select name="cboGraphicNo" size="10" multiple="multiple" id="cboGraphicNo" style="height:200px;width:100%">
              <option value="1">Order No 1 / Sales Order 1</option>
              <option value="2">Order No 2 / Sales Order 2</option>
              <option value="3">Order No 3 / Sales Order 3</option>
              <option value="4">Order No 4 / Sales Order 4</option>
              <option value="5">Order No 5 / Sales Order 5</option>
              </select></td>
              </tr>
          </table></td>
          <td width="3%">&nbsp;</td>
          <td><div style="overflow:auto;width:100%;height:230px;overflow-x:hidden"><table width="100%" border="0" cellspacing="1" cellpadding="0" style="background:#FFF">
            <tr style="color:#FF0">
              <td bgcolor="#333333" width="6%" style="text-align:center" height="25"><input type="checkbox" style="height:30px;width:30px" class="txtGraphicNo" id="num9"   role="checkbox" /></td>
              <td bgcolor="#333333" width="24%" style="text-align:center" height="25">TECHNIQUE</td>
              <td bgcolor="#333333" width="24%" style="text-align:center" height="25">COLOR</td>
              <td bgcolor="#333333" width="24%" style="text-align:center" height="25">INK TYPE</td>
              <td bgcolor="#333333" width="22%" style="text-align:center" height="25">ISSUABLE WEIGHT</td>
              </tr>
            <tr>
              <td bgcolor="#333333" height="25"><input type="checkbox" style="height:30px;width:30px" class="txtGraphicNo" id="num10"   role="checkbox" checked="checked"/></td>
              <td bgcolor="#333333" height="25">Technique 1</td>
              <td bgcolor="#333333" align="left" >Color 1</td>
              <td bgcolor="#333333" align="left" >Ink Type 1</td>
              <td bgcolor="#333333" align="right" >100</td>
              </tr>
            <tr>
              <td bgcolor="#333333" height="25"><span style="text-align:center">
                <input type="checkbox" style="height:30px;width:30px" class="txtGraphicNo" id="num11"   role="checkbox" checked="checked"/>
                </span></td>
              <td bgcolor="#333333" height="25">Technique 2</td>
              <td bgcolor="#333333" align="left" height="25">Color 2</td>
              <td bgcolor="#333333" align="left" >Ink Type 2</td>
              <td bgcolor="#333333" align="right" >100</td>
              </tr>
            <tr>
              <td bgcolor="#333333" height="25"><span style="text-align:center">
                <input type="checkbox" style="height:30px;width:30px" class="txtGraphicNo" id="num12"   role="checkbox" />
                </span></td>
              <td bgcolor="#333333" height="25">Technique 3</td>
              <td bgcolor="#333333" align="left" height="25">Color 3</td>
              <td bgcolor="#333333" align="left" >Ink Type 3</td>
              <td bgcolor="#333333" align="right" >100</td>
              </tr>
            <tr>
              <td bgcolor="#333333" height="25"><span style="text-align:center">
                <input type="checkbox" style="height:30px;width:30px" class="txtGraphicNo" id="num13"   role="checkbox" />
                </span></td>
              <td bgcolor="#333333" height="25">Technique 4</td>
              <td bgcolor="#333333" align="left" height="25">Color 4</td>
              <td bgcolor="#333333" align="left" >Ink Type 4</td>
              <td bgcolor="#333333" align="right" >100</td>
              </tr>
            <tr>
              <td bgcolor="#333333" height="25"><span style="text-align:center">
                <input type="checkbox" style="height:30px;width:30px" class="txtGraphicNo" id="num14"   role="checkbox" />
                </span></td>
              <td bgcolor="#333333" height="25">Technique 5</td>
              <td bgcolor="#333333" align="left" height="25">Color 5</td>
              <td bgcolor="#333333" align="left" >Ink Type 5</td>
              <td bgcolor="#333333" align="right" >100</td>
              </tr>
            <tr>
              <td bgcolor="#333333" height="25"><span style="text-align:center">
                <input type="checkbox" style="height:30px;width:30px" class="txtGraphicNo" id="num14"   role="checkbox" />
                </span></td>
              <td bgcolor="#333333" height="25">Technique 5</td>
              <td bgcolor="#333333" align="left" height="25">Color 5</td>
              <td bgcolor="#333333" align="left" >Ink Type 5</td>
              <td bgcolor="#333333" align="right" >100</td>
              </tr>
          </table></div></td>
        </tr>
        
      </table></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td><table style="background:#FFF;margin-top:5px" width="100%" border="0" cellspacing="1" cellpadding="0" id="tblParent">
        <tr style="color:#FF0">
          <td width="6%" align="center" bgcolor="#333333">REMOVE</td>
          <td width="20%" align="center" bgcolor="#333333">ORDER NO</td>
          <td width="12%" align="center" bgcolor="#333333">TECHNIQUE</td>
          <td width="12%" align="center" bgcolor="#333333">COLOR</td>
          <td width="12%" align="center" bgcolor="#333333">INK TYPE</td>
          <td width="8%" align="center" bgcolor="#333333">SAMPLE CON.</td>
          <td width="8%" align="center" bgcolor="#333333">ACTUAL CON.</td>
          <td width="7%" align="center" bgcolor="#333333">STOCK BAL</td>
          <td width="7%" align="center" bgcolor="#333333">ISSUED WEIGHT</td>
          <td width="8%" align="center" bgcolor="#333333">WEIGHT</td>
        </tr>        
        <tr>
          <td bgcolor="#333333" style="text-align:center"><a  id="butAdd" class="red button small" style="height:35px;vertical-align:central">Remove</a></td>
          <td bgcolor="#333333" style="text-align:left">Order No 1/S1</td>
          <td bgcolor="#333333">Technique 1</td>
          <td bgcolor="#333333" style="text-align:left">Color 1</td>
          <td bgcolor="#333333" style="text-align:left">Ink Type 1</td>
          <td bgcolor="#333333" style="text-align:right">0.0254</td>
          <td bgcolor="#333333" style="text-align:right">0.0324</td>
          <td bgcolor="#333333" style="text-align:right">100</td>
          <td bgcolor="#333333" style="text-align:right">50</td>
          <td bgcolor="#333333" style="text-align:center"><input type="text" style="height:30px;margin-top:3px;margin-bottom:3px;width:60px;text-align:right" class="txtGraphicNo" id="num" role="textbox" value="" /></td>
        </tr>
        <tr>
          <td bgcolor="#333333" style="text-align:center"><a  id="butAdd" class="red button small" style="height:35px;vertical-align:central">Remove</a></td>
          <td bgcolor="#333333" style="text-align:left">Order No 2/S2</td>
          <td bgcolor="#333333">Technique 2</td>
          <td bgcolor="#333333" style="text-align:left">Color 2</td>
          <td bgcolor="#333333" style="text-align:left">Ink Type 2</td>
          <td bgcolor="#333333" style="text-align:right">0.0554</td>
          <td bgcolor="#333333" style="text-align:right">0.0424</td>
          <td bgcolor="#333333" style="text-align:right">500</td>
          <td bgcolor="#333333" style="text-align:right">30</td>
          <td bgcolor="#333333" style="text-align:center"><input type="text" style="height:30px;margin-top:3px;margin-bottom:3px;width:60px;text-align:right" class="txtGraphicNo" id="num" role="textbox" value="" /></td>
        </tr>
      </table></td>
    </tr>
    <tr>
    	<td style="text-align:center"><a id="butSearch" class="green button logout" style="height:20px;margin-top:10px;width:50px">SAVE</a></td>
    </tr>
  </table>
			
		</div>
		<p>
		  
</p>
	
</body>
</html>
<title>Return to Stock</title>

<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../../css/button.css" rel="stylesheet" type="text/css" />

<form id="frmPopUp" name="frmPopUp" method="post">
  <div align="center">
    <div class="trans_layoutL">
      <div class="trans_text">Color Room Return to Stock</div>
      <table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0">
         <tr class="normalfnt">
            <td colspan="2">Serial No</td>
            <td width="57%"><input id="txtSerialYear" name="txtSerialYear" type="text" value="2014" disabled style="width:50px;"">&nbsp;<input id="txtSerialNo" name="txtSerialNo" type="text" style="width:100px;" value="200045" disabled ></td>                   
            <td width="12%">Date</td>
                        <td width="17%" style="text-align:right"><input value="2014-08-03" name="dtDate" type="text"  class="txtbox" id="dtDate" style="width:120px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
              </tr>
		</table>
</td>
      </tr>
      <tr><td><table  width="100%" class="bordered" id="tblInkItems" >
                <tr>
                	<th colspan="5"><div style="float:right"><a class="button white small" id="butInsertRow">Add New Item</a></div></th>
                </tr>
              <tr>
              	<th width="3%">&nbsp;</th>
                <th width="38%">Item</th>
                <th width="10%">Issued Qty</th>
                <th width="12%">Return Qty</th>
                <th width="37%">Remark</th>
              </tr>
              <tr>
                <td align="center"><img class="mouseover delImg" src="../../../images/del.png" width="15" height="15" /></td>
                <td style="text-align:left">CARBOLIC ACID</td>
                <td style="text-align:right">30</td>
                <td align="center"><input type="text" name="txtReturn" id="txtReturn" value="8" style="text-align:right;width:90px" /></td>
                <td align="center"><textarea rows="1" name="txtRemark" id="txtRemark" style="width:100%" ></textarea></td>
              </tr>
              <tr>
              	<td align="center"><img class="mouseover delImg" src="../../../images/del.png" width="15" height="15" /></td>
                <td style="text-align:left">LANKA SOL</td>
                <td style="text-align:right">45</td>
                <td align="center"><input type="text" name="txtReturn" id="txtReturn" value="12" style="text-align:right;width:90px" /></td>
                <td align="center"><textarea rows="1" name="txtRemark" id="txtRemark" style="width:100%" ></textarea></td>

              </tr>
            </table></td></tr>
      <tr height="32">
        <td align="center"><a class="button white medium" id="butNew" name="butNew">New</a><a class="button white medium" id="butSave" name="butSave">Save</a><a class="button white medium" id="butConfirm" name="butConfirm">Approve</a><a class="button white medium" id="butReport" name="butReport">Report</a><a class="button white medium" id="butClose" name="butClose">Close</a></td>
      </tr>
    </table>
    </div>
  </div>
</form>


<title>Untitled Document</title>

<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../../css/button.css" rel="stylesheet" type="text/css" />

<form id="frmStockAllocate" name="frmStockAllocate" method="post">
<div align="center" style="width:950px">
	<div class="trans_layoutXL">
    <div class="trans_text">Materials Issue Note</div>
    <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
<tr>
        <td><table width="900px" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="9%" height="27" class="normalfnt">Issue No</td>
            <td width="39%"><input id="txtMrnNo" class="txtText" type="text" value="" style="width:60px" disabled="disabled" name="txtMrnNo">
<input id="txtYear" class="txtText" type="text" value="" style="width:40px" disabled="disabled" name="txtYear"></td>
            <td width="31%">&nbsp;</td>
            <td width="6%" class="normalfnt">Date</td>
            <td width="15%"><input id="dtDate" class="txtbox" type="text" onclick="return showCalendar(this.id, '%Y-%m-%d');" onkeypress="return ControlableKeyAccess(event);" onmouseout="EnableRightClickEvent();" onmousedown="DisableRightClickEvent();" style="width:120px;" value="2014-08-03" name="dtDate">
<input class="txtbox" type="reset" onclick="return showCalendar(this.id, '%Y-%m-%');" style="visibility:hidden;" value=""></td>
          </tr>
        </table></td>
      </tr>      <tr>
        <td><table width="900px" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="10%" height="27" class="normalfnt">Department</td>
            <td width="39%"><select name="cboDepartment" id="cboDepartment" style="width:250px"  class="validate[required] txtText" >
                  <option value=""></option>
                  <option value="1" selected="selected">IT</option><option value="2">Marketing</option><option value="38">Canteen 02</option><option value="43">Administration</option>            </select></td>
            <td width="25%">&nbsp;</td>
            <td width="11%" class="normalfnt">&nbsp;</td>
            <td width="15%">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table width="900px" border="0" cellpadding="0" cellspacing="0">
          <tr>
<td width="10%" height="27" rowspan="2" class="normalfnt">Note</td>
            <td width="41%" rowspan="2"><textarea name="txtRemarks" id="txtRemarks" cols="35" rows="3" ></textarea></td>
            <td width="27%" rowspan="2"><div id="divPrice" style="display:none"></div></td>
            <td width="10%" rowspan="2" valign="top" class="normalfnt">&nbsp;</td>
            <td width="12%" valign="bottom"><img  src="../../../images/Tadd.jpg" width="92" height="24" id="butAddItems" name="butAddItems" /></td>
          </tr>
        </table></td>
      </tr>
       <tr> <td><div style="width:900px;height:200px;overflow:scroll" >
          <table width="100%" class="bordered" id="tblMrn" >
            <tr>
              <th width="4%" height="22" >Del</th>
              <th width="10%" >Order No</th>
              <th width="11%" >Sales Order  No</th>
              <th width="13%" >Main Category</th>
              <th width="12%" >Sub Category</th>
              <th width="26%" >Item Description</th>
              <th width="9%">UOM</th>
              <th width="8%">MRN Bal Qty</th>
              <th width="7%"> Qty</th>
              </tr>
              <tr>
              <td><img src="http://127.0.0.1/nsoft-3.0/images/del.png" alt="" width="15" height="15"></td>
              <td>200025/2014</td>
              <td>s1</td>
              <td>RAW MATERIA</td>
              <td>Foil</td>
              <td>D- 1 - S -SILVER</td>
              <td>m</td>
              <td>100</td>
              <td><input id="100" class="validate[required,custom[number],max[100]] calculateValue Qty" type="text" value="100" style="width:80px;text-align:center"></td>
              </tr>
              <tr>
              <td><img src="http://127.0.0.1/nsoft-3.0/images/del.png" alt="" width="15" height="15"></td>
              <td>200025/2014</td>
              <td>s2</td>
              <td>RAW MATERIA</td>
              <td>Foil</td>
              <td>D- 1 - S -SILVER</td>
              <td>m</td>
              <td>200</td>
              <td><input id="1002" class="validate[required,custom[number],max[100]] calculateValue Qty" type="text" value="100" style="width:80px;text-align:center" /></td>
              </tr>
              <tr>
              <td><img src="http://127.0.0.1/nsoft-3.0/images/del.png" alt="" width="15" height="15"></td>
              <td>200026/2014</td>
              <td>s1</td>
              <td>RAW MATERIAL</td>
              <td>RHINE STONE</td>
              <td>ART-20-2MM (ROSE)</td>
              <td>kg</td>
              <td>100</td>
              <td><input id="1003" class="validate[required,custom[number],max[100]] calculateValue Qty" type="text" value="100" style="width:80px;text-align:center" /></td>
              </tr>
              <tr>
              <td><img src="http://127.0.0.1/nsoft-3.0/images/del.png" alt="" width="15" height="15"></td>
              <td>200026/2014</td>
              <td>s2</td>
              <td>RAW MATERIAL</td>
              <td>GLITTER</td>
              <td>K-12 (1/170 ) - BLACK</td>
              <td>kg</td>
              <td>350</td>
              <td><input id="1004" class="validate[required,custom[number],max[100]] calculateValue Qty" type="text" value="100" style="width:80px;text-align:center" /></td>
              </tr>
              
              
                      </table>
        </div></td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound">
        <a name="butNew"  class="button white medium" id="butNew">New</a>
                  <a id="butSave" name="butSave"  class="button white medium">Save</a>
		          <a id="butConfirm" name="butConfirm" style="   display:none" class="button white medium">Approve</a>
         <a id="butReport" name="butReport"  class="button white medium">Report</a>
         <a href="main.php"  class="button white medium">Close</a></td>
      </tr>
    </table></td>
    </tr>
  </table>
    </table>
    </div>
</div>
</form>

<title>Color Room Process</title>
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../../css/button.css" rel="stylesheet" type="text/css" />
<form id="frmPopUp" name="frmPopUp" method="post">
  <div align="center">
    <div class="trans_layoutD" style="width:700px">
      <div class="trans_text">Color Room Ink Allocation Process</div>
      <table width="100%" border="0">
      <tr>
        <td>
  <fieldset>
  <legend class="normalfnt">Color Room Main Form </legend>
  <table width="100%" border="0" >
 <tr><td align="center"> <fieldset>
  <table>
  <tr>
    <td align="center"><a class="button green medium" id="butProcess" name="butProcess">CRN</a></td>
   </tr>
   <tr>
    <td align="center" class="normalfnt">Color Room Requisition Note</td>  
  </tr>
  </table>
  </fieldset></td>
  <td align="center"> <fieldset>
  <table>
  <tr>
    <td align="center"><a class="button green medium" id="butProcess" name="butProcess">Ink Issue</a></td>
  </tr>
   <tr>
    <td align="center" class="normalfnt">Ink Issue to Production</td>
  </tr>
  </table>
  </fieldset></td>
    <td align="center"> <fieldset>
  <table>
  <tr>
    <td width="161" align="center"><a class="button green medium" id="butProcess" name="butProcess">Ink Return</a></td>
  </tr>
   <tr>
    <td align="center" class="normalfnt">Ink Return to Color Room</td>
  </tr>
  </table>
  </fieldset></td>
  
  </tr>
  
   <tr>
   <td align="center"> <fieldset>
  <table>
  <tr>
    <td align="center"><a class="button green medium" id="butProcess" name="butProcess">Ink Wastage</a></td>
   </tr>
   <tr>
    <td align="center" class="normalfnt">Ink Wastage of Color Room</td>  
  </tr>
  </table>
  </fieldset></td>
    
    <td align="center" class="normalfnt">&nbsp;</td>
    <td align="center"><fieldset>
  <table>
  <tr>
    <td align="center"><a class="button green medium" id="butProcess" name="butProcess">Ink Wastage</a></td>
   </tr>
   <tr>
    <td align="center" class="normalfnt">Color Room Day End Process</td>  
  </tr>
  </table>
  </fieldset></td>
    <td align="center" class="normalfnt">&nbsp;</td>
  
  </tr>
</table>
   </fieldset>

</td>
      </tr>
      <tr>
        <td align="center"><a class="button white medium" id="butClose" name="butClose">Close</a></td>
      </tr>
    </table>
    </div>
  </div>
</form>


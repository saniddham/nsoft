<?php
session_start();

$locationId					= $_SESSION["CompanyID"];
$companyId					= $_SESSION["headCompanyId"];
$session_userId 			= $_SESSION["userId"];
$thisFilePath 				= $_SERVER['PHP_SELF'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Production Day End Confirmation Report</title>
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="css/button.css"/>
<link rel="stylesheet" type="text/css" href="css/promt.css"/>

<!--
<link rel="stylesheet" type="text/css" href="../../../css/mainstyle.css"/>
<link rel="stylesheet" type="text/css" href="../../../css/button.css"/>
<link rel="stylesheet" type="text/css" href="../../../css/promt.css"/>

<link rel="stylesheet" href="../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/script.js"></script>

<script src="../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.min.js"></script>-->

<style>
#apDiv1 {
	position: absolute;
	left: 468px;
	top: 181px;
	width: 650px;
	height: 322px;
	z-index: 1;
	visibility: hidden;
}
.APPROVE {
	font-size: 16px;
	font-weight:bold;
	font-family: "Arial Black", Gadget, sans-serif;
	color: #36F;
}
#apDiv2 {
	position: absolute;
	left: 407px;
	top: 240px;
	width: 294px;
	height: 113px;
	z-index: 1;
}
</style>
</head>

<body>
<form id="frmRptGatepassReturn" name="frmRptGatepassReturn" method="post" >
<table width="500" align="center">
	<tr><td><table width="100%" align="center">
    <tr>
    	<td colspan="3"><?php include 'reportHeader.php'?></td>
    </tr>
   	<tr>
   	  <td colspan="3" style="text-align:center">&nbsp;</td></tr>
    <tr>
    <td colspan="3" style="text-align:center"><strong>Color Room Return Items Report</strong></td>
    </tr>
    <tr><td>&nbsp;</td></tr>
  
            <td colspan="3" align="center"></td>
    	</tr>
   	    <tr>
    		<td colspan="3">
        	<table width="80%" align="center" border="0" class="rptBordered" id="tblMain">
                <thead> 
                <tr class="normalfnt">
                    <th width="12%">No.</th>
                    <th width="30%">Item</th>
                    <th width="31%">UOM</th>
                    <th width="27%">Return Qty</th>
             
                </tr>
                </thead>
                <tbody>
                <tr class="normalfnt">
                	<td style="text-align:center">1</td>
                    <td style="text-align:left">Item 1</td>
                    <td style="text-align:center">kg</td>
                    <td style="text-align:right">4</td>
               </tr>
                <tr class="normalfnt">
                	<td style="text-align:center">2</td>
                    <td style="text-align:left">Item 2</td>
                    <td style="text-align:center">kg</td>
                    <td style="text-align:right">10</td>
               </tr>
                <tr class="normalfnt">
                	<td style="text-align:center">3</td>
                    <td style="text-align:left">Item 3</td>
                    <td style="text-align:center">kg</td>
                    <td style="text-align:right">60</td>
               </tr>
                <tr class="normalfnt">
                	<td style="text-align:center">4</td>
                    <td style="text-align:left">Item 4</td>
                    <td style="text-align:center">kg</td>
                    <td style="text-align:right">12</td>
               </tr>
                
                </tbody>
                </table></td></tr>
                
             </table>
        </td>
    </tr>
    <tr><td>&nbsp;</td></tr>
    <tr>
    <td>
    <table width="100%" cellspacing="0" cellpadding="0" border="0">
                  <tbody>
                <tr>
                    <td bgcolor="#FFFFFF">&nbsp;</td>
                </tr>
				</tbody></table>
    </td>
    </td>
    </tr>
</table>
</form> 
</body>
</html>
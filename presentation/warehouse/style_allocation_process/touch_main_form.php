<title>Color Room Process</title>
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../../css/button.css" rel="stylesheet" type="text/css" />
<form id="frmPopUp" name="frmPopUp" method="post">
  <div align="center">
    <div style="width:auto;height:600px; background:#000;padding-right:15px;
	padding-top:10px;
	padding-left:30px;
	padding-right:30px;">
      <table width="100%" border="0">
        <tr>
        <td align="right"><a class="button red medium" style="font-size:18px;" id="butClose" name="butClose">Log out</a></td>
      </tr>
      <tr>
        <td>
  <fieldset>
  <legend class="normalfnt" style="color:#FFF;font-size:18px">Color Room Main Form </legend>
  <table width="100%" style="height:500px"  border="0" >
 <tr><td align="center"> <fieldset>
  <table>
  <tr>
    <td align="center"><a class="button" id="butProcess" name="butProcess" style="font-size:18px;background:#C10000;color:#FFF">Ink Creation</a></td>
   </tr>
   <tr>
    <td align="center" class="normalfntMid" style="color:#FFF;font-size:18px">Color Room Ink Creation</td>  
  </tr>
  </table>
  </fieldset></td>
  <td align="center"> <fieldset>
  <table width="283">
  <tr>
    <td width="146" align="center"><a class="button" id="butProcess" name="butProcess" style="font-size:18px;background:#FF8080;color:#FFF">CRN</a></td>
  </tr>
   <tr>
    <td align="center" class="normalfntMid" style="color:#FFF;font-size:18px">Color Room Requisition Note</td>
  </tr>
  </table>
  </fieldset></td>
    <td align="center"> <fieldset>
  <table width="247">
  <tr>
    <td width="161" align="center"><a href="touch/return_to_stock.php" class="button" id="butProcess" name="butProcess" style="font-size:18px;background:#A40;color:#FFF"">Item Return</a></td>
  </tr>
   <tr>
    <td align="center" class="normalfntMid" style="color:#FFF;font-size:18px">Items Return to Stock</td>
  </tr>
  </table>
  </fieldset></td>
  
  </tr>
  
   <tr>
   <td align="center"></td>
    
    <td align="center" ><fieldset>
  <table>
  <tr>
    <td align="center"><a href="touch_process.php" class="button" id="butProcess" name="butProcess" style="font-size:18px;background:#936;color:#FFF">Process</a></td>
   </tr>
   <tr>
    <td align="center" class="normalfntMid" style="color:#FFF;font-size:18px">Color Room Day End Process</td>  
  </tr>
  </table>
  </fieldset></td>
    <td align="center"></td>
  
  </tr>
  <tr>
   <td align="center"> <fieldset>
  <table>
  <tr>
    <td align="center"><a class="button" id="butProcess" name="butProcess" style="font-size:18px;background:#564729;color:#FFF">Ink Issue</a></td>
   </tr>
   <tr>
    <td align="center" class="normalfntMid" style="color:#FFF;font-size:18px">Ink Issue to Production</td>  
  </tr>
  </table>
  </fieldset></td>
    
    <td align="center"><fieldset>
  <table>
  <tr>
    <td align="center"><a class="button" id="butProcess" name="butProcess" style="font-size:18px;background:#F0CF13;color:#FFF">Ink Return</a></td>
   </tr>
   <tr>
    <td align="center" class="normalfntMid" style="color:#FFF;font-size:18px">Ink Return to Color Room</td>  
  </tr>
  </table>
  </fieldset></td>
    <td align="center"><fieldset>
  <table>
  <tr>
    <td align="center"><a href="touch_inkWastage.php" class="button" id="butProcess" name="butProcess" style="font-size:18px;background:#06C;color:#FFF">Ink Wastage</a></td>
   </tr>
   <tr>
    <td align="center" class="normalfntMid" style="color:#FFF;font-size:18px">Ink Wastage of Color Room</td>  
  </tr>
  </table>
  </fieldset></td>
  
  </tr>
</table>
   </fieldset>

</td>
      </tr>
      
    </table>
    </div>
  </div>
</form>


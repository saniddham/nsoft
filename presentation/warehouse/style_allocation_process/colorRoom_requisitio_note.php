<?php //(define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<title>Color Room Requisition Note</title>
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../../css/button.css" rel="stylesheet" type="text/css" />
<form id="frmCRN" name="frmCRN" method="post">
  <div align="center">
    <div class="trans_layoutD" style="width:700px">
      <div class="trans_text">Color Room Requisition Note</div>
      <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
        <tr>
          <td><table width="100%" border="0" align="center" bgcolor="#FFFFFF">
              <tr>
                <td width="3%" height="24">&nbsp;</td>
                <td width="14%" class="normalfnt">Serial No</td>
                <td width="36%"><input id="txtSerialNo" name="txtSerialNo" type="text" style="width:100px;">
                  <input id="txtSerialYear" name="txtSerialYear" type="text" style="width:50px;""></td>
                <td width="10%" class="normalfnt">Date</td>
                <td width="32%"><input name="dtDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="txtbox" id="dtDate" style="width:120px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/>
                  <input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                <td width="5%">&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td class="normalfnt">Graphic No</td>
                <td><select name="cboGraphicNo" id="cboGraphicNo" style="width:200px" class="chosen-select">
               <option>Graphic 1</option>
                  </select></td>
                <td class="normalfnt">Orders</td>
                <td width="32%"><select name="cboOrders" id="cboOrders" style="width:200px" class="chosen-select">
               <option>200135/2014/ S 1</option>
                  </select></td>
                <td width="5%">&nbsp;</td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td width="32%" style="text-align:right"><a class="button green medium" id="butAddItem" name="butAddItem">Add Items</a></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td><table  width="100%" class="bordered" id="tblInkItems" >
              <tr>
                <th width="5%">Del</th>
                <th width="35%">Item</th>
                <th width="9%">UOM</th>
                <th width="19%">weight</th>
                <th width="15%">Extra</th>
                <th width="17%">Orders</th>
              </tr>
              <tr>
                <td style="text-align:center"><img class="mouseover delImg" src="../../../images/del.png" width="15" height="15" /></td>
                <td align="center"><span style="text-align:left">57-P BRILIANT REGAL RED</span></td>
                <td align="center">KG</td>
                <td align="center"><input type="text" name="txtExtra" id="txtExtra" value="0.120" style="text-align:right;width:90px"" /></td>
                <td align="center"><input type="text" name="txtExtra" id="txtExtra" value="0.30" style="text-align:right;width:90px"" /></td>
                <td align="center"><a class="button green small" id="butAddOrders" name="butAddOrders">Add Orders</a></td>
              </tr>
              <tr>
                <td style="text-align:center"><img class="mouseover delImg" src="../../../images/del.png" width="15" height="15" /></td>
                <td align="center">MAGNA CRACKLE NEUTRAL</td>
                <td align="center">KG</td>
                <td align="center"><input type="text" name="txtExtra" id="txtExtra" value="0.100" style="text-align:right;width:90px"" /></td>
                <td align="center"><input type="text" name="txtExtra" id="txtExtra" value="0.20" style="text-align:right;width:90px"" /></td>
                <td align="center"><a class="button green small" id="butAddOrders" name="butAddOrders">Add Orders</a></td>
              </tr>
            </table></td>
        </tr>
        <tr>
          <td align="center"><a class="button white medium" id="butNew" name="butNew">New</a>
            <?php if($form_permision['add']||$form_permision['edit']){ ?>
            <a class="button white medium" id="butSave" name="butSave">Save</a>
            <?php } ?>
            <a class="button white medium" id="butConfirm" name="butConfirm">Approve</a><a class="button white medium" id="butReport" name="butReport">Report</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
        </tr>
      </table>
    </div>
  </div>
</form>

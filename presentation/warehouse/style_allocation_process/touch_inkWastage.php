<title>Color Room Process</title>
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../../css/button.css" rel="stylesheet" type="text/css" />
<link href="../../customerAndOperation/bulk/bulkStockAllocation_inkType/css.css" rel="stylesheet" />

<form id="frmPopUp" name="frmPopUp" method="post">
  <div align="center">
    <div style="width:auto;height:600px;background:#000;padding-right:15px;
	padding-top:20px;

	padding-left:30px;
	padding-right:30px;">
      <table width="100%" border="0">
      <tr>
      <td>
      <table width="100%">
      <tr>
                  <td style="background:#06C;color:#FFF;font-size:18px;border-radius:10px 10px 10px 10px;text-align:center" height="30" class="normalfnt">COLOR ROOM INK WASTAGE</td>

      <td align="right"><a class="button red medium" style="font-size:18px;" id="butClose" name="butClose">Log out</a> <a href="touch_main_form.php" class="button pink medium" style="font-size:18px;" id="butClose" name="butClose">Home</a></td></tr>
<tr><td>&nbsp;</td><td>&nbsp;</td></tr>
</table>
      </td>
      </tr>
        
      <tr>
      <td height="51">
      	<table align="center">
        <tr>
        <td width="59" class="normalfnt" style="color:#FFF;font-size:20px">Ink</td>
        <td width="323" class="normalfnt" style="color:#FFF;font-size:18px"><select name="cboLocation" id="cboLocation" style="width:300px;height:33px;font-size:18px">
                  <option>Direct Print/10-100 Shoney Rubber/Bond 55</option></select></td>
        <td width="91" align="center"><a class="button green" style="font-size:18px;" id="butProcess" name="butProcess">Search</a></td>
        </tr>
        <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
        
        </table>
      </td>
      </tr>
     <tr>
     <td>
     <table width="100%" align="center">
     <tr>
     <td><table width="50%" height="144" border="0" align="center" cellpadding="0" cellspacing="1" class="tableBorder_allRound" id="tblParent"  style="background:#FFF;margin-top:5px">
        <tr style="color:#FF0">
          <td width="9%" height="36" align="center" bgcolor="#333333" class="tableBorder_allRound"><input name="chkOrder" type="checkbox" value="" ></td>
          <td class="tableBorder_allRound" width="18%" align="center" bgcolor="#333333">ORDER NO</td>
          <td class="tableBorder_allRound" width="19%" align="center" bgcolor="#333333">SALES ORDER NO</td>
          
        </tr>
       <tr style="color:#FFF">
          <td  width="9%" height="34" align="center" bgcolor="#333333"><input name="chkOrder" type="checkbox" value="" ></td>
          <td width="9%" height="34" align="center" bgcolor="#333333">123232</td>
          <td width="9%" height="34" align="center" bgcolor="#333333">S1</td>
          
        </tr>
         <tr style="color:#FFF">
          <td width="9%" height="34" align="center" bgcolor="#333333"><input name="chkOrder" type="checkbox" value="" ></td>
          <td width="9%" height="34" align="center" bgcolor="#333333">232323</td>
          <td width="9%" height="34" align="center" bgcolor="#333333">S1</td>
          
        </tr>
         <tr style="color:#FFF">
          <td width="9%" height="34" align="center" bgcolor="#333333"><input name="chkOrder" type="checkbox" value="" ></td>
          <td width="9%" height="34" align="center" bgcolor="#333333">232331</td>
          <td width="9%" height="34" align="center" bgcolor="#333333">S2</td>
          
        </tr>
         <tr style="color:#FFF">
          <td width="9%" height="34" align="center" bgcolor="#333333"><input name="chkOrder" type="checkbox" value="" ></td>
          <td width="9%" height="34" align="center" bgcolor="#333333">365571</td>
          <td width="9%" height="34" align="center" bgcolor="#333333">S1</td>
          
        </tr>
    </table>
</td>
     </tr>
     <tr><td>&nbsp;</td></tr>
     <tr>
     <td>
     <table width="80%" height="144" border="0" align="center" cellpadding="0" cellspacing="1" class="tableBorder_allRound" id="tblParent"  style="background:#FFF;margin-top:5px">
        <tr style="color:#FF0">
          <td class="tableBorder_allRound" width="19%" align="center" bgcolor="#333333">ORDER NO</td>
          <td class="tableBorder_allRound" width="19%" align="center" bgcolor="#333333">SALES ORDER NO</td>
          <td width="19%" height="36" align="center" bgcolor="#333333" class="tableBorder_allRound">STOCK BAL (KG)</td>
          <td width="20%" height="36" align="center" bgcolor="#333333" class="tableBorder_allRound">WASTAGE (KG)</td>
          <td width="23%" height="36" align="center" bgcolor="#333333" class="tableBorder_allRound">REMARK</td>

        </tr>
       <tr style="color:#FFF">
          <td width="19%" height="34" align="center" bgcolor="#333333">123232</td>
          <td width="19%" height="34" align="center" bgcolor="#333333">S1</td>
		  <td  width="19%" height="34" align="center" bgcolor="#333333">5</td>
          <td width="20%" height="34" align="center" bgcolor="#333333"><input name="txtWastage" value="0.3" type="text" style="font-size:18px;text-align:right"></td>
          <td width="23%" height="34" align="center" bgcolor="#333333"><input name="txtWastage" value="" type="text" style="font-size:18px;text-align:left;width:150px"></td>

        </tr>
         <tr style="color:#FFF">
          <td width="19%" height="34" align="center" bgcolor="#333333">232323</td>
          <td width="19%" height="34" align="center" bgcolor="#333333">S1</td>
		  <td  width="19%" height="34" align="center" bgcolor="#333333">12</td>
          <td width="20%" height="34" align="center" bgcolor="#333333"><input name="txtWastage" value="0.15" type="text" style="font-size:18px;text-align:right"></td>
          <td width="23%" height="34" align="center" bgcolor="#333333"><input name="txtWastage" value="" type="text" style="font-size:18px;text-align:left;width:150px"></td>
          
        </tr>
         <tr style="color:#FFF">
          <td width="19%" height="34" align="center" bgcolor="#333333">232331</td>
          <td width="19%" height="34" align="center" bgcolor="#333333">S2</td>
   		  <td  width="19%" height="34" align="center" bgcolor="#333333">8</td>
          <td width="20%" height="34" align="center" bgcolor="#333333"><input name="txtWastage" value="0.4" type="text" style="font-size:18px;text-align:right"></td>
          <td width="23%" height="34" align="center" bgcolor="#333333"><input name="txtWastage" value="" type="text" style="font-size:18px;text-align:left;width:150px"></td>

        </tr>
        
    </table>
     </td>
     </tr>
     </table>
     </td>
     </tr>
     <tr><td width="28%" align="center"><a class="button green" style="font-size:18px; id="butProcess" name="butProcess">Save</a></td>
</tr>
     </table>
    </div>
  </div>
</form>


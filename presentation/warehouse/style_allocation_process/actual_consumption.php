<title>Foil/Special RM Actual Consumption</title>

<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../../css/button.css" rel="stylesheet" type="text/css" />

<form id="frmCRN" name="frmCRN" method="post">
  <div align="center">
    <div class="trans_layoutD" style="width:800px">
      <div class="trans_text">Foil/Special RM Actual Consumption</div>
      <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
        <tr>
          <td><table width="100%" border="0" align="center" bgcolor="#FFFFFF">
              <tr class="normalfnt">
                <td width="16%">Serial No</td>
                <td><input name="txtReturnYear" type="text" disabled="disabled" id="txtReturnYear" style="width:60px" value="" />&nbsp;<input name="txtReturnNo" type="text" disabled="disabled" id="txtReturnNo" style="width:90px"value="" /></td>
                <td class="normalfnt">Date</td>
                <td width="25%"><input name="txtReturnNo2" type="text" id="txtReturnNo2" style="width:90px"value="" /></td>
              </tr>
              <tr class="normalfnt">
                <td>Order No</td>
                <td width="43%"><select name="cboSalesOrderNo" id="cboSalesOrderNo" style="width:200px" class="chosen-select">
               <option>200135/2014</option>
                </select></td>
                <td width="16%" class="normalfnt">Sales Order</td>
                <td><select name="cboOrders" id="cboOrders" style="width:200px" class="chosen-select">
               <option>S 1</option>
                </select></td>
              </tr>
              <tr class="normalfnt">
                <td>Graphic No</td>
                <td><select name="cboGraphicNo" id="cboGraphicNo" style="width:200px" class="chosen-select">
               <option>Graphic 1</option>
                </select></td>
                <td class="normalfnt">&nbsp;</td>
                <td style="text-align:right">&nbsp;</td>
              </tr>
             
            </table></td>
        </tr>
        <tr>
          <td><table  width="100%" class="bordered" id="tblInkItems" >
              <tr>
                <th width="32%">Item</th>
                <th width="4%">UOM</th>
                <th width="13%">S</th>
                <th width="13%">M</th>
                <th width="13%">L</th>
                <th width="13%">XL</th>
                <th width="13%">XXL</th>
              </tr>
              <tr>
                <td align="center"><span class="normalfnt" style="text-align:center">D- 1 - S -SILVER</span></td>
                <td align="center">KG</td>
                <td align="center"><input type="text" name="txtSQty" id="txtSQty" value="0.220" style="text-align:right;width:100%" /></td>
                <td align="center"><input type="text" name="txtExtra" id="txtExtra" value="0.110" style="text-align:right;width:100%" /></td>
                <td align="center"><input type="text" name="txtExtra" id="txtExtra" value="0.200" style="text-align:right;width:100%" /></td>
                <td align="center"><input type="text" name="txtExtra" id="txtExtra" value="0.37" style="text-align:right;width:100%" /></td>
                <td align="center"><input type="text" name="txtExtra" id="txtExtra" value="0.40" style="text-align:right;width:100%" /></td>

              </tr>
              <tr>
                <td align="center"><span class="normalfnt" style="text-align:center">K-36 ( 1/170 ) - S/GOLD</span></td>
                <td align="center">KG</td>
                <td align="center"><input type="text" name="txtExtra" id="txtExtra" value="0.100" style="text-align:right;width:100%" /></td>
                <td align="center"><input type="text" name="txtExtra" id="txtExtra" value="0.110" style="text-align:right;width:100%" /></td>
                <td align="center"><input type="text" name="txtExtra" id="txtExtra" value="0.30" style="text-align:right;width:100%" /></td>
                <td align="center"><input type="text" name="txtExtra" id="txtExtra" value="0.50" style="text-align:right;width:100%" /></td>
                <td align="center"><input type="text" name="txtExtra" id="txtExtra" value="0.55" style="text-align:right;width:100%" /></td>

              </tr>
            </table></td>
        </tr>
        <tr height="32">
          <td align="center"><a class="button white medium" id="butNew" name="butNew">New</a><a class="button white medium" id="butSave" name="butSave">Save</a><a class="button white medium" id="butConfirm" name="butConfirm">Approve</a><a class="button white medium" id="butReport" name="butReport">Report</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>
        </tr>
      </table>
    </div>
  </div>
</form>

<style type="text/css">
.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: left;
}
.tableBorder_allRound {
	border: 1px solid #CCCCCC;
	border-radius: 10px 10px 10px 10px;
}
.orderNo {
	font-family: Verdana;
	font-size: 14px;
	color: #000040;
	margin: 0px;
	font-weight: bold;
	text-align: center;
}
/*BEGIN - MAIN INTERFACE GRID DESIGN {*/
 table .bordered{
    *border-collapse: collapse; /* IE7 and lower */
    border-spacing: 0;
    width: 100%;    
}
.bordered {
	border: solid #ccc 1px;
	border-radius: 6px;
	box-shadow: 0 1px 1px #ccc;
	font-size:11px;
	font-family: Verdana;
}  
.bordered td{
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 2px;
}

.bordered th {
    border-left: 1px solid #ccc;
    border-top: 1px solid #ccc;
    padding: 4px;
    text-align: center;    
}

.bordered th {
    background-color: #dce9f9;
    background-image: -webkit-gradient(linear, left top, left bottom, from(#ebf3fc), to(#dce9f9));
    background-image: -webkit-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:    -moz-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:     -ms-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:      -o-linear-gradient(top, #ebf3fc, #dce9f9);
    background-image:         linear-gradient(top, #ebf3fc, #dce9f9);

    box-shadow: 0 1px 0 rgba(255,255,255,.8) inset;        
    border-top: none;
    text-shadow: 0 1px 0 rgba(255,255,255,.5); 
}

.bordered td:first-child, .bordered th:first-child {
    border-left: none;
}

.bordered th:first-child {

    border-radius: 6px 0 0 0;
}

.bordered th:last-child {

    border-radius: 0 6px 0 0;
}

.bordered th:only-child{

    border-radius: 6px 6px 0 0;
}

.bordered tr:last-child td:first-child {

    border-radius: 0 0 0 6px;
}

.bordered tr:last-child td:last-child {

    border-radius: 0 0 6px 0;
}
/*END - MAIN INTERFACE GRID DESIGN }*/
</style>
<table  width="600" border="0" cellspacing="0" cellpadding="0" class="tableBorder_allRound" style="border-color:#025077">
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="100%" border="0">
        <tr>
          <td width="150" class="normalfnt">DearNilu,</td>
          <td width="450">&nbsp;</td>
        </tr>
        <tr>
          <td width="150">&nbsp;</td>
          <td width="450" class="normalfnt">Following Order Allocated Items requested by Production.</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td><table width="100%" border="0">
        <tr>
          <td width="50%"><table width="100%" border="0" class="tableBorder_allRound">
              <tr>
                <td height="32" width="33%" class="normalfnt">CRN No</td>
                <td width="2%" class="normalfnt">:</td>
                <td width="65%" class="orderNo">200025 / 2014</td>
              </tr>
            </table></td>
          <td width="50%"><table width="100%" border="0" class="tableBorder_allRound">
              <tr>
                <td height="32" width="33%" class="normalfnt">Graphic No</td>
                <td width="3%" class="normalfnt">:</td>
                <td width="64%" class="orderNo">Graphic 1</td>
              </tr>
            </table></td>
        </tr>
      </table></td>
  </tr>
  <tr>
  	<td>
    <table width="100%" border="0">
    <tr>
    	<td>
        <table width="100%" border="0" class="bordered">
        <tr class="normalfnt">
          <th width="6%" style="text-align:center">No</th>
          <th width="33%" style="text-align:center">Item Name</th>
          <th width="20%" style="text-align:center">Orders</th>
          <th width="20%" style="text-align:center">Sales Order</th>
          <th width="21%" style="text-align:center">Qty/Weight</th>
        </tr>
        <tr class="normalfnt">
          <td width="6%" style="text-align:center">01</td>
          <td width="33%" style="text-align:left">D- 1 - S -SILVER</td>
          <td width="20%" style="text-align:center">200180/2014</td>
          <td width="20%" style="text-align:right">s1</td>
          <td width="21%" style="text-align:right">45m</td>
        </tr>
        <tr class="normalfnt">
            <td width="6%" style="text-align:center">02</td>
            <td width="33%" style="text-align:left">ART-20-2MM (ROSE)</td>
            <td width="20%" style="text-align:center">200181/2014</td>
            <td width="20%" style="text-align:right">s2</td>
            <td width="21%" style="text-align:right">110g</td>
            </tr>
        </table>
        </td>
    </tr>
    </table>
    </td>
  </tr>
  
  <tr>
  	<td>
    <table width="100%" border="0">
        <tr>
          <td width="150" class="normalfnt">Thanks,</td>
          <td width="450">&nbsp;</td>
        </tr>
        <tr class="normalfnt">
          <td width="150"><strong>aaa</strong><br />
    ...................</td>
          <td width="450" class="normalfnt">&nbsp;</td>
        </tr>
        <tr>
          <td colspan="2" class="normalfnt">(This is a <strong><span style="color:#025077">NSOFT</span> </strong>system generated email.)</td>
        </tr>
      </table>
    </td>
  </tr>
</table>

<title>Color Room Item Issue</title>
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../../css/button.css" rel="stylesheet" type="text/css" />
<form id="frmCIN" name="frmCIN" method="post">
  <div align="center">
    <div class="trans_layoutD" style="width:700px">
      <div class="trans_text">Color Room Ink Items Issue Note</div>
<table width="100%" border="0">
  <tr>
    <td><table width="100%" border="0">
  <tr>
  <td colspan="6"><table width="100%" border="0">
<tr>
                <td width="3%" height="24">&nbsp;</td>
                <td width="12%" class="normalfnt">Serial No</td>
                <td width="38%"><input id="txtSerialNo" name="txtSerialNo" type="text" style="width:100px;">
                  <input id="txtSerialYear" name="txtSerialYear" type="text" style="width:50px;""></td>
                <td width="10%" class="normalfnt">Date</td>
                <td width="32%"><input name="dtDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="txtbox" id="dtDate" style="width:120px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/>
                  <input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
                <td width="5%">&nbsp;</td>
              </tr>  
              </table></td>
  </tr>
  <tr>
    <td width="3%">&nbsp;</td>
    <td width="12%" class="normalfnt">CRN No</td>
    <td width="46%"><select name="cboCRNno" id="cboCRNno" style="width:100px" class="chosen-select">
                 <option>200034</option>
                  </select> <select name="cboCRNyear" id="cboCRNyear" style="width:50px" >
                 <option>2014</option>
                  </select></td>
    <td width="9%" class="normalfnt">&nbsp;</td>
    <td width="27%">&nbsp;</td>
    <td width="3%">&nbsp;</td>
  </tr>
  <tr>
    <td width="3%">&nbsp;</td>
    <td width="12%">&nbsp;</td>
    <td width="46%">&nbsp;</td>
    <td width="9%">&nbsp;</td>
    <td width="27%">&nbsp;</td>
     <td width="3%">&nbsp;</td>
</table>
</td>
  </tr>
  <tr>
    <td><table width="100%" class="bordered" id="tblIssueItems">
  <tr>
    <th width="3%">&nbsp;</th>
    <th width="19%">Item</th>
    <th width="13%">Order No</th>
    <th width="12%">Sales Order No</th>
    <th width="13%">Requested Qty</th>
    <th width="13%">Issued Qty</th>
    <th width="13%">Stock Bal</th>
    <th width="14%">QTY</th>
  </tr>
  <tr>
    <td><input name="chkItem" id="chkItem" type="checkbox" value="" checked ></td>
    <td style="text-align:center" class="normalfnt">A-1 MC 1100 O/RED</td>
    <td style="text-align:right">65634</td>
    <td style="text-align:right">S1</td>
    <td style="text-align:right">500</td>
    <td style="text-align:right">120</td>
    <td style="text-align:right">1000</td>
    <td style="text-align:right"><input name="txtQty" type="text" style="width:90px; text-align:right" value="100"></td>
  </tr>
  <tr>
    <td><input name="chkItem" id="chkItem" type="checkbox" value=""></td>
    <td style="text-align:center" class="normalfnt">MAGNA ACTIVATOR M</td>
    <td style="text-align:right">65634</td>
    <td style="text-align:right">S1</td>
    <td style="text-align:right">250</td>
    <td style="text-align:right">95</td>
    <td style="text-align:right">650</td>
    <td style="text-align:right"><input name="txtQty" type="text" style="width:90px; text-align:right" value="110"></td>
  </tr>
</table>
</td>
  </tr>
  <tr>
  <td>&nbsp;</td>
  </tr>
  <tr>
 <td align="center"><a class="button white medium" id="butNew" name="butNew">New</a>
            <?php if($form_permision['add']||$form_permision['edit']){ ?>
            <a class="button white medium" id="butSave" name="butSave">Save</a>
            <?php } ?>
            <a class="button white medium" id="butConfirm" name="butConfirm">Approve</a><a class="button white medium" id="butReport" name="butReport">Report</a><a href="main.php" class="button white medium" id="butClose" name="butClose">Close</a></td>  
 </tr>
 <tr>
  <td>&nbsp;</td>
  </tr>
  <tr>
</table>
      
</div>
</div>
</form>

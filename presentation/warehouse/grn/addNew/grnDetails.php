<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
///////////////////////////////
// H B G KORALA   ////////////
//////////////////////////////
 
$mainPath 			= $_SESSION['mainPath'];
$location 			= $_SESSION['CompanyID'];
$company 			= $_SESSION['headCompanyId'];
$userId 			= $_SESSION['userId'];
$thisFilePath 		=  $_SERVER['PHP_SELF'];
 
$programName		='Good Received Note';
$programCode		='P0224';
$menuID				='224';
$serialCategory		='5';

$grnNo 				= (!isset($_REQUEST['grnNo'])?'':$_REQUEST['grnNo']);
$year 				= (!isset($_REQUEST['year'])?'':$_REQUEST['year']);

$invoiceNo 			='';
$invoiceDate		='';
$exchRate 			='';
$poNo 				='';
$poYear 			='';
$date 				='';
$deliveryNo	 		='';
$savedStat			='';
$intStatus			='';
$currency 			='';
$exchRate 			='';


if(($grnNo=='')&&($year=='')){
	//$savedStat 	= (int)getApproveLevel($programName);
	//$intStatus	= $savedStat+1;
	$date		= '';
}
else{
$result = loadHeader($grnNo,$year);
while($row=mysqli_fetch_array($result))
{
	$invoiceNo 	= $row['strInvoiceNo'];
	$invoiceDate= $row['SUPPLIER_INVOICE_DATE'];
	$exchRate 	= $row['dblExchangeRate'];
	$poNo 		= $row['intPoNo'];
	$poYear 	= $row['intPoYear'];
	$date 		= $row['datdate'];
	$deliveryNo = $row['strDeliveryNo'];
	$savedStat	=$row['intApproveLevels'];
	$intStatus	=$row['intStatus'];
	$currency 	= $row['currency'];
    $wayBillNo = $row['WayBillNo'];
	if($row['dblExchangeRate']==''){
		$exchRate = $row['dblBuying'];
	}
}
}
				 
$exchangeRateEditFlag 	= loadEexchangeRateEditFlag($menuID,$userId);
//$grnRateEditFlag 		= loadGrnRateEditFlag($menuID,$userId);
$editMode				=loadEditMode($programCode,$intStatus,$savedStat,$userId);
$confirmatonMode		=loadConfirmatonMode($programCode,$intStatus,$savedStat,$userId);
	if($grnNo==''){
		$confirmatonMode=0;	
	}

 ?>
<title>GOODS RECEIVED NOTE</title>
<?php
//include_once "include/javascript.html";
?>
<!--<script type="text/javascript" src="presentation/warehouse/grn/addNew/grnDetails-js.js"></script>
-->
<script>
	//global variables
	var serialItemId='';
	var serialQty	=0;
	var serialArr 	= {};
	var addedFlag	= {};
	var serialCount ={};

</script>
<form id="frmGRNDetails" name="frmGRNDetails" method="post" action="">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
</table>
<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">GOODS RECEIVED NOTE</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
        <?php
		
		
		?>
          <tr>
            <td width="10%" height="22" class="normalfnt">GRN No</td>
            <td width="22%"><input name="txtGrnNo" type="text" disabled="disabled" class="txtText" id="txtGrnNo" style="width:60px" value="<?php echo $grnNo ?>"/><input name="txtGrnYear" type="text" disabled="disabled" class="txtText" id="txtGrnYear" style="width:40px" value="<?php echo $year ?>"/></td>
            <td width="17%">&nbsp;</td>
            <td width="24%">&nbsp;</td>
            <td width="11%" class="normalfnt">GRN Amount</td>
            <td width="16%"><input name="txtTotGrnAmmount" type="text" disabled="disabled" class="txtNumber" id="txtTotGrnAmmount" style="width:120px ; text-align:right" /></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="10%" height="22" class="normalfnt">GRN Date&nbsp;<span class="compulsoryRed">*</span></td>
            <td width="25%"><input name="dtDate" type="text" value="<?php if($grnNo){ echo substr($date,0,10); }else { echo date("Y-m-d"); }?>" class="txtbox  validate[required]" id="dtDate" style="width:102px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/ <?php if($editMode==0){  ?> disabled="disabled"<?php } ?> /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
            <td width="11%" class="normalfnt"><span class="normalfnt">Invoice No</span>&nbsp;<span class="compulsoryRed">*</span></td>
            <td width="12%"><input name="txtInvioceNo" type="text"  class="validate[required]" id="txtInvioceNo" style="width:103px" value="<?php echo $invoiceNo ?>" <?php if($editMode==0){  ?> disabled="disabled"<?php } ?>/></td>

            <td width="15%">&nbsp;</td>
            <td width="11%" class="normalfnt">Invoice Date&nbsp;<span class="compulsoryRed">*</span></td>
            <td width="16%"><input name="dtInvDate" type="text" value="<?php if($invoiceDate){ echo substr($invoiceDate,0,10); }?>" class="txtbox validate[required]" id="dtInvDate" style="width:120px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/ <?php if($editMode==0){  ?> disabled="disabled"<?php } ?> /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
          </tr>
          <tr>
            <td height="27" class="normalfnt">PO No&nbsp;<span class="compulsoryRed">*</span></td>
            <td><select name="cboPO" style="width:150px" id="cboPO" class="chzn-select validate[required]" <?php if($editMode==0){  ?> disabled="disabled"<?php } ?> >
              <option value=""></option>
              <?php
				$sql = "select 
distinct  
tb1.intPONo,
tb1.intPOYear,
tb1.ShipmentMode
 from (SELECT

IFNULL((SELECT
Sum(ware_grndetails.dblRetunSupplierQty)
FROM
ware_grnheader
Inner Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear
WHERE
ware_grnheader.intStatus =  '1' AND
ware_grnheader.intPoNo =  trn_podetails.intPONo AND
ware_grnheader.intPoYear =  trn_podetails.intPOYear AND 
ware_grndetails.intItemId = trn_podetails.intItem),0) as `dblRetunSupplierQty`,

sum(trn_podetails.dblGRNQty) as dblGRNQty,
sum(trn_podetails.dblQty) as dblQty,
trn_podetails.intPONo,
trn_podetails.intPOYear,
ware_grnheader.intGrnNo,
ware_grnheader.intGrnYear,
trn_poheader.intShipmentMode AS ShipmentMode
FROM
trn_podetails
Left Join ware_grnheader ON trn_podetails.intPONo = ware_grnheader.intPoNo AND trn_podetails.intPOYear = ware_grnheader.intPoYear
Left Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear AND trn_podetails.intItem = ware_grndetails.intItemId
Inner Join trn_poheader ON trn_podetails.intPONo = trn_poheader.intPONo AND trn_podetails.intPOYear = trn_poheader.intPOYear
WHERE
trn_poheader.intStatus =  '1' 
AND  trn_poheader.intDeliveryTo='$location' 
GROUP BY
trn_podetails.intPONo,
trn_podetails.intPOYear,
trn_podetails.intItem order by trn_podetails.intPOYear desc,
trn_podetails.intPONo desc) as tb1 ";


if($grnNo==''){
$sql.="where (tb1.dblQty-tb1.dblGRNQty+IFNULL(tb1.dblRetunSupplierQty,0))>0 ";
}
//echo $sql;								
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if(($poNo."/".$poYear)==($row['intPONo']."/".$row['intPOYear']))
						echo "<option value=\"".$row['intPONo']."/".$row['intPOYear']."\" selected=\"selected\">".$row['intPONo']."/".$row['intPOYear']."</option>";	
						else
						echo "<option value=\"".$row['intPONo']."/".$row['intPOYear']."\">".$row['intPONo']."/".$row['intPOYear']."</option>";	
					    $shipmentMode = $row['ShipmentMode'];
					}
				?>
              </select></td>
            <td class="normalfnt">Exchange Rate&nbsp;<span class="compulsoryRed">*</span></td>
                <td class="normalfnt"><input name="txtExRate" type="text"  class="validate[required,custom[number]]" id="txtExRate" style="width:103px; text-align:right" value="<?php echo $exchRate ?>" disabled="disabled" <?php //2015-11-26 - Rajini if(($exchangeRateEditFlag!=1) || ($editMode==0)){ ?> <?php // } ?>/></td>
            <td class="normalfnt" valign="middle" id="divCurrency" >&nbsp;<?php echo $currency; ?></td>
              <td class="normalfnt">Delivery No</td>
            <td><input name="txtDeliveryNo" type="text"  class="validate[]" id="txtDeliveryNo" style="width:120px" value="<?php echo $deliveryNo ?>" <?php if($editMode==0){  ?> disabled="disabled"<?php } ?>/></td>

          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
              <td width="40%" class="normalfnt"></td>
              <td width="10%" class="normalfnt"><span class="normalfnt">WayBill No</span>&nbsp;<span class="compulsoryRed"></span></td>
              <td width="40%"><input name="txtWayBillNo" type="text"  class="" id="txtWayBillNo" style="width:103px" value="<?php echo $wayBillNo ?>" <?php if($editMode==0){  ?> disabled="disabled"<?php } ?>/></td>
              <td width="100%">&nbsp;</td>
              <td width="8%"><?php if($editMode!=0){ ?><a id="butAddItems"   class="button green medium">Add</a><?php } ?></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><div style="width:900px;height:300px;overflow:scroll" >
          <table width="100%"  class="bordered" id="tblGRNItems" >
            <tr class="">
              <th width="6%" height="22" >Del</th>
              <th width="9%" >Main Category</th>
              <th width="11%" >Sub Category</th>
              <th width="19%" >Description</th>
              <th width="5%">UOM</th>
              <th width="5%">PO Qty</th>
              <th width="6%">Bal Qty</th>
              <th width="6%"> Qty&nbsp;<span class="compulsoryRed">*</span></th>
              <th width="6%"> FOC Qty</th>
              <th width="6%">GRN Rate&nbsp;<span class="compulsoryRed">*</span></th>
              <th width="6%">PO Rate</th>
              <th width="10%">Amount</th>
              <th width="5%">Serial</th>
              <th width="8%">Expire Date</th>
              </tr>
              <?php
			      $sql = "SELECT
ware_grndetails.intItemId,
mst_maincategory.FIXED_ASSET, 
mst_item.intMainCategory,
mst_item.strName AS item,
mst_item.intSubCategory,
mst_subcategory.strName AS subcat,
ware_grndetails.dblGrnQty,

IFNULL((SELECT
Sum(ware_grndetails.dblGrnQty)
FROM
ware_grnheader
Inner Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear
WHERE
ware_grnheader.intPoNo =  tb1.intPONo AND
ware_grnheader.intPoYear =  tb1.intPOYear AND
ware_grndetails.intItemId =  tb1.intItem AND
ware_grnheader.intStatus  <=  ware_grnheader.intApproveLevels AND
ware_grnheader.intStatus  >0 ),0)
 as  totGrn, 

IFNULL((SELECT
Sum(ware_returntosupplierdetails.dblQty)
FROM
ware_returntosupplierheader
Inner Join ware_returntosupplierdetails ON ware_returntosupplierheader.intReturnNo = ware_returntosupplierdetails.intReturnNo AND ware_returntosupplierheader.intReturnYear = ware_returntosupplierdetails.intReturnYear
Inner Join ware_grnheader ON ware_returntosupplierheader.intGrnNo = ware_grnheader.intGrnNo AND ware_returntosupplierheader.intGrnYear = ware_grnheader.intGrnYear
WHERE
ware_grnheader.intPoNo =  tb1.intPONo AND
ware_grnheader.intPoYear =   tb1.intPOYear AND
ware_returntosupplierdetails.intItemId =  tb1.intItem AND
ware_returntosupplierheader.intStatus <=  ware_returntosupplierheader.intApproveLevels AND
ware_returntosupplierheader.intStatus  >0
 ),0) as dblRetunSupplierQty, 
 
ware_grndetails.dblFocQty,
ware_grndetails.dblPoRate,
ware_grndetails.dblGrnRate,
ware_grndetails.dblInvoiceRate,
mst_maincategory.strName AS maincat,
sum(tb1.dblQty) as dblQty, 
mst_units.strCode as uom ,
ware_grndetails.EXPIRE_DATE	AS EXPIRE_DATE,
mst_item.EXPIRERY_ITEM AS EXPIRERY_ITEM 
FROM
ware_grndetails
Inner Join mst_item ON ware_grndetails.intItemId = mst_item.intId
Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
Inner Join ware_grnheader ON ware_grndetails.intGrnNo = ware_grnheader.intGrnNo AND ware_grndetails.intGrnYear = ware_grnheader.intGrnYear
Inner Join trn_podetails as tb1 ON ware_grnheader.intPoNo = tb1.intPONo AND ware_grnheader.intPoYear = tb1.intPOYear AND ware_grndetails.intItemId = tb1.intItem
WHERE 
/*mst_item.intStatus = '1' AND*/  
ware_grndetails.intGrnNo =  '$grnNo' AND
ware_grndetails.intGrnYear =  '$year'  
GROUP BY tb1.intItem 
Order by mst_maincategory.strName asc, mst_subcategory.strName asc, mst_item.strName asc";
				$result = $db->RunQuery($sql);
				$totAmm=0;
				while($row=mysqli_fetch_array($result))
				{
					$category_fixed=$row['FIXED_ASSET'];
					$mainCatName=$row['maincat'];
					$subCatName=$row['subcat'];
					$mainCatId=$row['intMainCategory'];
					$subCatId=$row['intSubCategory'];
					$itemName=$row['item'];
					$uom=$row['uom'];
					$grnQty=$row['dblGrnQty'];
					$foc=$row['dblFocQty'];
					$poQty=$row['dblQty'];
					$balQty=$row['dblQty']-$row['totGrn']+$row['dblRetunSupplierQty'];
					$itemId=$row['intItemId'];
					$poRate=$row['dblPoRate'];
					$grnRate=$row['dblGrnRate'];
					$invoiceRate=$row['dblInvoiceRate'];
					$ammount=($row['dblGrnQty']-$row['dblFocQty'])*$row['dblGrnRate'];
					$itemExpire	= $row["EXPIRERY_ITEM"]=='1'? 'validate[required]':'';
					$itemExpire_disable	= $row["EXPIRERY_ITEM"]=='1'? '':'disabled="disabled"';
					$totAmm+=$ammount;
  					
			  ?>
<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF" class="serialFlag" <?php if($category_fixed==1) { ?> id="1" <?php } ?>><?php if($editMode==1){  ?><img class="delImg" src="images/del.png" width="15" height="15" /><?php } ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $mainCatId; ?>"><?php echo $mainCatName; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $subCatId; ?>"><?php echo $subCatName; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $itemId; ?>" class="item"><?php echo $itemName; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $uom; ?>" class="uom"><?php echo $uom; ?></td>
			<td align="right" bgcolor="#FFFFFF" id="<?php echo $itemId; ?>"><?php echo $poQty; ?></td>
			<td align="right" bgcolor="#FFFFFF" id=""><?php echo $balQty; ?></td>
			<td align="center" bgcolor="#FFFFFF" id="" ><input  id="<?php echo $itemId; ?>" class="validate[required,custom[number],max[<?php echo ($balQty); ?>]] calculateValue grnQty" style="width:50px;text-align:right" type="text" value="<?php echo $grnQty; ?>" <?php if($editMode==0){  ?> disabled="disabled"<?php } ?>/></td>
			<td align="center" bgcolor="#FFFFFF" id="" ><input  id="<?php echo $itemId; ?>" class="validate[custom[number],max[<?php echo $balQty; ?>]] calculateValue focQty" style="width:40px;text-align:right" type="text" value="<?php echo $foc; ?>" <?php if($editMode==0){  ?> disabled="disabled"<?php } ?>/></td>
			<td align="center" bgcolor="#FFFFFF" id="" ><input  id="<?php echo $poRate; ?>" class="validate[required,custom[number]] calculateValue rate" style="width:60px;text-align:right" type="text" value="<?php echo $grnRate; ?>" <?php if($editMode==0){  ?> disabled="disabled"<?php } ?> onKeyPress="return validateDecimalPlaces(this.value,6,event);"/></td>
			<td align="right" bgcolor="#FFFFFF" id="<?php echo $itemId; ?>" class="invoiceRate"><?php echo $invoiceRate; ?></td>
			<td align="right" bgcolor="#FFFFFF" id="<?php echo $itemId; ?>" class="ammount"><?php echo number_format($ammount,2,'.',''); ?></td>
			<td align="right" bgcolor="#FFFFFF" id="<?php echo $itemId; ?>" class="addSerial"><?php if($serialCategory==$mainCatId) { ?><a id="butSerial" name="butSerial"  class="button green small"><b>+</b></a><?php } ?>&nbsp;</td>
			<td align="right" bgcolor="#FFFFFF" class="expireDate"><input <?php echo $itemExpire_disable?> name="dtInvDate<?php echo $itemId ?>" type="text" value="<?php echo $row["EXPIRE_DATE"]?>" class="txtbox <?php echo $itemExpire?>" id="dtInvDate<?php echo $itemId; ?>" style="width:70px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" /><input type="reset" value=""  class="txtbox" style="visibility:hidden;width:1px"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
            </tr>              
<?php
				}

			  if(($form_permision['add']||$form_permision['edit']) && $editMode==1){
				 $emode =1;
			  }else {
				  $emode =0;
			  }
			  ?>
          </table>
        </div></td>
      </tr>
      <tr>
        <td align="center" class=""><table width="100%" border="0">
          <tr>
            <td rowspan="2"><iframe id="iframeFiles" src="presentation/warehouse/grn/addNew/filesUpload.php?txtFolder=<?php echo "$grnNo-$year".'&editMode='.$emode; ?>" name="iframeFiles" style="width:400px;height:175px;border:none"  ></iframe></td>
            <td width="11%" class="normalfnt">&nbsp;</td>
            <td width="10%">&nbsp;</td>
            <td width="11%" class="normalfnt">&nbsp;</td>
            <td width="11%">&nbsp;</td>
          </tr>
          <tr>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td align="center" class="tableBorder_allRound">
        <a name="butNew"  class="button white medium" id="butNew">New</a>
         <?php if(($form_permision['add']||$form_permision['edit']) && $editMode==1){ ?>
         <a id="butSave" name="butSave"  class="button white medium">Save</a>
		 <?php } ?>
         <a id="butConfirm" name="butConfirm" style=" <?php if(($intStatus>1) and ($confirmatonMode==1)){  echo 'display:yes'; } else { ?>  <?php echo 'display:none';  } ?>" class="button white medium">Approve</a>
         <a id="butReport" name="butReport"  class="button white medium">Report</a>
         <a href="main.php"  class="button white medium">Close</a></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
  <script type="text/javascript" language="javascript">
  $('#txtTotGrnAmmount').val('<?php echo number_format($totAmm,2,'.','') ?> ');
  </script>
</form>

	<!-- items to prn -->
	<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1"></div>

<?php
//------------------------------function  Permissin to edit exchange rate---------------------
function loadEexchangeRateEditFlag($menuID,$userId)
{
global $db;

$sql = "SELECT
		menus_special.intStatus
		FROM
		menus_special
		Inner Join menus_special_permision ON menus_special.intId = menus_special_permision.intSpMenuId
		WHERE
		menus_special_permision.intUser =  '$userId' AND menus_special.intStatus='1' AND 
		menus_special.intMenuId =  '$menuID'";
		$result = $db->RunQuery($sql);
		$row=mysqli_fetch_array($result);
		$exchangeRateEditFlag 	= $row['intStatus'];
			 return $exchangeRateEditFlag;
}
//------------------------------function load Header---------------------
function loadHeader($grnNo,$year)
{
	global $db;
		 $sql = "SELECT
				ware_grnheader.strInvoiceNo,
				ware_grnheader.SUPPLIER_INVOICE_DATE,
				ware_grnheader.strDeliveryNo,
				ware_grnheader.dblExchangeRate, 
				ware_grnheader.datdate,
				ware_grnheader.intPoNo,
				ware_grnheader.intStatus,
				ware_grnheader.intApproveLevels, 
				ware_grnheader.intPoYear, 
				mst_financecurrency.strCode as currency, 
				mst_financeexchangerate.dblBuying, 
				ware_grnheader.WayBillNo
				FROM
				ware_grnheader
				Inner Join trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo
				/*Inner Join mst_financeexchangerate ON trn_poheader.intCurrency = mst_financeexchangerate.intCurrencyId */ 
				Inner Join mst_financeexchangerate ON trn_poheader.intCurrency = mst_financeexchangerate.intCurrencyId AND trn_poheader.dtmPODate = mst_financeexchangerate.dtmDate
				Inner Join mst_financecurrency ON trn_poheader.intCurrency = mst_financecurrency.intId 
				Inner Join mst_locations ON trn_poheader.intCompany = mst_locations.intId AND mst_locations.intCompanyId = mst_financeexchangerate.intCompanyId
				WHERE
				ware_grnheader.intGrnNo =  '$grnNo' AND
				ware_grnheader.intGrnYear =  '$year'";
				 $result = $db->RunQuery($sql);
			 return $result;
}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
			 
	return $editMode;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	 $sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}


 
?>


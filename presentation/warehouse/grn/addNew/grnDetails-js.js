var basePath	="presentation/warehouse/grn/addNew/";
var reportId	="895";

		
$(document).ready(function() {
			
	$("#frmGRNDetails").validationEngine();
	
	$('#frmGRNDetails #cboSupplier').focus();
	
	calculateTotalAmnt();
	
	$("#frmGRNDetails #butAddItems").die('click').live('click',function(){
		loadPopup();
	});
	//----------------------------
	$("#frmGRNDetails .addSerial").die('click').live('click',function(){
		loadPopupSerial(this);
	});
	//----------------------------
	$("#frmGRNDetails #cboPO").die('change').live('change',function(){
		clearRows();
		if(loadExchangeRate() ==  true)
		{
			disablePopup();
			loadPopup();
		}
	});
	//----------------------------	
	$("#frmGRNDetails .delImg").die('click').live('click',function(){
		deleteClickedRow(this);
	});
	
	$('#frmGRNDetails .delImgPopup').die('click').live('click',function(){
		$(this).parent().parent().remove();
	});
	//----------------------------	
	$("#frmGRNDetails .clsValidateBalQty").die('keyup').live('keyup',function(){
		validateBalQty(this);
	});
	//----------------------------
	$("#frmGRNDetails .calculateValue").die('keyup').live('keyup',function(){
		calculateValue(this);
	});
	//----------------------------
	$('#frmGRNDetails #cboCurrency').die('change').live('change',function(){
	  loadExchangeRate();
	});
	  //-------------------------------------------------------
   $('#frmGRNDetails #cboSupplier').die('change').live('change',function(){
	   loadSupplierDetails();
   });
   //-------------------------------------------------------
 
	$('#frmGRNDetails #butSave').die('click').live('click',function(){
		save();
	});
	
   //-------------------------------------------------------
	$('#frmGRNDetails #butNew').die('click').live('click',function(){
		newPage();
	});
	//-----------------------------------
	$('#frmGRNDetails #butReport').die('click').live('click',function(){
		viewReport();
	});
	//----------------------------------	
	$('#frmGRNDetails #butConfirm').die('click').live('click',function(){
		confirmReport()
	});
	//-----------------------------------------------------
	$('#frmGRNDetails #butClose').die('click').live('click',function(){
			//load('main.php');	
	});
 
  //-------------------------------------------- 
	$('#frmGRNDetailsPopup #cboMainCategory').die('change').live('change',function(){
		loadSubCategory();
  });
 //-------------------------------------------- 
	$('#frmGRNDetailsPopup #imgSearchItems').die('click').live('click',function(){
	loadItems();
  });
 //-------------------------------------------------------
 $('#frmGRNDetailsPopup #chkAll').die('click').live('click' ,function(){
	 checkAll();
	});
  //-------------------------------------------------------
    
$('#frmGRNDetailsPopup #butAdd').die('click').live('click' ,addClickedRows);
$('#frmGRNDetailsPopup #butClose1').die('click').live('click' ,disablePopup);

$('#frmGRNDetailsSerialPopup .add2').die('click').live('click' ,addSerials);
$('#frmGRNDetailsSerialPopup .close2').die('click').live('click' ,disablePopup);






 
});

//----------end of ready -------------------------------

function loadPopup()
{
	disablePopup();
	popupWindow3('1');
	var poNo = $('#cboPO').val();
	$('#popupContact1').load(basePath+'grnDetailsPopup.php?poNo='+poNo,function(){
	//$.getScript('presentation/warehouse/grn/addNew/grnDetails-js.js');
 	});	
}//-------------------------------------------------------

//-------------------------------------------------------
function loadPopupSerial(obj)
{
  	var poNo 		= $('#cboPO').val();
	var grnNo 		= $('#txtGrnNo').val();
	var grnYear 	= $('#txtGrnYear').val();
		serialItemId=$(obj).parent().find(".addSerial").attr('id');
		serialQty	=parseFloat($(obj).parent().find(".grnQty").val());
		
			var arr_s	= '';
			if(serialArr[serialItemId]){
			arr_s="[";
				var len = serialArr[serialItemId].length
				for (var i=0; i<len; ++i) {
						var serials	= serialArr[serialItemId][i];
						arr_s += "{";
						arr_s += '"serials":"'+		serials +'"' ;
						arr_s +=  '},';
				}
			arr_s = arr_s.substr(0,arr_s.length-1);
			arr_s += " ]";
			}
			//alert(arr_s);
	//disablePopup();
	popupStatus = 0;
	$('#popupContact1').html('');
 	popupWindow3('1');
  	$('#popupContact1').load(basePath+'grnDetailsSerialPopup.php?poNo='+poNo+'&grnNo='+grnNo+'&grnYear='+grnYear+'&serialItemId='+serialItemId+'&serialQty='+serialQty+'&arr_s='+JSON.stringify(serialArr[serialItemId]),function(){
 			//$('#butAdd').die('click').die('click').live('click',addSerials);
			//$('#butClose1').die('click').die('click').live('click',disablePopup);
	});	
	
}
//-------------------------------------------------------
 //-------------------------------------
function calculateTotalAmnt()
{
 
	var rowCount = document.getElementById('tblGRNItems').rows.length;
	var totGRNAmmount=0;
	var amnt=0;
	$('#tblGRNItems .ammount').each(function(){
		amnt = parseFloat($(this).html());
		if(isNaN(amnt)){
			amnt=0;
		}
		totGRNAmmount+=amnt;
	});
	
	totGRNAmmount= RoundNumber(totGRNAmmount,2).toFixed(2);
	$('#txtTotGrnAmmount').val(totGRNAmmount)
	
}
//---------------------------------------
function alertx()
{
	$('#frmGRNDetails #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmGRNDetails #butDelete').validationEngine('hide')	;
}

 
//----------------------------------------------
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
//----------------------------------------------- 
function clearRows()
{
	//var rowCount = $('#tblDispatchPopup >tbody >tr').length;
	var rowCount = document.getElementById('tblGRNItems').rows.length;
	//alert(rowCount);
	//var cellCount = document.getElementById('tblMain').rows[1].cells.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblGRNItems').deleteRow(1);
			
	}
	calculateTotalAmnt();
}
//------------------------------------------------------------
function checkAlreadySelected(){
	
	$('#tblGRNItems .item').each(function(){
			var itemNo	= $(this).attr('id');
			
			var j=0;
			$('#tblItemsPopup .itemP').each(function(){
				j++;
				var itemNoP = 	$(this).attr('id');
				if((itemNo==itemNoP)){
					document.getElementById('tblItemsPopup').rows[j].cells[0].childNodes[0].checked=true;
					document.getElementById('tblItemsPopup').rows[j].cells[0].childNodes[0].disabled=true;
				}
			});
	});
}
//-----------------------------------------------------------------------
function loadExchangeRate(){
	var po = $('#cboPO').val();
	var grnDate		= $('#dtDate').val();
	var url 		= basePath+"grnDetails-db-get.php?requestType=loadExchangeRate";

		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"po="+po+"&grnDate="+grnDate,
			async:false,
			success:function(json){
				if(json.type == 'fail')
				{
					alert(json.msg)	;
					$('#cboPO').val('');
					return false;
					
				}
				else
				{	var shipmentMode = json.shipmentMode;
					document.getElementById("txtExRate").value=json.excRate;
					document.getElementById("divCurrency").innerHTML=json.currency;

					if(shipmentMode == 4){
                       // $("#txtWayBillNo").attr("disabled", false);
                        var element = document.getElementById("txtWayBillNo");
                        element.classList.add("validate[required]");
					}else {

                        var element = document.getElementById("txtWayBillNo");
                        element.classList.remove("validate[required]");
                       // $("#txtWayBillNo").attr("disabled", true);
					}
					return true;
				}
			}
		});
}
//disabling popup with jQuery magic!
 
 
function addClickedRows()
{
	//var rowCount = $('#tblDispatchPopup >tr').length;
	var rowCount = document.getElementById('tblItemsPopup').rows.length;
	var totGRNAmmount=0;

		var i=0;
		$('#tblItemsPopup .itemP').each(function(){
			i++;
		if((document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].checked==true) && (document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].disabled==false)){
			
			
			var itemId=$(this).attr('id');
			var maincatId=$(this).parent().find(".mainCatNameP").attr('id');
			var subCatId=$(this).parent().find(".subCatNameP").attr('id');
			var code=$(this).parent().find(".code").html();
			var uom=$(this).parent().find(".uomP").html();
			var itemName=$(this).parent().find(".itemP").html();
			var unitPrice=parseFloat($(this).parent().find(".unitPriceP").attr('id'));
			unitPrice = unitPrice;
			var mainCatName=$(this).parent().find(".mainCatNameP").html();
			var subCatName=$(this).parent().find(".subCatNameP").html();
			var prn=$(this).parent().find(".unitPriceP").html();
			var poQty=parseFloat($(this).parent().find(".poQty").html());
			var balQty=parseFloat($(this).parent().find(".poQty").attr('id'));
			var addSerial=$(this).parent().find(".addSerial").html().trim();
			var itemExpire = $(this).parent().find(".expireDate").html().trim()=='1'? 'validate[required]':'';
			var itemExpire_disable = $(this).parent().find(".expireDate").html().trim()=='1'? '':'disabled="disabled"';
			
 			if(addSerial!=''){
				serialArr[itemId]	=[]; 
				var serialFlagId=1;
			}
			else{
				var serialFlagId=0;
			}

			
			balQty = RoundNumber(balQty,4)
			
			var ammount=0;
			addSerial	= addSerial==""?'&nbsp;':addSerial;
			var content='<tr class="normalfnt"><td align="center"  id="'+serialFlagId+'"  class="serialFlag"  bgcolor="#FFFFFF"><img class="delImg" src="images/del.png" width="15" height="15" /></td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'">'+mainCatName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'">'+subCatName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="item">'+itemName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+uom+'" class="uom">'+uom+'</td>';
			content +='<td align="right" bgcolor="#FFFFFF" id="'+itemId+'">'+poQty+'</td>';
			content +='<td align="right" bgcolor="#FFFFFF" id="'+prn+'">'+balQty+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="" ><input  id="'+itemId+'" class="validate[required,custom[number],max['+balQty+']] calculateValue grnQty" style="width:50px;text-align:right" type="text" value="0"/></td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="" ><input  id="'+itemId+'" class="validate[custom[number],max['+balQty+']] calculateValue focQty" style="width:40px;text-align:right" type="text" value="0"/></td>';
			content +='<td align="center" bgcolor="#FFFFFF" id=""><input  id="'+unitPrice+'" class="validate[required,custom[number]] calculateValue rate" style="width:60px;text-align:right" type="text" value="'+unitPrice+'"  onKeyPress=\"return validateDecimalPlaces(this.value,6,event);\"/></td>';
			content +='<td align="right" bgcolor="#FFFFFF" id="'+itemId+'" class="invoiceRate" >'+unitPrice+'</td>';
			content +='<td align="right" bgcolor="#FFFFFF" id="'+itemId+'" class="ammount">'+ammount+'</td>';
			content +="<td align=\"right\" bgcolor=\"#FFFFFF\" id=\""+itemId+"\" class=\"addSerial\">"+addSerial+"</td>";
			content +="<td align=\"right\" bgcolor=\"#FFFFFF\" class=\"expireDate\"><input "+itemExpire_disable+" name=\"dtInvDate"+itemId+"\" type=\"text\" class=\"txtbox "+itemExpire+"\" id=\"dtInvDate"+itemId+"\" style=\"width:70px;\" onmousedown=\"DisableRightClickEvent()\" onmouseout=\"EnableRightClickEvent()\" onkeypress=\"return ControlableKeyAccess(event)\" onclick=\"return showCalendar(this.id,'%Y-%m-%d')\"/><input type=\"reset\" value=\"\"  class=\"txtbox\" style=\"visibility:hidden;width:1px;\" /></td>";
			content +='</tr>';	
			add_new_row('#frmGRNDetails #tblGRNItems',content);
			totGRNAmmount+=ammount;
		}
	});
	$('#txtTotGrnAmmount').val(totGRNAmmount)
	$('#txtTotGrnAmmount').val(0)
	disablePopup();
	disablePopup();
}

function addSerials(){
	
 	
 		var poNo 		= $('#cboPO').val();
		var grnNo 		= $('#txtGrnNo').val();
		var grnYear 	= $('#txtGrnYear').val();
		var m=0;
		var j=0;
		var duplicateFlag=0;
		
		serialArr[serialItemId]		=[];
		serialCount[serialItemId]=[];
		addedFlag[serialItemId]		=1;
		$('#tblItemsSerialPopup .serial').each(function(){
			m++;
			if($(this).val().trim()!=''){
				j++;
				var serial					=$(this).val().trim();
				serialArr[serialItemId][j-1]=serial;
				if(!serialCount[serialItemId][serial])
					serialCount[serialItemId][serial]=0;
				serialCount[serialItemId][serial]++;
 				if(serialCount[serialItemId][serial]>1){
					duplicateFlag=1;
				}
 			}
  		});
  
  		if(serialQty!=j){
			alert("No of serials not match with item qty");
			return false;			
		}
  		else if(duplicateFlag==1){
			alert("Duplicate Serials exists.");
			return false;			
		}
		else{
			//alert("addes successfully.");
			disablePopup();
			return true;			
		}
 	
}

function deleteClickedRow(obj){
 		var itemId=$(obj).parent().parent().find(".item").attr('id');
		serialArr[itemId]		=[];
		addedFlag[itemId]		=0;
		$(obj).parent().parent().remove();
		calculateTotalAmnt();
}

function  validateBalQty(obj){
		var input=$(obj).val();
		var balQty=$(obj).closest('td').attr('id');
		if((input>balQty) || (input<0)){
		alert("Invalid Qty");
			$(obj).val(balQty);
		}
}

function calculateValue(obj){
	
	if(decimalPlaces($(obj).val()) > 6)		
	$(obj).val(parseFloat($(obj).val()).toFixed(6));

	var price 	= parseFloat($(obj).parent().parent().find(".rate").val());
	var qty	  	= $(obj).parent().parent().find(".grnQty").val();
	var focQty 	= $(obj).parent().parent().find(".focQty").val();
	if(qty == ''){
		qty = 0;
	}
	if(focQty == ''){
		focQty = 0;
	}
	if(parseFloat(focQty) > parseFloat(qty)){
		alert('Invalid FOC Qty');
		$(obj).parent().parent().find(".focQty").val(0);
		$('#frmGRNDetails  .calculateValue').die('keyup').live('keyup');
		return false;
	}
	var qty = parseFloat(qty)-parseFloat(focQty);
	
	
	if(isNaN(price)){
		price=0;
	}
	if(isNaN(qty)){
		qty=0;
	}
	var amm=qty*price;
	amm= RoundNumber(amm,2).toFixed(2);
	$(obj).parent().parent().find(".ammount").html(amm);
	calculateTotalAmnt();
}

function loadSupplierDetails(){
	
	var supplier = $('#cboSupplier').val();
	var url 		= basePath+"grnDetails-db-get.php?requestType=loadSupplierDetails";
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		data:"supplier="+supplier,
		async:false,
		success:function(json){

				$('#frmGRNDetails #cboCurrency').val(json.currency);
				$('#frmGRNDetails #cboPayTerm').val(json.payTerm);
				$('#frmGRNDetails #cboPayMode').val(json.payMode);
				$('#frmGRNDetails #cboShipmentMode').val(json.shipmentMode);
				document.getElementById("divExchRate").innerHTML=json.excRate;
		}
	});
}

function save(){
	
	disablePopup();
	var requestType = '';
	if ($('#frmGRNDetails').validationEngine('validate'))   
    { 
		showWaiting();
		var data = "requestType=save";
 
		var arrHeader = "{";
							arrHeader += '"serialNo":"'+$('#txtGrnNo').val()+'",' ;
							arrHeader += '"Year":"'+$('#txtGrnYear').val()+'",' ;
							arrHeader += '"deliveryNo":'+URLEncode_json($('#txtDeliveryNo').val().trim())+',' ;
							arrHeader += '"poNo":"'+$('#cboPO').val()+'",' ;
							arrHeader += '"exchRate":"'+$('#txtExRate').val()+'",' ;
							arrHeader += '"invoiceNo":'+URLEncode_json($('#txtInvioceNo').val().trim())+',' ;
							arrHeader += '"date":"'+$('#dtDate').val()+'",' ;
							arrHeader += '"invDate":"'+$('#dtInvDate').val()+'",' ;
        					arrHeader += '"waybillNo":"'+$('#txtWayBillNo').val()+'"' ;
			arrHeader += ' }';

			var rowCount = document.getElementById('tblGRNItems').rows.length;
			if(rowCount==1){
				alert("items not selected to GRN");hideWaiting();
				return false;				
			}
			var row = 0;
			var errorQtyFlag=0;
			var erroGrnRateFlag=0;
			var	errorSerialFlag=0;
			
			var arr="[";
			var arr_s="[";
		
			
			$('#tblGRNItems .item').each(function(){
	
				var itemId	= $(this).attr('id');
				var Qty	= $(this).parent().find(".grnQty").val();
				var FOC	= $(this).parent().find(".focQty").val();
				var poRate	= $(this).parent().find(".rate").attr('id');
				var grnRate	= $(this).parent().find(".rate").val();
				var invoiceRate	= $(this).parent().find(".invoiceRate").html();
				var serialsFlag	= $(this).parent().find(".serialFlag").attr('id');
				var expireDate	= $(this).parent().find(".expireDate").children(0).val();

				if(!addedFlag[itemId]){
					addedFlag[itemId]=0;
				}
				if(serialArr[itemId]){
					if((serialsFlag==1) && (serialArr[itemId].length<=0)){
						errorSerialFlag=1;
 					}
				}
				
				//----------------------------------------------------------
				if(serialArr[itemId]){
					var len = serialArr[itemId].length
					for (var i=0; i<len; ++i) {
							var serials	= serialArr[itemId][i];
							arr_s += "{";
							arr_s += '"itemId":"'+		itemId +'",' ;
							arr_s += '"Qty":"'+		Qty +'",' ;
							arr_s += '"FOC":"'+		FOC +'",' ;
							arr_s += '"poRate":"'+		poRate +'",' ;
							arr_s += '"grnRate":"'+		grnRate +'",' ;
 							arr_s += '"serials":'+URLEncode_json(serials)+',';
 							arr_s += '"invoiceRate":"'+		invoiceRate +'",' ;
							arr_s += '"expireDate":"'+expireDate+'"' ;
							arr_s +=  '},';
					}
				}
				
 				//----------------------------------------------------------
					if(Qty>0){
						arr += "{";
						arr += '"itemId":"'+		itemId +'",' ;
						arr += '"serialsFlag":"'+		serialsFlag +'",' ;
 						arr += '"addedFlag":"'+		addedFlag[itemId] +'",' ;
  						arr += '"Qty":"'+		Qty +'",' ;
						arr += '"FOC":"'+		FOC +'",' ;
						arr += '"poRate":"'+		poRate +'",' ;
						arr += '"grnRate":"'+		grnRate +'",' ;
						arr += '"invoiceRate":"'+		invoiceRate +'",' ;
						arr += '"expireDate":"'+expireDate+'"' ;
						arr +=  '},';
						
						
						
						
					}
					if(Qty<=0){
						errorQtyFlag=1;
					}
					else if(grnRate<=0){
						erroGrnRateFlag=1;
					}
			});
			
			if(errorQtyFlag==1){
					alert("Please enter GRN Quantities");hideWaiting();
					return false;
			}
			else if(erroGrnRateFlag==1){
					alert("Please enter GRN Rates");hideWaiting();
					return false;
			}
			else if(errorSerialFlag==1){
					alert("Please enter serials");hideWaiting();
					return false;
			}
			
				
			arr_s = arr_s.substr(0,arr_s.length-1);
			arr_s += " ]";
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
		
		
		
			data+="&arrHeader="	+	arrHeader;
			data+="&arr_s="	+	arr_s;
			data+="&arr="	+	arr;

		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = basePath+"grnDetails-db-set.php";
     	var obj = $.ajax({
			url:url,
			type:'post',
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){

					$('#frmGRNDetails #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t=setTimeout("alertx()",1000);
						$('#frmGRNDetails #txtGrnNo').val(json.serialNo);
						$('#frmGRNDetails #txtGrnYear').val(json.year);
						
						var src = basePath+"filesUpload.php?txtFolder="+json.serialNo+'-'+json.year+'&editMode=1'
 						$('#iframeFiles').attr('src',src);
						
						if(json.confirmationMode==1){
							$('#frmGRNDetails #butConfirm').show();
						}
						else{
							$('#frmGRNDetails #butConfirm').hide();
						}
						return;
					}
				},
			error:function(xhr,status){
					
					$('#frmGRNDetails #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
				}		
			});
		hideWaiting();
	}
}

function newPage(){	
	
	window.location.href = "?q=224";
	$('#frmGRNDetails').get(0).reset();
	clearRows();
	$('#frmGRNDetails #txtGrnNo').val('');
	$('#frmGRNDetails #txtGrnYear').val('');
	$('#frmGRNDetails #txtInvioceNo').val('');
	$('#frmGRNDetails #cboPO').val('');
	$('#frmGRNDetails #txtDeliveryNo').val('');
	$('#frmGRNDetails #txtTotGrnAmmount').val('');
	var currentTime = new Date();
	var month = currentTime.getMonth()+1 ;
	var day = currentTime.getDate();
	var year = currentTime.getFullYear();
	if(day<10)
	day='0'+day;
	if(month<10)
	month='0'+month;
	d=year+'-'+month+'-'+day;
	
	$('#frmGRNDetails #dtDate').val(d);
	
}

function viewReport(){
	
	if($('#frmGRNDetails #txtGrnNo').val()!=''){
		window.open('?q='+reportId+'&grnNo='+$('#txtGrnNo').val()+'&year='+$('#txtGrnYear').val());	
	}
	else{
		alert("There is no GRN No to view");
	}
}

function confirmReport(){
	
	if($('#frmGRNDetails #txtGrnNo').val()!=''){
		window.open('?q='+reportId+'&grnNo='+$('#txtGrnNo').val()+'&year='+$('#txtGrnYear').val()+'&approveMode=1');	
	}
	else{
		alert("There is no GRN No to confirm");
	}
}

function loadSubCategory(){
	
	var mainCategory = $('#frmGRNDetailsPopup #cboMainCategory').val();
	var url 		= basePath+"grnDetails-db-get.php?requestType=loadSubCategory&mainCategory="+mainCategory;
	var httpobj 	= $.ajax({url:url,async:false})
	document.getElementById('cboSubCategory').innerHTML=httpobj.responseText;
}

function loadItems(){
	
	var rowCount = document.getElementById('tblItemsPopup').rows.length;
	for(var i=1;i<rowCount;i++)
	{
		document.getElementById('tblItemsPopup').deleteRow(1);
	}
	var poNo = $('#txtPONo').val();
	var mainCategory = $('#frmGRNDetailsPopup #cboMainCategory').val();
	var subCategory = $('#frmGRNDetailsPopup #cboSubCategory').val();
	var description = $('#frmGRNDetailsPopup #txtItmDesc').val();
	var url 		= basePath+"grnDetails-db-get.php?requestType=loadItems";
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		data:"poNo="+poNo+"&mainCategory="+mainCategory+"&subCategory="+subCategory+"&description="+description,
		async:false,
		success:function(json){

			var length = json.arrCombo.length;
			var arrCombo = json.arrCombo;


			for(var i=0;i<length;i++)
			{
				var itemId=arrCombo[i]['itemId'];	
				var maincatId=arrCombo[i]['maincatId'];	
				var subCatId=arrCombo[i]['subCatId'];	
				var code=arrCombo[i]['code'];	
				var itemName=arrCombo[i]['itemName'];
                                var supItemCode=arrCombo[i]['supItemCode'];
				var uom=arrCombo[i]['uom'];	
				var unitPrice=arrCombo[i]['unitPrice'];	
				var mainCatName=arrCombo[i]['mainCatName'];
				var subCatName=arrCombo[i]['subCatName'];	
				var poQty=arrCombo[i]['poQty'];
				var grnQty=arrCombo[i]['grnQty'];
				var retQty=arrCombo[i]['retQty'];
				var grnBal=parseFloat(poQty)-parseFloat(grnQty)+parseFloat(retQty);
				var balQty=parseFloat(poQty)-parseFloat(grnQty);
				var addSerial=arrCombo[i]['addSerial'];
				var isItemExpired = arrCombo[i]['IsItemExpired'];
				//balQty = (balQty,4);
				//balQty= balQty.fixed(4);
				//balQty = Math.

					
				var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF" id=""><input type="checkbox" id="chkDisp" /></td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'" class="mainCatNameP">'+mainCatName+'</td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'" class="subCatNameP">'+subCatName+'</td>';
                                if(supItemCode!=null)
                                {
				content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="codeP">'+supItemCode+'</td>';
                                }else{
                                content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="codeP">'+code+'</td>';    
                                }
                                content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="itemP">'+itemName+'</td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="'+uom+'" class="uomP">'+uom+'</td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="'+unitPrice+'" style="display:none"  class="unitPriceP">'+poNo+'</td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="'+grnBal+'" style="display:none" class="poQty">'+poQty+'</td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="'+grnBal+'" style="display:none" class="poQty">'+poQty+'</td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" style="display:none" class="addSerial">'+addSerial+'</td>';
				content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" style="display:none" class="expireDate">'+isItemExpired+'</td></tr>';
				add_new_row('#frmGRNDetailsPopup #tblItemsPopup',content);
			}
				checkAlreadySelected();

		}
	});
}

function checkAll(){
	if(document.getElementById('chkAll').checked==true)
	 var chk=true;
	else
	 var chk=false;
	
	var rowCount = document.getElementById('tblItemsPopup').rows.length;
	for(var i=1;i<rowCount;i++)
	{
		document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].checked=chk;
	}
}

function maxDecimals(field, rules, i, options){
var dec=0;
	if(Math.floor(field.val()) === field.val()) dec= 0;
    dec = (field.val()).toString().split(".")[1].length || 0;	
	if (dec > 4) {
     // this allows the use of i18 for the error msgs
     return options.allrules.validate2fields.alertText;
  }}


function decimalPlaces(num) {
  var match = (''+num).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
  if (!match) { return 0; }
  return Math.max(
       0,
       // Number of digits right of decimal point.
       (match[1] ? match[1].length : 0)
       // Adjust for scientific notation.
       - (match[2] ? +match[2] : 0));
}

 		 



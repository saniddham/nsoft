<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	include_once "{$backwardseperator}dataAccess/Connector.php";
	$serialCategory	='5';
	///
	/////////// type of print load part /////////////////////
if($requestType=='loadExchangeRate')
{
    //var_dump($_REQUEST); die;
	$po  = $_REQUEST['po'];
    $date = !isset($_REQUEST["grnDate"])?date('Y-m-d'):$_REQUEST["grnDate"];

	$poArray   = explode('/',$po);
	
	$sql = "SELECT
			mst_financeexchangerate.dblBuying, 
			mst_financecurrency.strCode
			FROM
			trn_poheader
			Inner Join mst_financeexchangerate ON trn_poheader.intCurrency = mst_financeexchangerate.intCurrencyId 
			Inner Join mst_financecurrency ON trn_poheader.intCurrency = mst_financecurrency.intId
			Inner Join mst_locations ON trn_poheader.intCompany = mst_locations.intId AND mst_locations.intCompanyId = mst_financeexchangerate.intCompanyId
			WHERE
			trn_poheader.intPONo =  '$poArray[0]' AND trn_poheader.intPOYear =  '$poArray[1]' AND mst_financeexchangerate.dtmDate='$date' ";
	$result = $db->RunQuery($sql);
	if(!mysqli_num_rows($result))
	{
		$response['type']	= 'fail';
		$response['msg']    = 'Please enter exchange rates for '.$date;
	}
	else
	{
		$row=mysqli_fetch_array($result);
		$response['excRate'] 	= $row['dblBuying'];
		$response['currency'] = $row['strCode'];
	}

	$sql1 ="SELECT
          trn_poheader.intShipmentMode
          FROM
          trn_poheader
          WHERE
          trn_poheader.intPONo = '$poArray[0]'  AND 
          trn_poheader.intPOYear = '$poArray[1]' ";

    if(!mysqli_num_rows($result))
    {
        $response['type']	= 'fail';
        $response['msg']    = 'No Shipment mode '.$date;
    }else{
        $result = $db->RunQuery($sql1);
        $row=mysqli_fetch_array($result);
        $response['shipmentMode'] = $row['intShipmentMode'];

    }


	echo json_encode($response);

}
else if($requestType=='loadSubCategory')
	{
		$mainCategory  = $_REQUEST['mainCategory'];
		$sql = "SELECT
				mst_subcategory.intId,
				mst_subcategory.strCode,
				mst_subcategory.strName
				FROM mst_subcategory
				WHERE
				mst_subcategory.intMainCategory =  '$mainCategory' 
				ORDER BY mst_subcategory.strName ASC";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	else if($requestType=='loadItems')
	{
		$poNo  = $_REQUEST['poNo'];
		$poNoArray 	 = explode('/',$poNo);
		$mainCategory  = $_REQUEST['mainCategory'];
		$subCategory  = $_REQUEST['subCategory'];
		$description  = $_REQUEST['description'];
		
		  $sql="select * from (SELECT
				mst_item.intId,
				mst_item.intMainCategory,
				mst_item.intSubCategory,
				mst_item.strCode,
                mst_item.strCode as SUP_ITEM_CODE,
				mst_item.strName as itemName,
				mst_item.intUOM,
				ROUND(AVG(ROUND(tb.dblUnitPrice,6)*(100-dblDiscount)/100),6) as dblUnitPrice,
				mst_maincategory.strName as mainCatName,
				mst_maincategory.FIXED_ASSET, 
				mst_subcategory.strName as subCatName,   
				tb.intPONo,
				tb.intPOYear,
				sum(tb.dblQty) as dblQty,
				
				IFNULL((SELECT
				Sum(ware_grndetails.dblGrnQty)
				FROM
				ware_grnheader
				Inner Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear
				WHERE
				ware_grnheader.intPoNo =  tb.intPONo AND
				ware_grnheader.intPoYear =  tb.intPOYear AND
				ware_grndetails.intItemId =  tb.intItem AND
				ware_grnheader.intStatus  <=  ware_grnheader.intApproveLevels AND 
				ware_grnheader.intStatus>0),0)
				 as dblGRNQty, 
				 
				IFNULL((SELECT
				Sum(ware_returntosupplierdetails.dblQty)
				FROM
				ware_returntosupplierheader
				Inner Join ware_returntosupplierdetails ON ware_returntosupplierheader.intReturnNo = ware_returntosupplierdetails.intReturnNo AND ware_returntosupplierheader.intReturnYear = ware_returntosupplierdetails.intReturnYear
				Inner Join ware_grnheader ON ware_returntosupplierheader.intGrnNo = ware_grnheader.intGrnNo AND ware_returntosupplierheader.intGrnYear = ware_grnheader.intGrnYear
				WHERE
				ware_grnheader.intPoNo =  tb.intPONo AND
				ware_grnheader.intPoYear =   tb.intPOYear AND
				ware_returntosupplierdetails.intItemId =  tb.intItem AND
				ware_returntosupplierheader.intStatus <=  ware_returntosupplierheader.intApproveLevels AND 
				ware_returntosupplierheader.intStatus > 0
				 ),0) as dblRetunSupplierQty, 
				mst_units.strCode as uom,
				mst_item.EXPIRERY_ITEM AS EXPIRERY_ITEM 
				FROM
				trn_podetails as tb  
				Inner Join trn_poheader ON tb.intPONo = trn_poheader.intPONo AND tb.intPOYear = trn_poheader.intPOYear
				/*Left Join ware_grnheader ON tb.intPONo = ware_grnheader.intPoNo AND tb.intPOYear = ware_grnheader.intPoYear and ware_grnheader.intStatus<=ware_grnheader.intApproveLevels  and ware_grnheader.intStatus>0
				Left Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear AND tb.intItem = ware_grndetails.intItemId*/
				Inner Join mst_item ON tb.intItem = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId
                                LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
				WHERE  
				mst_item.intStatus = '1' AND 
				trn_poheader.intStatus='1' AND 
				tb.intPONo =  '$poNoArray[0]' AND
				tb.intPOYear =  '$poNoArray[1]' ";
				if($mainCategory!='')
				$sql.=" AND mst_item.intMainCategory =  '$mainCategory' ";
				if($subCategory!='')
				$sql.=" AND mst_item.intSubCategory =  '$subCategory'";
				if($description!=''){
				$sql.=" AND
				mst_item.strName LIKE  '%$description%'";
				}
				$sql.=" group by mst_item.intId) as tb1 where (tb1.dblQty-IFNULL(tb1.dblGRNQty,0)+IFNULL(tb1.dblRetunSupplierQty,0))>0";	
				$sql.=" Order by tb1.mainCatName asc, tb1.subCatName asc, tb1.itemName asc";
		//echo $sql;
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$poQty = $row['dblQty'];
			if(!$prnQty)
			$prnQty=0;
			$grnQty = $row['dblGRNQty'];
			if(!$poQty)
			$poQty=0;
			$retQty = $row['dblRetunSupplierQty'];
			if(!$retQty)
			$retQty=0;
			
			$data['itemId'] 	= $row['intId'];
			$data['maincatId'] = $row['intMainCategory'];
			$data['subCatId'] 	= $row['intSubCategory'];
			$data['code'] = $row['strCode'];
                        $data['supItemCode'] = $row['SUP_ITEM_CODE'];
			$data['itemName'] = $row['itemName'];
			$data['uom'] 	= $row['uom'];
			$data['unitPrice'] = round($row['dblUnitPrice'],6);
			$data['mainCatName'] = $row['mainCatName'];
			$data['subCatName'] = $row['subCatName'];
			$data['poQty'] = round($poQty,4);
			$data['grnQty'] =round($grnQty,4);
			$data['retQty'] =round($retQty,4);
			$category_fixed	 = $row['FIXED_ASSET'];		
			if($category_fixed==1)
				$data['addSerial'] 	= '<a id="butSerial" name="butSerial"  class="button green small"><b>+</b></a>';
			else
				$data['addSerial'] 	= ' ';
			$data['IsItemExpired'] = $row['EXPIRERY_ITEM'];
			$arrCombo[] = $data;
		}
		$response['arrCombo'] 	= $arrCombo;
		echo json_encode($response);

	}
	
?>
<?php 
ini_set('display_errors',0);
	session_start();

	$backwardseperator = "../../../../";
	$mainPath 		= $_SESSION['mainPath'];
	$userId 		= $_SESSION['userId'];
	$companyId 		= $_SESSION['CompanyID'];
	$headCompanyID	= $_SESSION['headCompanyId'];
	require_once "{$backwardseperator}dataAccess/Connector.php";
	require_once $backwardseperator."class/cls_commonFunctions_get.php";
	require_once $backwardseperator."class/warehouse/cls_warehouse_get.php";
	require_once $backwardseperator."class/finance/fixedAssets/fixedAssetsRegistry/cls_registry_get.php";
	require_once $backwardseperator."class/finance/fixedAssets/fixedAssetsRegistry/cls_registry_set.php";
	require_once $backwardseperator."class/masterData/exchange_rate/cls_exchange_rate_get.php";
	//ini_set('display_errors', 1); 

	
	$obj_common		= new cls_commonFunctions_get($db);
	$obj_reg_get	= new Cls_Registry_Get($db);
	$obj_reg_set	= new Cls_Registry_Set($db);
	$obj_exchn_get	= new cls_exchange_rate_get($db);

	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$objwhouseget= new cls_warehouse_get($db);
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];

	$arrHeader 	 = json_decode($_REQUEST['arrHeader'],true);

	$serialNo 	 = $arrHeader['serialNo'];
	$year 		 = $arrHeader['Year'];
	$exchRate 	 = $arrHeader['exchRate'];
	$invoiceNo 	 =$obj_common->replace($arrHeader['invoiceNo']);
	$invDate	 = $arrHeader['invDate'];
	$poNo 		 = $arrHeader['poNo'];
	$poNoArray	 =explode("/",$poNo);
	$poNo		 =$poNoArray[0];
	$poYear		 =$poNoArray[1];
	$deliveryNo  =$obj_common->replace($arrHeader['deliveryNo']);
	$date 		 = $arrHeader['date'];
 	$waybillNo1     =$arrHeader['waybillNo'];
	$arr 		= json_decode($_REQUEST['arr'], true);
	$arr_s 		= json_decode($_REQUEST['arr_s'], true);
	if($waybillNo1 == ''){
        $waybillNo ="NULL";
    }else{
        $waybillNo1 = "'$waybillNo1'";

        $waybillNo = str_replace('"', '', $waybillNo1 );
    }


	$ApproveLevels=(int)getApproveLevel('Good Received Note');
	$grnApproveLevel = $ApproveLevels+1;
	
	$programName='Good Received Note';
	$programCode='P0224';
//------------save---------------------------	
	if($requestType=='save')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		$savableFlag= 1;

//------------------------------
		  
		  $sql = "SELECT
					trn_poheader.intSupplier,
					trn_poheader.dtmPODate,
					intCurrency,
					trn_poheader.intShipmentTerm
					 
				FROM
				trn_poheader
				WHERE
					trn_poheader.intPoNo ='$poNo' AND   
					trn_poheader.intPOYear = '$poYear' 
				";
		$result 			= $db->RunQuery2($sql);
		$row 				= mysqli_fetch_array($result);
		$supplierId			= $row['intSupplier'];
		$purchaseDate		= $row['dtmPODate'];
		$currency			= $row['intCurrency'];
		$intShipmentTerm	= $row['intShipmentTerm'];
		
		
		  $rowExchng		= $obj_exchn_get->GetAllValues($currency,2,$date,$headCompanyID,'RunQuery2');
		  if($rowExchng['BUYING_RATE'] == '' || $rowExchng['BUYING_RATE'] == 0)
		  {
			 $rollBackFlag=1;
			 $rollBackMsg= 'Please enter exchange rates for '.$date;
		  }
		  
		  
		//--------------------
/*		$sql = "SELECT
				ware_grnheader.intGrnNo,
				ware_grnheader.intGrnYear
				FROM ware_grnheader
				WHERE
				ware_grnheader.strInvoiceNo =  '$invoiceNo'
				";	*/ 
				
				//	comment by roshan for allow to raised same invoice no for import invoices . 2012-09-17 12:38PM
/*		$sql = "SELECT
					ware_grnheader.intGrnNo,
					ware_grnheader.intGrnYear,
					trn_poheader.intSupplier,
					trn_poheader.dtmPODate
				FROM
				trn_poheader
					Inner Join ware_grnheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
				WHERE
					trn_poheader.intShipmentTerm <> '2' AND
					ware_grnheader.strInvoiceNo = '$invoiceNo'
				";
*///commented by hemanthi on 2013-12-01
		$sql = "SELECT
					ware_grnheader.intGrnNo,
					ware_grnheader.intGrnYear,
					trn_poheader.intSupplier,
					trn_poheader.dtmPODate
				FROM
				trn_poheader
					Inner Join ware_grnheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
				WHERE 
				ware_grnheader.intStatus	=	1 AND 
				trn_poheader.intSupplier = '$supplierId' AND
				ware_grnheader.strInvoiceNo = '$invoiceNo'  AND
				concat(ware_grnheader.intGrnNo,'/',ware_grnheader.intGrnYear) <>  '$serialNo/$year' 
				";
				
		$result 			= $db->RunQuery2($sql);
		$row 				= mysqli_fetch_array($result);
		$existingGrnNo 		= $row['intGrnNo'];
		$existingGrnYear	= $row['intGrnYear'];
		
		
		
		//--------------------------
		if($serialNo==''){//insert
			$serialNo 	= getNextGrnNo();
			$year = date('Y');
			$editMode=0;
			$savedStatus=$grnApproveLevel;
			$savedLevels=$ApproveLevels;
		}
		else{//update
			$editMode=1;
			$savableFlag=getSaveStatus($serialNo,$year);//check wether already confirmed
			$sql = "SELECT
			ware_grnheader.intStatus, 
			ware_grnheader.intApproveLevels 
			FROM ware_grnheader 
			WHERE
			ware_grnheader.intGrnNo =  '$serialNo' AND
			ware_grnheader.intGrnYear =  '$year'";

			$result = $db->RunQuery2($sql);
			$row=mysqli_fetch_array($result);
			$savedStatus=$row['intStatus'];
			$savedLevels=$row['intApproveLevels'];
		}
		//--------------------------
		$poStatus=getPOStatus($poNo,$poYear);//check wether po not confirmed
		//--------------------------
		$response['arrData'] = $objwhouseget->getLocationValidation('GRN',$companyId,$poNo,$poYear);//check for grn location
		//--------------------------
		$editPermission=loadEditMode($programCode,$savedStatus,$savedLevels,$userId);
		$locationEditFlag=getEditLocationValidation('GRN',$companyId,$serialNo,$year);
		//--------------------------
		$invoice_count = get_duplicate_invoice_flag($supplierId,$invoiceNo,$serialNo,$year);
		//--------------------------
		
		 if($response['arrData']['type']=='fail'){
			 $rollBackFlag=1;
			 $rollBackMsg=$response['arrData']['msg'];
		 }
		 else if(( $locationEditFlag==1) && ($editMode==1)){
			 $rollBackFlag=1;
			 $rollBackMsg="Invalid Edit Location";
		 }
		 else if($invoice_count > 0){
			 $rollBackFlag=1;
			 $rollBackMsg = "Duplicate Invoice No for same Supplier";
			 //$rollBackMsg="This Invoice No already exists for the supplier in GRN No : ".$existingGrnNo."/".$existingGrnYear;
		 }
		 else if($poStatus!=1){
			 $rollBackFlag=1;
			 $rollBackMsg="This PO No is not confirmed.Cant raise GRN";
		 }
		 else if($savableFlag==0){
			 $rollBackFlag=1;
			 $rollBackMsg="This GRN No is already confirmed.Cant edit";
		 }
		 else if($editPermission==0){
			 $rollBackFlag=1;
			 $rollBackMsg="No Permission to save.";
		 }
		 else if($exchRate==0 || $exchRate==''){
			 $rollBackFlag=1;
			 $rollBackMsg= 'Please enter exchange rates for '.$date;
		 }
		 else if($intShipmentTerm == 2 && $waybillNo=="NULL")
		  {
			 $rollBackFlag=1;
			 $rollBackMsg= 'Waybill number should be enterd';
		  }
		else if($editMode==1){//edit existing grn
			$sql = "UPDATE `ware_grnheader` SET intPoNo = '$poNo',
												intPoYear = '$poYear',
												intStatus ='$grnApproveLevel', 
												intApproveLevels ='$ApproveLevels', 
												strInvoiceNo ='$invoiceNo', 
												SUPPLIER_INVOICE_DATE ='$invDate',
												strDeliveryNo ='$deliveryNo', 
												datdate ='$date',   
												dblExchangeRate ='$exchRate', 
												intModifiedBy ='$userId', 
												dtmModifiedDate =now(), 
												intCompanyId = '$companyId',
												WayBillNo = $waybillNo  
					WHERE (`intGrnNo`='$serialNo') AND (`intGrnYear`='$year')";
			$result = $db->RunQuery2($sql);
			$maxAppByStatus=(int)getMaxAppByStatus($serialNo,$year)+1;
			$sql = "UPDATE `ware_grnheader_approvedby` SET intStatus ='$maxAppByStatus' 
					WHERE (`intGrnNo`='$serialNo') AND (`intYear`='$year') AND (`intStatus`='0')";
			$result1 = $db->RunQuery2($sql);
		}
		else{//new grn
			if($existingGrnNo==''){//if invoice no is not duplicate
			
				//commented on 22-05-2013 due to duplicate grn saving.
				/*$sql = "DELETE FROM `ware_grnheader` WHERE 
						(`intGrnNo`='$serialNo') AND (`intGrnYear`='$year')";
				$result1 = $db->RunQuery2($sql);*/
				
				$sql = "INSERT INTO `ware_grnheader` (`intGrnNo`,`intGrnYear`,`intPoNo`,intPoYear,dblExchangeRate,strInvoiceNo,SUPPLIER_INVOICE_DATE,strDeliveryNo,intStatus,intApproveLevels,datdate,dtmCreateDate,intUser,intCompanyId,`intGrnRateApproval`,WayBillNo) 
						VALUES ('$serialNo','$year','$poNo','$poYear','$exchRate','$invoiceNo','$invDate','$deliveryNo','$grnApproveLevel','$ApproveLevels','$date',now(),'$userId','$companyId','1',$waybillNo)";
				$result = $db->RunQuery2($sql);
				}//end of if(existingGrnNo=='')
/*				else{
					$rollBackFlag=1;
					$rollBackMsg="This Invoice No already exists in GRN No : ".$existingGrnNo."/".$existingGrnYear;
				}
*/		}
		if(!$result){
					$rollBackFlag=1;
					$rollBackMsg .= $db->errormsg;
 		}
		
		//-----------delete and insert to detail table-----------------------
		if(($result) && ($rollBackFlag!=1)){
		$sql = "DELETE FROM `ware_grndetails` WHERE (`intGrnNo`='$serialNo') AND (`intGrnYear`='$year')";
		$result2 = $db->RunQuery2($sql);
			$toSave=0;
			$saved=0;
			$rollBackFlag=0;
			$grnRateApproval=1;	
			$rollBackMsg ="Maximum GRN Qtys for items are..."; 
			$itemList='';
			foreach($arr as $arrVal)
			{

				$itemId 	 	= $arrVal['itemId'];
				$itemList		.=$arrVal['itemId'].",";
				$addedFlag 	 	= $arrVal['addedFlag'];
				$serialsFlag 	= $arrVal['serialsFlag'];
 				//$arr_s 		= json_decode($arrVal['arr_s'], true);
				$Qty 		 	= round($arrVal['Qty'],4);
				$FOC 		 	= round($arrVal['FOC'],4);
				if($FOC == '')
				$FOC			=0;
				$poRate 		= round($arrVal['poRate'],6);
				$grnRate 		= round($arrVal['grnRate'],6);
				$invoiceRate 	= round($arrVal['invoiceRate'],6);
				$expireDate		= $arrVal['expireDate']==""? 'Null':"'".$arrVal['expireDate']."'";

				//----------------------------
				if($rollBackFlag!=1){
					if($grnRate<=$invoiceRate){
					$invoiceRate=$grnRate;
					}
					else{
					$grnRateApproval=0;	//if atleast one items grn rate is greater than po rate
					}
					//--------------------
					
					//-------validate qty-------------
				  $sql1 = "SELECT
						sum(tb1.dblQty) as dblQty,
						
						IFNULL((SELECT
						Sum(ware_grndetails.dblGrnQty)
						FROM
						ware_grnheader
						Inner Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear
						WHERE
						ware_grnheader.intPoNo =  tb1.intPONo AND
						ware_grnheader.intPoYear =  tb1.intPOYear AND
						ware_grndetails.intItemId =  tb1.intItem AND
						ware_grnheader.intStatus  <=  ware_grnheader.intApproveLevels AND 
						ware_grnheader.intStatus > 0 ),0)
						 as totGRNQty,
						 
						IFNULL((SELECT
						Sum(ware_returntosupplierdetails.dblQty)
						FROM
						ware_returntosupplierheader
						Inner Join ware_returntosupplierdetails ON ware_returntosupplierheader.intReturnNo = ware_returntosupplierdetails.intReturnNo AND ware_returntosupplierheader.intReturnYear = ware_returntosupplierdetails.intReturnYear
						Inner Join ware_grnheader ON ware_returntosupplierheader.intGrnNo = ware_grnheader.intGrnNo AND ware_returntosupplierheader.intGrnYear = ware_grnheader.intGrnYear
						WHERE
						ware_grnheader.intPoNo =  tb1.intPONo AND
						ware_grnheader.intPoYear =   tb1.intPOYear AND
						ware_returntosupplierdetails.intItemId =  tb1.intItem AND
						ware_returntosupplierheader.intStatus <=  ware_returntosupplierheader.intApproveLevels AND 
						ware_returntosupplierheader.intStatus > 0),0) as dblRetunSupplierQty,
						  
						mst_item.strName 
						FROM
						trn_poheader 
						Inner Join trn_podetails as tb1 ON trn_poheader.intPONo = tb1.intPONo AND trn_poheader.intPOYear = tb1.intPOYear 
						Inner Join mst_item ON tb1.intItem = mst_item.intId
							WHERE 
							trn_poheader.intStatus='1' AND 
							trn_poheader.intPoNo =  '$poNo' AND
							trn_poheader.intPoYear =  '$poYear' AND 
							tb1.intItem='$itemId'";
					$result1 = $db->RunQuery2($sql1);
					while($row1=mysqli_fetch_array($result1))
					{ 
						if(round(($row1['dblQty']-$row1['totGRNQty']+$row1['dblRetunSupplierQty']),2)<round($Qty,2)){
							$rollBackFlag = 1;
							$rollBackMsg .= " </br>".$row1['strName']." = ".($row1['dblQty']-$row1['totGRNQty']+$row1['dblRetunSupplierQty'])." </br>"; 
						}
						
					}
					
					//--------------------
					################################### 
					###### get inspection item#########
					###################################
					$sql = "SELECT
								mst_item.intInspectionItem
							FROM mst_item
							WHERE
								mst_item.intId =  '$itemId'";
					$result3 = $db->RunQuery2($sql);
					$row_insp = mysqli_fetch_array($result3);
					$insp = $row_insp['intInspectionItem'];
					
					
					$sql = "INSERT INTO `ware_grndetails` (`intGrnNo`,`intGrnYear`,`intItemId`,`dblGrnQty`,`dblFocQty`,`dblPoRate`,`dblGrnRate`,`dblInvoiceRate`,`dblRetunSupplierQty`,intInspectionItem,dblInvoiceBalanceQty,EXPIRE_DATE) 
					VALUES ('$serialNo','$year','$itemId','$Qty','$FOC','$poRate','$grnRate','$invoiceRate','0','$insp','$Qty',$expireDate)";
					$result3 = $db->RunQuery2($sql);
					if($result3==1){
					$saved++;
					}
				}
				$toSave++;
					//---------------------------------
					$sql = "UPDATE `ware_grnheader` SET intGrnRateApproval ='$grnRateApproval'  
							WHERE (`intGrnNo`='$serialNo') AND (`intGrnYear`='$year')";
					$result = $db->RunQuery2($sql);
					//---------------------------------
					
				//-------------2013-10-23------
				if(($serialsFlag==1) && ($addedFlag!=1)){
					$noOfSerials=0;
					$result	=$obj_reg_get->loadDetailData_grn_fixed($serialNo,$year,'',$itemId,'','','','','','','RunQuery2');
					while($row=mysqli_fetch_array($result))
					{
 						$noOfSerials++;
					}
					if($noOfSerials==0){
						  $rollBackMsg = "Please add serials";
						  $rollBackFlag=1;
					}
				}
 				else if($addedFlag==1){
					$noOfSerials=0;
					foreach($arr_s as $arrVal_s)
					{
						$itemId_s 	 = $arrVal_s['itemId'];
						if($itemId_s==$itemId){
							$noOfSerials++;
						}
					}
 					if($noOfSerials==0){
						  $rollBackMsg = "Please add serials";
						  $rollBackFlag=1;
					}
				
					if($addedFlag==1){
						if(($result)){
						$obj_reg_set->DeleteDetails_grn_fixed($serialNo,$year,$itemId,'RunQuery2');
							foreach($arr_s as $arrVal_s)
							{
								$itemId_s 	 = $arrVal_s['itemId'];
								$grnRate = $arrVal_s['grnRate'];
								//$serials = $arrVal_s['serials'];
								$serials	= $obj_common->replace($arrVal_s['serials']);
								$depreRate	= GetDepreciationRate($itemId_s);
								if($itemId_s==$itemId){
									$response=$obj_reg_set->SaveDetails_grn_fixed($serialNo,$year,$supplierId,$itemId_s,$serials,"'".$purchaseDate."'","'".$date."'",$currency,$grnRate,0,'RunQuery2','','NULL','NULL',$companyId,'NULL','NULL',$depreRate,0);
									if($response['savedStatus']=='fail'){
										$rollBackMsg = $response['savedMassege'];
										$rollBackFlag=1;
									}
								}
							}
						}
					}
				//----------------
				}
					
					
			}//end of foreach($arr as $arrVal)
			$itemList=substr($itemList, 0, -1);
			$obj_reg_set->DeleteNotInListDetails_grn_fixed($serialNo,$year,$itemList,'RunQuery2');
			
		}//end of if($result)
 		if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= '';
		}
		else if(($savableFlag==1) &&(!$result)){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= "Header saving error".$rollBackMsg;
			$response['q'] 			= $sql;
		}
		else if(($savableFlag==1) &&($toSave==0) &&($saved==0)){
			$response['type'] 		= 'fail';
			$response['msg'] 		= "Details saving error";
			$response['q'] 			= '';
		}
		else if(($result) && ($toSave==$saved)){
			$confirmationMode=getConfirmationMode($programCode,$userId,$ApproveLevels,$grnApproveLevel);
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			if($editMode==1)
			$response['msg'] 		= 'Updated successfully.';
			else
			$response['msg'] 		= 'Saved successfully.';
			
			$response['serialNo'] 		= $serialNo;
			$response['year'] 		= $year;
			$response['confirmationMode'] = $confirmationMode;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		
		$db->CloseConnection();		
	}

//----------------------------------------
	echo json_encode($response);
//-----------------------------------------	
	function getNextGrnNo()
	{
		global $db;
		global $companyId;
		$sql = "SELECT
				sys_no.intGRNNo
				FROM sys_no
				WHERE
				sys_no.intCompanyId =  '$companyId'
				";	
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$nextGrnNo = $row['intGRNNo'];
		
		$sql = "UPDATE `sys_no` SET intGRNNo=intGRNNo+1 WHERE (`intCompanyId`='$companyId')  ";
		$db->RunQuery2($sql);
		
		return $nextGrnNo;
	}
//---------------------------------------	
	function getSaveStatus($serialNo,$year)
	{
		global $db;
		$sql = "SELECT ware_grnheader.intStatus, ware_grnheader.intApproveLevels FROM ware_grnheader WHERE (intGrnNo='$serialNo') AND (`intGrnYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
			
			if(($status==0) || ($row['intStatus']==($row['intApproveLevels']+1))){
				$editableFlag=1	;
			}
			else{
				$editableFlag=0;	
			}
			
		return $editableFlag;
	}
//---------------------------------------	
	function getPOStatus($serialNo,$year)
	{
		global $db;
	  	$sql = "SELECT
				trn_poheader.intStatus
				FROM
				trn_poheader
				WHERE
				trn_poheader.intPoNo =  '$serialNo' AND
				trn_poheader.intPoYear =  '$year'";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
			
		return $status;
	}
//-----------------------------------------------------------
	function getEditLocationValidation($type,$location,$serialNo,$year)
	{
		$flag= 1;
		
		global $db;
		  $sql = "SELECT
					ware_grnheader.intCompanyId
					FROM
					ware_grnheader
					Inner Join mst_locations ON ware_grnheader.intCompanyId = mst_locations.intId
					WHERE
					ware_grnheader.intGrnNo =  '$serialNo' AND
					ware_grnheader.intGrnYear =  '$year'";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			if($location == $row['intCompanyId'])
			$flag=0;
 			
		return $flag;

}
//------------------------------function loadConfirmatonMode-------------------
function getConfirmationMode($programCode,$intUser,$approveLevels,$status){
		global $db;
		$k=$approveLevels+2-$status;
		$sqlp = "SELECT 
		menupermision.intEdit, 
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		if($rowp['int'.$k.'Approval']==1) 
		 $confirmatonMode=1; 
		else
		 $confirmatonMode=0; 
	 
	return $confirmatonMode;
}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		$resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
		 
		// $editMode;
		 
	return $editMode;
}
//------------------------------function get MaxAppByStatus---------------------
function getMaxAppByStatus($serialNo,$year){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	  $sql = "SELECT
				max(ware_grnheader_approvedby.intStatus) as status 
				FROM
				ware_grnheader_approvedby
				WHERE
				ware_grnheader_approvedby.intGrnNo =  '$serialNo' AND
				ware_grnheader_approvedby.intYear =  '$year'";
				
		$resultm = $db->RunQuery2($sql);
		 $rowm=mysqli_fetch_array($resultm);
		 
		 return $rowm['status'];

}
//--------------------------------------------------------getMaxAppByStatus
function GetDepreciationRate($itemId)
{
	global $db;
	
	$sql = "SELECT dblDepreciationRate AS RATE
			FROM mst_item I
			INNER JOIN mst_subcategory S ON S.intId = I.intSubCategory
			WHERE I.intId = $itemId";
	$result = $db->RunQuery2($sql);
	$row	= mysqli_fetch_array($result);
	return $row['RATE'];
	
}
function get_duplicate_invoice_flag($supplierId,$invoiceNo,$serialNo,$year){
	
	global $db;
	$sql= "SELECT
	Count(ware_grnheader.strInvoiceNo) as invoices 
	FROM
	ware_grnheader
	INNER JOIN trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
	WHERE 
	ware_grnheader.intStatus	=	1 AND 
	trn_poheader.intSupplier = '$supplierId' AND
	TRIM(ware_grnheader.strInvoiceNo) = TRIM('$invoiceNo')  AND
	concat(ware_grnheader.intGrnNo,'/',ware_grnheader.intGrnYear) <>  '$serialNo/$year' 
	";	
	
	$result = $db->RunQuery2($sql);
	$row	= mysqli_fetch_array($result);
	return $row['invoices'];
	
}
?>
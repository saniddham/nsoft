<?php 
	ini_set('display_errors',0);
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	$programName='Good Received Note';

	include_once $backwardseperator."dataAccess/Connector.php";
	include $backwardseperator."libraries/mail/mail_bcc_2.php";
	require_once $backwardseperator."class/cls_mail.php";
	include_once ($backwardseperator."class/finance/cls_common_get.php");
	require_once $backwardseperator."class/finance/fixedAssets/fixedAssetsRegistry/cls_registry_get.php";
	require_once $backwardseperator."class/finance/fixedAssets/fixedAssetsRegistry/cls_registry_set.php";
	require_once $backwardseperator."class/masterData/exchange_rate/cls_exchange_rate_get.php";
    require_once $_SESSION['ROOT_PATH']."class/common/azureDBconnection.php";

 
  	$obj_reg_get	= new Cls_Registry_Get($db);
	$obj_reg_set	= new Cls_Registry_Set($db);
	$obj_exchn_get	= new cls_exchange_rate_get($db);
    $objAzure = new cls_azureDBconnection();

	
	include_once "../../../../config.php";
	$path		= MAIN_URL;
	
	$objMail = new cls_create_mail($db);
	$finance_common_get 	= new cls_common_get($db);
	//$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////

	
	$grnNo = $_REQUEST['grnNo'];
	$year = $_REQUEST['year'];
	
	if($_REQUEST['status']=='approve')
	{ 
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		$rollBackFlag=0;
		$rollBackMsg = '';

		$sql				="SELECT
								trn_poheader.intSupplier,
								trn_poheader.intPONo AS PO_NO,
								trn_poheader.intPOYear AS PO_Year,
								ware_grnheader.strInvoiceNo,
								ware_grnheader.datdate AS datdate,
								ware_grnheader.intCompanyId AS company,
								trn_poheader.intCurrency AS currency,
								mst_locations.intCompanyId headCompany
								FROM
								ware_grnheader
								INNER JOIN trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
								INNER JOIN mst_locations ON ware_grnheader.intCompanyId = mst_locations.intId
								WHERE
								ware_grnheader.intGrnNo = '$grnNo' AND
								ware_grnheader.intGrnYear = '$year'
								";
		$results = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($results);
		
		$supplierId			= $row['intSupplier'];
		$invoiceNo			= $row['strInvoiceNo'];
		$poNo               = $row['PO_NO'];
		$poYear             = $row['PO_Year'];
		$invoice_count 		= get_duplicate_invoice_flag($supplierId,$invoiceNo,$grnNo,$year);	
		if($invoice_count > 0){
			$rollBackFlag=1;
			$sqlM=$sql;
			$rollBackMsg = "Duplicate Invoice No for same Supplier";
		}
		
		$rowExchng		= $obj_exchn_get->GetAllValues($row['currency'],2,$row['datdate'],$row['headCompany'],'RunQuery2');
		  if($rowExchng['BUYING_RATE'] == '')
		  {
			 $rollBackFlag=1;
			 $rollBackMsg= 'Please enter exchange rates for '.$row['datdate'];
		  }
			
	 
		$sql = "UPDATE `ware_grnheader` SET `intStatus`=intStatus-1 WHERE (`intGrnNo`='$grnNo') AND (`intGrnYear`='$year') and intStatus not in(0,1)";
		$result = $db->RunQuery2($sql);
		if((!$result) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sql;
			$rollBackMsg = "Approval error!";
		}
		
		
		if($result){
			$sql = "SELECT ware_grnheader.intUser, 
			ware_grnheader.intStatus, 
			ware_grnheader.intApproveLevels, 
			ware_grnheader.intCompanyId AS location,
			mst_locations.intCompanyId AS companyId,
			ware_grnheader.dblExchangeRate  
			FROM
			ware_grnheader
			Inner Join mst_locations ON ware_grnheader.intCompanyId = mst_locations.intId
			WHERE (`intGrnNo`='$grnNo') AND (`intGrnYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$createdUser = $row['intUser'];
			$status = $row['intStatus'];
			$savedLevels = $row['intApproveLevels'];
			$location 	= $row['location'];
			$company = $row['companyId'];
					
			$approveLevel=$savedLevels+1-$status;
			$sqlI = "INSERT INTO `ware_grnheader_approvedby` (`intGrnNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
				VALUES ('$grnNo','$year','$approveLevel','$userId',now(),0)";
			$resultI = $db->RunQuery2($sqlI);
			if((!$resultI) && ($rollBackFlag!=1)){
				$rollBackFlag=1;
				$sqlM=$sqlI;
				$rollBackMsg = "Approval error!";
			}
		//------------------------------
		if($status==1)
		{
			if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
			////////////// send mail to creATER //////////////////
			sendFinlConfirmedEmailToUser($grnNo,$year,$objMail,$mainPath,$root_path);
			}
		}
		//------------------------------
		
		//BEGIN	- SEND NOTIFICATION EMAIL TO PO CREATED USER { 
		if($status==1)
		{
			SendNotificationMailToPOCreatedUser($grnNo,$year,$objMail,$mainPath,$root_path);
		}
		//END	- SEND NOTIFICATION EMAIL TO PO CREATED USER }
		
		if($status==1){//if final confirmation raised,insert to stock
				$sql1 = "SELECT
 						ware_grndetails.dblGrnQty,
 						ware_grndetails.dblFocQty,
						ware_grndetails.intItemId, 
						ware_grndetails.intGrnNo,
						ware_grndetails.intGrnYear,
						ware_grnheader.datdate,
						ware_grndetails.dblGrnRate ,
						mst_maincategory.FIXED_ASSET 
						FROM
						ware_grndetails 
						INNER JOIN mst_item ON ware_grndetails.intItemId = mst_item.intId
						INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
						Inner Join ware_grnheader ON ware_grndetails.intGrnNo = ware_grnheader.intGrnNo AND 
						ware_grndetails.intGrnYear = ware_grnheader.intGrnYear 
						WHERE
						ware_grndetails.intGrnNo =  '$grnNo' AND
						ware_grndetails.intGrnYear =  '$year' AND 
						ware_grndetails.intInspectionItem <> 1";
				$result1 = $db->RunQuery2($sql1);
				$toSavedQty=0;
				$transSavedQty=0;
				while($row=mysqli_fetch_array($result1))
				{
					$toSavedQty+=round($row['dblGrnQty'],4);
					$item=$row['intItemId'];
					$Qty=round($row['dblGrnQty'],4);
					$grnNo=$row['intGrnNo'];
					$grnYear=$row['intGrnYear'];
					$grnDate=$row['datdate'];
					$grnRate=$row['dblGrnRate'];
					$currency=loadCurrency($grnNo,$grnYear);
					
					//----2014-11-21--------------------
					if($row['FIXED_ASSET']==1){
						$responseFA	= addToFixedAssetRegistry($grnNo,$grnYear,$item);
						if($responseFA['rollBackFlag']==1)
						$rollBackFlag	=1;
						$sqlM			=$responseFA['sqlE'];
						$rollBackMsg 	=$responseFA['rollBackMsg'];
					}
 					//----END 2014-11-21-------------------
					$sqlI = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intUser,dtDate) 
						VALUES ('$company','$location','$grnNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$item','$Qty','GRN','$userId',now())";
					$resultI = $db->RunQuery2($sqlI);
					if($resultI){
						$transSavedQty+=$Qty;
					}
					if((!$resultI) && ($rollBackFlag!=1)){
						$rollBackFlag=1;
						$sqlM=$sqlI;
						$rollBackMsg = "Approval error!";
					}

				}
			}
			
		//BEGIN	- SEND NOTIFICATION EMAIL TO PO CREATED USER { 
		if($status==1)
		{
			//ApproveFixedAssets($grnNo,$year);
		}
		//END	- SEND NOTIFICATION EMAIL TO PO CREATED USER }
		
		//BEGIN - {
		if($status==1)
		{
			$result 		= $finance_common_get->SaveSupplierForcast($grnNo,$year);
			if(!$result['type'])
			{
				$rollBackFlag	= 1;
				$rollBackMsg 	= $result['msg'];
				$sqlM			= $result['sql'];
			}
			
		}
		//END 	- }
			//------------------------------
			if($status==$savedLevels){//if first confirmation raised,grn qty field in po table shld update
				$sql1 = "SELECT
							ware_grnheader.intPoNo,
							ware_grnheader.intPoYear,
							ware_grndetails.intItemId,
							ware_grndetails.dblGrnQty
							FROM
							ware_grndetails
							Inner Join ware_grnheader ON ware_grndetails.intGrnNo = ware_grnheader.intGrnNo AND ware_grndetails.intGrnYear = ware_grnheader.intGrnYear
							WHERE
							ware_grndetails.intGrnNo =  '$grnNo' AND
							ware_grndetails.intGrnYear =  '$year'";
				$result1 = $db->RunQuery2($sql1);
				while($row=mysqli_fetch_array($result1))
				{
					$poNo=$row['intPoNo'];
					$poYear=$row['intPoYear'];
					$item=$row['intItemId'];
					$Qty=$row['dblGrnQty'];
					
					$sql2 = "SELECT
							trn_podetails.intPONo,
							trn_podetails.intPOYear,
							trn_podetails.intItem,
							trn_podetails.dblQty,
							trn_podetails.dblGRNQty,
							trn_podetails.intPrnNo,
							trn_podetails.intPrnYear 
							FROM trn_podetails
							WHERE
							trn_podetails.intPONo =  '$poNo' AND
							trn_podetails.intPOYear =  '$poYear' AND
							trn_podetails.intItem =  '$item'";
					$result2 = $db->RunQuery2($sql2);
					while($row2=mysqli_fetch_array($result2))//same po may contain more than 1 same items.bt prn num may different.
					{
							$prnNo=$row2['intPrnNo'];
							$prnYear=$row2['intPrnYear'];
							$poQty=$row2['dblQty'];
							$grnQty=$row2['dblGRNQty'];
							$balToUpdate=$poQty-$grnQty;
							if($balToUpdate<$Qty){
								$upQty=$balToUpdate;
								$Qty=$Qty-$upQty;
							}
							else{
								$upQty=$Qty;
								$Qty=0;
							}
							$sql = "UPDATE `trn_podetails` SET 	dblGRNQty = dblGRNQty+'$upQty' 
									WHERE
									trn_podetails.intPONo =  '$poNo' AND
									trn_podetails.intPOYear =  '$poYear' AND
									trn_podetails.intItem =  '$item' AND 
									trn_podetails.intPrnNo =  '$prnNo' AND 
									trn_podetails.intPrnYear =  '$prnYear'";
							$result = $db->RunQuery2($sql);
							if((!$result) && ($rollBackFlag!=1)){
								$rollBackFlag=1;
								$sqlM=$sql;
								$rollBackMsg = "Approval error!";
							}
							
					}
					
				}
			}
			//------------------------------------------------------
		}
	
		$toSavedQty=round($toSavedQty,4);
		$transSavedQty=round($transSavedQty,4);
 		if(($toSavedQty!= $transSavedQty) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$rollBackMsg = "Approval Error";
			$sqlM=$toSavedQty."-".$transSavedQty;
		}
	//$rollBackFlag=1;
		$poStatus=getPOStatus($grnNo,$year);//check wether po not confirmed
		if($poStatus!=1){ 
			$rollBackFlag=1;
			$rollBackMsg="This PO No is not confirmed.Cant raise GRN";
		}
	
		if(($rollBackFlag==0) &&($status==1)){
		    if($company == 1) {
                updateAzureDBTables($grnNo, $year, $db, "PO_GRN");
            }
 			sendFinlConfirmedEmailToEmailConfig($grnNo,$year,$objMail,$mainPat,$root_path);
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= "Final Approval raised sucessfully";
			$response['q'] 			= $sql;
		}
		else if($rollBackFlag==0){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= "Approval raised sucessfully";
			$response['q'] 			= '';
		}
		
		else if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= $sqlM;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sqlM;
		}
		$db->CloseConnection();		
		
		//----------------------------------------
			echo json_encode($response);
		//-----------------------------------------	
		
	}
	else if($_REQUEST['status']=='reject')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		$rollBackFlag=0;
		$rollBackMsg = '';
		
		
		$sql = "SELECT ware_grnheader.intUser, 
		ware_grnheader.intStatus, 
		ware_grnheader.intApproveLevels, 
		ware_grnheader.intCompanyId AS location,
		mst_locations.intCompanyId AS companyId
		FROM
		ware_grnheader
		Inner Join mst_locations ON ware_grnheader.intCompanyId = mst_locations.intId
		WHERE (`intGrnNo`='$grnNo') AND (`intGrnYear`='$year')";
		$results = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($results);
		$createdUser = $row['intUser'];
		$status = $row['intStatus'];
		$savedLevels = $row['intApproveLevels'];
		$location 	= $row['location'];
		$company = $row['companyId'];
		
		$sql = "UPDATE `ware_grnheader` SET `intStatus`=0 WHERE (`intGrnNo`='$grnNo') AND (`intGrnYear`='$year') ";
		$result = $db->RunQuery2($sql);
		if((!$result) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sql;
			$rollBackMsg = "Rejection error!";
		}
		
		
		//$sql = "DELETE FROM `ware_grnheader_approvedby` WHERE (`intGrnNo`='$grnNo') AND (`intYear`='$year')";
		//$result2 = $db->RunQuery($sql);
		
		$sqlI = "INSERT INTO `ware_grnheader_approvedby` (`intGrnNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
			VALUES ('$grnNo','$year','0','$userId',now(),0)";
		$resultI = $db->RunQuery2($sqlI);
		if($resultI)
		{
			if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
			sendRejecedEmailToUser($grnNo,$year,$objMail,$mainPath,$root_path);
			}
		}
		if((!$resultI) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sqlI;
			$rollBackMsg = "Rejection error!";
		}
		
		//----------------------------------------------------------------------
		if($status==1){//if final confirmation  has been raised,insert - records to stock
				$sql1 = "SELECT
 						ware_grndetails.dblGrnQty,
						ware_grndetails.intItemId  
						FROM ware_grndetails
						WHERE
						ware_grndetails.intGrnNo =  '$grnNo' AND
						ware_grndetails.intGrnYear =  '$year'";
				$result1 = $db->RunQuery2($sql1);
				while($row=mysqli_fetch_array($result1))
				{
					$item=$row['intItemId'];
					$Qty=round($row['dblGrnQty'],4);
					
					$sqlI = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intItemId,dblQty,strType,intUser,dtDate) 
						VALUES ('$company','$location','$grnNo','$year','$item','-$Qty','CGRN','$userId',now())";
					$resultI = $db->RunQuery2($sqlI);
					if((!$resultI) && ($rollBackFlag!=1)){
						$rollBackFlag=1;
						$sqlM=$sqlI;
						$rollBackMsg = "Rejection error!";
					}
				}
			}
			//----------------------------------------------------------------
			if($savedLevels>=$status){//atleast one confirmation has been raised ,roll back grn qty from po table....
				$sql1 = "SELECT
							ware_grnheader.intPoNo,
							ware_grnheader.intPoYear,
							ware_grndetails.intItemId,
							ware_grndetails.dblGrnQty
							FROM
							ware_grndetails
							Inner Join ware_grnheader ON ware_grndetails.intGrnNo = ware_grnheader.intGrnNo AND ware_grndetails.intGrnYear = ware_grnheader.intGrnYear
							WHERE
							ware_grndetails.intGrnNo =  '$grnNo' AND
							ware_grndetails.intGrnYear =  '$year'";
				$result1 = $db->RunQuery2($sql1);
				while($row=mysqli_fetch_array($result1))
				{
					$poNo=$row['intPoNo'];
					$poYear=$row['intPoYear'];
					$item=$row['intItemId'];
					$Qty=$row['dblGrnQty'];
					
					$sql2 = "SELECT
							trn_podetails.intPONo,
							trn_podetails.intPOYear,
							trn_podetails.intItem,
							trn_podetails.dblQty,
							trn_podetails.dblGRNQty,
							trn_podetails.intPrnNo,
							trn_podetails.intPrnYear 
							FROM trn_podetails
							WHERE
							trn_podetails.intPONo =  '$poNo' AND
							trn_podetails.intPOYear =  '$poYear' AND
							trn_podetails.intItem =  '$item'";
					$result2 = $db->RunQuery2($sql2);
					while($row2=mysqli_fetch_array($result2))//same po may contain more than 1 same items.bt prn no may different.
					{
							$prnNo=$row2['intPrnNo'];
							$prnYear=$row2['intPrnYear'];
							$poQty=$row2['dblQty'];
							$grnQty=$row2['dblGRNQty'];
							if($grnQty>$Qty){
								$upQty=$grnQty-$Qty;
								$Qty=0;
							}
							else{
								$upQty=0;
								$Qty=$Qty-$grnQty;
							}
							$sql = "UPDATE `trn_podetails` SET 	dblGRNQty = '$upQty' 
									WHERE
									trn_podetails.intPONo =  '$poNo' AND
									trn_podetails.intPOYear =  '$poYear' AND
									trn_podetails.intItem =  '$item' AND 
									trn_podetails.intPrnNo =  '$prnNo' AND 
									trn_podetails.intPrnYear =  '$prnYear'";
							$result = $db->RunQuery2($sql);
							if((!$result) && ($rollBackFlag!=1)){
								$rollBackFlag=1;
								$sqlM=$sql;
								$rollBackMsg = "Rejection error!";
							}
					}
					
				}
			}
		//------------------------------------
		if($rollBackFlag==0){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= "Rejection raised sucessfully";
			$response['q'] 			= '';
		}
		else if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= $sqlM;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sqlM;
		}
		$db->CloseConnection();		
		
		//----------------------------------------
			echo json_encode($response);
		//-----------------------------------------	
				
	}
	
	else if($_REQUEST['status']=='approveGRNRates')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		$rollBackFlag=0;
		$rollBackMsg = '';
		
		
		$sql = "UPDATE `ware_grnheader` SET `intGrnRateApproval`='1',`intGrnRateApprovedBy`='$userId',`intGrnRateApprovedDate`=now()   WHERE (`intGrnNo`='$grnNo') AND (`intGrnYear`='$year')";
		$result = $db->RunQuery2($sql);
		if((!$result) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sql;
			$rollBackMsg = "Rejection error!";
		}
		
		$sql = "UPDATE `ware_grndetails` SET `dblInvoiceRate`=dblGrnRate  WHERE (`intGrnNo`='$grnNo') AND (`intGrnYear`='$year')";
		$result = $db->RunQuery2($sql);
		if((!$result) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sql;
			$rollBackMsg = "Rejection error!";
		}
		
		//------------------------------------
		if($rollBackFlag==0){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= "GRN Rate Approved sucessfully";
			$response['q'] 			= '';
		}
		else if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= $sqlM;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sqlM;
		}
		$db->CloseConnection();		
		
		//----------------------------------------
			echo json_encode($response);
		//-----------------------------------------	
	}




//--------------------------------------------------------------
function loadCurrency($grnNo,$grnYear)
{
	global $db;
	 $sql = "SELECT
			trn_poheader.intCurrency
			FROM
			ware_grnheader
			Inner Join trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
			WHERE
			ware_grnheader.intGrnNo =  '$grnNo' AND
			ware_grnheader.intGrnYear =  '$grnYear'";

	$result = $db->RunQuery2($sql);
	$row=mysqli_fetch_array($result);
	return $row['intCurrency'];;	
}
//--------------------------------------------------------
function sendFinlConfirmedEmailToUser($serialNo,$year,$objMail,$mainPat,$root_path){
	global $db;
				
		    $sql = "SELECT
			sys_users.strFullName,
			sys_users.strEmail 
			FROM 
			ware_grnheader
			Inner Join sys_users ON ware_grnheader.intUser = sys_users.intUserId
			WHERE
			ware_grnheader.intGrnNo =  '$serialNo' AND
			ware_grnheader.intGrnYear =  '$year'";

				
		 $result = $db->RunQuery2($sql);
		 
		$row=mysqli_fetch_array($result);
			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="CONFIRMED GRN ('$serialNo'/'$year')"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST = NULL;
			
			$_REQUEST['program']='GRN';
			$_REQUEST['field1']='GRN No';
			$_REQUEST['field2']='GRN Year';
			$_REQUEST['field3']='';
			$_REQUEST['field4']='';
			$_REQUEST['field5']='';
			$_REQUEST['value1']=$serialNo;
			$_REQUEST['value2']=$year;
			$_REQUEST['value3']='';
			$_REQUEST['value4']='';
			$_REQUEST['value5']='';
			
			$_REQUEST['subject']="CONFIRMED GRN ('$serialNo'/'$year')";
			
			$_REQUEST['statement1']="Approved this";
			$_REQUEST['statement2']="to view this";
			$_REQUEST['link']=base64_encode($_SERVER['REMOTE_ADDR'].dirname($_SERVER['PHP_SELF'])."?q=895&grnNo=$serialNo&year=$year&approveMode=0"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}

function SendNotificationMailToPOCreatedUser($serialNo,$year,$objMail,$mainPat,$root_path)
{
	global $db;
				
	$sql = "SELECT
			  U.strEmail    	AS EMAIL,
			  U.strFullName 	AS FULL_NAME,
			  PH.intPONo		AS PO_NO,
			  PH.intPOYear		AS PO_YEAR,
			  GH.strInvoiceNo	AS INVOICE_NO
			FROM ware_grnheader GH
			  INNER JOIN trn_poheader PH
				ON PH.intPONo = GH.intPoNo
				  AND PH.intPOYear = GH.intPoYear
			  INNER JOIN sys_users U
				ON U.intUserId = PH.intUser
			WHERE GH.intGrnNo = $serialNo
				AND GH.intGrnYear = $year";			
	$result = $db->RunQuery2($sql);		 
	$row	= mysqli_fetch_array($result);
	$enterUserName 			= $row['FULL_NAME'];
	$enterUserEmail 		= $row['EMAIL'];
	$header					= "GRN NOTIFICATION ('$serialNo'/'$year')"; 
	$_REQUEST 				= NULL;
	$_REQUEST['program']	= 'Confirmed GRN Notification';
	$_REQUEST['field1']		= 'GRN No';
	$_REQUEST['field2']		= 'PO No';
	$_REQUEST['field3']		= 'INVOICE';
	$_REQUEST['field4']		= '';
	$_REQUEST['field5']		= '';
	$_REQUEST['value1']		= $serialNo.'/'.$year;
	$_REQUEST['value2']		= $row["PO_NO"].'/'.$row["PO_YEAR"];
	$_REQUEST['value3']		= $row["INVOICE_NO"];
	$_REQUEST['value4']		= '';
	$_REQUEST['value5']		= '';	
	$_REQUEST['subject']	= "GRN NOTIFICATION ('$serialNo'/'$year')";
	
	$_REQUEST['statement1']	= "";
	$_REQUEST['statement2']	= "to view this";
	$_REQUEST['link']		= base64_encode($_SERVER['REMOTE_ADDR'].dirname($_SERVER['PHP_SELF'])."?q=895&grnNo=$serialNo&year=$year&approveMode=0"); 
	$path					= $root_path."presentation/mail_approval_template.php";
	
	$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);
}

function sendRejecedEmailToUser($serialNo,$year,$objMail,$mainPat,$root_path){
	global $db;
				
		    $sql = "SELECT
			sys_users.strFullName,
			sys_users.strEmail 
			FROM 
			ware_grnheader
			Inner Join sys_users ON ware_grnheader.intUser = sys_users.intUserId
			WHERE
			ware_grnheader.intGrnNo =  '$serialNo' AND
			ware_grnheader.intGrnYear =  '$year'";

				
		 $result = $db->RunQuery2($sql);
		 
		$row=mysqli_fetch_array($result);
			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="REJECTED GRN ('$serialNo'/'$year')"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST = NULL;
			
			$_REQUEST['program']='GRN';
			$_REQUEST['field1']='GRN No';
			$_REQUEST['field2']='GRN Year';
			$_REQUEST['field3']='';
			$_REQUEST['field4']='';
			$_REQUEST['field5']='';
			$_REQUEST['value1']=$serialNo;
			$_REQUEST['value2']=$year;
			$_REQUEST['value3']='';
			$_REQUEST['value4']='';
			$_REQUEST['value5']='';
			
			$_REQUEST['subject']="REJECTED GRN ('$serialNo'/'$year')";
			
			$_REQUEST['statement1']="Rejected this";
			$_REQUEST['statement2']="to view this";
			$_REQUEST['link']=base64_encode($_SERVER['REMOTE_ADDR'].dirname($_SERVER['PHP_SELF'])."?q=895&grnNo=$serialNo&year=$year&approveMode=0"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}
function getPOStatus($serialNo,$year)
	{
		global $db;
	  	$sql = "SELECT
trn_poheader.intStatus
FROM
ware_grnheader
INNER JOIN trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
WHERE
ware_grnheader.intGrnNo = '$serialNo' AND
ware_grnheader.intGrnYear = '$year'";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
			
		return $status;
	}

function ApproveFixedAssets($grnNo,$year)
{
	global $db;
	
	$sql = "UPDATE fixed_assets_registry
			SET STATUS = 1
			WHERE GRN_NO = '$grnNo'
				AND GRN_YEAR = '$year'";
	$db->RunQuery2($sql);
}
//--------------------------------------------------------
function get_duplicate_invoice_flag($supplierId,$invoiceNo,$serialNo,$year){
	
	global $db;
	$sql= "SELECT
	Count(ware_grnheader.strInvoiceNo) as invoices 
	FROM
	ware_grnheader
	INNER JOIN trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
	WHERE 
	ware_grnheader.intStatus	=	1 AND 
	trn_poheader.intSupplier = '$supplierId' AND
	ware_grnheader.strInvoiceNo = '$invoiceNo'  AND
	concat(ware_grnheader.intGrnNo,'/',ware_grnheader.intGrnYear) <>  '$serialNo/$year' 
	";	
	
	$result = $db->RunQuery2($sql);
	$row	= mysqli_fetch_array($result);
	return $row['invoices'];
	
}

function sendFinlConfirmedEmailToEmailConfig($serialNo,$year,$objMail,$mainPat,$root_path){
	global $db;
	global $location;
 		
		$nowDate 		= date('Y-m-d');
  		$mailHeader		= "CONFIRMED GRN ('$serialNo'/'$year')";
		$FROM_NAME		= "NSOFT";
		$FROM_EMAIL		= 'nsoft@screenlineholdings.com';
  		
		//----get supplier---------------------
			$sql = "SELECT
					mst_supplier.strEmail,
					mst_supplier.intCountryId as country_supplier 
 					FROM
					ware_grnheader
					INNER JOIN trn_poheader ON ware_grnheader.intPONo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
					INNER JOIN mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
					WHERE
					ware_grnheader.intGrnNo = '$serialNo' AND
					ware_grnheader.intGrnYear = '$year' AND 
					mst_supplier.intTypeId IN (3,4,14) 
					";
			$result = $db->RunQuery2($sql);
 			$row=mysqli_fetch_array($result);
			$toEmails 		= $row['strEmail'];
			$country_sup	= $row['country_supplier'];
		//----get mail users---------------------
			$sql1 = "SELECT
						sys_users.strEmail,sys_users.strFullName
					FROM
					sys_mail_eventusers
						Inner Join sys_users ON sys_users.intUserId = sys_mail_eventusers.intUserId
					WHERE 
						sys_mail_eventusers.intCompanyId = '$location' AND 
						sys_mail_eventusers.intMailEventId =  '1018'
					";
			$result1 = $db->RunQuery2($sql1);
			$cc = '';
			while($row1=mysqli_fetch_array($result1))
			{
					$cc .= $row1['strEmail'].',';
			}
			
			//------------
			if( $country_sup != '204'){//if oversease supplier
				$sql2 = "SELECT
							sys_users.strEmail,sys_users.strFullName
						FROM
						sys_mail_eventusers
							Inner Join sys_users ON sys_users.intUserId = sys_mail_eventusers.intUserId
						WHERE 
							sys_mail_eventusers.intCompanyId = '$location' AND 
							sys_mail_eventusers.intMailEventId =  '1019'
						";
				$result2 = $db->RunQuery2($sql2);
				while($row2=mysqli_fetch_array($result2))
				{
						$cc .= $row2['strEmail'].',';
				}
			}
	
 		ob_start();
		
		$grnNo=$serialNo;
		$year=$year;
			include 'rptGrn_mail.php'; 
  		$body 			= ob_get_clean();
 		//---------
		
		$bcc 		  = 'roshan@screenlineholdings.com';
		
		
		if($toEmails != ''){
			sendMessage2($FROM_EMAIL,$FROM_NAME,$toEmails,$mailHeader,$body,$cc,$bcc);
		}
	
 
}

function addToFixedAssetRegistry($grnNo,$grnYear,$itemId){
	
	global $db;
	global $obj_reg_get;
	global $obj_reg_set;
	
	$noOfSerials=0;

 		$obj_reg_set->DeleteDetails_orginal_fixed($grnNo,$grnYear,$itemId,'RunQuery2');
		$result	=$obj_reg_get->loadDetailData_saved_in_grn($grnNo,$grnYear,'',$itemId,'','','','','','','RunQuery2');
		while($row=mysqli_fetch_array($result))
		{		

				$noOfSerials++;
				$supplierId 	 	= $row['SUPPLIER_ID'];
				$itemId 	 		= $row['ITEM_ID'];
				$serialNo 	 		= $row['SERIAL_NO'];
				$purchaseDate 	 	= $row['PURCHASE_DATE'];
				$grnDate 	 		= $row['GRN_DATE'];
				$price 	 			= $row['ITEM_PRICE'];
				$location 	 		= $row['LOCATION_ID'];
				//$deparment 	 		= $row['DEPARTMENT_ID'];
				$depreciateRate		= $row['DEPRECIATE_RATE'];
				$currency			= $row['CURRENCY'];

 					$response=$obj_reg_set->SaveDetails_orginal_fixed($grnNo,$grnYear,$supplierId,$itemId,$serialNo,$purchaseDate,$grnDate,$currency,$price,0,'RunQuery2','','NULL','NULL',$location,'NULL','NULL',$depreciateRate,0);
  					//print_r($response);
					$fixed_asset_id= $response['serial_no'];
					if($response['savedStatus']=='fail'){
						$rollBackMsg = $response['savedMassege'];
						$rollBackFlag=1;
						$sqlE		= $response['error_sql'];
					}//record to depreciation table
					else{
						$response2=$obj_reg_set->SaveDetails_depriciation_fixed($fixed_asset_id,$grnNo,$grnYear,$grnDate,$itemId,$serialNo,$currency,$price,$depreciateRate,$price,0,$price,'SYSTEM_GRN','RunQuery2');
						if($response2['savedStatus']=='fail'){
							$rollBackMsg = $response2['savedMassege'];
							$rollBackFlag=1;
							$sqlE		= $response2['error_sql'];
						}
					}
					
 			}
  	
 	if($noOfSerials==0){
		  $rollBackMsg = "Please add serials";
		  $rollBackFlag=1;
	}
	
	
	$response['rollBackFlag']=$rollBackFlag;
	$response['rollBackMsg']=$rollBackMsg;
	$response['sqlE']=$sqlE;
	
	return $response;
 //----------------
 	
}

function updateAzureDBTables($grnNo, $grnYear, $db, $transactionType)
{
    global $objAzure;
    $NBT_taxcodes = array(2,3,22);
    $environment = $objAzure->getEnvironment();
    $headerTable = 'PurchaseHeader'.$environment;
    $lineTable = 'PurchaseLine'.$environment;

    $azure_connection = $objAzure->connectAzureDB();
    $sql_select = "SELECT
	mst_supplier.strName AS supplier,
	mst_supplier.strCode AS supplier_code,
	mst_financecurrency.strCode AS currency_code,
	trn_poheader.intPaymentTerm AS payTerm,
	trn_poheader.intPaymentMode AS payMode,
	trn_poheader.intShipmentTerm AS shipmentTerm,
	trn_poheader.intShipmentMode AS shipmentMode,
	trn_poheader.dtmDeliveryDate AS deliveryDate,
	trn_poheader.strRemarks AS narration,
	mst_companies.strName AS delToCompany,
	mst_locations.intId AS delToLocation,
	trn_poheader.dtmPODate AS orderDate,
	trn_poheader.intStatus,
	trn_poheader.PRINT_COUNT,
	trn_poheader.intApproveLevels,
	trn_poheader.intUser,
	ware_grnheader.intCompanyId AS poRaisedLocationId,
	sys_users.strUserName,
	trn_poheader.intReviseNo,
	trn_poheader.PO_TYPE,
	CONCAT(ware_grnheader.intGrnNo,'/',ware_grnheader.intGrnYear) AS receiptNo,
	CONCAT(trn_poheader.intPONo,'/',trn_poheader.intPOYear) AS poString,
	ware_grnheader.datdate AS postingDate	
FROM
	trn_poheader
INNER JOIN mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
INNER JOIN mst_locations ON trn_poheader.intDeliveryTo = mst_locations.intId
INNER JOIN mst_companies ON mst_locations.intCompanyId = mst_companies.intId
INNER JOIN sys_users ON trn_poheader.intUser = sys_users.intUserId
INNER JOIN ware_grnheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
INNER JOIN mst_financecurrency ON mst_financecurrency.intId = trn_poheader.intCurrency
WHERE
	ware_grnheader.intGrnNo = '$grnNo'
AND ware_grnheader.intGrnYear = '$grnYear'";

    $result = $db->RunQuery2($sql_select);
    while ($row = mysqli_fetch_array($result)) {
        $supplier = $row['supplier'];
        $supplier_code = $row['supplier_code'];
        $currency = ($row['currency_code'] == 'EURO')?"Eur":($row['currency_code'] == 'LKR'?"":$row['currency_code']);
        $payTerm = $row['payTerm'];
        $payMode = $row['payMode'];
        $shipmentMode = $row['shipmentMode'];
        $deliveryDate = $row['deliveryDate'];
        $locationId = $row['poRaisedLocationId'];
        $orderDate = $row['orderDate'];
        $receiptNo = $row['receiptNo'];
        $pOBy = $row['strUserName'];
        $remarks = $row['strRemarks'];
        $intStatus = $row['intStatus'];
        $savedLevels = $row['intApproveLevels'];
        $user = $row['intUser'];
        $intCreatedUser = $row['intUser'];
        $reviseNo = $row['intReviseNo'];
        $PRINT_COUNT = $row['PRINT_COUNT'];
        $po_type = $row['shipmentTerm'];
        $old_poString = $row['poString'];
        $narration = trim($row['narration']);
        $narration_sql = $db->escapeString($narration);
        $narration = str_replace("'", '', $narration);
        //$narration_sql = $db->escapeString($narration);
        $successHeader = '0';

        $response = getLatestPODetails($old_poString,$db);
        $poString = $response['new_poString'];
        $version_no = $response['version_no'];
        $postingDate = date('Y-m-d H:i:s');

                $sql = "SELECT
                  CONCAT(ware_grndetails.intGrnNo,'/',ware_grndetails.intGrnYear) AS receipt_no,
                  ware_grndetails.intItemId,
                  ware_grndetails.dblGrnQty AS dblGrnQty,
                  trn_podetails.SVAT_ITEM,
                  round(trn_podetails.dblUnitPrice,6) AS dblUnitPrice,
                    sum(trn_podetails.dblQty) AS dblQty,
                    trn_podetails.intTaxCode,
                    trn_podetails.dblDiscount,
                    mst_financetaxgroup.strCode AS taxCode,
                    mst_item.strCode AS itemCode,
                    mst_item.strName AS itemName,
                    mst_maincategory.strName AS mainCategory,
                    mst_subcategory.strName AS subCategory
                    FROM
                    ware_grndetails
                INNER JOIN ware_grnheader ON ware_grndetails.intGrnNo = ware_grnheader.intGrnNo
                AND ware_grndetails.intGrnYear = ware_grnheader.intGrnYear
                INNER JOIN trn_podetails ON ware_grnheader.intPoNo = trn_podetails.intPONo
                AND ware_grnheader.intPoYear = trn_podetails.intPOYear
                AND ware_grndetails.intItemId = trn_podetails.intItem
                LEFT JOIN mst_financetaxgroup ON trn_podetails.intTaxCode = mst_financetaxgroup.intId
                INNER JOIN mst_item ON ware_grndetails.intItemId = mst_item.intId
                INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
                INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
                WHERE
                    ware_grndetails.intGrnNo = '$grnNo'
                AND ware_grndetails.intGrnYear = '$grnYear'
                GROUP BY
                ware_grndetails.intItemId
                HAVING
                    dblQty > 0";

        $result_data = $db->RunQuery2($sql);
        $i = 0;
        while ($row_data = mysqli_fetch_array($result_data)){
            $line_no = $row_data['intItemId'];
            $item_code = $row_data['itemCode'];
            $description = $row_data['itemName'];
            $description = str_replace("'","''",$description);
            $item_category = $row_data['mainCategory'];
            $item_subcategory = $row_data['subCategory'];
            $qty = round($row_data['dblGrnQty'],5);
            $tax_code = $row_data['intTaxCode'];
            $discount = $row_data['dblDiscount'];
            $unit_price = round($row_data['dblUnitPrice'] * (100-$discount)/100 ,5);
            $successDetails = '0';
            $i++;
            if(in_array($tax_code,$NBT_taxcodes) ||  ($item_code == 'SERFNBT' && $tax_code != 0)){
                $tax_amount = calculateNewTaxAmount($tax_code,$qty,$unit_price,$discount,$db);
                $tax_amount = round($tax_amount, 2);
            }
            else{
                $tax_amount = 0;
            }
            $vat_group = ($tax_code != 0)?"VAT":"NO_VAT";

            $sql_azure_details = "INSERT into $lineTable (Transaction_type, Document_Type, Purchase_Order_No, Revised_Count, Line_No, Reciept_No, Item_Code, Narration, Item_Category, Item_SubCategory, Quantity, Unit_Cost, VAT_Prod_Posting_Group, NBT_Amount) VALUES ('$transactionType', 'Order','$poString', '$reviseNo','$line_no','$receiptNo','$item_code','$description','$item_category','$item_subcategory','$qty','$unit_price','$vat_group','$tax_amount')";
            if($azure_connection) {
                $getResults = $objAzure->runQuery($azure_connection, $sql_azure_details);
                if($getResults != FALSE){
                    $successDetails = '1';
                }
            }
            $sql_db_details = "INSERT into trn_financemodule_purchaseline (Transaction_type, Purchase_Order_No, Revised_Count, Line_No, Receipt_No, Item_Code, Narration, Item_Category, Item_SubCategory, Quantity, Unit_Cost, VAT_Prod_Posting_Group, NBT_Amount, deliveryStatus, deliveryDate) VALUES ('$transactionType','$poString', '$reviseNo','$line_no', '$receiptNo','$item_code', '$description','$item_category','$item_subcategory','$qty','$unit_price','$vat_group', '$tax_amount', '$successDetails', NOW())";
            $result_db_details = $db->RunQuery2($sql_db_details);
        }
        $sql_azure_header = "INSERT INTO $headerTable (Transaction_type, Document_Type, Receipt_No, Purchase_Order_No, Revised_Count, Vendor_Code, Vendor_Name, Posting_Date, Order_Date, Revised_Date,  Currency_Code, Payment_Term, Payment_Method, Shipment_Mode, Delivery_Date, Location_Code, Narration, PO_Type, No_of_Lines) VALUES ('$transactionType', 'Order', '$receiptNo', '$poString', '$reviseNo', '$supplier_code', '$supplier', '$postingDate', '$orderDate', '', '$currency', '$payTerm', '$payMode', '$shipmentMode','$deliveryDate','$locationId',CONVERT(varchar(250),'$narration'), '$po_type', '$i')";
        if($azure_connection) {
            $getResultsHeader = $objAzure->runQuery($azure_connection, $sql_azure_header);
            if($getResultsHeader != FALSE){
                $successHeader = '1';
            }
        }
        $sql_db_header = "INSERT INTO trn_financemodule_purchaseheader (Transaction_type, Document_Type, Receipt_No, Purchase_Order_No, Revised_Count, Vendor_Code, Vendor_Name, Posting_Date, Order_Date, Revised_Date, Currency_Code, Payment_Term, Payment_Method, Shipment_Mode, Delivery_Date, Location_Code, Narration, PO_Type, No_of_Lines, version_no, deliveryStatus, deliveryDate) VALUES ('$transactionType', 'Order','$receiptNo', '$poString', '$reviseNo','$supplier_code', '$supplier', '$postingDate', '$orderDate', '', '$currency', '$payTerm', '$payMode', '$shipmentMode','$deliveryDate','$locationId','$narration_sql', '$po_type', '$i', '$version_no', '$successHeader', NOW())";
        $result_db_header = $db->RunQuery2($sql_db_header);
    }

}

function getLatestPODetails($poString,$db){
    $version_no = 0;
    $new_poString = $poString;
    $sql = "SELECT MAX(version_no) AS version_no, Purchase_Order_No FROM `trn_financemodule_purchaseheader` WHERE `Purchase_Order_No` LIKE '%$poString%' GROUP BY Purchase_Order_No ORDER BY version_no desc limit 1";
    $result = $db->RunQuery2($sql);
    while($row = mysqli_fetch_array($result)){
        $version_no = $row['version_no'];
        $new_poString = $row['Purchase_Order_No'];
    }
    $response = array();
    $response['version_no'] = $version_no;
    $response['new_poString'] = $new_poString;
    return $response;
}

function calculateNewTaxAmount($taxId, $quantity, $unitPrice, $discount, $db){
    $amount=$quantity*$unitPrice;
    $amount = $amount * (100 - $discount) / 100;
    $sql = "SELECT
				mst_financetaxgroup.intId,
				mst_financetaxgroup.strProcess
				FROM
				mst_financetaxgroup
				WHERE
				mst_financetaxgroup.intId =  '$taxId'";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $taxProcess = $row['strProcess'];
    $arrTax = explode("/", $taxProcess);
    $operation = '';

    //(3)capture calculation method(isolated/inclusive/exclusive) and the array 'jsonTaxCode' contains only tax ids
    $jsonTaxCode = "[ ";
    if (count($arrTax) == 1)//If strProcess contain only one value (Example $row['strProcess'] = 5)
    {
        $operation = 'Isolated';
        $jsonTaxCode .= '{ "taxId":"' . $taxProcess . '"}';
    } else if (count($arrTax) > 1) //IF there are multiple tax ids in the tax group(example strProcess = {4/Inclusive/7})
    {
        $operation = $arrTax[1];//this should be inclusive/exclusive
        for ($i = 0; $i < count($arrTax); $i = $i + 2) {
            $jsonTaxCode .= '{ "taxId":"' . $arrTax[$i] . '"},'; //create a json array geting 0 and 2 value from the array
        }

        $jsonTaxCode = $jsonTaxCode . substr(0, count($jsonTaxCode) - 1);
    }
    $jsonTaxCode .= " ]";
    $taxCodes = json_decode($jsonTaxCode, true);

    //(4)get tax rates for all tax ids in the tax group ( tax array)
    if (count($taxCodes) != 0) {
        foreach ($taxCodes as $taxCode) {
            //get tax rates from the mst_financetaxisolated table sending taxId to the callTaxValue function which got by json array
            $codeValues[] = callTaxValue($taxCode['taxId'],$db);
        }
    }

    if (count($codeValues) > 1) // if there are more than one tax types in the tax group (this can be identified from mst_financetaxgroup.strProcess field)
    {
        if ($operation == 'Inclusive') {
            //step 1: po amount will be multiplied by the first tax rate
            //step 2 : result of the step 1(po amount+tax ammount), will be multiplied by the second tax rate.
            $firstVal = ($amount * $codeValues[0]) / 100;
            $withTaxVal = $firstVal + ((($amount + $firstVal) * $codeValues[1]) / 100);
            $val1 = ($amount * $codeValues[0]) / 100;
            $val2 = ((($amount + $firstVal) * $codeValues[1]) / 100);
        } else if ($operation == 'Exclusive') {
            //get the summation of the two tax rates and multiply it from the amount
            $withTaxVal = ($amount * ($codeValues[0] + $codeValues[1])) / 100;
            $val1 = ($amount * $codeValues[0]) / 100;
            $val2 = ($amount * $codeValues[1]) / 100;
        }
    }
    else if(count($codeValues) == 1 && $operation == 'Isolated')//there is only one tax type for the tax group
    {
        $withTaxVal = ($amount*$codeValues[0])/100;
        $val1 = ($amount*$codeValues[0])/100;
    }

    return $withTaxVal;
}

function callTaxValue($taxId, $db)
{
    $sql = "SELECT
				mst_financetaxisolated.intId,
				mst_financetaxisolated.strCode,
				mst_financetaxisolated.dblRate
				FROM
				mst_financetaxisolated
				WHERE
				mst_financetaxisolated.intId = '$taxId'
				";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $taxVal = $row['dblRate'];
    return $taxVal;
}


?>
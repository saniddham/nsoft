<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
session_start();
$backwardseperator = "../../../../";
$companyId = $_SESSION['CompanyID'];
$intUser  = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
include  	"{$backwardseperator}dataAccess/Connector.php";

$grnNo = $_REQUEST['grnNo'];
$year = $_REQUEST['year'];
$approveMode = $_REQUEST['approveMode'];

$programName='Good Received Note';
$programCode='P0224';


//$grnNo = '100000';
//$year = '2012';
//$approveMode=1;

 $sql = "SELECT
mst_companies.strName as delToCompany,
mst_locations.strName as delToLocation ,
ware_grnheader.datdate,
ware_grnheader.intPoNo,
ware_grnheader.intPoYear,
ware_grnheader.strInvoiceNo,
ware_grnheader.strDeliveryNo,
ware_grnheader.intStatus,
ware_grnheader.intApproveLevels,
ware_grnheader.intUser,
ware_grnheader.intCompanyId as poRaisedLocationId, 
sys_users.strUserName, 
ware_grnheader.intGrnRateApproval 
FROM
ware_grnheader 
Inner Join mst_locations ON ware_grnheader.intCompanyId = mst_locations.intId
Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId 
Inner Join sys_users ON ware_grnheader.intUser = sys_users.intUserId
WHERE
ware_grnheader.intGrnNo =  '$grnNo' AND
ware_grnheader.intGrnYear =  '$year'";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$company = $row['delToCompany'];
					$location = $row['delToLocation'];
					$locationId = $row['poRaisedLocationId'];//this locationId use in report header(reportHeader.php)--------------------
					$poNo = $row['intPoNo'];
					$poYear = $row['intPoYear'];
					$invoiceNo = $row['strInvoiceNo'];
					$deliveryNo = $row['strDeliveryNo'];
					$grnDate = $row['datdate'];
					$grnBy = $row['strUserName'];
					$intStatus = $row['intStatus'];
					$savedLevels = $row['intApproveLevels'];
					$rateApproval = $row['intGrnRateApproval'];					
					$user = $row['intUser'];
				 }
				 

?>
<head>
<title>Goods Recieve Rate Approval Report</title>
<script type="application/javascript" src="presentation/warehouse/grn/listing/rptGrn-js.js"></script>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:252px;
	top:173px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<?php
 if($rateApproval==0)//pending
{
?>
<div id="apDiv1"><img src="images/pending.png"  /></div>
<?php
}
?>
<form id="frmGRNApprovalReport" name="frmGRNApprovalReport" method="post">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include 'reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>GOODS RECIEVE RATE APPROVAL NOTE</strong><strong></strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
  <tr>
    <td colspan="9" align="center" bgcolor="#FFDFCB">
    <?php
	 $userPermission=1;
	//--------------	
	?>
    <?php if(( $userPermission==1) && ($rateApproval==0) && ($approveMode==1))  { ?>
    <img src="images/approve.png" align="middle" class="noPrint mouseover" id="imgApproveRate" />
    <?php
	}
	?></td>
  </tr>
  <tr>
  <?php
 	if($rateApproval==1)
	{
	?>
   <td colspan="9" class="APPROVE" >CONFIRMED</td>
   <?PHP
	}
	?>
  </tr>
   <?php
   //-----------------
	  $sqlc = "SELECT
			ware_grnheader_approvedby.intApproveUser,
			ware_grnheader_approvedby.dtApprovedDate,
			sys_users.strUserName as UserName,
			ware_grnheader_approvedby.intApproveLevelNo
			FROM
			ware_grnheader_approvedby
			Inner Join sys_users ON ware_grnheader_approvedby.intApproveUser = sys_users.intUserId
			WHERE
			ware_grnheader_approvedby.intGrnNo =  '$grnNo' AND
			ware_grnheader_approvedby.intYear =  '$year' AND
			ware_grnheader_approvedby.intApproveLevelNo =  '$intStatus'
";
	 $resultc = $db->RunQuery($sqlc);
	 $rowc=mysqli_fetch_array($resultc);
	 $rejectBy=$rowc['UserName'];
	 $rejectDate=$rowc['dtApprovedDate'];

   //-----------------
   ?>
  <tr>
    <td width="1%">&nbsp;</td>
    <td width="11%"><span class="normalfnt"><strong>GRN No</strong></span></td>
    <td width="2%" align="center" valign="middle"><strong>:</strong></td>
    <td width="17%"><span class="normalfnt"><div id="divGrnNo"><?php echo $grnNo ?>/<?php echo $year ?></div></span></td>
    <td width="11%" class="normalfnt"><strong>PO NO</strong></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="12%"><span class="normalfnt"><?php echo $poNo ?>/<?php echo $poYear ?></span></td>
    <td width="13%"><div id="divPoNo" style="display:none"><?php echo $poNo ?>/<?php echo $year ?></div></td>
  <td width="13%"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Delivery No</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $deliveryNo ?></span></td>
    <td><span class="normalfnt"><strong>Invoice No</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $invoiceNo  ?></span></td>
    
    <?php if($intStatus==0){
					  $sqlc = "SELECT
							ware_grnheader_approvedby.intApproveUser,
							ware_grnheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName,
							ware_grnheader_approvedby.intApproveLevelNo
							FROM
							ware_grnheader_approvedby
							Inner Join sys_users ON ware_grnheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							ware_grnheader_approvedby.intGrnNo =  '$grnNo' AND
							ware_grnheader_approvedby.intYear =  '$year' AND
							ware_grnheader_approvedby.intApproveLevelNo =  '0'
";
					 $resultc = $db->RunQuery($sqlc);
					 $rowc=mysqli_fetch_array($resultc);
					 $rejectBy=$rowc['UserName'];
					 $rejectDate=$rowc['dtApprovedDate'];
	}
 	?>
    
    <td class="normalfnt"><?php if($intStatus==0){?><strong>Rejected Date</strong><?php } ?></td>
    <td><?php if($intStatus==0){?><strong>:</strong>&nbsp;<span class="normalfnt"><?php echo $rejectDate ?></span><?php } ?></td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Date</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $grnDate  ?></span></td>
    <td><span class="normalfnt"><strong> GRN By</strong></span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $grnBy  ?></span></td>
    <td><span class="normalfnt"><?php if($intStatus==0){?><strong>Rejected By</strong><?php }?></span></td>
    <td><?php if($intStatus==0){?><strong>:</strong>&nbsp;<span class="normalfnt"><?php echo $rejectBy ?></span><?php } ?></td>
  </tr>
  
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td width="5%">&nbsp;</td>
        <td colspan="7" class="normalfnt">
          <table width="100%" class="rptBordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr  >
              <th width="10%" >Main Category</th>
              <th width="11%" >Sub Category</th>
              <th width="10%" >Item Code</th>
              <th width="23%" >Item</th>
              <th width="6%" >UOM</th>
              <th width="9%" >PO Rate</th>
              <th width="9%" >GRN Rate</th>
              <th width="7%" >FOC</th>
              <th width="4%" >Qty</th>
              <th width="6%" >Amount</th>
              </tr>
              <?php 
	  	   $sql1 = "SELECT
ware_grndetails.intGrnNo,
ware_grndetails.intGrnYear,
ware_grndetails.intItemId,
Sum(ware_grndetails.dblGrnQty) as dblGrnQty,
Sum(ware_grndetails.dblFocQty) as dblFocQty,
ware_grndetails.dblPoRate,
ware_grndetails.dblGrnRate,
ware_grndetails.dblInvoiceRate,
mst_item.strCode,
mst_item.strName,
mst_item.intUOM, 
mst_maincategory.strName AS mainCategory,
mst_subcategory.strName AS subCategory
FROM ware_grndetails
Inner Join mst_item ON ware_grndetails.intItemId = mst_item.intId
Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
WHERE
ware_grndetails.intGrnNo =  '$grnNo' AND
ware_grndetails.intGrnYear =  '$year' 
GROUP BY
ware_grndetails.intItemId    
";
			$result1 = $db->RunQuery($sql1);
			$totQty=0;
			$totAmmount=0;
			while($row=mysqli_fetch_array($result1))
			{
	  ?>
            <tr class="normalfnt"  <?php if($row['dblGrnRate']>$row['dblInvoiceRate']){ ?> bgcolor="#FF7777" <?php } else { ?> bgcolor="#FFFFFF" <?php } ?>>
              <td class="normalfnt" >&nbsp;<?php echo $row['mainCategory']?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['subCategory'] ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['strCode'] ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['strName'] ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['intUOM'] ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['dblPoRate'] ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['dblGrnRate'] ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['dblFocQty'] ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['dblGrnQty'] ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo round(($row['dblGrnQty']-$row['dblFocQty'])*$row['dblGrnRate'],4) ?>&nbsp;</td>
              </tr>
      <?php 
			$totQty+=$row['dblGrnQty'];
			$totAmmount+=($row['dblGrnQty']-$row['dblFocQty'])*$row['dblGrnRate'];
			}
	  ?>
            <tr class="normalfnt"  bgcolor="#CCCCCC">
              <td class="normalfnt" style="display:none" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $totQty ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $totAmmount ?>&nbsp;</td>
              </tr>
            </table>
          </td>
        <td width="6%">&nbsp;</td>
        </tr>
      
      </table>
    </td>
</tr>
            <tr>
<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
</body>

<?php
 ini_set('display_errors',0);
	(session_id() == ''?session_start():'');
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$companyId 	= $_SESSION['CompanyID'];
	include "{$backwardseperator}dataAccess/Connector.php";
	require_once $backwardseperator."class/warehouse/cls_warehouse_get.php";

	$objwhouseget= new cls_warehouse_get($db);

	$programName='Good Received Note';
	$programCode='P0224';
	$grnApproveLevel = (int)getApproveLevel($programName);

	$requestType 	= $_REQUEST['requestType'];

		$db->OpenConnection();
		$db->RunQuery2('Begin');
	
 if($requestType=='getValidation')
	{
		$grnNo  = $_REQUEST['grnNo'];
		$grnNoArray=explode("/",$grnNo);
		$grnNo=$grnNoArray[0];
		$grnNoYear=$grnNoArray[1];
		$folderName	=$grnNo.'-'.$grnNoYear;
		
		///////////////////////
		$sql = "SELECT
		ware_grnheader.intStatus, 
		ware_grnheader.intApproveLevels, 
		ware_grnheader.intGrnRateApproval,
		ware_grnheader.intPoNo,
		ware_grnheader.intPoYear 
		FROM ware_grnheader
		WHERE
		ware_grnheader.intGrnNo =  '$grnNo' AND
		ware_grnheader.intGrnYear =  '$grnNoYear'";

		$result = $db->RunQuery2($sql);
		$errorFlg=0;
		$msg =""; 
		$msg1 ="";
		$row=mysqli_fetch_array($result);
		$status=$row['intStatus'];
		$approveLevels=$row['intApproveLevels'];	
		$grnRateApproval=$row['intGrnRateApproval'];
		$poNo=$row['intPoNo'];
		$poYear=$row['intPoYear'];
		
		
		//--------------------------
		$poStatus=getPOStatus($poNo,$poYear);//check wether po not confirmed
		//--------------------------
		$response['arrData'] = $objwhouseget->getLocationValidation('GRN',$companyId,$poNo,$poYear);//check for grn location
		//--------------------------
		$confirmatonMode=loadConfirmatonMode($programCode,$status,$approveLevels,$userId);
		
		$file_uploaded_status	= get_file_uploaded_status($folderName,$companyId);
		
/*		 if($response['arrData']['type']=='fail'){
			$errorFlg = 1;
			 $msg=$response['arrData']['msg'];
		 }
		else */
		if($poStatus!=1){
			 $errorFlg=1;
			 $msg="This PO No is not confirmed.Cant raise GRN";
		}
		else if($status==1){// 
			$errorFlg = 1;
			$msg ="Final confirmation of this GRN is already raised"; 
		}
		else if($grnRateApproval==0){
			$errorFlg = 1;
			$msg ="GRN rate confirmations pending for highlighted items"; 
		}
		else if($file_uploaded_status==0){
			$errorFlg = 1;
			$msg ="Please attach the invoice";
		}
		else if($confirmatonMode==0){// 
			$errorFlg = 1;
			$msg ="No Permission to Approve"; 
		}
		else if($status==$approveLevels+1){
				$sql1 = "SELECT
						sum(tb1.dblQty) as dblQty,
						ware_grndetails.dblGrnQty,
						
						IFNULL((SELECT
						Sum(ware_grndetails.dblGrnQty)
						FROM
						ware_grnheader
						Inner Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear
						WHERE
						ware_grnheader.intPoNo =  tb1.intPONo AND
						ware_grnheader.intPoYear =  tb1.intPOYear AND
						ware_grndetails.intItemId =  tb1.intItem AND
						ware_grnheader.intStatus  <=  ware_grnheader.intApproveLevels AND 
						ware_grnheader.intStatus > 0 ),0)
						 as totGRNQty,
						 
						IFNULL((SELECT
						Sum(ware_returntosupplierdetails.dblQty)
						FROM
						ware_returntosupplierheader
						Inner Join ware_returntosupplierdetails ON ware_returntosupplierheader.intReturnNo = ware_returntosupplierdetails.intReturnNo AND ware_returntosupplierheader.intReturnYear = ware_returntosupplierdetails.intReturnYear
						Inner Join ware_grnheader ON ware_returntosupplierheader.intGrnNo = ware_grnheader.intGrnNo AND ware_returntosupplierheader.intGrnYear = ware_grnheader.intGrnYear
						WHERE
						ware_grnheader.intPoNo =  tb1.intPONo AND
						ware_grnheader.intPoYear =   tb1.intPOYear AND
						ware_returntosupplierdetails.intItemId =  tb1.intItem AND
						ware_returntosupplierheader.intStatus <=  ware_returntosupplierheader.intApproveLevels AND 
						ware_returntosupplierheader.intStatus > 0 ),0) as dblRetunSupplierQty,
						  
						mst_item.strName 
						FROM
						ware_grndetails
						Inner Join ware_grnheader ON ware_grndetails.intGrnNo = ware_grnheader.intGrnNo AND ware_grndetails.intGrnYear = ware_grnheader.intGrnYear
						Inner Join trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
						Inner Join trn_podetails as tb1 ON trn_poheader.intPONo = tb1.intPONo AND trn_poheader.intPOYear = tb1.intPOYear AND ware_grndetails.intItemId = tb1.intItem 
						Inner Join mst_item ON tb1.intItem = mst_item.intId
							WHERE
							ware_grndetails.intGrnNo =  '$grnNo' AND
							ware_grndetails.intGrnYear =  '$grnNoYear'";
					$result1 = $db->RunQuery2($sql1);
					
					$totTosaveQty=0;
					$totSavableQty=0;
					while($row1=mysqli_fetch_array($result1))
					{  
						$balQtyToGRN =($row1['dblQty']-$row1['totGRNQty']+$row1['dblRetunSupplierQty']);
						if(round(($row1['dblQty']-$row1['totGRNQty']+$row1['dblRetunSupplierQty']),2) < round($row1['dblGrnQty'],2)){
							$errorFlg = 1;
							$msg1 ="Maximum GRN Quantities are </br>";
							$msg .=$row1['strName']." = ".($row1['dblQty']-$row1['totGRNQty']+$row1['dblRetunSupplierQty'])." </br>"; 
						}
						
					$totTosaveQty +=$row1['dblGrnQty'];
					$totSavableQty +=$balQtyToGRN;
						
					}
		}
					$msg=$msg1.$msg;
		
		$totTosaveQty=round($totTosaveQty,2)	;
		$totSavableQty=round($totSavableQty,2)	;
	
 	if(($totTosaveQty > $totSavableQty) && ($errorFlg!=1)){
		$errorFlg=1;
		$msg ="Balance to GRN Qty is not tally with This Qtys.";
	}
	
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);
	}

//---------------------------
else if($requestType=='validateRejecton')
	{
		$grnNo  = $_REQUEST['grnNo'];
		$grnNoArray=explode("/",$grnNo);
		$grnNo=$grnNoArray[0];
		$grnNoYear=$grnNoArray[1];
		///////////////////////
		$sql = "SELECT
		ware_grnheader.intStatus, 
		ware_grnheader.intApproveLevels , 
		ware_grnheader.intPoNo,
		ware_grnheader.intPoYear 
		FROM ware_grnheader
		WHERE
		ware_grnheader.intGrnNo =  '$grnNo' AND
		ware_grnheader.intGrnYear =  '$grnNoYear'";

		$result = $db->RunQuery2($sql);
		$errorFlg=0;
		$msg =""; 
		$row=mysqli_fetch_array($result);
		$poNo=$row['intPoNo'];
		$poYear=$row['intPoYear'];
		
		$rejectionMode=loadRejectionMode($programCode,$row['intStatus'],$row['intApproveLevels'],$userId);
		
		//--------------------------
		$poStatus=getPOStatus($grnNo,$grnNoYear);//check wether po not confirmed
		//--------------------------
		$response['arrData'] = $objwhouseget->getLocationValidation('GRN',$companyId,$poNo,$poYear);//check for grn location
		//--------------------------
		
		
/*		if($response['arrData']['type']=='fail'){
			 $errorFlg = 1;
			 $msg=$response['arrData']['msg'];
		}
		else */
		/*if($poStatus!=1){
			 $errorFlg=1;
			 $msg="This PO No is not confirmed.Cant reject GRN";
		}
		else*/ if($row['intStatus']==1){// 
			$errorFlg = 1;
			$msg ="Final confirmation of this GRN is already raised.So cant reject"; 
		}
		else if($row['intStatus']==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="This GRN is already Rejected"; 
		}
		else if($rejectionMode==0){//no confirmation has been raised
			$errorFlg = 1;
			$msg ="No Permission to reject this GRN"; 
		}
		else{//confirmation has been raised
			$errorFlg = 0;
			
		}
	
	if($errorFlg==1){
		$response['status'] = 'fail';
		$response['msg'] 	= $msg;
	}
	else{
		$response['status'] 	= 'pass';
		$response['msg'] 		= '';
	}
		
		echo json_encode($response);
}
			$db->RunQuery2('Commit');
		$db->CloseConnection();		

//------------------------------function loadRejectionMode-------------------
function loadRejectionMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$rejectMode=0;
	$sqlp = "SELECT
		menupermision.intReject 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery2($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intReject']==1){
	 if($intStatus!=0){
	 $rejectMode=1;
	 }
	}
	 
	return $rejectMode;
}
//---------------------------------------	
	function getPOStatus($serialNo,$year)
	{
		global $db;
	  	$sql = "SELECT
				trn_poheader.intStatus
				FROM
				trn_poheader
				WHERE
				trn_poheader.intPoNo =  '$serialNo' AND
				trn_poheader.intPoYear =  '$year'";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
			
		return $status;
	}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	 $sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery2($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
//--------------------------------------------------------
function get_file_uploaded_status($folderName,$location){
	
	global $db;
	$flag	=0;
	$tdir	="../../../../documents/GRN/$folderName";
	
	$sql = "SELECT
			mst_locations.COMPULSORY_INVOICE_UPLOAD_IN_GRN_FLAG
			FROM
			mst_locations
			WHERE
			mst_locations.intId =  '$location' ";
       
		$results = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($results);
		$loc_flag = $row['COMPULSORY_INVOICE_UPLOAD_IN_GRN_FLAG'];
	
	
	if($loc_flag==1)
	{
		$m=0;
		$dirs = scandir($tdir);
		foreach($dirs as $file)
		{
			
			if (($file == '.')||($file == '..'))
			{
			}
			else
			{
				
				if (! is_dir($tdir.'/'.$file))
				{
					$flag	= 1;
				}
			}
		}
	}
	else{
					$flag	= 1;
	}
	
	return $flag;
 }

?>

<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
date_default_timezone_set('Asia/Kolkata');

require_once	"libraries/jqgrid2/inc/jqgrid_dist.php";

$company 		= $_SESSION['headCompanyId'];
$location 		= $_SESSION['CompanyID'];

$programName	='Purchase Order';
$programCode	='P0224';
$menuSpecialId	='21';

$reportMenuId	='895';
$menuId			='224';
 $intUser  		= $_SESSION["userId"];

$userDepartment	=getUserDepartment($intUser);
$approveLevel 	= (int)getMaxApproveLevel();


################################################################
################## new part ####################################
################################################################

//----------Supplier--------------------------------
$str_supplier = "" ;
$sql1 = "SELECT strName AS supplierName  FROM mst_supplier WHERE intStatus = 1 ORDER BY strName ASC";
$result1 = $db->RunQuery($sql1);
$sup   = "" ;
$supplier = array("All");
while($row=mysqli_fetch_array($result1))
{
	array_push($supplier,$row['supplierName']);
}
foreach ($supplier AS $s){
	$supplier2 = $s;
	if($s != "All"){
		$supplier1 = $s;
	}
	else{
		$supplier1 = '';
	}
	$sup  .= $supplier1.":".$supplier2.";" ;
}
$str_supplier = $sup;
$str_supplier .= ":" ;
//----------GRN YEAR--------------------------------
$str_year='';
$year =array("All");
for($y=date("Y"); $y>=2012; $y--){
	array_push($year,$y);
}
foreach ($year AS $arrYear){
	$y2 = $arrYear;
	if($arrYear != "All"){
		$y1 = $arrYear;
	}
	else{
		$y1 = '';
	}
	$str_year 		   .= $y1.":".$y2.";" ;
}
$str_year 			.= ":" ;
// -----------SHIPMENT TERMS-----------------------------------
$str_shipment = '';
$sql_shipmentTerm = "SELECT
					mst_shipmentterms.strName
					FROM mst_shipmentterms
					WHERE intStatus = 1";
$result_Shipment = $db->RunQuery($sql_shipmentTerm);
$a = array("All");
while($row = mysqli_fetch_array($result_Shipment))
{
	array_push($a,$row['strName']);
}
foreach ($a AS $b){
	$shipmentTerm2 = $b;
	if($b != "All"){
		$shipmentTerm1 = $b;
	}
	else{
		$shipmentTerm1 = '';
	}
	$str_1 		   .= $shipmentTerm1.":".$shipmentTerm2.";" ;
}
$str_shipment  = $str_1;
$str_shipment  .= ":";
// --------------------------------------------------
///// create where part ///////////////////
$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];
$where_string = '';
$where_array = array(
				'Status'=>'tb1.intStatus',
				'GRN_No'=>'tb1.intGrnNo',
				'GRN_Year'=>'tb1.intGrnYear',
				'shipment' => 'mst_shipmentterms.strName',
				'PO_No'=>'tb1.intPoNo',
				'PO_Year'=>'tb1.intPoYear',
				'Invoice_No'=>'tb1.strInvoiceNo',
				'Supplier'=>'mst_supplier.strName',
				'User'=>'sys_users.strUserName'
				);
$arr_status = array('Approved'=>'1','Rejected'=>'0','Pending'=>'2');

foreach($arr as $k=>$v)
{
	if($v['field']=='Status')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}

if(!count($arr)>0){
	// $where_string .= "AND mst_shipmentterms.strName = '".$shipmentTerm1."'";
	 //$where_string .= " AND mst_supplier.strName = '".$supplierIni."'";
	 //$where_string .= " AND tb1.intGrnYear = '".date('Y')."'";
	 $where_string .= " AND 1=1 ";
}

################## end code ####################################

$sql = "select * from(SELECT DISTINCT 
							if(tb1.intStatus=1,'Approved',if(tb1.intStatus=0,'Rejected','Pending')) as Status,
							tb1.intGrnNo as `GRN_No`,
							tb1.intGrnYear as `GRN_Year`,
							tb1.intPoNo as `PO_No`,
							tb1.intPoYear as `PO_Year`,
							tb1.strInvoiceNo as `Invoice_No`,
							mst_supplier.strName as `Supplier`,
							tb1.datdate as `Date`,
							tb1.SUPPLIER_INVOICE_DATE as `Invoice_Date`,
							
							IFNULL((SELECT
							Sum(ware_grndetails.dblGrnQty )
							FROM
							ware_grnheader
							Inner Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear
							WHERE
							ware_grnheader.intGrnNo =  tb1.intGrnNo AND
							ware_grnheader.intGrnYear =  tb1.intGrnYear),0) as `GRN_Qty`,
							
							ROUND(IFNULL((SELECT
							Sum(ROUND(ware_grndetails.dblGrnQty * ware_grndetails.dblGrnRate,2))
							FROM
							ware_grnheader
							Inner Join ware_grndetails ON ware_grnheader.intGrnNo = ware_grndetails.intGrnNo AND ware_grnheader.intGrnYear = ware_grndetails.intGrnYear
							WHERE
							ware_grnheader.intGrnNo =  tb1.intGrnNo AND
							ware_grnheader.intGrnYear =  tb1.intGrnYear),0),2) as `GRN_Amount`,
							  
							sys_users.strUserName as `User`, 
							  
							IFNULL((
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(ware_grnheader_approvedby.dtApprovedDate),')' )
								FROM
								ware_grnheader_approvedby
								Inner Join sys_users ON ware_grnheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_grnheader_approvedby.intGrnNo  = tb1.intGrnNo AND
								ware_grnheader_approvedby.intYear =  tb1.intGrnYear AND
								ware_grnheader_approvedby.intApproveLevelNo =  '1' AND 
								ware_grnheader_approvedby.intStatus='0' 
							),IF(((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.intStatus>1),'Approve', '')) as `1st_Approval`,  ";
							
						for($i=2; $i<=$approveLevel; $i++){
							
							if($i==2){
							$approval="2nd_Approval";
							}
							else if($i==3){
							$approval="3rd_Approval";
							}
							else {
							$approval=$i."th_Approval";
							}
							
							
						$sql .= "IFNULL(
						/*condition*/
(
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(ware_grnheader_approvedby.dtApprovedDate),')' )
								FROM
								ware_grnheader_approvedby
								Inner Join sys_users ON ware_grnheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_grnheader_approvedby.intGrnNo  = tb1.intGrnNo AND
								ware_grnheader_approvedby.intYear =  tb1.intGrnYear AND
								ware_grnheader_approvedby.intApproveLevelNo =  '$i' AND 
								ware_grnheader_approvedby.intStatus='0' 
							),
							/*end condition*/
							
							/*false part*/

							IF(
							/*if condition */
							((SELECT
								menupermision.int".$i."Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND (tb1.intStatus>1) AND (tb1.intStatus<=tb1.intApproveLevels) AND ((SELECT
								max(ware_grnheader_approvedby.dtApprovedDate) 
								FROM
								ware_grnheader_approvedby
								Inner Join sys_users ON ware_grnheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_grnheader_approvedby.intGrnNo  = tb1.intGrnNo AND
								ware_grnheader_approvedby.intYear =  tb1.intGrnYear AND
								ware_grnheader_approvedby.intApproveLevelNo =  ($i-1) AND 
								ware_grnheader_approvedby.intStatus='0' )<>'')),
								
								/*end if condition*/
								/*if true part*/
								'Approve',
								/*fase part*/
								 if($i>tb1.intApproveLevels,'-----',''))
								
								/*end IFNULL false part*/
								) as `".$approval."`, "; 
									
								}
								
						 	$sql .= "IF(((SELECT
									menus_special_permision.intSpMenuId
									FROM
									menus_special
									INNER JOIN menus_special_permision ON menus_special.intId = menus_special_permision.intSpMenuId
									WHERE
									menus_special_permision.intUser = '$intUser' AND
									menus_special.intId = '$menuSpecialId') ='') || (tb1.intStatus !=1),
								
								/*if true part*/
								   '', 
								 
								/*fase part*/
								   'Print'
								  
								  )  as `PRINT`,  ";

							$sql .= "'View' as `View` ,mst_shipmentterms.strName AS `shipment`,
							        tb1.WayBillNo AS waybillno
						FROM
							ware_grnheader as tb1 
							Inner Join trn_poheader ON tb1.intPoNo = trn_poheader.intPONo AND tb1.intPoYear = trn_poheader.intPOYear
							Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
							Inner Join sys_users ON tb1.intUser = sys_users.intUserId 
							Inner Join mst_locations ON tb1.intCompanyId = mst_locations.intId 
							Inner Join mst_shipmentterms ON trn_poheader.intShipmentTerm = mst_shipmentterms.intId
							WHERE
							mst_locations.intCompanyId =  '$company' AND 
							tb1.intCompanyId =  '$location' 
							$where_string
							)  as t where 1=1 ";
					  	 // echo $sql;

$formLink			= "?q=$menuId&grnNo={GRN_No}&year={GRN_Year}";	 
$reportLink  		= "?q=$reportMenuId&grnNo={GRN_No}&year={GRN_Year}";
$reportLinkApprove  = "?q=$reportMenuId&grnNo={GRN_No}&year={GRN_Year}&approveMode=1";
$reportLinkPrint  	= "?q=$reportMenuId&grnNo={GRN_No}&year={GRN_Year}&mode=print";
						 
$col = array();


//STATUS
$col["title"] 	= "Status"; // caption of column
$col["name"] 	= "Status"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
//edittype
$col["stype"] 	= "select";
$str = ":All;Pending:Pending;Approved:Approved;Rejected:Rejected" ;
$col["editoptions"] 	=  array("value"=> $str);
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$cols[] = $col;	$col=NULL;

//PO No
$col["title"] 	= "GRN No"; // caption of column
$col["name"] 	= "GRN_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "2";
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$col['link']	= $formLink;	 
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag


$cols[] = $col;	$col=NULL;



//PO Year
$col["title"] = "GRN Year"; // caption of column
$col["name"] = "GRN_Year"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$col["stype"] 	= "select";
$str = $str ;
$col["editoptions"] 	=  array("value"=> $str_year);
$cols[] = $col;	$col = NULL;

//wayBillNo //hasitha //2260
$col["title"] = "Way Bill No"; // caption of column
$col["name"] = "waybillno"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Shipment Terms
$col["title"] = "Shipment Terms"; // caption of column
$col["name"] = "shipment"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "2";
$col["align"] = "center";
$col["stype"] 	= "select";
$str = $str ;
$col["editoptions"] 	= array("value"=> $str_shipment);
$cols[] = $col;	$col=NULL;

//PO Year
$col["title"] = "PO No"; // caption of column
$col["name"] = "PO_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;



//PO Year
$col["title"] = "PO Year"; // caption of column
$col["name"] = "PO_Year"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Invoice Date
$col["title"] = "Invoice Date"; // caption of column
$col["name"] = "Invoice_Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "left";

$cols[] = $col;	$col=NULL;


//Delivery Date
$col["title"] = "Invoice No"; // caption of column
$col["name"] = "Invoice_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "left";

$cols[] = $col;	$col=NULL;



//Delivery Date
$col["title"] = "Supplier"; // caption of column
$col["name"] = "Supplier"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["align"] = "left";
$col["stype"] 	= "select";
$str = $str ;
$col["editoptions"] 	=  array("value"=> $str_supplier);
$cols[] = $col;	$col=NULL;


//Supplier
$col["title"] = "Date"; // caption of column
$col["name"] = "Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//GRN Qty
$col["title"] = "GRN Qty"; // caption of column
$col["name"] = "GRN_Qty"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//GRN Amount
$col["title"] = "GRN Amount"; // caption of column
$col["name"] = "GRN_Amount"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//User
$col["title"] = "User"; // caption of column
$col["name"] = "User"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;



//FIRST APPROVAL
$col["title"] = "1st Approval"; // caption of column
$col["name"] = "1st_Approval"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

for($i=2; $i<=$approveLevel; $i++){
	if($i==2){
	$ap="2nd Approval";
	$ap1="2nd_Approval";
	}
	else if($i==3){
	$ap="3rd Approval";
	$ap1="3rd_Approval";
	}
	else {
	$ap=$i."th Approval";
	$ap1=$i."th_Approval";
	}
//SECOND APPROVAL
$col["title"] = $ap; // caption of column
$col["name"] = $ap1; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;
}

//PRINT
$col["title"] = "Print"; // caption of column
$col["name"] = "PRINT"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkPrint;
$col['linkName']	= 'Print';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

//VIEW
$col["title"] = "Report"; // caption of column
$col["name"] = "View"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

$jq = new jqgrid('',$db);

$grid["caption"] 		= "GRN Listing";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'GRN_No'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes


// export XLS file
// export to excel parameters - range could be "all" or "filtered"
//$grid["export"] = array("format"=>"xlsx", "filename"=>"my-file", "sheetname"=>"test");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);




$out = $jq->render("list1");
?>
<title>GRN Listing</title>

	<?php 
		//include "include/listing.html";
	?>
<form id="frmlisting" name="frmlisting" method="post" action="">

        <div align="center" style="margin:10px">        
			<?php echo $out?>
        </div>
</form>

 <?php

//------------------------------function load Default Department---------------------
function getMaxApproveLevel(){
	global $db;
	
	//echo $savedStat;
	$appLevel=0;
	$sqlp = "SELECT
			Max(ware_grnheader.intApproveLevels) AS appLevel
			FROM ware_grnheader";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['appLevel'];
}
//------------------------------function load User Department---------------------
function getUserDepartment($intUser){
	global $db;

	$sqlp = "SELECT
			sys_users.intDepartmentId
			FROM
			sys_users
			WHERE
			sys_users.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['intDepartmentId'];

}
?>


<?php
ini_set('dispaly_errors',1);
session_start();
$backwardseperator = "../../../../";
$companyId = $_SESSION['CompanyID'];
$locationId = $_SESSION['CompanyID'];
$intUser  = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
include_once  	"{$backwardseperator}dataAccess/Connector.php";

include_once "../../../../config.php";
$path		= MAIN_URL;

if($grnNo==''){
$grnNo = $_REQUEST['grnNo'];
$year = $_REQUEST['year'];
}
$folderName 		= 	$grnNo.'-'.$year;

$approveMode = $_REQUEST['approveMode'];
$mode 			= $_REQUEST['mode'];

$programName='Good Received Note';
$programCode='P0224';


//$grnNo = '100000';
//$year = '2012';
//$approveMode=1;

 $sql = "SELECT
mst_companies.strName as delToCompany,
mst_locations.strName as delToLocation ,
ware_grnheader.datdate,
ware_grnheader.intPoNo,
ware_grnheader.intPoYear,
trn_poheader.dtmPODate as poDate,
ware_grnheader.strInvoiceNo,
ware_grnheader.strDeliveryNo,
ware_grnheader.intStatus,
ware_grnheader.intApproveLevels,
ware_grnheader.intUser,
ware_grnheader.intCompanyId as poRaisedLocationId, 
sys_users.strUserName , 
mst_financecurrency.strCode as currency, 
mst_supplier.strName as suppName ,
ware_grnheader.PRINT_COUNT,
ware_grnheader.SUPPLIER_INVOICE_DATE as invoiceDate 
FROM
ware_grnheader 
Inner Join mst_locations ON ware_grnheader.intCompanyId = mst_locations.intId
Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId 
Inner Join sys_users ON ware_grnheader.intUser = sys_users.intUserId 
Inner Join trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
Inner Join mst_financecurrency ON trn_poheader.intCurrency = mst_financecurrency.intId 
WHERE
ware_grnheader.intGrnNo =  '$grnNo' AND
ware_grnheader.intGrnYear =  '$year'";
				 $result = $db->RunQuery2($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$company = $row['delToCompany'];
					$location = $row['delToLocation'];
					$locationId = $row['poRaisedLocationId'];//this locationId use in report header(reportHeader.php)--------------------
					$poNo = $row['intPoNo'];
					$poDate = $row['poDate'];
					$poYear = $row['intPoYear'];
					$supplier = $row['suppName'];
					$invoiceNo = $row['strInvoiceNo'];
					$invoiceDate = $row['invoiceDate'];
					$deliveryNo = $row['strDeliveryNo'];
					$grnDate = $row['datdate'];
					$grnBy = $row['strUserName'];
					$intStatus = $row['intStatus'];
					$savedLevels = $row['intApproveLevels'];
					$createdUser = $row['intUser'];
					$currency = $row['currency'];
					$PRINT_COUNT = $row['PRINT_COUNT'];
				 }
				 
?>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
<style>
.normalfntBlue {
	font-family: Verdana;
	font-size: 11px;
	color: #0B3960;
	margin: 0px;
	font-weight: normal;
	text-align: left;
}
.normalfnt {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align:left;
}
.normalfntsm {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: left;
}
.normalfntMid {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: center;
}
.normalfntRight {
	font-family: Verdana;
	font-size: 11px;
	color: #000000;
	margin: 0px;
	font-weight: normal;
	text-align: right;
}
.compulsoryRed {
	color: #F00;
}
table .rptBordered {
    border-spacing: 0;
    width: 100%;
}
.rptBordered {
    border: 1px solid #CCCCCC;
    border-radius: 6px 6px 6px 6px;
    box-shadow: 0 1px 1px #CCCCCC;
    font-family: Verdana;
    font-size: 11px;
}
.rptBordered tr:hover {
    background: none repeat scroll 0 0 #FBF8E9;
    transition: all 0.1s ease-in-out 0s;
}
.rptBordered td {
    border-left: 1px solid #CCCCCC;
    border-top: 1px solid #CCCCCC;
    padding: 2px;
}
.rptBordered th {
    border-left: 1px solid #CCCCCC;
    border-top: 1px solid #CCCCCC;
    padding: 4px;
    text-align: center;
}
.rptBordered th {
    background-color: #CCCCCC;
    background-image: -moz-linear-gradient(center top , #EBF3FC, #CCCCCC);
    border-top: medium none;
    box-shadow: 0 1px 0 rgba(255, 255, 255, 0.8) inset;
    text-shadow: 0 1px 0 rgba(255, 255, 255, 0.5);
}
.rptBordered td:first-child, .rptBordered th:first-child {
    border-left: medium none;
}
.rptBordered th:first-child {
    border-radius: 6px 0 0 0;
}
.rptBordered th:last-child {
    border-radius: 0 6px 0 0;
}
.rptBordered th:only-child {
    border-radius: 6px 6px 0 0;
}
.rptBordered tr:last-child td:first-child {
    border-radius: 0 0 0 6px;
}
.rptBordered tr:last-child td:last-child {
    border-radius: 0 0 6px 0;
}
</style>

</head>

<body>
<div align="center">
  <table width="100%">
  <tr>
    <td colspan="9" align="center" ><table width="100%" cellpadding="0" cellspacing="0">
      <tr>
        <td class="tophead"><table width="100%" cellpadding="0" cellspacing="0">
          <tr>
            <td width="15%"><img src=<?php echo MAIN_ROOT ?>"/images/screenline.jpg" alt="" class="mainImage" /></td>
            <td width="1%" class="normalfnt">&nbsp;</td>
            <td align="center" valign="top" width="68%" class="topheadBLACK"><?php
	 $SQL = "SELECT
mst_companies.strName,
mst_locations.strName as locationName,
mst_locations.strAddress,
mst_locations.strStreet,
mst_locations.strCity,
mst_country.strCountryName,
mst_locations.strPhoneNo,
mst_locations.strFaxNo,
mst_locations.strEmail,
mst_companies.strWebSite,
mst_locations.strZip
FROM
mst_locations
Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
Inner Join mst_country ON mst_companies.intCountryId = mst_country.intCountryID
WHERE
mst_locations.intId =  '$locationId'
;";
	
	$result = $db->RunQuery2($SQL);
	$row = mysqli_fetch_array($result);
	$companyName		= $row["strName"];
	$locationName		= $row["locationName"];
	$companyAddress1	= $row["strAddress"];
	$companyStreet		= $row["strStreet"];
	$companyCity		= $row["strCity"];
	$companyCountry		= $row["strCountryName"];
	$companyZipCode		= $row["strZip"];
	$companyPhone		= "(".$companyZipCode.")".$row["strPhoneNo"];
	$companyFax		 	= "(".$companyZipCode.")".$row["strFaxNo"];
	$companyEmail		= $row["strEMail"];
	$companyWeb		 	= $row["strWebSite"];
	
	?>
              <b><?php echo $companyName.""; ?></b>
              <p class="normalfntMid">
                <?php // (".$locationName.") --> Edited By Lasantha 14th of May 2012?>
                <?php echo $companyAddress1." ".$companyStreet." ".$companyCity." ".$companyCountry."."."<br><b>Tel : </b>".$companyPhone." <b>Fax : </b>".$companyFax." <br><b>E-Mail : </b>".$companyEmail." <b>Web : </b>".$companyWeb;?></p></td>
            <td width="16%" class="tophead">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
    </table></td>
  </tr>
  </table>
<div style="background-color:#FFF" ><strong>GOODS RECEIVE NOTE</strong><strong></strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
  <tr>
  <?php
 	if($intStatus==1)
	{
	?>
   <td colspan="9" class="confirmed" >CONFIRMED</td>
   <?PHP
	}
	else if($intStatus==0)
	{
   ?>
   <td colspan="9" class="reject" >REJECTED</td>
   <?php
	}
	else
	{
   ?>
   <td width="10%" colspan="9" class="pending">PENDING</td>
   <?php
	}
   ?>
  </tr>
  <tr>
    <td width="1%">&nbsp;</td>
    <td width="11%"><span class="normalfnt">GRN No</span></td>
    <td width="2%" align="center" valign="middle"><strong>:</strong></td>
    <td width="31%"><span class="normalfnt"><div id="divGrnNo"><?php echo $grnNo ?>/<?php echo $year ?></div></span></td>
    <td width="12%" class="normalfnt">Invoice No</td>
    <td width="2%" align="center" valign="middle"><strong>:</strong></td>
    <td width="22%" colspan="3"><span class="normalfnt"><?php echo $invoiceNo  ?></span></td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">GRN Date</td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $grnDate  ?></span></td>
    <td><span class="normalfnt">Invoice Date</span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td colspan="3"><span class="normalfnt"><?php echo $invoiceDate  ?></span><span class="normalfnt"></span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">PO No</td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $poNo ?>/<?php echo $poYear ?></span></td>
    <td><span class="normalfnt">PO Date</span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td colspan="3"><span class="normalfnt"><?php echo $poDate ?></span></td>
  </tr>

  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">Supplier Name</td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $supplier  ?></span></td>
    <td><span class="normalfnt">Currency</span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td colspan="3"><span class="normalfnt"><?php echo $currency  ?></span></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">Delivery No</td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $deliveryNo ?></span></td>
    <td><span class="normalfnt">GRN By</span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td colspan="3"><span class="normalfnt"><?php echo $grnBy  ?></span></td>
  </tr>
  </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td colspan="7">
          <table width="100%" class="rptBordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr  >
              <th width="8%" >Main Category</th>
              <th width="10%" >Sub Category</th>
              <th width="9%" >Item Code</th>
              <th width="17%" >Item</th>
              <th width="5%" >UOM</th>
              <th width="8%" >GRN Rate</th>
              <th width="5%" >FOC</th>
              <th width="4%" >Qty</th>
              <th width="7%" >Amount</th>
              <th width="6%" >Tax Code</th>
              <th width="9%" >Tax Amount</th>
               </tr>
            <?php 
	  	   $sql1 = "SELECT
ware_grndetails.intGrnNo,
ware_grndetails.intGrnYear,
ware_grndetails.intItemId,
ware_grndetails.dblGrnQty as dblGrnQty,
ware_grndetails.dblFocQty as dblFocQty,
ware_grndetails.dblPoRate,
ROUND(ware_grndetails.dblGrnRate,6) AS dblGrnRate,
ROUND(ware_grndetails.dblInvoiceRate,6) AS dblInvoiceRate,
ROUND(AVG(trn_podetails.dblUnitPrice),6) as dblUnitPrice,
AVG(trn_podetails.dblDiscount) as dblDiscount,
sum(trn_podetails.dblQty) as dblQty,
sum(trn_podetails.dblTaxAmmount) as dblTaxAmmount,
trn_podetails.intTaxCode,
mst_financetaxgroup.strCode as taxCode, 
mst_item.strCode,
mst_item.strName,
mst_item.intUOM, 
mst_units.strCode as unit, 
mst_maincategory.strName AS mainCategory,
mst_subcategory.strName AS subCategory, 
ware_grndetails.intInspectionItem,
ware_grndetails.intInspected 
FROM ware_grndetails 
Inner Join ware_grnheader ON ware_grndetails.intGrnNo = ware_grnheader.intGrnNo AND ware_grndetails.intGrnYear = ware_grnheader.intGrnYear
Inner Join trn_podetails ON ware_grnheader.intPoNo = trn_podetails.intPONo AND ware_grnheader.intPoYear = trn_podetails.intPOYear AND ware_grndetails.intItemId = trn_podetails.intItem
left Join mst_financetaxgroup ON trn_podetails.intTaxCode = mst_financetaxgroup.intId
Inner Join mst_item ON ware_grndetails.intItemId = mst_item.intId
Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
Inner Join mst_units ON mst_units.intId = mst_item.intUOM
WHERE
ware_grndetails.intGrnNo =  '$grnNo' AND
ware_grndetails.intGrnYear =  '$year' 
GROUP BY
ware_grndetails.intItemId     
Order by mst_maincategory.strName asc, mst_subcategory.strName asc, mst_item.strName asc";
//echo $sql1;
			$result1 = $db->RunQuery2($sql1);
			$totQty=0;
			$totAmmount=0;
			$totTax=0;
			//7iyii
			while($row=mysqli_fetch_array($result1))
			{
					$taxAmount = $row['dblTaxAmmount'];
					$totAmount = $row['dblUnitPrice']*$row['dblQty']*(100-$row['dblDiscount'])/100;
					$grnAmmount=round(($row['dblGrnQty']-$row['dblFocQty'])*$row['dblGrnRate'],2);
					$grnTaxAmmount=round(($taxAmount/$totAmount)*$grnAmmount,2);
					$color="#B8DCDC";
					$inspectStatus='';
					if($row['intInspectionItem']==1){
						if($row['intInspected']==1){
							$color="#00A6A6";
							$inspectStatus='Inspected';
						}
						else{
							$color="#9D0000";
							$inspectStatus='Not Inspected';
						}
					}
	  ?>
            <tr class="normalfnt"  bgcolor= <?php if($row['dblGrnRate']>$row['dblInvoiceRate']){ ?>"#FF7777" <?php } else { ?> "#FFFFFF" <?php } ?> >
              <td class="normalfnt" >&nbsp;<?php echo $row['mainCategory']?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['subCategory'] ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['strCode'] ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['strName'] ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['unit'] ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo number_format($row['dblGrnRate'],6) ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['dblFocQty'] ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['dblGrnQty'] ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo number_format(($row['dblGrnQty']-$row['dblFocQty'])*$row['dblGrnRate'],2); ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['taxCode'] ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $grnTaxAmmount ?>&nbsp;</td>
               </tr>
            <?php 
			$totQty+=$row['dblGrnQty'];
			$totTax+=$grnTaxAmmount;
			$totAmmount+=round(($row['dblGrnQty']-$row['dblFocQty'])*$row['dblGrnRate'],2);
			}
	  ?>
            <tr class="normalfnt"  bgcolor="#CCCCCC">
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $totQty ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo number_format($totAmmount,2); ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
               </tr>
            </table>
        </td>
        <td width="6%">&nbsp;</td>
        </tr>
      </table>
    </td>
</tr>
<?php  
					 $flag=0;
					 $sqlc = "SELECT
							ware_grnheader_approvedby.intApproveUser,
							ware_grnheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName,
							ware_grnheader_approvedby.intApproveLevelNo
							FROM
							ware_grnheader_approvedby
							Inner Join sys_users ON ware_grnheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							ware_grnheader_approvedby.intGrnNo =  '$grnNo' AND
							ware_grnheader_approvedby.intYear =  '$year'  order by dtApprovedDate asc
";
					 $resultc = $db->RunQuery2($sqlc);
					while($rowc=mysqli_fetch_array($resultc)){
						if($rowc['intApproveLevelNo']==1)
						$desc="1st ";
						else if($rowc['intApproveLevelNo']==2)
						$desc="2nd ";
						else if($rowc['intApproveLevelNo']==3)
						$desc="3rd ";
						else
						$desc=$rowc['intApproveLevelNo']."th ";
					 //  $desc=$ap.$desc;
					 $desc2=$rowc['UserName']."(".$rowc['dtApprovedDate'].")";
					 if($rowc['UserName']=='')
					 $desc2='---------------------------------';
					 
					 if($rowc['intApproveLevelNo']>0){
						 $flag=1;
				?>
<?php
	//}
	//else{			
					}
					 if($rowc['intApproveLevelNo']==0){
						 $flag=2;
					  ?>
<?php
					 }
					 if($rowc['intApproveLevelNo']==-1){
						 $flag=3;
					  ?>
<?php
					 }
					 
		 if($rowc['intApproveLevelNo']>0){
			 $finalSavedLevel=$rowc['intApproveLevelNo'];
		 }
	 }//end of while
	 
	if($flag>1){
		$finalSavedLevel=0;
	}
	
	if(($flag<=1) || ($intStatus>0)){
	for($j=$finalSavedLevel+1; $j<=$savedLevels; $j++)
	{ 
		if($j==1)
		$desc="1st ";
		else if($j==2)
		$desc="2nd ";
		else if($j==3)
		$desc="3rd ";
		else
		$desc=$j."th ";
		$desc2='---------------------------------';
	?>
<?php
	}
	}
?>

<?php
	$docs = filesInDir("../../../../documents/GRN/$folderName",$folderName,$path);
	if($docs!=''){
?>
<tr>
 <td class="normalfnt"><b><u>Uploaded Documents</u></b> </td>
</tr>
<tr><td><table width="100%">
<?php
	echo $docs ;
	
  ?>
  </table></td></tr>
  <?php
	}
  ?>
<tr height="40" >
  <td align="center" class="normalfntMid">&nbsp;</td>
</tr>
</table>
 </div>        
</body>
</html>
<?php
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	$sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery2($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
//------------------------------function loadRejectionMode-------------------
function loadRejectionMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$rejectMode=0;
	$sqlp = "SELECT
		menupermision.intReject 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery2($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intReject']==1){
	 if($intStatus!=0){
	 $rejectMode=1;
	 }
	}
	 
	return $rejectMode;
}
//-------------------------------------------------------------------------
function filesInDir($tdir,$folderName,$path)
{
	$m=0;
	$dirs = scandir($tdir);
	$docs ='';
	//global $path;
	
	foreach($dirs as $file)
	{
		
		if (($file == '.')||($file == '..'))
		{
		}
		else
		{
			
			if (! is_dir($tdir.'/'.$file))
			{
				
			$docs .= "	<tr bgcolor=\"#ffffff\" class=\"normalfnt\">
						<td width=\"5%\">".++$m."</td>
						<td width=\"50%\" align=\"left\"><a target=\"_blank\" href=\"$path"."presentation/documents/GRN/".$folderName."/$file\">$file</a></td>
						<td width=\"45%\"></td>
					 </tr>";
			}
		}
	}
	
	return $docs;
}

?>	

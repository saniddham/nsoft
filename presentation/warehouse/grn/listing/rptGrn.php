<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
$companyId = $_SESSION['CompanyID'];
$intUser  = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
$poUrl ="?q=1157";

$grnNo = $_REQUEST['grnNo'];
$year = $_REQUEST['year'];
$approveMode = $_REQUEST['approveMode'];
$mode 			= $_REQUEST['mode'];
$programName='Good Received Note';
$programCode='P0224';

 $sql = "SELECT
mst_companies.strName as delToCompany,
mst_locations.strName as delToLocation ,
ware_grnheader.datdate,
ware_grnheader.intPoNo,
ware_grnheader.intPoYear,
ware_grnheader.strInvoiceNo,
ware_grnheader.strDeliveryNo,
ware_grnheader.intStatus,
ware_grnheader.intApproveLevels,
ware_grnheader.intUser,
ware_grnheader.intCompanyId as poRaisedLocationId, 
ware_grnheader.SUPPLIER_INVOICE_DATE as InvoiceDate, 
sys_users.strUserName , 
mst_financecurrency.strCode as currency, 
mst_supplier.strName as suppName ,
ware_grnheader.PRINT_COUNT,
ware_grnheader.WayBillNo
FROM
ware_grnheader 
Inner Join mst_locations ON ware_grnheader.intCompanyId = mst_locations.intId
Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId 
Inner Join sys_users ON ware_grnheader.intUser = sys_users.intUserId 
Inner Join trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
Inner Join mst_financecurrency ON trn_poheader.intCurrency = mst_financecurrency.intId 
WHERE
ware_grnheader.intGrnNo =  '$grnNo' AND
ware_grnheader.intGrnYear =  '$year'";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$company = $row['delToCompany'];
					$location = $row['delToLocation'];
					$locationId = $row['poRaisedLocationId'];//this locationId use in report header(reportHeader.php)--------------------
					$poNo = $row['intPoNo'];
					$poYear = $row['intPoYear'];
					$supplier = $row['suppName'];
					$invoiceNo = $row['strInvoiceNo'];
					$deliveryNo = $row['strDeliveryNo'];
					$grnDate = $row['datdate'];
					$grnBy = $row['strUserName'];
					$intStatus = $row['intStatus'];
					$savedLevels = $row['intApproveLevels'];
					$createdUser = $row['intUser'];
					$currency = $row['currency'];
					$PRINT_COUNT = $row['PRINT_COUNT'];
					$InvoiceDate = $row['InvoiceDate'];
					$WayBillNo =$row['WayBillNo'];
                    $poUrl = $poUrl."&poNo=".$poNo."&year=".$poYear;
				 }


$confirmationMode=loadConfirmatonMode($programCode,$intStatus,$savedLevels,$intUser);
$rejectionMode=loadRejectionMode($programCode,$intStatus,$savedLevels,$intUser);

?>
<head>
<title>Goods Recieve Note Report</title>
<script type="application/javascript" src="presentation/warehouse/grn/listing/rptGrn-js.js"></script>

<style>
.break { page-break-before: always; }

@media print {
.noPrint
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:255px;
	top:180px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>

</head>

<body>
<?php
 if($intStatus>1)//pending
{

?>
<div id="apDiv1"></div>
<?php
}
//echo $PRINT_COUNT.'-'.$mode.'-'.$intStatus.'-';
 if($mode != 'print'  && $intStatus==1 )
{
?>
<div style="position: absolute; left: 534px; top: 129px; width: 165px; height: 158px;"><img width="162" height="151" src="images/duplicate.png"  /></div>
<?php
}
if(($intStatus==1) && ($mode == 'print'))
{
	$sql = "UPDATE `ware_grnheader` SET `PRINT_COUNT`=PRINT_COUNT+1 WHERE (`intGrnNo`='$grnNo') AND (`intGrnYear`='$year')";
	$db->RunQuery($sql);
}
?>
<?php
 if($intStatus>1)//pending
{
?>
<div id="apDiv1"><img src="images/pending.png"  /></div>
<?php
}
?>

<form id="frmGRNReport" name="frmGRNReport" method="post" action="rptGrn.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top">
  <?php include 'reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>GOODS RECEIVE NOTE</strong><strong></strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
  <tr>
    <td colspan="9" align="center" >
    <?php
	if($intStatus>1)
	{
	?>
    <?php if($approveMode==1){
			if($confirmationMode==1) {
			?>
			<img src="images/approve.png" align="middle" class="mouseover noPrint" id="imgApprove" />
			<?php
			}
			if($rejectionMode==1) {
			?>
            <img src="images/reject.png" align="middle" class="mouseover noPrint" id="imgReject" />
                <?php
                }
            }
	}
	?>
    </td>
  </tr>
  <tr>
  <?php
 	if($intStatus==1)
	{
	?>
   <td colspan="9" class="confirmed" >CONFIRMED</td>
   <?PHP
	}
	else if($intStatus==0)
	{
   ?>
   <td colspan="9" class="reject" >REJECTED</td>
   <?php
	}
	else
	{
   ?>
   <td width="10%" colspan="9" class="pending">PENDING</td>
   <?php
	}
   ?>
  </tr>
  <tr>
    <td width="1%">&nbsp;</td>
    <td width="11%"><span class="normalfnt">GRN No</span></td>
    <td width="2%" align="center" valign="middle"><strong>:</strong></td>
    <td width="31%"><span class="normalfnt"><div id="divGrnNo"><?php echo $grnNo ?>/<?php echo $year ?></div></span></td>
    <td width="12%" class="normalfnt">PO NO</td>
    <td width="2%" align="center" valign="middle"><strong>:</strong></td>
    <td colspan="3"><a href="<?php echo $poUrl ?>" target="_blank"><span class="normalfnt"><?php echo $poNo ?>/<?php echo $poYear ?></span></a>   <div id="divPoNo" style="display:none"><?php echo $poNo ?>/<?php echo $year ?></div></td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">Delivery No</td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $deliveryNo ?></span></td>
    <td><span class="normalfnt">Supplier Name</span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td colspan="3"><span class="normalfnt"><?php echo $supplier  ?></span></td>
    </tr>

  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">Date</td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $grnDate  ?></span></td>
    <td><span class="normalfnt">Invoice No</span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td colspan="3"><span class="normalfnt"><?php echo $invoiceNo  ?></span><span class="normalfnt"></span></td>
    </tr>
	<tr>
    <td>&nbsp;</td>
    <td class="normalfnt"></td>
    <td align="center" valign="middle"><strong></strong></td>
    <td><span class="normalfnt"></span></td>
    <td><span class="normalfnt">Invoice Date</span></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td colspan="3"><span class="normalfnt"><?php echo $InvoiceDate  ?></span><span class="normalfnt"></span></td>
    </tr>


  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">GRN By</td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $grnBy  ?></span></td>
    <td class="normalfnt">Currency</td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td width="10%"><span class="normalfnt"><?php echo $currency  ?></span></td>
     <?php if($WayBillNo != NULL) {?>
      <td class="normalfnt">Way Bill No</td>
      <td align="center" valign="middle"><strong>:</strong></td>
      <td width="10%"><span class="normalfnt"><?php echo $WayBillNo  ?></span></td>
      <?php } ?>
      <td width="4%">&nbsp;</td>
       <td width="8%">&nbsp;</td>
  </tr>
  </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td colspan="7" class="normalfnt">
          <table width="100%" class="rptBordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr >
              <th width="8%" >Main Category</th>
              <th width="10%" >Sub Category</th>
              <th width="9%" >Item Code</th>
              <th width="17%" >Item</th>
              <th width="5%" >UOM</th>
              <th width="8%" >Expiry Date</th>
              <th width="8%" >GRN Rate</th>
              <th width="5%" >FOC</th>
              <th width="4%" >Qty</th>
              <th width="7%" >Amount</th>
              <th width="6%" >Tax Code</th>
              <th width="9%" >Tax Amount</th>
              <th width="10%" >Inspection</th>
              </tr>
            <?php
	  	   $sql1 = "SELECT
ware_grndetails.intGrnNo,
ware_grndetails.intGrnYear,
ware_grndetails.intItemId,
ware_grndetails.dblGrnQty as dblGrnQty,
ware_grndetails.dblFocQty as dblFocQty,
ware_grndetails.dblPoRate,
ROUND(ware_grndetails.dblGrnRate,6) AS dblGrnRate,
ware_grndetails.dblInvoiceRate,
ROUND(AVG(trn_podetails.dblUnitPrice),6) as dblUnitPrice,
AVG(trn_podetails.dblDiscount) as dblDiscount,
sum(trn_podetails.dblQty) as dblQty,
sum(trn_podetails.dblTaxAmmount) as dblTaxAmmount,
trn_podetails.intTaxCode,
mst_financetaxgroup.strCode as taxCode, 
mst_item.strCode,
mst_item.strCode as SUP_ITEM_CODE,
mst_item.strName,
mst_item.intUOM, 
mst_units.strCode as unit, 
mst_maincategory.strName AS mainCategory,
mst_subcategory.strName AS subCategory, 
ware_grndetails.intInspectionItem,
ware_grndetails.intInspected ,
ware_grndetails.EXPIRE_DATE AS EXPIRY_DATE
FROM ware_grndetails 
Inner Join ware_grnheader ON ware_grndetails.intGrnNo = ware_grnheader.intGrnNo AND ware_grndetails.intGrnYear = ware_grnheader.intGrnYear
Inner Join trn_podetails ON ware_grnheader.intPoNo = trn_podetails.intPONo AND ware_grnheader.intPoYear = trn_podetails.intPOYear AND ware_grndetails.intItemId = trn_podetails.intItem
left Join mst_financetaxgroup ON trn_podetails.intTaxCode = mst_financetaxgroup.intId
Inner Join mst_item ON ware_grndetails.intItemId = mst_item.intId
Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
Inner Join mst_units ON mst_units.intId = mst_item.intUOM
LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
WHERE
ware_grndetails.intGrnNo =  '$grnNo' AND
ware_grndetails.intGrnYear =  '$year' 
GROUP BY
ware_grndetails.intItemId     
Order by mst_maincategory.strName asc, mst_subcategory.strName asc, mst_item.strName asc";
//echo $sql1;
			$result1 = $db->RunQuery($sql1);
			$totQty=0;
			$totAmmount=0;
			$totTax=0;
			//7iyii
			while($row=mysqli_fetch_array($result1))
			{
					$taxAmount = $row['dblTaxAmmount'];
					$totAmount = $row['dblUnitPrice']*$row['dblQty']*(100-$row['dblDiscount'])/100;
					$grnAmmount= round(($row['dblGrnQty']-$row['dblFocQty'])*$row['dblGrnRate'],2);
					$grnTaxAmmount= round(($taxAmount/$totAmount)*$grnAmmount,2);
					$color="#B8DCDC";
					$inspectStatus='';
					if($row['intInspectionItem']==1){
						if($row['intInspected']==1){
							$color="#00A6A6";
							$inspectStatus='Inspected';
						}
						else{
							$color="#9D0000";
							$inspectStatus='Not Inspected';
						}

					}

	  ?>
            <tr class="normalfnt"  bgcolor= <?php if($row['dblGrnRate']>$row['dblInvoiceRate']){ ?>"#FF7777" <?php } else { ?> "#FFFFFF" <?php } ?> >
              <td class="normalfnt" >&nbsp;<?php echo $row['mainCategory']?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['subCategory'] ?>&nbsp;</td>
              <?php
              if($row['SUP_ITEM_CODE']!=null)
              {
              ?>
              <td class="normalfnt" >&nbsp;<?php echo $row['SUP_ITEM_CODE'] ?>&nbsp;</td>
              <?php 
             }else
                  {
              ?>
              <td class="normalfnt" >&nbsp;<?php echo $row['strCode'] ?>&nbsp;</td>
              <?php
              }
              ?>
              <td class="normalfnt" >&nbsp;<?php echo $row['strName'] ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['unit'] ?>&nbsp;</td>
              <td class="normalfntRight" ><?php echo $row['EXPIRY_DATE'] ?></td>
              <td class="normalfntRight" >&nbsp;<?php echo number_format($row['dblGrnRate'],6) ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['dblFocQty'] ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['dblGrnQty'] ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo number_format(($row['dblGrnQty']-$row['dblFocQty'])*$row['dblGrnRate'],2); ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['taxCode'] ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo number_format($grnTaxAmmount,2) ?>&nbsp;</td>
              <td class="normalfnt" align="center"><div style="color:<?php echo $color;?>; text-align:center""><?php echo $inspectStatus ?></div></td>
              </tr>
            <?php
			$totQty+=$row['dblGrnQty'];
			$totTax+=$grnTaxAmmount;
			$totAmmount+= round(($row['dblGrnQty']-$row['dblFocQty'])*$row['dblGrnRate'],2);
			}
	  ?>
            <tr class="normalfnt"  bgcolor="#CCCCCC">
              <td class="normalfnt" style="display:none" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $totQty ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo number_format($totAmmount,2); ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td width="2%" class="normalfntRight" ></td>
              </tr>
            </table>
        </td>
        <td width="6%">&nbsp;</td>
        </tr>

      </table>
    </td>
</tr>
<?php
					 $flag=0;
					 $sqlc = "SELECT
							ware_grnheader_approvedby.intApproveUser,
							ware_grnheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName,
							ware_grnheader_approvedby.intApproveLevelNo
							FROM
							ware_grnheader_approvedby
							Inner Join sys_users ON ware_grnheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							ware_grnheader_approvedby.intGrnNo =  '$grnNo' AND
							ware_grnheader_approvedby.intYear =  '$year'  order by dtApprovedDate asc
";
					 $resultc = $db->RunQuery($sqlc);
					while($rowc=mysqli_fetch_array($resultc)){
						if($rowc['intApproveLevelNo']==1)
						$desc="1st ";
						else if($rowc['intApproveLevelNo']==2)
						$desc="2nd ";
						else if($rowc['intApproveLevelNo']==3)
						$desc="3rd ";
						else
						$desc=$rowc['intApproveLevelNo']."th ";
					 //  $desc=$ap.$desc;
					 $desc2=$rowc['UserName']."(".$rowc['dtApprovedDate'].")";
					 if($rowc['UserName']=='')
					 $desc2='---------------------------------';

					 if($rowc['intApproveLevelNo']>0){
						 $flag=1;
				?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> Approved By - </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
            </tr>
<?php
	//}
	//else{
					}
					 if($rowc['intApproveLevelNo']==0){
						 $flag=2;
					  ?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong> Rejected By - </strong></span><span class="normalfnt"><?php echo $rowc['UserName']."(".$rowc['dtApprovedDate'].")";?></span></td>
            </tr>
<?php
					 }
					 if($rowc['intApproveLevelNo']==-1){
						 $flag=3;
					  ?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong> Revised By - </strong></span><span class="normalfnt"><?php echo $rowc['UserName']."(".$rowc['dtApprovedDate'].")";?></span></td>
            </tr>
<?php
					 }

		 if($rowc['intApproveLevelNo']>0){
			 $finalSavedLevel=$rowc['intApproveLevelNo'];
		 }
	 }//end of while

	if($flag>1){
		$finalSavedLevel=0;
	}

	if(($flag<=1) || ($intStatus>0)){
	for($j=$finalSavedLevel+1; $j<=$savedLevels; $j++)
	{
		if($j==1)
		$desc="1st ";
		else if($j==2)
		$desc="2nd ";
		else if($j==3)
		$desc="3rd ";
		else
		$desc=$j."th ";
		$desc2='---------------------------------';
	?>
        <tr>
            <td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> Approved By - </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
        </tr>
<?php
	}
	}
?>
<tr height="80" >
  <td align="center" class="normalfntMid"></td>
</tr>
<tr height="40">
<?php
	//$locationId = '';
	$sql = "SELECT mst_locations.intCompanyId FROM mst_locations WHERE mst_locations.intId =  '$locationId'";
	$result 	= $db->RunQuery($sql);
	$row		= mysqli_fetch_array($result);
	$createCompanyId = $row['intCompanyId'];
?>
<?Php
	$url  = "{$backwardseperator}presentation/sendToApproval.php";		// * file name
	$url .= "?status=$intStatus";										// * set recent status
	$url .= "&approveLevels=$savedLevels";								// * set approve levels in order header
	$url .= "&programCode=$programCode";								// * program code (ex:P10001)
	$url .= "&program=GRN";									// * program name (ex:Purchase Order)
	$url .= "&companyId=$createCompanyId";									// * created company id
	$url .= "&createUserId=$createdUser";

	$url .= "&field1=GRN No";
	$url .= "&field2=GRN Year";
	$url .= "&value1=$grnNo";
	$url .= "&value2=$year";

	$url .= "&subject=GRN FOR APPROVAL ('$grnNo'/'$year')";

	$url .= "&statement1=Please Approve this";
	$url .= "&statement2=to Approve this";
							// * doc year
	$url .= "&link=".urlencode(base64_encode($mainPath."presentation/warehouse/grn/listing/rptGrn.php?grnNo=$grnNo&year=$year&approveMode=1"));


/*edit mode for the doc*/
if($savedLevels +1 == $intStatus){
	$editMode = 1;
}else {
	$editMode = 0;
}
?>
<td>
<table width="100%">
<tr>
  <td align="center" class="normalfntMid"><iframe id="iframeFiles2" src="<?php echo $url;?>" name="iframeFiles" style="width:500px;height:175px;border:none"  ></iframe></td>
  <td align="center" class="normalfntMid"><iframe  id="iframeFiles" src="presentation/warehouse/grn/addNew/filesUpload.php?txtFolder=<?php echo "$grnNo-$year".'&editMode='.$editMode; ?>" name="iframeFiles" style="width:400px;height:175px;border:none;" class="noPrint"  ></iframe></td>
</tr>
</table>
</td>
</tr>
<tr height="40" >
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>
</form>
</body>
</html>
<?php
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;

	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	$sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";

	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);

	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}

	return $confirmatonMode;
}
//------------------------------function loadRejectionMode-------------------
function loadRejectionMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;

	$rejectMode=0;
	$sqlp = "SELECT
		menupermision.intReject 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";

	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);

	if($rowp['intReject']==1){
	 if($intStatus!=0){
	 $rejectMode=1;
	 }
	}

	return $rejectMode;
}
//-------------------------------------------------------------------------
?>

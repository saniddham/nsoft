// JavaScript Document
var basePath = "presentation/warehouse/grn/listing/";

$(document).ready(function() {
	$('#frmGRNReport').validationEngine();
	
	$('#frmGRNReport #imgApprove').click(function(){
	var val = $.prompt('Are you sure you want to approve this GRN ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					if(validateGrnRate()==0){ 
					///////////
					var url = basePath+"rptGrn-db-set.php"+window.location.search+'&status=approve';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmGRNReport #imgApprove').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmGRNReport #imgApprove').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
					}
					hideWaiting();

					}
				}});
	});


	
	$('#frmGRNReport #imgReject').click(function(){
	var val = $.prompt('Are you sure you want to reject this GRN ?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{
				showWaiting();
				if(validateRejecton()==0){
					///////////
					var url = basePath+"rptGrn-db-set.php"+window.location.search+'&status=reject';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmGRNReport #imgReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmGRNReport #imgReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
				}
				hideWaiting();
			}
		}});
	});
	
	$('#frmGRNApprovalReport #imgApproveRate').click(function(){
	var val = $.prompt('Are you sure you want to approve this GRN rates ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
						showWaiting();
/*						var url = "rptGrn-db-set.php"+window.location.search+'&status=approveGRNRates';
						var obj = $.ajax({url:url,async:false});
						window.location.href = window.location.href;
						window.opener.location.reload();//reload listing page
*/
					///////////
					var url = basePath+"rptGrn-db-set.php"+window.location.search+'&status=approveGRNRates';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmGRNReport #imgApproveRate').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmGRNReport #imgApproveRate').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						hideWaiting();
					}
				}});
	});
	
});
//---------------------------------------------------
function validateGrnRate(){
		var ret=0;	
	    var grnNo = document.getElementById('divGrnNo').innerHTML;
		var url 		= basePath+"rptGrn-db-get.php?requestType=getValidation";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"grnNo="+grnNo,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#imgApprove').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertx()",3000);
					  ret= 1;
					}
				}
			});
			return ret;
}
//---------------------------------------------------
function validateRejecton(){
		var ret=0;	
	    var grnNo = document.getElementById('divGrnNo').innerHTML;
		var url 		= basePath+"rptGrn-db-get.php?requestType=validateRejecton";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"grnNo="+grnNo,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#imgApprove').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertx()",3000);
					  ret= 1;
					}
				}
			});
			return ret;
}
//------------------------------------------------
function alertx()
{
	$('#frmGRNReport #imgApprove').validationEngine('hide')	;
}
//--------------------------------------------
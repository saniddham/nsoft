<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
include_once ("class/cls_commonErrorHandeling_get.php");
include_once ("class/warehouse/general_stock_dispose/cls_general_stock_dispose_get.php");
require_once ("class/warehouse/cls_warehouse_get.php");

$obj_commonErr		= new cls_commonErrorHandeling_get($db);
$obj_data_get		= new cls_general_stock_dispose_get($db);
$objwhouseget		= new cls_warehouse_get($db);

$programName		='Genaral Stock Dispose';
$progrmCode			='P0804';
$session_userId 	= $_SESSION["userId"];
$serialNo 			= $_REQUEST['serialNo'];
$serialYear 		= $_REQUEST['year'];
$mode				= $_REQUEST["mode"];


$header_array 		= $obj_data_get->get_header_array($serialNo,$serialYear,'RunQuery');
$detail_result		= $obj_data_get->get_details_result($serialNo,$serialYear,'RunQuery');

$permition_arr		= $obj_commonErr->get_permision_withApproval_save($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery');
$permision_save		= $permition_arr['permision'];
$permition_arr		= $obj_commonErr->get_permision_withApproval_confirm($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery');
$permision_confirm	= $permition_arr['permision'];
$permision_reject	= $permision_confirm;


$locationId			= $header_array["LOCATION"];

if(!isset($_REQUEST["SerialNo"]))
	$dispDate		= date("Y-m-d");
else
	$dispDate		= $header_array["DATE"];
	
$company_Id			= $header_array["COMPANY_ID"];
?>
<head>
<title>Item Dispose Report</title>
<link rel="stylesheet" type="text/css" href="css/button.css"/>
<link rel="stylesheet" type="text/css" href="css/promt.css"/>
<script type="text/javascript" src="presentation/warehouse/general_stock_dispose/listing/rpt_general_stock_dispose.js"></script>
</head>
<body>
<style type="text/css">
.apDiv1 {
	position:absolute;
	left:380px;
	top:100px;
	width:auto;
	height:auto;
	z-index:0;
	opacity:0.1;
}
</style>
<?php
 if($header_array['STATUS']>1 && $header_array['STATUS'] <= ($header_array['LEVELS']+1))//pending
{
	
?>
<div id="apDiv1"  style="position: absolute; left: 385px; top: 148px; width: 165px; height: 210px;"><img width="413" height="219" src="images/pending.png"  /></div>
<?php
}
?>
<form id="frmRptGeneral_stock_dispose" name="frmRptGeneral_stock_dispose" method="post" >
  <table width="980" align="center">
    <tr>
      <td><?php include 'reportHeader.php'?></td>
    </tr>
    <tr>
      <td class="reportHeader" align="center">Item Disposal</td>
    </tr>
	<?php
		include "presentation/report_approve_status_and_buttons.php"
     ?>
<tr>
      <td><table width="100%" border="0" class="normalfnt">
          <tr>
            <td width="14%">Dispose No</td>
            <td width="1%">:</td>
            <td width="43%"><?php echo $header_array["DISPOSE_NO"].'/'.$header_array["YEAR"] ?></td>
            <td width="14%">Dispose Date</td>
            <td width="2%">:</td>
            <td width="26%"><?php echo $header_array["DATE"]?></td>
          </tr>
          <tr>
            <td>Note</td>
            <td>:</td>
            <td><?php echo $header_array["NOTE"]?></td>
            <td></td>
            <td></td>
            <td></td>
          </tr>

       </table></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
      <td ><table width="100%%" border="0" class="rptBordered" id="tblMain">
          <thead>
            <tr>
              <th width="10%" nowrap="nowrap" >Main Category</th>
              <th width="10%" nowrap="nowrap" >Sub Category</th>
              <th width="7%" height="22" >Item Code</th>
              <th width="24%" nowrap="nowrap" >Item Description</th>
              <th width="4%" >UOM</th>
              <th width="7%" >GRN No</th>
              <th width="6%" >Date</th>
              <th width="6%" >Rate</th>
              <th width="5%">Currency</th>
              <th width="8%">Exchange Rate</th>
              <th width="8%">Stock Qty</th>
              <th width="5%">Qty</th>
            </tr>
          </thead>
          <tbody>
			<?php
            while($row = mysqli_fetch_array($detail_result)){
				$opCombo		='<select style=\"width:50px\" name=\"cboOperation\" id=\"cboOperation\"   class=\"validate[required] operation\"><option value=\"-1\">-</option></select>';
			   $stockBalQty		=$objwhouseget->get_stock_balance_bulk($locationId,$row['ITEM'],$row['GRN_NO'],$row['GRN_YEAR'],$row['GRN_DATE'],$row['RATE'],'RunQuery');			
			
            ?>
					<tr class="normalfnt">
					<td align="center" bgcolor="#FFFFFF" id="<?php echo $row['intMainCategory']; ?>"><?php echo $row['mainCat']; ?></td>
					<td align="center" bgcolor="#FFFFFF" id="<?php echo $row['intSubCategory']; ?>"><?php echo $row['subCat']; ?></td>
                                        <?php
                                        if($row['SUP_ITEM_CODE']!=null)
                                        {
                                        ?>
                                        <td align="center" bgcolor="#FFFFFF" id="<?php echo $row['ITEM']; ?>"><?php echo $row['SUP_ITEM_CODE']; ?></td>
                                        <?php
                                        }else{
                                        ?>
					<td align="center" bgcolor="#FFFFFF" id="<?php echo $row['ITEM']; ?>"><?php echo $row['strCode']; ?></td>
                                        <?php
                                        }
                                        ?>
					<td align="center" bgcolor="#FFFFFF" id="<?php echo $row['ITEM']; ?>" class="item"><?php echo $row['itemNm']; ?></td>
					<td align="center" bgcolor="#FFFFFF" id="<?php echo $row['unit']; ?>" class="uom"><?php echo $row['unit']; ?></td>
					<td align="center" bgcolor="#FFFFFF" id="<?php echo $row['GRN_NO'].'/'.$row['GRN_YEAR']; ?>"  class="grnNo"><?php echo $row['GRN_NO'].'/'.$row['GRN_YEAR']; ?></td>
					<td align="center" bgcolor="#FFFFFF" id="<?php echo $row['GRN_DATE']; ?>"  class="grnDate"><?php echo $row['GRN_DATE']; ?></td>
					<td align="center" bgcolor="#FFFFFF" id="<?php echo $row['RATE']; ?>"  class="grnRate"><?php echo $row['RATE']; ?></td>
					<td align="center" bgcolor="#FFFFFF" id="<?php echo $row['CURRENCY']; ?>"  class="currency"><?php echo $row['curr']; ?></td>
					<td align="right" bgcolor="#FFFFFF" id="<?php echo $stockBalQty; ?>"><?php echo $row['EXCH_RATE'];; ?></td>
					<td align="right" bgcolor="#FFFFFF" id="<?php echo $stockBalQty; ?>"><?php echo $stockBalQty; ?></td>
					<td align="center" bgcolor="#FFFFFF" id="<?php echo $stockBalQty; ?>" ><?php echo $row['QTY']; ?></td>
            
            <?php
            }
            ?>            
</tbody>
        </table></td>
    </tr>
     <tr>
      <td>&nbsp;</td>
    </tr>
    <tr>
    <td>
    <?php
			$creator		= $header_array['CREATOR'];
			$createdDate	= $header_array['CREATE_DATE'];
 			$resultA 		= $obj_data_get->get_Report_approval_details_result($serialNo,$serialYear,'RunQuery');
			include "presentation/report_approvedBy_details.php"
 	?>
    </td>
    </tr>
<tr height="40">
<?php
	//$locationId = '';
	$locationId 		= $header_array['LOCATION'];
	$sql			 	= "SELECT mst_locations.intCompanyId FROM mst_locations WHERE mst_locations.intId =  '$locationId'";
	$result				= $db->RunQuery($sql);
	$row				= mysqli_fetch_array($result);
	$createCompanyId 	= $row['intCompanyId'];
?>
<?Php 
	$intStatus 			= $header_array['STATUS'];
	$savedLevels 		= $header_array['LEVELS'];
	$createUserId 		= $header_array['USER'];
	

	$url  = "{$backwardseperator}presentation/sendToApproval.php";		// * file name
	$url .= "?status=$intStatus";										// * set recent status
	$url .= "&approveLevels=$savedLevels";								// * set approve levels in order header
	$url .= "&programCode=$progrmCode";								// * program code (ex:P10001)
	$url .= "&program=DISPOSE NOTE";									// * program name (ex:Purchase Order)
	$url .= "&companyId=$createCompanyId";									// * created company id
	$url .= "&createUserId=$createUserId";	
	
	$url .= "&field1=Dispose No";												 
	$url .= "&field2=Dispose Year";	
	$url .= "&value1=$serialNo";												 
	$url .= "&value2=$serialYear";	
		
	$url .= "&subject=DISPOSE NOTE FOR APPROVAL ('$serialNo'/'$serialYear')";	
	
	$url .= "&statement1=Please Approve this";	
	$url .= "&statement2=to Approve this";	
							// * doc year
	$url .= "&link=".urlencode(base64_encode($mainPath."presentation/warehouse/general_stock_dispose/listing/rpt_general_stock_dispose.php?serialNo=$serialNo&year=$serialYear&mode=Confirm"));
?>
  <td align="center" class="normalfntMid"><iframe id="iframeFiles2" src="<?php echo $url;?>" name="iframeFiles" style="width:500px;height:150px;border:none"  ></iframe></td>
</tr>  </table>
</form>
</body>
</html>
<?php
 ?>
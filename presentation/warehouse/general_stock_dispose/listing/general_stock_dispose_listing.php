<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
require_once	"libraries/jqgrid2/inc/jqgrid_dist.php";

$session_locationId = $_SESSION["CompanyID"];
$session_companyId 	= $_SESSION["headCompanyId"];
$intUser			= $_SESSION["userId"];

$programName		='Genaral Stock Dispose';
$programCode		='P0804';
$reportID			= 947;
$approveLevel = (int)getMaxApproveLevel();

################################################################
################## new part ####################################
################################################################
///// create where part ///////////////////
$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];
//print_r($arr);
$where_string = '';
$where_array = array(
				'Status'=>'tb1.intStatus',
				'SERIAL'=>'CONCAT(DISPOSE_NO,'/',YEAR)',
				'Date'=>'tb1.DATE',
				'User'=>'sys_users.strUserName'
				);
$arr_status = array('Approved'=>'1','Rejected'=>'0','Pending'=>'2');
if($arr){
foreach($arr as $k=>$v)
{
	if($v['field']=='Status')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}
}
if(!count($arr)>0)					 
	$where_string .= "AND substr(tb1.DATE,1,10) = '".date('Y-m-d')."'";
	
################## end code ####################################

$sql = "SELECT SUB_1.* FROM
			(SELECT 
					if(tb1.STATUS=1,'Approved',if(tb1.STATUS=0,'Rejected','Pending')) as STATUS,
					CONCAT(DISPOSE_NO,'/',YEAR) AS SERIAL,
					tb1.DISPOSE_NO,
					tb1.`YEAR`,
					tb1.NOTE,
					tb1.LEVELS,
					tb1.DATE,
					tb1.CREATE_DATE,
					tb1.`USER`,
					tb1.LOCATION,
					tb1.MODIFIED_BY,
					tb1.MODIFIED_DATE,
					sys_users.strUserName,				
							IFNULL((
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(ware_general_item_disposal_approveby.dtApprovedDate),')' )
								FROM
								ware_general_item_disposal_approveby
								Inner Join sys_users ON ware_general_item_disposal_approveby.intApproveUser = sys_users.intUserId
								WHERE
								ware_general_item_disposal_approveby.intDisposeNo  = tb1.DISPOSE_NO AND
								ware_general_item_disposal_approveby.intYear =  tb1.YEAR AND
								ware_general_item_disposal_approveby.intApproveLevelNo =  '1' AND
								ware_general_item_disposal_approveby.intStatus =  '0'
							),IF(((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.STATUS>1),'Approve', '')) as `1st_Approval`,  ";
							
						for($i=2; $i<=$approveLevel; $i++){
							
							if($i==2){
							$approval="2nd_Approval";
							}
							else if($i==3){
							$approval="3rd_Approval";
							}
							else {
							$approval=$i."th_Approval";
							}
							
							
						$sql .= "IFNULL(
						/*condition*/
(
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(ware_general_item_disposal_approveby.dtApprovedDate),')' )
								FROM
								ware_general_item_disposal_approveby
								Inner Join sys_users ON ware_general_item_disposal_approveby.intApproveUser = sys_users.intUserId
								WHERE
								ware_general_item_disposal_approveby.intDisposeNo  = tb1.DISPOSE_NO AND
								ware_general_item_disposal_approveby.intYear =  tb1.YEAR AND
								ware_general_item_disposal_approveby.intApproveLevelNo =  '$i' AND
								ware_general_item_disposal_approveby.intStatus =  '0' 
							),
							/*end condition*/
							
							/*false part*/

							IF(
							/*if condition */
							((SELECT
								menupermision.int".$i."Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND (tb1.STATUS>1) AND (tb1.STATUS<=tb1.LEVELS) AND ((SELECT
								concat(sys_users.strUserName )
								FROM
								ware_general_item_disposal_approveby
								Inner Join sys_users ON ware_general_item_disposal_approveby.intApproveUser = sys_users.intUserId
								WHERE
								ware_general_item_disposal_approveby.intDisposeNo  = tb1.DISPOSE_NO AND
								ware_general_item_disposal_approveby.intYear =  tb1.YEAR AND
								ware_general_item_disposal_approveby.intApproveLevelNo =  ($i-1) AND 
								ware_general_item_disposal_approveby.intStatus='0' )<>'')),
								
								/*end if condition*/
								/*if true part*/
								'Approve',
								/*fase part*/
								 if($i>tb1.LEVELS,'-----',''))
								
								/*end IFNULL false part*/
								) as `".$approval."`, "; 
									
								}
								
							$sql .= "IFNULL((SELECT
								concat(sys_users.strUserName,'(',max(ware_general_item_disposal_approveby.dtApprovedDate),')' )
								FROM
								ware_general_item_disposal_approveby
								Inner Join sys_users ON ware_general_item_disposal_approveby.intApproveUser = sys_users.intUserId
								WHERE
								ware_general_item_disposal_approveby.intDisposeNo  = tb1.DISPOSE_NO AND
								ware_general_item_disposal_approveby.intYear =  tb1.YEAR AND
								ware_general_item_disposal_approveby.intApproveLevelNo =  '0' AND
								ware_general_item_disposal_approveby.intStatus =  '0'
							),IF(((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.STATUS<=tb1.LEVELS AND 
								tb1.STATUS>1),'Reject', '')) as `Reject`,";
								
								
							$sql .= "'View' as `View`   
				
				FROM `ware_general_item_disposal_header` as tb1 
				INNER JOIN sys_users ON tb1.USER = sys_users.intUserId
				WHERE tb1.LOCATION = $session_locationId
				$where_string
		)  
		AS SUB_1 WHERE 1=1";
		
		//echo $sql;

$jq = new jqgrid('',$db);	
$col = array();

//BEGIN - ACTIVE/INACTIVE {
$col["title"] 			= "Status";
$col["name"] 			= "STATUS";
$col["width"] 			= "2"; 						// not specifying width will expand to fill space
$col["align"] 			= "center";
$col["sortable"] 		= true; 						// this column is not sortable
$col["search"] 			= true; 						// this column is not searchable
$col["editable"] 		= false;
$col["stype"] 			= "select";
$str 					= "Pending:Pending;Approved:Approved;Rejected:Rejected;:All" ;
$col["searchoptions"] 	= array("value" => $str, "separator" => ":", "delimiter" => ";");
$cols[] 				= $col;	
$col					= NULL;
//END 	- ACTIVE/INACTIVE }

$col["title"] 			= "Serial";
$col["name"] 			= "SERIAL";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 					
$col["editable"] 		= false;
$col['link']			= '?q=804&serialNo={DISPOSE_NO}&year={YEAR}';
$col["linkoptions"] 	= "target='general_stock_dispose.php'";
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Serial No";
$col["name"] 			= "DISPOSE_NO";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true;
$col["hidden"] 			= true;
$col["editable"] 		= false;
$col['link']			= '?q=804&serialNo={DISPOSE_NO}&year={YEAR}';
$col["linkoptions"] 	= "target='general_stock_dispose.php'";
$cols[] 				= $col;	
$col					= NULL;

$col["title"] 			= "Serial Year";
$col["name"] 			= "YEAR";
$col["width"] 			= "2";
$col["align"] 			= "center";
$col["sortable"] 		= true; 					
$col["search"] 			= true; 
$col["hidden"] 			= true;
$col["editable"] 		= false;
$col['link']			= '?q=804&serialNo={DISPOSE_NO}&year={YEAR}';
$col["linkoptions"] 	= "target='general_stock_dispose.php'";
$cols[] 				= $col;	
$col					= NULL;

//DATE
$col["title"] = "Date"; // caption of column
$col["name"] = "DATE"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;
 

//DATE
$col["title"] = "By"; // caption of column
$col["name"] = "strUserName"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;
 
//FIRST APPROVAL
$col["title"] = "1st Approval"; // caption of column
$col["name"] = "1st_Approval"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["search"] = false;
$col["align"] = "center";
$col['link']			= '?q='.$reportID.'&serialNo={DISPOSE_NO}&year={YEAR}&mode=Confirm';
$col["linkoptions"] 	= "target='rpt_general_stock_dispose.php'";
$col['linkName']	= 'Approve';
 $cols[] = $col;	$col=NULL;

for($i=2; $i<=$approveLevel; $i++){
	if($i==2){
	$ap="2nd Approval";
	$ap1="2nd_Approval";
	}
	else if($i==3){
	$ap="3rd Approval";
	$ap1="3rd_Approval";
	}
	else {
	$ap=$i."th Approval";
	$ap1=$i."th_Approval";
	}
//SECOND APPROVAL
$col["title"] = $ap; // caption of column
$col["name"] = $ap1; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["search"] = false;
$col["align"] = "center";
$col['link']			= '?q='.$reportID.'&serialNo={DISPOSE_NO}&year={YEAR}&mode=Confirm';
$col["linkoptions"] 	= "target='rpt_general_stock_dispose.php'";
$col['linkName']	= 'Approve';
 $cols[] = $col;	$col=NULL;
}
/*
$col["title"] = 'Reject'; // caption of column
$col["name"] = 'Reject'; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "4";
$col["search"] = false;
$col["align"] = "center";
$col['link']			= 'rptJournalEntry.php?SerialNo={DISPOSE_NO}&SerialYear={YEAR}&mode=Reject';
$col["linkoptions"] 	= "target='rptJournalEntry.php'";
$col['linkName']	= 'Reject';
 $cols[] = $col;	$col=NULL;*/


//VIEW
$col["title"] = "Report"; // caption of column
$col["name"] = "View"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["search"] = false;
$col["align"] = "center";
$col['link']			= '?q='.$reportID.'&serialNo={DISPOSE_NO}&year={YEAR}';
$col["linkoptions"] 	= "target='rpt_general_stock_dispose.php'";
$cols[] = $col;	$col=NULL;
 
 
$grid["caption"] 		= "Dispose Listing";
$grid["multiselect"] 	= false;
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'YEAR,DISPOSE_NO'; // by default sort grid by this field
$grid["sortorder"] 		= "ASC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes
$grid["search"] 		= true; 
$grid["postData"] 		= array("filters" => $sarr ); 
$grid["export"] 		= array("format"=>"xls", "filename"=>"my-file", "sheetname"=>"test");

$sarr = <<< SEARCH_JSON
{ 
	"groupOp":"AND",
    "rules":[
      {"field":"Status","op":"eq","data":"Pending"}
     ]
}
SEARCH_JSON;

$grid["postData"] = array("filters" => $sarr ); 

$jq->set_options($grid);
$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);

$out = $jq->render("list1");
?>
<title>Dispose Listing</title>
<?php 
		//include "include/listing.html";
	?>

<form id="frmlisting" name="frmlisting" method="post" action="">
        <div align="center" style="margin:10px">        
			<?php echo $out?>
        </div>
</form>
<?php

//------------------------------function load Default Department---------------------
function getMaxApproveLevel(){
	global $db;
	
	//echo $savedStat;
	$appLevel=0;
	$sqlp = "SELECT
			Max(ware_general_item_disposal_header.LEVELS) AS appLevel
			FROM ware_general_item_disposal_header";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['appLevel'];
}
?>

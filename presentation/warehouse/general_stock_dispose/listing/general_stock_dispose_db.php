<?php
//ini_set('display_errors',1);
session_start();

$backwardseperator 	= "../../../";
$mainPath 			= $_SESSION['mainPath'];
$userId 			= $_SESSION['userId'];
$location 			= $_SESSION['CompanyID'];
$company 			= $_SESSION['headCompanyId'];
$requestType		= $_REQUEST["RequestType"];

$programName		='Genaral Stock Dispose';
$progrmCode			='P0804';
$session_userId 	= $_SESSION["userId"];
$session_companyId 	= $_SESSION["headCompanyId"];

//BEGIN - INCLUDE FILES {
include_once  ("../../../../dataAccess/Connector.php");
include "{$backwardseperator}libraries/mail/mail_bcc.php";
require_once $_SESSION['ROOT_PATH']."class/cls_mail.php";
include_once ("../../../../class/warehouse/general_stock_dispose/cls_general_stock_dispose_get.php");
include_once ("../../../../class/warehouse/general_stock_dispose/cls_general_stock_dispose_set.php");
require_once "../../../../class/warehouse/cls_warehouse_set.php";
require_once "../../../../class/warehouse/cls_warehouse_get.php";
include_once  ("../../../../class/cls_commonFunctions_get.php");
include_once  ("../../../../class/cls_commonErrorHandeling_get.php");
require_once $_SESSION['ROOT_PATH']."class/common/azureDBconnection.php";
	
$objMail 			= new cls_create_mail($db);
$objwhouseget		= new cls_warehouse_get($db);
$objwhouseset		= new cls_warehouse_set($db);
$obj_commonErr		= new cls_commonErrorHandeling_get($db);
$obj_common			= new cls_commonFunctions_get($db);
$obj_data_get		= new cls_general_stock_dispose_get($db);
$obj_data_set		= new cls_general_stock_dispose_set($db);
$objAzure           = new cls_azureDBconnection();
 
//END 	- INCLUDE FILES }

if($requestType=='approve')//save
	{
		$savedStatus	= true;
		$db->begin();
		
		$serialNo			= $_REQUEST["serialNo"];
		$serialYear			= $_REQUEST["year"];
	
		$header_array 		= $obj_data_get->get_header_array($serialNo,$serialYear,'RunQuery2');

		//$permition_arr		= $obj_commonErr->get_permision_withApproval_save($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery2');
		//$permision_save		= $permition_arr['permision'];
		$permition_arr		= $obj_commonErr->get_permision_withApproval_confirm($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery2');
		$permision_confirm	= $permition_arr['permision'];
		
		if($permision_confirm !=1){
			$messageErr		= 'No Permision to Confirm'; 
			$sqlErr			= '';
			$rollBackFlag 	=1;
		}

		$trans_response = $obj_data_set->updateHeaderStatus($serialYear,$serialNo,'','RunQuery2');
		if($trans_response['type']=='fail'){
			$messageErr		= $trans_response['msg']; 
			$sqlErr			= $trans_response['sql'];
			$rollBackFlag	=1;
		}
		if($rollBackFlag!=1){
   			$header_array	= $obj_data_get->get_header_array($serialNo,$serialYear,'RunQuery2');
			$status			= $header_array['STATUS'];
			$savedLevels	= $header_array['LEVELS'];
			$approval		= $savedLevels+1-$status;
			
			$trans_response = $obj_data_set->approved_by_insert($serialYear,$serialNo,$session_userId,$approval,'RunQuery2');
			if($trans_response['type']=='fail'){
				$messageErr		= $trans_response['msg']; 
				$sqlErr			= $trans_response['sql'];
				$rollBackFlag	=1;
			}
		}
		
		$result		= $obj_data_get->get_details_result($serialNo,$serialYear,'RunQuery2');
		while($row	= mysqli_fetch_array($result)){
			
				$location 	= $_SESSION['CompanyID'];
				$company 	= $_SESSION['headCompanyId'];
				$userId 	= $_SESSION['userId'];
				$itemId 	= $row['ITEM'];
				$grnNo 	 	= $row['GRN_NO'];
				$grnYear	= $row['GRN_YEAR'];
				$grnDate 	= $row['GRN_DATE'];
				$oldRate 	= $row['RATE'];
				$currency 	= $row['CURRENCY'];
				$Qty	 	= $row['QTY'];
				$toSave++;
			 	
			   $stockBalQty	= $objwhouseget->get_stock_balance_bulk($location,$itemId,$grnNo,$grnYear,$grnDate,$oldRate,'RunQuery2');			
			
			   $stockBalQty = round($stockBalQty,4);
				
					if($stockBalQty>=$Qty){//change only qty
						$saveQty	=$Qty;
						$rate		=$oldRate;
						if($status == 1){
						$response['arrData'] = $objwhouseset->insert_ware_stocktransactions_bulk($company,$location,$serialNo,$serialYear,$grnNo,$grnYear,$grnDate,$rate,$currency,$itemId,-$saveQty,'Dispose',$userId);
						$saved				+=$response['arrData']['result'];
						$sqlString			.=$response['arrData']['q'];
						}
						else{
						$toSave=0;	
						}
					}
					else{
						$rollBackFlag = 1;
						$messageErr.='Stock Qty is less than the Adjust Qty ('.$itemId.'/'.$grnNo.'/'.$grnYear.'/'.$grnDate.'/'.$oldRate.'/'.$saveQty;
					}
		}
			
		//$rollBackFlag=1;
		if($rollBackFlag==1){
			$db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= $messageErr;
			$response['sql']		= $sqlErr;
		}
		else if($saved == $toSave){
   			$header_array			= $obj_data_get->get_header_array($serialNo,$serialYear,'RunQuery2');
			$response['type'] 		= "pass";
			if($header_array['STATUS']==1){ 
				if($header_array['USER']!=$userId){//if confiremed user and created user same, email won't be generate.
					$obj_data_set->sendFinlConfirmedEmailToUser($serialNo,$serialYear,$objMail,$mainPath,$root_path,'RunQuery2');
				}

				if($company == '1'){
                    updateAzureDBTables($serialNo,$serialYear,$location);
                }
				$response['msg'] 		= "Final Approval Raised successfully.";
			}
 			else {
				$response['msg'] 		= "Approved successfully.";
			}
			$db->commit();
		}
		else{
			$db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= $messageErr;
			$response['sql']		= $sqlErr;
		}
			echo  json_encode($response);
	}
	
else if($requestType=='reject')//save
	{
		
		$savedStatus	= true;
		$db->begin();
		
		$serialNo			= $_REQUEST["serialNo"];
		$serialYear			= $_REQUEST["year"];
	
		$header_array 		= $obj_data_get->get_header_array($serialNo,$serialYear,'RunQuery2');
		$permition_arr		= $obj_commonErr->get_permision_withApproval_confirm($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery2');
		$permision_confirm	= $permition_arr['permision'];
		
		if($permision_confirm !=1){
			$messageErr	= 'No Permision to Reject'; 
			$sqlErr		= '';
			$rollBackFlag =1;
		}
		else if($header_array['STATUS'] ==1){
			$messageErr	= 'No Permision to Reject. Final Approval Raised'; 
			$sqlErr		= '';
			$rollBackFlag =1;
		}

		$trans_response = $obj_data_set->updateHeaderStatus($serialYear,$serialNo,'0','RunQuery2');
		if($trans_response['type']=='fail'){
			$messageErr	= $trans_response['msg']; 
			$sqlErr		= $trans_response['sql'];
			$rollBackFlag=1;
		}
		if($rollBackFlag!=1){
			
			$trans_response = $obj_data_set->approved_by_insert($serialYear,$serialNo,$session_userId,0,'RunQuery2');
			if($trans_response['type']=='fail'){
				$messageErr	= $trans_response['msg']; 
				$sqlErr		= $trans_response['sql'];
				$rollBackFlag=1;
			}
		}
		
		if($rollBackFlag==1){
			$db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= $messageErr;
			$response['sql']		= $sqlErr;
		}
		else if($rollBackFlag==0){
   			$header_array			= $obj_data_get->get_header_array($serialNo,$serialYear,'RunQuery2');
			$response['type'] 		= "pass";
			if($header_array['STATUS']==0){ 
				if($header_array['USER']!=$userId){//if confiremed user and created user same, email won't be generate.
					$obj_data_set->sendRejecedEmailToUser($serialNo,$serialYear,$objMail,$mainPath,$root_path,'RunQuery2');
				}
				$response['msg'] 		= "Rejected successfully.";
			}
 			else{ 
				$response['msg'] 		= "Rejection failed.".$messageErr;
			}
			$db->commit();
 		}
		else{
			$db->rollback();
			$response['type'] 		= "fail";
			$response['msg'] 		= $messageErr;
			$response['sql']		= $sqlErr;
		}
			echo  json_encode($response);
	}


function updateAzureDBTables($disposal_no, $disposal_year,$company)
{
    global $objAzure;
    global $db;
    $environment = $objAzure->getEnvironment();
    $table = 'Inventory'.$environment;

    $azure_connection = $objAzure->connectAzureDB();
    $sql_select = "SELECT
                CONCAT(
                    DD.DISPOSE_NO,
                    '/',
                    DD.`YEAR`
                ) AS disposal_no,
                DD.ITEM,
                DD.QTY AS dblQty,
                mst_financecurrency.strCode AS currency,
                mst_item.strCode AS item_code,
                mst_item.strName AS description,
                mst_subcategory.strName AS subCategory,
                mst_maincategory.strName AS mainCategory,
                mst_maincategory.intSendDetails AS sendMainCat,
                mst_subcategory.intSendDetails AS sendSubCat,
                mst_item.dblLastPrice AS price,
                DH.LOCATION AS location,
                (SELECT
                        round(
                            sum(
                                (
                                    ware_grnheader.dblExchangeRate * ware_stocktransactions_bulk.dblQty * ware_stocktransactions_bulk.dblGRNRate
                                ) / 1.0000
                            ),
                            2
                        )
                    FROM
                        ware_stocktransactions_bulk
                    LEFT JOIN ware_grnheader ON ware_grnheader.intGrnNo = ware_stocktransactions_bulk.intGRNNo
                    AND ware_grnheader.intGrnYear = ware_stocktransactions_bulk.intGRNYear
                    WHERE
                        ware_stocktransactions_bulk.intDocumentNo = DD.DISPOSE_NO
                    AND ware_stocktransactions_bulk.intDocumntYear = DD.`YEAR`
                    AND ware_stocktransactions_bulk.intItemId = DD.ITEM
                    AND ware_stocktransactions_bulk.strType = 'Dispose'
                    GROUP BY
                        ware_stocktransactions_bulk.intDocumentNo,
                        ware_stocktransactions_bulk.intDocumntYear,
                        ware_stocktransactions_bulk.intItemId
                ) AS amount
            FROM
                ware_general_item_disposal_header DH
            INNER JOIN ware_general_item_disposal_details DD ON DH.DISPOSE_NO = DD.DISPOSE_NO AND DH.`YEAR` = DD.`YEAR`
            INNER JOIN mst_financecurrency ON DD.CURRENCY = mst_financecurrency.intId
            INNER JOIN mst_item ON DD.ITEM = mst_item.intId
            INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
            INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
            WHERE
                DH.DISPOSE_NO = '$disposal_no' AND DH.`YEAR` = '$disposal_year'
            GROUP BY DD.ITEM";

    $result = $db->RunQuery2($sql_select);
    $line_no = rand(100,1000);
    while ($row = mysqli_fetch_array($result)) {
        $disp_no = $row['disposal_no'];
        $item_category = $row['mainCategory'];
        $item_subcategory = $row['subCategory'];
        $item_code = $row['item_code'];
        $description = $row['description'];
        $amount = $row['amount'];
        $location = $row['location'];
        $successHeader = '0';
        $line_no++;
        $postingDate = date('Y-m-d H:i:s');
        $sendDetails = ($row['sendMainCat'] == 1 && $row['sendSubCat'] == 1)? true:false;

        if($sendDetails) {
            $sql_azure_header = "INSERT INTO $table (Document_Category, Document_No, Line_No,  External_Doc_No, Posting_Date, Item_Category, Item_Sub_Category, Item_Code, Description, Amount, Location) VALUES ('DisposalNote', '$disp_no', '$line_no', '$disp_no', '$postingDate', '$item_category', '$item_subcategory', '$item_code', '$description', '$amount','$location')";
            if ($azure_connection) {
                $getResultsHeader = $objAzure->runQuery($azure_connection, $sql_azure_header);
                if ($getResultsHeader != FALSE) {
                    $successHeader = '1';
                }
            }
            $sql_db_header = "INSERT INTO trn_financemodule_inventory (Document_Category, Document_No, Line_No, External_Doc_No, Posting_Date, Item_Category, Item_Sub_Category, Item_Code, Description, Amount, Location, deliveryStatus, deliveryDate) VALUES ('DisposalNote', '$disp_no', '$line_no','$disp_no', '$postingDate', '$item_category', '$item_subcategory', '$item_code', '$description', '$amount','$location','$successHeader', NOW())";
            $result_db_header = $db->RunQuery2($sql_db_header);
        }

    }

}
function getNextSysNo($db, $companyId, $transaction_type){
    $current_serial_no = $companyId.'00000';
    $sql = "SELECT `$transaction_type` FROM `trn_financemodule_sysNo` WHERE `COMPANY_ID` = '$companyId'";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    if($row[$transaction_type] != ''){
        $current_serial_no = $row[$transaction_type];
    }
    //update next serial no
    $sql_update = "UPDATE `trn_financemodule_sysNo` SET `$transaction_type`='$current_serial_no'+1 WHERE `COMPANY_ID`='$companyId'";
    $result = $db->RunQuery2($sql_update);
    return $current_serial_no;
}
$db->commit();
?>
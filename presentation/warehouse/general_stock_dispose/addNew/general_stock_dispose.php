<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
//ini_set('display_errors',1);
//**************************
// 		H B G KORALA 	   *
//**************************
  
include_once ("class/cls_commonErrorHandeling_get.php");
include_once ("class/warehouse/general_stock_dispose/cls_general_stock_dispose_get.php");
require_once ("class/warehouse/cls_warehouse_get.php");

$obj_commonErr		= new cls_commonErrorHandeling_get($db);
$obj_data_get		= new cls_general_stock_dispose_get($db);
$objwhouseget		= new cls_warehouse_get($db);

$location 			= $_SESSION['CompanyID'];
$serialNo 			= $_REQUEST['serialNo'];
$serialYear 		= $_REQUEST['year'];

$programName		='Genaral Stock Dispose';
$progrmCode			='P0804';

$header_array 		= $obj_data_get->get_header_array($serialNo,$serialYear,'RunQuery');
$detail_result		= $obj_data_get->get_details_result($serialNo,$serialYear,'RunQuery');
$intStatus			= $header_array['STATUS']; 
$permition_arr		= $obj_commonErr->get_permision_withApproval_save($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery');
$permision_save		= $permition_arr['permision'];
$permition_arr		= $obj_commonErr->get_permision_withApproval_confirm($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery');
$permision_confirm	= $permition_arr['permision'];

?>
<title>General Item Disposal</title>
<?php
/*include "include/javascript.html";
*/?>
<!--<script type="text/javascript" src="presentation/warehouse/general_stock_dispose/addNew/general_stock_dispose-js.js"></script>
-->
	

<form id="frmGeneral_stock_dispose" name="frmGeneral_stock_dispose" method="post" action="">
<div align="center">
		<div class="trans_layoutL">
		  <div class="trans_text">Genaral Item Disposal</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF">
    <td><table width="100%" border="0">
     <tr>
       <td>
         <table width="100%" border="0" cellpadding="0" cellspacing="0">
           <?php
		?>
           <tr>
             <td width="1%" class="normalfnt">&nbsp;</td>
             <td width="10%" height="22" class="normalfnt">Dispose No</td>
             <td width="29%"><input name="txtSerialNo" type="text" disabled="disabled" class="txtText" id="txtSerialNo" style="width:60px" value="<?php echo $serialNo ?>"/><input name="txtSerialYear" type="text" disabled="disabled" class="txtText" id="txtSerialYear" style="width:40px" value="<?php echo $serialYear ?>"/></td>
             <td width="13%">&nbsp;</td>
             <td width="26%">&nbsp;</td>
             <td width="7%" height="22" class="normalfnt">Date&nbsp;<span class="compulsoryRed">*</span></td>
             <td width="14%"><input name="dtDate" type="text" value="<?php if($serialNo){ echo substr($header_array['DATE'],0,10); }else { echo date("Y-m-d"); }?>" class="txtbox  validate[required]" id="dtDate" style="width:102px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');"/ <?php if($permision_save==0){  ?> disabled="disabled"<?php } ?> /><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
             </tr>
  <tr>
    <td width="1%" class="normalfnt">&nbsp;</td>
    <td width="10%" height="22" class="normalfnt">Note</td>
    <td width="29%"><textarea name="txtNote" id="txtNote" cols="35" rows="3" class="" <?php if($permision_save==0){  ?> disabled="disabled"<?php } ?>><?php echo $header_array['NOTE']; ?></textarea></td>
    <td width="13%">&nbsp;</td>
    <td width="26%">&nbsp;</td>
    <td width="7%" height="22" class="normalfnt">&nbsp;</td>
    <td width="14%" align="right" valign="bottom"><?php if($permision_save!=0){ ?><a id="butAddItems"   class="button green medium">Add</a><?php } ?></td>
    </tr>        </table>     
         </td>
     </tr>
      <tr>
        <td><div style="width:900px;height:350px;overflow:scroll" >
          <table width="100%" class="bordered" id="tblMain" >
            <tr>
              <th width="4%" >Del</th>
              <th width="12%" >Main Category</th>
              <th width="11%" >Sub Category</th>
              <th width="9%" height="22" >Item Code</th>
              <th width="14%" >Item Description</th>
              <th width="4%" >UOM</th>
              <th width="7%" >GRN No</th>
              <th width="6%" >Date</th>
              <th width="6%" >Rate</th>
              <th width="8%">Currency</th>
              <th width="9%">Stock Qty</th>
              <th width="6%">Qty</th>
              <th width="4%">(-)</th>
              </tr>
			<?php
            while($row = mysqli_fetch_array($detail_result)){
			$opCombo		='<select style="width:50px" name="cboOperation" id="cboOperation"   class="validate[required] operation"><option value="-1">-</option></select>';
			$stockBalQty	=$objwhouseget->get_stock_balance_bulk($location,$row['ITEM'],$row['GRN_NO'],$row['GRN_YEAR'],$row['GRN_DATE'],$row['RATE'],'RunQuery');			
			
            ?>
					<tr class="normalfnt">
					<td align="center" bgcolor="#FFFFFF"><img class="delImg" src="images/del.png" width="15" height="15" /></td>
					<td align="center" bgcolor="#FFFFFF" id="<?php echo $row['intMainCategory']; ?>"><?php echo $row['mainCat']; ?></td>
					<td align="center" bgcolor="#FFFFFF" id="<?php echo $row['intSubCategory']; ?>"><?php echo $row['subCat']; ?></td>
                                        <?php
                                        if($row['SUP_ITEM_CODE']!=null)
                                        {
                                        ?>
                                        <td align="center" bgcolor="#FFFFFF" id="<?php echo $row['ITEM']; ?>"><?php echo $row['SUP_ITEM_CODE']; ?></td>
                                        <?php
                                        }else{
                                        ?>
					<td align="center" bgcolor="#FFFFFF" id="<?php echo $row['ITEM']; ?>"><?php echo $row['strCode']; ?></td>
                                        <?php
                                        }
                                        ?>
					<td align="center" bgcolor="#FFFFFF" id="<?php echo $row['ITEM']; ?>" class="item"><?php echo $row['itemNm']; ?></td>
					<td align="center" bgcolor="#FFFFFF" id="<?php echo $row['unit']; ?>" class="uom"><?php echo $row['unit']; ?></td>
					<td align="center" bgcolor="#FFFFFF" id="<?php echo $row['GRN_NO'].'/'.$row['GRN_YEAR']; ?>"  class="grnNo"><?php echo $row['GRN_NO'].'/'.$row['GRN_YEAR']; ?></td>
					<td align="center" bgcolor="#FFFFFF" id="<?php echo $row['GRN_DATE']; ?>"  class="grnDate"><?php echo $row['GRN_DATE']; ?></td>
					<td align="center" bgcolor="#FFFFFF" id="<?php echo $row['RATE']; ?>"  class="grnRate"><?php echo $row['RATE']; ?></td>
					<td align="center" bgcolor="#FFFFFF" id="<?php echo $row['CURRENCY']; ?>"  class="currency"><?php echo $row['curr']; ?></td>
					<td align="right" bgcolor="#FFFFFF" id="<?php echo $stockBalQty; ?>"><?php echo $stockBalQty; ?></td>
					<td align="center" bgcolor="#FFFFFF" id="<?php echo $stockBalQty; ?>" ><input  id="<?php echo $stockBalQty; ?>" class="validate[required,custom[number],max[<?php echo $stockBalQty; ?>]] qty" style="width:80px;text-align:right" type="text" value="<?php echo $row['QTY']; ?>"/></td>
					<td align="center" bgcolor="#FFFFFF" id="<?php echo $stockBalQty; ?>" ><?php echo $opCombo; ?></td></tr>
            
            <?php
            }
            ?>            
            
            </table>
          </div></td>
      </tr>
      <tr>
      <td width="100%" align="center" bgcolor="">
                    <a class="button white medium" alt="New" name="butNew" width="92" height="24" id="butNew" tabindex="28">New</a>
                    <?php
                if(($form_permision['add']||$form_permision['edit']) && $permision_save==1)
				{
				?>
                    <a class="button white medium"  style="display:inline"   alt="Save" name="butSave"width="92" height="24"   id="butSave" tabindex="24">Save</a><?php } 
				
				?>
                    <a class="button white medium"  style=" <?php if(($intStatus>1) and ($permision_confirm==1)){  echo 'display:yes'; } else { ?>  <?php echo 'display:none';  } ?>"   alt="Approve" name="butConfirm"width="92" height="24"   id="butConfirm" tabindex="30">Approve</a>
                    <a class="button white medium"   alt="Report" name="butReport"width="92" height="24"   id="butReport" tabindex="31">Report</a>
                    <a class="button white medium" href="main.php"  alt="Close" name="butClose" width="92" height="24" border="0"   id="butClose" tabindex="27">Close</a></td>
                        
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
	<!-- items to prn -->
	<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1">
   <!-- <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php" style="width:800;height:800;border:0;overflow:hidden">
    </iframe>-->
    </div>

	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>


 
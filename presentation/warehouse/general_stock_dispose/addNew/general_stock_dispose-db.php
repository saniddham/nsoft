<?php
ini_set('display_errors',0);
	session_start();
	$backwardseperator 	= "../../../../";
	$mainPath 			= $_SESSION['mainPath'];
	$userId 			= $_SESSION['userId'];
	$location 			= $_SESSION['CompanyID'];
	$company 			= $_SESSION['headCompanyId'];
	$requestType 		= $_REQUEST['requestType'];
	
	include_once "{$backwardseperator}dataAccess/Connector.php";

	
	include_once ("../../../../class/warehouse/general_stock_dispose/cls_general_stock_dispose_get.php");
	include_once ("../../../../class/warehouse/general_stock_dispose/cls_general_stock_dispose_set.php");
	include_once "../../../../class/warehouse/cls_warehouse_set.php";
	include_once "../../../../class/warehouse/cls_warehouse_get.php";
	include_once  ("../../../../class/cls_commonFunctions_get.php");
	include_once  ("../../../../class/cls_commonErrorHandeling_get.php");
		//die($requestType);
	$objwhouseget		= new cls_warehouse_get($db);
	$objwhouseset		= new cls_warehouse_set($db);
	$obj_commonErr		= new cls_commonErrorHandeling_get($db);
	$obj_common			= new cls_commonFunctions_get($db);
	$obj_data_get		= new cls_general_stock_dispose_get($db);
	$obj_data_set		= new cls_general_stock_dispose_set($db);

	
	$db->begin();
	if($requestType=='loadMainCategories')
	{ 
		$response['arrCombo'] 	= $objwhouseget->getMainCategory_options();
		echo json_encode($response);
	}
	else if($requestType=='selectMainCategory')
	{
		$response['mainCat'] 	= $objwhouseget->getMainCategoryId($_REQUEST['subCat']);
		echo json_encode($response);
	}
	else if($requestType=='loadSubCategories')
	{
		$response['arrCombo'] 	= $objwhouseget->getSubCategory_options($_REQUEST['mainCat']);
		echo json_encode($response);
	}
	else if($requestType=='loadItems')
	{
		$response['arrCombo'] 	= $objwhouseget->getItem_options($_REQUEST['subCat']);
		echo json_encode($response);
	}
	else if($requestType=='loadGridData')
	{
		$response['arrData'] 	= $obj_data_get->getDetails($location,$_REQUEST['subCat'],$_REQUEST['itemId'],'RunQuery');
		echo json_encode($response);
	}
	else if($requestType=='save')//save
	{
		
		$arrHeader 	 	= json_decode($_REQUEST['arrHeader'],true);
		$serialNo 	 	= $arrHeader['serialNo'];
		$serialYear	 	= $arrHeader['Year'];
		$date 		 	= $arrHeader['date'];
		$remarks 	 	= replace1($arrHeader['remarks']);
 	
		$programName	='Genaral Stock Dispose';
		$progrmCode		='P0804';
	
		$arr 	 		= json_decode($_REQUEST['arr'], true);
		
		$db->begin();
		
		$header_array 	= $obj_data_get->get_header_array($serialNo,$serialYear,'RunQuery2');

		$permition_arr	= $obj_commonErr->get_permision_withApproval_save($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery2');
		$permision_save	= $permition_arr['permision'];
		
		
		if($serialNo==''){
			$editMode=0;
			$journal_array 	= $obj_common->GetSystemMaxNo('GENERAL_ITEM_DISPOSE_NO',$session_locationId);
			$serialNo		= $journal_array["max_no"];
			$serialYear		= date('Y');
			$approveLevels 	= $obj_common->getApproveLevels('Genaral Stock Dispose');
 			$status			=$approveLevels+1;
		}
		else{
			$editMode=1;
			$header_array 	= $obj_data_get->get_header_array($serialNo,$serialYear,'RunQuery2');
			
 			$status=$header_array['STATUS'];
			$approveLevels 	= $obj_common->getApproveLevels('Genaral Stock Dispose');
 			$status			=$approveLevels+1;
		}


		if($rollBackFlag != 1){
			if($editMode==0){
				$trans_response	=$obj_data_set->save_header($serialYear,$serialNo,$date,$remarks,$approveLevels,$status,$location,$userId,'RunQuery2');
 			}
			else{
				$trans_response	=$obj_data_set->update_header($serialYear,$serialNo,$date,$remarks,$approveLevels,$status,$location,$userId,'RunQuery2');
			}
 			if($trans_response['type']=='fail'){
				$rollBackMsg	= $trans_response['msg']; 
				$sqlErr			= $trans_response['sql'];
				$rollBackFlag	=1;
			}
		}
		
		$maxAppByStatus	=$obj_data_get->get_25_MaxAppByStatus($serialYear,$serialNo,'RunQuery2');
		$maxAppByStatus	=(int)$maxAppByStatus+1;
		$trans_response	=$obj_data_set->approved_by_update($serialYear,$serialNo,$maxAppByStatus,'RunQuery2');
		if($trans_response['type']=='fail'){
			$rollBackMsg	= $trans_response['msg']; 
			$sqlErr			= $trans_response['sql'];
			$rollBackFlag	=1;
		}
		
		$trans_response	= $obj_data_set->delete_details($serialYear,$serialNo,'RunQuery2');
		if($trans_response['type']=='fail'){
			$rollBackMsg	= $trans_response['msg']; 
			$sqlErr			= $trans_response['sql'];
			$rollBackFlag	=1;
		}
		
		if($rollBackFlag != 1){
			$toSave			=0;
			$saved			=0;
			$rollBackFlag	=0;
			$rollBackMsg 	=''; 
			$sqlString		='';
			foreach($arr as $arrVal)
			{
				$location 		= $_SESSION['CompanyID'];
				$company 		= $_SESSION['headCompanyId'];
				$userId 		= $_SESSION['userId'];
				$itemId 	 	= $arrVal['itemId'];
				$grnNo 	 		= $arrVal['grnNo'];
				$grnNoArray 	= explode('/',$grnNo);
				$grnNo 	 		= $grnNoArray[0];
				$grnYear 	 	= $grnNoArray[1];
				$grnDate 	 	= $arrVal['grnDate'];
				$oldRate 	 	= $arrVal['oldRate'];
				$currency 	 	= $arrVal['currency'];
				$operationType 	= $arrVal['operationType'];
				if($operationType=='-1'){
					$newRate	=0;	
				}
				if($newRate==''){
					$newRate	=0;	
				}
				$Qty 	 = $arrVal['Qty'];
				if($Qty==''){
					$Qty		=0;	
				}
			 	
			   $stockBalQty=$objwhouseget->get_stock_balance_bulk($location,$itemId,$grnNo,$grnYear,$grnDate,$oldRate,'RunQuery2');			
			   $stockBalQty = round($stockBalQty,4);
				$toSave++;
				
				if($stockBalQty>=$Qty){//change only qty
					$saveQty	=$Qty;
					$rate		=$oldRate;
					$response['arrData'] = $obj_data_set->save_details($serialNo,$serialYear,$company,$location,$grnNo,$grnYear,$grnDate,$rate,$currency,$itemId,$saveQty,$userId,'RunQuery2');
					if($response['arrData']['type']=='pass')
					$saved++;
					
					$sqlString	.=$response['arrData']['q'];
				}
				else{
					$rollBackFlag=1;
					$rollBackMsg.='Stock Qty is less than the Dispose Qty';
				}
				
		
			}//end of foreach
		}
			if($rollBackFlag==1){
				$db->rollback();
				$response['type'] 		= 'fail';
				$response['msg'] 		= $rollBackMsg;
				$response['q'] 			= $sqlErr;
			}
			else if($saved==0){
				$db->rollback();
				$response['type'] 		= 'fail';
				$response['msg'] 		= 'Not Saved';
				$response['q'] 			= '';
			}
			else if(($toSave==$saved)){
				$header_array 		= $obj_data_get->get_header_array($serialNo,$serialYear,'RunQuery2');
				$permition_arr		= $obj_commonErr->get_permision_withApproval_confirm($header_array['STATUS'],$header_array['LEVELS'],$session_userId,$progrmCode,'RunQuery2');
				$permision_confirm	= $permition_arr['permision'];
				$db->commit();
				$response['type'] 		= 'pass';
				$response['msg'] 		= 'Saved successfully.';
				$response['serialNo'] 	= $serialNo;
				$response['year'] 		= $serialYear;
				$response['confirmationMode'] 		= $permision_confirm;
				
			}
			else{
				$db->rollback();
				$response['type'] 		= 'fail';
				$response['msg'] 		= $db->errormsg;
				$response['q'] 			= $sqlString;
			}
			echo  json_encode($response);
	}
	$db->commit();


function replace1($str)
{
	  $str 	= 	str_replace('\\', "\\\\",	$str);
	  $str 	= 	str_replace("'","\'",	$str);
	  $str	=	str_replace("\n", "<br>",$str);  
	  
	  return $str;
}	
	
?>
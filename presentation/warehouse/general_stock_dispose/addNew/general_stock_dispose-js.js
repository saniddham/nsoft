var basePath	= "presentation/warehouse/general_stock_dispose/addNew/";	
var reportId	= 947;		

$(document).ready(function() {
  $("#frmGeneral_stock_dispose").validationEngine();
	
  $('#frmGeneral_stock_dispose #butSave').die('click').live('click',save);
 //-------------------------------------------- 

	$('#frmGeneral_stock_dispose #butConfirm').die('click').live('click',Confirm);
	$('#frmGeneral_stock_dispose #butReject').die('click').live('click',Reject);
	$('#frmGeneral_stock_dispose #butReport').die('click').live('click',Report);
 //-------------------------------------------- 

  $('#frmGeneral_stock_dispose_Popup #cboMainCategory').die('change').live('change',loadSubCategory);
 //-------------------------------------------- 
  $('#frmGeneral_stock_dispose_Popup #cboSubCategory').die('change').live('change',loadGridData);
 //-------------------------------------------- 
  $('#frmGeneral_stock_dispose_Popup #cboItem').die('change').live('change',loadGridData);
 //-------------------------------------------- 
 	$('#frmGeneral_stock_dispose #butAddItems').die('click').live('click',loadPopup);
 //-------------------------------------------- 
 	$('#frmGeneral_stock_dispose_Popup #butAdd').die('click').live('click',addClickedRows);
 //-------------------------------------------- 
 	$('#frmGeneral_stock_dispose_Popup #butClose1').die('click').live('click',disablePopup);
 //-------------------------------------------- 

    
	
//----------------------------	
$('.delImg').click(function(){
	$(this).parent().parent().remove();
});

//-----------------------------------------------------
$('#butClose').click(function(){
		//load('main.php');	
});
//--------------refresh the form-----------------------
	$('#frmGeneral_stock_dispose #butNew').click(function(){
		window.location.href = "?q=804";
		$('#frmGeneral_stock_dispose').get(0).reset();
		//window.location.href = basePath+'general_stock_dispose.php';
	});
//-----------------------------------------------------
});

//-------------------------------------
function alertx()
{
	$('#frmGeneral_stock_dispose #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmGeneral_stock_dispose #butDelete').validationEngine('hide')	;
}

function closePopUp(){
	
}
//----------------------------------------------
function add_new_row(table,rowcontent){
	
	//alert($('#frmGeneral_stock_dispose_popup #tblMain_popup').length);
        if ($(table).length>=0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
//----------------------------------------------- 
function clearRows_popup()
{
	var rowCount = document.getElementById('tblMain_popup').rows.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblMain_popup').deleteRow(1);
	}
}
//----------------------------------------------- 
function clearRows()
{
	var rowCount = document.getElementById('tblMain').rows.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblMain').deleteRow(1);
	}
}
//----------------------------------------------------
function loadMainCategory(){
	    var subCat = $('#cboSubCategory').val();
		var url 		= basePath+"general_stock_dispose-db.php?requestType=loadMainCategories";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"subCat="+subCat,
			async:false,
			success:function(json){
					$('#cboMainCategory').html(json.arrCombo)

			}
		});
}
//----------------------------------------------------
function selectMainCategory(){
	    var subCat = $('#cboSubCategory').val();
		var url 		= basePath+"general_stock_dispose-db.php?requestType=selectMainCategory";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"subCat="+subCat,
			async:false,
			success:function(json){ 
					$('#cboMainCategory').val(json.mainCat)

			}
		});
}
//----------------------------------------------------
function loadSubCategory(){
	    var mainCat = $('#cboMainCategory').val();
		var url 		= basePath+"general_stock_dispose-db.php?requestType=loadSubCategories";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"mainCat="+mainCat,
			async:false,
			success:function(json){
					$('#cboSubCategory').html(json.arrCombo)
			}
		});
}
//----------------------------------------------------
function loadPopup(){
		popupWindow3('1');
		$('#popupContact1').load(basePath+'general_stock_dispose_popup.php',function(){
	});
}
//----------------------------------------------------
function loadGridData(){ 

		showWaiting();

		 load_items_combo();
		 clearRows_popup();
	//$("#frmGeneral_stock_dispose_Popup #tblMain_popup tr:gt(1)").remove();
	    var subCat = $('#cboSubCategory').val();
	    var itemId = $('#cboItem').val();
		var url 		= basePath+"general_stock_dispose-db.php?requestType=loadGridData";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"subCat="+subCat+"&itemId="+itemId,
			async:false,
			success:function(json){
				
				var length = json.arrData.length;
				var arrData = json.arrData;
				for(var i=0;i<length;i++)
				{
					var maincatId=arrData[i]['intMainCategory'];	
					var mainCatName=arrData[i]['mainCatName'];
					var subCatId=arrData[i]['intSubCategory'];
					var subCatName=arrData[i]['subCatName'];	
					var code=arrData[i]['strCode'];
                                        var supItemCode=arrData[i]['SUP_ITEM_CODE'];
					var uom=arrData[i]['uom'];	
					var itemId=arrData[i]['intItemId'];	
					var itemName=arrData[i]['itemName'];
					var grnNo=arrData[i]['intGRNNo'];	
					var grnYear=arrData[i]['intGRNYear'];
					var currency=arrData[i]['currency'];
					var currencyId=arrData[i]['intCurrencyId'];
					var date=arrData[i]['dtGRNDate'];	
					var rate=arrData[i]['dblGRNRate'];
					var stockBal=arrData[i]['stockBal'];	
					var opCombo='<select style="width:50px" name="cboOperation" id="cboOperation"   class="validate[required] operation"><option value="-1">-</option></select>';
//alert(opCombo);
					var content='<tr class="normalfnt">';
					content +='<td align="center" bgcolor="#FFFFFF" id=""><input type="checkbox" id="chkSel" class="chkP" /></td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'" class="mainCatP">'+mainCatName+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'" class="subCatP">'+subCatName+'</td>';
                                        if(supItemCode!=null)
                                        {
                                        content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="codeP">'+supItemCode+'</td>';   
                                        }else{
					content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="codeP">'+code+'</td>';
                                            }
					content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="itemP">'+itemName+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+uom+'" class="uomP">'+uom+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+currencyId+'"  class="currencyP">'+currency+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+grnNo+'/'+grnYear+'"  class="grnNoP">'+grnNo+'/'+grnYear+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+date+'"  class="grnDateP">'+date+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+rate+'"  class="grnRateP">'+rate+'</td>';
					content +='<td align="right" bgcolor="#FFFFFF" id="'+stockBal+'" class = "stockBalP">'+stockBal+'</td>';
					content +='</tr>';

					//$("#frmGeneral_stock_dispose_Popup #tblMain_popup > tbody:last").append(content); 
					//alert(content);
					add_new_row('#frmGeneral_stock_dispose_Popup #tblMain_popup',content);
					
				}
				
				checkAlreadySelected();

			}
		});
					hideWaiting();
}
//----------------------------------------------------
function addClickedRows()
{
	var rowCount = document.getElementById('tblMain_popup').rows.length;//alert(rowCount);
	var totRetAmmount=0;

	var i=0;
	$('#tblMain_popup .chkP').each(function(){
		i++;
		if(($(this).is(":checked")) && (!$(this).is(":disabled"))){
			
				var maincatId		=$(this).parent().parent().find(".mainCatP").attr('id');
				var mainCatName		=$(this).parent().parent().find(".mainCatP").html();
				var subCatId		=$(this).parent().parent().find(".subCatP").attr('id');
				var subCatName		=$(this).parent().parent().find(".mainCatP").html();	
				var code			=$(this).parent().parent().find(".codeP").html();
				var uom				=$(this).parent().parent().find(".uomP").html();
				var itemId			=$(this).parent().parent().find(".itemP").attr('id');
				var itemName		=$(this).parent().parent().find(".itemP").html();
				var grnNo			=$(this).parent().parent().find(".grnNoP").attr('id');	
				var currency		=$(this).parent().parent().find(".currencyP").html();		
				var currencyId		=$(this).parent().parent().find(".currencyP").attr('id');	
				var date			=$(this).parent().parent().find(".grnDateP").html();		
				var rate			=$(this).parent().parent().find(".grnRateP").html();	
				var stockBal		=$(this).parent().parent().find(".stockBalP").attr('id');	
				var opCombo			='<select style="width:50px" name="cboOperation" id="cboOperation"   class="validate[required] operation"><option value="-1">-</option></select>';
			
		
			var qty='';

				var content='<tr class="normalfnt">';
					content +='<td align="center" bgcolor="#FFFFFF"><img class="delImg" src="images/del.png" width="15" height="15" /></td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'">'+mainCatName+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'">'+subCatName+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'">'+code+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="item">'+itemName+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+uom+'" class="uom">'+uom+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+grnNo+'"  class="grnNo">'+grnNo+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+date+'"  class="grnDate">'+date+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+rate+'"  class="grnRate">'+rate+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+currencyId+'"  class="currency">'+currency+'</td>';
					content +='<td align="right" bgcolor="#FFFFFF" id="'+stockBal+'">'+stockBal+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+stockBal+'" ><input  id="'+stockBal+'" class="validate[required,custom[number],max['+stockBal+']] qty" style="width:80px;text-align:right" type="text" value="0"/></td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+stockBal+'" >'+opCombo+'</td></tr>';
					
					add_new_row('#frmGeneral_stock_dispose #tblMain',content);

		//----------------------------	
		$('.delImg').click(function(){
			$(this).parent().parent().remove();
		});
		
		}
	});
	disablePopup();
	closePopUp();
}
//---------------------------------------------------
function load_items_combo()
{
	var mainCategory 	= $('#cboMainCategory').val();
	var subCategory 	= $('#cboSubCategory').val();
	var itemId		 	= $('#cboItem').val();
	
	if(mainCategory==''){
		alert("Please select the main category");
		return false;
	}
	
	var url 		= basePath+"general_stock_dispose-db.php?requestType=loadItems";
	var httpobj = $.ajax({
		url:url,
		dataType:'json',
		data:"subCat="+subCategory,
		async:false,
		success:function(json){
				$('#cboItem').html(json.arrCombo)
				$('#cboItem').val(itemId)
		}
	});

}
//-----------------------------------------------------
function Report()
{
	if($('#frmGeneral_stock_dispose #txtSerialNo').val()=="")
	{
		alert("No details available to view report.");
		return;
	}
 	var url  = "?q="+reportId+"&serialNo="+$('#frmGeneral_stock_dispose #txtSerialNo').val();
	    url += "&year="+$('#frmGeneral_stock_dispose #txtSerialYear').val();
 	window.open(url);
}
function Reject()
{
 	var url  = "?q="+reportId+"&serialNo="+$('#frmGeneral_stock_dispose #txtSerialNo').val();
	    url += "&year="+$('#frmGeneral_stock_dispose #txtSerialYear').val();
	    url += "&mode=Reject";
	window.open(url);

}
function Confirm()
{
	
 	var url  = "?q="+reportId+"&serialNo="+$('#frmGeneral_stock_dispose #txtSerialNo').val();
	    url += "&year="+$('#frmGeneral_stock_dispose #txtSerialYear').val();
	    url += "&mode=Confirm";
 	window.open(url);
}
//------------------------------------------------------------
function checkAlreadySelected(){
	
	$('#tblMain .item').each(function(){
		
				var itemId	= $(this).attr('id');
				var grnNo	= $(this).parent().find(".grnNo").html();
				var grnDate	= $(this).parent().find(".grnDate").html();
				var rate	= $(this).parent().find(".grnRate").html();
				var j=0;
			$('#tblMain_popup .chkP').each(function(){
				j++;
				var itemIdP			=$(this).parent().parent().find(".itemP").attr('id');
				var grnNoP			=$(this).parent().parent().find(".grnNoP").attr('id');	
				var grnDateP		=$(this).parent().parent().find(".grnDateP").html();		
				var rateP			=$(this).parent().parent().find(".grnRateP").html();	
				
				if((itemId==itemIdP) && (grnNo==grnNoP)&& (grnDate==grnDateP)&& (rate==rateP)){
					document.getElementById('tblMain_popup').rows[j].cells[0].childNodes[0].checked=true;
					document.getElementById('tblMain_popup').rows[j].cells[0].childNodes[0].disabled=true;
				}
			});
	});
}
//-----------------------------------------------------------------------
function save(){
	
	var requestType = '';
	if ($('#frmGeneral_stock_dispose').validationEngine('validate'))   
    { 
 		showWaiting();

			var data = "requestType=save";

			var rowCount = document.getElementById('tblMain').rows.length;
			if(rowCount==1){
				alert("items not selected to Adjust");
				hideWaiting();
				return false;				
			}
			var toSave=0;
			$('#tblMain .item').each(function(){
				var Qty	= $(this).parent().find(".qty").val();
				if((Qty==0)){
					toSave++;
				}
			});
			
			if(toSave==rowCount-1){
				alert("No changes to Save");
				hideWaiting();
				return false;				
			}
			
			var row = 0;
			
		var arrHeader = "{";
							arrHeader += '"serialNo":"'+$('#txtSerialNo').val()+'",' ;
							arrHeader += '"Year":"'+$('#txtSerialYear').val()+'",' ;
							arrHeader += '"remarks":'+URLEncode_json($('#txtNote').val())+',' ;
							arrHeader += '"date":"'+$('#dtDate').val()+'"' ;
			arrHeader += ' }';
		
		
			var arr="[";
			
			$('#tblMain .item').each(function(){
				
				var itemId	= $(this).attr('id');
				var grnNo	= $(this).parent().find(".grnNo").html();
				var grnDate	= $(this).parent().find(".grnDate").html();
				var oldRate	= $(this).parent().find(".grnRate").html();
				var currency	= $(this).parent().find(".currency").attr('id');
				var operationType	= $(this).parent().find(".operation").val();
				var Qty	= $(this).parent().find(".qty").val();
					
					if((Qty>0) ){
						
						arr += "{";
						arr += '"itemId":"'+	itemId +'",' ;
						arr += '"grnNo":"'+	grnNo +'",' ;
						arr += '"grnDate":"'+	grnDate +'",' ;
						arr += '"oldRate":"'+	oldRate +'",' ;
						arr += '"currency":"'+		currency +'",' ;
						arr += '"operationType":"'+		operationType +'",' ;
						arr += '"Qty":"'+		Qty +'"' ;
						arr +=  '},';
						
					}
			});
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data += "&arrHeader="	+	arrHeader;
			data += "&arr="	+	arr;

		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = basePath+"general_stock_dispose-db.php";
     	var obj = $.ajax({
			url:url,
			type:'post',
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmGeneral_stock_dispose #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t=setTimeout("alertx()",99000);
						$('#txtSerialNo').val(json.serialNo);
						$('#txtSerialYear').val(json.year);
						if(json.confirmationMode==1){
							$('#butConfirm').show();
						}
						else{
							$('#butConfirm').hide();
						}
						//return;
					}
				},
			error:function(xhr,status){
					
					$('#frmGeneral_stock_dispose #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
			
				/*	}
				}});*/
	}
					hideWaiting();
	
}

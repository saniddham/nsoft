			
$(document).ready(function() {
	
	$("#frmUnAllocation").validationEngine();
		
  //permision for add 
  if(intAddx)
  {
 	$('#frmUnAllocation #butNew').show();
	$('#frmUnAllocation #butSave').show();
  }
  
  //permision for edit 
  if(intEditx)
  {
  	$('#frmUnAllocation #butSave').show();
  }
  
  //permision for delete
  if(intDeletex)
  {
  	$('#frmUnAllocation #butDelete').show();
  }
  
  //permision for view
  if(intViewx)
  {
  }
  
 //-------------------------------------------- 
  $('#frmUnAllocation #cboStyle').change(function(){
	    var style = $('#cboStyle').val();
		var url 		= "unAllocation-db-get.php?requestType=loadOrderNos&style="+style;
		var httpobj 	= $.ajax({url:url,async:false})

					document.getElementById("cboOrderNo").innerHTML=httpobj.responseText;
		
  });
 //-------------------------------------------- 
  $('#frmUnAllocation #cboOrderNo').change(function(){
	    var style = $('#cboStyle').val();
	    var orderNo = $('#cboOrderNo').val();
		var url 		= "unAllocation-db-get.php?requestType=loadItems&style="+style+"&orderNo="+orderNo;
		var httpobj 	= $.ajax({url:url,async:false})

					document.getElementById("cboItem").innerHTML=httpobj.responseText;
		
  });
  //-------------------------------------------------------
  $('#frmUnAllocation #butSerach').click(function(){
	document.getElementById("frmUnAllocation").submit(); 
  });
  //-------------------------------------------------------
 
  $('#frmUnAllocation #butSave').click(function(){
	  showWaiting();
	var requestType = '';
	if ($('#frmUnAllocation').validationEngine('validate'))   
    { 

		var data = "requestType=save";
			var rowCount = document.getElementById('tblitems').rows.length;
			if(rowCount==1){
				alert("items not selected to Un Allocate");
				return false;				
			}
			var row = 0;
			
			var arr="[";
			
			var orderNo = 	$('#frmUnAllocation #cboOrderNo').val();
			var style = 	$('#frmUnAllocation #cboStyle').val();
			for(var i=1;i<rowCount;i++)
			{
					 arr += "{";
					 
					var itemId = 	document.getElementById('tblitems').rows[i].cells[1].id;
					var Qty = 	document.getElementById('tblitems').rows[i].cells[4].childNodes[0].value;
					
					if(Qty>0){

						arr += '"orderNo":"'+	orderNo +'",' ;
						arr += '"style":"'+		style +'",' ;
						arr += '"itemId":"'+	itemId +'",' ;
						arr += '"Qty":"'+		Qty +'"' ;
						arr +=  '},';
						
					}
			}
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;

		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = "unAllocation-db-set.php";
     	var obj = $.ajax({
			url:url,
			
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmUnAllocation #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						//$('#frmSampleInfomations').get(0).reset();
						//saveImage(json.sampleNo,json.sampleYear,json.revisionNo);
						var t=setTimeout("alertx2()",2000);
						return;
					}
				},
			error:function(xhr,status){
					$('#frmUnAllocation #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
   
	
	//--------------refresh the form----------
	$('#frmUnAllocation #butNew').click(function(){
		$('#frmUnAllocation').get(0).reset();
		clearRows();
		$('#frmUnAllocation #cboStyle').val('');
		$('#frmUnAllocation #cboStyle').change()
	});

//-----------------------------------
$('#butReport').click(function(){
	if($('#txtPoNo').val()!=''){
		window.open('../listing/rptIssue.php?issueNo='+$('#txtIssueNo').val()+'&year='+$('#txtIssueYear').val());	
	}
	else{
		alert("There is no Issue No to view");
	}
});
/*//----------------------------------	
$('#butConfirm').click(function(){
	if($('#txtPoNo').val()!=''){
		window.open('../listing/rptIssue.php?issueNo='+$('#txtIssueNo').val()+'&year='+$('#txtIssueYear').val()+'&approveMode=1');	
	}
	else{
		alert("There is no Issue No to confirm");
	}
});
*///-----------------------------------------------------
$('#butClose').click(function(){
		//load('main.php');	
});
});
//----------end of ready -------------------------------


//-------------------------------------
function alertx()
{
	$('#frmUnAllocation #butSave').validationEngine('hide')	;
}
function alertx2()
{
	$('#frmUnAllocation #butSave').validationEngine('hide')	;
	document.location.href=document.location.href;
}
function alertDelete()
{
	$('#frmUnAllocation #butDelete').validationEngine('hide')	;
}

//----------------------------------------------- 
function clearRows()
{
	//var rowCount = $('#tblDispatchPopup >tbody >tr').length;
	var rowCount = document.getElementById('tblitems').rows.length;
	//alert(rowCount);
	//var cellCount = document.getElementById('tblMain').rows[1].cells.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblitems').deleteRow(1);
			
	}
}

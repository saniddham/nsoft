<?php 
	session_start();
	$backwardseperator = "../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$companyId 	= $_SESSION['CompanyID'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	
	$arr 		= json_decode($_REQUEST['arr'], true);
	
/*	$programName='Material Issued Note';
	$programCode='P0231';

	$ApproveLevels = (int)getApproveLevel('Material Issued Note');
	$issueApproveLevel = $ApproveLevels+1;
*/	
//------------save---------------------------	
	if($requestType=='save')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		//-----------delete and insert to detail table-----------------------
		$rollBackMsg ="Maximum un allocatable quantities are";
			foreach($arr as $arrVal)
			{
				$location 	= $_SESSION['CompanyID'];
				$company 	= $_SESSION['headCompanyId'];
				$orderNo 	 = $arrVal['orderNo'];
				$orderNoArray   = explode('/',$orderNo);
				if($orderNoArray[0]==''){
					$orderNoArray[0]=0;
				}
				if($orderNoArray[1]==''){
					$orderNoArray[1]=0;
				}
				$style 		 = $arrVal['style'];
				$itemId 	 = $arrVal['itemId'];
				$Qty 		 = $arrVal['Qty'];
				
			 $sql="SELECT
					mst_item.strName
					FROM
					mst_item
					WHERE
					mst_item.intId =  '$itemId'";
				$result = $db->RunQuery2($sql);
				$row=mysqli_fetch_array($result);
				$itemName = $row['strName'];
		
				$stockBalQty=getStockBalance($company,$location,$orderNoArray[0],$orderNoArray[1],$style,$itemId);

				if($Qty>$stockBalQty){
				//	call roll back--------****************
					$rollBackFlag=1;
					$rollBackMsg .="</br> ".$orderNoArray[0]."/".$orderNoArray[1]."-".$style."-".$itemName." =".$stockBalQty;
					//exit();
				}

				//----------------------------
				if($rollBackFlag!=1){
						$resultB = getGrnWiseStockBalance($location,$orderNoArray[0],$orderNoArray[1],$style,$itemId);
						while($rowB=mysqli_fetch_array($resultB)){
								if(($Qty>0) && ($rowB['stockBal'])>0){
								if(($Qty>0) && ($rowB['stockBal']>=$Qty)){
									$saveQty=$Qty;
									$Qty=0;
									}
									else if($Qty>$rowB['stockBal']){
									$saveQty=$rowB['stockBal'];
									$Qty=$Qty-$saveQty;
									}
									$grnNo=$rowB['intGRNNo'];
									$grnYear=$rowB['intGRNYear'];
									$grnDate=$rowB['dtGRNDate'];
									$grnRate=$rowB['dblGRNRate'];	
									$currency=loadCurrency($grnNo,$grnYear);
																
						$sql = "INSERT INTO `ware_stocktransactions` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intOrderNo,intOrderYear,strStyleNo,intItemId,dblQty,strType,intUser,dtDate) 
							VALUES ('$company','$location','0','0','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$orderNoArray[0]','$orderNoArray[1]','$style','$itemId','-$saveQty','UNALLOCATE','$userId',now())";
						$result = $db->RunQuery2($sql);
						
						$sqlI = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intUser,dtDate) 
							VALUES ('$company','$location','0','0','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$itemId','$saveQty','UNALLOCATE','$userId',now())";
						$resultI = $db->RunQuery2($sqlI);
								}
						}
					if($result==1){
					$saved++;
					}
				}
				$toSave++;
			}
		//echo $rollBackFlag;
		
		if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= '';
		}
		else if(($result)){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			if($editMode==1)
			$response['msg'] 		= 'Updated successfully.';
			else
			$response['msg'] 		= 'Saved successfully.';
			
			$response['serialNo'] 		= $serialNo;
			$response['year'] 		= $year;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		
		$db->CloseConnection();		
	}
//----------------------------------------
	echo json_encode($response);
//-----------------------------------------	
	function getStockBalance($company,$location,$orderNo,$orderYear,$style,$item)
	{
		global $db;
				$sql="SELECT DISTINCT
						ware_stocktransactions.intItemId,
						mst_item.strName,
						Sum(ware_stocktransactions.dblQty) AS qty
						FROM
						ware_stocktransactions
						Inner Join mst_item ON ware_stocktransactions.intItemId = mst_item.intId
						WHERE
						ware_stocktransactions.intLocationId =  '$location' AND 
						ware_stocktransactions.strStyleNo =  '$style' AND
						ware_stocktransactions.intOrderNo =  '$orderNo' AND
						ware_stocktransactions.intOrderYear =  '$orderYear'  
						AND ware_stocktransactions.intItemId =  '$item' 
						GROUP BY
						ware_stocktransactions.intItemId,
						mst_item.strName";

		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		return val($row['qty']);	
	}
//--------------------------------------------------------------
function getGrnWiseStockBalance($location,$orderNo,$orderYear,$styleNo,$item)
{
	global $db;
	 $sql = "SELECT
			Sum(ware_stocktransactions.dblQty) AS stockBal,
			ware_stocktransactions.intGRNNo,
			ware_stocktransactions.intGRNYear,
			ware_stocktransactions.dtGRNDate,
			ware_stocktransactions.dblGRNRate 
			FROM ware_stocktransactions  
			WHERE
			ware_stocktransactions.intItemId =  '$item' AND
			ware_stocktransactions.intLocationId =  '$location' AND 
			ware_stocktransactions.intOrderNo='$orderNo' AND 
			ware_stocktransactions.intOrderYear='$orderYear' AND 
			ware_stocktransactions.strStyleNo='$styleNo' 
			GROUP BY
			ware_stocktransactions.intItemId,
			ware_stocktransactions.intGRNNo,
			ware_stocktransactions.intGRNYear 
			ORDER BY
			ware_stocktransactions.dtGRNDate ASC";

	$result = $db->RunQuery2($sql);
	return $result;	
}
//--------------------------------------------------------------
function loadCurrency($grnNo,$grnYear)
{
	global $db;
	 $sql = "SELECT
			trn_poheader.intCurrency
			FROM
			ware_grnheader
			Inner Join trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
			WHERE
			ware_grnheader.intGrnNo =  '$grnNo' AND
			ware_grnheader.intGrnYear =  '$grnYear'";

	$result = $db->RunQuery2($sql);
	$row=mysqli_fetch_array($result);
	return $row['intCurrency'];;	
}
//--------------------------------------------------------------
?>
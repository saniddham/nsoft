<?php
session_start();
$backwardseperator = "../../../";
$mainPath = $_SESSION['mainPath'];

$thisFilePath =  $_SERVER['PHP_SELF'];

include  "{$backwardseperator}dataAccess/permisionCheck.inc";
$sql = "SELECT DISTINCT intCompanyId From mst_locations WHERE intId=".$_SESSION["CompanyID"]."";
$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
	$companyId = $row['intCompanyId']; 
}
 $location 	= $_SESSION['CompanyID'];
 $company 	= $_SESSION['headCompanyId'];

 $style = $_GET['cboStyle'];
 $orderNo = $_GET['cboOrderNo'];
 $orderNoArray 	 = explode('/',$orderNo);
 $item = $_GET['cboItem'];
 $sql="SELECT
		mst_item.strName
		FROM
		mst_item
		WHERE
		mst_item.intId =  '$item'";
	$result = $db->RunQuery($sql);
	$row=mysqli_fetch_array($result);
	$itemName = $row['strName'];
?>
<script type="application/javascript" >
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Un Allocation</title>
<link href="../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../css/promt.css" rel="stylesheet" type="text/css" />

<script type="application/javascript" src="../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="unAllocation-js.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/script.js"></script>

<link rel="stylesheet" type="text/css" href="../../../libraries/calendar/theme.css" />
<script src="../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../libraries/calendar/runCalender.js" type="text/javascript"></script>

<link rel="stylesheet" href="../../../libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="../../../libraries/validate/template.css" type="text/css">

</head>

<body>
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>

<script src="../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../libraries/javascript/jquery-impromptu.min.js"></script>

<form id="frmUnAllocation" name="frmUnAllocation"  method="get" action="unAllocation.php" autocomplete="off">
<div align="center">
  <div class="trans_layoutD" style="width:800">
	<div class="trans_text" >Un Allocation</div>
<table width="800">
<tr>
  <td width="100%"><table width="100%">
<tr>
      <td class="normalfnt">&nbsp;</td>
      <td class="normalfnt" align="left" >Style No
      </td>
      <td class="normalfnt" align="left"><select name="cboStyle" id="cboStyle" style="width:200px" class="validate[required]">
        <option value=""></option>
        <?php  $sql = "SELECT DISTINCT
						ware_stocktransactions.strStyleNo
						FROM ware_stocktransactions WHERE
ware_stocktransactions.intLocationId =  '$location' 
						";
						$result = $db->RunQuery($sql);
						while($row=mysqli_fetch_array($result))
						{
							if($row['strStyleNo']==$style)
							echo "<option value=\"".$row['strStyleNo']."\" selected=\"selected\">".$row['strStyleNo']."</option>";	
							else
							echo "<option value=\"".$row['strStyleNo']."\">".$row['strStyleNo']."</option>";
						}
        				?>
      </select>
      </td>
      <td bgcolor="#FFFFFF" class="">&nbsp;</td>
      <td bgcolor="#FFFFFF" class=""></td>
    </tr><tr>
      <td class="normalfnt">&nbsp;</td>
      <td width="132" align="left" class="normalfnt">Order No
      </td>
      <td width="549" align="left" class="normalfnt"><select name="cboOrderNo" id="cboOrderNo" style="width:200px" class="validate[required]">
        <option value="<?php echo $orderNo ?>"><?php echo $orderNo ?></option>
      </select>
      </td>
      <td width="29" bgcolor="#FFFFFF" class="">&nbsp;</td>
      <td width="22" bgcolor="#FFFFFF" class=""></td>
    </tr>
<tr>
      <td class="normalfnt">&nbsp;</td>
      <td class="normalfnt" align="left" >Item
      </td>
      <td class="normalfnt" align="left"><select name="cboItem" id="cboItem" style="width:200px" class="">
        <option value="<?php echo $item ?>"><?php echo $itemName ?></option>
      </select>
      </td>
      <td bgcolor="#FFFFFF" class=""><img src="../../../images/search.png" width="25" height="25" alt="search" id="butSerach" /></td>
      <td bgcolor="#FFFFFF" class=""></td>
    </tr>   
<tr>
      <td bgcolor="#FFFFFF" class=""></td>
      <td bgcolor="#FFFFFF" class=""></td>
      <td bgcolor="#FFFFFF" class=""></td>
      <td bgcolor="#FFFFFF" class=""></td>
      <td bgcolor="#FFFFFF" class=""></td>
</tr>
     <tr>
      <td width="10" class="normalfnt">&nbsp;</td>
      <td colspan="4" rowspan="5" class="normalfnt">
      <div id="divTable1" style="overflow:scroll;width:100%;height:300px"  >
<table width="97%" id="tblitems" border="0" cellpadding="0" cellspacing="1" bgcolor="#FF9900">
        <tr class="">
          <td width="44" bgcolor="#FAD163" class="normalfntMid"><input type="checkbox" name="chkAll" id="chkAll" />
          </td>
          <td width="150"  height="24" bgcolor="#FAD163" class="normalfntMid"><strong>Item</strong></td>
          <td width="150"  height="24" bgcolor="#FAD163" class="normalfntMid"><strong>Allocated Qty</strong></td>
          <td width="150"  height="24" bgcolor="#FAD163" class="normalfntMid"><strong>Stock balance</strong></td>
          <td width="150"  height="24" bgcolor="#FAD163" class="normalfntMid"><strong>Un Allocate Qty</strong></td>
        </tr>
        
        <?php
				$sql="SELECT DISTINCT
						ware_stocktransactions.intItemId,
						mst_item.strName,
						Sum(ware_stocktransactions.dblQty) AS qty
						FROM
						ware_stocktransactions
						Inner Join mst_item ON ware_stocktransactions.intItemId = mst_item.intId
						WHERE
						ware_stocktransactions.intLocationId =  '$location' AND 
						ware_stocktransactions.strStyleNo =  '$style' AND
						ware_stocktransactions.intOrderNo =  '$orderNoArray[0]' AND
						ware_stocktransactions.intOrderYear =  '$orderNoArray[1]' ";
						if($item!=''){
				$sql .=" AND ware_stocktransactions.intItemId =  '$item'";
						}
						$sql .="GROUP BY
						ware_stocktransactions.intItemId,
						mst_item.strName";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					$itemId=$row['intItemId'];
					$item=$row['strName'];
					$stockBal=$row['qty'];
					$unAllowQty=0;
					
				$sqlb="SELECT DISTINCT
						ware_stocktransactions.intItemId,
						mst_item.strName,
						Sum(ware_stocktransactions.dblQty) AS allocQty
						FROM
						ware_stocktransactions
						Inner Join mst_item ON ware_stocktransactions.intItemId = mst_item.intId
						WHERE
						ware_stocktransactions.intLocationId =  '$location' AND 
						ware_stocktransactions.strType in ('ALLOCATE','UNALLOCATE') AND 
						ware_stocktransactions.strStyleNo =  '$style' AND
						ware_stocktransactions.intOrderNo =  '$orderNoArray[0]' AND
						ware_stocktransactions.intOrderYear =  '$orderNoArray[1]' ";
						if($itemId!=''){
				$sqlb .=" AND ware_stocktransactions.intItemId =  '$itemId'";
						}
						$sqlb .="GROUP BY
						ware_stocktransactions.intItemId,
						mst_item.strName";
						//echo $sqlb;
					$resultb = $db->RunQuery($sqlb);
					$rowb=mysqli_fetch_array($resultb);
					$allocatedQty=$rowb['allocQty'];
					?>
                <tr class="normalfnt">
              <td bgcolor="#FFFFFF" class="normalfntMid"><input name="chkSelect" type="checkbox" value="<?php echo $customerId ; ?>" /></td>
              <td bgcolor="#FFFFFF" class="normalfntMid" id="<?php echo $itemId ; ?>"><?php echo $item ?></td>
              <td bgcolor="#FFFFFF" class="normalfntMid" id="<?php echo $allocatedQty ; ?>"><?php echo $allocatedQty ?></td>
              <td bgcolor="#FFFFFF" class="normalfntMid" id="<?php echo $stockBal ; ?>"><?php echo $stockBal ?></td>
              <td bgcolor="#FFFFFF" class="normalfntMid" id="<?php echo $stockBal ; ?>"><input  id="<?php echo $stockBal ?>" class="validate[required,custom[integer],max[<?php echo $stockBal ?>]] calculateValue" style="width:80px;text-align:center" type="text" value="<?php echo $unAllowQty ?>"/></td>
                </tr>        
				<?php		
				}
				
		?>
        
        
     </table>  
    </div> 
           </td>
      </tr>
      
    <tr>
      <td height="21" class="normalfnt">&nbsp;</td>
      <td width="22" bgcolor="#FFFFFF" class="">&nbsp;</td>
      </tr>
    <tr>
      <td height="21" class="normalfnt">&nbsp;</td>
      <td bgcolor="#FFFFFF" class="">&nbsp;</td>
      </tr>
    <tr style="display:none">
      <td height="21" class="normalfnt">&nbsp;</td>
      <td bgcolor="#FFFFFF" class="">&nbsp;</td>
      </tr>
    <tr>
      <td height="21" class="normalfnt">&nbsp;</td>
      <td bgcolor="#FFFFFF" class="">&nbsp;</td>
      </tr>
  </table></td>
</tr>
<?php
$editMode=1;
$confirmatonMode=1;
?>
<tr>
  <td align="center">
  <table width="100%" class="tableBorder_allRound">
<tr>
        <td align="center" class="tableBorder_allRound"><img src="../../../images/Tnew.jpg" width="92" height="24" id="butNew" name="butNew" class="mouseover" /><?php if($editMode==1){ ?><img src="../../../images/Tsave.jpg" width="92" height="24" id="butSave" name="butSave" class="mouseover" /><?php } ?><img src="../../../planning/img/Tclose.jpg" width="92" height="24" class="mouseover" /></td>
      </tr>  </table>
  </td>
</tr>
<tr>
  <td align="center">&nbsp;</td>
</tr>
    </table>
</div>
</div>
<?php

?>
</form>
</body>
</html>
<?php
	session_start();
	$backwardseperator = "../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	if($requestType=='loadOrderNos')
	{
		$style  = $_REQUEST['style'];
		
		 $sql = "SELECT DISTINCT
				ware_stocktransactions.intOrderNo,
				ware_stocktransactions.intOrderYear
				FROM ware_stocktransactions
				WHERE
				ware_stocktransactions.strStyleNo =  '$style'";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intOrderNo']."/".$row['intOrderYear']."\">".$row['intOrderNo']."/".$row['intOrderYear']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	
	if($requestType=='loadItems')
	{
		$style  = $_REQUEST['style'];
		$orderNo  = $_REQUEST['orderNo'];
		$orderNoArray 	 = explode('/',$orderNo);
		
		
		 $sql = "SELECT DISTINCT
				ware_stocktransactions.intItemId,
				mst_item.strName
				FROM
				ware_stocktransactions
				Inner Join mst_item ON ware_stocktransactions.intItemId = mst_item.intId
				WHERE
				ware_stocktransactions.strStyleNo =  '$style' AND
				ware_stocktransactions.intOrderNo =  '$orderNoArray[0]' AND
				ware_stocktransactions.intOrderYear =  '$orderNoArray[1]'";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intItemId']."\">".$row['strName']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	else if($requestType=='loadMrnNo')
	{
		$orderNo  = $_REQUEST['orderNo'];
		$orderNoArray 	 = explode('/',$orderNo);
		$styleNo  = $_REQUEST['styleNo'];
		
		  $sql = "SELECT DISTINCT
				ware_mrndetails.intMrnNo, 
				ware_mrndetails.intMrnYear  
				FROM ware_mrndetails 
				Inner Join ware_mrnheader ON ware_mrndetails.intMrnNo = ware_mrnheader.intMrnNo AND ware_mrndetails.intMrnYear = ware_mrnheader.intMrnYear
				WHERE 
				ware_mrnheader.intStatus=1 AND 
				ware_mrnheader.intCompanyId =  '$location' AND 
				ware_mrndetails.intOrderNo =  '$orderNoArray[0]' AND
				ware_mrndetails.intOrderYear =  '$orderNoArray[1]' AND 
				ware_mrndetails.strStyleNo =  '$styleNo' AND 
				(ware_mrndetails.dblQty-ware_mrndetails.dblIssudQty)>0";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intMrnNo']."/".$row['intMrnYear']."\">".$row['intMrnNo']."/".$row['intMrnYear']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	else if($requestType=='loadItems')
	{
		$orderNo  = $_REQUEST['orderNo'];
		$orderNoArray 	 = explode('/',$orderNo);
		$styleNo  = $_REQUEST['styleNo'];
		$mrnNo  = $_REQUEST['mrnNo'];
		$mrnNoArray 	 = explode('/',$mrnNo);
		$mainCategory  = $_REQUEST['mainCategory'];
		$subCategory  = $_REQUEST['subCategory'];
		$description  = $_REQUEST['description'];
		

		if($styleNo=='null'){
			$styleNo='';
		}
		if($orderNoArray[0]==''){
			$orderNoArray[0]=0;
		}
		if($orderNoArray[1]==''){
			$orderNoArray[1]=0;
		}
		
		  $sql="select * from (SELECT
				mst_item.intId,
				mst_item.intMainCategory,
				mst_item.intSubCategory,
				mst_item.strCode,
				mst_item.strName as itemName,
				mst_item.intBomItem,
				mst_item.intUOM,
				mst_item.dblLastPrice,
				mst_maincategory.strName as mainCatName,
				mst_subcategory.strName as subCatName,   
				ware_mrndetails.intMrnNo,
				ware_mrndetails.intMrnYear,
				ware_mrndetails.intOrderNo,
				ware_mrndetails.intOrderYear,
				ware_mrndetails.strStyleNo,
				sum(ware_mrndetails.dblQty) as dblQty,
				sum(ware_mrndetails.dblIssudQty ) as dblIssudQty
				FROM
				ware_mrndetails 
				Inner Join ware_mrnheader ON ware_mrndetails.intMrnNo = ware_mrnheader.intMrnNo AND ware_mrndetails.intMrnYear = ware_mrnheader.intMrnYear
				Inner Join mst_item ON ware_mrndetails.intItemId = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
				WHERE 
				ware_mrnheader.intStatus = '1' AND 
				ware_mrndetails.intOrderNo =  '$orderNoArray[0]' AND
				ware_mrndetails.intOrderYear =  '$orderNoArray[1]' AND  
				ware_mrndetails.strStyleNo =  '$styleNo' AND 
				ware_mrndetails.intMrnNo =  '$mrnNoArray[0]' AND 
				ware_mrndetails.intMrnYear =  '$mrnNoArray[1]'";
				if($mainCategory!='')
				$sql.=" AND mst_item.intMainCategory =  '$mainCategory' ";
				if($subCategory!='')
				$sql.=" AND mst_item.intSubCategory =  '$subCategory'";
				if($description!=''){
				$sql.=" AND
				mst_item.strName LIKE  '%$description%'";
				}
				$sql.=" group by mst_item.intId) as tb1 where tb1.dblQty-tb1.dblIssudQty>0";	

		//echo $sql;
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$mrnQty = $row['dblQty'];
			if(!$mrnQty)
			$mrnQty=0;
			$issueQty = $row['dblIssudQty'];
			if(!$issueQty)
			$issueQty=0;
			
			if($row['intBomItem']==0){
			$stockBalQty=getStockBalance_bulk($location,$row['intId']);
			}
			else{
			$stockBalQty=getStockBalance($company,$location,$orderNoArray[0],$orderNoArray[1],$row['strStyleNo'],$row['intId']);
			}
			$data['intMrnNo'] 	= $row['intMrnNo'];
			$data['intMrnYear'] 	= $row['intMrnYear'];
			$data['intOrderNo'] 	= $row['intOrderNo'];
			$data['intOrderYear'] 	= $row['intOrderYear'];
			$data['strStyleNo'] 	= $row['strStyleNo'];
			$data['itemId'] 	= $row['intId'];
			$data['maincatId'] = $row['intMainCategory'];
			$data['subCatId'] 	= $row['intSubCategory'];
			$data['code'] = $row['strCode'];
			$data['itemName'] = $row['itemName'];
			$data['uom'] 	= $row['intUOM'];
			$data['unitPrice'] = $row['dblLastPrice'];
			$data['mainCatName'] = $row['mainCatName'];
			$data['subCatName'] = $row['subCatName'];
			$data['mrnQty'] = $mrnQty;
			$data['issueQty'] = $issueQty;
			$data['stockBalQty'] = $stockBalQty;
			
			$arrCombo[] = $data;
		}
		$response['arrCombo'] 	= $arrCombo;
		echo json_encode($response);

	}
	
	//-----------------------------------------------------------
	function getStockBalance($company,$location,$orderNo,$orderYear,$style,$item)
	{
		global $db;
		  $sql = "SELECT
				Sum(ware_stocktransactions.dblQty) as stockBal 
				FROM ware_stocktransactions
				WHERE
				ware_stocktransactions.intCompanyId =  '$company' AND
				ware_stocktransactions.intLocationId =  '$location' AND
				ware_stocktransactions.intOrderNo =  '$orderNo' AND
				ware_stocktransactions.intOrderYear =  '$orderYear' AND
				ware_stocktransactions.strStyleNo =  '$style' AND
				ware_stocktransactions.intItemId =  '$item'";

		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
	function getStockBalance_bulk($location,$item)
	{
		global $db;
		   $sql = "SELECT
				Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$location'
				GROUP BY
				ware_stocktransactions_bulk.intItemId";

		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
?>
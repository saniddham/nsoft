<?php
	session_start();
	$backwardseperator = "../../../../";
	$companyId 		= $_SESSION['headCompanyId'];
	$locationId 	= $_SESSION['CompanyID'];
	
	$intUser  		= $_SESSION["userId"];
	$mainPath 	  	= $_SESSION['mainPath'];
	$thisFilePath 	=  $_SERVER['PHP_SELF'];
	include_once  	"{$backwardseperator}dataAccess/Connector.php";
	require_once $_SESSION['ROOT_PATH']."class/warehouse/stores_requesition_note/cls_stores_requesition_note_get.php";
	
	$obj_st_req_get		= new cls_stores_requesition_note_get($db);
	
	$SRNNo 			= $_REQUEST['SRNNo'];
	$SRNYear 		= $_REQUEST['SRNYear'];
	$approveMode	= $_REQUEST['approveMode'];
	$programCode	= 'P0256';
	$userPermission = 0;
	
  $sql = "SELECT
		intRequisitionNo,
		intRequisitionYear,
		SSRT.strName AS srnTo,
		SRH.strGraphicNo,
		SRH.intOrderYear,
		SSRF.strName AS srnFrom,
		DATE(SRH.dtDate) AS dtDate,
		SRH.strNote,SRH.intStatus,
		SRH.intApproveLevels,
		SRH.int25ExceedingApproved,
		SRH.int100ExceedingApproved,
		SRH.int25ExceedingApproveLevels,
		SRH.int100ExceedingApproveLevels,
		SRH.intExcessApproved,
		SRH.intExcessApproveLevels,
		SRH.intUser,
		SRH.intLocationId as srnRaisedLocationId, 
		sys_users.strUserName  
 		FROM ware_storesrequesitionheader SRH
		INNER JOIN mst_substores SSRT ON SSRT.intId=SRH.intReqToStores
		INNER JOIN mst_substores SSRF ON SSRF.intId=SRH.intReqFromStores
		INNER JOIN sys_users ON SRH.intUser = sys_users.intUserId
 		WHERE SRH.intRequisitionNo = '$SRNNo' AND
		SRH.intRequisitionYear = '$SRNYear' /*AND
		SRH.intLocationId = '$locationId' */";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$locationId 			= $row['srnRaisedLocationId'];//this locationId use in report header(reportHeader.php)--------------------
					$SRNFrom 				= $row['srnFrom'];
					$SRNTo					= $row['srnTo'];
					$graphicNo					= $row['strGraphicNo'];
					$orderYear				= $row['intOrderYear'];
					$Date					= $row['dtDate'];
					$Note 					= $row['strNote'];
					$intStatus 				= $row['intStatus'];
					$approveLevel 			= $row['intApproveLevels'];
					$int25ExceedingApproved = $row['int25ExceedingApproved'];
					$int100ExceedingApproved = $row['int100ExceedingApproved'];
					$int25Exceeding_levels 	= $row['int25ExceedingApproveLevels'];
					$intExcess_levels 		= $row['intExcessApproveLevels'];
					$int100Exceeding_levels = $row['int100ExceedingApproveLevels'];
					$intExcessApproved		= $row['intExcessApproved'];
					
					$userName 					= $row['strUserName'];
					$createdUser 			= $row['intUser'];
					$remarks 				= $row['strRemarks'];
					$appLevels				=$savedLevels;
				 }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Stores Requisition Report</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/button.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="rpt_stores_requesition_note-js.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>

<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position: absolute;
	left: 287px;
	top: 184px;
	width: 650px;
	height: 322px;
	z-index: 1;
}
.APPROVE {
	font-size: 16px;
	font-weight: bold;
	font-family: "Arial Black", Gadget, sans-serif;
	color: #36F;
}
</style>
</head>

<body>
<?php
$rpApproveLevel = (int)getMainApproveLevel('Stores Requisition Note');
 
if($intStatus>1)//pending
{
?>
<div id="apDiv1"><img src="../../../../images/pending.png"  /></div>
<?php
}
?>
<form id="frmSRNReport" name="frmSRNReport" method="post" action="rpt_stores_requesition_note.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include '../../../../reportHeader.php'?></td>
<td width="20%"></td>
</tr>
<tr>
<td colspan="3"></td>
</tr>
</table>

<div align="center">
<div style="background-color:#FFF" ><strong>STORES REQUISITION REPORT</strong></div>
<table width="1100" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td colspan="2">
  <table width="100%">
  <tr>
    <td colspan="9" align="center" bgcolor="#FFFFFF">
    <?php
	if($intStatus>1 && $approveMode==1)
	{
		$k=$rpApproveLevel+2-$intStatus;
		$sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser' ";	
		
		$resultp = $db->RunQuery($sqlp);
		$rowp=mysqli_fetch_array($resultp);
		$userPermission=0;
		if($rowp['int'.$k.'Approval']==1)
		{	
		?>
    		<a id="imgApprove" class="button green medium" style="" name="imgApprove">Approve </a>
         <?php
		}
		
		$sqlp = "SELECT
				 menupermision.intReject
				 FROM menupermision 
				 INNER JOIN menus ON menupermision.intMenuId = menus.intId
				 WHERE
				 menus.strCode = '$programCode' AND
				 menupermision.intUserId = '$intUser' ";	
		
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 $userPermission=0;
		 if($rowp['intReject']==1)
		 {
			?>
    		<a id="imgReject" class="button green medium" style="" name="imgReject">Reject </a>
            <?php
		 }
	}
	?>
    </td>
  </tr>
  <tr>
  <?php
 	if($intStatus==1)
	{
	?>
   <td colspan="9" align="center" class="APPROVE" style="color:#6C6">CONFIRMED</td>
   <?PHP
	}
	else if($intStatus==0)
	{
   ?>
   <td colspan="9" align="center" class="APPROVE" style="color:#F00">REJECTED</td>
   <?php
	}
	else
	{
   ?>
   <td width="15%" colspan="9" align="center" class="APPROVE">PENDING</td>
   <?php
	}
   ?>
  </tr>
  <tr>
    <td width="1%">&nbsp;</td>
    <td width="10%"><span class="normalfnt">SRN No</span></td>
    <td width="1%" align="center" valign="middle">:</td>
    <td width="20%"><span class="normalfnt"><?php echo $SRNNo.'/'.$SRNYear; ?></span></td>
    <td width="3%">&nbsp;</td>
    <td width="3%" align="center" valign="middle">&nbsp;</td>
    <td width="8%"><span class="normalfnt">Date</span></td>
    <td width="1%" align="center" valign="middle">:</td>
    <td width="20%"><span class="normalfnt"><?php echo $Date; ?></span></td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">SRN From</td>
    <td align="center" valign="middle">:</td>
    <td colspan="2"><span class="normalfnt"><?php echo $SRNFrom; ?></span></td>
    <td align="center" valign="middle">&nbsp;</td>
    <td><span class="normalfnt">Order Year</span></td>
    <td align="center" valign="top">:</td>
    <td rowspan="1" class="normalfnt"><?php echo $orderYear; ?></td>
    </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">SRN To</td>
    <td align="center" valign="middle">:</td>
    <td colspan="2"><span class="normalfnt"><?php echo $SRNTo; ?></span></td>
    <td align="center" valign="middle">&nbsp;</td>
    <td class="normalfnt">Graphic No</td>
    <td align="center" valign="top">:</td>
    <td align="center" class="normalfnt"><?php echo $graphicNo; ?></td>
    </tr>
  </table>
  </td>
</tr>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<tr>
  <td colspan="2">
    <table width="100%">
       <tr>
        <td width="1%">&nbsp;</td>
        <td colspan="7" class="normalfnt">
            <table width="100%" class="bordered tblMainGrid" id="tbl1" >
            	<tr class="">
                 <th width="13%" nowrap="nowrap" >Main Category</th>
                 <th width="16%" nowrap="nowrap" >Sub Category</th>
                 <th width="34%" nowrap="nowrap">Item Description </th>
                 <th width="3%" >Qty</th>
                 <th width="14%" >25% Exceed Qty</th>
                 <th width="13%" >100% Exceed Qty</th>
                 <th width="7%" >Extra Qty</th>
                </tr>
                <?php
				$result1 = getGridDetails($SRNNo,$SRNYear,$locationId,$SRNFrom);
				while($row1=mysqli_fetch_array($result1))
				{
					$ordersArray 	= explode(",", $orders);
					$resp			=	$obj_st_req_get->get_max_qty($ordersArray,$orderYear,$row1['intItemId'],$companyId,'RunQuery');
					$maxQty			=$resp['maxQty'];
				?>
                <tr class="normalfnt">
                 <td align="left" class="normalfnt"><?php echo $row1['mainCatName']; ?></td>
                 <td align="left" class="normalfnt"><?php echo $row1['subCatName']; ?></td>
                 <td align="left" class="normalfnt" ><?php echo $row1['itemName']; ?></td>
                 <td style="text-align:center" class="normalfnt" ><?php echo $row1['dblQty']; ?></td>
                 <td style="text-align:center" class="normalfnt" ><span class="normalfnt" style="text-align:center"><?php echo $row1['dblExceeded_25']; ?></span></td>
                 <td style="text-align:center" class="normalfnt" ><span class="normalfnt" style="text-align:center"><?php echo $row1['dblExceeded_100']; ?></span></td>
                 <td style="text-align:center" class="normalfnt" ><?php echo $row1['dblExQty']; ?></td>
                </tr>
                <?php
				}
				?>
            </table>
        </td>
        <td width="3%">&nbsp;</td>
        </tr>
      
      </table>
    </td>
</tr>
<tr>
  <td width="10" bgcolor="#FFFFFF">&nbsp;</td>
  <td width="884" bgcolor="#FFFFFF">&nbsp;</td>
</tr>
<?php  
 	if($intStatus!=0)
	{

				for($i=1; $i<=$rpApproveLevel; $i++)
				{
					 $sqlc = "SELECT
								ware_storesrequesitionheader_approvedby.intApproveUser,
								ware_storesrequesitionheader_approvedby.dtApprovedDate,
								sys_users.strUserName,
								ware_storesrequesitionheader_approvedby.intApproveLevelNo
								FROM
								ware_storesrequesitionheader_approvedby
								INNER JOIN sys_users ON ware_storesrequesitionheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_storesrequesitionheader_approvedby.intRequisitionNo = '$SRNNo' AND
								ware_storesrequesitionheader_approvedby.intYear = '$SRNYear' AND
								ware_storesrequesitionheader_approvedby.intApproveLevelNo = '$i'
								";
					 $resultc = $db->RunQuery($sqlc);
					 $rowc=mysqli_fetch_array($resultc);
						if($i==1)
						$desc="1st ";
						else if($i==2)
						$desc="2nd ";
						else if($i==3)
						$desc="3rd ";
						else
						$desc=$i."th ";
					 //  $desc=$ap.$desc;
					 $desc2=$rowc['strUserName']."(".$rowc['dtApprovedDate'].")";
					 if($rowc['strUserName']=='')
					 $desc2='---------------------------------';
				?>

            <tr>
                <td bgcolor="#FFFFFF">&nbsp;</td>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><?php echo $desc; ?>Approved By - <?php echo $desc2;?></span></td>
            </tr>
<?php
			}
	}
?>

<tr height="90" >
  <td colspan="2" align="center" class="normalfntMid"></td>
</tr>
<tr height="40">
<?php
	//$locationId = '';
	$sql = "SELECT mst_locations.intCompanyId FROM mst_locations WHERE mst_locations.intId =  '$locationId'";
	$result 	= $db->RunQuery($sql);
	$row		= mysqli_fetch_array($result);
	$createCompanyId = $row['intCompanyId'];
?>
<?Php 
	$url  = "{$backwardseperator}presentation/sendToApproval.php";		// * file name
	$url .= "?status=$intStatus";										// * set recent status
	$url .= "&approveLevels=$savedLevels";								// * set approve levels in order header
	$url .= "&programCode=$programCode";								// * program code (ex:P10001)
	$url .= "&program=SRN";									// * program name (ex:Purchase Order)
	$url .= "&companyId=$createCompanyId";									// * created company id
	$url .= "&createUserId=$createdUser";	
	
	$url .= "&field1=SRN No";												 
	$url .= "&field2=SRN Year";	
	$url .= "&value1=$SRNNo";												 
	$url .= "&value2=$SRNYear";	
	
	$url .= "&subject=SRN FOR APPROVAL ('$SRNNo'/'$SRNYear')";	
	
	$url .= "&statement1=Please Approve this";	
	$url .= "&statement2=to Approve this";	
							// * doc year
	$url .= "&link=".urlencode(base64_encode($mainPath."presentation/warehouse/stores_requesition_note/listing/rpt_stores_requesition_note_exceeding.php?SRNNo=$SRNNo&year=$SRNYear&approveMode=1"));
?>
  <td colspan="2" align="center" class="normalfntMid"><iframe id="iframeFiles2" src="<?php echo $url;?>" name="iframeFiles" style="width:500px;height:150px;border:none"  ></iframe></td>
</tr>
<tr height="40">
  <td colspan="2" align="center" class="normalfntMid">Printed Date: <?php echo date("Y/m/d") ?></td>
</tr>
</table>
</div>        
</form>
</body>
</html>
<?php
function getMainApproveLevel($program)
{
	global $db;
	$sql = "SELECT intApprovalLevel FROM sys_approvelevels WHERE strName='$program' ";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	return $row['intApprovalLevel'];
}
function getGridDetails($SRNNo,$SRNYear,$locationId,$SRNFrom)
{
	global $db;
	$sql = "SELECT SRD.intRequisitionNo,SRD.intRequisitionYear,SRD.intItemId,MI.strName AS itemName,
			MC.strName AS mainCatName,MC.intId AS mainCatId,SC.strName AS subCatName,SC.intId AS subCatId,SRD.dblQty,dblExceeded_25,dblExceeded_100, SRD.dblExQty 
			FROM ware_storesrequesitiondetails SRD
			INNER JOIN mst_item MI ON MI.intId=SRD.intItemId
			INNER JOIN mst_maincategory MC ON MC.intId=MI.intMainCategory
			INNER JOIN mst_subcategory SC ON SC.intId=MI.intSubCategory
			WHERE SRD.intRequisitionNo='$SRNNo' AND
			SRD.intRequisitionYear='$SRNYear'";
	$result = $db->RunQuery($sql);
	return $result;
}
?>
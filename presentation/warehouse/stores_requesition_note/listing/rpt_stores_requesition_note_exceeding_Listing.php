<?php
date_default_timezone_set('Asia/Colombo');
//ini_set('allow_url_include',1);
//ini_set('display_errors',1);
session_start();

$backwardseperator 	= "../../../../";
$thisFilePath 		=  $_SERVER['PHP_SELF'];

require_once $_SESSION['ROOT_PATH']."dataAccess/Connector.php";
include_once "../../../../libraries/jqgrid2/inc/jqgrid_dist.php";
require_once $_SESSION['ROOT_PATH']."class/cls_permisions.php";
	
$objpermisionget= new cls_permisions($db);

$company 	= $_SESSION['headCompanyId'];
$location = $_SESSION['CompanyID'];

$programName	= 'Stores Requisition Note';
$programCode	= 'P0798';
 
$intUser  = $_SESSION["userId"];

$userDepartment=getUserDepartment($intUser);
$approveLevel_25 = (int)get_25_MaxApproveLevel();
$approveLevel_100 = (int)get_100_MaxApproveLevel();
$approveLevel_excess = (int)get_excess_MaxApproveLevel();

$option=1;
$option=getOption($objpermisionget);

$strOption3=getOption3ComboValues();

 $sql = "select * from(SELECT DISTINCT 
							if(tb1.int25ExceedingApproved=1,'Approved',if(tb1.int25ExceedingApproved=0,'Rejected',if(tb1.int25ExceedingApproved IS NULL,'Not Exceed','Pending'))) as 25Status,
							if(tb1.int100ExceedingApproved=1,'Approved',if(tb1.int100ExceedingApproved=0,'Rejected',if(tb1.int100ExceedingApproved IS NULL,'Not Exceed','Pending'))) as 100Status,
							if(tb1.intExcessApproved=1,'Approved',if(tb1.intExcessApproved=0,'Rejected',if(tb1.intExcessApproved IS NULL,'Not Exceed','Pending'))) as excessStatus,
						/*	if(((tb1.intStatus <>1) && (tb1.int25ExceedingApproved<=int25ExceedingApproveLevels+1)  && (tb1.int25ExceedingApproved>0) 
							&& ((SELECT
							Sum(trn_sample_size_wise_item_consumptions.CONSUMPTION)
							FROM
							trn_sample_size_wise_item_consumptions
							INNER JOIN ware_storesrequesitiondetails ON 
							trn_sample_size_wise_item_consumptions.ITEM_ID = ware_storesrequesitiondetails.intItemId
							WHERE 
							trn_sample_size_wise_item_consumptions.ORDER_NO IN (tb1.strOrderNos) 
							AND trn_sample_size_wise_item_consumptions.ORDER_YEAR = tb1.intOrderYear AND 
							ware_storesrequesitiondetails.intRequisitionNo = tb1.intRequisitionNo AND
							ware_storesrequesitiondetails.intRequisitionYear = tb1.intRequisitionYear) > 0 )),'Re Save','') as RE_SAVE, */
							tb1.intRequisitionNo as `SRN_No`,
							tb1.intRequisitionYear as `SRN_Year`,
							SSRT.strName AS srnTo,
							SSRF.strName AS srnFrom,
							strGraphicNo as `Graphic`,
							date(tb1.dtDate) as `Date`,
							sys_users.strUserName as `Create_User`, 

							IFNULL((SELECT
							Sum(ware_storesrequesitiondetails.dblQty)
							FROM
							ware_storesrequesitionheader
							Inner Join ware_storesrequesitiondetails ON ware_storesrequesitionheader.intRequisitionNo = ware_storesrequesitiondetails.intRequisitionNo AND ware_storesrequesitionheader.intRequisitionYear = ware_storesrequesitiondetails.intRequisitionYear
							WHERE
							ware_storesrequesitionheader.intRequisitionNo =  tb1.intRequisitionNo AND
							ware_storesrequesitionheader.intRequisitionYear =  tb1.intRequisitionYear),0) as `SRN_Qty`,
							  
							sys_users.strUserName as `User`, 
							  
							IFNULL((
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(ware_storesrequesitionheader_so_wise_approvedby.dtApprovedDate),')' )
								FROM
								ware_storesrequesitionheader_so_wise_approvedby
								Inner Join sys_users ON ware_storesrequesitionheader_so_wise_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_storesrequesitionheader_so_wise_approvedby.intRequisitionNo  = tb1.intRequisitionNo AND
								ware_storesrequesitionheader_so_wise_approvedby.intYear =  tb1.intRequisitionYear AND
								ware_storesrequesitionheader_so_wise_approvedby.intApproveLevelNo >  '1' AND 
							    ware_storesrequesitionheader_so_wise_approvedby.intStatus='0'  AND int25ExceedingApproveLevels IS NOT NULL 
							),IF(((
							((SELECT
								menupermision.int2Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1) ||
							( (SELECT
								menupermision.int3Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1)	
								 ) AND tb1.int25ExceedingApproved>1),'Approve', '')) as `25_Approval`,  ";
  							
 					
					
						$sql .= "IFNULL((
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(ware_storesrequesitionheader_so_wise_approvedby.dtApprovedDate),')' )
								FROM
								ware_storesrequesitionheader_so_wise_approvedby
								Inner Join sys_users ON ware_storesrequesitionheader_so_wise_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_storesrequesitionheader_so_wise_approvedby.intRequisitionNo  = tb1.intRequisitionNo AND
								ware_storesrequesitionheader_so_wise_approvedby.intYear =  tb1.intRequisitionYear AND
								ware_storesrequesitionheader_so_wise_approvedby.intApproveLevelNo =  '0' AND 
							    ware_storesrequesitionheader_so_wise_approvedby.intStatus='0' AND 
								tb1.int25ExceedingApproved<>1
							),'') as `25_RejectedBy` ,  
	
	
							IFNULL((
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(ware_storesrequesitionheader_so_wise_approvedby.dtApprovedDate),')' )
								FROM
								ware_storesrequesitionheader_so_wise_approvedby
								Inner Join sys_users ON ware_storesrequesitionheader_so_wise_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_storesrequesitionheader_so_wise_approvedby.intRequisitionNo  = tb1.intRequisitionNo AND
								ware_storesrequesitionheader_so_wise_approvedby.intYear =  tb1.intRequisitionYear AND
								ware_storesrequesitionheader_so_wise_approvedby.intApproveLevelNo =  '3' AND 
							    ware_storesrequesitionheader_so_wise_approvedby.intStatus='0'  AND int100ExceedingApproveLevels IS NOT NULL  
							),IF(((SELECT
								menupermision.int3Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.int100ExceedingApproved>1),'Approve', '')) as `100_Approval`,  ";
  							
 					
					
						$sql .= "IFNULL((
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(ware_storesrequesitionheader_so_wise_approvedby.dtApprovedDate),')' )
								FROM
								ware_storesrequesitionheader_so_wise_approvedby
								Inner Join sys_users ON ware_storesrequesitionheader_so_wise_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_storesrequesitionheader_so_wise_approvedby.intRequisitionNo  = tb1.intRequisitionNo AND
								ware_storesrequesitionheader_so_wise_approvedby.intYear =  tb1.intRequisitionYear AND
								ware_storesrequesitionheader_so_wise_approvedby.intApproveLevelNo =  '0' AND 
							    ware_storesrequesitionheader_so_wise_approvedby.intStatus='0' AND 
								tb1.int100ExceedingApproved<>1 
							),'') as `100_RejectedBy` ,   
	
	
							IFNULL((
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(ware_storesrequesitionheader_so_wise_approvedby.dtApprovedDate),')' )
								FROM
								ware_storesrequesitionheader_so_wise_approvedby
								Inner Join sys_users ON ware_storesrequesitionheader_so_wise_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_storesrequesitionheader_so_wise_approvedby.intRequisitionNo  = tb1.intRequisitionNo AND
								ware_storesrequesitionheader_so_wise_approvedby.intYear =  tb1.intRequisitionYear AND
								ware_storesrequesitionheader_so_wise_approvedby.intApproveLevelNo >=  '1' AND 
							    ware_storesrequesitionheader_so_wise_approvedby.intStatus='0'  AND int100ExceedingApproveLevels IS NOT NULL  
							),IF(((
							((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1) 
								|| ((SELECT
								menupermision.int2Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1)
								||
								((SELECT
								menupermision.int3Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1)) AND  tb1.intExcessApproved>1),'Approve', '')) as `Excess_Approval`,  ";
  							
 					
					
						$sql .= "IFNULL((
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(ware_storesrequesitionheader_so_wise_approvedby.dtApprovedDate),')' )
								FROM
								ware_storesrequesitionheader_so_wise_approvedby
								Inner Join sys_users ON ware_storesrequesitionheader_so_wise_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_storesrequesitionheader_so_wise_approvedby.intRequisitionNo  = tb1.intRequisitionNo AND
								ware_storesrequesitionheader_so_wise_approvedby.intYear =  tb1.intRequisitionYear AND
								ware_storesrequesitionheader_so_wise_approvedby.intApproveLevelNo =  '0' AND 
							    ware_storesrequesitionheader_so_wise_approvedby.intStatus='0' AND 
								tb1.intExcessApproved <> 1
							),'') as `Excess_RejectedBy` , "; 
	
	
	
							
								 
							$sql .= "  
							'View' as `View`   
							
							FROM
							
							ware_storesrequesitionheader as tb1
							INNER JOIN mst_substores SSRT ON SSRT.intId=tb1.intReqToStores
							INNER JOIN mst_substores SSRF ON SSRF.intId=tb1.intReqFromStores
							Inner Join sys_users ON tb1.intUser = sys_users.intUserId 
							Inner Join mst_locations ON tb1.intLocationId = mst_locations.intId 
							WHERE  
							(tb1.int25ExceedingApproved IS NOT NULL OR tb1.int100ExceedingApproved IS NOT NULL OR tb1.intExcessApproved IS NOT NULL )AND  
							tb1.intLocationId = '$location'  ";
 							if($option==1)//only loging user's mrns
							$sql .= " AND tb1.intUser = '$intUser'" ; 
							
							$sql .= "  )  as t where 1=1
						";
					   // 	echo $sql;
$col = array();

 
//STATUS
$col["title"] 	= "Over 25% Approve Status"; // caption of column
$col["name"] 	= "25Status"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "6";
//edittype
$col["stype"] 	= "select";
$str = ":All;Pending:Pending;Approved:Approved;Rejected:Rejected;Not Exceed:Not Exceed" ;
$col["editoptions"] 	=  array("value"=> $str);
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist

$cols[] = $col;	$col=NULL;

//STATUS
$col["title"] 	= "Over 100% Approve Status"; // caption of column
$col["name"] 	= "100Status"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "7";
//edittype
$col["stype"] 	= "select";
$str = ":All;Pending:Pending;Approved:Approved;Rejected:Rejected;Not Exceed:Not Exceed" ;
$col["editoptions"] 	=  array("value"=> $str);
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist

$cols[] = $col;	$col=NULL;


//STATUS
$col["title"] 	= "Excess Approve Status"; // caption of column
$col["name"] 	= "excessStatus"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "7";
//edittype
$col["stype"] 	= "select";
$str = ":All;Pending:Pending;Approved:Approved;Rejected:Rejected;No Excess:No Excess" ;
$col["editoptions"] 	=  array("value"=> $str);
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist

$cols[] = $col;	$col=NULL;

//SRN No
$col["title"] 	= "SRN No"; // caption of column
$col["name"] 	= "SRN_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "2";
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$col['link']	= "../addNew/stores_requesition_note.php?SRNNo={SRN_No}&SRNYear={SRN_Year}";	 
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag

$reportLink  = "rpt_stores_requesition_note_for_approval.php?SRNNo={SRN_No}&SRNYear={SRN_Year}";
$reportLinkApprove  = "rpt_stores_requesition_note_for_approval.php?SRNNo={SRN_No}&SRNYear={SRN_Year}&approveMode_exceeding=1";

$cols[] = $col;	$col=NULL;


//SRN Year
$col["title"] = "SRN Year"; // caption of column
$col["name"] = "SRN_Year"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Department
$col["title"] = "Department"; // caption of column
$col["name"] = "Department"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
if($option==3)//all in loging user  department's mrns
{
$col["stype"] 	= "select";
$col["editoptions"] 	=  array("value"=> $strOption3);
}
$col["width"] = "4";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//DATE
$col["title"] = "Date"; // caption of column
$col["name"] = "Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Create User
$col["title"] = "Create User"; // caption of column
$col["name"] = "Create_User"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

 
 
$ap1="Over 25% - "."Approval";
 //SECOND APPROVAL
$col["title"] = $ap1; // caption of column
$col["name"] = '25_Approval'; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;
 

 
$ap1="Over 100% - "."Approval";
 //SECOND APPROVAL
$col["title"] = $ap1; // caption of column
$col["name"] = '100_Approval'; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;


 
$ap1="Excess - "."Approval";
 //SECOND APPROVAL
$col["title"] = $ap1; // caption of column
$col["name"] = 'Excess_Approval'; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;
 
 
$col["title"] = "Over 25% Rejected By"; // caption of column
$col["name"] = "25_RejectedBy"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "5";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

$col["title"] = "Over 100% Rejected By"; // caption of column
$col["name"] = "100_RejectedBy"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "6";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

$col["title"] = "Excess Rejected By"; // caption of column
$col["name"] = "Excess_RejectedBy"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "6";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

//VIEW
$col["title"] = "Report"; // caption of column
$col["name"] = "View"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

$jq = new jqgrid();

$grid["caption"] 		= "SRN (Exceeding Qty) Listing";
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'SRN_No'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes

$grid["export"]["range"] = "filtered"; // or "all"
// initialize search, 'name' field equal to (eq) 'Client 1'
/*$sarr = <<< SEARCH_JSON
{ 
	"groupOp":"AND",
    "rules":[
      {"field":"Status","op":"eq","data":"Pending"}
     ]
}
SEARCH_JSON;

$grid["search"] = true; 
$grid["postData"] = array("filters" => $sarr ); */

// export XLS file
// export to excel parameters - range could be "all" or "filtered"
//$grid["export"] = array("format"=>"xlsx", "filename"=>"my-file", "sheetname"=>"test");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

// params are array(<function-name>,<class-object> or <null-if-global-func>,<continue-default-operation>)
$e["on_render_excel"] = array("custom_excel_layout", null, true);
$jq->set_events($e);
				
function custom_excel_layout($param)
{
	$grid = $param["grid"];
	$xls = $param["xls"];
	$arr = $param["data"];
	
	$xls->addHeader(array('SRN (EXCEEDING QTY) LISTING'));
	$xls->addHeader(array());
	$xls->addHeader($arr[0]);
	$summary_arr = array(0);
	
	for($i=1;$i<count($arr);$i++)
	{
		$xls->addRow($arr[$i]);
		$summary_arr[0] += $arr[$i]["id"];
	}
	//$xls->addRow($summary_arr);
	return $xls;
}


$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);



$out = $jq->render("list1");
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SRN (Exceeding Qty) Listing<?php /*?>(<?php echo $option; ?>)<?php */?></title>

<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />


</head>

<body>	
<form id="frmlisting" name="frmlisting" method="post" action="">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">
	<tr>
		<td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
	</tr> 
</table>
	<link rel="stylesheet" type="text/css" media="screen" href="../../../../libraries/jqdrid/js/themes/smoothness/jquery-ui.custom.css"></link>	
	<link rel="stylesheet" type="text/css" media="screen" href="../../../../libraries/jqdrid/js/jqgrid/css/ui.jqgrid.css"></link>	
	
	<script src="../../../../libraries/jqdrid/js/jquery.min.js" type="text/javascript"></script>
	<script src="../../../../libraries/jqdrid/js/jqgrid/js/i18n/grid.locale-en.js" type="text/javascript"></script>
	<script src="../../../../libraries/jqdrid/js/jqgrid/js/jquery.jqGrid.min.js" type="text/javascript"></script>	
	<script src="../../../../libraries/jqdrid/js/themes/jquery-ui.custom.min.js" type="text/javascript"></script>
	<script src="../../../../libraries/javascript/script.js" type="text/javascript"></script>
 	<script type="application/javascript" src="rptMrn-js.js"></script>
	
    <td><table width="100%" border="0">
      <tr>
        <td>
        <div align="center" style="margin:10px">
        
			<?php echo $out?>
        </div>
        </td>
      </tr>

    </table></td>
    </tr>


</form>
</body>
</html>
<script type="text/javascript">
 		$('.cls_re_save').parent().css('background-color', 'red');
		//alert($('.cls_re_save').parent().html());

</script> 
<?php

//-----------------------------------------------------------------
function get_25_MaxApproveLevel(){
	global $db;
	
	//echo $savedStat;
	$appLevel=0;
	  $sqlp = "SELECT
			Max(ware_storesrequesitionheader.int25ExceedingApproveLevels) AS appLevel
			FROM ware_storesrequesitionheader";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['appLevel'];
}
//----------------------------------------------------------------------
function get_100_MaxApproveLevel(){
	global $db;
	
	//echo $savedStat;
	$appLevel=0;
	  $sqlp = "SELECT
			Max(ware_storesrequesitionheader.int100ExceedingApproveLevels) AS appLevel
			FROM ware_storesrequesitionheader";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['appLevel'];
}

//------------------------------function load User Department---------------------
function getUserDepartment($intUser){
	global $db;

	$sqlp = "SELECT
			sys_users.intDepartmentId
			FROM
			sys_users
			WHERE
			sys_users.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['intDepartmentId'];

}

//----------------------------------------------------------------------------
function getOption3ComboValues(){
	global $db;

	$sql = "SELECT
			mst_department.intId,
			mst_department.strName
			FROM
			mst_department
			WHERE
			mst_department.intStatus =  '1'";
	$result = $db->RunQuery($sql);
	$strOption3 = ":All";
	while($row=mysqli_fetch_array($result))
	{
		$strOption3 .= ";".$row['strName'].":".$row['strName'] ;
	}
			 
	return $strOption3;

}
//------------------------------function load User Department---------------------
function getOption($objpermisionget){
	
	$option2 	= $objpermisionget->boolSPermision(3);
	$option3 	= $objpermisionget->boolSPermision(4);
	
	if($option3)
	$option=3;
	else if($option2)
	$option=2;
	else
	$option=1;
		
	return $option;


}
function get_excess_MaxApproveLevel(){

	global $db;
	
	//echo $savedStat;
	$appLevel=0;
	  $sqlp = "SELECT
			Max(ware_storesrequesitionheader.intExcessApproveLevels) AS appLevel
			FROM ware_storesrequesitionheader";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['appLevel'];
	
}
?>


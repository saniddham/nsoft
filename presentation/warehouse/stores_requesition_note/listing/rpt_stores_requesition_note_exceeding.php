<?php
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
ini_set('display_errors',0);
session_start();
$backwardseperator 	= "../../../../";

$companyId 		= $_SESSION['headCompanyId'];
$locationId 	= $_SESSION['CompanyID'];
$intUser  		= $_SESSION["userId"];
$mainPath 		= $_SESSION['mainPath'];
$thisFilePath 	=  $_SERVER['PHP_SELF'];
include_once "{$backwardseperator}dataAccess/Connector.php";
require_once $_SESSION['ROOT_PATH']."class/warehouse/cls_warehouse_get.php";
require_once $_SESSION['ROOT_PATH']."class/cls_permisions.php";
require_once $_SESSION['ROOT_PATH']."class/warehouse/stores_requesition_note/cls_stores_requesition_note_get.php";


$obj_wh_get			= new cls_warehouse_get($db);
$obj_permision		= new cls_permisions($db);
$obj_st_req_get		= new cls_stores_requesition_note_get($db);

$SRNNo 				= $_REQUEST['SRNNo'];
$SRNYear 			= $_REQUEST['SRNYear'];
$approveMode		= $_REQUEST['approveMode'];
$userPermission 	= 0;

$approveMode 		= $_REQUEST['approveMode'];
$approveMode_exceeding 	= $_REQUEST['approveMode_exceeding'];

$programName		= 'Stores Requisition Note';
$programCode		= 'P0256';
$srnApproveLevel 	= (int)getApproveLevel($programName);


/*$mrnNo = '100000';
$year = '2012';
$approveMode=1;
*/
  $sql = "SELECT
		intRequisitionNo,
		intRequisitionYear,
		SSRT.strName AS srnTo,
		SRH.strOrderNos,
		SRH.intOrderYear,
		SSRF.strName AS srnFrom,
		DATE(SRH.dtDate) AS dtDate,
		SRH.strNote,SRH.intStatus,
		SRH.intApproveLevels,
		SRH.int25ExceedingApproved,
		SRH.int100ExceedingApproved,
		SRH.int25ExceedingApproveLevels,
		SRH.int100ExceedingApproveLevels,
		SRH.intExcessApproved,
		SRH.intExcessApproveLevels,
		SRH.intUser,
		SRH.intLocationId as srnRaisedLocationId, 
		sys_users.strUserName  
 		FROM ware_storesrequesitionheader SRH
		INNER JOIN mst_substores SSRT ON SSRT.intId=SRH.intReqToStores
		INNER JOIN mst_substores SSRF ON SSRF.intId=SRH.intReqFromStores
		INNER JOIN sys_users ON SRH.intUser = sys_users.intUserId
 		WHERE SRH.intRequisitionNo = '$SRNNo' AND
		SRH.intRequisitionYear = '$SRNYear' AND
		SRH.intLocationId = '$locationId' ";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$locationId 			= $row['srnRaisedLocationId'];//this locationId use in report header(reportHeader.php)--------------------
					$SRNFrom 				= $row['srnFrom'];
					$SRNTo					= $row['srnTo'];
					$orders					= $row['strOrderNos'];
					$orderYear				= $row['intOrderYear'];
					$Date					= $row['dtDate'];
					$Note 					= $row['strNote'];
					$intStatus 				= $row['intStatus'];
					$approveLevel 			= $row['intApproveLevels'];
					$int25ExceedingApproved = $row['int25ExceedingApproved'];
					$int100ExceedingApproved = $row['int100ExceedingApproved'];
					$int25Exceeding_levels 	= $row['int25ExceedingApproveLevels'];
					$intExcess_levels 		= $row['intExcessApproveLevels'];
					$int100Exceeding_levels = $row['int100ExceedingApproveLevels'];
					$intExcessApproved		= $row['intExcessApproved'];
					
					$userName 				= $row['strUserName'];
					$createdUser 			= $row['intUser'];
					$remarks 				= $row['strRemarks'];
					$savedLevels 			= $row['intApproveLevels'];
					$appLevels				=$savedLevels;
				 }
	
	$int25Exceeding_permision 	= 0;
	$int100Exceeding_permision 	= 0;
	$intExcess_permision 		= 0;
	
	$row	= $obj_permision->get_special_permision('56','RunQuery');
 	$user = $row['intUser'];
	if($intUser==$user){
		$int25Exceeding_permision = 1;
	}
	$row	= $obj_permision->get_special_permision('57','RunQuery');
	$user = $row['intUser'];
	if($intUser==$user){
		$int100Exceeding_permision = 1;
	}
	$row	= $obj_permision->get_special_permision('58','RunQuery');
	$user = $row['intUser'];
	if($intUser==$user){
		$intExcess_permision = 1;
	}
				 
$confirmationMode=loadConfirmatonMode($programCode,$intStatus,$savedLevels,$intUser);
$rejectionMode=loadRejectionMode($programCode,$intStatus,$savedLevels,$intUser);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>SRN Report (Exceed Qtys)</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/promt.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/button.css" rel="stylesheet" type="text/css" />

<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">

<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="rpt_stores_requesition_note-js.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>

<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script>
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script>
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>
<style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position: absolute;
	left: 256px;
	top: 183px;
	width: 650px;
	height: 391px;
	z-index: 1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<?php
 if($intStatus>1)//pending
{
?>
<div id="apDiv1"><img src="../../../../images/pending.png"  /></div>
<?php
}
?>
<form id="frmSRNReport" name="frmSRNReport" method="post" action="rpt_stores_requesition_note_exceeding.php">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include '../../../../reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>STORES REQUISITION NOTE</strong><strong></strong></div>
<table width="1100" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
  <?php
  if(($approveMode_exceeding ==1) && ($int25Exceeding_permision==1) && ($int25ExceedingApproved >1)){
  ?>
  <tr>
    <td colspan="9" align="center" bgcolor="#FFDFCB">
            <a  class="button green medium"  id="imgAppQty25" name="imgAppQty25" >Approve 25% Exceed Qtys</a>
            <a  class="button green medium"   id="imgRejQty25" name="imgRejQty25" >Reject 25% Exceed Qtys</a>
    </td>
  </tr>
  <?php
  }
  else if(($approveMode_exceeding ==1) && ($int100Exceeding_permision==1) && ($int100ExceedingApproved >1)){
  ?>
  <tr>
    <td colspan="9" align="center" bgcolor="#FFDFCB">
            <a  class="button green medium"  id="imgAppQty100" name="imgAppQty100" >Approve 100% Exceed Qtys</a>
            <a  class="button green medium"   id="imgRejQty100" name="imgRejQty100" >Reject 100% Exceed Qtys</a>
    </td>
  </tr>
  <?php
  }
  else if(($approveMode_exceeding ==1) && ($intExcess_permision==1) && ($intExcessApproved >1)){
  ?>
  <tr>
    <td colspan="9" align="center" bgcolor="#FFDFCB">
            <a  class="button green medium"  id="imgAppQtyExcess" name="imgAppQtyExcess" >Approve Excess Qtys</a>
            <a  class="button green medium"   id="imgRejQtyExcess" name="imgRejQtyExcess" >Reject Excess Qtys</a>
    </td>
  </tr>
  <?php
  }
  else{
  ?>
  
  <tr>
    <td colspan="9" align="center" bgcolor="#FFDFCB">
    <?php
	if($intStatus>1)
	{
	?>
    <?php if($approveMode==1){
			if($confirmationMode==1) { 
			?>
            <a  class="button green medium"  id="imgApprove" name="imgApprove" >Approve</a>
			<?php
			}
			if($rejectionMode==1) { 
			?>
            <a  class="button green medium"  id="imgReject" name="imgReject" >Reject</a>
                <?php
                }
            }
	}
	?>
    </td>
  </tr>
  <?php
  }
  ?>
 
 
 <tr>
  <?php
 	if((($int25ExceedingApproved==1) || ($int100ExceedingApproved==1)))
	{
	?>
   <td colspan="9" class="APPROVE" >CONFIRMED QUANTITIES</td>
   <?PHP
	}
 	else if((($int25ExceedingApproved>1) && ($int25ExceedingApproved <= $int25Exceeding_levels+1)) || (($int100ExceedingApproved>1) && ($int25ExceedingApproved <= $int100Exceeding_levels+1)))
	{
   ?>
   <td colspan="9" class="APPROVE" >PENDING QUANTITIES</td>
   <?PHP
	}
 	else if((($int25ExceedingApproved=='0') || ($int100ExceedingApproved=='0')  || ($intExcessApproved=='0')))
	{
   ?>
   <td colspan="9" class="APPROVE" style="color:#F00">REJECTED QUANTITIES</td>
   <?php
	}
	else
	{
 	?>
    <td colspan="9"></td>
    <?php
	}
		//	echo $int25ExceedingApproved.'-'.$int100ExceedingApproved.'-'.$intExcessApproved;

	?>
 </tr>
 <tr>
  <?php
 	if((($intExcessApproved==1)))
	{
	?>
   <td colspan="9" class="APPROVE" >CONFIRMED EXCESS QUANTITIES</td>
   <?PHP
	}
	else if($intExcessApproved !='')
	{
	?>
   <td colspan="9" class="APPROVE" >PENDING EXCESS QUANTITIES</td>
   <?PHP
	}
	?>
</tr>

  
  <tr>
  <?php
 	if($intStatus==1)
	{
	?>
   <td colspan="9" class="APPROVE" >CONFIRMED SRN</td>
   <?PHP
	}
	else if($intStatus==0)
	{
   ?>
   <td colspan="9" class="APPROVE" style="color:#F00">REJECTED SRN</td>
   <?php
	}
	else
	{
   ?>
   <td width="10%" colspan="9" class="APPROVE">PENDING SRN</td>
   <?php
	}
   ?>
   <?php
   //-----------------
	  $sqlc = "SELECT
			ware_mrnheader_approvedby.intApproveUser,
			ware_mrnheader_approvedby.dtApprovedDate,
			sys_users.strUserName as UserName,
			ware_mrnheader_approvedby.intApproveLevelNo
			FROM
			ware_mrnheader_approvedby
			Inner Join sys_users ON ware_mrnheader_approvedby.intApproveUser = sys_users.intUserId
			WHERE
			ware_mrnheader_approvedby.intRequisitionNo =  '$SRNNo' AND
			ware_mrnheader_approvedby.intYear =  '$SRNYear' AND
			ware_mrnheader_approvedby.intApproveLevelNo =  '$intStatus'
";
	 $resultc = $db->RunQuery($sqlc);
	 $rowc=mysqli_fetch_array($resultc);
	 $rejectBy=$rowc['UserName'];
	 $rejectDate=$rowc['dtApprovedDate'];

   //-----------------
   ?>
  </tr>
  <tr>
    <td width="1%">&nbsp;</td>
    <td width="10%"><span class="normalfnt">SRN No</span></td>
    <td width="1%" align="center" valign="middle">:</td>
    <td width="20%"><span class="normalfnt"><?php echo $SRNNo.'/'.$SRNYear; ?></span></td>
    <td width="3%">&nbsp;</td>
    <td width="3%" align="center" valign="middle">&nbsp;</td>
    <td width="8%"><span class="normalfnt">Date</span></td>
    <td width="1%" align="center" valign="middle">:</td>
    <td width="20%"><span class="normalfnt"><?php echo $Date; ?></span></td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">SRN From</td>
    <td align="center" valign="middle">:</td>
    <td colspan="2"><span class="normalfnt"><?php echo $SRNFrom; ?></span></td>
    <td align="center" valign="middle">&nbsp;</td>
    <td><span class="normalfnt">Order Year</span></td>
    <td align="center" valign="top">:</td>
    <td rowspan="1" class="normalfnt"><?php echo $orderYear; ?></td>
    </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">SRN To</td>
    <td align="center" valign="middle">:</td>
    <td colspan="2"><span class="normalfnt"><?php echo $SRNTo; ?></span></td>
    <td align="center" valign="middle">&nbsp;</td>
    <td class="normalfnt">Order Nos</td>
    <td align="center" valign="top">:</td>
    <td align="center" class="normalfnt"><?php echo $orders; ?></td>
    </tr>
  </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
         <td colspan="7" class="normalfnt">
          <table width="100%" class="bordered tblMainGrid" id="tbl1" >
            	<tr class="">
                 <th width="13%" nowrap="nowrap" >Main Category</th>
                 <th width="18%" nowrap="nowrap" >Sub Category</th>
                 <th width="36%" nowrap="nowrap">Item Description </th>
                 <th width="7%" >Maximum SRN Qty</th>
                 <th width="3%" >Qty</th>
                 <th width="5%" >Extra Qty</th>
                </tr>
                <?php
				$result1 = getGridDetails($SRNNo,$SRNYear,$locationId,$SRNFrom);
				while($row1=mysqli_fetch_array($result1))
				{
					$ordersArray 	= explode(",", $orders);
					$resp			=	$obj_st_req_get->get_max_qty($ordersArray,$orderYear,$row1['intItemId'],$companyId,'RunQuery');
					$maxQty			=$resp['maxQty'];
				?>
                <tr class="normalfnt">
                 <td align="left" class="normalfnt"><?php echo $row1['mainCatName']; ?></td>
                 <td align="left" class="normalfnt"><?php echo $row1['subCatName']; ?></td>
                 <td align="left" class="normalfnt" ><?php echo $row1['itemName']; ?></td>
                 <td style="text-align:center" class="normalfnt" ><?php echo round($maxQty,6); ?></td>
                 <td style="text-align:center" class="normalfnt" ><?php echo $row1['dblQty']; ?></td>
                 <td style="text-align:center" class="normalfnt" ><?php echo $row1['dblExQty']; ?></td>
                </tr>
                <?php
				
				}
				?>
            </table>
          </td>
         </tr>
      
      </table>
    </td></tr>
	<?php
  	$finalPrintedApproval=0;
    if($int25ExceedingApproved != ''){
		$sql = "SELECT 
				ware_srn_25_exceeding_approval.intApproveLevelNo,
				ware_srn_25_exceeding_approval.dtApprovedDate,
				sys_users.strUserName
				FROM
				ware_srn_25_exceeding_approval
				INNER JOIN sys_users ON ware_srn_25_exceeding_approval.intApproveUser = sys_users.intUserId
				WHERE
				ware_srn_25_exceeding_approval.intRequisitionNo = $SRNNo AND
				ware_srn_25_exceeding_approval.intYear = $SRNYear /*AND
				ware_srn_25_exceeding_approval.intStatus = 0 */
				order by dtApprovedDate asc";
		$result = $db->RunQuery($sql);
		while($rowA=mysqli_fetch_array($result)){
		$by	= $rowA['strUserName'].'  ('.$rowA['dtApprovedDate'].')'; 
		
					if($rowA['intApproveLevelNo']==-10){
					$desc="Over 25% Completed By ";
					$col ="#00CC66";
					}
					else if($rowA['intApproveLevelNo']==-2){
					$desc="Over 25% Cancelled By ";
					$col ="#FF0000";
					}
					else if($rowA['intApproveLevelNo']==-1){
					$desc="Over 25% Revised By ";
					$col ="#FF8040";
					}
					else if($rowA['intApproveLevelNo']==0){
					$desc="Over 25% Rejected By ";
					$col ="#FF8040";
					}
					else if($rowA['intApproveLevelNo']==1){
					$desc="Over 25% - 1st Approved By ";
					$col ="#000000";
					}
					else if($rowA['intApproveLevelNo']==2){
					$desc="Over 25% - 2nd Approved By ";
					$col ="#000000";
					}
					else if($rowA['intApproveLevelNo']==3){
					$desc="Over 25%  - 3rd Approved By ";
					$col ="#000000";
					}
					else{
					$desc=$rowA['intApproveLevelNo']."th Approved By ";
					$col ="#000000";
					}
					//  $desc=$ap.$desc;
					$desc2=$rowA['strUserName']." (".$rowA['dtApprovedDate'].")";
					if($rowA['strUserName']=='')
					$by='---------------------------------';
					$desc_exceed=$desc." - ".$by;
					
					$finalPrintedApproval=$rowA['intApproveLevelNo'];
					//$desc=str_pad($desc,25," ",STR_PAD_RIGHT);
					//$desc=str_replace(" ","&nbsp;",$desc);
		echo '<tr><td bgcolor="#FFFFFF"><span class="normalfnt"><strong>'.$desc_exceed.'</strong></span></td></tr>';
		}
/*			$int25ExceedingApproved = $row['int25ExceedingApproved'];
			$int100ExceedingApproved = $row['int100ExceedingApproved'];
			$int25Exceeding_levels 	= $row['int25ExceedingApproveLevels'];
			$int100Exceeding_levels = $row['int100ExceedingApproveLevels'];
*/			//echo $finalPrintedApproval.'-'.$int25Exceeding_levels.'-'.$finalPrintedApproval.'-'.$int25ExceedingApproved;
			if(($finalPrintedApproval<$int25Exceeding_levels) && ($finalPrintedApproval>=0) && ($int25ExceedingApproved>0)){
			for($j=$finalPrintedApproval+1; $j<=$int25Exceeding_levels; $j++)
			{ 
				if($j==1)
				$desc="Over 25% - 1st Approved By ";
				else if($j==2)
				$desc="Over 25% - 2nd Approved By ";
				else if($j==3)
				$desc="Over 25% - 3rd Approved By ";
				else
				$desc="Over 25% - ".$j."th Approved By ";
				$desc2='---------------------------------';
 			?>
				<tr>
					<td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc;?>- </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
				</tr>
		<?php
			}
			}
		
	}
	else if($int100ExceedingApproved != ''){
		$sql = "SELECT 
				ware_srn_100_exceeding_approval.intApproveLevelNo,
				ware_srn_100_exceeding_approval.dtApprovedDate,
				sys_users.strUserName as UserName
				FROM
				ware_srn_100_exceeding_approval
				INNER JOIN sys_users ON ware_srn_100_exceeding_approval.intApproveUser = sys_users.intUserId
				WHERE
				ware_srn_100_exceeding_approval.intRequisitionNo = $SRNNo AND
				ware_srn_100_exceeding_approval.intYear = $SRNYear /*AND
				ware_srn_100_exceeding_approval.intStatus = 0*/
				order by dtApprovedDate asc";
		$result = $db->RunQuery($sql);
		while($rowA=mysqli_fetch_array($result)){
		$by	= $rowA['strUserName'].'  ('.$rowA['dtApprovedDate'].')'; 
		
					if($rowA['intApproveLevelNo']==-10){
					$desc="Over 100% Completed By ";
					$col ="#00CC66";
					}
					else if($rowA['intApproveLevelNo']==-2){
					$desc="Over 100% Cancelled By ";
					$col ="#FF0000";
					}
					else if($rowA['intApproveLevelNo']==-1){
					$desc="Over 100% Revised By ";
					$col ="#FF8040";
					}
					else if($rowA['intApproveLevelNo']==0){
					$desc="Over 100% Rejected By ";
					$col ="#FF8040";
					}
					else if($rowA['intApproveLevelNo']==1){
					$desc="Over 100% - 1st Approved By ";
					$col ="#000000";
					}
					else if($rowA['intApproveLevelNo']==2){
					$desc="Over 100% - 2nd Approved By ";
					$col ="#000000";
					}
					else if($rowA['intApproveLevelNo']==3){
					$desc="Over 100%  - 3rd Approved By ";
					$col ="#000000";
					}
					else{
					$desc="Over 100%  - ".$rowA['intApproveLevelNo']."th Approved By ";
					$col ="#000000";
					}
					//  $desc=$ap.$desc;
					$desc2=$rowA['UserName']." (".$rowA['dtApprovedDate'].")";
					if($rowA['UserName']=='')
					$desc2='---------------------------------';
					
					
					$desc_exceed=$desc." - ".$desc2;
					
					$finalPrintedApproval=$rowA['intApproveLevelNo'];
					//$desc=str_pad($desc,25," ",STR_PAD_RIGHT);
					//$desc=str_replace(" ","&nbsp;",$desc);
		echo '<tr><td bgcolor="#FFFFFF"><span class="normalfnt"><strong>'.$desc_exceed.'</strong></span></td></tr>';
		}

	
/*			$int25ExceedingApproved = $row['int25ExceedingApproved'];
			$int100ExceedingApproved = $row['int100ExceedingApproved'];
			$int25Exceeding_levels 	= $row['int25ExceedingApproveLevels'];
			$int100Exceeding_levels = $row['int100ExceedingApproveLevels'];
*/			if(($finalPrintedApproval<$int100Exceeding_levels) && ($finalPrintedApproval>=0) && ($int100ExceedingApproved>0)){
			for($j=$finalPrintedApproval+1; $j<=$int100Exceeding_levels; $j++)
			{ 
				if($j==1)
				$desc="Over 100% - 1st Approved By ";
				else if($j==2)
				$desc="Over 100% - 2nd Approved By ";
				else if($j==3)
				$desc="Over 100% - 3rd Approved By ";
				else
				$desc="Over 100% - ".$j."th Approved By ";
				$desc2='---------------------------------';
 			?>
				<tr>
					<td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc;?>- </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
				</tr>
		<?php
			}
			}
	}
	else if($intExcessApproved != ''){
		$sql = "SELECT 
				ware_srn_excess_approval.intApproveLevelNo,
				ware_srn_excess_approval.dtApprovedDate,
				sys_users.strUserName as UserName
				FROM
				ware_srn_excess_approval
				INNER JOIN sys_users ON ware_srn_excess_approval.intApproveUser = sys_users.intUserId
				WHERE
				ware_srn_excess_approval.intRequisitionNo = $SRNNo AND
				ware_srn_excess_approval.intYear = $SRNYear /*AND
				ware_srn_excess_approval.intStatus = 0*/
				order by dtApprovedDate asc";
		$result = $db->RunQuery($sql);
		while($rowA=mysqli_fetch_array($result)){
		$by	= $rowA['strUserName'].'  ('.$rowA['dtApprovedDate'].')'; 
		
					if($rowA['intApproveLevelNo']==-10){
					$desc="Excess Completed By ";
					$col ="#00CC66";
					}
					else if($rowA['intApproveLevelNo']==-2){
					$desc="Excess Cancelled By ";
					$col ="#FF0000";
					}
					else if($rowA['intApproveLevelNo']==-1){
					$desc="Excess Revised By ";
					$col ="#FF8040";
					}
					else if($rowA['intApproveLevelNo']==0){
					$desc="Excess Rejected By ";
					$col ="#FF8040";
					}
					else if($rowA['intApproveLevelNo']==1){
					$desc="Excess - 1st Approved By ";
					$col ="#000000";
					}
					else if($rowA['intApproveLevelNo']==2){
					$desc="Excess - 2nd Approved By ";
					$col ="#000000";
					}
					else if($rowA['intApproveLevelNo']==3){
					$desc="Excess  - 3rd Approved By ";
					$col ="#000000";
					}
					else{
					$desc="Excess  - ".$rowA['intApproveLevelNo']."th Approved By ";
					$col ="#000000";
					}
					//  $desc=$ap.$desc;
					$desc2=$rowA['UserName']." (".$rowA['dtApprovedDate'].")";
					if($rowA['UserName']=='')
					$desc2='---------------------------------';
					
					
					$desc_exceed=$desc." - ".$desc2;
					
					$finalPrintedApproval=$rowA['intApproveLevelNo'];
					//$desc=str_pad($desc,25," ",STR_PAD_RIGHT);
					//$desc=str_replace(" ","&nbsp;",$desc);
		echo '<tr><td bgcolor="#FFFFFF"><span class="normalfnt"><strong>'.$desc_exceed.'</strong></span></td></tr>';
		}

	
			$intExcessApproved = $row['intExcessApproved'];
 			$intExcess_levels  = $row['intExcessApproveLevels'];
 			if(($finalPrintedApproval<$intExcess_levels) && ($finalPrintedApproval>=0) && ($intExcessApproved>0)){
			for($j=$finalPrintedApproval+1; $j<=$intExcess_levels; $j++)
			{ 
				if($j==1)
				$desc="Excess - 1st Approved By ";
				else if($j==2)
				$desc="Excess - 2nd Approved By ";
				else if($j==3)
				$desc="Excess - 3rd Approved By ";
				else
				$desc="Excess - ".$j."th Approved By ";
				$desc2='---------------------------------';
 			?>
				<tr>
					<td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc;?>- </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
				</tr>
		<?php
			}
			}
	}
	else{
		$desc_exceed='';	
	}
    ?>  
     
    
</tr>
<?php  
					 $flag=0;
					  $sqlc = "SELECT
							ware_storesrequesitionheader_approvedby.intApproveUser,
							ware_storesrequesitionheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName,
							ware_storesrequesitionheader_approvedby.intApproveLevelNo
							FROM
							ware_storesrequesitionheader_approvedby
							Inner Join sys_users ON ware_storesrequesitionheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							ware_storesrequesitionheader_approvedby.intRequisitionNo =  '$SRNNo' AND
							ware_storesrequesitionheader_approvedby.intYear =  '$SRNYear'  /*AND 
							ware_storesrequesitionheader_approvedby.intStatus='0'  */
							order by dtApprovedDate asc";
					 $resultc = $db->RunQuery($sqlc);
					while($rowc=mysqli_fetch_array($resultc)){
						if($rowc['intApproveLevelNo']==1)
						$desc="1st ";
						else if($rowc['intApproveLevelNo']==2)
						$desc="2nd ";
						else if($rowc['intApproveLevelNo']==3)
						$desc="3rd ";
						else
						$desc=$rowc['intApproveLevelNo']."th ";
					 //  $desc=$ap.$desc;
					 $desc2=$rowc['UserName']."(".$rowc['dtApprovedDate'].")";
					 if($rowc['UserName']=='')
					 $desc2='---------------------------------';
					 
					 if($rowc['intApproveLevelNo']>0){
						 $flag=1;
				?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> Approved By - </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
            </tr>
<?php
	//}
	//else{			
					}
					 if($rowc['intApproveLevelNo']==0){
						 $flag=2;
					  ?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong> Rejected By - </strong></span><span class="normalfnt"><?php echo $rowc['UserName']."(".$rowc['dtApprovedDate'].")";?></span></td>
            </tr>
<?php
					 }
					 if($rowc['intApproveLevelNo']==-1){
						 $flag=3;
					  ?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong> Revised By - </strong></span><span class="normalfnt"><?php echo $rowc['UserName']."(".$rowc['dtApprovedDate'].")";?></span></td>
            </tr>
<?php
					 }
					 
		 if($rowc['intApproveLevelNo']>0){
			 $finalSavedLevel=$rowc['intApproveLevelNo'];
		 }
	 }//end of while
	 
	if($flag>1){
		$finalSavedLevel=0;
	}
	
	
	//echo $flag.'-'.$intStatus.'-'.$finalSavedLevel.'-'.$savedLevels;
	if(($flag<=1) || ($intStatus>0)){
	for($j=$finalSavedLevel+1; $j<=$savedLevels; $j++)
	{ 
		if($j==1)
		$desc="1st ";
		else if($j==2)
		$desc="2nd ";
		else if($j==3)
		$desc="3rd ";
		else
		$desc=$j."th ";
		$desc2='---------------------------------';
	?>
        <tr>
            <td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> Approved By - </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
        </tr>
<?php
	}
	}
?>
<tr height="205" >
  <td align="center" class="normalfntMid" valign="bottom"></td>
</tr>
<tr height="40">
<?php
	//$locationId = '';
	$sql = "SELECT mst_locations.intCompanyId FROM mst_locations WHERE mst_locations.intId =  '$locationId'";
	$result 	= $db->RunQuery($sql);
	$row		= mysqli_fetch_array($result);
	$createCompanyId = $row['intCompanyId'];
?>
<?Php 
	$url  = "{$backwardseperator}presentation/sendToApproval.php";		// * file name
	$url .= "?status=$intStatus";										// * set recent status
	$url .= "&approveLevels=$savedLevels";								// * set approve levels in order header
	$url .= "&programCode=$programCode";								// * program code (ex:P10001)
	$url .= "&program=SRN";									// * program name (ex:Purchase Order)
	$url .= "&companyId=$createCompanyId";									// * created company id
	$url .= "&createUserId=$createdUser";	
	
	$url .= "&field1=SRN No";												 
	$url .= "&field2=SRN Year";	
	$url .= "&value1=$SRNNo";												 
	$url .= "&value2=$SRNYear";	
	
	$url .= "&subject=SRN FOR APPROVAL ('$SRNNo'/'$SRNYear')";	
	
	$url .= "&statement1=Please Approve this";	
	$url .= "&statement2=to Approve this";	
							// * doc year
	$url .= "&link=".urlencode(base64_encode($mainPath."presentation/warehouse/stores_requesition_note/listing/rpt_stores_requesition_note_exceeding.php?SRNNo=$SRNNo&year=$SRNYear&approveMode=1"));
?>
  <td align="center" class="normalfntMid"><iframe id="iframeFiles2" src="<?php echo $url;?>" name="iframeFiles" style="width:500px;height:150px;border:none"  ></iframe></td>
</tr>
<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
</body>
</html>
<?php
function getMainApproveLevel($program)
{
	global $db;
	$sql = "SELECT intApprovalLevel FROM sys_approvelevels WHERE strName='$program' ";
	$result = $db->RunQuery($sql);
	$row = mysqli_fetch_array($result);
	return $row['intApprovalLevel'];
}
function getGridDetails($SRNNo,$SRNYear,$locationId,$SRNFrom)
{
	global $db;
	$sql = "SELECT SRD.intRequisitionNo,SRD.intRequisitionYear,SRD.intItemId,MI.strName AS itemName,
			MC.strName AS mainCatName,MC.intId AS mainCatId,SC.strName AS subCatName,SC.intId AS subCatId,SRD.dblQty, SRD.dblExQty 
			FROM ware_storesrequesitiondetails SRD
			INNER JOIN mst_item MI ON MI.intId=SRD.intItemId
			INNER JOIN mst_maincategory MC ON MC.intId=MI.intMainCategory
			INNER JOIN mst_subcategory SC ON SC.intId=MI.intSubCategory
			WHERE SRD.intRequisitionNo='$SRNNo' AND
			SRD.intRequisitionYear='$SRNYear'";
	$result = $db->RunQuery($sql);
	return $result;
}
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
			 
	return $editMode;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	$sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
	
//------------------------------function loadRejectionMode-------------------
function loadRejectionMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$rejectMode=0;
	$sqlp = "SELECT
		menupermision.intReject 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intReject']==1){
	 if($intStatus!=0){
	 $rejectMode=1;
	 }
	}
	 
	return $rejectMode;
}

?>
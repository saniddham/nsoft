<?php
//ini_set('display_errors',1);
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 		= $_SESSION['mainPath'];
	$userId 		= $_SESSION['userId'];
	$requestType	= $_REQUEST['status'];
	$locationId 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	
	$pgrmCode		=	'P0256';
	
	include_once "{$backwardseperator}dataAccess/Connector.php";
	require_once $_SESSION['ROOT_PATH']."class/cls_commonErrorHandeling_get.php";
	require_once $_SESSION['ROOT_PATH']."class/warehouse/stores_requesition_note/cls_stores_requesition_note_get.php";
 	require_once $_SESSION['ROOT_PATH']."class/warehouse/stores_requesition_note/cls_stores_requesition_note_set.php";
 	
	$obj_st_req_get	= new cls_stores_requesition_note_get($db);
	$obj_st_req_set	= new cls_stores_requesition_note_set($db);
	
	
	
	$obj_com_err_get	=	new cls_commonErrorHandeling_get($db);
	
	$SRNNo		  = $_REQUEST['SRNNo'];
	$SRNYear	  = $_REQUEST['SRNYear'];

if($requestType=='approve')
{
	$db->OpenConnection();
	$db->RunQuery2('Begin');
	$rollBackFlag 	= 0;
	
		$sql = "SELECT intStatus, 
				intApproveLevels ,
				ware_storesrequesitionheader.int25ExceedingApproved,
				ware_storesrequesitionheader.int100ExceedingApproved,
				ware_storesrequesitionheader.int25ExceedingApproveLevels,
				ware_storesrequesitionheader.int100ExceedingApproveLevels ,
				ware_storesrequesitionheader.intExcessApproved,
				ware_storesrequesitionheader.intExcessApproveLevels 
 				FROM ware_storesrequesitionheader 
				WHERE intRequisitionNo = '$SRNNo' AND 
				intRequisitionYear = '$SRNYear' ";
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		
		$approveLevel = $row['intApproveLevels'];
		$status    = $row['intStatus'];
		$newApprLevel = ($approveLevel+1)-$status;
		$int25ExceedingApproved = $row['int25ExceedingApproved'];
		$int100ExceedingApproved = $row['int100ExceedingApproved'];
		$int25Exceeding_levels 	= $row['int25ExceedingApproveLevels'];
		$int100Exceeding_levels = $row['int100ExceedingApproveLevels'];
		$intExcessApproved 	= $row['intExcessApproved'];
		$intExcess_levels 	= $row['intExcessApproveLevels'];
		
		//$confirmatonMode=loadConfirmatonMode($pgrmCode,$status,$approveLevel,$userId);
		$res			=$obj_com_err_get->get_permision_withApproval_confirm($status,$approveLevel,$userId,$pgrmCode,'RunQuery2');
		$confirmatonMode=$res['permision'];
		
		if($status==1){// 
			$rollBackFlag 	= 1;
			$rollBackMsg 	="Final confirmation of this SRN is already raised"; 
		}
 		else if((($int25ExceedingApproved>1) && ($int25ExceedingApproved <= $int25Exceeding_levels+1)) || (($int100ExceedingApproved>1) && ($int100ExceedingApproved <= $int100Exceeding_levels+1))  || (($intExcessApproved>1) && ($intExcessApproved <= $intExcess_levels+1)) ){
			
			$rollBackMsg	.=" Please approve  "; 
			if(($int25ExceedingApproved>1) && ($int25ExceedingApproved <= $int25Exceeding_levels+1))
			$rollBackMsg	.=" '25% exceeded' Qtys,"; 
			if(($int100ExceedingApproved>1) && ($int100ExceedingApproved <= $int100Exceeding_levels+1))
			$rollBackMsg	.=" '100% exceeded' Qtys,"; 
			if((($intExcessApproved>1) && ($intExcessApproved <= $intExcess_levels+1)))
			$rollBackMsg	.=" 'Excess' Qtys"; 
			
			$rollBackFlag	= 1;
			$rollBackMsg	.=" Before Approve SRN"; 
		}
		else if(($int25ExceedingApproved=='0') || ($int100ExceedingApproved=='0')){// 
			$rollBackFlag	= 1;
			$rollBackMsg	="Qtys Rejected.Can't Approve SRN".'-'.$int25ExceedingApproved.'-'.$int100ExceedingApproved; 
		}
		else if($confirmatonMode==0){// 
			$rollBackFlag	= 1;
			$rollBackMsg 	="No Permission to Approve"; 
		}
	
		if($rollBackFlag==0){
			$sqlUpd = "UPDATE ware_storesrequesitionheader 
						SET
						intStatus = intStatus-1
						WHERE
						intRequisitionNo = '$SRNNo' AND 
						intRequisitionYear = '$SRNYear' ";
			$resultUpd = $db->RunQuery2($sqlUpd);
			if(!$resultUpd)
			{
					$rollBackFlag			= 1;
					$rollBackMsg			= $db->errormsg;
			}
		}
		
		if($rollBackFlag==0){ 
		
			$sql = "SELECT intStatus, 
					intApproveLevels ,
					ware_storesrequesitionheader.int25ExceedingApproved,
					ware_storesrequesitionheader.int100ExceedingApproved,
					ware_storesrequesitionheader.int25ExceedingApproveLevels,
					ware_storesrequesitionheader.int100ExceedingApproveLevels ,
					ware_storesrequesitionheader.intExcessApproved,
					ware_storesrequesitionheader.intExcessApproveLevels 
					FROM ware_storesrequesitionheader 
					WHERE intRequisitionNo = '$SRNNo' AND 
					intRequisitionYear = '$SRNYear' ";
			$result = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($result);
			
			$approveLevel = $row['intApproveLevels'];
			$status    = $row['intStatus'];
			$newApprLevel = ($approveLevel+1)-$status;
		
			$sqlIns = "INSERT INTO ware_storesrequesitionheader_approvedby 
						(
						intRequisitionNo, 
						intYear, 
						intApproveLevelNo, 
						intApproveUser, 
						dtApprovedDate, 
						intStatus
						)
						VALUES
						(
						'$SRNNo', 
						'$SRNYear', 
						'$newApprLevel', 
						'$userId', 
						now(), 
						0
						) ";
			$resultIns = $db->RunQuery2($sqlIns);
			if($resultIns)
			{
				$rollBackFlag			= 0;
				$rollBackMsg			= "Approval raised sucessfully";
			}
			else
			{
				$rollBackFlag			= 1;
				$rollBackMsg			= $db->errormsg;
			}
		}
	
	
	if($rollBackFlag==1)
	{
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $rollBackMsg;
	}
	else
	{
		$db->RunQuery2('Commit');
		$response['type'] 		= 'pass';
		$response['msg'] 		= $rollBackMsg;
	}
	
	$db->CloseConnection();
	echo json_encode($response);
}

if($requestType=='approveQtys')
{
	$programCode	= 'P0798';
	$db->OpenConnection();
	$db->RunQuery2('Begin');
	$rollBackFlag 	= 0;
	
		$sql = "SELECT intStatus, 
				intApproveLevels ,
				ware_storesrequesitionheader.int25ExceedingApproved,
				ware_storesrequesitionheader.int100ExceedingApproved,
				ware_storesrequesitionheader.int25ExceedingApproveLevels,
				ware_storesrequesitionheader.int100ExceedingApproveLevels ,
				ware_storesrequesitionheader.intExcessApproved,
				ware_storesrequesitionheader.intExcessApproveLevels 
 				FROM ware_storesrequesitionheader 
				WHERE intRequisitionNo = '$SRNNo' AND 
				intRequisitionYear = '$SRNYear' ";
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		
		$approveLevel = $row['intApproveLevels'];
		$status    = $row['intStatus'];
		$newApprLevel = ($approveLevel+1)-$status;
		$int25ExceedingApproved = $row['int25ExceedingApproved'];
		$int100ExceedingApproved = $row['int100ExceedingApproved'];
		$int25Exceeding_levels 	= $row['int25ExceedingApproveLevels'];
		$int100Exceeding_levels = $row['int100ExceedingApproveLevels'];
		$intExcessApproved 	= $row['intExcessApproved'];
		$intExcess_levels 	= $row['intExcessApproveLevels'];
		
		
		if($status==1){// 
			$rollBackFlag 	= 1;
			$rollBackMsg 	="Final confirmation of this SRN is already raised"; 
		}
		else if(($int25ExceedingApproved=='0') || ($int100ExceedingApproved=='0') || ($intExcessApproved=='0')){// 
			$rollBackFlag	= 1;
			$rollBackMsg	="Qtys Rejected.Can't Approve SRN"; 
		}
		else if(($int25ExceedingApproved!='2') && ($int100ExceedingApproved!='2') && ($intExcessApproved!='2')){// 
			$rollBackFlag	= 1;
			$rollBackMsg	="No Pending to Approve"; 
		}
		
 		if((($int25ExceedingApproved>1) && ($int25ExceedingApproved <= $int25Exceeding_levels+1)) || (($int100ExceedingApproved>1) && ($int100ExceedingApproved <= $int100Exceeding_levels+1))  || (($intExcessApproved>1) && ($intExcessApproved <= $intExcess_levels+1)) ){
			
 			
			if(($int100ExceedingApproved>1)){
				$app_type	= 3;
				$confirmatonMode	=$obj_st_req_get->get_permision_confirm($company,$programCode,$app_type,$userId,'RunQuery2');
				if($confirmatonMode ==1){
					$up_str		.= " int100ExceedingApproved = 1 ";
					if($intExcessApproved>1)
					$up_str		.= " , intExcessApproved = 1 ";
					if($int25ExceedingApproved>1)
					$up_str		.= " , int25ExceedingApproved = 1 ";
				}
			}
			else if(($int25ExceedingApproved>1) ){
				$app_type	= 2;
				$confirmatonMode	=$obj_st_req_get->get_permision_confirm($company,$programCode,$app_type,$userId,'RunQuery2');
				if($confirmatonMode ==1){
					$up_str		.= " int25ExceedingApproved = 1 ";
					if($intExcessApproved>1)
					$up_str		.= " , intExcessApproved = 1 ";
				}
			}
			else if((($intExcessApproved>1))){
				$app_type	= 1;
				$confirmatonMode	=$obj_st_req_get->get_permision_confirm($company,$programCode,$app_type,$userId,'RunQuery2');
				if($confirmatonMode ==1)
				$up_str		.= " intExcessApproved = 1 ";
			}
 		}
		
		$confirmatonMode	=$obj_st_req_get->get_permision_confirm($company,$programCode,$app_type,$userId,'RunQuery2');
		if($confirmatonMode==0){// 
			$rollBackFlag	= 1;
			$rollBackMsg 	="No Permission to Approve"; 
		}
	
		if($rollBackFlag==0){
			$sqlUpd = "UPDATE ware_storesrequesitionheader 
						SET
						 $up_str 
						WHERE
						intRequisitionNo = '$SRNNo' AND 
						intRequisitionYear = '$SRNYear' ";
			$resultUpd = $db->RunQuery2($sqlUpd);
			if(!$resultUpd)
			{
					$rollBackFlag			= 1;
					$rollBackMsg			= $db->errormsg;
			}
		}
		
		if($rollBackFlag==0){ 
 		
			$sqlIns = "INSERT INTO ware_storesrequesitionheader_so_wise_approvedby 
						(
						intRequisitionNo, 
						intYear, 
						intApproveLevelNo, 
						intApproveUser, 
						dtApprovedDate, 
						intStatus
						)
						VALUES
						(
						'$SRNNo', 
						'$SRNYear', 
						'$app_type', 
						'$userId', 
						now(), 
						0
						) ";
			$resultIns = $db->RunQuery2($sqlIns);
			if($resultIns)
			{
				$rollBackFlag			= 0;
				$rollBackMsg			= "Approval raised sucessfully";
			}
			else
			{
				$rollBackFlag			= 1;
				$rollBackMsg			= $db->errormsg;
			}
		}
	
	
	if($rollBackFlag==1)
	{
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $rollBackMsg;
	}
	else
	{
		$db->RunQuery2('Commit');
		$response['type'] 		= 'pass';
		$response['msg'] 		= $rollBackMsg;
	}
	
	$db->CloseConnection();
	echo json_encode($response);
}

else if($requestType=='reject')
{
	$db->OpenConnection();
	$db->RunQuery2('Begin');
	$rollBackFlag 	= 0;
	
	$sqlUpd = "UPDATE ware_storesrequesitionheader 
				SET
				intStatus = 0
				WHERE
				intRequisitionNo = '$SRNNo' AND 
				intRequisitionYear = '$SRNYear' ";
	$resultUpd = $db->RunQuery2($sqlUpd);
	if($resultUpd)
	{
		$sqlMaxStatus = "SELECT MAX(intStatus) AS maxStatus 
							FROM ware_storesrequesitionheader_approvedby 
							WHERE intRequisitionNo='$SRNNo' AND 
							intYear='$SRNYear' ";
		$resultMax = $db->RunQuery2($sqlMaxStatus);
		$rowMax = mysqli_fetch_array($resultMax);
		$maxStatus = $rowMax['maxStatus'];
		
		$sqlUpd = "UPDATE ware_storesrequesitionheader_approvedby 
					SET intStatus = $maxStatus+1 
					WHERE intRequisitionNo='$SRNNo' AND
					intYear='$SRNYear' AND 
					intStatus = 0 ";
		$result = $db->RunQuery2($sqlUpd);
		if($result)
		{
			$sqlIns = "INSERT INTO ware_storesrequesitionheader_approvedby 
						(
						intRequisitionNo, 
						intYear, 
						intApproveLevelNo, 
						intApproveUser, 
						dtApprovedDate, 
						intStatus
						)
						VALUES
						(
						'$SRNNo', 
						'$SRNYear', 
						0, 
						'$userId', 
						now(), 
						0
						) ";
			$resultIns = $db->RunQuery2($sqlIns);
			if($resultIns)
			{
				$rollBackFlag			= 0;
				$rollBackMsg			= "Rejected sucessfully";
			}
			else
			{
				$rollBackFlag			= 1;
				$rollBackMsg			= $db->errormsg;
			}
		}
		else
		{
			$rollBackFlag			= 1;
			$rollBackMsg			= $db->errormsg;
		}

	}
	else
	{
		$rollBackFlag			= 1;
		$rollBackMsg			= $db->errormsg;
	}
	if($rollBackFlag==1)
	{
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $rollBackMsg;
	}
	else
	{
		$db->RunQuery2('Commit');
		$response['type'] 		= 'pass';
		$response['msg'] 		= $rollBackMsg;
	}
	
	$db->CloseConnection();
	echo json_encode($response);
}

else if($requestType=='rejectQtys')
{
 	
	$programCode	= 'P0798';
	$db->OpenConnection();
	$db->RunQuery2('Begin');
	$rollBackFlag 	= 0;
	
		$sql = "SELECT intStatus, 
				intApproveLevels ,
				ware_storesrequesitionheader.int25ExceedingApproved,
				ware_storesrequesitionheader.int100ExceedingApproved,
				ware_storesrequesitionheader.int25ExceedingApproveLevels,
				ware_storesrequesitionheader.int100ExceedingApproveLevels ,
				ware_storesrequesitionheader.intExcessApproved,
				ware_storesrequesitionheader.intExcessApproveLevels 
 				FROM ware_storesrequesitionheader 
				WHERE intRequisitionNo = '$SRNNo' AND 
				intRequisitionYear = '$SRNYear' ";
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		
		$approveLevel = $row['intApproveLevels'];
		$status    = $row['intStatus'];
		$newApprLevel = ($approveLevel+1)-$status;
		$int25ExceedingApproved = $row['int25ExceedingApproved'];
		$int100ExceedingApproved = $row['int100ExceedingApproved'];
		$int25Exceeding_levels 	= $row['int25ExceedingApproveLevels'];
		$int100Exceeding_levels = $row['int100ExceedingApproveLevels'];
		$intExcessApproved 	= $row['intExcessApproved'];
		$intExcess_levels 	= $row['intExcessApproveLevels'];
		
		
		if($status==1){// 
			$rollBackFlag 	= 1;
			$rollBackMsg 	="Final confirmation of this SRN is already raised"; 
		}
		else if(($int25ExceedingApproved=='0' && $int25Exceeding_levels >0) && ($int100ExceedingApproved=='0'  && $int100Exceeding_levels >0) && ($intExcessApproved=='0' && $intExcess_levels > 0)){// 
			$rollBackFlag	= 1;
			$rollBackMsg	="Qtys Already Rejected"; 
		}
		else if(($int25ExceedingApproved=='1') && ($int100ExceedingApproved=='1') && ($intExcessApproved=='1')){// 
			$rollBackFlag	= 1;
			$rollBackMsg	="Qtys Already Confirmed"; 
		}
		
 		if((($int25ExceedingApproved>1) && ($int25ExceedingApproved <= $int25Exceeding_levels+1)) || (($int100ExceedingApproved>1) && ($int100ExceedingApproved <= $int100Exceeding_levels+1))  || (($intExcessApproved>1) && ($intExcessApproved <= $intExcess_levels+1)) ){
  			
			if(($int100ExceedingApproved>1)){
				$app_type	= 3;
				$confirmatonMode	=$obj_st_req_get->get_permision_confirm($company,$programCode,$app_type,$userId,'RunQuery2');
				if($confirmatonMode ==1){
					$up_str		.= " int100ExceedingApproved = 0 ";
					if($intExcessApproved>1)
					$up_str		.= " , intExcessApproved = 0 ";
					if($int25ExceedingApproved>1)
					$up_str		.= " , int25ExceedingApproved = 0 ";
				}
			}
			else if(($int25ExceedingApproved>1) ){
				$app_type	= 2;
				$confirmatonMode	=$obj_st_req_get->get_permision_confirm($company,$programCode,$app_type,$userId,'RunQuery2');
				if($confirmatonMode ==1){
					$up_str		.= " int25ExceedingApproved = 0 ";
					if($intExcessApproved>1)
					$up_str		.= " , intExcessApproved = 0 ";
				}
			}
			else if((($intExcessApproved>1))){
				$app_type	= 1;
				$confirmatonMode	=$obj_st_req_get->get_permision_confirm($company,$programCode,$app_type,$userId,'RunQuery2');
				if($confirmatonMode ==1)
				$up_str		.= " intExcessApproved = 0 ";
			}
 		}
		
		$confirmatonMode	=$obj_st_req_get->get_permision_confirm($company,$programCode,$app_type,$userId,'RunQuery2');
		if($confirmatonMode==0){// 
			$rollBackFlag	= 1;
			$rollBackMsg 	="No Permission to Reject"; 
		}
	
	
	
	$sqlUpd = "UPDATE ware_storesrequesitionheader 
				SET
				$up_str 
				WHERE
				intRequisitionNo = '$SRNNo' AND 
				intRequisitionYear = '$SRNYear' ";
	$resultUpd = $db->RunQuery2($sqlUpd);
	if($resultUpd)
	{
		$sqlMaxStatus = "SELECT MAX(intStatus) AS maxStatus 
							FROM ware_storesrequesitionheader_so_wise_approvedby 
							WHERE intRequisitionNo='$SRNNo' AND 
							intYear='$SRNYear' ";
		$resultMax = $db->RunQuery2($sqlMaxStatus);
		$rowMax = mysqli_fetch_array($resultMax);
		$maxStatus = $rowMax['maxStatus'];
		
		$sqlUpd = "UPDATE ware_storesrequesitionheader_so_wise_approvedby 
					SET intStatus = $maxStatus+1 
					WHERE intRequisitionNo='$SRNNo' AND
					intYear='$SRNYear' AND 
					intStatus = 0 ";
		$result = $db->RunQuery2($sqlUpd);
		if($result)
		{
			$sqlIns = "INSERT INTO ware_storesrequesitionheader_so_wise_approvedby 
						(
						intRequisitionNo, 
						intYear, 
						intApproveLevelNo, 
						intApproveUser, 
						dtApprovedDate, 
						intStatus
						)
						VALUES
						(
						'$SRNNo', 
						'$SRNYear', 
						0, 
						'$userId', 
						now(), 
						0
						) ";
			$resultIns = $db->RunQuery2($sqlIns);
			if($resultIns)
			{
				$rollBackFlag			= 0;
				$rollBackMsg			= "Rejected sucessfully";
			}
			else
			{
				$rollBackFlag			= 1;
				$rollBackMsg			= $db->errormsg;
			}
		}
		else
		{
			$rollBackFlag			= 1;
			$rollBackMsg			= $db->errormsg;
		}

	}
	else
	{
		$rollBackFlag			= 1;
		$rollBackMsg			= $db->errormsg;
	}
	if($rollBackFlag==1)
	{
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $rollBackMsg;
	}
	else
	{
		$db->RunQuery2('Commit');
		$response['type'] 		= 'pass';
		$response['msg'] 		= $rollBackMsg;
	}
	
	$db->CloseConnection();
	echo json_encode($response);
}



else if($_REQUEST['status']=='approveQty_25')
{
	$db->OpenConnection();
	$db->RunQuery2('Begin');
	
	$rollBackFlag=0;
	$rollBackMsg = '';

	$sql = "UPDATE `ware_storesrequesitionheader` SET `int25ExceedingApproved`=`int25ExceedingApproved`-1 WHERE (intRequisitionNo='$SRNNo') AND (`intRequisitionYear`='$SRNYear') and intStatus not in(0,1)";
	$result = $db->RunQuery2($sql);
	if((!$result) && ($rollBackFlag!=1)){
		$rollBackFlag=1;
		$sqlM=$sql;
		$rollBackMsg = "Approval error!";
	}
	else{
		if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
		//sendRejecedEmailToUser($SRNNo,$SRNYear,$objMail,$mainPath,$root_path);
		}
	}

	if($result){
		$sql = "SELECT ware_storesrequesitionheader.int25ExceedingApproved, ware_storesrequesitionheader.int25ExceedingApproveLevels FROM ware_storesrequesitionheader WHERE (intRequisitionNo='$SRNNo') AND (`intRequisitionYear`='$SRNYear')";
		$results = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($results);
		$status = $row['int25ExceedingApproved'];
		$savedLevels = $row['int25ExceedingApproveLevels'];
		
		$approveLevel=$savedLevels+1-$status;
		
		$sqlI = "INSERT INTO `ware_srn_25_exceeding_approval` (`intRequisitionNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
		VALUES ('$SRNNo','$SRNYear','$approveLevel','$userId',now(),0)";
		$resultI = $db->RunQuery2($sqlI);
		if((!$resultI) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sqlI;
			$rollBackMsg = "Approval error!";
		}
	}
	
	
	//-------------------
	if($rollBackFlag==0){
		$db->RunQuery2('Commit');
		$response['type'] 		= 'pass';
		$response['msg'] 		= "Qty Approved sucessfully";
		$response['q'] 			= '';
	}
	else if($rollBackFlag==1){
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $rollBackMsg;
		$response['q'] 			= $sqlM;
	}
	else{
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $db->errormsg;
		$response['q'] 			= $sqlM;
	}
	$db->CloseConnection();	
	
	echo json_encode($response);
		
}
else if($_REQUEST['status']=='approveQty_100')
{
	$db->OpenConnection();
	$db->RunQuery2('Begin');
	
	$rollBackFlag=0;
	$rollBackMsg = '';

	$sql = "UPDATE `ware_storesrequesitionheader` SET `int100ExceedingApproved`=`int100ExceedingApproved`-1 WHERE (intRequisitionNo='$SRNNo') AND (`intRequisitionYear`='$SRNYear') and intStatus not in(0,1)";
	$result = $db->RunQuery2($sql);
	if((!$result) && ($rollBackFlag!=1)){
		$rollBackFlag=1;
		$sqlM=$sql;
		$rollBackMsg = "Approval error!";
	}
	else{
		if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
		//sendRejecedEmailToUser($SRNNo,$SRNYear,$objMail,$mainPath,$root_path);
		}
	}

	if($result){
		$sql = "SELECT ware_storesrequesitionheader.int100ExceedingApproved, ware_storesrequesitionheader.int100ExceedingApproveLevels FROM ware_storesrequesitionheader WHERE (intRequisitionNo='$SRNNo') AND (`intRequisitionYear`='$SRNYear')";
		$results = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($results);
		$status = $row['int100ExceedingApproved'];
		$savedLevels = $row['int100ExceedingApproveLevels'];
		
		$approveLevel=$savedLevels+1-$status;
		
		$sqlI = "INSERT INTO `ware_srn_100_exceeding_approval` (`intRequisitionNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
		VALUES ('$SRNNo','$SRNYear','$approveLevel','$userId',now(),0)";
		$resultI = $db->RunQuery2($sqlI);
		if((!$resultI) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sqlI;
			$rollBackMsg = "Approval error!";
		}
	}
	
	
	//-------------------
	if($rollBackFlag==0){
		$db->RunQuery2('Commit');
		$response['type'] 		= 'pass';
		$response['msg'] 		= "Qty Approved sucessfully";
		$response['q'] 			= '';
	}
	else if($rollBackFlag==1){
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $rollBackMsg;
		$response['q'] 			= $sqlM;
	}
	else{
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $db->errormsg;
		$response['q'] 			= $sqlM;
	}
	$db->CloseConnection();	
	
	echo json_encode($response);
		
}
else if($_REQUEST['status']=='approveQty_excess')
{
	$db->OpenConnection();
	$db->RunQuery2('Begin');
	
	$rollBackFlag=0;
	$rollBackMsg = '';

	$sql = "UPDATE `ware_storesrequesitionheader` SET `intExcessApproved`=`intExcessApproved`-1 WHERE (intRequisitionNo='$SRNNo') AND (`intRequisitionYear`='$SRNYear') and intStatus not in(0,1)";
	$result = $db->RunQuery2($sql);
	if((!$result) && ($rollBackFlag!=1)){
		$rollBackFlag=1;
		$sqlM=$sql;
		$rollBackMsg = "Approval error!";
	}
	else{
		if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
		//sendRejecedEmailToUser($SRNNo,$SRNYear,$objMail,$mainPath,$root_path);
		}
	}

	if($result){
		$sql = "SELECT ware_storesrequesitionheader.intExcessApproved, ware_storesrequesitionheader.intExcessApproveLevels FROM ware_storesrequesitionheader WHERE (intRequisitionNo='$SRNNo') AND (`intRequisitionYear`='$SRNYear')";
		$results = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($results);
		$status = $row['intExcessApproved'];
		$savedLevels = $row['intExcessApproveLevels'];
		
		$approveLevel=$savedLevels+1-$status;
		
		$sqlI = "INSERT INTO `ware_srn_excess_approval` (`intRequisitionNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
		VALUES ('$SRNNo','$SRNYear','$approveLevel','$userId',now(),0)";
		$resultI = $db->RunQuery2($sqlI);
		if((!$resultI) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sqlI;
			$rollBackMsg = "Approval error!";
		}
	}
	
	
	//-------------------
	if($rollBackFlag==0){
		$db->RunQuery2('Commit');
		$response['type'] 		= 'pass';
		$response['msg'] 		= "Qty Approved sucessfully";
		$response['q'] 			= '';
	}
	else if($rollBackFlag==1){
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $rollBackMsg;
		$response['q'] 			= $sqlM;
	}
	else{
		$db->RunQuery2('Rollback');
		$response['type'] 		= 'fail';
		$response['msg'] 		= $db->errormsg;
		$response['q'] 			= $sqlM;
	}
	$db->CloseConnection();	
	
	echo json_encode($response);
		
}
	else if($_REQUEST['status']=='rejectQty_25')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		$rollBackFlag=0;
		$rollBackMsg = '';
		
		
 		$sql = "SELECT ware_storesrequesitionheader.int25ExceedingApproved, ware_storesrequesitionheader.int25ExceedingApproveLevels FROM ware_storesrequesitionheader WHERE (intRequisitionNo='$SRNNo') AND (`intRequisitionYear`='$SRNYear')";
		$results = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($results);
		$createdUser = $row['intUser'];
		$status = $row['int25ExceedingApproved'];
		$savedLevels = $row['int25ExceedingApproveLevels'];
	
	
		$sql = "UPDATE `ware_storesrequesitionheader` SET `int25ExceedingApproved`=0 WHERE (intRequisitionNo='$SRNNo') AND (`intRequisitionYear`='$SRNYear')";
		$result = $db->RunQuery2($sql);
		if((!$result) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sql;
			$rollBackMsg = "Rejection error!";
		}
		else{
			if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
			//sendRejecedEmailToUser($SRNNo,$SRNYear,$objMail,$mainPath,$root_path);
			}
		}
 		$sqlI = "INSERT INTO `ware_srn_25_exceeding_approval` (`intRequisitionNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
			VALUES ('$SRNNo','$SRNYear','0','$userId',now(),0)";
		$resultI = $db->RunQuery2($sqlI);
		if((!$resultI) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sqlI;
			$rollBackMsg = "Rejection error!";
		}
		
		//-------------------
		if($rollBackFlag==0){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= "Qty Rejected sucessfully";
			$response['q'] 			= '';
		}
		else if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= $sqlM;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sqlM;
		}
		$db->CloseConnection();		
		
		echo json_encode($response);
			
	}
	else if($_REQUEST['status']=='rejectQty_100')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		$rollBackFlag=0;
		$rollBackMsg = '';
		
		
 		$sql = "SELECT ware_storesrequesitionheader.int100ExceedingApproved, ware_storesrequesitionheader.int100ExceedingApproveLevels FROM ware_storesrequesitionheader WHERE (intRequisitionNo='$SRNNo') AND (`intRequisitionYear`='$SRNYear')";
		$results = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($results);
		$createdUser = $row['intUser'];
		$status = $row['int100ExceedingApproved'];
		$savedLevels = $row['int100ExceedingApproveLevels'];
	
	
		$sql = "UPDATE `ware_storesrequesitionheader` SET `int100ExceedingApproved`=0 WHERE (intRequisitionNo='$SRNNo') AND (`intRequisitionYear`='$SRNYear')";
		$result = $db->RunQuery2($sql);
		if((!$result) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sql;
			$rollBackMsg = "Rejection error!";
		}
		else{
			if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
			//sendRejecedEmailToUser($SRNNo,$SRNYear,$objMail,$mainPath,$root_path);
			}
		}
 		$sqlI = "INSERT INTO `ware_srn_100_exceeding_approval` (`intRequisitionNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
			VALUES ('$SRNNo','$SRNYear','0','$userId',now(),0)";
		$resultI = $db->RunQuery2($sqlI);
		if((!$resultI) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sqlI;
			$rollBackMsg = "Rejection error!";
		}
		
		//-------------------
		if($rollBackFlag==0){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= "Qty Rejected sucessfully";
			$response['q'] 			= '';
		}
		else if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= $sqlM;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sqlM;
		}
		$db->CloseConnection();		
		
		echo json_encode($response);
			
	}
	else if($_REQUEST['status']=='rejectQty_excess')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		$rollBackFlag=0;
		$rollBackMsg = '';
		
		
 		$sql = "SELECT ware_storesrequesitionheader.intExcessApproved, ware_storesrequesitionheader.intExcessApproveLevels FROM ware_storesrequesitionheader WHERE (intRequisitionNo='$SRNNo') AND (`intRequisitionYear`='$SRNYear')";
		$results = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($results);
		$createdUser = $row['intUser'];
		$status = $row['intExcessApproved'];
		$savedLevels = $row['intExcessApproveLevels'];
	
	
		$sql = "UPDATE `ware_storesrequesitionheader` SET `intExcessApproved`=0 WHERE (intRequisitionNo='$SRNNo') AND (`intRequisitionYear`='$SRNYear')";
		$result = $db->RunQuery2($sql);
		if((!$result) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sql;
			$rollBackMsg = "Rejection error!";
		}
		else{
			if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
			//sendRejecedEmailToUser($SRNNo,$SRNYear,$objMail,$mainPath,$root_path);
			}
		}
 		$sqlI = "INSERT INTO `ware_srn_excess_approval` (`intRequisitionNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
			VALUES ('$SRNNo','$SRNYear','0','$userId',now(),0)";
		$resultI = $db->RunQuery2($sqlI);
		if((!$resultI) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sqlI;
			$rollBackMsg = "Rejection error!";
		}
		
		//-------------------
		if($rollBackFlag==0){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= "Qty Rejected sucessfully";
			$response['q'] 			= '';
		}
		else if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= $sqlM;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sqlM;
		}
		$db->CloseConnection();		
		
		echo json_encode($response);
			
	}


?>
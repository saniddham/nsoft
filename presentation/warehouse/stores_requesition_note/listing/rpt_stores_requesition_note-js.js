// JavaScript Document
$(document).ready(function() {
	
	$("#frmSRNReport").validationEngine();
	$('#frmSRNReport #imgApprove').live('click',urlApprove);
	$('#frmSRNReport #imgReject').live('click',urlReject);
	
	$('#frmSRNR_approval_report #imgApprove').click(function(){
		approveQty();
	});
	$('#frmSRNR_approval_report #imgReject').click(function(){
		rejectQty();
	});
	$('#frmSRNReport #imgAppQty100').click(function(){
		approveQty100();
	});
	$('#frmSRNReport #imgRejQty100').click(function(){
		rejectQty100();
	});
	$('#frmSRNReport #imgAppQtyExcess').click(function(){
		approveQtyExcess();
	});
	$('#frmSRNReport #imgRejQtyExcess').click(function(){
		rejectQtyExcess();
	});
	
});


function urlApprove()
{
	var val = $.prompt('Are you sure you want to approve this Requesition ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url = "rpt_stores_requesition_note-db-set.php"+window.location.search+'&status=approve';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmSRNReport #imgApprove').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmSRNReport #imgApprove').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
								return;
							}		
						});
					hideWaiting();
					}
				
			}});
}
function urlReject()
{
	var val = $.prompt('Are you sure you want to reject this Requesition ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url = "rpt_stores_requesition_note-db-set.php"+window.location.search+'&status=reject';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmSRNReport #imgReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx1()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmSRNReport #imgReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx1()",3000);
								return;
							}		
						});
					hideWaiting();
					}
				
			}});
}
function approveQty25(){
	
	var val = $.prompt('Are you sure you want to approve this SRN Qtys ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					//if(validateQuantities()==0){
					///////////
					var url = "rpt_stores_requesition_note-db-set.php"+window.location.search+'&status=approveQty_25';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmSRNReport #imgAppQty25').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmSRNReport #imgAppQty25').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						//}
 					   hideWaiting();
					}
				}});
	
}
function approveQty100(){
	
	var val = $.prompt('Are you sure you want to approve this SRN Qtys ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					//if(validateQuantities()==0){
					///////////
					var url = "rpt_stores_requesition_note-db-set.php"+window.location.search+'&status=approveQty_100';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmSRNReport #imgAppQty100').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmSRNReport #imgAppQty100').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						//}
 					   hideWaiting();
					}
				}});
	
}
function approveQtyExcess(){
	
	var val = $.prompt('Are you sure you want to approve this SRN Qtys ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
					showWaiting();
					//if(validateQuantities()==0){
					///////////
					var url = "rpt_stores_requesition_note-db-set.php"+window.location.search+'&status=approveQty_excess';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmSRNReport #imgAppQtyExcess').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmSRNReport #imgAppQtyExcess').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						//}
 					   hideWaiting();
					}
				}});
	
}
function rejectQty25(){

	var val = $.prompt('Are you sure you want to reject this SRN Qtys ?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{
					showWaiting();
					//if(validateRejecton()==0){
					///////////
					var url = "rpt_stores_requesition_note-db-set.php"+window.location.search+'&status=rejectQty_25';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmSRNReport #imgRejQty25').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmSRNReport #imgRejQty25').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						//}
						hideWaiting();
			}
		}});
	
}
function rejectQty100(){

	var val = $.prompt('Are you sure you want to reject this SRN Qtys ?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{
					showWaiting();
					//if(validateRejecton()==0){
					///////////
					var url = "rpt_stores_requesition_note-db-set.php"+window.location.search+'&status=rejectQty_100';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmSRNReport #imgRejQty100').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmSRNReport #imgRejQty100').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						//}
						hideWaiting();
			}
		}});
	
}
function rejectQtyExcess(){

	var val = $.prompt('Are you sure you want to reject this SRN Qtys ?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{
					showWaiting();
					//if(validateRejecton()==0){
					///////////
					var url = "rpt_stores_requesition_note-db-set.php"+window.location.search+'&status=rejectQty_excess';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmSRNReport #imgRejQtyExcess').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmSRNReport #imgRejQtyExcess').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						//}
						hideWaiting();
			}
		}});
	
}


function approveQty()
{
	var val = $.prompt('Are you sure you want to approve this Qtys ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url = "rpt_stores_requesition_note-db-set.php"+window.location.search+'&status=approveQtys';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmSRNR_approval_report #imgApprove').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmSRNR_approval_report #imgApprove').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
								return;
							}		
						});
					hideWaiting();
					}
				
			}});
}
function rejectQty()
{
	var val = $.prompt('Are you sure you want to reject this Qtys ?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
				if(v)
				{
 					showWaiting();
					var url = "rpt_stores_requesition_note-db-set.php"+window.location.search+'&status=rejectQtys';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmSRNR_approval_report #imgReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx1()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmSRNR_approval_report #imgReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx1()",3000);
								return;
							}		
						});
					hideWaiting();
					}
				
			}});
}





function alertx()
{
	$('#frmSRNReport #imgApprove').validationEngine('hide')	;
}
function alertx1()
{
	$('#frmSRNReport #imgReject').validationEngine('hide')	;
}
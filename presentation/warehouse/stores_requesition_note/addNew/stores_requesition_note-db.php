<?php
	//ini_set('display_errors',1); 
	session_start();
	$backwardseperator	=	"../../../../";
	$mainPath		=	$_SESSION['mainPath'];
	$userId			=	$_SESSION['userId'];
	$location		=	$_SESSION['CompanyID'];
	$company		=	$_SESSION['headCompanyId'];
	$pgrmCode		=	'P0256';
	
	include "{$backwardseperator}dataAccess/Connector.php";
	require_once "{$backwardseperator}libraries/mail/mail.php";
	require_once $_SESSION['ROOT_PATH']."class/cls_mail.php";
	require_once $_SESSION['ROOT_PATH']."class/warehouse/stores_requesition_note/cls_stores_requesition_note_get.php";
	require_once $_SESSION['ROOT_PATH']."class/warehouse/cls_warehouse_get.php";
	require_once $_SESSION['ROOT_PATH']."class/warehouse/stores_requesition_note/cls_stores_requesition_note_set.php";
	require_once $_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
	require_once $_SESSION['ROOT_PATH']."class/cls_commonErrorHandeling_get.php";
	require_once $_SESSION['ROOT_PATH']."class/cls_permisions.php";
	
	$obj_st_req_get	= new cls_stores_requesition_note_get($db);
	$obj_st_req_set	= new cls_stores_requesition_note_set($db);
	$obj_common		= new cls_commonFunctions_get($db);
	$obj_commonErr	= new cls_commonErrorHandeling_get($db);
	$objMail 		= new cls_create_mail($db);
	$obj_permision	= new cls_permisions($db);
	$obj_warehouse_get		= new cls_warehouse_get($db);
	
//---------------------------
 	$row						= $obj_permision->get_auto_allocation_permision($company);
	$allocateFlag			  	= $row['AUTO_ALLOCATION_PERMISION'];
	$fullAllocateFlag		  	= $row['FULL_AUTO_ALLOCATION_PERMISION'];
	
	$row						= $obj_common->get_auto_allocation_percentages($company,'RunQuery');
	$sampAlloPercentage  	  	= $row['SAMPLE_ALLOCATION_%'];
	$fullAlloPercentage  	  	= $row['FULL_ALLOCATION_%'];
	$extraAlloPercentageFoil  	= $row['FOIL_EXTRA_ALLOCATION_%'];
	$extraAlloPercentageSpecial = $row['SP_RM_EXTRA_ALLOCATION_%'];
	$extraAlloPercentageInkItm  = $row['INK_ITEM_EXTRA_ALLOCATION_%'];
	
	$percentage_foil_ful	= $fullAlloPercentage+$fullAlloPercentage*$extraAlloPercentageFoil/100;
	$percentage_sp_rm_full	= $fullAlloPercentage+$fullAlloPercentage*$extraAlloPercentageSpecial/100;
	$percentage_ink_full	= $fullAlloPercentage+$fullAlloPercentage*$extraAlloPercentageInkItm/100;
	$percentage_foil		= $sampAlloPercentage+$sampAlloPercentage*$extraAlloPercentageFoil/100;
	$percentage_sp_rm		= $sampAlloPercentage+$sampAlloPercentage*$extraAlloPercentageSpecial/100;
	$percentage_ink			= $sampAlloPercentage+$sampAlloPercentage*$extraAlloPercentageInkItm/100;
	$arr_perc['foil']['25']		= $percentage_foil;
	$arr_perc['foil']['100']	= $percentage_foil_ful;
	$arr_perc['special']['25']	= $percentage_sp_rm;
	$arr_perc['special']['100']	= $percentage_sp_rm_full;
	$arr_perc['ink']['25']		= $percentage_ink;
	$arr_perc['ink']['100']		= $percentage_ink_full;
  //----------------------------
	
	$response		=	array('type'=>'', 'msg'=>'');
	
 	$requestType 	=	$_REQUEST['requestType'];
  	$arrHeader 		=	$_REQUEST['arrHeader'];
	$arrDetails		=	$_REQUEST['arrDetails'];
 
	$programName		= 'Stores Requisition Note';
	$programCode		= 'P0256';
	$approveLevel 		= (int)getApproveLevel($programName);

	if($requestType=='loadGraphics'){
		$arrHeader 				=	$_REQUEST['arrHeader'];
		$arrHeader 				=	json_decode($arrHeader,true);
 		$year	 				=	$arrHeader['year'];

 		$optionss				=	$obj_st_req_get->get_graphics_options($year,'','RunQuery');
		$response['optionss']	=	$optionss;
	
	}
	else if($requestType=='loadItems')
	{
		$arrHeader 				=	$_REQUEST['arrHeader'];
		$arrHeader 				=	json_decode($arrHeader,true);
		$year	 				=	$arrHeader['year'];
		$graphics 				=	$obj_common->replace($arrHeader['graphics']);
 		
		$result		=	$obj_st_req_get->get_grapic_order_color_items($year,$graphics,'RunQuery');
 		while($row	=	mysqli_fetch_array($result))
		{
 			$transferedQty			= 0;
			$data['itemId'] 		= $row['intId'];
			$data['itemType'] 		= $row['itemType'];
			$data['maincatId'] 		= $row['intMainCategory'];
			$data['subCatId'] 		= $row['intSubCategory'];
			$data['code'] 			= $row['strCode'];
			$data['itemName'] 		= $row['itemName'];
			$data['uom'] 			= $row['uom'];
			$data['unitPrice'] 		= round($row['dblUnitPrice'],4);
			$data['mainCatName'] 	= $row['mainCatName'];
			$data['subCatName'] 	= $row['subCatName'];
			
			$max_normal_qty			= $obj_st_req_get->get_graphic_wise_ink_item_maximum_srn_qty($company,$year,$graphics,$row['intId'],$obj_warehouse_get,$obj_common,$row['itemType'],$arr_perc,'RunQuery');		
			$data['max_qty']		= $obj_common->ceil_to_decimal_places(($max_normal_qty),2);
			
			$arrCombo[] = $data;
		}
		$response['arrCombo'] 	= $arrCombo;

	}
	else if($requestType	==	'save')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
	
		$arrHeader 				=	json_decode($arrHeader,true);
		$arrDetails 			=	json_decode($arrDetails,true);
 		
  		$serialNo 				=	$arrHeader['serialNo'];
  		$year	 				=	$arrHeader['year'];
 		$srnFrom 				=	$arrHeader['srnFrom'];
		$srnTo		 			=	$arrHeader['srnTo'];
		$graphics 				=	$obj_common->replace($arrHeader['graphics']);
 		$orderYear	 			=	$arrHeader['orderYear'];
		$date 					=	$arrHeader['date'];
		
 		
		$savableFlag	=	1;
	    $rollBackFlag	=	0;
		
		if($serialNo	==	''){
			$editMode		=	0;
			$mode			=	'INSERT';
			$createdDate	=	date('Y-m-d h:m:s');
			$createdDate	=	"'".$createdDate."'";
			$creator		=	$userId;
			$status			=	$approveLevel+1;
			$levels			=	$approveLevel;
		}
		else{
			$editMode		=	1;
			$mode			=	'UPDATE';
 			$row 			=	$obj_st_req_get->other_stores_requisition_header_select($serialNo,$year,'RunQuery2');
			$status			=	$row['intStatus'];
			$levels			=	$row['intApproveLevels'];
			$creator		=	$row['intUser'];
			$createdDate	=	$row['dtmCreateDate'];
			$createdDate	=	"'$createdDate'";
		}
 
 		$editPermition=$obj_commonErr->get_permision_withApproval_save($status,$levels,$userId,$pgrmCode,'RunQuery2');
		
		if(($editPermition['type']	==	'false')){
			$rollBackMsg	=	$editPermition['msg'];
			$rollBackFlag	=	1;
		}
 		if($rollBackFlag	==	0){
			if($editMode	==	1){
 				$resp 	= $obj_st_req_set->other_stores_requisition_header_update($serialNo,$year,$status,$levels,$srnFrom,$srnTo,$orderYear,$graphics,$date,$userId,$location,'RunQuery2');
 				if($resp['type']=='fail'){
					$rollBackFlag	=	1;
					$rollBackSql	= $resp['sql'];
				}
			}
			else if($rollBackFlag	==	0){
				$data['arrData'] = $obj_common->GetSystemMaxNo('intSRNNo',$location);
				if($data['arrData']['rollBackFlag']==1)
				{
					$rollBackFlag		= 1;
					$rollBackMsg		= $data['arrData']['msg'];
					$rollBackSql		= $data['sql'];

				}
				else
				{
					$rollBackFlag	= 0;
					$serialNo		= $data['arrData']['max_no'];
					$year			= date('Y');
				
					//echo $serialNo.','.$year.','.$status.','.$levels.','.$srnFrom.','.$srnTo.','.$orders.','.$date.','.$userId.','.$location;
					$resp		= $obj_st_req_set->other_stores_requisition_header_insert($serialNo,$year,$status,$levels,$srnFrom,$srnTo,$orderYear,$graphics,$date,$userId,$location,'RunQuery2');
					if($resp['type']=='fail'){
						$rollBackFlag	=	1;
						$rollBackSql	=	$resp['sql'];
					}
				}
			}
 			
			if(($rollBackFlag==1)){
				$rollBackFlag	=	1;
				$rollBackMsg	=	$resp['msg'];
				$sqlE			=	$resp['sql'];
			}
 
		}
 		if(($rollBackFlag !=	1)){
 			$saved		= 0;
			$toSave		= 0;
			$i			= 0;
			$totExQty	= 0;
 
			$result		= $obj_st_req_set->other_stores_requisition_details_delete($serialNo,$year,'RunQuery2');
			$result		= $obj_st_req_set->other_stores_requisition_details_sales_order_wise_delete($serialNo,$year,'RunQuery2');
			
			foreach($arrDetails as $arrVal)
			{
				if($rollBackFlag	!=	1){

					$i++;
					$item				= $arrVal['itemId'];
					$data				= $obj_warehouse_get->get_item_name($item,'RunQuery2');
					$itemName			= $data['strName'];
					$qty				= $obj_common->ceil_to_decimal_places($arrVal['qty'],2);
					$extQty				= $obj_common->ceil_to_decimal_places($arrVal['extraQty'],2);
					$totExQty 			+=$extQty;
					$toSave +=2;
			
					$exceed_qty_25	=0;
					$exceed_qty_100	=0;
					$result_s	= $obj_warehouse_get->get_sales_orders_for_graphic($orderYear,$graphics,$item,'RunQuery2');
					while($row_s=mysqli_fetch_array($result_s)){//sales order wise
					  
						$orderNo			= $row_s['intOrderNo'];
						$orderYear			= $row_s['intOrderYear'];	
						$salesOrder			= $row_s['intSalesOrderId'];
						$qty_25				=0;
						$qty_100			=0;
						$toSave++;
						
						$prapotion			= $obj_warehouse_get->get_sales_order_bal_to_production_order_qty_prapotion($orderNo,$orderYear,$salesOrder,$graphics,$item,$arr_perc['ink']['100'],$arr_perc['ink']['25'],'RunQuery2');
						
									//echo "|".$item."-".$obj_common->ceil_to_decimal_places($prapotion,2);
						//echo "|".$qty."-".$prapotion;
						
						$to_srn_qty			= $obj_common->ceil_to_decimal_places($qty*$prapotion,2);
				
						$day_cons_ink_itm	= $obj_warehouse_get->get_cumulateve_ink_item_consumption($orderNo,$orderYear,$salesOrder,$item,'RunQuery2');
						$samp_conPC			= $obj_warehouse_get->get_order_item_sample_consumption($orderNo,$orderYear,$salesOrder,$item,'RunQuery2');
						if($day_cons_ink_itm > 0){
							$details_100	= $obj_warehouse_get->maximum_order_item_allocatable_details($orderNo,$orderYear,$salesOrder,$item,'1',$arr_perc['foil']['100'],$arr_perc['ink']['100'],$arr_perc['special']['100'],'RunQuery2');
							$max_to_srn_100	= ($obj_common->ceil_to_decimal_places($details_100['tot_qty_for_allocate'],2)) - ($obj_common->ceil_to_decimal_places($details_100['color_room_stock_bal'],2));
							$max_to_srn_100			= $obj_common->ceil_to_decimal_places($max_to_srn_100,2);
							if($to_srn_qty > $max_to_srn_100){
								$flag_100		= 1;
								$qty_100		=$to_srn_qty - $max_to_srn_100;
								$exceed_qty_100	+= $to_srn_qty - $max_to_srn_100;
							}
						}
						else{
							$details_25		= $obj_warehouse_get->maximum_order_item_allocatable_details($orderNo,$orderYear,$salesOrder,$item,'1',$arr_perc['foil']['25'],$arr_perc['ink']['25'],$arr_perc['special']['25'],'RunQuery2');
							$max_to_srn_25	= ($obj_common->ceil_to_decimal_places($details_25['tot_qty_for_allocate'],2)) - ($obj_common->ceil_to_decimal_places($details_25['color_room_stock_bal'],2));
							$max_to_srn_25			= $obj_common->ceil_to_decimal_places($max_to_srn_25,2);
							if($to_srn_qty > $max_to_srn_25){
								$flag_25		= 1;
								$to_srn_qty.' - '.$max_to_srn_25;
								$qty_25			=$to_srn_qty - $max_to_srn_25;
								$exceed_qty_25	+= $to_srn_qty - $max_to_srn_25;
							}
						}
						//--------------S/O wise insert
						$resp	= NULL;
						$resp	=	$obj_st_req_set->other_stores_requisition_details_sales_order_wise_insert($serialNo,$year,$orderNo,$orderYear,$salesOrder,$item,$to_srn_qty,$qty_25,$qty_100,0,'RunQuery2');
						//print_r($resp);
						if($resp['type']=='fail'){
							$rollBackFlag	=	1;
							$rollBackMsg	=	$resp['msg'];
							$rollBackSql	=	$resp['sql'];
						}
						else{
						$saved++;
						}
						
						
					}// end of sales order wise
					
					if($exceed_qty_25 >0 ){
						$maxQty_msg_25 .= $itemName.'=>'.$obj_common->ceil_to_decimal_places($exceed_qty_25,2)."</br>";
					}
					if($exceed_qty_100 >0 ){
						$maxQty_msg_100 .= $itemName.'=>'.$obj_common->ceil_to_decimal_places($exceed_qty_100,2)."</br>";
					}
					if($extQty >0 ){
						$maxQty_msg_ex .= $itemName.'=>'.$obj_common->ceil_to_decimal_places($extQty,2)."</br>";
					}
 		
					//-----------------S/O wise 
					$resp	= NULL;
					$resp	=	$obj_st_req_set->other_stores_requisition_details_insert($serialNo,$year,$item,$qty,$exceed_qty_25,$exceed_qty_100,$extQty,'RunQuery2');
					//print_r($resp);
					if($resp['type']=='fail'){
						$rollBackFlag	=	1;
						$rollBackMsg	=	$resp['msg'];
						$rollBackSql	=	$resp['sql'];
					}
					else{
					$saved ++;
					}
					//--------item wise excess------
					$resp	= NULL;
					$resp	=	$obj_st_req_set->other_stores_requisition_details_sales_order_wise_insert($serialNo,$year,0,0,0,$item,0,0,0,$extQty,'RunQuery2');
					//print_r($resp);
					if($resp['type']=='fail'){
						$rollBackFlag	=	1;
						$rollBackMsg	=	$resp['msg'];
						$rollBackSql	=	$resp['sql'];
					}
					else{
					$saved++;
					}
					
					
					
				}
			} 
		}
		
		if(($rollBackFlag !=1) && ($toSave	==	$saved)){
 			$sql = "UPDATE `ware_storesrequesitionheader` SET int25ExceedingApproved =NULL ,int25ExceedingApproveLevels =NULL,
					int100ExceedingApproved =NULL ,int100ExceedingApproveLevels = NULL
					WHERE (`intRequisitionNo`='$serialNo') AND (`intRequisitionYear`='$year')";
			$result1 = $db->RunQuery2($sql);
 			
				if($flag_25>0){ //if exceeding 25% qty only with samp conPC
					$appMsg ='Exceeds 25% SRN Qty :'.$maxQty_msg_25.'</br>'./*'This has been sent to approve'.*/'</br>';
					//send_25_ExceedingSRNForApproval($serialNo,$year,$obj_permision,$objMail,$mainPath,$root_path);
					//$appLevels	=(int)$obj_common->get_special_approve_levels('Permission to approve 25% Exceeded SRN','RunQuery2');	
					$appLevels	= 1;
					$exStatus		=$appLevels+1;
				
					$sql = "UPDATE `ware_storesrequesitionheader` SET int25ExceedingApproved ='$exStatus' ,int25ExceedingApproveLevels ='$appLevels'
							WHERE (`intRequisitionNo`='$serialNo') AND (`intRequisitionYear`='$year')";
					$result1 = $db->RunQuery2($sql);
					if(!$result1){
						$rollBackFlag=1;
						$rollBackMsg ="Header Saving Error";
						$rollBackSql =$sql;
					}
					if($rollBackFlag != 1){
 						$maxAppByStatus=(int)$obj_st_req_get->get_25_MaxAppByStatus($serialNo,$year,'RunQuery2')+1;
						$sql = "UPDATE `ware_srn_25_exceeding_approval` SET intStatus ='$maxAppByStatus' 
								WHERE (`intRequisitionNo`='$serialNo') AND (`intYear`='$year') AND (`intStatus`='0')";
						$result1 = $db->RunQuery2($sql);
						if((!$result1) && $editMode==1){
							$rollBackFlag=1;
							$rollBackMsg ="Header Saving Error";
							$rollBackSql =$sql;
						}
					}
				}
				else if($flag_100>0){//if exceeding 100% qty with cumulative conPC
					$appMsg ='Actual Consumptions not raised.Exceeds 100% SRN Qty :'.$maxQty_msg_100.'</br>'./*'This has been sent to approve'.*/'</br>';
					//send_100_ExceedingSRNForApproval($serialNo,$year,$obj_permision,$objMail,$mainPath,$root_path);
					//$appLevels	=(int)$obj_common->get_special_approve_levels('Permision to approve 100% Exceeded SRN','RunQuery2');	
					$appLevels	= 1;
					$exStatus		=$appLevels+1;
				
					$sql = "UPDATE `ware_storesrequesitionheader` SET int100ExceedingApproved ='$exStatus' ,int100ExceedingApproveLevels ='$appLevels'
							WHERE (`intRequisitionNo`='$serialNo') AND (`intRequisitionYear`='$year')";
					$result1 = $db->RunQuery2($sql);
					if(!$result1){
						$rollBackFlag=1;
						$rollBackMsg ="Header Saving Error";
						$rollBackSql =$sql;
					}
					if($rollBackFlag != 1){
						//inactive previous recordrs in approvedby table
						$maxAppByStatus=(int)$obj_st_req_get->get_100_MaxAppByStatus($serialNo,$year,'RunQuery2')+1;
						$sql = "UPDATE `ware_srn_100_exceeding_approval` SET intStatus ='$maxAppByStatus' 
								WHERE (`intRequisitionNo`='$serialNo') AND (`intYear`='$year') AND (`intStatus`='0')";
						$result1 = $db->RunQuery2($sql);
						if((!$result1) && $editMode==1){
							$rollBackFlag=1;
							$rollBackMsg ="Header Saving Error";
							$rollBackSql =$sql;
						}
					}
				}
				if($totExQty > 0){////////
					$appMsg .='Excess Qty exists for </br>'.$maxQty_msg_ex./*'This has been sent to approve'.*/'</br>';
					//send_Extra_SRNForApproval($serialNo,$year,$obj_permision,$objMail,$mainPath,$root_path);
					//$appLevels	=(int)$obj_common->get_special_approve_levels('Permision to approve Extra SRN','RunQuery2');	
					$appLevels	= 1;
					$exStatus		=$appLevels+1;
				
					$sql = "UPDATE `ware_storesrequesitionheader` SET intExcessApproved ='$exStatus' ,intExcessApproveLevels ='$appLevels'
							WHERE (`intRequisitionNo`='$serialNo') AND (`intRequisitionYear`='$year')";
					$result1 = $db->RunQuery2($sql);
					if(!$result1){
						$rollBackFlag=1;
						$rollBackMsg ="Header Saving Error";
						$rollBackSql =$sql;
					}

					if($rollBackFlag != 1){
						//inactive previous recordrs in approvedby table
						$maxAppByStatus=(int)$obj_st_req_get->get_excess_MaxAppByStatus($serialNo,$year,'RunQuery2')+1;
						$sql = "UPDATE `ware_srn_excess_approval` SET intStatus ='$maxAppByStatus' 
								WHERE (`intRequisitionNo`='$serialNo') AND (`intYear`='$year') AND (`intStatus`='0')";
						$result1 = $db->RunQuery2($sql);
						if((!$result1) && $editMode==1){
							$rollBackFlag=1;
							$rollBackMsg ="Header Saving Error";
							$rollBackSql =$sql;
						}
					}
				}
			}
		
		
 		
	
		if($rollBackFlag	==	1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= 'Error with saving Data '.$rollBackMsg;
			$response['q'] 			= $rollBackSql;
		}
		else if($i	==	0){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= 'Error with saving Data';
			$response['q'] 			= $sql;
		}
		else if(($result) && ($toSave	==	$saved)){
			
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			if($editMode	==	1)
			$response['msg'] 		= 'Updated successfully.'.'</br>'.$appMsg;
			else
			$response['msg'] 		= 'Saved successfully.'.'</br>'.$appMsg;
			
			$response['serialNo'] 	= $serialNo;
			$response['year'] 		= $year;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sqlE;
		}
	
	$db->CloseConnection();
 		
 	}
 	else if($requestType	==	'loadApproval'){
			$serialNo		=   $_REQUEST['serialNo'];
			$year			=	$_REQUEST['year'];
 			$row 			=	$obj_st_req_get->other_stores_requisition_header_select($serialNo,$year,'RunQuery');
			$status			=	$row['intStatus'];
			$levels			=	$row['intApproveLevels'];
			$creator		=	$row['intUser'];
			$createdDate	=	$row['dtmCreateDate'];
			$createdDate	=	"'$createdDate'";
			$response			=	$obj_commonErr->get_permision_withApproval_save($status,$levels,$userId,$pgrmCode,'RunQuery');
 	}
	
	
	
 	echo json_encode($response);

//--------------------------------------------------------
function send_100_ExceedingSRNForApproval($serialNo,$year,$obj_permision,$objMail,$mainPath,$root_path){
	global $db;
 				
 		 	$row	= $obj_permision->get_special_permision('57','RunQuery2');
 			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="SRN ('$serialNo'/'$year') - (100% EXCEEDING SRN QUANTITIES)"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST = NULL;
			
			$_REQUEST['program']='SRN';
			$_REQUEST['field1']='SRN No';
			$_REQUEST['field2']='SRN Year';
			$_REQUEST['field3']='';
			$_REQUEST['field4']='';
			$_REQUEST['field5']='';
			$_REQUEST['value1']=$serialNo;
			$_REQUEST['value2']=$year;
			$_REQUEST['value3']='';
			$_REQUEST['value4']='';
			$_REQUEST['value5']='';
			
			$_REQUEST['subject']="SRN - (100% EXCEEDING SRN QUANTITIES) ('$serialNo'/'$year')";
			
			$_REQUEST['statement1']="Approve Quantities - ";
			$_REQUEST['statement2']="to view this";
			$_REQUEST['link']=base64_encode($mainPath."presentation/warehouse/stores_requesition_note/listing/rpt_stores_requesition_note.php?SRNNo=$serialNo&SRNYear=$year&approveMode_exceeding=0"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}
//--------------------------------------------------------
function send_25_ExceedingSRNForApproval($serialNo,$year,$obj_permision,$objMail,$mainPath,$root_path){
	global $db;
 				
 		 	$row	= $obj_permision->get_special_permision('56','RunQuery2');
 			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="SRN ('$serialNo'/'$year') - (25% EXCEEDING SRN QUANTITIES)"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST = NULL;
			
			$_REQUEST['program']='SRN';
			$_REQUEST['field1']='SRN No';
			$_REQUEST['field2']='SRN Year';
			$_REQUEST['field3']='';
			$_REQUEST['field4']='';
			$_REQUEST['field5']='';
			$_REQUEST['value1']=$serialNo;
			$_REQUEST['value2']=$year;
			$_REQUEST['value3']='';
			$_REQUEST['value4']='';
			$_REQUEST['value5']='';
			
			$_REQUEST['subject']="SRN - (25% EXCEEDING SRN QUANTITIES) ('$serialNo'/'$year')";
			
			$_REQUEST['statement1']="Approve Quantities - ";
			$_REQUEST['statement2']="to view this";
			$_REQUEST['link']=base64_encode($mainPath."presentation/warehouse/stores_requesition_note/listing/rpt_stores_requesition_note.php?SRNNo=$serialNo&SRNYear=$year&approveMode_exceeding=0"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}
//--------------------------------------------------------
function send_Extra_SRNForApproval($serialNo,$year,$obj_permision,$objMail,$mainPath,$root_path){
	global $db;
 				
 		 	$row	= $obj_permision->get_special_permision('58','RunQuery2');
 			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="SRN ('$serialNo'/'$year') - (SRN EXCESS QTY)"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST = NULL;
			
			$_REQUEST['program']='SRN';
			$_REQUEST['field1']='SRN No';
			$_REQUEST['field2']='SRN Year';
			$_REQUEST['field3']='';
			$_REQUEST['field4']='';
			$_REQUEST['field5']='';
			$_REQUEST['value1']=$serialNo;
			$_REQUEST['value2']=$year;
			$_REQUEST['value3']='';
			$_REQUEST['value4']='';
			$_REQUEST['value5']='';
			
			$_REQUEST['subject']="SRN - (EXCESS QTY) ('$serialNo'/'$year')";
			
			$_REQUEST['statement1']="Approve Quantities - ";
			$_REQUEST['statement2']="to view this";
			$_REQUEST['link']=base64_encode($mainPath."presentation/warehouse/stores_requesition_note/listing/rpt_stores_requesition_note.php?SRNNo=$serialNo&SRNYear=$year&approveMode_exceeding=0"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}
//--------------------------------------------------------
	
  ?>
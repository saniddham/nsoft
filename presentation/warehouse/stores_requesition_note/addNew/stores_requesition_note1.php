<?php
	//ini_set('display_errors',1);
	session_start();
	$backwardseperator = "../../../../";
	$thisFilePath 	=  $_SERVER['PHP_SELF'];
	$locationId 	= $_SESSION['CompanyID'];
	$intUser  		= $_SESSION["userId"];

	//include  		"{$backwardseperator}dataAccess/permisionCheck.inc";
	include_once  		"{$backwardseperator}dataAccess/connector.php";
	require_once $_SESSION['ROOT_PATH']."class/warehouse/stores_requesition_note/cls_stores_requesition_note_get.php";
  	$obj_st_req_get	= new cls_stores_requesition_note_get($db);
	
	$programName	= 'Stores Requisition Note';
	$programCode	= 'P0256';

	$serialDisplay	= $_REQUEST['serialNo'];
	$serialNo 		= substr($_REQUEST['serialNo'],3,4);
	$curr_date		= date('Y-m-d');

	//$row=$objmemget->formHeader($serialNo);
 	
	//$editMode=$objmemget->loadEditMode($programCode,$intUser,$serialNo,$createdBy);
	//$deleteMode=$objmemget->loadDeleteMode($programCode,$intUser);

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Meeting Minutes</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/button.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $backwardseperator; ?>css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">
<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="stores_requesition_note-js.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>
<!--<script type="application/javascript" src="../../../../libraries/jquery/multi_select_combo.js"></script>
--><link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
</head>

<body>
<table width="100%" border="0" align="center">
  <tr>
    <td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
  </tr>
</table>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script> 
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>
<link rel="stylesheet" href="../../../../libraries/chosen/chosen/chosen.css" />

<!--<script type="text/javascript" src="../../../../libraries/jquery/jquery-1.8.3-min.js"></script>
--> 
<script src="../../../../libraries/chosen/chosen/prototype.js" type="text/javascript"></script> 
<script src="../../../../libraries/chosen/chosen/chosen.proto.js" type="text/javascript"></script> 
<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script> 
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script> 
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script> 
<script>
jQuery.noConflict();
</script>
<form id="frmStoresRequisition" name="frmStoresRequisition" autocomplete="off" action="stores_requesition_note.php" method="post">
  <div align="center">
    <div class="trans_layoutXL" style="width:950px"  align="center">
      <div class="trans_text">Stores Requisition Note</div>
      <table width="900" border="0" class="">
        <tr>
          <td  class="normalfnt" colspan="3" align="center"><fieldset class="tableBorder_allRound">
            <table width="100%" border="0" class="">
              <tr>
         <td colspan="5"><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="11%" height="22" class="normalfnt">SRN No</td>
            <td width="24%"><input name="txtSRNNo" type="text" disabled="disabled" class="txtText" id="txtSRNNo" style="width:60px" value="<?php echo $SRNNo; ?>" />&nbsp;<input name="txtSRNYear" type="text" disabled="disabled" class="txtText" id="txtSRNYear" style="width:40px" value="<?php echo $SRNYear; ?>" /></td>
            <td width="7%">&nbsp;</td>
            <td width="39%">&nbsp;</td>
            <td width="5%" class="normalfnt">Date</td>
            <td width="14%"><input name="dtDate" type="text" disabled="disabled" class="txtNumber" id="dtDate" style="width:100px" value="<?php echo(($dtDate)?$dtDate:date('Y-m-d')); ?>" /></td>
          </tr>
          <tr>
            <td height="22" class="normalfnt">SRN From</td>
            <td><select style="width:200px" name="cboSRNFrom" id="cboSRNFrom" class="validate[required]">
			<?php //$sql = $objmemget->userList(); ?>
            <?php
				$sql = "SELECT intId,strName
						FROM mst_substores
						WHERE intParentId=0 AND
						intMainStoresType=0 AND
						intLocation='$locationId' AND
						intStatus=1
						ORDER BY strName";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($cboSRNFrom==$row['intId'])
						echo "<option value=\"".$row["intId"]."\" selected=\"selected\" >".$row["strName"]."</option>";
					else
						echo "<option value=\"".$row["intId"]."\" >".$row["strName"]."</option>";
				}
			?>
            </select></td>
            <td>SRN To</td>
            <td><select style="width:200px" name="cboSRNTo" id="cboSRNTo" class="validate[required]">
              <?php
				$sql = "SELECT intId,strName
						FROM mst_substores
						WHERE intParentId=0 AND
						intMainStoresType=1 AND
						intLocation='$locationId' AND
						intStatus=1
						ORDER BY strName";
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
					if($cboSRNTo==$row['intId'])
						echo "<option value=\"".$row["intId"]."\" selected=\"selected\" >".$row["strName"]."</option>";
					else
						echo "<option value=\"".$row["intId"]."\" >".$row["strName"]."</option>";
				}
			?>
            </select></td>
            <td class="normalfnt">&nbsp;</td>
            <td>&nbsp;</td>
          </tr>
        </table></td>
      </tr>  
              <tr>
                <td width="11%" class="normalfnt">Graphic Nos</td>
                <td colspan="3" ><select name="select" multiple="multiple" class="txtCalled normalfnt graphics" style="width:650px; height:50px" tabindex="4" data-placeholder="">
                  <?php
					$options = $obj_st_req_get->get_graphics_options('RunQuery');
					//echo $options;
				?>
                </select></td>
                <td width="15%" >
                <a id="butSearchOrders" class="button white medium"   name="butSearchOrders"  > Search </a>
                </td>
              </tr>
            
<tr>
                <td class="normalfnt">Order Nos</td>
                <td colspan="3" ><select name="select" multiple="multiple" class="txtAttendees normalfnt orders" style="width:650px; height:50px" tabindex="4" data-placeholder="">
                  <?php
				//	$sql = $objmemget->userList();
			
		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
				$selected=0;
  				foreach($attendeesArr as $x)
				{
					if($x==$row['intUserId']){
					  $selected=1;	
					}
				}
					if($selected==1){
 						$html .= "<option value=\"".$row['intUserId']."\" selected=\"selected\">".$row['strFullName']."</option>";
					}
					else{
						$html .= "<option value=\"".$row['intUserId']."\">".$row['strFullName']."</option>";
					}
 		}
		echo $html;
						?>
                  </select></td>
                <td>
                <a id="butSearchItems" class="button white medium"   name="butSearchItems"  > Search </a>
                </td>
              </tr>            
              <tr>
                <td colspan="5" class="normalfnt"><div style="width:900px;height:300px;overflow:scroll" >
          <table width="100%" class="bordered" id="tblSRNItem" >
            <tr>
              <th width="8%" height="22" >Del</th>
              <th width="17%" >Main Category</th>
              <th width="17%" >Sub Category</th>
              <th width="31%" >Item Description</th>
              <th width="13%">Qty</th>
              <th width="14%"> Stock Balance</th>
              </tr>
            <?php
			if($SRNNo!="" && $SRNYear!="")
			{
				$sql = "SELECT SRD.intRequisitionNo,SRD.intRequisitionYear,SRD.intItemId,MI.strName AS itemName,
						MC.strName AS mainCatName,MC.intId AS mainCatId,SC.strName AS subCatName,SC.intId AS subCatId,SRD.dblQty,
						IFNULL((SELECT SUM(dblQty) FROM ware_sub_stocktransactions_bulk 
						WHERE intLocationId='$locationId' AND intSubStores='$cboSRNFrom' AND intItemId=MI.intId),0) AS stockBlance
						FROM ware_storesrequesitiondetails SRD
						INNER JOIN mst_item MI ON MI.intId=SRD.intItemId
						INNER JOIN mst_maincategory MC ON MC.intId=MI.intMainCategory
						INNER JOIN mst_subcategory SC ON SC.intId=MI.intSubCategory
						WHERE SRD.intRequisitionNo='$SRNNo' AND
						SRD.intRequisitionYear='$SRNYear' ";
				
				$result = $db->RunQuery($sql);
				while($row=mysqli_fetch_array($result))
				{
				?>
                	<tr class="normalfnt">
                    <td align="center" bgcolor="#FFFFFF"><img class="delImg" src="../../../../../images/del.png" width="15" height="15" /></td>
                    <td align="center" bgcolor="#FFFFFF" id="<?php echo $row['mainCatId']; ?>" class="clsMainCat"><?php echo $row['mainCatName']; ?></td>
                    <td align="center" bgcolor="#FFFFFF" id="<?php echo $row['subCatId']; ?>" class="clsSubCat"><?php echo $row['subCatName']; ?></td>
                    <td align="center" bgcolor="#FFFFFF" id="<?php echo $row['intItemId']; ?>" class="clsItem"><?php echo $row['itemName']; ?></td>
                    <td align="center" bgcolor="#FFFFFF" id=""><input  id="txtQty" name="txtQty" class="clsQty" style="width:60px;text-align:right" type="text" value="<?php echo $row['dblQty']; ?>"/></td>
                    <td align="center" bgcolor="#FFFFFF" class="clsStockBal" ><?php echo $row['stockBlance']; ?></td>
                    </tr>
                <?php
				}
			}
			?>
            </table>
          </div></td>
                </tr>
              
              </table>
          </fieldset></td>
        </tr>
        <tr>
          <td colspan="4" align="center">
                  </td>
        </tr>
        <tr>
          <td height="34" colspan="4"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
            <tr>
              <td width="100%" align="center" bgcolor=""><a id="butNew"  class="button white medium" style="" name="butAddNewTask"> New </a>
                <?php if($editMode==1){ ?>
                <a id="butSave" class="button white medium" style="" name="butSave"> Save </a>
                <?php } ?>
                <a id="butReport" class="button white medium" <?php if($serialNo==''){ ?>style="display:none"<?php } ?> name="butReport"  > Report </a>
                <a id="butClose" class="button white medium" name="butClose" href="../../../../main.php">Close</a></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td colspan="4" class="normalfnt"><?php
		$sql = $objmemget->userList();
			
 					$result = $db->RunQuery($sql);
					$id="'";
					while($row=mysqli_fetch_array($result))
					{		
							$id=$row['intUserId'];
							$name=$row['strFullName'];
						
							$names .=  $row['strFullName'].",";
							$ids .=  $row['intUserId'].",";
 							 $str .=" { 
							 id: '$id',
								userName: '$name'
							},";
					}
				$names = substr($names, 0, -1);	
				$ids = substr($ids, 0, -1);	
				$str = substr($str, 0, -1);	
 				?>
              
   
	<script type="text/javascript">
             </script>
  
             <?php
		$sqlu = $objmemget->userList();
			
		$html = "<option value=\"\"></option>";
		$resultu = $db->RunQuery($sqlu);
		while($rowu=mysqli_fetch_array($resultu))
		{
				$html .= "<option value=\"".$rowu['intUserId']."\">".$rowu['strFullName']."</option>";
		}
		  $html;
?>
            <div style="display:none" id="divSuggestNames"><?php echo $names; ?></div>
            <div style="display:none" id="divSuggestIds"><?php echo $ids; ?></div>
            <div style="display:none" id="divSuggestStr"><?php echo $html; ?></div></td>
        </tr>
      </table>
    </div>
  </div>
</form>
<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1"> 
  <!-- <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php" style="width:800;height:800;border:0;overflow:hidden">
    </iframe>--> 
</div>
<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
</body>
</html>

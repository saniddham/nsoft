var para;
var objName; 
// JavaScript Document
 
jQuery(document).ready(function() {
	
  	jQuery("#frmStoresRequisition").validationEngine();
 
	loadMultipleattrbutes('');
   //-------------------------------------------- 
 	jQuery('#frmStoresRequisition .graphics').live('change',load_items);
 //-------------------------------------------- 
 	jQuery('#frmStoresRequisition #butSearchItems').live('click',load_items);
 //-------------------------------------------- 
 	jQuery('#frmStoresRequisition .delImg').live('click',delete_items);
 //-------------------------------------------- 
 	jQuery('#frmStoresRequisition #cboOrderYear').live('change',load_graphics);
 //-------------------------------------------- 
 	jQuery('#frmStoresRequisition #butNew').live('click',load_new);
 //-------------------------------------------- 

jQuery('#butSave').live('click',function(){
 		showWaiting_c();
		
		if(!validateData())
			return;

		var data = "requestType=save";
 		var arrHeader = "{";
				
					arrHeader += '"serialNo":"'+ jQuery('#txtSRNNo').val()+'",' ;
					arrHeader += '"year":"'+ jQuery('#txtSRNYear').val()+'",' ;
					arrHeader += '"srnFrom":"'+ jQuery('#cboSRNFrom').val()+'",' ;
					arrHeader += '"srnTo":"'+ jQuery('#cboSRNTo').val()+'",' ;
					arrHeader += '"graphics":"'+ (jQuery('.graphics').val())+'",' ;
					arrHeader += '"orderYear":"'+ (jQuery('#cboOrderYear').val())+'",' ;
					arrHeader += '"date":"'+ jQuery('.srnDate').val() +'"' ;
					
		arrHeader += ' }';
		
		var i=0;
		var rcds=0;
		var errorDescFlag=0;
		var errorActionByFlag=0;
		var arrDetails="";
		
		
		jQuery('.item').each(function(){

			i++;
			var itemId 				=	jQuery(this).parent().find('.item').attr('id');
			var qty					=	jQuery(this).parent().find('.Qty').val();
			var extraQty	 		=	jQuery(this).parent().find('.exQty').val();
  
			if((qty > 0))
			{
				rcds++;
				arrDetails += "{";
							arrDetails += '"itemId":"'+ itemId +'",' ;
							arrDetails += '"qty":"'+ qty +'",' ;
							arrDetails += '"extraQty":"'+ extraQty +'"' ;
							arrDetails +=  '},';
			}
		})
		
		if(rcds==0){
			alert("There is no items to save");
			hideWaiting_c();
			return false;
		}
		
		arrDetails = arrDetails.substr(0,arrDetails.length-1);
		
		var arrHeader=arrHeader;
		var arrDetails='['+arrDetails+']';
 		data+="&arrHeader="	+	arrHeader+"&arrDetails="	+	arrDetails;
 		
		var url = "stores_requesition_note-db.php";
		jQuery.ajax({
				url:url,
				async:false,
				dataType:'json',
				type:'post',
				data:data,
		success:function(json){
				jQuery('#frmStoresRequisition #butSave').validationEngine('showPrompt', json.msg,json.type );
				if(json.type=='pass')
				{
					jQuery('#txtSRNNo').val(json.serialNo);
					jQuery('#txtSRNYear').val(json.year);
 					showConfirmButton();
					var t=setTimeout("alertx()",99000);
					hideWaiting_c();
					jQuery("#butReport").show();  
					return;
				}
				hideWaiting_c();
			},
		error:function(xhr,status){
				jQuery('#frmStoresRequisition #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
				var t=setTimeout("alertx()",5000);
				hideWaiting_c();
				return;
			}
			
		});

 	});

  //-----------------------------------
jQuery('#butReport').live('click',function(){	

/*	if(jQuery('#txtSerialNo').val()!=''){
		window.open('../listing/rptMeetingMinutes.php?serialNo='+jQuery('#txtSerialNo').val());	
	}
	else{
		alert("There is no Meeting Minute to view");
	}
*/
	var SRNNo 		= jQuery('#txtSRNNo').val();
	var SRNYear  	= jQuery('#txtSRNYear').val();
	var url  = "../listing/rpt_stores_requesition_note.php?SRNNo="+SRNNo;
	    url += "&SRNYear="+SRNYear;
	    url += "&mode=view";
 		window.open('../listing/rpt_stores_requesition_note.php?SRNNo='+jQuery('#txtSRNNo').val()+"&SRNYear="+SRNYear+"&mode=view");	
});
//----------------------------------	

  //-----------------------------------
jQuery('#butConfirm').live('click',function(){	

/*	if(jQuery('#txtSerialNo').val()!=''){
		window.open('../listing/rptMeetingMinutes.php?serialNo='+jQuery('#txtSerialNo').val());	
	}
	else{
		alert("There is no Meeting Minute to view");
	}
*/
	var SRNNo 		= jQuery('#txtSRNNo').val();
	var SRNYear  	= jQuery('#txtSRNYear').val();
	var url  = "../listing/rpt_stores_requesition_note.php?SRNNo="+SRNNo;
	    url += "&SRNYear="+SRNYear;
	    url += "&mode=view";
 		window.open('../listing/rpt_stores_requesition_note.php?SRNNo='+jQuery('#txtSRNNo').val()+"&SRNYear="+SRNYear+"&approveMode=1");	
});
//----------------------------------	

});//end of ready

function alertx()
{
	jQuery('#frmStoresRequisition #butSave').validationEngine('hide')	;
	jQuery('#frmStoresRequisition #butComplete').validationEngine('hide')	;
	jQuery('#frmStoresRequisition #butDelete').validationEngine('hide')	;
	
}
   
//------------------------------------------------------------------------------
function showWaiting_c()
{
		var popupbox = document.createElement("div");
		var windowWidth = document.documentElement.clientWidth;
		var windowHeight = document.documentElement.clientHeight;
		var scrollH = (document.body.scrollHeight);
		//alert(windowHeight);
   popupbox.id = "divBackGroundBalck";
   popupbox.style.position = 'absolute';
   popupbox.style.zIndex = 100;
   popupbox.style.textAlign = 'center';
   popupbox.style.left = 0 + 'px';
   popupbox.style.top = 0 + 'px'; 
   popupbox.style.background="#000000"; 
   popupbox.style.width = screen.width + 'px';
   popupbox.style.height =  (scrollH)+ 'px';
   popupbox.style.opacity = 0.5;
   popupbox.style.color = "#FFFFFF";
	document.body.appendChild(popupbox);
	//document.getElementById('divBackGroundBalck').innerHTML = "this is text code";
	var popupbox1 = document.createElement("div");
	 popupbox1.id = "divBackgroundImg";
   popupbox1.style.position = 'absolute';
   popupbox1.style.zIndex = 101;
   popupbox1.style.verticalAlign = 'center';
   popupbox1.style.left =  windowWidth/2-100/2 +'px';
   popupbox1.style.top = (jQuery(window).scrollTop()+200) + 'px'; 
   popupbox1.style.width = '100px';
   popupbox1.style.height =  '100px';
   popupbox1.style.opacity = 1;
   popupbox1.style.color = "#FFFFFF";
	document.body.appendChild(popupbox1);
	
	//alert(mainPath);
	document.getElementById('divBackgroundImg').innerHTML = "<img src=\""+projectName+"/images/loading_go.gif\" /><span class=\"normalfnt\" style=\"color:white;\">Please Wait...</span>";
	
}

function hideWaiting_c()
{
	try
	{
		var box = document.getElementById('divBackGroundBalck');
		box.parentNode.removeChild(box);
		
		var box1 = document.getElementById('divBackgroundImg');
		box1.parentNode.removeChild(box1);
		
	}
	catch(err)
	{        
	}	
}

function validateData()	
{
  	if((jQuery('.graphics').val()=='')){
			alert("Please Select 'Graphic'");
			jQuery('.graphics').focus();
			hideWaiting_c();
			return false;
		}
 		var i=0;
		jQuery('.item').each(function(){
 			var itemId 		=	jQuery(this).attr('id');
 			if((itemId!=''))
			{
				i++;
			}
		});
		
		if(i==0){
			alert("There is no items to save");
			hideWaiting_c();
			return false;
		}
		
	
	return true;
}

function disableFields(obje){
}
  
function quote(string)
{
	string = string.replace(/'/gi,"\\'");
	string = string.replace(/"/gi,'\\"');
	return string;	
}

 
function load_graphics(){

 		jQuery('#frmStoresRequisition #grap').html('');
		
		var data	  = '';	
  		var arrHeader = "{";
 					arrHeader += '"year":"'+ (jQuery('#cboOrderYear').val())+'"' ;
 		arrHeader += ' }';
		var data = "arrHeader="+arrHeader;
		
 		var url = "stores_requesition_note-db.php?requestType=loadGraphics";
		jQuery.ajax({
				url:url,
				async:false,
				dataType:'json',
				type:'post',
				data:data,
		success:function(json){
			jQuery('#frmStoresRequisition #grap').html('<select name="select" multiple="multiple" class="txtCalled normalfnt multiSelect graphics" style="width:650px; height:50px" tabindex="4" data-placeholder=""></select>');
 			jQuery('#frmStoresRequisition .graphics').html(json.optionss)
			jQuery('#frmStoresRequisition .graphics').val('')
		}
		});
		
 		loadMultipleattrbutes('graphics');
	
 }
 
function load_items(){

 		showWaiting_c();
		delete_all_items();
 		
		var data	  = '';	
  		var arrHeader = "{";
 					arrHeader += '"year":"'+ (jQuery('#cboOrderYear').val())+'",' ;
					arrHeader += '"graphics":'+ URLEncode_json(jQuery('.graphics').val());
		arrHeader += ' }';
		var data = "arrHeader="+arrHeader;
		
 		var url = "stores_requesition_note-db.php?requestType=loadItems";
		jQuery.ajax({
				url:url,
				async:false,
				dataType:'json',
				type:'post',
				data:data,
				success:function(json){
 
								var length 		= json.arrCombo.length;
 								var arrCombo 	= json.arrCombo;


								for(var i=0;i<length;i++)
								{
									var itemId			=arrCombo[i]['itemId'];	
									var itemName		=arrCombo[i]['itemName'];	
									var maincatId		=arrCombo[i]['maincatId'];	
									var mainCatName		=arrCombo[i]['mainCatName'];
									var subCatId		=arrCombo[i]['subCatId'];	
									var subCatName		=arrCombo[i]['subCatName'];	
									var code			=arrCombo[i]['code'];	
									var uom				=arrCombo[i]['uom'];	
									var max_qty			=arrCombo[i]['max_qty'];
									if(max_qty < 0){
										max_qty	= 0;
									}
 					
					
									var content='<tr class="normalfnt"><td align="center"  bgcolor="#FFFFFF"><img class="delImg" src="../../../../images/del.png" width="15" height="15" /></td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'" class="mainCat">'+mainCatName+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'" class="subCat">'+subCatName+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="item">'+itemName+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+uom+'" class="uom">'+uom+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id=""><input style="text-align:right; width:100%" id="'+max_qty+'" class="validate[required,custom[number],min[0]] calculateValue Qty" style="width:50px;text-align:right" type="text" value="'+max_qty+'"/></td>';
									content +='<td align="center" bgcolor="#FFFFFF" id=""><input style="text-align:right; width:100%" id="'+max_qty+'" class="validate[required,custom[number],min[0]] calculateValue exQty" style="width:50px;text-align:right" type="text" value="'+0+'"/></td></tr>';
									
  									add_new_row('#frmStoresRequisition #tblSRNItem',content);
								}
									//checkAlreadySelected();
	
		}
		});
 		hideWaiting_c();
}

function loadMultipleattrbutes(field){
	
  	//-------------------------
     var select, selects, _i, _len, _results;
    if (Prototype.Browser.IE && (Prototype.BrowserFeatures['Version'] === 6 || Prototype.BrowserFeatures['Version'] === 7)) {
      return;
    }
	
	if(field!=''){
		var a1="."+field;
		var a2="."+field+"-deselect";
		//alert(a1);
	}
	else{
		var a1=".multiSelect";
		var a2=".multiSelect-deselect";
		//alert(a1);
	}

	
    selects = $$(a1);
    _results = [];
    for (_i = 0, _len = selects.length; _i < _len; _i++) {
      select = selects[_i];
      _results.push(new Chosen(select));
    }
    deselects = $$(a2);
    for (_i = 0, _len = deselects.length; _i < _len; _i++) {
      select = deselects[_i];
      _results.push(new Chosen(select,{allow_single_deselect:true}));
    }
   // return _results;	
	//-----------------------------------------------------------------------------
	
}
//----------------------------------------------
function add_new_row(table,rowcontent){
        if (jQuery(table).length>0){
            if (jQuery(table+' > tbody').length==0) jQuery(table).append('<tbody />');
            (jQuery(table+' > tr').length>0)?jQuery(table).children('tbody:last').children('tr:last').append(rowcontent):jQuery(table).children('tbody:last').append(rowcontent);
        }
    }
//----------------------------------------------- 
function delete_items(){
 			jQuery(this).parent().parent().remove();
}

function showConfirmButton(){

 		
 		var url = "stores_requesition_note-db.php?requestType=loadApproval";
		jQuery.ajax({
				url:url,
				async:false,
				dataType:'json',
				type:'post',
				data:'serialNo='+jQuery('#txtSRNNo').val()+'&year='+jQuery('#txtSRNYear').val(),
		success:function(json){
 			if(json.permision==1)
				jQuery('#frmStoresRequisition #butConfirm').show();
			else
				jQuery('#frmStoresRequisition #butConfirm').hide();
 		}
		});
	
}

function approve(){
   	
	var SRNNo 		= jQuery('#txtSRNNo').val();
	var SRNYear  	= jQuery('#txtSRNYear').val();
	var url  = "../listing/rpt_stores_requesition_note.php?SRNNo="+SRNNo;
	    url += "&SRNYear="+SRNYear;
	    url += "&approveMode=1";
 	window.open(url,'../listing/rpt_stores_requesition_note.php');
}

function view(){
	var SRNNo 		= jQuery('#txtSRNNo').val();
	var SRNYear  	= jQuery('#txtSRNYear').val();
	var url  = "../listing/rpt_stores_requesition_note.php?SRNNo="+SRNNo;
	    url += "&SRNYear="+SRNYear;
	    url += "&mode=view";
	window.open(url,'../listing/rpt_stores_requesition_note.php');
}

function delete_all_items(){

	var rowCount = document.getElementById('tblSRNItem').rows.length;
	//alert(rowCount);
	//var cellCount = document.getElementById('tblMain').rows[1].cells.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblSRNItem').deleteRow(1);
			
	}
	
}

function load_new(){
		window.location.href = "stores_requesition_note.php";
}

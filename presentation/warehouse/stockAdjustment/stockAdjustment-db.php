<?php
	session_start();
	$backwardseperator = "../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	$requestType 	= $_REQUEST['requestType'];
	
	require_once $backwardseperator."dataAccess/Connector.php";

	require_once $backwardseperator."class/warehouse/stockAdjustment/cls_stockAdjustment_get.php";
	require_once $backwardseperator."class/warehouse/cls_warehouse_set.php";
	require_once $backwardseperator."class/warehouse/cls_warehouse_get.php";
    require_once $_SESSION['ROOT_PATH']."class/common/azureDBconnection.php";
	
	$objstockadjget= new cls_stockAdjustment_get($db);
	$objwhouseget= new cls_warehouse_get($db);
	$objwhouseset= new cls_warehouse_set($db);
    $objAzure = new cls_azureDBconnection();
	
	
	$db->OpenConnection();
	$db->RunQuery2('Begin');
	if($requestType=='loadMainCategories')
	{ 
		$response['arrCombo'] 	= $objwhouseget->getMainCategory_options();
		echo json_encode($response);
	}
	else if($requestType=='selectMainCategory')
	{
		$response['mainCat'] 	= $objwhouseget->getMainCategoryId($_REQUEST['subCat']);
		echo json_encode($response);
	}
	else if($requestType=='loadSubCategories')
	{
		$response['arrCombo'] 	= $objwhouseget->getSubCategory_options($_REQUEST['mainCat']);
		echo json_encode($response);
	}
	else if($requestType=='loadItems')
	{
		$response['arrCombo'] 	= $objwhouseget->getItem_options($_REQUEST['subCat']);
		echo json_encode($response);
	}
	else if($requestType=='loadGridData')
	{
		$response['arrData'] 	= $objstockadjget->getDetails($location,$_REQUEST['subCat'],$_REQUEST['itemId']);
		echo json_encode($response);
	}
	else if($requestType=='save')//save
	{
		$arr 	 = json_decode($_REQUEST['arr'], true);
		
			$toSave=0;
			$saved=0;
			$rollBackFlag=0;
			$rollBackMsg =''; 
			$sqlString='';
			$dataArray = array();

			$documentNo = getNextDocumentNo($db,$location);
			$docString = $documentNo.'/'.date('Y');
			
			foreach($arr as $arrVal)
			{
				$location 	= $_SESSION['CompanyID'];
				$company 	= $_SESSION['headCompanyId'];
				$userId 	= $_SESSION['userId'];
				$itemId 	 = $arrVal['itemId'];
				$grnNo 	 = $arrVal['grnNo'];
				$grnNoArray   = explode('/',$grnNo);
				$grnNo 	 = $grnNoArray[0];
				$grnYear 	 = $grnNoArray[1];
				$grnDate 	 = $arrVal['grnDate'];
				$oldRate 	 = $arrVal['oldRate'];
				$newRate 	 = $arrVal['newRate'];
				$currency 	 = $arrVal['currency'];
				$operationType 	 = $arrVal['operationType'];
				if($operationType=='-1'){
					$newRate=0;	
				}
				if($newRate==''){
					$newRate=0;	
				}
				$Qty 	 = $arrVal['Qty'];
				if($Qty==''){
					$Qty=0;	
				}
			 	
			   $stockBalQty=$objwhouseget->getStockBalance_bulk($location,$itemId,$grnNo,$grnYear,$grnDate,$oldRate);			
			  // echo $stockBalQty;
			
			   //10-04-2013--------
			   $stockBalQty = round($stockBalQty,4);
			   $stockBalQty = round($Qty,4);
			   //------------------
				
				if($operationType==1){//add to stock
					if(($Qty!=0) && ($newRate!=0) && ($newRate!=$oldRate)){//change both rate and qty
						$saveQty=$stockBalQty;
						$rate=$oldRate;
                        $oldValue = (-1)* $rate *$saveQty;
						if($saveQty>0){
						$toSave++;
						$response['arrData'] = $objwhouseset->insert_ware_stocktransactions_bulk($company,$location,$grnNo,$grnYear,$grnNo,$grnYear,$grnDate,$rate,$currency,$itemId,-$saveQty,'Adjust RQ+',$userId);
						$saved+=$response['arrData']['result'];
						$sqlString.=$response['arrData']['q'];
						}
						
						$saveQty=$stockBalQty+$Qty;
						$rate=$newRate;
						$newValue=$rate*$saveQty;
						if($saveQty>0){
						$toSave++;
						$response['arrData'] = $objwhouseset->insert_ware_stocktransactions_bulk($company,$location,$grnNo,$grnYear,$grnNo,$grnYear,$grnDate,$rate,$currency,$itemId,$saveQty,'Adjust RQ+',$userId);
						$saved+=$response['arrData']['result'];
						$sqlString.=$response['arrData']['q'];
						}
						$value = $newValue + $oldValue;
						if($value >0) {
                            array_push($dataArray, storeValues($docString, $value, $grnNo, $grnYear, $rate, $location, $itemId));
                        }
                        else if($value <0) {
                            array_push($dataArray, storeValues($docString, -$value, $grnNo, $grnYear, $rate, $location, $itemId));
                        }
					}
					else if(($newRate!=0) && ($Qty==0) && ($stockBalQty>0)){//change only rate
						$toSave++;
						$saveQty=$stockBalQty;
						$rate=$oldRate;
						$oldValue = (-1) * $rate * $saveQty;
						$response['arrData'] = $objwhouseset->insert_ware_stocktransactions_bulk($company,$location,$grnNo,$grnYear,$grnNo,$grnYear,$grnDate,$rate,$currency,$itemId,-$saveQty,'Adjust R+',$userId);
						$saved+=$response['arrData']['result'];
						$sqlString.=$response['arrData']['q'];
				
						$toSave++;
						$saveQty=$stockBalQty;
						$rate=$newRate;
						$newValue = $rate * $saveQty;
						$response['arrData'] = $objwhouseset->insert_ware_stocktransactions_bulk($company,$location,$grnNo,$grnYear,$grnNo,$grnYear,$grnDate,$rate,$currency,$itemId,$saveQty,'Adjust R+',$userId);
						$saved+=$response['arrData']['result'];
						$sqlString.=$response['arrData']['q'];
                        $value = $newValue + $oldValue;
                        if($value >0) {
                            array_push($dataArray, storeValues($docString, $value, $grnNo, $grnYear, $rate, $location, $itemId));
                        }
                        else if($value <0){
                            array_push($dataArray, storeValues($docString, -$value, $grnNo, $grnYear, $rate, $location, $itemId));
                        }
					}
					else if(($newRate==0) && ($Qty!=0)){//change only qty
						$saveQty=$Qty;
						$rate=$oldRate;
						if($saveQty>0){
						$toSave++;
						$response['arrData'] = $objwhouseset->insert_ware_stocktransactions_bulk($company,$location,$grnNo,$grnYear,$grnNo,$grnYear,$grnDate,$rate,$currency,$itemId,$saveQty,'Adjust Q+',$userId);
						$saved+=$response['arrData']['result'];
						$sqlString.=$response['arrData']['q'];
						}
                        $value = $rate * $saveQty;
						if($value > 0) {
                            array_push($dataArray, storeValues($docString, $value, $grnNo, $grnYear, $rate, $location, $itemId));
                        }
					}
				}
				else{ //substract from stock
					if($stockBalQty>=$Qty){//change only qty
						$toSave++;
						$saveQty=$Qty;
						$rate=$oldRate;
						$response['arrData'] = $objwhouseset->insert_ware_stocktransactions_bulk($company,$location,$grnNo,$grnYear,$grnNo,$grnYear,$grnDate,$rate,$currency,$itemId,-$saveQty,'Adjust Q-',$userId);
						$saved+=$response['arrData']['result'];
						$sqlString.=$response['arrData']['q'];
                        $value = $rate * $saveQty;
                        if($value > 0) {
                            array_push($dataArray, storeValues($docString, -$value, $grnNo, $grnYear, $rate, $location, $itemId));
                        }
					}
					else{
						$rollBackFlag++;
						$rollBackMsg.='Stock Qty is less than the Adjust Qty';
					}
				}
				
		
			}//end of foreach

			if($rollBackFlag==1){
				$db->RunQuery2('Rollback');
				$response['type'] 		= 'fail';
				$response['msg'] 		= $rollBackMsg;
				$response['q'] 			= '';
			}			
			else if(($toSave==$saved)){
			    if($company == '1'){
			        updateAzureDB($db,$dataArray,$location);
                }
				$db->RunQuery2('Commit');
				$response['type'] 		= 'pass';
				$response['msg'] 		= 'Saved successfully.';
			}
			else{
				$db->RunQuery2('Rollback');
				$response['type'] 		= 'fail';
				$response['msg'] 		= $db->errormsg;
				$response['q'] 			= $sqlString;
			}
			echo  json_encode($response);
	}

	function storeValues($documentNo,$value,$grnNo,$grnYear,$rate,$location,$itemId){
	    $data = array(
	        'document_no'=> $documentNo,
            'value'=>$value,
            'rate'=>$rate,
            'grnNo'=>$grnNo,
            'grnYear'=>$grnYear,
            'location'=>$location,
            'itemId'=>$itemId
        );
	    return $data;
    }


	function updateAzureDB($db,$dataArray,$company){

        global $objAzure;
        $environment = $objAzure->getEnvironment();
        $table = 'Inventory'.$environment;

        $azure_connection = $objAzure->connectAzureDB();
        $line_no = rand(100,1000);
        for($i=0;$i<count($dataArray);$i++) {
            $record = $dataArray[$i];
            $grnNo  = $record['grnNo'];
            $grnYear = $record['grnYear'];
            $itemId = $record['itemId'];
            $line_no++;
            $location = $record['location'];
            $value = $record['value'];
            $docNo = $record['document_no'];
            $postingDate = date('Y-m-d H:i:s');

            $sql_grn = "SELECT dblExchangeRate FROM `ware_grnheader` WHERE `intGrnNo` = '$grnNo' AND `intGrnYear` = '$grnYear'";

            $result = $db->RunQuery2($sql_grn);
            $row = mysqli_fetch_array($result);
            $exchangeRate = $row['dblExchangeRate'];

            $sql_item = "SELECT
                        mst_maincategory.strName AS mainCategory,
                        mst_subcategory.strName AS subCategory,
                        mst_maincategory.intSendDetails AS sendMainCat,
                        mst_subcategory.intSendDetails AS sendSubCat,
                        mst_item.strCode AS item_code,
                        mst_item.strName AS description
                    FROM
                        mst_item
                    INNER JOIN mst_maincategory ON mst_maincategory.intId = mst_item.intMainCategory
                    INNER JOIN mst_subcategory ON mst_subcategory.intId = mst_item.intSubCategory
                    WHERE
                        mst_item.intId = '$itemId'";
            $result_item = $db->RunQuery2($sql_item);
            $row_item = mysqli_fetch_array($result_item);

            $item_category = $row_item['mainCategory'];
            $item_subcategory = $row_item['subCategory'];
            $item_code = $row_item['item_code'];
            $description = str_replace("'","''",$row_item['description']);
            $amount = round($value * $exchangeRate, 2);
            $grn_no = $grnNo . '/' . $grnYear;
            $successHeader = '0';
            $sendDetails = ($row_item['sendMainCat'] == 1 && $row_item['sendSubCat'] == 1)? true:false;

            if($sendDetails) {
                $sql_azure_header = "INSERT INTO $table (Document_Category, Document_No, Line_No,  External_Doc_No, Posting_Date, Item_Category, Item_Sub_Category, Item_Code, Description, Amount, Location) VALUES ('StockAdjustment', '$docNo','$line_no','$grn_no', '$postingDate', '$item_category', '$item_subcategory', '$item_code', '$description', '$amount','$location')";
                if ($azure_connection) {
                    $getResultsHeader = $objAzure->runQuery($azure_connection, $sql_azure_header);
                    if ($getResultsHeader != FALSE) {
                        $successHeader = '1';
                    }
                }
                $sql_db_header = "INSERT INTO trn_financemodule_inventory (Document_Category, Document_No, Line_No, External_Doc_No, Posting_Date, Item_Category, Item_Sub_Category, Item_Code, Description, Amount, Location, deliveryStatus, deliveryDate) VALUES ('StockAdjustment','$docNo', '$line_no', '$grn_no', '$postingDate', '$item_category', '$item_subcategory', '$item_code', '$description', '$amount','$location','$successHeader', NOW())";
                $result_db_header = $db->RunQuery2($sql_db_header);
            }
        }

    }

    function getNextDocumentNo($db,$companyId)
    {

        $sql_select = "SELECT
                    sys_no.STOCK_ADJUSTMENT_NO
                    FROM sys_no
                    WHERE
                    sys_no.intCompanyId =  '$companyId'";

        $result = $db->RunQuery2($sql_select);
        $row = mysqli_fetch_array($result);

        $nextIssueNo = $row['STOCK_ADJUSTMENT_NO'];

        if($nextIssueNo == 0){
            $nextIssueNo = $companyId.'00000';
        }

        $sql = "UPDATE `sys_no` SET STOCK_ADJUSTMENT_NO=$nextIssueNo+1 WHERE (`intCompanyId`='$companyId')  ";
        $db->RunQuery2($sql);

        return $nextIssueNo;
    }

    function getNextSysNo($db,$companyId, $transaction_type){

        $current_serial_no = $companyId.'000000';
        $sql = "SELECT `$transaction_type` FROM `trn_financemodule_sysNo` WHERE `COMPANY_ID` = '$companyId'";
        $result = $db->RunQuery2($sql);
        $row = mysqli_fetch_array($result);
        if($row[$transaction_type] != NULL || $row[$transaction_type] != ''){
            $current_serial_no = $row[$transaction_type];
        }
        //update next serial no
        $sql_update = "UPDATE `trn_financemodule_sysNo` SET `$transaction_type`='$current_serial_no'+1 WHERE `COMPANY_ID`='$companyId'";
        $result = $db->RunQuery2($sql_update);
        return $current_serial_no;
    }
		$db->CloseConnection();		
	
?>
var basePath	="presentation/warehouse/stockAdjustment/";
	
			
$(document).ready(function() {
	
	loadMainCategory();
	loadSubCategory();
	
   $("#frmStockAdjustment").validationEngine();
   $('#frmStockAdjustment #cboDepartment').focus();
		
  //-------------------------------------------- 
  $('#frmStockAdjustment #cboMainCategory').change(function(){
		loadSubCategory();
  });
 //-------------------------------------------- 
  $('#frmStockAdjustment #cboSubCategory').change(function(){
	    selectMainCategory();
		loadItems();
		loadGridData();
  });
 //-------------------------------------------- 
  $('#frmStockAdjustment #cboItem').change(function(){
		loadGridData();
  });
  //-------------------------------------------------------
 
  $('#frmStockAdjustment #butSave').click(function(){
	var requestType = '';
	if ($('#frmStockAdjustment').validationEngine('validate'))   
    { 
	var val = $.prompt('Are you sure you want to do this stock adjustment?',{
				buttons: { Ok: true, Cancel: false },
				callback: function(v,m,f){
					if(v)
					{
						
			var data = "requestType=save";

			var rowCount = document.getElementById('tblMain').rows.length;
			if(rowCount==1){
				alert("items not selected to Adjust");
				return false;				
			}
			var toSave=0;
			$('#tblMain .item').each(function(){
				var newRate	= $(this).parent().find(".grnRateNew").val();
				var Qty	= $(this).parent().find(".qty").val();
				if((Qty==0) && (newRate==0)){
					toSave++;
				}
			});
			
			if(toSave==rowCount-1){
				alert("No changes to Save");
				return false;				
			}
			
			var row = 0;
			
			var arr="[";
			
			$('#tblMain .item').each(function(){
				
				var itemId	= $(this).attr('id');
				var grnNo	= $(this).parent().find(".grnNo").html();
				var grnDate	= $(this).parent().find(".grnDate").html();
				var oldRate	= $(this).parent().find(".grnRate").html();
				var newRate	= $(this).parent().find(".grnRateNew").val();
				var currency	= $(this).parent().find(".currency").attr('id');
				var operationType	= $(this).parent().find(".operation").val();
				var Qty	= $(this).parent().find(".qty").val();
					
					if((Qty>0) || (newRate>0)){
						
						arr += "{";
						arr += '"itemId":"'+	itemId +'",' ;
						arr += '"grnNo":"'+	grnNo +'",' ;
						arr += '"grnDate":"'+	grnDate +'",' ;
						arr += '"oldRate":"'+	oldRate +'",' ;
						arr += '"newRate":"'+		newRate +'",' ;
						arr += '"currency":"'+		currency +'",' ;
						arr += '"operationType":"'+		operationType +'",' ;
						arr += '"Qty":"'+		Qty +'"' ;
						arr +=  '},';
						
					}
			});
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;

		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = basePath+"stockAdjustment-db.php";
     	var obj = $.ajax({
			url:url,
			type:'post',
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmStockAdjustment #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t=setTimeout("alertx()",4000);
						$('#frmStockAdjustment #cboItem').change();
						return;
					}
				},
			error:function(xhr,status){
					
					$('#frmStockAdjustment #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
			
					}
				}});
	}
	
   });
   
	
	//--------------refresh the form----------
	$('#frmStockAdjustment #butNew').click(function(){
		window.location.href ='?q=100';
		$('#frmStockAdjustment').get(0).reset();
		clearRows();
		$('#frmStockAdjustment #txtGrnNo').val('');
		$('#frmStockAdjustment #txtGrnYear').val('');
		$('#frmStockAdjustment #txtInvioceNo').val('');
		$('#frmStockAdjustment #cboPO').val('');
		$('#frmStockAdjustment #txtDeliveryNo').val('');
		$('#frmStockAdjustment #txtTotGrnAmmount').val('');
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmStockAdjustment #dtDate').val(d);
	});
//----------------------------	
$('.delImg').click(function(){
	$(this).parent().parent().remove();
});

  //-----------------------------------------------------
$('#frmStockAdjustment #butClose').click(function(){
		//load('main.php');	
});
//--------------refresh the form-----------------------
	$('#frmStockAdjustment #butNew').click(function(){
		$('#frmStockAdjustment').get(0).reset();
		clearRows();
		$('#frmStockAdjustment #txtGrnNo').val('');
		$('#frmStockAdjustment #txtGrnYear').val('');
		$('#frmStockAdjustment #txtInvioceNo').val('');
		$('#frmStockAdjustment #cboPO').val('');
		$('#frmStockAdjustment #cboPO').focus();
		$('#frmStockAdjustment #txtDeliveryNo').val('');
		$('#frmStockAdjustment #txtTotGrnAmmount').val('');
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmStockAdjustment #dtDate').val(d);
	});
//-----------------------------------------------------
});

//-------------------------------------
function alertx()
{
	$('#frmStockAdjustment #butSave').validationEngine('hide')	;
}
function alertDelete()
{
	$('#frmStockAdjustment #butDelete').validationEngine('hide')	;
}

function closePopUp(){
	
}
//----------------------------------------------
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
//----------------------------------------------- 
function clearRows()
{
	var rowCount = document.getElementById('tblMain').rows.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblMain').deleteRow(1);
	}
}
//----------------------------------------------------
function loadMainCategory(){
	    var subCat = $('#cboSubCategory').val();
		var url 		= basePath+"stockAdjustment-db.php?requestType=loadMainCategories";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"subCat="+subCat,
			async:false,
			success:function(json){
					document.getElementById("cboMainCategory").innerHTML=json.arrCombo;
			}
		});
}
//----------------------------------------------------
function selectMainCategory(){
	    var subCat = $('#cboSubCategory').val();
		var url 		= basePath+"stockAdjustment-db.php?requestType=selectMainCategory";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"subCat="+subCat,
			async:false,
			success:function(json){ 
					document.getElementById("cboMainCategory").value=json.mainCat;
			}
		});
}
//----------------------------------------------------
function loadSubCategory(){
	    var mainCat = $('#cboMainCategory').val();
		var url 		= basePath+"stockAdjustment-db.php?requestType=loadSubCategories";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"mainCat="+mainCat,
			async:false,
			success:function(json){
					document.getElementById("cboSubCategory").innerHTML=json.arrCombo;
			}
		});
}
//----------------------------------------------------
function loadItems(){
	    var subCat = $('#cboSubCategory').val();
		var url 		= basePath+"stockAdjustment-db.php?requestType=loadItems";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"subCat="+subCat,
			async:false,
			success:function(json){
					document.getElementById("cboItem").innerHTML=json.arrCombo;
			}
		});
}
//----------------------------------------------------
function loadGridData(){
	
		 clearRows();
	
	    var subCat = $('#cboSubCategory').val();
	    var itemId = $('#cboItem').val();
		var url 		= basePath+"stockAdjustment-db.php?requestType=loadGridData";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"subCat="+subCat+"&itemId="+itemId,
			async:false,
			success:function(json){
				
				var length = json.arrData.length;
				var arrData = json.arrData;
				for(var i=0;i<length;i++)
				{
					var maincatId=arrData[i]['intMainCategory'];	
					var mainCatName=arrData[i]['mainCatName'];
					var subCatId=arrData[i]['intSubCategory'];
					var subCatName=arrData[i]['subCatName'];	
					var code=arrData[i]['strCode'];	
                                        var supItemCode=arrData[i]['SUP_ITEM_CODE'];	
					var uom=arrData[i]['uom'];	
					var itemId=arrData[i]['intItemId'];	
					var itemName=arrData[i]['itemName'];
					var grnNo=arrData[i]['intGRNNo'];	
					var grnYear=arrData[i]['intGRNYear'];
					var currency=arrData[i]['currency'];
					var currencyId=arrData[i]['intCurrencyId'];
					var date=arrData[i]['dtGRNDate'];	
					var rate=arrData[i]['dblGRNRate'];
					var stockBal=arrData[i]['stockBal'];	
					var opCombo='<select style="width:50px" name="cboGatePassTo" id="cboGatePassTo"   class="validate[required] operation"><option value="1">+</option><option value="-1">-</option></select>';

					var content='<tr class="normalfnt">';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'">'+mainCatName+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'">'+subCatName+'</td>';
                                        if(supItemCode!=null)
                                        {
                                        content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'">'+supItemCode+'</td>';   
                                        }else{
					content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'">'+code+'</td>';
                                        }
					content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="item">'+itemName+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+uom+'" class="uom">'+uom+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+grnNo+'/'+grnYear+'"  class="grnNo">'+grnNo+'/'+grnYear+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+date+'"  class="grnDate">'+date+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+rate+'"  class="grnRate">'+rate+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+currencyId+'"  class="currency">'+currency+'</td>';
					content +='<td align="right" bgcolor="#FFFFFF" id="'+stockBal+'">'+stockBal+'</td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+stockBal+'" ><input  id="'+stockBal+'" class="validate[required,custom[number]] grnRateNew" style="width:80px;text-align:right" type="text" value="0" readonly="readonly" /></td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+stockBal+'" ><input  id="'+stockBal+'" class="validate[required,custom[number]] qty" style="width:80px;text-align:right" type="text" value="0"/></td>';
					content +='<td align="center" bgcolor="#FFFFFF" id="'+stockBal+'" >'+opCombo+'</td></tr>';
					add_new_row('#frmStockAdjustment #tblMain',content);
					
				}
			}
		});
}
//----------------------------------------------------

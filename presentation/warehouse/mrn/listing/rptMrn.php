<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
$companyId 		= $_SESSION['CompanyID'];
$intUser 		= $_SESSION["userId"];
$mainPath 		= $_SESSION['mainPath'];
$thisFilePath 	= $_SERVER['PHP_SELF'];

$mrnNo 			= $_REQUEST['mrnNo'];
$year 			= $_REQUEST['year'];
//$mrnType = $_REQUEST['mrnType'];
$approveMode 	= $_REQUEST['approveMode'];

$programName='Material Request Note';
$programCode='P0229';
$spMenuId = 74;
$mrnReportId = 938;

$sql = "SELECT
ware_mrnheader.datdate,
ware_mrnheader.strRemarks, 
ware_mrnheader.intStatus,
ware_mrnheader.intApproveLevels,
ware_mrnheader.intUser,
ware_mrnheader.APPROVAL_REMARKS,
ware_mrnheader.REJECT_REMARKS,
ware_mrnheader.intCompanyId as mrnRaisedLocationId, 
sys_users.strUserName, 
mst_department.strName as department,
ware_mrnheader.mrnType,
mst_locations.strName as locName 
FROM
ware_mrnheader 
Inner Join mst_department ON ware_mrnheader.intDepartment = mst_department.intId
Inner Join mst_locations ON ware_mrnheader.intCompanyId = mst_locations.intId
Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
Inner Join sys_users ON ware_mrnheader.intUser = sys_users.intUserId
WHERE
ware_mrnheader.intMrnNo =  '$mrnNo' AND
ware_mrnheader.intMrnYear =  '$year' 
GROUP BY
ware_mrnheader.intMrnNo,
ware_mrnheader.intMrnYear";


$result = $db->RunQuery($sql);
while($row=mysqli_fetch_array($result))
{
$locationId = $row['mrnRaisedLocationId'];//this locationId use in report header(reportHeader.php)--------------------
$location_name = $row['locName'];
$department = $row['department'];
$mrnDate = $row['datdate'];
$mrnBy = $row['strUserName'];
$intStatus = $row['intStatus'];
$savedLevels = $row['intApproveLevels'];
$user = $row['strUserName'];
$createdUser = $row['intUser'];
$remarks = $row['strRemarks'];
$APPROVAL_REMARKS = $row['APPROVAL_REMARKS'];
$REJECT_REMARKS = $row['REJECT_REMARKS'];
	
$mrnType = $row['mrnType'];
}
$textDisabled = 'disabled';
$displayMainTable = 'display: none';
$checkApproveStatus = 0 ;
if($intStatus > 0 && $intStatus <= $savedLevels){
$checkApproveStatus = 1 ;
}

$confirmationMode = loadConfirmatonMode($programCode,$intStatus,$savedLevels,$intUser);
$rejectionMode = loadRejectionMode($programCode,$intStatus,$savedLevels,$intUser);
$checkSpecialPermision = loadSpecialPermissionMode($spMenuId,$intUser);
if($confirmationMode == 1 || $createdUser == $intUser || $intStatus == 1 ){
$displayMainTable = ''; 
}

?>
<head>
<title>MRN  Report</title>
<script type="application/javascript" src="presentation/warehouse/mrn/listing/rptMrn-js.js"></script>
<style>
.break { page-break-before: always; }

@media print {
.noPrint
{
display:none;
}
}
#apDiv1 {
position:absolute;
left:200px;
top:20px;
width:650px;
height:322px;
z-index:1;
}
.APPROVE {
font-size: 18px;
font-weight: bold;
}
</style>
</head>

<body>
<form id="frmMrnReport" name="frmMrnReport" method="post">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include 'reportHeader.php'?></td>
<td width="20%"></td>
</tr>
<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>MATERIAL REQUISITION NOTE - <?php if($mrnType==3){echo 'PRODUCTION ORDERWISE ';}else if($mrnType==6){echo 'PRODUCTION ORDERWISE ADDITIONAL';}else if( $mrnType==5){echo 'PRODUCTION NON ORDERWISE ';}else if( $mrnType==7){echo 'LOAN SETTLEMENT TO SUPPLIER ';}else if( $mrnType==8){echo 'LOAN OUT TO SUPPLIER ';}?></strong><strong></strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<?php
ob_start();
?>
<tr>
<td>
<table width="100%">
<tr>
<td colspan="16" align="center" bgcolor="#FFDFCB">
	<?php
	if($intStatus>1)
	{
		?>
		<?php if($approveMode == 1){
		 if($confirmationMode == 1 )
		 {
			if($createdUser != $intUser && $checkApproveStatus == 1){ $textDisabled = ''; ?>
			<a id="butSave_rpt" name="butSave" style="margin-top: 5px" class="button white medium">Save Changes</a><?php } ?>
			<img src="images/approve.png"  align="middle" style="margin-bottom: 5px" class="mouseover noPrint" id="imgApprove" />
			<?php
		}
		if($rejectionMode==1)
		 {
			?>
			<img src="images/reject.png" align="middle" style="margin-bottom: 5px" class="mouseover noPrint" id="imgReject" />
			<?php
		}
	}
	}
	?>
</td>
</tr>
<tr>
<?php
if($intStatus==1)
{
	?>
	<td colspan="2" class="APPROVE">CONFIRMED</td>
	<?PHP
}
else if($intStatus==0)
{
	?>
	<td colspan="3" class="APPROVE" style="color:#F00">REJECTED</td>
	<?php
}
else if($intStatus==-2)
{
	?>
	<td colspan="5" class="APPROVE" style="color:#F00">CANCELLED BY THE SYSTEM</td>
	<?php
}
else
{
	?>
	<td width="16%" colspan="6" class="APPROVE">PENDING</td>
	<?php
}
?>
<?php
//-----------------
$sqlc = "SELECT
ware_mrnheader_approvedby.intApproveUser,
ware_mrnheader_approvedby.dtApprovedDate,
sys_users.strUserName as UserName,
ware_mrnheader_approvedby.intApproveLevelNo
FROM
ware_mrnheader_approvedby
Inner Join sys_users ON ware_mrnheader_approvedby.intApproveUser = sys_users.intUserId
WHERE
ware_mrnheader_approvedby.intMrnNo =  '$mrnNo' AND
ware_mrnheader_approvedby.intYear =  '$year' AND
ware_mrnheader_approvedby.intApproveLevelNo =  '$intStatus'";
$resultc = $db->RunQuery($sqlc);
$rowc=mysqli_fetch_array($resultc);
$rejectBy=$rowc['UserName'];
$rejectDate=$rowc['dtApprovedDate'];
//-----------------
?>
</tr>
<tr>
<td width="2%">&nbsp;</td>
<td width="13%"><span class="normalfnt"><strong>MRN No</strong></span></td>
<td width="1%" align="center" valign="middle"><strong>:</strong></td>
<td width="13%"><span class="normalfnt" id="rptmrnNo"><?php echo $mrnNo ?>/<?php echo $year ?></span></td>
<td width="9%" class="normalfnt">&nbsp;</td>
<td width="16%" class="normalfnt"><strong>Remarks(approval)</strong></td>
<td width="1%" align="center" valign="middle"><strong>:</strong></td>
<td colspan="9" rowspan="2" valign="top" bordercolor="#000000"><span class="normalfnt">
  <?php 
   echo $remarks;
	$count1=substr_count($remarks, '<br>');
	//$count1 +=substr_count($REJECT_REMARKS, '<br>');
	//$count1 +=substr_count( $REJECT_REMARKS, "\n" );
		$arr_remars = explode("Level ",$APPROVAL_REMARKS);
		if(sizeof($arr_remars) > 1) {
		foreach ($arr_remars AS $arr_leve) {
		echo $arr_leve."<br />";
		$count1++;
	}}?>
	</span>
</td>
<td width="10%" class="normalfnt"><strong>Remarks(reject)</strong></td>
<td width="1%" align="center" valign="middle"><strong>:</strong></td>
<td width="54%"><span class="normalfnt">
 <?php 
   echo $REJECT_REMARKS;
   ?>
	</span>
</td>
<td width="1%"><div id="divMrnNo" style="display:none"><?php echo $mrnNo ?>/<?php echo $year ?></div></td>
<td width="5%"></td>
</tr>
<tr>
<td>&nbsp;</td>
<td class="normalfnt"><strong>MRN By</strong></td>
<td align="center" valign="middle"><strong>:</strong></td>
<td><span class="normalfnt"><?php echo $user  ?></span></td>
<td>&nbsp;</td>

<td>&nbsp;</td>
<td align="center" valign="middle">&nbsp;</td>
<td>&nbsp;</td>
<?php if($intStatus==0){
	$sqlc = "SELECT
ware_mrnheader_approvedby.intApproveUser,
ware_mrnheader_approvedby.dtApprovedDate,
sys_users.strUserName as UserName,
ware_mrnheader_approvedby.intApproveLevelNo
FROM
ware_mrnheader_approvedby
Inner Join sys_users ON ware_mrnheader_approvedby.intApproveUser = sys_users.intUserId
WHERE
ware_mrnheader_approvedby.intMrnNo =  '$mrnNo' AND
ware_mrnheader_approvedby.intYear =  '$year' AND
ware_mrnheader_approvedby.intApproveLevelNo =  '0' ";
	$resultc = $db->RunQuery($sqlc);
	$rowc=mysqli_fetch_array($resultc);
	$rejectBy=$rowc['UserName'];
	$rejectDate=$rowc['dtApprovedDate'];
}
?>
<td class="normalfnt">&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
  <td>&nbsp;</td>
  <td class="normalfnt"><strong>Date</strong></td>
  <td align="center" valign="middle"><strong>:</strong></td>
  <td><span class="normalfnt"><?php echo $mrnDate  ?></span></td>
  <td>&nbsp;</td>
  <td><strong>Remarks(reject)</strong></td>
  <td align="center" valign="middle"><strong>:</strong></td>
  <td colspan="9" rowspan="2" valign="top"><span class="normalfnt">
    <?php 
   echo $REJECT_REMARKS;
   ?>
  </span></td>
  </tr>
<tr>
<tr>
<td>&nbsp;</td>
<td class="normalfnt"><strong>Department</strong></td>
<td align="center" valign="middle"><strong>:</strong></td>
<td colspan="3" align="left"><span class="normalfnt"><?php echo $department; ?></span></td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
<tr>
<tr>
<?php $mrnListResult = getMrnOrderWise($mrnNo,$year);
$i = 0;
$row = mysqli_fetch_array($mrnListResult);
$mrnList = $row['mrn'];
if($mrnList != ''){?>
<td>&nbsp;</td>
<td class="normalfnt" style="border: 1px solid #dddddd; background-color: #dce9f9"><strong>Order No</strong></td>
<td align="center"  valign="middle" style="border: 1px solid #dddddd;background-color: #dce9f9"><strong>:</strong></td>
<td colspan="13" align="left" style="border: 1px solid #dddddd;background-color: #dce9f9"><span class="normalfnt"><strong>MRN NO</strong></span></td>
<?php } ?>
</tr>
<tr><?php $mrnListResult = getMrnOrderWise($mrnNo,$year);
$i = 0;
$count=0;
while($row = mysqli_fetch_array($mrnListResult))
{
	if($row['ord'] != '')
		$count++;
	$mrnList 	= $row['mrn'];
	$arrMrn 	= explode(',',$mrnList);
	$ordList 	= $row['ord'];
	$order     	= explode(',',$ordList);
	$order_no   = explode('/',$order[0]);

if($mrnList != '')
{
?>


<td>&nbsp;</td>
<td class="normalfnt" style="border: 1px solid #dddddd"><?php echo $ordList; ?></td>
<td align="center" valign="middle" style="border: 1px solid #dddddd"><strong>:</strong></td>
<td colspan="13" align="left" style="border: 1px solid #dddddd"><span class="normalfnt"><strong></strong>
<?php
foreach ($arrMrn AS $r_mrn){
$arr_F =  explode('/',$r_mrn);
echo  '<a href=?q='.$mrnReportId.'&mrnNo='.$arr_F[0].'&year='.$arr_F[1].'>'.$r_mrn.'</a>'.'&nbsp&nbsp'; } ?>
</span>
</td>
</tr>
</tr>

<tr> 
<td></td>
<td class="normalfnt"><strong>Bulk Costing</strong></td>
<td align="center" valign="middle"><strong>:</strong></td>
<td class="normalfnt">
<?php 
echo  '<a href=?q=117'.'&orderNo='.$order_no[0].'&orderYear='.$order_no[1].'>'.$order_no[0].'/'.$order_no[1].'</a>'.                         '&nbsp&nbsp' ?>

</td> 
</tr>
<?php
}

}
?>
</table>
</td>
</tr>
<tr>
<td>
<table width="100%">
<tr style="<?php echo $displayMainTable; ?>">
<td width="5%"></td>
<td colspan="7" class="normalfnt">
	<table width="100%" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
		<tr class="">
			<th width="8%">MRN Type</th>
			<th width="8%">Order No</th>
			<th width="12%">Sales Order  No</th>
			<th width="14%">Main Category</th>
			<th width="11%">Sub Category</th>
			<th width="9%">Item Code</th>
			<th width="25%">Item</th>
			<th width="6%">UOM</th>
			<th width="6%">Tot Bal MRN (Order)</th>
			<th width="6%">Tot Issued Qty</th>
			<th width="2%" style="display: none">Cleared Qty</th>
			<th width="6%">Qty</th>
			<th width="9%" bgcolor="#FFE6E6">Extra Qty</th>
			<th width="14%">Total Qty</th>
			<?php if($checkSpecialPermision == 'true'){ ?>
				<th width="9%">Unit Price</th>
				<th width="9%">Currency</th>
				<th width="9%">Amount</th>
			<?php } ?>
		</tr>
		<?php
		$sql1 = "SELECT
			ware_mrndetails.intMrnNo,
			ware_mrndetails.intMrnYear,
			ware_mrndetails.intOrderNo,
			ware_mrndetails.intOrderYear,
			ware_mrndetails.strStyleNo,
			ware_mrndetails.dblIssudQty,
			trn_orderdetails.strSalesOrderNo,
			trn_orderdetails.intSalesOrderId,
			ware_mrndetails.intItemId,
			ware_mrndetails.INITIAL_QTY,
			ware_mrndetails.INITIAL_EXTRA_QTY,
			ware_mrndetails.dblQty,
			ware_mrndetails.dblMRNClearQty, 
			ware_mrndetails.EXTRA_QTY, 
			ware_mrndetails.MRN_TYPE,
			mst_item.strCode,
			mst_item.intId AS itemId,
			mst_item.strName,
			mst_item.intUOM, 
                        mst_item.ITEM_HIDE,
                        mst_item.strCode as SUP_ITEM_CODE,
			mst_item.dblLastPrice,
			mst_financecurrency.strCode AS currencyCode,
                        mst_maincategory.intId AS mainCatId,
			mst_maincategory.strName AS mainCategory,
			mst_subcategory.strName AS subCategory, 
			mst_units.strCode as uom , 
			mst_part.strName as part,
			mst_financeexchangerate.dblBuying as rate,
			trn_orderheader.INSTANT_ORDER,
			trn_orderheader.PO_TYPE  
			FROM ware_mrndetails 
			left Join trn_orderdetails ON ware_mrndetails.intOrderNo = trn_orderdetails.intOrderNo AND ware_mrndetails.intOrderYear = trn_orderdetails.intOrderYear AND ware_mrndetails.strStyleNo = trn_orderdetails.intSalesOrderId
			Inner Join mst_item ON ware_mrndetails.intItemId = mst_item.intId
			INNER JOIN mst_financecurrency ON mst_item.intCurrency = mst_financecurrency.intId
			INNER JOIN mst_financeexchangerate on  mst_item.intCurrency =    mst_financeexchangerate.intCurrencyId
			Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
			Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
			Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
			left Join mst_part ON trn_orderdetails.intPart = mst_part.intId 
			LEFT JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
			LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier =  
                        mst_supplier.intId
                        WHERE
			ware_mrndetails.intMrnNo =  '$mrnNo' AND
			ware_mrndetails.intMrnYear =  '$year'  
			AND
			mst_financeexchangerate.intCompanyId = '1' 
			AND mst_financeexchangerate.dtmDate = (select MAX(mst_financeexchangerate.dtmDate) 
			FROM mst_financeexchangerate 
			WHERE mst_financeexchangerate.intCompanyId = '1') 
			 Order by mst_maincategory.strName asc, mst_subcategory.strName asc, mst_item.strName asc";
			
		$result1 = $db->RunQuery($sql1);
		$totQty = 0;
		$totClearQty = 0;
		$totAmmount = 0;
		$r=0;
		while($row=mysqli_fetch_array($result1))
		{ 
			$r++;
			$arrayRow[$r-1]	 	=$row;
			$currencyCode 		= $row['currencyCode'];
			$orderNo 			= $row['intOrderNo']."/".$row['intOrderYear'];
			$salesOrderNo 		= $row['strSalesOrderNo']."/".$row['part'];
			$dblPrice 			= $row['dblLastPrice'];
			$exchange_rate 		= $row['rate'];
			$orderQty 			= $row['dblQty'];
			$issuedQty 			= $row['dblIssudQty'];
			$orderWiseIssuedQty = getIssuedQty($mrnNo,$year,$row['intOrderNo'],$row['intOrderYear'],$row['strStyleNo'],$row['itemId']);
			$ExtraQty			= $row['EXTRA_QTY'];
			$MRNClearQty 		= $row['dblMRNClearQty'];
                        $mainCatId          = $row['mainCatId'];
                        $ITEM_HIDE          = $row['ITEM_HIDE'];
			//if($ExtraQty < 0)
			//$ExtraQty = 0;
			/*if($MRNClearQty>$Extraqty)
			{
			
			$ExtraQty = 0;
			}
			else
			{
			
			$ExtraQty = $Extraqty;
			}*/
			$mrnType_Details 	= $row['MRN_TYPE'];
			$initial_orderQty 	= $row['INITIAL_QTY'];
			$initial_extraQty 	= $row['INITIAL_EXTRA_QTY'];
			$QtyWithoutExtra 	= $orderQty - $ExtraQty;
			
			if($QtyWithoutExtra<0)
			$QtyWithoutExtra=0;
			
			if($row['intOrderNo']==0){
				$orderNo='';
				$salesOrderNo='';
			}
			// Balance MRN Qty
			$priQty=round(getPRIQty($row['intOrderNo'],$row['intOrderYear'],$row['strStyleNo'],$row['itemId']),4);
			$mrnQty=round(getMRNQty($row['intOrderNo'],$row['intOrderYear'],$row['strStyleNo'],$row['itemId']),4);
			$actualPriQty=$priQty-$mrnQty;

			////////2017-11-02/////////
			if($row['PO_TYPE']==1 && $row['INSTANT_ORDER']==1){
				$priQty_split =0;
				$mrnQty_split =0;
				$actualPriQty_split=0;
				$result_sp	  = get_split_orders($row['intOrderNo'],$row['intOrderYear'],$row['strStyleNo']);
				while($row_sp=mysqli_fetch_array($result_sp))
				{
					$priQty_split =round(getPRIQty($row_sp['intOrderNo'],$row_sp['intOrderYear'],$row_sp['intSalesOrderId'],$row['itemId']),4);
					$mrnQty_split =round(getMRNQty($row_sp['intOrderNo'],$row_sp['intOrderYear'],$row_sp['intSalesOrderId'],$row['itemId']),4);

					$actualPriQty_split +=round(($priQty_split)*(100+$extra_percentage)/100,4) -$mrnQty_split;

				}
				$actualPriQty +=$actualPriQty_split;
			}
			///////2017-11-02//////////
			
			if($actualPriQty<0){
				$actualPriQty = 0;
			}
			$actualPriQty=getRoundedWeight($actualPriQty, $row['itemId']);
			// ----------------------
			if($mrnType_Details == 6){
				$backgroundColor = "bgcolor='#FFE6E6'";
			}else{
				$backgroundColor = "bgcolor='#FFFFFF'";
			}

			if($mrnType_Details==3)
				$mrnType_dec='Order based';
			else if($mrnType_Details==6)
				$mrnType_dec='Order based Additional';
			else if($mrnType_Details==5)
				$mrnType_dec='Non Order based';
			else if($mrnType_Details==2)
				$mrnType_dec='Technical R&D';
			else if($mrnType_Details==1)
				$mrnType_dec='Sample';
			else if($mrnType_Details==0)
				$mrnType_dec='Non RM';
			else if($mrnType_Details==7)
				$mrnType_dec='Loan Settlement To Supplier';

			?>
<tr class="normalfnt"  <?php echo $backgroundColor; ?> id="dataRowRpt">
<td class="normalfnt mrnType" id="<?php echo $mrnType_Details; ?>" nowrap>&nbsp;<?php echo $mrnType_dec; ?></td>
<td class="normalfnt" id="rptOrderNo"><?php echo $orderNo; ?></td>
<td class="StrStyle" id="<?php echo $row['strStyleNo']; ?>">&nbsp;<?php echo $salesOrderNo; ?>&nbsp;</td>
<td class="normalfnt" nowrap id="rptMainCat">&nbsp;<?php echo $row['mainCategory']; ?>&nbsp;</td>
<td class="normalfnt" nowrap id="rptSubCat">&nbsp;<?php echo $row['subCategory']; ?>&nbsp;</td>
 <?php
if($row['SUP_ITEM_CODE']!=null)
{
?>
<td class="normalfnt">&nbsp;<?php echo $row['SUP_ITEM_CODE'] ?>&nbsp;</td>
<?php
}else{
?>
<td class="normalfnt">&nbsp;<?php echo $row['strCode'] ?>&nbsp;</td>
<?php
}

if($ITEM_HIDE==1)
{
    
?>
<td class="itemIdRpt" nowrap id="<?php echo $row['itemId']; ?>">&nbsp;*****&nbsp;</td>
<?php
}else
{
?>
<td class="itemIdRpt" nowrap id="<?php echo $row['itemId']; ?>">&nbsp;<?php echo $row['strName']; ?>&nbsp;</td>
<?php
}
?>
<td class="normalfntMid" id="unitOfMe"><?php echo $row['uom']; ?></td>
<td style="background-color: #DDDDDD; text-align: right" id="<?php echo $actualPriQty;?>" class="actualPriQty"><?php echo $actualPriQty;?></td>
<td style="background-color: #DDDDDD; text-align: right"><?php echo round($orderWiseIssuedQty,4); ?></td>
<td class="normalfntRight" id="dblMrnClearQty" style="display: none"><?php echo $row['dblMRNClearQty']; ?></td>
<td class="normalfntRight" id="rptOrderQtyWithoutExtra"><?php echo round($QtyWithoutExtra,4); ?></td>
<td class="normalfntRight" id="rptExtra"><?php echo round($ExtraQty,4); ?></td>
<td class="normalfntRight"><input type="text" <?php echo $textDisabled; ?> value="<?php echo $orderQty_round = round($orderQty,4); ?>" class="validate[custom[number],max[<?php echo $initial_orderQty; ?>]] rptExtra" id="rptOrderQty"  style="width:100%; text-align: right" /></td>
 
				<?php 
				 if($checkSpecialPermision == 'true')
				 { 
					
					 ?>
					<td class="normalfntRight" style="text-align: right" id="rptdblprice">
					<?php 
					 if($currencyCode!="USD")
					{
							$sql_usd = "SELECT mst_financeexchangerate.dblBuying as dblBuying_usd,
							mst_financeexchangerate.intCurrencyId,
							mst_financeexchangerate.intCompanyId,
							mst_financeexchangerate.dtmDate
							FROM
							mst_financeexchangerate
							WHERE 
							mst_financeexchangerate.intCompanyId = '1' AND
							mst_financeexchangerate.intCurrencyId = '1' AND
							mst_financeexchangerate.dtmDate = (select MAX(mst_financeexchangerate.dtmDate)
							FROM
							mst_financeexchangerate
							WHERE
							mst_financeexchangerate.intCompanyId = '1' ) ";
					
				
							$result2 = $db->RunQuery($sql_usd);
							while($row_1=mysqli_fetch_array($result2))
							{
							$rate_usd = $row_1['dblBuying_usd'];
							
							 echo round( $dblPrice_1 = ($dblPrice*($exchange_rate/$rate_usd )),4);
							}
							}
							else
							{
							  echo round($dblPrice,4); 
							  
							}
							?></td>
								<td class="normalfntRight" style="text-align: right"><?php echo USD ?></td>
								<td class="normalfntRight" style="text-align: right" id="rptAmount">&nbsp;
								<?php
								 if($currencyCode!="USD")
								{
							$sql_usd = "SELECT mst_financeexchangerate.dblBuying as dblBuying_usd,
							mst_financeexchangerate.intCurrencyId,
							mst_financeexchangerate.intCompanyId,
							mst_financeexchangerate.dtmDate
							FROM
							mst_financeexchangerate
							WHERE 
							mst_financeexchangerate.intCompanyId = '1' AND
							mst_financeexchangerate.intCurrencyId = '1' AND
							mst_financeexchangerate.dtmDate = (select MAX(mst_financeexchangerate.dtmDate)
							FROM
							mst_financeexchangerate
							WHERE
							mst_financeexchangerate.intCompanyId = '1' ) ";
					
					
							$result2 = $db->RunQuery($sql_usd);
							while($row_1=mysqli_fetch_array($result2))
							{
							$rate_usd = $row_1['dblBuying_usd'];
							
							 echo round($amount = ($dblPrice*$orderQty_round*($exchange_rate/$rate_usd )),4);
							}
							}
							else
							{
							 echo round($amount = ($dblPrice*$orderQty_round),4);	
							}
							 }
		?>&nbsp;</td>
				<?php 
				
?>

				
			</tr>
			<?php
			$totQty+=$row['dblQty'];
			$totClearQty+=$row['dblMRNClearQty'];
			$totExtra += $ExtraQty;
			$totamount+= $amount;
			$totWithoutExtra += $QtyWithoutExtra;
		}?>
		
		<tr class="normalfnt"  bgcolor="#CCCCCC">
			<td class="normalfnt">&nbsp;</td>
			<td class="normalfnt">&nbsp;</td>
			<td class="normalfnt">&nbsp;</td>
			<td class="normalfnt">&nbsp;</td>
			<td class="normalfnt">&nbsp;</td>
			<td class="normalfnt">&nbsp;</td>
			<td class="normalfnt">&nbsp;</td>
			<td class="normalfntRight" >&nbsp;</td>
			<td class="normalfntRight"></td>
			<td class="normalfnt">&nbsp;</td>
			<td class="normalfntRight"><?php echo round($totWithoutExtra,4); ?></td>
			<td class="normalfntRight"><?php echo round($totExtra,4); ?></td>
			<td class="normalfntRight"><?php echo round($totQty,4); ?></td>
			<?php if($checkSpecialPermision == 'true'){ ?>
				<td class="normalfnt">&nbsp;</td>
				<td class="normalfnt">&nbsp;</td>
				 <td class="normalfntRight"><?php echo round($totamount,4); ?></td>
			<?php } 
		?>

		</tr>
	</table>
</td>
<td width="6%">&nbsp;</td>
</tr>

</table>
</td>
</tr>
<tr><td height="25" valign="bottom" class="normalfnt"><b>Summary</b></td></tr>
<tr>
<td>
<table width="100%">
<tr>
<td width="5%">&nbsp;</td>
<td colspan="7" class="normalfnt">
	<table width="100%" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
		<tr class="">
			<th width="8%" >MRN Type</th>
			<th width="14%" >Main Category</th>
			<th width="11%" >Sub Category</th>
			<th width="9%" >Item Code</th>
			<th width="25%" >Item</th>
			<th width="6%" >UOM</th>
			<th width="6%" >Qty</th>
			<th width="9%" style="display: none">Cleared Qty</th>
			<th width="9%" bgcolor="#FFE6E6" >Extra Qty</th>
			<th width="9%" >Total Qty</th>
		</tr>
		<?php
		$sql1 = "SELECT
ware_mrndetails.intMrnNo,
ware_mrndetails.intMrnYear,
GROUP_CONCAT(ware_mrndetails.intOrderNo) as intOrderNo,
ware_mrndetails.intOrderYear,
ware_mrndetails.strStyleNo,
trn_orderdetails.strSalesOrderNo,
trn_orderdetails.intSalesOrderId,
ware_mrndetails.intItemId,
sum(ware_mrndetails.dblQty) as dblQty,
sum(ware_mrndetails.dblMRNClearQty) as dblMRNClearQty, 
sum(ware_mrndetails.EXTRA_QTY) as EXTRA_QTY, 
ware_mrndetails.MRN_TYPE,
mst_item.strCode,
mst_item.strName,
mst_item.intUOM,
mst_item.ITEM_HIDE,
mst_item.strCode as SUP_ITEM_CODE,
mst_maincategory.strName AS mainCategory,
mst_maincategory.intId AS mainCatId,
mst_subcategory.strName AS subCategory, 
mst_units.strCode as uom , 
mst_part.strName as part 
FROM ware_mrndetails 
left Join trn_orderdetails ON ware_mrndetails.intOrderNo = trn_orderdetails.intOrderNo AND ware_mrndetails.intOrderYear = trn_orderdetails.intOrderYear AND ware_mrndetails.strStyleNo = trn_orderdetails.intSalesOrderId
Inner Join mst_item ON ware_mrndetails.intItemId = mst_item.intId
Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
left Join mst_part ON trn_orderdetails.intPart = mst_part.intId 
LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
WHERE
ware_mrndetails.intMrnNo =  '$mrnNo' AND
ware_mrndetails.intMrnYear =  '$year' 
group by mst_item.intId 
Order by mst_maincategory.strName asc, mst_subcategory.strName asc, mst_item.strName asc 
";


		$result1 = $db->RunQuery($sql1);
		$totQty_S = 0;
		$totClearQty_S = 0;
		$totAmmount_S = 0;
		while($row = mysqli_fetch_array($result1))
		{
			$orderNo 		= $row['intOrderNo']."/".$row['intOrderYear'];
			$salesOrderNo 	= $row['strSalesOrderNo']."/".$row['part'];
			$orderQty 		= $row['dblQty'];
			$ExtraQty 		= $row['EXTRA_QTY'];
			
			$MRNClearQty 	= $row['dblMRNClearQty'];
                        $mainCatId 	= $row['mainCatId'];
                        $ITEM_HIDE          = $row['ITEM_HIDE'];
			
			
			
			
			$mrnType_Details = $row['MRN_TYPE'];
			$QtyWithoutExtra = $orderQty - $ExtraQty;
			
			if($QtyWithoutExtra<0)
			$QtyWithoutExtra=0;
			
			if($row['intOrderNo'] == 0){
				$orderNo = '';
				$salesOrderNo = '';
			}
			if($mrnType_Details == 6){
				$backgroundColor = "bgcolor='#FFE6E6'";
			}else{
				$backgroundColor = "bgcolor='#FFFFFF'";
			}

			if($mrnType_Details==3)
				$mrnType_dec='Order based';
			else if($mrnType_Details==6)
				$mrnType_dec='Order based Additional';
			else if($mrnType_Details==5)
				$mrnType_dec='Non Order based';
			else if($mrnType_Details==2)
				$mrnType_dec='Technical R&D';
			else if($mrnType_Details==1)
				$mrnType_dec='Sample';
			else if($mrnType_Details==0)
				$mrnType_dec='Non RM';
			else if($mrnType_Details==7)
				$mrnType_dec='Loan Settlement To Supplier';
			?>
			<tr class="normalfnt"  <?php echo $backgroundColor; ?> >
				<td class="normalfnt" nowrap >&nbsp;<?php echo $mrnType_dec; ?></td>
				<td class="normalfnt" nowrap >&nbsp;<?php echo $row['mainCategory']?>&nbsp;</td>
				<td class="normalfnt" nowrap >&nbsp;<?php echo $row['subCategory'] ?>&nbsp;</td>
				 <?php
                                if($row['SUP_ITEM_CODE']!=null)
                                {
                                ?>
                                <td class="normalfnt">&nbsp;<?php echo $row['SUP_ITEM_CODE'] ?>&nbsp;</td>
                                <?php
                                }else{
                                ?>
                                <td class="normalfnt">&nbsp;<?php echo $row['strCode'] ?>&nbsp;</td>
                                <?php
                                }
                               
                                if($ITEM_HIDE == 1)
                                {
                                ?>    
                                <td  class="normalfnt" nowrap >*****</td>
                                <?php
                                } else {
                                    
                                
                                ?>
				<td class="normalfnt" nowrap >&nbsp;<?php echo $row['strName'] ?>&nbsp;</td>
                                <?php
                                }
                                ?>
				<td class="normalfntMid" >&nbsp;<?php echo $row['uom'] ?></td>
				<td class="normalfntRight" >&nbsp;<?php echo round($QtyWithoutExtra,4); ?>&nbsp;</td>
				<td class="normalfntRight" style="display: none">&nbsp;<?php echo $row['dblMRNClearQty'] ?>&nbsp;</td>
				<td class="normalfntRight"  bgcolor="#FFE6E6"  >&nbsp;<?php echo round($ExtraQty,4); ?>&nbsp;</td>
				<td class="normalfntRight" >&nbsp;<?php echo round($orderQty,4); ?>&nbsp;</td>
			</tr>
			<?php
			$totQty_S += $row['dblQty'];
			$totClearQty_S += $row['dblMRNClearQty'];
			$totAmmount_S += $ExtraQty;
			$totWithoutExtra_S += $QtyWithoutExtra;
		}
		?>
		<tr class="normalfnt"  bgcolor="#CCCCCC">
			<td class="normalfnt" >&nbsp;</td>
			<td class="normalfnt" >&nbsp;</td>
			<td class="normalfnt" >&nbsp;</td>
			<td class="normalfnt" >&nbsp;</td>
			<td class="normalfntRight" >&nbsp;</td>
			<td class="normalfntRight" >&nbsp;</td>
			<td class="normalfntRight" >&nbsp;<?php echo round($totWithoutExtra_S,4); ?>&nbsp;</td>
			<td class="normalfntRight" >&nbsp;<?php echo round($totAmmount_S,4); ?>&nbsp;</td>
			<td class="normalfnt" style="text-align: right">&nbsp;<?php echo round($totQty_S,4); ?></td>
		</tr>
	</table>
</td>
<td width="6%">&nbsp;</td>
</tr>

</table>
</td>
</tr>
<?php
$flag=0;
$sqlc = "SELECT
ware_mrnheader_approvedby.intApproveUser,
ware_mrnheader_approvedby.dtApprovedDate,
sys_users.strUserName as UserName,
ware_mrnheader_approvedby.intApproveLevelNo
FROM
ware_mrnheader_approvedby
Inner Join sys_users ON ware_mrnheader_approvedby.intApproveUser = sys_users.intUserId
WHERE
ware_mrnheader_approvedby.intMrnNo =  '$mrnNo' AND
ware_mrnheader_approvedby.intYear =  '$year'  /*AND 
ware_mrnheader_approvedby.intStatus='0'  */
order by dtApprovedDate asc";
$resultc = $db->RunQuery($sqlc);
while($rowc=mysqli_fetch_array($resultc)){
if($rowc['intApproveLevelNo']==1)
$desc="1st ";
else if($rowc['intApproveLevelNo']==2)
$desc="2nd ";
else if($rowc['intApproveLevelNo']==3)
$desc="3rd ";
else
$desc=$rowc['intApproveLevelNo']."th ";
//  $desc=$ap.$desc;
$desc2=$rowc['UserName']."(".$rowc['dtApprovedDate'].")";
if($rowc['UserName']=='')
$desc2='---------------------------------';

if($rowc['intApproveLevelNo']>0){
$flag=1;
?>
<tr>
<td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> Approved By - </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
</tr>
<?php
}
if($rowc['intApproveLevelNo']==0){
$flag=2;
?>
<tr>
<td bgcolor="#FFFFFF"><span class="normalfnt"><strong> Rejected By - </strong></span><span class="normalfnt"><?php echo $rowc['UserName']."(".$rowc['dtApprovedDate'].")";?></span></td>
</tr>
<?php
}
if($rowc['intApproveLevelNo']==-1){
$flag=3;
?>
<tr>
<td bgcolor="#FFFFFF"><span class="normalfnt"><strong> Revised By - </strong></span><span class="normalfnt"><?php echo $rowc['UserName']."(".$rowc['dtApprovedDate'].")";?></span></td>
</tr>
<?php
}

if($rowc['intApproveLevelNo']>0){
$finalSavedLevel=$rowc['intApproveLevelNo'];
}
}//end of while

if($flag>1){
$finalSavedLevel=0;
}

if(($flag<=1) || ($intStatus>0)){
for($j=$finalSavedLevel+1; $j<=$savedLevels; $j++)
{
if($j==1)
$desc="1st ";
else if($j==2)
$desc="2nd ";
else if($j==3)
$desc="3rd ";
else
$desc=$j."th ";
$desc2='---------------------------------';
?>
<tr>
<td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> Approved By - </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
</tr>
<?php
}
}

echo $body 			= ob_get_clean();


?>
<tr height="90" >
<td align="center" class="normalfntMid"></td>
</tr>
<tr height="40">
<?php
//$locationId = '';
$sql = "SELECT mst_locations.intCompanyId FROM mst_locations WHERE mst_locations.intId =  '$locationId'";
$result 	= $db->RunQuery($sql);
$row		= mysqli_fetch_array($result);
$createCompanyId = $row['intCompanyId'];
?>
<?Php
$url  = "{$backwardseperator}presentation/sendToApproval.php";		// * file name
$url .= "?status=$intStatus";										// * set recent status
$url .= "&approveLevels=$savedLevels";								// * set approve levels in order header
$url .= "&programCode=$programCode";								// * program code (ex:P10001)
$url .= "&program=MRN";									// * program name (ex:Purchase Order)
$url .= "&companyId=$createCompanyId";									// * created company id
$url .= "&createUserId=$createdUser";

$url .= "&field1=MRN No";
$url .= "&field2=MRN Year";
$url .= "&field3=MRN Location";
$url .= "&value1=$mrnNo";
$url .= "&value2=$year";
$url .= "&value3=$location_name";


$url .= "&subject=MRN FOR APPROVAL ('$mrnNo'/'$year')";

$url .= "&statement1=Please Approve this";
$url .= "&statement2=to Approve this";
// * doc year
$url .= "&link=".urlencode(base64_encode($mainPath."?q=938&mrnNo=$mrnNo&year=$year&approveMode=1"));
$url .= "&showDetails=1";


/*edit mode for the doc*/
if($savedLevels +1 == $intStatus){
	$editMode = 1;
}else {
	$editMode = 0;
}
?>
<table width="100%">
<tr>
<td align="center" class="normalfntMid"><iframe id="iframeFiles2" src="<?php echo $url;?>" name="iframeFiles" style="width:500px;height:175px;border:none"  ></iframe></td>
<td align="center" class="normalfntMid"><iframe  id="iframeFiles" src="presentation/warehouse/mrn/addNew/filesUpload.php?txtFolder=<?php echo "$mrnNo-$year".'&editMode='.$editMode; ?>" name="iframeFiles" style="width:400px;height:175px;border:none;" class="noPrint"  ></iframe></td>
</tr>
</table>
</tr>
</tr>
<tr height="40">
<td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>
</form>
</body>
</html>
<?php
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
global $db;

$editMode=0;
$sqlp = "SELECT
menupermision.intEdit  
FROM menupermision 
Inner Join menus ON menupermision.intMenuId = menus.intId
WHERE
menus.strCode =  '$programCode' AND
menupermision.intUserId =  '$intUser'";

$resultp = $db->RunQuery($sqlp);
$rowp=mysqli_fetch_array($resultp);

if($rowp['intEdit']==1){
if($intStatus==($savedStat+1) || ($intStatus==0)){
$editMode=1;
}
}

return $editMode;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
global $db;

$confirmatonMode=0;
$k=$savedStat+2-$intStatus;
$sqlp = "SELECT
menupermision.int".$k."Approval 
FROM menupermision 
Inner Join menus ON menupermision.intMenuId = menus.intId
WHERE
menus.strCode =  '$programCode' AND
menupermision.intUserId =  '$intUser'";

$resultp = $db->RunQuery($sqlp);
$rowp=mysqli_fetch_array($resultp);

if($rowp['int'.$k.'Approval']==1){
if($intStatus!=1){
$confirmatonMode=1;
}
}

return $confirmatonMode;
}
function loadSpecialPermissionMode($spMenuId,$intUser){
global $db;

$sqlp = "SELECT IF(EXISTS(SELECT * FROM menus_special_permision WHERE intSpMenuId = '$spMenuId' AND intUser = '$intUser') ,'true', 'false') AS validUser";

$resultp = $db->RunQuery($sqlp);
$rowp = mysqli_fetch_assoc($resultp);
$status = $rowp['validUser'];
return $status;
}
//------------------------------function loadRejectionMode-------------------
function loadRejectionMode($programCode,$intStatus,$savedStat,$intUser){
global $db;

$rejectMode=0;
$sqlp = "SELECT
menupermision.intReject 
FROM menupermision 
Inner Join menus ON menupermision.intMenuId = menus.intId
WHERE
menus.strCode =  '$programCode' AND
menupermision.intUserId =  '$intUser'";

$resultp = $db->RunQuery($sqlp);
$rowp=mysqli_fetch_array($resultp);

if($rowp['intReject']==1){
if($intStatus!=0){
$rejectMode=1;
}
}

return $rejectMode;
}
//-------------------------------------------------------------------------
function getPRIQty($orderNo,$orderYear,$salesOrderNo,$item)
{
global $db;
$sql = "	SELECT
IFNULL(trn_po_prn_details_sales_order.REQUIRED,'0') AS priQty
FROM
trn_po_prn_details_sales_order
WHERE
trn_po_prn_details_sales_order.ORDER_NO = '$orderNo'
AND trn_po_prn_details_sales_order.ORDER_YEAR = '$orderYear'
AND trn_po_prn_details_sales_order.SALES_ORDER = '$salesOrderNo'
AND trn_po_prn_details_sales_order.ITEM = '$item'";

$result = $db->RunQuery($sql);
$row = mysqli_fetch_array($result);
$priQty=$row['priQty'];
return $priQty;
}
function getMRNQty($orderNo,$orderYear,$salesOrderNo,$item)
{
global $db;
$sql = "SELECT
IFNULL(sum(ware_mrndetails.dblQty),'0') AS mrnQty
FROM
ware_mrndetails
INNER JOIN ware_mrnheader ON ware_mrndetails.intMrnNo = ware_mrnheader.intMrnNo
AND ware_mrndetails.intMrnYear = ware_mrnheader.intMrnYear
WHERE
ware_mrndetails.intOrderNo = '$orderNo'
AND ware_mrndetails.intOrderYear = '$orderYear'
AND ware_mrndetails.strStyleNo = '$salesOrderNo'
AND ware_mrndetails.intItemId = '$item'
AND ware_mrnheader.intStatus = '1'";

$result = $db->RunQuery($sql);
$row = mysqli_fetch_array($result);
$mrnQty = $row['mrnQty'];

return $mrnQty;
}
function getIssuedQty($mrnNo,$year,$orderNo,$orderYear,$styleNo,$item){
global $db;
$sql = "SELECT
IFNULL(
sum(ware_issuedetails.dblQty),
'0'
) AS mrnQty
FROM
ware_issuedetails
INNER JOIN ware_issueheader ON ware_issuedetails.intIssueNo = ware_issueheader.intIssueNo 
AND ware_issuedetails.intIssueYear = ware_issueheader.intIssueYear
WHERE ";
if($orderNo > 0){

$sql .= "ware_issuedetails.strOrderNo = '$orderNo'
AND ware_issuedetails.intOrderYear = '$orderYear'
AND ware_issuedetails.strStyleNo = '$styleNo'  ";
}
else{
$sql .= "ware_issuedetails.intMrnNo = '$mrnNo'
AND ware_issuedetails.intMrnYear = '$year' ";
}

$sql .= "AND ware_issuedetails.intItemId = '$item'
AND ware_issueheader.intStatus >= 1 
AND ware_issueheader.intStatus <= ware_issueheader.intApproveLevels";

$result = $db->RunQuery($sql);
$row = mysqli_fetch_array($result);
$issuedQty = $row['mrnQty'];
return $issuedQty;
}
function getMrnOrderWise($mrnNo,$year)
{
global $db;
$sql = "SELECT
(
SELECT
GROUP_CONCAT(distinct D2.intMrnNo,'/',D2.intMrnYear) AS mrnDetails
FROM
ware_mrndetails AS D2
INNER JOIN ware_mrnheader ON  D2.intMrnNo = ware_mrnheader.intMrnNo AND D2.intMrnYear = ware_mrnheader.intMrnYear
WHERE
D1.intOrderNo = D2.intOrderNo AND D1.intOrderYear = D2.intOrderYear AND D1.intOrderNo > 0 
AND ware_mrnheader.intStatus <= (ware_mrnheader.intApproveLevels+1)
) AS mrn,
If(D1.intOrderNo > 0 AND D1.intOrderYear , CONCAT(D1.intOrderNo,'/',D1.intOrderYear),NULL) AS ord
FROM
ware_mrndetails AS D1 
WHERE
D1.intMrnNo = '$mrnNo'
AND D1.intMrnYear = '$year'
GROUP BY
D1.intOrderNo,
D1.intOrderYear";
//echo $sql;

$result_grn = $db->RunQuery($sql);
return  $result_grn;
}
function getRoundedWeight($qty, $item){
global $db;

$sql = "SELECT
mst_item.strName,
mst_item.intUOM 
FROM `mst_item`
WHERE
mst_item.intId = '$item'
";
$result = $db->RunQuery($sql);
$row = mysqli_fetch_array($result);
$uom	= ($row['intUOM']);

if($uom ==6 && floor($qty)==0){//only for weight < 1kg

if($qty<0.005 && $qty>=0.0001)
$qty=0.005;
//$y	=((round($qty,3)*1000)%100);
$y	= ((floor($qty * 1000) / 1000)*1000)%100;
$z	=ceil($y/10)*10;
$a	=$z-$y;

if($a > 5)
$val1=0;
else if($a>0)
$val1=5;
else
$val1	=0;

$val2 	=  floor((floor($qty * 1000) / 1000)*100)/100;
$val	= $val2+$val1/1000;

}
else
$val	=$qty;
//echo $qty.'/'.$y.'/'.$z.'/'.$a.'/'.$val1.'/'.$val2.'/'.$val.'</br>';
return $val;
}
	
	
function get_split_orders($orderNo,$year,$so){
	
	global $db;	
    $sql = "SELECT
			trn_orderdetails.intOrderNo,
			trn_orderdetails.intOrderYear,
			trn_orderdetails.intSalesOrderId
			FROM
			trn_orderdetails
			INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
			WHERE
			trn_orderheader.DUMMY_PO_NO = '$orderNo' AND
			trn_orderheader.DUMMY_PO_YEAR = '$year' AND
			trn_orderdetails.DUMMY_SO_ID = '$so'
			";
    $result = $db->RunQuery($sql);
	return $result;
	
}
	
	

?>	
<?php
if($intStatus>1)//pending
{
$count	=$count;
	$top=17+$count*9+$count1;
	//$top=17;
	if($intUser==2)
		echo $count.'/'.$count1;
?>
<div id="apDiv1" style="margin-top: <?php echo $top; ?>%"><img src="images/pending.png"  /></div>
<?php
}
?>

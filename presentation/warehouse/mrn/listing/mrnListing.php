<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
date_default_timezone_set('Asia/Colombo');

$thisFilePath 		=  $_SERVER['PHP_SELF'];

require_once	"libraries/jqgrid2/inc/jqgrid_dist.php";
require_once $backwardseperator."class/cls_permisions.php";

$objpermisionget= new cls_permisions($db);

$company 	= $_SESSION['headCompanyId'];
$location = $_SESSION['CompanyID'];

$programName='Material Request Note';
$programCode='P0229';

$reportMenuId	='938';
$menuId			='229';

$intUser  = $_SESSION["userId"];

$userDepartment=getUserDepartment($intUser);
$approveLevel = (int)getMaxApproveLevel();

$option=1;
$option=getOption($objpermisionget);

$strOption3=getOption3ComboValues();
$strOption4=getOption4ComboValues();
$strOption5=getLocationsComboValues();

################################################################
################## new part ####################################
################################################################
///// create where part ///////////////////
$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];
//print_r($arr);
$where_string = '';
$where_array = array(
				'Status'=>'tb1.intStatus',
				'MRN_No'=>'tb1.intMrnNo',
				'MRN_Year'=>'tb1.intMrnYear',
				'ORDER_NO'=>'ware_mrndetails.intOrderNo',
                'GRAPHIC_NO'=>'trn_orderdetails.strGraphicNo',
				'MRN_Type'=>'tb1.mrnType',
				'Location'=>'mst_locations.strName',
				'Department'=>'mst_department.strName',
				'Date'=>'substr(tb1.datdate,1,10)',
				'User'=>'sys_users.strUserName',
				'Material_Code'=>'mst_item.strCode'
				);
$arr_status = array('Approved'=>'1','Rejected'=>'0','Pending'=>'2','Cancel'=>'-2');
//$arr_type = array('Production_Orderwise'=>'3','Production_Non_Orderwise'=>'5','Production_Orderwise_Additional'=>'6','Technical_R&D'=>'2','Sample'=>'1','Non_RM'=>'0');
$arr_type = array('Loan out to Supplier'=>'8','Loan settlement to Supplier'=>'7','Production Orderwise'=>'3','Production Non Orderwise'=>'5','Production Orderwise Additional'=>'6','Technical R&D'=>'2','Sample'=>'1','Non RM'=>'0');

//----------year-----------------------
$str_year = '';
for ($y = date("Y"); $y >= 2018; $y--) {
    $str_year .= $y . ":" . $y . ";";
}
$str_year .= ":All";

foreach($arr as $k=>$v)
{
	if($v['field']=='Status')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($v['field']=='MRN_Type')
	{
		//if($arr_type[$v['data']]==3)
			//$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		//else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_type[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}
if(!count($arr)>0){
	$where_string .= "AND substr(tb1.datdate,1,10) = '".date('Y-m-d')."'";
	$where_string .= "AND (tb1.mrnType) < 7 ";
}

################## end code ####################################


 $sql = "select * from (SELECT DISTINCT 
							if(tb1.intStatus=1,'Approved',if(tb1.intStatus=0,'Rejected',if(tb1.intStatus=-2,'Cancel','Pending'))) as Status,
							tb1.intMrnNo as `MRN_No`,
							tb1.intMrnYear as `MRN_Year`,
							trn_orderdetails.strGraphicNo as `GRAPHIC_NO`,
if(tb1.mrnType=7,'Loan settlement to Supplier',(if(tb1.mrnType=8,'Loan out to Supplier','Others'))) as Main_type,							if(tb1.mrnType=3,'Production Orderwise',if(tb1.mrnType=2,'Technical',if(tb1.mrnType=1,'Sample',if(tb1.mrnType=5,'Production Non Orderwise',if(tb1.mrnType=6,'Production Orderwise Additional',if(tb1.mrnType=7,'Loan settlement to Supplier',if(tb1.mrnType=8,'Loan out to Supplier','Non RM'))))))) as `MRN_Type`,
/*							if(tb1.mrnType=3,'Production Orderwise',if(tb1.mrnType=2,'Technical',if(tb1.mrnType=1,'Sample',if(tb1.mrnType=5,'Production Non Orderwise',if(tb1.mrnType=6,'Production Orderwise Additional','Non_RM'))))) as `MRN_Type`,*/
							
							(
							SELECT 
							IF (
								sum(ware_mrndetails.EXTRA_QTY) > 0,
								'Extra QTY',
								'MRN QTY'
							)
							FROM
								ware_mrndetails
							WHERE
								tb1.intMrnNo = ware_mrndetails.intMrnNo
							AND tb1.intMrnYear = ware_mrndetails.intMrnYear
							) AS QTY_Type,
							
							(
							SELECT
							GROUP_CONCAT(DISTINCT CONCAT(IF(ware_mrndetails.intOrderNo=0,'',ware_mrndetails.intOrderNo),IF(ware_mrndetails.intOrderNo=0,'','/'),IF(ware_mrndetails.intOrderYear=0,'',ware_mrndetails.intOrderYear)))
							FROM
								ware_mrndetails
							WHERE
								tb1.intMrnNo = ware_mrndetails.intMrnNo
							AND tb1.intMrnYear = ware_mrndetails.intMrnYear
							) AS ORDER_NO,

							mst_locations.strName as `Location`,
							mst_department.strName as `Department`,
							substr(tb1.datdate,1,10) as `Date`,
							sys_users.strUserName as `Create_User`, 
							  
							IFNULL((
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(ware_mrnheader_approvedby.dtApprovedDate),')' )
								FROM
								ware_mrnheader_approvedby
								Inner Join sys_users ON ware_mrnheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_mrnheader_approvedby.intMrnNo  = tb1.intMrnNo AND
								ware_mrnheader_approvedby.intYear =  tb1.intMrnYear AND
								ware_mrnheader_approvedby.intApproveLevelNo =  '1' AND 
							    ware_mrnheader_approvedby.intStatus='0'  
							),IF(((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.intStatus>1),'Approve', '')) as `1st_Approval`,  ";

						for($i=2; $i<=$approveLevel; $i++){

							if($i==2){
							$approval="2nd_Approval";
							}
							else if($i==3){
							$approval="3rd_Approval";
							}
							else {
							$approval=$i."th_Approval";
							}


						$sql .= "IFNULL(
						/*condition*/
(
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(ware_mrnheader_approvedby.dtApprovedDate),')' )
								FROM
								ware_mrnheader_approvedby
								Inner Join sys_users ON ware_mrnheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_mrnheader_approvedby.intMrnNo  = tb1.intMrnNo AND
								ware_mrnheader_approvedby.intYear =  tb1.intMrnYear AND
								ware_mrnheader_approvedby.intApproveLevelNo =  '$i' AND 
							    ware_mrnheader_approvedby.intStatus='0'  
							),
							/*end condition*/
							
							/*false part*/

							IF(
							/*if condition */
							((SELECT
								menupermision.int".$i."Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND (tb1.intStatus>1) AND (tb1.intStatus<=tb1.intApproveLevels) AND ((SELECT
								max(ware_mrnheader_approvedby.dtApprovedDate) 
								FROM
								ware_mrnheader_approvedby
								Inner Join sys_users ON ware_mrnheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_mrnheader_approvedby.intMrnNo  = tb1.intMrnNo AND
								ware_mrnheader_approvedby.intYear =  tb1.intMrnYear AND
								ware_mrnheader_approvedby.intApproveLevelNo =  ($i-1) AND 
							    ware_mrnheader_approvedby.intStatus='0'  )<>'')),
								
								/*end if condition*/
								/*if true part*/
								'Approve',
								/*fase part*/
								 if($i>tb1.intApproveLevels,'-----',''))
								
								/*end IFNULL false part*/
								) as `".$approval."`, ";

								}


						$sql .= "IFNULL((
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(ware_mrnheader_approvedby.dtApprovedDate),')' )
								FROM
								ware_mrnheader_approvedby
								Inner Join sys_users ON ware_mrnheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_mrnheader_approvedby.intMrnNo  = tb1.intMrnNo AND
								ware_mrnheader_approvedby.intYear =  tb1.intMrnYear AND
								ware_mrnheader_approvedby.intApproveLevelNo =  '0' AND 
							    ware_mrnheader_approvedby.intStatus='0' AND 
								tb1.intStatus<>1
							),'') as `RejectedBy` , ";


							$sql .= "
							(SELECT
							IF(Sum(ware_mrndetails.dblIssudQty)='0','None',IF(Sum(ware_mrndetails.dblQty)=Sum(ware_mrndetails.dblIssudQty),'Totally Issued','Partially Issued'))
							FROM
							ware_mrndetails
							WHERE
							tb1.intMrnNo = ware_mrndetails.intMrnNo AND tb1.intMrnYear = ware_mrndetails.intMrnYear
							) as Issue_Progress, 							
							(
							SELECT
							GROUP_CONCAT(mst_item.strCode)
							FROM
								mst_item
							INNER JOIN ware_mrndetails ON mst_item.intId =  ware_mrndetails.intItemId
							WHERE
							tb1.intMrnNo = ware_mrndetails.intMrnNo
							AND tb1.intMrnYear = ware_mrndetails.intMrnYear
							) AS Material_Code,
							
							'View' as `View`   
							
							FROM
							
							ware_mrnheader as tb1
							Inner Join sys_users ON tb1.intUser = sys_users.intUserId 
							Inner Join mst_locations ON tb1.intCompanyId = mst_locations.intId
							Inner Join mst_department ON tb1.intDepartment = mst_department.intId
							INNER JOIN ware_mrndetails ON tb1.intMrnNo = ware_mrndetails.intMrnNo AND tb1.intMrnYear = ware_mrndetails.intMrnYear
							LEFT JOIN trn_orderdetails ON trn_orderdetails.intOrderNo = ware_mrndetails.intOrderNo AND 
							trn_orderdetails.intOrderYear = ware_mrndetails.intOrderYear AND ware_mrndetails.strStyleNo = trn_orderdetails.intSalesOrderId
							INNER JOIN mst_item ON mst_item.intId =  ware_mrndetails.intItemId
							WHERE  tb1.intCompanyId = '$location'  ";
							if($option==3)//all loging user  department's mrns
							$sql .= " AND tb1.intCompanyId = '$location'" ;
							else if($option==2)//all loging user  department's mrns
							$sql .= " AND sys_users.intDepartmentId = '$userDepartment'" ;
							else if($option==1)//only loging user's mrns
							$sql .= " AND tb1.intUser = '$intUser'" ;

							$sql .=$where_string;


							$sql .= "  )  as t where 1=1
						";


$formLink			= "?q=$menuId&mrnNo={MRN_No}&year={MRN_Year}";
$reportLink  		= "?q=$reportMenuId&mrnNo={MRN_No}&year={MRN_Year}&mrnType={MRN_Type}";
$reportLinkApprove  = "?q=$reportMenuId&mrnNo={MRN_No}&year={MRN_Year}&mrnType={MRN_Type}&approveMode=1";
$reportLinkPrint  	= "?q=$reportMenuId&mrnNo={MRN_No}&year={MRN_Year}&mode=print";

$col = array();
$jq = new jqgrid();

//STATUS
$col["title"] 	= "Status"; // caption of column
$col["name"] 	= "Status"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] 	= "3";
//edittype
$col["stype"] 	= "select";
$str = ":All;Pending:Pending;Approved:Approved;Rejected:Rejected;Cancel:Cancel" ;
$col["editoptions"] 	=  array("value"=> $str);
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist

$cols[] = $col;	$col=NULL;
//MRN No
$col["title"] 	= "MRN No"; // caption of column
$col["name"] 	= "MRN_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] 	= "2";
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$col['link']	= $formLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag

$cols[] = $col;	$col=NULL;


//MRN Year
$col["title"] = "MRN Year"; // caption of column
$col["name"] = "MRN_Year"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "2";
$col["stype"] 	= "select";
$str_year = $str_year;
$col["editoptions"] = array("value" => $str_year);
//searchOper
$col["align"] 	= "center";
$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "Order No"; // caption of column
$col["name"] = "ORDER_NO"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Order No
$col["title"] = "Graphic No"; // caption of column
$col["name"] = "GRAPHIC_NO"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//MRN Type
//STATUS
$col["title"] 	= "Main Type"; // caption of column
$col["name"] 	= "Main_type"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] 	= "3";
//edittype
$col["stype"] 	= "select";
//$str = ":All;Production_Orderwise:Production_Orderwise;Production_Non_Orderwise:Production_Non_Orderwise;Production_Orderwise_Additional:Production_Orderwise_Additional;Technical:Technical;Sample:Sample;Non_RM:Non_RM" ;
$str = "Others:Others;Loan settlement to Supplier:Loan settlement to Supplier;Loan out to Supplier:Loan out to Supplier;:All" ;
$col["editoptions"] 	=  array("value"=> $str);
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$cols[] = $col;	$col=NULL;

//MRN Type
//STATUS
$col["title"] 	= "MRN Type"; // caption of column
$col["name"] 	= "MRN_Type"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] 	= "3";
//edittype
$col["stype"] 	= "select";
//$str = ":All;Production_Orderwise:Production_Orderwise;Production_Non_Orderwise:Production_Non_Orderwise;Production_Orderwise_Additional:Production_Orderwise_Additional;Technical:Technical;Sample:Sample;Non_RM:Non_RM" ;
$str = ":All;Production Orderwise:Production Orderwise;Production Non Orderwise:Production Non Orderwise;Production Orderwise Additional:Production Orderwise Additional;Technical:Technical;Sample:Sample;Non RM:Non RM;Loan settlement to Supplier:Loan settlement to Supplier;Loan out to Supplier:Loan out to Supplier" ;
$col["editoptions"] 	=  array("value"=> $str);
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$cols[] = $col;	$col=NULL;

//Extre Qty Type
//STATUS
$col["title"] 	= "QTY Type"; // caption of column
$col["name"] 	= "QTY_Type"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] 	= "3";
//edittype
$col["stype"] 	= "select";
$str = ":All;MRN QTY:MRN QTY;Extra QTY:Extra QTY";
$col["editoptions"] 	=  array("value"=> $str);
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$cols[] = $col;	$col=NULL;

//Location
$col["title"] = "Location"; // caption of column
$col["name"] = "Location"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
if($option==3)//all in loging user  department's mrns
{
$col["stype"] 	= "select";
$col["editoptions"] 	=  array("value"=> $strOption5);
}
$col["width"] = "4";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Department
$col["title"] = "Department"; // caption of column
$col["name"] = "Department"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
if($option==3)//all in loging user  department's mrns
{
$col["stype"] 	= "select";
$col["editoptions"] 	=  array("value"=> $strOption3);
}
$col["width"] = "4";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//DATE
$col["title"] = "Date"; // caption of column
$col["name"] = "Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["index"] = "Date";
$col["width"] = "3";
$col["align"] = "center";
$col["sorttype"] = "date";
$cols[] = $col;	$col=NULL;

//TO DATE
/*$col["title"] = "ToDate"; // caption of column
$col["name"] = "ToDate"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["index"] = "ToDate";
$col["width"] = "3";
$col["align"] = "center";
$col["formatter"] = "date";
$strEditOption = "dataInit:function(o){link_dtpicker(o);}" ;
$col["editoptions"] 	=  array("value"=> $strEditOption);
$cols[] = $col;	$col=NULL;*/

//Create User
$col["title"] = "Create User"; // caption of column
$col["name"] = "Create_User"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;

//Issue Status
$col["title"] = "Issue Status"; // caption of column
$col["name"] = "Issue_Progress"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["stype"] 	= "select";
$str = ":All;None:None;Totally Issued:Totally Issued;Partially Issued:Partially Issued" ;
$col["editoptions"] 	=  array("value"=> $str);
$col["align"] = "right";
$cols[] = $col;	$col=NULL;

//Material Code
$col["title"] = "Material Code"; // caption of column
$col["name"] = "Material_Code"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
/*if($option==3)//all in loging user  department's mrns
{
$col["stype"] 	= "select";
$col["editoptions"] 	=  array("value"=> $strOption4);
}*/
$col["width"] = "5";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//FIRST APPROVAL
/*$col["title"] = "1st Approval"; // caption of column
$col["name"] = "1st_Approval"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "5";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;*/

for($i=1; $i<=$approveLevel; $i++){
	if($i==1){
	$ap="1st Approval";
	$ap1="1st_Approval";
	}
	else if($i==2){
	$ap="2nd Approval";
	$ap1="2nd_Approval";
	}
	else if($i==3){
	$ap="3rd Approval";
	$ap1="3rd_Approval";
	}
	else {
	$ap=$i."th Approval";
	$ap1=$i."th_Approval";
	}
//SECOND APPROVAL
$col["title"] = $ap; // caption of column
$col["name"] = $ap1; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "5";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;
}

$col["title"] = "Rejected By"; // caption of column
$col["name"] = "RejectedBy"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "5";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

//VIEW
$col["title"] = "Report"; // caption of column
$col["name"] = "View"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias)
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;



$grid["caption"] 		= "MRN Listing";
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'MRN_No'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes

$grid["export"]["range"] = "filtered"; // or "all"
// initialize search, 'name' field equal to (eq) 'Client 1'
/*$sarr = <<< SEARCH_JSON
{
	"groupOp":"AND",
    "rules":[
      {"field":"Status","op":"eq","data":"Pending"}
     ]
}
SEARCH_JSON;

$grid["search"] = true;
$grid["postData"] = array("filters" => $sarr ); */

// export XLS file
// export to excel parameters - range could be "all" or "filtered"
//$grid["export"] = array("format"=>"xlsx", "filename"=>"my-file", "sheetname"=>"test");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

// params are array(<function-name>,<class-object> or <null-if-global-func>,<continue-default-operation>)
$e["on_render_excel"] = array("custom_excel_layout", null, true);
$jq->set_events($e);


function custom_excel_layout($param)
{
	$grid = $param["grid"];
	$xls = $param["xls"];
	$arr = $param["data"];

	$xls->addHeader(array('MRN LISTING'));
	$xls->addHeader(array());
	$xls->addHeader($arr[0]);
	$summary_arr = array(0);

	for($i=1;$i<count($arr);$i++)
	{
		$xls->addRow($arr[$i]);
		$summary_arr[0] += $arr[$i]["id"];
	}
	//$xls->addRow($summary_arr);
	return $xls;
}


$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
)
);



$out = $jq->render("list1");
?>
<title>MRN Listing</title>

<form id="frmlisting" name="frmlisting" method="post" action="">

        <div align="center" style="margin:10px">
			<?php echo $out?>
        </div>
</form>
<?php

//------------------------------function load Default Department---------------------
function getMaxApproveLevel(){
	global $db;

	//echo $savedStat;
	$appLevel=0;
	  $sqlp = "SELECT
			Max(ware_mrnheader.intApproveLevels) AS appLevel
			FROM ware_mrnheader";

		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);

	return $rowp['appLevel'];
}
//------------------------------function load User Department---------------------
function getUserDepartment($intUser){
	global $db;

	$sqlp = "SELECT
			sys_users.intDepartmentId
			FROM
			sys_users
			WHERE
			sys_users.intUserId =  '$intUser'";

		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);

	return $rowp['intDepartmentId'];

}

//------------------------------function load User Department---------------------
function getOption3ComboValues(){
	global $db;

	$sql = "SELECT
			mst_department.intId,
			mst_department.strName
			FROM
			mst_department
			WHERE
			mst_department.intStatus =  '1'";
	$result = $db->RunQuery($sql);
	$strOption3 = ":All";
	while($row=mysqli_fetch_array($result))
	{
		$strOption3 .= ";".$row['strName'].":".$row['strName'] ;
	}

	return $strOption3;

}
//------------------------------function load User Department---------------------
function getOption($objpermisionget){

	$option2 	= $objpermisionget->boolSPermision(3);
	$option3 	= $objpermisionget->boolSPermision(4);

	if($option3)
	$option=3;
	else if($option2)
	$option=2;
	else
	$option=1;

	return $option;


}
//------------------------------function load Material Codes---------------------
function getOption4ComboValues(){
	global $db;

	$sql = "SELECT
			mst_item.intId,
			mst_item.strCode
			FROM
			mst_item";
	$result = $db->RunQuery($sql);
	$strOption4 = ":All";
	while($row=mysqli_fetch_array($result))
	{
		$strOption4 .= ";".$row['strCode'].":".$row['strCode'] ;
	}

	return $strOption4;

}
//------------------------------function load Location---------------------
function getLocationsComboValues(){
	global $db;

	$sql = "SELECT
			mst_locations.intId,
			mst_locations.strName
			FROM
			mst_locations";
	$result = $db->RunQuery($sql);
	$strOption5 = ":All";
	while($row=mysqli_fetch_array($result))
	{
		$strOption5 .= ";".$row['strName'].":".$row['strName'] ;
	}

	return $strOption5;

}
?>


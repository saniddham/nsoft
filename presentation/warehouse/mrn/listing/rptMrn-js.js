//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
// JavaScript Document
var basePath = "presentation/warehouse/mrn/listing/";

$(document).ready(function() {
	$('#frmMrnReport').validationEngine();
	
	$('#frmMrnReport #imgApprove').click(function(){
		
	/*var val = $.prompt('Are you sure you want to approve this MRN ?',{
				buttons: { Ok: true, Cancel: false }, */
        var ApproveState = {
            state0: {
                html: '<label>Are you sure you want to approve this MRN ?</label></br><label>Enter the Reason : <textarea name="txtReason" id="txtReason" cols="50" rows="3"></textarea></label><br />',
                buttons: {Ok: true, Cancel: false},
                persistent: true,
                submit: function (v, m, f) {
                    if (v) {
                        showWaiting();
                        if (validateQuantities() == 0) {

                            var reason 	= $('#txtReason').val();

                            var arrHeader = "{";
                            arrHeader += '"reason":'+URLEncode_json(reason)+'';
                            arrHeader += "}";

                            var url = basePath + "rptMrn-db-set.php" + window.location.search + '&status=approve';
                            var obj = $.ajax({
                                url: url,
                                type: 'post',
                                dataType: "json",
                                data: window.location.search+'&status=approve&arrHeader='+arrHeader,
                                async: false,

                                success: function (json) {
                                    $('#frmMrnReport #imgApprove').validationEngine('showPrompt', json.msg, json.type /*'pass'*/);
                                    if (json.type == 'pass') {
                                        var t = setTimeout("alertx()", 1000);
                                        window.location.href = window.location.href;
                                        window.opener.location.reload();//reload listing page
                                        return;
                                    }
                                },
                                error: function (xhr, status) {

                                    $('#frmMrnReport #imgApprove').validationEngine('showPrompt', errormsg(xhr.status), 'fail');
                                    var t = setTimeout("alertx()", 3000);
                                }
                            });
                            ////////////
                        }
                        hideWaiting();
                    }
                }
            }
    };
        $.prompt(ApproveState);
	});

    $('#frmMrnReport #butSave_rpt').click(function(){
        var val = $.prompt('Are you sure you want to save this MRN ?',{
            buttons: { Ok: true, Cancel: false },
            callback: function(v,m,f){
                if(v)
                {
                    showWaiting();

                    var arrHeader = "{";
                    arrHeader += '"MRN_No":"'+$('#rptmrnNo').text()+'"'
                    arrHeader += ' }';

                    var arr="[";
                    $('#tblMainGrid #dataRowRpt').each(function(){
                        var orderNo = $(this).find("#rptOrderNo").text();
                        var strStyleNo = $(this).find(".StrStyle").attr('id');
                        var itemId = $(this).find(".itemIdRpt").attr('id');
                        var extraQty = $(this).find("#rptExtra").text();
                        var totalQty = $(this).find("#rptOrderQty").val();
                        var mrnType = $(this).find(".mrnType").attr('id');
                            arr += "{";
                            arr += '"orderNo":"'+orderNo +'",' ;
                            arr += '"strStyleNo":"'+strStyleNo +'",' ;
                            arr += '"itemId":"'+itemId +'",' ;
                            arr += '"extraQty":"'+extraQty +'",' ;
                            arr += '"totalQty":"'+totalQty +'",' ;
                            arr += '"mrnType":"'+mrnType+'"' ;
                            arr +=  '},';
                    });
                    arr = arr.substr(0,arr.length-1);
                    arr += "]";
                    data = "&arrHeader="+arrHeader;
                    data += "&arr="+arr;
                    if(validateQuantities()==0){
                        var url = basePath+"rptMrn-db-set.php"+window.location.search+'&status=update';
                        var obj = $.ajax({
                            url:url,
                            type:'post',
                            dataType: "json",
                            data:data,
                            async:false,

                            success:function(json){
                                $('#frmMrnReport #imgApprove').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
                                hideWaiting();
                                if(json.type=='pass')
                                {
                                    var t=setTimeout("alertx()",1000);
                                    window.location.href = window.location.href;
                                    window.opener.location.reload();//reload listing page
                                    return;
                                }
                            },
                            error:function(xhr,status){

                                $('#frmMrnReport #imgApprove').validationEngine('showPrompt', errormsg(xhr.status),'fail');
                                var t=setTimeout("alertx()",3000);
                            }
                        });
                    }
                    hideWaiting();
                }
            }});
    });

    $("#tblMainGrid #rptOrderQty").die('keyup').live('keyup',function(){
        changeExtaQty(this);
    });

	$('#frmMrnReport  #imgReject').click(function(){
	
    var ApproveState = {
    state0: {
    html: '<label>Are you sure you want to reject this MRN ?</label></br><label>Enter the Reason : <textarea name="txtReason" id="txtReason" cols="50" rows="3"></textarea></label><br />',
    buttons: {Ok: true, Cancel: false},
    persistent: true,
    submit: function (v, m, f) {
    if (v) {
    showWaiting();
    if (validateQuantities() == 0) {

    var reason 	= $('#txtReason').val();
    var arrHeader = "{";
    arrHeader += '"reason":'+URLEncode_json(reason)+'';
    arrHeader += "}";

    var url = basePath + "rptMrn-db-set.php" + window.location.search + '&status=reject';
    var obj = $.ajax({
    url: url,
    type: 'post',
    dataType: "json",
    data: window.location.search+'&status=reject&arrHeader='+arrHeader,
    async: false,
                              
     success: function (json) {
     $('#frmMrnReport  #imgReject').validationEngine('showPrompt', json.msg, json.type /*'pass'*/);
									
     if (json.type == 'pass') {
										
      var t = setTimeout("alertx()", 1000);
      window.location.href = window.location.href;
      window.opener.location.reload();//reload listing page
      return;
      }
      },
      error: function (xhr, status) {

      $('#frmMrnReport  #imgReject').validationEngine('showPrompt', errormsg(xhr.status), 'fail');
      var t = setTimeout("alertx()", 3000);
       }
       });
                            ////////////
       }
      hideWaiting();
       }
       }
       }
    };
        $.prompt(ApproveState);
	});
	});
//---------------------------------------------------
function validateQuantities(){
		var ret=0;
	    var mrnNo = document.getElementById('divMrnNo').innerHTML;
		var url 		= basePath+"rptMrn-db-get.php?requestType=getValidation";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"mrnNo="+mrnNo,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#imgApprove').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					var t=setTimeout("alertx()",3000);
					  ret= 1;
					}
				}
			});
			return ret;
}
//---------------------------------------------------
function validateRejecton(){
		var ret=0;
	    var mrnNo = document.getElementById('divMrnNo').innerHTML;
		var url 		= basePath+"rptMrn-db-get.php?requestType=validateRejecton";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"mrnNo="+mrnNo,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#imgReject').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertx()",3000);
					  ret= 1;
					}
				}
			});
			return ret;

}
//------------------------------------------------
function alertx() {
	$('#frmMrnReport #imgApprove').validationEngine('hide')	;
}
//------------------------------------------------
function changeExtaQty(e) {
    var userEnterQty = parseFloat(e.value);
    var balanceQty = parseFloat($(e).closest("tr").find(".actualPriQty").attr('id'));
    var dblLastPrice = parseFloat($(e).closest('tr').find('#rptdblprice').text());
	var mrnType = parseInt($(e).closest("tr").find(".mrnType").attr('id'));
    if(isNaN(userEnterQty)){
        userEnterQty = 0;
    }
    if(userEnterQty >= balanceQty && (mrnType == 3)){
        if(userEnterQty-balanceQty > 0 ){
            $(e).closest('tr').find('#rptExtra').html((userEnterQty-balanceQty).toFixed(4));
        }else{
            $(e).closest('tr').find('#rptExtra').html(0);
        }
        $(e).closest("tr").find("#rptOrderQtyWithoutExtra").html(parseFloat(balanceQty).toFixed(4));
    }
    else if(userEnterQty < balanceQty && (mrnType == 3)){
        $(e).closest("tr").find("#rptExtra").html("0");
        $(e).closest("tr").find("#rptOrderQtyWithoutExtra").html(userEnterQty);
       }
    var newAmount = userEnterQty*dblLastPrice;
    $(e).closest('tr').find('#rptAmount').html(newAmount.toFixed(4));
}
//------------------------------------------------

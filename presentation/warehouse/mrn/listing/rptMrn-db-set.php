<?php
ini_set('display_errors',0);
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
session_start();
$backwardseperator 	= "../../../../";
$mainPath 			= $_SESSION['mainPath'];

$userId 			= $_SESSION['userId'];
$location 			= $_SESSION['CompanyID'];
$company 			= $_SESSION['headCompanyId'];
$programName		='Material Request Note';
$programCode		='P0229';

include_once 		"{$backwardseperator}dataAccess/Connector.php";
include_once 		"{$backwardseperator}libraries/mail/mail.php";
require_once 		$backwardseperator."class/cls_mail.php";
require_once 		"../../../../class/warehouse/mrn/cls_mrn_get.php";
require_once "../../../../class/cls_commonFunctions_get.php";
require_once "../../../../class/finance/budget/cls_common_function_budget_get.php";
require_once "../../../../class/finance/budget/entering/cls_budget_set.php";

$extra_percentage = getExtraPercentage($company);

$objMail 				= new cls_create_mail($db);
$obj_mrn_get			= new cls_mrn_get($db);
$obj_common				= new cls_commonFunctions_get($db);
$obj_comm_budget_get	= new cls_common_function_budget_get($db);
$obj_budget_set			= new cls_budget_set($db);

$mrnApproveLevel 		= (int)getApproveLevel($programName);

$mrnNo 					= $_REQUEST['mrnNo'];
$year 					= $_REQUEST['year'];
$mrnType 				= $_REQUEST['mrnType'];

//-------APPROVE MRN----------------------------------------
if($_REQUEST['status']=='approve')
{
    $db->OpenConnection();
    $db->RunQuery2('Begin');

    $arrHeader		= json_decode($_REQUEST['arrHeader'], true);
    $reject_reason	= $obj_common->replace($arrHeader['reason']);
    
    $rollBackFlag=0;
    $rollBackMsg = '';
	
	$sql="SELECT
		ware_mrnheader.mrnType
		FROM
		ware_mrnheader
		WHERE
		ware_mrnheader.intMrnNo = '$mrnNo' AND
		ware_mrnheader.intMrnYear = '$year'";
	$result = $db->RunQuery2($sql);
    $rowm=mysqli_fetch_array($result);
	$mrnType   = $rowm['mrnType'];


    if($mrnType == 'Production_Orderwise' || $mrnType == 3 || $mrnType == 'Production_Orderwise_Additional' || $mrnType == 6){
        $sql_1 = "SELECT 
						trn_orderheader.intOrderNo,
						trn_orderheader.intOrderYear,
						trn_orderheader.intStatus,
						trn_orderheader.intApproveLevelStart,
						trn_orderheader.PO_TYPE,
						trn_orderheader.INSTANT_ORDER,
						ware_mrnheader.mrnType 
					FROM trn_orderheader 
					INNER JOIN ware_mrndetails ON trn_orderheader.intOrderNo = ware_mrndetails.intOrderNo AND
						trn_orderheader.intOrderYear = ware_mrndetails.intOrderYear
					INNER JOIN ware_mrnheader ON ware_mrndetails.intMrnNo = ware_mrnheader.intMrnNo AND ware_mrndetails.intMrnYear = ware_mrnheader.intMrnYear

					WHERE
						ware_mrndetails.intMrnNo = '$mrnNo'
					AND ware_mrndetails.intMrnYear = '$year'";
        $result_1 = $db->RunQuery2($sql_1);
        while($row=mysqli_fetch_array($result_1))
        {
            $intOrderNo   = $row['intOrderNo'];
            $intOrderYear = $row['intOrderYear'];
            $intStatus = $row['intStatus'];
            $intApproveLevelStart = $row['intApproveLevelStart'];
            if($intStatus <= 0 || ($intStatus>=$intApproveLevelStart+1))
            {
                $rollBackFlag=1;
                $sqlM=$sql;
                $rollBackMsg = "Approval error! Unapproved Order.";
            }
            else if($row['PO_TYPE']==1 && $row['INSTANT_ORDER']!=1)
            {
                $rollBackFlag=1;
                $sqlM=$sql;
                $rollBackMsg = "Can't raise MRN for non-instant, Pre-Costing orders.";
            }
        }
    }
    // check blanace to MRN
    if($rollBackFlag!=1){
        $arr_resp=checkBalanceQty($mrnNo,$year);
        $rollBackFlag = $arr_resp['rollBackFlag'];
        $rollBackMsg  = $arr_resp['rollBackMsg'];
    }

    $sql_Status = "SELECT ware_mrnheader.intUser, ware_mrnheader.intStatus, ware_mrnheader.intApproveLevels FROM ware_mrnheader WHERE (intMrnNo='$mrnNo') AND (`intMrnYear`='$year')";
    $results_Status = $db->RunQuery2($sql_Status);
    $row = mysqli_fetch_array($results_Status);
    $status_remark = $row['intStatus'];
    $savedLevels_remark = $row['intApproveLevels'];
    $approveLevel_Remark = $savedLevels_remark + 1 - $status_remark;
    if($reject_reason != '') {
        $reject_reason2 = 'Level '.'Level-'. ($approveLevel_Remark + 1) . ' -: ' . $reject_reason . "\r\n";
    }
    $sql = "UPDATE `ware_mrnheader` SET `intStatus`=intStatus-1, `APPROVAL_REMARKS` = CONCAT(IFNULL(`APPROVAL_REMARKS`,''),'$reject_reason2') WHERE (intMrnNo='$mrnNo') AND (`intMrnYear`='$year') and intStatus not in(0,1)";
    $result = $db->RunQuery2($sql);
    if((!$result) && ($rollBackFlag!=1)){
        $rollBackFlag=1;
        $sqlM=$sql;
        $rollBackMsg = "Approval error!";
    }
    //-----2014-05-08-----BUDGET VALIDATION-------------------------------------------
    $arr_mrn_item_tot_U		= NULL;
    $arr_mrn_sub_cat_tot_U	= NULL;
    $arr_mrn_item_tot_A		= NULL;
    $arr_mrn_sub_cat_tot_A	= NULL;

    if($rollBackFlag != 1){
        $header_array		=$obj_mrn_get->get_header($mrnNo,$year,'RunQuery2');
        $results_details	=$obj_mrn_get->get_details_results($mrnNo,$year,'RunQuery2');
        $results_fin_year	=$obj_comm_budget_get->loadFinanceYear_with_selected_date($company,$header_array['DATE'],'RunQuery2');
        $row_fin_year		=mysqli_fetch_array($results_fin_year);
        $budg_year			=$row_fin_year['intId'];

        while($row_details	=mysqli_fetch_array($results_details)){
            $item_price_mrn		= $obj_comm_budget_get->getItemPrice($location,$row_details['ITEM_ID'],$row_details['QTY'],'RunQuery2');
            if($item_price_mrn <=0 || $item_price_mrn =='')
                $item_price_mrn			= $obj_comm_budget_get->get_price_for_mrn_item($location,$row_details['ITEM_ID'],'RunQuery2');
            $sub_cat_budget_flag	= $obj_comm_budget_get->check_sub_category_budgeted_or_not($location,$row_details['SUB_CATEGORY_ID'],'RunQuery2');

            if($sub_cat_budget_flag	==0){	//item wise budget
                $budget_type			= $obj_comm_budget_get->load_budget_type($location,$row_details['SUB_CATEGORY_ID'],$row_details['ITEM_ID'],'RunQuery2');
                if($budget_type == 'U')	//unit budget
                    $arr_mrn_item_tot_U[$row_details['ITEM_ID']]	+= $row_details['QTY'];
                else if($budget_type == 'A'){//amount budget
                    if($item_price_mrn <= 0 || $item_price_mrn ==''  ){
                        $rollBackMsg 	= "There is no price for ".$obj_common->get_item_name($row_details['ITEM_ID'],'RunQuery2');
                        $rollBackFlag	=1;
                    }
                    $arr_mrn_item_tot_A[$row_details['ITEM_ID']]	+= ($row_details['QTY'])*$item_price_mrn;
                }

            }
            else if($sub_cat_budget_flag	==1){//sub category wise budget
                $budget_type			= $obj_comm_budget_get->load_budget_type($location,$row_details['SUB_CATEGORY_ID'],0,'RunQuery2');
                if($budget_type == 'U')//unit budget
                    $arr_mrn_sub_cat_tot_U[$row_details['SUB_CATEGORY_ID']]	+= $row_details['QTY'];
                else if($budget_type == 'A'){//amount budget
                    if($item_price_mrn <= 0 || $item_price_mrn ==''  ){
                        $rollBackMsg 	= "There is no price for ".$obj_common->get_item_name($row_details['ITEM_ID'],'RunQuery2');
                        $rollBackFlag	=1;
                    }
                    $arr_mrn_sub_cat_tot_A[$row_details['SUB_CATEGORY_ID']]	+= ($row_details['QTY'])*$item_price_mrn;
                }
            }
        }
    }
    //print_r($arr_mrn_sub_cat_tot_A);


    $arr_mrn_item_tot_U_1		= NULL;
    $arr_mrn_sub_cat_tot_U_1	= NULL;
    $arr_mrn_item_tot_A_1		= NULL;
    $arr_mrn_sub_cat_tot_A_1	= NULL;
    $arr_mrn_item_tot_U_1		= $arr_mrn_item_tot_U;
    $arr_mrn_sub_cat_tot_U_1	= $arr_mrn_sub_cat_tot_U;
    $arr_mrn_item_tot_A_1		= $arr_mrn_item_tot_A;
    $arr_mrn_sub_cat_tot_A_1	= $arr_mrn_sub_cat_tot_A;


    while (list($key_i, $value_i) = each($arr_mrn_item_tot_U)) {//item ,unit budget
        $budget_bal	= $obj_comm_budget_get->load_budget_balance_for_mrn($location,$header_array['DEPARTMENT_ID'],$budg_year,'',$key_i,date('m'),'RunQuery2');
        $budget_bal = $budget_bal['balUnits'];
        if($value_i > $budget_bal){
            $rollBackFlag	=1;
            $rollBackMsg	="There is no budget balance for this month for item '".$obj_common->get_item_name($key_i,'RunQuery2')."'. Budget balance is ".$budget_bal." Units";
        }
    }
    while (list($key_i, $value_i) = each($arr_mrn_item_tot_A)) {//item, amount budget
        $budget_bal	= $obj_comm_budget_get->load_budget_balance_for_mrn($location,$header_array['DEPARTMENT_ID'],$budg_year,'',$key_i,date('m'),'RunQuery2');
        $budget_bal = $budget_bal['balAmount'];
        if($value_i > $budget_bal){
            $rollBackFlag	=1;
            $rollBackMsg	="There is no budget balance for this month for item '".$obj_common->get_item_name($key_i,'RunQuery2')."'. Budget balance is ".$budget_bal." Amount";
        }
    }
    while (list($key, $value) = each($arr_mrn_sub_cat_tot_U)) {//sub category ,unit budget
        $budget_bal	= $obj_comm_budget_get->load_budget_balance_for_mrn($location,$header_array['DEPARTMENT_ID'],$budg_year,$key,'',date('m'),'RunQuery2');
        $budget_bal = $budget_bal['balUnits'];
        if($value > $budget_bal){
            $rollBackFlag	=1;
            $rollBackMsg	="There is no budget balance for this month for sub category '".$obj_common->get_sub_category_name($key,'RunQuery2')."'. Budget balance is ".$budget_bal." Units";
        }
    }
    while (list($key, $value) = each($arr_mrn_sub_cat_tot_A)) {//sub category ,amount budget
        $budget_bal	= $obj_comm_budget_get->load_budget_balance_for_mrn($location,$header_array['DEPARTMENT_ID'],$budg_year,$key,'',date('m'),'RunQuery2');
        $budget_bal = $budget_bal['balAmount'];
        if($value > $budget_bal){
            $rollBackFlag	=1;
            $rollBackMsg	="There is no budget balance for this month for sub category '".$obj_common->get_sub_category_name($key,'RunQuery2')."'. Budget balance is ".$budget_bal." Amount";
        }
    }
    //print_r();
    //$rollBackFlag	=1;
    //--END OF -----2014-05-08-----BUDGET VALIDATION----------------------------------------

    if($result){
        $sql = "SELECT ware_mrnheader.intUser, ware_mrnheader.intStatus, ware_mrnheader.intApproveLevels FROM ware_mrnheader WHERE (intMrnNo='$mrnNo') AND (`intMrnYear`='$year')";
        $results = $db->RunQuery2($sql);
        $row = mysqli_fetch_array($results);
        $createdUser = $row['intUser'];
        $status = $row['intStatus'];
        $savedLevels = $row['intApproveLevels'];
        
        $approveLevel=$savedLevels+1-$status;
        $sqlI = "INSERT INTO `ware_mrnheader_approvedby` (`intMrnNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
				VALUES ('$mrnNo','$year','$approveLevel','$userId',now(),0)";
        $resultI = $db->RunQuery2($sqlI);
        if((!$resultI) && ($rollBackFlag!=1)){
            $rollBackFlag=1;
            $sqlM=$sqlI;
            $rollBackMsg = "Approval error!";
        }

        if($status==1)
        {
            if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
                ////////////// send mail to creater //////////////////
                sendFinlConfirmedEmailToUser($mrnNo,$year,$objMail,$mainPath,$root_path);
				
            }
        }
		
		if($status==1 && $savedLevels==3) //nsoft 3.12.4
        {
            if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
                ////////////// send mail to creater //////////////////
                sendFinlConfirmedEmailToManagement($mrnNo,$year,$objMail,$mainPath,$root_path);
				
            }
        }

    }
    //-----2014-05-08-----BUDGET UPDATION-------------------------------------------
    $header_array		=$obj_mrn_get->get_header($mrnNo,$year,'RunQuery2');
    if($header_array['STATUS'] == 1){
        if($rollBackFlag != 1){
            while (list($key_i, $value_i) = each($arr_mrn_item_tot_U_1)) {
                $response_b		= $obj_budget_set->updatePlanDetails_units_or_amount($location,$header_array['DEPARTMENT_ID'],$budg_year,'',$key_i,date('m'),0,$value_i,'MRN','U','RunQuery2');
                if($response_b['savedStatus']	 == 'fail'){
                    $rollBackFlag		=1;
                    $rollBackMsg		.=$response_b['savedMassege'];
                    $sqlM				.=$response_b['error_sql'];
                }
            }
        }
        if($rollBackFlag != 1){
            while (list($key_i, $value_i) = each($arr_mrn_item_tot_A_1)) {
                $response_b		= $obj_budget_set->updatePlanDetails_units_or_amount($location,$header_array['DEPARTMENT_ID'],$budg_year,'',$key_i,date('m'),$value_i,0,'MRN','A','RunQuery2');
                if($response_b['savedStatus']	 == 'fail'){
                    $rollBackFlag		=1;
                    $rollBackMsg		.=$response_b['savedMassege'];
                    $sqlM				.=$response_b['error_sql'];
                }
            }
        }
        if($rollBackFlag != 1){
            while (list($key, $value) = each($arr_mrn_sub_cat_tot_U_1)) {
                $response_b		= $obj_budget_set->updatePlanDetails_units_or_amount($location,$header_array['DEPARTMENT_ID'],$budg_year,$key,0,date('m'),0,$value,'MRN','U','RunQuery2');
                if($response_b['savedStatus']	 == 'fail'){
                    $rollBackFlag		=1;
                    $rollBackMsg		.=$response_b['savedMassege'];
                    $sqlM				.=$response_b['error_sql'];
                }
            }
        }
        if($rollBackFlag != 1){
            while (list($key, $value) = each($arr_mrn_sub_cat_tot_A_1)) {
                $response_b		= $obj_budget_set->updatePlanDetails_units_or_amount($location,$header_array['DEPARTMENT_ID'],$budg_year,$key,0,date('m'),$value,0,'MRN','A','RunQuery2');
                if($response_b['savedStatus']	 == 'fail'){
                    $rollBackFlag		=1;
                    $rollBackMsg		.=$response_b['savedMassege'];
                    $sqlM				.=$response_b['error_sql'];
                }
            }
        }
    }
	//echo $status.'&& '.$mrnType==3;
	if($status==1 ){
		//echo "sdfdsfdwf";
		//update_po_prn_details_for_mrn_qry($mrnNo,$year);
		//insert_additional_po_prn_details($mrnNo,$year);
		update_po_prn_details_transactions($mrnNo,$year);
	}
	
	
    //$rollBackFlag =1;
    //-----END OF 2014-05-08-----BUDGET UPDATION------------------------------------

    if(($rollBackFlag==0) &&($status==1)){
        $db->RunQuery2('Commit');
        $response['type'] 		= 'pass';
        $response['msg'] 		= "Final Approval raised sucessfully";
        $response['q'] 			= $sql;
    }
    else if($rollBackFlag==0){
        $db->RunQuery2('Commit');
        $response['type'] 		= 'pass';
        $response['msg'] 		= "Approval raised sucessfully";
        $response['q'] 			= '';
    }
    else if($rollBackFlag==1){
        $db->RunQuery2('Rollback');
        $response['type'] 		= 'fail';
        $response['msg'] 		= $rollBackMsg;
        $response['q'] 			= $sqlM;
    }
    else{
        $db->RunQuery2('Rollback');
        $response['type'] 		= 'fail';
        $response['msg'] 		= $db->errormsg;
        $response['q'] 			= $sqlM;
    }
    $db->CloseConnection();

    //----------------------------------------
    echo json_encode($response);
    //-----------------------------------------

}
//---------REJECT MRN--------------------------------------
else if($_REQUEST['status']=='reject')
{
    $db->OpenConnection();
    $db->RunQuery2('Begin');
    $arrHeader		= json_decode($_REQUEST['arrHeader'], true);
    $reject_reason	= $obj_common->replace($arrHeader['reason']);
    $rollBackFlag=0;
    $rollBackMsg = '';


    $sql = "SELECT ware_mrnheader.intUser, ware_mrnheader.intStatus,ware_mrnheader.intApproveLevels FROM ware_mrnheader WHERE  (intMrnNo='$mrnNo') AND (`intMrnYear`='$year')";
    $results = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($results);
    $createdUser = $row['intUser'];
    $status = $row['intStatus'];
    $savedLevels = $row['intApproveLevels'];

    $sql = "UPDATE `ware_mrnheader` SET `intStatus`=0, REJECT_REMARKS ='$reject_reason' WHERE (intMrnNo='$mrnNo') AND (`intMrnYear`='$year')";
    $result = $db->RunQuery2($sql);
    if((!$result) && ($rollBackFlag!=1)){
        $rollBackFlag=1;
        $sqlM=$sql;
        $rollBackMsg = "Rejection error!";
    }

    //	$sql = "DELETE FROM `ware_mrnheader_approvedby` WHERE (`intMrnNo`='$mrnNo') AND (`intYear`='$year')";
    //	$result2 = $db->RunQuery2($sql);

    $sqlI = "INSERT INTO `ware_mrnheader_approvedby` (`intMrnNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
			VALUES ('$mrnNo','$year','0','$userId',now(),0)";
    $resultI = $db->RunQuery2($sqlI);
    if((!$resultI) && ($rollBackFlag!=1)){
        $rollBackFlag=1;
        $sqlM=$sqlI;
        $rollBackMsg = "Rejection error!";
    }
    if($resultI)
    {
        if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
            sendRejecedEmailToUser($mrnNo,$year,$objMail,$mainPath,$root_path);
        }
    }

    //-------------------
    if($rollBackFlag==0){
        $db->RunQuery2('Commit');
        $response['type'] 		= 'pass';
        $response['msg'] 		= "Rejection raised sucessfully";
        $response['q'] 			= '';
    }
    else if($rollBackFlag==1){
        $db->RunQuery2('Rollback');
        $response['type'] 		= 'fail';
        $response['msg'] 		= $rollBackMsg;
        $response['q'] 			= $sqlM;
    }
    else{
        $db->RunQuery2('Rollback');
        $response['type'] 		= 'fail';
        $response['msg'] 		= $db->errormsg;
        $response['q'] 			= $sqlM;
    }
    $db->CloseConnection();

    //----------------------------------------
    echo json_encode($response);
    //-----------------------------------------

}
//---------------------------------------------------------
else if ($_REQUEST['status']=='update'){
    $db->OpenConnection();
    $db->RunQuery2('Begin');

    $arr = json_decode($_REQUEST['arr'], true);

    $rollBackFlag = 0;
    $rollBackMsg = '';
    $extraFlag = 0;

   foreach($arr as $arrVal_Check) {
        $extraQty_S = $arrVal_Check['extraQty'];
        $mrn_Type_S = $arrVal_Check['mrnType'];

        if($extraQty_S >= 0.01 || $mrn_Type_S == 6) {
            $extraFlag = 1;
        }
    }
    
    foreach($arr as $arrVal) {
        $orderNo = $arrVal['orderNo'];
        $orderNoArray = explode('/', $orderNo);
        $itemId = $arrVal['itemId'];
        $totalQty = $arrVal['totalQty'];
        $extraQty = $arrVal['extraQty'];
        $strStyleNo = $arrVal['strStyleNo'];

        if($extraFlag == 0) {
            $sql_Header = "SELECT intApproveLevels,intStatus FROM ware_mrnheader WHERE intMrnNo = '$mrnNo' AND intMrnYear = '$year'";
            $result_Header = $db->RunQuery2($sql_Header);
            $row = mysqli_fetch_array($result_Header);
            $approve_Level = $row['intApproveLevels'];
            $status_header = $row['intStatus'];
            $nextLevel = $approve_Level - $status_header + 2 ;
            if($nextLevel > 1 && $nextLevel < $approve_Level){
                $sql_Update_H = "UPDATE `ware_mrnheader` SET `intStatus`='2', `intApproveLevels`='$nextLevel' WHERE (`intMrnNo`='$mrnNo') AND (`intMrnYear`='$year')";
                $result_update = $db->RunQuery2($sql_Update_H);
                if((!$result_update) ){
                    $rollBackFlag = 1;
                    $sqlM = $sql_Update_H;
                    $rollBackMsg = "Approve Levels Updating error!";
                    break;
                }
            }
        }
        else{
            $sql_Header = "SELECT intApproveLevels,intStatus FROM ware_mrnheader WHERE intMrnNo = '$mrnNo' AND intMrnYear = '$year'";
            $result_Header = $db->RunQuery2($sql_Header);
            $row = mysqli_fetch_array($result_Header);
            $approve_Level = $row['intApproveLevels'];
            $status_header = $row['intStatus'];
            $newStatus = $mrnApproveLevel - $approve_Level + $status_header ;
            // $nextLevel = $mrnApproveLevel ;
            if($status_header > 1 && $status_header < $mrnApproveLevel){
                $sql_Update_H = "UPDATE `ware_mrnheader` SET `intStatus`='$newStatus', `intApproveLevels`='$mrnApproveLevel' WHERE (`intMrnNo`='$mrnNo') AND (`intMrnYear`='$year')";
                $result_update = $db->RunQuery2($sql_Update_H);
                if((!$result_update) ){
                    $rollBackFlag = 1;
                    $sqlM = $sql_Update_H;
                    $rollBackMsg = "Approve Levels Updating error!";
                    break;
                }
            }
        }

        $sql_qty = "SELECT INITIAL_QTY FROM `ware_mrndetails` WHERE `intMrnNo` = '$mrnNo' AND `intMrnYear` = '$year' AND `intOrderNo` = '$orderNoArray[0]' AND `intOrderYear` = '$orderNoArray[1]' AND `strStyleNo` = '$strStyleNo' AND `intItemId` = '$itemId'";
        $result_qty = $db->RunQuery2($sql_qty);
        while ($row = mysqli_fetch_array($result_qty)){
            $INITIAL_QTY = $row['INITIAL_QTY'];
            if($totalQty > $INITIAL_QTY){
                $rollBackFlag = 1;
                $sqlM = $sql_qty;
                $rollBackMsg = "Qty Is greater than the initial Qty";
                break;
            }
        }

        $sql_Update = "UPDATE `ware_mrndetails` SET `dblQty`='$totalQty', `EXTRA_QTY`='$extraQty' WHERE (`intMrnNo`='$mrnNo')  AND (`intMrnYear`='$year') AND (`intOrderNo`='$orderNoArray[0]') AND (`intOrderYear`='$orderNoArray[1]') AND (`strStyleNo`='$strStyleNo') AND (`intItemId`='$itemId')";
        $result_update = $db->RunQuery2($sql_Update);
        if((!$result_update) ){
            $rollBackFlag = 1;
            $sqlM = $sql;
            $rollBackMsg = "Updating error!";
            break;
        }
        $sql = "SELECT ware_mrnheader.intUser, ware_mrnheader.intStatus, ware_mrnheader.intApproveLevels FROM ware_mrnheader WHERE (intMrnNo='$mrnNo') AND (`intMrnYear`='$year')";
        $results = $db->RunQuery2($sql);
        $row = mysqli_fetch_array($results);
        $createdUser = $row['intUser'];
        if($createdUser == $userId){ //if confiremed user and created user same, email won't be generate.
                $rollBackFlag = 1;
                $sqlM = $sql;
                $rollBackMsg = "Can not update by the create user";
                break;
         }
    }
    if($rollBackFlag == 0){
        $db->RunQuery2('Commit');
        $response['type'] 		= 'pass';
        $response['msg'] 		= "Updated sucessfully";
        $response['q'] 			= $sql;
    }
    else if($rollBackFlag == 1){
        $db->RunQuery2('Rollback');
        $response['type'] 		= 'fail';
        $response['msg'] 		= $rollBackMsg;
        $response['q'] 			= $sqlM;
    }
    else{
        $db->RunQuery2('Rollback');
        $response['type'] 		= 'fail';
        $response['msg'] 		= $db->errormsg;
        $response['q'] 			= $sqlM;
    }
    $db->CloseConnection();
    echo json_encode($response);
}
//---------------------FUNCTIONS------------------------------------------
function getStockBalance($company,$location,$orderNo,$orderYear,$style,$item)
{
    global $db;
    $sql = "SELECT
				Sum(ware_stocktransactions.dblQty) as stockBal 
				FROM ware_stocktransactions
				WHERE
				ware_stocktransactions.intCompanyId =  '$company' AND
				ware_stocktransactions.intLocationId =  '$location' AND
				ware_stocktransactions.intOrderNo =  '$orderNo' AND
				ware_stocktransactions.intOrderYear =  '$orderYear' AND
				ware_stocktransactions.strStyleNo =  '$style' AND
				ware_stocktransactions.intItemId =  '$item'";

    $result = $db->RunQuery2($sql);
    $rows = mysqli_fetch_array($result);
    return val($rows['stockBal']);
}
//--------------------------------------------------------
function getStockBalance_bulk($location,$item)
{
    global $db;
    $sql = "SELECT
				Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$location'
				GROUP BY
				ware_stocktransactions_bulk.intItemId";

    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    return val($row['stockBal']);
}
//--------------------------------------------------------
function sendFinlConfirmedEmailToUser($serialNo,$year,$objMail,$mainPath,$root_path){
    global $db;

    $sql = "SELECT
			sys_users.strFullName,
			sys_users.strEmail 
			FROM 
			ware_mrnheader
			Inner Join sys_users ON ware_mrnheader.intUser = sys_users.intUserId
			WHERE
			ware_mrnheader.intMrnNo =  '$serialNo' AND
			ware_mrnheader.intMrnYear =  '$year'";


    $result = $db->RunQuery2($sql);

    $row=mysqli_fetch_array($result);
    $enterUserName = $row['strFullName'];
    $enterUserEmail = $row['strEmail'];

    $header="CONFIRMED MRN ('$serialNo'/'$year')";

    //send mails ////
    $_REQUEST = NULL;
    $_REQUEST = NULL;

    $_REQUEST['program']='MRN';
    $_REQUEST['field1']='MRN No';
    $_REQUEST['field2']='MRN Year';
    $_REQUEST['field3']='';
    $_REQUEST['field4']='';
    $_REQUEST['field5']='';
    $_REQUEST['value1']=$serialNo;
    $_REQUEST['value2']=$year;
    $_REQUEST['value3']='';
    $_REQUEST['value4']='';
    $_REQUEST['value5']='';

    $_REQUEST['subject']="CONFIRMED MRN ('$serialNo'/'$year')";

    $_REQUEST['statement1']="Approved this";
    $_REQUEST['statement2']="to view this";
    $_REQUEST['link']=base64_encode($mainPath."?q=938&mrnNo=$serialNo&year=$year&approveMode=0");
    //$path=$mainPath.'presentation/mail_approval_template.php';
    $path=$root_path."presentation/mail_approval_template.php";
    $objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}

function sendFinlConfirmedEmailToManagement($serialNo,$year,$objMail,$mainPath,$root_path){ //suvini nsoft3.12.4
    global $db;

    $sql = "SELECT
			sys_users.strFullName as strFullName,
			sys_users.strEmail as strEmail
			FROM
			sys_users
			WHERE
			sys_users.intUserId IN (7,864,463)";


    $result = $db->RunQuery2($sql);

    while ($row		    = mysqli_fetch_array($result))
	{
    $enterUserName  = $row['strFullName'];
    $enterUserEmail = $row['strEmail'];

    $header="NOTIFICATION FOR THE APPROVAL OF 1ST DAY MRN('$serialNo'/'$year')";

    //send mails ////
    $_REQUEST = NULL;
    $_REQUEST = NULL;

    $_REQUEST['program']='MRN';
    $_REQUEST['field1']='MRN No';
    $_REQUEST['field2']='MRN Year';
    $_REQUEST['field3']='';
    $_REQUEST['field4']='';
    $_REQUEST['field5']='';
    $_REQUEST['value1']=$serialNo;
    $_REQUEST['value2']=$year;
    $_REQUEST['value3']='';
    $_REQUEST['value4']='';
    $_REQUEST['value5']='';

    $_REQUEST['subject']="NOTIFICATION FOR THE APPROVAL OF 1ST DAY MRN('$serialNo'/'$year')";

    $_REQUEST['statement1']="Approved this";
    $_REQUEST['statement2']="to view this";
    $_REQUEST['link']=base64_encode($mainPath."?q=938&mrnNo=$serialNo&year=$year&approveMode=0");
    //$path=$mainPath.'presentation/mail_approval_template.php';
    $path=$root_path."presentation/mail_approval_template.php";
    $objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);
	}
}
//--------------------------------------------------------
function sendRejecedEmailToUser($serialNo,$year,$objMail,$mainPath,$root_path){
    global $db;

    $sql = "SELECT
			sys_users.strFullName,
			sys_users.strEmail 
			FROM 
			ware_mrnheader
			Inner Join sys_users ON ware_mrnheader.intUser = sys_users.intUserId
			WHERE
			ware_mrnheader.intMrnNo =  '$serialNo' AND
			ware_mrnheader.intMrnYear =  '$year'";


    $result = $db->RunQuery2($sql);

    $row=mysqli_fetch_array($result);
    $enterUserName = $row['strFullName'];
    $enterUserEmail = $row['strEmail'];

    $header="REJECTED MRN ('$serialNo'/'$year')";

    //send mails ////
    $_REQUEST = NULL;
    $_REQUEST = NULL;

    $_REQUEST['program']='MRN';
    $_REQUEST['field1']='MRN No';
    $_REQUEST['field2']='MRN Year';
    $_REQUEST['field3']='';
    $_REQUEST['field4']='';
    $_REQUEST['field5']='';
    $_REQUEST['value1']=$serialNo;
    $_REQUEST['value2']=$year;
    $_REQUEST['value3']='';
    $_REQUEST['value4']='';
    $_REQUEST['value5']='';

    $_REQUEST['subject']="REJECTED MRN ('$serialNo'/'$year')";

    $_REQUEST['statement1']="Rejected this";
    $_REQUEST['statement2']="to view this";
    $_REQUEST['link']=base64_encode($mainPath."?q=938&mrnNo=$serialNo&year=$year&approveMode=0");
    //$path=$mainPath.'presentation/mail_approval_template.php';
    $path=$root_path."presentation/mail_approval_template.php";
    $objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}
//--------------------------------------------------------
function checkBalanceQty($mrnNo,$year){
	
    global $db;
	global $extra_percentage;
    $sql = "SELECT intOrderNo,intOrderYear,strStyleNo,intItemId,dblQty,EXTRA_QTY,MRN_TYPE FROM ware_mrndetails WHERE intMrnNo = '$mrnNo' AND intMrnYear = '$year' GROUP BY
	intMrnNo,intMrnYear,intOrderNo,intOrderYear,strStyleNo,intItemId";
    $result = $db->RunQuery2($sql);
    $arr['rollBackFlag'] = 0;
    $arr['rollBackMsg']  = " ";
    while ($row = mysqli_fetch_array($result)){

        $no = $row['intOrderNo'];
        $year = $row['intOrderYear'];
        $so = $row['strStyleNo'];
        $sql_heatSeal_check = "select mt.intId, mt.strName from trn_orderdetails tod inner join trn_sampleinfomations_details tsd on tod.intSampleNo = tsd.intSampleNo
		and tod.intSampleYear = tsd.intSampleYear and tod.strCombo = tsd.strComboName and tod.strPrintName = tsd.strPrintName and 
		tod.intRevisionNo = tsd.intRevNo inner join mst_techniques mt on mt.intId = tsd.intTechniqueId where mt.heatTransfer =1 and tod.intOrderNo = '$no' and tod.intOrderYear = '$year' and tod.intSalesOrderId = '$so'" ;

        $heatSealAddition = 0;
        $heatSealStatus = $db->RunQuery2($sql_heatSeal_check);
        $heatSealCount = mysqli_num_rows($heatSealStatus);
        if ($heatSealCount > 0) {
            $heatSealAddition = 2;
        }

        $salesOrderId = $row['strStyleNo'];
		$itemId		  = $row['intItemId'];
        $priQty       =round(getPRIQty($row['intOrderNo'],$row['intOrderYear'],$row['strStyleNo'],$row['intItemId']),4);
		$priQty		  = round(($priQty)*(100+$extra_percentage + $heatSealAddition)/100,4);
        $mrnQty       =round(getMRNQty($row['intOrderNo'],$row['intOrderYear'],$row['strStyleNo'],$row['intItemId']),4);
        
		//2017-07-07 allow precosting mrn
		$order_type		= get_order_type($row['intOrderNo'],$row['intOrderYear']);
		$instant		= get_instant_type($row['intOrderNo'],$row['intOrderYear']);
		$instantPre		= get_instant_pre_type($row['intOrderNo'],$row['intOrderYear']);

		
		//$actualPriQty = $priQty-$mrnQty;
		//if($order_type==0){//other
		if($order_type==0 || ($instant!=1 && $order_type==1)|| ($instantPre!=1 && $order_type==2)){//other or pre costing non instant or split from non instant dummy
		$actualPriQty_s	=$priQty-$mrnQty; 
		}
		else if($order_type==1){//pre costing & instant
			$priQty_pre =$priQty;
			$mrnQty_pre =$mrnQty;
			$priQty_act =0;
			$mrnQty_act =0;
			$priQty_tot =0;
			$mrnQty_tot =0;

			$result_act = get_actual_details($row['intOrderNo'],$row['intOrderYear'],$salesOrderId);
			while($row_act=mysqli_fetch_array($result_act)){
				$priQty_A	= round(getPRIQty($row_act['ORDER_NO'],$row_act['ORDER_YEAR'],$row_act['SO'],$itemId),4);
				$priQty_A		  		= round(($priQty_A)*(100+$extra_percentage + $heatSealAddition)/100,4);
				$mrnQty_A				= round(getMRNQty($row_act['ORDER_NO'],$row_act['ORDER_YEAR'],$row_act['SO'],$itemId),4);
				$priQty_act +=$priQty_A;
				$mrnQty_act +=$mrnQty_A;
			}
			$priQty_tot 	=$priQty_act+$priQty_pre;
			$mrnQty_tot 	=$mrnQty_act+$mrnQty_pre;
			$actualPriQty_s	=($priQty_tot-$mrnQty_tot);
			$x='MIN(('.$priQty.'),('.$priQty_tot.'-'.$mrnQty_tot.'))';
		}  
		else if($order_type==2){//actual & instant

			$actualPriQty_s	=0;

		} 
		if($actualPriQty_s < 0)
			$actualPriQty_s=0;
		$actualPriQty     = $actualPriQty_s;
		//$actualPriQty     =round($Qty-$actualPriQty_s,8);
		//end 2017-07-07 allow pre costing MRN
		
		
        $Qty 		  = $row['dblQty'];
        $EXTRA_QTY    = $row['EXTRA_QTY'];
        $ItemType     = $row['MRN_TYPE'];

        if(round(getRoundedWeight($actualPriQty,$row['intItemId']),3) < round(($Qty - $EXTRA_QTY),3) && $ItemType == 3 && round(($Qty - $EXTRA_QTY),3) > 0 ){
            $arr['rollBackFlag']= 1;
            $arr['sqlM']        = $sql;
			$itm	= getItemDetail($row['intOrderNo'],$row['intOrderYear'],$row['strStyleNo'],$row['intItemId']);
            //$arr['rollBackMsg'] = $x."Qty is Greater than the Balance MRN Qty for item :".$itm.'==>'.round(getRoundedWeight($actualPriQty,$row['intItemId']),3).' > '.round(($Qty - $EXTRA_QTY),3);
            $arr['rollBackMsg'] = $x."Qty is Greater than the Balance MRN Qty for item :".$itm.'==>'.$actualPriQty.' > '.round(($Qty - $EXTRA_QTY),3);			break;
        }
    }
    return $arr;
}
function getPRIQty($orderNo,$orderYear,$salesOrderNo,$item)
{
    global $db;
    $sql = "	SELECT
						IFNULL(trn_po_prn_details_sales_order.REQUIRED,'0') AS priQty
					FROM
						trn_po_prn_details_sales_order
					WHERE
						trn_po_prn_details_sales_order.ORDER_NO = '$orderNo'
					AND trn_po_prn_details_sales_order.ORDER_YEAR = '$orderYear'
					AND trn_po_prn_details_sales_order.SALES_ORDER = '$salesOrderNo'
					AND trn_po_prn_details_sales_order.ITEM = '$item'";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    return val($row['priQty']);
}
function getMRNQty($orderNo,$orderYear,$salesOrderNo,$item)
{
    global $db;
    $sql = "SELECT
				IFNULL(sum(ware_mrndetails.dblQty),'0') AS mrnQty
			FROM
				ware_mrndetails
			INNER JOIN ware_mrnheader ON ware_mrndetails.intMrnNo = ware_mrnheader.intMrnNo
			AND ware_mrndetails.intMrnYear = ware_mrnheader.intMrnYear
			WHERE
				ware_mrndetails.intOrderNo = '$orderNo'
			AND ware_mrndetails.intOrderYear = '$orderYear'
			AND ware_mrndetails.strStyleNo = '$salesOrderNo'
			AND ware_mrndetails.intItemId = '$item'
			AND ware_mrnheader.intStatus = '1'";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    return val($row['mrnQty']);
}
function getRoundedWeight($qty, $item){
    global $db;

    $sql = "SELECT
			mst_item.strName,
			mst_item.intUOM 
			FROM `mst_item`
			WHERE
			mst_item.intId = '$item'
			";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $uom	= ($row['intUOM']);
	
	if($uom ==6 && floor($qty)==0){//only for weight < 1kg
	
	if($qty<0.005 && $qty>=0.0001)
		$qty=0.005;
		//$y	=((round($qty,3)*1000)%100);
		$y	= ((floor($qty * 1000) / 1000)*1000)%100;
		$z	=ceil($y/10)*10;
		$a	=$z-$y;
		
		if($a > 5)
			$val1=0;
		else if($a>0)
			$val1=5;
		else
			$val1	=0;
			
			$val2 	=  floor((floor($qty * 1000) / 1000)*100)/100;
			$val	= $val2+$val1/1000;
		
	}
	else
	$val	=$qty;
 	//echo $qty.'/'.$y.'/'.$z.'/'.$a.'/'.$val1.'/'.$val2.'/'.$val.'</br>';
	return $val;
}
function update_po_prn_details_for_mrn_qry($mrnNo,$year){
	global $db;
	$sql="SELECT
			ware_mrndetails.intOrderNo,
			ware_mrndetails.intOrderYear,
			ware_mrndetails.strStyleNo,
			ware_mrndetails.intItemId,
			ware_mrndetails.dblQty,
			ware_mrndetails.EXTRA_QTY
			FROM `ware_mrndetails`
			WHERE
			-- ware_mrndetails.MRN_TYPE = 3 AND
			ware_mrndetails.intMrnNo = '$mrnNo' AND
			ware_mrndetails.intMrnYear = '$year'
			";
	$result = $db->RunQuery2($sql);
	while($row	=mysqli_fetch_array($result)){
		$orderNo			= $row['intOrderNo'];
		$orderYear			= $row['intOrderYear'];
		$so					= $row['strStyleNo'];
		$item				= $row['intItemId'];
		$totQty				= $row['dblQty'];
		$extra				= $row['EXTRA_QTY'];
 			  $sql_u	="UPDATE `trn_po_prn_details_sales_order` SET `MRN_TOT_QTY`=IFNULL(`MRN_TOT_QTY`,0)+'$totQty', `MRN_EXTRA_QTY`=IFNULL(`MRN_EXTRA_QTY`,0)+'$extra' WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`SALES_ORDER`='$so') AND (`ITEM`='$item')";
		$result_u = $db->RunQuery2($sql_u);
 	}
}
function insert_additional_po_prn_details($mrnNo,$year){
	global $db;
	$sql="SELECT
			ware_mrndetails.intOrderNo,
			ware_mrndetails.intOrderYear,
			ware_mrndetails.strStyleNo,
			ware_mrndetails.intItemId,
			ware_mrndetails.dblQty,
			ware_mrndetails.EXTRA_QTY,
			trn_orderdetails.intQty	as orderQty,
			trn_po_prn_details_sales_order_additional_mrn_items.ORDER_NO AS saved_order
			FROM `ware_mrndetails`
			INNER JOIN trn_orderdetails ON ware_mrndetails.intOrderNo = trn_orderdetails.intOrderNo AND ware_mrndetails.intOrderYear = trn_orderdetails.intOrderYear AND ware_mrndetails.strStyleNo = trn_orderdetails.intSalesOrderId
			LEFT JOIN trn_po_prn_details_sales_order_additional_mrn_items ON ware_mrndetails.intOrderNo = trn_po_prn_details_sales_order_additional_mrn_items.ORDER_NO AND ware_mrndetails.intOrderYear = trn_po_prn_details_sales_order_additional_mrn_items.ORDER_YEAR AND ware_mrndetails.strStyleNo = trn_po_prn_details_sales_order_additional_mrn_items.SALES_ORDER AND ware_mrndetails.intItemId = trn_po_prn_details_sales_order_additional_mrn_items.ITEM
			WHERE
			ware_mrndetails.MRN_TYPE = 6 AND
			ware_mrndetails.intMrnNo = '$mrnNo' AND
			ware_mrndetails.intMrnYear = '$year'
			";
	$result = $db->RunQuery2($sql);
	while($row	=mysqli_fetch_array($result)){
		$orderNo			= $row['intOrderNo'];
		$orderYear			= $row['intOrderYear'];
		$so					= $row['strStyleNo'];
		$item				= $row['intItemId'];
		$totQty				= $row['dblQty'];
		$extra				= $row['EXTRA_QTY'];
		$orderQty				= $row['orderQty'];
		if($row['saved_order'] > 0){
 			  $sql_u	="UPDATE `trn_po_prn_details_sales_order_additional_mrn_items` SET `MRN_TOT_QTY`=IFNULL(`MRN_TOT_QTY`,0)+'$totQty', `MRN_EXTRA_QTY`=IFNULL(`MRN_EXTRA_QTY`,0)+'$extra' WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`SALES_ORDER`='$so') AND (`ITEM`='$item')";
		}
		else{
		$sql_u	="INSERT INTO `trn_po_prn_details_sales_order_additional_mrn_items` (`ORDER_NO`, `ORDER_YEAR`, `SALES_ORDER`, `ITEM`, `CONS_PC`, `PRODUCTION_QTY`, `MRN_TOT_QTY`, `MRN_EXTRA_QTY`) VALUES ('$orderNo', '$orderYear', '$so', '$item', '0', '$orderQty', '$totQty', '$extra')";
		}
		
		$result_u = $db->RunQuery2($sql_u);
 	}
}

function getItemDetail($orderNo,$orderYear,$so,$item){
	global $db;
	
    $sql = "SELECT
			mst_item.strName,
			mst_item.intUOM 
			FROM `mst_item`
			WHERE
			mst_item.intId = '$item'
			";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $name	= ($row['strName']);
    $sql = "SELECT

			concat(trn_orderdetails.intOrderNo,'/',
			trn_orderdetails.intOrderYear,'/',
			trn_orderdetails.strSalesOrderNo,'/',
			mst_part.strName) as so
			FROM
			trn_orderdetails
			INNER JOIN mst_part ON trn_orderdetails.intPart = mst_part.intId
			WHERE
			trn_orderdetails.intOrderNo = '$orderNo' AND
			trn_orderdetails.intOrderYear = '$orderYear' AND
			trn_orderdetails.intSalesOrderId = '$so'
			";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    $so	= ($row['so']);
	
	return $so.'/'.$name;
}

function update_po_prn_details_transactions($mrnNo,$year){
	
	global $db;
	$sql="SELECT 
			ware_mrnheader.intCompanyId,
			ware_mrndetails.intOrderNo,
			ware_mrndetails.intOrderYear,
			ware_mrndetails.strStyleNo,
			ware_mrndetails.intItemId,
			ware_mrndetails.MRN_TYPE, 
			trn_po_prn_details_sales_order_stock_transactions.TRANSACTION,
			ware_mrndetails.dblQty,
			ware_mrndetails.EXTRA_QTY,
			trn_orderdetails.intQty	as orderQty,
			trn_po_prn_details_sales_order_stock_transactions.ORDER_NO AS saved_order
			FROM `ware_mrndetails` 
			INNER JOIN ware_mrnheader ON ware_mrnheader.intMrnNo = ware_mrndetails.intMrnNo AND ware_mrnheader.intMrnYear = ware_mrndetails.intMrnYear
			INNER JOIN trn_orderdetails ON ware_mrndetails.intOrderNo = trn_orderdetails.intOrderNo AND ware_mrndetails.intOrderYear = trn_orderdetails.intOrderYear AND ware_mrndetails.strStyleNo = trn_orderdetails.intSalesOrderId
			LEFT JOIN trn_po_prn_details_sales_order_stock_transactions ON ware_mrndetails.intOrderNo = trn_po_prn_details_sales_order_stock_transactions.ORDER_NO AND ware_mrndetails.intOrderYear = trn_po_prn_details_sales_order_stock_transactions.ORDER_YEAR AND ware_mrndetails.strStyleNo = trn_po_prn_details_sales_order_stock_transactions.SALES_ORDER AND ware_mrndetails.intItemId = trn_po_prn_details_sales_order_stock_transactions.ITEM  
			AND (trn_po_prn_details_sales_order_stock_transactions.TRANSACTION='MRN' OR trn_po_prn_details_sales_order_stock_transactions.TRANSACTION='MRN_EXTRA' ) 
			AND trn_po_prn_details_sales_order_stock_transactions.LOCATION_ID =ware_mrnheader.intCompanyId
			WHERE
			(ware_mrndetails.MRN_TYPE = 3 OR ware_mrndetails.MRN_TYPE = 6 ) AND
			ware_mrndetails.intMrnNo = '$mrnNo' AND
			ware_mrndetails.intMrnYear = '$year'
			";
	$result = $db->RunQuery2($sql);
	while($row	=mysqli_fetch_array($result)){
		$orderNo			= $row['intOrderNo'];
		$orderYear			= $row['intOrderYear'];
		$so					= $row['strStyleNo'];
		$item				= $row['intItemId'];
		$totQty				= $row['dblQty'];
		$extra				= $row['EXTRA_QTY'];
		$orderQty			= $row['orderQty'];
		$location			= $row['intCompanyId'];
		$type				= $row['MRN_TYPE'];
		if($type==3)
		$additional	=0;
		else
		$additional	=1;
		if($row['saved_order'] > 0){
			  if($row['TRANSACTION']=='MRN'){
 			  	$sql_u	="UPDATE `trn_po_prn_details_sales_order_stock_transactions` SET `QTY`=IFNULL(`QTY`,0)+'$totQty' WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`SALES_ORDER`='$so') AND (`ITEM`='$item') AND (`TRANSACTION`='MRN') AND (`LOCATION_ID`='$location')";
				$result_u 	= $db->RunQuery2($sql_u);
			  }
			  else if($row['TRANSACTION']=='MRN_EXTRA'){
			  	$sql_u2	="UPDATE `trn_po_prn_details_sales_order_stock_transactions` SET `QTY`=IFNULL(`QTY`,0)+'$extra' WHERE (`ORDER_NO`='$orderNo') AND (`ORDER_YEAR`='$orderYear') AND (`SALES_ORDER`='$so') AND (`ITEM`='$item') AND (`TRANSACTION`='MRN_EXTRA') AND (`LOCATION_ID`='$location')";
				$result_u2 	= $db->RunQuery2($sql_u2);
			  }
		}
		else{
			$sql_u	="INSERT INTO `trn_po_prn_details_sales_order_stock_transactions` (`ORDER_NO`, `ORDER_YEAR`, `SALES_ORDER`, `ITEM`, `LOCATION_ID`, `TRANSACTION`, `QTY`) VALUES ('$orderNo', '$orderYear', '$so', '$item', '$location', 'MRN', '$totQty')";
			$result_u 	= $db->RunQuery2($sql_u);
			$sql_u2	="INSERT INTO `trn_po_prn_details_sales_order_stock_transactions` (`ORDER_NO`, `ORDER_YEAR`, `SALES_ORDER`, `ITEM`, `LOCATION_ID`, `TRANSACTION`, `QTY`) VALUES ('$orderNo', '$orderYear', '$so', '$item', '$location', 'MRN_EXTRA', '$extra')";
			$result_u2 	= $db->RunQuery2($sql_u2);
		}
		
  	}
	
}

function getExtraPercentage($company){
	global $db;	
    $sql = "SELECT
			mst_companies.EXTRA_MRN_PERCENTAGE 
			FROM `mst_companies`
			WHERE
			mst_companies.intId = '$company'
			";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
   // return ($row['EXTRA_MRN_PERCENTAGE']);
   if($company==36)
   return 5;
   else
   return 0;
   
}

function get_order_type($no,$year){
	
	global $db;	
    $sql = "SELECT
			trn_orderheader.PO_TYPE 
			FROM `trn_orderheader`
			WHERE
			trn_orderheader.intOrderNo = '$no' and 
			trn_orderheader.intOrderYear = '$year'
			";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    return ($row['PO_TYPE']);
	
}
function get_actual_details($no,$year,$so){
	
	global $db;	
    $sql = "SELECT
			trn_orderdetails.intOrderNo AS ORDER_NO,
			trn_orderdetails.intOrderYear AS ORDER_YEAR,
			trn_orderdetails.intSalesOrderId AS SO 
			FROM
			trn_orderdetails
			INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
			WHERE
			trn_orderheader.DUMMY_PO_NO = '$no' AND
			trn_orderheader.DUMMY_PO_YEAR = '$year' AND
			trn_orderdetails.DUMMY_SO_ID = '$so'
			";
    $result = $db->RunQuery2($sql);
    return $result;
}

function get_pre_details($no,$year,$so){
	global $db;	
    $sql = "SELECT
			trn_orderdetails.intOrderNo AS DUMMY_PO_NO,
			trn_orderdetails.intOrderYear AS DUMMY_PO_YEAR,
			trn_orderdetails.DUMMY_SO_ID AS SO 
			FROM
			trn_orderdetails
			INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
			WHERE
			trn_orderheader.intOrderNo = '$no' AND
			trn_orderheader.intOrderYear = '$year' AND
			trn_orderdetails.intSalesOrderId = '$so'
			";
    $result = $db->RunQuery2($sql);
	return $row = mysqli_fetch_array($result);
}

function get_instant_type($no,$year){
	
	global $db;	
    $sql = "SELECT
			trn_orderheader.INSTANT_ORDER 
			FROM `trn_orderheader`
			WHERE
			trn_orderheader.intOrderNo = '$no' and 
			trn_orderheader.intOrderYear = '$year'
			";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    return ($row['INSTANT_ORDER']);
	
}

function get_instant_pre_type($no,$year){
	
	global $db;	
    $sql = "SELECT
			oh2.INSTANT_ORDER
			FROM
			trn_orderheader AS oh1
			INNER JOIN trn_orderheader AS oh2 ON oh1.DUMMY_PO_NO = oh2.intOrderNo AND oh1.DUMMY_PO_YEAR = oh2.intOrderYear
			WHERE
			oh1.intOrderNo = '$no' AND
			oh1.intOrderYear = '$year'";
    $result = $db->RunQuery2($sql);
    $row = mysqli_fetch_array($result);
    return ($row['INSTANT_ORDER']);
	
}


?>
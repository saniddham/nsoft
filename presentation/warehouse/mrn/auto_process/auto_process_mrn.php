<?php
ini_set('display_errors',0);
session_start();

date_default_timezone_set('Asia/Colombo');
ini_set('max_execution_time', 11111111) ;
//ini_set('display_errors',1);

$thisFilePath 	=  $_SERVER['PHP_SELF'];
include_once 	$_SESSION['ROOT_PATH'].'dataAccess/DBManager2.php';
require_once 	$_SESSION['ROOT_PATH']."class/cls_commonFunctions_get.php";
require_once 	$_SESSION['ROOT_PATH']."class/finance/budget/cls_common_function_budget_get.php";
require_once 	$_SESSION['ROOT_PATH']."class/finance/budget/entering/cls_budget_set.php";
require_once 	$_SESSION['ROOT_PATH']."class/warehouse/mrn/cls_mrn_get.php";
		
$db 					= new DBManager2(1);
$obj_comm 				= new cls_commonFunctions_get($db);
$obj_mrn_get			= new cls_mrn_get($db);
$obj_comm_budget_get	= new cls_common_function_budget_get($db);
$obj_budget_set			= new cls_budget_set($db);

$arr_mrn_item_tot_U		= NULL;
$arr_mrn_sub_cat_tot_U	= NULL;
$arr_mrn_item_tot_A		= NULL;
$arr_mrn_sub_cat_tot_A	= NULL;

$db->connect();//open connection.
//auto cancellation (pending issies and days after mrn for today >30)
	  $sql	= "SELECT 
				TB1.intMrnNo,
				TB1.intMrnYear,
				TB1.intStatus,
				TB1.issueStatus as pendingStatus ,
				TB2.intStatus as approvedStatus ,
				DATE(TB1.datdate) as mrnDate 
				FROM 
				(SELECT
				ware_mrnheader.intMrnNo,
				ware_mrnheader.intMrnYear,
				ware_mrnheader.intStatus,
				ware_mrnheader.datdate,
				ware_issueheader.intStatus as issueStatus
				FROM
				ware_mrnheader
				INNER JOIN ware_mrndetails ON ware_mrnheader.intMrnNo = ware_mrndetails.intMrnNo AND ware_mrnheader.intMrnYear = ware_mrndetails.intMrnYear
				LEFT JOIN ware_issuedetails ON ware_mrndetails.intMrnNo = ware_issuedetails.intMrnNo AND ware_mrndetails.intMrnYear = ware_issuedetails.intMrnYear AND ware_mrndetails.intOrderNo = ware_issuedetails.strOrderNo AND ware_mrndetails.intOrderYear = ware_issuedetails.intOrderYear AND ware_mrndetails.strStyleNo = ware_issuedetails.strStyleNo AND ware_mrndetails.intItemId = ware_issuedetails.intItemId
				LEFT JOIN ware_issueheader ON ware_issuedetails.intIssueNo = ware_issueheader.intIssueNo AND ware_issuedetails.intIssueYear = ware_issueheader.intIssueYear
				WHERE  
				ware_mrnheader.intStatus = '1' AND 
				DATEDIFF(NOW(), ware_mrnheader.datdate) > 30 AND 
				(ware_issueheader.intStatus <> 1 OR ware_issueheader.intStatus IS NULL) 
				group by 
				ware_mrnheader.intMrnNo,
				ware_mrnheader.intMrnYear
				) 
				
				as TB1 
				LEFT JOIN    
				
				(
				SELECT
				ware_mrnheader.intMrnNo,
				ware_mrnheader.intMrnYear,
				ware_mrnheader.intStatus,
				ware_mrnheader.datdate,
				ware_issueheader.intStatus as issueStatus
				FROM
				ware_mrnheader
				INNER JOIN ware_mrndetails ON ware_mrnheader.intMrnNo = ware_mrndetails.intMrnNo AND ware_mrnheader.intMrnYear = ware_mrndetails.intMrnYear
				LEFT JOIN ware_issuedetails ON ware_mrndetails.intMrnNo = ware_issuedetails.intMrnNo AND ware_mrndetails.intMrnYear = ware_issuedetails.intMrnYear AND ware_mrndetails.intOrderNo = ware_issuedetails.strOrderNo AND ware_mrndetails.intOrderYear = ware_issuedetails.intOrderYear AND ware_mrndetails.strStyleNo = ware_issuedetails.strStyleNo AND ware_mrndetails.intItemId = ware_issuedetails.intItemId
				LEFT JOIN ware_issueheader ON ware_issuedetails.intIssueNo = ware_issueheader.intIssueNo AND ware_issuedetails.intIssueYear = ware_issueheader.intIssueYear
				WHERE  
				ware_mrnheader.intStatus = '1' AND 
				DATEDIFF(NOW(), ware_mrnheader.datdate) > 30 AND 
				ware_issueheader.intStatus = 1  
				group by 
				ware_mrnheader.intMrnNo,
				ware_mrnheader.intMrnYear
				
				) as TB2 on TB1.intMrnNo = TB2.intMrnNo and TB1.intMrnYear=TB2.intMrnYear 
				Having approvedStatus IS NULL";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$mrn_no		= $row['intMrnNo'];
			$mrn_year	= $row['intMrnYear'];
			
		 	$sql_u		= " UPDATE ware_mrnheader SET ware_mrnheader.intStatus = -2 WHERE ware_mrnheader.intMrnNo='$mrn_no' and ware_mrnheader.intMrnYear='$mrn_year'";
			$result_u 	= $db->RunQuery($sql_u);
			
			$header_array		=$obj_mrn_get->get_header($mrn_no,$mrn_year,'RunQuery');
			$res_budg_year		=$obj_comm_budget_get->loadFinanceYear_with_selected_date($header_array['LOCATION_ID'],$row['mrnDate'],'RunQuery');
			$row_b_year			=mysqli_fetch_array($res_budg_year)	;
			$budg_year			= $row_b_year['intId'];
 			//-------------------------------------------------
			$results_details	=$obj_mrn_get->get_details_results($mrn_no,$mrn_year,'RunQuery');
			while($row_details	=mysqli_fetch_array($results_details)){

				$item_price_mrn			= $obj_comm_budget_get->get_price_for_mrn_item($header_array['LOCATION_ID'],$row_details['ITEM_ID'],'RunQuery');
				$sub_cat_budget_flag	= $obj_comm_budget_get->check_sub_category_budgeted_or_not($header_array['LOCATION_ID'],$row_details['SUB_CATEGORY_ID'],'RunQuery');
				 if($sub_cat_budget_flag	==0){//item wise budget
					$budget_type			= $obj_comm_budget_get->load_budget_type($header_array['LOCATION_ID'],$row_details['SUB_CATEGORY_ID'],$row_details['ITEM_ID'],'RunQuery');
					if($budget_type == 'U'){ //unit budget
					$arr_mrn_item_tot_U[$row_details['ITEM_ID']]	+= $row_details['QTY'];
					$response_b		= $obj_budget_set->updatePlanDetails_units_or_amount($header_array['LOCATION_ID'],$header_array['DEPARTMENT_ID'],$budg_year,'',$row_details['ITEM_ID'],$row['mrnDate'],0,-$row_details['QTY'],'MRN','U','RunQuery');
					}
					else{					//amount budget
					$arr_mrn_item_tot_A[$row_details['ITEM_ID']]	+= ($row_details['QTY'])*$item_price_mrn;
					$response_b		= $obj_budget_set->updatePlanDetails_units_or_amount($header_array['LOCATION_ID'],$header_array['DEPARTMENT_ID'],$budg_year,'',$row_details['ITEM_ID'],$row['mrnDate'],-$row_details['QTY'],0,'MRN','A','RunQuery');
					}
					
				}
				else if($sub_cat_budget_flag	==1){//sub category wise budget
				
					$budget_type			= $obj_comm_budget_get->load_budget_type($header_array['LOCATION_ID'],$row_details['SUB_CATEGORY_ID'],0,'RunQuery');
					if($budget_type == 'U'){//unit budget
					$arr_mrn_sub_cat_tot_U[$row_details['SUB_CATEGORY_ID']]	+= $row_details['QTY'];
					$response_b		= $obj_budget_set->updatePlanDetails_units_or_amount($header_array['LOCATION_ID'],$header_array['DEPARTMENT_ID'],$budg_year,$row_details['SUB_CATEGORY_ID'],0,$row['mrnDate'],0,-$row_details['QTY'],'MRN','U','RunQuery');
					}
					else{					//amount budget
					$arr_mrn_sub_cat_tot_A[$row_details['SUB_CATEGORY_ID']]	+= ($row_details['QTY'])*$item_price_mrn;
					$response_b		= $obj_budget_set->updatePlanDetails_units_or_amount($header_array['LOCATION_ID'],$header_array['DEPARTMENT_ID'],$budg_year,$row_details['SUB_CATEGORY_ID'],0,$row['mrnDate'],-$row_details['QTY'],0,'MRN','A','RunQuery');
					}
				}
			}
			//-------------------------------------------------
			
		}
		//print_r($arr_mrn_item_tot_U);
//-----------------------------------
//partial clearance (Approved issues and MRN => days >90)
	$sql	= "SELECT
				ware_mrnheader.intMrnNo,
				ware_mrnheader.intMrnYear,
				ware_mrndetails.intOrderNo,
				ware_mrndetails.intOrderYear,
				ware_mrndetails.strStyleNo,
				ware_mrndetails.intItemId,
				mst_item.intMainCategory,
				mst_item.intSubCategory,
				mst_maincategory.strName as mainCatName,
				mst_subcategory.strName as subCatName,
				ware_mrnheader.intStatus,
				ware_issueheader.intStatus,
				DATE(ware_mrnheader.datdate) as mrnDate ,
				ware_mrndetails.dblQty,
				ware_mrndetails.dblIssudQty,
				ware_mrndetails.dblMRNClearQty,
				(ware_mrndetails.dblQty - ware_mrndetails.dblIssudQty) as toClearQty
				FROM
								ware_mrnheader
								INNER JOIN ware_mrndetails ON ware_mrnheader.intMrnNo = ware_mrndetails.intMrnNo AND ware_mrnheader.intMrnYear = ware_mrndetails.intMrnYear
								INNER JOIN ware_issuedetails ON ware_mrndetails.intMrnNo = ware_issuedetails.intMrnNo AND ware_mrndetails.intMrnYear = ware_issuedetails.intMrnYear AND ware_mrndetails.intOrderNo = ware_issuedetails.strOrderNo AND ware_mrndetails.intOrderYear = ware_issuedetails.intOrderYear AND ware_mrndetails.strStyleNo = ware_issuedetails.strStyleNo AND ware_mrndetails.intItemId = ware_issuedetails.intItemId
								INNER JOIN ware_issueheader ON ware_issuedetails.intIssueNo = ware_issueheader.intIssueNo AND ware_issuedetails.intIssueYear = ware_issueheader.intIssueYear
								Inner Join mst_item ON ware_mrndetails.intItemId = mst_item.intId
								Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
								Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
				WHERE
								ware_mrnheader.intStatus = '1' AND 
								DATEDIFF(NOW(), ware_mrnheader.datdate) > 90 AND 
								ware_issueheader.intStatus = 1 AND 
								(ware_mrndetails.dblQty - ware_mrndetails.dblIssudQty) > 0
				GROUP BY
				ware_mrnheader.intMrnNo,
				ware_mrnheader.intMrnYear,
				ware_mrndetails.intOrderNo,
				ware_mrndetails.intOrderYear,
				ware_mrndetails.strStyleNo,
				ware_mrndetails.intItemId
				";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$mrn_no		= $row['intMrnNo'];
			$mrn_year	= $row['intMrnYear'];
			$order_no	= $row['intOrderNo'];
			$order_year	= $row['intOrderYear'];
			$sales_order= $row['strStyleNo'];
			$item		= $row['intItemId'];
			$qty		= $row['toClearQty'];
			if($qty == ''){
				$qty	= 0;
			}
			
		 	$sql_u		= " UPDATE ware_mrndetails SET ware_mrndetails.dblMRNClearQty = dblMRNClearQty+$qty WHERE ware_mrndetails.intMrnNo='$mrn_no' AND ware_mrndetails.intMrnYear='$mrn_year' AND ware_mrndetails.intOrderNo='$order_no' AND ware_mrndetails.intOrderYear='$order_year' AND ware_mrndetails.strStyleNo='$sales_order' AND ware_mrndetails.intItemId='$item'";
			$result_u 	= $db->RunQuery($sql_u);
			
			
			//--------GET BUDGET UPDATION ARRAY-------------------------
				$header_array		=$obj_mrn_get->get_header($mrn_no,$mrn_year,'RunQuery');
				$res_budg_year		=$obj_comm_budget_get->loadFinanceYear_with_selected_date($header_array['LOCATION_ID'],$row['mrnDate'],'RunQuery');
				$row_b_year			=mysqli_fetch_array($res_budg_year)	;
				$budg_year			= $row_b_year['intId'];
				
				$header_array		=$obj_mrn_get->get_header($mrn_no,$mrn_year,'RunQuery');
				//if($rollBackFlag != 1){
					
						$item_price_mrn			= $obj_comm_budget_get->get_price_for_mrn_item($header_array['LOCATION_ID'],$item,'RunQuery');
						$sub_cat_budget_flag	= $obj_comm_budget_get->check_sub_category_budgeted_or_not($header_array['LOCATION_ID'],$rowc['intSubCategory'],'RunQuery');
						 if($sub_cat_budget_flag	==0){//item wise budget
							$budget_type			= $obj_comm_budget_get->load_budget_type($header_array['LOCATION_ID'],$row_details['SUB_CATEGORY_ID'],$row_details['ITEM_ID'],'RunQuery');
							if($budget_type == 'U'){ //unit budget
							$arr_mrn_item_tot_U[$row_details['ITEM_ID']]	+= $row_details['QTY'];
							$response_b		= $obj_budget_set->updatePlanDetails_units_or_amount($header_array['LOCATION_ID'],$header_array['DEPARTMENT_ID'],$budg_year,'',$row_details['ITEM_ID'],$row['mrnDate'],0,-$row_details['QTY'],'MRN','U','RunQuery');
							}
							else{					//amount budget
							$arr_mrn_item_tot_A[$row_details['ITEM_ID']]	+= ($row_details['QTY'])*$item_price_mrn;
							$response_b		= $obj_budget_set->updatePlanDetails_units_or_amount($header_array['LOCATION_ID'],$header_array['DEPARTMENT_ID'],$budg_year,'',$row_details['ITEM_ID'],$row['mrnDate'],-$row_details['QTY'],0,'MRN','A','RunQuery');
							}
							
						}
						else if($sub_cat_budget_flag	==1){//sub category wise budget
						
							$budget_type			= $obj_comm_budget_get->load_budget_type($header_array['LOCATION_ID'],$row_details['SUB_CATEGORY_ID'],0,'RunQuery');
							if($budget_type == 'U'){//unit budget
							$arr_mrn_sub_cat_tot_U[$row_details['SUB_CATEGORY_ID']]	+= $row_details['QTY'];
							$response_b		= $obj_budget_set->updatePlanDetails_units_or_amount($header_array['LOCATION_ID'],$header_array['DEPARTMENT_ID'],$budg_year,$row_details['SUB_CATEGORY_ID'],0,$row['mrnDate'],0,-$row_details['QTY'],'MRN','U','RunQuery');
							}
							else{					//amount budget
							$arr_mrn_sub_cat_tot_A[$row_details['SUB_CATEGORY_ID']]	+= ($row_details['QTY'])*$item_price_mrn;
							$response_b		= $obj_budget_set->updatePlanDetails_units_or_amount($header_array['LOCATION_ID'],$header_array['DEPARTMENT_ID'],$budg_year,$row_details['SUB_CATEGORY_ID'],0,$row['mrnDate'],-$row_details['QTY'],0,'MRN','A','RunQuery');
							}
						}
				//	}
			//--------END OF GET BUDGET UPDATION ARRAY----------------------
			
		}//END OF WHILE LOOP
		//print_r($arr_mrn_item_tot_U);
 //-----------
 
 
$db->commit();		
$db->disconnect();

  ?>
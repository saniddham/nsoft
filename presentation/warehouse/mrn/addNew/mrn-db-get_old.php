<?php
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
	ini_set('display_errors',0);
	ini_set('max_execution_time', 1111111111) ;
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$requestType 	= $_REQUEST['requestType'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	$extra_percentage = getExtraPercentage($company);

	//---------LOAD HEADER DATA--------------------------------------
	if($requestType=='loadHeaderData')
	{
		$grnNo  = $_REQUEST['grnNo'];
		$grnNoArray = explode('/',$grnNo);
		$grnNo 	 = $grnNoArray[0];
		$grnYear = $grnNoArray[1];
		
		$sql = "SELECT
				ware_grnheader.strInvoiceNo,
				trn_poheader.intSupplier,
				mst_supplier.strName
				FROM
				ware_grnheader
				Inner Join trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
				Inner Join mst_supplier ON trn_poheader.intSupplier = mst_supplier.intId
				WHERE
				ware_grnheader.intGrnNo =  '$grnNo' AND
				ware_grnheader.intGrnYear =  '$grnYear'";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['invoiceNo'] 	= $row['strInvoiceNo'];
			$response['supplier'] = $row['strName'];
		}
		echo json_encode($response);
	}
	//---------LOAD MAIN CATEGORIES--------------------------------------
	else if($requestType=='loadMainCategory')
	{
		$itemType  = $_REQUEST['itemType'];
		$sql = "SELECT mst_maincategory.intId,
							mst_maincategory.strName
							FROM mst_maincategory";
		if($itemType=='0')
		$sql .= " WHERE mst_maincategory.strName <> 'RAW MATERIAL'";	
		$sql .= " ORDER BY mst_maincategory.strName ASC";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	//---------LOAD SUB CATEGORIES--------------------------------------
	else if($requestType=='loadSubCategory')
	{
		$mainCategory  = $_REQUEST['mainCategory'];
		$sql = "SELECT
				mst_subcategory.intId,
				mst_subcategory.strCode,
				mst_subcategory.strName
				FROM mst_subcategory
				WHERE
				mst_subcategory.intMainCategory =  '$mainCategory' 
				ORDER BY mst_subcategory.strName ASC";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	//------LOAD ORDER NOS--------------------------------------
	else if($requestType=='loadOrderNo')
	{
		$orderYear  = $_REQUEST['orderYear'];
		$sql = "SELECT DISTINCT
							trn_orderheader.intOrderNo,
							trn_orderheader.intOrderYear
							FROM trn_orderheader 
							LEFT JOIN trn_orderheader as preCosting on 
							trn_orderheader.DUMMY_PO_NO = preCosting.intOrderNo and trn_orderheader.DUMMY_PO_YEAR = preCosting.intOrderYear 
							WHERE
							trn_orderheader.intOrderYear = '$orderYear' AND
							trn_orderheader.intStatus >  '0' AND trn_orderheader.intStatus < (trn_orderheader.intApproveLevelStart+1) 
							AND ( (trn_orderheader.PO_TYPE =0) or (trn_orderheader.PO_TYPE =2 and preCosting.INSTANT_ORDER <>1) OR (trn_orderheader.PO_TYPE =1 and trn_orderheader.INSTANT_ORDER =1))
							order by intOrderYear desc,intOrderNo desc";
							
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intOrderNo']."/".$row['intOrderYear']."\">".$row['intOrderNo']."/".$row['intOrderYear']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	//------LOAD SALES ORDER NOS--------------------------------------
	else if($requestType=='loadStyleNo')
	{
		$orderNo  = $_REQUEST['orderNo'];
		$orderNoArray 	 = explode('/',$orderNo);
		 $sql = "SELECT DISTINCT 
		 		trn_orderdetails.intSalesOrderId, 
				trn_orderdetails.strSalesOrderNo,
				trn_orderdetails.strCombo,
				mst_part.strName
				FROM
				trn_orderdetails
				Inner Join mst_part ON trn_orderdetails.intPart = mst_part.intId 
				WHERE 
				trn_orderdetails.intOrderNo =  '$orderNoArray[0]' AND
				trn_orderdetails.intOrderYear =  '$orderNoArray[1]'";
		$result = $db->RunQuery($sql);
 	
			$html .= "<tr id=\"\"><th><input name=\"chkSoAll\" id=\"chkSoAll\" class=\"chkSoAll\" type=\"checkbox\"></th><th><strong>Sales Orders</strong></th></tr>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<tr id=\"".$row['intSalesOrderId']."\"><td align=\"center\"><input name=\"chkSo\" id=\"chkSo\" class=\"chkSo\" type=\"checkbox\"></td><td id=\"".$row['intSalesOrderId']."\" class=\"normalfnt so\" nowrap>".$row['strSalesOrderNo']."/".$row['strName']."/".$row['strCombo']."</td></tr>";
		}
		echo $html;
		//echo json_encode($response);
	}
	//------LOAD GENERAL ITEMS--------------------------------------
	else if($requestType=='loadGeneralItems')
	{
		
 		$orderNo  = $_REQUEST['orderNo'];
		$orderNoArray 	 = explode('/',$orderNo);
		$orderNo		=$orderNoArray[0];
		$orderYear		=$orderNoArray[1];
		$salesOrderNo  = $_REQUEST['salesOrderNo'];
		
		$salesOrderNo_list='';
		$arr 		 = json_decode($_REQUEST['arr'], true);
		$arr_salesOrderNo 		 = json_decode($_REQUEST['salesOrderNo'], true);
		foreach($arr_salesOrderNo as $arrVal)
		{
			if($salesOrderNo_list!='')
			$salesOrderNo_list      .= ",".$arrVal['so'];
			else
			$salesOrderNo_list      .= $arrVal['so'];
		}
		$salesOrderNo_list;
		$mainCategory  = $_REQUEST['mainCategory'];
		$subCategory  = $_REQUEST['subCategory'];
		$description  = $_REQUEST['description'];
		$itemType  = $_REQUEST['itemType'];
		$bomItemPermission= getBomItemPermission($userId);
		
		if($salesOrderNo_list=='')
			$salesOrderNo_list=0;
				
		   $sql="SELECT
				mst_item.strName as itemName,
				mst_item.intId, 
				mst_item.intMainCategory,
				mst_item.intSubCategory,
				mst_maincategory.strName as mainCatName,
				mst_subcategory.strName as subCatName,
				mst_item.strCode , 
				mst_units.strCode as uom 
				FROM 
				mst_item  
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
				left join trn_po_prn_details_sales_order on trn_po_prn_details_sales_order.ORDER_NO = '$orderNo' and trn_po_prn_details_sales_order.ORDER_YEAR='$orderYear' 
				and trn_po_prn_details_sales_order.SALES_ORDER IN ($salesOrderNo_list) and trn_po_prn_details_sales_order.ITEM = mst_item.intId
				WHERE  
				mst_item.intStatus = '1' and trn_po_prn_details_sales_order.ORDER_NO is null "; 
						/*if($bomItemPermission==1){
							$sql.=" AND mst_item.intBomItem IN  ('0','1') "; 
						}
						else{
							$sql.=" AND mst_item.intBomItem =  '0' "; 
						}*/
				
				if($itemType=='5')
					$sql.=" AND mst_item.DIRECT_NON_DIRECT_RM_TYPE =  '0' ";
				if($mainCategory!='')
					$sql.=" AND mst_item.intMainCategory =  '$mainCategory' ";
				if($subCategory!='')
				$sql.=" AND mst_item.intSubCategory =  '$subCategory'";
				if($description!=''){
				$sql.=" AND
				mst_item.strName LIKE  '%$description%'";
				}
				$sql.=" Order by mst_maincategory.strName asc, mst_subcategory.strName asc, mst_item.strName asc";
		//echo $sql;
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			 $stockBalQty=getStockBalance_bulk($location,$row['intId']);
			 //$priQty=getPRIQty($orderNo,$orderYear,$salesOrderNo,$item);
			 
			$data['itemId'] 	= $row['intId'];
			$data['maincatId'] = $row['intMainCategory'];
			$data['subCatId'] 	= $row['intSubCategory'];
			$data['mainCatName'] = $row['mainCatName'];
			$data['subCatName'] = $row['subCatName'];
			$data['code'] = $row['strCode'];
			$data['uom'] = $row['uom'];
			$data['itemName'] = $row['itemName'];
			
			$salesOrderPRIQty_list='';
			$salesOrderBalQty_list='';
			foreach($arr_salesOrderNo as $arrVal)
			{
				if($salesOrderPRIQty_list!='')
				$salesOrderPRIQty_list      .= ",0";
				else
				$salesOrderPRIQty_list      .= 0;
				
				if($salesOrderBalQty_list!='')
				$salesOrderBalQty_list      .= ",0";
				else
				$salesOrderBalQty_list      .= 0;
			}
		
			$data['salesOrderNo_list'] 		= $salesOrderNo_list;
			$data['salesOrderPRIQty_list'] 	= $salesOrderPRIQty_list;
			$data['salesOrderBalQty_list'] 	= $salesOrderBalQty_list;
			$data['requiredQty'] 			= 0;
			$data['allocatedQty'] 			= 0;
			$data['mrnQty'] 				= 0;
			$data['issueQty'] 				= 0;
			$data['actualPriQty'] 			= 0;
			
			
			$data['stockBal'] = round($stockBalQty,4);
			
			$arrCombo[] = $data;
		}
		if($arrCombo==null){
			$arrCombo[0]="none";
		}
		$response['arrCombo'] 	= $arrCombo;
		echo json_encode($response);
	}

	//------LOAD BOM ITEMS--------------------------------------
	else if($requestType=='loadBomItems')
	{
		$itemType  			= $_POST['itemType'];
		$orderNo  			= $_POST['orderNo'];
		$orderNoArray 	 	= explode('/',$orderNo);
		$salesOrderNo  		= $_POST['salesOrderNo'];
		$order_type			= get_order_type($orderNoArray[0],$orderNoArray[1]);
		$instant			= get_instant_type($orderNoArray[0],$orderNoArray[1]);
		$instantPre			= get_instant_pre_type($orderNoArray[0],$orderNoArray[1]);
		
		$salesOrderNo_list='';
		$salesOrderNo 		 = json_decode($_POST['salesOrderNo'], true);
		foreach($salesOrderNo as $arrVal)
		{
			if($salesOrderNo_list!=''){
			$salesOrderNo_list      .= ",".$arrVal['so'];
 			}
			else{
			$salesOrderNo_list      .= $arrVal['so'];
 			}
		}
		//echo $salesOrderNo_list."sdsda";
		
		$mainCategory = $_POST['mainCategory'];
		$subCategory  = $_POST['subCategory'];
		$description  = $_POST['description'];
		
		//---------------------
		if(($orderNoArray[0]!='') /*&& ($salesOrderNo!='')*/){
			
			
			$result = getBomItems($orderNoArray[0],$orderNoArray[1],$salesOrderNo_list,$mainCategory,$subCategory,$description);
			while($row=mysqli_fetch_array($result))
			{
				//to get pri qtys
				$salesOrderQty_list='';
				$salesOrderNo1 		 = json_decode($_POST['salesOrderNo'], true);
				$k=0;
				foreach($salesOrderNo1 as $arrVal1)
				{
					if($salesOrderQty_list!=''){
					$salesOrderQty_list      .= ",".getRequiredQty($orderNoArray[0],$orderNoArray[1],$arrVal1['so'],$row['intId']);
					}
					else{
					$salesOrderQty_list      .= getRequiredQty($orderNoArray[0],$orderNoArray[1],$arrVal1['so'],$row['intId']);
					}
					$k++;
				}
				//
				
				//to get bal to mrn qtys
				$salesOrderBalQty_list='';
				$salesOrderNo1 		 = json_decode($_POST['salesOrderNo'], true);
				$k=0;
				$actualPriQty=0;
				$tot_actualPriQty_s1 =0;
				foreach($salesOrderNo1 as $arrVal1)
				{
					$priQty				= round(getPRIQty($orderNoArray[0],$orderNoArray[1],$arrVal1['so'],$row['intId']),4);
					$priQty		  		= round(($priQty)*(100+$extra_percentage)/100,4);
					$mrnQty				= round(getMRNQty($orderNoArray[0],$orderNoArray[1],$arrVal1['so'],$row['intId']),4);
					
 					//2017-07-06 (allow to mn for pre costing)
					//$actualPriQty_s		= $priQty-$mrnQty;//previous figure
					if($order_type==0 || ($instant!=1 && $order_type==1)|| ($instantPre!=1 && $order_type==2)){//other or pre costing non instant or split from non instant dummy
						$actualPriQty_s	=$priQty-$mrnQty; 
					}
					else if($order_type==1){//pre costing , instant
						$priQty_pre =$priQty;
						$mrnQty_pre =$mrnQty;
						$priQty_act =0;
						$mrnQty_act =0;
						$priQty_tot =0;
						$mrnQty_tot =0;
						
					    $result_act = get_actual_details($orderNoArray[0],$orderNoArray[1],$arrVal1['so']);
						while($row_act=mysqli_fetch_array($result_act)){
							$priQty_A	= round(getPRIQty($row_act['ORDER_NO'],$row_act['ORDER_YEAR'],$row_act['SO'],$row['intId']),4);
							$priQty_A	= round(($priQty_A)*(100+$extra_percentage)/100,4);
							$mrnQty_A	= round(getMRNQty($row_act['ORDER_NO'],$row_act['ORDER_YEAR'],$row_act['SO'],$row['intId']),4);
							$priQty_act +=$priQty_A;
							$mrnQty_act +=$mrnQty_A;
						}
						$priQty_tot 	=$priQty_act+$priQty_pre;
						$mrnQty_tot 	=$mrnQty_act+$mrnQty_pre;
						$actualPriQty_s	=($priQty_tot-$mrnQty_tot);
					}  
					else if($order_type==2){//actual , instant
						
						$actualPriQty_s	= 0; // does not allow to mrn for instant+split
						
					}  
					// end 2017-07-06 (allow to mn for pre costing)
					
					$actualPriQty_s1	= getRoundedWeight($actualPriQty_s,$row['intId']);
					$actualPriQty_1_s	= $actualPriQty_s;
					if($actualPriQty_s<0)
						$actualPriQty_s=0;
					if($actualPriQty_s1<0)
						$actualPriQty_s1=0;
					if($salesOrderBalQty_list!=''){
						$salesOrderBalQty_list      .= ",".$actualPriQty_s1;
					}
					else{
						$salesOrderBalQty_list      .= $actualPriQty_s1;
					}
					$actualPriQty +=$actualPriQty_s;
					$tot_actualPriQty_s1 += $actualPriQty_s1;
					
					$k++;
				}
				
				//
				 
				
				$salesOrderNo	 	= $row['intSalesOrderId'];
				$data['salesOrderNo_list'] 	= $salesOrderNo_list;
				$data['salesOrderPRIQty_list'] 	= $salesOrderQty_list;
				$data['salesOrderBalQty_list'] 	= $salesOrderBalQty_list;
 				$data['itemId'] 	= $row['intId'];
				$data['maincatId'] = $row['intMainCategory'];
				$data['subCatId'] 	= $row['intSubCategory'];
				$data['mainCatName'] = $row['mainCatName'];
				$data['subCatName'] = $row['subCatName'];
				$data['code'] = $row['strCode'];
				$data['uom'] = $row['uom'];
				$data['itemName'] = $row['itemName'];
				
				$data['requiredQty'] 	= '';
				$data['allocatedQty'] 	= '';
				$data['mrnQty'] 	= '';
				$data['issueQty'] 	= '';
				//$data['stockBal'] = round(getStockBalance($company,$location,$orderNoArray[0],$orderNoArray[1],$salesOrderNo,$row['intId']),4); 
				$data['stockBal'] = round(getStockBalance_bulk($location,$row['intId']),4); //Route warehouse bulk quantities for order wise mrns'
				/*$priQty			= round(getPRIQty($orderNoArray[0],$orderNoArray[1],$salesOrderNo_list,$row['intId']),4);
				$mrnQty			= round(getMRNQty($orderNoArray[0],$orderNoArray[1],$salesOrderNo_list,$row['intId']),4);
				$actualPriQty	= $priQty-$mrnQty;*/
				$actualPriQty_1	= $actualPriQty;
				$actualPriQty	= getRoundedWeight($actualPriQty,$row['intId']);
				
				if($actualPriQty<0 || $actualPriQty==''){
					$actualPriQty=0;
				}
				if($actualPriQty_1<0 || $actualPriQty_1==''){
					$actualPriQty_1=0;
				}
				//$data['actualPriQty']=round($actualPriQty,4);
				$data['actualPriQty']=round($tot_actualPriQty_s1,4);
				$data['actualPriQty_1']=round($actualPriQty_1,4);
				$data['x']=$x;
				
				$arrCombo[] = $data;
			}
		}
		
		if($arrCombo==null){
			$arrCombo[0]="none";
		}
		
		$response['arrCombo'] 	= $arrCombo;
		echo json_encode($response);

	}
//---------------------------------------------------------------------------

else if($requestType=='loadAdditionalBomItems')
	{
		$itemType  = $_REQUEST['itemType'];
		$orderNo  = $_REQUEST['orderNo'];
		$orderNoArray 	 = explode('/',$orderNo);
		$salesOrderNo  = $_REQUEST['salesOrderNo'];
		$mainCategory  = $_REQUEST['mainCategory'];
		$subCategory  = $_REQUEST['subCategory'];
		$description  = $_REQUEST['description'];
		
		//---------------------
		if(($orderNoArray[0]!='') && ($salesOrderNo!='')){
			
			
			$result = getAdditionalBomItems($orderNoArray[0],$orderNoArray[1],$salesOrderNo,$mainCategory,$subCategory,$description);
			while($row=mysqli_fetch_array($result))
			{
				$data['itemId'] 	= $row['intId'];
				$data['maincatId'] = $row['intMainCategory'];
				$data['subCatId'] 	= $row['intSubCategory'];
				$data['mainCatName'] = $row['mainCatName'];
				$data['subCatName'] = $row['subCatName'];
				$data['code'] = $row['strCode'];
				$data['uom'] = $row['uom'];
				$data['itemName'] = $row['itemName'];
				
				
				$data['requiredQty'] 	= '';
				$data['allocatedQty'] 	= '';
				$data['mrnQty'] 	= '';
				$data['issueQty'] 	= '';
				//$data['stockBal'] = round(getStockBalance($company,$location,$orderNoArray[0],$orderNoArray[1],$salesOrderNo,$row['intId']),4); 
				$data['stockBal'] = round(getStockBalance_bulk($location,$row['intId']),4); //Route warehouse bulk quantities for order wise mrns'
				$priQty=round(getPRIQty($orderNoArray[0],$orderNoArray[1],$salesOrderNo,$row['intId']),4);
				$mrnQty=round(getMRNQty($orderNoArray[0],$orderNoArray[1],$salesOrderNo,$row['intId']),4);
				
				
				$actualPriQty=$priQty-$mrnQty;
				if($actualPriQty<0){
					$actualPriQty=0;
				}
				$data['actualPriQty']=round($actualPriQty,4);
				
				
				$arrCombo[] = $data;
			}
		}
		
		if($arrCombo==null){
			$arrCombo[0]="none";
		}
		
		$response['arrCombo'] 	= $arrCombo;
		echo json_encode($response);

	}
//---------------------------------------------------------------------------

//----------------------------FUNCTIONS--------------------------------------
	function getStockBalance($company,$location,$orderNo,$orderYear,$salesOrderNo,$item)
	{
		global $db;
		  $sql = "SELECT
				Sum(ware_stocktransactions.dblQty) as stockBal 
				FROM ware_stocktransactions
				WHERE
				ware_stocktransactions.intCompanyId =  '$company' AND
				ware_stocktransactions.intLocationId =  '$location' AND
				ware_stocktransactions.intOrderNo =  '$orderNo' AND
				ware_stocktransactions.intOrderYear =  '$orderYear' AND
				ware_stocktransactions.intSalesOrderId =  '$salesOrderNo' AND
				ware_stocktransactions.intItemId =  '$item'";

		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
	function getStockBalance_bulk($location,$item)
	{
		global $db;
		   $sql = "SELECT
				Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$location'
				GROUP BY
				ware_stocktransactions_bulk.intItemId";
				
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
	function getPRIQty($orderNo,$orderYear,$salesOrderNo,$item)
	{
		global $db;
		   $sql = "	SELECT
						sum(IFNULL(trn_po_prn_details_sales_order.REQUIRED,0)) AS priQty
					FROM
						trn_po_prn_details_sales_order
					WHERE
						trn_po_prn_details_sales_order.ORDER_NO = '$orderNo'
					AND trn_po_prn_details_sales_order.ORDER_YEAR = '$orderYear'
					AND trn_po_prn_details_sales_order.SALES_ORDER IN ($salesOrderNo)
					AND trn_po_prn_details_sales_order.ITEM = '$item'";
		//echo $sql;
				
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return val($row['priQty']);	
	}
	//--------------------------------------------------------------
	function getMRNQty($orderNo,$orderYear,$salesOrderNo,$item)
	{
		global $db;
		   $sql = "SELECT
				IFNULL(sum(IFNULL(ware_mrndetails.dblQty,0)),'0') AS mrnQty
			FROM
				ware_mrndetails
			INNER JOIN ware_mrnheader ON ware_mrndetails.intMrnNo = ware_mrnheader.intMrnNo
			AND ware_mrndetails.intMrnYear = ware_mrnheader.intMrnYear
			WHERE
				ware_mrndetails.intOrderNo = '$orderNo'
			AND ware_mrndetails.intOrderYear = '$orderYear'
			AND ware_mrndetails.strStyleNo IN ($salesOrderNo)
			AND ware_mrndetails.intItemId = '$item'
			AND ware_mrnheader.intStatus = '1'";
		//echo $sql;
				
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return val($row['mrnQty']);	
	}
	//--------------------------------------------------------------
	function getBomItems($orderNo,$orderYear,$salesOrderNo,$mainCategory,$subCategory,$description)
	{
		global $db;
			  	$sql = "(
						SELECT DISTINCT
							mst_item.strName AS itemName,
							mst_item.intId,
							mst_item.intMainCategory,
							mst_item.intSubCategory,
							mst_maincategory.strName AS mainCatName,
							mst_subcategory.strName AS subCatName,
							mst_item.strCode,
							mst_units.strCode AS uom,
							trn_orderdetails.intSalesOrderId ,
							concat(trn_orderdetails.strSalesOrderNo,'/',mst_part.strName) as  strSalesOrderNo 
						FROM
							(
						select * from trn_po_prn_details_sales_order 
						WHERE  
						trn_po_prn_details_sales_order.ORDER_NO='$orderNo' AND 
						trn_po_prn_details_sales_order.ORDER_YEAR='$orderYear' ";
						if($salesOrderNo > 0)
						$sql.=" AND trn_po_prn_details_sales_order.SALES_ORDER IN ($salesOrderNo)";
						
						$sql.=") as MAIN "; 
						
						$sql.=" INNER JOIN trn_orderdetails ON 
						trn_orderdetails.intOrderNo=MAIN.ORDER_NO AND 
						trn_orderdetails.intOrderYear=MAIN.ORDER_YEAR AND 
						trn_orderdetails.intSalesOrderId=MAIN.SALES_ORDER   
						INNER JOIN mst_item ON mst_item.intId=MAIN.ITEM 
						INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
						INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
						INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
						Inner Join mst_part ON trn_orderdetails.intPart = mst_part.intId 
						WHERE
							trn_orderdetails.intOrderNo = '$orderNo'
						AND trn_orderdetails.intOrderYear = '$orderYear' ";
						if($salesOrderNo > 0)
						$sql.=" AND trn_orderdetails.intSalesOrderId IN ($salesOrderNo) ";
						
						$sql.=" AND mst_item.intMainCategory = '$mainCategory'
						AND mst_item.intStatus = '1'";
						
						if($subCategory!='')
						$sql.=" AND mst_item.intSubCategory =  '$subCategory'";
						if($description!=''){
						$sql.=" AND
						mst_item.strName LIKE  '%$description%'";
						}
						$sql.=" group by mst_item.intId  
								Order by mst_maincategory.strName asc, mst_subcategory.strName asc, mst_item.strName asc
						)";
				
					//echo $sql;

		$result = $db->RunQuery($sql);
		return $result;
	}
	//-------------------------------------------------------------
	function getBomItemPermission($user){
		global $db;
		   $sql = "SELECT *
				FROM sys_bomitem_users
				WHERE
				sys_bomitem_users.intUserId =  '$user'
				";
		$result = $db->RunQuery($sql);
		if($result){
			return 1;
		}
		else{
			return 0;
		}
	}
	//--------------------------------------------------------------
	
//--------------------------------------------------------------
	function getAdditionalBomItems($orderNo,$orderYear,$salesOrderNo,$mainCategory,$subCategory,$description)
	{
		global $db;
				$sql="SELECT
				mst_item.strName as itemName,
				mst_item.intId, 
				mst_item.intMainCategory,
				mst_item.intSubCategory,
				mst_maincategory.strName as mainCatName,
				mst_subcategory.strName as subCatName,
				mst_item.strCode , 
				mst_units.strCode as uom 
				FROM 
				mst_item  
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
				WHERE  
				mst_item.intStatus = '1' "; 
						/*if($bomItemPermission==1){
							$sql.=" AND mst_item.intBomItem IN  ('0','1') "; 
						}
						else{
							$sql.=" AND mst_item.intBomItem =  '0' "; 
						}*/
				
				if($mainCategory!='')
					$sql.=" AND mst_item.intMainCategory =  '$mainCategory' ";
				if($subCategory!='')
				$sql.=" AND mst_item.intSubCategory =  '$subCategory'";
				if($description!='')
				$sql.=" AND mst_item.strName LIKE  '%$description%'";
				
				$sql.=" AND mst_item.intId NOT IN (
						SELECT DISTINCT
							mst_item.intId
						FROM
							(
						select * from trn_po_prn_details_sales_order 
						WHERE  
						trn_po_prn_details_sales_order.ORDER_NO='$orderNo' AND 
						trn_po_prn_details_sales_order.ORDER_YEAR='$orderYear'AND 
						trn_po_prn_details_sales_order.SALES_ORDER='$salesOrderNo'
						) as MAIN				
						INNER JOIN trn_orderdetails ON 
						trn_orderdetails.intOrderNo=MAIN.ORDER_NO AND 
						trn_orderdetails.intOrderYear=MAIN.ORDER_YEAR AND 
						trn_orderdetails.intSalesOrderId=MAIN.SALES_ORDER   
						INNER JOIN mst_item ON mst_item.intId=MAIN.ITEM 
						INNER JOIN mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
						INNER JOIN mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
						INNER JOIN mst_units ON mst_item.intUOM = mst_units.intId
						WHERE
							trn_orderdetails.intOrderNo = '$orderNo'
						AND trn_orderdetails.intOrderYear = '$orderYear'
						AND trn_orderdetails.intSalesOrderId = '$salesOrderNo'
						AND mst_item.intMainCategory = '$mainCategory'
						AND mst_item.intStatus = '1'";
						
						if($subCategory!='')
						$sql.=" AND mst_item.intSubCategory =  '$subCategory'";
						if($description!=''){
						$sql.=" AND
						mst_item.strName LIKE  '%$description%'";
						}
				
				
				$sql.=")";
				
				$sql.=" Order by mst_maincategory.strName asc, mst_subcategory.strName asc, mst_item.strName asc";
		
					//echo $sql;

		$result = $db->RunQuery($sql);
		return $result;
	}
	
	
	function getRoundedWeight($qty, $item){
    global $db;

    $sql = "SELECT
			mst_item.strName,
			mst_item.intUOM 
			FROM `mst_item`
			WHERE
			mst_item.intId = '$item'
			";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $uom	= ($row['intUOM']);
	
	if($uom ==6 && floor($qty)==0){//only for weight < 1kg
	
	if($qty<0.005 && $qty>=0.0001)
		$qty=0.005;
		//$y	=((round($qty,3)*1000)%100);
		$y	= ((floor($qty * 1000) / 1000)*1000)%100;
		$z	=ceil($y/10)*10;
		$a	=$z-$y;
		
		if($a > 5)
			$val1=0;
		else if($a>0)
			$val1=5;
		else
			$val1	=0;
			
			$val2 	=  floor((floor($qty * 1000) / 1000)*100)/100;
			$val	= $val2+$val1/1000;
		
	}
	else
	$val	=$qty;
 	//echo $qty.'/'.$y.'/'.$z.'/'.$a.'/'.$val1.'/'.$val2.'/'.$val.'</br>';
	return $val;
}

function getRequiredQty($orderNo,$year,$so,$item){
	global $db;
    $sql = "SELECT
			trn_po_prn_details_sales_order.REQUIRED
			FROM `trn_po_prn_details_sales_order`
			WHERE 
			trn_po_prn_details_sales_order.ORDER_NO = '$orderNo' AND
			trn_po_prn_details_sales_order.ORDER_YEAR = '$year' AND
			trn_po_prn_details_sales_order.SALES_ORDER = '$so' AND
			trn_po_prn_details_sales_order.ITEM = '$item'
			GROUP BY
			trn_po_prn_details_sales_order.ORDER_NO,
			trn_po_prn_details_sales_order.ORDER_YEAR,
			trn_po_prn_details_sales_order.SALES_ORDER,
			trn_po_prn_details_sales_order.ITEM

			";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
	//$actualPriQty_s	= getRoundedWeight($row['REQUIRED'],$item);
	$actualPriQty_s	= $row['REQUIRED'];
	if($row['REQUIRED']=='')
    return 0;
	else
	return $actualPriQty_s;
}
function getExtraPercentage($company){
	global $db;	
    $sql = "SELECT
			mst_companies.EXTRA_MRN_PERCENTAGE 
			FROM `mst_companies`
			WHERE
			mst_companies.intId = '$company'
			";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    return ($row['EXTRA_MRN_PERCENTAGE']);
}

function get_order_type($no,$year){
	
	global $db;	
    $sql = "SELECT
			trn_orderheader.PO_TYPE 
			FROM `trn_orderheader`
			WHERE
			trn_orderheader.intOrderNo = '$no' and 
			trn_orderheader.intOrderYear = '$year'
			";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    return ($row['PO_TYPE']);
	
}


function get_instant_type($no,$year){
	
	global $db;	
    $sql = "SELECT
			trn_orderheader.INSTANT_ORDER 
			FROM `trn_orderheader`
			WHERE
			trn_orderheader.intOrderNo = '$no' and 
			trn_orderheader.intOrderYear = '$year'
			";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    return ($row['INSTANT_ORDER']);
	
}

function get_instant_pre_type($no,$year){
	
	global $db;	
    $sql = "SELECT
			oh2.INSTANT_ORDER
			FROM
			trn_orderheader AS oh1
			INNER JOIN trn_orderheader AS oh2 ON oh1.DUMMY_PO_NO = oh2.intOrderNo AND oh1.DUMMY_PO_YEAR = oh2.intOrderYear
			WHERE
			oh1.intOrderNo = '$no' AND
			oh1.intOrderYear = '$year'";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    return ($row['INSTANT_ORDER']);
	
}

function get_actual_details($no,$year,$so){
	
	global $db;	
    $sql = "SELECT
			trn_orderdetails.intOrderNo AS ORDER_NO,
			trn_orderdetails.intOrderYear AS ORDER_YEAR,
			trn_orderdetails.intSalesOrderId AS SO 
			FROM
			trn_orderdetails
			INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
			WHERE
			trn_orderheader.DUMMY_PO_NO = '$no' AND
			trn_orderheader.DUMMY_PO_YEAR = '$year' AND
			trn_orderdetails.DUMMY_SO_ID = '$so'
			";
    $result = $db->RunQuery($sql);
    return $result;
}

function get_pre_details($no,$year,$so){
	global $db;	
    $sql = "SELECT
			trn_orderheader.DUMMY_PO_NO AS ORDER_NO,
			trn_orderheader.DUMMY_PO_YEAR AS ORDER_YEAR,
			trn_orderdetails.DUMMY_SO_ID AS SO 
			FROM
			trn_orderdetails
			INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
			WHERE
			trn_orderheader.intOrderNo = '$no' AND
			trn_orderheader.intOrderYear = '$year' AND
			trn_orderdetails.intSalesOrderId = '$so'
			";
    $result = $db->RunQuery($sql);
	return $row = mysqli_fetch_array($result);
}

 	//-------------------------------------------------------------
?>

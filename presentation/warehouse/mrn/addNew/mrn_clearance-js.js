var basePath	="presentation/warehouse/mrn/addNew/";
			
$(document).ready(function() {
	
	$("#frmMrnClearance").validationEngine();
   //-------------------------------------------------------
 
  $('#frmMrnClearance #butClear').click(function(){
	if ($('#frmMrnClearance').validationEngine('validate'))   
    { 
		showWaiting();

		var requestType = '';
		var data = "requestType=clear";
			data+="&serialNo="		+	$('#cboMrnNo').val();
			data+="&Year="	+	$('#cboMrnYear').val();


			var rowCount = document.getElementById('tblMrn').rows.length;
			if(rowCount==1){
				alert("No items to clear");
				hideWaiting();
				return false;				
			}
			var row = 0;
			
			var arr="[";
			
			$('#tblMrn .item').each(function(){
	
				var orderNo = 	$(this).parent().find(".orderNo").html();
				var salesOrderId = 	$(this).parent().find(".salesOrderNo").attr('id');
				var itemId	= $(this).attr('id');
				var mrnQty	= parseFloat($(this).parent().find(".mrnQty").html());
				var clearQty	= parseFloat($(this).parent().find(".Qty").val());
				mrnQty=mrnQty-clearQty;
				
					
					if(clearQty>0){
						row++;
						arr += "{";
						arr += '"orderNo":"'+		orderNo +'",' ;
						arr += '"salesOrderId":"'+		salesOrderId +'",' ;
						arr += '"itemId":"'+		itemId +'",' ;
						arr += '"mrnQty":"'+		mrnQty +'",' ;
						arr += '"clearQty":"'+		clearQty  +'"' ;
						arr +=  '},';
					}
			});
			
			if(row==0){
				alert("Please enter 'Qty' to clear");
				hideWaiting();
				return false;
			}
			
			
			
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;

		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = basePath+"mrn-db-set.php";
     	var obj = $.ajax({
			url:url,
			type:'post',
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmMrnClearance #butClear').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t=setTimeout("alertxC()",1000);
						return;
					}
				},
			error:function(xhr,status){
					
					$('#frmMrnClearance #butClear').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertxC()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
	  hideWaiting();
   });
	
 //----------------------------------------
	$('.delImg').click(function(){
		$(this).parent().parent().remove();
	});
});

function alertxC()
{
	$('#frmMrnClearance #butClear').validationEngine('hide')	;
	document.location.href=document.location.href;
}

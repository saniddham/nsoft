<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
///////////////////////////////
// H B G KORALA   ////////////
//////////////////////////////
$mainPath 			= $_SESSION['mainPath'];
$location 			= $_SESSION['CompanyID'];
$company 			= $_SESSION['headCompanyId'];
$thisFilePath 		= $_SERVER['PHP_SELF'];
$intUser  			= $_SESSION["userId"];

$mrnNo 				= $_REQUEST['mrnNo'];
$year 				= $_REQUEST['year'];
$editMode			= 0;
$confirmatonMode	= 0;

$programName		='Material Request Note';
$programCode		='P0229';

$userDepartment		= getUserDepartment($intUser);


if(($mrnNo=='')&&($year=='')){
	$savedStat 		= '';
	$department		=loadDefaultDepartment($intUser);
	$remarks		='';
	$intStatus		=$savedStat+1;
	$date			='';
	$type			='4';  //general - 0, sample - 1, technical - 2, production - 3
}
else{
	$result=loadHeader($mrnNo,$year);
	while($row=mysqli_fetch_array($result))
	{
		$department		  = $row['department'];
		$remarks		  = $row['strRemarks'];
		$date 			  = $row['datdate'];
		$savedStat		  = $row['intApproveLevels'];
		$intStatus		  = $row['intStatus'];
		$mrnType	      = $row['mrnType'];
	}
}
	
	$editMode			=loadEditMode($programCode,$intStatus,$savedStat,$intUser,$userDepartment);
	$confirmatonMode	=loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser);
	if($mrnNo==''){
		$confirmatonMode	=0;	
	}
	$mainPage	=$backwardseperator."main.php";
	
$extra_percentage = getExtraPercentage($company);
?> 

<title>Material Requisition Note</title>
<?php
//include "include/javascript.html";
?>
<!--<script type="text/javascript" src="presentation/warehouse/mrn/addNew/mrn-js.js"></script>
--> 

<form id="frmMrn" name="frmMrn" method="post" action="">
<table width="100%" border="0" align="center" bgcolor="#FFFFFF">

</table>
<div align="center">
		<div class="trans_layoutL" style="width:1100px">
		  <div class="trans_text">Material Requisition Note</div>
		  <table width="100%" border="0" align="center" bgcolor="#FFFFFF"><tr><td>
   <table width="100%" border="0">
   <tr><td><table width="100%" class="tableBorder_allRound">
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="10%" height="22" class="normalfnt">MRN No</td>
            <td width="22%"><input name="txtMrnNo" type="text" disabled="disabled" class="txtText" id="txtMrnNo" style="width:60px" value="<?php echo $mrnNo ?>" /><input name="txtYear" type="text" disabled="disabled" class="txtText" id="txtYear" style="width:40px"value="<?php echo $year ?>"  /></td>
            <td width="17%"><div id="divItem" style="display:none"><input name="txtItemType" type="text" disabled="disabled" class="txtText" id="txtItemType" style="width:40px" value="<?php echo $type ?>"  /></div></td>
            <td width="25%">&nbsp;</td>
            <td width="8%" class="normalfnt">Date</td>
            <td width="18%"><input name="dtDate" type="text" value="<?php echo date("Y-m-d"); ?>" class="txtbox" id="dtDate" style="width:120px;" onMouseDown="DisableRightClickEvent();" onMouseOut="EnableRightClickEvent();" onKeyPress="return ControlableKeyAccess(event);"  onclick="return showCalendar(this.id, '%Y-%m-%d');" <?php if($editMode==0){  ?> disabled="disabled"<?php } ?>/><input type="reset" value=""  class="txtbox" style="visibility:hidden;"   onclick="return showCalendar(this.id, '%Y-%m-%');" /></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="10%" height="27" class="normalfnt">Department</td>
            <td width="39%"><select name="cboDepartment" id="cboDepartment" style="width:250px"  class="validate[required] txtText" <?php if($editMode==0){  ?> disabled="disabled"<?php } ?>>
                  <option value=""></option>
                  <?php
					$sql = "SELECT
							mst_department.intId,
							mst_department.strName
							FROM
							mst_department
							WHERE
							mst_department.intStatus =  '1'";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						if($row['intId']==$department)
						echo "<option value=\"".$row['intId']."\" selected=\"selected\">".$row['strName']."</option>";	
						else
						echo "<option value=\"".$row['intId']."\">".$row['strName']."</option>";	
					}
				?>
            </select></td>
            <td width="25%">&nbsp;</td>
            <td width="11%" class="normalfnt">&nbsp;</td>
            <td width="15%">&nbsp;</td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
<td width="10%" height="27" rowspan="2" class="normalfnt">Note</td>
            <td width="41%" rowspan="2"><textarea name="txtRemarks" id="txtRemarks" cols="35" rows="3" <?php if($editMode==0){  ?> disabled="disabled"<?php } ?>><?php echo $remarks; ?></textarea></td>
            <td width="27%" rowspan="2"><div id="divPrice" style="display:none"><?php echo $priceEdit; ?></div></td>
            <td width="12%" rowspan="2" valign="top" class="normalfnt">&nbsp;</td>
            <td width="10%" valign="bottom"><img <?php if($editMode!=1){ ?>style="display:none"<?php } ?> src="images/Tadd.jpg" width="92" height="24" id="butAddItems" name="butAddItems" /></td>
          </tr>
        </table></td>
      </tr>
      </table></td></tr>
       <tr> <td><div style="width:1100px;height:600px;overflow:scroll" >
          <table width="100%" class="bordered" id="tblMrn" >
            <tr>
              <th width="5%" height="22" >Del</th>
              <th width="5%" >Order No</th>
              <th width="5%" >Graphic No</th>
              <th width="17%" >Sales Order  No</th>
              <th width="14%" >Main Category</th>
              <th width="7%" >Sub Category</th>
              <th width="23%" >Item Description</th>
              <th width="4%">UOM</th>
              <th width="5%">Balance MRN Qty</th>
              <th width="8%">Qty</th>
			  <th width="10%">Extra Qty</th>
			  <th width="10%">Total Qty</th>
              </tr>
            <?php
			     $sql = "SELECT
				ware_mrndetails.dblQty, 
				mst_item.strName as itemName,
				mst_item.intBomItem,
				mst_item.intId as intItemNo, 
				mst_item.intMainCategory,
				mst_item.intSubCategory,
				mst_maincategory.strName as mainCatName,
				mst_subcategory.strName as subCatName,
				ware_mrndetails.intOrderNo,
				ware_mrndetails.intOrderYear,
				ware_mrndetails.strStyleNo as strSalesOrderId,  
				ware_mrndetails.EXTRA_QTY,
				ware_mrndetails.MRN_TYPE,
				trn_orderdetails.strSalesOrderNo, 
				trn_orderdetails.strGraphicNo, 
				mst_item.strCode, 
				mst_units.strCode as uom , 
				mst_part.strName as part,
				trn_orderheader.INSTANT_ORDER,
				trn_orderheader.PO_TYPE,
                                mst_item.ITEM_HIDE
                               FROM
				ware_mrndetails 
				left Join trn_orderdetails ON ware_mrndetails.intOrderNo = trn_orderdetails.intOrderNo AND ware_mrndetails.intOrderYear = trn_orderdetails.intOrderYear AND ware_mrndetails.strStyleNo = trn_orderdetails.intSalesOrderId
				Inner Join mst_item ON ware_mrndetails.intItemId = mst_item.intId
				Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
				Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
				Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
				left Join mst_part ON trn_orderdetails.intPart = mst_part.intId
				LEFT JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
                               
				WHERE 
				/*mst_item.intStatus = '1' AND*/ 
				ware_mrndetails.intMrnNo =  '$mrnNo' AND
				ware_mrndetails.intMrnYear =  '$year'  
				ORDER BY mst_maincategory.strName asc, mst_subcategory.strName asc, mst_item.strName asc ";
                             //echo $sql;
				$result = $db->RunQuery($sql);
				$totAmm=0;
				while($row=mysqli_fetch_array($result))
				{
                                        $graphicNumber=$row['strGraphicNo'];
					$mainCatName=$row['mainCatName'];
					$subCatName=$row['subCatName'];
					$mainCatId=$row['intMainCategory'];
					$subCatId=$row['intSubCategory'];
					$itemId=$row['intItemNo'];
					$itemName=$row['itemName'];
					$uom=$row['uom'];
					$part=$row['part'];
					$allocatedQty=$row['dblAllocatedQty'];
					$mrnQty=$row['dblMrnQty'];
					$extraQty = $row['EXTRA_QTY'];
					$balToMrnQty=$allocatedQty-$mrnQty;
                                        $MRN_TYPE = $row['MRN_TYPE'];
                                        $ITEM_HIDE = $row['ITEM_HIDE'];
					if($row['intBomItem']==1){
						$balToMrnQty=10000;
					}
					$Qty=$row['dblQty'];
					$QtyWithoutExtra = round($Qty - $extraQty,5);
					$priQty=round(getPRIQty($row['intOrderNo'],$row['intOrderYear'],$row['strSalesOrderId'],$row['intItemNo']),4);
					$mrnQty=round(getMRNQty($row['intOrderNo'],$row['intOrderYear'],$row['strSalesOrderId'],$row['intItemNo']),4);
					$actualPriQty=round(($priQty)*(100+$extra_percentage)/100,4) -$mrnQty;
					
					if($row['PO_TYPE']==1 && $row['INSTANT_ORDER']==1){
						$priQty_split =0;
						$mrnQty_split =0;
						$actualPriQty_split=0;
						$result_sp	  = get_split_orders($row['intOrderNo'],$row['intOrderYear'],$row['strSalesOrderId']);
						while($row_sp=mysqli_fetch_array($result_sp))
						{
							$priQty_split =round(getPRIQty($row_sp['intOrderNo'],$row_sp['intOrderYear'],$row_sp['intSalesOrderId'],$row['intItemNo']),4);
							$mrnQty_split =round(getMRNQty($row_sp['intOrderNo'],$row_sp['intOrderYear'],$row_sp['intSalesOrderId'],$row['intItemNo']),4);
							
							$actualPriQty_split +=round(($priQty_split)*(100+$extra_percentage)/100,4) -$mrnQty_split;
							
						}
						//echo $actualPriQty.'/'.$actualPriQty_split;
						$actualPriQty +=$actualPriQty_split;
					}
						
						
					
					if($actualPriQty<0){
						$actualPriQty=0;
					}
					
					$orderNo=$row['intOrderNo']."/".$row['intOrderYear'];
					$salesOrderId=$row['strSalesOrderId'];
					$salesOrderNo=$row['strSalesOrderNo']."/".$part;
					if($row['intOrderNo']==0){
						$orderNo='';
						$salesOrderId='';
						$salesOrderNo='';
						$priQty='';
						$mrnQty='';
						$actualPriQty='';
					}

			if($date>='2017-03-29')
			$actualPriQty=getRoundedWeight($actualPriQty, $row['intItemNo']);
			
					
				?>
		<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF"><?php if($editMode==1){  ?><img class="delImg" src="images/del.png" width="15" height="15" /><?php } ?><span style="visibility: hidden" id="popupItemType"><?php echo $MRN_TYPE; ?></span></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $orderNo ?>" class="orderNo"><?php echo $orderNo ?></td>
            <td align="center" bgcolor="#FFFFFF" id="<?php echo $graphicNumber ?>" class="orderNo"><?php echo $graphicNumber ?></td>
            <td align="center" bgcolor="#FFFFFF" id="<?php echo $salesOrderId ?>" class="salesOrderNo"><?php echo $salesOrderNo ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $maincatId ?>"><?php echo $mainCatName ?></td>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $subCatId ?>"><?php echo $subCatName ?></td>
                        <?php
                        if($ITEM_HIDE==1)
                        {
                        ?>
                        <td align="center" bgcolor="#FFFFFF" id="<?php echo $itemId ?>" class="item">*****</td>
                        <?php
                        }else{
                         ?>
			<td align="center" bgcolor="#FFFFFF" id="<?php echo $itemId ?>" class="item"><?php echo $itemName ?></td>
                        <?php
                        }
                        ?>
			<td align="center" bgcolor="#FFFFFF" id=""><?php echo $uom ?></td>
            <td align="center" bgcolor="#FFFFFF" class="actualPriQty" id="<?php echo $actualPriQty ?>"><?php echo $actualPriQty ?></td>
			<?php if($MRN_TYPE == 3){ ?>
			<td align="center" bgcolor="#FFFFFF" id=""><input onkeyup="checkQty(this)" id="<?php echo $QtyWithoutExtra ?>" class="validate[required,custom[number],max[<?php echo $actualPriQty; ?>]] calculateValue Qty" style="width:80px;text-align:center" type="text" value="<?php echo $QtyWithoutExtra ?>"   disabled="disabled" /></td>
			<td align="center" bgcolor="#FFFFFF" id=""><input  id="ExtBal" class="validate[required,custom[number]] calculateValue ExtraQty" style="width:80px;text-align:center" type="text" value="<?php echo $extraQty; ?>" disabled="disabled" /></td>
			<td align="center" bgcolor="#FFFFFF" id=""><input onkeyup="checkQtyParent(this)" id="<?php echo $totQty ?>" class="validate[required,custom[number]] calculateValue totQty" style="width:80px;text-align:center" type="text" value="<?php echo $Qty ?>" <?php if($editMode==0){  ?> disabled="disabled"<?php } ?>/></td>
           <?php } else {?>
			<td align="center" bgcolor="#FFFFFF" id=""><input  id="<?php echo $Qty ?>" class="validate[required,custom[number]] calculateValue Qty" style="width:80px;text-align:center" type="text" value="0"   disabled="disabled" /></td>
			<td align="center" bgcolor="#FFFFFF" id=""><input  id="ExtBal" class="validate[required,custom[number]] calculateValue " style="width:80px;text-align:center" type="text" value="<?php echo $extraQty ?>"   disabled="disabled"/></td>
			<td align="center" bgcolor="#FFFFFF" id=""><input onkeyup="checkQtyParent(this)" id="<?php echo $Qty ?>" class="validate[required,custom[number]] calculateValue totQty" style="width:80px;text-align:center" type="text" value="<?php echo $Qty ?>"    /></td>
        	<?php } ?>

		</tr> <?php
	} ?>
          </table>
        </div></td>
       </tr>
       <tr>
           <td align="center" class=""><table width="100%" border="0">
                   <tr>
                       <td rowspan="2"><iframe id="iframeFiles" src="presentation/warehouse/mrn/addNew/filesUpload.php?txtFolder=<?php echo "$mrnNo-$year".'&editMode='.$editMode; ?>" name="iframeFiles" style="width:400px;height:175px;border:none"  ></iframe></td>
                       <td width="11%" class="normalfnt">&nbsp;</td>
                       <td width="10%">&nbsp;</td>
                       <td width="11%" class="normalfnt">&nbsp;</td>
                       <td width="11%">&nbsp;</td>
                   </tr>
                   <tr>
                       <td class="normalfnt">&nbsp;</td>
                       <td>&nbsp;</td>
                       <td class="normalfnt">&nbsp;</td>
                       <td>&nbsp;</td>
                   </tr>
               </table></td>
       </tr>
      <tr>
        <td align="center" class="tableBorder_allRound">
        <a name="butNew"  class="button white medium" id="butNew">New</a>
         <?php if(($form_permision['add']||$form_permision['edit']) && $editMode==1){ ?>
         <a id="butSave" name="butSave"  class="button white medium">Save</a>
		 <?php } ?>
         <a id="butConfirm" name="butConfirm" style=" <?php if(($intStatus>1) and ($confirmatonMode==1) && $confirmatonMode==1){  echo 'display:yes'; } else { ?>  <?php echo 'display:none';  } ?>" class="button white medium">Approve</a>
         <a id="butReport" name="butReport"  class="button white medium">Report</a>
         <a href="main.php"  class="button white medium">Close</a></td>
      </tr>
    </table></td>
    </tr>
  </table>

  </div>
  </div>
</form>
	<!-- items to prn -->
	<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1">
   <!-- <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php" style="width:800;height:800;border:0;overflow:hidden">
    </iframe>-->
    </div>

	<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>

<?php
//------------------------------function load Header---------------------
function loadHeader($mrnNo,$year)
{
	global $db;
	  $sql = "SELECT
			ware_mrnheader.datdate,
			ware_mrnheader.intStatus,
			ware_mrnheader.intApproveLevels,
			ware_mrnheader.intUser,
			ware_mrnheader.strRemarks,
			ware_mrnheader.APPROVAL_REMARKS,
			sys_users.strUserName, 
			ware_mrnheader.intDepartment as department,
			ware_mrnheader.mrnType 
			FROM
			ware_mrnheader 
			Inner Join mst_locations ON ware_mrnheader.intCompanyId = mst_locations.intId
			Inner Join mst_companies ON mst_locations.intCompanyId = mst_companies.intId
			Inner Join sys_users ON ware_mrnheader.intUser = sys_users.intUserId
			WHERE
			ware_mrnheader.intMrnNo =  '$mrnNo' AND
			ware_mrnheader.intMrnYear =  '$year' 
			GROUP BY
			ware_mrnheader.intMrnNo,
			ware_mrnheader.intMrnYear"; 
			$result = $db->RunQuery($sql);
			return $result;
}
//------------------------------function load Default Department---------------------
function loadDefaultDepartment($intUser){
	global $db;
	 $sql1 = "SELECT
				sys_users.intDepartmentId
				FROM
				sys_users
				WHERE
				sys_users.intUserId =  '$intUser'";	
	$result1 = $db->RunQuery($sql1);
	$row1=mysqli_fetch_array($result1);
	$department=$row1['intDepartmentId'];
	return $department;
}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser,$depatment){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	  $sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId 
		Inner Join sys_users ON sys_users.intUserId = menupermision.intUserId
		WHERE 
		sys_users.intDepartmentId =  '$depatment' 
		AND menus.strCode =  '$programCode' 
		AND menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
			 
	return $editMode;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	$sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
//------------------------------function load User Department---------------------
function getUserDepartment($intUser){
	global $db;

	$sqlp = "SELECT
			sys_users.intDepartmentId
			FROM
			sys_users
			WHERE
			sys_users.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['intDepartmentId'];

}
//------------------------------------------------------------

function getPRIQty($orderNo,$orderYear,$salesOrderNo,$item)
	{
		global $db;
		   $sql = "	SELECT
						IFNULL(trn_po_prn_details_sales_order.REQUIRED,'0') AS priQty
					FROM
						trn_po_prn_details_sales_order
					WHERE
						trn_po_prn_details_sales_order.ORDER_NO = '$orderNo'
					AND trn_po_prn_details_sales_order.ORDER_YEAR = '$orderYear'
					AND trn_po_prn_details_sales_order.SALES_ORDER = '$salesOrderNo'
					AND trn_po_prn_details_sales_order.ITEM = '$item'";
		//echo $sql;
				
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$priQty=$row['priQty'];
		//return val($row['priQty']);	
		return $priQty;
	}
//--------------------------------------------------------------
function getMRNQty($orderNo,$orderYear,$salesOrderNo,$item)
	{
		global $db;
		   $sql = "SELECT
				IFNULL(sum(ware_mrndetails.dblQty),'0') AS mrnQty
			FROM
				ware_mrndetails
			INNER JOIN ware_mrnheader ON ware_mrndetails.intMrnNo = ware_mrnheader.intMrnNo
			AND ware_mrndetails.intMrnYear = ware_mrnheader.intMrnYear
			WHERE
				ware_mrndetails.intOrderNo = '$orderNo'
			AND ware_mrndetails.intOrderYear = '$orderYear'
			AND ware_mrndetails.strStyleNo = '$salesOrderNo'
			AND ware_mrndetails.intItemId = '$item'
			AND ware_mrnheader.intStatus = '1'";
		//echo $sql;
				
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$mrnQty = $row['mrnQty'];
		//return val($row['mrnQty']);	
		return $mrnQty;
	}
//--------------------------------------------------------------
	function getRoundedWeight($qty, $item){
    global $db;

    $sql = "SELECT
			mst_item.strName,
			mst_item.intUOM 
			FROM `mst_item`
			WHERE
			mst_item.intId = '$item'
			";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $uom	= ($row['intUOM']);
	
	if($uom ==6 && floor($qty)==0){//only for weight < 1kg
	
	if($qty<0.005 && $qty>=0.0001)
		$qty=0.005;
		//$y	=((round($qty,3)*1000)%100);
		$y	= ((floor($qty * 1000) / 1000)*1000)%100;
		$z	=ceil($y/10)*10;
		$a	=$z-$y;
		
		if($a > 5)
			$val1=0;
		else if($a>0)
			$val1=5;
		else
			$val1	=0;
			
			$val2 	=  floor((floor($qty * 1000) / 1000)*100)/100;
			$val	= $val2+$val1/1000;
		
	}
	else
	$val	=$qty;
 	//echo $qty.'/'.$y.'/'.$z.'/'.$a.'/'.$val1.'/'.$val2.'/'.$val.'</br>';
	return $val;
}

function getExtraPercentage($company){
	global $db;	
    $sql = "SELECT
			mst_companies.EXTRA_MRN_PERCENTAGE 
			FROM `mst_companies`
			WHERE
			mst_companies.intId = '$company'
			";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    return ($row['EXTRA_MRN_PERCENTAGE']);
}

function get_split_orders($orderNo,$year,$so){
	
	global $db;	
    $sql = "SELECT
			trn_orderdetails.intOrderNo,
			trn_orderdetails.intOrderYear,
			trn_orderdetails.intSalesOrderId
			FROM
			trn_orderdetails
			INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
			WHERE
			trn_orderheader.DUMMY_PO_NO = '$orderNo' AND
			trn_orderheader.DUMMY_PO_YEAR = '$year' AND
			trn_orderdetails.DUMMY_SO_ID = '$so'
			";
    $result = $db->RunQuery($sql);
	return $result;
	
}


?>
<script>
var currentItemType = '<?php echo $mrnType ?>';
</script>

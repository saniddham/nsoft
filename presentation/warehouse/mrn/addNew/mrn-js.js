var basePath	="presentation/warehouse/mrn/addNew/";
var reportId	="938";
$(document).ready(function() {
	//$(document).off();
	
	$("#frmMrn").validationEngine();

	$('#frmMrn #cboSupplier').focus();
	//------
	$("#frmMrn #butAddItems").click(function(){
		var rowCount = document.getElementById('tblMrn').rows.length;
		if(rowCount==1){
			currentItemType = '';
		}
		if($('#cboDepartment').val()==''){
			alert("Please select the deparment");
			return false;
		}
		closePopUp();
		loadPopup();
	});
	$("#frmMrnPopup .totQty").die('keyup').live('keyup',function(){
   		checkQty(this);	
	});
	
	$("#frmMrn .totQty").die('keyup').live('keyup',function(){
   		checkQtyParent(this);	
	});
	
	//-------------------------	
 
  $('#frmMrn #butSave').click(function(){
	var requestType = '';
	if ($('#frmMrn').validationEngine('validate'))   
    { 
		showWaiting();
		
		var data = "requestType=save";
		
		var arrHeader = "{";
							arrHeader += '"serialNo":"'+$('#txtMrnNo').val()+'",' ;
							arrHeader += '"Year":"'+$('#txtYear').val()+'",' ;
							arrHeader += '"department":"'+$('#cboDepartment').val()+'",' ;
							arrHeader += '"remarks":'+URLEncode_json($('#txtRemarks').val())+',' ;
							arrHeader += '"type":"'+currentItemType+'",' ;
							arrHeader += '"date":"'+$('#dtDate').val()+'"' ;
			arrHeader += ' }';

			var rowCount = document.getElementById('tblMrn').rows.length;
			if(rowCount==1){
				alert("items not selected to MRN");
				hideWaiting();
				return false;				
			}
			var row = 0;
			var errorFlagQty=0;
			
			var arr="[";
			
			$('#tblMrn .item').each(function(){
	
				var orderNo = 	$(this).parent().find(".orderNo").html();
				var salesOrderId = 	$(this).parent().find(".salesOrderNo").attr('id');
				var itemId	= $(this).attr('id');
				var Quantity	= $(this).parent().find(".totQty").val();
				var extraQty    = $(this).parent().find("#ExtBal").val();
                var popupItemType = $(this).parent().find("#popupItemType").text();
					if(Quantity > 0 || extraQty > 0 ){
					    arr += "{";
						arr += '"orderNo":"'+		orderNo +'",' ;
						arr += '"salesOrderId":"'+		salesOrderId +'",' ;
						arr += '"itemId":"'+		itemId +'",' ;
						arr += '"Qty":"'+		Quantity  +'",' ;
						arr += '"extraQty":"'+		extraQty  +'",' ;
                        arr += '"popupItemType":"'+		popupItemType  +'"' ;
						arr +=  '},';
					}
					if(Quantity <= 0 && extraQty <= 0 ){
						errorFlagQty=1;	
					}
			});

			if(errorFlagQty==1){
				alert("Please enter MRN Quantities.");
				hideWaiting();
				return false;
			}
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			data+="&arrHeader="	+	arrHeader;
			data+="&arr="	+	arr;
		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = basePath+"mrn-db-set.php";
     	var obj = $.ajax({
			url:url,
			type:'post',
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmMrn #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					hideWaiting();
					if(json.type=='pass')
					{
						var t=setTimeout("alertx()",9000);
						$('#txtMrnNo').val(json.serialNo);
						$('#txtYear').val(json.year);
						if(json.confirmationMode==1){
							$('#butConfirm').show();
						}
						else{
							$('#butConfirm').hide();
						}
						//return;
						alert('The MRN '+json.serialNo+'/'+json.year+' is saved successfully');
						window.location.href = '?q=229&mrnNo='+json.serialNo+'&year='+json.year;						//return;
					}
				},
			error:function(xhr,status){
					hideWaiting();
					$('#frmMrn #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",5000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
   
  //-------------------------------------------------------
 
  $('#frmMrnClearance #butClear').click(function(){
	if ($('#frmMrnClearance').validationEngine('validate'))   
    { 
		var requestType = '';
		var data = "requestType=clear";
			data+="&serialNo="		+	$('#cboMrnNo').val();
			data+="&Year="	+	$('#cboMrnYear').val();


			var rowCount = document.getElementById('tblMrn').rows.length;
			if(rowCount==1){
				alert("No items to clear");
				return false;				
			}
			var row = 0;
			
			var arr="[";
			
			$('#tblMrn .item').each(function(){
	
				var orderNo = 	$(this).parent().find(".orderNo").html();
				var salesOrderId = 	$(this).parent().find(".salesOrderNo").attr('id');
				var itemId	= $(this).attr('id');
				var mrnQty	= parseFloat($(this).parent().find(".mrnQty").html());
				var clearQty	= parseFloat($(this).parent().find(".Qty").val());
				mrnQty=mrnQty-clearQty;
				
					
					if(clearQty>0){
						arr += "{";
						arr += '"orderNo":"'+		orderNo +'",' ;
						arr += '"salesOrderId":"'+		salesOrderId +'",' ;
						arr += '"itemId":"'+		itemId +'",' ;
						arr += '"mrnQty":"'+		mrnQty +'",' ;
						arr += '"clearQty":"'+		clearQty  +'"' ;
						arr +=  '},';
					}
			});
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;

		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = basePath+"mrn-db-set.php";
     	var obj = $.ajax({
			url:url,
			type:'post',
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmMrnClearance #butClear').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						var t=setTimeout("alertxC()",1000);
						return;
					}
				},
			error:function(xhr,status){
					
					$('#frmMrnClearance #butClear').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertxC()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
	}
   });
	
//--------------refresh the form----------
$('#frmMrn #butNew').click(function(){
		window.location.href = "?q=229";
});
//----------------------------------------
$('#frmMrn .delImg').click(function(){
	$(this).parent().parent().remove();
});

$('#frmMrnClearance .delImg').click(function(){
	$(this).parent().parent().remove();
});

//-----------------------------------
$('#butReport').click(function(){
	if($('#txtMrnNo').val()!=''){
		window.open('?q='+reportId+'&mrnNo='+$('#txtMrnNo').val()+'&year='+$('#txtYear').val());	
	}
	else{
		alert("There is no MRN to view");
	}
});
//----------------------------------	
$('#butConfirm').click(function(){
	if($('#txtMrnNo').val()!=''){
		window.open('?q='+reportId+'&mrnNo='+$('#txtMrnNo').val()+'&year='+$('#txtYear').val()+'&mrnType='+currentItemType+'&approveMode=1');
	}
	else{
		alert("There is no MRN to confirm");
	}
});
//-----------------------------------------------------
$('#butClose').click(function(){
		//load('main.php');	
});
//--------------refresh the form-----------------------
	$('#frmMrn #butNew').click(function(){
		window.location.href = "?q=229";
		clearRows();
		$('#frmMrn #txtMrnNo').val('');
		$('#frmMrn #txtYear').val('');
		$('#frmMrn #cboDepartment').removeAttr('disabled');
		$('#frmMrn #dtDate').removeAttr('disabled');
		$('#frmMrn #cboDepartment').val('');
		$('#frmMrn #cboDepartment').focus();
		$('#frmMrn #txtRemarks').val('');
		
		var currentTime = new Date();
		var month = currentTime.getMonth()+1 ;
		var day = currentTime.getDate();
		var year = currentTime.getFullYear();
		if(day<10)
		day='0'+day;
		if(month<10)
		month='0'+month;
		d=year+'-'+month+'-'+day;
		
		$('#frmMrn #dtDate').val(d);
	});
 //-------------------------------------------- 
 $('#frmMrnPopup .chkSoAll').die('click').live('click',function(){
 		if(document.getElementById('chkSoAll').checked==true)
		 var chk=true;
		else
		 var chk=false;
 		var rowCount = document.getElementById('salesOrderTbl').rows.length;
		for(var i=1;i<rowCount;i++)
		{
			//alert(document.getElementById('salesOrderTbl').rows[i].cells[0].childNodes[0].html());
			document.getElementById('salesOrderTbl').rows[i].cells[0].childNodes[0].checked=chk;
		}
	});
//-----------------------------------------------------
});

////////////----end of ready ----//////////////////////////////////

//-------------------------------------------------------
function loadPopup()
{
		popupWindow3('1');
		//currentItemType = $('#frmMrn #txtItemType').val();
		//alert("currentItemType "+currentItemType);
		var rowCount = document.getElementById('tblMrn').rows.length;
			
				$('#popupContact1').load(basePath+'mrnPopup.php?type='+currentItemType,function(){
				$('#butAdd').click(addClickedRows);
				$('#butClose1').click(disablePopup);
				
				/*if((currentItemType==3 || currentItemType==6 )&& rowCount>1){
				$('#frmMrnPopup #cboItemType').val('3').prop('selected', true);			
				$('#frmMrnPopup #cboItemType').prop('disabled', true);
				var url 		= basePath+"mrn-db-get.php?requestType=loadMainCategory&itemType=3";
				var httpobj 	= $.ajax({url:url,async:false})
				document.getElementById('cboMainCategory').innerHTML=httpobj.responseText;
				}*/
				/*if(currentItemType==6 && rowCount>1){
				$('#frmMrnPopup #cboItemType').val('6').prop('selected', true);			
				$('#frmMrnPopup #cboItemType').prop('disabled', true);
				var url 		= basePath+"mrn-db-get.php?requestType=loadMainCategory&itemType=6";
				var httpobj 	= $.ajax({url:url,async:false})
				document.getElementById('cboMainCategory').innerHTML=httpobj.responseText;
				}*/
				/* if(currentItemType==5 && rowCount>1){
				$('#frmMrnPopup #cboItemType').val('5').prop('selected', true);
				$('#frmMrnPopup #cboItemType').prop('disabled', true);
				$('#frmMrnPopup #cboOrderNo').val('');
				$('#frmMrnPopup #cbStyleNo').val('');
				document.getElementById('rw1').style.display='none';
				document.getElementById('rw2').style.display='none';
				var url 		= basePath+"mrn-db-get.php?requestType=loadMainCategory&itemType=5";
				var httpobj 	= $.ajax({url:url,async:false})
				document.getElementById('cboMainCategory').innerHTML=httpobj.responseText;
				}
				if(currentItemType==2 && rowCount>1){
				$('#frmMrnPopup #cboItemType').val('2').prop('selected', true);
				$('#frmMrnPopup #cboItemType').prop('disabled', true);
				$('#frmMrnPopup #cboOrderNo').val('');
				$('#frmMrnPopup #cbStyleNo').val('');
				document.getElementById('rw1').style.display='none';
				document.getElementById('rw2').style.display='none';
				var url 		= basePath+"mrn-db-get.php?requestType=loadMainCategory&itemType=2";
				var httpobj 	= $.ajax({url:url,async:false})
				document.getElementById('cboMainCategory').innerHTML=httpobj.responseText;
				}
				if(currentItemType==1 && rowCount>1){
				$('#frmMrnPopup #cboItemType').val('1').prop('selected', true);
				$('#frmMrnPopup #cboItemType').prop('disabled', true);
				$('#frmMrnPopup #cboOrderNo').val('');
				$('#frmMrnPopup #cbStyleNo').val('');
				document.getElementById('rw1').style.display='none';
				document.getElementById('rw2').style.display='none';
				var url 		= basePath+"mrn-db-get.php?requestType=loadMainCategory&itemType=1";
				var httpobj 	= $.ajax({url:url,async:false})
				document.getElementById('cboMainCategory').innerHTML=httpobj.responseText;
				}
				if(currentItemType==0 && rowCount>1){
				$('#frmMrnPopup #cboItemType').val('0').prop('selected', true);
				$('#frmMrnPopup #cboItemType').prop('disabled', true);
				$('#frmMrnPopup #cboOrderNo').val('');
				$('#frmMrnPopup #cbStyleNo').val('');
				document.getElementById('rw1').style.display='none';
				document.getElementById('rw2').style.display='none';
				var url 		= basePath+"mrn-db-get.php?requestType=loadMainCategory&itemType=0";
				var httpobj 	= $.ajax({url:url,async:false})
				document.getElementById('cboMainCategory').innerHTML=httpobj.responseText;
				} */
				 //-------------------------------------------- 
				  $('#frmMrnPopup #cboItemType').change(function(){
					    clearPopupRows();
						var itemType = $('#cboItemType').val();
						if(itemType==0 || itemType==1 || itemType==2 || itemType==4 || itemType==5 || itemType==7 || itemType==8){
						$('#frmMrnPopup #cboOrderNo').val('');
						$('#frmMrnPopup #cbStyleNo').val('');
						document.getElementById('rw1').style.display='none';
						document.getElementById('rw2').style.display='none';
						document.getElementById('rw3').style.display='none';
						}
						else{
						document.getElementById('rw1').style.display='';
						document.getElementById('rw2').style.display='';
						document.getElementById('rw3').style.display='';
						}
						
						var url 		= basePath+"mrn-db-get.php?requestType=loadMainCategory&itemType="+itemType;
						var httpobj 	= $.ajax({url:url,async:false})
						document.getElementById('cboMainCategory').innerHTML=httpobj.responseText;
						
				  });
				 //-------------------------------------------- 
				  $('#frmMrnPopup #cboMainCategory').change(function(){
						var mainCategory = $('#cboMainCategory').val();
						var url 		= basePath+"mrn-db-get.php?requestType=loadSubCategory&mainCategory="+mainCategory;
						var httpobj 	= $.ajax({url:url,async:false})
						document.getElementById('cboSubCategory').innerHTML=httpobj.responseText;
						
				  });
				 //-------------------------------------------- 
				 $('#frmMrnPopup  #chkAll').click(function(){
						if(document.getElementById('chkAll').checked==true)
						 var chk=true;
						else
						 var chk=false;
						
						var rowCount = document.getElementById('tblItemsPopup').rows.length;
						for(var i=1;i<rowCount;i++)
						{
							document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].checked=chk;
						}
					});
				 //-------------------------------------------- 
					 $('#frmMrnPopup .chkSoAll').die('click').live('click',function(){
 						if(document.getElementById('chkSoAll').checked==true)
						 var chk=true;
						else
						 var chk=false;
 						var rowCount = document.getElementById('salesOrderTbl').rows.length;
						for(var i=1;i<rowCount;i++)
						{
							//alert(document.getElementById('salesOrderTbl').rows[i].cells[0].childNodes[0].html());
							document.getElementById('salesOrderTbl').rows[i].cells[0].childNodes[0].checked=chk;
						}
					});
				//-------------------------------------------- 
				  $('#frmMrnPopup #cboOrderYear').change(function(){
						var orderYear = $('#cboOrderYear').val();
						var url 		= basePath+"mrn-db-get.php?requestType=loadOrderNo&orderYear="+orderYear;
						var httpobj 	= $.ajax({url:url, async:false});
                      	var response = JSON.parse(httpobj.responseText);
						document.getElementById('cboOrderNo').innerHTML=response["orderNos"];
                      	document.getElementById('cboGraphicNo').innerHTML=response["graphicNos"];
                        document.getElementById('salesOrderTbl').innerHTML="";

				  });
				 //-------------------------------------------- 
				  $('#frmMrnPopup #cboOrderNo').change(function(){
						var orderNo = $('#cboOrderNo').val();
                        var graphicNo = $('#cboGraphicNo').val();
						var url 		= basePath+"mrn-db-get.php?requestType=loadStyleNo&orderNo="+orderNo;
						var httpobj 	= $.ajax({url:url,async:false});
                      	var response = JSON.parse(httpobj.responseText);
						//alert(httpobj.responseText);
						document.getElementById('salesOrderTbl').innerHTML=response["salesOrders"];


                        document.getElementById('cboGraphicNo').innerHTML=response["graphicNos"];

                        var grphicOptionLength = document.getElementById("cboGraphicNo").length;
                        if (grphicOptionLength == 2){
                            $("select[name='cboGraphicNo'] option:eq(1)").attr("selected", "selected");
                        }


				  });
                    //--------------------------------------------
                    $('#frmMrnPopup #cboGraphicNo').change(function(){
                        var graphicNo = $('#cboGraphicNo').val();
                        var orderYear = $('#cboOrderYear').val();
                        var orderNo = $('#cboOrderNo').val();
                        var url 		= basePath+"mrn-db-get.php?requestType=loadGraphicNo&graphicNo=" + graphicNo + "&orderYear=" + orderYear;
                        var httpobj 	= $.ajax({url:url,async:false});
                        var response = JSON.parse(httpobj.responseText);
                        document.getElementById('cboOrderNo').innerHTML=response["orderNos"];

                    });
				 //--------------------------------------------
				  $('#frmMrnPopup #imgSearchItems').click(function(){
					  	
						var so_selected_flag = 0;
						var rowCount = document.getElementById('tblItemsPopup').rows.length;
						for(var i=1;i<rowCount;i++)
						{
							document.getElementById('tblItemsPopup').deleteRow(1);
						}
						var itemType = $('#cboItemType').val();
						var orderYear = $('#cboOrderYear').val();
						var orderNo = $('#cboOrderNo').val();
						var salesOrderId = '';
						var salesOrderNo = '';
						var mainCategory = $('#cboMainCategory').val();
						var subCategory = $('#cboSubCategory').val();
						var description = $('#txtItmDesc').val();
                        var graphicNo = $('#cboGraphicNo').val();


						//salesOrders
						var arr="[";
						var i=0;
						var salesOrderId_list	='';
						var salesOrderNo_list	='';
						$('#salesOrderTbl .chkSo').each(function(){
							i++;
 							var salesOrderId = 	$(this).parent().parent().find('.so').attr('id');
 							var salesOrderNo = 	$(this).parent().parent().find('.so').html();
								if(document.getElementById('salesOrderTbl').rows[i].cells[0].childNodes[0].checked==true){
									so_selected_flag=1;
									arr += "{";
									arr += '"so":"'+		salesOrderId  +'"' ;
									arr +=  '},';
									if(salesOrderId_list==''){
									salesOrderId_list += salesOrderId;
									salesOrderNo_list += salesOrderNo;
									}
									else {
									salesOrderId_list += ','+salesOrderId;
									salesOrderNo_list += ','+salesOrderNo;
									}
								}
 						});
						arr = arr.substr(0,arr.length-1);
						arr += " ]";
						//						
						if(itemType==4){
								alert("Please select the item type");
								return false;
						}
						else if(itemType==0 || itemType==1 || itemType==2 || itemType==5 || itemType==7 || itemType==8){//genaral items
							if(mainCategory==''){
								alert("Please select the main category");
								return false;
								//comment by roshan 2012-09-27 // users request they want to search items without selecting category //
							}
							else if((itemType==1 || itemType==2 || itemType==5 || itemType==7 || itemType==8)&& subCategory==''){
								alert("Please select the sub category");
								return false;
								//comment by roshan 2012-09-27 // users request they want to search items without selecting category //
							}
						}
						else if(itemType==3){//bom items
							if(orderYear==''){
								alert("Please select Order Year");
								return false;
							}
							else if(orderNo==''){
								alert("Please select Order No");
								return false;
							}
							else if(so_selected_flag==0){
								alert("Please select atleast one Sales Order");
								return false;
							} 
							else if(mainCategory==''){
								alert("Please select the main category");
								return false;
							}
						}
						else if(itemType==6){//bom items
							if(orderYear==''){
								alert("Please select Order Year");
								return false;
							}
							else if(orderNo==''){
								alert("Please select Order No");
								return false;
							} 
							else if(so_selected_flag==0){
								alert("Please select atleast one Sales Order");
								return false;
							} 
							else if(mainCategory==''){
								alert("Please select the main category");
								return false;
							}
							else if(subCategory==''){
								alert("Please select the sub category");
								return false;
								//comment by roshan 2012-09-27 // users request they want to search items without selecting category //
							}
						}
						
						
						showWaiting();
						
						if(itemType==0 || itemType==1 || itemType==2 || itemType==5 || itemType==6 || itemType==7 || itemType==8){//genaral items
						var url 		= basePath+"mrn-db-get.php?requestType=loadGeneralItems";
						}
						else if(itemType==3){//bom items
						var url 		= basePath+"mrn-db-get.php?requestType=loadBomItems";
						}
						/*else if(itemType==6){//bom items
						var url 		= basePath+"mrn-db-get.php?requestType=loadAdditionalBomItems";
						}*/
						
						var httpobj = $.ajax({
							url:url,
							dataType:'json',
							data:"orderNo="+orderNo+"&mainCategory="+mainCategory+"&subCategory="+subCategory+"&salesOrderNo="+arr+"&description="+description+"&itemType="+itemType,
							async:false,
							type: "post",
							success:function(json){

								var length = json.arrCombo.length;
								var arrCombo = json.arrCombo;
								
								if(arrCombo[0]=='none'){
									alert("Items are not exist");
									return;
									//hideWaiting();
								}
								
								if(!orderNo){
										orderNo = '';
								}
								if(!salesOrderId){
									salesOrderId = '';
								}
									
								if(!salesOrderNo){
										salesOrderNo = '';
								}
									
 
								var salesOrderId_sel=salesOrderId_list;
								var salesOrderNo_sel=salesOrderNo_list;
								for(var i=0;i<length;i++)
								{
									if(itemType==3){
									//var salesOrderId=arrCombo[i]['intSalesOrderId'];
									//var salesOrderNo=arrCombo[i]['strSalesOrderNo'];
									}
									var itemId=arrCombo[i]['itemId'];	
									var maincatId=arrCombo[i]['maincatId'];	
									var subCatId=arrCombo[i]['subCatId'];	
									var code=arrCombo[i]['code'];
                                                                        var supItemCode=arrCombo[i]['supItemCode'];
                                                                        var itemName=arrCombo[i]['itemName'];
                                                                        var itemHide=arrCombo[i]['itemHide'];
                                                                        var uom=arrCombo[i]['uom'];
									var stockBal=arrCombo[i]['stockBal'];	
									var mainCatName=arrCombo[i]['mainCatName'];
									var subCatName=arrCombo[i]['subCatName'];
									var salesOrderPRIQty_list=arrCombo[i]['salesOrderPRIQty_list'];	
									var salesOrderBalQty_list=arrCombo[i]['salesOrderBalQty_list'];	
									
									var actualPriQty=arrCombo[i]['actualPriQty'];
									var actualPriQty_1=arrCombo[i]['actualPriQty_1'];
									if(!actualPriQty)
										actualPriQty = 0;
									if(!actualPriQty_1)
										actualPriQty_1 = 0;
										
									
									if(itemType==6 ){
										//alert('ok');
										salesOrderId = salesOrderId_list;
										salesOrderNo = salesOrderNo_list;
									}
																																			
									var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF" id=""><input type="checkbox" id="chkDisp" /></td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'" class="mainCatNameP">'+mainCatName+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'" class="subCatNameP">'+subCatName+'</td>';
									if(supItemCode!= null)
                                                                        {
                                                                        content +='<td align="center" bgcolor="#FFFFFF" id="'+code+'" class="codeP">'+supItemCode+'</td>';   
                                                                        }else{
                                                                        content +='<td align="center" bgcolor="#FFFFFF" id="'+code+'" class="codeP">'+code+'</td>';
                                                                            }
                                                                        if(itemHide==0)
                                                                        {
									content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="itemP">'+itemName+'</td>';
                                                                        }else{
                                                                         content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="itemP">*****</td>';   
                                                                        }
                                                                        content +='<td align="center" bgcolor="#FFFFFF" id="'+uom+'" class="uomP">'+uom+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+orderNo+'" style="display:none"  class="orderNoP">'+orderNo+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+salesOrderId_list+'" style="display:none"  class="salesOrderNoP">'+salesOrderNo_list+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+salesOrderBalQty_list+'" style="display:none"  class="productionQtyP">'+salesOrderPRIQty_list+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="'+actualPriQty+'" class="actualPriQtyP">'+actualPriQty+'</td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="" class="Qt">'+'<input id="'+actualPriQty+'" class="validate[required,custom[number]] calculateValue QtyP" style="width:80px;text-align:center" value="'+actualPriQty+'" disabled="disabled" type="text"></td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="" class="">'+'<input id="ExtBalP" class="validate[required,custom[number]] calculateValue " style="width:80px;text-align:center" value="0" disabled="disabled" type="text"></td>';
									content +='<td align="center" bgcolor="#FFFFFF" id="" class="">'+'<input onkeyup="checkQty(this)" id="form-validation-field-0" class="validate[required,custom[number]] calculateValue totQtyP" style="width:80px;text-align:center" value="'+actualPriQty+'" type="text"></td>';
									
									content +='<td align="center" bgcolor="#FFFFFF" id="'+stockBal+'" class="stockBalP">'+stockBal+'</td></tr>';

									add_new_row('#frmMrnPopup #tblItemsPopup',content);
									
								}
									checkAlreadySelected();

							}
						});hideWaiting();
						
				  });
				  //-------------------------------------------------------
			
 			});	
}//-------------------------------------------------------
function addClickedRows()
{
	var rowCount = document.getElementById('tblItemsPopup').rows.length;
	var totRetAmmount=0;
	currentItemType = $('#cboItemType').val();
	var itemType = $('#cboItemType').val();
	//$('#frmMrn #txtItemType').val(itemType);
	var i=0;
	$('#tblItemsPopup .itemP').each(function(){
			i++;
 			var itemId=$(this).attr('id');
			var orderNo=$(this).parent().find(".orderNoP").html();
			//alert(orderNo+'/1');
			var itemName=$(this).html();
			if((document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].checked==true) && (document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].disabled==false)){
				checkAndDeleteExisting(orderNo,itemId,itemName);
			}
	});


	var i=0;
	$('#tblItemsPopup .itemP').each(function(){
		i++;
		
		
		if((document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].checked==true) && (document.getElementById('tblItemsPopup').rows[i].cells[0].childNodes[0].disabled==false)){
			
 			var itemId=$(this).attr('id');
			var maincatId=$(this).parent().find(".mainCatNameP").attr('id');
			var subCatId=$(this).parent().find(".subCatNameP").attr('id');
			var code=$(this).parent().find(".code").html();
			var uom=$(this).parent().find(".uomP").html();
			var itemName=$(this).html();
			var mainCatName=$(this).parent().find(".mainCatNameP").html();
			var subCatName=$(this).parent().find(".subCatNameP").html();
			var stockBal=$(this).parent().find(".stockBalP").html();
			var orderNo=$(this).parent().find(".orderNoP").html();
			var salesOrderNo=$(this).parent().find(".salesOrderNoP").html();
			var salesOrderId=$(this).parent().find(".salesOrderNoP").attr('id');
			var PRI_initial_list=$(this).parent().find(".productionQtyP").html();//initial pri qtys
			var mrn_bal_rounded=$(this).parent().find(".actualPriQtyP").html(); 
			var actualBalQty=$(this).parent().find(".productionQtyP").attr('id');//bal to mrn
			var qty=$(this).parent().find(".QtyP").val();// 
			var extra=$(this).parent().find("#ExtBalP").val();//
            var graphicNo = $('#cboGraphicNo').val();
			
 			
			var arr_so = salesOrderId.split(",");
			var arr_so_no = salesOrderNo.split(",");
			var arr_pri = PRI_initial_list.split(",");
			var arr_bal = actualBalQty.split(",");
	
			var index =0;
			var sum   =0;
			var sum_act_bal	=0;
			for (index = 0; index < arr_pri.length; ++index) {
  				sum+=parseFloat(arr_pri[index]);
  				sum_act_bal+=parseFloat(arr_bal[index]);
			}
   			var index =0;
			var actualQty_prapo		= 0;
			var actualExQty_prapo	= 0;
			var actualQtyBal		= 0;
			var pri					= 0;
			for (index = 0; index < arr_pri.length; ++index) {
				//alert(arr_pri[index]+'/'+sum+'/'+extra)
				if(itemType==3){
					if(sum==0){
						actualQty_prapo		= (parseFloat(qty)/arr_pri.length).toFixed(4);
						actualExQty_prapo	= (parseFloat(extra)/arr_pri.length).toFixed(4);
					}
					else{
						//alert(extra+'/'+sum+'*'+arr_pri[index]);
						//actualQty_prapo		= (parseFloat(qty)/sum*parseFloat(arr_pri[index])).toFixed(4);
						//actualExQty_prapo	= (parseFloat(extra)/sum*parseFloat(arr_pri[index])).toFixed(4);
						
						actualQty_prapo		= (parseFloat(qty)/sum_act_bal*parseFloat(arr_bal[index])).toFixed(4);
						actualExQty_prapo	= (parseFloat(extra)/sum*parseFloat(arr_pri[index])).toFixed(4);
						//alert(actualExQty_prapo);
						if(isNaN(actualQty_prapo))
							actualQty_prapo=0;
						if(isNaN(actualExQty_prapo))
							actualExQty_prapo=0;
							
						if(actualQty_prapo<0)
							actualQty_prapo=0;
						if(actualExQty_prapo<0)
							actualExQty_prapo=0;
					}
					//alert(actualExQty_prapo);
				
					actualQtyBal	= (parseFloat(arr_bal[index])).toFixed(4);
					if(actualQtyBal<0)
					 actualQtyBal=0;
					pri				= arr_pri[index];
					//alert(arr_pri[index]+'/'+sum+'*'+qty+'*'+extra+'_'+actualQtyBal+'___'+pri);
					//alert(actualQty_prapo+'/'+actualExQty_prapo+'/'+actualQtyBal);
					var tot_actualQty_prapo	= ((parseFloat(actualQty_prapo)+parseFloat(actualExQty_prapo))).toFixed(4);
					actualExQty_prapo = (tot_actualQty_prapo-actualQtyBal).toFixed(4);;
 					
					if(actualExQty_prapo<0)
					actualExQty_prapo=0;
					actualQty_prapo = (tot_actualQty_prapo- actualExQty_prapo).toFixed(4);
					
					if(actualQty_prapo<0)
						actualQty_prapo=0;
					if(actualExQty_prapo<0)
						actualExQty_prapo=0;
			
 				}
				else if(itemType==6){
						actualQty_prapo		= (parseFloat(qty)/arr_pri.length).toFixed(4);
						actualExQty_prapo	= 0;
				}
				else{
 						actualQtyBal		= 0;
						actualQty_prapo		= qty;
					 
				}
				if(actualQtyBal <0)
					actualQtyBal=0;
					
				if(qty==0){
					//actualQty_prapo=0;
					//actualExQty_prapo=0;
				}
				
			
			var content='<tr class="normalfnt"><td align="center" bgcolor="#FFFFFF"><img class="delImg" src="images/del.png" width="15" height="15" /><span id="popupItemType" style="visibility: hidden">'+itemType+'</span></td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+orderNo+'" class="orderNo">'+orderNo+'</td>';
            content +='<td align="center" bgcolor="#FFFFFF" id="'+graphicNo+'" class="graphicNo">'+graphicNo+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+arr_so[index]+'" class="salesOrderNo">'+arr_so_no[index]+'</td>';
			//content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'">'+itemName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+maincatId+'">'+mainCatName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+subCatId+'">'+subCatName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'" class="item">'+itemName+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+uom+'" class="uom">'+uom+'</td>';
			content +='<td align="center" bgcolor="#FFFFFF" id="'+(parseFloat(actualQtyBal)).toFixed(4)+'" class="actualPriQty">'+(parseFloat(actualQtyBal)).toFixed(4)+'</td>';
			if(itemType == 3){
				content +='<td align="center" bgcolor="#FFFFFF" id=""><input id="'+arr_pri[index]+'" class="validate[required,custom[number],max['+arr_pri[index]+']] calculateValue Qty" style="width:80px;text-align:center" type="text" value="'+actualQty_prapo+'" onkeyup="checkQty(this)" disabled /></td>';
				content +='<td align="center" bgcolor="#FFFFFF" id=""><input  id="ExtBal" class="validate[required,custom[number]] calculateValue ExQty" style="width:80px;text-align:center" type="text" value="'+actualExQty_prapo+'" disabled/></td>';
				content +='<td align="center" bgcolor="#FFFFFF" id=""><input onkeyup="checkQtyParent(this)" id="" class="validate[required,custom[number]] calculateValue totQty" style="width:80px;text-align:center" type="text" value="'+tot_actualQty_prapo+'"/></td>';
			}
			else{
				content +='<td align="center" bgcolor="#FFFFFF" id=""><input id="'+actualQty_prapo+'" class="validate[required,custom[number]] calculateValue Qty" style="width:80px;text-align:center" type="text" value="'+actualQty_prapo+'" disabled/></td>';
				content +='<td align="center" bgcolor="#FFFFFF" id=""><input  id="ExtBal" class="validate[required,custom[number]] calculateValue " style="width:80px;text-align:center" type="text" value="'+0+'" disabled/></td>';
				content +='<td align="center" bgcolor="#FFFFFF" id=""><input onkeyup="checkQtyParent(this)" id="" class="validate[required,custom[number]] calculateValue totQty" style="width:80px;text-align:center" type="text" value="'+actualQty_prapo+'" /></td>';
			}
			  content +='</tr>';
			if((pri >0 && itemType==3) || (actualQty_prapo >0 || actualExQty_prapo >0 || itemType!=3))//exists in pri
 			add_new_row('#frmMrn #tblMrn',content);
			}
			 // alert(content);
		
		//----------------------------	
		$('.delImg').click(function(){
			$(this).parent().parent().remove();
		});
		
		}
	});
	disablePopup();
	closePopUp();
}
//-------------------------------------
function alertx()
{
	$('#frmMrn #butSave').validationEngine('hide')	;
	document.location.href=document.location.href;
	//window.opener.location.reload();//reload listing page
	//window.location.href = "mrn.php?mrnNo="+serialNo+"&year="+year;
	window.opener.opener.location.reload();//reload listing page
}
function alertxC()
{
	$('#frmMrn #butClear').validationEngine('hide')	;
	document.location.href='?q=229';
}
function alertDelete()
{
	$('#frmMrn #butDelete').validationEngine('hide')	;
}

function closePopUp(){
}
//----------------------------------------------
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
//----------------------------------------------- 
function clearRows()
{
	//var rowCount = $('#tblDispatchPopup >tbody >tr').length;
	var rowCount = document.getElementById('tblMrn').rows.length;
	//alert(rowCount);
	//var cellCount = document.getElementById('tblMain').rows[1].cells.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblMrn').deleteRow(1);
			
	}
}
//----------------------------------------------- 
function clearPopupRows()
{
	var rowCount = document.getElementById('tblItemsPopup').rows.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblItemsPopup').deleteRow(1);
			
	}
}
//------------------------------------------------------------
function checkAlreadySelected(){
	$('#tblMrn .item').each(function(){
			var itemId	= $(this).attr('id');
			var orderNo = 	$(this).parent().find(".orderNo").html();
			var salesOrderId = 	$(this).parent().find(".salesOrderNo").attr('id');
			
			var j=0;
			$('#tblItemsPopup .itemP').each(function(){
				j++;
				var itemIdP = 	$(this).attr('id');
				var orderNoP=$(this).parent().find(".orderNoP").html();
				var salesOrderNoP=$(this).parent().find(".salesOrderNoP").attr('id');
				
				if((orderNo==orderNoP) && (salesOrderId==salesOrderNoP) && (itemId==itemIdP)){
					document.getElementById('tblItemsPopup').rows[j].cells[0].childNodes[0].checked=true;
					document.getElementById('tblItemsPopup').rows[j].cells[0].childNodes[0].disabled=true;
 
 					$(this).parent().find(".totQtyP").prop("disabled", true);;
				}
			});
	});
	
	
	var rowCount = document.getElementById('tblMrn').rows.length;
	var rowCount1 = document.getElementById('tblItemsPopup').rows.length;
	
}
function checkQty(e) {
	//alert(e.value);
    var EnterBalance = parseFloat(e.value);//input by user
   // var balanceQty   = $(e).closest("tr").find(".actualPriQty").attr('id');
    var balanceQty   = parseFloat($(e).closest("tr").find(".actualPriQtyP").attr('id'));

	//alert(EnterBalance+'-'+balanceQty);
    if(EnterBalance >= balanceQty){
		//alert(EnterBalance+'-'+balanceQty);
 		if($('#cboItemType').val()==3)
        	$(e).closest("tr").find("#ExtBalP").val((EnterBalance-balanceQty).toFixed(4));
        	$(e).closest("tr").find(".QtyP").val(parseFloat(balanceQty).toFixed(4));
 		if($('#cboItemType').val()!=3)
        	$(e).closest("tr").find(".QtyP").val(parseFloat(EnterBalance).toFixed(4));
    }
    else if(EnterBalance < balanceQty){
		//alert(2);
        $(e).closest("tr").find("#ExtBalP").prop('disabled', true);
        $(e).closest("tr").find("#ExtBalP").val("0");
		//alert($(e).closest("tr").find("#popupItemType").html());
 		if($('#cboItemType').val()==3)
 			$(e).closest("tr").find(".QtyP").val(EnterBalance);		
    }
 }
//---------------------------------------------------
function checkQtyParent(e) {
	//alert(e.value);
    var EnterBalance = parseFloat(e.value);//input by user
   // var balanceQty   = $(e).closest("tr").find(".actualPriQty").attr('id');
    var balanceQty   = parseFloat($(e).closest("tr").find(".actualPriQty").attr('id'));

	//alert(EnterBalance+'-'+balanceQty);
	//alert($(e).closest("tr").find("#popupItemType").html());
    if(EnterBalance >= balanceQty){
		//alert(EnterBalance+'-'+balanceQty);
 		if($(e).closest("tr").find("#popupItemType").html()==3)
        	$(e).closest("tr").find("#ExtBal").val((EnterBalance-balanceQty).toFixed(4));
        	$(e).closest("tr").find(".Qty").val(parseFloat(balanceQty).toFixed(4));
 		if($(e).closest("tr").find("#popupItemType").html()!=3)
        	$(e).closest("tr").find(".Qty").val(parseFloat(EnterBalance).toFixed(4));
    }
    else if(EnterBalance < balanceQty){
		//alert(2);
        $(e).closest("tr").find("#ExtBal").prop('disabled', true);
        $(e).closest("tr").find("#ExtBal").val("0");
		//alert($(e).closest("tr").find("#popupItemType").html());
 		if($(e).closest("tr").find("#popupItemType").html()==3)
 			$(e).closest("tr").find(".Qty").val(EnterBalance);		
    }
 }

function checkAndDeleteExisting(orderNoP,itemIdP,name){
		
		var orderNo ='';
		var salesOrderId ='';
			var r=0;
			$('#tblMrn .item').each(function(){
	
				orderNo = 	$(this).parent().find(".orderNo").html();
				salesOrderId = 	$(this).parent().find(".salesOrderNo").attr('id');
				var itemId	= $(this).attr('id');
				
				if(orderNoP==orderNo /*&& so==salesOrderId */&& itemIdP==itemId){
					r++;
					$(this).parent().remove();
				}
			});
	if(r>0)
		alert("Removed parent form item :"+orderNo+'/'+name);

	
}
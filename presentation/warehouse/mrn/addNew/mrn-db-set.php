<?php 
ini_set('display_errors',0);
//////////////////////////////////////////////
//Create By:H.B.G Korala
/////////////////////////////////////////////
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$companyId 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	$location 	= $_SESSION['CompanyID'];
	include "{$backwardseperator}dataAccess/Connector.php";
	require_once 	"../../../../class/warehouse/mrn/cls_mrn_get.php";
 	require_once 	"../../../../class/cls_commonFunctions_get.php";
	require_once 	"../../../../class/finance/budget/cls_common_function_budget_get.php";
	require_once "../../../../class/finance/budget/entering/cls_budget_set.php";
	
	$extra_percentage = getExtraPercentage($company);

	$obj_mrn_get			= new cls_mrn_get($db);
	$obj_common				= new cls_commonFunctions_get($db);
	$obj_comm_budget_get	= new cls_common_function_budget_get($db);
	$obj_budget_set			= new cls_budget_set($db);
	
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	
	$arrHeader 	 = json_decode($_REQUEST['arrHeader'],true);
	$serialNo 	 = $arrHeader['serialNo'];
	$year 		 = $arrHeader['Year'];
	$department	 = $arrHeader['department'];
	$date 		 = $arrHeader['date'];
	$type 		 = $arrHeader['type'];
	$type_c		 = $type;
	$remarks 	 = replace1($arrHeader['remarks']);
	
	$arr 		 = json_decode($_REQUEST['arr'], true);
    $isExtra = false;
	$flag = 0;
	
	foreach($arr as $arrVal)
	{
		$extraQty      = $arrVal['extraQty'];
		$popupItemType = $arrVal['popupItemType'];
		if($extraQty >= 0.01  || $popupItemType == 6 ){
			$isExtra = true;
		}
		if($type_c == 6 && $arrVal['popupItemType']==3){
			$type = 3;
		}
		if($arrVal['popupItemType'] == 3){
			$flag = 1;
		}
	}
	
	if($type_c == 3 && $flag==0)
	$type = 6;

	$ApproveLevels_ini		= (int)getApproveLevel('Material Request Note');
	$mrnApproveLevel_ini 	= $ApproveLevels_ini+1;
	$ApproveLevels 			= $ApproveLevels_ini;
	$mrnApproveLevel 		= $mrnApproveLevel_ini;
	
	if(!$isExtra){
		$ApproveLevels = $ApproveLevels_ini-2;
		$mrnApproveLevel = $mrnApproveLevel_ini - 2;
	}
	

	$programName='Material Request Note';
	$programCode='P0229';
	
//------------SAVE---------------------------	
	if($requestType=='save')
	{
		$db->OpenConnection();
		$db->RunQuery('Begin');
		$savableFlag= 1;
		
		if($serialNo==''){
			$serialNo 	= getNextMrnNo();
			$year = date('Y');
			$editMode=0;
			$savedStatus=$mrnApproveLevel;
			$savedLevels=$ApproveLevels;
		}
		else{
			$savableFlag=getSaveStatus($serialNo,$year);
			$editMode=1;
			$sql = "SELECT
			ware_mrnheader.intStatus, 
			ware_mrnheader.intApproveLevels 
			FROM ware_mrnheader  
			WHERE
			ware_mrnheader.intMrnNo =  '$serialNo' AND
			ware_mrnheader.intMrnYear =  '$year'";

			$result = $db->RunQuery($sql);
			$row=mysqli_fetch_array($result);
			$savedStatus=$row['intStatus'];
			$savedLevels=$row['intApproveLevels'];
		}

		//--------------------------
		$editPermission=loadEditMode($programCode,$savedStatus,$savedLevels,$userId);
		$locationEditFlag=getEditLocationValidation('MRN',$companyId,$serialNo,$year);
		//--------------------------

        if($company == '1' && ($type == '7' || $type == '8') && $department != '57'){
        	$rollBackFlag=1;
			$rollBackMsg="This type of MRN cannot be raised from this location";
		}

		//-----------delete and insert to header table-----------------------
		 if(( $locationEditFlag==1) && ($editMode==1)){
			 $rollBackFlag=1;
			 $rollBackMsg="Invalid Edit Location";
		 }
		 else if($savableFlag==0){
			 $rollBackFlag=1;
			 $rollBackMsg="This MRN No is already confirmed.cant edit";
		 }
		 else if($editPermission==0){
			 $rollBackFlag=1;
			 $rollBackMsg="No Permission to save.";
		 }
//		 else if($company == '1' && ($type == '7' || $type = '8') && $department != '57'){
//             $rollBackFlag=1;
//             $rollBackMsg="This type of MRN cannot be raised from this location";
//		 }
		else if($editMode==1){
			$sql = "UPDATE `ware_mrnheader` SET intStatus ='$mrnApproveLevel', 
														   intDepartment ='$department', 
															strRemarks ='$remarks',
														   intApproveLevels ='$ApproveLevels', 
														   datdate ='$date', 
	    												  intModifiedBy ='$userId', 
														  mrnType ='$type', 
														  dtmModifiedDate =now() 
					WHERE (`intMrnNo`='$serialNo') AND (`intMrnYear`='$year')";
			$result = $db->RunQuery($sql);
			
			//inactive previous recordrs in approvedby table
			$maxAppByStatus=(int)getMaxAppByStatus($serialNo,$year)+1;
			$sql = "UPDATE `ware_mrnheader_approvedby` SET intStatus ='$maxAppByStatus' 
					WHERE (`intMrnNo`='$serialNo') AND (`intYear`='$year') AND (`intStatus`='0')";
			$result1 = $db->RunQuery($sql);
		}
		else{
			//$sql = "DELETE FROM `ware_mrnheader` WHERE (`intMrnNo`='$serialNo') AND (`intMrnYear`='$year')";
			//$result1 = $db->RunQuery($sql);

			$sql = "INSERT INTO `ware_mrnheader` (`intMrnNo`,`intMrnYear`,intDepartment,intStatus,intApproveLevels,strRemarks,datdate,dtmCreateDate,intUser,intCompanyId,mrnType) 
					VALUES ('$serialNo','$year','$department','$mrnApproveLevel','$ApproveLevels','$remarks','$date',now(),'$userId','$companyId','$type')";
			$result = $db->RunQuery($sql);
		}
		if(!$result){
			$rollBackFlag			= 1;
			$rollBackMsg 			= $db->errormsg;
			$sqlE		 			= $sql;
		}
		//-----------delete and insert to detail table-----------------------
		if(($result) && ($rollBackFlag!=1)){
		$sql = "DELETE FROM `ware_mrndetails` WHERE (`intMrnNo`='$serialNo') AND (`intMrnYear`='$year')";
		$result2 = $db->RunQuery($sql);
			$toSave			=0;
			$saved			=0;
			$rollBackFlag	=0;
			$c				=0;
			$isExtra		=false;
			//$rollBackMsg ="Maximum MRN Qtys for items are..."; 
			foreach($arr as $arrVal)
			{
				$orderNo 	  	= $arrVal['orderNo'];
				$orderNoArray 	= explode('/',$orderNo);
				$orderNoArray[0];
				$salesOrderId 	= $arrVal['salesOrderId'];
				$itemId 	  	= $arrVal['itemId'];
				$order_status	= getOrderStatus($orderNoArray[0],$orderNoArray[1]);
				$order_type		= get_order_type($orderNoArray[0],$orderNoArray[1]);
				$instant		= get_instant_type($orderNoArray[0],$orderNoArray[1]);
				$instantPre		= get_instant_pre_type($orderNoArray[0],$orderNoArray[1]);

                $heatSealAddition = 0;
                $heatSealCount = getHeatSealStatus($orderNoArray[0], $orderNoArray[1],$salesOrderId);
                if ($heatSealCount > 0) {
                    $heatSealAddition = 2;
                }
				//print_r($order_status);
				$itemName	  	= getItemName($itemId);
                $priQty       	= round(getPRIQty($orderNoArray[0],$orderNoArray[1],$salesOrderId,$itemId),4);
//                var_dump("pri qty 1 : ".$priQty);
				$priQty		   = round(($priQty)*(100+$extra_percentage + $heatSealAddition)/100,4);
//                var_dump("pri qty 2 : ".$priQty);
                $mrnQty       	= round(getMRNQty($orderNoArray[0],$orderNoArray[1],$salesOrderId,$itemId),4);
                $actualPriQty 	= $priQty - $mrnQty;
//                var_dump("mrn qty : ".$mrnQty);
				if($actualPriQty < 0)
					$actualPriQty = 0;
				$Qty 		  = $arrVal['Qty'];
				$extraQty     = $arrVal['extraQty'];
				$Qty1		  = round($Qty - $extraQty,8);
				if($Qty1<0)
				$Qty1	= 0;
				
				$ItemType    = $arrVal['popupItemType'];
				//2017-07-07 allow pre costing mrn
				//$extraQty_to_be     =round($Qty-$actualPriQty,8);
					//if($order_type==0){//other
					if($order_type==0 || ($instant!=1 && $order_type==1)|| ($instantPre!=1 && $order_type==2)){//other or pre costing non instant or split from non instant dummy
						$actualPriQty_s	=$priQty-$mrnQty; 
					}
					else if($order_type==1){//pre costing & instant
						$priQty_pre =$priQty;
						$mrnQty_pre =$mrnQty;
						$priQty_act =0;
						$mrnQty_act =0;
						$priQty_tot =0;
						$mrnQty_tot =0;
						
					    $result_act = get_actual_details($orderNoArray[0],$orderNoArray[1],$salesOrderId);
						while($row_act=mysqli_fetch_array($result_act)){
							$priQty_A	= round(getPRIQty($row_act['ORDER_NO'],$row_act['ORDER_YEAR'],$row_act['SO'],$itemId),4);
							$priQty_A		  		= round(($priQty_A)*(100+$extra_percentage + $heatSealAddition)/100,4);
							$mrnQty_A				= round(getMRNQty($row_act['ORDER_NO'],$row_act['ORDER_YEAR'],$row_act['SO'],$itemId),4);
							$priQty_act +=$priQty_A;
							$mrnQty_act +=$mrnQty_A;
						}
						$priQty_tot 	=$priQty_act+$priQty_pre;
						$mrnQty_tot 	=$mrnQty_act+$mrnQty_pre;
						$actualPriQty_s	=($priQty_tot-$mrnQty_tot);
						$x='MIN(('.$priQty.'),('.$priQty_tot.'-'.$mrnQty_tot.'))';
					}  
					else if($order_type==2){//actual and instant
						
						$actualPriQty_s	=0;
						
					}  
				if($actualPriQty_s<0)
					$actualPriQty_s=0;
				$extraQty_to_be     =round($Qty-$actualPriQty_s,8);
				
				//2017-07-07 end - allow pre costing mrn
 				$popupItemType = $arrVal['popupItemType'];
				if((round($Qty,4)-round($actualPriQty_s,4)) >= 0.01 || $popupItemType == 6)
				$isExtra=true;
				
				if(($order_status[0] <= 0 || ($order_status[0]>=$order_status[1]+1)) && $orderNoArray[0] >0){
					$rollBackFlag = 1;
					$rollBackMsg = "Approval error! Unapproved Order.";
					break;
				}
				else if($order_status[2] == 1 && $order_status[3] !=1 ){//2017-07-06 (removed)
					$rollBackFlag = 1;
					$rollBackMsg = "Can't raise MRN for Non-Instant pre costing orders.";
					break;
				} 
				else if((getRoundedWeight($actualPriQty_s,$itemId) < $Qty1 ) && $ItemType == 3 && round($extraQty,3) < round(($extraQty_to_be),3) && $rollBackFlag!=1){
					$rollBackFlag = 1;
					$rollBackMsg=$x."-Ordered Quantity is Greater than the Balance MRN Qty for the item :".$itemName. '(('.getRoundedWeight($actualPriQty_s,$itemId).' < '.$Qty1.' ) && '.$ItemType .'== 3 && '.$extraQty.' != ('.$extraQty_to_be.') )'.$extra_percentage.'this is heat seal'.$heatSealAddition;
					break;
				}
				else if(($companyId==2 || $companyId==101) && $orderNo>0)
                 {
					$rollBackFlag = 1;
					$rollBackMsg = "orderwise mrn can not be raised for this location";
					break; 
				 }

                $totalQty    = $Qty ;

				//-----------Validate MRN Qty-----------------
						//no need to validate
				//--------------------------------------------
				if($rollBackFlag!=1){
					if($orderNoArray[0]=='')
					$orderNoArray[0]=0;
					
					if($orderNoArray[1]=='')
					$orderNoArray[1]=0;
					if($orderNoArray[0]==0)
					$salesOrderId='';
					
					$sql3 = "INSERT INTO `ware_mrndetails` (`intMrnNo`,`intMrnYear`,`intOrderNo`,intOrderYear,`strStyleNo`,`intItemId`,`dblQty`,dblIssudQty,`EXTRA_QTY`,`MRN_TYPE`,`INITIAL_QTY` ,`INITIAL_EXTRA_QTY`) 
					VALUES ('$serialNo','$year','$orderNoArray[0]','$orderNoArray[1]','$salesOrderId','$itemId','$totalQty','0','$extraQty','$ItemType','$totalQty','$extraQty')";
					$result3 = $db->RunQuery($sql3);
					if($result3==1){
					$saved++;
					}
				}
				
				if($ItemType==3){
					$ItemType_h	=3;
					$c++;
				}
				else if(($ItemType==6 && $c==0))
					$ItemType_h	=3;
				else 
					$ItemType_h	=$ItemType;

				$toSave++;
			}
			
			$sql_u_h = "UPDATE `ware_mrnheader` SET   mrnType ='$type' 
					WHERE (`intMrnNo`='$serialNo') AND (`intMrnYear`='$year')";
			$result_u_h = $db->RunQuery($sql_u_h);
		}
		
		//-----2014-05-08-----BUDGET VALIDATION-------------------------------------------
		$arr_mrn_item_tot_U		= NULL;
		$arr_mrn_sub_cat_tot_U	= NULL;
		$arr_mrn_item_tot_A		= NULL;
		$arr_mrn_sub_cat_tot_A	= NULL;
		
		if($rollBackFlag != 1){
			$header_array		=$obj_mrn_get->get_header($serialNo,$year,'RunQuery');
			$results_details	=$obj_mrn_get->get_details_results($serialNo,$year,'RunQuery');
			$results_fin_year	=$obj_comm_budget_get->loadFinanceYear_with_selected_date($company,$header_array['DATE'],'RunQuery');
			$row_fin_year		=mysqli_fetch_array($results_fin_year);
			$budg_year			=$row_fin_year['intId'];
			
			while($row_details	=mysqli_fetch_array($results_details)){
				$item_price_mrn		= $obj_comm_budget_get->getItemPrice($location,$row_details['ITEM_ID'],$row_details['QTY'],'RunQuery');
				if($item_price_mrn <=0 || $item_price_mrn =='')
				$item_price_mrn			= $obj_comm_budget_get->get_price_for_mrn_item($location,$row_details['ITEM_ID'],'RunQuery');
				$sub_cat_budget_flag	= $obj_comm_budget_get->check_sub_category_budgeted_or_not($location,$row_details['SUB_CATEGORY_ID'],'RunQuery');
				 if($sub_cat_budget_flag	==0){//item wise budget
					$budget_type			= $obj_comm_budget_get->load_budget_type($location,$row_details['SUB_CATEGORY_ID'],$row_details['ITEM_ID'],'RunQuery');
					if($budget_type == 'U') //unit budget
						$arr_mrn_item_tot_U[$row_details['ITEM_ID']]	+= $row_details['QTY'];
					else if($budget_type == 'A'){	//amount budget
						if($item_price_mrn <= 0 || $item_price_mrn ==''  ){
							$rollBackMsg 	= "There is no price for ".$obj_common->get_item_name($row_details['ITEM_ID'],'RunQuery');
							$rollBackFlag	=1;
						}
						$arr_mrn_item_tot_A[$row_details['ITEM_ID']]	+= ($row_details['QTY'])*$item_price_mrn;
					}
					
				}
				else if($sub_cat_budget_flag	==1){//sub category wise budget
					$budget_type			= $obj_comm_budget_get->load_budget_type($location,$row_details['SUB_CATEGORY_ID'],0,'RunQuery');
					if($budget_type == 'U')//unit budget
						$arr_mrn_sub_cat_tot_U[$row_details['SUB_CATEGORY_ID']]	+= $row_details['QTY']-$row_details['MRN_CLEAR_QTY'];
					else if($budget_type == 'A'){	//amount budget
						if($item_price_mrn <= 0 || $item_price_mrn ==''  ){
							$rollBackMsg 	= "There is no price for ".$obj_common->get_item_name($row_details['ITEM_ID'],'RunQuery');
							$rollBackFlag	=1;
						}
						$arr_mrn_sub_cat_tot_A[$row_details['SUB_CATEGORY_ID']]	+= ($row_details['QTY']-$row_details['MRN_CLEAR_QTY'])*$item_price_mrn;
					}
				}
			}
		}
		//print_r($arr_mrn_item_tot_A);
		while (list($key_i, $value_i) = each($arr_mrn_item_tot_U)) {//item ,unit budget
			
				$budget_bal	= $obj_comm_budget_get->load_budget_balance_for_mrn($location,$header_array['DEPARTMENT_ID'],$budg_year,'',$key_i,date('m'),'RunQuery');
				$budget_bal = $budget_bal['balUnits'];
				if($value_i > $budget_bal){
					$rollBackFlag	=1;
					$rollBackMsg	="There is no budget balance for this month for item '".$obj_common->get_item_name($key_i,'RunQuery')."' ".$value_i." > .Budget balance  :".$budget_bal." Units";
				}
		}
		while (list($key_i, $value_i) = each($arr_mrn_item_tot_A)) {//item, amount budget
				$budget_bal	= $obj_comm_budget_get->load_budget_balance_for_mrn($location,$header_array['DEPARTMENT_ID'],$budg_year,'',$key_i,date('m'),'RunQuery');
				$budget_bal = $budget_bal['balAmount'];
				if($value_i > $budget_bal){
					$rollBackFlag	=1;
					$rollBackMsg	="There is no budget balance for this month for item '".$obj_common->get_item_name($key_i,'RunQuery')."' ".$value_i." > .Budget balance  :".$budget_bal." Amount";
				}
		}
		while (list($key, $value) = each($arr_mrn_sub_cat_tot_U)) {//sub category ,unit budget
				$budget_bal	= $obj_comm_budget_get->load_budget_balance_for_mrn($location,$header_array['DEPARTMENT_ID'],$budg_year,$key,'',date('m'),'RunQuery');
				$budget_bal = $budget_bal['balUnits'];
				if($value > $budget_bal){
					$rollBackFlag	=1;
					$rollBackMsg	="There is no budget balance for this month for sub category '".$obj_common->get_sub_category_name($key,'RunQuery')."' ".$value." > .Budget balance  :".$budget_bal." Units";
				}
		}
		while (list($key, $value) = each($arr_mrn_sub_cat_tot_A)) {//sub category ,amount budget
				$budget_bal	= $obj_comm_budget_get->load_budget_balance_for_mrn($location,$header_array['DEPARTMENT_ID'],$budg_year,$key,'',date('m'),'RunQuery');
				$budget_bal = $budget_bal['balAmount'];
				if($value > $budget_bal){
					$rollBackFlag	=1;
					$rollBackMsg	="There is no budget balance for this month for sub category '".$obj_common->get_sub_category_name($key,'RunQuery')."' ".$value."> .Budget balance  :".$budget_bal." Amount";
				}
		}
		//$rollBackFlag	=1;
		//--END OF -----2014-05-08-----BUDGET VALIDATION----------------------------------------
		
		//echo $rollBackFlag;
		
		if(!$isExtra){
			$ApproveLevels = $ApproveLevels_ini-2;
			$mrnApproveLevel = $mrnApproveLevel_ini - 2;
			$sql_uh2 = "UPDATE `ware_mrnheader` SET intStatus ='$mrnApproveLevel', 
														   intApproveLevels ='$ApproveLevels'
					WHERE (`intMrnNo`='$serialNo') AND (`intMrnYear`='$year')";
			$result_uh2 = $db->RunQuery($sql_uh2);
		}
		
		if($rollBackFlag==1){
			$db->RunQuery('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= '';
		}
		else if(($savableFlag==1) &&(!$result)){
			$db->RunQuery('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= $sqlE;
		}
		else if(($savableFlag==1) &&($toSave==0) &&($saved==0)){
			$response['type'] 		= 'fail';
			$response['msg'] 		= "Details saving error";
			$response['q'] 			= '';
		}
		else if(($result) && ($toSave==$saved)){
			$confirmationMode = getConfirmationMode($programCode,$userId,$ApproveLevels,$mrnApproveLevel);
			$db->RunQuery('Commit');
			$response['type'] 		= 'pass';
			if($editMode==1)
			$response['msg'] 		= 'Updated successfully.';
			else
			$response['msg'] 		= 'Saved successfully.';
			
			$response['serialNo'] 		= $serialNo;
			$response['year'] 		= $year;
			$response['confirmationMode'] = $confirmationMode;
		}
		else{
			$db->RunQuery('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		
		$db->CloseConnection();		
	}
//------------CLEAR MRN---------------------------	
	if($requestType=='clear')
	{
		$db->OpenConnection();
		$db->RunQuery('Begin');
		
		$serialNo 	 		= $_REQUEST['serialNo'];
		$year 		 		= $_REQUEST['Year'];
		$mrnNo 	 			= $_REQUEST['serialNo'];
		$year				= $_REQUEST['Year'];
		$header_array		=$obj_mrn_get->get_header($mrnNo,$year,'RunQuery');
	//	print_r($header_array);
		$mrn_status			= $header_array['STATUS'];
		$location_saved		= $header_array['LOCATION_ID'];
		$results_fin_year	=$obj_comm_budget_get->loadFinanceYear_with_selected_date($company,$header_array['DATE'],'RunQuery');
		$row_fin_year		=mysqli_fetch_array($results_fin_year);
		$budg_year			=$row_fin_year['intId'];
		
		
		$sql = "UPDATE `ware_mrnheader` SET 		  intModifiedBy ='$userId', 
													  dtmModifiedDate =now() 
				WHERE (`intMrnNo`='$serialNo') AND (`intMrnYear`='$year')";
		$result = $db->RunQuery($sql);
		//-----------delete and insert to detail table-----------------------
		if($result){
			$toSave=0;
			$saved=0;
			$rollBackFlag=0;
			$rollBackMsg ="Maximum clear Qty for items are..."; 
			$k=0;
			foreach($arr as $arrVal)
			{
				$clearQty 		 	= round($arrVal['clearQty'],4);
				if($clearQty > 0){
					$orderNo			= $arrVal['orderNo'];
					$orderNoArray		= explode('/',$orderNo);
					$salesOrderId		= $arrVal['salesOrderId'];
					$itemId				= $arrVal['itemId'];
					$mrnQty 			= round($arrVal['mrnQty'],4);
					$clearQty 			= round($arrVal['clearQty'],4);
	
					if($orderNoArray[0]==''){
					$orderNoArray[0]=0;
					}
					if($orderNoArray[1]=='')
					$orderNoArray[1]=0;
					
					//------check maximum PO Qty--------------------
					$sqlc = "SELECT 
					ware_mrndetails.dblQty, 
					ware_mrndetails.dblIssudQty, 
					IFNULL((SELECT 
					Sum(ware_issuedetails.dblReturnQty) 
					FROM `ware_issuedetails`
					WHERE
					ware_issuedetails.intMrnNo = ware_mrndetails.intMrnNo AND
					ware_issuedetails.intMrnYear = ware_mrndetails.intMrnYear AND
					ware_issuedetails.strOrderNo = ware_mrndetails.intOrderNo AND
					ware_issuedetails.intOrderYear = ware_mrndetails.intOrderYear AND
					ware_issuedetails.strStyleNo = ware_mrndetails.strStyleNo  AND  
					ware_issuedetails.intItemId = ware_mrndetails.intItemId 
					),0) as dblReturnedQty, 
					mst_item.strName as itemName,
					mst_item.intBomItem,
					 mst_item.intId as intItemNo, 
					mst_item.intMainCategory,
					mst_item.intSubCategory,
					mst_maincategory.strName as mainCatName,
					mst_subcategory.strName as subCatName,
					ware_mrndetails.intOrderNo,
					ware_mrndetails.intOrderYear,
					trn_orderdetails.intSalesOrderId,  
					trn_orderdetails.strSalesOrderNo, 
					mst_item.strCode,
					ware_mrnheader.intCompanyId as saved_location,
					ware_mrnheader.intStatus as mrn_status 
					FROM
					ware_mrndetails 
					INNER JOIN ware_mrnheader ON ware_mrndetails.intMrnNo = ware_mrnheader.intMrnNo AND ware_mrndetails.intMrnYear = ware_mrnheader.intMrnYear
					left Join trn_orderdetails ON ware_mrndetails.intOrderNo = trn_orderdetails.intOrderNo AND ware_mrndetails.intOrderYear = trn_orderdetails.intOrderYear AND ware_mrndetails.strStyleNo = trn_orderdetails.intSalesOrderId
					Inner Join mst_item ON ware_mrndetails.intItemId = mst_item.intId
					Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
					Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId
					WHERE
					ware_mrndetails.intOrderNo =  '$orderNoArray[0]' AND
					ware_mrndetails.intOrderYear =  '$orderNoArray[1]' AND  
					ware_mrndetails.strStyleNo =  '$salesOrderId' AND
					ware_mrndetails.intItemId =  '$itemId' AND
					ware_mrndetails.intMrnNo =  '$serialNo' AND
					ware_mrndetails.intMrnYear =  '$year' 
					ORDER BY mst_maincategory.strName asc, mst_subcategory.strName asc, mst_item.strName asc ";
					
					$resultsc = $db->RunQuery($sqlc);
					$rowc = mysqli_fetch_array($resultsc);
					$location_saved	=$rowc['saved_location'];
					$maxQty 	 = $rowc['dblQty']-$rowc['dblIssudQty']+$rowc['dblReturnedQty'];
					//if(($mrnQty<$minQty) && ($mrnQty>$maxQty)){
					$clearQty	=round($clearQty,4);
					$maxQty =round($maxQty,4);
					if(($clearQty>$maxQty)){
					//	call roll back--------****************
						$rollBackFlag=1;
						if($rowc['intBomItem']==1){
						 $rollBackMsg .="\n ".$orderNoArray[0]."/".$orderNoArray[1]."-".$salesOrderId."-".$rowc['itemName']." =".$maxQty;
						}
						else
						{
	
						 $rollBackMsg .="\n ".$rowc['itemName']." =".$maxQty;
						}
						//exit();
						
					}
	
					//----------------------------
					if($rollBackFlag!=1){
						
						$sqls = "select 
						dblQty,
						EXTRA_QTY 
						from 
						`ware_mrndetails`    
						WHERE (`intMrnNo`='$serialNo') 
						AND (`intMrnYear`='$year') 
						AND (`intOrderNo`='$orderNoArray[0]') 
						AND (`intOrderYear`='$orderNoArray[1]') 
						AND (`strStyleNo`='$salesOrderId') 
						AND (`intItemId`='$itemId')";
						$results 			= $db->RunQuery($sqls);
						$rows 	 			= mysqli_fetch_array($results);
						$mrn_exist 			= $rows['dblQty'];
						$mrn_extra_exist	= $rows['EXTRA_QTY'];
						if($mrn_extra_exist >= $clearQty)
							$deduct_extra= $clearQty;
						else
							$deduct_extra= $mrn_extra_exist;
						
						//$deduct_extra		= ($mrn_extra_exist-$clearQty);
						$deduct_qty			= $clearQty;
						//if($deduct_extra < 0)
							//$deduct_extra=$mrn_extra_exist;
						
						$sql3 = "UPDATE `ware_mrndetails` SET  
						dblQty = dblQty -'$deduct_qty', 
						EXTRA_QTY =EXTRA_QTY -'$deduct_extra', 
						dblMRNClearQty =dblMRNClearQty+'$clearQty'  
						WHERE (`intMrnNo`='$serialNo') 
						AND (`intMrnYear`='$year') 
						AND (`intOrderNo`='$orderNoArray[0]') 
						AND (`intOrderYear`='$orderNoArray[1]') 
						AND (`strStyleNo`='$salesOrderId') 
						AND (`intItemId`='$itemId')";
						$result3 = $db->RunQuery($sql3);
						
						$sql3_1 = "UPDATE `ware_mrndetails` SET  
						dblQty 		= round(dblQty,8), 
						EXTRA_QTY 	= round(EXTRA_QTY,8) 
						WHERE (`intMrnNo`='$serialNo') 
						AND (`intMrnYear`='$year') 
						AND (`intOrderNo`='$orderNoArray[0]') 
						AND (`intOrderYear`='$orderNoArray[1]') 
						AND (`strStyleNo`='$salesOrderId') 
						AND (`intItemId`='$itemId')";
						$result3_1 = $db->RunQuery($sql3_1);
						
						$saved++;
						
						//if($result3==1 && $mrn_status==1){
							
							$sql3_q = "UPDATE `trn_po_prn_details_sales_order_stock_transactions` SET  
							QTY =QTY-'$deduct_qty'  
							WHERE 
								(`ORDER_NO`='$orderNoArray[0]') 
							AND (`ORDER_YEAR`='$orderNoArray[1]') 
							AND (`SALES_ORDER`='$salesOrderId') 
							AND (`ITEM`='$itemId') 
							AND (`TRANSACTION`='MRN') 
							AND (`LOCATION_ID`=".$location_saved.")";
							$result3 = $db->RunQuery($sql3_q);
							
							$sql3 = "UPDATE `trn_po_prn_details_sales_order_stock_transactions` SET  
							QTY =QTY-'$deduct_extra'  
							WHERE 
								(`ORDER_NO`='$orderNoArray[0]') 
							AND (`ORDER_YEAR`='$orderNoArray[1]') 
							AND (`SALES_ORDER`='$salesOrderId') 
							AND (`ITEM`='$itemId') 
							AND (`TRANSACTION`='MRN_EXTRA') 
							AND (`LOCATION_ID`=".$location_saved.")";
							$result3 = $db->RunQuery($sql3);
						//}
					}
					
				//--------GET BUDGET UPDATION ARRAY-------------------------
					$arr_mrn_item_tot_U		= NULL;
					$arr_mrn_sub_cat_tot_U	= NULL;
					$arr_mrn_item_tot_A		= NULL;
					$arr_mrn_sub_cat_tot_A	= NULL;
					if($rollBackFlag != 1){
						$item_price_mrn		= $obj_comm_budget_get->getItemPrice($location,$itemId,$clearQty,'RunQuery');
						if($item_price_mrn <=0 || $item_price_mrn =='')
							$item_price_mrn			= $obj_comm_budget_get->get_price_for_mrn_item($location,$itemId,'RunQuery');
							$sub_cat_budget_flag	= $obj_comm_budget_get->check_sub_category_budgeted_or_not($location,$rowc['intSubCategory'],'RunQuery');
							 if($sub_cat_budget_flag	==0){
								$budget_type			= $obj_comm_budget_get->load_budget_type($location,$rowc['intSubCategory'],$itemId,'RunQuery');
								if($budget_type == 'U')
									$arr_mrn_item_tot_U[$itemId]	+= $clearQty;
								else if($budget_type == 'A')
									$arr_mrn_item_tot_A[$itemId]	+= ($clearQty)*$item_price_mrn;
								
							}
							else if($sub_cat_budget_flag	==1){
								$budget_type			= $obj_comm_budget_get->load_budget_type($location,$rowc['intSubCategory'],0,'RunQuery');
								if($budget_type == 'U')
									$arr_mrn_sub_cat_tot_U[$rowc['intSubCategory']]	+= $clearQty;
								else if($budget_type == 'A')
									$arr_mrn_sub_cat_tot_A[$rowc['intSubCategory']]	+= ($clearQty)*$item_price_mrn;
							}
						}
				//--------END OF GET BUDGET UPDATION ARRAY----------------------
					
					
					$toSave++;
				}
			}
			
			
		}
		
 		
		//-----2014-05-08-----BUDGET UPDATION-------------------------------------------
		if($header_array['STATUS'] == 1){	
			if($rollBackFlag != 1){
				while (list($key_i, $value_i) = each($arr_mrn_item_tot_U)) {
					$response_b		= $obj_budget_set->updatePlanDetails_units_or_amount($location,$header_array['DEPARTMENT_ID'],$budg_year,'',$key_i,date('m'),0,-$value_i,'MRN','U','RunQuery');
					if($response_b['savedStatus']	 == 'fail'){
						$rollBackFlag		=1;
						$rollBackMsg		.=$response_b['savedMassege'];
						$sqlM				.=$response_b['error_sql'];
					}
				}
			}
			if($rollBackFlag != 1){
				while (list($key_i, $value_i) = each($arr_mrn_item_tot_A)) {
					$response_b		= $obj_budget_set->updatePlanDetails_units_or_amount($location,$header_array['DEPARTMENT_ID'],$budg_year,'',$key_i,date('m'),-$value_i,0,'MRN','A','RunQuery');
					if($response_b['savedStatus']	 == 'fail'){
						$rollBackFlag		=1;
						$rollBackMsg		.=$response_b['savedMassege'];
						$sqlM				.=$response_b['error_sql'];
					}
				}
			}
			if($rollBackFlag != 1){
				while (list($key, $value) = each($arr_mrn_sub_cat_tot_U)) {
					$response_b		= $obj_budget_set->updatePlanDetails_units_or_amount($location,$header_array['DEPARTMENT_ID'],$budg_year,$key,0,date('m'),0,-$value,'MRN','U','RunQuery');
					if($response_b['savedStatus']	 == 'fail'){
						$rollBackFlag		=1;
						$rollBackMsg		.=$response_b['savedMassege'];
						$sqlM				.=$response_b['error_sql'];
					}
				}
			}
			if($rollBackFlag != 1){
				while (list($key, $value) = each($arr_mrn_sub_cat_tot_A)) {
					$response_b		= $obj_budget_set->updatePlanDetails_units_or_amount($location,$header_array['DEPARTMENT_ID'],$budg_year,$key,0,date('m'),-$value,0,'MRN','A','RunQuery');
					if($response_b['savedStatus']	 == 'fail'){
						$rollBackFlag		=1;
						$rollBackMsg		.=$response_b['savedMassege'];
						$sqlM				.=$response_b['error_sql'];
					}
				}
			}
		}
		//$rollBackFlag =1;	
  		//-----END OF 2014-05-08-----BUDGET UPDATION------------------------------------
		//$rollBackFlag=1;
	
		
		if($rollBackFlag==1){
			$db->RunQuery('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= '';
		}
		else if($saved==0){
			$db->RunQuery('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= "There is no any quantity to clear";
			$response['q'] 			= $sql3."dddd".$k;
		}
		else if(($result) && ($toSave==$saved) && $saved > 0){
			$db->RunQuery('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= 'Cleared successfully.';
			
			$response['serialNo'] 		= $serialNo;
			$response['year'] 		= $year;
		}
		else{
			$db->RunQuery('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		
		$db->CloseConnection();		
	}
	echo json_encode($response);
//-----------------------------------------	-------------------

//---------------FUNCTIONS-----------------	-------------------
	function getNextMrnNo()
	{
		global $db;
		global $companyId;
		$sql = "SELECT
				sys_no.intMrnNo
				FROM sys_no
				WHERE
				sys_no.intCompanyId =  '$companyId'
				";	
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$nextMrnNo = $row['intMrnNo'];
		
		$sql = "UPDATE `sys_no` SET intMrnNo=intMrnNo+1 WHERE (`intCompanyId`='$companyId')  ";
		$db->RunQuery($sql);
		
		return $nextMrnNo;
	}
//---------------------------------------------------	
	function getSaveStatus($mrnNo,$year)
	{
		global $db;
		$sql = "SELECT ware_mrnheader.intStatus, ware_mrnheader.intApproveLevels FROM ware_mrnheader WHERE (intMrnNo='$mrnNo') AND (`intMrnYear`='$year')";
			$results = $db->RunQuery($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
			
			if(($status==0) || ($row['intStatus']==($row['intApproveLevels']+1))){
				$editableFlag=1	;
			}
			else{
				$editableFlag=0;	
			}
			
		return $editableFlag;
	}
//-----------------------------------------------------------
	function getEditLocationValidation($type,$location,$serialNo,$year)
	{
		$flag= 1;
		
		global $db;
		  $sql = "SELECT
					ware_mrnheader.intCompanyId
					FROM
					ware_mrnheader
					Inner Join mst_locations ON ware_mrnheader.intCompanyId = mst_locations.intId
					WHERE
					ware_mrnheader.intMrnNo =  '$serialNo' AND
					ware_mrnheader.intMrnYear =  '$year'";
			$results = $db->RunQuery($sql);
			$row = mysqli_fetch_array($results);
			if($location == $row['intCompanyId'])
			$flag=0;
 			
		return $flag;

}
//------------------------------function loadConfirmatonMode-------------------
    function getConfirmationMode($programCode,$intUser,$approveLevels,$status){
		global $db;
		$k=$approveLevels+2-$status;
		$sqlp = "SELECT 
		menupermision.intEdit, 
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		if($rowp['int'.$k.'Approval']==1) 
		 $confirmatonMode=1; 
		else
		 $confirmatonMode=0; 
	 
	return $confirmatonMode;
}
//------------------------------function load loadEditMode---------------------
    function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		$resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
		 
		// $editMode;
		 
	return $editMode;
}
//------------------------------function get MaxAppByStatus---------------------
    function getMaxAppByStatus($serialNo,$year){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	  $sql = "SELECT
				max(ware_mrnheader_approvedby.intStatus) as status 
				FROM
				ware_mrnheader_approvedby
				WHERE
				ware_mrnheader_approvedby.intMrnNo =  '$serialNo' AND
				ware_mrnheader_approvedby.intYear =  '$year'";
				
		$resultm = $db->RunQuery($sql);
		 $rowm=mysqli_fetch_array($resultm);
		 
		 return $rowm['status'];

}
//-------------------------------------------------------- 
    function replace($str)
{
	  $str 	= 	str_replace('\\', "\\\\",	$str);
	  $str 	= 	str_replace("'","\'",	$str);
	  $str	=	str_replace("\n", "<br>",$str);  
	  
	  return $str;
}	

//------------------------------------------------------- 
    function replace1($str)
{
	  $str 	= 	str_replace('\\', "\\\\",	$str);
	  $str 	= 	str_replace("'","\'",	$str);
	  $str	=	str_replace("\n", "<br>",$str);  
	  
	  return $str;
}	
//-------------------------------------------------------------------
function getPRIQty($orderNo,$orderYear,$salesOrderNo,$item)
{
    global $db;
    $sql = "	SELECT
						IFNULL(trn_po_prn_details_sales_order.REQUIRED,'0') AS priQty
					FROM
						trn_po_prn_details_sales_order
					WHERE
						trn_po_prn_details_sales_order.ORDER_NO = '$orderNo'
					AND trn_po_prn_details_sales_order.ORDER_YEAR = '$orderYear'
					AND trn_po_prn_details_sales_order.SALES_ORDER = '$salesOrderNo'
					AND trn_po_prn_details_sales_order.ITEM = '$item'";
//    var_dump("my sql");
//    var_dump($sql);
    $result = $db->RunQuery($sql);
//    var_dump("this is my result");
//    var_dump($result);
    $row = mysqli_fetch_array($result);

    return val($row['priQty']);
}
function getMRNQty($orderNo,$orderYear,$salesOrderNo,$item)
{
    global $db;
    $sql = "SELECT
				IFNULL(sum(ware_mrndetails.dblQty),'0') AS mrnQty
			FROM
				ware_mrndetails
			INNER JOIN ware_mrnheader ON ware_mrndetails.intMrnNo = ware_mrnheader.intMrnNo
			AND ware_mrndetails.intMrnYear = ware_mrnheader.intMrnYear
			WHERE
				ware_mrndetails.intOrderNo = '$orderNo'
			AND ware_mrndetails.intOrderYear = '$orderYear'
			AND ware_mrndetails.strStyleNo = '$salesOrderNo'
			AND ware_mrndetails.intItemId = '$item'
			AND ware_mrnheader.intStatus = '1'";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    return val($row['mrnQty']);
}

function getItemName($itemId){
	
    global $db;
    $sql = "SELECT
			mst_item.strName
			FROM `mst_item`
			WHERE
			mst_item.intId = '$itemId'
			";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    return ($row['strName']);
}

function getOrderStatus($orderNo,$year){
    global $db;
    $sql = "SELECT
			trn_orderheader.intStatus,
			trn_orderheader.intApproveLevelStart ,
			PO_TYPE ,
			INSTANT_ORDER 
			FROM
			trn_orderheader
			WHERE
			trn_orderheader.intOrderNo = '$orderNo' AND
			trn_orderheader.intOrderYear = '$year'

			";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
	$arr[0]	= $row['intStatus'];
	$arr[1]	= $row['intApproveLevelStart'];
	$arr[2]	= $row['PO_TYPE'];
	$arr[3]	= $row['INSTANT_ORDER'];
    return $arr;
	
}

	function getRoundedWeight($qty, $item){
    global $db;

    $sql = "SELECT
			mst_item.strName,
			mst_item.intUOM 
			FROM `mst_item`
			WHERE
			mst_item.intId = '$item'
			";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    $uom	= ($row['intUOM']);
	
	if($uom ==6 && floor($qty)==0){//only for weight < 1kg
	
	if($qty<0.005 && $qty>=0.0001)
		$qty=0.005;
		//$y	=((round($qty,3)*1000)%100);
		$y	= ((floor($qty * 1000) / 1000)*1000)%100;
		$z	=ceil($y/10)*10;
		$a	=$z-$y;
		
		if($a > 5)
			$val1=0;
		else if($a>0)
			$val1=5;
		else
			$val1	=0;
			
			$val2 	=  floor((floor($qty * 1000) / 1000)*100)/100;
			$val	= $val2+$val1/1000;
		
	}
	else
	$val	=$qty;
 	//echo $qty.'/'.$y.'/'.$z.'/'.$a.'/'.$val1.'/'.$val2.'/'.$val.'</br>';
	return round($val,8);
}

function getExtraPercentage($company){
    global $db;
     $sql = "SELECT
			mst_companies.EXTRA_MRN_PERCENTAGE 
			FROM `mst_companies`
			WHERE
			mst_companies.intId = '$company'
			";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
	//print_r($row);
    //return ($row['EXTRA_MRN_PERCENTAGE']);
	if($company==36)
	return 5;
	else
	return 0;
}

function get_order_type($no,$year){
	
	global $db;	
    $sql = "SELECT
			trn_orderheader.PO_TYPE 
			FROM `trn_orderheader`
			WHERE
			trn_orderheader.intOrderNo = '$no' and 
			trn_orderheader.intOrderYear = '$year'
			";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    return ($row['PO_TYPE']);
	
}
function get_actual_details($no,$year,$so){
	
	global $db;	
    $sql = "SELECT
			trn_orderdetails.intOrderNo AS ORDER_NO,
			trn_orderdetails.intOrderYear AS ORDER_YEAR,
			trn_orderdetails.intSalesOrderId AS SO 
			FROM
			trn_orderdetails
			INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
			WHERE
			trn_orderheader.DUMMY_PO_NO = '$no' AND
			trn_orderheader.DUMMY_PO_YEAR = '$year' AND
			trn_orderdetails.DUMMY_SO_ID = '$so'
			";
    $result = $db->RunQuery($sql);
    return $result;
}

function get_pre_details($no,$year,$so){
	global $db;	
    $sql = "SELECT
			trn_orderdetails.intOrderNo AS DUMMY_PO_NO,
			trn_orderdetails.intOrderYear AS DUMMY_PO_YEAR,
			trn_orderdetails.DUMMY_SO_ID AS SO 
			FROM
			trn_orderdetails
			INNER JOIN trn_orderheader ON trn_orderdetails.intOrderNo = trn_orderheader.intOrderNo AND trn_orderdetails.intOrderYear = trn_orderheader.intOrderYear
			WHERE
			trn_orderheader.intOrderNo = '$no' AND
			trn_orderheader.intOrderYear = '$year' AND
			trn_orderdetails.intSalesOrderId = '$so'
			";
    $result = $db->RunQuery($sql);
	return $row = mysqli_fetch_array($result);
}


function get_instant_type($no,$year){
	
	global $db;	
    $sql = "SELECT
			trn_orderheader.INSTANT_ORDER 
			FROM `trn_orderheader`
			WHERE
			trn_orderheader.intOrderNo = '$no' and 
			trn_orderheader.intOrderYear = '$year'
			";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    return ($row['INSTANT_ORDER']);
	
}

function get_instant_pre_type($no,$year){
	
	global $db;	
    $sql = "SELECT
			oh2.INSTANT_ORDER
			FROM
			trn_orderheader AS oh1
			INNER JOIN trn_orderheader AS oh2 ON oh1.DUMMY_PO_NO = oh2.intOrderNo AND oh1.DUMMY_PO_YEAR = oh2.intOrderYear
			WHERE
			oh1.intOrderNo = '$no' AND
			oh1.intOrderYear = '$year'";
    $result = $db->RunQuery($sql);
    $row = mysqli_fetch_array($result);
    return ($row['INSTANT_ORDER']);
	
}

function getHeatSealStatus($no,$year,$so){
    global $db;
    $sql_heatSeal_check = "select mt.intId, mt.strName from trn_orderdetails tod inner join trn_sampleinfomations_details tsd on tod.intSampleNo = tsd.intSampleNo
		and tod.intSampleYear = tsd.intSampleYear and tod.strCombo = tsd.strComboName and tod.strPrintName = tsd.strPrintName and 
		tod.intRevisionNo = tsd.intRevNo inner join mst_techniques mt on mt.intId = tsd.intTechniqueId where mt.heatTransfer =1 and tod.intOrderNo = '$no' and tod.intOrderYear = '$year' and tod.intSalesOrderId = '$so'" ;

    $result = $db->RunQuery($sql_heatSeal_check);
    return mysqli_num_rows($result);
}

?>


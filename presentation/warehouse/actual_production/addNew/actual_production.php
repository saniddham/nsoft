<?php
	//ini_set('display_errors',1);
	session_start();
	$backwardseperator = "../../../../";
	$thisFilePath 	=  $_SERVER['PHP_SELF'];
	$locationId 	= $_SESSION['CompanyID'];
	$intUser  		= $_SESSION["userId"];
	$company 	= $_SESSION['headCompanyId'];

	//include  		"{$backwardseperator}dataAccess/permisionCheck.inc";
	include_once  		"{$backwardseperator}dataAccess/connector.php";
	require_once $_SESSION['ROOT_PATH']."class/warehouse/stores_requesition_note/cls_stores_requesition_note_get.php";
	require_once $_SESSION['ROOT_PATH']."class/cls_commonErrorHandeling_get.php";
	require_once $_SESSION['ROOT_PATH']."class/warehouse/cls_warehouse_get.php";
  
  	$obj_st_req_get	= new cls_stores_requesition_note_get($db);
	$obj_commonErr	= new cls_commonErrorHandeling_get($db);
	$obj_ware_get	= new cls_warehouse_get($db);
	
	$programName	= 'Stores Requisition Note';
	$programCode	= 'P0256';

	$serialNo		=$_REQUEST['SRNNo'];
	$serialYear		=$_REQUEST['SRNYear'];
	$curr_date		= date('Y-m-d');
   	
 	$row 			=	$obj_st_req_get->other_stores_requisition_header_select($serialNo,$serialYear,'RunQuery');
	$status			=	$row['intStatus'];
	$levels			=	$row['intApproveLevels'];
	$year_selected	=	$row['intOrderYear'];
	if($year_selected==''){
		$year_selected = date('Y');
	}
	
 	$reult_d		=	$obj_st_req_get->other_stores_requisition_details_result($serialNo,$serialYear,'RunQuery');
 	
	$order_selected1=	$row['strOrderNos'];
	$order_selected = explode(",", $order_selected1);

	
 	
 	$editPermition		=$obj_commonErr->get_permision_withApproval_save($status,$levels,$intUser,$programCode,'RunQuery');
	$editMode			=$editPermition['permision'];
 	$confirnPermition	=$obj_commonErr->get_permision_withApproval_confirm($status,$levels,$intUser,$programCode,'RunQuery');
	$confirmMode		=$confirnPermition['permision'];
	
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Actual Production</title>
<link href="../../../../css/mainstyle.css" rel="stylesheet" type="text/css" />
<link href="../../../../css/button.css" rel="stylesheet" type="text/css" />
<link href="<?php echo $backwardseperator; ?>css/promt.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/validationEngine.css" type="text/css">
<link rel="stylesheet" href="<?php echo $backwardseperator; ?>libraries/validate/template.css" type="text/css">
<script type="application/javascript" src="../../../../libraries/jquery/jquery.js"></script>
<script type="application/javascript" src="../../../../libraries/jquery/jquery-ui.js"></script>
<script type="application/javascript" src="actual_production-js.js"></script>
<script type="application/javascript" src="../../../../libraries/javascript/script.js"></script>
<!--<script type="application/javascript" src="../../../../libraries/jquery/multi_select_combo.js"></script>
--><link rel="stylesheet" type="text/css" href="../../../../libraries/calendar/theme.css" />
<script src="../../../../libraries/calendar/calendar.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/calendar-en.js" type="text/javascript"></script>
<script src="../../../../libraries/calendar/runCalender.js" type="text/javascript"></script>
</head>

<body>
<table width="100%" border="0" align="center">
  <tr>
    <td height="6" colspan="2" id="td_comDetHeader"><?php include  $backwardseperator.'Header.php'; ?></td>
  </tr>
</table>
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.js"></script> 
<script type="application/javascript" src="../../../../libraries/javascript/jquery-impromptu.min.js"></script>
<link rel="stylesheet" href="../../../../libraries/chosen/chosen/chosen.css" />

<!--<script type="text/javascript" src="../../../../libraries/jquery/jquery-1.8.3-min.js"></script>
--> 
<script src="../../../../libraries/chosen/chosen/prototype.js" type="text/javascript"></script> 
<script src="../../../../libraries/chosen/chosen/chosen.proto.js" type="text/javascript"></script> 
<script src="../../../../libraries/validate/jquery-1.js" type="text/javascript"></script> 
<script src="../../../../libraries/validate/jquery_002.js" type="text/javascript" charset="utf-8"></script> 
<script src="../../../../libraries/validate/jquery.js" type="text/javascript" charset="utf-8"></script> 
<script>
jQuery.noConflict();
</script>
<form id="frmStoresRequisition" name="frmStoresRequisition" autocomplete="off" action="stores_requesition_note.php" method="post">
  <div align="center">
    <div class="trans_layoutXL" style="width:950px"  align="center">
      <div class="trans_text">Actual Production</div>
      <table width="900" border="0" class="">
        <tr>
          <td  class="normalfnt" colspan="3" align="center"><fieldset class="tableBorder_allRound">
            <table width="100%" border="0" class="">
              <tr>
         <td colspan="5"><table width="100%" border="0" cellpadding="0" cellspacing="0">
          <tr>
            <td width="11%" height="22" class="normalfnt">Year</td>
            <td width="24%"><select style="width:60px" name="cboOrderYear" id="cboOrderYear" class="validate[required]">
              <?php echo $options = $obj_st_req_get->get_order_years_options($year_selected,'RunQuery') ?>
              </select></td>
            <td width="7%">&nbsp;</td>
            <td width="39%">&nbsp;</td>
            <td width="5%" class="normalfnt">Date</td>
            <td width="14%"><input name="dtDate" type="text" disabled="disabled" class="txtNumber srnDate" id="dtDate" style="width:100px" value="<?php echo(($dtDate)?$dtDate:date('Y-m-d')); ?>" />&nbsp;</td>
          </tr>
        </table></td>
      </tr>  
              <tr>
                <td width="11%" class="normalfnt">Graphic Nos</td>
                <td colspan="3" ><div id="grap"><select name="select" class="txtCalled normalfnt multiSelect graphics validate[required]" style="width:200px; height:20px" tabindex="4" data-placeholder="" >
                  <?php
					$options = $obj_st_req_get->get_graphics_options($year_selected,$graphic_selected,'RunQuery');
					echo $options;
				?>
                  </select></div></td>
                <td width="15%" >&nbsp;</td>
              </tr>
<tr>
  <td colspan="5" class="normalfnt"><div style="width:900px;height:300px;overflow:scroll" >
    <table width="100%" class="bordered" id="tblMain" >
            <tr>
              <th width="7%" height="22" >Del</th>
              <th width="13%" >Group</th>
              <th width="28%" >Printer</th>
              <th width="11%">Order No</th>
              <th width="17%">Sales Order</th>
              <th width="15%">Part/Combo</th>
              <th width="9%">Pieces</th>
              </tr>
                                  	<tr class="normalfnt">
                        <td align="center" bgcolor="#FFFFFF"><img class="delImg" src="../../../../images/del.png" width="15" height="15" /></td>
                        <td align="center" bgcolor="#FFFFFF" id="<?php echo $row['mainCatId']; ?>" class="mainCatName"><select style="width:100%" name="cboOrderYear2" id="cboOrderYear2" class="validate[required]">
                         </select>                          <br /></td>
                        <td align="center" bgcolor="#FFFFFF" id="<?php echo $row['mainCatId']; ?>" class="mainCatName"><select style="width:100%" name="cboOrderYear3" id="cboOrderYear3" class="validate[required]">
                         </select></td>
                        <td align="center" bgcolor="#FFFFFF" id="<?php echo $row['unitId']; ?>" class="uom"><span class="mainCatName">
                          <select style="width:100%" name="cboOrderYear4" id="cboOrderYear4" class="validate[required]">
                           </select>
                        </span></td>
                        <td align="center" bgcolor="#FFFFFF" class="srnQty"><span class="mainCatName">
                          <select style="width:100%" name="cboOrderYear5" id="cboOrderYear5" class="validate[required]">
                           </select>
                        </span></td>
                        <td align="center" bgcolor="#FFFFFF" id=""><span class="mainCatName">
                          <select style="width:100%" name="cboOrderYear6" id="cboOrderYear6" class="validate[required]">
                           </select>
                        </span></td>
                        <td align="center" bgcolor="#FFFFFF" id=""><input  id="maxTrnsfQty" class="validate[required,custom[number],max[<?php echo $maxTrnsfQty; ?>]] calculateValue Qty" style="width:70px;text-align:center" type="text" value="<?php echo $row['dblQty']; ?>" onKeyUp="chekQty(this);"/></td></tr>
              <tr>
              
              
              <?php
			  if($transfNo!="" && $year!="")
				{
					$sql = " SELECT STD.intTransfNo,STD.intTransfYear,STD.intItemId,MI.strName AS itemName,
								MC.strName AS mainCatName,MC.intId AS mainCatId,SC.strName AS subCatName,SC.intId AS subCatId,STD.dblQty,
								(SELECT SUM(dblQty) FROM ware_storesrequesitiondetails SRD WHERE SRD.intRequisitionNo=STH.intRequisitionNo AND 
								SRD.intRequisitionYear=STH.intRequisitionYear AND SRD.intItemId=STD.intItemId ) AS SRNQty,
								(SELECT SUM(dblExQty) FROM ware_storesrequesitiondetails SRD WHERE SRD.intRequisitionNo=STH.intRequisitionNo AND 
								SRD.intRequisitionYear=STH.intRequisitionYear AND SRD.intItemId=STD.intItemId ) AS excessQty,
								(SELECT SUM(dblTransferQty+dblExTransferQty) FROM ware_storesrequesitiondetails SRD WHERE SRD.intRequisitionNo=STH.intRequisitionNo AND 
								SRD.intRequisitionYear=STH.intRequisitionYear AND SRD.intItemId=STD.intItemId ) AS totTransQty,
								MU.intId AS unitId,MU.strName AS unitName
								FROM ware_storestransferdetails STD
								INNER JOIN ware_storestransferheader STH ON STH.intTransfNo=STD.intTransfNo AND STH.intTransfYear=STD.intTransfYear
								INNER JOIN mst_item MI ON MI.intId=STD.intItemId
								INNER JOIN mst_maincategory MC ON MC.intId=MI.intMainCategory
								INNER JOIN mst_subcategory SC ON SC.intId=MI.intSubCategory
								INNER JOIN mst_units MU ON MU.intId=MI.intUOM
								WHERE STD.intTransfNo='$transfNo' AND
								STD.intTransfYear='$year' ";
					$result = $db->RunQuery($sql);
					while($row=mysqli_fetch_array($result))
					{
						$stockBalQty=getStockBalance_bulk($location,$tranferFrom,$row['intItemId']);
						$balToTransfQty=$row['SRNQty']-$row['totTransQty'];
						$maxTrnsfQty=$balToTransfQty;
						if($stockBalQty<$maxTrnsfQty)
						{
							$maxTrnsfQty=$stockBalQty;
						}
					?>
                    	<tr class="normalfnt">
                        <td align="center" bgcolor="#FFFFFF"><img class="delImg" src="../../../../images/del.png" width="15" height="15" /></td>
                        <td align="center" bgcolor="#FFFFFF" id="<?php echo $row['mainCatId']; ?>" class="mainCatName"><?php echo $row['mainCatName']; ?></td>
                        <td align="center" bgcolor="#FFFFFF" id="<?php echo $row['mainCatId']; ?>" class="mainCatName"><?php echo $row['mainCatName']; ?></td>
                        <td align="center" bgcolor="#FFFFFF" id="<?php echo $row['unitId']; ?>" class="uom"><?php echo $row['unitName']; ?></td>
                        <td align="center" bgcolor="#FFFFFF" class="srnQty"><?php echo $row['SRNQty']; ?></td>
                        <td align="center" bgcolor="#FFFFFF" id="">&nbsp;</td>
                        <td align="center" bgcolor="#FFFFFF" id=""><input  id="maxTrnsfQty" class="validate[required,custom[number],max[<?php echo $maxTrnsfQty; ?>]] calculateValue Qty" style="width:70px;text-align:center" type="text" value="<?php echo $row['dblQty']; ?>" onKeyUp="chekQty(this);"/></td></tr>
                    <?php
					}
				}
			 ?>
            </table>
    </div></td>
</tr>
              
              </table>
          </fieldset></td>
        </tr>
        <tr>
          <td colspan="4" align="center">
                  </td>
        </tr>
        <tr>
          <td height="34" colspan="4"><table width="100%" border="0" cellpadding="0" cellspacing="0" class="bcgl2">
            <tr>
              <td width="100%" align="center" bgcolor=""><a id="butNew"  class="button white medium" style="" name="butAddNewTask"> New </a>
                <?php if($editMode==1){ ?>
                <a id="butSave" class="button white medium" style="" name="butSave"> Save </a>
                <?php } ?>
                 <a id="butConfirm" class="button white medium"  <?php if($confirmMode!=1){ ?>style="display:none"<?php } ?> name="butConfirm"> Confirm </a>
                 <a id="butReport" class="button white medium" <?php if($serialNo==''){ ?>style="display:none"<?php } ?> name="butReport"  > Report </a>
                <a id="butClose" class="button white medium" name="butClose" href="../../../../main.php">Close</a></td>
            </tr>
          </table></td>
        </tr>
        <tr>
          <td colspan="4" class="normalfnt"> 
   
          </tr>
      </table>
    </div>
  </div>
</form>
<div    style="width:900px; position: absolute;display:none;z-index:100"  id="popupContact1"> 
  <!-- <iframe onload="loadMain();"   id="iframeMain1" name="iframeMain1" src="presentation/customerAndOperation/sample/sampleDispatch/addNew/sampleDispatchPopup.php" style="width:800;height:800;border:0;overflow:hidden">
    </iframe>--> 
</div>
<div style="height: 0px; opacity: 0.7; display: none;" id="backgroundPopup"></div>
</body>
</html>

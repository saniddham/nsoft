<?php
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];
	$requestType 	= $_REQUEST['requestType'];
	include "{$backwardseperator}dataAccess/Connector.php";
	
	/////////// type of print load part /////////////////////
	if($requestType=='loadSubCategoriesCombo')
	{
		$mainCat  = $_REQUEST['mainCat'];
		 $sql = "SELECT
				mst_subcategory.intId,
				mst_subcategory.strName
				FROM mst_subcategory
				WHERE
				mst_subcategory.intMainCategory =  '$mainCat' AND
				mst_subcategory.intStatus =  '1'";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	else if($requestType=='loadItemCombo')
	{
		$mainCat  = $_REQUEST['mainCat'];
		$subCat  = $_REQUEST['subCat'];
		 $sql = "SELECT
				mst_item.intId,
				mst_item.strName
				FROM mst_item
				WHERE 
				mst_item.intStatus = '1' AND 
				mst_item.intMainCategory =  '$mainCat' AND
				mst_item.intSubCategory =  '$subCat' AND
				mst_item.dblLastPrice >  '0' AND 
				mst_item.intCurrency >  '0' 
				order by mst_item.strName asc";
		$result = $db->RunQuery($sql);
		$html = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		echo $html;
		echo json_encode($response);
	}
	else if($requestType=='loadItemDetails')
	{
		$itemId  = $_REQUEST['itemId'];
		 $sql = "SELECT
					mst_item.intUOM,
					mst_item.intSubCategory, 
					mst_units.strCode
					FROM
					mst_item
					Inner Join mst_units ON mst_item.intUOM = mst_units.intId
					WHERE 
					mst_item.intId =  '$itemId' 
					order by mst_item.strName asc";
		$result = $db->RunQuery($sql);
		while($row=mysqli_fetch_array($result))
		{
			$response['stockQty'] = getStockBalance_bulk($location,$itemId); 
			$response['allocatedQty'] = getAllocatedQty($location,$itemId);
			$response['uomId'] = $row['intUOM'];
			$response['uom'] = $row['strCode'];
			$response['subCatId'] = $row['intSubCategory'];
			$response['itemCombo'] = getItemCombo($row['intSubCategory']);
		}
		echo json_encode($response);
	}
	else if($requestType=='loadMainCategorySubCategory')
	{
		$itemId  = $_REQUEST['itemId'];
		//-------------------------------------
		$sql = "SELECT
				mst_item.intMainCategory,
				mst_item.intSubCategory
				FROM
				mst_item
				WHERE 
				mst_item.intId =  '$itemId'";

		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		$mainCatId=$row['intMainCategory'];	
		$subCatId=$row['intSubCategory'];	
		
		//----------------------------------------
		 $sql = "SELECT
				mst_subcategory.intId,
				mst_subcategory.strName
				FROM mst_subcategory
				WHERE
				mst_subcategory.intMainCategory =  '$mainCatId' AND
				mst_subcategory.intStatus =  '1' 
				ORDER BY mst_subcategory.strName ASC";
		$result = $db->RunQuery($sql);
		$htmlSub = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$htmlSub .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		//----------------------------------------
		 $sql = "SELECT
				mst_item.intId,
				mst_item.strName
				FROM mst_item
				WHERE 
				mst_item.intStatus = '1' AND 
				mst_item.intSubCategory =  '$subCatId'  AND
				mst_item.dblLastPrice >  '0' AND 
				mst_item.intCurrency >  '0'  
				order by mst_item.strName asc";
		$result = $db->RunQuery($sql);
		$itemCombo = "<option value=\"\"></option>";
		while($row=mysqli_fetch_array($result))
		{
			$itemCombo .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		//-----------------------------------------
		$response['mainCatId'] = $mainCatId;
		$response['subCatCombo'] = $htmlSub;
		$response['subCatId'] = $subCatId;
		$response['itemCombo'] = $itemCombo;
		$response['stockQty'] = getStockBalance_bulk($location,$itemId);
		
		echo json_encode($response);
	}
	//-----------------------------------------------------------
	
	function getStockBalance_bulk($location,$item)
	{
		global $db;
		 		$sql = "SELECT
				Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$location'
				GROUP BY
				ware_stocktransactions_bulk.intItemId";

		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
	function getItemCombo($subCat)
	{
		global $db;
		 		$sql = "SELECT
						mst_item.intId,
						mst_item.strName
						FROM mst_item
						WHERE 
						mst_item.intStatus = '1' AND 
						mst_item.intSubCategory =  '$subCat'  AND
						mst_item.dblLastPrice >  '0' AND 
						mst_item.intCurrency >  '0' 
						order by mst_item.strName asc";

		$html = "<option value=\"\"></option>";
		$result = $db->RunQuery($sql);
		$row = mysqli_fetch_array($result);
		while($row=mysqli_fetch_array($result))
		{
			$html .= "<option value=\"".$row['intId']."\">".$row['strName']."</option>";
		}
		return $html;
	}
	//--------------------------------------------------------------
function getAllocatedQty($location,$itemId)
{
	global $db;
	  $sql = "SELECT
		Sum(ware_itemcreationdetails.dblQty) as Qty 
		FROM
		ware_itemcreationdetails
		Inner Join ware_itemcreationheader ON ware_itemcreationdetails.intAllocationNo = ware_itemcreationheader.intAllocationNo AND ware_itemcreationdetails.intAllocationYear = ware_itemcreationheader.intAllocationYear
		WHERE
		ware_itemcreationdetails.intSubItemId =  '$itemId' AND
		ware_itemcreationheader.intCompanyId =  '$location' AND
		ware_itemcreationheader.intStatus >1  AND 
		ware_itemcreationheader.intStatus <=ware_itemcreationheader.intApproveLevels  ";

	$result = $db->RunQuery($sql);
	$row=mysqli_fetch_array($result);
		return val($row['Qty']);	
}
//--------------------------------------------------------------
?>
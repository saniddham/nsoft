var basePath	="presentation/warehouse/newItemAllowcation/addNew/";
var reportId	="912";
			
$(document).ready(function() {
	
  		$("#frmNewItmAllocation").validationEngine();
		//--------
		$("#insertRow").die('click').live('click',function(){
			insertRow();
		});
		//--------
		$(".mainCat").die('change').live('change',function(){
			loadSubCategories(this);
		});
		//--------
		$(".subCat").die('change').live('change',function(){
			loadItems(this);
		});
		//--------
		$(".item").die('change').live('change',function(){
			loadItemDetails(this);
		});
		//--------
		
		$(".allocate").die('click').live('click',function(){
			closePopUp();	
			loadItemAllocation(this);
		});
		//--------
		$('.delImg').die('click').live('click',function(){
			var rowCount = document.getElementById('tbMain').rows.length;
			if(rowCount>2){
			$(this).parent().parent().remove();
			var mainItem=$(this).parent().parent().find('.item').val();
			deleteDuplicate(mainItem);
			}
		});
		//----- 
		$('.delImgP').die('click').live('click',function(){
			var rowCount = document.getElementById('tblItemsPopup').rows.length;
			if(rowCount>2)
			$(this).parent().parent().remove();
		});
		//--------
		$(".mainCatP").die('change').live('change',function(){
			loadSubCategoriesP(this);
		});
		//--------
		$("#insertRowP").die('click').live('click',function(){
			insertRowP();
		});
		//--------
		$(".mainCatP").die('change').live('change',function(){
			loadSubCategoriesP(this);
		});
		//--------
		$(".subCatP").die('change').live('change',function(){
			loadItemsP(this);
		});
		//--------
		$(".itemP").die('change').live('change',function(){
			loadItemDetailsP(this);
		});
		//--------
		$('#butDelete').die('click').live('click',function(){
			deleteDuplicate(itemId);
			unColorRow(this);
		});	
		//--------
		$('#butClose1').die('click').live('click',function(){
			disablePopup();
		});	
		//--------
				
  //-------------------------------------------------------
 
  $('#frmNewItmAllocation #butSave').click(function(){
	var requestType = '';
	if ($('#frmNewItmAllocation').validationEngine('validate'))   
    { 
		showWaiting();
		var data = "requestType=save";
		
			data+="&serialNo="		+	$('#txtAllocationNo').val();
			data+="&Year="	+	$('#txtYear').val();
			data+="&note="	+	$('#txtNote').val();

			var rowCount1 = document.getElementById('tbMain').rows.length;
			if(rowCount1==1){
				alert("items not selected to Allocate");hideWaiting();
				return false;				
			}
			//------------------
			var arr1="[";
			
			for(var i=1;i<rowCount1;i++)
			{
					var mainItem = 	document.getElementById('tbMain').rows[i].cells[3].childNodes[0].value;
					var Qty = 	document.getElementById('tbMain').rows[i].cells[5].childNodes[0].value;
					
					if(Qty>0){
						arr1 += "{";
						arr1 += '"mainItem":"'+	mainItem +'",' ;
						arr1 += '"Qty":"'+		Qty +'"' ;
						arr1 +=  '},';
					}
			}
			arr1 = arr1.substr(0,arr1.length-1);
			arr1 += " ]";
			
			data+="&arr1="	+	arr1;
			//------------------
			var rowCount = document.getElementById('tblMainItemWiseItems').rows.length;
			if(rowCount==1){
				alert("items not selected to Allocate");hideWaiting();
				return false;				
			}
			var arr="[";
			
			for(var i=1;i<rowCount;i++)
			{
					var mainItem = 	document.getElementById('tblMainItemWiseItems').rows[i].cells[0].id;
					var allocatedItemId = 	document.getElementById('tblMainItemWiseItems').rows[i].cells[1].id;
					var Qty = 	document.getElementById('tblMainItemWiseItems').rows[i].cells[2].innerHTML;
					var itemNm = 	document.getElementById('tblMainItemWiseItems').rows[i].cells[3].innerHTML;
					var mainItemNm = 	document.getElementById('tblMainItemWiseItems').rows[i].cells[4].innerHTML;
					
					if(Qty>0){
						arr += "{";
						arr += '"mainItem":"'+	mainItem +'",' ;
						arr += '"allocatedItemId":"'+	allocatedItemId +'",' ;
						arr += '"itemNm":"'+	URLEncode(itemNm) +'",' ;
						arr += '"mainItemNm":"'+	URLEncode(mainItemNm) +'",' ;
						arr += '"Qty":"'+		Qty +'"' ;
						arr +=  '},';
					}
			}
			arr = arr.substr(0,arr.length-1);
			arr += " ]";
			
			data+="&arr="	+	arr;
			//------
		///////////////////////////// save main infomations /////////////////////////////////////////
		var url = basePath+"newItemAllocation-db-set.php";
     	var obj = $.ajax({
			url:url,
			type:'post',
			dataType: "json",  
			data:data,//$("#frmSampleInfomations").serialize()+'&requestType='+requestType,
			//data:'{"requestType":"addsampleInfomations"}',
			async:false,
			
			success:function(json){
					$('#frmNewItmAllocation #butSave').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
					if(json.type=='pass')
					{
						//$('#frmSampleInfomations').get(0).reset();
						//saveImage(json.sampleNo,json.sampleYear,json.revisionNo);
						//loadCombo_frmSampleInfomations();
						var t=setTimeout("alertx()",1000);
						$('#txtAllocationNo').val(json.serialNo);
						$('#txtYear').val(json.year);
						if(json.confirmationMode==1){
							$('#butConfirm').show();
						}
						else{
							$('#butConfirm').hide();
						}
						return;
					}
				},
			error:function(xhr,status){
					
					$('#frmNewItmAllocation #butSave').validationEngine('showPrompt', errormsg(xhr.status),'fail');
					var t=setTimeout("alertx()",3000);
					//function (xhr, status){errormsg(status)}
				}		
			});
			hideWaiting();
	}
   });
   
	
	//--------------refresh the form----------
	$('#frmNewItmAllocation #butNew').click(function(){
		window.location.href = "?q=450";
		$('#frmNewItmAllocation').get(0).reset();
		clearRows();
		$('#frmNewItmAllocation #txtAllocationNo').val('');
		$('#frmNewItmAllocation #txtYear').val('');
		$('#frmNewItmAllocation #txtNote').val('');
	});
	//----------------------------------------

//-----------------------------------
$('#butReport').click(function(){
	if($('#txtPoNo').val()!=''){
		window.open('?q='+reportId+'&allocationNo='+$('#txtAllocationNo').val()+'&year='+$('#txtYear').val());	
	}
	else{
		alert("There is no Allocation No to view");
	}
});
//----------------------------------	
$('#butConfirm').click(function(){
	if($('#txtPoNo').val()!=''){
		window.open('?q='+reportId+'&allocationNo='+$('#txtAllocationNo').val()+'&year='+$('#txtYear').val()+'&approveMode=1');	
	}
	else{
		alert("There is no Allocation No to confirm");
	}
});
//-----------------------------------------------------
$('#butClose').click(function(){
		//load('main.php');	
});
//--------------refresh the form-----------------------
	$('#frmNewItmAllocation #butNew').click(function(){
		window.location.href = "?q=450";
		$('#frmNewItmAllocation').get(0).reset();
		clearRows();
		$('#frmNewItmAllocation #txtAllocationNo').val('');
		$('#frmNewItmAllocation #txtYear').val('');
		$('#frmNewItmAllocation #txtNote').val('');
	});
//-----------------------------------------------------
});

//----------end of ready -------------------------------
//-------------------------------------
function alertx()
{
	$('#frmNewItmAllocation #butSave').validationEngine('hide')	;
}
//----------------------------------------------- 
function alertDelete()
{
	$('#frmNewItmAllocation #butDelete').validationEngine('hide')	;
}
//----------------------------------------------- 
function closePopUp(){
	
}
//-----------------------------------------------------------------
function insertRow()
{
	var tbl = document.getElementById('tbMain');	
	var rows = tbl.rows.length;
	//tbl.rows[1].cells[5].childNodes[0].nodeValue;
	tbl.insertRow(rows);
	tbl.rows[rows].className='normalfnt';
	tbl.rows[rows].innerHTML = tbl.rows[rows-1].innerHTML;
	document.getElementById('tbMain').rows[rows].cells[2].childNodes[0].value='';
	document.getElementById('tbMain').rows[rows].cells[3].childNodes[0].value='';
	document.getElementById('tbMain').rows[rows].cells[3].childNodes[0].innerHTML='';
	document.getElementById('tbMain').rows[rows].cells[4].innerHTML='';
	document.getElementById('tbMain').rows[rows].cells[5].childNodes[0].value='';
	document.getElementById('tbMain').rows[rows].cells[5].childNodes[0].removeAttribute('readOnly'); 
	
  	document.getElementById('tbMain').rows[rows].cells[0].style.backgroundColor ='#FFFFFF';
  	document.getElementById('tbMain').rows[rows].cells[1].style.backgroundColor ='#FFFFFF';
  	document.getElementById('tbMain').rows[rows].cells[2].style.backgroundColor ='#FFFFFF';
  	document.getElementById('tbMain').rows[rows].cells[3].style.backgroundColor ='#FFFFFF';
  	document.getElementById('tbMain').rows[rows].cells[4].style.backgroundColor ='#FFFFFF';
  	document.getElementById('tbMain').rows[rows].cells[5].style.backgroundColor ='#FFFFFF';
  	document.getElementById('tbMain').rows[rows].cells[6].style.backgroundColor ='#FFFFFF';
	
	
/*		//-------------------------------
		$(".allocate").click(function(){
			loadItemAllocation(this);
		});
		//-------------------------------
		$('.delImg').click(function(){
			var rowCount = document.getElementById('tbMain').rows.length;
			if(rowCount>2){
			$(this).parent().parent().remove();
			var mainItem=$(this).parent().parent().find('.item').val();
			deleteDuplicate(mainItem);
			}
		});
*/}			
//-----------------------------------------------------------------
function insertRowP()
{
	var tbl = document.getElementById('tblItemsPopup');	
	var rows = tbl.rows.length;
	//tbl.rows[1].cells[5].childNodes[0].nodeValue;
	tbl.insertRow(rows);
	tbl.rows[rows].className='normalfnt';
	tbl.rows[rows].innerHTML = tbl.rows[rows-1].innerHTML;
	document.getElementById('tblItemsPopup').rows[rows].cells[2].childNodes[0].value='';
	document.getElementById('tblItemsPopup').rows[rows].cells[3].childNodes[0].value='';
	document.getElementById('tblItemsPopup').rows[rows].cells[3].childNodes[0].innerHTML='';
	document.getElementById('tblItemsPopup').rows[rows].cells[4].innerHTML='';
	document.getElementById('tblItemsPopup').rows[rows].cells[5].childNodes[0].value='';
	document.getElementById('tblItemsPopup').rows[rows].cells[6].innerHTML='';
	document.getElementById('tblItemsPopup').rows[rows].cells[5].childNodes[0].id='';
	document.getElementById('tblItemsPopup').rows[rows].cells[6].id='';
	document.getElementById('tblItemsPopup').rows[rows].cells[4].id='';
	
}	
//----------------------------------------------
function loadSubCategoriesP(obj){
	var mainCat = $(obj).parent().parent().find('.mainCatP').val();
	var url 		=  basePath+"newItemAllocation-db-get.php?requestType=loadSubCategoriesCombo&mainCat="+mainCat;
	var httpobj 	= $.ajax({url:url,async:false});
	
	var row=obj.parentNode.parentNode.rowIndex;
	//$(obj).parent().parent().find('#cboItem').text(httpobj.responseText);
	document.getElementById('tblItemsPopup').rows[row].cells[2].childNodes[0].innerHTML=httpobj.responseText;
}
//----------------------------------------------
function loadSubCategories(obj){
	var mainCat = $(obj).parent().parent().find('.mainCat').val();
	var url 		=  basePath+"newItemAllocation-db-get.php?requestType=loadSubCategoriesCombo&mainCat="+mainCat;
	var httpobj 	= $.ajax({url:url,async:false});
	
	var row=obj.parentNode.parentNode.rowIndex;
	//$(obj).parent().parent().find('#cboItem').text(httpobj.responseText);
	document.getElementById('tbMain').rows[row].cells[2].childNodes[0].innerHTML=httpobj.responseText;
}
//----------------------------------------------
function loadItems(obj){
	var mainCat = $(obj).parent().parent().find('.mainCat').val();
	var subCat = $(obj).parent().parent().find('.subCat').val();
	var url 		=  basePath+"newItemAllocation-db-get.php?requestType=loadItemCombo&mainCat="+mainCat+"&subCat="+subCat;
	var httpobj 	= $.ajax({url:url,async:false});
	
	var row=obj.parentNode.parentNode.rowIndex;
	//$(obj).parent().parent().find('#cboItem').text(httpobj.responseText);
	document.getElementById('tbMain').rows[row].cells[3].childNodes[0].innerHTML=httpobj.responseText;
}
//-----------------------------------------------
function loadItemDetails(obj){
	var itemId = $(obj).parent().parent().find('.item').val();
	var url 		=  basePath+"newItemAllocation-db-get.php?requestType=loadItemDetails&itemId="+itemId;
/*	var httpobj 	= $.ajax({url:url,async:false});
	$(obj).parent().parent().find('.uom').text(httpobj.responseText);
*/	
		var url =  basePath+"newItemAllocation-db-get.php?requestType=loadItemDetails";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"itemId="+itemId,
			async:false,
			success:function(json){

					$(obj).parent().parent().find('.uom').attr('id',(json.uomId));
					$(obj).parent().parent().find('.uom').text(json.uom);
					$(obj).parent().parent().find('.qtyCellP').attr('id',(json.stockQty));
					$(obj).parent().parent().find('.stkQtyP').html(json.stockQty);
					$(obj).parent().parent().find('.stkQtyP').attr('id',(json.stockQty));
			}
		});
	
	
	
}
//----------------------------------------------
function loadItemsP(obj){
	var mainCat = $(obj).parent().parent().find('.mainCatP').val();
	var subCat = $(obj).parent().parent().find('.subCatP').val();
	var url 		=  basePath+"newItemAllocation-db-get.php?requestType=loadItemCombo&mainCat="+mainCat+"&subCat="+subCat;
	var httpobj 	= $.ajax({url:url,async:false});
	
	var row=obj.parentNode.parentNode.rowIndex;
	//$(obj).parent().parent().find('#cboItem').text(httpobj.responseText);
	document.getElementById('tblItemsPopup').rows[row].cells[3].childNodes[0].innerHTML=httpobj.responseText;
}
//-----------------------------------------------
function loadItemDetailsP(obj){
	var itemId = $(obj).parent().parent().find('.itemP').val();
	var url 		=  basePath+"newItemAllocation-db-get.php?requestType=loadItemDetails&itemId="+itemId;
/*	var httpobj 	= $.ajax({url:url,async:false});
	$(obj).parent().parent().find('.uom').text(httpobj.responseText);
*/	
		var url =  basePath+"newItemAllocation-db-get.php?requestType=loadItemDetails";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"itemId="+itemId,
			async:false,
			success:function(json){

					$(obj).parent().parent().find('.uomP').attr('id',(json.uomId));
					$(obj).parent().parent().find('.uomP').text(json.uom);
					$(obj).parent().parent().find('.qtyCellP').attr('id',(json.stockQty));
					$(obj).parent().parent().find('.stkQtyP').html(json.stockQty);
					$(obj).parent().parent().find('.qtyP').attr('class','validate[required,custom[number],max['+(json.stockQty-json.allocatedQty)+']] qtyP');
			}
		});
	
	
	
}
//----------------------------------------------
function clearRows()
{
	//var rowCount = $('#tblDispatchPopup >tbody >tr').length;
	var rowCount = document.getElementById('tbMain').rows.length;
	//alert(rowCount);
	//var cellCount = document.getElementById('tblMain').rows[1].cells.length;
	for(var i=2;i<rowCount;i++)
	{
			document.getElementById('tbMain').deleteRow(1);
			
	}
	
	var rowCount = document.getElementById('tblMainItemWiseItems').rows.length;
	//alert(rowCount);
	//var cellCount = document.getElementById('tblMain').rows[1].cells.length;
	for(var i=1;i<rowCount;i++)
	{
			document.getElementById('tblMainItemWiseItems').deleteRow(1);
			
	}
	$('#tbMain .mainCat').each(function(){
	unColorRow(this);	
	});
}
//----------------------------------------------
function add_new_row(table,rowcontent){
        if ($(table).length>0){
            if ($(table+' > tbody').length==0) $(table).append('<tbody />');
            ($(table+' > tr').length>0)?$(table).children('tbody:last').children('tr:last').append(rowcontent):$(table).children('tbody:last').append(rowcontent);
        }
    }
//----------------------------------------------- 
function loadItemAllocation(obj){
		closePopUp();

		rw=obj.parentNode.parentNode.rowIndex;
	 	var itemId = $(obj).parent().parent().find('.item').val();
	 	var qty =$(obj).parent().parent().find('.qty').val();
		var flag=checkForAlreadySelected(itemId);
		
		if(itemId==''){
				alert("Please select the item.");
				return false;
		}
		else if(qty<=0){
				alert("Please enter the Qty.");
				return false;
		}

		popupWindow3('1');
		$('#popupContact1').load( basePath+'newItemAllocationPopup.php?itemId='+itemId+'&qty='+qty+'&flag='+flag,function(){
				selectAlreadySelected(itemId);
				//--------
				$('#butAdd').die('click').live('click',function(){
					addClickedRows(rw);
					closePopUp();
				});	
				//--------
		});	
}
//----------------------------------------------------
function addClickedRows(rw)
{
  		$("#frmNewItmAllocationPopup").validationEngine();
		var flag=validateQty();
		if(flag>0){
			alert("Invalid Qty");
			return false;
		}
		//return false;
	
	var rowCount = document.getElementById('tblItemsPopup').rows.length;
	
	var totQty=0;
	var flag=0;
	for(var i=1;i<rowCount;i++)
	{
		var mainItemId=document.getElementById('itemIdMain').innerHTML;
		var itemId=document.getElementById('tblItemsPopup').rows[i].cells[3].childNodes[0].value;
		var Qty=parseFloat(document.getElementById('tblItemsPopup').rows[i].cells[5].childNodes[0].value);
		var id=document.getElementById('tblItemsPopup').rows[i].cells[3].childNodes[0];
		var itemName=id.options[id.selectedIndex].text;
		var Mainitemname=document.getElementById('itemMain').innerHTML;

		if(i==1){
			deleteDuplicate(mainItemId);
		}

			if(Qty==''){
			 Qty=0;
			}
		totQty+=Qty;
		var content='<tr class="normalfnt">';
		content +='<td align="center" bgcolor="#FFFFFF" id="'+mainItemId+'">'+mainItemId+'</td>';
		content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'">'+itemId+'</td>';
		content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'">'+Qty+'</td>';
		content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'">'+itemName+'</td>';
		content +='<td align="center" bgcolor="#FFFFFF" id="'+itemId+'">'+Mainitemname+'</td>';
		content +='</tr>';
	//	alert(content);
		
		if(Qty>0){
		flag++;	
		add_new_row('#frmNewItmAllocation #tblMainItemWiseItems',content);
		}
	}
 //	$(obj).parent().parent().find('.invoQty').val(totQty);
 if(flag>0){
  	document.getElementById('tbMain').rows[rw].cells[0].style.backgroundColor ='#91D9D7';
  	document.getElementById('tbMain').rows[rw].cells[1].style.backgroundColor ='#91D9D7';
  	document.getElementById('tbMain').rows[rw].cells[2].style.backgroundColor ='#91D9D7';
  	document.getElementById('tbMain').rows[rw].cells[3].style.backgroundColor ='#91D9D7';
  	document.getElementById('tbMain').rows[rw].cells[4].style.backgroundColor ='#91D9D7';
  	document.getElementById('tbMain').rows[rw].cells[5].style.backgroundColor ='#91D9D7';
  	document.getElementById('tbMain').rows[rw].cells[6].style.backgroundColor ='#91D9D7';
 }
	
	disablePopup();
	
}
//-------------------------------------
function validateQty(){
	
	var rowCount = document.getElementById('tblItemsPopup').rows.length;
	var qty=0;
	var flag=0;
	for(var i=1;i<rowCount;i++)
	{
		 qty = parseFloat(document.getElementById('tblItemsPopup').rows[i].cells[5].childNodes[0].value);
		 stockQty = parseFloat(document.getElementById('tblItemsPopup').rows[i].cells[5].id);
	
		if(qty>stockQty){
			flag++;
		}
		if(qty<=0){
			flag++;
		}
	}
		return flag;
}
//--------------------------------------------
function deleteDuplicate(itemId){
	var rowCount = document.getElementById('tblMainItemWiseItems').rows.length;
	for(var i=1;i<rowCount;i++)
	{
		 if(document.getElementById('tblMainItemWiseItems').rows[i].cells[0].id==itemId){
			document.getElementById('tblMainItemWiseItems').deleteRow(i);
			rowCount = document.getElementById('tblMainItemWiseItems').rows.length;
			i=0;
		 }
	}
}
//------------------------------------------------------------
function selectAlreadySelected(clickedMainItemId){
	
	var rowCount = document.getElementById('tblMainItemWiseItems').rows.length;
	var flag=0;
	for(var i=1;i<rowCount;i++)
	{
			var mainItemId = 	document.getElementById('tblMainItemWiseItems').rows[i].cells[0].innerHTML;
			var itemId = 	document.getElementById('tblMainItemWiseItems').rows[i].cells[1].innerHTML;
			var Qty = 	document.getElementById('tblMainItemWiseItems').rows[i].cells[2].innerHTML;
			
			if(mainItemId==clickedMainItemId){
				flag++;
					var tbl = document.getElementById('tblItemsPopup');	
					var rows = tbl.rows.length;
					tbl.insertRow(rows);
					tbl.rows[rows].className='normalfnt';
					tbl.rows[rows].innerHTML = tbl.rows[rows-1].innerHTML;
					document.getElementById('tblItemsPopup').rows[rows].cells[2].childNodes[0].value='';
					document.getElementById('tblItemsPopup').rows[rows].cells[3].childNodes[0].value=itemId;
					document.getElementById('tblItemsPopup').rows[rows].cells[3].childNodes[0].innerHTML='';
					document.getElementById('tblItemsPopup').rows[rows].cells[4].innerHTML='';
					document.getElementById('tblItemsPopup').rows[rows].cells[5].childNodes[0].value=Qty;
					
					loadMainCategorySubCategory(rows,itemId);
					
					
					
						//-------------------------------
						$('.delImg').click(function(){
							var rowCount = document.getElementById('tblItemsPopup').rows.length;
							if(rowCount>2)
							$(this).parent().parent().remove();
						});
			}
			var url =  basePath+"newItemAllocation-db-get.php?requestType=loadItemDetails";
			var httpobj = $.ajax({
				url:url,
				dataType:'json',
				data:"itemId="+itemId,
				async:false,
				success:function(json){
/*					document.getElementById('tblItemsPopup').rows[rows].cells[2].childNodes[0].value=json.subCatId;
					document.getElementById('tblItemsPopup').rows[rows].cells[3].childNodes[0].innerHTML=json.itemCombo;
					document.getElementById('tblItemsPopup').rows[rows].cells[3].childNodes[0].value=itemId;
*/					document.getElementById('tblItemsPopup').rows[rows].cells[4].id=json.uomId;
					document.getElementById('tblItemsPopup').rows[rows].cells[4].innerHTML=json.uom;
					document.getElementById('tblItemsPopup').rows[rows].cells[5].childNodes[0].value=Qty;
					document.getElementById('tblItemsPopup').rows[rows].cells[5].childNodes[0].id=json.stockQty;
					document.getElementById('tblItemsPopup').rows[rows].cells[5].id=json.stockQty;
					document.getElementById('tblItemsPopup').rows[rows].cells[5].childNodes[0].className='validate[required,custom[number],max['+(json.stockQty-json.allocatedQty)+']] qtyP';
				//	$(this).parent().parent().find('.qtyP').attr('class','validate[required,custom[number],max['+json.stockQty+']] qtyP');

				}
			});
	}
	if(flag>0){
		document.getElementById('tblItemsPopup').deleteRow(1);
	}
}
//------------------------------------------------------------------
function checkForAlreadySelected(itemId){
	var rowCount = document.getElementById('tblMainItemWiseItems').rows.length;
	var flag=0;
	for(var i=1;i<rowCount;i++)
	{
			var mainItemId = 	document.getElementById('tblMainItemWiseItems').rows[i].cells[0].innerHTML;
			var Qty = 	document.getElementById('tblMainItemWiseItems').rows[i].cells[2].innerHTML;
			
			if(mainItemId==itemId){
				flag++;
			}
	}
	
	return flag;
}
//------------------------------------------------------------------
function unColorRow(obj){
	rw=obj.parentNode.parentNode.rowIndex;
	
  	document.getElementById('tbMain').rows[rw].cells[0].style.backgroundColor ='#FFFFFF';
  	document.getElementById('tbMain').rows[rw].cells[1].style.backgroundColor ='#FFFFFF';
  	document.getElementById('tbMain').rows[rw].cells[2].style.backgroundColor ='#FFFFFF';
  	document.getElementById('tbMain').rows[rw].cells[3].style.backgroundColor ='#FFFFFF';
  	document.getElementById('tbMain').rows[rw].cells[4].style.backgroundColor ='#FFFFFF';
  	document.getElementById('tbMain').rows[rw].cells[5].style.backgroundColor ='#FFFFFF';
  	document.getElementById('tbMain').rows[rw].cells[6].style.backgroundColor ='#FFFFFF';
}
//-----------------------------------------------------------------
function loadMainCategorySubCategory(rows,itemId){
	
	var url =  basePath+"newItemAllocation-db-get.php?requestType=loadMainCategorySubCategory";
	var httpobj = $.ajax({
	url:url,
	dataType:'json',
	data:"itemId="+itemId,
	async:false,
	success:function(json){
	
	document.getElementById('tblItemsPopup').rows[rows].cells[1].childNodes[0].value=json.mainCatId;
	
	document.getElementById('tblItemsPopup').rows[rows].cells[2].childNodes[0].innerHTML=json.subCatCombo;
	document.getElementById('tblItemsPopup').rows[rows].cells[2].childNodes[0].value=json.subCatId;
	
	document.getElementById('tblItemsPopup').rows[rows].cells[3].childNodes[0].innerHTML=json.itemCombo;
	document.getElementById('tblItemsPopup').rows[rows].cells[3].childNodes[0].value=itemId;

	document.getElementById('tblItemsPopup').rows[rows].cells[6].id=json.stkQty;
	document.getElementById('tblItemsPopup').rows[rows].cells[6].innerHTML=json.stockQty;
	

			}
		});
}
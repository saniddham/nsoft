<?php 
	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$companyId 	= $_SESSION['CompanyID'];
	include "{$backwardseperator}dataAccess/Connector.php";
	$response = array('type'=>'', 'msg'=>'');
	
	/////////// parameters /////////////////////////////
	/////////// parameters /////////////////////////////
	$requestType 	= $_REQUEST['requestType'];
	
	$serialNo 	 = $_REQUEST['serialNo'];
	$year 		 = $_REQUEST['Year'];
	$note 	 = $_REQUEST['note'];
	
	$arr1 		= json_decode($_REQUEST['arr1'], true);
	$arr 		= json_decode($_REQUEST['arr'], true);
	
	$programName='New Item Allocation';
	$programCode='P0449';

	$ApproveLevels = (int)getApproveLevel($programName);
	$allocationApproveLevel = $ApproveLevels+1;
	
//------------save---------------------------	
	if($requestType=='save')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		$savableFlag= 1;
		
		if($serialNo==''){
			$serialNo 	= getNextAllocationNo();
			$year = date('Y');
			$editMode=0;
			$savedStatus=$allocationApproveLevel;
			$savedLevels=$ApproveLevels;
		}
		else{
			$editMode=1;
			$savableFlag=getSaveStatus($serialNo,$year);
			$sql = "SELECT
			ware_itemcreationheader.intStatus, 
			ware_itemcreationheader.intApproveLevels 
			FROM
			ware_itemcreationheader
			WHERE
			ware_itemcreationheader.intAllocationNo =  '$serialNo' AND
			ware_itemcreationheader.intAllocationYear =  '$year'";

			$result = $db->RunQuery2($sql);
			$row=mysqli_fetch_array($result);
			$savedStatus=$row['intStatus'];
			$savedLevels=$row['intApproveLevels'];
		}
		
		//--------------------------
		$editPermission=loadEditMode($programCode,$savedStatus,$savedLevels,$userId);
		$locationEditFlag=getEditLocationValidation('NEWITEMCREATION',$companyId,$serialNo,$year);
		//--------------------------
		
		//-----------delete and insert to header table-----------------------
		 if(( $locationEditFlag==1) && ($editMode==1)){
			 $rollBackFlag=1;
			 $rollBackMsg="Invalid Edit Location";
		 }
		 else if($savableFlag==0){
			 $rollBackFlag=1;
			 $rollBackMsg="This Allocation No is already confirmed.cant edit";
		 }
		 else if($editPermission==0){
			 $rollBackFlag=1;
			 $rollBackMsg="No Permission to save.";
		 }
		else if($editMode==1){//edit existing grn
		$sql = "UPDATE `ware_itemcreationheader` SET intStatus ='$allocationApproveLevel', 
												       intApproveLevels ='$ApproveLevels', 
													  strNote ='$note', 
													  intModifiedBy ='$userId', 
													  dtmModifiedDate =now() 
				WHERE (`intAllocationNo`='$serialNo') AND (`intAllocationYear`='$year')";
		$result = $db->RunQuery2($sql);
		}
		else{
			//$sql = "DELETE FROM `ware_itemcreationheader` WHERE (`intAllocationNo`='$serialNo') AND (`intAllocationYear`='$year')";
			//$result1 = $db->RunQuery2($sql);

			$sql = "INSERT INTO `ware_itemcreationheader` (`intAllocationNo`,`intAllocationYear`,strNote,intStatus,intApproveLevels,dtmCreateDate,intUser,intCompanyId) 
					VALUES ('$serialNo','$year','$note','$allocationApproveLevel','$ApproveLevels',now(),'$userId','$companyId')";
			$result = $db->RunQuery2($sql);
			$maxAppByStatus=(int)getMaxAppByStatus($serialNo,$year)+1;
			$sql = "UPDATE `ware_itemcreationheader_approvedby` SET intStatus ='$maxAppByStatus' 
					WHERE (`intAllocationNo`='$serialNo') AND (`intYear`='$year') AND (`intStatus`='0')";
			$result1 = $db->RunQuery2($sql);
		}
		//-----------delete and insert to detail table-----------------------
		if($result){
		$sql = "DELETE FROM `ware_itemcreationdetails` WHERE (`intAllocationNo`='$serialNo') AND (`intAllocationYear`='$year')";
		$result2 = $db->RunQuery2($sql);
		$sql = "DELETE FROM `ware_itemcreationdetails_main` WHERE (`intAllocationNo`='$serialNo') AND (`intAllocationYear`='$year')";
		$result2 = $db->RunQuery2($sql);
			$toSave=0;
			$saved=0;
			//$rollBackFlag=0;
			if($rollBackFlag!=1) 
			$rollBackMsg ="Maximum Invoice Qtys for items are..."; 
			//-------------
			foreach($arr1 as $arrVal1)
			{
				$location 	= $_SESSION['CompanyID'];
				$company 	= $_SESSION['headCompanyId'];
				$mainItemId 	= $arrVal1['mainItem'];
				$Qty 		 = $arrVal1['Qty'];
		
				//----------------------------
				if($rollBackFlag!=1){
					$sql = "INSERT INTO `ware_itemcreationdetails_main` (`intAllocationNo`,`intAllocationYear`,intMainItemId,`dblQty`) 
					VALUES ('$serialNo','$year','$mainItemId','$Qty')";
					$result3 = $db->RunQuery2($sql);
					if($result3==1){
					$saved++;
					}
				}
				$toSave++;
			}
			
			if($rollBackFlag!=1){
	
				$rowI=loadItemDetails($mainItemId);
				$grnRateNew=$rowI['dblLastPrice'];
				$currenNew=$rowI['intCurrency'];
			
				if($grnRateNew<=0){
					$rollBackFlag=1;
					$rollBackMsg="Price Not Added for items";	
				}
				else if($currenNew<=0){
					$rollBackFlag=1;
					$rollBackMsg="Currency Not Added for items";	
				}
			}
			
			
			//-------------
			if($rollBackFlag!=1){
			foreach($arr as $arrVal)
			{
				$location 	= $_SESSION['CompanyID'];
				$company 	= $_SESSION['headCompanyId'];
				$mainItemId 	= $arrVal['mainItem'];
				$itemId 	 = $arrVal['allocatedItemId'];
				$item 	 = $arrVal['itemNm'];
				$mainItemNm 	 = $arrVal['mainItemNm'];
				$Qty 		 = $arrVal['Qty'];
		
				$stockBalQty=getStockBalance_bulk($location,$itemId);
				$allocatedQty=getAllocatedQty($location,$itemId);
				$rowO=loadItemDetails($itemId);
				$grnRateOld=$rowO['dblLastPrice'];
				$currenOld=$rowO['intCurrency'];
	
				//------check maximum ISSUE Qty--------------------
				$allocateQty = $stockBalQty-$allocatedQty;
				if($grnRateOld<=0){
					$rollBackFlag=1;
					$rollBackMsg="Price Not Added for items";	
				}
				else if($currenOld<=0){
					$rollBackFlag=1;
					$rollBackMsg="Currency Not Added for items";	
				}
				else if($Qty>$allocateQty){
				//	call roll back--------****************
					$rollBackFlag=1;
					$rollBackMsg .="</br> Main item :".$mainItemNm." , Sub item :".$item." =".$allocateQty;
					//exit();
				}

				//----------------------------
				if($rollBackFlag!=1){
					$sql = "INSERT INTO `ware_itemcreationdetails` (`intAllocationNo`,`intAllocationYear`,intMainItemId,`intSubItemId`,`dblQty`) 
					VALUES ('$serialNo','$year','$mainItemId','$itemId','$Qty')";
					$result3 = $db->RunQuery2($sql);
					if($result3==1){
					$saved++;
					}
				}
				$toSave++;
			}
		}

			//--------------
		}
		
		//echo $rollBackFlag;
		
		if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= '';
		}
		else if(($result) && ($toSave==$saved)){
			$confirmationMode=getConfirmationMode($programCode,$userId,$ApproveLevels,$allocationApproveLevel);
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			if($editMode==1)
			$response['msg'] 		= 'Updated successfully.';
			else
			$response['msg'] 		= 'Saved successfully.';
			
			$response['serialNo'] 		= $serialNo;
			$response['year'] 		= $year;
			$response['confirmationMode'] = $confirmationMode;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sql;
		}
		
		$db->CloseConnection();		
	}
//----------------------------------------
	echo json_encode($response);
//-----------------------------------------	
	function getNextAllocationNo()
	{
		global $db;
		global $companyId;
		$sql = "SELECT
				sys_no.intWareItmAllocationNo
				FROM sys_no
				WHERE
				sys_no.intCompanyId =  '$companyId'
				";	
		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		$nextAllocNo = $row['intWareItmAllocationNo'];
		
		$sql = "UPDATE `sys_no` SET intWareItmAllocationNo=intWareItmAllocationNo+1 WHERE (`intCompanyId`='$companyId')  ";
		$db->RunQuery2($sql);
		
		return $nextAllocNo;
	}
	//--------------------------------------------------------------
	function getStockBalance_bulk($location,$item)
	{
		global $db;
		       $sql = "SELECT
				Sum(ware_stocktransactions_bulk.dblQty) AS stockBal
				FROM ware_stocktransactions_bulk
				WHERE
				ware_stocktransactions_bulk.intItemId =  '$item' AND
				ware_stocktransactions_bulk.intLocationId =  '$location'
				GROUP BY
				ware_stocktransactions_bulk.intItemId";

		$result = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($result);
		return val($row['stockBal']);	
	}
	//--------------------------------------------------------------
function loadItemDetails($mainItem)
{
	global $db;
	  $sql = "SELECT
			mst_item.dblLastPrice,
			mst_item.intCurrency 
			FROM mst_item 
			WHERE 
			mst_item.intId =  '$mainItem'";

	$result = $db->RunQuery2($sql);
	$row=mysqli_fetch_array($result);
	return $row;	
}
	//--------------------------------------------------------------
function getAllocatedQty($location,$itemId)
{
	global $db;
	  $sql = "SELECT
Sum(ware_itemcreationdetails.dblQty) as Qty 
FROM
ware_itemcreationdetails
Inner Join ware_itemcreationheader ON ware_itemcreationdetails.intAllocationNo = ware_itemcreationheader.intAllocationNo AND ware_itemcreationdetails.intAllocationYear = ware_itemcreationheader.intAllocationYear
WHERE
ware_itemcreationdetails.intSubItemId =  '$itemId' AND
ware_itemcreationheader.intCompanyId =  '$location' AND
ware_itemcreationheader.intStatus >1  AND 
ware_itemcreationheader.intStatus <=ware_itemcreationheader.intApproveLevels  ";

	$result = $db->RunQuery2($sql);
	$row=mysqli_fetch_array($result);
		return val($row['Qty']);	
}
	//--------------------------------------------------------------
	function getSaveStatus($serialNo,$year)
	{
		global $db;
		$sql = "SELECT ware_itemcreationheader.intStatus, ware_itemcreationheader.intApproveLevels FROM ware_itemcreationheader WHERE (intAllocationNo='$serialNo') AND (`intAllocationYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$status = $row['intStatus'];
			
			if(($status==0) || ($row['intStatus']==($row['intApproveLevels']+1))){
				$editableFlag=1	;
			}
			else{
				$editableFlag=0;	
			}
			
		return $editableFlag;
	}
//------------------------------------------------------------
	function getEditLocationValidation($type,$location,$serialNo,$year)
	{
		$flag= 1;
		
		global $db;
		  $sql = "SELECT
					ware_itemcreationheader.intCompanyId
					FROM
					ware_itemcreationheader
					Inner Join mst_locations ON ware_itemcreationheader.intCompanyId = mst_locations.intId
					WHERE
					ware_itemcreationheader.intAllocationNo =  '$serialNo' AND
					ware_itemcreationheader.intAllocationYear =  '$year'";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			if($location == $row['intCompanyId'])
			$flag=0;
 			
		return $flag;

}
//------------------------------function loadConfirmatonMode-------------------
function getConfirmationMode($programCode,$intUser,$approveLevels,$status){
		global $db;
		$k=$approveLevels+2-$status;
		$sqlp = "SELECT 
		menupermision.intEdit, 
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		if($rowp['int'.$k.'Approval']==1) 
		 $confirmatonMode=1; 
		else
		 $confirmatonMode=0; 
	 
	return $confirmatonMode;
}
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		$resultp = $db->RunQuery2($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
		 
		// $editMode;
		 
	return $editMode;
}
//------------------------------function get MaxAppByStatus---------------------
function getMaxAppByStatus($serialNo,$year){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	  $sql = "SELECT
				max(ware_itemcreationheader_approvedby.intStatus) as status 
				FROM
				ware_itemcreationheader_approvedby
				WHERE
				ware_itemcreationheader_approvedby.intAllocationNo =  '$serialNo' AND
				ware_itemcreationheader_approvedby.intYear =  '$year'";
				
		$resultm = $db->RunQuery2($sql);
		 $rowm=mysqli_fetch_array($resultm);
		 
		 return $rowm['status'];

}
//--------------------------------------------------------getMaxAppByStatus

?>



<?php 

	session_start();
	$backwardseperator = "../../../../";
	$mainPath 	= $_SESSION['mainPath'];
	$userId 	= $_SESSION['userId'];
	$location 	= $_SESSION['CompanyID'];
	$company 	= $_SESSION['headCompanyId'];

	include "{$backwardseperator}dataAccess/Connector.php";
	include "{$backwardseperator}libraries/mail/mail.php";
	require_once $backwardseperator."class/cls_mail.php";
	
	$objMail = new cls_create_mail($db);
	
	$programName='New Item Allocation';
	$programCode='P0449';
	$issueApproveLevel = (int)getApproveLevel($programName);
	
	/////////// parameters /////////////////////////////

	
	$allocationNo = $_REQUEST['allocationNo'];
	$year = $_REQUEST['year'];
	
	if($_REQUEST['status']=='approve')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		$rollBackFlag=0;
		$rollBackMsg = '';
		
		$sql = "UPDATE `ware_itemcreationheader` SET `intStatus`=intStatus-1 WHERE (intAllocationNo='$allocationNo') AND (`intAllocationYear`='$year') and intStatus not in(0,1)";
		$result = $db->RunQuery2($sql);
		if((!$result) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sql;
			$rollBackMsg = "Approval error!";
		}
		
		if($result){
			$sql = "SELECT ware_itemcreationheader.intUser,ware_itemcreationheader.intStatus, ware_itemcreationheader.intApproveLevels,
					ware_itemcreationheader.intCompanyId as location,
					mst_locations.intCompanyId as companyId 
					FROM
					ware_itemcreationheader
					Inner Join mst_locations ON ware_itemcreationheader.intCompanyId = mst_locations.intId WHERE (intAllocationNo='$allocationNo') AND (`intAllocationYear`='$year')";
			$results = $db->RunQuery2($sql);
			$row = mysqli_fetch_array($results);
			$createdUser = $row['intUser'];
			$status = $row['intStatus'];
			$savedLevels = $row['intApproveLevels'];
			$location 	= $row['location'];
			$company = $row['companyId'];
			
			$approveLevel=$savedLevels+1-$status;
			$sqlI = "INSERT INTO `ware_itemcreationheader_approvedby` (`intAllocationNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
				VALUES ('$allocationNo','$year','$approveLevel','$userId',now(),0)";
			$resultI = $db->RunQuery2($sqlI);
			if((!$resultI) && ($rollBackFlag!=1)){
				$rollBackFlag=1;
				$sqlM=$sqlI;
				$rollBackMsg = "Approval error!";
			}
			
			if($status==1)
			{
				if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
				////////////// send mail to creater //////////////////
				sendFinlConfirmedEmailToUser($allocationNo,$year,$objMail,$mainPath,$root_path);
				}
			}
		//--------------------------------------------------------------------
		if($status==1){//if final confirmation raised,insert to stock
				$sql1 = "SELECT 
 						ware_itemcreationdetails.dblQty,
						ware_itemcreationdetails.intMainItemId,   
						ware_itemcreationdetails.intSubItemId,   
						mst_item.intBomItem 
						FROM 
						ware_itemcreationdetails 
						Inner Join mst_item ON ware_itemcreationdetails.intSubItemId = mst_item.intId
						WHERE
						ware_itemcreationdetails.intAllocationNo =  '$allocationNo' AND
						ware_itemcreationdetails.intAllocationYear =  '$year' 
						ORDER BY intMainItemId ASC";
				$result1 = $db->RunQuery2($sql1);
				$tmpMainItem='';
				$toSavedQty=0;
				$transSavedQty=0;
				while($row=mysqli_fetch_array($result1))
				{
					$toSavedQty+=round($row['dblQty'],4);
					//$location 	= $_SESSION['CompanyID'];
					$mainItem=$row['intMainItemId'];
					$subItem=$row['intSubItemId'];
					$Qty=$row['dblQty'];
					
						$resultG = getGrnWiseStockBalance_bulk($location,$subItem);
						while($rowG=mysqli_fetch_array($resultG)){//save grn wise items
								if(($Qty>0) && ($rowG['stockBal']>0)){
									if($Qty<=$rowG['stockBal']){
									$saveQty=$Qty;
									$Qty=0;
									}
									else if($Qty>$rowG['stockBal']){
									$saveQty=$rowG['stockBal'];
									$Qty=$Qty-$saveQty;
									}
									$grnNo=$rowG['intGRNNo'];
									$grnYear=$rowG['intGRNYear'];
									$grnDate=$rowG['dtGRNDate'];
									$grnRate=$rowG['dblGRNRate'];	
									$currency=loadCurrency($grnNo,$grnYear,$subItem);
									$saveQty=round($saveQty,4);	
									if($saveQty>0){
									$sqlI = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intUser,dtDate) 
										VALUES ('$company','$location','$allocationNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$subItem','-$saveQty','ITEMCREATION-','$userId',now())";
									$resultI = $db->RunQuery2($sqlI);
									if($resultI){
										$transSavedQty+=$saveQty;
									}
									if((!$resultI) && ($rollBackFlag!=1)){
										$rollBackFlag=1;
										$sqlM=$sqlI;
										$rollBackMsg = "Approval error!";
									}
									
									}
									
									if($tmpMainItem!=$mainItem){
										//echo $tmpMainItem.":".$mainItem."</br>";
										
										$rowN=loadNewItemQty($allocationNo,$year,$mainItem);
										$grnRateNew=$rowN['dblLastPrice'];
										$currenNew=$rowN['intCurrency'];
										$itmQtyNew=$rowN['dblQty'];
										if($grnRateNew==''){
											$grnRateNew=0;
										}
										if($currenNew==''){
											$currenNew=0;
										}
										
										$sqlI = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intUser,dtDate) 
											VALUES ('$company','$location','$allocationNo','$year','0','0',now(),'$grnRateNew','$currenNew','$mainItem','$itmQtyNew','ITEMCREATION+','$userId',now())";
										$resultI = $db->RunQuery2($sqlI);
										if((!$resultI) && ($rollBackFlag!=1)){
											$rollBackFlag=1;
											$sqlM=$sqlI;
											$rollBackMsg = "Approval error!";
										}
										$tmpMainItem=$row['intMainItemId'];
									}
								}
					}
				}
			}
		//--------------------------------------------------------------------
			if($status==$savedLevels){//if first confirmation raised,requisition table shld update
			}
		//---------------------------------------------------------------------------
		}
	
		$toSavedQty=round($toSavedQty,4);
		$transSavedQty=round($transSavedQty,4);
		
		if(($toSavedQty!=$transSavedQty) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$rollBackMsg = "Approval Error";
			$sqlM=$toSavedQty."-".$transSavedQty;
		}
		
		if(($rollBackFlag==0) &&($status==1)){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= "Final Approval raised sucessfully";
			$response['q'] 			= $sql;
		}
		else if($rollBackFlag==0){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= "Approval raised sucessfully";
			$response['q'] 			= '';
		}
		else if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= $sqlM;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sqlM;
		}
		$db->CloseConnection();		
		
		//----------------------------------------
			echo json_encode($response);
		//-----------------------------------------	
	}
	else if($_REQUEST['status']=='reject')
	{
		$db->OpenConnection();
		$db->RunQuery2('Begin');
		
		$rollBackFlag=0;
		$rollBackMsg = '';
		
		//$grnApproveLevel = (int)getApproveLevel($programName);
		$sql = "SELECT ware_itemcreationheader.intUser,ware_itemcreationheader.intStatus,ware_itemcreationheader.intApproveLevels FROM ware_itemcreationheader WHERE  (intAllocationNo='$allocationNo') AND (`intAllocationYear`='$year')";
		$results = $db->RunQuery2($sql);
		$row = mysqli_fetch_array($results);
		$createdUser = $row['intUser'];
		$status = $row['intStatus'];
		$savedLevels = $row['intApproveLevels'];
		
		$sql = "UPDATE `ware_itemcreationheader` SET `intStatus`=0 WHERE (intAllocationNo='$allocationNo') AND (`intAllocationYear`='$year')";
		$result = $db->RunQuery2($sql);
		if((!$result) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sql;
			$rollBackMsg = "Rejection error!";
		}

		//$sql = "DELETE FROM `ware_itemcreationheader_approvedby` WHERE (`intAllocationNo`='$allocationNo') AND (`intYear`='$year')";
		//$result2 = $db->RunQuery2($sql);
		
		$sqlI = "INSERT INTO `ware_itemcreationheader_approvedby` (`intAllocationNo`,`intYear`,`intApproveLevelNo`,intApproveUser,dtApprovedDate,intStatus) 
			VALUES ('$allocationNo','$year','0','$userId',now(),0)";
		$resultI = $db->RunQuery2($sqlI);
		if((!$resultI) && ($rollBackFlag!=1)){
			$rollBackFlag=1;
			$sqlM=$sqlI;
			$rollBackMsg = "Rejection error!";
		}
		
		if($resultI)
		{
			if($createdUser!=$userId){//if confiremed user and created user same, email won't be generate.
			sendRejecedEmailToUser($allocationNo,$year,$objMail,$mainPath,$root_path);
			}
		}
		//---------------------------------------------------------------------------------
		if($status==1){//if final confirmation raised,insert to stock
				$sql1 = "SELECT 
 						ware_itemcreationdetails.dblQty,
						ware_itemcreationdetails.intMainItemId,   
						ware_itemcreationdetails.intSubItemId,   
						mst_item.intBomItem 
						FROM 
						ware_itemcreationdetails 
						Inner Join mst_item ON ware_itemcreationdetails.intSubItemId = mst_item.intId
						WHERE
						ware_itemcreationdetails.intAllocationNo =  '$allocationNo' AND
						ware_itemcreationdetails.intAllocationYear =  '$year' 
						ORDER BY intMainItemId ASC";
				$result1 = $db->RunQuery2($sql1);
				$tmpMainItem='';
				while($row=mysqli_fetch_array($result1))
				{
					//$location 	= $_SESSION['CompanyID'];
					$mainItem=$row['intMainItemId'];
					$subItem=$row['intSubItemId'];
					$Qty=$row['dblQty'];
					
						$resultG = getGrnWiseStockBalance_bulk($location,$subItem);
						while($rowG=mysqli_fetch_array($resultG)){//save grn wise items
								if(($Qty>0) && ($rowG['stockBal']>=$Qty)){
									if($Qty<=$rowG['stockBal']){
									$saveQty=$Qty;
									$Qty=0;
									}
									else if($Qty>$rowG['stockBal']){
									$saveQty=$rowG['stockBal'];
									$Qty=$Qty-$saveQty;
									}
									$grnNo=$rowG['intGRNNo'];
									$grnYear=$rowG['intGRNYear'];
									$grnDate=$rowG['dtGRNDate'];
									$grnRate=$rowG['dblGRNRate'];	
									$currency=loadCurrency($grnNo,$grnYear,$subItem);
									
									$sqlI = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intUser,dtDate) 
										VALUES ('$company','$location','$allocationNo','$year','$grnNo','$grnYear','$grnDate','$grnRate','$currency','$subItem','$saveQty','CITEMCREATION-','$userId',now())";
									$resultI = $db->RunQuery2($sqlI);
									if((!$resultI) && ($rollBackFlag!=1)){
										$rollBackFlag=1;
										$sqlM=$sqlI;
										$rollBackMsg = "Rejection error!";
									}
									
									
									if($tmpMainItem!=$row['intMainItemId']){
										
										$rowN=loadNewItemQty($allocationNo,$year,$mainItem);
										$grnRateNew=$rowN['dblLastPrice'];
										$currenNew=$rowN['intCurrency'];
										$itmQtyNew=$rowN['dblQty'];
										
										$sqlI = "INSERT INTO `ware_stocktransactions_bulk` (`intCompanyId`,`intLocationId`,`intDocumentNo`,intDocumntYear,intGRNNo,intGRNYear,dtGRNDate,dblGRNRate,intCurrencyId,intItemId,dblQty,strType,intUser,dtDate) 
											VALUES ('$company','$location','$allocationNo','$year','0','0',now(),'$grnRateNew','$currenNew','$mainItem','-$itmQtyNew','CITEMCREATION+','$userId',now())";
										$resultI = $db->RunQuery2($sqlI);
										if((!$resultI) && ($rollBackFlag!=1)){
											$rollBackFlag=1;
											$sqlM=$sqlI;
											$rollBackMsg = "Rejection error!";
										}
									}
								}
								$tmpMainItem=$row['intMainItemId'];
					}
				}
			}
		//---------------------------------------------------------------------------------
			if($savedLevels>=$status){//if atleast one confirmation processed, roll back requisition table-
			}

		//------------------------------------
		if($rollBackFlag==0){
			$db->RunQuery2('Commit');
			$response['type'] 		= 'pass';
			$response['msg'] 		= "Rejection raised sucessfully";
			$response['q'] 			= '';
		}
		else if($rollBackFlag==1){
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $rollBackMsg;
			$response['q'] 			= $sqlM;
		}
		else{
			$db->RunQuery2('Rollback');
			$response['type'] 		= 'fail';
			$response['msg'] 		= $db->errormsg;
			$response['q'] 			= $sqlM;
		}
		$db->CloseConnection();		
		
		//----------------------------------------
			echo json_encode($response);
		//-----------------------------------------	
		
	}
//--------------------------------------------------------------
function getGrnWiseStockBalance_bulk($location,$item)
{
	global $db;
	  $sql = "SELECT
			Sum(ware_stocktransactions_bulk.dblQty) AS stockBal,
			ware_stocktransactions_bulk.intGRNNo,
			ware_stocktransactions_bulk.intGRNYear,
			ware_stocktransactions_bulk.dtGRNDate,
			ware_stocktransactions_bulk.dblGRNRate
			FROM ware_stocktransactions_bulk
			WHERE
			ware_stocktransactions_bulk.intItemId =  '$item' AND
			ware_stocktransactions_bulk.intLocationId =  '$location'
			GROUP BY
			ware_stocktransactions_bulk.intItemId,
			ware_stocktransactions_bulk.intGRNNo,
			ware_stocktransactions_bulk.intGRNYear
			ORDER BY
			ware_stocktransactions_bulk.dtGRNDate ASC";

	$result = $db->RunQuery2($sql);
	return $result;	
}
//--------------------------------------------------------------
function loadCurrency($grnNo,$grnYear,$item)
{
	global $db;
	
	if(($grnNo!=0) && ($grnYear!=0)){
	  $sql = "SELECT
			trn_poheader.intCurrency as currency 
			FROM
			ware_grnheader
			Inner Join trn_poheader ON ware_grnheader.intPoNo = trn_poheader.intPONo AND ware_grnheader.intPoYear = trn_poheader.intPOYear
			WHERE
			ware_grnheader.intGrnNo =  '$grnNo' AND
			ware_grnheader.intGrnYear =  '$grnYear'";
	}
	else{
	$sql = "SELECT
			mst_item.intCurrency as currency 
			FROM mst_item
			WHERE
			mst_item.intId =  '$item'";
	}

	$result = $db->RunQuery2($sql);
	$row=mysqli_fetch_array($result);	
	return $row['currency'];;	
}
//--------------------------------------------------------------
function loadNewItemQty($allocationNo,$year,$mainItem)
{
	global $db;
	  $sql = "SELECT
			ware_itemcreationdetails_main.dblQty, 
			mst_item.dblLastPrice,
			mst_item.intCurrency 
			FROM ware_itemcreationdetails_main 
			Inner Join mst_item ON ware_itemcreationdetails_main.intMainItemId = mst_item.intId 
			WHERE
			ware_itemcreationdetails_main.intAllocationNo =  '$allocationNo' AND
			ware_itemcreationdetails_main.intAllocationYear =  '$year' AND
			ware_itemcreationdetails_main.intMainItemId =  '$mainItem'";

	$result = $db->RunQuery2($sql);
	$row=mysqli_fetch_array($result);	
	return $row;	
}
//--------------------------------------------------------
function sendFinlConfirmedEmailToUser($serialNo,$year,$objMail,$mainPat,$root_path){
	global $db;
				
		    $sql = "SELECT
			sys_users.strFullName,
			sys_users.strEmail 
			FROM 
			ware_itemcreationheader
			Inner Join sys_users ON ware_itemcreationheader.intUser = sys_users.intUserId
			WHERE
			ware_itemcreationheader.intAllocationNo =  '$serialNo' AND
			ware_itemcreationheader.intAllocationYear =  '$year'";

				
		 $result = $db->RunQuery2($sql);
		 
		$row=mysqli_fetch_array($result);
			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="CONFIRMED NEW ITEM ALLOCATION ('$serialNo'/'$year')"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST = NULL;
			
			$_REQUEST['program']='NEW ITEM ALLOCATION';
			$_REQUEST['field1']='Allocation No';
			$_REQUEST['field2']='Allocation Year';
			$_REQUEST['field3']='';
			$_REQUEST['field4']='';
			$_REQUEST['field5']='';
			$_REQUEST['value1']=$serialNo;
			$_REQUEST['value2']=$year;
			$_REQUEST['value3']='';
			$_REQUEST['value4']='';
			$_REQUEST['value5']='';
			
			$_REQUEST['subject']="CONFIRMED NEW ITEM ALLOCATION ('$serialNo'/'$year')";
			
			$_REQUEST['statement1']="Approved this";
			$_REQUEST['statement2']="to view this";
			$_REQUEST['link']=base64_encode($mainPath."presentation/warehouse/newItemAllowcation/listing/rptNewItemAllocation.php?allocationNo=$serialNo&year=$year&approveMode=0"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}
//--------------------------------------------------------
function sendRejecedEmailToUser($serialNo,$year,$objMail,$mainPat,$root_path){
	global $db;
				
		    $sql = "SELECT
			sys_users.strFullName,
			sys_users.strEmail 
			FROM 
			ware_itemcreationheader
			Inner Join sys_users ON ware_itemcreationheader.intUser = sys_users.intUserId
			WHERE
			ware_itemcreationheader.intAllocationNo =  '$serialNo' AND
			ware_itemcreationheader.intAllocationYear =  '$year'";

				
		 $result = $db->RunQuery2($sql);
		 
		$row=mysqli_fetch_array($result);
			$enterUserName = $row['strFullName'];
			$enterUserEmail = $row['strEmail'];
			 
			$header="REJECTED NEW ITEM ALLOCATION('$serialNo'/'$year')"; 
			 
			//send mails ////
			$_REQUEST = NULL;
			$_REQUEST = NULL;
			
			$_REQUEST['program']='NEW ITEM ALLOCATION';
			$_REQUEST['field1']='Allocation No';
			$_REQUEST['field2']='Allocation Year';
			$_REQUEST['field3']='';
			$_REQUEST['field4']='';
			$_REQUEST['field5']='';
			$_REQUEST['value1']=$serialNo;
			$_REQUEST['value2']=$year;
			$_REQUEST['value3']='';
			$_REQUEST['value4']='';
			$_REQUEST['value5']='';
			
			$_REQUEST['subject']="REJECTED NEW ITEM ALLOCATION ('$serialNo'/'$year')";
			
			$_REQUEST['statement1']="Rejected this";
			$_REQUEST['statement2']="to view this";
			$_REQUEST['link']=base64_encode($mainPath."presentation/warehouse/newItemAllowcation/listing/rptNewItemAllocation.php?allocationNo=$serialNo&year=$year&approveMode=0"); 
			//$path=$mainPath.'presentation/mail_approval_template.php';
			$path=$root_path."presentation/mail_approval_template.php";
			$objMail->send_Response_Mail2($path,$_REQUEST,$_SESSION["systemUserName"],$_SESSION["email"],$header,$enterUserEmail,$enterUserName);

}
//--------------------------------------------------------
?>
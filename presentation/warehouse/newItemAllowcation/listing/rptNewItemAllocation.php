<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php

  $companyId = $_SESSION['CompanyID'];
$intUser  = $_SESSION["userId"];
$mainPath = $_SESSION['mainPath'];
$thisFilePath =  $_SERVER['PHP_SELF'];
 
$allocationNo = $_REQUEST['allocationNo'];
$year = $_REQUEST['year'];
$approveMode = $_REQUEST['approveMode'];

$programName='New Item Allocation';
$programCode='P0449';
 
      $sql = "SELECT 
ware_itemcreationheader.strNote,
ware_itemcreationheader.intStatus,
ware_itemcreationheader.intApproveLevels,
ware_itemcreationheader.intUser,
ware_itemcreationheader.dtmCreateDate,
mst_locations.strName,
ware_itemcreationheader.intCompanyId as allocationRaisedLocationId,
sys_users.strUserName
FROM
ware_itemcreationheader  
Inner Join mst_locations ON ware_itemcreationheader.intCompanyId = mst_locations.intId
Inner Join sys_users ON ware_itemcreationheader.intUser = sys_users.intUserId
WHERE
ware_itemcreationheader.intAllocationNo =  '$allocationNo' AND
ware_itemcreationheader.intAllocationYear =  '$year' 
GROUP BY
ware_itemcreationheader.intAllocationNo,
ware_itemcreationheader.intAllocationYear 
";
				 $result = $db->RunQuery($sql);
				 while($row=mysqli_fetch_array($result))
				 {
					$locationId = $row['allocationRaisedLocationId'];//this locationId use in report header(reportHeader.php)--------------------
					$note=$row['strNote'];
					$allocDate = $row['dtmCreateDate'];
					$allocBy = $row['strUserName'];
					$intStatus = $row['intStatus'];
					$savedLevels = $row['intApproveLevels'];
					$user = $row['strUserName'];
					$createUserId= $row['intUser'];
				 }
				 
$confirmationMode=loadConfirmatonMode($programCode,$intStatus,$savedLevels,$intUser);
$rejectionMode=loadRejectionMode($programCode,$intStatus,$savedLevels,$intUser);
?>
 <head>
 <title>New Item Allocation Report</title>
 <script type="text/javascript" src="presentation/warehouse/newItemAllowcation/listing/rptNewItemAllocation-js.js"></script>
 <style>
.break { page-break-before: always; }

@media print {
.noPrint 
{
    display:none;
}
}
#apDiv1 {
	position:absolute;
	left:280px;
	top:172px;
	width:650px;
	height:322px;
	z-index:1;
}
.APPROVE {
	font-size: 18px;
	font-weight: bold;
}
</style>
</head>

<body>
<?php
 if($intStatus>1)//pending
{
?>
<div id="apDiv1"><img src="images/pending.png"  /></div>
<?php
}
?>
<form id="frmItemAllocationReport" name="frmItemAllocationReport" method="post">
<table width="100%" cellpadding="0" cellspacing="0">
<tr>
<td colspan="3"></td>
</tr>
<tr>
<td width="20%"></td>
<td width="60%" height="80" valign="top"><?php include 'reportHeader.php'?></td>
<td width="20%"></td>
</tr>

<tr>
<td colspan="3"></td>
</tr>
</table>
<div align="center">
<div style="background-color:#FFF" ><strong>NEW ITEM ALLOCATION REPORT</strong><strong></strong></div>
<table width="900" border="0" align="center" bgcolor="#FFFFFF">
<tr>
  <td>
  <table width="100%">
  <tr>
    <td colspan="9" align="center" bgcolor="#FFDFCB">
    <?php
	if($intStatus>1)
	{
	?>
    <?php if($approveMode==1){
			if($confirmationMode==1) { 
			?>
			<img src="images/approve.png" align="middle" class="mouseover noPrint" id="imgApprove" />
			<?php
			}
			if($rejectionMode==1) { 
			?>
            <img src="images/reject.png" align="middle" class="mouseover noPrint" id="imgReject" />
                <?php
                }
            }
	}
	?>
    </td>
  </tr>
  <tr>
  <?php
 	if($intStatus==1)
	{
	?>
   <td colspan="9" class="APPROVE" >CONFIRMED</td>
   <?PHP
	}
	else if($intStatus==0)
	{
   ?>
   <td colspan="9" class="APPROVE" style="color:#F00">REJECTED</td>
   <?php
	}
	else
	{
   ?>
   <td width="10%" colspan="9" class="APPROVE">PENDING</td>
   <?php
	}
   ?>
  </tr>
  <tr>
    <td width="1%">&nbsp;</td>
    <td width="11%"><span class="normalfnt"><strong>Allocation No</strong></span></td>
    <td width="1%" align="center" valign="middle"><strong>:</strong></td>
    <td width="26%"><span class="normalfnt"><?php echo $allocationNo ?>/<?php echo $year ?></span></td>
    <td width="9%" class="normalfnt">&nbsp;</td>
    <td width="1%" align="center" valign="middle">&nbsp;</td>
    <td width="28%">&nbsp;</td>
    <td width="1%"><div id="divAllocNo" style="display:none"><?php echo $allocationNo ?>/<?php echo $year ?></div></td>
  <td width="3%"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Allocation  By</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $allocBy  ?></span></td>
    <td>&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt"><strong>Date</strong></td>
    <td align="center" valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $allocDate  ?></span></td>
    <td class="normalfnt"><strong>Note</strong></td>
    <td align="center"  valign="middle"><strong>:</strong></td>
    <td><span class="normalfnt"><?php echo $note  ?></span></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  
  
  <tr>
    <td>&nbsp;</td>
    <td class="normalfnt">&nbsp;</td>
    <td align="center" valign="middle">&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
  </tr>
  </table>
  </td>
</tr>
<tr>
  <td>
    <table width="100%">
      <tr>
        <td width="5%">&nbsp;</td>
        <td colspan="7" class="normalfnt">
          <table width="100%" class="bordered" id="tblMainGrid" cellspacing="0" cellpadding="0">
            <tr >
              <th width="20%" height="14" >Main Category</th>
              <th width="21%" >Sub Category</th>
              <th width="14%" >Item Code</th>
              <th width="30%" >Item</th>
              <th width="7%" >UOM</th>
              <th width="8%" >Qty</th>
              </tr>
              <?php 
	  	    $sql1 = "SELECT
					ware_itemcreationdetails_main.intMainItemId,
					mst_item.strName,
					mst_item.intMainCategory,
					mst_maincategory.strName as mainCategory,
					mst_item.intSubCategory,
					mst_subcategory.strName as subCategory,
					mst_item.strCode,
					ware_itemcreationdetails_main.dblQty,
					mst_item.intUOM,
					mst_units.strCode as uom 
					FROM
					ware_itemcreationdetails_main
					Inner Join mst_item ON ware_itemcreationdetails_main.intMainItemId = mst_item.intId
					Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
					Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
					Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
					WHERE
					ware_itemcreationdetails_main.intAllocationNo =  '$allocationNo' AND
					ware_itemcreationdetails_main.intAllocationYear =  '$year' 
					order by mst_item.strName asc
					";
			$result1 = $db->RunQuery($sql1);
			$totQty=0;
			$totAmmount=0;
			while($row=mysqli_fetch_array($result1))
			{
				$mainItem=$row['intMainItemId'];
	  ?>
            <tr class="normalfnt"  bgcolor="#FFD7AE">
              <td class="normalfnt" >&nbsp;<?php echo $row['mainCategory']?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['subCategory'] ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['strCode'] ?>&nbsp;</td>
              <td class="normalfnt" >&nbsp;<?php echo $row['strName'] ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['uom'] ?>&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php echo $row['dblQty'] ?>&nbsp;</td>
              </tr>
					  <?php //sub items qty
	  	    $sql2 = "SELECT
					ware_itemcreationdetails.intSubItemId,
					mst_item.strName,
					mst_item.intMainCategory,
					mst_maincategory.strName as mainCategory,
					mst_item.intSubCategory,
					mst_subcategory.strName as subCategory,
					mst_item.strCode,
                    mst_item.strCode as SUP_ITEM_CODE,
					ware_itemcreationdetails.dblQty, 
					mst_item.intUOM,
					mst_units.strCode as uom 
					FROM
					ware_itemcreationdetails
					Inner Join mst_item ON ware_itemcreationdetails.intSubItemId = mst_item.intId
					Inner Join mst_maincategory ON mst_item.intMainCategory = mst_maincategory.intId
					Inner Join mst_subcategory ON mst_item.intSubCategory = mst_subcategory.intId 
					Inner Join mst_units ON mst_item.intUOM = mst_units.intId 
                                        LEFT JOIN mst_supplier ON mst_item.intPerfectSupplier = mst_supplier.intId
					WHERE
					ware_itemcreationdetails.intAllocationNo =  '$allocationNo' AND
					ware_itemcreationdetails.intAllocationYear =  '$year' AND  
					ware_itemcreationdetails.intMainItemId =  '$mainItem' 
					order by mst_item.strName asc  
					";
                            $result2 = $db->RunQuery($sql2);
                            $totQty=0;
                            $totAmmount=0;
                            while($row2=mysqli_fetch_array($result2))
                            {
                      ?>
                            <tr class="normalfnt"  bgcolor="#FFFFFF">
                              <td class="normalfnt" >&nbsp;<?php echo $row2['mainCategory']?>&nbsp;</td>
                              <td class="normalfnt" >&nbsp;<?php echo $row2['subCategory'] ?>&nbsp;</td>
                              <td class="normalfnt" >&nbsp;<?php if($row2['SUP_ITEM_CODE']!=null) echo $row2['SUP_ITEM_CODE']; else echo $row2['strCode'] ?>&nbsp;</td>
                              <td class="normalfnt" >&nbsp;<?php echo $row2['strName'] ?>&nbsp;</td>
                              <td class="normalfntRight" >&nbsp;<?php echo $row2['uom'] ?>&nbsp;</td>
                              <td class="normalfntRight" >&nbsp;<?php echo $row2['dblQty'] ?>&nbsp;</td>
                              </tr>
                      <?php 
                            }
			}
	  ?>
            <tr class="normalfnt"  bgcolor="#CCCCCC">
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfnt" >&nbsp;</td>
              <td class="normalfntRight" >&nbsp;<?php //echo $totQty ?>&nbsp;</td>
              </tr>
            </table>
          </td>
        <td width="6%">&nbsp;</td>
        </tr>
      
      </table>
    </td>
</tr>
<?php  
					   $flag=0;
					   $sqlc = "SELECT
							ware_itemcreationheader_approvedby.intApproveUser,
							ware_itemcreationheader_approvedby.dtApprovedDate,
							sys_users.strUserName as UserName, 
							ware_itemcreationheader_approvedby.intApproveLevelNo
							FROM
							ware_itemcreationheader_approvedby
							Inner Join sys_users ON ware_itemcreationheader_approvedby.intApproveUser = sys_users.intUserId
							WHERE
							ware_itemcreationheader_approvedby.intAllocationNo =  '$allocationNo' AND
							ware_itemcreationheader_approvedby.intYear =  '$year' order by dtApprovedDate asc
";
					 $resultc = $db->RunQuery($sqlc);
					while($rowc=mysqli_fetch_array($resultc)){
						if($rowc['intApproveLevelNo']==1)
						$desc="1st ";
						else if($rowc['intApproveLevelNo']==2)
						$desc="2nd ";
						else if($rowc['intApproveLevelNo']==3)
						$desc="3rd ";
						else
						$desc=$rowc['intApproveLevelNo']."th ";
					 //  $desc=$ap.$desc;
					 $desc2=$rowc['UserName']."(".$rowc['dtApprovedDate'].")";
					 if($rowc['UserName']=='')
					 $desc2='---------------------------------';
					 
					 if($rowc['intApproveLevelNo']>0){
						 $flag=1;
				?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> Approved By - </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
            </tr>
<?php
	//}
	//else{			
					}
					 if($rowc['intApproveLevelNo']==0){
						 $flag=2;
					  ?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong> Rejected By - </strong></span><span class="normalfnt"><?php echo $rowc['UserName']."(".$rowc['dtApprovedDate'].")";?></span></td>
            </tr>
<?php
					 }
					 if($rowc['intApproveLevelNo']==-1){
						 $flag=3;
					  ?>
            <tr>
                <td bgcolor="#FFFFFF"><span class="normalfnt"><strong> Revised By - </strong></span><span class="normalfnt"><?php echo $rowc['UserName']."(".$rowc['dtApprovedDate'].")";?></span></td>
            </tr>
<?php
					 }
					 
		 if($rowc['intApproveLevelNo']>0){
			 $finalSavedLevel=$rowc['intApproveLevelNo'];
		 }
	 }//end of while
	
	
	//echo $flag;
	if($flag>1){
		$finalSavedLevel=0;
	}
	
	if(($flag<=1) || ($intStatus>0)){
	for($j=$finalSavedLevel+1; $j<=$savedLevels; $j++)
	{ 
		if($j==1)
		$desc="1st ";
		else if($j==2)
		$desc="2nd ";
		else if($j==3)
		$desc="3rd ";
		else
		$desc=$j."th ";
		$desc2='---------------------------------';
	?>
        <tr>
            <td bgcolor="#FFFFFF"><span class="normalfnt"><strong><?php echo $desc; ?> Approved By - </strong></span><span class="normalfnt"><?php echo $desc2;?></span></td>
        </tr>
<?php
	}
	}
?>
<tr height="90" >
  <td align="center" class="normalfntMid"></td>
</tr>
<tr height="40">
<?php
	//$locationId = '';
	$sql = "SELECT mst_locations.intCompanyId FROM mst_locations WHERE mst_locations.intId =  '$locationId'";
	$result 	= $db->RunQuery($sql);
	$row		= mysqli_fetch_array($result);
	$createCompanyId = $row['intCompanyId'];
?>
<?Php 
	$url  = "{$backwardseperator}presentation/sendToApproval.php";		// * file name
	$url .= "?status=$intStatus";										// * set recent status
	$url .= "&approveLevels=$savedLevels";								// * set approve levels in order header
	$url .= "&programCode=$programCode";								// * program code (ex:P10001)
	$url .= "&program=NEW ITEM ALLOCATION";									// * program name (ex:Purchase Order)
	$url .= "&companyId=$createCompanyId";									// * created company id
	$url .= "&createUserId=$createUserId";	
	
	$url .= "&field1=Allocation No";												 
	$url .= "&field2=Allocation Year";	
	$url .= "&value1=$allocationNo";												 
	$url .= "&value2=$year";	
	
	$url .= "&subject=NEW ITEM ALLOCATION FOR APPROVAL ('$allocationNo'/'$year')";	
	
	$url .= "&statement1=Please Approve this";	
	$url .= "&statement2=to Approve this";	
							// * doc year
	$url .= "&link=".urlencode(base64_encode($mainPath."presentation/warehouse/newItemAllowcation/listing/rptNewItemAllocation.php?allocationNo=$allocationNo&year=$year&approveMode=1"));
?>
  <td align="center" class="normalfntMid"><iframe id="iframeFiles2" src="<?php echo $url;?>" name="iframeFiles" style="width:500px;height:150px;border:none"  ></iframe></td>
</tr>
<tr height="40">
  <td align="center" class="normalfntMid"><span class="normalfntMid"><strong>Printed Date: <?php echo date("Y/m/d") ?></strong></span></td>
</tr>
</table>
</div>        
</form>
</body>
</html>
<?php
//------------------------------function load loadEditMode---------------------
function loadEditMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	//echo $savedStat;
	$editMode=0;
	$sqlp = "SELECT
		menupermision.intEdit  
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
		 
		 if($rowp['intEdit']==1){
			 if($intStatus==($savedStat+1) || ($intStatus==0)){ 
				 $editMode=1;
			 }
		 }
			 
	return $editMode;
}
//------------------------------function loadConfirmatonMode-------------------
function loadConfirmatonMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$confirmatonMode=0;
	$k=$savedStat+2-$intStatus;
	$sqlp = "SELECT
		menupermision.int".$k."Approval 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['int'.$k.'Approval']==1){
	 if($intStatus!=1){
	 $confirmatonMode=1;
	 }
	}
	 
	return $confirmatonMode;
}
	
//------------------------------function loadRejectionMode-------------------
function loadRejectionMode($programCode,$intStatus,$savedStat,$intUser){
	global $db;
	
	$rejectMode=0;
	$sqlp = "SELECT
		menupermision.intReject 
		FROM menupermision 
		Inner Join menus ON menupermision.intMenuId = menus.intId
		WHERE
		menus.strCode =  '$programCode' AND
		menupermision.intUserId =  '$intUser'";	
					
	$resultp = $db->RunQuery($sqlp);
	$rowp=mysqli_fetch_array($resultp);
	
	if($rowp['intReject']==1){
	 if($intStatus!=0){
	 $rejectMode=1;
	 }
	}
	 
	return $rejectMode;
}
//-------------------------------------------------------------------------
?>	

<?php (define('UNLOCKPAGE',true)?die('<<< Access denied >>>'):'');?>
<?php
date_default_timezone_set('Asia/Kolkata');
//ini_set('allow_url_include',1);
// ini_set('display_errors',1);
 $thisFilePath 		=  $_SERVER['PHP_SELF'];

require_once	"libraries/jqgrid2/inc/jqgrid_dist.php";

$company 	= $_SESSION['headCompanyId'];
$location = $_SESSION['CompanyID'];
 
$programName='New Item Allocation';
$programCode='P0449';

$intUser  = $_SESSION["userId"];

$reportMenuId	='912';
$menuId			='450';

$userDepartment=getUserDepartment($intUser);
$approveLevel = (int)getMaxApproveLevel();

################################################################
################## new part ####################################
################################################################
///// create where part ///////////////////
$arr =  json_decode($_REQUEST['filters'],true);
//echo $arr['rules'][0]['field'];
$arr = $arr['rules'];
//print_r($arr);
$where_string = '';
$where_array = array(
				'Status'=>'tb1.intStatus',
				'Allocation_No'=>'tb1.intAllocationNo',
				'Allocation_Year'=>'tb1.intAllocationYear',
				'Date'=>'tb1.dtmCreateDate',
				'User'=>'sys_users.strUserName'
				);
$arr_status = array('Approved'=>'1','Rejected'=>'0','Pending'=>'2');
foreach($arr as $k=>$v)
{
	if($v['field']=='Status')
	{
		if($arr_status[$v['data']]==2)
			$where_string .= "AND  ".$where_array[$v['field']]." >1 ";
		else
			$where_string .= "AND  ".$where_array[$v['field']]." = '".$arr_status[$v['data']]."' ";
	}
	else if($where_array[$v['field']])
		$where_string .= "AND  ".$where_array[$v['field']]." like '%".$v['data']."%' ";
}
if(!count($arr)>0)					 
	$where_string .= "AND substr(tb1.dtmCreateDate,1,10) = '".date('Y-m-d')."'";
	
################## end code ####################################

$sql = "select * from(SELECT DISTINCT 
							if(tb1.intStatus=1,'Approved',if(tb1.intStatus=0,'Rejected','Pending')) as Status,
							tb1.intAllocationNo as `Allocation_No`,
							tb1.intAllocationYear as `Allocation_Year`,
							tb1.dtmCreateDate as `Date`,
							sys_users.strUserName as `User`,

							IFNULL((SELECT
								concat(sys_users.strUserName,'(',max(ware_itemcreationheader_approvedby.dtApprovedDate),')' )
								FROM
								ware_itemcreationheader_approvedby
								Inner Join sys_users ON ware_itemcreationheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_itemcreationheader_approvedby.intAllocationNo  = tb1.intAllocationNo AND
								ware_itemcreationheader_approvedby.intYear =  tb1.intAllocationYear AND
								ware_itemcreationheader_approvedby.intApproveLevelNo =  '1' AND
								ware_itemcreationheader_approvedby.intStatus =  '0'
							),IF(((SELECT
								menupermision.int1Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND tb1.intStatus>1),'Approve', '')) as `1st_Approval`,  ";
							
						for($i=2; $i<=$approveLevel; $i++){
							
							if($i==2){
							$approval="2nd_Approval";
							}
							else if($i==3){
							$approval="3rd_Approval";
							}
							else {
							$approval=$i."th_Approval";
							}
							
							
						$sql .= "IFNULL(
						/*condition*/
(
                                                        	SELECT
								concat(sys_users.strUserName,'(',max(ware_itemcreationheader_approvedby.dtApprovedDate),')' )
								FROM
								ware_itemcreationheader_approvedby
								Inner Join sys_users ON ware_itemcreationheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_itemcreationheader_approvedby.intAllocationNo  = tb1.intAllocationNo AND
								ware_itemcreationheader_approvedby.intYear =  tb1.intAllocationYear AND
								ware_itemcreationheader_approvedby.intApproveLevelNo =  '$i' AND
								ware_itemcreationheader_approvedby.intStatus =  '0'
							),
							/*end condition*/
							
							/*false part*/

							IF(
							/*if condition */
							((SELECT
								menupermision.int".$i."Approval 
								FROM menupermision 
								Inner Join menus ON menupermision.intMenuId = menus.intId
								WHERE
								menus.strCode = '$programCode' AND
								menupermision.intUserId =  '$intUser')=1 AND (tb1.intStatus>1) AND (tb1.intStatus<=tb1.intApproveLevels) AND ((SELECT
								max(ware_itemcreationheader_approvedby.dtApprovedDate) 
								FROM
								ware_itemcreationheader_approvedby
								Inner Join sys_users ON ware_itemcreationheader_approvedby.intApproveUser = sys_users.intUserId
								WHERE
								ware_itemcreationheader_approvedby.intAllocationNo  = tb1.intAllocationNo AND
								ware_itemcreationheader_approvedby.intYear =  tb1.intAllocationYear AND
								ware_itemcreationheader_approvedby.intApproveLevelNo =  ($i-1) AND
								ware_itemcreationheader_approvedby.intStatus =  '0')<>'')),
								
								/*end if condition*/
								/*if true part*/
								'Approve',
								/*fase part*/
								 if($i>tb1.intApproveLevels,'-----',''))
								
								/*end IFNULL false part*/
								) as `".$approval."`, "; 
									
								}
								
							$sql .= "'View' as `View`   
						FROM
							ware_itemcreationheader as tb1 
							Inner Join sys_users ON tb1.intUser = sys_users.intUserId 
							Inner Join mst_locations ON tb1.intCompanyId = mst_locations.intId 
							WHERE
							mst_locations.intCompanyId =  '$company' AND 
							tb1.intCompanyId =  '$location'
							$where_string
							)  as t where 1=1
						";
					  //	    echo $sql;
					  
$formLink			= "?q=$menuId&allocationNo={Allocation_No}&year={Allocation_Year}";	 
$reportLink  		= "?q=$reportMenuId&allocationNo={Allocation_No}&year={Allocation_Year}";
$reportLinkApprove  = "?q=$reportMenuId&allocationNo={Allocation_No}&year={Allocation_Year}&approveMode=1";
$reportLinkPrint  	= "?q=$reportMenuId&allocationNo={Allocation_No}&year={Allocation_Year}&mode=print";
						 
$col = array();

//STATUS
$col["title"] 	= "Status"; // caption of column
$col["name"] 	= "Status"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "3";
//edittype
$col["stype"] 	= "select";
$str = ":All;Pending:Pending;Approved:Approved;Rejected:Rejected" ;
$col["editoptions"] 	=  array("value"=> $str);
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$cols[] = $col;	$col=NULL;

//PO No
$col["title"] 	= "Allocation No"; // caption of column
$col["name"] 	= "Allocation_No"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] 	= "2";
//searchOper
$col["align"] 	= "center";
//$col["link"] = "http://localhost/?id={id}"; // e.g. http://domain.com?id={id} given that, there is a column with $col["name"] = "id" exist
$col['link']	= $formLink;	 
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag

 
$cols[] = $col;	$col=NULL;



//PO Year
$col["title"] = "Allocation Year"; // caption of column
$col["name"] = "Allocation_Year"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//Supplier
$col["title"] = "Date"; // caption of column
$col["name"] = "Date"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;


//User
$col["title"] = "User"; // caption of column
$col["name"] = "User"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "2";
$col["align"] = "center";
$cols[] = $col;	$col=NULL;



//FIRST APPROVAL
$col["title"] = "1st Approval"; // caption of column
$col["name"] = "1st_Approval"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

for($i=2; $i<=$approveLevel; $i++){
	if($i==2){
	$ap="2nd Approval";
	$ap1="2nd_Approval";
	}
	else if($i==3){
	$ap="3rd Approval";
	$ap1="3rd_Approval";
	}
	else {
	$ap=$i."th Approval";
	$ap1=$i."th_Approval";
	}
//SECOND APPROVAL
$col["title"] = $ap; // caption of column
$col["name"] = $ap1; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLinkApprove;
$col['linkName']	= 'Approve';
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;
}



//VIEW
$col["title"] = "Report"; // caption of column
$col["name"] = "View"; // grid column name, must be exactly same as returned column-name from sql (tablefield or field-alias) 
$col["width"] = "3";
$col["search"] = false;
$col["align"] = "center";
$col['link']	= $reportLink;
$col["linkoptions"] = "target='_blank'"; // extra params with <a> tag
$cols[] = $col;	$col=NULL;

$jq = new jqgrid('',$db);

$grid["caption"] 		= "New Item Allocation Listing";
$grid["multiselect"] 	= false;
// $grid["url"] = ""; // your paramterized URL -- defaults to REQUEST_URI
$grid["rowNum"] 		= 20; // by default 20
$grid["sortname"] 		= 'Allocation_No'; // by default sort grid by this field
$grid["sortorder"] 		= "DESC"; // ASC or DESC
$grid["autowidth"] 		= true; // expand grid to screen width
$grid["multiselect"] 	= false; // allow you to multi-select through checkboxes


// export XLS file
// export to excel parameters - range could be "all" or "filtered"
//$grid["export"] = array("format"=>"xlsx", "filename"=>"my-file", "sheetname"=>"test");


// export PDF file
// export to excel parameters
//$grid["export"] = array("format"=>"pdf", "filename"=>"my-file", "heading"=>"Invoice Details", "orientation"=>"landscape");

// export filtered data or all data
//$grid["export"]["range"] = "all"; // or "all" //filtered
$jq->set_options($grid);

$jq->select_command =$sql;
$jq->set_columns($cols);
$jq->set_actions(array(	
	"add"=>false, // allow/disallow add
	"edit"=>false, // allow/disallow edit
	"delete"=>false, // allow/disallow delete
	"rowactions"=>false, // show/hide row wise edit/del/save option
	"search" => "advance", // show single/multi field search condition (e.g. simple or advance)
	"export"=>true
) 
);




$out = $jq->render("list1");
?>
<title>New Item Allocation Listing</title>
 	<?php 
	//	include "include/listing.html";
	?>
 <form id="frmlisting" name="frmlisting" method="post" action="">

        <div align="center" style="margin:10px">        
			<?php echo $out?>
        </div>
</form>
 <?php

//------------------------------function load Default Department---------------------
function getMaxApproveLevel(){
	global $db;
	
	//echo $savedStat;
	$appLevel=0;
	$sqlp = "SELECT
			Max(ware_itemcreationheader.intApproveLevels) AS appLevel
			FROM ware_itemcreationheader";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['appLevel'];
}
//------------------------------function load User Department---------------------
function getUserDepartment($intUser){
	global $db;

	$sqlp = "SELECT
			sys_users.intDepartmentId
			FROM
			sys_users
			WHERE
			sys_users.intUserId =  '$intUser'";	
				
		 $resultp = $db->RunQuery($sqlp);
		 $rowp=mysqli_fetch_array($resultp);
			 
	return $rowp['intDepartmentId'];

}
?>


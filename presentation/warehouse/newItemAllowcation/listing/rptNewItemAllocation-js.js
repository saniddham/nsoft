// JavaScript Document
var basePath = "presentation/warehouse/newItemAllowcation/listing/";

$(document).ready(function() {
	$('#frmItemAllocationReport').validationEngine();
	
	$('#frmItemAllocationReport #imgApprove').click(function(){
	var val = $.prompt('Are you sure you want to approve this Item Allocation ?',{
			buttons: { Ok: true, Cancel: false },
			callback: function(v,m,f){
			if(v)
			{
					showWaiting();
					if(validateQuantities()==0){
					///////////
					var url = basePath+"rptNewItemAllocation-db-set.php"+window.location.search+'&status=approve';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmItemAllocationReport #imgApprove').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmItemAllocationReport #imgApprove').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						}
 					   hideWaiting();
			}
			}});
	});
	
	$('#frmItemAllocationReport #imgReject').click(function(){
	var val = $.prompt('Are you sure you want to reject this Item Allocation ?',{
		buttons: { Ok: true, Cancel: false },
		callback: function(v,m,f){
			if(v)
			{
					showWaiting();
					if(validateRejecton()==0){
					///////////
					var url = basePath+"rptNewItemAllocation-db-set.php"+window.location.search+'&status=reject';
					var obj = $.ajax({
						url:url,
						type:'post',
						dataType: "json",  
						data:'',
						async:false,
						
						success:function(json){
								$('#frmItemAllocationReport #imgReject').validationEngine('showPrompt', json.msg,json.type /*'pass'*/);
								if(json.type=='pass')
								{
									var t=setTimeout("alertx()",1000);
									window.location.href = window.location.href;
									window.opener.location.reload();//reload listing page
									return;
								}
							},
						error:function(xhr,status){
								
								$('#frmItemAllocationReport #imgReject').validationEngine('showPrompt', errormsg(xhr.status),'fail');
								var t=setTimeout("alertx()",3000);
							}		
						});
					////////////
						}
						hideWaiting();
			}
		}});
	});
	
});
//---------------------------------------------------
function validateQuantities(){
		var ret=0;	
	    var allocationNo = document.getElementById('divAllocNo').innerHTML;
		var url 		= basePath+"rptNewItemAllocation-db-get.php?requestType=getValidation";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"allocationNo="+allocationNo,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#imgApprove').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertx()",1000);
					  ret= 1;
					}
				}
			});
			return ret;
}
//---------------------------------------------------
function validateRejecton(){
		var ret=0;	
	    var allocationNo = document.getElementById('divAllocNo').innerHTML;
		var url 		= basePath+"rptNewItemAllocation-db-get.php?requestType=validateRejecton";
		var httpobj = $.ajax({
			url:url,
			dataType:'json',
			data:"allocationNo="+allocationNo,
			async:false,
			success:function(json){
					if(json.status=='fail')
					{
					$('#imgApprove').validationEngine('showPrompt', json.msg,json.status /*'pass'*/);
					//alert(json.msg);
					var t=setTimeout("alertx()",3000);
					  ret= 1;
					}
				}
			});
			return ret;

}
//------------------------------------------------
function alertx()
{
	$('#frmItemAllocationReport #imgApprove').validationEngine('hide')	;
}
//--------------------------------------------